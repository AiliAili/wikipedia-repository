{{good article}}
{{Infobox person
| name        = Karamokho Alfa
| image       = <!-- just the filename, without the File: or Image: prefix or enclosing [[brackets]] -->
| alt         = 
| caption     = 
| birth_name  = Ibrahima Musa Sambeghu
| birth_date  = <!-- {{Birth date and age|YYYY|MM|DD}} or {{Birth-date and age|Month DD, YYYY}} -->
| birth_place = 
| death_date  = c. 1751<!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|Month DD, YYYY|Month DD, YYYY}} (death date then birth date) -->
| death_place = 
| nationality = Fulbe
| other_names = 
| occupation  = Cleric
| known_for   = Founder of the Imamate of Futa Jallon
}}
[[File:Guinee Fouta Djalon Canyon.jpg|thumb|240px|Canyon in the Futa Jallon]]
'''Karamokho Alfa'''{{efn|Ibrahima Sambeghu was given the name "Karamokho Alfa" as an adult.  "Karamokho" means teacher in the [[Mandinka language]] and "Alfa" means teacher in the [[Fula language]].{{sfn|Isichei|1997|p=301}}}} (born '''Ibrahima Musa Sambeghu''' and sometimes called '''Alfa Ibrahim''') (died c. 1751) was a [[Fula people|Fula]] religious leader who led a [[jihad]] that created the [[Imamate of Futa Jallon]] in what is now [[Guinea]].  This was one of the first of the [[Fulbe jihad]]s that established Muslim states in [[West Africa]].

Alfa Ba, Karamoko Alfa's father, formed a coalition of Muslim Fulbe and called for the ''jihad'' in 1725, but died before the struggle began. The ''jihad'' was launched around 1726-1727. After a crucial, concluding victory at [[Talansan]], the state was established at a meeting of nine Fulbe ''[[ulama]]'' who each represented one of the Futa Jallon provinces. Ibrahima Sambeghu, who became known as Karamokho Alfa, was the hereditary ruler of [[Timbo]] and one of the nine ''ulama''. He was elected leader of the ''jihad''. Under his leadership, Futa Jallon became the first Muslim state to be founded by the Fulbe. Despite this, Karamokho Alfa was constrained by the other eight ''ulama''. Some of the other ''Ulama'' had more secular power than Karamokho Alfa, who directly ruled only the ''diwal'' of [[Timbo]]; for this reason the new state was always a tenuous confederation. Karamoko Alfa ruled the theocratic state until 1748, when his excessive devotions caused him to become mentally unstable and Sori was selected as de facto leader. Karamokho Alfa died around 1751 and was formally succeeded by Ibrahim Sori, his cousin.

==Background==

The [[Futa Jallon]] is the highland region where the [[Senegal River|Senegal]] and [[Gambia River|Gambia]] rivers rise.{{sfn|Ruthven|2006|p=264}}{{sfn|Haggett|2002|p=2316}}
In the fifteenth century the valleys were occupied by [[Mandé peoples]] - [[Susu people|Susu]] and [[Yalunka people|Yalunka]] farmers. Around that time, Fulbe herders began  moving into the region, grazing their livestock on the plateaux.  At first they peacefully accepted a subordinate position to the Susu and Yalunka.{{sfn|Gray|1975|p=207}} 
The Fulbe and Mandé peoples intermixed to some extent, and the more sedentary of the Fulbe came to look down on their [[Pastoralism|pastoral]] cousins.{{sfn|Willis|1979|p=25}}

Europeans began to establish trading posts on the upper Guinea coast in the seventeenth century, stimulating a growing trade in hides and slaves. The pastoral Fulbe expanded their herds to meet the demand for hides. They began to compete for land with the agriculturalists, and became interested in the profitable slave trade.{{sfn|Gray|1975|p=207}}  
They were increasingly influenced by their Muslim trading partners.{{sfn|Gray|1975|p=205}}

In the last quarter of the seventeenth century the [[Zawāyā]] reformer Nasir al-Din launched a jihad to restore purity of religious observance in the [[Futa Toro]] region to the north.  He gained support from the [[Torodbe]] clerical clan against the warriors, but by 1677 the movement had been defeated.{{sfn|Gray|1975|p=205}} 
Some of the Torodbe migrated south to [[Bundu, Senegal|Bundu]] and some continued on to the Futa Jallon.{{sfn|Gray|1975|p=206}}
The Torodbe, the kinsmen of the Fulbe of the Futa Jallon, influenced them in embracing a more militant form of Islam.{{sfn|Gray|1975|p=207}}

==Jihad==
{{Location map
| Guinea
| width   = 
| float   = 
| border  = 
| caption = Karamokho Alfa's capital of [[Timbo]] in modern [[Guinea]]
| alt     = 
| relief  = yes
| AlternativeMap = 
| overlay_image = 
| label   = 
| lat_deg = 10.633333
| lon_deg = -11.833333
}}
Alfa Ba, Karamoko Alfa's father, formed a coalition of Muslim Fulbe and called for the ''jihad'' in 1725, but died before the struggle began.{{sfn|Ndukwe|1996|p=48}}
The ''jihad'' was launched around 1726 or 1727.{{sfn|Amanat|Bernhardsson|2002|p=244}}
The movement was primarily religious, and its leaders included both Mandé and Fulbe ''[[marabout]]s''.{{sfn|Ogot|1992|p=289}}  
The ''jihad'' also attracted some formerly non-Muslim Fulbe, who associated it not just with Islam but with freedom of the Fulbe from subordination to the Mandé peoples.{{sfn|Gray|1975|p=208}}
It was opposed by other non-Muslim Fulbe and by non-Muslim Yalunka leaders.{{sfn|Ogot|1992|p=289}}

According to tradition, [[Ibrahim Sori]] symbolically launched the war in 1727 by destroying the great ceremonial drum of the Yalunka people with his sword.{{sfn|Alford|1977|p=4}}
The jihadists then won a major victory at [[Talansan]].{{sfn|Adamu|1988|p=244}}
A force of 99 Muslims defeated a non-Muslim force ten times greater, killing many of their opponents.{{sfn|Rashedi|2009|p=38}} 
After this victory the state was established at a meeting of nine Fulbe ''[[ulama]]'' who each represented one of the Futa Jallon provinces.{{sfn|Gray|1975|p=208}} 
Ibrahima Sambeghu, who became known as Karamokho Alfa,{{efn|The leaders of the Fulbe ''diwe'' used the title ''"Alfa"'', or "teacher".  Karamokho Alfa was the ''Alfa'' of the Timbo ''Diwal''.{{sfn|Ogot|1992|p=291}}}} was the hereditary ruler of [[Timbo]] and one of the nine ''ulama''. He was elected leader of the ''jihad''.{{sfn|Ogot|2010|p=346}}
He took the title ''[[almami]]'', or "the [[Imam]]".{{sfn|Holt|Lambton|Lewis|1977|p=365}}
Under his leadership Futa Jallon became the first Muslim state to be founded by the Fulbe.{{sfn|Ndukwe|1996|p=48}}

Karamoko Alfa managed to enlist disadvantaged groups such as gangs of young men, outlaws and slaves.{{sfn|Lapidus|2002|p=418}}
Karamokho Alfa's maternal cousin was [[Maka Jiba]], the ruler of Bundu, and both men studied in [[Fugumba]] under the famous scholar [[Tierno Samba]]. However, there are no records of Bundu participation in the Futa Jallon ''jihad'', perhaps because of the internal troubles in Bundu at that time, or perhaps because Maka Jiba was not greatly interested in the cause.{{sfn|Gomez|2002|p=72}}
Although he was an inspired religious leader, Karamoko Alfa was not qualified as a military leader.  Ibrahim Sori took this role.{{sfn|Alford|1977|p=4}} 
Some of the population resisted conversion for many years, particularly the nomadic Fulbe herders. They rightly feared that the ''marabouts'' would abuse their authority.{{sfn|Ogot|1992|p=292}}

[[File:Timbo and the sources of the Bafino.png|thumb|240px|Timbo and the sources of the Bafino - Fougumba to the northwest of the map, Timbo to the right of center.]]

==Ruler==

Karamokho Alfa was constrained by the other eight ''ulama'', each of whom ruled their own province, or ''diwal''.{{sfn|Ogot|2010|p=346}}{{efn|The provinces were [[Labé]], [[Buriya]], Timbi, [[Kebaali]], [[Kollade]], [[Koyin]], Fugumba and [[Fode Haaji]].{{sfn|Ogot|1992|p=291}}}}
The structure of the new Fulbe state had an ''[[almami]]'' at its head, Karamokho Alfa being the first, with his political capital at Timbo.{{sfn|U.N. Comité de culture|1999|p=331}} 
However, some of the other ''Ulama'' had more secular power than Karamokho Alfa, who directly ruled only the ''diwal'' of [[Timbo]].{{sfn|Gray|1975|p=208}}
The religious capital was at [[Fugumba]], where the council of the ''alama'' sat.  The council operated as a strong curb on the power of the ''almami'', and the ''ulama'' retained much autonomy, so the new state was always a loose federation.{{sfn|U.N. Comité de culture|1999|p=331}}

Karamokho Alfa was known for his Islamic scholarship and piety.{{sfn|Gray|1975|p=209}} 
He respected the rights of the old "masters of the soil", saying "it was Allah who had established them."
Despite this ruling, the ''[[imam]]s'' reserved the right to reassign land, since they held it in trust for the people.
In effect the existing property owners were not displaced, but now had to pay ''[[Zakāt]]'' as a form of rent.{{sfn|Willis|1979|p=28}}
Karamoko Alfa ruled the theocratic state until 1748, when his excessive devotions caused him to become mentally unstable and Sori was selected as de facto leader.{{sfn|Alford|1977|p=4}}

[[File:Fula jihad states map general c1830.png|thumb|240px|Fulbe Jihad states around 1830 - [[Futa Jallon]] to the west]]

==Legacy==
Karamokho Alfa died around 1751 and was formally succeeded by Ibrahim Sori, his cousin.{{sfn|Gray|1975|p=209}}{{efn|Ibrahima Sambeghu's ancestor Mamadou Moktar Bari had two sons. Fode Seri was the ancestor of the Seriyanke of Fougumba, and Fode Seidi was ancestor of the Seidiyanke of Timbo. Fode Seidi's great-grandson Alfa Kikala was the grandfather of both Almami Sory and Karamoko Alfa.{{sfn|Derman|Derman|1973|p=20}}}}  
Ibrahim Sori Mawdo was chosen because Alfa Saadibu, son of Karamoko Alfa, was too young.{{sfn|Harrison|2003|p=68}}
Ibrahim Sori was an aggressive military commander who initiated a series of wars.
After many years of conflict, Ibrahim Sori achieved a decisive victory in 1776 that consolidated the power of the Fulbe state. The ''jihad'' had achieved its goals and Ibrahim Sori assumed the title of ''almami''.{{sfn|Gray|1975|p=209}}

Under Ibrahima Sori slaves were sold to obtain munitions needed for the wars.  This was considered acceptable as long as the slaves were not Muslim.{{sfn|Thornton|1998|p=315-316}} 
The ''jihad'' created a valuable supply of slaves from the defeated peoples that may have provided a motive for further conquests.{{sfn|Gray|1975|p=207}}
The Fulbe ruling class became wealthy slave owners and slave traders. Slave villages were founded, whose inhabitants provided food for their Fulba masters to consume or sell. At one time more than half the population were slaves.{{sfn|Isichei|1997|p=301}}
As of 2013 the Fulbe were the largest ethnic group in Guinea at 40% of the population, after the Malinke (30%) and Susu (20%).{{sfn|AFRICA :: GUINEA - CIA}}

The ''jihad'' in Futa Jallon was followed by a ''jihad'' in Futa Toro between 1769 and 1776 led by [[Sileymaani Baal]].
The largest of the Fulani ''jihads'' was led by the scholar [[Usman dan Fodio]] and established the [[Sokoto Caliphate]] in 1808, 
stretching across what is now the north of [[Nigeria]].
The Fulbe Muslim state of [[Masina, Mali|Masina]] was established to the south of [[Timbuktu]] in 1818.  {{sfn|Stanton|Ramsamy|Seybolt|2012|p=148}}

[[File:Guinee Fouta Djalon Doucky.jpg|thumb|240px|Children in the village of [[Doucky]] in Futa Jallon in 2005]]
Karamokho Alfa came to be thought of as a saint. 
A story is told of a miracle that occurred more than a hundred years after his death.
The chief of the [[Ouassoulounké]], Kondé Buraima, opened Karamokho Alfa's tomb and cut off the left hand of the body.
Blood poured from the severed wrist, causing Kondé Buraima to flee in terror.{{sfn|Sanneh|1997|p=78}}

==See also==
* [[Alfaya (party)]]

==Notes and references==
'''Notes'''
{{notes}}
'''Citations'''
{{reflist|colwidth=30em}}
'''Sources'''
{{refbegin}}
*{{cite book |ref=harv
 |last=Adamu|first=Mahdi|title=Pastoralists of the West African Savanna|url=https://books.google.com/books?id=aWS7AAAAIAAJ&pg=PA244|accessdate=2013-02-11
 |date=1988-11-01|publisher=Manchester University Press|isbn=978-0-7190-2248-7}}
*{{cite web |ref={{harvid|AFRICA :: GUINEA - CIA}} |url=https://www.cia.gov/library/publications/the-world-factbook/geos/gv.html
 |title=AFRICA :: GUINEA|accessdate=2013-02-10 |publisher=CIA}}
*{{cite book |ref=harv
 |last=Alford|first=Terry|title=Prince Among Slaves|url=https://books.google.com/books?id=YArRYeytsQ0C&pg=PA4|accessdate=2013-02-10
 |year=1977|publisher=Oxford University Press|isbn=978-0-19-504223-8|page=4}}
*{{cite book |ref=harv
 |last1=Amanat|first1=Abbas|last2=Bernhardsson|first2=Magnus T.|title=Imagining the End: Visions of Apocalypse from the Ancient Middle East to Modern America
 |url=https://books.google.com/books?id=OuBWgjR0gyAC&pg=PA244|accessdate=2013-02-10
 |date=2002-02-09|publisher=I.B.Tauris|isbn=978-1-86064-724-6}}
*{{cite book|ref=harv
 |last1=Derman|first1=William|last2=Derman|first2=Louise|title=Serfs Peasants Socialst|url=https://books.google.com/books?id=Wr745AMRf2gC&pg=PA16|accessdate=2013-02-10
 |year=1973|publisher=University of California Press|isbn=978-0-520-01728-3}}
*{{cite book|ref=harv
 |last=Gomez|first=Michael A.|title=Pragmatism in the Age of Jihad: The Precolonial State of Bundu
 |url=https://books.google.com/books?id=gfhGouf7_TsC&pg=PA72|accessdate=2013-02-10
 |date=2002-07-04|publisher=Cambridge University Press|isbn=978-0-521-52847-4}}
*{{cite book|ref=harv
 |last=Gray|first=Richard|title=The Cambridge History of Africa|url=https://books.google.com/books?id=q3mx8aAo6x0C&pg=PA207|accessdate=2013-02-10
 |date=1975-09-18|publisher=Cambridge University Press|isbn=978-0-521-20413-2}}
*{{cite book|ref=harv
 |last=Haggett|first=Peter|title=Encyclopedia of World Geography|url=https://books.google.com/books?id=pR1Pszc1mVgC&pg=PA2316|accessdate=2013-03-04
 |year=2002|publisher=Marshall Cavendish|isbn=978-0-7614-7306-0}}
*{{cite book|ref=harv
 |last=Harrison|first=Christopher|title=France and Islam in West Africa, 1860-1960|url=https://books.google.com/books?id=K1E3WE8dVEUC&pg=PA68|accessdate=2013-02-10
 |date=2003-09-18|publisher=Cambridge University Press|isbn=978-0-521-54112-1|page=68}}
*{{cite book|ref=harv
 |last1=Holt|first1=P. M.|last2=Lambton|first2=Ann K. S.|last3=Lewis|first3=Bernard|title=The Cambridge History of Islam:
 |url=https://books.google.com/books?id=y99jTbxNbSAC&pg=PA365|accessdate=2013-02-10
 |date=1977-04-21|publisher=Cambridge University Press|isbn=978-0-521-29137-8}}
*{{cite book |ref=harv
 |last=Isichei|first=Elizabeth|title=A History of African Societies to 1870|url=https://books.google.com/books?id=3C2tzBSAp3MC&pg=PA301|accessdate=2013-02-10
 |date=1997-04-13|publisher=Cambridge University Press|isbn=978-0-521-45599-2|page=301}}
*{{cite book |ref=harv
 |last=Lapidus|first=Ira M.|title=A History of Islamic Societies|url=https://books.google.com/books?id=I3mVUEzm8xMC&pg=PA418|accessdate=2013-02-10
 |date=2002-08-22|publisher=Cambridge University Press|isbn=978-0-521-77933-3|page=418}}
*{{cite book|ref=harv
 |last=Ndukwe|first=Pat I.|title=Fulani|url=https://books.google.com/books?id=HnwGNU6uoBQC&pg=PA48|accessdate=2013-02-10
 |year=1996|publisher=The Rosen Publishing Group|isbn=978-0-8239-1982-6|page=48}}
*{{cite book|ref=harv
 |last=Ogot|first=Bethwell Allan|title=Africa from the Sixteenth to the Eighteenth Century|url=https://books.google.com/books?id=WAQbp7aLpZkC&pg=PA289|accessdate=2013-02-10
 |year=1992|publisher=UNESCO|isbn=978-92-3-101711-7|page=289}} 
*{{cite book |ref=harv
 |last=Ogot|first=Bethwell Allan|title=História Geral da África – Vol. V – África do século XVI ao XVIII
 |url=https://books.google.com/books?id=t7GE3JnMYgMC&pg=PA346|accessdate=2013-02-10
 |year=2010|publisher=UNESCO|isbn=978-85-7652-127-3|page=346}}
*{{cite book |ref=harv
 |last=Rashedi|first=Khorram|title=Histoire du Fouta-Djallon|url=https://books.google.com/books?id=SufoLuHKpr0C&pg=PA38|accessdate=2013-02-11
 |date=January 2009 |publisher=Harmattan|isbn=978-2-296-21852-9|page=38}}
*{{cite book|ref=harv
 |last=Ruthven|first=Malise|title=Islam in the World|url=https://books.google.com/books?id=92lQfWj6_VIC&pg=PA264|accessdate=2013-02-10
 |date=2006-02-24|publisher=Oxford University Press|isbn=978-0-19-977039-7}}
*{{cite book |ref=harv
 |last=Sanneh|first=Lamin O.|title=The Crown and the Turban: Muslims and West African Pluralism
 |url=https://books.google.com/books?id=ywXUKjjP0OQC&pg=PA78|accessdate=2013-02-10
 |year=1997|publisher=Basic Books|isbn=978-0-8133-3058-7}}
*{{cite book |ref=harv
 |last1=Stanton |first1=Andrea L. |last2=Ramsamy |first2=Edward |last3=Seybolt |first3=Peter J. |author4=Carolyn M. Elliott
 |title=Cultural Sociology of the Middle East, Asia, and Africa: An Encyclopedia|url=https://books.google.com/books?id=GtCL2OYsH6wC&pg=RA1-PA148|accessdate=2013-02-10
 |date=2012-01-05|publisher=SAGE|isbn=978-1-4129-8176-7}}
*{{cite book |ref=harv
 |last=Thornton|first=John|title=Africa and Africans in the Making of the Atlantic World, 1400-1800
 |url=https://books.google.com/books?id=AVZDHeVEeywC&pg=PA315|accessdate=2013-02-10
 |date=1998-04-28|publisher=Cambridge University Press|isbn=978-0-521-62724-5}}
*{{cite book|ref=harv
 |author=U.N. Comité de culture|title=Histoire générale de l'Afrique.: Volume V, L'Afrique du XVIe au XVIIIe siècle
 |url=https://books.google.com/books?id=qUGYlKG2t-0C&pg=PA331|accessdate=2013-02-10
 |date=1999-01-01|publisher=UNESCO|isbn=978-92-3-201711-6}}
*{{cite book |ref=harv
 |last=Willis|first=John Ralph|title=Studies in West African Islamic History|url=https://books.google.com/books?id=P0fkfzq4hUIC&pg=PA25|accessdate=2013-02-10
 |year=1979|publisher=Cass|isbn=978-0-7146-1737-4}}
{{refend}}

{{DEFAULTSORT:Alfa, Karamokho}}
[[Category:1751 deaths]]
[[Category:Fula people]]
[[Category:History of Guinea]]
[[Category:Fula history]]
[[Category:Year of birth unknown]]