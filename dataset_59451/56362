{{merge from|Research Network of Computational and Structural Biotechnology|date=March 2015}}
{{Infobox journal
| title = Computational and Structural Biotechnology Journal
| cover =
| editor = [[Gianni Panagiotou]]
| discipline = [[Computational biology]]
| abbreviation = Comput. Struct. Biotechnol. J.
| publisher = [[Elsevier]] on behalf of the [[Research Network of Computational and Structural Biotechnology]]
| CiteScore = 3.03
| impact =
| impact-year =
| history = 2012–present
| openaccess = Yes
| license = [[Creative Commons licenses|Creative Commons Attribution]]
| website = http://csbj.org/
| link1 = http://www.journals.elsevier.com/computational-and-structural-biotechnology-journal
| link1-name = Journal page at publisher's website
| link2 = http://www.sciencedirect.com/science/journal/20010370
| link2-name = Online archive
| eISSN = 2001-0370
| OCLC = 829963344
| CODEN = CSBJAC
}}
'''''Computational and Structural Biotechnology Journal''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]], published by [[Elsevier]] on behalf of the [[Research Network of Computational and Structural Biotechnology]] (RNCSB), covering all aspects of [[Computational biology|computational]] and [[structural biology]].<ref>{{cite web |title=Elsevier Publishes Open Access Journal: Computational and Structural Biotechnology Journal (CSBJ) on behalf of Research Network of Computational and Structural Biotechnology |url=http://www.elsevier.com/about/press-releases/research-and-journals/elsevier-publishes-open-access-journal-computational-and-structural-biotechnology-journal-csbj-on-behalf-of-research-network-of-computational-and-structural-biotechnology |accessdate=2015-01-17 |date=2014-06-02}}</ref> It was established in 2012 and the [[editor-in-chief]] is Gianni Panagiotou ([[University of Hong Kong]]). In June 2014 the RNCSB and Elsevier announced their collaboration.<ref>{{cite journal |url=http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4151870/pdf/main.pdf |title=Note from the Research Network of Computational and Structural Biotechnology (RNCSB) |volume=10 |issue=16 |date=11 June 2014 | doi=10.1016/j.csbj.2014.05.001 |pmid=25210601 |pmc=4151870 |journal=Computational and Structural Biotechnology Journal |pages=i}}</ref><ref>{{cite press release |url=http://www.prnewswire.com/news-releases/elsevier-publishes-open-access-journal-computational-and-structural-biotechnology-journal-csbj-on-behalf-of-research-network-of-computational-and-structural-biotechnology-261499341.html |title=Elsevier Publishes Open Access Journal: Computational and Structural Biotechnology Journal (CSBJ) on behalf of Research Network of Computational and Structural Biotechnology |website=prnewswire.com |date=2 June 2014 |accessdate=18 January 2014}}</ref>

As of February 2016, the Journal Authority Factor (JAF)<ref>{{cite journal |url=http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4349059/pdf/637.pdf |title=A Glaring Paradox |volume=199 |issue=3 |date=2 March 2015 | doi=10.1534/genetics.115.174771 |pmid=25740911 |pmc=4349059 |journal=Genetics |pages=i}}</ref> of ''Computational and Structural Biotechnology Journal'' is '''''32.5'''''.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[PubMed Central]], [[Scopus]], [[Global Health]], and [[Chemical Abstracts Service]].

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/computational-and-structural-biotechnology-journal}}

[[Category:Publications established in 2012]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:English-language journals]]
[[Category:Elsevier academic journals]]
[[Category:Biomedical informatics journals]]