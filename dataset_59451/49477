{{redirect|Dcom|the doctoral degree|Doctor of Commerce|other uses|DCOM (disambiguation)}}
{{Multiple issues|
{{Citation style|date=October 2011|details=Violates Wikipedia:External links: "Wikipedia articles may include links to web pages outside Wikipedia (external links), but they should not normally be used in the body of an article."}}
{{more footnotes|date=December 2010}}
}}

'''Distributed Component Object Model''' ('''DCOM''') is a [[proprietary software|proprietary]] [[Microsoft]] technology for communication between [[software componentry|software components]] on networked [[computer]]s. DCOM, which originally was called "Network [[Object Linking and Embedding|OLE]]", extends [[Microsoft]]'s [[Component Object Model|COM]], and provides the communication substrate under [[Microsoft]]'s [[Component Object Model#COM+|COM+]] application server infrastructure.

The addition of the "D" to COM was due to extensive use of [[DCE/RPC]] (Distributed Computing Environment/Remote Procedure Calls) – more specifically Microsoft's enhanced version, known as [[MSRPC]].

In terms of the extensions it added to COM, DCOM had to solve the problems of
*[[Marshalling (computer science)|Marshalling]] – serializing and deserializing the arguments and return values of method calls "over the wire".
*Distributed [[Garbage collection (computer science)|garbage collection]] – ensuring that references held by clients of interfaces are released when, for example, the client process crashed, or the network connection was lost.
*It had to combine Hundreds/Tens of Thousands of objects held in the client's browser with a single transmission in order to minimize bandwidth utilization.

One of the key factors in solving these problems is the use of DCE/RPC as the underlying RPC mechanism behind DCOM. DCE/RPC has strictly defined rules regarding marshalling and who is responsible for freeing memory.

DCOM was a major competitor to [[CORBA]]. Proponents of both of these technologies saw them as one day becoming the model for code and service-reuse over the [[Internet]]. However, the difficulties involved in getting either of these technologies to work over Internet [[Firewall (networking)|firewalls]], and on unknown and insecure machines, meant that normal [[HTTP]] requests in combination with [[web browser]]s won out over both of them. Microsoft, at one point, attempted and failed to head this off by adding an extra http transport to DCE/RPC called ''ncacn_http'' (Network Computing Architecture connection-oriented protocol). This was later resurrected to support a [[Microsoft Exchange Server|Microsoft Exchange]] 2003 connection over HTTP.

DCOM is supported natively in Windows NT 4.0, Windows 2000, Windows XP, and Windows Server 2003, as well as Windows 7, Windows 8, Windows 10, Windows Server 2008, Windows Server 2008 R2, Windows Server 2012, Windows Server 2012 R2 and the Windows Server 2016 Technical Preview.

== Hardening ==
As part of the initiative that began at Microsoft as part of Secure Development Lifecycle to re-architect insecure code, DCOM saw some significant security-focused changes in Windows XP Service Pack 2.<ref>[http://technet.microsoft.com/en-us/library/bb457156.aspx#EIAA DCOM Security Enhancements]</ref>

== Alternative versions and implementations ==
'''COMsource''': Its source code is available, along with full and complete documentation, sufficient to use and also implement an interoperable version of DCOM. According to that documentation, COMsource comes directly from the [[Windows NT]] 4.0 source code, and even includes the source code for a [[Windows Registry|Windows NT Registry Service]].

The [[Wine (software)|Wine]] Team is also implementing DCOM for binary interoperability purposes; they are not currently interested in the networking side of DCOM, which is provided by [[Microsoft RPC|MSRPC]]. They are restricted to implementing [[Network Data Representation|NDR]] (Network Data Representation) through Microsoft's API{{Citation needed|date=June 2007}}, but are committed to making it as compatible as possible with MSRPC.

'''TangramCOM''' is a separate project from [[Wine (software)|Wine]], focusing on implementing DCOM on Linux-based smartphones.

The [[Samba (software)|Samba]] Team is also implementing DCOM for over-the-wire interoperability purposes: unlike the Wine Team, they are not currently interested in binary-interoperability, as the Samba MSRPC implementation is far from binary-interoperable with Microsoft's MSRPC.

== See also ==
* [[ActiveX]]
* [[Component Object Model]]
* [[Dynamic Data Exchange]] (DDE)
* [[.NET Remoting]]
* [[OLE for process control|OLE for Process Control]]

==References==
{{reflist}}

== External links ==
* [http://msdn.microsoft.com/library/cc201989.aspx DCOM Remote Protocol Specification]
* [http://opengroup.org/comsource The Open Groups COMsource]
* [http://www.opengroup.org/comsource/ COMsource]
* [https://web.archive.org/web/20091213022641/http://tcom.andjoin.com:80/ TangramCOM]
* [http://www.softwareag.com/Corporate/products/lm/leg_integration/downloads/default.asp EntireX DCOM]

{{Microsoft APIs}}
{{Microsoft Windows components}}

[[Category:Component-based software engineering]]
[[Category:Inter-process communication]]
[[Category:Windows communication and services]]
[[Category:Object models]]
[[Category:Object request broker]]