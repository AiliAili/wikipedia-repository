'''Thomas Cochrane''' <small>[[Bachelor of Medicine, Bachelor of Surgery|M.B.]] [[Magister Chirurgiae|C.M]]</small> (1866–1953) was a [[Scottish people|Scottish]] [[Medical missions in China|medical missionary]]. He is notable for founding the first school to teach Western Medicine to Chinese trainee doctors and for his influential [[London]]-based campaigns to promote missionary work.

==Early life==
Cochrane was brought up in [[Greenock]], [[Scotland]].<ref>see F.French, p.16</ref> In 1882 he was inspired to become a missionary after listening to the evangelical preacher [[Dwight L. Moody]].<ref>See F. French p.18-19</ref> He trained to become a doctor at [[Glasgow University]] and in 1897 traveled to China with the [[London Missionary Society]].<ref>See F. French p.22-25</ref>

==Work in China==
Cochrane arrived in Chaoyang hospital, [[Mongolia]] when the country was in the throes of the [[Boxer rebellion]].<ref>See  F. French,  p.47-52</ref> Braving threats from bandits, he traveled from village to village in an ox cart dispensing medical aid. Mr and Mrs Liddell, the parents of [[Eric Liddell]] the famous runner, were also  there at the time.<ref>See F. French p.50</ref> In 1900, he moved to [[Beijing]] to restore the hospital there which had been almost totally demolished.<ref>See F. French p.55</ref> Cochrane succeeded in obtaining royal support for his work after he healed the [[Empress Dowager Cixi]]'s chief eunuch and her chief lady in waiting.<ref>See F. French p.64-67</ref> He decided to establish the hospital as a training hospital to train Chinese as western style doctors, and with the support of the empress and other missionary bodies founded the Peking Union Medical College.<ref>See F.French pp.68-78</ref>  To support the students, Cochrane translated western medical books like ''Heath’s Anatomy'' (1909)  and ''Heath’s Osteology'' (1910) into [[Mandarin Chinese|Mandarin]]. In 1916 the hospital was handed over to the [[Rockefeller University|Rockefeller Institute]].<ref>See F. French p. 82</ref>

==Campaigns to promote missionary work==
Cochrane believed that missionary work should be co-ordinated, organised and well supplied. In 1913, the Christian Literature Society of China published his Survey of the Missionary Occupation of China with an accompanying atlas showing missionary occupation in the provinces of China. The work called for trained personnel, medical equipment, and supplies.  After he returned to England in 1915, he continued to encourage the gathering and promotion of knowledge on the state of missionary work and needs across the world.  He founded the Mildmay Movement for World Evangelisation, based in the Mildmay Centre, next to the Mildmay Mission Hospital (a separate and older organisation)  in [[Islington]], London.<ref>See F. French pp.87-92</ref> In 1924, together with like minded trustees businessman Sidney J. W. Clark and [[Roland Allen]] he set up the Survey Application Trust, an organisation to promote missionary work through publications.<ref>See F. French p. 94</ref> He himself served as the editor of the publication ''World Dominion''.<ref>See F. French p.96</ref> In 1949, he founded the publication ''The World Christian Handbook''.<ref>See F. French p.114</ref>  His campaigning though his publications and the missionary conferences held at the Mildmay Centre were an inspiration to many at the time to fund and participate in missionary activities.<ref>Oral history: personal recollections of John Haddon Martin, and S. King p.5-7</ref>

==Personal life==
Cochrane was married twice. He had three sons by his first wife and after she died in 1930, he married again and gained six stepdaughters. He died in [[Pinner]], [[Middlesex]] in 1953.

==Notes==
{{Reflist}}

==References==
*Francesca French, Thomas Cochrane, Pioneer and Missionary Statesman, London: Hodder and Stoughton 1956
*Obituary, Times, Thursday, Dec 10, 1953; pg. 8; Issue 52801; col D
*George A. Hood, [http://www.bdcconline.net/en/stories/c/cochrane-thomas.php Thomas Cochrane], 1856-1953 in ''Biographical Dictionary of Chinese Christianity'', accessed 30 July 2010
*Dr T. Cochrane, Medical Missionary in China, The Times, Thursday, Dec 10, 1953; pg. 10; Issue 52801; col E
*Practical anatomy : a manual of dissections / [by Christopher Heath] ; translated by Thomas Cochrane and E.T. Hsieh. 1911. (9th ed. / edited by J. Ernest Lane.) Shanghai : China Medical Missionary Association, 1911
<!--*K. Chimin Wong, History of Chinese Medicine, Tientsin, Tientsin Press, c. 1932, mentions the founding of the Peking Union Missionary college p.&nbsp;466–472 but interestingly makes no mention of Cochrane.-->

==External links==
* {{Gutenberg author | id=Cochrane,+Thomas | name=Thomas Cochrane}}
* {{Internet Archive author |sname=Thomas Cochrane |birth=1866 |death=1953}}

{{Protestant missions to China}}

{{Authority control}}

{{DEFAULTSORT:Cochrane, Thomas}}
[[Category:1866 births]]
[[Category:1953 deaths]]
[[Category:Christian medical missionaries]]
[[Category:Protestant missionaries in China]]
[[Category:Scottish evangelicals]]
[[Category:Scottish Protestant missionaries]]
[[Category:Protestant missionaries in Mongolia]]
[[Category:British expatriates in China]]