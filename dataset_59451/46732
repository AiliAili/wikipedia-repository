{{Infobox person
| name = Joanne Brough
| image = JB3.jpg
| caption = Joanne Brough
| imagesize = 220 px
| birth_name = Joanne Estelle Walker
| birth_date = {{birth date|1927|11|4|mf=yes}}
| birth_place = [[Joplin, Missouri]], U.S.
| death_date = {{death date and age|2005|2|24|1927|11|4|mf=yes}}
| death_place = [[Joplin, Missouri]], U.S.
| occupation = Television producer, executive
| spouse = Arthur Chaves (d.) (m. 1949–1957; divorced)<br />Charles Brough (d.) (m. 1968–2005) 
| children = 3
}}

'''Joanne Brough''' (aka Joan Brough) (born November 4, 1927) was an American [[television producer]] and executive. She began her career in 1960 at [[KTLA]], a Los Angeles television station, and went on to work for [[CBS]] Television from 1963 to 1978, rising through the ranks to become one of the first female network [[development executive]]s.<ref>Myrna Oliver, [http://articles.latimes.com/2005/mar/12/local/me-brough12 “Joanne Brough, 77; TV Producer, Network Development Executive,”] ''Los Angeles Times,'' March 12, 2005.</ref>  She oversaw and developed such hit shows as ''All in the Family, Kojak, Hawaii Five-O, M*A*S*H, Knots Landing, Eight Is Enough, The Waltons, Dallas, Falcon Crest'', and others. From 1978 to 1986, she joined [[Lorimar Productions]], becoming Vice President of Creative Affairs, supervising all television programs. During her tenure there, she became Executive Producer of ''[[Falcon Crest]]'', as well as several movies for television. In 1990, she went to work for Lee Rich Productions in association with [[Warner Brothers]], as a development executive, producing TV films and specials for three years. From 1993 to 1998, she took on the task of producing television in [[Singapore]] and [[Indonesia]]. Her later years were spent as an educator while continuing to develop new projects.

She received two Nosotros Awards and a medal from the Mexican-American Opportunity Foundation while producing the TV series ''Falcon Crest''. She also received an award from the Dyslexia Association of Singapore for the Asian dramatic series ''Masters of the Sea'', presented by the wife of the Prime Minister of Singapore.

== Early life ==
Joanne Brough (né Joanne Estelle Walker) was born in Joplin, Missouri, to father James Franklin Walker, an entrepreneur, real estate developer, and photographer, and mother Marion Tindall Walker (Smith), a Presbyterian deacon later in life. She grew up at her parents’ Sagmount Hotel and Inn,<ref>[http://cdm.sos.mo.gov/cdm4/item_viewer.php?CISOROOT=/jplnpstcrds&CISOPTR=927&CISOBOX=1&REC=9 Missouri Digital Heritage Library: Sagmount]</ref> a large resort in the countryside outside of Joplin. She first attended Lake Hill School, a two-room country grammar school, where ages were mixed. Upon graduating from Joplin High School, she traveled to Los Angeles to attend the [[University of California]] in 1945 as an English major at age sixteen. She returned to Joplin when her father died, and later married her first husband, Arthur R. Chaves, there in 1949. They then settled in [[Los Angeles]], California, in 1950.

== Early career ==
Her early career paved a path to her future achievements in television production. As a young woman, she worked for Joplin radio station [[WMBH]] as a DJ spinning the 78-rpm hits of the 1940s and as host of four talk shows. Later in Los Angeles, she was hired as an assistant promotions director for [[Paramount Television|Paramount TV Productions]] at local TV station [[KTLA]]. She also worked as a [[literary agent]] at Film Artists International and as a sub-writer on the daytime soap opera ''[[The Edge of Night]]'' under the writer [[Henry Slesar]].

== Executive ==
In 1963, Joanne went to work for the [[CBS]] Television Network and remained there until 1978. During her initial three years at CBS, she served as a reader under her mentor Helen Madden, and was later promoted to executive story editor for the network in 1973. She then advanced at CBS to become a program development executive as CBS enjoyed ratings success. She worked in all areas: comedy series, drama series, movies, and miniseries. Some of the hit series included [[Earl Hamner]]’s ''The Waltons, Hawaii-Five-0, The Mary Tyler Moore Show, Kojak, All in the Family'', and ''M*A*S*H''.<ref>Aida Pavletich, “Women in Film: Joanne Brough,” ''The Hollywood Reporter'', July 25, 1973.</ref>

While at CBS, she was also involved in the original development of the long-running TV series ''Dallas'', and was consequently hired by [[Lorimar Television]] (producers of ''Dallas'') during the first season of the show in 1978. She went on to spend eight years as Vice President Creative Affairs at Lorimar, reporting directly to the president, [[Lee Rich]].

During this period, she supervised all of Lorimar's on-air shows and also worked in development of all new shows, including the number-one miniseries ''Lace'', based on [[Shirley Conran]]’s best-selling novel. At the same time, she created a number of series presentations. She was closely involved in the development of many top prime-time soaps besides ''Dallas'', such as ''Knots Landing, Falcon Crest'', and ''Flamingo Road''. She also supervised these programs, reading and giving notes for each draft of every script, and also for every completed episode. She viewed most dailies and attended all pilot casting sessions. One of her anecdotes during that time involved the highly rated "Who Shot J.R.?" episode of ''Dallas'', the outcome of which was a guarded secret that she never divulged, despite pleas from family, friends, and the media.

== Producer ==
While at Lorimar, Joanne became an executive producer, overseeing at least five movies that were made for network television from 1981 through 1986.

That experience led her to take the helm as Executive Producer of the Emmy-award-winning drama<ref>[http://www.imdb.com/title/tt0081858/awards?ref_=tt_awd “Falcon Crest: Awards,” Internet Movie Database]</ref> ''Falcon Crest'', starring [[Jane Wyman]], for three seasons beginning in 1986, and later as consulting producer for one season—for a total of 106 episodes.

From 1992 to 1993, Joanne Brough executive produced ''Killer Rules'', a murder mystery movie about the Italian Mafia in Rome, Italy, for Lee Rich Productions, Warner Brothers, and NBC.

In October 1993, she embarked on a new phase of her career at age sixty-six, traveling to [[Singapore]] under a two-year contract to start an English drama industry there for the [[Singapore Broadcasting Corporation]] (SBC). She created and executive produced the first English drama in Asia, ''Masters of the Sea.''<ref>Andrea Teo, [http://dr.ntu.edu.sg/bitstream/handle/10220/2606/AMIC_1998_SEP2-4_14.pdf;jsessionid=70BA656195481A4E9B75FC67C4A8E90B?sequence=1  “TV entertainment in Singapore,” page 4, in AMIC Seminar on TV Content: the Asian way, Bangkok. Singapore: Asian Media Information and Communication Centre, September 2–4, 1998]</ref> In 1994, one year after her arrival, she had trained the writers, directors, cast, and crew well enough to be on the air with the new prime time drama series, modeled after ''Dallas'' for an Asian audience. Later in 1995 and 1996, she produced a new one-hour police story series and one-hour family drama series, both of which remained on the air in 1997.

With drama in Singapore well underway, she accepted a contract in 1995 from [[RCTI]], Indonesia's number-one television network, where she did the same thing, only in another language—[[Bahasa Indonesia]]. She spent several years as an expat in [[Jakarta]], Indonesia, where the staff she trained and mentored produced Indonesia's first open-ended serialized drama.<ref>Melissa Grego, “Living Dangerously,” ''The Hollywood Reporter'', November 3–9, 1998.</ref> She might have remained there longer but for the political unrest of 1997–1998 that forced her to flee with her husband as rioters protested Suharto’s government and “smoke billowed from gutted buildings”<ref>Doug Johnson, “Ex-TV Producer Finds a New Drama: Teaching,” ''The Washington Times'', February 21, 2000.</ref> near their Jakarta home.

== Educator ==
Joanne had served as a guest lecturer on television production at [https://www.uclaextension.edu/Pages/default.aspx UCLA Extension] in the 1980s. Later in 1998, upon returning from [[Southeast Asia]], she settled in her hometown of [[Joplin, Missouri]], having been drafted to teach courses in serialized TV drama and script writing in the Communications department at [[Missouri Southern State University]].<ref>[https://query.nytimes.com/gst/fullpage.html?res=9F00E5DD123CF936A25750C0A9639C8B63 “Joanne Brough, 77, Influential TV Executive,” The New York Times, March 15, 2005.]</ref> When asked about readjusting to small-town life, she commented that it was "just right for someone who had to escape a revolution."<ref>Doug Johnson, “Ex-TV Producer Finds a New Drama: Teaching,” ''The Washington Times'', February 21, 2000.</ref> She spent her final years there in the company of visiting colleagues, media scholars, family, and friends.

== Personal life ==
She was married for thirty-seven years to Charles Brough, a social science researcher. She had three children from her prior marriage to Arthur Chaves (Cheryl Preston, a writer/editor; Alice Capello, a photographer; Arthur Woodson Chaves, aka Ray Woodson, a radio sportscaster at [[KNBR]] in San Francisco). She also had three grandchildren (Shawna Halsey, Acacia May, Nicholas Chaves), as well as two step-daughters (Toni Brough Martin (d.), Marianna Strongheart). Joanne was a member of [[Mensa International]] and the [[Academy of Television Arts & Sciences]].

== Filmography ==
* ''[[Hawaii Five-O]]'' (1968–80) (development executive)
* ''[[The Mary Tyler Moore Show]]'' (TV Series) (1970–77) (development executive)
* ''[[All in the Family]]'' (TV Series) (1971–79) (development executive)
* ''[[The Waltons]]'' (TV Series) (1971–81) (development executive)
* ''[[M*A*S*H]]'' (TV Series) (1972–83) (development executive)
* ''[[Kojak]]'' (TV Series) (1973–78) (development executive)
* ''[[Dallas]]'' (TV Series) (1978–91) (development executive)
* ''[[Knots Landing]]'' (TV Series) (1979–93) (executive program supervisor)
* [[Flamingo Road (TV series)|''Flamingo Road'']] (TV Series) (1980–82) (executive program supervisor)
* [http://www.imdb.com/title/tt0082616/ ''Killjoy''] (TV Movie) (1981) (executive program supervisor)
* [http://www.imdb.com/title/tt0082724/?ref_=fn_al_tt_2 ''A Matter of Life and Death''] (TV Movie) (1981) (executive program supervisor)
* [http://www.imdb.com/title/tt0082757/?ref_=fn_al_tt_1 ''Mistress of Paradise''] (1981) (TV Movie) (executive producer)
* [http://www.imdb.com/title/tt0082868/?ref_=fn_al_tt_1 ''Our Family Business''] (1981) (TV Movie) (executive producer)
* [http://www.imdb.com/title/tt0084832/?ref_=fn_al_tt_4 ''Two of a Kind''] (TV Movie) (1982) (executive program supervisor)
* [http://www.imdb.com/title/tt0084789/?ref_=fn_al_tt_1 ''This Is Kate Bennett...''] (1982) (TV Movie) (executive producer)
* [http://www.imdb.com/title/tt0084891/?ref_=fn_al_tt_1 ''Washington Mistress''] (1982) (TV Movie) (executive producer)
* [http://www.imdb.com/title/tt0085104/?ref_=fn_al_tt_1 ''Two Marriages''] (1983–84) (TV Series) (executive program supervisor)
* [[Lace (miniseries)|''Lace'']] (1984) (TV Miniseries) (development executive, executive program supervisor)
* ''[[Falcon Crest]]'' (1986–89) (TV Series – 78 episodes) (executive producer)
* ''[[Falcon Crest]]'' (1989–90) (TV Series – 11 episodes) (consulting producer)
* [http://articles.latimes.com/1991-04-01/entertainment/ca-1149_1_america-s-missing-children ''America's Missing Children''] (1991) (TV Special) (supervising producer)
* [http://www.imdb.com/title/tt0107318/?ref_=fn_al_tt_1 ''Killer Rules''] (1992) (TV Movie) (executive producer)
* ''Masters of the Sea'' (1993–95) (TV Series - Singapore) (executive producer, creator)
* ''[[Triple Nine (TV series)|Triple Nine]]'' (1995–98) (TV Series – Singapore) (consulting producer; wrote bible)
* ''Asian War Chronicles'' (1995) (TV Series in development – Singapore) (consulting producer)
* ''Growing Up'' (1995) (TV Series – Singapore) (consulting producer)
* ''Two Sides of the Coin (Dua Sisi Mata Uang)'' (1995–98) (TV Series – Indonesia) (executive producer, creator)

==References==
{{reflist}}

== Other external links ==
* [http://www.imdb.com/name/nm0112652/ Joanne Brough] at the Internet Movie Database
* Joanne Brough at [http://www.falconcrest.org/ ''Falcon Crest'' fan website]

{{Authority control}}

{{DEFAULTSORT:Brough, Joanne}}
[[Category:People from Joplin, Missouri]]
[[Category:American television producers]]
[[Category:1927 births]]
[[Category:2005 deaths]]