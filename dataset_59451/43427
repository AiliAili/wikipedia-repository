<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=P.V.5 & P.V.5A
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Floatplane Fighter
 | national origin=[[United Kingdom]]
 | manufacturer=[[Port Victoria Marine Experimental Aircraft Depot]]|RNAS Marine Experimental Aircraft Depot, Port Victoria
 | designer=
 | first flight=July 1917
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Port Victoria P.V.2]]
 | variants with their own articles=
}}
|}
The '''Port Victoria P.V.5''' was a [[United Kingdom|British]] single-engined [[floatplane]] [[fighter aircraft]] of the First World War. A single example was built and flown at the [[Royal Naval Air Service]]'s [[Port Victoria Marine Experimental Aircraft Depot]] on the [[Isle of Grain]] in 1917. Despite demonstrating good manoeuvrability and handling, no production followed, with the Royal Naval Air Service instead using landplanes for the fighter role.

==Design and development==
In 1916, the Air Department of the British [[Admiralty]] issued a requirement for a single-seater fighter floatplane. The specification demanded a speed of {{convert|85|kn|abbr=on}} at {{convert|6500|ft|m|abbr=on}}, an endurance of four hours and an armament of a single machine gun and two 65&nbsp;lb (30&nbsp;kg) bombs. The use of a {{convert|150|hp|kW|abbr=on}} [[Smith Static]] [[radial engine]] as powerplant was requested.<ref name="Collyer p50-1">Collyer ''Air Enthusiast'' Forty-three, pp. 50–51.</ref><ref name="Bruce57 p334">Bruce 1957, p. 334.</ref>

The [[Port Victoria Marine Experimental Aircraft Depot]] prepared two designs to meet the requirement. One, the Port Victoria P.V.5, was a development of its earlier [[Port Victoria P.V.2|P.V.2]] [[sesquiplane]], while the P.V.5A differed in having a more conventional [[biplane]] wing.<ref name="Collyer p50-1"/>

Both the P.V.5 and P.V.5A were nearing completion in late 1916, but the absence of their intended engines delayed testing.<ref name="Collyer p51">Collyer ''Air Enthusiast'' Forty-three, p. 51.</ref> The Smith Static was an experimental ten-cylinder single-row radial engine developed by the American John W. Smith, which had attracted the attention of the Admiralty because of its light weight and promised low fuel and oil consumption,<ref name="Bruce 57 p8,333">Bruce 1957, pp. 8, 333.</ref> but proved to be a failure, with only a few engines ever completed.<ref name="Mason bomb p65">Mason 1994, p. 65.</ref><ref name="Bruce57 p8">Bruce 1957, p. 8.</ref> When it was realised at Grain that the Smith Static (which was to be used by the [[Port Victoria P.V.4|P.V.4]] and P.V.5A as well as the P.V.5) would not be forthcoming, a 150&nbsp;hp [[Hispano-Suiza 8]] [[V8 engine]] was obtained, and it was decided to modify the P.V.5 to use it.<ref name="Collyer p50-1"/> Soon after, in January, overall control for the supply of aircraft was transferred to the [[Ministry of Munitions]], who subjected the operations of Port Victoria to scrutiny, and while work continued on the P.V.5, the P.V.5A was suspended.<ref name="Bruce57 p335-6">Bruce 1957, pp. 335–336.</ref><ref name="mason fighter p109">Mason 1992, p. 109.</ref> Work eventually restarted on the P.V.5A, and it flew in 1918 fitted with a 200 hp Hispano-Suiza, although no production followed.<ref name="Bruce57 p336"/>

The P.V.5's wing bracing struts also carried the aircraft's floats, forming a "W" shape when viewed from the front, with no bracing wires used, while a high-lift wing section, developed by the [[National Physical Laboratory (United Kingdom)|National Physical Laboratory]] and used on the [[Port Victoria P.V.1]] and P.V.2 was again employed. Armament was the specified single [[synchronization gear|synchronised]] [[Vickers machine gun]], with two 65&nbsp;lb bombs carried internally. The Hispano-Suiza engine was enclosed in an annular cowling and drove a two-bladed propeller. Flat-bottomed pontoon-type floats were fitted, which were angled outwards to divert spray away from the engine and propeller.<ref name="Collyer p50-1"/><ref name="mason fighter p109">Mason 1992, p. 109.</ref><ref name="Bruce57 p334-5">Bruce 1957, pp. 334–335.</ref>

The P.V.5, serial number ''N53'', flew in July 1917,<ref name="mason fighter p109"/> but capsized when it alighted at the end of its first flight when a float failed.<ref name="Collyer p51"/> The aircraft was manoeuvrable and pleasant to fly, with a good view from the cockpit,<ref name="Bruce57 p336">Bruce 1957, p. 56.</ref> but performance failed to meet specifications,<ref name="mason fighter p109"/> this being blamed by Port Victoria on the aircraft's propeller being poorly matched to the aircraft, and the Hispano-Suiza engine being heavier than the Smith Static that the aircraft was designed for.<ref name="Collyer p51-2">Collyer ''Air Enthusiast'' Forty-three, pp. 51–52.</ref> No production followed, with the fighter requirements of the Royal Naval Air Service already being met by landplanes such as the [[Sopwith Pup]] and [[Sopwith Camel|Camel]]<ref name="mason fighter p109"/>

==Specifications (P.V.5) ==
{{Aircraft specs
|ref=British Aeroplanes 1914–18<ref name="bruce57 p336-7">Bruce 1957, pp. 336–337.</ref>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=25
|length in=6
|length note=
|upper span m=
|upper span ft=32
|upper span in=
|upper span note=
|lower span m=
|lower span ft=21
|lower span in=
|lower span note=
|height m=
|height ft=9
|height in=9
|height note=
|wing area sqm=
|wing area sqft=245
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=1788
|empty weight note=
|gross weight kg=
|gross weight lb=2456
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity= {{convert|36|impgal|abbr=on}}
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hispano-Suiza 8]]
|eng1 type=water-cooled [[V8 engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=150<!-- prop engines -->
|eng1 note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=94.5
|max speed kts=
|max speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=9900
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=<br>
**4 min 50 s to {{convert|2000|ft|m|abbr=on}}
**20 min 15 s to {{convert|6500|ft|m|abbr=on}}
<!--
        Armament
-->
|guns= 1× [[.303 British|0.303 in]] [[Vickers machine gun]]
|bombs= 2× 65 lb (30 kg) bombs

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|group=lower-alpha}}

{{reflist|30em}}

==References==
{{refbegin}}
*{{cite book|last=Bruce|first=J.M.|title=British Aeroplanes 1914–18|year=1957|publisher=Putnam|location=London}}
*{{cite magazine|last=Collyer|first=David|title=Babies Kittens and Griffons|magazine=[[Air Enthusiast]]|issue=Forty-three|year=1991|pages=50–55|issn=0143-5450}}
*{{Cite book |last=Mason|first= Francis K. |title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books |year=1994 |isbn= 0-85177-861-5}}
*{{cite book|last=Mason|first=Francis K.|title=The British Fighter since 1912|year=1992|publisher=Naval Institute Press|location=Annapolis, Maryland, USA|isbn=1-55750-082-7}}
{{refend}}

==External links==
*[http://www.airwar.ru/enc/fww1/pv5.html "PV.5"] ''Уголок Неба'' (in Russian).

{{Port Victoria Aircraft}}

[[Category:Floatplanes]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Sesquiplanes]]
[[Category:Single-engine aircraft]]
[[Category:Port Victoria aircraft|PV5]]