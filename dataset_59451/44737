{{italic title}}{{Infobox journal
| cover =<!-- Deleted image removed:  [[File:JEB 124-8.jpg|J Exp Biol cover]] -->
| title = The Journal of Experimental Biology
| discipline   = [[Comparative physiology]], integrative [[biology]]
| website      = http://jeb.biologists.org
| link1 = http://jeb.biologists.org/contents-by-date.0.shtml
| link1-name = Online access
| publisher    = [[Company of Biologists|The Company of Biologists]]
| country      = [[United Kingdom]]
| abbreviation = J. Exp. Biol.
| frequency    = 24 per year
| openaccess   = After 6 months
| impact       = 2.914
| impact-year  = 2015
| history = 1923-present
| formernames  = ''The British Journal of Experimental Biology'' (1923–1925)
| ISSN = 0022-0949
| eISSN =  1477-9145
| RSS = http://jeb.biologists.org/rss/current.xml
| OCLC = 1754580
}}
'''''The Journal of Experimental Biology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of [[comparative physiology]] and [[integrative biology]]. It is published by [[Company of Biologists|The Company of Biologists]] from editorial offices in [[Cambridge]], [[United Kingdom]].

== History ==
The journal was established in [[Edinburgh]] in 1923, entitled ''The British Journal of Experimental Biology'' (''Br. J. Exp. Biol.'': {{ISSN|0366-0788}}). It was published by [[Oliver and Boyd]] and edited by F. A. E. Crew and an Editorial Board of nine members including [[Julian Huxley]].<ref>[http://jeb.biologists.org/cgi/issue_pdf/edboard_pdf/1/1 Editorial Board for Volume 1, Issue 1 - ''Journal of Experimental Biology''<!-- Bot generated title -->]</ref> However, the journal soon ran into financial trouble and was rescued in 1925 by G. P. Bidder, the founder of the [[Company of Biologists|The Company of Biologists]].<ref>[http://www.raunvis.hi.is/~steindor/enska.html Steindór J. Erlingsson<!-- Bot generated title -->]</ref><ref>[http://jeb.biologists.org/cgi/content/full/207/1/1 Happy Anniversary JEB! - Phillips 207 (1): 1 - ''Journal of Experimental Biology''<!-- Bot generated title -->]</ref> Following the appointment of [[James Gray (zoologist)|Sir James Gray]] as the journal's first Editor-in-Chief in 1925, the journal was renamed ''The Journal of Experimental Biology'' in 1929 ({{ISSN|0022-0949}}).

Since the journal's establishment in 1923, there have been seven Editors-in-Chief: Sir James Gray (1926–1955), J. A. Ramsay (1952–1974), Sir [[Vincent Wigglesworth]] (1955–1974), John Treherne (1974–1989), Charlie Ellington (1989–1994), and Bob Boutilier (1994–2003). {{As of|2004}}, Hans Hoppeler ([[Bern]]) is the journal's current Editor-in-Chief.

The journal has published ground-breaking work in the areas of [[biomechanics]], skin [[Organ transplant|transplantation]],<ref>[http://jeb.biologists.org/cgi/content/abstract/28/3/385 The Technique of Free Skin Grafting in Mammals - Billingham and Medawar 28 (3): 385 - ''Journal of Experimental Biology''<!-- Bot generated title -->]</ref> and [[neurophysiology]],<ref>[http://jeb.biologists.org/cgi/content/abstract/26/2/201 The Efferent Regulation of the Muscle Spindle in the Frog - KATZ 26 (2): 201 - Journal of Experimental Biology<!-- Bot generated title -->]</ref> and has published work by [[Nobel Prize]] winners [[Peter Medawar]] and [[August Krogh]].

== The modern journal ==

The journal currently publishes papers on a wide range of subjects ranging from [[biomechanics]] and [[Metabolism|metabolic]] [[physiology]] to [[neurophysiology]] and [[neuroethology]]. Besides peer-reviewed research, the journal features additional material such as "Inside JEB" providing information about some of each issue's content,<ref>[http://jeb.biologists.org/current.shtml JEB - Table of Contents (June 15 2008, 211 [12&#93;)<!-- Bot generated title -->]</ref> "Outside JEB" discussing literature published in other journals,<ref>[http://jeb.biologists.org/content/vol211/issue7/#OUTSIDE_JEB JEB - Table of Contents (April 1 2008, 211 [7&#93;)<!-- Bot generated title -->]</ref> "JEB Classics" revisiting key papers published in the journal's past,<ref>[http://jeb.biologists.org/content/vol211/issue6/#JEB_CLASSICS JEB - Table of Contents (March 15 2008, 211 [6&#93;)<!-- Bot generated title -->]</ref> and "Commentaries" which review topics of current interest.<ref>[http://jeb.biologists.org/content/vol210/issue21/#COMMENTARY JEB - Table of Contents (November 1 2007, 210 [21&#93;)<!-- Bot generated title -->]</ref> Issues from 1923 are available online via the journal website and [[HighWire Press]] as [[Portable Document Format|PDF]]s, with text versions available from 2000 on. Content older than 6 months is available for free and all articles are available to readers in developing countries via the [[HINARI|Health InterNetwork Access to Research Initiative]].

In 2009, ''The Journal of Experimental Biology'' was included in the [[School Library Association]]'s top 100 journals in Biology and Medicine over the last 100 years.<ref>[http://units.sla.org/division/dbio/publications/resources/dbio100.html Top 100 Journals in Biology and Medicine] (accessed 10 February 2011)</ref>

== References ==
{{reflist|2}}

== External links ==
* {{Official website|http://jeb.biologists.org/}}
* [http://www.biologists.com/web/index.html The Company of Biologists website]

{{DEFAULTSORT:Journal Of Experimental Biology, The}}
[[Category:Biology journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1923]]
[[Category:The Company of Biologists academic journals]]