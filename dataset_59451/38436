{{good article}}
{{Infobox television episode
 |title = Book of the Stranger
 |image = Game-of-Thrones-S06-E04-Book-of-the-Stranger.jpg
 |image_size = 280px
 |caption = [[Daenerys Targaryen]] emerges from the flames after killing all of the khals. 
 |series = [[Game of Thrones]]
 |season = 6
 |episode = 4
 |director = [[Daniel Sackheim]]
 |writer = [[David Benioff]]<br>[[D. B. Weiss]]
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = Anette Haellmigk
 |editor = Katie Weiland
 |production = 
 |airdate = {{Start date|2016|5|15}}
 |length = 59 minutes
 |guests = 
* [[Diana Rigg]] as Olenna Tyrell
* [[Ben Crompton]] as Eddison Tollett
* [[Jacob Anderson]] as Grey Worm
* [[Natalia Tena]] as Osha
* [[Gemma Whelan]] as Yara Greyjoy
* [[Finn Jones]] as Loras Tyrell
* [[Julian Glover]] as Grand Maester Pycelle
* [[Ian Gelder]] as Kevan Lannister
* [[Rupert Vansittart]] as Yohn Royce
* [[Daniel Portman]] as Podrick Payne
* [[Hannah Waddingham]] as Septa Unella
* [[Lino Facioli]] as Robin Arryn
* [[Enzo Cilenti]] as Yezzan zo Qaggaz
* George Georgiou as Razdal mo Eraz
* [[Yousef (Joe) Sweid|Yousef Sweid]] as Ash
* Michael Heath as Kesh
* [[Joe Naufahu]] as Khal Moro
* [[Tamer Hassan]] as Khal Forzo
* [[Souad Faress]] as High Priestess of the Dosh Khaleen
* [[Hannah John-Kamen]] as Dothraki widow
* Chuku Modu as Aggo
* Staz Nair as Qhono
* Deon Lee-Williams as Iggo
* Elie Haddad as Khal Brozho
* Andrei Claude as Khal Rhalko
* Darius Dar Khan as Khal Quorro 
* Fola Evans-Akingbola as Khal Moro's wife
* Wuese Houston-Jibo as Dothraki widow
* Angelique Fernandez as Dothraki widow
* Diogo Sales as Dothraki bloodrider
* Junade Khan as Dothraki bloodrider
* Michael Hooley as Night's Watch member
* Robert Fawsitt as Night's Watch member
* Portia Victoria as a Dothraki woman
 |season_list =
 |prev = [[Oathbreaker (Game of Thrones)|Oathbreaker]]
 |next = [[The Door (Game of Thrones)|The Door]]
 |episode_list = [[Game of Thrones (season 6)|''Game of Thrones'' (season 6)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}
"'''Book of the Stranger'''" is the fourth episode of the [[Game of Thrones (season 6)|sixth season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 54th overall. The episode was written by series co-creators [[David Benioff]] and [[D. B. Weiss]], and directed by [[Daniel Sackheim]].

[[Sansa Stark]] ([[Sophie Turner (actress)|Sophie Turner]]) arrives at the Wall and reunites with [[Jon Snow (character)|Jon Snow]] ([[Kit Harington]]). They later receive a message from [[Ramsay Bolton]] ([[Iwan Rheon]]) challenging Jon to come take back [[Winterfell]] and rescue Rickon Stark. In King's Landing [[the High Sparrow]] ([[Jonathan Pryce]]) allows [[Margaery Tyrell]] ([[Natalie Dormer]]) to reunite with her brother, [[Loras Tyrell|Loras]] ([[Finn Jones]]) in the cells of the Great Sept, and [[Cersei Lannister]] ([[Lena Headey]]) and [[Jaime Lannister]] ([[Nikolaj Coster-Waldau]]) plot with [[Kevan Lannister]] ([[Ian Gelder]]) and [[Olenna Tyrell]] ([[Diana Rigg]]) to have them released. In Vaes Dothrak, [[Jorah Mormont]] ([[Iain Glen]]) and [[Daario Naharis]] ([[Michiel Huisman]]) reunite with [[Daenerys Targaryen]] ([[Emilia Clarke]]), who kills the khals in a large temple fire, from which she emerges unburnt.

"Book of the Stranger" received widespread acclaim from critics, who noted the reunion of Jon Snow and Sansa Stark, and Daenerys Targaryen taking charge of all the khalasars as high points of the episode, one calling them "huge, forward moving story elements that harkened back to season 1." Filming of the episode's closing scene was shot at two different locations. In the United States, the episode [[Nielsen rating|achieved a viewership]] of 7.82 million in its initial broadcast. The episode was Emilia Clarke's selection for the [[68th Primetime Emmy Awards]] to support her nominations.

==Plot==
===At the Wall===

[[Jon Snow (character)|Jon Snow]] ([[Kit Harington]]), having just resigned from the Night's Watch, is disillusioned by the betrayal of his fellow Night's Watchmen and tired of all of the endless fighting. Jon states his intent to head south, but Edd is unhappy with Jon's decision after seeing what had happened at Hardhome. They are interrupted by the lookout's horn as [[Sansa Stark]] ([[Sophie Turner (actress)|Sophie Turner]]), [[Brienne of Tarth]] ([[Gwendoline Christie]]), and [[Podrick Payne]] ([[Daniel Portman]]) arrive at Castle Black and Sansa is reunited with Jon. After telling each other their stories, Sansa tries to convince Jon to help her retake Winterfell. However, Jon is still reluctant to fight. Frustrated, Sansa declares to Jon that she will take back Winterfell whether he helps her or not.

Meanwhile, Brienne confronts [[Davos Seaworth]] ([[Liam Cunningham]]) and [[Melisandre]] ([[Carice van Houten]]), and informs them that she killed [[Stannis Baratheon|Stannis]] after he admitted to using dark magic to assassinate Renly. She warns Davos and Melisandre that even though that was in the past, she does not forget or forgive.

Some time later, a letter from Ramsay to Jon arrives. Ramsay boasts that he has Rickon in his custody. He demands Sansa's return, threatening to have the Bolton army exterminate the wildlings, kill Rickon and gang-rape Sansa while forcing Jon to watch before they kill him. Angered, Sansa decides to fight to take back Winterfell from the Boltons, and Jon agrees. When Tormund ([[Kristofer Hivju]]) warns him that the wildlings don't have the numbers to battle Ramsay's army, Sansa points out that Jon can use his status as the son of the "[[Eddard Stark|last true Warden of the North]]" to unite the various Northern houses and take Ramsay down.

===At Runestone===
[[Petyr Baelish|Littlefinger]] ([[Aidan Gillen]]) arrives at Runestone in the Vale, with a pet falcon for his stepson, Lord of the Eyrie [[List of A Song of Ice and Fire characters#Robert Arryn|Robin Arryn]] ([[Lino Facioli]]). When Lord of Runestone [[Yohn Royce]] ([[Rupert Vansittart]]) asks how Sansa became married to Ramsay Bolton instead of at the Fingers, Littlefinger claims Bolton's men attacked them and kidnapped her on the way. When Royce is dubious, Littlefinger adds that he was the only person who knew of their destination, implying his guilt. He manipulates Arryn into considering executing Royce, then into giving him a second chance after he pledges his absolute loyalty. Littlefinger then tells Arryn that Sansa, his cousin, has escaped the Boltons and is taking refuge at Castle Black, but that she is still not safe. Arryn agrees to command Royce to lead the knights of the Vale to protect her.

===In Meereen===

Despite [[Grey Worm]] ([[Jacob Anderson]]) and [[Missandei]]'s ([[Nathalie Emmanuel]]) objections, [[Tyrion Lannister]] ([[Peter Dinklage]]) arranges a diplomatic meeting with the masters of Astapor, Yunkai and Volantis. He proposes a deal allowing the cities seven years to transition away from slavery, while compensating the masters for any losses. In return, the masters will cease their support of the Sons of the Harpy. As the masters deliberate, Tyrion is confronted by the former slaves of Meereen, who oppose any kind of negotiation with the masters. Grey Worm and Missandei reluctantly support Tyrion, but in private they warn him that the masters, whom they are both familiar with, will use him if he tries to use them.

===In King's Landing===

[[Margaery Tyrell]] ([[Natalie Dormer]]) is brought to meet [[the High Sparrow]] ([[Jonathan Pryce]]), who warns her to stay away from her life of riches and sin, and recounts his past of how, as a proud cobbler, he learned his wealth-guided pursuits were lies and that the shoeless poor were closer to the truth than anyone. He then takes Margaery to see Loras ([[Finn Jones]]), who is breaking under the Sparrows' torture, and is willing to do anything to make it stop. Margaery realises that the High Sparrow is trying to use Loras to break her, and tells him to remain strong.

[[Cersei Lannister]] ([[Lena Headey]]) meets with [[Tommen Baratheon]] ([[Dean-Charles Chapman]]), who brings up the High Sparrow. Tommen is reluctant to provoke him, but Cersei says he is dangerous because he has no respect for the Crown. Tommen tells Cersei that Margaery's walk of atonement will happen soon. Cersei relays the information to [[Kevan Lannister]] ([[Ian Gelder]]) and [[Olenna Tyrell]] ([[Diana Rigg]]) in the small council chamber. Tyrell is horrified and pledges her army to defeat the Sparrows. Kevan has been ordered not to attack by Tommen, but agrees to stand down his army after Cersei says that by destroying the Sparrows he can have back his son Lancel, who is part of the order, and that Tommen will forgive him when he has Margaery back.

===At Pyke===
[[Theon Greyjoy]] ([[Alfie Allen]]) returns to the Iron Islands and reunites with his sister, [[Yara Greyjoy|Yara]] ([[Gemma Whelan]]). Yara has not forgiven Theon for not coming with her after her costly [[The Laws of Gods and Men|assault on the Dreadfort]], and accuses Theon of returning to the Iron Islands to take advantage of their father Balon's death to seize the throne. Theon insists he only heard the news after landing and promises that he will instead support Yara's claim at the Kingsmoot.

===In Winterfell===

[[Osha (character)|Osha]] ([[Natalia Tena]]) is brought before [[Ramsay Bolton]] ([[Iwan Rheon]]), who asks her why she was helping Rickon. Osha claims that she intended to betray Rickon and attempts to seduce Ramsay while reaching for a nearby knife. However, Ramsay tells her that he is aware Osha used a similar ruse to escape Theon. Realizing this, she tries to stab him, but he quickly stabs her in the neck with another knife, killing her.

===In Vaes Dothrak===

[[Jorah Mormont]] ([[Iain Glen]]) and [[Daario Naharis]] ([[Michiel Huisman]]) arrive at Vaes Dothrak and hide their weapons, as they are forbidden in the city. In the process, Jorah accidentally reveals his greyscale infection to Daario. They infiltrate the city and are forced to kill two of Khal Moro's bloodriders when discovered.

At the Temple of the Dosh Khaleen, [[Daenerys Targaryen]] ([[Emilia Clarke]]) befriends one of the younger Dosh Khaleen ([[Hannah John-Kamen]]), and when they go outside, they encounter Jorah and Daario. The two men want to try to sneak Daenerys out of Vaes Dothrak, but Daenerys knows that is impossible due to the estimated 100,000 Dothraki present in the city. Instead, she tells them she has a different plan.

Later that night, Daenerys stands before the gathered khals in the temple to hear her fate. There, she recalls [[A Golden Crown#In Vaes Dothrak|her pregnancy ritual]] in this temple and [[You Win or You Die#In Vaes Dothrak|her husband's vow]] to conquer Westeros. She accuses the khals of being unsuited to leading the Dothraki due to their lack of ambition, and says she will lead them. When Khal Moro and the other khals threaten to gang-rape her instead of serving her, she tells them they will die instead. She tips two braziers onto the straw floor, quickly setting the entire temple on fire and killing the khals, who have been barred inside by the younger Dosh Khaleen. A Dothraki crowd of thousands witnesses Daenerys emerge from the burning temple, naked but unburned. Amazed, they, Jorah and Daario bow down to her.

==Production==
===Writing===
[[File:D. B. Weiss and David Benioff.jpg|left|thumb|Series co-creators [[David Benioff]] and [[D. B. Weiss]] wrote the episode.]]
"Book of the Stranger" was written by the series' creators [[David Benioff]] and [[D. B. Weiss]]. Some material in this episode is taken from the Jon XIII chapter in ''[[A Dance With Dragons]]''. Some elements in the episode are also based on the sixth novel in the ''[[A Song of Ice and Fire]]'' series, ''[[The Winds of Winter]]'', which author [[George R. R. Martin]] had hoped to have completed before the sixth season began airing.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.slate.com/blogs/browbeat/2016/01/02/george_rr_martin_says_winds_of_winter_won_t_be_out_before_game_of_thrones.html |archivedate=August 17, 2016 |url=http://www.slate.com/blogs/browbeat/2016/01/02/george_rr_martin_says_winds_of_winter_won_t_be_out_before_game_of_thrones.html |title=George R.R. Martin’s Winds of Winter Won’t Be Out Before Game of Thrones’ Sixth Season |author=Shetty, Sharan |work=[[Slate (magazine)|Slate]] |accessdate=April 7, 2016 |date=January 2, 2016}}</ref>

===Filming===
[[File:Emilia Clarke 2013 (Straighten Colors 2).jpg|right|thumb|upright|Actress [[Emilia Clarke]] portrays Daenerys Targaryen in the series.]]
"Book of the Stranger" was directed by [[Daniel Sackheim]]. He joined the series as a director in the sixth season. He also directed the previous episode, "[[Oathbreaker (Game of Thrones)|Oathbreaker]]".<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ew.com/article/2015/06/23/game-thrones-season-6-directors |archivedate=August 17, 2016 |url=http://www.ew.com/article/2015/06/23/game-thrones-season-6-directors|title=Game of Thrones directors revealed for mysterious season 6|last=Hibberd|first=James|date=June 25, 2015|work=[[Entertainment Weekly]]|access-date=June 26, 2015}}</ref> In an interview Sackheim commented on the Jon and Sansa reunion stating "Sometimes as a director, you're just looking at what's in front of you, and not taking into account the bigger picture and the epic nature of two siblings who have been separated for six seasons — and have never had scenes together, and were both really looking forward to it — reuniting. The only note I gave them during the scene was, "Hold yourself back. As much as it's joyous to see each other, you're equally as scared. You don't know what to expect." The operative word was fear. Fear of the unknown. In a way, it added to the emotional resonance of the scene."<ref name="thrsackheim">{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.hollywoodreporter.com/live-feed/game-thrones-jon-snow-daenerys-895034|archivedate=August 17, 2016 |url=http://www.hollywoodreporter.com/live-feed/game-thrones-jon-snow-daenerys-895034 |title='Game of Thrones' Director on Giving New Life to Jon Snow and Daenerys Targaryen |work=[[The Hollywood Reporter]] |accessdate=May 18, 2016 |date=May 17, 2016 |author=Wigler, Josh}}</ref>

For the final scene with Daenerys Targaryen emerging from the great fire of the Temple of the Dosh Khaleen, the filming took place in two different locations, with the close ups of Emilia Clarke taking place on a closed set in [[Belfast]], and the large-scale set shots taking place in Spain.<ref name="ew">{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ew.com/article/2016/05/15/game-thrones-emilia-clarke-dany |archivedate=August 17, 2016 |url=http://www.ew.com/article/2016/05/15/game-thrones-emilia-clarke-dany |title=Game of Thrones: Emilia Clarke on that epic nude scene |work=[[Entertainment Weekly]] |author=Hibberd, James |accessdate=May 16, 2016 |date=May 15, 2016}}</ref> In an interview, Clarke had previously indicated she had become reluctant to do nude scenes unless it served the plot.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ew.com/article/2015/09/16/emilia-clarke-sex-scenes |archivedate=August 17, 2016 |url=http://www.ew.com/article/2015/09/16/emilia-clarke-sex-scenes |title=Emilia Clarke clarifies sex scenes position, denies report |work=[[Entertainment Weekly]] |accessdate=May 16, 2016 |date=September 16, 2015 |author=Hibberd, James}}</ref> After the episode aired, Clarke made a point to indicate that it was not a body double in the final scene of the episode,<ref name="ew"/> stating, "I'd like to remind people the last time I took my clothes off was season 3. That was awhile ago. It's now season 6. But this is all me, all proud, all strong. I'm just feeling genuinely happy I said 'Yes.' That ain't no body double!"<ref name="ew"/> She continued, "Taking off my clothes is not the easiest thing, but with the magic of the effects, I don't have to do a season 1 and go on a cliff and do it, I'm in control of it."<ref name="ew"/>

Series co-creator and executive producer Weiss praised Clarke's portrayal in the scene saying "Emilia absolutely crushed it. It's one of those weird scenes because it was half shot in Spain, half in Belfast. But largely due to her performance, it works brilliantly."<ref name="ew"/> Sackheim, the director of the episode, noted in an interview, "With the interior, there was only one way for her to play it, which is, bemused. She's the keeper of the secret. She knows how to extricate herself from this. I thought the ease with which she delivered the lines was necessary for the audience to feel jeopardy for her and for them to think she was crazy. The sequence outside was all about claiming the throne — or reclaiming the throne."<ref name="thrsackheim"/> Sackheim also stated, "We wanted to clearly distinguish everything we've seen from the end of the last season and the beginning of this one."<ref name="thrsackheim"/>

==Reception==
=== Ratings ===
"Book of the Stranger" was viewed by 7.82 million American households on its initial viewing on [[HBO]], which was slightly more than the previous week's rating of 7.28 million viewers for the episode "Oathbreaker".<ref name="ratings">{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://tvbythenumbers.zap2it.com/2016/05/17/sunday-cable-ratings-game-of-thrones-rises-fear-the-walking-dead-falls/ |archivedate=August 17, 2016 |url=http://tvbythenumbers.zap2it.com/2016/05/17/sunday-cable-ratings-game-of-thrones-rises-fear-the-walking-dead-falls/|title=Sunday cable ratings: ‘Game of Thrones’ rises, ‘Fear the Walking Dead’ falls|publisher=[[TV by the Numbers]]|last=Porter|first=Rick|date=May 17, 2016|accessdate=May 17, 2016}}</ref> The episode also acquired a 3.9 rating in the 18–49 demographic, making it the highest rated show on cable television of the night.<ref name="ratings"/> In the United Kingdom, the episode was viewed by 2.775 million viewers on [[Sky Atlantic]]; it also received 0.116 million timeshift viewers.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (9-15 May 2016)|publisher=[[Broadcasters' Audience Research Board|BARB]]|accessdate=May 25, 2016}}</ref>

===Critical reception===
"Book of the Stranger" received universal praise from critics, with many citing the reunion of Jon Snow and Sansa Stark, the final scene involving Daenerys Targaryen killing the leaders of the khalasar, and the forward moving storytelling as strong points for the episode. The review aggregator [[Rotten Tomatoes]] surveyed 50 reviews of the episode and judged 100% of them to be positive. The episode also achieved an average rating of 8.9/10, by far the highest of the season at this point. The website's critical consensus reads, ""Book of the Stranger"'s warm reunions, new alliances, and exquisitely fiery finale is ''Game of Thrones'' at its best."<ref>{{cite web |url=http://www.rottentomatoes.com/tv/game-of-thrones/s06/e04/ |title=Book of the Stranger - Game of Thrones: Season 6, Episode 4 |publisher=[[Rotten Tomatoes]] |accessdate=May 15, 2016 |date=May 15, 2016}}</ref>

In a review for [[IGN]], Matt Fowler wrote of the episode, ""Book of the Stranger" handed us two very lovely, satisfying moments with the Stark/Snow reunion at Castle Black (and the subsequent vow to defeat Ramsay and rescue Rickon) and Daenerys's conquering of Vaes Dothrak. Both were huge, forward-moving story elements that harkened back to Season 1 and gave viewers something to root for and grab onto as the show itself heads into its final arcs."<ref name="ign">{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ign.com/articles/2016/05/16/game-of-thrones-book-of-the-stranger-review |archivedate=August 17, 2016 |url=http://www.ign.com/articles/2016/05/16/game-of-thrones-book-of-the-stranger-review |title=Game of Thrones "Book of the Stranger" Review |publisher=[[IGN]] |author=Fowler, Matt |accessdate=May 15, 2016 |date=May 15, 2016}}</ref> Fowler also noted, "As a reader of the books with no more books to read, Season 6 has been a very interesting experience," giving the episode a 9.2 out of 10.<ref name="ign"/> Jeremy Egner of ''[[The New York Times]]'' also praised the scenes at Castle Black and in Vaes Dothrak, writing "''Game of Thrones'' lived up to its billing as ''A Song of Ice and Fire'' on Sunday, as there was plenty of action in both of the signature halves of the story."<ref>{{cite web |url=https://www.nytimes.com/2016/05/15/arts/television/game-of-thrones-season-6-episode-4-recap.html?rref=collection%2Fspotlightcollection%2Ftv-recaps |title=‘Game of Thrones’ Season 6, Episode 4: An Hour of Ice and Fire |work=[[The New York Times]] |author=Egner, Jeremy |accessdate=May 15, 2016 |date=May 15, 2016}}</ref> Brandon Nowalk of ''[[The A.V. Club]]'' wrote, "Now that is how you set the table. "Book Of The Stranger" doesn't just check off plot points. In fact, there aren't a lot of plot points to check off. It's an episode of introductions, reunions, and wall-to-wall scheming," giving the episode an A.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.avclub.com/tvclub/strong-women-rule-game-thrones-newbies-236796 |archivedate=August 17, 2016 |url=http://www.avclub.com/tvclub/strong-women-rule-game-thrones-newbies-236796 |title=Strong women rule on Game Of Thrones (newbies) |publisher=[[The A.V. Club]] |accessdate=May 15, 2016 |date=May 15, 2016 |author=Nowalk, Brandon}}</ref> Eliana Dockterman of ''[[Time (magazine)|Time]]'' wrote about the strong female storylines in the episode, stating "The creators of ''Game of Thrones'' have been touting the sixth season of the show as the year when women finally wreak vengeance. The fourth episode, "Book of the Stranger," suggests that they will hold true to their word."<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://time.com/4336622/game-of-thrones-women-sansa-cersei-yara-daenerys/ |archivedate=August 17, 2016 |url=http://time.com/4336622/game-of-thrones-women-sansa-cersei-yara-daenerys/ |title=Game of Thrones’ Women Are Finally Taking Over |work=[[Time (magazine)|Time]] |accessdate=May 15, 2016 |date=May 15, 2016 |author=Dockterman, Eliana}}</ref>

===Accolades===

{| class="wikitable sortable plainrowheaders"
|-
! Year
! Award
! Category
! Nominee(s)
! Result
! class="unsortable" | {{Abbreviation|Ref.|Reference}}
|-
| rowspan=2|2016
|scope="row"| [[68th Primetime Emmy Awards]]
|scope="row"| [[Primetime Emmy Award for Outstanding Supporting Actress in a Drama Series|Outstanding Supporting Actress in a Drama Series]]
|scope="row"| [[Emilia Clarke]] as [[Daenerys Targaryen]]
| {{nom}}
| <ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://variety.com/2016/tv/awards/game-of-thrones-2016-emmys-nominations-1201814023/ |archivedate=August 17, 2016 |url=http://variety.com/2016/tv/awards/game-of-thrones-2016-emmys-nominations-1201814023/ |title=‘Game of Thrones’ Rules 2016 Emmy Race With 23 Nominations |work=[[Variety (magazine)|Variety]] |first=Laura |last=Prudom |date=July 14, 2016 |accessdate=July 14, 2016}}</ref>
|-
|scope="row"| [[American Society of Cinematographers]]
|scope="row"| Outstanding Achievement in Cinematography in Regular Series
|scope="row"| Anette Haellmigk
| {{nom}}
|<ref>{{cite web|archiveurl= https://web.archive.org/web/20170205114244/http://www.hollywoodreporter.com/lists/lion-tops-asc-cinematographer-awards-966477 |archivedate=February 5, 2016|url=http://www.hollywoodreporter.com/lists/lion-tops-asc-cinematographer-awards-966477|title='Lion' Tops ASC Cinematographer Awards|first=Carolyn|last=Giardina|work=[[The Hollywood Reporter]]|date=February 4, 2017|accessdate=February 5, 2017|deadurl=no}}</ref>
|-
|}

==References==
{{reflist|30em}}

==External links==
{{wikiquotepar|Game_of_Thrones_(TV_series)#Book of the Stranger_.5B6.4.5D|Book of the Stranger}}
* {{URL|1=http://www.hbo.com/game-of-thrones/episodes/6/54-book-of-the-stranger/index.html|2="Book of the Stranger"}} at [[HBO.com]]
* {{IMDb episode|4283016}}
* {{tv.com episode|game-of-thrones/book-of-the-stranger-3381048/|Book of the Stranger}}

{{Game of Thrones Episodes}}

[[Category:2016 American television episodes]]
[[Category:Game of Thrones episodes]]