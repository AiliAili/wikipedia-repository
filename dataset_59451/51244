{{about|the conference|information on the encryption method|EuroCrypt}}
'''Eurocrypt''' (or '''EUROCRYPT''') is a conference for [[cryptography]] research.  The full name of the conference is now the '''Annual International Conference on the Theory and Applications of Cryptographic Techniques'''.  Eurocrypt is one of the [[International Association for Cryptologic Research|IACR]] flagship conferences, along with [[CRYPTO (conference)|CRYPTO]] and [[ASIACRYPT]].

Eurocrypt is held annually in the spring in various locations throughout Europe.  The first workshop in the series of conferences that became known as Eurocrypt was held in 1982.  In 1984, the name "Eurocrypt" was first used.  Generally, there have been published proceedings including all papers at the conference every year, with two exceptions; in 1983, no proceedings was produced, and in 1986, the proceedings contained only abstracts. [[Springer Science+Business Media|Springer]] has published all the official proceedings, first as part of Advances in Cryptology in the [[Lecture Notes in Computer Science]] series.

== Conference information by year ==
* 1982: March 29 - April 2, [[Feuerstein Castle|Burg Feuerstein]], [[Germany]], [[Thomas Beth]], ed.
* 1983: March 21–25, [[Udine]], [[Italy]].  (no proceedings published)
* 1984: April 9–11, [[Paris]], [[France]].  [[Thomas Beth]], [[Norbert Cot]], and [[Ingemar Ingemarsson]], eds. (ISBN 3-540-16076-0)
* 1985: April, [[Linz]], [[Austria]].  [[Franz Pichler]], ed.  (ISBN 3-540-16468-5)
* 1986: May 20–22, [[Linköping]], [[Sweden]], [[Ingemar Ingemarsson]], ed.
* 1987: April 13–15, [[Amsterdam]], [[Netherlands]], [[David Chaum]] and [[Wyn L. Price]], eds.  (ISBN 3-540-19102-X)
* 1988: May 25–27, [[Davos]], [[Switzerland]], [[C. G. Günther]], ed.  (ISBN 3-540-50251-3)
* 1989: April 10–13, [[Houthalen]], [[Belgium]], [[Jean-Jacques Quisquater]] and [[Joos Vandewalle]], eds.  (ISBN 3-540-53433-4)
* 1990: May 21–24, [[Aarhus]], [[Denmark]], [[Ivan Damgård]], ed.  (ISBN 3-540-53587-X)
* 1991: April 8–11, [[Brighton]], [[United Kingdom]], [[Donald W. Davies]], ed.  (ISBN 3-540-54620-0)
* 1992: May 24–28, [[Balatonfüred]], [[Hungary]], [[Rainer A. Rueppel]], ed.  (ISBN 3-540-56413-6)
* 1993: May 23–27, [[Lofthus, Oslo|Lofthus]], [[Norway]], [[Tor Helleseth]], ed.  (ISBN 3-540-57600-2)
* 1994: May 9–12, [[Perugia]], [[Italy]], [[Alfredo De Santis]], ed.  (ISBN 3-540-60176-7)
* 1995: May 21–25, [[Saint-Malo]], France, [[Louis Guillou|Louis C. Guillou]] and [[Jean-Jacques Quisquater]], eds. (ISBN 3-540-59409-4)
* 1996: May 12–16, [[Zaragoza]], [[Spain]], [[Ueli Maurer (cryptographer)|Ueli Maurer]], ed. (ISBN 3-540-61186-X)
* 1997: May 11–15, [[Konstanz]], Germany, [[Walter Fumy]], ed. (ISBN 3-540-62975-0)
* 1998: May 31 - June 4, [[Espoo]], [[Finland]], [[Kaisa Nyberg]], ed. (ISBN 3-540-64518-7)
* 1999: May 2–6, [[Prague]], [[Czech Republic]], [[Jacques Stern]], ed.  (ISBN 3-540-65889-0)
* 2000: May 14–18, [[Bruges]], [[Belgium]], [[Bart Preneel]], ed.  (ISBN 3-540-67517-5)
* 2001: May 6–10, [[Innsbruck]], Austria, [[Birgit Pfitzmann]], ed.  (ISBN 3-540-42070-3)
* 2002: April 28 - May 2, Amsterdam, Netherlands, [[Lars Knudsen]], ed.  (ISBN 3-540-43553-0)
* 2003: May 4–8, [[Warsaw]], [[Poland]], [[Eli Biham]], ed.  (ISBN 3-540-14039-5)
* 2004: May 2–6, [[Interlaken]], Switzerland, [[Christian Cachin]] and [[Jan Camenisch]], eds.  (ISBN 3-540-21935-8)
* 2005: May 22–26, Aarhus, Denmark, [[Ronald Cramer]], ed.  (ISBN 3-540-25910-4)
* 2006: May 28 - June 1, [[Saint Petersburg]], [[Russia]], [[Serge Vaudenay]], ed. (ISBN 3-540-34546-9)
* 2007: May 20–24, [[Barcelona]], [[Spain]], [[Moni Naor]], ed. (ISBN 978-3-540-72539-8)
* 2008: April 14–17, [[Istanbul]], [[Turkey]], [[Nigel Smart (Cryptographer)|Nigel Smart]], ed. (ISBN 978-3-540-78966-6)
* 2009: April 26–30, [[Cologne]], Germany, [[Antoine Joux]], ed. (ISBN 978-3-642-01000-2)
* 2010: May 30 - June 3, [[Nice]], France. [[Henri Gilbert]], ed. (ISBN 978-3-642-13189-9)
* 2011: May 15–19, [[Tallinn]], Estonia. [[Kenneth G. Paterson]], ed. (ISBN 978-3-642-20464-7)
* 2012: April 15–19, [[Cambridge]], UK, [[David Pointcheval]] and Thomas Johansson, eds. (ISBN 978-3-642-29010-7)

== See also ==
* [[CRYPTO (conference)|CRYPTO]]
* [[ASIACRYPT]]
* [[INDOCRYPT]]
* [[List of cryptology conferences]]
* [[International Association for Cryptologic Research]] (IACR)

== External links ==
* [https://www.iacr.org/conferences/ IACR conferences page; contains links to Eurocrypt home pages from 1995]
* [http://www.informatik.uni-trier.de/~ley/db/conf/eurocrypt/index.html Bibliography data for Eurocrypt proceedings]
* [https://web.archive.org/web/20060221114104/http://dsns.csie.nctu.edu.tw:80/research/crypto/ Conference proceedings online, 1982-1997]

[[Category:Cryptography conferences]]