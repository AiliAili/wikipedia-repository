{{Italic title}}
{{Infobox journal
| title         = Iranica Antiqua
| cover         = [[File:Iranica Antiqua cover.jpeg]]
| editor        = E. Haerinck, B. Overlaet
| discipline    = Archaeology,  Oriental Studies, Iranian Studies
| former_names  = 
| abbreviation  = 
| publisher     = Peeters Publishers, Leuven| country       = Belgium
| frequency     = Annual
| history       = 1961-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://poj.peeters-leuven.be/content.php?url=journal&journal_code=IA
| ISSN          = 0021-0870
| eISSN         = 1783-1482
}}
'''''Iranica Antiqua''''' is a [[Peer review|scholarly]] journal publishing papers on ancient Iran in its broadest sense. The journal was established by Iranist [[Roman Ghirshman]] and Louis Vanden Berghe in 1961. The journal is edited by Prof. Em. Dr Ernie HAERINCK and Dr. Bruno OVERLAET , Belgium.  Articles are in French, English or German.

According to its official website, the journal publishes different articles such as "preliminary excavation reports, contributions on archaeological problems, studies on different aspects of history, institutions, religion, epigraphy, numismatics and history of art of ancient Iran, as well as on cultural exchanges and relations between Iran and its neighbours".<ref>{{cite web|title=Iranica Antiqua|url=http://poj.peeters-leuven.be/content.php?url=journal&journal_code=IA|author=Peeters online journals}}</ref>

Iranica Antiqua is abstracted and indexed in the Arts & Humanities Citation Index and Current Contents/Arts & Humanities; Bibliographie linguistique/Linguistic Bibliography; Index Islamicus; Scopus; INIST/CNRS; CrossRef; Thomson Scientific Links.


Supplement series : The aim of this series is to provide an opportunity for publication of more substantial works of monograph length and collective works on particular themes.


==See also==
*[[Encyclopaedia Iranica]]
*[[Iranian studies]]

== References ==
{{reflist}}

== External links ==
{{Official|1=http://poj.peeters-leuven.be/content.php?url=journal&journal_code=IA}}

[[Category:Iranian studies journals]]
[[Category:Annual journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1961]]


{{area-journal-stub}}