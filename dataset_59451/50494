{{unreferenced|date=July 2015}}
{{Infobox journal
| cover =
| discipline = [[Chemistry]]
| language = Spanish
| abbreviation = An. Quím.
| website =
| publisher = [[Spanish Royal Society of Chemistry|Real Sociedad Española de Química]]
| country = Spain
| history = 1903–1998
| ISSN = 1130-2283
| OCLC = 802697545
| CODEN = ANQUEX
}}
The '''''Anales de Química''''' was a [[Peer review|peer-review]] [[scientific journal]] in the field of [[chemistry]]. The first issue was published in 1903 by the ''Real Sociedad Española de Física y Química'' (later the [[Spanish Royal Society of Chemistry|Real Sociedad Española de Química]], the Spanish Royal Society of Chemistry). Its publication ended in 1998.

==History==
The ''Anales des Quimica'' were published in the following order:

* ''Anales de la Real Sociedad Española de Física y Química'', 1903 (vol. 1) to 1940 (vol. 36; {{ISSN|0365-6675}}, [[CODEN]]: ASEFAR)
* ''Anales de Física y Química'', 1941 (vol 37) to 1947 (vol. 43; {{ISSN|0365-2351}}, CODEN: AFQMAH)

In 1948 (vol. 44), the journal was split into two sections:
* ''Anales de la Real Sociedad Española de Física y Química/Ser. A, Física'', 1948 (vol. 44) to 1967 (vol. 63; {{ISSN|0034-0871}}, CODEN: ARSFAM)
* ''Anales de la Real Sociedad Española de Física y Química/Ser. B, Química'', 1948 (vol. 44) to 1967 (vol. 63; {{ISSN|0034-088X}}, CODEN ARSQAL)

Section A subsequently became the ''[[Anales de Física]]''. From 1968, section B was continued as the ''Anales de Quimica'' (1968, vol 64, to 1979, vol. 75; {{ISSN|0365-4990}}, CODEN: ANQUBU).

From 1980 (vol. 76) to 1989 (vol. 85) this title was again split in three sections:

* ''Anales de Química/Serie A, Química Física y Ingenieria Química'' ({{ISSN|0211-1330}}, CODEN: AQSTDQ)
* ''Anales de Química/Serie B, Química Inorgánica y Química Analítica'' ({{ISSN|0211-1349}}, CODEN: AQSAD3)
* ''Anales de Química/Serie C, Química Orgánica y Bioquímica'' ({{ISSN|0211-1357}}, CODEN: AQSBD6)

From 1990 (vol. 86) until 1995 (vol. 91), sections A to C were merged again, returning ''Anales de Química'' (1990, vol 86, to 1995, vol. 91; {{ISSN|1130-2283}}, CODEN: ANQUEX). Finally, the journal was renamed to ''Anales de Química, International Edition'' (1996, vol. 92, to 1998, vol. 94; ({{ISSN|1130-2283}}, CODEN: AQIEFZ) until it was merged in 1998 to form the ''[[European Journal of Organic Chemistry]]'' and the ''[[European Journal of Inorganic Chemistry]]''.

To continue the tradition of ''Anales de Química'', the Spanish Royal Society of Chemistry established a new journal in 1999, the ''[[Anales de la Real Sociedad Española de Química]]''.

== See also ==

* [[Chemische Berichte]]
* [[Bulletin des Sociétés Chimiques Belges]]
* [[Bulletin de la Société Chimique de France]]
* [[European Journal of Inorganic Chemistry]]
* [[European Journal of Organic Chemistry]]
* [[Gazzetta Chimica Italiana]]
* [[Liebigs Annalen]]
* [[Recueil des Travaux Chimiques des Pays-Bas]]
* Chimika Chronika
* Revista Portuguesa de Química
* ACH—Models in Chemistry

== External links ==
* [http://www.rseq.org/ Spanish Royal Society of Chemistry]

{{DEFAULTSORT:Anales de Quimica}}
[[Category:1903 establishments in Spain]]
[[Category:1998 disestablishments in Spain]]
[[Category:Chemistry journals]]
[[Category:Publications established in 1903]]
[[Category:Spanish-language journals]]
[[Category:Publications disestablished in 1998]]
[[Category:Defunct journals]]
[[Category:Academic journals published by learned and professional societies]]