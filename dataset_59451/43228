<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = FH Phantom
  |image =FH-1 CVB-42 landing NAN9 46.jpg
  |caption = McDonnell FH-1 Phantom
}}{{Infobox Aircraft Type
  |type = [[Naval aviation|Carrier-based]] [[fighter aircraft]]
  |manufacturer = [[McDonnell Aircraft]]
  |designer =
  |first flight=26 January 1945
  |introduced=August {{avyear|1947}}
  |retired={{avyear|1949}} USN, USMC<br/>July {{avyear|1954}}<ref name="Mills">Mills 1991, pp. 226-227.</ref> USNR
  |status =
  |primary user = [[United States Navy]]
  |more users = [[United States Marine Corps]]
  |produced =
  |number built = 62
  |unit cost =
  |variants with their own articles =
  |developed into = [[McDonnell F2H Banshee]]
}}
|}

The '''McDonnell FH Phantom''' was a twin-engined [[jet engine|jet]] [[fighter aircraft]] designed and first flown during [[World War II]] for the [[United States Navy]]. The Phantom was the first purely jet-powered aircraft to land on an American [[aircraft carrier]]<ref name="Angel p298"/>{{#tag:ref|The first aircraft to land on an American carrier under jet power was the unconventional composite propeller-jet [[Ryan Aeronautical Company|Ryan]] [[FR Fireball]], designed to utilize its [[Reciprocating engine|piston engine]] during takeoff and landing. On 6 November 1945, the piston engine of an FR-1 failed on final approach; the pilot started the jet engine and landed, thereby performing the first jet-powered carrier landing, albeit unintentionally.<ref>"First Jet Landing". ''Naval Aviation News'', United States Navy, March 1946, p. 6.</ref>|group=N}} and the first jet deployed by the [[United States Marine Corps]]. Although with the end of the war, only 62 FH-1s were built, it helped prove the viability of [[Carrier-based aircraft|carrier-based]] jet fighters. As McDonnell's first successful fighter, leading to the development of the follow-on [[McDonnell F2H Banshee|F2H Banshee]], which was one of the two most important naval jet fighters of the [[Korean War]], it would also establish McDonnell as an important supplier of Navy aircraft.<ref>[https://books.google.com/books?id=8jjd-pkdRR0C&pg=PA10 ''USN F-4 Phantom II vs VPAF MiG-17/19: Vietnam 1965–73'']. Osprey Publishing.</ref> When McDonnell chose to bring the name back with the Mach 2–class  [[McDonnell Douglas F-4 Phantom II]], it launched what would become the most versatile and widely used western combat aircraft of the Vietnam War era, adopted by the USAF and the US Navy.<ref>[http://www.penguinrandomhouse.com/books/221514/usaf-mcdonnell-douglas-f-4-phantom-ii-by-peter-davies/ USAF McDonnell Douglas F-4 Phantom II Penguin Random House Books]</ref>

The FH Phantom was originally designated the '''FD Phantom''', but the designation was changed as the aircraft entered production.

==Design and development==
In early 1943, aviation officials at the United States Navy were impressed with McDonnell's audacious [[XP-67 Bat]] project. McDonnell was invited by the Navy to cooperate in the development of a shipboard jet fighter, using an engine from the turbojets under development by [[Westinghouse Aviation Gas Turbine Division|Westinghouse Electric Corporation]]. Three prototypes were ordered on 30 August 1943 and the designation XFD-1 was assigned. Under the [[1922 United States Navy aircraft designation system]], the letter "D" before the dash designated the aircraft's manufacturer. The [[Douglas Aircraft Company]] had previously been assigned this letter, but the USN elected to reassign it to McDonnell because Douglas had not provided any fighters for Navy service in years.<ref name="Mesko7">Mesko 2002, p. 7.</ref>

McDonnell engineers evaluated a number of engine combinations, varying from eight 9.5&nbsp;in (24&nbsp;cm) diameter engines down to two engines of 19&nbsp;inch (48&nbsp;cm) diameter. The final design used the two 19&nbsp;in (48&nbsp;cm) engines after it was found to be the lightest and simplest configuration.<ref name="AI Nov87 p233">''Air International'' November 1987, p. 233.</ref> The engines were buried in the wing root to keep intake and exhaust ducts short, offering greater aerodynamic efficiency than underwing [[nacelle]]s,<ref name="AI Nov87 p234">''Air International'' November 1987, p. 234.</ref> and the engines were angled slightly outwards to protect the [[fuselage]] from the hot exhaust blast.<ref name="Mesko7"/> Placement of the engines in the middle of the airframe allowed the [[cockpit]] with its bubble-style canopy to be placed ahead of the wing, granting the pilot excellent visibility in all directions. This engine location also freed up space under the nose, allowing designers to use [[tricycle gear]], thereby elevating the engine exhaust path and reducing the risk that the hot blast would damage the aircraft carrier deck.<ref name="Mesko p5">Mesko 2002, p. 5.</ref> The construction methods and aerodynamic design of the Phantom were fairly conventional for the time; the aircraft had unswept wings, a conventional [[empennage]], and an [[aluminum]] [[monocoque]] structure with [[flush rivet]]ed aluminum skin. [[Folding wing]]s were used to reduce the width of the aircraft in storage configuration. Provisions for four [[.50 BMG|.50-caliber (12.7 mm)]] [[machine gun]]s were made in the nose, while racks for eight 5 in (127&nbsp;mm) [[High Velocity Aircraft Rocket]]s could be fitted under the wings, although these were seldom used in service.<ref name="Mesko7"/> Adapting a jet to carrier use was a much greater challenge than producing a land-based fighter because of slower landing and takeoff speeds required on a small carrier deck. The Phantom used [[Flap (aircraft)|split flaps]] on both the folding and fixed wing sections to enhance low-speed landing performance,<ref name="AI Nov87 p234-5">''Air International'' November 1987, pp. 234–235.</ref> but no other [[high-lift device]]s were used. Provisions were also made for [[JATO|Rocket Assisted Take Off (RATO)]] bottles to improve takeoff performance.<ref name="Mesko7"/>

[[File:McDonnell FH-1 Phantom of VF-17A on USS Saipan (CVL-48) in May 1948.jpg|thumb|left|A U.S. Navy FH-1 of [[VF-171|VF-17A]] ''Phantom Fighters'' taxies to the catapult during carrier qualifications on the light aircraft carrier {{USS|Saipan|CVL-48|2}}, in May 1948]]

When the first XFD-1, [[United States military aircraft serials|serial number]] ''48235'', was completed in January 1945, only one Westinghouse 19XB-2B engine was available for installation. Ground runs and taxi tests were conducted with the single engine, and such was the confidence in the aircraft that the first flight on 26 January 1945 was made with only the one turbojet engine.<ref name="Franc mdd p382">Francillon 1979, p. 382.</ref>{{#tag:ref|McDonnell assistant Chief Engineer Kendall Perkins has stated that this "first flight" was no more than a "hop", and that the real first flight would wait until a second engine was fitted a few days later.<ref name="AI Nov 87 p258"/>|group=N}} During flight tests, the Phantom became the first naval aircraft to exceed 500&nbsp;mph (434 kn, 805 kph).<ref name="Mills"/> With successful completion of tests, a production contract was awarded on 7 March 1945 for 100 FD-1 aircraft. With the end of the war, the Phantom production contract was reduced to 30 aircraft, but was soon increased back to 60.<ref name="AI Nov 87 p258">''Air International'' November 1987, p. 258.</ref>

The first prototype was lost in a fatal crash on 1 November 1945,<ref name="Angel p297-8">Angelucci and Bowers 1987, pp. 297–298.</ref> but the second and final Phantom prototype ([[United States military aircraft serials|serial number]] ''48236'') was completed early the next year and became the first purely jet-powered aircraft to operate from an American [[aircraft carrier]], completing four successful takeoffs and landings on 21 July 1946, from {{USS|Franklin D. Roosevelt|CV-42|2}} near [[Norfolk, Virginia]].<ref name="Mills"/> At the time, she was the largest carrier serving with the U.S. Navy, allowing the aircraft to take off without assistance from a [[Aircraft catapult|catapult]].<ref name="AI Nov 87 p258"/> The second prototype crashed on 26 August 1946.<ref name="ASN236">{{ASN accident|id=82095}}{{dead link|date=May 2011}}</ref>

Production Phantoms incorporated a number of design improvements. These included provisions for a flush-fitting centerline [[drop tank]], an improved gunsight, and the addition of [[Air brake (aircraft)|speed brakes]]. Production models used [[Westinghouse J30]]-WE-20 engines with 1,600&nbsp;lbf (7.1&nbsp;kN) of thrust per engine. The top of the vertical tail had a more square shape than the rounder tail used on the prototypes, and a smaller [[rudder]] was used to resolve problems with [[Flight control surfaces|control surface]] clearance discovered during test flights. The horizontal tail surfaces were shortened slightly, while the fuselage was stretched by 19&nbsp;in (48&nbsp;cm). The amount of framing in the windshield was reduced to enhance pilot visibility.<ref name="Mesko7"/><ref name="AI Nov 87 p258"/>

Halfway through the production run, the Navy reassigned the designation letter "D" back to Douglas, with the Phantom being redesignated FH-1.<ref name="AI Nov 87 p258"/> Including the two prototypes, a total of 62 Phantoms were finally produced, with the last FH-1 rolling off the assembly line in May 1948.<ref>Wagner 1982, p. 503.</ref>

Realizing that the production of more powerful jet engines was imminent, McDonnell engineers proposed a more powerful variant of the Phantom while the original aircraft was still under development&nbsp;– a proposal that would lead to the design of the Phantom's replacement, the [[McDonnell F2H Banshee|F2H Banshee]]. Although the new aircraft was originally envisioned as a modified Phantom, the need for heavier armament, greater internal fuel capacity, and other improvements eventually led to a substantially heavier and bulkier aircraft that shared few parts with its agile predecessor.<ref name="Mesko10">Mesko 2002, p. 10.</ref> Despite this, the two aircraft were similar enough that McDonnell was able to complete its first '''F2H-1''' in August 1948, a mere three months after the last '''FH-1''' had rolled off the assembly line.<ref>Wagner 1982, p. 504.</ref>

==Operational history==
[[File:FH-1s NAN11-49.jpg|thumb|right|Three FH-1 Phantoms of VMF-122 in 1949]]
[[File:Minneapolis Navy Reserve FH F4U SNJ NAN10-51.jpg|thumb|right|Three aircraft of the Minneapolis U.S. Naval Air Reserve (front to back): an FH-1 Phantom, an F4U-1 Corsair, and an SNJ Texan in 1951.]]

The first Phantoms were delivered to USN fighter squadron [[VF-171|VF-17A]] (later redesignated [[VF-171]]) in August 1947;<ref name="AI Nov 87 p259">''Air International'' November 1987, p. 259.</ref> the squadron received a full complement of 24 aircraft on 29 May 1948.{{citation needed|date=May 2011}} Beginning in November 1947, Phantoms were delivered to [[United States Marine Corps]] squadron [[VMFA-122|VMF-122]], making it the first USMC combat squadron to deploy jets.<ref name="AI Nov 87 p259"/> VF-17A became the USN's first fully operational jet carrier squadron when it deployed aboard {{USS|Saipan|CVL-48|6}} on 5 May 1948.<ref name="Grossnick p171">Grossnick 1997, p. 171.</ref>{{#tag:ref|Squadron [[VF-51|VF-5A]], flying the [[North American FJ-1 Fury]], had conducted the Navy's first all-jet aircraft carrier operations at sea on 10 March 1948 aboard {{USS|Boxer|CV-21|2}}, but the entire squadron was not considered operational at the time.|group=N}}

The Phantom was one of the first jets used by the U.S. military for exhibition flying. Three Phantoms used by the [[Naval Air Test Center]] were used by a unique demonstration team called the [[Gray Angels]], whose members consisted entirely of naval aviators holding the rank of [[Rear Admiral]] ([[Daniel V. Gallery]], [[Apollo Soucek]] and [[Edgar A. Cruise]].)<ref name="AI Nov 87 p259"/><ref name="goebel">Goebel, Greg. [http://www.vectorsite.net/avbansh.html#m1 "The FH-1 Phantom."] {{webarchive |url=https://web.archive.org/web/20110514001132/http://www.vectorsite.net/avbansh.html#m1 |date=May 14, 2011 }} ''The McDonnell FH-1 Phantom & F2H Banshee'', 1 November 2010. Retrieved: 10 May 2011.</ref> The team's name was an obvious play on the name of the recently formed U.S. Navy [[Blue Angels]], who were still flying propeller-powered [[Grumman F8F Bearcat]]s at the time. The "Grays" flew in various [[air show]]s during the summer of 1947, but the team was abruptly disbanded after their poorly timed arrival at a September air show in [[Cleveland, Ohio]], nearly caused a head-on low-altitude collision with a large formation of other aircraft; their Phantoms were turned over to test squadron [[VX-3 (US Navy squadron)|VX-3]].<ref name="Mills"/> The VMF-122 Phantoms were later used for air show demonstrations until they were taken out of service in 1949, with the team being known alternately as the [[Marine Phantoms]] or the [[Flying Leathernecks (aerobatic team)|Flying Leathernecks]].<ref name="Mills"/><ref name="AI Nov 87 p259"/>

The Phantom's service as a frontline fighter would be short-lived. Its limited range and light armament&nbsp;– notably, its inability to carry [[bombs]]&nbsp;– made it best suited for duty as a [[point-defence]] [[interceptor aircraft]]. However, its speed and rate of climb were only slightly better than existing propeller-powered fighters and fell short of other contemporary jets, such as the [[Lockheed P-80 Shooting Star]], prompting concerns that the Phantom would be outmatched by future enemy jets it might soon face. Moreover, recent experience in World War II had demonstrated the value of naval fighters that could double as [[Ground attack aircraft|fighter-bomber]]s, a capability the Phantom lacked. Finally, the aircraft exhibited some design deficiencies&nbsp;– its navigational [[avionics]] were poor, it could not accommodate newly developed [[ejection seat]]s,<ref name="Mills"/> and the location of the machine guns in the upper nose caused pilots to be dazzled by [[muzzle flash]].<ref name="Mesko10"/>

The F2H Banshee and [[Grumman F9F Panther]], both of which began flight tests around the time of the Phantom's entry into service, better satisfied the Navy's desire for a versatile, long-range, high-performance jet. Consequently, the FH-1 saw little weapons training, and was primarily used for carrier qualifications to transition pilots from propeller-powered fighters to jets in preparation for flying the Panther or Banshee. In June 1949, VF-171 (VF-17A) re-equipped with the Banshee, and their Phantoms were turned over to [[VF-172]]; this squadron, along with the NATC, VX-3, and VMF-122, turned over their Phantoms to the [[United States Naval Reserve]] by late 1949 after receiving F2H-1 Banshees. The FH-1 would see training duty with the USNR until being replaced by the F9F Panther in July 1954; none ever saw combat,<ref name="Mills"/> having been retired from frontline service prior to the outbreak of the [[Korean War]].

===Civilian use===
In 1964, [[Progressive Aero, Incorporated]] of [[Fort Lauderdale, Florida]] purchased three surplus Phantoms, intending to use them to teach civilians how to fly jets. A pair were stripped of military equipment and restored to flying condition, but the venture was unsuccessful, and the aircraft were soon retired once again.<ref name="Mesko8">Mesko, 2002 p. 8.</ref>

==Variants==
;XFD-1
: Prototype aircraft powered by {{convert|1165|lbf|kN|abbr=on}} Westinghouse 19XB-2B engines (J-30). Two built.<ref name="Angel p298">Angelucci and Bowers 1987, p. 268.</ref>
;FH-1 (FD-1): Production version with {{convert|1600|lbf|kN|abbr=on}} [[Westinghouse J30|Westinghouse J30-WE-20]] engines (originally designated FD-1). 60 built.<ref name="Angel p298"/>

==Operators==
;{{USA}}
* [[United States Navy]]
** [[VX-3 (US Navy squadron)|VX-3]]
** [[VF-171|VF-171 (VF-17A)]]
** [[VF-172]]
** [[United States Navy Reserve|Naval Air Reserve]]
* [[United States Marine Corps]]
** [[VMFA-122|VMF-122]]
** [[VMA-311|VMF-311]]

==Aircraft on display==
[[File:FH1Phantom DC.JPG|thumb|FH-1 Phantom on display in Washington, D.C.]]
;FH-1
*111759 - [[National Air and Space Museum]] of the [[Smithsonian Institution]] in [[Washington, D.C.]], [[United States]].<ref>[http://airandspace.si.edu/collections/artifact.cfm?id=A19600130000 "FH-1 Phantom/111759."] ''NASM.'' Retrieved: 29 October 2012.</ref> This aircraft served with Marine Fighter Squadron 122 (VMF-122). It was retired in April 1954, with a total of 418 flight hours. The aircraft was transferred to the Smithsonian by the U.S. Navy in 1959.<ref name= "Hamilton">Hamilton, Hayden. "The McDonell FH-1 Phantom: the Forgotten Phantom". ''AAHS Journal,'' Vol. 55, No. 2, Summer 2010.</ref>
*111768 - [[Elmira Corning Regional Airport#Wings of Eagles|Wings of Eagles Discovery Center]] in [[Horseheads (village), New York|Horseheads, New York]].<ref>[http://www.wingsofeagles.com/?page_id=550 "FH-1 Phantom/111768."] ''Wings of Eagles Discovery Center.'' Retrieved: 29 October 2012.</ref> It has had a busy post-retirement life. Formerly a Progressive Aero aircraft c/n 456 (civil registration N4283A) it was placed on display at the [[Marine Corps Museum]]. The aircraft was later transferred to the [[St. Louis Aviation Museum]], and then the [[National Warplane Museum]] in [[Geneseo, New York]]. In 2006 the aircraft was moved its current location.<ref name= "Hamilton"/>  5 Aug 2016 aircraft is on display in H3 of [[Pima Air & Space Museum]], [[Tucson, Arizona]].
*111793 - [[National Museum of Naval Aviation]] at [[Naval Air Station Pensacola]], [[Florida]].<ref>[http://www.navalaviationmuseum.org/attractions/aircraft-exhibits/item/?item=fh-1_phantom "FH-1 Phantom/111793."] ''National Museum of Naval Aviation.'' Retrieved: 15 January 2015.</ref> This aircraft was accepted by the Navy on 28 February 1948. After flying for a brief time with Marine Fighter Squadron (VMF) 122, the first Marine jet squadron, at Marine Corps Air Station (MCAS) Cherry Point, North Carolina, it was stricken from the naval inventory in 1949. The museum acquired the aircraft from National Jets, Inc., of Fort Lauderdale, Florida, in 1983.<ref name= "Hamilton"/>

==Specifications (FH-1 Phantom)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
|ref=McDonnell Douglas Aircraft since 1920 <ref name="Franc mdd p383">Francillon 1979, p. 383.</ref> unless otherwise noted
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|crew=One
|span main=40 ft 9 in; 16 ft 3 in with [[folding wing|folded wings]]<ref name="AI Nov87 p234"/>
|span alt=12.42 m / 4.95 m
|length main=37 ft 3 in
|length alt=11.35 m
|height main=14 ft 2 in
|height alt=4.32 m
|area main=276 sq ft
|area alt=25.6 m<sup>2</sup>
|empty weight main=6,683 lb
|empty weight alt=3,031 kg
|loaded weight main=10,035 lb
|loaded weight alt=4,552 kg
|max takeoff weight main=12,035 lb
|max takeoff weight alt=5,459 kg
|more general='''Fuel capacity:''' 375 gal (1,420 l) internal, 670 gal (2,540 l) with external drop tank
|engine (jet)=[[Westinghouse J30]]-WE-20
|type of jet=[[turbojet]]s
|number of jets=2
|thrust main=1,600 lbf
|thrust alt=7.1 kN
|max speed main=417 knots
|max speed alt=479 mph, 771 km/h
|max speed more=at sea level
|cruise speed main=216 knots
|cruise speed alt=248 mph, 399 km/h
|ceiling main=41,100 ft
|ceiling alt=12,525 m
|range main=604 [[nautical mile|nmi]]
|range alt=695 mi, 1,120 km
|ferry range main=852 nmi
|ferry range alt=980 mi, 1,580 km
|ferry range more=with external drop tank
|climb rate main=4,230 ft/min
|climb rate alt=21.5 m/s
|loading main=36.4 lb/ft²
|loading alt=178 kg/m²
|thrust/weight=0.32
|guns=4 × [[.50 BMG|.50 in]] (12.7 mm) [[machine gun]]s
|rockets=8 × 5 in (127 mm) [[High Velocity Aircraft Rocket]]s<ref name="Mesko7"/>
}}

==See also==
{{aircontent|
|related=
* [[McDonnell F2H Banshee]]
|similar aircraft=
* [[de Havilland Vampire|de Havilland Sea Vampire]]
* [[Hawker Sea Hawk]]
* [[North American FJ-1 Fury]]
* [[Supermarine Attacker]]
* [[Vought F6U Pirate]]
|lists=
* [[List of fighter aircraft]]
* [[List of military aircraft of the United States]]
|see also=
}}

==References==

===Notes===
{{Reflist|group=N}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Angelucci, Enzo and [[Peter M. Bowers]]. ''The American Fighter''. Sparkford, Somerset, UK: Haynes Publishing Group, 1987. ISBN 0-85429-635-2.
* Francillon, René J. ''McDonnell Douglas Aircraft since 1920''. London: Putnam & Company, Ltd, 1979. ISBN 0-370-00050-1.
* Green, William. ''War Planes of the Second World War, Volume Four: Fighters''. London: MacDonald & Co. (Publishers) Ltd., 1961 (sixth impression 1969). ISBN 0-356-01448-7.
* Green, William and Gordon Swanborough. ''WW2 Aircraft Fact Files: US Navy and Marine Corps Fighters''. London: Macdonald and Jane's, 1976. ISBN 0-356-08222-9.
* Grossnick, Roy A. [http://webarchive.loc.gov/all/20110507140321/http%3A//www%2Ehistory%2Enavy%2Emil/avh%2D1910/PART06%2EPDF "Part 6: Postwar Years: 1946–1949"]. ''United States Naval Aviation 1910–1995''. Washington, D.C.: Naval Historical Center, 1997. ISBN 0-945274-34-3.
* Hamilton, Hayden. "The McDonnell FH-1 Phantom: the Forgotten Phantom". ''AAHS Journal'', Vol. 55, No. 2, Summer 2010.
* Mesko, Jim. ''FH Phantom/F2H Banshee in action''. Carrollton, Texas: Squadron/Signal Publications, Inc., 2002. ISBN 0-89747-444-9.
* Mills, Carl. ''Banshees in the Royal Canadian Navy''. Willowdale, Ontario, Canada: Banshee Publication, 1991. ISBN 0-9695200-0-X.
* "Mr Mac's First Phantom: The Story of the McDonnell FH-1". ''[[Air International]]'' Vol. 33, No. 5, November 1987, pp.&nbsp;231–235, 258–260. Bromley, UK: Fine Scroll. ISSN 0306-5634.
* Wagner, Ray. ''American Combat Planes''. New York: Doubleday, third edition, 1982. ISBN 0-385-13120-8.
{{refend}}

==External links==
{{Commons category|McDonnell FH Phantom}}
* [http://www.flightglobal.com/pdfarchive/view/1947/1947%20-%200524.html "Phantom Development"] a 1947 ''Flight'' article by [[John W. R. Taylor]]

{{McDonnell Douglas military aircraft}}
{{USN fighters}}
{{Authority control}}

{{DEFAULTSORT:Mcdonnell Fh Phantom}}
[[Category:Carrier-based aircraft]]
[[Category:McDonnell aircraft|F1H Phantom]]
[[Category:United States fighter aircraft 1940–1949|McDonnell F1H Phantom]]
[[Category:World War II jet aircraft of the United States]]
[[Category:Cruciform tail aircraft]]