{{Use mdy dates|date=October 2011}}
{{Infobox Mayor
| name = Kathryn J. Whitmire
| image =Kathy Whitmire.jpeg
| caption =Undated official photo as Mayor of Houston
| office = 57th [[List of mayors of Houston|Mayor of Houston]]
| term_start = 1982
| term_end = 1992
| predecessor = [[Jim McConn]]
| successor = [[Bob Lanier (politician)|Bob Lanier]]
|office2=Houston City Controller
|term_start2=1978
|term_end2=1982
|predecessor2=Leonel J. Castillo
|successor2=Lance Lalor
| birth_date = {{Birth date and age|1946|08|15}}
| birth_place = [[Houston, Texas]], US
| death_date =
| death_place =
| nationality =
| party = [[Democratic Party (United States)|Democratic]]
| spouse = {{marriage|Jim Whitmire|1970}}, his death in 1976;<br />{{marriage|Alan J. Whelms|2002}}
| relations =John Whitmire (brother-in-law)
| children =
| residence = [[Hauʻula, Hawaii]]
| alma_mater = [[University of Houston]]
| occupation =
| profession = [[Businesswoman]], politician, accountant, professor
| signature =
| website =
| footnotes =
}}

'''Kathryn Jean "Kathy" Whitmire''' (née Niederhofer) (born August 15, 1946) is best known as the first female Mayor of the city of [[Houston]], Texas, serving five consecutive two-year terms, from 1982 to 1991. She also served two terms as City Controller from 1977 to 1981, which made her the first female elected to any office in the city. A native of Houston, she was the daughter of Karl Niederhofer, a licensed electrician, and his wife Ida (née Reeves). After earning both Bachelor and master's degrees from the University of Houston, she married fellow student Jim Whitmire and began working for eight years with a major national accounting firm, during which time she became a Certified Public Accountant (CPA). In 1977, she was appointed to a two-year term as Houston [[comptroller|City Controller]]. In 1979, she was elected to another term in the same office.

Whitmire drew national attention when she defeated former Sheriff Jack Heard in an election for Mayor of Houston. The election drew national attention because it symbolized a major political realignment in what was then the fourth-largest city in the United States.

In office, she implemented many reforms to city finances, enabling new programs without raising taxes. Her appointment of the city's first African American police chief and the first Hispanic woman as presiding judge of the Municipal Court, her support of a failed job rights bill for homosexuals, among other acts, cemented her support among many minority groups.<ref name="Reinhold">[https://query.nytimes.com/gst/fullpage.html?sec=health&res=9905E4D91538F934A35752C1A963948260 Reinhold, Robert. "Aids Issue Seen as Minor Factor in Houston Vote."  ''New York Times''. November 7, 1985.] Accessed December 13, 2015</ref>

When former mayor Louie Welch attempted a comeback in the 1985 election, he was unable to mount a convincing argument that he could more ably lead the city out of a recession than Whitmire could. Instead, the opposition to Whitmire focused on public fears about the AIDS epidemic.{{efn-ua|Prior to the mayoral election, a referendum repealed the 1982 non-discrimination ordinance. Despite Whitmire's strong opposition to the repeal, she won the mayoral race in the general election. Welch, who strongly supported the referendum, lost.}}  Two city councilmen, Anthony Hall and Judson Robinson, allied themselves with the so-called "straight slate", which opposed gay rights and supported Welch. The issue failed to affect Whitmire's support. She won the election, getting 59.8 percent of the votes, while Hall and Robinson lost their seats.<ref name="Reinhold"/> Her string of victories ended with the 1991 mayoral election when she was defeated by long-time political power broker [[Bob Lanier (politician)|Bob Lanier]] and State Representative [[Sylvester Turner]]. Lanier defeated Turner in the December runoff. She has never run for political office again.

==Early life==
Kathy Niederhofer received her basic education in the Houston Independent School District, ultimately graduating from [[San Jacinto High School (Houston)|San Jacinto High School]]. She then enrolled at the [[University of Houston]], and graduated ''magna cum laude'' with a Bachelor of Business Administration degree in accounting in 1968. She continued her studies at the university to earn a [[Master of Accountancy]] degree in 1970. In that same year, she married a fellow student, James M. (Jim) Whitmire, who died in 1976.{{efn-ua|Jim Whitmire died after a 10-year fight with juvenile diabetes and its complications.}}  In 1970, she began working in the Houston office of the well-known accounting firm [[Coopers and Lybrand]]. She also began working to qualify as a [[Certified Public Accountant]] (CPA), and opened an accounting firm with her husband. She also found time and energy to serve on the faculty of the Department of Business Management at the University of {{nowrap|Houston–Downtown}}.<ref name="KJWPapers">[http://www.lib.utexas.edu/taro/uhwarc/00027/warc-00027.html "Kathryn J. Whitmire Papers." Texas Archival Resources Online.] Accessed December 9, 2015.</ref>

==Political career==
Kathy's exposure to and interest in city politics began in her parents' home. Her father had been active in precinct-level politics, and the family frequently talked about local political issues. After she married, her husband's brother, [[John Whitmire]], already a rising star in the Texas Democratic Party, was a willing political mentor.  Whitmire was appointed as City Controller to serve out the unexpired term of [[Leonel Castillo]], who had been appointed as Commissioner of Immigration for the United States Citizenship and Immigration Services, making him the first Hispanic to hold that role. Castillo accepted the Federal position on April 7, 1977.<ref>USCIS History</ref> As City Controller, Whitmire became extremely knowledgeable about all aspects of the city's finances. Whitmire criticized the late then-Mayor Jim McConn for his inefficiency and lax administration.<ref name="Encyclopedia">[http://www.encyclopedia.com/doc/1G2-3404706848.html "Kathryn Jean Niederhofer Whitmire." ''Encyclopedia of World Biography,'' 2004. Encyclopedia.com.] Accessed December 11, 2015.</ref>  Becoming familiar with "sweetheart contracts", that had often been awarded to friends and supporters of influential office holders, she stopped the practice by rigorously enforcing observance of the Open Bidding laws. She then served five continuous two-year terms as mayor, partly during a downturn in the economy.<ref name="UPI">[http://www.upi.com/Archives/1987/11/03/Mayor-Kathy-Whitmire-challenged-by-a-crowded-but-politically/6433562914000/  "Mayor Kathy Whitmire, challenged by a crowded but politically..." UPI Archives. November 3, 1981.] Accessed December 8, 2015.</ref>  She also diligently looked for inefficiencies or outright wasteful practices in each of the city offices. Although this angered many "insiders", she won election in 1979 to a second term, becoming the first female elected to a top job in the Houston City government.

Whitmire decided to run for mayor in the 1981 election. The incumbent, Jim McConn, had already served two terms in the office and was supported by the business community, who had largely controlled city politics for decades. However, McConn lost in the primary to Jack Heard, formerly Sheriff of Harris County and the City Controller, Whitmire. There was a sharp contrast between the two candidates in the general election. Heard was 63 years old, with 25 years of experience in political office. Whitmire was only 35 and had little political experience. She said in her speeches that her opponent's experience was too narrowly focused on law enforcement. According to one report, Heard spent $1.5 million on the race, while his opponent spent $650,000.<ref name="Harsch">[http://www.csmonitor.com/1981/1119/111943.html  Harsch, Jonathan.  "Houston's city controller wins mayoral race."  ''Christian Science Monitor''. November 19, 1981.] Accessed December 16, 2015.</ref> While Heard had credibility with those who were more concerned about law and order or maintaining the status quo, Whitmire campaigned on her fiscal conservatism and moderate-to-liberal views on social issues. Whitmire was supported by a coalition of women, minorities, and other groups who were looking for a more progressive city government. These voters had been strengthened by the wave of newcomers who had moved into Houston during the boom years. She won the race with 62.5 percent of the vote. As noted by the ''New York Times'', <blockquote>The developers and bankers and oil millionaires who used to handpick candidates can no longer be said to dominate the political scene. Houston is home today to too many different kinds of people, with too many different sets of values and interests, for any one group to control elections so easily.<ref name="Stevens">[https://www.nytimes.com/1981/11/22/weekinreview/the-houston-that-was-loses-its-hold.html?_r=0 Stevens, William K. "The Houston that was loses its hold." ''New York Times''. November 22, 1981.] Accessed December 16, 2015.</ref></blockquote>

Whitmire was the first mayor to appoint an African American, [[Lee P. Brown]], as Houston's [[police chief]].  Brown had previously served as Commissioner of Public Safety, in Atlanta, Georgia. In Houston, he introduced the concept of Community Policing, creating improved relationships between the police department and the various diverse communities of the city. He left Houston in 1990 to serve as Police Commissioner of New York City. {{efn-ua| Brown later returned to live in Houston, and successfully ran for mayor in 1997.}}  Brown was succeeded by the city's first female police chief, [[Elizabeth Watson]], after he resigned to accept the top police job in New York City.<ref name="TTW">[http://houstonlifestyles.com/historically-houston-kathy-whitmire-houstons-candidate-of-firsts/ Tompkins-Walsh, Teresa. "HISTORICALLY HOUSTON Kathy Whitmire Houston’s Candidate of Firsts."] ''Houston Lifestyles and Homes''. December 15, 2015</ref> Whitmire also appointed the first [[Hispanic]], [[Sylvia R. Garcia]], as presiding judge of the Houston Municipal Court. {{efn-ua|Garcia later ran for Houston City Controller and Commissioner of [[Harris County, Texas]]. More recently Garcia has served in the Texas Legislature.}}

In 1985, she ran for mayor against five-term mayor [[Louie Welch]]. She won that election, getting about 60 percent of the vote. She was re-elected in 1987, winning 74 percent of the vote. Her closest rival in a field of six was Bill Anderson, who only received 12 percent of the vote. She then won the 1989 mayoral election, receiving 63 percent of the vote (176,342 votes), while former mayor Fred Hofheinz received 88,971 votes (32 percent of the total).{{efn-ua|This would be the last time any sitting mayor could serve more than three terms, because a subsequent law imposed a three-term limit on the office.}}{{citation needed|date=January 2016}}

Bob Lanier was a wealthy attorney who was considered a political kingmaker in Houston. During the 1980s, he had sold off much of his investment portfolio, just in time to avoid the economic collapse that engulfed many other investors. Appointed as chairman of the Texas Highway Commission, he became a critic of Mayor Whitmire's plan for Houston Metro to build a monorail system. Supporters of the Metro agency decided that it was prudent to compromise with Lanier, so they agreed to fund more street improvements, rather than concentrate solely on a rail system. Lanier soon became chairman of the Metro system board. The mayor fired Lanier from his Metro position in December 1989.  It proved to be fatal to Whitmire's political future. Unable to find another satisfactory candidate for her office, Lanier decided to challenge her himself. The hot-button issues for the 1991 election became fear of street crime and skepticism about the functionality of an expensive monorail system proposed by Whitmire. Lanier promised to cancel the latter and put more police on the streets. Lanier made inroads into Whitmire's white supporters, while a black candidate, Sylvester Turner, cut into her black support.<ref name="Miller">[http://www.khou.com/story/news/local/2014/12/20/former-houston-mayor-bob-lanier-has-died/20707349/ Miller, Doug. "Popular, powerful Houston Mayor Bob Lanier dies at 89." KHOU-11 News. December 20, 2014.] Accessed December 15, 2015.</ref>

==After the mayorship==
After the 1991 election. she turned her talents to teaching at [[Rice University]], and the [[University of Maryland]]. At Rice, she served as Director of the Rice Institute for Policy Analysis and held the Tsanoff Lectureship in Public Affairs. In the latter position she taught courses in public policy, management, and political science.<ref>[http://www.frogpond.com/authorsprofile.cfm?authorsid=kwhitmire "Kathy Whitmire" Frogpond.com] Accessed December 8, 2015.</ref> In 1994, she was appointed President and CEO of [[Junior Achievement]]. In 1995 and 1996, Whitmire served as lecturer on Public Policy at the John F. Kennedy School of Government at Harvard University and was a Fellow at Harvard's Institute of Politics.<ref name="KJWPapers"/> In 1997, she accepted a position at the newly formed Academy of Leadership at the University of Maryland College Park campus. She is credited with attracting former U.S. Senator [[Bill Bradley]] to join the Academy as a scholar and chairman of the board.<ref name="Folkenflik">[http://articles.baltimoresun.com/1997-07-12/news/1997193042_1_whitmire-university-of-maryland-academy-of-leadership Folkenflik, David. "Former mayor, future leaders Campus: Kathyrn J. Whitmire is wrapping up her first year at the University of Maryland, College Park's Academy of Leadership, where she hopes to prepare members of a new generation to guide their communities." ''Baltimore Sun''. July 12, 1997.] Accessed December 11, 2015.</ref>

Unlike most of Houston's former mayors who are still living, Whitmire has moved away from the city. She has said that she has no plans to return, although she visits the city regularly to see family and friends.<ref>[http://www.houstontx.gov/controller/whitmire174.html "Former Mayor and Former Controller Kathy Whitmire visits Controller Green." Houston City Controller's Office.August 26, 2010.] Accessed December 13, 2015.</ref>  She moved to Hawaii in 2001, where she became an active investor in real estate. She married Alan J. Whelms in 2002<ref name="KJWPapers"/> She commutes by air to work at the University of Maryland, where she is a professor at the [[James MacGregor Burns|James MacGregor Burns Academy of Leadership.]]<ref>[http://www.houstontx.gov/savvy/archives/Winter03/winter03_exmayors.htm Smith, Roger. "Ex-Mayors shaped today's Houston." ''CitySavvy''. Winter 2003.] Retrieved December 12, 2015.</ref> She has also taught at [[Harvard University]] and [[Rice University]].<ref name="Nakaso"/> On July 1, 2005, she became the volunteer president of the [[Outdoor Circle]], an organization dedicated to preserving Hawaii's beauty by eliminating outdoor blight.<ref name="Nakaso">{{cite news |url=http://the.honoluluadvertiser.com/article/2005/Jul/03/bz/bz01p.html |last=Nakaso |first=Dan |title=Outdoor Circle grows |newspaper=[[The Honolulu Advertiser]] |date=July 3, 2005 |accessdate=May 5, 2016}}</ref>

When Annise Parker, Kathy's long-time friend and supporter, ran for Mayor in 2010, Whitmire flew to Houston to show her support by hosting a fund raiser. Parker ultimately won the election and became the city's first gay mayor.<ref name="TTW" />

==Awards and honors==
Whitmire has received numerous awards for her efforts and accomplishments, including:<ref name="KJWPapers"/>
* Public Service Award from the American Society of Women CPAs (1984)
* International Business Award from the Houston World Trade Association (1985)
* Michael A. DiNunzio Award for Partnerships for Youth from the U.S. Conference of Mayors (1985)
* Distinguished Professional Woman by the Committee on the Status of Women at the University of Texas Health Science Center at Houston(1987)
* Institute of Human Relations Award from the International New Thought Alliance (1986)
* Distinguished Sales Award by the Sales and Marketing Executives Society of Houston for her economic development efforts for Houston. (1987)
* In 1985, Whitmire received the Michael A. DiNunzio Award for Partnerships for Youth from the U.S. Conference of Mayors
* In 1987, she co-chaired the National League of Cities' (NLC) International Economic Development Task Force and
* was the 1987 chair of the NLC's Finance, Administration and Intergovernmental Relations Steering Committee
* Served as second vice-president on the board of directors of the Texas Municipal League and was a member of the board of trustees of the UH Foundation and the executive committee of the Houston Economic Development Council
* Inducted into the Houston Hall of Fame on August 26, 2010<ref>[http://downtownhouston.org/news/blogpost/ex-mayor-whitmire-heads-hall-fame-class/ Radowick, Peter. "Former Mayor Kathy Whitmire Heads Houston Hall of Fame Class of 2010." August 26, 2010.] Accessed December 8, 2015.</ref>

==Political legacy==
Soon after Whitmire left office, Bob Stein, a political scientist at Rice University, said that her legacy was to make the city operate more efficiently, citing improvements in fundamental operations like garbage collection and public transportation. He added, "She brought (Houston) into the 21st Century (''sic'') in city administration." His concluding opinion was, "I think she really was one of the great administrative mayors of this city's history, maybe the finest."<ref name="Victoria">[https://news.google.com/newspapers?nid=861&dat=19911107&id=f69HAAAAIBAJ&sjid=RYAMAAAAIBAJ&pg=1745,4967491&hl=en "Whitmire's 10-year legacy." ''The Victoria Advocate''. November 7, 1981.] Retrieved December 22, 2015.</ref>

A quarter of a century has elapsed since Whitmire was first elected mayor and 14 years have passed since she left the office. The "good old boy network" that once controlled city politics has lost much of its political clout. It can no longer count on support by the local news media. {{efn-ua|Since then the ''Houston Post'' has gone out of business and the ''Houston Chronicle'' was sold to new owners.}} Many of the reforms that occurred during that period are still in effect. Elected city offices are still more open to minority candidates. There have been two African-Americans (Lee P. Brown and Sylvester Turner) and one woman (Annise Parker) elected to the mayor's office.{{efn-ua|Parker also happens to be gay.<ref>[https://www.nytimes.com/2009/12/13/us/politics/13houston.html?_r=0 McKinley, Jr., James C. "Houston Is Largest City to Elect Openly Gay Mayor." ''New York Times''. December 12, 2009] Accessed December 29, 2015</ref>}}  A number of minority members have been elected as council members and as city controller.

==Notes==
{{notelist-ua}}

==References==
{{Portal|Houston|Biography}}
{{Reflist|30em}}

== External links ==
* Whitmire, Mayor Kathy and Jim Barlow. [http://digital.houstonlibrary.org/cdm/singleitem/collection/oralhistory/id/79 Mayor Kathy Whitmire Oral History], Houston Oral History Project, July 14, 2008.

{{s-start}}
{{s-off}}
{{succession box
|  title=[[Mayor of Houston, Texas]]
|  before=[[Jim McConn]]
|  after=[[Bob Lanier (politician)|Bob Lanier]]
|  years=1982–1991
}}
{{s-end}}
{{United States Conference of Mayors Presidents}}
{{Mayors of Houston}}

{{DEFAULTSORT:Whitmire, Kathryn J.}}
[[Category:1946 births]]
[[Category:Living people]]
[[Category:American accountants]]
[[Category:American businesspeople]]
[[Category:American members of the Churches of Christ]]
[[Category:Women mayors of places in the United States]]
[[Category:Mayors of Houston]]
[[Category:People from Houston]]
[[Category:People from Hawaii]]
[[Category:San Jacinto High School alumni]]
[[Category:University of Houston alumni]]
[[Category:Women in Texas politics]]
[[Category:Women in finance]]
[[Category:Texas Democrats]]
[[Category:University of Maryland, College Park faculty]]