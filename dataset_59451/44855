{{About|Comptroller and Auditor General of India|similar title in other jurisdictions|Comptroller and Auditor General}}
{{Use Indian English|date=February 2014}}
{{Use dmy dates|date=February 2014}}

{{Infobox official post
|post            = Comptroller and Auditor General (C&AG)
|body            = 
|native_name     = {{lang|sa|भारत के नियंत्रक-महालेखापरीक्षक (निमलेप)}}
|flag            = Flag of India.svg
|flagsize        = 100px
|flagborder      = 
|flagcaption     = 
|insignia        = 
|insigniasize    = 
|insigniacaption = 
|department      = 
|reports_to      = 
|image           = 
|imagesize       =
|alt             = 
|incumbent       = [[Shashi Kant Sharma]]
|incumbentsince  = 23 May 2013
|style           = 
|residence       = 
|nominator       = [[Prime Minister of India]]
|nominatorpost   = 
|appointer       = [[President of India]]
|appointerpost   = 
|Preceded by     = [[Vinod Rai]]
|termlength      = 6 yrs or up to 65 yrs of age<br> <small>(whichever is earlier)</small>
|inaugural       = 
|formation       = 
|abolished       = 
|succession      = 
|deputy          = 
|salary          = {{INRConvert|90|k}}<ref name="THE COMPTROLLER AND AUDITOR-GENERAL'S (DUTIES, POWERS AND CONDITIONS OF SERVICE) ACT, 1971"/><ref name="CAG - Article 148 of Constitution of India"/>
|allegiance      = 
|commands        = 
|website         = {{url|http://saiindia.gov.in}}
}}
{{Politics of India}}
The '''Comptroller and Auditor General (CAG) of India''' is an authority, established by the [[Constitution]] under '''Constitution of India/Part V - Chapter V/Sub-part 7B/Article 148''', who [[audit]]s all [[receipts]] and [[cost|expenditure]] of the [[Government of India]] and the [[states and territories of India|state governments]], including those of bodies and authorities substantially financed by the government. The CAG is also the [[external auditor]] of [[Government-owned corporations]] and conducts supplementary audit of government companies, i.e., any non-banking/ non-insurance company in which Union Government has an equity share of at least 51 per cent or subsidiary companies of existing government companies. The reports of the CAG are taken into consideration by the [[Public Accounts Committee (India)|Public Accounts Committee]]s (PACs) and Committees on Public Undertakings (COPUs), which are special committees in the [[Parliament of India]] and the state legislatures. The CAG is also the head of the Indian Audit and Accounts Department, the affairs of which are managed by officers of [[Indian Audit and Accounts Service]], and has over 58,000 employees across the country.

The CAG is mentioned in the Constitution of India under Article 148 – 151.

The CAG is ranked 9th and enjoys the same status as a judge of [[Supreme Court of India]] in [[Indian order of precedence]]. The current CAG of India is [[Shashi Kant Sharma]],<ref name="Defence Secy to be new CAG">{{cite news|url=http://www.thehindu.com/news/national/defence-secretary-shashi-kant-sharma-to-be-new-cag/article4736437.ece|title=Defence Secretary Shashi Kant Sharma to be new CAG|last=|first=|date=|work=|access-date=|via=}}</ref> who was appointed on 23 May 2013. He is the 12th CAG of India.

== Appointment ==
The [[Comptroller]] and Auditor-General of India is appointed by the [[President of India]]<ref>Constitution of India, Article 148 for the period of six years.1</ref> following a recommendation by the [[Prime Minister of India|Prime Minister]]. On appointment, he/she has to make an oath or affirmation before the President of India.

===Oath or affirmation===
"I,A.B.,having appointed Comptroller and Auditor-General of India do swear in the name of God/solemnly affirm that I will bear true faith and allegiance to the Constitution of India as by law established, that I will uphold the sovereignty and integrity of India, that I will duly and faithfully and to the best of my ability, knowledge and judgement perform the duties of my office without fear or favour, affection or ill-will and that I will uphold the Constitution and the laws.
===Duties of the CAG===
As per the provisions of the constitution, the CAG’s (DPC) (Duties, Powers and Conditions of Service) Act, 1971 was enacted. As per the various provisions, the duties of the CAG include the audit of:

* Receipts and expenditure from the Consolidated Fund of India and of the State and Union Territory having legislative assembly.
* Trading, manufacturing, profit and loss accounts and balance sheets, and other subsidiary accounts kept in any Government department; Accounts of stores and stock kept in Government offices or departments. 
* Government companies as per the provisions of the Companies Act, 1956.
* Corporations established by or under laws made by Parliament in accordance with the provisions of the respective legislation.
* Authorities and bodies substantially financed from the Consolidated Funds of the Union and State Governments. Anybody or authority even though not substantially financed from the Consolidated Fund, the audit of which may be entrusted to the C&AG.
* Grants and loans given by Government to bodies and authorities for specific purposes. 
* Entrusted audits e.g. those of Panchayati Raj Institutions and Urban Local Bodies under Technical Guidance & Support (TGS).
=== Recent achievements ===
Recently the CAG under [[Vinod Rai]] has constantly been in the limelight for its reports exposing mega corruption, particularly in [[2G spectrum scam]], [[Concerns and controversies over the 2010 Commonwealth Games|Commonwealth Games scam]] and other scams.<ref name="A watchdog that bites">{{cite news|title=A watchdog that bites |url=http://www.thehindu.com/opinion/editorial/article3796000.ece |accessdate=27 August 2012 |newspaper=The Hindu |date=20 August 2012 |archiveurl=http://www.webcitation.org/6AF1G2lN4?url=http://www.thehindu.com/opinion/editorial/article3796000.ece |archivedate=27 August 2012 |location=Chennai, India |deadurl=yes |df=dmy }}</ref><ref name="CAG 'activism' gets the thumbs up">{{cite news|title=CAG ‘activism’ gets the thumbs up |url=http://www.deccanherald.com/content/183601/cag-activism-gets-thumbs-up.html |accessdate=27 August 2012 |newspaper=Deccan Herald |author=B S Arun |archiveurl=http://www.webcitation.org/6AF1G2hzW?url=http://www.deccanherald.com/content/183601/cag-activism-gets-thumbs-up.html |archivedate=27 August 2012 |quote="Corporate India needs to go through a phase of reflection and soul searching" |deadurl=yes |df=dmy }}</ref>

=== Suggested reforms ===
In June 2012, [[Lal Krishna Advani]] a veteran Indian politician and former [[Deputy Prime Minister of India]] (as well as former [[Leader of the Opposition (India)|Leader of the Opposition]] in [[Parliament of India|Indian Parliament]])<ref>{{cite web|title=Members Bioprofile|url=http://164.100.47.132/LssNew/Members/Biography.aspx?mpsno=9|publisher=Lok Sabha of India/National Informatics Centre, New Delhi|accessdate=27 April 2011}}</ref> suggested that CAG's appointment should be made by a [[bipartisan]] [[collegium]] consisting of the prime minister, the [[Chief Justice of India]], the [[Ministry of Law and Justice (India)|Law Minister]] and the Leaders of the Opposition in the Lok Sabha and the Rajya Sabha.<ref name="Karunanidhi backs Advani's view on collegium to appoint CAG, EC">{{cite news|title=Karunanidhi backs Advani's view on collegium to appoint CAG, EC|url=http://articles.timesofindia.indiatimes.com/2012-06-05/india/32055097_1_karunanidhi-backs-collegium-l-k-advani|accessdate=5 October 2012|newspaper=Times of India|date=5 June 2012}}</ref><ref name="Karunanidhi backs Advani's plea for collegium">{{cite news|title=Karunanidhi backs Advani's plea for collegium|url=http://www.thehindu.com/news/national/article3493632.ece|accessdate=5 October 2012|newspaper=The Hindu|date=5 June 2012|location=Chennai}}</ref> Subsequently, [[M Karunanidhi]], the head of [[Dravida Munnetra Kazhagam]] (DMK) party and five times [[Chief Minister of Tamil Nadu]]<ref>{{cite web|url=http://www.dmk.in/ |title=DMK's Official Homepage-Chennai-Tamilnadu-India 800x600 screen resolution |publisher=Dmk.in |date=9 December 2011 |accessdate=24 January 2012}}</ref> supported the suggestion. Advani made this demand to remove any impression of bias or lack of transparency and fairness because, according to him, the current system was open to "manipulation and [[partisanship]]".<ref name="Karunanidhi backs Advani's view on collegium to appoint CAG, EC"/><ref name="Karunanidhi backs Advani's plea for collegium"/> Similar demand was made by many former CEC's such as [[B B Tandon]], [[N Gopalaswamy]] and [[S Y Quraishi]],<ref name="Collegium needed to select EC: SY Quraishi">{{cite news|title=Collegium needed to select EC: SY Quraishi|url=http://articles.timesofindia.indiatimes.com/2012-07-16/india/32697233_1_collegium-sy-quraishi-election-commissioner|accessdate=5 October 2012|newspaper=Times of India|date=16 July 2012}}</ref> however the government did not seem too keen.<ref name="Ex-CECs backed collegium, Law Ministry not too keen">{{cite news|title=Ex-CECs backed collegium, Law Ministry not too keen|url=http://www.indianexpress.com/news/excecs-backed-collegium-law-ministry-not-too-keen/960128/0|accessdate=5 October 2012|newspaper=Indian Express|date=10 June 2012}}</ref>

CPI MP [[Gurudas Dasgupta]] wrote a letter to the PM and demand CAG has appointed by the collegium of consisting the PM, the CJI and the leader of the opposition in Lok Sabha but the PM declined. Former CAG V. K. Shunglu has suggested in its CWG scam report that CAG be made a multi-member body.{{Citation needed|date=February 2017}}

PMO minister [[V. Narayanasamy|V.Narayanasamy]] in his interview with [[Press Trust of India|PTI]] said Government is considering the Shunglu panel report but PM and Finance Minister declined it. Later V. Narayanasamy said he misquoted but PTI reaffirmed it.{{Citation needed|date=February 2017}}

== Compensation ==
The salary and other conditions of service of the CAG are determined by the [[Parliament of India]] through "The Comptroller and Auditor-General's (Duties, Powers and Conditions of Service) Act, 1971". His salary is same as that of judge of the Supreme court of India. Neither his salary nor rights in respect of leave of absence, pension or age of retirement can be varied to his disadvantage ''after'' his appointment. The CAG is not eligible for further office either under the [[Government of India]] or under the Government of any State after he has ceased to hold his office. These provisions are in order to ensure the ''independence'' of CAG.<ref name="CAG - Article 148 of Constitution of India">{{cite web | url=http://saiindia.gov.in/english/home/about_us/mandate/Constitutional/Constitutional.html | title=CAG – Article 148 of Constitution of India | accessdate=6 April 2012}}</ref><ref name="Chapter V - Constitution of India">{{cite encyclopedia|title=Chapter V - Constitution of India|encyclopedia=|url=http://en.wikisource.org/wiki/Constitution_of_India/Part_V#Chapter_V_.7BComptroller_and_Auditor-General_of_India.7D|accessdate=6 April 2012|date=}}</ref>

{| class="wikitable" style="margin:1ex 0 1ex 1ex;"
|- colspan="3" style="text-align:center;"
|+ '''Salary of CAG'''
! Date !! Salary
|-
| 1 January 2006 || style="text-align:right;"| {{INRConvert|90|k}}<ref name="THE COMPTROLLER AND AUDITOR-GENERAL'S (DUTIES, POWERS AND CONDITIONS OF SERVICE) ACT, 1971">{{cite web|url=http://www.cag.gov.in/html/about_legal_dpc.htm|title=THE COMPTROLLER AND AUDITOR-GENERAL'S (DUTIES, POWERS AND CONDITIONS OF SERVICE) ACT, 1971|publisher=CAG India|accessdate=27 August 2012|archiveurl=http://www.webcitation.org/6AE3014Om?url=http://www.cag.gov.in/html/about_legal_dpc.htm|archivedate=27 August 2012|deadurl=yes|df=dmy}}</ref><ref name="HC and SC Judges Salaries and Conditions of Service Amendment Bill 2008">{{cite web|title=The High Court and Supreme Court Judges Salaries and Conditions of Service Amendment Bill 2008|url=http://www.prsindia.org/uploads/media/1230018357/1230018357_The_High_Court_and_Supreme_Court_Judges__Salaries_and_Conditions_of_Service__Amendment_Bill__2008.pdf|publisher=PRS India}}</ref>
|}

== Removal ==
The CAG can be removed only on an address from both house of parliament on the ground of proved misbehaviour or incapacity. The CAG vacates the office on attaining the age of 65 years age even without completing the 6 years term.

== Indian Audit and Accounts Service ==
{{Main|Indian Audit and Accounts Service}}
The [[Constitution of India]] [Article 148] provides for an independent office to the CAG of India. He or she is the head of Indian Audit and Accounts Department. He/She has a duty to uphold the Constitution of India and laws of the Parliament to safeguard the interests of the public exchequer. The Indian Audit and Accounts Service aids the CAG in the discharge of his/her functions.

== Scope of audits ==
{{Quote box
 |quote = "CAG is not a munimji or an accountant or something like that... He is a constitutional authority who can examine the revenue allocation and matters relating to the economy. CAG is the principal auditor whose function is to go into the economy, effectiveness and efficiency of the use of resources by the government. If the CAG will not do, then who else will do it"
 |source = – Observation of a bench of [[Supreme Court of India]] while dismissing a petition challenging CAG reports on 2G spectrum, Coal Blocks Allotment, etc.<ref name="SC Rejects PIL Against CAG Examining Coal Allotment">{{cite news|title=SC Rejects PIL Against CAG Examining Coal Allotment |url=http://news.outlookindia.com/items.aspx?artid=776877 |accessdate=1 October 2012 |newspaper=Outlook India |date=1 October 2012 |archiveurl=https://web.archive.org/web/20121001120643/http://news.outlookindia.com/items.aspx?artid=776877 |archivedate=1 October 2012 |deadurl=yes |df=dmy }}</ref>|width = 25%
}}

Audit of government accounts (including the accounts of the state governments) in India is entrusted to the CAG of India who is empowered to audit all expenditure from the Consolidated Fund of the union or state governments, whether incurred within India or outside, all revenue into the Consolidated Funds and all transactions relating to the Public Accounts and the Contingency Funds of the Union and the states. Specifically, audits include:
* [[financial transaction|Transactions]] relating to [[debt]], [[Deposit account|deposit]]s, [[remittances]], Trading, and manufacturing
* [[income statement|Profit and loss accounts]] and [[balance sheet]]s kept under the order of the [[President of India|President]] or [[Governors and Lieutenant-Governors of states of India|Governors]]
* Receipts and stock accounts. CAG also audits the books of accounts of the government companies as per Companies Act.

In addition, the CAG also executes performance and compliance audits of various functions and departments of the government. Recently, the CAG as a part of thematic review on "Introduction of New Trains" is deputing an auditors' team on selected trains, originating and terminating at Sealdah and Howrah stations, to assess the necessity of their introduction.<ref>{{cite news| url=http://timesofindia.indiatimes.com/city/kolkata/Comptroller-and-Auditor-General-lens-on-trains-introduced-by-Mamata-Banerjee/articleshow/14829314.cms | title= Comptroller and Auditor General lens on trains introduced by Mamata Banerjee| date= 12 July 2012 | work=The Times Of India}}</ref> In a path-breaking judgement, the Supreme Court of India ruled that the CAG General could audit private firms in revenue-share deals with government.

CAG has been appointed as external auditor of three major UN organisations: the Vienna-based [[International Atomic Energy Agency]] (IAEA), the Geneva-based [[World Intellectual Property Organisation]] (WIPO) and [[World Food Programme]] (WFP).

CAG has been elected the Chairman of the [[United Nation]]s' panel of external auditors.<ref name="CAG Vinod Rai elected U.N. external audit panel chief">{{cite news | url=http://timesofindia.indiatimes.com/india/CAG-Vinod-Rai-to-head-UN-audit-panel/articleshow/11039779.cms | title=CAG Vinod Rai elected U.N. external audit panel chief | date=8 December 2011 | accessdate=28 February 2012 | work=The Times Of India}}</ref><ref name="CAG Vinod Rai to head UN audit panel">{{cite news | url=http://timesofindia.indiatimes.com/india/CAG-Vinod-Rai-to-head-UN-audit-panel/articleshow/11039779.cms | title=CAG Vinod Rai to head UN audit panel | date=9 December 2011 | accessdate=28 February 2012 | work=The Times Of India}}</ref>

Rebecca Mathai is an Indian who is presently the External Auditor of UN organisation World Food Programme(WFP) Headquartered at Rome, Italy.{{citation needed|date=October 2012}}

== Reforms suggested by former CAG Vinod Rai ==
In November 2009, the CAG requested the government to amend the 1971 Audit Act to bring all private-public partnerships (PPPs), Panchayti Raj Institutions and societies getting government funds within the ambit of the CAG.The amendment further proposes to enhance CAG’s powers to access information under the Audit Act. In the past, almost 30% of the documents demanded by CAG officials have been denied to them.<ref>[http://www.dnaindia.com/india/1857261/report-four-years-on-upa-govt-yet-to-accept-cag-s-request-for-more-powers Four years on, UPA govt yet to accept CAG's request for more powers | Latest News & Updates at DNAIndia.com<!-- Bot generated title -->]</ref> The PPP model has become a favourite mode of executing big infrastructure projects worth millions of rupees and these projects may or may not come under the audit purview of the CAG, depending on sources of funds and the nature of revenue sharing agreements between the government and the private entities. Currently, it is estimated that 65 percent of government spending does not come under the scrutiny of the CAG.<ref>[http://news.oneindia.in/feature/2013/after-shaking-the-govt-cag-vinod-rai-is-retiring-1221390.html After shaking and hurting the govt he is retiring - News Oneindia<!-- Bot generated title -->]</ref>

== Prominent audit reports ==
Following are some of the most debated CAG reports:

=== 2G Spectrum allocation ===
{{main|2G Spectrum scam}}

A CAG report on issue of Licences and Allocation of 2G Spectrum<ref>{{cite web|url=http://www.saiindia.gov.in/english/home/Our_Products/Audit_Report/Government_Wise/union_audit/recent_reports/union_performance/2010_2011/Civil_%20Performance_Audits/Report_no_19/Report_no_19.html|title=Report No. -19 of 2010-11 for the period ended March 2010  Performance Audit of Issue of Licences and Allocation of 2G Spectrum by the Department of Telecommunications ( Ministry of Communications and Information Technology)|publisher=CAG|accessdate=17 April 2015}}</ref> resulted in a huge controversy. The report estimated that there was a ''presumptive loss'' of {{INRConvert|1766|b}} by the [[United Progressive Alliance]] (UPA) government.<ref>{{cite web|url=http://cag.gov.in/html/reports/civil/2010-11_19PA/Telecommunication%20Report.pdf |title=Performance Audit Report on the Issue of Licences and Allocation of 2G Spectrum |accessdate=5 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120321130348/http://cag.gov.in/html/reports/civil/2010-11_19PA/Telecommunication%20Report.pdf |archivedate=21 March 2012 |df=dmy }}</ref> In a [[chargesheet]] filed on 2 April 2011 by the investigating agency [[Central Bureau of Investigation|Central Bureau of Investigation (CBI)]], the agency pegged the loss at {{INRConvert|310|b}}<ref name="Indiatimes 2G loss">{{cite news|url=http://articles.timesofindia.indiatimes.com/2011-09-07/india/30122800_1_spectrum-trai-2g|newspaper=Times of India | title=2G loss? Govt gained over Rs.3,000cr: Trai | date=7 September 2011}}</ref>

All the speculations of profit, loss and no-loss were put to rest on 2 February 2012 when the [[Supreme Court of India]] on a [[Public Interest Litigation|public interest litigation (PIL)]] declared allotment of spectrum as ''"unconstitutional and arbitrary"'' and quashed all the 122 licenses issued in 2008 during tenure of [[A. Raja]] (then [[Ministry of Communications and Information Technology (India)|minister for communications & IT]] in the UPA government) the main accused.<ref name="SC quashes 122 licences">{{cite news|title=SC quashes 122 licences|url=http://articles.timesofindia.indiatimes.com/2012-02-02/india/31016262_1_spectrum-licences-2g-spectrum-allotment-case|newspaper=Times of India | date=2 February 2012}}</ref> The court further said that [[A. Raja]] "wanted to favour some companies at the cost of the public exchequer" and "virtually gifted away important national asset".<ref>{{cite news|title=2G verdict: A Raja 'virtually gifted away important national asset', says Supreme Court|url=http://timesofindia.indiatimes.com/india/2G-verdict-A-Raja-virtually-gifted-away-important-national-asset-says-Supreme-Court/articleshow/11728003.cms|newspaper=Times of India | date=2 February 2012}}</ref>

Revenue loss calculation was further established on 3 August 2012 when according to the directions of the Supreme Court, [[Govt of India]] revised the reserve price for 2G spectrum to {{INRConvert|140|b}}<ref name="Cabinet sets Rs 14,000 cr as reserve price for 2G spectrum">{{cite news|title=Cabinet sets Rs 14,000 cr as reserve price for 2G spectrum|url=http://www.firstpost.com/business/cabinet-sets-rs-14000-cr-as-reserve-price-for-2g-spectrum-404702.html|accessdate=8 August 2012|newspaper=Firstpost|date=4 August 2012}}</ref><ref name="Cabinet decision on 2G auction price demolishes zero-loss theory">{{cite news|title=Cabinet decision on 2G auction price demolishes zero-loss theory|url=http://www.thehindu.com/news/national/article3739078.ece?homepage=true|accessdate=8 August 2012|newspaper=The Hindu|date=8 August 2012|location=Chennai, India|first=Shalini|last=Singh}}</ref>

=== Coal Mine Allocation ===
{{main |Coal Mining Scam}}

A 2012 CAG report on Coal Mine Allocation<ref>{{cite web|url=http://www.saiindia.gov.in/english/home/Our_Products/Audit_Report/Government_Wise/union_audit/recent_reports/union_performance/2012_2013/Commercial/Report_No_7/Report_No_7.html|title=Report No. - 7  of 2012-13 for the period ended March 2012 - Performance Audit of Allocation of Coal Blocks and Augmentation of Coal Production (Ministry of Coal)|publisher=CAG|accessdate=17 April 2015}}</ref> received massive media and political reaction as well as public outrage. During the 2012 monsoon session of the Parliament, the [[Bharatiya Janata Party|BJP]] protested the Government's handling of the issue demanding the resignation of the prime minister and refused to have a debate in the Parliament. The deadlock resulted in Parliament functioning only seven of the twenty days of the session.<ref>{{cite web|url=http://www.dnaindia.com/india/report_turmoil-ridden-monsoon-session-of-parliament-ends_1737812|title=Turmoil-ridden Monsoon session of Parliament ends|publisher=DNA|accessdate=7 September 2012}}</ref><ref>{{cite web|url=http://timesofindia.indiatimes.com/india/Stalling-Parliament-is-also-part-of-democracy-Sushma-says/articleshow/16296444.cms|title=Stalling Parliament is also a part of Democracy|last=|first=|date=|website=|publisher=|accessdate=7 September 2012}}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>

The CAG report criticised the Government by saying it had the authority to allocate coal blocks by a process of competitive bidding, but chose not to.<ref name="CAG report">{{cite web|url=http://www.sourcewatch.org/images/4/40/Draft_CAG_report_Pt_1.pdf|title="Draft Performance Audit, Allocation of Coal Blocks and Augmentation of Coal Production by Coal India Limited" Report of the Comptroller and Auditor General of India (Union Government (Commercial))|publisher=Comptroller and Auditor General of India (Union Government (Commercial))|accessdate=8 September 2012}} Hereafter Draft CAG Report.</ref> As a result, both [[State-owned enterprise|public sector enterprises]] (PSEs) and private firms paid less than they might have otherwise. In its draft report in March, the CAG estimated that the "windfall gain" to the allocatees was {{INRConvert|10673|b}}.<ref name="CAG report" /> The CAG Final Report tabled in Parliament put the figure at {{INRConvert|1856|b}}<ref>{{cite news| url=http://blogs.wsj.com/indiarealtime/2012/08/27/transcript-prime-minister-singh-counters-coalgate-allegations/ | work=The Wall Street Journal | title=Transcript: Prime Minister Manmohan Singh Counters 'Coalgate' Allegations - India Real Time - WSJ}}</ref>

While the initial CAG report suggested that coal blocks could have been allocated more efficiently, resulting in more revenue to the government, at no point did it suggest that corruption was involved in the allocation of coal. Over the course of 2012, however, the question of corruption came to dominate the discussion. In response to a complaint by the BJP, the [[Central Vigilance Commission]] (CVC) directed the CBI to investigate the matter. The CBI named a dozen Indian firms in a [[First Information Report]] (FIR), the first step in a criminal investigation. These FIRs accuse them of overstating their net worth, failing to disclose prior coal allocations, and hoarding rather than developing coal allocations.<ref name="hindustantimes.com">[http://www.hindustantimes.com/India-news/NewDelhi/Firms-hid-earlier-allocations-to-get-new-blocks-says-CBI/Article1-925788.aspx Firms hid earlier allocations to get new blocks, says CBI - Hindustan Times<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20131104093339/http://www.hindustantimes.com/India-news/NewDelhi/Firms-hid-earlier-allocations-to-get-new-blocks-says-CBI/Article1-925788.aspx |date=4 November 2013 }}</ref><ref>[http://zeenews.india.com/news/nation/coalgate-now-dmk-leader-in-the-dock_798230.html Coalgate: Now, DMK leader in the dock<!-- Bot generated title -->]</ref> The CBI officials investigating the case have speculated that bribery may be involved.<ref name="hindustantimes.com"/>

=== Fodder scam ===
{{main|Fodder scam}}

The scandal was first exposed due to the CAG report in the matter in December 1995. The report alleged of fraudulent withdrawal of government funds worth {{INRConvert|9.5|b}} in the [[Bihar]] animal husbandry department against non-existent supplies of fodder and medicines.<ref name="CBI orders prosecution of Laloo, Mishra in fodder scam">{{cite news|title=CBI orders prosecution of Laloo, Mishra in fodder scam|url=http://www.rediff.com//news/apr/28bihar.htm|accessdate=1 October 2012|newspaper=Rediff.com|date=April 1996|archiveurl=https://web.archive.org/web/20100531143055/http://www.rediff.com//news/apr/28bihar.htm|archivedate=31 May 2010}}</ref> Subsequently, based on [[Patna High Court]]'s orders, CBI investigated the case and registered as many as 63 cases. Many accused have been convicted while many cases are still under [[trial (law)|trial]].<ref name="toi2007jsd">{{Citation|title=Fodder scam case: 58 convicted, given jail terms |newspaper=Times of India |date=31 May 2007 |accessdate=5 November 2008 |url=http://www1.timesofindia.indiatimes.com/articleshow/2088947.cms |quote=Snippet: ''... A total 63 cases were registered in the scam and 41 were transferred to Jharkhand after it was created from Bihar in November 2000 ... The CBI has filed charge sheets in almost every case and trials are under progress. Till now the special CBI court has passed judgment in 16 cases and nearly 200 accused in different cases have been punished with two to seven years imprisonment ...''12121 |deadurl=yes |archiveurl=https://web.archive.org/web/20090501110715/http://www1.timesofindia.indiatimes.com/articleshow/2088947.cms |archivedate=1 May 2009 }}</ref>

=== Krishna-Godavari (KG) D-6 gas block ===
The oil ministry imposed a fine of ₹7000 crores on [[Mukesh Ambani]]'s company for the sharp drop in production of gas and violations mentioned in CAG's 2011 report. Oil ministry did not approve company's US$7.2 billion stake in deal with [[BP]].<ref>[http://indiawires.com/10404/news/national/steep-fall-in-gas-output-rs-7000cr-fine-on-reliance/ Steep Fall in Gas Output: Rs 7000cr Fine on Reliance | IndiaWires<!-- Bot generated title -->]</ref><ref>[http://www.newsbullet.in/india/34-more/36279-reddy-upset-over-transfer-from-oil-ministry ] {{webarchive |url=https://web.archive.org/web/20131029210205/http://www.newsbullet.in/india/34-more/36279-reddy-upset-over-transfer-from-oil-ministry |date=29 October 2013 }}</ref><ref>{{cite news| url=http://economictimes.indiatimes.com/news/politics/nation/jaipal-reddys-shift-from-oil-ministry-creates-a-political-storm/articleshow/17011133.cms | work=The Times Of India | title=Jaipal Reddy's shift from Oil Ministry creates a political storm | date=30 October 2012}}</ref> So [[Jaipal Reddy]] known for his honesty was shifted from oil ministry to the [[Ministry of Science and Technology (India)|Science and Technology ministry]] owing to pressure from [[Reliance Industries|Reliance group of Industries]].<ref>{{cite news| url=http://www.thehindu.com/news/national/pm-owes-an-explanation-on-shifting-jaipal-reddy-bjp-sp/article4043609.ece | location=Chennai, India | work=The Hindu | first=B. Muralidhar | last=Reddy | title=PM owes an explanation on shifting Jaipal Reddy: BJP, SP | date=29 October 2012}}</ref><ref>[http://articles.timesofindia.indiatimes.com/2012-10-29/india/34797158_1_jaipal-reddy-petroleum-ministry-science-and-technology ]{{dead link|date=February 2014}}</ref><ref>[http://newindianexpress.com/nation/CAG-audit-of-Krishna-Godavari-D6-gas-block-resumes/2013/05/11/article1584583.ece CAG audit of Krishna-Godavari D6 gas block resumes - The New Indian Express<!-- Bot generated title -->]</ref> RIL allowed the CAG to begin the audit in April this year<!-- which year?? Mention the year!! --> after stalling it for a year. But unresolved issues could stall audit of KG Basin again.<ref>[http://www.dnaindia.com/india/1834793/report-dna-special-unresolved-issues-could-stall-audit-of-kg-basin-again dna special: Unresolved issues could stall audit of KG Basin, again | Latest News & Updates at DNAIndia.com<!-- Bot generated title -->]</ref> Then Reliance appointed Defence Secretary [[Shashi Kant Sharma|Shashikant Sharma]] as new CAG to audit KG Basin, said [[Prashant Bhushan]].<ref>[http://www.business-standard.com/article/current-affairs/new-cag-is-a-weak-bureaucrat-says-prashant-bhushan-113052100905_1.html Bhushan questions new CAG selection | Business Standard<!-- Bot generated title -->]</ref> In KG D-6, most of the cost had been recovered by the private player and the increase in price would only go as profit. About 90% of receipts from K-G D-6 were so far booked as expenditure and in the remaining 10%, only 1% was paid to the government and rest 9% went to the operator as profit.<ref>{{cite news| url=http://timesofindia.indiatimes.com/business/india-business/CAG-may-look-into-likely-profit-for-RIL-from-gas-price-hike/articleshow/20869584.cms | work=The Times Of India | title=CAG may look into likely profit for RIL from gas price hike - The Times of India | date=2 July 2013}}</ref>

== List of Comptroller and Auditors General of India ==
{| class="wikitable"
|-
! No.
! Comptroller and Auditor General of India
! Year tenure began
! Year tenure ended
|-
| 1
| [[V. Narahari Rao]]
| 1949
| 1954
|-
| 2
| [[A. K. Chanda]]
| 1954
| 1960
|-
| 3
| [[A. K. Roy]]
| 1960
| 1966
|-
| 4
| [[S. Ranganathan]]
| 1966
| 1972
|-
| 5
| [[A. Bakshi]]
| 1972
| 1978
|-
| 6
| [[Gian Prakash]]
| 1978
| 1984
|-
| 7
| [[T. N. Chaturvedi]]
| 1984
| 1990
|-
| 8
| [[C. G. Somiah]]
| 1990
| 1996
|-
| 9
| [[V. K. Shunglu]]
| 1996
| 2002
|-
| 10
| [[VN Kaul]]
| 2002
| 2008
|-
| 11
| [[Vinod Rai]]
| 2008
| 2013
|-
| 12
| [[Shashi Kant Sharma]]
| 2013
| Incumbent (6 years tenure or 65 years of age, whichever is earlier
|}
Source:<ref name="Former CAG">{{cite web | url=http://saiindia.gov.in/english/home/about_us/former.html | title=Former CAG | accessdate=9 May 2012}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{cite web | title = Official website of the Comptroller and Auditor General of India | url = http://cag.gov.in/ | accessdate = 19 January 2009}}
* [http://www.indiaenvironmentportal.org.in/category/publisher/comptroller-and-auditor-general-india Reports by Comptroller and Auditor General of India]

[[Category:Comptrollers in India]]
[[Category:Government audit]]