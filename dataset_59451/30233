{{Infobox coin
| Denomination          = Buffalo nickel
| Country               = [[United States]]
| Value                  = 5 cents (.05
| Unit                  = [[United States dollar|US dollars]])
| Mass                  = [[Shield nickel#use of Ni|5.000]]
| Diameter_inch         = 0.8350
| Diameter              = 21.21
| Mint marks             = [[Denver Mint|D]], [[San Francisco Mint|S]].  Centered under "FIVE CENTS" on the reverse.  [[Philadelphia Mint]] specimens lack mint mark.
| Edge                  = Plain
| Composition =
  {{plainlist |
* 75% [[copper]]
* 25% [[nickel]]
  }}
| Years of Minting      = 1913–1938
| Catalog Number        = 
| Obverse               = {{Css Image Crop|Image = NNC-US-1913-5C-Buffalo Nickel (TyI-mound).jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 3|Location = center|Description=}}
| Obverse Design        = Right profile of an American Indian
| Obverse Designer      = [[James Earle Fraser (sculptor)|James Earle Fraser]]
| Obverse Design Date   = 1913
| Obverse Discontinued  = 1938
| Reverse               = {{Css Image Crop|Image = NNC-US-1913-5C-Buffalo Nickel (TyI-mound).jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center|Description=}}
| Reverse Design        = An [[American bison]]
| Reverse Designer      = [[James Earle Fraser (sculptor)|James Earle Fraser]]
| Reverse Design Date   = 1913
| Reverse Discontinued  = 1913
| Reverse2              = {{Css Image Crop|Image = NNC-US-1913-5C-Buffalo Nickel (TyII-line).jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center|Description=}}
| Reverse2 Designer     = [[James Earle Fraser (sculptor)|James Earle Fraser]]
| Reverse2 Design Date  = 1913
| Reverse2 Discontinued = 1938
}}

The '''Buffalo nickel''' or '''Indian Head nickel''' is a [[copper-nickel]] [[Nickel (United States coin)|five-cent piece]] that was struck by the [[United States Mint]] from 1913 to 1938. It was designed by [[Sculpture|sculptor]] [[James Earle Fraser (sculptor)|James Earle Fraser]].

As part of a drive to beautify the coinage, five denominations of US coins had received new designs between 1907 and 1909. In 1911, [[Presidency of William Howard Taft|Taft administration]] officials decided to replace [[Charles E. Barber]]'s [[Liberty head nickel|Liberty Head design]] for the nickel, and commissioned Fraser to do the work. They were impressed by Fraser's designs showing a [[Indigenous peoples of the Americas|Native American]] and an [[American bison]]. The designs were approved in 1912, but were delayed several months because of objections from the Hobbs Manufacturing Company, which made mechanisms to detect [[slug (coin)|slugs]] in nickel-operated machines. The company was not satisfied by changes made in the coin by Fraser, and in February 1913, [[United States Secretary of the Treasury|Treasury Secretary]] [[Franklin MacVeagh]] decided to issue the coins despite the objections.

Despite attempts by the Mint to adjust the design, the coins proved to strike indistinctly, and to be subject to wear—the dates were easily worn away in circulation. In 1938, after the expiration of the minimum 25-year period during which the design could not be replaced without congressional authorization, it was replaced by the [[Jefferson nickel]], designed by [[Felix Schlag]]. Fraser's design is admired today, and has been used on commemorative coins and the gold [[American Buffalo (coin)|American Buffalo]] series.

== Background ==

In 1883, the [[Liberty Head nickel]] was issued, featuring designs by Mint Engraver [[Charles E. Barber]]. After the coin was released, it was modified to add the word "CENTS" to the reverse because the similarity in size with the [[half eagle]] allowed criminals to gild the new nickels and pass them as five dollar coins.{{sfn|Breen|1988|pp=252–253}}  An Act of Congress, passed into law on September 26, 1890 required that coinage designs not be changed until they had been in use 25&nbsp;years, unless Congress authorized the change.{{sfn|Bowers|2007|p=149}}  The act made the current five-cent piece and [[Morgan dollar|silver dollar]] exceptions to the twenty-five year rule; they were made eligible for immediate redesign.{{sfn|Richardson|1891|pp=806–807|loc=26 Stat L. 484, amendment to R.S. §3510}} However, the Mint continued to strike the Liberty Head nickel in large numbers through the first decade of the 20th century.{{sfn|Breen|1988|p=255}}

President [[Theodore Roosevelt]] in 1904 expressed his dissatisfaction with the artistic state of the American coinage,{{sfn|Breen|1988|p=573}} and hoped to hire sculptor [[Augustus Saint-Gaudens]] to redesign all the coins. Constrained by the 1890 act, the Mint only hired Saint-Gaudens to redesign the cent and the four gold pieces. Saint-Gaudens, before his 1907 death, designed the [[eagle (United States coin)|eagle]] and [[double eagle]], which entered circulation that year; the cent, [[quarter eagle]], and half eagle were designed by other artists and released into circulation by 1909. By that time, the Liberty Head nickel had been in circulation for more than 25&nbsp;years, and was eligible for redesign regardless of the special provision. In 1909, Mint Director [[Frank Leach]] instructed Barber to make pattern coins for new nickels. Most of these coins featured the first president, [[George Washington]]. The press found out about the pieces, and speculated they would be released into circulation by the end of the year. The Mint received orders from banks in anticipation of the "Washington nickel".{{sfn|Burdette|2007|pp=93–97}}  However, the project was discontinued when Leach left office on November 1, 1909, to be replaced by [[Abram Andrew]].{{sfn|Burdette|2007|pp=93–97}}

Andrew was dissatisfied with the just-issued [[Lincoln cent]], and considered seeking congressional authorization to replace the cent with a design by sculptor [[James Earle Fraser (sculptor)|James Earle Fraser]]. While the change in the cent did not occur, according to numismatic historian Roger Burdette, "Fraser's enthusiasm eventually led to adoption of the Buffalo nickel in December 1912".{{sfn|Burdette|2007|p=98}}

== Inception ==

=== New design ===

[[File:Franklin MacVeagh (ca. 1918).jpg|thumb|upright|left|Franklin MacVeagh, Secretary of the Treasury under Taft. A letter to him by his son may have been the genesis of the Buffalo nickel.]]

On May 4, 1911, Eames MacVeagh, son of [[United States Secretary of the Treasury|Treasury Secretary]] [[Franklin MacVeagh]] wrote to his father:

{{quote
| A little matter that seems to have been overlooked by all of you is the opportunity to beautify the design of the nickel or five cent piece during your administration, and it seems to me that it would be a permanent souvenir of a most attractive sort. As possibly you are aware, it is the only coin the design of which you can change during your administration, as I believe there is a law to the effect that the designs must not be changed oftener than every twenty-five years. I should think also it might be the coin of which the greatest numbers are in circulation.{{sfn|Taxay|1983|p=340}}
}}

Soon after the MacVeagh letter, Andrew announced that the Mint would be soliciting new designs for the nickel. Fraser, who had been an assistant to Saint-Gaudens, approached the Mint, and rapidly produced concepts and designs. The new Mint director, George Roberts, who had replaced Andrew, initially favored a design featuring assassinated President [[Abraham Lincoln]], but Fraser soon developed a design featuring a Native American on one side and a bison on the other. Andrew and Roberts recommended Fraser to MacVeagh, and in July 1911, the Secretary approved hiring Fraser to design a new nickel. Official approval was slow in coming; it was not until January 1912 that MacVeagh asked Roberts to inform Fraser that he had been commissioned.{{sfn|Taxay|1983|pp=340–342}}  MacVeagh wrote, "Tell him that of the three sketches which he submitted we would like to use the sketch of the head of the Indian and the sketch of the buffalo."{{sfn|Burdette|2007|p=172}}  Roberts transmitted the news, then followed up with a long list of instructions to the sculptor, in which he noted, "The motto, 'In God We Trust', is not required upon this coin and I presume we are agreed that nothing should be upon it that is not required."{{sfn|Burdette|2007|p=173}}  Fraser completed the models by June 1912, and prepared coin-size [[electrotyping|electrotypes]]. He brought the models and electrotypes to Washington on July 10, where they met with the enthusiastic agreement of Secretary MacVeagh.{{sfn|Burdette|2007|p=174}}

=== Hobbs affair ===

[[File:Roberts medal.png|thumb|right|Mint Director George E. Roberts (shown on his Mint medal) did his best to bring the nickel to fruition despite the conflict between Fraser and Hobbs.]]

In July 1912, word of the new design became publicly known, and coin-operated machine manufacturers sought information. Replying to the inquiries, MacVeagh wrote that there would be no change in the diameter, thickness, or weight of the nickel. This satisfied most firms. However, Clarence Hobbs of the Hobbs Manufacturing Company, of [[Worcester, Massachusetts]] requested further information. According to Hobbs, his firm was the manufacturer of a device which would detect counterfeit nickels inserted into vending machines with complete accuracy.{{sfn|Burdette|2007|pp=181–183}}  Discussions continued for most of the rest of 1912, with Hobbs demanding various changes to the design, to which the artist was reluctant to agree. When in December 1912, the Hobbs Company submitted a modified design for the nickel, MacVeagh strongly opposed it. On December 18, Roberts officially approved Fraser's design, and the sculptor was authorized to complete and perfect the design, after which he would be paid $2,500 ([[United States dollar|US$]]{{formatnum:{{Inflation|US|2500|1912|r=-2}}}} with inflation{{Inflation-fn|US}}) for his work.{{sfn|Burdette|2007|pp=193–194}}

On January 7, 1913, Fraser's approved design was used to strike experimental pieces; the sculptor later wrote that he remembered several of the workmen commenting that the new piece struck more easily than the old. Afterwards, Roberts asked Fraser if the Hobbs Company was content with the design. The sculptor told the Mint director that the firm wanted changes made, and Fraser agreed to meet with them further. Over the following two weeks, Fraser worked with George Reith, the Hobbs Company's mechanic who had invented the anti-slug device, in an attempt to satisfy the firm's concerns. On January 20, Fraser wired the Mint from his studio in New York, announcing that he was submitting a modified design, and explained that the delay was "caused by working with inventor until he was satisfied".{{sfn|Burdette|2007|pp=194–196}}  The next day, [[Philadelphia Mint]] Superintendent John Landis sent Roberts a sample striking of the revised design, stating, "the only change is in the border, which has been made round and true".{{sfn|Burdette|2007|p=196}}

[[File:Clarence W. Hobbs (ca. 1919).jpg|thumb|upright|left||Clarence W. Hobbs. Objections by his firm delayed the Buffalo nickel for months.]]

Despite the apparent agreement, the Hobbs Company continued to interpose objections. Engraver Barber was asked for his view; he stated that Reith, who had attended the trial striking, had been given all the time and facilities he had asked for in testing the new pieces, and the mechanic had pronounced himself satisfied.{{sfn|Burdette|2007|pp=200–201}}  Hobbs Company agent C. U. Carpenter suggested that Reith had been intimidated by the preparations that had already gone into the issue of the modified nickel, "and, instead of pointing out clearly just what the situation demanded, agreed to adapt our device to the coin more readily that {{sic}} he was warranted in doing".{{sfn|Burdette|2007|p=201}}  On February 3, Hobbs sent Roberts a lengthy list of changes that he wanted in the coin, and the sculptor was required to attend a conference with Hobbs and Reith.{{sfn|Taxay|1983|p=345}}  On the fifth, following the conference, which ended with no agreement, Fraser sent MacVeagh a ten-page letter, complaining that his time was being wasted by the Hobbs Company, and appealing to the Secretary to bring the situation to a close.{{sfn|Burdette|2007|p=204}}  MacVeagh agreed to hold a meeting at his office in Washington on February 14. When the Hobbs Company requested permission to bring a lawyer, Fraser announced he would be doing the same. The Hobbs Company sought letters of support from the business community, with little success; Fraser's efforts to secure support from artists for his position were more fruitful.{{sfn|Burdette|2007|p=205}}  Barber prepared patterns showing what the nickel would look like if the changes demanded by Hobbs were made. MacVeagh conducted the meeting much like a legal hearing, and issued a letter the following day.{{sfn|Burdette|2007|p=206}}

The Secretary noted that no other firm had complained, that the Hobbs mechanism had not been widely sold, and that the changes demanded—a clear space around the rim and the flattening of the Indian's cheekbone—would affect the artistic merit of the piece.

{{quote
| It is of course true that only the most serious business considerations should stand in the way of the improvement of the coinage, and this particular coin has great claims of its own, because of its special quality. If we should stop new coinage—which is always allowed every twenty-five years—for any commercial obstacles less than imperative, we should have to abandon a worthy coinage altogether. This would be a most serious handicap to the art of the Nation, for scarcely any form of art is more influential than an artistic coin, where the coin is widely circulated.

You will please, therefore, proceed with the coinage of the new nickel.{{sfn|Burdette|2007|p=207}}
}}

After he issued his decision, MacVeagh learned that the [[Port Authority Trans-Hudson|Hudson & Manhattan Railroad Company]], which Hobbs claimed had enthusiastically received his device, was actually removing it from service as unsatisfactory. The Secretary's decision did not end the Hobbs Company efforts, as the firm appealed to President [[William Howard Taft|Taft]]. With only two weeks remaining in his term, the President was not minded to stop the new nickel (production of which had started on February 18) and MacVeagh wrote to Taft's secretary, [[Charles D. Hilles]], "Certainly Hobbs got all the time and attention out of this administration that any administration could afford to give to one manufacturing corporation."{{sfn|Burdette|2007|pp=208–209}} Numismatic historian and coin dealer [[Q. David Bowers]] describes the Hobbs matter as "much ado about nothing from a company whose devices did not work well even with the Liberty Head nickels".{{sfn|Bowers|2007|p=37}}

== Release and production ==

[[File:Buffalonickel.jpg|thumb|200px|A dateless coin, showing the effects of circulation on the Buffalo nickel. This is a Type II reverse.]]

The first coins to be distributed were given out on February 22, 1913, when Taft presided at groundbreaking ceremonies for the [[National American Indian Memorial]] at [[Fort Wadsworth]], [[Staten Island, New York]]. The memorial, a project of department store magnate [[Rodman Wanamaker]], was never built, and today the site is occupied by an [[abutment]] for the [[Verrazano-Narrows Bridge]]. Forty nickels were sent by the Mint for the ceremony; most were distributed to the Native American chiefs who participated.{{sfn|Burdette|2007|pp=211–213}}  Payment for Fraser's work was approved on March 3, 1913, the final full day of the Taft administration. In addition to the $2,500 agreed upon, Fraser received $666.15 ([[United States dollar|US$]]{{formatnum:{{Inflation|US|666.15|1913|r=-2}}}} with inflation{{Inflation-fn|US}}) for extra work and expenses through February 14.{{sfn|Burdette|2007|p=210}}

The coins were officially released to circulation on March 4, 1913, and quickly gained positive comments as depicting truly American themes.{{sfn|Lange|2006|p=149}}  However, ''[[The New York Times]]'' stated in an editorial that "The new 'nickel' is a striking example of what a coin intended for wide circulation should not be&nbsp;...<nowiki>[it]</nowiki> is not pleasing to look at when new and shiny, and will be an abomination when old and dull."{{sfn|Burdette|2007|p=214}}  ''[[The Numismatist]]'', in March and May 1913 editorials, gave the new coin a lukewarm review, suggesting that the Indian's head be reduced in size and the bison be eliminated from the reverse.{{sfn|Bowers|2007|pp=46–47}}

With the coin now in production, Barber monitored the rate at which dies were expended, as it was the responsibility of his Engraver's Department to supply all three mints with working dies. On March 11, 1913, he wrote to Landis that the dies were being used up three times faster than with the Liberty Head nickel. His department was straining to produce enough new dies to meet production. In addition, the date and denomination were the points on the coin most subject to wear, and Landis feared the value on the coin would be worn away.{{sfn|Burdette|2007|pp=252–253}}  Barber made proposed revisions, which Fraser approved after being sent samples.{{sfn|Taxay|1983|p=346}}  These changes enlarged the legend "FIVE CENTS" and changed the ground on which the bison stands from a hill to flat ground.{{sfn|Burdette|2007|p=253}}  According to data compiled by numismatic historian David Lange from the [[United States National Archives|National Archives]], the changes to what are known as Type II nickels (with the originals Type I) actually decreased the die life.{{sfn|Bowers|2007|p=45}}  The new Treasury Secretary, [[William Gibbs McAdoo|William G. McAdoo]], wanted further changes in the coin, but Fraser had moved on to other projects and was uninterested in revisiting the nickel.{{sfn|Burdette|2007|p=255}}  The thickness of the numerals in the date was gradually increased, making them more durable; however the problem was never addressed with complete success, and even many later-date Buffalo nickels have the date worn away.{{sfn|Lange|2006|p=149}}

The Buffalo nickel saw minor changes to the design in 1916.{{sfn|Burdette|2007|p=287}}  The word "LIBERTY" was given more emphasis and moved slightly; however many Denver and San Francisco issues of the 1920s exhibit weak striking of the word, the Denver issue of 1926 especially; Bowers questions whether any change was made to the portrait of the Indian,{{sfn|Bowers|2007|p=46}} though [[Walter Breen]] in his reference work on United States coins states that Barber made the Indian's nose slightly longer. According to Breen, however, none of these modifications helped, with the coin rarely found well-struck and with the design subject to considerable wear throughout the remainder of its run.{{sfn|Breen|1988|p=257}}  The bison's horn and tail also posed striking problems, again with the Denver and San Francisco issues of the 1920s in general, and 1926-D in particular, showing the greatest propensity for these deficiencies.

The piece was struck by the tens of millions, at all three mints ([[Philadelphia Mint|Philadelphia]], [[Denver Mint|Denver]] and [[San Francisco Mint|San Francisco]]), through the remainder of the 1910s. In 1921, a recession began, and no nickels at all were struck the following year.{{sfn|Bowers|2007|p=49}}  The low mintage for the series was the 1926-S, at 970,000&nbsp;— the only date-mint combination with a mintage of less than 1 million.  The second lowest mintage for the series came with the 1931 nickel struck at the [[San Francisco Mint]]. The 1931-S was minted in a quantity of 194,000 early in the year. There was no need for more to be struck, but Acting Mint Director [[Mary Margaret O'Reilly]] asked the San Francisco Mint to strike more so that the pieces would not be hoarded.   Using materials on hand, including the melting down of worn-out nickels, San Francisco found enough metal to strike 1,000,000&nbsp;more pieces. Large quantities were saved in the hope they would become valuable, and the coin is not particularly rare today despite the low mintage.{{sfn|Bowers|2007|p=115}}

A well-known variety in the series is the 1937–D "three-legged" nickel, on which one of the buffalo's legs is missing. Breen relates that this variety was caused by a pressman, Mr. Young, at the [[Denver Mint]], who in seeking to remove marks from a reverse die (caused by the dies making contact with each other), accidentally removed or greatly weakened one of the animal's legs.  By the time Mint inspectors discovered and condemned the die, thousands of pieces had been struck and mixed with other coins.{{sfn|Breen|1988|p=257}}

Another variety is the 1938-D/S, caused by dies bearing an "S" mintmark being repunched with a "D" and used to strike coins at Denver. While the actual course of events is uncertain, Bowers is convinced that the variety was created because Buffalo nickel dies intended for the San Francisco mint were repunched with the "D" and sent to Denver so they would not be wasted—no San Francisco Buffalo nickels were struck in 1938, but they were produced at Denver, and it was already known that a new design would be introduced. The 1938-D/S was the first repunched mintmark of any US coin to be discovered, causing great excitement among numismatists when the variety came to light in 1962.{{sfn|Bowers|2007|pp=125–126}}

When the Buffalo nickel had been in circulation for the minimum 25 years, it was replaced with little discussion or protest. The problems of die life and weak striking had never been solved, and Mint officials advocated its replacement. In January 1938, the Mint announced an open competition for a new nickel design, to feature early President [[Thomas Jefferson]] on the obverse, and Jefferson's home, [[Monticello]] on the reverse.{{sfn|Bowers|2007|pp=127–128}}  In April, [[Felix Schlag]] was announced as the winner.{{sfn|Bowers|2007|p=129}}  The last Buffalo nickels were struck in April 1938, at the Denver Mint, the only mint to strike them that year. On October 3, 1938, production of the [[Jefferson nickel]] began, and they were released into circulation on November 15.{{sfn|Bowers|2007|pp=141–142}}

== Design, models, and name controversy ==

[[File:Irontail1912.jpeg|thumb|upright|[[Iron Tail|Chief Iron Tail]], [[Oglala Lakota]], circa 1912]]

In a 1947 radio interview, Fraser discussed his design:

{{quote
| Well, when I was asked to do a nickel, I felt I wanted to do something totally American—a coin that could not be mistaken for any other country's coin. It occurred to me that the buffalo, as part of our western background, was 100% American, and that our [[North American Indian]] fitted into the picture perfectly.{{sfn|Burdette|2007|p=224}}
}}

The visage of the Indian which dominates Fraser's obverse design was a composite of several Native Americans. Breen noted (before the advent of the [[Sacagawea dollar]]) that Fraser's design was the second and last US coin design to feature a realistic portrait of an Indian, after [[Bela Pratt]]'s [[Indian Head gold pieces|1908 design]] for the [[half eagle]] and [[quarter eagle]].{{sfn|Breen|1988|p=256}}

The identity of the Indians whom Fraser used as models is somewhat uncertain, as Fraser told various and not always consistent stories during the forty years he lived after designing the nickel. In December 1913, he wrote to Mint Director Roberts that "<nowiki>[b]</nowiki>efore the nickel was made I had done several portraits of Indians, among them [[Iron Tail]], [[Two Moons]], and one or two others, and probably got characteristics from those men in the head on the coins, but my purpose was not to make a portrait but a type."{{sfn|Bowers|2007|pp=38–39}}

[[File:2001buffalo.JPG|thumb|left|250px|Fraser's design was adapted for a 2001 commemorative [[Dollar coin (United States)|silver dollar]].  The mound is similar to the Type I reverse initially used in 1913.]]

By 1931, Two Guns White Calf, son of the last [[Blackfoot]] tribal chief, was capitalizing off his claim to be the model for the coin. To try to put an end to the claim, Fraser wrote that he had used three Indians for the piece, including "[[Iron Tail]], the best Indian head I can remember. The other one was Two Moons, the other I cannot recall."{{sfn|Bowers|2007|p=39}}  In 1938, Fraser stated that the three Indians had been "[[Iron Tail]], a Sioux, [[John Big Tree|Big Tree]], a Kiowa, and [[Two Moons]], a Cheyenne".{{sfn|Bowers|2007|p=39}}  Despite the sculptor's efforts, he (and the Mint) continued to receive inquiries about the identity of the Indian model until his 1953 death.{{sfn|Burdette|2007|p=219}}

Nevertheless, [[John Big Tree]], a [[Seneca nation|Seneca]], claimed to be a model for Fraser's coin, and made many public appearances as the "nickel Indian" until his 1967 death at the age of 92 (though he sometimes alleged he was over 100&nbsp;years of age). [[John Big Tree|Big Tree]] was identified as the model for the nickel in wire service reports about his death,{{sfn|Porterfield|1970|p=16}} and he had appeared in that capacity at the Texas Numismatic Association convention in 1966.{{sfn|Bowers|2007|p=39}}  After [[John Big Tree|Big Tree]]'s death, the Mint stated that he most likely was not one of the models for the nickel. There have been other claimants: in 1964, Montana Senator [[Mike Mansfield]] wrote to Mint Director [[Eva B. Adams]], enquiring if Sam Resurrection, a [[Choctaw]] was a model for the nickel. Adams wrote in reply, "According to our records, the portrait is a composite. There have been many claimants for this honor, all of whom are undoubtedly sincere in the belief that theirs is the one that adorns the nickel."{{sfn|Porterfield|1970|p=16}}

[[File:2006 American Buffalo Proof Reverse.jpg|thumb|upright|Reverse of the American Buffalo gold coins, struck beginning in 2006]]

According to Fraser, the animal that appears on the reverse is the [[American bison]] [[Black Diamond (buffalo)|Black Diamond]]. In an interview published in the ''[[New York Herald]]'' on January 27, 1913, Fraser was quoted as saying that the animal, which he did not name, was a "typical and shaggy specimen" which he found at the [[Bronx Zoo]].{{sfn|Burdette|2007|p=223}}  Fraser later wrote that the model "was not a plains buffalo, but none other than Black Diamond, the contrariest animal in the Bronx Zoo. I stood for hours&nbsp;... He refused point blank to permit me to get side views of him, and stubbornly showed his front face most of the time."{{sfn|Burdette|2007|p=223}}  However, Black Diamond was never at the Bronx Zoo, but instead lived at the [[Central Park Zoo]] until he was sold and slaughtered in 1915. Black Diamond's mounted head is still extant, and has been exhibited at coin conventions.{{sfn|Bowers|2007|p=37}}  The placement of Black Diamond's horns differs considerably from that of the animal on the nickel, leading to doubts that Black Diamond was Fraser's model. One candidate cited by Bowers is Bronx, a bison who was for many years the herd leader of the bison at the Bronx Zoo.{{sfn|Bowers|2007|p=38}}

From its inception, the coin was referred to as the "Buffalo nickel", reflecting the American colloquialism for the North American bison. As the piece is 75%&nbsp;copper and 25%&nbsp;nickel, prominent numismatist Stuart Mosher objected to the nomenclature in the 1940s, writing that he was "uncertain why it is called a 'Buffalo nickel' although the name is preferable to 'Bison copper'". The numismatic publication with the greatest circulation, ''[[Coin World]]'', calls it an Indian head nickel, while [[R.S. Yeoman]]'s  ''[[A Guide Book of United States Coins|Red Book]]'' refers to it as an "Indian Head or Buffalo type".{{sfn|Bowers|2007|pp=41–42}}

In 2001, the design was adopted for use on a commemorative silver dollar.{{sfn|US Mint, 2001 American Buffalo Commemorative Coins}}  In 2006, the Mint began striking [[American Buffalo (coin)|American Buffalo]] gold bullion pieces, using a modification of Fraser's Type I design.{{sfn|US Mint Strikes First Pure Gold U.S. Coins}}

== See also ==

{{portal|Numismatics}}

* The [[Nickel Trophy]], an oversized Indian Head nickel awarded to winners of the (now defunct) annual football game between the [[North Dakota State Bison]] and the [[University of North Dakota]] [[Fighting Sioux]]
* [[Hobo nickel]], artistically carved nickels created during the [[Great Depression]]

== References ==

{{reflist|20em}}

'''Bibliography'''

* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 2007
  | title = A Guide Book of Buffalo and Jefferson Nickels
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-2008-4
  | ref = harv
  }}
* {{cite book
  | last = Breen
  | first = Walter
  | authorlink = Walter H. Breen
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York, N.Y.
  | isbn = 978-0-385-14207-6
  | ref = harv
  }}
* {{cite book
  | last = Burdette
  | first = Roger W.
  | year = 2007
  | title = Renaissance of American Coinage, 1909–1915
  | publisher = Seneca Mill Press
  | location = Great Falls, Va.
  | isbn = 978-0-9768986-2-7
  | ref = harv
  }}
* {{cite book
  | last = Lange
  | first = David W.
  | year = 2006
  | title = History of the United States Mint and its Coinage
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1972-9
  | ref = harv
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | year = 1983
  | origyear = 1966
  | title = The U.S. Mint and Coinage
  | edition = reprint
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York, N.Y.
  | isbn = 978-0-915262-68-7
  | ref = harv
  }}
* Van Ryzin, Robert R. "Which Indian Really Modeled? Numismatic News Feb. 6, 1990
* Van Ryzin, Robert R. Fascinating Facts, Mysteries & Myths About U.S. Coins, Krause Publications ISBN 978-1-4402-0650-4
'''Other sources'''

* {{cite news
  | last = Porterfield
  | first = Walden R.
  | date = March 3, 1970
  | title = The Billion Dollar Profile
  | newspaper = The Milwaukee Journal
  | page = 16
  | url = https://news.google.com/newspapers?id=rwwqAAAAIBAJ&sjid=VCgEAAAAIBAJ&dq=john%20big%20tree%20buffalo%20nickel&pg=7288%2C2086169
  | accessdate = November 21, 2010
  | ref = harv
  }}
* {{cite book
  | editor-last = Richardson
  | editor-first = William Allen
  | year = 1891
  | title = Supplement to the revised statutes of the United States
  | volume = 1
  | publisher = US Government Printing Office
  | location = Washington, D.C.
  | url = https://books.google.com/?id=Dcg4AAAAIAAJ
  | accessdate = February 13, 2012
  | ref = harv
  }}
* {{cite web
  | publisher = United States Mint
  | title = The United States Mint Historical Image Library
  | url = http://www.usmint.gov/about_the_mint/CoinLibrary/#buffalo
  | accessdate = November 21, 2010
  | ref = {{sfnRef|US Mint, 2001 American Buffalo Commemorative Coins}}
  }}
* {{cite web
  | publisher = United States Mint
  | date = June 20, 2006
  | title = United States Mint Strikes First Pure Gold U.S. Coins for Investors & Collectors
  | url = http://www.usmint.gov/pressroom/index.cfm?action=press_release&ID=674
  | accessdate = November 21, 2010
  | ref = {{sfnRef|US Mint Strikes First Pure Gold U.S. Coins}}
  }}

== External links ==

{{Commons category|Buffalo nickels}}
* [http://www.ngccoin.com/CoinDetail.aspx?ContentID=65 NGC Coin Encyclopedia for Buffalo Nickels]

{{Nickels}}
{{Coinage (United States coin)}}
{{Featured article}}

[[Category:1913 introductions]]
[[Category:Five-cent coins of the United States]]
[[Category:Native Americans in art]]
[[Category:Bison]]