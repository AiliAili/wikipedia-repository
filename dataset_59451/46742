{{Use dmy dates|date=June 2015}}
{{Use Australian English|date=June 2015}}
{{Infobox MP
| honorific-prefix =
| name = Fanny Brownbill
| honorific-suffix =
| image =
| alt = 
| constituency_MP = [[Electoral district of Geelong|Geelong]]
| parliament = Victorian
| majority = 
| predecessor = [[William Brownbill]]
| successor = [[Edward Montgomery (Australian politician)|Edward Montgomery]]
| term_start = 11 June 1938
| term_end = 10 October 1948
| birthname = Fanny Eileen Alford
| birth_date = {{birth date|1890|4|28|df=y}}
| birth_place = [[Modewarre]], [[Victoria (Australia)|Victoria]]
| death_date = {{death date and age|1948|10|10|1890|4|28|df=y}}
| death_place = [[Geelong]], [[Victoria (Australia)|Victoria]], [[Australia]]
| nationality = Australian
| spouse = [[William Brownbill]]
| party = [[Australian Labor Party]]
| relations =
| children =
| residence =
| alma_mater = 
| occupation = 
| profession =
| religion = [[Church of Christ]]
| signature =
| signature_alt =
| footnotes =
}}
'''Fanny Eileen Brownbill''' (28 April 1890 – 10 October 1948) was an Australian state politician, serving as the [[Australian Labor Party|Labor Party]] Member for [[Geelong]], [[Victoria (Australia)|Victoria]], serving from 1938 until her death in 1948. Brownbill was the first woman to win a seat for [[Australian Labor Party|Labor]] in Victoria.

== Early life and career ==
Brownbill was born Fanny Alford, the youngest of seven children, in Modewarre, [[Victoria (Australia)|Victoria]], to an Australian father, James Alford, and English mother, Ann Abbot. In 1913, she became a housekeeper to William Brownbill, a baker and widower with four children. They married in 1920, and that same year, William entered Parliament as the [[Australian Labor Party|Labor]] Member for [[Electoral district of Geelong|Geelong]] in the Legislative Assembly.<ref>{{cite web|last=Thomas|first=Joanne|url=http://adb.anu.edu.au/biography/brownbill-fanny-eileen-9602|title=Brownbill, Fanny Eileen (1890–1948)|work=Australian National University|publisher=Australian Dictionary of Biography|accessdate=25 October 2011}}</ref> He served again after an electoral defeat, in total serving 15 years as the MLA for Geelong.<ref>{{cite web|last=Browne|first=Geoff|url=http://www.parliament.vic.gov.au/re-member/bioregfull.cfm?mid=931|title=Brownbill, William|work=Library Committee, Parliament of Victoria|publisher=Victorian Government Printing Office|accessdate=25 October 2011}}</ref>

Upon William's death while in office in 1938, Fanny contested the seat,<ref>{{cite book|last=FitzHerbert|first=Margaret|title=Liberal women: Federation--1949|year=2004|publisher=Federation Press|pages=305|url=https://books.google.com/books?id=DUCCAxmw4yMC&printsec=frontcover&dq=inauthor:%22Margaret+FitzHerbert%22&hl=en&ei=9TSmTtPKIIOtiQf_xMCzDg&sa=X&oi=book_rbesult&ct=result&resnum=1&ved=0CC0Q6AEwAA#v=onepage&q&f=false}}</ref> winning comfortably, and became the first [[Australian Labor Party|Labor]] woman to win a parliamentary seat in [[Victoria (Australia)|Victoria]],<ref>{{cite news |url=http://nla.gov.au/nla.news-article22680990 |title=STATE PARLIAMENT LOSES ONLY WOMAN MEMBER. |newspaper=[[The Argus (Australia)|The Argus (Melbourne, Vic. : 1848 - 1956)]] |location=Melbourne, Vic. |date=11 October 1948 |accessdate=28 October 2011 |page=7 |publisher=National Library of Australia}}</ref> the first woman elected from a non-metropolitan area, and the second [[Australian Labor Party|Labor]] woman to do so in [[Australia]]. She served for ten years until 1948, when she died suffering from heart disease.<ref name="argus1">{{cite news |url=http://nla.gov.au/nla.news-article11161471 |title=WOMEN IN POLITICS. |newspaper=[[The Argus (Australia)|The Argus (Melbourne, Vic. : 1848 - 1956)]] |location=Melbourne, Vic. |date=1 June 1938 |accessdate=28 October 2011 |page=6 |publisher=National Library of Australia}}</ref>

Brownbill's particular political passions were the welfare of women, children and the aged. In her maiden speech, she spoke of a mother's life of sacrifice, and urged the Railways Commissioners to allow perambulators on the railways.<ref>[http://www.parliament.vic.gov.au/re-member/bioregspeech.cfm?mid=930 Fanny Brownbill's Inaugural Speech to Parliament], Parliament of Victoria.</ref> One of her many achievements was the establishment of Grace McKellar House, a nursing home for the elderly, which is still operating today.

Her by-election was, at times, dramatically fought, with statements from the leader of the [[United Australia Party]], the conservative party of the day, claiming that women were not suited to politics. From ''[[The Argus (Australia)|The Argus]]'' newspaper: "''Speaking in support of Mr R. H. Weddell, the endorsed U.A.P. candidate for the Geelong seat, at Geelong West on Monday, the Federal Treasurer (Mr. Casey) expressed doubt whether there was a place for women in politics...'If there is a place for women in politics,' added Mr Casey, 'it is probably in the Legislative Council or in the Senate, where things are quieter and the old gentlemen occasionally drowse into their beards. My advice is, however good a woman may be, to stick to a man for what has always been recognised in the past to be a man's job." – ''[[The Argus (Australia)|The Argus]]'', 1 June 1938.<ref name="argus1" />

Brownbill fought back, stating: "As for his remark that he doubted whether any woman, intellectually of otherwise, could stand up to men of equal ability, it is so audacious and conceited that it almost takes my breath away. What a high opinion Mr Casey has of men, and what a low one of women."<ref name="argus1" />

From 1943 to 1948 she was the sole female Member of Parliament after Country/Independent [[Ivy Weber]] resigned. After Brownbill's death in 1948, she was described by Acting Premier John McDonald as "...the embodiment of tolerance. Her charming personality had endeared her to all in the chamber, irrespective of the party." In addition, both Mr William Galvin <ref>{{cite web|last=Browne|first=Geoff|url=http://www.parliament.vic.gov.au/re-member/bioregfull.cfm?mid=1082|title=Galvin, (Leslie) William|work=Library Committee, Parliament of Victoria|publisher=Victorian Government Printing Office|accessdate=25 October 2011}}</ref> and Mr Trevor Oldham <ref>{{cite web|last=Browne|first=Geoff|url=http://www.parliament.vic.gov.au/re-member/bioregfull.cfm?mid=1357|title=Oldham, Trevor|work=Library Committee, Parliament of Victoria|publisher=Victorian Government Printing Office|accessdate=25 October 2011}}</ref>  gave glowing tributes to Mrs Brownbill <ref>{{cite web|title=Obituary|url=https://news.google.com/newspapers?nid=1300&dat=19481020&id=X9FVAAAAIBAJ&sjid=6LwDAAAAIBAJ&pg=1852,2431552|work=The Age|accessdate=25 October 2011}}</ref>  It was nearly 20 years before another woman was elected to the [[Victorian Legislative Assembly|Victorian Parliament]], when [[Dorothy Goble]] won the seat of Mitcham in 1967.<ref>{{cite web|last=Browne|first=Geoff|url=http://www.parliament.vic.gov.au/re-member/bioregfull.cfm?mid=1098|title=Goble, Dorothy Ada|work=Library Committee, Parliament of Victoria|publisher=Victorian Government Printing Office|accessdate=25 October 2011}}</ref>

== References ==
{{reflist}}

{{s-start}}
{{s-par|au-vic-la}}
{{succession box | title=Member for [[Electoral district of Geelong|Geelong]] | before=[[William Brownbill]] | years=1938–1948 | after = [[Edward Montgomery (Australian politician)|Edward Montgomery]]}}
{{s-end}}

{{DEFAULTSORT:Brownbill, Fanny Eileen}}
[[Category:1890 births]]
[[Category:1948 deaths]]
[[Category:Members of the Victorian Legislative Assembly]]
[[Category:Australian Labor Party members of the Parliament of Victoria]]
[[Category:20th-century Australian politicians]]