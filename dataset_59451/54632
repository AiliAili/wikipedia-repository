{{for|the 1975 Spanish film|The Regent's Wife}}
{{infobox book |
| name         = La Regenta
| author       = Leopoldo Alas "Clarín"
| country      = Spain
| language     = Spanish
| genre        = Realist novel
| publisher    = (copyright expired)
| release_date = 1885
| media_type   = Print
}}
[[Image:La Regenta.jpg|right|thumb|Statue dedicated to La Regenta in Oviedo]]

'''''La Regenta''''' is a [[Literary realism|realist]] [[novel]] by Spanish author [[Leopoldo Alas y Ureña]], also known as Clarín, published in 1884 and 1885.

==Plot==

The story is set in ''Vetusta'' ("Vetusto" in Spanish stands for "antiquated", "extremely old", a provincial capital city, very identifiable with [[Oviedo]], capital of [[Asturias]] - especially since it is said that two monks, Nolan and John, founded the city, this being Oviedo's mythical genesis), where the main character of the work, Ana Ozores "La Regenta", marries the former prime magistrate of the city, Víctor Quintanar, a kind but fussy man much older than she. Feeling sentimentally abandoned, Ana lets herself be courted by the province casanova, Álvaro Mesía. To complete the circle, D. Fermín de Pas (Ana's confessor and [[canon (priest)|canon]] in the [[cathedral]] of Vetusta) also falls in love with her and becomes Mesía's unmentionable rival. A great panorama of secondary characters, portrayed by Clarín with merciless irony, completes the human landscape of the novel.

==Interpretation==

The author uses the city of Vetusta as a symbol of vulgarity, lack of culture and hypocrisy. On the other hand, Ana incarnates the tortured ideal that perishes progressively before a hypocritical society. With these forces in tension, the [[Asturian people|Asturian]] writer narrates a cruel story of Spanish provincial life in the days of the [[Spain under the Restoration|Restoration]].

{{Portal|Victorian era}}

==Adaptations==

This novel has been adapted to Film and TV, written and directed by Fernando Méndez-Leite and premiered in 1995.
Also a Musical will be premiered at the Campoamor Theatre, in Oviedo, hometown of La Regenta; [http://www.laregentaelmusical.com/ "La Regenta El Musical"] on July 2012, written and composed by [http://sigfridocecchini.com/Sigfrido_Cecchini.html Sigfrido Cecchini], with the stage direction of [http://www.emiliosagi.com/ Emilio Sagi].

==External links==
{{es}} Full text of the novel can be found [http://www.cervantesvirtual.com/servlet/SirveObras/12473959873481628265679/index.htm here]
* [http://www.polyglotproject.com/books/Spanish/la_regenta Full text in Spanish] with English translation
* http://www.laregentaelmusical.com/
* http://sigfridocecchini.com/Sigfrido_Cecchini.html

{{DEFAULTSORT:Regenta, La}}
[[Category:1885 novels]]
[[Category:Spanish novels]]
[[Category:Adultery in novels]]
[[Category:Asturian culture]]
[[Category:Oviedo]]
[[Category:Asturias in fiction]]
[[Category:Novels set in Spain]]


{{Asturias-stub}}
{{1880s-novel-stub}}