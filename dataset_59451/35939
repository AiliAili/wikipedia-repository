{{EngvarB|date=August 2013}}
{{Use dmy dates|date=August 2013}}
{{Infobox cricketer
| name              = Haseeb Ahsan
| image             = Haseeb Ahsan.jpg
|image_size         = 150px
| imagealt          = Black and white portrait of Haseeb Ahsan
| birth_date        = {{birth date|1939|7|15|df=y}}
| birth_place       = [[Peshawar]], [[North-West Frontier Province (1901–1955)|North-West Frontier Province]] (now [[Khyber Pakhtunkhwa]])<br/>[[Pakistan]]
| death_date        = {{death date and age|2013|3|8|1939|7|15|df=y}}
| death_place       = [[Karachi]], Pakistan
| batting           =  [[Glossary of cricket terms#R|Right-hand]] [[batsman]]
| bowling           = [[Glossary of cricket terms#R|Right-arm]] [[off spinner]]
| role              = [[Bowling (cricket)|Bowler]]
| international     = yes
| country           = Pakistan
| testdebutfor      = Pakistan
| testcap           = 25
| testdebutagainst  = West Indies
| testdebutdate     = 17 January
| testdebutyear     = 1958
| lasttestdate      = 2 February
| lasttestyear      = 1962
| lasttestfor       = Pakistan
| lasttestagainst   =  England
| columns           = 2
| column1           = [[Test cricket|Tests]]
| matches1          = 12
| runs1             = 61
| bat avg1          = 6.77
| 100s/50s1         = -/-
| top score1        = 14
| deliveries1       = 2835
| wickets1          = 27
| bowl avg1         = 49.25
| fivefor1          = 2
| tenfor1           = –
| best bowling1     = 6/202
| catches/stumpings1= 1/–
| column2           = [[First-class cricket|First-class]]
| matches2          = 49
| runs2             = 242
| bat avg2          = 5.62
| 100s/50s2         = –/–
| top score2        = 36
| deliveries2       = 8244
| wickets2          = 142
| bowl avg2         = 27.71
| fivefor2          = 13
| tenfor2           = 2
| best bowling2     = 8/23
| catches/stumpings2= 9/–
| source            = http://www.espncricinfo.com/ci/content/player/40380.html ESPNcricinfo
| date              = 26 August
| year              = 2013
}}

'''Haseeb Ahsan''' ({{lang-ur|'''حسيب احسن'''}}; 15 July 1939 – 8 March 2013) was a Pakistani [[cricket]]er who played 12 [[Test cricket|Tests]] for [[Pakistan cricket team|Pakistan]] between 1958 and 1962. He was born in [[Peshawar]], [[Khyber Pakhtunkhwa]]. A [[Glossary of cricket terms#R|right-arm]] [[off spinner]], he took 27 [[wicket]]s in Test cricket at an [[Bowling average|average]] of 49.25, including two [[Glossary of cricket terms#F|five-wicket haul]]s. During his [[First-class cricket|first-class]] career, he played 49 matches and took 142 wickets at an average of 27.71.<ref name="Profile">{{cite news| url=http://www.espncricinfo.com/ci/content/player/40380.html | title=Haseeb Ahsan |publisher=[[ESPNcricinfo]] | accessdate=1 December 2012 }}</ref> Former Pakistan cricketer [[Waqar Hasan]] said about him that he "was a fighter to the core and served Pakistan cricket with honour and dignity."<ref name="Dawn">{{cite news| url=http://dawn.com/2013/03/09/former-test-cricketer-haseeb-ahsan-dies/ | title=Former Test cricketer Haseeb Ahsan dies |work=[[Dawn (newspaper)|Dawn]] |date=8 March 2013 |accessdate=4 December 2014}}</ref>

Ahsan had conflicts with former Pakistan captain [[Javed Burki]]. A controversy regarding his bowling action resulted in the premature end of his international career when he was only 23. He worked as chief selector, team manager of Pakistan, and member of the [[1987 Cricket World Cup]] organising committee. He died in [[Karachi]] on 8 March 2013, aged 73.

==Cricketing career==
[[File:Jawaharlal Nehru Stadium Chennai panorama.jpg|thumb|right|300px|Ahsan achieved his best bowling performance at the [[Jawaharlal Nehru Stadium, Chennai|Nehru Stadium]].|alt=A panoramic view of a cricket ground]]
Ahsan played 49 [[First-class cricket|first-class]] matches for [[Pakistan national cricket team|Pakistan]], [[Karachi cricket teams|Karachi]], [[Pakistan International Airlines cricket team|Pakistan International Airlines]] (PIA), [[Rawalpindi cricket team|Rawalpindi]] [[Peshawar cricket team|Peshawar]] and other teams between 1955 and 1963.<ref>{{cite news| url=http://cricketarchive.com/Archive/Players/1/1062/all_teams.html | title=Teams Haseeb Ahsan played for |publisher=[[CricketArchive]] |accessdate=9 March 2013}}</ref> During his first-class career, he achieved five or more wickets in an [[innings]] on thirteen occasions, and ten or more wickets in a match two times.<ref name="Profile"/>

Ahsan made his first-class debut for North West Frontier Province and Bahawalpur, playing his only match of the season against the [[Marylebone Cricket Club]] in 1955–56.<ref name="FC">{{cite news| url=http://cricketarchive.com/Archive/Players/1/1062/First-Class_Matches.html | title=First-class matches played by Haseeb Ahsan (49)|publisher=CricketArchive | accessdate=9 March 2013}}</ref> He played three matches for Peshawar during 1956–57 with his best bowling figures came against Punjab B, taking eight for 76.<ref name="Seasons">{{cite news| url=http://cricketarchive.com/Archive/Players/1/1062/f_Bowling_by_Season.html | title=First-class bowling in each season by Haseeb Ahsan|publisher=CricketArchive | accessdate=9 March 2013}}</ref><ref>{{cite news| url=http://cricketarchive.com/Archive/Scorecards/22/22341.html | title=Peshawar v Punjab B – Quaid-e-Azam Trophy 1956/57 (North Zone) |publisher=CricketArchive | accessdate=9 March 2013}}</ref> In the next season, he was more effective with the ball, taking 43 wickets in nine matches.<ref name="FC"/><ref name="Seasons"/> In the same season, he achieved his best bowling figures in first-class cricket, taking eight for 23 against Punjab B.<ref>{{cite news| url=http://cricketarchive.com/Archive/Scorecards/22/22714.html | title=Peshawar v Punjab B – Quaid-e-Azam Trophy 1957/58 (North Zone) |publisher=CricketArchive | accessdate=9 March 2013}}</ref> During the season, Ahsan made his Test debut against the [[West Indies national cricket team|West Indies]] at the [[Kensington Oval]], in the same match in which [[Hanif Mohammad]] scored 337 runs; during the first match of the [[History of cricket in the West Indies from 1945–46 to 1970#Pakistan 1957–58|1958 series between the teams]], Ahsan conceded 84 runs in 21 overs without taking a wicket.<ref name="Testdebut">{{cite news| url=http://www.espncricinfo.com/ci/engine/match/62835.html | title=Pakistan tour of West Indies, 1957/58 – 1st Test |publisher=ESPNcricinfo | accessdate=9 March 2013}}</ref><ref name="Dawn1">{{cite news| url=http://dawn.com/2013/03/08/former-cricketer-and-respected-administrator-haseeb-ahsan-dies/ | title=Pakistan cricket heavyweight Haseeb Ahsan dies |work=Dawn |publisher=Herald | accessdate=9 March 2013}}</ref> He played three matches of the series and took five wickets.<ref>{{cite news| url=http://www.espncricinfo.com/ci/engine/records/bowling/most_wickets_career.html?id=118;type=series | title=Records / Pakistan in West Indies Test Series, 1957/58 / Most wickets|publisher=ESPNcricinfo | accessdate=9 March 2013}}</ref>

Ahsan took only 14 wickets in the next two first class seasons; his best bowling figures were five for 51.<ref name="Seasons"/> He was a part of the Pakistan cricket team that [[Pakistani cricket team in India in 1960–61|toured India in 1960–61]], where he played nine matches, including five Tests, and took 24 wickets at an average of 28.75. During the 1960–61 season, Ahsan took 26 wickets, including six for 80 against the [[West Zone cricket team|West Zone]].<ref name="Seasons"/><ref>{{cite news| url=http://cricketarchive.com/Archive/Scorecards/24/24516.html | title=West Zone v Pakistani – Pakistan in India 1960/61 |publisher=CricketArchive | accessdate=9 March 2013}}</ref> In Test cricket, he was most successful against [[India national cricket team|India]], taking fifteen wickets at an [[Bowling average|average]] of 32.66.<ref>{{cite news| url=http://cricketarchive.com/Archive/Players/1/1062/t_Bowling_by_Opponent.html | title=Test bowling against each opponent Haseeb Ahsan |publisher=CricketArchive | accessdate=9 March 2013}}</ref> His best bowling figures were six wickets for 202, against the same team at the [[Jawaharlal Nehru Stadium, Chennai|Nehru Stadium]].<ref>{{cite news| url=http://www.espncricinfo.com/ci/engine/match/62886.html | title=Pakistan tour of India, 1960/61 – 4th Test |publisher=ESPNcricinfo | accessdate=9 March 2013}}</ref> During the 1961–62 and 1962 seasons, Ahsan took 28 wickets in ten matches, including a five-wicket haul against [[Worcestershire County Cricket Club|Worcestershire]].<ref name="FC"/><ref name="Seasons"/><ref>{{cite news| url=http://cricketarchive.com/Archive/Scorecards/25/25370.html | title=Worcestershire v Pakistanis – Pakistan in British Isles 1962 |publisher=CricketArchive | accessdate=9 March 2013}}</ref> In the next two domestic seasons, he played eight matches and took 12 wickets, including five for 43 runs, against Sargodha cricket team while playing for PIA during the Ayub Trophy.<ref name="FC"/><ref name="Seasons"/><ref>{{cite news| url=http://cricketarchive.com/Archive/Scorecards/25/25982.html | title=Sargodha v Pakistan International Airlines – Ayub Trophy 1962/63 (Quarter – Final) |publisher=CricketArchive | accessdate=9 March 2013}}</ref> He played his last Test at the [[National Stadium, Karachi]], where he took two wickets conceding 64 runs.<ref>{{cite news| url=http://www.espncricinfo.com/ci/engine/match/62900.html | title=England tour of Pakistan, 1961/62 – 3rd Test |publisher=ESPNcricinfo | accessdate=9 March 2013}}</ref>

By the end of his career, Ahsan had taken 27 wickets in 12 Test matches at an average of nearly 50, including two five-wicket hauls. He made 61 runs, his highest score was 14.<ref name="Profile"/>

==Administrative career==
During the 1980s, Ahsan was the chief selector and manager of the Pakistan cricket team. It was he who first selected [[Wasim Akram]] for the [[History of cricket in Pakistan from 1971 to 1985#New Zealand 1984–85|series against New Zealand]] in 1984–85. Akram described him as "a powerful selector, [who] spotted young talent and threw them into the bigger battles".<ref name="espndeath">{{cite news| url=http://www.espncricinfo.com/pakistan/content/story/624078.html| title=Pakistan News: Haseeb Ahsan dies aged 73 |publisher=ESPNcricinfo |first=Umar |last=Farooq |date=8 March 2013 | accessdate=9 March 2013}}</ref><ref name="DTdeath">{{cite news| url=http://www.dailytimes.com.pk/default.asp?page=2013\03\09\story_9-3-2013_pg2_2| title=Former Pakistan Test cricketer Haseeb dies at 73 |work=[[Daily Times (Pakistan)|Daily Times]]|author=Staff Report |date=9 March 2013 | accessdate=9 March 2013}}</ref> He was Technical Committee's chairman for the [[1987 Cricket World Cup]] and one of the members of the tournament organising committee. During the same tournament, he served as a team manager for Pakistan. In 2003, former [[Pakistan Cricket Board]]'s (PCB) Tuqir Zia appointed him as President of the Sindh Cricket Association. He was also the Ireland's honorary Counsel General and director of the Karachi's [[American Express]].<ref name="PSC">{{cite news| url=http://www.espncricinfo.com/pakistan/content/story/128897.html| title=PCB announces provincial office bearers |publisher=ESPNcricinfo |author=[[Pakistan Cricket Board]] (PCB) |date=16 July 2003 | accessdate=9 March 2013}}</ref> PCB chairman [[Zaka Ashraf]] said of Ahsan that he was "not only a superb Test cricketer but also was a good administrator who intimately knew the game".<ref name="Dawn1"/> He was the member of the panel that heard the appeals opposed to doping bans that were imposed on [[Shoaib Akhtar]] and [[Mohammad Asif]].<ref>{{cite news| url=http://www.espncricinfo.com/pakistan/content/story/266940.html| title=Justice Fakhruddin Ibrahim to head panel: New panel to hear doping appeal |publisher=ESPNcricinfo |author=Cricinfo Staff |date=5 November 2006 | accessdate=9 March 2013}}</ref> His colleagues described him that he was a "perfect administrator".<ref name="Saad">{{cite web|url=http://www.espncricinfo.com/magazine/content/story/624201.html|title=Courageous, combative, commonsensical |publisher=ESPNcricinfo |first=Saad |last=Shafqat|date=9 March 2013|accessdate=10 March 2013}}</ref>

==Personal life and controversies==
Ahsan was born in [[Peshawar]], [[North-West Frontier Province (1901–1955)|North-West Frontier Province]] (now [[Khyber Pakhtunkhwa]]) on 15 July 1939, and was an Urdu-speaker.<ref name="Saad"/><ref name="Tribune">{{cite news| url=http://tribune.com.pk/story/517678/former-chief-selector-haseeb-ahsan-dies/| title=Former chief selector Haseeb Ahsan dies |work=[[The Express Tribune]]|publisher=[[Agence France-Presse]] (AFP) |date=8 March 2013 | accessdate=9 March 2013}}</ref> He was educated at [[Islamia College University|Islamia College, Peshawar]].<ref name="dawn">{{cite web|url=http://dawn.com/2013/03/10/haseeb-ahsan-a-man-of-conviction-and-self-belief/|title=Haseeb Ahsan – a man of conviction and self-belief |work=Dawn|publisher=Herald|first=Qamar|last=Ahmed|date=10 March 2013|accessdate=10 March 2013|location=[[Johannesburg]]}}</ref> Ahsan was never married. Former Pakistan cricketer, [[Aftab Baloch]], said Ahsan that he was a "fine gentleman". He had conflicts with former Pakistan captain [[Javed Burki]]. A controversy regarding his bowling action arose during a Test match against India. This was sixth match in which he was "[[List of international cricketers called for throwing|called for throwing]]." He continued his bowling until the issue reappeared during [[Pakistani cricket team in England in 1962|Pakistan's tour to England in 1962]]; the controversy ended his international career at the age of 23.<ref name="Saad"/><ref name="dawn"/>

==Death==
Ahsan suffered from [[renal failure]] for two years.  He was on [[dialysis]] and was admitted at the [[Aga Khan University Hospital, Karachi|Aga Khan Hospital]], [[Karachi]].<ref name="Dawn"/> President of [[Karachi City Cricket Association]] (KCCA) Sirajul Islam Bukhari stated about him that he "fought illness with courage."<ref name="Dawn1"/> He died in Karachi on 8 March 2013 at the age of 73.<ref>{{cite news| url=http://www.thefrontierpost.com/news/19534/| title=Former Pakistan cricketer Haseeb Ahsan dies |work=[[The Frontier Post]] |date=8 March 2013 | accessdate=9 March 2013}}</ref><ref>{{cite news| url=http://www.thefrontierpost.com/article/211050/| title=Haseeb Ahsan passes away |work=The Frontier Post|author=F.P. Report|date=9 March 2013 | accessdate=9 March 2013}}</ref> Ahsan was buried at the [[PECHS|PECHS graveyard]].<ref name="Dawn"/> PCB chairman, chief operating officer Subhan Ahmad and Director General [[Javed Miandad]] condoled his death.<ref>{{cite news| url=http://www.pcb.com.pk/press-release/pcb-condoles-the-death-of-haseeb-ahsan.html| title=PCB condoles the death of Haseeb Ahsan  |publisher=[[Pakistan Cricket Board]] (PCB) |date=8 March 2013 | accessdate=9 March 2013}}</ref> [[Chief Minister of Punjab, Pakistan|Chief Minister of the Punjab]] [[Shahbaz Sharif]] "expressed deep sense of grief and sorrow" on his death.<ref>{{cite news| url=http://www.lhrtimes.com/2013/03/09/cm-punjab-condoles-death-of-ex-test-cricketer-haseeb-ahsan/ | title=CM Punjab condoles death of ex-Test cricketer Haseeb Ahsan  |work=The Lahore Times |date=9 March 2013 | accessdate=9 March 2013}}</ref>

==References==
{{reflist|2}}

==External links==
*{{cite web|url=http://www.cricketarchive.com/Archive/Players/1/1062/1062.html|title=Haseeb Ahsan |publisher=[[CricketArchive]]}}
*{{cite web|url=http://cricket.yahoo.com/player-profile/haseeb-ahsan_1405|title=Haseeb Ahsan |publisher=[[Yahoo! Cricket]]}}

{{Good article}}

{{DEFAULTSORT:Ahsan, Haseeb}}
[[Category:1939 births]]
[[Category:2013 deaths]]
[[Category:Pakistan cricket team selectors]]
[[Category:Islamia College University alumni]]
[[Category:Karachi B cricketers]]
[[Category:Karachi Blues cricketers]]
[[Category:Pakistan International Airlines cricketers]]
[[Category:Pakistan Test cricketers]]
[[Category:Pakistan Universities cricketers]]
[[Category:Sportspeople from Peshawar]]
[[Category:Peshawar cricketers]]
[[Category:Peshawar University cricketers]]
[[Category:Pakistan Eaglets cricketers]]
[[Category:Muhajir people]]