{{Orphan|date=February 2014}}

{{Infobox artist
| bgcolour      = #6495ED
| name          = Carolyn Heller
| image         = Carolyn Froshin Heller.jpg
| imagesize     =
| caption       = 
| birth_name    = Carolyn Frohsin
| birth_date    ={{birth date |1937|1|9|}}
| birth_place   = Alexander City, Alabama, United States
| death_date    = {{death date and age |2011|8|22|1937|1|9|}}
| death_place   = Tampa, Florida, United States
| nationality   = American
| field         = Painting, decorative
| training      = H. Sophie Newcomb Memorial College, University of South Florida, Tampa Museum of Art
| movement      = 
| works         = 
| patrons       = Ronda Storms
| influenced by = 
| influenced    = 
| awards        = 
}}

'''Carolyn Heller''' (9 January 1937 – 22 August 2011), was a [[Florida]] [[painter]] and [[decorative arts|decorative artist]]. She painted primarily in the realm of [[decorative art]]s, using bright colors and often [[tropical]] motifs.

==Early life and education==

She was born Caroyln Frohsin in [[Alexander City, Alabama]] in 1937.<ref name=Meacham>{{cite web|last=Meacham|first=Andrew|title=Carolyn Heller, artist and friend, leaves colorful legacy|url=http://www.tampabay.com/news/obituaries/carolyn-heller-artist-and-friend-leaves-colorful-legacy/1188457|work=Epilogue|publisher=Tampa Bay Times|accessdate=9 December 2013}}</ref> Her parents, Ralph and Frances, were the owners of the Alabama-based [[Frohsins Department Store]].<ref name=Cabrera/> She attended [[H. Sophie Newcomb Memorial College]] and studied art. She married lawyer Edward Heller in 1958.<ref name=Meacham/>

==Mid-life and career==

Heller painted beyond canvas on easels, painting objects ranging from [[chair]]s to [[pool table]]s.<ref name=Meacham/> She painted primarily in [[acrylic paint|acrylic]].<ref name=Cabrera>{{cite web|last=Cabrera|first=Cloe|title=Tampa artist was 'vibrant, full of life'|url=http://tbo.com/news/tampa-artist-was-vibrant-full-of-life-253030|work=Local news|publisher=Tampa Tribune|accessdate=9 December 2013}}</ref> Throughout the 1950s and 1970s, her work was heavily themed around [[flower]]s. In the 2000s, her paintings became more [[abstract art|abstract]], with her continued use of bold colors.<ref name=TBT>{{cite web|title=Exhibition pays tribute to Carolyn Heller|url=http://tbo.com/local/communitynewsexhibition-pays-tribute-to-carolyn-heller-647384|work=Community News|publisher=Tampa Bay Tribune|accessdate=9 December 2013}}</ref> It was during the 2000s when she started producing decorative arts. She designed jewelry and [[textile]]s, and also produced [[serigraph]]s. Her studio was located in her garage. She painted while watching television, with an affinity for [[adult animation]].<ref name=Cabrera/> To sell her work, she hosted [[happy hour]] events at her house to entertain potential patrons and buyers. Patrons of Heller included [[Ronda Storms]].<ref name=Meacham/> She studied and worked closely with, [[Syd Solomon]], [[Ida Kohlmeyer]], and [[William Pachner]].<ref name=Cabrera/>

===1960s===
Heller moved to [[Tampa, Florida]], with Edward, in 1961. She quickly became involved in the arts community.<ref name=Meacham/> She took classes at the [[University of South Florida]] and [[Tampa Museum of Art]].<ref name=Cabrera/> She had four children with Edward: Alan, Emily, Janet, and Fran. After the children went to college, she became a full-time artist.<ref name=Cabrera/> They lived in the [[Hyde Park, Florida|Hyde Park]] neighborhood of Tampa. She lived in the neighborhood until her death.<ref name=Lohn>{{cite web|last=Lohn|first=Anjuli|title=Late artist's family uncovers hidden trove of prints|url=http://www.myfoxtampabay.com/story/19919440/2012/10/25/late-artists-family-uncovers-hidden-trove-of-prints|work=Bio|publisher=FOX 13 News|accessdate=9 December 2013}}</ref><ref name=Sheehan/>

===1970s===
In the late 1970s, Heller and Edward divorced.<ref name=Meacham/>

==Later life and legacy==

In 1996, she joined the [[public art]] board of the [[Hillsborough Arts Council]]. She left the council in 2004.<ref name=Meacham/> She was supporter of the Tampa AIDS Network, donating artworks to their fundraising auctions.<ref name=Meacham/> She died in August 2011, of a [[blood clot]], at [[Tampa General Hospital]].<ref name=Meacham/>

Approximately 150 works were held by Heller upon her death. The works are on display via an online gallery created by her family.<ref name=Sheehan>{{cite web|last=Sheehan|first=Keeley|title=Family of Carolyn Heller to share her art online, in museum exhibit|url=http://www.tampabay.com/news/family-of-carolyn-heller-to-share-her-art-online-in-museum-exhibit/1258535|work=News|publisher=Tampa Bay Times|accessdate=9 December 2013}}</ref> Her family discovered a collection of never published prints in her house. A selection of the prints were loaned to a local community center to teach children about art appreciation.<ref name=Lohn/> The Carolyn F. Heller Grant is awarded annually, in the memory of Heller, by the Hillsborough Arts Council.<ref name=Spencer>{{cite web|last=Spencer|first=Camille|title=Artist Receives Carolyn F. Heller Grant Award|url=http://southtampa.patch.com/groups/editors-picks/p/xx-e063f1a1|work=News|publisher=South Tampa-Hyde Park Patch|accessdate=9 December 2013}}</ref>

==Major exhibitions==

*''Pieces of a Dream'', group show, 2009, University of Tampa, Tampa, Florida<ref name=TT1>{{cite web|title='Pieces Of A Dream' Debuts Emerging Artists|url=http://tbo.com/south-tampa/pieces-of-a-dream-debuts-emerging-artists-217601|work=South Tampa News|publisher=Tampa Tribune|accessdate=9 December 2013}}</ref>
*Retrospective, solo show, 2013, Tampa Museum of Art, Tampa, Florida<ref name=Sheehan/>

==Major collections==

*''Hot Flashes'', [[Tampa General Hospital]], Tampa, Florida<ref name=Cabrera/>
*''Tampa Stars'', [[Tampa-Hillsborough County Public Library System]], Tampa, Florida<ref name=CN1>{{cite web|title=Tampa Stars|url=http://culturenow.org/entry&permalink=07807&seo=Tampa-Stars_Carolyn-Heller-and-Hillsborough-County-Public-Art|publisher=Culture Now|accessdate=9 December 2013}}</ref>

== References ==
{{reflist}}

==External links==
*{{official website|http://carolynhellerart.com/}}

{{DEFAULTSORT:Heller, Carolyn}}
[[Category:1937 births]]
[[Category:2011 deaths]]
[[Category:20th-century American painters]]
[[Category:21st-century American painters]]
[[Category:Artists from Alabama]]
[[Category:People from Tampa, Florida]]
[[Category:Artists from Florida]]
[[Category:American women artists]]
[[Category:Tulane University alumni]]
[[Category:People from Alexander City, Alabama]]
[[Category:20th-century women artists]]
[[Category:21st-century women artists]]
[[Category:American women painters]]