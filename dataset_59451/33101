{{featured article}}
{{Use dmy dates|date=December 2016}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:Bundesarchiv DVM 10 Bild-23-61-53, Großlinienschiff "SMS Prinzregent Luitpold".jpg|300px]]
| Ship caption = SMS ''Prinzregent Luitpold''
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship name = ''Prinzregent Luitpold''
| Ship namesake = Prince Regent [[Luitpold, Prince Regent of Bavaria|Luitpold]] of Bavaria
| Ship builder = [[Germaniawerft]], [[Kiel]]
| Ship laid down = October 1910
| Ship launched = 17 February 1912
| Ship commissioned = 19 August 1913
| Ship fate = Scuttled at [[Gutter Sound]], [[Scapa Flow]] 21 June 1919
| Ship status = 
| Ship notes = Raised in 1931 and broken up for scrapping 1933
| Ship badge = 
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Kaiser|battleship}}
| Ship displacement =
* {{convert|24,724|t|LT|abbr=on}} '''designed'''
* {{convert|27,000|t|LT|abbr=on|-1}} maximum load
| Ship length = {{convert|172.4|m|ftin|abbr=on}}
| Ship beam = {{convert|29.0|m|ftin|abbr=on}}
| Ship draft = {{convert|9.1|m|ftin|abbr=on}}
| Ship propulsion = 2-shaft Parsons turbines
|Ship power=
*{{convert|19,123|kW|shp|abbr=on|lk=in|order=flip}}
*{{convert|28,501|kW|shp|abbr=on|lk=in|order=flip||-2}} (trial)
| Ship speed = {{convert|21.7|kn}}
| Ship range = {{convert|7900|nmi|lk=in|abbr=on}} at {{convert|12|kn}}
| Ship crew =
*41 officers
*1,043 enlisted
| Ship armament =
* 10 × [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} guns]]
* 14 × [[15 cm SK L/45|{{convert|15|cm|abbr=on}} guns]]
* 12 × [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} guns]]
*  5 × {{convert|50|cm|in|abbr=on}} torpedo tubes
| Ship armor =
* [[Belt armor|Belt]]: {{convert|350|mm|in|abbr=on}}{{sfn|Gröner|p=26}}
* [[Conning tower]]: {{convert|400|mm|in|abbr=on}}{{sfn|Gröner|p=26}}
* [[Gun turret|Turrets]]: {{convert|300|mm|in|abbr=on}}{{sfn|Gröner|p=26}}
| Ship notes = 
}}
|}

'''SMS ''Prinzregent Luitpold'''''{{efn|name=SMS}} was the fifth and final vessel of the {{sclass-|Kaiser|battleship|4}} of [[battleship]]s of the [[Imperial German Navy]]. ''Prinzregent Luitpold''{{'}}s keel was laid in October 1910 at the [[Germaniawerft]] dockyard in [[Kiel]]. She was launched on 17&nbsp;February 1912 and was commissioned into the navy on 19&nbsp;August 1913. The ship was equipped with ten {{convert|30.5|cm|in|sp=us|adj=on}} guns in five twin turrets, and had a top speed of {{convert|21.7|kn}}.

''Prinzregent Luitpold'' was assigned to the III&nbsp;Battle Squadron of the [[High Seas Fleet]] for the majority of her career; in December 1916, she was transferred to the IV&nbsp;Battle Squadron. Along with her four sister ships, {{SMS|Kaiser|1911|2}}, {{SMS|Friedrich der Grosse|1911|2}}, {{SMS|Kaiserin||2}}, and {{SMS|König Albert||2}}, ''Prinzregent Luitpold'' participated in all of the major fleet operations of [[World War&nbsp;I]], including the [[Battle of Jutland]] on 31&nbsp;May&nbsp;– 1&nbsp;June 1916. The ship was also involved in [[Operation Albion]], an amphibious assault on the Russian-held islands in the [[Gulf of Riga]], in late 1917.

After Germany's defeat in the war and the signing of the [[Armistice with Germany|Armistice]] in November 1918, ''Prinzregent Luitpold'' and most of the [[capital ship]]s of the High Seas Fleet were interned by the [[Royal Navy]] in [[Scapa Flow]]. The ships were disarmed and reduced to skeleton crews while the [[Allies of World War I|Allied powers]] negotiated the final version of the [[Treaty of Versailles]]. On 21&nbsp;June 1919, days before the treaty was signed, the commander of the interned fleet, Rear Admiral [[Ludwig von Reuter]], [[Scuttling of the German fleet in Scapa Flow|ordered the fleet to be scuttled]] to ensure that the British would not be able to seize the ships. ''Prinzregent Luitpold'' was raised in July 1931 and subsequently [[ship breaking|broken up]] for scrap in 1933.

== Construction ==
[[File:Kaiser class diagram.jpg|thumb|left|The shaded areas represent the portions of the ship protected by armor|alt=A large warship with five gun turrets, two tall masts, two funnels, and heavy armor protection.]]

''Prinzregent Luitpold'' was {{convert|172.4|m|ftin|abbr=on}} long [[Length overall|overall]] and displaced a maximum of {{convert|27,000|t|LT|sp=us|-1}}.  She had a beam of {{convert|29|m|ftin|abbr=on}} and a draft of {{convert|9.1|m|ftin|abbr=on}} forward and {{convert|8.8|m|ftin|abbr=on}} aft. She had a crew of 41&nbsp;officers and 1,043&nbsp;enlisted men. ''Prinzregent Luitpold'' was powered by two sets of Parsons [[steam turbine]]s, supplied with steam by 14&nbsp;coal-fired boilers. Unlike her four sisters, the ship was intended to use a [[diesel engine]] on the center shaft, but this was not ready by the time work on the ship was completed. The engine was never installed, and so ''Prinzregent Luitpold'' was slightly slower than her sisters, which were equipped with a third turbine on the center shaft. The powerplant produced a top speed of {{convert|21.7|kn}}. She carried {{convert|3,600|t|LT|sp=us|-1}} of coal, which enabled a maximum range of {{convert|7,900|nmi|lk=in|sp=us|-1}} at a cruising speed of {{convert|12|kn}}.{{sfn|Gröner|p=26}}

''Prinzregent Luitpold'' was armed with a main battery of ten [[30.5 cm SK L/50 gun]]s in five twin turrets.{{sfn|Gröner|p=26}} The ship dispensed with the inefficient hexagonal turret arrangement of previous German battleships; instead, three of the five turrets were mounted on the centerline, one forward and two of them arranged in a [[superfire|superfiring pair]] aft. The other two turrets were placed [[Glossary of nautical terms#E|''en echelon'']] amidships, such that both could fire on the [[broadside]].{{sfn|Staff, ''Battleships''|p=4}} The ship was also armed with fourteen [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} SK L/45 guns]] in [[casemate]]s amidships, eight [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} SK L/45 guns]] in casemates and four 8.8&nbsp;cm L/45 anti-aircraft guns. The ship's armament was rounded out by five {{convert|50|cm|in|abbr=on}} torpedo tubes, all mounted in the hull; one was in the bow, and the other four were on the [[broadside]].{{sfn|Gröner|p=26}}

== Service history ==
Ordered under the contract name ''Ersatz Odin'' as a replacement for the obsolete [[coastal defense ship]] {{SMS|Odin||2}},{{sfn|Staff, ''Battleships''|p=6}}{{efn|name=provisional names}} ''Prinzregent Luitpold'' was laid down at the [[Howaldtswerke]] dockyard in [[Kiel]] in October 1910.{{sfn|Gardiner & Gray|p=147}} She was launched on 17&nbsp;February 1912 and christened by [[Princess Theresa of Bavaria]]; [[Ludwig III of Bavaria|Ludwig&nbsp;III]], the last king of Bavaria and the son of the ship's namesake, [[Luitpold, Prince Regent of Bavaria]], gave a speech.{{sfn|Hildebrand Röhr & Steinmetz|p=54}} After fitting-out work was completed, the ship was commissioned into the fleet on 19&nbsp;August 1913. ''Prinzregent Luitpold'' was equipped with facilities for a squadron commander, and became the flagship of the III&nbsp;Battle Squadron upon commissioning.{{sfn|Staff, ''Battleships''|pp=21–22}}

Directly after commissioning, ''Prinzregent Luitpold'' took part in the annual autumn maneuvers, which followed the fleet cruise to Norway. The exercises lasted from 31&nbsp;August to 9&nbsp;September. Unit drills and individual ship training were conducted in October and November.{{sfn|Staff, ''Battleships''|pp=14, 22}} In early 1914, ''Prinzregent Luitpold'' participated in additional ship and unit training. The annual spring maneuvers were conducted in the North Sea at the end of March. Further fleet exercises followed in April and May in the Baltic and North Seas. The ship went to [[Kiel Week]] that year. Despite the rising international tensions following the [[Assassination of Archduke Franz Ferdinand|assassination]] of [[Archduke Franz Ferdinand]] on 28&nbsp;June, the [[High Seas Fleet]] began its summer cruise to Norway on 13&nbsp;July. During the last peacetime cruise of the Imperial Navy, the fleet conducted drills off [[Skagen]] before proceeding to the Norwegian fjords on 25&nbsp;July. The following day the fleet began to steam back to Germany, as a result of Austria-Hungary's [[July Ultimatum|ultimatum to Serbia]]. On the 27th, the entire fleet assembled off [[Cape Skadenes]] before returning to port, where they remained at a heightened state of readiness.{{sfn|Staff, ''Battleships''|pp=14, 22}} War between Austria-Hungary and Serbia broke out the following day, and in the span of a week all of the major European powers had joined the conflict.{{sfn|Heyman|p=xix}}

''Prinzregent Luitpold'' was present during the first [[sortie]] by the German fleet into the North Sea, which took place on 2–3&nbsp;November 1914. No British forces were encountered during the operation. A second operation followed on 15–16&nbsp;December.{{sfn|Staff, ''Battleships''|pp=19, 22}} This sortie was the initiation of a strategy adopted by Admiral [[Friedrich von Ingenohl]], the commander of the High Seas Fleet. He intended to use the [[battlecruiser]]s of Rear Admiral [[Franz von Hipper]]'s [[I&nbsp;Scouting Group]] to raid British coastal towns to lure out portions of the British [[Grand Fleet]] where they could be destroyed by the High Seas Fleet.{{sfn|Herwig|pp=149–150}} Early on 15&nbsp;December the fleet left port to [[raid on Scarborough, Hartlepool and Whitby|raid the towns of Scarborough, Hartlepool, and Whitby]]. That evening, the German battle fleet of some twelve dreadnoughts—including ''Prinzregent Luitpold'' and her four [[sister ship|sisters]]—and eight pre-dreadnoughts came to within {{convert|10|nmi|abbr=on}} of an isolated squadron of six British battleships. However, skirmishes between the rival [[destroyer]] screens in the darkness convinced von Ingenohl that he was faced with the entire Grand Fleet. Under orders from [[Kaiser Wilhelm&nbsp;II]] to avoid risking the fleet unnecessarily, von Ingenohl broke off the engagement and turned the battle fleet back toward Germany.{{sfn|Tarrant|pp=31–33}}

''Prinzregent Luitpold'' went into the Baltic for squadron training from 23&nbsp;to 29&nbsp;January 1916.{{sfn|Staff, ''Battleships''|pp=19, 22}} While on the maneuvers, the newer battleship {{SMS|König||2}} became the III&nbsp;Squadron flagship. Vice Admiral [[Reinhard Scheer]], the commander of III&nbsp;Squadron, lowered his flag on 24&nbsp;January and transferred it to ''König''.{{sfn|Staff, ''Battleships''|p=22}} The Kaiser removed von Ingenohl from his post on 2&nbsp;February, following the loss of the [[armored cruiser]] {{SMS|Blücher}} at the [[Battle of Dogger Bank (1915)|Battle of Dogger Bank]] the month before. Admiral [[Hugo von Pohl]] succeeded him as the commander of the fleet.{{sfn|Tarrant|pp=43–44}} Pohl continued the policy of sweeps into the North Sea to destroy isolated British formations. On 24&nbsp;April, ''Prinzregent Luitpold'' ran aground in the [[Kaiser Wilhelm Canal]], though she was freed without causing significant damage.{{sfn|Staff, ''Battleships''|p=22}} A series of advances into the North Sea were conducted throughout the rest of 1915; ''Prinzregent Luitpold'' was present for the sweeps on 17–18 May, 29–30 May, 10&nbsp;August, 11–12 September, and 23–24 October. The III&nbsp;Squadron completed the year with another round of unit training in the Baltic on 5–20 December.{{sfn|Staff, ''Battleships''|pp=19, 22}}

Pohl's tenure as fleet commander was brief; by January 1916 [[hepatic cancer]] had weakened him to the point where he was no longer able to carry out his duties. He was replaced by Vice Admiral Reinhard Scheer in January.{{sfn|Herwig|p=161}} Scheer proposed a more aggressive policy designed to force a confrontation with the British Grand Fleet; he received approval from the Kaiser in February.{{sfn|Tarrant|p=50}} The first of Scheer's operations was conducted the following month, on 5–7 March, with an uneventful sweep of the [[Hoofden]].{{sfn|Staff, ''Battleships''|pp=32, 35}} ''Prinzregent Luitpold'' was also present during an advance to the [[Amrun Bank]] on 2–3 April. Another sortie was conducted on 21–22 April.{{sfn|Staff, ''Battleships''|pp=19, 22}}

=== Battle of Jutland ===
{{main|Battle of Jutland}}
[[File:Map of the Battle of Jutland, 1916.svg|thumb|350px|Maps showing the maneuvers of the British (blue) and German (red) fleets on 31 May&nbsp;– 1 June 1916|alt=The British fleet sailed from northern Britain to the east while the Germans sailed from Germany in the south; the opposing fleets met off the Danish coast]]

''Prinzregent Luitpold'' was present during the fleet operation that resulted in the battle of [[Jutland]] which took place on 31&nbsp;May and 1&nbsp;June 1916. The German fleet again sought to draw out and isolate a portion of the Grand Fleet and destroy it before the main British fleet could retaliate. During the operation, ''Prinzregent Luitpold'' was the third ship in the VI&nbsp;Division of III&nbsp;Squadron and the seventh ship in the line, directly astern of {{SMS|Kaiserin||2}} and ahead of {{SMS|Friedrich der Grosse|1911|2}}. The VI&nbsp;Division was behind only the V&nbsp;Division, consisting of the four {{sclass-|König|battleship|2}}s. The eight {{sclass-|Helgoland|battleship|5}}- and {{sclass-|Nassau|battleship|2}}s of the I&nbsp;and II&nbsp;Divisions in I&nbsp;Squadron followed the VI&nbsp;Division. The six elderly [[pre-dreadnought]]s of the III&nbsp;and IV&nbsp;Divisions in II&nbsp;Battle Squadron formed the rear of the formation.{{sfn|Tarrant|p=286}}

Shortly before 16:00, the battlecruisers of I&nbsp;Scouting Group encountered the British 1st Battlecruiser Squadron under the command of Vice Admiral [[David Beatty, 1st Earl Beatty|David Beatty]]. The opposing ships began an artillery duel that saw the destruction of {{HMS|Indefatigable|1909|2}}, shortly after 17:00,{{sfn|Tarrant|pp=94–95}} and {{HMS|Queen Mary||2}}, less than half an hour later.{{sfn|Tarrant|pp=100–101}} By this time, the German battlecruisers were steaming south to draw the British ships toward the main body of the High Seas Fleet. At 17:30, the crew of the leading German battleship, ''König'', spotted both the I&nbsp;Scouting Group and the 1st Battlecruiser Squadron approaching. The German battlecruisers were steaming to starboard, while the British ships steamed to port. At 17:45, Scheer ordered a [[Boxing the compass|two-point]] turn to port to bring his ships closer to the British battlecruisers, and a minute later, the order to open fire was given.{{sfn|Tarrant|p=110}}{{efn|name=compass points}}

''Prinzregent Luitpold'' engaged the nearest target her gunners could make out, one of the {{sclass-|Lion|battlecruiser|2}}s, at a range of some {{convert|22300|yd|m|abbr=on}}, though her shots fell short. Beatty's ships increased speed and at 17:51 veered away to further increase the distance to the III&nbsp;Squadron battleships.{{sfn|Campbell|p=54}} At 18:08, ''Prinzregent Luitpold'' shifted her fire to the battleship {{HMS|Malaya||2}} at a range of {{convert|19100|yd|m|abbr=on}}, though without any success.{{sfn|Campbell|p=99}} By 18:38, ''Malaya'' disappeared in the haze and ''Prinzregent Luitpold'' was forced to cease fire.{{sfn|Campbell|p=104}} The British destroyers {{HMS|Nestor|1915|2}} and {{HMS|Nomad||2}}, which had been disabled earlier in the engagement, lay directly in the path of the advancing High Seas Fleet.{{sfn|Tarrant|p=114}} ''Prinzregent Luitpold'' and her three sisters destroyed ''Nomad'' with their secondary guns while the I&nbsp;Squadron battleships dispatched ''Nestor''.{{sfn|Campbell|p=101}} At around 19:00, the German battle line came into contact with the 2nd Light Cruiser Squadron; ''Prinzregent Luitpold'' fired two salvos from her main battery at an unidentified four-funneled cruiser at 19:03 but made no hits.{{sfn|Campbell|p=111}}

Shortly after 19:00, the German cruiser {{SMS|Wiesbaden||2}} had become disabled by a shell from the British battlecruiser {{HMS|Invincible|1907|2}}; Rear Admiral [[Paul Behncke]] in ''König'' attempted to maneuver the III&nbsp;Squadron to cover the stricken cruiser.{{sfn|Tarrant|p=137}} Simultaneously, the British 3rd and 4th Light Cruiser Squadrons began a torpedo attack on the German line; while advancing to torpedo range, they smothered ''Wiesbaden'' with fire from their main guns. The eight III&nbsp;Squadron battleships fired on the British cruisers, but even sustained fire from the battleships' main guns failed to drive off the British cruisers.{{sfn|Tarrant|p=138}} The armored cruisers {{HMS|Defence|1907|2}}, {{HMS|Warrior|1905|2}}, and {{HMS|Black Prince|1904|2}} joined in the attack on the crippled ''Wiesbaden''.{{sfn|Tarrant|p=139}} Between 19:14 and 19:17, several German battleships and battlecruisers opened fire on ''Defence'' and ''Warrior''. Instead of joining the fire on the much closer cruisers, ''Prinzregent Luitpold'' engaged the leading battleships of the British line, firing a total of 21&nbsp;salvos. The gunners reported ranges of {{convert|17500|to|18800|yd|m|abbr=on}}, though this was an overestimation that caused the ship's salvos to fall past their intended target.{{sfn|Campbell|p=152}}

By 20:00, the German line was ordered to complete a 180-degree turn eastward to disengage from the British fleet.{{sfn|Tarrant|p=169}} The maneuver, conducted under heavy fire, caused disorganization in the German fleet. ''Kaiserin'' had come too close to ''Prinzregent Luitpold'' and was forced to haul out of line to starboard to avoid a collision. ''Prinzregent Luitpold'' came up alongside ''Kaiserin'' at high speed, which forced ''Kaiserin'' to remain out of line temporarily.{{sfn|Campbell|pp=200–201}} The turn reversed the order of the German line; ''Prinzregent Luitpold'' was now the eighth ship from the rear of the German line, leading the III&nbsp;Squadron.{{sfn|Tarrant|p=172}}{{sfn|Campbell|p=201}} At around 23:30, the German fleet reorganized into the night-cruising formation. ''Kaiserin'' was the eleventh ship, in the center of the 24-ship line.{{sfn|Campbell|p=275}}

After a series of night engagements between the leading battleships and British destroyers, the High Seas Fleet punched through the British light forces and reached [[Horns Reef]] by 04:00 on 1&nbsp;June.{{sfn|Tarrant|pp=246–247}} The German fleet reached Wilhelmshaven a few hours later; the I&nbsp;Squadron battleships took up defensive positions in the outer [[roadstead]], and ''Prinzregent Luitpold'', ''Kaiserin'', ''Kaiser'', and {{SMS|Kronprinz||2}} stood ready just outside the entrance to Wilhelmshaven.{{sfn|Campbell|p=320}} The remainder of the battleships and battlecruisers entered Wilhelmshaven, where those that were still in fighting condition replenished their stocks of coal and ammunition.{{sfn|Tarrant|p=263}} In the course of the battle, ''Prinzregent Luitpold'' fired one-hundred and sixty-nine 30.5&nbsp;cm shells and one-hundred and six 15&nbsp;cm rounds.{{sfn|Tarrant|p=292}} She and her crew emerged from the battle completely unscathed.{{sfn|Staff, ''Battleships''|p=22}}

=== Subsequent operations ===
In early August, ''Prinzregent Luitpold'' and the rest of the operational III&nbsp;Squadron units conducted divisional training in the Baltic.{{sfn|Staff, ''Battleships''|pp=19, 22}} On 18&nbsp;August, Admiral Scheer attempted a repeat of the 31&nbsp;May operation; the two serviceable German battlecruisers—{{SMS|Moltke||2}} and {{SMS|Von der Tann||2}}—supported by three dreadnoughts, were to bombard the coastal town of [[Sunderland, Tyne and Wear|Sunderland]] in an attempt to draw out and destroy Beatty's battlecruisers. The rest of the fleet, including ''Prinzregent Luitpold'', would trail behind and provide cover.{{sfn|Massie|p=682}} During the operation, ''Prinzregent Luitpold'' carried the Commander of [[U-boat]]s.{{sfn|Staff, ''Battleships''|p=22}} On the approach to the English coast, Scheer turned north after receiving a false report from a [[zeppelin]] about a British unit in the area.{{sfn|Staff, ''Battleships''|p=15}} As a result, the bombardment was not carried out, and by 14:35, Scheer had been warned of the Grand Fleet's approach and so turned his forces around and retreated to German ports.{{sfn|Massie|p=683}}

Another fleet advance followed on 18–20&nbsp;October, though it ended without encountering any British units. Two weeks later, on 4&nbsp;November, ''Prinzregent Luitpold'' took part in an expedition to the western coast of Denmark to assist two U-boats—{{SMU|U-20|Germany|2}} and {{SMU|U-30|Germany|2}}—that had become stranded there. The fleet was reorganized on 1&nbsp;December;{{sfn|Staff, ''Battleships''|p=22}} the four ''König''-class battleships remained in III&nbsp;Squadron, along with the newly commissioned {{SMS|Bayern||2}}, while the five ''Kaiser''-class ships, including ''Prinzregent Luitpold'', were transferred to IV&nbsp;Squadron.{{sfn|Halpern|p=214}} ''Prinzregent Luitpold'' became the flagship of the new squadron. In the Wilhelmshaven Roads on 20&nbsp;January 1917, the ship struck a steel [[hawser]] that became entangled in the ship's starboard propeller.{{sfn|Staff, ''Battleships''|p=22}} In March, ''Friedrich der Grosse'' was replaced as the fleet flagship by the newly commissioned battleship {{SMS|Baden||2}}. ''Friedrich der Grosse'' in turn replaced ''Prinzregent Luitpold'' as the flagship of IV&nbsp;Squadron.{{sfn|Staff, ''Battleships''|pp=15, 22}} Steadily decreasing morale and discontent with rations provoked a series of small mutinies in the fleet. On 6&nbsp;June and 19&nbsp;July, stokers protested the low quality of the food they were given, and on 2&nbsp;August, some 800&nbsp;men went on a hunger strike. The ship's officers relented and agreed to form a ''Menagekommission'', a council that gave the enlisted men a voice in their ration selection and preparation.{{sfn|Herwig|p=232}} One of the ringleaders of the protests, however, was arrested and executed on 5&nbsp;September.{{sfn|Staff, ''Battleships''|p=22}}

=== Operation Albion ===
{{main|Operation Albion}}
[[File:Bundesarchiv Bild 146-1970-074-34, Besetzung der Insel Ösel, Truppenanlandung.jpg|thumb|German troops landing at Ösel|alt=A small boat packed with soldiers passes in front of a cruiser and several transport ships]]

In early September 1917, following the German conquest of the Russian port of [[Riga]], the German navy decided to eliminate the Russian naval forces that still held the [[Gulf of Riga]]. The ''Admiralstab'' (the Navy High Command) planned an operation to seize the Baltic island of [[Saaremaa|Ösel]], and specifically the Russian gun batteries on the [[Sõrve Peninsula|Sworbe Peninsula]].{{sfn|Halpern|p=213}} On 18&nbsp;September, the order was issued for a joint operation with the army to capture Ösel and [[Muhu|Moon]] Islands; the primary naval component was to comprise the flagship, ''Moltke'', along with the III&nbsp;and IV&nbsp;Battle Squadrons of the High Seas Fleet. Along with nine light cruisers, three torpedo boat flotillas, and dozens of [[naval mine|mine]] warfare ships, the entire force numbered some 300&nbsp;ships, supported by over 100&nbsp;aircraft and six zeppelins. The invasion force amounted to approximately 24,600&nbsp;officers and enlisted men.{{sfn|Halpern|pp=214–215}} Opposing the Germans were the old Russian pre-dreadnoughts {{ship|Russian battleship|Slava||2}} and {{ship|Russian battleship|Tsesarevich||2}}, the [[armored cruiser]]s {{ship|Russian cruiser|Bayan|1907|2}}, {{ship|Russian cruiser|Admiral Makarov||2}}, and {{ship|Russian cruiser|Diana|1899|2}}, 26&nbsp;destroyers, and several torpedo boats and gunboats. The garrison on Ösel numbered some 14,000&nbsp;men.{{sfn|Halpern|p=215}}

The operation began on the morning of 12&nbsp;October, when ''Moltke'' and the III&nbsp;Squadron ships engaged Russian positions in Tagga Bay while ''Prinzregent Luitpold'' and the rest of IV Squadron shelled Russian gun batteries on the [[Sworbe Peninsula]] on Ösel.{{sfn|Halpern|p=215}} ''Prinzregent Luitpold'', along with ''Kaiser'' and ''Kaiserin'', were tasked with silencing the Russian guns at [[Hundsort]] which had taken ''Moltke'' under fire. The ships opened fire at 05:44, and by 07:45, Russian firing had ceased and German troops were moving ashore.{{sfn|Staff, ''Battle for the Baltic Islands''|pp=20–21}} Two days later, Vice Admiral [[Wilhelm Souchon]] left Tagga Bay with ''Prinzregent Luitpold'', ''Friedrich der Grosse'', and ''Kaiserin'' to support German ground forces advancing on the Sworbe Peninsula.{{sfn|Staff, ''Battle for the Baltic Islands''|p=67}} By 20&nbsp;October, the fighting on the islands was winding down; Moon, Ösel, and Dagö were in German possession. The previous day, the ''Admiralstab'' had ordered the cessation of naval actions and the return of the dreadnoughts to the High Seas Fleet as soon as possible.{{sfn|Halpern|p=219}} On the 24th, ''Prinzregent Luitpold'' was detached from the task force and returned to Kiel.{{sfn|Staff, ''Battleships''|p=23}}

After arriving in Kiel, ''Prinzregent Luitpold'' went into drydock for periodic maintenance, from which she emerged on 21&nbsp;December. She then proceeded on to Wilhelmshaven, where she resumed guard duty in the Bight. On 17&nbsp;March 1918, the ship steamed to the Baltic for training exercises, and the following day the battlecruiser {{SMS|Derfflinger||2}} rammed her outside Kiel. The accident caused no serious damage, however. The ship participated in the fruitless advance to Norway on 23–25&nbsp;April 1918, after which she resumed guard duties in the German Bight.{{sfn|Staff, ''Battleships''|p=23}}

=== Fate ===
[[File:Internment at Scapa Flow.svg|thumb|400px|Map of the scuttled ships showing ''Prinzregent Luitpold'' (#17); [http://upload.wikimedia.org/wikipedia/en/9/92/Internment_at_Scapa_Flow.svg click] for a larger view|alt=A map designating the locations where the German ships were sunk.]]
{{main|Scuttling of the German fleet in Scapa Flow}}
''Prinzregent Luitpold'' and her four sisters were to have taken part in a [[Naval order of 24 October 1918|final fleet action]] at the end of October 1918, days before the [[Armistice with Germany|Armistice]] was to take effect. The bulk of the High Seas Fleet was to have sortied from their base in Wilhelmshaven to engage the British Grand Fleet; Scheer—by now the [[Grand Admiral]] (''Großadmiral'') of the fleet—intended to inflict as much damage as possible on the British navy, to improve Germany's bargaining position, despite the expected casualties. But many of the war-weary sailors felt that the operation would disrupt the peace process and prolong the war.{{sfn|Tarrant|pp=280–281}} On the morning of 29&nbsp;October 1918, the order was given to sail from Wilhelmshaven the following day. Starting on the night of&nbsp;29 October, sailors on {{SMS|Thüringen||2}} and then on several other battleships [[Wilhelmshaven mutiny|mutinied]].{{sfn|Tarrant|pp=281–282}} The unrest ultimately forced Hipper and Scheer to cancel the operation.{{sfn|Tarrant|p=282}} Informed of the situation, the Kaiser stated "I no longer have a navy".{{sfn|Herwig|p=252}}

Following the capitulation of Germany in November 1918, most of the High Seas Fleet, under the command of Rear Admiral [[Ludwig von Reuter]], was interned in the British naval base in [[Scapa Flow]].{{sfn|Tarrant|p=282}} Prior to the departure of the German fleet, Admiral [[Adolf von Trotha]] made clear to von Reuter that he could not allow the Allies to seize the ships, under any circumstances.{{sfn|Herwig|p=256}} The fleet rendezvoused with the British [[light cruiser]] {{HMS|Cardiff|D58|2}}, which led the ships to the Allied fleet that was to escort the Germans to Scapa Flow. The massive flotilla consisted of some 370&nbsp;British, American, and French warships.{{sfn|Herwig|pp=254–255}} Once the ships were interned, their guns were disabled through the removal of their [[Breech mechanism|breech blocks]], and their crews were reduced to 200&nbsp;officers and men per ship.{{sfn|Herwig|p=255}}

The fleet remained in captivity during the negotiations that ultimately produced the [[Treaty of Versailles]]. Von Reuter believed that the British intended to seize the German ships on 21&nbsp;June 1919, which was the deadline for Germany to have signed the peace treaty. Unaware that the deadline had been extended to the 23rd, Reuter [[Scuttling of the German fleet in Scapa Flow|ordered the ships to be sunk]] at the next opportunity. On the morning of 21&nbsp;June, the British fleet left Scapa Flow to conduct training maneuvers, and at 11:20 Reuter transmitted the order to his ships.{{sfn|Herwig|p=256}} ''Prinzregent Luitpold'' sank at 13:30; she was subsequently raised on 9&nbsp;July 1931 and broken up by 1933 in [[Rosyth]],{{sfn|Gröner|p=26}} as with several other vessels, upside-down having capsized in the scuttling.{{sfn|Bowman|p=224}}
{{clear}}

== Footnotes ==

{{portal|Battleships}}

'''Footnotes'''

{{notes
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''" ({{lang-de|His Majesty's Ship}}).
}}

{{efn
| name = provisional names
| German warships were ordered under provisional names. For new additions to the fleet, they were given a single letter; for those ships intended to replace older or lost vessels, they were ordered as "Ersatz (name of the ship to be replaced)".
}}

{{efn
| name = compass points
| The compass can be divided into 32 points, each corresponding to 11.25 degrees. A two-point turn to port would alter the ships' course by 22.5 degrees.
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==
{{Commons category|SMS Prinzregent Luitpold (ship, 1912)|SMS Prinzregent Luitpold}}
* {{cite book
  | last = Bowman
  | first = Gerald
  | year = 2002
  | origyear = 1964
  | title = The Man Who Bought A Navy: The Story of The World's Greatest Salvage Achievement at Scapa Flow
  | publisher = Harrap
  | location = Surbiton
  | oclc = 2219189
  | ref = {{sfnRef|Bowman}}
  }}
* {{cite book
  | last = Campbell
  | first = John
  | year = 1998
  | title = Jutland: An Analysis of the Fighting
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-1-55821-759-1
  | ref = {{sfnRef|Campbell}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last1 = Gröner
  | first1 = Erich
  | last2 = Jung
  | first2 = Dieter
  | last3 = Martin
  | first3 = Maass
  | year = 1990
  | title = German Warships: 1815–1945
  | trans_title = Die deutschen Kriegsschiffe, 1815–1945
  | volume = 1
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = Amherst
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last = Heyman
  | first = Neil M.
  | title = World War I
  | year = 1997
  | location = Westport
  | publisher = Greenwood Publishing Group
  | isbn = 978-0-313-29880-6
  | ref = {{sfnRef|Heyman}}
  }}
* {{cite book
  | last1 = Hildebrand
  | first1 = Hans H.
  | last2 = Röhr
  | first2 = Albert
  | last3 = Steinmetz
  | first3 = Hans-Otto
  | year = 1990
  | language = German
  | title = Die Deutschen Kriegsschiffe. Biographien&nbsp;— ein Spiegel der Marinegeschichte von 1815 bis zur Gegenwart. (10 Bände)
  | trans_title=German Warships. Biographies&nbsp;— a Mirror of Naval History from 1815 to the Present. (10 Volumes)
  | publisher = Mundus Verlag
  | location = 
  | isbn = 
  | ref = {{sfnRef|Hildebrand Röhr & Steinmetz}}
  }}
* {{cite book
  | last = Massie
  | first = Robert K.
  | authorlink = Robert K. Massie
  | year = 2003
  | title = [[Castles of Steel]]
  | publisher = Ballantine Books
  | location = New York City
  | isbn = 978-0-345-40878-5
  | oclc = 57134223
  | ref = {{sfnRef|Massie}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918
  | volume = 2
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-468-8
  | oclc = 449845203
  | ref = {{sfnRef|Staff, ''Battleships''}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2008
  | origyear = 1995
  | title = Battle for the Baltic Islands 1917: Triumph of the Imperial German Navy
  | publisher = Pen & Sword Maritime
  | location = Barnsley
  | isbn = 978-1-84415-787-7
  | oclc = 232131032
  | ref = {{sfnRef|Staff, ''Battle for the Baltic Islands''}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}

{{Kaiser class battleship}}
{{1919 shipwrecks}}

{{DEFAULTSORT:Prinzregent Luitpold}}
[[Category:Kaiser-class battleships]]
[[Category:World War I battleships of Germany]]
[[Category:World War I warships scuttled in Scapa Flow]]
[[Category:Maritime incidents in 1919]]
[[Category:1912 ships]]
[[Category:Ships built in Kiel]]