{{about|the academic journal|the British magazine|Current World Archaeology}}
{{Infobox journal
| title = World Archaeology
| cover = [[File:World Archaeology journal cover 2007.jpg]]
| editor = [[Alan K. Outram]]
| discipline = [[Archaeology]]
| abbreviation = World Archaeol.
| publisher = [[Routledge]]
| country =
| frequency = Quarterly
| history = 1969–present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.tandfonline.com/action/journalInformation?journalCode=rwar20
| link1 = http://www.tandfonline.com/toc/rwar20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/rwar20
| link2-name = Online archive
| JSTOR = 00438243
| OCLC = 48535549
| LCCN = 75646489
| CODEN = WOAREN
| ISSN = 0043-8243
| eISSN = 1470-1375
}}
'''''World Archaeology''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering all aspects of [[archaeology]]. It was established in 1969 and originally published triannually by [[Routledge & Kegan Paul]]. In 2004 it changed to a quarterly publication schedule while remaining under the Routledge [[imprint (trade name)|imprint]].

Each of the year's first three issues within a volume are dedicated to specific individual themes and topics within archaeology, and contributions address the topic from a variety of perspectives. The fourth and last issue of the year has been given over to coverage of current debates within archaeology, in which papers discuss significant issues and global concerns in the field.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Abstracts in Anthropology]]
* [[America: History and Life]]
* [[Anthropological Index Online]]
* [[Anthropological Literature]]
* [[Art Index]]
* [[Avery Index to Architectural Periodicals]]
* [[Getty Research Institute|Bibliography of the History of Art]]
* [[Bibliography of Native North Americans]]
* [[British & Irish Archaeological Bibliography]]
* [[British Humanities Index]]
* [[Scopus]]
* [[GEOBASE (database)|GEOBASE]]
* [[Historical Abstracts]]
* [[Humanities Index]]
* [[Humanities International Index]]
* [[Index Islamicus]]
* [[International Bibliography of the Social Sciences]]
* [[Linguistic Bibliography]]
* [[Boston College School of Theology and Ministry|New Testament Abstracts]]
* [[ProQuest|ProQuest Central]]
* [[Religion Index One]]
* [[Arts and Humanities Citation Index]]
}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=rwar20}}

[[Category:Archaeology journals]]
[[Category:Publications established in 1969]]
[[Category:English-language journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Quarterly journals]]