{{Infobox road
|state=MD
|type=MD
|route=36
|map_alt=
|map_notes=
|length_mi=29.43
|length_round=2
|length_ref=<ref name=HLR />
|length_notes=
|tourist=[[File:MUTCD D6-4.svg|20px|alt=|link=]] [[Historic National Road]]<br>[[File:MD scenic byway.svg|20px|alt=|link=]] [[Mountain Maryland Scenic Byway]]
|established=1927
|decommissioned=
|maint=[[Maryland State Highway Administration|MDSHA]]
|direction_a=South
|terminus_a={{jct|state=WV|WV|46}} at [[Westernport, Maryland|Westernport]]
|junction={{jct|state=MD|MD|135}} at Westernport<br />
{{jct|state=MD|I|68|US|40}} near [[Frostburg, Maryland|Frostburg]]<br />
{{jct|state=MD|US-Alt|40|dab1=Cumberland}} at Frostburg
|direction_b=North
|terminus_b={{jct|state=MD|US-Alt|40|dab1=Cumberland}} at [[Cumberland, Maryland|Cumberland]]
|counties=[[Allegany County, Maryland|Allegany]]
|previous_type=MD
|previous_route=35
|next_type=MD
|next_route=37
}}

'''Maryland Route 36''' (also known as '''MD&nbsp;36''' or '''Route 36''') is a {{convert|29.43|mi|km|adj=on}} [[state highway]] located in [[Allegany County, Maryland|Allegany County]], [[Maryland]], [[United States]]. MD&nbsp;36's southern terminus is at the [[West Virginia Route 46|WV&nbsp;46]] bridge in [[Westernport, Maryland|Westernport]] and its northern terminus at [[U.S. Route 40 Alternate (Keyser's Ridge-Cumberland)|U.S. Route 40 Alternate]] near [[Cumberland, Maryland|Cumberland]]. Between Westernport and [[Frostburg, Maryland|Frostburg]], it is known as Georges Creek Road, and from Frostburg to Cumberland it is known as Mount Savage Road. Like the majority of Maryland state highways, MD&nbsp;36 is maintained by the [[Maryland State Highway Administration]] (MDSHA).

MD&nbsp;36 serves as the main road through the [[Georges Creek Valley]], a region which is historically known for [[coal mining]], and has been designated by MDSHA as part of the Coal Heritage Scenic Byway.<ref name=mdsha_coal>{{cite web |author= Staff |publisher= Maryland State Highway Administration |url= http://www.sha.state.md.us/exploremd/oed/scenicByways/Coal.pdf |format= PDF |title= Coal Heritage Scenic Byway |accessdate= June 7, 2008 |year= 2008 |archiveurl= https://web.archive.org/web/20070221211032/http://www.sha.state.md.us/exploremd/oed/scenicByways/Coal.pdf |archivedate= February 21, 2007}}</ref> MD&nbsp;36 is the main road connecting the towns of Westernport, [[Lonaconing, Maryland|Lonaconing]], and [[Midland, Maryland|Midland]] in southwestern Allegany County, as well as Frostburg, [[Mount Savage, Maryland|Mount Savage]], and [[Corriganville, Maryland|Corriganville]] in northwestern Allegany County.

==Route description==
MD&nbsp;36 has two main sections: Georges Creek Road, which runs along the [[Georges Creek Valley]], from [[Westernport, Maryland|Westernport]] to [[Frostburg, Maryland|Frostburg]] in southwestern Allegany County, and Mount Savage Road, which runs eastward from Frostburg to [[Cumberland, Maryland|Cumberland]] in northwestern Allegany County. MD&nbsp;36 is a part of the [[National Highway System (United States)|National Highway System]] as a principal arterial from I-68 to US 40 Alternate in Frostburg and from MD&nbsp;35 at Corriganville to US&nbsp;40&nbsp;Alternate in Cumberland.<ref name=HLR /><ref name="National Highway System">{{cite map|publisher=Federal Highway Administration|title=National Highway System: Cumberland, MD|url=http://www.fhwa.dot.gov/planning/national_highway_system/nhs_maps/maryland/cumberland_md.pdf|date=October 1, 2012|section=|format=PDF|accessdate=2014-09-02}}</ref>

[[File:2016-05-05 16 09 08 View north along Maryland State Route 36 (Victory Post Road) in Westernport, Allegany County, Maryland.jpg|thumb|left|View north from the south end of MD 36 in Westernport]]
===Georges Creek Road===
MD&nbsp;36 begins at the [[West Virginia Route 46]] (WV&nbsp;46) bridge in Westernport and runs northeast across western [[Allegany County, Maryland|Allegany County]] as a two-lane road named Georges Creek Road, named for [[Georges Creek (Potomac River)|Georges Creek]], a [[Potomac River]] tributary which the road parallels.<ref name=HLR/> The speed limit for most of the length between Westernport and [[Lonaconing, Maryland|Lonaconing]] is {{convert|50|mph|km/h|abbr=on}}.<ref name=HLR/> A short distance outside Westernport city limits, MD&nbsp;36 intersects [[Maryland Route 937|MD&nbsp;937]], an old alignment of MD&nbsp;36. Near [[Barton, Maryland|Barton]], MD&nbsp;36 intersects [[Maryland Route 935|MD&nbsp;935]], which is the old alignment of MD&nbsp;36 through Barton. MD&nbsp;36 bypasses Barton, climbing the hillside above the Georges Creek Valley before descending back into the valley as it approaches Lonaconing. MD&nbsp;935 returns to MD&nbsp;36 south of Lonaconing, with its northern terminus at MD&nbsp;36.<ref name=HLR />

As the road enters Lonaconing, it narrows and the speed limit drops to {{convert|25|mph|km/h|abbr=on}}, increasing to {{convert|35|mph|km/h|abbr=on}} after it travels through Lonaconing.<ref name=HLR/> MD&nbsp;36 passes through Lonaconing as Main Street, and it intersects [[Maryland Route 657|MD&nbsp;657]] near the center of Lonaconing.<ref name=HLR/> Along Main Street in Lonaconing is the [[Lonaconing, Maryland#The Lonaconing Furnace (1836–1855)|Lonaconing Iron Furnace]], a historic [[blast furnace]] which operated in the early 19th century.<ref name=BaltOhio>{{cite book |last= Dilts |first= James |title= The Great Road: The Building of the Baltimore and Ohio, the Nation's First Railroad, 1828–1853 |publisher= [[Stanford University Press]] |year= 1996 |isbn=0-8047-2629-9 |page= 287}}</ref>

[[Image:MD36 near I-68 in Frostburg.jpg|thumb|right|MD&nbsp;36 approaching Interstate 68 south of Frostburg]]
MD&nbsp;36 then continues toward [[Midland, Maryland|Midland]]. At Midland, there is a sharp curve in the road, and the speed limit drops to {{convert|25|mph|km/h|abbr=on}}.<ref name=HLR/> Along this curve, MD&nbsp;36 intersects Church Street, which connects to [[Maryland Route 936|MD&nbsp;936]], the old alignment of MD&nbsp;36 between Midland and Frostburg. The new alignment of MD&nbsp;36 proceeds northeast, passing near [[Vale Summit, Maryland|Vale Summit]], where it intersects [[Maryland Route 55|MD&nbsp;55]].<ref name=HLR/>

North of the MD&nbsp;55 intersection, MD&nbsp;36 meets [[Interstate 68]] at a [[diamond interchange]] at exit 34.<ref name=HLR/> Near this interchange is [[God's Ark of Safety]], a church famous for its attempt to build a replica of [[Noah's Ark]].<ref name=ark>{{cite news |first= Caitlin |last= Cleary |url= http://post-gazette.com/pg/06106/682602-85.stm |title= If the flood comes too soon, this ark won't be quite ready |work= [[Pittsburgh Post-Gazette]] |date= April 16, 2006 |accessdate= April 16, 2006}}</ref> Between Midland and Frostburg, the speed limit is again {{convert|50|mph|km/h|abbr=on}},<ref name=HLR/> and there is a short section near the Interstate&nbsp;68 interchange where MD&nbsp;36 expands to four lanes. Upon entering Frostburg, MD&nbsp;36 joins [[U.S. Route 40 Alternate (Keyser's Ridge-Cumberland)|{{nowrap|U.S. Route 40 Alternate}}]] as Main Street. MD&nbsp;36 follows Main Street westward through Frostburg, meeting the northern terminus of MD&nbsp;936 at Grant Street. At Depot Street, near the center of Frostburg, MD&nbsp;36 connects to the western depot of the [[Western Maryland Scenic Railroad]]. At the intersection with Water Street, MD&nbsp;36 leaves {{nowrap|U.S. Route 40 Alternate}}, and upon leaving Frostburg city limits its name changes to Mount Savage Road.<ref name=HLR />

[[Image:MD 36 near the Mount Savage Castle.jpg|thumb|left|Route&nbsp;36 approaching Mount Savage, with part of the Mount Savage Castle visible in the background]]
===Mount Savage Road===
After leaving Frostburg, MD&nbsp;36 is known as Mount Savage Road, as it travels eastward, perpendicular to its signed direction, toward [[Mount Savage, Maryland|Mount Savage]]. North of [[Eckhart Mines, Maryland|Eckhart Mines]], MD&nbsp;36 meets [[Maryland Route 638|MD&nbsp;638]], which connects MD&nbsp;36 to {{nowrap|U.S. Route 40 Alternate}} in Eckhart Mines.<ref name=HLR/> The road between Frostburg and Mount Savage is particularly curvy, and includes several [[hairpin turn]]s near Frostburg. The speed limit on this section of the road is {{convert|35|mph|km/h|abbr=on}}.<ref name=HLR/>

As the road enters Mount Savage, it passes by the [[Mount Savage Castle]], a Scottish-style castle built in 1840, which currently operates as a [[bed-and-breakfast]]. In Mount Savage, the route narrows as it follows Main Street,<ref name=HLR/> and the road is frequently obstructed by parked cars, making it difficult for two cars to pass by each other. East of Mount Savage, the route widens and its speed limit gradually increases to {{convert|50|mph|km/h|abbr=on}}.<ref name=HLR/>

In [[Barrelville, Maryland|Barrelville]], MD&nbsp;36 intersects [[Maryland Route 47|MD&nbsp;47]], which connects it with [[Pennsylvania Route 160|PA&nbsp;160]] in [[Somerset County, Pennsylvania|Somerset County]].<ref name=HLR/> From its intersection with MD&nbsp;47 to its terminus at [[Cumberland, Maryland|Cumberland]], MD&nbsp;36 follows newer alignments, with the old alignments being designated [[Maryland Route 831|MD&nbsp;831]]. At [[Corriganville, Maryland|Corriganville]], MD&nbsp;36 intersects [[Maryland Route 35|MD&nbsp;35]], which connects it with [[Pennsylvania Route 96|PA&nbsp;96]] in [[Bedford County, Pennsylvania|Bedford County]].<ref name=HLR/> MD&nbsp;36 ends at U.S. Route 40 Alternate at the [[Cumberland Narrows|Narrows]] near Cumberland.<ref name=HLR />

==History==
[[Image:MD 36 at MD 55 south of Frostburg, Maryland.jpg|thumb|right|Route&nbsp;36 approaching the southern terminus of Maryland Route&nbsp;55 in Vale Summit]]
MD&nbsp;36 passes through the [[Georges Creek Valley]], which has a long history of [[coal mining]]. In recognition of this, the [[Maryland State Highway Administration|MDSHA]] has designated MD&nbsp;36 as part of the Coal Heritage Scenic Byway.<ref name=mdsha_coal /> Coal mining was a major industry in western Maryland in the 19th century, with railroads being the major route connecting the coal mines to markets outside the Georges Creek Valley.<ref name=Cumberland_coal>{{cite web |author= Staff |publisher= [[U.S. National Park Service]] |url= http://www.nps.gov/history/nr/travel/cumberland/history.htm |accessdate= July 25, 2008 |title= Cumberland History}}</ref> [[Underground mining (soft rock)|Deep mining]], which was the primary mining method used in western Maryland, declined in use after [[World War&nbsp;II]], replaced primarily by [[surface mining]].<ref name=MD_coal>{{cite web |author= Staff |publisher= [[Office of Surface Mining Reclamation and Enforcement]] |title= Maryland |url= http://www.osmre.gov/pdf/maryland.pdf |format= PDF |accessdate= July 25, 2008 |year= 1997 |archiveurl= https://web.archive.org/web/20061008164802/http://www.osmre.gov/pdf/maryland.pdf |archivedate= October 8, 2006 |deadurl= yes}}</ref> Although Maryland coal production is now only a small fraction of total U.S. coal production,<ref name=MD_coal /> coal from the Georges Creek Valley is used to power the [[AES Corporation|AES]] [[Warrior Run Generating Station|Warrior Run]] power plant in Cumberland.<ref name=Coal_AES>{{cite news |work= [[Washington Examiner]] |date= June 30, 2008 |first= Andrew |last= Cannarsa |title= Coal, aggregate industries facing hurdles |url= http://washingtonexaminer.com/2008/06/coal-aggregate-industries-facing-hurdles/46856 |accessdate= July 25, 2008}}</ref>

MD&nbsp;36 was assigned a route number before 1927, earlier than most of the other Maryland state highways.<ref name=mdsha_1910_map>{{cite map |publisher= Maryland Geological Survey |title= Map of Maryland showing the Highways |year= 1910 |url= http://www.mdhighwaycentennial.com/images/template/gallery/maps/1910-1-500000.jpg |accessdate= June 30, 2008 |archiveurl= https://web.archive.org/web/20080817064225/http://www.mdhighwaycentennial.com/images/template/gallery/maps/1910-1-500000.jpg |archivedate= August 17, 2008}}</ref><ref name=mdsha_1927_map>{{cite map |publisher= Maryland Geological Survey |title=Map of Maryland showing State Road system |year= 1927 |url= http://www.mdhighwaycentennial.com/images/template/gallery/maps/1927SIDE1.jpg |accessdate= June 8, 2008 |archiveurl= https://web.archive.org/web/20080826164131/http://www.mdhighwaycentennial.com/images/template/gallery/maps/1927SIDE1.jpg |archivedate= August 26, 2008}}</ref> The original alignment of MD&nbsp;36 in southern Allegany County closely paralleled the [[Georges Creek Railroad]]. Later realignments have shifted MD&nbsp;36 away from the railroad in several locations, but three crossings remain: one north of Lonaconing, one south of Lonaconing near the [[Maryland Route 935|MD&nbsp;935]] intersection,<ref name=gm_rail_crossing_2>{{google maps |url= https://maps.google.com/?ie=UTF8&ll=39.55236,-78.989693&spn=0.003615,0.009388&z=17 |title= Map of crossing near Lonaconing, MD |accessdate= June 5, 2008}}</ref> and a third crossing near Westernport.<ref name=gm_rail_crossing_3>{{google maps |url= https://maps.google.com/?ie=UTF8&ll=39.501905,-79.047511&spn=0.007235,0.018775&z=16 |title= Map of crossing near Westernport, MD |accessdate= June 5, 2008 |link=no}}</ref>

Over the years, multiple new alignments of MD&nbsp;36 have been built for various reasons, such as to smooth out curves in the road. Several of the old alignments have been assigned route numbers of their own. The southernmost of these is [[Maryland Route 937|MD&nbsp;937]], which consists of the old alignment through Westernport. Prior to the construction of the bridge connecting MD&nbsp;36 to [[West Virginia Route 46|WV&nbsp;46]], MD&nbsp;937 was the alignment of MD&nbsp;36 through Westernport, ending at [[Maryland Route 135|MD&nbsp;135]]. In [[Barton, Maryland|Barton]], [[Maryland Route 935|MD&nbsp;935]] carries the old alignment of MD&nbsp;36. The longest of the old alignment sections is [[Maryland Route 936|MD&nbsp;936]], which runs from Midland to Frostburg, and was bypassed in the 1970s with a new alignment of MD&nbsp;36 following part of [[Maryland Route 55|MD&nbsp;55]] and connecting to {{nowrap|[[Interstate 68]]}}. Prior to this change, MD 55 ended in Midland;<ref>{{cite map |publisher= Maryland State Roads Commission |cartography= Bureau of Traffic |title= Map of Maryland |year= 1960 |url= http://www.mdhighwaycentennial.com/images/template/gallery/maps/1960SIDE1.jpg |accessdate= June 8, 2008 |archiveurl= https://web.archive.org/web/20080826164207/http://www.mdhighwaycentennial.com/images/template/gallery/maps/1960SIDE1.jpg |archivedate=August 26, 2008 }}</ref> it has since been truncated to its current terminus at Vale Summit.<ref name=mdsha_1979_map>{{cite map |publisher= Maryland State Highway Administration |title= State of Maryland State Highway System |year= 1979 |url= http://www.mdhighwaycentennial.com/images/template/gallery/maps/1979-80SIDE1.jpg |accessdate= June 30, 2008 |archiveurl= https://web.archive.org/web/20080826164148/http://www.mdhighwaycentennial.com/images/template/gallery/maps/1979-80SIDE1.jpg |archivedate= August 26, 2008}}</ref>

North of Frostburg, several old alignments are designated as [[Maryland Route 831|MD&nbsp;831]], though these segments of road are not signed. Among these segments of road are Kriegbaum Road (designated as [[Maryland Route 831|MD&nbsp;831C]]), and Old Mount Savage Road (designated as [[Maryland Route 831|MD&nbsp;831A]]). Kriegbaum Road splits from MD&nbsp;36 west of [[Corriganville, Maryland|Corriganville]] and runs through Corriganville, returning to MD&nbsp;36 east of the town. Old Mount Savage Road intersects MD&nbsp;36 west of the [[Cumberland Narrows]], and runs southward to intersect {{nowrap|U.S. Route 40 Alternate}} near its current intersection with MD&nbsp;36.<ref name=HLR/>

==Junction list==
{{jcttop|state=MD|county=Allegany|length_ref=<ref name=HLR>{{Maryland HLR |year= 2013 |county1=  Allegany |link=yes |accessdate= October 21, 2010}}</ref>}}
{{MDint
|location=Westernport
|lspan=3
|mile=0.00
|road={{jct|state=WV|WV|46|dir1=east|city1=Piedmont}}
|notes=Southern terminus; [[West Virginia]] state line at [[Potomac River]]
}}
{{MDint
|mile=0.06
|road={{jct|state=MD|MD|135|location1=[[Keyser, West Virginia|Keyser, WV]]|city2=Bloomington}}
}}
{{MDint
|mile=1.44
|road={{jct|state=MD|MD|937|dir1=south|name1=Creek Side Drive|city1=Franklin}}
|notes=Former alignment of MD&nbsp;36
}}
{{MDint
|location=Barton
|mile=4.07
|road={{jct|state=MD|MD|935|dir1=north|name1=Legislative Road}}
|notes=Former alignment of MD&nbsp;36
}}
{{MDint
|location=Lonaconing
|lspan=2
|mile=6.85
|road={{jct|state=MD|MD|935|dir1=south|name1=Legislative Road}}
}}
{{MDint
|mile=8.03
|road=Douglas Avenue north
|notes=Former [[Maryland Route 657|MD&nbsp;657]]
}}
{{MDint
|location=Midland
|mile=11.06
|road={{jct|state=MD|MD|936|dir1=north}} via Church Street
|notes=Former alignment of MD&nbsp;36
}}
{{MDint
|location=Vale Summit
|mile=14.04
|road={{jct|state=MD|MD|55|dir1=north|name1=Vale Summit Road|city1=Clarysville}}
}}
{{MDint
|location=Frostburg
|lspan=5
|mile=15.05
|road={{jct|state=MD|I|68|US|40|name2=National Freeway|city1=Cumberland|location2=[[Morgantown, West Virginia|Morgantown, WV]]}}
|notes=Exit&nbsp;34 on I-68
}}
{{MDint
|mile=16.44
|road={{jct|state=MD|MD|743|dir1=east|name1=Old National Pike|city1=Eckhart Mines}}
}}
{{MDint
|mile=16.53
|road={{jct|state=MD|US-Alt|40|dab1=Cumberland|dir1=east|name1=[[National Pike]]}}
|notes=MD&nbsp;36 joins {{nowrap|Alt US 40}}
}}
{{MDint
|mile=17.07
|road={{jct|state=MD|MD|936|dir1=south|name1=Grant Street}}
}}
{{MDint
|mile=17.60
|road={{jct|state=MD|US-Alt|40|dab1=Cumberland|dir1=west|name1=[[National Pike]]}}
|notes=MD&nbsp;36 leaves {{nowrap|Alt US 40}}
}}
{{MDint
|location=Zihlman
|mile=20.36
|road={{jct|state=MD|MD|638|dir1=south|name1=Parkersburg Road|city1=Eckhart Mines}}
}}
{{MDint
|location=Barrelville
|mile=24.34
|road={{jct|state=MD|MD|47|dir1=north|name1=Barrelville Road|city1=Barrelville}}
|notes=Connects MD&nbsp;36 to [[Pennsylvania Route 160|PA&nbsp;160]]
}}
{{MDint
|location=Corriganville
|lspan=3
|mile=26.64
|road={{jct|state=MD|MD|831|dir1=east|name1=Kreigbaum Road}}
|notes=Officially MD 831C; former alignment of MD&nbsp;36
}}
{{MDint
|mile=27.34
|road={{jct|state=MD|MD|35|dir1=north|name1=Ellerslie Road|city1=Ellerslie}}
|notes=Connects MD&nbsp;36 to [[Pennsylvania Route 96|PA&nbsp;96]]
}}
{{MDint
|mile=27.51
|road={{jct|state=MD|MD|831|dir1=west|name1=Kreigbaum Road}}
|notes=Officially MD 831C
}}
{{MDint
|location=Cumberland
|lspan=2
|mile=29.05
|road={{jct|state=MD|MD|831|dir1=south|name1=Old Mount Savage Road}}
|notes=Officially MD 831A; former alignment of MD&nbsp;36
}}
{{MDint
|mile=29.43
|road={{jct|state=MD|US-Alt|40|dab1=Cumberland|name1=[[National Pike]]}}
|notes=Northern terminus
}}
{{jctbtm}}

==See also==
*{{Portal-inline|Maryland Roads}}

==References==
{{Reflist|30em}}

==External links==
{{Attached KML|display=title,inline}}
{{Commons category|Maryland Route 36}}
* [http://www.m-plex.com/roads/mdmplex/mp_altus40_md36.html Photographs of the section of Route 36 which overlaps with Route 40]
* [http://mdroads.com/routes/022-039.html#md036 MDRoads.com article on Maryland Route 36]
{{featured article}}

[[Category:State highways in Maryland|036]]
[[Category:Roads in Allegany County, Maryland|Maryland Route 036]]