{{Infobox university
 |name              = '''Banff International Research Station for Mathematical Innovation and Discovery'''
 |native_name       = BIRS
 |image_name        = Banff International Research Station.jpg
 |caption           = Banff International Research Station
 |latin_name        = 
 |motto             = 
 |mottoeng          = 
 |established       = 2003
 |closed            = 
 |type              = [[science]], [[mathematics]],<br>& [[research]] institute 
 |affiliation       = 
 |endowment         = 
 |officer_in_charge = 
 |chairman          = 
 |chancellor        = 
 |president         = 
 |superintendent    = 
 |provost           = 
 |vice_chancellor   = 
 |rector            = 
 |principal         = 
 |dean              = 
 |director          = [[Nassif Ghoussoub]]
 |head_label        = 
 |head              = 
 |faculty           = 
 |staff             = 
 |students          = 
 |undergrad         = 
 |postgrad          = 
 |doctoral          = 
 |other             = 
 |city              = [[Banff, Alberta|Banff]]
 |province          = [[Alberta]]
 |country           = <br>{{CAN}}
 |coor              = {{coord|51.1724|-115.5636|type:edu_region:CA|display=inline,title}}
 |campus            = [[Banff Centre|The Banff Centre]] in<br>[[Banff National Park]]
 |former_names      = 
 |free_label        = 
 |free              = 
 |sports            = 
 |colours           = 
 |nickname          = 
 |mascot            = 
 |athletics         = 
 |affiliations      = [[University of British Columbia]], [[Pacific Institute for the Mathematical Sciences]], [http://www.mitacs.ca MITACS], [http://www.mprime.ca MPrime], [[MSRI]]
 |website           = [http://www.birs.ca/ www.birs.ca]
 |logo              = 
 |footnotes         = 
}}

The '''Banff International Research Station (BIRS) for Mathematical Innovation and Discovery''' was established in 2003.<ref name="NSERC">{{cite web|title=NSERC Grant Details|url=http://www.outil.ost.uqam.ca/CRSNG/Detail.aspx?Cle=483160&Langue=2|publisher=Natural Sciences and Engineering Research Council of Canada|accessdate=22 April 2013}}</ref> It provides an independent research institute for the mathematical sciences in North America, a counterpart to the [[Mathematical Research Institute of Oberwolfach]] in Europe.<ref>{{cite web|title=World renowned mathematicians to visit Banff, study ‘Whitney problems’|url=http://www.ucalgary.ca/news/utoday/april11-2013/world-renowned-mathematicians-to-visit|publisher=University of Calgary|accessdate=19 April 2013}}</ref>  The research station, commonly known by its acronym, "BIRS", hosts over 2000 international scientists each year to undertake research collaboration in the [[mathematical sciences]].<ref>{{cite web|title=Banff math centre gets $10M from NAFTA partners|url=http://www.cbc.ca/news/technology/story/2010/09/27/banff-math-centre.html|publisher=CBC News|accessdate=19 April 2013}}</ref>

== Research activities ==

The research that takes place at the Banff International Research Station is either in [[pure mathematics]], [[applied mathematics]], or in other areas of science where they [[Mathematical sciences|intersect with mathematics]].

<blockquote>
"BIRS embraces all aspects of the [[Mathematical sciences|mathematical]], [[Computational science|computational]] and [[Statistical sciences|statistical]] sciences from the most fundamental challenges of [[Pure mathematics|pure]] and [[applied mathematics]], [[Theoretical computer science|theoretical]] and [[Computer_science#Applied_computer_science|applied computer science]], [[statistics]], and [[mathematical physics]], to [[Financial mathematics|financial]] and [[industrial mathematics]], as well as the mathematics of [[information technology]], and the [[life sciences]]."<ref>{{cite web|title=Message from the Director|url=http://www.birs.ca/about/Message-from-director|publisher=BIRS Website|accessdate=19 April 2013}}</ref> 
</blockquote>

There are a [http://scholar.google.ca/scholar?start=0&q=Banff+International+Research+Station&hl=en&as_sdt=0,5 wide range of research publications] citing lectures, meetings and reports from BIRS.

== Research program ==

The Banff International Research Station hosts [http://www.birs.ca/programs/general-program-descriptions five types of meetings]:

# ''5-Day Workshops'': These make up the core program at BIRS, with up to 42 participants per workshop, 48 weeks per year. Some workshops have only 21 participants, and they share a week at BIRS, running concurrently.
# ''2-Day Workshops'': Weekend workshops, typically consisting of 25 people, and typically from the surrounding areas in [[Alberta]] and [[British Columbia]].
# ''Focused Research Groups'':  Up to 8 people from different institutions meet for 1–2 weeks, to work on a specific problem or finish up major projects.
# ''Research in Teams'':  2–4 people from different institutions meet for 1–2 weeks to concentrate on their research.
# ''Summer Schools and Training Camps'': instructional meetings for up to 40 students for up to 14 days.

The [http://www.birs.ca/events/2013/5-day-workshops core program of 5-day workshops] is created two years in advance. Every summer, BIRS issues a [http://www.birs.ca/announcements/2012-06-15/call-for-proposals-2014 Call for Proposals], soliciting applications for workshops from the global scientific community. Each year, it gets more competitive to get a space in the 48 available weeks at BIRS: 79 proposals were received for the 2003 program, and 168 were received for the 2014 program.<ref name=2014Program>{{cite web|title=The 2014 Scientific Program Announcement|url=http://www.birs.ca/announcements/2013-02-08/2014-scientific-program|publisher=BIRS Website|accessdate=23 April 2013}}</ref> An extensive peer-review process by international experts culminates in the selection of the scientific program for a given year.<ref name=2014Program /><ref name=REVIEW>{{cite web|title=The Review Process: Ensuring High Calibre Research|url=http://www.birs.ca/about/governance/scientific-management/review-process|publisher=BIRS Website|accessdate=23 April 2013}}</ref>

Summer schools and training camps must apply through the same process as 5-day workshops. An example of a summer school is the [http://www.birs.ca/events/2012/summer-schools/12ss012 International Mathematical Olympiad (IMO) training camp], to prepare high school students for competing at the [http://www.imo-official.org/ IMO]. The other types of meetings are far less competitive, and may be applied for at any time, through the [https://www.birs.ca/proposals/ BIRS website].

== Meeting facilities ==

The Banff International Research Station occupies two buildings on the campus of the [[Banff Centre]], in [[Banff National Park]]. One of the buildings, Corbett Hall, is a residence building that provides bedrooms, a common lounge area, a small library, and space for small teams of people to work. The other building, TransCanada PipeLines Pavilion, hosts administrative offices, two lecture rooms, and a series of smaller rooms for break-out sessions and research teams.<ref>{{cite web|title=Description of Facilities|url=http://www.birs.ca/facilities/description-of-facilities|publisher=BIRS Website|accessdate=19 April 2013}}</ref>  As part of the Banff Centre campus, BIRS researchers have full access to all of its [http://www.banffcentre.ca/conferences/ amenities and services].

The idea behind this choice of location for a research facility is to create an atmosphere where scientists can remove themselves from day-to-day life, and immerse themselves in their research.<ref>{{cite journal|last=Klarreich|first=Erica|journal=Nature|date=30 August 2001|issue=412|page=846|title=...as mathematicians beat retreat to Alberta|doi=10.1038/35091220|url=http://www.nature.com/nature/journal/v412/n6850/full/412846c0.html|accessdate=22 April 2013}}</ref>

== Automated lecture capture ==

In 2012, the Banff International Research Station installed a fully automated [[lecture capture]] system.<ref name=AVR>{{cite web|title=Automated Video Recording|url=http://www.birs.ca/facilities/automated-video |publisher=BIRS Website|accessdate=23 April 2013}}</ref>  It provides [[Streaming media|live video streaming]] and [[video recording]] of the lectures that take place in its main lecture room. Video recordings are automatically posted on the [http://www.birs.ca/videos/ BIRS website] within a few minutes after a lecture ends. Use of the system is opt-in, decided by the individual lecturers at the time of their lecture, via a [[touchscreen]] panel in the lecture room. The automated system at BIRS employs high quality cameras to ensure that mathematics written on chalkboards can be seen clearly. Embedded [[microphones]] and [[Audio signal processing|audio processing systems]] capture both the lecturer and questions from the audience.

Recent research videos recorded at BIRS are also available in the [https://itunes.apple.com/us/podcast/banff-international-research/id307566214 iTunes podcast directory].

== Funding ==
The Banff International Research Station is funded by four governments:

# The federal government of Canada, through the [[Natural Sciences and Engineering Research Council]] (NSERC)<ref name=NSERC /> 
# The provincial government of Alberta, through [http://www.collectionscanada.gc.ca/eppp-archive/100/205/301/ic/cdc/www.abheritage.ca/abinvents/innovation/policy/provincial_policy_3.htm Alberta Science and Research Authority] (ASRA)<ref>{{cite web|title=Alberta Science and Research Investments Program Annual Report 2008|url=http://eae.alberta.ca/media/204480/asrip_08_annual_final_2.pdf|accessdate=19 April 2013}}</ref>
# The U.S. [[National Science Foundation]] (NSF)<ref name=NSF>{{cite web|title=NSF funding award abstract|url=http://nsf.gov/awardsearch/showAward?AWD_ID=0969578|publisher=National Science Foundation|accessdate=23 April 2013}}</ref> 
# Mexico's [[CONACYT|National Science and Technology Council]], (CONACYT)<ref>{{cite web|title=University of British Columbia Media Release, 2005|url=http://www.publicaffairs.ubc.ca/media/releases/2005/mr-05-125.html|accessdate=19 April 2013}}</ref>

== See also ==

* BIRS Director, [[Nassif Ghoussoub]]
* [[The Banff Centre]]
* [[Banff, Alberta]]
* [[Pacific Institute for the Mathematical Sciences]]
* [[Mathematical Sciences Research Institute]]
* [http://www.birs.ca/testimonials Comments from BIRS researchers]

== References ==

{{reflist}}

[[Category:Research stations]]
[[Category:Mathematical institutes]]
[[Category:Research institutes in Canada]]
[[Category:International research institutes]]
[[Category:Buildings and structures in Banff, Alberta]]
[[Category:Research institutes established in 2003]]
[[Category:2003 establishments in Alberta]]