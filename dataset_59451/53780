{{Infobox academic division
|name           = School of Accounting and Finance
|former_names   = School of Accountancy
|type           = School
|affiliation    = [[University of Waterloo]]
|parent         = [[University of Waterloo Faculty of Arts|Faculty of Arts]]
|image_name     = File:WAT ACCOUNTING LOGO.JPG
|image_size     = 300px
|image_alt      = 
|caption        = 
|established    = {{Start date|1985}}
|closed         = <!-- {{End date|YYYY}} -->
|city           = [[Waterloo, Ontario|Waterloo]]
|province       = [[Ontario]]
|country        = [[Canada]]
|director       = James Barnett
|academic_staff = 54
|undergrad      = 1500
|postgrad       = 275
|alumni         = https://uwaterloo.ca/saf/2016-17-saf-mentor-profiles
|symbol         = 
|website        = {{URL|accounting.uwaterloo.ca}}
}}

The '''School of Accounting and Finance (SAF)''' at [[University of Waterloo]] is a ''professional school'' within the Faculty of Arts. The School was established in 1985 under the name 'School of Accountancy'. Its name was changed in 2008 to better reflect its program offering. The School has made important contributions to the development of the accounting profession in Canada.<ref name="Vision and Challenge">{{cite web | url = http://www.accounting.uwaterloo.ca/history.pdf | title = Vision and Challenge: Accounting Education at the University of Waterloo 1981–2001| accessdate = 2009-10-23}}</ref> The faculty is a significant producer of accounting research and industry teaching material.<ref name="Vision and Challenge"/> Alumni hold leadership positions in public accounting, industry, government and academia.<ref>http://accounting.uwaterloo.ca/alumni/fellowships/fellowship_honourees.htm</ref> Today, nearly 1000 students are enrolled in the School's programs. In September, 2009 a new {{convert|52,000|sqft|m2|sigfig=1}} building was officially opened to house the School.

The School of Accounting and Finance offers the largest accounting and finance undergraduate [[cooperative education]] (co-op) program in Canada.<ref name="The Directory of Canadian Universities">{{cite web | url = http://www.aucc.ca/can_uni/our_universities/waterloo_e.html | title = The Directory of Canadian Universities: The University of Waterloo | accessdate = 2007-05-31}}</ref>
Most of the School's programs are uniquely accredited by Canadian industry associations. Access to exceptional faculty, advanced professional standing and the opportunity for paid work experience in between academic terms make admission to undergraduate programs administered by the School extremely competitive.{{Citation needed|date=September 2011}}

==Programs of study==

The School of Accounting and Finance offers three undergraduate programs, one post-baccalaureate diploma, two graduate programs and one doctoral program. Additionally, the School jointly administers one undergraduate program and one graduate program with the Faculty of Mathematics and one undergraduate program with the Faculty of Science.<ref>http://accounting.uwaterloo.ca/</ref> These programs are:
''School of Accounting and Finance Programs'': Bachelor of Accounting and Financial Management (BAFM); Bachelor of Computing and Financial Management (BCFM), Biotechnology/Chartered Professional Accountancy; Mathematics/Chartered Professional Accountantcy; Master of Accounting (MAcc), Master of Taxation (MTax) and PhD in Accounting.

==Undergraduate Education==
''Bachelor of Accounting and Financial Management Co-op (BAFM)''

Students in the BAFM Program may customize a degree so long as the basic minimum requirements set out in the BAFM degree requirements are met. Graduates of the BAFM program typically aspire to a host of professional accounting and/or finance credentials such as the Chartered Professional Accountant (CPA), Certified Management Accountant (CMA),  Chartered Business Valuator (CBV) Chartered Financial Analyst (CFA) designation. All School of Accounting and Finance program graduates are eligible to apply to the School's MAcc program.

''Mathematics and Chartered Accountancy Co-op (Math/CPA)''

Students in the MathCPA program may customize a degree so long as the basic minimum requirements set out in Mathematics Chartered Accountancy Plan are met. Typically, students take math courses along with the accounting and finance courses. The work-term sequence in structured similar to the BAFM schedule. Students in the program may obtain co-op employment deemed by professional bodies to be ''qualifying practical experience'' for accreditation purposes. 

Graduates of the MathCPA program typically aspire to a CPA designation. In addition to preparation for CPA accreditation, three study options are offered that allow students to explore complimentary areas: finance, actuarial science, and operations research management. Students who complete the Finance Option are highly prepared to pursue the CFA curriculum and accreditation process. Students who complete the Actuarial Science Option are suited to explore professional actuary exams. Students who complete the Operations Research Management Option are suited for specialized roles in public accounting, business or research. MathCPA students/graduates are eligible to apply to the School's MAcc program.

The Math/CA program was renamed to Math/CPA in 2013 to better reflect the professional qualifications that the program is designed to prepare students for.

''Biotechnology and Chartered Accountancy Co-op (Biotech/CPA)''

Students in the ScienceCPA program take science courses along with the accounting and finance courses. Students are especially well-prepared for work in public accounting or with biotechnology-intensive businesses. Student take two of four co-op terms during busy season (Jan-Apr). Students in the program may obtain co-op employment deemed by professional bodies to be ''qualifying practical experience'' for accreditation purposes. ScienceCPA students have access to the University's "Chartered Accounting" job posting and application cycle. ScienceCPA students/graduates are eligible to apply to the School's MAcc program.

The Biotech/CA program was renamed to Biotech/CPA in 2013 to better reflect the professional qualifications that the program is designed to prepare students for.

''Bachelor of Computing and Financial Management (BCFM)''

This program is offered jointly with the [[David R. Cheriton School of Computer Science]] in the Faculty of Mathematics. Students take computer science courses along with accounting and finance courses. Upon completion, these students will receive a unique degree in Canada, "Bachelor of Computing and Financial Management". Students in this program typically complete six co-op work terms, with the first commencing between semester two and three. The work term sequence is: May-Aug, Jan-Apr, Sep-Dec, May-Aug, Jan-Apr and Sep-Dec. CFM students/graduates are eligible to apply to the School's MAcc program. CFM students are well-suited to pursue a number of professional accreditation in finance and/or advanced business or science degrees.

==University Reputation==
*According to Maclean's University Rankings,<ref>{{cite web|accessdate=2008-05-19|url=http://www.macleans.ca/universities|title=Maclean's University Rankings|publisher=Maclean's }}</ref> University of Waterloo ranks #1 in the following categories:
**Best Overall
**Most Innovative
**Best Source of Leaders of Tomorrow
*The Globe & Mail gave the University of Waterloo the followings grades in its 2007 annual report card.<ref>{{cite web|accessdate=2008-05-19|url=http://www.globecampus.ca/in-the-news/globecampusreports/#university-report-card-2007|title=Globe & Mail - Globe Campus|publisher=The Globe & Mail }}</ref>
**Reputation amongst employers = A+
**Academic reputation = A
**Quality of education = A-
**Libraries = A-
*The Mathematics & Chartered Accountancy and Biotechnology & Chartered Accountancy programs are some of the most competitive in Canada.<ref>{{cite web|accessdate=2007-05-19|url=http://www.thestar.com/article/324667|title=The Most Competitive Programs in Canada|publisher=The Star }}</ref>
**The Mathematics program admission process is based on individual selection from students with a minimum fourth year high school average in the low-90's. In 2009, around 900 applicants applied for 80 spots.
**The Biotechnology program admission is based on individual selection from students with a fourth year high school average in the mid-90's.<ref>{{cite web|accessdate=2008-10-19|url=http://www.findoutmore.uwaterloo.ca/admissions/programs.php|title=Become a Student|publisher=University of Waterloo }}</ref> On average, 10 to 15 students are admitted to the program.

==References==
{{reflist|2}}

==External links==
*[https://uwaterloo.ca/school-of-accounting-and-finance/]

{{University of Waterloo}}

[[Category:University of Waterloo|Accountancy and Finance]]
[[Category:Accounting in Canada]]
[[Category:Accounting schools]]
[[Category:1985 establishments in Ontario]]