{{Infobox journal
| title = European Judaism
| cover = [[File:Jnl cover eurojud.jpg]]
| abbreviation = Eur. Judaism
| editor = Jonathan Magonet
| discipline = [[Judaism studies]]
| publisher = [[Berghahn Books]] in association with the [[Leo Baeck College]] and the Michael Goulston Education Foundation
| frequency = Biannually
| history = 1968-present
| website = http://journals.berghahnbooks.com/ej/
| link1 = http://berghahn.publisher.ingentaconnect.com/content/berghahn/ejud
| link1-name = Online archives
| ISSN = 0014-3006
| eISSN = 1752-2323
| LCCN = 79007340
| OCLC = 02309096
}}
'''''European Judaism''''' is a biannual [[academic journal]] published by [[Berghahn Books]] in association with the [[Leo Baeck College]] and the Michael Goulston Education Foundation. It was established in 1968 and covers [[Judaism studies]] concerning [[Judaism]] in Europe. The [[editor-in-chief]] is [[Jonathan Magonet]].

== Abstracting and indexing ==
''European Judaism'' is indexed and abstracted in:
{{columns-list|colwidth=30em|
* [[ATLA Religion Database]]
* [[Educational Resources Information Center]]
* [[Index for Jewish Periodicals]]
* [[Index of Articles on Jewish Studies]]
* [[Infotrac]]
* [[International Bibliography of Periodical Literature]]
* [[International Bibliography of Book Reviews of Scholarly Literature on the Humanities and Social Sciences]]
* [[International Bibliography of Periodicals]]
* [[MLA International Bibliography]]
* [[Social Service Abstracts]]
* [[Sociological Abstracts]]
* [[TOC Premier]]
}}

== External links ==
* {{Official website|http://journals.berghahnbooks.com/ej/}}

[[Category:Judaic studies journals]]
[[Category:Berghahn Books academic journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 1968]]
[[Category:English-language journals]]