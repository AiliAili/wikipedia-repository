{{Infobox journal
| title = Journal of Policy Analysis and Management
| discipline = [[Public administration]]
| abbreviation = J. Pol. Anal. Manag.
| editor =  [[Maureen Pirog]]
| publisher = [[Wiley-Blackwell]] on behalf of the [[Association for Public Policy Analysis and Management]]
| frequency = Quarterly
| history = 1981–present
| impact = 2.576
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1520-6688
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1520-6688/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1520-6688/issues
| link2-name = Online archive
| ISSN  = 0276-8739
| eISSN = 1520-6688
| LCCN = 81649164
| OCLC = 07442272
}}
The '''''Journal of Policy Analysis and Management''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] covering issues and practices in [[policy analysis]] and [[public management]]. It was established in 1981 and contains books reviews and a department devoted to discussing ideas and issues of importance to practitioners, researchers, and academics. It is the official journal of the [[Association for Public Policy Analysis and Management]] and published by [[Wiley-Blackwell]]. The current [[editor-in-chief]] is [[Maureen Pirog]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.576, ranking it 31st out of 333 journals in the category "Economics" and 2nd out of 46 journals in the category "Public Administration".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Economics and Public Administration |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== History ==
The [[Association for Public Policy Analysis and Management]] established the ''Journal of Policy Analysis and Management'' in 1981 through the merger of two other journals – "Policy Analysis" and "Public Policy."

== Editors-in-chief ==
The following persons have been editors-in-chief of the ''Journal of Policy Analysis and Management'':
{{columns-list|colwidth=30em|
* 2004-Present [[Maureen Pirog]]
* 1999-2004 Peter Reuter
* 1994-1999 Janet Rothenberg-Pack
* 1989-1994 Lee Friedman
* 1985-1989 David Weimer
* 1981-1985 [[Raymond Vernon]]
}}

== Raymond Vernon Memorial Prize ==
Every year, the association awards the Raymond Vernon Memorial Prize for the best article published in the current volume of the journal. The prize selection committee usually is drawn from the [[editorial board]] and [[Wiley-Blackwell]] underwrites all expenses related to the prize.

== Journal sections ==
The journal has five special sections: Point/Counterpoint, Policy Retrospectives, Professional Practice, Methods for Policy Analysis, and Book Reviews. These sections usually publish shorter, specialized articles on those topics.

== References ==
{{Reflist}}

== External links ==
* {{Official|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1520-6688}}

[[Category:Policy analysis journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Quarterly journals]]
[[Category:Economics journals]]
[[Category:Publications established in 1981]]
[[Category:English-language journals]]