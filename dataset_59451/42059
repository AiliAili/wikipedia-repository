<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Feeling
 | image=Aerosport 2013 (Igualada) EC-GE2 (aircraft).JPG
 | caption=DTA Feeling at the [[Aerosport air show]] in 2013 
}}{{Infobox Aircraft Type
 | type=[[Ultralight trike]]
 | national origin=[[France]]
 | manufacturer=[[DTA sarl]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]24,967 (Feeling 582 Diva model, 2011)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''DTA Feeling''' is a [[France|French]] [[ultralight trike]], designed and produced by [[DTA sarl]] of [[Montélimar]]. The aircraft is supplied complete and ready-to-fly.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 210. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The aircraft has a great deal of parts commonality with the [[DTA Evolution]], but includes a [[cockpit fairing]], windshield, panniers, and a standard instrumentation package. The Feeling was designed to comply with the [[Fédération Aéronautique Internationale]] [[microlight]] category, including the category's maximum gross weight of {{convert|450|kg|lb|0|abbr=on}}. The aircraft has a maximum gross weight of {{convert|450|kg|lb|0|abbr=on}}. It features a [[Flying wires|cable-braced]] [[hang glider]]-style [[DTA Diva]] [[high-wing]], weight-shift controls, a two-seats-in-[[tandem]] open cockpit, [[tricycle landing gear]] with [[wheel pants]] and a single engine in [[pusher configuration]].<ref name="WDLA11" />

The aircraft is made from bolted-together [[aluminum]] tubing, with its double surface wing covered in [[Dacron]] sailcloth. Its {{convert|9.40|m|ft|1|abbr=on}} span wing is supported by a single tube-type [[kingpost]] and uses an "A" frame weight-shift control bar. The powerplant is a twin cylinder, liquid-cooled, [[two-stroke]], [[dual-ignition]] {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine, with the four cylinder, air and liquid-cooled, [[four-stroke]], [[dual-ignition]] {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912]] or {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912S]] engines optional.<ref name="WDLA11" />

With the Rotax 582 engine, the aircraft has an empty weight of {{convert|200|kg|lb|0|abbr=on}} and a gross weight of {{convert|450|kg|lb|0|abbr=on}}, giving a useful load of {{convert|250|kg|lb|0|abbr=on}}. With full fuel of {{convert|75|l}} the payload is {{convert|196|kg|lb|0|abbr=on}}.<ref name="WDLA11" />

A number of different wings can be fitted to the basic carriage as well as the Diva wing, including the [[DTA Dynamic]], and the [[strut-braced]] [[DTA Magic]].<ref name="WDLA11" /><ref>{{cite web|url=http://www.dta.fr/FR/Tricycles/Feeling/Feeling.shtml |title=DTA - Tricycles - Feeling |publisher=Dta.fr |date= |accessdate=2013-06-05}}</ref>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Feeling 582 Diva) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=12.0
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=200
|empty weight lb=
|empty weight note=
|gross weight kg=450
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|75|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type=twin cylinder, liquid-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=48
|eng1 hp=

|prop blade number=3
|prop name=composite
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=140
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=100
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=60
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=4
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=37.5
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{commons category|DTA sarl}}
*{{Official website|http://www.dta.fr/FR/Tricycles/Feeling/Feeling.shtml}}
{{DTA aircraft}}

[[Category:French sport aircraft 2000–2009]]
[[Category:French ultralight aircraft 2000–2009]]
[[Category:Single-engined pusher aircraft]]
[[Category:Ultralight trikes]]