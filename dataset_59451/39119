{{chembox
| Verifiedfields = changed
| Watchedfields = changed
| verifiedrevid = 444641443
| Name = Caesium fluoride
| ImageFile = Caesium fluoride.jpg
| ImageName = Caesium fluoride
| ImageFile1 = Caesium-fluoride-3D-ionic.png
| ImageName1 = Caesium fluoride
| IUPACName = Caesium fluoride
| OtherNames = Cesium fluoride
|Section1={{Chembox Identifiers
| ChemSpiderID_Ref = {{chemspidercite|correct|chemspider}}
| ChemSpiderID = 24179
| InChI = 1/Cs.FH/h;1H/q+1;/p-1
| SMILES = [F-].[Cs+]
| InChIKey = XJHCXCQVJFPJIK-REWHXWOFAY
| StdInChI_Ref = {{stdinchicite|correct|chemspider}}
| StdInChI = 1S/Cs.FH/h;1H/q+1;/p-1
| StdInChIKey_Ref = {{stdinchicite|correct|chemspider}}
| StdInChIKey = XJHCXCQVJFPJIK-UHFFFAOYSA-M
| CASNo = 13400-13-0
| CASNo_Ref = {{cascite|correct|CAS}}
| RTECS = FK9650000
| PubChem = 25953
}}
|Section2={{Chembox Properties
| Formula = CsF
| MolarMass = 151.903 g/mol<ref name=crc/>
| Appearance = white crystalline solid
| Density = 4.64 g/cm<sup>3</sup><ref name=crc/>
| Solubility = 5730 g/L (25 °C)<ref name=crc>{{RubberBible92nd|page=4.57}}</ref>
| MeltingPtC = 703
| MeltingPt_notes =<ref name=crc/>
| BoilingPtC = 1251
| BoilingPt_notes =
| pKb = 
| RefractIndex = 1.477
| MagSus = -44.5·10<sup>−6</sup> cm<sup>3</sup>/mol<ref>{{RubberBible92nd|page=4.132}}</ref>
}}
|Section3={{Chembox Structure
| Coordination = [[Octahedron|Octahedral]]
| CrystalStruct = [[Cubic crystal system|cubic]],  [[Pearson symbol|cF8]]
| SpaceGroup = Fm{{overline|3}}m, No. 225<ref name=str>{{cite journal|doi=10.1103/PhysRev.21.143|title=Precision Measurements of Crystals of the Alkali Halides|journal=Physical Review|volume=21|issue=2|pages=143|year=1923|last1=Davey|first1=Wheeler P.}}</ref>
| LattConst_a =  0.6008 nm<ref name=str/>
| UnitCellFormulas = 4
| UnitCellVolume = 0.2169 nm<sup>3</sup><ref name=str/>
| Dipole = 7.9 [[Debye|D]]
}}
|Section4={{Chembox Thermochemistry
| DeltaHf = -555 kJ/mol
}}
|Section7={{Chembox Hazards
| ExternalSDS = [https://fscimage.fishersci.com/msds/13612.htm External MSDS]
| RPhrases = 
| SPhrases = 
| FlashPt = Non-flammable
}}
|Section8={{Chembox Related
| OtherAnions = [[Caesium chloride]]<br/>[[Caesium bromide]]<br/>[[Caesium iodide]]<br/>[[Caesium astatide]]
| OtherCations = [[Lithium fluoride]]<br/>[[Sodium fluoride]]<br/>[[Potassium fluoride]]<br/>[[Rubidium fluoride]]<br/>[[Francium fluoride]]
}}
}}

'''Caesium fluoride''' or '''cesium fluoride''' is an [[inorganic compound]] usually encountered as a [[hygroscopic]] white solid. It is used in [[organic synthesis]] as a source of the fluoride anion. [[Caesium]] has the highest [[electropositivity]] of all non-radioactive elements and [[fluorine]] has the highest electronegativity of all elements.

==Synthesis and properties==
[[File:CsF@DWNT.png|thumb|left|150px|Crystalline CsF chains grown inside double-wall [[carbon nanotube]]s.<ref name=chains>{{cite journal|doi=10.1038/ncomms8943|pmid=26228378|pmc=4532884|title=Single-atom electron energy loss spectroscopy of light elements|journal=Nature Communications|volume=6|pages=7943|year=2015|last1=Senga|first1=Ryosuke|last2=Suenaga|first2=Kazu}} (Supplementary information)</ref>]]
Caesium fluoride can be prepared by the reaction of [[caesium hydroxide]] (CsOH) with [[hydrofluoric acid]] (HF). The resulting salt can then be purified by recrystallization. The reaction is shown below:

:CsOH(aq) + HF(aq) → CsF(aq) + H<sub>2</sub>O(l)

Another way to make caesium fluoride is to react [[caesium carbonate]] (Cs<sub>2</sub>CO<sub>3</sub>) with hydrofluoric acid. The resulting salt can then be purified by recrystallization. The reaction is shown below:

:Cs<sub>2</sub>CO<sub>3</sub>(aq) + 2 HF(aq) → 2 CsF(aq) + H<sub>2</sub>O(l) + CO<sub>2</sub>(g)

In addition, elemental fluorine and caesium can be used to form caesium fluoride as well, but doing so is very impractical because of the expense.<ref>[https://www.youtube.com/watch?v=TLOFaWdPxB0 Reacting Fluorine with Caesium]. Youtube</ref> While this is not a normal route of preparation, caesium metal reacts vigorously with all the [[halogen]]s to form [[Alkali metal halide|caesium halides]]. Thus, it burns with fluorine gas, F<sub>2</sub>, to form caesium fluoride, CsF according to the following reaction:

:2 Cs(s) + F<sub>2</sub>(g) → 2 CsF(s)

CsF is more soluble than [[sodium fluoride]] or [[potassium fluoride]]. It is available in anhydrous form, and if water has been absorbed it is easy to dry by heating at 100&nbsp;°C for two hours ''[[in vacuo]]''.<ref name="Friestad">{{cite book|author1=Friestad, G. K. |author2=Branchaud, B. P. |title=Handbook of Reagents for Organic Synthesis: Acidic and Basic Reagents|editor1=Reich, H. J. |editor2=Rigby, J. H. |publisher=Wiley|location=New York|year=1999|pages=99–103|isbn=978-0-471-97925-8}}</ref> CsF reaches a [[vapor pressure]] of 1 [[kilopascal]] at 825&nbsp;°C, 10 kPa at 999&nbsp;°C, and 100 kPa at 1249&nbsp;°C.<ref>{{cite book | editor = Lide, D. R. | title = CRC Handbook of Chemistry and Physics | edition = 86th | location = Boca Raton (FL) | publisher = CRC Press | year = 2005 | isbn = 0-8493-0486-5 |page=6.63|chapter=Vapor Pressure|url=http://www.physics.nyu.edu/kentlab/How_to/ChemicalInfo/VaporPressure/morepressure.pdf}}</ref>

CsF chains with a thickness as small as one or two atoms can be grown inside [[carbon nanotube]]s.<ref name=chains/>

==Structure==
Caesium fluoride has the halite structure, which means that the Cs<sup>+</sup> and F<sup>&minus;</sup> pack in a [[cubic closest packed]] array as do Na<sup>+</sup> and Cl<sup>&minus;</sup> in sodium chloride.<ref name=str/>

==Applications in organic synthesis==
Being highly dissociated it is a more reactive source of fluoride than related salts.  CsF is less [[hygroscopic]] alternative to [[tetra-n-butylammonium fluoride]] (TBAF) and [[TASF reagent|TAS-fluoride]] (TASF) when anhydrous "naked" [[fluoride]] [[ion]] is needed.

===As a base===
As with other soluble fluorides, CsF is moderately basic, because [[hydrofluoric acid|HF]] is a [[weak acid]]. The low [[nucleophile|nucleophilicity]] of [[fluoride]] means it can be a useful base in [[organic chemistry]].<ref name="greenwood">{{Greenwood&Earnshaw1st|pages=82–83}}</ref> CsF gives higher yields in [[Knoevenagel condensation]] reactions than [[potassium fluoride|KF]] or [[sodium fluoride|NaF]].<ref name="Fiorenza">{{cite journal|year=1985|journal=Tetrahedron Letters|volume=26|pages=787–788|doi=10.1016/S0040-4039(00)89137-6|title=Fluoride ion induced reactions of organosilanes: the preparation of mono and dicarbonyl compounds from β-ketosilanes|author1=Fiorenza, M|author2=Mordini, A|author3=Papaleo, S|author4=Pastorelli, S|author5=Ricci, A|issue=6}}</ref>

===Formation of Cs-F bonds===
Caesium fluoride is also a popular source of fluoride in [[organofluorine chemistry]]. For example, CsF reacts with [[hexafluoroacetone]] to form a caesium perfluoroalkoxide salt, which is stable up to 60&nbsp;°C, unlike the corresponding [[sodium]] or [[potassium]] salt. It will convert [[electron-deficient]] [[aryl chloride]]s to [[aromatic|aryl]] fluorides ([[halex reaction]]).<ref>{{cite journal|author1=Evans, F. W. |author2=Litt, M. H. |author3=Weidler-Kubanek, A. M. |author4=Avonda, F. P. |title=Formation of adducts between fluorinated ketones and metal fluorides|year=1968|journal=Journal of Organic Chemistry|volume=33|pages=1837–1839|doi=10.1021/jo01269a028|issue=5}}</ref>

===Deprotection agent===
Due to the strength of the [[silicon|Si]]–[[fluorine|F]] bond, fluoride ion is useful for [[desilylation]] reactions (removal of Si groups) in [[organic chemistry]]; caesium fluoride is an excellent source of anhydrous fluoride for such reactions.  Removal of silicon groups ([[desilylation]]) is a major application for CsF in the laboratory, as its [[anhydrous]] nature allows clean formation of [[water (molecule)|water]]-sensitive intermediates. Solutions of caesium fluoride in [[THF]] or [[Dimethylformamide|DMF]] attack a wide variety of [[organosilicon]] compounds to produce an organosilicon fluoride and a [[carbanion]], which can then react with [[electrophile]]s, for example:<ref name="Fiorenza"/>

:[[Image:CsF desilylation.png|500px]]

Desilylation is also useful for the removal of [[silyl]] [[protecting group]]s.<ref>{{OrgSynth |author=Smith, Adam P. |author2=Lamba, Jaydeep J. S. |author3=Fraser, Cassandra L. |last-author-amp=yes |title=Efficient Synthesis of Halomethyl-2,2'-bipyridines: 4,4'-Bis(chloromethyl)-2,2'-bipyridine|volume=78|page=82 |year=2002|collvol= 10|collvolpages=107 |prep=v78p0082}}</ref>

===Other uses===
Single crystals of the salt are transparent into the deep [[infrared]]. For this reason it is sometimes used as the windows of cells used for [[infrared spectroscopy]].

==Precautions==
Like other soluble fluorides, CsF is moderately toxic.<ref name="msds-csf">[http://www.hazard.com/msds/f2/bms/bmsqc.html MSDS Listing for cesium fluoride]. ''[http://www.hazard.com/ www.hazard.com].'' MSDS Date: April 27, 1993. Retrieved on September 7, 2007.</ref> Contact with [[acid]] should be avoided, as this forms highly toxic/corrosive [[hydrofluoric acid]]. The caesium [[ion]] (Cs<sup>+</sup>) and [[caesium chloride]] are generally not considered toxic.<ref name="msds-cscl">"[http://hazard.com/msds/mf/baker/baker/files/c1903.htm MSDS Listing for cesium chloride]." ''[http://www.jtbaker.com/ www.jtbaker.com].'' MSDS Date: January 16, 2006. Retrieved on September 7, 2007.</ref>

==References==
{{reflist}}

==External links==
{{Commons category|Caesium fluoride}}
*[https://web.archive.org/web/20060116134617/http://www.npi.gov.au:80/database/substance-info/profiles/44.html National Pollutant Inventory-Fluoride and compounds fact sheet]

{{Authority control}}
{{good article}}
{{Caesium compounds}}
{{fluorine compounds}}

[[Category:Fluorides]]
[[Category:Caesium compounds]]
[[Category:Metal halides]]
[[Category:Alkali metal fluorides]]