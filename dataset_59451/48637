'''Johanna Hurwitz''' (born October 9, 1937)<ref name="gpl">{{cite web|url=http://www.yourlibrary.ws/Childrens_Webpage/j-author102000.htm|title=Juvenile Books Author of the Month|publisher=[[Greenville, Rhode Island]] Public Library|accessdate=2009-02-12}}</ref> is an American award-winning<ref name="nahas">{{cite news|url=https://query.nytimes.com/gst/fullpage.html?res=9E0DEFD81331F934A25752C0A96F958260|title=Books and Authors With Roots on the Island; Lending a Guiding Hand By Her Children's Books|last=Nahas|first=Donna Kutt|date=1999-01-17|publisher=''[[New York Times]]''|accessdate=2009-02-12}}</ref> author of more than sixty children's books.<ref name="kidsread">{{cite web|url=http://www.kidsreads.com/authors/au-hurwitz-johanna.asp|title=Johanna Hurwitz|publisher=kidsread.com|accessdate=2009-02-12}}</ref><ref name="scholastic-interview">{{cite web|url=http://www2.scholastic.com/browse/collateral.jsp?id=10576_type=Contributor_typeId=1839|title=Johanna Hurwitz Interview Transcript|publisher=scholastic.com|accessdate=2009-02-12}}</ref> She has sold millions of books in many different languages.<ref name=nahas/>

Hurwitz graduated from [[Queens College, City University of New York|Queens College, New York]] with a degree in English and [[Columbia University]] with a master's in library science.<ref name=nahas/><ref name="weiss">{{cite book|last=Weiss|first=Jacqueline Schachter|title=Profiles in Children's Literature|publisher=Scarecrow Press|year=2001|pages=159–64|isbn=0-8108-3787-0}}</ref>

After many years working as a librarian,<ref name="ravenstone">{{cite web|url=http://www.ravenstonepress.com/hurwitz.html|title=Johanna Hurwitz|date=2003-10-22|publisher=Ravenstone Press|accessdate=2009-02-12}}</ref> Hurwitz wrote her first book, ''[[Busybody Nora]]'', in 1976,<ref name=nahas/> one of the first in the [[chapter book]]s genre for transitioning young readers from shorter stories to novels. "Busybody Nora" had taken 17 times for publishing companies to publish the book. Fortunately, Ravenstone Press was pleased to publish the book after three months Johanna Hurwitz had submitted the story.<ref name=ravenstone/><ref name="loer">{{cite news|title=Chapter Books Lead Young Readers from Pictures to Novels|last=Loer|first=Stephanie|date=2001-04-29|publisher=''[[Boston Globe]]''|accessdate=2009-02-12}}</ref>  Her books include biographies for children of [[Anne Frank]], [[Astrid Lindgren]], [[Leonard Bernstein]], and [[Helen Keller]].<ref name="scholastic">{{cite web|url=http://www2.scholastic.com/browse/contributor.jsp?id=1839|title=Johanna Hurwitz|publisher=scholastic.com|accessdate=2009-02-12}}</ref>  Her 1999 book, ''The Just Desserts Club'', combined related short stories with recipes.<ref name="nyt1114">{{cite news|url=https://query.nytimes.com/gst/fullpage.html?res=9504E6D7113AF937A25752C1A96F958260&sec=health&spon=&pagewanted=all|title=Especially for Young Readers|last=Creedon-Sandell|first=Catherine|date=1999-11-14|publisher=''[[New York Times]]''|accessdate=2009-02-12}}</ref>

Hurwitz is the aunt of [[Garance Franke-Ruta]] and [[Ted Frank]].<ref>''Much Ado About Aldo'' (1978).</ref>

==Selected works==
===Fiction===
*''[[Busybody Nora]]'', illustrated by Susan Jeschke, ([[William Morrow and Company|William Morrow]], 1976)
*''[[New Shoes for Silvia]]'', illustrated by [[Jerry Pinkney]], (William Morrow, 1993)

==References==
{{reflist|25em
}}

==External links==
* {{official }}
* {{LCAuth|n79061027|Johanna Hurwitz|103|}}

{{Portal |Children's literature}}

{{Authority control}}

{{DEFAULTSORT:Hurwitz, Johanna}}
[[Category:1937 births]]
[[Category:Living people]]
[[Category:American children's writers]]
[[Category:Columbia University School of Library Service alumni]]
[[Category:Queens College, City University of New York alumni]]
[[Category:Place of birth missing (living people)]]