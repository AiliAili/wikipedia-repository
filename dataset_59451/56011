{{Infobox journal
| title         = British Medical Bulletin
| cover         = 
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = Br. Med. Bull.
| discipline    = [[Medicine]]
| peer-reviewed = 
| language      = English
| editor        = Norman Vetter
| publisher     = [[Oxford University Press]]
| country       = 
| history       = 1943–present
| frequency     = Quarterly
| openaccess    = 
| license       = 
| impact        = 3.658
| impact-year   = 2014
| ISSNlabel     =
| ISSN          = 0007-1420
| eISSN         = 1471-8391
| CODEN         = BMBUAQ
| JSTOR         = 
| LCCN          = 
| OCLC          = 855519
| website       = http://bmb.oxfordjournals.org/
| link1         = http://bmb.oxfordjournals.org/content/current
| link1-name    = Online access
| link2         = http://bmb.oxfordjournals.org/archive
| link2-name    = Online archive
| boxwidth      = 
}}
'''''British Medical Bulletin''''' is a quarterly [[peer-review]]ed [[general medical journal]] that publishes [[review article]]s on a wide variety of medical subjects.<ref>{{cite web | url=http://www.oxfordjournals.org/our_journals/brimed/about.html | title=About the Journal | publisher=Oxford University Press | accessdate=6 September 2015}}</ref>

The journal was established in 1943 and is published by [[Oxford University Press]]. Since 2014 the journal has been online-only.<ref>{{cite web | url=http://www.ncbi.nlm.nih.gov/nlmcatalog/376542 | title=British Medical Bulletin | publisher=NLM Catalog | accessdate=6 September 2015}}</ref> The [[editor-in-chief]] is Norman Vetter ([[Cardiff University]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.658.<ref name=WoS>{{cite book |year=2015 |chapter=British Medical Bulletin |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://bmb.oxfordjournals.org/}}

[[Category:Publications established in 1943]]
[[Category:General medical journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Quarterly journals]]
[[Category:Review journals]]
[[Category:English-language journals]]
[[Category:Online-only journals]]