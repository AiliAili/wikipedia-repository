'''Return on capital''' ('''ROC'''), or '''return on invested capital''' ('''ROIC'''), is a ratio used in [[finance]], [[Valuation (finance)|valuation]] and [[accounting]], as a measure of the profitability and value-creating potential of companies after taking into account the amount of [[initial capital invested]].<ref name="Nuno Fernandes">Fernandes, Nuno. Finance for Executives: A Practical Guide for Managers. NPV Publishing, 2014, p. 36.</ref> The ratio is calculated by dividing the after-tax [[operating income]] ([[NOPAT]]) by the [[book value]] of both debt and [[equity capital]] less cash/equivalents. 

==Return on invested capital formula==

*<math>ROIC = \frac{\textrm{Net Operating Profit} - \textrm{Adjusted Taxes}}{\textrm{Invested Capital}}</math>

There are four main components of this measurement that are worth noting.<ref>{{cite web|last1=Damodaran|first1=Aswath|title=Return on Capital (ROC), Return on Invested Capital (ROIC), and Return on Equity (ROE): Measurement and Implications|url=http://people.stern.nyu.edu/adamodar/pdfiles/papers/returnmeasures.pdf|website=New York University Stern School of Business|accessdate=2015-10-20}}</ref> While ratios such as [[return on equity]] and [[return on assets]] use net income as the numerator, ROIC uses operating income. Second, this operating income is adjusted to reflect an effective or marginal tax rate. Third, while many financial computations use market value instead of book value (for instance, calculating [[debt-to-equity ratio]]s or calculating the weights for the [[weighted average cost of capital]] (WACC)), ROIC uses book values of capital as the denominator. This procedure is done because, unlike market values which reflect future expectations in efficient markets, book values more closely reflect the amount of initial capital invested to generate a return. Lastly, because ROIC attempts to measure how well a firm is able to generate an operating return per unit of invested capital, the ratio is often calculated using the invested capital during a given year, rather than the average of invested capital; however, some analysts still prefer to use the latter.

Some practitioners make an additional adjustment to the formula to add depreciation, amortization, and depletion charges back to the numerator. Since these charges are considered "non-cash expenses" which are often included as part of operating expenses, the practice of adding these back is said to more closely reflect the cash return of a firm over a given period of time. However, others may argue that these non-cash charges should remain left out of the formula as they reflect the decline in the useful life of certain assets in the denominator.

==Invested capital formula==

*<math>Invested Capital = \textrm{Fixed Assets} + \textrm{Intangible Assets} + \textrm{Current Assets} - \textrm{Current Liabilities} - \textrm{Cash}</math>

The invested capital calculation measures the amount of initial capital invested by an enterprise used to generate a return for its capital providers (debt and equity investors). An equivalent way of calculating this amount is by subtracting current liabilities, non-operating assets, and cash and equivalents from a firm's total assets.

==Relationship between ROIC and WACC==
Because financial theory states that the value of an investment is determined by both the amount of and risk of its expected cash flows to an investor, it is worth noting ROIC and its relationship to the [[weighted average cost of capital]] (WACC). 

The cost of capital is the return expected from investors for bearing the risk that the projected cash flows of an investment deviate from expectations. It is said that for investments in which future cash flows are incrementally less certain, rational investors require incrementally higher rates of return as compensation for bearing higher degrees of risk. In corporate finance, WACC is a common measurement of the minimum expected weighted average return of all investors in a company given the riskiness of its future cash flows.

Since return on invested capital is said to measure the ability of a firm to generate a return on its capital, and since WACC is said to measure the minimum expected return demanded by the firm's capital providers, the difference between ROIC and WACC is sometimes referred to as a firm's "excess return", or "[[economic profit]]".

==See also==
*[[Cash flow return on investment]] (CFROI)
*[[Profit (accounting)|Profitability]]
*[[Rate of profit]]
*[[Rate of return on a portfolio]]
*[[Profit maximization]]
*[[Tendency of the rate of profit to fall]]

==References==
{{Reflist}}

{{Financial ratios}}

[[Category:Financial ratios]]