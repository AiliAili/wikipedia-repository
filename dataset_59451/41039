{{Infobox rail line
|name=Tram route 5
|image=Tram5Antwerpen.svg
|caption=Itinerary of tram route 5
|system=[[Antwerp Tram|Antwerp tram network]]
|operator= [[De Lijn]]
|open=March 4, 2006
|type=tram
|start=[[Linkeroever]] P+R
|end=[[Wijnegem]] Fortveld
|mapcolor=white text on dark red background
|stations=29
|locale=[[Antwerp]]
|stock=[[HermeLijn]]
|map={{BS-map|inline=yes
|map=
{{BS|	uKBHFa	||	[[Wijnegem]] Fortveld	| |}}
{{BS|	uBHF||	Schijnbeemden	||}}
{{BS|	uBHF||	[[Wijnegem Shopping Center]]	||}}
{{BS|	uBHF||	Ertbrugge      ||}}
{{BS|	uBHF||	Havik	||}}
{{BS|	uBHF||	Ruggeveld	||}}
{{BS|	uBHF||	Frans Van Dijck	||}}
{{BS|	uBHF||	Van Overmeire	||}}
{{BS|	uBHF||	Ruggeveldlaan	||}}
{{BS|	uBHF||	Hermans	||}}
{{BS|	uBHF||	Antwerp Stadion	||}}
{{BS|	uBHF||	Clara Snellings	||}}
{{BS|	uBHF||	Craeybeckx	||}}
{{BS|	uBHF||	Confortalei	||}}
{{BS|	uBHF||	Lakborslei	||}}
{{BS|	uBHF||	Ten Eekhove	||}}
{{BS|	uBHF||	[[Sportpaleis]]	||}}
{{BS|	utBHFCCa ||	[[Schijnpoort (Antwerp premetro station)|Schijnpoort]]||}}
{{BS|	utBHF||	[[Handel (Antwerp premetro station)|Handel]]	||}}
{{BS|	utBHF||	[[Elisabeth (Antwerp premetro station)|Elisabeth]]	||}}
{{BS|	utBHF||	[[Astrid (Antwerp premetro station)|Astrid]]|, [[Antwerp Central Station]]|}}
{{BS|	utBHF||	[[Opera (Antwerp premetro station)|Opera]]	||}}
{{BS|	utBHF||	[[Meir (Antwerp premetro station)|Meir]]||}}
{{BS|	utBHF||	[[Groenplaats (Antwerp premetro station)|Groenplaats]]	||}}
{{BS|	utBHFCCe||	[[Van Eeden (Antwerp premetro station)|Van Eeden]]	||}}
{{BS|	uBHF||	Halewijn	||}}
{{BS|	uBHF||	A. Van Cauwelaert	||}}
{{BS|	uKBHFe||	P+R [[Linkeroever]]	||}}
}}

}}

'''Tram route 5''' is a tram route in [[Antwerp]] running from [[Linkeroever]] to [[Wijnegem]] that is operated by the Flemish transport company [[De Lijn]]. The modern route was put into service on March 4, 2006, initially connecting [[Linkeroever]] with [[Deurne, Belgium|Deurne]]. The tram route uses the [[Antwerp Pre-metro|Antwerp pre-metro]] between the stations [[Van Eeden (Antwerp premetro station)|Van Eeden]]  and [[Schijnpoort (Antwerp premetro station)|Schijnpoort]].<ref>https://www.delijn.be/nl/lijnen/lijn//1/5/7/TRAM Retrieved 11/06/2016</ref> On April 14, it was extended from the Wim Saerensplein in Deurne to the Wijnegem Fortveld terminus.

It is the second Antwerp tram route to use the number 5. The route's number is written in white on a dark red background.

== History ==

=== Historical route (1903-1956) ===

The first tram route 5 in Antwerp was put into operation on November 12, 1903 on the trajectory between the Kammenstraat near the [[Groenplaats]] in the city centre and the "Dikke Mee" near the [[Nachtegalenpark]] in [[Wilrijk]]. Trams having their terminus at the "Wilrijksepoort" gate near the city walls used a red rollsign, while those who continued the trajectory to Wilrijk used a red and white rollsign. In July 1904, the short piece between the terminal and the Groenplaats was constructed. In 1912, the route was elongated to the Bist, also in Wilrijk. The extension was originally built as a single track, but a second track was constructed a year later.

In 1926, the Antwerp city council decided to avoid using the turning loop at the Groenplaats in the future, replacing it with a turning loop through the neighboring streets. However, there was no consensus on the trajectory that this new turning loop should have followed, and it took until 1931 before the problem was resolved, allowing the tram route to drive by the Groenplaats without using the turning loop there.

In 1936, the route was bundled with route 23 to form a single route from Wilrijk to the Antwerp Stadion in Deurne. In Wilrijk, the line was shortened a bit, and had its new terminus at the August Van Daelplaats (now Frans Nagelsplein). During the occupation, in the [[Second World War]], the part between the Groenplaats and Deurne was transferred to route 12.  In April 1944, the German command ordered to suspend service on the route between the Berkenlaan and its terminus. The route was temporarily shortened to the Jan De Voslei using the Jan van Rijswijcklaan. After the liberation of Antwerp in November 1944, the route is restored to its previous itinerary.

New one-way trams were introduced in 1950 on the route. However, service on the route was suspended on December 31, 1956, and all tracks were removed between 1957 and 1963. The route was replaced on January 1, 1957 by a new bus route 5. During the 1960s, some small changes were made to the trajectory in Wilrijk. In 1974, due to the introduction of a car free zone on the Groenplaats after it had been renewed, the terminus of route 5 was moved from the southern to the northern part of the square. Eventually, the route remained in service until 31 January 1975, when it was, due to a reorganization of the bus network, added to route 25.<ref>http://allemaalbusjes.skynetblogs.be/archive/2008/07/13/lijn-5-van-vroeger-tot-nu.html Retrieved 11/06/2016</ref>

=== Present day route (2006- present) ===

The creation of route 5 was a part of the anti-congestion plan proposed by the "Bond voor Trein-, Tram- en Busgebruikers" (translation: "Alliance for Train, Tram and Bus Users").<ref>http://www.treintrambus.be/actueel/persberichten/114-bttb-dient-aangepast-anti-filetramplan-in-voor-tweede-fase-werken-ring-r1-te-antwerpen-05012005-.html Retrieved 11/06/2016</ref> In accordance with the plan, on March 4, 2006, [[Tram route 12 (Antwerp)|route 12]] (originally [[Schoonselhof cemetery|Schoonselhof]]-Wim Saerensplein)  was cut down to a short city route (Bolivarplaats-Sportpaleis). The freed trajectories were taken over by [[Tram route 24 (Antwerp)|route 24]], which was extended from Bolivarplaats to Schoonselhof, and the newly created route 5 (Linkeroever-Sportpaleis-Wim Saerensplein). The creation of the new tram route allowed the citizens of Deurne to benefit from a faster connection with the city centre, using the pre-metro network between Linkeroever and Schijnpoort. The creation of route 5 also gave some relieve to the heavily used tram route 3, which was until then the only route to run through the northern pre-metro axis.<ref>http://www.mobielvlaanderen.be/persberichten/050929-notatram5.doc Retrieved 11/06/2016</ref>

On April 14, 2012 the route was extended from the Wim Saerensplein in Deurne to the Fortveldstraat in Wijnegem, after construction works had started in 2009. The route shares the part between the Ruggeveldlaan-Turnhoutsebaan and the Fortveldstraat with [[Tram route 10 (Antwerp)|tram route 10]], which was also extended at the same time, and also passes the [[Wijnegem Shopping Center]].<ref>http://www.nieuwsblad.be/cnt/df3milsk Retrieved 11/06/2016</ref>

The masterplan for mobility proposed by the [[Flemish Government|Flemish government]] includes an extension of the route to [[Schilde]]. However, problems have arisen as the new bridge over the [[Albert Canal|Albert canal]] would not be suitable for tram tracks.<ref>http://www.standaard.be/cnt/4g39v2n2 Retrieved 11/06/2016</ref>

== Material ==
[[File:Tram 5 near terminus (410548243).jpg|thumb|A HermeLijn tram on route 5 near the old terminus at Wim Saerensplein]]
Tram route 5 almost always uses the newer [[HermeLijn]] trams built by [[Siemens]] between 1999 and 2012. The older [[PCC streetcar|PCC-cars]] can only exceptionally be seen on the route.

{{Antwerp Premetro navbox}}

== References ==
{{Reflist}}

== External links ==
* [https://www.delijn.be/en/?vertaling=true www.delijn.be], the operator of all public city transport in Antwerp and Flanders.

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Tram routes in Antwerp]]