<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=T.III
 | image=Fokker T.III.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Torpedo bomber]] and [[bomber]]
 | national origin=[[Netherlands]]
 | manufacturer=[[Fokker|Fokker-Flugzeugwerke]]
 | designer=
 | first flight=Summer 1923
 | introduced=
 | retired=
 | status=
 | primary user=[[Portuguese Naval Aviation|Aviação Naval Portuguesa]]
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=5<ref name=DNV/>
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Fokker T.III or T.3''' was a single engine [[floatplane]] designed in the [[Netherlands]] in the early 1920s as a [[bomber]] or [[torpedo bomber]].

==Design and development==
Fokker's second [[torpedo bomber]] was an enlarged version on the T.II from two years before. With a slightly greater span and 30% heavier, it had a crew of three rather than two and a more powerful engine.  Its [[floatplane|floats]] could be exchanged for a wheeled [[Landing gear|undercarriage]].  It first flew in the summer of 1923.<ref name=DNV/>

The T.III was a [[cantilever]] [[monoplane#Types|low wing monoplane]] with thick section, straight tapered, square tipped wings. Angular [[Aileron#Horns and aerodynamic counterbalances|overhung ailerons]] were used.  Its [[fuselage]] was flat topped and sided and deep from tail to nose. The three crew sat in [[tandem]], separate, open oval [[cockpit]]s, with the forward two close together over the wing and fitted with dual control.<ref name=HH/> The third cockpit was spaced further away behind the [[trailing edge]] and fitted with two machine guns, one on a conventional mounting firing upwards and the other firing downwards through the fuselage underside.  The rear cockpit also held the two release levers for 50&nbsp;kg (110&nbsp;lb) and for 100&nbsp;kg (220&nbsp;lb) bombs.<ref name=TP/>  The braced [[tailplane]] was mounted on top of the fuselage and carried [[balanced rudder|horn-balanced]] [[elevator (aircraft)|elevator]]s. In plan the tail was straight tapered and square tipped, with a large cut-out for rudder movement.<ref name=DNV/><ref name=HH/> There were several variations of the vertical tail though in all the [[rudder]] was straight edged and balanced. Some had no [[fin]], with the rudder finishing at the keel and with a nick above the fuselage,<ref name=DNV/><ref name=HH/> another similar but with a rudder extension to below the keel<ref name=TP/> and another still with a very short fin and small [[Dorsal (anatomy)|dorsal]] [[fillet (aircraft)|fillet]] but no fin extension.<ref name=DNV/><ref name=TP/>

Several different engines were fitted.<ref name=HH/> A standard engine was a 450&nbsp;hp (300&nbsp;kW) [[Napier Lion]] water-cooled [[W-12 engine|W-12]]. An alternative was the 360&nbsp;hp (270&nbsp;kW) [[Rolls-Royce Eagle IX]] water-cooled [[V-12 engine]].<ref name=Flight/>  Both drove a four blade [[propeller (aircraft)|propeller]].  The two engines had similar metal [[aircraft fairing#Engine cowlings|cowlings]], differing externally only in the number and position of the exhaust pipes (3×4 on the Lion, 2×6 on the Eagle) and the lower output shaft of the Eagle.<ref name=DNV/><ref name=HH/>

The T.III's alternative undercarriages were both fixed, with the two legs independent of each, avoiding the cross bracing which would have interfered with the bomb or [[torpedo]] release. Each mainwheel was on a horizontal axle supported at the outer end by a longitudinal V-strut from the wing underside. There were two more pairs of Vs, one fixed below the wing and the other on the lower fuselage, angled towards each other to support the inner end of the axle. A tailskid completed the undercarriage.<ref name=HH/> The T.III's twin float undercarriage projected just forward of the nose. The floats were mounted on the fuselage by two sets of N-struts, with diagonal transverse bracing between them, on each float.<ref name=DNV/> Both wood and aluminium floats were available. Fitting floats added 243&nbsp;kg (536&nbsp;lb) to the unladened weight and reduced the maximum speed by 11&nbsp;km/h (7&nbsp;mph).<ref name=HH/>

==Operational history==

Early in 1924 the [[Portuguese Naval Aviation]] (Aviação Naval Portuguesa) ordered five T.IIIs to replace their ageing [[Tellier T.3]] [[flying boats]].  The first Fokker flew to Portugal at the end of August, though with a stop at [[Bordeaux]] after about nine hours flying to rectify fuel problems. It had a wheeled undercarriage and an Eagle engine.  Another F.III was crated out but when the remaining three left [[Schellingwoude]] one of them, with its crew of two, was lost in fog over the sea near [[Brest, France]].<ref name=DNV/><ref name=Flight/>

The Portuguese had originally seen the Fokkers as vehicles for worldwide flight but these did not proceed. The remaining T.IIIs served the Portuguese Navy as [[reconnaissance aircraft]] until 1928, when they were replaced by [[CAMS 37A]] [[amphibious aircraft|amphibians]].<ref name=DNV/>

==Variants==
;T.III: Landplane bomber.
;T.IIIW: Floatplane torpedo bomber.  Wheels and floats were interchangeable.

==Operators==
;{{flag|Portugal}}
*[[Portuguese Naval Aviation|Aviação Naval Portuguesa]]

==Specifications (Lion engine, landplane)==
{{Aircraft specs
|ref=Hegener<ref name=HH/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Three
|length m=14.08
|length note=
|span m=21.20
|span note=
|height m=3.39
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=

|empty weight lb=5140
|empty weight note=
|gross weight lb=7317
|gross weight note=
|max takeoff weight lb=9565
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Napier Lion]]
|eng1 type= water-cooled [[W engine|W-12]] 
|eng1 hp=450
|eng1 note=
|power original=
|more power=

|prop blade number=4
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=169
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|more performance=
<!--
        Armament
-->
|armament=
*Two [[machine gun]]s in rear cockpit.
*At least 3×100 kg (220 lb) and 1×50 kg (110 lb) bombs together.<ref name=TP/>
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{commons category|Fokker T.III}}
{{reflist|refs=

<ref name=TP>{{cite book |title= Fokker – Aircraft Builders to the World |last=Postma|first=Thijs|year=1980|publisher=Jane's  |location=London |isbn=0 7106 0059 3|page=50}}</ref>

<ref name=HH>{{cite book |title=Fokker, the man and his aircraft |last=Hegener |first=H |year=1961|publisher=|location=|isbn=0 8168 6370 9|pages=128–9, 205}}</ref>

<ref name=DNV>{{cite book |title= De Nederlandse vliegtuigen |last=Wesselink|first=Theo|last2= Postma|first2=Thijs|year=1982|publisher=Romem  |location=Haarlem |isbn=90 228 3792 0|page=36}}</ref>

<ref name=Flight>{{cite magazine|date=18 September 1924 |title=A fine flight ...|magazine=[[Flight International|Flight]]|volume=XVI|issue=38|page=575|url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200575.html}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Fokker aircraft}}

[[Category:Floatplanes]]
[[Category:Fokker aircraft|T.III]]
[[Category:Dutch military aircraft 1920–1929]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]