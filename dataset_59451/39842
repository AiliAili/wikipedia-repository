{{Infobox military person
|name=Hermann Frommherz
|image=
|caption=
|birth_date= 10 August 1891
|death_date= {{Death date and age|df=yes|1964|12|30|1891|8|10}}
|birth_place=[[Waldshut-Tiengen|Waldshut]], Germany
|death_place=Waldshut, Germany
|placeofburial=
|placeofburial_label=
|nickname=
|allegiance=[[German Empire]], Weimar Republic, 3rd Reich
|branch=[[Luftwaffe|''Luftstreitkräfte'']], [[Luftwaffe]]
|serviceyears=1911 - ca. 1942
|rank=[[Generalmajor]]
|unit=
|commands=
|battles=
|awards=[[Military Order of St. Henry]], [[Royal House Order of Hohenzollern]], [[Military Karl-Friedrich Merit Order]] (Baden)
|relations=
|laterwork=
}}
Generalmajor (Major General) '''Hermann Frommherz''' (10 August 1891 – 30 December 1964) [[Military Order of St. Henry]], [[Royal House Order of Hohenzollern]], Knight's Cross of the [[Military Karl-Friedrich Merit Order]], began his military career in World War I as an ace fighter pilot. He was credited with 32 victories. During World War II he was involved in the German takeover of Czechoslovakia{{Citation needed|date=July 2009}} and rose to become a [[Luftwaffe]] Generalmajor.

==Early life==
Hermann Frommherz was born in [[Waldshut-Tiengen|Waldshut]], in the [[Baden]] region of Germany near the Swiss border. He studied engineering in Stuttgart. In late 1911, he joined the [[Mecklenburg-Schwerin]] 	Jäger-Bataillon Nr. 14 in the [[Prussian Army]]. In the reserves when World War I began, he was mobilized in July 1914. He served in France with Regiment Nr. 14 and was promoted to [[Vizefeldwebel]] (non-commissioned officer). He was transferred to Infantry Regiment Nr. 250, which went into combat in Russia. Frommherz earned the Second Class [[Iron Cross]] in February 1915. By April 1915, he was with the 113th Infantry Regiment. On 1 June 1915, he transferred to aviation service.<ref name=Fokker81>Franks, VanWyngarden 2003, p. 81.</ref><ref name=lines111>Franks et al 1993, pp. 111-112.</ref>

==World War I aerial service==
Frommherz began as a two-seater pilot with the ''[[Luftstreitkräfte]]'s'' ''Kampfstaffel'' (Tactical Bomber Squadron) 20 of ''Kagohl IV'', at the [[Battle of Verdun]] and over the [[Somme River]]. He was [[commission (document)|commissioned]] as a Leutnant on 1 August 1916. Kasta 20 then moved to serve in Romania in December 1916, before being posted onward to Macedonia and [[Thessaloniki]], Greece.<ref name=lines111/>

On 3 March 1917, Frommherz was assigned to ''[[Jagdstaffel 2]]'';<ref name=lines111/> this elite squadron had been led by aviation tactical and strategic pioneer [[Oswald Boelcke]] and had been named for him after he was killed in action.{{citation needed|date=February 2013}} Flying a light blue [[Albatros D.III]] nicknamed "Blaue Maus",<ref>Franks 2002, p. 92.</ref> Frommherz scored his first victory on 11 April 1917 - a [[No. 23 Squadron RAF|No 23 Squadron RFC]] [[SPAD VII]]-  and a [[Royal Aircraft Factory BE 2]]e as his second on the 14th.<ref name=lines111/> Both times, he forced the plane to land and the English pilot or crew were taken prisoner.{{citation needed|date=February 2013}}

Frommherz was injured in a crash on 1 May 1917. By October, when he had recovered, he was seconded to instructor duty with FEA 3. In December, he received [[Lübeck]]'s [[Hanseatic Cross]].<ref name=lines111/>

Upon Frommherz's return on 1 March 1918 to ''Jasta 2'' to fly a [[Fokker Dr.I]] Triplane on 1 March 1918, he began a string of 30 victories that ran from 3 June 1918 until the war's end. He had two victories in June, six each in July and August, ten in September, four in October, and two on 4 November. Notable among his kills were the half dozen against the formidable [[Bristol F.2 Fighter]]s. In the midst of his victory string, on 29 July 1918, he succeeded [[Hermann Göring]] as commanding officer of ''[[Jagdstaffel 27]]''.<ref name=lines111/>

Leutnant Frommherz had a good reputation as a commanding officer. Ernst de Ridder, when newly assigned to the Jasta, claimed he was allowed to retrain himself from the Fokker Dr.1 to the [[Fokker D.VII]], then nurse-maided into combat with an experienced pilot to watch over him. As de Ridder stated, "He was so concerned about his boys." When de Ridder was wounded, Frommherz brought de Ridder's newly awarded [[Iron Cross]] to the hospital.<ref>{{cite news|url=http://findarticles.com/p/articles/mi_qa3897/is_200112/ai_n9017672/pg_3?tag=artBody;col1 | work=Flight Journal | title=rabbit: Adventures of a WW I fighter pilot, The | first=Eric | last=Tegler | year=2001}}</ref>

De Ridder left a description of Frommherz's [[Fokker D.VII]] insignia. It consisted of the yellow nose and tail common to his Jasta, along with red and black chevrons of a [[Staffelführer]] (squadron leader) painted on top of the upper wing.<ref name="books.google.com">Franks, VanWyngarden 2003, p. 82.</ref>

Frommherz's blooming career now garnered him the Knight's Cross with Swords of the [[House Order of Hohenzollern]] on 30 September 1918, when his victory total stood at 26. The following month, he received the Knight's Cross of the [[Kingdom of Saxony]]'s [[Karl Friedrich Military Merit Order]]. At some point, he had also been awarded the Knight's Cross of the [[Military Order of Saint Henry]]. Frommherz was also nominated for the [[Pour le Merite]], having scored the required 20 victories; however, the award was still unapproved upon the Kaiser's abdication. Despite this non-award, Frommherz was seen wearing the decoration after the war;<ref name=lines111/> he had certainly fulfilled the criteria for it.<ref>The Pour le Merite website http://www.pourlemerite.org/</ref>

==After World War I==

Postwar, Frommherz was active in the German Police Aviation Service. He also flew mail for [[Deutsche Luftreederei]], a predecessor to [[Deutsche Luft Hansa]]. In 1920, he returned to Baden as technical chief at the new airfield at Lorach.<ref name=lines111/>

Beginning in 1922, the German high command ran a secret training site at [[Lipetsk]] in the Soviet Union.<ref>Aviation of World War II website http://www.airpages.ru/cgi-bin/epg.pl?nav=ru11&page=lipetsk</ref> Frommherz became an instructor there in 1925. He was also an instructor in China. From 1931 to 1932, he taught the pilots of [[Chiang Kai-shek]]'s new air force fighter tactics.<ref name=lines111/>

Frommherz returned to Germany to join the nascent [[Luftwaffe]]. He was Commanding Officer of I Gruppe, [[Jagdgeschwader 134]] ("Horst Wessel") from September 1938 until 1 November 1938 as [[Oberstleutnant]]; when it was reconstituted as JG 142, he continued in command until the first day of 1939.<ref>Jagdgeschwader 142 "Horst Wessel" http://www.ww2.dk/air/jagd/jg142.html</ref><ref>Jagdgeschwader 134 "Horst Wessel" http://www.ww2.dk/air/jagd/jg134.html</ref><ref>Development of Luftwaffe fighter aircraft in 1937-39 years. https://translate.google.com/translate?hl=en&sl=ru&u=http://wunderwaffe.narod.ru/HistoryBook/LuftAces/Day/06.htm&sa=X&oi=translate&resnum=6&ct=result&prev=/search%3Fq%3D%2522hermann%2BFrommherz%2522%26start%3D60%26hl%3Den%26client%3Dfirefox-a%26channel%3Ds%26rls%3Dorg.mozilla:en-US:official%26sa%3DN</ref> As such, he was involved in the German invasion and conquest of Czechoslovakia, which had the code name [[Fall Grün (Czechoslovakia)|Fall Grün]] (Case Green).{{Citation needed|date=July 2009}}

As a Major General, he was Commander of [[Jagdfliegerführer Deutsche Bucht]] from 1 April until 30 September 1942, following [[Werner Junck]].{{citation needed|date=February 2013}}

==Post World War II==
Hermann Frommherz returned to civic affairs in his native town of [[Waldshut-Tiengen|Waldshut]].
He died of a heart attack on 30 December 1964.<ref name=lines111/>

==Inline citations==
{{Reflist|30em}}

==References==

===Text===

* [[Norman Franks|Franks, Norman]]; Bailey, Frank W.; Guest, Russell. ''Above the Lines: The Aces and Fighter Units of the German Air Service, Naval Air Service and Flanders Marine Corps, 1914–1918''. Grub Street, 1993. ISBN 0-948817-73-9, ISBN 978-0-948817-73-1.
* [[Norman Franks|Franks, Norman]]. ''Albatros Aces of World War 1''. Osprey Publishing, 2000. ISBN 1-85532-960-3, ISBN 978-1-85532-960-7.
* Franks, Norman; VanWyngarden, Greg. ''Fokker D VII Aces of World War 1''. Osprey Publishing, 2003. ISBN 1-84176-533-3, ISBN 978-1-84176-533-4.
*{{cite book |last1=Franks |first1=Norman |year=2004 |title=Jasta Boelcke |publisher=Grub Street |location=London |isbn=978-1904010760}}
* VanWyngarden, Greg; Dempsey, Harry. ''Jagdstaffel 2 Boelcke: Von Richthofen's Mentor''. Osprey Publishing, 2007. ISBN 1-84603-203-2, ISBN 978-1-84603-203-5.

===Online===

* The Aerodrome website page on Frommherz http://www.theaerodrome.com/aces/germany/frommherz.php Accessed 6 November 2008.
* [[Jagdfliegerführer Deutsche Bucht]]  Accessed 6 November 2008.

{{s-start}}
{{s-mil}}
{{succession box|
before=Generalleutnant [[Werner Junck]]|
after=Oberst [[Karl Hentschel]]|
title= Commander of [[Jagdfliegerführer Deutsche Bucht]]|
years=1 April 1942 – 30 September 1942
}}
{{s-end}}

{{wwi-air}}

{{DEFAULTSORT:Frommherz}}
[[Category:1891 births]]
[[Category:1964 deaths]]
[[Category:People from Waldshut-Tiengen]]
[[Category:German World War I flying aces]]
[[Category:Luftwaffe World War II generals]]
[[Category:Recipients of the Iron Cross (1914), 2nd class]]
[[Category:Knights of the Military Order of St. Henry]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Recipients of the Hanseatic Cross (Lübeck)]]
[[Category:Knights of the Military Karl-Friedrich Merit Order]]
[[Category:Luftstreitkräfte personnel]]
[[Category:People from the Grand Duchy of Baden]]
[[Category:Major generals of the Luftwaffe]]