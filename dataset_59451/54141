{{Use dmy dates|date=January 2015}}
{{Use Australian English|date=January 2015}}
{{Infobox station
| name                = Clapham
| image               = Clapham-LookingSouth-Aug08.jpg
| caption             = 
| address             = [[Clapham, South Australia|Clapham]]
| coordinates         = 
| owned               = 
| operator            = 
| line                = [[Belair railway line|Belair Line]]
| distance            = 10.0&nbsp;km from [[Adelaide railway station|Adelaide]]
| platforms           = 2
| tracks              = 
| structure           = 
| parking             = n/a
| bicycle             = n/a
| disabled            = 
| bus_routes          = n/a
| code                = 
| zone                = 
| website             = 
| opened              = 1910s
| closed              = 28 April 1995
| rebuilt             = 
| services            = 
{{s-rail|title=TransAdelaide}}
{{s-line|system=TransAdelaide|line=Belair|previous=Torrens Park|next=Lynton}}
}}

'''Clapham railway station'''  was located on the [[Belair railway line|Belair line]], in the inner southern [[Adelaide]] suburb of [[Clapham, South Australia|Clapham]], 10 kilometres from [[Adelaide railway station]].

It closed on 28 April 1995 along with [[Millswood railway station|Millswood]] and [[Hawthorn railway station, Adelaide|Hawthorn]] when the line was converted to two single lines as part of the standardisation of the [[Adelaide-Wolseley railway line|Adelaide-Wolseley line]]. It runs parallel to Egmont Terrace. It consists of two earth-filled concrete faced platforms, one each side of the dual lines, which were originally both broad gauge. Each platform had a timber and iron open passenger shelter, and there was a ticket office at the foot of the Western platform which was manned only at peak hours in the 1960s. There is a shelter (the bench is gone though) and disused public address speaker on the down platform. During the Belair line renewal in 2009, the top edge of the eastbound platform was removed because of the risk of it being struck by passing trains.

On [[Adelaide Metro]] rail network maps in most carriages, gaps on the Belair line map remain where the entries for Clapham and the other disused stations on the Belair line, have been removed.

The station was originally the terminus of the Clapham branch line, which began at [[Mitcham railway station, Adelaide|Mitcham station]], and the original station was to the west of the more recent platforms (which were on the main line); this explains the unusual layout of the intersection of the local roads west of the Springbank Road overpass (opened 1924). At that time, Belair Road, known as Bull's Creek Road, ran on the west side of the railway line from Wattlebury Road to McPherson Street. (The then main road to Belair, now called the Old Belair Road, was via Blythwood Road). The Clapham branch line, and the old station, were abandoned formally in 1917. Presumably, the newer station was built a little before that.

The Clapham passenger service, typically a [[South Australian Railways P class|P class locomotive]] and a couple of carriages, was known as the ''Clapham Dodger'': this service must have ceased when the branch line was closed.

==References==
{{Reflist}}

*Callaghan WH. ''The overland railway''. ARHS NSW, St James. 1992.
*Eardley, G. "The P class locomotives of South Australia." ''ARHS bulletin'' 416, June, 1972. 
*Jennings R. ''Line clear: 100 years of train working Adelaide-Serviceton.'' Mile End Railway Museum, Roseworthy. 1986.

{{Coord|-34.9895|138.609|format=dms|display=title|region:AU-SA_type:railwaystation}}
{{Belair railway line, Adelaide}}

[[Category:Disused railway stations in South Australia]]
[[Category:Railway stations closed in 1995]]