{{More footnotes|date=July 2014}}
<!-- Begin Infobox Dogbreed.  The text of the article should go AFTER this section. See: -->
<!-- http://en.wikipedia.org/wiki/Wikipedia:WikiProject_Dog_breeds#Infobox_Dogbreed_template -->
<!-- for full explanation of the syntax used in this template. -->
{{Infobox Dogbreed
| name = Estrela Mountain Dog
| image = Estrela Mountain Dog 6 month old male.jpg
| image_caption = Six-month-old male
| altname = Portuguese Shepherd<br>Cão da Serra da Estrela
| nickname = Estrela
| country = <br />Portugal
<!-----Traits----->
|weight       = 
|maleweight   = {{convert|45|-|60|kg|lb|abbr=on}}
|femaleweight = {{convert|35|-|45|kg|lb|0|abbr=on}}
|height       = 
|maleheight   = {{convert|63|-|75|cm|in|0|abbr=on}}
|femaleheight = {{convert|60|-|71|cm|in|0|abbr=on}}
|coat         = Comes in both a long hair and a short hair, and has the texture of goat hair.
|colour       = Fawn, wolf gray and yellow with a dark facial mask
|litter_size  = 
|life_span    = Over 10 Years
<!-----Classification and standards----->
|fcigroup = [[FCI Pinscher and Schnauzer, Molossoid and Swiss Mountain Dog Group|2]]
|fcisection = 2.2 Molossian: Mountain type
|fcinum = 173
| fcistd = http://www.fci.be/Nomenclature/Standards/173g02-en.pdf
|akcfss     = yes
| akcgroup = [[Foundation Stock Service Program|FSS]]
| akcstd = 
|akcmisc    = 
|ankcgroup  = 
|ankcstd    = 
|ckcgroup   = 
|ckcstd     = 
|ckcmisc    = 
| kcukgroup = Pastoral
| kcukstd = http://www.thekennelclub.org.uk/services/public/breed/standard.aspx?id=5123
|nzkcgroup  = Utility
|nzkcstd    = http://www.nzkc.org.nz/breed_info/br619.html
| ukcgroup = [[Guardian Dog Group|Guardian Dog]]
| ukcstd = http://www.ukcdogs.com/WebSite.nsf/Breeds/EstrelaMountainDogRevisedMay12008
<!-----Notes----->
|note = The AKC does not have its own standard and instead links to that of the FCI
}}
<!-- End Infobox Dogbreed info. Article Begins Here -->

The '''Estrela Mountain Dog''' is a large [[dog breed|breed]] of [[dog]] which has been used for centuries in the [[Serra da Estrela|Estrela Mountains]] of Portugal to [[Livestock guardian dog|guard herds]] and homesteads.

== Description ==
=== Coat ===
The Estrela [[Mountain dog]] comes in two coat types. Both types should have coat resembling the texture of goat hair.

Long coat: The thick, slightly coarse outer coat lies close over the body and may be flat or slightly waved, but never curly. Undercoat is very dense and normally lighter in color than the outer coat. The hair on the front sides of the legs and the head is short and smooth. Hair on the ears diminishes in length from the base of the ears to the tips. The hair on the neck, the buttocks, the tail, and the back side of the legs is longer resulting in a ruff at the neck, breeches on the buttocks and backs of the legs, and feathering on the tail.  The males can have a "lion's mane".

Short coat: The outer coat is short, thick, and slightly coarse, with a shorter dense undercoat. Any feathering should be in proportion.

=== Color ===

Fawn, wolf gray and yellow, with or without brindling, white markings or shadings of black throughout the coat. All colors have a dark facial mask, preferably black. Blue coloration is very undesirable.

=== Size ===
Desirable height for mature males is 25½ - 28½ inches and for mature females is 24½ - 27 inches. Mature males in good working condition weigh between 88 and 110 [[pound (mass)|pound]]s. Mature females in good working condition weigh between 66 and 88 [[pound (mass)|pound]]s.

===Temperament===

A large, athletic dog, the Estrela Mountain Dog is a formidable opponent for any predator - fortunately, it is not often called upon to rise to anyone's life-or-death defense. It is calm but fearless and will not hesitate to react to danger, making it an exceptional watchdog as well as an excellent guard dog. It is intelligent, loyal, and faithful, affectionate to those it knows but wary of those it does not. It is instinctively protective of any children in its family. It needs early and continued socialization to be trustworthy around small pets and other dogs.

It's important to begin training and socializing the Serra da Estrela dog from puppyhood to nurture its acceptance of different situations. This is a strong independent-minded breed that will need persistent training and consistent leadership. It has a tendency to bark, especially when protecting his or her territory. As with most livestock guardians, the Serra da Estrela dog is not a "pet" for everyone. Strong ownership is paramount.

== History ==
<!-- ====================================================================================================================================
ARTICLE HAS PERMISSION TO USE THE HISTORY OF THE ESTRELA MOUNTAIN DOG - DON'T TAG - also BBC article: http://www.bbc.com/news/world-europe-16845256

==================================================================================================================================== -->
The breed has been developed in the mountains of Serra da Estrela, in what is now Portugal.<ref>{{cite web|url=http://scc.asso.fr/Fiches-et-standards-de-race?destination=detail&numero_scc_chien=513&pointeur=0|title=Fiches et standards-de race|publisher=scc.asso.fr|accessdate=}}</ref><ref>[History of the Estrela Mountain Dog ]</ref> The Estrela Mountain Dog is one of the oldest breeds in Portugal.<ref>[(cf BBC article)]</ref> The earliest of the Estrela ancestors were herd-guarding dogs in the Serra da Estrela, in what is now Portugal. Since there are no written records, it is not known for sure whether the ancestors which contributed to this breed were brought by the Romans when they colonized the Iberian Peninsula, or later by the invading Visigoths. Regardless, there is no disagreement that the Estrela is one of the oldest breeds in Portugal.

Those early guardian dogs were not the distinct breed we know today. Rather, the Estrela developed over a period of hundreds of years. Shepherds would have chosen to breed the dogs that had the characteristics necessary to survive in their mountain environment and to do their job: large size, strength, endurance, agility, a deep chest, ability to tolerate a marginal diet, the set of the legs, a powerful mouth, a tuft of hair around the neck, an easy, jog-like gait, a warm coat, and a watchful, mistrustful, yet loyal temperament. Since the region was isolated, there was little breeding with non-native dogs, leading to the purity of the breed.[[File:Cao da serra da estrela 777.jpg|thumb|||Two Estrela Mountain Dogs]]Life changed little for the people and dogs of the region, even into the 20th century. The isolation of the region meant the breed was relatively unknown outside it until the early 1900s, and even then, they were mostly ignored in early dog shows. The Portuguese admired foreign breeds much more than their own. Shepherds often castrated their dogs to prevent them from leaving their flocks to mate. These factors had a negative effect on the Estrela. So from 1908 to 1919, special shows called concursos were held to promote and preserve the Estrela breed in the region. During this period there was some attempt at a registry (of which there is no surviving record). Special livestock guardian working trials were included in these shows. The trial consisted of an owner/shepherd bringing his dog into a large field with many flocks of sheep. The dog was observed by judges for its reactions coming into the field and as the shepherd was ordered to move the flock, which inevitably produced stragglers. The dog was expected to move from his spot of guarding to bring the stragglers back, and then assume a leadership position at the head of the flock.

The first, tentative, recorded breed standard was published in 1922. This standard only reflected the functional features naturally found in the best dogs of the time, although it did mention having dew claws as reflecting a “perfect” dog. The characteristic hooked tail and turned-back (rosed) ears, which later became part of the official standard, were not mentioned in this preliminary standard.

The first official breed standard was written in 1933. This standard attempted to differentiate the Estrela as a distinct breed. This led to the hooked tail and double dew claws becoming a requirement. All colors were allowed. The standard has undergone small refinements since then. For example, dew claws became optional by 1955, and the allowed colors have been limited a few times to achieve today’s current set.[[File:Cao da serra da estrela głowa 546.jpg|thumb|||Estrela Mountain Dog portrait]]Prior to World War II, the Estrela breeders were still primarily the shepherds and farmers of the region. Since they were mostly illiterate, they did not make any attempt to follow the official breed standard, if they even knew one existed. But by the early 1950s, interest in the breed returned, and the annual concursos were reinstated. Again the intent was to stimulate interest among the Serra residents and to encourage them to adhere to the official standard. During this period, the long-haired variety was most popular at shows, but “show dogs” represented (and still do) only a small portion of the Estrela population in Portugal. Many of the working dogs were (and are) short-haired.[[File:Estrela Mountain Dog Lion's Mane.JPG|thumb|Shows the mane of a male Estrela]]

[[File:estrela.png|thumb|200px||Estrela Mountain Dog resting]]Early in the 1970s, interest was steeply declining. There was some concern about the degeneration and even possible extinction of the breed. But the Portuguese revolution of 1974 helped save the Estrela. It led to changes both in dog shows in Portugal and in Portuguese dog breeds. Prior to the revolution, dog showing had largely been a pastime of the wealthy, with their preference for non-Portuguese breeds as status symbols. Now, working people could and did show the native dogs they preferred. Also, with the revolution came an increase in crime and thus more interest in guard dogs.

There is no record of the Estrela outside Portugal prior to 1972. While some undoubtedly did leave the country, they were probably interbred, with no effort to maintain the breed. In 1972 and 1973, pairs were imported to the US. Others were probably imported into the US since then, but it was not until 1998 that the first papered dog was imported into the United States. The United Kingdom was the first country to establish the breed outside Portugal in 1972. Today the Estrela can be found in many countries.

Today, the Estrela Mountain Dog remains true to its guardian heritage. It is still a working dog, guarding flocks in its native Portugal and elsewhere (the Portuguese Marines had even used them as patrol dogs). It is also an ideal family pet because of its alertness, loyalty, intelligence, and its instinct to nurture young; all features it needed in its earliest days.

==References==
<references />
'''Sources'''
* UKC Estrela Mountain Dog Breed Standard
* [http://emdaa.com/breed-history Estrela Mountain Dog Association of America, EMDAA]
* [http://StarMountainKennel.com, Star Mountain Kennel]

===Permission===
Permission was given by the breed club for the use of that history section from the [http://emdaa.com/breed-history breed club website]. It has a correctly assigned CC-BY-SA-3.0 license shown on the bottom of the breed club website as well.
BBC article: http://www.bbc.com/news/world-europe-16845256.

==External links==
<!--Editors: breed registry, club, rescue, and kennel information links are placed on DMOZ, not here-->
*[http://www.caodaserradaestrela.net/ Estrela Mountain Dog portal]
*{{dmoz|dmoz.org/Recreation/Pets/Dogs/Breeds/Livestock_Guardian_Dogs/Estrela_Mountain_Dog/}}

{{Portuguese dogs}}
{{Pastoral dogs}}

[[Category:Dog breeds]]
[[Category:Dog breeds originating in Europe]]
[[Category:Dog breeds originating in Portugal]]
[[Category:Livestock guardians]]<!-- see talk pg for further info. -->
[[Category:Molossers]]
[[Category:Mountain dogs]]
[[Category:Rare dog breeds]]
[[Category:Working dogs]]