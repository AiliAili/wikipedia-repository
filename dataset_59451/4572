{{good article}}
{{Infobox stadium
| name                = Kristins Hall
| nickname            =
| image               = [[File:Kristins hall Lillehammer.jpg|300px]]
| caption             =
| fullname            =
| location            = [[Stampesletta]], [[Lillehammer]], [[Norway]]
| coordinates         = {{coord|61.12320|10.47134|type:landmark_region:NO-05|format=DMS|display=title,inline}}
| broke_ground        =
| built               =
| opened              = December 1988
| renovated           =
| expanded            =
| closed              =
| demolished          =
| owner               = [[Lillehammer Municipality]]
| operator            =
| surface             =
| construction_cost   = {{NOK|65 million}}
| architect           =
| structural engineer =
| services engineer   =
| general_contractor  =
| project_manager     =
| main_contractors    = 
| former_names        =
| tenants             = [[Lillehammer IK]] (1988–)<br>[[1989 World Ice Hockey Championships|1989 Ice Hockey World Championships Group B]]<br>[[1994 Winter Paralympics]]<br>[[2016 Winter Youth Olympics]]
| capacity            = 3,197
| dimensions          =
| scoreboard          =
}}

'''Kristins Hall''' is an [[arena]] located at [[Stampesletta]] in [[Lillehammer]], [[Norway]]. It consist of an [[ice rink]], a combined [[team handball|handball]] and [[floorball]] court, and a [[curling rink]]. The venue, owned and operated by the [[Lillehammer Municipality]], opened in 1988 and cost 65 million [[Norwegian krone]] (NOK) to build. One of the motivations for its construction was to help Lillehammer's bid to be selected as the host of the [[1994 Winter Olympics]]. The ice rink has a capacity for 3,194 spectators and is the home rink of [[GET-ligaen]] hockey club [[Lillehammer IK]]. Kristins Hall is located next to the larger [[Håkons Hall]], which opened in 1993. During the 1994 Winter Olympics, Kristins Hall was a training rink, and subsequently hosted the [[ice sledge hockey at the 1994 Winter Paralympics|ice sledge hockey tournament]] at the [[1994 Winter Paralympics]]. The venue also co-hosted Group B of the [[1989 World Ice Hockey Championships]].

==Construction==
Plans for an ice rink in Lillehammer started in the 1980s with the [[Lillehammer bid for the 1992 Winter Olympics]]. In 1985, Lillehammer Municipal Council accepted an agreement with the [[Norwegian Confederation of Sports]] (NIF), which offered to finance 50 percent of a new multi-use arena in Lillehammer.<ref>{{cite news |title=Formannskapet i Lillehammer går inn for at det til |date=13 December 1985 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> Combined with the construction of the skiing resort [[Hafjell]], it was part of a plan to document the construction of new venues in and around Lillehammer to help the town secure the right to host the Olympics.<ref>{{cite news |title=Lillehammers OL-søknad, og den støtte Regjeringen har vedtatt |date=21 October 1985 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> The construction received NOK&nbsp;25 million in state grants.<ref>{{cite news |title=Et rimelig kompromiss |date=3 April 1987 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> To promote the Olympic bid, the venue was given priority by NIF in their recommendation for use of public grants. In May 1987, the municipal council stated that they did not want to apply to host the B-Group of the 1989 World Ice Hockey Championships, because they feared the venue would not be completed by November 1987. NIF President [[Hans B. Skaset]] stated that this could jeopardize the entire Olympic bid if Lillehammer withdrew from arranging such a small event.<ref>{{cite news |title=Lillehammer-nei til ishockey-VM |date=29 May 1987 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> The decision was changed a week later.<ref>{{cite news |title=Lillehammer ombestemmer seg |date=5 June 1987 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> The venue opened in December 1988 and cost NOK&nbsp;65 million.<ref name=yog>{{cite web |url=http://www.idrett.no/nyheter/Documents/YOG2016_Lillehammer_PHOTO.PDF |author=[[Norwegian Olympic and Paralympic Committee and Confederation of Sports]] |title=Candidate city for the Winter Youth Olympic Games: Lillehammer 2016 |archiveurl=http://www.webcitation.org/5wIOlQ6Nd |archivedate=6 February 2011 |accessdate=6 February 2011}}</ref>

After Lillehammer was awarded the 1994 Winter Olympics in 1988, it became necessary to build a larger venue to hold the Olympic ice hockey matches. The name of the arena was decided by the Lillehammer Municipal Council in October 1988, as part of a broader branding policy, based on the history of the [[Birkebeiner]]. Originally the administration had suggested the new larger hall be named Håkons Hall, after [[Haakon Haakonarson]], later king of Norway, while the smaller hall would be named Sveres Hall, for [[Sverre Sigurdsson]]. During the political debate, a number of female councilors suggested that the smaller hall be named Kristins Hall, for Sigurdsson's daughter [[Kristina Sverresdotter]], which was passed by the city council. [[Kristin and Håkon]] would be used to name the mascots for the Olympics.<ref>{{cite news |title=Håkons hall valgt blant mange forslag |date=7 January 1992 |work=[[Bergens Tidende]] |page=43 |last=Svegaarden |first=Knut Espen |last2=Olsen |first2=Geir |language=Norwegian}}</ref> Kristins Hall has had small renovations throughout the years, including an upgrade in 2007 which included new ice hockey sideboards, a new ice machine, a new lighting system, and a new handball floor.<ref name=yog />

==Facilities==
[[File:Stampesletta.jpg|thumb|left|[[Stampesletta]] with Kristins Hall located behind [[Håkons Hall]] to the right]]
The municipally owns and operates venue is located at Stampesletta, about {{convert|1|km|sp=us}} from the town center of Lillehammer, Norway. With a gross area of {{convert|9000|m2|sp=us}}, it consists of three main sections: an ice hockey rink, a combined handball and floorball court, and a curling rink. The venue has eight locker rooms, of which two are designed for judges and referees, a weight room, a {{convert|100|m|sp=us|adj=on}} long, four-track sprint track, meeting rooms, three kiosks, VIP facilities and a cafeteria. The handball hall has an artificial surface measuring {{convert|22|by|44|m|sp=us}}.<ref name=yog /><ref name=facilities>{{cite web |url=http://www.lillehammer.kommune.no/artikkel/27201/5046:26958 |title=Kristins hall |author=[[Lillehammer Municipality]] |archiveurl=http://www.webcitation.org/5xbD6ZFGG |archivedate=31 March 2011 |accessdate=31 March 2011 |language=Norwegian}}</ref>

The ice rink is certified by the [[Norwegian Ice Hockey Association]] to hold 3,197 spectators, but can accommodate up to 4,000 people in special circumstances.<ref name=yog /> The cooling and heating systems for Håkons Hall and Kristins Hall are connected, allowing them to function as energy reserves for each other.<ref>{{cite web |url=http://www.la84foundation.org/6oic/OfficialReports/1994/E_BOOK3.PDF |title=1994 Winter Olympics Report, volume III |author=Lillehammer Olympic Organizing Committee |accessdate=10 December 2010 |pages=27–29}}</ref>

==Tenants and events==
The ice rink is the home of [[Lillehammer IK]], which plays in [[GET-ligaen]], the premier ice hockey league in Norway. During the season, they play one to two home games per week, typically attracting crowds of 1,000 to 1,500 spectators.<ref name=yog /> They inaugurated the arena in December 1988 with a game against Oshaug.<ref name=bvm>{{cite news |title=Klart for dropp i Kristins Hall |date=7 December 1988 |last=Larsen| first=Gunnar Tore |work=[[Aftenposten]] |page=29 |language=Norwegian}}</ref> The ice rink is also used by the [[Norwegian College of Elite Sport]] in Lillehammer and Lillehammer Kunstløpklubb. The handball court is used by Lillehammer Innebandyklubb and Lillehammerstudentenes IL, while the curling rink is used by Lillehammer Curlingklubb. The venue is owned and operated by Lillehammer Municipality.<ref name=facilities />

The official opening of the venue took place on 12 December 1988, when [[Norway men's national ice hockey team|Norway]] played [[West Germany men's national ice hockey team|West Germany]].<ref>{{cite news |title=Motstander i rett klasse for hockeylandslaget |date=15 December 1988 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref><ref name="Landskamp ishockey lørdag">{{cite news |title=Landskamp ishockey lørdag |date=17 December 1988 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> Kristins Hall held two more Norwegian friendly internationals during the season, before it was host to the B-Group during the [[1989 World Ice Hockey Championships]].<ref name="Landskamp ishockey lørdag"/> Ten games were played in Lillehammer, including the opening game between Norway and [[Japan men's national ice hockey team|Japan]], while 18 games were played in [[Oslo]].<ref>{{cite news |title=Lillehammer og Oslo deler BVM  |date=16 October 1987 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> During the 1994 Winter Olympics, Kristins Hall was used as a training venue for the [[ice hockey at the 1994 Winter Olympics|ice hockey teams]], which played their games at Håkons Hall and [[Gjøvik Olympic Cavern Hall]].<ref>{{cite news |title=Lillehammer 1 år igjen |date=12 February 1993 |work=[[Bergens Tidende]] |page=14 |language=Norwegian}}</ref> During the [[1994 Winter Paralympics]], Kristins Hall hosted the [[ice sledge hockey at the 1994 Winter Paralympics|ice sledge hockey tournament]].<ref>{{cite news |title=Norge til kjelkehockey-finale med 6–1 over Estland |date=15 March 1994 |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref> Lillehammer is scheduled to host the [[2016 Winter Youth Olympics]],<ref>{{cite news |url=http://www.insidethegames.biz/olympics/youth-olympics/2016/15116-lillehammer-awarded-2016-winter-youth-olympic-games |title=Lillehammer awarded 2016 Winter Youth Olympic Games |work=Inside the Games |date=7 December 2011 |accessdate=7 December 2011 |archivedate=7 December 2011 |deadurl=no |archiveurl=http://www.webcitation.org/63lPt2SIC}}</ref> with Kristins Hall to be used for curling and ice hockey. This involves an expansion of the curling rink to satisfy international requirements, including expansion of the spectator capacity.<ref name=yog />

==References==
{{reflist|2}}

==External links==
{{commons category|Kristins hall}}
*{{official website|http://www.lillehammer.kommune.no/artikkel/27201/5046:26958}} {{no icon}}

{{GET-ligaen}}
{{2016 Winter Youth Olympics venues}}
{{Sport in Lillehammer}}

[[Category:Handball venues in Norway]]
[[Category:Indoor ice hockey venues in Norway]]
[[Category:Curling venues in Norway]]
[[Category:Sports venues in Lillehammer]]
[[Category:1988 establishments in Norway]]
[[Category:Sports venues completed in 1988]]
[[Category:Venues of the 2016 Winter Youth Olympics]]
[[Category:Lillehammer IK]]
