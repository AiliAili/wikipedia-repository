{{Infobox person
| name        = Christophe Cuvillier
| birth_date  = {{Birth date and age|1962|12|05}}
| birth_place = [[Etterbeek]], [[Belgium]]
| nationality = [[French people|French]]
| alma_mater = [[HEC Paris]]
| occupation  = CEO of [[Unibail-Rodamco]]
| employer = [[Unibail-Rodamco]]
}}

:''This article incorporates material from the corresponding article in the French wikipedia''

'''Christophe Cuvillier''' (born December 5, 1962 in [[Etterbeek]]) is a [[French people|French]] businessman and current chief executive officer of the European real-estate group [[Unibail-Rodamco]].

== Education ==

Cuvillier studied at the [[ESADE]] business school in Barcelona, Spain and then the [[University of California, Berkley|University of California, Berkeley]]. He completed his studies at [[HEC Paris]], where he received his diploma in international business management in 1984.<ref name=lsa>[http://www.lsa-conso.fr/carnet-des-decideurs/cuvillier-christophe,121620 "Christophe Cuvillier"], ''lse.fr'' {{fr icon}}</ref>

== Career ==

=== 1986-2000: Lancome, L'Oreal Group ===

Cullivier began his business career as a sales trainee at the luxury cosmetics brand [[Lancôme]], part of [[L’Oreal]], in 1986. At Lancôme, he rose through the ranks to managing director of the company’s United Kingdom branch in 1992, before moving to [[Sydney]] as director of L’Oreal’s Australian Luxury Products division in 1993.<ref name=lsa/>

In 1995, Cuvillier returned to Lancôme as the managing director of its operations in France, and in 1998 he became director of L’Oreal’s Luxury Products Division in France.

=== 2000-2011: Kering Group ===

In 2000, Cuvillier left L’Oreâl to join the [[Kering| Kering Group]] (formerly PPR) as chief operating officer of Marketing and Products at [[Fnac]], one of its subsidiaries.<ref name=lsa/> In 2003, he was named Fnac’s chief operating officer of International and development, and in 2005 he became chief executive officer of [[Conforama]], another Kering brand.<ref>[http://www.euroinvestor.fr/actualites/2013/01/30/guillaume-poitrinal-to-be-succeeded-by-christophe-cuvillier-as-ceo-of-unibail-rodamco/12187453 "Guillaume Poitrinal to be succeeded by Christophe Cuvillier as CEO of Unibail-Rodamco"], (Press Release) ''reuters'', January 30, 2013</ref> In 2008, he was appointed chairman and chief executive officer of Fnac,<ref>[http://www.kering.com/en/press-releases/christophe_cuvillier_will_succeed_denis_olivennes_as_chairman_and_ceo_of_fnac "Christophe Cuvillier will succeed Denis Oliviennes as Chairman and CEO of fnac"], ''kering.com'', March 24, 2008</ref> a post he served until 2011.

=== 2011-Present: Unibail-Rodamco ===

Cuvillier left the luxury sector in 2011, joining real-estate group Unibail-Rodamco as chief operating officer and a member of its Management Board.<ref>Myrtam Chavot, [http://www.lesechos.fr/entreprises-secteurs/service-distribution/actu/0202727638201-christophe-cuvillier-prend-les-renes-d-unibail-rodamco-561608.php "Christophe Cuvillier takes the reins at Unibail-Rodamco"], ''lesechos.fr'', April 25, 2013 {{fr icon}}</ref> In April 2013, he succeeded Guillaume Poitrinal as chairman and CEO of the company, a position Poitrinal had held since 2005.<ref>[http://www.property-magazine.eu/guillaume-poitrinal-to-be-succeeded-by-christophe-cuvillier-as-ceo-of-unibail-rodamco-23830.html "Guillaume Poitrinal to be succeeded by Christophe Cuvillier as CEO of Unibail-Rodamco"], ''property magazine'', January 31, 2012</ref><ref>[http://www.boursier.com/actions/actualites/news/unibail-rodamco-christophe-cuvillier-succede-officiellement-a-guillaume-poitrinal-528266.html "Unibail-Rodamco: Christophe Cuvillier officially succeeds Guillaume Poitrinal"], ''boursier.com'', April 26, 2013 {{fr icon}}</ref>

== Personal life ==

Christophe Cuvillier is married to Kateri Loeb, and together they have three children.<ref name=lsa/> He also holds the Knight insignia of the French Legion of Honor.<ref>[http://www.unibail-rodamco.com/W/do/centre/management-board "Management Board"], ''unibail-rodamco.com''</ref>

== References ==

{{reflist}}

== See also ==

=== See also ===

*[[Unibail-Rodamco]]
*[[Fnac]]

{{DEFAULTSORT:Cuvillier, Christophe}}
[[Category:1962 births]]
[[Category:Living people]]
[[Category:French business executives]]
[[Category:French chief executives]]
[[Category:HEC Paris alumni]]
[[Category:Chevaliers of the Légion d'honneur]]
[[Category:Chief operating officers]]