{{about|the Tolstoy novel|other uses|Resurrection (disambiguation)}}
{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Resurrection
| image        = File:Resurrection.jpg
| caption      = First US edition
| title_orig   = Воскресение, Voskreseniye
| translator   = 
| author       = [[Leo Tolstoy]]
| cover_artist = 
| country      = Russia
| language     = Russian 
| series       = 
| genre        = [[Philosophical novel]]
| publisher    = First published serially in ''[[Niva (journal)|Niva]]''<br>then [[Dodd, Mead and Company|Dodd, Mead]] (US)
| release_date = 1899 
| english_pub_date = 1900
| media_type   = Print (Hardcover, Paperback) and English-language Audio Book
| pages        = 483 (Oxford World's Classics edition)
| isbn         = 
| preceded_by  = 
| followed_by  = 
}}

'''''Resurrection''''' ({{lang-ru|Воскресение}}, Voskreseniye), first published in 1899, was the last [[novel]] written by [[Leo Tolstoy]]. The book is the last of his major long fiction works published in his lifetime. Tolstoy intended the novel as an exposition of the injustice of man-made laws and the [[hypocrisy]] of the institutionalized church. The novel also explores the economic philosophy of [[Georgism]], of which Tolstoy had become a very strong advocate towards the end of his life, and explains the theory in detail. It was first published serially in the popular weekly magazine ''[[Niva (journal)|Niva]]'' in an effort to raise funds for the resettlement of the [[Doukhobor]]s.

==Plot outline==
The story is about a nobleman named Dmitri Ivanovich Nekhlyudov, who seeks redemption for a sin committed years earlier. His brief affair with a maid had resulted in her being fired and ending up in prostitution.

Ten years later, Nekhlyudov sits on a jury which sentences the maid, Maislova, to prison in Siberia for murder (poisoning a client who beat her). The book narrates his attempts to help her practically, but focuses on his personal mental and moral struggle. He goes to visit her in prison, meets other prisoners, hears their stories, and slowly comes to realize that below his gilded aristocratic world, yet invisible to it, is a much larger world of cruelty, injustice and suffering. Story after story he hears and even sees people chained without cause, beaten without cause, immured in dungeons for life without cause, and a twelve-year-old boy sleeping in a lake of human dung from an overflowing latrine because there is no other place on the prison floor, but clinging in a vain search for love to the leg of the man next to him, until the book achieves the bizarre intensity of a horrific fever dream.
[[File:Tolsoy-Res-Awake 004.jpg|thumb|left|An illustration by [[Leonid Pasternak]] in one of the early English editions.]]

==Popular and critical reception==

The book was eagerly awaited. "How all of us rejoiced," one critic wrote on learning that Tolstoy had decided to make his first fiction in 25 years, not a short novella but a full-length novel. "May God grant that there will be more and more!" It outsold ''[[Anna Karenina]]'' and ''[[War and Peace]]''. Despite its early success, today ''Resurrection'' is not as famous as the works that preceded it.<ref name = Simmons>Ernest J. Simmons, ''Introduction to Tolstoy's Writings'' http://www.ourcivilisation.com/smartboard/shop/smmnsej/tolstoy/chap12.htm</ref>

Some writers have said that ''Resurrection'' has characters that are one-dimensional and that as a whole the book lacks Tolstoy's earlier attention to detail. By this point, Tolstoy was writing in a style that favored meaning over aesthetic quality.<ref name = Simmons/>
 
The book faced much censorship upon publication. The complete and accurate text was not published until 1936. Many publishers printed their own editions because they assumed that Tolstoy had given up all copyrights as he had done with previous books. Instead, Tolstoy retained the copyright and donated all royalties to the [[Doukhobors]], who were Russian pacifists hoping to emigrate to Canada.<ref name = Simmons/>

It is said of legendary Japanese filmmaker [[Kenji Mizoguchi]] that he was of the opinion that "All [[melodrama]] is based on Tolstoy's ''Resurrection''".<ref>{{cite AV media|people=[[Kaneto Shindo|Shindo, Kaneto]]|title=[[Kenji Mizoguchi: The Life of a Film Director]]|year=1975}}</ref>

==Adaptations==
Operatic adaptations of the novel include the ''[[Risurrezione]]'' by Italian composer [[Franco Alfano]], ''[[Vzkriesenie]]'' by Slovak composer [[Ján Cikker]], and ''[[Resurrection]]'' by American composer [[Tod Machover]].

Additionally, various film adaptations, including a Russian film ''Katyusha Maslova'' of director [[Pyotr Chardynin]] (1915, the first film role of [[Natalya Lisenko]]); a 1944 Italian film ''[[Resurrection (1944 film)|Resurrection]]''; a 1949 Chinese film version entitled "蕩婦心" (''A Forgotten Woman'') starring [[Bai Guang]]; a [[Resurrection (1960 film)|Russian film version]] directed by [[Mikhail Shveitser]] in 1960, with [[Yevgeny Matveyev]], [[Tamara Semina]] and [[Pavel Massalsky]], have been made. The best-known film version, however, is [[Samuel Goldwyn]]'s  English-language ''[[We Live Again]]'', filmed in 1934 with [[Fredric March]] and [[Anna Sten]], and directed by [[Rouben Mamoulian]]. The Italian directors [[Paolo and Vittorio Taviani]] released their TV film ''Resurrezione'' in 2001.

A 1968 [[BBC]] mini-series ''Resurrection'', rebroadcast in the US on ''[[Masterpiece Theatre]]''.<ref>{{IMDb title|tt0065338}}.</ref>

==Notes==
<references/>

== External links ==
{{Wikisource|The Awakening: The Resurrection|''The Awakening: The Resurrection''}}
{{Gutenberg|no=1938|name=Resurrection}} translated by [[Louise Maude]]

{{Leo Tolstoy}}
{{Resurrection}}

{{Authority control}}

{{DEFAULTSORT:Resurrection (Novel)}}
[[Category:1899 novels]]
[[Category:Novels by Leo Tolstoy]]
[[Category:Novels first published in serial form]]
[[Category:Philosophical novels]]
[[Category:Works originally published in Russian magazines]]
[[Category:Works originally published in literary magazines]]
[[Category:Dodd, Mead and Company books]]
[[Category:Russian novels adapted into films]]
[[Category:Novels adapted into television programs]]