{{Infobox journal
| title = GAIA
| cover = [[File:GAIA Logo.svg|Version 2|200px]]
| editor = Irmi Seidl
| discipline = [[Environmental science]], [[sustainability science]]
| abbreviation = Gaia
| language = English, German
| publisher = [[oekom verlag]]
| country = 
| frequency = Quarterly
| history = 1992–present
| openaccess = Yes
| license = [[Creative-Commons-licence 3.0 Attribution]]
| impact = 1.397
| impact-year = 2015
| website = http://www.gaia-online.net
| link1 = http://www.ingentaconnect.com/content/oekom/gaia
| link1-name = Online access at [[IngentaConnect]]
| link2 = http://www.oekom.de/nc/zeitschriften/gaia/archive.html
| link2-name = Online archive
| JSTOR =
| OCLC = 57378075
| LCCN = 94640130
| CODEN = 
| ISSN = 0940-5550
}}
'''''GAIA: Ecological Perspectives for Science and Society''''' (''Gaia: Ökologische Perspektiven für Wissenschaft und Gesellschaft'') is a [[peer-reviewed]] [[academic journal]] established in 1992. Its main focus is on background information, analyses, and solutions of environmental and [[sustainability]] problems. Since 2001 it is published by oekom verlag. Articles are in English and German. The [[editor-in-chief]] is Irmi Seidl ([[Swiss Federal Institute for Forest, Snow and Landscape Research]]).

== Sections ==

The journal contains the following sections:

* ''Magazine'': reporting on ongoing environmental and sustainability research, environmental policy, and research promotion
* ''Forum'': original papers, including essays, expressions of opinion, and reactions to articles that have appeared in recent issues
* ''Research'': original scientific articles about environmental and sustainability research
* ''Books'': reviews of new publications

== Masters Student Paper Award ==
In 2014 the ''GAIA'' Masters Student Paper Award was established. It recognizes outstanding results from young scientists in the field of environmental and sustainability research. The prize is awarded in cooperation with the Selbach-Umwelt-Stiftung.<ref>Selbach-Umwelt-Stiftung: [http://www.selbach-umwelt-stiftung.org/index.php?id=355 GAIA Masters Student Paper Award], retrieved March 3, 2015.</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[CAB Abstracts]]<ref name= CABAB>{{cite web |url=http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title=Serials cited |work=[[CAB Abstracts]] |publisher=[[CABI (organisation)|CABI]] |accessdate=2015-03-28}}</ref>
* [[Cambridge Scientific Abstracts]]
* [[Current Contents]]/Social & Behavioral Science<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-03-28}}</ref>
* Current Contents/Agriculture, Biology & Environmental Sciences<ref name=ISI/>
* [[EBSCO Publishing|EBSCO databases]]
* [[GeoRef]]
* [[ProQuest|ProQuest databases]]
* [[Science Citation Index Expanded]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-03-28}}</ref>
* [[Social Sciences Citation Index]]<ref name=ISI/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.397.<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Environmental Sciences |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
* [http://www.oekom.de/etc/gaia/ Website GAIA (English)]
* [http://www.ingentaconnect.com/content/oekom/gaia/ IngentaConnect (full text version)]
* [http://www.oekom.de/ oekom verlag]

{{DEFAULTSORT:Gaia}}

[[Category:Multilingual journals]]

[[Category:Quarterly journals]]
[[Category:Environmental social science journals]]