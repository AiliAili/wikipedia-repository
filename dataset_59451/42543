<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= 336 Skymaster <br/> 337 Super Skymaster
 |image= File:Cessna 337G Skymaster, North Shore Air AN0145753.jpg
 |caption= A Cessna 337G Skymaster
}}{{Infobox Aircraft Type
 |type=Personal use and [[air taxi]] aircraft
 |manufacturer=[[Cessna]]<br />[[Reims Aviation]]
 |designer=
 |first flight= 1961
 |introduced= 1962
 |retired=
 |status= Production completed
 |primary user=Private individuals and organizations
 |more users=<!--up to three more. please separate with <br/>.-->
 |produced=1963-1982
 |number built=2,993<ref name="Janes"/>
 |unit cost=
 |variants with their own articles=[[O-2 Skymaster]] <br/>[[Conroy Stolifter]] <br/>[[Spectrum SA-550]]
}}
|}
[[File:Cessna Skymaster 336 at Tahoe.JPG|thumb|A rare 1964 Cessna 336 seen at [[Truckee Tahoe Airport]] in 2012. The fixed landing gear was later made retractable with the introduction of the 337.]]
[[File:CL215andC-337.JPG|thumb|Part of the contract fleet of Cessna 337 Skymasters on firefighting detection duty with the [[Ministry of Natural Resources (Ontario)|Ontario Ministry of Natural Resources]] at [[Dryden, Ontario]], 1996.]]

The '''Cessna Skymaster''' is a United States twin-engine civil utility [[aircraft]] built in a [[push-pull configuration]]. Its [[Piston engine|engines]] are mounted in the nose and rear of its pod-style fuselage. [[Twin boom]]s extend aft of the wings to the [[vertical stabilizer]]s, with the rear engine between them. The [[Tailplane|horizontal stabilizer]] is aft of the [[pusher propeller]], mounted between and connecting the two booms.<ref name="Janes">Wood, Derek: ''Jane's World Aircraft Recognition Handbook'', page 471. Jane's Publishing Company, 1985. ISBN 0-7106-0343-6</ref>  The combined [[tractor configuration|tractor]] and [[pusher configuration|pusher]] engines produce centerline thrust and a unique sound.<ref name="PlaneAndPilot">Plane and Pilot: ''1978 Aircraft Directory'', page 92. Werner & Werner Corp Publishing, 1978. ISBN 0-918312-00-0</ref> The [[Cessna O-2 Skymaster]] is a military version of the Cessna 337 Super Skymaster.

==Development==
The first Skymaster, Model 336 Skymaster, had fixed landing gear and initially flew on February 28, 1961.<ref name="Taylor">Taylor, Michael: ''Encyclopedia of Modern Military Aircraft''; page 67; Gallery Books; 1987; ISBN 0-8317-2808-6</ref><ref name="Fitzsimons"/> It went into production in May 1963<ref name="Janes"/> with 195 being produced through mid-1964.<ref name="PlaneAndPilot"/>

In February 1965, Cessna introduced the Model 337 Super Skymaster.<ref name="Green">Green, William: ''Observers Aircraft'', page 46. Frederick Warne Publishing, 1974. ISBN 0-7232-1526-X</ref> The model was larger, and had more powerful engines, retractable landing gear, and a dorsal air scoop for the rear engine. (The "Super" prefix was subsequently dropped from the name.)<ref name="PlaneAndPilot"/> In 1966, the turbocharged T337 was introduced, and in 1973, the pressurized P337G entered production.<ref name="PlaneAndPilot"/>

Cessna built 2993 Skymasters of all variants, including 513 military [[Cessna O-2 Skymaster|O-2]] versions.<ref name="Janes"/> Production in America ended in 1982, but was continued by [[Reims Aviation|Reims]] in France, with the FTB337 [[STOL]] and the military FTMA ''Milirole''.<ref name="Fitzsimons"/>

==Design==
The Skymaster handles differently from a conventional twin-engine aircraft, primarily in that if an engine fails, the plane will not [[Aircraft principal axes|yaw]] toward that engine. Without the issue of differential thrust inherent to conventional (engine-on-wing) twins, engine failure on takeoff will not produce yaw from the runway heading.  With no one-engine-out [[Minimum control speeds|minimum controllable speed]] (Vmc), in-flight control at any flying speed with an engine inoperative is not as critical as it is with engines on the wing with the associated leverage; however, performance in speed and, particularly, rate of climb are affected. Flying a Skymaster requires a pilot to hold a multiengine rating, although many countries issue a special "centerline thrust rating" for the Skymaster and other similarly configured aircraft.<ref name="PlaneAndPilot"/>

Ground handling requires certain attention and procedures. The rear engine tends to overheat and can quit while [[taxiing]] on very hot days.<ref name="McClellan">McClellan, J Mac: ''Adam A500'', [[Flying (magazine)|''Flying Magazine'']] pages 52-58. [[Hachette Filipacchi Media U.S.]], December 2007. ISSN 0015-4806</ref> Accidents have occurred when pilots, unaware of the shutdown, have attempted take-off on the nose engine alone, though the single-engine take-off roll exceeded the particular runway length.<ref>{{cite web|url=http://www.ntsb.gov/aviationquery/brief.aspx?ev_id=20111111X12732|title=NTSB Identification: CEN12FA058, 14 CFR Part 91: General Aviation|date=November 10, 2011|publisher=NTSB}}</ref> [[Federal Aviation Administration]] [[Airworthiness Directive]] 77-08-05 prohibits single-engine take-offs and requires the installation of a placard marked "DO NOT INITIATE SINGLE ENGINE TAKEOFF".<ref>{{cite web|url = http://rgl.faa.gov/Regulatory_and_Guidance_Library/rgAD.nsf/AOCADSearch/40B73D072FD60CCE86256A340068370C?OpenDocument|title = Airworthiness Directive 77-08-05|accessdate = 2014-10-24|publisher=Federal Aviation Administration|date=April 28, 1977}}</ref>

The Skymaster's unique sound is made by its rear [[Pusher configuration|pusher propeller]] slicing through turbulent air from the front propeller and over the airframe while its front [[Tractor configuration|tractor propeller]] addresses undisturbed air.<ref name="PlaneAndPilot"/>

==Operational history==
From 1976 until the middle 1990s, the [[California Department of Forestry and Fire Protection]] used O-2 variants of the 337 Skymaster as tactical aircraft during firefighting operations. These were replaced with North American [[OV-10 Bronco]]s, starting in 1993.<ref name="CalFire">{{cite web | publisher=State of California | url= http://calfire.ca.gov/about/about_aviation_history.php | title=CDF aviation management history | accessdate=2015-07-04 }}</ref>

===Brothers to the Rescue===
{{Main|Brothers to the Rescue}}

From 1991 until 2001 the [[Cuba]]n exile group ''[[Hermanos al Rescate]]'' (Brothers to the Rescue) used Skymasters, among other aircraft, to fly [[search and rescue]] missions over the [[Florida Straits]] looking for rafters attempting to cross the straits to [[defector|defect]] from Cuba, and when they found them, dropped life-saving supplies to them. Rescues were coordinated with the [[US Coast Guard]], which worked closely with the group.  They chose Skymasters because their high wing offered better visibility of the waters below, they were reliable and easy to fly for long-duration missions (averaging 7 hours), and they added a margin of safety with twin-engine centerline thrust.  In 1996, two of the Brothers to the Rescue Skymasters were shot down by the [[Cuban Air Force]] over international waters. Both aircraft were downed by a [[Mikoyan MiG-29|MiG-29]], while a second jet fighter, a [[Mikoyan-Gurevich MiG-23|MiG-23]] orbited nearby.<ref name="Minnesota">{{cite web|url = http://www1.umn.edu/humanrts/cases/86-99.html|title = Armando Alejandre Jr., Carlos Costa, Mario de la Pena y Pablo Morales v. Republica de Cuba, Case 11.589, Report No. 86/99, OEA/Ser.L/V/II.106 Doc. 3 rev. at 586 (1999)|accessdate = 2007-12-07|last = [[University of Minnesota]] Human Rights Library|authorlink = |year = 1999}}</ref>

==Variants==
===Cessna===

*'''327 Baby Skymaster''' - reduced scale four-seat version of the 337, with [[cantilever]] wings replacing the 336/337strut-braced configuration. It first flew in December 1967. One [[prototype]] was built before the project was cancelled in 1968 due to lack of commercial interest in the design. The prototype was delivered to [[NASA]] to serve as a full-scale model for [[wind tunnel]] testing. It was used in a joint Langley Research Center and Cessna project on noise reduction and the use of ducted versus free propellers.<ref name="Cessnas">{{cite web|url = http://www.wingsoverkansas.com/features/article.asp?id=461|title = The Cessnas that got away|accessdate = 2008-12-22|last = Murphy|first = Daryl|authorlink = |year = 2006}}</ref>
*'''336 Skymaster''' - production version powered by two {{convert|195|hp|abbr=on}} Continental IO-360-A engines, 195 built.
*'''337 Super Skymaster''' - 336; retractable [[Landing gear|undercarriage]], redesigned nose [[cowling]] and new rear engine intake, and greater wing [[angle of incidence (aerodynamics)|angle of incidence]], powered by two {{convert|210|hp|abbr=on}} Continental IO-360-C engines, 239 built.
[[File:Cessna Super Skymaster 337F N793DS.JPG|thumb|Cessna 337F Super Skymaster]]
*'''337A Super Skymaster''' - 337; minor detail changes, 255 built.
*'''{{Visible anchor|337B Super Skymaster}}''' - 337A; increased take-off gross weight, optional belly cargo pack, 230 built.
*'''T337B (1967) Turbo Super Skymaster''' - 337B; two Continental turbocharged fuel injected {{convert|210|hp|abbr=on}} engines which boosted service ceiling to {{convert|33000|ft|m}}, cruise speed to {{convert|233|mi/h|km/h|abbr=on}}, and range to {{convert|1640|mi|km}}
*'''{{visible anchor|337C Super Skymaster}}''' - 337B; new instrument panel and increased take-off gross-weight, 223 built.
*'''{{Visible anchor|337D Super Skymaster}}''' - 337C; minor detail changes, 215 built.
*'''337E Super Skymaster''' - 337D; cambered wingtips and minor changes, 100 built.
*'''337F Super Skymaster''' - 337E; increased take-off gross weight, 114 built.
*'''{{Visible anchor|337G Super Skymaster}}''' - 337F; split [[airstair]] entry door, smaller rear side windows, improved [[Flap (aircraft)|flaps]], larger front propeller, powered by Continental IO-360-G engines, 352 built.
*'''P337G Super Skymaster''' - 337G; pressurized cabin and turbocharged engines, 292 built.
*'''{{Visible anchor|337H Skymaster}}''' - 337G; minor changes and optional turbocharged engines, 136 built.
*'''P337H Pressurized Skymaster''' - T337G; minor changes, 64 built.
*'''337M''' - US military version designated [[O-2 Skymaster]] in service, 513 built.[[File:Cessna Skymaster O-2 5.jpg|Cessna Skymaster O-2|right|thumb]]
*'''[[Cessna O-2|O-2A]]''' - US military designation of the 337M Forward air control, observation aircraft for the [[US Air Force]]. 501 delivered to the USAF and 12 to the [[Imperial Iranian Air Force]]<ref name="Fitzsimons">Fitzsimons, Bernie: ''The Defenders - A Comprehensive Guide to the Warplanes of the USA'', page 54. Gallery Books, 1988. ISBN 0-8317-2181-2</ref>
*'''O-2B''': Psychological warfare version for the US Air Force (31 former civil aircraft were converted to O-2B standard).<ref name="Fitzsimons"/>
*'''O-2T''': Twin-turboprop version of O-2, with two 317&nbsp;hp (236&nbsp;kW) [[Allison 250]]-B15 engines, a longer span wing and improved high lift devices.<ref name="JAWA69 p304–5">Taylor 1969, pp. 304–305.</ref>
*'''O-2TT''': Improved twin turboprop forward air control aircraft, with same wing (43&nbsp;ft 0 in (13.11 m) wing and engines of O-2T but with new forward fuselage with tandem seating for pilot and observer to give improved view.<ref name="Fitzsimons"/><ref name="JAWA69 p304–5"/>
*'''Summit Sentry O2-337''' : Military version.<ref name="Taylor"/>
*'''Lynx''': Armed military version for the Rhodesian Air Force.
*'''T337H-SP'''

===Reims Cessna===
[[File:Reims FT337G Pressurized Skymaster II, Portugal - Air Force AN0552140.jpg|thumb|right|Reims Cessna FTB337G Milirole of the Portuguese Air Force.]]
*'''F337E Super Skymaster''',  24 built.
*'''F337F Super Skymaster''',  31 built.
*'''F337G Super Skymaster''',  29 built.
*'''FT337G Super Skymaster''', 22 built.
*'''F337H Super Skymaster''',   1 built.
*'''FP337H Pressurized Skymaster''', 1 built.
*'''FTB337G Milirole'''; military F337G with [[Sierra Industries]] Robertson STOL modifications and underwing [[hardpoint]]s, 61 built.<ref name="Fitzsimons"/>
*'''Lynx''' : [[Rhodesia]]n designation for 21 FTB337Gs delivered to the Rhodesian Air Force.

===Conversions/modifications===
*'''[[AVE Mizar]]''' - [[Flying car (aircraft)|flying car]], created by [[Advanced Vehicle Engineers]], was an attachment of Skymaster wings, tail, and rear engine to a [[Ford Motor Company|Ford]] [[Ford Pinto|Pinto]] outfitted with aircraft controls and instruments.<ref>[http://www.fordpinto.com/mitzar1.htm Mitzar, Flying Pinto?] {{webarchive |url=https://web.archive.org/web/20040816075959/http://www.fordpinto.com/mitzar1.htm |date=August 16, 2004 }}</ref>
*'''[[Conroy Stolifter]]''' - an extensive single-turboprop engine STOL cargo plane conversion of the Skymaster. Front engine was replaced with a [[Garrett AiResearch TPE-331]] [[turboprop]]; rear engine was removed, and its space filled with an extended cargo pod.
*'''Groen RevCon 6-X''' - test conversion of a Cessna 337 Skymaster airplane. This aircraft conversion tested the theory of using fixed-wing airplanes as the basic airframes for gyroplanes to reduce cost and shorten development time.
*'''Summit Sentry''' - Summit Aviation of [[Middletown, Delaware]] re-manufactured existing used 337 airframes into the militarized '''O2-337''' which includes four wing-mounted NATO standard pylons capable of carrying 350&nbsp;lb (159&nbsp;kg) each for 7.62&nbsp;mm and 12.7&nbsp;mm gun pods, rocket launchers, bombs, markers and flares. The aircraft was marketed for the target identification and marking, reconnaissance, helicopter escort and aerial photography roles. Examples were sold to the [[Armed Forces of Haiti|Haitian Air Force]], [[Honduras]], [[Nicaragua]], [[Senegal]] and the [[Thai Navy]]. The variant was still in production in 1987.<ref name="Taylor"/>
*'''[[Spectrum SA-550]]''' - built by [[Spectrum Aircraft Corporation]] of [[Van Nuys, California|Van Nuys]], [[California]], it was an extensive single-turboprop engine conversion of a Reims FTB337G constructed in the mid 1980s. They removed the nose engine, lengthened the nose, and replaced the rear engine with a turboprop.<ref>[http://www.skymaster.org.uk/spectrum.asp The Spectrum SA-550]</ref>

==Military operators==
;{{BFA}} : [[Military of Burkina Faso#Air Force|''Force Aérienne de Burkina Faso'']]<ref name="fiwaf04 p47">''Flight International'' 16–22 November 2004, p. 47.</ref>
;{{COL}}: [[Colombian Air Force]]<ref name="fiwaf04 p52">''Flight International'' 16–22 November 2004, p. 52.</ref>
;{{MOZ}}: [[Military of Mozambique|''Força Aérea de Moçambique'']] (3 x FTB-337G) - ex-Portuguese Air Force FTB-337G refurbished and offered by the Government of Portugal to Mozambique in 2011 and 2012.<ref name="Mais Alto">''Mais Alto'' June 2012, p. 16.</ref>
;{{NIC}}:<ref name="fiwaf04 p77">''Flight International'' 16–22 November 2004, p. 77.</ref>
;{{TOG}}: [[Military of Togo|Togo Air Force]]<ref name="Taylor"/>
;{{ZWE}}: [[Air Force of Zimbabwe]]<ref name="Taylor"/>

===Former military operators===
; {{BAN}}: [[Bangladesh Air Force]]<ref name="fiwaf04 p45">''Flight International'' 16–22 November 2004, p. 45.</ref>
; {{CHI}}: [[Chilean Army]] Three examples, between the end of 1970s and mid-1990s, sold to civilian interests.{{Citation needed|date=July 2015}}
; {{TCD}}<ref name="Taylor"/>
; {{ECU}}<ref name="Taylor"/>
; {{SLV}}<ref name="Taylor"/>
; {{EQG}}<ref name="fiwaf04 p56">''Flight International'' 16–22 November 2004, p. 56.</ref>
; {{flag|Guinea-Bissau}}: [[Military of Guinea-Bissau|''Força Aérea de Guine-Bissau'']]<ref name="fiwaf04 p62">''Flight International'' 16–22 November 2004, p. 62.</ref>
; {{HTI}}: [[Armed Forces of Haiti|Haitian Air Force]]<ref name="Taylor"/>
; {{JAM}}<ref name="Taylor"/>
; {{NIG}}: [[Military of Niger]]<ref name="fiwaf04 p77"/>
; {{MEX}}<ref name="Taylor"/>
; {{POR}}: [[Portuguese Air Force]]<ref name="Taylor"/> (32 × FTB-337G): Purchased in 1973 to replace the force's aging [[Dornier Do 27]] fleet, which had been used intensively in the [[Portuguese Colonial War]]. The first 337 deliveries did not arrive until December 1974—after the end of the war. The last Skymaster in service with the Portuguese Air Force was retired on July 25, 2007.
; {{LKA}}: [[Sri Lanka Air Force]]<ref name="Taylor"/>

==Specifications (337D)==
{{Aircraft specifications<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully-formatted line beginning with an asterisk "*" -->
|ref=Jane's All The World's Aircraft 1969–70<ref name="JAWA69 p303-4">Taylor 1969, pp. 303–304.</ref>
|crew=one
|capacity=five passengers
|length main=29 ft 9 in
|length alt=9.07 m
|span main=38 ft 0 in
|span alt=11.58 m
|height main=9 ft 4 in
|height alt=2.84 m
|area main=201 ft²
|area alt=18.7 m²
|empty weight main=2,655 lb
|empty weight alt=1,204 kg
|loaded weight main=<!-- lb-->
|loaded weight alt=<!-- kg-->
|max payload=
|max takeoff weight main=4,400 lb
|max takeoff weight alt=2,000 kg
|engine (prop)=[[Continental IO-360|Continental IO-360-C]]
|type of prop=air-cooled [[flat-six]] piston engine
|number of props=2
|power main=210 [[horsepower|hp]]
|power alt=157 kW
|max speed main=199 mph
|max speed alt=173 [[knot (unit)|kn]], 320 km/h
|max speed more=at sea level
|cruise speed main=144 mph
|cruise speed alt=125 knots, 232 km/h
|cruise speed more=at 10,000 ft (3,050 m) (econ cruise)
|range main=965 mi
|range alt=839 [[nautical mile|nmi]], 1,553 km
|ceiling main=19,500 ft
|ceiling alt=5,945 m
|climb rate main=1,200 ft/min
|climb rate alt=6.1 m/s
|loading main=<!-- lb/ft²-->
|loading alt=<!-- kg/m²-->
|power/mass main=<!-- hp/lb-->
|power/mass alt=<!-- W/kg-->
}}

==See also==
{{Aircontent
|related=
* [[AVE Mizar]]
* [[O-2 Skymaster]]
|similar aircraft=
* [[Adam A500]]
|lists=
|see also=
}}

==References==
{{Reflist|30em}}

*Taylor, John W. R. ''Jane's All The World's Aircraft 1969–70''. London: Sampson Low, Marston & Company, 1969. ISBN 0-354-00051-9.
* [http://www.flightglobal.com/pdfarchive/view/2004/2004-09%20-%202334.html "World Air Forces 2004"]. ''[[Flight International]]'', 16–22 November 2004. pp.&nbsp;41–100.

==External links==
{{commons category|Cessna 337}}
* [http://www.wingsoverkansas.com/photos/cessna/dm/got-away/Cessna327.jpg Photo of the Cessna 327 Baby Skymaster]

{{Cessna}}

[[Category:Cessna aircraft|Skymaster]]
[[Category:Twin-engined push-pull aircraft]]
[[Category:United States civil utility aircraft 1960–1969]]
[[Category:Twin-boom aircraft]]
[[Category:High-wing aircraft]]