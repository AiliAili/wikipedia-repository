{{Infobox film
| name           = Carmen Miranda: Bananas Is My Business
| image          = Bananas is My Business.jpg
| caption        = Cover of the DVD release
| alt            =
| director       = [[Helena Solberg]]
| producer       = {{Plainlist |
* Helena Solberg
* David Meyer
}}
| writer         = Helena Solberg
| starring       = {{Plainlist |
* Cynthia Adler
* Eric Barreto
* Mario Cunha
* [[Alice Faye]]
* [[Aurora Miranda]]
* [[Carmen Miranda]]
* Leticia Monte
* [[Rita Moreno]]
* [[Cesar Romero]]
* Synval Silva
* Helena Solberg {{small|Narrator}}
}}
| distributor    = {{Plainlist |
* Radiante Filmes {{small|(Brazil)}}
* [[Cinema International Corporation]] {{small|(International)}}
* [[PBS]] and Fox Lorber Home Video {{small|(USA)}} }}
| released       = {{Film date|1995|5|13||Brazil}} <!-- PLEASE DO NOT ADD THE 2012 RE-RELEASE DATE! WP:FILM guidelines dictate we must use the earliest and country of origin release dates. Any attempts to add an international airdate will be removed, but can be added in the release section. Thank you. -->
| runtime        = 91 minutes
| country        = {{Plainlist |
* United States
* Brazil
* United Kingdom  }}
| language       = English<br>Portuguese
}}
'''''Carmen Miranda: Bananas is My Business''''' is a 1995 documentary filmed and directed by [[Helena Solberg]].<ref>[http://carmen.miranda.nom.br/hsolberg.htm LANÇAMENTO DE BANANAS IS MY BUSINESS DE HELENA SOLBERG"]</ref> This documentary chronicles the life and career of [[Carmen Miranda]], Hollywood's symbol of Latin American spirit in the 1940s. The documentary tells her life story in a series of stages, beginning with her roots and rise to stardom in her home country of [[Wikipedia:Brazil|Brazil]], her transition and development as a performer in the [[United States]], first on [[Broadway theatre|Broadway]] in [[New York City]], then in the film industry after she signed with [[20th Century Fox]] in [[Los Angeles]], and her later years in life, before her death and her return to Brazil. Helena Solberg uses two different film styles, biography and directorial reverie, in which Solberg uses actor Erick Barretos to “resurrect [[Carmen Miranda]] in several fantasy sequences.<ref>{{cite web |url=https://www.nytimes.com/1995/07/05/movies/film-review-tragic-figure-beneath-a-crown-of-fruit.html/ |title=FILM REVIEW Tragic Figure Beneath A Crown Of Fruit |author= |date= |work= |publisher=THE NEW YORK TIMES |accessdate=June 5, 2012}}</ref> Helena Solberg's attitudes shift throughout the documentary from awe-struck child to empathetic and forgiving Brazilian woman, which she uses to represent the contradictory subplots of Carmen Miranda's life. 
Alongside the fantasy like resurrection of Miranda, Solberg accompanies her documentary with multiple interviews with Carmen Miranda's friends and family, like her sister, her first boyfriend, the guitarist [[Laurindo Almeida]], samba composer Synval Silva, [[Cesar Romero]], and [[Alice Faye]].<ref>{{cite web |url=http://articles.latimes.com/1995-10-07/entertainment/ca-54142_1_carmen-miranda|title=Documentário devassa a síndrome Carmen Miranda |author= |date= December 5, 1994|work= |publisher=[[Folha de S.Paulo]]|accessdate=December 6, 2013}}</ref><ref>{{cite web |url=http://www1.folha.uol.com.br/fsp/1994/12/05/ilustrada/21.html|title=TV Reviews : 'Carmen Miranda' Looks Behind Image|author=KEVIN THOMAS |date=October 7, 1995|work= |publisher=[[The Los Angeles Times]]|accessdate=December 6, 2013}}</ref>

==Brief synopsis==
Born in 1909, Carmen Miranda was already famous in [[Brazil]] during the 1930s; her discovery by [[Broadway theatre|Broadway]] impresario [[Lee Shubert]] in 1939 made her an international star. The film tracks Miranda's astonishing ascent in popularity, from dirt-poor singer and dancer in [[Rio de Janeiro]] to Broadway and [[Hollywood]] (where she became, in 1945, the highest paid female entertainer in the U.S.). Her success came with a price tag: she was caught between being a "real" [[Latin American]] or being Hollywood's version of one—with all the notoriety and fortune it would bring. Today Miranda is a cult figure, known mainly for her exuberant renditions of such songs as "[[South American Way]]" and "The Lady in the Tutti-Frutti Hat," performed in garish costumes topped with fruit-filled turbans.

===The career start===
Carmen Miranda, an almost ghostly character in the imagination of Portuguese, Brazilian, and American audiences, comes back to life in the first scene of the documentary as a dream narrated by [[Helena Solberg]]. Images from her memorial service in [[Rio de Janeiro]] follow, showing the grief of her Brazilian fans as she says goodbye to what she considered her homeland. Born in the small Portuguese village of Varzea da Ovelha e Aliviada on February 9, 1909, Carmen was appropriated by the people of her village as a symbol of success. Making use of interviews with her younger sister [[Aurora Miranda]], the documentary tales the migration story of Carmen, from Portugal to Brazil, where they arrived in November 1909. Carmen Miranda, daughter of a modest barber, Jose Maria Pinto da Cunha, lived in [[Rio de Janeiro]]. There, while working at a hat store, she was first discovered as a singing talent, growing up in Rio de Janeiro, as a working class adolescent, she noticed the strong influence of [[Samba]] music as a powerful cultural aspect of life in Rio’s slums.<ref>{{cite web |url=http://www1.folha.uol.com.br/folha/ilustrada/ult90u501928.shtml|title=Relato da estréia de Carmen Miranda em Nova York é de arrepiar; leia |author= |date= |work= |publisher=[[Folha de S.Paulo]]|accessdate=December 6, 2013}}</ref>

Embracing that current as a way of expressing herself as an artist, Carmen rose through the radio ranks, while “In those days, a girl that sang on the radio was frowned upon… In a world dominated by man, she was able to navigate through those struggles.”As a local artist, she kept a close relationship with composer Synval Silva and [[Laurindo Almeida]] until she left for the United States. This opportunity came in 1939, when she performed along her band of Brazilian musicians. Carmen Miranda embarked on [[The Normandy]] for New York after being signed by [[Lee Shubert]], who included her in the cast for Broadway play 'The Streets of Paris'. This was the episode that transformed the life of who was later to be known as 'The Brazilian Bombshell'. Once in New York, Carmen Miranda showed how her extravagant looks, and beautiful voice spoke for her, despite of the fact that her American audience could not understand a word she was speaking.

===Career in the United States and the fight for her identity===
As she became more popular, and stories about her success were heard in Brazil through the media, Brazilians were skeptical of Carmen Miranda's success in New York. Carmen Miranda found herself fighting tirelessly to prove her identity as a Brazilian,  but also to keep the attention from her American audience. Her appropriation of the style would win her many enemies within Brazil, as she represented a sector of Brazilian culture, the Afro-Brazilian, who represented not only the racialized other according to Brazil’s white elite, but where also a threat to national identity.<ref>{{cite web |url=http://www.sfgate.com/news/article/Carmen-pays-respect-3158308.php|title=Carmen' pays respect|author=Barry Walters|date= February 23, 1996|work=[[The San Francisco Examiner]] |publisher= |accessdate=February 15, 2014}}</ref> The press and the elite constantly attacked her image, many who “looked at her as an embarrassment and an affront to their cultural heritage”.<ref>{{cite web |url=http://community.seattletimes.nwsource.com/archive/?date=20010913&slug=miranda12|title=Flashy actress accepted decades after her death |author=Anthony Faiola |date=September 13, 2001  |work=[[The Seattle Times]] |publisher= |accessdate=}}</ref>

[[Helena Solberg]] also suggests that Miranda’s image was exploited and used by the United States Government during World War II as part of its [[Good Neighbor Policy]], towards Latin America, whose natural resources were vital and needed to fight the war.<ref>{{cite web |url=http://community.seattletimes.nwsource.com/archive/?date=19951005&slug=2145139/ |title=Entertaining 'Gallowglass' As Good As T.V. Gets |author= |date= |work= |publisher=The Seattle Times |accessdate=June 5, 2012}}</ref> Even though this would bring credibility to her image, Carmen, in one of her many identities, would eventually lead her to even larger criticism. When Carmen became a blond for her movies in Hollywood, when the World War II was over, the audience back in Brazil bashed her with critics once again, this time saying that she was too ‘Americanized’ However, her American audience seemed to be captivated by the exotic and colorful style of the singer/actress.

She was sensually silly, a comical icon of fertility, and friendliness that threatened no one. Time after time, Carmen Miranda was consumed by sadness, since she knew that her beloved public image as Carmen Miranda was a commodity to be consumed by U.S. audiences, while her value to her people in Brazil declined as she was considered “Americanized.” Solberg includes interviews with [[Rita Moreno]], who offers her critique of Hollywood’s stereotyping of Latin American women as  "always left by the guy, you had to be vivacious, fiery! an exaggeration."

Opinions differ about Carmen's image, [[Cesar Romero]] says she was out of step with the times, a novelty that wore off. But [[Alice Faye]] and others deny that she could have changed her image and still been employed: “You could argue [with the studio],” she says, “but then you were suspended.” <ref>{{cite web |url=http://brightlightsfilm.com/carmen-miranda-bananas-is-my-business/#.VbjRCLNVikp|title=Carmen Miranda: Bananas Is My Business|author=Gary Morris |date=April 1, 1996|work=brightlightsfilm.com/ |publisher= |accessdate=July 29, 2015}}</ref>

===Death and the aftermath===
The final stage of the movie chronicles the events leading up to Carmen Miranda’s death. In the documentary, Helena Solberg uses interviews with Carmen Miranda's closest friends and workers, such as her housekeeper, to show that its troubled marriage to filmmaker David Sebastian, and her exhausting work schedule led to a deep depression. With doctors orders, Carmen Miranda took a leave from work and traveled back to Brazil to rest for several weeks. Upon her return to Los Angeles, [[Carmen Miranda]] appeared on [[NBC]] television series [[The Jimmy Durante Show]] where on August 4, 1955, Carmen Miranda suffered a mild heart attack after completing a dance number with Jimmy Durante. Carmen Miranda fell to her knees, upon when Jimmy Durante told the band to “stop the music” as he helped Carmen Miranda up to her feet as she laughed, “I'm all out of breath!” Carmen Miranda would finish off the show only to later suffer a second heart attack in her [[Beverly Hills]] home that led to her death.

The documentary shows the service that was held in Los Angeles and also shows the return of her body to Rio de Janeiro in accordance with Carmen Miranda's last wishes.<ref>{{cite web |url=http://articles.chicagotribune.com/2005-07-10/travel/0507090151_1_carmen-miranda-museum-rio-tiny|title=Rio remembers Carmen Miranda|author=Michael T. Luongo|date=July 10, 2005|work= |publisher=[[Chicago Tribune]]|accessdate=March 15, 2014}}</ref> Upon her arrival, the Brazilian government declared a period of national mourning, as more than 60,000 people attended her ceremony at the Rio town hall and where more than half a million Brazilians escorted her body to her final resting place.<ref>{{cite web |url=http://articles.chicagotribune.com/1999-11-26/entertainment/9911260052_1_helena-solberg-south-american-screen|title=Carmen Miranda's Sad Fate |author=John Petrakis |date= November 26, 1999|work= |publisher=[[Chicago Tribune]]|accessdate=December 6, 2013}}</ref>

== Cast ==
*[[Alice Faye]] — Herself
*[[Aloísio de Oliveira]] — Himself
*[[Aurora Miranda]] — Herself
*[[Carmen Miranda]] — Herself (archive footage)
*Caribé da Rocha — Herself
*Cássio Barsante — Herself
*[[Cesar Romero]] —  Himself
*[[Cynthia Adler]] — [[Hedda Hopper]]
*Eric Barreto — Carmen Miranda (Fantasy Sequences)
*Estela Romero — Herself
*[[Helena Solberg]] — Herself (Narrator)
*Ivan Jack — Himself
*Jeanne Allan — Herself
*[[Jorge Guinle]] — Himself
*[[Laurindo Almeida]] — Himself
*Letícia Monte — Carmen Miranda (teenager)
*Mario Cunha — Himself
*Raul Smandek — Himself
*[[Rita Moreno]] — Herself
*[[Synval Silva]] — Himself
*Ted Allan — Himself

== Development ==

=== Production ===
{{Quote box
| quote = "I believe that the vision in relation the figure Carmen's is changing, people are seeing more clearly the impact she caused in both the culture of Brazil, and in the United States. Her legacy is being more valued, in the 40s, there was an elite that did not want see the country's image associated with Carmen and her songs. But she remains a strong icon, that your image follows arousing the interest of different generations."
| source = [[Helena Solberg]] about [[Carmen Miranda]].<ref>{{cite web|url=http://musica.uol.com.br/ultnot/2009/02/08/ult89u10252.jhtm|title=Carmen Miranda é revisitada no dia em que completaria cem anos|work=[[Universo Online|Uol]]|publisher=|accessdate=|date=9 February 2009|first=|last=}}</ref>
| width =20em
| align =
}}

The film was produced by David Mayer and [[Helena Solberg]], with cinematography by Pole [[Tomasz Magierski]]; the rest of the team changed depending on the country where the documentary was being filmed. The film was funded by the [[Corporation for Public Broadcasting]], [[Public Broadcasting Service]], [[Channel 4]] ([[UK]]), [[Radio and Television of Portugal]] and [[:pt:RioFilme|RioFilme]].

The film had a high cost for a documentary, around US $550,000, due to the image rights paid to major studios for the use of Carmen Miranda movies. Through searching, the producers found new images until then unreleased, including [[home movies]] of the singer herself, which added a new cost to the use rights.<ref>{{cite book |url=https://books.google.com/books?id=5fH9bMp--b4C&pg=PA464&lpg=PA464&dq=helena+solberg+the+new+york+times&source=bl&ots=WWdSBbWJzA&sig=n_IIrl0vX7kbJP9JzqE3Q5n6gTs&hl=pt-BR&sa=X&ei=wJy0UvaDGIvrkQeAsYDoBg&ved=0CCoQ6AEwADgK#v=onepage&q&f=false|title=O cinema da retomada: depoimentos de 90 cineastas dos anos 90|author=Lúcia Nagib and Almir Rosa |year=|work= |publisher=Editora 34|accessdate=February 15, 2014}}</ref>

=== Release ===
The documentary had its world premiere at the 27th [[Festival de Brasília|Brasilia Film Festival]] in December 1994, where it won the Audience Award for Best Film, the Special Jury Prize, and the Film Critics' Award. In the U.S., it premiered in [[New York City|New York]] at the Film Forum and has been televised nationally by the [[Public Broadcasting Service|PBS]] in the [[POV (TV series)|POV series]].<ref>{{cite web |url=http://www1.folha.uol.com.br/fsp/1995/10/10/ilustrada/3.html|title=Coluna Joyce Pascowitch: Catapulta|author=Joyce Pascowitch|date=October 10, 1995|work= |publisher=[[Folha de S.Paulo]]|accessdate=}}</ref><ref>{{cite web |url=http://www.sfgate.com/movies/article/FILM-REVIEW-Rise-Descent-of-a-Bombshell-2992798.php|title=FILM REVIEW -- Rise, Descent of a Bombshell / Engaging portrait reveals bitter side of Carmen Miranda's life|author=PETER STACK |year=1996|work= |publisher=[[San Francisco Chronicle]]|accessdate=February 15, 2014}}</ref>

The [[docudrama]] received several awards and was well received by critics at festivals in [[Chicago]], [[Locarno]], [[Toronto]], [[Melbourne]], [[Yamagata, Yamagata|Yamagata]], and [[London]], and closed the year in [[Havana]].<ref>{{cite web |url=http://www.thefreelibrary.com/Helena+Solberg+unmasks+a+Brazilian+idol.-a017943045|title=Helena Solberg unmasks a Brazilian idol.|author= |year=1996|work= |publisher=|accessdate=}}</ref>

=== Awards ===
{|class="wikitable" border="1" cellspacing="1" cellpadding="3"
! Year
! Festival
! Category
! Country
! Result<ref>{{cite web |url=http://www.adorocinema.com/filmes/filme-225238/ |title=Carmen Miranda: Bananas Is My Business - Prêmios|author= |year=|work= |publisher=Adoro Cinema|accessdate=February 15, 2014}}</ref>
|-
|rowspan="3"|1994 || rowspan="3"|[[Festival de Brasília]]<ref>{{cite web |url=http://www.newvideo.com/new-video-digital/carmen-miranda-bananas-is-my-business/|title=Carmen Miranda: Bananas Is My Business|author= |date= |work= |publisher=New Video Group|accessdate=February 11, 2014}}</ref> || Best Documentary || {{BRA}} || {{Won}}
|-
|| Jury's Special Award || {{BRA}} || {{Won}}
|-
|| Critics Award || {{BRA}} || {{Won}}
|-
|rowspan="3"|1995 || [[Havana Film Festival]] || Best Documentary || {{CUB}} || {{Won}}
|-
|| [[Chicago International Film Festival]]<ref>{{cite web |url=http://articles.chicagotribune.com/1996-02-15/features/9602150280_1_jiri-weiss-bananas-michel-piccoli|title=Documentary Praises Carmen Miranda |author=Michael Wilmington |date=February 15, 1996 |work= |publisher=[[Chicago Tribune]] |accessdate=}}</ref> || Best Documentary || {{USA}} || {{Won}}
|-
|| [[Yamagata International Documentary Film Festival]]<ref>{{cite web |url=http://www.imdb.com/title/tt0109381/awards?ref_=tt_awd|title=Awards - Carmen Miranda: Bananas Is My Business|author=|date=February 15, 1996 |work= |publisher=[[IMDb]]|accessdate=}}</ref> || Best Documentary || {{JAP}} || {{Nom}}
|-
|rowspan="2"|1996  || International Film Festival of Uruguay || Best Documentary || {{URU}} || {{Won}}
|-
|| Encontro Internacional de Cinema de Portugal || Best Film - Popular Jury || {{POR}} || {{Won}}
|-
|}

=== Soundtrack ===
{| class="wikitable"
|-
!  style="text-align:center; width:250px;"|Song<ref>{{cite web |url=http://ringostrack.com/pt/movie/carmen-miranda-bananas-is-my-business/13816|title=Listen Carmen Miranda: Bananas Is My Business (1995) Soundtrack|author=|date=|work= |publisher= RingosTrack|accessdate=December 6, 2013}}</ref>
!  style="text-align:center; width:150px;"|Performance
!  style="text-align:center; width:300px;"|Note(s)
|-
|"Adeus Batucada"
| style="text-align:center;"| [[Synval Silva]]
|
*(during the closing credits) 
|-
|"[[Aquarela do Brasil]]"
|style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the film ''[[The Gang's All Here (1943 film)|The Gang's All Here]]'' (1943).
|-
|"Ao Voltar da Batucada"
|style="text-align:center;"| [[Synval Silva]]
|
|-
|"A Week-End in Havana"
| style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the film ''[[Week-End in Havana]]'' (1941).
|-
|"Boneca de Pixe"
| style="text-align:center;"| [[Carmen Miranda]]
| style="text-align:center;"| 
|-
|"Cai, Cai"
| style="text-align:center;"| [[Carmen Miranda]]
|  
*Performance of the film ''[[That Night in Rio]]'' (1941).
|-
|"[[Chica Chica Boom Chic]]"
| style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the film ''[[That Night in Rio]]'' (1941).
|-
|"Camisa Listada"
| style="text-align:center;"| [[Carmen Miranda]]
|
|-
|"Cantores de Rádio"
| style="text-align:center;"| [[Aurora Miranda]]<br>[[Carmen Miranda]]
|
*Performance of the film ''[[Hello, Hello, Carnival!]]'' (1936).
|-
|"Coração"
| style="text-align:center;"| [[Synval Silva]]
|
|-
|"[[Disseram que Voltei Americanizada]]"
|style="text-align:center;"| Erick Barreto
| 
*(as Carmen Miranda)
|-
|"Diz que tem"
| style="text-align:center;"| [[Carmen Miranda]]
|
|-
|"[[I Like You Very Much]]"
| style="text-align:center;"| [[Carmen Miranda]]<br>Erick Barreto	
|
*Performance of the [[short film]] ''Sing With The Stars''
*(as Carmen Miranda)
|-
|"I Make My Money With Bananas"
| style="text-align:center;"| Erick Barreto
|
*(as Carmen Miranda)
|-
|"[[K-K-K-Katy]]"
| style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the [[short film]] ''Sing With The Stars''
|-
|"[[O Que É Que A Baiana Tem?|O que é que a baiana tem?]]"
| style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the film ''[[Banana da Terra]]'' (1939).
|-
|"O Samba e o Tango"
|style="text-align:center;"| [[Carmen Miranda]]
|
|-
|"P'rá Você Gostar de Mim" (Taí)
|style="text-align:center;"| [[Carmen Miranda]]
|
|-
|"Primavera no Rio"
|style="text-align:center;"| [[Carmen Miranda]]
|
|-
|"[[South American Way]]"
|style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the film ''[[Down Argentine Way]]'' (1940).
|-
|"[[Street of Dreams (1932 song)|Street of Dreams]]"
|style="text-align:center;"| [[The Ink Spots]]
|
|-
|"[[Tico-Tico no Fubá]]"
|style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the [[short film]] ''Sing With The Stars''
|-
|"The Lady In The Tutti Frutti Hat"
|style="text-align:center;"| [[Carmen Miranda]]
|
*Performance of the film ''[[The Gang's All Here (1943 film)|The Gang's All Here]]'' (1943).
|-
|"The Soul Of Carmen Miranda"
|style="text-align:center;"| [[John Cale]]
|
*(during the beginning of the credits)<ref>[http://ringostrack.com/pt/movie/carmen-miranda-bananas-is-my-business/13816 Carmen Miranda: Bananas Is My Business Trilha sonora (1995)]</ref>
|-
|}

=== Festivals ===
{|class="wikitable" border="1" cellspacing="1" cellpadding="3"
! Festival<ref>{{cite web |url=http://radiantefilmes.com/filmes/carmen-miranda-bananas-is-my-business/ |title=Prêmios e Festivais|author=|date= |work= |publisher=Radiante Filmes|accessdate=}}</ref>
! Country
! Year
|-
|[[Melbourne International Film Festival]] || {{flag|Australia}} || 1995
|-
|[[Locarno International Film Festival]] || {{flag|Switzerland}} || 1995
|-
|[[Toronto International Film Festival]] || {{flag|Canada}} || 1995
|-
|[[Yamagata International Documentary Film Festival]] || {{flag|Japan}} || 1995
|-
|[[BFI London Film Festival]] || {{flag|England}} || 1995
|-
|[[International Film Festival Rotterdam]] || {{flag|Netherlands}} || 1996
|-
|[[Göteborg International Film Festival]] || {{flag|Sweden}} || 1996
|-
|[[Santa Barbara International Film Festival]] || {{flag|USA}} || 1996
|-
|[[:pt:Festival de Cinema Latino-americano de Toulouse|Rencontres de Cinema d'amérique Latine]] || {{flag|France}} || 1996
|-
|[[:fr:Festival international de films de femmes de Créteil|Festival international de films de femmes de Créteil]] || {{flag|France}} || 1996
|-
|[[Hong Kong International Film Festival]] || {{flag|HK}} || 1996
|-
|Mostra Internacional de Films de Dones de Barcelona || {{flag|Spain}} || 1996
|-
|[[Jerusalem Film Festival]] || {{flag|Israel}} || 1996
|-
|[[Galway|Galway Film Fleadh]] || {{flag|Ireland}} || 1996
|-
|[[New Zealand International Film Festivals|Wellington Film Festival]] || {{flag|New Zealand}} || 1996
|-
|The London Latin American Film Festival || {{flag|England}} || 1996
|-
|[[Helsinki Film Festival]] || {{flag|Switzerland}} || 1996
|-
|[[Films from the South]] || {{flag|Norway}} || 1996
|-
|Festival de Cine Realizado por Mujeres || {{flag|Spain}} || 1996
|-
|International Women's Film Festival Dortmund || {{flag|France}} || 1999
|-
|[[Flying Broom International Women's Film Festival]] || {{flag|Turkey}} || 2000
|-
| It's All True – International Documentary Film Festival || {{flag|Brazil}} || 2014
|-
|}

== Repercussion ==
The film was extremely well received by American criticism. In [[Brazil]], was acquired by networks [[Canal Brasil]] and [[TV Cultura]], and the cable channels [[GNT]] and [[Curta!]]. In [[Latin America]], by the [[Discovery Channel]] and [[Film&Arts]]. In the [[United States]] aired nationally by [[PBS]] network, and in [[France]] by [[Canal Plus]].

=== Critical reception ===

==== In the United States ====
The film received generally positive reviews from critics. Dave Kehr of the ''[[Daily News (New York)|Daily News]]'', commented in his review ''"Carmen Miranda remains one of the most immediately recognizable images in movie history (...) an explosion of fantasy, energy and playful eroticism"'' and that the documentary directed by Helena Solberg ''" is a complex, personal and moving study of a great entertainer who became a victim of the cultural moment she embodied."''<ref>{{cite web |url=http://www.nydailynews.com/archives/nydn-features/bananas-bears-fruit-balanced-performer-ripe-stuff-carmen-miranda-documentary-proves-a-peeling-article-1.699384|title='BANANAS' BEARS FRUIT AS A BALANCED LOOK AT A PERFORMER WITH THE RIPE STUFF, CARMEN MIRANDA DOCUMENTARY PROVES MOST A-PEELING|author=Dave Kehr|date=July 5, 1995|work= |publisher=[[Daily News (New York)|Daily News]]|accessdate=April 24, 2015}}</ref>

In his review of the film, ''[[New York Times]]'' critic [[Stephen Holden]] wrote, ''"The film’s clips of Miranda from the Hollywood years are like jolts of electricity, for she exudes an incandescent vitality along with a percussive vocal bravura. It is sad that Hollywood, after crowning her with bananas, couldn’t think of anything else to do with her except to turn that image into a joke."''<ref>{{cite web |url=https://www.nytimes.com/movie/review?res=990CEED9133CF936A35754C0A963958260|title=FILM REVIEW; Tragic Figure Beneath A Crown Of Fruit|author=[[Stephen Holden]]|date=July 5, 1995|work= |publisher=[[The New York Times]]|accessdate=}}</ref>

In his analysis for ''[[The Village Voice]]'', Amy Taubin wrote that the film was ''"fabulous and gorgeous."'' Godfrey Cheshire in ''[[Variety (magazine)|Variety]]'' says ''"Bananas Is My Business provides a fascinating account of mega star."''<ref>{{cite web |url=http://variety.com/1995/film/reviews/carmen-miranda-bananas-is-my-business-1200442209/|title=Review: ‘Carmen Miranda: Bananas Is My Business’|author=[[Stephen Holden]]|date=July 5, 1995|work=Godfrey Cheshire |publisher=[[Variety (magazine)|Variety]]|accessdate=April 24, 2015}}</ref>

In his review for ''[[The Austin Chronicle]]'', Marjorie Baumgarten recalled that ''"Bananas Is My Business is a documentary that wants to find the person behind the stereotypical icon of Hollywood."''<ref>{{cite web |url=http://www.austinchronicle.com/calendar/film/1996-11-01/137997/|title=Carmen Miranda: Bananas Is My Business|author=Marjorie Baumgarten|date=November 1, 1996|work=|publisher=[[The Austin Chronicle]]|accessdate=April 24, 2015}}</ref>

The ''[[New York Magazine]]'' wrote that ''"Bananas is my Business reveals a much more fascinating figure"'' more criticized the documentary, which in its conception ''"is flawed in several respects."''<ref>{{cite book |url=https://books.google.com/books?id=xOQCAAAAMBAJ&pg=PA57&lpg=PA57&dq=New+York+Magazine+Carmen+Miranda&source=bl&ots=yfbkL-5E6M&sig=D0s2pbMUermjkmA975V6TmOsOgY&hl=pt-BR&sa=X&ei=vYKpU8OcNobKsQSx-4LoCg&ved=0CDIQ6AEwBw#v=onepage&q=New%20York%20Magazine%20Carmen%20Miranda&f=false|title=Carmen Miranda: Bananas Is My Business|author=|date=July 17, 1995|work=|publisher=[[New York Magazine]]|accessdate=April 24, 2015}}</ref>

Ken Ringle writing for the ''[[Washington Post]]'' called the film ''"provocative, affectionate and intelligent"'' and that ''"Bananas Is My Business is a treasure house of fascinating reporting."''<ref>{{cite web |url=https://www.washingtonpost.com/archive/lifestyle/1995/08/25/carmen-miranda-peeling-away-the-myths/8e6a74f0-b9aa-4c88-8f28-5584ce4d78a9/|title=CARMEN MIRANDA': PEELING AWAY THE MYTHS|author=Ken Ringle|date=August 25, 1995|work=|publisher=[[Washington Post]]|accessdate=}}</ref> The ''[[Time Out (magazine)|Time Out]]'' magazine wrote that ''"The most interesting material here relates to Miranda's role as a national symbol for Brazilians, and as the embodiment of Roosevelt's Good Neighbor policy during WWII. The exuberant personality comes through loud and clear, but once she made it in Hollywood, Carmen never varied her act, and it's all too obvious why she was crushed under the weight of all those bananas."''<ref>{{cite web |url=http://www.timeout.com/london/film/carmen-miranda-bananas-is-my-business|title=Carmen Miranda: Bananas Is My Business|author=TCH|date=|work=|publisher=[[Time Out (magazine)|Time Out]]|accessdate=April 24, 2015}}</ref>

[[Jonathan Rosenbaum]] of the ''[[Chicago Reader]]'' called the film ''"Highly personal and informative (...) so interesting and intelligent you’re not likely to object."''<ref>{{cite web |url=http://www.jonathanrosenbaum.net/1996/02/carmen-miranda-bananas-is-my-business-2/|title=Carmen Miranda: Bananas Is My Business
|author=TCH|date=February 16, 1996 |work=[[Jonathan Rosenbaum]]|publisher=|accessdate=April 24, 2015}}</ref>

Tim Purtell for ''[[Entertainment Weekly]]'' says ''"Bananas is my Business traces the Brazilian Bombshell's journey from hat maker to South American recording star to Hollywood, where she was typecast as a Latin bimbo. Piquant interviews with Miranda's sister and various musicians and composers mingle with showbiz dish and, in a playful touch, dramatic reenactments by a man in Miranda drag. He's good, but the show belongs to Miranda herself — in clips, home movies, and rare archival footage — performing with a delirious, inimitable vitality."''<ref>{{cite web |url=http://www.ew.com/article/1996/06/07/carmen-miranda-bananas-my-business|title=Carmen Miranda: Bananas Is My Business
|author=Tim Purtell|date=|work=|publisher=[[Entertainment Weekly]]|accessdate=April 24, 2015}}</ref>

David Hiltbrand magazine ''[[People (magazine)|People]]'''s wrote ''"this documentary is also an unusually personal essay about Miranda's enduring impact on Pan-American culture and on the imagination of Brazilian filmmaker Helena Solberg. Weaving together news, interviews with intimates, vibrant Technicolor footage from Miranda's films and chimerical dream sequences featuring Carmen impersonator Erik Barreto, this film creates a portrait with both depth and flair."''<ref>{{cite web |url=http://www.people.com/people/archive/article/0,,20101750,00.html|title=Picks and Pans Review: Carmen Miranda: Bananas Is My Business|author=|date=October 9, 1995|work=David Hiltbrand |publisher=[[People (magazine)|People]]|accessdate=March 10, 2014}}</ref>

Roger Hurlburt for ''[[Sun Sentinel]]'', wrote that ''"Before adopting the fruit-laden turbans that became her trademark, Miranda was a radio singer. Beautiful, charismatic and optimistic, she once said that all she needed to be happy was a bowl of soup and the freedom to sing (...), she also found herself hovering between two cultures. In Brazil, the press accused her of selling out to Hollywood. At the same time, studio heads refused to allow Miranda to stray from the self-mocking image that made her a top moneymaker (...) Bananas Is My Business examines with sensitivity and integrity the myth that was Carmen Miranda."''<ref>{{cite web |url=http://articles.sun-sentinel.com/1995-10-05/lifestyle/9510030452_1_carmen-samba-latin-americans|title=Miranda Act|author=Roger Hurlburt|date=October 5, 1995|work= |publisher=[[Sun-Sentinel]]|accessdate=May 24, 2014}}</ref>

Barry Walters wrote for the ''[[San Francisco Chronicle]]'' ''"Miranda was a Madonna, one who never had the freedom to change her image with the passing seasons, and Solberg captures the pain of her crippling fame (...) The result is both thoroughly entertaining and deeply moving. You'll never think of the lady with the bananas in the same way again."''<ref>{{cite web |url=http://www.sfgate.com/news/article/Carmen-pays-respect-3158308.php|title=Carmen' pays respect|author=Barry Walters |date=February 23, 1996|work= |publisher=[[San Francisco Chronicle]]|accessdate=July 7, 2015}}</ref>

In its review, the ''[[TV Guide]]'' magazine says ''"director Helena Solberg unearths fascinating material about Miranda's early life and importance within Brazilian pop culture (she was jeered as a sell-out during what was supposed to be a triumphant return to her native country). Belying her image as a goofy Latin bomshell, Miranda emerges as a remarkably canny manipulator of her own star persona and career."''<ref>{{cite web |url=http://movies.tvguide.com/carmen-miranda-bananas-is-my-business/review/130874|title=REVIEW: Carmen Miranda: Bananas Is My Business|author=|date=|work= |publisher=[[TV Guide]]|accessdate=March 10, 2014}}</ref>

[[Jonathan Rosenbaum]] wrote that Solberg's documentary is ''"highly personal and informative–an eye-opener."''<ref>{{cite web |url=http://www.jonathanrosenbaum.net/1996/02/carmen-miranda-bananas-is-my-business-2/|title=Carmen Miranda: Bananas Is My Business
|author=Jonathan Rosenba|date= February 16, 1996|work= |publisher=|accessdate=March 10, 2014}}</ref>

==== In Brazil ====
In Brazil, Sonia Nolasco wrote for ''[[O Estado de S. Paulo]]'' that ''"there are so many surprises and revelations in the documentary "Banana Is My Business" that the viewer gets vexed of ignoring the greatness of our biggest export product."'' Paul Francis for ''[[O Globo]]'' said ''"the film Solberg and Meyer is the first news about the Brazilian cinema that interests me since [[Barravento]]."''

Luiz Zanin Oricchio wrote for ''[[O Estado de S. Paulo]]'' that ''"The film is not a bunch of facts and images. Has an axis, a direction, a thesis without being cold as theorem."''

Inácio Araújo, film critic for the ''[[Folha de S.Paulo]]'' ''"Bananas is my Business, is a beautiful documentary in which Helena Solberg outlines your profile and the trajetora it would take Carmen Miranda international fame, and premature death (...) in the reconstitution sector, there is something to question: any attempt to imitate Carmen results inferior to it (especially dance). The documental part compensate it is failure."''<ref>{{cite web |url=http://www1.folha.uol.com.br/ilustrada/2014/08/1501858-critica-documentario-refaz-a-rota-de-carmen-miranda-ao-sucesso.shtml|title=Crítica: Documentário refaz a rota de Carmen Miranda ao sucesso|author=Inácio Araújo|date=August 18, 2014|work= |publisher=[[Folha de S.Paulo]]|accessdate=March 10, 2014}}</ref>

The journalist [[Arnaldo Jabor]] wrote for the ''[[Folha de S.Paulo]]'' ''"a precious movie (...) Helena Solberg and David Meyer, in a research work and lyricism were beyond mere documentary and redesigned not only the rise and fall of Carmen Miranda but also a portrait of our fragility. You need to watch Bananas Is My Business to see who we are."''<ref>{{cite web |url=http://www1.folha.uol.com.br/fsp/1995/8/08/ilustrada/16.html|title=Carmen foi do getulismo ao capitalismo|author=[[Arnaldo Jabor]]|date=August 8, 1995|work= |publisher=[[Folha de S.Paulo]]|accessdate=March 10, 2014}}</ref>

[[Nelson Motta]] in his review for ''[[O Estado de S. Paulo]]'' wrote that ''"you laugh, you cry, (...) you falls in love madly for this woman, with enormous eyes, and mouth full of sex, joy and music."''

Diogo Mainardi for ''[[Veja (magazine)|Veja]]'' ''"Bananas Is My Business should be adopted as the national motto, coined in all currencies. It is our contribution to humanity, bananas, grimaces and smiles."''<ref>{{cite web |url=http://www.carmen.miranda.nom.br/veja2.html|title=COM BANANAS NA CABEÇA|author=Diogo Mainardi|date=July 26, 1995|work= |publisher=[[Veja (magazine)|Veja]]|accessdate=March 10, 2014}}</ref>

Zuenir Ventura for the ''[[Jornal do Brasil]]'' ''"a definitive and extraordinary film about Carmen Miranda."''

==References==
{{Reflist|30em}}

== External links ==
* {{Official website|http://radiantefilmes.com/filmes/carmen-miranda-bananas-is-my-business}}
* {{IMDb title|0109381}}
* {{AFI film|65111|Carmen Miranda: Bananas Is My Business}}
* ''[http://brightlightsfilm.com/16/carmen.php#.U85KI-NdWPs Carmen Miranda: Bananas is My Business]'' at the [[Bright Lights Film Journal]]
* {{mojo title|carmenmiranda|Carmen Miranda: Bananas is my Business}}
* {{AllRovi movie|134922}}
* {{Rotten Tomatoes|carmen_miranda_bananas_is_my_business}}
* ''[http://movies.tvguide.com/carmen-miranda-bananas-is-my-business/130874 Carmen Miranda: Bananas Is My Business]'' at [[TV Guide]]
* ''[https://www.yahoo.com/movies/film/carmen-miranda-bananas-is-my-business Carmen Miranda: Bananas is My Business]'' at [[Yahoo Movies]]
* ''[http://movies.msn.com/movies/movie-synopsis/carmen-miranda-bananas-is-my-business/ Carmen Miranda: Bananas is My Business]'' on [[MSN Movies]]

{{Carmen Miranda}}

[[Category:1995 films]]
[[Category:Documentary films about entertainers]]
[[Category:Brazilian biographical films]]
[[Category:British biographical films]]
[[Category:Brazilian films]]
[[Category:Portuguese-language films]]
[[Category:Carmen Miranda]]
[[Category:British films]]
[[Category:1990s documentary films]]
[[Category:POV (TV series) films]]
[[Category:World War II films]]
[[Category:Documentary films about women]]