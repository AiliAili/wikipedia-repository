[[Image:ContinuousDiscontinuousAnnealing.png|thumb|upright=1.35|Abnormal or discontinuous grain growth  leads to a heterogeneous [[microstructure]] where a limited number of [[Crystallites|grains]] grow much faster than the rest.]]

'''Abnormal''' or '''discontinuous grain growth''', also referred to as '''exaggerated''' or '''secondary recrystallisation grain growth''', is a [[grain growth]] phenomenon through which certain energetically favorable grains ([[crystallite]]s) grow rapidly in a [[Matrix (geology)|matrix]] of finer grains resulting in a [[bimodal distribution|bimodal]] grain size distribution.<ref name="ref2">{{cite book| last1=Kang| first1=S.J.L | title= Sintering: densification, grain growth, and microstructure| publisher= Elsevier Butterworth Heinemann| year= 2005}}</ref>

In [[ceramic materials]] this phenomena can result in the formation of elongated prismatic, acicular (needle-like) grains in a densified matrix with implications for improved fracture toughness through the impedance of crack propagation.<ref name="ref3">{{cite journal| last1=Padture| first1=NP|last2=Lawn|first2=BR| title= Toughness properties of a silicon carbide with an in situ induced heterogeneous grain structure | journal= J. Amer. Ceram. Soc| year=1994| volume=77| pages=2518–2522.| doi=10.1111/j.1151-2916.1994.tb04637.x}}</ref>

== Mechanisms ==

Abnormal grain growth (AGG) is encountered in metallic or ceramic systems exhibiting one or more of several characteristics.<ref name="ref2" /><ref>{{cite journal| last1=Hillert| first1=M| title= On the theory of normal and abnormal grain growth| journal= Acta metallurgica| year=1965| volume=13| issue=3| pages= 227–238.| doi=10.1016/0001-6160(65)90200-2}}</ref><ref name="ref1" />
# Secondary phase inclusions, precipitates or impurities above a certain threshold concentration.
# High anisotropy in solid/liquid interfacial energy or grain boundary energy (solid/solid) in bulk materials.
# Highly anisotropic surface energy in thin film materials.
# High chemical inequilibrium.

Although many gaps remain in our fundamental understanding of AGG phenomena, in all cases abnormal grain growth occurs as a result of very high local rates of interface migration and is enhanced by the localised formation of liquid at grain boundaries.

== Significance ==

Abnormal grain growth is often recorded as an undesirable phenomenon occurring during the [[sintering]] of ceramic materials as rapidly growing grains may lower the hardness of the bulk material through [[Grain boundary strengthening|Hall Petch type effects]]. However, the controlled introduction of [[dopant]]s to bring about controlled AGG may be used to impart fibre-toughening in ceramic materials. In [[piezoelectric ceramics]] the occurrence of AGG may bring about the degradation of [[Poling (piezoelectricity)|piezoelectric effect]] and thus in these systems AGG is avoided.

== Example systems ==

[[Image:Abnormal Grain Growth in Rutile.png|thumb|300px|Abnormal grain growth observed in [[Rutile]] [[Titanium dioxide|TiO<sub>2</sub>]], induced by the presence of a [[zircon]] secondary phase.<ref name="ref1">{{cite journal| last1=Hanaor| first1=D.A.H | last2=Xu| first2=W|last3=Ferry|first3=M|last4=Sorrell|first4=C.C | title= Abnormal grain growth of rutile TiO<sub>2</sub> induced by ZrSiO<sub>4</sub>| journal= Journal of Crystal Growth| year= 2012| volume=359| pages=83–91|url=http://www.sciencedirect.com/science/article/pii/S0022024812005672|| doi=10.1016/j.jcrysgro.2012.08.015}}
</ref>]]

#[[Rutile|Rutile TiO<sub>2</sub>]] frequently exhibits a prismatic or acicular [[Crystal_habit|growth habit]]. In the presence of alkali dopants or a solid state [[Zircon|ZrSiO<sub>4</sub>]] dopant, rutile has been observed to crystallise from a parent [[anatase]] phase material in the form of abnormally large grains existing in a matrix of finer equiaxed anatase or rutile grains.<ref name="ref1" />
#Al<sub>2</sub>O<sub>3</sub> with [[silica]] and/or [[yttria]] dopants/impurities has been reported to exhibit undesirable AGG.<ref>{{cite journal| last1=Bae| first1=I.J|last2=Baik|first2=S| title= Abnormal grain growth of alumina| journal= J. Amer. Ceram. Soc| year=1997| volume=80| issue=5| pages=1149–1156.}}</ref>
#BaTiO<sub>3</sub> [[barium titanate]] with an excess of TiO<sub>2</sub> is known to exhibit abnormal grain growth with profound consequences on this materials piezoelectric performance.
#[[Tungsten carbide]] has been reported to exhibit AGG of faceted grains in the presence of a liquid cobalt-containing phase at grain boundaries<ref>{{cite journal| last1=Park| first1=YJ|last2=Hwang|first2=NM|last3=Yoon|first3=DY| title=Abnormal growth of faceted (WC) grains in a (Co) liquid matrix| journal=metallurgical and materials transactions| year=1996| volume=27|issue=9| pages=2809–2819.| doi=10.1007/bf02652373}}</ref>
#[[Silicon nitride|Silicon Nitride(Si<sub>3</sub>N<sub>4</sub>)]] may exhibit AGG depending on the size distribution of β-phase material in an α-Si<sub>3</sub>N<sub>4</sub> precursor. This type of grain growth is of importance in the toughening of silicon nitride materials<ref>{{cite journal| last1=Dressler| first1=W| last2=Kleebe|first2=HJ|last3=et. al |title=Model experiments concerning abnormal grain growth in silicon nitride| journal= J. Eur. Ceram. Soc| year=1996| volume=16|issue=1| pages=3–14.| doi=10.1016/0955-2219(95)00175-1}}</ref>
#[[Silicon carbide]] has been shown to exhibit improved fracture toughness as the result of AGG processes yielding elongated crack tip/wake bridging grains, with consequences for applications in ballistic armor. This type of crack-bridging based enhanced fracture toughness of ceramic materials exhibiting AGG is consistent with reported morphological effects on crack propagation in ceramics<ref name="ref3" />
#Strontium Barium Niobate, used for [[electro-optics]] and dielectric applications is known to exhibit AGG with significant consequences on the material's electronic performance<ref>{{cite journal| last1=Lee| first1=HYJ|last2=Freer|first2=R| title= The mechanism of abnormal grain growth in Sr0. 6Ba0. 4Nb2O6 ceramics| journal=J. App. Phys| year=1997| volume=81| issue = =1| pages=376–382.}}</ref>
#[[Calcium titanate|CaTiO<sub>3</sub> perovskite]] systems doped with [[Barium oxide|BaO]] have been observed to exhibit AGG without the formation of liquid as the result of polytype interfaces between solid phases<ref>{{cite journal| last1=Recnik| first1=A| title= Polytype induced exaggerated grain growth in ceramics| journal= J. Eur. Ceram. Soc| year=2001| volume=21|issue=10| pages=2117–2121.| doi=10.1016/s0955-2219(01)00184-4}}</ref>

== See also ==
{{colbegin|2}}
* [[Crystallites]]
* [[Fractography]]
* [[Grain boundary]]
* [[Metallurgy]]
* [[Micrography (Microscopy)|Micrography]]
* [[Micrograph]]
* [[Microstructure]]
* [[Sintering]]
{{colend}}

== References ==

{{reflist}}

== External links ==
* [https://www.sciencemag.org/content/341/6153/1500.abstract Abnormal Grain Growth by Cyclic Heat Treatment]
* [http://www.virginia.edu/ep/SurfaceScience/Thermodynamics.html University of Virginia, Surface Energy]

{{DEFAULTSORT:Abnormal grain growth}}
[[Category:Materials science]]
[[Category:Crystallography]]