{{Infobox journal
| title = Journal of International Affairs
| cover =<!-- Deleted image removed:  [[File:Journal of Intl Affairs Cover - Fall-Winter 2011.jpg|150px]] -->
| discipline = [[international relations|International affairs]]
| publisher = The Trustees of [[Columbia University]] in the City of New York
| editor          = David Kampf
| editor_title    = Editor in Chief
| country = United States
| frequency    = Biannual
| history      = 1947–present
| website = http://jia.sipa.columbia.edu/
| ISSN         = 0022-197X
| OCLC = 39098532
}}

The '''''Journal of International Affairs''''' is a leading foreign affairs journal edited by the graduate students at the [[School of International and Public Affairs]] at [[Columbia University]]. It was established in 1947 as a [[nonprofit]] organization, and is the oldest university-affiliated periodical in the field. The print edition of the ''Journal'' has readers in over eighty countries and has earned worldwide recognition for its unique single-topic format. The ''Journal'''s contributors are drawn from a diverse group of academics and practitioners. In 2011, the Journal began distributing issues electronically [http://www.amazon.com/Authoritarian-Journal-International-Affairs-ebook/dp/B006HHIMOQ/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1329182173&sr=1-1 through the Amazon Kindle store].

==Editorial Board==
{|
|- valign ="top"
|
* Editor in Chief: David Kampf
* Managing Editor: Natasha Cohen
* Senior Editor: Bebe Santa-Wood
* Senior Editor: Tina Cai
* Senior Editor: Sam Schneider
* Senior Editor: Cameron Torreon
* Senior Editor, Special Projects: Shawn C. Bush Jr.
* Director of Digital and Online Content: Christy Grace Provines
* Managing Editor for Online Content: Tuvshin Gantulga
* Senior Editor, Cordier/GPPN: Zoryana Melesh
* Features Editor: Djivo Djurovic
* Book Reviews Editor: Mahua Bisht
* Production Director: Daniele Selby
* Director of Business and Marketing: Sophia Davis
* Director of Editorial Assistants: Meghana Palepu
* Marketing Assistant: Amir Bolouryazad
|}

==Previous Editors in Chief==
{|
|- valign ="top"
|
* Harrison Parker, 1947–48
* John Lippmann, 1948–49
* Richard Rowson, 1950–51
* Richard Newman, 1951–52
* Roger Ross, 1952–53
* Albert Mark, 1953–54
* Richard Cornell, 1954–55
* Richard Palmer, 1955–56
* John Kruse, 1956–57
* R. Laurence Garufi, 1957–58
* Richard Dodson, 1958–59
* Aurelius Fernandez, 1959–60
* Edward Greathead, 1960–61
* Bruce Nelan, 1961–62
* Everett Kelsey, 1962–63
* Bernard Klem, 1963–64
* Aaron Klieman, 1964–65
* Robert Matthews, 1966–67
* John Farrell, 1967–68
* John Quitter, 1968–69
* John Rounsaville, 1969–70
* Paul  Shapiro, 1970–71
* David Sullivan, 1971–72
* James Grant, 1972–73
* Brian Connelly, 1973–74
* Gregg Rubinstein, 1974–75
* Marc Wall, 1975
* Mary-Ellen Pignatelli, 1975–76
* Douglas Cardon, 1976
* Alan Buckley, 1976–77
* Stephen Douglas, 1977–78
|
* David Chaffetz, 1978–79
* Lesley Rimmel, 1980–81
* Martin Marris, 1981–82
* Joshua Katz, 1982–83
* Lauren Kelley, 1984
* Roger Baumann, 1985
* James Ryan, 1986
* Paul Walting, 1987
* William Rogers, 1988
* Martin Malin, 1989
* Julie Rasmussen, 1990
* Gregory White, 1991
* Matthew Tiedemann, 1992
* Melissa Sawin, 1992–93
* Sonali Weerackody, 1993–94
* M. Kathleen Tedesco, 1994–95
* Michael Pedroni, 1996–97
* Ana Cutter, 1997–98
* S. Austin Merrill, 1999–2000
* Robert A. Schupp, 1999–2000
* Deirdre Brennan, 2000–1
* Nancy van Itallie, 2001–2
* Priscilla Ryan, 2002–3
* Genevieve Sangudi, 2003–4
* Hamilton Boardman, 2004–5
* Moran Banai, 2005–6
* Daniel Doktori, 2006–7
* Saul Acosta Gomez, 2007–8
* Eugene Sokoloff, 2008–9
* Gerald Stang, 2009–10	
* Rikha Sharma Rani, 2010–11
|
* Paul Fraioli, 2011–12
* Jon Grosh, 2012–13
* Andrew Zsoldos, 2013–14
* Bryan Griffin, 2014-2015
* Eliza Keller, 2015-2016
|}

==Advisory Board==
{|
|- valign ="top"
|
* [[Richard K. Betts|Richard Betts]]
* [[John Coatsworth]]
* [[Robert Jervis]]
* [[Rashid Khalidi]]
* [[Mahmood Mamdani]]
* [[Sharyn O'Halloran]]
* [[Jenik Radon]]
* [[Anya Schiffrin]]
* [[Stephen Sestanovich]]
* [[Gary Sick]]
* [[Alfred Stepan]]
|}

==Notable contributors==
{|
|- valign ="top"
|
* [[Konrad Adenauer]]
* [[Hannah Arendt]]
* [[Jagdish Bhagwati]]
* [[Boutros Boutros-Ghali]]
* [[Willy Brandt]]
* [[Zbigniew Brzezinski]]
* [[Jimmy Carter]]
* [[Domingo Cavallo]]
* [[Noam Chomsky]]
* [[F.W. de Klerk]]
* [[Mohamed ElBaradei]]
* [[James Grant (financial writer and biographer)|James Grant]]
* [[W. Averell Harriman]]
| 
* [[Wei Jingsheng]]
* [[John Kerry]]
* [[Robert S. McNamara]]
* [[Margaret Mead]]
* [[Luis Moreno Ocampo]]
* [[Hans Morgenthau]]
* [[Condoleezza Rice]]
* Bishop Samuel Ruiz
* [[Rajiv Shah]]
* [[Strobe Talbott]]
* [[Paul Volcker]]
* [[Kenneth Waltz]]
* [[Muhammad Yunus]]
|}

==Recent Issues==
{|
|- valign ="top"
|
* Shifting Sands: The Middle East in the 21st Century (Spring/Summer 2016)
* [http://www.amazon.com/Geopolitics-Energy-Journal-International-Affairs-ebook/dp/B01BLXS07Y/ref=sr_1_fkmr0_1 The Geopolitics of Energy] (Fall/Winter 2015)
* Migration (Spring/Summer 2015)
* Breaking Point: Protests and Revolutions (Fall/Winter 2014)
* [http://jia.sipa.columbia.edu/global-food-security/ Global Food Security] (Spring/Summer 2014)
* [http://jia.sipa.columbia.edu/gender-issue-beyond-exclusion/ The Gender Issue: Beyond Exclusion] (Fall/Winter 2013)
* [http://jia.sipa.columbia.edu/rise-latin-america/ The Rise of Latin America] (Spring/Summer 2013)
* [http://jia.sipa.columbia.edu/in-the-journal/443 Transnational Organized Crime] (Fall/Winter 2012)
* [http://jia.sipa.columbia.edu/in-the-journal/385 The Future of the City] (Spring/Summer 2012)
* [http://jia.sipa.columbia.edu/in-the-journal/317 Inside the Authoritarian State] (Fall/Winter 2011)
* [http://jia.sipa.columbia.edu/in-the-journal/286 Sino-Indian Relations] (Spring/Summer 2011)
* [http://jia.sipa.columbia.edu/in-the-journal/195 Innovating for Development] (Fall/Winter 2010)
* [http://jia.sipa.columbia.edu/in-the-journal/77 Rethinking Russia] (Spring/Summer 2010)
* [http://jia.sipa.columbia.edu/in-the-journal/54 Pakistan & Afghanistan] (Fall/Winter 2009)
* [http://jia.sipa.columbia.edu/in-the-journal/126 Africa in the 21st Century] (Spring/Summer 2009)
* [http://jia.sipa.columbia.edu/in-the-journal/63 Global Finance] (Fall/Winter 2008)
|
* [http://jia.sipa.columbia.edu/in-the-journal/133 Water: A Global Challenge] (Spring/Summer 2008)
* [http://jia.sipa.columbia.edu/in-the-journal/134 Religion & Statecraft] (Fall/Winter 2007)
* [http://jia.sipa.columbia.edu/in-the-journal/278 Iran] (Spring/Summer 2007)
* [http://jia.sipa.columbia.edu/in-the-journal/59 Historical Reconciliation] (Fall/Winter 2006)
* [http://jia.sipa.columbia.edu/in-the-journal/135 The Globalization of Disaster] (Spring/Summer 2006)
* [http://jia.sipa.columbia.edu/in-the-journal/61 The Politics of the Sea] (Fall/Winter 2005)
* [http://jia.sipa.columbia.edu/in-the-journal/130 Financing Development] (Spring/Summer 2005)
* [http://jia.sipa.columbia.edu/in-the-journal/131 State Building] (Fall/Winter 2004)
* [http://jia.sipa.columbia.edu/in-the-journal/132 Land] (Spring/Summer 2004)
* [http://jia.sipa.columbia.edu/in-the-journal/272 Governance] (Fall/Winter 2003)
* [http://jia.sipa.columbia.edu/in-the-journal/273 Central Asia] (Spring/Summer 2003)
* [http://jia.sipa.columbia.edu/in-the-journal/274 Face of the State] (Fall/Winter 2002)
* [http://jia.sipa.columbia.edu/in-the-journal/275 Toeing the Blue Line] (Spring/Summer 2002)
* [http://jia.sipa.columbia.edu/in-the-journal/276 Children] (Fall/Winter 2001)
* [http://jia.sipa.columbia.edu/in-the-journal/277 Rogue States] (Spring/Summer 2001)
* [http://jia.sipa.columbia.edu/in-the-journal/279 Turkey] (Fall/Winter 2000)
|}

==External links==
* Official website: [http://jia.sipa.columbia.edu jia.sipa.columbia.edu]

{{DEFAULTSORT:Journal Of International Affairs}}
[[Category:International relations journals]]
[[Category:Publications established in 1947]]
[[Category:Biannual journals]]
[[Category:Academic journals edited by students]]