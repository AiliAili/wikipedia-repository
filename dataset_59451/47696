[[File:Elisha Hoffman.jpg|thumb|right|Elisha Hoffman]]
'''Elisha Albright (E. A.) Hoffman''' (born May 7, 1839 in [[Orwigsburg, Pennsylvania|Orwigsburg]], [[Pennsylvania]], USA; died 1929 in [[Chicago]], [[Illinois]]) was a [[Presbyterianism|Presbyterian]] minister, composer of over 2,000 hymns and editor of over 50 song books. The son of an Evangelical minister, Hoffman grew up singing sacred hymns both in church and in the home with his parents.<ref name="Hall, 1914">Jacob Henry Hall. ''Biography of Gospel Song and Hymn Writers''. New York: Fleming H. Revell Company, 1914.</ref> After completing high school, Hoffman furthered his education at [[Union Seminary (Pennsylvania)|Union Seminary]] in New Berlin, Pennsylvania, and was subsequently ordained as a Presbyterian minister in 1868.<ref name="Hall, 1914"/>


Following his seminary education, Hoffman began work with the publishing branch of the [[Evangelical Association]] in [[Cleveland]], Ohio.<ref>Donald Paul Hustad. ''Dictionary Handbook to Hymns for the Living Church''. Carol Stream, Illinois: Hope Publishing Company, 1978.</ref> After serving in this position for 11 years, Hoffman held several pastoral positions in the midwest. He pastored churches in both Cleveland and Grafton, Ohio, in the 1880s; moved to Benton Harbor, Michigan and the First Presbyterian Church in the mid-1890s; and finished his ministry in Cabery, Illinois from 1911-22.<ref>George W. Sanville. ''Forty Gospel Hymn Stories''. Winona Lake, Indiana: The Rodeheaver Hall Mack Co., 1943.</ref> Hoffman died in 1929 in Chicago, Illinois, and is buried there in [[Oak Woods Cemetery]].<ref>William Jensen Reynolds. ''Hymns of Our Faith''. Nashville, Tennessee: Broadman Press, 1964.</ref> 

During the course of his life, Hoffman composed over 2,000 hymns, and edited over 50 song books, including: ''The Evergreen'', 1873; ''Spiritual Songs for Gospel Meetings and the Sunday School'', 1878; ''Temperance Jewels'' (Boston, Massachusetts: Oliver Ditson & Company, 1879); ''Bells of Victory'' (Boston, Massachusetts: Oliver Ditson & Company, 1888); ''Pentecostal Hymns No. 1'' (Chicago, Illinois: Hope Publishing Company, 1894); ''Favorite Gospel Songs: A Tune Book'' (Jersey City, New Jersey: J. N. Davis, 1894); and ''Jubilant Voices'' (Chicago, Illinois: Hope Publishing Company, 1907).<ref name="Hall, 1914"/>

==Biography==

Elisha Albright Hoffman was born May 7, 1839 in [[Orwigsburg, Pennsylvania|Orwigsburg]], Schuylkill County, Pennsylvania.<ref>[http://www.hymntime.com/tch/bio/h/o/f/hoffman_ea.htm Elisha Albright Hoffman 1839-1929], Hymntime.com.</ref> Hoffman’s parents, Francis A. and Rebecca A. Hoffman, were both of German descent.<ref name="Hall, 1914"/> His father worked as a minister in the Evangelical Association for over 60 years, which likely influenced Hoffman’s decision to enter the ministry.<ref name="Hall, 1914"/>

Hoffman’s musical education was obtained from his parents. While possessing natural musical abilities, Hoffman never attended a school of music. Any musical instruction Hoffman received came from his experiences at his father’s church or at home.<ref name="Hall, 1914"/> In addition to singing at church, the Hoffman household had a daily family worship time, of which hymn singing was an important part.<ref name="Hall, 1914"/> Hoffman, therefore, became very familiar with the musical and spiritual tradition of Evangelical hymnology at a very early age. It was during these times of family worship that Hoffman developed a love for sacred music and a belief that song was “as natural a function of the soul as breathing was a function of the body.”<ref name="Hall, 1914"/>

During the [[American Civil War]], when Hoffman was 24, he enlisted as a Private in the Union Army on July 9, 1863. He served with Company A, 47th Infantry Regiment, Pennsylvania. He was discharged just over a month later on Aug. 14, 1863.

Hoffman attended public school in Philadelphia and graduated from [[Central High School (Philadelphia)|Central High School]] in the scientific course.<ref name="Hall, 1914"/> After finished high school, Hoffman attended Union Seminary, associated with the Evangelical Association, in New Berlin Pennsylvania.<ref name="Hall, 1914"/> After receiving his degree from Union, Hoffman spent eleven years working with the Evangelical Association's publishing house in Cleveland, Ohio.<ref name="Hall, 1914"/> In 1866 at 26, Hoffman married Susan M. Orwig who was 22 at the time. Hoffman was ordained by the Presbyterian Churches in 1873, at the age of 34. Two years later in 1876, his wife, Susan died, leaving him a single parent of their three sons. 

In early 1879, at the age of 40, Hoffman remarried to Emma, a woman who was 26 years old. The couple had a baby boy in December of that same year, adding to the family's three other boys. At the time, they were living in Cleveland, Ohio, and had Hoffman's sister-in-law living with them and working as a dressmaker. 

Upon leaving his position with the Evangelical Association, Hoffman began his pastoral ministry. From 1880 until his retirement in 1922, Hoffman pastored several churches in Cleveland and Grafton, Ohio; Benton Harbor, Michigan; and Cabery, Illinois.<ref name="Hall, 1914"/> His longest post was held at the Benton Harbor Presbyterian Church in Michigan where he served for 33 years. It was during these years in ministry that Hoffman composed the bulk of his hymns. There are over two thousand hymns composed by Hoffman in print.<ref name="Hall, 1914"/> Among his most popular and widely recognized songs are: "What a Wonderful Saviour!" "Enough for Me," "Are You Washed in the Blood?" "No Other Friend Like Jesus," "I Must Tell Jesus," and "Is Your All on the Altar?” Hoffman also assisted in the compilation and editing process of over 50 different song books.<ref name="Hall, 1914"/>
Hoffman died on November 5, 1929 in Chicago, Illinois.<ref name="Hall, 1914"/>

==Works==

In the vast majority of his compositions Hoffman is the author of both the words and music. In his composition, Hoffman sought to create songs for congregational worship. According to Hoffman, a hymn is "a lyric poem, reverently and devotionally conceived, which is designed to be sung and which expresses the worshipper's attitude toward God or God's purposes in human life. It should be simple and metrical in form, genuinely emotional, poetic and literary in style, spiritual in quality, and in its ideas so direct and so immediately apparent as to unify a congregation while singing it."<ref name="Hall, 1914"/> Operating under this definition of a hymn, most of Hoffman’s compositions are metrically simple (3/4 or 4/4). As per the majority of hymns, Hoffman’s are also very simple in form, usually a collection of 8 or 16 bar stanzas separated by the return of a central refrain.

The typical musical and lyrical style in which Hoffman composed can be seen in one of his most popular hymns "What a Wonderful Savior!" The piece is in common time and in the key of D major. The [[meter (hymn)|metre]] of this hymn is 8.6.8.6 or common metre. (The metre of a hymn refers to the syllables contained in each line of a stanza. Another of Hoffman’s hymns, “Leaning on the Everlasting Arms,” is in long meter, or 8.8.8.8.) As with many of Hoffman’s hymns, the text of this hymn is fairly repetitive, however there are deep theological truths in the simple lyrics.

==Further reading==

# Homaday, Clifford L. “Some German Contributions to American Hymnody.” ''Monatshefte für deutschen Unterricht'' 32, no. 3 (March 1940). {{jstor|30169655}}
# McCutchan, Robert G. “American Church Music Composers of the Early Nineteenth Century.” ''Church History'' 2, no. 3 (September 1933). {{jstor|3159846}}
# Wilson, Robert S. and Melvin R. Wilhoit. “Elisha Albright Hoffman.” ''Hymn: A Journal of Congregational Song'' 35, no. 1 (1984).[http://web.ebscohost.com/ehost/detail?vid=3&hid=123&sid=6e19d35c-5f4a-4322b3c77d97c724e684%40sessionmgr111&bdata=JkF1dGhUeXBlPWlwJnNpdGU9ZWhvc3QtbGl2ZSZzY29wZT1zaXRl#db=mah&AN=MAH0000187723] (accessed September 27, 2012).

==References==
{{reflist}}

==External links==
* [http://www.hymntime.com/tch/bio/h/o/f/hoffman_ea.htm Elisha Hoffman Works List]

{{DEFAULTSORT:Hoffman, Elisha}}
[[Category:1839 births]]
[[Category:1929 deaths]]
[[Category:Presbyterian Church in the United States of America ministers]]
[[Category:19th-century composers]]