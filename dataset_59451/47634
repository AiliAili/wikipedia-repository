{{Infobox artist
| name          = Ealy H. Mays
| image         =Ealy Mays in Paris.jpg|thumb
| caption       = Ealy Mays in Paris.
| birth_name     = Ealy Horton Mays
| birth_date     = {{Birth date|1959|1|15|mf=y}}
| birth_place      = [[Wichita Falls]], [[Texas]]
| nationality   = American
| field         = [[visual artist]], [[painting]], [[drawing]]
}}

'''Ealy Mays''' (born January 15, 1959) is a [[Paris]]-based [[African-American]] [[contemporary artist]]. His work has been exhibited in Mexico’s Galeria Clave, Paris’ [[Carrousel du Louvre]], Mexico’s annual [[José Clemente Orozco]] Art competition, and New York’s [[Solomon R. Guggenheim Museum|Guggenheim]] museum, to name a few. Legendary painter [[Henry O. Tanner]] was the first African American to exhibit at the Louvre in 1897. Mays’ 2005 “Migration of the Superheroes” exhibition at the [[Carrousel du Louvre]] makes him one of the few African-American artists to date to follow Tanner’s footsteps to the Louvre.<ref name="artisandirectltd">[http://artisandirectltd.net/owm/the+artists+showcase/winter2012 The Artist Showcase, Winter 2012 Art Basel edition; Profiled Artist: Ealy Mays], pp. 100–101.</ref>

Mays counts [[Jacob Lawrence]], [[Jackson Pollock]], [[Maxfield Parrish]], [[Diego Rivera]], [[José Clemente Orozco]], [[Rufino Tamayo]], [[Herbert Gentry]], [[Edward Clark (artist)]], and [[Franz Kline]] as mentors. Residing in Paris for the last 15 years, he is a permanent recurring resident at Cité Internationale des Arts in Paris [http://www.citedesartsparis.net], and an alumnus of the [[Skowhegan School of Painting and Sculpture]] in Maine.

==Background==
In a September 1996 ''Dayton Daily News'' article on Ealy Mays, "A Pure Painter", writer Kathy Whyde Jesse<ref>[http://nl.newsbank.com/nl-search/we/Archives?p_action=doc&p_docid=0F51B08F8F0FA991&p_docnum=2 ''Dayton Daily News'', "A Pure Painter - Med Student Quietly Impresses Art World With His Natural Talent".]</ref> profiled the then 37-year-old local Daytonian and Fairview High School graduate who was a medical school student and a recipient of the "[[Camille Cosby]] Fellowship for American Artists of African Descent", on completion of his fellowship at the [[Skowhegan School of Painting and Sculpture]] in Maine, while on a sabbatical from medical school in Mexico. Mays discussed, among other things, the influence of [[Maxfield Parrish]] on his work and his affection for legendary African-American artist [[Jacob Lawrence]], who inspired him to apply to the Skowhegan program. According to Mays, [[Jacob Lawrence]], returned the affection by telling him that he, Mays, was a "pure painter".

===Early years===
The west [[Texas]] native, born in [[Wichita Falls]] [[Texas]], to physician Dr. Dewey Mays [http://www.healthgrades.com/physician/dr-dewey-mays-wfh5t/background-check] and his school teacher wife Mrs. Ruby Mays, was raised in [[Dayton, Ohio]]. The family moved to Dayton after the elder Mays graduated from Howard University's School of Medicine. Mays states that his parents chose to live in Dayton, because at the time in the mid 1960s, Dayton had a strong industrial base and was one of the best places to live and to raise a family in America. His 1998 painting, “Death of an American Boom Town (Dayton Ohio)” demurs the economic decline of the industrial mid-west and in particular, the demise in quality of life in his former hometown of Dayton, Ohio.

[[File:Death of a Factory Town.jpg|thumb|left|Death of an American Boom Town]]

===College and university===
On graduating from Fairview High School in Dayton, Mays attended [[Wiley College]] in Marshal Texas. He then set out in his father's footsteps to study medicine even though art remained his passion. He chose to study in [[Mexico]] where he believed he would have been able to get his education while indulging his passion for art so he applied and was accepted to study medicine at [[Universidad Autónoma de Guadalajara]] School of Medicine (UAG) in 1985.

===The Mexican years===
[[File:Last Train to Chihuahua.jpg|thumb|left|Last Train to [[Chihuahua (state)|Chihuahua]]]]
While in Mexico, Ealy Mays met celebrated Mexican painter [[Rufino Tamayo]] who took notice of his work and became a mentor. According to Mays, Tamayo would be the inspiration for his signature watermelon paintings. [[Rufino Tamayo]] painted red watermelons (sandias) [http://www.lourdessosagaleria.com/tamayo-e.html] and Ealy Mays perfected blue watermelons. Mays also became familiar with the works of legendary Mexican muralists [[José Clemente Orozco]] and [[Diego Riviera]], whose art on extremely large surfaces and [[murals]] impressed him.  Both artists would come to have great influence on Mays' work. He often participated in the annual José Clemente Orozco Art competition. His other exhibits in Mexico included exhibitions at the Tercer Luger October Show in Guadalajara, the American Day Celebration in Guadalajara (Fiesta Americana) [http://upload.wikimedia.org/wikipedia/commons/2/24/Ealy_Mays%27s_exhibition_-_FIESTA_AMERICANA_GUADALARA%2C_MEXICO.pdf], the October Art Fair in Guadalajara, and at Mexico’s Galeria Clava.

[[File:The Dead Dance.jpg|thumb|The Dead Dance]]

Local familiarity with Mexican cultural history led to artworks which took on distinctive Mexican themes and characters and which paid tribute to the subtleties of the Mexican culture - from Lucha Libre and Mexico’s love of free-wrestling to “Día de los Muertos” ([[Day of the Dead]]), which is a Mexican national holiday to celebrate, pray for, and to honor departed loved ones. Such artworks include Mays' "The Dead Dance", which conveyed the Mexican dead celebrating in skeletal forms. His “Last Train to [[Chihuahua (state)|Chihuahua]]”, which depicts a revolution-era train arriving at a station, received wide coverage in local newspapers,[http://commons.wikimedia.org/wiki/File%3AEventos_Culturales_I_copy.jpg] and “The Birth of The Mestizo”, combine many elements of pre-Hispanic Mexican [[Aztec]] culture's encounter with the [[Conquistadores]], and the resulting post-Hispanic Mexico - where the eagle perched on cactus devoured the snake, which today, remains a part of the [[Coat of arms of Mexico]]

[[File:Birth of the Mestizo Warrior copy.jpg|thumb|left|Birth of the Mestizo Warrior]]

===Skowhegan with Jacob Lawrence===
On his return from Mexico Ealy Mays was accepted for a residency at the [[Skowhegan School of Painting and Sculpture]] in Maine, which he still regards as a significant period in his life. Colleagues in residence included fellow artist [[Anish Kapoor]], video artist [[Gary Hill]], photo-artist [[Nan Goldin]], and artist [[Jessica Stockholder]]. Jacob Lawrence was also in residence.<ref name="artisandirectltd" /> Lawrence would later befriend, mentor, and provided a hand written recommendation for Mays, to the [[Studio Museum in Harlem]].

==The Paris years==
Ealy Mays headed to Paris shortly after leaving the [[Skowhegan School of Painting and Sculpture]] in 1996. He followed in the footsteps of other African American artists, performers, and intellectuals such as [[Victor Séjour]], [[Henry O. Tanner]], [[Ira Aldridge]], [[Richard Wright (author)|Richard Wright]], [[James Baldwin]], [[Josephine Baker]], and others who, since the mid-19th century, have chosen Paris and elsewhere in France and Europe for study or expatriate life. Mays believed that Paris could provide him a venue without ostracism or social stigma for his type of work. The city was also home to some of his mentors a generation before him, such as [[Edward Clark (artist)]]. While he continues to exhibit in the United States, he chose the life of an expatriate in Paris since the late '90s. Having been featured in varied collective exhibits in the US prior to living in Paris, such as at the Guggenheim Museum, Mays credits Paris with being the gateway for international recognition and exhibitions in Paris and throughout France, in Italy, Finland, Germany, Russia, Vienna Austria, and even in the United States.

[[File:Cleaning Picasso's Studio copy.jpg|thumb|left|Cleaning Out Picasso's Studio]]

==Past Exhibits / Featured==
In an April 2009 New York Times article, "An American (Celebration) in Paris", Ealy Mays was cited as one of the expat artists featured in a Barack Obama-themed exhibition.  Writer Jon Frosch described his work as "A more satirically edged black-and-white painting by African-American artist Ealy Mays invites the viewer to locate two figures — a lone black soldier and a white man with an Obama T-shirt strumming on a banjo, among a sea of zombie-like Wall Street workers.".<ref>[http://intransit.blogs.nytimes.com/2009/04/09/an-american-celebration-in-paris ''New York Times'' Travel, "An American (Celebration) in Paris".]</ref>

A September 2011 ''Washington Post'' article by travel writer Robin Bennefield, "Understanding black Paris", featured Mays in his favorite pastime as a local historian in pursuit of preserving the legacy of African-American artists and intellectuals in the city.<ref>[http://www.washingtonpost.com/blogs/therootdc/post/understanding-black-paris/2011/08/19/gIQAsbsEfK_blog.html?fb_ref=NetworkNews&fb_source=profile_multiline ''Washington Post'', "Understanding black Paris".]</ref>

In the March 2012 ''Modern Luxury DC Magazine'', Ealy Mays’s "Mona Lisa Likes Pancakes" was featured along with Romare Bearden's "Untitled", Laura Abbate's "Untitled", John McMahon's "Obama 2008", and C. M. Birge's "Dancer Male Bronze", among the collection of fame philanthropist and patron of the arts Reginald Van Lee.[http://www.pcah.gov/members/reginald-van-lee]<ref>[http://digital.modernluxury.com/publication/?i=101916 ''Modern Luxury DC Magazine'', March 2012, pp. 96–97.]</ref>

In 2012, Mays exhibited in the Contemporary Art Fair in New York.<ref>[http://www.contemporaryartfairnyc.com/participants.php Contemporary Art Fair New York.]</ref> He appears regularly in local Parisian newspapers art round-ups such as a ''[[Le Monde]]'' October 2008 piece that featured Mays' submarine in a local art show and described it as "a formidable submarine which takes one into other waters",<ref>[http://dominiquehasselmann.blog.lemonde.fr/2008/10 Ealy Mays in ''Le Monde''′s "Nail Hunting" section.]</ref> and ''[[Le Figaro]]''′s coverage of a local Barack Obama-themed gallery exhibition, "Obama in a Parisian Gallery",.<ref>[http://www.lefigaro.fr/elections-americaines-2008/2008/11/06/01017-20081106ARTFIG00556-obama-s-affiche-dans-une-galerie-parisienne-.php Ealy Mays in ''Le Figaro''′s "Obama in a Parisian Gallery".]</ref>

==Current exhibits and feature==
Over the years, Ealy Mays has produced contemporary and historical stories through his visual art. He has carefully infused ethnic, political, and satirical subtleties, in tribute to generations of artists before him, while keeping his guard as a social critic of his time. "Cleaning Out Picasso's studio" is his nod to the influence of African artifacts on the legendary painter's work. "Le Garçon" is his nod to his adopted city with a depiction of a bustling Parisian waiter in early morning action. Mays' submarine installations include cross cultural depictions of Russian culture in "The Russian Sub" and "The Kursk" which pays tribute to sailors of the doomed Kursk in the Barents Sea.

[[File:Le Garcon copy.jpg|thumb|Le Garçon]]

Mays' work is featured in American exhibits both at the University of Maryland’s David C. Driskell Center For The Study of The Visual Arts and Culture of African Americans and The African Diaspora,<ref>[http://www.driskellcenter.umd.edu/Thompson/Tradition%20Redefined%20FINAL%20checklist%20for%20publicity.pdf University of Maryland David C. Driskell Center For The Study of The Visual Arts and Culture of African Americans and The African Diaspora.]</ref> and in Rice University's Travelling Art Exhibition,<ref>[http://issuu.com/riceuniversity/docs/rice-magazine-issue-14-2012/1 Rice University's Travelling Art Exhibition, Tradition Redefined.]</ref> wherein his “The Last Vernissage” figures prominently among the likes of [[Romare Bearden]], [[Beauford Delaney]], [[Herbert Gentry]], [[Lois Mailou Jones]], [[Richard Mayhew]], [[Henry O. Tanner]], and others, in Larry and Brenda Thompson’s “Tradition Redefined, A collection of African American Artists."

Most recently Ealy Mays' works "Death of an American Boom Town", "Submarine of Radicals" and "Uptown Ice Cream", were featured in "Landscape of Being",<ref>[http://www.margaretwheatley.com/articles/The-Landscape-of-Being.pdf Landscape of Being], pp. 24–25, 30.</ref> Agency Art Life and Society's international ebook, (pp.&nbsp;24–25, 30), and tribute project by global artists, to the state of being in the world, based largely on the philosophies of German sculptor and performance artist [[Joseph Beuys]].

Ealy Mays was also profiled in ''The Artist Showcase'' magazine's December 2012 Art Basel Miami edition, which also featured the works of Nelson Mandela at Robben Island.<ref>[http://artisandirectltd.net/owm/the+artists+showcase/winter2012 ''The Artist Showcase'', Winter 2012 Art Basel edition; Featured Artist: Nelson Mandela, pp. 14–17.]</ref>

==References==
;Notes
{{Reflist}}

==External links==
*[http://www.ealymaysatelier.com/ Ealy Mays's Official Website]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Mays, Ealy}}
[[Category:1959 births]]
[[Category:Living people]]
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:21st-century American painters]]
[[Category:African-American artists]]
[[Category:American people of Jamaican descent]]
[[Category:American contemporary painters]]
[[Category:Artists from Texas]]
[[Category:Artists from Dayton, Ohio]]
[[Category:American expatriates in France]]
[[Category:People from Skowhegan, Maine]]