{{Technical|date=August 2009}}
In Microsoft's [[.NET Framework]], the '''Common Type System''' ('''CTS''') is a standard that specifies how [[data types|type]] definitions and specific values of types are represented in 
computer memory. It is intended to allow programs written in different programming languages to easily share information. As used in [[programming language]]s, a [[data types|type]] can be described as a definition of a set of values (for example, "all integers between 0 and 10"), and the allowable operations on those values (for example, addition and subtraction).

The specification for the CTS is contained in [[Ecma]] standard 335, "Common Language Infrastructure (CLI) Partitions I to VI." The [[Common Language Infrastructure|CLI]] and the CTS were created by Microsoft, and the [[.NET Framework|Microsoft .NET framework]] is an implementation of the standard.

==Functions of the Common Type System==
*To establish a framework that helps enable cross-language integration, type safety, and high performance code execution.
*To provide an [[object-oriented]] model that supports the complete implementation of many programming languages.
*To define rules that languages must follow, which helps ensure that objects written in different languages can interact with each other.
*The CTS also defines the rules that ensures that the data types of objects written in various languages are able to interact with each other.
*The CTS also specifies the rules for type visibility and access to the members of a type, i.e. the CTS establishes the rules by which assemblies form scope for a type, and the Common Language Runtime enforces the visibility rules.
*The CTS defines the rules governing [[type inheritance]], virtual methods and object lifetime.
*Languages supported by .NET can implement all or some common data types…

When [[rounding]] fractional values, the [[Rounding#Round half to even|halfway-to-even]] ("banker's") method is used by default, throughout the Framework. Since version 2, "Symmetric Arithmetic Rounding" (round halves away from zero) is also available by programmer's option.<ref>{{cite web | url = http://msdn.microsoft.com/en-us/library/system.midpointrounding.aspx | title = MidpointRounding Enumeration | work = [[MSDN Library]]}}</ref>
*it is used to communicate with other languages are present

==Type categories ==
The common type system supports two general categories of types:
;Value types : [[Value type]]s directly contain their data, and instances of value types are either allocated on the [[call stack|stack]] or allocated inline in a structure. Value types can be built-in (implemented by the runtime), user-defined, or enumerations.
;Reference types : [[Reference type]]s store a reference to the value's memory address, and are allocated on the [[dynamic memory allocation|heap]]. Reference types can be self-describing types, pointer types, or interface types. The type of a reference type can be determined from values of self-describing types. Self-describing types are further split into arrays and class types. The class types are user-defined classes, boxed value types, and delegates.

The following example written in [[Visual Basic .NET]] shows the difference between reference types and value types:
<source lang="vbnet">
Imports System

Class Class1
    Public Value As Integer = 0
End Class 'Class1
 
Class Test
    Shared Sub Main()
        Dim val1 As Integer = 0
        Dim val2 As Integer = val1
        'Coded By
        val2 = 123
        Dim ref1 As New Class1()
        Dim ref2 As Class1 = ref1
        ref2.Value = 123
        Console.WriteLine("Values: {0}, {1}", val1, val2)
        Console.WriteLine("Refs: {0}, {1}", ref1.Value, ref2.Value)
    End Sub 'Main
End Class 'Test
</source>
The output of the above example

 Values: 0, 123
 Refs: 123, 123

===Boxing and unboxing===

===Boxing===
Converting value types to reference types is also known as [[Boxing (computer science)|boxing]]. As can be seen in the example below, it is not necessary to tell the compiler an Int32 is boxed to an object, because it takes care of this itself.
<source lang="csharp">
Int32 x = 10; 
object o = x ; // Implicit boxing
Console.WriteLine("The Object o = {0}",o); // prints out "The Object o = 10"
</source>
However, an Int32 can always be explicitly boxed like this:
<source lang="csharp">
Int32 x = 10; 
object o = (object) x; // Explicit boxing
Console.WriteLine("The object o = {0}",o); // prints out "The object o = 10"
</source>

===Unboxing===
The following example intends to show how to unbox a reference type back to a value type. First an Int32 is boxed to an object, and then it is unboxed again. Note that unboxing requires explicit cast.
<source lang="csharp">
Int32 x = 5; 
object o1 = x; // Implicit Boxing
x = (int)o1; // Explicit Unboxing
</source>

==See also==
*[[.NET Framework]]
*[[Blittable types]]
*[[Common Language Infrastructure]]

==References==
{{Reflist}}

==External links==
*[https://msdn.microsoft.com/en-us/library/zcx1eb1e(v=vs.110).aspx Microsoft developer's guide describing the CTS]
*[https://msdn.microsoft.com/en-us/library/hfa3fa08(v=vs.110).aspx Built-in types in the .NET Framework]

{{Common Language Infrastructure}}

[[Category:Common Language Infrastructure]]