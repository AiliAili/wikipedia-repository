<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox aircraft begin
 |name= Short 360
 |image= File:Shorts360airsey.jpg
 |caption= An [[Air Seychelles]] Short 360
}}{{Infobox aircraft type
 |type= Transport aircraft 
 |manufacturer= [[Short Brothers]]
 |first flight= 1 June 1981
 |introduced= November 1982
 |retired=
 |primary user= [[Air Cargo Carriers]]
 |more users= [[Emerald Airways]] <br>[[Aeroperlas]] <br>[[Skyway Enterprises]]
 |produced= 1981–1991
 |number built= 165
 |unit cost=
 |developed from= [[Short 330]]
 |variants with their own articles= [[Short C-23 Sherpa|Short C-23B/C Sherpa]]
}}
|}

The '''Short 360''' (also '''SD3-60'''; also '''Shorts 360''')<ref name="Mondey p. 228">Mondey 1981, p. 228.</ref> is a commuter aircraft that was built by British manufacturer [[Short Brothers]] during the 1980s. The Short 360 seats up to 39 passengers and was introduced into service in November 1982. It is a larger version of the [[Short 330]]. It is also affectionately known in aviation circles as "The Shed".

==Development==
During the 1970s the world's commuter airline market began to evolve from the 20-seat class to larger and more comfortable cabins. Short Brothers of Northern Ireland had created the [[Short SC.7 Skyvan|Skyvan]] in 1962, followed by the related but larger [[Short 330]] in 1974.

[[File:Short SD360 cabin interior.jpg|thumb|left|Cabin interior of [[Manx Airlines]] Short 360 showing 'box' structure and two-and-one seating layout]]

The Short 360 is a 36-seat derivative of the 30–33 seat [[Short 330]]. In high density configuration, 39 passengers could be carried. The two Short airliners have a high degree of commonality and are very close in overall dimensions. The later 360 is easily identified by a larger, swept tail unit mounted on a revised rear fuselage. The 360 has a 3'0" (91&nbsp;cm) fuselage "plug" which gave sufficient additional length for two more seat rows (six more passengers), while the extra length smoothed out the aerodynamic profile and reduced [[Parasitic drag#Profile drag|drag]].<ref name="Mondey p. 228"/> Seating is arranged with two seats on the starboard side of the cabin and one seat on the port side. The 360's power is supplied by two Pratt & Whitney PT6A-65Rs. The development was announced in 1980, with the prototype's first flight on 1 June 1981<ref name="Simpson 2001 495">{{Harvnb|Simpson|2001|p=495}}</ref> and [[Type certificate|type certification]] awarded on 3 September 1981.

After initiating production with the basic model, Short marketed a number of 360 developments. First was the ''360 Advanced'', in late 1985, with 1,424 shp (1,062&nbsp;kW) PT6A-65-AR engines. That was followed by the ''360/300'', in March 1987, with six-blade propellers, more powerful PT6A-67R engines, and aerodynamic improvements, giving a higher cruise speed and improved "hot and high" performance. The 360/300 was also built in 360/300F freighter configuration.

==Operational history==

The first production Short 360 had its maiden flight on 19 August 1982<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1982/1982%20-%202182.html?search=Suburban|title=embraer - fairchild - 1982 - 2182 - Flight Archive|publisher=}}</ref> and entered service with [[Suburban Airlines]] (later merged with [[Allegheny Airlines]]/[[US Airways]]) in November 1982.<ref name="Eastwood/Roach p. 455">Eastwood/Roach p. 455.</ref> Building on the strengths and reputation of its 330 antecedent, the 360s found a niche in regional airline use worldwide, being able to operate comfortably from 4,500&nbsp;ft (1,400 m) runways – opening up hundreds of airfields that would otherwise be inaccessible to airliners. With a cruise speed about 215&nbsp;mph (370&nbsp;km/h), at an altitude of 10,000&nbsp;ft (3,048 m), the [[Cabin pressurization|unpressurized]] 360 was not the fastest turboprop in its market but it offered acceptable performance at a reasonable price combined with ease of service and maintainability.<ref>Smith 1986, p. 2.</ref> The PT6A turboprops are fully ICAO Stage 3 noise-compliant, making the 360 one of the quietest turboprop aircraft operating today. Production of the 360 ceased in [[1991 in aviation|1991]] after 165 deliveries.<ref name="air_360">[http://www.airliners.net/info/stats.main?id=354 "Short 360."] ''Airliners.net.'' Retrieved: 9 August 2007.</ref> In 1998, approximately 110 360s were in service.<ref name="air_360"/> In 2009 a retired Emerald Airlines 360 was bought by Kingsland Primary School in Stoke-on-Trent, for use as a mobile classroom.<ref>Narain, Jayra. [http://www.dailymail.co.uk/news/article-1165888/Fasten-seat-belts-children-new-geography-classroom-landed-playground.html "Fasten your seat belts, children, your new geography classroom has landed in the playground."] ''Daily Mail,'' 31 March 2009. Retrieved: 18 May 2011.</ref>

==Variants==
[[File:94-0308 Short C-23B+ Sherpa U.S. Army (9311649275).jpg|thumb|U.S. Army Short C-23B+ Sherpa]]

*'''360-100''' - the first production model with Pratt & Whitney Canada PT6A-65R turboprop engines.<ref name="Frawley_2003">Frawley 2003, p. 193.</ref>
*'''360 Advanced''' - with PT6A-65AR engines rated at 1,424 shp (1,062&nbsp;kW) each.<ref name="Frawley_2003" /> The aircraft was later redesignated ''360-200''. Introduced in late 1985.<ref name="Frawley_2003"/>
*{{visible anchor|'''360-300'''}} - with more powerful PT6A-67R engines with six-blade [[Propeller (aircraft)|propeller]]s. Higher cruise speed and improved performance.<ref name="Frawley_2003"/>
*'''360-300F''' - the freighter version of the -300,<ref name="Frawley_2003"/> with capacity for five [[LD3]] cargo containers.
*'''[[Short C-23 Sherpa]]''' B+ and C variants are military-configured Short 360s operated by the [[Military of the United States|United States military]].<ref name="olive-drab">[http://www.olive-drab.com/idphoto/id_photos_c23sherpa.php "Olive-Drab: C-23."] ''olive-drab.com.'' Retrieved: 18 August 2010.</ref>

==Operators==
[[File:Tiara Air Shorts 360 Dallimonti.jpg|thumb|A Tiara Air Short 360 at Aruba Airport]]
[[File:British Airways Express (G-BVMY), Dublin, June 1995 (01).jpg|thumb|right|A British Airways Express Short 360 at [[Dublin Airport]] in 1995]]

In 2013 there were a total of 13 Short 360 aircraft (all variants) in passenger service with [[Air Seychelles]] (1), [[Deraya Air Taxi]] (2), [[Pacific Coastal Airlines]] (2, stored out of service), [[Servicios Aéreos Profesionales]] (1), [[Tiara Air]] (2), [[Trans Executive Airlines|Interisland Airways]] (1), [[La Costeña (airline)|La Costena]] (1), [[Comeravia]] (1), [[Malu Aviation]] (1) and [[Ayit Aviation and Tourism]] (1).<ref>''[[Last Chance to Fly]]'', 2012.</ref>  The Short 360 specifically proved very popular with the U.K.'s regional airlines including the Isle of Man-based-Manx. This fed passengers into larger hubs in England, Scotland and Ireland.{{citation needed|date=October 2014}}

Current and previous operators have included:

=== Civil operators ===
;{{flag|Argentina}}
*[[Líneas Aéreas Privadas Argentinas|LAPA]]
;{{flag|Australia}}
*[[Hazelton Airlines]]
*Murray Valley Airlines
*[[Sunshine Express Airlines]]
*[[Sunstate Airlines]]
*Airlines of Tasmania
;{{flag|Aruba}}
*[[Tiara Air]]
;{{flag|Canada}}
*[[Pacific Coastal Airlines]]
;{{flag|China}}
*[[Civil Aviation Administration of China|CAAC]]
;{{flag|Costa Rica}}
*[[Grupo TACA|TACA]] (SANSA)
;{{flag|Democratic Republic of Congo}}
*[[Malu Aviation]]
;{{flag|Dominican Republic}}
*[[SAP Air Group]]
;{{flag|Germany}}
*[[Nightexpress]]
*[[Rheinland Air Service]] (RAS)
*[[Express Airways]] (EPA)
;{{flag|Guam}}
*[[Freedom Air (Guam)]]<ref>[http://www.freedomairguam.com/charter.htm "Freedom Air."] ''freedomairguam.com.'' Retrieved: 18 August 
2010.</ref>
;{{flag|Guatemala}}
*[[Grupo TACA|TACA]] (INTER)
;{{flag|Guernsey}}
*[[Aurigny]]
;{{flag|Honduras}}
*[[Grupo TACA|TACA]] (ISLEÑA)<ref>[http://www2s.biglobe.ne.jp/~ito-nori/s360.html "HR-IAP."] ''biglobe.ne.jp''. Retrieved: 18 August 2010.</ref>
;{{flag|Ireland}}
*[[Aer Arann]]
*[[Aer Lingus]]<ref>[http://www.airliners.net/open.file/223859/M/ "EI-BSP."] ''airliners.net.'' Retrieved: 18 August 2010.</ref>
;{{flag|Israel}}
*[[Ayit Aviation and Tourism]]
;{{flag|Nicaragua}}
*[[Grupo TACA|TACA]] (LA COSTENA)<ref>[http://www.airliners.net/photo/La-Costeña/Short-360/0996521/&sid=1f933cdb7ce66c67fb685d50ffd820d3 "La Costeña."] ''airliners.net''.</ref>
;{{flag|Panama}}
*[[Grupo TACA|TACA]] [[Aeroperlas]]
;{{flag|Philippines}}
*[[Philippine Airlines]]
;{{flag|Portugal}}
*[[Aero VIP (Portugal)|Aero Vip]]
;{{flag|Puerto Rico}}
*[[Air Flamenco]]
*[[M&N Aviation]]
;{{flag|Seychelles}}
*[[Air Seychelles]] (Former)
;{{flag|Thailand}}
*[[Thai Airways International|Thai Airways]]<ref>[http://www2s.biglobe.ne.jp/~ito-nori/s360.html "HS-TSE."] ''biglobe.ne.jp.'' Retrieved: 18 August 2010.</ref>
;{{flag|United Kingdom}}
*[[HD Air Ltd|HD Air]] ''(formerly BAC Express)''<ref>[http://www.hdair.com/fleet.htm "Fleet: G-CLAS," "G-EXPS," "G-TMRA" and "G-TMRB."] ''hdair.com.'' Retrieved: 18 August 2010.</ref>
*[[British Airways|British Regional Airways]]/[[Loganair]]<ref>[http://www.users.zetnet.co.uk/jcurry/balogan.htm "G-BNMT."] ''users.zetnet.co.uk.'' Retrieved: 18 August 2010.</ref>
*[[Manx Airlines]] 
*Jersey European (now [[Flybe]])<ref>[http://www.airliners.net/open.file/629226/M/ G-OBHD] Retrieved: 18 August 2010.</ref>
*[[Gill Airways]]<ref>[http://www.users.zetnet.co.uk/jcurry/gillshort.htm "G-BNYI."] ''users.zetnet.co.uk.'' Retrieved: 18 August 2010.</ref>
;{{flag|United States}}
* Allegheny Commuter Airlines (operated by Pennsylvania Airlines and Suburban Airlines)
*[[American Eagle Airlines|American Eagle]] (operated by [[Executive Airlines]], [[Flagship Airlines]] and [[Simmons Airlines]])
*[[Air Cargo Carriers]]
* [[Business Express]]
* [[Comair]]
* Dash Air
*[[Federal Express]]<ref>[http://www.fedex.com/ag/about/facts.html "About FedEx: FedEx Facts."] ''FedEx.'' REtrieved: 18 May 2011.</ref>
*[[Gulfstream International Airlines]]
* [[Imperial Airlines]]
* Interisland Airways (Hawaii)
* [[Mississippi Valley Airlines]] (MVA)
*[[Trans Executive Airlines]]
* Trans International Express
* [[United Express]] (operated by [[WestAir Commuter Airlines]])
* [[US Airways Express]] (operated by Allegheny Commuter Airlines)
*[[Smokejumper|US Forest Service]] (smokejumper aircraft)

A number of small air cargo airlines have also operated the Short 360 in freight operations in the U.S.

=== Military operators ===
;{{flag|United States}}
*[[US Army]]
;{{flag|Venezuela}}
*[[Venezuelan Air Force]] (Fuerza Aérea Venezolana)

==Accidents and incidents==
The Short 360 has been involved in 15 hull-loss accidents, resulting in the loss of 16 airframes.<ref>[http://aviation-safety.net/database/dblist.php?field=typecode&var=441%&cat=%1&sorteer=datekey&page=1 "Short 360: hull losses."] ''aviation-safety.net.'' Retrieved; 24 April 2013.</ref>
* 22 October 1985: A CAAC flight overran the runway while landing at Enshi Airport. There were no fatalities, but the airframe was written off.
* 31 January 1986: An Aer Lingus flight crashed on approach to East Midlands Airport, UK, due to airframe icing and turbulent conditions. There were no fatalities, but the airframe was written off.
* 13 December 1987: [[Philippine Airlines Flight 443]], using a Short 360 registration EI-BTJ crashed into a 5,000' mountain in the [[Philippines]] while approaching Iligan. All 11 passengers and 4 crew on board were killed.
* 28 November 1989: A newly built aircraft, not yet delivered to a customer, was destroyed by a bomb on the apron at Belfast City Airport, Northern Ireland. The device had been planted by the IRA. There were no fatalities., 
* 20 August 1990: A CCAir aircraft parked at Charlotte-Douglas Airport (Charlotte, North Carolina, USA) was blown by a wind gust into an electrical power cart, and a fire started. There were no fatalities, but the airframe was written off.
* 25 November 1997: An aircraft operated by Corporate Air landed heavily at Billings-Logan Airport (Billings, Montana, USA) in gusty wind conditions. The nosewheel strut collapsed, leading to a crash with the loss of the airframe. There were no fatalities.
* 9 February 1998: A British Regional Airlines aircraft landed heavily at Stornoway Airport In Scotland. The undercarriage was damaged leading to a crash with the loss of the airframe. There were no fatalities. 
[[File:Shorts 360 right landing gear fire.jpg|thumb|right|Air Cargo Carriers Flight 1290 damage caused by brake fire.]]
* 13 January 2000: A Sirte Oil Company Short 360 crashed on approach near [[Brega]];  22 of the 41 passengers and crew on board were killed.
* 4 February 2001: An Air Aran Short 360 crashed on approach to Sheffield City Airport, UK, after reverse thrust was selected while the aircraft was still airborne. There were no fatalities.
* 27 February 2001: [[Loganair Flight 670]] crashed into the [[Firth of Forth]] in Scotland shortly after takeoff from Edinburgh Airport. Both engines failed after ingesting  blowing snow while on the ground. Both pilots were killed (no others on board).
* 21 August 2004: A [[Venezuelan Air Force]] Short 360 crashed into a mountain while descending to land at [[Maracay]], killing all 30 people on board.
* 16 December 2004: An Air Cargo Carriers aircraft was lost at Oshawa Municipal Airport. After landing on a snow-covered runway, the pilot attempted a go-around when he realized he would be unable to stop. The aircraft failed to gain altitude and crashed. There were no fatalities.
* 5 February 2006: Two Short 360 freighters operated by Air Cargo Carriers were flying in formation when they collided near Watertown, Wisconsin, US. One lost a wing section and crashed, killing the three occupants; The other aircraft remained under control but was damaged beyond repair during an emergency landing at an airport.
* 17 May 2012: An Air Cargo Carriers Short 360 (registration N617FB) was substantially damaged following a wheel brake fire during taxi at the George Bush Intercontinental Airport, Houston, Texas. There were no injuries to the flight crew of two. The airplane was 60 pounds over Max. takeoff weight and during the long  taxi to position for takeoff the crew used a higher than normal power setting and rode the brakes in an attempt lower weight by burning fuel. It's intended destination was Austin, Texas.
* 29 October 2014: A [[Skyway Enterprises Flight 7101|Skyway Enterprises]] Short SD-360 (registration N380MQ) cargo flight on behalf of FedEx, scheduled from Sint Maarten, Kingdom of Netherlands to San Juan, Puerto Rico lost altitude during climb out and crashed into the water about 2 nautical miles off the end of runway at about 18:35L (22:35 UTC), killing both members of the flight crew.

==Specifications (360-300)==
[[File:Aruba-Short-360-Tiara-Air.JPG|thumb|right|Side view of Tiara Air Short 360-100]]

{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's All The World's Aircraft 1988–89<ref name="Janes 88 p305-7">Taylor 1988, pp. 305–307.</ref>

|crew=Three (Two pilots plus one cabin crew)
|capacity=36 passengers
|length main=70 ft 9⅝ in
|length alt=21.58 m
|span main=74 ft 9½ in
|span alt=22.80 m
|height main=23 ft 10¼ in
|height alt=7.27  m
|area main=454 ft²
|area alt=42.18 m²
|airfoil=[[NACA]] 63A series (modified)
|empty weight main=17,350 lb
|empty weight alt=7,870 kg
|max takeoff weight main=27,100 lb
|max takeoff weight alt=12,292 kg

|engine (prop)=[[Pratt & Whitney Canada PT6]]A-67R
|type of prop=turboprop
|number of props=2
|power main=1,424 [[horsepower|shp]]
|power alt= 1,062 kW

|max speed alt=242 kn, 470 km/h
|max speed main=280 mph
|max speed more=(at 18,000 ft/3,409 m)
|cruise speed main=249 mph
|cruise speed alt=216 kn, 400 km/h
|cruise speed more=at 22,000 ft (4,167 m)
|stall speed main=85 mph
|stall speed alt=73 kn, 136 km/h
|stall speed more=(flaps and landing gear down)
|range main=732 mi 
|range alt=636 [[nautical mile|nmi]], 1,178km
|ceiling main=20,000 ft MSL <ref>FAA TCDS A41EU</ref>
|ceiling alt=6,096 m MSL
|climb rate main=952 ft/min
|climb rate alt=4.7 m/s
}}

==See also==
{{aircontent
|see also=
|related=
*[[Short 330]]
*[[Short C-23 Sherpa]]
*[[Short SC.7 Skyvan]]
|similar aircraft=
|lists=
*[[List of civil aircraft]]
}}

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Barnes C.H. and Derek N. James.''Shorts Aircraft since 1900''.  London: Putnam, 1989. ISBN 0-85177-819-4.
* Donald, David, ed. ''The Encyclopedia of Civil Aircraft''. London: Aurum, 1999. ISBN 1-85410-642-2.
* Eastwood Tony and Roach John.''Turbo Prop Airliner Production List''.  West Drayton: The Aviation Hobby Shop, 2007. 
* Frawley, Gerard. ''The International Directory of Civil Aircraft, 2003/2004''. Fyshwick, ACT, Australia: Aerospace Publications Pty Ltd., 2003. ISBN 1-875671-58-7.
* Mondey, David. ''Encyclopedia of the World's Commercial and Private Aircraft''. New York: Crescent Books, 1981. ISBN 0-517-36285-6.
* Simpson, Rod. ''Airlife's World Aircraft''.  London: Airlife Publishing Ltd., 2001. ISBN 1-84037-115-3.
* [[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1988–89''. Coulsdon, UK: Jane's Defence Data, 1988. ISBN 0-7106-0867-5.
* Smith, P.R. ''Shorts 330 and 360'' (Air Portfolios 2). London: Jane's Publishing Company Limited, 1986. ISBN 0-7106-0425-4.
{{Refend}}

==External links==
{{Commons category|Short 360}}
* [http://www.airliners.net/info/stats.main?id=354 Short 360 page on Airliners.net]

{{Short Brothers aircraft}}

[[Category:British airliners 1980–1989]]
[[Category:Short Brothers aircraft|360]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Turboprop aircraft]]