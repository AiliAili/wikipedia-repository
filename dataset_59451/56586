{{notability|date=October 2015}}
{{Infobox journal
| title = Digital Humanities Quarterly
| cover = 
| editor = Julia Flanders
| discipline = [[Humanities]]
| abbreviation = 
| publisher = [[Alliance of Digital Humanities Organizations]]
| country =
| frequency = Quarterly
| history = 2007–present
| openaccess = Yes
| license = [[Creative Commons licenses|Creative Commons Attribution-Noncommercial-No Derivative Works 3.0]]
| impact = 
| impact-year = 
| website = http://www.digitalhumanities.org/dhq/
| link1 =
| link1-name =
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 122912409
| LCCN = 2007214388
| CODEN = 
| ISSN = 1938-4122
| eISSN = 
}}
'''''Digital Humanities Quarterly''''' is a [[peer review|peer-reviewed]] [[Open access (publishing)|open-access]] [[academic journal]] covering all aspects of digital media in the humanities. The journal is also a community experiment in journal publication.<ref name="diglib">{{Cite web| title = Digital  Humanities| work = Digital Library Federation| accessdate = 2011-07-17| url = http://www.diglib.org/community/groups/digitalhumanities/}}</ref>

The journal is funded and published by the [[Alliance of Digital Humanities Organizations]]<ref>{{Cite journal| doi = 10.1093/llc/fqr002| volume = 26| issue = 1| pages = 3–4| last = Vanhoutte| first = Edward| title = Editorial| journal = [[Literary and Linguistic Computing]]| accessdate = 2011-07-11| date = 2011-04-01| url = http://llc.oxfordjournals.org/content/26/1/3.short}}</ref> and its [[editor-in-chief]] is Julia Flanders.<ref name="howard" />

== Editorial policy ==
''Digital Humanities Quarterly'' has been noted among the "few interesting attempts to peer review born-digital scholarship."<ref>{{Cite news| last = Katz| first = Stan| title = Reviewing Digital Scholarship| work = The Chronicle of Higher Education| accessdate = 2011-07-13| date = 2010-05-31| url = http://chronicle.com/blogs/brainstorm/reviewing-digital-scholarship/24402}}</ref> Having emerged from a desire to disseminate digital humanities practices to the wider arts and humanities community and beyond,<ref>{{Cite journal| doi = 10.1093/llc/fqm037| volume = 23| issue = 1| pages = 103–108| last = Archer| first = Dawn| title = Digital Humanities 2006: When Two Became Many| journal = Literary and Linguistic Computing| accessdate = 2011-07-11| date = 2008-04-01| url = http://llc.oxfordjournals.org/content/23/1/103.short}}</ref> the journal is committed to [[Open access (publishing)| open access]] and [[open standard]]s to deliver journal content, publishing under a [[Creative Commons]] license.<ref name="About">{{cite web|url=http://www.digitalhumanities.org/dhq/about/about.html |title=About DHQ | work= Digital Humanities Quarterly |accessdate=2011-03-31}}</ref> It develops translation services and multilingual reviews in keeping with the international character of the Alliance of Digital Humanities Organizations. <ref name="diglib" />

The journal aims to heighten the visibility and acceptance of digital humanities with reviews that are modeled on traditional book reviews but focus on digital projects, providing assessments of "software tools, sites, other kinds of innovations that need the same kind of critical scrutiny and benefit from the same kind of contextualizing review that a traditional book review offers."<ref name="howard">{{Cite news| issn = 0009-5982| last = Howard| first = Jennifer| title = Hot Type: No Reviews of Digital Scholarship = No Respect| work = The Chronicle of Higher Education| accessdate = 2011-07-12| date = 2010-05-23| url = http://chronicle.com/article/Hot-Type-No-Reviews-of/65644/}}</ref>

== References ==
{{reflist|35em}}

== External links ==
* {{Official|http://www.digitalhumanities.org/dhq/}}

{{humanities-journal-stub}}

[[Category:Open access journals]]
[[Category:Digital humanities]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2007]]
[[Category:Multidisciplinary humanities journals]]
[[Category:English-language journals]]