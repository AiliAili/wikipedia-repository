{{Infobox journal
| title = Chaos
| cover = 
| discipline = [[Nonlinear systems]]
| abbreviation = Chaos
| editor = [[Jürgen Kurths]]
| publisher = [[American Institute of Physics]]
| country = United States
| history = 1991-present
| frequency = Quarterly
| impact = 2.049
| impact-year = 2015
| website = http://chaos.aip.org/
| link2 = http://scitation.aip.org/content/aip/journal/chaos/browse
| link2-name = Online archive
| ISSN = 1054-1500
| eISSN = 1089-7682
| CODEN = CHAOEH
| OCLC = 35131011
| LCCN = 91657086
}}
'''''Chaos: An Interdisciplinary Journal of Nonlinear Science''''' is a quarterly [[peer-reviewed]] [[scientific journal]] covering [[nonlinear systems]] and describing the manifestations in a manner comprehensible to researchers from a broad spectrum of disciplines. The [[editor-in-chief]] is [[Jürgen Kurths]] of the [[Potsdam Institute for Climate Impact Research]].

== Abstracting and indexing == 
The journal is abstracted and indexed in the [[Science Citation Index ]] and [[Current Contents]]/Physical Chemical and Earth Sciences.<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2017-01-06}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.049.<ref name=WoS>{{cite book |year=2015 |chapter=Chaos |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://chaos.aip.org/}}

[[Category:American Institute of Physics academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]
[[Category:Physics journals]]