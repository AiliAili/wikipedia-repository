{{Use dmy dates|date=May 2015}}
{{EngvarB|date=May 2015}}
{{Infobox person
| name        = Nick Leventis
| image       = Nick Leventis.jpg
| alt         = Nick Leventis
| caption     = Image from the [[Strakka Racing]] Photograph Archive, February 2010
| birth_date  = {{Birth date|df=yes|1980|01|31}}<!-- {{Birth date|df=yes|YYYY|MM|DD}} -->
| birth_place = London
| death_date  = <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place = 
| nationality = English
| other_names = 
| known_for   = 
| occupation  = Racing driver
| spouse= 
| website={{URL|nickleventis.com}}
| residence=[[Notting Hill, London|Notting Hill]]
}}
'''Nick Leventis''' (born 31 January 1980) is a British racing driver and founder of [[Strakka Racing]]. In 2010, the Strakka team of Leventis, [[Danny Watts]], and [[Jonny Kane]] won the [[2010 24 Hours of Le Mans]] in the LMP2 category.  In August Leventis and Strakka secured their first overall victory in the [[Le Mans Series]], winning the [[2010 1000 km of Hungaroring|1000&nbsp;km of Hungaroring]].<ref name="Strakka">{{cite web|url=http://www.strakkaracing.com |title=Strakka Racing \\ Professional Racing Team \ Silverstone |publisher=Strakkaracing.com |date=31 December 2010 |accessdate=22 February 2011}}</ref>{{Primary source claim|date=June 2011}}

In 2013, Nick will contest the [[FIA World Endurance Championship]] for a second time. The series features 6-hour races in Europe, USA, South America, the Middle East, the Far East and includes the famous [[Le Mans 24 Hour]] race in France. He'll race a 200&nbsp;mph Strakka Racing [[Honda Performance Development ARX-03c]] LMP1 prototype.

In addition to his own racing career, Nick is dedicated to helping young and aspiring driving talent through [[Strakka Performance]] – a bespoke driver development programme he created in 2012.

He has also raised many thousands of pounds for charity by abseiling down [[The Shard]] in London (Western Europe's tallest skyscraper), raising money for the [[Commando Spirit Appeal]] on behalf of the [[Royal Marines Charitable Trust Fund]], and has ski-dived from 30,000&nbsp;ft above [[Mount Everest]] for the [[Global Angels]] charity

==Early life==
Nick Leventis was born in [[Kensington]], London and educated at [[Harrow School]].  Leventis initially competed in downhill [[alpine skiing]] until a serious back injury in 2003 forced him to quit the sport at a professional level.  From here he turned his focus to motorsports.

==Racing==

Having missed out on racing in junior formula, Nick began his racing career in 2004, driving a BMW M3 in the [[BMW LMA Euro Saloon Championship]] – dominating Class B and finishing runner-up in the overall standings. He also proved his merit in endurance racing, achieving a class win and 6th overall in the first-ever [[Britcar Silverstone 24 Hours]], co-driving with [[Duller Motorsport]].

In 2005 Nick branched out, racing a variety of cars and demonstrating his inherent versatility and mechanical understanding by tackling not only contemporary disciplines, but also historic events as well. He competed in the [[Ferrari Historic Challenge]] and [[Le Mans Classic]], campaigning a rare [[Ferrari Dino 246]] and scoring encouraging results. He achieved back-to-back victories in the [[Roy Salvadori Trophy]] at the [[Silverstone Classic]] meeting with a [[1957 Aston Martin DBR1]] in both 2006 and 2007, and in 2007 he also reached a personal landmark with a Le Mans podium in the Legends race with a [[Ferrari P3]].

Nick competed in the Belgian and Italian touring car series, gaining several podiums, two class victories at Spa and consecutive wins in the prestigious [[Vallelunga 6 Hour Silver Cup]] in 2006 and 2007. Although still relatively inexperienced in pure mileage terms, he had ably demonstrated that the next step on the ladder – racing a works [[Aston Martin DBR9 GT1]] – was well within his reach. His conviction was rewarded with a podium in the [[Epilogue of Brno 6 Hour]] race and a GT1 class win and 2nd overall in the highly competitive [[Vallelunga Gold Cup]].

Nick made his debut at the [[Le Mans 24 Hours]] in 2008 – although the [[Aston Martin DBR9 GT1]] was to retire with mechanical problems in the 17th hour.

In 2009, Nick and Strakka Racing entered a [[Ginetta]]-[[Zytek]] [[Ginetta-Zytek GZ09S|GZ09S]] LMP1 sports prototype in the [[Le Mans Series]]. Partnered by [[Danny Watts]] and [[Peter Hardman]], Nick and the team claimed pole in Barcelona and finished 14th in LMP1 in the [[Le Mans 24 Hours]].

For 2010, Strakka Racing raced a LMP2 twin-turbo V6 engined HPD ARX-01c in the Le Mans Series, recording one of the most remarkable seasons in sportscar racing. LMP2 victory at the Le Mans 24 Hour race broke five records, including the highest ever overall finish for a LMP2 car (5th), a new lap record and the greatest distance ever covered in the famous race by a LMP2 car. The team also achieved the first ever LMS outright win for a LMP2 chassis, with victory at the 1,000&nbsp;km race at the [[Hungaroring]]. 

Nick's driver development programme continued in the LMS in 2011. Nick helped the team finish runner-up in LMP2 for a second consecutive season.

In 2012, Strakka Racing contested the inaugural [[FIA World Endurance Championship]] with a new 3.4-litre normally aspirated V8 engined HPD ARX-03a, with Nick racing in America, the Middle East and the Far East for the first time. The team finished on the [[FIA Endurance Trophy for Private LMP1 Teams']] podium in all eight races – achieving the 100% record with an outstanding final stint at Shanghai, when Nick took the car from 4th to 3rd in the closing stages. The highlight of the season was a 3rd overall finish at the 6 Hours of Bahrain.

He was the brainchild behind the creation of [[Strakka Performance]] in 2012, a driver development programme that enlists the Strakka Racing engineering department, data and systems, two race cars and the coaching expertise of team-mates [[Danny Watts]] and [[Jonny Kane]]. In the first six months of operation, [[Strakka Performance]] ran drivers in Europe, South Africa and Asia.

In 2013, Nick will contest the [[FIA World Endurance Championship]] in a [[Strakka Racing HPD ARX-03c]].

Nick has a son, Marley, and has run the [[New York City Marathon]] (in a time of 3hrs 11mins 33secs). He has also raised many thousands of pounds for charity by abseiling down [[The Shard]] in London (Western Europe's tallest skyscraper), raising money for the [[Commando Spirit Appeal]] on behalf of the [[Royal Marines Charitable Trust Fund]], and has ski-dived from 30,000&nbsp;ft above [[Mount Everest]] for the [[Global Angels charity]].

==Extreme sports==
After his skiing accident sidelined his ability to compete in the sport, Leventis began to develop an interest in extreme sports and is a regular surfer and wake boarder.  In 2007 Nick joined the [[Relentless (drink)|Relentless Artist]] stable.<ref name = "Relentless">{{cite web|url=http://www.relentlessenergy.com/artists/view/mickey-smith |title=Artists &#124; Relentless |publisher=Relentlessenergy.com |date=20 February 2007 |accessdate=22 February 2011}}</ref>

In October 2010 Nick completed a 30,000&nbsp;ft tandem skydive over [[Mount Everest]] with jump partner Tom Noonan in what was a new Himalayan record.<ref name  = "Everest Skydive">{{cite web|url=http://www.everest-skydive.com |title=EVEREST SKYDIVE 2011 |publisher=Everest-skydive.com |date= |accessdate=22 February 2011}}</ref> The jump was made in aid of the [[Global Angels]] foundation, a children's charity working with disadvantaged children<ref name = "Global Angels">{{cite web|url=http://www.globalangels.org/news/angels-news/nick-leventis-aiming-high |title=Nick Leventis – 'Aiming High' |publisher=Globalangels.org |date= |accessdate=22 February 2011}}</ref> The Everest jump is considered to be the most dangerous in the world, given the high altitude and thick air.<ref name = "Daily Sports Car">[http://www.dailysportscar.com/viewArticle.cfm?articleUID=793DF7AD-1143-FDC9-35FC646058937D8D  " A new high for Nick Leventis World Record Skydive"] ''[[Daily Sports Car]]''. London October 2010. Retrieved on 2010 – 10</ref> Nick Leventis will be attempting to create solo skydive records in the New Year (2011).<ref name = "Explore Himalaya">[http://www.explorehimalaya.com/blog/nick-leventis-sets-a-new-skydiving-record/ "Nick Leventis sets a new skydiving record"] "[[Explore Himalaya]]". London October 2010. Retrieved on 2011 – 02</ref> Leventis' skydiving has been part of an effort to raise funds for his personal charity, [[Global Angels]].

==Racing record==
===24 Hours of Le Mans results===
{| class="wikitable" style="text-align:center; font-size:90%"
|-
! Year
! Team
! Co-Drivers
! Car
! Class
! Laps
! {{Tooltip|Pos.|Overall Position}}
! {{Tooltip|Class<br>Pos.|Class Position}}
|-
! [[2008 24 Hours of Le Mans|2008]]
|align="left"| {{flagicon|DEU}} [[Vitaphone Racing Team]]<br>{{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Peter Hardman]]<br>{{flagicon|BRA}} [[Alexandre Sarnes Negrão]]
|align="left"| [[Aston Martin DBR9]]
| GT1
| 82
| DNF
| DNF
|-
! [[2009 24 Hours of Le Mans|2009]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Peter Hardman]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[Ginetta]]-[[Zytek]] [[Ginetta-Zytek GZ09S|GZ09S]]
| LMP1
| 325
| 21st
| 14th
|-
! [[2010 24 Hours of Le Mans|2010]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Jonny Kane]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[HPD ARX-01C]]
| LMP2
| 367
| 5th
|style="background:#FFFFBF;"| '''1st'''
|-
! [[2011 24 Hours of Le Mans|2011]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Jonny Kane]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[HPD ARX-01d]]
| LMP2
| 144
| DNF
| DNF
|-
! [[2012 24 Hours of Le Mans|2012]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Jonny Kane]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[HPD ARX-03a]]
| LMP1
| 303
| 30th
| 8th
|-
! [[2013 24 Hours of Le Mans|2013]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Jonny Kane]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[HPD ARX-03c]]
| LMP1
| 332
| 6th
| 6th
|-
! [[2015 24 Hours of Le Mans|2015]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Jonny Kane]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[Strakka Racing|Strakka]] [[Dome (constructor)|Dome]] S103-[[Nissan]]
| LMP2
| 264
| DNF
| DNF
|-
! [[2016 24 Hours of Le Mans|2016]]
|align="left"| {{flagicon|GBR}} [[Strakka Racing]]
|align="left"| {{flagicon|GBR}} [[Jonny Kane]]<br>{{flagicon|GBR}} [[Danny Watts]]
|align="left"| [[Zytek Z11SN|Gibson 015S]]-[[Nissan]]
| LMP2
| 351
| 8th
| 4th
|}

===Complete FIA World Endurance Championship results===
{| class="wikitable" style="text-align:center; font-size:90%"
! Year
! Entrant
! Class
! Car
! Engine
! 1
! 2
! 3
! 4
! 5
! 6
! 7
! 8
! Rank
! Points
|-
| [[2012 FIA World Endurance Championship season|2012]]
! [[Strakka Racing]]
! LMP1
! [[HPD ARX-03a]]
! [[Honda]] LM-V8 3.4 L V8
|style="background:#DFFFDF;"| [[2012 12 Hours of Sebring|SEB]]<br /><small>8</small>
|style="background:#DFFFDF;"| [[2012 6 Hours of Spa-Francorchamps|SPA]]<br /><small>6</small>
|style="background:#DFFFDF;"| [[2012 24 Hours of Le Mans|LMS]]<br /><small>22</small>
|style="background:#DFFFDF;"| [[2012 6 Hours of Silverstone|SIL]]<br /><small>5</small>
|style="background:#DFFFDF;"| [[2012 6 Hours of São Paulo|SÃO]]<br /><small>5</small>
|style="background:#FFDF9F;"| [[2012 6 Hours of Bahrain|BHR]]<br /><small>3</small>
|style="background:#DFFFDF;"| [[2012 6 Hours of Fuji|FUJ]]<br /><small>6</small>
|style="background:#DFFFDF;"| [[2012 6 Hours of Shanghai|SHA]]<br /><small>6</small>
! 7th
! 64
|-
| [[2013 FIA World Endurance Championship season|2013]]
! [[Strakka Racing]]
! LMP1
! [[HPD ARX-03c]]
! [[Honda]] LM-V8 3.4 L V8
|style="background:#EFCFFF;"| [[2013 6 Hours of Silverstone|SIL]]<br/><small>Ret</small>
|style="background:#DFFFDF;"| [[2013 6 Hours of Spa-Francorchamps|SPA]]<br/><small>7</small>
|style="background:#DFFFDF;"| [[2013 24 Hours of Le Mans|LMS]]<br/><small>6</small>
| [[2013 6 Hours of São Paulo|SÃO]]
| [[2013 6 Hours of Circuit of the Americas|COA]]
| [[2013 6 Hours of Fuji|FUJ]]
| [[2013 6 Hours of Shanghai|SHA]]
| [[2013 6 Hours of Bahrain|BHR]]
! 15th
! 22
|-
| [[2015 FIA World Endurance Championship season|2015]]
! [[Strakka Racing]]
! LMP2
! [[Strakka Racing|Strakka]] [[Dome (constructor)|Dome]] S103
! [[Nissan]] VK45DE 4.5 L V8
|style="background:#FFDF9F;"| [[2015 6 Hours of Silverstone|SIL]]<br /><small>3</small>
|style="background:#DFFFDF;"| [[2015 6 Hours of Spa-Francorchamps|SPA]]<br /><small>5</small>
| [[2015 24 Hours of Le Mans|LMS]]
| [[2015 6 Hours of Nürburgring|NÜR]]
| [[2015 6 Hours of Circuit of the Americas|COA]]
| [[2015 6 Hours of Fuji|FUJ]]
| [[2015 6 Hours of Shanghai|SHA]]
| [[2015 6 Hours of Bahrain|BHR]]
! 4th*
! 25*
|}
<nowiki>*</nowiki> Season still in progress.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.nickleventis.com}}
* {{DriverDB driver|nick-leventis}}

{{DEFAULTSORT:Leventis, Nick}}
[[Category:1980 births]]
[[Category:Living people]]
[[Category:Articles created via the Article Wizard]]
[[Category:English racing drivers]]
[[Category:European Le Mans Series drivers]]
[[Category:24 Hours of Le Mans drivers]]
[[Category:FIA World Endurance Championship drivers]]