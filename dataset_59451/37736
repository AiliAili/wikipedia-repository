{{Good article}}
{|{{Infobox ship begin |infobox caption= }}
{{Infobox ship image
|Ship image=[[File:Berk-i Satvet class cruiser.png|300px]]
|Ship caption=One of the two ''Peyk-i Şevket''-class cruisers
}}
{{Infobox ship career
|Hide header=
|Ship country=[[Ottoman Empire]]
|Ship flag={{shipboxflag|Ottoman Empire|naval-1845}}
|Ship name=
|Ship owner=
|Ship namesake=
|Ship ordered=18 January 1903
|Ship builder=[[Germaniawerft]], [[Kiel]]
|Ship laid down=February 1906
|Ship launched=1 December 1906
|Ship acquired=13 November 1907
|Ship commissioned=November 1907
|Ship decommissioned=
|Ship in service=
|Ship out of service=
|Ship struck=1944
|Ship reinstated=
|Ship honors=
|Ship fate=Broken up for scrap, 1953–55
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Peyk-i Şevket|cruiser|1}}
|Ship displacement={{convert|775|MT|abbr=on}}
|Ship length={{convert|80|m|abbr=on}}
|Ship beam={{convert|8.4|m|abbr=on}}
|Ship draft={{convert|2.5|m|abbr=on}}
|Ship propulsion=2-shaft [[triple-expansion engine]]s
|Ship speed={{convert|21|kn|abbr=on}}
|Ship range={{convert|3240|nmi|abbr=on}}
|Ship complement=105
|Ship armament=*2 × {{convert|105|mm|abbr=on}} guns
*6 × {{convert|57|mm|abbr=on}} guns
*2 × {{convert|37|mm|abbr=on}} guns
*3 × {{convert|450|mm|abbr=on}} [[torpedo tube]]s
}}
|}

'''''Berk-i Satvet''''' was a [[torpedo cruiser]] of the [[Ottoman Navy]], the second and final member of the {{sclass-|Peyk-i Şevket|cruiser|4}}. She was built by the [[Germaniawerft]] shipyard in Germany in 1906–07, and was delivered to the Ottoman Navy in November 1907. The ship's primary armament consisted of three {{convert|450|mm|abbr=on}} [[torpedo tube]]s and a pair of {{convert|105|mm|abbr=on}} guns, and she was capable of a top speed of {{convert|21|kn|lk=in}}. The ship's early career was uneventful; the [[Italo-Turkish War]] of 1911–12 passed without any action of the Ottoman fleet. ''Berk-i Satvet'' saw action during the [[Balkan Wars]] of 1912–13 in the [[Aegean Sea|Aegean]] and [[Black Sea]]s, against Greek and Bulgarian opponents, respectively.

After the Ottoman Empire entered [[World War I]], ''Berk-i Satvet'' was employed in patrols in the Black Sea. These included attacks on Russian ports with the ex-German warships [[SMS Goeben|''Yavuz Sultan Selim'']] and [[SMS Breslau|''Midilli'']]. In January 1915, ''Berk-i Satvet'' struck a [[naval mine]] while escorting a convoy to [[Zonguldak]]; the explosion severely damaged the ship and kept her out of service until April 1918. For the remainder of the war, she patrolled the Black Sea. The ship was renamed '''''Berk''''' in 1923 and modernized twice, in the mid-1920s and in the late-1930s. She remained in service until 1944, when she was stricken from the [[naval register]]. ''Berk'' was ultimately broken up for scrap in 1953–55.

==Design==
{{main|Peyk-i Şevket-class cruiser}}

''Berk-i Satvet'', classified as a [[torpedo cruiser]] by the [[Ottoman Navy]],<ref>''Fleets of the World'', p. 140</ref> was also sometimes referred to as a [[torpedo gunboat]]. She was {{convert|80|m|abbr=on}} long, with a [[beam (nautical)|beam]] of {{convert|8.4|m|abbr=on}} and a [[draft (hull)|draft]] of {{convert|2.5|m|abbr=on}}. She displaced {{convert|775|MT|abbr=on|sp=us}} while on [[sea trial]]s. The ship was powered by a pair of vertical [[triple-expansion engine]]s each driving a [[screw propeller]]. The engines were rated at {{convert|5100|ihp|lk=in}} for a top speed of {{convert|21|kn|lk=in}}; ''Berk-i Satvet'' had a cruising radius of {{convert|3240|nmi|lk=in}}. Her crew numbered 105&nbsp;officers and enlisted men.<ref name=GG392>Gardiner & Gray, p. 392</ref>

''Berk-i Satvet''{{'}}s primary offensive armament was her three {{convert|450|mm|abbr=on}} [[torpedo tube]]s. One was mounted in the bow, above water, and the other two were in deck-mounted swivel launchers [[amidships]]. She was armed with a pair of {{convert|105|mm|abbr=on}} guns that were placed in shielded single mounts on the [[forecastle]] and [[quarterdeck]]. She also carried six {{convert|57|mm|abbr=on}} guns, four of which were mounted in [[sponson]]s, and a pair of {{convert|37|mm|abbr=on}} guns. She had no armor protection.<ref name=GG392/>

==Service history==
[[File:Balkans at 1905.jpg|thumb|upright|Map of Ottoman holdings in the Balkans before the [[Balkan Wars]]]]
''Berk-i Satvet'' was ordered on 18 January 1903 and laid down in February 1906 at the [[Germaniawerft]] shipyard in [[Kiel]], Germany. She was launched on 1 December of that year, and completed in 1907. After completing sea trials, she was transferred to the Ottoman Navy, arriving in Constantinople on 13 November, where she was formally commissioned into the Ottoman fleet.<ref name=LG149>Langensiepen & Güleryüz, p. 149</ref> In 1909, she and her sister participated in the first fleet maneuver conducted by the Ottoman Navy in twenty years.<ref>Langensiepen & Güleryüz, p. 14</ref> During the [[Italo-Turkish War]] of 1911–12, ''Berk-i Satvet'' was assigned to the Reserve Division, which was centered on the elderly [[ironclad warship|ironclads]] {{ship|Ottoman ironclad|Mesudiye||2}} and {{ship|Ottoman ironclad|Âsâr-ı Tevfik||2}}. She did not see action during the conflict, since the Ottoman fleet spent the war in port.<ref>Beehler, p. 12</ref>

===Balkan Wars===
''Berk-i Satvet'' was transferred to the [[Black Sea]] on 9 December 1912, to reinforce the squadron there during the [[First Balkan War]]. On 4&nbsp;February 1913, ''Berk-i Satvet'' bombarded Bulgarian positions at [[Şarköy]] on the northern coast of the [[Sea of Marmara]] in preparation for an amphibious assault.<ref name=LG25>Langensiepen & Güleryüz, p. 25</ref> Four days later, the Ottoman navy returned to support the landing at Şarköy. ''Turgut Reis'' and ''Barbaros Hayreddin'', along with two small cruisers provided artillery support to the right flank of the invading force once it went ashore. The ships were positioned about one kilometer off shore, with ''Berk-i Satvet'' leading the line, which also included the cruiser {{ship|Ottoman cruiser|Mecidiye||2}} and the [[pre-dreadnought battleship]]s {{ship|Ottoman battleship|Barbaros Hayreddin||2}} and {{ship|Ottoman battleship|Turgut Reis||2}}. The Bulgarian army resisted fiercely, which ultimately forced the Ottoman army to retreat, though the withdrawal was successful in large part due to the gunfire support from the fleet. ''Berk-i Satvet'' and ''Mecidiye'' covered the left flank while the two battleships supported the left during the evacuation. In the course of the operation, ''Berk-i Satvet'' had fired eighty-four 105&nbsp;mm shells.<ref>Erickson, pp. 266–270</ref>

On 9 March, ''Berk-i Satvet'' joined a sweep toward [[Imbros]], an island in the [[Aegean Sea]] at the entrance to the [[Gulf of Saros]]; she briefly engaged a pair of Greek destroyers and stopped a steamer flying under the French flag. The vessel, which appeared to be supplying Bulgarian forces, was taken as a prize by the destroyer {{ship|Ottoman destroyer|Yahisar||2}}.<ref name=LG24>Langensiepen & Güleryüz, p. 24</ref> Later in March, she again escorted ''Barbaros Hayreddin'' and ''Turgut Reis'' in the Black Sea, while the two battleships bombarded Bulgarian troops that were attempting to breach the line of defenses at [[Çatalca]].<ref>Erickson, p. 288</ref> On 13 April, ''Berk-i Satvet'' joined a fleet consisting of ''Barbaros Hayreddin'', ''Turgut Reis'', ''Âsâr-ı Tevfik'', and several smaller warships. The ships sortied out of the [[Dardanelles]] and encountered a Greek fleet. After a brief engagement at extreme range, the Ottomans and Greeks withdrew to the Dardanelles and Imbros, respectively.<ref name=LG24/>

===World War I===
[[File:Ottoman Fleet 1914.png|thumb|Silhouettes of the major warships of the Ottoman Navy in 1914; the ''Peyk-i Şevket''-class is the fourth ship in the second row]]
In late July 1914, [[World War I]] broke out in Europe, though the Ottomans initially remained neutral.<ref>Langensiepen & Güleryüz, p. 27</ref> On 14 August, ''Berk-i Satvet'' joined patrols of the Dardanelles, the defenses of which were strengthened with several new minefields.<ref>Langensiepen & Güleryüz, p. 29</ref> Tensions between the Ottomans and a British fleet patrolling the entrance to the Dardanelles increased until 5 November, when Britain and France declared war on the Ottoman Empire.<ref>Langensiepen & Güleryüz, p. 30</ref> In the meantime, ''Berk-i Satvet'' had been transferred to the Black Sea. She joined the cruiser [[SMS Breslau|''Midilli'']], formerly the German ''Breslau'', [[Black Sea Raid|for an attack]] on the Russian port of [[Novorossiysk]] on 29 October. ''Berk-i Satvet'' embarked on another attack, this time with the [[battlecruiser]] [[SMS Goeben|''Yavuz Sultan Selim'']], formerly the German ''Goeben''. The battlecruiser shelled [[Sevastopol]] while ''Berk-i Satvet'' observed; she had been sent with ''Yavuz Sultan Selim'' primarily to train her crew.<ref>Langensiepen & Güleryüz, p. 45</ref>

''Berk-i Satvet'' sortied with her sister and ''Yavuz Sultan Selim'' on 5 December to provide distant support to a troop convoy headed to [[Rize]]. On 2 January 1915 at 15:00, she, ''Midilli'', and the cruiser {{ship|Ottoman cruiser|Hamidiye||2}} steamed out of the [[Bosporus]] to escort a transport to [[Zonguldak]], after which the three cruisers are to conduct a patrol off the port. At 18:00 into the voyage, a Russian mine exploded, which led ''Berk-i Satvet''{{'}}s commander to take evasive action. The ship struck a mine in the darkness, which caused significant damage. The mine destroyed both of her propellers and caused serious flooding at her stern. Two [[tug boat|tugs]] arrived and towed ''Berk-i Satvet'' to [[İstinye]] with ''Hamidiye'' as an escort.<ref>Langensiepen & Güleryüz, p. 46</ref> The damage was so severe that the ship was disabled for most of the war.<ref>Halpern, p. 228</ref> After lengthy repairs, the ship was recommissioned on 1 April 1918 and patrols between Constantinople and [[Batumi]]. She remained in the Black Sea through the end of the war. On 30 November, the Ottoman Empire signed the [[Armistice of Mudros]] with the [[Triple Entente|Entente powers]], which concluded the conflict.<ref>Langensiepen & Güleryüz, p. 54</ref>

===Later career===
The ship was renamed ''Berk'' in 1923 following the end of the [[Turkish War of Independence]], which saw the [[Republic of Turkey]] replace the old [[Ottoman Empire]].<ref name=GG392/> At the time, the ship had been placed out of service.<ref>Langensiepen & Güleryüz, p. 59</ref> From 1924 to 1925, she was modernized at the [[Gölcük Naval Shipyard]] and was recommissioned in 1925.<ref name=LG149/> In 1927, the ship visited [[İzmir]].<ref>Langensiepen & Güleryüz, p. 90</ref> She was rebuilt in 1937–39 and incorporated substantial improvements. Her [[Stem (ship)|stem]] was replaced and her [[superstructure]] was rebuilt. The old gun armament was replaced with a pair of {{convert|88|mm|abbr=on}} 45-[[Caliber (artillery)|caliber]] guns and four 37&nbsp;mm 40-caliber guns, and equipment to handle 25 mines was installed.<ref name=GG392/> The ship continued in service until 1944, when she was stricken from the [[naval register]]. She was [[hulk (ship)|hulked]] at the Gölcük shipyard in 1950,<ref name=GG392/> and broken up for scrap between 1953 and 1955.<ref name=LG149/>

==Notes==
{{Reflist|20em}}

==References==
*{{Cite book |last=Beehler|first=William Henry|title=The History of the Italian-Turkish War: September 29, 1911, to October 18, 1912|year=1913|location=Annapolis|publisher=United States Naval Institute|url=https://books.google.com/books?id=OWcoAAAAYAAJ&printsec=frontcover#v=onepage&q&f=false}}
*{{Cite book |last=Erickson|first=Edward J.|title=Defeat in detail: the Ottoman Army in the Balkans, 1912–1913|year=2003|location=|publisher=Greenwood Publishing Group|isbn=9780275978884}}
*{{cite book|title=Fleets of the World|year=1915|location=Philadelphia|publisher=J. B. Lippincott Company|oclc=8418713}}
*{{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, MD|publisher=Naval Institute Press|isbn=0-85177-245-5}}
*{{cite book|last=Halpern|first=Paul G.|title=A Naval History of World War I|publisher=Naval Institute Press|location=Annapolis, Maryland|date=1994|isbn=1-55750-352-4}}
*{{cite book|last1=Langensiepen|first1=Bernd|last2=Güleryüz|first2=Ahmet|year=1995|title=The Ottoman Steam Navy 1828–1923|publisher=Conway Maritime Press|location=London|isbn=978-0-85177-610-1|lastauthoramp=y}}

{{Berk-i Satvet class cruiser}}

{{use dmy dates|date=February 2015}}

{{DEFAULTSORT:Berk-i Satvet}}
[[Category:Cruisers of the Ottoman Navy]]
[[Category:Ships built in Kiel]]
[[Category:1906 ships]]
[[Category:Peyk-i Şevket-class cruiser]]