{{Infobox journal
| title = Journal of Psychiatry & Neuroscience
| cover = [[File:JPsyNeuro logo.jpg|225px]]
| editor = Patricia Boksa, Ridha Joober
| discipline = [[Psychiatry]], [[neuroscience]]
| former_names = Psychiatric Journal of the University of Ottawa
| abbreviation = J. Psychiatry Neurosci.
| publisher = [[Canadian Medical Association]]
| country = Canada
| frequency = Bimonthly
| history = 1976-present
| openaccess = Yes
| license =
| impact = 5.861
| impact-year = 2014
| website = http://www.cma.ca/publications/jpn
| link1 =
| link1-name =
| link2 = http://www.cma.ca/publications/jpn/issuesbydate
| link2-name = Online archive
| JSTOR =
| OCLC = 476993381
| LCCN = 91641773
| CODEN = JPNEEF
| ISSN = 1180-4882
| eISSN = 1488-2434
| ISSN2 = 0702-8466
| ISSN2label = ''Psychiatric Journal of the University of Ottawa'':
}}
The '''''Journal of Psychiatry & Neuroscience''''' is a bimonthly [[open access]] [[Peer review|peer-reviewed]] [[scientific journal]] covering research in [[psychiatry]] and [[neuroscience]] concerning the mechanisms involved in the [[etiology]] and treatment of [[psychiatric disorder]]s. The journal was established in 1976 as the ''Psychiatric Journal of the University of Ottawa'' and obtained its current title in 1991.<ref name=PubMed>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9107859 |title=Journal of Psychiatry & Neuroscience |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2013-10-27}}</ref> It is published by the [[Canadian Medical Association]] and the [[editors-in-chief]] are Patricia Boksa and Ridha Joober ([[McGill University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Index Medicus]]/[[MEDLINE]]/[[PubMed]],<ref name=PubMed /> [[Science Citation Index]], [[Social Sciences Citation Index]], [[Current Contents]]/Social & Behavioral Sciences, and [[BIOSIS Previews]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-10-27}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.861.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Psychiatry & Neuroscience |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.cma.ca/publications/jpn}}

{{DEFAULTSORT:Journal of Psychiatry and Neuroscience}}
[[Category:Neuroscience journals]]
[[Category:Psychiatry journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1976]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]


{{psychiatry-journal-stub}}
{{neuroscience-journal-stub}}