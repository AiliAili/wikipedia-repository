{{other people|Charles Metcalfe}}
{{Infobox military person
|name=Major General Charles D. Metcalf
|birth_date={{Birth year and age|1933}}
|death_date=
|birth_place= Amamosa, [[Iowa]]
|death_place=
|placeofburial=
|placeofburial_label= Place of burial
|image= Charles Metcalf 01.jpg
|caption=Charles Metcalf, former Director of the National Museum of the United States Air Force
|nickname=
|allegiance={{Flagicon|United States}} [[United States|United States of America]]
|branch= [[File:Flag of the United States Air Force.svg|25px]][[United States Air Force]]
|serviceyears=1955-1991
|rank=[[Major general (United States)|Major General]]
|commands=Air Force Accounting and Finance Center
|unit=
|battles=
|awards=[[Air Force Distinguished Service Medal|Distinguished Service Medal]]<br/>[[Defense Superior Service Medal]]<br/>[[Legion of Merit]]<br/>[[Meritorious Service Medal (United States)|Meritorious Service Medal]]<br/>[[Air Force Commendation Medal]]
|relations=
|laterwork=
}}
'''Charles David Metcalf''' (born 1933) served in the [[United States Air Force]] for 36 years, retiring as a [[Major general (United States)|Major General]].  He went on to serve as the Director of the [[National Museum of the United States Air Force]] from 1996 to 2010.  He is a well known civic leader in the [[Dayton, Ohio]] area.

==Early life==
Metcalf was born in [[Anamosa]], [[Iowa]] in 1933.  He graduated from Anamosa High School in 1951. He then attended [[Coe College]] in [[Cedar Rapids]], Iowa.  He graduated in 1955, receiving a [[Bachelor of Arts]] degree economics and accounting.  He was also a distinguished graduate of Coe College’s [[Air Force Reserve Officer Training Corps]] program.<ref name="MIL">United States Air Force military biography, [http://www.af.mil/bios/bio.asp?bioID=6454 Major General Charles D. Metcalf] {{webarchive |url=https://web.archive.org/web/20071214075158/http://www.af.mil/bios/bio.asp?bioID=6454 |date=December 14, 2007 }}, U.S. Air Force Public Affairs Office, Washington, DC, November 1989.</ref>

==Military career==
He entered the Air Force in October 1955 and was assigned to the Air Force Hospital at Wimpole Park, [[England]], as accounting and finance officer.  In 1958, he became the base accounting and finance officer at [[Donaldson Air Force Base]], [[South Carolina]].  He was transferred to [[George Air Force Base]], [[California]] in May 1959, again as a base level accounting and finance officer.  He completed [[Squadron Officer School]] in 1962.<ref name ="MIL"/>

In September 1962, Metcalf became the first Air Force accounting and finance officer assigned in the [[South Vietnam|Republic of Vietnam]].  He activated that country’s first military finance office at [[Tan Son Nhut Air Base]].  After his Vietnam tour, Metcalf was selected to attend graduate school at [[Michigan State University]] at [[East Lansing]], receiving a [[master's degree]] in business administration in 1964.<ref name ="MIL"/>

Metcalf was assigned to Headquarters [[Air Defense Command]] at [[Ent Air Force Base]], [[Colorado]] in September 1964, where he served as chief of the Budget Operations Division. In August 1967, he was assigned to the Office of Aerospace Programs in [[The Pentagon]] in [[Washington, D.C.]].  While at the Pentagon, he also served as executive officer to the Assistant Secretary of the Air Force for Financial Management.  He then returned to Aerospace Defense Command headquarters and served for two years as deputy director of budget.<ref name ="MIL"/>

In 1973, Metcalf was selected to attend [[Air War College]] at [[Maxwell Air Force Base]], [[Alabama]].  He graduated in June 1974, and was assigned to [[Scott Air Force Base]], [[Illinois]] as director of budget for the [[Military Airlift Command]]. He returned to Pentagon in August 1975 and served as deputy chief of the Air Force Budget Directorate’s Operations Appropriation Division. In July 1976, he returned to Scott Air Force Base as Military Airlift Command’s comptroller. In June 1982, he was assigned to the Office of the Comptroller of the Air Force in Washington, DC as deputy director of budget where he was promoted to [[Brigadier general (United States)|brigadier general]].<ref name ="MIL"/>

In July 1983, he became comptroller for [[Air Force Logistics Command]] at [[Wright-Patterson Air Force Base]], Ohio. During his tenure as the Air Force Logistic Command comptroller, he attended [[Harvard University]]’s Executive Development Program in [[Cambridge, Massachusetts]].  In August 1986, he was promoted to [[Major general (United States)|major general]].<ref name ="MIL"/>

In 1988, General Metcalf took command of the Air Force Accounting and Finance Center (now part of the [[Defense Finance and Accounting Service]]).  At that time the center was located at [[Lowry Air Force Base]] in [[Denver]], Colorado.  As commander of the finance center, he was responsible for the Air Force’s worldwide accounting and finance network, paying all Air Force active duty, [[Air National Guard]], [[Air Force Reserve]], and retired members, and accounting for all funds appropriated to the Air Force by the [[United States Congress]].  During this period, Metcalf also served as Deputy Assistant Secretary of the Air Force for Accounting and Deputy Director of the [[Defense Security Cooperation Agency|Defense Security Assistance Agency]].  He filled these three roles concurrently until he retired from the Air Force on 1 February 1991.<ref name ="MIL"/>

General Metcalf’s military decorations include the [[Air Force Distinguished Service Medal|Distinguished Service Medal]] with one oak leaf cluster, the [[Defense Superior Service Medal]], the [[Legion of Merit]], [[Meritorious Service Medal (United States)|Meritorious Service Medal]] with three oak leaf clusters, and [[Air Force Commendation Medal]].<ref name ="MIL"/><ref Name="CIV">United States Air Force Senior Executive Service biography, [http://www.af.mil/bios/bio.asp?bioID=8182 Charles D. Metcalf] {{webarchive |url=https://web.archive.org/web/20071214074631/http://www.af.mil/bios/bio.asp?bioID=8182 |date=December 14, 2007 }}, U.S. Air Force Public Affairs Office, Washington, DC, January 2006.</ref>

==Air Force Museum==
In 1996, Metcalf became Director of the National Museum of the United States Air Force, commonly called “the Air Force Museum.”  The museum is located on Wright-Patterson Air Force Base, and is the world’s largest and oldest military aviation museum.<ref name="CIV"/>

[[File:Gen Metcalf 02.jpg|right|thumb|250px|Charles Metcalf accepting the C-141 "Hanoi Taxi" into museum collection after its last flight]] As museum director, Metcalf oversees 96 permanent federal [[civil service]] employees and 475 volunteers who design and maintain exhibits, conserve collection [[Cultural artifact|artifacts]], conduct research, restore vintage aircraft,<ref>Gaffney, Timothy R., [http://archive.is/20070815084432/http://www.conservababes.com/forums/lofiversion/index.php/t16587.html "Rebuilding History: Memphis Belle to be restored"], ''Dayton Daily News'', 15 October 2005.</ref><ref>[http://www.iht.com/articles/ap/2006/12/01/america/NA_GEN_US_Iraqi_Plane.php "MiG fighter uncovered in Iraqi desert being restored by U.S. Air Force],
''Associated Press'', 1 December 2006</ref> educate the public about [[aviation history]], plan special events,<ref>[http://www.centennialofflight.gov/user/news_releases/press_stamp.htm Wright Brothers' First Flight To Be Honored on U.S. Postage Stamp"] {{webarchive |url=https://web.archive.org/web/20071011191459/http://www.centennialofflight.gov/user/news_releases/press_stamp.htm |date=October 11, 2007 }}, U.S. Centennial of Flight Commission, Washington, DC, 24 April 2003.</ref> and work in the museum’s administrative offices. He is responsible for a building complex with {{convert|1700000|sqft|m2}} of public exhibit space (17 acres of indoor space).  The museum collection has more than 400 aircraft and other large aerospace vehicles plus thousands of aviation artifacts on display.<ref name="CIV"/>  In addition to the main museum building, a large aircraft hangar annex houses retired [[Air Force One]] Presidential aircraft<ref>Curnutte, Mark, [http://www.enquirer.com/editions/1998/05/21/plane21.html "Airliner known as 26000 flies into history Air Force Museum will be new home"], ''The Cincinnati Enquirer'', 21 May 1998.</ref> along with [[X-plane (aircraft)|X-type test vehicles]]. The museum also has an aviation art gallery,<ref>Asher, Gerry, [http://www.asaa-avart.org/vAerobrushWinter2004.html "Notes from Lead"], ''Aero Brush'', American Society of Aviation Artists, Winter 2004.</ref> a six-story [[IMAX|IMAX theatre]] that seat 500 viewers, and a full-motion simulator that gives the sensation of flying a [[Fighter aircraft|jet fighter]]. Outside on the museum grounds, there is an air park with numerous large aircraft along with a [[World War II]] control tower.<ref Name="AFM">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=429 National Museum of the United States Air Force Fact Sheet] {{webarchive |url=https://web.archive.org/web/20071116170439/http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=429 |date=November 16, 2007 }}, National Museum of the U.S. Air Force, Public Affairs Division, Wright-Patterson Air Force Base, Ohio, Revised June 2007.</ref> In addition, Metcalf oversees an inventory of more than 6,000 aerospace vehicles and historically valuable artifacts that are on loan to 450 civilian museums, cities, and veteran organizations throughout the [[United States]].<ref name="CIV"/>

Every year, Metcalf and his staff host approximately 800 events that bring government dignitaries, military leaders, [http://www.defenselink.mil/home/photoessays/2006-04/p20060419b03.html war heroes], corporate executive, military reunion groups, and aviation enthusiasts to the museum.  These events include the biennial Dawn Patrol Rendezvous [[World War I]] fly-in, the annual [[Radio control|Radio-Controlled Model Aircraft]] [[Air Show]], outdoor and indoor concerts featuring the Air Force Band of Flight, and the Wings and Things guest lecture series.  His education program reaches over 100,000 students every year.<ref name="AFM"/> Metcalf also makes the museum available for many formal military ceremonies.

Metcalf has greatly expanded the museum and its collection during his tenure. The {{convert|200000|sqft|m2|sing=on}} Cold War Gallery was completed in 2003.  Its centerpiece is a [[B-2 bomber]].<ref name="AFP">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=433 Museum Expansion Projects Fact Sheet] {{webarchive |url=https://web.archive.org/web/20071116170513/http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=433 |date=November 16, 2007 }}, National Museum of the U.S. Air Force, Public Affairs Division, Wright-Patterson Air Force Base, Ohio, Revised June 2007.</ref> The latest addition to the museum is a $3.2 million Missile and Space Gallery which was opened in 2004.<ref>Wilfong, John, [http://dayton.bizjournals.com/dayton/stories/2004/06/14/story7.html "Museum's $3.2M silo to attract more visitors"], ''Dayton Business Journal'', 11 June 2004.</ref> Currently the museum is planning to build a {{convert|53000|sqft|m2|sing=on}} addition to house a Space Gallery.

In 2005, Metcalf was promoted to the [[Senior Executive Service]], the highest federal civilian rank below Presidential appointees.<ref Name="CIV"/>

As director of the Air Force Museum, Metcalf plays a leading role in overseeing the United States Air Force Heritage Program, which includes 12 field museums and 260 domestic and international heritage sites. This program manages approximately 31,000 military aviation items that are displayed at Air Force bases around the world.<ref Name="CIV"/>

General Metcalf retired from civilian service on 16 December 2010 and was replaced by Lt. Gen. (ret) [[John L Hudson]].<ref name="NMUSAFNewDir2010">{{cite web|last=Swan |first=Sarah |title=New director takes the lead at National Museum of the U.S. Air Force |url=http://www.nationalmuseum.af.mil/news/story.asp?id=123235357 |publisher=[[National Museum of the United States Air Force]] |accessdate=23 December 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20101225153057/http://www.nationalmuseum.af.mil/news/story.asp?id=123235357 |archivedate=25 December 2010 |df= }}</ref>

==Community leader==
[[File:Charles Metcalf DESA speech-closeup.jpg|right|thumb|150px|Charles Metcalf accepting his Distinguished Eagle Scout Award]]
As a youth Metcalf earned the [[Boy Scouts of America]]'s (BSA) [[Eagle Scout (Boy Scouts of America)|Eagle Scout]] rank on August 19, 1949. He continued his involvement the BSA as a member of the BSA’s Leadership and Standards Committee and was recognized on October 27, 2009 with BSA's [[Distinguished Eagle Scout Award]].<ref name="desametcalf">{{cite web|url=http://www.nationalmuseum.af.mil/news/story.asp?id=123174191 |title=Museum director receives Distinguished Eagle Scout Award |last=Swan |first=Sarah |date=October 28, 2009 |publisher=National Museum of the U.S. Air Force |accessdate=2010-01-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20121014122730/http://www.nationalmuseum.af.mil/news/story.asp?id=123174191 |archivedate=2012-10-14 |df= }}</ref> In addition to his work at the Air Force museum, General Metcalf is the Central Region Vice President of the [[Boy Scouts of America]]. He is a former member of the [[Oakwood, Montgomery County, Ohio|Oakwood, Ohio]] City Council, and served on the Corporate Development Board of Trustees for Dayton, Ohio.  He has served as chairman of the Board of Directors for the Greater Dayton [[United Way of America|United Way]] campaign, and as a member of the Board of Directors for Greater Dayton [[Public Television]].  He has also served as a member of Michigan State University’s National Alumni Board.<ref Name="CIV"/>

==References==
{{Reflist|2}}

==External links==
{{Portal|Biography|United States Air Force}}
*[http://www.nationalmuseum.af.mil/ National Museum of the U.S. Air Force]
*[http://www.afmuseum.com/ Air Force Museum Foundation]
*[http://www.wpafb.af.mil/ Wright-Patterson Air Force Base]

{{DEFAULTSORT:Metcalf, Charles D.}}
[[Category:1933 births]]
[[Category:Living people]]
[[Category:Distinguished Eagle Scouts]]
[[Category:People from Anamosa, Iowa]]
[[Category:Military personnel from Dayton, Ohio]]
[[Category:Michigan State University alumni]]
[[Category:Harvard University alumni]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Legion of Merit]]
[[Category:United States Air Force generals]]
[[Category:Wright-Patterson Air Force Base]]
[[Category:Recipients of the Defense Superior Service Medal]]