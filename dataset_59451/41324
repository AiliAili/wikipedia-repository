{{Infobox NRHP | name =Calhoun School Principal's House
  | nrhp_type =
  | image = Calhoun Colored School Principal's House 2012.JPG
  | caption = The principal's house at Calhoun Colored School.
  | location= CR 33, [[Calhoun, Alabama]]
  | coordinates = {{coord|32|3|28|N|86|32|58|W|region:US-AL_type:edu|display=inline,title}}
| locmapin = Alabama#USA
  | area =
  | architecture= Victorian
  | added = March 26, 1976
  | refnum=76000340<ref name="nris">{{NRISref|2008a}}</ref>
}}

The '''Calhoun Colored School''' (1892–1945) was a private boarding and day school in Calhoun, [[Lowndes County, Alabama]], about {{convert|28|mi|km}} southwest of the capital of Montgomery.<ref>{{cite web | url=http://www.digitalhistory.uh.edu/exhibits/calhoun/thorn.html | title=The Calhoun School, Miss Charlotte Thorn's "Lighthouse on the Hill" in Lowndes County, Alabama. | work=The Alabama Review | date=1984 | accessdate=6 August 2015 | author=Ellis, R.H.}}</ref> Founded in 1892 by Miss [[Charlotte Thorn]] and Miss [[Mabel Dillingham]] in partnership with [[Booker T. Washington]] of [[Tuskegee Institute]], to provide education to rural black students, who comprised the majority in this area, the Calhoun Colored School was first designed to educate rural [[African American|African-American]] students according to the industrial school model common at the time.<ref>{{cite news |url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9400EED7153EE333A25752C2A9679C94679ED7CF&oref=slogin |accessdate=2007-10-16 |date=1896-01-21 |format=PDF |publisher=''[[The New York Times]]'' |title=IN AID OF COLORED MEN |page=9}}</ref>

In addition, the school sponsored a land bank that helped 85 families buy land. It created a joint venture with the county to improve a local road so farmers could get their products to market. As the school developed, it raised its standards, created a large library, and offered more of an academic curriculum.

The principal's house, the only surviving original building, has been listed on the [[National Register of Historic Places]] in recognition of the school's importance in the history of education of African Americans.

==History==
In 1891, the [[United States]] was still adjusting to the aftermath of the [[American Civil War]],  [[Reconstruction era of the United States|Reconstruction]], and the [[Financial Panic of 1873]].  Many African Americans living in the [[rural]] [[Southern United States|South]] worked under the [[sharecropping system]].  The dependence of southern agriculture on [[cotton]], whose price continued to drop, contributed to difficulties in the South making economic progress.  African Americans, then called "colored" or "[[Negro]]", living in Calhoun ([[Lowndes County, Alabama|Lowndes County]]), Alabama were subject to white political and social domination although they comprised the majority of the county's population.

Conservative white Democrats had regained power in the state legislature and begun to pass statutes that stripped African Americans from voter rolls or made elections so complicated they were effectively [[Disfranchisement after Reconstruction era|disenfranchised]]. In 1901 the state passed a new constitution with provisions that created barriers to voter registration. These suppressed voting by most blacks, as well as tens of thousands of poor whites.<ref>Glenn Feldman, ''The Disenfranchisement Myth: Poor Whites and Suffrage Restriction in Alabama,'' Athens: University of Georgia Press, 2004, pp. 135–136</ref>

Lowndes County, in the [[Black Belt (region of Alabama)|Black Belt]], had been an area of large cotton plantations before the Civil War. Devoted to agriculture, in 1890 the county had the highest proportion of Negroes to whites of any in Alabama. Most blacks worked as sharecroppers cultivating cotton.

[[Booker T. Washington]], then president of [[Tuskegee Institute]], spoke at Hampton Institute (his alma mater) to recruit teachers to help with education of blacks in Alabama. He told of the people of Calhoun and their great desire to educate their children. Two Hampton teachers, Charlotte Thorn and Mabel Dillingham, white women from [[New England]], responded to his plea for help. They traveled with Washington to Calhoun to find a site and get a school built and operating.<ref name="Alabama Review">{{cite web |title=The Calhoun School, Miss Charlotte Thorn's "Lighthouse on the Hill" in Lowndes County, Alabama |publisher=''The Alabama Review'' |last=Ellis |first=R. H |accessdate=2007-10-16 |url=http://www.digitalhistory.uh.edu/exhibits/calhoun/thorn.html |year=1984}}</ref>

Thorn and Dillingham used their extensive networks among families and friends to raise funds and receive donations of all kinds. They also used the Hampton Institute publication, ''[[The Southern Workman]],'' to publicize frequent articles about the school and aid fundraising. Thorn's nephew, [[Sidney Dickinson]], spent time with his parents during his youth assisting at the school.<ref name="Greenville">{{cite web|url=http://www.gcma.org/pages/see/exhibitions/default/0/41|title=Greenville County Museum of Art|work=Greenville County Museum of Art|accessdate=3 May 2015}}</ref>

==Hampton-Tuskegee model==
[[File:CalhounCarpentry.jpg|left|thumb|200px|A [[carpentry]] class at the Calhoun School.]]
[[File:CalhounCookingClass.png|thumb|200px|left|A cooking class at the Calhoun School.]]
Washington, Thorn and Dillingham developed the Calhoun Colored School according to the [[Hampton University|Hampton]]-[[Tuskegee Institute|Tuskegee]] model. First, students would receive a basic [[elementary education]] and then an industrial education at the high school level. This model envisioned preparing students for the work available in the rural areas in which most of them lived. Boys would become farmers and perhaps skilled tradesmen, and girls would become wives and homemakers, as well as [[laundress]]es, [[dressmaker]]s, or [[domestic worker]]s. The best students were encouraged to become teachers and work in the community or outlying areas to promote further education along the Hampton-Tuskegee model. There was a great push to continue to improve literacy among both children and adults, and teaching was a high calling.

Second, the school was to be apolitical. The education was not designed to encourage African Americans to challenge the ''status quo.'' African Americans were offered a basic education that enabled them to return to their communities and support themselves within the system. Washington feared that if a colored school challenged the politics of the day, white citizens might refuse to allow it to open or would later shut it down. This was not the [[Classical education movement|classical]] type of education offered to many white students in the north. Highly educated African Americans such as [[W. E. B. Du Bois]] thought the Tuskegee model was too limited and did not support it. Born in Massachusetts and educated in the North, including a doctorate at Harvard University, he argued for ensuring that the most talented students could get a full academic education, to advance the race.

In October 1892 the co-principals Thorn and Dillingham met with 300 blacks from the area who wanted to learn more about their plans to start a school. Many of the adults who came to the first meeting would work to build the teachers' cottages, schoolhouses, barn, shop and dormitories that by 1896 comprised the full campus. N.J. Bell of Montgomery donated the initial {{convert|10|acre|m2}} of land for the site of the school. By 1896 the school had a working farm of {{convert|100|acre|km2}}; 300 pupils, of whom 40 were boarders; and 13 teachers.<ref name="Ellis">Ellis, R. H.(1984) "The Calhoun School, Miss Charlotte Thorn's "Lighthouse on the Hill" in Lowndes County, Alabama", ''The Alabama Review'', 37(3), pp. 183–201. Retrieved 2007-11-8.</ref><ref name="The New York Times p. 9">"IN AID OF COLORED MEN", ''The New York Times'', 1896-01-21, p. 9. Retrieved on 2007-11-8.</ref>

The Hampton-Tuskegee model was based on educating African Americans to build their lives from basic skills. Critics thought that it conformed to white expectations of low aspirations for blacks in the South during this period. But it also related to the needs of the chiefly rural economy of Alabama, especially in the Black Belt. It did not emphasize human agency or empowerment for change, and most white citizens and some blacks would not have supported such an idea then.<ref name="Ellis"/>

==Fundraising, land bank, and road building==
Mabel Dillingham died of [[yellow fever]] in 1895. Her brother Pitt Dillingham, a minister, worked with Charlotte Thorn as co-principal for several years after Mabel's death. He also helped by public speaking and giving lectures in the North to support fundraising.<ref name="The New York Times p. 9"/>

The founders, their community, and board had two major ideas that went beyond the Hampton-Tuskegee model. Knowing that land ownership was as or more critical than education to enable blacks to be self-supporting, in 1894 the school organized a land company. With a land bank containing more than {{convert|4000|acre|km2}}, they sold land in 40- to {{convert|60|acre|m2|sing=on}} tracts, with financing arranged by Northern friends of the school. In the first 13 years, the school issued 92 deeds for land to 85 people. The new landowners could build real houses on their land. Their three- to eight-room houses were each better than the sharecroppers' cabins which they had occupied before.<ref name="Ellis"/>

Although Calhoun was near a railway, freight charges were too high for small farmers. Calhoun had been isolated by poor dirt roads that turned to slick clay in rain. The school tried to get the roads improved to enable farmers to get their goods to market. It took nearly 40 years, but Thorn Dickinson, Miss Thorn's nephew, was able to arrange a joint venture with the county.  Using skills earned at [[Williams College]] and [[Massachusetts Institute of Technology]] (MIT), Dickinson laid out the road, the school students graded it, and the county surfaced with gravel what became Lowndes County Route 33.<ref name="Ellis"/>

==Literacy program==
[[File:Students calhoun.jpg|thumb|200px|right|Students in military formation.]]
Initially the school’s [[literacy]] program included oral reading, elocution, rote memory, literature appreciation, and home, school, and community connections.  Additionally, outside materials were brought in to appeal to student interests, extensive discussions about words were held, poems were memorized, students were given help with articulation and expression, and students were taught to write their thoughts and spell correctly.  "Connecting school and home" and the emphasis on composition were in addition to the Hampton-Tuskegee model.

Later the literacy program included night classes for adults.  These non-graded classes were part of the Hampton-Tuskegee model.  The missionary committee led community outreach.  Students and teachers went into the homes of freedmen to teach them to read and write, skills they eagerly worked for. Outreach included mothers’ meetings, Sunday afternoon church services, and holiday community celebrations on campus.

Donations of money and books, mostly by Northern supporters, created the library at the school. While the co-principals officially adhered closely to the two-pronged Hampton-Tuskegee model, their literacy practice revealed a much richer model of teaching.

The Calhoun Colored School (CCS) began as a strict follower of the Hampton-Tuskegee model, but the school eventually developed a classical education.  This emphasized thinking and problem solving. It included a multifaceted literacy program.  When CCS hired college-trained Academic Department heads, they began to use teaching methods and materials that followed national trends.  As new teachers from outside CCS came to dominate the teaching force, they created a literacy program that resembled those at some of the better northern schools.

Despite good teaching methods, solid curriculum and quality materials, the students of CCS did not make the desired progress in literacy.  While these students were living in poverty, they were interested in education and had family support.  The social attitudes of that era may have limited the thinking of the faculty and staff.  "…it hindered their ability to envision African Americans as users of literacy; that is, while minimal access to literacy was made available, opportunities to use literacy in meaningful ways were delimited."<ref name="Willis"/>

==Academic department heads==
[[File:CalhounPrincipalsHouse.jpg|thumb|200px|right|The principal's cottage, c. 1900]]
[[File:Staff calhoun.jpg|thumb|200px|right|The staff of the Calhoun Colored School, c. 1900]]
From 1892–1945 there were six different Academic Department heads. Each brought improvements to the curriculum of the school.  The first was Susan Showers (1896), who initiated literacy societies for students, home visits by teachers, single grade assignments for teachers, community socials, holiday celebrations, and evening religious services. (Partnering with the community was not part of the Hampton-Tuskegee model).

Clara Hart (1898) succeeded Susan Showers.  She began the first free kindergarten at CCS, put reading instruction in every grade level, ensured the use of good literature for all the children, and furthered composition work.

Mabel Edna Brown (1907) was the first African American to work as Academic Department head. Her focus was to offer a more classical education, which was a major departure from the Hampton-Tuskegee model. She declared, "In all grades we are trying to raise the standards of thinking, accuracy, quickness, articulation, and correct expression."<ref name="Willis">{{cite journal |last=Willis |first=Arlette Ingram |year=2002 |title=Literacy at Calhoun Colored School 1892–1945 |journal=Reading Research Quarterly |issue=37 |pages=8–44}}</ref> She doubled the time devoted to literacy instruction in the primary grades through the sixth grade, to comprise one-half of the school day.  By 1909, the kindergarten was using games to teach reading, writing and math.  The school also began to solicit donations of books written by African Americans for the school library.

Jessie Guernsey, who earned both a bachelor’s and a master's degree from [[New York City]]'s [[Columbia University|Columbia Teachers College]], came to Calhoun in 1912 to begin her tenure as Academic Department head.  Under her leadership, high-interest grade-level books were used to teach reading.  She also increased weekly recitations, and reinstated using creative writing to teach spelling and grammar.  She continued literary societies, debates, musical training and social gatherings. The elementary grades began using state-adopted textbooks, while the secondary grades used more literature, newspapers and magazines.  The school added another year of study, to include grade ten.  The growth in curriculum and community outreach strained financial resources. At the same time the county-run elementary schools for African American children were becoming more and more popular.  Many of the CCS graduates had become teachers in the county school system.

In the mid-1920s Edward Allen became the first man to be Academic Department head at CCS.  He shifted the curriculum focus from teaching the basics of reading to teaching higher-order thinking skills. This was a major departure from the Hampton-Tuskegee model. During Allen’s tenure, he greatly expanded the library, both in number and types of books.

In the late 1920s R. Luella Jones came to CCS. She emphasized making the curriculum more rigorous, both to earn accreditation and to ensure placement of graduates in institutions of higher learning. She added college preparatory classes such as Latin, additional science and math classes, and made industrial arts an elective option.  She also added grades eleven and twelve so that students could graduate from CCS.  The library’s collection expanded to over 6,000 volumes.

==Closing years==
Charlotte Thorn died on August 29, 1932.  She had led the school for more than 30 years following Mabel Dillingham's death in 1895. Although Calhoun continued as a private school for several more years, the [[Great Depression]] reduced sources of support. In addition, increasing mechanization in agriculture decreased the need for rural labor. With the [[Great Migration (African American)|Great Migration]] of blacks from the area to northern and midwestern industrial cities for jobs, the area population was reduced to the point that the school could not survive financially.<ref name="Willis"/>

==Present day==
[[File:The Calhoun High School 2012.JPG|thumb|Calhoun High School]]
In 1943, the state of Alabama acquired the Calhoun Colored School. A new facility was built on the grounds that serves as the [[Public school (government funded)|public]] [[Secondary education in the United States|high school]] run by the [[Lowndes County, Alabama|Lowndes County]] Board of Education; the county is majority-African American in ethnicity.

The principal's house on County Route 53 is the last remaining structure from the original school. It was listed on the [[National Register of Historic Places listings in Alabama#Lowndes County|National Register of Historic Places]] in recognition of the important achievements of the school and the role it played in African-American education in Lowndes County.<ref>{{cite book |page=193 |title=African American Historic Places |editor-first=Beth L. |editor-last=Savage |year=1995 |publisher=[[John Wiley and Sons]] |isbn=0-471-14345-6 |url=https://books.google.com/books?id=wjZIkchWX5AC |accessdate=2007-10-16}}</ref>

==References==
{{Reflist}}

==External links==
* [http://sites.google.com/site/calhountigers/ The Calhoun High School Website]
* [https://sites.google.com/site/lowndescountypublicschools/Home Lowndes County Public Schools Website]

{{NRHP in Lowndes County, Alabama}}

[[Category:School buildings on the National Register of Historic Places in Alabama]]
[[Category:Educational institutions established in 1892]]
[[Category:National Register of Historic Places in Lowndes County, Alabama]]
[[Category:African-American history of Alabama]]
[[Category:Defunct schools in Alabama]]
[[Category:Historically segregated African-American schools in the United States]]