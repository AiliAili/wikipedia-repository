{{Infobox television episode
| title        = Unbowed, Unbent, Unbroken
| series       = [[Game of Thrones]]
| image        = Game-of-Thrones-S05E06-Theon-watches-Sansa-get-raped.jpg
| image_size   = 280
| alt          = 
| caption      = Reek is forced to watch in terror as Sansa is raped.
| season       = 5
| episode      = 6
| director     = [[Jeremy Podeswa]]
| writer       = [[Bryan Cogman]]
| producer     = 
| music        = [[Ramin Djawadi]] 
| photographer = Gregory Middleton
| editor       = Crispin Green
| production   = 
| airdate      = {{Start date|2015|05|17}}
| length       = 54 minutes
| guests       = 
* [[Diana Rigg]] as Olenna Tyrell
* [[Jonathan Pryce]] as High Sparrow
* [[Alexander Siddig]] as Doran Martell
* [[DeObia Oparei]] as Areo Hotah
* [[Keisha Castle-Hughes]] as Obara Sand
* [[Rosabell Laurenti Sellers]] as Tyene Sand
* [[Jessica Henwick]] as Nymeria Sand
* [[Finn Jones]] as Loras Tyrell
* [[Will Tudor]] as Olyvar
* [[Eugene Simon]] as Lancel Lannister
* [[Faye Marsay]] as the Waif
* [[Adewale Akinnuoye-Agbaje]] as Malko
* [[Toby Sebastian]] as Trystane Martell
* [[Nell Tiger Free]] as Myrcella Baratheon
* [[Charlotte Hope]] as Myranda
* [[Elizabeth Webster]] as Walda Bolton
* Michael Yare as Slaver
* James McKenzie Robinson as Joss
* Hattie Gotobed as Ghita
| awards       = 
| season_list  = 
| prev         = [[Kill the Boy]]
| next         = [[The Gift (Game of Thrones)|The Gift]]
| episode_list = [[Game of Thrones (season 5)|''Game of Thrones'' (season 5)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}
"'''Unbowed, Unbent, Unbroken'''" is the sixth episode of the [[Game of Thrones (season 5)|fifth season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 46th overall. The episode was written by [[Bryan Cogman]], and directed by [[Jeremy Podeswa]].

The episode polarized critics and viewers for its ending, depicting a violent sexual assault. It received a mixed rating of 58% on [[Rotten Tomatoes]], the lowest rating received to date by a ''Game of Thrones'' episode. The previous lowest rating for an episode was a 91% rating for [[Game of Thrones (season 4)|season 4]]'s "[[The Watchers on the Wall]]".<ref name="Rotten Tomatoes">{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s05/e06/|title=Unbowed, Unbent, Unbroken at Rotten Tomatoes|work=[[Rotten Tomatoes]]|accessdate=May 19, 2015}}</ref><ref>{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s04/e09/|title=The Watchers of the Wall|work=[[Rotten Tomatoes]]|accessdate=May 19, 2015}}</ref> Nonetheless, director [[Jeremy Podeswa]] received an [[Emmy Award]] nomination for [[Primetime Emmy Award for Outstanding Directing for a Drama Series|Outstanding Directing for a Drama Series]] for this episode.<ref>{{cite web |url=http://deadline.com/2015/07/emmy-award-nominations-2015-full-list-1201476759/ |title=Emmy Nominations 2015 – Full List |publisher=Deadline.com |first=Patrick |last=Hipes |date=July 16, 2015 |accessdate=July 16, 2015}}</ref><ref name="emmys">{{cite news |url=http://variety.com/2015/tv/news/2015-primetime-emmy-award-winners-list-1201598030/|title=Emmy Award Winners 2015 – Full List |author=staff|date=September 20, 2015 |work=Variety |accessdate=September 21, 2015}}</ref> The name of the episode comes from the House Martell [[motto]] ''Unbowed, Unbent, Unbroken''

==Plot==

===In Braavos===
Arya Stark ([[Maisie Williams]]) continues her training with the Faceless Men, washing the bodies of those that die in the temple. She asks the Waif ([[Faye Marsay]]) what they do with the bodies after they're clean, but the Waif tells her that she is not yet ready to know. Arya then asks how to pass the Game of Faces. The Waif hints that in order to pass, she must be able to convincingly lie. When Jaqen H'ghar ([[Tom Wlaschiha]]) comes to test Arya again, she tells him how she came to join the Faceless Men, but lies about several key facts. Jaqen is able to tell when Arya is lying, including when Arya claims that she hated [[Sandor Clegane]]. Jaqen tells Arya that she is lying not only to him, but to herself.

Some time later, a man brings his sick daughter to the temple so that she can die in peace. Arya talks to the sickly girl and lies about how she was ill like her in the past. Arya then gives the girl the temple's poisoned water to end her suffering. After she cleans the body, Arya is taken by Jaqen to a chamber underneath the temple, where the Faceless Men store the faces of all the people that have died in the temple. Jaqen tells Arya that she is not yet ready to become "no one", but she is ready to become "someone else".

===In Dorne===
Trystane Martell ([[Toby Sebastian]]) and Myrcella Baratheon ([[Nell Tiger Free]]) walk through the Water Gardens, and make plans to marry one another, much to the concern of Trystane's father, Prince Doran ([[Alexander Siddig]]). Meanwhile, Jaime Lannister ([[Nikolaj Coster-Waldau]]) and Bronn ([[Jerome Flynn]]) disguise themselves as Dornish guards and infiltrate the gardens. Bronn knocks out Trystane and Jaime moves to take Myrcella but they are ambushed by the Sand Snakes: Obara ([[Keisha Castle-Hughes]]), Nymeria ([[Jessica Henwick]]), and Tyene ([[Rosabell Laurenti Sellers]]). Jaime and Bronn battle with them briefly before more Dornish guards, led by Areo Hotah ([[DeObia Oparei]]), arrive and arrest both groups. Ellaria Sand ([[Indira Varma]]) is also arrested for orchestrating the planned kidnapping.

===On the Valyrian peninsula===
Jorah Mormont ([[Iain Glen]]) and Tyrion Lannister ([[Peter Dinklage]]) continue to make their way to Meereen on foot. Tyrion tells Jorah how he is on the run from Westeros [[The Children (Game of Thrones)|after killing his father Tywin]], and also tells Jorah how his father, Jeor Mormont, was [[And Now His Watch Is Ended|killed by Night's Watch mutineers]] during an expedition north of the Wall. Shortly afterwards, they are captured by slavers, who plan to take the pair to Volantis for sale. After the leader of the slavers, Malko, mentions that Daenerys Targaryen has reopened the fighting pits in Meereen, Tyrion convinces the slavers to take them to Meereen instead, saying that Jorah is an accomplished warrior and can earn the slavers more money in the pits.

===In King's Landing===
Petyr Baelish ([[Aidan Gillen]]) returns to King's Landing at Cersei Lannister's request, where he is confronted by Brother Lancel ([[Eugene Simon]]) and a group of Sparrows. Lancel warns Petyr that the Sparrows have purged King's Landing of all vices, and they will not tolerate his prostitution business, though Petyr is unperturbed. Petyr then meets with Cersei, who has started to question where his loyalties lie. He assures her that he is loyal to the throne, and informs her that Sansa is alive in Winterfell and is set to marry Ramsay Bolton. Cersei is infuriated at the Boltons' apparent treachery, but Petyr convinces her that he has already set up a plan to deal with the situation. He informs her of Stannis Baratheon's impending attack on the Boltons and asks to lead the knights of the Vale to Winterfell to destroy whatever is left of the victor. In return, he asks to be named Warden of the North, a condition Cersei agrees to.

Olenna Tyrell ([[Diana Rigg]]) arrives in King's Landing in an effort to free Loras Tyrell ([[Finn Jones]]) from the Sparrows' custody. She warns Cersei that her actions have put the Lannister-Tyrell alliance in peril, though Cersei insists that she had nothing to do with Loras' confinement and expresses confidence that there will not be enough evidence to convict him. At Loras' holy inquest, the High Sparrow ([[Jonathan Pryce]]) interrogates both Loras and Margaery Tyrell ([[Natalie Dormer]]), and they both deny that Loras is homosexual. Olyvar ([[Will Tudor]]) is then brought in and testifies against Loras, citing a unique birthmark that Loras has, and also noting that Margaery had seen them in bed together. After Loras, enraged, attempts to attack Olyvar, the High Sparrow is convinced that there is enough evidence to proceed with Loras' trial. He also orders Margaery arrested for lying during the holy inquest. As Margaery is taken away by the Faith Militant, she calls for Tommen Baratheon ([[Dean-Charles Chapman]]) to do something about it, but he is too intimidated by the presence of the Faith Militant to respond.

===In Winterfell===
While Myranda ([[Charlotte Hope]]) bathes Sansa Stark ([[Sophie Turner (actress)|Sophie Turner]]), she attempts to intimidate her by telling her about Ramsay's ([[Iwan Rheon]]) former mistresses, whom Ramsay later hunted and killed with his dogs. Sansa boldly replies that she is not frightened. Later, Reek ([[Alfie Allen]]) arrives to take Sansa to the Godswood, where she is then married to Ramsay. Afterward, Ramsay takes her to his chambers and orders her to undress. When Reek tries to leave, Ramsay orders him to stay and watch, and proceeds to rape Sansa, to Reek's distress.

==Production==

===Writing===
[[File:Bryan Cogman Fan Photograph (cropped).jpg|right|thumb|The episode was written by series producer Bryan Cogman.]]
This episode was written by the series producer [[Bryan Cogman]], who has written at least one episode in every season of the show.  It contains some content from George Martin's novel ''A Feast for Crows'', chapters Arya II, The Queenmaker, Cat of the Canals, and Cersei X and ''A Dance with Dragons'', chapters the Ugly Little Girl, Tyrion X, and the Prince of Winterfell, though series consultant Elio Garcia describes the portrayal of some of these events as "vastly different" from the original.<ref name=Westeros.org>{{cite web|url=http://www.westeros.org/GoT/Episodes/Entry/Unbowed_Unbent_Unbroken/Book_Spoilers/#Book_to_Screen|title=EP506: Unbowed, Unbent, Unbroken|last=Garcia|first=Elio|last2=Antonsson|first2=Linda|work=Westeros.org|date=May 17, 2015|accessdate=May 18, 2015}}</ref>

Like other episodes this season, it also included content and storylines written specifically for the television adaptation.  Myles McNutt of ''A.V. Club'' points out that this changes the way the viewers interpret the showrunner's decisions.  When describing his opinion of the decision to show Sansa raped by Ramsay on their wedding night (a storyline given to a different character, Jeyne Poole, in the books),<ref name=NYPost>{{cite web|url=http://nypost.com/2015/05/19/its-a-stark-reality-outrage-over-sansa-rape-scene-misses-the-point/|title=It’s a Stark reality: Outrage over Sansa rape scene misses the point|last=Stewart|first=Sara|work=New York Post|date=May 19, 2015|accessdate=May 19, 2015}}</ref> he compares the scene to a similar one between Daenerys and Drogo in season one (which was consensual in the novels): "While we could frame the shifted events of Dany and Khal Drogo’s wedding night in light of where we knew Dany’s story was going, here we have no idea what this does to Sansa’s storyline."<ref name=AVClub>{{cite web|url=http://www.avclub.com/tvclub/game-thrones-experts-unbowed-unbent-unbroken-219563|title=Game of Thrones (experts): "Unbowed, Unbent, Unbroken"|last=McNutt|first=Myles|work=AV Club|date=May 17, 2015|accessdate=May 18, 2015}}</ref> Most critics questioned the decision to show Sansa raped on her wedding night, but, as ''Business Insider'' pointed out, "The book version of this scene was much, much worse," with Theon ordered, graphically, to participate in Jeyne's mistreatment.<ref name=BInsider>{{cite web|url=http://www.businessinsider.com/game-of-thrones-sansa-wedding-2015-5/|title=The biggest scene on Sunday's 'Game of Thrones' was toned down dramatically from the books|last=Renfro|first=Kim|work=Business Insider|date=May 18, 2015|accessdate=May 19, 2015}}</ref> In an interview with ''Entertainment Weekly'', show writer Bryan Cogman was asked about the decision to decrease the level of violence, responding, "Lord no. ''No-no-no-no-no.'' No. It’s still a shared form of abuse that they have to endure, Sansa and Theon. But it’s not the extreme torture and humiliation that scene in the book is."<ref name=EntertainmentWeekly>{{cite web|url=http://www.ew.com/article/2015/05/17/game-thrones-sansa-ramsay-interview?hootPostID=946f5fbe46c5ea4b1f185fca1cb9cbd4/|title=Game of Thrones producer explains Sansa's wedding night horror|last=Hibbard|first=James|work=Entertainment Weekly|date=May 17, 2015|accessdate=May 19, 2015}}</ref>

However, in other ways, the episode veers back to book canon: "Whereas Loras’ arrest suggested the show was replacing Margaery’s alleged dalliances with his homosexuality, here the show gradually builds to Margaery’s arrest for lying on her brother’s behalf."<ref name=AVClub />

===Filming===
"Unbowed, Unbent, Unbroken" was directed by [[Jeremy Podeswa]]. He also directed the previous episode, "[[Kill the Boy]]".<ref name="s5 directors">{{cite web |url=http://insidetv.ew.com/2014/07/15/game-of-thrones-season-5-directors/ |title='Game of Thrones' season 5 directors chosen |work=[[Entertainment Weekly]] |first=James |last=Hibberd |date=July 15, 2014 |accessdate=July 15, 2014}}</ref>

==Reception==

===Ratings===
"Unbowed, Unbent, Unbroken" was watched by 6.24 million American viewers during its first airing.<ref>{{cite web|url=http://tvbythenumbers.zap2it.com/2015/05/19/sunday-cable-ratings-game-of-thrones-tops-night-keeping-up-with-the-kardashians-mad-men-more/405665/|title=Sunday Cable Ratings: 'Game of Thrones' Tops Night + 'Keeping Up With the Kardashians', 'Mad Men' & More|last=Kondolojy|first=Amanda|work=TV by the Numbers|date=May 19, 2015|accessdate=May 19, 2015}}</ref> With Live+7 DVR viewing factored in, the episode had an overall rating of 8.79 million viewers, and a 4.5 in the 18-49 demographic.<ref>{{cite web|url=http://tvbythenumbers.zap2it.com/2015/06/01/game-of-thrones-leads-adults-18-49-viewership-gains-orphan-black-tops-percentage-increases-in-live-7-cable-ratings-for-week-ending-may-17/410970/|title='Game of Thrones' Leads Adults 18-49 & Viewership Gains, 'Orphan Black' Tops Percentage Increases in Live +7 Cable Ratings for Week Ending May 17|last=Kondolojy|first=Amanda|work=TV by the Numbers|date=June 1, 2015|accessdate=June 1, 2015}}</ref> In the United Kingdom, the episode was viewed by 2.285 million viewers, making it the highest-rated broadcast that week. It also received 0.126 million timeshift viewers.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (18-24 May 2015)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=April 7, 2016}}</ref>

===Critical reviews and controversy===
The episode received polarized reviews from critics. On [[Rotten Tomatoes]], the episode received a 58% approval rating from critics with a [[rating average]] of 7.5 out of 10, the lowest of any episode in the series.<ref name="Rotten Tomatoes"/> The majority of professional criticism concerned the decision to have Ramsay rape Sansa on their wedding night, with most critics describing the scene as gratuitous and artistically unnecessary. "This grim scene was difficult for the show to justify," said Charlotte Runcie of ''[[The Daily Telegraph]]''.<ref name=Telegraph>{{cite web|url=http://www.telegraph.co.uk/culture/tvandradio/game-of-thrones/11608928/Game-of-Thrones-Unbowed-Unbent-Unbroken-season-5-episode-6-review.html|title=Game of Thrones: Unbowed, Unbent, Unbroken, season 5 episode 6, review: 'raw emotion'|last=Runcie|first=Charlotte|work=The Telegraph|date=May 17, 2015|accessdate=May 18, 2015}}</ref> Joanna Robinson of ''Vanity Fair'' added, "this rape scene undercuts all the agency that’s been growing in Sansa since the end of last season. [...] I’d never advocate that ''Game of Thrones'' (or any work of fiction) shy away from edgy plots out of fear of pushback or controversy. But edgy plots should always accomplish something above pure titillation or shock value and what, ''exactly'', was accomplished here?"<ref name=VanityFair /> Christopher Orr wrote in ''The Atlantic'', "I continue to be astonished that showrunners Benioff and Weiss still apparently believe that their tendency to ramp up the sex, violence, and—especially—sexual violence of George R.R. Martin’s source material is a strength rather than the defining weakness of their adaptation."<ref name=Atlantic>{{cite web|url=https://www.theatlantic.com/entertainment/archive/2015/05/game-of-thrones-roundtable-season-5-episode-six-unbowed-unbent-unbroken/393503/|title=Game of Thrones: A Pointless Horror and a Ridiculous Fight|last=Kornhaber|first=Spencer|last2=Orr|first2=Christopher|last3=Sullivan|first3=Amy|work=The Atlantic|date=May 17, 2015|accessdate=May 18, 2015}}</ref> Myles McNutt of ''[[The A.V. Club]]'' wrote, "The issue with the show returning to rape as a trope is not simply because there have been thinkpieces speaking out against it, and is not solely driven by the rational concerns lying at the heart of those thinkpieces. It’s also that the show has lost my faith as a viewer."<ref name=AVClub /> Writers from ''[[Vanity Fair (magazine)|Vanity Fair]]'', ''The Mary Sue'' and ''The Daily Beast'' all disapproved of the decision to use Sansa's victimization as a motivating agent for Theon, saying that the scene undermined Sansa's character development: "Was it really important to make that scene about Theon's pain?" wrote Joanna Robinson of ''Vanity Fair''.<ref name=VanityFair>{{cite web |url=http://www.vanityfair.com/hollywood/2015/05/game-of-thrones-rape-sansa-stark |title=''Game of Thrones'' Absolutely Did ''Not'' Need to Go There with Sansa Stark |work=Vanity Fair |first=Joanna |last=Robinson |date=May 17, 2015 |accessdate=May 18, 2015}}</ref><ref name=DailyBeast>{{cite web|url=http://www.thedailybeast.com/articles/2015/05/19/the-rape-of-sansa-stark-game-of-thrones-goes-off-book-and-enrages-its-female-fans.html|title= The Rape of Sansa Stark: ‘Game of Thrones’ Goes Off-book and Enrages Its Female Fans|last=Leon|first=Melissa|work=The Daily Beast|date=May 19, 2015|accessdate=May 20, 2015}}</ref><ref name=TheMarySueIsSoDone>{{cite web|url=http://www.themarysue.com/we-will-no-longer-be-promoting-hbos-game-of-thrones/|title= We Will No Longer Be Promoting HBO's Game of Thrones|last=Pantozzi|first=Jill|work=The Mary Sue|date=May 18, 2015|accessdate=May 20, 2015}}</ref>

Other critics responded positively to the scene. Sean T. Collins of ''Rolling Stone'' wrote: "[B]y involving a multidimensional main character instead of one introduced primarily to suffer, the series has a chance to grant this story the gravity and seriousness it deserves.<ref>{{cite web|url=http://www.rollingstone.com/tv/recaps/game-of-thrones-recap-stark-reality-20150517|title='Game of Thrones' Recap: Stark Reality|last=Collins|first=Sean T.|work=Rolling Stone|date=May 17, 2015|accessdate=May 23, 2015}}</ref> Sarah Hughes of ''The Guardian'' wrote: "I have repeatedly made clear that I’m not a fan of rape as a plot device – but the story of Ramsay and Sansa’s wedding was more than that. [...] The writers are walking a very fine line here. They handled it well tonight, telling a gothic tale of innocence sacrificed".<ref>{{cite web|url=https://www.theguardian.com/tv-and-radio/tvandradioblog/2015/may/18/game-of-thrones-recap-season-five-episode-six-unbent-unbowed-unbroken|title=Game of Thrones recap: season five, episode six – Unbent, Unbowed, Unbroken |last=Hughes|first=Sarah|work=The Guardian|date=May 18, 2015|accessdate=May 23, 2015}}</ref> Alyssa Rosenberg of ''The Washington Post'' wrote that the scene "managed to maintain a fine balance, employing a dignity and care for the experiences of victims that ''Game of Thrones'' has not always demonstrated."<ref>{{cite web|url=http://www.washingtonpost.com/news/act-four/wp/2015/05/17/game-of-thrones-season-5-episode-6-review-unbowed-unbent-unbroken/|title=‘Game of Thrones’ Season 5, Episode 6 review: "Unbowed, Unbent, Unbroken"|last=Rosenberg|first=Alyssa|work=The Washington Post|date=May 17, 2015|accessdate=May 23, 2015}}</ref>

Some critics questioned why this scene in particular should generate outrage when similar scenes have not. Sara Stewart of the ''New York Post'' pointed out that the rape and sexual abuse of both female and male characters is typical for ''Game of Thrones'': "Why are we suddenly so outraged about the rape of Sansa Stark, when this show has served up a steady diet of sexual assault and violence against women since its first season began?"<ref name=NYPost /> [[Cathy Young]] of ''[[Reason (magazine)|Reason]]'' magazine, writing in ''Time'' noted what she calls a lack of complaint in response to the sexual mistreatment of male characters in earlier seasons, specifically the literal emasculation of Theon Greyjoy and the sexual assault of Gendry.<ref name=Time>{{cite web|url=http://time.com/3891450/the-problem-with-the-backlash-to-the-game-of-thrones-rape-scene/|title = The Problem with the Backlash to the Game of Thrones Rape Scene|work=Time|date=May 21, 2015|accessdate=May 22, 2015}}</ref>

Criticism of the scene has not extended to the quality of the acting. Joanna Robinson of ''Vanity Fair'' wrote, "And if we can say one positive thing about that scene it's that Allen nailed his performance. Theon's horror mirrored our own and the camera—focusing on his reaction—let our minds fill in the blanks."<ref name=VanityFair /> Sophie Turner defended the scene as an artistic challenge for herself as an actor, saying, "When I read that scene, I kinda loved it. I love the way Ramsay had Theon watching. It was all so messed up. It’s also so daunting for me to do it. [...] I think it's going to be the most challenging season for me so far, just because it's so emotional for her. It’s not just crying all the time, like seasons 2 or 3, it’s super messed up."<ref name=EWTurner>{{cite web|url=http://www.ew.com/article/2015/05/17/game-thrones-sansa-wedding/|title=Game of Thrones: Sophie Turner says she 'loved' that horrifying scene |last=Hibberd|first=James|work=Entertainment Weekly|date=May 17, 2015|accessdate=May 20, 2015}}</ref> Iwan Rheon (Ramsay Bolton) agreed, referring to Turner's performances this season as "absolutely amazing."<ref name=DailyMail>{{cite web|url=http://www.dailymail.co.uk/news/article-3088849/Democratic-senator-says-s-quitting-Game-Thrones-gratuitous-rape-scene-actress-defends-controversial-episode.html|title= 'Gratuitous and disgusting': Senator leads boycott of Game of Thrones over shocking rape scene of Sansa Stark but actress playing her says she 'kinda loved it'|work=The Daily Mail|date=May 20, 2015|accessdate=May 20, 2015}}</ref>

Some viewers, including U.S. Senator [[Claire McCaskill]], announced that they would stop watching the show because of this scene.<ref name="DailyMail"/><ref name=BusinessInsiderTani>{{cite web|url=http://www.businessinsider.com.au/a-us-senator-says-shes-going-to-stop-watching-game-of-thrones-over-gratuitous-rape-scene-2015-5|title=A US senator says she's going to stop watching 'Game of Thrones' over 'gratuitous' rape scene|last=Tani|first=Maxwell|work=Business Insider|date=May 20, 2015|accessdate=May 29, 2015}}</ref>  According to ''Business Insider'', this scene and increased use of streaming services are likely reasons why ratings dropped from 6.2 million viewers for this episode to 5.4 million for the next episode, "[[The Gift (Game of Thrones)|The Gift]]."<ref name=BusinessInsider>{{cite web|url=http://uk.businessinsider.com/game-of-thrones-season-5-ratings-2015-5?r=US|title='Game of Thrones' ratings are falling: Here are two possible reasons why|last=Acuna|first=Kirsten|last2=Renfro|first2=Kim|work=Business Insider|date=May 28, 2015|accessdate=May 28, 2015}}</ref> However, there is some question as to how much of this drop is attributable to its Memorial Day weekend air date. Rebecca Martin of ''[[Wetpaint]]'' maintains that the air date was probably the only reason for the decrease in ratings.<ref name=WetPaint>{{cite web|url=http://www.wetpaint.com/2015-05-28-game-of-thrones-ratings-drop-sansa/|title=Game of Thrones Ratings Drop — Is It Because of Sansa Rape?|last=Martin|first=Rebecca|work=Wetpaint|date=May 28, 2015|accessdate=May 29, 2015}}</ref> The season two episode "[[Blackwater (Game of Thrones)|Blackwater]]," which also aired on Memorial Day, also suffered a notable drop in ratings.<ref name="Entertainment Weekly">{{cite web|url=http://www.ew.com/article/2012/05/30/game-of-thrones-ratings-blackwater|title='Game of Thrones' ratings dip for 'Blackwater'|last= Hibberd|first= James|work= Entertainment Weekly|date= May 30, 2012|accessdate= June 7, 2015}}</ref> No episode was aired on Memorial Day weekend in either season three or season four.<ref name= Fansided>{{cite web|url=http://fansided.com/2013/05/25/game-of-thrones-season-3-episode-9-no-show-on-may-26-during-memorial-day-weekend-will-air-june-2/|title=Game of Thrones Season 3, Episode 9: No Show on May 26 During Memorial Day Weekend; Will Air June 2|last= Sanchez|first= Josh|work= Fansided|date= May 24, 2013|accessdate= June 7, 2015}}</ref> Whatever the reason, the ratings for the episode after "The Gift," "[[Hardhome]]," were higher than those of both previous episodes.<ref name=TVHardhome>{{cite web|url=http://tvbythenumbers.zap2it.com/2015/06/02/sunday-cable-ratings-game-of-thrones-tops-night-keeping-up-with-the-kardashians-silicon-valley-naked-and-afraid-more/411032/|title=Sunday Cable Ratings: 'Game of Thrones' Tops Night + 'Keeping Up With the Kardashians', 'Silicon Valley', 'Naked and Afraid' & More|last=Bibel|first=Sara|work=TV by the Numbers|date=June 2, 2015|accessdate=June 2, 2015}}</ref>

===Accolades===
For the [[67th Primetime Emmy Awards]], this episode was nominated for [[Primetime Emmy Award for Outstanding Directing for a Drama Series|Outstanding Directing for a Drama Series]]<ref>{{cite web |url=http://deadline.com/2015/07/emmy-award-nominations-2015-full-list-1201476759/ |title=Emmy Nominations 2015 – Full List |publisher=Deadline.com |first=Patrick |last=Hipes |date=July 16, 2015 |accessdate=July 16, 2015}}</ref> At the [[67th Primetime Creative Arts Emmy Awards]], the episode won Outstanding Production Design for a Fantasy Program.

{| class="wikitable sortable plainrowheaders"
|-
! Year
! Award
! Category
! Nominee(s)
! Result
! class="unsortable" | {{Abbreviation|Ref.|Reference}}
|-
| rowspan=3| 2015
|scope="row"| [[67th Primetime Emmy Awards|Primetime Emmy Award]]
|scope="row"| [[Primetime Emmy Award for Outstanding Directing for a Drama Series|Outstanding Directing for a Drama Series]]
|scope="row"| [[Jeremy Podeswa]]
| {{nom}}
| rowspan="3" |<ref>{{cite web |url=http://www.emmys.com/shows/game-thrones |title=Game of Thrones |publisher=Emmys.com |accessdate=April 9, 2017}}</ref>
|-
|scope="row" rowspan=2|[[67th Primetime Creative Arts Emmy Awards|Primetime Creative Arts Emmy Awards]]
|scope="row"| [[Primetime Emmy Award for Outstanding Cinematography for a Single-Camera Series|Outstanding Cinematography for a Single-Camera Series]]
|scope="row"| Greg Middleton
| {{nom}}
|-
|scope="row"| Outstanding Production Design for a Fantasy Program
|scope="row"|  Deborah Riley, Paul Ghirardani, Rob Cameron
| {{won}}
|-
| rowspan=2|2016
|scope="row"| [[ADG Excellence in Production Design Award]]
|scope="row"| One-Hour Single Camera Fantasy Television Series
|scope="row"| Deborah Riley
| {{won}}
|<ref>{{cite web |url=http://deadline.com/2016/01/adg-award-winners-2016-full-list-art-directors-guild-1201693936/ |title=Art Directors Guild Award Winners|first=Erik|last=Pedersen|date=January 31, 2016|publisher=Deadline.com|accessdate=April 9, 2017}}</ref>
|-
|scope="row"| [[Canadian Society of Cinematographers]]
|scope="row"| TV Series Cinematography
|scope="row"| Gregory Middleton
|{{nom}}
|<ref>{{cite web|title=CSC Awards 2016|url=http://www.csc.ca/CSCawards/2016/default.asp|publisher=Canadian Society of Cinematographers|accessdate=April 9, 2017}}</ref>
|-
|}

==References==
{{reflist|2}}

==External links==
{{wikiquotepar|Game_of_Thrones_(TV_series)#Unbowed,_Unbent,_Unbroken_.5B5.06.5D|Unbowed, Unbent, Unbroken}}
* {{URL|1=http://www.hbo.com/game-of-thrones/episodes/5/46-unbowed-unbent-unbroken/index.html|2="Unbowed, Unbent, Unbroken"}} at [[HBO.com]]
* {{IMDb episode|3866842|Unbowed, Unbent, Unbroken}}
* {{tv.com episode|game-of-thrones/unbowed-unbent-unbroken-3076967/|Unbowed, Unbent, Unbroken}}

{{Game of Thrones Episodes}}

{{DEFAULTSORT:Unbowed, Unbent, Unbroken}}
[[Category:2015 American television episodes]]
[[Category:Game of Thrones episodes]]
[[Category:Rape in fiction]]