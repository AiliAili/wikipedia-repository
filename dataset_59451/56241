{{Infobox journal
| title         = The Church Quarterly Review
| cover         = 
| editor        = 
| discipline    = [[Church of England]], [[Theology]]
| peer-reviewed = 
| language      = [[English language|English]]
| formernames   = 
| abbreviation  = 
| publisher     = [[Society for Promoting Christian Knowledge]] (since 1920)
| country       = England
| frequency     = Quarterly
| history       = 1875-1971
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0269-4034
| eISSN         = 
| boxwidth      = 
}}

'''''The Church Quarterly Review''''' is an English journal published by the [[Society for Promoting Christian Knowledge]]. It existed independently from 1875 until 1968; in that year it merged with the ''London Quarterly and Holborn Review'', a [[Methodism|Methodist]] journal and became known as ''The Church Quarterly'', which was published until 1971.

==History==
It was first published privately in 1875, at the instigation of [[Richard William Church]], then [[List of Deans of St Paul's|Dean]] of [[St Paul's Cathedral]], and focused on [[Church of England]] and [[theology]] issues from a [[High church]] perspective. Its original mission statement was "to be worthily representative of the teaching and position of the Church of England,"<ref name="altholz"/> and it advertised itself as "the recognised organ of orthodox opinion for the Church of England."<ref>{{cite journal|title=The Church Quarterly Review (advertisement)|journal=[[The Nineteenth Century]]|year=1884|volume=15|pages=1081|url=https://books.google.com/books?id=NDIAAAAAYAAJ&pg=PA1081|accessdate=6 July 2010}}</ref> The first issue was published in October 1875, and the first article ("Italy and her Church") was written by [[William Ewart Gladstone]].<ref name="altholz">{{cite journal|last=Altholz|first=Josef L.|title=''The Church Quarterly Review'', 1875–1900: A Marked File and Other Sources|journal=[[Victorian Periodicals Review]]|year=1984|volume=17|issue=1–2|pages=52–57|jstor=20082103}}</ref>

In 1920, the Society for Promoting Christian Knowledge took over the journal, and ended its longstanding policy of publishing mainly anonymous contributions<ref name="nyt">{{cite news|title=The English Church Quarterly|url=https://query.nytimes.com/mem/archive-free/pdf?res=9500E7DD113CEE3ABC4C52DFB767838A699FDE|accessdate=4 July 2010|newspaper=[[The New York Times]]|date=14 November 1881}}</ref> as well as its High church associations; in 1921, longtime editor A.C. Headlam gave up his position.<ref name="altholz"/>

In 1968, the journal merged with the ''London Quarterly and Holborn Review'', a Methodist journal (merged from two Victorian journals). The result of this merger was ''The Church Quarterly'', which ceased publication in 1971.<ref name="altholz"/>

==Editors==
*1876-1879: Arthur Rawson Ashwell<ref name="altholz"/>
*1881: Cazenove<ref name="nyt"/>
*1901-1921: [[Arthur Cayley Headlam]]<ref name="altholz"/><ref>{{cite web|title=A. Headlam|url=http://www.headlam.me.uk/html_pages/headlam_A.htm|accessdate=5 July 2010}}</ref><ref>{{cite news|title=Our Cable Letter|url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9407E6DD1E39E733A25756C0A9669D946097D6CF|accessdate=4 July 2010|newspaper=[[The New York Times]]|date=5 October 1901}}</ref>
*1956-1969: [[John W. C. Wand|John William Charles Wand]]<ref>{{cite web|last=Arnott|first=F.R.|title= Wand, John William Charles (1885 - 1977) |url=http://adbonline.anu.edu.au/biogs/A120423b.htm|publisher=[[Australian Dictionary of Biography]]|accessdate=5 July 2010}}</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Church Quarterly Review, The}}
[[Category:British quarterly magazines]]
[[Category:Church of England]]
[[Category:Defunct magazines of the United Kingdom]]
[[Category:English magazines]]
[[Category:Magazines established in 1875]]
[[Category:Magazines disestablished in 1971]]
[[Category:British religious magazines]]