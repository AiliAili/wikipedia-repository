{{Infobox journal
| title = Optica
| cover = [[File:Optica_Journal_Cover_2014.jpg]]
| editor = [[Alex L. Gaeta]]
| discipline = Theoretical and applied [[optical science]] and [[photonics]]
| abbreviation = Optica
| publisher = [[The Optical Society]]
| country =
| frequency = Monthly
| history = 2014–present
| openaccess = Yes
| license =
| impact = 
| impact-year = 
| website = http://www.osapublishing.org/optica/
| link1 = https://www.osapublishing.org/optica/browse.cfm
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 1843247545
| LCCN =
| CODEN =
| ISSN =
| eISSN = 2334-2536
}}
'''''Optica''''' is a monthly [[peer-reviewed]] [[open access]] [[scientific journal]] published by [[The Optical Society]]. It covers the entire spectrum of theoretical and applied [[optics]] and [[photonics]]. It was established in July 2014.<ref>{{cite web |title=OSA press release for the launch of Optica|url=http://www.osa.org/en-us/about_osa/newsroom/news_releases/2014/osa_to_launch_new_gold_open-access_journal_focused/ |publisher=[[Optical Society of America]] |accessdate=21 July 2015}}</ref> The founding [[editor-in-chief]] is [[Alex L. Gaeta]] ([[Columbia University]]).<ref name=aboutoptica>{{cite web |url=https://www.osapublishing.org/optica/journal/optica/about.cfm |title=About Optica  |publisher=[[Optical Society of America]] |accessdate=21 July 2015}}</ref><ref name=launch-editorial>{{cite journal |last1=Gaeta |first1=Alexander L. |journal=Optica |date=22 July 2014 |volume=1 |issue=1 |pages=ED1 |title=Welcome to ''Optica'': a new home for high-impact optics and photonics research|doi=10.1364/OPTICA.1.000ED1}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-07-25}}</ref>
* [[Current Contents]]/Physical, Chemical & Earth Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-25}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI/>

==References==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.osapublishing.org/optica/}}

[[Category:Open access journals]]
[[Category:Optics journals]]
[[Category:Optical Society academic journals]]
[[Category:Publications established in 2014]]
[[Category:Monthly journals]]