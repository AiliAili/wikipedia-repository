{{About|a genre of dramatic works|other uses|Comedy (disambiguation)|the popular meaning of the term "comedy"|Humour}}
{{pp-move-indef}}
{{Globalize|date=August 2008}}
[[File:Thalia sarcophagus Louvre Ma475.jpg|thumb|right|[[Thalia (muse)|Thalia]], muse of comedy, holding a comic mask - detail of "Muses Sarcophagus", the [[Muse|nine Muses]] and their attributes; marble, early second century AD, Via Ostiense - ''Louvre'']]
[[File:Charlie Chaplin.jpg|thumb|right|[[Charlie Chaplin]]]]
In a modern sense, '''comedy''' (from the {{lang-el|[[wikt:κωμῳδία|κωμῳδία]]}}, ''kōmōidía'') refers to any discourse or work generally intended to be [[humour|humorous]] or amusing by inducing [[laughter]], especially in [[theatre]], [[television]], [[film]], and [[stand-up comedy]]. The origins of the term are found in [[Ancient Greece]]. In the [[Athenian democracy]], the [[public opinion]] of voters was influenced by the [[political satire]] performed by the [[comic poets]] at the [[Ancient Greek theatre|theaters]].<ref>Henderson, J. (1993) ''Comic Hero versus Political Elite'' pp. 307–19 in {{cite book |editor1=Sommerstein, A.H. |editor2=S. Halliwell |editor3=J. Henderson |editor4=B. Zimmerman |title=Tragedy, Comedy and the Polis |year=1993 |publisher=Levante Editori |location=Bari}}</ref> The theatrical genre of Greek comedy can be described as a dramatic performance which pits two groups or societies against each other in an amusing [[agon]] or conflict. [[Northrop Frye]] depicted these two opposing sides as a "Society of Youth" and a "Society of the Old".<ref>([[Anatomy of Criticism]], 1957)</ref>  A revised view characterizes the essential agon of comedy as a struggle between a relatively powerless youth and the societal conventions that pose obstacles to his hopes. In this struggle, the youth is understood to be constrained by his lack of social authority, and is left with little choice but to take recourse in ruses which engender very dramatic [[irony]] which provokes [[laughter]].<ref>Marteinson, 2006</ref>

[[Satire]] and [[political satire]] use comedy to portray persons or social institutions as ridiculous or corrupt, thus alienating their audience from the object of their humour. [[Parody]] subverts popular genres and forms, critiquing those forms without necessarily condemning them.

Other forms of comedy include [[screwball comedy]], which derives its humour largely from bizarre, surprising (and improbable) situations or characters, and [[black comedy]], which is characterized by a form of humor that includes darker aspects of human behavior or human nature.  Similarly [[Toilet humour|scatological humour]], sexual humour, and [[race humour]] create comedy by violating [[Convention (norm)|social conventions]] or [[taboo]]s in comic ways. A [[comedy of manners]] typically takes as its subject a particular part of society (usually upper class society) and uses humor to parody or satirize the behaviour and mannerisms of its members. [[Romantic comedy]] is a popular genre that depicts burgeoning romance in humorous terms and focuses on the foibles of those who are falling in love.

== Etymology ==
[[File:Tragic comic masks - roman mosaic.jpg|thumb|Tragic Comic Masks of [[Theatre of Ancient Greece|Ancient Greek Theatre]]
represented in the  [[Hadrian's Villa]] mosaic.]]

The word "comedy" is derived from the [[Ancient Greek language|Classical Greek]] κωμῳδία ''kōmōidía'', which is a compound either of [[komos|κῶμος]] ''kômos'' (revel) or κώμη ''kṓmē'' (village) and ᾠδή ''ōidḗ'' (singing); it is possible that ''κῶμος'' itself is derived from ''κώμη'', and originally meant a village revel. The adjective "comic" (Greek κωμικός ''kōmikós),'' which strictly means that which relates to comedy is, in modern usage, generally confined to the sense of "laughter-provoking".<ref name = "Cornford1934pn">Cornford (1934){{Page needed|date=June 2011}}</ref> Of this, the word came into modern usage through the Latin ''comoedia'' and Italian ''commedia'' and has, over time, passed through various shades of meaning.<ref name = OED>Oxford English Dictionary</ref>

The [[Ancient Greece|Greeks]] and [[Ancient Rome|Romans]] confined their use of the [[word]] "comedy" to descriptions of stage-plays with happy endings. [[Aristotle]] defined comedy as an imitation of men worse than the average (where [[tragedy]] was an imitation of men better than the average).  However, the characters portrayed in comedies were not worse than average in every way, only insofar as they are Ridiculous, which is a species of the Ugly. The Ridiculous may be defined as a mistake or deformity not productive of pain or harm to others; the mask, for instance, that excites laughter, is something ugly and distorted without causing pain.<ref>[[Richard McKeon|McKeon, Richard]]. ''The Basic Works Of Aristotle'', University of North Carolina at Chapel Hill, 2001, p. 1459.</ref> In the [[Middle Ages]], the term expanded to include narrative poems with happy endings.  It is in this sense that [[Dante]] used the term in the title of his poem, ''[[The Divine Comedy|La Commedia]]''.

As time progressed, the word came more and more to be associated with any sort of performance intended to cause laughter.<ref name = OED/> During the Middle Ages, the term "comedy" became synonymous with [[satire]], and later with [[humour]] in general.

Aristotle's [[Poetics (Aristotle)|''Poetics'']] was translated into [[Arabic language|Arabic]] in the [[Islamic Golden Age|medieval Islamic world]], where it was elaborated upon by [[Arabic literature|Arabic writers]] and [[Early Islamic philosophy|Islamic philosophers]], such as Abu Bischr, and his pupils [[Al-Farabi]], [[Avicenna]], and [[Averroes]]. They disassociated comedy from [[Greek drama]]tic representation and instead identified it with [[Arabic poetry|Arabic poetic]] themes and forms, such as ''[[hija]]'' (satirical poetry). They viewed comedy as simply the "art of reprehension", and made no reference to light and cheerful events, or to the troubling beginnings and happy endings associated with classical Greek comedy.

After the [[Latin translations of the 12th century]], the term "comedy" gained a more general meaning in [[medieval literature]].<ref>{{cite journal|doi=10.2307/470561|title=Comedy as Satire in Hispano-Arabic Spain|first=Edwin J.|jstor=470561|last=Webber|journal=Hispanic Review|volume=26|issue=1|date=January 1958|publisher=University of Pennsylvania Press|pages=1–11}}</ref>

In the late 20th century, many scholars preferred to use the term ''[[laughter]]'' to refer to the whole [[gamut]] of the comic, in order to avoid the use of ambiguous and problematically defined genres such as the [[grotesque]], [[irony]], and [[satire]].<ref>Herman Braet, Guido Latré, Werner Verbeke (2003) [https://books.google.com/books?id=6mqDEpy0YUsC&pg=PA1 ''Risus mediaevalis: laughter in medieval literature and art''] p.1 quotation: {{quotation|The deliberate use by Menard of the term 'le rire' rather than 'l'humour' reflects accurately the current evidency to incorporate all instances of the comic in the analysis, while the classification in genres and fields such as grotesque, humour and even irony or satire always poses problems. The terms humour and laughter are therefore pragmatically used in recent historiography to cover the entire spectrum.}}</ref><ref>Ménard, Philippe (1988) ''Le rire et le sourire au Moyen Age dans la litterature et les arts. Essai de problématique'' in Bouché, T. and Charpentier H. (eds., 1988) ''Le rire au Moyen Âge, Actes du colloque international de Bordeaux'', pp.7-30</ref>

== History ==

=== Dionysiac origins, Aristophanes and Aristotle ===
[[File:Samia (Girl from Samos) Mytilene 3cAD.jpg|thumb|[[Roman Empire|Roman-era]] [[Roman mosaic|mosaic]] depicting a scene from [[Menander]]'s comedy ''[[Samia (play)|Samia]]'' ("The Woman from Samos")]]
{{See also|Old Comedy|Menander|Ancient Greek comedy}}
Starting from 425 BCE, [[Aristophanes]], a comic playwright and satirical author of the [[Theatre of ancient Greece|Ancient Greek Theater]] wrote 40 comedies, 11 of which survive.  Aristophanes developed his type of comedy from the earlier [[satyr plays]], which were often highly [[obscene]].<ref>Aristophanes (1996) [https://books.google.com/books?id=YhaawA_m9SEC&pg=PR9 ''Lysistrata''], Introduction, p.ix, published by Nick Hern Books</ref> Of the satyr plays the only surviving examples are by [[Euripides]] which are much later examples and not representative of the genre.<ref>Reckford, Kenneth J. (1987)[https://books.google.com/books?id=v9MSdTvbOFAC&pg=PA105 ''Aristophanes' Old-and-new Comedy: Six essays in perspective''] p.105</ref> In ancient Greece, comedy originated in bawdy and [[ribald]] songs or recitations apropos of [[phallic processions]] and fertility festivals or gatherings.<ref name = "Cornford1934p3">[[Francis MacDonald Cornford|Cornford, F.M.]] (1934) [https://books.google.com/books?id=uhE9AAAAIAAJ ''The Origin of Attic Comedy''] pp.3-4 quotation: {{quotation|That Comedy sprang up and took shape in connection with Dionysiac or Phallic ritual has never been doubted.}}</ref>

Around 335 BCE, [[Aristotle]], in his work ''[[Poetics (Aristotle)|Poetics]]'', stated that comedy originated in [[Phallic processions]] and the light treatment of the otherwise base and ugly. He also adds that the origins of comedy are obscure because it was not treated seriously from its inception.<ref>{{cite web|url=http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Aristot.+Poet.+1449a |title=Aristotle, Poetics, lines beginning at 1449a |publisher=Perseus.tufts.edu |date= |accessdate=2012-06-30}}</ref> However, comedy had its own [[Muse]]: [[Thalia (muse)|Thalia]].

[[Aristotle]] taught that comedy was generally a positive for society, since it brings forth happiness, which for [[Aristotle]] was the ideal state, the final goal in any activity. For Aristotle, a comedy did not need to involve sexual humor.  A comedy is about the fortunate arise of a sympathetic character. Aristotle divides comedy into three categories or subgenres:  [[farce]], [[romantic comedy]], and [[satire]].  On the contrary, [[Plato]] taught that comedy is a destruction to the self. He believed that it produces an emotion that overrides rational self-control and learning. In ''[[The Republic (Plato)|The Republic]]'', he says that the Guardians of the state should avoid laughter, " 'for ordinarily when one abandons himself to violent laughter, his condition provokes a violent reaction.' " Plato says comedy should be tightly controlled if one wants to achieve the ideal state.

Also in ''Poetics'', Aristotle defined Comedy as one of the original four genres of [[literature]]. The other three genres are [[tragedy]], [[epic poetry]], and [[lyric poetry]]. Literature in general is defined by Aristotle as a [[mimesis]], or imitation of life. Comedy is the third form of literature, being the most divorced from a true mimesis. Tragedy is the truest mimesis, followed by epic poetry, comedy and lyric poetry. The genre of comedy is defined by a certain pattern according to Aristotle's definition. Comedies begin with low or base characters seeking insignificant aims, and end with some accomplishment of the aims which either lightens the initial baseness or reveals the insignificance of the aims.

=== In ancient Sanskrit drama ===
After 200 BCE, in ancient [[Sanskrit drama]], [[Bharata Muni]]'s ''[[Natya Shastra]]'' defined humour (''hāsyam'') as one of the nine ''[[nava rasas]]'', or principle ''[[Rasa (aesthetics)|rasas]]'' (emotional responses), which can be inspired in the audience by ''bhavas'', the imitations of emotions that the actors perform. Each ''rasa'' was associated with a specific ''bhavas'' portrayed on stage. In the case of humour, it was associated with mirth (''hasya'').

=== Shakespearean and Elizabethan comedy ===
[[File:MND title page.jpg|thumb|left|upright|Title page of the [[first quarto]] of Shakespeare's ''[[Midsummer Night's Dream]]'' (1600)]]
"Comedy", in its [[Elizabethan]] usage, had a very different meaning from modern comedy. A Shakespearean comedy is one that has a happy ending, usually involving marriages between the unmarried characters, and a tone and style that is more light-hearted than Shakespeare's other plays.<ref>Regan, Richard. "[http://www.faculty.fairfield.edu/rjregan/rrScom.htm Shakespearean comedy]"</ref>

The [[Punch and Judy]] show has roots in the 16th-century Italian [[commedia dell'arte]]. The figure of Punch derives from the Neapolitan stock character of [[Pulcinella]].<ref name="eb1911-punch">{{cite EB1911|last=Wheeler|first=R. Mortimer|wstitle=Punch (puppet)|title=Punch|volume=22|pages=648-649}}</ref> The figure who later became Mr. Punch made his first recorded appearance in England in 1662.<ref name="British seaside"/> Punch and Judy are performed in the spirit of outrageous comedy — often provoking shocked laughter — and are dominated by the anarchic clowning of Mr. Punch.<ref>{{cite news|title=Mr Punch celebrates 350 years of puppet anarchy|url=http://www.bbc.co.uk/news/entertainment-arts-17895716|agency=BBC|date=11 June 2015}}</ref> Appearing at a significant period in British history, professor Glyn Edwards states: "[Pulcinella] went down particularly well with Restoration British audiences, fun-starved after years of [[Puritanism]]. We soon changed Punch's name, transformed him from a marionette to a hand puppet, and he became, really, a spirit of Britain - a subversive maverick who defies authority, a kind of puppet equivalent to our [[political cartoon]]s."<ref name="British seaside">{{cite news|title=Punch and Judy around the world|url=http://www.telegraph.co.uk/expat/expatlife/7949781/Punch-and-Judy-around-the-world.html|agency=The Telegraph|date=11 June 2015}}</ref>

=== 19th to early 20th century ===
In early 19th century England, [[pantomime]] acquired its present form which includes slapstick comedy and featured the first mainstream [[clown]] [[Joseph Grimaldi]], while comedy routines also featured heavily in British [[music hall]] theatre which became popular in the 1850s.<ref>Jeffrey Richards (2014). "The Golden Age of Pantomime: Slapstick, Spectacle and Subversion in Victorian England". I.B.Tauris,</ref> British comedians who honed their skills in music hall sketches include [[Charlie Chaplin]], [[Stan Laurel]] and [[Dan Leno]].<ref name="Karno"/> English music hall comedian and theatre impresario [[Fred Karno]] developed a form of sketch comedy without dialogue in the 1890s, and Chaplin and Laurel were among the comedians who worked for him.<ref name="Karno">McCabe, John. "Comedy World of Stan Laurel". p. 143. London: Robson Books, 2005, First edition 1975</ref> American film producer [[Hal Roach]] stated: "Fred Karno is not only a genius, he is the man who originated slapstick comedy. We in Hollywood owe much to him."<ref>J. P. Gallagher (1971). "Fred Karno: master of mirth and tears". p. 165. Hale.</ref> American [[vaudeville]] emerged in the 1880s and remained popular until the 1930s, and featured comedians such as [[W. C. Fields]], [[Buster Keaton]] and the [[Marx Brothers]].

=== 20th century film and television ===
[[File:Lewis and Martin.jpg|upright|thumb|[[Dean Martin]] and [[Jerry Lewis]] (ca. 1950)]]
[[File:Jim-Carrey-2008.jpg|upright|thumb|[[Jim Carrey]] mugs for the camera]]
[[File:Rowan Atkinson and Manneken Pis.jpg|upright|thumb|[[Rowan Atkinson]] as [[Mr. Bean]]]]
[[File:Jackie Chan Cannes.jpg|thumb|upright|[[Jackie Chan]] at the [[2008 Cannes Film Festival]]]]
[[File:MargaretChoSanFrancisco.jpg|thumb|upright|[[Stand-up comedy|Stand-up comedian]] [[Margaret Cho]]]]
[[File:Oleg popov2.jpg|thumb|upright|[[Oleg Popov|Popov the Clown]] in 2009]]
[[File:Dame Edna at the royal wedding cropped.jpg|upright|thumb|[[Barry Humphries]] in character in London as "[[Dame Edna Everage]]" on the day of the 2011 [[Wedding of Prince William and Kate Middleton]]]]
[[File:Jordan Peele Peabody 2014 (cropped).jpg|upright|thumb|[[Jordan Peele]] at the Peabody awards.]]
The advent of cinema in the late 19th century, and later radio and television in the 20th century broadened the access of comedians to the general public. Charlie Chaplin, through silent film, became one of the best known faces on earth. The silent tradition lived on well in to the 20th century through mime artists like [[Marcel Marceau]], and the physical comedy of artists like [[Rowan Atkinson]] as [[Mr. Bean]]. The tradition of the circus clown also continued, with such as [[Bozo the Clown]] in the United States and [[Oleg Popov]] in Russia. Radio provided new possibilities - with Britain producing the influential [[Goon Show]] after the Second World War. [[American cinema]] has produced a great number of globally renowned comedy artists, from [[Laurel and Hardy]], the [[Three Stooges]], [[Abbott and Costello]], [[Martin and Lewis|Dean Martin and Jerry Lewis]], as well as [[Bob Hope]] during the mid-20th century, to performers like [[George Carlin]], [[Robin Williams]], and [[Eddie Murphy]] at the end of the century. [[Hollywood]] attracted many international talents like the British comics [[Peter Sellers]], [[Dudley Moore]] and [[Sacha Baron Cohen]], Canadian comics [[Dan Aykroyd]], [[Jim Carrey]], and [[Mike Myers]], and the [[Australian comedy|Australian comedian]]  [[Paul Hogan]], famous for ''[[Crocodile Dundee]]''. Other centres of creative comic activity have been the [[cinema of Hong Kong]], [[Bollywood]], and French [[farce]].

American television has also been an influential force in world comedy: with American series like ''[[M*A*S*H (TV series)|M*A*S*H]]'', ''[[Seinfeld]]'' and ''[[The Simpsons]]'' achieving large followings around the world. British television comedy also remains influential, with quintessential works including ''[[Fawlty Towers]]'', [[Monty Python]], ''[[Dad's Army]]'', ''[[Blackadder]]'', and ''[[The Office (UK TV series)|The Office]]''. Australian satirist [[Barry Humphries]], whose comic creations include the housewife and "gigastar" Dame [[Edna Everage]], For his delivery of Dadaist and [[Absurdism|absurdist]] humour to millions, was described by biographer Anne Pender in 2010 as not only "the most significant theatrical figure of our time ... [but] the most significant comedian to emerge since Charlie Chaplin".<ref>{{cite web|url=http://www.brisbanetimes.com.au/entertainment/books/absurd-moments-in-the-frocks-of-the-dame-20100914-15ar3.html |title=Absurd moments: in the frocks of the dame |first=Steve |last=Meacham |publisher=Brisbanetimes.com.au |date=2010-09-15 |accessdate=2011-12-20}}</ref>

== Studies on the theory of the comic ==
The phenomena connected with [[laughter]] and that which provokes it have been carefully investigated by psychologists. They agree the predominant characteristics are incongruity or contrast in the object and shock or emotional seizure on the part of the subject. It has also been held that the feeling of superiority is an essential factor: thus [[Thomas Hobbes]] speaks of laughter as a "sudden glory". Modern investigators have paid much attention to the origin both of laughter and of smiling, as well as the development of the "play instinct" and its emotional expression.

[[George Meredith]] said that "One excellent test of the civilization of a country ... I take to be the flourishing of the Comic idea and Comedy; and the test of true Comedy is that it shall awaken thoughtful laughter." Laughter is said to be the cure to being sick. Studies show that people who laugh more often get sick less.<ref>{{cite journal |url=http://www.ep.tc/realist/15/03.html |title=An impolite interview with Lenny Bruce |journal=The Realist |issue=15 |date=February 1960 |page=3 |accessdate=2011-12-30 }}</ref><ref>{{cite web|url=http://emotional-literacy-education.com/classic-books-online-b/esycm10.htm |title=Essay on Comedy, Comic Spirit |first=George |last=Meredith|publisher=Encyclopedia of the Self, by Mark Zimmerman |year=1987 |accessdate=2011-12-30 }}</ref>

American literary theorist [[Kenneth Burke]] writes that the "comic frame" in rhetoric is "neither wholly euphemistic, nor wholly debunking—hence it provides the charitable attitude towards people that is required for purposes of persuasion and co-operation, but at the same time maintains our shrewdness concerning the simplicities of ‘cashing in.’" <ref>[http://newantichoicerhetoric.web.unc.edu/the-comedic-frame/]</ref> The purpose of the comic frame is to satirize a given circumstance and promote change by doing so. The comic frame makes fun of situations and people, while simultaneously provoking thought.<ref>[http://www.kbjournal.org/biebel]</ref> The comic frame does not aim to vilify in its analysis, but rather, rebuke the stupidity and foolery of those involved in the circumstances.<ref>[http://www.ithaca.edu/hs/history/journal/papers/sp02comedyandtragedy.html]</ref> For example, on ''[[The Daily Show]]'', [[Jon Stewart]] uses the "comic frame" to intervene in political arguments, one such way is his sudden contrast of serious news with crude humor. In a segment on President Obama's trip to China Stewart remarks on America's debt to the Chinese government while also having a weak relationship with the country. After depicting this dismal situation, Stewart shifts to speak directly to President Obama, calling upon him to "shine that turd up."<ref>Trischa Goodnow Knapp (2011). ''[https://books.google.com/books?id=OQ9Gu4-tYsQC&pg=PA237 The Daily Show and Rhetoric: Arguments, Issues, and Strategies]''. p. 327. Lexington Books, 2011</ref> For Stewart and his audience, introducing coarse language into what is otherwise a serious commentary on the state of foreign relations serves to frame the segment comically, creating a serious tone underlying the comedic agenda presented by Stewart.

== Forms ==
{{Main article|Comedic genres}}

Comedy may be divided into multiple [[genre]]s based on the source of humor, the method of delivery, and the context in which it is delivered.  The different forms of comedy often overlap, and most comedy can fit into multiple genres. Some of the subgenres of comedy are [[farce]], [[comedy of manners]], [[burlesque]], and [[satire]].

Some comedy apes certain cultural forms: for instance, parody and [[satire]] often imitate the conventions of the genre they are parodying or satirizing. For example, ''[[The Onion]]'' and ''[[The Colbert Report]]'' parody newspapers and television news shows like ''[[The O'Reilly Factor]]''.

Another form of comedy would be self-deprecation.  Many comedians focus on their misfortunes and foibles to entertain the public.

== Performing arts ==
{{Performing arts}}
{{Main article|Comedy (drama)}}
{{Prose|section|date=April 2008}}

=== Historical forms ===
* [[Ancient Greek comedy]], as practiced by [[Aristophanes]] and [[Menander]]
* [[Ancient Roman comedy]], as practiced by [[Plautus]] and [[Terence]]
* [[Burlesque]], from [[Music hall]] and [[Vaudeville]] to [[Performance art]]
* [[City comedy|Citizen comedy]], as practiced by [[Thomas Dekker (poet)|Thomas Dekker]], [[Thomas Middleton]] and [[Ben Jonson]]
* [[Clown]]s such as [[Richard Tarlton]], [[William Kempe]], and [[Robert Armin]]
* [[Comedy of humours]], as practiced by [[Ben Jonson]] and [[George Chapman]]
* [[Comedy of intrigue]], as practiced by [[Niccolò Machiavelli]] and [[Lope de Vega]]
* [[Comedy of manners]], as practiced by [[Molière]], [[William Wycherley]] and [[William Congreve]]
* [[Comedy of menace]], as practiced by [[David Campton]] and [[Harold Pinter]]
* ''[[comédie larmoyante]]'' or 'tearful comedy', as practiced by [[Pierre-Claude Nivelle de La Chaussée]] and [[Louis-Sébastien Mercier]]
* ''[[Commedia dell'arte]]'', as practiced in the twentieth century by [[Dario Fo]], [[Vsevolod Meyerhold]], and [[Jacques Copeau]]
* [[Farce]], from [[Georges Feydeau]] to [[Joe Orton]] and [[Alan Ayckbourn]]
* [[Jester]]
* Laughing comedy, as practiced by [[Oliver Goldsmith]] and [[Richard Brinsley Sheridan]]
* [[Restoration comedy]], as practiced by [[George Etherege]], [[Aphra Behn]] and [[John Vanbrugh]]
* Sentimental comedy, as practiced by [[Colley Cibber]] and [[Richard Steele]]
* [[Shakespearean comedy]], as practiced by [[William Shakespeare]]
* [[Stand-up comedy]]
* [[Dada]]ist and [[Surrealism|Surrealist]] performance, usually in [[cabaret]] form
* [[Theatre of the Absurd]], used by some critics to describe [[Samuel Beckett]], [[Harold Pinter]], [[Jean Genet]] and [[Eugène Ionesco]]<ref>This list was compiled with reference to ''The Cambridge Guide to Theatre'' (1998).</ref>
* [[Sketch comedy]]

=== Plays ===
* [[Comedy (drama)|Comic theatre]]
** [[Musical theater#Development of musical comedy|Musical comedy]] and palace

=== Opera ===
* [[Comic opera]]

=== Improvisational comedy ===
* [[Improvisational theatre]]
* [[Bouffon]] comedy
* [[Clowns]]

=== Joke ===
* [[One-liner joke]]
* [[Blonde jokes]]
* [[Shaggy dog story|Shaggy-dog story]]
* [[An Englishman, an Irishman and a Scotsman|Paddy Irishman joke]]
* [[Polish jokes]]
* [[Light bulb jokes]]

=== Stand-up comedy ===
[[Stand-up comedy]] is a mode of comic performance in which the performer addresses the audience directly, usually speaking in their own person rather than as a dramatic [[Character (arts)|character]].
* [[Impressionist (entertainment)]]
* [[Alternative comedy]]
* [[Comedy club]]
* [[Comedy album]]s

== Events and awards ==
* [[American Comedy Awards]]
* [[British Comedy Awards]]
* [[Canadian Comedy Awards]]
* [[Cat Laughs|Cat Laughs Comedy Festival]]
* [[The Comedy Festival]], in Aspen, formerly the HBO Comedy Arts Festival
* [[Edinburgh Festival Fringe]]
* [[Edinburgh Comedy Festival]]
* [[Halifax Comedy Festival]]
* [[Halloween Howls Comedy Festival]]
* [[Just for Laughs]] festival, in Montreal
* [[Leicester Comedy Festival]]
* [[Mark Twain Prize for American Humor]]
* [[Melbourne International Comedy Festival]]
* [[New Zealand International Comedy Festival]]
* [[New York Underground Comedy Festival]]
* [[HK International Comedy Festival]]

== List of comedians ==
* [[List of stand-up comedians]]
* [[List of musical comedians]]
* [[List of Australian comedians]]
* [[List of British comedians]]
* [[List of Canadian comedians]]
* [[List of Finnish comedians]]
* [[List of German language comedians]]
* [[List of Indian comedians]]
* [[List of Italian comedians]]
* [[List of Mexican comedians]]
* [[List of Puerto Rican comedians]]
* [[List of Filipino Comedian]]

== Mass media ==
{{literature}}
{{Prose|section|date=April 2008}}

=== Literature ===
{{expand section|date=April 2010}}
* [[Comic novel]]
* [[Light poetry]]

=== Film ===
* [[Comedy film]]
** [[Anarchic comedy film]]
** [[Gross-out film]]
** [[Parody film]]
** [[Romantic comedy film]]
** [[Screwball comedy film]]
** [[Slapstick film]]

=== Television and radio ===
* [[Television comedy]]
** [[Situation comedy]]
* [[Radio comedy]]

==== Comedy networks ====
* [[British sitcom]]
* [[British comedy]]
* [[Comedy Central]] - A television channel devoted strictly to comedy.
* [[Comedy Nights with Kapil]] - An Indian television program
* [[German television comedy]]
* [[List of British TV shows remade for the American market]]
* [[Paramount Comedy (Spain)]].
* [[Paramount Comedy]] [[Paramount Comedy 1|1]] and [[Paramount Comedy 2|2]].
* [[TBS (TV network)]]
* [[The Comedy Channel]] (Australia)
* [[The Comedy Channel (UK)]]
* [[The Comedy Channel (United States)]] – merged into [[Comedy Central]].
* [[Ha! (TV channel)|HA!]] – merged into [[Comedy Central]]
* [[The Comedy Network]], a Canadian TV channel.
* [[Gold (UK TV channel)|Gold]]

== See also ==
{{Portal|Comedy}}
* [[Lists of comedy films]]
* [[List of comedy television series]]
* [[List of genres]]
* [[Women in comedy]]

== References ==

=== Footnotes ===
{{Reflist|30em}}

=== Notations ===
{{Refbegin|30em}}
* {{cite book|author=Aristotle| title=[[Poetics (Aristotle)|Poetics]]}}
* {{cite book|last=Buckham |first=Philip Wentworth | url=https://books.google.com/?id=IjAZAAAAYAAJ&printsec=frontcover&dq=The+Theatre+of+the+Greeks|title=Theatre of the Greeks| year=1827}}
* {{cite book|last=Marteinson |first=Peter| year=2006| url=http://www.chass.utoronto.ca/french/as-sa/editors/origins.html | title=On the Problem of the Comic: A Philosophical Study on the Origins of Laughter| publisher= Legas Press| location=Ottawa}} http://french.chass.utoronto.ca/as-sa/editors/origins.html
* Pickard-Cambridge, Sir Arthur Wallace
** ''[[Dithyramb]], Tragedy, and Comedy '', 1927.
** ''The Theatre of Dionysus in Athens'', 1946.
** ''The Dramatic Festivals of Athens'', 1953.
* {{cite book|last=Raskin |first=Victor| title=The Semantic Mechanisms of Humor| year=1985 |publisher=Springer |isbn=978-90-277-1821-1 }}
* {{cite book|last=Riu |first=Xavier | title=Dionysism and Comedy| year=1999| url=http://ccat.sas.upenn.edu/bmcr/2000/2000-06-13.html}}
* {{cite book|last=Sourvinou-Inwood |first=Christiane| title=Tragedy and Athenian Religion| publisher=Lexington Books | year=2003 |isbn=978-0-7391-0400-2 }}
* {{cite book|last=Trypanis |first=C.A.|title=Greek Poetry from Homer to Seferis|publisher=University of Chicago Press|year=1981}}
* {{cite book|last=Wiles |first=David| title=The Masked Menander: Sign and Meaning in Greek and Roman Performance| year=1991 |publisher=Cambridge University Press |isbn=978-0-521-40135-7 }}
{{Refend}}

== External links ==
{{Americana Poster}}
*{{Wikiversity-inline|Collaborative play writing}}

{{Sister project links|Comedy}}
<!-- ==============================({{NoMoreLinks}})============================== -->
<!-- DO NOT ADD MORE LINKS TO THIS ARTICLE UNLESS THEY CONFORM TO [[WP:EL]]. WIKIPEDIA IS NOT A COLLECTION OF LINKS -->
<!-- ============================================================================= -->
* [http://www.alldocumentaries.org/category/comedy/ Comedy Documentary Films] at [http://www.alldocumentaries.org/ All Documentaries]
* {{dmoz|Arts/Performing_Arts/Comedy}}
* [http://www.dbu.edu/mitchell/comedydi.htm A Vocabulary for Comedy] from a professor at [[Dallas Baptist University]]
* [http://www.funnywitty.com 6 Elements of Comedy] by comedian Bobby Schimmel
{{Comedy footer}}
<!-- In the case of having own category with the same name of this article, the only one has to remain itself except the others. Then the other categories are moved into this article-named category, or it will get the complex and superfluity. -->

{{Theatre}}

{{Authority control}}

[[Category:Comedy| ]]