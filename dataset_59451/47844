{{format footnotes|date=July 2015}}
{{Infobox officeholder
|name = Jonathan F. Fanton
|honorific_suffix = 
|image = 
|caption = 
|birth_date = {{birth year and age|1943}}
|birth_place = 
|citizenship = 
|residence = 
|known_for = 
|alma_mater = [[Yale University]]
|employer =
|occupation = 
|home_town =
|boards =
|spouse = 
|order = 
|office = President of [[American Academy of Arts and Sciences]]
|term_start = 2014
|term_end = 
|predecessor = [[Leslie Cohen Berlowitz]]
|successor = 
|order2 = 3rd
|title2 = President of [[John D. and Catherine T. MacArthur Foundation]]
|term_start2 = 1999
|term_end2 = 2009
|predecessor2 = [[Adele Simmons]]
|successor2 = [[Robert L. Gallucci]]
|order3 = 6th
|title3 = President of [[The New School for Social Research]]
|term_start3 = 1982
|term_end3 = 1999
|predecessor3 = John Everett
|successor3 = [[Bob Kerrey]]
}}
'''Jonathan F. Fanton''' (born 1943) is the president of the [[American Academy of Arts and Sciences]].<ref name=globe>{{cite web|url=http://www.bostonglobe.com/metro/2014/04/17/american-academy-arts-and-sciences-picks-new-president-after-scandal/p6k2Uf8354oIujQABZN9TP/story.html|title=American Academy of Arts and Sciences picks new president|work=Boston Globe|date=April 17, 2014|accessdate=June 26, 2015}}</ref> He previously served as the president of the [[John D. and Catherine T. MacArthur Foundation]] from 1999 to 2009 and as the president of [[The New School for Social Research]] from 1982 to 1999.<ref>{{cite web|url=http://philanthropynewsdigest.org/news/people-in-the-news-4-20-14-appointments-and-promotions|title=People in the News (4/20/14): Appointments and Promotions|date=April 20, 2014|accessdate=June 26, 2015|work=Philanthropy News Digest}}</ref> He has served as board chair for several organizations, including [[Human Rights Watch]],<ref>{{cite web|url=https://www.hrw.org/node/76172 Board of Directors|title=Board of Directors|publisher = [[Human Rights Watch]]|accessdate=June 26, 2015}}, Human Rights Watch.</ref> the Security Council Report,<ref>{{cite web|url=http://www.securitycouncilreport.org/security-council-report---2009-annual-report.php|title=2009 Annual Report|work=Security Council Report Annual Report|accessdate=June 26, 2015}}, Security Council Report.</ref> and the [[New York State]] Commission on Independent Colleges and Universities.<ref>Fanton, Jonathan (September 13, 1988). {{cite web|url=https://www.nytimes.com/1988/09/13/opinion/financial-aid-partnership-keeps-higher-education-within-reach.html|title=Financial-Aid Partnership Keeps Higher Education Within Reach|work=New York Times|date=September 9, 2013|accessdate=June 26, 2015}} (Letter to the Editor). ''New York Times''.</ref> He currently chairs the board of [[Scholars At Risk]]<ref>{{cite web|url=http://scholarsatrisk.nyu.edu/People/Board.php|title=Board|publisher= [[Scholars At Risk]]|accessdate=June 26, 2015}}, Scholars at Risk.</ref> and serves on the board of the [[Asian Cultural Council]],<ref>{{cite web|url=http://www.asianculturalcouncil.org/about-us/our-board-of-trustees|title=Our Board of Trustees|publisher= [[Asian Cultural Council]]|accessdate=June 26, 2015}}.</ref> the board of the [[Benjamin Franklin House]],<ref>{{cite web|url=http://www.benjaminfranklinhouse.org/site/sections/about_house/govern.htm|title=Governance|publisher= [[Benjamin Franklin House]]|accessdate=June 26, 2015}}, Benjamin Franklin House.</ref> and the advisory board of the Newman’s Own Foundation.<ref>{{cite web|url=http://newmansownfoundation.org/about-us/our-people/our-board|title=Advisory Board|work=Newman's Own Foundation|accessdate=June 26, 2015}}, Newman's Own Foundation.</ref> He was elected a fellow of the [[American Academy of Arts and Sciences|American Academy]] in 1999.<ref>{{cite web|url=https://www.amacad.org/multimedia/pdfs/alphalist.pdf|title=Alphabetical Index of Active Members|publisher= [[American Academy of Arts and Sciences]]|accessdate=June 26, 2015}}, American Academy of Arts and Sciences.</ref>

== Early life and career ==
Born in [[Mobile, Alabama|Mobile]], [[Alabama]], Fanton grew up in [[Trumbull, Connecticut|Trumbull]] and [[Weston, Connecticut|Weston]], [[Connecticut]]. In 1961, he graduated from [[Choate School]]. At [[Yale University]], he earned a baccalaureate degree in 1965, and a Ph.D. in [[American history]] in 1978.<ref>{{cite web|url=http://www.roosevelthouse.hunter.cuny.edu/?rh-staff=jonathan-fanton|title=Jonathan Fanton|publisher= [[Roosevelt House Public Policy Institute at Hunter College]]|accessdate=June 26, 2015}}, Staff Directory, Roosevelt House Public Policy Institute at Hunter College.</ref> As an undergraduate, Fanton directed the Ulysses S. Grant Program, a summer enrichment program for talented students from the inner city.<ref name=gsas>{{cite web|url=http://gsas.yale.edu/news/extraordinary-alumnus-lead-american-academy-arts-and-sciences|title=Extraordinary Alumnus to Lead the American Academy of Arts and Sciences|work=Yale Graduate School of Arts and Sciences|accessdate=June 26, 2015}}. Yale Graduate School of Arts and Sciences.</ref>  He went on to serve at [[Yale]] as associate provost and as an assistant to President [[Kingman Brewster]].<ref>{{cite web|url=http://www.iccnow.org/?mod=fanton|title=Dr. Jonathan Fanton|publisher= [[International Criminal Court]]|accessdate=June 26, 2015}}, International Criminal Court.</ref> He subsequently served as vice president for planning at the [[University of Chicago]].<ref>{{cite web|url=http://artsbeat.blogs.nytimes.com/2014/04/17/american-academy-of-arts-and-sciences-names-new-president/|title=American Academy of Arts and Sciences Names New President|work=New York Times|date=April 17, 2014|accessdate=June 26, 2015}}</ref>

==Academic and nonprofit leadership==
In 1982, Fanton was inaugurated president of [[The New School for Social Research]] in [[New York City]], a leadership position that he held for 17 years.<ref>{{cite web|url=http://articles.chicagotribune.com/2008-06-24/features/0806200367_1_jonathan-fanton-college-presidents-tenure|title=MacArthur Foundation's Fanton to End his Tenure as President|work=Chicago Tribune|date=June 24, 2008|accessdate=June 26, 2015}}</ref> One of his signature accomplishments as president was the reconnection of [[The New School]] to its European roots through assistance provided to dissident scholars in [[Eastern Europe|Eastern]] and [[Central Europe]], many of whom were leaders of [[human rights]] organizations in their home countries.<ref name=gsas /> After becoming president of the [[MacArthur Foundation]] in 1999, he worked to strengthen the organization’s commitment to a variety of issues, including international justice, human rights, peace and security, biodiversity conservation, and community and economic development.<ref name=gsas /><ref>{{cite web|url=http://www.macfound.org/about/our-history/past-presidents/|title=Past Presidents|publisher= [[MacArthur Foundation]]|accessdate=June 26, 2015}}, MacArthur Foundation.</ref> From 2009 to 2014, Fanton was interim director of the [[Roosevelt House Public Policy Institute at Hunter College]].<ref name=gsas /> Since July 1, 2014, he has served as the president of the [[American Academy of Arts and Sciences]], one of the oldest learned societies and independent policy research centers in the [[United States]].<ref name=globe />

==Scholarship==
Fanton is the author of ''Foundations and Civil Society, Volumes I and II'' (MacArthur Foundation, 2008) and ''The University and Civil Society, Volumes I and II'' (New School for Social Research, 1995, 2002). He is also co-editor of ''John Brown: Great Lives Observed'' (Prentice-Hall, 1973) and ''The Manhattan Project: A Documentary Introduction to the Atomic Age'' (McGraw-Hill, 1991).

== References ==
{{Reflist}}

== External links ==
* [http://jonathanfanton.com Jonathan Fanton]
* [https://www.amacad.org American Academy of Arts and Sciences]

{{MacArthur Foundation}}

{{DEFAULTSORT:Fanton, Jonathan}}
[[Category:Articles created via the Article Wizard]]
[[Category:Yale University alumni]]
[[Category:Living people]]
[[Category:1943 births]]
[[Category:People from Trumbull, Connecticut]]
[[Category:People from Weston, Connecticut]]