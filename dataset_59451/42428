{|{{Infobox Aircraft Begin
 | name=Pedaliante
 | image=Pedaliante.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[human-powered aircraft]]
 | national origin=Italy
 | manufacturer=
 | designer=[[Enea Bossi, Sr.|Enea Bossi]]
 | first flight=1936
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''''Pedaliante''''' ([[Italian language|Italian]] for "Pedal Glider") was a [[human-powered aircraft]] designed and built by [[Enea Bossi, Sr.|Enea Bossi]] and [[Vittorio Bonomi]] and credited with, in 1936, making one of the first fully human-powered flights. The aircraft successfully traveled {{convert|1|km|abbr=on}} as part of an [[Italy|Italian]] competition, but was denied the monetary prize due to its catapult launch.

==History==

===Early development===
In 1932, [[Enea Bossi, Sr.|Enea Bossi]] heard of an airplane which had successfully flown while powered only by a {{convert|0.75|kW|hp|abbr=on|2}} engine. This prompted Bossi to calculate the minimum power that a manned aircraft would need to fly. The calculation yielded a value of approximately {{convert|0.70|kW|hp|abbr=on|2}}, which convinced Bossi that [[Human-powered aircraft|human-powered flight]] might be possible.<ref name="Gossamer">{{cite book |last= Grosser |first= Morton |title= Gossamer Odyssey: The Triumph of Human-Powered Flight |publisher= [[Houghton Mifflin]] |year= 1981 |isbn= 0-7603-2051-9}}</ref>

During a trip to [[Philadelphia]], Bossi tested the speed at which a glider would take off under tow. The experiment consisted of hiring a professional bicyclist to tow a [[Glider aircraft|glider]]. A [[spring scale]] was attached to the tow line to sense the force exerted by the bicyclist, the results confirming that a speed at which the necessary lift could be obtained was indeed attainable. This same experimental procedure was later repeated as part of the development of the [[Gossamer Condor]] and the [[Gossamer Albatross]].<ref name="Gossamer"/>

A second experiment conducted during a trip to [[Paris]] involved a [[propeller (aircraft)|propeller]]-driven [[bicycle]] designed by Bossi; the test rider achieved a speed of {{convert|37|km/h|mph|abbr=on}}, but one drawback was noted: the [[Gyroscope|gyroscopic]] effect of the propeller generated so much [[torque]] that the bicycle became unstable. Bossi concluded, erroneously, that a successful human-powered aircraft would therefore require two counter-rotating propellers to cancel out the effects of torque.<ref name="Gossamer"/>

In 1933, the ''[[Polytechnische Gesellschaft]]'' (Polytechnic Society) in [[Frankfurt]] offered a prize to promote human-powered flight. During this period, many [[Nazi Germany|German]] activities were copied by Italy, and in 1936 the [[National Fascist Party|Italian government]] initiated an equivalent contest: offering 100,000 [[Italian lira|lire]] for a {{convert|1|km|abbr=on|2}} human-powered flight made by an Italian citizen. Bossi was aware that he could not receive the prize due to his [[United States nationality law|American citizenship]], but he opted to attempt to win it, anyway.<ref name="Gossamer"/>

===Aircraft design===
Bossi’s [[human-powered aircraft]], named the ''Pedaliante'', utilized conventional glider configuration and construction.<ref name="Roper">{{cite web | url = http://www.humanpoweredflying.propdesigner.co.uk/html/before_1939.html | title = Muscle Assisted Flights Before 1939 | accessdate = 2007-12-06 | publisher = Chris Roper }}</ref> The high-winged [[streamlined]] [[monoplane]] design featured two [[laminate]]d [[balsa wood]] propellers – each approximately {{convert|2|m|abbr=on}} in [[diameter]].<ref name="Gossamer"/><ref name="Roper"/><ref name="RAEShumpowhist">{{cite web | url = http://www.raes.org.uk/cmspage.asp?cmsitemid=SG_hum_pow_history | title = Significant achievements | accessdate = 2007-12-06 | publisher = Royal Aeronautical Society }}</ref> The [[Aircraft flight control system|control surfaces]] consisted of a conventional rear [[rudder]], [[Elevator (aircraft)|elevator]], and a pair of roll [[Spoiler (aeronautics)|spoilers]] on the wings – all activated by a divided control [[Yoke (aircraft)|yoke]]. The pilot sat semi-upright, and a bicycle chain transmitted the power from the pedals to an overhead transverse shaft that was [[bevel]]-geared to the two propellers, which extended from the wing on each side of the [[fuselage]].<ref name="Gossamer"/>

[[Vittorio Bonomi]], an Italian [[Glider (sailplane)|sailplane]] manufacturer, was contracted to build the aircraft. The wooden airframe was originally specified to have an empty weight of {{convert|73|kg|abbr=on|1}}, with an overweight contingency of {{convert|9.1|kg|abbr=on|2}}. While this design would have been feasible, the ''[[Ministero dell' Aeronautica]]'' - (Ministry of Aeronautics) required that the aircraft satisfy the same structural requirements of an engine-powered aircraft, forcing the designer to increase empty weight to nearly {{convert|100|kg|abbr=on|2}}.<ref name="Gossamer"/>

===Flights===
Bossi and Bonomi enlisted [[Emilio Casco]], a [[major]] in the [[Italian Army]] and a very strong bicyclist, to pilot the ''Pedaliante''. After several weeks of trials in early 1936, Casco took off in the ''Pedaliante'' and flew {{convert|91.4|m|abbr=on}} completely under his own power. Although subsequent calculations have verified that this flight was physically possible, most agree that it was Casco’s considerable physical strength and endurance which made the accomplishment feasible; it was not a feat which could be attained by a typical person.<ref name="Gossamer"/>

To improve performance it was decided that additional thrust was necessary; in response, the propellers were increased in diameter to {{convert|2.25|m|abbr=on}}, increasing available thrust. Despite the additional thrust, the team still found the aircraft to be too heavy to travel the {{convert|1|km|abbr=on}} the contest demanded. However, the German ''[[:de: HV-1 Mufli|HV-1 Mufli]]'' <sup>[[German language|(de)]]</sup> (Muskelkraft-Flugzeug), a human-powered aircraft built by Helmut Hässler & [[:de:Franz Villinger|Franz Villinger]] <sup>[[German language|(de)]]</sup>, traveled {{convert|235|m|abbr=on}} on its debut flight in 1935 and attained a distance of {{convert|712|m|abbr=on}} in 1937 utilizing a [[Catapult|tensioned cable launching system]].<ref name="Gossamer"/>

Incorporating a catapult launch to a height of {{convert|9|m|abbr=on}}, the ''Pedaliante'' made a flight on 13 September 1936 which traveled several hundred meters.<ref name="Time">{{cite web | url = http://www.time.com/time/magazine/article/0,9171,883606,00.html | title = Icarus to Bossi | date = 1937-02-08 | accessdate = 2008-02-18 | publisher = Time Magazine }}</ref> On 18 March 1937, at [[Cinisello Balsamo|Cinisello airport]] near [[Milan]], the aircraft was launched at a height of {{convert|9|m|abbr=on}} and Casco successfully pedaled the craft for its full {{convert|1|km|abbr=on}}. This set a world record for human-powered flight, but as a catapult launch was not permitted in the rules of the competition, the ''Pedaliante'' did not win the prize for which it was designed. The aircraft was retired the following year having made a total 80 flights – 43 without the assistance of a catapult launch.<ref name="Canadian">{{cite journal |last= Bossi|first= Enea|date=December 1960|title= A man has flown by his own power in 1937|journal= Canadian Aeronautical Journal |volume= 6 |issue=10 |pages= 395–399}}</ref> At the time, the ''Mufli'' and the ''Pedaliante'' were the most advanced Human-powered aircraft ever built.<ref name="Gossamer"/><ref name="RAEShumpowhist"/>

==Specifications==
{{Aircraft specs
|ref=<ref name="RAEShpag">{{cite web | url = http://www.raes.org.uk/cms/uploaded/files/SG_HPAG_toucan.pdf | title = The Structural Design and Construction of Man Powered Aircraft | date = 2007-04-27 | author = M.S. Pressnell | accessdate = 2007-12-06 | publisher = Royal Aeronautical Society }}</ref><ref name="Gossamer"/><ref name="Roper"/><ref name="RAEShumpowhist"/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=17.7
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=23.4
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=13.4
|airfoil=[[NACA airfoil|NACA 0012-F1]]
|empty weight kg=97
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=</li>
<!--
        Powerplant
-->
|eng1 number=
|eng1 name=
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=2
|prop name=2x [[laminate]]d [[balsa wood]] fixed pitch propellers
|prop dia m=2.25
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=A bicycle chain transmitted the power from the pedals to an overhead transverse shaft which was [[bevel]]-geared to the two propellers, extending from the wing on each side of the [[fuselage]].

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
}}

==References==
{{commons category|Pedaliante}}
{{reflist}}

[[Category:Italian experimental aircraft 1930–1939]]
[[Category:Human-powered aircraft]]
[[Category:Single-engined twin-prop tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Individual aircraft]]
[[Category:Italian inventions]]
[[Category:Aircraft manufactured in Italy]]