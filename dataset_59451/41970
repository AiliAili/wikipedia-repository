{{about|the plant|other uses under leaf vegetables|Spinach (disambiguation)}}
{{taxobox
|name = Spinach
|image = Spinacia oleracea Spinazie bloeiend.jpg
|image_caption = Spinach in flower
|regnum = [[Plantae]]
|unranked_divisio = [[Angiosperms]]
|unranked_classis = [[Eudicots]]
|unranked_ordo = [[Core eudicots]]
|ordo = [[Caryophyllales]]
|familia = [[Amaranthaceae]],<br/> formerly [[Chenopodiaceae]]
|subfamilia = [[Chenopodioideae]]
|genus = ''[[Spinacia]]''
|species = '''''S. oleracea'''''
|binomial = ''Spinacia oleracea''
|binomial_authority = [[Carl Linnaeus|L.]]
}}

'''Spinach''' (''Spinacia oleracea'') is an edible [[flowering plant]] in the family [[Amaranthaceae]] native to [[Central Asia|central]] and [[western Asia]]. Its leaves are eaten as a [[leaf vegetable|vegetable]].

It is an [[annual plant]] (rarely [[biennial plant|biennial]]) growing to {{convert|30|cm|in|0|abbr=on}} tall. Spinach may survive over winter in temperate regions. The [[leaf|leaves]] are alternate, simple, ovate to triangular, and very variable in size from about {{convert|2–30|cm|in|0|abbr=on}} long and {{convert|1–15|cm|in|1|abbr=on}} broad, with larger leaves at the base of the plant and small leaves higher on the flowering stem. The [[flower]]s are inconspicuous, yellow-green, {{convert|3–4|mm|in|1|abbr=on}} in diameter, maturing into a small, hard, dry, lumpy [[fruit]] cluster {{convert|5–10|mm|in|1|abbr=on}} across containing several [[seed]]s.

Common spinach, ''S. oleracea'', was long considered to be in the family [[Chenopodiaceae]], but in 2003, that family was merged into the family Amaranthaceae in the order [[Caryophyllales]]. Within the family [[Amaranthaceae]] ''sensu lato'', Spinach belongs to subfamily [[Chenopodioideae]].

==Etymology==
[[File:Espinac 5nov.JPG|thumb|Spinach plant in November, [[Castelltallat]]]]
The English word "spinach" dates to the late 14th century, and is from ''espinache'' ([[French language|Fr]]. ''épinard''), of uncertain origin. The traditional view derives it from O.Prov. ''espinarc'', which perhaps is via [[Catalan language|Catalan]] ''espinac'', from [[Andalusian Arabic]] {{lang|ar|اسبيناخ}} ''{{transl|ar|ALA|asbīnākh}}'', from Arabic {{lang|ar|السبانخ}} ''{{transl|ar|ALA|al-sabānikh}}'', from [[Persian language|Persian]] {{lang|fa|اسپاناخ}} ''{{transl|fa|ALA|aspānākh}}'', meaning purportedly 'green hand',<ref>Douglas Harper, ''Online Etymological Dictionary'' s.v. [http://www.etymonline.com/index.php?term=spinach ''spinach'']. Accessed 03/07/2010,</ref> but the multiplicity of forms makes the theory doubtful.<ref>{{OED|spinach}}</ref>

==History==
Spinach is thought to have originated in [[ancient Persia]] (modern Iran and neighboring countries). It is not known by whom, or when, spinach was introduced to [[India]], but the plant was subsequently introduced to [[History of China#Ancient China|ancient China]], where it was known as "Persian vegetable" (''bōsī cài''; 波斯菜; present:菠菜). The earliest available record of the spinach plant was recorded in Chinese, stating it was introduced into China via [[Nepal]] (probably in 647 AD).<ref name="aggie-horticulture.tamu.edu">{{cite journal|first=Victor R. |last=Boswell |title=Garden Peas and Spinach from the Middle East". Reprint of "Our Vegetable Travelers |journal=National Geographic Magazine |volume=96 |issue=2 |date=August 1949 |accessdate=March 7, 2010 |url=http://aggie-horticulture.tamu.edu/Plantanswers/publications/vegetabletravelers/peas.html |archivedate=March 23, 2010 |archiveurl=http://web.archive.org/web/20100323061749/http://aggie-horticulture.tamu.edu/plantanswers/publications/vegetabletravelers/peas.html}}</ref>

In AD 827, the [[Saracen]]s introduced spinach to [[Sicily]].<ref>{{cite book|last1=Rolland|first1=Jacques L.|last2=Sherman|first2=Carol|title=The Food Encyclopedia|date=2006|publisher=Robert Rose|location=Toronto|isbn=9780778801504|pages=335–338}}</ref> The first written evidence of spinach in the [[Mediterranean]] was recorded in three 10th-century works: the medical work by al-Rāzī (known as Rhazes in the West) and in two agricultural treatises, one by Ibn Waḥshīyah and the other by Qusṭus al-Rūmī. Spinach became a popular vegetable in the Arab Mediterranean and arrived in Spain by the latter part of the 12th century, where it was called ''{{transl|ar|ALA|raʼīs al-buqūl}}'', 'the [[rais|chieftain]] of [[leafy greens]]'.<ref>{{cite book|last1=Ibn al-ʻAwwām|first1=Yaḥyá ibn Muḥammad|authorlink1=Ibn al-Awwam|title=Kitāb al-Filāḥah|url=https://books.google.com/books?id=daZEAAAAcAAJ&pg=PA160#v=onepage&q&f=false|accessdate=July 30, 2014|chapter=23.8}}</ref> Spinach was also the subject of a special treatise in the 11th century by Ibn Ḥajjāj.<ref>Clifford A. Wright. ''Mediterranean Vegetables: A Cook's ABC of Vegetables and their Preparation in Spain, France, Italy, Greece, Turkey, the Middle East, and North Africa, with More than 200 Authentic Recipes for the Home Cook''. (Boston: Harvard Common Press, 2001). pp. 300-301.</ref>

The prickly-seeded form of spinach was known in [[Germany]] by no later than the 13th century, though the smooth-seeded form was not described until 1552.<ref name="aggie-horticulture.tamu.edu"/>

Spinach first appeared in [[England]] and [[France]] in the 14th century, probably via [[Spain]], and it gained quick popularity because it appeared in early spring, when other vegetables were scarce and when [[Lent]]en dietary restrictions discouraged consumption of other foods. Spinach is mentioned in the first known English cookbook, the ''[[Forme of Cury]]'' (1390), where it is referred to as 'spinnedge' and/or 'spynoches'.<ref>{{cite book|first1=Jacques |last1=Rolland |first2=Carol |last2=Sherma |title=Spinach |work=The Food Encyclopedia: Over 8,000 Ingredients, Tools, Techniques and People |location=Toronto |publisher=Robert Rose |year=2006 |accessdate=March 7, 2010 |url=http://www.canadianliving.com/glossary/spinach.php |archive-url=http://web.archive.org/web/20110724195456/http://www.canadianliving.com/glossary/spinach.php |archive-date=July 24, 2011}}</ref> Smooth-seeded spinach was described in 1552.<ref name="aggie-horticulture.tamu.edu"/>

Spinach was supposedly the favourite vegetable of [[Catherine de' Medici]]. Dishes served on a bed of spinach are known as "Florentine", reflecting Catherine's birth in [[Florence]].<ref>[http://www.whfoods.com/genpage.php?tname=foodspice&dbid=43 Spinach], The George Mateljan Foundation</ref>

During [[World War I]], wine fortified with spinach juice was given to [[Military of France|French soldiers]] weakened by hemorrhage.<ref name="GrieveGrieve1971">{{cite book|author1=Margaret Grieve|author2=Maud Grieve|title=A modern herbal: the medicinal, culinary, cosmetic and economic properties, cultivation and folk-lore of herbs, grasses, fungi, shrubs, & trees with all their modern scientific uses|url=https://books.google.com/books?id=KgfHxvGFHAoC&pg=PA761|accessdate=13 August 2010|date=1 June 1971|publisher=Courier Dover Publications|isbn=978-0-486-22799-3|pages=761–}}</ref>

==Culinary information==

===Nutrition===
{{nutritional value | name=Spinach, raw
| water=91.4 g
| kJ=97
| protein=2.9 g 
| fat=0.4 g 
| carbs=3.6 g 
| fiber=2.2 g
| sugars=0.4 g
| calcium_mg=99
| iron_mg=2.71
| magnesium_mg=79
| phosphorus_mg=49
| potassium_mg=558
| sodium_mg=79
| zinc_mg=0.53
| manganese_mg=0.897
| vitC_mg=28
| thiamin_mg=0.078
| riboflavin_mg=0.189
| niacin_mg=0.724
| vitB6_mg=0.195
| folate_ug=194
| vitA_ug=469
| vitA_iu=9377
| betacarotene_ug=5626
| lutein_ug=12198
| vitE_mg=2
| vitK_ug=483
| source_usda = 1
| note=[http://ndb.nal.usda.gov/ndb/search/list?qlookup=11457&format=Full Link to USDA database entry] 
}}
In a {{convert|100|g|abbr=on}} serving providing only 23 [[calories]], spinach has a high nutritional value, especially when fresh, frozen, steamed, or quickly boiled. It is a rich source (20% or more of the [[Daily Value]], DV) of [[vitamin A]], [[vitamin C]], [[vitamin K]], [[magnesium]], [[manganese]], [[iron]] and [[folate]] (table). Spinach is a good source (10-19% of DV) of the [[B vitamins]] [[riboflavin]] and [[vitamin B6|vitamin B<sub>6</sub>]], [[vitamin E]], [[calcium]], [[potassium]], and [[dietary fiber]].

====Iron====
Spinach, along with other green, leafy vegetables,<ref name="usda">{{cite web|url=https://ndb.nal.usda.gov/ndb/foods/show/3167?fgcd=&man=&lfacet=&count=&max=35&sort=&qlookup=11457&offset=&format=Full&new=&measureby=|title=Raw spinach per 100 g, Full Report from the USDA National Nutrient Database|publisher=US Department of Agriculture, National Nutrient Database for Standard Reference Release 27|date=2014|accessdate=1 February 2015}}</ref> contains an appreciable amount of [[iron]] attaining 21% of the Daily Value in a {{convert|100|g|abbr=on}} amount of raw spinach (table). For example, the [[United States Department of Agriculture]] states that a {{convert|100|g|abbr=on}} serving of cooked spinach contains {{nowrap|3.57 mg}} of iron, whereas a {{convert|100|g|abbr=on}} ground hamburger patty contains {{nowrap|2.49 mg}}.<ref name="usda"/> However, spinach contains iron absorption-inhibiting substances, including high levels of [[oxalate]], which can bind to the iron to form ferrous oxalate and render much of the iron in spinach unusable by the body.<ref name="noonan">{{cite journal|journal=Asia Pac J Clin Nutr|year=1999|volume=8|issue=1|pages=64–74|title=Oxalate content of foods and its effect on humans|authors=Noonan SC, Savage GP|pmid=24393738|url=http://apjcn.nhri.org.tw/server/APJCN/8/1/64.pdf}}</ref> In addition to preventing absorption and use, high levels of oxalates remove iron from the body.<ref name="noonan"/><ref>{{Cite book| title = Nutrition and diet therapy| year = 1997| pages = 229| isbn = 978-0-8151-9273-2| last1 = Williams| first1 =  Sue Rodwell| last2 =  Long     | first2 =  Sara }}</ref>

====Calcium====
Spinach also has a moderate [[calcium]] content which can be affected by [[oxalate]]s, decreasing its absorption.<ref name="noonan"/><ref>{{Cite book| title = Nutrition| url = https://books.google.com/?id=46o0PzPI07YC&printsec=frontcover| year = 2003| page = 474| isbn = 978-0-7637-0765-1| last1 = Insel | first1 = Paul M.| last2 = Turner | first2 = R. Elaine| last3 = Ross | first3 = Don| accessdate = 2009-04-15 }}</ref> The calcium in spinach is among the least bioavailable of food calcium sources.<ref name="noonan"/><ref name=Heaney2006>{{Cite book| url = https://books.google.com/books?id=il8rmEAZoW8C&pg=PA135 | title = Calcium in human health| year = 2006| author = Heaney, Robert Proulx| pages = 135| isbn = 978-1-59259-961-5| accessdate = 2009-04-15 }}</ref> By way of comparison, the human body can absorb about half of the calcium present in [[broccoli]], yet only around 5% of the calcium in spinach.<ref>{{cite book|url=https://books.google.com/books?id=LhSLKVmauGoC&pg=PA404|title=Understanding Nutrition|authors=Whitney E, Rady Rolfes S|publisher=Cengage Learning| date=Jan 1, 2010|isbn=0538734655}}</ref>

===Types of spinach===
A distinction can be made between older varieties of spinach and more modern ones. Older varieties tend to [[Bolting (horticulture)|bolt]] too early in warm conditions. Newer varieties tend to grow more rapidly, but have less of an inclination to run up to seed. The older varieties have narrower leaves and tend to have a stronger and more bitter taste. Most newer varieties have broader leaves and round seeds.

The three basic types of spinach are:
*'Savoy' has dark green, crinkly and curly leaves. It is the type sold in fresh bunches in most supermarkets in the United States. One heirloom variety of savoy is 'Bloomsdale', which is somewhat resistant to bolting. Other common heirloom varieties are 'Merlo Nero' (a mild variety from [[Italy]]) and 'Viroflay' (a very large spinach with great yields).
*Flat- or smooth-leaf spinach has broad, smooth leaves that are easier to clean than 'Savoy'. This type is often grown for canned and frozen spinach, as well as soups, baby foods, and processed foods.  'Giant Noble' is an example variety.
*Semi-savoy is a hybrid variety with slightly crinkled leaves. It has the same texture as 'Savoy', but it is not as difficult to clean. It is grown for both fresh market and processing. 'Tyee Hybrid' is a common semi-savoy.

==Production, marketing, and storage==
{| class="wikitable" style="float:right; clear:left; width:18em;"
|+ Top spinach producing countries - 2014<br />
!Country
!<center>Production <br><center><small> (millions of tonnes)
|-
|<center>{{CHN}} || <center>22.1
|-
|<center>{{USA}} || <center>0.35
|-
|<center>{{JPN}} || <center>0.26
|-
|<center>{{TUR}} || <center>0.21
|-
|<center>{{IDN}} || <center>0.13
|-
|<center>{{FRA}} || <center>0.12
|-
|<center>{{PAK}} || <center>0.10
|-
|-bgcolor="#CCCCCC"
|<center>'''World''' ||<center> '''24.3'''
|-
|colspan=3 |<small>Source: [[FAO|UN Food & Agriculture Organization]], Statistics Division (FAOSTAT)<ref name="faostat14">{{cite web|url=http://www.fao.org/faostat/en/#data/QC|publisher=UN Food & Agriculture Organization|title=Crops/Regions/World List for Production Quantity of Spinach in 2014|year=2016|accessdate=14 December 2016}}</ref>
|}

In 2014, the world total production of spinach was 24.3 million [[tonne]]s, with China alone accounting for 91% of this quantity.<ref name=faostat14/>

The [[Environmental Working Group]] reported spinach is one of the dozen most heavily [[pesticide]]-contaminated produce products.<ref>{{cite web|url=https://www.ewg.org/foodnews/summary.php|title=Highlights of the Dirty Dozen™ for 2016|publisher=Environmental Working Group|date=2016|accessdate=14 December 2016}}</ref> Depending on the soil and location where the spinach is grown, spinach may be high in cadmium contamination.<ref>{{Cite web |url=https://www.atsdr.cdc.gov/toxguides/toxguide-5.pdf |title=ToxGuide for cadmium |publisher=Agency for Toxic Substances and Disease Registry, US Department of Health and Human Services  |location=Atlanta, GA |publication-date=October 2012}}</ref> 

Spinach is sold loose, bunched, packaged fresh in bags, canned, or frozen. Fresh spinach loses much of its nutritional value with storage of more than a few days.<ref>{{cite web | url = http://www.sciencedaily.com/releases/2005/03/050323124809.htm | title=Storage Time And Temperature Effects Nutrients In Spinach | accessdate = 2008-07-05}}</ref> While refrigeration slows this effect to about eight days, spinach will lose most of its folate and carotenoid content, so for longer storage, it is blanched and frozen, cooked and frozen, or canned. Storage in the freezer can be for up to eight months.

Spinach is packaged in air, or in nitrogen gas to extend shelf life. Some packaged spinach is exposed to radiation to kill any harmful bacteria that may be on the leaves. The [[Food and Drug Administration]] approves of irradiation of spinach leaves up to 4.0 [[kilogray]]s; however, using radiation to sanitize spinach is of concern because it may deplete the leaves of their nutritional value.<ref name="bliss">{{cite web|url=http://www.ars.usda.gov/is/pr/2010/100527.htm|title=Nutrient Retention of Safer Salads Explored|publisher=US Department of Agriculture|author=Bliss RM|date=27 May 2010}}</ref> Researchers at the [[Agricultural Research Service]] experimentally tested the concentrations of vitamins C, E, K, B<sub>9</sub>, and four [[carotenoids]] in packaged spinach following irradiation. They found with increasing level of irradiation, four nutrients showed little or no change. Those nutrients include vitamins B<sub>9</sub>, E, K, and the carotenoid neoxanthin.<ref name=bliss/> This study showed the irradiation of packaged spinach to have little or no change to the nutritional value of the crop, and the health benefits of irradiating packed spinach to reduce harmful bacteria seem to outweigh the loss of nutrients.

==In popular culture==
The comics and cartoon character [[Popeye|Popeye the Sailor Man]] has been portrayed since 1931 as having a strong affinity for spinach,<ref>{{Cite journal
  | last = Sutton
  | first = Mike
  | authorlink = Mike Sutton (criminologist)
  | title = SPINACH, IRON and POPEYE:  Ironic lessons from biochemistry and history on the importance of healthy eating, healthy scepticism and adequate citation
  | journal = Internet Journal of Criminology
  | year = 2010
  | page = 15
  | url = http://www.internetjournalofcriminology.com/Sutton_Spinach_Iron_and_Popeye_March_2010.pdf
  | accessdate = 4 November 2011}}</ref> becoming physically stronger after consuming it. The commonly accepted version of events states this portrayal was based on faulty calculations of the iron content.<ref>{{cite web|url=https://www.theguardian.com/technology/2009/dec/08/ec-segar-popeye-google-doodle|title=E.C. Segar, Popeye's creator, celebrated with a Google doodle|last=Gabbatt|first=Adam|date=8 December 2009|publisher=guardian.co.uk|accessdate=5 May 2010}}</ref> In this version, German scientist Emil von Wolff misplaced a decimal point in an 1870 measurement of spinach's iron content, leading to an iron value ten times higher than it should have been, and this faulty measurement was not noticed until the 1930s. This caused the popular misconception that spinach is exceedingly high in iron that makes the body stronger.<ref>{{cite book |title=The Truth about Food |last=Fullerton-Smith |first=Jill |year=2007 |publisher=[[Bloomsbury Publishing]] |location= |isbn= |page=224 |pages= |url=https://books.google.com/books?id=xx_FMVnrxWAC&pg=PA224&dq=spinach+does+not+have+high+iron&hl=en&sa=X&ei=2Pk_T6-_MMqW2QW-guGXCA&ved=0CDwQ6AEwAQ#v=onepage&q=spinach%20does%20not%20have%20high%20iron&f=false |accessdate=February 18, 2012}}</ref>

Criminologist [[Mike Sutton (criminologist)|Mike Sutton]] wrote an article in the ''[[Internet Journal of Criminology]]'', claiming the Popeye and iron link is just another long-standing [[Urban legend|myth]], and spinach was chosen and promoted in Popeye for its vitamin A content alone.<ref>{{Cite journal
  | authorlink = Dr Mike Sutton
  | title = SPINACH, IRON and POPEYE:  Ironic lessons from biochemistry and history on the importance of healthy eating, healthy scepticism and adequate citation
  | journal = Internet Journal of Criminology
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | language =
  | url = http://www.internetjournalofcriminology.com/Sutton_Spinach_Iron_and_Popeye_March_2010.pdf
  | jstor =
  | doi =
  | id =
  | mr =
  | zbl =
  | jfm =
  | accessdate = }}</ref><ref>{{cite web | author = [[Karl Kruszelnicki]] | title = Popeye's spinach story rich in irony | url = http://www.abc.net.au/science/articles/2011/12/06/3384516.htm | publisher = [[Australian Broadcasting Corporation]] | date = 6 December 2011}}</ref>
In the cited article, he also disputes the above — what he calls the 'Spinach Popeye Iron Decimal Error Story (SPIDES)' — due to lack of verifiable sources, although he found a different reference from 1934 reporting 20 times the actual iron content.<ref>{{cite journal|last=Sherman|first=W.C|author2=Elvehjem, Hart |title=Further studies on the availability of iron in biological materials|journal=J. Biol. Chem.|year=1934|volume=107|pages=383–394|url=http://www.jbc.org/content/107/2/383.full.pdf|accessdate=29 January 2014}}</ref> In another article, Sutton distinguishes between the myth of spinach's iron content, which he blames on "bad science", and the myth that the error was caused by a misplaced decimal point.<ref>{{cite web |author-last=Sutton |author-first=M. |author-link=Mike Sutton (criminologist) |year=2010 |title=The Spinach Popeye Iron Decimal Error Myth is Finally Busted |publisher=BestThinking |url=http://www.bestthinking.com/articles/science/chemistry/biochemistry/the-spinach-popeye-iron-decimal-error-myth-is-finally-busted |accessdate=2015-02-01 }}</ref>

==See also==
{{Portal|Food}}
* [[Spinach dip]]
* [[Spinach salad]]
* [[Spinach soup]]
* [[Spinach in the United States]]
* ''[[Tetragonia tetragonioides]]''

==References==
{{reflist|30em}}

==Further reading==
* {{cite conference |author1=D. Maue |author2=S. Walia |author3=S. Sahore |author4=M. Parkash |author5=S. K. Walia |author6=S. K. Walia | title=Prevalence of Multiple Antibiotic Resistant Bacteria in Ready-to-Eat Bagged Salads | booktitle=American Society for Microbiology meeting. June 5–9 | year=2005 | pages=Atlanta}} [http://www.abstractsonline.com/viewer/viewAbstractPrintFriendly.asp?CKey={C411486E-C86E-478F-A13C-1C04C6802182}&SKey={647E37D8-104B-4973-9287-9D0CF1AA7E5A}&MKey={382D7E47-BE0B-4BBA-B3A6-E511C92FA999}&AKey={32093528-52DC-4EBE-9D80-29DAD84C92CE} Abstract]
* Rogers, Jo. ''What Food is That?: and how healthy is it?''. The Rocks, Sydney, NSW: Lansdowne Publishing Pty Ltd, 1990. ISBN 1-86302-823-4.
* Cardwell, Glenn. ''Spinach is a Good Source of What?''. The Skeptic. Volume 25, No 2, Winter 2005. Pp 31–33. ISSN 0726-9897
* Blazey, Clive. ''The Australian Vegetable Garden: What's new is old''. Sydney, NSW: New Holland Publishers, 1999. ISBN 1-86436-538-2
* Stanton, Rosemary. ''Complete Book of Food and Nutrition''. Australia, Simon & Schuster, Revised Edition, 1995. ISBN 0-7318-0538-0

==External links==
{{Cookbook}}
{{Commons category|Spinacia oleracea}}
* [http://www.factfish.com/statistic/spinach%2C%20production%20quantity Statistics about the worldwide spinach production]
* {{Cite EB1911|wstitle=Spinach}}

{{Authority control}}

[[Category:Amaranthaceae]]
[[Category:Flora of Nepal]]
[[Category:Leaf vegetables]]
[[Category:Plants described in 1753]]