{{Infobox Television episode
| title        = The Snuke
| series       = South Park
| image        = [[Image:1104 24layout.jpg|240px]]
| caption      = Mayhem around South Park, shown with a split screen, ''[[24 (TV series)|24]]''-style. From top left: Hillary Clinton gets hospitalized, Kyle investigates the motive of a terrorist threat, CIA helicopters land in South Park, and Bahir plays checkers with Butters.
| season       = 11
| episode      = 4
| airdate      = March 28, 2007
| production   =
| writer       = Trey Parker
| director     = [[Trey Parker]]
| guests       =
| episode_list = [[South Park (season 11)|''South Park'' (season 11)]]<br/>[[List of South Park episodes|List of ''South Park'' episodes]]
| season_list  =
| prev         =[[Lice Capades]]
| next         =[[Fantastic Easter Special]]
}}

"'''The Snuke'''" is the fourth episode of the eleventh season (or the 157th episode overall) of [[Comedy Central]]'s animated comedy series ''[[South Park]]''. It is largely a parody of the television series ''[[24 (TV series)|24]] ''and was originally broadcast on March 28, 2007.  The episode is rated  [[TV-MA]].

When a new Muslim student joins the class, Cartman immediately suspects him of being a terrorist. After finding out that [[Hillary Clinton]] is scheduled to campaign in town that day, he informs the [[CIA]] and does some research. The CIA shows up and detects the scent of a bomb deep within Mrs. Clinton. Through complex networking and investigating, the source of the bomb is found and the day is saved.

==Plot==
The episode begins when a new student enrolls in [[Ms. Garrison]]'s class. He is named Bahir Hassan Abdul Hakeem, a child of [[Muslim]] parents whose mere presence makes [[Cartman]] paranoid to the point that he leaves class, asking Ms. Garrison whether Bahir has been searched for bombs. Angered by this, Ms. Garrison tells Cartman to stop being so intolerant, stating that not all Muslims resort to terrorism, but Cartman states that while not all Muslims are terrorists, most of them are. Suspecting that the Muslim kid and his parents are involved in a terrorist attack, Cartman calls [[Kyle Broflovski|Kyle]] (who is at home sick), on his cell phone during recess, and Cartman asks him to do a web search for Bahir's background. Cartman also asks Kyle to see if there are important events that day, and figures that Bahir may target [[Hillary Clinton]], who is in town for a [[Hillary Rodham Clinton presidential campaign, 2008|political rally]]. Cartman takes this as a terrorist threat, then proceeds to call the [[Central Intelligence Agency|CIA]], stubbornly claiming that he will only speak directly to the President.

A short while later, the school is evacuated via a [[Fire alarm notification appliance|fire alarm]] and announcement from Principal Victoria. Bahir goes with Butters to hang out, and Butters starts to accept Bahir as a friend. As that is happening, the CIA calls Clinton's convoy to warn them of a possible threat. They decide to continue the rally, and as she is doing so, her security finds that there is a nuclear device in Hillary Clinton's [[vagina]]. This was referred to as a "Snuke" (a [[suitcase nuke]] designed to fit in a woman's "snizz") in Clinton's "snatch". To try to locate the detonator, Cartman tortures Bahir's parents by farting in their faces. Cartman gets no response and once he hears that Bahir is at [[Butters Stotch|Butters]]' house, Cartman runs off.

While Cartman attempts to accost Bahir while running away from Butters' house, a group of [[Russia]]n [[communism|neo-soviets]] abduct both Cartman and Bahir, the former for alerting the CIA to the attempted terrorist attack. While they threaten their prisoners, their conversation reveals that the Russians who placed the snuke are merely pawns in service of America's oldest rival - the [[United Kingdom|British]]. The Russians are a distraction while an 18th-century style fleet of British wooden sailing vessels make a surprise attack to "put an end to the [[American Revolution]]". After Kyle, [[Stan Marsh|Stan]], [[United States Department of Homeland Security|Homeland Security]], the [[Federal Bureau of Investigation|FBI]], [[Bureau of Alcohol, Tobacco, Firearms and Explosives|ATF]], the Secret Service, and a single NSA representative take over Kyle's bedroom (and end up relieving each other of duty in the space of a few minutes), they all work towards finishing what Kyle and Stan had started: uncovering the terrorists' intentions, finishing just as Cartman finds out about the plan himself. They raid the mercenaries, but the Russians then warn the federal agents that the detonator is set to go off when the clock reaches 1:00. However, the power is cut and the clock is reset, blinking 12:00 repeatedly once the power comes back on. The various American federal agents open fire on the Russian terrorists and free Cartman and Bahir. Meanwhile, the [[United States Air Force]] attacks and effortlessly destroys the British fleet. Upon hearing the news of the attack's failure from the fleet's leader, [[Elizabeth II|the Queen]] commits [[suicide]].

Back in South Park, Kyle tells everybody that the moral of the experience was that one should not be suspicious of just one race of people, "because actually, [[anti-Americanism|most of the world hates America]]." Even though Cartman is now convinced that Bahir is innocent, he refuses to apologize for falsely implicating him, pointing out that if he had not suspected Bahir due to his religion he would have never called Kyle, and the actual terrorism plot would not have been solved. Therefore, he concludes, "[[racism]] and [[bigotry]] saved America," so no apology needs to be made at all. As Kyle tries to explain Cartman's conclusion is not the point, Bahir's parents show up and announce that they are leaving the country due to Cartman's torture. Cartman proudly responds: "Ok, who got rid of the Muslims, huh? That was all me."

== Reception ==
{{Anchor|Critics}}
{{expand section|date=January 2015}}
[[IGN]] gave the episode a score of 9 out of 10, judging the episode "amazing".<ref> South Park: "The Snuke" Review. Parker and Stone mentions in the commentary of "The Snuke" that the actual cast and crew of ''24'' watched the episode as it aired and were so delighted, they sent over one of the suitcase nuke props from the show.

The following takes place between recess and geography class...
March 29, 2007
by Travis Fickett
http://tv.ign.com/articles/776/776961p1.html
[[IGN]]
[[News Corporation]]
</ref>

== References ==
{{Reflist}}

==External links==
* [http://www.southparkstudios.com/full-episodes/s11e04-the-snuke The Snuke] Full episode at South Park Studios
* [http://www.southparkstudios.com/guide/episodes/s11e04-the-snuke The Snuke] Episode guide at South Park Studios
* {{IMDb episode|0983725}}
* {{Tv.com episode|1000649}}

{{South Park episodes|11}}
{{24 (TV series)}}

{{DEFAULTSORT:Snuke, The}}
[[Category:South Park (season 11) episodes]]
[[Category:24 (TV series)]]
[[Category:Cultural depictions of Hillary Clinton]]
[[Category:Cultural depictions of Elizabeth II]]
[[Category:Islam-related television episodes]]
[[Category:Terrorism in fiction]]
[[Category:United States presidential election, 2008 in popular culture]]
[[Category:Islamophobia in the United States]]