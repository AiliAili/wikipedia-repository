{{Use mdy dates|date=July 2015}}
{{Infobox astronaut
| name          = Kenneth D. Cameron
| other_names   = Kenneth Donald Cameron
| image         = Kenneth Cameron.jpg
| type          = [[NASA]] [[Astronaut]]
| nationality   = American
| status        = Retired
| birth_date    = {{Birth date and age|1949|11|29}}
| birth_place   = [[Cleveland, Ohio]], U.S.
| death_date    =
| death_place   =
| alma_mater    = [[Massachusetts Institute of Technology|MIT]], B.S. 1978, M.S. 1979<br />[[Michigan State University|MSU]], MBA 2002
| occupation    = [[Naval aviator]], [[test pilot]]
| current_occupation =
| previous_occupation =
| rank          = [[Colonel (United States)|Colonel]], [[United States Marine Corps|USMC]]
| space_time    = 23d 10h 11m
| selection     = [[List of astronauts by selection#1984|1984 NASA Group 10]]
| missions      = [[STS-37]], [[STS-56]], [[STS-74]]
| insignia      = [[File:Sts-37-patch.png|40px]] [[File:Sts-56-patch.png|40px]] [[File:Sts-74-patch.png|40px]]
| retirement    = December 2008
| awards        = [[File:Dfc-usa.jpg|20px|link=Distinguished Flying Cross (United States)]]
}}
{{Portal|Software Testing}}

'''Kenneth Donald Cameron''' (born November 29, 1949), ([[Colonel (United States)|Col]], [[United States Marine Corps|USMC]], Ret.), is a retired American [[United States naval aviator|naval aviator]], [[test pilot]], [[engineer]], [[United States Marine Corps|U.S. Marine Corps officer]], and [[NASA]] [[astronaut]].

==Background==
Cameron was born November 29, 1949, in [[Cleveland, Ohio]]. He graduated from [[Rocky River High School (Ohio)|Rocky River High School]], [[Rocky River, Ohio]], in 1967. He went on to attend the [[Massachusetts Institute of Technology]], receiving a [[Bachelor of Science]] degree in [[Aeronautics]] and [[Astronautics]] in 1978, and a [[Master of Science]] degree in Aeronautics and Astronautics in 1979.<ref name=bio>[http://www.spacefacts.de/bios/astronauts/english/cameron_kenneth.htm Spacefacts Biography of Kenneth Cameron]. ''Spacefacts''. Retrieved July 18, 2011.</ref> He completed numerous courses in [[Russian language]] and [[Russians|Russian]] [[space technology|space systems]] at MIT, the [[Lyndon B. Johnson Space Center]], and at the [[Gagarin Cosmonaut Training Center]], [[Moscow, Russia]]. He later earned a [[Master of Business Administration]] degree from [[Michigan State University]] in 2002. He enjoys [[Aviation|flying]], [[Athletics (physical culture)|athletics]], [[woodworking]], [[Reading (process)|reading]], [[shooting]], motorcycle riding, and [[amateur radio]].<ref name=bio />

He was a [[Boy Scouting (Boy Scouts of America)|Boy Scout]] and earned the rank of [[Ranks in the Boy Scouts of America#Star|Star Scout]].<ref>[http://www.scouting.org/filestore/pdf/02-558.pdf Kenneth D. Cameron at scouting.org]</ref>

==Military and flight experience==
Cameron was commissioned in the U.S. Marine Corps in 1970 at [[Officer Candidate School (U.S. Marine Corps)|Officer Candidate School]] at [[Marine Corps Base Quantico]], [[Virginia]]. After graduating from [[The Basic School]] and [[Vietnamese language]] school, he was assigned to the [[South Vietnam|Republic of Vietnam]] for a one-year [[tour of duty]] as an [[Platoon Commander#United States organization|infantry platoon commander]] with the [[1st Battalion, 5th Marines]] and later with the [[Marine Security Guards]] at the [[United States Embassy, Saigon|U.S. Embassy in Saigon]]. Upon his return to the United States, he served as [[executive officer]] with India Company, [[3rd Battalion, 2nd Marines]], at [[Marine Corps Base Camp Lejeune]], [[North Carolina]]. He reported to [[Naval Air Station Pensacola]], [[Florida]], in 1972 for flight training, receiving his [[Naval Aviator]] [[United States Aviator Badge|wings]] in 1973. He was then assigned to [[VMA-223]] at [[Marine Corps Air Station Yuma]], [[Arizona]], flying [[A-4 Skyhawk|A-4M Skyhawks]].

In 1976, Cameron was reassigned to the [[Massachusetts Institute of Technology]], where he participated in the Marine College Degree and Advanced Degree Programs. Upon graduation, he was assigned to flying duty for one year with [[Marine Aircraft Group 12]] at [[Marine Corps Air Station Iwakuni]], Japan. He was subsequently assigned to the [[Pacific Missile Test Center]] in 1980, and in 1982 to the [[U.S. Naval Test Pilot School]] at [[Naval Air Station Patuxent River]], [[Maryland]]. Following graduation in 1983, he was assigned as project officer and [[test pilot]] in the [[F/A-18 Hornet]], A-4, and [[OV-10 Bronco]] [[airplane]]s with the Systems Engineering Test Directorate at the [[Naval Air Test Center]].

He has logged over 4,000 hours flying time in 48 different types of aircraft.

==NASA career==
Selected by NASA in May 1984, Cameron became an astronaut in June 1985. His technical assignments have included work on [[Space tether|Tethered Satellite]] Payload, flight [[software]] testing in the [[Shuttle Avionics Integration Laboratory]] (SAIL), launch support activities at [[Kennedy Space Center]], and spacecraft communicator ([[CAPCOM]]) in [[Mission Control]] for [[STS-28]], [[STS-29|29]], [[STS-30|30]], [[STS-33|33]] and [[STS-34|34]].  Cameron's management assignments in NASA include Section Chief for astronaut software testing in SAIL, astronaut launch support activities, and Operations Assistant to the [[Hubble Space Telescope|Hubble]] Repair Mission Director. In 1994, Cameron served as the first NASA Director of Operations in [[Star City, Russia]], where he worked with the [[Gagarin Cosmonaut Training Center]] staff to set up a support system for astronaut operations and training in Star City, and received Russian training in [[Soyuz spacecraft|Soyuz]] and [[Mir]] spacecraft systems, and flight training in Russian [[Aero L-39|L-39]] aircraft.

He was absent from NASA between 1996 and 2003. However, following the [[Space Shuttle Columbia disaster|loss of the Space Shuttle ''Columbia'' and her crew]], Cameron returned to the space program in October 2003, taking a founding position as a Principal Engineer in the NASA Engineering & Safety Center, based at the NASA [[Langley Research Center]], in [[Hampton, Virginia]]. In June 2005, Cameron was selected as Deputy Director for Safety of the NESC, and in June 2007 he was relocated to the NESC office at the Johnson Space Center in Houston.

A veteran of three space flights, Cameron has logged over 561 hours in [[Outer space|space]]. He served as Pilot on [[STS-37]] (April 5–11, 1991), and was the spacecraft commander on [[STS-56]] (April 9–17, 1993) and [[STS-74]] (November 12–20, 1995).

===Spaceflight experience===
Cameron flew his first mission as Pilot on [[STS-37]]. This mission was launched on April 5, 1991, and featured the deployment of the [[Gamma Ray]] Observatory for the purpose of exploring gamma ray sources throughout the [[universe]]. [[Space Shuttle Atlantis|Space Shuttle ''Atlantis'']] landed on April 11, 1991. On his second mission, he was spacecraft commander on [[STS-56]], carrying ATLAS-2. During this nine-day mission, the crew of Space Shuttle ''[[Space Shuttle Discovery|Discovery]]'' conducted [[Earth's atmosphere|atmospheric]] and [[Sun|solar]] studies in order to better understand the effect of solar activity on the [[Earth]]’s [[climate]] and [[natural environment|environment]], and deployed and retrieved the [[Wiktionary:autonomy|autonomous]] observatory ''Spartan''. STS-56 launched on April 8, 1993, and landed at Kennedy Space Center on April 17, 1993. On his third mission, Cameron commanded ''Atlantis'' on [[STS-74]], NASA’s second [[Space Shuttle]] mission to rendezvous and dock with the Russian [[space station]] [[Mir]], and the first mission to use the Shuttle to assemble a module and attach it to a space station. STS-74 launched on November 12, 1995, and landed at Kennedy Space Center on November 20, 1995.

==Post-NASA career==
Following his first NASA retirement on August 5, 1996, he joined Hughes Training, Inc., a subsidiary of [[General Motors]] Corporation, as [[Executive Director]] of Houston Operations. In September 1997, Cameron transferred to [[Saab Automobile]], AB, in [[Sweden]], as Vehicle Line Executive for the [[Saab 9-3]] [[automobile]]. He was called "Kalle Comet" by his co workers at Saab. Upon his return to the United States, Cameron worked at the [[General Motors Technical Center]] in [[Warren, Michigan]], near [[Detroit]], in positions in World Wide Purchasing, Supplier Technology Acquisition, [[Research and Development]], and [[Fuel Cell]] Vehicle Development.

Cameron retired from NASA for good in December 2008 to join [[Northrop Grumman]] Corporation as the Director of Houston Operations for Northrop Grumman Aerospace Systems. Cameron currently owns and flies a [[Cozy MK IV]], an experimental airplane of composite fiberglass construction with [[Canard (aeronautics)|canard design]] configuration and pusher propeller.

==Awards and honors==
* [[Defense Superior Service Medal]]
* [[Legion of Merit]]
* [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] (2)
* [[Navy Commendation Medal]] with Combat "V"
* [[Combat Action Ribbon]]
* NASA Leadership Medal
* [[NASA Exceptional Achievement Medal]]
* [[NASA Space Flight Medal]]s (3)
* [[List of military decorations#South Vietnam (former)|Vietnamese]] Meritorious Unit Citation
* Admiral Louis de Flores Award (MIT)
* [[Charles Stark Draper Laboratory]] Fellowship
* [[Marine Corps Association]] Leadership Sword

==References==
{{reflist|colwidth=80}}

{{NASA|article=Astronaut Bio: K. Cameron|url=http://www.jsc.nasa.gov/Bios/htmlbios/cameron.html}}

==External links==
{{Commons category}}
{{Portal|Biography|United States Marine Corps|Spaceflight}}
* [http://www.jsc.nasa.gov/Bios/htmlbios/cameron.html Cameron's official NASA biography]
* [http://www.astronautix.com/astros/cameron.htm Astronautix biography of Kenneth D. Cameron]
* [http://www.spacefacts.de/bios/astronauts/english/cameron_kenneth.htm Spacefacts biography of Kenneth D. Cameron]
* [http://www.spaceacts.com/STARSHIP/seh/cameron.html Cameron at Spaceacts]

{{NASA Astronaut Group 10}}
{{US Marine Corps navbox}}

{{Use American English|date=January 2014}}

{{DEFAULTSORT:Cameron, Kenneth D.}}
[[Category:1949 births]]
[[Category:Living people]]
[[Category:1991 in spaceflight]]
[[Category:1993 in spaceflight]]
[[Category:1995 in spaceflight]]
[[Category:American astronauts]]
[[Category:American military personnel of the Vietnam War]]
[[Category:American aviators]]
[[Category:American test pilots]]
[[Category:Aviators from Ohio]]
[[Category:Massachusetts Institute of Technology alumni]]
[[Category:American engineers]]
[[Category:Michigan State University alumni]]
[[Category:Military personnel from Cleveland]]
[[Category:United States Naval Test Pilot School alumni]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Defense Superior Service Medal]]
[[Category:Recipients of the NASA Exceptional Achievement Medal]]
[[Category:United States Marine Corps astronauts]]
[[Category:United States Marine Corps officers]]
[[Category:United States Naval Aviators]]
[[Category:NASA people]]
[[Category:Northrop Grumman people]]
[[Category:American businesspeople]]
[[Category:American business executives]]
[[Category:20th-century American businesspeople]]