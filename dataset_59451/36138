{{Infobox racing car
| Car_name = Allard J2X-C
| Image = 
| Category = [[Group C]]
| Constructor = [[Allard]]
| Designer = [[Hayden Burvill]]<br>[[John Iley]] (aerodynamics)
| Predecessor = 
| Successor = 
| Team = [[Bob Pond Racing]]
| Drivers = [[Robs Lamplough]]
| Technical ref = <ref name="tech1"/>
| Chassis = [[Carbon-fibre]] and [[aluminium]] [[honeycomb]] two-piece [[monocoque]]
| Front suspension = [[Double wishbone suspension]], with [[push-rod]] actuated [[coil springs]] over [[Shock absorber|dampers]]
| Rear suspension = [[Double wishbone suspension]], with [[push-rod]] actuated [[coil springs]] over [[Shock absorber|dampers]]
| Length = {{convert|4799|mm|in|1|abbr=on}}
| Width = {{convert|2000|mm|in|1|abbr=on}}
| Height = {{convert|920|mm|in|1|abbr=on}}
| Wheelbase = {{convert|2850|mm|in|1|abbr=on}}
| Track = {{convert|1620|mm|in|1|abbr=on}} front<br>{{convert|1582|mm|in|1|abbr=on}} rear
| Engine name = [[Nicholson McLaren Engines]]-prepared [[Cosworth DFR]],
| Capacity = {{convert|3494|cc|cuin|1|abbr=on}}
| Configuration = 32 valve, [[twin-cam|DOHC]] [[V8 engine|V8]],
| Turbo/NA = [[naturally-aspirated]]
| Engine position = [[mid-engine]]d, [[longitudinal engine|longitudinally mounted]]<ref name="tech2"/>
| Gearbox name = Modified [[Leyton House]]-[[March Engineering]]
| Gears = 6-speed
| Type = [[sequential manual transmission|sequential manual]]
| Differential = 
| Weight = {{convert|860|kg|lb|1|abbr=on}}
| Fuel = 
| Lubricants = 
| Tyres = [[BF Goodrich]]<br>[[Goodyear Tire and Rubber Company|Goodyear]]
| Debut = 1993 [[IMSA GTP Championship]] [[Laguna Seca Raceway]]
| Races = 1 (3 entries)
| Wins = 0
| Cons_champ = 0
| Drivers_champ = 0
| Teams_champ = 0
| Poles = 0
| Fastest_laps = 0
}}

The '''Allard J2X-C''', or the '''Allard J2X''' as it is sometimes referred to, was a [[Group C]] [[sports racing car]] built by [[Allard]] in 1992 for use in international [[sports car racing]] events. It featured a 3.5-litre [[Cosworth DFR]] [[V8 engine]], capable of producing around {{convert|580|hp|kW PS|1|abbr=on}}. The J2X-C had bodywork that is more reminiscent of modern [[Le Mans Prototype]]s than a conventional Group C car, but the engine proved too weak for the level of downforce, and this, coupled with the fact that Allard Holdings were liquidated during the car's development, severely restricted the J2X and prevented it ever reaching its potential. One car was built.

==Development==
In the 1980s, Chris Humberstone, who had a history of designing cars for various [[Formula One]] teams, licensed the rights to the [[Allard]] name from Alan Allard, the son of the company's founder, [[Sidney Allard|Sidney]].<ref name="racecar engineering story">{{cite journal |title=Hindsight: Evolution theory |magazine=Racecar Engineering |date=July 2005 | |volume=15 |first=Mark |last=Fuller}}</ref> After a few years of wrangling, the company hired [[Hayden Burvill]] from [[Brun Technics]] to begin developing the J2X-C.<ref name="racecar engineering story"/> He was joined in 1991 by [[John Iley]], who was hired as the aerodynamicist, and the car was designed to have as little frontal area as possible, giving it a unique look.<ref name="racecar engineering story"/> Although it was originally planned to use a [[Chevrolet small block engine|Chevrolet small block]]-derived [[V8 engine]],<ref name="racecar engineering story"/> the car was instead fitted with a 3.5-litre [[Cosworth DFR]] V8 engine, derived from a Formula One engine, which produced about {{convert|580|hp|kW PS|1|abbr=on}} of power and {{convert|400|lbft|Nm kgm|1|abbr=on}} of [[torque]].<ref name="tech1">{{cite web |url=http://www.mulsannescorner.com/allardj2x.html |title=1992–1993 Allard J2X |publisher=Mulsanne's Corner |accessdate=12 September 2013 |first=Mark |last=Fuller}}</ref> The gearbox was also from an F1 car: a [[Leyton House]]-[[March Engineering]] 6-speed [[sequential manual transmission]] modified for endurance racing.<ref name="tech1"/> This transmission would prove to be problematic throughout the car's lifetime.<ref name="racecar engineering story"/>

The J2X-C used [[double wishbone suspension]], with [[push-rod]] actuated [[coil springs]] over [[Shock absorber|dampers]] at both ends of the car;<ref name="tech2">{{cite web |url=http://www.ultimatecarpage.com/spec/3532/Allard-J2X-C.html |title=1992 – 1993 Allard J2X-C Specifications |publisher=Ultimatecarpage.com |date=22 February 2008 |accessdate=12 September 2013 |first=Wouter |last=Melissen}}</ref> the front suspension was mounted on the [[carbon-fibre]] [[monocoque]], whilst the rear suspension was mounted to a carbon-fibre sub-structure that had been designed to allow quick transmission replacement.<ref name="supercars.net">{{cite web |url=http://www.supercars.net/cars/3666.html |title=Allard J2X-C |publisher=Supercars.net |accessdate=12 September 2013}}</ref> The car's radical bodywork generated a high amount of downforce; it was calculated to give approximately {{convert|5500|lb|kg|0|abbr=on}} of downforce at {{convert|150|mph|km/h|0|abbr=on}}, and {{convert|9778|lb|kg|0|abbr=on}} at {{convert|200|mph|km/h|0|abbr=on}}.<ref name="racecar engineering story"/> However, some of the more conventional cars were able to match this level of downforce; the works [[Toyota TS-010]]s had a claimed maximum downforce of over {{convert|9500|lb|kg|0|abbr=on}}, for example.<ref name="racecar engineering story"/> It was, however, higher than the works [[Nissan R91CP]], which had a claimed maximum of {{convert|6438|lb|kg|0|abbr=on}} at {{convert|200|mph|km/h|0|abbr=on}},<ref>{{cite web |url=http://www.mulsannescorner.com/aerodatabasenissanr91cp.html |title=1991 Nissan R91CP – Race Car Aerodynamics Database |publisher=Mulsanne's Corner |accessdate=3 January 2014 |first=Michael J. |last=Fuller}}</ref> whilst the 1993 [[Joest Racing|Joest]]-[[Porsche 962C]] had a claimed maximum of {{convert|5584|lb|kg|0|abbr=on}} at {{convert|200|mph|km/h|0|abbr=on}}.<ref>{{cite web |url=http://www.mulsannescorner.com/aerodatabasejoestporsche962.html |title=1993 Joest-Porsche 962 – Race Car Aerodynamics Database |publisher=Mulsanne's Corner |accessdate=3 January 2014 |first=Michael J. |last=Fuller}}</ref>

==Racing history==
[[Terai Engineering]] attempted to enter the J2X-C at the [[500 km of Suzuka]] in April 1992, but the car was far from ready, and did not attend.<ref>{{cite web |url=http://www.racingsportscars.com/results/Suzuka-1992-04-19-3460.html |title=500 km Suzuka 1992 – Race Results |publisher=Racing Sports Cars |accessdate=12 September 2013}}</ref> The J2X-C was first tested on 9 July 1992, with [[Costas Los]] selected to drive it at [[Pembrey Circuit]].<ref name="racecar engineering story"/> He said of the car; "''the J2X felt very different to a regular Group C car... Contrary to most Group C cars I had driven, it was a lot more tuneable than I was accustomed to.''"<ref name="racecar engineering story"/> However, he did state that the car's lack of [[power-assisted steering]] was a problem.<ref name="racecar engineering story"/> The team struggled to find a buyer for the car, as the Group C era was drawing to a close by 1992 and 1993; the [[IMSA GTP Championship]] was on its last legs, and the [[World Sportscar Championship]] was beginning to shift away from the category.<ref name="racecar engineering story"/>

With the J2X-C far from being completely developed, Allard Holdings were liquidated in the first quarter of 1993, and the car was sold to [[Robs Lamplough]] for £76,000.<ref name="racecar engineering story"/> The car's lack of straightline speed, due to a combination of undeveloped aerodynamics, the high level of downforce, and the low power of the engine, would restrict the car's racing career even further.<ref name="racecar engineering story"/> After Lamplough had bought the car, he ran the car in the test session for the [[1993 24 Hours of Le Mans]]; however, he was only able to finish 19th overall, and last in the car's category, even lapping slower than four of the GT cars.<ref>{{cite web |url=http://www.racingsportscars.com/results/Le_Mans-1993-05-16.html |title=Le Mans Test 1993 |publisher=Racing Sports Cars |accessdate=12 September 2013}}</ref> The car was clocked at just {{convert|172|mph|km/h|0|abbr=on}} down the [[Mulsanne Straight]],<ref name="supercars.net"/> and this led to Lamplough opting to not run in the [[24 Hours of Le Mans]] race.<ref name="racecar engineering story"/> Instead, Lamplough debuted the car, with assistance from [[Bob Pond Racing]], at the ninth round of the IMSA GTP Championship, held at [[Laguna Seca Raceway]]; ninth place overall, and last in the GTP category, was the best Lamplough could do with the car.<ref>{{cite web |url=http://www.racingsportscars.com/results/Laguna_Seca-1993-07-25-3033.html |title=Laguna Seca 1993 – Race Results |publisher=Racing Sports Cars |accessdate=12 September 2013}}</ref> The car never raced again.<ref name="racecar engineering story"/>

==Later history and legacy==
Lamplough held onto the J2X-C for a while, but eventually sold the car, which then passed through the hands of several owners before ending up in Canada.<ref name="racecar engineering story"/> Although even conventional rivals such as the [[Toyota TS-010]] were able to develop more downforce, the J2X-C was far from the end of its development, and various other companies had considered developing a similar style of car.<ref name="racecar engineering story"/> Most manufacturers considered the radical bodywork just too great a risk, as [[Spice Engineering]]'s lead designer, [[Graham Humphries]], stated; "''With limited resources, it was decided instead to follow the more conventional route of further developing what we knew.''"<ref name="racecar engineering story"/> However, [[Le Mans Prototypes]] of the early 2000s and beyond, such as the [[Audi R8 (race car)|Audi R8]], the [[Lola B01/60]] and the [[Lola B05/40]] have all been said to use some of the lessons learned in the J2X-C.<ref name="racecar engineering story"/> The J2X-C was, as of 2008, in running order having been fully restored, and ran in the 2007 [[Goodwood Festival of Speed]], as part of the Group C 25th Anniversary celebration.<ref>{{cite web |url=http://www.ultimatecarpage.com/txt/3532/2/Allard-J2X-C.html |title=1992 – 1993 Allard J2X-C – Images, Specifications and Information, Page 2 |publisher=Ultimatecarpage.com |date=22 February 2008 |accessdate=12 September 2013 |first=Wouter |last=Melissen}}</ref>

==References==
{{Reflist|2}}

{{Good Article}}

[[Category:Group C cars]]
[[Category:Allard Motor Company vehicles|J2X-C]]
[[Category:IMSA GTP cars]]