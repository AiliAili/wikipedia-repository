{{lowercase title}}
The '''cGAS–STING pathway''' is a component of the [[innate immune system]] that functions to detect the presence of [[cytosol]]ic [[DNA]] and, in response, trigger expression of inflammatory genes. DNA is normally found in the [[cell nucleus|nucleus]] of the cell. Localization of DNA to the [[cytosol]] is associated with [[carcinogenesis|tumorigenesis]] or [[virus|viral]] infection. The cGAS – STING pathway acts to detect cytosolic DNA and induce an immune response.

Upon binding DNA, the protein cyclic GMP-AMP Synthase ([[Cyclic GMP-AMP synthase|cGAS]]) triggers reaction of GTP and ATP to form cyclic GMP-AMP (cGAMP). cGAMP binds to Stimulator of Interferon Genes ([[Stimulator of interferon genes|STING]]) which triggers phosphorylation of [[IRF3]] via [[TANK-binding kinase 1|TBK1]]. IRF3 can then go to the nucleus to trigger [[Transcription (genetics)|transcription]] of inflammatory genes. This pathway plays a critical role in mediating immune defense against double-stranded DNA [[virus]]es.

The innate immune system relies on [[germline]] encoded [[pattern recognition receptor]]s (PRRs) to recognize distinct [[pathogen-associated molecular pattern|PAMPs]]. Upon recognition of a PAMP, PRRs generate signal cascades leading to transcription of genes associated with the immune response. Because all pathogens utilize [[nucleic acid]] to propagate, DNA and [[RNA]] can be recognized by PRRs to trigger immune activation. In normal cells, nucleic acid is confined to the [[cell nucleus|nucleus]] or [[mitochondrion|mitochondria]]. The presence of nucleic acid in the cytosol is indicative of cellular damage or infection and leads to activation genes associated with the immune response. One way cytosolic nucleic acid is sensed is via the cGAS/STING pathway. Cytosolic DNA is recognized by cyclic-GMP-AMP synthase (cGAS). Upon DNA recognition cGAS dimerizes and stimulates the formation of cyclic-GMP-AMP (cGAMP). cGAMP then binds directly to stimulator of interferon genes (STING) which triggers phosphorylation/activation of the transcription factor IRF3 via TBK1. IRF3 is able to enter the nucleus to promote transcription of inflammatory genes, such as [[Interferon type I|IFN-β]].

==Cyclic GMP-AMP synthase (cGAS)==

===Structure===
cGAS is a 522 [[amino acid]] protein and a member of the [[nucleotidyltransferase]] family. N-terminal residues 1-212 are necessary to bind dsDNA. This region may contain two different DNA binding domains. C-terminal residues 213-522 contain part of the nucleotidyltransferase (NTase) motif and a Mab21 domain and are highly conserved in cGAS from zebrafish to humans. These regions are necessary to form the catalytic pocket for the cGAS substrates: [[Guanosine triphosphate|GTP]] and [[Adenosine triphosphate|ATP]], and to perform the necessary cyclization reaction.<ref>Filiz Civril,	Tobias Deimling,	Carina C. de Oliveira Mann,	Andrea Ablasser,	Manuela Moldt, Gregor Witte,	Veit Hornung	& Karl-Peter Hopfner. "Structural mechanism of cytosolic DNA sensing by cGAS". Nature 498, 332–337 (20 June 2013). doi:10.1038/nature12305. PMID 24077100</ref><ref>Yang Li1, Jinfeng Lu, Yanhong Han, Xiaoxu Fan, Shou-Wei Ding. "RNA Interference Functions as an Antiviral Immunity Mechanism in Mammals". Science 11 October 2013:  Vol. 342 no. 6155 pp. 231-234  DOI: 10.1126/science.1241911. PMID 24115437</ref><ref>Philip J. Kranzusch, Russell E. Vance. "cGAS Dimerization Entangles DNA Recognition". Immunity Volume 39, Issue 6, 12 December 2013, Pages 992–994. PMID 24332024</ref>

===Function===
cGAS is found in the cytosol and is responsible for detecting cytosolic double stranded DNA, normally found in the cell nucleus, in order to stimulate production of IFN-β. Upon directly binding cytosolic DNA, cGAS forms dimers to catalyze production of [[Cyclic guanosine monophosphate–adenosine monophosphate|2’3’-cGAMP]] from ATP and GTP. cGAMP then acts a second messenger, binding to STING, to trigger activation of the transcription factor IRF3. IRF3 leads to transcription of type-1 IFN-β. cGAS is unable to produce 2’3’-cGAMP in the presence of RNA.

===Discovery===
Prior to the discovery of cGAS, it was known that interferon beta was produced in the presence of cytosolic dsDNA and that STING-deficient cells were unable to produce interferon in the presence of dsDNA. Through biochemical fractionation of cell extracts and quantitative mass spectrometry, Sun, et al.<ref>Sun L1, Wu J, Du F, Chen X, Chen ZJ. "Cyclic GMP-AMP synthase is a cytosolic DNA sensor that activates the type I interferon pathway". Science. 2013 Feb 15;339(6121):786-91. doi: 10.1126/science.1232458. PMID 23258413</ref> identified cGAS as the DNA-sensing protein able to trigger interferon beta by synthesizing the second messenger, 2’3’-cGAMP. This activity is dependent on cytosolic DNA.

===Enzymatic activity===
cGAS catalyzes formation of cGAMP in the presence of dsDNA. cGAS directly binds dsDNA via positively charged amino acid residues interacting with the negatively charged DNA phosphate backbone. Mutations in the positively charged residues completely abrogate DNA binding and subsequent interferon production through STING. Upon binding dsDNA, cGAS dimerizes and undergoes conformational changes that open up a catalytic nucleotide binding pocket, allowing [[Guanosine triphosphate|GTP]] and [[Adenosine triphosphate|ATP]] to enter. Here they are stabilized through base stacking, hydrogen bonds, and divalent cations in order to catalyze phosphodiester bond formation to produce the cyclic dinucleotide cGAMP.

==Cyclic GMP-AMP (cGAMP)==

===Structure===
Cyclic GMP-AMP (cGAMP) is a cyclic dinucleotide (CDN) and the first to be found in metazoans. Other CDNs (c-di-GMP and c-di-AMP) are commonly found in bacteria, archaea, and protozoa. As the name suggests, cGAMP is cyclic molecule composed of one Adenine monophosphate (AMP) and one Guanine monophosphate (GMP) connected by two phosphodiester bonds. However, cGAMP differs from other CDNs in that it contains a unique phosphodiester bonds between the 2’ OH of GMP and the 5’ phosphate of AMP.<ref name="Diner">Elie J. Diner, Dara L. Burdette, Stephen C. Wilson, Kathryn M. Monroe, Colleen A. Kellenberger, Mamoru Hyodo, Yoshihiro Hayakawa, Ming C. Hammond, and Russell E. Vance."The innate immune DNA sensor cGAS produces a non-canonical cyclic-di-nucleotide that activates human STING". Cell Rep. 2013 May 30; 3(5): 1355–1361. doi:  10.1016/j.celrep.2013.05.009. PMID 23707065</ref> The other bond is between the 3’ OH of AMP and the 5’ phosphate of GMP. The unique 2’-5’ phosphodiester bond may be advantageous because it is less susceptible to degradation caused by 3’-5’ phosphodiesterases. Other advantages of the unique 2’-5’ linkage may be that cGAMP is able to bind multiple allelic variants of STING found in the human population, while other CDNs, composed of only 3’-5’ linkages, are not.

[[File:CGAS bound to dsDNA.png|thumb|cGAS bound to dsDNA. Adapted from PDB 4O6A <ref>http://www.rcsb.org/pdb/explore.do?structureId=4O6A</ref>]]

===Discovery===
cGAMP was discovered by James Chen and colleagues <ref>Jiaxi Wu1, Lijun Sun, Xiang Chen, Fenghe Du, Heping Shi, Chuo Chen, Zhijian J. Chen. "Cyclic GMP-AMP Is an Endogenous Second Messenger in Innate Immune Signaling by Cytosolic DNA". Science 15 February 2013:  Vol. 339 no. 6121 pp. 826-830  DOI: 10.1126/science.1229963. PMID 23258412</ref> by collecting cytoplasmic extracts from cells [[transfection|transfected]] with different types of DNA. Cellular extracts were assayed for STING activation by detecting activated IRF3 dimers. Using [[affinity chromatography|affinity purification chromatography]], the STING activating substance was purified and mass spectrometry was used to identify the substance as cyclic-GMP-AMP (cGAMP).

Chemically synthesized cGAMP was shown to trigger IRF3 activation and IFN-β production. cGAMP was found to be much more potent than other cyclic di-nucleotides (c-di-GMP and c-di-AMP). cGAMP was shown to definitively bind STING by using radiolabeled cGAMP [[cross-link]]ed to STING. Adding in unlabeled cGAMP, c-di-GMP, or c-di-AMP was found to compete with radio-labeled cGAMP, suggesting that CDN binding sites overlap. It was later shown that cGAMP has a unique 2’-5’ phosphodiester bond, which differs from conventional 3’-5’ linked CDNs and that this bond may explain some of the unique signaling properties of cGAMP.<ref name="Diner" />

==Stimulator of Interferon Genes (STING)==

STING  is an [[endoplasmic reticulum]] resident protein and has been shown to directly bind to a variety of different cyclic-di-nucleotides.<ref name="Diner" />
 
===Expression===
 
STING is expressed broadly in numerous tissue types, of both immune and non-immune origin.<ref>http://www.genecards.org/cgi-bin/carddisp.pl?gene=TMEM173#expression</ref> STING was identified in murine embryonic fibroblasts, and is required for the type 1 interferon response in both immune and non-immune cells.<ref>Hiroki Ishikawa1 Zhe Ma & Glen N. Barber. "STING regulates intracellular DNA-mediated, type I interferon-dependent innate immunity". Nature 461, 788-792 (8 October 2009) | doi:10.1038/nature08476. PMID 19776740</ref>

===Structure===
 
STING is a 378 amino acid protein. Its N-terminal region (residues 1-154) contains four trans-membrane domains. Its C-terminal domain contains the dimerization domain, the cyclic dinucleotide interaction domain, as well as a domain responsible for interacting and activating TBK1. Upon binding of 2’-3’ cGAMP, STING undergoes a significant conformational change (approximately 20 Angstrom inward rotation) that encloses cGAMP.

[[File:STING bound to cGAMP.png|thumb|STING bound to cGAMP. While STING exists as a homodimer, only one subunit is shown to highlight the interaction of side chain residues with cGAMP. Adapted from PDB 4KSY <ref>http://www.rcsb.org/pdb/explore.do?structureId=4KSY</ref>]]

===Function===
 
Upon binding of 2’-3’ cGAMP (and other bacterial CDNs), STING activates TBK1 to phosphorylate downstream transcription factors IRF3, which induces the type 1 IFN response, and [[STAT6]], which induces [[chemokine]]s such as [[CCL2]] and [[CCL20]] independently of IRF3.<ref>Dara L. Burdette,	Kathryn M. Monroe,	Katia Sotelo-Troha,	Jeff S. Iwig,	Barbara Eckert, Mamoru Hyodo,	Yoshihiro Hayakawa	& Russell E. Vance. "STING is a direct innate immune sensor of cyclic di-GMP". Nature 478, 515–518 (27 October 2011) doi:10.1038/nature10429. PMID 21947006</ref> STING is also thought to activate the [[NF-κB]] [[transcription factor]] through the activity of the [[IκB kinase]] (IKK), though the mechanism of NF-κB activation downstream of STING remains to be determined. The signaling pathways activated by STING combine to induce an innate immune response to cells with ectopic DNA in the cytosol. Loss of STING activity inhibits the ability of [[fibroblast|mouse embryonic fibroblasts]] to fight against infection by certain viruses, and more generally, is required for the type 1 IFN response to introduced cytosolic DNA.<ref>Hiroki Ishikawa1 Zhe Ma & Glen N. Barber. "STING regulates intracellular DNA-mediated, type I interferon-dependent innate immunity". Nature 461, 788-792 (8 October 2009) | doi:10.1038/nature08476. PMID 19776740</ref>

STING’s general role as an adapter molecule in the cytosolic DNA-type 1 IFN response across cell types has been suggested to function through [[dendritic cell]]s (DCs). DCs link the innate immune system with the adaptive immune system through [[phagocytosis]] and [[MHC class II|MHC]] presentation of foreign antigen. The type 1 IFN response initiated by DCs, perhaps through recognition of phagocytosed DNA,<ref>Woo SR, Fuertes MB, Corrales L, Spranger S, Furdyna MJ, Leung MY, Duggan R, Wang Y, Barber GN, Fitzgerald KA, Alegre ML, Gajewski TF. "STING-dependent cytosolic DNA sensing mediates innate immune recognition of immunogenic tumors.". Immunity. 2014 Nov 20;41(5):830-42. doi: 10.1016/j.immuni.2014.10.017.</ref> has an important co-stimulatory effect. This has recently led to speculation that 2’-3’ cGAMP could be used as a more efficient and direct adjuvant than DNA to induce immune responses.

===Allelic Variation===
 
Naturally occurring variations in human STING (hSTING) have been found at amino acid position 232 (R232 and H232). H232 variants have diminished type 1 IFN responses <ref>Cai X, Chiu YH, Chen ZJ. "The cGAS-cGAMP-STING pathway of cytosolic DNA sensing and signaling.". Mol Cell. 2014 Apr 24;54(2):289-96. doi: 10.1016/j.molcel.2014.03.040. PMID 24766893</ref> and mutation at this position to alanine abrogates the response to bacterial CDNs. Substitutions enhancing ligand binding were also found. G230A substitutions were shown to increase hSTING signaling upon c-di-GMP binding. This residue is found on the lid of the binding pocket, possibly increasing c-di-GMP binding ability.<ref>Guanghui Yi, Volker P. Brendel, Chang Shu, Pingwei Li, Satheesh Palanathan, C. Cheng Kao. "Single Nucleotide Polymorphisms of Human STING Can Affect Innate Immune Response to Cyclic Dinucleotides". PLoS ONE October 21, 2013DOI: 10.1371/journal.pone.0077846. PMID 24204993</ref>

==Biological Importance of the cGAS-STING pathway==

===Role in viral response===

The cGAS-cGAMP-STING pathway is able to generate interferon beta in response to cytosolic DNA. It was shown that DNA viruses, such as HSV-1, are able to trigger cGAMP production and subsequent activation of interferon beta via STING . RNA viruses, such as [[vesicular stomatitis virus|VSV]] or [[Sendai virus]], are unable to trigger interferon via cGAS-STING. cGAS or STING defective are unable to produce interferon in response to HSV-1 infection which eventually leads to death, while mice with normal cGAS and STING function are able to recover.
 
Retroviruses, such as [[Subtypes of hiv|HIV-1]], were also shown to activate IFN via the cGAS/STING pathway. In these studies, inhibitors of retroviral reverse transcription abrogated IFN production, suggesting that it is the viral cDNA which is activating cGAS.<ref>Daxing Gao, Jiaxi Wu, You-Tong Wu, Fenghe Du, Chukwuemika Aroh, Nan Yan, Lijun Sun, Zhijian J. Chen. "Cyclic GMP-AMP Synthase Is an Innate Immune Sensor of HIV and Other Retroviruses". Science 23 August 2013: Vol. 341 no. 6148 pp. 903-906 DOI: 10.1126/science.1240933. PMID 23929945</ref>

===Role in tumor surveillance===

The cGAS/STING pathway also has a role in tumor surveillance. In response to cellular stress, such as DNA damage, cells will upregulate [[NKG2D]] ligands so that they may be recognized and destroyed by [[natural killer cell|Natural Killer]] (NK) and [[T cell]]s. In many tumor cells, the DNA damage response is constitutively active, leading to the accumulation of cytoplasmic DNA. This activates the cGAS/STING pathway leading to activation of IRF3. It was shown in lymphoma cells that the NKG2D ligand, Rae1, was upregulated in a STING/IRF3 dependent manner. Transfection of DNA into these cells also triggered Rae1 expression that was dependent on STING. In this model, the transcription factor IRF3, via cGAS/STING, upregulates stress-induced ligands, such as Rae1, in tumor cells, so as to aid in NK-mediated tumor clearance <ref>Adeline R. Lam, Nina Le Bert
, Samantha S.W. Ho
, Yu J. Shen1,2, Melissa L.F. Tang
, Gordon M. Xiong
,
J. Ludovic Croxford
, Christine X. Koo, Ken J. Ishii, Shizuo Akira
, David H. Raulet
, and Stephan Gasser. "RAE1 Ligands for the NKG2D Receptor Are Regulated by
STING-Dependent DNA Sensor Pathways in Lymphoma". Cancer Research  DOI: 10.1158/0008-5472.CAN-13-1703. PMID 24590060</ref>

===Role in autoimmune disease===

Cytoplasmic DNA, due to viral infection, can lead to activation of interferon beta to help clear the infection. However, chronic activation of STING, due to host DNA in the cytosol, can also activate the cGAS/STING pathway, leading to autoimmune disorders. An example of this occurs in [[Aicardi–Goutières syndrome]] (AGS). Mutations in the 3’ repair exonuclease, TREX1, cause endogenous retroelements to accumulate in the cytosol, which can lead to cGAS/STING activation, resulting in IFN production. Excessive IFN production leads to an over-active immune system, resulting in AGS and other immune disorders. In mice, it was found that autoimmune symptoms associated with TREX1 deficiency were relieved by cGAS, STING, or IRF3 knockout, implying the importance of aberrant DNA sensing in autoimmune disorders<ref>Daxing Gao, Jiaxi Wu, You-Tong Wu, Fenghe Du, Chukwuemika Aroh, Nan Yan, Lijun Sun, Zhijian J. Chen. "Cyclic GMP-AMP Synthase Is an Innate Immune Sensor of HIV and Other Retroviruses". Science 23 August 2013: Vol. 341 no. 6148 pp. 903-906 DOI: 10.1126/science.1240933</ref>

===Therapeutic role===

DNA has been shown to be a potent adjuvant to boost the immune response to antigens encoded by vaccines. cGAMP, through IRF3 activation of STING, stimulates transcription of interferon. This makes cGAMP a potential vaccine adjuvant capable of boosting inflammatory responses.<ref name="Diner" /> Studies have shown that vaccines encoded with the chicken antigen, [[ovalbumin]] (OVA), in conjunction with cGAMP, were able to activate antigen-specific T and B cells in a STING-dependent manner in vivo. When stimulated with OVA peptide, the T cells from mice vaccinated with OVA + cGAMP were shown to have elevated IFN-g and [[Interleukin 2|IL-2]] when compared to animals receiving only OVA.<ref>Xiao-Dong Li1, Jiaxi Wu, Daxing Gao, Hua Wang, Lijun Sun, Zhijian J. Chen1. "Pivotal Roles of cGAS-cGAMP Signaling in Antiviral Defense and Immune Adjuvant Effects". Science 20 September 2013: Vol. 341 no. 6152 pp. 1390-1394 DOI: 10.1126/science.1244040. PMID 23989956</ref> Furthermore, the enhanced stability of cGAMP, due to the unique 2’-5’ phosphodiester bond, may make it a preferred adjuvant to DNA for in vivo applications.

==References==
{{reflist}}

{{DEFAULTSORT:cGAS - STING cytosolic DNA sensing pathway, The}}
[[Category:DNA]]
[[Category:Immune system]]