{{Infobox journal
| cover = [[File:Psychoneuroendocrinology.gif]]
| title = Psychoneuroendocrinology
| abbreviation = Psychoneuroendocrinology
| editor = [[Robert Dantzer]],
[[Ned H. Kalin]]| discipline = [[Psychoneuroendocrinology]]
| publisher = [[Elsevier]] on behalf of the [[International Society of Psychoneuroendocrinology]]
| frequency = Monthly
| history = 1976-present
| impact = 4.944
| impact-year = 2014
| website = http://www.journals.elsevier.com/psychoneuroendocrinology/
| link1 = http://www.psyneuen-journal.com/current
| link1-name = Online access
| link2 = http://www.psyneuen-journal.com/issues
| link2-name = Online archive
| ISSN = 0306-4530
| OCLC = 644285842
| CODEN = PSYCDE
}}
'''''Psychoneuroendocrinology''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] covering research in [[psychoneuroendocrinology]] published by [[Elsevier]]. It is an official journal of the [[International Society of Psychoneuroendocrinology]] and was established in 1976. The [[editors-in-chief]] are [[Robert Dantzer]] ([[University of Texas MD Anderson Cancer Center]]) and [[Ned H. Kalin]] ([[University of Wisconsin School of Medicine and Public Health]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Chemical Abstracts]]
* [[Current Contents]]/Life Sciences
* [[EMBASE]]
* [[Elsevier BIOBASE]]
* [[MEDLINE]]
* [[PsycINFO]]
* [[PsycLIT]]
* [[Science Citation Index]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 4.944.<ref name=WoS>{{cite book |year=2015 |chapter=Psychoneuroendocrinology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/psychoneuroendocrinology/}}

[[Category:Elsevier academic journals]]
[[Category:Endocrinology journals]]
[[Category:Neuroscience journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1976]]
[[Category:English-language journals]]