{{for|the later Duke of Croatia|Álmos, Duke of Croatia}}
{{distinguish|Almış}}
{{Infobox royalty
| title          =[[Kende]] or [[gyula (title)|gyula]] of the [[Hungarians]]
| name =Álmos
| full name =
| image          =   Picta.jpg
| caption =Álmos depicted in the ''[[Illuminated Chronicle]]''
| succession    =
| reign         =c. 850&nbsp;– c. 895
| coronation    =
| predecessor   =[[Levedi]] (?)
| successor     =[[Árpád]]
| spouse         =
| issue          =[[Árpád]]
| house    =[[House of Árpád]]
| father         =[[Ügyek]] or [[Előd]]
| mother         =[[Emese]]
| birth_date  =c. 820
| birth_place =
| death_date  =c. 895 (aged 75)
| death_place =[[Transylvania]] (debated)
| place of burial=
| religion = [[Hungarian mythology|Hungarian Paganism]]|}}
'''Álmos''' ({{IPA-hu|ˈaːlmoʃ}}), also '''Almos'''{{sfn|Spinei|2003|p=33}} or '''Almus''',{{sfn|Kirschbaum|1995|p=40}} (c. 820{{ndash}}c. 895) was{{spaced ndash}}according to the uniform account of Hungarian chronicles{{spaced ndash}}the first head of the "loose federation"{{sfn|Kirschbaum|1995|p=38}} of the [[Hungarian tribes]] from around 850. Whether he was the [[Sacred king|sacred ruler]] (''[[kende]]'') of the Hungarians, or their military leader ''([[Gyula (title)|gyula]])'' is subject to scholarly debate. He apparently accepted the [[Khazar Khaganate|Khazar]] [[khagan]]'s suzerainty in the first decade of his reign, but the Hungarians acted independently of the Khazars from around 860. The 14th-century ''[[Illuminated Chronicle]]'' narrates that he was murdered in Transylvania at the beginning of the [[Hungarian conquest of the Carpathian Basin]] around 895.

== Ancestry ==
{{Further|Előd|Emese|Ügyek}}
[[Anonymus (chronicler)|Anonymus]], the unknown author of the ''Gesta Hungarorum''{{spaced ndash}}who wrote his "historical romance"{{sfn|Róna-Tas|1999|p=59}} around 1200 or 1210{{sfn|Kristó|Makk|1996|p=9}}{{spaced ndash}}states that Álmos descended "from the line"<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 5), p. 17.</ref> of [[Attila the Hun]].{{sfn|Engel|2001|p=19}}{{sfn|Spinei|2003|p=54}} A late 13th-century chronicler, [[Simon of Kéza]] wrote that Álmos was "of the [[Turul]] kindred".<ref>''Simon of Kéza: The Deeds of the Hungarians'' (ch. 2.27), p. 81.</ref>{{sfn|Spinei|2003|p=54}} He also wrote of Attila the Hun's banner, which bore "the image of the bird the Hungarians call ''turul''"<ref>''Simon of Kéza: The Deeds of the Hungarians'' (ch. 1.10), p. 43.</ref>{{spaced ndash}}identified as either a [[gyrfalcon]] or a hawk.{{sfn|Engel|2001|p=19}} A bird has an important role in the legend about Álmos's birth, which was preserved both by the ''Gesta Hungarorum'' and by the ''Illuminated Chronicle''.{{sfn|Kristó|Makk|1996|p=10}} The legend says that Álmos's mother, already pregnant with him, dreamed of a bird of prey "which had the likeness of a hawk"<ref>''The Hungarian Illuminated Chronicle'' (ch. 25), p. 98.</ref> impregnating her.{{sfn|Kristó|Makk|1996|pp=10-11}} Historians Gyula Kristó{{sfn|Kristó|Makk|1996|p=10}} and [[Victor Spinei]] wrote that this story, which has close analogies in Turkic folklore, initially narrated the origin of Álmos's family from a totemic ancestor.{{sfn|Spinei|2003|p=54}}

According to the ''Gesta Hungarorum'', Álmos was born to [[Emese]], a daughter of  "Prince Eunedubelian".{{sfn|Kristó|Makk|1996|p=9}} However, Kristó writes that her name, containing the old Hungarian word for mother ''(em)'', may have been invented by Anonymus.{{sfn|Kristó|Makk|1996|p=9}} The name of Álmos's father is likewise uncertain because the Hungarian chronicles preserved it in two variants.{{sfn|Kristó|Makk|1996|p=9}} Anonymus states that [[Ügyek]] was his name,<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 3), pp.&nbsp;12–13.</ref> but the 14th-century ''Illuminated Chronicle'' says that [[Előd]]{{spaced ndash}}himself the son of Ügyek{{spaced ndash}}was Álmos's father.{{sfn|Kristó|Makk|1996|p=9}} Kristó says that both names may have been the chroniclers' inventions, since Ügyek's name derives from the ancient Hungarian ''ügy'' ("saint, holy") word, and Előd's name simply refers to an ancestor.{{sfn|Kristó|Makk|1996|p=9}} Anonymus writes that Ügyek married Emese in 819.{{sfn|Kristó|Makk|1996|p=9}} If this date is correct, Álmos was born around 820.{{sfn|Kristó|Makk|1996|p=10}}

Although Anonymus makes a connection between the name of Álmos and the Hungarian word for dream ''(álom)'', many historians, including András Róna-Tas{{sfn|Róna-Tas|1999|p=227}} and Victor Spinei,{{sfn|Spinei|2003|p=33}} argue that his name is of [[Turkic languages|Turkic]] origin. If the latter theory is correct, it has a meaning of "the bought one".{{sfn|Kristó|1996|p=166}} Álmos's family may have also been of Turkic stock, but according to Victor Spinei, a name's etymology does not always reflect its bearer's ethnicity.{{sfn|Spinei|2009|p=353}}

{{Quote|''In the year of Our Lord's incarnation 819, Ügek&nbsp;... took to wife in [[Dentumoger]] the daughter of Prince Eunedubelian, called Emese, from whom he begot a son, who was named Álmos. But he is called Álmos from a divine event, because when she was pregnant a divine vision appeared to his mother in a dream in the form of a falcon that seemed to come to her and impregnate her and made known to her that from her womb a torrent would come forth and from her loins glorious kings be generated, but that they would not multiply in their own land. Because a dream is called'' álom ''in the [[Hungarian language]] and his birth was predicted in a dream, so he was called Álmos. Or he was called Álmos, that is holy, because holy kings and dukes were born of his line.''|Anonymus: ''[[Gesta Hungarorum]]''<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 3), pp.&nbsp;13–15.</ref>}}

== Reign ==
Álmos, according to ''Gesta Hungarorum'', was freely elected by the heads of the seven [[Magyar tribes|Hungarian tribes]] as their "leader and master".<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 5), p. 17.</ref>{{sfn|Kristó|Makk|1996|p=11}}{{sfn|Engel|2001|p=19}} Anonymus adds that to ratify Álmos's election, the seven chiefs "swore an oath, confirmed in pagan manner with their own blood spilled in a single vessel".<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 5), p. 17.</ref>{{sfn|Engel|2001|p=19}} Anonymus says that they also adopted the basic principles of the government, including the hereditary right of Álmos's offsprings to his office and the right of his electors' descendant to have a seat in the prince's council.{{sfn|Engel|2001|p=19}} According to author Pál Engel, this report of the "treaty by blood" ({{lang-hu|vérszerződés}}), which reflects its authors' political philosophy rather than actual events, was "often presented by Hungarian historians as the very first manifestation of modern parliamentary thinking in Europe" up until 1945.{{sfn|Engel|2001|p=19}}

In a sharply contrasting narrative from around 950, the [[Byzantine Emperor]] [[Constantine VII Porphyrogenitus]] states that instead of Álmos, his son Árpád was the first supreme head of the Hungarian tribes, and that Árpád's election was initiated by the Khazar khagan.{{sfn|Spinei|2003|p=33}}{{sfn|Kristó|Makk|1996|p=12}} The emperor says the khagan sent an envoy to the ''"[[voivodes]]"'' (heads of the Hungarian tribes)<ref>''Constantine Porphyrogenitus: De Administrando Imperio'' (ch. 38), p. 171.</ref> after they had been forced by the [[Pechenegs]] to leave their dwelling places near the Khazar Khaganate and to settle in a new territory called ''[[Etelköz]]''.{{sfn|Kristó|Makk|1996|p=12}} The khagan was planning to appoint one of the ''voivodes'' named [[Levedi]] to lead the Hungarian tribes{{sfn|Spinei|2003|p=33}} to represent the khagan's interests.{{sfn|Kristó|Makk|1996|p=12}} Although Levedi refused the khagan's offer, he proposed one of his peers, Álmos or Álmos's son [[Árpád]], to the proposed new position.{{sfn|Kristó|Makk|1996|p=12}}{{sfn|Spinei|2003|p=33}} The khagan accepted Levedi's offer. Upon his initiative the Hungarians elected their first prince, but they preferred Árpád to his father.{{sfn|Kristó|Makk|1996|p=13}}{{sfn|Spinei|2003|p=33}}

Gyula Kristó and many other historians refute Porphyrogenitus's report of the omission of Álmos in favor of his son, saying that the ''turul'' legend connected to Álmos's birth proves his role as forefather of his dynasty.{{sfn|Kristó|Makk|1996|p=13}}{{sfn|Kristó|1996|p=165}} These historians say that the emperor's account is based on a report by one of Árpád's descendants named Termacsu, who emphasized by this report of Árpád's election that only those descending from Árpád were suitable to lead the Hungarians; other children of Álmos were excluded.{{sfn|Kristó|Makk|1996|p=13}} András Róna-Tas says that Constantine Porphyrogenitus preserved the memory of a ''coup d'état'' organized against Levedi ''kende'' by Álmos ''gyula'', who had his own son Árpád elected as sacred ruler in his opponent's place.{{sfn|Róna-Tas|1999|p=330}} A late 9th-century Central Asian scholar, [[Abu Abdallah al-Jayhani]]{{spaced ndash}}whose works were partially preserved in [[Ibn Rusta]]'s and other Muslim authors' books{{spaced ndash}}mentions the existence of these two high offices among the Hungarians.{{sfn|Kristó|Makk|1996|p=14}}{{sfn|Kristó|1996|p=104-105}} He describes the ''kende'' as the Hungarians' sacred ruler and the ''gyula'' as their military commander.{{sfn|Kristó|Makk|1996|p=14}} Historians still debate which of the two offices was held by Álmos.{{sfn|Kristó|Makk|1996|p=14}}{{sfn|Engel|2001|p=19}}{{sfn|Róna-Tas|1999|p=330}}

{{Quote|''The chagan said to'' [Levedi]'': "We have invited you upon this account, in order that, since you are noble and wise and valorous and first among the'' [Hungarians]'', we may appoint you prince of your nation, and you may be obedient to our word and our command." But he, in reply, made answer to the chagan: "Your regard and purpose for me I highly esteem and express to you suitable thanks, but since I am not strong enough for this rule, I cannot obey you; on the other hand, however, there is a voivode other than me, called'' [Álmos]'', and he has a son called'' [Árpád]''; let one of these, rather, either that'' [Álmos] ''or his son'' [Árpád]'', be made prince, and be obedient to your word." That chagan was pleased at this saying, and gave some of his men to go with him, and sent them to the'' [Hungarians]'', and after they had talked the matter over with the'' [Hungarians]'', the'' [Hungarians] ''preferred that'' [Árpád] ''should be prince rather than'' [Álmos] ''his father, for he was of superior parts and greatly admired for wisdom and counsel and valour, and capable of this rule; and so they made him prince according to the custom, or 'zakanon', of the Chazars, by lifting him upon a shield.''|[[Constantine Porphyrogenitus]]: ''[[De Administrando Imperio]]''<ref>''Constantine Porphyrogenitus: De Administrando Imperio'' (ch. 38), p. 173.</ref>}}

Kristó says that Álmos stood at the head of the Hungarian tribal confederation from around 850.{{sfn|Kristó|1996|p=166}} Porphyrogenitus's narration says that he initially accepted the khagan's suzerainty.{{sfn|Kristó|Makk|1996|p=14}} The Hungarians apparently achieved their independence around 860, since the earliest reports on their plundering raids in Central Europe were recorded thereafter.{{sfn|Kristó|Makk|1996|p=14}} The ''[[Annals of St. Bertin]]'' mentions their incursion into [[Louis the German]]'s realm in 862.{{sfn|Kristó|1996|p=133}} Three tribes seceding from the Khazar Khaganate, together known by Porphyrogenitus as ''"[[Kabars|Kabaroi]]"'',<ref>''Constantine Porphyrogenitus: De Administrando Imperio'' (ch. 40), p. 175.</ref> also joined with the Hungarians in the 860s or 870s.{{sfn|Kristó|1996|p=148}} Spinei says that the memory of their arrival was preserved by Anonymus, who mentions "the seven dukes of the [[Cumans]]" who "subjected themselves to Prince Álmos" at Kiev.<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 10), p. 29.</ref>{{sfn|Spinei|2003|p=51}}

Anonymus writes of a war between the Hungarians and the [[Kievan Rus]]', ending with the victory of the Hungarians, who were commanded by Álmos.{{sfn|Spinei|2003|p=42}} The ''[[Russian Primary Chronicle]]'' refers to a "Hungarian hill"<ref>''Russian Primary Chronicle'' (years 880-882), p. 61.</ref> at Kiev in connection with the town's occupation by [[Oleg of Novgorod]] in 882.{{sfn|Spinei|2003|p=42}} The same chronicle mentions "a castle of Ol'ma" ''(Олъминъ дворъ)'' standing on the same hill.{{sfn|Kristó|1996|p=133}} [[George Vernadsky]] says that this fortress had been named after Álmos, but this theory has not been widely accepted by historians.{{sfn|Kristó|1996|p=133}}

== Death ==
{{Further|Hungarian conquest of the Carpathian Basin}}
[[File:Honfoglalas.gif|thumb|alt=The Hungarian Conquest|The "[[Hungarian conquest of the Carpathian Basin|Hungarian land-taking]]"]]
The Hungarians who lived in the westernmost parts of the [[Pontic steppes]] were occasionally hired by neighboring powers to intervene in their wars.{{sfn|Spinei|2003|p=51}} For instance, they invaded [[Great Moravia|Moravia]] in alliance with [[Arnulf of Carinthia|Arnulf of East Francia]] in 892.{{sfn|Spinei|2003|p=51}}{{sfn|Kirschbaum|1995|p=29}} Their intervention in a conflict between the [[First Bulgarian Empire]] and the [[Byzantine Empire]] caused a joint counter-invasion by the [[Bulgars]] and [[Pechenegs]].{{sfn|Spinei|2003|pp=51-52}} The Hungarians were forced to leave the Pontic steppes and to cross the Carpathians in search of a new homeland around 895.{{sfn|Kirschbaum|1995|pp=39-40}}{{sfn|Spinei|2003|pp=52-55}}

According to the ''Gesta Hungarorum'', the Hungarians invaded the Carpathian Basin under Álmos, who "appointed his son, Árpád, as leader and master"<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 13), p. 37.</ref> of the Hungarian tribal federation at [[Ungvár]] (Uzhhorod, [[Ukraine]]).{{sfn|Kristó|Makk|1996|p=15}} Thereafter Anonymous does not mention Álmos.{{sfn|Kristó|Makk|1996|p=15}} In a contrasting report, the ''Illuminated Chronicle'' says that Álmos "could not enter [[Pannonia]], for he was killed in Erdelw"<ref>''The Hungarian Illuminated Chronicle'' (ch. 28), p. 98.</ref> (Transylvania).{{sfn|Kristó|Makk|1996|p=14}}{{sfn|Engel|2001|p=19}} Kristó says that the chronicle preserves the memory of Álmos's sacrifice because of the catastrophic defeat of his people by the Pechenegs.{{sfn|Kristó|Makk|1996|p=15}} If this is true, his ritual murder proves that Álmos was the sacred leader of the Hungarian tribal federation.{{sfn|Kristó|Makk|1996|p=15}}{{sfn|Engel|2001|p=19}} Róna-Tas refutes this and says that if the chronicle's report is reliable, Álmos became the victim of a political murder committed or initiated by his own son.{{sfn|Róna-Tas|1999|p=344}} Preferring the narration of the ''Gesta Hungarorum'' to the report by the ''Illuminated Chronicle'', Victor Spinei states that Álmos was not murdered in Transylvania, since Anonymus writes that the Hungarians bypassed this region when invading the Carpathian Basin.{{sfn|Spinei|2009|p=72}}

==Family==

No source preserved the name of Álmos's wife.<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'', note 9 on p. 15.</ref>{{sfn|Kristó|Makk|1996|p=Appendix 1}} Anonymus writes that she was "the daughter of a certain most noble prince".<ref>''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (ch. 4), p. 15.</ref> Álmos's only child known by name was Árpád, who succeeded Álmos after his death.{{sfn|Kristó|Makk|1996|p=Appendix 1}} 
The following is a family tree presenting Álmos's closest relatives:{{sfn|Kristó|Makk|1996|p=Appendix 1}}

{{familytree/start |summary=Árpád's family}}
{{familytree |border=1| | | | | | ÜGY | | EUN|           ÜGY=[[Ügyek]]|EUN=Eunedubelian}}
{{familytree | | | | | | |!| | | |!|}}
{{familytree |border=1| | | | | | ELÖ |y| EME|           ELÖ=[[Előd]] or Ügyek|EME=[[Emese]]}}
{{familytree | | | | | | | | |!| }}
{{familytree |border=1| | | | | | | |ÁLM| | | |           ÁLM=Álmos|boxstyle_ÁLM = background-color:  #d0e5f5}}
{{familytree | | | | | | | | |!| }}
{{familytree |border=1| | | | | | | |ÁRP| | | |           ÁRP=[[Árpád]]}}
{{familytree | | | | | | | | |!| | | | | | | | | | | | | |}}
{{familytree |border=1| | | | | | | |KOH| | | | | | | |           KOH=[[Kings of Hungary family tree|Hungarian monarchs]]}}
{{familytree/end}}

== See also ==
*[[Árpád dynasty]]
*[[Sacred king]]

==Footnotes==
{{Reflist|2}}

== References ==

=== Primary sources ===
{{Refbegin}}
*''Anonymus, Notary of King Béla: The Deeds of the Hungarians'' (Edited, Translated and Annotated by Martyn Rady and László Veszprémy) (2010). In: Rady, Martyn; Veszprémy, László; Bak, János M. (2010); ''Anonymus and Master Roger''; CEU Press; ISBN 978-963-9776-95-1.
*''Constantine Porphyrogenitus: De Administrando Imperio'' (Greek text edited by Gyula Moravcsik, English translation by Romillyi J. H. Jenkins) (1967). Dumbarton Oaks Center for Byzantine Studies. ISBN 0-88402-021-5.
*''Simon of Kéza: The Deeds of the Hungarians'' (Edited and translated by László Veszprémy and Frank Schaer with a study by Jenő Szűcs) (1999). CEU Press. ISBN 963-9116-31-9.
*''The Hungarian Illuminated Chronicle:'' Chronica de Gestis Hungarorum (Edited by Dezső Dercsényi) (1970). Corvina, Taplinger Publishing. ISBN 0-8008-4015-1.
*''The Russian Primary Chronicle: Laurentian Text'' (Translated and edited by Samuel Hazzard Cross and Olgerd P. Sherbowitz-Wetzor) (1953). Medieval Academy of America. ISBN 978-0-915651-32-0.
{{Refend}}

===Secondary sources===
{{Refbegin}}
*{{cite book |last=Engel |first=Pál |year=2001|title=The Realm of St Stephen: A History of Medieval Hungary, 895–1526 |publisher= I.B. Tauris Publishers |isbn=1-86064-061-3|ref=harv}}
*{{cite book |last=Kirschbaum |first=Stanislav J. |year=1995 |title=A History of Slovakia: The Struggle for Survival |publisher=Palgrave Macmillan |isbn=963-482-113-8 |ref=harv}}
*{{cite book |last=Kristó |first=Gyula |year=1996 |title=Hungarian History in the Ninth Century |publisher= Szegedi Középkorász Műhely |isbn=1-4039-6929-9 |ref=harv}}
*{{Cite book |last1=Kristó |first1=Gyula |last2=Makk |first2=Ferenc |year=1996 |title=Az Árpád-ház uralkodói ''[=Rulers of the House of Árpád]''|publisher=I.P.C. Könyvek | isbn=963-7930-97-3|ref=harv|language=hu}}
*{{cite book |last=Róna-Tas |first=András |year=1999 |title=Hungarians and Europe in the Early Middle Ages: An Introduction to Early Hungarian History (Translated by Nicholas Bodoczky) |publisher=CEU Press |isbn=978-963-9116-48-1|ref=harv}}
*{{Cite book |last=Spinei |first=Victor |year=2003 |title=The Great Migrations in the East and South East of Europe from the Ninth to the Thirteenth Century |publisher= Romanian Cultural Institute (Center for Transylvanian Studies) and Museum of Brăila Istros Publishing House |isbn= 973-85894-5-2|ref=harv}}
*{{Cite book |last=Spinei |first=Victor |year=2009 |title=The Romanians and the Turkic Nomads North of the Danube Delta from the Tenth to the Mid-Thirteenth century |publisher= Koninklijke Brill NV |isbn=978-90-04-17536-5|ref=harv}}
{{Refend}}

==External links==
{{Refbegin}}
*{{cite web |last=Marek |first=Miroslav |url=http://genealogy.euweb.cz/arpad/arpad1.html |title= Arpad |publisher= Genealogy.EU}}
{{Refend}}

{{s-start}}
{{s-hou|[[House of Árpád]]| |c. 820| |c. 895}}
{{s-reg}}
|-
{{succession box|
  before=[[Levedi]] (?)|
  title=[[Kende]] or [[Gyula (title)|gyula]] of the [[Hungarians]]|
  after=[[Árpád]]|
  years=c. 850&nbsp;– c. 895}}
{{s-end}}
{{Hungarian kings}}
{{good article}}

{{DEFAULTSORT:Almos}}
[[Category:Hungarian prehistory]]
[[Category:Hungarian princes]]
[[Category:9th-century Hungarian people]]
[[Category:9th-century rulers in Europe]]
[[Category:House of Árpád]]