{{Infobox journal
|cover	= [[File:Journal of Chemical Physics.jpg]]
|discipline = [[Chemical physics]]
|website = http://jcp.aip.org/
|publisher = [[American Institute of Physics]]
|country = [[United States]]
|abbreviation =	J. Chem. Phys.
|history = 1933–present
|frequency = weekly
|impact = 2.894
|impact-year = 2015
|ISSN	= 0021-9606 
|eISSN	= 1089-7690 
|CODEN	= JCPSA6
}}
'''''The Journal of Chemical Physics''''' is a [[scientific journal]] published by the [[American Institute of Physics]] that carries research papers on [[chemical physics]].<ref name=about>[http://jcp.aip.org/jcp/staff.jsp "About the Journal"] from the ''Journal of Chemical Physics'' website.</ref> <!-- reference gives all the facts in the above paragraph --> Two volumes, each of 24 issues, are published annually. It was established in 1933 when ''[[Journal of Physical Chemistry A|Journal of Physical Chemistry]]'' editors refused to publish theoretical works.<ref>{{cite book|last=Levine|first=Ira N.|title=Physical Chemistry |publisher=McGraw-Hill | isbn=9780072538625 | page = 2 | url = http://dl.alivechem.com/dverleech/files/alivechem.com__Physical_Chemistry-levine.pdf }}</ref>

The editors have been:<ref name=about/>
*2008–present: [[Marsha I. Lester]]
*2007–2008: Branka M. Ladanyi
*1998–2007: [[Donald H. Levy]]
*1983–1997: John C. Light
*1960–1982: J. W. Stout
*1958–1959: C. A. Hutchison
*1956–1957 (Acting): J. E. Mayer
*1953–1955: [[Clyde A. Hutchison, Jr.]]
*1942–1952: [[Joseph Edward Mayer]]
*1933–1941: [[Harold Urey]]

==See also==
* ''[[Annual Review of Physical Chemistry]]''
* ''[[Russian Journal of Physical Chemistry A]]''
* ''[[Russian Journal of Physical Chemistry B]]''

==References==
{{Reflist}}

==External links==
*[http://jcp.aip.org/ Journal home page]
*[http://www.aip.org/aip/ American Institute of Physics]
*[http://journals.aip.org/ American Institute of Physics Journals]

{{DEFAULTSORT:Chemical Physics, Journal Of}}
[[Category:Chemical physics journals]]
[[Category:American Institute of Physics academic journals]]
[[Category:Publications established in 1933]]
[[Category:English-language journals]]