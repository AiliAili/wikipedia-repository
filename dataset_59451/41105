In [[statistics]], '''Whittle likelihood''' is an approximation to the [[likelihood function]] of a stationary Gaussian [[time series]]. It is named after the mathematician and statistician [[Peter Whittle (mathematician)|Peter Whittle]], who introduced it in his PhD thesis in 1951.<ref name="Whittle1951">{{cite book|last=Whittle|first=P.|title=Hypothesis testing in times series analysis|publisher=Almqvist & Wiksells Boktryckeri AB|place=Uppsala|year=1951}}</ref>
It is commonly utilized in [[time series analysis]] and [[signal processing]] for parameter estimation and signal detection.

== Context ==
In a [[Gaussian process|stationary Gaussian time series model]], the [[likelihood function]] is (as usual in Gaussian models) a function of the associated mean and covariance parameters. With a large number (<math>N</math>) of observations, the (<math>N \times N</math>) covariance matrix may become very large, making computations very costly in practice. However, due to stationarity, the covariance matrix has a rather simple structure, and by using an approximation, computations may be simplified considerably (from <math>O(N^2)</math> to <math>O(N\log(N))</math>).<ref>{{cite web|last=Hurvich|first=C.|title=Whittle's approximation to the likelihood function|publisher=[[NYU Stern]]|url=http://people.stern.nyu.edu/churvich/TimeSeries/Handouts/Whittle.pdf|year=2002}}</ref> The idea effectively boils down to assuming a [[heteroscedasticity|heteroscedastic]] zero-mean Gaussian model in [[Fourier domain]]; the model formulation is based on the time series' [[discrete Fourier transform]] and its [[power spectral density]].<ref name="CalderDavis">{{Citation|last1=Calder|first1=M.|last2=Davis|first2=R. A.|editor1-last=Kotz|editor1-first=S.|editor2-last=Johnson|editor2-first=N. L.|title=Breakthroughs in Statistics|chapter=An introduction to Whittle (1953) "The analysis of multiple stationary time series"|pages=141–169|publisher=Springer-Verlag|place=New York|year=1997|doi=10.1007/978-1-4612-0667-5_7}} <br> See also: {{Citation|last1=Calder|first1=M.|last2=Davis|first2=R. A.|title=An introduction to Whittle (1953) "The analysis of multiple stationary time series"|work=Technical report 1996/41|publisher=Department of Statistics, [[Colorado State University]]|url=http://www.stat.colostate.edu/statresearch/stattechreports.html|year=1996}}</ref>
<ref name="hannan1994">{{Citation|last=Hannan|first=E. J.|editor-last=Kelly|editor-first=F. P.|title=Probability, statistics and optimization; a tribute to Peter Whittle|chapter=The Whittle likelihood and frequency estimation|publisher=Wiley|place=Chichester|year=1994}}</ref> <ref>{{Citation|last1=Pawitan|first1=Y.|editor1-last=Kotz|editor1-first=S.|editor2-last=Read|editor2-first=C. B.|editor3-last=Banks|editor3-first=D. L.|title=Encyclopedia of Statistical Sciences|chapter=Whittle likelihood|pages=708–710|volume=Update Volume 2|publisher=Wiley & Sons|place=New York|year=1998|doi=10.1002/0471667196.ess0753}}</ref>

== Definition ==
Let <math>X_1,\ldots,X_N</math> be a stationary Gaussian time series with (''one-sided'') power spectral density <math>S_1(f)</math>, where <math>N</math> is even and samples are taken at constant sampling intervals <math>\Delta_t</math>.
Let <math>\tilde{X}_1,\ldots,\tilde{X}_{N/2+1}</math> be the (complex-valued) [[discrete Fourier transform]] (DFT) of the time series. Then for the Whittle likelihood one effectively assumes independent zero-mean [[Gaussian distribution]]s for all <math>\tilde{X}_j</math> with variances for the real and imaginary parts given by

:<math> \operatorname{Var}\left(\operatorname{Re}(\tilde{X}_j)\right) = \operatorname{Var} \left( \operatorname{Im} (\tilde{X}_j)\right) = S_1(f_j)</math>

where <math>f_j=\frac j {N \, \Delta_t}</math> is the <math>j</math>th Fourier frequency. This approximate model immediately leads to the (logarithmic) likelihood function

:<math>\log\left(P(x_1,\ldots,x_N)\right) \propto -\sum_j \left( \log\left( S_1(f_j) \right) + \frac{|\tilde{x}_j|^2}{\frac N {2\,\Delta_t} S_1(f_j)} \right)</math>

where <math>|\cdot|</math> denotes the absolute value with <math>|\tilde{x}_j|^2=\left(\operatorname{Re}(\tilde{x}_j)\right)^2 + \left( \operatorname{Im} (\tilde{x}_j)\right)^2</math>.<ref name="CalderDavis" /> <ref name="hannan1994" />
<ref name="RoeverEtAl2011a">{{cite journal|last1=Röver|first1=C.|last2=Meyer|first2=R.|last3=Christensen|first3=N.|title=Modelling coloured residual noise in gravitational-wave signal processing|journal=Classical and Quantum Gravity|volume=28|issue=1|year=2011|pages=025010|DOI=10.1088/0264-9381/28/1/015010|arxiv=0804.3853}}</ref>

== Special case of a known noise spectrum ==
In case the noise spectrum is assumed a-priori ''known'', and noise properties are not to be inferred from the data, the likelihood function may be simplified further by ignoring constant terms, leading to the sum-of-squares expression

:<math>\log\left(P(x_1,\ldots,x_N)\right) \;\propto\; -\sum_j\frac{|\tilde{x}_j|^2}{\frac N {2 \, \Delta_t} S_1(f_j)}</math>

This expression also is the basis for the common [[matched filter]].

== Accuracy of approximation ==
The Whittle likelihood in general is only an approximation, it is only exact if the spectrum is constant, i.e., in the trivial case of [[white noise]].
The [[efficiency (statistics)|efficiency]] of the Whittle approximation always depends on the particular circumstances.<ref>{{cite journal|last1=Choudhuri|first1=N.|last2=Ghosal|first2=S.|last3=Roy|first3=A.|title=Contiguity of the Whittle measure for a Gaussian time series|journal=Biometrika|volume=91|issue=4|year=2004|pages=211–218|DOI=10.1093/biomet/91.1.211}}</ref>
<ref>{{cite journal|last1=Countreras-Cristán|first1=A.|last2=Gutiérrez-Peña|first2=E.|last3=Walker|first3=S. G.|title=A Note on Whittle's Likelihood|journal=Communications in Statistics – Simulation and Computation|volume=35|issue=4|year=2006|pages=857–875|DOI=10.1080/03610910600880203}}</ref>

Note that due to [[linearity]] of the Fourier transform, Gaussianity in Fourier domain implies Gaussianity in time domain and vice versa. What makes the Whittle likelihood only approximately accurate is related to the [[sampling theorem]] — the effect of Fourier-transforming only a ''finite'' number of data points, which also manifests itself as [[spectral leakage]] in related problems (and which may be ameliorated using the same methods, namely, [[window function|windowing]]). In the present case, the implicit periodicity assumption implies correlation between the first and last samples (<math>x_1</math> and <math>x_N</math>), which are effectively treated as "neighbouring" samples (like <math>x_1</math> and <math>x_2</math>).

== Applications ==

===Parameter estimation===
Whittle's likelihood is commonly used to estimate signal parameters for signals that are buried in non-white noise. The [[power spectral density|noise spectrum]] then may be assumed known,<ref name="finn1992">{{cite journal|last=Finn|first=L. S.|title=Detection, measurement and gravitational radiation|journal=Physical Review D|volume=46|issue=12|year=1992|pages=5236–5249|DOI=10.1103/PhysRevD.46.5236|arxiv=gr-qc/9209010}}</ref>
or it may be inferred along with the signal parameters.<ref name="hannan1994" /> <ref name="RoeverEtAl2011a" />

===Signal detection===
Signal detection is commonly performed utilizing the [[matched filter]], which is based on the Whittle likelihood for the case of a ''known'' noise power spectral density.<ref>{{cite journal|last=Turin|first=G. L.|title=An introduction to matched filters|journal=IRE Transactions on Information Theory|year=1960|volume=6|issue=3|pages=311–329|doi=10.1109/TIT.1960.1057571}}</ref>
<ref>{{cite book|last1=Wainstein|first1=L. A.|last2=Zubakov|first2=V. D.|title=Extraction of signals from noise|publisher=Prentice-Hall|place=Englewood Cliffs, NJ|year=1962}}</ref>
The matched filter effectively does a [[maximum-likelihood]] fit of the signal to the noisy data and uses the resulting [[Likelihood-ratio test|likelihood ratio]] as the detection statistic.<ref name="Roever2011b">{{cite journal|last=Röver|first=C.|title=Student-t-based filter for robust signal detection|journal=Physical Review D|volume=84|issue=12|year=2011|pages=122004|DOI=10.1103/PhysRevD.84.122004|arxiv=1109.0442}}</ref>

The matched filter may be generalized to an analogous procedure based on a [[Student-t distribution]] by also considering uncertainty (e.g. [[spectral density estimation|estimation]] uncertainty) in the noise spectrum. On the technical side, this entails repeated or iterative matched-filtering.<ref name="Roever2011b" />

===Spectrum estimation===
The Whittle likelihood is also applicable for estimation of the [[power spectral density|noise spectrum]], either alone or in conjunction with signal parameters.
<ref>{{cite journal|last1=Choudhuri|first1=N.|last2=Ghosal|first2=S.|last3=Roy|first3=A.|title=Bayesian estimation of the spectral density of a time series|journal=Journal of the American Statistical Association|volume=99|issue=468|year=2004|pages=1050–1059|DOI=10.1198/016214504000000557}}</ref>
<ref>{{cite journal|last1=Edwards|first1=M. C.|last2=Meyer|first2=R.|last3=Christensen|first3=N.|title=Bayesian semiparametric power spectral density estimation in gravitational wave data analysis|journal=Physical Review D|volume=92|issue=6|year=2015|pages=064011|DOI=10.1103/PhysRevD.92.064011|arxiv=1506.00185}}</ref>

== See also ==
*[[likelihood function]]
*[[weighted least squares]]
*[[matched filter]]
*[[discrete Fourier transform]]
*[[power spectral density]]
*[[colors of noise|coloured noise]]
*[[statistical signal processing]]

== References ==
{{Reflist}}

<!--- Categories --->
[[Category:Time series models]]
[[Category:Frequency domain analysis]]
[[Category:Statistical signal processing]]
[[Category:Signal estimation]]
[[Category:Normal distribution]]