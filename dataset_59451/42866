<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=April 2017}}
{{Use British English|date=April 2017}}
{|{{Infobox Aircraft Begin
 | name=Gannet
 | image=Gloster Gannet.png
 | caption=Gannet with Carden engine
}}{{Infobox Aircraft Type
 | type=light aircraft
 | national origin=United Kingdom
 | manufacturer=[[Gloster Aircraft Company|Gloucestershire Aircraft Company]]
 | designer=[[Henry Folland|H.P.Folland]]
 | first flight=23 October 1923
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Gloster Gannet''' was a single-seat single-engined light aircraft built by the [[Gloster Aircraft Company|Gloucestershire Aircraft Company Limited]] of [[Cheltenham]], United Kingdom, to compete in the 1923 Lympne Trials. Engine development problems prevented it from taking part.

==Development==
In  1923 the [[Royal Aero Club]] (RAeC) organised what became known as the [[Lympne light aircraft trials]] after the airfield where they were based, though the RAeC referred to the competing aircraft as ''motor-gliders''.  The intention was to develop economical private aviation, so the engine size was limited to 750&nbsp;cc with immediate consequences for aircraft size and weight.  Various sponsors provided attractive prizes, particularly the total of £1500 jointly from the Duke of Sutherland and the [[Daily Mail aviation prizes|Daily Mail]].  The event took place from 8–13 October 1923.

The Gannet was Gloster's intended entry. It was a small, single-bay biplane, one of the smallest aircraft built in [[United Kingdom|Britain]].  It was powered by a specially designed [[Carden Aero Engines|Carden]] vertical two-cylinder air-cooled two-stroke engine of exactly 750&nbsp;cc.<ref name="DNJ">{{harvnb|James|1971|pp=93–6}}</ref><ref name="Flight">''Flight'' 4 October 1923</ref>

The thick wings were based on a pair of spruce '''I'''-section spars with spruce ribs and internal wire bracing.  The inner section and interplane struts were of streamlined steel tube; ailerons were carried on upper and lower wings.  The Lympne aircraft were aimed at the private owner, for whom transport and storage might be an important issue, so the Gannet's wings were hinged at the rear spar so they could be folded back along the fuselage.  Both wing centre section trailing edges folded to permit the wing-folding; the upper section was also folded up to aid access to the cockpit.  The 2 imp. gallon (9.1 litre) fuel tank was in the upper centre section.<ref name="DNJ"/><ref name="Flight"/>

The fuselage was constructed around ash longerons, with plywood sides and fabric top and bottom.  The undercarriage was joined  to the centre section via a V-shaped pair of struts, had a single axle and, initially very small wheels.<ref name="DNJ"/><ref name="Flight"/>

Unfortunately for Gloster, the Carden engine had not had time for proper development and overheating and lubrication problems prevented the Gannet from flying at Lympne, though it was present, registered as ''G-EBHU'' and bearing Trials number ''7''.<ref name="Flight">''Flight''   18 October 1923</ref> The photographic evidence<ref>''Flight'' 18 October 1923 p.639</ref> shows it was either flying or making fast ground runs by then, though James gives the first flight date as either 23 October or 23 November.<ref>James gives the October date in text p.95 but the same day in November on p.350.  Both dates are after the Trials, though he says  "did not take part" on p.95 but "flew [in the competition]" on p.350 on p.</ref>

The following year the Gannet was re-engined with a 7&nbsp;hp (5.2&nbsp;kW) [[Blackburne (motorcycles)|Blackburne]] Tomtit inverted V-twin. It was also fitted with larger diameter wheels. It seems to have remained airworthy until permanently withdrawn from use on 25 January 1928,<ref>{{Citation |url= http://www.caa.co.uk/docs/HistoricalMaterial/G-EBHU.pdf |title=Civil Aviation Authority Registration Document for G-EBHU |accessdate=2009-02-24 |work= |publisher= Civil Aviation Authority|date= }}</ref> but Gloster did not enter it into the two other Lympne light aircraft trials held in 1924 and 1926. It appeared at the Olympia Aero show in 1929 and went for scrap in 1932.<ref name="DNJ"/><ref name="Flight"/> 
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Blackburne powered)==

{{aerospecs
|ref={{harvnb|James|1971|p=96}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=1
|capacity=
|length m=5.02
|length ft=16
|length in=6
|span m=5.4
|span ft=18
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=1.82
|height ft=6
|height in=0
|wing area sqm=9.56
|wing area sqft=103
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=149
|empty weight lb=330
|gross weight kg=208
|gross weight lb=460
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=698&nbsp;cc 2-cylinder inverted V air-cooled [[Blackburne Tomtit]]
|eng1 kw=5.2<!-- prop engines -->
|eng1 hp=7<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=115
|max speed mph=72
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=225
|range miles=140
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=0.68
|climb rate ftmin= to 4,000 ft (1,220 m) 133
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==

{{reflist}}

==References==
{{Commons category|Gloster Gannet}}
{{refbegin}}
*{{Citation |last= |first= |authorlink= |coauthors= |year= |month= |title= THE GLOUCESTERSHIRE " GANNET " LIGHT 'PLANE.|journal= [[Flight International|Flight]]|volume= |issue=4 October 1923 |pages=609–611 |id= |url= http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200609.html|accessdate= |quote= }}
*{{Citation |last= |first= |authorlink= |coauthors= |year= |month= |title=The Light 'Plane Meeting at Lympne |journal=[[Flight International|Flight]] |volume= |issue=18 October 1923 |pages= 635–648|id= |url=http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200635.html |accessdate= |quote= }}
*{{Citation |title= Gloster Aircraft since 1917|last= James|first=Derek N. |authorlink= |coauthors= |year=1971 |publisher=Putnam Publishing  |location=London |isbn= 0-370-00084-6|page= |pages= |url=|ref=harv }}
{{refend}}
<!-- ==External links== -->
{{Gloster aircraft}}

[[Category:British sport aircraft 1920–1929]]
[[Category:Gloster aircraft|Gannet]]