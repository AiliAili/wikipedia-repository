{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or

[[Wikipedia:WikiProject Books]] -->
| name          = Never Bet the Devil Your Head
| image         = [[File:Never Bet the Devil Your Head.png|frameless]]
| caption       = "Never Bet Your Head" as it appeared in its original published form
| author        = [[Edgar Allan Poe]]
| country       = United States
| language      = English
| series        = 
| genre         = [[Satire]]<br>[[Short story]]
| published_in   = ''[[Graham's Magazine]]''
| publisher     = 
| media_type    = Print ([[Periodical]])
| pub_date  = [[1841 in literature|1841]]
| english_pub_date = 
| preceded_by   = 
| followed_by   = 
}}

"'''Never Bet the Devil Your Head'''", often [[subtitle (titling)|subtitled]] "A Tale with a Moral", is a [[short story]] by American author [[Edgar Allan Poe]], first published in 1841. The [[satire|satirical]] tale pokes fun at the notion that all literature should have a [[moral]]<ref name=Silverman>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. New York: Harper Perennial, 1991. p. 169. ISBN 0-06-092331-8</ref> and spoofs [[transcendentalism]].

==Plot summary==
The [[narrator]], presented as the author himself, is dismayed by literary critics saying that he has never written a moral tale. The narrator then begins telling the story of his friend Toby Dammit. Dammit is described as a man of many vices, at least in part due to his [[left-handed]] mother flogging him with her left hand, which is considered improper. Dammit often made rhetorical bets, becoming fond of the expression "I'll bet the [[devil]] my head". Though the narrator tries to break Dammit of bad habits, he fails. Nevertheless, the two remain friends.

While traveling one day, they come across a [[covered bridge]]. It is gloomy and dark, lacking windows. Dammit, however, is unaffected by its gloom and is in an unusually good mood. As they cross the bridge, they are stopped by a [[turnstile]] partway across. Dammit bets the devil his head that he can leap over it. Before the narrator can reply, a cough alerts them to the presence of a little old man. The old man is interested in seeing if Dammit is capable of making such a leap and offers him a good running start. The narrator thinks to himself that it is improper for an old man to push Dammit into making the attempt — "I don't care who the devil he is", he adds.

The narrator watches as Dammit makes a perfect jump, though directly above the turnstile he falls backwards. The old man quickly grabs something and limps away. The narrator, upon checking on his friend, sees that Dammit's head is gone ("what might be termed a serious injury"). He realizes that just above the turnstile, lying horizontally, was a sharp iron bar that happened to be lying at just the spot where his friend's neck hit when he jumped. The narrator sends for the "[[homeopathy|homeopathists]]", who "did not give him little enough physic, and what little they did give him he hesitated to take. So in the end he grew worse, and at length died". After the bill for his funeral expenses is left unpaid, the narrator has Dammit's body dug up and sold for dog meat.

==Analysis==
"Never Bet the Devil Your Head" is a clear attack on [[transcendentalism]], which the narrator calls a "[[disease]]" afflicting Toby Dammit. The narrator, in fact, sends the bill for Dammit's funeral expenses to the transcendentalists, who refuse to pay because of their disbelief in [[evil]].<ref>Herndon, Jerry A. "Poe's Ligeia: Debts to Irving and Emerson" collected in ''Poe and His Times: The Artist and His Milieu'', edited by Benjamin Franklin Fisher IV. Baltimore: The Edgar Allan Poe Society, 1990. p. 118. ISBN 0-9616449-2-3</ref> Despite specific mentions of transcendentalism and its flagship journal ''[[The Dial]]'', Poe denied that he had any specific targets.<ref name=Sova/>  Elsewhere, he certainly admitted a distaste for transcendentalists, whom he called "Frogpondians" after the pond on [[Boston Common (park)|Boston Common]].<ref>Royot, Daniel. "Poe's humor," as collected in ''The Cambridge Companion to Edgar Allan Poe'', Kevin J. Hayes, ed. Cambridge University Press, 2002. pp. 61-2. ISBN 0-521-79727-6</ref> He ridiculed their writings in particular by calling them "[[metaphor]]-run," lapsing into "obscurity for obscurity's sake" or "mysticism for mysticism's sake."<ref>Ljunquist, Kent. "The poet as critic" collected in ''The Cambridge Companion to Edgar Allan Poe'', Kevin J. Hayes, ed. Cambridge University Press, 2002. p. 15. ISBN 0-521-79727-6</ref> Poe once wrote in a letter to [[Thomas Holley Chivers]] that he did not dislike transcendentalists, "only the pretenders and [[sophism|sophists]] among them."<ref name=Silverman/>

==Publication history==
The story was first published in the September 1841 issue of ''[[Graham's Magazine]]'' as "Never Bet Your Head: A Moral Tale". Its republication in the August 16, 1845 issue of the ''[[Broadway Journal]]'' included its now-standard title "Never Bet the Devil Your Head".<ref name=Sova>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001. p. 170. ISBN 0-8160-4161-X</ref> Noted Poe biographer Arthur Hobson Quinn dismissed the story, saying "it is a trifle."<ref>Quinn, Arthur Hobson. ''Edgar Allan Poe: A Critical Biography''. Johns Hopkins University Press, 1998. p. 325. ISBN 0-8018-5730-9</ref>

==Adaptations==
"Never Bet the Devil Your Head" is the final segment (retitled "Toby Dammit") of the three-part ''Histoires extraordinaires'' (English title: ''[[Spirits of the Dead]]'') ([[1968 in film|1968]]), directed by [[Federico Fellini]].<ref name=Sova/>

"Never Bet the Devil Your Head" was adapted as a [[radio play]] for the ''[[CBS Radio Workshop]]'' in 1957. The cast features noted voice actors [[John Dehner]] as Mr. Poe, [[Daws Butler]] as Toby Dammit and [[Howard McNear]] as the Devil.<ref>{{cite web|url=http://www.otrsite.com/logs/logc1011.htm|title=CBS Radio Workshop|website=Jerry Haendiges Vintage Radio Logs|access-date=19 October 2016}}</ref> The program is available on the [[Internet Archive]].<ref>{{cite web|url= https://archive.org/details/CBSRadioWorkshop|title=CBS Radio Workshop|website=[[Internet Archive]]|access-date=19 October 2016}}</ref>

==References==
{{reflist}}

==External links==
* {{wikisource-inline|single=true}}
* Text of [http://classiclit.about.com/library/bl-etexts/eapoe/bl-eapoe-never.htm Never Bet the Devil Your Head]
* {{librivox book | title=The Works of Edgar Allan Poe, Raven Edition, Volume 5 | author=Edgar Allan POE}}

{{Edgar Allan Poe}}

[[Category:Short stories by Edgar Allan Poe]]
[[Category:1841 short stories]]
[[Category:Works originally published in Graham's Magazine]]