{{Use dmy dates|date=July 2013}}
{{For|the political science journal published at Illinois State University|Critique (journal)}}
{{Infobox journal
| title = Critique: Journal of Socialist Theory
| cover =
| editor = [[Hillel H. Ticktin]]
| discipline = [[Political science]]
| abbreviation =
| publisher = [[Routledge]] on behalf of the Centre for the Study of Socialist Theory and Movements ([[University of Glasgow]])
| country =
| frequency = Quarterly
| history = 1973-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.critiquejournal.net/
| link1 = http://www.tandf.co.uk/journals/titles/03017605.asp
| link1-name = Journal page at publisher's site
| link2 = http://www.informaworld.com/smpp/title~content=t741801732~db=all
| link2-name = Online access
| JSTOR =
| OCLC = 70889137
| LCCN =
| CODEN =
| ISSN = 0301-7605
| eISSN = 1748-8605
}}
'''''Critique: Journal of Socialist Theory''''' is a [[Marxism|Marxist]] [[academic journal]] published by the Centre for the Study of Socialist Theory and Movements ([[University of Glasgow]]). The journal was established in May 1973 by founding [[Editor-in-chief|editor]] [[Hillel H. Ticktin]] as ''Critique: Journal of Marxist Theory and Soviet Studies.''

Originating as an anti-[[Stalinism|Stalinist]] [[Europe-Asia Studies|Soviet Studies]] journal, with the editor accepting the analysis of [[Leon Trotsky]] as a corrective to the Stalinist distortion of [[Marxism]], the initial aim of ''Critique'' was to analyze the empirical reality of Stalinism, while rejecting the [[empiricism|empiricist method]], in order to discover the objective laws of motion of Stalinism. The journal accepted Trotsky’s 1936 prognosis that the [[Soviet Union]] under [[Joseph Stalin]]’s program of ''[[Socialism in One Country]]'' would fail and that the [[capitalism|capitalist market system]] would be restored.

Since the [[dissolution of the Soviet Union]], ''Critique'' has become a more general journal of socialist theory covering political economy, philosophy, history and art, examining capitalist and non-capitalist societies and the instability of world capitalism after the [[Cold War]]. ''Critique'' is issued four times per year and has been published by [[Routledge]] since April 2006.  Notable contributors have included [[Hillel Ticktin]], [[István Mészáros (professor)|István Mészáros]], [[Bertell Ollman]], [[Ernest Mandel]], [[James Petras]], [[Roman Rosdolsky]] and Chris Arthur.

== External links ==
* {{Official website|http://www.critiquejournal.net/history.html}}
* [http://www.gcal.ac.uk/archives/critique/index.html ''Critique: Journal of Socialist Theory'' Archive]. [[Glasgow Caledonian University]]
* [http://www.gla.ac.uk/schools/critical/research/researchcentresandnetworks/socialisttheoryandmovement/ Socialist Theory and Movements Research Network]. University of Glasgow

{{DEFAULTSORT:Critique}}
[[Category:Publications established in 1973]]
[[Category:Marxist journals]]
[[Category:Socialist publications]]
[[Category:English-language journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Quarterly journals]]
[[Category:Political science journals]]