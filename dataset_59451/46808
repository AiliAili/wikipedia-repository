{{Chembox
| ImageFile = Calcium_perchlorate.svg
| ImageSize =
| ImageAlt =
| ImageFile1 =
| ImageSize1 = 
| ImageAlt1 = 
| IUPACName = Calcium perchlorate 
| OtherNames = Calcium perchlorate tetrahydrate, Calcium diperchlorate, Perchloric acid calcium salt (2:1), Calcium perchlorate, hydrated 
|Section1={{Chembox Identifiers
| CASNo = 13477-36-6
| PubChem = 61629
| ChemSpiderID = 55537
| SMILES = [Ca+2].O=Cl(=O)(=O)[O-].[O-]Cl(=O)(=O)=O
| InChI = 1/Ca.2ClHO4/c;2*2-1(3,4)5/h;2*(H,2,3,4,5)/q+2;;/p-2
| InChIKey = ZQAOXERLHGRFIC-NUQVWONBAO
| StdInChI = 1S/Ca.2ClHO4/c;2*2-1(3,4)5/h;2*(H,2,3,4,5)/q+2;;/p-2
| StdInChIKey = ZQAOXERLHGRFIC-UHFFFAOYSA-L
}}
|Section2={{Chembox Properties
| Formula = Ca(ClO<sub>4</sub>)<sub>2</sub>
| MolarMass = 238.9792 g/mol
| Appearance = White to yellow crystalline solid
| Density = 2.651 g/cm3
| MeltingPtC = 270
| MeltingPt_notes = 
| BoilingPt =
| Solubility = 188 g/100mL (20 °C)
| SolubleOther = EtOH, MeOH}}
|Section3={{Chembox Hazards
| ExternalSDS = 
| EUClass = 
| MainHazards = Inhalation, ingestion or contact (skin, eyes) with vapors or substance may cause severe injury, burns, etc. Fire may produce irritating, corrosive and/or toxic gases. Runoff from fire control or dilution water may cause pollution. {{citation needed|date=February 2016}}
| NFPA-H = -
| NFPA-F = 0
| NFPA-R = -
| NFPA-S = OX
| RPhrases = 
| SPhrases = 
| RSPhrases = 
| FlashPt = 
| AutoignitionPt = 
| ExploLimits = 
| LD50 =
| SkinHazard = Avoid contact with skin. In case of contact, rinse immediately with (manufacturer's instructions)  
| EyeHazard = Avoid contact with eyes. In case of contact, rinse immediately with plenty of water and seek medical advice 
| InhalationHazard =
| IngestionHazard =
| GHSPictograms =
| GHSSignalWord =
| HPhrases =
| PPhrases =
| TLV = 
| TLV-TWA =
| TLV-STEL = 
| PEL = 
 }}
}}
'''Calcium perchlorate''' is classified as a metal [[perchlorate]] salt with the molecular formula Ca(ClO<sub>4</sub>)<sub>2</sub>. It is an inorganic compound that is a yellow-white crystalline solid in appearance. As a strong [[oxidizing agent]], it reacts with [[reducing agent]]s when heated to generate heat and products that may be gaseous (which will cause pressurization in closed containers). Calcium perchlorate has been categorized as having explosive [[Reactivity (chemistry)|reactivity]]. Ca(ClO<sub>4</sub>)<sub>2</sub> is a common chemical on the soil of planet [[Mars]], counting for almost 1% of the Martian dust, by weight.

== Properties ==
Calcium perchlorate is a strong inorganic oxidizing agent, enhancing the [[combustion]] of other substances that can potentially lead to explosion. The perchlorate ion, ClO<sub>4</sub><sup>−</sup>, has a highly symmetrical [[Tetrahedron|tetrahedral]] structure that is strongly stabilized in solution by its low [[Electron donor|electron-donating]] proton-accepting power and its relatively low [[polarizability]].

=== Eutectic System ===
Calcium perchlorate solution forms a simple [[eutectic system]]. The eutectic composition of the calcium perchlorate solution is 4.2&nbsp;mol / 100 g H<sub>2</sub>O, very similar to the composition of closely related metal [[cation]] perchlorates of [[strontium]] and [[barium]].

== Occurrences ==

=== Electrolyte Conductance ===
[[Electrolyte]] [[Electrical conductance|conductance]] of Ca(ClO<sub>4</sub>)<sub>2</sub> and double charged metal cations in the organic solvent [[acetonitrile]] has been tested. The interest in metal cation perchlorate interactions with [[photosensitive]] [[ligand]]s has increased due to the development of highly specific [[fluorescence]] indicators.

== Production ==
Perchlorate salts are the product of a [[base (chemistry)|base]] and [[perchloric acid]]. Calcium perchlorate can be prepared through the heating of a mixture of [[calcium carbonate]] and [[ammonium perchlorate]]. [[Ammonium carbonate]] forms in the gaseous state, leaving behind a calcium perchlorate solid.

== Reactions ==

=== Water ===
Being very [[hygroscopic]], calcium perchlorate is commonly seen in the presence of four water molecules, referred to as calcium perchlorate tetrahydrate.
:::::::::::::::<big>Ca(ClO<sub>4</sub>)<sub>2</sub> • 4H<sub>2</sub>O</big>

=== Cyclic Hydrogenphosphonates ===
A hybrid organic-inorganic molecule is formed using dioxazaphosphocanes, eight-membered cyclic hydrogenphosphonates and calcium. Calcium from the calcium perchlorate contributes to the structural integrity of the [[oligomeric]] molecule; the four calcium ions are bridged between four dioxazaphosphocane [[Functional group|moieties]].

== Human toxicity ==
Calcium perchlorate is toxic to humans, by ingestion or inhalation of dust particles, or (less so) by skin contact.

== References ==
# {{Citation
| url = http://cameochemicals.noaa.gov/chemical/2787#section5
| title = Calcium Perchlorate
| work = Cameo Chemicals
| publisher = [[Office of Response and Restoration]], [[NOAA's Ocean Service]], [[National Oceanic and Atmospheric Administration]], [[USA.gov]]
| accessdate = October 25, 2012
}}
# {{Citation
| url = http://www.chemicalbook.com/ProductChemicalPropertiesCB9181917_EN.htm
| title = Calcium Perchlorate
| work = ChemicalBook
| publisher = [[ChemicalBook]]
| accessdate = October 25, 2012
}}
# {{Citation
| url = http://www.sciencedirect.com/science/article/pii/S0167732211003515
| title = Ion association and solvation in solutions of Mg<sup>2+</sup>, Ca<sup>2+</sup>, Sr<sup>2+</sup>, Ba<sup>2+</sup> and Ni<sup>2+</sup> perchlorates in acetonitrile: Conductometric study
| work = Journal of Molecular Liquids 
| publisher = [[Elsevier B.V.]]
| accessdate = October 25, 2012
| doi=10.1016/j.molliq.2011.10.012
| volume=165
| pages=78–86
}}
# {{Citation
| url = http://onlinelibrary.wiley.com/doi/10.1002/1099-0682%28200210%292002:10%3C2727::AID-EJIC2727%3E3.0.CO;2-D/pdf
| title = A New Oligomeric Complex of Cyclic Hydrogenphosphonates with Calcium Perchlorate
| work = European Journal of Inorganic Chemistry 
| publisher = [[European Journal of Inorganic Chemistry]]
| accessdate = November 15, 2012
| doi=10.1002/1099-0682(200210)2002:10<2727::AID-EJIC2727>3.0.CO;2-D
| volume=2002
| pages=2727–2729
}}
# {{Citation
| url = http://web.mit.edu/semenko/Public/Military%20Manuals/RogueSci-Mirror/explo/perchlorates.html
| title = Perchlorates
| work = Megalomania's Method of Making Perchlorates
| publisher = [[Megalomania's Controversial Chem Lab]]
| accessdate = October 31, 2012
}}
# {{Citation
| url = http://www.hbcpnetbase.com/tables/default.asp
| title = Physical Constraints of Inorganic Compounds
| work = Handbook of Chemistry and Physics 
| publisher = [[Taylor and Francis Group, LLC]]
| accessdate = October 25, 2012
}}
# {{Citation
| url = http://www.springerlink.com/content/q576526454ln3243/fulltext.pdf
| title = Polythermal Study of the Systems M(ClO<sub>4</sub>)<sub>2</sub>–H<sub>2</sub>O (M<sup>2+</sup> = Mg<sup>2+</sup>, Ca<sup>2+</sup>, Sr<sup>2+</sup>, Ba<sup>2+</sup>)
| work = Russian Journal of Applied Chemistry
| publisher = [[Original Russian Text]]
| accessdate = November 13, 2012
}}

{{Calcium compounds}}
{{Perchlorates}}

[[Category:Perchlorates]]
[[Category:Calcium compounds]]