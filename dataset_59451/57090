{{Infobox publisher
| name         = Gebrüder Borntraeger Verlagsbuchhandlung
| image        = 
| caption      =
| parent       = [[E. Schweizerbart]]
| status       = active
| traded_as    = 
| predecessor  =
| founded      = 1790
| founder      = Friedrich Nicolovius
| successor    = 
| country      = [[Germany]]
| headquarters = [[Stuttgart]]
| distribution =  worldwide
| keypeople    = 
| publications = 
| topics       = [[Botany]], [[Earth Science]]s, [[Environmental Science]]s.
| genre        = scientific
| imprints     = 
| revenue      = 
| owner        =
| numemployees =
| url          = http://www.schweizerbart.de
}}
'''Gebrüder Borntraeger Verlagsbuchhandlung''' (Borntraeger Brothers) is a scientific publisher covering the fields of Botany, Earth and Environmental Sciences.

==History==
The publishing house was established in 1790 in [[Königsberg]] by (Matthias) Friedrich Nicolovius (1768-1836). It was taken over in 1818 by the Borntraeger brothers. Their authors included [[Friedrich Wilhelm Bessel]] and [[Immanuel Kant]]. In 1867 they purchased Verlagsbuchhandlung from Edward Eggers, moved to Berlin and shifted the emphasis to natural sciences. Carl Robert Thost, one of the 34 founding members of the German Palaeontological Society ("[[Paläontologische Gesellschaft]]"), strengthened the publisher's board in 1895.<ref>{{cite journal |title=none |journal=Paläontologische Zeitschrift |volume=1 |issue=1 |date=March 1914}}</ref> 

After [[World War II]], they relocated to [[Nikolassee]]. In 1968 the owners of [[E. Schweizerbart]] took over the publisher, and moved to [[Stuttgart]] where they cooperate with sister publisher [[E. Content Free Trial]] but the publishers, whilst affiliated, remain formally independent.<ref>{{cite web |url=http://www.schweizerbart.de/home/about |title=About Schweizerbart and Gebr. Borntraeger and J. Cramer &mdash; Schweizerbart science publishers |publisher=Schweizebart Science Publishers |accessdate=25 March 2015}}</ref>

In 1986, the company acquired the botanical publisher [[J. Cramer]] (whose titles include ''[[Dissertationes Botanicae]]'' and  ''[[Bibliotheca Phycologica]]''), which continues to operate as an imprint in the Gebr. Borntraeger stable.

==The Geological Guide collection==

The publisher is noted for the collection of Geological Guides ('''''Sammlung geologischer Führer''''') whose first 36 volumes appeared before the Second World War from 1897 to 1939. Volume 37 was published as the first sequel after the War in 1958 and by 2013 the series had reached volume 138.<ref>{{cite web |url=http://www.balogh.com/schweizerbart/sammlung_geologischer_fuehrer.html |title=Sammlung Geologischer Führer |volume=37&ndash;97|language=de| issn=0343-737X}}</ref>

==References==
{{reflist}}

== Literature ==
* {{cite book |title=Gebrüder Borntraeger, Berlin und Leipzig 1790–1930 |publisher=[[Gebr. Borntraeger]] |location=Berlin |year=1930}}
* {{cite book |title=200 Jahre Gebrüder Borntraeger Verlagsbuchhandlung, Berlin, Stuttgart: ein Jubiläum, |publisher=Gebr. Borntraeger |year=1990}}

== External links ==
* {{official website|http://www.schweizerbart.de/home/start|Official website with Schweizerbart}}

{{DEFAULTSORT:Borntraeger Verlagsbuchhandlung, Gebruder}}

{{publisher-stub}}
{{Germany-company-stub}}

[[Category:Companies established in 1790]]
[[Category:Companies based in Stuttgart]]
[[Category:Book publishing companies of Germany]]
[[Category:Publishing companies of Germany]]
[[Category:Publishing companies established in the 1790s]]
[[Category:1790 establishments in Prussia]]