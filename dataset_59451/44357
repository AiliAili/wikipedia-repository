{{Use mdy dates|date=May 2014}}
{{Infobox airport
| name         = St. Pete–Clearwater International Airport
| image        = St Petersburg Clearwater airport logo.jpg
| image-width  = 180
| image2       = St. Petersburg-Clearwater International Airport FL 31 Dec 2008.jpg
| caption2     = [[USGS]] 1998<!--file misnamed, not 2008--> [[orthophoto]]
| IATA         = PIE
| ICAO         = KPIE
| FAA          = PIE
| type         = Public
| owner        = County of Pinellas
| focus_city   = [[Allegiant Air]]
| city-served  = [[Tampa Bay Area]] 
| location     = [[Pinellas County, Florida|Pinellas County]]
| elevation-f  = 11
| website      = {{URL|www.fly2pie.com|www.Fly2PIE.com}}
| coordinates  = {{coord|27|54|36|N|082|41|15|W|region:US-FL_scale:40000|display=inline,title}}
| pushpin_map            = USA Florida#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Florida / United States
| pushpin_label          = '''PIE'''
| r1-number    = 18/36
| r1-length-f  = 9,730
| r1-surface   = Asphalt
| r3-number    = 4/22
| r3-length-f  = 5,903
| r3-surface   = Asphalt
| stat-year    = 
| stat1-header = Aircraft operations (2016)
| stat1-data   = 105,273
| stat2-header = Based aircraft (2017)
| stat2-data   = 261
| stat3-header = Total passengers (2012)
| stat3-data   = 865,942 
| stat4-header = Cargo tonnage (2011)
| stat4-data   = 15,060
| footnotes    = Sources: [[Federal Aviation Administration|FAA]],<ref name="FAA">{{FAA-airport|ID=PIE|use=PU|own=PU|site=03475.*A}}. Federal Aviation Administration. Effective Jan 5, 2017.</ref> Airport website<ref name="PIE">{{cite web | url = http://www.fly2pie.com/media/Statistics/piefactsheet.pdf | title = Fact Sheet 2012 | format = PDF | publisher = St. Petersburg–Clearwater International Airport | accessdate = December 31, 2012}}</ref>
}}

'''St. Pete–Clearwater International Airport''' {{airport codes|PIE<ref>{{cite web | url = http://www.iata.org/publications/Pages/code-search.aspx | title = IATA Airport code Search (PIE: St.Pete/Clearwater) | publisher = [[International Air Transport Association]] | accessdate = December 31, 2012}}</ref>|KPIE|PIE}} is a public/military airport in [[Pinellas County, Florida]] serving the [[Tampa Bay Area]].<ref name="FAA" /> It is nine miles north of downtown St. Petersburg,<ref name="FAA" /> seven miles southeast of [[Clearwater, Florida|Clearwater]], and seventeen miles southwest of [[Tampa, Florida|Tampa]].

The [[National Plan of Integrated Airport Systems]] for 2011–2015 [[FAA airport categories|categorized]] it as a ''primary commercial service'' airport since it has over 10,000 passenger boardings (enplanements) per year. In 2014 it showed double-digit growth and handled more than one million passengers, setting a record.<ref>http://www.bizjournals.com/tampabay/news/2015/01/09/st-pete-clearwater-airport-set-passenger-record-in.html</ref>

Most scheduled airline traffic in the [[Tampa Bay Area]] uses [[Tampa International Airport]] (TPA), ten miles (16&nbsp;km) east, but St. Pete–Clearwater remains a destination for low-cost carriers. St. Pete–Clearwater is a focus city for Las Vegas-based [[Allegiant Air]]. PIE is less busy than Tampa, and is frequently used by pilots of private planes and executive jets.{{citation needed|date=December 2015}}

The airport uses "Tampa Bay The Easy Way" as an advertising slogan and Fly2PIE in reference to its three-letter IATA and FAA codes.

== History ==
The airport is on the west shoreline of [[Tampa Bay]], six miles (10&nbsp;km) north of [[St. Petersburg, Florida]] (the "birthplace of commercial air transportation"). Barely a decade after the pioneer flight of the [[Wright brothers]] at [[Kitty Hawk, North Carolina|Kitty Hawk]] in 1903, the first tickets for airline travel were sold by the [[St. Petersburg-Tampa Airboat Line]] of [[Tony Jannus]] to fare-paying passengers. Using a [[Benoist XIV]] [[amphibious aircraft]], the inaugural flight took place from a location near the downtown [[St. Petersburg Pier]].<ref>{{cite web |title=Tony Jannus&nbsp;—An Enduring Legacy of Aviation |publisher=Tony Jannus Distinguished Aviation Society |url=http://www.tonyjannusaward.com/history.html |accessdate=April 25, 2008 |archiveurl = http://web.archive.org/web/20080204230034/http://www.tonyjannusaward.com/history.html <!-- Bot retrieved archive --> |archivedate = February 4, 2008}}</ref> Mayor Abram C. Pheil of St. Petersburg and Mae Peabody of Dubuque, Iowa, were the first passengers, flying across the bay to [[Tampa, Florida|Tampa]] and, according to a [[United Press International|United Press]] account, reportedly reaching the maximum speed of 75&nbsp;miles per hour during the flight. Other reports indicate that they reached an altitude of {{convert|50|ft|m|0}}.

This was the beginning of commercial air transportation anywhere in the world and is commemorated by a replica of the Benoist aircraft and a plaque at the airport terminal baggage claim area. Another replica is displayed at the St. Petersburg Museum of History adjacent to the Pier. Since 1991, the terminal holds the archives of the Florida Aviation Historical Society.<ref name=McCarthy/>

=== Construction and wartime ===
{{see also|Pinellas Army Air Field}}

Construction of the airport at its present site started in March 1941. After the [[attack on Pearl Harbor]], the airport was acquired by the [[United States Army Air Forces]], which used it as a military flight training base assigned to [[Third Air Force]].

The 304th Fighter Squadron, a combat training unit of the 337th Fighter Group based [[Curtiss P-40|P-40 Warhawk]]s and, later, [[P-51 Mustang]]s at Pinellas Army Airfield (as it was then known) for the duration of [[World War II]]. Antisubmarine patrols against German U-boats in the Gulf of Mexico were also flown from the airfield.

To commemorate the airport's vital role during that conflict, a plaque was dedicated at the airport terminal in 1994 by the P-51 Fighter Pilots Association and Brigadier General [[James H. Howard]], who was the only European Theater fighter pilot to be awarded the [[Medal of Honor]] in World War II and later served as the last wartime base commander of Pinellas Army Airfield. A permanent exhibit honoring General Howard is located in the terminal.

=== Postwar operation ===
[[Image:PIE airport map.PNG|thumb|upright|FAA diagram (October 2006)]]
After World War II the property was returned to Pinellas County by the U.S. government to operate as a civil airport. It was originally called Pinellas International Airport and given the IATA designation, PIE, which it still uses, because PIA was already taken by [[General Wayne A. Downing Peoria International Airport|Peoria International Airport]].<ref>{{citation |title= Insiders' Guide to the Greater Tampa Bay Area: Including Tampa, St. Petersburg, & Clearwater |series= Insiders' Guide Series |first= Anne W. |last= Anderson |publisher= Globe Pequot |year= 2010 |isbn= 9780762753475 |page= 16 |url= https://books.google.com/books?id=f4LDwCA8B3EC&pg=PA16&lpg=PA16&dq=St.+Petersburg-Clearwater+International+Airport+name+change&source=bl&ots=b4DAhEDjWz&sig=oBM_K_9uYU7oSpJZEbQZ6Qw22mY&hl=en&sa=X&ei=7rVkUJ_PFcyLhQeopYGoAw&ved=0CDAQ6AEwAQ#v=onepage&q=St.%20Petersburg-Clearwater%20International%20Airport%20name%20change&f=false }}</ref> It was later changed to St. Petersburg–Clearwater International Airport because, according to airport manager "Bobo" Hayes, tourists didn't know where Pinellas county was.<ref name=McCarthy>{{citation |title= Aviation in Florida |first= Kevin M. |last= McCarthy |others= Illustrated by William Trotter |edition= illustrated |publisher= [http://www.pineapplepress.com/ Pineapple Press Inc] |year= 2003 |isbn= 9781561642816 |pages= 159–164 |url= https://books.google.com/books?id=jdIClpM7DgQC&pg=PA163&lpg=PA163&dq=St.+Petersburg-Clearwater+International+Airport+name+change&source=bl&ots=jbLcafq4nN&sig=RyNrz748rYYXFF9PugyCxpGoQVM&hl=en&sa=X&ei=7rVkUJ_PFcyLhQeopYGoAw&ved=0CCoQ6AEwAA#v=onepage&q&f=false }}</ref>

In the 1950s some airlines flew to both PIE and TPA, such as [[Delta Air Lines]], [[Eastern Air Lines]], [[National Airlines (NA)|National Airlines]] and [[Northwest Airlines]]. The April 1957 Official Airline Guide (OAG) shows 17 airline departures: ten Eastern, six National and one Mackey. Four of those flights were nonstop beyond Florida, including an Eastern [[Douglas DC-4|DC-4]] to Chicago and a [[Lockheed L-1049 Super Constellation|1049G]] to Pittsburgh.

With the advent of the [[Jet age]] runway 17/35 was extended north into Tampa Bay; the first scheduled jet was a Northwest Airlines 720B from Chicago in late 1961. (The 1961 Aviation Week directory says PIE's longest runways were 5700 ft, but probably 17/35 was 8000 ft when the 720B arrived.) The increased capacity of the [[Boeing 707]] and [[Douglas DC-8]], compared to the [[Douglas DC-6|DC-6]] and [[Douglas DC-7|DC-7]], prompted the [[Civil Aeronautics Board]] to approve consolidation of airline service for the Tampa Bay area at TPA in the early 1960s.

Eastern was the last scheduled airline at PIE; it left in 1964. Airline service returned to PIE in the 1970s when [[Air Florida]] flew [[Lockheed L-188 Electra|Electras]] out of PIE; in 1982, Northeastern International started DC-8 nonstops to Islip, New York, and in 1983 [[People Express Airlines (1980s)|People Express]] started nonstops to Newark with [[Boeing 737]]s and [[Boeing 727]]s. In 1986 [[Florida Express]] flew [[BAC One-Eleven]]s nonstop to its hub in Orlando.<ref>http://www.departedfligthts.com, Florida Express route map</ref> In 1987 [[American Airlines]] started nonstops to its Raleigh-Durham hub, but by October 1989 PIE had no scheduled airlines.

American Trans Air and Air South began flights in the mid 1990s but both ended operations at PIE in 2004, ATA downsizing due to bankruptcy and Southeast going defunct.

===Recent developments===
In September 2006 [[Allegiant Air]] announced scheduled service from St. Petersburg–Clearwater to cities in Iowa, Illinois, Indiana, Michigan, Missouri, New York, Ohio, Pennsylvania, South Carolina, Tennessee, and Virginia. Allegiant's destination count from PIE has increased to 21&nbsp;airports in the eastern United States. In February, the Lansing, Michigan service shifted to [[Grand Rapids, Michigan]], with four weekly flights.

The airport recently completed a US$22 million renovation, including, among other things, larger gates, new plumbing, and building [[Jetway|loading bridges]], as the previous system required all passengers to walk across the [[tarmac]] to the gate.<ref>[http://www.sptimes.com/2007/06/26/Northpinellas/_Other__airport_gets_.shtml 'Other' airport gets facelift], ''[[St. Petersburg Times]]'', June 26, 2007.</ref>

In January 2015 [[Silver Airways]] announced it was beginning service to PIE, but in March the company had cancelled its plans.<ref>"[http://www.tampabay.com/news/business/airlines/silver-airways-aborts-flights-to-st-pete-clearwater-international-airport/2223613 Silver Airways aborts flights to St. Pete-Clearwater International Airport]" ([http://www.webcitation.org/6XVSOW0Ga Archive]). ''[[Tampa Bay Times]]''. Thursday 31 March 2015. Retrieved on April 3, 2015.</ref>

== Facilities ==
{{multiple image|align=left|direction=vertical|footer_align=center|image1=PIE Runway27.jpg|width1=150|caption1=<center>Runway 27 (March 1988)</center>|image2=PIE Rwy 17L-35R.jpg|width2=150|caption2=<center>Runway 17L and taxiway/runway 17R (1988)</center>}}

The airport covers 1900 acres (769 [[hectare|ha]]) at an elevation of 11 feet (3 m). It has two asphalt [[runway]]s: 18/36 is 9,730 by 150 feet (2,966 x 46 m) with an [[Instrument Landing System|ILS]] approach, and 4/22 is 5,903 by 150 feet (1,799 x 46 m).<ref name="FAA" />

The airport is also the home of [[Coast Guard Air Station Clearwater]], the largest and busiest [[U.S. Coast Guard]] [[Coast Guard Air Station|Air Station]] in the United States, operating [[HC-130]] Hercules and [[HH-60J Jayhawk|MH-60T Jayhawk]] aircraft. The [[U.S. Army Reserve]] also maintains an Army Aviation Support Facility (AASF) at PIE immediately west of the approach end of Runway 17R for Companies A and F, 5th Battalion, 159th Aviation Regiment and Medical Evacuation Unit, operating [[UH-60 Blackhawk]] helicopters.

[[U.S. Customs and Border Protection]], the [[Federal Aviation Administration]] (FAA)-operated control tower, the FAA's Central Florida Region [[Flight Service Station|Automated Flight Service Station (AFSS)]]...the busiest AFSS in the United States...and the St. Petersburg [[VHF omnidirectional range|VORTAC]] for airways navigation are also important federal government services at the airport.

Along with scheduled passenger and charter airlines and military flight operations, [[United Parcel Service]] / [[UPS Airlines]], other air cargo, general/corporate aviation are also major activities, with UPS conducting extensive [[Boeing 757]]. The entire tract of the airport is designated as a [[Free port|Foreign Trade Zone]] (FTZ) and a large Airport Industrial Park developed in the 1980s is a major center of commerce. The airport and its tenants employ over 3,000&nbsp;people and have an economic benefit of more than $400&nbsp;million yearly to the Tampa Bay area.

The airport has a 24-hour airport rescue and fire-fighting ([[ARFF]]) department (Index C), along with operations, facilities, engineering, security, and administrative personnel.

For the year ending June 30, 2016, the airport had 105,273 aircraft operations, an average of 288 operations per day; with 72% [[general aviation]], 14% military, 11% commercial airline and 3% [[air taxi]]. In January 2017, there were 261 aircraft based at this airport: 100 single-engine, 31 multi-engine, 53 jet, 38 [[helicopter]], 38 military and 1 ultralight.<ref name="FAA" />

{{clear}}

== Airlines and destinations ==
[[Image:PIE Baggage Claim.jpg|thumb|Baggage-claim area, with replica of [[Benoist XIV]] [[flying boat]] flown by aviation pioneer [[Tony Jannus]] in 1914 (2009 photo)]]
[[Image:PIE Baggage Claim2.jpg|thumb|PIE's baggage-claim area has four baggage carousels (2009 photo)]]

St. Petersburg–Clearwater International Airport has one terminal and thirteen gates: 1–12 and 14.

===Passenger===
{{Airport destination list
| [[Allegiant Air]] | [[Lehigh Valley International Airport|Allentown]], [[Appleton International Airport|Appleton]], [[Asheville Regional Airport|Asheville]], [[Austin–Bergstrom International Airport|Austin]],<ref name="Allegiant Air">http://www.usatoday.com/story/travel/flights/todayinthesky/2016/08/30/allegiant-7-route-expansion-features-puerto-rico-austin-and-pittsburgh/89569998/</ref> [[Bangor International Airport|Bangor]], [[MidAmerica St. Louis Airport|Belleville/St. Louis]], [[Central Illinois Regional Airport|Bloomington/Normal]], [[The Eastern Iowa Airport|Cedar Rapids]], [[Chattanooga Metropolitan Airport|Chattanooga]], [[Chicago Rockford International Airport|Chicago/Rockford]], [[Cincinnati/Northern Kentucky International Airport|Cincinnati]], [[Rickenbacker International Airport|Columbus–Rickenbacker]], [[Concord Regional Airport|Concord (NC)]], [[Cleveland Hopkins International Airport|Cleveland]], [[Dayton International Airport|Dayton]], [[Des Moines International Airport|Des Moines]], [[Elmira Corning Regional Airport|Elmira]], [[Bishop International Airport|Flint]], [[Fort Wayne International Airport|Fort Wayne]], [[Gerald R. Ford International Airport|Grand Rapids]], [[Piedmont Triad International Airport|Greensboro]], [[Greenville-Spartanburg International Airport|Greenville/Spartanburg]], [[Harrisburg International Airport|Harrisburg]], [[Tri-State Airport|Huntington (WV)]], [[Indianapolis International Airport|Indianapolis]], [[Kansas City International Airport|Kansas City]], [[McGhee Tyson Airport|Knoxville]], [[Blue Grass Airport|Lexington]], [[Louisville International Airport|Louisville]] (begins May 24, 2017),<ref>http://www.routesonline.com/news/38/airlineroute/270813/allegiant-adds-new-routes-in-s17/</ref> [[Memphis International Airport|Memphis]], [[Louis Armstrong New Orleans International Airport|New Orleans]], [[Stewart International Airport|Newburgh]], [[Niagara Falls International Airport|Niagara Falls]], [[Eppley Airfield|Omaha]], [[General Wayne A. Downing Peoria International Airport|Peoria]], [[Pittsburgh International Airport|Pittsburgh]], [[Plattsburgh International Airport|Plattsburgh]], [[Raleigh-Durham International Airport|Raleigh/Durham]], [[Roanoke Regional Airport|Roanoke]],  [[Sioux Falls Regional Airport|Sioux Falls]], [[South Bend Regional Airport|South Bend]], [[Springfield-Branson National Airport|Springfield/Branson]], [[Syracuse Airport|Syracuse]], [[Toledo Express Airport|Toledo]], [[Trenton–Mercer Airport|Trenton]],<ref>http://www.routesonline.com/news/38/airlineroute/268137/allegiant-continues-w16-route-expansion/</ref> [[Tri-Cities Regional Airport|Tri-Cities (TN)]], [[Wilkes-Barre/Scranton International Airport|Wilkes–Barre/Scranton]], [[Youngstown-Warren Regional Airport|Youngstown/Warren]]<br>'''Seasonal''': [[Hector International Airport|Fargo]], [[Hagerstown Regional Airport|Hagerstown (MD)]], [[Quad City International Airport|Moline/Quad Cities]], [[Portsmouth International Airport at Pease|Portsmouth (NH)]],<ref name="Allegiant Air"/> [[Richmond International Airport|Richmond]], [[Tulsa International Airport|Tulsa]] 
| {{nowrap|[[Sun Country Airlines]]}} | '''Charter:''' [[Gulfport-Biloxi International Airport|Gulfport/Biloxi]]
| [[Sunwing Airlines]] | '''Seasonal:''' [[Halifax Stanfield International Airport|Halifax]], [[Ottawa International Airport|Ottawa]], [[Toronto Pearson International Airport|Toronto–Pearson]]
}}

===Cargo===
{{Airport destination list
| [[UPS Airlines]] | [[Greenville-Spartanburg International Airport|Greenville/Spartanburg]], [[Louisville International Airport|Louisville]], [[Orlando International Airport|Orlando]]
'''Seasonal''': [[Southwest Georgia Regional Airport|Albany (GA)]], [[Philadelphia International Airport|Philadelphia]]
}}

==Statistics==

===Airline market share===
{{Bar graph
| title = Carrier shares for (Jan 2014 - Dec 2014)<ref name="transtats.bts.gov" />
| bar_width   = 37
| width_units = em
| label_type  = Carrier&nbsp;&nbsp;
| data_type   = Passengers (arriving and departing)
| data_max    = 1,178,000
| label1      = [[Allegiant Air|Allegiant]]
| data1       = 1,178,000
| comment1    = 96.34%
| label3      = [[Vision Airlines|Vision]]
| data3       = 27,510
| comment3    = 2.25%
| label4      = [[Sun Country Airlines|Sun Country]]
| data4       = 17,290
| comment4    = 1.41%
}}

===Top domestic destinations===
{| class="wikitable sortable" style="font-size:95%;"
|+ Top domestic destinations (November 2015 - October 2016)<ref name="transtats.bts.gov">
  {{cite web
  | url = http://www.transtats.bts.gov/airports.asp?pn=1
  | title = St. Petersburg, FL: St. Petersburg-Clearwater International (PIE)
  | publisher = [[Bureau of Transportation Statistics]] (BTS), [[Research and Innovative Technology Administration]] (RITA), [[U.S. Department of Transportation]]
  | date = June 2015 | accessdate = May 2016
  }}
</ref>
! Rank
! City
! Airport
! Passengers
|-
| 1
| [[Cincinnati, OH]]
| [[Cincinnati/Northern Kentucky International Airport|Cincinnati/Northern Kentucky International Airport (CVG)]]
| 50,000
|-
| 2
| [[Grand Rapids, MI]]
| [[Gerald R. Ford International Airport|Gerald R. Ford International (GRR)]]
| 34,000
|-
| 3
| [[Indianapolis, IN]]
| [[Indianapolis International Airport|Indianapolis International Airport (IND)]]
| 31,000
|-
| 4
| [[Asheville, NC]]
| [[Asheville Regional Airport|Asheville (AVL)]]
| 29,000
|-
| 5
| [[Knoxville, TN]]
| [[McGhee Tyson Airport|McGhee Tyson Airport (TYS)]]
| 28,000
|-
| 6
| [[Pittsburgh, PA]]
| [[Pittsburgh International Airport|Pittsburgh International Airport (PIT)]]
| 27,000
|-
| 7
| [[Fort Wayne, IN]]
| [[Fort Wayne International Airport|Fort Wayne International Airport (FWA)]]
| 24,000
|-
| 8
| [[Lexington, KY]]
| [[Blue Grass Airport|Blue Grass Airport (LEX)]]
| 23,000
|-
| 9
| [[Huntington, WV]]
| [[Tri-State Airport|Tri-State Airport (HTS)]]
| 23,000
|-
| 10
| [[South Bend, IN]]
| [[South Bend International Airport|South Bend International (SBN)]]
| 22,000
|}

===Annual traffic===
{| class="wikitable sortable" style="font-size: 95%"
|+ '''Annual passenger traffic (enplaned + deplaned) at St Pete-Clearwater Airport, 1992 thru 2014<ref>[http://www.fly2pie.com/news-media/passenger-statistics-reports Passenger Statistics & Reports. Retrieved on March 6, 2015.]</ref>
! Year
! Passengers
! Year
! Passengers
! Year
! Passengers
|-
| ||||2010||776,087||2000||734,940
|-
| ||||2009||776,535||1999||791,973
|-
| ||||2008||742,380||1998||911,195
|-
| ||||2007||747,369||1997||883,086
|-
| 2016||1,837,035||2006||389,997||1996||1,045,928
|-
| 2015||1,645,402||2005||596,510||1995||1,086,051
|-
| 2014||1,247,987||2004||1,333,069||1994||721,977
|-
| 2013||1,017,049||2003||997,761||1993||561,322
|-
| 2012||865,942||2002||623,959||1992||397,940
|-
| 2011||833,068||2001||637,310||||
|}

== Accidents and incidents ==
On June 6, 1982, a [[Douglas C-47]]A (N95C), of Fromhagen Aviation, was written off after the starboard engine failed during the takeoff for a training flight. All five people aboard survived.<ref name=ASN060682>{{cite web | url = http://aviation-safety.net/database/record.php?id=19820606-0 | title = N95C Accident report | publisher = Aviation Safety Network | accessdate = July 25, 2010}}</ref>

On September 30, 2015, the pilot of a [[Piper PA-30 Twin Comanche|Piper PA-30]], registered to Jet Aircraft Management, crashed and died while practicing takeoffs and landings.<ref>{{Cite web|url=http://www.baynews9.com/content/news/baynews9/news/article.html/content/news/articles/bn9/2015/9/30/_1_dead_in_small_pla.html|title=Pilot killed in small plane crash at St. Pete-Clearwater International Airport|last=|first=|date=September 30, 2015|website=Bay News 9|publisher=|accessdate=2016-12-28}}</ref><ref>{{Cite web|title = Pilot dies in plane crash at St. Pete-Clearwater Airport|url = http://www.wtsp.com/story/news/local/2015/09/30/small-plane-crash-reported-stpete-clearwater-airport/73086884/|website = 10NEWS|accessdate = 2015-10-25}}</ref>

On June 1, 2016, Allegiant Air flight 871 made an emergency landing at the airport, its scheduled destination. The plane departed from Moline, Illinois. No injuries were reported, and the airline declined to comment on the mechanical failure.<ref>{{cite web|url=http://www.tampabay.com/news/business/airlines/allegiant-flight-declares-emergency-at-st-pete-clearwater-airport/2279881|title=Allegiant flight declares emergency at St. Pete-Clearwater Airport|last=Staff|first=|date=June 1, 2016|website=Tampa Bay Times|publisher=|accessdate=2016-12-28}}</ref>

== See also ==
* [[Coast Guard Air Station Clearwater]]
* [[List of airports in the Tampa Bay Area]]
* [[Pinellas Army Air Field]]

{{Portal|Florida|Aviation}}

== References ==
{{reflist}}
;Other sources
{{refbegin}}
* {{Air Force Historical Research Agency}}
* Bickel, Karl A. – ''The Mangrove Coast'', 1942 by Coward McCann, Inc., Fourth Edition in 1989 by Omni Print Media, Inc., p.&nbsp;265
{{refend}}

== External links ==
{{Commons category|St. Petersburg-Clearwater International Airport}}
* [http://www.fly2pie.com/ St. Petersburg–Clearwater International Airport]
* [http://www.amfly.com/atc_live.htm St. Petersburg–Clearwater International Airport real-time ATC feed]
* {{FAA-diagram|00625}}
* {{FAA-procedures|PIE}}
* {{US-airport|PIE}}

{{Florida airports}}

{{DEFAULTSORT:Saint Petersburg-Clearwater International Airport}}
[[Category:1941 establishments in Florida]]
[[Category:Airports in Florida]]
[[Category:Airports in the Tampa Bay area]]
[[Category:Transportation in Pinellas County, Florida]]
[[Category:Transportation in the Tampa Bay Area]]
[[Category:Clearwater, Florida]]
[[Category:Buildings and structures in St. Petersburg, Florida]]
[[Category:Transportation in St. Petersburg, Florida]]
[[Category:Airports established in 1941]]