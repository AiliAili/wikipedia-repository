{{Italic title}}
'''''GeneReviews''''' is an online database containing standardized [[Peer review|peer-reviewed]] articles that describe specific [[heritable disease]]s. It was established in 1997 as ''GeneClinics'' by Roberta A Pagon ([[University of Washington]]) with funding from the [[National Institutes of Health]].<ref>{{cite journal |doi=10.1002/humu.10069 |title=GeneTests-GeneClinics: Genetic testing information for a growing audience |year=2002 |last1=Pagon |first1=Roberta A. |last2=Tarczy-Hornoch |first2=Peter |last3=Baskin |first3=Patricia K. |last4=Edwards |first4=Joseph E. |last5=Covington |first5=Maxine L. |last6=Espeseth |first6=Miriam |last7=Beahler |first7=Christine |last8=Bird |first8=Thomas D. |last9=Popovich |first9=Bradley |journal=Human Mutation |volume=19 |issue=5 |pages=501–9 |pmid=11968082 }}</ref> Its focus is primarily on single-gene disorders, providing current disorder-specific information on diagnosis, management, and genetic counseling. Links to disease-specific and/or general consumer resources are included in each article when available. The database is published on the [[National Center for Biotechnology Information]] Bookshelf site. Articles are updated every two or three years or as needed, and revised whenever significant changes in clinically relevant information occur. Articles are searchable by author, title, gene, and name of disease or protein, and are available free of charge.<ref>{{cite web |title=GeneReviews |url=http://www.genereviews.org |accessdate=2013-06-13 }}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.ncbi.nlm.nih.gov/sites/GeneTests/review}}

[[Category:Medical databases]]
{{genetics-stub}}
{{website-stub}}