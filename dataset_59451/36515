{{about|the roller coaster|the mythological chariot of Apollo|Solar deity}}
{{Good article}}
{{Infobox roller coaster
| name                 = Apollo's Chariot
| logo                 = Apollo's Chariot logo.png
| logodimensions       = 180px
| image                = Apollo's Chariot (Busch Gardens Europe) 01.jpg
| imagedimensions      = 250px
| caption              = The first drop of ''Apollo's Chariot'' is at an angle of 65°.
| previousnames        = 
| location             = Busch Gardens Williamsburg
| locationarticle      = <!--Must not be linked.-->
| section              = Festa Italia
| subsection           = <!--Should be linked.-->
| coordinates          = {{coord|37.23480|N|76.64130|W|type:landmark|display=title,inline}}
| status               = Operating
| opened               = {{Start date|1999|3|30}}
| soft_opened          = {{Start date|1999|3|27}}
| year                 = 1999
| closed               = <!--Use {{End date|YYYY|MM|DD}} -->
| cost                 = US$20 million
| previousattraction   = 
| replacement          = 
| type                 = Steel
| type2                = <!--Must not be linked, will auto-categorize the coaster.-->
| type3                = <!--Must not be linked, will auto-categorize the coaster.-->
| manufacturer         = Bolliger & Mabillard
| designer             = [[Werner Stengel]]
| model                = [[Hyper Coaster]]
| track                = 
| lift                 = [[Chain lift hill]]
| height_ft            = 170
| drop_ft              = 210
| length_ft            = 4882
| speed_mph            = 73
| inversions           = 0
| duration             = 2:15
| angle                = 65
| capacity             = 1,750
| acceleration         = <!--Expression in full form e.g. "X to Y mph (χ to ψ km/h) in Z seconds". -->
| acceleration_from    = <!--Initial speed in mph or km/h defaults to zero, only numeric characters-->
| acceleration_mph     = <!--Final speed in mph may contain only numeric characters-->
| acceleration_km/h    = <!--Final speed in km/h may contain only numeric characters-->
| acceleration_in      = <!--Number of seconds may contain words -->
| gforce               = 4.1
| restriction_in       = 52
| trains               = 3
| carspertrain         = 9
| rowspercar           = 1
| ridersperrow         = 4
| virtual_queue_name   = Quick Queue
| virtual_queue_image  = Quick Queue availability logo (Busch Gardens).svg
| virtual_queue_status = available
| single_rider         = available
| accessible           = <!--Must be "available" if available.-->
| transfer_accessible  = <!--Must be "available" if available.-->
| custom_label_1       = 
| custom_value_1       = 
| custom_label_2       = 
| custom_value_2       = 
| custom_label_3       = 
| custom_value_3       = 
| custom_label_4       = 
| custom_value_4       = 
| rcdb_number          = 531
}}

'''Apollo's Chariot''' is a [[steel roller coaster]] at the [[Busch Gardens Williamsburg]] [[theme park]] in [[James City County, Virginia]], United States. The ride was the first [[Hyper Coaster]] designed by [[Switzerland|Swiss]] firm [[Bolliger & Mabillard]]. It officially opened to the public on March 30, 1999.

The {{convert|4882|ft|m|adj=mid|-long}} ride is characterised by eight [[air-time]] hills, with heights ranging between {{Convert|49|and|131|ft|m}}. Riders ascend {{Convert|170|ft}} on the [[chain lift hill]] before dropping {{Convert|210|ft}} at an angle of 65°. ''Apollo's Chariot'' is generally well received with it consistently rating highly in industry rankings.

==History==
''Apollo's Chariot'' was announced on September 5, 1998, as the tallest and fastest roller coaster at [[Busch Gardens Williamsburg]].<ref name=Announcement>{{cite news|last=Deacle|first=Robin|title=New Ride Not For Mere Mortals|url=http://articles.dailypress.com/1998-09-05/news/9809050076_1_busch-gardens-chariot-facts-ride|accessdate=January 28, 2013|newspaper=[[Daily Press (Virginia)|Daily Press]]|date=September 5, 1998}}</ref> On January 23, 1999, the ride was nearing completion with approximately 20 pieces of track left to be installed.<ref name="January construction">{{cite news|last=Deacle|first=Robin|title=Sweet Chariot|url=http://articles.dailypress.com/1999-01-23/news/9901230059_1_roller-coaster-apollo-s-chariot-drops|accessdate=January 8, 2013|newspaper=[[Daily Press (Virginia)|Daily Press]]|date=January 23, 1999}}</ref> ''Apollo's Chariot'' opened as the first [[Hyper Coaster]] from Swiss manufacturer, [[Bolliger & Mabillard]].<ref name="HC search">{{cite RCDB|coaster_name=Roller Coaster Search Results|location=Hyper Coaster|url=http://rcdb.com/r.htm?ot=2&mo=8314|accessdate=February 10, 2013}}</ref>

Busch Gardens held ''Apollo's Chariot''{{'}}s opening ceremony on March 30, 1999. Italian fashion model [[Fabio Lanzoni]] was brought in to promote the new roller coaster. During the ride's inaugural run, a 10-pound goose struck him in the face leaving his nose covered with blood. He was treated at a nearby hospital for minor cuts.<ref>{{cite news|last=Rivenberg|first=Roy|title=Fabio Survives Goose Encounter, but Take a Gander at His Honker|url=http://articles.latimes.com/1999/apr/09/news/cl-25542|accessdate=June 9, 2015|newspaper=[[Los Angeles Times]]|date=April 9, 1999}}</ref>

==Characteristics==
The {{convert|4882|ft|m|adj=mid|-long}} ''Apollo's Chariot'' is a Bolliger & Mabillard Hyper Coaster. The park's existing [[Terrain roller coaster|terrain]] is utilised to allow a {{Convert|170|ft|m|adj=mid|-tall}} [[lift hill]] to be translated into a first drop stretching {{Convert|210|ft}}. With a top speed of {{Convert|73|mph}}, the ride features eight [[air-time]] hills. Riders of ''Apollo's Chariot'' experience up to 4.1 times the [[G-force|force of gravity]] on the 2-minute, 15-second ride. ''Apollo's Chariot'' operates with three trains with nine cars per train. Riders are arranged four across in a single row for a total of 36 riders per train. This configuration of trains allows for a theoretical capacity of 1,750 riders per hour.<ref name="RCDB">{{Cite RCDB|coaster_name=Apollo's Chariot|location=Busch Gardens Williamsburg|rcdb_number=531|accessdate=June 28, 2012}}</ref> Riders are restrained by a lapbar and the seats are elevated so riders’ feet don't touch the ground.<ref name=Announcement />

==Ride experience==
{{Stack|[[File:Apollos Chariot small drop.jpg|thumb|The final drop on ''Apollo's Chariot'']]}}
After departing from the [[station (roller coaster)|station]], the [[train (roller coaster)|train]] begins to climb the {{convert|170|ft|adj=on}} [[chain lift hill]]. When the train reaches the top, it drops down a few feet in a [[pre-drop]]. The pre-drop serves to reduce the stress and pull of the chain. After the pre-drop, the train goes down a {{convert|210|ft|m}} drop toward a water-filled ravine at a 65 degree angle and reaching a top speed of {{convert|73|mph}}. At the end of the ravine, the train enters a second airtime hill with a {{convert|131|ft|adj=on}} drop. A short narrow above ground tunnel is at the bottom of the second drop. After the tunnel, the train descends a {{convert|144|ft|adj=on}} drop, which banks to the left as it descends. The train then goes through an upward helix. Coming out of the helix, the train drops {{convert|102|ft|m}} then turns right and rises up into the [[mid-course brake run]]. The train drops {{convert|48|ft|m}} out of the brake run followed by another drop at {{convert|87|ft|m}} toward the ravine. The train then banks right, makes a {{convert|38|ft|adj=on}} dip, turns left and goes through a small {{convert|16|ft|adj=on}} dip. The train then makes one last {{convert|49|ft|adj=on}} airtime drop before climbing up and into the final [[brake run]].<ref name=POV>{{cite web|title=Apollo's Chariot Front Seat on-ride widescreen POV Busch Gardens Williamsburg|work=Coaster Force|date=17 October 2009|url=https://www.youtube.com/watch?v=hGTOGzKMTbI|publisher=YouTube|accessdate=January 28, 2013}}</ref><ref name=CN>{{cite web|title=Apollo's Chariot at Coaster-Net|url=http://www.coaster-net.com/ride-gallery/6-apollo-s-chariot/|publisher=Coaster-Net|accessdate=January 28, 2013}}</ref> There is approximately 26 seconds of airtime during the 2 minute and 15 second ride.<ref name="Ride screams fun">{{cite news|last=McDonald|first=Sam|title=Apollo's Ride Screams Fun|url=http://articles.dailypress.com/1999-04-02/entertainment/9904010089_1_coaster-busch-garden-apollo-s-chariot-s|accessdate=January 28, 2013|newspaper=[[Daily Press (Virginia)|Daily Press]]|date=April 2, 1999}}</ref>

==Reception==
In the ride's debut year, Busch Gardens Williamsburg had lower attendance levels than they were expecting. This was attributed to prolonged periods of inclement weather. As a result of this, the park planned to re-launch Apollo's Chariot in 2000 in an attempt to drive attendance.<ref name="Amusement Business">{{cite journal|last=Powell|first=Tom|title=Williamsburg Busch Gardens Unveils 'Huge' Halloween Promo|journal=Amusement Business|date=September 13, 1999|volume=111|issue=37|page=33}}</ref>

In ''[[Amusement Today]]''{{'}}s annual [[Golden Ticket Awards]], ''Apollo's Chariot'' has consistently ranked highly. In its debut year, it ranked position 20. In the 13 years since, the coaster has consistently ranked higher, peaking at #4 in 2005, 2007, 2008 and 2012.<ref name="GTA1999" /><ref name="GTA2012" />

{{GTA table
| type       = steel
| accessdate = September 12, 2016
| 1999       = 20
| 2000       = 9
| 2001       = 7
| 2002       = 5
| 2003       = 5
| 2004       = 5
| 2005       = 4
| 2006       = 5
| 2007       = 4
| 2008       = 4
| 2009       = 5
| 2010       = 4
| 2011       = 7
| 2012       = 4
| 2013       = 5
| 2014       = 7
| 2015       = 6
| 2016       = 6
}}

In Mitch Hawker's worldwide Best Roller Coaster Poll, ''Apollo's Chariot'' entered at position 6 in 1999, before dropping to a low of 32 in 2012. The ride's ranking in subsequent polls is shown in the table below.<ref name=HawkerPoll>{{cite web|url=http://www.ushsho.com/steelpoll12yeartable2012.htm |title=Steel Roller Coaster Poll 12 Year Results Table (1999 - 2012)|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=August 4, 2013}}</ref>

{| class="wikitable"
!  style="text-align:center; background:white;" colspan="500"|Mitch Hawker's Best Roller Coaster Poll: Best Steel-Tracked Roller Coaster<ref name=HawkerPoll />
|- style="background:#white;"
! style="text-align:center;"|Year!!1999!!2000!!2001!!2002!!2003!!2004!!2005!!2006!!2007!!2008!!2009!!2010!!2011!!2012
|-
!  style="text-align:center; background:#white;"|Ranking
|<center>4</center>
|{{N/A}}
|<center>6</center>
|<center>8</center>
|<center>13</center>
|<center>7</center>
|<center>14</center>
|<center>18</center>
|<center>12</center>
|<center>20</center>
|<center>16</center>
|<center>22</center>
|{{N/A}}
|<center>32</center>
|}

==References==
{{Reflist|30em}}

==External links==
{{commons category}}
* {{Official website|http://seaworldparks.com/en/buschgardens-williamsburg/Attractions/Rides/Apollos-Chariot}}
* {{RCDB|531}}

{{Busch Gardens Williamsburg}}

{{DEFAULTSORT:Apollo's Chariot}}
[[Category:Busch Gardens Williamsburg]]
[[Category:Steel roller coasters]]
[[Category:Roller coasters in Virginia]]
[[Category:Roller coasters introduced in 1999]]
[[Category:Roller coasters manufactured by Bolliger & Mabillard]]
[[Category:Roller coasters operated by SeaWorld Parks & Entertainment]]