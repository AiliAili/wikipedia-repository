{{italictitle}}
{{Infobox journal
| title         = Early Music History
| cover         = [[File:Early Music History.jpg]]
| editor        = [[Iain Fenlon]]
| discipline    = Music
| frequency     = Annually
| abbreviation  =
| impact        =
| impact-year   =
| publisher     = [[Cambridge University Press]]
| country       = [[United Kingdom]]
| history       = 1981-present
| website       = http://journals.cambridge.org/action/displayJournal?jid=EMH
| link1         =
| link1-name    =
|JSTOR = 02611279
| ISSN          = 0261-1279
| eISSN         = 1474-0559
| OCLC = 49342621
| LCCN = 2007-233702
}}
'''''Early Music History''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published annually by [[Cambridge University Press]], which specialises in the study of [[music]] from the [[early Middle Ages]] to the end of the 17th century. It was established in 1981 and is edited by [[Iain Fenlon]].
{{cquote|''Early Music History'' exists to stimulate further exploration of familiar phenomena through unfamiliar means, and to add to the growing appreciation of the value of interdisciplianary approaches and their potentialities.  Another emphasis might be called the contextual.  At present some music historians tend to concentrate on the internal analysis of a composition and its relation to a specific and usually narrowly defined historical frame.  Indeed, much musicological writing presents by implication a formidable orthodoxy in which history is perceived as a succession of paradigms of musical language, style, and form.  Some recent work has attempted explanation through exploration of a wider range of evidence, and Early Music History intends to strengthen this trend by encouraging studies that examine the economic, political, and social ramifications of research.  In that musicology itself can on benefit if its vision is extended and its methods refined and broadened, the board believes that ''Early Music History'' will mark a new departure in the development of the discipline while continuing to support its traditional tasks.|15px|15px|Iain Fenlon, preface to Volume I<ref>{{cite journal |author=Iain Fenlon |title=Preface |journal=Early Music History |volume=1 |pages=vii-vii |year=1981|doi= 10.1017/S0261127900000243|url= |issn=1474-0559}}</ref>}}

==References==
<references/>

[[Category:Cambridge University Press academic journals]]
[[Category:Music journals]]
[[Category:Publications established in 1981]]
[[Category:Annual journals]]
[[Category:English-language journals]]


{{history-journal-stub}}
{{music-publication-stub}}