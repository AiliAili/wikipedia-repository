{{Infobox journal
| title = Biosemiotics
| cover = [[File:Biosemiotics 2016 cover.jpg]]
| former_name = <!-- or |former_names= -->
| abbreviation = Biosemiotics
| discipline = [[Biosemiotics]]
| editor = Timo Maran, Alexei Sharov, Morten Tønnessen
| publisher = [[Springer Science+Business Media]]
| country = 
| history = 2008-present
| frequency = Triannually
| openaccess = 
| license = 
| impact = 0.391
| impact-year = 2015
| ISSN = 1875-1342
| eISSN = 1875-1350
| CODEN = 
| JSTOR = 
| LCCN = 
| OCLC = 263367598
| website = http://www.springer.com/life+sci/journal/12304
| link2 = http://link.springer.com/journal/volumesAndIssues/12304
| link2-name = Online archive
}}
'''''Biosemiotics''''' is a triannual [[peer-reviewed]] [[scientific journal]] on [[biosemiotics]] published by [[Springer Science+Business Media]]. It was established in 2008 with 3 issues per year and is an official journal of the ''[[International Society for Biosemiotic Studies]]''. The [[editors-in-chief]] are Timo Maran ([[University of Tartu]]), Alexei Sharov ([[National Institute on Aging]]), and Morten Tønnessen ([[University of Stavanger]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Academic OneFile]]
*[[Arts and Humanities Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-11-08}}</ref>
*[[Current Contents]]/Arts and Humanities<ref name=ISI/>
*Current Contents/Social & Behavioral Sciences<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-11-08}}</ref>
*[[Science Citation Index Expanded]]<ref name=ISI/>
*[[Social Science Citation Index]]<ref name=ISI/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.391.<ref name=WoS>{{cite book |year=2016 |chapter=Biosemiotics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.springer.com/life+sci/journal/12304}}

[[Category:Semiotics journals]]
[[Category:Biology journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 2008]]
[[Category:Triannual journals]]


{{journal-stub}}