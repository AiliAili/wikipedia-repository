{{Use British English|date=September 2011}}
{{Use dmy dates|date=June 2011}}
{{Infobox person
| name        = Felice Beato
| image       = Felice Beato.jpg
| alt         = A head-and-shoulders photograph of Felice Beato in profile. He is facing towards the left of the frame and has a full beard.
| caption     = Felice, by an unknown photographer (possibly a self-portrait), c. 1866
| birth_name  = 
| birth_date  = 1832
| birth_place = [[Venice]], [[Kingdom of Lombardy–Venetia]]
| death_date  = 29 January 1909
| death_place = [[Florence]], [[Kingdom of Italy]]
| nationality = British
| other_names = Felix Beato
| occupation  = Photographer
| known_for   = One of the first people to take photographs in East Asia and one of the first [[war photography|war photographers]]
| notable_works  = 
| relatives   = [[Antonio Beato]] (brother)
}}
'''Felice Beato''' (1832 – 29 January 1909), also known as '''Felix Beato''',{{refn|It appears that Felice was his original name. However, he tended to prefer Felix.<ref name="bennettpj86" /> During his lifetime he was often referred to in print as "Signor Beato", and his surname was often misspelled ("Beat", "Beatto", etc).<ref>Clark, Fraser, and Osman, ''[[passim]].''</ref>|group="note"}} was an [[Italian Briton|Italian–British]] photographer. He was one of the first people to take photographs in East Asia and one of the first [[war photography|war photographers]]. He is noted for his [[genre works]], portraits, and views and [[panoramic photography|panoramas]] of the architecture and landscapes of Asia and the [[Mediterranean Sea|Mediterranean]] region. Beato's travels gave him the opportunity to create images of countries, people, and events that were unfamiliar and remote to most people in Europe and North America. His work provides images of such events as the [[Indian Rebellion of 1857]] and the [[Second Opium War]], and represents the first substantial [[wiktionary:oeuvre|oeuvre]] of [[photojournalism]]. He influenced other photographers, and his influence in Japan, where he taught and worked with numerous other photographers and artists, was particularly deep and lasting.

==Early life and identity==
A death certificate discovered in 2009 shows that Beato was born in [[Venice]] in 1832 and died on 29 January 1909 in [[Florence]]. The death certificate also indicates that he was a [[British subject]] and a bachelor.<ref name="BennettHistory">Bennett, ''History of Photography in China, 1842–1860,'' p. 241.</ref>{{refn|Recent scholarship had uncovered an application by Beato for a travel permit in 1858 that included information suggesting he was born in 1833 or 1834 on the island of Corfu.<ref name="Dobson">Dobson, "'I been to keep up my position'", p. 31.</ref> <!--Earlier sources had given his birth date as 1825 or ca. 1825, but these dates may have been confused references to the possible birth date of his brother, [[Antonio Beato|Antonio]]. However, the death certificate discovered in 2009 provides the first definitive evidence of Beato's dates and places of birth and death.-->|group="note"}} It is likely that early in his life Beato and his family moved to [[Corfu]], at the time part of the British [[protectorate]] of the [[Ionian Islands]], and so Beato was a British subject.<ref name="BennettHistory" /><ref name="Dobson" />{{refn|Beato has long been described as British, Italian, [[Corfiot Italians|Corfiot Italian]], and/or Greek. The movements of his family and of early nineteenth century history in the [[Adriatic Sea|Adriatic]] mean that he can justifiably be described by all these terms. Corfu was on and off part of Venetian territory from 1386 until 1815, when the [[Treaty of Paris (1815)|Treaty of Paris]] placed it and the other Ionian Islands under British protection. Corfu was ceded to Greece in 1864. A line of the Beato family is recorded as having moved to Corfu in the 17th century and was one of the noble Venetian families that ruled the island during the [[Republic of Venice]].<ref>Gray, p. 68.</ref>|group="note"}}

Because of the existence of a number of photographs signed "Felice Antonio Beato" and "Felice A. Beato", it was long assumed that there was one photographer who somehow photographed at the same time in places as distant as Egypt and Japan. In 1983 it was shown by Chantal Edel<ref>Zannier, ''Antonio e Felice Beato,'' n.p.</ref> that "[[Felice A. Beato|Felice Antonio Beato]]" represented two brothers, Felice Beato and Antonio Beato, who sometimes worked together, sharing a signature. The confusion arising from the signatures continues to cause problems in identifying which of the two photographers was the creator of a given image.

==Mediterranean, the Crimea and India==
Little is certain about Felice Beato's early development as a photographer, though it is said that he bought his first and only lens in Paris in 1851.<ref name="Clark">Clark, Fraser, and Osman, p. 90.</ref> He probably met the British photographer [[James Robertson (photographer)|James Robertson]] in Malta in 1850 and accompanied him to [[Constantinople]] in 1851. James Robertson (1813–88),  became his brother-in-law in 1855. Superintendent of the Imperial Mint, Robertson opened one of the first commercial photography studios in the capital between 1854 and 1856. Robertson had been an engraver at the Imperial Ottoman Mint since 1843 and had probably taken up photography in the 1840s.<ref>Broecker, p. 58; Clark, Fraser, and Osman, pp. 89, 90.</ref> In 1853 the two began photographing together and they formed a partnership called "Robertson & Beato" either in that year or in 1854, when Robertson opened a photographic studio in [[Beyoğlu|Pera]], Constantinople. Robertson and Beato were joined by Beato's brother Antonio on photographic expeditions to [[Malta]] in 1854 or 1856 and to Greece and [[Jerusalem]] in 1857. A number of the firm's photographs produced in the 1850s are signed "Robertson, Beato and Co.", and it is believed that the "and Co." refers to Antonio.<ref>Pare, ''Photography and Architecture,'' p. 245 (citing Vaczek and Buckland, p. 190); Clark, Fraser, and Osman, pp. 90–91.</ref>

[[File:Image-Secundra Bagh after Indian Mutiny higher res.jpg|thumb|left|alt=A photograph of the ruins of a palace with human skeletal remains in the foreground|The ruins of Sikandar Bagh palace showing the skeletal remains of rebels in the foreground, Lucknow, India, 1858]]

In late 1854 or early 1855 James Robertson married Beato's sister, Leonilda Maria Matilda Beato. They had three daughters, Catherine Grace (b. 1856), Edith Marcon Vergence (b. 1859), and Helen Beatruc (b. 1861).<ref name="Clark"/>

In 1855 Felice Beato and Robertson travelled to [[Balaklava]], Crimea, where they took over reportage of the [[Crimean War]] following [[Roger Fenton]]'s departure. In contrast to Fenton's depiction of the dignified aspects of war, Beato and Robertson showed the destruction.<ref>Greenough, p. 21; Pare, "Roger Fenton", p. 226.</ref> They photographed the fall of [[Sevastopol]] in September 1855, producing about 60 images.<ref>Broecker, p. 58.</ref>

In February 1858 Felice Beato arrived in [[Kolkata|Calcutta]] and began travelling throughout [[Indo-Gangetic Plain|Northern India]] to document the aftermath of the [[Indian Rebellion of 1857]].<ref>Harris, p. 23; Dehejia, p. 121; Masselos and Gupta, p. 1.</ref>{{refn|Gernsheim states that Beato and Robertson ''both'' travelled to India in 1857, but it is now generally accepted that Beato travelled there alone.<ref>Gernsheim, p. 96.</ref>|group="note"}} During this time he produced possibly the first-ever photographic images of corpses.<ref name="Zannier, Beato, 447">Zannier, "Beato", p. 447.</ref> It is believed that for at least one of his photographs taken at the palace of [[Sikandar Bagh]] in [[Lucknow]] he had the skeletal remains of Indian rebels disinterred or rearranged to heighten the photograph's dramatic impact<ref>Gartlan, "Felice Beato", p. 128.</ref> (see [[#Taku Forts|events at Taku Forts]]). He was also in the cities of [[Delhi]], [[Kanpur|Cawnpore]], [[Meerut]], [[Varanasi|Benares]], [[Amritsar]], [[Agra]], [[Shimla|Simla]], and [[Lahore]].<ref>Harris, p. 23; Clark, Fraser, and Osman, pp. 91–92.</ref> Beato was joined in July 1858 by his brother Antonio, who later left India, probably for health reasons, in December 1859. Antonio ended up in Egypt in 1860, setting up a photographic studio in [[Thebes, Egypt|Thebes]] in 1862.<ref>Clark, Fraser, and Osman, pp. 90, 91.</ref>

==China==
In 1860 Felice Beato left the partnership of Robertson & Beato, though Robertson retained use of the name until 1867. Beato was sent from India to photograph the Anglo-French military expedition to China in the [[Second Opium War]]. He arrived in [[Hong Kong]] in March<ref>Clark, Fraser, and Osman, p. 92.</ref> and immediately began photographing the city and its surroundings as far as [[Guangzhou|Canton]].<ref name="cfo92-93">Clark, Fraser, and Osman, pp. 92–93.</ref> Beato's photographs are some of the earliest taken in China.<ref>Rosenblum, p. 124.</ref>

While in Hong Kong, Beato met [[Charles Wirgman]], an artist and correspondent for the ''[[Illustrated London News]].'' The two accompanied the Anglo-French forces travelling north to [[Dalian Bay|Talien Bay]], then to [[Beitang, Tanggu District|Pehtang]] and the [[Taku Forts]] at the mouth of the [[Hai He|Peiho]], and on to [[Beijing|Peking]] and [[Summer Palace|Qingyi Yuan]], the suburban Summer Palace.<ref name="cfo92-93" /> For places on this route and later in Japan, Wirgman's (and others') illustrations for the ''Illustrated London News'' would often be derived from Beato's photographs.{{refn|For an anthology of the illustrations (as well as the texts, although in Japanese translation only) of the ''Illustrated London News''{{'}} Japanese material, see Kanai.|group="note"}}

===Taku Forts===
[[File:Felice Beato (British, born Italy) - (Interior of the Angle of Taku North Fort Immediately After Its Capture by Storm) - Google Art Project.jpg|thumb|alt=The interior of an earthen and wooden fort with dead bodies scattered around it|Interior of Fort Taku immediately after capture 21 August 1860]]

Beato's photographs of the Second Opium War are the first to document a military campaign as it unfolded,<ref name="lacoste10">Lacoste, p. 10.</ref> doing so through a sequence of dated and related images. His photographs of the Taku Forts represent this approach on a reduced scale, forming a narrative recreation of the battle. The sequence of images shows the approach to the forts, the effects of bombardments on the exterior walls and fortifications, and finally the devastation within the forts, including the bodies of dead Chinese soldiers.<ref name="lacoste10" /> The photographs were not taken in this order, as the photographs of dead Chinese had to be taken first—before the bodies were removed; only then was Beato free to take the other views of the exterior and interior of the forts.<ref>Harris.</ref>

Beato's images of the Chinese dead and his manner of producing them particularly reveal the ideological aspects of his photojournalism. Dr. David F. Rennie, a member of the expedition, noted in his campaign memoir, "I walked round the ramparts on the west side. They were thickly strewn with dead—in the north-west angle thirteen were lying in one group around a gun. Signor Beato was here in great excitement, characterising the group as 'beautiful,' and begging that it might not be interfered with until perpetuated by his photographic apparatus, which was done a few minutes afterwards."<ref>Quoted in "Felice Beato" (Luminous Lint).</ref>

===Summer Palace===
[[File:Yuanmingyuan before the burning, Beijing, 6–18 October, 1860.jpg|thumb|Wenchang Pavilion aka. Wenchang Tower (文昌阁) of the [[Summer Palace]] (Yihe Yuan), before being burnt down, October 1860]]

Just outside Peking, Beato took photographs at Yihe Yuan (the Summer Palace), a private estate of the Emperor of China comprising palace pavilions, temples, a large artificial lake, and gardens. Some of these photographs, taken between 6 and 18 October 1860, are unique images of buildings that were plundered and looted by the Anglo-French forces beginning on 6 October. On 18 and 19 October the buildings were torched by the British First Division on the orders of [[James Bruce, 8th Earl of Elgin|Lord Elgin]] as a reprisal against the emperor for the torture and deaths of twenty members of an Allied diplomatic party. Bennett writes that "These [photographs] appear to be the earliest images of Peking so far discovered, and are of the utmost historical and cultural importance."<ref>Bennett, "China", pp. 292–295.</ref>

Among the last photographs that Beato took in China at this time were portraits of Lord Elgin, in Peking to sign the [[Convention of Peking]], and [[Prince Gong (Qing dynasty)|Prince Kung]], who signed on behalf of the [[Xianfeng Emperor]].<ref name="lacoste10" />

Beato returned to England in October 1861, and during that winter he sold 400 of his photographs of India and China to [[Henry Hering]], a London commercial portrait photographer.<ref>Lacoste, pp. 11, 184.</ref>

==Japan==
[[File:Satsuma-samurai-during-boshin-war-period.jpg|thumb|left|[[Samurai]] of the Satsuma clan, during the [[Boshin War]] period (1868–1869)]]

By 1863 Beato had moved to [[Yokohama]], Japan, joining [[Charles Wirgman]], with whom he had travelled from Bombay to Hong Kong.<ref name="Bennett 2006, 94">Bennett, ''Photography in Japan, 1853–1912,'' p. 94.</ref> The two formed and maintained a partnership called "Beato & Wirgman, Artists and Photographers" during the years 1864–1867,<ref>Clark, Fraser, and Osman, p. 97; Clark, "A Chronology of Charles Wirgman (1832?–1891)", p. 35.</ref> one of the earliest<ref>Dobson, "Japan", p. 770.</ref> and most important<ref name="Zannier, Beato, 447"/> commercial studios in Japan. Wirgman again produced illustrations derived from Beato's photographs, while Beato photographed some of Wirgman's sketches and other works. (Beato's photographs were also used for engravings within Aimé Humbert's ''Le Japon illustré''<ref>Clark, Fraser, and Osman, pp. 96, 98, 100; Hockley, pp. 77–80.</ref> and other works.) Beato's Japanese photographs include portraits, genre works, landscapes, cityscapes, and a series of photographs documenting the scenery and sites along the [[Tōkaidō (road)|Tōkaidō Road]], the latter series recalling the [[ukiyo-e]] of [[Hiroshige]] and [[Hokusai]]. During this period, foreign access to (and within) the country was greatly restricted by the [[Shogun]]ate. Accompanying ambassadorial delegations<ref name="Gartlan, Felix Beato, 129">Gartlan, "Felix Beato", p. 129.</ref> and taking any other opportunities created by his personal popularity and close relationship with the British military, Beato reached areas of Japan where few westerners had ventured, and in addition to conventionally pleasing subjects sought sensational and macabre subject matter such as heads on display after decapitation.<ref>Handy, p. 57.</ref> His images are remarkable not only for their quality, but also for their rarity as photographic views of [[Edo period]] Japan.<ref name="Bennett 2006, 97">Bennett, ''Photography in Japan, 1853–1912,'' p. 97.</ref>

The greater part of Beato's work in Japan contrasted strongly with his earlier work in India and China, which "had underlined and even celebrated conflict and the triumph of British imperial might".<ref>Dobson, "'I been to keep up my position'", p. 32.</ref> Aside from the ''Portrait of Prince Kung'', any appearances of Chinese people in Beato's earlier work had been peripheral (minor, blurred, or both) or as corpses. With the exception of his work in September 1864 as an official photographer on the British military expedition to [[Bombardment of Shimonoseki|Shimonoseki]],<ref name="Bennett 2006, 94"/> Beato was eager to portray Japanese people, and did so uncondescendingly, even showing them as defiant in the face of the [[Extraterritoriality|elevated status]] of westerners.<ref>Dobson, "'I been to keep up my position'", ''passim.''</ref>

Beato was very active while in Japan. In 1865 he produced a number of dated views of [[Nagasaki, Nagasaki|Nagasaki]] and its surroundings.<ref name="Bennett 2006, 94"/> From 1866 he was often caricatured in ''Japan Punch,'' which was founded and edited by Wirgman.<ref name="Bennett 2006, 95">Bennett, ''Photography in Japan, 1853–1912,'' p. 95.</ref> In an October 1866 fire that destroyed much of Yokohama, Beato lost his studio and many, perhaps all, of his negatives.<ref name="Bennett 2006, 94"/>

While Beato was the first photographer in Japan to sell albums of his works, he quickly recognised their full commercial potential.<ref>Hockley, p. 67.</ref> By around 1870 their sale had become the mainstay of his business.<ref>Hockley, p. 68.</ref> Although the customer would select the content of earlier albums, Beato moved toward albums of his own selection. It was probably Beato who introduced to photography in Japan the double concept of views and costumes/manners, an approach common in photography of the Mediterranean.<ref>Hockley, p. 72.</ref> By 1868 Beato had readied two volumes of photographs, "Native Types", containing 100 portraits and genre works, and "Views of Japan", containing 98 landscapes and cityscapes.<ref>{{cite book|title=F.ベアト写真集1（Photo Album, in Japanese but with some English subtitles, from the Yokohama Archives of History）|date=July 6, 2006|isbn=978-4-7503-2369-5}}</ref>

Many of the photographs in Beato's albums were [[hand-colouring|hand-coloured]], a technique that in his studio successfully applied the refined skills of [[Japanese art|Japanese]] [[watercolor painting|watercolourists]] and [[woodblock printing|woodblock printmakers]] to European photography.<ref name="Bennett 2006, 97"/>

Since about the time of the ending of his partnership with Wirgman in 1869, Beato attempted to retire from the work of a photographer, instead attempting other ventures<ref name="bennettpj86">Bennett, ''Photography in Japan,'' p. 86.</ref><ref name="Bennett 2006, 97"/> and delegating photographic work to others within his own studio in Yokohama, "F. Beato & Co., Photographers",<ref>Clark, Fraser, and Osman, p. 100.</ref> which he ran with an assistant named H. Woollett and four Japanese photographers and four Japanese artists.<ref name="cfo103">Clark, Fraser, and Osman, p. 103.</ref> [[Kusakabe Kimbei]] was probably one of Beato's artist-assistants before becoming a photographer in his own right.<ref>''Bakumatsu Nihon no fūkei to hitobito: Ferikkusu Beato shashinshū,'' p. 186.</ref> But these other ventures would fail, and Beato's photographic skills and personal popularity would ensure that he could successfully return to work as a photographer.<ref name="bennettpj86" />

Beato photographed with [[Ueno Hikoma]],<ref>Himeno, p. 24.</ref> and possibly taught photography to [[Baron Raimund von Stillfried|Raimund von Stillfried]].<ref>Gartlan, ''A Chronology of Baron Raimund von Stillfried-Ratenicz (1839–1911)'', p. 130.</ref>

[[File:SaigoTsugumichiAndBeato1882.jpg|thumb|Felice Beato with [[Saigo Tsugumichi]] (both seated in front), with foreign friends. Photograph by [[Hugues Krafft]] in 1882.]]

In 1871 Beato served as official photographer with the [[United States expedition to Korea|United States naval expedition]] of [[Christopher Raymond Perry Rodgers|Admiral Rodgers]] to Korea.<ref name="Bennett 2006, 95"/> Although it is possible that an unidentified Frenchman photographed Korea during the [[French campaign against Korea (1866)|1866 invasion]] of [[Ganghwa Island]],<ref>Choi and Park; Bennett, "Korea", p. 805.</ref> Beato's photographs are the earliest of Korea whose provenance is clear.<ref>Bennett, "Korea", p. 805.</ref>

Beato's business ventures in Japan were numerous. He owned land<ref>Clark, Fraser, and Osman, pp. 99, 100, 108.</ref> and several studios, was a property consultant, had a financial interest in the Grand Hotel of Yokohama,<ref name="Bennett 2006, 95"/> and was a dealer in imported carpets and women's bags, among other things. He also appeared in court on several occasions, variously as plaintiff, defendant, and witness.<ref>Clark, Fraser, and Osman, pp. 104–109, ''passim.''</ref> On 6 August 1873 Beato was appointed [[Consulate general|Consul General]] for Greece in Japan.<ref name="cfo103" />

In 1877 Beato sold most of his stock to the firm [[Stillfried & Andersen]],<ref name="Bennett 2006, 97"/> who then moved into his studio. In turn, Stillfried & Andersen sold the stock to [[Adolfo Farsari]] in 1885.<ref>Clark, Fraser, and Osman, p. 107.</ref> Following the sale to Stillfried & Andersen, Beato apparently retired for some years from photography, concentrating on his parallel career as a financial speculator and trader.<ref>Clark, Fraser, and Osman, pp. 107–108.</ref> On 29 November 1884 he left Japan, ultimately landing in [[Port Said]], Egypt.<ref>Clark, Fraser, and Osman, pp. 111–112.</ref> It was reported in a Japanese newspaper that he had lost all his money on the Yokohama silver exchange.<ref name="cfo112">Clark, Fraser, and Osman, p. 112.</ref>

==Burma and Later years==
[[File:Hman Kyaung Mandalay.jpg|thumb|left|upright|Queen's Silver Pagoda, Mandalay, c. 1889]]

From 1884 to 1885 Beato was the official photographer of the expeditionary forces led by Baron (later Viscount) [[Garnet Wolseley, 1st Viscount Wolseley|G.J. Wolseley]] to [[Khartoum]], Sudan, in relief of [[Charles George Gordon|General Charles Gordon]].<ref name="cfo112" />

Briefly back in England in 1886, Beato lectured the London and Provincial Photographic Society on photographic techniques.<ref name="cfo112" />

Felice Beato arrived in Burma probably in December 1886, after Upper Burma had been annexed by the British in late 1885. Much publicity had been made in the British press about the three [[Anglo-Burmese Wars]], which had started in 1825 and culminated in December 1885 with the fall of Mandalay and the capture of King [[Thibaw Min]].

Beato, who had covered military operations in India and China, was probably attracted by the news of the annexation. While he arrived in Burma after the main military operations ended, he would still get to see more of the action, as the annexation by the British led to an insurgency which lasted for the following decade. This allowed Beato to take a number of pictures of British military in operations or at the Royal Palace, Mandalay, as well as insurgency soldiers and prisoners.

Felice Beato set up a photographic studio in Mandalay and, in 1894, a curiosa and antiques dealership, running both businesses separately and, according to records at the time, very successfully.

His past experience and the credibility derived from his time in Japan brought him a large clientele of opulent locals, posing in traditional attire for official portraits. Other images, from Buddhas to landscapes and buildings, were sold from master albums in Burma and Europe.

In 1896, Trench Gascoigne published some of Beato’s images in ''Among Pagodas and Fair Ladies'' and, the following year, Mrs Ernest Hart’s ''Picturesque Burma'' included more, while George W. Bird in his ''Wanderings in Burma'' not only presented thirty-five credited photographs but published a long description of Beato’s businesses and recommended visitors to come by his shop.

By that time, Beato’s photographs had come to represent the very image of Burma to the rest of the world, which it would remain for decades to come.

As his curios business developed, with branches in Rangoon, Mandalay but also in Colombo and London, he also acquired the Photographic Art Gallery in Mandalay in 1903, another photographic studio. In his old age, Felice Beato had become an important business party in Colonial Burma, involved in many enterprises from electric works to life insurance and mining.

Scott O’Connor used a number of these images in his two books, ''The Silken East'' in 1904 and ''Mandalay and Other Cities of the Past in Burma'' in 1907, popularizing Beato’s work to an even larger extent.

==Death and legacy==

Although Beato was previously believed to have died in Rangoon or Mandalay in 1905 or 1906,<ref>Robinson, p. 41; Clark, Fraser, and Osman, p. 116; Zannier, "Beato", p. 446.</ref> his death certificate, discovered in 2009, indicates that he died on 29 January 1909 in [[Florence]], Italy.

Whether acknowledged as his own work, sold as Stillfried & Andersen's, or encountered as anonymous engravings, Beato's work had a major impact:

<blockquote>For over fifty years into the early twentieth century, Beato's photographs of Asia constituted the standard imagery of travel diaries, illustrated newspapers, and other published accounts, and thus helped shape "Western" notions of several Asian societies.<ref>Gartlan, "Felix Beato", p. 131.</ref></blockquote>

==Photographic techniques==
Photographs of the 19th century often now show the limitations of the technology used, yet Felice Beato managed to successfully work within and even transcend those limitations. He predominantly produced [[albumen print|albumen silver prints]] from [[collodion process|wet collodion]] glass-plate [[negative (photography)|negatives]].<ref>Lacoste, pp. 24–25.</ref>

Beato pioneered and refined the techniques of hand-colouring photographs<ref name="Gartlan, Felix Beato, 129"/> and making panoramas.<ref>Gartlan, "Felix Beato", p. 130.</ref> He may have started hand-colouring photographs at the suggestion of Wirgman, or he may have seen the hand-coloured photographs made by partners Charles Parker and William Parke Andrew.<ref>Bennett, ''Early Japanese Images,'' p. 39.</ref> Whatever the inspiration, Beato's coloured landscapes are delicate and naturalistic and his coloured portraits, more strongly coloured than the landscapes, are appraised as excellent.{{refn|Bennett quotes and summarises collector Henry Rosin's appraisal of Beato's hand-coloured photographs.<ref>Bennett, ''Early Japanese Images,'' p. 43; Robinson, p. 48.</ref>|group="note"}} As well as providing views in colour, Beato worked to represent very large subjects in a way that gave a sense of their vastness. Throughout his career, Beato's work is marked by spectacular panoramas, which he produced by carefully making several contiguous exposures of a scene and then joining the resulting prints together, thereby re-creating the expansive view.<ref name="Lacoste 8-9">Lacoste, pp. 8–9.</ref> The complete version of his panorama of Pehtang comprises seven photographs joined together almost seamlessly for a total length of more than 2 metres (6 1/2&nbsp;ft).<ref name="Lacoste 8-9"/>

{{wide image|Panorama of Edo.jpg|800px|''Panorama of Yedo from Atagoyama'': a panorama (comprising five albumen prints) of Edo (now Tokyo), showing [[daimyo]] residences, 1865 or 1866}}
{{clear}}

==See also==
* [[John McCosh]]
* ''[[Felice...Felice...]]'', a 1998 Dutch drama loosely based on Beato's time in Japan
* [[Sakoku]]
* [[List of Westerners who visited Japan before 1868]]

==Footnotes==
{{Reflist|group="note"}}

==Citations==
{{Reflist|30em}}

==References==
{{refbegin|colwidth=30em}}
*''Bakumatsu Nihon no fūkei to hitobito: Ferikkusu Beato shashinshū'' ({{nihongo2|幕末日本の風景と人びと　フェリックス・ベアト写真集}}). Yokohama: Akashi Shoten, 1987.&nbsp;{{Ja icon}} OCLC 18743438.
*Baldwin, Gordon, Malcolm Daniel, and Sarah Greenough. ''All the Mighty World: The Photographs of Roger Fenton, 1852–1860.'' New Haven: [[Yale University Press]], 2004. ISBN 1-58839-128-0; ISBN 1-58839-129-9.
*Banta, Melissa, and Susan Taylor, eds. ''A Timely Encounter: Nineteenth-Century Photographs of Japan.'' Cambridge, Mass.: Peabody Museum Press, 1988. ISBN 0-87365-810-8.
*Bennett, Terry. ''Early Japanese Images.'' Rutland, Vermont: Charles E. Tuttle, 1996. ISBN 0-8048-2029-5; ISBN 0-8048-2033-3.
*Bennett, Terry. "[http://www.old-japan.co.uk/article_korea.html Felice Beato and the United States Expedition to Korea of 1871]". Old Japan. Accessed 3 April 2006.
*Bennett, Terry. ''History of Photography in China, 1842–1860.'' London: Bernard Quaritch, 2009. ISBN 0-9563012-0-7.
*Bennett, Terry. ''Korea''. In ''Encyclopedia of Nineteenth-Century Photography,'' 2: pp.&nbsp;804–806. New York: Routledge, 2008. ISBN 978-0-415-97235-2.
*Bennett, Terry. ''Photography in Japan, 1853–1912.'' North Clarendon, Vermont: Tuttle, 2006. ISBN 0-8048-3633-7.
*Bird, George W. ''Wanderings in Burma'', Simpkin, Marshall, Hamilton, Kent & Co., London 1897.
*Broecker, William L., ed. ''International Center of Photography Encyclopedia of Photography.'' New York: Pound Press; Crown, 1984. ISBN 0-517-55271-X.
*Choi Injin and Park Juseok. ''The Century of Korean Photography: Images from the Land of the Morning Calm.'' Seoul: RIHP, 2001. OCLC 71135623.
*Clark, John. A chronology of Charles Wirgman (1832?–1891). In Clark, ''Japanese Exchanges in Art, 1850s to 1930s with Britain, Continental Europe, and the USA,'' pp.&nbsp;25–58. Sydney: Power Publications, 2001. ISBN 978-1-86487-303-0
*Clark, John. ''Japanese Exchanges in Art, 1850s to 1930s with Britain, Continental Europe, and the USA: Papers and Research Materials.'' Sydney: Power Publications, 2001. ISBN 1-86487-303-5.
*Clark, John, John Fraser, and Colin Osman. ''A revised chronology of Felice (Felix) Beato (1825/34?–1908?)''. In Clark, ''Japanese Exchanges in Art, 1850s to 1930s with Britain, Continental Europe, and the USA.'' Sydney: Power Publications, 2001. ISBN 978-1-86487-303-0
*Dehejia, Vidya, et al. ''India through the Lens: Photography 1840–1911.'' Washington, D.C.: Freer Gallery of Art and Arthur M. Sackler Gallery; Ahmedabad: Mapin; Munich, Prestel, 2000. ISBN 81-85822-73-5; ISBN 3-7913-2408-X.
*Dobson, Sebastian. "'I been to keep up my position': Felice Beato in Japan, 1863–1877". In Rousmaniere and Hirayama, eds, ''Reflecting Truth: Japanese Photography in the Nineteenth Century,'' pp.&nbsp;30–39. Amsterdam : Hotei, 2004. ISBN 978-90-74822-76-3.
*Dobson, Sebastian. "Japan." In ''Encyclopedia of Nineteenth-Century Photography,'' 2: pp.&nbsp;769–773.
*''Encyclopedia of Nineteenth-Century Photography.'' 2 vols. New York: Routledge, 2008. ISBN 0-415-97235-3.
*"[http://www.luminous-lint.com/app/photographer/Felice__Beato/A/ Felice Beato]." Luminous Lint. Accessed 19 August 2011.
*Gartlan, Luke. "A chronology of Baron Raimund von Stillfried-Ratenicz (1839–1911)." In Clark, ''Japanese Exchanges in Art, 1850s to 1930s with Britain, Continental Europe, and the USA,'' pp.&nbsp;121–178.  Sydney: Power Publications, 2001. ISBN 978-1-86487-303-0
*Gartlan, Luke. ''Felix Beato''. In ''Encyclopedia of Nineteenth-Century Photography,'' 2: pp.&nbsp;128–131.
*Gernsheim, Helmut. ''The Rise of Photography: 1850–1880: The Age of Collodion.'' London: Thames and Hudson, 1988. ISBN 0-500-97349-0.
*Gray, Ezio. ''Le terre nostre ritornano... Malta, Corsica, Nizza''. Novara: De Agostini Editoriale, 1943.&nbsp;{{It icon}} OCLC 22249643.
*Greenough, Sarah. '''A new starting point': Roger Fenton's life''. In Baldwin, Daniel, and Greenough, ''All the Mighty World,'' pp.&nbsp;2–31. New York: Metropolitan Museum of Art; New Haven: Yale University Press, 2004. ISBN 978-1-58839-128-5.
*Griffiths, Alan. "[http://www.luminous-lint.com/_switchbox.php?action=ACT_SING_TH&amp;p1=233 Second Chinese Opium War (1856–1860)]". Luminous-Lint. Accessed 3 April 2006.
*Handy, Ellen. ''Tradition, novelty and invention: Portrait and landscape photography in Japan, 1860s–1880s''. In Banta and Taylor, eds, ''A Timely Encounter : Nineteenth Century Photographs of Japan,'' pp.&nbsp;53–71. Cambridge, Mass.: Peabody Museum, 1988. ISBN 978-0-87365-810-2.
*Harris, David. ''Of Battle and Beauty: Felice Beato's Photographs of China.'' Santa Barbara: Santa Barbara Museum of Art, 1999. ISBN 0-89951-101-5; ISBN 0-89951-100-7.
*Hart, Mrs Ernest. ''Picturesque Burma'', J.M. Dent & Co., London 1897.
*Himeno, Junichi. ''Encounters with foreign photographers: The introduction and spread of photography in Kyūshū''. In Rousmaniere and Hirayama, eds, ''Reflecting Truth: Japanese Photography in the Nineteenth Century,'' pp.&nbsp;18–29.  Amsterdam : Hotei, 2004. ISBN 978-90-74822-76-3.
*Hockley, Allen. ''Photo albums and their implications for the study of early Japanese photography''. In Rousmaniere and Hirayama, eds, ''Reflecting Truth: Japanese Photography in the Nineteenth Century,'' pp.&nbsp;66–85.  Amsterdam : Hotei, 2004. ISBN 978-90-74822-76-3.
*Kanai Madoka ({{nihongo2|金井圓}}), ed. ''Egakareta Bakumatsu Meiji: Irasutoreiteddo Rondon Nyūsu Nihon tsūshin 1853–1902'' ({{nihongo2|描かれた幕末明治　イラストレイテッド・ロンドン・ニュース日本通信1853–1902}}). Tokyo: Yushodo, 1973.&nbsp;{{Ja icon}}<!-- no ISBN -->
*Lacoste, Anne. ''Felice Beato: A Photographer on the Eastern Road. Los Angeles: J. Paul Getty Museum, 2010.'' ISBN 1-60606-035-X.
*Lowry, John. ''Victorian Burma by Post: Felice Beato's Mail-Order Business'', Country Life, March 13, 1975, pp. 659-660.
*Masselos, Jim, and Narayani Gupta. ''Beato's Delhi 1857, 1957.'' Delhi: Ravi Dayal, 2000. ISBN 81-7530-028-0.
*Pare, Richard. ''Photography and Architecture: 1839–1939.'' Montréal: Centre Canadien d'Architecture/Canadian Centre for Architecture; New York: Callaway, 1982. ISBN 0-935112-06-5; ISBN 0-935112-07-3.
*O'Connor, Scott & Clarence, Vincent. ''The Silken East'', Volume I & II. Hutchinson & Co. Paternoster Row, London 1904.
*O'Connor, Scott & Clarence, Vincent. ''Mandalay and Other Cities of the Past in Burma'', Hutchinson & Co. Paternoster Row, London 1907.
*Pare, Richard. ''Roger Fenton: The artist's eye''. In Baldwin, Daniel, and Greenough, ''All the Mighty World: The Photographs of Roger Fenton, 1852–1860,'' pp.&nbsp;221–230. New Haven: Yale University Press, 2004. ISBN 1-58839-128-0; ISBN 1-58839-129-9.
*Peres, Michael R. ''Focal Encyclopedia of Photography: Digital Imaging, Theory and Applications.'' 4th ed. Amsterdam: Focal Press, 2007. ISBN 0-240-80740-5. ''S.v.'' "Copying".
* Rasch, Carsten. ''Photographien aus dem alten Japan - Felice Beato und seine Photographien aus der Edo-Ära''. Hamburg, 2014. ISBN 978-3-738-60700-0.
*Rennie, David Field. ''The British Arms in Northern China and Japan: Pekin 1860; Kagoshima 1862.'' London: John Murray, 1864. Available [https://books.google.com/books?id=CF44SqfehdIC here] at Google Books.
*Robinson, Bonnell D. ''Transition and the Quest for Permanence: Photographer and Photographic Technology in Japan, 1854–1880s''. In Banta and Taylor, eds, ''A Timely Encounter: Nineteenth-Century Photographs of Japan,''  pp.&nbsp;39–52. Cambridge, Mass.: Peabody Museum Press, 1988. ISBN 0-87365-810-8.
*[[Naomi Rosenblum|Rosenblum, Naomi]]. ''A World History of Photography.'' 3rd ed. New York: Abbeville Press, 1997. ISBN 0-7892-0028-7; ISBN 0-7892-0329-4.
*Rousmaniere, Nicole Coolidge, and Mikiko Hirayama, eds. ''Reflecting Truth: Japanese Photography in the Nineteenth Century.'' Amsterdam: Hotei, 2004. ISBN 90-74822-76-2.
*''Thacker's Indian Directory''. Thacker, Spink & Co. 3 Esplanade East, Calcutta, India. Issues from 1863 to 1915.
*Trench Gascoigne, Gwendolen. ''Among Pagodas and Fair Ladies'', A. D. Innes & Co. Bedford Street, London 1896.
*Vaczek, Louis, and Gail Buckland. ''Travelers in Ancient Lands: A Portrait of the Middle East, 1839–1919.'' Boston: NYGS, 1981. ISBN 0-8212-1130-7.
*Zannier, Italo. "Beato." In ''The Dictionary of Art'' (34 vols; New York: Grove, 1996; ISBN 1-884446-00-0), 3:446–447.
*Zannier, Italo. ''Antonio e Felice Beato.'' Venice: Ikona Photo Gallery, 1983.&nbsp;{{It icon}} OCLC 27711779.
{{refend}}

==External links==
{{Commons category}}
*[http://www.getty.edu/art/gettyguide/artMakerDetails?maker=1967 Biography of Felice Beato], with links to 40 photographs. J.&nbsp;Paul Getty Museum.
*[http://collections.si.edu/search/results.htm?tag.cstype=all&fq=name%3A%22Beato%2C+Felice%22&q=beato%2C+felice&fq=data_source:%22Freer+Gallery+of+Art+and+Arthur+M.+Sackler+Gallery+Archives%22 Gallery of Felice Beato photographs] (53 images including landscapes and portraits). The Freer Gallery of Art and Arthur M. Sackler Gallery Archives.
*"[http://dl.lib.brown.edu/libweb/collections/askb/beato.php Photographic views of Lucknow taken after the Indian Mutiny]". Brown University Library; Anne S. K. Brown Military Collection.
*[http://oldphoto.lb.nagasaki-u.ac.jp/en/list.php?req=1&target=Beato Photographs by Beato], "Japanese Old Photographs in Bakumatsu-Meiji Period". Nagasaki University Library.
*[http://svrdam.cca.qc.ca/search/bs.aspx?langID=1#s=Felice%20Beato&amp;p=1&amp;a=kw&amp;nr=1&amp;nq=1 Collections Online, s.v. "Beato, Felice"]. Canadian Centre for Architecture.
* {{webarchive |url=https://web.archive.org/web/20070509014718/http://www.art-antiques.ch/exhibitions/2003/photograph/artist/Beato/index.html |date=9 May 2007 |title="Photographers: Felice Beato, 1825–1908" }}. Asia through the Lens. Bachmann Eckenstein Art & Antiques, 2006. Archived by the [[Wayback Machine]] on 9 May 2007.
*[http://digitalgallery.nypl.org/nypldigital/dgkeysearchresult.cfm?word=Beato%2C%20Felice&s=3&notword=&f=4&cols=4 Catalogue search for "Beato, Felice"]. New York Public Library.
*[http://images.wellcome.ac.uk/indexplus/result.html?create_creator_name_name%3Atext=%22Felice+A.+Beato%22&%24+not+%22Contemporary+clinical+images%22+index+wi_collection=.&%24%3Dsort=sort+sortexpr+image_sort&*sform=wellcome-images&_IXACTION_=query&_IXFIRST_=1&_IXSPFX_=templates%2Fb&_IXFPFX_=templates%2Ft&%24+with+image_sort=.&s=OXn5qzRDO6l Gallery of Felice Beato photographs from Wellcome Images]. (74 images from China and India). Wellcome Library, London.
*[https://www.fostinum.org/felice-beato.html Fostinum: Felice Beato]

{{Featured article}}

<!--{{Authority control}}-->

{{Authority control}}

{{DEFAULTSORT:Beato, Felice}}
[[Category:1832 births]]
[[Category:1909 deaths]]
[[Category:Architectural photographers]]
[[Category:British expatriates in China]]
[[Category:British expatriates in Japan]]
[[Category:British people of Italian descent]]
[[Category:British people of the Second Opium War]]
[[Category:British photojournalists]]
[[Category:Italian photojournalists]]
[[Category:People from Corfu]]
[[Category:Photography in Myanmar]]
[[Category:Photography in China]]
[[Category:Photography in Greece]]
[[Category:Photography in India]]
[[Category:Photography in Japan]]
[[Category:Photography in Ukraine]]
[[Category:Pioneers of photography]]
[[Category:Portrait photographers]]
[[Category:War photographers]]