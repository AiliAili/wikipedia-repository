{{good article}}
{{Use American English|date=December 2013}}
{{Use mdy dates|date=December 2013}}
{{Infobox School
|name           = Brooklyn Free School
|image          = File:Brooklyn Free School – first day of their eleventh year.jpg
|caption        = First day of school, 2014
|motto          = “Where children are free to be themselves”
|established    = 2003
|type           = Private
|schooltype     = [[ungraded school|ungraded]] elementary and secondary [[democratic free school]]
| coordinates = {{Coord|40|41|13.5|N|73|58|4.9|W|format=dms|type:edu_region:US-CA|display=inline,title}}
| affiliation = Nonsectarian
| grades         = [[Ungraded school|Ungraded]] (ages 4 to 18)
|us_nces_school_id = {{NCES Private School ID|A0701595|school_name=Brooklyn Free School|access_date=September 3, 2015}}
|principal      = Noleca Radway
|principal_label = Director
|founder = Alan Berger
|faculty        = 8 (2014)
|enrollment     = 80
|enrollment_as_of = 2015
|colors         =
|school colors  =
|mascot         =
|campus type    = Urban
|address        = 372 Clinton Avenue
|city           = [[Brooklyn]]
|state          = [[New York (state)|New York]]
|zipcode        = 11238
|country        = United States of America
|website        = {{URL|brooklynfreeschool.org}}
}}

The '''Brooklyn Free School''' is a private, [[ungraded school|ungraded]], [[democratic free school]] in [[Fort Greene, Brooklyn]], founded in 2004. Students range in age from 4 to 18 years old. The school follows the noncoercive philosophy of the 1960s/70s [[free school movement]] schools, which encourages self-directed learning and protects child freedom of activity. There are no grades, no tests, and classes are non-compulsory. As of 2015, the school enrolls 80 students and has about 24 graduates.

The school was the first free school in New York City since 1975. It started in a rented portion of a [[Park Slope]] Methodist church, but moved to a [[brownstone]] in Fort Greene. Students participate in the design of classes and in the school's governance, which is done at a weekly Democratic Meeting. Staff and students all have equal votes. The school is funded through sliding-scale tuition, grants, and donations. In 2012, Lucas Kavner of ''[[The Huffington Post]]'' called the Brooklyn Free School "arguably New York's most radical center of learning".<ref name="HuffPo 2012"/>

== History ==

The Brooklyn Free School was founded in 2004 in [[Park Slope, Brooklyn]],<ref name="HuffPo 2012"/> and began its first [[academic year|academic session]] later that year.<ref name="CBS 2006"/> Its director, Alan Berger, had been an assistant principal at [[Murry Bergtraum High School|a Manhattan high school]] before he left to found the alternative school. He had read about a free school in [[Woodstock, New York]], and was "grabbed ... to the core".<ref name="NYT 2006"/> Berger published his idea for the school in the October 2003 issue of the park Slope Food Co-op newsletter.<ref name="NYT 2006"/> About 170 people showed interest, and a group held biweekly planning sessions<ref name="NYT 2004"/> until the school opened in the 16th Street Brooklyn First Free Methodist Church's bottom two floors in 2004.<ref name="NYT 2006"/> The original class was thirty students with three teachers.<ref name="NYT 2006"/> It was the first free school in New York City since the Park Slope<ref name="NYT 2006"/> Fifteenth Street School closed in 1975.<ref name="HuffPo 2012"/> By November 2012, the school had moved to a four-floor [[brownstone]] in [[Fort Greene]].<ref name="HuffPo 2012"/> The school had 42 pupils by November 2006,<ref name="CBS 2006"/> 60 by 2012,<ref name="HuffPo 2012"/> and 80 by 2015.<ref name="VOA 2015"/> As of 2015, Lily Mercogliano is the school's director.<ref name="VOA 2015"/>

== Program ==

{{Quote box|quote=We're trying to nurture kids to stay themselves ... That's what they need to bring to the world, to live a successful, individually happy life. |source=Alan Berger, Brooklyn Free School founder and principal, 2012<ref name="HuffPo 2012"/> |width=30em |quoted=1}}The school operates under a "noncoercive" philosophy where students are encouraged to develop their own interests and where all learning is self-directed.<ref name="HuffPo 2012"/> As such, Brooklyn Free School has no grades, no tests, and no compulsory classes or homework. Students are free to pursue the activities of their interest, such as reading alone or taking a class.<ref name="HuffPo 2012"/> Students are free to leave classes as they please.<ref name="HuffPo 2012"/> Classes have included philosophy seminars, cheese-tasting, book discussions, business, astrology, psychology, videography, and Tibet.<ref name="NYT 2006"/> Some classes are taught by volunteers.<ref name="HuffPo 2012"/> By law, students are required to attend for 5.5 hours a day.<ref name="CBS 2006"/> Principal Alan Berger contends that the school provides an education better adapted for the Internet era, as one more original, enterprising, and adaptive in the face of a changing economy.<ref name="NYT 2006"/>

The Brooklyn Free School holds a weekly, mandatory Democratic Meeting on Wednesday mornings.<ref name="NYT 2006"/> The meeting runs the school, and students and teachers alike have equal votes. Students are not required to pay attention. Meeting topics range from disciplinary grievances to admissions<ref name="NYT 2006"/> to computer use. A meeting chair is chosen at the beginning of the meeting and the floor is opened for propositions. Anyone wishing to discuss a school issue can call schoolwide meetings.<ref name="HuffPo 2012"/>

As of 2015, the school enrolls about 80 students, about half of whom are African-American or Latino.<ref name="VOA 2015"/><!-- The school had 60 students in 2012, about half of whom were white.<ref name="HuffPo 2012"/>--> The school is divided into upper and lower schools, the former ages 11 to 18 and the latter ages 4 to 11,<ref name="HuffPo 2012"/> though they are not physically separated by age.<ref name="NYT 2004"/> Children apply for admission and visit for a five-day orientation. Students are admitted by unanimous vote of a teacher-parent-student admissions committee. The group first determines whether applicants' parents support their decision to attend and whether the school can provide for the students' needs.<ref name="NYT 2006"/> The school keeps a waiting list.<ref name="HuffPo 2012"/> 

The school is funded through tuition, grants, and donations.<ref name="NYT 2006"/> The majority of students come from middle-class families from Brooklyn. The private school has sliding-scale tuition, and less than half pay full tuition.<ref name="NYT 2006"/> Founding director Alan Berger said that 20 percent paid full tuition in 2012.<ref name="HuffPo 2012"/> In 2015, about a third paid less than $500 in tuition, and another third paid half tuition. The sliding scale's full tuition is set at $22,000.<ref name="VOA 2015"/>

The school graduated 21 students as of 2012,<ref name="HuffPo 2012"/> and 24 as of 2015.<ref name="VOA 2015"/> Students compile their own transcript and nominate themselves for graduation. Some take [[standardized tests|standardized]] state and college entrance tests. The majority of Brooklyn Free School graduates continue to college.<ref name="VOA 2015"/>

== Reception ==

Lucas Kavner of ''[[The Huffington Post]]'' wrote in 2012 that the school serves as a model for independent, [[democratic schools]] at the forefront of renewed interest in the 1960s/70s [[free school movement]]. He added that critics contend that the school's environment does not prepare students for [[real life]], and that students from families that cannot hire tutors will suffer disproportionately. The school inspired the Manhattan Free School (founded in 2008), and, in turn, was inspired by the Albany Free School (founded in 1969). Kavner called the Brooklyn Free School "arguably New York's most radical center of learning".<ref name="HuffPo 2012"/>

An article in ''[[The New York Times]]'' in 2006 wrote that parents hired outside tutors in concern for the school's academic preparation. A third of the original students left within the 2004 academic year, as did the original teachers.<ref name="NYT 2006"/>

== References ==

{{reflist|30em|refs=

<ref name="CBS 2006">{{cite web|url=http://www.cbsnews.com/news/no-grades-no-tests-at-free-school/ |accessdate=December 27, 2013 |title=No Grades, No Tests At 'Free School' |last1=Conroy |first1=Scott |date=November 19, 2006 |work=[[CBS News]] |publisher=[[CBS]] |agency=[[Associated Press]] |archiveurl=http://www.webcitation.org/6MAhDNnyz?url=http%3A%2F%2Fwww.cbsnews.com%2Fnews%2Fno-grades-no-tests-at-free-school%2F |archivedate=December 27, 2013 |deadurl=no |df=mdy }}</ref>

<ref name="HuffPo 2012">{{cite web|url=http://www.huffingtonpost.com/2012/11/30/brooklyn-free-school-_n_2214263.html |accessdate=December 7, 2013 |title=At Brooklyn Free School, A Movement Reborn With Liberty And No Testing For All |last1=Kavner |first1=Lucas |date=November 30, 2012 |work=[[The Huffington Post]] |publisher=[[AOL]] |archiveurl=http://www.webcitation.org/6Lh7tdRa3?url=http%3A%2F%2Fwww.huffingtonpost.com%2F2012%2F11%2F30%2Fbrooklyn-free-school-_n_2214263.html |archivedate=December 7, 2013 |deadurl=no |df=mdy }}</ref>

<ref name="NYT 2004">{{cite web |url=https://www.nytimes.com/2004/02/15/nyregion/neighborhood-report-park-slope-one-man-s-solution-to-the-educational-rat-race.html?pagewanted=print&src=pm |accessdate=December 26, 2013 |title=One Man's Solution To the Educational Rat Race |last1=Bahrampour |first1=Tara |date=February 15, 2004 |work=[[The New York Times]] |archiveurl=http://www.webcitation.org/6MASbpYto?url=https://www.nytimes.com/2004/02/15/nyregion/neighborhood-report-park-slope-one-man-s-solution-to-the-educational-rat-race.html?pagewanted%3Dprint%26src%3Dpm |archivedate=December 27, 2013 |deadurl=no }}</ref>

<ref name="NYT 2006">{{cite web |url=https://www.nytimes.com/2006/05/07/nyregion/thecity/07free.html?pagewanted=all |accessdate=December 7, 2013 |title=Land of the Free |last1=Gell |first1=Aaron |date=May 7, 2006 |work=[[The New York Times]] |archiveurl=http://www.webcitation.org/6Lh716URi?url=https://www.nytimes.com/2006/05/07/nyregion/thecity/07free.html?pagewanted%3Dall%26_r%3D0 |archivedate=December 7, 2013 |deadurl=no }}</ref>

<ref name="VOA 2015">{{cite web|url=http://m.voanews.com/a/brooklyn-free-school-students-control-education/2723321.html |accessdate=September 3, 2015 |title=At Brooklyn Free School, Students Control Their Education |author= |date=April 16, 2015 |publisher=[[Voice of America]] |archiveurl=http://www.webcitation.org/6bGhjYhuG?url=http%3A%2F%2Fm.voanews.com%2Fa%2Fbrooklyn-free-school-students-control-education%2F2723321.html |archivedate=September 3, 2015 |deadurl=no |df=mdy }}</ref>

}}

== External links ==
{{external media|video1=[https://www.youtube.com/watch?v=tjsR5HFVaus Video of the school's operations] by [[Voice of America]]}}
* {{official website}}

{{Democratic free schools}}
{{Portal bar|Education|New York City|Schools}}

[[Category:2004 establishments in New York]]
[[Category:Alternative schools in the United States]]
[[Category:Democratic free schools]]
[[Category:Educational institutions established in 2004]]
[[Category:Private schools in New York City]]