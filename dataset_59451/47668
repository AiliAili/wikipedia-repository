The '''Edwards equation''' in organic chemistry is a two-parameter equation for correlating [[nucleophile|nucleophilic reactivity]], as defined by relative rate constants, with the [[Base (chemistry)|basicity]] of the nucleophile (relative to protons) and its [[polarizability]]. This equation was first developed by John O. Edwards in 1954<ref name=ed1>{{cite journal | last = Edwards | first = J.O. | title = Correlation of Relative Rates and Equilibria with a Double Basicity Scale | journal = Journal of the American Chemical Society | volume = 76 | issue = 6 | pages = 1540–1547 | year = 1954 | doi=10.1021/ja01635a021 }}</ref> and later revised based on additional work in 1956.<ref name=ed2>{{cite journal | last = Edwards | first = J.O. | title = Polarizability, Basicity and Nucleophilic Character | journal = Journal of the American Chemical Society | volume = 78 | issue = 9 | pages = 1819–1820 | year = 1956 | doi=10.1021/ja01590a012 }}</ref>
The general idea is that most nucleophiles are also good bases because the concentration of negatively charged electron density that defines a nucleophile will strongly attract positively charged protons, which is the definition of a base according to [[Brønsted-Lowry acid-base theory]]. Additionally, highly polarizable nucleophiles will have greater nucleophilic character than suggested by their basicity because their electron density can be shifted with relative ease to concentrate in one area.

== History ==

Prior to Edwards developing his equation, other scientists were also working to define [[nucleophilicity]] quantitatively. Brønsted and Pederson first discovered the relationship between basicity, with respect to protons, and nucleophilicity in 1924:<ref name=Harris>{{cite book | last1 = Harris | first1 = J. Milton | last2 = McManus | first2 = Samuel P. | title = Nucleophilicity | publisher = American Chemical Society | volume = 215 | date = 1987 | location = | pages = 7–8 | doi = 10.1021/ba-1987-0215 | isbn = 9780841209527 }}</ref>
:where <math>\log k _B = \beta_n  pK_{b} + C </math>

where k<sub>b</sub> is the rate constant for nitramide decomposition by a base (B) and β<sub>N</sub> is a parameter of the equation.

Swain and Scott later tried to define a more specific and quantitative relationship by correlating nucleophilic data with a single-parameter equation<ref>{{cite journal|last1=Swain|first1=C. Gardner|last2=Scott|first2=Carleton B.|title=Quantitative Correlation of Relative Rates. Comparison of Hydroxide Ion with Other Nucleophilic Reagents toward Alkyl Halides, Esters, Epoxides and Acyl Halides|journal=Journal of the American Chemical Society|date=January 1953|volume=75|issue=1|pages=141–147|doi=10.1021/ja01097a041}}</ref><ref>{{GoldBookRef|title=Swain–Scott equation| file = S06201}}</ref> derived in 1953:

:<math>\log_{10}\left(\frac{k}{k_0}\right) = sn</math>

This equation relates the rate constant ''k'', of a reaction, normalized to that of a standard reaction with water as the nucleophile (''k''<sub>0</sub>), to a nucleophilic constant ''n'' for a given nucleophile and a substrate constant ''s'' that depends on the sensitivity of a substrate to nucleophilic attack (defined as 1 for [[methyl bromide]]). This equation was modeled after the [[Hammett equation]].

However, both the Swain-Scott equation and the Brønsted relationship make the rather inaccurate assumption that all nucleophiles have the same reactivity with respect to a specific reaction site. There are several different categories of nucleophiles with different attacking atoms (e.g. oxygen, carbon, nitrogen) and each of these atoms has different nucleophilic characteristics.<ref name=Harris/> The Edwards equation attempts to account for this additional parameter by introducing a polarizability term.

== Edwards equations ==

The first generation of the Edwards equation<ref name=ed1/> was

: <math>\log \frac{k}{k_0} = \alpha E_n + \beta H\,</math>

where k and k<sub>0</sub> are the rate constants for a nucleophile and a standard (H<sub>2</sub>O). H is a measure of the basicity of the nucleophile relative to protons, as defined by the equation:

: <math>\ H = pK_a + 1.74 </math>

where the pK<sub>a</sub> is that of the conjugate acid of the nucleophile and the constant 1.74 is the correction for the pK<sub>a</sub> of H<sub>3</sub>O<sup>+</sup>.

E<sub>n</sub> is the term Edwards introduced to account for the polarizability of the nucleophile. It is related to the oxidation potential (E<sub>0</sub>) of the reaction <math>\mathrm{2 X^- \rightleftharpoons X_2 + 2 e^-}</math> (oxidative dimerization of the nucleophile) by the equation:

: <math>\ E_n = E_0 + 2.60 </math>

where 2.60 is the correction for the oxidative dimerization of water, obtained from a least-squares correlation of data in Edwards’ first paper on the subject.<ref name=ed1/> α and β are then parameters unique to specific nucleophiles that relate the sensitivity of the substrate to the basicity and polarizability factors.<ref>{{cite book | last = Carroll | first = Felix | title = Perspectives on Structure and Mechanism in Organic Chemistry | publisher = Wiley | edition = 2 | date = 2010 | location = New Jersey | pages = 506–507 | isbn = 978-0470276105}}</ref>
However, because some β’s appeared to be negative as defined by the first generation of the Edwards equation, which theoretically should not occur, Edwards adjusted his equation. The term E<sub>n</sub> was determined to have some dependence on the basicity relative to protons (H) due to some factors that affect basicity also influencing the electrochemical properties of the nucleophile. To account for this, E<sub>n</sub> was redefined in terms of basicity and polarizability (given as [[molar refractivity]], R<sub>N</sub>):

: <math>\ E_n = a P + b H </math>    where   <math>\ P \equiv \log \frac{R_N}{R_{H_20}}</math>

The values of a and b, obtained by the method of least squares, are 3.60 and 0.0624 respectively.<ref name=ed2/> With this new definition of E<sub>n</sub>, the Edwards equation  can be rearranged:

: <math>\log \frac{k}{k_0} = A P + B H\,</math>

where A= αa and B = β + αb. However, because the second generation of the equation was also the final one, the equation is sometimes written as <math>\log \frac{k}{k_0} = \alpha P + \beta H\,</math>, especially since it was republished in that form in a later paper of Edwards’,<ref name=ed3>{{cite journal | last1 = Edwards | first1 = J.O. | last2 = Pearson | first2 = Ralph G. | title = The Factors Determining Nucleophilic Reactivities | journal = Journal of the American Chemical Society | volume = 84 | issue = 1 | pages = 16–24 | year = 1962 | doi=10.1021/ja00860a005 }}</ref>  leading to confusion over which parameters are being defined.

== Significance ==

A later paper by Edwards and Pearson, following research done by Jencks and Carriuolo in 1960<ref>{{cite journal|last1=Jencks|first1=William P.|last2=Carriuolo|first2=Joan|title=Reactivity of Nucleophilic Reagents toward Esters|journal=Journal of the American Chemical Society|date=April 1960|volume=82|issue=7|pages=1778–1786|doi=10.1021/ja01492a058}}</ref><ref>{{cite journal|last1=Jencks|first1=William P.|last2=Carriuolo|first2=Joan|title=General Base Catalysis of the Aminolysis of Phenyl Acetate|journal=Journal of the American Chemical Society|date=February 1960|volume=82|issue=3|pages=675–681|doi=10.1021/ja01488a044}}</ref> led to the discovery of an additional factor in nucleophilic reactivity, which Edwards and Pearson called the [[alpha effect]],<ref name=ed3/> where nucleophiles with a lone pair of electrons on an atom adjacent to the nucleophilic center have enhanced reactivity. The alpha effect, basicity, and polarizability are still accepted as the main factors in determining nucleophilic reactivity. As such, the Edwards equation is applied in a qualitative sense much more frequently than in a quantitative one.<ref>{{cite journal | last = Hudson | first = Michael J. |author2=Laurence M. Harwood |author3=Dominic M. Laventine |author4=Frank W. Lewis | title = Use of Soft Heterocyclic N‑Donor Ligands To Separate Actinides and Lanthanides | journal = Inorganic Chemistry | volume = 52 | issue = 7 | pages = 3414–3428 | year = 2013 | doi= 10.1021/ic3008848 }}</ref>
In studying nucleophilic reactions, Edwards and Pearson noticed that for certain classes of nucleophiles most of the contribution of nucleophilic character originated from their basicity, resulting in large β values. For other nucleophiles, most of the nucleophilic character came from their high polarizability, with little contribution from basicity, resulting in large α values. This observation led Pearson to develop his [[HSAB theory|hard-soft acid-base theory]], which is arguably the most important contribution that the Edwards equation has made to current understanding of organic and inorganic chemistry.<ref>{{cite journal|title=Hard and Soft Acids and Bases|author=Pearson, Ralph G.|journal= [[J. Am. Chem. Soc.]] |year=1963| volume= 85 |issue=22|pages=3533–3539|doi=10.1021/ja00905a001}}</ref> Nucleophiles, or bases, that were polarizable, with large α values, were categorized as “soft”, and nucleophiles that were non-polarizable, with large β and small α values, were categorized as “hard”.

The Edwards equation parameters have since been used to help categorize acids and bases as hard or soft, due to the approach’s simplicity.<ref>{{cite journal | last1 = Yingst | first1 = Austin | last2 = MacDaniel | first2 = Darl H. | title = Use of the Edwards Equation to Determine Hardness of Acids | journal = Inorganic Chemistry | volume = 6 | issue = 5 | pages = 1067–1068 | year = 1967 | doi= 10.1021/ic50051a051 }}</ref>

== See also ==
* [[Free-energy relationship]]
* [[Brønsted catalysis equation]]
* [[Bell–Evans–Polanyi principle]]

== References ==

{{reflist}}

[[Category:Physical organic chemistry]]
[[Category:Equations]]