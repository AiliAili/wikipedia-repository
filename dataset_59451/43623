__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Sikorsky S-35
 | image=Sikorsky S-35.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type= Transatlantic [[sesquiplane]]
 | national origin=United States
 | manufacturer=[[Sikorsky Aircraft|Sikorsky Manufacturing Company]]
 | designer=
 | first flight=23 August 1926
 | introduced=
 | retired=
 | status=Destroyed
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Sikorsky S-35''' was an [[United States|American]] twin-engined [[sesquiplane]] transport later modified to three-engines. It was designed and built by the [[Sikorsky Aircraft|Sikorsky Manufacturing Company]] for an attempt by [[Rene Fonck]] on a non-stop [[Transatlantic crossing|Atlantic crossing]] for the [[Orteig Prize]].<ref name="archive" /> It was destroyed in the attempt.<ref name="archive" />

==Design and development==
The S-35 was designed as a twin-engined transport with a {{convert|1,000|mi|km|adj=on|abbr=off}} range.<ref name="archive" /> During 1926 [[Rene Fonck]], a French [[First World War]] fighter ace, was looking for a multi-engine aircraft to enter a competition to be the first to fly non-stop from New York to Paris. The prize was $25,000, offered by [[Raymond Orteig]].<ref name="archive" /> Fonck had Sikorsky redesign the aircraft with three engines.<ref name="archive" />

The S-35 was a [[sesquiplane]] with a fixed tailskid landing gear. It was modified to take three {{convert|425|hp|kW|0|abbr=on}} [[Bristol Jupiter|Gnome-Rhône Jupiter 9A]] radial engines and fitted with jettisonable auxiliary landing gear.<ref name=avi>{{cite web |url=http://www.aviastar.org/air/usa/sik_s-35.php|title=Sikorsky S-35|author=|date=February 2007|website=aviastar.org|publisher=|access-date=6 August 2016}}</ref>These modifications took time to complete and the aircraft first flew on 23 August 1926 from [[Roosevelt Field]].<ref name="archive" /> Sikorsky started a series of test flights but as none were at the [[MTOW|maximum takeoff weight]] of {{convert|24200|lb}}, Sikorsky wanted to delay the transatlantic crossing until early 1927, but the promoters of the flight would not accept a delay and the aircraft was prepared for the crossing.<ref name="archive" />

== Operational history==
The first transatlantic attempt was scheduled for the 16 September but was abandoned after the aircraft developed a fuel leak.<ref name="archive" />  The next available break in the weather was to be the 21 September and the aircraft was fueled during the previous night from 50 barrels of gasoline. When the aircraft was weighed it was found to be {{convert|4000|lb}} overweight.<ref name="archive" /> Fonck and his co-pilot Lt Lawrence Curtin of the United States Navy were joined by a radio operator and a Sikorsky mechanic for the flight.<ref name="archive" /> In front of a large crowd at Roosevelt Field the aircraft gathered speed, the auxiliary landing gear broke away, the aircraft failed to get airborne and plunged down a steep slope at the end of the runway and burst into flames.<ref name="archive" /><ref>{{cite book|title=Long Island aircraft crashes 1909-1959|author=Joshua Stoff|page=48}}</ref> The two pilots escaped injury but the radio operator and mechanic were killed, the aircraft which had cost $80,000 was not insured.<ref name="archive" />

==Specifications==
{{Aircraft specs
|ref=Best<ref name="archive" />
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=four
|capacity=
|length m=
|length ft=44
|length in=0
|length note=
|upper span m=
|upper span ft=101
|upper span in=0
|upper span note=
|lower span m=
|lower span ft=76
|lower span in=0
|lower span note=
|height m=
|height ft=16
|height in=0
|height note=
|wing area sqm=
|wing area sqft=794
|wing area note=upper wing
|empty weight kg=
|empty weight lb=9700
|empty weight note=
|gross weight kg=
|gross weight lb=20000
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Powerplant
-->
|eng1 number=3
|eng1 name=[[Bristol Jupiter|Gnome-Rhône Jupiter 9A]]
|eng1 type=radial engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->425
|eng1 note=
|power original=
|thrust original=

|prop blade number=2<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->10
|prop dia in=<!-- propeller aircraft -->6
|prop note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=65
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->7 hours
|ceiling m=
|ceiling ft=16800
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=70 minutes to 16800ft
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass met=
|power/mass imp=
|power/mass note=
|thrust/weight=
|thrust/weight note=

|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
*[[Orteig Prize]]
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=
<ref name="archive">{{cite journal |last=Best |first= Martin |authorlink=  |coauthors= |year=2002 |month= |title=Sikorsky American Fixed-Wing Aircraft, Part 1: Sikorsky S-29-A to S-35|journal=Air-Britain Archive |volume=2002 |issue=4 |pages=127 |issn=0262-4923}}</ref>
}}

==External links==
{{commons category|Sikorsky S-35}}
*[https://www.youtube.com/watch?v=3gJKLpQDOKI The S-35 in practice flights]

{{Aviation accidents and incidents in 1926}}
{{Sikorsky Aircraft}}

[[Category:Sikorsky aircraft|S-035]]
[[Category:United States airliners 1920–1929]]
[[Category:Trimotors]]
[[Category:Sesquiplanes]]