{{Infobox award
| name           = Critics Adult Film Awards
| current_awards = 
| image          = 
| imagesize      = 
| alt            = 
| caption        = 
| description    = Excellence in the porn film genre.<ref name="stagerotic8208" />
| presenter      = Critics Adult Film Association
| sponsor        = East Coast Producers Association of the [[Adult Film Association of America]]<ref name="stagerotic8208" />
| host           = 
| date           = <!-- {{Start date|1981}} -->
| location       = [[New York City]]
| country        = {{USA}}
| reward         = Trophy
| year           = 1981
| year2          = 1987
| holder         = 
| website        = 
| network        = 
| runtime        = 
| ratings        = 
| previous       = 
| main           = 
| next           = 
}}
The '''Critics Adult Film Association''' (CAFA) was a [[New York City|New York]]-based group of [[East Coast of the United States|East Coast]] adult sex [[Film criticism|film critics]], which bestowed [[award]]s upon those working in [[pornographic film]] during the 1980s. The awards were first presented in 1981, honoring the movies of the previous year. ''Talk Dirty to Me,'' a sex [[comedy film|comedy]] starring [[John Leslie (pornographic actor)|John Leslie]], who won Best Actor, was voted best film of 1980. [[Samantha Fox (actress)|Samantha Fox]] was the first Best Actress, winning for her role in ''This Lady Is A Tramp,'' another sex comedy.

''[[Amanda By Night]]'', an adult [[Crime film|crime-drama]] and ''Roommates'', also a drama, would become CAFA's most-honored movies, with the latter being presented with seven awards in 1983, including Best Director ([[Chuck Vincent (director)|Chuck Vincent]]) and Best [[Actress]] ([[Veronica Hart]]).<ref>''The Film Journal'' Volume 87, Pubsun Corp., 1984.</ref> ''Amanda By Night'' scooped six awards the previous year in a ceremony "held at the famed [[Copacabana (nightclub)|Copacabana]]":<ref name="stagerotic8208">"1981 Adult Film Critics Awards", ''Porn Stars: The Stag Erotic Series'' Vol. 3 No. 4, July/August 1982, pp. 6–9.</ref> Best Film, Best Actress (Hart), Best [[Supporting actor|Supporting Actress]] ([[Lisa De Leeuw]]), Best Director ([[Gary Graver|Robert McCallum]]), Best [[Screenplay]] and Best Editing.

The CAFA and [[Adult Film Association of America]] awards were considered the major adult film awards of the [[Golden Age of Porn]]. By 1985, sex news magazine ''Cheri'' declared: "Held at the Silver Lining, a snazzy little [[bistro]] on the [[West Side (Manhattan)|West Side]]'s Restaurant Row, the fifth-annual Critics Adult Film Awards was a gentle reminder that porn films can be measured against standards other than the [[Penile plethysmograph|Peter Meter]]."<ref name="cheri1085">"CAFA Capers: The N.Y. Critics Salute Porndom's Finest", ''Cheri Magazine'', October 1985, pp. 24-27.</ref>

The seventh CAFA Awards were presented May 27, 1987 in [[Manhattan]].<ref name="cinemablue8711">Will H. Jarvis, "The 7th Annual CAFA Awards: The X-rated stars cum out at night!", ''Cinema Blue'' magazine, Vol. 4, No. 8, November 1987, pp. 6-9.</ref> The association seems to have faded away after that, as did the New York-based adult film industry, overtaken by the [[X-Rated Critics Organization]] and its [[XRCO Award|Heart-On Awards]], which had first been presented two years earlier and which were based in California, the new centre of porn production.

In 1986 all three awards programs chose the same Best Picture, Best Director and Best Actress.<ref name="stag1286">Bill Bottiggi, "The 10th Annual Erotica Awards", ''Stag'' magazine, December 1986, pp. 56-57.</ref>

== CAFA Awards ==
The [[Trophy|trophies]] were called the Critics Adult Film Award and each was inscribed with the year the award was being given for, on a gold-plated round icon with decorative edges on a wooden base.<ref name="spelvin-cafa">Photo [http://voices.yahoo.com/image/592220/index.html?cat=49 (enlarged)] accompanying [http://voices.yahoo.com/an-interview-porn-star-icon-georgina-3431958.html?cat=49 "An Interview with Porn Star Icon Georgina Spelvin"] by Charles Shea LeMone, June 8, 2009. Retrieved 2012-05-22.</ref>

In 1987, ''Cinema Blue'' magazine described the voting as a two-stage process: "CAFA President Jack Shugg and officers Steffani Martin, Colette Connor and Will H. Jarvis, aided and abetted by the members of CAFA...mail out two ballots to the New York porn critics. On the first ballot, the critics make five choices for best movies and performers in each category. (This year, any film or video released between January 1, 1986 and February 28, 1987 was eligible.) The ballots are then mailed back and the officers and their assistants count them. The choices with the most votes are then listed on a second ballot and again mailed to the critics who make one final choice in each category. These votes are once again tabulated and the movie or performer in each category with the most votes becomes the winner."<ref name="cinemablue8711" />

Winners are listed below by the year in which the awards presentation was held, not by year the award was for, which was the previous year.

=== Best Film ===
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Film
|-
||1981
||''Talk Dirty to Me''<ref name="rame">Peter van Aarle, [http://www.rame.net/library/lists/best.html "Historical 'Best Porn Movie' Winners"], Rec.Arts.Movies.Erotica (rame.net), June 7, 1997. Retrieved 2012-05-11</ref>
|-
|1982||''Amanda By Night''<ref name="stagerotic8208" /><ref name="rame" />
|-
|1983||''Irresistible''<ref name="rame" />
|-
|1984 (tie)||
* ''[[The Devil in Miss Jones#The Devil in Miss Jones 2|The Devil in Miss Jones 2]]''<ref name="rame" />
* ''Sexcapades''<ref name="rame" />
|-
|1985 (tie)||
* ''Raw Talent'' – Joyce Snyder, producer and Larry Revene, director<ref name="cheri1085" /><ref name="rame" />
* ''Good Girls, Bad Girls'' – Arthur Ben, producer-director<ref name="cheri1085" /><ref name="rame" />
|-
|1986||''Taboo American Style''<ref name="stag1286" /><ref name="rame" />
|-
|1987||'''Winner:''' ''[[The Devil in Miss Jones#The Devil in Miss Jones 3: A New Beginning|The Devil in Miss Jones 3]]''<ref name="exfg8801">Will H. Jarvis, "The 7th Annual CAFA Awards," ''Erotic X-Film Guide'', January 1988, pp. 12-17.</ref>
'''Nominees:''' ''[[Behind the Green Door: the Sequel]]'', ''Every Woman Has a Fantasy 2'', ''Lust on the Orient Express'', ''The Oddest Couple'', ''Star Angel''<ref name="cinemablue8711" />
|}

=== Best Actress ===
[[File:Veronica Hart.png|thumb|right|[[Veronica Hart]] at the 2002 [[AVN Adult Entertainment Expo]]]]
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Actress
|-
|1981||[[Samantha Fox (actress)|Samantha Fox]] – ''This Lady Is A Tramp''<ref name="rame" />
|-
|1982||[[Veronica Hart]] – ''Amanda By Night''<ref name="stagerotic8208" /><ref name="ABN-DVD">Caballero Video Co., ''Amanda By Night'' DVD case</ref>
|-
|1983||Veronica Hart – ''Roommates''<ref name="rame" />
|-
|1984||[[Sharon Mitchell]] – ''Sexcapades''<ref name="rame" />
|-
|1985||[[Colleen Brennan]] – ''Trinity Brown''<ref name="cheri1085" />
|-
|1986||[[Gloria Leonard]] – ''Taboo American Style''<ref name="stag1286" /><ref name="rame" />
|-
|1987||[[Sheri St. Claire]] – ''Sweet Revenge''<ref name="cinemablue8711" /><ref name="exfg8801" />
|}

=== Best Actor ===
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Actor
|-
|1981||[[John Leslie (pornographic actor)|John Leslie]] – ''Talk Dirty to Me''<ref name="rame" />
|-
|1982||Richard Pacheco – ''Nothing to Hide''<ref name="stagerotic8208" /><ref name="rame" />
|-
|1983||John Leslie – ''Talk Dirty to Me 2''<ref name="rame" />
|-
|1984||[[Eric Edwards (pornographic actor)|Eric Edwards]] – ''Sexcapades''<ref name="rame" />
|-
|1985||[[Tom Byron]]{{ref|byron|1}}
|-
|1986||Eric Edwards – ''Corporate Assets''<ref name="rame" />
|-
|1987||[[Jerry Butler (pornographic actor)|Jerry Butler]] – ''Star Angel''{{ref|snake|2}}<ref name="cinemablue8711" /><ref name="exfg8801" />
|}

=== Best Supporting Actress ===
[[File:Sharon Mitchell.jpg|thumb|right|Erotic actress Sharon Mitchell]]
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Actress
|-
|1981||[[Vanessa del Rio]] – ''Dracula Exotica''<ref name="rame" />
|-
|1982 (tie)||
* [[Lisa De Leeuw]] – ''Amanda By Night''<ref name="stagerotic8208" /><ref name="rame" />
* Vanessa del Rio – ''Dancers''<ref name="stagerotic8208" /><ref name="rame" />
|-
|1983 (tie)||
* Lisa De Leeuw<ref name="rame" /> – ''The Blonde Next Door''
* Sharon Mitchell – ''Blue Jeans''<ref name="rame" />
|-
|1984||Sharon Mitchell – ''Night Hunger''<ref name="rame" />
|-
|1985||Colleen Brennan – ''Good Girl, Bad Girl''<ref name="cheri1085" /><ref name="rame" />
|-
|1986||[[Sheri St. Claire]] – ''Corporate Assets''<ref name="rame" />
|-
|1987 (tie)||
* [[Kay Parker]] – ''Careful He May Be Watching''<ref name="cinemablue8711" /><ref name="exfg8801" />
* [[Tasha Voux]] – ''The Oddest Couple''<ref name="cinemablue8711" /><ref name="exfg8801" />
|}

=== Best Supporting Actor ===
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Actor
|-
|1981||Richard Pacheco – ''Talk Dirty to Me''<ref name="rame" />
|-
|1982||Richard Pacheco – ''Dancers''<ref name="stagerotic8208" /><ref name="rame" />
|-
|1983||[[Jamie Gillis]] – ''Roommates''<ref name="rame" />
|-
|1984||[[Robert Kerman|R. Bolla]] – ''The Devil in Miss Jones 2''<ref name="rame" />
|-
|1985||Tom Byron{{ref|byron|1}}
|-
|1986||[[Jose Duval]] – ''Taboo American Style''<ref name="rame" />
|-
|1987||[[Ron Jeremy]] – ''Sweet Revenge''<ref name="cinemablue8711" /><ref name="exfg8801" />
|}

=== Best Director ===
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Director
|-
|1981||[[Anthony Spinelli]] – ''Talk Dirty to Me''<ref name="rame" />
|-
|1982||[[Gary Graver|Robert McCallum]] – ''Amanda By Night''<ref name="stagerotic8208" /><ref name="rame" />
|-
|1983||[[Chuck Vincent (director)|Chuck Vincent]] – ''Roommates''<ref name="rame" />
|-
|1984||Anthony Spinelli – ''Dixie Ray, Hollywood Star''<ref name="rame" />
|-
|1985 (tie)||
* [[Fred J. Lincoln]] – ''Go for It''<ref name="cheri1085" /><ref name="rame" />
* [[Gerard Damiano]] – ''Throat... 12 Years After''<ref name="cheri1085" />
|-
|1986||[[Henri Pachard]] – ''Taboo American Style''<ref name="stag1286" /><ref name="rame" />
|-
|1987||[[Cecil Howard]] – ''Star Angel''{{ref|snake|2}}<ref name="cinemablue8711" /><ref name="exfg8801" />
|}

=== Best Erotic Scene ===
[[File:Vanessa del Rio 1.jpg|thumb|right|American pornographic actress Vanessa del Rio]]
{|class=wikitable width="67%"
!width="15"|Year
!width="200"|Scene
|-
|1981||
|-
|1982||''Games Women Play''<ref name="stagerotic8208" />
|-
|1983 (tie)<ref name=EFG>{{cite book|title=Erotic Film Guide|date=September 1983|publisher=Eton Publishing Company, Inc.|pages=12|edition=Vol.1 No. 9|accessdate=9 March 2015|ref=EFG}}</ref>||
* ''Roommates'' ([[Kelly Nichols]] and Ron Hudd)
* ''Talk Dirty To Me 2'' (John Leslie and Nicole Black)
|-
|1984||''Silk Satin and Sex'' (with Vanessa del Rio and Jerry Butler)<ref name="VdR-blog">Vanessa del Rio, [http://vanessadelrio.wordpress.com/about/ "My Mundane Meandering As Mistress of Masturbatory Memories"] blog. Retrieved 2012-05-22.</ref><ref>Jerry Butler, Robert Rimmer and Catherine Tavel, ''Raw Talent: The Adult Film Industry As Seen By Its Most Popular Male Star'', Prometheus Books, 1990, p. 318. ISBN 0-87975-625-X</ref>
|-
|1985 (tie)||
* Suze Randall's ''Stud Hunters'' ("the Pippi Anderssen group-grope")<ref name="cheri1085" />
* ''Private Teacher'' ("a Tom Byron/[[Kay Parker]] tete-a-tit")<ref name="cheri1085" />
|-
|1986||''Passage Through Pamela'' (transsexual scene)<ref name="stag1286" />
|-
|1987||''The Oddest Couple'' (Danielle and David Christopher)<ref name="cinemablue8711" /><ref name="exfg8801" />
|}

=== Other Categories ===
Like the [[Academy Awards]], CAFA also honored the best in various production categories. For example, at the awards show held in 1985, ''Every Woman Has a Fantasy'' scooped up [[cinematography]] and music awards, ''Raw Talent'' polished off top honors for editing prowess and Sandy Amsterdam's production design and ''Talk DIrty to Me III'' netted make-up and costuming kudos for [[Traci Lords]]' mermaid tail.<ref name="cheri1085" />

However, unlike the Academy Awards, CAFA also presented awards in categories specific to the pornographic movie industry, such as Best Group Scene, bestowed in 1987 to ''[[The Devil in Miss Jones#The Devil in Miss Jones 3: A New Beginning|The Devil in Miss Jones 3: A New Beginning]]'' with [[Vanessa del Rio]] and seven studs.<ref name="cinemablue8711" /><ref name="VdR-blog" /><ref>Dian Hanson and Vanessa del Rio, [http://www.taschen.com/pages/en/catalogue/sex/reading_room/175.thats_slut_with_a_capital_s.5.htm "That's 'Slut' with a capital 'S' "], Taschen Books. Retrieved 2012-05-22.</ref> With the advent of pornography on video, a Best Adult Video award was created, won by [[Candida Royalle]] for ''Urban Heat''<ref>Dr. Eve, [http://www.dreve.co.za/shop/femme-productions-–dvd.html "Femme Productions – DVD"]</ref> in 1985 and ''Christine's Secret'' in 1987.<ref name="cinemablue8711" />

The annual [[Gerard Damiano]] Special Achievement Award, named after the producer, writer and director of the [[1972 in film|1972]] cult classic ''[[Deep Throat (film)|Deep Throat]]'', went to actress [[Georgina Spelvin]] in 1982,<ref name="stagerotic8208" /><ref name="spelvin-cafa" /> Erica Eaton, publicist and former CAFA chairperson, in 1985<ref name="cheri1085" /> and [[Gloria Leonard]] in 1987.<ref name="cinemablue8711" /><ref name="exfg8801" />

== Notes ==
{{Refbegin}}
{{note|byron|1}}There is a discrepancy as to which movie(s) Tom Byron won for in 1985. ''Cheri'' magazine states "He managed to walk off with Best Actor (for ''Sister Dearest'') and Best Supporting Actor (''Talk Dirty To Me III'')".<ref name="cheri1085" /> Internet Adult Film Database founder Peter Van Aarle lists ''Private Teacher'' as his Best Actor win and ''Sister Dearest'' as his Best Supporting Actor award.<ref name="rame" />

{{note|snake|2}} In the book ''The X-Rated Videotape Guide II'', author [[Robert Rimmer]] states, "In May 1987, the CAFA (a New York film critics group) gave ''Snake Eyes II'' awards for Best Script (Anne Randall), Best Actor (Jerry Butler, who also received it for the original ''Snake Eyes''), and Best Director (Cecil Howard)."<ref name="Rimmer">Robert H. Rimmer, [http://www.amazon.com/gp/reader/087975673X/ref=sib_dp_ptu#reader-link ''The X-Rated Videotape Guide II''], p. 522. Prometheus Books, 1991. ISBN 0-87975-673-X</ref> However, this seems unlikely as ''Snake Eyes II'' was not released until 1987<ref>{{IMDb title|0191473|''Snake Eyes II''}}</ref><ref name="iafd">Data for ''Snake Eyes 2'' was taken from the [http://www.iafd.com/title.rme/title=snake+eyes+2/year=1987/snake-eyes-2.htm Internet Adult Film Database].</ref> and it contradicts two other sources that list Butler and Howard winning in 1987 for ''Star Angel'' and no category called Best Script.<ref name="cinemablue8711" /><ref name="exfg8801" /> Possibly ''Snake Eyes II'' won the following year, however, there doesn't seem to be a record of whether any awards were held in 1988.
{{Refend}}

== References ==
{{Reflist}}

== External links ==
* [http://www.imdb.com/event/ev0000196/overview IMDb: CAFA Awards]. Partial listing of film awards handed out by the association

{{adult entertainment awards}}

[[Category:Sex industry in the United States]]
[[Category:American pornographic film awards]]
[[Category:Adult industry awards]]
[[Category:American pornography]]
[[Category:Awards established in 1981]]
[[Category:American film awards]]