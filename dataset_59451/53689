{{more footnotes|date=August 2009}}
{{Infobox University
|name            =Pakistan Institute of Public Finance Accountants
|native_name     =
|latin_name      =
|image           =[[Image:Pipfa logo.png]]
|motto           =
|established     =1993
|type            =[[Professional accountancy body|Professional Accountancy Institute]]
|endowment       =
|staff           =
|president       =Sajid Hussain
|students        =>25,000
|members         =>5,000
|postgrad        =
|doctoral        =
|faculty         =
|city            =[[Karachi]]
|province        =
|country         =[[Pakistan]]{{flagicon|Pakistan}}
|campus          =[[Karachi]] Head Office, Branch Offices at [[Lahore]], [[Islamabad]], and [[Faisalabad]]
|affiliations    =[[International Federation of Accountants]]
|website         =[http://www.pipfa.org.pk]
}}
'''Pakistan Institute of Public Finance Accountants ''' (PIPFA) is an autonomous body recognized mainly in the government sector and established under license from the [[Securities and Exchange Commission of Pakistan]] by the authority given under section 42 of the Companies Ordinance, 1984.

The body is co-sponsored by the [[Institute of Chartered Accountants of Pakistan]], the [[Institute of Cost and Management Accountants of Pakistan]] and the [[Auditor General of Pakistan]].

PIPFA has more than 5,000 members and a number of them are members
of [[Institute of Chartered Accountants of Pakistan|ICAP]] and [[ICMAP]].

The institute was established to produce a second tier of [[accounting]] [[professionals]] in [[Pakistan]].

==Mission statement==
''"identification, development and imparting knowledge to provide a structure for the training of accounting professionals in the specialized areas"''[http://www.pipfa.org.pk/announcement.asp]

==International recognition==
PIPFA is the associate member of [[International Federation of Accountants]] (IFAC).

==Administrative structure==
There are 12 Governors on the Board. Three governors are appointed by each of the three sponsoring bodies and the members of the Association elect three.

In addition to the Board of Governors, PIPFA has the following committees: Examination Committee, Executive Committee, Publication and Seminar Committee, Board of Studies, Regulation and Discipline Committee. These committees are established for the following purposes:

* Education and training of students seeking professional qualification and membership of the Association.
* Investigation of cases of unethical conduct by the members or students and initiate the necessary disciplinary action.
* Provision of technical advice to the members on any accounting or auditing issues.
* Regional committees are organized to promote the professional education activity at regional levels through seminars and workshops

==Educational requirements==
The prerequisite entry requirement is Intermediate education from a recognized education board, university or institution or any other qualification considered equivalent by the Board of Governors or A Levels of General Certificate of Education.

Candidates who have passed CMA Program offered by [[ICMAP]] or CA Program by [[Institute of Chartered Accountants of Pakistan|ICAP]] examinations are eligible for direct membership of the Institute. On July,2016 BOD announced Gateway entry test to membership for ICAP students and ICMAP students who has passed up to CAF and stage 4 respectively. The first gateway exams to be held on November 2016

==Global Affiliation==
'''Association of Chartered Certified Accountants''' ('''ACCA''') grants exemptions in five papers (F2, F3, F4, F5 &F7) to PIPFA affiliate and members.<ref>https://portal.accaglobal.com/accrweb/faces/page/public/accreditations/enquiry/main/EnqProgrammesTable.jspx?_afPfm=a6i56ks33</ref>

'''Chartered Institute of Public Finance and Accountancy''' ('''CIPFA''') grants Direct route to CIPFA membership for members of the Pakistan Institute of Public Finance Accountants(PIPFA).<ref>http://www.cipfa.org/members/become-a-member/direct-membership/pipfa-pakistan</ref>

'''[[Chartered Institute of Management Accountants]]''' ('''CIMA''') grants complete exemption from certificate level and operational level consisting of eight papers.  (Operational level study case Excluded).

==See also==
* [[IFACnet]]
* [[Chartered Institute of Public Finance and Accountancy]]
* [[Institute of Chartered Accountants of Pakistan]]
* [[Institute of Cost and Management Accountants of Pakistan]]
* [[The Society of Accounting Education Pakistan]]

==References==
{{Reflist}}

==External links==
* [http://www.pipfa.org.pk The Official Website of PIPFA]
* [http://www.ifacnet.com/ IFACnet - A KnowledgeNet for Accountants in Business]
* [http://www.accountancy.com.pk Accountancy Forum]
* [http://www.soae.edu.pk The Society of Accounting Education (SOAE Pakistan)]
{{coord missing|Pakistan}}

{{DEFAULTSORT:Pakistan Institute Of Public Finance Accountants}}
[[Category:Professional accounting bodies]]
[[Category:Professional associations based in Pakistan]]
[[Category:Accounting in Pakistan]]
[[Category:Organizations established in 1993]]
[[Category:Pakistan federal departments and agencies]]
[[Category:1993 establishments in Pakistan]]
[[Category:Government agencies established in 1993]]