{{Infobox football biography
| name      = Joshua Beloya
| image       = 
| caption     = 
| fullname    = Joshua Beloya McNally
| birth_date  = {{birth date and age|1991|2|20}}
| birth_place = [[Bacolod]], [[Philippines]]
| height      = {{height|ft=5|in=8}}
| position    = [[Forward (association football)|Striker]]
| currentclub = [[Stallion F.C.|Stallion]]
| clubnumber  = 
| youthyears1 = | youthclubs1 = [[FC Zürich]]
| years1      = 2011  | clubs1 = [[Ceres F.C.|Ceres]]
| years2      = 2012  | clubs2 = [[Kaya F.C.|Kaya]]
| years3      = 2012–2013 | clubs3 = [[Ceres F.C.|Ceres]]
| years4      = 2013| clubs4 = [[Global F.C.|Global]]
| years5      = 2014–2015| clubs5 = [[Ceres F.C.|Ceres]]
| years6      = 2015–2016| clubs6 = [[Kaya F.C.|Kaya]]
| years7      = 2016–    | clubs7 = [[Stallion F.C.|Stallion]]
| caps1       = 5  | goals1 = 12
| caps2       = 12 | goals2 = 3
| caps3       = 0  | goals3 = 0
| caps4       = 5  | goals4 = 1
| caps5       = 0  | goals5 = 0
| caps6       = 1  | goals6 = 1
| caps7       = 0  | goals7 = 0
| nationalyears1 = 2012  | nationalteam1 = Philippines U-21
| nationalyears2 = 2011  | nationalteam2 = [[Philippines national under-23 football team|Philippines U-23]]
| nationalyears3 = 2012– | nationalteam3 = [[Philippines national football team|Philippines]]
| nationalcaps1  = 4 | nationalgoals1 = 1
| nationalcaps2  = 4 | nationalgoals2 = 3
| nationalcaps3  = 1 | nationalgoals3 = 0
| pcupdate       = May 2, 2013
| ntupdate       = November 15, 2012
}}

'''Joshua Beloya McNally''' (born February 20, 1991) commonly known as '''Joshua Beloya''', is an American-Filipino [[Association football|football]] player currently playing as a [[Forward (association football)|striker]] for [[Stallion F.C.|Stallion]] and the [[Philippines national football team|Philippines national team]].

==Career==

===Club career===
He played for the youth team of [[FC Zürich]] in his early days. When he relocated to [[Bacolod]], he joined [[Ceres F.C.|Ceres FC]] and became part of the Bacolod selection which became the champions of the [[2011 PFF National Men's U-23 Championship]]. He was then named the tournament's best striker after scoring 15 goals.<ref name="BMirror blueprint">{{cite news |url=http://businessmirror.com.ph/home/sports/11610-bacolods-blueprint-for-u-23-success |title=Bacolod’s blueprint for U-23 success |author=Olivares, Rick |work=Business Mirror |location=Philippines |date=2011-05-26 |accessdate=2012-01-27 }}</ref>

In early January 2012, he signed up for [[Kaya F.C.|Kaya]] in the [[United Football League (Philippines)|United Football League]].<ref>{{cite news |title=Gier, Beloya, Greatwich lead new UFL faces |url=http://www.interaksyon.com/interaktv/gier-beloya-greatwich-lead-new-ufl-faces |work=InterAKTV |publisher=Associated Broadcasting Company (TV5) |date=2012-01-12
|accessdate=2012-01-12}}</ref> He made his debut in the 1–0 win against [[Philippine Air Force F.C.|Philippine Air Force]] in the opening match.<ref>{{cite news |title=Kaya FC dominates defending UFL champion Air Force in opener |url=http://www.interaksyon.com/interaktv/kaya-fc-dominates-defending-ufl-champion-air-force-in-opener |work=InterAKTV |publisher=Associated Broadcasting Company (TV5) |date=2012-01-14 |accessdate=2012-01-27}}</ref>

In April 1, 2013, it was announced that [[Global F.C.]] signed Beloya in an effort to beef up its lineup for the Asian Football Confederation President’s Cup in May, 2013.<ref>{{cite news |title=Global FC signs striker Joshua Beloya for President’s Cup campaign|url=http://www.interaksyon.com/interaktv/global-fc-signs-striker-joshua-beloya-for-presidents-cup-campaign |work=InterAKTV |publisher=Associated Broadcasting Company (TV5) |date=2013-04-01 |accessdate=2013-04-01}}</ref> He made his Global debut on April 2, 2013 in the 1-1 draw against the [[Loyola Meralco Sparks F.C.]] coming in as a substitute in the 85th minute for Ange Guisso.<ref>[http://www.uflphilippines.com.ph/gameid-1360637330510/results.html Loyola Meralco Sparks vs Global FC]</ref>

===International career===
Beloya came to the attention of the Philippines national football team assistant coach in May 2011 when he scored 12 goals in five matches for his club [[Ceres F.C.|Ceres FC]].<ref>{{cite news |url=http://www.visayandailystar.com/2011/May/10/sportnews2.htm |title=Negros’ Fil-am striker impresses Azkals coach |work=The Visayan Daily Star|date=2011-05-11 |accessdate=2012-02-18 }}</ref> By September 2011, he joined the national team training pool and was named in the provisional [[Philippines national under-23 football team|Philippines under-23]] squad for the [[Football at the 2011 Southeast Asian Games|2011 Southeast Asian Games]].<ref>{{cite web |url=http://soccercentral.ph/news/provisional-roster-southeast-asian-games-named |title=Privisional roster for the Southeast Asian Games named |author=Manahan, John Paul |publisher=Soccer Central Philippines |date=2011-09-09 |accessdate=2011-10-01 }}</ref> During the tournament, he was noted as the player who scored the two late goals in the team's only winning match against [[Laos national under-23 football team|Laos]] which ended 3–2.<ref>{{cite news |title=U23 Azkals nip Laos, 3-2, keep semis hopes alive |url=http://sports.inquirer.net/24183/u23-azkals-nip-laos-3-2-keep-semis-hopes-alive |author=Tupas, Cedelf |work=Philippine Daily Inquirer |date= 2011-11-11 | accessdate=2012-01-27}}</ref> Beloya is also a member of the Philippine Dolphins, the [[Philippines national beach soccer team]].<ref>{{cite news |title=Joshua Beloya plays for the Philippine Dolphins, the National Beach Football Team. |url=http://www.watchmendaily.com/sports/pff-bootfest-another-tough-battle-awaits-ceres-in-semis/ |author=Galunan Jr., Jerome S. |work=Watchmen Daily |date= 2013-01-22 | accessdate=2013-01-30}}</ref>

==Personal life==
Beloya was born in [[Olongapo]] to an American father and a Filipina mother. His father, Joe Lawrence McNally, was an American serviceman who died while Joshua was in his early childhood. His mother then remarried a [[Swiss people|Swiss]] citizen and moved to [[Switzerland]], where he grew up until he and his mother relocated to [[Bacolod]], the mother's hometown, in his mid-teens.<ref name="BMirror blueprint" />

==Honours==

===Club===

;Ceres
*[[PFF National Men's Club Championship]]: [[2012–13 PFF National Men's Club Championship|2012–13]], [[2013–14 PFF National Men's Club Championship|2013–14]]

==References==
{{reflist|2}}

==External links==
* [http://kayafc.com/players/first-team/?show=32 Joshua Beloya] Kaya FC profile
* [http://114.4.38.131/rs2011/bm/cm/athlete_profile.aspx?aid=128744 Joshua Beloya] 2011 SEA Games athlete profile

{{Global F.C. squad}}

{{DEFAULTSORT:Beloya, Joshua}}
[[Category:1991 births]]
[[Category:Living people]]
[[Category:People from Olongapo]]
[[Category:Filipino emigrants to Switzerland]]
[[Category:Swiss people of American descent]]
[[Category:Filipino people of American descent]]
[[Category:American soccer players]]
[[Category:Filipino footballers]]
[[Category:Sportspeople from Bacolod]]
[[Category:Footballers from Negros Occidental]]
[[Category:Association football forwards]]
[[Category:American sportspeople of Filipino descent]]
[[Category:Ceres F.C. players]]
[[Category:Global F.C. players]]
[[Category:Stallion F.C. players]]
[[Category:Kaya F.C. players]]
[[Category:United Football League (Philippines) players]]