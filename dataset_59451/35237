{{good article}}
{{Infobox hurricane season
| Basin=NIO
| Year=2001
| Track=2001 North Indian Ocean cyclone season summary.jpg
| First storm formed=May 21, 2001
| Last storm dissipated=November 12, 2001
| Strongest storm name=ARB 01
| Strongest storm pressure=932
| Strongest storm winds=115
| Average wind speed=3
| Total depressions=6 (1 unofficial)
| Total storms=4 (1 unofficial)
| Total intense=1
| Total super=0
| Fatalities=108
| Damages=104
| five seasons=[[1999 North Indian Ocean cyclone season|1999]], [[2000 North Indian Ocean cyclone season|2000]], '''2001''', [[2002 North Indian Ocean cyclone season|2002]], [[2003 North Indian Ocean cyclone season|2003]]
|Atlantic season=2001 Atlantic hurricane season
|East Pacific season=2001 Pacific hurricane season
|West Pacific season=2001 Pacific typhoon season
}}
The '''2001 North Indian Ocean cyclone season''' was fairly quiet, although activity was evenly spread between the [[Arabian Sea]] and the [[Bay of Bengal]]. There were six [[tropical cyclone scales|depressions]] tracked by the [[India Meteorological Department]] (IMD), which is the official [[Regional Specialized Meteorological Center]] for the northern [[Indian Ocean]]. The agency also tracked four cyclonic storms, which have [[maximum sustained wind|maximum winds]] of at least 65&nbsp;km/h (40&nbsp;mph) sustained over 3&nbsp;minutes. The American-based [[Joint Typhoon Warning Center]] tracked an additional storm &ndash; [[Tropical Storm Vamei]] &ndash; which crossed from the [[South China Sea]] at a record-low latitude.

[[2001 India cyclone|The first storm]] originated on May&nbsp;21, and became the strongest recorded storm in the Arabian Sea at the time. The IMD estimated peak 3&nbsp;minute winds of 215&nbsp;km/h (135&nbsp;mph) while the storm was off the west coast of India. The storm weakened greatly before making [[landfall (meteorology)|landfall]] in [[Gujarat]], and although impact on land was minor, it left up to 950&nbsp;fishermen missing. A few weeks later, the first Bay of Bengal system originated &ndash; a short-lived depression that dropped heavy rainfall upon striking [[Odisha]]. After a period of inactivity during the [[monsoon]] season,<ref name="imd"/> there were cyclonic storms in September and October in the northern Arabian Sea. Both lasted only a few days and dissipated due to unfavorable [[wind shear]]. Another cyclonic storm formed in the Bay of Bengal and struck [[Andhra Pradesh]], which dropped heavy rainfall that was equivalent to 300% of the average October precipitation total. The rains caused flooding, particularly in [[Cuddapah]], where a dam was deliberately opened and inundated the town overnight. There were 153&nbsp;deaths due to the storm and RS5&nbsp;billion ([[Indian rupee]]s, $104&nbsp;million USD) in damage. The final storm of the season tracked by the IMD was a short-lived depression in November in the Bay of Bengal.
__TOC__
{{Clear}}

==Season summary==
<center>
<timeline>
ImageSize = width:781 height:175
PlotArea  = top:10 bottom:80 right:50 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early
DateFormat = dd/mm/yyyy
Period     = from:01/05/2013 till:01/01/2014
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/05/2013
Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.5,0.8,1)      legend:Depression
  id:DD     value:rgb(0.37,0.73,1)    legend:Deep_Depression
  id:TS     value:rgb(0,0.98,0.96)    legend:Cyclonic_Storm
  id:ST     value:rgb(0.8,1,1)        legend:Severe_Cyclonic_Storm
  id:VS     value:rgb(1,1,0.8)        legend:Very_Severe_Cyclonic_Storm
  id:ES     value:rgb(1,0.76,0.25)    legend:Extremely_Severe_Cyclonic_Storm
  id:SU     value:rgb(1,0.38,0.38)    legend:Super_Cyclonic_Storm
Backgroundcolors = canvas:canvas
BarData =
  barset:Hurricane
  bar:Month
PlotData=
  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:21/05/2013 till:28/05/2013 color:ES text:"ARB 01"
  from:12/06/2013 till:13/06/2013 color:TD text:"Depression"
  from:24/09/2013 till:27/09/2013 color:TS text:"ARB 02"
  barset:break
  from:08/10/2013 till:10/10/2013 color:TS text:"ARB 03"
  from:14/10/2013 till:17/10/2013 color:TS text:"BOB 01"
  from:11/11/2013 till:12/11/2013 color:TD text:"BOB 02"
  barset:break
  from:29/12/2013 till:01/01/2014 color:TS text:"Vamei"
 bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/05/2013 till:31/05/2013 text:May
  from:01/06/2013 till:30/06/2013 text:June
  from:01/07/2013 till:31/07/2013 text:July
  from:01/08/2013 till:31/08/2013 text:August
  from:01/09/2013 till:30/09/2013 text:September
  from:01/10/2013 till:31/10/2013 text:October
  from:01/11/2013 till:30/11/2013 text:November
  from:01/12/2013 till:31/12/2013 text:December
</timeline>
</center>
The [[India Meteorological Department]] (IMD) in [[New Delhi]] &ndash; the official [[Regional Specialized Meteorological Center]] for the northern [[Indian Ocean]] as recognized by the [[World Meteorological Organization]] &ndash; issued warnings for tropical cyclones developing in the region. The basin's activity is sub-divided between the [[Arabian Sea]] and the [[Bay of Bengal]] on opposite coasts of India, and is generally split before and after the [[monsoon]] season. The IMD utilized satellite imagery to track storms,<ref name="wmo">{{cite report|publisher=World Meteorological Organization|title=Annual Summary of the Global Tropical Cyclone Season 2001|accessdate=2015-05-16|url=http://www.wmo.int/pages/prog/www/tcp/documents/doc/TD1132-TCseason2001.doc|format=DOC}}</ref> and used the [[Dvorak technique]] to estimate intensity.<ref name="imd"/>

Toward the end of the year, convection was generally lower than normal in the Bay of Bengal, despite being a typical hotspot for activity. There were no deaths or damage throughout the year outside of India, and damage there was lower than what occurred in the previous few seasons.<ref name="wmo"/>

==Systems==

===Extremely Severe Cyclonic Storm ARB 01===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 01A 24 may 2001 0936Z.jpg
|Track=Cyclone 01A 2001 track.png
|Formed=May 21
|Dissipated=May 28
|3-min winds=115
|1-min winds=110
|Pressure=932
}}
{{Main|2001 India cyclone}}
The first storm of the season originated from a tropical disturbance that formed east of [[Somalia]] on May&nbsp;18. Over the following few days, the system gradually organized,<ref name="mgp">{{cite web|author=Gary Padgett|publisher=Typhoon2000|date=2001-07-01|accessdate=2015-05-16|title=Monthly Tropical Weather Summary for May 2001|url=http://www.typhoon2000.ph/garyp_mgtcs/may01.txt}}</ref> becoming a depression on May&nbsp;21. It moved eastward toward the coastline of southwestern India and rapidly intensified on May&nbsp;22, strengthening from a deep depression to a very severe cyclonic storm within 24&nbsp;hours.<ref name="imd"/> After approaching the coastline, the storm turned to the north and northwest away from land due to a [[ridge (meteorology)|ridge]].<ref name="mgp"/> Based on the well-defined [[eye (cyclone)|eye]] and the storm's satellite presentation,<ref name="imd"/> the IMD estimated peak winds of 215&nbsp;km/h (135&nbsp;mph) on May&nbsp;24, and the JTWC estimated 1&nbsp;minute winds of 205&nbsp;km/h (125&nbsp;mph).<ref name="bt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=2001 Missing (2001141N14068)|publisher=Bulletin of the American Meteorological Society|accessdate=2015-05-16|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-2001141N14068}}</ref> It became the strongest storm on record in the Arabian Sea, only to be surpassed by [[Cyclone Gonu]] in 2007.<ref>{{cite book|author=Sekhar Chandra Dutta|page=171|accessdate=2015-05-16|publisher=The Energy and Resources Institute|title=Improving Earthquake and Cyclone Resistance of Structures|year=2012|url=https://books.google.com/books?id=ioRCRjUqaWoC&pg=PA171&lpg=PA171&dq=strongest+arabian+sea+cyclone+gonu+2001&source=bl&ots=CcchAb7Edf&sig=bEY3KvJ4wbnXC4Oni3pfh6HWZZQ&hl=en&sa=X&ei=QWxXVe6SIcqXygTynYHAAw&ved=0CEMQ6AEwCA#v=onepage&q=strongest%20arabian%20sea%20cyclone%20gonu%202001&f=falsed}}</ref> Soon after reaching peak intensity, the cyclone rapidly weakened as it turned northward. By May&nbsp;28, it had deteriorated into a deep depression, and the IMD downgraded the storm to a remnant low before the circulation reached the Gujarat coast.<ref name="bt"/>

Ahead of the storm, all ports in Gujarat, including Kandla, one of the largest in the country, were closed as a precautionary measure.<ref>{{cite news|agency=Reuters|publisher=CNN|date=2001-05-26|accessdate=2015-05-16|title=India cyclone weakening but still 'a threat'|url=http://edition.cnn.com/2001/WORLD/asiapcf/south/05/26/india.cyclone.gujarat/index.html}}</ref> Over 10,000 people were evacuated from coastal areas in the threatened region. Offshore, between 1,500 and 2,000 fishing vessels lost contact with the mainland immediately after the storm.<ref name="cnn">{{cite news|agency=Reuters|publisher=CNN|date=2001-05-25|accessdate=2015-05-16|title=Cyclone threatens India, Pakistan|url=http://edition.cnn.com/2001/WORLD/asiapcf/south/05/25/india.cyclone.ports/index.html}}</ref> However, because the storm remained offshore, the coast only experienced minor damage, although rainfall was widespread.<ref name="imd"/> About 200&nbsp;houses were washed away in [[Kosamba]],<ref name="mgp"/> and one person died in [[Jamnagar]].<ref name="imd"/> About 950 fishermen were missing after the storm, which prompted a helicopter search.<ref>{{cite news|author=Harish Desai|agency=Associated Press|date=2001-05-25|title=Cyclone weakens, but 950 fishermen missing in western India}}{{subscription required|via=Lexis Nexis}}</ref>
{{clear}}

===Cyclonic Storm ARB 02===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 02A 26 sept 2001 0934Z.jpg
|Track=Cyclone 02A 2001 track.png
|Formed=September 25
|Dissipated=September 28
|3-min winds=35
|1-min winds=35
|Pressure=1000
}}
An upper-level disturbance spawned a low pressure area in the eastern Arabian Sea on September&nbsp;24.<ref name="imd"/> It initially consisted of a well-defined circulation on the eastern edge of an area of convection about 370&nbsp;km (230&nbsp;mi) west-southwest of Mumbai. With low wind shear beneath the subtropical ridge, the system gradually organized and developed curved banding features while moving west-northwestward.<ref name="sgp">{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary September 2001|year=2001|accessdate=2014-05-07|url=http://www.typhoon2000.ph/garyp_mgtcs/sep01.txt}}</ref> At 0900&nbsp;UTC on September&nbsp;24, the IMD classified the system as a depression, and nine hours later upgraded it further to a deep depression.<ref name="imd"/> On the same day, the JTWC issued a [[Tropical Cyclone Formation Alert]], although they did not begin advisories due to the center being on the east side of the convection.<ref name="sgp"/> At 0900&nbsp;UTC on September&nbsp;25, the IMD upgraded the system to a cyclonic storm,<ref name="imd"/> estimating peak winds of 65&nbsp;km/h (40&nbsp;mph).<ref name="sbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=2001 Missing (2001267N17070)|publisher=Bulletin of the American Meteorological Society|accessdate=2015-05-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-2001267N17070}}</ref>

Early on September&nbsp;26, the circulation moved beneath the convection, prompting the JTWC to begin advisories on Tropical Cyclone 02A.<ref name="sgp"/> Around that time, the storm's outskirts dropped light rainfall in western India.<ref name="imd"/> Forecasters initially anticipated that the storm would strengthen to winds of 95&nbsp;km/h (60&nbsp;mph) and strike the southern [[Arabian Peninsula]]. However, persistent wind shear dislocated the circulation from the convection and imparted weakening.<ref name="sgp"/> The IMD downgraded the storm to a deep depression and later depression on September&nbsp;27,<ref name="imd"/> by which time the thunderstorms were rapidly dwindling. The JTWC issued their last advisory on September&nbsp;28 after the circulation had no nearby convection. At that time, the storm was located 185&nbsp;km (115&nbsp;mi) east-southeast of [[Masirah Island]] off [[Oman]].<ref name="sgp"/> The IMD also downgraded the depression to a low pressure area on September&nbsp;28 and noted that the remnant system became poorly defined the following day.<ref name="imd"/> 
{{clear}}

===Cyclonic Storm ARB 03===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 03A 09 oct 2001 0555Z.jpg
|Track=Cyclone 03A 2001 track.png
|Formed=October 7
|Dissipated=October 13
|3-min winds=35
|1-min winds=35
|Pressure=998
}}
Similar to the previous storm, an area of convection formed in the Arabian Sea about 185&nbsp;km (115&nbsp;mi) west-southwest of Mumbai on October&nbsp;7. It was associated with a circulation that moved westward from the Indian Coast,<ref name="ogp">{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary October 2001|year=2001|accessdate=2015-05-12|url=http://www.typhoon2000.ph/garyp_mgtcs/oct01.txt}}</ref> which formed as a well-defined low pressure area over western India.<ref name="imd"/> The convection organized and increased, aided by low wind shear and good outflow.<ref name="ogp"/> Late on October&nbsp;8, the IMD classified the system as a depression, and early the next day upgraded it to a deep depression.<ref name="imd"/> A low-level circulation formed beneath a well-defined mid-level storm, with intense convection and strong winds north of the center. At 06:00&nbsp;UTC on October&nbsp;9, the JTWC began classifying the system as Tropical Cyclone 03A.<ref name="imd"/>

With increasing banding features, the storm strengthened while moving west-northwestward, steered by a ridge to the north.<ref name="imd"/> At 09:00&nbsp;UTC that day, the IMD upgraded the system to a cyclonic storm, estimating peak winds of 65&nbsp;km/h (40&nbsp;mph).<ref name="imd"/> Soon thereafter, the storm began weakening due to increased dry air and the upper-level environment becoming unfavorable. The thunderstorms diminished and disappeared over the circulation by October&nbsp;10. That day, the JTWC discontinued advisories,<ref name="ogp"/> and the IMD downgraded it to a remnant low pressure area south of Pakistan. The storm brushed the Indian coast with rainfall, reaching {{convert|105|mm|in|abbr=on}} in [[Gujarat]] state, although there was no major damage.<ref name="imd"/>
{{clear}}

===Cyclonic Storm BOB 01===
{{Infobox Hurricane Small
|Basin=NIO
|Image=BOB storm 15 oct 2001 0520Z.jpg
|Formed=October 14
|Dissipated=October 17
|3-min winds=35
|Pressure=998
}}
Early on October&nbsp;14, a low pressure area formed off the eastern coast of India. While moving generally westward, the system quickly organized into a depression that day.<ref name="imd"/> On October&nbsp;15, the IMD estimated peak winds of 65&nbsp;km/h (40&nbsp;mph),<ref name="4bt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=2001 Missing (2001288N14084)|publisher=Bulletin of the American Meteorological Society|accessdate=2015-05-15|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-2001288N14084}}</ref> based on satellite imagery appearance warranting a [[Dvorak technique|Dvorak rating]] of 2.5; this made it a cyclonic storm.<ref name="imd"/> However, the circulation remained removed from the deep convection.<ref name="ngp"/> Early on November&nbsp;16, the storm made landfall near [[Nellore]], [[Andhra Pradesh]]. It quickly weakened over land, degenerating into a remnant low pressure area over [[Rayalaseema]] on October&nbsp;17.<ref name="imd"/>

While moving ashore, the storm dropped heavy rainfall in Andhra Pradesh and extending into [[Tamil Nadu]],<ref name="imd"/> causing flooding in some areas for the first time in 40&nbsp;years.<ref name="rw1"/> In a 24&#8209;hour period, [[Sullurpeta]] recorded {{convert|261|mm|in|abbr=on}} of precipitation, and 13&nbsp;stations recorded daily totals of over 100&nbsp;mm (4&nbsp;in);<ref name="imd"/> the highest two-day rainfall total was {{convert|676.5|mm|in|abbr=on}}, and some areas received 300% of the average October rainfall within 36&nbsp;hours.<ref name="rw1">{{cite report|author=International Federation of Red Cross And Red Crescent Societies|title=India floods 2001 appeal No. 21/01 operations update No. 6|date=2001-11-09|accessdate=2015-05-16|url=http://reliefweb.int/report/india/india-floods-2001-appeal-no-2101-operations-update-no-6|publisher=ReliefWeb}}</ref> Damage was heaviest in Andhra Pradesh, particularly in Nellore, [[Chittoor]], and [[Kadapa]],<ref name="imd"/> although floods also extended into [[Bihar]]. Several regional roads and rail lines were damaged, including portions of [[National Highway 5 (India)(old numbering)|National Highway 5]], which stranded hundreds of trucks; the routes were reopened within two weeks.<ref name="rw1"/> The rains breached 1,635&nbsp;water tanks, while {{convert|125000|ha|acre|abbr=on}} of crop fields, mostly rice and groundnuts, were impaired. About 1,000&nbsp;head of cattle were killed as well.<ref name="imd"/> In Cuddapah, excess water was released from irrigation dams along the Buggavanka River; water levels rose {{convert|1.5|m|ft|abbr=on}} in the middle of the night, catching residents off guard, and damaging 18,244&nbsp;houses. The dam was also breached in Nellore, and many towns in the region were inundated or isolated for two days.<ref name="rw1"/> Across the state, the storm damaged 55,747&nbsp;houses, accounting for RS5&nbsp;billion ([[Indian rupee]]s, $104&nbsp;million USD) in losses.<ref name="imd"/> There were 153&nbsp;deaths related to the floods and the storm, mostly in Cuddapah.<ref name="rw1"/>

Following the storm, the [[Indian Red Cross Society]] used funds related to previous floods and an earthquake to help 2,000&nbsp;families.<ref>{{cite report|author=International Federation of Red Cross And Red Crescent Societies|title=India floods 2001 appeal No. 21/01 operations update No. 7|date=2001-12-28|accessdate=2015-05-16|url=http://reliefweb.int/report/india/india-floods-2001-appeal-no-2101-operations-update-no-7|publisher=ReliefWeb}}</ref> After the floods, the Indian government provided food and housing to 61,681&nbsp;residents in 130&nbsp;shelters, and distributed 20&nbsp;kg of rice to each household. The army flew helicopters to drop off food, candles, and kerosene to stranded families in Cuddapah. Stagnant waters were disinfected after the floods, and deceased cattle were burned to reduce infection.<ref name="rw1"/> 
{{clear}}

===Other systems===
[[File:Tropical Cyclone 04B 11 nov 2001 0809Z.jpg|right|thumb|Satellite image of the November depression east of India]]
For several days, the JTWC monitored a disturbance in the northern Bay of Bengal for potential development,<ref name="jgp">{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary June 2001|year=2001|accessdate=2014-05-01|url=http://www.typhoon2000.ph/garyp_mgtcs/jun01.txt}}</ref> associated with the southwest monsoon.<ref name="wmo"/> On June&nbsp;9, a low pressure area formed, and it became well-defined by June&nbsp;11.<ref name="imd"/> By that time, there was convection located west of an exposed circulation.<ref name="jgp"/> Early the following day, the IMD classified it as a depression,<ref name="imd">{{cite journal|journal=MAUSAM|volume=53|number=3|date=July 2002|title=Cyclones and depressions over north Indian Ocean during 2001|accessdate=25 August 2016|url=http://www.imdchennai.gov.in/Mausamcw/2001.pdf|format=PDF}}</ref> estimating peak winds of 45&nbsp;km/h (30&nbsp;mph).<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=2001 Missing (2001163N20087)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-10-20|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-2001163N20087}}</ref> Strong wind shear prevented much development.<ref name="jgp"/> Shortly after forming, the system moved northwestward and made [[landfall (meteorology)|landfall]] near [[Paradip]], [[Odisha]]. It quickly weakened below depression intensity on June&nbsp;13 while progressing inland, degenerating into a remnant low near [[Madhya Pradesh]] on June&nbsp;15. The system dropped heavy rainfall along its path, with a daily peak of {{convert|350|mm|in|abbr=on}} in [[Vidarbha]].<ref name="imd"/>

On November&nbsp;7, a cycling area of convection was persistent off the east coast of India, associated with a broad circulation embedded within a [[trough (meteorology)|trough]]. The thunderstorms expanded and gradually organized, aided by good outflow and low wind shear.<ref name="ngp">{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary November 2001|year=2002|accessdate=2014-05-15|url=http://www.typhoon2000.ph/garyp_mgtcs/nov01.txt}}</ref> On November&nbsp;11, the IMD upgraded the system to a depression off Tamil Nadu and Andhra Pradesh.<ref name="imd"/> On the same day, the JTWC began classifying the depression as Tropical Cyclone 04B. Located within a weakness of the ridge, the depression moved slowly to the north and northeast, and was initially expected to move ashore. However, increasing shear removed the convection from the center, and the storm remained offshore while weakening.<ref name="ngp"/> On November&nbsp;12, the IMD downgraded the system back to a low pressure area. The storm brought rainfall to coastal portions of eastern India, peaking at {{convert|150|mm|in|abbr=on}} in [[Paradip]].<ref name="imd"/>

The [[List of Equatorial tropical cyclones|near-equator]] [[Tropical Storm Vamei]] crossed [[Sumatra]] from the [[South China Sea]] at the end of December. According to the [[Japan Meteorological Agency]] &ndash; the official agency covering the western Pacific Ocean &ndash; the storm dissipated on December&nbsp;28 along the east coast of Sumatra. On the next day, the remnants entered the Bay of Bengal, and thunderstorms soon reformed over the circulation due to weak to moderate wind shear. After the remnants of Vamei organized further, the JTWC classified it as Tropical Cyclone 05B on December&nbsp;30, although the agency later treated it as a continuation of the original storm. Moving west-northwest, the storm re-intensified to a secondary peak of 65&nbsp;km/h (40&nbsp;mph). However, an increase in shear left the circulation exposed from the convection. Vamei quickly weakened, dissipating early on January&nbsp;1, 2002.<ref>{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary December 2001|year=2002|accessdate=2014-05-15|url=http://www.typhoon2000.ph/garyp_mgtcs/dec01.txt}}</ref><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=2001 Vamei (2001361N01106)|publisher=Bulletin of the American Meteorological Society|accessdate=2015-05-15|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-2001361N01106}}</ref> The IMD never tracked the storm.<ref name="imd"/>
{{clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of notable tropical cyclones]]
*[[2001 Atlantic hurricane season]]
*[[2001 Pacific hurricane season]]
*[[2001 Pacific typhoon season]]
*South-West Indian Ocean cyclone seasons: [[2000–01 South-West Indian Ocean cyclone season|2000–01]], [[2001–02 South-West Indian Ocean cyclone season|2001–02]]
*Australian region cyclone seasons: [[2000–01 Australian region cyclone season|2000–01]], [[2001–02 Australian region cyclone season|2001–02]]
*South Pacific cyclone seasons: [[2000–01 South Pacific cyclone season|2000–01]], [[2001–02 South Pacific cyclone season|2001–02]]

==References==
{{Reflist|2}}

==External links==
*[http://australiasevereweather.com/cyclones/tc2005bt.htm Gary Padgett Tropical Cyclone Summary]
*[http://australiasevereweather.com/cyclones/tc2006bt.htm Gary Padgett Tropical Cyclone Summary Part 2]
*[http://www.wmo.ch/web/www/TCP/OperationPlans/TCP21-OP2005.pdf Tropical Cyclone Operational Plan for the Bay of Bengal and the Arabian Sea]
*[http://www.imd.gov.in/services/cyclone/impact.htm Impact of Cyclonic Storms and Suggested Mitigation Actions] (by India Meteorological Department)
*[http://www.wmo.ch/pages/prog/www/tcp/documents/doc/TD1132-TCseason2001.doc Annual Summary of Global TC Season 2001]
*[http://ftp.wmo.int/pages/prog/www/TCP_vO/Reports/PTC-29-final-report.pdf WMO/ESCAP Panel on Tropical Cyclones Final Report]

{{TC Decades|Year=2000|basin=North Indian Ocean|type=cyclone}}

{{DEFAULTSORT:2001 North Indian Ocean Cyclone Season}}
[[Category:2001 North Indian Ocean cyclone season|*]]