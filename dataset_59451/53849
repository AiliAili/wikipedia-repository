{{about|the acoustic phenomenon|echoes in telecommunications|Signal reflection|other uses}}
{{listen|
filename=Echo_samples.ogg|
title=Echo samples|
description=Clean signal, followed by different versions of echo.
}}

In [[audio signal processing]] and [[acoustics]], '''echo''' is a [[Reflection (physics)|reflection]] of sound that arrives at the listener with a delay after the direct sound. The delay is proportional to the distance of the reflecting surface from the source and the listener. Typical examples are the echo produced by the bottom of a well, by a building, or by the walls of an enclosed room and an empty room. A true echo is a single reflection of the sound source.{{cn|date=May 2016}}

The word ''echo'' derives from the [[Greek language|Greek]] ἠχώ (''ēchō''),<ref>[http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A1999.04.0057%3Aentry%3Dh%29xw%2F ἠχώ], Henry George Liddell, Robert Scott, ''A Greek-English Lexicon'', on Perseus</ref> itself from ἦχος (''ēchos''), "sound".<ref>[http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A1999.04.0057%3Aentry%3Dh%29%3Dxos ἦχος], Henry George Liddell, Robert Scott, ''A Greek-English Lexicon'', on Perseus</ref> [[Echo (mythology)|Echo]] in the folk story of Greek is a mountain nymph whose ability to speak was cursed, only able to repeat the last words anyone spoke to her. Some animals use echo for location sensing and navigation, such as [[Cetacea|cetaceans]] (dolphins and whales) and bats.

==Acoustic phenomenon==
Acoustic waves are reflected by walls or other hard surfaces, such as mountains and privacy fences. The reason of reflection may be explained as a discontinuity in the [[Wave propagation|propagation]] [[Transmission medium|medium]]. This can be heard when the reflection returns with sufficient magnitude and [[Delay (audio effect)|delay]] to be perceived distinctly.
When sound, or the echo itself, is reflected multiple times from multiple surfaces, the echo is characterized as a [[reverberation]]. 
[[File:Sediment echo-sounder hg.png|thumb|This illustration depicts the principle of sediment echo sounding, which uses a narrow beam of high energy and low frequency]]

The human ear cannot distinguish echo from the original direct sound if the delay is less than 1/15 of a second.{{cn|date=May 2016}} The velocity of sound in dry air is approximately 343&nbsp;m/s at a temperature of 25&nbsp;°C. Therefore, the reflecting object must be more than {{gaps|17.2|m}} from the sound source for echo to be perceived by a person located at the source. When a sound produces an echo in two seconds, the reflecting object is {{gaps|343|m}} away.  In nature, canyon walls or rock cliffs facing water are the most common natural settings for hearing echoes. The strength of echo is frequently measured in [[decibel|dB]] sound pressure level (SPL) relative to the directly transmitted wave.  Echoes may be desirable (as in [[sonar]]) or undesirable (as in [[telephone]] systems).

==In music==
In music performance and recording, electric echo effects have been used since the 1950s. The [[Echoplex]] is a [[Magnetic tape|tape]] [[Delay (audio effect)|delay]] [[audio signal processing|effect]], first made in 1959 that recreates the sound of an acoustic echo. Designed by Mike Battle, the Echoplex set a standard for the effect in the 1960s and was used by most of the notable guitar players of the era; original Echoplexes are highly sought after. While Echoplexes were used heavily by guitar players (and the occasional bass player, such as [[Chuck Rainey]], or trumpeter, such as [[Don Ejudvntfhllis]]), many [[recording studio]]s also used the Echoplex. Beginning in the 1970s, Market built the [[Solid state (electronics)|solid-state]] Echoplex for Maestro. In the 2000s, most echo [[effects unit]]s use electronic or digital circuitry to recreate the echo effect.

==Famous echoes==
[[File:Toothed whale sound production.png|thumb|100 pixel|[[Whales]] [[Animal echolocation|echolocation]] organs, which produce echoes and receive sounds. Arrows illustrate the outgoing and incoming path of sound.]]
*[[Hamilton Mausoleum]], [[Hamilton, South Lanarkshire|Hamilton]], South [[Lanarkshire]], [[Scotland]]: Its high stone holds the record for the longest echo in the world, taking 15 seconds for the sound of a slammed door to decay.
*[[Gol Gumbaz]] of [[Bijapur, Karnataka|Bijapur]], [[India]]: Any whisper, clap or sound gets echoed repeatedly.
*The [[Golkonda]] Fort of [[Hyderabad, India|Hyderabad]], [[India]]
*The Echo Wall at the [[Temple of Heaven]], [[Beijing]], [[China]]
*[[Whispering gallery|The Whispering Gallery]] of [[St Paul's Cathedral]], [[London]], England, UK
*[[Echo Point (lookout)|Echo Point]], the [[Three Sisters (Australia)|Three Sisters]], [[Katoomba, New South Wales|Katoomba]], [[Australia]]
*The Temple of Kukulcan ''("El Castillo")'', [[Chichen Itza]], [[Mexico]]
*The [[Baptistry of Pisa]], [[Pisa]], [[Italy]]
*The echo near [[Milan]] visited by [[Mark Twain]] in ''[[The Innocents Abroad]]''
*The echo in [[Chinon]], [[France]] which is used in a traditional local rhyme.
*The gazebo of [[Napier Museum]] in Trivandrum, Kerala, India.

==References==
{{reflist}}

==External links==
{{Commons category|Echo (acoustics)|Echo}}
{{wikiquote|Echo}}
*[http://www.sonicwonders.org/?p=1291 More information on Chinon echo. ]
*[http://www.acoustics.salford.ac.uk/acoustics_info/duck/ Listen to Duck echoes and an animated demonstration of how an echo is formed. ]

{{Acoustics}}

[[Category:Acoustics]]
[[Category:Audio effects]]