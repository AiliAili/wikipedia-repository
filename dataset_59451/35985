{{good article}}
{{Infobox religious building
|building_name=Akhtala Vank, Pghindzavank <br/> Ախթալայի վանք, Պղնձավանք <br/> ახტალის ტაძარი 
|image=Akhtala monastery 110.JPG|alt=Ancient stone church with many gables sits in a grassy area that is surround by vegetation.
|image_size            = 275px
|caption= 
|map_type = Armenia
|map_size = 275
|location={{flagicon|Armenia}} [[Lori Province]], [[Armenia]]
|geo         = {{coord|41.150578|44.763919|display=inline,title}}
|religious_affiliation=[[Armenian Apostolic Church]]
|district=
|consecration_year=
|status=
|leadership=
|website=
|architect=
|architecture_type=[[Monastery]], [[Church (building)|Church]], [[Fortress]]
|architecture_style=[[Armenian architecture|Armenian]]
|facade_direction=
|year_started=
|year_completed=10th century.
|construction_cost=
|capacity=
|length=
|width=
|width_nave=
|height_max=
|dome_quantity=
|dome_height_outer=
|dome_height_inner=
|dome_dia_outer=
|dome_dia_inner=
|minaret_quantity=
|minaret_height=
|spire_quantity=
|spire_height=
|materials=
}}
[[File:SG Sanahin.JPG|alt=Two bearded figures in stone relief, holding an object that looks like an open door with a bell on top.|thumb|Smbat and Gurgen Bagratuni, relief from Sanahin monastery.]]
'''Akhtala''' ({{lang-hy|Ախթալայի վանք}}; {{lang-ka|ახტალის ტაძარი}}); also known as '''Pghindzavank''' ({{lang-hy|Պղնձավանք}}, meaning ''Coppermine Monastery'') is a 10th-century fortified [[Armenian Apostolic Church]] [[monastery]] located in the town of [[Akhtala]] in the [[Administrative divisions of Armenia|marz]] of [[Lori Province|Lori]], {{convert|185|km|mi|sp=us}} north of [[Yerevan]].<ref>{{cite web |title=RA Lori Marz |publisher=National Statistical Service of Republic of Armenia |url=http://www.armstat.am/Eng/Publications/2007/MARZ_2006E/MARZ_2006E_28.pdf |format=PDF |accessdate=2007-10-05 |archiveurl = https://web.archive.org/web/20071125012459/http://www.armstat.am/Eng/Publications/2007/MARZ_2006E/MARZ_2006E_28.pdf |archivedate = November 25, 2007}}</ref> The monastery is currently inactive.<ref name="LP">{{cite book |last=Ashworth |first=Susie |author2=Simone Egger|author3=Campbell Mattinson |title=Georgia Armenia & Azerbaijan |publisher=[[Lonely Planet]] |edition=2nd |pages=147 |isbn=1-74059-138-0 |year=2004}}</ref> The fortress played a major role in protecting the north-western regions of Armenia ([[Gugark]]) and is among the most well preserved of all in modern Armenia.<ref name="tacentral" /> The main church at the compound is famous for its highly artistic frescoes, which cover the inside walls, the partitions, and the bearings of the building. The modern name of Akhtala was first recorded in a royal decree of 1438. The etymology of the name ''Akhtala'' is believed to be of [[Turkic languages|Turkic]] origin, meaning ''white glade''.<ref name="Akhtala">{{cite book |last=Tadevosyan |first=Aghasi |title=Historical Monuments of Armenia: Akhtala |year=2007 |publisher="Var" Center for Cultural Initiatives |location=Yerevan, Armenia |isbn=978-99941-2-070-3}}</ref> The original Armenian name of the settlement where the monastery is built is Pghindzahank, which means ''copper mine''.<ref name="Akhtala"/>
==The fortress==
[[File:Monasterio de Akhtala, Armenia, 2016-09-30, DD 01.jpg|thumb|left|The monastery within the fortress walls]]
Between 1887 and 1889 the French archaeologist [[Jacques de Morgan]] discovered 576 rectangular stone [[sepulcher]]s, along with cultural items made of clay, bronze and iron near Akhtala dating back to the 8th century BC.<ref name="tacentral" /><ref>{{cite book |last=Kurkjian |first=Vahan |authorlink=Vahan Kurkjian |title=A History of Armenia |publisher =[[AGBU|Armenian General Benevolent Fund]] |year=1958 |pages=8 |location=New York |url=http://penelope.uchicago.edu/Thayer/E/Gazetteer/Places/Asia/Armenia/_Texts/KURARM/2*.html |asin=B000BQMKSI}}</ref> The settlement of modern Akhtala was known as Agarak in the 5th century.<ref name="tacentral">{{cite web |last=Ney |first=Rick |author2=Rafael Torossian|author3= Bella Karapetian |title=Lori marz |work=TourArmenia Travel Guide |publisher=TourArmenia |year=2005 |url=http://www.tacentral.com/lori/LORI_Region_compress.pdf |format=.PDF |accessdate=2007-10-07| archiveurl= https://web.archive.org/web/20071016003132/http://www.tacentral.com/lori/LORI_Region_compress.pdf| archivedate= 16 October 2007 <!--DASHBot-->| deadurl= no}}</ref><ref name="Akhtala" /> The fortress was almost certainly built on top of Bronze and Iron Age foundations.<ref name="tacentral" /> It was built in the late tenth century by the Kyurikids, this branch of the [[Bagratuni Dynasty|Bagratunis]] originated from [[Kiurike I|Gurgen]] (the name was pronounced Kyurikeh in the local dialect of Gugark). He was the son of the patrons of [[Sanahin]] and [[Haghpat]] monasteries located not far from Akhtala, King [[Ashot III|Ashot III the Merciful]] and Queen Khosrovanush. Gurgen's brothers were King [[Smbat II|Smbat II the Conqueror]] and [[Gagik I of Armenia|Gagik I Bagratuni]], under whom the [[Bagratuni Kingdom of Armenia]] reached the peak of its prosperity.<ref>{{cite book
 | last =Macler
 | first =F.
 | title =Armenia, The Kingdom of the Bagratides
 | publisher =The Cambridge Ancient History
 | volume=vol. IV
 | pages=161–165}}</ref>[[File:AkhtFortressWall.JPG|alt=Stone ruins of a structural wall showing remaining windows and turrets, surrounded by vegetation.|thumb|left|The eastern wall of the fortress.]]
Ashot III established the [[Kingdom of Lori]] (Tashir-Dzoraget Kingdom) in Gugark for strategic reasons and enthroned Gurgen in 982.<ref name="Redgate">{{cite book | last = Redgate | first = Anne Elizabeth| title = The Armenians | edition = First | year = 2000 | publisher = Blackwell Publishers Inc. | location = Massachusetts | pages = 225–26,258,261 |isbn = 0-631-22037-2}}</ref> Gurgen along with his brother Smbat are depicted on the sculptures of the patrons in both Sanahin and Haghpat. When the Tashir-Dzoraget kingdom fell as a result of [[Seljuq dynasty|Seljuk]] raids the Kyurikids migrated to [[Tavush]] and Metsnaberd yet they maintained ties with their ancestral fortress and compound in Akhtala. The fortress was built on an elevated rocky outcrop surrounded by deep canyons from three sides forming a natural protection.<ref name="ansambles">{{cite book
 | last =Khalpakhchian
 | first =O.
 | title =Architectural ansambles of Armenia
 | publisher =Iskusstvo Publishers
 | location=Moscow
 | pages =480 pages
 | year=1980
 | isbn = 0-569-08690-6 }}</ref> The somewhat accessible parts between the cliffs are reinforced by towers and walls. The only entrance to the compound is on the northern side protected by bell-shaped towers and walls. The walls and towers of the fortress are built of bluish basalt and lime mortar. The Kyurikids gradually lost their influence under the Seljuk grasp by the end of the 12th century.

The monastic life was revived in Akhtala when the [[Zakarid-Mxargrzeli|Zakarids]] heading the combined Georgian and Armenian forces [[Zakarids|liberated most of Armenia]].<ref name="Akhtala"/> The 13th-century historians [[Kirakos Gandzaketsi]] and [[Vardan Areveltsi]]<ref>{{Cite web
 | last =Bedrosian
 | first =Robert
 | title =Vardan Areweltsi's Compilation of History
 | url =http://rbedrosian.com/va4.htm
 | accessdate =2007-10-07
 | postscript =<!--None-->}}
</ref> called the area Pghndzahank (copper mine), because of rich copper deposits in the surroundings. Gandzaketsi writes the following: "Ivane, Zakare's brother, also died [that year] and was buried at Pghndzahank' near the church which he himself had built, taking it from the Armenians and making it into a Georgian monastery."<ref>{{Cite web
 | last =Bedrosian
 | first =Robert
 | title =Kirakos Ganjakets'i's History of the Armenians
 | url =http://rbedrosian.com/kg8.htm
 | accessdate =2007-10-03
 | postscript =<!--None-->}}
</ref>
Pghndzahank became the property of Ivane Zakarian in the 1180s. While Ivane's brother Zakare was Armenian Apostolic, Ivane had accepted Greek Orthodoxy in the [[List of the Kings of Georgia#King of All Georgia|Georgian court]]. Several monasteries in northern Armenia were converted by the Zakarids to Greek Orthodoxy, a prominent example is the monastery of [[Kobayr]]. By doing so Ivane enhanced his position within the Georgian court and gained influence among the [[Chalcedonian]] Armenians who mostly inhabited Northern and North-Western Armenia. The Zakarids began to lose control starting in the 1220s during the disastrous [[Mongol invasions of Georgia and Armenia]].<ref name="Redgate"/> The son of Ivane, Avak was forced to recognize his subordination to the Mongol leader [[Chormaqan]]. The Mongol rule continued until 1340 when it was interrupted by successive conquests of Turkic tribes. The Turkic tribe of [[Kara Koyunlu]] began attacking the Caucasus and took control of most of Armenia proper by 1400.<ref name="Redgate"/> Their rule was interrupted by the conquests of [[Tamerlane]]. One of the cliffs that surrounds Akhtala is known as Lenktemur, named after Tamerlane who according to local tradition buried one of his wives under the cliff.<ref name="Akhtala"/> [[File:Akhtala bond.jpg|alt=An printed document with ornate decorative borders.|thumb|right|A French [[Bond (finance)|bond]] for the Akhtala mines issued in 1887, depicting the fortress and the monastery.]]
Since the late 18th century the monastery serviced ethnic Greeks who had settled in Akhtala in order to work in the gold and silver mines. Roughly 800 Greek families were moved from [[Gümüşhane]] in the [[Ottoman Empire]] to Akhtala in 1763<ref>{{cite web
 | last =Nazaryan
 | first =Lena
 | title =The Last Remaining Greeks of Madan
 | publisher = Hetq Online
 | date =2007-08-20
 | url =http://archive.hetq.am/eng/society/0708-madan.html
 | accessdate =2007-10-05 | archiveurl= https://web.archive.org/web/20071024123339/http://archive.hetq.am/eng/society/0708-madan.html| archivedate= 24 October 2007 <!--DASHBot-->| deadurl= no}}</ref> by the Georgian King [[Erekle II]].<ref>{{cite book
 | last =Ronald Grigor
 | first =Suny
 | authorlink =Ronald Grigor Suny
 | title =The Making of the Georgian Nation
 | publisher =[[Indiana University Press]]
 | year =1994
 | edition =2nd
 | pages =56
 | isbn =0-253-20915-3 }}</ref> The Greeks called the monastery "Meramani". The Greek miners have left inscriptions on the monastery walls.<ref name="tacentral"/> In the 19th century Akhtala was taken over by the Armenian princely family of [[Melikov]]s.<ref name="Akhtala"/> Currently the monastery has its pilgrimage days on September 20–21. Armenians, Greeks and Georgians visit the monastery on this occasion. The Ambassador of Greece, Panayota Mavromichali visited the monastery on September 20, 2006.<ref name="Akhtala"/> An ore mining and processing plant in Akhtala has been dumping copper mine tailings in the pit below the monastery. This has been classified as a threat to local residents.<ref>{{cite web
 | title =Akhtala Vank and Tailing Dump
 | publisher =Armenia Tree Project
 | url =http://www.armeniatree.org/thethreat/resources/mining_site_photos_061907.pdf
 | format =PDF
 | accessdate =2007-10-03| archiveurl= https://web.archive.org/web/20071005005943/http://www.armeniatree.org/thethreat/resources/mining_site_photos_061907.pdf| archivedate= 5 October 2007 <!--DASHBot-->| deadurl= no}}
</ref>

==Surp Astvatsatsin (Holy Mother of God) church==
The main building of the monastic compound is Surp Astvatsatsin (Holy Mother of God) church. The exact date of the building of the church is unknown.<ref name="tacentral"/> It is generally regarded as an 11th-13th century complex,<ref name="LP"/> but the current church has been built on an earlier foundation.<ref name="Akhtala"/> Kirakos Gandzaketsi mentions that Ivane Zakarian was buried in the church in 1227. [[Stepanos Orbelian]] refers to the church in 1216. Modern researchers date the murals within the church to 1205–1216. Princess Mariam, the daughter of [[Kiurike II|Gurgen II]] (Kyurikeh II) made a record in 1188 on the back of a [[khachkar]] found in a place called Ayor adjacent to Akhtala which refers to the construction of the Holy Mother of God church at Akhtala. The inscription on the khackar states the following: "I, the daughter if Kyurikeh, Mariam, erected Surp Astvatsatsin at Pghndzahank, those who honor us remember us in their prayers."<ref name="Akhtala"/> In 1185 Mariam had constructed the [[narthex]] of the main church in Haghpat. According to some local lore, the church was built in the 7th century by Byzantine emperor of Armenian extraction, [[Heraclius]]. Another legend assumes that the church was built in the 5th century by Georgian King [[Vakhtang I Gorgasali]]. There is no reasonable evidence to support either story.<ref name="Akhtala"/> [[File:Akhtala entrance.JPG|alt=Gabled roof with extensions over a covered entrance with two gabled arches.|thumb|right|The western facade of the church. The arched hall and entrance.]] The church used to contain the cross which according to [[folklore]] was used by [[John the Baptist]] to baptise Jesus Christ. Vasak, the father of Prince Prosh, is said to have given this relic to Ivane Zakarian who later sold it for a large sum to the monastery of [[Noravank]] in [[Syunik Region|Syunik]].<ref name="Akhtala"/>

The church is situated in the middle of the fortress' territory along the longitudinal axis. It belongs to the domed basilica type of churches, where the bearings join with the side-chapels of the apse. Two pairs of arches divided the longitudinal stretched prayer hall into three naves, the central one of which (with double side-chapels) on the eastern side ends with low staged, half-rounded apse and the side-chapels end with sacristies.<ref name="ansambles"/> They are characterized with stylish iconography, richness of theme and variety of different colors (where blue is dominant). The vertical axis of the building was crowned by a massive dome. The pointed dome with the cylindrical drum has not survived. It was damaged during Tamerlane's invasion and completely demolished in 1784 when the [[Caucasian Avars|Avar]] Omar Khan invaded the [[Transcaucasus]] from [[Dagestan]].<ref name="Akhtala"/> In the 19th century, [[Caucasus Viceroyalty (1844-1881)|Viceroy of the Caucasus]], Prince [[Mikhail Semyonovich Vorontsov|Mikhail Vorontsov]] built a semi-spherical wooden dome covered with iron sheets in place of the original dome. The dome was renovated during Soviet years.<ref name="Akhtala"/>

==Murals of Surp Astvatsatsin==
[[File:AkhtalaSouthMural.JPG|alt=Colourful paintings of saints on the stone walls and inside the arches.|thumb|right|Fragment of murals on the southern wall of the church.]]
The murals are one of the best representations of [[Byzantine art]] outside the traditional borders of Byzantium. The majority of the murals bear scriptures in Greek. The murals were painted under the patronage of [[atabek]] Ivane Zakarian between 1205 and 1216. Parallels have been drawn between the murals and the 11th century Armenian miniature paintings of the [[Mugni Gospels]].<ref name="Akhtala"/> The coloring of the murals is characteristic of typical Byzantine art while the thematic solutions are more Armenian. New and Old Testaments scenes as well as various saints including Saint [[Gregory the Illuminator]] are depicted on the murals.<ref name="tacentral"/> A large image of the Holy Virgin is depicted in the dome holding Jesus. The mural has been badly damaged and only parts of it survived. Below the Holy Virgin, the Communion is shown where Jesus is depicted twice, turning on the right and left sharing bread with the [[Apostles]].<ref name="Lidov">{{cite book
 | last =Lidov
 | first =Alexei
 | title =The Mural Paintings of Akhtala
 | publisher =Nauka Publishers
 | location=Moscow
 | pages =129 pages
 | year=1991
 | isbn = 5-02-017569-2 }}</ref> The images of the Apostles [[Saint Peter|Peter]], [[John the Evangelist]], [[Apostle Paul|Paul]] and [[Matthew the Evangelist|Matthew]] have survived. The common Christians saints are depicted below the Communion scene, including [[Pope Sylvester I|Pope Sylvester]], [[James, son of Alphaeus|Saint James]] the son of [[Alphaeus|Alpheus]], Saint [[John Chrysostom]], [[Basil the Great]], [[Gregory the Illuminator]], Jacob of Mtsbin, [[Clement of Rome]], [[Gregory Thaumaturgus|Gregory the Thaumaturgist]], [[Cyril of Alexandria]] and [[Eusebius of Caesarea]]. The murals on the western wall depict the [[Kingdom of God|Kingdom of Heaven]]. The northern wall depicts the trial of Jesus by the high priest of [[Caiaphas]] and by the [[Roman governor|Roman Procurator]] [[Pontius Pilate]]. Some of the murals were renovated in 1979. The arches, niches and columns are also covered by murals.<ref name="Lidov"/>

==Other structures==
[[File:Akhtalaruins.JPG|alt=Remnants of stone wall with foliage growing through the entranceway.|thumb|Ruins at the monastery complex.]]
The most prominent structure after the Holy Mother of God church is a rectangular chapel built against its western wall. The remaining section of the façade of the main church is situated immediately next to it with a ridge roof. Ivane Zakarian and his son Avak were buried inside in 1227.<ref name="Akhtala"/> A small structure with a lean-to roof is attached to the north wall of the main church. It was used to store ceremonial items. On the north-western side of the monastery, a single nave and ridge-roofed church is located detached from the main church.<ref name="ansambles"/> Another building that hasn't survived used to be located next to it. Numerous dilapidated dwellings and auxiliary structures are scattered in the territory of the fortress such as a two-story building believed to be a residence for guards.<ref name="tacentral"/> There are traditional networks of tunnels, crypts, water reservoirs and wine cellars, found among most monasteries of medieval Armenia. Not far from the monastery one can find other medieval monuments such as the Holy Trinity monastery, Saint George church, a 13th-century spring monument, a 19th-century Russian chapel, a Greek church as well as various khachkars and chapels.<ref name="tacentral"/>

==Known residents==
Inscriptions from nearby khachkars point out that the monastery was headed by Petreh in the 1240s. The most prominent figure who resided at the monastery was the translator and scribe Simon of Pghndzahank. His diaries have survived. He was born in 1188 and was a clergyman for several years at the monastery translating Byzantine theological literature.<ref name="Akhtala"/> He collaborated with another Armenian of Chalcedonian faith, Minas Syunakyats of [[Trabzon]]. In 1227 Simon compiled a volume of works by [[Gregory of Nyssa]]. His diary reads:

{{quote|''In 1227 I completed the book by Bishop Gregory of Nyssa which was a preserved old copy translated by the sinful and undeserving clergyman Simon who lived in Armenia, near Lore town, at the Holy Mother of God monastery of Pghndzahank. The book was translated during the reign of atabek Ivane, the founder of the monastery, may God grant him and his sons long life.''<ref name="Akhtala"/>}}

Simon also translated into Armenian the Elements of Theology by [[Proclus|Proclus Diadochos]], The Fountain of Wisdom by [[John of Damascus|John Damascene]], [[The Ladder of Divine Ascent]] by [[John Climacus|John of Sinai]], A History of Georgia (Kartlis Tskhovreba) and The Greek Prayer Book. Simon also noted in his diaries that he only translated works which previously had not been translated into Armenian. The prominent Armenian filmmaker of the 20th century [[Sergei Parajanov]] filmed two episodes of his film [[The Color of Pomegranates]] at the monastery.<ref name="Akhtala"/>

==Gallery==
<center>
<gallery widths="140px" heights="140px" perrow=5 caption="Akhtala Monastery">
Image:Akhtala_fortress_entrance.JPG|Northern wall. The only entrance to the compound
Image:AkhtalaFortWindow.JPG|A view of the hills from one of the windows of the fortress
Image:MelikovTomb.jpg|Tomb of Prince Ivan Aleksandrovich Melikov
Image:AkhtalaAltar.JPG|The main altar of the church and its murals
Image:AkhtMural2.JPG|Murals on the western wall (bottom left)
Image:AkhtMural3.JPG|Murals on the western wall (middle)
Image:AkhtalaHolyVirgin.JPG|The badly damaged painting of the Holy Virgin holding Jesus
File:Akhtal Fort.jpg|Fresco at the monastery
Image:AkhtFort.JPG|A section of the eastern wall
File:Akhtala Monastery.jpg|Akhtala monastery and the fortified walls

File:2579_Akhtala_Pghindzavank_Surb-Astvatsatsin_View-From-The-Road.jpg|Fortress wall and Surb Astvatsatsin, view from the road
File:2590_Akhtala_Pghindzavank_Half-ruined-Fortress-Wall_06-2016.JPG|Half-ruined fortress wall
File:2611_Akhtala_Pghindzavank_Surb-Astvatsatsin_Western-Southern-Facades_06-2016.JPG|Western and southern facades of Surb Astvatsatsin
File:2608_Akhtala_Pghindzavank_Surb-Astvatsatsin_Western-Facade_Fragment_06-2016.JPG|Western facade fragment
File:2610_Akhtala_Pghindzavank_Surb-Astvatsatsin_Western-Facade_Fragment_06-2016.JPG|Western facade fragment
File:2614_Akhtala_Pghindzavank_Surb-Astvatsatsin_Altar_East-Mural_06-2016.JPG|Altar and murals on the eastern wall 
File:2615_Akhtala_Pghindzavank_Surb-Astvatsatsin_South-Mural_06-2016.JPG|Murals on the southtern wall
File:2616_Akhtala_Pghindzavank_Surb-Astvatsatsin_North-Mural_06-2016.JPG|Murals on the northern wall
File:2617_Akhtala_Pghindzavank_Surb-Astvatsatsin_West_Mural_06-2016.JPG|Murals on the western wall
File:Akhtala_monastery_100.JPG|Northern side of the northern wall. The only entrance to the compound
File:Ախթալա_1.JPG|Southern side of the northern wall
File:Looking_towards_the_gate_of_the_monastery_at_Akhtala.jpg|Looking towards the gate
File:Ամրոց_Ախթալա_(Պղնձահանք,_Միսխանա).jpg|Southern side of the northern wall
File:Ахтала_wiki_07.jpg|Fortress wall and Surb Astvatsatsin
File:Akhtala_map_1886.jpg|Akhtala map (1886)
</gallery>
</center>

==References==
{{reflist|2}}

==Further reading==
* [[Alexei Lidov|Lidov A.]], Murals and the art of the Armenian Chalcedonites, "History of the Ancient and Medieval World"
* Akhtala, [[Soviet Armenian Encyclopedia]], vol. 1, Yerevan, 1974
* Melikset-Bek L., The Georgian Sources on Armenia and the Armenians, vol. 3, Yerevan, 1955
* Jalalian A., Kotanjian N., Surp Astvatsatsin Monastery at Akhtala, "Christian Armenia" Encyclopedia, Yerevan, 2002
* Durnovo L., A Brief History of Classical Armenian Painting, Yerevan, 1957
* Durnovo L., An Outline of Medieval Armenian Art, Moscow, 1979
* Bishop Kirion, Akhtala Monastery, Tbilisi, 2005

{{commons category|Akhtala monastery}}
{{Armenian Churches}}

[[Category:Christian monasteries in Armenia]]
[[Category:Tourist attractions in Lori Province]]
[[Category:Castles in Armenia]]
[[Category:Zakarids]]
[[Category:Christian monasteries established in the 10th century]]
[[Category:Oriental Orthodox congregations established in the 10th century]]
[[Category:10th-century establishments in Armenia]]
[[Category:Buildings and structures in Lori Province]]