{|{{Infobox Aircraft Begin
 |name= RD-500 
 |image = RD-500 turbojet engine Kosice 2003.jpg
 |caption = Cutaway of RD-500 turbojet engine
}}
{{Infobox Aircraft Engine
 |type= [[Turbojet]]
 |national origin = [[Soviet Union]]
 |manufacturer= [[Klimov]] 
 |first run=31 December 1947
 |major applications= [[Yakovlev Yak-23]]  
 |number built = about 859
 |program cost = 
 |unit cost = 
 |developed from = [[Rolls-Royce Derwent V]]
 |developed into = 
 |variants with their own articles = 
}}
|}
The '''Klimov RD-500''' was an unlicensed [[Soviet Union|Soviet]] copy of the [[Rolls-Royce Derwent V]] [[turbojet]] that was sold to the Soviet Union in 1947. The [[Klimov]] [[OKB]] adapted it for Soviet production methods and materials.

==Development==
Producing metric drawings and analyzing the materials used in the Derwent V went fairly quickly, but finding a substitute for the high-temperature, [[creep (deformation)|creep]]-resistant [[Nimonic|Nimonic 80]] steel alloy was a more difficult challenge. Eventually an alloy that matched Nimonic 80's high-temperature properties was found in KhN 80T, but it was not creep-resistant. The first Derwent V copy, designated as the RD-500 (''Reaktivnyy Dvigatel{{'}}'' — jet engine) after the Factory No. 500 where the engine was first produced, was being tested on 31 December 1947, but problems cropped up quickly. Combustion was uneven and this cracked the combustion chambers. This may have had something to do with the modifications made by the Soviets to the fuel, speed and starter systems. But these problems were resolved by September 1948 when the engine passed its 100-hour State acceptance test.{{sfn|Kay|2007|page=46}}

RD-500 was a close copy of the Derwent with a single-stage [[centrifugal compressor]], nine [[combustion chamber]]s and a single-stage [[turbine]]. It matched the Derwent's thrust of 15.9&nbsp;kN (3,506&nbsp;lbf) and was only {{Convert|13.7|kg|abbr=on}} heavier. The main problem with the engine in service was with its turbine blades, 30% of which failed inspection due to recrystallization after casting. The poor creep resistance of the KhN 80T alloy resulted in dangerous elongation of the turbine blades. Up to 40% of the early production RD-500s had to be individually adjusted before delivery and the service life of the engine never approached the 100 hours demonstrated in the acceptance test.{{sfn|Kay|2007|page=46}}

The Soviets had enormous problems building the engines to standard, as demonstrated in the 20,000 man-hours required to build a single engine in 1947. This figure dropped to a more reasonable 7,900 man-hours by November 1948 and declined further still to 4,734 man-hours by 1 March 1949, close to the target of 4,000 man-hours. Production by Factory No. 500 totaled 97 in 1948 and 462 in 1949. Factory No. 16 in [[Kazan]] was brought into the program and built 300 engines in 1949.{{sfn|Kay|2007|page=46}} Production was canceled around 1950 in favor of the superior [[Klimov VK-1]] turbojet based on the [[Rolls-Royce Nene]].{{sfn|Kay|2007|page=47}}

The RD-500 was used in a number of early Soviet jet fighters including the [[Lavochkin La-15]], the [[Yakovlev Yak-25 (1947)|Yakovlev Yak-25]], and the [[Yakovlev Yak-30 (1948)|Yakovlev Yak-30]], but only the [[Yakovlev Yak-23]] was accepted for service, albeit in small numbers.<ref>Gunston, pp. 477–78</ref>

The RD-500 was copied and developed further in the [[People's Republic of China]] (PRC) as the '''Shenyang Aircraft Development Office PF-1A'''.

==Applications==
* [[Lavochkin La-15]]
* [[Mikoyan-Gurevich KSK]] test plane
* [[Raduga KS-1 Komet]]
* [[Yakovlev Yak-23]]
* [[Yakovlev Yak-25 (1947)|Yakovlev Yak-25]]
* [[Yakovlev Yak-30 (1948)|Yakovlev Yak-30]]
* [[Yakovlev Yak-1000]]

==Specifications (RD-500)==
{{Jetspecs
|ref=Kay, ''Turbojet''
|type=[[Turbojet]]
|length={{Convert|2.11|m}}
|diameter={{Convert|1.09|m}}
|weight={{Convert|580.7|kg|abbr=on}}
|compressor=Single-stage centrifugal compressor
|combustion=Nine
|turbine=Single-stage
|fueltype=
|oilsystem=
|power=
|thrust={{Convert|15.9|kN|lbf|abbr=on}}
|compression=
|fuelcon=
|specfuelcon=1.4
|power/weight=
|thrust/weight=2.73
}}
<!-- ==See also== -->
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar engines=
|lists= 
* [[List of aircraft engines]]}}

==References==

===Notes===
{{Reflist|2}}

===Bibliography===
{{Refbegin}}
* {{cite book |last1=Kay |first1=Anthony L. |title=Turbojet: History and Development 1930–1960 |volume=2: USSR, USA, Japan, France, Canada, Sweden, Switzerland, Italy, Czechoslovakia and Hungary |location=Marlborough, Wiltshire |publisher=Crowood Press |year=2007 |isbn=978-1-86126-939-3 |ref=harv}}
* [[Bill Gunston|Gunston, Bill]]. ''The Osprey Encyclopaedia of Russian Aircraft 1875–1995''. London, Osprey, 1995 ISBN 1-85532-405-9
{{Refend}}

{{Klimov aeroengines}}
{{Russian and Soviet military designation sequences}}

{{DEFAULTSORT:Klimov Rd-500}}
[[Category:Turbojet engines 1940–1949]]
[[Category:Klimov aircraft engines|RD-500]]
[[Category:Centrifugal-flow turbojet engines]]