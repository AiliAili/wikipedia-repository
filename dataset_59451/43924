<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=E430
 |image=Yuneec E430.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=[[light sport aircraft|Light sport]] [[electric aircraft]]
 |manufacturer=[[Yuneec International]]
 |designer=
 |first flight=12 June 2009
 |introduced=
 |retired=
 |status=under development
 |primary user=
 |more users=
 |produced=
 |number built=at least two prototypes
 |unit cost= US$89,995(2010 - projected)<ref name="AvWeb29Jul10" />
 |variants with their own articles=
}}
|}

The '''Yuneec International E430''' is a [[China|Chinese]] two-seat [[electric aircraft]] designed for commercial production by electric [[model aircraft]] manufacturer [[Yuneec International]]. The first flight of the E430 took place from the Yuneec factory near [[Shanghai]], [[China]] on June 12, 2009.<ref name="Gizmag22Jun09">{{cite web|url = http://www.gizmag.com/yuneec-e430-electric-aircraft/12036/|title = The Yuneec E430 aims to be the world's first commercially available electric aircraft |accessdate = 2009-07-30|last = Hanlon|first = Mike|authorlink = |date=June 2009}}</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 85. WDLA UK, Lancaster UK, 2011. {{ISSN|1368-485X}}</ref>

==Design==
The E430 is a two-seat, [[V tail]]ed, composite aircraft with a high-aspect ratio wing. Take-off speed is 40&nbsp;mph, cruise speed is 60&nbsp;mph, and max speed is 95&nbsp;mph.<ref name="WDLA11" /><ref name="Specs">{{cite web|url = http://yuneec.com/Aircraft_specification.html|title = E430 Specifications|accessdate = 2009-07-30|last = Yuneec International|authorlink = |year = 2008}}</ref><ref name="AvWeb24Jun09">{{cite web|url = http://www.avweb.com/avwebflash/news/ElectricAviationMovesForward_200609-1.html|title = Electric Aviation Moves Forward |accessdate = 2009-06-25|last = Grady|first = Mary|authorlink = |date=June 2009}}</ref><ref name="AvWeb22Jul09">{{cite web|url = http://www.avweb.com/avwebflash/news/FlightstarUltralightFirstFlightYuneecStartsUSFlightTesting_200774-1.html|title = Electric Flight Update: Flightstar Ultralight First Flight, Yuneec Starts U.S. Flight Testing  |accessdate = 2009-07-23|last = Grady|first = Mary|authorlink = |date=July 2009}}</ref>

The company claims that the battery packs have an expected lifespan of 1500 hours and cost [[United States Dollar|US$]]7000 each, with the aircraft carrying 3-5 battery packs, giving two to two and half hours endurance. The batteries can be recharged in 3–4 hours from a 220v outlet. The company projects that by the time the first customers require replacement battery packs that improved and less expensive ones will be available.<ref name="AvWeb29Jul10">{{cite web|url = http://www.avweb.com/podcast/podcast/EAAAirVenture2010_CliveCoote_YuneecInternational_ElectricAirplane_203013-1.html?kw=RelatedStory|title = AVweb Interviews Yuneec International's Clive Coote |accessdate = 29 July 2010|last = Pew|first = Glenn|authorlink = |date=July 2010}}</ref><ref name="AvWeb27Jul09">{{cite web|url = http://www.avweb.com/news/airventure/EAAAirVenture2009_YuneecElectricLSAHopefulDebuts_200833-1.html|title = Yuneec Electric LSA Hopeful Debuts At AirVenture |accessdate = 2009-07-29|last = Grady|first = Mary|authorlink = |date=July 2009}}</ref>

==Development==
The aircraft is being developed as a [[homebuilt aircraft|kit aircraft]] for the US market. The development of the E430 is being funded  entirely by Yuneec CEO Tian Yu. The company is constructing a 260,000 square foot (25,000 sq m) factory to produce the aircraft in Shanghai, that was expected to open in October 2009. Production of the E430 is expected to commence in late 2010 and first customer deliveries were initially forecast for early 2011.<ref name="AvWeb24Jun09" /><ref name="AvWeb22Jul09" /><ref name="AvWeb27Jul09" />

The aircraft was first flown on 12 June 2009 and then shipped for further testing to [[Camarillo, California]]. On 14 July 2009 the prototype aircraft was registered in the USA as N386CX and on 18 July 2009 it was given a [[Certificate of Airworthiness]] by the [[Federal Aviation Administration]] and further test flights were carried out, totalling 22 hours. The prototype E430 was then shipped by truck to [[Wisconsin]] and displayed at [[EAA AirVenture Oshkosh]] in July 2009.<ref name="AvWeb27Jul09" /><ref name="News">{{cite web|url = http://yuneeccouk.site.securepod.com/Aircraft_News.html|title = E430 News|accessdate = 2009-07-30|last = Yuneec International|authorlink = |date=July 2009}}</ref><ref name="FAAreg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=386CX|title = FAA Registry N-Number Inquiry Results|accessdate = 2009-07-30|last = [[Federal Aviation Administration]]|authorlink = |date=June 2009}}</ref> It was also on display at that venue in summer of 2010.<ref name=cw2010>{{cite web | title = EAA AirVenture 2010: Rain on the Wittman Field runways can't dampen Oshkosh Fly-In enthusiasm | author = Sara Black | date = August 31, 2010 | publisher = Composites World | url = http://www.compositesworld.com/articles/eaa-airventure-2010 | accessdate = 2012-03-16 }}</ref>

Yuneec is working on developing a solar-cell installation for the wings that will recharge the aircraft's batteries.<ref name="AvWeb27Jul09" />

In July 2009 the company estimated that the price for a commercially available [[light sport aircraft]] production version of the E430 would be US$89,000.<ref name="AvWeb27Jul09" /><ref name=cw2010/>

At [[AirVenture]] 2010 the company announced that it would start accepting orders for the aircraft after the show. At that date the aircraft was advertised as having an endurance of 2.25 to 2.5 hours with a useful load of {{convert|390|lb|kg|0|abbr=on}}. Deliveries were initially scheduled for late 2011, but by the end of 2012 there was no indication that more than prototypes had been completed.<ref name="AvWeb28Jul10">{{cite web|url = http://www.avweb.com/news/airventure/EAAAirVenture2010_yuneec_electric_aircraft_e430_eviva_202997-1.html|title = Yuneec: Electric Aircraft Are Here, Now|accessdate = 29 July 2010|last = Pew | first = Glenn |authorlink = |date=July 2010}}</ref><ref name="News2012">{{cite web|url = http://www.yuneec.com/Aircraft_News.html|title = e430 News|accessdate = 5 December 2012|last = Yuneec International|date = 1 August 2010}}</ref>

In June 2013 it was announced that the aircraft will be produced, marketed and supported by [[GreenWing International]], however that company went out of business in about 2014.<ref name="Grady26Jun13">{{cite news|url = http://www.avweb.com/avwebflash/news/NewCompanyWillMarketYuneecElectricAircraft_208910-1.html|title = New Company Will Market Yuneec Electric Aircraft |accessdate = 27 June 2013|last = Grady|first = Mary|date = 27 June 2013| work = AVweb}}</ref><ref name="FB">{{cite web|url = https://www.facebook.com/GreenWingInternational|title = GreenWing International|work=[[Facebook]]|accessdate = 10 January 2017|author = GreenWing International|date = 29 December 2013}}</ref>

==Operational history==
The E430 was named the winner of the Lindberg prize for electric aircraft at [[AirVenture]] in 2010. In the same year it was named [[Brit plc|Brit Insurance]] Design of the Year in the transport category.<ref name="News2012" />

By December 2012 a total of two examples had been [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]]. The first one was the initial prototype shipped to the US, registered in the [[Experimental aircraft|Experimental - Exhibition]] category on 14 July 2009, although its registration expired on 31 March 2012. The second was registered in the Experimental - Research and Development category on 26 January 2011 to Flying Tian of [[Monterey Park, California]].<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/MMS_results.aspx?Mmstxt=05628Q0&Statetxt=CA&conVal=0&PageNo=1|title = Make / Model Inquiry Results|accessdate = 5 December 2012|last = [[Federal Aviation Administration]]|date = 5 December 2012}}</ref>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (E430) ==
{{Aircraft specs
|ref=Yuneec International, AvWeb and Bayerl<ref name="WDLA11" /><ref name="Specs" /><ref name="AvWeb24Jun09" /><ref name="News" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=6.98
|length ft=
|length in=
|length note=
|span m=13.8
|span ft=
|span in=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=11.37
|wing area sqft=
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg= 250
|empty weight lb=
|empty weight note=with batteries
|gross weight kg=470
|gross weight lb=
|gross weight note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Yuneec Power Drive 40]], powered by Yuneec OEM Lithium Polymer batteries, 13 kg (28.6 lbs), 66.6V (30 Ah) each
|eng1 type=
|eng1 kw=40<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=2
|prop name=fixed pitch
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=150
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=90
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=70
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=227
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=25:1
|climb rate ms=3.5
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=41.3
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

== See also ==
* [[Electric aircraft]]

== References ==
{{reflist|30em}}

== External links ==
* {{official website|http://www.yuneec.com/}}
{{Yuneec International aircraft}}

[[Category:Electric aircraft]]
[[Category:Homebuilt aircraft]]
[[Category:V-tail aircraft]]
[[Category:Yuneec aircraft|E430]]