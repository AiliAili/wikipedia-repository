{{Italic title}}
[[File:Wonder-Book, 1880.jpg|thumb|Eustace Bright telling the stories to several children, the frontispiece illustration of an 1880 edition <!-- anonymous? -->]]
'''''A Wonder-Book for Girls and Boys''''' (1851) is a children's book by American author [[Nathaniel Hawthorne]] in which he retells several Greek myths. It was followed by a sequel, ''[[Tanglewood Tales]]''.

==Overview==
The stories in ''A Wonder-Book for Girls and Boys'' are all [[story-within-a-story|stories within a story]], the [[frame story]] being that a [[Williams College]] student, Eustace Bright, is telling these tales to a group of  children at [[Tanglewood]], an area in [[Lenox, Massachusetts]], where Hawthorne lived for a time. All the tales are modified from the original myths.

[[File:Midas gold2.jpg|right|thumb| "Midas' Daughter Turned to Gold" by [[Walter Crane]], illustrating the [[Midas]] myth for an 1893 edition ]]
''A Wonder-Book for Girls and Boys'' covers the myths of
*[[Perseus|The Gorgon's Head]] - recounts the story of Perseus killing Medusa at the request of the king of the island, Polydectes.
*[[Midas|The Golden Touch]] - recounts the story of King Midas and his "Golden Touch".
*[[Pandora|The Paradise of Children]] - recounts the story of Pandora opening the box filled with all of mankind's Troubles.
*[[Heracles#The Twelve Labours|The Three Golden Apples]] - recounts the story of Heracles procuring the Three Golden Apples from the Hesperides' orchard, with the help of Atlas.
*[[Baucis and Philemon|The Miraculous Pitcher]] - recounts the story of Baucis and Philemon providing food and shelter to two strangers who were Zeus and "Quicksilver" (Hermes) in disguise. Baucis and Philemon were rewarded by the gods for their kindness; they were promised never to live apart from one another.
*[[Bellerophon|The Chimæra]] - recounts the story of Bellerophon taming Pegasus and killing the Chimæra.

==Composition and publication history==
Hawthorne expressed his idea to rewrite Greek myths as early as 1846 when he outlined a book to [[Evert Augustus Duyckinck]] of stories "taken out of the cold moonshine of classical mythology, and modernized, or perhaps gothicized, so that they may be felt by children of these days."<ref name=Miller345>Miller, Edwin Haviland. ''Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 345. ISBN 0-87745-332-2.</ref> In 1851, just after the birth of his daughter [[Mother Mary Alphonsa|Rose]] he proposed the idea again in the form of a collection of six tales. His aim would be, he wrote, "substituting a tone in some degree Gothic or romantic, or any such tone as may please myself, instead of the classic coldness, which is as repellent as the touch of marble... and, of course, I shall purge out all the old heathen wickedness, and put in a moral wherever practicable."<ref name=Miller345/>

Publisher [[James Thomas Fields]] pushed for Hawthorne to complete the project quickly. Fields had begun reissuing the author's earlier series for children titled ''Grandfather's Child'', originally published by [[Elizabeth Palmer Peabody]] and now renamed ''True Stories from History and Biography'', and was also planning a new edition of ''[[Twice-Told Tales]]''.<ref>Wineapple, Brenda. ''Hawthorne: A Life''. Random House: New York, 2003: 229. ISBN 0-8129-7291-0.</ref> The entirety of the collection was written between June and mid-July 1851. He sent the final manuscript to Fields on July 15 and wrote: "I am going to begin to enjoy the summer now and to read foolish novels, if I can get any, and smoke cigars and think of nothing at all — which is equivalent to thinking of all manner of things."<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 369. ISBN 0-395-27602-0.</ref>

[[File:Hawthorne - Lenox home 01.JPG|thumb|left| Recreation of the home in Lenox, Massachusetts, where Hawthorne wrote ''A Wonder-Book'']]
The Hawthornes had moved [[The Berkshires]] shortly after the publication of ''[[The Scarlet Letter]]'' and it was here that he completed not only ''A Wonder-Book'' but also his novel ''[[The House of the Seven Gables]]''.<ref>Wright, John Hardy. ''Hawthorne's Haunts in New England''. Charleston, SC: The History Press, 2008: 93. ISBN 978-1-59629-425-7.</ref> He was able to spend time with several other literary figures, including [[Herman Melville]], who was then living at [[Arrowhead (Herman Melville House)|Arrowhead]] in [[Pittsfield, Massachusetts|Pittsfield]]. Melville is referenced by name in ''A Wonder-Book'': "On the higher side of Pittsfield, sits Herman Melville, shaping out the gigantic conception of his 'White Whale' while the gigantic shape of [[Mount Greylock|Graylock]]{{sic}} looms upon him from his study-window."<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 373. ISBN 0-395-27602-0.</ref> Biographer Philip McFarland called this period "by far the most productive creative period of Hawthorne's life", though it also marked the end of his time as a writer of short tales.<ref>McFarland, Philip. ''Hawthorne in Concord''. New York: Grove Press, 2004: 137. ISBN 0-8021-1776-7</ref> The Hawthornes would soon move temporarily to [[West Newton, Massachusetts]], where the author would begin to write ''[[The Blithedale Romance]]'', a novel he conceived while in Lenox.<ref>Mather, Edward. ''Nathaniel Hawthorne: A Modest Man''. New York: Thomas Y. Crowell Company, 1940: 210.</ref>

==Response==
Hawthorne wrote ''A Wonder-Book'' immediately after ''The House of the Seven Gables''. That novel had sold 6,710 copies by August 1851, and ''A Wonder-Book'' sold 4,667 copies in just two months after its November 1851 publication.<ref>Mitchell, Thomas R. "[https://books.google.com/books?id=iIjx-GhqXlYC&dq=%22A+Wonder-Book+for+Girls+and+Boys%22&source=gbs_navlinks_s In the Whale's Wake]", in ''Hawthorne and Melville: Writing a Relationship'' (Jana L. Argersinger and Leland S. Person, editors). Athens, GA: University of Georgia Press, 2008: 258. 9780820330969</ref> By comparison, his friend Herman Melville's novel ''[[Moby-Dick]]'' was released the same month, with the British edition selling under 300 copies in two years, and the American edition under 1800 in the first year.<ref>Cotkin, George. ''[https://books.google.com/books?id=IWJpAgAAQBAJ&pg=PA134 Dive Deeper: Journeys with Moby-Dick]''. New York: Oxford University Press, 2012: 134. ISBN 9780199855742</ref> Hawthorne later authorized a French edition of ''A Wonder-Book'' and bought a copy in [[Marseille]] while on a European vacation in 1859. He joked in his journal about the purchase of "the two volumes of the ''Livre des Merveilles'', by a certain author of my acquaintance."<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 515. ISBN 0-395-27602-0.</ref>

==References==
{{Reflist|2}}

==External links==
*[http://www.gutenberg.org/etext/32242 ''A Wonder Book for Girls & Boys''], illustrated by Walter Crane (1893)
* {{librivox book | title=A Wonder Book for Girls and Boys | author=Nathaniel HAWTHORNE}}

{{Portal |Children's literature}}

{{Nathaniel Hawthorne}}

{{DEFAULTSORT:Wonder-Book for Girls and Boys}}
[[Category:1851 books]]
[[Category:Children's short story collections]]
[[Category:Short story collections by Nathaniel Hawthorne]]
[[Category:American children's books]]
[[Category:Works based on Greek and Roman works]]