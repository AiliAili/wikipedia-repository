The [[Supreme Court of the United States|U.S. Supreme Court]] is entrusted with resolving disputes about how the [[United States Constitution]] and other federal laws should be applied to cases that have been appealed from lower courts. The justices base their decisions on their interpretation of both legal doctrine and the [[precedent]]ial application of laws in the past. In most cases, interpreting the law is relatively clear-cut and the justices decide unanimously without dissent. However, in more complicated or controversial cases, the Court is often divided.

It has long been commonly assumed that the votes of Supreme Court justices reflect their [[Jurisprudence|jurisprudential philosophies]] as well as their [[Ideology#Political ideologies|ideological leanings]], [[Attitude (psychology)|personal attitudes]], [[Value (personal and cultural)|values]], [[Political philosophy|political philosophies]], or policy preferences. A growing body of academic research has confirmed this understanding: scholars have found that the justices largely vote in consonance with their perceived values.<ref name=SegalSpaeth2002>{{cite book
| title=The Supreme Court and the Attitudinal Model Revisited
| first1=Jeffrey A.
| last1=Segal
| first2=Harold J.
| last2=Spaeth
| publisher=Cambridge University Press
| location=Cambridge
| year=2002
| isbn=978-0521783514
}}
</ref><ref>
{{cite journal
| title=Is the Roberts Court Especially Activist? A Study of Invalidating (and Upholding) Federal, State, and Local Laws
| first1=Lee
| last1=Epstein
| first2=Andrew D.
| last2=Martin
| journal=Emory Law Journal
| volume=61
| year=2012
| pages=737–758
| quote=…for Justices appointed since 1952, Epstein & Landes’s findings parallel ours: the vast majority were opportunistic restraintists (activists), willing to uphold laws that were consistent with their policy preferences and strike those that were not.
| url= http://www.law.emory.edu/fileadmin/journals/elj/61/61.4/Epstein_Martin.pdf
}}</ref> Analysts have used a variety of methods to deduce the specific perspective of each justice over time.

== Ideological leanings over time ==
Researchers have carefully analyzed the judicial rulings of the Supreme Court—the votes and written opinions of the justices—as well as their upbringing, their political party affiliation, their speeches, editorials written about them at the time of their Senate confirmation, and the political climate in which they are appointed, confirmed, and work.<ref name=ZornCaldeira2008>Zorn and Caldeira provide a good overview of these methods and their limitations: {{cite web
| first1=Christopher
| last1=Zorn
| first2=Gregory A.
| last2=Caldeira
| title=Measuring Supreme Court Ideology
| date=5 February 2008
| url=http://www.adm.wustl.edu/media/courses/supct/ZC2.pdf
| accessdate=30 November 2012
}}</ref> From this data, scholars have inferred the ideological leanings of each justice and how the justices are likely to vote on upcoming cases.<ref>
For example:
* {{cite journal
| first1=Jeffrey A.
| last1=Segal
| first2=Albert D.
| last2=Cover
| title=Ideological Values and the Votes of U.S. Supreme Court Justices
| journal=American Political Science Review
|date=June 1989
| volume=83
| issue=2
| pages=557–565
| url=http://www.uic.edu/classes/pols/pols200mm/Segal89.pdf

| doi=10.2307/1962405
}} Segal and Cover found (p. 561) a strong correlation (0.80) between justices' perceived ideological perspectives on civil liberties and civil rights issues as attributed to them in elite newspaper editorials written just before their confirmation (their [[Segal–Cover score]]) and their later votes in the study period 1953–1988.
* {{cite journal
| last1=Epstein
| first1=Lee
| first2= Thomas G.
| last2=Walker
| first3=William J.
| last3=Dixon
| title=The Supreme Court and Criminal Justice Disputes: A Neo-Institutional Perspective
| journal=American Journal of Political Science
|date=November 1989
| volume=33
| issue=4
| pages=825–841
| url=http://epstein.law.northwestern.edu/research/cjdisputes.pdf
| doi=10.2307/2111111
}} Epstein, Walker, and Dixon found they could explain and predict rulings in criminal justice cases (in the study period 1946–1986) using a simple model with four inputs: the political party affiliation of the majority of justices, the political party affiliation of the current president (representing the current political climate), the Supreme Court rulings in criminal justice cases in the previous year, and the percent of criminal cases the Court decides to hear in the current year (how much interest they take in the issue). In this analysis, the political party affiliation of the majority of justices provided about one-fourth of the predictive power.
* {{cite journal

| last1=Pinello
| first1=Daniel R.
| title=Linking Party to Judicial Ideology in American Courts: A Meta-Analysis
| journal=The Justice System Journal
| year=1999
| volume=20
| issue=3
| pages=219–254
| url= http://www.danpinello.com/Meta-Analysis.htm
}} Pinello conducted a meta-analysis of 84 studies of American courts covering 222,789 cases and found that political party affiliation was a dependable indicator of rulings: Democratic judges voted in favor of liberal solutions more often than Republican judges did, especially in federal courts (the U.S. Supreme Court, [[United States courts of appeals|U.S. Courts of Appeal]], and [[United States district court|U.S. District Courts]]).
</ref>

Using statistical analysis of Supreme Court votes, scholars found that an inferred value representing a Justice's ideological preference on a simple conservative–liberal scale is sufficient to predict a large number of that justice's votes.<ref>
Some examples:
* {{cite journal

| first1=Bernard
| last1=Grofman
| first2= Timothy J.
| last2= Brazill
| title=Identifying the median justice on the Supreme Court through multidimensional scaling: Analysis of 'natural courts' 1953–1991
| journal= Public Choice
| year=2002
| volume=112
| pages=55–79
| url=http://www.socsci.uci.edu/~bgrofman/14%20Grofman-Brazill-Median%20Justice.pdf

| doi=10.1023/A:1015601614637
}} Grofman and Brazill performed multidimensional scaling (MDS) using SYSTAT 5.0 of the entire range of cases considered by the Supreme Court, 1953–1991. Analyzing terms with an unchanging membership ("natural courts") and a complete bench of nine members (3,363 cases), they found that a one-dimensional scale provided a satisfactory explanation of votes and that the degree of unidimensionality generally rose over the years. "On average, over the 15 courts, the mean r<sup>2</sup> values are 0.86 for a one dimensional metric MDS solution, and 0.97 for a two dimensional metric MDS solution."
* {{cite web

| last1= Poole
| first1=Keith
| title= The Unidimensional Supreme Court
| date=10 July 2003
| url=http://voteview.com/the_unidimensional_supreme_court.htm
| quote=The bottom line is that the current Court is basically unidimensional.
| accessdate=27 November 2012}} Poole used various statistical measures to show that a unidimensional scale provides a good measure of the Rehnquist Court during the 8-year period 1995–2002.
</ref> Subsequently, using increasingly sophisticated statistical analysis, researchers have found that the policy preferences of many justices shift over time.<ref name=MartingQuinn2007>
{{cite journal
| first1= Andrew D.
| last1=Martin
| first2= Kevin M.
| last2=Quinn
| title=Assessing Preference Change on the US Supreme Court
| journal=The Journal of Law, Economics, & Organization
| year=2007
| volume=23
| issue=2
| pages=365–385
| url=http://169.229.248.216/files/prefchange.pdf
| doi=10.1093/jleo/ewm028
}}</ref><ref
name=Epstein2007>
{{cite journal
| last1=Epstein
| first1=Lee
| first2=Andrew D.
| last2=Martin
| first3=Kevin M.
| last3=Quinn
| first4=Jeffrey A.
| last4=Segal
| title=Ideological Drift among Supreme Court Justices: Who, When, and How Important?
| journal= Northwestern University Law Review
| year=2007
| volume=101
| issue=4
| pages=1483–1503
| url=http://mqscores.wustl.edu/media/PrefChange.pdf
}}</ref><ref
name=Ruger2005>{{cite journal
| title=Justice Harry Blackmun and the Phenomenon of Judicial Preference Change
| first=Theodore W.
| last=Ruger
| journal=Missouri Law Review
| volume=70
| pages=1209
| year=2005
| url= https://law.missouri.edu/lawreview/docs/70-4/Ruger.pdf
}}</ref> The ideological leanings of justices (and the drift over time) can be seen clearly in the research results of two sets of scholars using somewhat different models:

[[Andrew D. Martin]] and Kevin M. Quinn have employed [[Markov chain Monte Carlo]] methods to fit a [[Bayesian statistics|Bayesian]] measurement model of ideal points (policy preferences on a one-dimensional scale) for all the justices based on the votes in every contested Supreme Court case since 1937.<ref
name=MartinQuinn2002>
{{cite journal
| last1=Martin
| first1=Andrew D.
| first2=Kevin M.
| last2=Quinn
| title=Dynamic Ideal Point Estimation via Markov Chain Monte Carlo for the U.S. Supreme Court, 1953–1999
| journal=Political Analysis
| year=2002
| volume=10
| issue=2
| pages=134–153
| url= http://adm.wustl.edu/media/pdfs/pa02.pdf
| doi=10.1093/pan/10.2.134}}</ref><ref name=MartinQuinn2001>
{{cite web
| last1=Martin
| first1=Andrew D.
| first2=Kevin M.
| last2=Quinn
| title=The Dimensions of Supreme Court Decision Making: Again Revisiting ''The Judicial Mind''
| date=2 May 2001
| url=http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=EE358C86092701340D6D30D3F7D129DA?doi=10.1.1.24.1490&rep=rep1&type=pdf
}}
</ref><ref
name=MartinEtAl2005>
{{cite journal
| first1=Andrew D.
| last1=Martin
| first2=Kevin M.
| last2= Quinn
| first3=Lee
| last3=Epstein
| title=The Median Justice on the United States Supreme Court
| journal=North Carolina Law Review
| volume=83
| pages=1275–1322
| year=2005
| url=http://scholarship.law.berkeley.edu/cgi/viewcontent.cgi?article=2604&context=facpubs
}}</ref><ref
name=Jackman2011>Jackman provides a simple description of this kind of statistical analysis:
{{cite journal
| doi=10.4135/9781412959636.n585
| title=Statistical Inference, Classical and Bayesian
| journal=International Encyclopedia of Political Science
| year=2011
| last1=Jackman
| first1=Simon
| isbn=9781412959636
| url=http://jackman.stanford.edu/papers/download.php?i=6}}</ref> The graph below shows the results of their analysis: the ideological leaning of each justice from the term beginning in October 1937 to the term that began in October 2015.<ref
name=MartinQuinnDataset>
{{cite web
| title= Martin–Quinn dataset
| url=http://mqscores.wustl.edu/measures.php
}}</ref><ref
name=Silver2012>
{{cite news
| last=Silver
| first=Nate
| title=Supreme Court May Be Most Conservative in Modern History
| url=http://fivethirtyeight.blogs.nytimes.com/2012/03/29/supreme-court-may-be-most-conservative-in-modern-history/
| newspaper=New York Times
| date=29 March 2012}}
In his FiveThirtyEight blog, Silver graphed the Martin–Quinn data to show the movement of the median, most conservative, and most liberal justices over time.</ref> Note that the scale and zero point are arbitrary—only the relative distance of the lines is important. Each unique color represents a particular Supreme Court seat, which makes the transitions from retiring justices to newly appointed justices easier to follow. The black lines represent the leanings of the Chief Justices. The yellow line represents the estimated location of the median justice—who, as Duncan Black’s [[median voter theorem]] posits, is often the [[swing vote]] in closely divided decisions.<ref
name=Duncan1948>
{{cite journal
| last=Black
| first=Duncan
|date=February 1948
| title=On the rationale of group decision-making
| journal=Journal of Political Economy
| volume=56
| issue=1
| pages=23–34
| doi=10.1086/256633
| jstor=1825026}}
</ref>

[[File:Graph of Martin-Quinn Scores of Supreme Court Justices 1937-Now.png|800px|Graph of Martin–Quinn Scores of U.S. Supreme Court Justices from 1937 to 2015]]

Michael A. Bailey used a slightly different [[Markov chain Monte Carlo]] [[Bayesian statistics|Bayesian]] method to determine ideological leanings and made significantly different scaling assumptions.<ref
name=Bailey2012>
{{cite web
| last=Bailey
| first=Michael A.
| title=Measuring Court Preferences, 1950–2011: Agendas, Polarity and Heterogeneity
| date=August 2012
| url=http://www9.georgetown.edu/faculty/baileyma/CourtPref_July2012.pdf
| accessdate=20 November 2012}}</ref><ref
name=BaileyMaltzman2011>{{cite book
| title=The constrained court: law, politics, and the decisions justices make
| first1=Michael A.
| last1=Bailey
| first2=Forrest
| last2=Maltzman
| publisher=Princeton University Press
| location=Princeton
| year=2011
| isbn=978-0691151052
}}</ref><ref
name=Bailey2004>
{{cite journal
| doi=10.1017/S0003055404001194
| title=The Statistical Analysis of Roll Call Data
|date=May 2004
| last1=Clinton
| first1=Joshua
| last2=Jackman
| first2=Simon
| last3=Rivers
| first3=Douglas
| journal=American Political Science Review
| volume=98
| issue=2
| url= http://www.cs.princeton.edu/courses/archive/fall09/cos597A/papers/ClintonJackmanRivers2004.pdf
}}</ref> He analyzed cases by calendar year and supplemented the data regarding votes in each Court case with additional information from the majority, concurring, and dissenting opinions in which justices commented on previous cases, as well as with votes made by members of Congress on similar legislation, amicus filings by [[United States Solicitor General|Solicitors General]] and members of Congress, and presidential and Congressional positions on Court cases. This additional information gave him a richer dataset and also enabled him to deduce preference values that are more consistent with the [[NOMINATE (scaling method)|DW-Nominate]] Common Space scores used to evaluate the ideological leanings of members of Congress and Presidents.<ref
name="EpsteinMartinSegalWesterland2007">
{{cite journal
| last1=Epstein
| first1=Lee
| last2=Martin
| first2=Andrew D.
| last3=Segal
| first3=Jeffrey A.
| last4=Westerland
| first4=Chad
| title=The Judicial Common Space
| journal=Journal of Law, Economics, and organization
| volume=23
| issue=2
|date=May 2007
| pages=303–325
| quote=The [NOMINATE] Common Spaces scores are bounded below by −1 and above by 1, whereas the Martin–Quinn scores are theoretically unbounded (currently, they range from about –6 [Justice Douglas] to 4 [Justice Thomas]).
| url=http://epstein.law.northwestern.edu/research/JCS.pdf
| doi=10.1093/jleo/ewm024
}}</ref> However, he only used votes and cases related to the major topics addressed by the courts in the postwar area: crime, civil rights, free speech, religion, abortion, and privacy. He did not include federalism or economic issues.<ref
name=Bailey2012 /><ref
name=Bailey2007>
{{cite journal
| last=Bailey
| first=Michael A.
| title=Comparable Preference Estimates across Time and Institutions for the Court, Congress, and Presidency
| journal=American Journal of Political Science
| volume=51
| issue=3
| date=July 2007
| pages=433–448
| url=http://www9.georgetown.edu/faculty/baileyma/ajps_offprint_bailey.pdf
| doi=10.1111/j.1540-5907.2007.00260.x}}</ref>

The graph below shows the ideological leaning of each justice by calendar year from 1950 to 2011.<ref
name=BaileyDataset>
{{cite web
| title=Bailey dataset
| url=http://www9.georgetown.edu/faculty/baileyma/Data/Data_Measuring1950to2011_June2012.htm
| accessdate=22 November 2012
}}</ref> The scale and zero point roughly correspond to [[NOMINATE (scaling method)|DW-Nominate]] Common Space scores, but otherwise are arbitrary. As in the graph above, each unique color represents a particular Supreme Court seat. The black lines represent the leanings of the Chief Justices. The yellow line represents the median justice.

[[File:Graph of Bailey Scores of Supreme Court Justices 1950-2011.png|800px|Graph of Bailey Scores of Supreme Court Justices 1950–2011]]

These two graphs differ because of the choices of data sources, data coverage, coding of complicated cases, smoothing parameters, and statistical methods. Each of the lines in these graphs also has a wide band of uncertainty. Because these analyses are based on statistics and probability, it is important not to over-interpret the results.<ref name=Ho2010>
{{cite journal
| last1=Ho
| first1=Daniel E.
| first2=Kevin M.
| last2=Quinn
| title=How Not to Lie with Judicial Votes: Misconceptions, Measurement, and Models
|date=June 2010
| journal=California Law Review
| volume=98
| issue=3
| pages=813–876
| url=http://www.californialawreview.org/assets/pdfs/98-3/HoQuinn.Mac.pdf
}}</ref><ref
name=NOMINATE-IDEAL2009>
{{cite journal
| doi=10.3162/036298009789869727
| title=Comparing NOMINATE and IDEAL: Points of Difference and Monte Carlo Tests
|date=November 2009
| last1=Carroll
| first1=Royce
| last2=Lewis
| first2=Jeffrey B.
| last3=Lo
| first3=James
| last4=Poole
| first4=Keith T.
| last5=Rosenthal
| first5=Howard
| journal=Legislative Studies Quarterly
| volume=34
| issue=4
| pages=555–591
| url=http://voteview.com/nominatevideal.pdf
}}</ref> Also, the nature of the cases the Supreme Court chooses to hear may lead the justices to appear more liberal or conservative than they would if they were hearing a different set of cases. And all cases are valued equally even though, clearly, some cases are much more important than others.<ref name=Farnsworth2007>
{{cite journal
| last1=Farnsworth
| first1=Ward
| title=The Use and Limits of Martin–Quinn Scores to Assess Supreme Court Justices, with Special Attention to the Problem of Ideological Drift
| date=September 2007
| journal=Northwestern University Law Review
| volume=101
| issue=4
| pages=1891–1903
| ssrn=1000986
}}</ref><ref name=McGuire2009>
{{cite journal
| last1= McGuire
| first1=Kevin T.
| last2=Vanberg
| first2=Georg
| last3=Smith, Jr.
| first3=Charles E.
| last4=Caldeira
| first4=Gregory A.
| title= Measuring Policy Content on the U.S. Supreme Court
| date= October 2009
| journal=The Journal of Politics
| volume=71
| issue=4
| pages=1305–1321
| doi=10.1017/s0022381609990107
| url=http://mcguire.web.unc.edu/files/2014/01/policy_content.pdf
}}</ref> Still, they offer an indication of the overall ideological orientation of the justices and provide a visualization of changes in the Court's orientation over time.

== Ideological shifts since 1937 ==
In the early 1930s (earlier than the data on the Martin–Quinn graph), the [[Four Horsemen (Supreme Court)|"Four Horsemen"]] (Justices [[James Clark McReynolds|James McReynolds]], [[Pierce Butler (justice)|Pierce Butler]], [[George Sutherland]], and [[Willis Van Devanter]]) mostly opposed the New Deal agenda proposed by President [[Franklin D. Roosevelt]]. The liberal [[The Three Musketeers (Supreme Court)|"Three Musketeers"]] (Justices [[Harlan F. Stone|Harlan Stone]], [[Benjamin N. Cardozo|Benjamin Cardozo]], and [[Louis Brandeis]]) generally supported the New Deal. Two justices (Chief Justice [[Charles Evans Hughes]] and Justice [[Owen Roberts]]) normally cast the swing votes.

As the Martin–Quinn graph shows, by the 1939 term, Roosevelt had moved the Court to a more liberal position by appointing four new justices including strong liberals [[Hugo Black]], [[William O. Douglas]], and [[Frank Murphy]]. However, led by the increasingly conservative Chief Justices [[Harlan F. Stone|Harlan Stone]] and [[Fred M. Vinson|Fred Vinson]], the Court shifted in a more conservative direction through the early 1950s.

President [[Dwight D. Eisenhower|Dwight Eisenhower]] appointed [[Earl Warren]] to be Chief Justice in 1953, and both graphs indicate that the Court then turned in a more liberal direction as Warren grew substantially more liberal and especially when he was joined by strong liberal justices [[William J. Brennan, Jr.|William Brennan]], [[Arthur Goldberg]], [[Abe Fortas]], and [[Thurgood Marshall]] (though Justices Black and [[Felix Frankfurter]] became more conservative over time).

In the 1970s, the Court shifted in a more conservative direction when President [[Richard Nixon]] appointed Chief Justice [[Warren E. Burger|Warren Burger]] and strong conservative Justices [[Lewis F. Powell, Jr.|Lewis Powell]], [[William Rehnquist]], and [[Harry Blackmun]], and more so when President [[Ronald Reagan]] elevated Rehnquist to Chief Justice (though Blackmun became more liberal over time). The Court shifted to an even more conservative orientation when it was joined by strong conservative Justices [[Antonin Scalia]], [[Clarence Thomas]], [[Samuel Alito]], and Chief Justice [[John G. Roberts|John Roberts]] (appointed by President [[George W. Bush]]), though Justice [[David Souter]] became more liberal over time.<ref

name=Liptak2010>
{{cite news
| last=Liptak
| first=Adam
| title=Court Under Roberts Is Most Conservative in Decades
| url=https://www.nytimes.com/2010/07/25/us/25roberts.html?pagewanted=all
| newspaper=New York Times
| date=24 July 2010}}
</ref>

Both graphs indicate that the current Roberts Court was quite conservative until the death of Scalia, with four very strong conservative justices (including Chief Justice Roberts) and the median position held by Justice [[Anthony Kennedy]] (appointed by President [[Ronald Reagan]]).<ref

name=Silver2012 /><ref name=EpsteinLandesPosner2013>

{{cite journal
| title=How Business Fares in the Supreme Court
| first1=Lee
| last1=Epstein
| first2=William M.
| last2=Landes
| first3=Richard A.
| last3= Posner
| journal= Minnesota Law Review
| volume=97
| year=2013
| pages=1431–1472
| quote=We find that five of the ten Justices who, over the span of our study (the 1946 through 2011 Terms), have been the most favorable to business are currently serving, with two of them ranking at the very top among the thirty-six Justices in our study.
| url=http://www.minnesotalawreview.org/wp-content/uploads/2013/04/EpsteinLanderPosner_MLR.pdf
}}</ref>

The most volatile seat appears to be [[List of Justices of the Supreme Court of the United States by seat|Seat 10]] (light blue lines) which was held by conservative [[Pierce Butler (justice)|Pierce Butler]] until 1939, then liberal [[Frank Murphy]] until 1949, then moderate-conservative [[Tom C. Clark|Tom Clark]] until 1967, then strong liberal [[Thurgood Marshall]] until 1991, and then strong conservative [[Clarence Thomas]]. The path of Justice [[Harry Blackmun]] illustrates the ideological drift shown by many justices.<ref name=Ruger2005 /> Blackmun (purple line) had a conservative score (Quinn–Martin = 1.767; Bailey = 0.43) in the 1970 term, his first on the bench, but had shifted to a liberal score (Quinn–Martin = −1.943; Bailey = −0.81) by the 1993 term, his last. The median justice was [[Byron White]] for most of the time from 1970 to 1993, [[Sandra Day O’Connor]] from 1994 to 2005, and [[Anthony Kennedy]] since 2006.

== Career "liberal" voting percentage by issue area from 1946–2012 ==
The following sortable table{{efn|To sort, click on the arrow in the header. To sort by multiple columns, click on the first column's sort arrow, then shift-click on subsequent columns' sort arrows.}} lists the lifetime percentage "liberal" scores of Supreme Court justices as compiled in the Supreme Court Database.<ref>{{cite web
|url=http://epstein.wustl.edu/research/justicesdata.html
|title=The U.S. Supreme Court Justices Database
|last1=Epstein
|first1=Lee
|last2=Walker
|first2=Thomas G.
|last3=Staudt
|first3=Nancy
|last4=Hendrickson
|first4=Scott
|last5=Roberts
|first5=Jason
|date=March 2, 2013
|accessdate=February 14, 2016}}</ref> The table shows data for justices whose service began at or after the 1946 term; the data ends with the 2011–2012 term.

The term "liberal" in the Supreme Court Database represents the voting direction of the justices across the various issue areas. It is most appropriate in the areas of criminal procedure, civil rights, and First Amendment cases, where it signifies pro-defendant votes in criminal procedure cases, pro-women or -minorities in civil rights cases, and pro-individual against the government in First Amendment cases. In takings clause cases, however, a pro-government/anti-owner vote is considered liberal. The use of the term is probably less appropriate in union cases, where it represents pro-union votes against both individuals and the government, and in economic cases, where it represents pro-government votes against challenges to federal regulatory authority and pro-competition, anti-business, pro-liability, pro-injured person, and pro-bankruptcy decisions. In federalism and federal taxation cases, the term indicates pro-national government positions.

* Justice Number = Order that Supreme Court Justice was appointed
* Justice = Justice Name
* Year Nom = Year Nominated to the Supreme Court
* Role = Chief Justice or Associate Justice
* '''Criminal Procedure''' = A higher number means pro-defendant votes in cases involving the rights of persons accused of crime, except for the due process rights of prisoners.
* '''Civil Rights''' = A higher number means more votes permitting intervention on First Amendment freedom cases which pertain to classifications based on race (including Native Americans), age, indigence, voting, residence, military, or handicapped status, sex, or alienage.
* '''First Amendment''' = A higher number reflects votes that advocate individual freedoms with regards to speech.
* '''Union''' = A higher number means pro-union votes in cases involving labor activity.
* '''Economic''' = A higher number means more votes against commercial business activity, plus litigation involving injured persons or things, employee actions concerning employers, zoning regulations, and governmental regulation of corruption other than that involving campaign spending.
* '''Federalism''' = A higher number means votes for a larger, more empowered government in conflicts between the federal and state governments, excluding those between state and federal courts, and those involving the priority of federal fiscal claims.
* '''Federal Taxes''' = A higher number means more votes widening the government's ability to define and enforce tax concepts and policies in cases involving the Internal Revenue Code and related statues.

A highlighted row indicates that the Justice is currently serving on the Court.

{| class="wikitable sortable" id="justicespercentliberal"
|- bgcolor="#CCCCCC" align="center"
! Num
! Justice 
! Year Nom
! Role
! Criminal Procedure
! Civil Rights
! First Amendment
! Union
! Economic
! Federalism
! Federal Taxes
<!-- ================== Beginning of Table Rows ================== -->
|-
| align="center" | 85 || {{sortname|Fred M.|Vinson}} || align="center" | 1946 || Chief Justice || align="center" | 29.6% || align="center" | 45.2% || align="center" | 25.6% || align="center" | 34.2% || align="center" | 53.2% || align="center" | 46.7% || align="center" | 73.5%
|-
| align="center" | 86 || {{sortname|Tom C.|Clark}} || align="center" | 1949 || Associate || align="center" | 34.9% || align="center" | 56.0% || align="center" | 36.6% || align="center" | 64.3% || align="center" | 71.0% || align="center" | 55.8% || align="center" | 73.6%
|-
| align="center" | 87 || {{sortname|Sherman|Minton}} || align="center" | 1949 || Associate || align="center" | 28.4% || align="center" | 39.7% || align="center" | 28.2% || align="center" | 58.3% || align="center" | 59.2% || align="center" | 56.7% || align="center" | 71.1%
|-
| align="center" | 88 || {{sortname|Earl|Warren}} || align="center" | 1953 || Chief Justice || align="center" | 74.6% || align="center" | 82.8% || align="center" | 82.4% || align="center" | 72.0% || align="center" | 81.6% || align="center" | 74.7% || align="center" | 78.8%
|-
| align="center" | 89 || {{sortname|John Marshall|Harlan II}} || align="center" | 1955 || Associate || align="center" | 39.0% || align="center" | 45.3% || align="center" | 44.4% || align="center" | 55.0% || align="center" | 38.4% || align="center" | 55.8% || align="center" | 69.7%
|-
| align="center" | 90 || {{sortname|William J.|Brennan, Jr.}} || align="center" | 1956 || Associate || align="center" | 76.1% || align="center" | 83.1% || align="center" | 84.5% || align="center" | 66.1% || align="center" | 71.7% || align="center" | 67.2% || align="center" | 70.2%
|-
| align="center" | 91 || {{sortname|Charles Evans|Whittaker}} || align="center" | 1957 || Associate || align="center" | 43.5% || align="center" | 47.3% || align="center" | 37.0% || align="center" | 42.5% || align="center" | 33.8% || align="center" | 56.5% || align="center" | 60.0%
|-
| align="center" | 92 || {{sortname|Potter|Stewart}} || align="center" | 1958 || Associate || align="center" | 45.9% || align="center" | 50.4% || align="center" | 64.0% || align="center" | 58.2% || align="center" | 45.3% || align="center" | 59.8% || align="center" | 66.1%
|-
| align="center" | 93 || {{sortname|Byron|White}} || align="center" | 1962 || Associate || align="center" | 33.2% || align="center" | 56.2% || align="center" | 39.4% || align="center" | 62.4% || align="center" | 58.3% || align="center" | 67.0% || align="center" | 84.8%
|-
| align="center" | 94 || {{sortname|Arthur|Goldberg}} || align="center" | 1962 || Associate || align="center" | 77.6% || align="center" | 98.3% || align="center" | 93.1% || align="center" | 65.0% || align="center" | 66.7% || align="center" | 61.1% || align="center" | 78.3%
|-
| align="center" | 95 || {{sortname|Abe|Fortas}} || align="center" | 1965 || Associate || align="center" | 78.9% || align="center" | 83.6% || align="center" | 79.1% || align="center" | 60.0% || align="center" | 70.7% || align="center" | 64.3% || align="center" | 43.8%
|-
| align="center" | 96 || {{sortname|Thurgood|Marshall}} || align="center" | 1967 || Associate || align="center" | 80.2% || align="center" | 84.8% || align="center" | 83.7% || align="center" | 67.9% || align="center" | 65.0% || align="center" | 68.3% || align="center" | 74.2%
|-
| align="center" | 97 || {{sortname|Warren E.|Burger}} || align="center" | 1969 || Chief Justice || align="center" | 19.8% || align="center" | 38.3% || align="center" | 30.7% || align="center" | 42.9% || align="center" | 42.3% || align="center" | 66.7% || align="center" | 72.1%
|-
| align="center" | 98 || {{sortname|Harry|Blackmun}} || align="center" | 1970 || Associate || align="center" | 42.5% || align="center" | 62.1% || align="center" | 56.5% || align="center" | 60.9% || align="center" | 54.8% || align="center" | 67.4% || align="center" | 74.4%
|-
| align="center" | 99 || {{sortname|Lewis F.|Powell, Jr.}} || align="center" | 1971 || Associate || align="center" | 29.1% || align="center" | 40.9% || align="center" | 46.0% || align="center" | 50.5% || align="center" | 44.2% || align="center" | 60.5% || align="center" | 56.1%
|-
| align="center" | 100 || {{sortname|William|Rehnquist}} || align="center" | 1971 || Associate || align="center" | 14.5% || align="center" | 25.2% || align="center" | 18.6% || align="center" | 48.3% || align="center" | 40.5% || align="center" | 31.3% || align="center" | 62.5%
|-
| align="center" | 101 || {{sortname|John Paul|Stevens}} || align="center" | 1975 || Associate || align="center" | 66.4% || align="center" | 64.6% || align="center" | 67.2% || align="center" | 63.1% || align="center" | 58.2% || align="center" | 56.5% || align="center" | 59.4%
|-
| align="center" | 102 || {{sortname|Sandra Day|O'Connor}} || align="center" | 1981 || Associate || align="center" | 26.4% || align="center" | 45.5% || align="center" | 41.9% || align="center" | 41.5% || align="center" | 43.2% || align="center" | 48.0% || align="center" | 58.9%
|-
| align="center" | 100 || {{sortname|William|Rehnquist}} || align="center" | 1986 || Chief Justice || align="center" | 19.7% || align="center" | 30.7% || align="center" | 25.9% || align="center" | 31.4% || align="center" | 45.1% || align="center" | 46.6% || align="center" | 77.8%
|-
| align="center" | 103 || {{sortname|Antonin|Scalia}} || align="center" | 1986 || Associate || align="center" | 27.0% || align="center" | 30.7% || align="center" | 29.3% || align="center" | 33.3% || align="center" | 41.7% || align="center" | 51.1% || align="center" | 69.4%
|-bgcolor="ccccff"
| align="center" | 104 || {{sortname|Anthony|Kennedy}} || align="center" | 1987 || Associate || align="center" | 32.2% || align="center" | 42.4% || align="center" | 44.1% || align="center" | 39.2% || align="center" | 43.6% || align="center" | 53.4% || align="center" | 79.6%
|-
| align="center" | 105 || {{sortname|David|Souter}} || align="center" | 1990 || Associate || align="center" | 56.8% || align="center" | 69.5% || align="center" | 70.4% || align="center" | 57.6% || align="center" | 52.7% || align="center" | 64.4% || align="center" | 70.7%
|-bgcolor="ccccff"
| align="center" | 106 || {{sortname|Clarence|Thomas}} || align="center" | 1991 || Associate || align="center" | 21.2% || align="center" | 23.6% || align="center" | 29.5% || align="center" | 32.3% || align="center" | 40.0% || align="center" | 44.6% || align="center" | 57.1%
|-bgcolor="ccccff"
| align="center" | 107 || {{sortname|Ruth Bader|Ginsburg}} || align="center" | 1993 || Associate || align="center" | 61.9% || align="center" | 69.8% || align="center" | 69.2% || align="center" | 75.0% || align="center" | 56.3% || align="center" | 59.3% || align="center" | 78.1%
|-bgcolor="ccccff"
| align="center" | 108 || {{sortname|Stephen|Breyer}} || align="center" | 1994 || Associate || align="center" | 56.2% || align="center" | 69.1% || align="center" | 50.6% || align="center" | 81.5% || align="center" | 51.8% || align="center" | 65.1% || align="center" | 76.7%
|-bgcolor="ccccff"
| align="center" | 109 || {{sortname|John|Roberts}} || align="center" | 2005 || Chief Justice || align="center" | 28.1% || align="center" | 40.0% || align="center" | 33.3% || align="center" | 50.0% || align="center" | 38.2% || align="center" | 69.0% || align="center" | 85.7%
|-bgcolor="ccccff"
| align="center" | 110 || {{sortname|Samuel|Alito}} || align="center" | 2005 || Associate || align="center" | 19.9% || align="center" | 37.8% || align="center" | 21.7% || align="center" | 44.4% || align="center" | 37.4% || align="center" | 61.5% || align="center" | 85.7%
|-bgcolor="ccccff"
| align="center" | 111 || {{sortname|Sonia|Sotomayor}} || align="center" | 2009 || Associate || align="center" | 62.5% || align="center" | 63.9% || align="center" | 76.9% || align="center" | 66.7% || align="center" | 51.2% || align="center" | 53.3% || align="center" | 100.0%
|-bgcolor="ccccff"
| align="center" | 112 || {{sortname|Elena|Kagan}} || align="center" | 2010 || Associate || align="center" | 71.4% || align="center" | 61.9% || align="center" | 62.5% || 1align="center" | 100.0% || align="center" | 48.0% || align="center" | 71.4% || align="center" | 50.0%
|}
{{notelist}}

== See also ==
* [[Supreme Court of the United States]]
* [[Segal–Cover score]]
* [[Judicial discretion]]
* [[Judicial activism]]
* [[Judicial restraint]]

== Notes ==
{{Reflist|30em}}

{{SCOTUS horizontal}}

[[Category:Supreme Court of the United States]]