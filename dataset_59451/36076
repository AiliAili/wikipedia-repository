{{other people|Nadia Ali}}
{{Use dmy dates|date=October 2012}}
{{good article}}
{{Infobox musical artist <!-- See Wikipedia:WikiProject_Musicians -->
| name                = Nadia Ali
| background          = solo_singer
| image               = Nadia Ali 2010.jpg
| image_size          = 200px
| caption             = Nadia Ali in 2009
| birth_name          = Nadia Ali
| alias               =
| birth_date          = {{Birth date and age|df=yes|1980|8|3}}
| birth_place         = [[Tripoli]], [[Libya]]
| origin              = [[Queens]], [[New York (state)|New York]], [[United States|U.S.]]
| death_date          =
| genre               = {{hlist|[[Electronic dance music|EDM]]|[[Trance music|trance]]|[[House music|house]]}}
| occupation          = {{hlist|Singer|songwriter}}
| instrument          = Vocals
| years_active        = 2001–present
| label               = {{hlist|Smile in Bed|[[Armada Music|Armada]]|[[Strictly Rhythm]]}}
| associated_acts     = {{hlist|[[iiO (dance group)|iiO]]|[[Armin van Buuren]]|[[Morgan Page]]|[[Sultan & Shepard|Sultan & Ned Shepard]]|Starkillers}}
| website             = {{url|nadiaali.com}}
| current_members     =
| past_members        =
| notable_instruments =
}}

'''Nadia Ali''' ({{lang-ur|{{Nastaliq| نادیہ علی}}}}, born 3 August 1980) is a Pakistani American singer and songwriter. Ali gained prominence in 2001 as the [[frontwoman]] and songwriter of the band [[iiO (dance group)|iiO]] after their first debut single "[[Rapture (iiO song)|Rapture]]" reached No. 2 on the [[UK Singles Chart]].<ref name=OCC/> The song also charted across several countries in Europe.<ref name=iiO/> Their 2006 single, "[[Is It Love? (iiO song)|Is It Love?]]", reached the top of the  ''[[Billboard (magazine)|Billboard]]'' [[Hot Dance Club Songs|Hot Dance Club Play Chart]].<ref name=BB/>

After embarking on a solo career in 2005, Ali became a vocalist in [[electronic dance music]]. She released her debut album ''[[Embers (album)|Embers]]'' in 2009. Three singles from the album reached the top-ten of the ''Billboard'' Hot Dance Club Play Chart, including the No. 1 hit, "[[Love Story (Nadia Ali song)|Love Story]]".<ref name=BB1/><ref name=BB2/><ref name=BB3/>

In 2010, Ali released a remix album series titled "Queen of Clubs Trilogy" to mark her decade-long career as a singer. "Rapture" was re-released as the only single from the trilogy and the song was once again a chart success in Europe.<ref name=Romania/> Ali released the single "[[Pressure (Nadia Ali song)|Pressure]]" with Starkillers and Alex Kenji in 2011, which became a club and festival anthem and received an International Dance Music Award.<ref name=IDMA2/> In 2012, she collaborated with [[BT (musician)|BT]] and [[Arty (musician)|Arty]] on the single "Must Be the Love".

==Life and career==

===1980–2005: Early life and iiO===
Nadia Ali was born in [[Tripoli]], [[Libya]] to [[Pakistani people|Pakistani]] parents in 1980. The family relocated when she was five years old and she was subsequently raised in  [[Queens, New York]].<ref name=VOA/>

Ali started working in the [[New York City|New York]] offices of [[Gianni Versace S.p.A.|Versace]] when she was 17. A colleague from Versace introduced her to [[Record producer|producer]] Markus Moser, who was looking for a female-singer (or "chanteuse") to collaborate on some of his original productions for a [[girl group]] in Germany.<ref name=BT/> The two teamed up with Moser working on production, while Ali wrote the lyrics and vocals for the songs.<ref name= djf/> Her first song was the single "[[Rapture (iiO song)|Rapture]]", which she wrote in 30 minutes based on an encounter with an Australian nightclub patron.<ref name = HDC/>  A demo of the song was first played at the New York club [[Twilo]] in 2001 and received early support from influential DJ [[Pete Tong]] who played the demo on his show on [[BBC Radio 1]]. The song eventually became an Ibiza favourite after support from prominent D.J.s such as [[Sasha (DJ)|Sasha]], [[Danny Tenaglia]] and [[Sander Kleinenberg]] during the summer season.<ref name=MM/> Released in late 2001 by [[Ministry of Sound]], the single became a commercial success peaking at No. 2 on UK Singles Chart and ''Billboard's'' Hot Dance Club Play Chart, while charting in several countries in Europe.<ref name=OCC/><ref name=iiO/><ref name=BB/> The success of "Rapture", Ali said, caused the formation of iiO as the music they were initially working on was quite different from dance music and were asked to come up with a project name to promote the single.<ref name = PRO/> They originally named themselves Vaiio after the [[Sony]] [[VAIO]] [[laptop]] Ali used to write the lyrics on.<ref name=iiO/> The duo toured internationally and released several more singles, including "[[At the End]]", "[[Runaway (iiO song)|Runaway]]", "[[Smooth (iiO song)|Smooth]]", and "[[Kiss You (iiO song)|Kiss You]]". Their first studio album, ''[[Poetica (iiO album)|Poetica]]'' followed in 2005.<ref name=BB1/>

Ali left the group in 2005 to pursue a solo career, while Moser continued to release iiO material featuring her on vocals. Most notably, these releases include the 2006 single "[[Is It Love? (iiO song)|Is It Love?]]", which reached No. 1 in America on the ''Billboard'' Hot Dance Club Play Chart,<ref name=BB/> the 2007 remix album ''[[Reconstruction Time: The Best Of iiO Remixed]]''<ref name=BB1/> and the 2011 studio album ''Exit 110''.<ref name=BB1/>

===2006–09: ''Embers''===
[[File:Nadia Ali.jpg|thumb|Nadia Ali performing at Avalon, Boston in 2006]]
Ali started working on her debut solo album soon after leaving iiO, a process which took her four years.  Her first solo release was the 2006 single, "Who is Watching?", a collaboration with Dutch DJ [[Armin van Buuren]], which appeared on his album ''[[Shivers (album)|Shivers]]''.<ref name=SH/> This was followed by "Something to Lose" in 2006, a duet with singer [[Rosko]], produced by [[John Creamer & Stephane K]] and released by Ultra Records. The track was licensed to [[Roger Sanchez|Roger Sanchez's]] ''Release Yourself, Vol. 5'', as well as [[Sharam Tayebi]] of [[Deep Dish (band)|Deep Dish]] for his [[Global Underground]] debut ''[[GU029|Dubai]]''.<ref name=RY/><ref name=GU/>

In June 2008, she released "[[Crash and Burn (Nadia Ali song)|Crash and Burn]]", the first single from her solo album. The single became a club success peaking at No. 6 on ''Billboard's'' [[Hot Dance Club Songs|Hot Dance Club Play Chart]].<ref name=BB2/> She released the second single, "[[Love Story (Nadia Ali song)|Love Story]]" from the as-yet untitled album in February 2009. It topped ''Billboard'' Hot Dance Club Play Chart in April 2009 and was nominated for the Best Progressive/House Track at the 2010 International Dance Music Awards at the [[Winter Music Conference]].<ref name=BB3/><ref name=IDMA/> Ali was featured on MTV Iggy in March 2009, where she recorded three live acoustic videos, performing "Rapture", "Crash and Burn" and "Love Story".<ref name=IGGY/>

The third single "[[Fine Print (Nadia Ali song)|Fine Print]]" was released in July 2009. Ali announced that the single preceded the release of her debut solo album ''[[Embers (album)|Embers]]''.<ref name=FP/> The single peaked at No. 4 on ''Billboard's'' Hot Dance Club Play Chart.<ref name=BB4/> ''Embers'' was released in September 2009. Co-produced by [[Sultan & Ned Shepard]], Alex Sayz and Scott Fritz, Ali self-released the album on her own label, Smile in Bed Records.<ref name=muumuu/> ''Embers'' generally received positive reviews, Chase Gran from [[About.com]] called it a "well rounded, gourmet album with impressive songs".<ref name=Ab/> Gail Navarro from Racket magazine complimented Ali on her songwriting saying, "It wasn’t just her sultry sound mixed in together with that enchanting singing voice; her songwriting got me hook, line and sinker".<ref name=RM/> Speaking about the self-release of the album, she has cited her creative independence and the pressure of deadlines as the main reasons why she created her own record label.<ref name=same/><ref name=BM/>

Ali released two collaborations in 2009, the first "Better Run" with [[Tocadisco]] was released on his album ''TOCA 128.0 FM'' and "12 Wives In Tehran" with Serge Devant was released on his album ''Wanderer''.<ref name=TD/><ref name=SD/>

===2010–11: ''Queen of Clubs Trilogy''===
[[File:Nadia Ali Queen of Clubs.jpeg|left|thumb|150px| Promotional material for ''Queen of Clubs Trilogy'' featured a [[playing card]] with Nadia Ali as the "[[Queen (playing card)|Queen of Clubs]]" ]]
Ali's first release in 2010 was the track "Try", a collaboration with German producer [[Schiller (band)|Schiller]], chosen as the lead single from his album ''[[Atemlos (Schiller album)|Atemlos]]'', the music video premiered on [[YouTube]] in February 2010.<ref name=Try/> In April 2010, Ali released "[[Fantasy (Nadia Ali song)|Fantasy]]", the fourth single from ''Embers''. The track was chosen as a single by her fans after a poll conducted by Ali on her [[Facebook]] page.<ref name=muumuu/> The music video for "Fantasy" was set to the [[Morgan Page]] remix, which served as a prologue to Ali's next project; ''Queen of Clubs Trilogy: The Best of Nadia Ali Remixed''. The package consisted of three releases: ''[[Queen of Clubs Trilogy: Ruby Edition|Ruby Edition]]'' (August 2010), ''[[Queen of Clubs Trilogy: Onyx Edition|Onyx Edition]]'' (October 2010) and ''[[Queen of Clubs Trilogy: Diamond Edition|Diamond Edition]]'' (December 2010). It featured collaborations with, and remixes by [[Armin van Buuren]], [[Avicii]] and [[Gareth Emery]] amongst several other prominent DJs and producers.<ref name=QOC/>

<blockquote class="toccolours" style="text-align:left; width:30%; float:right; padding: 8px 13px 8px 13px; display:table;">I think the fact that it (electronic dance music) is mostly male dominated makes females stand out that much more if they are driven enough.  I believe anything is possible with hard work.<p style="text-align: right;">– Nadia Ali<ref name=HO/>
</blockquote>

With a decade-long career, [[MTV]] described Ali as one of the "enduring empresses" of electronic dance music and ''Queen of Clubs Trilogy'' as "aptly titled".<ref name=MTVG/> Noted for being the "definitive" and "unmistakable" voice of dance music, she is said to have "enriched" and "invigorated" the genre.<ref name=Mobo/><ref name=CMNB/> Ali has gone on to become an oft-requested collaborator by DJs and producers.<ref name=muumuu/><ref name=RDW/> She was praised for acquiring notability in a male and DJ-dominated genre where vocalists serve as supporting acts.<ref name=HO/><ref name=TOI/> She said this was a double-edged sword as she was also treated as competition by DJs.<ref name=RDW/>  In December 2010, she received her first [[Grammy]] nomination when the Morgan Page remix of "Fantasy" was nominated in the [[Best Remixed Recording, Non-Classical]] category.<ref name=grammy/>

Her first track with iiO, "[[Rapture (iiO song)#Nadia Ali re-release|Rapture]]" was re-released as a single from ''Queen of Clubs Trilogy'' with remixes by Tristan Garner, Gareth Emery and Avicii. A new music video for the track was shot based on the "Queen of Clubs" theme and released on 24 January 2011.<ref name=RAM/> The song peaked at No. 3 on the [[Romanian Top 100]] chart, while charting in other European countries.<ref name=Romania/>

[[File:NadiaAli Armin Only Poland.jpg|thumb|180px|Nadia Ali performing at [[Armin Only]] in Poland]]
Throughout 2010, Ali's collaborations with DJs and producers were released. These included "That Day" with [[Dresden and Johnston]], which was featured on various compilation albums. Follow-up release "The Notice" with Swiss duo Chris Reece was released on 13 July.<ref name=Notice/> Ali was featured on the track "Feels So Good" on Armin van Buuren's fourth album ''[[Mirage (Armin van Buuren album)|Mirage]]''. Released as the fifth single from the album, the song was voted as the Best Trance Track at the 27th International Dance Music Awards.<ref name=IDMA2/><ref name=FSG/>

During 2011, Ali announced the release of collaborations with several DJs and producers. The first of these was "Call My Name" with the duo [[Sultan & Ned Shepard]], released by Harem Records on 9 February. "Call My Name" was a club success, charting at No. 5 on ''Billboard'' Hot Dance Club Play Chart.<ref name=BB5/> The second track "[[Pressure (Nadia Ali song)|Pressure]]", a collaboration with Starkillers and Alex Kenji was released on 15 February by [[Spinnin' Records]].<ref name=PRESS/> The [[Alesso]] remix of "Pressure" became a club and festival anthem and received support from notable DJs such as [[Armin van Buuren]], [[Tiesto]], [[Swedish House Mafia]] and [[Calvin Harris]] and was voted the Best Progressive House Track at the 27th International Dance Music Awards.<ref name=IDMA2/><ref name=EF/>

In April, iiO released the studio album ''Exit 110'', which featured Ali on vocals.   On 23 May, her next collaboration, "Free To Go" with Alex Sayz was released by Zouk Recordings.<ref name=FTG/> She was featured on [[Sander van Doorn]]'s second studio album ''[[Eleve11]]'' on the track "Rolling the Dice", a collaboration between van Doorn, [[Sidney Samson]] and her.<ref name=Sander/> Her next release was the single "Believe It" with the German duo Spencer & Hill, which was released on 3 October by [[Wall Recordings]].<ref name=BI/> She collaborated once again with Starkillers on the single "Keep It Coming", which was released on 26 December by Spinnin' Records.<ref name=KIC/>

===2012–15===
[[File:Nadia Ali 2011.jpg|thumb|left|Nadia Ali performing at Josephine's in [[Washington, D.C.]] in 2011]]
As of February 2010, Ali had begun working on her second studio album.<ref name=muumuu/> A music video for the lead single from the album, "When It Rains", was released on her YouTube channel in August 2011.<ref name=WIR/>

In May 2012, Ali announced her move to Los Angeles citing the need for a change after spending 26 years in New York City.<ref name=Submerge/><ref name=Strictly/>

Her first release in 2012 was "This Is Your Life", the fourth single from Swiss DJ EDX's album ''On the Edge''.<ref name=EDX/> That was followed by "Carry Me", a collaboration with [[Morgan Page]], the fourth single from his third studio album, ''[[In the Air (Morgan Page album)|In the Air]]''.<ref name=MP/> Her next release was "Must Be the Love", the lead single from [[BT (musician)|BT's]] ninth studio album ''[[A Song Across Wires]]'', which was a collaboration between him, [[Arty (musician)|Arty]] and Ali.<ref name=BT2/> In 2012, she also pre-announced her album "Phoenix", which, as of September 2015, had not shipped.<ref name="MTV2"/>

In December 2012, Ali announced her engagement to her fiancé, whom she married in October 2013.<ref name=Sutra/><ref name=hangout/>

In January 2014, Ali released an acoustic cover of [[The Police]] song "[[Roxanne (song)|Roxanne]]" as a free download.<ref name=Roxanne/> In September 2015, Ali released the single "All In My Head", a collaboration with PANG!.<ref name=Pang/> The release was her first single as a lead artist since 2011.

==Musical style and influences==
{{listen
 | pos          = right 
 | filename     = Crash and Burn.ogg
 | title        = "Crash and Burn" (2008)
 | description  = "Crash and Burn" combines Ali's signature music styles: Eastern, electronic and acoustic
 | format       = [[Ogg]]
}}
Ali is perhaps best known for her characteristic voice and vocal abilities.<ref name=RM/><ref name=Mobo/><ref name=LS/> Reema Kumari Jadeja from [[Music of Black Origin Awards|MOBO]] described her work as "masterfully encapsulating euphoric and melancholic, Ali’s signature music style sees Eastern mystique caressed with intelligent [[electronica]] and fortified with [[soul music|soul]]".<ref name=Mobo/> The songs on ''Embers'' were likened to Madonna's work in her prime and a "modern re-interpretation" of Stevie Nicks. ''[[Billboard (magazine)|Billboard]]'' praised her voice for having "too much life on its own".<ref name=RM/><ref name=CMNB/><ref name=LS/> Ali has been influenced by an eclectic mix of artists, which she credits to her Eastern background and upbringing in Queens.<ref name=PRO/><ref name=BM/> She listed [[alternative music|alternative]], [[folk music|folk]], and [[Pakistani music|Pakistani]] music as her biggest influences.<ref name= HDC/><ref name= PRO/> Some of her vocal and songwriting influences, she said, were [[Stevie Nicks]], [[Nusrat Fateh Ali Khan]], [[Madonna (entertainer)|Madonna]], [[Sade (band)|Sade]] and [[Bono]].<ref name=BT/><ref name=HDC/><ref name=same/>
Her debut album was noted for a blend of electronica, acoustic and Middle Eastern melodies.<ref name=Ab/> She has been praised for her songwriting, describing personal experiences with people, which "hit a powerful and striking chord" with the listener.<ref name=RM/>

{{clear}}

==Discography==
{{Main article|Nadia Ali discography|List of Nadia Ali songs}}

;Studio albums
* ''[[Embers (album)|Embers]]'' (2009)

;Compilation albums
* ''[[Queen of Clubs Trilogy: Ruby Edition]]'' (2010)
* ''[[Queen of Clubs Trilogy: Onyx Edition]]'' (2010)
* ''[[Queen of Clubs Trilogy: Diamond Edition]]'' (2010)

==Awards==
{{awards table}}
|-
|rowspan="2" style="text-align:center;"|2012
| rowspan="1" style="text-align:center;"|"Feels So Good"
| Best Trance Track at 27th International Dance Music Awards
|{{won}}
|-
| rowspan="1" style="text-align:center;"|"[[Pressure (Nadia Ali song)|Pressure (Alesso Remix)]]"
| Best Progressive Track at 27th International Dance Music Awards
|{{won}}
{{end}}

==References==
{{reflist|30em|refs=

<ref name=VOA>{{cite video |people= Imran Siddiqui (interviewer) |date= 21 September 2006|title= Nadia Ali interview |url= https://www.youtube.com/watch?v=K6YY0OVYyWs|format= |medium= Television Production |publisher= [[Voice of America]] |location= United States |accessdate=1 June 2011 }}</ref>

<ref name=MM>{{cite web |url= http://www.hitquarters.com/index.php3?page=intrview/opar/intrview_MMoser.html |title= Interview with Markus Moser, one half of dance duo iio (UK Top 5) |date= 15 May 2003 |work= Hit Quarters |accessdate=7 October 2011 |archiveurl=http://www.webcitation.org/62FQAu1Xj|archivedate= 7 October 2011}}</ref>

<ref name=BT>{{cite web |url= http://www.armadamusic.com/news/2010/07/behind-the-voice-nadia-ali |title= Behind the Voice: Nadia Ali |date= 16 July 2010 |work= Armada Music |accessdate=26 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880019|archivedate= 31 May 2011}}</ref>

<ref name= HDC>{{cite interview |last=Ali |first=Nadia |interviewer = Adz |title= The Rapturous Voice of iiO - Nadia Ali |work= Hump Day Crew
|publisher= Joy 94.9 |location= Melbourne, Australia |date= 12 November 2009}}</ref>

<ref name=OCC>{{cite web |url= http://www.theofficialcharts.com/artist/_/iio/ |title= UK Charts > iiO |work= UK Singles Chart |publisher= The Official UK Charts Company |accessdate=7 May 2011 |archiveurl=http://www.webcitation.org/query?id=1306860170879864|archivedate= 31 May 2011}}</ref>

<ref name=BB>{{cite web |url= {{BillboardURLbyName|artist=iio|chart=Dance/Club Play Songs}}
|title= iiO- singles |work= Billboard |publisher= Prometheus Global Media |accessdate=7 May 2011}}</ref>

<ref name=iiO>{{cite web |url={{BillboardURLbyName|artist=iio|bio=true}} |title= iiO Biography & Awards |work= Billboard |publisher= Prometheus Global Media |accessdate=6 June 2011}}</ref>

<ref name=BB1>{{cite web |url={{BillboardURLbyName|artist=iio|chart=all}}
|title= iiO - Albums |work= Billboard |publisher= Prometheus Global Media |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170879881|archivedate= 31 May 2011}}</ref>

<ref name=muumuu>{{cite web |url= http://www.muumuse.com/2010/02/interview-with-nadia-ali.html 
|title= Interview with Nadia Ali |last= Stern |first= Bradley |date= 17 February 2010 |work= muumuse |accessdate=28 April 2011|archiveurl=http://www.webcitation.org/query?id=1306860170879890|archivedate= 31 May 2011}}</ref>

<ref name=same>{{cite web |url= http://www.samesame.com.au/features/4608/Nadia-Ali--A-Love-Story.htm
|title= Nadia Ali - A Love Story |last= Murphy |first= Dan |date= 2 October 2009|work= Same Same |accessdate=1 June 2011|archiveurl=http://www.webcitation.org/5z7Yek0ZI|archivedate= 1 June 2011}}</ref>

<ref name=BM>{{cite web |url = http://beatsmedia.com/interviews/beatsmedia-interviews-nadia-ali
|title= Beatsmedia interviews Nadia Ali |last= |first= |date= 4 July 2010 |work= Beatsmedia |publisher= |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880054|archivedate= 31 May 2011}}</ref>

<ref name=PRO>{{cite web |url = http://blog.promotion-us.com/nadia-ali-interview |title= Nadia Ali interview |date= 12 August 2009 |work= Pro Motion |publisher= The Brad LeBeau Co. Inc |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170879899|archivedate= 31 May 2011}}</ref>

<ref name=BB2>{{cite web |url= http://www.billboard.com/charts/2008-10-18/dance-club-play-songs |title= Dance Club Play Chart |date= 18 October 2008 |work= Billboard |publisher= Prometheus Global Media|accessdate=28 April 2011}}</ref>

<ref name=BB3>{{cite web |url= http://www.billboard.com/charts/2009-04-25/dance-club-play-songs |title= Dance Club Play Chart |date= 25 April 2009|work= Billboard.com |publisher= Rovi Corporation |accessdate=28 April 2011}}</ref>

<ref name=IDMA2>{{cite web |url= http://www.wintermusicconference.com/events/idmas/index.php?wmcyear=2012#idmanominees  |title= 27th International Dance Music Awards - Winter Music Conference 2012 - WMC 2012 |work= International Dance Music Awards |publisher= Winter Music Conference |accessdate=1 February 2012 |archiveurl=http://www.webcitation.org/657CVFjrT|archivedate= 1 February 2012}}</ref>

<ref name=IDMA>{{cite web |url= http://www.wintermusicconference.com/idmaballot/nominees/2010.php  |title= Winter Music Conference 2010 - WMC 2010 - IDMA's - 2009 IDMA Nominees and Winners FOR 2010 |work= International Dance Music Awards |publisher= Winter Music Conference |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8qjZwHz|archivedate= 2 June 2011}}</ref>

<ref name=BB4>{{cite web |url= http://www.billboard.com/charts/2009-10-24/dance-club-play-songs  |title= Dance Club Play Chart |date= 24 October 2009 |work= Billboard |publisher= Prometheus Global Media |accessdate=28 April 2011}}</ref>

<ref name=Ab>{{cite web |url= http://dancemusic.about.com/od/reviews/fr/Nadia_Ali_-_Embers_CD_Review.htm |title= Nadia Ali – ‘Embers’ |last= Gran |first= Chase |work= ''About.com'' |publisher= The New York Times Company |accessdate=28 April 2011|archiveurl=http://www.webcitation.org/query?id=1306860170879934|archivedate= 31 May 2011}}</ref>

<ref name=RM>{{cite web |url= http://racketmag.com/music/nadia-ali-embers-cd-review |title= Nadia Ali – ‘Embers’ – CD Review|last= Navarro |first= Gail |date= 5 December 2009 |work= Racket Magazine |accessdate=28 April 2011|archiveurl=http://www.webcitation.org/query?id=1306860170879943|archivedate= 31 May 2011}}</ref>

<ref name=Try>{{cite web |url= http://www.muumuse.com/2010/02/schiller-ft-nadia-ali-try-video-premiere.html |title=Schiller ft. Nadia Ali: Try (Video Premiere) |last= Stern |first= Bradley |date= 19 February 2010 |work= muumuse |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170879952|archivedate= 31 May 2011}}</ref>

<ref name=QOC>Albums in the ''Queen of Clubs Trilogy: The Best of Nadia Ali Remixed'':
*{{cite AV media notes|others=Nadia Ali|title=[[Queen of Clubs Trilogy: Ruby Edition]]|year=2010|type=Liner Notes|publisher=Smile in Bed Records}}
*{{cite AV media notes|others=Nadia Ali|title=[[Queen of Clubs Trilogy: Onyx Edition]]|year=2010|type=Liner Notes|publisher=Smile in Bed Records}}
*{{cite AV media notes|others=Nadia Ali|title=[[Queen of Clubs Trilogy: Diamond Edition]]|year=2010|type=Liner Notes|publisher=Smile in Bed Records}}</ref>

<ref name=Notice>{{cite web |url= http://www.muumuse.com/2010/07/nadia-ali-and-chris-reece-the-notice-video-premiere.html |title= Nadia Ali and Chris Reece – The Notice (video premiere)
|last= Stern |first= Bradley |date= 13 July 2010 |work= muumuse |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880001|archivedate= 31 May 2011}}</ref>

<ref name=BB5>{{cite web |url= http://www.billboard.com/charts/2011-04-30/dance-club-play-songs |title= Dance Club Play Chart |date= 30 April 2011 |work= Billboard |publisher= Prometheus Global Media |accessdate=28 April 2011}}</ref>

<ref name=Mobo>{{cite web |url= http://www.mobo.com/news-blogs/mobo-exclusive-nadia-ali-empress-of-the-empire |title= MOBO Exclusive: Nadia Ali - Empress of the Empire |last= Jadeja |first= Reema Kumari |date= 28 December 2010 |work= MOBO |publisher= MOBO Organisation |accessdate=26 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880036|archivedate= 31 May 2011}}</ref>

<ref name=LS>{{cite web |url= http://www.muumuse.com/2009/02/nadia-ali-love-story.html |title= Nadia Ali - Love Story
|last= Stern |first= Bradley |date= 12 February 2009 |work= muumuse |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880045|archivedate= 31 May 2011}}</ref>

<ref name=HO>{{cite web |url = http://hipsteroverkill.com/News/tabid/1965/articleType/ArticleView/articleId/589/NADIA-ALI-Interview-Article.aspx
|title= Nadia Ali: interview Article |last= Richards |first= Lola |date= 8 January 2010 |work= Hipster Overkill |publisher= |accessdate=7 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880080|archivedate= 31 May 2011}}</ref>

<ref name=MTVG>{{cite web |url= http://www.mtv.com/news/articles/1657309/nadia-ali-grammy-nomination.jhtml |title= Nadia Ali stunned by 'Fantasy' Grammy Nomination |last= Bhansali |first= Akshay |date= 4 February 2011 |work= MTV |publisher= MTV Networks |accessdate=31 May 2011|archiveurl=http://www.webcitation.org/query?id=1306860170880097|archivedate= 31 May 2011}}</ref>

<ref name=SH>{{cite web |url=http://www.allmusic.com/album/shivers-r787704
|title= Shivers - Armin van Buuren |work= Allmusic |publisher= Rovi Corporation |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8n1IGCL |archivedate= 2 June 2011}}</ref>

<ref name=RY>{{cite web |url= http://www.allmusic.com/album/release-yourself-vol-5-r846211
|title= Release Yourself, Vol.5 - Roger Sanchez |work= Allmusic |publisher= Rovi Corporation |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8m5Irbi |archivedate= 2 June 2011}}</ref>

<ref name=GU>{{cite web |url=http://www.allmusic.com/album/dubai-r859999
|title= Dubai - Sharam |work= Allmusic |publisher= Rovi Corporation |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8lxXcas |archivedate= 2 June 2011}}</ref>

<ref name=TD>{{cite web |url=http://www.allmusic.com/album/toca-1280-fm-r2090302
|title= Toca 128.0 FM - Tocadisco |work= Allmusic |publisher= Rovi Corporation |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8n6nP0p |archivedate= 2 June 2011}}</ref>

<ref name=SD>{{cite web |url=http://www.allmusic.com/album/wanderer-r1572500
|title= Wanderer - Serge Devant |work= Allmusic |publisher= Rovi Corporation |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8n3nmNO |archivedate= 2 June 2011}}</ref>

<ref name=RAM>{{cite web |url= http://www.armadamusic.com/news/2011/01/nadia-ali-rapture-music-video |title= Nadia Ali - Rapture music video |date= 25 January 2011 |work= Armada Music |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8mn01nD |archivedate= 2 June 2011}}</ref>

<ref name=IGGY>{{cite web |url= http://blog.mtviggy.com/2009/09/16/nadia-alis-new-album-embers |title= Nadia Ali's New Album Embers |author= Samantha |date=16 September 2009  |work= MTV Iggy |publisher= MTV Networks |accessdate=2 June 2011 |archiveurl=http://www.webcitation.org/5z8mtVDeF |archivedate= 2 June 2011}}</ref>

<ref name=CMNB>{{cite web |url=http://www.billboard.com/articles/review/1067831/sultan-ned-shepard-featuring-nadia-ali-call-my-name
|title= Sultan & Ned Shepard featuring Nadia Ali, "Call My Name" | last=Mason |first= Kerri |date= 1 April 2011 |work= Billboard |publisher= Prometheus Global Media |accessdate=6 September 2011|archiveurl=http://www.webcitation.org/61Tk9T8KC|archivedate= 6 September 2011}}</ref>

<ref name=grammy>{{cite web |url= http://www.grammy.com/nominees?year=2010&genre=30  |title= Best Remixed Recording, Non-Classical - Nominees and Winners | last= |first= |date= |publisher= National Academy of Recording Arts and Sciences |accessdate=11 May 2011 |archiveurl=http://www.webcitation.org/5z8rTkI6k |archivedate= 2 June 2011}}</ref>

<ref name=FP>{{cite web |url= http://www.muumuse.com/2009/07/nadia-ali-prepares-release-of-new.html |title=Nadia Ali prepares release of new single, album |last= Stern |first= Bradley |date= 1 July 2009 |work= muumuse |accessdate=3 June 2011|archiveurl=http://www.webcitation.org/5zAGzMLUi |archivedate= 3 June 2011}}</ref>

<ref name=TOI>{{cite news |title='Rapture' singer Nadia Ali in the city |first= |last= |url= http://articles.timesofindia.indiatimes.com/2011-06-01/art-culture/29607498_1_rapture-nadia-ali-electronic-music |newspaper= Times of India |date= 1 June 2011 |accessdate=3 June 2011 |archiveurl=http://www.webcitation.org/5zAHZ069O |archivedate= 3 June 2011}}</ref>

<ref name=FTG>{{cite web |url= http://www.armadamusic.com/news/2011/05/alex-sayz-feat-nadia-ali-free-to-go/ |title= Alex Sayz feat. Nadia Ali - Free to Go |date= 25 May 2011 |work= Armada Music |accessdate=6 June 2011 |archiveurl=http://www.webcitation.org/5zEqeaxJb |archivedate= 6 June 2011}}</ref>

<ref name=PRESS>{{cite web |url= https://www.youtube.com/watch?v=NKfcg85fhTU/ |title= Nadia Ali, Starkillers & Alex Kenji - Pressure (Original Mix) |date= 15 February 2011 |work= ''Spinnin' ''  |accessdate=6 June 2011}}</ref>

<ref name=Romania>Chart Positions for Nadia Ali - "Rapture": 
*{{cite web|url=http://www.rt100.ro/#/|title=Romanian Top 100|publisher=Romanian Top 100|accessdate=2 June 2011|archiveurl=http://www.webcitation.org/5z8u4lvJq |archivedate= 2 June 2011}}
*{{cite web |url= http://www.ultratop.be/nl/showitem.asp?interpret=Nadia+Ali&titel=Rapture&cat=s |title=Ultratop.be – Nadia Ali – Rapture |work=Ultratop50 |publisher=Ultratop & Hung Medien/hitparade.ch  |language=Dutch |accessdate=9 June 2011}}
*{{cite web |url= http://www.dutchcharts.nl/showitem.asp?interpret=Nadia+Ali&titel=Rapture&cat=s |title=Dutchcharts.nl – Nadia Ali – Rapture |work=Mega Single Top 100 |publisher=Hung Medien/hitparade.ch  |language=Dutch |accessdate=9 June 2011}}
*{{cite web |url= http://www.lescharts.com/showitem.asp?interpret=Nadia+Ali&titel=Rapture&cat=s |title=Lescharts.com – Nadia Ali – Rapture  |work=Les classement single |publisher=Hung Medien |language=French |accessdate=9 June 2011}}</ref>

<ref name= djf>{{cite web |url=http://www.djfix.com/interviews/show/50/IIO |title= Welcome to DJ Fix |last=Warner |first=Jennifer  |work= DJ Fix |accessdate=9 June 2011 |archiveurl=http://www.webcitation.org/5zJ8j41wp |archivedate= 9 June 2011}}</ref>

<ref name=FSG>{{cite web |url= http://www.armadamusic.com/music/?release=ARMD1094 |title= Armin van Buuren feat. Nadia Ali - Feels So Good |date= 19 June 2011 |work= Armada Music |accessdate=22 June 2011 |archiveurl=http://www.webcitation.org/5zdacGOOI |archivedate= 22 June 2011}}</ref>

<ref name=WIR>{{cite web |url= https://www.youtube.com/watch?v=_B55u21Kk-A |title= Nadia Ali  – "When it Rains" Official Music Video |date= 19 August 2010 |work= Smile in Bed |accessdate=19 August 2011}}</ref>

<ref name=Sander>{{cite web |url= http://www.inthemix.com.au/news/intl/50835/Sander_van_Doorn_unveils_new_artist_album |title= Sander van Doorn unveils new artist album |date= 16 August 2011 |work= inthemix |accessdate=19 August 2011}}</ref>

<ref name=BI>{{cite web |url= https://www.youtube.com/watch?v=_B55u21Kk-A |title= Spencer & Hill & Nadia Ali  – "Believe It" (Club Mix) (Exclusive Preview) |date= 23 September 2011 |work= Spinnin' |accessdate=23 September 2011}}</ref>

<ref name=EF>{{cite web |url=http://www.entertainment-focus.com/news/nadia-ali-starkillers-and-alex-kenji-release-some-pressure |title= Nadia Ali, Starkillers & Alex Kenji Release Some Pressure |last= Veevers |first= Brandon |work= Entertainment Focus |date= 19 October 2011 |accessdate=24 October 2011 |archiveurl=http://www.webcitation.org/62fe1silO |archivedate= 24 October 2011}}</ref>

<ref name=KIC>{{cite web |url= https://www.youtube.com/watch?v=bEMJ2RcuLj8 |title= Starkillers & Nadia Ali  – Keep It Coming [Teaser] |date= 14 December 2011 |work= ''Spinnin' '' |accessdate=10 February 2012}}</ref>

<ref name=EDX>{{cite web |url= http://www.armadamusic.com/news/2012/01/edx-on-the-edge/ |title= EDX - On The Edge |date= 29 January 2012 |work= Armada Music |accessdate=26 May 2011|archiveurl=http://www.webcitation.org/65LO0g6np |archivedate= 10 February 2012}}</ref>

<ref name=MP>{{cite web |url= http://dancemusic.about.com/od/reviews/fr/Morgan-Page-In-The-Air-Cd-Review.htm |title= Morgan Page - 'In the Air' CD Review |last= Norman |first= Ben |work= ''About.com'' |publisher= The New York Times Company |accessdate=30 March 2012}}</ref>

<ref name=MTV2>{{cite web |url= http://www.mtv.com/news/articles/1682850/nadia-ali-phoenix.jhtml |title= Nadia Ali In Early Stages Of ''Phoenix'' Album
 |last= Rosado |first= Nicola |date= 11 April 2012 |work= MTV |publisher= MTV Networks |accessdate=12 April 2012 |archiveurl=http://www.webcitation.org/66szZbGLb|archivedate= 13 April 2012}}</ref>

<ref name=BT2>{{cite web |url= http://www.armadamusic.com/news/2012/05/armada-music-signs-new-bt-album/ |title= Armada Music signs new BT album |date= 1 May 2012 |work= Armada Music |accessdate=22 May 2012 |archiveurl=http://www.webcitation.org/67qmvrS4S|archivedate= 22 May 2012}}</ref>

<ref name=RDW>{{cite web |url= http://www.realdetroitweekly.com/detroit/nadia-ali/Content?oid=1546850 |title= Nadia Ali: The Voice |date= 13 June 2012 |work= Real Detroit Weekly |accessdate=15 August 2012 |archiveurl=http://www.webcitation.org/69w5B0U8S |archivedate= 15 August 2012}}</ref>

<ref name=Submerge>{{cite web |url= http://submergemag.com/featured/nadia-ali/6110/ |title= All the World's a Stage |date= 23 May 2012 |work= SubMerge Magazine |accessdate=15 August 2012 |archiveurl=http://www.webcitation.org/69w64eaVj |archivedate= 15 August 2012}}</ref>

<ref name=Strictly>{{cite web |url= http://www.strictly.com/news/latest/Interview+with+Nadia+Ali/477 |title= Strictly News - Interview with Nadia Ali |work= Strictly Rhythm |accessdate=15 August 2012 |archiveurl=http://www.webcitation.org/69w6CDabE |archivedate= 15 August 2012}}</ref>

<ref name= Sutra>{{cite video |people= Matt Christensen (Director) |date= 21 February 2013 |title= Live at Sutra Nightclub December 2012 - Nadia Ali
 |url= https://www.youtube.com/watch?v=YSiqCwAan2M |format= |medium= YouTube Video |publisher= Hashtag Management|location= Orange County, California |accessdate=2 April 2013 }}</ref>

<ref name=Roxanne>{{cite web |url= http://blog.lessthan3.com/2014/01/premiere-nadia-ali-roxanne/ |title= Premiere: Nadia Ali - Roxanne |date= 13 January 2014 |last= Bennett |first= Josh |work= Less Than 3 |accessdate= 14 January 2014|archiveurl=http://www.webcitation.org/6McJLHwmi|archivedate= 14 January 2014}}</ref>

<ref name=hangout>{{cite video |date= 17 December 2013 |title= Nadia Ali **Live** V-Live Saturday January 21st |url= https://www.youtube.com/watch?v=ofwnYWVqZCQ |format= |medium= YouTube Video |publisher= [[Google Hangouts]] |location= Los Angeles, California |accessdate=14 January 2014 }}</ref>

<ref name=Pang>{{cite web |url= http://www.dancingastronaut.com/2015/09/da-premiere-nadia-ali-pang-head-original-mix/ |title= DA Premiere: Nadia Ali & PANG! - All In My Head (Original Mix) |date= September 2015 |last= Spada |first= Andew |work= Dancing Astronaut |accessdate= 28 September 2015 |archiveurl=http://www.webcitation.org/6bsn6QHl3|archivedate= 28 September 2015}}</ref>
}}

==External links==
{{Portal|Biography|Asian Americans}}
{{commons category}}
*{{Official website|http://www.nadiaali.com/}}

{{Nadia Ali}}
{{IiO}}

{{Authority control}}

{{DEFAULTSORT:Ali, Nadia}}
[[Category:1980 births]]
[[Category:Living people]]
[[Category:Muhajir people]]
[[Category:American female singer-songwriters]]
[[Category:American house musicians]]
[[Category:American musicians of Pakistani descent]]
[[Category:American people of Pakistani descent]]
[[Category:Pakistani emigrants to the United States]]
[[Category:Singers from New York City]]
[[Category:Pakistani singer-songwriters]]
[[Category:Pakistani female singers]]
[[Category:Pakistani electronic musicians]]
[[Category:Pakistani expatriates in Libya]]
[[Category:Women in electronic music]]
[[Category:Songwriters from New York]]
[[Category:People from Tripoli]]