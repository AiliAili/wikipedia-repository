<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=TDN
 |image=Naval Aircraft Factory TDN-1 piloted.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=Assault drone
 |national origin=United States
 |manufacturer=[[Naval Aircraft Factory]]
 |designer=
 |first flight=15 November 1942
 |introduced=
 |retired=
 |status=
 |primary user=[[United States Navy]]
 |more users=
 |produced=
 |number built=104-114<ref name="VS"/>
 |developed from=
 |variants with their own articles=
 |developed into=
}}
|}

The '''Naval Aircraft Factory TDN''' was an early [[unmanned combat aerial vehicle]] - referred to at the time as an "[[flying bomb|assault drone]]" - developed by the [[United States Navy]]'s [[Naval Aircraft Factory]] during the [[Second World War]]. Developed and tested during 1942 and 1943, the design proved moderately successful, but development of improved drones saw the TDN-1 relegated to second-line duties, and none were used in operational service.

==Design and development==
The development of the [[radar altimeter]] and [[television]] in the early 1940s made remotely guided, bomb- or torpedo-carrying aircraft a practical proposition,<ref name="DS2">Parsch 2005</ref> and in January 1942, the Naval Aircraft Factory was instructed to initiate the development of such an aircraft, with a go-ahead for prototype construction being given in February.<ref>Naval Aviation News, Volume 55 (January 1973). p.12.</ref> A production contract for 100 aircraft was issued in March, with John S. Kean being assigned as project manager of the TDN-1 project,<ref>Trimble 1990, p. 263.</ref> with the aircraft being designed to be capable of using either television or radar as its guidance system.<ref>Newcome 2004, p.67.</ref> Constructed mainly from wood, the TDN-1 had a fixed tricycle landing gear, and could be fitted with a conventional cockpit in place of its guidance equipment for test flights.<ref name="VS">Goebel 2010</ref>

In an example of the use of companies traditionally uninvolved in the aviation industry to reduce interference with higher priority projects, production of the final thirty aircraft was licensed to the [[Brunswick-Balke-Collender Company]], a [[Michigan]]-based manufacturer of [[bowling balls]] and [[billiard tables]].<ref>Cunningham 1951, p.91.</ref>

==Operational history==
One hundred production TDN-1 aircraft were ordered in March 1942.<ref name="Osprey">Zaloga 2008, p.8.</ref> Despite being specifically designed to be a simple, low-performance aircraft,<ref name="DS1"/> and despite proving promising in testing, the type was considered to be too complicated and expensive for use operationally. The improved [[Interstate TDR]] was selected for development as an alternative,<ref name="Osprey"/> the majority of TDN-1s being used in the test, liaison and training roles, with some being expended as aerial targets.<ref name="VS"/> The TDN-1 is often credited as the first US drone to take off from an aircraft carrier freely ([[USS Sable (IX-81)|USS ''Sable'']]). An [[Airspeed Queen Wasp]] had already been catapulted from [[HMS Ark Royal (1914)|HMS ''Pegasus'']] in 1937.

==Variants and operators==
{{flagicon|United States|1912}} [[United States Navy]]
;XTDN-1
:Four prototype aircraft powered by [[Franklin O-300]] engines.<ref name="DS1"/>
;TDN-1
:Production version of XTDN-1; 100 aircraft produced.<ref>Trimble 1990, p.267.</ref>

==Specifications (TDN-1)==
[[File:Naval Aircraft Factory TDN-1.jpg|thumb|right|TDN-1 aircraft aboard {{USS|Sable|IX-81|6}}.]]
{{Aircraft specs
|ref=<ref name="DS1">Parsch 2003.</ref>
|prime units?=imp
|genhide=
|crew=0-1 (optional pilot)
|capacity=
|length m=
|length ft=37
|length in=
|length note=
|span m=
|span ft=48
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=

<!--
        Powerplant
-->
|eng1 number=2
|eng1 name= [[Lycoming O-435]]-2
|eng1 type= [[Flat engine|horizontally-opposed]] six-cylinder piston engines
|eng1 kw=
|eng1 hp=220
|eng1 shp=
|power original=
|thrust original=
|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=Y
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=145
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=

<!--
        Armament
-->
|armament=* one {{convert|2000|lb|adj=on}} bomb or [[aerial torpedo]].
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}
{{clear}}

==See also==
{{Portal|Aviation|United States Navy}}
{{aircontent
|see also=
* [[History of unmanned aerial vehicles]]
|related=
|similar aircraft=
* [[Gorgon (U.S. missile)]]
* [[Interstate TDR]]
* [[LTV-N-2 Loon]]
* [[McDonnell LBD Gargoyle]]
|lists=
* [[List of unmanned aerial vehicles]]
* [[List of military aircraft of the United States (naval)]]
}}

==References==
{{commons category|Naval Aircraft Factory TDN}}
;Citations
{{reflist|2}}
;Bibliography
{{refbegin}}
* {{cite book |last1=Cunningham |first1=William Glenn |title=The Aircraft Industry: A study in industrial location |year=1951 |publisher=L.L. Morrison |location=Los Angeles |asin=B0007DXJL2  |url=https://books.google.com/books?ei=A33lTPG6JsG88ga0wOHcDA&ct=result&id=4uAmAAAAMAAJ&dq=Naval+Aircraft+Factory+TDN&q=TDN#search_anchor |accessdate=2010-11-18}}
*{{cite web |url=http://www.vectorsite.net/twcruz_1.html#m4 |title= The Aerial Torpedo |first=Greg |last=Goebel |year=2010 |work=Cruise Missiles |publisher=VectorSite |accessdate=2010-11-18}}
* {{cite book |last1=Newcome |first1=Lawrence R. |title=Unmanned Aviation: A Brief History of Unmanned Aerial Vehicles |url=https://books.google.com/books?id=fkKDPM7F7bMC&pg=PA8#v=onepage&q&f=false |accessdate=2010-11-17 |year=2004 |publisher=American Institute of Aeronautics and Astronautics |location=Reston, Virginia |isbn=978-1-56347-644-0}}
*{{cite web |url=http://www.designation-systems.net/dusrm/app1/td.html#_TDN |title= TD Series |first=Andreas |last=Parsch |year=2003 |work=Directory of U.S. Military Rockets and Missiles, Appendix 1: Early Missiles and Drones |publisher=designation-systems.net |accessdate=2010-11-18}}
*{{cite web |url=http://www.designation-systems.net/dusrm/app1/bq-4.html |title= Interstate BQ-4/TDR |first=Andreas |last=Parsch |year=2005 |work=Directory of U.S. Military Rockets and Missiles, Appendix 1: Early Missiles and Drones |publisher=designation-systems.net |accessdate=2010-11-17}}
* {{cite book |last1=Trimble |first1=William F. |title=Wings for the Navy: A History of the Naval Aircraft Factory 1917-1956 |url=|year=1990 |publisher=Naval Institute Press |location=Annapolis, MD |isbn=978-0-87021-663-3}}
* {{cite book |last1=Zaloga |first1=Steven |title=Unmanned Aerial Vehicles: Robotic Air Warfare 1917-2007 |url=https://books.google.com/books?id=r92M0Z9xAe4C&printsec=frontcover&dq=Unmanned+Aerial+Vehicles:+Robotic+Air+Warfare+1917-2007&hl=en&sa=X&ei=T4AoT6y5NOPo2AWJ3d3bAg&ved=0CD0Q6AEwAA#v=onepage&q=TDN-1&f=false |accessdate=2010-11-17 |series=New Vanguard |volume=144 |year=2008 |publisher=Osprey Publishing |location=New York |isbn=978-1-84603-243-1}}
{{refend}}

;Further reading
{{refbegin}}
* {{cite journal |url=http://stagone.org/command-break.html#m4 |title= Unmanned Precision Weapons Aren't New |first=Nick T. |last=Spark |year=2005 |work=Proceedings Magazine |publisher=U.S. Naval Institute |accessdate=2005-02-01}}
{{refend}}

{{USN target drones}}

[[Category:Naval Aircraft Factory aircraft|TDR]]
[[Category:United States bomber aircraft 1940–1949]]
[[Category:Unmanned aerial vehicles of the United States]]
[[Category:World War II guided missiles of the United States]]
[[Category:Twin-engined tractor aircraft]]
[[Category:High-wing aircraft]]