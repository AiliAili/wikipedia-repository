{{Infobox song
| Name           = 575
| Artist         = [[Perfume (Japanese band)|Perfume]]
| Album          = [[JPN (album)|JPN]]
| Recorded       = 2010<br />[[Shibuya]], [[Tokyo]]<br /><small>(Contemode Studios)</small>
| Released       = July 14, 2010
| Format         = [[Music download|Digital download]]
| Genre          = [[J-Pop]]
| Length         = 4:24
| Label          = {{hlist|[[Tokuma Japan Communications]]|[[Universal Music Japan]]}}
| Writer         = [[Yasutaka Nakata]]
| Producer       = Yasutaka Nakata
| prev           = "[[Laser Beam/Kasuka na Kaori|Kasuka na Kaori]]"
| prev_no        = 8
| track_no       = 9
| next           = "[[Voice (Perfume song)|Voice]]"
| next_no        = 10
| Misc           = 
}}

"'''575'''" is a song recorded by Japanese recording girl group [[Perfume (Japanese band)|Perfume]] for their third studio album, ''[[JPN (album)|JPN]]'' (2011). It was written, composed, arranged, and produced by Japanese musician and [[Capsule (band)|Capsule]] member [[Yasutaka Nakata]]. The song was included as a [[B-side]] track for the group’s single, "[[Voice (Perfume song)|Voice]]". It was also released exclusively to Uta stores in Japan on July 14, 2010. Musically, "575" was described as a mellow [[Japanese pop music|Japanese pop]] song. It marks the first time that the group perform in a [[Rap (music)|rap]] structure, delivered after the first chorus. The song's title, and the structure of its verses, derives from the structure of [[haiku]], a Japanese style of poetry which comprises a 5-syllable line, a 7-syllable line, and then another 5-syllable line.

Upon its release, the track garnered positive reviews from music critics, who praised the song’s composition and the rap delivery. Due to the song being released digitally and as a B-side to "Voice", it was ruled ineligible to chart on Japan’s [[Oricon Singles Chart]]. However, it peaked at number 73 on ''[[Billboard (magazine)|Billboard]]'''s [[Japan Hot 100]] chart, and number four on the [[RIAJ Digital Track Chart]]. It was certified gold by the [[Recording Industry Association of Japan]] (RIAJ) for cellphone purchases of 100,000 units. A music video was originally to debut on their JPN tour in 2012, but the idea was scrapped. Instead, it was included on the live DVD.

==Background and composition==
"575" was written, composed, arranged, and produced by Japanese musician and [[Capsule (band)|Capsule]] member [[Yasutaka Nakata]].<ref name="albumnotes">{{cite AV media notes |title=JPN|others=Perfume|year=2011|type=CD Album; Liner notes|publisher=Tokuma Japan Communications; Universal Music Japan|id=DJ0134|location=Japan}}</ref> Alongside this, it was recorded, mixed, and mastered by him. The song was recorded in 2010 at Contemode Studios, [[Shibuya]], [[Tokyo]] by Nakata. It was selected as a [[B-side]] track to "[[Voice (Perfume song)|Voice]]", the second single to the groups album ''[[JPN (album)|JPN]]'' (2011). <ref name="voice">{{cite AV media notes |title=Voice|others=Perfume|year=2010|type=CD Single; Liner notes|publisher=Tokuma Japan Communications; Universal Music Japan|id=TKCA-73565 |location=Japan}}</ref> It also appeared on the album, listed at number 9 on the track list.<ref name="albumnotes"/> The instrumental version appeared on the [[CD single]] and digital EP for the "Voice" single.<ref name="voice"/> The song was released exclusively to Uta stores in Japan on July 14, 2010.<ref name="uta">{{cite news|author=Hiraga, Tetsuo|url= http://www.hotexpress.co.jp/news/100714_perfume/|title=Perfume新曲がCMソングに|work=Hot Express|date=July 14, 2010|accessdate=April 11, 2016|language=Japanese}}</ref>

Musically, the song has been described as a mellow [[Japanese pop music|Japanese pop]] song.<ref name="japantimes">{{cite news|author=Martin, Ian|url= http://www.japantimes.co.jp/culture/2011/12/08/music/concert-previews/perfume-jpn/#.Vws7MqR97IV|title=Perfume – JPN (album review)|work=[[The Japan Times]]|date=December 8, 2011|accessdate=April 11, 2016}}</ref><ref name="randomjpop">{{cite news|author=Random J|url= http://randomjpop.blogspot.co.nz/2012/01/album-reivew-perfume-jpn.html|title=Perfume – JPN (album review)|work=Random J Pop|date=January 18, 2012|accessdate=April 11, 2016}}</ref> It marks the first time that the group perform in a [[Rap (music)|rap]] structure, delivered after the first chorus. Throughout a majority of the song, the girl’s vocals are heavily processed with post-production tools such as [[vocoder]] and [[autotune]]. Only two [[English language|English]] phrases are used in the song; these being the lyrics, "Give it up" and "Good night". Japanese music editor Random J reviewed the album on his personal blog, and commented about the songs composition, "'575' is Perfume's equivalent of a slow jam... And the girls even spit some bars on it."<ref name="randomjpop"/> Ian Martin from ''[[The Japan Times]]'' described the songs composition as, "a curiously mellow take on the 1990s ballad/rap hybrid J-pop formula."<ref name="japantimes"/> Perfume stated together that they were "very surprised, yet very anxious" about the rap section.<ref>{{Citation |last=|first=|title=Interview with Perfume|journal=Ongakutohito|issue=September|pages=37|date=September 2010|language=Japanese}}</ref>

==Critical response==
"575" received favorable reviews from most music critics. Japanese music editor Random J reviewed the song on his personal blog, and gave it a positive remark. He described it as a "really nice, spaced out, mellow song". Regarding the song’s rap section, he commented "Their flow won't have [[Jay-Z]] throwing in the towel, but they sound a damn earshot better than most dudes in the rap game right now."<ref name="randomjpop"/> A staff editor from ''CD Journal'' gave the song a positive review. They compared the composition and delivery to the groups song "[[Baby Cruising Love/Macaroni|Macaroni]]", from their 2008 debut album ''[[Game (Perfume album)|Game]]''. The reviewer praised the groups "brave" vocal delivery.<ref name="cdjournal">{{cite news|author=CD Journal Staff|url=http://artist.cdjournal.com/d/voice/4110060799|title=Perfume – Voice (single review)|work=CD Journal|date=August 11, 2010|accessdate=April 11, 2016|language=Japanese}}</ref> Another editor from the same publication reviewed the album, and gave it a positive review. The reviewer complimented the song's rap section and praised the "new grounded" compositions.<ref>{{cite news|author=CD Journal Staff|url=http://artist.cdjournal.com/d/jpn/4111091673|title=Perfume – JPN (album review)|work=CD Journal|date=November 30, 2011|accessdate=April 12, 2016|language=Japanese}}</ref> Ian Martin from ''[[The Japan Times]]'' gave the song a mixed review; despite his appreciate of the slow composition, he criticized, in general, the lack of "creativity" and "invention" through the second half of the album and felt these factors were presented throughout other projects by Nakata apart from Perfume.<ref name="japantimes"/>

==Commercial performance==
Due to the song being released digitally and as a B-side to "Voice", it was ineligible to chart on Japan’s [[Oricon Singles Chart]] because it does not count digital sales. However, it managed to chart on other record charts in Japan. It peaked at number 73 on ''[[Billboard (magazine)|Billboard]]'''s [[Japan Hot 100]] chart and is the highest charting non-single track from ''JPN''.<ref name="billboard">{{cite web | title=Japan Billboard Hot 100 | url=http://www.billboard-japan.com/charts/detail?a=hot100&year=2010&month=08&day=30 | work=[[Billboard (magazine)|Billboard]] |language=Japanese | date=August 25, 2010 | accessdate=January 7, 2015}}</ref><ref>{{cite web | title=Japan Billboard Hot 100 | url=http://www.billboard-japan.com/charts/detail?a=hot100&year=2011&month=12&day=19 | work=[[Billboard (magazine)|Billboard]] |language=Japanese | date=December 14, 2011 | accessdate=January 7, 2015}}</ref> It then charted on the [[RIAJ Digital Track Chart]], peaking at number four and was the groups highest charting single on there.<ref name="riajchart">{{cite web |title=レコード協会調べ 2010年08月25日～2010年08月31日 ＜略称：レコ協チャート（「着うたフル(R)」）＞
|trans_title=Record Association report: 2010.08.25~2010.08.31 <abb. Reco-kyō Chart 'Chaku-uta Full'> |url=http://satsuki.musicdb.gr.jp/all_info/ranking_weekly/WeeklyRankingAction.do?term=2010s&weeklyCd=20100831&rankKbn=50 |language=Japanese |publisher=[[Recording Industry Association of Japan]] |date=September 3, 2010 |archiveurl=https://web.archive.org/web/20150107051041/http://satsuki.musicdb.gr.jp/all_info/ranking_weekly/WeeklyRankingAction.do?term=2010s&weeklyCd=20100831&rankKbn=50 |archivedate=January 7, 2015 |accessdate=January 7, 2015}}</ref> The song was certified gold by the [[Recording Industry Association of Japan]] (RIAJ) for cell phone purchases of up to 100,000 units. <ref name="cert">{{cite web|title=レコード協会調べ　9月度有料音楽配信認定 |trans_title=Record Association Investigation: September Digital Music Download Certifications |url=http://www.riaj.or.jp/data/others/chart/w101020.html |publisher=[[Recording Industry Association of Japan]] |language=Japanese |date=October 20, 2010 |accessdate=December 13, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130915153303/http://www.riaj.or.jp/data/others/chart/w101020.html |archivedate=September 15, 2013 |df= }}</ref> In conjunction with the sales of "Voice" and "575" on the CD Single, it was certified gold by the RIAJ for physical shipments of 100,000 units in Japan.<ref name="sales">{{cite web | title=オリコンランキング情報サービス「you大樹」 | trans_title=Oricon Ranking Information Service 'You Big Tree' | url=http://ranking.oricon.co.jp |language=Japanese | work=[[Oricon]] |subscription=yes | accessdate=January 5, 2015}}</ref><ref name="RIAJ-aug2010">{{cite web | title=ゴールド等認定作品一覧　2010年8月 | trans_title=Works Receiving Certifications List (Gold, etc) (August 2009) | url=https://www.riaj.or.jp/data/others/gold/201008.html | publisher=[[Recording Industry Association of Japan]] |language=Japanese | date=September 10, 2011 | accessdate=March 29, 2014}}</ref>

==Music video and live performances==
[[File:Perfume575musicvideo.png|thumb|right|200px|Perfume member, [[Yuka Kashino|Kashiyuka]], singing in the music video for "575".]]
A music video was used as a backdrop projection for their Tokyo dome 2010 tour [12345678910], projected on all the screens.<ref name="news"/> Instead, it was released on the limited edition DVD. It featured the girls singing the song in front of the songs title. Inter cut scenes of the live performance, and the girls getting ready for the show, were included in the video.<ref name="news">{{cite news|author=Hot Express staff|url=http://www.hotexpress.co.jp/news/110202_perfume/|title=
◆Perfume 新曲『レーザービーム』を発射！|work=Hot Express|date=February 2, 2011|accessdate=April 12, 2016|language=Japanese}}</ref>

The song has been performed on one concert tour, and has appeared on one commercial in Japan. The song was used as the theme song for the KDDI Light Pool commercial in Japan.<ref name="cdjournal"/><ref>{{cite news|author=Recochoku Staff|url=http://recochoku.jp/song/S20339134/|title=Perfume – 575 – from JPN|work=Recochoku.co.jp|date=November 30, 2011|accessdate=April 12, 2016|language=Japanese}}</ref> Japanese rapper, [[Kreva (rapper)|Kreva]], performed a [[Cover song|cover version]] of the song on Music Japan Broadcast in early 2011.<ref name="kreva">{{cite news|author=Oricon Style Staff|url=http://www.oricon.co.jp/music/interview/2011/perfume1130/index2.html|title=日常に近い！？ふと口ずさむ感じの歌|work=[[Oricon Style]]|date=November 30, 2011|accessdate=April 12, 2016|language=Japanese}}</ref> The song was performed on their 2010 [[Tokyo Dome]] concert tour, where it was included during the second segment. It was included on the live DVD, released on February 9, 2011.<ref name="livedvd">{{cite AV media notes |title=Live at Tokyodome: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11|others=Perfume|year=2011|type=Live DVD Album; Liner notes|publisher=Tokuma Japan Communications; Universal Music Japan|id=TKBA-1147|location=Japan}}</ref> The performance included the girls dancing around geometrical shapes and was positively received from music critics. Yuki Sugioka from Hot Express complimented the girls performance, alongside the stage production of the segment.<ref>{{cite news|author=Sugioka, Yuki|url=http://www.hotexpress.co.jp/live_report/101110_perfume/|title=
Perfume ライブレポート|work=Hot Express|date=November 3, 2010|accessdate=April 12, 2016|language=Japanese}}</ref> The song was included on the group's [[compilation album|compilation]] [[box set]], ''Perfume: Complete LP Box'' (2016).<ref>{{cite AV media notes |title=Perfume: Complete LP Box|others=Perfume|year=2016|type=Six 12" Vinyls; Liner notes|publisher=Tokuma Japan Communications; Universal Music Japan|id=TKJA-10066|location=Japan}}</ref>

==Credits and personnel==
Details adapted from the liner notes of the ''JPN'' album.<ref name="albumnotes"/>

* [[Ayano Ōmoto]] – vocals
* [[Yuka Kashino]] – vocals
* [[Ayaka Nishiwaki]] – vocals
* [[Yasutaka Nakata]] – [[Record producer|producer]], [[composer]], arranger, mixing, mastering.

==Chart and certifications==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable plainrowheaders"
!Chart (2010)
!Peak<br>position
|-
!scope="row"|[[Japan Hot 100]] (''[[Billboard (magazine)|Billboard]]'')<ref name="billboard"/>
| style="text-align:center;"|73
|-
!scope="row"|Japan [[RIAJ Digital Track Chart]] ([[Recording Industry Association of Japan|RIAJ]])<ref name="riajchart"/>
| style="text-align:center;"|4
|-
|}
{{col-2}}

===Certification===
{{Certification Table Top}}
{{Certification Table Entry |region=Japan |artist=Perfume |title=575 |type=single |award=Gold |certref=<ref name="cert"/> |relyear=2010 |autocat=yes}}
{{Certification Table Bottom}}

{{col-end}}

==Release history==
{| class="wikitable plainrowheaders"
|-
!scope="col"|Region
!scope="col"|Date
!scope="col"|Format
!scope="col"|Label
|-
|rowspan="2"|Japan<ref name="uta"/><ref name="voice"/><ref name="voicedvd">{{cite AV media notes |title=Voice|others=Perfume|year=2010|type=CD and DVD Single; Liner notes|publisher=Tokuma Japan Communications; Universal Music Japan|id=TKCA-73560|location=Japan}}</ref><ref>{{cite web|url=https://itunes.apple.com/jp/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=[[iTunes Store]] (Japan)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|July 14, 2011
|Digital download
|rowspan="2"|{{hlist|[[Tokuma Japan Communications]]|[[Universal Music Japan]]}}
|-
|rowspan="11"|August 11, 2010
|{{hlist|[[CD single]]|[[DVD single]]|[[Music download|digital download]]}}
|-
!scope="row"|United States<ref>{{cite web|url=https://itunes.apple.com/us/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (United States of America)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|rowspan="10"|[[Music download|Digital download]]
|rowspan="10"|Universal Music Japan
|-
!scope="row"|Australia<ref>{{cite web|url=https://itunes.apple.com/au/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (Australia)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|New Zealand<ref>{{cite web|url=https://itunes.apple.com/nz/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (New Zealand)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|Canada<ref>{{cite web|url=https://itunes.apple.com/ca/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (Canada)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|United Kingdom<ref>{{cite web|url=https://itunes.apple.com/gb/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (United Kingdom)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|Germany<ref>{{cite web|url=https://itunes.apple.com/de/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (Germany)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|Ireland<ref>{{cite web|url=https://itunes.apple.com/ie/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (Ireland)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|France<ref>{{cite web|url=https://itunes.apple.com/fr/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (France)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|Spain<ref>{{cite web|url=https://itunes.apple.com/es/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (Spain)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
!scope="row"|Taiwan<ref>{{cite web|url=https://itunes.apple.com/tw/album/voice-ep/id661936282|title=Voice – EP – by Perfume|publisher=iTunes Store (Taiwan)|date=August 11, 2010|accessdate=April 11, 2016}}</ref>
|-
|}

==See also==
*"[[Voice (Perfume song)|Voice]]" – Corresponding single to "575".

==References==
{{reflist|2}}

==External links==
*[http://www.perfume-web.jp/discography/?o09 ''JPN''] – Perfume's official website.
*[http://www.perfume-web.jp/discography/?s11 "Voice"] – Perfume's official website.

{{Perfume (Japanese band)}}
{{Good article}}

[[Category:2010 singles]]
[[Category:2010 songs]]
[[Category:Perfume (Japanese band) songs]]
[[Category:Song recordings produced by Yasutaka Nakata]]
[[Category:Songs written by Yasutaka Nakata]]
[[Category:Universal Music Japan singles]]