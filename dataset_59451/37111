{{good article}}
{{Use dmy dates|date=July 2012}}
{{Use British English|date=July 2012}}
{{Infobox person
| name = Herbert E. Balch
| image = Herbert_Balch.jpg
| image_size = 
| caption = Portrait of Balch
| birth_date = {{birth date |1869|11|4|df=y}}
| birth_place = [[Wells, Somerset|Wells]], [[Somerset]]
| death_date = {{death date and age|1958|5|27|1869|11|4|df=y}}
| death_place = [[Wells, Somerset|Wells]], [[Somerset]]
| other_names = 
| known_for =
| occupation = [[Archaeologist]], [[Speleologist]], [[Geologist]], [[Naturalist]]
| nationality =
}}

'''Herbert Ernest Balch''' (4 November 1869 – 27 May 1958) MA FSA was an English archaeologist, naturalist, [[Caving|caver]] and [[geologist]] who explored the [[caves of the Mendip Hills]] and pioneered many of the techniques used by modern cavers. Born in Wells, he gained a scholarship to [[The Blue School, Wells|The Blue School]] before leaving school at the age of 14 to become a messenger for Wells Post Office.

Balch became interested in [[stratigraphy]] and cave archaeology after attending a talk by [[William Boyd Dawkins]]. Balch led much of the exploration through the caves near [[Wookey Hole]] village, discovering and mapping many caves. He also made discoveries of artefacts used by the people who lived in the caves during the [[Iron Age]]. Balch was a founder member of the Wells Natural History and Archaeology Society and through the society he founded the Wells Museum, largely including his own collection of artefacts.

==Early life==
Herbert Ernest Balch was born in [[Wells, Somerset|Wells]], [[Somerset]] on {{birth date |1869|11|4|df=y}}. His parents were William Balch, a brushmaker, and Sarah Ellen Balch.<ref name=ODNB>{{cite ODNB|id=75966|title=Balch, Herbert Ernest (1869–1958), cave explorer and museum curator|last=Salmon|first=Bridget}}</ref> At the age of seven he gained a scholarship to [[The Blue School, Wells|The Blue School]] in Wells. There he helped [[George Johnson (priest)|George Johnson]], [[Dean of Wells]], who had poor eyesight due to his age, by reading texts.<ref name=ODNB />

He left school at 14 and became telegraph messenger boy at Wells Post Office. He remained at the post office for his entire working life, working his way up to [[postmaster]]. Whilst working as a messenger, he spent a lot of time at the village of [[Wookey Hole]]. He began caving with other teenagers from the village in the mid-1880s. After attending a talk by [[William Boyd Dawkins]], he took an interest in cave archeology and especially in [[stratigraphy]]. In the 1890s, Balch was introduced to the [[caves of the Mendip Hills]] by [[Thomas Willcox]], manager of the [[Priddy Mineries|Priddy lead mine]]s. While caving with Willcox at [[Lamb Leer]] in 1897, one of Balch's ropes snapped. He saved himself by catching a guide rope,<ref name=ODNB /> but the {{convert|60|ft}} fall rendered him unconscious, suffering serious friction burns and back injuries.<ref name=ODNB />

As Balch finished his formal education early, he compensated through self-improvement, even cycling to London to buy books. He is known for discovering new caving techniques and carefully recording his finds. Balch would spend his free time on Saturdays and after work on caving expeditions, although he would not do any caving on Sundays, for religious reasons. He would set out for some expeditions directly after work, caving overnight and coming home in time to wash before returning to work.<ref name=ODNB />

==Excavations==
Balch's investigations into [[geomorphology]] and [[hydrology]] led to his decision to try to find the origins of the water that rose to the surface at Wookey Hole Caves, the source of the [[River Axe (Bristol Channel)|River Axe]]. In 1901, he led a team of miners and cavers to dig into [[Swildon's Hole]], where he found the 'Forty Foot Pot' as well as chambers full of [[stalagmite]]s.<ref name=ODNB /> Despite the publicity around the find, Balch refused to disclose the location of the cave as he believed it was too dangerous for amateur explorers. The farmer who owned the land denied them entrance the following year, turning the valley into a [[fish farm]],<ref name=oucc>{{cite web|url=http://www.oucc.org.uk/dtt/vol04/dtt4_24.htm|title=To he who lit the Stygian caves|last=Hooper|first=James|date=7 June 1994|work=Depth through thought -OUCC News |publisher=Oxford University Cave Club|accessdate=1 September 2016}}</ref> so the team headed upstream where they discovered [[Eastwater Cavern]], one of Balch's personal favourites.<ref name=ODNB />

Balch conducted many of his excavations in conjunction with the [[Somerset Archaeological and Natural History Society]]; he is credited with being the first to explore many of the caves in the area, the most famous being the [[Wookey Hole Caves]].<ref name=retire /> Balch kept a base at [[Rookham]] for his longer excavations, he bought a railway carriage in a field near Rookham from his friend, the headmaster at the Blue School. Balch called the 26 foot long carriage his 'Summer Palace' and modified it to form part of a bungalow. Balch's family would continue to use the carriage for summer holidays after Balch's death.<ref>{{cite news|title=Bristol Exploration Club - A Potted history of H. E. Balch 1869 - 1958|url=http://bec-cave.org.uk/index.php?option=com_content&view=article&id=1124:belfry-bulletin-no-493-october-1997&catid=68&Itemid=508&showall=&limitstart=9&lang=en|accessdate=1 September 2016|work=bec-cave.org.uk}}</ref>

In 1906, Balch started investigating the [[Iron Age]] cave dwellers in Wookey Hole Caves, embarking on a four-year study of the caves along with other members of the Mendip Nature Research Committee. The group mapped the caves, drew illustrations of their finds and took photographs, collecting the information in the 1914 book ''Wookey Hole: its Caves and Cave-Dwellers''.<ref name=ODNB /> When giving lectures on the cave dwellers, he would compare them to Eskimos, drawing parallels in their art and hunting styles, and would show pictures of the tools used by the cave dwellers that he discovered in Wookey Hole Caves, as well as items such as brooches and child's play toys. He also told of a goat-herd he discovered in a cave who had died, leaving his goats tethered, subsequently perishing.<ref>{{cite news|title=Nature's chronometer: Ticking of centuries|url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000511/19150209/045/0005|accessdate=31 August 2016|work=Exeter and Plymouth Gazette|date=9 February 1915|page=5}}</ref>

Balch was a member of caving clubs such as the [[Wessex Cave Club]]. He was a serious [[speleologist]] and often made ten-hour trips wearing cloth cap, old suit and tie.<ref>{{cite web| url=http://www.newscientist.com/article/mg12516984.000-a-cavers-centenary--review-of-a-man-deep-in-mendip-thecaving-diaries-of-harry-savory-19101921-edited-by-john-savory-.html|title=A caver's centenary / Review of 'A Man Deep In Mendip, The Caving Diaries of Harry Savory 1910-1921' edited by John Savory|last=Stanton|first=Willie|date=6 January 1990|work=New Scientist|accessdate=1 September 2016}}</ref> [[Balch Cave]] near [[Stoke St Michael]] is named after him.<ref>{{cite web|title=Balch Cave - Mendip Cave Registry Site Details|url=http://www.mcra.org.uk/registry/sitedetails.php?id=384|website=www.mcra.org.uk|accessdate=1 September 2016}}</ref> As an authority on the caves under the Mendip Hills, Balch was consulted by water companies who were looking for new water supplies.<ref name=ODNB />

==Wells Museum==
[[File:Plaque on Wells Museum to Herbert E Balch.jpg|thumb|upright|The plaque at the [[Wells and Mendip Museum]]]]
Balch was a founder member of the Wells Natural History and Archaeological Society, using his connections at Wells Cathedral to organise lectures. It was through the society and with help of [[Thomas Jex-Blake]], [[Dean of Wells]], that Balch managed to set up the Wells Museum in 1893, displaying his own artefacts in the cathedral's cloister. The museum had grown significantly by 1928 so Balch persuaded [[William Wyndham (1868–1950)|William Wyndham]] to purchase a property for the museum on the cathedral green.<ref name=ODNB /> In 1932, the museum was relocated to the cathedral green, eventually becoming the [[Wells and Mendip Museum]].<ref>{{cite web | title=Wells Natural History and Archaeological Society | work=Wells Museum website | url=http://www.wellsmuseum.org.uk/#!untitled/c1yi7 | accessdate=1 September 2016}}</ref> He remained honorary curator of the Wells Museum throughout his life.<ref name=retire>{{cite news|title=King of cave men: Famous mendip explorer retiring from postal service|url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000514/19310314/048/0005|accessdate=31 August 2016|work=Bath Chronicle and Weekly Gazette|date=14 March 1913|page=5}}</ref>

==Later life==
Balch married Ellen Elizabeth Brooks on 28 August 1893 but the marriage ended with her death from cancer in 1896. He married Ellen Elizabeth Seaford, his first wife's cousin, in 1899 and the couple went on to have seven children. The children would regularly help out at the museum and search for archaeological artefacts in fields.<ref name=ODNB />
 
Balch was awarded an honorary [[Master of Arts]] by [[Bristol University]] in July 1927 for devoting his leisure time to exploring and recording the caves of the Mendip Hills, becoming an authority on the subject.<ref>{{cite news|title=Mr. H. E. Balch|url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0001625/19270708/041/0002|accessdate=31 August 2016|work=Shepton Mallet Journal|date=8 July 1927|page=2}}</ref> He retired as postmaster on 31 March 1931,<ref name=retire /> whilst continuing his caving, working as the curator at the museum and giving lectures on archaeology.<ref name=ODNB />

In 1944, Balch was awarded the [[freedom of the City]] of Wells, the achievement he was most proud of. He spent time in retirement as churchwarden for [[Church of St Cuthbert, Wells]]. On 27 May 1958, Balch died at his home in Wells.<ref name=ODNB />

==Bibliography==
* {{cite book | title= The Netherworld of Mendip: Explorations in the Great Caverns of Somerset, Yorkshire, Derbyshire and Elsewhere | last2= Baker|first2= Ernest Albert | first1= Herbert E. | last1= Balch | publisher= Baker/Simpkins, Marshall, Hamilton | location= Kent | year= 1907}}
* {{cite book| title= Wookey Hole; Its Caves and Cave Dwellers | first= Herbert E. | last= Balch |author2=Dawkins, William Boyd | publisher= Oxford University Press | year= 1914}}
* {{cite book | title= The Caves of Mendip | last= Balch | first= Herbert E. | publisher= Folk Press | location= London | year= 1926}}
* {{cite book | title= Mendip-the Great Cave of Wookey Hole | last= Balch | first= Herbert E. | publisher= Clare, Son & Co. | location=  Wells | year= 1929}}
* {{cite book | title= Mendip-Cheddar, its Gorge and Caves | last= Balch | first= Herbert E. | publisher= Clare, Son & Co. | location=  Wells | year= 1935}}
* {{cite book | last= Balch | first= Herbert E. | title= Mendip : its swallet caves, and rock shelters  | location= Wells | publisher= Clare, Son & Co. | year= 1937}}

==References==
{{reflist|30em}}

{{Authority control}}

{{DEFAULTSORT:Balch, Herbert E.}}
[[Category:1869 births]]
[[Category:British cavers]]
[[Category:British speleologists]]
[[Category:People from Wells, Somerset]]
[[Category:1958 deaths]]