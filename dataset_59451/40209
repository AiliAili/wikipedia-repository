{{Infobox professional wrestler
|name       = Jon Bolen
|image      = Bolen_Mohawk.jpg
|caption    =
|birth_name =Jon Bolen
|names      = John Bolen<ref name=OWOW>{{cite web|url=http://www.onlineworldofwrestling.com/profiles/j/jon-bolen.html|title=OWOW profile}}</ref><br />'''Jon Bolen'''<ref name=OWOW/><br />'''War Child'''
<!-- Please don't change the height or weight. These are the measures as officially stated and they should not be changed. -->
|height      = {{height|ft=5|in=10}}<ref name=OWOW/>
|weight      = {{convert|220|lb|kg|abbr=on}}<ref name=OWOW/>
|birth_date  = 
|death_date  = 
|birth_place = [[Uniontown, Pennsylvania]]<ref name=OWOW/>
|death_place = 
|resides     =
|billed      = Uniontown, Pennsylvania<ref name=OWOW/>
|trainer     = [[Scott D'Amore]]<ref name=OWOW/><br />[[Ohio Valley Wrestling]]<ref name=OWOW/>
|debut       = October 16, 2004<ref name=OWOW/>
|retired     =
}}
'''Jon Bolen''' is an [[United States|American]] [[Professional wrestling|professional wrestler]] and [[Bodybuilding|bodybuilder]]. He currently competes on the [[independent circuit]] including [[Maximum Pro Wrestling]], the International Wrestling Cartel and the [[National Wrestling Alliance]]. After winning [[Total Nonstop Action Wrestling]]'s "Gut Check Challenge" in 2004, he was briefly signed to the company before then signing a developmental contract with [[World Wrestling Entertainment]] and assigned to its developmental territories [[Ohio Valley Wrestling]] and [[Deep South Wrestling]]. While in Ohio Valley Wrestling, he was one half of High Dosage with "Silverback" [[Ryan Reeves]].

==Early life==
Born in [[Uniontown, Pennsylvania]], Bolen became involved in weightlifting and football as a teenager. Attending [[California University of Pennsylvania]], Bolen played football as a two-time All-Conference and [[All-American]] [[nose guard]] and two-time defensive captain. Although tearing three [[ligaments]] in his knee breaking his [[tibula]] and [[fibula]], he spent only a year in rehab and continued playing during his senior year completing the season without missing a single game.

==Bodybuilding==
After graduation, he continued bodybuilding and eventually set a state bench record in the {{convert|275|lb|abbr=on}}. weight class at {{convert|630|lb|abbr=on}}. At a state power lifting meet, he attempted to qualify for the [[Arnold Classic]] by benching {{convert|700|lb|abbr=on}}. Although coming two inches of locking out the qualifying limit, he did manage to break his own record at {{convert|670|lb|abbr=on}}. During the meet, he met former [[Gold's Gym|Gold's Gym Venice]] owner Ed Conners who had happened to be in town that evening to attend a bodybuilding show. During this meeting, Conners encouraged Bolen to participate in the "Gut Check Challenge", a talent recruitment event organized by Total Nonstop Action Wrestling in conjunction with [[TrimSpa]] as part of the Show of Strength Pro Championship weekend.

==Professional wrestling career==

===Total Nonstop Action Wrestling===
Bolen accepted Conners' offer to fly him to [[Atlanta]], [[Georgia (U.S. state)|Georgia]] and eventually won the "[[TNA Gut Check|Gut Check Challenge]]" on October 16, 2004. The event saw entrants rated on the basis of their performance in five categories: "back bump", "mat techniques", "ring interviews", "running the ropes" and "squat challenge". The male and female events were won by Bolen and [[Jaime Dauncey]] respectively, with Bolen and Dauncey rewarded with a $4,000 USD cash prize and a subsequent appearance on TNA programming.<ref>{{cite web|url=http://www.jaimed.net/news.htm|title=Jaime D in Physical Magazine|date=2004-12-14|publisher=JaimeD.net}}</ref><ref>{{cite web|url=http://www.physicalmag.com/index.php?p=9&a=2952 |title=December 2004: Special Report|date=December 2004|publisher=Physical Online}}</ref> During the next two months, he trained under [[Scott D'Amore]] for three hours a day and five days a week and began appearing at house shows after an 8-week period.<ref name="Dombroski, Joe">{{cite web |url=http://www.iwcwrestling.com/interrogation_bolen.html|title=Interrogation with Jon Bolen |author=Dombroski, Joe|publisher=IWCwrestling.com|archiveurl=https://web.archive.org/web/20060530115257/http://www.iwcwrestling.com/interrogation_bolen.html|archivedate=2006-05-30 }}</ref>

Bolen appeared several times in TNA during 2005, including an appearance at [[TNA Turning Point#2005|Turning Point]] on December 11, where he teamed with [[Buck Quartermain]] and Joe Doering in losing effort to [[Lance Hoyt]] and [[The Naturals]] ([[Andy Douglas (wrestler)|Andy Douglas]] and [[Chase Stevens]]) in a six-man tag team match. In later appearances, he was utilized as a [[Job (professional wrestling)#Jobbers|preliminary wrestler]] during his later appearances with the promotion.

===World Wrestling Entertainment===

====Ohio Valley Wrestling====
Appearing on the undercard of the final episode of ''[[WWE Velocity]]'', he and [[Corey Graves|Sterling James Keenan]] had a WWE tryout match losing to [[Kid Kash]] and [[Jamie Noble]]. Bolen eventually signed a developmental contract with World Wrestling Entertainment in July 2006 and assigned to Ohio Valley Wrestling, a WWE developmental territory based in [[Louisville, Kentucky]]. On July 27, Bolen made his OVW debut losing to [[Elijah Burke]] at a house show. Two days later at an event at [[Six Flags Kentucky Kingdom]], he teamed with [[Damien Sandow|Aaron Stevens]], Rod Steel and Eddie Craven in an 8-man tag team match losing to Chet the Jet, [[Brent Albright]], [[Cody Rhodes|Cody Runnels]] and [[Shawn Osborne]]. On August 11, he participated in a 15-man battle royal which included Shawn Osborne, Elijah Burke, Eddie Craven III, [[Pat Buck]], [[Mike Kruel]], [[Shad Gaspard]], Lennox Lightfoot, Ranger Milton, [[JTG|Neighborhoodie]], [[Johnny Punch]], [[Seth Skyfire]], Rod Steele, [[Gregg Groothuis|Jack Bull]], [[Los Locos]] (Ramón and Raúl) and [[Deuce 'n Domino|The Untouchables]] (Deuce (wrestler)|Deuce Shade]] and [[Cliff Compton|Dice Domino]]).<ref>{{cite web|url=http://www.prowrestlinghistory.com/supercards/usa/misc/ovw/sixflags.html |title=Ohio Valley Wrestling: Six Flag Shows |year=2007 |publisher=ProWrestlingHistory.com |deadurl=yes |archiveurl=https://web.archive.org/web/20071009170422/http://www.prowrestlinghistory.com/supercards/usa/misc/ovw/sixflags.html |archivedate=2007-10-09 |df= }}</ref>

In September, Bolen formed a [[tag team]] with [[Ryan Reeves]] called High Dosage.<ref>{{cite web|url=http://www.cagematch.de/?id=28&nr=447|title=High Dosage's Cagematch profile}}</ref> On January 10, 2007, High Dosage received their first and only opportunity at [[OVW Southern Tag Team Championship|Southern Tag Team Championship]], which they lost to defending champions [[Cody Rhodes|Cody Runnels]] and [[Shawn Spears]].<ref>{{cite web|url=http://www.cagematch.de/?id=28&nr=447&view=matches#matches|title=High Dosage's match listing}}</ref> The team ended soon afterwards when Reeves was released from his WWE developmental contract on January 19, 2007.

On January 20, Bolen teamed up with Katie Lea to defeat Domino and Cherry in a mixed tag team match in [[Hillview, Kentucky]]. He would again team with Katie Lea in a handicap mixed tag team match against Cherry and [[Deuce 'n Domino]] on January 24. The following night, he and [[Ana Rocha|Ariel]] defeated [[Damien Sandow|Idol Stevens]] and [[Beth Phoenix]]. Bolen would remain in OVW only a brief time before his final television appearance in the promotion teaming with T.J. Dalton against Charles "The Hammer" Evans and [[Justin LaRouche|Justin "The Ox" LaRoche]] on January 31.

====Deep South Wrestling and departure====
By mid-February, Bolen appeared in Deep South Wrestling, teaming with Shawn Osborne in a losing effort against [[Freakin' Deacon]] and G-Rilla on February 15. Defeating [[Heath Miller]] in a dark match several days later, Bolen continued to team with Osborne, although the two were less than successful. On March 16, he also lost to [[Kofi Kingston]] at a DSW event in [[Atlanta, Georgia]].<ref>{{cite web |url=http://www.georgiawrestlinghistory.com/news-notes/2007/03/20.html |title=News and Notes: March 20, 2007|date=2007-03-20|publisher=GeorgiaWrestlingHistory.com}}</ref> In his final appearance, he and Osborne defeated Robert Anthony and Johnny Curtis at the DSW Arena in [[McDonough, Georgia]] on April 5.<ref>{{cite web |url=http://www.wrestleview.com/news2006/1177355311.shtml |title=Deep South Wrestling 4/15 TV report & full 4/21 NWA Anarchy Report|author=Goodman, Larry |author2=Adam Martin |date=2007-04-23|publisher=WrestleView.com}}</ref> Bolen was then released the following day.<ref>{{cite web |url=http://www.wwe.com/inside/news/354952824 |title=Inside WWE: Developmental talents released|date=2007-04-06|publisher=[[WWE.com]]}}</ref>

===Return to the independent circuit===
On June 9, 2007, Bolen won the NWA Upstate Kayfabe Dojo Championship after defeating [[Colin Delaney|Colin Olsen]], [[The Miracle Ultraviolence Connection|Cloudy]] and then-champion JP Black.<ref>{{cite web|url=http://www.nwanewyork.com/ResultsContinuingTradition07.html|title=Continuing The Tradition show results}}</ref> Beginning in 2008, the Kayfabe Dojo Title was retired without Bolen ever having lost it.<ref name=Kayfabe>{{cite web|url=http://www.wrestling-titles.com/us/ny/upstate/nwa-upstate/nwa-upstate-kd.html|title=NWA Upstate Kayfabe Dojo Championship history}}</ref> During the summer, he made his return to the International Wrestling Cartel, making a surprise appearance at the supercard ''Super Indy VI'' interfering on behalf on former Heavyweight Champion [[Ricky Reyes]], who was being attacked by Dennis Gregory and [[Brent Albright]]. Although scheduled to face Albright the following month at ''Summer Sizzler 4: Checkmate'', Albright was unable to make the event due to "travel issues" and Bolen instead faced [[Chris Hero]], who he defeated.

At the ''2007 Ellsworth Basebrawl'', Bolen was to team with former tag team partner [[Corey Graves|Sterling James Keenan]] against War Machine (Brent Albright and Sebastian Dark). However, Keenan ended up defeating Albright in an Ellsworth Street Fight after Bolen pulled out of the match.<ref>{{cite web |url=http://www.1wrestling.com/news/newsline.asp?news=29511 |title=The Indy Buzz - IWC Pro Wrestling |accessdate= |author= |authorlink= |date=2007-07-30 |format= |work= |publisher=1wrestling.com |pages= |language= |quote= }}{{dead link|date=September 2016|bot=medic}}</ref>

After being seen at many IWC events in the past several months, Bolen made his official return at the Promotional Consideration Paid for by the Following championship tournament as a color commentator.

In March 2009, Bolen was featured on all three shows of the inaugural tour of [[Scott D'Amore]]'s new promotion [[Maximum Pro Wrestling]] in Ontario.

In late 2013, Bolen began wrestling under the name "Warchild" as a part of Krimson's 'Dead Wrestling Society' stable.

==In wrestling==
*'''Finishing moves'''
:*''Meltdown'' ([[Professional wrestling throws#Pumphandle slam|Pumphandle slam]])<ref name=OWOW/>

*'''[[Music in professional wrestling|Entrance themes]]'''
**"Death Before Dishonor" by [[Five Finger Death Punch]]

==Championships and accomplishments==
*'''[[Border City Wrestling]]'''
**[[BCW Can-Am Heavyweight Championship|BCW Heavyweight Championship]] (1 time)<ref>{{cite web|url=http://www.cagematch.net/?id=5&nr=88|title=BCW Heavyweight Championship history}}</ref>
**[[BCW Can-Am Tag Team Championship|BCW Tag Team Championship]] (1 time) – with [[Johnny Devine]]<ref>http://www.cagematch.net/?id=2&nr=2727&page=11</ref>
*'''[[National Wrestling Alliance|NWA Upstate]]'''
:*NWA Upstate Kayfabe Dojo Championship (1 time)<ref name=Kayfabe/><ref>{{cite web|url=http://www.cagematch.net/?id=5&nr=584|title=NWA Upstate Kayfabe Dojo Championship history}}</ref>
*'''[[Pro Wrestling Illustrated]]'''
:*Ranked #'''425''' of the top 500 singles wrestlers in the [[Pro Wrestling Illustrated#PWI 500|PWI 500]] in 2006<ref>http://www.cagematch.net/?id=2&nr=2727&page=12</ref>
*'''Pro Wrestling Rampage'''
:*PWR Tag Team Championship (1 time) – with [[Conrad Kennedy III|Krimson]]<ref>{{cite web|url=http://www.cagematch.net/?id=5&nr=1240|title=PWR Tag Team Championship history}}</ref>

==References==
<references/>

==External links==
*[http://www.onlineworldofwrestling.com/profiles/j/jon-bolen.html Profile at Online World of Wrestling]
*[https://web.archive.org/web/20091027035327/http://geocities.com/swcbios/jonbolen  Super Wrestling Center - Jon Bolen]
{{BCW Can-Am Heavyweight Championship}}

{{DEFAULTSORT:Bolen, Jon}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American male professional wrestlers]]
[[Category:Professional wrestlers from Pennsylvania]]