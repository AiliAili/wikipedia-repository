{{Infobox Journal
| title        = Journal of Interdisciplinary History
| cover        = [[Image: joihlowres.gif]]
| editor       = [[Robert I. Rotberg]] and [[Theodore K. Rabb]]
| discipline   = [[History]]
| language     = English
| abbreviation = J. Interdiscipl. Hist.
| publisher    = [[MIT Press]]
| country      = United States
| frequency    = Quarterly
| history      = 1969–present
| openaccess   = 
| impact       = 0.727
| impact-year  = 2014
| website      = http://www.mitpressjournals.org/loi/jinh
| link1        = http://www.mitpressjournals.org/toc/jinh/current
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 00221953
| OCLC         = 1799976
| LCCN         = 
| CODEN        = 
| ISSN         = 0022-1953 
| eISSN        = 1530-9169
}}
The '''''Journal of Interdisciplinary History''''' is a [[peer-reviewed]] [[academic journal]] published four times a year by the [[MIT Press]]. It covers a broad range of historical themes and periods, linking history to other academic fields.

==Contents==
The journal features articles, review essays and book reviews, linking history with other fields of academic research, such as economics and demographics. Unlike most other historical journals, the content is not limited to one geographical area or historical period, and covers [[social history|social]], [[demographic history|demographic]], [[political history|political]], [[economic history|economic]], [[cultural history|cultural]] and technological history.

Each issue has 200 pages.

==Editors==
Since its inception, the ''Journal of Interdisciplinary History'' has been edited by [[Robert Rotberg]] and [[Theodore Rabb]].

#6 in History via the 2014 Thomson Reuters Journal Citation Reports.

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.727, ranking it 6th out of 87 journals in the category "History".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: History |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
* [http://www.mitpressjournals.org/loi/jinh Official website]

[[Category:English-language journals]]
[[Category:History journals]]
[[Category:MIT Press academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1969]]


{{history-journal-stub}}