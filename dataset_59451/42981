<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Ryan STA
 | image=Historial Ryan STA.jpg
 | caption=The prototype Historical Ryan STA on display
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Historical Aircraft Corporation]]
 | designer=
 | first flight=
 | introduced=1997
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=One
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] (date) -->
 | developed from= [[Ryan STA]]
 | variants with their own articles=
}}
|}
The '''Historical Ryan STA''' was an [[United States|American]] [[homebuilt aircraft]] that was designed and produced by the [[Historical Aircraft Corporation]] of [[Nucla, Colorado]]. The aircraft was an 85% scale replica of the original [[Ryan STA]] and when it was available was supplied as a kit for amateur construction.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 178. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

==Design and development==
The aircraft featured a [[Flying wires|wire-braced]] and [[strut-braced]] [[low-wing]], two seats in separate [[tandem]] open cockpits with windshields, fixed [[conventional landing gear]] and a single engine in [[tractor configuration]].<ref name="Aerocrafter" />

The aircraft [[fuselage]] was made from welded steel tubing, while the wings had wooden [[Spar (aviation)|spars]] and [[Rib (aircraft)|ribs]] covered in [[Aircraft dope|doped]] [[aircraft fabric]]. Several prefabricated components were supplied as part of the kit, including the fuselage frame, [[landing gear]], engine mount and tail assembly. The manufacturer rated the STA kit as suitable for beginners and estimated the construction time from the supplied kit as 1500&nbsp;hours. The kit could be completed to represent an STA, a military [[Ryan ST|PT-16]] or a [[Ryan ST|PT-20]].<ref name="Aerocrafter" />

The aircraft's {{convert|26.00|ft|m|1|abbr=on}} span wing had an area of {{convert|112.6|sqft|m2|abbr=on}}. The cockpit width was {{convert|22|in|cm|abbr=on}} and the standard engine used was the {{convert|100|hp|kW|0|abbr=on}} [[CAM 100]] powerplant. It had a typical empty weight of {{convert|725|lb|kg|abbr=on}} and a gross weight of {{convert|1275|lb|kg|abbr=on}}, giving a useful load of {{convert|500|lb|kg|abbr=on}}. With full fuel of {{convert|23|u.s.gal}} the payload for pilot, passenger and baggage was {{convert|362|lb|kg|abbr=on}}.<ref name="Aerocrafter" />

==Operational history==
In January 2014 no examples remained [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]]. Although one aircraft had been registered in 1997, it was listed as destroyed and deregistered in 2002. It is likely that no examples exist today.<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=498|title = Make / Model Inquiry Results|accessdate = 2 January 2014|last = [[Federal Aviation Administration]]|date = 2 January 2014}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (STA) ==
{{Aircraft specs
|ref=AeroCrafter<ref name="Aerocrafter" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=20.10
|length in=
|length note=
|span m=
|span ft=26.00
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=112.6
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=725
|empty weight note=
|gross weight kg=
|gross weight lb=1275
|gross weight note=
|fuel capacity={{convert|23|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[CAM 100]] 
|eng1 type= four cylinder [[four stroke]] [[aircraft engine]]
|eng1 kw=
|eng1 hp=100

|prop blade number=2
|prop name=fixed pitch wooden
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=125
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=110
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=53
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=490
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=16300
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=930
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=11.3
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

<!-- ==External links== -->
{{Historical Aircraft Corp aircraft}}

[[Category:Historical Aircraft Corp|Ryan STA]]
[[Category:United States sport aircraft 1990–1999]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Homebuilt aircraft]]
[[Category:Replica aircraft]]