{{Distinguish|Dakota (UK band)}}
{{Infobox musical artist
| name            = Dakota
| image           = Dakota Marketing Banner.jpg
| alt             =
| caption         = Website Banner for Dakota Website
| image_size      = 
| background      = group_or_band
| alias           =
| origin          = USA
| genre           = [[Album-oriented rock|AOR]]
| years_active    = 1979–1987, 2014–Current
| label           = [[Columbia Records (record label)|Columbia Records]], [[MCA Records(record label)|MCA Records]]
| associated_acts = The Buoys, Jerry-Kelly
| website         = http://www.dakotarockband.com
| current_members = Jerry Hludzik, Bill Kelly, John Lorance, Eli Hludzik
}}

'''Dakota''' is a US based melodic rock group active during the late 1970s and early 1980s.<ref name="dakota_official_website">http://www.dakotarockband.com/#!band/c10fk</ref><ref name="jerry_kelly_website">http://www.dakotajerrykelly.com/The%20Last%20Standing%20Man/mca.htm</ref><ref name="tribune_article">http://m.thetimes-tribune.com/lifestyles/reunited-dakota-to-release-new-album-the-long-road-home-in-september-1.1732721</ref><ref name = connectionsarticle>http://connections-magazine.com/Issue_PDFs/apr09/CMApr09-WEB.pdf</ref> Dakota first achieved moderate success with their 1979 self-titled album ''Dakota'',<ref>http://www.melodic.net/?page=interview&id=53&section=comments</ref> but the band's popularity increased rapidly while performing as the opening act for Queen on their 1980–1981 ''[[The Game Tour]]''.<ref name="jerry_kelly_website"/><ref>http://www.queenconcerts.com/support-bands.html</ref> The band released their second and best-selling [[Album-oriented rock|AOR]] album, titled ''Runaway'', in 1984.<ref name="review">http://www.melodic.net/?page=review&id=2293</ref> The band experienced several line-up changes during the 1980s and eventually stopped performing in 1987.<ref name="dakota_official_website"/> During the 1990s and 2000s original member Jerry Hludzik continued to record a number of records under the Dakota name, featuring guest musicians, friends and also his son Eli Hludzik. In 2014, original members Jerry Hludzik and Bill Kelly reunited and teamed up with Jon Lorance and Eli Hludzik to record a new Dakota album titled ''Long Road Home'', which was released on October 30, 2015.<ref name="dakota_official_website"/><ref name="jerry_kelly_website"/><ref name="tribune_article"/>

== Origins ==

The origins of Dakota can be traced back as far as the early 1970s in North East Pennsylvania, USA, to a band called [[The Buoys]] and a million-selling single called ''[[Timothy (song)|Timothy]]''.<ref name="dakota_official_website"/><ref>http://standardspeaker.com/news/how-a-song-about-cannibalism-will-forever-be-linked-to-mining-disaster-1.1534003</ref> The song, written by [[Rupert Holmes]], was supposedly about the local Sheppton mine disaster of 1963 <ref>http://www.songfacts.com/detail.php?id=2005</ref> and reached the Billboard Top 40 chart on April 17, 1971, remaining on the chart for eight weeks, peaking at #17, as listed in The Billboard Book of Top 40 Hits by [[Joel Whitburn]]. The Buoys was spearheaded by lead singer and guitarist Bill Kelly and Jerry Hludzik on guitar and supporting vocals.<ref name="dakota_official_website"/><ref>http://www.discogs.com/artist/279804-Buoys-The</ref>

In 1978 Bill and Jerry left the group and teamed up as the Jerry-Kelly band, making use of members from popular local bands. That same year the band was signed to [[Epic Records]] to record ''Somebody Else's Dream'',<ref>http://www.escape-music.com/ESM023.htm</ref> the first and only Jerry-Kelly album.<ref>http://www.westcoast.dk/artists/j/jerry-kelly-band/</ref> By 1979, with the band's next scheduled release due, Columbia records insisted that the band change its name and after several options were discussed ''Dakota'' was chosen.<ref name="dakota_official_website"/><ref name= "interview">http://www.hardnheavy.it/sito/Interviste/Dakota/01Dakota_eng.html</ref> Jerry-Kelly drummer, Gary Driscoll, left the band in early 1980, leaving the band without a drummer.<ref name="dakota_official_website"/> Producers [[Danny Seraphine]] and Hawk Wolinski drafted in [[Rufus (band)|Rufus]] drummer, John Robinson, and produced Dakota's first self-titled album ''Dakota'', with the track ''If It Takes All Night'' becoming the band's first single to receive regional airplay, peaking at #78 on the Billboard Charts in 1980.<ref name="dakota_official_website"/><ref name= "interview"/>

== ''The Game Tour'' (1980) ==

Long-time friend of the Bill and Jerry, Michael Stahl, who was working with Queen on their 1980 ''The Game Tour'', managed to secure Dakota as the opening act for Queen on their 35-date tour.<ref name="jerry_kelly_website"/><ref name="interview"/><ref name="interview"/> Dakota's lineup now consisted of Jerry Hludzik and Bill Kelly on guitars and vocals, Bill McHale on bass, Jeff Mitchell on keyboards, Lou Cossa on keys and guitar and new drummer Tony Romano. During the tour a feud between Columbia Records and the band [[Chicago (band)|Chicago]] developed and Dakota's association with Chicago founding member, Danny Seraphine (who produced their first single), led to the band receiving lackluster backing from Columbia Records for the remainder of their time at the label.<ref name="dakota_official_website"/>

== ''Runaway'' (1984) ==

After the 1980–1981 ''The Game Tour'' was over, and without tour support, the band lost several members, including Bill McHale, Jeff Mitchell, Lou Cossa and Tony Romano.<ref name="dakota_official_website"/><ref name="musicreview">http://www.itsaboutmusic.com/dakota.html</ref> Auditions for replacements were held during January and February 1982. Keyboard player and guitarist Rick Manwiller, previously from progressive rock band Steph, joined early that year.<ref>http://www.discogs.com/artist/1024248-Rick-Manwiller</ref> After working on new material during 1982, Dakota traveled to Los Angeles in early 1983 to record their second album, titled ''Runaway'' (MCA-5502) on the MCA label, engineered by [[Humberto Gatica]] and produced by Danny Seraphine. The lineup consisted of Jerry Hludzik and Bill Kelly on guitars and vocals, Danny Seraphine on drums and Rick Manwiller on keys. Several guest and session artists also appeared on the album, including guitarists [[Richie Zito]] and Paul Jackson, bassist Neal Steubenhouse, Rolling Stones sax-player [[Ernie Watts]], Chicago vocalist [[Bill Champlin]], Chicago keyboardist [[Robert Lamm]] and Toto keyboardist [[Steve Porcaro]].<ref name="dakota_official_website"/> ''Runaway'' was released in 1984, but in absence of promotional- and tour backing from Columbia Records the album did not achieve the chart success the band anticipated.<ref name="dakota_official_website"/><ref name="musicreview"/>

In 2002 ''Runaway'' was re-released on compact disc and in digital format and soon garnered acclaim from critics and fans alike as one of the most underrated AOR records of the 1980s.<ref name="review"/><ref name="review"/> In 2011 Swedish based band ''Houston'' covered the title track from the ''Runaway'' album for inclusion on their ''Relaunch'' album. The single was featured in the Red Bull short film ''Danny MacAskill's Imaginate'' in 2013.<ref>http://imaginate.redbull.com/</ref>

== Hiatus (1987) ==

Due to the strained relations with Columbia Records, as well as personal differences between band members, Dakota played their final performance of the 1980s at Scranton's Montage Amphitheater in 1987 in front of roughly 17,000 fans.<ref name="dakota_official_website"/> The band stopped performing, while band members pursued personal projects. During the 1990s and 2000s original member Jerry Hludzik continued to record three more records under the Dakota name,<ref>http://www.rockreport.be/review.asp?id=590</ref><ref>http://www.melodic.net/?page=review&id=2045</ref><ref>http://www.melodic.net/?page=review&id=20289</ref> featuring a variety of session musicians, friends and also his son Eli Hludzik, achieving success in Europe and Japan.<ref name="dakota_official_website"/>

== Reunion (2014) ==

In 2014 original members Jerry Hludzik and Bill Kelly reunited and teamed up with Jon Lorance and Eli Hludzik to record a new Dakota album titled ''Long Road Home'', which was released on 30 October 2015.<ref name="dakota_official_website"/><ref name="tribune_article"/>

== Discography ==

=== Studio Albums ===
* 1980: Dakota
* 1984: Runaway
* 1996: Mr Lucky
* 1997: The Last Standing Man
* 2000: Little Victories
* 2003: Deep 6
* 2015: Long Road Home

=== Compilations ===
* 2000: Three Live Times Ago... 20th Anniversary Limitied Edition

== References ==

{{reflist}}

== Further reading ==
* Nash, Bruce, and Allan Zullo. ''The Wacky Top 40: The Most Outrageous, Hilarious, and Unforgettable Songs in Pop History''.  [[Holbrook, Massachusetts]]: Bob Adams, Inc., 1993
* Chisak, Richard. Is There Life After the Garden? The Dakota-Queen Tour. Timothy. March, 1981
* ''AOR'', Vincenzo Ferrara, 2008, Boopen s.r.l
* ''I 100 Migliori Dischi AOR'', 2013, Christiano Canau & Gennaro Dleo, ISBN 9788896-131572
* [http://connections-magazine.com/Issue_PDFs/apr09/CMApr09-WEB.pdf Connections Magazine Article Page 44]
* [http://www.songfacts.com/detail.php?id=2005 "Timothy" on Songfacts.com]
* [http://www.rupertholmes.com/home.html Rupert Holmes website]

[[Category:American rock music groups]]