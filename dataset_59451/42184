<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=EOS 001
 | image=Airmotive Engineers EOS 001.JPG
 | caption=The prototype EOS 001
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Airmotive Engineers]]
 | designer=
 | first flight=
 | introduced=1978
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=At least two
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]]2,000 (kit price, 1978) -->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Airmotive EOS 001''' is an [[United States|American]] [[homebuilt aircraft]] that was designed and produced by [[Airmotive Engineers]] of [[Pontiac, Michigan]]. The aircraft was supplied as a kit for amateur construction, but is no longer available.<ref name="P&P">Plane and Pilot: ''1978 Aircraft Directory'', page 140. Werner & Werner Corp, Santa Monica CA, 1977. ISBN 0-918312-00-0</ref>

==Design and development==
The aircraft features a cantilever [[low-wing]], a single-seat, enclosed open cockpit under a [[bubble canopy]], retractable [[tricycle landing gear]] and a single engine in [[tractor configuration]].<ref name="P&P" /><ref>{{cite journal|title=none|magazine=Air Progress|date=November 1978|page=29}}</ref>

The aircraft is made from [[Adhesive|bonded]] and [[pop rivet]]ed [[aluminum]] sheet and has a {{convert|26.0|ft|m|1|abbr=on}} span wing. Optimized for simplicity of construction, the design has no compound curves and requires no [[Jig (tool)|jig]]s or special tools. The standard engine intended to be supplied with the kit was the {{convert|55|hp|kW|0|abbr=on}} [[two-stroke]] [[Hirth 2702]] powerplant with a reduction gearbox that allowed the propeller to operate at lower tip speeds. This engine permits the EOS 001 to cruise at {{convert|187|mph|km/h|0|abbr=on}} and achieve a top level speed of {{convert|200|mph|km/h|0|abbr=on}}. The prototype was equipped with a [[Volkswagen air-cooled engine]].<ref name="P&P" /><ref name="FAAReg2">{{cite web|url = http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=10AE|title = N-Number Inquiry Results - N10AE|accessdate = 10 October 2013|last = [[Federal Aviation Administration]]|date = 10 October 2013}}</ref>

The aircraft has an empty weight of {{convert|320|lb|kg|abbr=on}} and a gross weight of {{convert|750|lb|kg|abbr=on}}, giving a useful load of {{convert|430|lb|kg|abbr=on}}. With full fuel of {{convert|20|u.s.gal}} the payload is {{convert|310|lb|kg|abbr=on}}.<ref name="P&P" />

In 1978 the claimed completion cost was [[US$]]2,000, not counting labor.<ref name="P&P" />

==Operational history==
The prototype EOS 001 was [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]] (FAA) in 1978.<ref name="FAAReg2" />

By October 2013 two examples, including the prototype, had been at one time registered in the United States with the FAA, but both registrations were later canceled and it is likely that no examples of this type exist today.<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=EOS+001&PageNo=1|title = Make / Model Inquiry Results|accessdate = 9 October 2013|last = [[Federal Aviation Administration]]|date = 9 October 2013}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (version) ==
{{Aircraft specs
|ref=Plane and Pilot<ref name="P&P" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=16
|length in=1
|length note=
|span m=
|span ft=26
|span in=0
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=320
|empty weight note=
|gross weight kg=
|gross weight lb=750
|gross weight note=
|fuel capacity={{convert|20|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hirth 2702]]
|eng1 type=twin cylinder, air-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=55<!-- prop engines -->

|prop blade number=2
|prop name=fixed pitch
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=200
|max speed kts=
|max speed note=
|cruise speed kmh=187
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=52
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=2160
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
*[https://www.flickr.com/photos/tedkoston/4841749360/ Photo of an EOS 001]

[[Category:Airmotive Engineers aircraft|EOS 001]]
[[Category:United States sport aircraft 1970–1979]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Homebuilt aircraft]]