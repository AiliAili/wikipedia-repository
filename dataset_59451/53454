{{Refimprove|date=May 2008}}

'''Construction accounting''' is a form of [[project accounting]] applied to construction projects.  See also [[production accounting]].  Construction accounting is a vitally necessary form of accounting, especially when multiple [[contracts]] come into play. The construction field uses many terms not used in other forms of accounting, such as "draw" and [[progress billing]].  Construction accounting may also need to account for vehicles and equipment, which may or may not be owned by the company as a [[fixed asset]]. Construction accounting requires [[invoicing]] and [[vendor]] [[payment]], more or less as to the amount of [[business]] done.

In the United States, the authoritative literature on Construction accounting is AICPA Statement of Position SOP 81-1 (SOP 81-1).

== Construction Costs ==
These are all cost related to construction process, right from materials, labor costs, consultancy and all management expenses.
Construction accounting involves charging construction costs to the applicable contract.  Costs fall into three categories. Direct costs are labor, material, and subcontracting costs,land.<ref>AICPA SOP 81-1 Para. 72 a.</ref>  Indirect costs include indirect labor, supervision, tools, equipment costs, supplies, insurance, and support costs.<ref>AICPA SOP 81-1 Para. 72 b.</ref>  Selling, general and administrative costs, are generally excluded from contract costs.<ref>AICPA SOP 81-1 Para. 72 c,d.</ref>

== Revenue Recognition ==

Construction accounting requires unique revenue recognition rules for contracts in progress.

In most cases, revenue is recognized using the Percentage of Completion Method.  Under this method, revenue is recognized using an estimate for the overall anticipated profit for a particular contract multiplied by the estimated percent complete of that contract.<ref>AICPA SOP 81-1 Para. 22-25</ref>  This involves the inherent risk of relying upon estimates.<ref>SOP 81-1 Para. 26-29</ref>

Under SOP 81-1, revenue is also allowed to be computed using the Completed Contract Method.<ref>SOP 81-1 Para. 30</ref>  Under this method, contract revenues and costs are not recognized until the contract is substantially complete.  However, this method is not allowable if the results are significantly different than results using the Percentage of Completion Method.<ref>SOP 81-1 Para. 31</ref>  The Completed Contract Method is allowed in circumstances in which reasonable estimates cannot be determined.<ref>SOP 81-1 Para. 32</ref>  However, these types of circumstances can be construed as a lack of internal control.

== References ==

{{reflist}}

[[Category:Management accounting]]


{{accounting-stub}}