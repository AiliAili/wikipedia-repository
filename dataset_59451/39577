{{Infobox journal
| title = The Scientific World Journal
| cover = [[File:The Scientific World Journal.svg|200px]]
| editor =
| discipline = [[Biology]], [[biomedicine]], [[medicine]]
| former_name =
| abbreviation = Scientific World J.
| publisher = [[Hindawi Publishing Corporation]]
| country =
| frequency = Articles published as accepted
| history = 2001–present
| openaccess = Yes
| license =
| website = http://www.tswj.com
| link1 = http://www.tswj.com/contents/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 48386834
| LCCN = 2001215320
| CODEN = THESAS
| ISSN = 2356-6140
| eISSN = 1537-744X
}}
'''''The Scientific World Journal''''' (formerly, ''The ScientificWorldJournal'') is a [[peer-reviewed]] [[scientific journal]] covering fields in the [[Biology|life sciences]] ranging from [[biomedicine]] to [[environmental science]]s. It was established in 2001 and is published by [[Hindawi Publishing Corporation]]. The journal was delisted in the 2015 ''[[Journal Citation Reports]]'' (thus not receiving an [[impact factor]]) because of "anomalous citation patterns".

== Publication history ==
The journal was established in 2001 and is published online and in print.<ref name=history>{{cite web |url=http://www.hindawi.com/journals/tswj/biblio/ |title=Bibliographic Information |format= |work=The Scientific World Journal |accessdate=2015-10-10}}</ref> From 2001 to 2011, the [[editor-in-chief|managing editor]] was [[Graham Lees]]. In 2011, the journal was acquired by [[Hindawi Publishing Corporation]] and converted to [[open access]] and to a model without an [[editor-in-chief]]. According to [[Institute for Scientific Information]] counts, the number of published items in the journal grew steadily from 141 to 227 articles per year between 2008 and 2011 and then increased over five fold to 1,149 articles in 2011.<ref>{{cite book|title=Journal Citation Reports|publisher=ISI Web of Knowledge|edition=2013|accessdate=24 October 2014}}</ref>

== Organization ==
The journal was divided into "domains", each covering a particular subject (e.g., "[[Neuroscience]]") and some of these domains could be joined further into "clusters" (e.g., "Neuroscience, [[Neurology]] & [[Psychiatry]]").<ref name=history/> Each domain had its own set of principal and associate editors, as well as an [[editorial board]].<ref name=editors>{{cite web |url=http://www.tswj.com/editors/ |title=The Scientific World Journal: Editorial board |format= |work= |accessdate=2013-01-05}}</ref> Published articles could be assigned to multiple, different domains, so that an article on, for example, [[Air pollution|air particulates]] could be found easily both by researchers working in [[Environmental science|environmental]] or [[Earth science]]s and biomedical researchers searching for causes of [[asthma]].<ref>{{cite journal |title=TheScientificWorldJOURNAL: sustainability through innovation |journal=Learned Publishing |date=2004-07-01 |first1=Anne V. |last1=Allen |last2=Lees |first2=Graham V. |authorlink2=Graham Lees |volume=17 |issue=3 |pages=183–187 |doi=10.1087/095315104323159595 |url=http://www.ingentaconnect.com/content/alpsp/lp/2004/00000017/00000003/art00002 |format= |accessdate=2013-01-05}}</ref> After the journal was taken over by Hindawi, it was reorganized according to a more traditional model with more rigid subject sections. However, the journal has no formal editor-in-chief any more and decisions regarding acceptance or rejection of a manuscript appear to be made by majority vote from a mix of external reviewers and editors (whose identity remains confidential unless a manuscript is accepted for publication).<ref>{{cite web |url=http://www.tswj.com/workflow/ |title=Editorial Workflow |publisher=Hindawi Publishing Corporation |work=The Scientific World Journal |accessdate=2013-01-05}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name="indexing">{{cite web |url=http://www.tswj.com/ai/ |title=Indexing and abstracting information |publisher=Hindawi Publishing Corporation |work=The Scientific World Journal |accessdate=2013-01-05}}</ref>
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Academic Search|Academic Search Complete]]
* [[Aquatic Sciences and Fisheries Abstracts]]
* [[CAB Direct (database)|CAB Abstracts]]
* [[Chemical Abstracts]]
* [[CINAHL|CINAHL Plus]]
* [[EBSCO Information Services|EBSCO databases]]
* [[Embase]]
* [[CAB Direct (database)|Global Health]]
* [[Inspec]]
* [[MEDLINE]]/[[PubMed]]
* [[Meteorological & Geoastrophysical Abstracts]]
* [[ProQuest|ProQuest databases]]
* [[Scopus]]
* [[Zentralblatt MATH]]
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2013 [[impact factor]] of 1.219.<ref name=WoS>{{cite book |year=2014 |chapter=The Scientific World Journal |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> However, the journal did not receive an impact factor over 2011, nor was it listed in the 2015 ''Journal Citation Reports'' because of "anomalous citation patterns".<ref>{{cite web |title=The Scientific World Journal Will Lose Its Impact Factor — Again |work=The Scholarly Kitchen |authorlink=Jeffrey Beall |first1=Jeffrey |last1=Beall |url=http://scholarlyoa.com/2014/10/14/the-scientific-world-journal-will-lose-its-impact-factor-again/|accessdate=18 October 2014}}</ref>

== References ==
{{Reflist|colwidth=30em}}

== External links ==
* {{Official website|http://www.tswj.com/}}

{{DEFAULTSORT:Scientific World Journal, The}}
[[Category:Biology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2001]]
[[Category:General medical journals]]
[[Category:Open access journals]]
[[Category:Hindawi Publishing academic journals]]