{{italic title}}
{{Infobox journal
| cover        = [[File:CEjan2010cover.jpg]]
| editor       = Kelly Ritter
| discipline   = English language arts for college teachers
| abbreviation = CE
| publisher    = [[National Council of Teachers of English]]
| country      = United States
| frequency    = bimonthly
| history      = 1939-present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = http://www.ncte.org/journals/ce
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 1564053
| LCCN         = 2002-227121
| CODEN        = 
| ISSN         = 0010-0994
| eISSN        = 
}}
'''''College English''''' is an official publication of the American [[National Council of Teachers of English]] and is aimed at college-level teachers and scholars of English. The [[Peer review|peer-reviewed]] journal publishes articles on a range of topics related to the teaching of English language arts at the college level, including literature, rhetoric, critical theory, and pedagogy.<ref>{{cite web |url=http://www.ncte.org/journals/ce/write |title=CE Submission Guidelines |accessdate=May 29, 2009}}</ref> It sometimes publishes special issues devoted to specific themes. Its content is accessible electronically via [[ERIC]], [[ProQuest]], and [[JSTOR]], and is indexed by the [[Modern Language Association|MLA]].

==History==
''College English'' began in 1939 when it was spun off from ''[[The English Journal]]''.<ref>{{cite web |url=http://writing.colostate.edu/guides/research/rhet-res/pop4c.cfm |title=Writing Guide for ''College English'' |publisher=Colorado State University |accessdate=May 29, 2009 |archiveurl=https://web.archive.org/web/20081211082639/http://writing.colostate.edu/guides/research/rhet-res/pop4c.cfm |archivedate=December 11, 2008}}</ref> Its first editor was W. Wilbur Hatfield, who also edited ''The English Journal''. He continued to edit both publications until 1955.<ref>{{cite book |editor-first=Robert C. |editor-last=Pooley |title=Perspectives on English: Essays to Honor W. Wilbur Hatfield |location=New York |publisher=Appleton-Century-Crofts |year=1960 |page=5}}</ref>

===Editors===
Since its founding in 1939, ''College English'' has had ten editors:

* W. Wilbur Hatfield (1939-1955)<ref name="goggin">{{cite book |title=Authoring a Discipline: Scholarly Journals and the Post-World War II Emergence of Rhetoric and Composition |first=Maureen Daly |last=Goggin |publisher=Routledge |year=2000 |isbn=0-8058-3578-4 |pages=209–210}}</ref>
* Frederick L. Gwynn (1955-1960)<ref name="goggin"/>
* James E. Miller, Jr. (1960-1966)<ref name="goggin"/>
* Richard Ohmann (1966-1978)<ref name="goggin"/>
* Donald Gray (1978-1985)<ref name="goggin"/>
* James C. Raymond (1985-1992)<ref name="goggin"/>
* Louise Z. Smith (1992-1999)<ref name="goggin"/>
* Jeanne Gunner (1999-2006)
* John Schilb (2006-2012)
* Kelly Ritter (2012–present)

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.ncte.org/journals/ce}}

[[Category:American literary magazines]]
[[Category:Language education journals]]
[[Category:English-language education]]
[[Category:Publications established in 1939]]
[[Category:1939 establishments in the United States]]


{{US-lit-mag-stub}}
{{edu-journal-stub}}
{{ling-journal-stub}}