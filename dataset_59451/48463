{{Orphan|date=December 2014}}

{{Use dmy dates|date=December 2014}}
The '''Hermes Road Measurement System''' is the result of the EU-funded project<ref>{{cite web|url=http://www.hermesroadmeasurement.eu/index.html|title=FP7 - Hermes|work=hermesroadmeasurement.eu}}</ref> ''Innovative, Highly Efficient Road Surface Measurement and Control System'' that was undertaken between August 2012 and July 2014. The project was funded through the European Union's Seventh Framework Capacities Programme<ref>{{cite web|url=http://cordis.europa.eu/fp7/capacities/research-sme_en.html|title=European Commission: CORDIS: FP7:Research for the benefit of SMEs|work=europa.eu}}</ref> that aimed at supporting research for the benefit of small-to-medium enterprises (SMEs). This is realised by cooperation between a group of SMEs from several European countries, working in collaboration with a number of research and technological development (RTD) institutes, collectively constituting the project's consortium.

The Hermes system enables both the longitudinal and transversal profiles of a road to be simultaneously measured from a specially equipped vehicle travelling at normal road speeds. The developed approach has the advantage of eliminating the need for an inertial reference level, whilst improving accuracy of measurements by addressing errors otherwise resulting from the dynamics of a moving vehicle. An additional feature of this system is integration of the road profile measurements with their precise geographical location.

== Objectives ==
The project's objectives include the following:
* Development of a highly accurate longitudinal and transversal road measurement methodology.
* Design and implementation of a low-cost embedded system to execute the longitudinal and transversal measurements.
* Implementation of an integrated solution for road profile measurements incorporating state-of-the-art [[Geographic information system|GIS techniques]] for accurate localisation.
* Development of a novel information system for data logging, data extraction and data exchange with existing related systems.
* Performance of numerous field-trials, in countries represented by the project partners, of the complete system in roads having a wide range of characteristics in order to optimise system performance.

== System characteristics ==

=== Measured parameters ===
These include transversal road profile, [[Texture (roads)|macro-texture measurements]], longitudinal profile, rut depth, rut width, cross fall, curve radius, cracks, [[International Roughness Index]] (IRI), {{citation needed|date=December 2014}} mean profile depth, vehicle speed, position relative to the start point, [[GNSS|GNSS position]], together with pavement imaging and time-stamping of measurements.

=== Accuracy ===
* GNSS position horizontal accuracy: ≤0.5m RMS at speed up to 100&nbsp;km/h
* Relative position to start of measurements: ≤0.5% of travelled distance.
* Longitudinal profile vertical resolution: ≤1.5mm.
* Longitudinal profile sampling interval: ≤250mm.
* Reporting interval of longitudinal profiling: ≤500mm.
* Transversal profile vertical resolution: ≤1.5mm.
* Transversal profile sampling interval: ≤350mm.
* Transversal profile repetition interval: ≤10m.
* Reporting interval of transversal profiling: ≤20m.
* Timestamps: ≤5ms.

=== Standards compliance ===
The work has been set to meet a number of requirements that were formed based on the results of questionnaires collected from interested parties. Hence, the following standards are met:
* EN 13036-1 Macrotexture Depth.
* EN 13036-2 Friction.
* EN 13036-3 Horizontal Drainability.
* EN 13036-4 Pendulum Test.
* EN 13036-5 Longitudinal Uneveness.
* EN 13036-6 Transverse Profiles.
* EN 13036-7 Straightedge.
* EN 13036-8 Uneveness.
* EN ISO 11819-1 Statistical Pass-By.
* EN ISO 13473-1 Mean Profile Depth.

== System description ==
The Hermes system comprises a mobile unit, i.e., a special measurement vehicle having laser projection and camera imaging equipment.  Acquired data is logged by a computer using a relational database.  Data is subsequently post-processed using [[Structured Query Language|Structured Query Language-based]] queries, together with sophisticated custom-developed data analysis software.
[[File:Hermes van.jpeg|thumbnail|Hermes system road measurement vehicle]]

The measurement principle is based on projecting a laser line on the road surface and recording its image using a high resolution camera of high frame rate.
The deviation of the laser line is measured, which corresponds to undulations of the road surface.

[[File:Lasergrid.png|thumbnail|Multi-laser projection on road surface]]

[[File:Potholehermes.webm|thumbnail|Real-time pothole detection]]

A novel automated pothole detection algorithm has also been developed to automatically characterise the quality of the road surface and detect potentially hazardous defects.
A specially-developed GNSS controller provides high accuracy localisation of acquired measurement data.

== Dissemination and exploitation ==
The developed system addresses the needs of a broad spectrum of potential end-users, including road maintainers, road constructors, air-field maintainers and national technical control centres responsible for road maintenance. The novel Hermes technology has been disseminated through a range of different activities including the following:
* Publication of results in academic and scientific journals such as the [[IEEE]] Communications Society Journal.<ref>{{cite web|url=http://www.comsoc.org/|title=IEEE Communications Society|work=comsoc.org}}</ref>
* Organisation of Open Days, Workshops and Conferences such as the Baltic Electronics Conference that took place in Tallinn, Estonia.<ref>{{cite web|url=http://www.elin.ttu.ee/bec/14/|title=The Baltic Electronics Conference Home Page|work=ttu.ee}}</ref><ref>Mõlder, A.; Märtens, O.; Saar, T.; and Land R. Extraction of the variable width laser line. In: Proceedings of the 14th Biennial Baltic Electronics Conference: 2014 14th Biennial Baltic Electronics Conference, Tallinn, Estonia, 6–8 October 2014. IEEE, 2014, 2014. pp. 157–160</ref>
* Promotion of results at European and international conferences such as the IEEE International Conference on Communications (ICC 2014)<ref>Karkazis, P.; Papaefstathiou, I. ; Sarakis, L. ; Zahariadis, T. ; Velivassaki, T.-H. ; Bargiotas, D. Evaluation of RPL with a transmission count-efficient and trust-aware routing metric. In Communications (ICC), 2014 IEEE International Conference on, Sydney, NSW, 10–14 June 2014, pp. 550-556</ref> and HiPEAC 2014, Workshop on Reconfigurable Computing (WRC).<ref>Dollas A. FPGA Supercomputers as Compute Servers: Are We There Yet?, In 8th HiPEAC Workshop on Reconfigurable Computing, Vienna-Austria, 20–22 January 2014</ref>
* Promotion at exhibitions and trade shows such as the Bucharest International Technical Fair TIB 2013<ref>{{cite web|url=http://www.tradefairdates.com/TIB-M3360/Bucuresti.html|title=TIB Bucharest|work=tradefairdates.com}}</ref> and Siftex Exhibition, St. Petersburg, Russia.<ref>{{cite web|url=http://sfitex.primexpo.ru/media/23/sfitex_13_psr_eng.pdf|title=Sfitex|work=primexpo.ru}}</ref>

== Consortium ==

=== SME participants ===
* Ardoran OÜ<ref>{{cite web|url=http://www.ardoran.ee/|title=Eesti Koduleht|work=ardoran.ee}}</ref>
* Wing Computer Group<ref>{{cite web|url=http://www.wing.ro/|title=Wing Computer Group: Prin noi succesul tau este garantat!|work=wing.ro}}</ref>
* MobileMedia<ref>{{cite web|url=http://www.mobilemedia.gr/|title=Home|work=mobilemedia.gr}}</ref>
* Prometeo Innovations<ref>{{cite web|url=http://prometeoinnovations.com/|title=Prometeo Innovations|work=prometeoinnovations.com}}</ref>

=== Research and technological development partners ===
* National Institute of R&D for Optoelectronics<ref>http://www.inoe.ro/en/</ref>
* [[Tallinn University of Technology]], Thomas Johann Seebeck Department of Electronics<ref>{{cite web|url=http://www.ttu.ee/|title=Avalehekülg < Tallinna Tehnikaülikool: Sinu elustiil!|author=E-turundusagentuur ADM Interactive|work=ttu.ee}}</ref>
* Electronics Design<ref>{{cite web|url=http://electronics.ee/eng/index.html|title=Electronics Design, Ltd.|work=electronics.ee}}</ref>
* Telecommunication Systems Institute<ref>{{cite web|url=http://www2.tsi.gr/?lang=en|title=Telecommunication Systems Research Institute-Ινστιτούτο Τηλεπικοινωνιακών Συστημάτων|work=tsi.gr}}</ref>

== References ==
{{reflist}}

[[Category:Cartography]]
[[Category:Road maps]]
[[Category:Articles containing video clips]]