{{Infobox Mayor
| name=Adgur Rafet-ipa Kharazia<br>Адгəыр Рафеҭ-иҧа Харазиа<br>ადგურ ხარაზია
| image=
| order=[[Mayor of Sukhumi]]
| term_start=22 October 2014 
| term_end=
| president=[[Raul Khajimba]]
| predecessor=[[Alias Labakhua]]
| successor=
| order1=[[Mayor of Sukhumi]] {{small|Acting}}
| term_start1=5 November 2004 
| term_end1=16 February 2005
| president1=[[Vladislav Ardzinba]]
| predecessor1=[[Leonid Lolua]]
| successor1=[[Astamur Adleiba]]
| birth_date=
| birth_place=
| party=
| religion=
| spouse=
}}

'''Adgur Rafet-ipa Kharazia''' ({{lang-ab|''Адгəыр Рафеҭ-иҧа Харазиа''}}, {{lang-ka|''ადგურ ხარაზია''}}) is the current Mayor of [[Sukhumi]] and a former Minister for Agriculture and Vice Speaker of the [[People's Assembly of Abkhazia]].

==Acting Mayor of Sukhumi (first time)==

Adgur Kharazia was head of the [[Gulripsh district]] assembly before he was appointed acting mayor of [[Sukhumi]] by president [[Vladislav Ardzinba]] on 5 November 2004, in the heated aftermath of the [[Abkhazian presidential election, 2004|2004 presidential election]], succeeding [[Leonid Lolua]]. During his first speech he called upon the two leading candidates, [[Sergei Bagapsh]] and [[Raul Khadjimba]], to both withdraw.<ref>{{cite news|url=http://www.hri.org/cgi-bin/brief?/news/balkans/rferl/2004/04-11-10.rferl.html#21|title=MAYOR SUGGESTS ABKHAZ PRESIDENTIAL RIVALS SHOULD WITHDRAW|publisher=RFE/RL|date=November 10, 2004|accessdate=2008-07-01}}</ref>

On February 16 of 2005, newly elected President Bagapsh replaced Kharazia with [[Astamur Adleiba]] as mayor of Sukhumi.

==Member of the People's Assembly of Abkhazia==

In the [[Abkhazian parliamentary election, 2007|2007 parliamentary elections]] Adgur Kharazia successfully stood as candidate in the Dranda precinct No. 24, winning a majority in the first round. He formed part of the opposition.<ref>{{cite news|url=http://nregion.com/txt.php?i=11457|title=В Абхазии возросло влияние турецкого лобби|publisher=Новый Регион|date=April 1, 2007|accessdate=2008-07-01}}</ref> During the assembly's first meeting after the election Kharazia was nominated for the position of [[Speaker (politics)|speaker]] by fellow MP [[Rita Lolua]] but lost to [[Nugzar Ashuba]].<ref>{{cite news|url=http://www.apsnypress.info/news2007/april/3.htm |title=Нугзар Ашуба вновь избран спикером парламента. |publisher=[[Apsnypress|Апсныпресс]] |date=April 3, 2007 |accessdate=2008-07-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20071029165200/http://www.apsnypress.info/news2007/april/3.htm |archivedate=2007-10-29 |df= }}</ref>
On the 27th of July 2007, Kharazia took part in a round table on free speech organised by [[journalists]] that called upon the government to end what it called the pursuit of independent and opposition media.<ref>{{cite news|url=http://www.humanrights.ge/index.php?a=news&id=3713&lang=en|title=Abkhazian Journalists Call Upon De Facto Government to Stop Pursuing Media|publisher=humanrights.ge|date=August 10, 2007|accessdate=2008-07-01}}</ref>

==Mayor of Sukhumi (second time)==

Following the [[Abkhazian Revolution|May 2014 Revolution]] and the election of [[Raul Khajimba]], Kharazia was again appointed as Acting Mayor of Sukhumi on 22 October 2014.<ref name=apress13326>{{cite news|title=Адгур Харазия назначен исполняющим обязанности главы администрации г. Сухум|url=http://apsnypress.info/news/13326.html|accessdate=22 October 2014|agency=[[Apsnypress]]|date=22 October 2014}}</ref> On 4 April 2015, he won the by-election to the City Council in constituency no. 3 unopposed,<ref name=alhra30>{{cite web|title=Итоги выборов |url=http://alhra.org/index.php/30-sostoyanie-vybornogo-protsessa-na-12-00-04-04-2015g |website=alhra.org |publisher=Избирательная комиссия по выборам в органы местного самоуправления г.Сухум |accessdate=19 September 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20151208112518/http://alhra.org/index.php/30-sostoyanie-vybornogo-protsessa-na-12-00-04-04-2015g |archivedate=8 December 2015 |df= }}</ref> and was confirmed as mayor on 4 May.<ref name=ukaz120>{{cite web|last1=Khajimba|first1=Raul|authorlink=Raul Khajimba|title=УКАЗ О главе администрации города Сухум|url=http://presidentofabkhazia.org/upload/iblock/ab8/%D0%A3%D0%BA%D0%B0%D0%B7%20%D0%9E%20%D0%B3%D0%BB%D0%B0%D0%B2%D0%B5%20%D0%90%D0%B4%D0%BC%D0%B8%D0%BD%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D0%B8%20%D0%B3.%20%D0%A1%D1%83%D1%85%D1%83%D0%BC.pdf|website=presidentofabkhazia.org|accessdate=19 September 2015}}</ref>

==References==
{{Reflist}}

{{s-start}}
{{succession box | before=[[Leonid Lolua]] | title=acting [[Mayor of Sukhumi]] | years=2004&ndash;2005 | after=[[Astamur Adleiba]]}}
{{succession box | before=[[Alias Labakhua]] | title=[[Mayor of Sukhumi]] | years=2014&ndash;Present | after=Incumbent}}
{{s-end}}

{{DEFAULTSORT:Kharazia, Adgur}}
[[Category:Living people]]
[[Category:5th convocation of the People's Assembly of Abkhazia]]
[[Category:Mayors of Sukhumi]]
[[Category:4th convocation of the People's Assembly of Abkhazia]]
[[Category:Ministers for Agriculture of Abkhazia]]
[[Category:Heads of Gulripshi District]]
[[Category:5th convocation of the Sukhumi City Council]]

{{Abkhazia-bio-stub}}
{{Abkhazia-poli-stub}}
{{Georgia-mayor-stub}}