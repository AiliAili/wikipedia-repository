{{Infobox Journal
| title        = Architectural History
| cover        = [[File:Architectural History.gif]]
| editor       = Dr Alistair Fair
| discipline   = History of architecture
| language     = [[English language|English]]
| publisher    = [[SAHGB Publications]] Ltd
| country      = UK
| frequency    = Annually
| history      = 1958–present
| impact       = 
| impact-year  = 
| website      = http://www.sahgb.org.uk/index.cfm/display_page/Publications
| link1        = http://www.jstor.org/journal/archhist
| link1-name   = JSTOR.org
| JSTOR        = 
| OCLC         = 62483180
| LCCN         = 2005-237367
| CODEN        = 
| ISSN         = 0066-622X
}}

'''''Architectural History''''' is the main journal of the [[Society of Architectural Historians of Great Britain]] (SAHGB).<ref>[http://www.sahgb.org.uk/index.cfm/display_page/Publications Publications: Architectural History], ''Society of Architectural Historians of Great Britain''.</ref>

The journal is published each autumn and usually comprises around 400 pages. The architecture of the [[British Isles]] is a major theme of the journal, but articles can consider all places and periods. All articles are double peer-reviewed, anonymously; acceptance is dependent on satisfactory referees' comments and revision. All members of the SAHGB receive the journal, as do subscribing institutional libraries. Older issues from its inception in 1958 onwards are available online through [[JSTOR]].<ref>[http://www.jstor.org/journal/archhist Architectural History], [[JSTOR]]. {{ISSN|0066-622X}}.</ref>

The lead editor, Dr Alistair Fair, is a Research Associate at the [[University of Cambridge]].<ref>{{cite web| url=http://www.arct.cam.ac.uk/people/ajf56@cam.ac.uk | title=Dr Alistair Fair BA (Oxon.), MA (Lond.), PhD (Cantab.) | publisher=[http://www.arct.cam.ac.uk/ Department of Architecture], [[University of Cambridge]] | accessdate=23 November 2012 }}</ref>

== References ==
{{reflist}}

[[Category:Publications established in 1958]]
[[Category:1958 establishments in the United Kingdom]]
[[Category:Architecture journals]]
[[Category:History journals]]
[[Category:Architectural history]]
[[Category:Annual journals]]
[[Category:English-language journals]]


{{architecture-journal-stub}}
{{history-journal-stub}}