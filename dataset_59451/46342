{{lowercase title}}
{{COI|date=November 2016}}
{{Infobox company
| name = ayondo
| logo          = [[File:Ayondo Corporate Logo.svg|220px]]
| logo_size = 250
| logo_alt = 
| logo_caption = 
| logo_padding = 
| type = [[Private company|Private]]
| industry = {{plainlist|
* [[Financial Technology]]
* [[Financial Services]]
* [[Software]]
}}
| foundation       = {{Start date and age|2008|}}
| founders = {{plainlist|
* Thomas Winkler (Chairman)
* Robert Lempka (CEO)
}}
| area_served = Worldwide
| products = {{plainlist|
* ayondo Social Trading 
* TradeHub
}}
| homepage         = {{URL|https://www.ayondo.com}}
}}

'''ayondo''' is a [[financial technology]] group that is a global provider of [[social trading]] services and [[Brokerage firm|brokerage]] services for both business-to-consumer (B2C) and business-to-business (B2B) for [[Contract for difference|contracts for difference]] (CFDs).<ref>{{Cite web|url=http://infopub.sgx.com/FileOpen/Starland-20160620-SPA%20Announcement.ashx?App=Announcement&FileID=409492|title=PROPOSED ACQUISITION OF THE EQUITY INTEREST OF AYONDO HOLDING AG|last=STARLAND HOLDINGS LIMITED|first=|date=|website=infopub.sgx.com|publisher=|access-date=4 October 2016}}</ref>

ayondo Holding AG is the ultimate holding company of the group and is domiciled in Zug, Switzerland.<ref name=":0">{{Cite web|url=https://www.bloomberg.com/research/stocks/private/snapshot.asp?privcapId=58049125|title=Company Overview of ayondo Holding AG|last=|first=|date=3 October 2016|website=Company Overview|publisher=Bloomberg|access-date=}}</ref> ayondo markets Limited is domiciled in London, United Kingdom and is authorised and regulated by the [[Financial Conduct Authority]] (FCA Register number 184333).<ref>{{Cite web|url=https://register.fca.org.uk/ShPo_FirmDetailsPage?id=001b000000MfGf3AAF|title=Financial Conduct Authority|website=register.fca.org.uk|access-date=5 October 2016}}</ref> The company’s trading platform TradeHub allows users to trade various financial instruments ranging from indices and shares to commodities and interest rates.<ref name=":1">{{Cite web|url=https://www.bloomberg.com/research/stocks/private/snapshot.asp?privcapId=61854207|title=ayondo markets Limited: Private Company Information - Businessweek|website=www.bloomberg.com|access-date=5 October 2016}}</ref>

ayondo GmbH is domiciled in Frankfurt, Germany and is a tied agent of DonauCapital Investment GmbH and therefore registered in the intermediaries’ register of [[Federal Financial Supervisory Authority|BaFin]].<ref>{{Cite web|url=http://www.donaucapital.de/partner.html|title=Partner|website=DonauCapital Wertpapier AG|language=de-DE|access-date=5 October 2016}}</ref><ref>{{Cite web|url=https://portal.mvp.bafin.de/database/InstInfo/institutDetails.do?cmd=loadInstitutAction&institutId=108861|title=BaFin - Unternehmensdatenbank BaFin|last=BaFin|website=portal.mvp.bafin.de|access-date=5 October 2016}}</ref> ayondo GmbH is responsible for the firm’s social trading platform, which allows users to follow other traders and automatically copy the trades of experts.<ref name=":4">{{Cite web|url=http://www.financemagnates.com/forex/brokers/ayondo-ag-opens-an-office-in-singapore-after-unifying-its-brand/|title=Ayondo Opens an Office in Singapore after Unifying Its Brand {{!}} Finance Magnates|date=11 July 2014|language=en-US|access-date=2016-10-05}}</ref><ref name=":2">{{Cite web|url=http://www.scmp.com/week-asia/business/article/2020094/fintech-next-frontier-hong-kongs-battle-singapore|title=Fintech – the next frontier for Hong Kong’s battle with Singapore?|access-date=5 October 2016}}</ref> ayondo Asia Pte Ltd is domiciled in Singapore and handles white labels and mobile platform/software product development as the [[Research and development|R&D]] lab for the group.<ref name=":4"/><ref name=":8" /><ref name=":10" />

== History ==

ayondo Holding AG (previously known as Next Generation Finance AG) was founded in 2008 by Robert Lempka and Thomas Winkler.<ref name=":0" /><ref>{{Cite web|url=http://www.europeanceo.com/finance/ayondos-social-trading-platform-opens-doors-for-investors/|title=ayondo’s social trading platform opens doors for investors|date=6 January 2015|language=en-US|access-date=5 October 2016}}</ref> In 2009 the social trading network was launched under ayondo GmbH.<ref name="Ayondo Review">{{Cite web|url=https://www.howtocopytradeforex.com/ayondo-review-july-2015/|title=Ayondo Review|date=16 July 2015|language=en-US|access-date=5 October 2016}}</ref> In 2011 ayondo Holding invested in ayondo markets Limited which was formerly known as Gekko Global Markets Ltd and was incorporated in 1996.<ref name=":1"/><ref>{{Cite web|url=http://www.financial-spread-betting.com/reviews/ayondo-markets.html|title=Gekko Global Markets Review|website=www.financial-spread-betting.com|access-date=5 October 2016}}</ref> Gekko Global Markets rebranded to ayondo markets Limited in April 2014. In early 2014, ayondo Holding AG restructured the group and got directly involved in the operations of the subsidiary companies thereby changing its status as an investment holding company to an operational holding company.<ref name=":4"/>

Since the re-organisation in 2014, the group began its expansion into Asia after receiving funding from Singapore-based private equity group Luminor Capital which led the round of investment in establishing ayondo Asia Pte Ltd.<ref>{{Cite web|url=https://www.techinasia.com/gamified-social-trading-platform-ayondo-4m-luminor-capital|title=Tech in Asia - Connecting Asia's startup ecosystem|website=www.techinasia.com|language=en-US|access-date=5 October 2016}}</ref>

In late 2014&nbsp;ProSiebenSat.1&nbsp;Group’s investment arm SevenVentures entered into a media partnership deal with ayondo GmbH to increase public and brand awareness of the company and of social trading.<ref>{{Cite web|url=http://www.financemagnates.com/forex/brokers/breaking-ayondo-launches-social-trading-campaign-major-german-tv-networks/|title=ayondo Launches Social Trading TV Campaign with Major German TV Networks|date=19 December 2014|language=en-US|access-date=6 October 2016}}</ref>

In 2015 ayondo received additional funding from its existing shareholders again led by Luminor Capital.<ref>{{Cite web|url=http://dataconomy.com/social-trading-company-ayondo-acquires-6m-chf-for-international-expansion/|title=Social Trading Company ayondo Acquires 6m CHF for International Expansion - Dataconomy|date=30 April 2015|language=en-US|access-date=5 October 2016}}</ref> In 2015 ayondo introduced a security initiative, putting the security and safety of its clients’ assets as a top priority in order to offer them additional protection from adverse market conditions.<ref name=":5">{{Cite web|url=http://www.financemagnates.com/forex/brokers/ayondo-secures-massive-client-funds-protection-for-its-traders/|title=ayondo Secures Massive Client Funds Protection for its Traders {{!}} Finance Magnates|date=1 July 2015|language=en-US|access-date=5 October 2016}}</ref> This initiative had the following components:

* Removed negative balance funding obligations for all ayondo branded trading accounts. This means that in the event the clients’ losses exceed their account balance, ayondo shall not seek to recover these losses from the clients.<ref name=":5" />

* Arranged to provide, at its own cost, all ayondo branded clients extra protection of up to GBP 500,000 through FSCS excess cover insurance which is in addition to the [[Financial Services Compensation Scheme]] (FSCS) of GBP 50,000. This extensive protection applies to all FSCS eligible clients.<ref name=":5" />

In November 2015 ayondo entered into a business partnership with KGI Fraser Securities (‘KGI’) to provide trading services to KGI clients.<ref>{{Cite web|url=http://www.straitstimes.com/business/new-platform-allows-trading-of-cfds|title=New platform allows trading of CFDs|last=hermes|date=6 November 2015|access-date=5 October 2016}}</ref>

In 2013 ayondo was listed in the top 50 technology companies worldwide in the area of financial technology ("FinTech 50").<ref>{{Cite web|url=http://fintechcity.com/the-fintech-50-2013/4581027487|title=The-Fintech-50-2013|website=fintechcity.com|access-date=5 October 2016}}</ref>

In 2015 and 2016 ayondo received the award for Best Social Trading Platform at the ADVFN International Financial Awards.<ref>{{Cite web|url=http://www.londonstockexchange.com/exchange/news/market-news/market-news-detail/AFN/12727445.html|title=ADVFN International Financial Awards 2016 winners - RNS - London Stock Exchange|website=www.londonstockexchange.com|access-date=5 October 2016}}</ref><ref>{{Cite web|url=http://uk.advfn.com/awards_2016|title=ADVFN Awards 2016|website=uk.advfn.com|access-date=5 October 2016}}</ref><ref>{{Cite web|url=http://uk.advfn.com/awards_2015|title=ADVFN Awards 2015|website=uk.advfn.com|access-date=5 October 2016}}</ref>

In April 2016, ayondo announced that it is currently working on a Reverse Takeover (RTO) transaction which could make it the first FinTech company to be listed on the Singapore Exchange (SGX).<ref name=":10">{{Cite web|url=http://www.financemagnates.com/fintech/news/ayondo-launches-revamped-website-relocates-offices-in-uk-and-singapore/|title=ayondo Launches Revamped Website, Relocates Offices in UK and Singapore {{!}} Finance Magnates|date=7 September 2016|language=en-US|access-date=5 October 2016}}</ref><ref>{{Cite web|url=http://www.financemagnates.com/forex/brokers/breaking-ayondo-aims-singapore-listing-115-mln-valuation/|title=Breaking: ayondo Aims at Singapore Listing with over $155m Valuation {{!}} Finance Magnates|date=13 April 2016|language=en-US|access-date=5 October 2016}}</ref><ref>{{Cite web|url=http://www.straitstimes.com/business/ayondo-in-158m-reverse-takeover-deal-with-starland|title=Ayondo in $158m reverse takeover deal with Starland|last=hermes|date=21 June 2016|access-date=5 October 2016}}</ref>

In August 2016, ayondo released a simulated trading mobile app, called ayondo Academy, which was developed by the company’s mobile software R&D Lab in Singapore.<ref name=":8">{{Cite web|url=http://www.financemagnates.com/forex/brokers/ayondo-unveils-academy-app-fostering-simulated-trading-environments/|title=ayondo Unveils Academy App, Fostering Simulated Trading Environments {{!}} Finance Magnates|date=15 August 2016|language=en-US|access-date=5 October 2016}}</ref><ref name=":10" />

In September 2016, ayondo launched a refresh of its website.<ref name=":10" />

== Products and Services ==

ayondo provides self-directed and social trading platforms owned by ayondo markets Limited and ayondo GmbH respectively.<ref name=":4" /><ref name=":9">{{Cite web|url=http://www.investoo.com/ayondo-review/|title=Ayondo Review {{!}} Social Trading Platform|date=20 April 2015|language=en-US|access-date=5 October 2016}}</ref>

=== Self-directed trading ===

TradeHub is available on desktop browser and as apps on Android and iOS. ayondo provides TradeHub for live and demo trading.<ref name=":9" />

=== Social trading ===

ayondo’s social trading platform allows users to delve into social media-style stock trading. They can emulate the trades of other users, exchange ideas and gauge market sentiment.<ref name=":2" /> By choosing to ‘follow’ the trading activity of these expert traders, the retail client is able to benefit from the traders superior knowledge and potential profitability, ultimately replicating the traders monetary performance on his/her own trading account.<ref>{{Cite web|url=http://www.businessinsider.com/mobile-apps-turn-bedroom-traders-into-star-professionals-2015-5?IR=T&r=US&IR=T|title=Mobile apps are turning bedroom traders into star professionals|access-date=5 October 2016}}</ref><ref>{{Cite web|url=http://www.business2community.com/strategy/5-ways-business-owners-can-grow-wealth-outside-business-01638358#pg8WM56tsHFdCtKO.97|title=5 Ways Business Owners Can Grow Their Wealth Outside Of Their Business|access-date=5 October 2016}}</ref> It is available on both the web and on an iOS app.<ref>{{Cite web|url=http://socialtradingguru.com/ayondo-launches-new-iphone-social-trading-app-jul-2015|title=Ayondo Launches New iPhone Social Trading App Jul 2015|website=socialtradingguru.com|access-date=5 October 2016}}</ref>

== Reception ==
ayondo has 210,000 users from 195 countries on its social trading platform.<ref name=":10"/> User feedback on the social trading platform is varied. Some users mention that it is one of the fairest trading systems currently available and that there are good quality/low risk Top Traders to follow, relatively low investment entry cost and that deposits are being protected up to 500,000 GBP for ayondo markets Limited clients (non-white labelled) who are FSCS eligible.<ref name=":9" /><ref name=":6">{{Cite web|url=http://investingoal.com/ayondo-review/|title=Ayondo Review - Our Opinion - investingoal.com|date=5 January 2016|language=en-US|access-date=5 October 2016}}</ref> Some reviews conclude that there are areas for improvement, such as the ability to export a trader’s transaction history to Excel or other data formats for analysis, to customisable money management and have more Top Traders to follow. They also mention the need for support via chat could be improved and additional features for social interaction among traders and users, etc.<ref name="Ayondo Review"/><ref name=":6" /><ref>{{Cite web|url=http://socialtradingguru.com/network-reviews/ayondo-review|title=Ayondo Review|website=socialtradingguru.com|access-date=5 October 2016}}</ref>

== References ==
<!-- Inline citations added to your article will automatically display here. See https://en.wikipedia.org/wiki/WP:REFB for instructions on how to add citations. -->
{{reflist|30em}}

== External links ==
* [http://www.ayondo.com Official website]
* [https://www.crunchbase.com/organization/ayondo ayondo] at [[TechCrunch#CrunchBase|CrunchBase]]

[[Category:Financial software companies]]
[[Category:Financial services companies based in London]]
[[Category:Software companies]]
[[Category:Companies based in Frankfurt]]
[[Category:Software companies of Singapore]]
[[Category:Companies of Germany]]
[[Category:Companies of Singapore]]
[[Category:Financial technology companies]]
[[Category:Technology companies established in 2008]]
[[Category:Online brokerages]]
[[Category:Companies based in London]]
[[Category:Companies based in Singapore]]
[[Category:Companies of Switzerland]]
[[Category:Companies of the United Kingdom]]