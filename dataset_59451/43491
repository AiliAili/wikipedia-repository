{|{{Infobox Aircraft Begin
 | name=Rigault RP.01B
 | image=Rigault RP.01 F-WABO Mitry 29.05.57 edited-1.jpg
 | caption=The RP.01B "Le Napalm" under construction at Mitry-Mory airfield near Paris in May 1957
}}
{{Infobox Aircraft Type
 | type=light single-seat 
 | national origin=France
 | manufacturer=Paul Rigault
 | designer=Paul Rigault
 | first flight=1958
 | introduced=1958
 | retired=
 | status=No longer extant
 | primary user=the builder
 | number built=One
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Rigault RP.01B''' was a [[France|French-built]] high-wing single-engined ultralight aircraft of the 1950s.

==Development==

The RP.01B was a one-off aircraft which was designed and built by Monsieur Paul Rigault at [[Mitry-Mory]] airfield to the NE of Paris.<ref>Butler, 1963, p. 113</ref> The airfield now forms part of the site of [[Paris-Charles de Gaulle]] airport.

The aircraft was fitted with high-set wings which were supported by twin struts, a fixed tailwheel [[Landing gear|undercarriage]] and a single seat for the pilot owner. It was powered by a nine-cylinder Salmson 9 ADB air-cooled radial engine. It was of conventional wooden construction with plywood and fabric-covered fuselage and fabric-covered wings and control surfaces.<ref>Green, 1965, p. 62</ref>

==Operational history==

M. Rigault completed the aircraft during 1958 and named it "Le Napalm". He flew it regularly until at least early 1965.<ref>Green, 1965, p. 62</ref> The RP.01B is no longer extant.

==Specifications (RP.01B)==

{{aerospecs
|ref=Green, 1965
|met or eng?=eng

|crew=1
|capacity=
|length m=
|length ft=
|length in=
|span m=7.70
|span ft=25
|span in=3
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=324
|gross weight lb=714
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Salmson 9 (air cooled engine)|Salmson 9]] ADB
|eng1 kw=34
|eng1 hp=45
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|perfhide=Y <!-- REMOVE THIS LINE IF PERFORMANCE FIGURES COME TO LIGHT -->

|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

}}

==References==
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
*{{cite book|last=Butler|first=P.H.|title=French Civil Aircraft Register|year=1963|publisher=Merseyside Society of Aviation Enthusiasts|isbn=<!--none-->}}
*{{cite book|last=Green|first=William|title=The Aircraft of the World|year=1965|publisher=MacDonald & Co. (Publishers) Ltd|isbn=<!--none-->}}
{{refend}}

==External links==

[[Category:French civil utility aircraft 1950–1959]]