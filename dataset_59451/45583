{{Use Canadian English|date=December 2012}}
{{Use dmy dates|date=December 2012}}
{{Infobox military person
|name=William Ernest Shields
|image=
|caption=
|birth_date=15 October 1892
|death_date={{d-da|1 August 1921|15 October 1892}}
|birth_place=[[Toronto]], [[Ontario]]
|death_place=[[High River]], [[Alberta]]
|nickname=
|allegiance= [[King George V]] of the [[British Empire]]
|branch=[[Royal Flying Corps]], [[Royal Air Force]]
|serviceyears=1915-ca 1919
|rank=Captain
|unit=[[No. 41 Squadron RAF]]
|commands=
|battles=
|awards=[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (with Bar in lieu of a second award)
|relations=
|laterwork= Flew forest fire spotting missions for Dominion air patrol.
}}

'''William Ernest Shields''' [[Distinguished Flying Cross (United Kingdom)|DFC & Bar]] (15 October 1892 – 1 August 1921) was a [[Canada|Canadian]] [[World War I|First World War]] [[flying ace]], officially credited with 24 victories.<ref>http://www.theaerodrome.com</ref>

==Early life==
William Ernest Shields was born on 15 October 1892 in [[Toronto]], [[Ontario]], Canada, the son of W. H. Shields. Not quite a year after the British declaration of war at the start of World War I dragged Canada into the war, the younger Shields joined the [[Canadian Expeditionary Force]]. As was customary for Canadians entering the military at that time, he filled out an Attestation Paper. The date he joined is filled in next to his signature of "W. E. Shields" below his oath of [[allegiance]] to [[King George V]]; it was 25 March 1915.<ref name=attest>[http://www.theaerodrome.com/aces/canada/attestation/shields.php] Shields' Attestation Paper.</ref>

The reverse of the Attestation Paper shows that the examining [[medical officer]] measured him as being five feet five inches tall, with an expanded chest measurement of 36 inches. He had a fair complexion, brown eyes, and light brown hair.<ref name=attest/>

==Aerial service in the First World War==

Shields' military service details are unknown before he joined the [[Royal Flying Corps]]. His known history begins with his posting to [[No. 41 Squadron RAF]] on 20 March 1918. There he joined two other Canadian aces, [[William Gordon Claxton|William Claxton]] and [[Frederick McCall]].<ref name=trenches336>Shores et al 1990, pp. 336-337.</ref>

Shields was assigned a [[Royal Aircraft Factory SE.5]]a to fly. With it, he began his victory list on 12 June 1918, driving a German [[Albatros D.V]] down out of control over [[Guerbigny]], France. It was the start of a victory tally that would make him an ace several times over; he also became proficient at the hazardous mission of [[balloon buster|balloon busting]].<ref name=trenches336/>

By the time Shields scored his final victories on 4 November 1918, a week before war's end, they totaled 24. He was an ace on [[observation balloons]] alone, being credited with destroying five of them. He also destroyed 11 enemy fighter planes, and drove down eight out of control. His valour was rewarded with two awards of the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] and the rank of Captain.<ref name=trenches336/>

==Post First World War==

Shields returned to Canada. He married a Miss Nicholson on 22 June 1921 while living in [[Portage la Prairie]]. As he was employed by the Dominion's air patrol, he moved to [[High River]], Alberta as the acting sub-station superintendent of this station. His flying duties there included the aerial spotting of [[forest fires]] over the eastern [[Rocky Mountains]], including the [[Red Deer]] area.<ref name=herald>[http://www.theaerodrome.com/forum/newspaper-articles/32836-air-patrol-pilot-dead%3B-plane-dives.html] ''The Lethbridge Herald'', 1 August 1921.</ref>

On 1 August 1921, he and [[wireless operator]] G. H. Harding were going to fly west of Red Deer. Shields took off at 0737 hours. At an altitude of about 50 feet, the plane side-slipped into a nose-diving crash. Though Harding suffered only minor injuries, Shields died from a fracture to the back of his skull, expiring a few minutes after being pulled from the wreckage.<ref name=herald/>

==Text of award citations==

===Distinguished Flying Cross===

"Lieut. William Ernest Shields. A gallant officer who inspires others by his courage and dash. In six weeks he destroyed six enemy aircraft and drove down three others out of control. On one occasion he, single-handed, engaged three scouts, driving down two of them."<ref>[http://www.london-gazette.co.uk/issues/30989/supplements/12972] ''Supplements to The London Gazette'', 2 November 1918, pp. 12972-12973.</ref>

===Distinguished Flying Cross - Bar===

"Lt. (A./Capt.) William Ernest Shields, D.F.C. (FRANCE) Bold in attack and skilful in manoeuvre, this officer is conspicuous for his success and daring in aerial combats. On 22 September, when on offensive patrol, he was attacked by fourteen Fokkers; he succeeded in shooting down one. On another occasion he was attacked by six scouts and destroyed one of these. In all, since 28 June, this officer has accounted for fourteen enemy aircraft."<ref>[http://www.london-gazette.co.uk/issues/31170/pages/2033] ''The London Gazette'', 7 February 1919, p. 2033.</ref>

==Endnotes==
{{reflist}}

==References==
* Christopher F. Shores, [[Norman Franks]], Russell Guest. ''Above the Trenches: a Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915-1920.'' Grub Street, 1990. ISBN 0-948817-19-4, ISBN 978-0-948817-19-9.

===Websites===
{{refbegin}}
{{cite web |url=http://www.theaerodrome.com/aces/canada/index.php |title=WWI Aces of Canada |accessdate=2008-06-14 |work=www.theaerodrome.com|date= }}
{{refend}}

{{wwi-air}}

{{DEFAULTSORT:Shields, William Ernest}}
[[Category:Canadian aviators]]
[[Category:Canadian World War I flying aces]]
[[Category:1892 births]]
[[Category:1921 deaths]]