{{DISPLAYTITLE: Blind Ambition (''Family Guy'')}}
{{Infobox television episode
| title        =Blind Ambition
| series       =Family Guy
| image        =
| caption      =
| season       =4
| episode      =3
| airdate      =May 15, 2005
| production   =4ACX04
| writer       =[[Steve Callaghan]]
| director     =Chuck Klein
| guests       =*[[Gary Cole]]
*[[Gina Gershon]]
*[[Judd Hirsch]]
*[[Rachael MacFarlane]]
*[[Lisa Wilhoit]]
| episode_list =[[Family Guy (season 4)|''Family Guy'' (season 4)]]<br/>[[List of Family Guy episodes|List of ''Family Guy'' episodes]]
| season_list  = 
| prev         =[[Fast Times at Buddy Cianci Jr. High]]
| next         =[[Don't Make Me Over (Family Guy)|Don’t Make Me Over]]
}}

"'''Blind Ambition'''" is the third episode of the [[Family Guy (season 4)|fourth season]] of the American animated sitcom ''[[Family Guy]]''. It was first broadcast on [[Fox Broadcasting Company|Fox]] in the United States on May 15, 2005.<ref>[http://www.tv.com/family-guy/blind-ambition/episode/365970/cast.html Blind Ambition] {{webarchive |url=https://web.archive.org/web/20081221143021/http://www.tv.com/family-guy/blind-ambition/episode/365970/cast.html |date=December 21, 2008 }} tv.com. Retrieved on 2007-10-28.</ref> In the episode, [[Peter Griffin|Peter]] swallows an excessive number of nickels, causing him to become blind. He later becomes a hero after unwittingly saving Horace the bartender from a fire at his bar, The Drunken Clam, and then regains his sight. Meanwhile, Quagmire is forced to refrain from perverse sexual behavior or risk being driven out of the neighborhood following his arrest for spying on Lois in a ladies' lavatory.<ref name="Episode"/>

==Plot==
At the [[bowling alley]], [[Mort Goldman]] bowls a perfect game and becomes an overnight celebrity. Lois arrives to pick Peter up from the bowling alley, but discovers [[Glenn Quagmire|Quagmire]] [[Voyeurism|spying on her]] from the ceiling of the ladies' toilet. Quagmire is beaten up then arrested, but released shortly after by [[Joe Swanson|Joe]]. On his return, [[Lois Griffin|Lois]], [[List of characters in Family Guy#Bonnie Swanson|Bonnie]] and [[List of characters in Family Guy#Loretta Brown|Loretta]] reveal that they're petitioning the city of [[Quahog]] to have Quagmire [[Eviction|removed from their neighborhood]]. As Peter and the other guys are defending Quagmire, [[Ernie the Giant Chicken]] attacks Peter and starts a fight that causes huge casualties inside and outside of Quahog. After the fight, Peter returns to the neighborhood to return to the conversation and tells the women that, "Quagmire's a good guy, he's just a little mixed up, that's all!" Eventually, the women agree to let Quagmire stay in the neighborhood so long as he manages to control his perverse behavior. Quagmire's taught self-control through [[operant conditioning]] by Peter and his friends, and is eventually allowed out in public. Soon, however, he is distracted by women playing in a fountain in the [[shopping mall]] and panics, running into a CCTV camera operation room monitoring women's changing rooms. Discovering that a lady in a fitting room is having a [[myocardial infarction|heart attack]], he appears to rush to her aid, performing [[Cardiopulmonary resuscitation|CPR]] and saving her life. Quagmire is congratulated for his heroism, but his intention had been to molest the woman while she was unconscious (which he reveals by asking "What the hell is CPR?").<ref name="Episode">{{cite video|date=2005|title=Plot synopsis information for the episode "Blind Ambition"| medium=DVD|publisher=20th Century Fox}}</ref>

This upsets Peter, who is jealous of his friends' success. In the hope of becoming famous, Peter attempts to set a [[world record]] for eating the largest number of [[Nickel (United States coin)|nickels]], but develops [[Nickel#Toxicity|nickel poisoning]] and loses his vision. Attempting to drown his sorrows, Peter visits his local bar, The Drunken Clam, with his [[guide dog]], unaware that the bar is on fire (caused by God trying to impress a woman). Discovering the bartender Horace trapped under debris, Peter saves his life and is proclaimed a hero by local newsman [[Tom Tucker (Family Guy)|Tom Tucker]]. When told that he saved Horace from a burning building, Peter replied with disbelief, "That freakin' place was on fire?!" For his inadvertent bravery, Peter is awarded a medal by the mayor and receives an eye transplant, the replacement eyes coming from a homeless man dragged to death when Peter accidentally tied his guide dog's leash around the man's neck, thinking he was a parking meter. The end of this episode is an unconnected parody of the closing scene from ''[[Star Wars Episode IV: A New Hope]]''.

==Production==
[[File:Steve Callaghan by Gage Skidmore 2.jpg|right|155px|thumb|upright|alt=A man with brown hair, leans forward slightly while speaking into a microphone.|[[Steve Callaghan]] wrote the episode.]]
During ''Family Guy'''s third season, the show was cancelled by its network. In preparation in case the show was revived and began broadcasting again, five short scripts were written in 2001 for future episodes.<ref name="Goodman">{{cite video | people=Goodman, David|date=2005|title=Family Guy volume 4 Region 1 (Season 4) DVD commentary for the episode "Blind Ambition"| medium=DVD|publisher=20th Century Fox}}</ref> ''Blind Ambition'' was developed from one of those scripts.<ref name="Goodman"/> A number of scenes in the episode were removed before broadcast and one, the reappearance of [[Ernie the Giant Chicken]], had originally been set to broadcast in "[[The Cleveland–Loretta Quagmire]]". The scene was moved to this episode because "The Cleveland-Loretta Quagmire" already contained a lengthy fight sequence and overran its time allowance.<ref name="Goodman"/><ref name="MacFarlane"/><ref name="Callaghan">{{cite video | people=Callaghan, Steve|date=2005|title=Family Guy season 4 DVD commentary for the episode "Blind Ambition"| medium=DVD|publisher=20th Century Fox}}</ref> Several of the removed scenes focused on gags showing Peter and his friends attempting to rehabilitate Quagmire, one of which saw [[Brian Griffin]] transporting a fork-lift truck load of [[Porn|porn magazines]] away from Quagmire's house.<ref name="Goodman"/> Since the episode aired, a selection of action figures have been created of Peter acting as Gary, The No Trash Cougar.<ref name="Sheridan"/>

Show producer [[David A. Goodman|David Goodman]] received many telephone calls complaining about the scene where Peter attempts to seduce his son Chris, mistakenly believing him to be his wife, Lois. The scene was believed by some viewers to be encouraging child molestation.<ref name="Goodman"/> The show also received at least one letter of complaint regarding the scene where Quagmire watches Lois going to the toilet;<ref name="Goodman"/> screenwriter [[Chris Sheridan (writer)|Chris Sheridan]] comments on the DVD commentary that the number of complaints about this scene exceeded one.<ref name="Sheridan">{{cite video | people=Sheridan, Chris|date=2005|title=Family Guy season 4 DVD commentary for the episode "Blind Ambition"| medium=DVD|publisher=20th Century Fox}}</ref> It is prohibited on Fox to use the term 'Jesus Christ' without actually referring to the person himself, and so in the scene in which [[List of characters from Family Guy#Supernatural characters|God]] vaporises a person and exclaims "Jesus Christ", it was necessary for Jesus to physically appear before the two run away in order for the scene to be suitable for television airing.<ref name="Goodman"/><ref name="MacFarlane"/>

In addition to the regular cast, actor [[Gary Cole]], actress [[Gina Gershon]], actor [[Judd Hirsch]], voice actress [[Rachael MacFarlane]] and actress [[Lisa Wilhoit]] guest starred in the episode. Recurring guest voice actors [[Lori Alan]], actor [[John G. Brennan]], writer [[Danny Smith (writer)|Danny Smith]], and actress [[Jennifer Tilly]] made minor appearances.

==Cultural references==
*Stewie discovers the [[Keebler Company#Keebler Elves|Keebler Elves]] after crashing into the tree, who plan to kill their competition: [[Snap, Crackle and Pop]], with the help of [[Judd Hirsch]]'s nuclear weapon, which Peter views while peeking through a ball return at the bowling alley.<ref name="MacFarlane">{{cite video | people=Commentators|date=2005|title=Family Guy volume 4 Region 1 DVD commentary for the episode "Blind Ambition"| medium=DVD|publisher=20th Century Fox}}</ref> Later on in the bar, Crackle and Pop are seen discussing an attack by the Keebler Elves which apparently resulted in Snap's death.
*Beforehand, a ship crashes through Quahog buildings, a reference to ''[[Speed 2: Cruise Control]]''.<ref name="Goodman"/><ref name="MacFarlane"/>
*In one cutaway scene, [[W. Frederick Gerhardt]]'s [[Gerhardt Cycleplane|Cycleplane]] crashing is parodied.
*Peter discusses ''[[Scrubs (TV series)|Scrubs]]'' with Horace in the bar, a show for which both [[Neil Goldman and Garrett Donovan]] have written.<ref name="Goodman"/>
*The entire scene in which Peter receives his award from [[Adam West (Family Guy)|Mayor West]] is a reenactment of the ending of the original 1977 ''[[Star Wars]]'' film, ''[[Star Wars (film)|A New Hope]]''; [[Lucasfilm]] permitted the reproduction of the characters, music and sounds.<ref name="MacFarlane"/>
*The airplane that defeats [[Ernie the Giant Chicken]] in the fight is a mirror reference to the climatic scene in ''[[Raiders of the Lost Ark]]''.<ref name="Callaghan"/>
*Stewie being launched into a tree is a reference to ''[[The Brady Bunch]]''.<ref name="Goodman"/>

==Reception==
[[PopMatters]]' Kevin Wong gave the episode a positive review, feeling it was better than the two previous episodes of the season. He commented on the fight scene between Peter and [[Ernie the Giant Chicken]] as "a cartoon action sequence to end all cartoon action sequences: vehicles explode and limbs flail as Peter and the chicken beat each other senseless."<ref name="PopMatters">{{cite web|url=http://www.popmatters.com/pm/reviews/article/44864/family-guy-2005|title=Family Guy|date=June 13, 2005|accessdate=2008-01-22|author=Wong, Kevin|publisher=Popmatters.com}}</ref> "Blind Ambition" was criticized by Mike Drucker of [[IGN]], who found that "the long fight with the chicken in Blind Ambition was funny once before, but borderline tiring here". However, Drucker also noted that the ''[[Star Wars]]'' ending was "one of my favorite jokes in the series".<ref name="IGN">{{cite web|url=http://uk.dvd.ign.com/articles/672/672389p1.html|title=Family Guy&nbsp;– Volume 3|date=November 29, 2005|accessdate=2008-01-15|last=Drucker|first=MikeMike|publisher=[[IGN]]}}</ref>

==References==
{{Reflist|2}}

==External links==
{{wikiquote|Family_Guy/Season_4#Blind_Ambition_.5B4.03.5D|Blind Ambition (Family Guy)}}
{{Portal|Family Guy}}
*
* {{tv.com episode|family-guy/blind-ambition-365970|Blind Ambition}}
* {{imdb episode|0576915|Blind Ambition}}

{{Family Guy (season 4)}}

{{good article}}

{{DEFAULTSORT:Blind Ambition (Family Guy)}}
[[Category:Family Guy (season 4) episodes]]
[[Category:2005 American television episodes]]