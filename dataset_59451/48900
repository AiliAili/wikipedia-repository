{{Infobox organization
| name          = John William Pope Foundation
| native_name   = 
| image         = John William Pope Foundation logo.jpg
| image_size    = 
| alt           = 
| caption       = 
| map           = 
| map_size      = 
| map_alt       = 
| map_caption   = 
| map2          = 
| map2_size     = 
| map2_alt      = 
| map2_caption  = 
| abbreviation  = 
| motto         = 
| predecessor   = 
| merged        = 
| successor     = 
| formation     = 
| founder       = 
| extinction    =
| merger        = 
| type          = 
| status        = 
| purpose       = 
| headquarters  = 
| location      = [[Raleigh, North Carolina]]
| coords        = 
| region        = 
| services      = 
| membership    = 
| language      = 
| sec_gen       = 
| leader_title  = Vice president
| leader_name   = [[David Riggs]]
| leader_title2 = Chairman
| leader_name2  = [[Art Pope]]
| leader_title3 = 
| leader_name3  = 
| leader_title4 = 
| leader_name4  = 
| board_of_directors = 
| key_people    = 
| main_organ    = 
| parent_organization = 
| subsidiaries  = 
| secessions    = 
| affiliations  = 
| budget        = 
| staff         = 
| volunteers    = 
| slogan        = 
| website       = {{url|http://jwpf.org/}}
| remarks       = 
| formerly      = 
| footnotes     = 
}}

The '''John William Pope Foundation''' is a nonprofit 501(c)(3) private charitable foundation based in [[Raleigh, North Carolina|Raleigh]], [[North Carolina]], that contributes to conservative public policy organizations and think tanks, educational institutions, humanitarian charities, and the arts. [[Art Pope]], a businessman and philanthropist, is the current President and Chairman of the Board of Directors.<ref>{{cite news |title=The Fisherman’s Friend |author=John J. Miller |newspaper=[[National Review]] |date=December 21, 2009 |url=http://www.heymiller.com/2009/12/the-fishermans-friend }}</ref>

The Pope Foundation “has invested millions in a network of foundations and think tanks, and advocacy groups, both in North Carolina and nationally, that are designed to further conservative and free market ideas,”<ref>[http://projects.newsobserver.com/under_the_dome/art_pope_subject_of_new_yorker_profile Art Pope subject of New Yorker profile | newsobserver.com projects<!-- Bot generated title -->]</ref> including the [[John Locke Foundation]], [[Pope Center|James G. Martin Center for Academic Renewal]], [[Americans for Prosperity]], and North Carolina Institute for Constitutional Law.<ref>{{Cite news|url=http://www.jamesgmartin.center/about/|title=About — The James G. Martin Center for Academic Renewal|newspaper=The James G. Martin Center for Academic Renewal|language=en-US|access-date=2017-01-03}}</ref>

==History==
John William Pope, founder of the retail discount chain [[Variety Wholesalers]], created the Pope Foundation in 1986.

The Pope Foundation celebrated its 25th anniversary in December 2011 by hosting a fundraiser for StepUp Ministry, a nonprofit that assists low-income individuals in getting a job and achieving a stable lifestyle.<ref>{{cite news |title=Pope group steps up for StepUP |newspaper=[[Triangle Business Journal]] |date=November 18, 2011 |url=http://www.bizjournals.com/triangle/print-edition/2011/11/18/pope-group-steps-up-for-stepup.html?page=all}}</ref> The ministry gained $300,000 from the Pope Foundation dinner.

==Philanthropy==
In the 2011-2012 fiscal year, the John William Pope made over $9.3 million in grants, of which 60% went to support North Carolina public policy organizations, 28% went to support educational causes, 7% went to support national public policy organizations, 4% went to support humanitarian organizations, and 1% went to support the arts.<ref>[http://jwpf.org/grants/ John William Pope Foundation Grants]</ref>

The Pope Foundation also underwrites scholarships for Eagle Scouts to attend college. Since the first class of scholars in 2001, the Pope Foundation has invested over $1 million in scouts who plan to pursue careers in the free-enterprise system.<ref>{{cite news |title=Pope Foundation Announces 2013 Class of Eagle Scout Scholars |newspaper=[[jwpf.org]] |date=February 19, 2013 |url=http://jwpf.org/pope-foundation-announces-2013-class-of-eagle-scout-scholars/}}</ref>

The Pope Foundation also has invested heavily in capital construction projects for area universities, including $3 million to UNC-Chapel Hill for the renovation of [[Kenan Memorial Stadium]];<ref>{{cite news |title=UNC-CH receives $3 million for athlete study center |author=Eric Ferreri |newspaper=[[News & Observer]] |date=April 6, 2011 |url=http://www.newsobserver.com/2011/04/06/v-print/1107836/athlete-study-center-funded.html}}</ref> $4.5 million for a convocation center at [[Campbell University]];<ref>{{cite news |title=John William Pope Foundation completes $4.5 million grant to Convocation Center |newspaper=[[Campbell University]] |date=January 15, 2009 |url=http://www.campbell.edu/news/item/John-William-Pope-Foundation-completes-4.5-million-grant-to-Campbell-U}}</ref> and $1.2 million to finish renovations on Campbell University’s law school.<ref>{{cite news |title=John W. Pope Foundation Gifts Campbell Law $1.2 Million for Raleigh Building |newspaper=[[Campbell University]] |date=November 15, 2009 |url=http://www.campbell.edu/news/item/John-W.-Pope-Foundation-Gifts-Campbell-Law-1.2-Million-for-Raleigh-Bui}}</ref>

In December 2012, the Pope Foundation announced $810,500 in grants to community charities, schools, churches, and the arts.<ref>{{cite news |title=Foundation announces $800k in December donations |newspaper=[[News & Observer]] |date=December 13, 2012 |url=http://projects.newsobserver.com/under_the_dome/pope_foundation_announces_800k_in_december_donations}}</ref> To counter the effects of the federal government shutdown in October 2013, the Pope Foundation gave $185,000 in grants to "13 food bank-type groups" in central, eastern, and western North Carolina.,<ref>{{Cite web
| title = Pope Foundation gives money to ease government shutdown
| accessdate = 2013-11-14
| url = http://www.newsobserver.com/2013/10/23/3305878/pope-foundation-gives-money-to.html
}}</ref> including three charities in Vance County.<ref>{{Cite news
| last = SARAH MANSUR
| title = Pope Foundation steps up, helps three in Vance County
| work = The Daily Dispatch
| url = http://www.hendersondispatch.com/news/x2082475029/Pope-Foundation-steps-up-helps-three-in-Vance-County
| date = 2013-10-23
}}</ref> The Pope Foundation gave a total of over $1 million to humanitarian and arts nonprofits in 2013.<ref>{{Cite web
| title = Art Pope is no Grinch
| accessdate = 2013-11-14
| url = http://www.newsobserver.com/2013/12/18/3469445/morning-memo-gop-rivals-use-budget.html
}}</ref>

A study by [[Robert Brulle]] identified the John William Pope Foundation as a funder of think tanks involved in  what he calls the "climate change counter-movement".<ref>{{Cite journal
| doi = 10.1007/s10584-013-1018-7
| issn = 0165-0009
| pages = 1–14
| last = Brulle
| first = Robert J.
| title = Institutionalizing delay: foundation funding and the creation of U.S. climate change counter-movement organizations
| journal = Climatic Change
| accessdate = 2013-12-22
| url = http://link.springer.com/article/10.1007/s10584-013-1018-7
}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.jwpf.org/ John William Pope Foundation]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Foundations based in the United States]]
[[Category:Articles created via the Article Wizard]]
[[Category:Non-profit organizations based in North Carolina]]
[[Category:501(c)(3) nonprofit organizations]]
[[Category:Organizations based in Raleigh, North Carolina]]
[[Category:1986 establishments in North Carolina]]