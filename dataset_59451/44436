{{Infobox NCAA team season
  |Year=1939
  |Team=Alabama Crimson Tide
  |Image=
  |ImageSize=
  |Conference=Southeastern Conference
  |ShortConference=SEC
  |Record=5–3–1
  |ConfRecord=2–3–1
  |HeadCoach=[[Frank Thomas (American football)|Frank Thomas]]
  |HCYear = 9th
  |Captain =Carey Cox
  |StadiumArena=[[Bryant–Denny Stadium|Denny Stadium]]<br/>[[Legion Field]]
}}
{{1939 SEC football standings}}
The '''1939 Alabama Crimson Tide football team''' (variously "Alabama", "UA" or "Bama") represented the [[University of Alabama]] in the [[1939 college football season]]. It was the Crimson Tide's 46th overall and 7th season as a member of the [[Southeastern Conference]] (SEC). The team was led by head coach [[Frank Thomas (American football)|Frank Thomas]], in his ninth year, and played their home games at [[Bryant–Denny Stadium|Denny Stadium]] in [[Tuscaloosa, Alabama|Tuscaloosa]] and [[Legion Field]] in [[Birmingham, Alabama|Birmingham]], [[Alabama]]. They finished the season with a record of five wins, three losses and one tie (5–3–1 overall, 2–3–1 in the SEC).

The Crimson Tide opened the season with a victory over {{cfb link|year=1939|team=Howard Bulldogs|title=Howard}} before they [[upset]] [[1939 Fordham Rams football team|Fordham]] 7–6 in an intersectional contest at the [[Polo Grounds]] in week two. After their victory over {{cfb link|year=1939|team=Mercer Bears|title=Mercer}}, Alabama was shut out 21–0 by [[1939 Tennessee Volunteers football team|Tennessee]], their second consecutive shutout loss against the Volunteers. The Crimson Tide then rebounded with a [[homecoming]] victory over [[1939 Mississippi State Maroons football team|Mississippi State]]. However, Alabama would then go winless over their next three conference games with a tie against [[1939 Kentucky Wildcats football team|Kentucky]] followed by shutout losses to both [[1939 Tulane Green Wave football team|Tulane]] and [[1939 Georgia Tech Yellow Jackets football team|Georgia Tech]]. The Crimson Tide rebounded in their final game of the season to defeat [[1939 Vanderbilt Commodores football team|Vanderbilt]].

==Schedule==
{{CFB Schedule Start | time = no | rankyear = 1939 | tv = no | attend = yes }}
{{CFB Schedule Entry
| w/l          = w
| date         = September 30
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1939|team=Howard Bulldogs|title=Howard}}
| site_stadium = [[Bryant–Denny Stadium|Denny Stadium]]
| site_cityst  = [[Tuscaloosa, Alabama|Tuscaloosa, AL]]
| tv           = no
| score        = 21–0
| attend       = 6,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 7
| away         = yes
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = [[1939 Fordham Rams football team|Fordham]]
| site_stadium = [[Polo Grounds]]
| site_cityst  = [[New York City|New York, NY]]
| tv           = no
| score        = 7–6
| attend       = 41,454
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 14
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1939|team=Mercer Bears|title=Mercer}}
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 20–0
| attend       = 5,000
}}
{{CFB Schedule Entry
| w/l          = l
| date         = October 21
| away         = yes
| time         = no
| rank         = 8
| opprank      = 5
| opponent     = [[1939 Tennessee Volunteers football team|Tennessee]]
| site_stadium = [[Neyland Stadium|Shields-Watkins Field]]
| site_cityst  = [[Knoxville, Tennessee|Knoxville, TN]]
| gamename     = [[Third Saturday in October]]
| tv           = no
| score        = 0–21
| attend       = 40,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 28
| homecoming   = yes
| time         = no
| rank         = 20
| opponent     = [[1939 Mississippi State Maroons football team|Mississippi State]]
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| gamename     = [[Alabama–Mississippi State football rivalry|Rivalry]]
| tv           = no
| score        = 7–0
| attend       = 15,000
}}
{{CFB Schedule Entry
| w/l          = t
| date         = November 4
| time         = no
| rank         = 19
| opprank      = 15
| opponent     = [[1939 Kentucky Wildcats football team|Kentucky]]
| site_stadium = [[Legion Field]]
| site_cityst  = [[Birmingham, Alabama|Birmingham, AL]]
| tv           = no
| score        = 7–7
| attend       = 11,000
}}
{{CFB Schedule Entry
| w/l          = l
| date         = November 11
| away         = yes
| time         = no
| rank         = 
| opprank      = 7
| opponent     = [[1939 Tulane Green Wave football team|Tulane]]
| site_stadium = [[Tulane Stadium]]
| site_cityst  = [[New Orleans|New Orleans, LA]]
| tv           = no
| score        = 0–13
| attend       = 52,000
}}
{{CFB Schedule Entry
| w/l          = l
| date         = November 18
| time         = no
| rank         = 
| opponent     = [[1939 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| tv           = no
| score        = 0–6
| attend       = 23,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 30
| away         = yes
| time         = no
| rank         = 
| opponent     = [[1939 Vanderbilt Commodores football team|Vanderbilt]]
| site_stadium = [[Vanderbilt Stadium|Dudley Field]]
| site_cityst  = [[Nashville, Tennessee|Nashville, TN]]
| tv           = no
| score        = 39–0
| attend       = &nbsp;
}}
{{CFB Schedule End
| rank     = 
| timezone = 
| poll     = [[AP Poll]]
| hc       = yes
}}
*<small>Source: Rolltide.com: 1939 Alabama football schedule<ref name="1939schedule">{{cite web| url=http://www.rolltide.com/sports/m-footbl/archive/m-footbl-results-archive.html#1939 |title=1939 Alabama football schedule |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics| accessdate=April 15, 2012}}</ref></small>

==Game summaries==

===Howard===
{{See also2|{{cfb link|year=1939|team=Howard Bulldogs}}}}
{{AFB game box start
|Visitor=Howard
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=0 |H2=0 |H3=7 |H4=14
|Date=September 30
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=6,000
}}
*'''Source:'''<ref name="HU1">{{cite news |title=Slow starting Tide turns back game Howard crew, 21 to 0 |url=https://news.google.com/newspapers?id=_ExBAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6359%2C3887141 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 1, 1939 |page=6 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
To open the 1939 season, Alabama defeated Howard (now [[Samford University]]) 21–0 at Denny Stadium.<ref name="HU1"/><ref name=a1>1939 Season Recap</ref> After a scoreless first half, the Crimson Tide scored their first touchdown on a one-yard Paul Spencer run in the third quarter.<ref name="HU1"/> Alabama then closed the game with a pair of fourth-quarter touchdowns for the 21–0 win. The first came on a five-yard Herschel Mosley pass to [[Holt Rast]] and the second on a second, one-yard run by Spencer.<ref name="HU1"/> The victory improved Alabama's all-time record against Howard to 17–0–1.<ref name="HAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Samford |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2867 |accessdate=April 14, 2012}}</ref>
{{clear}}

===Fordham===
{{see also|1939 Fordham Rams football team}}
{{AFB game box start
|Visitor='''Alabama'''
|V1=7 |V2=0 |V3=0 |V4=0
|Host=Fordham
|H1=0 |H2=0 |H3=0 |H4=6
|Date=October 7
|Location=Polo Grounds<br/>New York, NY
|Attendance=41,454
}}
*'''Sources:'''<ref name="FU1">{{cite news |title=Inspired Tide overpowers Rams to edge out 7 to 6 victory |url=https://news.google.com/newspapers?id=Ak1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6374%2C4201095 |first=Bill |last=Boni |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 8, 1939 |page=10 |accessdate=April 15, 2012}}</ref><ref name="FU2">{{cite news |title=Ram is outplayed |first=Allison |last=Danzig |publisher=ProQuest Historical Newspapers |newspaper=The New York Times |date=October 8, 1939 |page=85}}</ref>
{{AFB game box end}}
On the road against a favored [[Fordham University|Fordham]] squad, the Crimson Tide defeated the [[Fordham Rams football|Rams]] 7–6 at the [[Polo Grounds]] in an intersectional matchup.<ref name=a1/><ref name="FU1"/><ref name="FU2"/> The Crimson Tide scored their only points of the game in the first quarter. Jimmy Nelson scored Alabama's only touchdown on an 18-yard run to cap a 40-yard drive, and then [[Hayward Sanford]] connected on the [[Conversion (gridiron football)|extra point]] to give the Crimson Tide a 7–0 lead.<ref name="FU1"/> Sanford later missed a 33-yard [[Field goal (American and Canadian football)|field goal]] in the first, and the Rams turned the ball over on downs at the Alabama 19-yard line to keep the score 7–0 at the end of the quarter.<ref name="FU1"/><ref name="FU2"/> Fordham then scored their only points of the game late in the fourth after Dom Principe scored on a short touchdown run; however, Alex Yudikaitis missed the extra point which proved to be the margin in their loss.<ref name="FU2"/> The victory improved Alabama's all-time record against Fordham to 1–1.<ref name="FUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Fordham |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1124 |accessdate=April 15, 2012}}</ref>

This game is also noted as being the second televised [[college football]] game after the [[1939 Waynesburg vs. Fordham football game]] played one week earlier.<ref name="2ndTV">{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=Televised Games |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |page=131 |url=http://grfx.cstv.com/photos/schools/alab/sports/m-footbl/auto_pdf/history-awards.pdf |accessdate=April 17, 2012 |format=PDF}}</ref> The game was televised exclusively in New York City as it was broadcast over [[W2XBS]] and only a few hundred televisions were thought to be in existence at the time.<ref name="2ndTV"/>
{{clear}}

===Mercer===
{{See also2|{{cfb link|year=1939|team=Mercer Bears}}}}
{{AFB game box start
|Visitor=Mercer
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=7 |H2=0 |H3=13 |H4=0
|Date=October 14
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=5,000
}}
*'''Source:'''<ref name="MU1">{{cite news |title=Uninspired Tide rolls over Mercer, 20 to 0 |url=https://news.google.com/newspapers?id=CE1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6366%2C4544689 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 15, 1939 |page=10 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
A week after the road win at Fordham, Alabama defeated the [[Mercer University|Mercer]] [[Mercer Bears football|Bears]] 20–0 at Denny Stadium in the first all-time meeting between the schools.<ref name=a1/><ref name="MU1"/><ref name="MAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Mercer |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1944 |accessdate=April 15, 2012}}</ref> The Crimson Tide took a 7–0 first quarter lead after Paul Spencer scored on a one-yard touchdown run to cap a 42-yard drive.<ref name="MU1"/> After a scoreless second quarter, Alabama scored a pair of third-quarter touchdowns for the 20–0 victory. Gene Blackwell scored first on an eight-yard run and Herschel Mosley scored on a four-yard run.<ref name="MU1"/>
{{clear}}

===Tennessee===
{{See also|1939 Tennessee Volunteers football team}}
{{AFB game box start
|Title=[[Third Saturday in October]]
|Visitor=#8 Alabama
|V1=0 |V2=0 |V3=0 |V4=0
|Host=#5 '''Tennessee'''
|H1=0 |H2=7 |H3=0 |H4=14
|Date=October 21
|Location=Shields-Watkins Field<br/>Knoxville, TN
|Attendance=40,000
}}
*'''Source:'''<ref name="UT1">{{cite news |title=40,000 see Vols triumph over Crimson Tide, 21 to 0 |url=https://news.google.com/newspapers?id=Dk1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6382%2C4828738 |first=Kenneth |last=Gregory |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 22, 1939 |page=10 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
On the Monday prior to their annual game against the [[Tennessee Volunteers football|Volunteers]], Alabama was selected to the No. 8 and Tennessee was selected to the No. 5 position in the first [[AP Poll]] of the 1939 season.<ref name="UT2">{{cite news |title=Pitt wins top in grid poll |url=https://news.google.com/newspapers?id=C01BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6320%2C4644115 |first=Bill |last=Boni |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 17, 1939 |page=8 |accessdate=April 15, 2012}}</ref> In the game, Alabama was [[shutout|shut out]] by [[Third Saturday in October|rival]] [[University of Tennessee|Tennessee]] 21–0 before an overflow crowd of 40,000 at Shields-Watkins Field.<ref name=a1/><ref name="UT1"/> After a scoreless first quarter, Tennessee took a 7–0 lead in the second after Johnny Butler scored on a 56-yard run.<ref name="UT1"/> Up by a touchdown at the end of the third, a pair of fourth-quarter touchdown runs gave the Volunteers the 21–0 win. The first was made by Bob Foxx on an 11-yard run and the second by [[Buss Warren]] om a 12-yard run.<ref name="UT1"/>

Although Alabama was shut out and lost by three touchdowns, [[List of Tennessee Volunteers head football coaches|Tennessee head coach]] [[Robert Neyland]] said of the Crimson Tide's performance that "I don't think the score indicates the difference between the teams. It should have been about 7 to 0."<ref name="UT3">{{cite news |title=Should have been 7–0 says Major Neyland |url=https://news.google.com/newspapers?id=Dk1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=5660%2C4828876 |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 22, 1939 |page=10 |accessdate=April 15, 2012}}</ref> The loss brought Alabama's all-time record against Tennessee to 13–7–2.<ref name="TNAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tennessee |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3180 |accessdate=April 15, 2012}}</ref>
{{clear}}

===Mississippi State===
{{see also|1939 Mississippi State Maroons football team}}
{{AFB game box start
|Visitor=Mississippi State
|V1=0 |V2=0 |V3=0 |V4=0
|Host=#20 '''Alabama'''
|H1=7 |H2=0 |H3=0 |H4=0
|Date=October 28
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=15,000
}}
*'''Source:'''<ref name="MSS1">{{cite news |title=Alabama pass turns back strong Maroon invaders, 7 to 0 |url=https://news.google.com/newspapers?id=FE1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6353%2C5113269 |publisher=Google News Archives |newspaper=The Tuscaloosa News |page=10 |date=October 29, 1939 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
After their loss to Tennessee, Alabama dropped from No. 8 to No. 20 in the AP Poll as they entered their annual [[homecoming]] game.<ref name="MSS2">{{cite news |title=Vols acclaimed best in nation |url=https://news.google.com/newspapers?id=EU1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6346%2C4928542 |first=Bill |last=White |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 24, 1939 |page=8 |accessdate=April 15, 2012}}</ref> Against [[Mississippi State University|Mississippi State]] the Crimson Tide defeated the [[Mississippi State Maroons football|Maroons]] 7–0 before 15,000 fans at Denny Stadium.<ref name=a1/><ref name="MSS1"/> The only points of the game came in the first quarter when [[Charley Boswell]] threw a four-yard touchdown pass to [[Holt Rast]].<ref name="MSS1"/> Alabama outgained the Maroons in rushing yardage 173 to 65 in the victory.<ref name="MSS1"/> The victory improved Alabama's all-time record against Mississippi State to 19–5–2.<ref name="MSSAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Mississippi State |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2049 |accessdate=April 15, 2012}}</ref>
{{clear}}

===Kentucky===
{{See also2|[[1939 Kentucky Wildcats football team]]}}
{{AFB game box start
|Visitor= #15 Kentucky
|V1=0 |V2=0 |V3=0 |V4=7
|Host= #19 Alabama
|H1=0 |H2=7 |H3=0 |H4=0
|Date=November 4
|Location=Legion Field<br>Birmingham, AL
|Attendance=11,000
}}
*'''Source:'''<ref name="KY1">{{cite news |title=Alabama, Kentucky battle to spectacular tie, 7–7 |url=https://news.google.com/newspapers?id=Gk1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6307%2C5431371 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 5, 1939 |page=11 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
As Alabama entered their contest against [[University of Kentucky|Kentucky]], they gained one position the rankings to No. 19 and the [[Kentucky Wildcats football|Wildcats]] entered the rankings at No. 15 in the weekly AP Poll.<ref name="KY2">{{cite news |title=Vols continue in No. 1 spot |first=Bill |last=Boni |agency=Associated Press |url=https://news.google.com/newspapers?id=Fk1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6366%2C5212980 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 31, 1939 |page=8 |accessdate=April 14, 2012}}</ref> In the game, the Crimson Tide battled the Wildcats to a 7–7 tie in the first game played at Legion Field of the season.<ref name=a1/><ref name="KY1"/> After a scoreless first, [[Holt Rast]] blocked a Kentucky [[Punt (gridiron football)|punt]] that was recovered by Alabama at the Wildcats' two-yard line.<ref name="KY1"/> Two plays later, Paul Spencer scored on a short run and Bud Waites converted the [[Conversion (gridiron football)|extra point]] to give the Crimson Tide a 7–0 lead.<ref name="KY1"/> Still down by a touchdown at the end of the third, Kentucky tied the game in the fourth on a short Noah Mullins run and Jim Hardin extra point.<ref name="KY1"/> The tie brought Alabama's all-time record against Kentucky 17–1–1.<ref name="KYAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Kentucky |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1628 |accessdate=April 15, 2012}}</ref>
{{clear}}

===Tulane===
{{see also|1939 Tulane Green Wave football team}}
{{AFB game box start
|Visitor= Alabama
|V1=0 |V2=0 |V3=0 |V4=0
|Host= #7 '''Tulane'''
|H1=0 |H2=6 |H3=7 |H4=0
|Date=November 11
|Location=Tulane Stadium<br/>New Orleans, LA
|Attendance=52,000
}}
*'''Source:'''<ref name="TU1">{{cite news |title=52,000 see Green Wave roll over Crimson Tide, 13 to 0 |first=Ben A. |last=Green |url=https://news.google.com/newspapers?id=IE1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6211%2C5711499 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 12, 1939 |page=10 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
After their tie with Kentucky, the Crimson Tide dropped out of the weekly AP Poll, and [[Tulane University|Tulane]] took the No. 7 position after their victory over {{cfb link|year=1939|team=Ole Miss Rebels|title=Ole Miss}}.<ref name="TU2">{{cite news |title=Vols increase lead in voting |first=Hugh S. |last=Fullerton, Jr. |agency=Associated Press |url=https://news.google.com/newspapers?id=HU1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6377%2C5524750 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 7, 1939 |page=8 |accessdate=April 15, 2012}}</ref> In New Orleans, the Crimson Tide was [[shutout]] by the [[Tulane Green Wave football|Green Wave]] 13–0 before a crowd of 52,000 at Tulane Stadium.<ref name=a1/><ref name="TU1"/> After a scoreless first, Tulane took a 6–0 halftime lead when Harry Hays scored a touchdown on a 69-yard [[Reverse (American football)|reverse]].<ref name="TU1"/> Robert Kellogg then scored the Green Wave's other touchdown in the third with his three-yard run.<ref name="TU1"/> The loss brought Alabama's all-time record against Tulane to 12–4–1.<ref name="TUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tulane |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3267 |accessdate=April 15, 2012}}</ref>
{{clear}}

===Georgia Tech===
{{see also|1939 Georgia Tech Yellow Jackets football team}}
{{AFB game box start
|Visitor='''Georgia Tech'''
|V1=6 |V2=0 |V3=0 |V4=0
|Host=Alabama
|H1=0 |H2=0 |H3=0 |H4=0
|Date=November 18
|Location=Legion Field<br>Birmingham, AL
|Attendance=23,000
}}
*'''Source:'''<ref name="GT1">{{cite news |title=Tornado breaks Alabama jinx with thrilling 6 to 0 victory |url=https://news.google.com/newspapers?id=Jk1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6267%2C6000137 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 19, 1939 |page=11 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
In their final home game of the season game against [[Georgia Institute of Technology|Georgia Tech]] Alabama lost their second consecutive game by a shutout, 6–0 against the [[Georgia Tech Yellow Jackets football|Yellow Jackets]] at Legion Field.<ref name=a1/><ref name="GT1"/> The only score of the game was set up after R. W. Murphy recovered a John Hanson [[fumble]] at the Alabama 38-yard line. Three plays later the Yellow Jackets scored on a 24-yard E. M. Wheby touchdown reception from Johnny Bosch, and after [[Holt Rast]] blocked the extra point attempt, Georgia Tech led 6–0.<ref name="GT1"/> The loss brought Alabama's all-time record against Georgia Tech to 11–11–3.<ref name="GTAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Georgia Tech |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1273 |accessdate=April 15, 2012}}</ref>
{{clear}}

===Vanderbilt===
{{see also|1939 Vanderbilt Commodores football team}}
{{AFB game box start
|Visitor= '''Alabama'''
|V1=13 |V2=6 |V3=7 |V4=13
|Host= Vanderbilt
|H1=0 |H2=0 |H3=0 |H4=0
|Date=November 30
|Location=Dudley Field<br>Nashville, TN
}}
*'''Source:'''<ref name="VU1">{{cite news |title=Tide overwhelms Vandy 39–0 to close season |url=https://news.google.com/newspapers?id=MU1BAAAAIBAJ&sjid=lLcMAAAAIBAJ&pg=6436%2C6550606 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 30, 1939 |page=9 |accessdate=April 15, 2012}}</ref>
{{AFB game box end}}
In their season finale against the [[Vanderbilt University|Vanderbilt]] [[Vanderbilt Commodores football|Commodores]], Alabama won 39–0 at Dudley Field on [[Thanksgiving (United States)|Thanksgiving Day]] to end a two-game losing streak.<ref name=a1/><ref name="VU1"/> In the first half touchdowns were scored on a Jimmy Nelson touchdown reception in the first and by a 77-yard Herschel Mosley run and a 67-yard Paul Spencer run in the second.<ref name="VU1"/> Up by three touchdowns at halftime, the Crimson Tide scored three second half touchdowns in the 39–0 victory. Second half touchdowns were scored on a 20-yard Jimmy Nelson reception and by Spencer on an eight-yard run and by Hal Newman on an 18-yard reception from Billy Harrell as time expired.<ref name="VU1"/> The victory improved Alabama's all-time record against Vanderbilt to 12–9.<ref name="VUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Vanderbilt |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3363 |accessdate=April 15, 2012}}</ref>
{{clear}}

==After the season==

===NFL Draft===
Several players that were [[Letterman (sports)|varsity lettermen]] from the 1939 squad were drafted into the [[National Football League Draft|National Football League (NFL)]] between the 1940 and 1942 drafts.<ref>{{cite web |url=http://www.pro-football-reference.com/colleges/alabama/drafted.htm |title=Alabama Drafted Players/Alumni |accessdate=April 7, 2012 |work=Sports Reference, LLC |publisher=Pro-Football-Reference.com}}</ref><ref name="NFLDraft">{{cite web |publisher=National Football League | url=http://www.nfl.com/draft/history/fulldraft?abbr=A&collegeName=Alabama&abbrFlag=0&type=school | title=Draft History by School–Alabama|accessdate=March 16, 2013}}</ref> These players included the following:
{| class="wikitable sortable" style="text-align:center"
! scope="col" | Year
! scope="col" | Round
! scope="col" | Overall
! scope="col" | Player name
! scope="col" | Position
! scope="col" | NFL team
|-
| rowspan=4|[[1940 NFL Draft|1940]]
| 4 
| 30 
! {{Sortname|Bobby|Wood|Bobby Wood (American football)}}
| Tackle 
| [[1940 Cleveland Rams season|Cleveland Rams]]
|-
| 5 
| 34 
! {{Sortname|Walt|Merrill|nolink=1}} 
| Tackle 
| [[1940 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 11 
| 93 
! {{Sortname|Cary|Cox|nolink=1}}
| Center 
| [[1940 Pittsburgh Steelers season|Pittsburgh Steelers]]
|-
| 11 
| 138 
! {{Sortname|Hayward|Sanford|Sandy Sanford}}
| End 
| [[1940 Washington Redskins season|Washington Redskins]]
|-
| rowspan=3|[[1941 NFL Draft|1941]]
| 3 
| 25 
! {{Sortname|Fred|Davis|Fred Davis (defensive lineman)}}
| Tackle 
| [[1941 Washington Redskins season|Washington Redskins]]
|-
| 7 
| 58 
! {{Sortname|Hal|Newman|nolink=1}}
| End 
| [[1941 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 10 
| 90 
! {{Sortname|Ed|Hickerson|nolink=1}}
| Guard 
| Washington Redskins
|-
| rowspan=3|[[1942 NFL Draft|1942]]
| 14 
| 123 
! {{Sortname|John|Wyhonic|nolink=1}}
| Guard 
| [[1942 Philadelphia Eagles season|Philadelphia Eagles]]
|-
| 18 
| 170 
! {{Sortname|Holt|Rast}}
| End 
| [[1942 Chicago Bears season|Chicago Bears]]
|-
| 19 
| 174 
! {{Sortname|Jimmy|Nelson|nolink=1}}
| Back 
| [[1942 Chicago Cardinals season|Chicago Cardinals]]
|}

==Personnel==
{{Col-begin}}
{{Col-2}}

===Varsity letter winners===
{| class="wikitable" border="1"
|-;
! Player
! Hometown
! Position
|-
| Warren Averitte
| [[Greenville, Mississippi]]
| [[Center (American football)|Center]]
|-
| Gene Blackwell
| [[Blytheville, Arkansas]]
| [[End (American football)|End]]
|-
| Tom Borders
| [[Birmingham, Alabama]]
| [[Tackle (American football)|Tackle]]
|-
| [[Charley Boswell]]
| [[Birmingham, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Carey Cox
| [[Bainbridge, Georgia]]
| [[Center (American football)|Center]]
|-
| [[Fred Davis (defensive lineman)|Fred Davis]]
| [[Louisville, Kentucky]]
| [[Tackle (American football)|Tackle]]
|-
| John Hanson
| [[Roanoke, Alabama]]
| [[Fullback (American football)|Fullback]]
|-
| Walter Merrill
| [[Andalusia, Alabama]]
| [[Tackle (American football)|Tackle]]
|-
| Herschel Mosley
| [[Blytheville, Arkansas]]
| [[Halfback (American football)|Halfback]]
|-
| Jimmy Nelson
| [[Live Oak, Florida]]
| [[Halfback (American football)|Halfback]]
|-
| Hal Newman
| [[Birmingham, Alabama]]
| [[End (American football)|End]]
|-
| [[Holt Rast]]
| [[Birmingham, Alabama]]
| [[End (American football)|End]]
|-
| Perron Shoemaker
| [[Birmingham, Alabama]]
| [[End (American football)|End]]
|-
| Paul Spencer
| [[Hampton, Virginia]]
| [[Fullback (American football)|Fullback]]
|-
| Joseph Sugg
| [[Russellville, Alabama]]
| [[Guard (American football)|Guard]]
|-
| W. L. Waites
| [[Tuscaloosa, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Erin Warren
| [[Montgomery, Alabama]]
| [[End (American football)|End]]
|-
| Dallas Wicke
| [[Pensacola, Florida]]
| [[Quarterback]]
|-
| John Wyhonic
| [[Connorville, Ohio]]
| [[Guard (American football)|Guard]]
|-
|colspan="3" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Tide Football Lettermen |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=127–141}}</ref>
|}
{{Col-2}}

===Coaching staff===
{| class="wikitable" border="1" style="font-size:90%;"
|-
! Name !! Position !! Seasons at<br />Alabama !! Alma Mater
|-
| [[Frank Thomas (American football)|Frank Thomas]] || [[Head coach]] ||align=center| 9 || [[Notre Dame Fighting Irish football|Notre Dame]] (1923)
|-
| [[Bear Bryant]] || Assistant coach ||align=center| 4 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Paul Burnum]] || Assistant coach ||align=center| 10 || [[Alabama Crimson Tide football|Alabama]] (1922)
|-
| [[Tilden Campbell]] || Assistant coach ||align=center| 4 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Hank Crisp]] || Assistant coach ||align=center| 19 || [[Virginia Tech Hokies football|VPI]] (1920)
|-
| [[Harold Drew]] || Assistant coach ||align=center| 9 || [[Bates Bobcats football|Bates]] (1916)
|-
| Joe Kilgrow || Assistant coach ||align=center| 2 || [[Alabama Crimson Tide football|Alabama]] (1937)
|-
|colspan="4" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Assistant Coaches |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=142–143}}</ref>
|}
{{Col-2}}
{{Col-end}}

==References==
'''General'''
{{refbegin}}
* {{cite web |url=http://www.rolltide.com/datadump/fls_files/files/football/1930s/1939.pdf |title=1939 Season Recap |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics |accessdate=April 15, 2012 |format=PDF}}
{{refend}}

'''Specific'''
{{Reflist|30em}}

{{Alabama Crimson Tide football navbox}}

[[Category:Alabama Crimson Tide football seasons]]
[[Category:1939 Southeastern Conference football season|Alabama]]
[[Category:1939 in Alabama|Crimson Tide]]