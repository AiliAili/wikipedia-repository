{{Infobox company
| name             = Messerschmitt-Bölkow-Blohm
| logo             = [[File:MBB-Logo.jpg|180px]]
| type             = 
| genre            = [[Aerospace]]
| fate             = merged
| predecessor      = [[Messerschmitt]] AG and [[Bölkow]] (1968) <br> [[Hamburger Flugzeugbau]] (1969)
| successor        = [[DASA]] (Deutsche Aerospace AG)
| foundation       = 1968
| founder          =  
| defunct          = 1989
| location_city    = [[Ottobrunn]]
| location_country = [[West Germany]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = 
| industry         = [[Aerospace]]-And some railroad equipment for the [[MBTA Commuter Rail]]
| products         = [[Helicopter]]s and [[airliner]] components
| services         = 
| market cap       = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = 
| footnotes        = 
| intl             = 
}}

'''Messerschmitt-Bölkow-Blohm (MBB)''' was a [[Germany|German]] [[aerospace manufacturer]] formed as the result of several mergers in the late 1960s. Among its best-known products was the [[MBB Bo 105]] light twin helicopter. The company was bought by [[DASA]] (Deutsche Aerospace AG) in 1989, now part of [[Airbus Group]].

==History==
[[File:Bo-105.jpg|thumb|right|The fourth MBB Bo 105 prototype in the Deutsches Museum in Munich]]
[[File:BK-117 Polizei-NRW D-HNWL.jpg|thumb|right|A BK 117 B2 of the [[Landespolizei|German Police]],  in Düsseldorf, October 2005]]

On 6 June 1968, [[Messerschmitt]] AG merged with the small civil engineering and civil aviation firm [[Bölkow]], becoming Messerschmitt-Bölkow. The following May, the firm acquired [[Hamburger Flugzeugbau]] (HFB), the aviation division of [[Blohm + Voss]]. The company then changed its name to Messerschmitt-Bölkow-Blohm (MBB).<ref>{{cite journal|magazine=Air Progress|date=August 1989|page=76|author=Nick Komos}}</ref>

Originally 51% of MBB was owned by the Blohm family, [[Willy Messerschmitt]] and [[Ludwig Bölkow]]. 22.07% was owned by the German State of [[Hamburg]], 17.05% by the state of [[Bavaria]], 7.16% by [[ThyssenKrupp|Thyssen AG]], 7.16% by [[Siemens AG]], 7.13% by [[Allianz|Allianz Versicherungs-AG]], 7.13% by [[Robert Bosch GmbH]] and 6.15% by [[Krupp|Friedrich Krupp GmbH]].<ref>Commerzbank AG: "Wer gehört zu wem", Hamburg 1985, 15. Auflage</ref>

In 1981, MBB acquired the [[Vereinigte Flugtechnische Werke|Vereinigte Flugtechnische Werke (VFW)]], which itself had been formed by merging [[Focke-Wulf]], [[Focke-Achgelis]] and [[Weser Flugzeugbau|Weserflug]]. In the following year, MBB acquired the astronautics company [[Entwicklungsring Nord|Entwicklungsring Nord (ERNO)]] and became MBB-ERNO.

In 1989 MBB was taken over by "Deutsche Aerospace AG" ([[DASA]]), which was renamed "Daimler-Benz Aerospace" in 1995. With the 1998 merger of [[Daimler Benz]] and [[Chrysler|Chrysler Corporation]], the aerospace division was renamed [[DaimlerChrysler Aerospace]] AG on 7 November 1998. European defense consolidation led to DASA's being merged with [[Aerospatiale-Matra]] of France and [[Construcciones Aeronáuticas SA]] (CASA) of Spain to form the [[EADS|European Aeronautic Defence and Space Company (EADS)]] in 2000. The former DaimlerChrysler Aerospace now operates as "EADS Germany".<ref>Answers.com (n.d.). [http://www.answers.com/topic/messerschmitt-b-lkow-blohm-gmbh].  Retrieved 22 March 2007.</ref>

==Subsidiaries==
*MBB-Liftsystems AG, which produces lifting systems for trucks and vans
*MBB-Sondertechnik, (today FHS Förder– und Hebesysteme GmbH) developed wind rotors in the 1980s and 1990s, and lifting systems for military use. 
* MBB Gelma GmbH, produces timekeeping units and machine control units (today owned by [[DORMA|DORMA KG]])
* MBB Group AG

==Aircraft==
[[File:MBB Bo 108.jpg|thumb|right|MBB Bo 108, later known as Eurocopter EC 135]]
* [[MBB Lampyridae]]
* [[Bolkow Bo 102|MBB Bo 102]]
* [[Bolkow Bo 103|MBB Bo 103]]
* [[MBB Bo 105]]
* [[MBB Bo 106]]
* MBB Bo 108 - became the [[Eurocopter EC 135]]
* [[MBB Bo 115]]
* [[MBB Bo 209]]
* [[MBB/Kawasaki BK 117]]
* [[MBB 223 Flamingo]]
* MBB/[[Hamburger Flugzeugbau HFB-320 Hansa Jet]]
* [[Lockheed F-104 Starfighter|MBB F-104G/CCV]] (CCV Program)

===Partnerships===
[[File:Rockwell-MBB X-31 landing.JPG|thumb|right|Rockwell-MBB X-31, one of two X-31 Enhanced Fighter Maneuverability Demonstrator aircraft (top)]]
* [[Airbus A300]]
* [[Airbus A310]]
* [[Airbus A320 family]]
* [[Eurofighter Typhoon]]
* [[Panavia Tornado]]
* [[Rockwell-MBB X-31]]
* [[Transall C-160]]
* [[MPC 75]]

==Missiles==
* [[AS.34 Kormoran]]
* [[Cobra (missile)]]

===Partnerships===
* [[HOT (missile)]]
* [[MILAN]]
* [[Roland (missile)]]

==Space Hardware==
* [[Helios (spacecraft)]]
* [[Symphonie]]

===Manned spacecraft===
* [[Spacelab]]

==References==
{{reflist}}

* {{cite book |last= Gunston |first= Bill |coauthors= |title= World Encyclopedia of Aircraft Manufacturers, 2nd Edition |year= 2005 |publisher= Sutton Publishing Limited |location= Phoenix Mill, Gloucestershire, England, UK |isbn= 0-7509-3981-8 |pages= 164 }}

==External links==
{{commons category|Messerschmitt-Bölkow-Blohm}}
* [http://www.airbus-group.com/airbusgroup/int/en.html Airbus Group]
* [http://www.mbbindustries.com/kunden/mbb/ttw.nsf/id/EN_Historie?opendocument MBB Industries AG]
* [http://www.mbb.ag/english/mbb_group_ag/mbb_history MBB Group AG]
* [http://www.mbb-projects.com/ MBB Projects GmbH]
* [http://www.eurocopter.com/site/en/ref/MBB_355.html MBB and Eurocopter history]
* [http://www.mbb.de MBB Palfinger GmbH]
* [http://www.wtec.org/loyola/satcom2/b_07.htm About Daimler-Benz Aerospace]
* [http://magnetbahnforum.de/index.php?MBB-KOMET MBB KOMET, the first high-speed maglev]
* [http://www.airbus-group.com/airbusgroup/int/en/news/events-press/airshows/100-Years-Ludwig-B-lkow.html Airbus Group: Aerospace pioneer Ludwig Bölkow]

{{MBB aircraft}}

{{DEFAULTSORT:Messerschmitt-Bolkow-Blohm}}
[[Category:Airframe manufacturers of Germany]]
[[Category:Companies based in Bavaria]]
[[Category:Defence companies of Germany]]
[[Category:Defunct motor vehicle manufacturers of Germany]]
[[Category:Helicopter manufacturers of Germany]]
[[Category:Aerospace companies of Germany]]