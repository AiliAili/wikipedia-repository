{{Infobox Organization
| name = Open Access Scholarly Publishers Association
| image = OASPA Logo.jpg
| abbreviation = OASPA
| size =
| motto = 
| type = [[List of international professional associations|International professional association]]
| formation =  14 October 2008
| headquarters = Online
| membership = Scholarly open access publishers
| leader_title = President
| leader_name = Paul Peters
| language = English
| website= {{url|http://www.oaspa.org/|oaspa.org}}
}}
The '''Open Access Scholarly Publishers Association''' ('''OASPA''') is a non-profit trade association representing the interests of [[open access (publishing)|open access]] journal publishers globally in all scientific, technical and scholarly disciplines. Along with promoting open access publishers (particularly [[open access journals]]), OASPA sets best practices and provides a forum for the exchange of information on and experiences of open access. OASPA brings together the major open access publishers on the one hand and independent&mdash;often [[learned society|society-based]] or university-based&mdash;publishers on the other, along with some [[hybrid open access journal|hybrid open access publisher]]s. While having started out with an exclusive focus on open access journals, it is now expanding its activities to include matters pertaining to [[open access book]]s too.<ref name=oaspa>''This article incorporates material from the [http://www.oaspa.org OASPA website], which is licensed under the [[Creative Commons licenses|Creative Commons Attribution License]].''</ref>

== Mission ==
The mission of OASPA is to support and represent the interests of open access publishers globally in all scientific, technical, and scholarly disciplines, and to advocate for Open Access journals in general.<ref>[http://www.oaspa.org/mission.php OASPA Mission], accessed Nov 28, 2010</ref> To this end, it provides a forum for professional exchange on matters of open access publishing in scholarly contexts, it engages in standardization efforts and outreach, identifies and promotes best practices for scholarly communications by open access, and supports the continuous development of viable business and publishing models.

== History ==
With the growth of the [[Open Access movement|open access movement]], the interactions between different open access publishers intensified, as they met each other at a multitude of trade or scientific conferences, workshops or similar events. Yet open access publishing and its peculiarities with respect to traditional publishing or scholarly communication were rarely in the focus of such gatherings, which brought about the need for a dedicated forum. With the intention to provide that, OASPA was launched on October 14, 2008 at an "Open Access Day" celebration in London hosted by the [[Wellcome Trust]].<ref>[http://www.oaspa.org/history.php OASPA History], accessed Nov 28, 2010</ref><ref>[http://www.sparceurope.org/news/new-open-access-scholarly-publishers-association-oaspa-launched New Open Access Scholarly Publishers Association (OASPA) Launched], a report by [[Scholarly Publishing and Academic Resources Coalition|SPARC Europe]], accessed Nov 28, 2010</ref><ref>[http://www.atypon-link.com/SCR/doi/abs/10.5555/scrn.12.10.5 Launch of Open Access Scholarly Publishers Association (OASPA)]. ''Scholarly Communications Report'' 12(10):5 (2008).</ref>
{{anchor|Founding members}}
The following organizations are founding members:<ref>{{cite web |url=http://oaspa.org/about/founding-members/ |title=Founding Members |work=Open Access Scholarly Publishers Association |accessdate=2015-01-06}}</ref>
{{div col|colwidth=15em}}
* [[BioMed Central]]
* [[Co-Action Publishing]]
* [[Copernicus Publications]]
* [[Hindawi Publishing Corporation]]
* [[JMIR Publications]]
* Medical Education Online (David Solomon
* [[Public Library of Science]]
* [[Sage Publications]]
* [[SPARC Europe]]
* [[Utrecht University Library]]
{{div col end}}

== Activities ==
OASPA organizes an annual Conference on Open Access Scholarly Publishing.<ref>[http://www.oaspa.org/coasp/index.php COASP homepage], accessed Feb 13, 2011</ref> The conference covers the whole spectrum of open access publishing, including [[business model]]s, publishing platforms, [[peer review]] modes, and distribution channels.

OASPA encourages publishers to use [[Creative Commons licenses]], particularly the Creative Commons Attribution License (CC-BY),<ref>[http://oaspa.org/blog/2010/01/ OASPA’s response to the OSTP's request for public comment on Public Access Policies for Science and Technology Funding Agencies Across the Federal Government], accessed February 13, 2011.</ref> which is in line with most definitions of "open", e.g. the [[The Open Definition|Open Definition]] by the [[Open Knowledge Foundation]].<ref>[http://www.opendefinition.org/ Open Definition], accessed February 13, 2011</ref> The organization also engages beyond Open Access journals, e.g. for free access to scholarly works that have been awarded [[Nobel Prize]]s.<ref>[http://www.sciecom.org/ojs/index.php/sciecominfo/article/view/3624/0 Open Access to Nobel Prize awarded work – a pilot project], accessed February 13, 2011</ref>

== Members ==
In order to join OASPA as a member organization, a publisher must meet set criteria established to promote transparency and best practices in scholarly publishing. These criteria were set in 2013 and revised again in June 2015.<ref>{{cite web|last1=Redhead|first1=Claire|title=Principles of Transparency and Best Practice in Scholarly Publishing|url=http://oaspa.org/principles-of-transparency-and-best-practice-in-scholarly-publishing-2/|website=Open Access Scholarly Publishers Association|publisher=OASPA|accessdate=30 April 2016}}</ref> There are five types of OASPA members:<ref>Further information on membership criteria is available at [http://www.oaspa.org/membership_criteria.html http://www.oaspa.org/membership_criteria.html].</ref>

* Professional OA Publisher (Small)
* Professional OA Publisher (Medium)
* Professional OA Publisher (Large)
* OA Scientist / Scholar Publisher
* Other Organisation

In addition to the [[#Founding members|founding members]] above, the following organizations are current members (as of January 2015):<ref>{{cite web |url=http://oaspa.org/membership/members/ |title=Members |work=Open Access Scholarly Publishers Association |accessdate=2015-01-06}}</ref>
{{div col|colwidth=15em}}
*[[African Journals OnLine]]
*[[American Institute of Physics]]
*[[American Physical Society]]
*[[BioOne]]
*[[Bloomsbury Qatar Foundation Publishing]]
*[[BMJ Group]]
*[[Brill Publishers]]
*[[Cambridge University Press]]
*[[The Company of Biologists]]
*[[Copyright Clearance Center]]
*[[CSIC Press]]
*[[Directory of Open Access Journals]]
*[[EBSCO Information Services]]
*[[ecancermedicalscience]]
*[[EDP Sciences]]
*[[Electronic Information for Libraries]]
*[[eLife]]
*[[Faculty of 1000]]
*[[Frontiers Media]]
*[[HighWire Press]]
*[[Institute of Historical Research]]
*[[International Network for the Availability of Scientific Publications]]
*[[IOP Publishing]]
*[[John Wiley & Sons]]
*[[Karger Publishers]]
*[[Knowledge Unlatched]]
*[[Leibniz Institute for Psychology Information]]
*[[Libertas Academica]]
*[[Living Reviews]]
*[[Lund University Library]]
*[[MDPI]]
*[[National Library of the Netherlands]]
*[[Nature Publishing Group]]
*[[Open Book Publishers]]
*[[Oxford University Press]]
*[[PeerJ]]
*[[Pensoft Publishers]]
*[[Portland Press]]
*[[ProQuest]]
*[[Royal Society]]
*[[ScienceOpen]]
*[[Springer Science+Business Media]]
*[[Taylor & Francis]]
*[[Technika (publisher)]]
*[[Ubiquity Press]]
*[[Unglue.it]]
*[[University of Adelaide Press]]
*[[University of Pittsburgh Library System]]
*[[University of Tromsø]]
{{div col end}}

== Criticism ==
Criticism has focused on OASPA's self-declared role as the "stamp of quality for open access publishing", because it is apparently at odds with OASPA's application of its own criteria for membership. Another voiced concern is the fact that OASPA has been founded by [[BioMed Central]] and other open access publishers, which would cause a conflict of interest in their "seal of approval".<ref>According to [http://poynder.blogspot.com/2010/02/open-access-linked-to-alabama-shooting.html Open Access linked to Alabama shooting] and [http://poynder.blogspot.com/2010/02/oa-interviews-sciyo-aleksandar-lazinica.html The OA Interviews: Sciyo's Aleksandar Lazinica] by journalist [[Richard Poynder]], several suspicious OA publishers &mdash; [[Dove Medical Press]], [[Sciyo]] and InTech &mdash; have at some point been OASPA members. According to [http://www.oaspa.org/members.php OASPA's list of members], none of these three are a member as of February 2011.</ref><ref>[http://scienceblogs.com/bookoftrogool/2010/02/oaspa_act_now_or_lose_credibil.php OASPA: act now or lose credibility forever] by librarian [[Dorothea Salo]], accessed February 13, 2011.</ref> OASPA has also been criticized for promoting [[gold open access]] in a way that may be at the expense of [[green open access]].<ref>According to [http://openaccess.eprints.org/index.php?/archives/675-guid.html Critique of Criteria for "Full Membership" in OASPA ("Open Access Scholarly Publishers Association")] by scientist and Green OA advocate [[Stevan Harnad]], OASPA accepted [[Oxford University Press]] as a member because it publishes some Gold OA journals, while ignoring that most OUP journals are not Gold OA, and even prohibit Green OA for a year. Accessed February 13, 2011.</ref> One member organization, [[Frontiers Media]], is included on [[Jeffrey Beall]]'s list of [[predatory open access publishing]] companies;<ref>[[Jeffrey Beall]] (Accessed March 14, 2016), [https://scholarlyoa.com/publishers/ Beall's List], </ref> at least two members, [[Hindawi Publishing Corporation#Beall's list|Hindawi]] and [[MDPI]], were once called predatory by Beall, but have since been removed from his list.<ref>[[MDPI]] (28 October 2015), [http://www.mdpi.com/about/announcements/534], ''Update: Response to Mr. Jeffrey Beall’s Repeated Attacks on MDPI''</ref> 

== Response to the ''Science'' sting ==
<!-- this is copied from [[Who's Afraid of Peer Review?#Responses from the open access academic publishing industry]] -->
As a response to the ''[[Who's Afraid of Peer Review?]]'' investigation, OASPA formed a committee to investigate the circumstances that led to the acceptance of the fake paper by 3 of its members.<ref name="redhead">{{cite web |last=Redhead |first=Claire |title=OASPA's response to the recent article in Science entitled "Who's Afraid of Peer Review?" |url=http://oaspa.org/response-to-the-recent-article-in-science/ |publisher=Open Access Scholarly Publishers Association |accessdate=21 October 2013}}</ref> On 11 November 2013, OASPA terminated the membership of two publishers ([[Dove Medical Press]] and Hikari Ltd.) who accepted the fake paper. Sage Press, which also accepted a fake paper, was put "under review" for 6 months.<ref name="oaspa"/> Sage announced in a statement that it was reviewing the journal that accepted the fake paper, but that it would not shut it down.<ref name="Statement by SAGE on the Journal of International Medical Research">{{cite web |last=Gamboa |first=Camille |title=Statement by SAGE on the Journal of International Medical Research |url=http://www.sagepub.com/press/2013/october/SAGE_statementSAGEJIMR.sp |publisher=Sage |accessdate=22 November 2013}}</ref>  Sage's membership was reinstated at the end of the review period following changes to the journal's editorial processes.<ref>{{cite news|last=Shaffi|first=Sarah|url=http://www.thebookseller.com/news/oaspa-reinstates-sage-membership.html|title=OASPA reinstates Sage membership|work=[[The Bookseller]]|date=29 April 2014|accessdate=2 June 2014}}</ref> Dove Medical Press were also reinstated in September 2015 after making a number of improvements to their editorial processes.<ref>{{cite news|last=Redhead|first=Claire|url=http://oaspa.org/dove-medical-press-reinstated-as-oaspa-members/|title=Dove Medical Press reinstated as OASPA Members|work=Open Access Scholarly Publishers Association|date=23 September 2015|accessdate=1 February 2016}}</ref>

== See  also ==
*[[Association for Learned and Professional Society Publishers]]
*[[Association of Publishing Agencies]]
*[[Directory of Open Access Journals]]
*[[International Association of Scientific, Technical, and Medical Publishers]]
*[[International Publishers Association]]
*[[:Category:Open access publishers]]
*[[Periodical Publishers Association]]
*[[Scholarly Publishing and Academic Resources Coalition]]
*[[Society for Scholarly Publishing]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.oaspa.org/}} 
* [http://river-valley.zeeba.tv/category/conferences/publishing/coasp/ Video recordings from the Conference on Open Access Scholarly Publishing]
*{{Citizendium}}

[[Category:Open access (publishing)]]
[[Category:Publishing organizations]]
[[Category:Academic publishing]]
[[Category:Organizations established in 2008]]