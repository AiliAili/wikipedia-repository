'''Yvonne Drewry''' (18 February 1918 – 9 August 2007) was an English artist and art teacher, noted for her work in and around [[Suffolk]].

==Early life and education==
Yvonne Marjorie Drewry was born in [[Brentford]], Middlesex, to Alfred F. Vere Drewry (1888-1980) and his wife Ada, née Anniss (1883-1965).<ref>{{Cite web|url = http://www.suffolkpainters.co.uk/index.cgi?choice=painter&pid=755|title = DREWRY, Yvonne M|last =|first =|date = 2015|website = Suffolk Painters|publisher =|access-date = 5 March 2016}}</ref> Her father ran a motor parts shop on Deansgate in [[Manchester]]. Her uncle [[James Sidney Drewry]] was an engineer and co-founder of [[Shelvoke and Drewry]], one of the UK’s largest manufacturers of municipal waste wagons and fire engines.{{citation needed|date=March 2016}}

After studying at [[Southport College]] of Art, in 1939 Drewry won an [[Andrew Grant]]{{dn|date=December 2016}} scholarship of £120 a year for three years<ref>The Scotsman, 5 May 1939</ref> to train at the [[Edinburgh College of Art]], where she studied under [[William George Gillies]], [[John Maxwell (artist)|John Maxwell]] and book illustrator [[Joan Hassall]].<ref>Artists in Britain Since 1945, David Buckman, Art Dictionaries Ltd, 2006 ISBN 9780953260959</ref><ref>{{cite web|url=http://www.artbiogs.co.uk/1/artists/drewry-yvonne|title=DREWRY Yvonne  1918-2006|work=artbiogs.co.uk}}</ref>

==Work==
Drewry was a prolific artist, working in oil, watercolour, and pen and ink. She was also a notable print-maker and typographer. Her other work included woven textiles and handicrafts. Her work was exhibited regularly in Suffolk galleries, including an annual exhibition at the Denis Taplin Gallery in Woodbridge, Gallery 44 in Aldeburgh, Mall Galleries, Gainsborough's House, Sudbury, Wolsey Art Gallery, Ipswich, and in her own studio, and featured several times in local press articles;<ref>East Anglian Daily Times, 8 May 1968</ref> she also exhibited internationally and made sales in France and the US, and is licensed through Bridgeman Images.<ref>{{cite web|url=http://www.bridgemanimages.com/en-GB/search/artist/36403/drewry-yvonne|title=Bridgeman Images|publisher=}}</ref> She was highly commended in the 1994 Laing Art Competition.

From 1985 she was part of the 8+1 Suffolk Group, a group  of nine artists that could manage and present their own exhibitions. Their first show was at [[Broughton Gallery]], [[Lanarkshire]] in August 1985.

Her main subjects were landscapes and seascapes celebrating the [[Suffolk]] countryside, along with still life pictures of flowers, plants and trees, including those growing in her own garden; she also produced occasional portraits. Her work was mainly figurative, although her later works were much more abstract in character, using vivid colours and broad brushstrokes. She is mentioned in [[Patrick Trevor-Roper]]'s influential book ''The World Through Blunted Sight: An inquiry into the influence of defective vision on art and character (1970)'' as having different colour perception in each eye.<ref>The World Through Blunted Sight: An inquiry into the influence of defective vision on art and character, Patrick Trevor-Roper, Thames & Hudson ISBN 9780285642072</ref> Her works are rarely titled, and whilst the landscapes are clearly recognisable, they are not usually particularly well-known views, although she did paint [[Snape Maltings]] and [[Shingle Street]]. She generally worked from life, and travelled around Suffolk and Norfolk in a [[Fiat 238]] camper van seeking suitable subjects.

[[File:Watercolour by Yvonne Drewry.jpg|thumb|Watercolour by Yvonne Drewry, 1974]]
[[File:Linocut by Yvonne Drewry.jpg|thumb|Linocut by Yvonne Drewry, 1976]]

===Oil===
Working largely on board, her oil paintings used a broad [[impasto]] with vivid colours. The earlier work is mainly figurative, but her later works were more impressionistic and dramatic. They were generally signed "Yvonne Drewry" and the year in oil paint.

===Watercolour===
The watercolour paintings were small, many being pen and ink drawings tinted with colour wash. They were generally signed "Yvonne Drewry" and the year in pen.

===Prints===
Her earliest works, including book illustrations, were generally in [[woodcut]], but later she worked mostly in multi-layered [[linocut]], often with as many as eight or nine different colours, generally printing on handmade Japanese paper. Most prints were unsigned, but some contained the initials "YD" in a cartouche.

===Books===
From 1944, Drewry created several short run [[private press]] illustrated books that she printed herself on an [[Albion press]] and bound under the imprint The Black Mill Press,<ref>{{cite web|url=http://www.lib.udel.edu/ud/spec/findaids/lieber/series4.html|title=University of Delaware: J. Ben Lieberman Papers: Series IV|work=udel.edu}}</ref> and later The Centaury Press (a reference to her favoured font, Bruce Rogers' [[Centaur (typeface)]], a well as the flower). The books were typically in editions of 24, printed on roughly cut handmade paper with slipcases. She illustrated existing work, including [[Edmund Spenser]]'s [[Prothalamion]], or used Japanese [[haiku]] to inspire her own illustrations; she also produced a posthumous edition of the engravings of Viola Paterson,<ref>{{cite web|url=http://www.huntsearch.gla.ac.uk/cgi-bin/foxweb/huntsearch/DetailedResults.fwx?collection=all&SearchTerm=52049&mdaCode=GLAHA|title=Hunterian Museum & Art Gallery Collections: GLAHA 52049|work=gla.ac.uk}}</ref> who was the niece of the painter [[James Paterson (painter)|James Paterson]] <ref>{{cite journal |journal=[[Print Quarterly]] |title=The Woodtypes of Mary Viola Paterson |first=Jane |last=Lindsey |volume=15 |issue=1 |date=March 1998 |page=56 |jstor=41825080 |accessdate=30 December 2016}}</ref> and mother of Drewry's friend from her Edinburgh college days, the artist Anne Paterson Wallace.

===Ephemera===
Drewry produced her own exhibition catalogues and posters in letterpress, and also created her own Christmas cards, which were usually multi-coloured linocuts, showing the influence of Joan Hassall. She also produced hand woven cloth on a floor loom, and made textile items such as [[appliqué]] cushions, and produced smaller craft items sold in her annual Sales of Work.

==Teaching==
Drewry was an important teacher of art at the [[Amberfield School]] in [[Nacton]],<ref>Benton End Remembered, Gwynneth Reynolds and Diana Grace (eds), Unicorn Press, 2002 ISBN 9780906290699</ref> in [[Felixstowe]], and at various local authority run evening classes for adults; she also ran short painting and printing courses in her own home. She was active in various art groups including the Deben (later Felixstowe) Art Group.<ref>{{cite web|url=http://www.felixstoweartgroup.org/about-us/|title=About Felixstowe Art Group|work=Felixstowe Art Group}}</ref> Her pupils included [[Maggi Hambling]], who cites her as a major childhood influence,<ref>{{cite web|url=http://www.eadt.co.uk/ea-life/maggi_celebrates_60th_birthday_in_style_1_73966|title=Maggi celebrates 60th birthday in style|author=Andrew Clarke|work=East Anglian Daily Times}}</ref><ref>{{cite web|url=http://www.independent.co.uk/life-style/an-oscar-winner-after-all-1261246.html|title=An Oscar winner after all|work=The Independent}}</ref><ref>{{cite web|url=http://www.tes.co.uk/article.aspx?storycode=392792|title=My best teacher|work=tes.co.uk}}</ref> and [[Malta]] based artist Juliet Horncastle.<ref>{{cite web|url=http://www.independent.com.mt/articles/2006-04-23/news/60-second-interview-juliet-horncastle-90343/|title=60 Second Interview - Juliet Horncastle - The Malta Independent|work=independent.com.mt}}</ref>

==Personal life==
In 1941, Drewry married Robert Alexander (Bob) Campbell (1916-2008), whom she met at Edinburgh College of Art. In 1942 they moved to [[Trimley St Martin]] in Suffolk, where they had four children.<ref>{{cite web|url=http://www.trimleymill.co.uk/paradise-regained/chapter-17/page-1|title=Chapter 17: The Mill - Page 1 of 4 - Trimley Mill|work=trimleymill.co.uk}}</ref> Drewry and Campbell had separated by 1968, when Campbell emigrated to Canada. Although known in her personal life after marriage as Mrs. Campbell, she always painted and exhibited under the name Yvonne Drewry.

In 1977 Drewry moved briefly to [[Tuddenham St Martin]], before settling in [[Hollesley]] in 1980, where she lived until 2004. Suffering from [[Alzheimer’s disease]], she spent her last years in a nursing home in [[Woodbridge, Suffolk|Woodbridge]], and died in 2007 aged 89.<ref>{{Cite journal|last=Macleod|first=Helen|date=October 2007|title=Yvonne Drewry|url=http://villagevoices.org.uk/VVArchive/October%202007.pdf|journal=Village Voices|page=29|doi=|pmid=|access-date=5 March 2016}}</ref>

==Selected exhibitions==
* 1981 Gainsborough House, Sudbury
* 1984 Denis Taplin Gallery, Woodbridge
* 1985 Broughton Gallery, Lanarkshire<ref>Scottish Field, Vol 131</ref>
* 1991 Laing Art Competition, Mall Galleries
* 1994 Laing Art Competition, Mall Galleries (Highly Commended)
* 1995 Ipswich Open, Wolsey Art Gallery
* 1996 Denis Taplin Gallery, Woodbridge

==References==
{{reflist|2}}

==External links==

*[http://www.drewry.net/TreeMill/Yvonne/YvonnesArt.html The Art of Yvonne Drewry]
*[https://www.flickr.com/photos/29146434@N06/sets/72157635314863355/ Flickr gallery]

{{authority control}}

{{DEFAULTSORT:Drewry, Yvonne}}
[[Category:20th-century English painters]]
[[Category:British women painters]]
[[Category:British women artists]]
[[Category:1918 births]]
[[Category:2007 deaths]]