{{Orphan|date=February 2017}}

{{Infobox journal
| title = International Journal of Stress Management
| cover = [[File:International_Journal_of_Stress_Management_journal_cover.gif]]
| editor = Sharon Glazer
| discipline = [[Stress management]]
| abbreviation = Int. J. Stress Manag.
| publisher = [[American Psychological Association]] on behalf of the [[International Stress Management Association]]
| country =
| frequency = Quarterly
| history = 2003-present
| openaccess = 
| license =
| impact = 0.853
| impact-year = 2014
| website = http://www.apa.org/pubs/journals/str/index.aspx
| link1 = http://psycnet.apa.org/index.cfm?fa=browsePA.volumes&jcode=str
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 44461463
| LCCN = 94648209
| CODEN = ISMAE8
| ISSN = 1072-5245
| eISSN = 1573-3424
}}
The '''''International Journal of Stress Management''''' is a quarterly [[peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]] on behalf of the [[International Stress Management Association]]. The journal was established in 2003 and covers research on all aspects of [[stress management]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]],<ref>{{cite web |date=19 November 2012 | url=http://www.scopus.com/source/sourceInfo.url?sourceId=14761&origin=resultslist |title=International Journal of Stress Management |work=[[Scopus]] |publisher=[[Elsevier]] |accessdate=2012-11-19}}</ref> [[PsycINFO]], and [[CINAHL]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.853, ranking it 49th out of 76 journals in the category "Psychology, Applied".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Psychology, Applied|title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[Occupational health psychology]]
* [[Industrial and organizational psychology]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.apa.org/pubs/journals/str/index.aspx}}

[[Category:American Psychological Association academic journals]]
[[Category:English-language journals]]
[[Category:Applied psychology journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2003]]