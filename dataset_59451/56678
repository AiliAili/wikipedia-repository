An '''editor in chief''' is a publication's editorial leader, having final responsibility for all operations and policies.<ref>{{cite web|title=editor in chief|url=http://www.thefreedictionary.com/editor+in+chief|work=The Free Dictionary by Farlex|publisher=Farlex, Inc.|accessdate=23 May 2012|author=Staff|year=2012}}</ref><ref>{{cite web|title=Encarta Dictionary definition |url=http://encarta.msn.com/dictionary_/editor%2520in%2520chief.html |archiveurl=http://www.webcitation.org/5kwpHmNCe?url=http://encarta.msn.com/dictionary_/editor%2520in%2520chief.html |archivedate=2009-11-01 |deadurl=yes |df= }}</ref>

==Description==
The editor in chief heads all the departments of the organization and is held accountable for delegating tasks to staff members and managing them.  The term is often used at [[newspaper]]s, [[magazine]]s, [[yearbook]]s, and [[television news]] programs. 

The term is also applied to [[academic journals]], where the editor in chief ultimately decides whether a submitted manuscript will be published.  This decision is made by the editor in chief after seeking input from [[Peer review|reviewers]] selected on a basis of relevant expertise.

Typical responsibilities of editors in chief include:<ref>{{cite web |last=Patil |first=Sayali Bedekar |title=Editor In Chief Responsibilities |work=Buzzle Web Portal: Intelligent Life on the Web |accessdate=18 August 2010 |url=http://www.buzzle.com/articles/editor-in-chief-responsibilities.html}}</ref>
* [[Fact checking]], spelling, grammar, writing style, page design and photos
* Rejecting writing that appears to be plagiarized, [[ghostwriter|ghostwritten]], published elsewhere, or of little interest to readers
* Editing content
* Contributing editorial pieces
* Motivating and developing editorial staff
* Ensuring the final draft is complete and there are no omissions
* Handling reader complaints and taking responsibility for issues after publication
* For books or journals, cross-checking citations and examining references

==Further reading==
* {{cite book|title=Making a Newspaper|author=John La Porte Given|chapter=The Editor-In-Chief|pages=30–35|location=New York|date=1907|publisher=H. Holt and company}}
* {{cite book|title=The Handbook of Journalism: All about Newspaper Work: Facts and Information|author=Nathaniel Clark Fowler|chapter=The Editor-In-Chief|location=New York|date=1913|publisher=Sully and Kleinteich}}
* The New Fowler's Modern English Usage (3rd ed. 1996 (edited by R. W. Burchfield); Bryan A. Garner, Garner's Modern American Usage (2009).

==References==
{{reflist|30em}}

==External links==
{{Wiktionary|editor in chief}}
* [http://www.merriam-webster.com/dictionary/editor-in-chief "Editor in chief"] (Merriam-webster.com)

{{Journalism roles}}

{{Authority control}}

[[Category:Types of editors]]


{{journalism-stub}}