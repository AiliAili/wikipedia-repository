{{good article}}
{{Use British English|date=November 2014}}
{{Use dmy dates|date=November 2014}}
{{Infobox University Boat Race
| name= 77th Boat Race
| winner = Cambridge
| margin = Oxford waterlogged
| winning_time= 21 minutes 50 seconds
| overall = 36–40
| umpire = [[Frederick I. Pitman]]<br>(Cambridge)
| date= {{Start date|1925|3|28|df=y}}
| prevseason= [[The Boat Race 1924|1924]]
| nextseason= [[The Boat Race 1926|1926]]
}}

The '''77th Boat Race''' took place on 28 March 1925.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Umpired by former rower [[Frederick I. Pitman]], Cambridge won in a time of 21 minutes 50 seconds as Oxford were waterlogged and unable to finish the race. The victory took the overall record in the event to 40&ndash;36 in Oxford's favour.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1924|1924 race]] by four-and-a-half lengths, while Oxford led overall with 40 victories to Cambridge's 35 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 25 August 2014}}</ref><ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>

Oxford were coached by G. C. Bourne who had rowed for the university in the [[The Boat Race 1882|1882]] and [[The Boat Race 1883|1883 races]], [[Stanley Garton]] (who had rowed three times between 1909 and 1911) and E. D. Horsfall (who had rowed in the three races prior to the [[First World War]]).  Cambridge's coaches were Francis Escombe, P. Haig-Thomas (four-time Blue who had rowed between 1902 and 1905), Sir Henry Howard (coach of the [[Lady Margaret Boat Club]])<ref>{{Cite web | url = http://archive.spectator.co.uk/article/16th-march-1934/9/why-cambridge-wins-the-boat-race | work = [[The Spectator]] | title = Why Cambridge Wins the Boat Race | first = Guy | last = Nickalls | authorlink=Guy Oliver Nickalls | date = 16 March 1934|  page = 9}}</ref> and David Alexander Wauchope (who had rowed in the [[The Boat Race 1895|1895 race]]).<ref>Burnell, pp. 110&ndash;111</ref><ref name=drink144/> For the seventeenth year the umpire was [[Eton College|Old Etonian]] [[Frederick I. Pitman]] who had rowed for Cambridge in the [[The Boat Race 1884|1884]], [[The Boat Race 1885|1885]] and [[The Boat Race 1886|1886 races]].<ref>Burnell, pp. 49, 108</ref>

As a result of various illnesses, the Oxford crew was not finalised until five days before the race, and according to former Dark Blue rower and author George Drinkwater, "the innumerable changes prevented the crew from ever really getting together".<ref name=drink144/>  Drinkwater also stated that the Oxford boat rowed with "three unfit men in the boat".<ref name=drink144/>  Similarly, Cambridge were affected by illness, in particular the boat club president [[Robert Morrison (rower)|Robert Morrison]] who was forced to leave the crew.<ref name=drink144/>

==Crews==
The Oxford crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 0.875&nbsp;[[Pound (mass)|lb]] (78.5&nbsp;kg), {{convert|0.375|lb|kg|1}} per rower more than their opponents. Cambridge's crew included six participants with Boat Race experience, all of whom had made their first appearance in the event the previous year.  Oxford saw three rowers return to the boat, including G. J. Mower-White who was rowing in his third consecutive race.<ref name=burn7172>Burnell, pp. 71&ndash;72</ref>  All of the participants in the race were registered as British.<ref>Burnell, p. 39</ref>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || A. H. Franklin ||  [[Lincoln College, Oxford|Lincoln]] || 11 st 9.5 lb || G. E. G. Goddard || [[Jesus College, Cambridge|Jesus]] || 11 st 2 lb
|-
| 2 || C. E. Pitman || [[Christ Church, Oxford|Christ Church]] || 11 st 7 lb || W. F. Smith || [[Trinity College, Cambridge|1st Trinity]] || 11 st 7 lb
|-
| 3 || E. C. T. Edwards || [[Christ Church, Oxford|Christ Church]] || 12 st 3.5 lb || H. R. Carver || [[Trinity College, Cambridge|3rd Trinity]]  || 12 st 13 lb
|-
| 4 || M. R. Grant  || [[Christ Church, Oxford|Christ Church]] || 11 st 8 lb || J. S. Herbert || [[King's College, Cambridge|King's]] || 11 st 9 lb
|-
| 5 || G. J. Mower-White (P)||  [[Brasenose College, Oxford|Brasenose]] || 13 st 4 lb || G. H. Ambler || [[Clare College, Cambridge|Clare]] || 12 st 7 lb
|-
| 6 || J. D. W. Thomson || [[University College, Oxford|University]] || 12 st 10 lb || G. L. Elliot-Smith || [[Lady Margaret Boat Club]]|| 13 st 4 lb
|-
| 7 || G. E. G. Gadsden ||  [[Christ Church, Oxford|Christ Church]] || 11 st 12 lb || S. K. Tubbs || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 11 st 12 lb
|-
| [[Stroke (rowing)|Stroke]] || A. V. Campbell  ||[[Christ Church, Oxford|Christ Church]] || 11 st 9 lb || A. G. Wansbrough || [[King's College, Cambridge|King's]] || 11 st 7 lb
|-
| [[Coxswain (rowing)|Cox]] || R. Knox || [[Balliol College, Oxford|Balliol]] || 8 st 2 lb || J. A. Brown || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 7 st 13 lb
|-
!colspan="7"|Source:<ref name=burn72>Burnell, p. 72</ref><br>(P) &ndash; boat club president<br>R. E. Morrison acted as the non-rowing president for Cambridge<ref>Burnell, pp. 50, 52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge won the [[coin flipping|toss]] and elected to start from the Middlesex station, handing the Surrey side of the river to Oxford. Pitman got the race underway at 3:41&nbsp;p.m. in a strong wind diagonally across the start which made the Surrey virtually unnavigable;  within a minute, the Dark Blues' boat was waterlogged.  Cambridge enjoyed the shelter of the Middlesex wall and rapidly went away.  Despite being advised to stop by their coach Garton, it was not until The Doves pub that Oxford gave up.  Cambridge slowed to a "strong paddle" and passed the finishing post in a time of 21 minutes 50 seconds.<ref name=drink144>Drinkwater, p. 144</ref>  

It was the slowest winning time since the [[The Boat Race 1912|1912 race]] and the third time in the history of the event that one or both of the crews sank (in the [[The Boat Race 1859|1859]] and [[The Boat Race 1912|1912 races]]).<ref name=results/>  According to author and former Oxford rower George Drinkwater, the race was "a complete 'washout' in the literal sense of the word".<ref name=drink144/>  Following the race, Pitman was heavily criticised for his placement of the stakeboats;  Drinkwater disagreed, noting that "when the take-boats were being placed ... the water was possible for both boats."<ref name=drink145/>  Indeed, prior to the race he had followed the requests of both boat club presidents to move the boats closer to the Middlesex station but did not want to give an inappropriate advantage to the crew starting from that side of the river.  In a letter from Pitman, published in ''[[The Field (magazine)|The Field]]'', he wrote that "I hope that the umpire may be relieved from the duty of fixing the course, or that he may have the assistance of a representative of both Universities on his launch in fixing the stake-boats and deciding whether the race can be rowed."<ref name=drink145>Drinkwater, p. 145</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-95-006387-4 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1925}}
[[Category:1925 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1925 sports events]]