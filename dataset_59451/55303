{{refimprove|date=August 2016}}
{{italictitle}}
{{Infobox Journal
|cover =
|discipline = [[African studies]]
|language = English
|website = http://www.africa-spectrum.org
|link1 =
|link1-name =
|publisher = GIGA (German Institute of Global and Area Studies, Institute of African Affairs)
|country = Germany
|history = 1966–present
|frequency = 3/year
| openaccess    = 
| license       = CC BY-ND<ref>{{cite web| url = http://www.doaj.org/doaj?func=openurl&issn=00020397&genre=journal&uiLanguage=en |title = Africa Spectrum | work = [[DOAJ]] | accessdate = 2013-01-28}}</ref>
| impact = 0.900 (Area Studies, ISI)
| impact-year = 2014
|ISSN = 0002-0397
|eISSN = 1868-6869
|Jstor =
|OCLC =
|LCCN =
}}
'''''Africa Spectrum''''' is an interdisciplinary [[Peer review|peer-reviewed]] [[academic journal]] concentrating on the analysis of current issues of development in Africa south of the [[Sahara]]. It is the only German academic journal exclusively devoted to Africa. ''Africa Spectrum'' is published three times a year by the GIGA Institute of African Affairs. The journal is part of the [[GIGA Journal Family]] of the [[German Institute of Global and Area Studies|GIGA German Institute of Global and Area Studies]] ([[Hamburg|Hamburg, Germany]]).

== Concept ==
''Africa Spectrum'' focuses on social science dealing with Africa and is dedicated to promote a deeper understanding of African peoples and cultures. It aims not just at academics and students, but also at general readers and practitioners with a concern for contemporary Africa. Since 2003 the journal collaborates closely with the Association of Africanists in Germany (Vereinigung für Afrikawissenschaften in Deutschland, VAD). The journal is indexed by the ''[[Social Sciences Citation Index]]'', ''[[African Studies Abstracts Online]]'', [[CSA (database company)|Cambridge Scientific Abstracts]], [[Scopus]], and the ''[[International Bibliography of the Social Sciences]]''.

== References ==
{{Reflist}}

==External links==
* [http://hup.sub.uni-hamburg.de/giga/afsp Africa Spectrum]
* [http://hup.sub.uni-hamburg.de/giga/journal-family/indexGIGA GIGA Journal Family]
* [http://www.giga-hamburg.de/english/index.php?file=giga.html&folder=giga GIGA German Institute of Global and Area Studies]
* [http://www.giga-hamburg.de/english/index.php?file=iaa.html&folder=iaa GIGA Institute of African Affairs]

[[Category:African studies journals]]
[[Category:Open access journals]]
[[Category:Development studies journals]]