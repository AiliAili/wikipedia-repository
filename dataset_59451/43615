<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R.II
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Bomber]]
 | national origin=Germany
 | manufacturer=[[Siemens-Schuckert]]
 | designer=[[Bruno and Franz Steffen]]<ref name="gt572">Grey & Thetford 1962, p.572</ref>
 | first flight=26 October 1915<ref name="hg184">Haddow & Grosz 1963, p.184</ref>
 | introduced=
 | retired=
 | status=
 | primary user=[[Luftstreitkräfte]]<ref name="hg187">Haddow & Grosz 1963, p.187</ref>
 | number built=1<ref name="hg184"/>
 | developed from= [[Siemens-Schuckert R.I]]<ref name="hg180">Haddow & Grosz 1963, p.180</ref>
 | variants with their own articles=
}}
|}

The '''Siemens-Schuckert R.II''' was a prototype bomber aircraft built in Germany during World War I.<ref name="jea">Taylor 1989, p.808</ref><ref name="iea">''The Illustrated Encyclopedia of Aircraft'', p.2920</ref> It was one of six aircraft based on the [[Siemens-Schuckert R.I]] that were originally intended to be identical, but which each developed in a different direction and were designated as different aircraft types by the German Inspectorate of Flying Troops (the [[Idflieg]]).<ref name="hg184"/> Although the R.II was the first of the batch to be completed,<ref name="hg184"/> it was the last accepted into military service,<ref name="hg184"/> and then only as a trainer.<ref name="gt572"/><ref name="hg187"/><ref name="jea"/>

==Design and development==
As designed, the R.II was a large three-bay biplane with unstaggered wings of unequal span<ref name="hg181">Haddow & Grosz 1963, p.181</ref> and a fully enclosed cabin. Power was to be supplied by three 180-kW (240-hp) [[Maybach HS]] engines mounted internally in the fuselage, which transmitted their power via driveshafts to two propellers mounted tractor-fashion on the interplane struts nearest the fuselage.<ref name="hg174">Haddow & Grosz 1963, p.174</ref> The main undercarriage consisted of divided units, each of which carried dual wheels, and the tail was supported by a pair of tailwheels.<ref name="hg185">Haddow & Grosz 1963, p.185</ref> The fueslage was forked into an upper and lower section, which allowed a clear field of fire to the rear of the aircraft.<ref name="gt572"/>

The R.II first flew on 26 October 1915<ref name="hg184"/> and was delivered to the military on 20 November.<ref name="hg186">Haddow & Grosz 1963, p.186</ref> The Maybach engines proved immediately troublesome,<ref name="hg186"/> prone to overheating and mechanical failure.<ref name="hg181"/> In February 1916, the engines were removed and returned to the manufacturer.<ref name="hg186"/> Ongoing trouble with the same engine in other aircraft eventually led Siemens-Schuckert to abandon work with these engines entirely<ref name="hg182">Haddow & Grosz 1963, p.182</ref> and in June, the firm asked the ''Idflieg'' whether to install [[Benz Bz.IV]] or [[Mercedes D.IVa]] in the R.II instead.<ref name="hg186"/> By the time that the ''Idflieg'' approved the Mercedes engine for installation, Siemens-Schuckert did not have the workforce available to install them, and placed the R.II in storage instead.<ref name="hg186"/>

While the R.II was in storage, the ''Idflieg'' changed its requirements for the aircraft, requesting that the operational altitude be increased from {{convert|3,000|to|3,500|m|ft|abbr=on}}, but hoping that even more altitude would be possible.<ref name="hg186"/> Siemens-Schuckert responded that this would not be possible with the Mercedes engines recently approved for installation, and suggested three Benz D.IV engines inside the fuselage, augmented by two more of the same engine mounted pusher-fashion in the interplane gap.<ref name="hg186"/> The ''Idflieg'' rejected this suggestion, insisting on the basic requirement of having the engines serviceable in flight.<ref name="hg186"/> By the time that workers were available for the R.II again, in early 1917, Siemens-Schuckert had gained experience with the similar [[Siemens-Schuckert R.VII|R.VII]] and attempted to meet the ''Idflieg'''s requirements by installing extra sections in the wing to extend it to six bays<ref name="hg186"/> and thereby offset the extra weight of the new engines.<ref name="hg182"/> The upper wings were replaced with a new design with greater chord,<ref name="hg186"/> and the tail surfaces were enlarged.<ref name="hg186"/> Finally, bomb racks were fitted to the wings that could carry either six 50-kg bombs or four 100-kg bombs.<ref>Haddow & Grosz 1963, p.186–87</ref>

During its acceptance flight, the newly refurbished R.II carried a 2,310-kg (5,100-lb) useful load to an altitude of {{convert|3,800|m|ft|abbr=on}} and stayed aloft for four hours. Having satisfied requirements, R.II was delivered to ''[[Riesenflugzeugersatzabteilung]]'' (Rea — "giant aircraft support unit"), the support unit for R-type aircraft, on 29 June 1917.<ref name="hg187"/> Although the R.II's performance was a significant improvement over other R-type aircraft produced by Siemens-Schuckert, its increased span and weight limited its speed, and it could not keep pace with similar aircraft manufactured by [[Staaken]] then entering service.<ref name="hg187"/> The R.II was therefore relegated to training duties with the ''[[Riesenflugzeug Schulabteilung]]'' ("giant aircraft training unit") at [[Döberitz]]. In June 1918, it was transferred back to Rea at [[Cologne]], where it crashed later that year.<ref name="hg187"/>

==Units using this aircraft==
* ''[[Luftstreitkräfte]]''
** ''Riesenflugzeugersatzabteilung''<ref name="hg187"/> 
** ''Riesenflugzeug Schulabteilung''<ref name="hg187"/>

==Specifications==
{{aerospecs
|ref=<!-- reference -->Grey & Thetford 1962, p.572
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=5
|capacity=
|length m=18.5
|length ft=60
|length in=8½
|span m=38
|span ft=124
|span in=8¼
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.6
|height ft=15
|height in=1⅛
|wing area sqm=233
|wing area sqft=2,516
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=6,150
|empty weight lb=13,540
|gross weight kg=8,460
|gross weight lb=18,612
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=3
|eng1 type=[[Mercedes D.IVa]]
|eng1 kw=<!-- prop engines -->190
|eng1 hp=<!-- prop engines -->260
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=110
|max speed mph=69
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->4
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Siemens-Schuckert aircraft}}
* {{cite book| last=Gray |first=Peter |author2=Owen Thetford  |title=German Aircraft of the First World War |publisher=Putnam |location=London |year=1962}}
* {{cite book| last=Haddow |first=G.W. |author2=Peter M. Grosz  |title=The German Giants: The Story of the R-planes 1914–1919 |publisher=Putnam |location=London |year=1962}}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London |pages= }}
* {{cite book| last=Taylor |first=Michael J.H. |title=Jane's Encyclopedia of Aviation |publisher=Studio Editions |location=London |year=1989}}
<!-- ==External links== -->
{{Siemens-Schuckert aircraft}}
{{Idflieg R-class designations}}

[[Category:German bomber aircraft 1910–1919]]
[[Category:Siemens-Schuckert aircraft|R.II]]
[[Category:Three-engined twin-prop tractor aircraft]]
[[Category:Biplanes]]