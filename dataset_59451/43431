<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 | name=Equator
 | image=Poescel P-300 Equator D-EALM LEB 02.06.73 edited-3.jpg
 | caption=The P-300 Equator prototype on display at the 1973 [[Paris Air Show]]
}}{{Infobox aircraft type
 | type=Amphibious executive aircraft
 | national origin=[[Germany]]
 | manufacturer=Pöschel Aircraft, later Equator Aircraft (Germany)
 | designer=Gŭnther Pöschel
 | first flight=8 November 1970
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Pöschel Equator''' was a single engine, 6/8 seat [[amphibious aircraft|amphibian]] built in the 1970s featuring [[glass-fibre]] covered [[fuselage]].  Three aircraft were built, each with different engine or wing positions, but no production followed.

==Design and development==
Günther Pöschel designed the Equator as a small executive transport which could operate from land or water.  It used the then quite novel glass-fibre [[composite material]] for the skin of its flying surfaces and fuselage in order to achieve a smooth and watertight finish.  The first version to fly, the P-300 Equator, was completed as a landplane with a fixed undercarriage and was intended to provide proof of principle. It had a [[high wing|high]], [[cantilever]] wing of straight tapered plan and no [[dihedral (aircraft)|dihedral]], carrying a full span combination of [[ailerons]] and [[flap (aircraft)#Types of flaps|slotted flaps]].<ref name=JAWA72>{{cite book |title= Jane's All the World's Aircraft 1972-73|last= Taylor |first= John W R |coauthors= |year=1972|publisher=Sampson Low, Marston & Co. Ltd|location= London|isbn=0-354-00109-4 |pages=94–5 }}</ref>  These surfaces replaced the original full span flaps plus lateral control [[spoiler (aeronautics)|spoilers]], which were found ineffective.<ref name=Flight>{{cite magazine |last= |first= |coauthors= |date=28 March 1981  |title=Composite Amphibian|magazine= [[Flight International|Flight]]|volume=119 |issue=3751 |page=908 |url= http://www.flightglobal.com/pdfarchive/view/1981/1981%20-%200878.html |accessdate= }}</ref> Despite its landplane configuration, this first aircraft had the small outboard floats intended for production amphibians; these rotated to the [[wingtips]] in flight and remained rotated for land operation.<ref name=JAWA72/>

The P-300 Equator's fuselage was a composite skinned, metal semi-monocoque structure.  The cabin extended from near the nose to just aft of the wing [[leading edge]].  Three rows of seats were enclosed by a long, smooth windscreen and two long side transparencies which hinged upwards for access, with two smaller windows behind. The most unusual feature of the P-300 was its engine and [[propeller]] layout: the six cylinder, 310&nbsp;hp (230&nbsp;hp) Lycoming TIO-541 was placed within the fuselage behind the cabin, with a long drive shaft extending aft to the tail.  The P-300 had a [[T-tail]] and the drive shaft first turned through 90°upwards into it, then turned again to emerge from a slender [[fairing (aircraft)|fairing]] at the [[fin]]/[[tailplane]] intersection, where it drove a two blade, [[tractor configuration]] propeller. The tail was unswept, carrying [[balanced rudder|horn balanced]] [[elevator (aircraft)|elevators]] and [[rudder]].  The fixed, [[tricycle undercarriage]] had short, almost horizontal, cantilever main legs; all wheels had fairings.<ref name=JAWA72/> The prototype P-300 was exhibited at the June 1973 [[Paris Air Show]] at [[Paris Le Bourget Airport|Le Bourget Airport]].

The P-300 was followed by the P-400 Turbo-Equator, powered by a 313&nbsp;kW (420&nbsp;hp) [[Allison 250]]-25-B17B turboprop engine. Lighter and small in diameter than the Lycoming, this engine was mounted on a revised, [[cruciform]], tail at the fin/ tailplane intersection, driving a three blade propeller.  A new, [[all-moving tailplane|all-moving]] [[trim tab|tabbed]] tailplane was fitted and the rudder had gained a trim tab. The displacement of the engine from the fuselage enabled another row of seats to be added, making eight places in all.  The cabin had a glazed door in place of the P-300's hinged transparencies.<ref name=JAWA76>{{cite book |title= Jane's All the World's Aircraft 1976-77|last= Taylor |first= John W R |coauthors= |year=1973|publisher=Jane's Yearbooks |location= London|isbn=0-354-00538-3}}</ref>  The wings had the same plan and dimensions as before<ref name=JAWA76/> but were fitted with the earlier, full span [[spoilerons]].<ref name=Flight/>  The wingtip floats were removed and the aircraft stabilized on water with short fuselage mounted [[sponsons]].  A retractable tricycle undercarriage was fitted, the wheels housed within the fuselage.<ref name=JAWA76/>

The P-400 first flew on 24 August 1977 but was destroyed during its eighth land take-off when the propeller went into reverse pitch.<ref name=Flight/>

The final Equator, numbered P-300 like the first, reverted to Lycoming piston power but with the engine mounted in [[pusher configuration]] on a pylon above the fuselage and wing, partly to allow later models to use different engines or even to have two engines in [[push-pull configuration]]. The sponsons were removed and wing roots were lowered to below mid-fuselage line and dihedral added, stabilizing the aircraft on the water with a "water-wing", the centre section undersides in contact with the water, an idea developed by the [[US Navy]] and used in the [[Taylor Coot]] [[homebuilt]] amphibian.  The earlier upright fin was replaced by a wide chord, swept but still cruciform tail.  The new wing position resulted in a revision of the cabin side transparencies, with three well-spaced square windows on each side. Cabin access was via a [[port (nautical)|portside]] door.<ref name=Flight/>

Some water trials had been made by March 1981<ref name=Flight/> and this P-300 is known to have flown from land.<ref name=P300_flight>{{cite web |url=http://www.equatoraircraft.com/weblog/history/|title=Equator aircraft history |author= |date= |work= |publisher= |accessdate=21 December 2011}}</ref>  Many variants were proposed<ref name=Flight/> but not proceeded with.
<!--==Operational history== -->

==Variants==
;Equator P-300 Equator<ref name="JAWA 82-3">{{cite book |title= Jane's All the World's Aircraft 1982-83|last= Taylor |first= John W R |coauthors= |year=1983|publisher=Jane's Yearbooks |location= London|isbn=}}</ref>
;Equator P-350 Equator<ref name="JAWA 82-3"/>
;Equator P-400 Equator<ref name="JAWA 82-3"/>
;Equator P-420 Turbo Equator<ref name="JAWA 82-3"/>
;Equator P-420 Twin Equator<ref name="JAWA 82-3"/>
;Equator P-450 Equator<ref name="JAWA 82-3"/>
;Equator P-550 Turbo Equator<ref name="JAWA 82-3"/>
;Equator P2 Excursion
;Equator P1300 Equator
:Version offered in 2015 as a modular aircraft with 2 to 20 seats.<ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 106. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (first P-300)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1972/3
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=up to 5 passengers
|length m=8.53
|length note=
|span m=12.40
|height m=3.10
|wing area sqm=18.0
|wing area note=gross
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=[[Wortmann]] laminar flow
|empty weight kg=800
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=1800
|max takeoff weight note=
|fuel capacity=800 L (211 US gal; 176 Imp gal)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming TIO-541]]
|eng1 type=6-cylinder [[horizontally opposed]], [[turbocharger|turbocharged]]
|eng1 hp=310
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=[[Hartzell Propeller]]
|prop dia m=2.40
|prop dia note=[[controllable pitch propeller|constant speed, reversible pitch]] , mounted on T-tail in  [[tractor configuration]] and driven via long drive shaft.
<!--
        Performance
-->
|perfhide=

|max speed kmh=387
|max speed note=at sea level
|cruise speed kmh=450
|cruise speed note=at 75% power at 7,300 m (23,950 ft)
|stall speed kmh=90
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=5400
|range note=at 75% power at 7,300 m (23,950 ft)
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=9000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=8.65
|climb rate note=
|time to altitude=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=

|more performance=
}}

==Notes==
{{reflist}}

==Bibliography==
{{refbegin}}
*{{cite book |title= Jane's All the World's Aircraft 1972-73|last= Taylor |first= John W R |coauthors= |year=1972|publisher=Sampson Low, Marston & Co. Ltd|location= London|isbn=0-354-00109-4 |pages=94–5 }}
*{{cite book |title= Jane's All the World's Aircraft 1976-77|last= Taylor |first= John W R |coauthors= |year=1973|publisher=Jane's Yearbooks |location= London|isbn=0-354-00538-3}}
*{{cite book |title= Jane's All the World's Aircraft 1982-83|last= Taylor |first= John W R |coauthors= |year=1983|publisher=Jane's Yearbooks |location= London|isbn=}}
*{{cite magazine |last= |first= |coauthors= |date=28 March 1981  |title=Composite Amphibian|magazine= [[Flight International|Flight]]|volume=119 |issue=3751 |page=908 |url= http://www.flightglobal.com/pdfarchive/view/1981/1981%20-%200878.html |accessdate= }}

{{refend}}
<!-- ==Further reading== -->

==External links==
*{{Official website|http://www.equatorair.de/}}
*{{cite web |url=http://www.equatoraircraft.com/weblog/history/|title=Equator aircraft history |author= |date= |work= |publisher= |accessdate=21 December 2011}}
<!-- Navboxes go here -->

{{DEFAULTSORT:Poschel Equator}}
[[Category:Amphibious aircraft]]
[[Category:German civil aircraft 1970–1979]]
[[Category:Pöschel aircraft|Equator]]
[[Category:Mid-engined aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]