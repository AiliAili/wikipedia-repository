{{about| protected area in South Australia|the associated suburb |Cleland, South Australia}}
{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox Australian place
| type                   = protected
| name                   = Cleland Conservation Park
| state                  = sa
| iucn_category          = II
| image                  = Cleland Wildlife park entrance.jpg
| image_upright          = 0.81
| caption                = Entrance to the park
| image_alt              = 
| latd                   = 34
| latm                   = 58
| lats                   = 03
| longd                  = 138
| longm                  = 41
| longs                  = 45
| relief                 = 1
| map_alt                = 
| nearest_town_or_city   = Adelaide city centre
| area                   =11.25
| area_footnotes         = <ref name=ReserveList>{{cite web|title=Protected Areas Information System - reserve list (as of 16 July 2015)|url= http://www.environment.sa.gov.au/files/sharedassets/public/park_management/protected-areas-30june2015.pdf |publisher=Department of Environment Water and Natural Resources (DEWNR)|accessdate=3 August 2015|pages=}}</ref>
| established            = {{start date|1945|01|01|df=y}}  
| established_footnotes  = <ref name=ReserveList/>
| visitation_num         = 
| visitation_year        = 
| visitation_footnotes   = 
| managing_authorities   = [[Department of Environment, Water and Natural Resources (South Australia)|Department of Environment, Water and Natural Resources]]
| url                    = http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide_Hills/Cleland_Conservation_Park
}}
[[File:Cleland-conservation-park.svg|thumb|right|Map of the park]]
'''Cleland Conservation Park''' is a  [[protected area]] located in the [[Adelaide Hills]], [[South Australia]] about {{Convert|22|km}} south-east of the [[Adelaide city centre]].<ref name=DEWNR>{{cite web|title=Cleland Conservation Park |url=http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide_Hills/Cleland_Conservation_Park |publisher=Department of Environment, Water and Natural Resources|accessdate=18 November 2015}}</ref> Cleland Conservation Park conserves a significant area of natural bushland on the [[Adelaide Hills]] face and includes the internationally popular ''Cleland Wildlife Park'' and the popular [[tourist destination]]s of [[Mount Lofty]] summit and Waterfall Gully.<ref name=DEWNR/> It is maintained by the South Australian Department of Environment, Water and Natural Resources (DEWNR).<ref name=DEWNR/>

The conservation park was named for Sir [[John Burton Cleland]] (1878-1971), a renowned naturalist, microbiologist, mycologist and ornithologist, and member of the [[Royal Society of South Australia]]. After a career in medicine and pathology, Cleland became keenly interested in wildlife conservation.<ref>R. V. Southcott, 'Cleland, Sir John Burton (1878–1971)', Australian Dictionary of Biography, National Centre of Biography, Australian National University, [http://adb.anu.edu.au/biography/cleland-sir-john-burton-5679/text9595], published in hardcopy 1981, accessed online 18 April 2014.</ref>

The conservation park occupies land in the gazetted suburbs of [[Cleland, South Australia|Cleland]], [[Crafers, South Australia|Crafers]] and [[Waterfall Gully, South Australia|Waterfall Gully]].<ref name=PLB>{{cite web|title=Search result for " Cleland (Suburb)" (Record no SA0040499) with the following layers selected - "Suburbs and Localities" and "Local Government Areas"  |url=http://maps.sa.gov.au/plb/#|publisher=[[Department of Planning, Transport and Infrastructure]] |date= |accessdate=15 April 2016}}</ref>

The conservation park is classified as an [[International Union for Conservation of Nature|IUCN]] [[IUCN protected area categories#Category II — National Park|Category II protected area]].<ref name=CAPAD2014SASum>{{cite web|title= Terrestrial Protected Areas of South Australia (see 'DETAIL' tab)|url=http://www.environment.gov.au/system/files/pages/d00ca066-1c8c-412a-9e16-2a37647454a7/files/capad2014sa.xlsx|work=CAPAD 2012|publisher=Australian Government - Department of the Environment|accessdate=13 March 2015|date=2014}}</ref>

==Cleland Wildlife Park==
[[File:Dromaius novaehollandiae -Cleland Wildlife Park, Australia-8a.jpg|thumb|People with emus]]
The Wildlife Park is accessible by sealed road from both the [[South Eastern Freeway]] and [[Greenhill Road]], and on foot on a formed but steep track from [[Waterfall Gully]] or [[Mount Lofty]].<ref name="southaustraliantrails.com">http://www.southaustraliantrails.com/trails.asp?id=13811 Retrieved 15-05-2009</ref> A limited public bus service operates (Route 823: 3 journeys each day).<ref name="adelaidemetro.com.au">http://www.adelaidemetro.com.au/ttsearch.php?action=search&searchtext=823 Retrieved 15-05-2009</ref> A fee is payable to enter the Wildlife Park (but not to the Conservation Park except for car parking at Mt Lofty summit).
Facilities include a souvenir shop, cafe and toilets. Many visitors pay to be photographed holding koalas.<ref>{{cite web|url=http://i206.photobucket.com/albums/bb230/freakybomber18/DSC00137.jpg |publisher=Photobucket|title=Example photo of visitor holding a koala}}</ref>  Several other options are available to visitors, including an [[Australian Aborigine|Aboriginal]] guide on a Cultural Tour of the Yurridla Aboriginal Trail, which explains dreaming stories of [[dingo]]es, [[emu]]s, [[koala]]s and [[Yurrabilla]], the creation ancestor, and a nightwalk, uncovering the secrets of the bush.
Cleland Wildlife Park offers visitors an opportunity to walk through large enclosures and interact with Australian animals such as kangaroos, koalas and emus, and to see others including wombats, Dingo and many bird and reptile species.

==Protected Area and Biodiversity Conservation==

There are seven major anthropogenic threats to biodiversity: habitat fragmentation, destruction, degradation (including pollution), overexploitation of species for human consumption, disease increases, introduction of invasive species, and global climate change. These activities such as agricultural expansion, reckless industrialisation and urbanisation, altered the ecosystems and distribution of species. Land use change has led to the degradation, fragmentation and destruction of habitats, therefore triggered the species movement and loss of biodiversity and species extinctions worldwide.  
<ref name="ReferenceA">Primack, R. 2010 . Essentials of conservation biology. Sinauer Associates, Sunderland, Mass., U.S.A.</ref>

There is a need to conserve and restore what humans have changed in the ecosystem on both a large and small scale. According to IUCN, protected areas are areas of land or sea dedicated by law or traditions (governance regimes, scientific, traditional knowledge and community-based approaches) to implementation of protection of biodiversity.<ref>iucn.org">http://www.iucn.org/about/work/programmes/gpap_home/gpap_biodiversity/</ref>

[[IUCN protected area management categories]] has classified the protected areas according to their management goals and objectives. These classifications are recognised by the United Nations and many national governments. IUCN protected area designations are: ''Ia) Strict Nature Reserve, Ib) Wilderness Area, II) National Park, III) Natural Monument or Feature, IV) Habitat/Species Management Area, V) Protected Landscape/Seascape, VI) Protected area with sustainable use of natural resources''.<ref>"iucn.org">http://www.iucn.org/about/work/programmes/gpap_home/gpap_biodiversity/</ref>

The National Parks and Wildlife Act 1972 (NPW Act) in South Australia provides the establishment and administration of reserves for conservation of wildlife in a natural environment, public benefit and pleasure and for other purposes.<ref name="legislation.sa.gov.au">https://www.legislation.sa.gov.au/lz/c/a/national%20parks%20and%20wildlife%20act%201972/current/1972.56.un.pdf</ref>

Cleland Conservation Park has established in 1st of Jan 1945 <ref name="legislation.sa.gov.au"/> and classified as Category II <ref name="iucn.org">http://www.iucn.org/about/work/programmes/gpap_home/gpap_quality/gpap_pacategories/gpap_pacategory2/</ref> protected area in IUCN protected area management categories.<ref name="environment.gov.au">https://www.environment.gov.au/land/nrs/science/capad/2014http://www.iucn.org/about/work/programmes/gpap_home/gpap_biodiversity/</ref>  
Primary objective for Category II is "to protect natural biodiversity along with its underlying ecological structure and supporting environmental processes, and to promote education and recreation."<ref name="iucn.org"/>

The current categories and order of the schedules were last reviewed in 2008 and under the NPW Act are:<ref name="environment.gov.au"/>

''Schedule 7: Endangered species (also including critically endangered and extinct species)''

''Schedule 8: Vulnerable species''

''Schedule 9: Rare species''<ref>The “Rare category” described here, have been created to be utilized in South Australia and it is consistence with 'Near threatened' category in current IUCN definitions (include species with naturally limited existence and decreasing in number</ref>

==Conservation Prioritization and Management Plans==

Identifying priorities for conservation is the first requirement within a protected area. An effective conservation action requires detailed information about the ecosystem, species and their distribution, as well as the distribution of any threat(s) that may affect them. Conservation planners should address three questions: “What needs to be protected?”, “Where should it be protected?” and “How should it be protected?” <ref name="Primack, R 2010">Primack, R. 2010. Essentials of conservation biology. Sinauer Associates, Sunderland, Mass., U.S.A.</ref>
 
All the reserves and parks in South Australia are proclaimed under the National Parks and Wildlife Act 1972 (NPW Act) and the Wilderness Protection Act 1992 (the WP Act). Conservation of reserves are committed to the Minister under the Crown Land Management Act 2009 (CL Act).<ref name="environment.sa.gov.au">http://www.environment.sa.gov.au/managing-natural-resources/Park_management/Management_plans</ref>

A management plan is the utmost important foundation for the direction of managing the reserve. The requirements for the preparation of management plans are outlined in the NPW and WP Acts and need to be prepared soon after the constitution of a reserve. The management plan should identify the vision for the reserve in addition to the strategies and objectives to meet that vision over a 10-year period. Every year, park managers outline the work programs that are aligned with the proposed strategies in the original management plan.<ref name="environment.sa.gov.au"/>

Protecting and managing endangered or rare species requires a firm grip of their natural history (their ecology and distinctive characteristics). These essential information are: the species’ morphology, physiology, demography, behaviour, distribution, genetics, environment, biotic interactions as well as interactions with people. The information can be achieved by studying published or unpublished literature and field work. Field work is crucial since only a small percentage of the world’s species have been studied. The data in field work is collected by using long term monitoring, surveying, censuses and demographic studies which can determine the historical change of the population size. This enables the managers to distinguish short term fluctuations from the long term decline of population size.<ref name="Primack, R 2010"/>
 
Natural Resources Adelaide & Mt Lofty Ranges (AMLR) are following four main strategies to conserve the biodiversity of the region:

''I) Maintain intact (viable) landscapes, II) reverse declines, III) control emerging threats, IV) recover threatened species and ecological communities''.<ref>http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/plants-and-animals/native-plants-animals-and-biodiversity/biodiversity-conservation-strategies</ref>

==Biodiversity Threatening Processes in Adelaide and Mount Lofty Ranges (AMLR)==

Threatening processes are practices or environmental factors that could threaten the survival or evolutionary development of species, populations, ecosystems and/or ecological communities.There are direct (legal and illegal ) threats impacting biodiversity in AMLR including historical vegetation clearances, the use of biological resources, climate change (causing drought and severe weather), the introduction of exotic species and poisoning practices. There are also threats associated with roads and transportation which cause wild life mortality. These have damaged the habitats, functioning of ecosystems, availability of food and the overall food web. According to Informing Biodiversity Conservation for the Adelaide and Mount Lofty Ranges Region, South Australian ecological stresses are:<ref>Department of Environment and Heritage (2010). "Informing Biodiversity Conservation for the Adelaide and Mount Lofty Ranges Region South Australia: Priorities, Strategies and Targets" (</ref>

# Habitat fragmentation
# Edge effects, spatial and temporal provision of resources
# Ecosystem conversion, habitat loss and modification
# Fragmentation of existing habitat (isolation of populations)
# Barriers to dispersal
# Reduced reproduction/ recruitment
# Distance effects (isolation)
# Species mortality
# Road mortality
# Species disturbance
# Indirect species effects (e.g. inbreeding, increased competition and loss of pollinator or host)
# Ecosystem degradation
# Altered fire regimes
# Altered hydro logical regimes.
 
Fire is an ecological stresses that threat to biodiversity while is a valuable management tool as well.<ref>"more detailed descriptions of threats can be found in the AMLR Regional NRM Plan, and the Regional Recovery Plan for Threatened Species & Ecological Communities of Adelaide and Mount Lofty Ranges 2009-2014."</ref>

==Native Plants and Animals Biodiversity in Adelaide Mount Lofty Ranges==

The Adelaide Mount Lofty Ranges (AMLR) region is home to a range of native plants and animals.<ref>http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/plants-and-animals/native-plants-animals-and-biodiversity/native-animals</ref>

'''Native Plants'''

Around 1,500 species of native plants including orchids, ferns, gum trees, herbs, lilies, grasses and rushes, originate within AMLR. Amongst those, 121 are nationally threatened while seven are considered to be threatened in the region. The two endangered plant species in the region are Fleurieu Peninsula Guinea-flower (Hibbertia tenuis) and Mount Compass oak-bush (Allocasuarina robusta).

'''Native Animals'''

There are a great number of different native animals in the AMLR region, however some of these species have already become extinct and many are endangered. These threatened and endangered animals include birds, mammals, reptiles, frogs and freshwater fish. The details are given below.

'''''Birds'''''

The AMLR region comprises 249 species of birds, 69 of which are considered threatened. The three species that are considered Endangered are: the [[Chestnut-rumped heathwren]] (Hylacola pyrrhopygia parkeri), the Mt Lofty Ranges Southern emu-wren (Stipiturus malachurus intermedius) and the Orange-bellied parrot (Neophema chrysogaster).

'''''Mammals'''''

The AMLR region comprises 27 species of native mammals, including an echidna, an antechinus, dunnart species, a bandicoot, possums, wombat, kangaroos, rats and bats. Seven species are considered threatened in a state or national level. The [[Southern brown bandicoot]] (Isoodon obesulus obesulus) and [[Western pygmy possum]] (Cercartetus concinnus) are considered vulnerable, while the [[Brush-tailed Bettong]] (Bettongia penicillata penicillata) has been listed as endangered.

'''''Reptiles'''''

The AMLR region comprises 68 species of native reptiles, including turtles, lizards, tortoises, geckos, legless lizards, skinks, snakes and goanna species. Six of the species are considered threatened, and the [[Heath goanna]] (Varanus rosenbergi) and [[Adelaide pygmy blue-tongue skink]] (Tiliqua adelaidensis) are listed as endangered.

'''''Frogs'''''

The AMLR region comprises 7 native frog species. The Brown Toadlet (Pseudophryne bibronii) is rare in South Australia and considered vulnerable in the region.

'''''Freshwater fish''' 
''

The AMLR region comprises 20 native freshwater fish, including catfish, galaxiids, lampreys and eels. Three of these species are considered threatened nationally while Climbing Galaxias (Galaxias brevipinnis), Congolli (Pseudaphritis urvillii), Mountain Galaxias (Galaxias olidus) and Pouched Lamprey (Geotria australis) are considered Vulnerable in the region.

==Long-term Monitoring of Species and Ecosystem==

Protection of species or individual populations is usually achievable via conservation and restoration of habitats. To do so, collecting essential data on biological communities and ecosystem function are important. Therefore, long term monitoring of populations needs to be accompanied by monitoring of the environment parameters. To determine the health of ecosystem, observation of the ecosystem processes such as temperature, rainfall, humidity and soil erosion, is necessary. Monitoring the community characteristics such as the present species and amount of biomass, are also other parameters in a healthy ecosystem. By monitoring the studies of these parameters, it enables managers to decide whether or not the goals of their projects are being achieved or if management plans need to be adjusted.<ref name="ReferenceA"/>

According to Primack (2010), there are “lagging” effects on the environment that may not surface for years after the original causes. This “lagging” creates a huge challenge towards understanding the changing of ecosystems. Hence, long term monitoring programs provide an early warning for threats to the functioning of ecosystems and communities of species.<ref name="ReferenceA"/> Examples as mentioned below, can address the conservation/knowledge gaps and help to refine the management priorities:

'''Changing in conservation status, an alarm towards becoming [[Extinct]]'''

The [[Adelaide pygmy blue-tongue skink]] (Tiliqua adelaidensis) had been classified [[Extinct]] until its rediscovery near Burra in South Australia in 1992.<ref>{{cite journal | last1 = Milne | first1 = T. | last2 = Bull | first2 = C. | year = 2000 | title = Burrow choice by individuals of different sizes in the endangered [[Adelaide pygmy blue-tongue skink]] Tiliqua adelaidensis | doi = 10.1016/s0006-3207(00)00040-9 | journal = Biological Conservation | volume = 95 | issue = 3| pages = 295–301 }}</ref> It is classified as [[Endangered]] in South Australia.<ref>http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/plants-and-animals/native-plants-animals-and-biodiversity/native-animals/reptiles</ref> Another example  [[Chestnut-rumped heathwren]] status has changed from [[Vulnerable species|Vulnerable]] in 2008,<ref name="naturalresources.sa.gov.au">http://www.naturalresources.sa.gov.au/files/sharedassets/public/plants_and_animals/threatened_species/pa-fact-chestnutrumpedheathwrenmountloftyranges.pdf</ref> to [[Endangered]] in 2016.<ref name="ReferenceB">http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/plants-and-animals/native-plants-animals-and-biodiversity/native-animals/birds</ref> Also, [[Western Pygmy Possum]] were listed as Vulnerable in AMRL in 2008<ref name="ReferenceC">http://www.naturalresources.sa.gov.au/files/sharedassets/public/plants_and_animals/threatened_species/pa-fact-westernpygmypossum.pdf</ref> and became Critically Endangered in the Adelaide and Mt Lofty Ranges Region in Sep 2015.<ref>Threatened Species at Cleland Wildlife Park - Discovery Circle. (2015) (1st ed.).</ref>

'''Different emerging threats and species response'''

In 2008 wildfire was counted as the primary potential threat to [[Chestnut-rumped heathwren]] while deliberate burning, residential development (habitat has been cleared/fragmented) and dieback due to Phytophthora, were the secondary threats.<ref name="naturalresources.sa.gov.au"/> However, high threats in 2010 recorded as drought, climate change and roadside accidents (due to inappropriate access and works).<ref name="ReferenceB"/>

The [[Southern brown bandicoot]]'s (Isoodon obesulus obesulus) decline in 2008 was due to some threats like loss or degradation of woodland habitat (exposed habitats as the result of kangaroos overgrazing), broad scale removal of habitats, predation (from introduced species like foxes and feral cats) and fire. In contrast, the decline of bandicoots in 2010 was due to climate change and drought, weed invasion (predominantly woody weeds) and dieback (particularly Phytophthora cinnamomi). These threats in addition to some other threats (nutrient input, altered hydrological regimes and fire regimes) have degraded the health of the forests and hence bandicoot habitats in general.<ref>Informing Biodiversity Conservation for the Adelaide and Mount Lofty Ranges Region, South Australia. (2010) (1st ed.).
</ref>

'''"Lagging time" needs to be considered for predictions and future projects planning'''

Harris, in one of his work that published in 2009, mentioned [[Western pygmy possum]] (Cercartetus concinnus)  as "Least concern, Lower risk" in IUCN list. At the time of his work, the species was not recognized to be endangered in the national or state level in Victoria, Western Australia or South Australia. However, at the same time, New South Wales listed this species as endangered since their distribution was threatened by massive vegetation clearing, the reduction of food sources (by overgrazing of livestock), fire regimes and introduced predators (like red fox and domestic cats) and some native Australian carnivors such as quolls, snakes and owls.<ref>{{cite journal | last1 = Harris | first1 = J | year = 2009 | title = Cercartetus concinnus (Diprotodontia: Burramyidae) | doi = 10.1644/831.1 | journal = Mammalian Species | volume = 831 | issue = | pages = 1–11 }}</ref> 
While the [[Western pygmy possum]]'s conservation status remained [[Vulnerable species|Vulnerable]] in AMRL in 2008  <ref name="ReferenceC"/>
and 2014,<ref>http://www.naturalresources.sa.gov.au/adelaidemtloftyranges/plants-and-animals/native-plants-animals-and-biodiversity/native-animals/mammals</ref> its latest status is [[Critically Endangered]] in the Adelaide and Mt Lofty Ranges Region in 2015. This could indicate some knowledge gaps about this particular species' responses.<ref>Threatened Species at Cleland Wildlife Park - Discovery Circle. (2015) (1st ed.)
</ref>

'''Threatened species and ecological communities response''' 
	
During Ash Wednesday bushfires in the South East in Feb 1983, many areas were severely burnt (including Scrub, Whennan's, Mount McIntyre, Yeate's, Millicent Golf Course, Mount Burr Mill Drop, East McRostie's, West McRostie's, Brooksby Road and Hackett Hill), [[Southern brown bandicoot]]s inhabited those areas again (except Yeate’s) by as early as April 1990. The study suggested that during the Ashe Wednesday bushfires, bandicoots moved through the extensive pine plantations and migrated to native vegetation in the neighbourhood. They found dispersal corridors along the edge of the fire by using the dense ground cover. However, in Cleland and the Mt Lofty Ranges, Paull stated that “It is not known whether bandicoots survived the fires within these patches in refugal areas or whether they recolonised from neighbouring unburnt patches.”<ref name="Paull, D. 1995">Paull, D. (1995). The distribution of the southern brown bandicoot (Isoodon obesulus obesulus)</ref>

'''The importance of habitat quality as well as degradation/fragmentation and destruction'''

The study by Paull (1995) suggested that the response of the [[Southern brown bandicoot]] to fire (controlled burns and uncontrolled bushfires) needed to be evaluated. In his article, Paull indicated that bandicoots preferred habitats that regenerated into patchy mosaics of scrub as they were less likely to be effected by bushfires. Further studies by him confirmed that bandicoots were vulnerable because of habitat fragmentation (an outcome of extensive historical vegetation clearance) and predation by feral carnivores in the South East and Mount Lofty Ranges.<ref name="Paull, D. 1995"/>

Also, the Mount Lofty Ranges’ [[Chestnut-rumped heathwren]] are endangered nationally and in SA according to the IUCN Red List since it has a small population and distribution which is continuing to decline further. This bird is threatened by habitat fragmentation caused by extensive land clearance through the region, residential development, invasion by woody weeds and feral predators.<ref name="dx.doi.org">{{cite journal | last1 = Joseph | first1 = L. | last2 = Field | first2 = S. | last3 = Wilcox | first3 = C. | last4 = Possingham | first4 = H. | year = 2006 | title = Presence-Absence versus Abundance Data for Monitoring Threatened Species | doi = 10.1111/j.1523-1739.2006.00529.x | journal = Conservation Biology | volume = 20 | issue = 6| pages = 1679–1687 }}</ref>

On the habitat quality subject for the [[Adelaide pygmy blue-tongue skink]] (Tiliqua adelaidensis), it has been observed that when artificial burrows were offered in the field to lizards, all lizards preferred vertical burrows over angled burrows, while juvenile lizards preferred shallower burrows than did adult lizards. Observation of 36 artificial burrows showed a significant increase in lizard numbers, during 2001-02 and over three surveys. The same study suggested that this local increase in population could be due to lizards locating appropriate burrows much more easily. The overall success could be appreciated as a tool for conservation management of this endangered species.<ref>{{cite journal | last1 = Milne | first1 = T. | last2 = Michael Bull | first2 = C. | last3 = Hutchinson | first3 = M. | year = 2003 | title = Fitness of the Endangered Pygmy Blue Tongue Lizard Tiliqua adelaidensis in Artificial Burrows | doi = 10.1670/38-03n | journal = Journal Of Herpetology | volume = 37 | issue = 4| pages = 762–765 }}</ref> Another study compared the fitness of female pygmy blue-tongue lizards in natural burrows and artificial ones, over a 3-year period. The study showed that the females in the artificial burrows had better body conditions and produced larger offspring with better body conditions also.<ref>{{cite journal | last1 = Souter | first1 = N. | last2 = Bull | first2 = C.M. | last3 = Hutchinson | first3 = M.N. | year = 2004 | title = Adding burrows to enhance a population of the endangered pygmy blue tongue lizard, Tiliqua adelaidensis | doi = 10.1016/s0006-3207(03)00232-5 | journal = Biological Conservation | volume = 116 | issue = 3| pages = 403–408 }}</ref>

'''Cost effective monitoring'''

Operations of projects are determined by district and regional priorities and resource availability such as funding and staffing.  
A study on [[Chestnut-rumped heathwren]] showed that to predict the risk of extinction precisely, assessment of the trends of population size and distribution are required. Therefore, choosing the most cost-effective monitoring techniques for species with low density and visibility are key factors for managers. Although effective detection of population trends is essential for managing species under threat, the main factor is the amount of money available for the monitoring technique. 
<ref name="dx.doi.org"/>

==Mount Lofty summit==
{{see also|Mount Lofty}}
This popular tourist destination is {{Convert|727|m}} above sea level. It provides sweeping vistas across the Adelaide Plains and Gulf St Vincent. Flinders Column, a white painted obelisk shaped like a lighthouse, is a landmark which can be seen from far away on a clear day.

Car parking facilities are provided: charges are payable. Public bus route 823 serves the summit with three journeys a day (including weekends and most holidays).<ref name="adelaidemetro.com.au"/>

Other facilities include an information centre/ souvenir shop, a cafe/restaurant (closed Mondays) and public toilets.

==Waterfall Gully==
{{see also|Waterfall Gully}}
[[File:CLELAND WILDLIFE PARK.jpg|thumb|Yellow-footed rock wallabies in the park]]
[[File:CLELAND WILDLIFE PK.jpg|thumb|[[Cape Barren goose]]]]
Waterfall Gully, another popular part of the park, is located on its western edge. It can be accessed via the sealed Waterfall Gully Road. A limited amount of free car parking is provided.  There is no scheduled public transport service.

The main attraction is a waterfall, the largest of several in the park. The base is a short walk from the car park and the top can be reached by a formed but steep footpath, which continues to Cleland Wildlife Park and Mount Lofty summit.<ref>http://www.southaustraliantrails.com/trails.asp?id=13811</ref>

Other facilities include a kiosk/ restaurant and public toilets.

==Other==
By far the largest part of the park consists of bushland, mostly woodland with some open spaces where clearing has taken place.<ref>A Hardy ''The Nature of Cleland'' 2nd Ed 1989</ref>

There are a number of walking trails, including the Waterfall Gully - Mount Lofty summit trail which is a popular and reasonably challenging  ascent of the west side of Mount Lofty,<ref name="southaustraliantrails.com"/> and parts of the long distance [[Heysen Trail|Heysen]] and [[Yurrebilla Trail|Yurrebilla]] Trails, which run north–south along the higher ground in the east of the park.

As of 22 December 2012, many trails in the park became shared-use, allowing mountain bikers to also make use of the trails.<ref>http://www.ridemorebikes.com/mountain-biking-cleland-conservation-park/  Retrieved 02-01-2013</ref>

==See also==
* [[List of protected areas in Adelaide]]
*[[Cleland (disambiguation)]]

==References==
{{reflist}}

==Further reading==
*{{Citation | author1=Hardy, Anne | title=The nature of Cleland | publication-date=1989 | publisher=State Publishing | edition=2nd | isbn=978-0-7243-6542-5 }}

==External links==
*[http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide_Hills/Cleland_Conservation_Park Cleland Conservation Park official webpage]
*[http://www.protectedplanet.net/24317 Cleland Conservation Park webpage on protected planet]
{{Commons category-inline|Cleland Wildlife Park}}

{{Protected areas of South Australia}}
{{Zoos of South Australia}}

[[Category:Protected areas in Adelaide]]
[[Category:Conservation parks of South Australia]]
[[Category:Wildlife sanctuaries of Australia]]
[[Category:1945 establishments in Australia]]
[[Category:Articles needing infobox zoo]]