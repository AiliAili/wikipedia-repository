{{Infobox scholar
| name = Ellen Gruenbaum
| image = <!-- filename only, i.e. without the File: (or Image:) prefix or enclosing [[brackets]] -->
| imagesize = 
| alt = 
| caption = 
| fullname = 
| othernames = 
| birth_name = 
| birth_date = <!-- {{birth date and age|YYYY|MM|DD}} for living persons, {{dirth date|YYYY|MM|DD}} for deceased -->
| birth_place = [[Burlington, California]]
| death_date = <!-- {{death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place = 
| death_cause = 
| era = 
| region = 
| alma_mater = [[University of Connecticut]]
| school_tradition = 
| main_interests = [[Gender studies]] and [[history]]
| principal_ideas = 
| major_works = ''The Female Circumcision Controversy''
| awards = 
| influences = 
| influenced = 
| footnotes = 
}}
'''Ellen Gruenbaum''' is an American anthropologist.  A specialist in researching medical practices that are based on a society’s culture.<ref name=Directory/>

== Personal life ==

Gruenbaum was born in St. Louis, Missouri, USA, and  received her A. B. in [[Anthropology]] at [[Stanford University]] in 1974.<ref name=Directory/> She went on to  the [[University of Connecticut]] to earn her M. A. in 1974 and her Ph. D. in 1982 in Anthropology.<ref>"Ellen Gruenbaum." LinkedIn.Web. <http://www.linkedin.com/profile/view?id=80753845&authType=name&authToken=BzCy>.</ref> Her doctoral thesis was "Health services, health, and development in Sudan : the impact of the Gezira irrigated scheme".<ref name=wct>[http://www.worldcat.org/title/health-services-health-and-development-in-sudan-the-impact-of-the-gezira-irrigated-scheme/oclc/12888556&referer=brief_results   WorldCat]</ref>

== Career ==

Gruenbaum is   professor and chair of the Anthropology Department at [[Purdue University]]. She has served as a professor of Anthropology, Director of the Women’s Studies Program and Dean of the College of Social Sciences at California State University, Fresno. She has worked at California State University, San Bernardino, University of Wisconsin in Manitowoc, and the University of Khartoum, Sudan.<ref>Gruenbaum, Ellen, "Purdue University." Directory. Purdue University, Aug. 2008. Web. 24 Apr. 2013.</ref>
Since August 2008  she has been   Department Head of Anthropology at  Purdue .<ref name=Directory>"Directory - Anthropology : College of Liberal Arts : Purdue University." College of Liberal Arts : Purdue University. Purdue University, Aug. 2008. Web. 17 Apr. 2013. <http://www.cla.purdue.edu/anthropology/directory/?personid=1560></ref>  She teaches cultural anthropology coursed that cover religion, gender, health and post-colonial situations in Africa and the Middle East.<ref name=Directory/> As a medical anthropologist she was the secretary for the Society for Medical Anthropology.<ref name=Directory/> She is on the editorial advisory board for the ''[[Journal of Middle East Women's Studies]]''.<ref name=Directory/>

Her specialization is medical anthropology with a focus on culturally oriented issues, particularly in Africa and the Middle East. Much of her research occurred in Sudan between 1974-1979, 1998, 1992, and 2004.<ref name=SMA>Gruenbaum, Ellen, "Society for Medical Anthropologists Global Directory."SMA. American Anthropological Association, n.d. Web. 24 Apr. 2013.</ref> She also conducted research in Sierra Leone between 2007-2008. Gruenbaum’s research focuses on practices relating to female genital cutting (circumcision), women’s health in rural African and Middle Eastern contexts, and child protection and human rights movements.<ref>Henriques, Isabel. "Medical Anthropologist Speaks on Abolishing Female Circumcision."Stop FGM Now. Waris Dirie Foundation, 29 Sept. 2010. Web. 24 Apr. 2013.</ref>

She has written several works including The Female Circumcision Controversy: An Anthropological Perspective, A Study of Social Services in Gezira Province, A Movement Against Clitoridectomy and Infibulation in Sudan, Nuer Women in Southern Sudan, and Development Schemes, Cultural Debates, and Rural Women’s Health in Sudan.

She served as the Secretary of the Society for Medical Anthropology from 2006 until 2009 and is on the editorial advisory board of the Journal of Middle East Women’s Studies.<ref name=SMA/> She is a member of the [[American Anthropological Association]] and has memberships in the Association for Africanist Anthropology, Association for Feminist Anthropology, Middle East Section, and [[Society for Medical Anthropology]].

She has worked with   another Purdue professor, Sophie A Lelièvre,   in Ghana to research cultural aspects of policy decisions related to breast health in many countries including Ghana, Lebanon, France, Japan, and Uruguay.<ref>Fiorini, Phil. "Purdue University; University News Service." Purdue Professors to Advance Breast Cancer Research Partnership through Trip to Ghana. Purdue University, 10 Jan. 2012. Web. 24 Apr. 2013.</ref>
Gruenbaum considers her greatest achievement in her career to be working with UNICEF in 2004 when she was able to conduct research and advocate social marketing for the involvement of girls in female genital cutting and circumcision.<ref name=interview>Gruenbaum, Dr. Ellen, and Emily Johnson. E-mail interview.
17 Apr. 2013.</ref> She also worked as a research consultant for CARE studying traditional health practices.<ref>Gruenbaum, Ellen, "Department of Anthropology." Ellen Gruenbaum Sabbatical in Sudan. Fresno State, n.d. Web. 24 Apr. 2013.</ref><ref>Celia W. Dugger, [https://www.nytimes.com/1996/10/05/world/genital-ritual-is-unyielding-in-africa.html "Genital Ritual Is Unyielding in Africa"], ''The New York Times'', 5 October 1996, p.&nbsp;3/5.</ref>

== Selected bibliography ==
=== Books ===
Gruenbaum is best known for her book ''The Female Circumcision Controversy''.<ref>{{cite book | last = Gruenbaum | first = Ellen | title = The female circumcision controversy: an anthropological perspective | publisher = University of Pennsylvania Press | location = Philadelphia | year = 2001 | isbn = 9780812217469 }}</ref> This book presents an account of the [[Female genital mutilation|female circumcision controversy]] in parts of Africa and the Middle East.<ref>{{Cite journal | title = Book review: Gruenbaum, Ellen, ''The Female Circumcision Controversy: An Anthropological Perspective'' | journal = [[Journal of the Royal Anthropological Institute]] | volume = 8 | issue = 1 | pages = 159–206 | publisher = [[Wiley-Blackwell|Wiley]] | doi = 10.1111/1467-9655.00104 | date = March 2002 | url = http://dx.doi.org/10.1111/1467-9655.00104 | ref = harv | postscript = .}}</ref>

=== Journal articles ===
Gruenbaum has also written a number of articles that reflect her work in Sudan, including:
* {{Cite journal | last = Gruenbaum | first = Ellen | title = Book review: "Women's Medicine: The ''Zar-Bori'' Cult in Africa and Beyond", I. M. Lewis, Ahmed Al-Safi, and Sayyid Hurreiz, eds | journal = [[Medical Anthropology Quarterly]] | volume = 9 | issue = 3 | pages = 418–420 | publisher = [[Wiley-Blackwell|Wiley]] | doi =10.1525/maq.1995.9.3.02a00100 | jstor = 649350 | date = September 1995 | url = http://dx.doi.org/10.1525/maq.1995.9.3.02a00100 | ref = harv | postscript = .}}
* {{Cite journal | last = Gruenbaum | first = Ellen | title = Feminist activism for the abolition of FGC in Sudan | journal = [[Journal of Middle East Women's Studies]] | volume = 1 | issue = 2 | pages = 89–111 | publisher = [[Duke University Press]] | doi = 10.1215/15525864-2005-2004  | jstor = 40326858 | date = Spring 2005 | url = http://dx.doi.org/10.1215/15525864-2005-2004  | ref = harv | postscript = .}}
* {{Cite journal | last = Gruenbaum | first = Ellen | title = Socio-cultural dynamics of female genital cutting: research findings, gaps, and directions | journal = [[Culture, Health & Sexuality]], special issue: Themed Symposium: Female Genital Cutting | volume = 7 | issue = 5 | pages = 429–441 | publisher = [[Taylor and Francis]] | doi = 10.1080/13691050500262953 | jstor = 4005473 | pmid = 16864214 | date = September 2005 | url = http://dx.doi.org/10.1080/13691050500262953 | ref = harv | postscript = .}}
* {{Cite journal | last = Gruenbaum | first = Ellen | title = Sexuality issues in the movement to abolish female genital cutting in Sudan | journal = [[Medical Anthropology Quarterly]] | volume = 20 | issue = 1 | pages = 121–138 | publisher = [[Wiley-Blackwell|Wiley]] | doi = 10.1525/maq.2006.20.1.121 | pmid = 16612996 | date = March 2006 | url = http://dx.doi.org/10.1525/maq.2006.20.1.121 | ref = harv | postscript = .}}

=== Working papers === 
* {{cite book | last = Gruenbaum | first = Ellen | title = Nuer women in Southern Sudan: health, reproduction and work | publisher = [[National Bureau of Economic Research|National Bureau of Economic Research (NBER)]] | year = 1990 | url = http://gencen.isp.msu.edu/documents/Working_Papers/ | id = WP215 | format = pdf }}

== References ==
{{reflist}}

{{Subject bar |portal1 =Biography| portal2 = History| portal3 = Anthropology| portal4 = Science | portal5 = Gender studies | portal6 = Sociology | portal7 = Culture |portal8 = United States }}

{{Authority control}}

{{DEFAULTSORT:Gruenbaum, Ellen}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American anthropologists]]
[[Category:American women anthropologists]]
[[Category:Purdue University faculty]]
[[Category:University of Connecticut alumni]]
[[Category:Stanford University alumni]]