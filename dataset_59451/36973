{{good article}}
<!-- Begin Infobox horse.  The text of the article should go AFTER this section. See: -->
<!-- http://en.wikipedia.org/wiki/Template talk:Infobox horse -->
<!-- for full explanation of the syntax used in this template. -->
{{Infobox horse 
|name= Azteca
|image=AztecaHorse.jpg 
|image_caption=Azteca horse
|features = 
|altname= 
|nickname= 
|country= [[Mexico]]
|group1= 
|std1= 
}}  
<!-- End Infobox horse info. Article Begins Here -->

The '''Azteca''' is a [[horse breed]] from [[Mexico]], with a subtype, called the "American Azteca", found in the United States. They are well-muscled horses that may be of any solid [[equine coat colors|color]], and the American Azteca may also have [[Pinto horse|pinto]] coloration. Aztecas are known to compete in many [[western riding]] and some [[English riding]] disciplines. The Mexican registry for the original Azteca and the United States registries for the American Azteca have registration rules that vary in several key aspects, including ancestral bloodlines and requirements for physical inspections. The Azteca was first developed in Mexico in 1972, from a blend of [[Andalusian horse|Andalusian]], [[American Quarter Horse]] and Mexican [[Criollo horse|Criollo]] bloodlines. From there, they spread to the United States, where [[American Paint Horse]] blood was added.

==Breed characteristics==
The three foundation breeds of the Azteca are the Andalusian (defined by the Mexican registry as either [[Andalusian horse|Pura Raza Española]] or [[Lusitano]]), American Quarter Horse, and Mexican Criollo or ''Criollo militar''.<ref name=amccra>[http://www.caballoazteca.org.mx/estandar.html Estandár] (in Spanish) AMCCRA Asociación Mexicana de Criadores de Caballos de Raza Azteca, A.C. Retrieved 2010-01-03. "Standard".</ref> They were chosen to produce a breed that combined athletic ability with a good temperament and certain physical characteristics.  Azteca [[stallion]]s and [[gelding]]s measure between {{hands|15|and|16.1}} at the [[withers]], while [[mare]]s stand between {{hands|14.3|and|16}}.<ref name=caskie>Caskie, Donald M. (Azteca Horse Association of Canada) [http://www.equiworld.net/breeds/azteca/index.htm Azteca: a horse custom-built for performance, style and tradition] Retrieved 2012-01-04.</ref> The ideal height is {{hands|14.3|-|15.1}}.<ref name=alta>[http://www.alta-escuela.com/cels/c_azteca.html El Caballo Azteca] (in Spanish) Alta Escuela Mexicana de Jinetes Domecq, 2010. Retrieved 2012-01-04. "The Azteca horse".</ref>   Both sexes usually weigh from {{convert|1000|to|1200|lbs|kg}}. The facial profile of the breed is straight or [[wikt:convex|convex]] and the neck slightly arched. Overall, they are well-muscled horses, with broad [[rump (animal)|croup]] and chest, as well as long, sloping shoulders.<ref name=Dutson/> [[Horse gait|Gaits]] are free and mobile, with natural [[collection (horse)|collection]] derived from the Andalusian ancestry of the breed.<ref name=Lynghaug/> The breed is found in all solid [[equine coat color|colors]], although [[gray (horse)|gray]] is most often seen. [[Horse markings|White markings]] are allowed on the face and lower legs by [[breed registry|breed associations]].<ref name=Dutson>Dutson, pp. 82-84</ref> The American Azteca registry also allows non-solid [[Pinto horse|pinto]] coloration.<ref name=Lynghaug>Lynghaug, pp. 143-145</ref> 
[[File:Azteca caballo.JPG|thumb|Side view]]

===Registration===
According to the breed standard of the Mexican registry, Azteca horses cannot have more than 75 percent of their parentage from any one of the foundation breeds (Andalusian, Quarter Horse and Mexican Criollo); Criollo blood may be no more than 50%, and only from unregistered mares within Mexico. Horses are classified in one of six registration categories, designated with letters A through F, depending on their parentage. Only certain crosses between the different classes are permitted.<ref name=esquema>[http://www.caballoazteca.org/IMG/doc/Esquema_OFICIAL_de_cruzamiento_AMCCRA.doc Esquema de cruzamiento para el caballo azteca]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} (in Spanish) Asociación Mexicana de Criadores de Caballos de Raza Azteca, A.C., y A.G. Retrieved 2012-01-04. "Official cross-breeding chart for the Azteca horse"</ref>  In Mexico, Azteca horses must conform to a strict [[phenotype]] standard established by the Secretaría de Agricultura, Ganadería, Desarrollo Rural, Pesca y Alimentación (SAGARPA), the Mexican agriculture ministry, which requires [[studbook selection|inspection]] of [[foals]] at seven months for the issue of a "birth certificate"; a foal that does not meet the breed standards may be denied registration even if both parents are registered Aztecas approved for breeding. Full registration and approval for breeding are subject to a second and more detailed inspection at age three or more, and granted only to those horses that fully satisfy the requirements of the standard.<ref name=Dutson/><ref name=cae>[http://www.caballoazteca.org/spip.php?article3 Documentación]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} (in Spanish) Caballo Azteca Europa. Retrieved 2012-01-06. "Documentation"</ref>

In the American Azteca registry, horses with [[American Paint Horse]] (APHA) breeding are also allowed. However, horses with more than 25 percent [[Thoroughbred]] blood in their pedigrees (common in many Paints and Quarter Horses) within four generations cannot be registered.<ref name=Lynghaug/> American Aztecas have four categories of registration based on the relative degree of blood from each foundation breed, seeking an ideal blend of 3/8 Quarter Horse and 5/8 Andalusian.<ref name=Lynghaug/> Unlike their Mexican counterparts, they do not have to go through physical inspections before being registered.<ref>{{cite web|url=http://www.americanazteca.com/American_Azteca_Our_Breeds_Official_Name.html|title=American Azteca: Our Breed's Official Name|publisher=American Azteca Horse International Association|accessdate=2011-05-30}}</ref>

==History==
[[File:Azteca1.jpg|thumb|right|An Azteca under saddle]]
The Azteca was first bred in 1972 as a horse for ''[[charro]]s'', the traditional horsemen of Mexico.<ref name=Dutson/> Antonio Ariza Cañadilla, along with others, was instrumental in the creation of the Azteca horse as the national horse of Mexico and with its official recognition by the Mexican Department of Agriculture on November 4, 1982. Ariza used imported Andalusians, crossed with Quarter Horses and Criollos and began to breed the foundation horses of the Azteca breed at Rancho San Antonio near [[Texcoco, Mexico]].<ref>{{cite web|url=http://www.ansi.okstate.edu/breeds/horses/azteca/index.htm|title=Azteca|publisher=Oklahoma State University|accessdate=2011-05-29| archiveurl= https://web.archive.org/web/20120609111209/http://www.ansi.okstate.edu/breeds/horses/azteca/index.htm| archivedate=June 9, 2012<!--DASHBot-->| deadurl= no}}</ref> Early in the Azteca's history, breeders realized the need for a unified breeding program in order to produce horses that met the required characteristics. The Azteca Horse Research Center was created at [[Lake Texcoco]], and in partnership with breeders developed the phenotype of the breed today. The first official Azteca was a stallion named Casarejo, who was a cross between an Andalusian stallion named Ocultado and a Quarter Horse mare named Americana. He was foaled at the ''Centro de Reproduccion Caballar Domecq'' in 1972.<ref>{{cite web|url=http://imh.org/index.php?option=com_flexicontent&view=items&cid=197:north-america&id=2237:azteca&Itemid=253|title=Azteca|publisher=International Museum of the Horse|accessdate=2012-01-03}}</ref>

The ''Associacion Mexicana de Criadores de Caballos de Raza Azteca'', or Mexican Breeders Association for the Azteca Horse, is the original breed registry and still maintains the international registry. The International Azteca Horse Association and its regional affiliates was formed in 1992. The majority of Aztecas are found in Mexico, and the Mexican association had registered between 10,000 and 15,000 horses as of 2005, according to the [[Texas Department of Agriculture]]. The Mexican registry adds approximately 1,000 horses per year.<ref name=Dutson/>

The Azteca Horse Registry of America was formed in 1989 for registering the US portion of the breed, followed by the Azteca Horse Owners Association in 1996 as an owners association.<ref>Harris and Langrish, p. 65</ref> This registry has slightly different registration and breeding rules, and is not approved by the Mexican government to register Azteca horses.<ref name=Dutson/> The American registry, now called the American Azteca Horse International Association, allows the use of American Paint horses, which are essentially Quarter Horses with pinto coloration, if they have less than 25 percent [[Thoroughbred]] breeding.  However, the US registry does not incorporate Criollo bloodlines.  The Mexican registry allows only the blood of Quarter Horses, Andalusians and Criollos in its registered Aztecas.<ref name=Lynghaug/>

==Uses==
Because of the breeds that make up the Azteca, they are known for their athleticism. They have been seen in competition in [[western riding]] events such as [[reining]], [[cutting (sport)|cutting]], [[team penning]] and [[roping]], as well as [[English riding]] events such as [[dressage]] and other events such as [[polo]] and [[bullfighting]]. They are also used for [[pleasure riding]].<ref name=Dutson/>

==Notes==
{{reflist}}

==References==
* {{cite book|title=Storey's Illustrated Guide to 96 Horse Breeds of North America|author=Dutson, Judith|publisher=Storey Publishing|year=2005|isbn=1-58017-613-5}}
* {{cite book|url=https://books.google.com/books?id=-M-lPnkMRe8C&pg=PA65&dq=azteca+horse&hl=en&ei=SvjiTZ2rJo7RiAL45KzHBg&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDQQ6AEwAQ#v=onepage&q=azteca%20horse&f=false|title=America's Horses: A Celebration of the Horse Breeds Born in the U.S.A|author=Harris, Moira C. and Bob Langrish|year=2006|publisher=Globe Pequot|isbn=1-59228-893-6}}
* {{cite book|url=https://books.google.com/books?id=r1baD0KOFPcC&pg=PA146&dq=azteca+horse&hl=en&ei=SvjiTZ2rJo7RiAL45KzHBg&sa=X&oi=book_result&ct=result&resnum=5&ved=0CEQQ6AEwBA#v=onepage&q=azteca%20horse&f=false|title=The Official Horse Breeds Standards Guide: The Complete Guide to the Standards of All North American Equine Breed Associations|author=Lynghaug, Fran|publisher=Voyageur Press|year=2009|isbn=0-7603-3499-4}}

==External links==
{{commons category|Azteca horse}}
* [http://www.americanazteca.com/ American Azteca Horse International Association]
* [http://caballoazteca.org.mx/ Asociación Mexicana de Criadores de Caballos de Raza Azteca] (in Spanish)
* [https://web.archive.org/web/20110623022542/http://www.caballoazteca.org/ - Caballo Azteca Europa] (in Spanish)

{{Equine|state=collapsed}}

[[Category:Horse breeds]]
[[Category:American Quarter Horses]]
[[Category:Horse breeds originating in Mexico]]