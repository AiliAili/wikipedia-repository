{{good article}}
{{Infobox baseball game
|year = 1978
|game = American League East tie-breaker game
|image = [[File:Fenway Park02.jpg|250px]]
|caption = [[Fenway Park]], the host of the 1978 American League East tie-breaker game 
|visitor = [[1978 New York Yankees season|New York Yankees]]
|home = [[1978 Boston Red Sox season|Boston Red Sox]]
|date = October 2, [[1978 in baseball|1978]]
|venue = [[Fenway Park]]
|city = [[Boston, Massachusetts]]
|attendance = 32,925
|umpires = {{unbulleted list
 |[[Home plate|HP]]: [[Don Denkinger]]
 |[[First base|1B]]: [[Jim Evans (umpire)|Jim Evans]]
 |[[Second base|2B]]: [[Al Clark (umpire)|Al Clark]]
 |[[Third base|3B]]: [[Steve Palermo]]
 }}
}}
The '''1978 American League East tie-breaker game''' was a [[one-game playoff|one-game extension]] to [[Major League Baseball]]'s (MLB) [[1978 Major League Baseball season|1978 regular season]], played between the [[Yankees–Red Sox rivalry|rival]] [[New York Yankees]] and [[Boston Red Sox]] to determine the winner of the [[American League]]'s (AL) [[American League East|East Division]]. The game was played at [[Fenway Park]] in [[Boston]], on October 2, 1978. The tie-breaker was necessitated after the Yankees and Red Sox finished the season tied for first place in the AL East with identical 99–63 records. The Red Sox were the [[Home advantage|home team]] by virtue of a [[coin toss]]. In baseball statistics, the tie-breaker counted as the 163rd regular season game for both teams, with all events in the game added to regular season statistics.

[[Ron Guidry]] [[starting pitcher|started]] for the Yankees, while [[Mike Torrez]] started for the Red Sox. The Yankees fell behind 2–0, with a [[home run]] by [[Carl Yastrzemski]] and a [[run batted in]] [[single (baseball)|single]] by [[Jim Rice]]. The Yankees took the lead on a three run [[home run]] by [[Bucky Dent]]. The Yankees defeated the Red Sox 5–4, with Guidry getting the [[win (baseball)|win]], while [[Goose Gossage]] recorded a [[save (baseball)|save]]. With the victory, the Yankees clinched the AL East championship, en route to winning the [[1978 World Series]]. {{as of|2016}}, the '78 Yankees remain the last team to have won the World Series after playing a tiebreaker.

==Background==
{{main article|1978 New York Yankees season|1978 Boston Red Sox season}}
The Yankees and Red Sox had combined to win the past three [[American League]] (AL) [[List of American League pennant winners|pennants]]. The Red Sox lost the [[1975 World Series]]. The Yankees lost the [[1976 World Series]], and then won the [[1977 World Series]]. The Yankees and Red Sox were both seen as contenders for the [[American League East|AL East]]. The Yankees, Red Sox, and [[Baltimore Orioles]], who competed for the AL East championship in 1977, all expected that they would compete for the AL East in 1978. The Orioles and Red Sox tied for second place in 1977, {{frac|2|1|2}} games behind the Yankees.<ref name=robesonian>{{cite news|url=https://news.google.com/newspapers?id=9zBVAAAAIBAJ&sjid=wz0NAAAAIBAJ&pg=5018,3619334&dq=yankees+red+sox&hl=en|newspaper=The Robesonian|title=Yankees, Red Sox And Orioles Are Optimistic About New Year|first=Schel|last=Nissenson|agency=[[Associated Press]]|date=March 30, 1978|accessdate=June 18, 2012}}</ref> The young [[Detroit Tigers]], with [[Lou Whitaker]] and [[Alan Trammell]], also appeared ready to challenge for the AL East.<ref name=robesonian/><ref>{{cite news|url=https://news.google.com/newspapers?id=r5djAAAAIBAJ&sjid=NkwDAAAAIBAJ&pg=2306,5981274&dq=yankees+red+sox&hl=en |title=Tigers just might surprise Yankees, Red Sox|newspaper=Ludington Daily News|date=March 16, 1978|accessdate=June 18, 2012}}</ref>

The Red Sox signed [[Mike Torrez]], who won two games in the 1977 World Series for the Yankees, as a [[free agent]] during the offseason.<ref name=robesonian/> Before the season, the Red Sox acquired [[Dennis Eckersley]] to join Torrez, [[Bill Lee (left-handed pitcher)|Bill Lee]], and [[Luis Tiant]] in their starting rotation.<ref>{{cite news|url=https://news.google.com/newspapers?id=7Z8oAAAAIBAJ&sjid=hykEAAAAIBAJ&pg=1575,6093918&dq=yankees+red+sox&hl=en |newspaper=The Milwaukee Journal|title=Red Sox Get Eckersley, Aim at Yanks|agency=Associated Press|page=9|date=March 31, 1978}}</ref> The Yankees acquired [[Goose Gossage]] and [[Rawly Eastwick]] to join [[Sparky Lyle]], 1977's AL [[Cy Young Award]] winner, in their bullpen during the offseason.<ref name=robesonian/> Both teams placed five players on the AL squad for the [[1978 Major League Baseball All-Star Game]]: Gossage, [[Ron Guidry]], [[Graig Nettles]], [[Thurman Munson]], and [[Reggie Jackson]] represented the Yankees, while [[Carl Yastrzemski]], [[Fred Lynn]], [[Rick Burleson]], [[Carlton Fisk]], and [[Jim Rice]] represented the Red Sox.<ref>{{cite news|url=https://news.google.com/newspapers?id=h04sAAAAIBAJ&sjid=os0EAAAAIBAJ&pg=2192,1466036&dq=yankees+red+sox&hl=en |title=Yankees, Red Sox place 5 players in All-Stars|newspaper=[[Spartanburg Herald-Journal]]|date=July 7, 1978|accessdate=June 18, 2012}}</ref>

The Red Sox had once led by 10 games; the [[Milwaukee Brewers]] were in second place at the time, while the Yankees were in third.<ref name="1978Standings">{{harvnb|Shaughnessy|1990|p=136}}</ref> The Yankees experienced injuries to [[Don Gullett]], [[Willie Randolph]], [[Catfish Hunter]], [[Bucky Dent]], and [[Mickey Rivers]],<ref>{{cite news|url=https://news.google.com/newspapers?id=bSMhAAAAIBAJ&sjid=emcEAAAAIBAJ&pg=5884,558602&dq=yankees+injuries&hl=en|title=Yankees' Injuries Continue|newspaper=Sarasota Herald-Tribune|agency=Associated Press|date=July 2, 1978|page=2-D|accessdate=June 18, 2012}}</ref> and fell to fourth place in the division.<ref>{{cite news|title=Yankees Fall to Fourth; In His Swinging Zone Almost a Yankee Another Chance Pitching Differently Not in the Groove|first=Murray|last=Chass|url=https://select.nytimes.com/gst/abstract.html?res=FA071EFD345513728DDDAE0994DF405B888BF1D3|newspaper=[[The New York Times]]|date=July 17, 1978|page=C4|authorlink=Murray Chass|accessdate=June 18, 2012}} {{subscription required|date=June 2012}}</ref>  After a shake up engineered by owner [[George Steinbrenner]], with Munson moving from catcher to [[right field]],<ref>{{cite news|url=https://news.google.com/newspapers?id=1NBHAAAAIBAJ&sjid=zf8MAAAAIBAJ&pg=1419,1876810&dq=yankees+red+sox&hl=en |title=George Steinbrenner shankes up Yankees' lineup|newspaper=[[The Morning Record and Journal]]|date=July 14, 1978|agency=[[United Press International]]|accessdate=June 18, 2012}}</ref> the Yankees fired their combustible manager [[Billy Martin]], replacing him with [[Bob Lemon]].<ref>{{cite news|title=Bob Lemon is new manager|newspaper=The Globe and Mail|date=July 25, 1978|page=P31|author=United Press International|quote=Tempestuous Billy Martin...resigned as manager of the New York Yankees yesterday...Martin's demise followed the latest in a series of battles with Yankees' principal owner George Steinbrenner and star outfielder Reggie Jackson.}}</ref> The Yankees trailed Boston by 14 games by mid-July.<ref>{{harvnb|Frommer|Frommer|2004|p=38}}</ref> However, New York finished the season 53–21 in their last 74 games (a {{Winning percentage|53|21}} [[winning percentage]]), while the Red Sox went 38–36 ({{Winning percentage|38|36}}) over the same time frame.<ref name=milwaukee>{{cite news|url=https://news.google.com/newspapers?id=K1waAAAAIBAJ&sjid=mikEAAAAIBAJ&pg=6470,2153210&dq=yankees&hl=en|title=Yankees Win in Classic Way|agency=Associated Press|newspaper=The Milwaukee Journal|page=7|date=October 3, 1978|accessdate=June 15, 2012}}</ref> This included a four-game sweep of Boston in Fenway Park in early September.<ref name="Massacre">{{harvnb|Shaughnessy|1990|p=138}}</ref><ref name="Sweep"/> The Yankees outscored the Red Sox by a composite score of 42–9, and the series was dubbed "The Boston Massacre" by the sports press.<ref name="Massacre"/><ref name="Sweep">{{harvnb|Frommer|Frommer|2004|pp=175–177}}</ref> By the end of the four games, the two teams were tied for first place.<ref name="FinalGames">{{harvnb|Frommer|Frommer|2004|pp=47–48}}</ref>

The Yankees took the AL East lead three days later, and did not lose it until the final Sunday of the season.<ref name="FinalGames"/> Holding a one-game lead with seven games to play, New York finished on a 6–1 run.<ref name="FinalGames"/> However, Boston was a perfect 7–0, enabling them to tie the Yankees at season's end.<ref name="FinalGames"/> After New York lost to the [[Cleveland Indians]] on October 1,<ref>{{cite news|title=Indian ambush stalls yankee drive toward title|newspaper=The Globe and Mail|date=October 2, 1978|page=S2}}</ref> the Fenway Park video screen flashed the happy news: "THANK YOU [[Rick Waits|RICK WAITS]], GAME TOMORROW."<ref>{{harvnb|Frommer|Frommer|2004|p=48}}</ref><ref>{{cite news|title=Waits' Big Day Gave Sox Chance|last=Goldberg|first=Jeff|newspaper=The Hartford Courant|date=October 1, 2003|page=C3}}</ref>

==The game==
The tie-breaker game was the first in the AL since [[1948 American League tie-breaker game|1948]], when the Indians defeated the Red Sox at Fenway Park, and the first in MLB since the advent of the division system in 1969.<ref name=morningrecord/> Guidry, who won 24 games in the 162-game regular season,<ref name=morningrecord>[https://news.google.com/newspapers?id=qjZIAAAAIBAJ&sjid=TAENAAAAIBAJ&pg=4365,64195&dq=yankees+red+sox&hl=en ''The Morning Record and Journal''] via Google News Archive Search</ref> started on three days of rest, less rest than usual.<ref name=Lyle244>Lyle & Golenbock, p. 244</ref> Torrez started the game for the Red Sox.<ref name=morningrecord/> He started for the Red Sox on [[Opening Day]]<ref>{{cite news|url=https://news.google.com/newspapers?id=hf0sAAAAIBAJ&sjid=fM0FAAAAIBAJ&pg=1703,2002525&dq=yankees+red+sox&hl=en |title=Mike Torrez&nbsp;— Traveling Man finding job security in Boston|first=Karl|last=Gulbronsen|newspaper=[[The Palm Beach Post]]|date=April 3, 1978|accessdate=June 18, 2012}}</ref> and had a 16–12 record, but contributed to the Red Sox struggles late in the season with six consecutive losses.<ref name=morningrecord/>

[[File:Bucky Dent signs autographs.jpg|right|upright|thumb|[[Bucky Dent]]]]
[[Carl Yastrzemski]] hit a [[home run]] in the second inning, and [[Jim Rice]] drove in [[Rick Burleson]] with a [[single (baseball)|single]] in the sixth inning.<ref name=Lyle244/> Meanwhile, the Yankees had been held to two hits through six innings.<ref name=milwaukee/>  With one out in the seventh inning, [[Chris Chambliss]] and [[Roy White]] of the Yankees both singled off of Torrez, and pinch hitter [[Jim Spencer]] flied out.<ref>{{harvnb|Shaughnessy|1990|p=144}}</ref> Dent then hit a fly ball that cleared the [[Green Monster]] wall in left field to give the Yankees a 3–2 lead.<ref>{{harvnb|Frommer|Frommer|2004|p=178}}</ref><ref>{{cite news|title=Yankee power KIs Bosox hopes|last=Patton|first=Paul|newspaper=The Globe and Mail|date=October 3, 1978|page=P37}}</ref>

Torrez was removed from the game after walking [[Mickey Rivers]].  [[Relief pitcher|Reliever]] [[Bob Stanley (baseball)|Bob Stanley]] came in, and after Rivers stole second [[Thurman Munson]] drove him in with a double.<ref name=Lyle244/> In the eighth inning, a home run by [[Reggie Jackson]] made the score 5–2 in favor of the Yankees.<ref name=Lyle244/>  The Red Sox cut New York's lead to just one run in the bottom of the eighth against closer [[Goose Gossage]] on RBI singles by [[Fred Lynn]] and Yastrzemski.<ref name=Lyle245>Lyle & Golenbock, p. 245</ref>  But the Yankees would hold off the Red Sox, thanks in part to a heads-up defensive play by right fielder [[Lou Piniella]] with one out in the bottom of the ninth.  With Burleson on first base, [[Jerry Remy]] hit a line drive to Piniella in right field, but Piniella was blinded by the late afternoon sun and could not see the ball. However, he pretended to field the play normally, pounding his glove as though he would easily catch the ball.  This prevented Burleson from advancing to third base.  When [[Jim Rice]] followed with a deep fly to the outfield, Burleson could only move up to third base instead of scoring the tying run.<ref name=milwaukee/><ref name=Lyle245 />

Batting with two out and two men on, Yastrzemski popped out to third baseman [[Graig Nettles]] in foul territory for the game's final out, and New York won the game, 5–4.  Guidry improved his record to 25–3, while Torrez took the loss. Gossage recorded his 27th save.<ref name="LineScore"/>

==Line score==
{{Linescore|Date=October 2, 1978|Location=[[Fenway Park]], [[Boston]] [[Massachusetts]]<ref name="LineScore">{{cite web|url=http://www.baseball-reference.com/boxes/BOS/BOS197810020.shtml |work=[[Baseball-Reference.com]] |date=October 2, 1978 |title=Oct 2, 1978, Yankees at Red Sox Box Score and Play by Play |accessdate=1 March 2009 <!--DASHBot--> |archiveurl=https://web.archive.org/web/20090412052415/http://www.baseball-reference.com/boxes/BOS/BOS197810020.shtml |archivedate=12 April 2009 |deadurl=no |df= }}</ref>
|Road='''New York Yankees'''|RoadAbr=[[New York Yankees|NYY]]
|R1=0|R2=0|R3=0|R4=0|R5=0|R6=0|R7=4|R8=1|R9=0|RR=5|RH=8|RE=0
|Home=Boston Red Sox|HomeAbr=[[Boston Red Sox|BOS]]
|H1=0|H2=1|H3=0|H4=0|H5=0|H6=1|H7=0|H8=2|H9=0|HR=4|HH=11|HE=0
|RSP=|HSP=
|WP=[[Ron Guidry]] (25–3)|LP=[[Mike Torrez]] (16–13)|SV=[[Goose Gossage]] (27)
|RoadHR=[[Bucky Dent]] (5), [[Reggie Jackson]] (27)|HomeHR=[[Carl Yastrzemski]] (17)
|}}

==Box score==
{{col-begin}}
{{col-break}}
{|class="wikitable sortable plainrowheaders" border="2"
!scope="col"|New York<ref name="LineScore"/>
!scope="col"|[[At bat|AB]]
!scope="col"|[[Run (baseball)|R]]
!scope="col"|[[Hit (baseball)|H]]
!scope="col"|[[Run batted in|RBI]]
!scope="col"|[[Base on balls|BB]]
!scope="col"|[[Strikeout|SO]]
!scope="col"|[[Batting average|AVG]]
|-
!scope="row"|{{sortname|Mickey|Rivers}}, [[Center fielder|CF]]
|2
|1
|1
|0
|2
|0
|.265
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Paul|Blair|dab=baseball}}, [[Pinch hitter|PH]]&ndash;[[Center fielder|CF]]
|1
|0
|1
|0
|0
|0
|.176
|-
!scope="row"|{{sortname|Thurman|Munson}}, [[Catcher|C]]
|5
|0
|1
|1
|0
|3
|.297
|-
!scope="row"|{{sortname|Lou|Piniella}}, [[Right fielder|RF]]
|4
|0
|1
|0
|0
|0
|.314
|-
!scope="row"|{{sortname|Reggie|Jackson}}, [[Designated hitter|DH]]
|4
|1
|1
|1
|0
|0
|.274
|-
!scope="row"|{{sortname|Graig|Nettles}}, [[Third baseman|3B]]
|4
|0
|0
|0
|0
|1
|.276
|-
!scope="row"|{{sortname|Chris|Chambliss}}, [[First baseman|1B]]
|4
|1
|1
|0
|0
|0
|.274
|-
!scope="row"|{{sortname|Roy|White}}, [[Left fielder|LF]]
|3
|1
|1
|0
|1
|1
|.269
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Gary|Thomasson}}, [[Left fielder|LF]]
|0
|0
|0
|0
|0
|0
|.233
|-
!scope="row"|{{sortname|Brian|Doyle|dab=baseball}}, [[Second baseman|2B]]
|2
|0
|0
|0
|0
|0
|.192
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Jim|Spencer}}, [[Pinch hitter|PH]]
|1
|0
|0
|0
|0
|0
|.227
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Fred|Stanley|dab=baseball}}, [[Second baseman|2B]]
|1
|0
|0
|0
|0
|0
|.219
|-
!scope="row"|{{sortname|Bucky|Dent}}, [[Shortstop|SS]]
|4
|1
|1
|3
|0
|1
|.243
|- class="sortbottom"
!scope="row"|'''Team totals'''
|35
|5
|8
|5
|3
|6
|.229
|}

{|class="wikitable sortable plainrowheaders" border="2"
!scope="col"|New York<ref name="LineScore"/>
!scope="col"|[[Innings pitched|IP]]
!scope="col"|[[Hits allowed|H]]
!scope="col"|[[Runs (baseball)|R]]
!scope="col"|[[Earned runs|ER]]
!scope="col"|[[Base on balls|BB]]
!scope="col"|[[Strikeout|SO]]
!scope="col"|[[Home runs allowed|HR]]
!scope="col"|[[Earned run average|ERA]]
|-
!scope="row"|{{sortname|Ron|Guidry}}, [[Win (baseball)|W]] (25–3)
|{{frac|6|1|3}}
|6
|2
|2
|1
|5
|1
|1.74
|-
!scope="row"|{{sortname|Goose|Gossage}}, [[Save (baseball)|S]] (27)
|{{frac|2|2|3}}
|5
|2
|2
|1
|2
|0
|2.01
|- class="sortbottom"
!scope="row"|'''Team totals'''
|9
|11
|4
|4
|2
|7
|1
|4.00
|}

{{col-break|gap=1em}}
{|class="wikitable sortable plainrowheaders" border="2"
!Boston<ref name="LineScore"/>
![[At bat|AB]]
![[Run (baseball)|R]]
![[Hit (baseball)|H]]
![[Run batted in|RBI]]
![[Base on balls|BB]]
![[Strikeout|SO]]
![[Batting average|AVG]]
|-
!scope="row"|{{sortname|Rick|Burleson}}, [[Shortstop|SS]]
|4
|1
|1
|0
|1
|1
|.248
|-
!scope="row"|{{sortname|Jerry|Remy}}, [[Second baseman|2B]]
|4
|1
|2
|0
|0
|0
|.278
|-
!scope="row"|{{sortname|Jim|Rice}}, [[Right fielder|RF]]
|5
|0
|1
|1
|0
|1
|.315
|-
!scope="row"|{{sortname|Carl|Yastrzemski}}, [[Left fielder|LF]]
|5
|2
|2
|2
|0
|1
|.277
|-
!scope="row"|{{sortname|Carlton|Fisk}}, [[Catcher|C]]
|3
|0
|1
|0
|1
|0
|.284
|-
!scope="row"|{{sortname|Fred|Lynn}}, [[Center fielder|CF]]
|4
|0
|1
|1
|0
|0
|.298
|-
!scope="row"|{{sortname|Butch|Hobson}}, [[Designated hitter|DH]]
|4
|0
|1
|0
|0
|1
|.250
|-
!scope="row"|{{sortname|George|Scott|dab=first baseman}}, [[First baseman|1B]]
|4
|0
|2
|0
|0
|2
|.233
|-
!scope="row"|{{sortname|Jack|Brohamer}}, [[Third baseman|3B]]
|1
|0
|0
|0
|0
|0
|.234
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Bob|Bailey|dab=baseball}}, [[Pinch hitter|PH]]
|1
|0
|0
|0
|0
|1
|.191
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Frank|Duffy|dab=baseball}}, [[Third baseman|3B]]
|0
|0
|0
|0
|0
|0
|.260
|-
!scope="row"|&nbsp;&nbsp;{{sortname|Dwight|Evans}}, [[Pinch hitter|PH]]
|1
|0
|0
|0
|0
|0
|.247
|- class="sortbottom"
!scope="row"|'''Team totals'''
|36
|4
|11
|4
|2
|7
|.306
|}

{|class="wikitable sortable plainrowheaders" border="2"
!scope="col"|Boston<ref name="LineScore"/>
!scope="col"|[[Innings pitched|IP]]
!scope="col"|[[Hits allowed|H]]
!scope="col"|[[Runs (baseball)|R]]
!scope="col"|[[Earned runs|ER]]
!scope="col"|[[Base on balls|BB]]
!scope="col"|[[Strikeout|SO]]
!scope="col"|[[Home runs allowed|HR]]
!scope="col"|[[Earned run average|ERA]]
|-
!scope="row"|{{sortname|Mike|Torrez}}, [[Loss (baseball)|L]] (16–13)
|{{frac|6|2|3}}
|5
|4
|4
|3
|4
|1
|3.96
|-
!scope="row"|{{sortname|Bob|Stanley|Bob Stanley (baseball player)}}
|{{frac|0|1|3}}
|2
|1
|1
|0
|0
|1
|2.60
|-
!scope="row"|{{sortname|Andy|Hassler}}
|{{frac|1|2|3}}
|1
|0
|0
|0
|2
|0
|3.87
|-
!scope="row"|{{sortname|Dick|Drago}}
|{{frac|0|1|3}}
|0
|0
|0
|0
|0
|0
|3.03
|- class="sortbottom"
!scope="row"|'''Team totals'''
|9
|8
|5
|5
|3
|6
|2
|5.00
|}
{{col-end}}

==Broadcast==

This game was televised regionally by the respective teams' rights holders, [[WSBK-TV]] in Boston and [[WPIX]] in New York City. [[Major League Baseball on ABC|ABC Sports]] picked up the contest for national viewers, thus providing alternate coverage of the game on its [[WABC-TV|New York]] and [[WCVB-TV|Boston]] affiliates. [[Keith Jackson]], [[Howard Cosell]] and [[Don Drysdale]] narrated the action in the ABC booth.

On radio, the [[Major League Baseball on CBS Radio|CBS Radio Network]] offered national coverage of the game, with [[Ernie Harwell]] doing play-by-play and [[Win Elliot]] working as an analyst. Locally in the home markets, [[WINS (AM)|WINS]] in New York City and [[WMEX (AM)|WITS]] in Boston fed the game to the teams' respective radio networks.

In the Red Sox' broadcast booth, [[Dick Stockton]] and [[Ken Harrelson|Ken "Hawk" Harrelson]] worked the television side while [[Ned Martin]] and [[Jim Woods]] were heard on radio. In the Yankees' booth, [[Phil Rizzuto]], [[Bill White (first baseman)|Bill White]] and [[Frank Messer]] alternated play-by-play on both radio and television, and were backed up on radio by [[Fran Healy (baseball)|Fran Healy]].

==Aftermath==
For the third straight year, the Yankees went on to face the [[1978 Kansas City Royals season|Kansas City Royals]] in the [[1978 American League Championship Series]].  The Yankees won the best-of-five series for their third consecutive pennant.  New York defeated the [[1978 Los Angeles Dodgers season|Los Angeles Dodgers]] in the [[1978 World Series|World Series]] to win their second consecutive championship, and 22nd overall.<ref>{{cite news|url=https://news.google.com/newspapers?id=Jg4gAAAAIBAJ&sjid=pmUFAAAAIBAJ&pg=3980,2737541&dq=1978+world+series&hl=en|title=Yanks Take Four Straight To Win 22nd World Series|agency=Associated Press|newspaper=The Lewiston Daily Sun|date=October 18, 1978|accessdate=June 15, 2012|authorlink=Associated Press|page=1}}</ref>

The loss of the Red Sox was seen as a manifestation of the [[Curse of the Bambino]], long thought to be the reason behind all things bad that ever happened to the Red Sox.<ref>{{harvnb|Shaughnessy|2005|pp=7–8}}</ref> Described as a "shocking blast" by the ''[[Sporting News]]'', Dent's home run silenced the Fenway Park crowd.  For the light-hitting Dent, it was just his fifth home run of the 1978 season.<ref name="DentHomeRun">{{harvnb|Shaughnessy|1990|p=146}}</ref> It sealed Dent's reputation among Yankee fans, while inspiring the permanent nickname "Bucky Fucking Dent" in [[New England]].<ref>{{cite news|title=For Boston, ousting rivals would be sweet|last=Graves|first=Gary|newspaper=USA Today|date=October 17, 2003|page=4C}}</ref> Dent, later the manager of the Yankees, was fired during a series in Boston in 1990.<ref>{{cite news|title=Dent Dumped by Yankees|date=June 7, 1990|first=Nick|last=Cafardo|newspaper=The Boston Globe|page=37|url=https://secure.pqarchiver.com/boston/access/61640175.html?FMT=ABS&FMTS=ABS:FT&type=current&date=Jun+7%2C+1990&author=Nick+Cafardo%2C+Globe+Staff&pub=Boston+Globe+%28pre-1997+Fulltext%29&edition=&startpage=37&desc=DENT+DUMPED+BY+YANKEES|accessdate=August 6, 2012}} {{subscription required|date=August 2012}}</ref> Twenty-five years later, in Game 7 of the [[2003 American League Championship Series]], [[Aaron Boone]] received similar treatment by Red Sox fans after he hit the home run in the bottom of the 11th inning that clinched the pennant for the Yankees, but the Yankees would later lose to the [[Florida Marlins]] in the [[2003 World Series|World Series]], which went six games.

Guidry and Rice were considered candidates for the [[Major League Baseball Most Valuable Player Award|AL Most Valuable Player (MVP) Award]] for their strong seasons.<ref name=morningrecord/> Rice was named MVP, with Guidry finishing second in the voting. Guidry won the AL [[Cy Young Award]].<ref name="1978 awards">{{cite web|url=http://www.baseball-reference.com/awards/awards_1978.shtml|title=Baseball Awards Voting for 1978|work=[[Baseball-Reference.com]]|accessdate=December 28, 2009}}</ref> Lemon was named AL Manager of the Year.<ref>{{cite news|url=https://news.google.com/newspapers?id=IGJQAAAAIBAJ&sjid=51gDAAAAIBAJ&pg=4605,2735639&dq=yankees+red+sox&hl=en|title=Another Sweet Twist – Bob Lemon named AL Manager of the Year|newspaper=The Evening Independent|date=October 25, 1978|agency=Associated Press|page=1C}}</ref>

==References==
;Bibliography
*{{cite book|title=Red Sox vs. Yankees: The Great Rivalry|first=Harvey|last=Frommer|first2=Frederic J.|last2=Frommer|publisher=Sports Publishing, LLC|year=2004|isbn=1-58261-767-8}}
*{{cite book |last1=Lyle |first1=Sparky |authorlink1=Sparky Lyle |first2=Peter |last2=Golenbock |authorlink2=Peter Golenbock |title=The Bronx Zoo: The Astonishing Inside Story of the 1978 World Champion New York Yankees |year=1979 |edition= first |publisher= Crown Publishers |location= New York |isbn=0-517-53726-5 |oclc=4664652}}
*{{cite book|last=Shaughnessy|first=Dan|title=The Curse of the Bambino|year=1990|publisher=Dutton|location=New York|isbn=0-525-24887-0|authorlink=Dan Shaughnessy}}
*{{cite book|last=Shaughnessy|first=Dan|title=Reversing the Curse|year=2005|publisher=Houghton Mifflin Company|location=Boston|isbn=0-618-51748-0}}
*{{cite book|title=The rivals: the Boston Red Sox vs. the New York Yankees: an inside history|author=The New York Times|author2=The Boston Globe|location=New York|publisher=St. Martin's Press|year=2004|isbn=0-312-33616-0|edition=1st|authorlink=The New York Times}}

;Inline citations
{{Reflist|30em}}

==Further reading==
*{{cite book|last=Schwartz|first=Jonathan|year=2000|title=A Day of Light and Shadows|location=Pleasantville, New York|publisher=Akadine Press|isbn=1-58579-011-7}}

{{Navboxes|list1=
{{New York Yankees}}
{{Boston Red Sox}}
{{1978 MLB season by team}}
{{Major League Baseball on ABC}}
{{Major League Baseball on CBS Radio}}
{{MLBtiebreaker}}
}}

{{DEFAULTSORT:American League East Tie-Breaker Game, 1978}}
[[Category:Major League Baseball tie-breaker games|1978 American League East Tie-Breaker Game]]
[[Category:New York Yankees|1978 American League East Tie-Breaker Game]]
[[Category:Boston Red Sox|1978 American League East Tie-Breaker Game]]
[[Category:1978 Major League Baseball season|American League East tie-breaker game]]
[[Category:1978 in Massachusetts]]
[[Category:20th century in Boston]]
[[Category:October 1978 sports events]]