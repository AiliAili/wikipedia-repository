{{Infobox journal
| title = International Regional Science Review
| cover = [[File:International Regional Science Review.tif]]
| editor = Alan T. Murray and Tony H. Grubesic
| discipline = [[Regional science]]
| former_names = 
| abbreviation = Int. Reg. Sci. Rev.
| publisher = [[Sage Publications]]
| country =
| frequency = Quarterly
| history = 1975-present
| openaccess = 
| license = 
| impact = 1.000
| impact-year = 2013
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200982
| link1 = http://irx.sagepub.com/content/current
| link1-name = Online access
| link2 = http://irx.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 1701088
| LCCN = 78643713 
| CODEN = 
| ISSN = 0160-0176 
| eISSN = 1552-6925 
}}
The '''''International Regional Science Review''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[regional science]]. The journal's editors-in-chiefs are Alan T. Murray and Tony H. Grubesic ([[Drexel University]]). It was established in 1975 and is currently published by [[Sage Publications]].

== Abstracting and indexing ==
''International Regional Science Review'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.000, ranking it 18 out of 38 journals in the category "Urban Studies",<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Urban Studis |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> 27 out of 55 journals in the category "Planning & Development",<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Planning & Development |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> and 66 out of 98 journals in the category "Environmental Studies".<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Environmental Studies |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200982}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Environmental studies journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1975]]
[[Category:Urban studies and planning journals]]