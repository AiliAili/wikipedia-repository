{{Infobox Airline
|airline        = Buddha Air Pvt. Ltd
|logo           = Buddha Air logo.svg
|logo_size      = 300px
|fleet_size     = 9
|destinations   = 12
|IATA           = U4
|ICAO           = BHA
|callsign       = BUDDHA AIR
|parent         =
|company_slogan = 
|founded        = 1997
|headquarters   = [[Jawalakhel]], [[Nepal]]
|License ceased = [[2011]]
|key_people     =  [[Surendra Bahadur Basnet ]](Company Chairman) <br/>Birendra B. Basnet ([[Managing Director]])
|hubs           = [[Tribhuvan International Airport]]
|secondary_hubs =
|focus_cities   =
|frequent_flyer = [https://www.buddhaair.com/content/royal-club-1.html Royal Club]
|lounge         =
|alliance       =
|subsidiaries   =
|website        = [http://www.buddhaair.com www.buddhaair.com]
}}
[[File:Beech 1900D, Buddha Air JP9932.jpg|thumb|Buddha Air [[Beechcraft 1900]] at [[Tribhuvan International Airport]] (2001)]]
'''Buddha Air Pvt. Ltd''' is an [[airline]] based in [[Jawalakhel]], [[Lalitpur District, Nepal|Lalitpur District]], [[Nepal]], near [[Patan, Nepal|Patan]].<ref>"[http://m.buddhaair.com/ Domestic/International]." Buddha Air. Retrieved on 26 September 2011. "The company headquarters is located at Jawalakhel, Lalitpur"</ref><ref>"[http://www.buddhaair.com/contact/ Contact Information]." Buddha Air. Retrieved on September 25, 2011. "Buddha Air Pvt. Ltd Pulchowk Rd Patan"</ref> It operates domestic as well as international services within Nepal and India, serving mainly large towns and cities in Nepal, linking Kathmandu with ten destinations and Varanasi of India. Since its establishment.<ref>{{cite web|title=My Business: Nepalese airline taking off|url=http://www.bbc.com/news/business-24513257|publisher=BBC|accessdate=21 April 2014|date=15 October 2013}}</ref> Its main base is [[Tribhuvan International Airport]], Kathmandu.<ref name="FI">[[Flight International]] 27 March 2007</ref> It also operates mountain flights from Kathmandu to the [[Everest]] range.<ref name="WA">[http://www.worldairroutes.com/BuddhaYeti.html World Air Routes] retrieved 18 November 2006</ref>

==History==
The airline was established on 23 April 1996 as a Private Limited Company by Surendra Bahadur Basnet, a retired Supreme Court judge and former government minister; and his son Birendra Bahadur Basnet.<ref name="Prof">{{Cite web|url=http://www.buddhaair.com/content/company-profile.html |title=Company Profile |publisher=Buddha Air |accessdate=7 May 2014}}</ref><ref name="Hist">{{Cite web|url=http://www.buddhaair.com/content/history-introduction.html |title=History |publisher=Buddha Air |accessdate=7 May 2014}}</ref> Operations commenced on 11 October 1997 with a sightseeing flight to Mount Everest using a brand new Beechcraft 1900D.<ref name="Hist"/> Within ten years the company had expanded to a fleet of seven 1900Ds.<ref>{{cite journal |author=<!--Staff writer(s); no by-line.--> |title=Seven {{sic|nolink=y|Aircrafts}} |url=http://www.buddhaair.com/downloads/get/yatra-issue-july-2007.html |journal=Buddha Yatra (Buddha Air inflight magazine) |location= |publisher=Buddha Air |date=July 2007 |accessdate=7 May 2014 }}</ref> In 2008 a loan from the [[International Finance Corporation]] allowed the company to expand further by purchasing two [[ATR 42]] aircraft.<ref name="IFC">{{cite press release |last=Seth |first=Minakshi |date=18 July 2012 |title=IFC Loan to Buddha Air to Improve Air Travel Connectivity in Nepal |url=http://ifcext.ifc.org/ifcext/pressroom/IFCPressRoom.nsf/0/ACE322C5FCCEF32185257A3F0036C299 |location=Kathmandu, Nepal |publisher=International Finance Corporation |accessdate=7 May 2014}}</ref> Buddha Air took delivery of its first 70-seat [[ATR 72]] in June 2010.<ref>{{cite press release |author=<!--Staff writer(s); no by-line.--> |title=Bigger Aircraft Better Comfort in Nepalese SKY  |url=http://www.buddhaair.com/press |location= |publisher=Buddha Air |date=21 June 2010 |accessdate=7 May 2014}}</ref> The name of the airline is derived from the Sanskrit word 'Buddha', a title used for the much revered [[Siddharta Gautama]].

==Destinations==
As of April 2014, Buddha Air offers flights to the following domestic and international destinations:<ref>{{Cite web|url=http://www.buddhaair.com/schedule/domestic |title=Domestic Schedule |publisher=Buddha Air |accessdate=7 May 2014}}</ref><ref>{{Cite web|url=http://www.buddhaair.com/schedule/international |title=International Schedule |publisher=Buddha Air |accessdate=7 May 2014}}</ref>

*'''Nepal
**[[Bhadrapur, Mechi|Bhadrapur]] - [[Bhadrapur Airport]]
**[[Bhairahawa]] - [[Gautam Buddha Airport]]
**[[Bharatpur, Nepal|Bharatpur]] - [[Bharatpur Airport]]
**[[Biratnagar]] - [[Biratnagar Airport]]
**[[Dhangadhi]] - [[Dhangadhi Airport]]
**[[Janakpur]] - [[Janakpur Airport]]
**[[Kathmandu]] - [[Tribhuvan International Airport]]<sup>[Base]</sup>
**[[Nepalgunj]] - [[Nepalgunj Airport]]
**[[Pokhara]] - [[Pokhara Airport]]
**[[Pipara Simara]] - [[Simara Airport]]
**[[Tumlingtar]] - [[Tumlingtar Airport]]
*'''India'''
**[[Varanasi]] - [[Varanasi Airport]] 
*'''Bhutan
**[[Paro, Bhutan|Paro]] - [[Paro Airport]] ''Charter''

The airline also offers [[air charter]] flights and daily mountain sightseeing flights.<ref>[http://www.nepaltourismdirectory.com/nepal_travel_destination.php?id=8&did=38&title=Airlines Nepal Tourism Directory] {{webarchive |url=https://web.archive.org/web/20061125100955/http://www.nepaltourismdirectory.com/nepal_travel_destination.php?id=8&did=38&title=Airlines |date=November 25, 2006 }} retrieved 18 November 2006</ref> Buddha Air became the first foreign airline to start flights to [[Paro, Bhutan]], in summer 2010, which was the airline's first international destination.
Buddha Air is said to be Nepal's safest and most reliable air carrier{{Citation needed|date=July 2015}}, with only one incident in its history. Buddha Air receives their aircraft direct from manufactures.{{Citation needed|date=July 2015}} Engineers are highly trained, as are pilots, who are tested in simulators yearly in world leading institutes.{{Citation needed|date=July 2015}}

==Fleet==
[[File:Buddha Air ATR 72-500 pre-delivery (F-WNUB).jpg|thumb|[[ATR 72|ATR 72-500]] of Buddha Air, predelivery at [[Toulouse-Blagnac Airport]] (August 2012)]]
The Buddha Air fleet consists of the following aircraft (as of August 2016):<ref>{{cite journal|title=Global Airline Guide 2016 (Part One)|journal=Airliner World|issue=October 2016|page=22}}</ref>

<center>
{| class="toccolours" border="1" cellpadding="3" style="border-collapse:collapse; text-align: center"
|+ '''Buddha Air fleet'''
|- style="background:lightgrey;"
!rowspan=2|Aircraft
!rowspan=2|In Fleet
!rowspan=2|Orders
!colspan=3|Passengers
!rowspan=2|Notes
|- style="background:lightgrey;"
!<abbr title=Business>C</abbr>
!<abbr title=Economy>Y</abbr>
!Total
|-
|align=left|[[ATR 42|ATR 42-320]]
|3
|0
|0
|47
|47
|
|-
|align=left|[[ATR 72|ATR 72-500]]
|4
|1
|0
|70
|70
|
|-
|align=left|[[Beechcraft 1900|Beechcraft 1900D]]
|2
|0
|0
|18
|18
|
|-

!Total
!9
!1
!colspan=3|
|
|}
</center>

==Awards==
2016 :  Frost & Sullivan Nepal Best Domestic Airlines Award

==Accidents==
On 25 September 2011, [[Buddha Air Flight 103]] crashed near the end of a sightseeing flight of the Mount Everest region. All 19 passengers and crew on board the Beechcraft 1900D died when it crashed near [[Tribhuvan International Airport|Kathmandu's airport]] while attempting to land.<ref name="NY">[http://nycaviation.com/2011/09/mount-everest-tour-plane-crashes-in-nepal/ "Mount Everest Tour Plane Crashes in Nepal."] NYCAviation. 25 September 2011</ref><ref name="BBC">[http://www.bbc.co.uk/news/world-south-asia-15051112 "Nepal tourist plane crashes near Kathmandu killing 19"] BBC News. 25 September 2011</ref>

==References==
{{reflist}}

==External links==
{{Portal|Nepal|Companies|Aviation}}
{{Commons category|Buddha Air}}
*{{official website|http://www.buddhaair.com/}}
*Thaha Khabar (http://thahakhabar.com/news/5582)

<!--Navigation boxes--><br />
{{Airlines of Nepal}}

[[Category:Airlines of Nepal]]
[[Category:Airlines established in 1997]]
[[Category:Lalitpur District, Nepal]]