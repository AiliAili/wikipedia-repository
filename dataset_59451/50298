{{Refimprove|date=March 2014}}
''For Al Ruwais in Qatar, see [[Ar Ru'ays]].''

{{Infobox settlement|
official_name = Al Ruwais|
pushpin_map = United Arab Emirates|
pushpin_map_caption    =Location in United Arab Emirates
|coordinates            = {{coord|24|6|12|N|52|35|1|E|display=inline,title}}
|subdivision_type       = Country
|subdivision_name       = [[Image:Flag of the United Arab Emirates.svg|25px]] [[United Arab Emirates]]
|subdivision_type1      = [[Emirates of the United Arab Emirates|Emirate]]
|subdivision_name1      = [[Emirate of Abu Dhabi]]
|subdivision_type2      = [[Department of Municipal Affairs (Abu Dhabi)|Municipal Region]]
|subdivision_name2      = [[Al Gharbia]]
}}

'''Al Ruwais''' is a town located some 240 kilometers west of [[Abu Dhabi]] city in the [[Al Gharbia|Western Region]] of [[Abu Dhabi (emirate)|Abu Dhabi Emirate]]. The Ruwais industrial and housing complex has been developed by [[ADNOC]] as a major contributor to the national economy and represents a series of multimillion-dollar investments by the company. Once a small fishing headland from which a handful of people scratched a seasonal living, Ruwais today is one of the most modern industrial complexes in the [[Middle East]].

In the 1970s, plans were laid to transform a remote [[desert]] site into a self-contained industrial town, geared to fulfilling the downstream requirements of Abu Dhabi's booming oil and gas industry. Centered on Takreer's Ruwais Refinery, the complex was officially inaugurated in 1982 by [[Zayed bin Sultan Al Nahyan|Sheikh Zayed bin Sultan Al Nahyan]], the late President of the [[UAE]] and Ruler of Abu Dhabi, and the visionary behind Abu Dhabi's remarkable development and prosperity. In addition to the original {{convert|120000|oilbbl|m3}}-per-day refinery, which was expanded in 1985 with the commissioning of a {{convert|27000|oilbbl/d|m3/d|abbr=on}} hydro cracker complex, major facilities at Ruwais include a natural gas liquids fractionation plant operated by Abu Dhabi Gas Industries Ltd ([[GASCO]]), a fertilizer plant run by Ruwais Fertilizer Industries ([[Abu_Dhabi_National_Oil_Company#Ruwais_Fertilizer_Industry_.28FERTIL.29|FERTIL]]), a Petrochemical Complex by Abu Dhabi National Polymers Company ([[Borouge]]), a marine terminal and a sulfur handling terminal.

Ruwais has also been developed into a model 'new town' with an evolving population. The Ruwais Housing Complex, covering an area of six square kilometers, is located 10 kilometers away from the industrial plants. The complex has its own shops, schools, banks, [[mosques]], clinic and hospital, and a wide range of sporting and leisure amenities including a beach club and an 'in-house' TV station.  Although originally designed to house the workers who support the oil and gas industry, Ruwais has grown to include other government workers of nearby projects, such as [[Barakah nuclear power plant|Barakah Nuclear Power Plant]]. 


==Climate==
The climate of Ruwais is similar to Abu Dhabi, which is a [[hot desert climate]] ([[Köppen climate classification]] ''BWh''). However, averages temperatures are typically 8 degrees C higher in the summer months.<ref>{{cite web| url=https://www.worldweatheronline.com/ruwais-weather-averages/abu-dhabi/ae.aspx | title=Ruwais, Abu Dhabi Monthly Climate Average, United Arab Emirates}}</ref>  The month of July is generally the hottest month with maximum temperatures averaging above {{convert|47|°C|0|abbr=on}}. Sandstorms occur intermittently, in some cases reducing visibility to a few meters.<ref>{{cite web| url=http://www.thenational.ae/news/uae-news/sandstorms-sweep-across-region | title=Sandstorms sweep across region | first=Gregor | last=McClenaghan | date=13 February 2009 | newspaper=[[The National (Abu Dhabi)|The National]] | location=Abu Dhabi, UAE }}</ref>

On average, January is the coolest month in the year. Since the Tropic of Cancer passes through the emirate, the southern part falls within the [[Tropics]]. However, despite the coolest month having a {{convert|22.0|C|F}} average, its climate is far too dry to be classed as tropical.

{{Weather box
|location = Abu Dhabi
|metric first = Yes
|single line = Yes
|Jan high C = 26
|Feb high C = 28
|Mar high C = 32
|Apr high C = 36
|May high C = 43
|Jun high C = 45
|Jul high C = 47
|Aug high C = 46
|Sep high C = 44
|Oct high C = 38
|Nov high C = 33
|Dec high C = 30
|Jan mean C = 22
|Feb mean C = 24
|Mar mean C = 29
|Apr mean C = 33
|May mean C = 39
|Jun mean C = 40
|Jul mean C = 42
|Aug mean C = 42
|Sep mean C = 39
|Oct mean C = 34
|Nov mean C = 29
|Dec mean C = 26
|Jan low C = 18
|Feb low C = 17
|Mar low C = 23
|Apr low C = 27
|May low C = 33
|Jun low C = 33
|Jul low C = 35
|Aug low C = 35
|Sep low C = 32
|Oct low C = 26
|Nov low C = 21
|Dec low C = 18
|Jan precipitation mm = 7
|Feb precipitation mm = 21
|Mar precipitation mm = 14
|Apr precipitation mm = 6
|May precipitation mm = 1
|Jun precipitation mm = 0
|Jul precipitation mm = 0
|Aug precipitation mm = 1
|Sep precipitation mm = 0
|Oct precipitation mm = 0
|Nov precipitation mm = 0
|Dec precipitation mm = 5
|unit precipitation days = 0.2&nbsp;mm
|Jan precipitation days = 3
|Feb precipitation days = 5
|Mar precipitation days = 3
|Apr precipitation days = 3
|May precipitation days = 0
|Jun precipitation days = 0
|Jul precipitation days = 0
|Aug precipitation days = 0
|Sep precipitation days = 0
|Oct precipitation days = 0
|Nov precipitation days = 0
|Dec precipitation days = 2
|Jan humidity = 60
|Feb humidity = 54
|Mar humidity = 46
|Apr humidity = 41
|May humidity = 28
|Jun humidity = 28
|Jul humidity = 28
|Aug humidity = 28
|Sep humidity = 40
|Oct humidity = 44
|Nov humidity = 57
|Dec humidity = 61
|Jan sun= 246
|Feb sun= 232
|Mar sun= 251
|Apr sun= 280
|May sun= 342
|Jun sun= 336
|Jul sun= 314
|Aug sun= 307
|Sep sun= 302
|Oct sun= 304
|Nov sun= 286
|Dec sun= 257
|source 1 = NOAA (1971–1991)<ref name="NOAA">{{cite web|title=Climate Normals for Ruwais|url=ftp://ftp.atdd.noaa.gov/pub/GCOS/WMO-Normals/RA-II/UE/41216.TXT|publisher=[[National Oceanic and Atmospheric Administration]]|accessdate=2016-10-12}}</ref>
|date=October 2016
}}

==Ruwais Housing Complex==
[[Image:Ruwais(UAE) with blue water towers.JPG|thumb|Watertowers of Ruwais Housing Complex]]
Ruwais Housing Complex (RHC) — which has its own shopping centers, schools, mosques, medical services, sports and recreation centers, banking and postal services, and a cable TV and video station — covers an area of six square kilometers. It lies between Ruwais Industrial Complex and Jebel Dhana, just off the main highway running westwards into [[Saudi Arabia]]. RHC was built in two phases between 1979 and 1984; in line with ADNOC's policy of ensuring that employees and their families have the very best of social and welfare facilities, all efforts have been made to make it a fully integrated community.

The complex has 1,357 residential units, ranging from one-bedroom apartments to four-bedroom, double-story 'executive' [[villa]]s. All have modern furnishing and fittings and are set amidst landscaped grounds. A large area of RHC has been planted with a variety of trees, shrubs, flower beds and lawns. It has a number of recreation parks and a green belt around the perimeter covers 140 hectares which is planted with more than 22,000 trees. To ensure a regular supply of plants all year round, the complex has its own nursery to propagate seeding. This has up to 250,000 plants in stock.

Although families who live in RHC are some 250 kilometers away from Abu Dhabi, a limited range of consumer goods is available. The complex has three commercial centers with more than 40 shops and two markets selling everything from foodstuffs to electrical
goods, household items and basic clothing.

The educational needs of Ruwrfais children, from nursery through to secondary level, are met by a total of 10 schools. Seven are managed by the Ministry of Education and three are privately owned, offering British or Indian systems. The schools, private and government, are equipped with modern classrooms and other facilities, such as science laboratories and libraries.

RHC has two recreation centers, one exclusively for women. The other is currently undergoing major renovation but will be open to all.  Sports facilities include a swimming pool open to men, gymnasium, theatre, bowling alley, general activity halls, tennis courts, library, restaurants and cafeterias. Cultural events, often involving troupes and performers from overseas, are staged regularly at the recreation centers. Residents also have access to a beach club at Jebel Dhanna, which in addition to water sports facilities has a TV and Games hall. There is also a local TV and video station which operates on a cable network.

The complex has all the ancillary services required of a modern town, including a well-maintained road network, street lighting in all areas, and a sewage system which includes a treatment plant for recycling sewage water for irrigation purposes. Electricity and desalinated water supplies are provided directly from the refinery.

Ruwais was affected by a wind and sand storm in mid-November 2015, which uprooted about 2000 trees.

===Divisions===
Ruwais Town is divided into 5 parts:

====Ruwais 1====
Ruwais 1 was inaugurated in 1982, and mostly consists of villas and a few four-story apartment buildings. The Ruwais Recreation Center, as well as the iconic "Ruwais Duck Pond" (which is currently closed due to works) are located here. Ruwais 1 is nearer to the Refinery, and also is the place where many businesses are located. From supermarkets to sport shops; banks to travel agencies; groceries, saloons, restaurants, furniture, bakeries, stationery shops, clothing shops, and gift shops, you can find anything in Ruwais 1. The villas are primarily based in 1970s and 1980s villas from the [[United States of America]]. It is the oldest area in Ruwais, and is located in the north. The Main Ruwais Bus Stop and a heli-pad along with schools are also located here. Several parks give the residents some enjoyment. The Ruwais Housing Complex Clinic was located here, near the Ruwais Ladies Club. After the opening of the Ruwais Hospital, the clinic was moved to Ruwais 3. The Bank Building, called because several banks and travel agencies are located here.

The Emirates General Market, a part of the EMKE Group, is located here, and is where most South Asians shop. The Abu Dhabi Market is located in the western side, and it is mostly used by Europeans and East Asians. The Ruwais Automatic Bakeries is commonly used by labourers and nearby residents.

The Senior Campus of International School of SABIS School and the two junior schools of Asian International Pvt. School are located here. Kindergarten campus of Ruwais Private School and Borouge Training Centers are also found here. Buses are shuttled around every 1 and half hours, and common destinations are the [[Ruwais Mall]], [[Ruwais Hospital]] and markets.

====Ruwais 2====
Built during 2000–2002, it has hundreds of four-floored apartments. It is known for its large park, the Ruwais 2 Park. It provides a popular resting spot, and many families have dinner here during the cool nights. It has many rides to attract children. Ruwais 2 mostly consists of residencies, and has one small grocery store and a fast food restaurant. The senior campus of Asian International School is located here.
Extension phase 01:  located to the south of Ruwais 2, and it is divided by an avenue road. The Higher College of Technology, Ruwais is located here. It consists fully of apartments, while new villas for local residents are also built here.
Extension Phase 02:  consists mainly of 5, 7 and newly built 11 floor buildings. The location is close to Glenelg School Of Abu Dhabi, Ruwais. A new male health center has been built, right off next to the park. It provides a number of facilities, such as an outdoor football artificial turf, four multipurpose outdoor courts, a swimming pool, an indoor jogging track, table tennis and billiards rooms, and an indoor multipurpose court. It also consists of a newly built gym. Two ADNOC Oasis 365 are built, one in building 130 and one in 143. These shops provide groceries to travelers and locals.

====Ruwais 3====
Ruwais 3 consists of high-rise flats of 12- and 15-floor buildings. It is located opposite to Ruwais 4. All the latest modern technological advancements are installed within the buildings.

====Ruwais 4====
Ruwais 4 is at the moment in building phase and consists 693 new Villas.

==Ruwais Mall==
On January 29, 2014, the first ever mall in Ruwais was inaugurated. It consists of a department store and a grocery shop. The branch of these shops is Lulu Hypermarket. The mall has two well known ice cream shops, [[Cold Stone Creamery]] and London Dairy. It has two well-known fast food restaurants - [[KFC]] and [[Pizza Hut]]. For children, there is another attraction - Funville. Near Funville lies Cine Royal, the mall's cinema.  3D cinemas show the latest releases. Two shops for sports franchises have been built, Athletic's Co. and Sun and Sand Sports Co. These shops sell products from worldwide companies such as [[Nike, Inc.|Nike]], [[Adidas]], [[Reebok]], [[Nike, Inc.|Nike]], [[Dunlop Sport|Dunlop]], and [[Wilson Sporting Goods|Wilson]]. An Indian restaurant, Royal Tandoor, is also available. Starbucks Coffee has recently opened which is located outside the main entrance.

Shops:
*Little Thinker
*Malabar Gold and Diamonds
*[[The Children's Place]]
*[[Cold Stone Creamery]]
*[[Call It Spring]]
*[[Splash (Fashion)|Splash]]
*[[Lulu Hypermarket|LuLu]]

==RHC Hospital and Clinic==

A purpose-built local, modern general hospital opened in 2012 to cater for medical, clinical and dental needs of residents. The hospital is administered by the Medical Services Division of the Administration Directorate. Ruwais Hospital is a 36-bed acute-care facility providing the full range of general and emergency medical services, including [[cardiology]],  [[maternity]], [[pediatrics]], [[dentistry]] and [[ophthalmology]], on both an outpatient and inpatient basis. The emergency department, staffed by highly skilled medical officers and well-trained nurses, is open 24 hours a day and operates a round-the-clock ambulance service, not only for ADNOC employees and their families but the whole of the immediate neighborhood. Ambulance crews are on hand, for example, to attend LO road accidents along the highway between Tarif and Sila. The RHC Clinic is now fully integrated with Ruwais Hospital, and provides a comprehensive health-care programme to all residents of the housing complex. It offers a wide range of general practitioner and specialist services, including nursing and child welfare, immunization and vaccination,
school health, dental and physiotherapy services. Ruwais Housing Complex is managed by Ruwais Housing Division, part of the ADNOC Administration Director. For specialist medical services, many residents are often obliged to travel the 240&nbsp;km for an appointment with other health professionals.

==Inside Ruwais==
Given that Ruwais is a smaller city, it enjoys a huge population of workers and employees, who enjoy the peacefulness of the town.{{citation needed|date=December 2016}} Projects are always on the rise and work never ends but when their shifts are over the Ruwaisians never tire of watching the sunset and smoking shisha at the beach rest area.

Although the city is the hub of ADNOC refineries, the pollution is typically much less than in neighboring Abu Dhabi.  This depends on prevailing winds, which tend to push any airborne particulate offshore.  On a typical summer day, pollutant measurements taken (in micrograms per cubic meter) have an undetectable baseline in Ruwais, but spike as high as 165 μg/m³.<ref>{{cite web| url=http://aqicn.org/city/uae/ruwais/ | title=Ruwais Air Pollution AQI as of Monday, August 5, 2016}}</ref>  That same day, baseline levels at Kalifah High School in Abu Dhabi were 81 μg/m³ and spiked as high as 612 μg/m³.<ref>{{cite web| url=http://aqicn.org/city/uae/abu-dhabi-city/kalifah-high-school/ | title=Kalifa High School, Abu Dhabi Air Pollution AQI as of Monday, August 5, 2016}}</ref>  (In comparison to a major US city,  lower Manhatten in New York City has a baseline of approximately 25 μg/m³ and spike as high as 100 μg/m³.<ref>{{cite web| url=http://aqicn.org/city/newyork/ | title=NY, New York Air Pollution AQI as of Tuesday, March 21, 2017 }}</ref>)   

For relaxing, the Ruwaisians like going to the beach, or hanging out at the recreation center which consists of a bowling alley, swimming pool, cinema, theater, library, food corner, tennis area, restaurant, hall and other facilities. Ruwais 2 park is a very common place to meet up with people. It is situated near the two tennis courts and football court. People come there to play, hang out, or walk.

==Schools==
Education in Ruwais consists of 2 main types of schools, those are Public and Private School, including 1 university.

*The Main Private Schools are:
 
1. Adnoc Schools (formerly known as The Glenelg School of Abu Dhabi)

2. The International School of Choueifat (SABIS)

3. The Asian International School (AIS)

*The Main Public Schools are:

1. Al Abbas Bin Abdul Muttalib Primary And Secondary School for Boys (AABAMPSSB)

2. Amrah Bint Abd Al Rahman Secondary School for Girls (ABAARSSG)

The 1 College is:

1. The Higher Colleges of Technologyfor Girls and Boys (HCTGB)


==Facilities in Ruwais Housing Complex==

Main Facilities in Ruwais includes:
* Recreation Center
* Ruwais Hospital
* Sport Centers (Men and Women Have independent facilities)
* Outdoor/Indoor Swimming Pools
* Outdoor Multi sports courts 
* Outdoor Play grounds
* Parks and Gardens
* ADNOC Beach that contains a BBQ area
* Event Center

Other private Facilities and Businesses in the area include:

* Commercial Banks
* Convenience stores
* Cafeteria
* Vihecle Services Center
* Car Rental
* Etisalat
* Travel Agencies
* Fish Market
* Money Exchange Centers

==January 10, 2017 Fire==
On January 10, 2017 at approximately 6:00 pm, a large fire broke out a Takreer refinery.  No casualties were reported.  ADNOC closed the newer section of the refinery, which doubled its capacity when it started operating in 2015. 

==References==
{{reflist}}

==External links==
*[http://ruwais.info Ruwais Housing Complex] - News and ads for the Ruwais community
*[http://www.dailymail.co.uk/wires/reuters/article-4123668/ADNOC-shuts-half-capacity-Ruwais-refinery-fire-sources.html Takreer Fire of January 10, 2017]


{{Abu Dhabi}}

[[Category:Populated places in Abu Dhabi (emirate)]]