{{Infobox football match
|                   title = 2011 All-Ireland Football Semi-Final
|                   image =
|                   event = [[2011 All-Ireland Senior Football Championship]]
|                   team1 = [[Donegal GAA|Donegal]]
|        team1association = [[File:Donegal colours.jpg|40px]]
|              team1score = 0-06
|                   team2 = [[Dublin GAA|Dublin]]
|        team2association = [[File:Dublin colours.PNG|40px]]
|              team2score = 0-08
|                 details =
|                    date = 28 August 2011
|                 stadium = [[Croke Park]]
|                    city = [[Dublin]]
|      man_of_the_match1a = 
|                 referee = [[Maurice Deegan]] ([[Laois GAA|Laois]])
|                 weather = 
|              attendance = 81,436<ref name=rte_as_it_happened/>
}}
The '''Donegal vs Dublin''' [[Gaelic football|football]] match that took place on 28 August 2011 at [[Croke Park]] in [[Dublin]], Ireland, was the second semi-final match of the [[2011 All-Ireland Senior Football Championship]]. Both teams reached the stage with an undefeated record in the competition. The game was administered by [[Laois GAA|Laois]] officials led by Stradbally referee [[Maurice Deegan]]. The result was a scoreline unprecedented in recent Championship history; [[Donegal GAA|Donegal]] led 0–04 - 0-02 at half time, Dublin won the second half 0–06 - 0-02 and the game by a scoreline of 0–08 - 0-06. The match was played in front of a sell-out crowd.

The number of points shared was the lowest between the teams in Championship history. The first half produced one of the lowest scorelines in Chamopionship history and the final scoreline had not been seen at this level of the sport since the 1950s, an era when games lasted ten minutes less. This was also the first Championship loss of [[Jim McGuinness]]'s successful reign as Donegal manager, and one of only three - the other two only coming in 2013. It became a defining game in the evolution of the then revolutionary tactic that was to become known as [[The System (Gaelic football)|The System]].

==Background==
Neither side had won an All-Ireland title since the 1990s. Neither side had even appeared in an All-Ireland final in all that time. Dublin had lost their previous four All-Ireland semi-finals.<ref name=rte_as_it_happened/> Former [[Kerry GAA|Kerry]] footballer [[Pat Spillane]], in his capacity as an analyst for [[RTÉ Television]], tipped an easy win for Dublin.<ref name=rte_as_it_happened/> A heavy rain shower fell ahead of the match.<ref name=rte_as_it_happened>{{cite news|first=Rory|last=Houston|url=http://www.rte.ie/sport/gaa/football/2011/0828/284053-gaatracker/|title=As it Happened: Dublin 0-08 Donegal 0-06|work=RTÉ Sport|publisher=Raidió Teilifís Éireann|date=28 August 2011|accessdate=28 August 2011|archiveurl=https://web.archive.org/web/20150315221525/http://www.rte.ie/sport/gaa/football/2011/0828/284053-gaatracker/|archivedate=15 March 2015}}</ref><ref name=score_as_it_happened/>

==Match==

===First half===
The first significant action was a free won by Dublin's [[Barry Cahill (Gaelic footballer)|Barry Cahill]] in the second minute. Cahill called forward his goalkeeper [[Stephen Cluxton]] to take a shot. Cluxton's attempt drifted to the left and wide. In the sixth minute Donegal's [[Christy Toye]] was hauled down by two Dublin players. [[Michael Murphy (Gaelic footballer)|Michael Murphy]] sent the resulting free wide. In the seventh minute Dublin's [[Alan Brogan]] shot spectacularly wide. A high challenge from Dublin's [[James McCarthy (Gaelic footballer)|James McCarthy]] led to a yellow card in the eight minute. [[Colm McFadden]] scored the resulting free from inside the 45-metre line to give the opening point of the match to Donegal. In the tenth minute Dublin's [[Bernard Brogan (junior)|Bernard Brogan]] ran through on the Donegal goal but his shot past [[Paul Durcan (Gaelic footballer)|Paul Durcan]] went to the left and wide. He made up for this two minutes later when he equalised by putting over Dublin's opening point from a free. The first 14 minutes of the match featured 12 frees. In the twenty-first minute Dublin's Bernard Brogan again attempted to score a point but his effort went to the right and wide. In the game's twenty-fourth minute [[Ryan Bradley (Gaelic footballer)|Ryan Bradley]] restored Donegal's lead, evading the efforts of most of Dublin's players to send the ball between the posts and score the first point from play. One minute later, Bernard Brogan sent over a free to bring the game level again. In the twenty-ninth minute [[Kevin Cassidy]] sent Donegal into the lead again with a point from play via the outside of his boot. Colm McFadden added another point from play a minute later to send Donegal into a two-point lead for the first time. One minute after that, Alan Brogan sent another effort to the right and wide. [[Diarmuid Connolly]] and Cluxton added to Dublin's wide tally just before the break. Dublin did not score from play in the first half.<ref name=rte_as_it_happened/><ref name=score_as_it_happened/>

===Second half===
Donegal opened the second half by adding to their lead. Colm McFadden's shot from play went over the bar to give him his third point of the game. More Dublin wides followed until their goalkeeper Stephen Cluxton stepped up to send a free over the Donegal bar and score his first point of the game. McFadden duly responded by sending another free over Cluxton's bar and Donegal opened up a three-point lead on a scoreline of 0–06 - 0-03. In the forty-ninth minute Dublin's [[Cian O'Sullivan]] showed his frustration by lashing out at [[Rory Kavanagh]], sending him flat onto the Croke Park turf. O'Sullivan picked up a yellow card for his trouble. In the fifty-second minute Bernard Brogan sent over a simple free to reduce the gap between the sides, with Cluxton sending over a '45 soon afterwards. In the fifty-seventh minute Diarmaid Connolly won a free close to the Donegal goal but, before it was taken, Connolly lost his cool and raised a fist to the face of a Donegal player. The referee [[Maurice Deegan]] consulted his lineman Rory Hickey before showing Connolly the red card and changing his decision to a hop-ball which Donegal cleared. With ten minutes left [[Kevin McManamon]] sent over Dublin's first point from play, to bring the game level. With eight minutes left [[Bryan Cullen]] gave Dublin the lead with his team's second point from play. Five minutes later Bernard Brogan sent a free over [[Paul Durcan (Gaelic footballer)|Paul Durcan]]'s bar to give Dublin a two-point lead. In stoppage time Durcan intercepted McManamon's pass to [[Michael Darragh MacAuley]] to prevent what looked a certain Dublin goal. Then Cluxton sent another free wide.<ref name=rte_as_it_happened/><ref name=score_as_it_happened>{{cite news|url=http://www.thescore.ie/live-dublin-v-donegal-212012-Aug2011/|title=Dublin 0-08 Donegal 0-06: As it happened|work=The Score|date=28 August 2011|accessdate=28 August 2011|archiveurl=https://web.archive.org/web/20150315221609/http://www.the42.ie/live-dublin-v-donegal-212012-Aug2011/|archivedate=15 March 2015}}</ref>

===Details===
{{football box
|date = 28 August 2011
|time = 3:30 pm [[Western European Summer Time|IST]]
|team1 = [[Donegal GAA|Donegal]] <br/> [[File:Donegal colours.jpg|40px]]
|score = 0–06 – 0–08
|report = [http://www.rte.ie/sport/gaa/football/2011/0828/284073-donegal_dublin/ Report]
|team2 = [[Dublin GAA|Dublin]] <br/> [[File:Dublin colours.PNG|40px]]
|goals1 = 
|goals2 = 
|stadium = [[Croke Park]], [[Dublin]]
|attendance = 81,436
|referee = [[Maurice Deegan]] ([[Laois GAA|Laois]])
}}
{| width=92% |
|-
|{{Football kit
| pattern_la  =_DonegalGAA2011
 | pattern_b  = _DonegalGAA2011
 | pattern_ra =_DonegalGAA2011
 | leftarm    = 008000 
 | body       = FFD700 
 | rightarm   = 008000 
 | shorts     = 008000
 | socks      = 008000 
 | title      = Donegal
}}
|{{Football kit
 |pattern_la = 
 |pattern_b  = _navyshoulders 
 |pattern_ra = 
 |leftarm    = 000080 
 |body       = 87CEEB 
 |rightarm   = 000080 
 |shorts     = 000080 
 |socks      = 87CEEB 
 |title      = Dublin
}}
|}

==Reactions==
The outcome confounded experts across the sport. Pat Spillane, uncertain what to make of proceedings after witnessing the first half, famously branded Donegal's style of play "Shi'ite football" live in front of the nation during his attempted half-time analysis.<ref>{{cite news|url=http://www.donegaldemocrat.ie/sport/local-sport/beware-of-praise-from-outside-conaghan-1-3971612|title=Beware of praise from outside – Conaghan|newspaper=Donegal Democrat|publisher=Johnston Press|date=21 June 2012|accessdate=21 June 2012|archiveurl=https://web.archive.org/web/20150315221724/http://www.donegaldemocrat.ie/sport/soccer/beware-of-praise-from-outside-conaghan-1-3971612|archivedate=15 March 2015}}</ref> This was also the origin of Spillane's infamous calling for McGuinness to be sent to [[The Hague]] and tried there for "crimes against football", a comment which would return to haunt him on many occasions for many years afterwards. His colleague, the former [[Meath GAA|Meath]] footballer [[Colm O'Rourke]], speaking on television after the match, called it "the game from hell".<ref name=day_shook_to_core/> Booing rang out around the stadium from Dublin fans, impatient at their team's lethargic manner of playing.<ref name=day_shook_to_core/> Those who followed [[Association football|the English game]] were heard to wryly, yet accurately, observe with the score at 0–01 - 0-01 after twenty minutes of play that [[Arsenal F.C.|Arsenal]] and [[Manchester United F.C.|Manchester United]] were serving up scores at a quicker rate with the end result in that match being 8-2 (a total of ten, as the only means by which to score in the English game is via the opening between the goalposts).<ref name=day_shook_to_core/>

Vincent Hogan would later use the match as an example to brand McGuinness "a leader of sheep" in a famous newspaper article, which backfired on him when he mocked what Donegal would offer in the 2012 season - they won the All-Ireland title that year.<ref>{{cite news|url=http://www.independent.ie/opinion/columnists/vincent-hogan/vincent-hogan-mcguinness-a-leader-of-sheep-2940524.html|title=McGuinness a leader of sheep|newspaper=Irish Independent|publisher=Independent News & Media|date=21 November 2011|accessdate=21 November 2011}}</ref><ref>{{cite news|first1=Alan|last1=Foley|first2=Tom|last2=Comack|url=http://www.donegaldemocrat.ie/sport/local-sport/sheep-jibe-was-not-welcomed-1-3274387|title='Sheep' jibe was not welcomed|newspaper=Donegal Democrat|publisher=Johnston Press|date=24 November 2011|accessdate=24 November 2011|archiveurl=https://web.archive.org/web/20150315221815/http://www.donegaldemocrat.ie/sport/soccer/sheep-jibe-was-not-welcomed-1-3274387|archivedate=15 March 2015}}</ref> McGuinness later described the 2011 Championship meeting with Dublin as "a game that came too soon" for his team.<ref name=day_shook_to_core/>

==Aftermath==
The teams would go on to become the dominant sides in their sport that decade, hoovering up a sequence of All-Ireland titles between them. When they met again at the same stage of the [[2014 All-Ireland Senior Football Championship]], it was possible for commentators to describe 28 August 2011 as "the day that shook football's landscape to the core" and the match as "one of the landmark games" in the history of its sport.<ref name=day_shook_to_core>{{cite news|first=Colm|last=Keys|url=http://www.independent.ie/sport/gaelic-games/gaelic-football/dublindonegal-2011-semifinal-the-day-that-shook-footballs-landscape-to-the-core-30539895.html|title=Dublin-Donegal 2011 semi-final: The day that shook football's landscape to the core: The 2011 All-Ireland semi-final clash remains  one of the landmark games in Gaelic football|newspaper=Irish Independent|publisher=Independent News & Media|date=27 August 2014|accessdate=27 August 2014}}</ref>

==See also==
{{portal|Gaelic games|Ireland|2010s}}
*[[All-Ireland Senior Football Championship records and statistics]]
*[[1992 All-Ireland Senior Football Championship Final]]

==References==
{{reflist}}

{{Donegal GAA matches}}
{{Dublin GAA matches}}

[[Category:Donegal GAA matches]]
[[Category:Dublin GAA matches]]
[[Category:Gaelic football]]