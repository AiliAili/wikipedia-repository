<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=P.V.4
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Floatplane]] 
 | national origin=[[United Kingdom]]
 | manufacturer=[[Port Victoria Marine Experimental Aircraft Depot]]
 | designer=
 | first flight=1917
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Port Victoria P.V.4''' was a prototype two-seat reconnaissance [[floatplane]] of the [[First World War]].  The P.V.4 was a small single engined [[pusher configuration|pusher]] [[biplane]]. It was unsuccessful as the engine it was designed around was a failure and alternatives were unsuitable, only one aircraft being built.

==Design and development==

In early 1916, the [[Port Victoria Marine Experimental Aircraft Depot]] designed a two-seat [[pusher configuration]] landplane [[fighter aircraft]], (possibly designated the P.V.3). Although this was not built, Port Victoria was ordered to build a [[floatplane]] derivative for reconnaissance operations, being required to carry a [[Lewis gun]] and radio and to have an endurance of eight hours. The resultant aircraft, the Port Victoria P.V.4, had [[sesquiplane]] wings and a small streamlined nacelle for the two crew, which was attached to the upper wing. It was to be powered by a 150&nbsp;hp (112&nbsp;kW) [[Smith Static]] [[radial engine]], an experimental engine which, while light, promised excellent fuel economy.<ref name="Mason Fighter p82">Mason 1992, p.82.</ref><ref name="Collyer p50">Collyer 1991, p.50.</ref>

While the airframe of the prototype was completed during the autumn of 1916, the promised engine never appeared. When this became apparent, it became clear that possible alternatives such as the [[Hispano-Suiza 8]] or [[Rolls-Royce Falcon]] could not be installed in the P.V.4, and eventually a 110&nbsp;hp (82&nbsp;kW) [[Clerget]] [[rotary engine]] was fitted, allowing the P.V.4 to undergo testing in June 1917.  The Clerget was longer than the Smith Static, however, which meant that the aircraft could not be rigged to be stable both at full power and with power off, with longitudinal control being lost at below 63&nbsp;mph (101&nbsp;km/h). As pusher aircraft were now considered obsolete, no further development took place.<ref name="Collyer p50"/><ref name="Bruce British p394">Bruce 1957, p. 394.</ref>

==Specifications==
{{Aircraft specs
|ref=British Aeroplanes 1914–18<ref name="Bruce British p394"/>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=2
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=32
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=220
|wing area note=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=2400
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Clerget]] 9Z
|eng1 type=nine-cylinder [[rotary engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=110<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=70
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=

|more performance=
<!--
        Armament
-->

|guns= 1× [[.303 British|.303 in]] [[Lewis gun]]
|bombs= 
|rockets= 
|missiles=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*Bruce, J.M. ''British Aeroplanes 1914–18''. London:Putnam, 1957.
*Collyer, David. "Babies Kittens and Griffons". ''[[Air Enthusiast]]'', Number 43, 1991. Stamford, UK:Key Publishing. {{ISSN|0143-5450}}. pp.&nbsp;50–55.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland:Naval Institute Press, 1992. ISBN 1-55750-082-7.
{{refend}}

==External links==
*[http://www.airwar.ru/enc/fww1/pv4.html Port Victoria PV.4] (in Russian)

{{Port Victoria Aircraft}}

[[Category:Floatplanes]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Single-engined pusher aircraft]]
[[Category:Rotary-engined aircraft]]
[[Category:Biplanes]]