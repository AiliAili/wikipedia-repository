{{featured article}}
{{Use mdy dates|date=October 2011}}
{{Infobox Officeholder
|name         = Louie Nunn
|image        = Louie-Nunn.jpg
|order        = 52nd [[Governor of Kentucky]]
|lieutenant   = [[Wendell H. Ford]]
|term_start   = December 12, 1967
|term_end     = December 7, 1971
|predecessor  = [[Edward T. Breathitt]]
|successor    = [[Wendell H. Ford]]
|birth_name   = Louie Broady Nunn
|birth_date   = {{birth date|1924|3|8}}
|birth_place  = [[Park, Kentucky|Park]], [[Kentucky]], [[United States|U.S.]]
|death_date   = {{death date and age|2004|1|29|1924|3|8}}
|death_place  = [[Versailles, Kentucky|Versailles]], [[Kentucky]], [[United States|U.S.]]
|resting_place=Cosby Cemetery <br/> [[Horse Cave, Kentucky]]
|party        = [[Republican Party (United States)|Republican]]
|spouse       = Beula Cornelius Aspley
|children     = 2 (including [[Steve Nunn|Steve]])
|education    = [[Western Kentucky University]] {{small|([[Bachelor of Arts|BA]])}}<br>[[University of Cincinnati]]<br>[[University of Louisville]] {{small|([[Bachelor of Laws|LLB]])}}
|allegiance   = {{flag|United States}}
|branch       = {{flag|United States Army}}
|serviceyears = 1943–1945
|rank         = [[Corporal]]
|unit         = [[97th Infantry Division (United States)|97th Infantry Division]]<br>[[Medical Corps (United States Army)|Army Medical Corps]]
|battles      = [[World War II]]
}}
'''Louie Broady Nunn''' (March 8, 1924&nbsp;– January 29, 2004) was the [[List of Governors of Kentucky|52nd governor of Kentucky]]. His election in 1967 made him the first [[Republican Party (United States)|Republican]] to hold that office since the end of [[Simeon Willis]]' term in 1947, and the last to hold the position until the election of [[Ernie Fletcher]] in 2003.

After rendering noncombat service in [[World War II]] and graduating from law school, Nunn entered local politics, becoming the first Republican [[county judge]] in the history of [[Barren County, Kentucky]]. He worked on the campaigns of Republican candidates for national office, including [[John Sherman Cooper]], [[Thruston Morton]], and [[Dwight D. Eisenhower]]. He was the Republican nominee for governor in 1963, but ultimately lost a close election to [[Democratic Party (United States)|Democrat]] [[Edward T. Breathitt|Ned Breathitt]]. An executive order signed by Governor [[Bert T. Combs]] that desegregated Kentucky's public services became a major issue in the campaign. Nunn vowed to repeal the order if elected, while Breathitt promised to continue it.

In 1967, Nunn again ran for governor. After defeating [[Marlow Cook]] in the Republican gubernatorial [[primary election|primary]], he eked out a victory over Democrat [[Henry Ward (Kentucky)|Henry Ward]]. The state offices were split between Democrats and Republicans, and Nunn was saddled with a Democratic [[Lieutenant Governor of Kentucky|lieutenant governor]], [[Wendell H. Ford]]. Despite a Democratic majority in the [[Kentucky General Assembly|General Assembly]], Nunn was able to enact most of his priorities, including tax increases that funded improvements to the [[List of Kentucky state parks|state park system]] and the construction of a statewide network of mental health centers. He oversaw the transition of [[Northern Kentucky University]] from a [[community college]] to a senior institution and brought the [[University of Louisville]] into the state university system. The later years of his administration were marred by [[Louisville riots of 1968|race riots in Louisville]] and a violent protest against the [[Vietnam War]] at the [[University of Kentucky]]. Following his term as governor, he unsuccessfully challenged [[Walter Huddleston|Walter "Dee" Huddleston]] in the [[United States Senate elections, 1972|1972 senatorial election]] and [[John Y. Brown, Jr.]] in the 1979 gubernatorial contest. In his later years, he supported the political ambitions of his son, [[Steve Nunn|Steve]], and advocated for the legalization of [[industrial hemp]] in Kentucky. He died of a heart attack on January 29, 2004.

==Early life==
Louie Broady Nunn was born in [[Park, Kentucky]]&nbsp;– a small community on the border of [[Barren County, Kentucky|Barren]] and [[Metcalfe County, Kentucky|Metcalfe]] counties&nbsp;– on March 8, 1924.<ref name=byrd13>Byrd, p. 13</ref> His given name, Louie, honored a deceased friend of his father's; his middle name, Broady, was a surname in his mother's family.<ref name=byrd17>Byrd, p. 17</ref> Louie was the youngest of the four sons born to Waller Harrison and Mary (Roberts) Nunn; their youngest child, Virginia, was their only daughter.<ref name=byrd16>Byrd, p. 16</ref> The Nunns were farmers and operated a general store, though Waller suffered from a congenital heart condition and severe [[arthritis]] and was limited to light chores.<ref name=wku>"WKU Hall of Distinguished Alumni". Western Kentucky University</ref><ref>Byrd, pp. 17–18</ref> The eldest brother, Lee Roy, became an influential campaigner and fundraiser for the [[Republican Party (United States)|Republican Party]].<ref name=byrd11>Byrd, p. 11</ref>

Nunn obtained the first eight years of his education in a one-room, one-teacher schoolhouse in Park.<ref name=powell108>Powell, p. 108</ref><ref name=byrd37>Byrd, p. 37</ref> During his teenage years, he gave himself a [[hernia]] while lifting a heavy piece of farm equipment.<ref name=byrd37/> This, combined with his father's health history, may have contributed to back pain issues that plagued him for most of his life.<ref>Byrd, pp. 17, 37</ref> In 1938, he matriculated to Hiseville High School.<ref name=byrd40>Byrd, p. 40</ref> He earned a [[Bachelor of Arts]] degree at Bowling Green Business University, now [[Western Kentucky University]].<ref name=nga>"Kentucky Governor Louie Broady Nunn". National Governors Association</ref>

After the [[Attack on Pearl Harbor|bombing of Pearl Harbor]] on December 7, 1941, Nunn departed for [[Cincinnati|Cincinnati, Ohio]], to take flying lessons in hopes of becoming a [[B-17 Flying Fortress|B-17]] pilot.<ref name=byrd42>Byrd, p. 42</ref> By the time he finished his flight training, however, the [[United States Army|Army]] had discontinued its air cadet program.<ref name=byrd42/> On June 2, 1943, he enlisted in the Army and received his [[recruit training]] at [[Fort Wolters]] near [[Fort Worth, Texas]].<ref name=byrd42/> He was transferred numerous times. First, he was stationed at [[Sheppard Air Force Base]] near [[Wichita Falls, Texas]].<ref name=byrd42/> Next, he was assigned to the [[97th Infantry Division (United States)|97th Infantry Division]], then received additional training at [[Fort Leonard Wood (military base)|Fort Leonard Wood]] in [[Missouri]].<ref name=byrd44>Byrd, p. 44</ref> Finally, he transferred to the [[Medical Corps (United States Army)|Army Medical Corps]], but his back injury flared up, and he received a medical discharge on September 13, 1945.<ref name=byrd44/> He held the rank of [[corporal]] at the time of his discharge.<ref name=sexton206>Sexton, p. 206</ref>

Following his military duty, Nunn pursued a pre-law degree at the [[University of Cincinnati]].<ref name=byrd44/> Three years later, he matriculated to the [[University of Louisville School of Law]] where he was a classmate of future congressman [[Marlow Cook]].<ref name=byrd46>Byrd, p. 46</ref> Nunn earned his [[Bachelor of Laws]] degree in 1950.<ref name=byrd45>Byrd, p. 45</ref><ref name=kye686>Harrison in ''The Kentucky Encyclopedia'', p. 686</ref> He opened his legal practice in [[Glasgow, Kentucky]], in September 1950.<ref name=byrd54>Byrd, p. 54</ref>

On October 12, 1950, Nunn married Beula Cornelius Aspley, a divorcee from Bond, Kentucky.<ref name=powell108/><ref name=kye686/> The couple had two children&nbsp;– Jennie Lou, born in 1951, and [[Steve Nunn|Steve]], born in 1952.<ref name=byrd57>Byrd, p. 57</ref> Aspley also had three children from her first marriage.<ref name=byrd50>Byrd, p. 50</ref> Nunn left the [[Methodism|Methodist]] denomination in which he had been raised after marrying Aspley, joining her as a member of the [[Christian Church (Disciples of Christ)|Christian Church]].<ref name=byrd23>Byrd, p. 23</ref>

==Political career==
On June 17, 1953, Nunn declared as a Republican candidate for [[county judge]] and was ultimately the only Republican to declare.<ref name=byrd68>Byrd, p. 68</ref> In the [[Democratic Party (United States)|Democratic]] primary, one of the challengers charged that the incumbent had used his office for personal gain.<ref name=byrd69>Byrd, p. 69</ref> In the wake of the investigation, a group of disgruntled Democrats formed an organization to elect Nunn, who defeated his Democratic challenger by a vote of 5,171 to 4,378, becoming the first Republican elected county judge in the history of the heavily Democratic county.<ref name=powell108/><ref name=byrd69/>

In 1956, Nunn served as statewide campaign manager for [[Dwight D. Eisenhower]]'s presidential bid, as well as the [[United States Senate|senatorial]] campaigns of [[John Sherman Cooper]] and [[Thruston Morton]].<ref name=nga/> The Kentucky Junior Chamber of Commerce named him "Young Man of the Year" in 1956.<ref name=byrd77>Byrd, p. 77</ref> He was not a candidate for re-election as county judge in 1957, but was appointed as [[city attorney]] for the city of Glasgow in 1958.<ref name=powell108/> He considered running for governor in 1959, but became convinced it would be a bad year for Republicans and did not make the race.<ref name=byrd78>Byrd, p. 78</ref> He managed successful re-election campaigns for Senator Cooper in 1960 and Senator Morton in 1962.<ref name=nga/> He also managed the state campaign of presidential candidate [[Richard Nixon]] in 1960.<ref name=sexton207>Sexton, p. 207</ref> Although [[John F. Kennedy]] won the election, Nixon carried Kentucky 54% to 46%.<ref>"1960 Presidential Election". John F. Kennedy Presidential Library and Museum</ref>

Nunn was the Republican nominee for [[governor of Kentucky]] in 1963.<ref name=powell108/> During the campaign, he attacked an executive order issued by sitting Democratic governor [[Bert T. Combs]] that desegregated public accommodations in the state.<ref name=nhok411>Harrison in ''A New History of Kentucky'', p. 411</ref> Calling the order "a dictatorial edict of questionable constitutionality", Nunn charged that it had been dictated by [[United States Attorney General|U.S. Attorney General]] [[Robert Kennedy]].<ref name=pearce221>Pearce, p. 221</ref> In a television appearance, Nunn displayed a copy of the order and declared "My first act will be to abolish this."<ref name=nhok411/> ''[[The New Republic]]'' accused him of conducting "the first outright segregationist campaign in Kentucky".<ref name=nhok411/> He lost the election to Democrat [[Edward T. Breathitt|Ned Breathitt]] by a margin of just over 13,000 votes.<ref name=nhok411/>

===Governor of Kentucky===
In 1967, Nunn faced his old classmate, [[Jefferson County, Kentucky|Jefferson County]] judge Marlow Cook, in Kentucky's first Republican gubernatorial [[primary election|primary]] in many years.<ref name=powell108/><ref name=kye686/> Nunn attacked Cook as a "[[Liberalism in the United States|liberal]], former New Yorker", and some of his supporters made reference to Cook's "Jewish backers".<ref name=nhok413>Harrison in ''A New History of Kentucky'', p. 413</ref> The injection of [[antisemitism]] into the campaign drew criticism from Senator John Sherman Cooper, who threw his support to Cook.<ref name=nhok413/> Nunn also attacked Cook for his Catholic faith, a tactic that proved particularly effective with the state's [[Protestantism|Protestant]] voters.<ref name=nhok413/> In a close vote, Nunn defeated Cook to secure the nomination.<ref name=nhok413/>

Nunn then faced Democrat [[Henry Ward (Kentucky)|Henry Ward]] in the general election.<ref name=powell108/> During the campaign, Nunn charged that Democrats wanted to raise taxes to pay for administrative inefficiencies.<ref name=kye686/> He also played up divisions within the Democratic party, and was endorsed by two-time former Democratic Governor [[Happy Chandler|A. B. "Happy" Chandler]].<ref name=kye686/><ref name=nhok413/> Nunn allied himself closely with the national Republican campaign against [[Lyndon B. Johnson]], bringing several prominent Republicans to the state to speak for him.<ref name=sexton207/> He won the election by a vote of 454,123 to 425,674, despite the fact that half of the other state offices went to Democrats, including the [[Lieutenant Governor of Kentucky|lieutenant governorship]], won by [[Wendell H. Ford]].<ref name=nhok414>Harrison in ''A New History of Kentucky'', p. 414</ref>

The General Assembly was controlled by Democrats, but Nunn was able to pass most of his agenda.<ref name=kye686/> Despite a campaign promise not to raise taxes, when the outgoing Breathitt administration projected a shortfall of $24&nbsp;million in the state budget, Nunn convinced the [[Kentucky General Assembly|General Assembly]] to pass an increase in the motor vehicle license fee from $5.00 to $12.50 and raise the state [[sales tax]] from three percent to five percent.<ref name=kye686/><ref name=sexton207/> Nunn's budget focused on increased funding for education, mental health, and economic development.<ref name=kye686/> In the 1970 legislative session, the General Assembly enacted Nunn's proposals to eliminate taxes on prescription drugs and the use fee charged on vehicles transferred within families, but rejected his plans to reduce the [[income tax]] for low-income families and increase tax credits for the blind and the elderly.<ref name=kye686/>

Nunn oversaw the entry of the [[University of Louisville]] into the state's public university system.<ref name=nga/> Fulfilling a campaign promise, he helped transform Northern Kentucky Community College into Northern Kentucky State College (which later became [[Northern Kentucky University]]), a four-year institution and member of the state university system.<ref name=crowley>Crowley, "Nunn was promoter of NKU, friends recall"</ref> Historian [[Lowell H. Harrison]] argued that these actions diluted state support to existing higher education institutions.<ref name=kye686/> Nunn also supported the newly created [[Kentucky Educational Television]].<ref name=kye686/>

Nunn doubled the accommodations in the [[List of Kentucky state parks|state park system]].<ref name=wku/> [[Barren River Lake State Resort Park]] was completed during his tenure, and three other parks were planned and funded during his administration.<ref name=wku/> He also greatly improved the state mental health system.<ref name=nyt/> Under his leadership, a statewide network of 22 mental health centers was completed, and all four state psychiatric hospitals were accredited for the first time.<ref name=nyt/> Nunn called the revamping of the state mental health system his proudest accomplishment as governor.<ref name=nyt/> There was not total agreement between Nunn and the legislature, however. The governor vetoed one-quarter of the bills passed in the 1968 legislative session and 14 percent of those passed in the 1970 session.<ref name=nhok414/> An [[fair housing|open housing]] bill became law without Nunn's signature, and he also refused to sign the 1970 state budget as a form of protest.<ref name=powell108/><ref name=nhok414/> (Unsigned bills become law after ten days under the [[Kentucky Constitution]], in contrast to the [[pocket veto]] provision in the [[United States Constitution|federal constitution]].)

A supporter of President Nixon's [[Law and order (politics)|law-and-order philosophies]], Nunn called out the [[United States National Guard|National Guard]] to break up violent protests in the state.<ref name=kye686/> In May 1968, he sent the Guard to [[Louisville, Kentucky|Louisville]] to break up [[Louisville riots of 1968|race-related protests]] that followed peaceful [[civil rights]] marches.<ref name=sexton208>Sexton, p. 208</ref> This action was criticized by civil rights leaders across the state.<ref name=sexton208/> In May 1970, Nunn again dispatched the Guard to quell protests against the [[Vietnam War]] at the [[University of Kentucky]], and imposed a curfew that interfered with [[final examination]]s.<ref name=nhok414/> The latter protest culminated in the burning of one of the university's [[Reserve Officers Training Corps|ROTC]] buildings.<ref name=kye686/>

From 1968 to 1969, Nunn served on the Executive Committee of the [[National Governors Association|National Governors' Conference]] and, in 1971, chaired the [[Republican Governors Association]].<ref name=nga/> The ''[[Louisville Courier-Journal]]'' said of Nunn's administration "On the whole, his management of the state's finances has been sound. ... [H]e took a general fund facing a deficit, restored it to solvency, and kept it healthy. No scandals have marred the Nunn record. He chose able men to direct his revenue and finance departments, and their efficiency saved the state millions of dollars."<ref>Sexton, pp. 208–209</ref> Historian [[Thomas D. Clark]] called Nunn the strongest of Kentucky's eight Republican governors.<ref name=sexton210 />

==Later career==
Following his term as governor, Nunn opened a law practice in [[Lexington, Kentucky|Lexington]].<ref name=kye686/> He campaigned for a seat in the U.S. Senate in 1972, losing to Democrat [[Walter Huddleston|Walter "Dee" Huddleston]].<ref name=powell108/> His loss came despite a landslide victory for Richard Nixon in the state and was generally blamed on his support for an increased sales tax during his gubernatorial administration.<ref name=sexton209>Sexton, p. 209</ref> He continued working on behalf of Republican candidates, and backed  [[Ronald Reagan]]'s primary challenge to incumbent [[Gerald Ford]] in 1975.<ref name=sexton209/> His last run for office came in 1979 when he was again the Republican nominee for governor against Democrat [[John Y. Brown, Jr.]]<ref name=sexton209/> He decried the excessive spending, expanding government, and increased state employment that had occurred under Democratic administrations.<ref name=sexton209/> He also attacked Brown for his playboy image (he was married to former [[Miss America]] [[Phyllis George]]) and his refusal to release his tax returns, as well as his inexperience in government.<ref name=sexton209/> Despite these attacks, Nunn lost by a vote of 558,008 to 381,278 and returned to his legal practice.<ref name=sexton209/>

In the 1980s, Nunn served on the boards of regents of [[Morehead State University]] and [[Kentucky State University]].<ref name=kye686/> He served as a lecturer at [[Western Kentucky University]], and received the Distinguished Alumni Award from the University of Louisville in 1999.<ref name=wku/> During the late 1980s, he criticized Senator [[Mitch McConnell]], one of the emerging leaders of the state's Republican party, for not doing more to support other Republicans in their bids for office; McConnell maintained that he had to focus on his own reelection campaign in 1990.<ref name=sexton209 /> In 1988, Nunn unsuccessfully challenged [[United States House of Representatives|Congressman]] [[Jim Bunning]] in his bid to retain his position as Kentucky's Republican national committeeman.<ref name=sexton209/>

In 1994, Nunn's wife Beula filed for divorce from a hospital bed where she lay dying of cancer.<ref name=samples/><ref name=estep>Estep, "Louie Nunn accused son Steve of abusing him"</ref> She claimed she was trying to preserve some of her estate for her children.<ref name=samples/> A Metcalfe County judge granted the divorce, but Nunn challenged the ruling, and it was later set aside.<ref name=estep/> Some property issues were still pending at the time of Beula's death in 1995.<ref name=estep/> During the divorce proceedings, Nunn's son Steve sided with his mother, causing a rift between him and his father.<ref name=estep/> A 1994 letter from the elder Nunn alleged that Steve Nunn physically and verbally abused Louie Nunn and other members of his family.<ref name=estep/> The letter was discovered in 2009 when Steve Nunn was charged with the murder of his former fiancée, Amanda Ross.<ref name=estep/>

In 1999, Nunn again considered a bid for governor, precluding a potential bid by his son, Steve.<ref name=sexton210>Sexton, p. 210</ref> He cited personal and health issues for not making the race. In 2000, he backed the presidential campaign of Senator [[John McCain]].<ref name=sexton210/> Nunn was reconciled to his son Steve, and when Steve ran for governor in 2003, Louie supported him.<ref name=sexton210 /> After Steve Nunn ran third in a four-way primary, the elder Nunn supported the Republican nominee, Ernie Fletcher, hosting a fundraiser for him.<ref name=sexton210/>

Nunn also became an advocate of legalizing [[industrial hemp]] in Kentucky, writing, "Frankly, I was opposed to the legalization of hemp for years because I had been of the opinion hemp was [[marijuana]]. I was short-sighted in my thinking, and I was wrong."<ref name=samples>Samples-Gutierrez, "Bluegrass hemp fight has an ally"</ref> In 2000, Nunn secured an acquittal for actor [[Woody Harrelson]], who came to [[Lee County, Kentucky]], and planted hemp seeds in open defiance of Kentucky's law forbidding the cultivation of hemp.<ref name=samples/>  Later, he traveled to [[South Dakota]] where, at the base of [[Mount Rushmore]], he publicly presented an [[Oglala Lakota]] leader with bales of hemp after the tribe's crop was confiscated by officers from the federal [[Drug Enforcement Administration]].<ref name=samples/>

Louie B. Nunn died of a heart attack at his home in [[Versailles, Kentucky]], on January 29, 2004, hours after hosting a luncheon with labor leaders seeking help in dealing with the newly elected Fletcher administration.<ref name=nyt>"Louie Nunn, Former Governor Of Kentucky, Is Dead at 79". ''The New York Times''</ref><ref name=sexton210/> He was buried at the Cosby Methodist Church cemetery in [[Hart County, Kentucky]].<ref name=wolfe>Wolfe, p. A6</ref> The Cumberland Parkway was renamed the [[Louie B. Nunn Cumberland Parkway]] in 2000, and the main lodge at the Barren River Lake State Resort Park is also named in Nunn's honor.<ref name=parkway>"Man Held in Shooting of 2 in Ginseng Patch". ''Lexington Herald-Leader''</ref><ref name=bailey>Bailey, p. D1</ref>

==See also==
{{Portal|Biography|Kentucky|Law|Politics|United States Army|World War II}}
* [[Louie B. Nunn Center for Oral History]]

==References==
{{Reflist|20em}}

===Bibliography===
*{{cite web |title=1960 Presidential Election |publisher=John F. Kennedy Presidential Library and Museum |url=http://www.jfklibrary.org/Research/Ready-Reference/JFK-Miscellaneous-Information/1960-Presidential-Election.aspx |accessdate=2012-08-17}}
*{{cite news |last=Bailey |first=Marilyn |title=Barren River State Park is Easy to Find but is Difficult to Leave |newspaper=Lexington Herald-Leader |date=October 10, 1985 |page=D1}}
*{{cite news |last=Byrd |first=Sigmun |title=The Louie Nunn Story |newspaper=Kentucky Post & Times-Star |year=1968}}
*{{cite news |last=Crowley |first=Patrick |title=Nunn was promoter of NKU, friends recall |newspaper=The Cincinnati Enquirer |date=January 31, 2004 |url=http://www.enquirer.com/editions/2004/01/31/loc_loc4a.html |accessdate=January 1, 2010}}
*{{cite news |last=Estep |first=Bill |author2=Ryan Alessi  |title=Louie Nunn accused son Steve of abusing him |newspaper=Lexington Herald-Leader |date=September 18, 2009 |url=http://www.kentucky.com/1089/story/939662.html |accessdate=December 29, 2009}}
*{{cite book |last=Harrison |first=Lowell H. |authorlink=Lowell H. Harrison |chapter=Nunn, Louie Broady |editor=Kleber, John E |others=Associate editors: [[Thomas D. Clark]], Lowell H. Harrison, and [[James C. Klotter]] |title=The Kentucky Encyclopedia |year=1992 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-1772-0 |url=http://www.kyenc.org/entry/n/NUNNL01.html |accessdate=December 29, 2009}}
*{{cite book |last=Harrison |first=Lowell H. |authorlink=Lowell H. Harrison |author2=James C. Klotter  |title=A New History of Kentucky |publisher=The University Press of Kentucky |year=1997 |isbn=0-8131-2008-X |url=https://books.google.com/books?id=63GqvIN3l3wC |accessdate=June 26, 2009|authorlink2=James C. Klotter }}
*{{cite web |title=Kentucky Governor Louie Broady Nunn |url=http://www.nga.org/cms/home/governors/past-governors-bios/page_kentucky/col2-content/main-content-list/title_nunn_louie.html |publisher=National Governors Association |accessdate=April 4, 2012}}
*{{cite news |title=Louie Nunn, Former Governor Of Kentucky, Is Dead at 79 |newspaper=The New York Times |date=January 31, 2004 |url=https://www.nytimes.com/2004/01/31/us/louie-nunn-former-governor-of-kentucky-is-dead-at-79.html |accessdate=December 29, 2009}}
*{{cite news |title=Man Held in Shooting of 2 in Ginseng Patch |newspaper=Lexington Herald-Leader |date=May 10, 2000 |page=B3}}
*{{cite book |last=Pearce |first=John Ed |title=Divide and Dissent: Kentucky Politics 1930–1963 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |year=1987 |isbn=0-8131-1613-9}}
*{{cite book |last=Powell |first=Robert A. |title=Kentucky Governors |publisher=Bluegrass Printing Company |location=Danville, Kentucky |year=1976 |oclc=2690774}}
*{{cite news |last=Samples-Gutierrez |first=Karen |title=Bluegrass hemp fight has an ally |newspaper=The Enquirer |date=March 31, 2001 |url=http://www.enquirer.com/editions/2001/03/31/loc_samples_louie_nunn.html |accessdate=December 29, 2009}}
*{{cite book |last=Sexton |first=Robert F. |author2=Al Cross  |chapter=Louie B. Nunn |title=Kentucky's Governors |editor=Lowell Hayes Harrison |publisher=The University Press of Kentucky |location=Lexington, Kentucky |year=2004 |isbn=0-8131-2326-7}}
*{{cite web |title=WKU Hall of Distinguished Alumni: 2001 HODA Inductees |url=http://alumni.wku.edu/s/808/media.aspx?sid=808&gid=1&pgid=1772 |publisher=Western Kentucky University |accessdate=August 14, 2012}}
*{{cite news |last=Wolfe |first=Charles |title=Nunn Called Last 'Person-to-Person Public Figure' |newspaper=The Kentucky Post |date=February 4, 2004 |pages=A6}}

==Further reading==
*{{cite web |title=Former Governor Louie B. Nunn Dies At 79 |url=http://www.wave3.com/Global/story.asp?S=1622979&nav=0RZFKVtm |date=January 30, 2004 |accessdate=January 4, 2010 |publisher=WAVE}}
*{{cite news |last=Loftus |first=Tom |title=2003: Nunn has moderate reputation in conservative GOP |newspaper=Louisville Courier-Journal |date=September 11, 2009 |url=http://www.courier-journal.com/article/20090911/NEWS01/90911013/2003--Nunn-has-moderate-reputation-in-conservative-GOP |accessdate=December 29, 2009}}

{{s-start}}
{{s-ppo}}
{{s-bef|before=[[John M. Robsion Jr.|John Robsion]]}}
{{s-ttl|title=[[Republican Party (United States)|Republican]] [[List of Republican nominees for Governor of Kentucky|nominee]] for [[Governor of Kentucky]]|years=[[United States gubernatorial elections, 1963|1963]], [[United States gubernatorial elections, 1967|1967]]}}
{{s-aft|after=[[Tom Emberton]]}}
|-
{{s-bef|before=[[Ronald Reagan]]}}
{{s-ttl|title=Chair of the [[Republican Governors Association]]|years=1970–1971}}
{{s-aft|after=[[William Milliken]]}}
|-
{{s-bef|before=[[John Sherman Cooper|John Cooper]]}}
{{s-ttl|title=[[Republican Party (United States)|Republican]] nominee for [[List of United States Senators from Kentucky|U.S. Senator]] from [[Kentucky]]<br>([[Classes of United States Senators|Class 2]])|years=[[United States Senate elections, 1972|1972]]}}
{{s-aft|after=[[Louie R. Guenthner, Jr.|Louie Guenthner]]}}
|-
{{s-bef|before=[[Bob Gable]]}}
{{s-ttl|title=[[Republican Party (United States)|Republican]] [[List of Republican nominees for Governor of Kentucky|nominee]] for [[Governor of Kentucky]]|years=[[United States gubernatorial elections, 1979|1979]]}}
{{s-aft|after=[[Jim Bunning]]}}
|-
{{s-off}}
{{s-bef|before=[[Edward T. Breathitt]]}}
{{s-ttl|title=[[Governor of Kentucky]]|years=1967–1971}}
{{s-aft|after=[[Wendell H. Ford]]}}
{{s-end}}

{{Governors of Kentucky}}

{{DEFAULTSORT:Nunn, Louie B.}}
[[Category:1924 births]]
[[Category:2004 deaths]]
[[Category:20th-century American politicians]]
[[Category:American Disciples of Christ]]
[[Category:American Methodists]]
[[Category:American military personnel of World War II]]
[[Category:Candidates in United States elections, 1972]]
[[Category:County judges in Kentucky]]
[[Category:Governors of Kentucky]]
[[Category:Kentucky lawyers]]
[[Category:Kentucky Republicans]]
[[Category:People from Barren County, Kentucky]]
[[Category:Republican Party state governors of the United States]]
[[Category:United States Army soldiers]]
[[Category:University of Louisville School of Law alumni]]