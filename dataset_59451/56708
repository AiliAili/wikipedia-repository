{{Infobox journal
| title = The Electronic Journal of Combinatorics
| cover         = 
| editor        =
| discipline    = Discrete mathematics
| former_names  = 
| abbreviation  =
| publisher     =
| country       = 
| frequency     = 
| history       = 
| openaccess    = Free to authors and readers
| license       = 
| impact        = 
| impact-year   = 
| website       =
| link1         = http://www.combinatorics.org
| link1-name    = E-JC homepage
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 38032205
| LCCN          = 
| CODEN         = 
| ISSN          = 1077-8926
| eISSN         = 
}}
The '''''Electronic Journal of Combinatorics''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] covering research in [[Combinatorics|combinatorial mathematics]].

The journal was established in 1994 by [[Herbert Wilf]] ([[University of Pennsylvania]]) and [[Neil Calkin]] ([[Georgia Institute of Technology]]).<ref>[http://www.math.upenn.edu/~wilf/website/E-JC%20history.pdf Wilf, H.S. ''About the Electronic Journal of Combinatorics'']</ref><ref>{{cite web|url=http://www.combinatorics.org/ojs/index.php/eljc/about/submissions#copyrightNotice |title=Submissions |publisher=Combinatorics.org |date=2012-03-01 |accessdate=2012-07-11}}</ref><ref>{{cite web|url=http://www.combinatorics.org/ojs/index.php/eljc/about/editorialPolicies#publicationFrequency |title=Editorial Policies |publisher=Combinatorics.org |date= |accessdate=2012-07-11}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal had a 2011 [[impact factor]] of 0.638.<ref name=WoS>{{cite book |year=2012 |chapter=Electronic Journal of Combinatorics |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-07-07 |series=[[Web of Science]] |postscript=.}}</ref>

==Editors-in-chief==
===Current===
Since 2013, one of the editors-in-chief has been designated the Chief Editorial Officer. The present officer is Richard Brualdi. The current [[editors-in-chief]] are:

* [[Miklós Bóna]], [[University of Florida]], United States
* [[Richard A. Brualdi]], [[University of Wisconsin, Madison]], United States
* Rod Canfield, [[University of Georgia]], United States
* [[David Conlon]], [[University of Oxford]], United Kingdom
* Catherine Greenhill, [[UNSW Australia]], Australia
* Svante Linusson, [[Royal Institute of Technology]], Sweden
* [[Brendan McKay]], [[Australian National University]], Australia
* [[Bojan Mohar]], [[Simon Fraser University]], Canada
* Marc Noy, [[Universitat Politècnica de Catalunya]], Spain
* [[Bruce Sagan]], [[Michigan State University]], United States
* [[Francisco Santos Leal|Paco Santos]], [[University of Cantabria]], Spain
* Edwin van Dam, [[Tilburg University]], Netherlands
* Ian Wanless, [[Monash University]], Australia
* David Wood, [[Monash University]], Australia 
* Qing Xiang, [[University of Delaware]], United States

=== Past ===
The following people have been [[editors-in-chief]] of the ''Electronic Journal of Combinatorics'':
{{columns-list|colwidth=30em|
* [[Herbert Wilf]] (1994–2001)
<!-- * [[Brendan McKay]] (1996–present) -->
* [[Fan Chung Graham]] (2000–2002)
<!-- * [[Richard A. Brualdi]] (2001–present) -->
* [[Peter Cameron (mathematician)|Peter Cameron]] (2001–2003)
* [[Carsten Thomassen]] (2002–2013)
* Richard Ehrenborg (2003–2004)
* [[Chris Godsil]] (2004–2008)
<!-- * [[Bruce Sagan]] (2004–present) -->
<!-- * [[E. Rodney Canfield]] (2006–present) -->
<!-- * [[Qing Xiang]] (2006–present)-->
<!-- * [[Michael Krivelevich]] (2007–present) -->
* Willem Haemers (2008–2013)
<!-- * [[Miklós Bóna]] (2010–present) -->
* Catherine Huafei Yan (2010–2013)
<!-- * [[Bojan Mohar]] (2013–present) -->
* József Solymosi (2013–2015)
<!-- * Edwin van Dam (2014–present) -->
<!-- * [[Ian Wanless]] (2013–present) -->
}}

== Dynamic surveys ==
In addition to publishing normal articles, the journal also contains a class of articles called Dynamic Surveys that are not assigned to volumes and can be repeatedly updated by the authors.<ref>[http://www.combinatorics.org/issue/view/Surveys Dynamic Surveys]</ref>

== Copyright ==
Since its inception, the journal has left copyright of all published material with its authors. Instead, authors provide the journal with an irrevocable licence to publish and agree that any further publication of the material acknowledges the journal.<ref>[http://www.combinatorics.org/ojs/index.php/eljc/about/submissions#copyrightNotice Copyright notice]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.combinatorics.org}}

[[Category:Mathematics journals]]
[[Category:Publications established in 1994]]
[[Category:Open access journals]]
[[Category:English-language journals]]