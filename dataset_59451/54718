{{italic title}}
[[Image:Robert.Helmont.the.watcher.jpg|right|thumb|"The Watcher", artwork by Picard and Montégut]]
'''''Robert Helmont''''' (1874; English: ''Robert Helmont, The Diary of a Recluse, 1870-1871'' (1892)) is a novella by French author [[Alphonse Daudet]]. It is partly based on Daudet's actual experiences during the [[Franco-Prussian War]] of 1870-71, as described in the Preface.<ref name=preface/> The book was originally published by [[Dentu]] in the 1873 ''Musel Universel'' but received little notice.<ref name=preface>Author's preface, English edition, 1896</ref> It was re-published in 1891 with illustrations by "Picard and Montégut". The forest [[Sénart]] is the same described in his novel ''Jack'' and contains some of the same characters.

Robert Helmont breaks his leg on the day of the start of the war. Unable to flee to Paris ahead of the advancing <!--as described in the novel-->"Saxons"<!--sic-->, he decides to hide in an [[Hermitage (religious retreat)|hermitage]] deep in the Sénart woods. For the next 5 months he has a series of adventures, as told by Daudet in colorful and naturalistic prose. Historically accurate details about costume, architecture, food and mannerisms provide an excellent painting of the time. Hot air balloons, messenger pigeons and late night cut-throat murders lend the novel an air of high adventure, mixed with a love of nature and solitude.

==Notes==
{{reflist}}

==External links==
*[https://archive.org/search.php?query=Robert%20Helmont%20AND%20mediatype%3Atexts ''Robert Helmont''] at [[Internet Archive]] (scanned books original editions color illustrated)

{{Alphonse Daudet}}

[[Category:1874 novels]]
[[Category:19th-century French novels]]
[[Category:French novellas]]
[[Category:French autobiographical novels]]
[[Category:Franco-Prussian War fiction]]
[[Category:Novels set in 19th-century France]]
[[Category:Works by Alphonse Daudet]]


{{1870s-novel-stub}}