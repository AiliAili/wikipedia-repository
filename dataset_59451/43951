<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = Deerhound
 |image = ASDeerhound.jpg
 |caption = The Armstrong Siddeley Deerhound
}}
{{Infobox Aircraft Engine
 |type=inline-bank [[radial engine]]
 |manufacturer=[[Armstrong Siddeley]]
 |national origin=United Kingdom
 |first run={{avyear|1935}}
 |major applications=[[Armstrong Whitworth Whitley]]
 |number built = 11 
 |program cost = 
 |unit cost = 
 |developed from = 
 |developed into = 
 |variants with their own articles =
}}
|}

The '''Armstrong Siddeley Deerhound''' was a large [[aircraft engine|aero engine]] developed by [[Armstrong Siddeley]] between 1935 and 1941. An increased [[Engine displacement|capacity]] variant known as the '''Boarhound''' was never flown,<ref>Gunston 1989, p.18.</ref> and a related, much larger, design known as the '''Wolfhound''' existed on paper only. Development of these engines was interrupted in April 1941, when the company's factory was bombed, and on 3 October 1941 the project was cancelled by the [[Air Ministry]].

==Design and development==
The Deerhound I was a triple-row, 21-cylinder, air-cooled [[radial engine]]<ref name="Gunston 1986 18">{{Cite book
  |title=World Encyclopedia of Aero Engines
  |last=Gunston  |first=Bill
  |publisher=Guild Publishing
  |year=1986
  |page=18
  |quote=3-row radials
}}</ref> design with the unusual feature of inline cylinder banks. Unlike earlier Armstrong Siddeley engines the Deerhound used [[overhead camshaft]]s to operate its [[poppet valve]]s, using one camshaft for each bank of three cylinders.<ref name="Lumsden">Lumsden 2003, p.77.</ref>

Flight testing began in 1938 using an [[Armstrong Whitworth Whitley|Armstrong Whitworth Whitley II]], [[United Kingdom military aircraft serials|serial number]] ''K7243'', during which cooling problems were encountered with the rear row of cylinders.<ref name="Pearce">{{cite web|last1=Pearce|first1=William|title=Armstrong Siddeley 'Dog' Engines|url=https://oldmachinepress.com/2015/10/27/armstrong-siddeley-dog-aircraft-engines/|website=oldmachinepress.com|accessdate=9 May 2016}}</ref> This problem was solved by a 'reversed-flow' cooling system, in which a large air duct at the rear of the [[cowling]] took in air and directed it forward to exit behind the [[propeller (aircraft)|propeller]].<ref name="Lumsden">Lumsden 2003, p.77.</ref> The project suffered a severe setback when the Whitley crashed on [[takeoff]] in March 1940, fatally injuring its crew.<ref name="Pearce" /> The accident was attributed to an incorrect [[Trim tab#Uses in aircraft|elevator trim]] setting and was not related to the engines.<ref name="Lumsden">Lumsden 2003, p.77.</ref> A single prototype Deerhound III was built and ran, and survived until the late-1970s before being scrapped. Development work on the early engines was cancelled by the Air Ministry on 23 April 1941, but running of the Mk III was allowed to continue until 3 October 1941; at this point all records were ordered to be handed over to [[Rolls-Royce Limited|Rolls-Royce]].
 
A projected increased [[Engine displacement|capacity]] variant known as the Boarhound was planned but never built,<ref name="Pearce" /> and a related much larger design, the Wolfhound, existed on paper only. The latter engine featured six banks of four cylinders, a displacement of around 61 litres (3,733 cu in) and a projected takeoff power rating of {{convert|2600|–|2800|hp|kW}}.<ref name="Pearce" />

==Armstrong Siddeley in-line radial engines==
The Hyena arrangement of cylinder banks arranged as a radial engine was continued with further designs, but with little commercial success. Only the Deerhound and Hyena were built.

;Hyena: 15 cylinders (5 banks of 3 cyl.)
;Terrier: 14 cylinders (7 banks of 2 cyl.)
;Deerhound: 21 cylinders (7 banks of 3 cyl.)
;Wolfhound: 28 cylinders (7 banks of 4 cyl.)
;Boarhound: 24 cylinders (6 banks of 4 cyl., same format as the later [[Junkers Jumo 222]])
;Mastiff: 36 cylinders (9 banks of 4 cyl.)

==Variants==
;Deerhound I:1,115&nbsp;hp (831&nbsp;kW): four built.
;Deerhound II:1,500&nbsp;hp (1,118&nbsp;kW), capacity enlarged to 41 L (2,509 cu in) by increasing [[bore (engine)|bore]] and [[stroke (engine)|stroke]]: six built.
;Deerhound III:1,800&nbsp;hp (1,342&nbsp;kW), major redesign by [[Stewart Tresilian]]: one engine built.

==Applications==
This engine's sole aircraft application was in a modified [[Armstrong Whitworth Whitley]] which was used as a testbed.

==Specifications (Deerhound I)==
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully formatted line with <li> -->
|ref=''Lumsden''.<ref name="Lumsden">Lumsden 2003, p.77.</ref>
|type=21-cylinder, 3-row air-cooled radial engine with 7 inline banks<ref name="Gunston 1986 18"/><ref name="Lumsden" />
|bore=5.26 in (135 mm) 
|stroke=4.95 in (127 mm)  
|displacement=2,259.75 cu in (38.19 L)
|length=
|diameter=
|width=
|height=
|weight= 
|valvetrain=Overhead camshaft
|supercharger=Fully supercharged
|turbocharger=
|fuelsystem=
|fueltype= 87 Octane [[Avgas]]
|oilsystem=
|coolingsystem=Air-cooled
|power=1,115 hp (831 kW) at 1,500 rpm
|specpower=
|compression=
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight= 
}}

==See also==
{{aircontent
|similar engines=
*[[Armstrong Siddeley Hyena]]
*[[Dobrynin VD-4K]]
*[[Lycoming R-7755]]
*[[Wright R-2160 Tornado]]
*[[JuMo 222]]
*[[BMW 803]]

|lists=
* [[List of aircraft engines]]
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
*Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* {{Cite book
  |title=Armstrong Siddeley –the Parkside story
  |last=Cook  |first=Ray
  |id=Historical Series Nº11
  |publisher=[[Rolls-Royce Heritage Trust]]
  |year=1988
  |isbn=0-9511710-3-8
  |ref=harv
  |pages=113–118
}}
{{refend}}
{{ASaeroengines}}

[[Category:Radial engines]]
[[Category:Armstrong Siddeley aircraft engines|Deerhound]]
[[Category:Aircraft piston engines 1930–1939]]
[[Category:Inline radial engines]]
[[Category:Aircraft air-cooled inline piston engines]]