{|{{Infobox Aircraft Begin
 | name=Anzani 10
 | image=Anzani 10 nmaf.jpg
 | caption=Anzani 10, propeller side.  Image from National Museum of the US Air Force
}}{{Infobox Aircraft Engine
 |type=10-cylinder radial piston
 |manufacturer=Anzani
 |national origin=France
 |first run=1913
 |major applications=
 |produced=
 |number built=
 |program cost=
 |unit cost=
 |developed from=
 |variants with their own articles=
}}|}
The '''[[Anzani]] 10''' was a 1913 10-[[Cylinder (engine)|cylinder]] [[Air-cooled engine|air-cooled]] [[radial engine|radial aircraft engine]].  It powered several experimental aircraft and also the later production versions of the [[Caudron G.3]] reconnaissance aircraft, the [[Caudron G.4]] bomber/trainer and the first production Cessna, the [[Cessna Model A|Model AA]].

==Design and development==
In the first decade of the 20th century Anzani developed his upright 3-cylinder 'W' type motorcycle engine, which powered [[Louis Blériot|Bleriot]]'s successful Channel crossing flight of 1909, into a three-cylinder symmetric or 'Y' radial, and from that to a 6-cylinder double-row radial engine.<ref name="Gunston">{{Harvnb|Gunston|1989|pages=15–6}}</ref>

By 1912<ref name="FlightJ13"/> he had built the '''Anzani 10''', a 10-cylinder engine, air-cooled like its predecessors, which, like other Anzani engines, was made with different size cylinders. One of the more powerful versions produced about 110&nbsp;hp (82&nbsp;kW) from 12.1 litres,<ref name="Gunston"/> a British-built Anzani 10 was rated at 125&nbsp;hp<ref name="FlightM14">''Flight'', 2 May 1914 p.474</ref> and a smaller version with a displacement of 8.27 litres produced 80&nbsp;hp (60&nbsp;kW).<ref name="FlightJu1913"/>  It was a double row engine, built with two rows of five cylinders separated along the crankshaft by about a cylinder radius, giving the engine a slimmer profile than other contemporary two-row radial engines.  Each half had its own crankpin, 180° apart,<ref>[http://www.daytonhistorybooks.com/page/page/3260348.htm A History of Aeronautics Pt 4 Ch 3] {{webarchive |url=https://web.archive.org/web/20111007205336/http://www.daytonhistorybooks.com/page/page/3260348.htm |date=October 7, 2011 }}</ref> with the connecting rods,of chrome nickel steel, broad and flat to bring the two halves close together.<ref name="Gunston"/><ref name="FlightJ14">''Flight'', 24 January 1914 pp.97-8</ref>  Cylinder heads and pistons were made of cast iron, the latter machined inside and out and fitted with a pair of rings.<ref name="FlightJ14"/>  Oil was forced through the crankshaft to the crankpins, then moved under centrifugal force to the cylinders and pistons<ref name="FlightJ14"/> from inside the crankcase which was a single light alloy casting.<ref name="FlightJ14"/>

Both inlet and exhaust valves were in the cylinder heads. The automatic inlet valves of earlier Anzani engines, opened by atmospheric pressure and closed by valve springs<ref name="FlightJu1913">''Flight'', 5 July 1913 p.748</ref> were retained, but fuel was fed from a mixing chamber in the crankcase via inlet tubes placed at the rear of the engine to avoid cooling of the mixture by the oncoming airflow.<ref name="FlightJ13">''Flight'', 4 January 1913 p.21</ref>   This arrangement placed the exhaust valves at the front of the engine, where they were operated from a cam in the rear of the crankcase via push rods and rockers.<ref name="FlightJ13"/><ref>In some images of the rear of the engine, it is initially difficult to see the narrow pushods against the background of the broader inlet tubes, e.g. Fig 4 in ''Flight'' 4 January 1913, p.21</ref>  A single carburettor fed the crankcase chambers from below.  Some versions used a single Gibaud magneto,<ref name="FlightJ14"/> running at 3,000 rpm, though others built by British Anzani had a pair of Bosch magnetos,<ref name="FlightM14"/> running slower.  Plugs (K.L.G. for the British variant<ref name="FlightM14"/>) were mounted in the sides of the cylinder heads, sloping upwards to avoid plug fouling by lubricating oil.<ref name="Gunston"/> The exhaust was collected by a prominent pair of semi-circular manifolds.

==Operational history==
One British-built 125&nbsp;hp Anzani 10 underwent exhaustive tests at [[RAE Farnborough|Farnborough]] in 1914.<ref name="Gunston"/>  Several early aircraft built singly or in small numbers flew with the Anzani 10, but the major users were the [[Caudron G.3]] and [[Caudron G.4|G.4]], particularly the later ones in which the Anzani replaced the lower powered rotary Gnomes.  Numbers of these are uncertain because of the engine change.  Because they powered the 66 Caudron G.4s purchased by the American Expeditionary Force after September 1917,<ref>Fahey 1946 p.12</ref> used largely as trainers, many Anzani 10s went to the USA. The first production aircraft built by [[Clyde Cessna]], the [[Cessna Model A|Model AA]] was powered by the Anzani 10, and 14 of these were made. Huff-Daland also used them in several aircraft.

==Applications==
:[[Avro 504K|Avro 504 K ''G-EBWO'']]<ref>{{Harvnb|Jackson|1959|pages=73, 79}}</ref>
:[[Blackburn Type I|Blackburn Land/Sea monoplane]] 1915
:[[Blackburn White Falcon]] 1915
:[[Blackburn Sidecar]] 1921
:[[Breda-Pensuti B.2]]
:[[Caudron Type F]]
:[[Caudron G.3]] 1914
:[[Caudron G.4]] 1915
:[[Central Centaur IV]] 1919
:[[Cessna Model A|Cessna Model AA]] 1920s
:[[Curtiss H-4]]
:[[Felixstowe F.1]]
:[[Handley Page Type G]] 1913
:[[Huff-Daland HD-1B]]
:[[Huff-Daland HD-4]]
:[[Huff-Daland HD-9A]]
:[[Huff-Daland TA-2]]
:[[London and Provincial Fuselage Biplane]]<ref>{{Harvnb|Jackson|1960|page=372}}</ref>
:[[Sopwith Grasshopper]]
:[[Sopwith Gunbus|Sopwith Greek Pusher]]
:[[Timm Collegiate]]
:[[Vickers F.B.12|Vickers F.B. 12C]] 1917

==Specifications (110 hp)==
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|ref=<ref name="Gunston"/>
|type=two-row ten-cylinder radial piston engine
|bore=105 mm (4.13 in)
|stroke=140 mm (5.51 in)
|displacement=12.12 litres (739.8 cu in)
|length=
|diameter=
|width=
|height=
|weight=140 kg (308 lb)<ref name="FlightJu1913"/>
|valvetrain=One inlet and one exhaust valves/cylinder. Automatic inlet valves, push and rocker operated exhaust valves.
|supercharger=
|turbocharger=
|fuelsystem=Single carburettor mounted between lowest cylinders, mixing chamber in crankcase.  Single magneto.  Plugs were fitted in the upper side of the cylinder head to avoid fouling.
|fueltype=
|oilsystem=
|coolingsystem=air-cooled
|power= 82 kW (110 hp)
|specpower=
|compression=
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight= 0.59 kW/kg (0.36 hp/lb)
|designer= A. Anzani
|reduction_gear=
|general_other=
|components_other=
|performance_other=
}}

==References==
{{Commons category|Anzani 10}}

===Notes===
{{Reflist}}

===Bibliography===
{{Refbegin}}
*{{Cite book|title= U.S. Army Aircraft (heavier-than-air) 1908-1946|last=Fahey|first=James Charles|authorlink=James Charles Fahey|publisher=Ships and Aircraft|location=New York|year=1946|oclc=1726749}}
*{{Cite book |title= British Civil Aircraft 1919-59|last=Jackson|first=Aubrey Joseph| year=1959|volume=1|publisher=Putnam Publishing |location=London |oclc=1301918 |ref=harv}}
*{{Cite book |title= British Civil Aircraft 1919-59|last=Jackson|first=Aubrey Joseph| year=1960|volume=2|publisher=Putnam Publishing |location=London |oclc=221677465 |ref=harv}}
*{{Cite journal |last= |first= |authorlink= |coauthors= |title=Aeronautical Engines|journal=[[Flight International|Flight]]|volume= |date=4 January 1913 |pages=20–1  |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200020.html |quote= }}
*{{Cite journal |last= |first= |authorlink= |coauthors= |title=Anzani engines and the new 200 h.p. model|journal=[[Flight International|Flight]]|volume= |date=5 July 1913 |pages=748 |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200722.html |quote= }}
*{{Cite journal |last= |first= |authorlink= |coauthors= |year= |month= |title=Aero engines at Paris show 1913|journal=[[Flight International|Flight]]|volume= |date=24 January 1914 |pages=97 |id= |url= http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200097.html |quote= }}
*{{Cite journal |last= |first= |authorlink= |coauthors= |title=Notes|journal= [[Flight International|Flight]]|volume= |date=2 May 1914 |pages=474 |url=http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200474.html |quote= }}
*{{Cite book|title=World Encyclopaedia of Aero Engines |last=Gunston|first=Bill|year=1989|publisher=Patrick Stephens |location= Wellingborough |pages=|isbn=1-85260-163-9|ref=harv}}
{{Refend}}

{{Anzani aeroengines}}

[[Category:Aircraft piston engines 1910–1919]]
[[Category:Radial engines]]
[[Category:Anzani aircraft engines|10]]