{{Infobox individual darts tournament
|tournament_name = [[World Darts Trophy]]
|image = 
|dates = 4 December 2004 – 12 December 2004
|venue = [[:nl:Vechtsebanen|De Vechtsebanen]]
|location = [[Utrecht (city)|Utrecht]], [[Utrecht (province)|Utrecht]]
|country = the [[Netherlands]]
|organisation = [[British Darts Organisation|BDO]] / [[World Darts Federation|WDF]]
|format = Men<br>[[Set (darts)|Sets]]<br>Final – Best of 11 [[Set (darts)|Sets]]<br>Women<br>[[Set (darts)|Sets]]<br>Final – Best of 5 [[Set (darts)|Sets]]
|prize_fund =
|winners_share =
|nine_dart =
|high_checkout = '''170''' {{flagicon|AUS}} [[Tony David]]
|winner = {{flagicon|NED}} [[Raymond van Barneveld]] (men)<br>{{flagicon|NED}} [[Francis Hoenselaar]] (women)
|prev = [[2003 World Darts Trophy|2003]]
|next = [[2005 World Darts Trophy|2005]]}}

The '''2004 Bavaria World Darts Trophy''' was the third edition of the [[World Darts Trophy]], a professional darts tournament held at the [[:nl:Vechtsebanen|De Vechtsebanen]] in [[Utrecht]], the [[Netherlands]], run by the [[British Darts Organisation]] and the [[World Darts Federation]].

The 2003 winner, [[Raymond van Barneveld]] retained the trophy beating [[Martin Adams]] in the final of the men's event, 6–4 in sets. [[Andy Fordham]]. the [[2004_BDO_World_Darts_Championship#Men|BDO World Champion]], was absent from the field this year in the men's event. 
In the women's event, the 2003 winner and the [[2004_BDO_World_Darts_Championship#Women|BDO World Champion]], [[Trina Gulliver]] lost at the quarter-final stage to [[Anastasia Dobromyslova]]. Dobromyslova was then beaten by [[Francis Hoenselaar]], last year's finalist, 3–1 in sets in the final.

==Seeds==

'''Men'''
# {{flagicon|NED}} [[Raymond van Barneveld]]
# {{flagicon|ENG}} [[Mervyn King (darts player)|Mervyn King]]
# {{flagicon|ENG}} [[Tony West (darts player)|Tony West]]
# {{flagicon|ENG}} [[Ted Hankey]]
# {{flagicon|AUS}} [[Tony David]]
# {{flagicon|SCO}} [[Gary Anderson (darts player)|Gary Anderson]]
# {{flagicon|ENG}} [[Tony O'Shea]]
# {{flagicon|ENG}} [[Darryl Fitton]]

== Prize Money ==
=== Men ===
{| class="wikitable" style="text-align: center;"
! style="background:#ffffff; color:#000000;"| Pos<ref>{{cite web | url=http://www.dartsdatabase.co.uk/EventPrizeFund.aspx?EventKey=269 | title=2004 World Darts Trophy Prize Money | publisher=Darts Database | accessdate=4 June 2015}}</ref>
! style="background:#ffffff; color:#000000;"| Money (Euros)
|-
| Winner
| 45,000
|-
| Runner-up
| 22,500
|-
| Semi-Finals
| 11,250
|-
| Quarter-Finals
| 6,000
|-
| Last 16
| 3,000
|-
| Last 32
| 2,000
|}

== Men's Tournament ==
{{32TeamBracket
| RD1=First Round<br />Best of 5 sets<ref name=2004WDT>{{cite web | url=http://www.dartsdatabase.co.uk/EventResults.aspx?EventKey=269 | title=2004 World Darts Trophy Results | publisher=Darts Database | accessdate=5 June 2006}}</ref><ref name=2004WDT2>{{cite web | url=https://www.mastercaller.nl/en/tournaments/bdo/world-darts-trophy-men/2004 | title=2004 BDO World Darts Trophy Men | publisher=Master Caller | accessdate=6 June 2015}}</ref>
| RD2=Second Round<br />Best of 5 sets<ref name=2004WDT/><ref name=2004WDT2/>
| RD3=Quarter-Finals<br />Best of 9 sets<ref name=2004WDT/><ref name=2004WDT2/>
| RD4=Semi-Finals<br />Best of 9 sets<ref name=2004WDT/><ref name=2004WDT2/>
| RD5=Final<br />Best of 11 sets<ref name=2004WDT/><ref name=2004WDT2/>
| score-width=15
| RD1-seed01=
| RD1-team01= {{flagicon|NED}} '''[[Raymond van Barneveld]]'''
| RD1-score01= '''3'''
| RD1-seed02=
| RD1-team02= {{flagicon|NZL}} [[Peter Hunt (darts player)|Peter Hunt]]
| RD1-score02= 1
| RD1-seed03=
| RD1-team03= {{flagicon|NED}} [[Andre Brantjes]]
| RD1-score03= 2
| RD1-seed04=
| RD1-team04= {{flagicon|SWE}} '''[[Markus Korhonen]]'''
| RD1-score04= '''3'''
| RD1-seed05=
| RD1-team05= {{flagicon|ENG}} '''[[Darryl Fitton]]'''
| RD1-score05= '''3'''
| RD1-seed06=
| RD1-team06= {{flagicon|NED}} [[Albertino Essers]]
| RD1-score06= 0
| RD1-seed07=
| RD1-team07= {{flagicon|ENG}} [[Davy Richardson]]
| RD1-score07= 2
| RD1-seed08=
| RD1-team08= {{flagicon|ENG}} '''[[Tony Eccles]]'''
| RD1-score08= '''3'''
| RD1-seed09=
| RD1-team09= {{flagicon|ENG}} '''[[Ted Hankey]]'''
| RD1-score09= '''3'''
| RD1-seed10=
| RD1-team10= {{flagicon|ENG}} [[John Walton (darts player)|John Walton]]
| RD1-score10= 1
| RD1-seed11=
| RD1-team11= {{flagicon|NED}} [[Dick van Dijk (darts player)|Dick van Dijk]]
| RD1-score11= 0
| RD1-seed12=
| RD1-team12= {{flagicon|ENG}} '''[[Gary Robson (darts player)|Gary Robson]]'''
| RD1-score12= '''3'''
| RD1-seed13=
| RD1-team13= {{flagicon|NOR}} '''[[Robert Wagner]]'''
| RD1-score13= '''3'''
| RD1-seed14=
| RD1-team14= {{flagicon|FIN}} [[Marko Kantele]]
| RD1-score14= 2 
| RD1-seed15=
| RD1-team15= {{flagicon|ENG}} [[Stephen Bunting]]
| RD1-score15= 0
| RD1-seed16=
| RD1-team16= {{flagicon|AUS}} '''[[Tony David]]'''
| RD1-score16= '''3'''
| RD1-seed17=
| RD1-team17= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD1-score17= '''3'''
| RD1-seed18=
| RD1-team18= {{flagicon|SCO}} [[Gary Anderson (darts player)|Gary Anderson]]
| RD1-score18= 1
| RD1-seed19=
| RD1-team19= {{flagicon|NED}} [[Rick Hofstra]]
| RD1-score19= 0
| RD1-seed20=
| RD1-team20= {{flagicon|NED}} '''[[Co Stompe]]''' 
| RD1-score20= '''3'''
| RD1-seed21=
| RD1-team21= {{flagicon|ENG}} '''[[Tony West (darts player)|Tony West]]'''
| RD1-score21= '''3'''
| RD1-seed22=
| RD1-team22= {{flagicon|SCO}} [[Mike Veitch]]
| RD1-score22= 0
| RD1-seed23=
| RD1-team23= {{flagicon|SCO}} [[Bob Taylor (darts player)|Bob Taylor]]
| RD1-score23= 0
| RD1-seed24=
| RD1-team24= {{flagicon|GER}} '''[[Tomas Seyler]]'''
| RD1-score24= '''3'''
| RD1-seed25=
| RD1-team25= {{flagicon|ENG}} '''[[Tony O'Shea]]'''
| RD1-score25= '''3'''
| RD1-seed26=
| RD1-team26= {{flagicon|AUS}} Steve Duke
| RD1-score26= 0
| RD1-seed27=
| RD1-team27= {{flagicon|FIN}} [[Jarkko Komula]]
| RD1-score27= 2
| RD1-seed28=
| RD1-team28= {{flagicon|NED}} '''[[Vincent van der Voort]]'''
| RD1-score28= '''3'''
| RD1-seed29=
| RD1-team29= {{flagicon|ENG}} '''[[Shaun Greatbatch]]'''
| RD1-score29= '''3'''
| RD1-seed30=
| RD1-team30= {{flagicon|ENG}} [[Martin Atkins (darts player)|Martin Atkins]]
| RD1-score30= 0
| RD1-seed31=
| RD1-team31= {{flagicon|ENG}} [[Dave Routledge]]
| RD1-score31= 0
| RD1-seed32=
| RD1-team32= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD1-score32= '''3'''
| RD2-seed01=
| RD2-team01= {{flagicon|NED}} '''[[Raymond van Barneveld]]'''
| RD2-score01= '''3'''
| RD2-seed02=
| RD2-team02= {{flagicon|SWE}} [[Markus Korhonen]]
| RD2-score02= 0
| RD2-seed03=
| RD2-team03= {{flagicon|ENG}} [[Darryl Fitton]]
| RD2-score03= 1
| RD2-seed04=
| RD2-team04= {{flagicon|ENG}} '''[[Tony Eccles]]'''
| RD2-score04= '''3'''
| RD2-seed05=
| RD2-team05= {{flagicon|ENG}} '''[[Ted Hankey]]'''
| RD2-score05= '''3'''
| RD2-seed06=
| RD2-team06= {{flagicon|ENG}} [[Gary Robson (darts player)|Gary Robson]]
| RD2-score06= 2
| RD2-seed07=
| RD2-team07= {{flagicon|NOR}} [[Robert Wagner]]
| RD2-score07= 2
| RD2-seed08=
| RD2-team08= {{flagicon|AUS}} '''[[Tony David]]'''
| RD2-score08= '''3'''
| RD2-seed09=
| RD2-team09= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD2-score09= '''3'''
| RD2-seed10=
| RD2-team10= {{flagicon|NED}} [[Co Stompe]]
| RD2-score10= 1
| RD2-seed11=
| RD2-team11= {{flagicon|ENG}} [[Tony West (darts player)|Tony West]]
| RD2-score11= 2
| RD2-seed12=
| RD2-team12= {{flagicon|GER}} '''[[Tomas Seyler]]'''
| RD2-score12= '''3'''
| RD2-seed13=
| RD2-team13= {{flagicon|ENG}} '''[[Tony O'Shea]]'''
| RD2-score13= '''3'''
| RD2-seed14=
| RD2-team14= {{flagicon|NED}} [[Vincent van der Voort]]
| RD2-score14= 0
| RD2-seed15=
| RD2-team15= {{flagicon|ENG}} [[Shaun Greatbatch]]
| RD2-score15= 0
| RD2-seed16=
| RD2-team16= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD2-score16= '''3'''
| RD3-seed01=
| RD3-team01= {{flagicon|NED}} '''[[Raymond van Barneveld]]'''
| RD3-score01= '''5'''
| RD3-seed02=
| RD3-team02= {{flagicon|ENG}} [[Tony Eccles]]
| RD3-score02= 0
| RD3-seed03=
| RD3-team03= {{flagicon|ENG}} [[Ted Hankey]]
| RD3-score03= 4
| RD3-seed04=
| RD3-team04= {{flagicon|AUS}} '''[[Tony David]]'''
| RD3-score04= '''5'''
| RD3-seed05=
| RD3-team05= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD3-score05= '''5'''
| RD3-seed06=
| RD3-team06= {{flagicon|GER}} [[Tomas Seyler]]
| RD3-score06= 2
| RD3-seed07=
| RD3-team07= {{flagicon|ENG}} [[Tony O'Shea]]
| RD3-score07= 0
| RD3-seed08=
| RD3-team08= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD3-score08= '''5'''
| RD4-seed01=
| RD4-team01= {{flagicon|NED}} '''[[Raymond van Barneveld]]'''
| RD4-score01= '''5'''
| RD4-seed02=
| RD4-team02= {{flagicon|AUS}} [[Tony David]]
| RD4-score02= 3
| RD4-seed03=
| RD4-team03= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD4-score03= '''5'''
| RD4-seed04=
| RD4-team04= {{flagicon|ENG}} [[Mervyn King (darts player)|Mervyn King]]
| RD4-score04= 4
| RD5-seed01=
| RD5-team01= {{flagicon|NED}} '''[[Raymond van Barneveld]]'''
| RD5-score01= '''6'''
| RD5-seed02=
| RD5-team02= {{flagicon|ENG}} [[Martin Adams]]
| RD5-score02= 4
}}

== Women's Tournament ==
{{8TeamBracket-Compact-NoSeeds|RD2=Quarter-Finals<br>Best of 3 sets<ref name=2004WWDT>{{cite web | url=http://www.dartsdatabase.co.uk/EventResults.aspx?EventKey=821 | title=2004 Women's World Darts Trophy Results | publisher=Darts Database | accessdate=5 June 2006}}</ref><ref name=2004WWDT2>{{cite web | url=https://www.mastercaller.nl/en/tournaments/bdo/world-darts-trophy-ladies/2004 | title=2004 BDO World Darts Trophy Ladies | publisher=Master Caller | accessdate=6 June 2015}}</ref>
| RD3=Semi-Finals<br>Best of 3 sets<ref name=2004WWDT/><ref name=2004WWDT2/>
| RD4=Final<br>Best of 5 sets<ref name=2004WWDT/><ref name=2004WWDT2/>
| team-width=200px
| RD1-seed01=
| RD1-team01= {{flagicon|NED}} '''[[Francis Hoenselaar]]'''
| RD1-score01= '''2'''
| RD1-seed02=
| RD1-team02= {{flagicon|WAL}} Jan Robbins
| RD1-score02= 1
| RD1-seed03=
| RD1-team03= {{flagicon|ENG}} '''Crissy Manley'''
| RD1-score03= '''2'''
| RD1-seed04=
| RD1-team04= {{flagicon|SWE}} Carina Ekberg
| RD1-score04= 0
| RD1-seed05=
| RD1-team05= {{flagicon|ENG}} [[Trina Gulliver]]
| RD1-score05= 0
| RD1-seed06=
| RD1-team06= {{flagicon|RUS}} '''[[Anastasia Dobromyslova]]'''
| RD1-score06= '''2'''
| RD1-seed07=
| RD1-team07= {{flagicon|NED}} [[Karin Krappen]]
| RD1-score07= 0
| RD1-seed08=
| RD1-team08= {{flagicon|NED}} '''[[Mieke de Boer]]'''
| RD1-score08= '''2'''
| RD2-seed01=
| RD2-team01= {{flagicon|NED}} '''[[Francis Hoenselaar]]'''
| RD2-score01= '''2'''
| RD2-seed02=
| RD2-team02= {{flagicon|ENG}} Crissy Manley
| RD2-score02= 0
| RD2-seed03=
| RD2-team03= {{flagicon|RUS}} '''[[Anastasia Dobromyslova]]'''
| RD2-score03= '''2'''
| RD2-seed04=
| RD2-team04= {{flagicon|NED}} [[Mieke de Boer]]
| RD2-score04= 0
| RD3-seed01=
| RD3-team01= {{flagicon|NED}} '''[[Francis Hoenselaar]]'''
| RD3-score01= '''3'''
| RD3-seed02=
| RD3-team02= {{flagicon|RUS}} [[Anastasia Dobromyslova]]
| RD3-score02= 1
}}

== References ==
{{Reflist|3}}

== External links ==

[[Category:Articles created via the Article Wizard]]
[[Category:Darts in the United Kingdom]]