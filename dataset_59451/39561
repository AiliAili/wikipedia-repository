{{Use dmy dates|date=September 2011}}
{{Infobox journal
| title = Dalton Transactions
| editor = Andrew Shore
| discipline = [[Chemistry]]
| abbreviation = Dalton Trans.
| publisher=[[Royal Society of Chemistry]]
| country = United Kingdom
| frequency = Weekly
| history = ''Journal of the Chemical Society A: Inorganic, Physical, Theoretical'' (1966–1971)<br>''Journal of the Chemical Society, Dalton Transactions: Inorganic Chemistry'' (1972–2003)<br>''Dalton Transactions'' (2003–present)
| openaccess = [[Hybrid open access|Hybrid]]
| impact = 4.177
| impact-year = 2015
| website = http://www.rsc.org/Publishing/Journals/dt/index.asp
| formernames =
| cover = [[File:CoverIssueDaltonTrans.jpg]]
| ISSN = 1477-9226
| eISSN = 1477-9234
| CODEN = DTARAF
| LCCN = 2003242012
| OCLC = 51500500
}}
'''''Dalton Transactions''''' is a [[peer review|peer-reviewed]] [[scientific journal]] publishing [[original research|original (primary) research]] and [[literature review|review articles]] on all aspects of the [[chemistry]] of [[Inorganic chemistry|inorganic]], [[bioinorganic]], and [[organometallic]] compounds. It is published weekly by the [[Royal Society of Chemistry]]. The journal was named after the English chemist, [[John Dalton]], best known for his work on modern [[atomic theory]]. Authors can elect to have accepted articles published as open access.<ref>{{cite web|url=http://www.rsc.org/Publishing/Journals/OpenScience/index.asp |title=RSC Open Access |publisher=Rsc.org |date= |accessdate=2013-03-15}}</ref> The [[editor]] is Andrew Shore.<ref name=editorialboard>{{cite web|title=Editorial Board|url=http://www.rsc.org/publishing/journals/dt/staff.asp|publisher=Royal Society of Chemistry |accessdate=2015-01-20}}</ref> ''Dalton Transactions'' was named a "rising star" by ''In-cites'' from [[Thomson Scientific]] in 2006.<ref>{{cite web|url=http://www.in-cites.com/most_imp/may2006.html |title=Rising Stars - May 2006 |publisher=in-cites |date=2004-10-18 |accessdate=2013-03-15}}</ref>

== Publication history ==
The journal was established as the [[Journal of the Chemical Society|''Journal of the Chemical Society A: Inorganic, Physical, Theoretical'']] in 1966. In 1972, the journal was divided into three separate journals: ''Journal of the Chemical Society, Dalton Transactions'' (covering inorganic and organometallic chemistry), ''[[Faraday Transactions|Journal of the Chemical Society, Faraday Transactions 1: Physical Chemistry in Condensed Phases]]'', and ''[[Faraday Transactions|Journal of the Chemical Society, Faraday Transactions 2: Molecular and Chemical Physics]]''. The ''Journal of the Chemical Society, Dalton Transactions'' was renamed in 2003 to ''Dalton Transactions''. In January 2000, ''[[Acta Chemica Scandinavica]]'' was absorbed.<ref>{{cite journal |last1=Harnung|first1=S|year=2001|title=Acta Chemica Scandinavica|journal=Dansk kemi|volume=82 |pages=44–46}}</ref>

While the ''Journal of the Chemical Society, Dalton Transactions'' was published as 12 issues a year from 1972, as submissions increased, the journal switched to 24 issues a year in 1992<ref>{{cite journal |last1=Dean |first1=J |year=1992 |title= Editorial |journal=J. Chem. Soc., Dalton Trans. |pages=vii–viii |publisher=Royal Society of Chemistry |doi=10.1039/DT99200F0VII |url=http://xlink.rsc.org/?DOI=10.1039/DT99200F0VII |accessdate=2011-02-26}}</ref> and then to 48 issues a year in 2006.<ref>{{cite journal |last1=Humphrey |first1=J |last2=Walton |first2=P |year=2006 |title=Dalton Transactions: Developing for the Inorganic Community |journal=Dalton Trans. |pages=15–17 |publisher=Royal Society of Chemistry |doi=10.1039/B516708F |url=http://xlink.rsc.org/?DOI=10.1039/10.1039/B516708F |accessdate=2011-02-26}}</ref>

In 2010, the journal introduced a sequential volume numbering scheme, with one volume per year. While volume numbers were not assigned retro-actively, the first issue of 2010 was assigned volume 39 (2010 being the 39th year since the establishment of the ''Journal of the Chemical Society, Dalton Transactions''.<ref>{{cite web |url=http://www.rsc.org/Publishing/librarians/News/ChemCommDaltonTransactions.asp |title=RSC - ChemComm & Dalton Transactions |publisher=Royal Society of Chemistry}}</ref>

== Subject coverage ==
''Dalton Transactions'' publishes articles on all aspects of the chemistry of [[Inorganic chemistry|inorganic]] and [[organometallic]] compounds, including [[bioinorganic]], biological inorganic, and [[Solid state chemistry|solid-state]] inorganic chemistry; the application of physicochemical techniques to the study of their structures, properties, and reactions, including [[chemical kinetics|kinetics]] and mechanisms; [[Chemical synthesis|synthesis]] and characterisation of new inorganic [[Materials science|materials]]; [[Homogeneous catalysis|homogeneous]] and [[heterogeneous catalysis]]; new or improved experimental techniques and syntheses.

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=masterList>{{cite web
 | title =Dalton Transactions
 | work=Master Journal List
 | publisher=[[Thomson Reuters]]
 | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1477-9226
 | accessdate =2011-01-29}}</ref>
* [[Chemical Abstracts Service]] – [[CASSI]]
* [[PubMed]] – [[MEDLINE]]
* [[Science Citation Index]]
* [[Scopus]]
* [[GeoRef]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.177, ranking it 5th out of 44 journals in the category "Chemistry, Inorganic & Nuclear".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Chemistry, Inorganic & Nuclear |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== Notable articles ==
According to the [[Web of Science]], the following three articles have been cited most often:<ref name="WoS2">{{cite web |url = http://isiwebofknowledge.com |title = Web of Science |year = 2010 |accessdate = 2011-01-31}}</ref>
#{{cite journal |author=C Janiak, |year=2000 |title=A critical account on π–π stacking in metal complexes with aromatic nitrogen-containing ligands |journal=Dalton Trans. |pages=3885–3896 |doi=10.1039/B003010O |issue=21}}
# {{cite journal |author=C Janiak, |year=2003 |title=Engineering coordination polymers towards applications |journal=Dalton Trans. |pages=2781–2894 |doi=10.1039/B305705B |issue=14}}
#{{cite journal |author=M Bochmann, |year=1996 |title=Cationic Group 4 metallocene complexes and their role in polymerisation catalysis: the chemistry of well defined Ziegler catalysts |journal=J. Chem. Soc., Dalton Trans.|pages=255–270 |doi=10.1039/DT9960000255 |issue=3}}
# {{cite journal |author=Wakeel Ahmed Dar and K. Iftikhar, |year=2016 |title= Phase Controlled Color Tuning of the Samarium and Europium Complexes and Excellent Photostability of their PVA Encapsulated Materials. Structural Elucidation, Photophysical Parameters and Energy Transfer Mechanism of Eu(III) Complex by Sparkle/PM3 Calculations. |journal=Dalton Trans. |pages=8956–8971 |doi=10.1039/C6DT00549G |issue=45}}

== Dalton Discussions ==
Dalton Discussions are scientific meetings that provide a forum for the exchange of views and newly acquired results in focused areas of inorganic chemistry. The papers, which are associated with the oral presentations at the meeting, are published in a special issue of ''Dalton Transactions'', which constitutes a permanent record of the meeting. The meetings are usually held annually.<ref>{{cite web |url=http://www.rsc.org/ConferencesAndEvents/RSCConferences/DDpast/index.asp |title=Past Dalton Discussions |publisher=Dalton Transactions |accessdate=2013-03-15}}</ref>

== See also ==
{{Portal|Chemistry}}
* [[List of scientific journals in chemistry]]
* [[List of important publications in chemistry#Inorganic chemistry|Important publications in inorganic chemistry]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.rsc.org/Publishing/Journals/dt/index.asp}}
* [http://www.rsc.org/ConferencesAndEvents/RSCConferences/DDpast/index.asp Dalton Discussions]

{{Royal Society of Chemistry|state=collapsed}}
[[Category:Chemistry journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 1966]]
[[Category:English-language journals]]
[[Category:Hybrid open access journals]]
[[Category:Weekly journals]]