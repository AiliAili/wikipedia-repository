{{Infobox company
|  name   = XCOR Aerospace
|  logo   = [[File:Xcor-aerospace-logo.png|150px|XCOR Aerospace]]
|  type   = [[Private company|Private]]
|  slogan =
|  foundation     = September 1999
|  location       = [[Mojave Air and Space Port]], [[Mojave, California]]<ref name=mrt20120707/>
|  key_people     = [[John H. Gibson]] (CEO)
|  num_employees  = 40<ref name=mrt20120707/>
|  revenue        = n/a
|  industry       = [[Aerospace engineering|Aerospace]] and [[space tourism]]
|  products       = [[suborbital|suborbital spaceflight]]
|  homepage       = [http://www.xcor.com www.xcor.com]
}}

'''XCOR Aerospace''' is an American [[private spaceflight]] and [[rocket engine]] development company based at the [[Mojave Air and Space Port]] in [[Mojave, California]], [[Midland International Air and Space Port|Midland International Air and Spaceport]] in [[Midland, Texas metropolitan area|Midland, Texas]]<ref>{{cite news
  | last = Pappalardo
  | first = Jeff
  | title = New Area 51: Mojave's Desert Outpost Holds Space Flight's Future
  | publisher = Popular Mechanics
  | date = July 2008
  | url = http://www.popularmechanics.com/science/air_space/4273921.html?page=1
  | accessdate = 2008-07-24}}</ref> and the Amsterdam area, the Netherlands.<ref>{{cite web |url=http://xcor.com/contact/?p=spaceexpeditions |title=Contact |publisher=XCOR Aerospace |accessdate=March 24, 2016}}</ref> XCOR was formed by former members of the [[Rotary Rocket]] rocket engine development team in September, 1999.

XCOR is headed by John "Jay" Gibson who is [[CEO]].<ref name=sdc20150721/>

XCOR Aerospace is the parent operation and is concerned with engineering and building spaceships. There are two main subdivisions within it; [[#XCOR Space Expeditions|XCOR Space Expeditions]] provides marketing and sales, and [[#XCOR Science|XCOR Science]] conducts scientific and educational payload flights.

==History==
The company was founded in [[Mojave, California]] in 1999<ref name=sdc20120523/> by [[Jeff Greason]], Dan DeLong, Aleta Jackson and Doug Jones<ref name=mrt20120707>
{{cite news |last=Petty|first=Kathleen |title=A look at XCOR |url=http://www.mywesttexas.com/editors_picks/article_4345625e-c892-11e1-a3d2-001a4bcf887a.html |accessdate=2012-07-09 |newspaper=Midland Reporter-Telegram |date=2012-07-07 }}</ref> 
who had previously worked at the [[Rotary Rocket]] company.<ref name=xa20120214>
{{cite web |title=Our Team:  Founders |url=http://www.xcor.com/bio/ |work=Bios|publisher=XCOR Aerospace |accessdate=2012-02-14 |quote=''Founders:  Jeff Greason, President; Dan DeLong, Vice President and Chief Engineer; Doug Jones, Co-Founder and Chief Test Engineer; Aleta Jackson, Co-Founder and Manager''}}</ref>

In 2001, XCOR designed and built [[EZ-Rocket]], the first privately built and flown [[Rocket-powered aircraft|rocket-powered airplane]].<ref name=forbes20140618>{{cite news|last1=Knapp|first1=Alex|title=Bootstrapping To The Stars|url=http://www.forbes.com/sites/alexknapp/2014/06/18/bootstrapping-to-the-stars/|accessdate=2014-06-19|publisher=Forbes|date=2014-06-18}}</ref>  EX-Rocket made its maiden flight in July 2001.

XCOR moved its development and manufacturing operations to [[Midland, Texas]] from July 2012 to 2015.<ref name=sdc20120523>
{{cite news |last=David|first=Leonard |title=Potential Colorado Spaceport Plan Gaining Steam |url=http://www.space.com/15829-colorado-spaceport-private-spaceflight.html |accessdate=2012-05-23 |newspaper=Space.com |date=2012-05-23}}</ref> The company uses the Mojave Air and Space port primarily to conduct test flights.<ref>[http://xcor.com/news/xcor-breaking-down-walls-with-midland-one-step-closer-to-space/?p=spaceexpeditions]</ref>

In 2015, XCOR attracted investment from Chinese venture firm [[Haiyin Capital]], valuing the company at $140 million.<ref>{{Cite journal |url= http://www.forbes.com/sites/alexknapp/2015/05/26/chinese-venture-firm-haiyin-capital-is-investing-in-space-company-xcor/ |title= Chinese Venture Firm Haiyin Capital Is Investing In Space Company XCOR |author= Alex Knapp |date= 26 May 2015 |journal= Forbes |publisher= Forbes }}</ref>

In mid-2015, John "Jay" Gibson succeeded [[Jeff Greason]]—CEO from 1997-2015—as CEO.<ref name=sdc20150721>
[http://www.space.com/28825-xcor-aerospace-lynx-plane-ceo.html XCOR Aerospace Picks New CEO], accessed 2015-07-21</ref><ref name=xa20120214/><ref>[http://www.xcor.com/about_us/index.html XCOR Aerospace Company Overview], accessed 2009-02-19</ref> Later that year three of the co-founders, [[Jeff Greason]], [[Dan DeLong]] and [[Aleta Jackson]], left the company to found [[Agile Aero]], an aerospace company focused on rapid development and prototyping of aerodynamic spacecraft.<ref>{{cite news |url= http://spacenews.com/xcor-co-founders-establish-new-company/ |title= XCOR Co-Founders Establish New Company |author= Jeff Foust |date= 30 November 2015 |publisher= SpaceNews }}</ref>

In May 2016, the company halted development of the Lynx spaceplane and pivoted company focus toward development its [[LOX]]/[[LH2]] engine technology, particularly on a funded project for [[United Launch Alliance]]. The company [[laid off]] more than 20 people of the 50–60 persons onboard prior to May.<ref name="sn20160531">{{cite news |last=Foust |first=Jeff |url=http://spacenews.com/xcor-lays-off-employees-to-focus-on-engine-development/ |title=XCOR lays off employees to focus on engine development |work=[[SpaceNews]] |date=2016-05-31 |accessdate=2016-07-13}}</ref>

== Projects ==
[[File:Xcor-rocketracer-N216MR-071029-07cr-7.jpg|thumb|right|The prototype Rocket Racer, a modified [[Velocity SE]] climbing to 10,000 feet on its first full flight, October 29, 2007 at the [[Mojave Spaceport]]]]
[[File:Xcor-rocketracer-N216MR-071029-37cr-16.jpg|thumb|right|The Rocket Racer on landing roll-out at Mojave.]]
[[File:Xcor-rocketracer-N216MR-071029-39cr-16.jpg|thumb|right|Aft view of the Rocket Racer on landing roll-out at Mojave.]]

===Lynx rocketplane===
{{Details|Lynx (spacecraft)}}

The Lynx was planned to be capable of carrying a pilot and a passenger or payload on [[sub-orbital spaceflight]]s over {{convert|100|km}}.  Between 20 and 50 test flights of Lynx were planned, along with numerous static engine firings on the ground. A full step-by-step set of taxi tests, runway hops and full-up flights were planned to get the vehicle to a state of operational readiness. Lynx was envisaged to be roughly the size of a small private airplane. It would be capable of flying several times a day making use of reusable, non-toxic engines to help keep the space plane's operating costs low.<ref name="xcpr20080326">{{cite web|url=http://www.xcor.com/press-releases/2008/08-03-26_Lynx_suborbital_vehicle.html |title=XCOR Aerospace announces new suborbital vehicle &quot;Lynx&quot; to fly within two years |publisher=XCOR Aerospace |deadurl=yes |archiveurl=https://web.archive.org/web/20080509143030/http://www.xcor.com/press-releases/2008/08-03-26_Lynx_suborbital_vehicle.html |archivedate=May 9, 2008 }}</ref> The Lynx superseded a previous design, the [[Xerus (spaceplane)|Xerus spaceplane]].<ref>Foust, Jeff (31 March 2008) [http://www.thespacereview.com/article/1095/1 One size may not fit all] The Space Review, Retrieved 20 January 2015</ref>  The Lynx was initially announced in March 2008, with plans for an operational vehicle within two years.<ref name="xcpr20080326" /> That date slipped, first to 2012,<ref name="cur">{{cite web|url=http://spaceexperiencecuracao.com/blog/press-release-april-12-2011/|title=Countdown has begin for SXC|date=2011-04-12|quote=Early next year, we will make the first sub-orbital flight, after which the final development will speed up tremendously. According to our schedule, we will be ready for commercial take-off by the end of 2013|publisher=Space Expedition Curacao|accessdate=17 April 2011}}</ref> then to 2015<ref name="AviationWeek">Norris, Guy (8 October 2014) [http://aviationweek.com/awin-only/xcor-lynx-moves-final-assembly XCOR Lynx Moves Into Final Assembly] Aviation Week, Retrieved 20 January 2015</ref> and in January 2016 the company declined to give a projected date for the first test flight.<ref name="MRT20160317">{{cite web | title = XCOR officials refrain from disclosing date for Lynx test flights | publisher = Midland Reporter-Telegram  | date = January 17, 2016 | url = http://www.mrt.com/news/top_stories/article_0783b3da-bced-11e5-97bc-6f2e78081a3e.html |accessdate = March 8, 2016}}</ref>
The Mark II was projected fly twelve to eighteen months after the first test flight depending on how fast the prototype moved through the test program.<ref name="xcpr20080326" /><ref name="AviationWeek" />

As of 2012, XCOR had presold 175 Lynx flights at {{currency|95000|US}} each.<ref name=mrt20120707/>

As of 31 May 2016, XCOR has laid off a significant portion of its workforce and has placed the development of the Lynx Spacecraft on indefinite hold to focus on development of a rocket engine.<ref name=SpaceNews-2016-05-31>{{cite news |url= http://spacenews.com/xcor-lays-off-employees-to-focus-on-engine-development/ |title= XCOR lays off employees to focus on engine development |author= Jeff Foust |date= 31 May 2016 |publisher= SpaceNews }}</ref>

===Thermoplastic polymer development===

XCOR has developed Nonburnite, a cryo-compatible, inherently non-combustible composite material based on a [[thermoplastic]] [[polymer|fluoropolymer]] resin.  Low coefficient of thermal expansion and inherent resistance to microcracking make it well suited to cryogenic tank use and also part of vehicle structure.<ref>{{cite web | url = http://www.xcor.com/products/cryo_compatable_composites.html | title = XCOR Aerospace: Cyro Compatible Fluoropolymer Composite Material | publisher = XCOR Aerospace}}</ref>  {{asof|2012|02}}, Nonburnite will be used in the tanks of the Lynx rocketplane.<ref name=rlv20120202>{{cite news|title=A Perspective on XCOR’s Progress Towards Flying "Lynx"|url=http://hobbyspace.com/nucleus/?itemid=35388|accessdate=2012-02-13|newspaper=RLV and Space Transport News|date=2012-02-02}}</ref>

===XCOR/ULA liquid-hydrogen, upper-stage engine development project===

In March 2011, [[United Launch Alliance]] (ULA) announced they had entered into a joint-development contract with XCOR for a flight-ready, {{convert|25000|to(-)|30000|lbf|kN}} cryogenic [[LH2]]/[[LOX]] [[upper stage|upper-stage]] rocket engine.  Partially as a result of positive results achieved from an earlier (2010) effort to develop a new [[aluminium alloy]] engine nozzle using innovative manufacturing techniques, ULA believes the new engine technology will save several hundred pounds of weight from the large engine and will "lead to significantly lower-cost and more-capable commercial and US government space flights."<ref name=ula20110317>
{{cite web |title=XCOR and ULA Demonstrate Revolutionary Rocket Engine Nozzle Technology — also sign contract for Liquid Hydrogen Engine Development |url=http://www.ulalaunch.com/site/pages/News.shtml#/68/ |publisher=ULA |accessdate=2011-03-23 |date=2011-03-17}}</ref>

The "multi-year project’s main objective [was]<!-- edited for time context within the Wiki-prose; this was a 2011 objective; unclear where it is in 2015 --> to produce a flight-ready LOX/LH2 upper-stage engine in the {{convert|25000|to|30000|lbf|kN}}-thrust class that costs significantly less to produce and is easier to operate and integrate than competing engine technologies" <ref name=aw20110323>
{{cite news |last=Morring|first=Frank, Jr. |title=ULA, XCOR to Develop Upper-Stage Engine |url=http://www.aviationweek.com/aw/generic/story.jsp?id=news/awx/2011/03/22/awx_03_22_2011_p0-299850.xml&headline=ULA,%20XCOR%20to%20Develop%20Upper-Stage%20Engine&channel=space |accessdate=2011-03-25 |newspaper=Aviation Week |date=2011-03-23 |quote=''United Launch Alliance (ULA) and XCOR Aerospace are planning a joint effort to develop a low-cost upper-stage engine in the same class as the venerable RL-10…"}}</ref>

2011 demonstration test firings of an aluminum nozzle on XCOR’s Lynx 5K18 LOX/kerosene engine demonstrated "the ability of the aluminum nozzle to withstand the high temperatures of rocket-engine exhaust over numerous tests, with no discernable degradation of the material properties of the alloys.  The tests validated the design, materials and manufacturing processes used in the nozzle, and laid a foundation for scaling the design to EELV-sized engines."<ref name=xcor20110322>
{{cite web |title=XCOR and ULA Demonstrate Revolutionary Rocket Engine Nozzle Technology; Also Sign Contract for Liquid Hydrogen Engine Development |url=http://xcor.com/press-releases/2011/11-03-22_XCOR_and_ULA_demonstrate_rocket_engine_nozzle.html |work=press release |publisher=XCOR Aerospace |accessdate=2011-03-25 |date=2011-03-22 }}</ref>

{{asof 2011|lc=y}}, the length of the development program was stated to depend on "the level of investment as milestones are met in the ''build-a-little, test-a-little'' approach favored by XCOR."  If investment is minimized, flight engines would not be available for five to ten years.<ref name=aw20110323/>

A subscale {{convert|2500|lbf|kN}}-thrust LH2/LOX engine was developed by 2013, named the XR-5H25, in order to support the XCOR/ULA [[Rocket engine|engine]] development program.  The first hot fire test of the prototype engine was in November 2013, the test was deemed successful.<ref name=pa20131119>
{{cite news |last=Messier|first=Doug |title=XCOR Reaches Milestone in Liquid Hydrogen Engine Program |url=http://www.parabolicarc.com/2013/11/19/xcor-reaches-milestone-liquid-hydrogen-engine-program/ |accessdate=2013-11-20 |newspaper=Parabolic Arc |date=2013-11-19 }}</ref>  
It was also the first demonstrated use of a piston-[[Pump-fed engine|pump-fed]] LH2 rocket engine, a new method to design "liquid hydrogen rocket engines that fundamentally breaks current cost, reliability and operational models."<ref name=pa20131119/>

In April 2015, ULA announced that the XCOR/ULA joint-development rocket engine is one of the candidate engines for a new ULA upper stage rocket to be fielded no earlier than 2023.  The [[Advanced Cryogenic Evolved Stage]] (ACES) is a long-life-on-orbit, high-performance, upper stage that, after consideration/competition by ULA, will use one of three engines to go into production with.  The other engines under consideration are the {{convert|25000|lbf|kN|disp=flip|adj=on}}-[[Aerojet Rocketdyne]] [[RL-10]] and the {{convert|150000|lbf|kN|disp=flip|adj=on}}-[[Blue Origin]] [[BE-3]].  ACES will be a second stage for the [[Vulcan (rocket)|Vulcan]] launch vehicle after 2023, in lieu of the [[Centaur (rocket stage)|Centaur upper stage]] that is projected to fly on Vulcan as early as 2019.<ref name=sn20150413>
{{cite news |last1=Gruss|first1=Mike |url=http://spacenews.com/ulas-vulcan-rocket-to-be-rolled-out-in-stages/ |work=SpaceNews |title=ULA’s Vulcan Rocket To be Rolled out in Stages |date=2015-04-13 |accessdate=2015-04-19 }}</ref>

===Historical projects===
Completed projects have included:
* [[EZ-Rocket]], a [[Rutan Long-EZ]] homebuilt aircraft fitted with two '''XR-4A3''' {{convert|400|lbf|kN}} thrust rocket engines replacing the normal propeller engine.<ref name=xcor-loxalc>{{cite web |url=http://xcor.com/engines/LOX-alcohol_4A3.html |title=LOX-Alcohol Rocket Engine |website=www.xcor.com |publisher=XCOR Aerospace, Inc. |accessdate=2015-06-15}}</ref>  EZ-Rocket has been flown at numerous airshows including the [[Oshkosh Airshow|2005 Oshkosh Airshow]].<ref>[http://www.spacedaily.com/news/xprize-05b.html "XCOR To Fly EZ-Rocket At X Prize Cup Countdown", ''Space Daily'', August 19, 2005, accessed February 19, 2009]</ref><!-- there is a bit more information, with sources, on this rocketplane at [[Rocket_Racing_League#Predecessor_aircraft]]-->  EZ-Rocket was "the first rocket-powered aircraft built and flown by a non-governmental entity."<ref name=mrt20120707/>  On 3 December 2005, XCOR Aerospace flew its EZ-Rocket from [[Mojave, California]] to [[California City, California]], both in Kern County. Test pilot [[Dick Rutan]] made the flight, which lasted about 9 minutes and carried US mail from the post office in Mojave to addresses in California City. This was the first time that a manned, rocket-powered aircraft was used to carry U.S. Mail.
* Rocket Racer - The EZ-Rocket program led to a second [[rocketplane]] design for the [[Rocket Racing League]].  It was built on a [[Velocity SE]] airframe and later became known as the [[Rocket Racing League#Predecessor aircraft|Mark-I X-Racer]].  It was powered by an XCOR regeneratively cooled and pump-fed '''XR-4K14''' rocket engine.<ref name=xa20101227>
[http://www.xcor.com/products/index.html Products Overview], ''XCOR Aerospace'', undated, accessed 2010-12-27. ''"Twin 400 lb-thrust XR-4A3 engines aboard the EZ-Rocket"'' (with in-flight photograph) ... ''"Another engine that we have developed in parallel is the XR-4K14, ... a 1,500 lb thrust regeneratively cooled LOX and pump-fed kerosene system ... used as the Rocket Racer aircraft's main engine."''</ref>  This rocket-powered aircraft flew several demonstration flights at the 2008 [[EAA AirVenture Oshkosh]] [[air show]].<ref name=ut20090806>
[http://www.universetoday.com/2009/08/06/xcor-xracer-videos/ XCOR X-Racer], by Nancy Atkinson, ''[[Universe Today]]'', 2009-08-06, accessed 2010-04-26.</ref>  The total thrust for the single-engine Mark-I X-Racer has been variously reported as {{convert|1500|lbf|abbr=on}}<ref name=xa20101227/>  to {{convert|1800|lbf|abbr=on}},<ref name=ps20060215>[http://www.popsci.com/military-aviation-space/article/2006-02/x-racers-start-your-rockets  X-Racers, Start Your Rockets! : The creators of the X prize offer a sensational vision of rocket-powered airplanes speeding through the sky. But can their new racing league steal a bit of Nascar's thunder?], Michael Belfiore, ''[[Popular Science]]'' (feature cover story), 2006-02-15, accessed 2010-09-02.</ref> approximately twice that of the EZ-Rocket initial prototype.  The engine uses pressure-fed LOX and pump-fed kerosene, a combination that allows the fuel to be stored in the airplane's wing tanks while avoiding potential complications with pumping liquid oxygen.<ref>{{cite news|last=Gatlin |first=Allison |title=XCOR performance tested |publisher=Antelope Valley Press |date=2008-07-11 |url=http://www.avpress.com/n/11/0711_s5.hts |accessdate=2008-07-14 |deadurl=yes |archiveurl=https://web.archive.org/web/20090715125913/http://avpress.com/n/11/0711_s5.hts |archivedate=July 15, 2009 }}</ref>  After flight testing of the X-Racer was completed in 2008, XCOR completed seven [[rocketplane]] flights in one day.<ref name=mrt20120707/>
* '''Tea cart engine''', a 15&nbsp;lbf (67 N) thrust rocket motor burning [[nitrous oxide]] and [[ethane]], mounted on a small industrial cart.  The tea cart engine has repeatedly been fired indoors at conferences and demonstrations and had accumulated over 1,837 firings and 9,039 seconds of run time<ref>{{cite web | url = http://xcor.com/products/engines/2P1_N2O_ethane_rocket_engine.html | title = XCOR Aerospace - 15 lb-thrust nitrous oxide / ethane rocket engine | publisher = XCOR Aerospace}}</ref> by February 25, 2009.{{Citation needed|date=May 2010}}<!-- cannot locate a date for the source at the cited URL -->
* LOX/[[Liquid methane|methane]] rocket engines in testing in 2005.<ref name=xcor2015>
{{cite press |url=http://xcor.com/press-releases/2005/05-08-30_XCOR_completes_methane_rocket_engine.html |title=XCOR Aerospace Completes Successful Development of Methane Rocket Engine |date=2005-08-30 |publisher=XCOR Aerospace |accessdate=2012-12-03}}</ref>

* Early LOX/methane work led to a NASA contract, jointly with [[Alliant Techsystems|ATK]], to develop a {{convert|7500|lbf|abbr=on}} engine for potential use as the [[Orion (Constellation program)|CEV]] lunar return engine. On January 16, 2007 XCOR announced the successful test firing of a preliminary "workhorse" version of this engine.<ref name=xcor20070116>
{{cite press |url=http://www.xcor.com/press-releases/2007/07-01-16_XCOR_begins_methane_engine_testing.html |title=XCOR Aerospace Begins Test Firing of Methane Rocket Engine |date=2007-01-16 |publisher=XCOR Aerospace |accessdate=2012-12-03}}</ref>

== XCOR Space Expeditions ==

=== History ===
The Space Expedition Corporation (SXC) was founded by Harry van Hulten and Lt-Gen (ret.) Ben Droste in 2008. Initially operating under a wet-lease agreement with XCOR Aerospace, it was acquired by XCOR Aerospace in June 2014 and is now a fully owned subsidiary. Michiel Mol and Maarten Elshove joined SXC in 2010 and are thus considered founders. After the acquisition by XCOR Aerospace SXC has been rebranded as XCOR Space Expeditions and is fully integrated into the XCOR brand. XCOR Aerospace is since organized as part of XCOR Corporate, but operates as the center of XCOR.<ref>XCOR, http://xcor.com/news/xcor-aerospace-acquires-space-expedition-corporation/?p=spaceexpeditions</ref>

XCOR Space Expeditions is based in the Amsterdam area, the Netherlands, and has a regional sales office for Asia in Hong Kong.<ref>XCOR, http://xcor.com/contact/?p=spaceexpeditions</ref>

XCOR Space Expeditions works closely with a global network of 35 independent resellers, also called space agents, that sell tickets for a flight on the Lynx Mark I or Lynx Mark II to the public.<ref>XCOR, http://xcor.com/about-us/company-overview/?p=spaceexpeditions</ref>

=== Space Flights ===
XCOR Space Expeditions offers suborbital flights with the XCOR Lynx Mark I and Lynx Mark II Spaceships for the public. It will operate from Mojave, California, and Curaçao, where a spaceport is planned to be built.<ref>XCOR,  http://spaceexpeditions.xcor.com/the-spaceflight/spaceports/</ref> There is no official date set for the start of operation.

XCOR Space Expeditions also offers medical check ups, training missions with space simulators and G-Force Fighter Jet flights for ticketholders, among other preoperational missions and events.<ref>XCOR,  http://spaceexpeditions.xcor.com/the-spaceflight/space-programs/</ref>

Strategic partnerships have been created with global companies such as Philips, KLM, Heineken, Unilever and Luminox, among others.<ref>XCOR, http://xcor.com/about-us/partners/?p=spaceexpeditions</ref>

== XCOR Science ==
XCOR Science will offer payload flights for educational institutions and science to conduct experiments in space.<ref>XCOR, http://science.xcor.com/payloads/</ref>

The Lynx Mark I and Mark II allow a maximum payload of 140&nbsp;kg to be transported into space besides the pilot. Payloads are separated into two parts, whereas Payload A with a maximum capacity of 20&nbsp;kg is situated in a vessel behind the pilot seat. Payload B with a mass up to 120&nbsp;kg takes the space of the second seat.<ref name=XCOR-media-5886-2015002200001-xco-brochure-payloads-lr>XCOR, http://xcor.com/media/5886/2015002200001-xco-brochure-payloads-lr.pdf</ref>

It is planned in the future to develop the Lynx Mark III which will have the same capabilities as the Lynx Mark II and will fly up to 100&nbsp;km above ground. In addition to the payloads of the Lynx Mark I and II, the Lynx Mark III will have an external dorsal mounted pod which will hold up an extra 650&nbsp;kg.<ref name=XCOR-media-5886-2015002200001-xco-brochure-payloads-lr/>

Payloads will only be available from the Mojave space station in California.<ref name=XCOR-media-5886-2015002200001-xco-brochure-payloads-lr/>

==See also==
* [[Rocket mail]]
* [[X Prize Cup]]

==References==
{{Reflist|30em}}

==External links==
* [http://spaceexpeditions.xcor.com XCOR Space Expeditions website]
* [http://science.xcor.com XCOR Science website]
* [http://www.xcor.com XCOR Aerospace website]
* [http://www.rocketracingleague.com/ Rocket Racing League]
* [http://www.rocketshiptours.com/ RocketShip Tours]
* [http://www.ustream.tv/recorded/38256457 XCOR at CSF Members Meeting], Rick Searfoss, Chief Test Pilot (starts @1:22), ''Commercial Spaceflight Federation'', 4 Sep 2013.

{{Space tourism}}

[[Category:Mojave Air and Space Port]]
[[Category:Private spaceflight companies]]
[[Category:Rocket engine manufacturers]]
[[Category:Space access]]