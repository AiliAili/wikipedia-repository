{{Use dmy dates|date=June 2015}}
{{Use South African English|date=June 2015}}
{{infobox military award
| name             = Meritorious Service Medal (Cape of Good Hope)
| image            = [[File:Meritorious Service Medal (Cape of Good Hope) Victoria.jpg|300px]]
| caption          = 
| awarded_by       = [[Queen Victoria|the Monarch of the United Kingdom of Great Britain and Ireland, and Empress of India]]
| country          = [[File:Cape Colony flag.png|25px]] [[Cape Colony|Cape of Good Hope]]
| type             = Military long service medal
| eligibility      = Warrant officers and senior non-commissioned officers
| for              = 21 years meritorious service
| campaign         = 
| status           = Discontinued in 1913
| description      = 
| motto            = 
| clasps           = 
| post-nominals    = 
| established      = 1896
| first_award      = 
| last_award       = 
| total            = 
| posthumous       = 
| recipients       = 
| precedence_label = Order of wear
| individual       = 
| higher           = [[File:Flag of the United Kingdom.svg|25px]] Queen Elizabeth II’s Long and Faithful Service Medal
| same             = [[File:Flag of the United Kingdom.svg|25px]] [[Meritorious Service Medal (United Kingdom)]]<br>[[File:BlueEnsignNatal.png|25px]] [[Meritorious Service Medal (Natal)]]<br>[[File:Flag of New Zealand.svg|25px]] [[Meritorious Service Medal (New Zealand)]]<br>[[File:Red Ensign of South Africa 1912-1928.svg|25px]] [[Meritorious Service Medal (South Africa)]]
| lower            = [[File:Flag of the United Kingdom.svg|25px]] [[Accumulated Campaign Service Medal]]
| related          = 
| image2           = [[File:Ribbon - Army Long Service and Good Conduct Medal (Cape).png|x29px]]
| caption2         = Ribbon Bar
}}

In May 1895, Queen Victoria authorised Colonial governments to adopt various British military medals and to award them to members of their local permanent military forces. The [[Cape Colony|Cape of Good Hope]] introduced this system in September 1895 and, in 1896, instituted the '''Meritorious Service Medal (Cape of Good Hope)'''.<ref name="Colonial">[http://www.geocities.ws/militaf/mil94.htm South African Medal Website – Colonial Military Forces] (Accessed 6 May 2015)</ref>

The medal is a distinctive Colonial version of the British [[Meritorious Service Medal (United Kingdom)|Meritorious Service Medal]]. It was coupled to a Meritorious Service Annuity and was awarded in limited numbers, usually upon retirement, to selected warrant officers and senior non-commissioned officers of the Permanent Force of the Cape of Good Hope who had completed twenty-one years of meritorious service.<ref name="Colonial"/>

==Origin==
The United Kingdom's Meritorious Service Medal was instituted by [[Queen Victoria]] on 19 December 1845 to recognise meritorious service by senior [[non-commissioned officer]]s of the British Army. The medal was initially only awarded to sergeants but, on 10 June 1884, eligibility was extended to all soldiers above the rank of Corporal. Recipients were also granted a [[Annuity (finance theory)|Meritorious Service Annuity]], the amount of which was based on rank.<ref name="Gazette33700">{{londongazette|issue=33700|startpage=1893|date=20 March 1931}}</ref><ref name="StratfordMSM">[http://www.stephen-stratford.co.uk/msm.htm Stephen Stratford Medals site - British Military & Criminal History - 1900 to 1999 - Army MSM] (Accessed 20 June 2015)</ref>

Upon its institution in 1845, a sum of £2,000 of public money was made available annually to grant Meritorious Service Annuities of £20 each to sergeants of the Regular Army as a reward for good, faithful and efficient service. This allowed for a maximum of 100 [[British Army]] soldiers to be in receipt of the annuity at any time and, since those selected to receive the annuity were also awarded the Meritorious Service Medal, the medal was awarded sparingly. Even though the budgeted amount for these annuities was increased from time to time, further awards of the medal and annuity were therefore restricted to only those registered candidates, recommended by their commanding officers, who were selected by the Commander-in-Chief of the Army from a list as those whom he considered to be the most deserving to receive any annuities which may have fallen vacant.<ref name="Gazette33700"/><ref name="StratfordMSM"/>

==Cape Colonial Forces==
In the late 19th century, the military forces of the [[Postage stamps and postal history of the Cape of Good Hope|Cape of Good Hope]], [[colloquialism|colloquially]] also known as the [[Cape Colony]], consisted of three separate military organisations, the permanent para-military Frontier Armed and Mounted Police and two part-time forces, the Burgher Force and the Volunteer Force, both district-based. These [[Cape Colonial Forces]] were established in 1855, after the United Kingdom granted the Cape of Good Hope representative government in 1853.<ref name="Colonial"/><ref name="GoodHope">[http://www-apps.crl.edu/collections/topics/official-gazettes/search?countryID=CAPE%20OF%20GOOD%20HOPE%20%28BRITISH%20COLONY%29&pc=1 Center for Research Libraries - Official Gazettes Search - Results for: Cape of Good Hope (British Colony)] (Accessed 26 May 2015)</ref>

==Adoption==
On 31 May 1895, Queen Victoria authorised Dominion and Colonial governments to adopt the Meritorious Service Medal, as well as the [[Distinguished Conduct Medal]] and the [[Army Long Service and Good Conduct Medal]], and to award them to permanent members of their local military forces. The Cape of Good Hope introduced this system in September 1895 and the Meritorious Service Medal (Cape of Good Hope) was instituted by Royal Warrant on 29 September 1896.<ref name="Colonial"/><ref name="StratfordMSM"/><ref name="JustDone">[http://www.justdone.co.za/SAMS/MedalView.php?MedalNo=1&MedalID=70 Just Done Productions Medal View - Meritorious Service Medal (1896)] (Access date 22 June 2015)</ref>

The Cape medal was identical to the British medal on the obverse, but had the additional inscription "CAPE OF GOOD HOPE" on the reverse. Other territories which took advantage of the authorisation include Canada, India, Natal, New South Wales, New Zealand, Queensland, South Australia, Tasmania, Victoria and, from 1901, the [[History of Australia#Federation|Commonwealth of Australia]]. Second versions of most of these medals, with the effigy of King Edward VII on the obverse, were introduced after his succession to the throne in 1901.<ref name="New ZealandMSM">[http://medals.nzdf.mil.nz/category/e/e1.html New Zealand Defence Force – New Zealand Long Service and Good Conduct Medals - The New Zealand Meritorious Service Medal] (Access date 21 June 2015)</ref><ref name="Australia">[http://www.itsanhonour.gov.au/honours/awards/imperial.cfm#msm_0275 Australian Government - It's an Honour - Imperial Awards - Meritorious Service Medal (1902-1975)] (Access date 21 June 2015)</ref>

==Award criteria==
Recipients of the Meritorious Service Medal (Cape of Good Hope) were usually already holders of the [[Army Long Service and Good Conduct Medal (Cape of Good Hope)]]. The medal and annuity were awarded only to selected candidates upon retirement as a reward after long and valuable service, upon recommendation by their commanding officers and selected from a list by the Commander-in-Chief of the Cape Colonial Forces, the Governor of the Cape of Good Hope.<ref name="Colonial"/><ref name="Gazette33700"/>

==Order of wear==
In the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], the Meritorious Service Medal (Cape of Good Hope) ranks on par with the United Kingdom's Meritorious Service Medal. It takes precedence after Queen Elizabeth II’s Long and Faithful Service Medal and before the [[Accumulated Campaign Service Medal]].<ref name="London Gazette"/>

===South Africa===
{{main|South African military decorations order of wear#Order of wear}}
With effect from 6 April 1952, when a new South African set of decorations and medals was instituted to replace the British awards used to date, the older British decorations and medals applicable to South Africa continued to be worn in the same order of precedence but, with the exception of the [[Victoria Cross]], took precedence after all South African orders, decorations and medals awarded to South Africans on or after that date. Of the official British medals which were applicable to South Africans, the Meritorious Service Medal (Cape of Good Hope) takes precedence as shown.<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3352|date=17 March 2003}}</ref><ref name="Notice1982">Government Notice no. 1982 of 1 October 1954 - ''Order of Precedence of Orders, Decorations and Medals'', published in the Government Gazette of 1 October 1954.</ref><ref name="Gazette 27376">Republic of South Africa Government Gazette Vol. 477, no. 27376, Pretoria, 11 March 2005, {{OCLC|72827981}}</ref>

[[File:Ribbon - Meritorious Service Medal (UK).png|x37px|Meritorious Service Medal (United Kingdom)]] [[File:Ribbon - Army Long Service and Good Conduct Medal (Cape).png|x37px|Meritorious Service Medal (Cape of Good Hope)]] [[File:Ribbon - Medal for Long Service and Good Conduct (Natal).png|x37px|Meritorious Service Medal (Natal)]]
* Preceded by the [[Meritorious Service Medal (United Kingdom)]] (MSM).
* Succeeded by the [[Meritorious Service Medal (Natal)]].

==Description==
The medal was struck in silver and is a disk, 36 millimetres in diameter and with a raised rim on both sides. The suspender is an ornamented scroll pattern swiveling type, affixed to the medal by means of a claw and a horizontal pin through the upper edge of the medal. On the Queen Victoria version, the suspender mount is a double-toe claw, while the King Edward VII version has a single-toe claw.<ref name="StratfordMSM"/>

;Obverse
The obverse of the first version of the medal bears the effigy of Queen Victoria, circumscribed "VICTORIA REGINA ET IMPERATRIX". The King Edward VII version has the effigy of the King in Field Marshal's uniform, circumscribed "EDWARDVS VII REX IMPERATOR".<ref name="Gazette33700"/><ref name="StratfordMSM"/>

;Reverse
The reverse of both versions have the words "FOR MERITORIOUS SERVICE" in three lines, encircled by a laurel wreath and surmounted by the Imperial Crown. At the top, above the crown and wreath, it is inscribed "CAPE OF GOOD HOPE" in a curved line.<ref name="Gazette33700"/><ref name="JustDone"/>

;Ribbon
The ribbon is that of the Army Long Service and Good Conduct Medal (Cape of Good Hope). While the ribbon of the British Meritorious Service Medal was plain crimson until mid-1916, the ribbon of the Cape of Good Hope medal is 32 millimetres wide and crimson, with a 4 millimetres wide yellow band in the centre.<ref name="Colonial"/><ref name="StratfordMSM"/>

==Discontinuation==
Of the four Colonies which were to form the Union of South Africa in 1910, the Cape of Good Hope and the [[Colony of Natal]] adopted their own territorial versions of the Meritorious Service Medal. The award of these medals was discontinued after the [[Union of South Africa]] was established in 1910. Once the [[Union Defence Force (South Africa)|Union Defence Forces]] were established in 1912, the Union began to award the [[Meritorious Service Medal (South Africa)]].<ref name="Colonial"/><ref name="Union1913">[http://www.geocities.ws/militaf/mil13.htm South African Medal Website – Union Defence Forces (1913–1939)] (Accessed 9 May 2015)</ref>

==References==
{{Reflist|30em}}
{{South African military decorations and medals}}
{{Efficiency and long service decorations and medals}}

[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|403.1]]
[[Category:Military decorations and medals of South Africa pre-1952]]
[[Category:Long and Meritorious Service Medals of Britain and the Commonwealth]]