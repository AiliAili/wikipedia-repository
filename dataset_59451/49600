{{Other uses|The Damned Thing (disambiguation){{!}}The Damned Thing}}
"'''The Damned Thing'''" is a [[horror literature|horror]] [[short story]] written by [[Ambrose Bierce]]. It first appeared in ''Tales from [[Town Topics (magazine)|New York Town Topics]]'' on December 7, 1893.<ref>{{Cite web|url=http://www.ambrosebierce.org/damned.htm|title="The Damned Thing"|website=www.ambrosebierce.org|access-date=2016-04-09}}</ref>

==Summary==
"The Damned Thing" is written in four parts, each with a comical subtitle. The story begins in Hugh Morgan's cabin, where local men have gathered around the battered corpse of Hugh Morgan to hold an [[inquest]] concerning his death. William Harker, a witness to the death, enters and is sworn in by the [[coroner]] to relate the circumstances. William reads a prepared statement about a hunting and fishing outing undertaken with Morgan. He and Morgan encountered a series of disturbances that Morgan referred to as "that damned thing". During the last encounter, Morgan fired his gun in fear, then fell to the ground and cried out in mortal agony. Harker saw his companion moving violently and erratically, while shouting and making disturbing cries. He thought Morgan was having convulsions because he didn't appear to be under attack. By the time Harker reached Morgan, Morgan was dead.

The coroner states that Morgan's diary contains no evidence in the matter of his death. A juror implies that Harker's testimony is symptomatic of insanity, and Harker leaves the inquest in anger. The jury concludes that Morgan was killed by a [[mountain lion]].

The story becomes [[epistolary novel|epistolary]] in nature, detailing entries from Morgan's diary. The journal covers the events leading up to Morgan's death as he becomes aware of an invisible creature that he is hunting.  He reasons out that it lacks color or has a color that renders it invisible but to make sure he is not crazy, he plans on inviting Harker with him when he hunts "the damned thing".  

== Analysis ==
Fighting [[invisibility|invisible]] monsters is a classic horror trope that may be traced to the invisible supernatural entities in [[Fitz James O'Brien|O'Brien]]'s ''[[What Was It?]]'' (1859) and [[Guy de Maupassant]]'s ''[[Le Horla]]'' (1887).<ref>{{Cite book|url=https://books.google.com/books?id=Uly8AgAAQBAJ|title=The Ashgate Encyclopedia of Literary and Cinematic Monsters|last=Weinstock|first=Professor Jeffrey Andrew|date=2014-02-28|publisher=Ashgate Publishing, Ltd.|isbn=9781472400604|language=en}}</ref> Later examples of invisibility in 19th-century fiction include ''[[The Plattner Story]]'' and ''[[The Invisible Man]]'' by [[H. G. Wells]]. 

In his take on the issue of invisibility, Bierce chose to "foreground the limitations of human senses",<ref>{{Cite book|url=https://books.google.com/books?id=Uly8AgAAQBAJ|title=The Ashgate Encyclopedia of Literary and Cinematic Monsters|last=Weinstock|first=Professor Jeffrey Andrew|date=2014-02-28|publisher=Ashgate Publishing, Ltd.|isbn=9781472400604|language=en}}</ref> speculating that in the course of evolution an  animal might have arisen whose color is invisible to the human eye. When accused of plagiarizing O'Brien, Bierce retorted that O'Brien's monster was "supernatural and impossible", whereas he described "a wild animal that cannot be seen, because, although opaque, like other animals, it is of invisible color".<ref>{{Cite book|url=https://books.google.com/books?id=9OHKUWhVPP0C|title=A Sole Survivor: Bits of Autobiography|last=Bierce|first=Ambrose|last2=Joshi|first2=S. T.|last3=Schultz|first3=David E.|date=1998-01-01|publisher=Univ. of Tennessee Press|isbn=9781572330184|language=en}}</ref> As a result, "The Damned Thing" has been classed as [[science fiction]] rather than as a Gothic narrative.<ref>See, e.g.: ''The Road to Science Fiction: From Gilgamesh to Wells'' (ed. James Gunn). Vol. 1. Rowman & Littlefield, 2002. ISBN 9780810844148. P. 305.</ref> 

Bierce's quasi-scientific arguments for invisibility of certain creatures were later developed by [[H. P. Lovecraft]] in ''[[The Colour Out of Space]]''.<ref>Hughes, William. ''Historical Dictionary of Gothic Literature''. Rowman & Littlefield, 2013. ISBN 9780810872288. P. 40.</ref> In Lovecraft's story ''[[The Unnamable (short story)|The Unnamable]]'', [[Randolph Carter]] is attacked by "some unseen entity of titanic size but undetermined nature".<ref>''Lovecraft and Influence: His Predecessors and Successors'' (ed. Robert H. Waugh). ISBN 9780810891166. P. 29.</ref>

==TV adaptations==
In 1975, [[SFRY|Yugoslav]] director Branko Pleša made a TV movie entitled ''Prokletinja'' ([[Serbo-Croatian language|Serbo-Croatian]] for "The Damned Thing") based on the story.<ref>[http://www.imdb.com/title/tt0200981/ ''Prokletinja'' at IMDb]</ref>

"The Damned Thing" was very loosely adapted into a [[The Damned Thing|film of the same name]] as part of the television series ''[[Masters of Horror]]''.<ref>{{cite web|title=Masters of Horror: The Damned Thing|url=http://www.imdb.com/title/tt0805419/|work=IMDb|accessdate=19 January 2013}}</ref> It was inspired by Ambrose Bierce's short story and was directed by [[Tobe Hooper]] and written by [[Richard Christian Matheson]].  The TV adaptation focuses on an invisible force wreaking havoc on a man's family and town that forces the town members to kill one other and themselves. 

== References ==
{{reflist}}

==External links==
{{wikisource|The Damned Thing}}
* [http://www.gutenberg.org/ebooks/23172 Free Gutenberg Editions] of this story.
* {{librivox book | title=The Damned Thing | author=Ambrose Bierce}}

{{Ambrose Bierce}}

{{DEFAULTSORT:Damned Thing, The}}
[[Category:1893 short stories]]
[[Category:Short stories by Ambrose Bierce]]
[[Category:Horror short stories]]
[[Category:Science fiction short stories]]
[[Category:Invisibility in fiction]]