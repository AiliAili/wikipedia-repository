{{Use British English|date=February 2016}}
{{Use dmy dates|date=April 2012}}
{{Infobox peer
| name          = Fitzroy Richard Somerset, Baron Raglan
| image         = Raglanlarge.jpg
| caption       = Lord Raglan in front of [[Raglan Castle]]
| birth_name    = Fitzroy Richard Somerset
| birth_date    = {{Birth date|1885|06|10|df=y}}
| birth_place   = 12 Albert Mansions, Victoria Street, Westminster
| death_date    = {{Death date and age|df=yes|1964|09|14|1885|06|10}}
| death_place   = 
| body_discovered = 
| resting_place = 
| resting_place_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| title         = 
| tenure        = 
| other_titles  = 
| residence     = [[Cefntilla Court]]
| nationality   = 
| locality      =  [[Llandenny]], [[Monmouthshire]], Wales
| other_names   = 
| wars_and_battles = 
| offices       = 
| networth      = 
| known_for     = Soldier<br/>Author<br/>Amateur Anthropologist
| years_active  =
| predecessor   = [[George Somerset, 3rd Baron Raglan]]
| heir          = 
| successor     = [[FitzRoy Somerset, 5th Baron Raglan|FitzRoy John Somerset, 5th Baron Raglan]]
| spouse        = Julia Hamilton (1923–1964)
| issue         = {{hlist|Fitzroy Somerset|Janetta Somerset|FitzRoy John Somerset, 5th Baron Raglan|Geoffrey Somerset, 6th Baron Raglan|Cecily Somerset}}
| parents       = [[George Somerset, 3rd Baron Raglan]]<br/>Ethel Jemima Ponsonby      
| occupation  =
| signature     = 
| footnotes     = 
| misc          = 
}}
'''Fitzroy Richard Somerset, 4th Baron Raglan''' (10 June 1885 – 14 September 1964) was a British soldier, author, and amateur [[anthropologist]]. He is best known for his book ''The Hero, A Study in Tradition, Myth and Drama'' in which he demonstrates that the life stories of many figures from myth and legend follow a common pattern of events, supporting his thesis that this pattern is drawn from the narrative of a ritual drama.

==Life==
FitzRoy Richard Somerset, heir to the peerage title [[Baron Raglan]], was born on 10 June 1885 to [[George Somerset, 3rd Baron Raglan|George Fitzroy Henry Somerset]], 3rd Baron Raglan and his wife Lady Ethel Jemima Ponsonby.<ref name="OxBio">{{cite book|last1=Miller|first1=Dean A.|title=FitzRoy Richard, fourth Baron Raglan (1885–1964)|date=2004|publisher=Oxford University Press|edition=Oxford Dictionary of National Biography|url=http://www.oxforddnb.com/view/article/75510|accessdate=27 January 2016}}</ref> He was educated at [[Eton College|Eton]] and the [[Royal Military College, Sandhurst]], and received a commission as [[Second lieutenant]] in the [[Militia (United Kingdom)|Militia]] regiment the [[Royal Monmouthshire Royal Engineers]] on 10 June 1902.<ref>{{LondonGazette |issue=27441 |supp= |startpage=3755 |date=10 June 1902}}</ref> In 1905 he entered the British Army and was commissioned in the [[Grenadier Guards]]. His military career included working as an aide-de-camp to the governor of Hong Kong, service in the Egyptian army from 1913 to 1919, district commissioner in [[Sudan]] and as a political officer in [[Mandatory Palestine|Palestine]] and Transjordan. In recognition of his services in Egypt he was made an Officer of the [[Order of the Nile]].<ref name=WelshBio>{{cite web|title=Dictionary of Welsh Biography|url=http://yba.llgc.org.uk/en/s2-SOME-RIC-1885.html?query=fitzroy+richard+somerset&field=name|website=The National Library of Wales|accessdate=30 January 2016}}</ref> He retired from the military in 1922 with the rank of [[major]].<ref name=Hero>{{cite book|last1=Somerset|first1=Fitzroy|title=The Hero: A Study in Tradition, Myth and Drama|publisher=Dover Publications|isbn=978-0486427089|page="About the Author"}}<!--|accessdate=30 January 2016--></ref>

With the death of his father in 1921, he assumed the title 4th Baron Raglan and, after retiring from the army, returned to his ancestral home, [[Cefntilla Court]] near [[Usk]] in [[Monmouthshire]]. He was very active in local affairs. He was a Justice of the Peace for the county as early as 1909 and served for twenty-one years (1928–49) as a member of the former Monmouthshire county council . He took a great interest in the Boy Scout movement and was county commissioner for Monmouthshire for twenty seven years (1927–54). He served as  [[Lord Lieutenant of Monmouthshire]] from 1942 until 1964.<ref name="OxBio"/>

During his life he studied and wrote on topics in areas such as anthropology, political science, and architecture. His interest in the antiquities of Monmouthshire led him to write, with Sir Cyril Fox, three volumes on the county's medieval and later domestic architecture. In 1933 he became president of Section H (Anthropology) of the British Association for the Advancement of Science, and from 1945 to 1947 he served as president of the Folklore Society .  He was chairman of the art and archaeology committee of the National Museum of Wales (1949–51) and president of the National Museum of Wales from 1957 to 1962. He was also president of the Royal Anthropological Institute from 1955 to 1957.<ref name= "WelshBio"/>

On 9 April 1923 Raglan married Julia Hamilton (7 January 1901 &ndash; 17 April 1971), daughter of [[Lieutenant Colonel|Lt.-Col.]] [[Robert Hamilton-Udny, 11th Lord Belhaven and Stenton]] by his marriage to Kathleen Gonville Bromhead. The Lord and Lady Raglan had five children, the first of whom died a few days after birth.  Julia, Lady Raglan also contributed to the study of folklore. In an article in the journal ''Folklore'' in 1939, she coined the term "Green Man" to describe the foliate heads found in English churches. Her theory on their origin is still debated.<ref>{{cite web |url=http://www.greenmanenigma.com/theories.html |title= : Theories and Interpretations|last1= |first1= |last2= |first2= |date= |website= The Enigma of the Green Man|publisher= |access-date= 24 August 2016}}</ref>  

Lord Raglan was also the source of various controversies over the course of his life. In 1938 he declared his wish to give up his job at the Ministry of Information on the grounds that he was not doing enough work to justify his salary. In 1958 he agitated Welsh nationalist feelings by declaring Welsh a ‘moribund’ language. Demands were made for his resignation from the National Museum of Wales, but he stood fast. (The motto of the Raglan barony is ''Mutare vel timere sperno'': ‘I scorn to change or to fear’).<ref name="OxBio"/><ref>{{cite web|title=Raglan Family Crest, Coat of Arms, and Family History|url=https://www.houseofnames.com/ragland-family-crest|website=House of Names|accessdate=31 January 2016}}</ref>

Lord Raglan died on 14 September 1964<ref>{{cite news|title=Lord Raglan, Soldier, Author, Anthropologist|url=https://news.google.com/newspapers?id=-mxAAAAAIBAJ&sjid=qaMMAAAAIBAJ&pg=2239%2C2556383|accessdate=27 January 2016|issue=182nd year – No. 200|publisher=The Glasgow Herald|date=15 September 1964}}</ref> at age 79 and was buried in the family plot in the [[Church of St John, Llandenny]].

==Literary works==
Lord Raglan was not only an active member of many societies and interested in administrative duties in national institutions but also published a number of books and papers on archaeology and anthropology. His first book, ''Jocasta's Crime – An Anthropological Study'', a study of incest and incest taboos, was published in 1933. He followed with ''The Science of Peace'',  a work on the origin, development, and prevention of war.
  
Lord Raglan's most enduring work, ''The Hero, a Study in Tradition, Myth and Drama'' was published in 1936. The book's central thesis is that [[hero]] figures of [[mythology]] had their origin in ritual [[drama]], not historical fact. In the book's most influential chapter, he outlined [[Rank-Raglan_mythotype|22 common traits]] of god-heroes which he called the "[[Hero#Mythic hero archetype|mythic hero archetype]]". Raglan then encapsulates the lives of several heroes and awards points (marks) for thematic elements for a possible score of 22.  He dissects [[Oedipus]], [[Theseus]], [[Romulus]], [[Heracles]], [[Perseus]], [[Jason]], [[Bellerophon]], [[Pelops]], [[Asclepios]], [[Dionysos]], [[Apollo]], [[Zeus]], [[Joseph (son of Jacob)|Joseph]], [[Moses]], [[Elijah]], [[Watu Gunung]], [[Juok|Nyikang]], [[Sigurd]] or Siegfried, [[Lleu Llaw Gyffes|Llew Llawgyffes]], [[Arthur]], and [[Robin Hood]]. Oedipus earns the highest score with 21 marks.

Significantly, Raglan excludes [[Jesus]] from the study, even though Jesus matched a number of traits in lord Raglan.<ref>{{cite web|title=Life events shared by Yeshua (Jesus) and the "Mythic Hero Archetype"|url=http://www.religioustolerance.org/chr_jcpa4.htm|website=ReligiousTolerance.org|accessdate=1 February 2016}}</ref> Raglan later claimed to omit Jesus to avoid conflict with his original publisher.

==Ancestry==
{{ahnentafel top|width=100%}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''FitzRoy Somerset, 4th Baron Raglan'''
|2= 2. [[George Somerset, 3rd Baron Raglan]]
|3= 3. Lady Ethel Ponsonby
|4= 4. [[Richard Somerset, 2nd Baron Raglan]]
|5= 5. Lady Georgina Lygon
|6= 6. [[Walter Ponsonby, 7th Earl of Bessborough]]
|7= 7. Lady Louisa Eliot
|8= 8. [[FitzRoy Somerset, 1st Baron Raglan]]
|9= 9. Lady Emily Wellesley-Pole
|10= 10. [[Henry Lygon, 4th Earl Beauchamp]]
|11= 11. Lady Susan Eliot
|12= 12. [[John Ponsonby, 4th Earl of Bessborough]]
|13= 13. Lady Maria Fane
|14= 14. [[Edward Eliot, 3rd Earl of St Germans]]
|15= 15. Lady Jemima Cornwallis
|16= 16. [[Henry Somerset, 5th Duke of Beaufort]]
|17= 17. Elizabeth Boscawen
|18= 18. [[William Wellesley-Pole, 3rd Earl of Mornington]]
|19= 19. Katherine Forbes
|20= 20. [[William Lygon, 1st Earl Beauchamp]]
|21= 21. Catherine Denn
|22= 22. [[William Eliot, 2nd Earl of St Germans]]
|23= 23. Lady Georgiana Leveson-Gower
|24= 24. [[Frederick Ponsonby, 3rd Earl of Bessborough]]
|25= 25. [[Henrietta Ponsonby, Countess of Bessborough|Lady Henrietta Spencer]]
|26= 26. [[John Fane, 10th Earl of Westmorland]]
|27= 27. [[Sarah Fane, Countess of Westmorland|Sarah Child]]
|28= 28. [[William Eliot, 2nd Earl of St Germans]] (= 22.)
|29= 29. Lady Georgiana Leveson-Gower (= 23.)
|30= 30. [[Charles Cornwallis, 2nd Marquess Cornwallis]]
|31= 31. Lady Louisa Gordon
}}</center>
{{Ahnentafel bottom}}

==Bibliography==
*''Jocasta's Crime: An Anthropological Study'', Methuen (London), 1933, Fertig (New York, NY), 1991
*''The Science of Peace'', Methuen, 1933, reprinted by Pierides Press, 2007 ISBN 978-1406789171
*''If I Were Dictator'', Methuen, 1934 (contributor)
*''The Hero: A Study in Tradition, Myth and Drama'', Methuen, 1936, reprinted by Dover Publication, 2011 ISBN 978-0486427089
*''How Came Civilisation?'', Methuen, 1939
*''Death and Rebirth'', C. A. Watts, 1945
*''The Origins of Religion'', C. A. Watts, 1949
*''(With Cyril Fox) Monmouthshire Houses, Parts I-III'', National Museum of Wales, 1951–54 ISBN 978-0720003987
*''The Temple and the House'', Routledge & Kegan Paul, 1964, Norton (New York, NY), 1965

==References==
{{Reflist|30em}}

==External links==
*[http://department.monm.edu/classics/courses/Clas230/MythDocuments/HeroPattern/default.htm Lord Raglan's Scale] The 22 points are applied to other heroes such as [[Beowulf]] and [[Harry Potter]].
*[http://yba.llgc.org.uk/en/s2-SOME-RIC-1885.html?query=Usk&field=content FitzRoy Somerset on National Library of Wales ''[[Dictionary of Welsh Biography]]'']
*[http://www.genealogics.org/pedigree.php?personID=I00082032&tree=LEO Pedigree at Genealogics]

{{s-start}}
{{s-hon}}
{{succession box | before=[[Sir Henry Mather-Jackson, 3rd Baronet|Sir Henry Mather-Jackson, Bt]] | title=[[Lord Lieutenant of Monmouthshire]] | years=1942–1964 | after=[[Edward Roderick Hill]]}}
{{s-reg|uk}}
{{succession box | title=[[Baron Raglan]] | before= [[George Somerset, 3rd Baron Raglan|George Fitzroy Henry Somerset]] | after=[[FitzRoy Somerset, 5th Baron Raglan|FitzRoy Somerset]] | years=1921–1964}}
{{s-end}}
{{Authority control}}

{{DEFAULTSORT:Raglan, Fitzroy Somerset, 4th Baron}}
[[Category:1885 births]]
[[Category:1964 deaths]]
[[Category:Somerset family|FitzRoy Somerset, 4th Baron Raglan]]
[[Category:Barons in the Peerage of the United Kingdom]]
[[Category:British anthropologists]]
[[Category:British beekeepers]]
[[Category:Graduates of the Royal Military College, Sandhurst]]
[[Category:Grenadier Guards officers]]
[[Category:British non-fiction writers]]
[[Category:Lord-Lieutenants of Monmouthshire]]
[[Category:People educated at Eton College]]
[[Category:British male writers]]