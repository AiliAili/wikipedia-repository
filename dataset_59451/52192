{{primarysources|date=November 2010}}

The '''Lithic Studies Society''' (LSS) was founded in 1979 to advance knowledge of, and education and research in, [[stone tool|lithic]] studies. The Society's members have diverse interests, spanning [[Paleolithic|Palaeolithic]] to historic periods across many areas of the world. The Society provides a convivial forum for the exchange of ideas and information and produces. It regularly holds lectures, day meetings, conferences and field trips, publishes an annual peer-reviewed journal (''Lithics'') and occasional thematic volumes. Additionally the society promotes the highest standards of lithics research and reporting, and advocates and contributes to policies relevant to lithic studies.

== ''Lithics'' ==
{{Infobox journal
| title         = Lithics
| cover         = 
| editor        = 
| discipline    = 
| peer-reviewed = 
| language      = 
| former_names  = 
| abbreviation  = 
| publisher     = 
| country       = 
| frequency     = 
| history       = 
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 477855038
| LCCN          = 
| CODEN         = 
| ISSN          = 	0262-7817
| eISSN         = 
| boxwidth      = 
}}
''Lithics'' is the Society's annual, [[peer review|peer-reviewed]] [[academic journal|journal]]. It is devoted to publishing research which enhances our understanding of past societies through the study of [[stone tool]]s. Published in the spring of each year, ''Lithics'' contains research articles, as well as shorter communications, book reviews, a bibliography of recent publications relevant to lithic studies, and news of the Society's related activities.

== Occasional Papers ==
The society has to date published six Occasional Papers:
* 1988 ''The Illustration of Lithic Artefacts''
* 1994 ''Stories in Stone''
* 1995 ''Lithics in Context''
* 1998 ''Stone Age Archaeology''
* 2001 ''Palaeolithic Archaeology of the Solent River''
* 2004 ''Lithics in Action''

== Wymer Bursary ==

To commemorate [[John Wymer]] and his contribution to [[archaeology]], the Society created the John Wymer Bursary in 2007. It is awarded annually to support any individual to further an interest in lithic-related study. The value of the bursary is presently £250.

Applications for the bursary are open to students, academics, others professionally engaged in lithic study, and those simply pursuing an interest in [[stone tools|lithics]] as a hobby. The bursary may be used towards [[excavation (archaeology)|excavation]] expenses, [[fieldwork]], study of collections, and participation in a conference or travel, but a case may be made for other uses. The bursary can be used to support activities in the UK or abroad.

== External links ==
* [http://www.lithics.org Lithic Studies Society]
* [http://www.independent.co.uk/news/obituaries/john-wymer-470210.html John Wymer obituary]

[[Category:Archaeological professional associations]]
[[Category:Organizations established in 1979]]