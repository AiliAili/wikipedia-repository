<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=45-80 Sekani
 |image=Fairchild 45-80.jpg
 |caption=Fairchild 45-80 Sekani (CF-BHD c/n 101) c. 1937
}}{{Infobox Aircraft Type
 |type=Passenger/cargo transport
 |manufacturer=[[Fairchild Aircraft Ltd. (Canada)]]
 |designer=Nathan Floyd Vanderlipp
 |first flight=24 August 1937
 |introduced=
 |retired=
 |status=prototypes only
 |primary user=
 |more users=
 |produced=
 |number built=2
 |variants with their own articles=
}}
|}
The '''Fairchild 45-80 Sekani''' (named for [[Sekani|an indigenous people of Canada]]) was a [[Canada|Canadian]] twin-engined transport aircraft developed in Canada in the late 1930s. Although the 45-80 was the largest [[bush plane]] developed by Fairchild, its poor performance doomed the project, and nearly the company.<ref name= "p. 325"/>

==Design and development==
Designed by [[Fairchild (Canada)]] in the hope of attracting orders from the [[Royal Canadian Air Force]] and [[Canadian Airways]],the 45-80 was an ambitious project, being the largest design yet attempted.<ref>Milberry 1979, p. 110.</ref> The Sekani was a large sesquiplane of conventional configuration, with the sets of wings joined to the top and bottom of the fuselage and braced by N-struts. The lower set of wings were little more than stubs; their bracing to the upper wings passed through the engine nacelles (mounted on the upper wings), and they carried the pontoon undercarriage beneath them. Following typical Fairchild construction, the fuselage and empennage were made of fabric-covered welded steel tubing while the main wings were a combination of spruce spars and stainless steel ribs, also fabric covered. The "stub" wings were of heavy steel construction. The retractable undercarriage (a first for a Canadian designed aircraft) also featured streamlined fairings when the aircraft was equipped with skis.<ref>Molson and Taylor 1982, p. 324–325.</ref>

Testing commenced in August 1937 and revealed a number of serious deficiencies in the design, including that the aircraft was overweight and impossible to control directionally when flying on only one engine as the ailerons caused severe drag tending to turn the aircraft in the opposite direction.<ref name= "p. 325">Molson and Taylor 1982, p. 325.</ref>

==Operational history==
The RCAF tested the 45-80 in October 1937, investigating its potential as a photographic platform, but ultimately rejecting it. Canadian Airways tests showed that the new design had a payload capacity no greater than the single-engine types that they were already operating. A second prototype was built, but the whole project was soon abandoned with the completed prototypes along with three unassembled production examples being scrapped.<ref name= "p. 325"/>

Fairchild recovered from the financial disaster that was looming after the Sekani's cancellation when the company landed a lucrative contract to build [[Bristol Blenheim|Bristol Bolingbroke]] bombers as part of the shadow factory system that was set up to supply [[Royal Air Force|RAF]] needs.

<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{aerospecs
|ref=''Canadian Aircraft Since 1909'' <ref>Molson and Tayler 1982, p. 326.</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew=Two pilots
|capacity=12 passengers
|length m=11.94
|length ft=39
|length in=2
|span m=17.27
|span ft=57
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.63
|height ft=11
|height in=11
|wing area sqm=54.3
|wing area sqft=584.5
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=3,223
|empty weight lb=7,100
|gross weight kg=4,313
|gross weight lb=9,500
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=2
|eng1 type=[[Pratt & Whitney Wasp Junior]]
|eng1 kw=<!-- prop engines -->300
|eng1 hp=<!-- prop engines -->400
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=308.9
|max speed mph=192
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=275.1
|cruise speed mph=171
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=4,572
|ceiling ft=15,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=6.0
|climb rate ftmin=1,180
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* [http://www.flightglobal.com/PDFArchive/View/1937/1937%20-%201127.html "Canadian Convertible."] [[Flight International|Flight]], 29 April 1937, p.&nbsp;427. Retrieved: 9 March 2008. 
* [[Larry Milberry|Milberry, Larry]]. ''Aviation In Canada''. Toronto: McGraw-Hill Ryerson Ltd., 1979. ISBN 0-07-082778-8.
* Molson, Ken M. and Taylor, Harold A. ''Canadian Aircraft Since 1909''. Stittsville, Ontario: Canada's Wings, Inc., 1982. ISBN 0-920002-11-0.
* Taylor, Michael J.H. ''Jane's Encyclopedia of Aviation''. London: Studio Editions, 1989, p.&nbsp;354.
{{refend}}

==External links==
* [http://1000aircraftphotos.com/Contributions/Shumaker/7146.htm Fairchild 45-80 Sekani (CF-BHD c/n 101)]
*[http://www.fliegerweb.com/de/lexicon/Geschichte/Fairchild+45-80+Sekani-682 680   Fairchild 45-80 Sekani, FliegerWeb.com, in German but a lot of technical data and pictures]
{{Fairchild aircraft}}

[[Category:Canadian airliners 1930–1939]]
[[Category:Fairchild Aircraft Ltd. aircraft|45-80]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Sesquiplanes]]
[[Category:Floatplanes]]