{{Use dmy dates|date=May 2015}}
{{Use British English|date=May 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Skylark 3
 | image=Slingsby T43 Skylark 3.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=Open class sailplane
 | national origin=[[United Kingdom]]
 | manufacturer=[[Slingsby Aviation|Slingsby Sailplanes Ltd.]]
 | designer=
 | first flight=July 1957
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=70
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Slingsby Skylark 2]]
 | variants with their own articles=
}}
|}
The '''Slingsby T.43 Skylark 3''' was a single seat [[FAI Open Class|Open Class]] [[sailplane]] developed from the [[Slingsby Skylark 2|Skylark 2]] with an extended wingspan.  It gained first place at the 1960 [[World Gliding Championships]].

==Development==

The first of Slingsby's Skylark series to go into production was the Skylark 2, a single seat competition sailplane with a span of just under 15 m.<ref>{{harvnb|Ellison|1971|pp=208, 211}}</ref>  Its successor, the Skylark 3 had much in common, but had a span increased to over 18 m and a consequent increase in aspect ratio.  It, too went into production and sold in numbers.<ref name="Ell1">{{harvnb|Ellison|1971|pp=211, 264–5}}</ref>

The Skylark 3 had high, pylon mounted wings with an inner section of parallel chord extending out almost to mid span, followed by an outer section with taper on the trailing edge.  Ailerons filled much of the outer sections and airbrakes, operating in pairs above and below the wings, were mounted on the main spar in the inboard section.<ref name="Ell1"/> The structure of the wing, like that of the rest of the aircraft was wooden, built around a main spar and a lighter rear spar and Gaboon ply covered from this rear spar forward.  Behind this spar the wing was fabric covered, though the ailerons were ply skinned.<ref name="JAWA56">{{harvnb|Bridgman|1956|p=92}}</ref>  The fuselage was a semi-monocoque, elliptical in cross section and built around spruce frames with a plywood skin.  The cockpit was immediately ahead of the pylon and wing leading edge, enclosed with a perspex canopy.  Tapered and clipped tailplane and elevators were mounted on top of the fuselage, far enough forward that the rudder hinge was behind the elevators.  These surfaces were plywood covered.  Fin and rudder together were tapered and flat topped; the fin was also ply skinned, but the unbalanced rudder was fabric covered.  Compared with the Skylark 2, vertical tail areas were increased by 35% and horizontal areas by 23%.  The undercarriage was conventional, with a nose skid, fixed monowheel and a small, faired tailskid.<ref name="JAWA56"/>

The Skylark 3 flew for the first time in July 1955.  70 were built; of at least 7 subtypes, the 3A,  3B and 3F were the most numerous.<ref name="Ell1"/>

==Operational history==

The high point of the Skylark 3's competitive career was at the 1960 [[World Gliding Championships]], held at [[Cologne]] in Germany, where it was flown by the Swedish-Argentinian Rolf Hossinger into first place.<ref name="Ell1"/> It was the last British designed sailplane to win the championships.  Most went to [[UK]] gliding clubs and individuals, but a few were exported including a few to the Korean Air Force.<ref name="Ell1"/>  Some, following standard Slingsby practice were sold as kits, both within the UK and to New Zealand.

The Skylark 3 still holds the British gliding record for a 'declared 500&nbsp;km goal' which was set by [[Nicholas Goodhart|Nick Goodhart]] on 11 May 1959 when he flew 360 miles from Lasham in England to Portmoak in Scotland. Goodhart launched from Lasham at 13:03 and landed at Portmoak at 19:30 hours, after an epic flight in which he had successively used: hill-lift, thermal-lift and wave, in an epic flight that involved intentional climbs to 18,000&nbsp;ft inside a cumulo-nimbus cloud, and a further climb to 15,500&nbsp;ft inside another cumulus.<ref>Gliding records</ref>  This is still the UK 20 metre goal-distance-record and the speed record for a 500&nbsp;km goal flight (2010) which is remarkable given the vastly improved performance of modern fibreglass gliders, as compared to the relatively low performance of the wood and fabric Skylark 3.

==Variants==
{{harvnb|Ellison|1971|pp=212, 264–5}}

;Skylark 3A: first production subtype. 7 built.
;Skylark 3B: cockpit moved forward 76&nbsp;mm, overall length increase by 80&nbsp;mm. 24 built. 
;Skylark 3C: 3A with strengthened spars to meet certification requirements.  2 built.
;Skylark 3D: 3B with strengthened spars to meet certification requirements. 2 built.
;Skylark 3E: modified 3B with wing a tip section of NACA 64,<sub>2</sub>,615 and narrower ailerons.  1 built.
;Skylark 3F: tab assisted ailerons and an increase in tailplane span from 2.88 m to 3.23 m. 30 built including kits.
;Skylark 3G: modified F with increased span, narrower ailerons. 4 built.
;Slingsby T.47
:A proposed extended span version of the Skylark 3 with 20 metre wings

''Note:''A retrospective alteration increasing the all up weight of the 3A and 3B to 830&nbsp;lb was incorporated into the 3F and 3G.
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Skylark 3A) ==
{{Aircraft specs
|ref={{harvnb|Ellison|1971|pp=211–2}}
|prime units?=kts
<!--
        General characteristics
-->
|crew=1
|capacity=
|length m=7.54
|span m=18.186
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=16.1
|aspect ratio=20.5
|airfoil=NACA 63<sub>3</sub>-620 at root, NACA 4415 at tip
|empty weight kg=253
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=358
|max takeoff weight lb=
|max takeoff weight note=
<!--
        Performance
-->
|stall speed kmh=58<!-- aerobatic -->
|never exceed speed kmh=216
|never exceed speed note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=32:1 at {{convert|40|kn|km/h mph|abbr=on|1}}<ref name="JAWA56"/>
|sink rate ms=0.56
|sink rate note=minimum, at {{convert|35|kn|km/h mph|abbr=on|1}}<ref name="JAWA56"/>
|wing loading kg/m2=22.2
|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
*[[Slingsby Skylark 2]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
*[[List of gliders#Slingsby|List of gliders]]
}}

==Notes==
{{reflist}}

==References==
{{commons category|Slingsby Skylark 3}}
{{refbegin}}
*{{cite book |title= British Gliders and Sailplanes|last=Ellison|first=Norman| year=1971|volume=|publisher=A & C Black Ltd|location=London |isbn=0 7136  1189 8|ref=harv}}
*{{cite book |title= Jane's All the World's Aircraft 1956-57|last= Bridgman |first= Leonard |coauthors= |edition= |year=1956|publisher= Jane's All the World's Aircraft Publishing Co. Ltd|location= London|isbn=|ref=harv }}
*{{cite web |url=http://www.gliding.co.uk/bgainfo/competitions/records/uk20m.htm |title=Gliding records|author= |date= |work= |publisher= |accessdate=}}
{{refend}}

<!-- ==External links== -->
{{Slingsby aircraft}}

[[Category:British sailplanes 1950–1959]]
[[Category:Slingsby aircraft|Skylark 3]]