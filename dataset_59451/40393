{{ad|date=April 2013}}
'''Epstein Becker & Green, P.C.,''' also known as “Epstein Becker Green”, is one of the largest [[health care]] [[law firm]]s in the [[United States]]<ref>[http://www.modernhealthcare.com/section/lists?djoPage=product_details&djoPid=9874&djoTry=1274822029 Modern Healthcare magazine (Feb. 2009).]</ref> and one of the top 10 U.S. employment law firms by number of lawyers.<ref>[http://www.workforce.com/section/03/feature/25/97/76/index.html Workforce Management magazine (Nov. 2008).]</ref>  Epstein Becker Green serves clients in various industries including health care, financial services, retail, [[Hospitality industry|hospitality]], and technology, representing entities from startups to Fortune 100 companies.

== Practice areas ==
Epstein Becker Green is involved in the following areas of [[legal practice]]:
* [[Health care]] and [[life sciences]]
* [[Labour law|Labor and employment]]
* [[Lawsuit|Litigation]]
* [[Corporate]] services
* [[Employee benefit]]s

== History ==
In October 1973, a Washington, D.C., [[lawyer]], Steven B. Epstein, and a childhood friend, New York lawyer Jeffrey H. Becker, formed “Epstein & Becker”, a firm focusing on [[health care law]]. Ronald M. Green, a New York labor lawyer and a friend of Becker, realized that health care law and labor law were “inherently [[synergistic]] practices” and, in August 1978, he joined the firm to build its labor and employment practice and his surname was added to the firm’s name. The firm was eventually [[Corporation|incorporated]] and became “Epstein Becker & Green, P.C.”. As the firm grew in size, it added capabilities in litigation, corporate services and employee benefits and established offices in more than 10 U.S. cities.

== International work ==
Epstein Becker Green is a founding member of the International Lawyers Network (ILN), an association of more than 90 law firms with more than 5,000 attorneys worldwide.<ref>[http://www.iln.com/Firm_Detail_24.htm ILN Members Directory – Epstein Becker & Green, P.C.]</ref> The ILN provides Epstein Becker Green attorneys with direct access to legal services in more than 60 countries, enabling the firm to serve clients internationally.

== Locations ==
Epstein Becker Green has offices in: [[Baltimore]], [[Boston]], [[Chicago]], [[Detroit]], [[Houston]], [[Los Angeles]], [[New York City]], [[Newark, New Jersey]], [[Princeton, New Jersey]], [[San Diego]], [[San Francisco]], [[Stamford, Connecticut]] and [[Washington, D.C.]]

== Recognition and rankings ==
* Recognized by ''Chambers USA: America’s Leading Lawyers for Business'' for excellence in eight practice areas; 23 Epstein Becker Green lawyers were ranked as “Leaders in Their Field” (June 2011)<ref>[http://www.chambersandpartners.com/USA Chambers USA: America’s Leading Lawyers for Business (2011).]</ref>
* Nominated for a 2011 ''Chambers USA'' Award for Excellence in the “Healthcare” category (March 2011)<ref>[http://www.chambersandpartners.com/pdfs/USAAwards_2011_Shortlist.pdf Chambers USA Awards for Excellence - Nominations 2011.]</ref>
* Was ranked 16th in the USA for diversity by ''[[The American Lawyer]]'''s 2011 "Diversity Scorecard" .<ref>http://www.law.com/jsp/tal/PubArticleTAL.jsp?id=1202494725139</ref>
* Was ranked second on ''Multi-Cultural Law'' magazine's 2011 "Top 50 Law Firms for Partners" list, third on its "Top 25 Law Firms for [[African-Americans]]" list, 14th on its "Top 100 Law Firms for Women" list, and 21st on its "Top 100 Law Firms for Diversity" list.
* Received a top rating by the [[Human Rights Campaign Foundation]] in 2010 as one of the best places to work for [[lesbians]], [[gays]], [[bisexuals]], and [[transgender]] people
* Was one of 23 law firms listed as "The Best" for women's advancement in a 2010 survey conducted by the Project for Attorney Retention.<ref>[http://www.pardc.org/PressReleases/NewPartners2010.pdf Women Lawyers Advance in New Partner Classes.]</ref>
* Ranked 16th among law firms nationwide for outstanding client service in the ninth annual “Client Service A-Team” report issued by BTI Consulting Group Inc. (Nov. 2009).<ref>[http://www.bticonsulting.com/PDFs/BTI_Client_Service_ATeam_2010_EXEC_SUMM.pdf The BTI Client Service A-Team Survey.]</ref>
* Was placed in the top 20 in ''[[The National Law Journal]]'''s 2009 ranking of the 250 largest U.S. law firms by number of women partners.<ref>[http://www.law.com/jsp/nlj/PubArticleNLJ.jsp?id=1202435209872&The__NLJ_ National Law Journal Survey]</ref>
* Ranked eighth in the USA among the 200 largest firms for its percentage of women attorneys by ''The American Lawyer'''s 2009 "Women in Law Firms" study.<ref>[http://www.law.com/jsp/tal/PubArticleTAL.jsp?id=1202430856584&Stuck_in_the_Middle&hbxlogin=1 Women in Law Firms].</ref>
* Appeared on a list of ten American law firms with between 20 and 99 [[equity partner]]s, of which at least 9 percent are minorities, in ''Diversity & The Bar'' magazine's 2009 ranking of the "Law Firms with Highest Minority Equity Partner Levels".

Epstein Becker Green is the only law firm to rank in the top 20 in 2009 surveys of both law firm client service and diversity.<ref>See rankings in the ninth annual “Client Service A-Team” report issued by BTI Consulting Group Inc., the ''American Lawyer''’s 2009 “Women in Law Firms” study, and the ''[[Minority Law Journal]]''’s 2009 “Diversity Scorecard”.</ref>

==''Pro bono'' and community service==
Epstein Becker Green’s national ''[[pro bono]]'' policy encourages all of its attorneys to undertake ''pro bono'' projects, both on their own and through structured ''pro bono'' opportunities made available by the firm and through partnerships with other organizations. Representative ''pro bono'' work includes children's issues, helping the [[homeless]], community-based projects, immigration/international matters, [[diversity (business)|diversity]], litigation, [[domestic violence]] cases, [[veteran]]s' rights matters, health care policy and access issues.

Many of Epstein Becker Green’s attorneys serve on governing boards of [[charitable]], [[educational]] and other not-for-profit organizations. The firm’s offices often partner with area schools to provide basic [[legal education]] in the classroom and to support community schools through [[fundraising]] and other activities.<ref>[http://www.ebglaw.com/about.aspx?Show=12087 Pro Bono & Community Service: EBG website.]</ref>

== References ==
{{reflist|2}}

== External links ==
* [http://www.ebglaw.com/ Official website]
* [http://www.linkedin.com/company/epstein-becker-&-green-p-c-/ LinkedIn - Epstein Becker Green]
* [http://www.twitter.com/ebglaw/ Twitter profile (@ebglaw)]
* [https://www.facebook.com/Epstein-Becker-Green-140779175933414/ Facebook - Epstein Becker Green]
* [http://www.iln.com/ International Lawyers Network]

{{DEFAULTSORT:Epstein Becker and Green}}
[[Category:Law firms based in New York City]]