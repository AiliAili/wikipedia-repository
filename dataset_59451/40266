{{About|a software testing methodology|decision trees and prediction modeling using them (known as Classification tree)|Decision tree|and|Classification tree}}

The '''Classification Tree Method''' is a method for test design,<ref>{{cite book|last=Bath|first=Graham|author2=McKay, Judy|title=The software test engineer's handbook : a study guide for the ISTQB test analyst and technical test analyst advanced level certificates|year=2008|publisher=Rocky Nook|location=Santa Barbara, CA|isbn=9781933952246|edition=1st}}</ref>  as it is used in different areas of [[software development]].<ref name=advStest>{{cite book|last=Hass|first=Anne Mette Jonassen|title=Guide to advanced software testing|year=2008|publisher=Artech House|location=Boston|isbn=1596932864|pages=179–186}}</ref>
It was developed by Grimm and Grochtmann in 1993.<ref name=GG>{{cite journal|last=Grochtmann|first=Matthias|author2=Grimm, Klaus|title=Classification Trees for Partition Testing|journal=Software Testing, Verification & Reliability|year=1993|volume=3|issue=2|pages=63–82|doi=10.1002/stvr.4370030203}}</ref>
Classification Trees in terms of the Classification Tree Method must not be confused with [[decision tree]]s.

The classification tree method consists of two major steps:<ref name=lkk>{{cite book|last=Kuhn|first=D. Richard|author2=Kacker, Raghu N. |author3=Lei, Yu |title=Introduction to combinatorial testing|year=2013|publisher=Crc Pr Inc|isbn=1466552298|pages=76–81}}</ref><ref name=pierre>{{cite book|last=Henry|first=Pierre|title=The testing network an integral approach to test activities in large software projects|year=2008|publisher=Springer|location=Berlin|isbn=978-3-540-78504-0|page=87}}</ref>
# Identification of test relevant aspects (so called ''classifications'') and their corresponding values (called ''classes'') as well as
# Combination of different classes from all classifications into [[test case]]s.

The identification of test relevant aspects usually follows the (functional) [[specification]] (e.g. [[requirements]], [[use case]]s …) of the [[system under test]]. 
These aspects form the input and output data space of the test object.

The second step of test design then follows the principles of combinatorial test design.<ref name=lkk />

While the method can be applied using a pen and a paper, the usual way involves the usage of the [[Classification Tree Method#Classification Tree Editor|Classification Tree Editor]], a software tool implementing the classification tree method.<ref name=GW />

== Application ==

Prerequisites for applying the classification tree method (CTM) is the selection (or definition) of a [[system under test]].
The CTM is a [[black-box testing]] method and supports any type of system under test. This includes (but is not limited to) [[Computer hardware|hardware systems]], integrated hardware-software systems, plain [[software system]]s, including [[embedded software]], [[user interface]]s, [[operating system]]s, [[parser]]s, and others (or [[subsystem]]s of mentioned systems).

With a selected system under test, the first step of the classification tree method is the identification of test relevant aspects.<ref name=lkk>{{cite book|last=Kuhn|first=D. Richard|author2=Kacker, Raghu N. |author3=Lei, Yu |title=Introduction to combinatorial testing|year=2013|publisher=Crc Pr Inc|isbn=1466552298|pages=76–81}}</ref>
Any system under test can be described by a set of classifications, holding both input and output parameters. 
(Input parameters can also include [[Environment (systems)|environment]]s states, [[pre-condition]]s and other, rather uncommon parameters).<ref name=advStest>{{cite book|last=Hass|first=Anne Mette Jonassen|title=Guide to advanced software testing|year=2008|publisher=Artech House|location=Boston|isbn=1596932864|pages=179–186}}</ref> 
Each classification can have any number of disjoint classes, describing the occurrence of the parameter.
The selection of classes typically follows the principle of [[equivalence partitioning]] for abstract test cases and [[boundary-value analysis]] for concrete test cases.<ref name=pierre>{{cite book|last=Henry|first=Pierre|title=The testing network an integral approach to test activities in large software projects|year=2008|publisher=Springer|location=Berlin|isbn=978-3-540-78504-0|page=87}}</ref>
Together, all classifications form the classification tree. 
For semantic purpose, classifications can be grouped into ''compositions''.

The maximum number of test cases is the [[Cartesian product]] of all classes of all classifications in the tree, quickly resulting in large numbers for realistic test problems.
The minimum number of test cases is the number of classes in the classification with the most containing classes.

In the second step, test cases are composed by selecting exactly one class from every classification of the classification tree. 
The selection of test cases originally<ref name=GG>{{cite journal|last=Grochtmann|first=Matthias|author2=Grimm, Klaus|title=Classification Trees for Partition Testing|journal=Software Testing, Verification & Reliability|year=1993|volume=3|issue=2|pages=63–82|doi=10.1002/stvr.4370030203}}</ref> was a manual task to be performed by the [[test engineer]].

=== Example ===

[[File:Classification Tree for Database System.png|thumb|Classification Tree for a Database System]]

For a [[database system]], test design has to be performed. 
Applying the classification tree method, the identification of test relevant aspects gives the classifications: ''User Privilege'', ''Operation'' and ''Access Method''.
For the ''User Privilege''s, two classes can be identified: ''Regular User'' and ''Administrator User''.
There are three ''Operation''s: ''Add'', ''Edit'' and ''Delete''.
For the ''Access Method'', again three classes are identified: ''Native Tool'', ''[[Web Browser]]'', ''[[API]]''.
The ''Web Browser'' class is further refined with the test aspect ''Brand'', three possible classes are included here: ''[[Internet Explorer]]'', ''[[Mozilla Firefox]]'', and ''[[Apple Safari]]''.

The first step of the classification tree method now is complete. Of course, there are further possible test aspects to include, e.g. access speed of the connection, number of [[database record]]s present in the database, etc. Using the graphical representation in terms of a tree, the selected aspects and their corresponding values can quickly be reviewed.

For the statistics, there are 30 possible test cases in total (2 privileges * 3 operations * 5 access methods). For minimum coverage, 5 test cases are sufficient, as there are 5 access methods (and access method is the classification with the highest number of disjoint classes).

In the second step, three test cases have been manually selected: 
# A regular user adds a new data set to the database using the native tool.
# An administrator user edits an existing data set using the Firefox browser.
# A regular user deletes a data set from the database using the API.

== Enhancements ==

=== Background ===

The CTM introduced the following advantages<ref name=advStest />  over the Category Partition Method<ref>{{cite journal|last=Ostrand|first=T. J.|author2=Balcer, M. J.|title=The category-partition method for specifying and generating functional tests|journal=Communications of the ACM|year=1988|volume=31|issue=6|pages=676–686|doi=10.1145/62959.62964}}</ref> (CPM) by Olstrad and Balcer:
* '''Notation''': CPM only had a textual notation, whereas CTM uses a graphical, tree-shaped representation.
* '''Refinements''' Selecting one representative might have an influence on the occurrence of other representatives.
: CPM only offers restrictions to handle this scenario.
: CTM allows modeling of hierarchical refinements in the classification tree, also called ''implicit dependencies''.
* '''Tool support''': The tool presented by Ostrand and Balcer only supported test case generation, but not the partitioning itself.
: Grochtmann and Wegener presented their tool, the [[Classification Tree Editor]] (CTE) which supports both partitioning as well as test case generation.<ref name="GW">{{cite journal|last=Grochtmann|first=Matthias|author2=Wegener, Joachim|title=Test Case Design Using Classification Trees and the Classification-Tree Editor CTE|journal=Proceedings of the 8th International Software Quality Week(QW '95), San Francisco, USA|year=1995|url=http://www.systematic-testing.com/documents/qualityweek1995_1.pdf}}</ref>
[[File:Classification Tree for Embedded System.png|thumb|Classification Tree for Embedded System Example containing concrete values, concrete timing, (different) transitions and distinguish between States and Actions]]

=== Classification Tree Method for Embedded Systems ===

The classification tree method first was intended for the design and specification of abstract test cases. With the classification tree method for embedded systems,<ref name="conrad">{{cite journal|last=Conrad|first=Mirko|author2=Krupp, Alexander|title=An Extension of the Classification-Tree Method for Embedded Systems for the Description of Events|journal=Electronic Notes in Theoretical Computer Science|date=1 October 2006|volume=164|issue=4|pages=3–11|doi=10.1016/j.entcs.2006.09.002}}</ref> test implementation can also be performed. Several additional features are integrated with the method:
# In addition to atomic test cases, test sequences containing several test steps can be specified.
# A concrete timing (e.g. in Seconds, Minutes ...) can be specified for each test step.
# Signal transitions (e.g. [[linear]], [[Spline (mathematics)|spline]], [[sine]] ...) between selected classes of different test steps can be specified.
# A distinction between [[Event (computing)|event]] and [[State (computer science)|state]] can be modelled, represented by different visual marks in a test.
The module and [[unit testing]] tool [[Tessy (software)|Tessy]] relies on this extension.

=== Dependency Rules and Automated Test Case Generation ===

One way of modelling constraints is using the refinement mechanism in the classification tree method. This, however, does not allow for modelling [[Constraint (information theory)|constraint]]s between classes of different classifications. Lehmann and Wegener introduced Dependency Rules based on [[Boolean expression]]s with their incarnation of the CTE.<ref name="LW">{{cite journal|last=Lehmann|first=Eckard|author2=Wegener, Joachim|title=Test Case Design by Means of the CTE XL|journal=Proceedings of the 8th European International Conference on Software Testing, Analysis & Review (EuroSTAR 2000)|year=2000|url=http://www.systematic-testing.com/documents/eurostar2000.pdf}}</ref> Further features include the automated generation of [[test suite]]s using combinatorial test design (e.g. [[all-pairs testing]]).

=== Prioritized Test Case Generation ===

Recent enhancements to the classification tree method include the [[Risk-based testing|prioritized test case generation]]: It is possible to assign weights to the elements of the classification tree in terms of occurrence and error [[probability]] or risk. These weights are then used during test case generation to prioritize test cases.<ref name="luniak">{{cite journal|last=Kruse|first=Peter M.|author2=Luniak, Magdalena|title=Automated Test Case Generation Using Classification Trees|journal=Software Quality Professional|date=December 2010|volume=13|issue=1|pages=4–12}}</ref><ref>Franke M, Gerke D, Hans C. und andere. Method-Driven Test Case Generation for Functional System Verification. Proceedings ATOS. Delft. 2012. P.36-44.</ref> [[Statistics|Statistical testing]] is also available (e.g. for [[wear]] and [[Fatigue (material)|fatigue]]  tests) by interpreting the element weights as a [[discrete probability distribution]].

=== Test Sequence Generation ===

With the addition of valid transitions between individual classes of a classification, classifications can be interpreted as a [[Finite-state machine|state machine]], and therefore the whole classification tree as a [[Statechart#Harel statechart|Statechart]]. This defines an allowed order of class usages in test steps and allows to automatically create test sequences.<ref name="seqgen">{{cite journal|last=Kruse|first=Peter M.|author2=Wegener, Joachim|title=Test Sequence Generation from Classification Trees|journal=Software Testing, Verification and Validation (ICST), 2012 IEEE Fifth International Conference on|date=April 2012|pages=539–548|doi=10.1109/ICST.2012.139|isbn=978-0-7695-4670-4}}</ref> Different coverage levels are available, such as [[Code coverage#Basic coverage criteria|state coverage]], transitions coverage and coverage of state pairs and transition pairs.

=== Numerical Constraints ===

In addition to Boolean dependency rules referring to classes of the classification tree, Numerical Constraints allow to specify [[formula]]s with classifications as variables, which will evaluate to the selected class in a test case.<ref name="numdeb">{{cite journal|last=Kruse|first=Peter M.|author2=Bauer, Jürgen |author3=Wegener, Joachim |title=Numerical Constraints for Combinatorial Interaction Testing|journal=Software Testing, Verification and Validation (ICST), 2012 IEEE Fifth International Conference on|date=April 2012|pages=758–763|doi=10.1109/ICST.2012.170|isbn=978-0-7695-4670-4}}</ref>

== Classification Tree Editor ==
The '''Classification Tree Editor''' (CTE) is a software tool for test design that implements the classification tree method.<ref name=vehicleElectronics>{{cite book|last=International|first=SAE|title=Vehicle electronics to digital mobility : the next generation of convergence ; proceedings of the 2004 International Congress on Transportation Electronics, Convergence 2004, [Cobo Center, Detroit, Michigan, USA, October 18 - 20, 2004]|year=2004|publisher=Society of Automotive Engineers|location=Warrendale, Pa.|isbn=076801543X|pages=305–306}}</ref><ref name=behave>{{cite book|last=[edited by] Gomes|first=Luís|author2=Fernandes, João M.|title=Behavioral modeling for embedded systems and technologies applications for design and implementation|year=2010|publisher=Information Science Reference|location=Hershey, PA|isbn=160566751X|page=386}}</ref><ref name=justyna>{{cite book|last=[edited by] Zander|first=Justyna|title=Model-based testing for embedded systems|publisher=CRC Press|location=Boca Raton|isbn=1439818452 |author2=Schieferdecker, Ina |author3=Mosterman, Pieter J.|page=10}}</ref><ref name=model2>{{cite book|last=[edited by] Rech|first=Jörg|author2=Bunse, Christian|title=Model-driven software development integrating quality assurance|year=2009|publisher=Information Science Reference|location=Hershey|isbn=1605660078|page=101}}</ref>

Over the time, several editions of the CTE tool have appeared, written in several (by that time popular) [[programming languages]] and developed by several companies.

=== CTE 1 ===

The original version of CTE was developed at [[Daimler-Benz]] Industrial Research<ref name=GW /><ref name=justyna />  facilities in Berlin. 
It appeared in 1993 and was written in [[Pascal (programming language)|Pascal]]. It was only available on [[Unix]] systems.

=== CTE 2 ===

In 1997 a major re-implementation was performed, leading to CTE 2. Development again was at Daimler-Benz Industrial Research. It was written in [[C (programming language)|C]] and available for [[win32]] systems.

CTE 2 was later licensed to Razorcat for inclusion with the module and [[unit testing]] tool [[Tessy (software)|Tessy]].
The classification tree editor for embedded systems<ref name="conrad" /><ref name=behave /> also based upon this edition.

=== CTE XL ===

In 2000, Lehmann and Wegener introduced Dependency Rules with their incarnation of the CTE, the CTE XL (eXtended Logics).<ref name=LW /><ref name=vehicleElectronics /><ref name=model2 /><ref name=rob>{{cite book|last=Olejniczak|first=Robert|title=Systematisierung des funktionalen Tests eingebetteter Software|year=2008|publisher=Technical University Munich|location=Doctoral dissertation|pages=61–63|url=http://d-nb.info/988858436/34.pdf|accessdate=10 October 2013}}</ref>  Further features include the automated generation of [[test suite]]s using combinatorial test design (e.g. [[all-pairs testing]]).<ref>{{cite journal|last=Cain|first=Andrew|author2=Chen, Tsong Yueh |author3=Grant, Doug |author4=Poon, Pak-Lok |author5=Tang, Sau-Fun |author6= Tse, TH |title=An Automatic Test Data Generation System Based on the Integrated Classification-Tree Methodology|journal=First International Conference, SERA 2003, San Francisco, CA, USA, June 25–27, 2003, Selected Revised Papers|year=2004|pages=225–238|doi=10.1007/978-3-540-24675-6_18|url=http://hub.hku.hk/bitstream/10722/43692/1/121170.pdf|accessdate=10 October 2013}}</ref>

Development was performed by [[DaimlerChrysler]]. CTE XL was written in [[Java (programming language)|Java]] and was supported on win32 systems. 
CTE XL was available for download free of charge.

In 2008, Berner&Mattner acquired all rights on CTE XL and continued development till CTE XL 1.9.4.

=== CTE XL Professional ===

Starting in 2010, CTE XL Professional was developed by Berner&Mattner.<ref name="luniak" /> A complete re-implementation was done, again using [[Java (programming language)|Java]] but this time [[Eclipse (software)|Eclipse]]-based. CTE XL Professional was available on win32 and [[win64]] systems.

New developments included:
* Prioritized test case generation: It is possible to assign weights to the elements of the classification tree in terms of occurrence and error [[probability]] or risk. These weights are then used during test case generation to prioritize test cases.<ref name="luniak" /><ref>Franke, M.; Gerke, D.; Hans, C; and others: Method-Driven Test Case Genera-tion for Functional System Verification, Air Transport and Operations Sym-posium 2012; p.354-365. Proceedings ATOS. Delft  2012.</ref> [[Risk-based testing|Risk-based]] and statistical testing is also available.
* Test Sequence Generation<ref name="seqgen" /> using [[Multi-Agent System]]s
* Numerical Constraints<ref name="numdeb" />

=== TESTONA ===

In 2014, Berner&Mattner started releasing its classification tree editor under the [[brand]] name [http://www.testona.net TESTONA].

A [http://www.testona.net/en/webshop/testona-light-free-of-charge/index.html free edition of TESTONA] is still available for download free of charge, however, with reduced functionality.

=== Versions ===

{| class="wikitable sortable"
|-
! Version !! Date !! Comment !! Written in !! OS
|-
| CTE 1.0 ||1993|| Original Version,<ref name=GW /><ref name=justyna />  limited to 1000 test cases (fix!)|| [[Pascal (programming language)|Pascal]] || [[Unix]] 
|-
| CTE 2.0 ||1998|| Windows Version,<ref name=behave /> unlimited number of test cases|| [[C (programming language)|C]] || Unix, [[win32]] 
|-
| CTE XL 1.0 ||2000|| Dependency Rules, Test Case Generation<ref name=LW /><ref name=vehicleElectronics /><ref name=model2 /> || [[Java (programming language)|Java]] || win32 
|-
| CTE XL 1.6 ||2006|| Last Version by Daimler-Benz<ref name=rob /> || Java || win32 
|-
| CTE XL 1.8 ||2008|| Development by Berner&Mattner || Java || win32 
|-
| CTE XL 1.9 ||2009|| Last Java-only Version|| Java || win32 
|-
| CTE XL Professional 2.1 || 2011-02-21 || First [[Eclipse (software)|Eclipse]]-based Version, Prioritized Test Case Generation,<ref name=luniak /> [[Deterministic]] Test Case Generation, [[Requirements traceability|Requirements-Tracing]] with [[IBM Rational DOORS|DOORS]]|| [[Java_version_history#Java_SE_6|Java 6]], [[Eclipse (software)|Eclipse]] 3.5 || win32 
|-
| CTE XL Professional 2.3 || 2011-08-02 || [[Quality Center|QualityCenter]] integration, Requirements Coverage Analysis and [[Traceability matrix|Traceability Matrix]], [[API]]|| Java 6, Eclipse 3.6 || win32 
|-
| CTE XL Professional 2.5 || 2011-11-11 || Test Result Anotation, [[Mind map|MindMap]] import|| Java 6, Eclipse 3.6 || win32, [[win64]] 
|-
| CTE XL Professional 2.7 || 2012-01-30 || Bug fix release|| Java 6, Eclipse 3.6 || win32, win64
|-
| CTE XL Professional 2.9 || 2012-06-08 || Implicit Mark Mode, Default classes, [[Command Line Interface]]|| Java 6, Eclipse 3.7 || win32, win64
|-
| CTE XL Professional 3.1 || 2012-10-19 || Test Post-Evaluation (e.g. for [[Root cause analysis|Root Cause Analysis]]), Test Sequence Generation,<ref name=seqgen /> Numerical Constraints<ref name=numdeb />|| Java 6, Eclipse 3.7 || win32, win64
|-
| CTE XL Professional 3.3 || 2013-05-28 || Test Coverage Analysis, Variant Management (e.g. as part of [[Product family engineering|Product Family Engineering]]), Equivalence Class Testing|| Java 6, Eclipse 3.7 || win32, win64
|-
| CTE XL Professional 3.5 || 2013-12-18 || Bounday Value Analysis Wizard, Import of [[AUTOSAR]] and [[MATLAB]] models || [[Java_version_history#Java_SE_7|Java 7]], Eclipse 3.8 || win32, win64
|-
| TESTONA 4.1 || 2014-09-22 || Bug fix release || Java 7, Eclipse 3.8 || win32, win64
|-
| TESTONA 4.3 || 2015-07-08 || Generation of Executable [[Test script|Test Scripts]] ([[Code generation (compiler)|Code Generation]]), Import of Test Results<ref>{{cite web|last1=Berner&Mattner|title=Press Release: Test Case Implementation with TESTONA 4.3|url=http://www.testona.net/en/media/press-releases/press-release-testona-4.3.html}}</ref> || Java 7, Eclipse 3.8 || win32, win64
|-
| TESTONA 4.5 || 2016-01-21 || Enhanced Export Facilities, [[GUI]] Improvements || Java 7, Eclipse 3.8 || win32, win64
|-
| TESTONA 5.1 || 2016-07-19 || Bug fix release, Switch to [[Java_version_history#Java_SE_8|Java 8]], Eclipse 4.5 || [[Java_version_history#Java_SE_8|Java 8]], Eclipse 4.5 || win32, win64
|}

== Advantages ==

* Graphical representation of test relevant aspects<ref name=advStest /> 
* Method for both identification of relevant test aspects and their combination into test cases<ref name=lkk />

== Limitations ==

* When test design with the classification tree method is performed without proper test decomposition, classification trees can get large and cumbersome.
* New users tend to include too many (esp. irrelevant) test aspects resulting in too many test cases.
* There is no algorithm or strict guidance for selection of test relevant aspects.<ref>{{cite journal|last=Chen|first=T.Y.|author2=Poon, P.-L.|title=Classification-Hierarchy Table: a methodology for constructing the classification tree|journal=Australian Software Engineering Conference, 1996., Proceedings of 1996|year=1996|pages=93–104|doi=10.1109/ASWEC.1996.534127|isbn=0-8186-7635-3}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.systematic-testing.com/functional_testing/cte_main.php?cte=1 Systematic Testing]

[[Category:Software development process]]
[[Category:Software testing]]