__NOTOC__
{{Infobox NCAA athlete
| color = #00009C
| fontcolor = #FFFFFF
| name = Greg Koubek
| image = 
| caption = 
| college = [[Duke Blue Devils men's basketball|Duke]]
| conference = [[Atlantic Coast Conference|ACC]]
| sport = [[Basketball]]
| position = [[Forward (basketball)|Forward]]
| jersey = 22
| nickname = 
| height_ft = 6
| height_in = 6
| weight_lb = 205
| nationality = American
| birth_date = {{birth date and age|1969|03|15}}
| birth_place = [[Clifton Park, New York]]
| death_date = 
| death_place = 
| highschool = [[Shenendehowa High School|Shenendehowa]] <br/>Clifton Park, New York
| former_school(s)=
| awards = 
| honors = 
*Team captain (1991)
*First player to play in four Final Fours
| tournaments = y
| tournament_list = 
* 4× NCAA ([[1988 NCAA Men's Division I Basketball Tournament|1988]], [[1989 NCAA Men's Division I Basketball Tournament|1989]], [[1990 NCAA Men's Division I Basketball Tournament|1990]], [[1991 NCAA Men's Division I Basketball Tournament|1991]])
| championships = y
| championship_list = 
* 1× NCAA ([[1991 NCAA Men's Division I Basketball Tournament|1991]])
* 1× ACC regular season ([[1990–91 Duke Blue Devils men's basketball team|1991]])
* 1× [[ACC Men's Basketball Tournament|ACC Tournament]] ([[1988 ACC Men's Basketball Tournament|1988]])
}}
'''Greg Koubek''' (born March 15, 1969 in [[Clifton Park, New York]])<ref name=UPDATE>{{Cite web| title = Greg Koubek| publisher = DukeUpdate.com| year = 2010| url = http://www.dukeupdate.com/Alumni/greg_koubek.htm| accessdate = November 20, 2010}}</ref><ref name=CAMP>{{Cite web| title = About: Greg Koubek, Director| work = koubekcamps.com| publisher = Greg Koubek Basketball Camps Inc.| url = http://www.koubekcamps.com/pages/about| accessdate = November 20, 2010}}</ref> is a retired American basketball player best known for his collegiate career at [[Duke University]] between 1988 and 1991. He also played professionally overseas after college for several years.

==Early life==
A native of the greater [[Albany, New York|Albany]] area, Koubek attended [[Shenendehowa High School]] from 1985 to 1987.<ref name=CAMP/> He led the basketball team to a state championship as a [[senior (education)|senior]] in 1987 and later became the first athlete in school history to have his jersey number retired.<ref name=CAMP/> Following the 1987 season he was named a McDonald's All-American, and he was the co-honoree of the [[Mr. New York Basketball]] award, given to the state's best high school boys' basketball player.<ref name=UPDATE/> Koubek finished his high school career having scored 1,972 points and grabbed 682 rebounds, both being the most in school history.<ref>{{Cite web| title = Shenendehowa Basketball Records| publisher = shenbasketball.com| url = http://www.shenbasketball.com/pb/wp_6487f286/wp_6487f286.html| accessdate = November 20, 2010}}</ref>

==College==
Koubek's career playing for the [[Duke Blue Devils men's basketball|Duke Blue Devils]] was an overall inauspicious one in terms of personal statistics. Through his first two seasons, he only started in one total game, but he did play in all 71&nbsp;contests.<ref name=UPDATE/> Between his [[freshman]] and [[Sophomore year|sophomore]] seasons he scored 312&nbsp;points. Duke won the [[ACC Men's Basketball Tournament|ACC Tournament]] in 1988, his freshman year, and also made it all the way to the [[NCAA Men's Division I Basketball Tournament|NCAA Tournament Final Four]] – the first of four straight Final Four appearances.<ref name=UPDATE/><ref name=CAMP/> As a [[junior (education)|junior]] in 1990, Duke made it to the [[1990 NCAA Men's Division I Basketball Tournament|1990 National Championship]] but lost to [[1989–90 UNLV Runnin' Rebels basketball team|UNLV]], 103–73, which is still the worst margin of defeat in NCAA Championship Game history. Koubek played in all 38 games and started 12 of them.<ref name=UPDATE/>

As a senior in [[1990–91 Duke Blue Devils men's basketball team|1990–91]], Koubek made NCAA men's basketball history. He started in 13 games and played in all 38 of them, including Duke's fourth consecutive Final Four. Consequently, he became the first player to ever play in four NCAA Final Fours.<ref>{{Cite web| last = Kent| first = Milton| title = Entering his fourth Final Four, Duke's Koubek knows road well| publisher = ''[[The Baltimore Sun]]''| date = March 26, 1991| url = http://articles.baltimoresun.com/1991-03-26/sports/1991085188_1_koubek-blue-devils-four-final| accessdate = November 28, 2010}}</ref> He also became one of very few players to [[List of NCAA Division I men's basketball players with 145 games played|appear in 145 or more career games]]<ref>{{Cite web | title = 2010–11 NCAA Men's Basketball Records | work = 2010–11 NCAA Men's Basketball Media Guide | publisher = [[National Collegiate Athletic Association]] | year = 2010 | url = http://web1.ncaa.org/web_files/stats/m_basketball_RB/2011/D1.pdf| format = PDF | accessdate = November 20, 2010}}</ref> (Koubek's 146 career games is second only to [[Christian Laettner]]'s 147 in Duke history.) That season, Duke won the ACC regular season title and went on to win the first of back-to-back NCAA championships. Koubek was a team [[captain (sports)|captain]] as a senior as well.<ref name=UPDATE/>

==Professional and later life==
Koubek spent a short stint playing in the [[United States Basketball League]]&nbsp;(USBL) right after college.<ref name=UPDATE/> Afterward, he played professionally for the next six years in [[South Africa]], [[Turkey]], [[Hungary]], and [[Japan]].<ref name=CAMP/> Koubek retired in 1997 and returned home to the Capital District of New York. Koubek became Executive Director of the Albany YMCA. He moved to [[California]]<ref name=CAMP/> where he serves as Executive Director of Gardena-Carson Family YMCA. In addition, he runs an annual basketball camp with his brother, Tim, called the Greg Koubek Basketball Camp.<ref name=CAMP/>

==See also==
*[[List of NCAA Division I men's basketball players with 145 games played]]
*[[List of Duke Blue Devils men's basketball seasons]]

==References==
{{Reflist}}

{{1991 Duke Blue Devils men's basketball navbox}}

{{DEFAULTSORT:Koubek, Greg}}
[[Category:1969 births]]
[[Category:Living people]]
[[Category:American expatriate basketball people in Hungary]]
[[Category:American expatriate basketball people in Japan]]
[[Category:American expatriate basketball people in South Africa]]
[[Category:American expatriate basketball people in Turkey]]
[[Category:Basketball players at the 1988 NCAA Men's Division I Final Four]]
[[Category:Basketball players at the 1989 NCAA Men's Division I Final Four]]
[[Category:Basketball players at the 1990 NCAA Men's Division I Final Four]]
[[Category:Basketball players at the 1991 NCAA Men's Division I Final Four]]
[[Category:Basketball players from New York]]
[[Category:Duke Blue Devils men's basketball players]]
[[Category:Forwards (basketball)]]
[[Category:McDonald's High School All-Americans]]
[[Category:Parade High School All-Americans (boys' basketball)]]
[[Category:People from Clifton Park, New York]]