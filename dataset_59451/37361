{{good article}}
{{Infobox military conflict
|conflict=Battle of Apamea
|partof=the [[Arab–Byzantine Wars]]
|image=
|caption=
|map_type=Syria
|map_size=290
|map_caption=Apamea shown within present-day Syria
|map_mark=Battle icon gladii red.svg
|map_marksize=15
|latitude = 35.418
|longitude = 36.398
|date=19 July 998
|place=plain of al-Mudiq, near [[Apamea (Syria)|Apamea]]
|casus=
|territory=
|result=Fatimid victory
|combatant1=[[Byzantine Empire]]
|combatant2=[[Fatimid Caliphate]]
|commander1=[[Damian Dalassenos]] {{KIA}}
|commander2=[[Jaysh ibn Samsama]]
|strength1=unknown
|strength2=10,000 Fatimid troops<br>1,000 [[Banu Kilab]] cavalry
|casualties1=variously 5,000, 6,000 or 10,000 dead; 2,000 captive
|casualties2=over 2,000 dead
|campaignbox={{Campaignbox Arab–Byzantine Wars}}
}}
The '''Battle of Apamea''' was fought on 19 July 998 between the forces of the [[Byzantine Empire]] and the [[Fatimid Caliphate]]. The battle was part of a series of military confrontations between the two powers over control of northern [[Bilad al-Sham|Syria]] and the [[Hamdanid]] emirate of [[Aleppo]]. The Byzantine regional commander, [[Damian Dalassenos]], had been besieging [[Apamea (Syria)|Apamea]], until the arrival of the Fatimid relief army from [[Damascus]], under [[Jaysh ibn Samsama]]. In the subsequent battle, the Byzantines were initially victorious, but a lone Kurdish rider managed to kill Dalassenos, throwing the Byzantine army into panic. The fleeing Byzantines were then pursued, with much loss of life, by the Fatimid troops. This defeat forced the Byzantine emperor [[Basil II]] to personally campaign in the region the next year, and was followed in 1001 by the conclusion of a ten-year truce between the two states.

== Background ==
In September 994 [[Michael Bourtzes]], the [[Byzantine]] military governor (''[[dux|doux]]'') of [[Antioch]] and northern [[Bilad al-Sham|Syria]], suffered a heavy defeat at the [[Battle of the Orontes]] at the hands of the [[Fatimid]] general [[Manjutakin]]. This Fatimid victory shook the Byzantine position in Syria, and posed a grave threat to its Arab vassal, the [[Hamdanid]] Emirate of [[Aleppo]]. To prevent its fall, Emperor [[Basil II]] himself intervened in the region in 995, forcing Manjutakin to retire to [[Damascus]]. After capturing [[Shayzar]], [[Hims]] and [[Rafaniya]], and building a new fortress at [[Antartus]], the emperor withdrew, leaving [[Damian Dalassenos]] as the new ''doux'' of Antioch.{{sfn|Honigmann|1935|p=106}}

Dalassenos maintained an aggressive stance. In 996 his forces raided the environs of [[Tripoli, Lebanon|Tripoli]] and [[Arqa]], while Manjutakin, again without success, laid siege to Aleppo and Antartus, but was forced to withdraw when Dalassenos with his army came to relieve the fortress.{{sfn|Honigmann|1935|pp=106–107}} The next year, Dalassenos repeated his raids against Tripoli, Rafaniya, Awgh, and [[al-Lakma]], capturing the latter.{{sfn|Honigmann|1935|pp=106–107}} At the same time, the inhabitants of [[Tyre, Lebanon|Tyre]], under the leadership of a sailor named Allaqa, [[Revolt of Tyre (996–998)|rose up in revolt]] against the Fatimids and requested Byzantine assistance; further south, in [[Palestine (region)|Palestine]], the [[Bedouin]] leader [[Mufarrij ibn Daghfal ibn al-Jarrah]] attacked [[Ramlah]].{{sfn|Honigmann|1935|p=107}}{{sfn|Canard|1961|pp=297–298}}

== Siege of Apamea and the Fatimid relief expedition ==
In early summer 998, Dalassenos learned that a catastrophic fire had broken out in [[Apamea (Syria)|Apamea]] and destroyed most of its provisions, so he marched towards the city. The Aleppines too endeavoured to seize Apamea and arrived there first, but withdrew at the approach of Dalassenos, who could not permit a vassal to grow too strong and intended to capture the town for the emperor. Although ostensibly allies with the Byzantines, the Aleppines left the provisions they had brought with them to be collected by the inhabitants of Apamea, aiding them in their resistance.{{sfn|Cheynet|Vannier|1986|pp=77–78}}{{sfn|Schlumberger|1900|pp=108, 110}} Subsequent events are presented by several authors, including the brief narrative of [[John Skylitzes]] and the more extensive accounts of the Christian Arab [[Yahya of Antioch]] and the Armenian [[Stephen of Taron]]. Arab accounts also survive, all apparently drawing upon the work of the 11th-century historian Hilal al-Sabi; the most detailed version is preserved by [[Ibn al-Qalanisi]].{{sfn|Canard|1961|p=297}}{{sfn|Holmes|2005|pp=347–349}}

The governor of Apamea, al-Mala'iti, called for aid upon the Fatimids. According to Ibn al-Qalanisi, the eunuch regent [[Barjawan]] appointed [[Jaysh ibn Samsama]] to command the relief army, naming him governor of Damascus and giving him a thousand men.{{sfn|Canard|1961|p=298}}{{sfn|Schlumberger|1900|pp=107–108}} Before confronting the Byzantines, the Fatimids had to deal with the revolt of Tyre and the rebellion of Ibn al-Jarrah. The Byzantines attempted to aid the besieged at Tyre by sending a fleet, but it was beaten off by the Fatimids, and the city captured in June 998.{{sfn|Honigmann|1935|p=107}}{{sfn|Canard|1961|p=298}} Ibn al-Jarrah's revolt was also suppressed, and Jaysh ibn Samsama returned to Damascus, where he stayed for three days to gather his forces for the relief of Apamea. There he was joined by the troops and volunteers from Tripoli, assembling a force numbering 10,000 men and 1,000 Bedouin riders of the [[Banu Kilab]] tribe.{{sfn|Canard|1961|pp=298–299}} According to Skylitzes, the Fatimid army comprised the forces of Tripoli, [[Beirut]], Tyre, and Damascus.{{sfn|Schlumberger|1900|p=108}} Meanwhile, Dalassenos was vigorously pursuing the siege, and the inhabitants of Apamea had been reduced to famine, being forced to eat cadavers and dogs, which they bought for the price of 25 silver ''[[dirham]]s'' (according to [[Abu-al-Faraj ibn al-Jawzi|Abu'l-Faraj]], two gold ''[[gold dinar|dinars]]'') a piece.{{sfn|Canard|1961|p=299}}{{sfn|Schlumberger|1900|p=110}}

== Battle ==
The two armies met on the large plain of al-Mudiq (cf. [[Qalaat al-Madiq]]), surrounded by mountains and located near the Lake of Apamea,{{sfn|Canard|1961|p=300}} on 19 July 998.{{sfn|Schlumberger|1900|p=110}} According to Ibn al-Qalanisi, the Fatimid army's left wing was commanded by Maysur the Slav, governor of Tripoli; the centre, where the [[Daylamite]] infantry and the army baggage train were located, was under the command of Badr al-Attar; the right was commanded by Jaysh ibn Samsama and Wahid al-Hilali. According to all accounts, the Byzantines charged the Fatimid army and drove it to flight, killing some 2,000 and capturing the baggage train. Only 500 ''[[ghilman]]'' under Bishara the [[Ikhshidid]] remained steadfast and held firm against the assault, while the Banu Kilab simply abandoned the fight and began looting the battlefield.{{sfn|Canard|1961|p=299}}{{sfn|Schlumberger|1900|p=110}} At that point, a Kurdish rider, named Abu'l-Hajar Ahmad ibn al-Dahhak al-Salil by [[Ibn al-Athir]] and Ibn al-Qalanisi and Bar Kefa by the Byzantine sources and Abu'l-Faraj, rode towards Dalassenos, who was near his battle standard on top of a height and was accompanied only by two of his sons and ten men of his retinue. Believing the battle won and that the Kurd wanted to surrender, Dalassenos took no precautions. As he approached the Byzantine general, Ibn al-Dahhak suddenly charged. Dalassenos lifted his arm to shield himself, but the Kurd launched his spear at him. The general wore no [[cuirass]], and the blow killed him.{{sfn|Canard|1961|p=299}}{{sfn|Schlumberger|1900|pp=110–111}}{{sfn|PmbZ|loc=Damianos Dalassenos (#21379)}}

Dalassenos' death changed the tide of the battle: the Fatimids took heart and, shouting "the enemy of God is dead!", turned on the Byzantines, who fell into panic and fled. The garrison of Apamea too sallied forth, completing the Byzantine debacle.{{sfn|Canard|1961|pp=299–300}}{{sfn|Schlumberger|1900|p=111}} The sources give various numbers for the Byzantine dead: [[Maqrizi]] mentions 5,000, Yahya of Antioch 6,000, and Ibn al-Qalanisi as many as 10,000 dead.{{sfn|PmbZ|loc=Damianos Dalassenos (#21379)}} Most of the remaining Byzantines (2,000 according to Ibn al-Qalanisi) were taken prisoner by the Fatimids. These included several senior officers, including the famed [[Georgians|Georgian]] ''[[patrikios]]'' Tchortovanel, a nephew of [[Tornike Eristavi]], as well as the two sons of Dalassenos, [[Constantine Dalassenos (duke of Antioch)|Constantine]] and [[Theophylact Dalassenos|Theophylact]], who were bought by Jaysh ibn Samsama for 6,000 ''dinars'' and spent the next ten years as captives in [[Cairo]].{{sfn|Canard|1961|p=300}}{{sfn|Schlumberger|1900|p=111}}{{sfn|Cheynet|Vannier|1986|p=78}} Stephen of Taron gives a slightly different account of the battle, whereby the victorious Byzantines were surprised by an attack by the regrouped Fatimids on their camp and that one of Dalassenos's brothers and one of his sons were killed, as well as the general himself. This version is commonly rejected by modern scholars.{{sfn|PmbZ|loc=Damianos Dalassenos (#21379)}}{{sfn|Cheynet|Vannier|1986|p=78}}

== Aftermath ==
Dalassenos' defeat forced Basil II to personally lead yet another campaign in Syria the following year. Arriving in Syria in mid-September, the emperor's army buried their fallen in the field of Apamea and then captured Shayzar, sacked Hisn Masyat and Rafaniya, torched Arqa, and raided the environs of Baalbek, Beirut, Tripoli and [[Jubayl]]. In mid-December, Basil returned to Antioch, where he installed [[Nikephoros Ouranos]] as ''doux'',{{sfn|Honigmann|1935|pp=107–108}} although according to his self-description as the "ruler of the East", his role seems to have been more extensive, with plenipotentiary military and civilian authority over the entire eastern frontier.{{sfn|Holmes|2005|p=477}} In 1001, Basil II concluded a ten-year truce with the Fatimid Caliph [[Al-Hakim bi-Amr Allah|al-Hakim]].{{sfn|Honigmann|1935|p=108}}{{sfn|Holmes|2005|pp=476–477}}

==References==
{{Reflist|30em}}

==Sources==
* {{cite journal | last = Canard | first = Marius | title = Les sources arabes de l'histoire byzantine aux confins des Xe et XIe siècles | journal = Revue des études byzantines | volume = 19 | year = 1961 | pages = 284–314 | url = http://www.persee.fr/web/revues/home/prescript/article/rebyz_0766-5598_1961_num_19_1_1264 | ref=harv | doi=10.3406/rebyz.1961.1264}}
* {{cite book|last1=Cheynet|first1=Jean-Claude|last2=Vannier|first2=Jean-François|title=Études Prosopographiques|location=Paris, France|publisher=Publications de la Sorbonne|year=1986|isbn=978-2-85944-110-4|language=French|url=https://books.google.com/books?id=x7a1cRMNz3UC|ref=harv}}
* {{cite book|last=Holmes|first=Catherine|title=Basil II and the Governance of Empire (976–1025)|location=Oxford|publisher=Oxford University Press|year=2005|isbn=978-0-19-927968-5|url=https://books.google.com/books?id=_h3_c0U1jVoC|ref=harv}}
* {{cite book | last = Honigmann | first = E. | title = Byzance et les Arabes, Tome III: Die Ostgrenze des Byzantinischen Reiches von 363 bis 1071 nach griechischen, arabischen, syrischen und armenischen Quellen | year = 1935 | location = Brussels | publisher = Éditions de l'Institut de Philologie et d'Histoire Orientales | language= German |ref=harv}}
* {{cite encyclopedia | editor1-last = Lilie | editor1-first = Ralph-Johannes | editor2-last = Ludwig | editor2-first = Claudia  | editor3-last = Zielke | editor3-first = Beate | editor4-last = Pratsch | editor4-first = Thomas | encyclopedia = Prosopographie der mittelbyzantinischen Zeit (PmbZ) Online. Berlin-Brandenburgische Akademie der Wissenschaften. Nach Vorarbeiten F. Winkelmanns erstellt | language = German | publisher = De Gruyter | year = 2013 | url = http://www.degruyter.com/view/db/pmbz | ref={{harvid|PmbZ}}}}
* {{cite book | last = Schlumberger | first = Gustave | authorlink = Gustave Schlumberger | title = L'Épopée byzantine à la fin du Xe siècle. Seconde partie, Basile II le tueur de Bulgares | year = 1900 | location = Paris | publisher = Hachette et Cie. | language = French | url = http://gallica.bnf.fr/ark:/12148/bpt6k2042084 | ref=harv}}

{{coord missing|Syria}}

[[Category:Conflicts in 998|Apamea]]
[[Category:990s in the Byzantine Empire]]
[[Category:Battles involving the Byzantine Empire|Apamea]]
[[Category:Battles involving the Fatimid Caliphate|Apamea]]
[[Category:Battles of the Arab–Byzantine wars|Apamea]]
[[Category:Apamea, Syria]]