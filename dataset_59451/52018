{{Infobox journal
| cover = [[File:POC new cover.gif|Image:POC new cover.gif]]
| discipline = [[Chemistry]]
| abbreviation = J. Phys. Org. Chem.
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1395
| publisher = [[John Wiley & Sons]]
| country =
| history = 1988-present
| impact = 1.38
| impact-year = 2014
| ISSN = 0894-3230
}}
The '''''Journal of Physical Organic Chemistry''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]], published since 1988 by [[John Wiley & Sons]]. It covers research in [[physical organic chemistry]] in its broadest sense and is available both online and in print. The current [[editor-in-chief]] is [[Luis Echegoyen]] ([[University of Texas at El Paso]]).<ref name="onlinelibrary">[http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1395/homepage/EditorialBoard.html ''Journal of Physical Organic Chemistry'', Editorial Board, Wiley Online Library]</ref>

== Highest cited papers ==
The following papers have been cited over 180 times:<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2010-03-31}}</ref>
# Chiappe C, Pieraccini D. Ionic liquids: solvent properties and organic reactivity, 18(4): 275-297, 2005
# Carmichael AJ, Seddon KR. Polarity study of some 1-alkyl-3-methylimidazolium ambient-temperature ionic liquids with the solvatochromic dye, Nile Red, 13(10): 591-595, 2000
# Matyjaszewski K, Ziegler MJ, Arehart SV, et al. Gradient copolymers by atom transfer radical copolymerization, 13(12): 775-786, 2000

== Abstracting and indexing ==
The journal is indexed in [[Chemical Abstracts Service]], [[Scopus]], and [[Web of Science]]. According to the ''[[Journal Citation Reports]]'', its [[impact factor|2014 impact factor]] is 1.38.<ref>2014. ''[[Journal Citation Reports]]''. [[Web of Science]] (Science ed.) [[Thomson Reuters]]. 2015.</ref>

== References ==
<references />

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1395}}

[[Category:Physical chemistry journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1988]]
[[Category:Monthly journals]]