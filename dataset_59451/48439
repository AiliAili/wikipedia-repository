{{Infobox Convention
| name       = Heimtextil
| image      = 
| caption    = Heimtextil logo
| status     = Active
| genre      = textiles expo
| venue      = [[Messe Frankfurt]]
| location   = [[Frankfurt am Main]]
| country    = Germany
| first      = 1971
| last       = 2014
| organizer  = [[Messe Frankfurt]]
| filing     = 
| attendance = 67,000
| website    = [http://heimtextil.messefrankfurt.com/frankfurt/en/aussteller/willkommen.html]
}}

'''Heimtextil''' in [[Frankfurt am Main]], Germany is an international trade fair for home and contract textiles with more than 2,700 exhibitors and roughly 67,000 trade visitors.<ref>{{cite web|url=http://www.messefrankfurt.com/frankfurt/en/media/textiletechnologies/heimtextil/frankfurt/texte/heimtextil-2014--erfolgreicher-jahresauftakt-mit-deutlichem-auss.html |title=Final Report |publisher=Heimtextil |date=2014-01-11 |accessdate=2014-06-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20140812175842/http://www.messefrankfurt.com/frankfurt/en/media/textiletechnologies/heimtextil/frankfurt/texte/heimtextil-2014--erfolgreicher-jahresauftakt-mit-deutlichem-auss.html |archivedate=2014-08-12 |df= }}</ref> Throughout the four days of the event in mid-January, the trade fair serves as a business and information platform for manufacturers, retailers and designers from around the world. The next edition takes place from 10 to 13 January 2017.<ref>http://heimtextil.messefrankfurt.com/frankfurt/en/aussteller/willkommen.html</ref>
International exhibitors present their products to a wide trade audience on the [[Messe Frankfurt]] exhibition grounds. As the first marketing and ordering event of the year, Heimtextil showcases new home and contract textiles. By presenting [[bed]], [[bathroom|bath]] and table [[linens]] as well as floor and [[window covering]]s, [[upholstery]] and [[Glazing (window)|sun protection]] systems, Heimtextil covers the entire home and household textiles portfolio from across the globe.<ref>{{cite web|url=http://www.fabricsandfurnishings.com/event/heimtextil-0 |title=Heimtextil |publisher=fabricsandfurnishings.com |date=2012-01-14 |accessdate=2013-09-18}}</ref>

== History ==

Heimtextil has been taking place on the Messe Frankfurt exhibition grounds annually since 1971.<ref>{{cite web|url=http://www.tradefairs.com/Trade_Fairs/Heimtextil/2014/International_Trade_Fair_for_Home_and_Contract_Textiles/14,,271,212921,0,2.html |title=trade fair Heimtextil 2014 in Frankfurt/Main |publisher=Tradefairs |date=2013-04-15 |accessdate=2013-09-18}}</ref> Before the start of Heimtextil, home textiles were part of the Frankfurt Spring Fair (now called [[Ambiente]]) but further growth of the segment in the 1960s and 1970s led to the establishment of an own fair. The first edition attracted over 600 exhibitors from 26 countries and exclusively focused on home textiles, floor coverings and household textiles. Since then, numbers increased to 2,658 exhibitors from 62 countries in 2013.<ref>{{cite web|url=http://textilesupdate.com/with-2658-companies-heimtextil-registers-exhibitor-growth-for-the-third-year-in-a-row |title=With 2,658 Companies, Heimtextil Registers Exhibitor Growth for the Third Year in a Row |publisher=Textiles Update |date= |accessdate=2013-09-18}}</ref> Heimtextil is continuing a long tradition of trade fairs hosted at Germany's trading centre with the Frankfurt Autumn Fair established as a regular trade fair in the year 1240.<ref>{{cite web|url=http://www.frankfurt-tourismus.de/cms/tourismussuite/en/tourist_information_frankfurt/frankfurt_city_history.html |title=History of Frankfurt City History |publisher=Frankfurt-tourismus.de |date= |accessdate=2013-09-18}}</ref>

== Heimtextil Trends ==

The trade fair is complemented by a number of special presentations, lectures, award ceremonies and industry events. The annual trend presentation is an example of one of the event's focus areas. Heimtextil Trends are scouted out by a team of international design firms who look at different developments at the outset of each season and determine the most significant and influential international trends.<ref>{{cite web|url=http://textilesupdate.com/heimtextil-trends-201415-international-design-experts-deliberate-on-future-trend-developments |title=Heimtextil Trends 2014/15: International Design Experts Deliberate on Future Trend Developments |publisher=Textiles Update |date= |accessdate=2013-09-18}}</ref> The results are presented in a book and a special show during the trade fair.<ref>{{cite web|url=http://textilesupdate.com/heimtextil-2014-design-experts-publish-interior-design-forecast |title=Heimtextil 2014/15: Design Experts Publish Interior Design Forecast |publisher=Textiles Update |date= |accessdate=2013-09-18}}</ref> Heimtextil Trends provide product developers, decorators and designers with orientation points.

== Special Interest Guides ==

Another one of Heimtextil's focus areas are the special interest catalogues: Contract Creations,<ref>{{cite web|url=http://textilesupdate.com/with-2658-companies-heimtextil-registers-exhibitor-growth-for-the-third-year-in-a-row |title=With 2,658 Companies, Heimtextil Registers Exhibitor Growth for the Third Year in a Row |publisher=Textiles Update |date= |accessdate=2013-09-18}}</ref> Green Directory<ref>{{cite web|url=http://www.hfnmag.com/textiles/green-efforts-grow-heimtextil/ |title=Green Efforts Grow at Heimtextil &#124; HFN, Home Furnishings News, housewares, rugs, furniture, textiles, tabletop, retail |publisher=Hfnmag.com |date=2011-11-08 |accessdate=2013-09-18}}</ref> and Coupon Business. Under the banner of Contract Creations, Heimtextil presents the product range of international exhibitors from the contract segment as well as a supporting programme for architects,regional planers, decorators and interior designers. Talks, special shows and competitions flesh out current topics in the contract business. In addition, Coupon Business Finder lists exhibitors that offer home and household textiles in small and very small quantities. It is suited for visitors who want to order quantities tailored to their varying needs for a flexible product range. Finally, the Green Directory contains a list of all exhibitors at the fair who use sustainable production methods. All special guides are usually distributed at the fair.

Other events at Heimtextil include the "Young Creations Award: Upcycling"<ref>{{cite web|url=http://textilesupdate.com/heimtextil-2nd-edition-of-the-competition-young-creations-award-upcycling |title=Heimtextil – 2nd edition of the competition ‘Young Creations Award: Upcycling’ |publisher=Textiles Update |date=2013-08-02 |accessdate=2013-09-18}}</ref> for sustainable products in the living textiles sector and "Webchance", a forum for online advertising and sales for stationary retail.<ref>{{cite web|url=http://www.editoratlarge.com/articles/2901/what-not-to-miss-at-heimtextil-in-frankfurt-this-january?page=2 |title=Interior Design News, Events, Jobs, EditorTV, Editorial Submissions & Content &#124; The Editor at Large > What not to miss at Heimtextil in Frankfurt this January |publisher=The Editor at Large |date=2012-12-14 |accessdate=2013-09-18}}</ref>

== Heimtextil trade fairs around the world ==

In addition to Heimtextil in Frankfurt am Main, Germany, Messe Frankfurt organises home textiles fairs in Europe, North America and Asia. These include 
* Interior Lifestyle and Heimtextil Japan in Tokyo
* Heimtextil Russia in Moscow
* Home Textiles Sourcing in New York City, USA
* Heimtextil India in New Delhi 
* Intertextile Shanghai Home Textiles and Intertextile Guangzhou Home Textiles in China.

== References ==

{{reflist}}

== Links ==

* Heimtextil website http://www.heimtextil.messefrankfurt.com
* Heimtextil Blog http://www.heimtextil-blog.com
* Messe Frankfurt website http://www.messefrankfurt.com
* Intertextile Shanghai Home Textiles http://www.intertextilehome.com/index.html
* Interior Lifestyle http://www.interior-lifestyle.com/en/top.php
* Heimtextil Russia http://www.heimtextil.messefrankfurt.ru
* Home Textiles Sourcing New York http://www.hometextilessourcing.com

[[Category:Textile industry]]