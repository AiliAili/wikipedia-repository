{{Infobox journal
| title = Applied Microbiology and Biotechnology
| cover = 
| former_name = 
| abbreviation = Appl. Microbiol. Biotechnol.
| discipline = [[Microbiology]] and [[Biotechnology]]
| editor = Alexander Steinbüchel
| publisher = [[Springer Science+Business Media]]
| country = 
| history = 1975-present
| frequency = Biweekly
| openaccess = 
| license = 
| impact = 3.337
| impact-year = 2014
| ISSN = 0175-7598
| eISSN = 1432-0614
| CODEN = 
| JSTOR = 
| LCCN = 
| OCLC = 
| website = http://www.springer.com/life+sciences/microbiology/journal/253
| link1 = http://link.springer.com/journal/volumesAndIssues/253
| link1-name = Online access
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
}}

The '''''Applied Microbiology and Biotechnology''''' is a [[peer-reviewed]] biweekly journal publishes papers and mini-reviews of new and emerging products, processes and technologies in the area of [[prokaryotic]] or [[eukaryotic]] cells, relevant enzymes and proteins; [[:Category:Applied genetics|applied genetics]] and molecular [[biotechnology]]; genomics and proteomics; applied microbial and cell physiology; environmental biotechnology; process and products and more.

==Abstracting and Indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Academic Search]]
* [[AGRICOLA]]
* [[Aquatic Sciences and Fisheries Abstracts]]
* [[Biological Abstracts]]
* [[BIOSIS]]
* [[CAB Abstracts]]
* [[CAB International]]
* [[CEABA-VtB]]
* [[ChemWeb]]
* [[Chemical Abstracts Service]]
* [[CSA Environmental Sciences]]
* [[EBSCO Information Services|EBSCO databases]]
* [[EI-Compendex]]
* [[Elsevier Biobase]]
* [[EMBASE]]
* [[Environment Index]]
* [[Enology Abstracts]]
* [[Food Science and Technology Abstracts]]
* [[Geobase]]
* [[GeoRef]]
* [[Global Health]]
* [[Health Reference Center Academic]]
* [[Index to Scientific & Technical Proceedings]]
* [[INIS Atomindex]]
* [[ProQuest|ProQuest databases]]
* [[PubMed]]/[[Medline]]
* [[Referativnyi Zhurnal]]
* [[Vitis - Viticulture]]
* [[Science Citation Index]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.337.<ref name=WoS>{{cite book |year=2015 |chapter=Applied Microbiology and Biotechnology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

[[Category:English-language journals]]
[[Category:Applied microbiology journals]]
[[Category:Biotechnology journals]]