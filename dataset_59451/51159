{{Infobox journal
| title = Ecology Law Quarterly
| cover = 
| editor = Elise O'Dea
| manager = Niran Somasundaram
| discipline = [[Environmental law]]
| former_names = 
| abbreviation = Ecol. Law Q.
| publisher = [[UC Berkeley School of Law]]
| country = United States
| frequency = Quarterly
| history = 1971-present
| openaccess = Yes
| license = 
| impact =
| impact-year = 
| website = http://www.boalt.org/elq/
| link1 = http://www.boalt.org/elq/archivedir.php
| link1-name = Online archive
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 473117706
| LCCN = 74644091
| CODEN = 
| ISSN = 0046-1121
| eISSN = 
}}
'''''Ecology Law Quarterly''''' (''[[Bluebook]]'' abbreviation: ''Ecology L.Q.'') is an [[environmental law|environmental]] [[law review]] published quarterly by students at the [[UC Berkeley School of Law]]. The journal also produces '''''Ecology Law Currents''''', an online-only publication that contains "short-form commentary and analysis on timely environmental law and policy issues."<ref name=website>[http://www.boalt.org/elq/ ''Ecology Law Quarterly'' website.] Accessed: February 24, 2013.</ref>

== Recognition ==
''Ecology Law Quarterly'' received the [[United Nations Environment Programme]]'s "[[Global 500 Roll of Honour]]" Award in 1990, in recognition of its environmental achievements.<ref name=about>[http://www.boalt.org/elq/about.php "About ELQ"], ELQ website. Accessed: February 24, 2013.</ref>

In a 1998 survey of experts, ''Ecology Law Quarterly'' was ranked as the most influential environmental law review in the United States.<ref>Gregory Scott Crespi, "Ranking the Environmental Law, Natural Resources Law, and Land Use Planning Journals: A Survey of Expert Opinion," 23 Wm. & Mary Envtl. L. & Pol'y Rev. 273 (1998).</ref>

== See also ==
* [[List of environmental law journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.boalt.org/elq/}} (for both ''Ecology Law Quarterly'' and ''Ecology Law Currents'')


[[Category:Environmental law journals]]
[[Category:University of California, Berkeley]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1971]]
[[Category:English-language journals]]
[[Category:Law in the San Francisco Bay Area]]
[[Category:Law journals edited by students]]


{{law-journal-stub}}
{{science-journal-stub}}
{{Internet-publish-stub}}
{{environment-stub}}