{{Infobox journal
| title = Asian Journal of International Law
| cover = [[Image:AsianJIL cover.jpg]]
| discipline = [[International law]]
| abbreviation = Asian J. Int. Law
| formernames = Singapore Year Book of International Law
| editor = [[Antony Anghie]], [[Simon Chesterman]], Tan Hsien-Li
| publisher = [[Cambridge University Press]]
| country =
| frequency = Biannually
| history = 2011–present
| website = http://www.AsianJIL.org
| ISSN = 2044-2513
}}
The '''''Asian Journal of International Law''''' is a [[Peer review|peer-reviewed]] [[law review]] focusing on public and private [[international law]]. It is an official publication of the [[Asian Society of International Law]] and is published by [[Cambridge University Press]].<ref>[http://journals.cambridge.org/action/displayFulltext?type=1&fid=7978209&jid=AJL&volumeId=1&issueId=01&aid=7978207 “An Asian Journal of International Law”, ''Asian Journal of International Law'', 1 (2011), pp. 1-2.]</ref> It is produced by the [[National University of Singapore Faculty of Law]] and succeeds the ''Singapore Year Book of International Law''.<ref>[http://law.nus.edu.sg/sybil Singapore Year Book of International Law archive page.] {{webarchive |url=https://web.archive.org/web/20110728022549/http://law.nus.edu.sg/sybil |date=July 28, 2011 }}</ref> The [[editors-in-chief]] are Antony Anghie, [[Simon Chesterman]], and Tan Hsien-Li.<ref name="CJO">[http://www.AsianJIL.org ''Asian Journal of International Law'', CJO Web site.]</ref>

The first issue, published in January 2011, included articles by leading Asian scholars and practitioners such as [[Hisashi Owada]], [[Xue Hanqin]], [[B. S. Chimni]], [[Tommy Koh]], Onuma Yasuaki, and [[Michael Hwang]].<ref>[http://journals.cambridge.org/action/displayIssue?decade=2010&jid=AJL&volumeId=1&issueId=01 ''Asian Journal of International Law'', 1(1) (2011), table of contents.]; [http://ilreports.blogspot.com/2011/01/inaugural-issue-asian-journal-of.html International Law Reporter, Inaugural Issue: Asian Journal of International Law, 26 January 2011.]</ref>

The launch of the Journal was welcomed as, perhaps, exemplifying a newly assertive Asia challenging the West in intellectual as well as economic terms.<ref>Boris N. Mamlyuk & Ugo Mattei , “Comparative International Law”, ''Brooklyn Journal of International Law'', 2011, vol. 36, p. 385 at 441. See also [http://twelvetables.wordpress.com/2010/11/22/%e0%b8%aa%e0%b8%a1%e0%b8%b2%e0%b8%84%e0%b8%a1%e0%b8%81%e0%b8%8e%e0%b8%ab%e0%b8%a1%e0%b8%b2%e0%b8%a2%e0%b8%a3%e0%b8%b0%e0%b8%ab%e0%b8%a7%e0%b9%88%e0%b8%b2%e0%b8%87%e0%b8%9b%e0%b8%a3%e0%b8%b0%e0%b9%80-2/ Twelve Tables (Thai)]; [http://neiarcadas.wordpress.com/2011/02/10/asianjil/ Blog do NEI (Portuguese)].</ref>

The journal is abstracted and indexed in [[Scopus]].

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.AsianJIL.org}}

{{NUS Faculty of Law}}
[[Category:International law journals]]
[[Category:Biannual journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:Publications established in 2011]]
[[Category:English-language journals]]
[[Category:Academic journals associated with universities and colleges]]
[[Category:Academic journals associated with learned and professional societies]]