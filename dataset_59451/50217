{{Use dmy dates|date=April 2012}}
{{Infobox UN resolution
|number = 937
|organ = SC
|date = 21 July
|year = 1994
|meeting = 3,407
|code = S/RES/937
|document = http://undocs.org/S/RES/937(1994)
|for = 14
|abstention = 0
|against = 0
|subject = Abkhazia, Georgia
|result = Adopted 
|image = Abkhazia Kodori Valley.PNG
|caption = [[Kodori Valley]] (highlighted) in Abkhazia
}}

'''United Nations Security Council resolution 937''', adopted on 21 July 1994, after reaffirming resolutions [[United Nations Security Council Resolution 849|849]] (1993), [[United Nations Security Council Resolution 854|854]] (1993), [[United Nations Security Council Resolution 858|858]] (1993), [[United Nations Security Council Resolution 876|876]] (1993), [[United Nations Security Council Resolution 881|881]] (1993), [[United Nations Security Council Resolution 892|892]] (1993), [[United Nations Security Council Resolution 896|896]] (1994), [[United Nations Security Council Resolution 901|901]] (1994), [[United Nations Security Council Resolution 906|906]] (1994) and [[United Nations Security Council Resolution 934|934]] (1994), the Council expanded the [[United Nations Observer Mission in Georgia]] (UNOMIG) to include co-operation with the [[Commonwealth of Independent States]] (CIS) and extended its mandate until 13 January 1995.<ref>{{cite book|last=Bothe|first=Michael|author2=Kondoch, Boris |title=International Peacekeeping: The Yearbook of International Peace Operations|publisher=Martinus Nijhoff Publishers|date=2002|page=214|isbn=978-90-411-1920-9}}</ref>

The Council reaffirmed the [[territorial integrity]] and sovereignty of [[Georgia (country)|Georgia]] and the right of all [[refugee]]s and displaced persons to return home. It also welcomed the [[Agreement on a Cease-fire and Separation of Forces]] signed between the [[Abkhazia|Abkhaz]] and Georgian sides in Moscow, Russia and recognised other agreements. It was important that negotiations continued to reach a political settlement both mutually acceptable to both parties.<ref>{{cite book|last=Gray|first=Christine D.|title=International law and the use of force|publisher=Oxford University Press|date=2000|page=226|isbn=978-0-19-876528-8}}</ref> The deployment of a CIS [[peacekeeping]] force depended on the consent of the parties. The parties were also urged to ensure complete [[freedom of movement]] for the CIS peacekeeping force and UNOMIG.

The parties were urged to accelerate negotiations to find a political settlement under the auspices of the United Nations with participation from [[Russia]] and representatives of the [[Organization for Security and Co-operation in Europe]]. The Secretary-General [[Boutros Boutros-Ghali]] was authorised to strengthen UNOMIG by 136 military observers, and expanded its mandate to include:<ref>{{cite book|last=Hilaire|first=Max|title=United Nations law and the Security Council|publisher=Ashgate Publishing, Ltd|date=2005|page=171|isbn=978-0-7546-4489-7}}</ref>

:(a) monitoring implementation of the Agreement on a Cease-fire and Separation of Forces;
:(b) observing the CIS peacekeeping force;
:(c) ensuring that troops and heavy weapons remain outside the security zone;
:(d) monitoring storage of heavy weapons;
:(e) monitoring of the withdrawal of Georgian troops from the [[Kodori Valley]];
:(f) patrolling the Kodori Valley;
:(g) investigating violations of agreements;
:(h) reporting on the implementation of the Agreement, violations and other developments;
:(i) maintaining contact with both parties, co-operate with the CIS and with its presence, ensuring the safe return of displaced persons.

The Secretary-General was further asked to set up a fund to support the implementation of the agreements and [[humanitarian aid|humanitarian efforts]], including [[demining]]. Within three months, he was required to report to the Council on developments in the situation. 

Resolution 937 was adopted by 14 votes to none; [[Rwanda]] was absent.<ref>{{cite book|last=United Nations, Dept. of Public Information,|title=The United Nations and the situation in Georgia|publisher=United Nations, Dept. of Public Information,|date=1995|page=30}}</ref>

==See also==
* [[Georgian–Abkhazian conflict]]
* [[List of United Nations Security Council Resolutions 901 to 1000]] (1994–1995)
* [[United Nations resolutions on Abkhazia]]
* [[War in Abkhazia (1992–1993)]]

==References==
{{reflist|colwidth=30em}}

==External links==
*[http://undocs.org/S/RES/937(1994) Text of the Resolution at undocs.org]
{{wikisource}}

{{UNSCR 1994}}

[[Category:1994 United Nations Security Council resolutions]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:1994 in Georgia (country)]]
[[Category:1994 in Abkhazia]]
[[Category:United Nations Security Council resolutions concerning Georgia (country)]]
[[Category:United Nations Security Council resolutions concerning Abkhazia]]
[[Category:July 1994 events]]