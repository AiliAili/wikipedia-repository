{{Good article}}
{{Infobox football match
|                   title = 1984 Intercontinental Cup
|                   image = [[File:ToyotaCup1984.jpg|200px]]
|                 caption = Match programme cover
|                   team1 = [[Liverpool F.C.|Liverpool]]
|        team1association = {{flagicon|ENG|size=30px}}
|              team1score = 0
|                   team2 = [[Club Atlético Independiente|Independiente]]
|        team2association = {{flagicon|ARG|size=30px}}
|              team2score = 1
|                    date = 9 December 1984
|                 stadium = [[National Olympic Stadium (Tokyo)|National Stadium]]
|                    city = [[Tokyo]]
|      man_of_the_match1a = [[José Alberto Percudani|José Percudani]] ([[Club Atlético Independiente|Independiente]])<ref name=toyota>{{cite web |url=http://www.rsssf.com/tablest/toyotamvp.html |title=Toyota Cup – Most Valuable Player of the Match Award |first = Marceloi|last = Lene de Arruda|publisher =Rec. Sport. Soccer. Statistics. Foundation |date = 2 January 2009 |accessdate=23 December 2009 }}</ref>
|                 referee = [[Romualdo Arppi Filho]] ([[Brazilian Football Confederation|Brazil]])
|              attendance = 62,000
|                 weather = 
|                previous = [[1983 Intercontinental Cup|1983]]
|                    next = [[1985 Intercontinental Cup|1985]]
}}
The '''1984 Intercontinental Cup''' was an [[association football]] match between [[Liverpool F.C.]] of England and [[Club Atlético Independiente]] of Argentina on 9 December 1984 at the [[Olympic Stadium (Tokyo)|National Stadium]] in [[Tokyo]], Japan, the annual [[Intercontinental Cup (football)|Intercontinental Cup]] contested between the winners of the [[Copa Libertadores]] and [[European Cup]]. Independiente were appearing in their sixth Intercontinental Cup, they had won the competition once in [[1973 Intercontinental Cup|1973]] and lost the other four. Liverpool were making their second appearance in the competition, after their loss in [[1981 Intercontinental Cup|1981]].

The teams had qualified for the competition by winning their continent's primary cup competition. Independiente qualified by winning the primary South American cup competition, the [[Copa Libertadores]]. They won the [[1984 Copa Libertadores]] defeating Brazilian team [[Grêmio Foot-Ball Porto Alegrense|Grêmio]] 3–1 on points in the [[1984 Copa Libertadores#Final|finals]]. Liverpool qualified by winning the primary European cup competition, the [[UEFA Champions League|European Cup]]. They beat Italian team [[A.S. Roma]] 4–2 in a [[Penalty shoot-out (association football)|penalty shoot-out]] after the match finished [[1984 European Cup Final|1–1]].

Watched by a crowd of 62,000, Independiente took the lead in the sixth minute when [[José Alberto Percudani|José Percudani]] scored. Liverpool had the better of the possession during the match, but they were unable to convert their chances ad the match finished in a 1–0 victory to Independiente. The win was the Argentine club's second triumph in the competition and the fifth in a row by the South American team.

==Match== 
===Background===
[[File:Shibuya 2008 Aug 10.jpg|thumb|left|The [[National Olympic Stadium (Tokyo)|National Stadium]] in Tokyo, which hosted the match.]]
Independiente qualified for the Intercontinental Cup as the reigning [[Copa Libertadores]] winners. They had won the [[1984 Copa Libertadores]] beating [[Grêmio Foot-Ball Porto Alegrense|Grêmio]] 3–1 on points over two legs in the [[1984 Copa Libertadores#Final|finals]]. It would be Independiente's sixth appearance in the competition. Their previous five appearances had resulted in one win in [[1973 Intercontinental Cup|1973]] and four defeats in [[1964 Intercontinental Cup|1964]], [[1965 Intercontinental Cup|1965]], [[1972 Intercontinental Cup|1972]] and [[1974 Intercontinental Cup|1974]].<ref name=intercontinental>{{cite web|url=http://www.rsssf.com/tablest/toyota.html |title=Intercontinental Club Cup |publisher=Rec.Sport.Soccer Statistics Foundation (RSSSF) |date=30 April 2005 |accessdate=15 December 2011 |first1=Karel |first2=Loris |last1=Stokkermans |last2=Magnani }}</ref>

Liverpool had qualified for the Intercontinental Cup as a result of winning the [[1983–84 European Cup]]. They had beaten [[A.S. Roma|Roma]] 4–2 in a [[Penalty shoot-out (association football)|penalty shoot-out]] after the match finished [[1984 European Cup Final|1–1]] to win their fourth European Cup. Liverpool were appearing in their second Intercontinental Cup. Their appearance in [[1981 Intercontinental Cup|1981]] resulted in a 3–0 defeat against [[Clube de Regatas do Flamengo|Flamengo]]. Liverpool were scheduled to appear in [[1977 Intercontinental Cup|1977]] and 1978 but did not compete. They declined to play in 1977 and were replaced by runners-up [[Borussia Mönchengladbach]], while in 1978, Liverpool and [[Boca Juniors]] declined to play each other.<ref name=intercontinental />

Liverpool's last match before the Intercontinental cup was against [[Coventry City F.C.|Coventry City]] in the [[1984–85 Football League]]. They won 3–1 courtesy of two goals from [[John Wark]] and one from [[Ian Rush]].<ref>{{cite web|url=http://www.lfchistory.net/SeasonArchive/Game/1431 |title=Liverpool 3–1 Coventry City |publisher=LFC History |accessdate=3 June 2015 }}</ref> The last match Independiente played before the Intercontinental Cup was against [[Rosario Central]] in the [[1984 Argentine Primera División]], which they lost 1–0.<ref name=argentina>{{cite web|url=http://www.rsssf.com/tablesa/arg84.html |title=Argentina 1984 |publisher=Rec. Sport. Soccer. Statistics. Foundation |date=20 March 2009 |accessdate=3 June 2015 |first=Pablo |last=Ciullini |deadurl=yes |archiveurl=https://web.archive.org/web/20150719213314/http://www.rsssf.com/tablesa/arg84.html |archivedate=19 July 2015 |df= }}</ref>

===Summary===
[[File:Independiente tokyo 1984.jpg|thumb|right|Independiente ''(wearing red)'' celebrate their victory.]]
Before the match, Liverpool lost defender [[Mark Lawrenson]] who had injured his hamstring in training. [[Gary Gillespie]] was his replacement.<ref name="times">{{cite news|title=Liverpool fail to bridge the gulf on the field |work=The Times |location=London |date=10 December 1984 |first=Gerry |last=Harrison }}</ref> Liverpool kicked off the match and the first few exchanges saw a number of rash tackles. Independiente defender [[Carlos Enrique]] tackled [[Craig Johnston]] robustly, but the referee indicated to play on. Moments later Liverpool midfielder [[Jan Mølby]] tackled Enrique late, which prompted the referee to award a [[Direct free kick|free kick]]. Liverpool controlled the opening exchanges of the match, but could not work the ball into the Independiente penalty area.<ref name="fourfourtwo">{{cite news|url=http://www.fourfourtwo.com/features/elusive-trophy-thirty-years-how-liverpools-greatest-team-failed-rule-world |title=The elusive trophy: thirty years on, how Liverpool's greatest team failed to rule the world |work=FourFourTwo |date=4 November 2014 |accessdate=3 June 2015 |first=Luke |last=Ginnell }}</ref> However, it was Independiente who opened the scoring, [[Claudio Marangoni]] sent a ball over the Liverpool defence for striker [[José Alberto Percudani|José Percudani]], whose low shot beat the advancing Liverpool goalkeeper [[Bruce Grobbelaar]] to give Independiente a 1–0 lead.<ref>{{cite web|url=http://en.archive.uefa.com/competitions/eusa/history/season=1984/intro.html |title=1984: Independiente deny Liverpool |publisher=Union of European Football Associations (UEFA) |date=9 December 1984 |accessdate=15 December 2011 }}</ref>

Following the goal, Independiente under instruction rom their manager [[José Pastoriza]], began to sit back in their own half inviting Liverpool to attack them. Their plan worked as Liverpool were unable to break down their defence, while their strikers Percudani and [[Alejandro Barberón]] counter-attacked when Independiente received the ball.<ref name=fourfourtwo /> The second half saw Liverpool continue to attack the Independiente goal, but to no avail. Midfielders [[John Wark]] and Mølby tried to find a way through the Independiente defence was equal to their efforts. Wark's efforts trying to engineer an equalising goal resulted in him being substituted for [[Ronnie Whelan]] in the 76th minute, due to fatigue.<ref name=fourfourtwo />

Despite being the better team for the majority of the match, Liverpool were unable to beat the Independiente defence, with their shooting in front of goal being the culprit. Both sides questioned some of the referee's decisions. Liverpool believed they should have had two penalties, while Independiente felt that the assistant referee's decisions were questionable. Incidentally, the referee had served half of his two match ban handed out by the Brazilian Football Association.<ref name="times" />

===Details===
{{footballbox
|date=9 December 1984
|time=12:00 [[Japan Standard Time|JST]]
|team1=[[Liverpool F.C.|Liverpool]] {{flagicon|ENG}}
|score=0–1
|report=<ref>{{cite web|url=http://www.fifa.com/classicfootball/clubs/matchreport/newsid=512088.html |title=Toyota Cup 1984 |publisher=FIFA |accessdate=4 June 2015 |archivedate=March 6, 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20080306222947/http://www.fifa.com/classicfootball/clubs/matchreport/newsid=512088.html }}</ref>
|team2={{flagicon|ARG}} [[Club Atlético Independiente|Independiente]]
|goals2=[[José Alberto Percudani|Percudani]] {{goal|6}}
|stadium=[[National Olympic Stadium (Tokyo)|National Stadium]], [[Tokyo]]
|attendance=62,000
|referee=[[Romualdo Arppi Filho]] ([[Brazilian Football Confederation|Brazil]])
}}

{| width=92%
|-
|{{Football kit
 | pattern_la = _redborder
 | pattern_b  = _vneckred
 | pattern_ra = _redborder
 | leftarm    = FFFF66
 | body       = FFFF66
 | rightarm   = FFFF66
 | shorts     = FFFF66
 | socks      = FFFF66
 | title      = Liverpool

}}
|{{Football kit
 | pattern_b  = _Ind81
 | leftarm    = FF0000
 | body       = FF0000
 | rightarm   = FF0000
 | shorts     = 191970
 | socks      = 191970
 | title      = Independiente
}}
|}

{| width="100%"
|valign="top" width="40%"|
{| style="font-size:90%" cellspacing="0" cellpadding="0"
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|ZIM}} [[Bruce Grobbelaar]]
|-
|RB ||'''2''' ||{{flagicon|ENG}} [[Phil Neal]] ([[Captain (association football)|c]])
|-
|CB ||'''6''' ||{{flagicon|SCO}} [[Alan Hansen]]
|-
|CB ||'''15'''||{{flagicon|SCO}} [[Gary Gillespie]]
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Alan Kennedy]]
|-
|RM ||'''10'''||{{flagicon|ENG}} [[Craig Johnston]]
|-
|CM ||'''11'''||{{flagicon|SCO}} [[John Wark]] || || {{suboff|76}}
|-
|CM ||'''8''' ||{{flagicon|DEN}} [[Jan Mølby]] || {{yel|42}}
|-
|LM ||'''5''' ||{{flagicon|SCO}} [[Steve Nicol]]
|-
|CF ||'''7''' ||{{flagicon|SCO}} [[Kenny Dalglish]]
|-
|CF ||'''9''' ||{{flagicon|WAL}} [[Ian Rush]]
|-
|colspan=3|'''Substitutes:'''
|-
|GK ||'''13'''||{{flagicon|ENG}} [[Bob Bolder]]
|-
|MF ||'''12'''||{{flagicon|IRL}} [[Ronnie Whelan]] || || {{subon|76}}
|-
|MF ||'''14'''||{{flagicon|SCO}} [[Kevin MacDonald (footballer)|Kevin MacDonald]]
|-
|FW ||'''16'''||{{flagicon|IRL}} [[Michael Robinson (footballer)|Michael Robinson]]
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|ENG}} [[Joe Fagan]]
|}
|valign="top"|
|valign="top" width="50%"|
{| style="font-size: 90%; margin: auto;" cellspacing="0" cellpadding="0"
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|URU}} [[Carlos Goyén]]
|-
|RB ||'''4''' ||{{flagicon|ARG}} [[Néstor Clausen]] || {{yel|72}}
|-
|CB ||'''2''' ||{{flagicon|ARG}} [[Hugo Villaverde]] || || {{suboff|74}}
|-
|CB ||'''6''' ||{{flagicon|ARG}} [[Enzo Trossero]] ([[Captain (association football)|c]])
|-
|LB ||'''3''' ||{{flagicon|ARG}} [[Carlos Enrique]]
|-
|RM ||'''8''' ||{{flagicon|ARG}} [[Ricardo Giusti]]
|-
|CM ||'''5''' ||{{flagicon|ARG}} [[Claudio Marangoni]]
|-
|LM ||'''7''' ||{{flagicon|ARG}} [[Jorge Burruchaga]]
|-
|AM ||'''10'''||{{flagicon|ARG}} [[Ricardo Bochini]]
|-
|CF ||'''11'''||{{flagicon|ARG}} [[Alejandro Barberón]]
|-
|CF ||'''9''' ||{{flagicon|ARG}} [[José Alberto Percudani|José Percudani]]
|-
|colspan=3|'''Substitutes:'''
|-
|GK ||''' '''||{{flagicon|ARG}} [[Gustavo Moriconi]]
|-
|DF ||''' ''' ||{{flagicon|ARG}} [[Rodolfo Zimmermann]]
|-
|DF ||'''13'''||{{flagicon|ARG}} [[Pedro Monzón]] || || {{subon|74}}
|-
|MF ||''' '''||{{flagicon|ARG}} [[Gerardo Reinoso]]
|-
|MF ||''' '''||{{flagicon|ARG}} [[Sergio Merlini]]
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|ARG}} [[José Omar Pastoriza|José Pastoriza]]
|}
|}

{| style="width:100%; font-size:90%;"
|
'''Man of the Match:''' 
<br />{{flagicon|ARG}} [[José Alberto Percudani|José Percudani]] (Independiente)<ref name=toyota />
|}

==Post-match==
Despite the defeat, Liverpool manager [[Joe Fagan]] could not fault the effort his players had put in: "Independiente are a good defensive tactical team and we could find no way through, the weather was ideal, we were just as fit as they were. The South Americans have better ball control than we do. We were disappointed with the result but I wasn't disappointed with the display."<ref name="times" />

Liverpool finished the [[1984–85 Football League]] in second place 13 points behind local rivals [[Everton F.C.|Everton]].<ref>{{cite web|url=http://www.rsssf.com/engpaul/FLA/1984-85.html |title=Season 1984–85 |publisher=Rec. Sport. Soccer. Statistics. Foundation |accessdate=3 June 2015 |first=Paul |last=Felton }}</ref> They also reached the [[1985 European Cup Final|final]] of the [[1984–85 European Cup]], which they lost 1–0 to [[Juventus F.C.|Juventus]]. However the events of the match were overshadowed for the [[Heysel Stadium disaster|disaster]] that occurred before kick-off. Liverpool fans breached a fence separating the two groups of supporters and charged the Juventus fans. The resulting weight of people caused a retaining wall to collapse, killing 39 people and injuring hundreds. English clubs were banned indefinitely from European competition, with a condition that when the ban was lifted, Liverpool would serve an extra three-year ban.<ref>{{harvtxt|Ponting|1992|p=189}}</ref> The ban eventually lasted for five years, clubs returning to European competition in the 1990–91 season.<ref>{{harvtxt|Hutchings,Nawrat|1995|p=251}}</ref>

Independiente would finish their season in the Primera Division in 14th place.<ref name=argentina /> Despite this, they competed in the [[1985 Copa Libertadores]] as the reigning champions. However, they were unable to retain their title as they exited in the semi-finals.<ref>{{cite web|url=http://www.rsssf.com/sacups/copa85.html |title=Copa Libertadores de América |publisher=Rec. Sport. Soccer. Statistics. Foundation |date=29 November 2012 |accessdate=3 June 2015 |first1=Pablo |last1=Ciullini |first2=Karel |last2=Stokkermans }}</ref>

==See also==
*[[1983–84 European Cup]]
*[[1984 Copa Libertadores]]

==Footnotes==
{{Reflist|2}}

==References==
*{{cite book |last1=Hale |first1=Steve |last2=Ponting |first2=Ivan |title=Liverpool In Europe |year=1992 |publisher=Guinness Publishing |location=London |isbn=0-85112-569-7 }}
*{{cite book |last1=Hutchings |first1=Steve |last2=Nawrat |first2=Chris |title=The Sunday Times Illustrated History of Football: The Post-War Years |publisher=Chancellor Press |year=1995 |location=London |isbn=1-85153-014-2 }}

==External links==
*[https://web.archive.org/web/20080306222947/http://www.fifa.com/classicfootball/clubs/matchreport/newsid=512088.html FIFA Article] Archived
*[http://www.lfchistory.net/SeasonArchive/Game/1432 Match Report] at LFC History

{{Intercontinental Cup (football)}} 
{{Liverpool F.C. matches}} 
{{Club Atlético Independiente matches}} 

[[Category:1984–85 in European football|Intercontinental Cup]]
[[Category:1984 in South American football|Intercontinental Cup]]
[[Category:1984 in Japanese football|Intercontinental Cup]]
[[Category:Liverpool F.C. matches|Intercontinental Cup 1984]]
[[Category:Club Atlético Independiente matches|Intercontinental Cup 1984]]
[[Category:Intercontinental Cup (football)]]
[[Category:Intercontinental Cup (football) matches hosted by Japan]]
[[Category:1984 in Argentine football|Inter]]
[[Category:1984–85 in English football|Inter]]
[[Category:Sports competitions in Tokyo]]
[[Category:December 1984 sports events]]
[[Category:1980s in Tokyo]]