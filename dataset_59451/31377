{{coord |53.7237|-1.8672|display=title|format=dms}}

[[File:The Halifax Gibbet - geograph.org.uk - 350422.jpg|thumb|alt=photograph|A replica of the Halifax Gibbet on its original site, 2008, with St Mary's Catholic church, Gibbet Street, in the background]]

The '''Halifax Gibbet''' {{IPAc-en|ˈ|h|æ|l|ɪ|f|æ|k|s|_|ˈ|dʒ|ɪ|b|ɪ|t}} was an early [[guillotine]], or [[decapitation|decapitating]] machine, used in the town of [[Halifax, West Yorkshire]], England. It was probably installed during the 16th&nbsp;century as an alternative to beheading by axe or sword. Halifax was once part of the Manor of [[Wakefield]], where ancient custom and law gave the [[Lord of the Manor]] the authority to execute summarily by decapitation any thief caught with stolen goods to the value of 13½[[Pound sterling#Pre-decimal|d]] or more, or who confessed to having stolen goods of at least that value. Decapitation was a fairly common method of execution in England, but Halifax was unusual in two respects: it employed a guillotine-like machine that appears to have been unique in the country, and it continued to decapitate petty criminals until the mid-17th century.

The device consisted of an axe head fitted to the base of a heavy wooden block that ran in grooves between two {{convert|15|ft|adj=on}} tall uprights, mounted on a stone base about {{convert|4|ft}} high. A rope attached to the block ran over a pulley, allowing it to be raised, after which the rope was secured by attaching it to a pin in the base. The block carrying the axe was then released either by withdrawing the pin or by cutting the rope once the prisoner was in place.

Almost 100 people were beheaded in Halifax between the first recorded execution in 1286 and the last in 1650, but as the date of the [[gibbeting|gibbet]]'s installation is uncertain, it cannot be determined with any accuracy how many were dealt with by the Halifax Gibbet. By 1650 public opinion considered beheading to be an excessively severe punishment for petty theft; use of the gibbet was forbidden by [[Oliver Cromwell]], [[Lord Protector]] of the [[Commonwealth of England]], and the structure was dismantled. The stone base was rediscovered and preserved in about 1840, and a non-working replica was erected on the site in 1974. The names of 52 people known to have been beheaded by the device are listed on a nearby plaque.

== History ==
What became known as the Halifax Gibbet Law gave the [[Lord of the Manor]] of Wakefield, of which the town of Halifax was a part,{{efn|The manor of Wakefield was one of the largest in England, stretching well beyond the boundaries of the present-day town of the same name. It occupied an area of about {{convert|30|mi}} east to west and about {{convert|20|mi}} north to south.{{sfnp|Holt|1997|p=19|ps=none}}}} the power to try and execute any felon for the theft of goods to the value of 13½[[British one penny coin (pre-decimal)|d]] or more:{{efn|In 1650, the year of the last executions, 13½d would have been the equivalent of about £5.40 in [[purchasing power]] as of 2008.<ref name="MeasuringWorth" />}}
{{quote|If a felon be taken within their [[Liberty (division)|liberty]] or precincts of the said forest [the Forest of Hardwick], either [[handhabend]] [caught with the stolen goods in his hand or in the act of stealing], backberand [caught carrying stolen goods on his back], or confessand [having confessed to the crime] cloth or any other commodity to the value of 13½d, that they shall after three market days or meeting days within the town of Halifax after such his apprehension, and being condemned he shall be taken to the gibbet and there have his head cut off from his body.{{sfnp|Carter|1986|p=284|ps=none}}}}

[[File:HalifaxGibbet.png|thumb|left|upright|alt=Schematic of the gibbet's main parts|17th-century engraving]]
The Gibbet Law may have been a last vestige of the [[Anglo-Saxon]] custom of [[infangthief and outfangthief|infangtheof]], which allowed landowners to enforce summary justice on thieves within the boundaries of their estates.{{sfnp|Judges|2002|p=lix|ps=none}} Samuel Midgley in his ''Halifax and its Gibbet-Law Placed in a True Light'',{{efn|Samuel Midgley died on 18&nbsp;July 1695, imprisoned in Halifax Gaol for debt. While in prison he wrote ''Halifax and its Gibbet-Law Placed in a True Light'' in an effort to support himself, but he could not afford to have it published. The manuscript came into the possession of William Bentley, the Halifax parish clerk, who decided to publish it under his own name, perhaps with some additions. William's son John produced a subsequent reprint under his own name, hence many sources refer to the book as John Bentley's ''Halifax and its Gibbet-Law''.<ref name="Turner 1906 p18–19" />}} published in 1761, states that the law dates from a time "not in the memory of man to the contrary".{{sfnp|Midgley|Bentley|1761|p=397|ps=none}} It may have been the consequence of rights granted by King [[Henry III of England|Henry III]] to [[John de Warenne, 6th Earl of Surrey|John de Warenne]] (1231–1304), Lord of the Manor of Wakefield.<ref name="ImperialMag" /> Such baronial jurisdiction was by no means unusual in medieval England and was described in the 11th-century legal text entitled ''De Baronibus, qui suas habent curias et consuetudines'' (Concerning the barons who have their courts of law and customs).<ref name="Copinger" /> Neither was the decapitation of convicted felons unique to Halifax; the [[Earl of Chester|earls of Chester]] amongst others also exercised the right to "behead any malefactor or thief, who was apprehended in the action, or against whom it was made apparent by sufficient witness, or confession, before four inhabitants of the place", recorded as the Custom of Cheshire.{{sfnp|Midgley|Bentley|1761|p=432|ps=none}}

A commission appointed by King [[Edward I of England|Edward I]] in 1278 reported that there were at that time 94 privately owned gibbets and [[gallows]] in use in Yorkshire, including one owned by the [[Archbishop of York]].{{sfnp|Pettifer|1992|pp=83–85|ps=none}} What was unusual about Halifax was that the custom lingered on there for so long after it had been abandoned elsewhere.<ref name="ImperialMag" />

Suspected thieves were detained in the custody of the lord of the manor's [[bailiff]], who would summon a jury of 16 local men "out of the most wealthy and best reputed", four each from four local townships.<ref name="SaturdayMag" /> The jury had only two questions to decide on: were the stolen goods found in the possession of the accused, and were they worth at least 13½d.{{sfnp|Holt|1997|p=22|ps=none}} The jury, the accused, and those claiming that their property had been stolen, were brought together in a room at the bailiff's house. No oaths were administered and there was no judge or defence counsel present; each side presented their case, and the jury decided on guilt or innocence.{{sfnp|Midgley|Bentley|1761|pp=417–423|ps=none}}

The law was applied so strictly that anyone who apprehended a thief with his property was not allowed to recover it unless the miscreant and the stolen goods were presented to the bailiff. The goods were otherwise forfeited to the lord of the manor, and their previous rightful owner was liable to find himself charged with [[theftbote]], or conniving in the felony.<ref name="ImperialMag" /> Halifax's reputation for strict law enforcement was noted by the antiquary [[William Camden]] and by the "Water Poet" [[John Taylor (poet)|John Taylor]], who penned the [[Beggar's Litany]]: "From Hell, Hull, and Halifax, Good Lord, deliver us!"{{sfnp|Lipson|1965|p=242|ps=none}}{{efn|Felons were not decapitated in [[Kingston upon Hull|Hull]], but were tied to gibbets in the [[Humber|Humber estuary]] at low tide and left to drown as the sea returned.{{sfnp|Peach|2010|p=74|ps=none}}}}

Before his execution a convicted felon was usually detained in custody for three market days,{{efn|Halifax at that time held three markets each week.{{sfnp|Carter|1986|p=285|ps=none}}}} on each of which he was publicly displayed in the [[stocks]], accompanied by the stolen goods.{{sfnp|Carter|1986|p=285|ps=none}} After the sentence had been carried out a county [[coroner]] would visit Halifax and convene a jury of 12&nbsp;men, sometimes the same individuals who had found the felon guilty, and ask them to give an account under oath of the circumstances of the conviction and execution, for the official records.<ref name="SaturdayMag" />

The punishment could only be meted out to those within the confines of the Forest of Hardwick, of which Halifax was a part. The gibbet was about {{convert|500|yd}} from the boundary of the area, and if the condemned person succeeded in escaping from the forest then he could not legally be brought back to face his punishment. At least two men succeeded in cheating the executioner in that way: a man named Dinnis and another called Lacy. Dinnis was never seen in Halifax again, but Lacy rather unwisely decided to return to the town seven years after his escape; he was apprehended and finally executed in 1623.{{sfnp|Pettifer|1992|pp=86–87|ps=none}}

The earliest known record of punishment by decapitation in Halifax is the beheading of John of Dalton in 1286,<ref name="CalderdaleROAM" /> but official records were not maintained until the parish registers began in 1538. Between then and 1650, when the last executions took place, 56 men and women are recorded as having been decapitated. The total number of executions identified since 1286 is just short of 100.{{sfnp|Holt|1997|p=20|ps=none}}

Local weavers specialised in the production of [[kersey (cloth)|kersey]], a hardwearing and inexpensive woollen fabric that was often used for military uniforms; by the 16th&nbsp;century Halifax and the surrounding [[Calder Valley]] was the largest producer of the material in England. In the final part of the manufacturing process the cloth was hung outdoors on large structures known as tenterframes and left to dry, after having been conditioned by a [[fulling]] mill.<ref name="Calderdale" /> [[Daniel Defoe]] wrote a detailed account of what he had been told of the gibbet's history during his visit to Halifax in Volume 3 of his ''[[A tour thro' the whole island of Great Britain]]'', published in 1727.{{sfnp|Lipson|1965|p=242|ps=none}}
He reports that "Modern accounts pretend to say, it [the gibbet] was for all sorts of felons; but I am well assured, it was first erected purely, or at least principally, for such thieves as were apprehended stealing cloth from the tenters; and it seems very reasonable to think it was so".<ref name="Defoe" />

Eighteenth-century historians argued that the area's prosperity attracted the "wicked and ungovernable"; the cloth, left outside and unattended, presented easy pickings, and hence justified severe punishment to protect the local economy. James Holt on the other hand, writing in 1997, sees the Halifax Gibbet Law as a practical application of the Anglo-Saxon law of ''infangtheof''. Royal [[assizes]] were held only twice a year in the area; to bring a prosecution was "vastly expensive", and the stolen goods were forfeited to the Crown, as they were considered to be the property of the accused.{{sfnp|Holt|1997|p=23|ps=none}} But the Halifax Gibbet Law allowed "the party injured, to have his goods restored to him again, with as little loss and damage, as can be contrived; to the great encouragement of the honest and industrious, and as great terror to the wicked and evil doers."{{sfnp|Midgley|Bentley|1761|p=404|ps=none}}

The Halifax Gibbet's final victims were Abraham Wilkinson and Anthony Mitchell.{{sfnp|Pettifer|1992|p=87|ps=none}}{{efn|Abraham Wilkinson's brother John was also accused of involvement in the thefts, but although he admitted to being present when they were carried out he would not confess to having taken part in them, and he was consequently released.{{sfnp|Midgley|Bentley|1761|pp=422–423|ps=none}} Several sources claim that it was John Wilkinson who was executed rather than his brother Abraham, but Midgley attributes this to an error in the parish register.{{sfnp|Midgley|Bentley|1761|p=407|ps=none}}}} Wilkinson had been found guilty of stealing {{convert|16|yd}} of russet-coloured kersey cloth, 9 yards of which, found in his possession, was valued at "9&nbsp;shillings at the least", {{sfnp|Midgley|Bentley|1761|p=423|ps=none}}<ref name="CalderdaleROAM" /> and Mitchell of stealing and selling two horses, one valued at 9&nbsp;shillings and the other at 48&nbsp;shillings.{{sfnp|Midgley|Bentley|1761|pp=422–423|ps=none}} The pair were found guilty and executed on the same day,{{sfnp|Midgley|Bentley|1761|p=413|ps=none}} 30&nbsp;April 1650.{{sfnp|Midgley|Bentley|1761|p=439|ps=none}} Writing in 1834 [[John William Parker]], publisher of ''[[The Saturday Magazine (magazine)|The Saturday Magazine]]'', suggested that the gibbet might have remained in use for longer in Halifax had the bailiff not been warned that if he used it again he would be "called to public account for it".<ref name="SaturdayMag" /> Midgley comments that the final executions "were by some persons in that age, judged to be too severe; thence came it to pass, that the gibbet, and the customary law, for the forest of Hardwick, got its suspension".{{sfnp|Midgley|Bentley|1761|p=417|ps=none}}

[[Oliver Cromwell]] finally ended the exercise of Halifax Gibbet Law. To the [[Puritans]] it was "part of ancient ritual to be jettisoned along with all the old feasts and celebrations of the medieval world and the Church of Rome". Moreover, it ran counter to the Puritan objection to imposing the death penalty for petty theft; felons were subsequently sent to the [[Assizes]] in [[York]] for trial.{{sfnp|Holt|1997|p=23|ps=none}}

== Mechanism ==
[[File:HalifaxGibbetAllen.jpg|thumb|250px|alt=Horse being led away to release the axe|Print of the Halifax Gibbet in use, from [[Thomas Allen (topographer)|Thomas Allen]]'s ''A New and Complete History of the County of York'' (1829)]]
It is uncertain when the Halifax Gibbet was first introduced, but it may not have been until some time in the 16th century; before then decapitation would have been carried out by an executioner using an axe or a sword. The device, which seems to have been unique in England,{{sfnp|Holt|1997|p=20|ps=none}} consisted of two {{convert|15|ft|adj=on}} tall parallel beams of wood joined at the top by a transverse beam. Running in grooves within the beams was a square wooden block {{convert|4|ft|6|in}} in length, into the bottom of which was fitted an axe head weighing {{convert|7|lb|12|oz}}.<ref name="SaturdayMag" /> The whole structure sat on a platform of stone blocks, {{convert|9|ft}} square and {{convert|4|ft}} high, which was ascended by a flight of steps.<ref name="CalderdaleROAM" /> A rope attached to the top of the wooden block holding the axe ran over a pulley at the top of the structure, allowing the block to be raised. The rope was then fastened by a pin to the structure's stone base.<ref name="SaturdayMag" />

The gibbet could be operated by cutting the rope supporting the blade or by pulling out the pin that held the rope. If the offender was to be executed for stealing an animal, a cord was fastened to the pin and tied to either the stolen animal or one of the same species, which was then driven off, withdrawing the pin and allowing the blade to drop.<ref name="SaturdayMag" />

In an early contemporary account of 1586 [[Raphael Holinshed]] attests to the efficiency of the gibbet, and adds some detail about the participation of the onlookers:
{{quote|In the nether end of the sliding block is an axe keyed or fastened with an iron into the wood, which being drawn up to the top of the frame is there fastened by a wooden pin&nbsp;... unto the middest of which pin also there is a long rope fastened that cometh down among the people, so that when the offender hath made his confession, and hath laid his neck over the nethermost block, every man there present doth either take hold of the rope (or putteth  forth his arm so near to the same as he can get, in token that he is willing to see true justice executed) and pulling out the pin in this manner, the head block wherein the axe is fastened doth fall down with such violence, that if the neck of the transgressor were so big as that of a bull, it should be cut in sunder at a stroke, and roll from the body by an huge distance.<ref name="Holinshed" />}}

An article in the September 1832 edition of ''[[The Imperial Magazine]]'' describes the victim's final moments:

{{quote|The persons who had found the verdict, and the attending clergymen, placed themselves on the scaffold with the prisoner. The [[Psalm 4|fourth psalm]] was then played round the scaffold on the bagpipes, after which the minister prayed with the prisoner till he received the final stroke.<ref name="ImperialMag" />{{efn|The [[Psalm 4|fourth psalm]] contains the words "I will both lay me down in peace and sleep".{{sfnp|Holt|1997|p=21|ps=none}}}}}}

In [[Thomas Deloney]]'s novel ''Thomas of Reading'' (1600) the invention of the Halifax Gibbet is attributed to a friar, who proposed the device as a solution to the difficulty of finding local residents willing to act as hangmen.{{sfnp|Pettifer|1992|p=86|ps=none}}

Although the guillotine as a method of beheading is most closely associated in the popular imagination with late 18th-century [[French Revolution|Revolutionary France]], several other decapitation devices had long been in use throughout Europe. It is uncertain whether [[Joseph-Ignace Guillotin|Dr Guillotin]] was familiar with the Halifax Gibbet, but its design was reported to have been copied by [[James Douglas, 4th Earl of Morton]], in the production of a similar device that became known as the [[Maiden (beheading)|Scottish Maiden]], now on display in the [[National Museum of Scotland]]. The Maiden was rather shorter than the Halifax Gibbet, standing only {{convert|10|ft}} tall, the same height as the French guillotine.{{sfnp|Tavernor|2007|p=90|ps=none}}

== Restoration ==
The Halifax Gibbet was dismantled after the last executions in 1650, and the site was neglected until the platform on which the gibbet had been mounted was rediscovered in about 1840.{{sfnp|Pettifer|1992|p=87|ps=none}} A full-size non-working replica was erected on the original stone base in August 1974; it includes a blade made from a casting of the original, which as of 2011 is displayed in the [[Bankfield Museum]] in [[Boothtown]] on the outskirts of Halifax.<ref name="Calderdale" /> A commemorative plaque nearby lists the names of the 52 people known to have been executed by the device.<ref name="CalderdaleROAM" />

== References ==
'''Notes'''
{{notelist|notes=}}

'''Citations'''
{{reflist|20em|refs=

<ref name="MeasuringWorth">
{{cite web |last=Officer |first=Lawrence H. |year=2009 |title=Purchasing Power of British Pounds from 1264 to Present |publisher=MeasuringWorth |url=http://www.measuringworth.com/ppoweruk/index.php |accessdate=13 May 2011 |mode=cs2}}
</ref>

<ref name="Turner 1906 p18–19">
{{citation |last=Turner |first=Joseph Horsfall |year=1906 |title=Halifax Books and Authors: A Series of Articles on the Books Written by Natives and Residents, Ancient and Modern, of the Parish of Halifax (Stretching from Todmorden to Brighouse), With Notices of their Authors and of the Local Printers |pages=18–19 |publisher=Internet Archive |url=https://archive.org/stream/halifaxbooksauth00turnuoft/halifaxbooksauth00turnuoft_djvu.txt
|accessdate=14 May 2011}}
</ref>

<ref name="ImperialMag">
{{cite journal |title=Gibbet Law of Halifax, Yorkshire |magazine=The Imperial Magazine |volume=II |series=2
|date=September 1832 |pages=407–409 |mode=cs2}}
</ref>

<ref name="Copinger">
{{citation |last=Copinger |first=W. A. |year=1905 |title=The Manors of Suffolk : notes on their history and devolution |publisher=Internet Archive [T. Fisher Unwin] |url=https://archive.org/stream/manorsofsuffolkn01copiuoft/manorsofsuffolkn01copiuoft_djvu.txt |accessdate=17 May 2011}}
</ref>

<ref name="SaturdayMag">
{{cite journal |last=Parker |first=John William |date=26 July 1834 |title=The Halifax Gibbet-Law |journal=[[The Saturday Magazine (magazine)|The Saturday Magazine]] |page=32 |issue=132 |mode=cs2}}
</ref>

<ref name="CalderdaleROAM">
{{cite web |title=Register of Ancient Monuments: Remains of Gibbet |publisher=Calderdale Council |url=http://www.calderdale.gov.uk/environment/conservation/ancient-monuments/monuments-full.jsp?propno=HSKFD7DWL2000
|accessdate=14 May 2011 |archiveurl= http://www.webcitation.org/6cNawny0F |archivedate=18 October 2015 |deadurl=no |mode=cs2}}
</ref>

<ref name="Calderdale">
{{cite web |title=The Piece Hall Halifax |page=5 |publisher=Calderdale Council |url=http://www.thepiecehall.co.uk/wp-content/uploads/2014/09/PIECE-HALL-HISTORY.pdf |format=PDF |accessdate=18 October 2015 |archiveurl=https://web.archive.org/web/20120319193747/http://www.thepiecehall.co.uk/wp-content/uploads/pdf/PIECE-HALL-HISTORY.pdf |archivedate=19 March 2012 |deadurl=yes |mode=cs2}}
</ref>

<ref name="Defoe">
{{cite web |last=Defoe |first=Daniel |year=1727 |title=A tour thro' the whole island of Great Britain, divided into circuits or journies |publisher=A Vision of Britain Through Time |location=Letter 8, Part 3: South and West Yorkshire |url=http://www.visionofbritain.org.uk/text/contents_page.jsp?t_id=Defoe |accessdate=17 May 2011 |mode=cs2}}
</ref>

<ref name="Holinshed">
{{cite web |last=Holinshed |first=Raphael |year=1586 |title=An Historicall Description of the Iland of Britaine <nowiki>[</nowiki>Selections<nowiki>]</nowiki> |publisher=Schoenberg Center for Electronic Text & Image
|page=185 |url=http://sceti.library.upenn.edu/sceti/printedbooksNew/index.cfm?TextID=holinshed_britain&PagePosition=195 |accessdate=14 May 2011 |mode=cs2}}
</ref>

}}

'''Bibliography'''
{{refbegin}}
* {{citation
  | last = Carter
  | first = A. T.
  | year = 1986
  | origyear = 1902
  | title = History of English Legal Institutions
  | publisher = Fred B. Rothman & Co.
  | isbn = 978-0-8377-2007-4
  }}
* {{citation
  | last = Holt
  | first = James Clarke
  | year = 1997
  | title = Colonial England, 1066–1215
  | publisher = Hambledon Continuum
  | isbn = 978-1-85285-140-8
  }}
* {{citation
  | last = Judges
  | first = A. V.
  | year = 2002
  | title = The Elizabethan Underworld: A Collection of Tudor and Early Stuart Tracts and Ballads
  | publisher = Routledge
  | isbn = 978-0-415-28676-3
  }}
* {{citation
  | last = Lipson
  | first = Ephraim
  | year = 1965
  | title = The History of the Woollen and Worsted Industries
  | publisher = Routledge
  | isbn = 978-0-7146-2339-9
  }}
* {{citation
  | last1 = Midgley
  | first1 = Samuel
  | last2 = Bentley
  | first2 = William
  | year = 1761
  | title = Halifax and its Gibbet-Law Placed in a True Light
  | url = https://books.google.com/?id=UlIPAAAAYAAJ&lpg=PA573&dq=Miggelay&pg=PP7#v=onepage&q=gibbet&f=false
  | publisher = J. Milner
  }}
* {{citation
  | last = Peach
  | first = Howard
  | year = 2010
  | title = Curious Tales of Old East Yorkshire
  | publisher = Sigma Press
  | isbn = 978-1-85058-749-1
  }}
* {{citation
  | last = Pettifer
  | first = Ernest William
  | year = 1992
  | title = Punishments of Former Days
  | origyear = 1947
  | edition = 2nd
  | publisher = Waterside Press
  | isbn = 978-1-872870-05-2
  }}
* {{citation
  | last = Tavernor
  | first = Robert
  | year = 2007
  | title = Smoot's Ear: The Measure of Humanity
  | publisher = Yale University Press
  | isbn = 978-0-300-12492-7
  }}
{{refend}}

== External links ==
* [http://www.guillotine.dk/Pages/gibbet.html The Guillotine Headquarters]
{{Featured article}}

[[Category:Execution equipment]]
[[Category:Execution methods]]
[[Category:Halifax, West Yorkshire]]
[[Category:History of West Yorkshire]]
[[Category:Execution sites in England]]