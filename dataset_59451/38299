{{good article}}
{{Infobox University Boat Race
| name= 125th Boat Race
| winner = Oxford
| margin = 3 1/2 lengths
| winning_time= 20 minutes 33 seconds
| date= {{Start date|1979|03|17|df=y}}
| umpire = Ronnie Howard<br>(Oxford)
| prevseason= [[The Boat Race 1978|1978]]
| nextseason= [[The Boat Race 1980|1980]]
| overall =68&ndash;56
| reserve_winner =Goldie
| women_winner =Cambridge
}}
The 125th [[The Boat Race|Boat Race]] took place on 17 March 1979.  Held annually, the event is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  The 150th anniversary race was won by Oxford by three-and-a-half-lengths. For the first time in 50 years, neither crew featured foreign rowers, while Cambridge's stroke was replaced just hours before the race.  Goldie won the reserve race in the slowest time in the history of the race while Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 August 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 20 August 2014 | publisher = The Boat Race Company Ltd}}</ref>  The rivalry is a major point of honour between the two universities, as of 2014 it is followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Ltd | accessdate = 5 July 2014}}</ref><ref>{{Cite book | url = https://books.google.com/books?id=o2QpA0fGyiIC&pg=PA287&lpg=PA287&dq=%22boat+race%22+%22united+kingdom%22+audience&source=bl&ots=WJsXwqiRfL&sig=5C_pRDSK839-C46kyEaZJXgjjSk&hl=en&sa=X&ei=gbElVLH2E8jW7Qat-oC4Aw&ved=0CCcQ6AEwAQ#v=onepage&q=%22boat%20race%22%20%22united%20kingdom%22%20audience&f=false | title=Gaming the World: How Sports Are Reshaping Global Politics and Culture| first = Andrei |last=Markovits|first2=Lars |last2=Rensmann| publisher = Princeton University Press| date= 6 June 2010 | isbn=978-0691137513|pages=287&ndash;288}}</ref> Oxford went into the race as reigning champions, having won the [[The Boat Race 1978|1978 race]] after Cambridge sank.  Cambridge, however, led overall with 68 victories to Oxford's 55.<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Ltd | title = Boat Race – Results| accessdate = 20 August 2014}}</ref>  The race was sponsored for the third consecutive year by [[Ladbrokes]].<ref name=oddson/>  Former Oxford [[Blue (university sport)|Blue]] Ronnie Howard was the umpire for the race.<ref>{{Cite news | title = Jekyll-and-Hyde form by Oxford | first = Desmond | last = Hill | work = [[The Daily Telegraph]] | date = 14 March 1979 | page = 35}}</ref> To allow for television viewing, the start time of the race (2&nbsp;p.m.) was an hour earlier than the traditional flood tide.<ref name=sick/>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

Oxford were being coached for the sixth consecutive time by [[Daniel Topolski]] who had himself rowed in the [[The Boat Race 1967|1967]] and [[The Boat Race 1968|1968 races]].<ref name=chase>{{Cite news | title = Chasing the Dark Blues away | first = Roger | last= Jennings | date = 11 March 1979 | page = 29 | work = [[The Guardian]]}}</ref>  As coach, Topolski had suffered just one defeat.<ref name=oddson/>  Cambridge's head coach was Czechoslovakian former international rower [[Bohumil Janoušek]]; although a double Olympic medallist, he was still cautious of the event: "It's a peculiar race.  The distance, the bends, the fact that only two crews race, the fact that during the course you encounter all sorts of water and wind conditions."<ref name=chase/> Janoušek had been employed in order to prevent Cambridge losing for the fourth consecutive time, an occurrence which last took place following the [[The Boat Race 1912|1912 race]].<ref name=oddson>{{Cite news | title = Oxford odds on to make it four wins in a row | first = Desmond | last = Hill | work = [[The Daily Telegraph]] | page = 31| date= 17 March 1979}}</ref>  Preparations for the race were hampered by appalling weather conditions: horizontal sleet and snow made practice rows challenging.<ref>{{Cite news | title = Cambridge battle against blizzard | first = Desmond | last = Hill | date =16 March 1979 | work = [[The Daily Telegraph]]| page = 35}}</ref>

==Crews==
Both crews weighed an average of 13&nbsp;[[Stone (unit)|st]] 4&nbsp;[[Pound (mass)|lb]] (84.2&nbsp;kg); Henderson, the Cambridge cox weighed {{convert|13|lb|kg}} more than his Dark Blue counterpart.<ref name=shock/>  In the week leading up to the race however, Cambridge's Andy Grey was struck down by gastroenteritis.  While he recovered, his roommate John Woodhouse became ill and withdrew from the race three hours prior to the event.  Woodhouse was replaced by Graham Phillips (who weighed {{convert|8|lb|kg|0}} less than Woodhouse) and the Light Blue boat was reorganised, with Phillips rowing at three and Nick Davies moving to stroke.<ref name=sick/>  Oxford's crew contained four returning Blues, with [[Boris Rankov]] making the second of what would become six appearances in the race.  Cambridge welcomed back four Blue rowers and the cox Henderson, all of whom had rowed the previous year.<ref>Dodd, p. 347&ndash;48</ref>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || P. J. Head || [[Oriel College, Oxford|Oriel]] || 12 st 4 lb ||S. J. Clegg || [[St Catharine's College, Cambridge|St Catharine's]] || 13 st 0 lb
|-
| 2 || R. A. Crockford || [[Corpus Christi College, Oxford|Corpus Christi]] || 13 st 4 lb || A. H. Grey || [[Pembroke College, Cambridge|Pembroke]] || 13 st 1 lb
|-
| 3 || R. J. Moore || [[St Edmund Hall, Oxford|St Edmund]] || 13 st 3 lb || A. G. Phillips || [[Jesus College, Cambridge|Jesus]] || 12 st 12 lb
|-
| 4 || [[Boris Rankov|N. B. Rankov]] || [[Corpus Christi College, Oxford|Corpus Christi]] || 14 st 5 lb || J. S. Palmer || [[Pembroke College, Cambridge|Pembroke]] || 14 st 2 lb
|-
| 5 || J. R. Crawford || [[Pembroke College, Oxford|Pembroke]] || 14 st 0 lb || A. N. de M. Jelfs || [[Fitzwilliam College, Cambridge|Fitzwilliam]] || 13 st 4 lb
|-
| 6 || C. J. Mahoney || [[Oriel College, Oxford|Oriel]] || 13 st 4 lb || P. W. Cross || [[Downing College, Cambridge|Downing]]  || 12 st 11 lb
|-
| 7 ||A. J. Wiggins || [[Keble College, Oxford|Keble]] || 13 st 4 lb || R. C. Ross || [[Lady Margaret Boat Club]]  || 14 st 4 lb
|-
| [[Stroke (rowing)|Stroke]] ||  M. J. Diserens || [[Keble College, Oxford|Keble]] || 12 st 9 lb ||R. N. E. Davies {{double dagger}} || [[St Catharine's College, Cambridge|St Catharine's]] || 13 st 5 lb
|-
| [[Coxswain (rowing)|Cox]] || C. P. Berners-Lee || [[Wadham College, Oxford|Wadham]] || 7 st 9 lb || G. Henderson || [[Downing College, Cambridge|Downing]]  || 8 st 8 lb
|-
!colspan="7"|Source:<ref name=shock>{{Cite news | title = Shock as crews weigh in | first = Desmond | last = Hill | work = [[The Daily Telegraph]] | date = 13 March 1979 | page=34}}</ref><br> (P) &ndash; boat club president<br>{{double dagger}} &ndash; John Woodhouse was replaced by Graham Phillips on the day of the race.<ref name=sick>{{Cite news | date = 19 March 1979 | first = Desmond | last = Hill | work = [[The Daily Telegraph]] | title = Easy for Oxford as Cambridge stroke goes sick | page = 31}}</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the toss (for the fifth consecutive year) and elected to start from the Surrey station.  Conditions were calm &ndash; Desmond Hill writing in ''[[The Daily Telegraph]]'' described the river as "glassy" &ndash; and the tide, as a result of the earlier start time, was very weak.<ref name=sick/>  The two boats were at 45 degrees to one another as the umpire dropped the flag to signal the start and within a minute, Oxford were clear of Cambridge.  Oxford held a lead of two lengths by [[Craven Cottage]] and passed the Mile Post five seconds ahead, and extended their lead to eight seconds by the time the crews shot [[Hammersmith Bridge]].  Davies brought two two pushes out of the Light Blues at [[Chiswick Eyot]] but Oxford maintained their lead and passed the finishing post in 20 minutes 33 seconds, three-and-a-half lengths ahead of Cambridge.<ref name=sick/><ref>Dodd, p. 348</ref>  It was Oxford's fourth consecutive victory and their fifth in six years.<ref name=results/>

Taking place 30 minutes before the main race, the reserve race saw Cambridge's Goldie defeat Oxford's Isis by twelve lengths and thirty seconds.<ref name=sick/>  As of 2014, the winning time of 22 minutes 50 seconds is the slowest time in the history of the event.  It was Goldie's third consecutive victory, and their eleventh in thirteen years.<ref name=results/>  Cambridge won the [[Women's Boat Race 1979|34th Women's Boat Race]], making it their third in a row, and their sixteenth victory in seventeen years.<ref name=results/>

==Reaction==
Oxford cox, Peter Berners-Lee suggested: "I got some help from the tide at the beginning, but very little later.  I was expecting a neck-and-neck race and I couldn't believe we were a length up at Fulham."<ref>{{Cite news | title = Stroke of bad luck | first = Roger | last = Jennings | work = [[The Observer]] | date = 18 March 1979 | page = 31}}</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1979}}
[[Category:The Boat Race]]
[[Category:1979 in English sport]]
[[Category:1979 in rowing]]
[[Category:March 1979 sports events]]