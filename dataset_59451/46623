'''Marcel Florent De Boodt''' (March 10, 1926 &ndash; January 23, 2012) was a professor emeritus of the [[University of Ghent]] and has been dean of the Faculty of Agriculture (Faculty of Bioscience Engineering) and the Belgian national delegate to the [[UNESCO]] [[Man and the Biosphere Programme]].

==Academic career==
Prof. De Boodt was born in [[Sint-Truiden|St-Truiden]], [[Belgium]].  In 1948, he graduated from the University of Ghent as an ''engineer for the chemistry and agricultural industries''. He studied as a [[Fulbright fellow]] from 1950 till 1951 at [[Iowa State University]] in the United States where he obtained a Master of Science in soil physics. He received his doctoral degree at the University of Ghent in 1957 under the supervision of Prof. Dr. L. De Leenheer.<ref name=Pedon>[http://www.plr.ugent.be/pedon_3.pdf Pedon, Staff News, University of Ghent], February 1993, pp. 4-6 </ref>

In January 1965, he was appointed professor and director of the Laboratory of Soil Physics of the Faculty of Agricultural Sciences at the University of Ghent, where he was in charge of lectures in soil physics and soil pollution. He participated very actively in the development of chemical polymers known as soil conditioners by which soil structurization and stabilization could be promoted almost instantaneously. His first applications were very useful to improve the agricultural production of soils in the temperate regions and in the wet tropics to fight against water erosion. The last applications were mainly dealing with water efficiency in the irrigation systems in semi-arid and arid zones.<ref name="Pedon"/>

De Boodt introduced the concept of "activating neutral surfaces in soil conditioning" It served mainly in desert reclamation and this in an economical justified way. From now on it became possible to treat seedling lines or plant pits of fruit trees and bushes which conserved the water in the root zone and prohibits evaporation at the soil surface. The method which was tried out in Egypt, gained a widespread interest in the other countries of North Africa, the Near East and in China.

One of the characteristic features of De Boodt was his love for the effort to bring the message of soil conservation and increased soil production through better soil physical treatment to the people of all nations. The European Market Commission often calls on him to set up important projects of which the desert reclamation in particular should be like the one tried out in [[Egypt]]. He often went on missions in [[Latin America]], especially in [[Peru]], [[Ecuador]] and [[Panama]], with as topic the erosion repair of new developments for the desert and in [[Saudi Arabia]] and the [[United Emirates]] for water us efficiency studies.<ref name="Pedon"/>

Throughout all these activities De Boodt became convinced that the most lasting effect of scientific collaboration consists in the transfer of knowledge and know-how. Under his guidance more than 100 people were trained and more than 80 obtained an MSc or PhD in soil science.  In addition, he was visiting professor in 20 different universities around the world.

At the University of Ghent he often has been the president of scientific advisory boards on a national and international level such as the National Science Foundation, the Commission for Agriculture and the Council for the Scientific Research to the Ministry of Agriculture. Prof. De Boodt was the Dean of the Faculty for Agriculture from 1980 till 1984.<ref>[http://www.ugent.be/nl/voorzieningen/bronnen/archief/collectie/archiefcollectie/faculteit/bi/overzicht.htm Decanen faculteit Bio-ingenieurswetenschappen]</ref>

After more than forty years of academic career, Prof. De Boodt retired in October 1991 as Director of the Laboratory of Soil Physics, Soil Conditioning and Horticultural Soil Science. For years he stayed active at the ICE till he died on January 23, 2012 at the age of 85.

==Organisations==
In 1980, he was the Belgian National Delegate to the ''UNESCO Man and Biosphere programme''.  He was president of the Belgian branch of the American Society for Advancement of Science from 1982 onwards; starting from 1985 president of the Scientific Council of the World Foundation for the Quality of Life and from 1986 onwards president of the International Soil Science Society.

In 1989, he founded the ''International Institute for Eremology''; the ICE is situated at University of Ghent in Belgium and offers a research base for a number of scientists and students. From this base, many projects are organized in many dryland countries distributed over all continents.<ref name="Pedon"/> The Centre received in 2008 the ''UNESCO Chair on Eremology''.<ref>[http://web.bgu.ac.il/NR/rdonlyres/267FF5E0-C961-433C-9938-797284593CAD/72094/GabrielsD.pdf Unesco Chair on Eremology]"</ref>

In 1991 he founded together with his spouse Marie-Christiane Maselis the ''De Boodt-Maselis foundation'' which grants every year the 'Prize De Boodt - Maselis' for the promotion of studies and research in Eremology.<ref>[http://www.unesco-vlaanderen.be/media/58411/unescoatvlaanderen_onderwijs.pdf Duurzaam waterbeheer in droge gebieden], page 19</ref>

==Prizes and Awards==
He received several awards and medals of merit and recognition from universities and government institutions in [[Iraq]] ( Silver Plate of Merit 1979), [[Thailand]] (Medal of Appreciation 1983), [[Egypt]] (Award of Recognition and Appreciation 1987) and [[Poland]] (Oczapowski Commemorative Medal 1990).<ref name="Pedon"/>

In 1986 he was awarded ''Doctorate Honoris Causa'' by the Agricultural University in [[Lublin]], Poland.<ref>[http://www.english.pan.pl/images/stories/pliki/publikacje/annual_report/annual_report_08.pdf Doctor Honoris Causa]</ref>

;Special Awards
Groot-officier Leopold II orde -
Groot-officier in de Kroonorde -
Francqui Chair.<ref>[http://www.francquifoundation.be/fr/chaires%20belges%20fr%20historique.htm Franqui Leerstoel Katholieke Universiteit Leuven 1972-1973]</ref>

== References ==
{{Reflist}}

{{DEFAULTSORT:DeBoodt, Marcel}}
[[Category:1926 births]]
[[Category:2012 deaths]]
[[Category:Belgian academics]]
[[Category:Members of Academia Europaea]]
[[Category:Members of the Polish Academy of Sciences]]
[[Category:Belgian agriculturalists]]
[[Category:Soil scientists]]