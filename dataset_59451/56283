{{Distinguish |Journal of Clinical Neurophysiology}}
{{Infobox journal
| title = Clinical Neurophysiology 
| cover = [[File:Clinical_Neurophysiology_cover.jpg|200px]]
| editor = D. Burke
| discipline = [[Clinical neurophysiology]]
| former_names = Electroencephalography and Clinical Neurophysiology
| abbreviation = Clin. Neurophysiol.
| publisher = [[Elsevier]]
| country = 
| frequency = Monthly
| history = 1949-present
| openaccess = 
| license = 
| impact = 2.979
| impact-year = 2013
| website = http://www.journals.elsevier.com/clinical-neurophysiology/
| link1 = http://www.clinph-journal.com/current
| link1-name = Online access
| link2 = http://www.clinph-journal.com/issues
| link2-name = Online archive
| link3 = http://www.sciencedirect.com/science/journal/00134694
| link3-name = ''Electroencephalography and Clinical Neurophysiology'' archives
| JSTOR = 
| OCLC = 40618487
| LCCN = 
| CODEN = CNEUFU
| ISSN = 1388-2457
| eISSN = 
}}
'''''Clinical Neurophysiology''''' is a monthly [[peer-reviewed]] [[medical journal]] published by [[Elsevier]]. It was established in 1949 as ''Electroencephalography and Clinical Neurophysiology'' and obtained its current title in 1999. The journal covers all aspects of [[neurophysiology]], especially as relating to the [[pathophysiology]] underlying diseases of the [[Central nervous system|central]] and [[peripheral nervous system]]. It is the official journal of the [[International Federation of Clinical Neurophysiology]], the [[Brazilian Society of Clinical Neurophysiology]], the [[Czech Society of Clinical Neurophysiology]], the [[Italian Clinical Neurophysiology Society]], and the [[International Society of Intraoperative Neurophysiology]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-14}}</ref>
* [[Chemical Abstracts]]
* [[Current Contents]]/Clinical Medicine
* Current Contents/Life Sciences<ref name=ISI/>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/100883319 |title= ''Clinical Neurophysiology: Official Journal of the International Federation of Clinical Neurophysiology'' |work= [[United States National Library of Medicine|NLM]] Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-12-14}}</ref>
* [[Science Citation Index]]<ref name=ISI/>
* [[Embase]]
* [[Neuroscience Citation Index]]
* [[PASCAL (database)|PASCAL]]
* [[Elsevier BIOBASE]]/Current Awareness in Biological Sciences
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.979.<ref name=WoS>{{cite book |year=2014 |chapter= ''Clinical Neurophysiology'' |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://www.journals.elsevier.com/clinical-neurophysiology/}}
*[http://www.ifcn.info/ International Federation of Clinical Neurophysiology]
*[http://www.sbnc.org.br/ Brazilian Society of Clinical Neurophysiology] {{Pt icon}}
*[http://www.neurofyziologie.cz/ Czech Society of Clinical Neurophysiology] {{Cs icon}}
*[http://www.sinc-italia.it/ Italian Clinical Neurophysiology Society] {{It icon}}
*[http://www.neurophysiology.org/ International Society of Intraoperative Neurophysiology]

[[Category:Neurology journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1949]]
[[Category:Neurophysiology]]
[[Category:Physiology journals]]
[[Category:Academic journals associated with international learned and professional societies]]