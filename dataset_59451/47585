'''John Douglas''' of Pinkerton ({{circa|lk=no}}1709{{snd}}20 June 1778) was a [[Scottish people|Scottish]] [[architect]] who designed and reformed several [[Estate houses in Scotland|country houses]] in the [[Scottish Lowlands]]. His work deserves to be noted for what the 2002 history of Scottish architecture remarks as an approach "of relentless surgery or concealment.".<ref name=":0">{{cite book |title=A History of Scottish Architecture: from the Renaissance to the Present Day |last1=Glendinning |first1=M. |last2=MacInnes |first2=R. |last3=MacKehnie |first3=A. |location=Edinburgh |publisher=[[Edinburgh University Press]] |year=2002 |isbn=978-0-7486-0849-2 }}</ref> His most notable works are [[Killin]] and [[Ardeonaig]] Church, [[Stirlingshire]] (1744);<ref name="KAPC">{{cite web
|url=http://www.scotlandschurchestrust.org.uk/church/killin-ardeonaig-parish-church
|title=Killin & Ardeonaig Parish Church
|website=Scotland's Churches Trust
|accessdate=21 December 2014
}}</ref> [[Archerfield House]], [[East Lothian]] (1745);<ref name="AH">{{cite web |url=http://canmore.rcahms.gov.uk/en/site/56746/details/archerfield+house/ |title=Archerfield House |website=[[Canmore (database)|Canmore database]] |accessdate=21 December 2014 }}</ref> [[Finlaystone House]], [[Renfrewshire (historic)|Renfewshire]] (1746{{ndash}}47),<ref name="FH" /> Wardhouse (Gordonhall), [[Insch]], [[Aberdeenshire]] (1757);<ref name="Wardhouse">{{cite web
|url=http://www.buildingsatrisk.org.uk/ref_no/1851
|title=Wardhouse, Insch
|website=buildingsatrisk.org.uk
|accessdate=21 December 2014
}}</ref> and [[Campbeltown]] [[Seat of local government|Town House]], Argyll and Bute (1758{{ndash}}60).<ref name="CTH">{{cite web |url=http://canmore.rcahms.gov.uk/en/site/38815/details/campbeltown+main+street+town+house/ |title=Campbeltown, Main Street, Town House |website=Canmore database |accessdate=21 December 2014 }}</ref> Several of these are [[listed building]]s.

== Biography ==

His date and place of birth are not known. In his will, he appears as John Douglas of Pinkerton,<ref>Pinkerton is not in the 2007 [[Times Atlas of the World|Times Atlas]]. The place may perhaps be Little Pinkerton or Meikle Pinkerton, [[Dunbar]], East Lothian.</ref> late architect in [[Leith]], who died on 20 June 1778.<ref>Will of John Douglas, 20 June 1778. National Archives of Scotland CC8/8/124</ref> The [[Edinburgh]] Recorder (records of the Edinburgh Friendly Fire Insurance Company, which began in 1720 as a loose-knit association of Edinburgh property owners for mutual financial protection against loss by fire),<ref>{{cite book |title=The Edinburgh Recorder: Spotlight on the Personalities, Properties and Their Fire Insurance Policies from 1720 to 1840 |editor-last=Gilhooley |editor-first=James |year=1990 |location=Edinburgh |publisher=Privately printed }}</ref> shows that he owned properties in High Street North at Fleshmarket Close East (Thomson’s Close) (records 2754-2757) and at Old Provost Close, East Head (record 2717).<ref>{{cite web |url=http://www.scottisharchitects.org.uk/architect_full.php?id=407903 |title=John Douglas, Architect |website=scottisharchitects.org.uk |accessdate=21 December 2014 }}</ref>

== Work as architect ==

John Douglas designed and renovated several country houses during the middle of the 18th century. He was considered an efficient designer and many of his projects are characterised by a horizontal hierarchy which is signposted by a central projecting body<ref name=":0" /> (for example, Archerfield House and Campbeltown Town House).

A cache of his drawings was discovered, curated and exhibited in 1989 by Ian Gow at the [[Royal Commission on the Ancient and Historical Monuments of Scotland]], entitled "John Douglas{{snd}}[[William Adam (architect)|William Adam]]'s Rival?". This material has been further analysed, to reveal more about his style in both executed and unexecuted designs (Archerfield House, Galloway House,<ref name= "GH">{{cite web |url=http://canmore.rcahms.gov.uk/en/site/63147/details/galloway+house/ |website=Canmore database |title=Galloway House |accessdate=21 December 2014}}</ref> Finlaystone House,<ref name="FH">{{cite web |url=http://www.finlaystonehouse.com/historycunninghams.htm |title=Finlaystone House History |website=finlaystonehouse.com |accessdate=21 December 2014 }}</ref> and two designs for Blair Castle <ref>{{cite web |url=http://www.euppublishing.com/doi/abs/10.3366/arch.2001.12.1.1 |title=John Douglas’ Country House Designs |last=Kinnear |first=Holly E. B. |year=2001 |journal=[[Architectural Heritage]] |volume=12 |issue=1 |pages=1–12 |issn=1350-7524 |format=PDF |publisher=Edinburgh University Press, for the [[Architectural Heritage Society of Scotland]] |location=Edinburgh }}</ref>).

== List of buildings ==

A book by [[Howard Colvin]] lists several of his projects, but some of the facts are incorrect (for example, Douglas was indeed responsible for the disastrous repair of [[Holyrood Abbey Church]] in 1760 and the dispute over non-payment of the rest of his fees was resolved only after his death).<ref>{{cite book |title=A Biographical Dictionary of British Architects 1600{{ndash}}1840 |first=Howard |last=Colvin |authorlink=Howard Colvin |year=1978 |publisher=[[John Murray (publisher)|John Murray]] |location=London }}</ref><ref name="ascelibrary.org">{{cite web |url=http://ascelibrary.org/doi/abs/10.1061/%28ASCE%291076-0431%282003%299%3A3%28109%29 |last1=Theodossopoulos |first1=D. |last2=Sinha |first2=B.P. |last3=Usmani |first3=A.S. |year=2003 |title=Case Study of the Failure of a Cross Vault: Church of Holyrood Abbey |journal=J. Archit. Eng. |volume=9 |issue=3 |pages=109–117 |format=PDF |publisher=[[American Society of Civil Engineers]] }}</ref> A map of the buildings on which he worked shows their distribution across Scotland.<ref>{{cite web |url=https://maps.google.com/maps/ms?msa=0&msid=211007301315030640668.0004c6ee94355886a1241 |title=Main Buildings Attributed to the 18C Architect John Douglas |website=Google maps |accessdate=21 December 2014 }}</ref>

* Freeland House, [[Forgandenny]], [[Perth and Kinross]] (1733) <ref>{{cite web |url=http://www.strathallan.co.uk/pages/download/1314881013StrathallanHistoryArchivesFreelandHouse.pdf |title=Freeland House |website=strathallan.co.uk |accessdate=21 December 2014 }}</ref>
* [[Traquair House]] (1733{{ndash}}38)
* Quarrell House, Stirlingshire (1735{{ndash}}36). Douglas collaborated with [[James Gibbs]] in the reform of the house
* Murthly Castle (1735). Entrance block.<ref>{{cite web |url=http://www.scottisharchitects.org.uk/building_full.php?id=206761 |title=Murthly House DSA Building/ Design Report |website=Dictionary of Scottish Architects DSA|accessdate=27 February 2016}}</ref>
* Ardmaddy Castle (1737). Attributed
* Lochnell House (1737{{ndash}}39)
* [[Abercairny]] House, [[Perth and Kinross]] (1737{{ndash}}38).<ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/88063/details/abercairny/ |website=Canmore database |title=Abercairny |accessdate=21 December 2014 }}</ref> Design for improvements
* [[Galloway House]], [[Garlieston]], [[Dumfries and Galloway]] (1740).<ref name="GH" /> Unexecuted design
* [[Glasserton]] House, Dumfries and Galloway (1740{{ndash}}41).<ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/216076/details/glasserton+house/ |title=Glasserton House |website=Canmore database |accessdate=21 December 2014}}</ref> Unexecuted design
* [[Arbroath]] Harbour (1741).<ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/35557/details/arbroath+harbour/ |title=Arbroath Harbour |website=Canmore database |accessdate=21 December 2014}}</ref> Advised on how to alleviate silting
* [[Lochmaben]] Town Hall, [[Dumfries and Galloway]] (1743).<ref>{{cite web |url=http://www.scran.ac.uk/database/record.php?usi=000-299-999-659-C&scache=3k9dehnl4i&searchdb=scran |title=Elevation of Lochmaben Town Hall, Dumfries and Galloway |website=[[Scran]] |accessdate=21 December 2014 }}</ref> Design of a steeple
* [[Taymouth Castle]] (Balloch Castle), Perth and Kinross (1743{{ndash}}50).<ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/24893/details/taymouth+castle/ |title=Taymouth Castle |website=Canmore database |accessdate=21 December 2014}}</ref> Work on the new sash windows, dining room and new entry to the house
* [[Amulree]] and Strathbraan Parish Church, Perth and Kinross (1743{{ndash}}52) <ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/88012/details/amulree+church+of+scotland+and+churchyard/ |title=Amulree Church of Scotland and Churchyard |website=Canmore database |accessdate=21 December 2014 }}</ref>
* [[Killin]] and [[Ardeonaig]] Parish Church (1744) <ref name="KAPC" />
* [[Kilmahew Castle]] (1744).<ref>{{cite web |url=https://canmore.org.uk/site/113685/kilmahew-house |title=Kilmahew House |website=Canmore database |accessdate=21 December 2014}}</ref> Attributed. A ruin
* [[Archerfield Estate and Links|Archerfield House]] (1745)<ref name="AH" />
* [[Crosbie Castle and the Fullarton estate|Fullarton House]], [[South Ayrshire]] (1745).<ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/145895/details/fullarton+house/ |title=Fullarton House |website=Canmore database |accessdate=21 December 2014}}</ref> Attributed
* [[Finlaystone House]], [[Renfrewshire (historic)|Renfewshire]] (1746{{ndash}}47)<ref name="FH" />
* [[Blair Castle]] (1748{{ndash}}56).<ref>{{cite web |url=http://canmore.rcahms.gov.uk/en/site/25802/details/blair+castle/ |title=Blair Castle |website=Canmore database |accessdate=21 December 2014 }}</ref> Unexecuted design for remodelling the castle in the [[Palladian architecture|Palladian style]]
* [[Edmonstone House]], (1744{{ndash}}49)
* Dysart, Hot Pot Wynd, Carmelite Monastery (1748).<ref>{{cite web
|url=http://canmore.rcahms.gov.uk/en/site/53988/details/dysart+hot+pot+wynd+carmelite+monastery/
|title=Dysart, Hot Pot Wynd, Carmelite Monastery
|website=Canmore database
|accessdate=21 December 2014
}}</ref> Unexecuted design
* [[St Salvator's College, St Andrews|St Salvator’s College]] (1754{{ndash}}58). University halls of residence (demolished) <ref>{{cite book |last=Cant |first=Ronald G. |year=1950 |title=The College of St Salvator: Its Foundation and Development, Including a Selection of Documents |location=Edinburgh |publisher=Oliver and Boyd, for the [[University Court#ScotlandUniversity Court]] of the [[University of St. Andrews]]}}</ref><ref>{{cite book |last=Grater |first=Abigail |year=2000 |title=The United College: The Architectural Development of the United College of St Salvator and St Leonard, University of St. Andrews, 1757{{ndash}}1931 |location=St Andrews |publisher=University of St. Andrews}}</ref>
* Wardhouse (Gordonhall), Aberdeenshire (1757)<ref name="Wardhouse" />
* [[Holyrood Abbey Church]], Edinburgh (1758{{ndash}}60). Replacement of roof timber truss with diaphragm walls. Collapsed 2 December 1768 <ref name="ascelibrary.org"/> 
* [[Campbeltown]] Town House (1758{{ndash}}60) <ref name="CTH" />
* [[Dalhousie Castle]], [[Midlothian]] (1770s) <ref>{{cite web |url=http://www.scran.ac.uk/database/record.php?usi=000-000-112-393-C&scache=3ka7nhnl4g&searchdb=scran |title=Dalhousie Castle, Midlothian |website=Scran |accessdate=21 December 2014 }}</ref>
* [[Upper Largo|Largo House]]. Attributed

== References ==
{{reflist}}

{{DEFAULTSORT:Douglas, John}}
[[Category:Year of birth missing]]
[[Category:1778 deaths]]
[[Category:Scottish architects]]
[[Category:18th-century Scottish architects]]
[[Category:Country houses]]