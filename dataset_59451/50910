{{Infobox journal
| title = Chungara
| cover = 
| editor = Vivien G. Standen
| discipline = [[Anthropology]]
| abbreviation = Chungara
| language = English, Spanish
| publisher = Departamento de Antropología ([[Universidad de Tarapacá]])
| country = Chile
| frequency = Quarterly
| history = 1972–present
| openaccess =
| license =
| impact = 0.895
| impact-year = 2015
| website = http://www.chungara.cl/index.php/en/our-journal
| link1 = http://www.chungara.cl/index.php/en/back-issues
| link1-name = Online archive
| link2 =
| link2-name =
| JSTOR =
| OCLC = 746941986
| LCCN = 76647090
| CODEN =
| ISSN = 0716-1182
| eISSN = 0717-7356
}}
'''''Chungara Revista de Antropología Chilena''''' (English: ''The Journal of Chilean Anthropology'') is a [[peer-reviewed]] [[academic journal]] on [[anthropology]] and [[archaeology]] with particular, but not exclusive, focus on the [[Andes|Andean region]]. The journal is published by the Departamento de Antropología ([[Universidad de Tarapacá]]) and the [[editor-in-chief]] is Vivien G. Standen (Universidad de Tarapacá).

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-06-08}}</ref> [[Current Contents]]/Social & Behavioral Sciences,<ref name=ISI/> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-06-08}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.694.<ref name=WoS>{{cite book |year=2014 |chapter=Chungara |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.chungara.cl/index.php/en/our-journal}}

{{DEFAULTSORT:Chungara, Revista}}
[[Category:Anthropology journals]]
[[Category:Multilingual journals]]
[[Category:Academic journals published by universities of Chile]]
[[Category:Publications established in 1972]]
[[Category:Biannual journals]]
[[Category:Media in Arica]]