{{good article}}
{{DISPLAYTITLE:3 (''The X-Files'')}}
{{Infobox television episode
| season     = [[The X-Files (season 2)|2]]
| episode    = 7
| title      = 3
| series     = [[The X-Files]] 
| guests     = 
* [[Justina Vail Evans|Justina Vail]] as The Unholy Spirit
* [[Perrey Reeves]] as [[List of Monster-of-the-Week characters in The X-Files#Kristen Kilar|Kristen Kilar]]
* Gustavo Moreno as The Father
* Frank Military as The Son/John<ref name="plot1"/>
| writer               = [[Chris Ruppenthal]]<br>[[Glen Morgan]]<br>[[James Wong (producer)|James Wong]]
| director           = [[David Nutter]]
| production    = 2X07
| length             = 45 minutes
| airdate               = November 4, 1994
| prev          = [[Ascension (The X-Files)|Ascension]]
| next          = [[One Breath (The X-Files)|One Breath]]
| episode_list = [[The X-Files (season 2)|List of season 2 episodes]]<br>[[List of The X-Files episodes|List of ''The X-Files'' episodes]]
}}

"'''3'''" is the seventh episode of the [[The X-Files (season 2)|second season]] of the American [[science fiction on television|science fiction]] television series ''[[The X-Files]]''. First broadcast on the [[Fox Broadcasting Company|Fox network]] on {{nowrap|November 4, 1994}}, the episode was written by [[Glen Morgan]], [[James Wong (producer)|James Wong]] and [[Chris Ruppenthal]], directed by [[David Nutter]], and featured guest appearances by [[Perrey Reeves]] and [[Malcolm Stewart (actor)|Malcolm Stewart]]. The episode is a [[List of Monster-of-the-Week characters in The X-Files|"Monster-of-the-Week"]] story, unconnected to the series' wider [[Mythology of The X-Files|mythology]].  Following on from the abduction of [[Dana Scully]] in the previous episode, "[[Ascension (The X-Files)|Ascension]]", "3" was the first episode of ''The X-Files'' not to feature series star [[Gillian Anderson]]. The episode earned 9 million households during its first broadcast, and received mixed reviews from both critics and the show's cast and crew.

The show centers on FBI special agents Fox Mulder (David Duchovny) and Dana Scully (Gillian Anderson) who work on cases linked to the paranormal, called X-Files. In the episode, Mulder is brought in to investigate a series of ritualistic murders in Los Angeles, which he first believes were the work of a cult. However, it turns out that the perpetrators are a group of vampires.

== Plot ==
In [[Los Angeles]], Garrett Lorre, a middle-aged businessman, embarks on a [[one night stand]] with an anonymous woman he has met at a corporate party. However, as they are having sex in his hot tub, the woman bites Lorre to drink his blood. Two other men join the woman, helping her kill Lorre by repeatedly stabbing him using [[hypodermic needles]].

The following day, before departing for Los Angeles, [[Fox Mulder]] stores the missing [[Dana Scully]]'s [[Federal Bureau of Investigation|FBI]] badge in an [[X-File]] and files it under her name. At the crime scene, Mulder meets with the [[Los Angeles Police Department|LAPD]] detectives investigating the case, explaining that Lorre's murder is the latest in a series of seemingly [[vampire|vampiric]] [[serial killer|serial killings]] that have spanned two other states. Because the killers write biblical passages in the victims' blood, Mulder believes that they view themselves as an "Unholy Trinity".

Mulder visits a local [[blood bank]] where a night watchman has been recently hired. Mulder has him arrested after he is caught drinking blood in the facility's storeroom. During his [[interrogation]], the suspect tells Mulder that he belongs to a trio of vampires who desire [[immortality]]; he is known as "The Son" while the other two, a man and a woman, are called "The Father" and "The Unholy Spirit". Mulder does not believe The Son's claims. However, at sunrise, The Son is burned to death when sunlight from the window touches his flesh. Mulder is taken aback, having previously assumed vampires to be purely [[mythological]].

During an examination of The Son's body, Mulder discovers a tattoo for Club Tepes, a local [[vampire lifestyle|vampire club]]. There, he comes across a young woman named Kristen Kilar who partakes in the consumption of blood. Mulder, having his suspicion aroused, follows Kristen after she and another club patron, David Yung, leave for an erotic liaison; he initially fears that Kristen is targeting Yung, but is beaten by Yung when he catches the agent spying on them. After Mulder leaves, Yung is murdered by the three killers, although the reason is not known.

Mulder runs a background check into Kristen, discovering that she formerly lived in [[Memphis]] and [[Portland, Oregon|Portland]]—both the previous locations of earlier murders. Mulder assists the LAPD in searching Kristen's home, where he finds various blood-related [[paraphernalia]]. When Kristen arrives later, Mulder is waiting for her. Kristen tells Mulder that she met The Son in [[Chicago]] and that they had engaged in "blood sports" together. Later, Kristen fled The Son as he formed the Unholy Trinity with his accomplices and began their killing spree, following her across the country. Mulder and Kristen kiss while The Son, who has returned from the dead, looks on.

The next morning, The Son confronts Kristen and tells her that by killing Mulder and drinking the blood of a "believer", she will become one of them. Kristen approaches Mulder with a knife but instead stabs The Father, who is hiding in the bedroom. The Son attacks Mulder but is subdued. Mulder and Kristen try to escape using a car parked in the garage, but the Unholy Spirit jumps onto the car hood and attacks them. Kristen drives into her, impaling her on a wooden peg on the wall. Kristen tricks Mulder into running outside of the house while she goes back inside and pours gasoline around herself and The Son. Kristen lights a match, blowing up the house and taking her own life in order to kill the other vampires. Firefighters and police find four bodies in the wreckage while Mulder stares at Scully's [[cross necklace]].<ref name="plot1">Lowry, pp.176–177</ref><ref name="plot2">Lovece, pp.123–125</ref>

== Production ==
{{quote box|right|quote="It's a very different show because it's the first one without Scully. She's been away for quite some time. It's a situation where Mulder is in a dark place, doesn't know which way to turn, and is really very much on his own. The whole vampire thing happened because he went to a dark place that he normally wouldn't have gone to."|source=David Nutter on "3"<ref name="rec2">Edwards, p.103</ref>| width  = 27%}}
Howard Gordon was originally supposed to write the seventh episode of the season, but when he became unavailable Glen Morgan and James Wong, who were working on writing [[One Breath (X-Files Episode)|the eighth episode of the season]], agreed to rewrite a freelance script provided by [[Chris Ruppenthal]].<ref name="prod0.2" /> The writers had to do significant edits, but retained the main plot surrounding three vampires.<ref name="prod0.2">Edwards, p.102</ref><ref name="prod1.1">Lovece, p.126</ref>

Club Tepes, named after Prince Vlad Tepes—better known as [[Vlad the Impaler]], who was the inspiration for [[Dracula]]<ref name="prod 1.2"/>—was shot inside a closed-down and redecorated nightclub, with extras recruited from other Vancouver clubs.<ref>{{cite news|title=A Surreal 'X-Files' Captures Earthlings! : Poltergeists, Space Aliens and Mutants Feed Show's Hold on Younger Audience|date=October 28, 1994|first=Daniel Howard|last=Cerone|url=http://articles.latimes.com/1994-10-28/entertainment/ca-55946_1_x-files|work=[[The Los Angeles Times]]|accessdate=December 12, 2011}}</ref> The location for Kristen's house was the mansion of hockey player [[Pavel Bure]], then the leading name of the [[Vancouver Canucks]]. The producers had an agreement for the late filming from all but one of Bure's neighbors, who was absent during the petitioning. Said neighbor later tried to sue Fox, only agreeing to let production continue after receiving an indenization.<ref>Gradnitzer and Pittson, p. 67</ref>

Perrey Reeves, who played Kristen, was David Duchovny's real-life girlfriend at the time.<ref name="prod1" /> Speaking of Mulder's possible sexual encounter with Kristen, series creator Chris Carter said, "I thought, 'This guy's a monk. Let's let him be a human. Especially in [Scully's] absence, it seemed like a perfect opportunity to do it."<ref name="prod1">Lowry, p.177</ref> Duchovny had previously acted alongside another real-life girlfriend, [[Maggie Wheeler]], in the [[The X-Files (season 1)|first season]] episode "[[Born Again (The X-Files)|Born Again]]".<ref name="wheeler">Lovece, p.100</ref> Gillian Anderson is absent from the episode as she was on leave to give birth to her daughter Piper at the time.<ref name="prod1.1" /> This episode was the first in which Scully did not appear.<ref name="prod 1.2">Lowry, p.176</ref>

==Reception==
[[File:PerreyReeves.jpg|thumb|right|The romantic scenes  between [[David Duchovny]] and his then-girlfriend [[Perrey Reeves]] (pictured) were widely criticized.]]

===Ratings===
"3" premiered on the [[Fox Broadcasting Company|Fox network]] on {{nowrap|November 4, 1994}}, and was first broadcast in the [[United Kingdom]] on [[BBC Two]] on {{nowrap|October 9, 1995}}.<ref name="BBCdate">{{cite AV media notes |title=The X-Files: The Complete Second Season |titlelink=The X-Files (season 2) |year=1994–1995 |others=[[David Nutter]], [[Daniel Sackheim]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> This episode earned a [[Nielsen rating]] of 9.4, with a 16 share, meaning that roughly 9.4 percent of all television-equipped households, and 16 percent of households watching television, were tuned in to the episode. It was viewed by 9 million households.<ref name="rating">Lowry, p.249</ref>

===Reviews===
"3" received mixed reviews from critics. [[Sarah Stegall]], in The Munchkyn Zone, gave the episode a 5 out of 5 rating. Stegall referred to it as a "compelling story" with "excellent special effects". She commended Duchovny's performance, saying he is "definitely at the top of his form in this episode".<ref>{{cite web|url=http://www.munchkyn.com/xf-rvws/3.html|archiveurl=https://web.archive.org/web/20011005034222/http://www.munchkyn.com/xf-rvws/3.html|title=Curing Mulder's Insomnia|work=The Munchkyn Zone|author=Stegall, Sarah|date=1994|archivedate=October 5, 2001}}</ref> While writing about vampire-related television shows for [[Metacritic]], Zeenat Burns described the episode as "wretched".<ref>{{cite web |url=http://www.metacritic.com/feature/best-and-worst-vampire-tv-shows|first=Zeenat |last=Burns |title=Ranked: Vampire TV Shows|publisher=[[Metacritic]] |date=June 9, 2010 |accessdate=October 19, 2011}}</ref> ''[[Entertainment Weekly]]'' gave the episode a C, criticizing the fact that it did not explore enough the "promising premise" of Scully's absence.<ref name="EW">{{cite web |url=http://www.ew.com/ew/article/0,,295179,00.html |title=X Cyclopedia: The Ultimate Episode Guide, Season 2 |publisher=''[[Entertainment Weekly]]'' |date=November 29, 1996 |accessdate=October 19, 2011}}</ref> Reviewer Zack Handlen of ''[[The A.V. Club]]'' also considered that without said premise "deserve[d] better than to be background noise for a by-the-numbers erotic thriller".<ref name=avclub/> Handlen described Mulder and Kristen's "tedious romance" as "all kinds of misguided",<ref name=avclub/> and felt the episode indulged in "lazy writing" regarding the over-explored theme of vampires which resulted in "terrible dialogue and heavy-handed attempts at mood".<ref name=avclub/> He still praised David Duchovny's performance and felt the first twenty minutes were "endurable trash" with a "serious ''[[USA Up All Night]]'' vibe".<ref name=avclub>{{cite web|url=http://www.avclub.com/articles/3one-breathfirewalker,13150/|first=Zack|last=Handlen|work=[[The A.V. Club]]|title=3/One Breath/Firewalker|date=August 29, 2008|accessdate=November 9, 2011}}</ref>

Co-writer Glen Morgan felt doing an episode on vampires was a mistake, and said that they also took heat for having Mulder fall for Kristen. Co-writer James Wong was also disappointed, saying that the script was a lot better than the show and that the episode was weakened when Fox [[censors]] had problems with the episode.<ref name="rec0">Edwards, pp.64–65</ref> Actor David Duchovny thought the episode had style, but suffered some lapses in logic, including the scene where Kristen shaves Mulder before the two kiss.<ref name="rec1">Lowry, p.178</ref>

== See also ==
*[[List of vampire television series#Episodes|List of vampire television series]]

== References ==
;Footnotes
{{reflist|2}}
;Bibliography
*{{cite book | year=1999 | last=Gradnitzer |first=Louisa | last2=Pittson |first2=Todd |title=X marks the spot: on location with the X-files|publisher=Arsenal Pulp Press|isbn=1-55152-066-4}}
*{{cite book | year=1996 | last=Edwards |first=Ted| title=X-Files Confidential|publisher=Little, Brown and Company|isbn=0-316-21808-1}}
*{{Cite book |title=The X-Files Declassified |first=Frank |last=Lovece |publisher=Citadel Press |year=1996 |isbn=0-8065-1745-X  }}
*{{Cite book |title=The Truth is Out There: The Official Guide to the X-Files |first=Brian |last=Lowry |publisher=Harper Prism |year=1995 |isbn=0-06-105330-9 }}

== External links ==
{{wikiquote|The_X-Files|TXF Season 2}}
*[https://web.archive.org/web/20010413100917/http://www.thexfiles.com/episodes/season2/2x07.html "3"] on TheXFiles.com
* {{imdb episode|0751066|3}}
* {{tv.com episode|the-xfiles/3-521|3}}

{{TXF episodes|2}}

[[Category:1994 American television episodes]]
[[Category:Blood in fiction]]
[[Category:California in fiction]]
[[Category:Los Angeles in fiction]]
[[Category:The X-Files (season 2) episodes]]
[[Category:Vampires in television]]