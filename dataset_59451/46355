{{Use dmy dates|date=November 2011}}
{{Use Australian English|date=November 2011}}

'''Eva Bacon''' (1909 - 23 July 1994), born Eva Goldner, was a socialist and feminist based in [[Brisbane]], [[Australia]], who was most active between the 1950s and the 1980s.<ref name = OurWomenOurState>{{Citation
 | title = Our Women, Our State: Women in Pictures: 1967
 | url = http://www.communities.qld.gov.au/q150-women/1960/index.shtml#item-eva-bacon
 | accessdate = 2011-10-18
}}</ref> Raised in Austria and a member of several leftist political organisations in her youth, Eva Goldner escaped Nazi occupied Austria in 1939, eventually migrating to Australia.<ref name=Grant>{{Citation
 | title = Great Queensland Women
 | url = http://library.uq.edu.au/search~S7?/X%22great+queensland+women%22&SORT=D/X%22great+queensland+women%22&SORT=D&SUBKEY=%22great%20queensland%20women%22/1%2C2%2C2%2CB/frameset&FF=X%22great+queensland+women%22&SORT=D&1%2C1%2C
 | year = 2005
 | author = Grant, H.
 | publisher = State of Queensland (Office for Women)
 | location = Brisbane
 | pages = 72–76
 | accessdate = 2011-10-18
}}</ref> Goldner remained involved in local and international politics and joined the [[Communist Party of Australia]] (CPA),<ref name = Grant /> marrying fellow member [[Ted Bacon]] in 1944.<ref name = Marriage>{{Citation
  | title = Marriages
  | newspaper = The Courier Mail
  | location = Queensland
  | pages = 6
  | date = 12 May 1944
  | url = http://trove.nla.gov.au/ndp/del/article/42058786?searchTerm=((Taylor,%20Jonathan%20(1941-)))%20NOT%20(id:82385fb5-7803-4870-8359-31db2dede5df)&searchLimits=
  | accessdate = 2011-10-18}}
</ref> Throughout her career Bacon was an active member of the CPA, and the [[Union of Australian Women]] (UAW),<ref name = Young1>{{Citation
  | last = Young
  | first = Pam
  | title = Obituary
  | newspaper = The Sydney Morning Herald
  | pages = 16
  | date = 29 October 1994
  | url = http://newsstore.smh.com.au/apps/viewDocument.ac?page=1&sy=smh&kw=Eva+Bacon&pb=all_ffx&dt=selectRange&dr=entire&so=relevance&sf=text&sf=headline&rc=10&rm=200&sp=nrm&clsPage=1&docID=news940829_0162_6319
  | accessdate = 2011-10-18}}</ref> where she was heavily involved in [[International Women's Day]] campaigns, including attending the 1975 UN [[First World Conference on Women|World Conference on Women]] in Mexico<ref name = Stevens>{{cite web
  | last = Stevens
  | first = Joyce
  | title = A History of International Women’s Day in Words and Images: The Nineteen Seventies and Eighties Continued
  | url = http://www.isis.aust.com/iwd/stevens/70s80s_3.htm
  | accessdate = 2011-10-18}}</ref> celebrating [[International Women's Year]]. Bacon was also an active member of the [[Women's Electoral Lobby]] (WEL), the [[Women's International League for Peace and Freedom]] (WILPF).<ref name = Young2>{{cite book
  | last = Young
  | first = Pam
  | title = Daring to Take a Stand: The Story of the Union of Australian Women in Queensland
  | publisher = Wavell Heights
  | year = 1998
  | location = Queensland
  | url = http://library.uq.edu.au/search~S7?/XPam+Young&SORT=D/XPam+Young&SORT=D&SUBKEY=Pam%20Young/1%2C15%2C15%2CB/frameset&FF=XPam+Young&SORT=D&1%2C1%2C
  | isbn =0-949861-17-0
}}</ref> She was passionate about childcare issues,<ref name = OurWomenOurState /> and through her political work clashed particularly with conservative Queensland Premier [[Joh Bjelke-Peterson]].<ref name = Young2 />

==Early Life and Young Adulthood==
Born in [[Austria]] in 1909 to Jewish parents, Eva Goldner was aware of fascism and anti-Semitism from early in her life.<ref name = Young1 />  She was a Communist militant in her youth and as a member of [[International Red Aid]], a Communist organisation established to provide aid to 'class-war' political prisoners,<ref>[[International Red Aid]]</ref> she worked to help victims of fascism.<ref name = OurWomenOurState /> Goldner and her mother, as leftist Jewish women, were forced to flee Austria in 1939 following the Nazi occupation of Austria, migrating to Australia after some time in England.<ref name = Young2 />
Goldner, a dressmaker and fashion designer by trade,<ref name = Grant /> became involved with the left in her new home where she attended her first [[International Women’s Day]] meeting the same year she arrived.<ref name = Grant /> She attributed that meeting to inspiring much of her later political activism. Goldner soon became a member of the [[Communist Party of Australia]],<ref name = Grant /> where, at a performance by the Unity Theatre Group, she met her husband Ted Bacon, a returned soldier and fellow communist and political activist.<ref name = Healy>{{cite book
  | last = Healy
  | first = Connie
  | title = Defiance: Political Theatre in Brisbane 1930-1962
  | publisher = Boombana Publications
  | year = 2000
  | location = Australia
  | url = http://library.uq.edu.au/search~S7?/XPolitical+theatre+in+brisbane&SORT=D/XPolitical+theatre+in+brisbane&SORT=D&SUBKEY=Political%20theatre%20in%20brisbane/1%2C9%2C9%2CB/frameset&FF=XPolitical+theatre+in+brisbane&SORT=D&2%2C2%2C
  | isbn = 1-876542-04-7 }}</ref> They married in 1944 in [[Brisbane]]<ref name = Marriage /> and had one child, a daughter, named Barbara.<ref name = Young1 />

== Politics ==

=== Communist Party of Australia ===
During her life in Australia, Eva Bacon was heavily involved in the Communist Party of Australia. A communist in her youth in Austria, it was natural for Bacon to seek out like-minded people when she escaped the Nazis for Australia. Through her involvement with the CPA, Bacon met and married her husband Ted,<ref name = Healy /> who served as the Secretary for the Queensland Branch of the CPA for some time.<ref>{{cite web
  | last = McGuire
  | first = John
  | title = Julius, Max Nordau (1916 – 1963) - Biography
  | publisher = Australian Dictionary of Biography
  | year = 1996
  | url = http://adb.anu.edu.au/biography/julius-max-nordau-10652
  | accessdate = 2011-10-19}}</ref> Ted and Eva were heavily involved in the CPA as shown by the hundreds of leaflets, pamphlets, typed speeches, and letters from protest, rallies, events, and meetings contained in their personal Archive.<ref name = Archive>{{Citation
  | title = E.A. and E. Bacon Collection, Fryer Archive Library, University of Queensland number UQFL 241, boxes 1-18
  | url = http://www.library.uq.edu.au/fryer/ms/uqfl241.pdf
}}</ref>

While Bacon insisted that her Communist leanings did not affect her work with other political groups such as the UAW,<ref name = Young2 /> it is clear that her involvement in the CPA had a negative impact on her reliability in other areas of her life. Right-wing politicians such as [[Joh Bjelke-Peterson]] drew connections between her involvement with the two groups to try and discredit the UAW as a Communist organisation.<ref name = Brennan/>

Bacon was a member of the [[Central Committee]] of the CPA since 1948 by her own account,<ref>{{Citation
  | last = Bacon
  | first = Eva
  | title = The Struggle for Unity in the Labour Movement in the Post War Period
  | url = http://www.library.uq.edu.au/fryer/ms/uqfl241.pdf
}} E.A. and E. Bacon Collection, Fryer Archive Library, University of Queensland, number UQFL 241, folder JQ 4098. C6B3216? Fryer, box 12.</ref> and she and Ted remained committed members throughout their lives.

=== Joh Bjelke-Peterson ===
Sir [[Joh Bjelke-Petersen]] was the Premier of Queensland from 8 August 1968 – 1 December 1987 who personally objected to Eva Bacon's political activities and her membership of the Communist Party.<ref name = Brennan>{{cite book
  | last = Brennan
  | first = Frank
  | title = Too Much Order with Too Little Law
  | publisher = University of Queensland Press
  | year = 1983
  | location = St Lucia, Queensland
  | pages = 129
  | url = http://library.uq.edu.au/search~S7?/Xfrank+brennan&SORT=D/Xfrank+brennan&SORT=D&SUBKEY=frank%20brennan/1%2C53%2C53%2CB/frameset&FF=Xfrank+brennan&SORT=D&26%2C26%2C
  | isbn = 0-7022-1842-1
}}</ref> His attitude towards political activists, and groups such as the [[Union of Australian Women]] was paternalistic: “...all these protest groups consist of the hard-core activists who hide behind the gullible, the naive and the well-meaning.”<ref name = Brennan /> On the subject of Joh Bjelke-Petersen, Eva Bacon has been quoted as saying “I believe Mr Bjelke-Petersen in using my personal political commitment is trying to enlist support for his anti-democratic policies”.<ref name = Young1 />

The Bjelke-Petersen government's ‘censorship laws’ were designed to limit the impact of the CPA in Queensland by censoring the publication of positive content about the organisation in the media, instead promoting negative perceptions of the group, and all the organisations with which it was associated.<ref name = Healy /> The Queensland government's ‘obscenity’ laws also restricted the promotion or dissemination of sexually orientated information even if it was educational.<ref>{{cite news
  | title = Leaflet called obscene.
  | newspaper = The Courier Mail
  | location = Queensland
  | pages = 12
  | type = microfilm
  | date = 7 Oct 1971
 <!-- | accessdate = 2011-10-17 -->}}</ref> The [[Women's liberation movement|Women's Liberation Movement]]’s pamphlet ‘Female Sexuality and Education’ was considered to be obscene by Bjelke-Petersen, and was confiscated from the Brisbane offices of Communist Party of Australia by the ‘State Licensing Branch’ of the Queensland Police Department at 5 p.m. on Friday 8 October 1971.<ref>{{Citation
  | last = Gifford
  | first = C. E.
  | title = Press Release
  | location = Queensland
  | year = 1971
  | url = http://www.library.uq.edu.au/fryer/ms/uqfl241.pdf
}} Located in the E.A. and E. Bacon Collection, Fryer Library, University of Queensland, number UQFL 241, box 6.</ref>

== Women's Rights ==

=== Union of Australian Women ===
The [[Union of Australian Women]] was, and in [[Victoria (Australia)|Victoria]] remains, a feminist activist organisation that relies on letter writing, petitions, marches and demonstrations to gain its objectives.<ref name = UAWHistory>{{cite web
  | title = Union of Australian Women: History and Background
  | publisher = Union of Australian Women Victoria
  | year = 2006
  | url = http://home.vicnet.net.au/~uawvic/about_us.htm
  | accessdate = 2014-04-14}}</ref> Some of these objectives were as follows:
*      to achieve and maintain an enhanced status for women
*      to obtain higher living standards for all
*      for the government to improve the welfare of its citizens
*	for public infrastructure to be shared by all (regardless of gender or race)
*	the right for women to work
*	to obtain fertility control
*	for the equality of indigenous Australians
*	to oppose the White Australia Policy<ref name = UAWHistory />

As an older organisation, it has had to change some of its goals over time, whilst maintaining its primary aims. This was evident for Bacon during the 1970s as she has spoken of the introduction of the notion that “the ‘personal’ is political”, and ‘sexual politics’ taking three years for the organisation to fully support. Her struggle with this shift was due to the move away from the economic and general political struggles that the UAW had previously focused on as the way to gain equality with men.<ref name  = Young2 /> As a member of the [[Union of Australian Women]], Bacon involved herself in numerous activities including writing for the UAW's magazine, ''[[Our Women]]'', as well as working on books such as ''Uphill all the way: A Documentary History of Women in Australia''. Her involvement in the Indigenous rights movement is acknowledged by the [[National Museum of Australia]].<ref>{{cite web
  | title =Union of Australian Women
  | publisher = National Museum of Australia
  | year = 2008
  | url = http://www.indigenousrights.net.au/organisation.asp?oID=13
  | accessdate = 2014-04-14}}</ref> Although believed to be a faction of the [[Communist Party of Australia]] by the Premier of Queensland at the time, [[Joh Bjelke-Petersen]], the Union of Australian Women has always maintained its independence from the CPA.<ref name = Young2 /><ref name = Brennan />

=== International Women's Day ===
Eva Bacon attended her first [[International Women’s Day]] (IWD) meeting in 1939, not long after she migrated to Australia.<ref name = Grant /> She became heavily involved in IWD activities after her involvement with the UAW began, and acted as the International Women’s Day Committee secretary for the UAW between 1951 and 1974.<ref name = OurWomenOurState /><ref name = Grant /><ref name = Stevens2>{{cite web
  | last = Stevens
  | first = Joyce
  | title = A History of International Women’s Day in Words and Images: The Nineteen Fifties and Sixties
  | url = http://www.isis.aust.com/iwd/stevens/50s60s.htm
  | accessdate = 2011-10-18}}</ref>

In 1958 Bacon was involved in with the UAW's International Women's Year celebrations which drew a crowd of hundreds to hear speakers [[Dymphna Cusak]] and Dame [[Sybil Thorndyke]], a celebration which also addressed women’s influence on history, socialism, and world peace. [[Eleanor Roosevelt]] sent a message about this meeting to the UAW, among others.<ref name = Stevens2 />

In 1960 Bacon coordinated a visit to Australia by Madam [[Chao Feng]] of the [[National Women’s Federation of China]] and Madame [[Roesijati R. Sukardi]], a journalist with the [[Indonesian Women’s Organisation]] to attend IWD meetings nationally.  As Australia did not recognise China at the time, the UAW set about obtaining visas for the visitors and were delayed to the point that Feng and Sukardi could not attend several of their appointed meetings.<ref name = Stevens2 />

According to personal anecdotes, Bacon was most proud of her involvement in the UWA's program for reviving the IWD celebrations in Australia following [[World War II]]. She encouraged women to join marches and demonstrations on IWD's official date of 8 March<ref name = Young1 /> and 'saw IWD as a campaign, needing work almost all the year round with 8 March as the highlight, rather than a one-day function.'<ref name = Stevens2 />

=== International Women's Year 1975 ===
The [[United Nations]] marked 1975 as [[International Women’s Year]] (IWY) and declared the [[Decade for Women]] covering 1976-1985.<ref name = Choike>{{cite web
  | publisher = United Nations
  | title = 1st World Conference on Women, Mexico 1975: Resolution adopted by the General Assembly of the United Nations concerning the World Conference on International Women's Year
  | url = http://www.choike.org/nuevo_eng/informes/1453.html
  | accessdate = 2010-10-18}}</ref> In this year a [[First World Conference on Women|World Conference on Women]] was held in Mexico for international government representatives to take part in and a Tribune simultaneously for other groups and interested parties<ref name = Stevens /> attended by women from all over the world to come together and discuss women’s issues and women’s rights.<ref name = Choike />

As her involvement with International Women’s Day events was so strong at home,<ref name = OurWomenOurState /> Eva Bacon was invited to attend as a delegate to the Tribune by the Australian Government who invested over $3 million into the 1974-1976 budgets for International Women’s Year activities.<ref>{{cite web
  | author = National Archive of Australia
  | title = International Women’s Year, 1975 – Fact Sheet 237
  | url = http://www.naa.gov.au/collection/fact-sheets/fs237.aspx
  | accessdate = 2011-10-18}}</ref> Bacon was the only representative from Queensland chosen to take part in the Tribune,<ref name = Grant /> taking part as a member and on behalf of the UAW according to official papers found in her personal Archive, along with Government documents and documents from the Australian Embassy in Mexico confirming her involvement.<ref>{{Citation
  | title=E.A. and E. Bacon Collection, Fryer Archive Library, University of Queensland, number UQFL 241, box 13.
|url = http://www.library.uq.edu.au/fryer/ms/uqfl241.pdf
}}</ref>

=== Childcare ===
Throughout her activist career Bacon lobbied for children’s rights and for the establishment of appropriate childcare facilities through every party she was involved in.<ref name = OurWomenOurState /> During the 1950s Bacon traveled back to Europe to attend conferences on motherhood and childcare,<ref name = Grant /><ref name = Young1 /> and in 1967 through the UAW Bacon lobbied for after school and work based child care for mothers, arguing to the [[Queensland Government]] that these were legitimate concerns for mothers and children.<ref name = OurWomenOurState /> Through her work with the UAW, Bacon was involved in the establishment of a childcare centre at [[The University of Queensland]] in 1971.<ref name=OurWomenOurState />

== The Eva and Ted Bacon Archive Collection ==
The [[Fryer Library]] at the [[University of Queensland]] holds one of the most extensive collections of archival material documenting Left-wing and radical activism in [[Brisbane]] and statewide.<ref name = Fryer>{{cite web
  | title = Fryer Library: Special Collections Library of the University of Queensland
  | publisher = University of Queensland
  | url = http://www.library.uq.edu.au/fryer/
  | accessdate = 2011-10-20}}</ref><ref>{{cite web
  | title = Online Exhibitions: Radical Politics and the University of Queensland
  | work = Fryer Library: Special Collections Library of the University of Queensland
  | publisher = University of Queensland
  | url = http://www.library.uq.edu.au/fryer/radical_politics/page1.html
  | accessdate = 2011-10-20}}</ref> The collection includes publications, literature and personal archives of key groups and figures of the movement including that of Eva and Ted Bacon.<ref>{{cite web
  | title = Online Exhibitions: International Women’s Day, March&nbsp;8th
  | work = Fryer Library: Special Collections Library of the University of Queensland
  | publisher = University of Queensland
  | url = http://www.library.uq.edu.au/fryer/iwd/
  | accessdate = 2011-10-20}}</ref> The Bacon archive was donated in the late 1980s in the hope of protecting the documents in the face of a still heavily anti-communist public sphere.<ref name = Archive /> Additions were continually made until 1999 when Ted's brother, on behalf of the deceased couple, compiled the last of their work and the Fryer archive became officially available for academic use.<ref name = Archive /> The Eva and Ted Bacon collection consists of a wide range of publications, photos and other literature as well as a series of hand-written notes, research and drafted political theses.<ref name = Archive />

== Selected articles published in ''Our Women'' ==
* 'Blue Flowers', 1961, October–December issue.<ref>{{cite journal|last=Bacon|first=Eva|title=Blue Flowers |journal=Our Women|date=October–December 1961|url=http://library.uq.edu.au/search~S7?/Xeva+bacon&searchscope=7&SORT=D/Xeva+bacon&searchscope=7&SORT=D&extended=0&SUBKEY=eva%20bacon/1%2C4%2C4%2CB/frameset&FF=Xeva+bacon&searchscope=7&SORT=D&1%2C1%2C}}</ref>
* 'The Magic Circle', 1964, June–September issue.<ref>{{cite journal|last=Bacon|first=Eva|title=The Magic Circle|journal=Our Women|date=June–September 1964|url=http://library.uq.edu.au/search~S7?/Xeva+bacon&searchscope=7&SORT=D/Xeva+bacon&searchscope=7&SORT=D&extended=0&SUBKEY=eva%20bacon/1%2C4%2C4%2CB/frameset&FF=Xeva+bacon&searchscope=7&SORT=D&1%2C1%2C}}</ref>
* 'Tropical Fruit Salad', 1965, March–May issue.<ref>{{cite journal|last=Bacon|first=Eva|title=Tropical Fruit Salad|journal=Our Women|date=March–May 1965|url=http://library.uq.edu.au/search~S7?/Xeva+bacon&searchscope=7&SORT=D/Xeva+bacon&searchscope=7&SORT=D&extended=0&SUBKEY=eva%20bacon/1%2C4%2C4%2CB/frameset&FF=Xeva+bacon&searchscope=7&SORT=D&1%2C1%2C}}</ref>
* 'The Language of Flowers', 1965, June–August issue.<ref>{{cite journal|last=Bacon|first=Eva|title=The Language of Flowers|journal=Our Women|date=June–August 1965|url=http://library.uq.edu.au/search~S7?/Xeva+bacon&searchscope=7&SORT=D/Xeva+bacon&searchscope=7&SORT=D&extended=0&SUBKEY=eva%20bacon/1%2C4%2C4%2CB/frameset&FF=Xeva+bacon&searchscope=7&SORT=D&1%2C1%2C}}</ref>

== References ==

{{Reflist}}

== External links ==

{{DEFAULTSORT:Bacon, Eva}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:1909 births]]
[[Category:1994 deaths]]
[[Category:Australian feminists]]
[[Category:Australian socialists]]
[[Category:Australian communists]]
[[Category:Women's International League for Peace and Freedom people]]