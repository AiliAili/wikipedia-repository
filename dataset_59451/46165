{{Infobox isotope
    | symbol =Am
    | mass_number =241
    | mass =241.056829144
    | num_neutrons =146
    | num_protons =95
    | abundance = [[synthetic element|none (synthetic)]]
    | halflife =432.2 years
    | background =#7F7
    | text_color =#FF4500
    | image = Americium button hd.jpg
    | image_caption = Small button containing <sup>241</sup>AmO<sub>2</sub> from a smoke alarm
    | decay_product =isotopes of neptunium#237
    | decay_symbol =Np
    | decay_mass =237
    | decay_mode1 =[[alpha decay|α-decay]] (alpha)
    | decay_energy1 =5.486
    | decay_mode2 =[[gamma ray|γ-emission]] (gamma)
    | decay_energy2 =59.5409 [[keV]]
0.0595409
    | decay_mode3 =[[Cluster decay|CD]] (Cluster Decay)
    | decay_mode4 =[[spontaneous fission|SF]] (Spontaneous Fission)
    | parent =Plutonium-241
    | parent_symbol =Pu
    | parent_mass =241
    | parent_decay =beta
    | parent2 =Curium-241
    | parent2_symbol =Cm
    | parent2_mass =241
    | parent2_decay =EC
    | parent3 =Berkelium-245
    | parent3_symbol =Bk
    | parent3_mass =245
    | parent3_decay =alpha
    | spin =5/2-
    | excess_energy =52.936008 [[MeV]]
52,930.6008
    | binding_energy =7.543272 [[MeV]]
7,543.272
    | decay_heat =114 [[Watts]] per kilogram per second
|magnetic_moment=1.61μ|parity=-1|quadrupole_moment=4.2}}

'''Americium-241 (<sup>241</sup>Am)''' is an [[Isotopes of americium|isotope of americium]]. Like all isotopes of americium, it is [[radioactive]]. <sup>241</sup>Am is the most common [[isotope]] of [[americium]]. It is the most prevalent isotope of americium in [[nuclear waste]]. Americium-241 has a [[half-life]] of 432.2 years. It is commonly found in ionization type [[Smoke detector|smoke detectors]]. It is a potential fuel for long-lifetime [[radioisotope thermoelectric generators]] (RTGs). Its common parent nuclides are [[beta decay|β<sup>−</sup>]] from <sup>241</sup>Pu, [[electron capture|EC]] from <sup>241</sup>Cm and [[alpha decay|α]] from <sup>245</sup>Bk. <sup>241</sup>Am is [[fissile]] and the [[critical mass]] of a bare sphere is 57.6-75.6 kilograms and a sphere diameter of 19–21 centimeters.<ref name="dias">http://typhoon.tokai-sc.jaea.go.jp/icnc2003/Proceeding/paper/6.5_022.pdf{{full citation needed|date=March 2009}} Dias et al.</ref> Americium-241 has a specific activity of 3.43 Ci/g (Curies per gram or 117.29 Gigabequerels (GBq) per gram).<ref>{{cite web|url = http://web.ornl.gov/info/reports/1962/3445605995483.pdf|publisher = Oak Ridge National Laboratory|title = The Preparation, Properties and Uses of Americium-241, Alpha, Gamma and Neutron Sources}}</ref> It is commonly found in the form of [[americium dioxide|americium-241 dioxide]] (<sup>241</sup>AmO<sub>2</sub>). This isotope also has one [[nuclear isomer|meta state]]; <sup>241m</sup>Am, with an exitation energy of 2.2 [[MeV]], and a [[half-life]] of 1.23 μs. Its presence in [[plutonium]] is determined by the original concentration of plutonium-241 and the sample age. Because of the low penetration of alpha radiation, americium-241 only poses a health risk when ingested or inhaled. Older samples of plutonium containing plutonium-241 contain a buildup of <sup>241</sup>Am. A chemical removal of americium-241 from reworked plutonium (e.g. during reworking of [[plutonium pit]]s) may be required in some cases.

== Nucleosynthesis ==
Americium-241 has been produced in small quantities in [[nuclear reactor]]s for decades, and many kilograms of <sup>241</sup>Am have been accumulated by now.<ref name="g12622">Greenwood, p. 1262</ref> Nevertheless, since it was first offered for sale in 1962, its price, about 1,500 [[United States dollar|USD]] per gram of <sup>241</sup>Am, remains almost unchanged owing to the very complex separation procedure.<ref name="smoke">[http://www.world-nuclear.org/info/inf57.html Smoke detectors and americium], World Nuclear Association, January 2009, Retrieved 28 November 2010</ref>

Americium-241 is not synthesized directly from uranium – the most common reactor material – but from the plutonium isotope <sup>239</sup>Pu. The latter needs to be produced first, according to the following nuclear process:

: <math>\mathrm{^{238}_{\ 92}U\ \xrightarrow {(n,\gamma)} \ ^{239}_{\ 92}U\ \xrightarrow [23.5 \ min]{\beta^-} \ ^{239}_{\ 93}Np\ \xrightarrow [2.3565 \ d]{\beta^-} \ ^{239}_{\ 94}Pu}</math>

The capture of two neutrons by <sup>239</sup>Pu (a so-called (n,γ) reaction), followed by a β-decay, results in <sup>241</sup>Am:

: <math>\mathrm{^{239}_{\ 94}Pu\ \xrightarrow {2~(n,\gamma)} \ ^{241}_{\ 94}Pu\ \xrightarrow [14.35 \ yr]{\beta^-} \ ^{241}_{\ 95}Am}</math>

The plutonium present in spent nuclear fuel contains about 12% of <sup>241</sup>Pu. Because it converts to <sup>241</sup>Am, <sup>241</sup>Pu can be extracted and may be used to generate further <sup>241</sup>Am.<ref name="smoke"/> However, this process is rather slow: half of the original amount of <sup>241</sup>Pu decays to <sup>241</sup>Am after about 14 years, and the <sup>241</sup>Am amount reaches a maximum after 70 years.<ref>[http://www.bredl.org/sapc/Pu_ReportI.htm BREDL Southern Anti-Plutonium Campaign], Blue Ridge Environmental Defense League, Retrieved 28 November 2010</ref>

The obtained <sup>241</sup>Am can be used for generating heavier americium isotopes by further neutron capture inside a nuclear reactor. In a [[light water reactor]] (LWR), 79% of <sup>241</sup>Am converts to <sup>242</sup>Am and 10% to its [[nuclear isomer]] <sup>242m</sup>Am:<ref>{{cite journal|doi=10.3327/jnst.41.448|author=Sasahara, A. |title=Neutron and Gamma Ray Source Evaluation of LWR High Burn-up UO<sub>2</sub> and MOX Spent Fuels|journal=Journal of Nuclear Science and Technology|date=2004|volume=41|issue=4|pages=448–456|url=http://www.jstage.jst.go.jp/article/jnst/41/4/448/_pdf|display-authors=etal}} [http://sciencelinks.jp/j-east/ article/200410/000020041004A0333355.php Abstract]</ref>

:79%:&nbsp;&nbsp; <math>\mathrm{^{241}_{\ 95}Am\ \xrightarrow {(n,\gamma)} \ ^{242}_{\ 95}Am}</math>

== Decay ==
{{Main article|Radioactive decay}}Americium-241 decays mainly via [[alpha decay]], with a weak [[gamma ray]] byproduct. The α-decay is shown as follows:

<math>\mathrm{^{241\!\,}_{\ 95}Am\ \overset{432.2y}{\longrightarrow} \ ^{237}_{\ 93}Np~+~^{4}_{2}\alpha^{2+} +\gamma~59.5409~keV}</math>

The α-decay energies are 5.486 [[Electronvolt|MeV]] for 85% of the time (the one which is widely accepted for standard α-decay energy), 5.443 MeV for 13% of the time, and 5.388 MeV for the remaining 2%.<ref>{{Cite web|url = http://www.researchcompliance.uc.edu/Libraries/Isotopes/Am-241.sflb.ashx|title = AMERICIUM-241|access-date = }}</ref> The [[Gamma ray|γ-ray]] energy is 59.5409 [[Electronvolt|keV]] for the most part, with little amounts of other energies such as 13.9 keV, 17.8 keV and 26.4 keV.<ref>{{Cite web|url = http://www.iaea.org/inis/collection/NCLCollectionStore/_Public/29/041/29041560.pdf|title = GAMMA RAY SPECTRUM OF AM-241 IN A BACK SCATTERING GEOMETRY USING A HIGH PURITY GERMANIUM DETECTOR}}</ref>

The second most common type of decay for americium-241 is cluster decay, with a branching ratio of less than 7.4×10<sup>−16</sup>. Also shown as follows:

<math>\mathrm{^{241\!\,}_{\ 95}Am\longrightarrow^{207}_{\ 81}Tl+^{34}_{14}Si}</math>

The least common (rarest) type of decay that americium-241 undergoes is [[spontaneous fission]], with a branching ratio of 4×10<sup>−12</sup> and happening 1.2 times a second per gram of <sup>241</sup>Am. It is written as such (the asterisk denotes an excited nucleus):

<math>\mathrm{^{241}_{\ 95}Am\longrightarrow~^{241}_{\ 95}Am^*\longrightarrow3^1_0n~+~fission~products~+energy~(\gamma)}
</math>

== Applications ==

===Ionization-type smoke detector===
{{Main article|Smoke detector}}Americium-241 is the only synthetic isotope to have found its way into the household, where the most common type of [[smoke detector]] (the ionization-type) uses <sup>241</sup>AmO<sub>2</sub> (americium-241 dioxide) as its source of [[ionizing radiation]].<ref>{{citation 
|url=http://www.uic.com.au/nip35.htm |archiveurl=https://web.archive.org/web/20080303223058/http://www.uic.com.au/nip35.htm 
|archivedate=2008-03-03 |title=Smoke Detectors and Americium |work=Nuclear Issues Briefing Paper |volume=35 |date=May 2002 |accessdate=2015-08-26}}</ref> This isotope is preferred over <sup>226</sup>[[radium|Ra]] as <sup>241</sup>Am emits 5 times more alpha particles and also emits relatively little harmful gamma radiation. With its half-life of 432.2 years, the americium in a smoke detector decreases and includes about 4.4% [[neptunium]] after 19 years, and about 7.4% after 32 years.The amount of americium in a typical new smoke detector is 0.29 [[microgram]] (about one-third the weight of a grain of sand) with an activity of 1 [[Curie|microcurie]]/37 kilobequerels (1.0 μCi/37 [[Becquerel|kBq]]). Some old industrial smoke detectors (notably from the Pyrotronics Corporation) can contain up to 80 μCi. The amount of <sup>241</sup>Am declines slowly as it decays into [[Isotopes of neptunium|neptunium-237]] a different transuranic element with a much longer half-life (about 2.14 million years). The radiated [[alpha-particles]] pass through an [[ionization chamber]], an air-filled space between two [[electrode]]s, which allows a small, constant [[electric current]] to pass between the capacitor plates due to the radiation ionizing the air space between. Any smoke that enters the chamber blocks/absorbs some of the alpha particles from freely passing through and reduces the ionization and therefore causes a drop in the current. The alarm's circuitry detects this drop in the current and as a result, triggers the piezoelectric buzzer to sound. Compared to the alternative optical smoke detector, the ionization smoke detector is cheaper and can detect particles which are too small to produce significant light scattering. However, it is more prone to [[false alarms]].<ref>Residential Smoke Alarm Performance, Thomas Cleary. Building and Fire Research Laboratory, National Institute of Standards and Technology; UL Smoke and Fire Dynamics Seminar. November 2007</ref><ref name="NIST">Bukowski, R. W. ''et al.'' (2007) [http://www.fire.nist.gov/bfrlpubs/fire07/art063.html Performance of Home Smoke Alarms Analysis of the Response of Several Available Technologies in Residential Fire Settings], NIST Technical Note 1455-1</ref><ref>{{cite web |url=http://media.cns-snc.ca/pdf_doc/ecc/smoke_am241.pdf|title = Smoke detectors and americium-241 fact sheet|publisher = Canadian Nuclear Society|accessdate =31 August 2009}}</ref><ref>{{cite web|url=http://www.atsdr.cdc.gov/toxprofiles/tp156.pdf|format=PDF; 2.1 MB|title=Toxicological Profile For Americium|author=Gerberding, Julie Louise |publisher=[[United States Department of Health and Human Services]]/[[Agency for Toxic Substances and Disease Registry]]|accessdate=29 August 2009|date=2004| archiveurl= https://web.archive.org/web/20090906112953/http://www.atsdr.cdc.gov/toxprofiles/tp156.pdf| archivedate= 6 September 2009 | deadurl= no}}</ref>

==== Manufacturing process ====
The process for making the americium used in the buttons on ionization-type smoke detectors begins with americium dioxide. The AmO<sub>2</sub> is thoroughly mixed with gold, shaped into a briquette, and fused by pressure and heat at over 1470°F (800°C). A backing of silver and a front covering of gold (or an alloy of gold or [[palladium]]) are applied to the briquette and sealed by hot forging. The briquette is then processed through several stages of cold rolling to achieve the desired thickness and levels of radiation emission. The final thickness is about 0.008 inches (0.2 mm), with the gold cover representing about one percent of the thickness. The resulting foil strip, which is about 0.8 inches (20 mm) wide, is cut into sections 39 inches (1 meter) long. The sources are punched out of the foil strip. Each disc, about 0.2 inches (5 mm) in diameter, is mounted in a metal holder, usually made of aluminum. The holder is the   housing, which is the majority of what is seen on the button. The thin rim on the holder is rolled over to completely seal the cut edge around the disc.<ref>{{Cite web|url=http://www.madehow.com/Volume-2/Smoke-Detector.html|title=Smoke Detector|last=|first=|date=|website=|dead-url=|access-date=}}</ref>

===Radionuclide===
As <sup>241</sup>Am has a ''roughly'' similar half-life to <sup>238</sup>Pu (432.2 years vs. 87 years), it has been proposed as an active [[isotope]] of [[radioisotope thermoelectric generator]]s, for use in spacecraft.<ref name="RTG">[http://fti.neep.wisc.edu/neep602/SPRING00/lecture5.pdf Basic elements of static RTGs]</ref><ref>G.L. Kulcinski, NEEP 602 Course Notes (Spring 2000), Nuclear Power in Space, University of Wisconsin Fusion Technology Institute (see last page)</ref> Even though americium-241 produces less heat and electricity than plutonium-238 (the power yield is 114.7&nbsp;mW/g for <sup>241</sup>Am vs. 390&nbsp;mW/g for <sup>238</sup>Pu).<ref name="RTG"/> and although its radiation poses a bigger threat to humans owing to gamma and neutron emission, the [[European Space Agency]] is still considering to use americium-241 for its space probes, as a result of the global shortage of plutonium-238.<ref>[http://www.spaceflightnow.com/news/n1007/09rtg/ Space agencies tackle waning plutonium stockpiles],  Spaceflight now, 9 July 2010</ref>

===Neutron source===
Oxides of <sup>241</sup>Am pressed with [[beryllium]] can be very efficient [[neutron source]]s, since it emits [[alpha particle]] during its [[radioactive decay]]:

: <math>\mathrm{^{241\!\,}_{\ 95}Am\ \overset{432.2y}{\longrightarrow} \ ^{237}_{\ 93}Np\ +\ ^{4}_{2}\alpha^{2+} +\ \gamma~59.5~keV}</math>
Here americium acts as the alpha source, and beryllium produces neutrons owing to its large cross-section for the (α,n) nuclear reaction:
: <math display="inline">\mathrm{^{9}_{4}Be\ +\ ^{4}_{2}\alpha^{2+} \longrightarrow \ ^{12}_{\ 6}C\ +\ ^{1}_{0}n\ +\ \gamma}</math>

The most widespread use of <sup>241</sup>AmBe neutron sources is a [[neutron probe]] – a device used to measure the quantity of water present in soil, as well as moisture/density for quality control in highway construction. <sup>241</sup>Am neutron sources are also used in well logging applications, as well as in [[neutron radiography]], tomography and other radiochemical investigations.

=== Production of other elements ===
[[File:Sasahara.svg|thumb|307x307px|Chart displaying actinides and their decays and transmutations.]]Americium-241 is sometimes used as a starting material for the production of other transuranic elements and [[transactinide]]s – for example, neutron bombardment of <sup>241</sup>Am yields <sup>242</sup>Am:
<math>\mathrm{^{241}_{\ 95}Am\ \xrightarrow {(n,\gamma)} \ ^{242}_{\ 95}Am}</math>

From there, 82.7% of <sup>242</sup>Am decays to <sup>242</sup>Cm and 17.3% to <sup>242</sup>Pu:

82.7% '''→''' <math>\mathrm{^{241}_{\ 95}Am\ \xrightarrow {(n,\gamma)} \ ^{242}_{\ 95}Am\ \xrightarrow [16.02 \ h]{\beta^-} \ ^{242}_{\ 96}Cm}</math>

17.3%'''→''' <math>\mathrm{^{241}_{\ 95}Am\ \xrightarrow {(n,\gamma)} \ ^{242}_{\ 95}Am\ \xrightarrow [16.02 \ h]{\beta^+} \ ^{242}_{\ 94}Pu}</math>

In the nuclear reactor, <sup>242</sup>Am is also up-converted by neutron capture to <sup>243</sup>Am and <sup>244</sup>Am, which transforms by β-decay to <sup>244</sup>Cm:
: <math>\mathrm{^{242}_{\ 95}Am\xrightarrow {(n,\gamma)}~^{243}_{\ 95}Am\ \xrightarrow {(n,\gamma)} \ ^{244}_{\ 95}Am\ \xrightarrow [10.1 \ h]{\beta^-} \ ^{244}_{\ 96}Cm}</math>

Irradiation of <sup>241</sup>Am by <sup>12</sup>C or <sup>22</sup>Ne ions yields the isotopes <sup>253</sup>Es ([[einsteinium]]) or <sup>263</sup>Db ([[dubnium]]), respectively.<ref name="Binder">{{cite book|last= Binder |first=Harry H.|title = Lexikon der chemischen Elemente: das Periodensystem in Fakten, Zahlen und Daten : mit 96 Abbildungen und vielen tabellarischen Zusammenstellungen|date = 1999|isbn = 978-3-7776-0736-8}}</ref> Furthermore, the element [[berkelium]] (<sup>243</sup>Bk isotope) had been first intentionally produced and identified by bombarding <sup>241</sup>Am with alpha particles, in 1949, by the same Berkeley group, using the same 60-inch cyclotron that had been used for many previous experiments. Similarly, [[nobelium]] was produced at the [[Joint Institute for Nuclear Research]], [[Dubna]], Russia, in 1965 in several reactions, one of which included irradiation of <sup>243</sup>Am with <sup>15</sup>N ions. Besides, one of the synthesis reactions for [[lawrencium]], discovered by scientists at Berkeley and Dubna, included bombardment of <sup>243</sup>Am with <sup>18</sup>O.<ref name="g1252">Greenwood, p. 1252</ref>

=== Spectrometer ===
Americium-241 has been used as a portable source of both gamma rays and alpha particles for a number of medical and industrial uses. The 59.5409 keV gamma ray emissions from <sup>241</sup>Am in such sources can be used for indirect analysis of materials in [[radiography]] and [[X-ray fluorescence]] spectroscopy, as well as for quality control in fixed [[nuclear density gauge]]s and [[nuclear densometer]]s. For example, this isotope has been employed to gauge [[glass]] thickness to help create flat glass.<ref name="g12622">Greenwood, p. 1262</ref> Americium-241 is also suitable for calibration of gamma-ray spectrometers in the low-energy range, since its spectrum consists of nearly a single peak and negligible Compton continuum (at least three orders of magnitude lower intensity).<ref>[http://www.nndc.bnl.gov/nudat2/indx_dec.jsp Nuclear Data Viewer 2.4], NNDC</ref>

=== Medicine ===
Americium-241 gamma rays has been used to provide passive diagnosis of thyroid function. This medical application is now obsolete. Americium-241's gamma rays can provide reasonable quality [[Radiography|radiographs]], with a 10-minute exposure time. <sup>241</sup>Am radiographs have only been taken for experimentally and are never used due to the long exposure time which is carcinogenic (due to gamma exposure), as well as the fact that like before, it takes a long time.<ref>{{Cite web|url=http://web.ornl.gov/info/reports/1962/3445605995483.pdf|title=Americium-241 Uses|last=|first=|date=|website=|publisher=|access-date=}}</ref>

==Hazards==
Americium-241 is a form of [[americium]] therefore having the same general hazards. Americium and its isotopes are both extremely toxic and radioactive. Although [[alpha particle|α-particles]] can be stopped by a sheet of paper, there are serious health concerns for ingestion of α-emitters. Americium and its isotopes are also very chemically toxic as well, in the form of heavy-metal toxicity. As little as 0.03 μCi (1,110 Bq) is the maximum permissible body burden for <sup>241</sup>Am. <ref>{{Cite web|url=http://www.nucleonica.net/wiki/index.php?title=Americium_Am|title=Americium Am|last=|first=|date=|website=|publisher=|access-date=}}</ref>

Americium-241 is an α-emitter with a weak γ-ray byproduct. Safely handling americium-241 requires knowing and following proper safety precautions, as without them it would be extremely dangerous. Its specific gamma dose constant is 3.14 x 10<sup>−1</sup> mR/hr/mCi or 8.48 x10<sup>−5</sup> mSv/hr/MBq at 1 meter. <ref>{{Cite web|url = http://www.researchcompliance.uc.edu/Libraries/Isotopes/Am-241.sflb.ashx|title = AMERICIUM-241 [241Am]}}</ref>

If consumed, americium-241 is excreted within a few days and only 0.05% is absorbed in the blood. From there, roughly 45% of it goes to the [[liver]] and 45% to the bones, and the remaining 10% is excreted. The uptake to the liver depends on the individual and increases with age. In the bones, americium is first deposited over [[Cortex (anatomy)|cortical]] and [[trabecula]]r surfaces and slowly redistributes over the bone with time. The biological half-life of <sup>241</sup>Am is 50 years in the bones and 20 years in the liver, whereas in the [[gonad]]s (testicles and ovaries) it remains permanently; in all these organs, americium promotes formation of cancer cells as a result of its radioactivity.<ref>Frisch, Franz ''Crystal Clear, 100 x energy'', Bibliographisches Institut AG, Mannheim 1977, ISBN 3-411-01704-X, p. 184</ref>

Americium-241 often enters landfills from discarded [[smoke detector]]s. The rules associated with the disposal of smoke detectors are relaxed in most jurisdictions. In the U.S., the "Radioactive Boy Scout" [[David Hahn]] was able to concentrate americium-241 from smoke detectors after managing to buy a hundred of them at remainder prices and also stealing a few.<ref name="Silverstein2005">[[Ken Silverstein]], [http://www.harpers.org/archive/1998/11/0059750 The Radioactive Boy Scout: When a teenager attempts to build a breeder reactor]. ''[[Harper's Magazine]]'', November 1998</ref><ref>{{cite news|publisher = [[Fox News]] |url= http://www.foxnews.com/story/0,2933,292111,00.html |title='Radioactive Boy Scout' Charged in Smoke Detector Theft|date=4 August 2007|accessdate=28 November 2007| archiveurl= https://web.archive.org/web/20071208062559/http://www.foxnews.com/story/0,2933,292111,00.html| archivedate= 8 December 2007 | deadurl= no}}</ref><ref>{{cite news|work=Detroit Free Press|url= http://www.freep.com/apps/pbcs.dll/article?AID=/20070827/BUSINESS05/70827091 |title=Man dubbed 'Radioactive Boy Scout' pleads guilty |date=27 August 2007 |agency=Associated Press |accessdate=27 August 2007 |archiveurl = https://web.archive.org/web/20070929095926/http://www.freep.com/apps/pbcs.dll/article?AID=/20070827/BUSINESS05/70827091|archivedate = 29 September 2007}}</ref><ref>{{cite news|publisher = [[Fox News]]|url= http://www.foxnews.com/story/0,2933,299362,00.html |title= 'Radioactive Boy Scout' Sentenced to 90 Days for Stealing Smoke Detectors|date=4 October 2007|accessdate=28 November 2007| archiveurl= https://web.archive.org/web/20071113123408/http://www.foxnews.com/story/0,2933,299362,00.html| archivedate= 13 November 2007 | deadurl= no}}</ref> There have been a few cases of exposure to americium-241, the worst case being that of [[Harold McCluskey]], who at the age of 64 was exposed to 500 times the occupational standard for americium-241 as a result of an explosion in his lab. McCluskey died at the age of 75, not as a result of exposure, but of a [[heart disease]] which he had before the accident.<ref name="tristateherald">{{cite news|first=Annette |last=Cary |title=Doctor remembers Hanford's 'Atomic Man' |publisher=''Tri-City Herald'' |url=http://www.hanfordnews.com/news/2008/story/11403.html |date=25 April 2008 |accessdate=17 June 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20100210232231/http://www.hanfordnews.com/news/2008/story/11403.html |archivedate=10 February 2010 |df= }}</ref><ref>{{cite news|author = AP wire|title = Hanford nuclear workers enter site of worst contamination accident|url = http://www.billingsgazette.com/index.php?id=1&display=rednews/2005/06/03/build/nation/94-contamination.inc|date = 3 June 2005|accessdate =17 June 2007 |archiveurl=http://health.phys.iit.edu/extended_archive/2005-June/002075.html |archivedate=13 June 2005}}</ref>

==References==
{{reflist}}

[[Category:Isotopes of americium]]
[[Category:Radioisotope fuels]]