{{Use dmy dates|date=March 2015}}
{{Use British English|date=March 2015}}
{{good article}}
{{Infobox football match
|                   title = 1969 FA Cup Final
|                   image = [[File:1969facupfinalprog.png|150px]]
|                 caption = Match programme cover
|                   event = [[1968–69 FA Cup]]
|                   team1 = [[Manchester City F.C.|Manchester City]]
|        team1association = 
|              team1score = 1
|                   team2 = [[Leicester City F.C.|Leicester City]]
|        team2association = 
|              team2score = 0
|                 details = 
|                    date = 26 April 1969
|                 stadium = [[Wembley Stadium (1923)|Wembley Stadium]]
|                    city = [[London]]
|      man_of_the_match1a = [[Allan Clarke (footballer born 1946)|Allan Clarke]] ([[Leicester City F.C.|Leicester City]])
| man_of_the_match1atitle = 
|      man_of_the_match1b = 
| man_of_the_match1btitle = 
|                 referee = [[George McCabe]] ([[South Yorkshire]])
|              attendance = 100,000
|                 weather = 
|                previous = [[1968 FA Cup Final|1968]]
|                    next = [[1970 FA Cup Final|1970]]
}}
The '''1969 FA Cup Final''' was the final match of the [[1968–69 FA Cup|1968–69 staging]] of [[England|English]] [[association football|football]]'s primary [[Single-elimination tournament|cup competition]], the Football Association Challenge Cup, better known as the [[FA Cup]]. The match was contested between [[Leicester City F.C.|Leicester City]] and [[Manchester City F.C.|Manchester City]] at [[Wembley Stadium (1923)|Wembley Stadium]] in [[London]] on Saturday 26 April 1969. This was the first FA Cup final since 1951 to take place in the month of April. Three-time winners Manchester City were appearing in their seventh final, whereas Leicester City were seeking to win the competition for the first time, having lost three previous finals.

Each team won six ties to reach the final, and overcame one of the 1968 finalists ([[West Bromwich Albion F.C.|West Bromwich Albion]] and [[Everton F.C.|Everton]]) at the semi-final stage. As Manchester City were reigning league champions and Leicester City were battling to avoid relegation, the Manchester club were strong favourites. The match finished 1–0 to Manchester City. The goal came in the 24th minute, scored by [[Neil Young (footballer born 1944)|Neil Young]].<ref>{{cite news|last=Odgen|first=Mark|url=http://www.telegraph.co.uk/sport/football/teams/manchester-city/8246770/Roberto-Mancini-to-lead-red-and-black-scarf-tribute-to-Manchester-City-FA-Cup-hero-Neil-Young.html|title=Roberto Mancini to lead red-and-black scarf tribute to Manchester City FA Cup hero Neil Young|newspaper=Daily Telegraph|date=7 January 2011|accessdate=9 January 2011| archiveurl= https://web.archive.org/web/20110120191850/http://www.telegraph.co.uk/sport/football/teams/manchester-city/8246770/Roberto-Mancini-to-lead-red-and-black-scarf-tribute-to-Manchester-City-FA-Cup-hero-Neil-Young.html| archivedate= 20 January 2011 <!--DASHBot-->| deadurl= no}}</ref> The victory was Manchester City's fourth FA Cup win.

==Route to the final==
{| class="wikitable" style="text-align:center;margin-left:1em;float:right"
|+ '''Leicester City'''
! style="width:25px;"| Round
! style="width:100px;"| Opposition
! style="width:50px;"| Score
|-
|rowspan="2"| 3rd
| [[Barnsley F.C.|Barnsley]] (a)
| 1–1
|-
| Barnsley (h)
| 2–1
|-
| 4th
| [[Millwall F.C.|Millwall]] (a)
| 1–0
|-
|rowspan="2" |5th
| [[Liverpool F.C.|Liverpool]] (h)
| 0–0
|-
| Liverpool (a)
| 1–0
|-
| 6th
| [[Mansfield Town F.C.|Mansfield Town]] (a)
| 1–0
|-
| Semi-final
| [[West Bromwich Albion F.C.|West Bromwich Albion]] ([[Hillsborough Stadium|n]])
| 1–0
|}As both Leicester City and Manchester City were [[Football League First Division|First Division]] clubs, they entered the competition in the third round.

Leicester City started their cup run against [[Barnsley F.C.|Barnsley]], but required a replay to overcome their Third Division opponents 2–1. The first Leicester goal was controversial, as the referee overruled his linesman, who had flagged for a foul. Barnsley equalised with a penalty, but Leicester quickly retook the lead. Later in the second half Leicester claimed a third goal, but the referee adjudged that the ball had not crossed the line.<ref>{{cite news|author=Michael Carey| title=Barnsley's persistence and skill wanes|publisher=The Guardian|date=1969-01-09}}</ref> A 1–0 win at Millwall followed.

In the fifth round, Leicester City faced [[Liverpool F.C.|Liverpool]]. The match was postponed six times before it eventually took place on 1 March.<ref name="Observer5th">{{cite news|author=Leslie Duxbury| title=Odds are now on Liverpool|publisher=The Observer |date=1969-03-02}}</ref> A 0–0 draw meant a replay at [[Anfield]]. [[Andy Lochhead]] gave the ''Foxes'' the lead on 34 minutes. Five minutes later McArthurs's handball gave Liverpool a penalty, but [[Peter Shilton]] saved [[Tommy Smith (footballer born 1945)|Tommy Smith]]'s spot-kick. Liverpool attacked for much of the second half, but Leicester held out to win 1–0.<ref>{{cite news|author=Tom German| title=Liverpool's power overcome by Leicester|publisher=The Times |date=1969-03-04}}</ref>  The quarter final saw a trip to [[Mansfield Town F.C.|Mansfield Town]], who had knocked out clubs from five different divisions. On a pitch described by ''The Times''' Geoffrey Green as "resembling a glutinous swamp", Leicester won 1–0. The goal was a header by [[Rodney Fern]] from a [[Len Glover]] cross.<ref>{{cite news|author=Geoffrey Green| title=Cup win clouded by relegation worry|publisher=The Times |date=1969-03-10}}</ref> The semi-final, played at [[Hillsborough Stadium]], was against cup-holders [[West Bromwich Albion F.C.|West Bromwich Albion]]. In a game of few chances, Allan Clarke scored from an Andy Lochhead knockdown with four minutes remaining.<ref name="Observersemi">{{cite news|author=Arthur Hopcraft| title=Clarke gives the killer touch to lucky Leicester|publisher=The Observer |date=1969-03-30}}</ref> Leicester City gained their fourth 1–0 win of the competition and reached the final.

{| class="wikitable" style="text-align:center;margin-left:1em;float:right"
|+ '''Manchester City'''
! style="width:25px;"| Round
! style="width:100px;"| Opposition
! style="width:50px;"| Score
|-
| 3rd
| [[Luton Town F.C.|Luton Town]] (h)
| 1–0
|-
|rowspan="2" |4th
| [[Newcastle United F.C.|Newcastle United]] (a)
| 0–0
|-
| Newcastle United (h)
| 2–0
|-
|5th
| [[Blackburn Rovers F.C.|Blackburn Rovers]] (a)
| 4–1
|-
| 6th
| [[Tottenham Hotspur F.C.|Tottenham Hotspur]] (h)
| 1–0
|-
| Semi-final
| [[Everton F.C.|Everton]] ([[Villa Park|n]])
| 1–0
|}
Manchester City's first tie was against [[Luton Town F.C.|Luton Town]] of the Third Division, which City won 1–0, [[Francis Lee]] the scorer.<ref>Summerbee, ''Mike Summerbee: The Autobiography, p. 187.</ref> In the fourth round, the club were drawn away to [[Newcastle United F.C.|Newcastle United]], one of only two top-flight clubs with an away draw.<ref>{{cite news|author=Geoffrey Green| title=Luck favours big clubs in FA Cup draw|publisher=The Times |date=1969-03-04}}</ref> The match finished 0–0, and so was replayed at [[Maine Road]]. Manchester City won 2–0, but had to play much of the match with 10 men after [[Mike Summerbee]] was [[Penalty card|sent off]].<ref name="Summerbee188">Summerbee, ''Mike Summerbee: The Autobiography, p. 188.</ref> Like Leicester's fifth round tie, Manchester City's match at [[Blackburn Rovers F.C.|Blackburn Rovers]]' [[Ewood Park]] was delayed multiple times due to poor weather. When it was eventually played Manchester City were comfortable 4–1 winners.<ref name="Summerbee188"/> In the sixth round, Manchester City were drawn at home to [[Tottenham Hotspur F.C.|Tottenham Hotspur]]. The match was closely contested; in his autobiography, City's [[Mike Doyle (footballer)|Mike Doyle]] described it as the hardest match of the whole cup run.<ref>Doyle, ''Blue Blood'', p. 57.</ref> As in the third round, City won 1–0 thanks to a Francis Lee goal.<ref>Summerbee, ''Mike Summerbee: The Autobiography'', pp. 189–90.</ref>

The semi-final, against [[Everton F.C.|Everton]], was played at [[Villa Park]]. Everton were renowned for their powerful midfield of [[Alan Ball Jr.|Ball]], [[Colin Harvey|Harvey]] and [[Howard Kendall|Kendall]], but opted to play more defensively than usual. Manchester City nevertheless paid special attention to this area, and instructed [[David Connor (footballer)|David Connor]] to [[Marking (association football)|man-mark]] Ball.<ref>Tossell, ''Big Mal'', p. 167.</ref> Mike Doyle suffered an injury in the first half, and spent 20 minutes off the field. City had the better of the game, but goalscoring chances were missed by Lee and Young.<ref>{{cite news|author=Geoffrey Green| title=Everton fear defeat too much to win|publisher=The Times |date=1969-03-23}}</ref> With little time remaining, Young forced a corner. Young took the corner himself, from which teenager [[Tommy Booth]] scored the game's only goal.<ref>Penney, ''Blue Heaven'', pp. 77–8.</ref>

==Build-up==
Manchester City were appearing in the final for the seventh time. They had won the cup three times previously (in [[1904 FA Cup Final|1904]], [[1934 FA Cup Final|1934]] and [[1956 FA Cup Final|1956]]), and had been beaten in the final three times (in [[1926 FA Cup Final|1926]], [[1933 FA Cup Final|1933]] and [[1955 FA Cup Final|1955]]). Leicester City were making their fourth cup final appearance, and their third of the decade, having lost on all three previous occasions (in [[1949 FA Cup Final|1949]], [[1961 FA Cup Final|1961]] and [[1963 FA Cup Final|1963]]). The clubs had met in the FA Cup in each of the preceding three seasons. In 1966 Manchester City won a fifth round tie 1–0 after a replay, and won again in the third round in 1967.<ref>{{cite news|author=Eric Todd| title=Sparring's over – time for action|publisher=The Guardian|date=1969-04-26 |page=16}}</ref> In the 1967–68 season Leicester finally prevailed. After a 0–0 draw at Maine Road, Leicester City came back from 2–0 down at Filbert Street to win 4–3.<ref>{{cite book |last=Goldstone |first=Phil |author2=Saffer, David |title=Manchester City Champions 1967/68 |publisher=Tempus |location=Stroud |year=2005 |isbn=978-0-7524-3611-1 |page=74}}</ref>

Manchester City manager Joe Mercer named his team for the final several days in advance. [[Glyn Pardoe]] missed training on Monday 21st with a leg injury,<ref name="Guardian22apr">{{cite news|title=Squads named for Wembley|publisher=The Guardian|date=1969-04-22|page=19}}</ref> but after the fitness of Pardoe, [[Tony Coleman]] and [[Alan Oakes]] was tested in a practice match, all three were passed fit.<ref name="Times22apr">{{cite news|title=Mercer chooses Wembley team|publisher=The Times|date=1969-04-22|page=13}}</ref> Leicester City named a 14-man squad before travelling to a training camp in [[Bisham]] on the Tuesday.<ref name="Times22apr"/> Manchester City travelled south on the Thursday, staying in [[Weybridge]].<ref name="Times22apr"/> Leicester had fitness doubts over [[Dave Gibson (footballer)|Dave Gibson]], [[Len Glover]] and [[John Sjoberg]], but initially expected all three to be available for the final.<ref name="Guardian22apr"/> However, in a practice match against [[Brentford F.C.|Brentford]], Sjoberg had to leave the field with a groin injury.<ref name="Guardian24apr">{{cite news|title=Sjoberg's injury worries Leicester|publisher=The Guardian|date=1969-04-24|page=21}}</ref> His place in the team was taken by [[Alan Woollett]].<ref name="Times26apr"/>

Each club received 16,000 tickets for the final from [[the Football Association]].<ref>{{cite news|title=George McCabe, a firm believer in man management|publisher=The Guardian|date=1969-04-25|page=22}}</ref> The match was televised live by the [[BBC]] and [[ITV (TV network)|ITV]]. Both broadcasters devoted several hours to match build-up, incorporating FA Cup-themed versions of other programmes, such as ''Cup Final [[It's a Knockout]]''.<ref>Tossell, ''Big Mal''. p. 168.</ref>

As the previous season's league champions, Manchester City were strong favourites, particularly as Leicester were embroiled in a struggle to avoid [[promotion and relegation|relegation]] from the First Division. ''The Times'' correspondent anticipated that the condition of the pitch would influence the match, stating that "if — as it is said — it is in a good, lush state Manchester will be happy. If, on the other hand, it proves to be heavy, then it could suit Leicester the better."<ref name="Times26apr"/> On the day before the game Joe Mercer criticised the pitch, likening it to a cabbage patch.<ref>{{cite news|title=Pitch "A cabbage patch"|publisher=The Guardian|date=1969-04-26|page=1}}</ref>

==Match==
At 21, Leicester's [[David Nish]] became the youngest ever captain of a cup finalist.<ref>{{cite web|url=http://www.lcfc.com/page/ClubTemplate4/0,,10274~1108196,00.html| title=David Nish |publisher=Leicester City FC |accessdate=2010-01-03}}</ref> His opposite number [[Tony Book]] became the third oldest at 35.<ref name="Times26apr">{{cite news|author=Geoffrey Green| title=Manchester City aim to entertain and win|publisher=The Times|date=1969-04-26|page=12}}</ref> Book had missed a large part of season through injury,<ref name="Times10apr">{{cite news|author=Geoffrey Green| title=Book travels from building site to Wembley in five years|publisher=The Times|date=1969-04-10|page=12}}</ref> but upon returning his impact was so great that he shared the award for the 1969 [[FWA Footballer of the Year]].<ref name="Times26apr"/>

As the teams prepared to leave their dressing rooms, Manchester City deliberately delayed their exit by a short period to play on any nerves the Leicester City players may have had.<ref name="book88">Book, ''Maine Man'', p. 88</ref> Manchester City coach Malcolm Allison was not permitted to take his place on the bench, as he was serving a touchline ban. Instead, he had to sit in the stand behind the dugout.<ref name="book88"/> Before kick-off, the players were introduced to the guest of honour, [[Anne, Princess Royal|Princess Anne]].<ref>Summerbee, Mike Summerbee: The Autobiography, p. 196.</ref>

Fears that the contest would be a mismatch proved to be unfounded, with Leicester playing in a more attacking manner than anticipated. ''The Observer'''s Hugh McIvanney wrote: "Suggestions that Leicester would attempt to minimise the discrepancy in talents by a concentration on defensive spoiling were exposed as unjust…offering the deceptively languid dribbles of Clarke, the thoughtful passes of Roberts and Gibson and the thrustful running of Lochhead as proof that the skills were not all on one side."<ref name="Observerreport">{{cite news|author=Hugh McIlvanney| title=Manchester prove they are masters|publisher=The Observer |date=1969-04-27}}</ref> [[Neil Young (footballer born 1944)|Neil Young]] and [[Tony Coleman]] both had early scoring chances for Manchester City, but missed the target. For Leicester City, a dribbling run by Clarke ended in a shot that was saved by Dowd,<ref name="Observerreport"/> and a mishit shot by Len Glover was cleared off the goal-line by a defender.<ref name="Times28apr"/> Manchester City scored midway through the first half. [[Mike Summerbee]] crossed the ball from wide on the right, and Young hit a left footed shot high into [[Peter Shilton]]'s net.<ref name="Times28apr">{{cite news|author=Geoffrey Green| title=Manchester City's sights set high|publisher=The Times|date=1969-04-28|page=6}}</ref> Few further chances occurred in the first half.<ref name="Observerreport"/>

Just after half-time, Leicester City had their strongest scoring chance, when [[Andy Lochhead]] received a headed knockdown from [[Allan Clarke (footballer born 1946)|Allan Clarke]], but Lochhead's shot went high above the goal.<ref name="Times28apr"/> A dominant period by Manchester City then followed, which included a chance for [[Colin Bell]] from a free-kick.<ref name="Observerreport"/> [[Len Glover]] was forced to move into defence with his team under pressure, but was injured shortly afterward and had to be substituted.<ref name="Observerreport"/> Defender [[Malcolm Manley]] came on in his place.<ref>Pawson, ''100 Years of the FA Cup'', p. 268.</ref>

A poll of journalists named Allan Clarke as man of the match.<ref name="Observerreport"/>

===Summary===
{{football box
|date=26 April 1969
|time=15:00 [[Western European Summer Time|BST]]
|team1=[[Manchester City F.C.|Manchester City]]
|score=1–0
|team2=[[Leicester City F.C.|Leicester City]]
|report=[https://web.archive.org/web/20100415223938/http://www.fa-cupfinals.co.uk/1969.htm Report]
|goals1=[[Neil Young (footballer born 1944)|Young]] {{goal|24}}
|goals2=
|stadium=[[Wembley Stadium (1923)|Wembley]], [[London]]
|attendance=100,000
|referee=[[George McCabe]] ([[South Yorkshire]]) }}

{| width=92% |
|-
|{{Football kit
 | pattern_la = _black_stripes_thin1
 | pattern_b  = _blackstripes_thin2
 | pattern_ra = _black_stripes_thin1
 | pattern_so = _redtop_2whitestripes
 | leftarm    = FF0000
 | body       = FF0000
 | rightarm   = FF0000
 | shorts     = 000000
 | socks      = 000000
 | title      = Manchester City
}}
|{{Football kit
 | pattern_la = _whiteborder
 | pattern_b  = _whitecollar
 | pattern_ra = _whiteborder
 | pattern_sh = 
 | pattern_so = _3_stripes_white
 | leftarm    = 0000DD
 | body       = 0000DD
 | rightarm   = 0000DD
 | shorts     = FFFFFF
 | socks      = 0000DD
 | title      = Leicester City
}}
|}

{| style="width:100%;"
|-
|  style="vertical-align:top; width:50%;"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0"
|-
|colspan="4"|
|-
!width="25"| !!width="25"|
|-
|GK ||'''1''' ||{{flagicon|ENG}} [[Harry Dowd]]
|-
|RB ||'''2''' ||{{flagicon|ENG}} [[Tony Book]] ([[Captain (association football)|c]])
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Glyn Pardoe]]
|-
|CB ||'''4''' ||{{flagicon|ENG}} [[Mike Doyle (footballer)|Mike Doyle]]
|-
|CB ||'''5''' ||{{flagicon|ENG}} [[Tommy Booth]] 
|-
|CM ||'''6''' ||{{flagicon|ENG}} [[Alan Oakes]]
|-
|RW ||'''7''' ||{{flagicon|ENG}} [[Mike Summerbee]]
|-
|CM ||'''8''' ||{{flagicon|ENG}} [[Colin Bell]]
|-
|FW ||'''9''' ||{{flagicon|ENG}} [[Francis Lee]]
|-
|FW ||'''10''' ||{{flagicon|ENG}} [[Neil Young (footballer born 1944)|Neil Young]]
|-
|LW ||'''11'''||{{flagicon|ENG}} [[Tony Coleman]]
|-
|colspan=4|'''Substitutes:'''
|-
|DF ||'''12'''||{{flagicon|ENG}} [[David Connor (footballer)|David Connor]]
|-
|colspan=4|'''Manager:'''
|-
|colspan="4"| {{flagicon|ENG}} [[Joe Mercer]]
|}
|  style="vertical-align:top; width:50%;"|
{| cellspacing="0" cellpadding="0" style="font-size:90%; margin:auto;"
|-
|colspan="4"|
|-
!width="25"| !!width="25"|
|-
|GK ||'''1''' || {{flagicon|ENG}} [[Peter Shilton]]
|-
|DF ||'''2''' || {{flagicon|WAL}} [[Peter Rodrigues]]
|-
|DF ||'''3''' || {{flagicon|ENG}} [[David Nish]] ([[Captain (association football)|c]]) 
|-
|MF ||'''4''' || {{flagicon|SCO}} [[Bobby Roberts (footballer)|Bobby Roberts]]
|-
|DF ||'''5''' || {{flagicon|ENG}} [[Alan Woollett]]
|-
|DF ||'''6''' || {{flagicon|ENG}} [[Graham Cross]] 
|-
|MF ||'''7''' || {{flagicon|ENG}} [[Rodney Fern]]
|-
|MF ||'''8''' || {{flagicon|SCO}} [[Dave Gibson (footballer)|Dave Gibson]]
|-
|FW ||'''9''' || {{flagicon|SCO}} [[Andy Lochhead]]
|-
|FW ||'''10'''|| {{flagicon|ENG}} [[Allan Clarke (footballer born 1946)|Allan Clarke]] 
|-
|MF ||'''11'''|| {{flagicon|ENG}} [[Len Glover]] || || {{suboff|70}}
|-
|colspan=4|'''Substitutes:'''
|-
|DF ||'''12'''|| {{flagicon|SCO}} [[Malcolm Manley]] || || {{subon|70}}
|-
|colspan=4|'''Manager:'''
|-
|colspan="4"| {{flagicon|IRL}} [[Frank O'Farrell]]
|}
|}

{|  style="width:100%; font-size:90%;"
|-
|  style="width:50%; vertical-align:top;"|
'''Match rules'''
*90 minutes.
*30 minutes of extra-time if necessary.
*Replay if scores still level.
*One named substitute.
|}

==Post-match==
The Manchester City team returned to Manchester the following evening. They travelled by train to [[Wilmslow]], from where they undertook a 13-mile parade in an open-topped bus. 25,000 people lined the route, with a further 3,000 people in [[Albert Square, Manchester|Albert Square]], where the parade finished.<ref>{{cite news|author=William Hanley| title=Ebullient welcome for Cup winners|publisher=The Guardian|date=1969-04-28|page=18}}</ref> Three days later, the team paraded the cup in front of their supporters before their match against [[West Ham United F.C.|West Ham United]] at [[Maine Road]].<ref>{{cite news| title=Pardoe shows his forwards how it's done|publisher=The Guardian|date=1969-05-01|page=11}}</ref>

The good conduct of the supporters of both teams was praised in Parliament by MPs [[Barnett Janner, Baron Janner|Barnett Janner]] and 
[[Tom Boardman, Baron Boardman|Tom Boardman]].<ref>{{cite news| title=Cup crowd praised|publisher=The Guardian|date=1969-05-02|page=22}}</ref>

Manchester City's cup was their fourth. In winning the trophy, Joe Mercer became the first person to win the league championship and FA Cup as both a captain and a manager.<ref name="Times28apr"/>  By winning the competition, Manchester City earned the right to compete in the [[1969–70 European Cup Winners' Cup]]. City went on to win the Cup Winners' Cup, beating [[Górnik Zabrze]] 2–1 in [[1970 European Cup Winners' Cup Final|the final]].<ref>Ward, ''The Manchester City Story'', p. 64.</ref> Leicester City continued to struggle in their remaining league matches and were relegated to the Second Division. Leicester became only the second club to reach a cup final and suffer relegation in the same season. By coincidence, the other club to have done so was Manchester City, who were subject to the same fate in [[1926 FA Cup Final|1926]].<ref>Ward, ''The Manchester City Story'', p. 63.</ref>

==References==
{{reflist|colwidth=30em}}

==Bibliography==
*{{cite book |last= Book|first= Tony|author2= David Clayton| title= Maine Man |publisher= Mainstream publishing |year= 2004 |isbn= 1-84018-812-X}}
*{{cite book |last=Doyle |first=Mike |author2=& Clayton, David |title=Blue Blood: the Mike Doyle Story |publisher=Bluecoat |location=Liverpool |year=2006 |isbn=1-904438-38-5}}
*{{cite book |last=Goldstone |first=Phil |author2=Saffer, David |title=Manchester City Champions 1967/68 |publisher=Tempus |location=Stroud |year=2005 |isbn=978-0-7524-3611-1}}
*{{cite book |last=Pawson |first=Tony |title=100 Years of the FA Cup |publisher=Heinemann |location=London |year=1972 |isbn=0-330-23274-6}}
*{{cite book |last=Penney |first=Ian |title=Blue Heaven: Manchester City's Greatest Games |publisher=Mainstream |location=Edinburgh|year=1996 |isbn=1-85158-872-8}}
*{{cite book |last=Summerbee |first=Mike |title=Mike Summerbee: The Autobiography |publisher=Century |location=London|year=2008 |isbn=978-1-8460-5493-8}}
*{{cite book |last=Tossell|first=David |title=Big Mal: The High Life and hard Times of Malcolm Allison, Football Legend |publisher=Mainstream |year=2008 |location=Edinburgh |isbn=978-1-84596-478-8}}
*{{cite book |last=Ward |first=Andrew |title=The Manchester City Story |publisher=Breedon|location=Derby |year=1984 |isbn=0-907969-05-4}}

==External links==
*[http://www.fa-cupfinals.co.uk/1969.htm FA Cup Finals Match Report]
*[http://www.rtfract.com/citypast.htm "Richard Tucker's Website" – contains numerous photographs related to Manchester City, including many of the 1969 Cup-Final.]

{{FA Cup Finals}}
{{1968–69 in English football}}
{{Manchester City F.C. matches}}
{{Leicester City F.C. matches}}

{{DEFAULTSORT:1969 Fa Cup Final}}
[[Category:1968–69 FA Cup|Final]]
[[Category:FA Cup Finals]]
[[Category:Leicester City F.C. matches|FA Cup Final 1969]]
[[Category:Manchester City F.C. matches|FA Cup Final 1969]]
[[Category:April 1969 sports events]]
[[Category:1969 in London]]