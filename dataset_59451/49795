{{italic title}}
{{no footnotes|date=March 2010}}

'''''Saracinesca''''' is a [[novel]] by [[F. Marion Crawford]], first published as a serial in ''[[Blackwood's Magazine]]'' and then as a book in New York (''[[Macmillan Publishers|Macmillan]]'') and Edinburgh (''Blackwood'') in 1887. Set chiefly in Rome of twenty years earlier, the novel paints a rich picture of the period, detailing the spiritual and economic problems of the aristocracy at a time when its influence and status were under attack from the emerging forces of modernity. This romance tells the tale of Giovanni Saracinesca and his courting of Corona d'Astradente, complete with intrigue and sword fights (Crawford was an expert fencer). It can be categorized as a work of historical fiction in that it relates a time when the author was only a child, and also in the sense that the particulars of that time and place are carefully delineated. In a sense, Crawford had been researching for this book all his life: his parents had witnessed the brief 1848 revolution, and his cousin, in her memoirs of Crawford, insisted that "[t]here is little doubt that Crawford as a boy had heard first-hand descriptions of [the] exciting events" of the 1860s.

''Saracinesca'' proved to be both an immediate hit and Crawford's greatest critical success. It was also a commercial triumph: he negotiated separate contracts for the serial printing and the simultaneous American and British publication, as well as future royalties. He followed it with two brilliant sequels, ''Sant' Ilario'' and ''Don Orsino,'' the three of which are usually considered a [[trilogy]]. Subsequent sequels, such as ''Corleone,'' continue the saga of Saracinesca family, but with a diversion from the previous focus on the drama and status of family members into heavily plotted, incident-heavy melodrama. Characters from ''Saracinesca'' and its sequels also appear in ''A Lady of Rome'' (1906) and ''The White Sister'' (1909).

Crawford, though an American by parentage and citizenship, was born in the Italian resort of Bagni de Lucca, spent most of his life abroad, and wrote ''Saracinesca'' while living in [[Sant' Agnello di Sorrento, Italy]].

==References==
*John Pilkington, Jr. (1964): Francis Marion Crawford, Twayne Publishers Inc. (Library of Congress Catalog Number: 64-20717)
*Maud Howe Elliott (1934): My Cousin, F. Marion Crawford, The Macmillan Company
*John Charles Moran (1981) : An F. Marion Crawford Companion, Greenwood Press

== External links ==
{{Gutenberg|no=13757|name=Saracinesca}}

[[Category:1887 novels]]
[[Category:Works originally published in Blackwood's Magazine]]
[[Category:Novels first published in serial form]]
[[Category:Novels set in Rome]]
[[Category:1860s in fiction]]
[[Category:Novels by Francis Marion Crawford]]
[[Category:19th-century American novels]]