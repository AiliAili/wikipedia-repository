{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox officeholder
|honorific-prefix = [[Lieutenant-Colonel]]
|name             = Ayscoghe Boucherett
|honorific-suffix = 
|image            = <!--Would love to find one: Kenneth Garlick, ''Sir Thomas Lawrence: A Complete Catalogue of the Oil Paintings'', 1989 lists him in the catalogue, though I do not own the book and so cannot read it in full -->
|caption = 
|alt              = 
|office           = [[Parliament of the United Kingdom|Member of Parliament]] for<br>[[Grimsby (UK Parliament constituency)|Grimsby]]
|term_start       = 1796
|term_end         = 1803
|predecessor      = [[John Harrison (Member of Parliament)|John Harrison]] <br/> [[Dudley Long North|Dudley North]]
|alongside        = [[William Mellish]] (1796–1802) and [[John Henry Loft]] (1802–1803)
|successor        = Hon. [[Charles Anderson-Pelham, 1st Earl of Yarborough|Charles Anderson Pelham]] <br/> [[John Henry Loft]]
|majority         = 
|birthname        = Ayscoghe Boucherett
|birth_date       = {{birth date|df=yes|1755|04|16}}
|birth_place      = 
|death_date       = {{death date and age|df=yes|1815|09|15|1755|04|16}}
|death_place      =
|party            = 
|spouse           = {{Marriage|Emelia Crockatt|1789}}
|children         = Emilia Mary, Ayscoghe, Maria, Juliana
|residence        = Willingham House, [[Lincolnshire]]<ref>[https://books.google.com/books?id=ZrAUAAAAQAAJ&source=gbs_navlinks_s ''The Gentleman's Magazine''], 1815, part ii, p. 371</ref>
|alma_mater       = 
|religion         = 
|website          = 
|signature        = 
}}

[[Lieutenant colonel (United Kingdom)|Lieutenant-Colonel]] '''Ayscoghe Boucherett''', {{post-nominals|country=GBR|JP|DL}} (16 April 1755 – 15 September 1815) was a British landowner, businessman and Member of Parliament for [[Great Grimsby]] from 1796 to 1803.

Born into a family of the Lincolnshire landed gentry, Boucherett became involved in local politics in Lincolnshire, and (owing mainly to his marriage) with artistic and mercantile circles in London. He was the chairman of the [[Grimsby Haven Company]], which oversaw the reopening and expansion of [[Old Dock, Grimsby|Grimsby's first dock]]. He was a friend of the artist [[Sir Thomas Lawrence]] and the proprietor of Willingham, Lincolnshire, where he constructed his country seat, [[Willingham House]], in 1790. For his investment in the Haven Company, he received the support of [[Charles Anderson-Pelham, 1st Baron Yarborough|Lord Yarborough]], one of its main investors and a principal land-owner in Grimsby; owing largely to Lord Yarborough's patronage, Boucherett was returned as the Member of Parliament for that borough at the 1796 election. He was not a frequent voter, but used his position to further the interests of his corporation. Nonetheless, the company met with financial difficulties after it opened the Harbour in 1800. In 1803, Boucherett resigned his seat in favour of Yarborough's heir and pursued a quieter political life. He died in a carriage accident in 1815.

==Early life and family connections==
[[File:Thomas Lawrence - Portrait of the Children of Ayscoghe Boucherett - WGA12514.jpg|thumb|upright=1.3|''The Children of Ayscoghe Boucherett'' by [[Thomas Lawrence]], c.&nbsp;1808]]
Boucherett was born on 16 April 1755, the son of Ayscoghe Boucherett of [[North Willingham|Willingham]] and [[Stallingborough]], Lincolnshire, and his wife, Mary White.<ref name="Stokes 1986">Stokes 1986</ref> The elder Boucherett had been the [[High Sheriff of Lincolnshire]] in 1754,<ref>''London Gazette''. 29 January 1754, p. 1.</ref> and was a landed gentleman in Lincolnshire, whose family was descended from Huguenot merchants; they married into the Ayscoghe family and inherited the Willingham estate through this marriage.<ref>Burke 1847, p. 122</ref> The elder Boucherett's daughter, Mary, had married [[Michael Barne (politician)|Michael Barne]] of Sotterley, Suffolk, an army officer and a member of parliament for [[Dunwich]].<ref>Burke 1847, 1, p. 122 ; Maddison 1902, p. 163 ; Howard & Crisp 1899, p. 158 ; Stokes 1986</ref>

The younger Boucherett was admitted at [[Queen's College, Cambridge]], in 1773, aged 18, but did not take a degree.<ref>J. Venn and J.A. Venn, [http://venn.lib.cam.ac.uk/cgi-bin/search-130418.pl?sur=boucherett&suro=w&fir=&firo=c&cit=&cito=c&c=all&tex=&sye=&eye=&col=all&maxcount=50 "Boucherett, Ayscoghe", ''Alumni Cantabrigienses'']{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> He married, on 17 March 1789, Emelia Crockatt, daughter of Charles Crockatt, a merchant, of London and of Luxborough Hall, Essex, and his wife, Anna Muilman,<ref group="note">[[Thomas Gainsborough]] painted a group portrait (including Crockatt and Muilman's brother) which is thought to have been commissioned to celebrate their engagement. For further details of this portrait and their families, see [http://www.tate.org.uk/art/artworks/gainsborough-peter-darnell-muilman-charles-crokatt-and-william-keable-in-a-landscape-t06746/text-summary "''Peter Darnell Muilman, Charles Crockatt and William Keable in a Landscape'', c. 1750, Thomas Gainsborough (Summary)"]. Tate. Retrieved 02 November 2014.</ref> who married, when widowed, the insurance broker and art connoisseur [[John Julius Angerstein]]. This union helped the younger Boucherett to garner connexions in London merchant circles.<ref>Stokes 1986 ; ''Report on the Export of Works of Art'' ''1991-92'', 1995, p. 26; Palmer, ODNB, J. J. Angerstein; <span>J. Venn and J.A. Venn, </span>[http://venn.lib.cam.ac.uk/cgi-bin/search-130418.pl?sur=boucherett&suro=w&fir=&firo=c&cit=&cito=c&c=all&tex=&sye=&eye=&col=all&maxcount=50 "Boucherett, Ayscoghe", ''Alumni Cantabrigienses'']{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> Emilia Boucherett died on 5 February 1837, aged 75.<ref>Maddison 1902, p. 163</ref> The Boucheretts had one son and three daughters:

* '''Emilia Mary Boucherett''' (7 August 1790 – 29 November 1870). Died unmarried.<ref name="ReferenceA">Burke 1847, 1, p. 122 ; Burke 1879, p. 167 ; Maddison 1902, p. 163</ref>
* '''Ayscoghe Boucherett''', J.P., D.L. (24 September 1791 – 1857), he was [[High Sheriff of Lincolnshire]] in 1820. He married, on 11 May 1816, Louisa Pigou, daughter of Frederick John Pigou of Dartford, Kent, and his wife Louisa, née Minchin. They had: Ayscoghe Boucherett (1817–32), Henry Robert Boucherett, J.P. (1818–77), who was High Sheriff of Lincolnshire in 1866, Hugo Boucherett (1819 – c. 1839), Louisa Boucherett (1821–95), and [[Emilia Jessie Boucherett]] (1825–1905), who was a noted women's rights campaigner.<ref>Burke 1847, 1, p. 122 ; Burke 1879, p. 167 ; Maddison 1902, p. 163 ; see ODNB for E. J. Boucherett for mother's name.</ref>
* '''Maria Boucherett''' (born 30 October 1795). She married, on 15 August 1815, Charles Parker Newdigate Newdegate of Harefield, Middlesex; their only child was the [[Conservative Party (UK)|Conservative]] politician and Member of Parliament for [[North Warwickshire (UK Parliament constituency)|North Warwickshire]], [[Charles Newdigate Newdegate]].<ref>Burke 1847, 1, p. 122 ; Burke 1847, 2, p. 924 ; Burke 1879, p. 167 ; Maddison 1902, p. 163 ; ODNB for C. N. Newdigate</ref>
* '''Juliana Boucherett''' (bapt. 27 April 1798). Died unmarried.<ref name="ReferenceA"/>

Boucherett paid for the construction of a new family seat in 1790; Willingham House was a larger and grander mansion than the family's previous seat closer to Willingham, and was constructed in the neoclassical style, most likely by Robert Mitchell, two miles west of the earlier house.<ref>Pevsner (1964), p. 76 ; Britton (1807), IX, p. 694</ref>

==Friendship with Sir Thomas Lawrence==
The family's connection with the art connoisseur John Julius Angerstein led to them becoming acquainted with certain artistic circles in the late eighteenth century; most notably, they established a close friendship with the portrait painter [[Sir Thomas Lawrence]], who would go on to be [[President of the Royal Academy]]. He visited Willingham House, the Boucherett family's country home, composing a number of studies in pastel of Boucherett's young children, beginning in 1793.<ref>{{cite web |url = http://www.christies.com/lotfinder/paintings/sir-thomas-lawrence-pra-head-studies-of-5639298-details.aspx |title = Sir Thomas Lawrence, P.R.A. (Bristol 1769–1830 London), Head Studies of Emilia Mary, Juliana and Maria Boucherett (Sale 5964, Lot 46) | publisher = Christie's |accessdate= 28 April 2014 }}</ref> The Boucherett family were also painted several times by Lawrence; he painted a group portrait of Emilia Boucherett, and her two eldest children, Ayscoghe and Emilia Maria, alongside Mrs Boucherett's half-sister, Juliana Angerstein;<ref>Russel, F. (1992). ''Christie's Review of the Season, 1992'', p. 49 ; see also for digital image of painting: {{cite web |url = http://revivalheritage.blogspot.co.uk/2012/12/willingham-house-market-rasen.html |title = Willingham House Lincolnshire |date = 12 December 2012 |accessdate = 28 April 2014}}</ref> another group portrait, ''The Children of Ayscoghe Boucherett'', depicting the four children of Boucherett and his wife, (painted in 1808 and now held in the [[Louvre Museum]]), has been described as "one of Lawrence's most celebrated group portraits".<ref>{{cite web |url = http://www.christies.com/lotfinder/paintings/sir-thomas-lawrence-pra-head-studies-of-5639298-details.aspx |title = Sir Thomas Lawrence, P.R.A. (Bristol 1769–1830 London), Head Studies of Emilia Mary, Juliana and Maria Boucherett (Sale 5964, Lot 46) | publisher = Christie's |accessdate= 28 April 2014 }}</ref><ref>[[Musée du Louvre]]. [http://musee.louvre.fr/bases/doutremanche/notice.php?lng=1&idOeuvre=1754&f=2100 Portrait of the children of Ayscoghe Boucherett, Work 1754]. Retrieved 3 May 2014.</ref> A separate study of the three daughters, Emilia Mary, Maria and Juliana, was sold by the auction house [[Christie's]] in 2012 for £121,000.<ref>{{cite web |url = http://www.christies.com/lotfinder/paintings/sir-thomas-lawrence-pra-head-studies-of-5639298-details.aspx |title = Sir Thomas Lawrence, P.R.A. (Bristol 1769–1830 London), Head Studies of Emilia Mary, Juliana and Maria Boucherett (Sale 5964, Lot 46) | publisher = Christie's |accessdate= 28 April 2014 }}</ref>

==Member of Parliament and public service==
[[File:Willingham House, Lincolnshire.jpg|thumb|260px|Willingham House, Lincolnshire]]
In the 1790s, Boucherett began to rise through Lincolnshire's civic and mercantile circles, especially in the north of the County, where the family had their seat; his connexions helped him to become involved with a group of businessmen planning to reopen and expand Grimsby's harbour. His rise is charted in his appointments to several civic posts: to be High Steward of Grimsby from 1794 (he remained as such until his death), succeeding Christopher Clayton, and to be High Sheriff of Lincolnshire for 1795–6.<ref>Oliver 1825, p. 123 for list of High Stewards ; Stokes 1986 ; ''Gentleman's Magazine''. 1815, pt. 2, p. 371</ref> As these plans for the harbour came to fruition, he was appointed chairman of the company tasked with performing the required work; it was the Grimsby Haven Company, which was created by an Act of Parliament in 1796 (36 Geo III, chap. 98) for the express purpose of building the dock and repairing the Haven.<ref>Stokes 1986 ; ''Gentleman's Magazine''. 1815, pt. 2, p. 371 ; Bates 1893, p. 8</ref> He secured the friendship and patronage of [[Charles Anderson-Pelham, 1st Baron Yarborough|Charles Anderson Pelham, 1st Baron Yarborough]], a prominent local land-owner involved in the Haven Company, and was returned as the Member of Parliament for Great Grimsby in 1796 owing to this friendship.<ref name="Stokes 1986"/>

Yarborough, Boucherett's patron, was an opponent of the administration of [[William Pitt the Younger]] and supported the [[William Cavendish-Bentinck, 3rd Duke of Portland|Duke of Portland]] during the 1790s.<ref>Thorne 1986a</ref> Boucherett was an infrequent voter, but he told the diarist [[Joseph Farington]] that, when he did vote, he tended to do so with [[Charles James Fox]], rather than [[William Pitt the Younger|Pitt the Younger]], although, he later became "disgusted" at Fox's style of opposition;<ref>Farington 1807 (1924 print), pp. 154–5</ref> he is also known to have voted against the [[Ferrol Expedition (1800)|Ferrol Expedition]] in 1801.<ref name="Stokes 1986"/> The seat did allow for him to further the interests of the Haven Company, with him proposing a bill to grant more funds in 1799.<ref>Stokes 1986 ; ''Journals of the House of Commons'', lvi (1799), p. 531</ref> He found, though, that his funds and the financial success of the company were both in decline by 1801.<ref name=":0" /> Although re-elected in 1802, the following year he resigned in favour of Lord Yarborough's eldest son and heir, ''the Hon''. [[Charles Anderson-Pelham, 1st Earl of Yarborough|Charles Anderson Pelham]], later 1st Earl and 2nd Baron Yarborough.<ref name=":0">Stokes 1986 ; he accepted the Steward of the Manor of East Hendred in 1803 (<span>J. Venn and J.A. Venn, [http://venn.lib.cam.ac.uk/cgi-bin/search-130418.pl?sur=boucherett&suro=w&fir=&firo=c&cit=&cito=c&c=all&tex=&sye=&eye=&col=all&maxcount=50 "Boucherett, Ayscoghe", ]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</span>''[http://venn.lib.cam.ac.uk/cgi-bin/search-130418.pl?sur=boucherett&suro=w&fir=&firo=c&cit=&cito=c&c=all&tex=&sye=&eye=&col=all&maxcount=50 Alumni Cantabrigienses]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}'').</ref>

==Later life==

Although opened in 1800 at the cost of £100,000,<ref>Bates 1893, p. 8</ref> the new harbour at Grimsby failed to attract the levels of trade the company had projected, caused largely by a lack of inland transport networks.<ref>Clarke 2000, p. 717</ref> The financial problems facing the Grimsby Haven Company led to factions and difficulty in its running and put strain on Boucherett's relations with Yarborough; having invested much money and time in the now failing company, Boucherett also found it draining of him financially.<ref name="Stokes 1986"/>

Aside from Parliamentary and business commitments, he served as a Deputy Lieutenant for Lincolnshire and a Justice of the Peace.<ref>''Gentleman's Magazine''. 1815, pt. 2, p. 371</ref> Boucherett was also an officer in the Yeoman volunteers, being a Captain the Market Raisin [[Yeomanry]] in 1798 and then a Lieutenant-Colonel in and Commandant of the North Lincolnshire Yeomanry from 1814 until his death. He died in a carriage accident on 15 September 1815.<ref>Stokes 1986 ; ''Gentleman's Magazine''. 1815, pt. 2, p. 371</ref> At his death, his assets barely covered the debts he had accrued in his lifetime.<ref name=":0" /> He was succeeded as the High Steward of Grimsby by ''the Hon''. [[George Anderson Pelham]], the second son of the first Lord Yarborough.<ref>Thorne 1986b ; Oliver 1825, p. 123</ref>

==References==

===Notes===
{{reflist|group="note"}}

===Citations===
{{reflist|20em}}

===Bibliography===
* Bates, A. (1893). [https://books.google.com/books?id=DivWdMBWbbMC ''A Gossip about Old Grimsby'']
* Britton, J. (1807). [https://books.google.com/books?id=-YtCAAAAYAAJ&dq=Willingham+House+Lincolnshire ''The Beauties of England and Wales; or Original Delineations, Topographical, Historical, and Descriptive of Each County'']
* Burke, J., Burke, J.B. (1847). [https://books.google.com/books/about/Burke_s_Genealogical_and_Heraldic_Histor.html?id=YdIKAAAAYAAJ ''A Genealogical and Heraldic Dictionary of the Landed Gentry''], 1.
* Burke, J., Burke, J.B. (1847). [https://books.google.com/books/about/Burke_s_Genealogical_and_Heraldic_Histor.html?id=0NEKAAAAYAAJ ''A Genealogical and Heraldic Dictionary of the Landed Gentry''], 2.
* Burke, B. (1879). [https://archive.org/details/genealogicalhera01byuburk ''A Genealogical and Heraldic Dictionary of the Landed Gentry''], 1.
* Clarke, P. (2000). [https://books.google.com/books?id=8sTabkXK6XEC ''The Cambridge Urban History of Britain''], 2
* [[Joseph Farington|Farington, J.]] (1807), ''The Farington Diary''. Greig, J. (1924, [https://archive.org/details/cu31924102773961 reprint]), iv
* Howard, J.J., Crisp, F.A. (1899). [https://archive.org/details/visitationofengl07howa ''Visitation of England and Wales''], 7.
* Maddison, A.R. (1902). [http://www.mocavo.com/Lincolnshire-Pedigrees-Volume-50/498057 ''Lincolnshire Pedigrees''], 1 (Harleian Society Publications, 50).
* Oliver, G. (1825) [https://books.google.com/books?id=tW0PAAAAYAAJ ''The Monumental Antiquities of Great Grimsby'']
* Palmer, S. (2004). "[http://www.oxforddnb.com/view/article/549 Angerstein, John Julius (c.1732–1823)]", ''Oxford Dictionary of National Biography''
* Pevsner, N. (1964). ''Buildings of Lincolnshire''.
* Stokes, W. (1986). [http://www.historyofparliamentonline.org/volume/1790-1820/member/boucherett-ayscoghe-1755-1815 "Boucherett, Ayscoghe (1755–1815), of Willingham and Stallingborough, Lincs."], ''The History of Parliament: the House of Commons 1790–1820'' (Thorne, R., ed.).
* Thorne, R.G. (1986a). [http://www.historyofparliamentonline.org/volume/1790-1820/member/anderson-pelham-charles-1749-1823 "Anderson Pelham, Charles (1749–1823) of Brocklesby, Lincs."], ''The History of Parliament: the House of Commons 1790–1820'' (Thorne, R., ed.)
* Thorne, R.G. (1986b). [http://www.historyofparliamentonline.org/volume/1790-1820/member/anderson-pelham-hon-george-1785-1835 "Anderson Pelham, Hon. George (1785–1835)"], ''The History of Parliament: the House of Commons 1790–1820'' (Thorne, R., ed.).
* Walker, L. (2004). [http://www.oxforddnb.com/templates/article.jsp?articleid=31982&back= "Boucherett, (Emilia) Jessie (1825–1905)"], ''Oxford Dictionary of National Biography''.
* Wolff, J. (2004). [http://www.oxforddnb.com/templates/article.jsp?articleid=20000&back= "Newdegate, Charles Newdigate (1816–1887)"], ''Oxford Dictionary of National Biography''.
* [https://books.google.com/books?id=hhVDAAAAcAAJ ''Journals of the House of Commons''], lvi (1799)
* [https://books.google.com/books?id=7voRAAAAYAAJ ''The Gentleman's Magazine''] (1815), part 2.
* ''[https://books.google.com/books?id=nPI_AQAAIAAJ Export of Works of Art 1991–92]'' (1995)

{{s-start}}
{{s-par|gb}}
{{succession box
 | title  = Member of Parliament for [[Great Grimsby (UK Parliament constituency)|Great Grimsby]]
 | years  = [[British general election, 1796|1796]]–1803
 | with   = [[William Mellish]] 1796–1802
 | with2  = [[John Henry Loft]] 1802–1803
 | before = [[John Harrison (Member of Parliament)|John Harrison]] <br/> [[Dudley Long North|Dudley North]]
 | after  = Hon. [[Charles Anderson-Pelham, 1st Earl of Yarborough|Charles Anderson Pelham]] <br/> [[John Henry Loft]]
}}
{{s-other}}
{{s-bef|before=[[Sir Joseph Banks, 1st Baronet|Sir Joseph Banks, Bt.]]}}
{{s-ttl|title=[[High Sheriff of Lincolnshire]]|years=1795–1796}}
{{s-aft|after=[[Sir William Earle Welby, 1st Baronet|Sir William Earle Welby]], Bt.}}
{{s-bef|before=Christopher Clayton}}
{{s-ttl|title=High Steward of [[Grimsby]]|years=1794–1812}}
{{s-aft|after=Hon. [[George Anderson Pelham]]}}
{{s-end}}

{{DEFAULTSORT:Boucherett, Ayscoghe}}
[[Category:1755 births]]
[[Category:1815 deaths]]
[[Category:People from Lincolnshire]]
[[Category:Members of the Parliament of Great Britain for English constituencies]]
[[Category:British MPs 1796–1800]]
[[Category:Members of the Parliament of the United Kingdom for English constituencies]]
[[Category:UK MPs 1801–02]]
[[Category:UK MPs 1802–06]]
[[Category:Deputy Lieutenants of Lincolnshire]]
[[Category:Members of Parliament for Grimsby]]