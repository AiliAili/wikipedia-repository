{{good article}}
{{infobox military conflict
| conflict    = First Battle of Ciudad Juárez
| partof      = the [[Mexican Revolution]], [[Border War (1910-1918)|Border War]]
| image       = [[File:Revolutionists entering Juarez.jpg|300px|alt=Black-and-white photograph of a group of men on horseback on a city street. Lead horsemen is carrying a flag.]]
| caption     = Revolutionaries entering Ciudad Juárez.
| date        = April 7 – May 10, 1911
| place       = [[Ciudad Juárez]], [[Chihuahua (state)|Chihuahua]]
| coordinates =
| map_type    =
| latitude    =
| longitude   =
| map_size    =
| map_caption =
| map_label   =
| territory   =
| result      = Maderista victory, rebels capture Ciudad Juárez.
| status      =
| combatant1  = [[Francisco Madero|Maderistas]]
| combatant2  = {{flag|Mexico|1893}}
| combatant3  =
| commander1  = [[Francisco Madero]]<br/>[[Pancho Villa]]<br/>[[Pascual Orozco]]
| commander2  = [[Juan N. Navarro]]
| commander3  =
| strength1   = 2,500
| strength2   = 700
| strength3   =
| casualties1 =
| casualties2 =
| casualties3 =
| notes       =
}}
{{Campaignbox Mexican Revolution}}
{{Campaignbox Battles of the Mexican Revolution involving the United States}}

The '''First Battle of Ciudad Juárez''' took place in April and May 1911 between federal forces loyal to President [[Porfirio Díaz]] and rebel forces of [[Francisco Madero]], during the [[Mexican Revolution]]. [[Pascual Orozco]] and [[Pancho Villa]] commanded Madero's army, which besieged [[Ciudad Juárez]], [[Chihuahua (state)|Chihuahua]]. After two days of fighting the city's garrison surrendered and Orozco and Villa took control of the town. The fall of Ciudad Juárez to Madero, combined with [[Emiliano Zapata]]'s [[Battle of Cuautla (1911)|taking of Cuautla]] in [[Morelos]], convinced Díaz that he could not hope to defeat the rebels. As a result, he agreed to the [[Treaty of Ciudad Juárez]], resigned and went into exile in France, thus ending the initial stage of the Mexican Revolution.<ref name=VnZ98>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA98 pg. 98]</ref>

==Background==
{{Details|Mexican Revolution}}
Diaz's 34-year dictatorial rule met with much opposition, which finally coalesced around the leadership of Francisco Madero. Madero had escaped from prison and, while in [[exile]] in the US in November 1910, called for an uprising against Díaz. In the northern state of [[Chihuahua (state)|Chihuahua]] the call was answered by trader, miner and arms smuggler [[Pascual Orozco]] and the "social bandit" [[Pancho Villa]], both of whom began guerrilla operations against Díaz's troops.<ref name=VnZ63>McLynn, pg. 63</ref>

[[File:Madero at the head of his forces (LOC) crop.jpg|thumb|right|alt=A black-and-white photo of a man, Francisco Madero, on horseback. Other men stand in background.|Madero at the head of his forces in 1910.]]

Encouraged by Villa's and Orozco's actions, as well as the outbreak of the ''Zapatista'' rebellion in Morelos, Madero crossed back into Mexico in February 1911. After a few minor engagements with units of Diaz's army, Madero, Orozco and Villa decided to attack the federal garrison at Ciudad Juárez. If they could take the city, they would control traffic between Mexico and the US. Furthermore, such a major success by the revolutionaries could very well be the final push that would knock over the ''Porfiriato''.<ref name=VnZ85>McLynn, pg. 85</ref>

At the same time the attack on the city would mark the first time that the revolutionary army would confront federal forces in a regular battle rather than relying on guerrilla tactics.<ref name=Katz103>Katz, pg. 103</ref> While the fusion of Madero's, Villa's and Orozco's men did result in a number of troops sufficient to form a force capable of fighting professional soldiers in pitched encounters, the federal troops still had the advantage in terms of training and discipline. The government, in fact, had a good chance of squashing the rebels by moving troops down from the state capital of [[Chihuahua, Chihuahua|Ciudad Chihuahua]] and capturing Madero in a [[pincer movement]]. However, the Díaz-appointed governor of the state, [[Miguel Ahumada (politician)|Miguel Ahumada]],<ref name=katz86>Katz, pg. 86</ref> was worried that if the soldiers abandoned the capital unrest would break out and the insurrection would spread. As a further sign of the apparent demoralization of federal rank-and-file soldiers (many of whom had been forcibly [[conscription|conscripted]] and actually sympathized with the revolutionaries), the troops in Ciudad Chihuahua decided to stay put.<ref name="Katz110">Katz, pg. 104</ref>

==The battle==
[[File:Mexican rebel camp.jpg|thumb|right|alt= A group of 14 armed men, eight of them crouching down, in sombreros and holding rifles|Revolutionary camp outside Ciudad Juárez in 1911.]]

===Run-up engagements===
Madero sent some of his forces to make a diversionary attack on Agua Prieta, which proved successful; the resulting [[First Battle of Agua Prieta]] was significant in that it was the first time railroads were used by the rebels to gain surprise and that US forces were involved in the fighting. The town was recaptured by federal troops two weeks later once additional reinforcements arrived.<ref name=VnZ85/>

The main rebel offensive occurred on April 7, when Madero led 1,500 men to Ciudad Juárez, preceded by Villa and Orozco with 500 men each. Along the way the insurrectionists captured [[Temósachi (municipality)|Temosachi]] and [[Bauche]], which greatly raised their morale.<ref name="Katz110">Katz, pg. 104</ref>

Ciudad Juárez, defended by 700 troops, was surrounded on three sides, with the only possible exit route for the besieged ''federales'' being the northern path into [[El Paso]] and the US.<ref name=VnZ86>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA86 pg. 86]</ref> Despite the fact that the revolutionaries had cut off the water supplies into town and the garrison was low on ammunition, its commander, Gen. [[Juan N. Navarro]], refused to surrender, convinced that the inexperience of the rebels in laying sieges would allow him to hold out.<ref name=VnZ87/>

===Villa and Orozco disobey Madero===
At this point Díaz tried to come to terms with Madero, particularly after the capture of Cuautla by the Zapatistas in south-central Mexico. Together with his [[finance minister]] and main advisor, [[José Yves Limantour]], Díaz planned to make concessions to the fairly moderate Madero and, once Madero was pacified, crack down hard on the more radical elements among the rebels.<ref name=VnZ87>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA87 pg. 87]</ref> A truce was agreed to on April 23.<ref name=wola147>De La Pedraja Tomán, pg. 147</ref>

While Madero was ready to negotiate with Díaz and hold off from attacking, Villa and Orozco would have none of it. On May 8<ref name=wola150>De La Pedraja Tomán, pg. 150</ref> the two generals launched an assault on the city without consulting Madero and blamed it on a "spontaneous" outbreak in fighting.<ref name=VnZ95/> The incident that served as an excuse for the offensive occurred when a federal officer insulted a female rebel on a bridge between Ciudad Juárez and El Paso.<ref name=wola150/> Madero attempted to halt the violence but Orozco and Villa pressed on. Both of them went to great lengths to avoid Madero so they wouldn't have to disobey a direct order. Even when Villa was eventually confronted by [[Castulo Herrera]] with explicit instructions to stop fighting, he simply ignored him. Likewise, when Madero finally managed to see Orozco in person, he was simply told that the battle was already raging and it was too late to stop it.<ref name=VnZ95>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA95 pg. 95]</ref>

[[File:The attack on Juarez (LOC) crop.jpg|thumb|left|alt= A man aims a rifle from around the corner of a bullet-scarred wall. In the background another man is attempting to take cover behind a low wall and partial door.|Street fighting in Ciudad Juárez]]

Navarro, for his part, pressured by Madero, tried to keep the ceasefire for as long as he could. This proved to be quite costly to the defenders. The rebel forces captured the outer defenses of the city unopposed, as no federal officer was willing to countermand the orders to hold fire. Instead, the troops withdrew deeper into the city.<ref name=wola151>De La Pedraja Tomán, pg. 151</ref>

Orozco attacked in the north and Villa in the south. Both of them led their troops parallel to the US border so that neither their shots, nor those of the town's garrison were likely to cross on to the American side.<ref name=Katz110>Katz, pg. 104</ref> In fact, several thousand American civilians had gathered in El Paso in order to watch the struggle as spectators.<ref name=Katz110/>

===Unconventional attack===
[[File:Giussepe Garibaldi 2.jpg|thumb|right|160px|[[Giuseppe Garibaldi II]], in Mexico in 1911, fought at the battle on the side of the revolutionaries.]]

The rebels took control of the bridges connecting the city to the US, cut off electricity and telegraph, captured the [[Bullfighting|bullring]] and reached the outskirts of the city center (where the second line of defenses had been constructed) on the first day of fighting. Navarro, influenced by the advice of the impetuous Col. Tamborrel, turned down an offer of safe passage made by the rebels.<ref name=wola150/>

The town was well defended and fortified, as Navarro had prepared a [[defense in depth]] with several [[concentric]] rings of trenches, barricades and fortifications. However, unknown to the defenders, the insurrectionists had with them significant supplies of dynamite and foreign experts from other guerrilla wars, including [[Boer]] Gen. [[Ben Viljoen]] and the grandson of the famous [[Italians|Italian]] revolutionary [[Giuseppe Garibaldi II]] (news dispatches from the battle also mention the participation of the future star of many [[Hollywood]] [[Western (genre)|Westerns]], [[Tom Mix]]).<ref name=VnZ95/> In order to counteract the formidable defenses, the rebels developed an effective strategy that managed to circumvent Navarro's well-placed machine-gun nests and street barricades. Rather than attacking through the streets, the rebels used the dynamite to blow the walls of the adobe houses that were huddled up next to each other, which allowed them to proceed through the city house by house.<ref name=Katz110/><ref name=VnZ95/>

Another novel aspect of the offensive used by the attackers, which was extensively commented upon by American journalist [[Timothy Turner (journalist)|Timothy Turner]] who was observing the battle from El Paso, was the rotation of troops that the rebels employed. Rather than attacking en masse with the whole army, Villa and Orozco had their soldiers engage the enemy for a few hours, then go back to secure positions to sleep, while other rebels took their place. As a consequence the rebel troops were always rested while the less numerous defending federal troops were forced to remain sleepless and ever vigilant.<ref name=Katz110/>

The city's defenders ran out of water on May 8 (according to a report later submitted by Madero himself) and were confined to only a few buildings in the center of the city. Most of the fighting at this point was at close quarters; as a result, the advantage in machine guns and artillery that the federals enjoyed earlier was no longer a factor. According to some accounts, Gen. Navarro's troops were on the verge of mutiny and he was worried that they would turn on his officers. To forestall that possibility, Navarro began negotiations with the rebel forces.<ref name=wola152>De La Pedraja Tomán, pg. 152</ref> He surrendered the town two days later, at 2:30&nbsp;p.m. on May 10, 1911.<ref name=Katz111>Katz, pg. 111</ref>

==Aftermath==
[[File:Toma de Juarez.jpg|thumb|right|alt=A black-and-white photograph of a group of men standing and sitting in front of a war damaged building. The caption at the bottom lists the names of those present|Official photograph of the victors of the Battle of Ciudad Juárez. [[Francisco Madero|Madero]] is seated in the center, [[Pascual Orozco|Orozco]] on the far right and [[Pancho Villa|Villa]] is standing on the far left.]]
After capturing the town, Madero and his revolutionary movement achieved a new kind of credibility, both within Mexico and in the US. Some American journalists crossed the border in order to congratulate Madero personally, and reported back to American readers on the quick and efficient way that order within the city had been reestablished.<ref name=VnZ98/>

===Fall of Díaz===
{{Details|Treaty of Ciudad Juárez}}
The outcome of the battle changed the perception of the rebel movement in Mexico. Before Madero's victory many believed that the rebel forces would scatter as soon as they were confronted by federal troops. The fall of Juárez proved that notion wrong and revealed the real strength of the rebel forces.<ref name=wola>De La Pedraja Tomán, pg. 154</ref>

The immediate effect of the rebels' success helped convince Porfirio Díaz to agree to the revolutionaries' demand for his resignation. Prompted by Limantour, two days after the end of the battle Díaz signed the Treaty of Ciudad Juárez with Madero, and ten days after the battle he resigned and went into exile in France.<ref name="VnZ98"/><ref name=Katz116>Katz, pg. 116</ref> This ended the first stage of the Mexican Revolution, and at the same time made it clear that even well-garrisoned troops were not invulnerable to guerrilla armies.<ref name=VnZ100>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA100 pg. 100]</ref>

[[File:Pascual Orozco3.png|upright|thumb|left|alt=A black-and-white photograph of a man in a cowboy hat, a cartridge belt around his waist and a rifle by his foot, looking straight at the camera|[[Pascual Orozco]], one of the victorious rebel generals at Ciudad Juárez. Orozco felt that he should be the head of the revolution.]]

===Dissension among the victors===
A more indirect consequence arose from the disagreements that occurred between Madero and his generals Villa and Orozco. The tensions began during the battle and continued afterward. In addition to the insubordination that the two leaders showed Madero while the fighting was still ongoing, they also clashed with him afterwards over payments to their soldiers and over the appointment of [[Venustiano Carranza]] as Madero's [[defence minister]]. Carranza was a civilian who had joined the revolution at the last minute, and for these reasons was despised by Orozco.<ref name=Katz111/>

However, the immediate cause for the outbreak of the conflict among the rebels was the fate of 
federal Gen. Juan N. Navarro who had defended Ciudad Juárez. Both Villa and Orozco wanted him executed for his earlier killing of rebel prisoners taken by his forces.<ref name=wola153>De La Pedraja Tomán, pg. 153</ref> Madero opposed it, despite the fact that a court-martial of federal officers who committed war crimes was one of the main points of [[Plan of San Luis Potosí|his manifesto]]. Orozco and Villa went to confront Madero, a tense situation arose and (according to Villa and other witnesses) Orozco drew his pistol on Madero.<ref name=wola153/><ref name=Katz112/> Eventually Madero escaped the predicament by appealing directly to Villa's and Orozco's troops and the two generals had to relent. Madero escorted Navarro personally across the [[US-Mexico border|US border]] to El Paso.<ref name=wola153/> Likewise, Carranza kept his post while Orozco in particular nursed a grudge. However, Madero did withdraw gold from the local bank and distributed it to the troops in accordance with the generals' wishes.<ref name=Katz112>Katz, pg. 112</ref><ref name=Katz113>Katz, 113</ref>

The motivations of the three in the infighting are not completely clear. According to [[Friedrich Katz]], Madero, in refusing to let Navarro be killed, was motivated not only by his personal humanism but also by a need to assert his civilian authority over military leaders. It also seems that Villa was mostly concerned with taking care of his soldiers and his personal hatred for Navarro, since it was his soldiers—known as ''Villistas''—who Navarro had earlier ordered to be [[bayonet]]ed. Years later, after he fell out with Orozco, Villa wrote in his memoirs that the whole episode was a set-up by Orozco, who had been paid 50,000 pesos by Díaz's agents to get him to kill Madero. While Katz and other scholars consider this assessment extreme, it is possible that Orozco, who believed he should be the rightful leader of the revolution, was trying to provoke Villa into killing Madero. That way Madero would be out of the way, Villa would be discredited among the intellectual revolutionary circles as a common bandit and Orozco could claim the mantle of leadership. In the end, however, Villa kept his cool, Madero convinced common soldiers to support him and Orozco did not have the nerve to go through with it.<ref name=VnZ98/><ref name=Katz1124>Katz, pg. 112-114</ref>

===Later developments resulting from the battle===
Orozco remained unsatisfied with the political positions he was offered in the Madero government. He became even more angry when Madero asked him to fight Zapata in central Mexico. As a result, in March 1912--less than a year after the battle of Ciudad Juárez--Orozco formally declared himself in rebellion against Madero.<ref name=VnZ131>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA131 pg. 131]</ref> He subsequently supported [[Victoriano Huerta]] in his ''coup d'état'' against Madero. After Huerta's fall Orozco went into exile in the US and was eventually killed by [[Texas Ranger Division|Texas Rangers]] while trying to make his way back into Mexico to start another revolt.<ref name=VnZ309>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA309 pg. 309]</ref>

Villa remained loyal to Madero (and even fought for him against Orozco)<ref name=VnZ135>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA135 pg. 135]</ref> and contributed greatly to the fall of Huerta.<ref name=VnZ1702>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA170 pgs. 170-172]</ref> However, he broke with Carranza<ref name=VnZ2313>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA231 pgs. 231-233]</ref> and lost the struggle for the control of the revolution to Carranza's Gen. [[Álvaro Obregón]].<ref name=VnZ29801>McLynn, [https://books.google.com/books?id=aI7bZBKwsh4C&pg=PA298 pgs. 298-301]</ref>

==References==
;Notes
{{Reflist|colwidth=20em}}

;Bibliography
*[[Frank McLynn]], ''Villa and Zapata'', Basic Books, 2000. ISBN 0-7867-1088-8. [https://books.google.com/books?id=aI7bZBKwsh4C&printsec=frontcover&dq=Villa+and+Zapata&cd=1#v=onepage&q&f=false].
*[[Friedrich Katz]], ''The Life and Times of Pancho Villa'', Stanford University Press, 1998. ISBN 0-8047-3046-6. [https://books.google.com/books?id=XAIcq6AJ3OwC&printsec=frontcover&dq=THE+LIFE+AND+TIMES+OF+PANCHO+VILLA&cd=1#v=onepage&q&f=false].
*René De La Pedraja Tomán, ''Wars of Latin America, 1899-1941'', McFarland, 2006. ISBN 0-7864-2579-2. [https://books.google.com/books?id=RhNzcCNIoeIC&pg=PA154&dq=Battle+of+Ciudad+Juarez&cd=1#v=onepage&q=Battle%20of%20Ciudad%20Juarez&f=false]
* {{cite book | first = Raymond | last = Caballero | title = Lynching Pascual Orozco, Mexican Revolutionary Hero and Paradox| publisher = Create Space | year = 2015 | isbn = 978-1514382509 }}

<!--spacing-->

{{MexicanRevolution}}

{{coord missing|Chihuahua}}

{{DEFAULTSORT:Battle Of Ciudad Juarez}}
[[Category:Battles of the Mexican Revolution|Ciudad Juarez]]
[[Category:1911 in Mexico]]
[[Category:1911 in Texas]]
[[Category:Ciudad Juárez]]
[[Category:Battles of the Mexican Revolution involving the United States|Ciudad Juarez]]