{{Featured article}}

{{Infobox coin
| Country             = United States
| Denomination        = Susan B. Anthony
| Value               = 1

| Unit                = [[United States dollar|U.S. dollar]]
| Mass                = 8.1
| Mass_troy_oz        = 0.260
| Diameter            = 26.5
| Diameter_inch       = 1.04
| Edge                = Reeded
| Composition         = .75 copper, .25 nickel, clad to pure copper core.
| Years of Minting    = 1979–1981, 1999
| Mint marks          = P ([[Philadelphia Mint]])<br>D ([[Denver Mint]])<br>S ([[San Francisco Mint]])
| Obverse             = {{Css Image Crop|Image = 1981-S SBA$ Type Two Deep Cameo.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 0|Location = center}}
| Obverse caption     = The [[Obverse and reverse|obverse]] of a [[Proof coinage|proof]] Susan B. Anthony dollar
| Obverse Design      = Right-facing profile of [[Susan B. Anthony]]
| Obverse Designer    = [[Frank Gasparro]]
| Obverse Design Date = 1979
| Reverse             = {{Css Image Crop|Image = 1981-S SBA$ Type Two Deep Cameo.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 235|Location = center}}
| Reverse caption     = The reverse of a proof Susan B. Anthony dollar
| Reverse Design      = An eagle clutching a laurel branch in its talons, displayed over a landscape of the Moon.
| Reverse Designer    = Frank Gasparro
| Reverse Design Date = 1971
}}

The '''Susan B. Anthony dollar''' was a [[United States]] [[dollar coin (United States)|dollar coin]] minted from 1979 to 1981, when the series was suspended due to poor public acceptance, and again in 1999. Proposed as a smaller replacement for the cumbersome [[Eisenhower dollar]], several shapes and compositions were tested, but all were opposed by the vending machine industry, a powerful lobby affecting coin legislation. Finally, a round [[planchet]] with an eleven-sided inner border was chosen for the smaller dollar.

The original design depicted an allegorical representation of [[Liberty (goddess)|Liberty]] on the obverse and a soaring eagle on the reverse but organizations and individuals in Congress called for the coin to depict a real woman. Several proposals were submitted, and social reformer [[Susan B. Anthony]] was selected as the design subject. The [[Obverse and reverse|reverse]] design of the Eisenhower dollar was kept. Both sides of the coin were designed by [[Frank Gasparro]], the [[Chief Engraver of the United States Mint]].

The Mint struck 500 million coins in anticipation of considerable public demand, but the Susan B. Anthony dollar was poorly received, in part because of confusion caused by its similarity in size and metallic composition to the [[Quarter (United States coin)|quarter]]. Despite its poor reception, the coins began seeing use in vending machines and mass transit systems, gradually depleting the surplus. In 1997, Congress passed a law authorizing the mintage of a new gold-colored one dollar coin depicting [[Sacagawea dollar|Sacagawea]], but production did not begin quickly enough to meet demand. To fill the gap, the Susan B. Anthony dollar was struck again in 1999; the series was retired the following year.

Special coins for sale to collectors were struck in [[Proof coinage|proof]] finish through the run of the Susan B. Anthony dollar, and some minting varieties are valuable to collectors. However, most circulation strikes remained in government stockpiles for years after minting, so many are available in uncirculated grades, and the premium over face value is minimal.

== Background ==
{{multiple image
| align = left
| direction = horizontal
| image1 = 1974S Eisenhower Obverse.jpg
| width1 = 175
| alt1 = One side of a coin, depicting the bust of a man
| image2 = 1974S Eisenhower Reverse.jpg
| width2 = 175
| alt2 = One side of a coin, depicting an eagle on the surface of the moon
| footer = The Eisenhower dollar was authorized by a bill signed into law on December 31, 1970.
}}

In the early 1960s, as the price of silver rose, [[United States Department of the Treasury|Treasury Department]] vaults were depleted of silver dollars by the public.{{sfn|Yeoman, 2008|p=28}} No silver dollars had been minted in the United States since 1935,{{sfn|Yeoman, 2008|p=222}} and a shortage developed in the [[Western United States]], especially in areas in which gambling was common. As a result, Congress voted to authorize production of 45 million new silver [[Peace dollar]]s on August 3, 1964.{{sfn|Stevenson, 1964}} However, the move drew strong condemnation from critics who believed that the issuance of the coins was influenced by special interests, and that they would be quickly removed from circulation.{{sfn|Burdette, 2005|pp=98–101}} The dollars were subsequently melted,{{sfn|Yeoman, 2008|p=221}} and the [[Coinage Act of 1965]], enacted on July 23, 1965, outlawed all production of dollar coins for a period of five years.{{sfn|Public Law 89–81}}

On May 12, 1969, the Joint Commission on the Coinage, a panel of 24 individuals organized by the 1965 Coinage Act,{{sfn|Treasury Department, 1968}} recommended resumption of dollar coin production following a study conducted by a Congressional task force.{{sfn|U.S. House of Representatives, 1969|pp=3, 40}} On October 1 and 3, 1969, a hearing before the [[United States House of Representatives|U.S. House of Representatives]] discussed the proposed legislation to authorize the coin, in a copper-nickel clad composition, with the {{convert|1.5|in|mm|adj=on}} diameter of the former silver dollars.{{sfn|U.S. House of Representatives, 1969|p=1}} A provision was added requiring the coin to depict recently deceased President [[Dwight D. Eisenhower]] on the [[Obverse and reverse|obverse]] and a design "emblematic of the symbolic eagle of [[Apollo 11]] landing on the moon" on the reverse.{{sfn|Public Law 91–607}}{{efn|Gasparro's alternative design depicted a less predatory eagle, but after details were leaked to Congress, the bird was mandated to appear on the coin as it was depicted on the Apollo 11 insignia.{{sfn|Julian, 1993|p=2851}}}} President [[Richard Nixon]] signed the bill into law on December 31, 1970.{{sfn|Public Law 91–607}} Both the obverse and reverse designs were created by [[Frank Gasparro]], the [[Chief Engraver of the United States Mint]].{{sfn|Yeoman, 2008|p=222}}

The coin, known as the [[Eisenhower dollar]], proved unpopular with the public, and very few circulated in transactions.{{sfn|Bailey, 1974|p=9}} In 1976, the [[RTI International|Research Triangle Institute]] conducted a survey of United States coinage. Among other things, they recommended the half dollar, which also saw little use, be entirely eliminated from production, and the size of the dollar be reduced.{{sfn|U.S. Senate, 1978b|p=30}} Their report read in part: <blockquote>A conveniently-sized dollar coin would significantly broaden the capabilities of consumers for cash transactions, especially with machines. Members of the automatic merchandising industry have expressed a strong interest in a smaller dollar, indicating their willingness to adapt their machinery to its use.{{sfn|U.S. Senate, 1978b|p=30}}</blockquote> Numismatic historian [[David L. Ganz]] suggested that Eisenhower, a [[Republican (United States)|Republican]], was chosen as a means of balancing [[Kennedy half dollar|the half dollar]], depicting [[Democrat (United States)|Democrat]] [[John F. Kennedy]].{{sfn|Ganz, 1977|p=205}} In a 1977 paper, he agreed with the findings of the Institute, suggesting that both coins should be eliminated; the half dollar production ceased entirely, and the dollar replaced by one of smaller diameter and with a different design.{{sfn|Ganz, 1977|p=205}} Treasury officials desired the small dollar coin as a cost-saving measure; [[Director of the United States Mint|Mint Director]] [[Stella Hackel Sims|Stella Hackel]] estimated that replacing half of the issued dollar bills with small dollars would save $19 million (${{Format price|{{Inflation|US|19,000,000|1978}}}} today) in annual production costs.{{sfn|U.S. Senate, 1978b|p=110}}{{efn|Although paper notes are less costly to print, a dollar coin is considerably more durable and requires less frequent replacement.{{sfn|Caskey & St. Laurent, 1994|pp=497–498}}}}

== Design history ==

=== Liberty design ===

The [[United States Mint|Mint]] began preparation for the reduced-diameter dollar coin in 1976. Although no legislation had yet been introduced, Treasury officials anticipated a positive reception from Congress, and the coin had near unanimous support from the Mint and the vending machine industry, an influential lobby in the area of coin design and creation.{{sfn|Ganz, 1976|p=54}} In 1977, [[United States Secretary of the Treasury|Treasury Secretary]] [[Michael Blumenthal]] publicly endorsed a smaller dollar coin and suggested that an allegorical representation of [[Liberty (goddess)|Liberty]] would be a suitable subject for the coin.{{sfn|Julian, 1993|p=2929}}

{{multiple image
| direction = horizontal
| image1 = Gasparro Liberty obverse.jpeg
| alt1 = One side of a coin design, depicting the bust of a woman representing Liberty
| image2 = Gasparro Liberty reverse.jpeg
| width2 = 200
| alt2 = One side of a coin design, depicting an eagle flying above a mountaintop
| footer = Gasparro's proposed design for the obverse and reverse of the dollar coin.
}}

Chief Engraver Gasparro was tasked with creating a design for the proposed coin. His obverse design depicted a bust of Liberty, while his reverse depicted a soaring eagle.{{sfn|Ganz, 1976|p=55}} The bust was pictured along with a pole, atop which sat a [[Phrygian cap]], a symbol used to represent freedom.{{sfn|Ganz, 1976|p=55}} Gasparro's Liberty design was based on a similar obverse that he created for a 1969 [[American Numismatic Association]] convention medal.{{sfn|Ganz, 1976|p=55}} The reverse, depicting an eagle flying above a mountain against the rising sun, was originally created by Gasparro in 1967 for a proposed commemorative half dollar.{{sfn|Ganz, 1976|pp=55–56}} Describing the reverse design, Gasparro stated that it was meant to symbolize "a new day being born".{{sfn|Ganz, 1976|p=56}}

The design was reviewed by the [[Commission of Fine Arts]], and in an April 29, 1976, letter, Commission member [[J. Carter Brown]] praised the design: <blockquote>I believe this would be a superb design for United States Coinage, rooted as it is in a great tradition, being based on the '[[Liberty Cap large cent|Liberty Cap cent]]' of 1794, following [[Augustin Dupré]]'s ''Libertas Americana'' medal commemorating [[Battles of Saratoga|Saratoga]] and [[Siege of Yorktown|Yorktown]] (1777–1781).{{sfn|U.S. House of Representatives, 1978|p=13}}</blockquote>

A bill to reduce the diameter of the dollar from {{convert|1.5|in|mm}} to {{convert|1.043|in|mm}} and the weight from 22.68 grams to 8.5 grams was introduced to the House of Representatives on May 1, 1978.{{sfn|U.S. House of Representatives, 1978|p=2}} The bill was introduced to the Senate on May 3, and the proposed weight was reduced from 8.5 grams to 8.1 grams.{{sfn|U.S. Senate, 1978a|pp=1–2}} The Mint conducted experiments involving eight-, ten-, eleven- and thirteen-sided coins,{{sfn|Ganz, 1976|p=55}} but it was decided that the dollar would be round, as costly modifications would be required to update vending machinery to accept other shapes.{{sfn|Logan, 1979a|p=46}} Instead, the bill prescribed an eleven-sided inner border, which was intended to aid identification by sight and by feel for the visually handicapped.{{sfn|U.S. House of Representatives, 1978|p=7}}

=== Selection of Susan B. Anthony ===

Treasury officials officially recommended Gasparro's design, which they referred to as a "modernized version of the classic Liberty design".{{sfn|U.S. Senate, 1978a|pp=5–6}} On May 3, 1978, Wisconsin's [[William Proxmire]] introduced legislation in the Senate which was identical to the Treasury proposal, except for mandating a design which was altered to social reformer [[Susan B. Anthony]] in place of the allegorical Liberty.{{sfn|U.S. House of Representatives, 1978|pp=36–37}} On May 15, Representatives [[Mary Rose Oakar]] and [[Patricia Schroeder]] introduced similar legislation to the House of Representatives.{{sfn|U.S. House of Representatives, 1978|p=2}} Anthony was also recommended by members of the [[National Organization for Women]], the [[Congressional Caucus for Women's Issues|Congresswomen's Caucus]], the [[National Women's Political Caucus]] and the [[League of Women Voters]].{{sfn|U.S. House of Representatives, 1978|pp=104–106}} In support of the proposed legislation, the League addressed a letter to [[Walter E. Fauntroy]], chairman of the Subcommittee on Historic Preservation and Coinage, reading in part: <blockquote>The League believes that the time has come, and is indeed long past, for the likeness of a prominent American woman to be placed on a denomination of U.S. currency. We believe strongly that the likeness should be that of an actual woman and not that of an imaginary or symbolic figure. Susan B. Anthony contributed immeasurably to the advancement of human dignity in this nation. It is entirely fitting and appropriate that her memory be honored through this measure.{{sfn|U.S. House of Representatives, 1978|p=107}}</blockquote> In addition, officials tallied suggestions sent to the Mint by the general public as to the subject of the dollar coin, and Susan B. Anthony had received the most support.{{sfn|U.S. House of Representatives, 1978|p=97}}

{{Multiple image
 | align = left
 | image1 = 1979 left-facing Susan B. Anthony design.jpeg
 | alt1 = A drawing of one side of a coin, depicting the profile of a woman
 | image2 = 1979 right-facing Susan B. Anthony design.jpeg
 | alt2 = A drawing of one side of a coin, depicting the profile of a woman
 | footer = Two drawings created by Gasparro as proposed designs for the Susan B. Anthony dollar obverse
}}

Gasparro began work on his Susan B. Anthony design in June 1978, before the legislation was authorized by Congress.{{sfn|Reiter, 1979a|p=116}} He enlisted the help of a friend in conducting research on Anthony, which he felt was necessary before creating the design.{{sfn|Reiter, 1979a|pp=116–117}} He referenced approximately six different images while creating the portrait of Anthony, but it was based largely on just two.{{sfn|Reiter, 1979a|p=117}} Gasparro created several different designs before receiving final approval. One of his portraits, depicting Anthony at age 28, was shown to Anthony's great-niece, Susan B. Anthony III, who rejected it on the grounds that it unnecessarily "prettified" her great-aunt, and she criticized another design depicting Anthony at age 84, which she believed made her appear too old.{{sfn|Martin, 2010|pp=224–225}}{{sfn|Woo, 2001}} Gasparro made several alterations with the intent to depict her at age 50, at the peak of her influence as a social reformer, but no photographs of her during that period were available.{{sfn|Reiter, 1979a|p=117}} He eventually received approval after modification, later stating his belief that he had accurately portrayed Anthony.{{sfn|Reiter, 1979a|p=117}}

Initially, Gasparro expected that Congress would authorize his soaring eagle reverse design to accompany the Susan B. Anthony obverse.{{sfn|Reiter, 1979a|p=117}} However, an amendment introduced by Utah Senator [[Jake Garn]] altered the legislation to maintain the Apollo 11 design in use on the Eisenhower dollar reverse.{{sfn|U.S. Senate, 1978a|p=2}}

The bill was approved by Congress and signed into law by President [[Jimmy Carter]] on October 10, 1978,{{sfn|Public Law 95–447}} and production of Eisenhower dollars ceased during that year.{{sfn|Yeoman, 2008|p=224}} After he signed the bill into law, Carter issued a statement, saying in part that he was confident that "this act—and the new dollar—will substantially improve our coinage system as well as cutting Government coin production costs."{{sfn|Carter, 1979|p=1737}} He went on to declare his approval of the decision to depict Anthony on the coins:<blockquote>I am particularly pleased that the new dollar coin will—for the first time in history—bear the image of a great American woman. The life of Susan B. Anthony exemplifies the ideals for which our country stands. The 'Anthony dollar' will symbolize for all American women the achievement of their unalienable right to vote. It will be a constant reminder of the continuing struggle for the equality of all Americans.{{sfn|Carter, 1979|p=1737}}</blockquote>

=== Design criticism ===

[[File:Apollo 11 insignia.png|thumb|alt=A logo depicting an eagle upon the surface of the moon|Numismatists criticized the pairing of Susan B. Anthony's portrait with a design based on the Apollo 11 insignia.]]

Gasparro regarded the Anthony design as the most important of his career. Remarking on the public perception of the coin, Gasparro related that "it's become part of a social movement. This new dollar's more than a coin; it's an issue."{{sfn|Reiter, 1979a|p=116}} The decision to use a portrait of Susan B. Anthony in place of the allegorical Liberty was met with criticism by most numismatists, who believed that the latter had greater artistic merit.{{sfn|Reiter, 1979a|p=116}} Art critic and numismatist [[Cornelius Clarkson Vermeule III|Cornelius Vermeule]] was critical of the obverse design replacement, as well as the decision to continue use of the Apollo 11 design.{{sfn|Reiter, 1979b|p=46}} Vermeule noted that although Eisenhower's administration established the [[NASA|National Aeronautics and Space Administration]], Anthony had no connection to the moon landing or the U.S. space program.{{sfn|Reiter, 1979b|p=48}} Commenting on the obverse and reverse pairing, he stated his belief that it was "a hasty marriage and a bad one".{{sfn|Reiter, 1979b|pp=48–50}} Although he believed that Gasparro's design was well executed, sculptor [[Robert Weinman]] criticized the decision to depict Anthony.{{sfn|Reiter, 1979b|p=50}} Concerned about the possibility of other groups seeking representation on the coinage in response to its passage, Weinman characterized the Susan B. Anthony dollar legislation as a "billboard or campaign button approach to a national coin".{{sfn|Reiter, 1979b|p=50}}

== Reception ==

[[File:Small dollar and quarter comparison.jpeg|thumb|left|300px|alt=Two coins being shown together as a comparison of their size|The similarity in size and material composition between the Susan B. Anthony dollar (left) and the [[Washington quarter]] (right) caused confusion in transactions.]]

The first Susan B. Anthony dollars were struck at the [[Philadelphia Mint]] on December 13, 1978.{{sfn|Stevens & Ganz, 1979|p=33}} First strikes at the [[Denver Mint|Denver]] and [[San Francisco Mint]]s followed on January 9, 1979 and January 29, respectively.{{sfn|Hackel, 1979|p=25}} Mint officials feared that the coins would be hoarded upon release, so they ordered the creation of a stockpile consisting of 500 million coins prior to the release date in July 1979.{{sfn|Julian, 1993|p=2931}} The dollars all bore a [[mint mark]] denoting their place of origin: 'P' for the Philadelphia Mint, 'D' for the Denver Mint and 'S' for the San Francisco Mint. The Anthony dollars were the first coin to bear a 'P' mint mark since the [[Jefferson nickel]]s issued during [[World War II]]; other coins struck there were left without a mintmark to note their place of origin.{{sfn|Martin, 2010|p=225}} In 1980, the 'P' mint mark was added to all other circulating coins, except the [[Penny (United States coin)|cent]], struck in Philadelphia.{{sfn|Martin, 2010|p=225}}

The Treasury Department, in cooperation with the Federal Reserve, undertook a $655,000 marketing campaign to educate bank employees and members of the public about the new coin,{{sfn|Wolenik, 1979|p=8}} and the vending industry engaged in a $100 million effort to retrofit machines to accept the coins.{{sfn|Ganz, 1979|p=44}}

Despite the marketing attempts, the coin received an overwhelmingly negative reception from the public.{{sfn|Schwager, 2014|p=60}}{{sfn|Logan, 1979b|p=42}} Less than two millimeters in diameter larger than the [[Quarter (United States coin)|quarter]] and struck in the same copper-nickel composition, the Susan B. Anthony dollar was widely confused for that denomination in transactions.{{sfn|Logan, 1979b|p=42}} Mint Director Hackel noted the difference in weight and design between the two coins and expressed her belief that the dollar would eventually find favor with the public, suggesting that the coin would become "customary to the American people in time".{{sfn|Logan, 1979b|p=42}} In the months following its release, complaints mounted and public transportation and many establishments throughout the country began refusing to accept them in payment.{{sfn|Ganz, 1979|p=44}} On July 13, 1979, California Representative [[Jerry Lewis (California politician)|Jerry Lewis]] introduced a bill to the House of Representatives with the intent to increase the size of the coin to aid identification.{{sfn|U.S. House of Representatives, 1979}} Discussing the bill, which was never passed, Lewis remarked that the Anthony dollar had come to be known derisively as the "Carter quarter", due to its size and association with the President.{{sfn|The San Bernardino County Sun, 1979|p=C-5}}

In total, 757,813,744 dollars dated 1979 were struck for circulation at the Philadelphia, Denver and San Francisco Mints.{{sfn|Yeoman, 2008|p=225}} Demand remained low through 1980, and the circulation strikes for that year totaled 89,660,708.{{sfn|Yeoman, 2008|p=225}} Due to its persistent unpopularity, production of Anthony dollars for circulation was suspended, and 9,742,000 1981 dollars were struck across all three Mints exclusively for sale to collectors; this mintage marked the end of production.{{sfn|Yeoman, 2008|p=225}} At the close of production, the Treasury encountered a dilemma: the Mint struck a large amount of dollars in anticipation of great public demand, resulting in a surplus of 520,000,000 coins in 1981.{{sfn|Reiter, 1981}} Melting the coins was impractical; the cost of manufacture was approximately 2 cents, and the 98 cents earned from [[seignorage]] was applied to the [[National debt of the United States|national debt]].{{sfn|Reiter, 1981}}{{sfn|Julian, 1993|p=2932}} Had the coins been melted, their seignorage would have been added to the debt.{{sfn|Julian, 1993|p=2932}} Accordingly, the coins were placed in government storage, to be dispensed as needed.{{sfn|Reiter, 1981}}

The coin's design did have repercussions north of the border; when Canada introduced its [[Loonie|new one-dollar coin]] in 1987, its dimensions were made similar so that vending machine specifications could be common between the two nations.<ref>{{citation |last=Lee |first=Robert |url=https://news.google.com/newspapers?id=w78yAAAAIBAJ&sjid=rO8FAAAAIBAJ&pg=5946,2073094 |title=New coin to replace dollar bill |work=Ottawa Citizen |date=1986-03-25 |accessdate=2013-04-14 |page=A1}}</ref>

== 1999 Reissue ==

[[File:Martha_Washington_dollar_pattern_obverse_NN.jpg|thumb|alt=The obverse of a coin depicting a woman dressed in eighteenth-century clothing|After passage of the United States $1 Coin Act of 1997, the Mint struck pattern coins, depicting [[Martha Washington]] and dated 1759, to test a more distinctive, gold-colored coin.]]

Despite their unpopularity in transactions, Anthony dollars saw heavy use in over 9,000 stamp-dispending machines situated in [[United States Postal Service]] buildings across the country beginning in 1993. Additionally, the coins saw use with mass transit authorities.{{sfn|Roach, 2010|p=22}} Various propositions were discussed in Congress since the last dollars were produced in 1981, but no action was taken to issue a new coin until the Treasury's Anthony dollar stores became depleted in the 1990s.{{sfn|Roach, 2010|p=22}} In February 1996, the stores totaled approximately 229,500,000, but that number was reduced to approximately 133,000,000 by the end of 1997.{{sfn|Roach, 2010|p=22}} Faced with the necessity of striking more Susan B. Anthony dollars to fill the demand, the Treasury supported legislation authorizing a new dollar coin that would not be confused with the quarter. Legislation authorizing a dollar coin in a gold-colored composition and with a plain edge was introduced to the House and Senate in 1997, where it eventually received approval with a provision calling for it to depict Native-American guide [[Sacagawea]].{{sfn|Roach, 2010|p=22}} On December 1, 1997, President [[Bill Clinton]] signed the 50 States Commemorative Coin Program Act into law. The Act, which authorized the creation of the [[50 State Quarters]] program, included a section entitled "United States $1 Coin Act of 1997".{{sfn|Public Law 105–124}} That section officially authorized what became the [[Sacagawea dollar]].{{sfn|Roach, 2010|pp=22–24}}

Following passage of the act, a series of test strikes depicting [[Martha Washington]] were carried out to test a variety of gold-colored metallic compositions.{{sfn|Gilkes, 2008|p=82}} Although the act provided for creation of the new coin, it also allowed striking of the Anthony design until production began on the gold-colored dollar.{{sfn|Public Law 105–124}} Nearing depletion of Treasury stores, on May 20, 1999, the U.S. Mint announced that production of the Susan B. Anthony dollar would resume.{{sfn|U.S. Mint, 1999}} In total, 41,368,000 Susan B. Anthony dollars were struck for circulation dated 1999.{{sfn|Yeoman, 2008|p=225}} The design was officially retired in 2000, when the Sacagawea dollar entered production.{{sfn|Yeoman, 2008|pp=225–226}}

== Collecting ==

As few Susan B. Anthony dollars circulated, many remain available in uncirculated condition and are worth little above face value.{{sfn|Krause, 1999|p=64}} However, some date and [[mint mark]] varieties are relatively valuable. The 1981 coins, having been issued only to collectors, are valued above the other circulation strikes in the series.{{sfn|Yeoman, 2008|pp=224–225}} In addition, a well-known variety of the 1979 circulation strikes on which the date appears nearer the rim commands a higher price than the regular issue.{{sfn|Krause, 1999|pp=64–65}}

All dates of the dollar also exist in [[Proof coinage|proof]] finish. The 1999 coins were sold as standalone proof strikes, rather than as part of a larger proof set, as the 1979, 1980 and 1981 issues were offered.{{sfn|Schwager, 2014|p=61}} The 1999 proof was minted exclusively at the Philadelphia Mint, and bears a 'P' mint mark, while all other proof Anthony dollars were minted at San Francisco and bear the 'S' of that Mint.{{sfn|Yeoman, 2008|p=225}} Some 1979 and 1981 proofs bear a mint mark which was applied to the coinage dies with a different punch, causing them to have a more legible appearance. They are considered scarce and are valued considerably higher than normal proofs of the series.{{sfn|Yeoman, 2008|p=225}}

== Notes ==

{{notelist}}

== References ==

{{reflist|30em}}

== Bibliography ==

{{Refbegin|30em}}
* {{cite journal
  | last = Bailey
  | first = Clement F.
  | date = September 1974
  | volume = 10
  | number = 11
  | title = Ike's Dollar — The Successful Failure
  | journal = [[COINage]]
  | location = [[Encino, California]]
  | publisher = Behn-Miller Publishers
  | pages = 8–10, 94–100
  | ref = {{sfnRef|Bailey, 1974}}
}}
* {{cite book
  | last = Burdette
  | first = Roger W.
  | year = 2005
  | title = Renaissance of American Coinage, 1916–1921
  | publisher = Seneca Mill Press
  | location = Great Falls, VA
  | isbn = 978-0-9768986-0-3
  | ref = {{sfnRef|Burdette, 2005}}
}}
* {{cite book
  | last = Carter
  | first = Jimmy
  | authorlink = Jimmy Carter
  | title = Public Papers of the Presidents of the United States
  | url = https://books.google.com/books?id=i6IPy0Yi0ycC&printsec=frontcover
  | volume = Administration of Jimmy Carter
  | date = 1979
  | publisher = [[United States Government Publishing Office|Government Printing Office]]
  | location = Washington, D.C.
  | isbn = 0-16-058934-7
  | ref = {{sfnRef|Carter, 1979}}
}}
* {{cite journal
  | last1 = Caskey
  | first1 = John P.
  | last2 = St. Laurent
  | first2 = Simon
  | date = August 1994
  | volume = 26
  | number = 3
  | title = The Susan B. Anthony Dollar and the Theory of Coin/Note Substitutions
  | journal = Journal of Money, Credit and Banking
  | jstor = 2078014
  | publisher = [[Ohio State University Press|The Ohio State University Press]]
  | location = [[Columbus, Ohio]]
  | pages = 495–510
  | ref = {{sfnRef|Caskey & St. Laurent, 1994}}
}}{{subscription}}
* {{cite journal
  | last = Ganz
  | first = David L.
  | authorlink = David L. Ganz
  | date = October 1976
  | volume = 12
  | number = 10
  | title = Our New Small-Size Dollar Coins
  | journal = COINage
  | location = Encino, California
  | publisher = Behn-Miller Publishers
  | pages = 54–58, 114–115
  | ref = {{sfnRef|Ganz, 1976}}
}}
* {{cite journal
  | last = Ganz
  | first = David L.
  | date = 1977
  | volume = 26
  | title = Toward a Revision of the Minting and Coinage Laws of the United States
  | journal = Cleveland State Law Review
  | url = http://engagedscholarship.csuohio.edu/cgi/viewcontent.cgi?article=2265&context=clevstlrev
  | publisher = [[Cleveland State University]]
  | pages = 175–257
  | ref = {{sfnRef|Ganz, 1977}}
}}
* {{cite journal
  | last = Ganz
  | first = David L.
  | date = October 1979
  | volume = 13
  | number = 10
  | title = The Selling of a Coin
  | journal = COINage
  | location = Encino, California
  | publisher = Behn-Miller Publishers
  | pages = 44–46
  | ref = {{sfnRef|Ganz, 1979}}
}}
* {{cite journal
  | last = Gilkes
  | first = Paul
  | date = June 2, 2008
  | volume = 49
  | number = 2512
  | title = Die trials, experimentals, patterns test coin metals
  | journal = [[Coin World]]
  | location = [[Sidney, Ohio]]
  | publisher = Amos Press
  | pages = 82–84
  | ref = {{sfnRef|Gilkes, 2008}}
}}
* {{cite report
  | last = Hackel
  | first = Stella B.
  | authorlink = Stella Hackel Sims
  | date = 1979
  | title = Annual Report of the Director of the Mint
  | url = https://archive.org/details/annualreportofdi197880unit
  | publisher = Government Printing Office
  | location = Washington, D.C.
  | ref = {{sfnRef|Hackel, 1979}}
}}
* {{cite book
  | last = Julian
  | first = R.W.
  | editor-last = Bowers
  | editor-first = Q. David
  | editorlink = Q. David Bowers
  | year = 1993
  | title = Silver Dollars & Trade Dollars of the United States
  | publisher = Bowers and Merena Galleries
  | location = Wolfeboro, New Hampshire
  | isbn = 0-943161-48-7
  | ref = {{sfnRef|Julian, 1993}}
}}
* {{cite journal
  | last = Krause
  | first = Barry
  | date = June 1999
  | volume = 46
  | number = 6
  | title = Anthony dollar gaining respect
  | journal = Coins
  | location = [[Iola, Wisconsin]]
  | publisher = [[Krause Publications]]
  | pages = 64–65
  | ref = {{sfnRef|Krause, 1999}}
}}
* {{cite journal
  | last = Logan
  | first = Charles
  | date = July 1979
  | volume = 13
  | number = 7
  | title = The Search for a Dollar
  | journal = COINage
  | location = Encino, California
  | publisher = Behin-Miller Publishers
  | pages = 40–46
  | ref = {{sfnRef|Logan, 1979a}}
}}
* {{cite journal
  | last = Logan
  | first = Charles
  | date = October 1979
  | volume = 13
  | number = 10
  | title = The Anthony – Who Goofed?
  | journal = COINage
  | location = Encinco, California
  | publisher = Behn-Miller Publishers
  | pages = 42, 101
  | ref = {{sfnRef|Logan, 1979b}}
}}
* {{cite journal
  | last = Martin
  | first = Erik
  | date = June 7, 2010
  | volume = 51
  | number = 2617
  | title = Anthony dollar hits wrong chord with collectors, public
  | journal = Coin World
  | location = Sidney, Ohio
  | publisher = Amos Press
  | pages = 224–226
  | ref = {{sfnRef|Martin, 2010}}
}}
* {{cite journal
  | last = Reiter
  | first = Ed
  | date = February 1979
  | volume = 26
  | number = 2
  | title = Gasparro's Greatest Challenge
  | journal = Coins
  | location = Iola, Wisconsin
  | publisher = Krause Publications
  | pages = 116–117
  | ref = {{sfnRef|Reiter, 1979a}}
}}
* {{cite journal
  | last = Reiter
  | first = Ed
  | date = April 1979
  | volume = 13
  | number = 4
  | title = Judging the Anthony Dollar
  | journal = COINage
  | location = Encino, California
  | publisher = Behn-Miller Publishers
  | pages = 46–50
  | ref = {{sfnRef|Reiter, 1979b}}
}}
* {{cite news
  | last = Reiter
  | first = Ed
  | title = Numismatics; What Will Become of Those Anthony Dollars?
  | url = https://www.nytimes.com/1981/10/11/arts/numismatics-what-will-become-of-those-anthony-dollars.html
  | newspaper = [[New York Times]]
  | date = October 11, 1981
  | ref = {{sfnRef|Reiter, 1981}}
}}
* {{cite journal
  | last = Roach
  | first = Steve
  | date = March 8, 2010
  | volume = 51
  | number = 2604
  | title = Wanted: new $1 coins (as long as they're not Anthony dollars)
  | journal = Coin World
  | location = Sidney, Ohio
  | publisher = Amos Press
  | pages = 22–24
  | ref = {{sfnRef|Roach, 2010}}
}}
* {{cite journal
  | last = Schwager
  | first = David
  | date = August 2014
  | volume = 50
  | number = 8
  | title = The Anthony Dollar 'Orphan'
  | journal = COINage
  | location = Dallas, Texas
  | publisher = Beckett Media
  | pages = 60–61
  | ref = {{sfnRef|Schwager, 2014}}
}}
* {{cite journal
  | last1 = Stevens
  | first1 = Larry
  | last2 = Ganz
  | first2 = David L.
  | date = February 1979
  | volume = 13
  | number = 2
  | title = Minting the Anthony Dollar
  | journal = COINage
  | location = Encino, California
  | publisher = Behn-Miller Publishers
  | pages = 33–35, 104
  | ref = {{sfnRef|Stevens & Ganz, 1979}}
}}
* {{cite news
  | last= Stevenson
  | first = I.K.
  | title = Date on New Silver Dollar Uncertain
  | url = https://news.google.com/newspapers?nid=A7-hzOuI2KQC&dat=19641213&printsec=frontpage&hl=en
  | newspaper = [[Sarasota Herald-Tribune]]
  | date = December 13, 1964
  | ref = {{sfnRef|Stevenson, 1964}}
}}
* {{cite news
  | author = [[The San Bernardino County Sun]]
  | title = Lewis — dump new dollar coin
  | url = https://www.newspapers.com/image/62938906/
  | newspaper = The San Bernardino Sun
  | date = July 15, 1979
  | ref = {{sfnRef|The San Bernardino County Sun, 1979}}
}}{{subscription}}
* {{cite web
  | url = http://www.gpo.gov/fdsys/pkg/STATUTE-79/pdf/STATUTE-79-Pg254.pdf
  | title = Public Law 89–81
  | author = [[United States Congress]]
  | date = July 23, 1965
  | accessdate = March 4, 2015
  | type = PDF
  | ref = {{sfnRef|Public Law 89–81}}
}}
* {{cite web
  | url = http://www.gpo.gov/fdsys/pkg/STATUTE-84/pdf/STATUTE-84-Pg1760.pdf
  | title = Public Law 91–607
  | author = United States Congress
  | date = December 31, 1970
  | accessdate = March 4, 2015
  | type = PDF
  | ref = {{sfnRef|Public Law 91–607}}
}}
* {{cite web
  | url = http://www.gpo.gov/fdsys/pkg/STATUTE-92/pdf/STATUTE-92-Pg1072.pdf
  | title = Public Law 95–447
  | author = United States Congress
  | date = October 10, 1978
  | accessdate = March 4, 2015
  | type = PDF
  | ref = {{sfnRef|Public Law 95–447}}
}}
* {{cite web
  | url = http://www.gpo.gov/fdsys/pkg/PLAW-105publ124/pdf/PLAW-105publ124.pdf
  | title = Public Law 105–124
  | author = United States Congress
  | date = December 1, 1997
  | accessdate = March 7, 2015
  | type = PDF
  | ref = {{sfnRef|Public Law 105–124}}
}}
* {{cite press release
  | author = United States Department of the Treasury
  | title = The Joint Commission on the Coinage
  | url = http://www.usmint.gov/historianscorner/?action=DocDL&doc=pr402.pdf
  | type = PDF
  | date = February 29, 1968
  | ref = {{sfnRef|Treasury Department, 1968}}
}}
* {{cite book
  | author = United States House of Representatives
  | title = The Coinage Act of 1969
  | url = https://archive.org/stream/coinageactheari01statgoog#page/n2/mode/2up
  | year = 1969
  | publisher = [[United States Government Publishing Office|Government Printing Office]]
  | location = Washington, D.C.
  | ref = {{sfnRef|U.S. House of Representatives, 1969}}
}}
* {{cite book
  | author = United States House of Representatives 
  | title = Proposed Smaller One-Dollar Coin
  | year = 1978
  | publisher = Government Printing Office
  | location = Washington, D.C.
  | ref = {{sfnRef|U.S. House of Representatives, 1978}}
}}
* {{cite book
  | author = United States House of Representatives
  | title = H.R. 4783
  | year = 1970
  | publisher = Government Printing Office
  | location = Washington, D.C.
  | ref = {{sfnRef|U.S. House of Representatives, 1979}}
}}
* {{cite press release
  | author = United States Mint
  | title = U.S. Mint to Strike 1999 Susan B. Anthony Dollar Coins
  | date = May 20, 1999
  | url = http://www.usmint.gov/pressroom/?action=press_release&id=78
  | ref = {{sfnRef|U.S. Mint, 1999}}
}}
* {{cite book
  | author = United States Senate
  | title = The Susan B. Anthony Dollar Coin Act of 1978
  | year = 1978
  | publisher = Government Printing Office
  | location = Washington, D.C.
  | ref = {{sfnRef|U.S. Senate, 1978a}}
}}
* {{cite book
  | author = United States Senate
  | title = Susan B. Anthony Dollar Coin Act of 1978
  | url = http://babel.hathitrust.org/cgi/pt?id=pur1.32754066833785;view=1up;seq=3
  | year = 1978
  | publisher = Government Printing Office
  | location = Washington, D.C.
  | ref = {{sfnRef|U.S. Senate, 1978b}}
}}
* {{cite journal
  | editor-last = Wolenik
  | editor-first = Robert
  | date = August 1979
  | volume = 13
  | number = 9
  | title = First Check-Up on the Anthony Dollar
  | journal = COINage
  | location = Encino, California
  | publisher = Behn-Miller Publishers
  | page = 8
  | ref = {{sfnRef|Wolenik, 1979}}
}}
* {{cite news
  | last = Woo
  | first = Elaine
  | title = Frank Gasparro, 92; Chief Engraver at U.S. Mint
  | url = http://articles.latimes.com/2001/oct/03/local/me-52824
  | newspaper = [[Los Angeles Times]]
  | date = October 3, 2001
  | ref = {{sfnRef|Woo, 2001}}
}}
*{{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = R.S. Yeoman
  | editor = Kenneth Bressett
  | title = A Guide Book of United States Coins
  | edition = 62nd
  | year = 2008
  | publisher = Whitman Publishing
  | location = Atlanta, Georgia
  | isbn = 978-079482491-4
  | ref = {{sfnRef|Yeoman, 2008}}
}}
{{refend}}

== External links ==
* [http://americanhistory.si.edu/collections/object-groups/art-of-frank-gasparro-10th-united-states-mint-chief-engraver Art of Frank Gasparro: 10th United States Mint Chief Engraver] – Smithsonian Institution

{{s-start}}
{{succession box
| title  = [[Dollar coin (United States)|Dollar coin of the United States]]
| before = [[Eisenhower dollar]]
| after  = [[Sacagawea dollar]]
| years  = 1979–1981, 1999
}}
{{s-end}}

{{US currency and coinage}}
{{Coinage (United States)}}
{{Suffrage}}

[[Category:United States dollar coins]]
[[Category:1979 introductions]]
[[Category:Susan B. Anthony]]