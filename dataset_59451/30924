{{Taxobox|status=DD
|status_ref=<ref name=iucn>Percequillo et al., 2008</ref>
|status_system=iucn3.1
|regnum=[[Animal]]ia
|phylum=[[Chordate|Chordata]]
|classis=[[Mammal]]ia
|ordo=[[Rodent]]ia
|familia=[[Cricetidae]]
|genus=''[[Euryoryzomys]]''
|species='''''E. emmonsae'''''
|binomial=''Euryoryzomys emmonsae''
|binomial_authority=([[Guy Musser|Musser]], Carleton, Brothers, and Gardner, 1998)
|synonyms=
*''Oryzomys emmonsae'' <small>Musser et al., 1998</small><ref name=Mea233>Musser et al., 1998, p.&nbsp;233</ref>
*''[Euryoryzomys] emmonsae'': <small>Weksler, Percequillo, and Voss, 2006</small><ref name=Wea11/>
|range_map=Euryoryzomys emmonsae distribution.png
|range_map_alt=Map of South America showing the distribution of ''Euryoryzomys emmonsae'' in central Brazil.
|range_map_caption=Distribution of ''Euryoryzomys emmonsae'' (in green).<ref>Musser et al., 1998, fig.&nbsp;79</ref>}}
'''''Euryoryzomys emmonsae''''', also known as '''Emmons's Rice Rat'''<ref name=iucn/> or '''Emmons' Oryzomys''',<ref name=MC1148>Musser and Carleton, 2005, p.&nbsp;1148</ref> is a [[rodent]] from the [[Amazon rainforest]] of Brazil in the genus ''[[Euryoryzomys]]'' of the family [[Cricetidae]]. Initially misidentified as ''[[Euryoryzomys macconnelli|E.&nbsp;macconnelli]]'' or ''[[Euryoryzomys nitidus|E.&nbsp;nitidus]]'', it was formally described in 1998. A rainforest species, it may be [[scansorial]], climbing but also spending time on the ground. It occurs only in a limited area south of the [[Amazon River]] in the state of [[Pará]], a distribution that is apparently unique among the [[muroid]] rodents of the region.

''Euryoryzomys emmonsae'' is a relatively large rice rat, weighing 46 to 78&nbsp;g (1.6 to 2.8&nbsp;oz), with a distinctly long tail and relatively long, tawny brown fur. The skull is slender and the [[incisive foramen|incisive foramina]] (openings in the bone of the [[palate]]) are broad. The animal has 80 [[chromosome]]s and its [[karyotype]] is similar to that of other ''Euryoryzomys''. Its [[conservation status]] is assessed as "[[Data Deficient]]", but deforestation may pose a threat to this species.

==Taxonomy==
In 1998, [[Guy Musser]], Michael Carleton, Eric Brothers, and Alfred Gardner reviewed the [[Taxonomy (biology)|taxonomy]] of species previously lumped under "''[[Oryzomys capito]]''" (now classified in the genera ''[[Hylaeamys]]'', ''[[Euryoryzomys]]'', and ''[[Transandinomys]]''). They described the new species ''Oryzomys emmonsae'' on the basis of 17 specimens from three locations in the state of [[Pará]] in northern Brazil; these animals had been previously identified as ''Oryzomys macconnelli'' (now ''[[Euryoryzomys macconnelli]]'') and then as ''Oryzomys nitidus'' (now ''[[Euryoryzomys nitidus]]'').<ref name=Mea233/> The [[specific name (zoology)|specific name]] honors [[Louise H. Emmons]], who, among other contributions to Neotropical mammalogy, collected three of the known examples of the species in 1986, including the [[holotype]].<ref>Musser et al., 1998, pp.&nbsp;233, 239</ref> The new species was placed in what they termed the "''Oryzomys nitidus'' group", which also included ''O.&nbsp;macconelli'', ''O.&nbsp;nitidus'', and ''[[Euryoryzomys russatus|O.&nbsp;russatus]]''.<ref name=Mea175>Musser et al., 1998, p.&nbsp;175</ref>

In 2000, James Patton, [[Maria Nazareth F. da Silva|Maria da Silva]], and Jay Malcolm reported on mammals collected at the [[Rio Juruá]] in western Brazil. In this report, they provided further information on the ''Oryzomys'' species reviewed by Musser and colleagues, including [[DNA sequence|sequence]] data from the [[mitochondrial DNA|mitochondrial]] [[cytochrome b|cytochrome ''b'']] gene. Their analysis reaffirmed that ''O.&nbsp;emmonsae'' was a distinct species and found that it was closest to ''O.&nbsp;macconnelli'' and ''O.&nbsp;russatus'', differing from both by about 12% in the cytochrome ''b'' sequence; ''O.&nbsp;nitidus'' was more distantly related, differing by 14.7%. The average sequence difference between the three ''O.&nbsp;emmonsae'' studied was 0.8%.<ref>Patton et al., 2000, fig.&nbsp;99; table&nbsp;37</ref>

In 2006, an extensive [[morphology (anatomy)|morphological]] and [[molecular phylogenetics|molecular phylogenetic]] analysis by Marcelo Weksler showed that species then placed in the genus ''[[Oryzomys]]'' did not form a single, cohesive ([[monophyly|monophyletic]]) group; for example, ''O.&nbsp;macconnelli'', ''[[Euryoryzomys lamia|O.&nbsp;lamia]]'' (placed under ''O.&nbsp;russatus'' by Musser and colleagues) and ''O.&nbsp;russatus'' clustered together in a single natural group ([[clade]]), but were not closely related to the [[type species]] of ''Oryzomys'', the [[marsh rice rat]] (''O.&nbsp;palustris'').<ref>Weksler, 2006, figs.&nbsp;34–39</ref> Later in 2006, Weksler and colleagues described several new genera to accommodate species previously placed in ''Oryzomys'', among which was ''[[Euryoryzomys]]'' for the "''O.&nbsp;nitidus'' complex", including ''O.&nbsp;emmonsae''.<ref name=Wea11>Weksler et al., 2006, p.&nbsp;11</ref>

Thus, the species is now known as ''Euryoryzomys emmonsae''.<ref name=iucn/> As a species of ''Euryoryzomys'', it is classified within the [[tribe (taxonomy)|tribe]] [[Oryzomyini]] ("rice rats"), which includes over a hundred species, mainly from South and Central America.<ref>Weksler et al., 2006, table&nbsp;1; Musser and Carleton, 2005</ref> Oryzomyini in turn is part of the subfamily [[Sigmodontinae]] of family [[Cricetidae]], along with hundreds of other  species of mainly small rodents.<ref>Musser and  Carleton, 2005</ref>

==Description==
''Euryoryzomys emmonsae'' is a fairly large,<ref>Musser et al., 1998, pp.&nbsp;233–234</ref> long-tailed rice rat with long, soft fur. The hairs on the back are 8 to 10&nbsp;mm (0.3 to 0.4&nbsp;in) long.<ref name=Mea234>Musser et al., 1998, p.&nbsp;234</ref> It generally resembles ''E.&nbsp;nitidus'' in these and other characters, but has a longer tail.<ref name=Mea233/> ''E.&nbsp;macconnelli'' is slightly larger and has longer and duller fur. In ''E.&nbsp;emmonsae'', the upperparts are tawny brown, but a bit darker on the head because many hairs have black tips. The hairs of the underparts are gray at the bases and white at the tips; overall, the fur appears mostly white. In most specimens, there is a patch on the chest where the gray bases are absent. The longest of the [[vibrissa]]e (whiskers) of the face extend slightly beyond the ears. The eyelids are black. The ears are covered with small, yellowish brown hairs and appear dark brown overall. The feet are covered with white hairs above and brown below. There are six [[Paw#Common characteristics|pads]] on the [[plantar]] surface, but the [[hypothenar]] is reduced. The [[ungual tufts]], tufts of hair which surround the bases of the claws, are well-developed. The tail is like the body in color above, and mostly white below, but in the 10&nbsp;mm (0.4&nbsp;in) nearest the tail tip it is brown below.<ref name=Mea234/>

Compared to ''E.&nbsp;nitidus''<ref name=Mea233/> and ''E.&nbsp;macconnelli'', the skull is relatively small and slender. It has broad and short [[incisive foramen|incisive foramina]] (perforations of the [[palate]] between the [[incisor]]s and the [[molar (tooth)|molars]]) and lacks [[sphenopalatine vacuities]] which perforate the [[mesopterygoid fossa]], the gap behind the end of the palate. The animal is similar to other members of the genus in the pattern of the [[artery|arteries]] of the head.<ref name=Mea234/> The [[alisphenoid strut]], an extension of the [[alisphenoid]] bone which separates two [[foramen|foramina]] (openings) in the skull (the [[masticatory-buccinator foramen]] and the [[foramen ovale accessorium]]) is rarely present; its presence is more frequent in ''E.&nbsp;nitidus''.<ref name=Mea236>Musser et al., 1998, p.&nbsp;236</ref> The [[capsular process]], a raising of the bone of the [[mandible]] (lower jaw) behind the third molar, houses the back end of the lower incisor in most ''Euryoryzomys'', but is absent in ''E.&nbsp;emmonsae'' and ''E.&nbsp;macconnelli''.<ref name=Wea12>Weksler et al., 2006, p.&nbsp;12</ref> Traits of the teeth are similar to those of ''E.&nbsp;nitidus'' and other ''Euryoryzomys''.<ref>Musser et al., 1998, pp.&nbsp;234; Weksler et al., 2006, p.&nbsp;12</ref>

The [[karyotype]] includes 80 [[chromosome]]s with a total of 86 major arms (2n&nbsp;=&nbsp;80; [[karyotype#Fundamental number|FN]]&nbsp;=&nbsp;86). The [[X chromosome]] is [[centromere#Subtelocentric|subtelocentric]] (with one pair of long arms and one pair of short arms) and the [[Y chromosome]] is [[acrocentric]] (with only one pair of arms, or with a minute second pair). Among the [[autosome]]s (non-sex chromosomes), the four [[centromere#Centromere positions|metacentric]] or [[submetacentric]] (with two pairs of arms as long as or not much shorter than the other) pairs of chromosomes are small, and the 35 pairs of acrocentrics range from large to small. Some of those have a minute second pair of arms and could also be classified as subtelocentric, which would raise FN to 90. This karyotype is similar to other known karyotypes of members of ''Euryoryzomys''.<ref name=Mea234/>

In thirteen specimens measured by Musser, head and body length ranges from 120 to 142&nbsp;mm (4.7 to 5.6&nbsp;in), tail length (12 specimens only) from 130 to 160&nbsp;mm (5.1 to 6.3&nbsp;in), hindfoot length from 32 to 35&nbsp;mm (1.3 to 1.4&nbsp;in), ear length (three specimens only) from 23 to 24&nbsp;mm (0.91 to 0.94&nbsp;in), and body mass from 46 to 78&nbsp;g (1.6 to 2.8&nbsp;oz).<ref>Musser et al., 1998, table&nbsp;37</ref>

==Distribution and ecology==
The known distribution of ''Euryoryzomys emmonsae'' is limited to a portion of the Amazon Rainforest south of the [[Amazon River]] in the state of Pará, between the [[Rio Xingu|Xingu]] and [[Rio Tocantins|Tocantins]] rivers, but the limits of its range remain inadequately known.<ref>Musser et al., 1998, p.&nbsp;236; Musser and Carleton, 2005, p.&nbsp;1148</ref> No other South American rainforest [[muroid]] rodent is known to have a similar distribution.<ref>Musser et al., 1998, pp.&nbsp;237, 239</ref> Musser and colleagues reported it from three locations and Patton and others added a fourth; in some of those it occurs together with ''E.&nbsp;macconnelli'' or ''[[Hylaeamys megacephalus]]''.<ref>Musser et al., 1998, pp.&nbsp;232, 237; Patton et al., 2000, p.&nbsp;146</ref>

Specimens of ''E.&nbsp;emmonsae'' for which detailed habitat data are available were caught in "viny forest", a [[microhabitat]] that often included much bamboo. All were captured on the ground, some in bamboo thickets and another under a log.<ref name=Mea237>Musser et al., 1998, p.&nbsp;237</ref> Musser and colleagues speculated that ''E.&nbsp;emmonsae'' may be [[scansorial]], spending time both on the ground and climbing in vegetation, like the similarly long-tailed rice rat ''[[Cerradomys subflavus]]''.<ref name=Mea239>Musser et al., 1998, p.&nbsp;239</ref>

==Conservation status==
The [[IUCN]] currently lists ''Euryoryzomys emmonsae'' as "[[Data Deficient]]" because it is so poorly known. It may be threatened by deforestation and logging, but occurs in at least one [[protected area]], the [[Floresta Nacional de Tapirape-Aquiri]].<ref name=iucn/>

==References==
{{reflist|colwidth=30em}}

==Literature cited==
*Musser,  G.G., Carleton, M.D., Brothers, E.M. and Gardner, A.L. 1998.  [http://hdl.handle.net/2246/1630 Systematic studies of oryzomyine rodents (Muridae: Sigmodontinae):  diagnoses and distributions of species formerly assigned to ''Oryzomys "capito"'']. Bulletin of the American Museum of  Natural History 236:1–376.
*Musser,  G.G. and Carleton, M.D. 2005. Superfamily Muroidea. Pp.&nbsp;894–1531 in Wilson, D.E. and  Reeder, D.M. (eds.). [http://www.bucknell.edu/msw3  Mammal Species of the World: a taxonomic and  geographic reference. 3rd ed].  Baltimore: The Johns Hopkins University Press, 2&nbsp;vols.,  2142&nbsp;pp.&nbsp;ISBN 978-0-8018-8221-0
*Patton,  J.L., da Silva, M.N.F. and Malcolm, J.R. 2000. [http://hdl.handle.net/2246/1593 Mammals of the Rio Juruá  and the evolutionary and ecological diversification of Amazonia].  Bulletin of the American Museum of Natural History 244:1–306.
*Percequillo, A., Wecksler, M. and Patton, J.L. 2008. {{IUCNlink|29402|Euryoryzomys emmonsae}}. In IUCN. IUCN Red List of Threatened Species. Version 2009.2. <[https://web.archive.org/web/20140627094911/http://www.iucnredlist.org/ www.iucnredlist.org]>. Downloaded on March 14, 2010.
*Weksler,  M. 2006. [http://hdl.handle.net/2246/5777  Phylogenetic relationships of oryzomyine  rodents (Muroidea: Sigmodontinae): separate and combined analyses of  morphological and molecular data].  Bulletin of the American Museum of Natural History 296:1–149.
*{{cite journal | last1 = Weksler | first1 = M. 
  | last2 = Percequillo | first2 = A. R. | last3 = Voss | first3 = R. S.
  | title = Ten new genera of oryzomyine rodents (Cricetidae: Sigmodontinae)
  | journal = American Museum Novitates | volume = 3537 | issue = | pages = 1–29
  | publisher = [[American Museum of Natural History]] | date = 2006-10-19 | hdl = 2246/5815 | doi= 10.1206/0003-0082(2006)3537[1:TNGOOR]2.0.CO;2}}

{{Oryzomyini nav}}

{{featured article}}

[[Category:Mammals of Brazil]]
[[Category:Oryzomyini]]
[[Category:Animals described in 1998]]