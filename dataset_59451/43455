{|{{Infobox aircraft begin
|name =PZL Ł.2
|image =PZL L2.jpg
|caption =PZL Ł.2 "Afrykanka" sports aircraft
}}{{Infobox aircraft type
|type =[[Liaison aircraft]]
|manufacturer =[[PZL]]
|designer =
|first flight = [[1930 in aviation|1930]]
|introduced =
|retired =[[1935 in aviation|1935]]
|status =
|primary user = [[Polish Air Force]]
|more users =
|produced =1930-1931
|number built =31
|unit cost =
|developed from =
|variants with their own articles =
}}
|}

The '''[[PZL]] Ł.2''' was the [[Poland|Polish]] [[Army cooperation aircraft|Army cooperation]] and [[liaison aircraft]], built in [[1929 in aviation|1929]] in the [[PZL|Polskie Zakłady Lotnicze]] (PZL) in [[Warsaw]]. Only a small series of 31 aircraft, including prototype, were made, and used by the [[Polish Air Force]] in the 1930s. The aircraft was known in Poland for accomplishing of a long-distance tour around Africa in 1931.

==Development==
In [[1927 in aviation|1927]], the Polish War Ministry opened a contest for a military liaison and observation aircraft. It was meant to operate from casual airfields, used by Army land units. [[Jerzy Dąbrowski]] and Franciszek Kott from the PZL works proposed an aircraft, designated initially '''PZL.2'''.<ref name=glass>Glass, A. (1977), pp.218-219</ref> It was one of the first PZL designs, what was indicated by its low number. The first prototype was flown in early [[1930 in aviation|1930]] by [[Bolesław Orliński]] (later it received civilian registration SP-ADN).<ref name=morgala>Morgała, A. (2003). pp.187-188</ref><ref name=cynk>Cynk, J. (1977). pp.128-135</ref>

In [[1930 in aviation|1930]] the aircraft was tested and evaluated by the [[Polish Air Force]]. Thanks to wing mechanization (flats and slats), it had short take-off and landing.<ref name=glass/> It was very advanced combination of high-lift devices in world's aviation those days.<ref name=cynk/> A competing designs [[Lublin R-X]] and [[PWS-5t2]], evaluated yet in 1929, were not satisfactory, so the Polish Air Force ordered 60 PZL.2.<ref name=morgala/> The aircraft took part in the second contest for an army co-operation aircraft in July 1931. In spite of advanced high-lift devices and all-duralumin construction of the PZL.2, the air force decided to choose a simpler, cheaper and quite satisfactory [[Lublin R-XIII]] plane.<ref name=morgala/>

An initial order for 60 PZL.2 was finally lowered to 30, which were built between April 1930 and August 1931.<ref name=morgala/> The designation changed then to '''PZL Ł.2''' (Ł for "łącznikowy", liaison) or Ł.2a (following an early manner of PZL works to mark the aircraft purpose in designation, after a pursuit [[PZL P.1]]). Including the prototype, they carried factory numbers 55.1 - 55.31.<ref name=morgala465>Morgała, A. (2003). p.465</ref>

One of the Ł.2, number 55.10 was converted to a long-distance sports aircraft (civilian registration SP-AFA).<ref name=morgala/> It had fuel tanks 600 l and a range of over 2000&nbsp;km. It was also fitted with a [[Townend ring]].<ref name=glass/>

Due to a decrease of orders, there remained parts for several aircraft. In 1930 the PZL proposed to the [[Polish Navy]] a liaison and patrol [[floatplane]] variant of Ł.2, designated '''PZL.9''', but it was not built. Then, the PZL proposed another patrol and fighter floatplane, basing on Ł.2 parts, '''PZL.15'''. It was a low-wing braced monoplane with thin tail boom, and utilized wings, tail and engine of Ł.2. It was not built either.<ref name=glass/> Parts of Ł.2 (wings, tail, engine) were utilized in a passenger aircraft prototype [[PZL.16]].<ref name=glass/>

==Design==
PZL Ł.2 was a high-wing braced [[parasol wing]] [[monoplane]], conventional in layout, of all-metal construction. It had a [[duralumin]] framed, canvas covered fuselage (engine part was covered with duralumin). Crew of two was sitting in [[tandem]] in open [[cockpit]]s, with twin controls. The observer had a 7.7&nbsp;mm Lewis machine gun on a ring mounting. The elliptical wing was two-spar, of duralumin construction, canvas-covered, fitted with [[Leading edge slats|slats]], [[Flap (aircraft)|flaps]] and [[flaperon]]s. Wings could be dismounted for transport. The tail was of duralumin construction, canvas covered. It had a conventional fixed [[landing gear]] with a rear skid.<ref name=glass/>

It had a 9-cylinder air-cooled Polish [[Škoda Works|Skoda]] Works licence-built [[Wright Whirlwind J-5A]] radial engine delivering 240&nbsp;hp (179&nbsp;kW) at take-off and 220&nbsp;hp (164&nbsp;kW) nominal, driving a two-blade wooden propeller, 2.7 m diameter (in SP-AFA - metal one). 190 litre fuel tank in a fuselage (600 l in SP-AFA). Cruise fuel consumption was 45-50 l/h.<ref name=glass220/><!--at what power setting?-->

==Operational history==
In May 1930 the prototype PZL.2 was shown by Bolesław Orliński at air meeting in [[Brno]], where it impressed viewers with short landing and minimal speed. After being fitted with a rear machine gun, it was shown at [[Paris Air Show]] in December 1930.<ref name=glass/>

Serial aircraft were used by the Polish Air Force as liaison and utility aircraft from 1930, first of all in escadres Nos. 43 and 63.<ref name=morgala/> From 1932 they were mostly replaced with [[Lublin R-XIII]] and relegated for training, among others in [[Dęblin]]. Several were damaged in crashes. Since the aircraft started to suffer from fatigue of rivets in frame joints, they were completely written off by the end of [[1935 in aviation|1935]].<ref name=morgala/>
[[File:PZL L2 SP-AFA.jpg|thumb|200px|PZL Ł.2 SP-AFA]]
PZL Ł.2 SP-AFA was used for several long-distance flights. Between 1 February - 5 May 1931 [[Stanisław Skarżyński]] with A. Markiewicz flew it on a tour around Africa, on Warsaw - [[Belgrade]] - [[Athens]] - [[Cairo]] - [[Khartoum]] - [[Juba, Sudan|Juba]] - [[Kisumu]] -  [[Elisabethville]] - [[Léopoldville]] - [[Port-Gentil]] - [[Douala]] - [[Lagos]] - [[Abidjan]] - [[Bamako]] - [[Dakar]] - [[Port Etienne]] - [[Casablanca]] - [[Alicante]] - [[Bordeaux]] - [[Paris]] - [[Berlin]] - Warsaw 25,050&nbsp;km-route (with some other stops). The aircraft was nicknamed ''Afrykanka'' then (Polish: the African female), coinciding with the aircraft registration.<ref name=glass/> According to J. Cynk, it was one of the first and greatest international enterprises of the Polish aviation, and it was also one of the most outstanding flights in 1931. The tour proved a durability of the Polish-built aircraft, withstanding different weather conditions and casual airstrips, during 147 flying hours, despite the engine had to be repaired twice on the way.<ref name=cynk/> In 7–8 June 1931 Skarżyński flew this aircraft from [[Poznań]] in a rally to [[Bucharest]]. In July 1932 it hauled Polish gliders SG-21 and SG-28 in international competition in [[Rhön]] (piloted by Skarżyński again). The aircraft was written off in autumn 1935.<ref name=glass/>

==Operators==
;{{POL}}
*[[Polish Air Force]] operated 29 aircraft.<ref name=morgala465/>
*[[PZL]] company operated two aircraft.

==Specifications==
{{Aircraft specs
|ref=Polish Aircraft 1893–1939,<ref name=Cynk>{{cite book |last=Cynk |first=Jerzy B. |title=Polish Aircraft 1893–1939 |year=1971 |publisher=Putnam |location=London |isbn=978-0-370-00085-5}}</ref> ''Polskie konstrukcje lotnicze 1893-1939''<ref name=glass220>Glass, A. op.cit., p. 220.</ref>
|prime units?=met
<!--
        General characteristics
-->
|crew=2
|capacity=
|length m=7.92
|span m=13.4
|height m=2.7
|wing area sqm=25.8
|aspect ratio=<!-- sailplanes -->
|airfoil=D.J.3 (modified [[NACA airfoil|NACA M12]])
|empty weight kg=892
|gross weight kg=1282
|max takeoff weight kg=1730
|max takeoff weight note=(''Afrykanka'')
|fuel capacity={{convert|150|l|USgal impgal|abbr=on}} (''Afrykanka'' - {{convert|630|l|USgal impgal|abbr=on}})
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Skoda-Wright J-5 Whirlwind]]
|eng1 type=9-cylinder air-cooled radial piston engine
|eng1 hp=220

|prop blade number=2
|prop name=wooden fixed pitch propeller
|prop dia m=<!-- propeller aircraft -->
<!--
        Performance
-->
|perfhide=

|max speed kmh=183
|max speed note=at sea level
|cruise speed kmh=
|stall speed kmh=63
|stall speed note=slats extended
|never exceed speed kmh=
|range km=
|combat range km=
|ferry range km=
|endurance=<!-- if range unknown -->
|ceiling m=4730
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|time to altitude=</li>
* {{convert|1000|m|abbr=on}} in 4 minutes 20 seconds</li>
* {{convert|2000|m|abbr=on}} in 10 minutes 5 seconds</li>
* {{convert|3000|m|abbr=on}} in 18 minutes 35 seconds
|lift to drag=
|wing loading kg/m2=
|fuel consumption kg/km=
|power/mass=
|more performance=</li>
*'''Take-off run''': {{convert|55|ma|abbr=on}}
*'''Landing run''': {{convert|45|ma|abbr=on}}
<!--
        Armament
-->
|armament=</li>
*1x {{convert|7.7|mm|in|abbr=on|3}} [[Lewis machine gun]] in the rear cockpit
|avionics=
}}

==See also==
{{aircontent
|see also=<!-- other relevant information -->
|related=
*[[PZL.9]] (a floatplane based on Ł.2)
*[[PZL.16]]
|similar aircraft=
*[[Lublin R-X]]
*[[Lublin R-XIII]]
|lists=<!-- related lists -->
}}

==References==
{{Reflist|1}}
* Glass, Andrzej (1977). ''Polskie konstrukcje lotnicze 1893-1939'' [Polish aviation designs 1893-1939], WKiŁ, Warsaw (no ISBN). {{pl icon}}
*Morgała, Andrzej (2003). ''Samoloty wojskowe w Polsce 1924–1939'' (''Military aircraft in Poland 1924–1939''), Bellona, Warsaw, ISBN 83-11-09319-9 {{pl icon}}
*Cynk, Jerzy (1971). ''Polish aircraft 1893-1939'', Putnam & Company, London, ISBN 0-370-00085-4

==External links==
{{commons category|PZL Ł.2}}
*[http://www.sanko.wroclaw.pl/samoloty/zestaw1/pzl-l2.html Photos and drawing]

{{PZL aircraft}}

{{Use dmy dates|date=September 2010}}

{{DEFAULTSORT:Pzl L.2}}
[[Category:Polish military utility aircraft 1930–1939]]
[[Category:Polish sport aircraft 1930–1939]]
[[Category:PZL aircraft]]