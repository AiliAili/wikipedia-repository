{{merge|Christianity in Abkhazia|date=December 2014}}
The orthodox church in [[Abkhazia]] is officially part of the [[Georgian Apostolic Autocephalous Orthodox Church]] (Tskhum-Apkhazeti Eparchy) with 
[[Catholicos-Patriarch of All Georgia|Catholicos-Patriarch]] [[Ilia II]] as its head.<ref>Witness through troubled times : a history of the Orthodox Church of Georgia, 1811 to the present, Abashidze, Zaza.</ref><ref>The Eastern Orthodox churches: concise histories with chronological checklists of their primates, Burgess, Michael, London.</ref><ref name="bible.ca">[http://www.bible.ca/orthodox-church-autocephalous-hierarchy-organization.htm http://www.bible.ca/orthodox-church-autocephalous-hierarchy-organization.htm]</ref> After the [[Georgian-Abkhaz conflict]], the autocephalous church of Georgia lost the control and jurisdiction over its property in Abkhazia. However, all autocephalous churches of the orthodox faith, including the [[Russian Orthodox Church]] and the [[Ecumenical Patriarchate of Constantinople]], recognise Abkhazia as part of the Georgian autocephalous church.<ref>A long walk to church: a contemporary history of Russian Orthodoxy, 2nd ed, Davis, Nathaniel</ref> The Current head of the orthodox church in Abkhazia is Archbishop Daniel of Tskhum-Apkhazeti Eparchy<ref name="bible.ca"/> However, the Georgian Orthodox Church is unable to operate there and most of its clerics as well as the parish have been expelled during the Abkhazian war and in its aftermath.

the war in Abkhazia, the only remaining Orthodox priest of the Georgian Church, ethnic Abkhaz Vissarion (Appliaa) headed the local Orthodox community. In the following years, the recently consecrated clerics from the neighbouring Russian [[Maykop]] Eparchy arrived in Abkhazia and soon engaged in a conflict with Vissarion. Through the mediation of Russian church officials, the two sides managed to reach a power-sharing agreement at Maikop in 2005, and organised themselves into the Eparchy of Abkhazia whose canonical status remains undefined. This failed, however, to settle the disagreement and the eparchy continues to straddle the division. Currently, there are a dozen or so Orthodox clerics in the region, most of whom belong to the Russian Orthodox Church, while the de facto head of the eparchy, Vissarion, nominally remains a subordinate to the Georgian Orthodox Patriarchate.<ref name="blagovest-info.ru">[http://www.blagovest-info.ru/index.php?ss=2&s=3&id=6177 Вновь обострился конфликт внутри православной общины Абхазии. Blagovest.info May 15, 2006. Retrieved on June 26, 2007 (Russian)]</ref>

The Georgian church officials complain that the Russian church interferes in Abkhazia by training and sending in priests loyal to Moscow.<ref>[http://www.state.gov/g/drl/rls/irf/2005/51553.htm Georgia: International Religious Freedom Report 2005. The United States Department of State. Retrieved on May 24, 2007.]</ref> The Russian church officials published translations of the [[Gospel]]s in [[Abkhaz language|Abkhazian]], which drew protests from the Georgian Orthodox and Apostolic Church as a violation of Orthodox Church canon law, constituting a meddling in the internal affairs of another Orthodox church and annexation of Georgian Orthodox property in Abkhazia.<ref>[http://www.geotimes.ge/index.php?m=home&newsid=2810 http://www.geotimes.ge/index.php?m=home&newsid=2810]</ref> The Russian Orthodox Church claims that the clerics of Maykop eparchy serve in Abkhazia only temporarily as the local Orthodox believers do not have contacts with the Georgian Orthodox Church.<ref name="blagovest-info.ru"/>

== Gallery ==
<gallery>
file:Новоафонский монастырь.JPG|New Athos Monastery
File:Сухум,Благовещенский собор.JPG|Church in Sokhumi
File:Mokva cathedral.jpg|[[Mokvi Cathedral|Mokvi cathedral]]
File:Lykhny temple.jpg|Likhni temple
</gallery>

==Armenian Apostolic Church==
Most of the [[Armenians in Abkhazia|ethnic Armenians living in Abkhazia]] who form the second largest ethnic group in the region of Abkhazia after the [[Abkhaz people]], forming 20% of the Abkhazian population with 45,000 out of a total of 215,000, belong to the [[Armenian Apostolic Church]].

==References==
{{Reflist}}
{{Asia in topic|Orthodoxy in}}

{{DEFAULTSORT:Orthodoxy In Abkhazia}}
[[Category:Christianity by country]]
[[Category:Abkhazia]]
[[Category:Eastern Orthodoxy in Georgia (country)]]

[[de:Abchasisch-Orthodoxe Kirche]]
[[fr:Éparchie d'Abkhazie]]
[[ka:ცხუმ-აფხაზეთის ეპარქია]]
[[pl:Abchaski Kościół Prawosławny]]
[[ru:Православие в Абхазии]]