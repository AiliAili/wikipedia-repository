[[File:Coal stockpile at Avonmouth docks - geograph.org.uk - 575556.jpg|thumb|right|Coal stockpile]]
'''Coal homogenization''' refers to the process of mixing [[coal]] to reduce the variance of the product supplied. This homogenization process is performed during the coal stockpiling operation. Although the terms [[Coal Blending|blending]] and homogenization are often used interchangeably, there are differences as the definitions show. The most notable difference is that blending refers to stacking coal from different sources together on one [[stockpile]].<ref>http://www.schade-lagertechnik.de/content/pdf/editorials/schade/Keeping_the_light_on_schade.pdf</ref> The reclaimed heap would then typically have a [[Weighted arithmetic mean|weighted average]] output quality of the input sources. In contrast, homogenization focuses on reducing the [[variance]] of only one source. A blending operation will cause some homogenization.

== Definitions ==

=== Mixing ===

Mixing is defined as a random rearrangement of particles by means of mechanical energy, e.g. rotary devices in a fixed volume. Traces of individual components can still be located within a small quantity of the mixed material of two or more material types. Application: small-scale storage.

=== Blending ===

Blending is defined as the integration of a number of raw materials with different physical or chemical properties in time in order to create a required specification or blend. The aim is to achieve a final product from, for example, two or more coal types, that has a well-defined chemical composition in which the elements are very evenly distributed and no large pockets of one type can be identified. When sampled, the average content and the [[standard deviation]] from the average are the same. Application: e.g. using different types of coal for specific recipes.

=== Homogenizing ===

Homogenizing is defined as the systematic regrouping of the input flow in order to provide a more homogeneous output flow of one type of material so that inherent fluctuations of chemical or physical properties in time are evened out compared to the input flow. Application: e.g. one batch of limestone or coal, i.e. to homogenize the material in itself.<ref>http://repository.tudelft.nl/view/ir/uuid%3A99f7f56a-bb99-426b-84cd-2b21898f970e/</ref> 
[[File:Krupp stacker rtca kestrel mine.jpg|thumb|right|Krupp coal stacker]]

== Applications ==

Worldwide, industry uses bulk material, like coal, as a source of energy or as a raw material for production processes. Intermediate storage facilities (for example [[stockpile]]s) are required to decouple the (discontinuous) supply of raw materials from the (continuous) production process. Large quality fluctuations of the material properties can occur due to the geographical origin of the raw materials. This means that processes using these raw materials have to deal with these fluctuations in order to produce products with a constant quality. The fluctuations can be dealt with in two ways:
#Adapting the process, so that it can handle quality fluctuations of the raw material.
#Adapting the raw material to the requirements of the process: upgrading.
Upgrading is defined as achieving a more constant quality throughout the material flow, where the mean quality cannot be influenced, but the frequency and amplitude of fluctuations around the mean value can.
 
Homogenization aims to upgrade the raw material to a uniform composition of the product being homogenized. In coal homogenization the primary aim is to reduce the [[variance]]<ref>http://www.saimh.co.za/beltcon/beltcon8/paper818.html</ref> of the quality of the product supplied (in comparison to the variance in quality of the different sources of supply). This is necessary to ensure consistent (or [[homogeneous]]) quality, as well as a constant [[calorific value]] of the coal.<ref>http://www.enelex.cz/en/aplikace.php?item=128&odd=2</ref>
Homogenization can be used at coal mines in order to get a better overall quality product from a stockpile. It can thus form an integral part of the quality management system in coal mines.<ref>http://www.enelex.cz/en/aplikace.php?item=128&odd=2</ref> When processing coal by more than one supplier like [[power station]]s and heat plants, coal homogenization can serve well as a strategy of processing the coal in order to get to the best acceptable quality.
Coal homogenization could be applied whenever a consistent quality is needed and multiple sources of different qualities are supplied or mined.
[[File:Krupp bridge reclaimer rtca kestrel mine.jpg|thumb|right|Krupp bridge reclaimer]]

== Methods ==

Typically a large number of thin layers are thrown by the [[stacker]] on a stockpile.  Stacking the layers on each other ensures that the variance in input quality is reduced once the coal is reclaimed in a vertical cross section. After the coal is stacked in horizontal layers, it is reclaimed in vertical slices.  When the standard deviation of each slice’s quality is determined, the resulting standard deviation is much smaller than that of the non-homogenized stockpile.  [[File:Four stacked layers.jpg|thumb|Layers of source stacked on each other]] [[File:Reclaimed slices of stacked layers.jpg|thumb|Effect of reclaiming slices of stacked product]] [[File:Source and product quality.jpg|thumb|Typical effect of homogenisation]]
 
In a simplified form the effectiveness of the homogenization process is represented by E, the homogenization effect:
 
:::::E = σ<small>in</small> / σ<small>out</small> (where σ is the standard deviation).
 
In practice, varying stockpile layer thickness, type of stacking and different reclaiming methods makes the above formulae of little use.  Thus, in practice the homogenization effect E is calculated as follows:
 
E = k √n, k usually varies between 0.5 and 0.7. (where n is the number of layers and k is a constant determined for each stockpiling operation)
 
The different types of stacking will influence k.<ref>http://www.flsmidth.com/~/media/Brochures/Brochures%20for%20crushers%20and%20raw%20material%20stores/stacker%20and%20reclaimer.ashx</ref>

== See also ==

*[[Coal Blending]]
*[[Bulk material handling]]
*[[Stockpile]]
*[[Stacker]]
*[[Reclaimer]]
*[[Variance]]
*[[Heat of combustion]]

== References ==

{{reflist}}

[[Category:Coal]]
[[Category:Bulk material handling]]
[[Category:Mining equipment]]