{{Use dmy dates|date=June 2013}}
{{Infobox royalty
| name            = Princess Alice of Battenberg
| title           = Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece and Denmark
| image           = 1885 Alice.jpg
| image_size      = 225px
| caption         = 
| full name       = Victoria Alice Elizabeth Julia Marie
| spouse          = [[Prince Andrew of Greece and Denmark]]
| issue           = [[Princess Margarita of Greece and Denmark|Margarita, Princess of Hohenlohe-Langenburg]]<br />[[Princess Theodora of Greece and Denmark (1906–1969)|Theodora, Margravine of Baden]]<br />[[Princess Cecilie of Greece and Denmark|Cecilie, Hereditary Grand Duchess of Hesse]]<br />[[Princess Sophie of Greece and Denmark|Sophie, Princess George of Hanover]]<br />[[Prince Philip, Duke of Edinburgh]]
| house           = [[House of Battenberg|Battenberg]]<!--Do not add a royal house or dynasty unless you have an official source. People are born into dynasties of ruling families. Whether or not a person is confirmed to belong to a dynasty by marriage requires a source that proves that fact. -->
| father          = [[Prince Louis of Battenberg]]
| mother          = [[Princess Victoria of Hesse and by Rhine]]
| birth_date      = {{Birth date|1885|2|25|df=y}}
| birth_place     = [[Windsor Castle]], [[Berkshire]], [[England]]
| death_date      = {{Death date and age|1969|12|5|1885|2|25|df=y}}
| death_place     = [[Buckingham Palace]], [[London]]
| place of burial = [[Church of Mary Magdalene]], [[Gethsemane]], [[Jerusalem]]
| signature       = Signature of Princess Alice of Battenberg, later Princess Andrew of Greece and Denmark.jpg
}}

'''Princess Alice of Battenberg''', later '''Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece and Denmark''' (Victoria Alice Elizabeth Julia Marie; 25 February 1885 &ndash; 5 December 1969), was the mother of [[Prince Philip, Duke of Edinburgh]], and mother-in-law of [[Elizabeth II|Queen Elizabeth II]].

A great-granddaughter of [[Queen Victoria]], she grew up in [[German Empire|Germany]], [[England]], and the [[Mediterranean Basin|Mediterranean]]. She was [[Congenital hearing loss|congenitally deaf]].  After marrying [[Prince Andrew of Greece and Denmark]] in 1903, she lived in [[Kingdom of Greece|Greece]] until the exile of most of the [[Greek royal family]] in 1917. On returning to Greece a few years later, her husband was blamed in part for the defeat of Greece in the [[Greco-Turkish War (1919–1922)]], and the family were once again forced into exile until the [[Greek plebiscite, 1935|restoration of the Greek monarchy in 1935]].

In 1930, she was diagnosed with [[schizophrenia]] and committed to a sanatorium in [[Switzerland]]; thereafter, she lived separately from her husband. After her recovery, she devoted most of her remaining years to charity work in Greece. She stayed in [[Athens]] during the [[Second World War]], sheltering Jewish refugees, for which she is recognized as "[[Righteous Among the Nations]]" by [[Israel]]'s [[Holocaust]] memorial institution, [[Yad Vashem]]. After the war, she stayed in Greece and founded an [[Eastern Orthodox Church|Orthodox]] nursing order of [[Nun#Eastern Orthodox|nuns]] known as the Christian Sisterhood of Martha and Mary.

After the fall of King [[Constantine II of Greece]] and the [[Greek military junta of 1967–1974|imposition of military rule in Greece]] in 1967, she was invited by her son and daughter-in-law to live at [[Buckingham Palace]] in [[London]], where she died two years later. Her remains were transferred to the [[Mount of Olives]] in [[Jerusalem]] in 1988.

==Early life==
Alice was born in the Tapestry Room at [[Windsor Castle]] in [[Berkshire]] in the presence of her great-grandmother, [[Queen Victoria]].<ref>Vickers, p. 2</ref> She was the eldest child of [[Prince Louis of Battenberg]] and his wife [[Princess Victoria of Hesse and by Rhine]]. Her mother was the eldest daughter of [[Princess Alice of the United Kingdom|Princess Alice, Grand Duchess of Hesse]], the second daughter of Queen Victoria and [[Albert, Prince Consort|Prince Albert]]. Her father was the eldest son of [[Prince Alexander of Hesse and by Rhine]] through his [[morganatic marriage]] to [[Julia von Hauke|Julia von Hauke, Princess of Battenberg]]. Her three younger siblings, [[Louise Mountbatten|Louise]], [[George Mountbatten, 2nd Marquess of Milford Haven|George]], and [[Louis Mountbatten, 1st Earl Mountbatten of Burma|Louis]], later became Queen of Sweden, 2nd Marquess of Milford Haven, and Earl Mountbatten of Burma, respectively.

She was christened Victoria Alice Elizabeth Julia Marie in [[Darmstadt]] on 25 April 1885. She had six godparents: her three surviving grandparents [[Louis IV, Grand Duke of Hesse|the Grand Duke of Hesse]], Prince Alexander of Hesse and by Rhine, and Julia, Princess of Battenberg; her aunts [[Grand Duchess Elizabeth Fyodorovna|Grand Duchess Elizabeth Fyodorovna of Russia]] and [[Princess Marie of Battenberg|Princess Marie of Erbach-Schönberg]]; and her great-grandmother Queen Victoria.<ref>Vickers, p. 19</ref>

Alice spent her childhood between Darmstadt, London, [[Jugenheim]], and [[Crown Colony of Malta|Malta]] (where her naval officer father was occasionally stationed).<ref name="dnb">{{citation|first=Hugo |last=Vickers|title=Alice, Princess (1885–1969)|journal=Oxford Dictionary of National Biography|publisher=Oxford University Press|year=2004|doi=10.1093/ref:odnb/66337 |url=http://www.oxforddnb.com/view/article/66337|accessdate=8 May 2009}} {{Subscription required}}</ref> Her mother noticed that she was slow in learning to talk, and became concerned by her indistinct pronunciation. Eventually, she was diagnosed with congenital deafness after her grandmother identified the problem and took her to see an ear specialist. With encouragement from her mother, Alice learned to both lip-read and speak in English and German.<ref>Vickers, pp. 24–26</ref> Educated privately, she studied French,<ref>Vickers, p. 57</ref> and later, after her engagement, she learned [[Greek language|Greek]].<ref>Vickers, pp. 57, 71</ref> Her early years were spent in the company of her royal relatives, and she was a [[bridesmaid]] at the marriage of [[George V|George, Duke of York (later King George V)]] and [[Mary of Teck]] in 1893.<ref>Vickers, pp. 29–48</ref> A few weeks before her sixteenth birthday she attended the funeral of Queen Victoria in [[St George's Chapel, Windsor Castle]], and shortly afterward she was [[confirmation|confirmed]] in the [[Anglican]] faith.<ref>Vickers, p. 51</ref>

==Marriage==
[[File:Princess Alice of Battenberg with children.jpg|thumb|right|Alice with her first two children, [[Princess Margarita of Greece and Denmark|Margarita]] and [[Princess Theodora of Greece and Denmark (1906-1969)|Theodora]], c. 1910]]
Princess Alice met and fell in love with Prince Andrew of Greece and Denmark (known as Andrea within the family), the fourth son of [[George I of Greece|King George I]] and [[Olga Constantinovna of Russia|Queen Olga]] of Greece, at [[Edward VII|King Edward VII]]'s London coronation in 1902.<ref>Vickers, p. 52</ref> They married in a [[civil ceremony]] on 6 October 1903 at Darmstadt. The following day, there were two religious marriage ceremonies; one [[Lutheran]] in the Evangelical Castle Church, and one [[Greek Orthodox]] in the Russian Chapel on the [[Mathildenhöhe]].<ref>The Russian Chapel was the personal possession of [[Nicholas II of Russia]] and his wife [[Alexandra Feodorovna (Alix of Hesse)]], Princess Alice's maternal aunt. It was constructed between 1897 and 1899 at the personal expense of the Russian imperial couple for use during family visits to Darmstadt. Source: {{citation|title=Die Russische Orthodoxe Kirche der Hl. Maria Magdalena auf der Mathildenhöhe in Darmstadt|first=Georg|last=Seide|location=Munich|publisher=Russische Orthodoxe Kirche im Ausland|year=1997|isbn=3-926165-73-1|language=German|page=2}}</ref> She adopted the style of her husband, becoming "Princess Andrew".<ref>Eilers, p. 181</ref> The bride and groom were closely related to the ruling houses of the United Kingdom, Germany, [[Russian Empire|Russia]], [[Denmark]], and Greece; their wedding was one of the great gatherings of the descendants of [[Queen Victoria]] and [[Christian IX of Denmark]] held before [[World War I]].<ref name="dnb" />

Prince and Princess Andrew<!--After her marriage she used the style: Princess Andrew--> had five children:
*[[Princess Margarita of Greece and Denmark]] (18 April 1905 &ndash; 24 April 1981), who married [[Gottfried, Prince of Hohenlohe-Langenburg]] (24 March 1897 &ndash; 11 May 1960);
*[[Princess Theodora of Greece and Denmark (1906–1969)|Princess Theodora of Greece and Denmark]] (30 May 1906 &ndash; 16 October 1969), who married [[Berthold, Margrave of Baden]] (24 February 1906 &ndash; 27 October 1963);
*[[Princess Cecilie of Greece and Denmark]] (22 June 1911 &ndash; 16 November 1937), who married [[Georg Donatus, Hereditary Grand Duke of Hesse]] (8 November 1906 &ndash; 16 November 1937);
*[[Princess Sophie of Greece and Denmark]] (26 June 1914 &ndash; 24 November 2001), who married firstly [[Prince Christoph of Hesse]] (14 May 1901 &ndash; 7 October 1943) and secondly [[Prince George William of Hanover (1915–2006)|Prince George William of Hanover]] (25 March 1915 &ndash; 8 January 2006); and
*[[Prince Philip, Duke of Edinburgh|Prince Philip of Greece and Denmark]] (born 10 June 1921), who married [[Elizabeth II|Queen Elizabeth II]] (born 21 April 1926).
All of Prince and Princess Andrew's children later had children of their own.

[[File:AliceBattenberg.jpg|thumb|left|''Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece and Denmark'' by [[Philip de László]], 1907.<br />Private collection of Prince Philip, Duke of Edinburgh.]]

After their marriage, Prince Andrew continued his career in the military and Princess Andrew<!--After her marriage she used the style: Princess Andrew--> became involved in charity work. In 1908, she visited Russia for the wedding of [[Grand Duchess Maria Pavlovna of Russia (1890-1958)|Grand Duchess Marie of Russia]] and [[Prince Wilhelm, Duke of Södermanland|Prince William of Sweden]]. While there, she talked with her aunt, [[Grand Duchess Elizabeth Fyodorovna]], who was formulating plans for the foundation of a religious order of nurses. Princess Andrew<!--After her marriage she used the style: Princess Andrew--> attended the laying of the foundation stone for her aunt's new church. Later in the year, the Grand Duchess began giving away all her possessions in preparation for a more spiritual life.<ref>Vickers, pp. 82–83</ref> On their return to Greece, Prince and Princess Andrew<!--After her marriage she used the style: Princess Andrew--> found the political situation worsening, as the [[Athens]] government had refused to support the [[Cretan State|Cretan]] parliament, which had called for the union of Crete (still nominally part of the [[Ottoman Empire]]) with the Greek mainland. A group of dissatisfied officers formed a [[Greek nationalism|Greek nationalist]] [[Military League]] that eventually led to Prince Andrew's resignation from the army and the rise to power of [[Eleftherios Venizelos]].<ref>Clogg, pp. 97–99</ref>

==Successive life crises==
With the advent of the [[Balkan Wars]], Prince Andrew was reinstated in the army and Princess Andrew<!--After her marriage she used the style: Princess Andrew--> acted as a nurse, assisting at operations and setting up field hospitals, work for which [[George V|King George V]] awarded her the [[Royal Red Cross]] in 1913.<ref name="dnb" /> During [[World War I]], her brother-in-law, [[Constantine I of Greece|King Constantine of Greece]], followed a neutrality policy despite the democratically elected government of Venizelos supporting the [[Allies of World War I|Allies]]. Princess Andrew<!--After her marriage she used the style: Princess Andrew--> and her children were forced to shelter in the palace cellars during the [[Greece during World War I|French bombardment of Athens]] on 1 December 1916.<ref>Vickers, p. 121</ref> By June 1917, the King's neutrality policy had become so untenable that she and other members of the Greek royal family were forced into exile when her brother-in-law abdicated. For the next few years, most of the Greek royal family lived in [[Switzerland]].<ref>Van der Kiste, pp. 96 ff.</ref>

The global war effectively ended much of the political power of Europe's dynasties. The naval career of her father, [[Prince Louis of Battenberg]], had collapsed at the beginning of the war in the face of anti-German sentiment in Britain. At the request of King George V, he relinquished the [[Grand Duchy of Hesse|Hessian]] title Prince of Battenberg and the style of [[Serene Highness]] on 14 July 1917 and anglicized the family name to [[Mountbatten]]. The following day, the King created him [[Marquess of Milford Haven]] in the [[peerage]] of the United Kingdom.<ref>Princess Alice of Battenberg never used the Mountbatten surname nor did she assume the [[courtesy title]] as a daughter of a British marquess since she had married into the Royal House of Greece in 1903.</ref> The following year, two of her aunts, [[Alix of Hesse|Alix, Empress of Russia]], and [[Grand Duchess Elizabeth Fyodorovna]] were murdered by [[Bolsheviks]] after the [[Russian revolution of 1917|Russian revolution]]. At the end of the war the Russian, German and [[Austro-Hungarian empire]]s had fallen, and Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s uncle, [[Ernest Louis, Grand Duke of Hesse]], was deposed.<ref>Vickers, pp. 137–138</ref>

[[File:Laszlo - Princess Andrew of Greece.jpg|thumb|right|''Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece and Denmark'' by [[Philip de László]], 1922.<br />Private collection of Prince Philip, Duke of Edinburgh.]]

On King Constantine's restoration in 1920, they briefly returned to Greece, taking up residence at [[Mon Repos, Corfu|Mon Repos]] on [[Corfu]].<ref>Inherited by Prince Andrew on his father's assassination in 1913.</ref> But after the defeat of the [[Hellenic Army]] in the [[Greco-Turkish War (1919-1922)|Greco-Turkish War]], a Revolutionary Committee under the leadership of Colonels [[Nikolaos Plastiras]] and [[Stylianos Gonatas]] seized power and forced King Constantine into exile once again.<ref>Vickers, p. 162</ref> Prince Andrew, who had served as commander of the Second Army Corps during the war, was arrested. [[Trial of the Six|Several former ministers and generals arrested]] at the same time were shot, and British diplomats assumed that Prince Andrew was also in mortal danger. After a show trial he was sentenced to banishment, and Prince and Princess Andrew<!--After her marriage she used the style: Princess Andrew--> and their children fled Greece aboard a British cruiser, [[HMS Calypso (D61)|HMS ''Calypso'']], under the protection of the British naval attaché, Commander [[Gerald Talbot (diplomat)|Gerald Talbot]].<ref>Vickers, p. 171</ref>

==Illness==
The family settled in a small house loaned to them by [[Princess Marie Bonaparte|Princess George of Greece]] at [[Saint-Cloud]], on the outskirts of [[Paris]], where Princess Andrew helped in a charity shop for Greek refugees.<ref>Vickers, pp. 176–178</ref> She became deeply religious, and on 20 October 1928 entered the [[Eastern Orthodox Church|Greek Orthodox Church]]. That winter, she translated her husband's defence of his actions during the Greco-Turkish War into English.<ref>{{citation|last=Greece|first=H.R.H. Prince Andrew of|authorlink=Prince Andrew of Greece|others=Translated and Preface by H.R.H. Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece|title=Towards Disaster: The Greek Army in Asia Minor in 1921|publisher=John Murray|location=London|year=1930}}</ref><ref>Vickers, pp. 198–199</ref> Soon afterward, she began claiming that she was receiving divine messages and that she had healing powers.<ref>Vickers, p. 200</ref> In 1930, after suffering a severe nervous breakdown, Princess Andrew<!--After her marriage she used the style: Princess Andrew--> was diagnosed with [[paranoid schizophrenia]], first by Thomas Ross, a psychiatrist who specialised in [[shell-shock]], and subsequently by [[Maurice Craig (psychiatrist)|Sir Maurice Craig]], who treated the future [[George VI|King George VI]] before he had speech therapy.<ref name=Cohen>Cohen, D. (2013), "Freud and the British Royal Family", ''The Psychologist'', Vol. 26, No. 6, pp. 462–463</ref> The diagnosis was confirmed at Dr [[Ernst Simmel]]'s sanatorium at [[Tegel]], [[Berlin]].<ref>Vickers, p. 205</ref> She was forcibly removed from her family and placed in Dr [[Ludwig Binswanger]]'s sanatorium in [[Kreuzlingen]], Switzerland.<ref>Vickers, p. 209</ref> It was a famous and well-respected institution with several celebrity patients, including [[Vaslav Nijinsky]], the ballet dancer and choreographer, who was there at the same time as Princess Andrew<!--After her marriage she used the style: Princess Andrew-->.<ref>Vickers, p. 213</ref> Binswanger also diagnosed [[schizophrenia]]. Both he and Simmel consulted [[Sigmund Freud]] who believed that the princess's delusions were the result of sexual frustration. He recommended "X-raying her ovaries in order to kill off her libido." Princess Andrew protested her sanity and repeatedly tried to leave the asylum.<ref name=Cohen/>

During Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s long convalescence, she and Prince Andrew drifted apart, her daughters all married German princes in 1930 and 1931 (she did not attend any of the weddings), and [[Prince Philip, Duke of Edinburgh|Prince Philip]] went to England to stay with his uncles, [[Louis Mountbatten, 1st Earl Mountbatten of Burma|Lord Louis Mountbatten]] and [[George Mountbatten, 2nd Marquess of Milford Haven]], and his grandmother, the [[Princess Victoria of Hesse and the Rhine|Dowager Marchioness of Milford Haven]].<ref>Ziegler, p. 101</ref>

Princess Andrew remained at Kreuzlingen for two years, but after a brief stay at a clinic in [[Meran]] was released and began an itinerant, incognito existence in Central Europe. She maintained contact with her mother, but broke off ties to the rest of her family until the end of 1936.<ref>Vickers, pp. 245–256</ref> In 1937, her daughter Cecilie, son-in-law and two of her grandchildren were killed in an [[Sabena OO-AUB Ostend crash|air accident at Ostend]]; she and Prince Andrew met for the first time in six years at the funeral (Prince Philip, Lord Louis Mountbatten and [[Hermann Göring]] also attended).<ref>Vickers, p. 273</ref> She resumed contact with her family, and in 1938 returned to Athens alone to work with the poor, living in a two-bedroomed flat near the [[Benaki Museum]].<ref>Vickers, pp. 281, 291</ref>

==World War II==
During [[World War II]], Princess Andrew<!--After her marriage she used the style: Princess Andrew--> was in the difficult situation of having sons-in-law fighting on the German side and a son in the British [[Royal Navy]]. Her cousin, Prince Victor zu Erbach-Schönberg,<ref>The son of Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s godmother and aunt, [[Princess Marie of Battenberg]], who had married into the Erbach-Schönberg family.</ref> was the German ambassador in Greece until the occupation of Athens by [[Axis powers|Axis]] forces in April 1941. She and her sister-in-law, [[Grand Duchess Elena Vladimirovna of Russia|Princess Nicholas of Greece]] (the mother of [[Princess Marina, Duchess of Kent]]), lived in Athens for the duration of the war, while most of the Greek royal family remained in exile in South Africa.<ref>Vickers, p. 292</ref><ref name="times">{{citation|title=Princess Andrew<!--After her marriage she used the style: Princess Andrew-->, Mother of the Duke of Edinburgh|newspaper=[[The Times]]|location=London|date=6 December 1969|page=8 col. E}}</ref> She moved out of her small flat and into her brother-in-law [[Prince George of Greece and Denmark|George]]'s three-storey house in the centre of Athens. She worked for the Red Cross, helped organize soup kitchens for the starving populace and flew to [[Sweden]] to bring back medical supplies on the pretext of visiting her sister, [[Louise Mountbatten|Louise]], who was married to [[Gustaf VI Adolf of Sweden|the Crown Prince]].<ref name="vickers">Vickers, pp. 293–295</ref> She organized two shelters for orphaned and stray children, and a nursing circuit for poor neighbourhoods.<ref>Vickers, p. 297</ref>

The occupying forces apparently presumed Princess Andrew<!--After her marriage she used the style: Princess Andrew--> was pro-German, as one of her sons-in-law, [[Prince Christoph of Hesse]], was a member of the [[NSDAP]] and the [[Waffen-SS]], and another, [[Berthold, Margrave of Baden]], had been invalided out of the German army in 1940 after an injury in France. Nonetheless, when visited by a German general who asked her, "Is there anything I can do for you?", she replied, "You can take your troops out of my country."<ref name="vickers" />
[[File:Bundesarchiv Bild 101I-521-2142-29, Athen, Panzer IV.jpg|thumb|right|German tanks roll through [[Athens]], 1943]]
After the fall of Italian dictator [[Benito Mussolini]] in September 1943, the German Army occupied Athens, where a minority of Greek [[Jew]]s had sought refuge. The majority (about 60,000 out of a total population of 75,000) were deported to [[Nazi concentration camps]], where all but 2,000 died.<ref>{{citation|last=Bowman |first=Stephen |contribution=Jews |editor-last=Clogg|editor-first=Richard|title=Minorities in Greece |publisher=Hurst & Co.|location=London|pages=64–80 |isbn=1-85065-706-8 |year=2002 }}</ref> During this period, Princess Andrew<!--After her marriage she used the style: Princess Andrew--> hid Jewish widow Rachel Cohen and two of her five children, who sought to evade the [[Gestapo]] and deportation to the death camps.<ref name="vickers2">Vickers, pp. 298–299</ref> Rachel's husband, Haimaki Cohen, had aided King [[George I of Greece]] in 1913. In return, King George had offered him any service that he could perform, should Cohen ever need it. Cohen's son remembered this during the [[Nazi Germany|Nazi]] threat, and appealed to Princess Andrew<!--After her marriage she used the style: Princess Andrew-->, who with Princess Nicholas was one of only two remaining members of the royal family left in Greece. She honoured the promise and saved the Cohen family.<ref name="vickers2" />

When Athens was liberated in October 1944, [[Harold Macmillan]] visited Princess Andrew<!--After her marriage she used the style: Princess Andrew--> and described her as "living in humble, not to say somewhat squalid conditions".<ref>Macmillan, pp. 558–559</ref> In a letter to her son, she admitted that in the last week before liberation she had had no food except bread and butter, and no meat for several months.<ref>Vickers, p. 306</ref> By early December the situation in Athens had far from improved; Communist guerillas ([[Greek People's Liberation Army|ELAS]]) were fighting the British for control of the capital. As the fighting continued, Princess Andrew<!--After her marriage she used the style: Princess Andrew--> was informed that her husband had died, just as hopes of a post-war reunion of the couple were rising.<ref name="times" /> They had not seen each other since 1939. During the fighting, to the dismay of the British, she insisted on walking the streets distributing rations to policemen and children in contravention of the curfew order. When told that she might have been shot by a stray bullet, she replied "they tell me that you don't hear the shot that kills you and in any case I am deaf. So, why worry about that?"<ref>Vickers, p. 311</ref>

==Widowhood==
Princess Andrew<!--After her marriage she used the style: Princess Andrew--> returned to Great Britain in April 1947 to attend the November wedding of her only son, Philip, to [[Elizabeth II|Princess Elizabeth]], the elder daughter and heir presumptive of [[King George VI]]. She had some of her remaining jewels used in Princess Elizabeth's engagement ring.<ref>Vickers, p. 326</ref> On the day of the wedding, her son was created [[Duke of Edinburgh]] by George VI. For the wedding ceremony, Princess Andrew sat at the head of her family on the north side of [[Westminster Abbey]], opposite the King, [[Queen Elizabeth The Queen Mother|Queen Elizabeth]] and [[Mary of Teck|Queen Mary]]. It was decided<!--By who?--> not to invite Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s daughters to the wedding because of anti-German sentiment in Britain following World War II.<ref>Bradford, p. 424</ref>

In January 1949, the princess founded a nursing order of Greek Orthodox nuns, the Christian Sisterhood of Martha and Mary, modelled after the convent that her aunt, the martyr [[Princess Elisabeth of Hesse and by Rhine (1864–1918)|Grand Duchess Elizabeth Fyodorovna]], had founded in Russia in 1909. She trained on the Greek island of [[Tinos]], established a home for the order in a hamlet north of Athens, and undertook two tours of the United States in 1950 and 1952 in an effort to raise funds. Her mother was baffled by her actions, "What can you say of a nun who smokes and plays [[canasta]]?", she said.<ref>Vickers, p. 336</ref> Her daughter-in-law became queen of the [[Commonwealth realm]]s in 1952, and Princess Andrew<!--After her marriage she used the style: Princess Andrew--> attended her [[coronation of Queen Elizabeth II|coronation]] in June 1953, wearing a two-tone grey dress and wimple in the style of her nun's habit. However, the order eventually failed through a lack of suitable applicants.<ref name="nyt">{{citation|title=Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece, 84, Mother of Prince Philip, Dead|newspaper=[[New York Times]]|date=6 December 1969|page=37 col. 2}}</ref>

In 1960, she visited India at the invitation of [[Rajkumari Amrit Kaur]], who had been impressed by Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s interest in Indian religious thought, and for her own spiritual quest. The trip was cut short when she unexpectedly took ill, and her sister-in-law, [[Edwina Mountbatten]], who happened to be passing through [[Delhi]] on her own tour, had to smooth things with the Indian hosts who were taken aback at Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s sudden change of plans. She later claimed she had had an [[out-of-body experience]].<ref>Vickers, pp. 364–366</ref> Edwina continued her own tour, and died the following month.

Increasingly deaf and in failing health, Princess Andrew<!--After her marriage she used the style: Princess Andrew--> left Greece for the last time following the [[Greek military junta of 1967–1974|21 April 1967 Colonels' Coup]]. Queen Elizabeth II and the Duke of Edinburgh invited Princess Andrew<!--After her marriage she used the style: Princess Andrew--> to reside permanently at [[Buckingham Palace]] in London.<ref name="dnb" /> King [[Constantine II of Greece]] and [[Queen Anne-Marie of Greece|Queen Anne-Marie]] went into exile that December after a failed royalist counter-coup.<ref>Clogg, pp. 188–189</ref><ref>Woodhouse, p. 293</ref>

==Death and burial==
[[File:Church of Mary Magdalene1.jpg|thumb|[[Church of Mary Magdalene]], Princess Andrew's burial place in [[Jerusalem]]]]
Despite suggestions of senility in later life, Princess Andrew<!--After her marriage she used the style: Princess Andrew--> remained lucid but physically frail.<ref>Vickers, p. 392</ref> She died at Buckingham Palace on 5 December 1969. She left no possessions, having given everything away. Initially her remains were placed in the Royal Crypt in [[St George's Chapel, Windsor Castle]], but before she died she had expressed her wish to be buried at the [[Church of Mary Magdalene|Convent of Saint Mary Magdalene]] in [[Gethsemane]] on the [[Mount of Olives]] in [[Jerusalem]] (near her aunt [[Princess Elisabeth of Hesse and by Rhine (1864–1918)|Grand Duchess Elizabeth Fyodorovna]], a Russian Orthodox saint). When her daughter, [[Princess Sophie of Greece and Denmark|Princess George of Hanover]], complained that it would be too far away for them to visit her grave, Princess Andrew jested, "Nonsense, there's a perfectly good bus service!"<ref>Vickers, p. 396</ref> Her wish was realized on 3 August 1988 when her remains were transferred to her final resting place in a crypt below the church.<ref name="dnb" /><ref>{{citation|url=http://www.jerusalem-mission.org/convent_magdalene.html|title=Convent of Saint Mary Magdalene - The Garden of Gethsemane|publisher=Russian Ecclesiastical Mission in Jerusalem|accessdate=8 May 2009}}</ref>

On 31 October 1994 Princess Andrew<!--After her marriage she used the style: Princess Andrew-->'s two surviving children, the Duke of Edinburgh and Princess George of Hanover, went to [[Yad Vashem]] (the Holocaust Memorial) in Jerusalem to witness a ceremony honouring her as "[[Righteous Among the Nations]]" for having hidden the Cohens in her house in Athens during the Second World War.<ref>Vickers, p. 398.</ref><ref>{{citation|title=Duke pays homage to Holocaust millions|first=Christopher|last=Walker|newspaper=The Times|location=London|date=1 November 1994|page=12}}</ref> Prince Philip said of his mother's sheltering of persecuted Jews, "I suspect that it never occurred to her that her action was in any way special. She was a person with a deep religious faith, and she would have considered it to be a perfectly natural human reaction to fellow beings in distress."<ref>{{citation| last=Brozan|first=Nadine|title=Chronicle|newspaper=New York Times|date=1 November 1994}}</ref> In 2010, the Princess was posthumously named a [[British Hero of the Holocaust|Hero of the Holocaust]] by the British Government.<ref>{{citation|url=http://www.telegraph.co.uk/history/britain-at-war/7402443/Britons-honoured-for-holocaust-heroism.html|title=Britons honoured for holocaust heroism |publisher=The Telegraph|date=9 March 2010|accessdate=4 July 2016|archiveurl=|archivedate=}}</ref>

==Titles, styles, and honours==

===Titles and styles===
*'''25 February 1885 – 6 October 1903:''' ''Her Serene Highness'' Princess Alice of Battenberg<ref name="ruv">Ruvigny, p. 71</ref>
*'''6 October 1903 – 5 December 1969:''' ''Her Royal Highness'' Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece and Denmark<ref name="ruv" />
*From 1949 until her death, she was sometimes known as Mother Superior Alice-Elizabeth<ref name="nyt" />

===Honours===
* {{flagicon|Kingdom of Greece}} Dame Grand Cross of the [[Order of Saints Olga and Sophia]] (1903)<ref>{{citation|authorlink=Hugh Massingberd|last=Montgomery-Massingberd|first=Hugh (ed.)|year=1977|title=Burke's Royal Families of the World'', 1st edition''|location=London|publisher=Burke's Peerage|isbn=0-85011-023-8|page=214}}</ref>
* {{flagicon|United Kingdom}} [[Royal Red Cross]] (1913)

'''Posthumous:'''
* {{flagicon|Israel}} [[Righteous Among the Nations]] (1993)

==Ancestry==
{{Ahnentafel top|width=100%}}
<center>{{Ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Princess Alice of Battenberg'''
|2= 2. [[Prince Louis of Battenberg]]
|3= 3. [[Princess Victoria of Hesse and by Rhine]]
|4= 4. [[Prince Alexander of Hesse and by Rhine (1823–1888)|Prince Alexander of Hesse and by Rhine]]
|5= 5. [[Julia, Princess of Battenberg|Countess Julia Hauke]]
|6= 6. [[Louis IV, Grand Duke of Hesse|Louis IV, Grand Duke of Hesse and by Rhine]]
|7= 7. [[Princess Alice of the United Kingdom]]
|8= 8. [[Louis II, Grand Duke of Hesse|Louis II, Grand Duke of Hesse and by Rhine]]
|9= 9. [[Princess Wilhelmine of Baden]]
|10= 10. [[John Maurice Hauke|Count John Maurice von Hauke]]
|11= 11. Sophie Lafontaine
|12= 12. [[Prince Karl of Hesse and by Rhine]]
|13= 13. [[Princess Elisabeth of Prussia]]
|14= 14. [[Albert, Prince Consort|Prince Albert of Saxe-Coburg and Gotha]]
|15= 15. [[Queen Victoria|Victoria of the United Kingdom]]
|16= 16. [[Louis I, Grand Duke of Hesse|Louis I, Grand Duke of Hesse and by Rhine]]
|17= 17. [[Princess Louise of Hesse-Darmstadt (1761–1829)|Princess Louise of Hesse-Darmstadt]]
|18= 18. [[Charles Louis, Hereditary Prince of Baden]]
|19= 19. [[Princess Amalie of Hesse-Darmstadt]]
|20= 20. [[:pl:Fryderyk Karol Hauke|Friedrich Karl Emanuel Hauke]]
|21= 21. [[:de:Maria Salomea Schweppenhäuser|Maria Salomé Schweppenhäuser]]
|22= 22. [[Franz Leopold Lafontaine]]
|23= 23. Maria Theresa Kornély
|24= 24. [[Louis II, Grand Duke of Hesse|Louis II, Grand Duke of Hesse and by Rhine]] (= 8)
|25= 25. [[Princess Wilhelmine of Baden]] (= 9)
|26= 26. [[Prince Wilhelm of Prussia (1783–1851)|Prince Wilhelm of Prussia]]
|27= 27. [[Princess Maria Anna of Hesse-Homburg]]
|28= 28. [[Ernest I, Duke of Saxe-Coburg and Gotha]]
|29= 29. [[Princess Louise of Saxe-Gotha-Altenburg (1800–1831)|Princess Louise of Saxe-Gotha-Altenburg]]
|30= 30. [[Prince Edward, Duke of Kent and Strathearn]]
|31= 31. [[Princess Victoria of Saxe-Coburg-Saalfeld]]
}}</center>
{{Ahnentafel bottom}}

==Footnotes==
{{Reflist|30em}}

==References==
{{Refbegin}}
There is only one English-language biography of Princess Alice of Battenberg: the official biography written by [[Hugo Vickers]].<sup>1</sup>
*{{Citation|last=Bradford|first=Sarah|title=King George VI|publisher=Weidenfeld and Nicolson|location=London|year=1989|isbn=0-297-79667-4}}
*{{Citation|last=Clogg |first=Richard |authorlink=Richard Clogg |title=A Short History of Modern Greece |year=1979 |publisher=Cambridge University Press |location=Cambridge |isbn=0-521-22479-9}}
*{{Citation|last=Eilers|first=Marlene A.|title=Queen Victoria's Descendants|publisher=Genealogical Publishing Co.|location=Baltimore, Maryland|year=1987}}
*{{Citation|last=Macmillan|first=Harold|authorlink=Harold Macmillan|title=War Diaries|publisher=Macmillan|location=London|year=1984|isbn=0-333-39404-6}}
*{{Citation|last=Ruvigny|first=Marquis of|title=The Titled Nobility of Europe|publisher=Harrison and Sons|location=London|year=1914}}
*{{Citation|first=John |last=Van der Kiste |authorlink=John Van der Kiste|title=Kings of the Hellenes|publisher=Alan Sutton Publishing|location=Stroud, Gloucestershire, England|year=1994|isbn=0-7509-0525-5}}
*{{Citation|last=Vickers|first=Hugo|authorlink=Hugo Vickers|title=Alice, Princess Andrew<!--After her marriage she used the style: Princess Andrew--> of Greece|publisher=Hamish Hamilton|location=London|year=2000|isbn=0-241-13686-5}}
*{{Citation|last=Woodhouse |first=C. M. |authorlink=Montague Woodhouse, 5th Baron Terrington |title=The Story of Modern Greece |year=1968 |publisher=Faber and Faber |location=London}}
*{{Citation|last=Ziegler|first=Philip|authorlink=Philip Ziegler|title=Mountbatten|publisher=Collins|location=London|year=1985|isbn=0-00-216543-0}}

<small><sup>1</sup>[http://catalog.loc.gov/webvoy.htm Library of Congress catalog] and [http://www.bl.uk/ British Library catalog] searched on 8 May 2009.</small>
{{Refend}}

==External links==
{{commons category}}
{{Portal|Greece}}
*[http://www.yadvashem.org/yv/en/righteous/stories/princess_alice.asp Yad Vashem The Holocaust Martyrs' and Heroes' Remembrance Authority]
*{{NPG name}}

{{Battenberg family}}
{{Greek princesses by marriage}}
{{Danish princesses by marriage}}
{{Prince Philip, Duke of Edinburgh}}
{{Authority control}}

{{Featured article}}

{{DEFAULTSORT:Alice Of Battenberg, Princess}}
[[Category:1885 births]]
[[Category:1969 deaths]]
[[Category:Greek princesses]]
[[Category:House of Battenberg]]
[[Category:House of Glücksburg (Greece)]]
[[Category:Members of the Royal Red Cross]]
[[Category:Deaf royalty and nobility]]
[[Category:People with schizophrenia]]
[[Category:Greek Righteous Among the Nations]]
[[Category:Burials at the Church of Maria Magdalene]]
[[Category:People from Windsor, Berkshire]]
[[Category:Dames of the Order of Queen Maria Luisa]]