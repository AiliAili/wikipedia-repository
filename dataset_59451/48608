{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox medical person
 | name              = Robert Arthur Hughes
 | image             = Dr. Robert Arthur Hughes.jpg
 | alt               = Photo of Dr. Robert Arthur Hughes
 | caption           = Dr. Robert Arthur Hughes
 | birth_date        = {{Birth date|df=yes|1910|03|12}}
 | birth_place       = [[Oswestry]], Shropshire
 | death_date        = {{Death date and age|df=yes|1996|06|01|1910|12|03}}
 | death_place       = [[Liverpool]], England
 | profession        = Surgeon & medical missionary to india
 |specialism        =
 |research_field    =
 | known_for         = Medical Mission in [[Shillong|Shillong, India]]
 | years_active      = 30 Years
 | education         = [[Bachelor of Medicine, Bachelor of Surgery]]<br>[[University of Liverpool School of Medicine]]
 | work_institutions = [[Dr. H. Gordon Roberts Hospital, Shillong]]
 | prizes         =
 | relations         =
}}

'''Dr. Robert Arthur Hughes''', [[M.B.Ch.B]], [[M.R.C.S.]], [[L.R.C.P.]], [[F.R.C.S.]], [[O.B.E.]], (3 December 1910 – 1 June 1996) was a medical [[missionary]] for the [[Presbyterian Church of Wales]] who worked in [[Shillong]] from 1939–1969 at the Welsh Mission Hospital, also known as the [[Dr. H. Gordon Roberts Hospital, Shillong]].<ref name="Obituary">{{cite news
|last=Rees
|first=D. Ben
|title=Obituary: Dr Arthur Hughes
|url=http://www.independent.co.uk/news/obituaries/obituary--dr-arthur-hughes-1337550.html
|publisher=The Independent
|accessdate=12 December 2012
|location=London
|date=17 June 1996}}</ref> Hughes trained as a surgeon in London prior to his time in [[India]].<ref name="Vehicles">{{cite book
|last=Rees
|first=D. Ben
|title=Vehicles of Grace and Hope: Welsh Missionaries in India 1800–1970
|year=2002
|location=William Carey Library
|isbn=0-87808-505-X}}</ref>{{rp|66}}  He is called the "Schweitzer of [[Assam]]," comparing his missionary work to that of the [[Nobel Peace Prize]] winner [[Albert Schweitzer]].<ref name="Obituary"/> During his 40 years in India, Hughes expanded the Welsh Mission Hospital and developed a travelling [[dispensary]] to aid those in the surrounding providences.<ref name="Christianity">{{cite book
|title=Christianity and Change in Northeast India
|year=2009
|publisher=Concept Publishing Company
|isbn=9788180694479
|editor1=Tanka Bahadur Subba |editor2=Joseph Puthenpurakal |editor3=Shaji Joseph Puykunnel }}</ref>{{rp|209}} Hughes is best known for attempting to eradicate [[malaria]] from the area, introducing a [[vagus nerve]] resection process to alleviate pain from [[peptic ulcers]] and a [[rickets]] treatment in the infant population, recognising a protein calorie deficiency disorder called [[kwashiorkor]] in the Indian population, founding the area's first [[blood bank]], performing the first lower segment [[Caesarean section]] without antibiotics to India, and expanding educational training for medical and nursing organisations.<ref name="Obituary"/><ref name="Gwalia">{{cite book
|last=Jenkins
|first=Nigel
|title=Gwalia in Khasia
|year=1995
|publisher=Gomer Press
|location=Llandysul, Dyfed
|isbn=1859021840}}</ref>{{rp|308}}

==Early life==
Robert Arthur Hughes was born on 3 December 1910 to Reverend Howell Harris Hughes and Annie Myfanwy Hughes in [[Oswestry]], a town in [[Shropshire]], England.<ref name="Noble">{{cite book
|last=Khongwir
|first=L.
|title=Noble Deeds in Silence: A Short Biography of Dr. Robert Arthur Hughes, M.B. ChAB., M.R.C.S., L.R.C.P., F.R.C.S., O.B.E.
|year=2010
|publisher=DVS Publishers
|isbn=978-81-86307-15-1}}</ref>{{rp|19}} Shropshire is along the border of [[Wales]], the country of origin for Hughes' parents. Robert Hughes and his brother, John Harris Hughes, were twins, though Robert was older and had better health during their youth.<ref name="Noble"/>{{rp|22}} Rev. Howell Harris Hughes, named after [[Howell Harris]], was the Minister at Oswald Road Presbyterian in addition to being a [[pacifist]] and involved in the [[Fellowship of Reconciliation]] in Wales.<ref>{{cite journal
|last=Morgan
|first=D. Densil
|title=Journal of Welsh Religious History
|url=http://welshjournals.llgc.org.uk/browse/viewpage/llgc-id:1164385/llgc-id:1165777/llgc-id:1165778/get650
|year=2004
|publisher=The Centre for Advanced Study of Religion in Wales
|issn=0967-3938
|series=New Series
|volume= 4
|editor=Geraint Tudur, Robert Pope}}</ref>{{rp|74}} Annie Hughes was educated at the [[University of Wales]] in Bangor and became headmistress at a school in [[Rhosllannerchrugog]] until her marriage to Hughes.<ref name="Call">{{cite web
|last=Rees
|first=D Ben
|title=The Call and Contribution of Dr Robert Arthur Hughes OBE, FRCS
|publisher=Modern Welsh Publications Ltd
|url=http://www.liverpool-welsh.co.uk/hughes.htm#_edn9
|accessdate=14 December 2012}}</ref>

During [[World War I]], Howell Harris was Minister of the Tabernacle Chapel in [[Bangor, Gwynedd]], and Arthur and Harris attended Christchurch School in [[Waterloo, Ashton under Lyne|Waterloo]] and then the Waterloo and Seaforth Grammar School during this time. When the boys were fifteen, the family moved to [[Llandudno]], where they attended the [[John Bright School]].<ref name="Vehicles"/>{{rp|66}}  Robert Hughes excelled in both academics and athletics. Harris Hughes was sickly during his youth and attended the University of Wales in Bangor and became an ordained Minister after attending the [[United Theological College, Aberystwyth|United Theological College]] in Aberystwth.<ref name="Noble"/>{{rp|22}}  He was eventually elected Moderator of the Presbyterian Church of Wales in 1975.<ref name="Call"/>

===Education===
In 1928, Robert Hughes entered [[Liverpool University]] to study medicine. During his time in the Faculty of Medicine, Hughes joined the Student Christian Movement and the Student Volunteer Movement Union.<ref name="Call"/> He was awarded the gold medal in surgery and two other academic distinctions when he graduated in 1933.<ref name="Vehicles"/>{{rp|66}}

After graduating in Medicine, Hughes as a House Surgeon worked at Royal Southern Hospital under the instruction of Mr. O. Herbert Williams, a distinguished surgeon.<ref name="Noble"/><ref>{{rp|27}}{{cite web
|title=Welsh Biography Online
|work=Williams, Owen Herbert
|publisher=National Library of Wales
|author=Emyr Wyn Jones
|url=http://wbo.llgc.org.uk/en/s2-WILL-HER-1884.html}}</ref><ref name="Welsh">{{cite web
|title=Welsh Biography Online
|url=http://wbo.llgc.org.uk/en/s8-HUGH-ART-1910.html
|work=Hughes, Robert Arthur|publisher=National Library of Wales
|accessdate=14 December 2012
|author=D Ben Rees}}</ref> When he finished his time as House Surgeon, Hughes joined the medical firm of the same hospital under Dr. Norman Capon and worked as House Physician. Hughes then decided to pursue a career in surgical medicine and was appointed the John Rankin Fellow in Human Anatomy, as anatomical knowledge was a requirement for the first FRCS examination.<ref name="Vehicles"/>{{rp|66}}<ref name="Hughes">{{cite web
|last=Rees
|first=D Ben
|title=Obituaries
|url=http://www.liverpool-welsh.co.uk/obituaries.htm
|work=Hughes, Robert Arthur (1910–1996)
|publisher=Liverpool Welsh
|accessdate=14 December 2012}}</ref>

After passing his first examination in 1935, Hughes spent a short amount of time as Junior Registrar at Stanley Hospital until he became Surgical Registrar and Assistant Surgical Pathologist at David Lewis Northern Hospital.<ref name="Vehicles"/>{{rp|66}}  In 1936, Hughes received his diploma MRCS, LRCP. After an unsuccessful first attempt, Hughes passed the final FRCS examination in 1937, becoming a [[Fellow of the Royal College of Surgeons of England]].<ref name="Noble"/>{{rp|27}}

In August 1937, Hughes applied to become a missionary for the Presbyterian Church of Wales. At the time, the Foreign Missions Committee of the church had two doctors applying but three hospitals in need of aid.<ref name="Noble"/>{{rp|38}} Once the Foreign Mission Committee accepted Hughes in 1937, he was sent to the [[London School of Tropical Medicine]] to receive his diploma in Tropical Medicine and Hygiene, or DTM&H. Hughes was assigned to join Dr. Roberts in Shillong upon the completion of his Tropical Medicine course.<ref name="Obituary"/>

==Marriage to Nancy Hughes==
While working at the David Lewis Northern Hospital, Hughes met Ann "Nancy" Beatrice Wright, who was working as a nurse. Nancy was born on 27 September 1908, to William and Mary Elizabeth Wright, and was the middle of three children.  She grew up in Claughton, [[Birkenhead]] and attended nursing school in [[Liverpool]].<ref name="Noble"/>{{rp|47}}  She worked at the Isolation Hospital in [[Bidston]] for three years to begin her training, and then took a job at Northern Hospital. Although she would return to Northern, she was accepted a position at Oxford Street Maternity Hospital to complete a course in midwifery training. Upon returning the Northern, she met Hughes, who was working there as a surgical tutor.<ref name="Vehicles"/> The two were married on 7 January 1939, and left for India three weeks later. Although Nancy was a trained nurse, the policy of the Mission Society prohibited her from working in the hospital.<ref name="Noble"/>{{rp|63}}  While Robert Hughes was away during [[World War II]], Nancy Hughes resumed her duties as a nurse, tending to injured [[Allies of World War II|Allied]] soldiers at Dr. H. Gordon Roberts Hospital.<ref name="Vehicles"/>{{rp|61}}

==Medical mission in India==
On 28 January 1939, the Hughes' left Liverpool for India aboard the City of Marseilles and in February they arrived at the Welsh Mission Hospital in Shillong, which was founded by Dr. Hugh Gordon Roberts in 1922 to serve the [[Khasi people|Khasi]] people.
Hughes was given charge of the general wards while Dr. Roberts took care of administrative matters until 1942. Upon that time, Hughes was appointed [[Senior Medical Officer]]. During daily life at the hospital, Hughes cared for patients suffering from road accidents, animal mauling, [[diphtheria]], [[malaria]], [[typhoid]], [[dysentery]], [[leprosy]], and [[tuberculosis]].<ref name="Noble"/>{{rp|66}}

When [[World War II]] reached Assam, Hughes was inducted into the British army in 1942 to serve as a liaison on the health of the Kohima and Dimapur Road workers between health officials and the British army.<ref name="Noble"/>{{rp|70}}  Hughes was forced to return to Shillong as fighting intensified and the local hospitals were the main recipients of soldiers from the [[Burma Road]]. From his return until 1945, Hughes was the consulting surgeon at two local military hospitals as well as Senior Medical Officer at the Welsh Mission Hospital.<ref name="Noble"/>{{rp|72}}<ref name="Welsh"/> During this time he treated 2,851 patients, many of whom were British officers and soldiers.<ref name="Vehicles"/>{{rp|67}}

After the War, Hughes turned his attention back to the Welsh Mission Hospital. Hughes constructed and installed the first central heating system in northeastern India to heat the hospital, and also created a steam dryer and [[cooking range]].<ref name="Gwalia"/>{{rp|308}} Due to the lack of a missionary colleague, Hughes began medical education programs for local men and women.<ref name="Vehicles"/>{{rp|67}}<ref name="Call"/>

In 1947, Hughes began a travelling dispensary to make weekly visits to marketplaces around Shillong to distribute medical care and health education, which he would do for 20 years.<ref name="Call"/> He used this time to survey the health of the villages, and turned his attention towards maternal and infant health.<ref name="Vehicles"/>{{rp|68}}<ref name="Christianity"/>{{rp|210}}<ref name="Call"/> Hughes developed [[midwifery]] training for local women and a maternity unit in the Mission Hospital to improve health.<ref name="Noble"/>{{rp|94}} He also became involved in advocacy, joining different social, medical, nursing, and welfare organisations with the goal of improving local health.<ref name="Obituary"/> Hughes worked at the Welsh Mission Hospital until 1969, when he and Nancy returned to Wales.

==Religious involvement in the Welsh Presbyterian Church==
While in India in 1944, Hughes was elected an [[elder (Christianity)|elder]] in the Shillong branch of the Presbyterian Church, with his main responsibility being religious education.<ref name="Welsh"/> When he began his travelling dispensary in 1947, Hughes intended to deliver medical care as well as evangelical [[sermon]]s.<ref name="Noble"/>{{rp|86}}
In 1971, Hughes was elected an elder by the Presbyterian Church in Liverpool, and was elected [[Moderator of the General Assembly|Moderator]] of the Presbyterian Church of Wales in 1992.<ref name="Noble"/>{{rp|144}} In 1991, the Hughes returned to Shillong to celebrate the 150th anniversary of the arrival of Welsh Presbyterian missionary involvement in northeastern India.<ref name="Vehicles"/>{{rp|68}}

==Legacy==
Hughes is known for performing several operations for the first time in India, such as the lower segment Caesarean section without antibiotics and vagus nerve resection process to alleviate pain from peptic ulcers.<ref name="Gwalia"/>{{rp|308}}<ref name="Noble"/>{{rp|97}}<ref name="Call"/> He also introduced [[ether]] to northeastern Indian hospitals as a form of [[general anaesthesia]], recognised and began treating rickets in the Khasi infant population, and developed India-specific treatments for [[kwashiorkor]], a protein calorie deficiency disorder.<ref name="Christianity"/>{{rp|209}}<ref name="Call"/><ref>{{cite web
|title=Kwashiorkor
|url=http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0002571/
|publisher=PubMed Health
|accessdate=18 December 2012
|author=A.D.A.M., Inc}}</ref> Under Hughes' direction, the Welsh Mission Hospital in 1942 employed more nurses and staff than the rest of the hospitals in Assam combined, as well as performed more surgeries.<ref name="Vehicles"/>{{rp|68}} Hughes began the first [[blood bank]] in Shillong to meet the medical needs of his patients.<ref name="Obituary"/><ref name="Noble"/>{{rp|97}}<ref>{{cite web
|title=History
|url=http://www.robertshosp.org/index_files/Page1961.htm
|publisher=Dr. H. Gordon Roberts Hospital}}</ref> As a result of these achievements, Hughes is known by many as the "Schweitzer of Assam."<ref name="Obituary"/> Hughes died Saturday, 1 June 1996, at the Cardiothoracic Hospital in Liverpool.

==References==
{{reflist}}

==External links==
* [http://www.robertshosp.org/index.htm   Dr. H. Gordon Roberts Hospital] (official site)

{{WAP assignment|course=Wikipedia:USEP/Courses/Medical_Missionaries_to_Community_Partners:_Great_Ideas_in_the_name_of_Public_Health_(Kent_Bream)|university=University of Pennsylvania|term=Fall 2012}}

{{DEFAULTSORT:Hughes, Robert Arthur}}
[[Category:1910 births]]
[[Category:1996 deaths]]
[[Category:Welsh Presbyterian missionaries]]
[[Category:Presbyterian missionaries in India]]