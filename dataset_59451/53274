{{Infobox venue
| stadium_name        = Bryson Field at Boshamer Stadium
| nickname            = The Bosh, The Bosh Pit
| image               = [[File:Boshamer Stadium 2013.jpg|300px|Bryson Field at Cary C. Boshamer Stadium]]
| fullname            = Bryson Field at Cary C. Boshamer Stadium
| location            = 235 Ridge Rd, Chapel Hill, NC 27599-5071, USA
| built               = 1970-1972
| opened              = March 21, 1972
| renovated           = 2007-2009
| owner               = University of North Carolina at Chapel Hill
| operator            = University of North Carolina at Chapel Hill
| surface             = Natural grass
| construction_cost   = $25.5 million (expansion)
| architect           = [[Populous (architects)|Populous]] (expansion)
| structural engineer = LHC Structural Engineers (2008 expansion)
| tenants             = North Carolina Tar Heels (NCAA) 1972-2007, 2009-Present<br />[[Atlantic Coast Conference Baseball Tournament]] (1973, 1975, 1981-83)
| seating_capacity    = 4,100 (standing room up to 5,000)
| dimensions          = Left Field: 335 ft (102.1 m)<br />Left Center Field: 370 ft (112.8 m)<br />Center Field: 400 ft (121.9 m)<br />Right Center Field: 355 ft (108.2 m)<br />Right Field: 340 ft (103.6 m)
}}
{{coord|35|54|23.36|N|79|2|34.71|W|display=title}}

'''Cary C. Boshamer Stadium''' is a [[baseball]] [[stadium]] in [[Chapel Hill, North Carolina]]. It is the home of the [[North Carolina Tar Heels baseball]] team.

==History==
[[File:Boshamer Stadium.jpg|thumb|Boshamer Stadium before 2007]]
The previous home of the Tar Heels had been a multi-use venue called '''Emerson Field''', which sat some 2,400 people. The combination baseball/football field was opened in 1916 and had been named for a university benefactor best known as the inventor of [[Bromo-Seltzer]]. The football team left Emerson for [[Kenan Memorial Stadium]] in 1927. Emerson would continue as the home of the baseball team for another 45 seasons. Its site is now occupied by Davis Library.

Boshamer Stadium first opened on March 21, 1972, near the tail end of the 1972 season.  It is named for Cary C. Boshamer (class of 1917), a textile industrialist from [[Gastonia, North Carolina|Gastonia]] whose donation made the new stadium possible. Although many Tar Heel players and fans speak of the stadium as "the Bosh", apparently the family survivors favor the "Boss-hammer" pronunciation.<ref>http://www.wbtv.com/story/22525874/how-do-you-say-boshamer-unc-may-have-it-wrong</ref> 
 
It has hosted five [[Atlantic Coast Conference Baseball Tournament]]s, in [[1973 Atlantic Coast Conference Baseball Tournament|1973]], [[1975 Atlantic Coast Conference Baseball Tournament|1975]], [[1981 Atlantic Coast Conference Baseball Tournament|1981]], [[1982 Atlantic Coast Conference Baseball Tournament|1982]], and [[1983 Atlantic Coast Conference Baseball Tournament|1983]].  North Carolina won the 1982 and 1983 tournaments.<ref name=guide>{{cite web|title=2012 ACC Baseball Guide |url=http://www.theacc.com/sports/m-basebl/2012-acc-baseball-guide.html |work=TheACC.com |accessdate=29 May 2012 |archiveurl=http://www.webcitation.org/682OROOXc?url=http%3A%2F%2Fwww.theacc.com%2Fsports%2Fm-basebl%2F2012-acc-baseball-guide.html |archivedate=2012-05-30 |deadurl=yes |df= }}</ref>

The Tar Heels' on-field success during the mid-2000s coincided with the decision to rebuild the 35-year-old facility.  Following the 2007 season, the stadium was almost completely demolished and rebuilt. UNC won their final game in the old Boshamer Stadium 9-4 over the [[University of South Carolina]].<ref>{{cite web|url=http://tarheelblue.cstv.com/sports/m-basebl/recaps/061007aaf.html |title=Tar Heels Headed To Omaha, Carolina defeats Gamecocks in Super Regional Championship Game. |author=Joedy McCreery |date=June 10, 2007 |accessdate=April 4, 2011 |archiveurl=http://www.webcitation.org/5xhK76PdC?url=http%3A%2F%2Ftarheelblue.cstv.com%2Fsports%2Fm-basebl%2Frecaps%2F061007aaf.html |archivedate=2011-04-04 |deadurl=yes |df= }}</ref>

===2008 renovations===
Due to extensive renovations to Boshamer, the Tar Heels played their 2008 season at [[USA Baseball National Training Complex]] in nearby [[Cary, North Carolina]].

The rebuilt stadium first opened on February 2, 2009.  At that time, the playing surface was formally renamed Bryson Field in honor of former first baseman Vaughn Bryson and his wife Nancy, both longtime supporters of the baseball program.

The entrance courtyard of the rebuilt stadium is named for the Steinbrenner family, as the result of a $1 million donation by [[New York Yankees]] owner [[George Steinbrenner]], whose granddaughter graduated from UNC.

Since expansion, the stadium has a listed capacity of 4,100, but has standing room for up to 5,000.  Before renovations, it seated 3,000 people from the end of one dugout to the other. Today, seating extends down both the 1st and 3rd base lines.  Sections past the dugout on the 1st base line are now reserved for student seating, nicknamed "The Bosh Pit".

==Attendance==
In 2013, the Tar Heels ranked 32nd among [[List of NCAA Division I baseball programs|Division I baseball programs]] in attendance, averaging 1,998 per home game.<ref name=13att>{{cite web|last=Cutler |first=Tami |title=2013 Division I Baseball Attendance - Final Report |url=http://www.sportswriters.net/ncbwa/news/2013/attendance130611.pdf |work=Sportswriters.net |publisher=NCBWA |accessdate=July 20, 2013 |archiveurl=http://www.webcitation.org/6IGG6kpVy?url=http%3A%2F%2Fwww.sportswriters.net%2Fncbwa%2Fnews%2F2013%2Fattendance130611.pdf |archivedate=July 20, 2013 |date=June 11, 2013 |deadurl=yes |df= }}</ref>

==Dimensions==
The field dimensions are as follows:

*Left Field: 335&nbsp;ft (102.1 m)
*Left Center Field: 370&nbsp;ft (112.8 m)
*Center Field: 400&nbsp;ft (121.9 m)
*Right Center Field: 355&nbsp;ft (108.2 m)
*Right Field: 340&nbsp;ft (103.6 m)

The asymmetry of the field is partly the result of an inward bulge in the fence in right center.

==Photos==
<gallery>
File:BoshamerSteinbrenner.jpg|Entrance courtyard.
File:BoshamerRF.jpg|The right field bulge.
File:Cary C. Boshamer Stadium.JPG|View from left field.
File:UNCbaseball.JPG|Game action against [[University of Maryland, College Park]] on April 25, 2009.
</gallery>

==See also==
* [[List of NCAA Division I baseball venues]]

==References==
{{Reflist}}
*{{cite web |url=http://www.ramsclub.com/ViewArticle.dbml?DB_OEM_ID=3350&KEY=&ATCLID=1450383 |title=Boshamer: A new home for the Heels |accessdate=November 21, 2008 |work= |publisher=The Rams Club |date= }}
*{{cite web |url=http://alumni.unc.edu/article.aspx?sid=3756 |title=Boshamer courtyard to be named For Steinbrenner Family |accessdate=November 21, 2008 |work= |publisher=UNC General Alumni Association |date=April 25, 2006 }}
*{{cite web |url=http://tarheelblue.cstv.com/facilities/unc-boshamar-stadium.html |title=Boshamer Stadium: Home of the Tar Heels |accessdate=November 21, 2008 |work= |publisher=UNC Athletics |date= }}
*{{cite web |url=http://alumni.unc.edu/article.aspx?sid=5426 |title=Boshamer torn down to make room for major expansion, renovation |accessdate=November 21, 2008 |work= |publisher=UNC General Alumni Association |date=October 18, 2007 }}

{{University of North Carolina at Chapel Hill}}
{{North Carolina Tar Heels baseball navbox}}
{{Atlantic Coast Conference baseball venue navbox}}
{{North Carolina NCAA Division I college baseball venue navbox}}
{{Triangle sports venues}}

[[Category:Baseball venues in North Carolina]]
[[Category:College baseball venues in the United States]]
[[Category:North Carolina Tar Heels baseball]]
[[Category:North Carolina Tar Heels sports venues]]