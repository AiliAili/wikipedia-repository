{{Orphan|date=October 2016}}

'''C6orf222''' is a protein that in humans is encoded by the ''C6orf222'' [[gene]] (6p21.31).<ref name=":1">{{cite web|last1=NCBI Protein|title=uncharacterized protein c6orf222|url=http://www.ncbi.nlm.nih.gov/protein/NP_001010903.3|publisher=NCBI|accessdate=23 January 2015}}</ref><ref name=":2">{{cite web|last1=NCBI Gene|title=C6orf222 chromosome 6 open reading frame 222 [ Homo sapiens (human) ]|url=http://www.ncbi.nlm.nih.gov/gene?LinkName=protein_gene&from_uid=285002257|publisher=NCBI|accessdate=23 January 2015}}</ref> C6orf222 is conserved in mammals, birds and reptiles with the most distant ortholog being the [[green sea turtle]],''Chelonia mydas''. The C6orf222 protein contains one mammalian conserved domain: DUF3293. The protein is also predicted to contain a [[BH3 domain]], which has predicted conservation in distant orthologs from the clade Aves.<ref name=":3">{{cite web|last1=NCBI BLAST|title=NP_001010903:uncharacterized protein C6orf222|url=http://blast.ncbi.nlm.nih.gov/Blast.cgi|publisher=NCBI|accessdate=30 January 2015}}</ref><ref name=":4">{{cite journal|last1=DeBartolo|first1=Joe|last2=Taipale|first2=Mikko|last3=Keating|first3=Amy|title=Genome-Wide Prediction and Validation of Peptides That Bind Human Prosurvival Bcl-2 Proteins|journal=PLOS Computational Biology|date=June 26, 2014|doi=10.1371/journal.pcbi.1003693|url=http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003693|accessdate=2 February 2015|volume=10|pages=e1003693}}</ref>

== Gene ==
''C6orf222'' is located on the negative DNA strand of the short arm of chromosome 6 at locus 21.31. The gene is 21,129 base pairs long and extends from base pair 36315757 to 36336977. The gene produces a single transcript that is 3,750 base pairs long.<ref>{{cite web|last1=Ensembl|title=Gene: C6orf222|url=http://useast.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000189325;r=6:36315757-36336885;t=ENST00000437635|publisher=Ensembl|accessdate=10 February 2015}}</ref> There is a unique upstream, in-frame stop codon in the 5’UTR region from base pairs 86-88 of the mRNA.<ref>{{cite web|last1=NCBI Nucleotide|title=Homo sapiens chromosome 6 open reading frame 222 (C6orf222), mRNA|url=http://www.ncbi.nlm.nih.gov/nuccore/NM_001010903.4|publisher=NCBI|accessdate=23 January 2015}}</ref>

=== Gene Neighborhood ===
The gene ''PNPLA1'' is found on the positive strand just upstream of ''C6orf222'' and belongs to the patatin-like phospholipase family. ''PNPLA1'' extends from base pairs 36239766 to 36313955.<ref>{{cite web|last1=NCBI Gene|title=PNPLA1 patatin-like phospholipase domain containing 1 [ Homo sapiens (human) ]|url=http://www.ncbi.nlm.nih.gov/gene/285848|publisher=NCBI|accessdate=30 April 2015}}</ref>  ''ETV7'' is found downstream on the negative strand and extends from base pairs 36354221 to 36387800.<ref>{{cite web|last1=NCBI Gene|title=ETV7 ets variant 7 [ Homo sapiens (human)|url=http://www.ncbi.nlm.nih.gov/gene/51513|publisher=NCBI|accessdate=30 April 2015}}</ref> There are two genes located upstream (''LOC105375036'') and downstream (''LOC105375037'') of ''C6orf222'' on the negative and positive strand, respectively. These genes code for [[Non-coding RNA]].<ref>{{cite web|last1=NCBI Gene|title=LOC105375036 uncharacterized LOC105375036 [ Homo sapiens (human) ]|url=http://www.ncbi.nlm.nih.gov/gene/105375036|publisher=NCBI|accessdate=30 April 2015}}</ref><ref>{{cite web|last1=NCBI Gene|title=LOC105375037 uncharacterized LOC105375037 [ Homo sapiens (human) ]|url=http://www.ncbi.nlm.nih.gov/gene/105375037|publisher=NCBI|accessdate=30 April 2015}}</ref>

=== Gene Expression ===
There are several lines of evidence that suggest the C6orf222 protein is expressed non-ubiquitously in select tissue locations at low levels, including the ascites, bladder, intestine, testes, vagina, lymph nodes and skin. The ascites and bladder displayed the strongest expression with 25 and 66 transcripts per million, respectively.<ref>{{cite web|last1=NCBI UniGene|title=EST Profile: C6orf222|url=http://www.ncbi.nlm.nih.gov/UniGene/ESTProfileViewer.cgi?uglist=Hs.162104|publisher=NCBI|accessdate=25 February 2015}}</ref><ref>{{cite web|last1=The Human Protein Atlas|title=C6orf222|url=http://www.proteinatlas.org/ENSG00000189325-C6orf222/tissue|accessdate=28 April 2015}}</ref>

=== Promoter ===
The [[promoter region]] for C6orf222 is predicted to exist between 36304561 and 36305162 and has a length of 602 base pairs.<ref name=":0">{{cite web|last1=El Dorado|title=Annotation and Analysis: C6orf222|url=https://www.genomatix.de/cgi-bin//eldorado/eldorado.pl?s=d1e63d8725cfd65adae7cab5ad3d9524;SHOW_ANNOTATION=c6orf222promoter_2;ELDORADO_VERSION=E28R1306|publisher=Genomatix|accessdate=15 March 2015}}</ref>

== Protein ==
C6orf222 consists of 652 amino acids,  has a weight of 71.9 kDa and an isoelectric point of 8.70 in humans.<ref name=":1"/><ref name=":14">{{cite web|last1=Subramaniam|first1=Shankar|title=SAPS|url=http://workbench.sdsc.edu/CGI/BW.cgi#!|publisher=SDSC|accessdate=11 February 2015}}</ref><ref>{{cite web|last1=ExPASy|title=Compute pI/MW|url=http://web.expasy.org/cgi-bin/compute_pi/pi_tool|publisher=ExPASy|accessdate=12 March 2015}}</ref>

=== Function ===
While the exact function of C6orf222 remains unknown, there has been evidence for it being involved in apoptosis as a pro-apoptotic protein due to the conserved [[BH3 interacting domain death agonist]] located at the [[C-terminus]] of the protein between amino acid residues 535 to 611 in humans.<ref name=":3"/><ref name=":3"/><ref name=":4"/>

=== Structure ===
C6orf222 contains a single domain of unknown function, DUF3292, found between amino acid residues 38-110. The protein is proline rich but asparagine and tyrosine poor. The charge distribution of the protein is fairly equal in that there are no positive or negative charge clusters sequestered within the protein.<ref name=":14"/>
[[File:C6orf222 Protein.png|thumb|A schematic representation of the conserved domains, nuclear localization signal, and phosphorylated amino acid residues in human C6orf222. The red diamond markers indicate conserved phosphoserine residues and they grey diamond markers indicate conserved phosphothreonine residues.<ref>{{Cite web|url = http://prosite.expasy.org/cgi-bin/prosite/PSImage.cgi?hit=38,110,1_1,DUF3292+535,611,1_3,BH3+142,151,1_4,NLS&site=18,1+31,1+59,1+61,1+65,1+92,1+155,1+226,1+286,1+292,1+297,1+306,1+318,1+340,1+357,1+374,1+387,1+432,1+438,1+450,1+463,1+473,1+485,1+488,1+515,1+562,1+589,1+77,0+95,0+449,0&len=652&hscale=0.8|title = My Domains - Image Creator|date = 9 May 2015|accessdate = 9 May 2015|website = My Domains|publisher = Prosite|last = |first = }}</ref>]]
The secondary structures of the protein were predicted via multiple bioinformatics servers. All four programs predicted extensive disorder in the protein at the N-terminus, ranging from 79% to 88% of the protein. This extent of disorder is consistent with the amino acid composition of the protein and being proline rich. There were several α-helix structures predicted at the C-terminus of the protein and conserved in orthologs of the human protein.<ref name=":14" /><ref>{{cite web|last1=Kelley|first1=Lawrence|last2=Sternberg|first2=Michael|title=Phyre2|url=http://www.sbg.bio.ic.ac.uk/phyre2/html/page.cgi?id=index|publisher=Structural Bioinformatics Group|accessdate=12 April 2015}}</ref><ref>{{cite journal|last1=Källberg|first1=Morten|last2=Wang|first2=Haipeng|last3=Wang|first3=Sheng|last4=Peng|first4=Jian|last5=Wang|first5=Zhiyong|last6=Lu|first6=Hui|last7=Xu|first7=Jinbo|title=Template-based protein structure modeling using the RaptorX web server|journal=Nature Protocols|date=2012|volume=7|pages=1511–1522|url=http://raptorx.uchicago.edu/about/|accessdate=13 April 2015|doi=10.1038/nprot.2012.085|pmid=22814390|pmc=4730388}}</ref><ref>{{cite journal|last1=Rost|first1=B|last2=Yachdav|first2=G|last3=Liu|first3=J|title=The PredictProtein Server|journal=Nucleic Acids Research|date=2004|volume=32|pages=321–326|url=http://ppopen.informatik.tu-muenchen.de/visual_results?req_id=$1$f6HnqoJD$nvZect6knFphD0uoZdJlq1|accessdate=12 April 2015|doi=10.1093/nar/gkh377|pmid=15215403|pmc=441515}}</ref> The protein was predominantly soluble, with a majority of the amino acid residues being exposed.15 The average of hydrophobicity for c6orf222 was -0.968, indicating the soluble nature of the protein.<ref>{{cite journal|last1=Gomi|first1=M|last2=Sonoyama|first2=M|last3=Mitaku|first3=S|title=High performance system for signal peptide prediction: SOSUIsignal|journal=Chem-Bio Info|date=2004|volume=4|pages=142–147|url=http://harrier.nagahama-i-bio.ac.jp/sosui/sosuisignal/sosuisignal.cgi|accessdate=12 April 2015|doi=10.1273/cbij.4.142}}</ref><ref name=":5" />

=== Post-Translational Modifications ===
There is extensive, predicted phosphorylation of C6orf222, with 42 phosphoserines and 7 phosphothreonines being conserved in orthologs of the human C6orf222 protein. These results implicate C6orf222 as being a [[phosphoprotein]]. The protein contains only one nuclear export signal residue, found at 275-L; however, the NES score was rather low at 0.672. Structural analysis of the protein to be sequestered in the nucleus with a 95% probability.<ref name=":20">{{cite web|last1=Nakai|first1=Kenta|last2=Horton|first2=Paul|title=PSORTII|url=http://psort.hgc.jp/cgi-bin/runpsort.pl|accessdate=2 April 2015}}</ref> This prediction is supported by the presence of a [[nuclear localization sequence]] (NLS) found between residues 142-151.<ref name=":5">{{cite journal|last1=Kosugi|first1=S|last2=Hasebe|first2=M|last3=Matsumura|first3=N|last4=Takashima|first4=H|last5=Miyamoto-Simo|first5=E|last6=Tomita|first6=M|last7=Yanagawa|first7=H|title=Six classes of nuclear localization signals specific to different binding grooves of importin α.|journal=Journal of Biological Chemistry|date=2009|volume=284|issue=1|pages=478–485|doi=10.1074/jbc.M807017200|pmid=19001369}}</ref><ref>{{cite web|last1=cNLS Mapper|title=cNLS Mapper Result|url=http://nls-mapper.iab.keio.ac.jp/cgi-bin/NLS_Mapper_y.cgi|accessdate=29 April 2015}}</ref>

=== Protein Interactions ===
Although the bioinformatics programs MINT, STRING and Gene Cards did not reveal any protein interactions with C6orf222.<ref>{{cite web|last1=Gene Cards|title=Chromosome 6 Open Reading Frame 222|url=http://www.genecards.org/cgi-bin/carddisp.pl?gene=C6orf222&search=8344456422fcde9038a3e5859fdd0797|publisher=Gene Cards|accessdate=10 February 2015}}</ref><ref>{{cite journal|last1=Licata|first1=L.|last2=Briganti|first2=L.|last3=Peluso|first3=D.|last4=Perfetto|first4=L.|last5=Iannuccelli|first5=M.|last6=Galeota|first6=E.|last7=Sacco|first7=F.|last8=Palma|first8=A.|last9=Nardozza|first9=A. P.|last10=Santonico|first10=E.|last11=Castagnoli|first11=L.|last12=Cesareni|first12=G.|title=MINT, the molecular interaction database: 2012 update|journal=Nucleic Acids Research|date=16 November 2011|volume=40|issue=D1|pages=D857–D861|doi=10.1093/nar/gkr930|pmid=22096227|pmc=3244991}}<!--|accessdate=8 April 2015--></ref><ref>{{cite journal|last1=Szklarczyk|first1=D.|last2=Franceschini|first2=A.|last3=Wyder|first3=S.|last4=Forslund|first4=K.|last5=Heller|first5=D.|last6=Huerta-Cepas|first6=J.|last7=Simonovic|first7=M.|last8=Roth|first8=A.|last9=Santos|first9=A.|last10=Tsafou|first10=K. P.|last11=Kuhn|first11=M.|last12=Bork|first12=P.|last13=Jensen|first13=L. J.|last14=von Mering|first14=C.|title=STRING v10: protein-protein interaction networks, integrated over the tree of life|journal=Nucleic Acids Research|date=28 October 2014|volume=43|issue=D1|pages=D447–D452|doi=10.1093/nar/gku1003|accessdate=8 April 2015|pmid=25352553|pmc=4383874}}</ref> A predicted BH3 domain in the C6orf222 protein was found to interact with both Bcl-2 and Bcl-xL, implicating C6orf222 as being involved in [[apoptosis]].<ref name=":4"/>

'''Table of Potential Transcription Factor Binding Sites in the Predicted ''C6orf222'' Promoter:'''<ref name=":0" />
{| class="wikitable"
!Transcription Factor
!Start
!End
!Strand
!Matrix Score
!Sequence
|-
|Myeloid zinc finger protein MZF1
|115
|125
|<nowiki>-</nowiki>
|1.000
|gtGGGGaggga
|-
|Heat shock factor 2
|91
|115
|<nowiki>-</nowiki>
|0.953
|aggtacctagaaAGAAaggtcagcg
|-
|NF-κβ
|106
|120
|<nowiki>-</nowiki>
|0.894
|gaGGGAggtacctag
|-
|GATA-binding factor 2
|133
|145
|<nowiki>+</nowiki>
|0.958
|caaaGATAactga
|-
|Pleomorphic adenoma gene 1
|168
|190
|<nowiki>-</nowiki>
|1.000
|taGGGGgagaaagtcgaggtggc
|-
|TF-yin yang 2
|196
|218
|<nowiki>+</nowiki>
|0.839
|catttcCCATtaagtcttgtttt
|-
|RBPJ-κ
|196
|208
|<nowiki>-</nowiki>
|0.951
|ttaaTGGGaaatg
|-
|Human acute myelogenous leukemia factors
|281
|295
|<nowiki>-</nowiki>
|0.834
|actGGGGtttgggt
|-
|Smad3 TF involved in TGF-β signaling
|244
|254
|<nowiki>-</nowiki>
|0.995
|aagGTCTggct
|}

== Homology and Evolution ==

=== Orthologous Space ===
82 organisms have predicted orthologs with ''C6orf222''.<ref name=":2"/> The most distant ortholog is the green sea turtle, which diverged from humans 296 million years ago, indicating ''C6orf222'' developed in reptiles and birds.<ref name=":3"/><ref name=":27"/>

'''Table of ''C6orf222'' Orthologs:'''<ref name=":3" />
{| class="wikitable"
!Scientific Name
!Common Name
!Divergence Date from Humans (MYA) <ref name=":28"/>
!NCBI Protein Acession
!Protein Length (amino acids)
!Sequence Similarity (%)
|-
|''[[Homo sapiens]]''
|Human
|0
|[http://www.ncbi.nlm.nih.gov/protein/NP_001010903.3 NP_001010903.3]
|652
|100
|-
|''[[Gorilla gorilla]]''
|Gorilla
|8.8
|[http://www.ncbi.nlm.nih.gov/protein/XP_004043942.1 XP_004043942.1]
|652
|97
|-
|''[[Callithrix jacchus]]''
|Common marmoset
|42.6
|[http://www.ncbi.nlm.nih.gov/protein/XP_002746521.2 XP_002746521.2]
|678
|81
|-
|''[[Camelus ferus]]''
|Bactrian camel
|94.2
|[http://www.ncbi.nlm.nih.gov/protein/XP_006195372 XP_006195372]
|658
|62
|-
|''[[Physeter catodon]]''
|Sperm whale
|94.2
|[http://www.ncbi.nlm.nih.gov/protein/XP_007115001.1 XP_007115001.1]
|638
|60
|-
|''[[Orcinus orca]]''
|Orca
|94.2
|[http://www.ncbi.nlm.nih.gov/protein/XP_004267852.1 XP_004267852.1]
|639
|60
|-
|''[[Canis lupus familiaris]]''
|Dog
|94.2
|[http://www.ncbi.nlm.nih.gov/protein/XP_850456 XP_850456]
|662
|58
|-
|''[[Mustela putorius furo]]''
|Ferret
|94.2
|[http://www.ncbi.nlm.nih.gov/protein/XP_004770808 XP_004770808]
|664
|57
|-
|''[[Oryctolagus cuniculus]]''
|Rabbit
|92.3
|[http://www.ncbi.nlm.nih.gov/protein/XP_008261475 XP_008261475]
|716
|49
|-
|''[[Mus musculus]]''
|Mouse
|92.3
|[http://www.ncbi.nlm.nih.gov/protein/NP_766038.1 NP_766038.1]
|669
|47
|-
|''[[Haliaeetus leucocephalus]]''
|Bald eagle
|296
|[http://www.ncbi.nlm.nih.gov/protein/XP_010583084.0 XP_010583084.0]
|407
|28
|-
|''[[Fulmarus glacialis]]''
|Northern fulmar
|296
|[http://www.ncbi.nlm.nih.gov/protein/XP_009570924.1 XP_009570924.1]
|435
|28
|-
|''[[Chelonia mydas]]''
|Green sea turtle
|296
|[http://www.ncbi.nlm.nih.gov/protein/XP_007063595.1 XP_007063595.1]
|387
|33
|}

=== Paralogous Space ===
[[File:C6orf222 rate of evolution.png|thumb|350x350px|''C6orf222'' rate of evolution in comparison with the fast evolving ''FAG'' gene and the slow evolving ''CYCS'' gene.]]
There are no predicted paralogs for ''C6orf222'' in both humans and mice.<ref name=":3" />

=== Conserved Regions ===
Multiple sequence alignments demonstrated amino acid reside conservation throughout the C6orf222 protein in a variety of orthologs, with the most extensive conservation being found at the [[C-terminus]] of the protein.The [[BH3 interacting-domain death agonist]] (BID) was found to be conserved at the C-terminus in a multiple sequence alignment in both strict and distant orthologs.<ref name=":27">{{cite web|last1=Subramaniam|first1=Shankar|title=CLUSTALW|url=http://workbench.sdsc.edu/CGI/BW.cgi#!|publisher=SDSC|accessdate=1 May 2015}}</ref> The equal distribution of positively and negatively charged amino acid residues is found to be conserved throughout orthologs of the human C6orf222 protein.<ref name=":20" />

=== Evolution ===
''C6orf222'' is a fast evolving gene, similar in nature to [[Fibrinogen alpha chain]] ''FGA''. This evolutionary trend is in contrast to [[Cytochrome c]], which has been shown to be a slow evolving gene.<ref name=":28">{{cite journal|last1=Kumar|first1=S|last2=Hedges|first2=S|last3=Dudley|first3=J|title=TimeTree: a public knowledge-base of divergence times among organisms.|journal=Bioinformatics|date=2006|volume=22|pages=2971–2972|url=http://www.timetree.org/|accessdate=1 May 2015|doi=10.1093/bioinformatics/btl505|pmid=17021158}}</ref>

==References==
{{reflist}}

*
*
*
*

[[Category:Human proteins]]