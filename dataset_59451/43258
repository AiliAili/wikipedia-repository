{{Use mdy dates|date=December 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=XV-11 Marvel
 | image=XV-11A Marvel.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=STOL research aircraft
 | national origin=United States of America
 | manufacturer=[[Mississippi State University]]
 | designer=
 | first flight=December 1, 1965
 | introduced=
 | retired=
 | status=
 | primary user= [[United States Army]]
 | more users= 
 | produced= 
 | number built=1
 | program cost= 
 | unit cost= 
 | developed from= [[AZ-1 Marvelette]]
 | variants with their own articles=
 | developed into=
}}
|}
The '''[[Mississippi State University]] XV-11A Marvel''' was an [[Experimental aircraft|experimental]] American [[STOL]] research aircraft of the 1960s. The MARVEL '''(Mississippi Aerophysics Research Vehicle with Extended Latitude)''' was a single-engined [[pusher configuration|pusher]] [[monoplane]] fitted with a [[boundary layer control]] system. The first all-composite aircraft, it carried out its initial program of research on behalf of the [[US Army]] in the late 1960s, and was rebuilt in the 1980s as a proof-of-concept for a utility aircraft.

==Development and design==
The Department of Astrophysics and Aerospace Engineering at the Mississippi State University had been involved in a program of research into [[boundary layer control]] on behalf of the [[Office of Naval Research]] and the [[US Army]] since the early 1950s, carrying out trials on a modified [[Schweizer SGS 2-12|Schweizer TG-3]] glider, a [[Piper L-21]] and a [[Cessna O-1]].<ref name="Janes 69 p388-9"/><ref name="Matt">Matt, Paul R. "[http://www.angelfire.com/ks2/janowski/other_aircraft/AG14/Marvelette.html Anderson Greenwood AG-14 and the MARVEL program]". Retrieved December 31, 2009.</ref>  Based on the results of these studies, the US Army awarded the Department a contract to develop a new STOL research aircraft the '''XV-11 MARVEL''' ("Mississippi Aerophysics Research Vehicle, Extended Latitude").<ref name="Raspet">"[http://www.ae.msstate.edu/rfrl/pages/marvel.html History: XV-11A Marvel]". Department of Aerospace Engineering, ''[[Mississippi State University]]''. Retrieved December 31, 2009.</ref>

The resultant design was a shoulder-winged [[monoplane]] powered by a single [[Allison T63]] [[turboprop]] engine driving a [[pusher configuration|pusher]] [[ducted propeller]]. The aircraft's structure was made completely of fiberglass, making the Marvel the first example of an all-composite aircraft.<ref>{{cite web|title=Flight lab loaning vintage aircraft|url=http://www.msstate.edu/web/media/detail.php?id=2584|accessdate=February 10, 2011}}</ref> The boundary layer control system used a blower driven by the engine to draw suction through more than one million tiny holes in the wings and fuselage, while instead of conventional [[Flap (aircraft)|flaps]] the Marvel used a form of [[wing warping]] to deflect the wing trailing edges to vary the wing's [[Camber (aerodynamics)|camber]].<ref name="Janes 69 p388-9"/> The aircraft's tail surfaces were attached to the duct around the propeller, extending behind the duct. The undercarriage, of the so-called "Pantobase" configuration, with tandem wheels fitted within two sprung wooden pontoons, was meant to allow operation from rough surfaces, or even from water.<ref name="Raspet"/><ref name="Heyman p52">Heyman 1990, p.52.</ref><ref name="Harding army">Harding 1990, pp. 184–185.</ref>  It was constructed mainly of [[fiberglass]], with steel used for reinforcement and as heat shields around the engine.<ref name="Heyman p51">Heyman 1990, p.51.</ref>

==Operational history==
[[File:XV-11 Marvel 2000.jpg|right|thumb|The XV-11 Marvel on display in August 2000]]
The aircraft's wing and ducted propeller were tested on a piston-engined test bed, the [[AZ-1 Marvelette|XAZ-1 Marvelette]].<ref name="Matt"/><ref name="Heyman p50-1">Heyman 1990, pp. 50–51.</ref>  The full-sized Marvel was built by the Parsons Corporation based at [[Traverse City]], [[Michigan]],<ref name="Janes 69 p388-9"/> making its first flight on December 1, 1965.<ref name="Heyman p52"/> It successfully completed a 100-hour flying program for the US Army in 1969, where it demonstrated good STOL performance, taking off within 125&nbsp;ft (38&nbsp;m), and although the boundary layer control system was not as effective as hoped, it still provided a large amount of test data. Following the completion of the US Army testing in 1969, it was sent to storage awaiting further tests.<ref name="Raspet"/><ref name="Harding army"/><ref name="Heyman p52-3">Heyman 1990, pp. 52–53.</ref>

The Marvel was brought out of storage as a proof-of-concept demonstrator of a STOL utility aircraft for [[Saudi Arabia]]. It was fitted with a more powerful (420&nbsp;hp (313&nbsp;kW)) engine and longer span (36&nbsp;ft 10½&nbsp;in (11.24&nbsp;m) wings, first flying in this form as the Marvel II on August 17, 1982. After initial tests in the United States, the Marvel II was shipped to [[Ta’if]], Saudi Arabia, carrying out a short test program which showed that its landing gear was unsuitable for operating from soft sand and it was still underpowered, after which it returned to Mississippi.<ref name="Raspet"/><ref name="Heyman p53">Heyman 1990, p.53.</ref>

In 2000, the aircraft was displayed at [[EAA Airventure]] promoting Mississippi State Department of Aerospace Engineering. In 2004, the XV-11A was donated to the [[Southern Museum of Flight]] at the [[Birmingham-Shuttlesworth International Airport|Birmingham International Airport]] in [[Alabama]].<ref>{{cite web|title=Flight lab loaning vintage aircraft|url=http://www.msstate.edu/web/media/detail.php?id=2584|accessdate=Feb 10, 2011}}</ref>

==Specifications (XV-11A) ==
{{Aircraft specs
|ref=Jane's All The World's Aircraft 1969–70<ref name="Janes 69 p388-9">Taylor 1969, pp. 388–389.</ref><!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=2
|capacity=2 passengers<ref name="Heyman p51">Heyman 1990, p.51.</ref>
|length m=
|length ft=23
|length in=3.75
|length note=
|span m=
|span ft=26
|span in=2.5
|span note=
|height m=
|height ft=8
|height in=8.25
|height note=
|wing area sqm=
|wing area sqft=106
|wing area note=
|aspect ratio=6.48
|airfoil=NACA 63615 (modified)
|empty weight kg=
|empty weight lb=1958
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=2620
|max takeoff weight note=
|fuel capacity=
|more general=

<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Allison Model 250|Allison T63]]-A-5A
|eng1 type=[[turboprop]]
|eng1 kw=
|eng1 shp=316
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=Aeroproducts Model 272 ducted fan
|prop dia m=
|prop dia ft=5
|prop dia in=6
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=225
|max speed kts=
|max speed note=at {{convert|15000|ft}}
|cruise speed kmh=
|cruise speed mph=184
|cruise speed kts=
|cruise speed note=range cruise at {{convert|15000|ft}}
|stall speed kmh=
|stall speed mph=60
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=287
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=265
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=15000
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=1880
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=

|avionics=
}}

==See also==
{{aircontent
|see also=
|related=
*[[AZ-1 Marvelette]]
|similar aircraft=
*[[Edgley Optica]]
*[[RFB Fantrainer]]
|lists=
*[[List of experimental aircraft]]
*[[List of military aircraft of the United States]]
}}

==References==
;Citations
{{reflist|2}}

;Bibliography
{{commons category|Mississippi State University XV-11 Marvel}}
{{refbegin}}
* Harding, Stephen. ''U.S. Army Aircraft Since 1947''. Shrewsbury, UK: Airlife Publishing, 1990. ISBN 1-85310-102-8.
* Heyman, Jos. "The Mississippi Marvel". ''[[Air Enthusiast]]'', Forty-one, Midsummer 1990. Bromley, UK: Tri-Service Press. ISSN 0143-5450. pp.&nbsp;49–53.
* [[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1969–70''. London:Jane's Yearbooks, 1969.
{{refend}}

{{US STOL and VTOL aircraft}}

[[Category:Ducted fan-powered aircraft]]
[[Category:Mississippi State University aircraft|V-11 Marvel]]
[[Category:United States experimental aircraft 1960–1969]]
[[Category:Pusher aircraft]]
[[Category:Individual aircraft]]