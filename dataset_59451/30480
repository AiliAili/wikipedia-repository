{{featured article}}
{{Infobox pro wrestling championship
|championshipname=CMLL World Light Heavyweight Championships
| image =LaMascaraLuchador.jpg
| image_size =200px
| caption =[[La Máscara (wrestler)|La Máscara]], the current and fifteenth CMLL World Light Heavyweight Champion
| alt=A picture of the masked wrestler posing on the ring ropes before a match
| currentholder=[[La Máscara (wrestler)|La Máscara]]
| won=April 8, 2016<ref name=LaMascara/>
| promotion=[[Consejo Mundial de Lucha Libre]]
| created=September 26, 1991<ref name=LHeavyBook/>
| mostreigns= [[Dr. Wagner Jr.]] ([[CMLL World Light Heavyweight Championship#Title History|2 times]])<br>[[Atlantis (wrestler)|Atlantis]] ([[CMLL World Light Heavyweight Championship#Title History|2 times]])
| firstchamp=[[Jerry Estrada]]
| longestreign=[[Último Guerrero]] ({{age in years and days|December 13, 2002|July 14, 2006}})
| shortestreign=Jerry Estrada (175 days)
| oldest=[[Villano III]] ({{age in years and days|March 23, 1952|November 22, 1991}})
| youngest=[[Rush (wrestler)|Rush]] ({{age in years and days|September 29, 1988|February 22, 2012}})
| lightest=[[Jerry Estrada]] ({{convert|87|kg|lb|abbr=on}})<!--In Mexico they measure weight in kilograms so that should be first -->
| heaviest= [[Rey Escorpión]] ({{convert|92|kg|lb|abbr=on}})<!--In Mexico they measure weight in kilograms so that should be first -->
}}
The '''CMLL World Light Heavyweight Championship''' (''Campeonato Mundial de Peso Semicompleto del CMLL'' in [[Spanish language|Spanish]]) is a [[professional wrestling]] [[Professional wrestling championship#World championships|world heavyweight championship]] promoted by [[Consejo Mundial de Lucha Libre]] (CMLL) since 1991. As it is a professional wrestling championship, it is not won via legitimate competition; it is instead won via a scripted ending to a match or on occasion awarded to a wrestler because of a storyline.<ref name=Kayfabe>{{cite web|url=http://entertainment.howstuffworks.com/pro-wrestling.htm |title=How Pro Wrestling Works |first=Ed |last=Grabianowski |work=How Stuff Works |accessdate=April 5, 2009 |deadurl=yes |archiveurl=http://www.webcitation.org/6LDla2zy5?url=http%3A%2F%2Fentertainment.howstuffworks.com%2Fpro-wrestling.htm |archivedate=2013-11-18 |df= }}</ref><ref>{{cite book|last1=Mazer|first1=Sharon|title=Professional Wrestling: Sport and Spectacle|date=February 1, 1998|publisher=University Press of Mississippi|isbn=1-57806-021-4|pages=18–19|quote=[http://www.webcitation.org/6iNSeiNUp?url=http://oi64.tinypic.com/2qn63o1.jpg page 18] / [http://www.webcitation.org/6iNS9iCe0?url=http://oi66.tinypic.com/fymveo.jpg page 19]|url=https://books.google.com/books?id=elWutuNtfUMC|accessdate=June 19, 2016}}</ref> The official definition of the [[Professional wrestling weight classes#Mexico|light heavyweight division]] in Mexico is between {{convert|92|kg|lb|abbr=on}} and {{convert|97|kg|lb|abbr=on}}, but the weight limits are not always strictly adhered to. Because CMLL puts more emphasis on the lower weight classes, this division is considered more important than the heavyweight division, which is considered the most important championship by most [[professional wrestling promotion|promotions]] outside of Mexico.

The current CMLL World Light Heavyweight Champion in his first reign is [[La Máscara (wrestler)|La Máscara]], who won it by defeating [[Ángel de Oro]] on April 8, 2016. La Máscara is the 15th overall champion and the 13th wrestler to officially hold the championship. The title has been [[wikt:vacate|vacated]] only once since its creation in 1991, and has had one unofficial reign.

==History==
The Mexican [[professional wrestling promotion]] [[Empresa Mexicana de Lucha Libre]] (EMLL) was founded in 1933 and initially recognized a series of "Mexican National" wrestling championships, endorsed by the ''Comisión de Box y Lucha Libre Mexico D.F.'' (Mexico City Boxing and Wrestling [[Commissioner#Sports|Commission]]).<ref name=MexChampMag/> The [[Mexican National Light Heavyweight Championship]] was created in 1942 as EMLL began promoting matches for that championship with the approval and oversight of the wrestling commission.<ref name=MexChampMag/><ref>{{cite book|first1=Royal |last1=Duncan |first2=Gary |last2=Will|title=Wrestling Title Histories|chapter=Mexico: National Heavyweight Title|pages=390–391|publisher=Archeus Communications|year=2000|isbn=0-9698161-5-4|url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref> In the 1950s EMLL became a member of the [[National Wrestling Alliance]] (NWA) and began promoting the [[NWA World Light Heavyweight Championship]] in the late 1950s. Previously that championship had been promoted in the US, but the NWA gave EMLL full control of the championship in 1958, positioning the NWA title as the highest-ranking title in the light heavyweight division with the Mexican National title positioned as the secondary championship.<ref name=MexChampMag>{{cite news|title=Los Reyes de Mexico: La Historia de Los Campeonatos Nacionales|work=Lucha 2000|id=Especial 21|date=December 20, 2004|language=Spanish}}</ref><ref name=NWA>{{cite book|title=National Wrestling Alliance: The Untold Story of the Monopoly That Strangled Pro Wrestling|author=Hornbaker, Tim|publisher=ECW Press|year=2006|isbn=1-55022-741-6|chapter=International Expansion|page=305|url=https://books.google.com/?id=npQBhSTtvCsC&pg=PT330&dq=EMLL+member+of+the+%22national+Wrestling+Alliance%22#v=onepage&q=EMLL%20member%20of%20the%20%22national%20Wrestling%20Alliance%22&f=false}}</ref>

EMLL left the NWA In the late 1980s to avoid the politics of the NWA, and later rebranded themselves as "Consejo Mundal de Lucha Libre" (CMLL).<ref name=Mondo>{{cite book|author=Madigan, Dan|title=Mondo Lucha A Go-Go: the bizarre & honorable world of wild Mexican wrestling|publisher=HarperCollins Publisher|year=2007|chapter=Okay&nbsp;... what is Lucha Libre?|pages=29–40 and 114–118|isbn=978-0-06-085583-3|url=https://books.google.com/?id=jtUt0rKjsZkC}}</ref> In 1991 CMLL decided to create a series of CMLL-branded world championships, with the CMLL World Light Heavyweight Championship (''Campeonato Mundial Semicompleto de CMLL'' in [[Spanish language|Spanish]]) created as the second CMLL-branded championship, after the [[CMLL World Heavyweight Championship]].<ref name=CMLLTitles>{{cite book |first1=Royal |last1=Duncan |first2=Gary |last2=Will |title=Wrestling Title Histories | publisher=Archeus Communications |chapter = Mexico: EMLL CMLL| pages = 395–410 |year= 2000 |isbn=0-9698161-5-4 | url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref> [[Jerry Estrada]] was chosen as the first champion, with CMLL booking the tournament for the championship to end with Estrada defeating [[Pierroth Jr.]]<ref name=LHeavyBook>{{cite book |first1=Royal |last1=Duncan |first2=Gary |last2=Will | title=Wrestling Title Histories | publisher=Archeus Communications | chapter = Mexico: CMLL EMLL Light Heavyweight Title | page = 395 | year= 2000 | isbn=0-9698161-5-4 | url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref> In 1996 the then-champion [[Dr. Wagner Jr.]] lost the championship to [[Yoshihiro Tajiri|Aquarius]] on a show in Japan, but the title change was not approved by CMLL and thus was never officially recognized. Dr. Wagner Jr. won the title back eight days later before returning to Mexico.<ref name=LHeavyBook/><ref name=YIR1996>{{cite news|title=1996 Especial!|work=Box y Lucha Magazine|pages=2–28|id=issue 2280|date=January 10, 1997|language=Spanish}}</ref> Since the title change was not officially recognized, CMLL considers Dr. Wagner Jr. a two-time champion, not a three-time champion.<ref name=LHeavyBook/>

On January 15, 2013, then-reigning champion [[Rush (wrestler)|Rush]] voluntarily gave up the CMLL World Light Heavyweight Championship as part of his ongoing [[feud (professional wrestling)|storyline feud]] with then-CMLL World Heavyweight Champion [[El Terrible]]. As part of the storyline, El Terrible stated that he would not defend against someone who represented a lower weight class, so Rush moved into the Heavyweight division for a title match.<ref name=RushVacated>{{cite web | url=http://www.mediotiempo.com/mas-deportes/lucha-libre/noticias/2013/01/16/rush-renuncio-al-campeonato-semicompleto-del-cmll | title= Rush renunció al Campeonato Semicompleto del CMLL | date=January 16, 2013 | accessdate=January 16, 2013 | work=Medio Tiempo | language=Spanish}}</ref> CMLL held a 16-man ''[[torneo cibernetico]]'' elimination match to determine which two wrestlers should compete in the finals for the vacant title. On January 29, 2013, [[Rey Escorpión]] defeated [[Volador Jr.]] in the tournament finals to become the 13th overall champion.<ref name=ReyEscorpion/>

==Reigns==
[[File:Dr. Wagner, Jr. Latin American Champion.jpg|left||thumb|150px|[[Dr. Wagner Jr.]], who lost the championship in Japan without it being sanctioned by CMLL|alt=The masked Dr. Wagner Jr. walking to the ring, holding a professional wrestling championship belt in his hands]]
{{main article|List of CMLL World Light Heavyweight Champions}}

[[La Máscara (wrestler)|La Máscara]] is the current CMLL World Light Heavyweight Champion in his first reign, since defeating [[Ángel de Oro]] on April 8, 2016.<ref name=LaMascara>{{cite web|url=http://cmll.com/?p=1788 |archiveurl=https://web.archive.org/web/20160404202904/http://cmll.com/?p=1788 |title=Resultados Arena México – Viernes Espectaculares: Viernes 8 de Abril '16 |last=Salazar |first=Alexis |date=April 8, 2016 |archivedate=April 4, 2016 |accessdate=April 9, 2016 |work=[[Consejo Mundial de Lucha Libre]] |language=Spanish |deadurl=yes |df= }}</ref> Overall, there have been 15 reigns shared between 13 wrestlers, which does not include one unofficial reign by Aquarius. Only two men have held the title more than once; both Dr. Wagner Jr. and [[Atlantis (wrestler)|Atlantis]] have officially held the title twice. Dr. Wagner Jr. has the longest combined reigns with 1,574 days, and [[Último Guerrero]] holds the record for the longest individual reign with {{age in years and days|December 13, 2002|July 14, 2006}}. Because Aquarius' eight-day reign in 1996 is not officially recognized by CMLL, Jerry Estrada's 175-day reign is the shortest in the history of the championship.<ref name=LHeavyBook/> Not only was Último Guerrero's reign the longest of any individual reign, he is also credited with a record 28 successful championship defenses.<ref name=Lucha2006>{{cite news|title=Lo Mejor de la Lucha Libre Mexicana durante el 2006|work=SuperLuchas|id=Issue 192|date=December 23, 2006|url=http://superluchas.com/2006/12/23/super-luchas-193/|accessdate=July 11, 2009|language=Spanish}}</ref>

On one occasion CMLL declared the championship vacant, which meant that there was no champion for a period of time. Sometimes, a championship is vacated due to an injury to the reigning champion,<ref name=CMLLPress>{{cite web | url=http://superluchas.com/2014/11/19/cmll-conferencia-de-prensa-de-infierno-en-el-ring/ | title=CMLL: Conferencia de prensa de Infierno en el Ring | date=November 19, 2014 | accessdate=November 19, 2014 | author=Angelita | publisher=SuperLuchas Magazine | language=Spanish}}</ref> or when a champion stops working for the promotion,<ref name=Canoe2000>{{cite web | url=http://www.canoe.com/SlamWrestlingInternational/mexico_timeline-can.html | title=SLAM! Wrestling International -- 2000: The Year-In-Review Mexico | accessdate=July 31, 2015 | work=Slam Wrestling! | publisher=Canoe.ca}}</ref> but in the case of the CMLL World Light Heavyweight Championship, there was a storyline reason behind it being declared vacant. In late 2013 the light heavyweight champion Rush was working a long-running storyline rivalry with El Terrible. When El Terrible won the CMLL World Heavyweight Championship, CMLL decided to enhance the rivalry by having Rush voluntarily give up the light heavyweight championship in order to receive a CMLL World Heavyweight Championship match against El Terrible.<ref name=RushVacated/> This allowed CMLL to advance the storyline as well as move the championship off Rush without having Rush lose a match, allowing Rey Escorpión to become the next champion.<ref name=ReyEscorpion/>

==Rules==
{{See also|Professional wrestling championship}}

The official definition by the Mexican ''lucha libre'' commission for the [[Professional wrestling weight classes#Mexico|light heavyweight division]] in Mexico is between {{convert|92|kg|lb|abbr=on}} and {{convert|97|kg|lb|abbr=on}}.<ref name="WeightDivision">{{cite web| url= http://www.ordenjuridico.gob.mx/Estatal/ESTADO%20DE%20MEXICO/Reglamentos/MEXREG004.pdf | title= Reglamento de Box y Lucha Libre Professional del Estado de México | publisher= Comisión de Box y Lucha Libre Mexico D.F. | date= August 30, 2001 | accessdate= April 3, 2009 | quote= Articulo 242: "Super medio 92 kilos / Semi Completo 97 kilos"|archiveurl=https://web.archive.org/web/20061130181418/http://www.ordenjuridico.gob.mx/Estatal/ESTADO%20DE%20MEXICO/Reglamentos/MEXREG004.pdf|archivedate=November 30, 2006|language=Spanish}}</ref> In the 20th century, CMLL were generally consistent and strict about enforcing the actual weight limits.<ref name=CMLLTitles/> In the 21st century the official definitions have at times been overlooked for certain champions. One example of this was when [[Mephisto (wrestler)|Mephisto]], officially listed as {{convert|90|kg|lb|abbr=on}}, won the [[CMLL World Welterweight Championship]], a weight class with a {{convert|78|kg|lb|abbr=on}} upper limit.<ref name="WeightDivision"/><ref name="SL91">{{cite news|title=Número Especial – Lo mejor de la lucha ilbre mexicana durante el 2004|work=SuperLuchas|id=91|date=January 24, 2005|language=Spanish}}</ref> Although the heavyweight championship is traditionally considered the most prestigious weight division in professional wrestling, CMLL places more emphasis on the lower weight divisions, often promoting those ahead of the CMLL World Heavyweight Championship.<ref name="Mondo"/>

With [[Consejo Mundial de Lucha Libre#Championships|twelve CMLL-promoted championships labelled as "World" titles]], the promotional focus shifts from championship to championship over time with no single championship consistently promoted as the "main" championship; instead CMLL's various major shows feature different weight divisions and are most often headlined by a ''[[Lucha de Apuestas]]'' ("Bet match") instead of a championship match.<ref name="Mondo"/> From 2013 until June 2016 only two major CMLL shows have featured championship matches: ''[[Sin Salida (2013)|Sin Salida]]'' in 2013, and the 2014 ''[[Juicio Final (2014)|Juicio Final]]'' show featuring the [[NWA World Historic Welterweight Championship]].<ref name=Medio1234>{{cite web | url=http://www.mediotiempo.com/mas-deportes/lucha-libre/noticias/2014/08/02/cayo-la-cabellera-de-princesa-blanca-y-la-mascara-de-seductora | title=Cayó la cabellera de Princesa Blanca y la máscara de Seductora | first=Apolo | last=Valdés | work=MedioTiempo | date=August 2, 2014 | accessdate=August 2, 2014 | language=Spanish}}</ref><ref name=SLNWA>{{cite web | url=http://superluchas.com/2013/06/02/mascara-dorada-nuevo-campeon-mundial-historico-nwa-peso-welter/ | title=Mascara Dorada nuevo campeón mundial histórico NWA peso welter | date=June 2, 2013 | accessdate=June 3, 2013 | work=SuperLuchas.net | publisher=SuperLuchas Magazine | language=Spanish}}</ref> The last time a CMLL World Light Heavyweight Championship match was featured on a major CMLL show was on September 18, 2004, when Último Guerrero successfully defended the title at the [[CMLL 71st Anniversary Show]].<ref>{{Cite news | author = Ocampo, Jorge | title = Anniversario 71 de CMLL | work =SuperLuchas | id = issue 72 | date = September 26, 2004| language = Spanish}}</ref> Championship matches usually take place under [[Professional wrestling match types#Series variations|best two-out-of-three falls rules]].<ref name="Mondo"/> On occasion single fall title matches have taken place, especially when promoting CMLL title matches in Japan, conforming to the traditions of the local promotion.<ref group=Note>An example of this was [[Tetsuya Bushi|Bushi]] winning the [[CMLL World Welterweight Championship]] in a one-fall match on a [[New Japan Pro Wrestling]] show.</ref><ref>{{cite web | url = http://www.njpw.co.jp/match/detail_result.php?e=1072 | title = Road to Tokyo Dome | accessdate=December 19, 2015 | work=[[New Japan Pro Wrestling]] | language=Japanesee | archivedate=December 19, 2015 | archiveurl=https://web.archive.org/web/20151219222634/http://www.njpw.co.jp/match/detail_result.php?e=1072}}</ref> Outside of CMLL, the light heavyweight championship has been defended on joint CMLL/[[Universal Wrestling Association]] (UWA) shows in 1993, on [[W*ING]], [[Big Japan Wrestling]], [[Dragondoor]] and [[New Japan Pro Wrestling]] shows in Japan.<ref name=LHeavyBook/><ref>{{cite web | url = http://superluchas.com/2014/01/17/njpwcmll-resultados-fantasticamania-2014-dia-3-17012014/| title = NJPW/CMLL: Resultados "Fantasticamania 2014" – Día 3 – 17/01/2014 | author=Dark Angelita | date=January 17, 2014 | accessdate=January 18, 2014 | work=SuperLuchas | language=Spanish}}</ref><ref name=LuchaReport011913>{{cite web | url = http://www.pwinsider.com/article/82932/legendary-lucha-venue-closing-and-more-the-lucha-report.html?p=1 | title = Legendary lucha venue closing and more: The Lucha Report | last=Zellner | first=Kris | date=January 19, 2014 | accessdate=January 19, 2014 | work=Pro Wrestling Insider}}</ref><ref>{{cite web | accessdate=July 16, 2016 | url=http://www.njpw.co.jp/english/match/card.php?e=803 | title=NJPW Presents CMLL Fantastica Mania 2014 | publisher=New Japan Pro Wrestling | archivedate=August 16, 2016 | archiveurl=https://web.archive.org/web/20160816000515/http://www.njpw.co.jp/english/match/card.php?e=803}}</ref>

==Tournaments==
===1991===
CMLL held a 16-man tournament from September 15 to October 26, 1991 to crown the first light heavyweight champion. Documentation on Pierroth Jr.'s path to the finals has not been found.<ref name=LHeavyBook/>

;Tournament brackets<ref name=LHeavyBook/><ref name=YIR1991>{{cite news|title=1991 Especial!|work=Box y Lucha Magazine|pages=2–28|id=issue 2020|date=January 9, 1992|language=Spanish}}</ref>
{{16TeamBracket-Compact-NoSeeds
| RD1=First round| RD2=Semifinals| RD3=Final
| RD1-team01='''[[Jerry Estrada]]'''
| RD1-team02=[[MS-1 (wrestler)|MS-1]]
| RD1-score01='''W'''
| RD1-score02=&nbsp;
| RD1-team03=[[Masakre]]
| RD1-team04='''[[Mascara Año 2000]]'''
| RD1-score03=&nbsp;
| RD1-score04='''W'''
| RD1-team05='''[[El Satánico]]'''
| RD1-team06=[[El Hijo del Solitario]]
| RD1-score05='''W'''
| RD1-score06=&nbsp;
| RD1-team07='''[[Norman Smiley|Black Magic]]'''
| RD1-team08= [[Universo 2000]]
| RD1-score07='''W'''
| RD1-score08=&nbsp;
| RD1-team09='''[[Pierroth Jr.]]'''
| RD1-team10=''Unknown''
| RD1-score09='''W'''
| RD1-score10=&nbsp;
| RD1-team11=''Unknown''
| RD1-team12=''Unknown''
| RD1-score11=&nbsp;
| RD1-score12=&nbsp;
| RD1-team13=''Unknown''
| RD1-team14=''Unknown''
| RD1-score13=&nbsp;
| RD1-score14=&nbsp;
| RD1-team15=''Unknown''
| RD1-team16=''Unknown''
| RD1-score15=&nbsp;
| RD1-score16=&nbsp;
| RD2-team01='''Jerry Estrada'''
| RD2-team02=Mascara Año 2000
| RD2-score01='''W'''
| RD2-score02=&nbsp;
| RD2-team03=Satanico
| RD2-team04='''Black Magic'''
| RD2-score03=&nbsp;
| RD2-score04='''W'''
| RD2-team05='''Pierroth Jr.'''
| RD2-team06=''Unknown''
| RD2-score05='''W'''
| RD2-score06=&nbsp;
| RD2-team07=''Unknown''
| RD2-team08=''Unknown''
| RD2-score07=&nbsp;
| RD2-score08=&nbsp;
| RD3-team01='''Jerry Estrada'''
| RD3-team02=Black Magic
| RD3-score01='''W'''
| RD3-score02=&nbsp;
| RD3-team03='''Pierroth Jr.'''
| RD3-team04=''Unknown''
| RD3-score03='''W'''
| RD3-score04=&nbsp;
| RD4-team01='''Jerry Estrada'''
| RD4-team02=Pierroth Jr.
| RD4-score01='''W'''
| RD4-score02=&nbsp;
}}

===2013===
[[File:PsicosisandDelta.jpg|thumb|[[Psicosis II|Psicosis]] (behind, in red) and [[Delta (wrestler)|Delta]] (in front, in white) both competed in the 2013 ''[[torneo cibernetico]]'' qualifying match.|alt=A picture of Delta sitting on the top rope of a wrestling ring, with Psicosis standing behind him tearing at Delta's mask]]
CMLL held a tournament to determine the next CMLL World Light Heavyweight Champion starting on January 22, 2013, and the finals were held the following week. The first round of the tournament was a 16-man ''torneo cibernetico'' elimination match, with the last two men in the match qualifying for the finals the following week on January 29. The ''torneo'' featured two teams of eight wrestling against each other; Team A consisted of [[Delta (wrestler)|Delta]], [[El Felino]], La Máscara, Mephisto, [[Mr. Águila]], Rey Escorpión, [[Stuka Jr.]] and Volador Jr. and Team B consisted of Atlantis, [[Blue Panther]], [[Diamante (wrestler)|Diamante]], [[Euforia (wrestler)|Euforia]], [[Maximo (wrestler)|Maximo]], [[Histeria (wrestler)|Morphosis]], [[Psicosis II|Psicosis]] and [[Sagrado (wrestler)|Sagrado]]. The match came down to Team A's Rey Escorpión and Volado Jr. versus Team B's Euforia. Escorpión pulled off Volador Jr.'s [[wrestling mask|mask]] and threw it to rival Euforia, causing Euforia to be disqualified, which meant that Escorpión and Volador Jr. qualified for the finals the following week.<ref name=CMLLMiddld2013>{{cite web | url=http://superluchas.com/2013/01/22/volador-jr-vs-rey-escorpion-por-el-campeonato-mundial-semicompleto-del-cmll/ | title= Volador Jr. vs. Rey Escorpión por el Campeonato Mundial Semicompleto del CMLL | date=January 22, 2013 | accessdate=January 23, 2013 | publisher=SuperLuchas Magazine| language=Spanish}}</ref> On January 29, Escorpión defeated Volador Jr. in the finals to become the 13th overall CMLL World Light Heavyweight champion.<ref name=ReyEscorpion>{{cite web|url=http://superluchas.com/2013/01/29/rey-escorpion-nuevo-monarca-mundial-semicompleto-del-cmll/|title=Rey Escorpión&nbsp;... ¡nuevo monarca mundial semicompleto del CMLL!|last=Mexicool|first=Rey|date=January 29, 2013|publisher=''SuperLuchas'' Magazine|accessdate=January 30, 2013|language=Spanish}}</ref>

;Cibernetico order of elimination<ref name=CMLLMiddld2013/>
{| class="wikitable"
!scope="col" style="background: #e3e3e3;"|#
!scope="col" style="background: #e3e3e3;"|Eliminated
!scope="col" style="background: #e3e3e3;"|Eliminated by
|-
!scope="row" |1
| Sagrado || Delta
|-
!scope="row" |2
| El Felino || Morphosis
|-
!scope="row" |3
| Atlantis || Rey Escorpión
|-
!scope="row" |4
| Delta || Euforia
|-
!scope="row" |5
| Mephisto || Blue Panther
|-
!scope="row" |6
| Morphosis || Mr. Águila
|-
!scope="row" |7
| Diamante || Stuka Jr.
|-
!scope="row" |8
| Blue Panther || Volador Jr.
|-
!scope="row" |9
| Mr. Águila || Máximo
|-
!scope="row" |10
| Stuka, Jr. || Máximo (double pin)
|-
!scope="row" |{{sort|11|10}}
| Máximo || Stuka (double pin)
|-
!scope="row" |12
| Psicosis || La Máscara
|-
!scope="row" |13
| La Máscara || Volador Jr.
|-
!scope="row" |14
| Euforia || Disqualification
|-
!scope="row" |15
| '''Finalist''' || Volador Jr.
|-
!scope="row" |{{sort|16|15}}
| '''Finalist''' || Rey Escorpión
|}

==Footnotes==
{{reflist|group=Note}}

==References==
{{reflist|30em}}

==External links==
*[http://www.wrestling-titles.com/mexico/emll/cmll-lh.html CMLL World Light Heavyweight title] at Wrestling-titles.com

{{Consejo Mundial de Lucha Libre championships}}
{{CMLL World Light Heavyweight Championship}}

{{DEFAULTSORT:Cmll World Light Heavyweight Championship}}
[[Category:Consejo Mundial de Lucha Libre championships]]
[[Category:Cruiserweight wrestling championships]]