{{Infobox Journal
| title        = Journal of Mental Health
| cover        = 
| editor       = Til Wykes
| discipline   = [[mental health]]
| language     = [[English language|English]]
| abbreviation = [[JMH]]
| publisher    = [[Informa Healthcare]]
| country      = [[United Kingdom|U.K.]]
| frequency    = [[bi-monthly]]
| history      = March 1992-
| openaccess   = No
| license      =
| impact       = 
| impact-year  = 
| website      = http://www.informaworld.com/smpp/title~content=t713432595~db=all
| link1        = 
| link1-name   = 
| link2        = 
| link2-name   = 
| RSS          = http://www.informaworld.com/ampp/rss~content=t713432595
| atom         = 
| JSTOR        = 
| OCLC         = 288963954
| LCCN         = 
| CODEN        = 
| ISSN         = 0963-8237
| eISSN        = 1360-0567  
}}
{{ redirect|JMH }}

'''''Journal of Mental Health''''' is a [[bi-monthly]] journal established in March 1992 by [[Ray Hodgson]] ([[University of Wales]] College of Medicine, Centre of Applied Public Health Medicine, Cardiff).  In 2002, [[Til Wykes]] became the Executive Editor and has continued in that role until the present time.<ref>{{cite journal | title = 'Next steps' on JMH - reform and consolidation | author = Til Wykes | journal = Journal of Mental Health | volume = 11 | issue = 3 |date=June 2002 | pages = 231–234 | doi = 10.1080/09638230120020023615 }}</ref><ref>{{cite news | url = https://www.theguardian.com/society/2003/aug/10/mentalhealth.film | title = Kids' films stigmatise mentally ill | author = Robin McKie | publisher = [[The Observer]] |date = August 10, 2003 }}</ref><ref>{{cite web | url = http://www.informaworld.com/smpp/title~db=jour~content=g781022919~tab=submit~mode=paper_submission_instructions | title = Instructions for Authors | publisher = Informa Healthcare | accessdate = 2008-12-21 }}</ref><ref>{{cite web | url = http://www.shef.ac.uk/psychopathologysymposia/pastevents/2002/wykes.html | title = Speaker biography: Prof T Wykes | publisher = Sheffield Psychopathology Symposia, [[University of Sheffield]] | year = 2002 | accessdate = 2008-12-21 }} {{Dead link|date=October 2010|bot=H3llBot}}</ref>

For the first three years it was published quarterly, with five editions in 1995 and 1996 before settling on a [[bi-monthly]] issue cycle.

The first flyer for the journal stated in 1990 that "we have no intention of
adding to the multitude of lightly thumbed, tenuously relevant and uninteresting journals
accumulating in our libraries and on our bookshelves". Instead, they wanted to publish "work
which will have a direct impact upon our daily clinical practice, which is thought-provoking
and which challenges assumptions and methods in mental health".<ref name="impact">{{cite journal | title = There’s more than one way to have an impact | journal = Journal of Mental Health |date=August 2004 | volume = 13 | issue = 4 | pages = 345–349 | doi = 10.1080/09638230410001729870 | author = Susan Grey, Til Wykes & Tom Craig }}</ref>

The journal was mentioned 82 times in 2003 ''Cases for Change'' document published by [[National Institute for Mental Health in England]].<ref name="impact"/>

== References ==
{{reflist}}

[[Category:Bimonthly journals]]

{{medical-journal-stub}}