{{Use dmy dates|date=July 2015}}
{{Infobox sportsperson
| honorific_prefix =
| name             = Viktor Saneyev
| honorific_suffix =
| image            =Viktor Saneyev c1972.jpg 
| image_size       = <!-- Use only when absolutely necessary -->
| alt              = 
| caption          = Viktor Saneyev c. 1972
| headercolor      =
| textcolor        =
| <!-- Personal information -->
| native_name      =  
| native_name_lang = 
| birth_name       = 
| fullname         = Viktor Danilovich Saneyev<br>Виктор Данилович Санеев<br>ვიქტორ სანეევი<ref name=sports-reference/>
| nickname         = 
| nationality      = 
| ethnicity        = 
| citizenship      = 
| birth_date       = {{birth date and age|df=yes|1945|10|3}}<ref name=britannica/>
| birth_place      = [[Sukhumi]], [[Georgia (country)|Georgia]], USSR.<ref name=britannica/>
| death_date       = <!-- {{death date and age|df=yes|death year|death month|death day|birth year|birth month|birth day}} -->
| death_place      = 
| resting_place    = 
| resting_place_coordinates = 
| monuments        = 
| residence        = 
| education        = 
| alma_mater       = 
| occupation       = 
| years_active     = 
| employer         = 
| agent            = 
| height           = 188 cm<ref name=sports-reference/>
| weight           = 78 kg<ref name=sports-reference/>
| spouse           = 
| life_partner     = 
| other_interests  = 
| website          = <!-- {{URL|www.example.com}} -->
| module           = 
| <!-- Sport -->
| country          = [[Soviet Union]]
| sport            = Athletics
| position         = 
| disability       = 
| disability_class = 
| weight_class     = 
| weight_class_type = 
| rank             = 
| event            = [[Triple jump]]
| event_type       = 
| collegeteam      = 
| universityteam   = 
| league           = 
| league_type      = 
| club             = Dynamo Sukhumi<br>Dynamo Tbilisi
| team             = 
| turnedpro        = 
| turnedpro_type   = 
| partner          = 
| former_partner   = 
| coach            = 
| retired          = 
| coaching         = 
| module2          = 
| <!-- Achievements and titles -->
| worlds           = 
| regionals        = 
| nationals        = 
| olympics         = 
| paralympics      = 
| commonwealth     = 
| highestranking   = 
| pb               = 17.44 m (1972)<ref name=sports-reference/>
| <!-- Medal record -->
| show-medals    = yes
| medaltemplates = 
{{Medal|Country | {{URS}} }}
{{Medal|Competition|[[Olympic Games]]}}
{{Medal|Gold| [[1968 Summer Olympics|1968 Mexico City]]|[[Athletics at the 1968 Summer Olympics – Men's triple jump|Triple jump]]}}
{{Medal|Gold| [[1972 Summer Olympics|1972 Munich]]|[[Athletics at the 1972 Summer Olympics – Men's triple jump|Triple jump]]}}
{{Medal|Gold| [[1976 Summer Olympics|1976 Montreal]]|[[Athletics at the 1976 Summer Olympics – Men's triple jump|Triple jump]]}}
{{Medal|Silver| [[1980 Summer Olympics|1980 Moscow]]|[[Athletics at the 1980 Summer Olympics – Men's triple jump|Triple jump]]}}
{{Medal|Competition|[[European Championships in Athletics|European Championships]]}}
{{Medal|Gold| [[1969 European Athletics Championships|1969 Athens]]|[[1969 European Athletics Championships – Men's triple jump|Triple jump]]}}
{{Medal|Gold| [[1974 European Athletics Championships|1974 Rome]]|[[1974 European Athletics Championships – Men's triple jump|Triple jump]]}}
}}
[[File:Viktor Saneyev 1968.jpg|thumb|235px|Saneyev c. 1968]]
'''Viktor Danilovich Saneyev''' ({{lang-ru|Виктор Данилович Санеев}}; born 3 October 1945) is a retired [[triple jumper]], who competed internationally for the [[Soviet Union|USSR]] and won four [[Olympic Games|Olympic]] medals; three golds (1968, 1972 and 1976) and one silver (1980). He was born in [[Sukhumi]], [[Georgian SSR]], and trained in [[Sukhumi]] and later in [[Tbilisi]].<ref name="Khavin"/>

==Athletics career==
Saneyev was born in a poor family, with disabled and paralyzed father who died when Saneyev was 15 years old.<ref name=int/> Saneyev took athletics in 1956, training in the high jump at the [[Gantiadi]] [[boarding school]]; his first coach was Akop Kerselyan. Six years later Kerselyan advised him to specialize in the [[triple jump]]. In 1963 Saneyev finished third in his first All-Union competition – Schoolchildrens' [[Spartakiad]]<ref name="Book">{{cite book|title=Viktor Saneyev|author=E. B. Chen|publisher=[[Fizkultura i sport (publisher)|Fizkultura i sport]]|series=Heroes of the Olympic Games|year=1978|location=Moscow|language=Russian|url=http://www.sportlib.ru/books/la/saneev/}}</ref> 
 
The major success came in 1968, when he won at the USSR Championships and at the [[1968 Summer Olympics]], where on 17 October he set the [[World Record]] twice: to 17.23 m and to 17.39 m.<ref name="WR">[http://www.olympic.org/common/asp/download_report.asp?file=en_report_95.pdf&id=95 World Record Progression – Triple Jump]. [[IOC]]</ref> On the same date four years later Saneyev set the World Record once again, now in [[Sukhumi]], to 17.44 m.<ref name="WR"/> He won gold medals at the [[1972 Summer Olympics]] and at the [[1976 Summer Olympics]] and a silver at the [[1980 Summer Olympics]].

He also won at the [[1969 European Athletics Championships|1969 European Championships]], [[1974 European Athletics Championships|1974 European Championships]], at the [[1970 European Athletics Indoor Championships|1970]], [[1971 European Athletics Indoor Championships|1971]], [[1972 European Athletics Indoor Championships|1972]], [[1975 European Athletics Indoor Championships|1975]], [[1976 European Athletics Indoor Championships|1976]] and [[1977 European Athletics Indoor Championships|1977]] [[European Athletics Indoor Championships]]. Saneyev was the USSR Champion in 1968–71, 1973–75 and 1978.<ref name="Khavin"/>

==1980 Olympics==
Saneyev came to the 1980 Olympics hoping for a fourth gold medal, though he understood that several jumpers had better chances for a gold, especially the world record holder [[João Carlos de Oliveira]]. Soviet [[Jaak Uudmäe]] won the gold medal (17.35 m), followed by Saneyev (17.24 m) and Oliveira (17.22 m). The event was marred with controversial judging. Five out of seven jumps by [[Ian Campbell (athlete)|Ian Campbell]] were discarded, as well as four jumps by Oliveira; Uudmäe had two fouls and Saneyev one.<ref>[http://www.sports-reference.com/olympics/summer/1980/ATH/mens-triple-jump.html Athletics at the 1980 Moskva Summer Games: Men's Triple Jump]. sports-reference.com</ref> All IAAF inspectors were pulled out of the field on the day of the triple jump final and replaced by Soviet staff.<ref name="dunaway">{{cite web |url=http://www.freerepublic.com/focus/news/2048486/posts |title=In 1980, the Soviets Turned the Olympics Into the Games of Shame |author=Dunaway, James |publisher=[[Austin American-Statesman]] |date=July 20, 2008 |accessdate=November 15, 2014}}</ref>

Both Campbell and de Oliveira jumped beyond Uudmäe's leading mark more than once, but all of these jumps were discarded despite protests.<ref name="Siukonen"/><ref name="Torro"/> The longest Campbell's jumps<ref name="Torro"/> was ruled a "scrape" foul: the officials claimed his trailing leg had touched the track during the step phase,<ref name="Siukonen"/><ref name="onlycon"/> though it was unlikely to scrape and jump that far.<ref name="Siukonen"/><ref name="onlycon"/> Saneyev did not contest his foul, though it also fell on his strongest jump. He later noted that the winning jump by Uudmäe was likely overstepped.<ref name=int>[http://runnersclub.ru/articles/viktor-saneev-ot-pryzhkov-ya-poluchal-udovolstvie Виктор Санеев: от прыжков я получал удовольствие]. runnersclub.ru (Interview in Russian). 29 June 2015</ref>

==Retirement==
Saneyev retired after the 1980 Olympics, and was much congratulated well before that: he was awarded [[Order of the Red Banner of Labour]] in 1969, [[Order of Lenin]] in 1972 and [[Order of Friendship of Peoples]] in 1976.<ref name="Khavin"/> At the 1980 Games he was selected as an Olympic torch bearer, though this honor is usually given to retired athletes.<ref name=int/> In retirement, he headed the USSR jumping team for four years, and later worked at his formative club, [[Dynamo Sports Club|Dynamo]] [[Tbilisi]].

In the early 1990s, after the [[dissolution of the Soviet Union|Soviet Union broke up]] and a civil war started in Georgia, Saneyev lost his job, and moved to Australia with his wife and 15-year-old son. His brief coaching contract soon expired, and Saneyev was about to sell his Olympic medals to feed his family. He reconsidered at the last moment and took odd jobs instead, such as pizza delivery. He finally found a regular job as a physical education teacher at [[St Joseph's College, Hunters Hill]], and later as the jumping coach at the [[New South Wales Institute of Sport]].<ref>Forrest, Brad. [http://www.coolrunning.com.au/general/1998e005.shtml "Viktor Saneyev"]. ''[[St George and Sutherland Shire Leader]]'', 31 March 1998.</ref><ref name=nov/>

Saneyev graduated from the [[Georgian State University of Subtropical Agriculture]] and [[Tbilisi State University]],<ref name=nov/> and thus enjoys growing subtropical plants in his backyard, including lemons and grapefruits.<ref name=int/>

==References==
{{Reflist|refs=
<ref name=britannica>{{cite web|title=Viktor Saneyev|url=http://www.britannica.com/EBchecked/topic/522281/Viktor-Saneyev|website=britannica.com|publisher=[[Encyclopædia Britannica]]|accessdate=23 May 2015}}</ref>
<ref name=int>[http://runnersclub.ru/articles/viktor-saneev-ot-pryzhkov-ya-poluchal-udovolstvie Виктор Санеев: от прыжков я получал удовольствие]. runnersclub.ru (Interview in Russian). 29 June 2015</ref>
<ref name=nov>[http://www.novayagazeta.ru/sports/30397.html Three-time WILL] {{ru-icon}} ''[[Novaya Gazeta]]'', 24 July 2006</ref>
<ref name="Khavin">{{cite book|title=All about Olympic Games.|author=Boris Khavin|publisher=[[Fizkultura i sport (publisher)|Fizkultura i sport]]|edition = 2nd|year=1979|location=Moscow|language=Russian|page=577}}</ref>
<ref name="onlycon">{{cite web |url=http://www.smh.com.au/sport/athletics/cheating-the-only-conclusion-you-can-jump-to-20130817-2s3q6.html |title=Cheating the only conclusion you can jump to |author=Lane, Tim |publisher=''[[The Sydney Morning Herald]]'' |accessdate=November 15, 2014 |date=August 18, 2013}}</ref>
<ref name="Siukonen">{{cite book |title= Urheilutieto 5 |last= Siukonen |first= Markku |year= 1980 |publisher= Oy Scandia Kirjat Ab |isbn= 951-9466-20-7 |language=Finnish |pages=363–364|display-authors=etal}}</ref>
<ref name=sports-reference>{{cite web|title=Viktor Saneyev|url=http://www.sports-reference.com/olympics/athletes/sa/viktor-saneyev-1.html|website=sports-reference.com|publisher=Sports Reference LLC}}</ref>
<ref name="Torro">{{cite web |url=https://jyx.jyu.fi/dspace/bitstream/handle/123456789/12014/URN_NBN_fi_jyu-2006367.pdf?sequence=1 |language=Finnish |format=PDF |title=Suomalainen näkökulma Moskovan olympiakisoihin sanomalehdistössä kesällä 1980 |accessdate=November 15, 2014}}</ref>
}}

==Further reading==
{{cite book|title=Viktor Saneyev|author=E. B. Chen|publisher=[[Fizkultura i sport (publisher)|Fizkultura i sport]]|series=Heroes of the Olympic Games|year=1978|location=Moscow|language=Russian|url=http://www.sportlib.ru/books/la/saneev/}}

==External links==
{{Commons category}}
*[http://www.sporting-heroes.net/athletics-heroes/displayhero.asp?HeroID=758 Viktor Saneyev on www.sporting-heroes.net]

{{s-start}}
{{s-ach|rec}}
{{succession box|title=[[Men's triple jump world record]] holder
|years=1968-10-17<br>1968-10-17 – 1971-08-05<br>1972-10-17 – 1975-10-15
|before=[[Giuseppe Gentile]]|after=[[Nelson Prudêncio]]
|before2=[[Nelson Prudêncio]]|after2=[[Pedro Pérez]]
|before3=[[Pedro Pérez]]|after3=[[João Carlos de Oliveira]]}}
{{s-end}}

{{Footer Olympic Champions Triple Jump Men}}
{{Footer European Champions Triple Jump Men}}
{{Footer European Champions Indoor Triple Jump Men}}
{{Footer Universiade Champions Triple Jump Men}}
{{Footer WBYP Triple Men}}
{{IAAF Hall of Fame}}

{{Authority control}}

{{DEFAULTSORT:Saneyev, Viktor}}
[[Category:1945 births]]
[[Category:Living people]]
[[Category:People from Sukhumi]]
[[Category:Soviet male triple jumpers]]
[[Category:Male triple jumpers from Georgia (country)]]
[[Category:Olympic athletes of the Soviet Union]]
[[Category:Olympic gold medalists for the Soviet Union]]
[[Category:Olympic silver medalists for the Soviet Union]]

[[Category:Athletes (track and field) at the 1968 Summer Olympics]]
[[Category:Athletes (track and field) at the 1972 Summer Olympics]]
[[Category:Athletes (track and field) at the 1976 Summer Olympics]]
[[Category:Athletes (track and field) at the 1980 Summer Olympics]]
[[Category:European Athletics Championships medalists]]
[[Category:Former world record holders in athletics (track and field)]]
[[Category:Recipients of the Order of the Red Banner of Labour]]
[[Category:Recipients of the Order of Lenin]]
[[Category:Recipients of the Order of Friendship of Peoples]]
[[Category:Expatriate sportspeople in Australia]]
[[Category:Medalists at the 1980 Summer Olympics]]
[[Category:Medalists at the 1976 Summer Olympics]]
[[Category:Medalists at the 1972 Summer Olympics]]
[[Category:Medalists at the 1968 Summer Olympics]]
[[Category:Olympic gold medalists in athletics (track and field)]]
[[Category:Olympic silver medalists in athletics (track and field)]]