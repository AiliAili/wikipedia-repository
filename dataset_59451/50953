{{About|the bibliographic identifier|the village in the United States|Coden, Alabama}}
{{No footnotes|date=July 2010}}
'''CODEN''' – according to [[ASTM]] standard E250 – is a six character, alphanumeric [[bibliographic code]], that provides concise, unique and unambiguous identification of the titles of [[periodicals]] and non-serial publications from all subject areas.

CODEN became particularly common in the scientific community as a citation system for periodicals cited in technical and chemistry-related publications and as a search tool in many bibliographic catalogues.

=={{anchor|CODEN for Periodical Titles|International CODEN Service}}History==
The CODEN, designed by [[Charles Bishop (researcher)|Charles Bishop]] ([[Chronic Disease Research Institute]] at the [[University at Buffalo]], [[State University of New York]], retired), was initially thought as a memory aid for the publications in his reference collection. Bishop took initial letters of words from periodical titles thereby using a code, which helped him arranging the collected publications. In 1953 he published his documentation system, originally designed as a four letter CODEN system; volume and page numbers have been added, in order to cite and locate exactly an article in a magazine. Later, a variation was published 1957.

After Bishop had assigned about 4,000 CODEN, the four letter CODEN system was further developed since 1961 by Dr. [[Kuentzel]] at the [[ASTM|American Society for Testing of Material]] (ASTM). He also introduced the fifth character to CODEN. In the beginning of the computer age the CODEN was thought as a machine-readable identification system for periodicals. In several updates since 1963, CODEN were registered and published in the ''CODEN for Periodical Titles'' by ASTM, counting to about 128,000 at the end of 1974.

Although it was soon recognized in 1966 that a five character CODEN would not be sufficient to provide all future periodical titles with CODEN, it was still defined as a five character code as given in ASTM standard E250 until 1972. In 1976 the ASTM standard E250-76 defined a six-character CODEN.

Beginning in the year 1975, the CODEN system was within the responsibility of the [[American Chemical Society]].

Today, the first four characters of the six-character CODEN for a periodical are taken from the initial letters of the words from its title, followed by a fifth letter—one of the first six letters (A–F) of the alphabet. The sixth and last character of the CODEN is an alphanumeric check character calculated from the preceding letters.  CODEN always uses capital letters.

In contrast to a periodical CODEN, the first two characters of a CODEN assigned to a non-serial publication (e.g. conference proceedings) are [[Arabic numeral|digit]]s. The third and fourth characters are letters. The fifth and sixth character corresponds to the serial CODEN, but differs in that the fifth character is taken from all letters of the alphabet.

In 1975 the ''International CODEN Service'' located at [[Chemical Abstracts Service]] (CAS) became responsible for further development of the CODEN. The CODEN is automatically assigned to all publications referred on CAS. On request of publishers the ''International CODEN Service'' also assigns CODEN for non chemistry-related publications.
For this reason CODEN may also be found in other [[data base]]s (e.g. [[RTECS]], or [[BIOSIS]]), and are assigned also to serials or magazines, which are not referred in CAS.

=={{anchor|International CODEN Directory}}Current sources==
CODEN assigned until 1966 can be looked up at the two-volume ''CODEN for Periodical Titles'' issued by L.E. Kuentzel. CODEN assigned until 1974 were published by J.G. Blumenthal. CODEN assigned until 1998 and their disintegration can be found at the ''International CODEN Directory'' (ISSN 0364-3670), which has been published since 1980 as a microfiches issue.

Finding a current CODEN is now best done with the online database of [[CASSI]] (''[[Chemical Abstracts Service Source Index]]''), covering all registered titles, CODEN, ISSN, ISBN, abbreviations for publications indexed by CAS since 1907, including serial and non-serial scientific and technical publications: [http://cassi.cas.org/search.jsp]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}.

CASSI online is the replacement for CASSI as a printed serial issue (ISSN 0738-6222, CODEN CASSE2), or as the Collective Index (0001-0634, CODEN CASSI6). CASSI will no longer be published in print. Only the CD-ROM issue of CASSI (ISSN 1081-1990, CODEN CACDFE) will be published furthermore.

== Examples ==
* To the journal ''[[Nature (journal)|Nature]]'' the CODEN "NATUAS" is assigned.
* To ''[[Technology Review]]'' the CODEN "TEREAU" is assigned.
* The ''Proceedings of the International Conference on Food Factors, Chemistry and Cancer Prevention'' (ISBN 4-431-70196-6) uses the CODEN "66HYAL".
* To ''Recent Advances in Natural Products Research, 3rd International Symposium on Recent Advances in Natural Products Research'' the CODEN "69ACLK" is assigned.
* US patent applications use CODEN "USXXDP".
* German patent applications use CODEN "GWXXBX".

== See also ==
* [[ISO 4]]
* [[International Standard Serial Number]] (ISSN)
* [[International Standard Book Number]] (ISBN)
* [[Library of Congress Control Number]] (LCCN)

== References ==
{{refbegin|2}}
* Bishop, Charles: "An integrated approach to the documentation problem". In: ''American Documentation'' (ISSN 0096-946X, CODEN AMDOA7), Vol. 4, p.&nbsp;54–65 (1953).
* Bishop, Charles: "Use of the CODEN system by the individual research scientist". In: ''American Documentation'' (ISSN 0096-946X, CODEN AMDOA7), Vol. 8, p.&nbsp;221–226 (1957).
* Kuentzel, L. E.: "Current status of the CODEN Project". In: ''Special Libraries'' (ISSN 0038-6723), Vol. 57, p.&nbsp;404–406 (1966).
* Kuentzel, L. E.: "CODEN for periodical titles, Vol. 1 ; Periodical titles by CODEN, non-periodical titles, deleted CODEN". In: ''ASTM data series publication'', American Society for Testing and Materials (ISSN 0066-0531); Vol. 23 A (1966).
* Kuentzel, L. E.: "CODEN for periodical titles, Vol. 2 ;  Periodical titles by title". In: ''ASTM data series publication'', American Society for Testing and Materials (ISSN 0066-0531); Vol. 23 A (1966).
* Hammer, Donald P.: "A review of the ASTM CODEN for Periodical Titles". ''Library Resources & Technical Services'' (ISSN 0024-2527), Vol. 12, p.&nbsp;359–365 (1968).
* Saxl, Lea: "Some thoughts about CODEN". In: ''Special Libraries'' (ISSN 0038-6723), Vol. 59, p.&nbsp;279–280 (1968).
* Pflueger, Magaret: "A vote for CODEN". In: ''Special Libraries'' (ISSN 0038-6723), Vol. 60, p.&nbsp;173 (1969).
* Blumenthal, Jennifer G. (ed.): "CODEN for periodical titles, Part 1 ; Periodical titles arranged CODEN". In: ''ASTM data series publication'', American Society for Testing and Materials (ISSN 0066-0531); Vol. 23 B (1970).
* Blumenthal, Jennifer G. (ed.): "CODEN for periodical titles, Part 2 ; Periodical titles arranged alphabetically by title". In: ''ASTM data series publication'', American Society for Testing and Materials (ISSN 0066-0531); Vol. 23 B (1970).
* Blumenthal, Jennifer G. (ed.): "CODEN for periodical titles ; Suppl. 1". In: ''ASTM data series publication'', American Society for Testing and Materials (ISSN 0066-0531); Vol. 23 B (1972).
* Blumenthal, Jennifer G. (ed.): "CODEN for periodical titles ; Suppl. 2". In: ''ASTM data series publication'', American Society for Testing and Materials (ISSN 0066-0531); Vol. 23 B (1974).
* ASTM Standard E 250-72: ''Standard recommended practice for use of CODEN for Periodical Title Abbreviations''. Philadelphia. ASTM, 1972.
* ASTM Standard E 250-76: ''Standard recommended practice for use of CODEN for Periodical Title Abbreviations''. Philadelphia. ASTM, 1976.
* Anon: "Chemical Abstract Service assumes ASTM CODEN assignment". In: ''Journal of Library Automation'' (ISSN 0022-2240), Vol. 8, p.&nbsp;12 (1975).
* Groot, Elizabeth H.: "Unique identifiers for serials: an annotated, comprehensive bibliography". In: ''The Serials Librarian'' (ISSN 0361-526X, CODEN SELID4), Vol. 1 (no. 1), p.&nbsp;51–75 (1976).
* Groot, Elizabeth H.: "Unique identifiers for serials: 1977 update". In: ''The Serials Librarian'' (ISSN 0361-526X, CODEN SELID4), Vol. 2 (no. 3), p.&nbsp;247–255 (1978).
{{refend}}

== External links ==
* Search for CODEN using Chemical Abstracts Service Source Index: [http://cassi.cas.org/search.jsp CASSI search online]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* Homepage of CASSI: [http://cassi.cas.org CASSI homepage]

[[Category:Library science]]
[[Category:Identifiers]]
[[Category:Universal identifiers]]