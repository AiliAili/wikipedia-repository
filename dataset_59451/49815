{{refimprove|date=January 2015}}
'''The Adventure of the Cheerful Four''' （Yukai na yoningumi no bōken, {{nihongo|愉快な四人組の冒険}}）is an episode of the [[Sherlock Holmes (puppetry)|NHK puppetry Sherlock Holmes]]. It is based on "[[The Sign of the Four]]" and was broadcast on 30 November and 7 December 2014 by [[NHK Educational TV]].

==Summary==
The episode is the adapted to musical and set in [[Beeton School]], a fictional boarding school and the songs as "[[Golden Slumbers]] (cradle song)", "[[Greensleeves]]" and "Agra Treasure", the tune made for the show are sung and [[Toccata and Fugue in D minor, BWV 565]] is used as sound effect. And both the puppets of Jonathan Small and Arthur Morstan have the atmosphere of the members of a famous British musical band.<ref>[https://twitter.com/sherlockgakuen/status/541887163873234944 {{nihongo|シャーロック学園}}, twitter of Sherlock Gakuen](Japanese)</ref>
 
==Plot==
One night Arthur Morstan, a pupil of [[Beeton School#Houses and uniform|Archer House]] is attacked and gets injured. His younger sister [[Minor Sherlock Holmes characters#Mary Morstan|Mary Morstan]] requests [[Sherlock Holmes]] to solve why his brother is attacked while [[Dr. Watson|John H. Watson]] falls in love with her. They began searching Arthur's room with the help of Abdullah, roommate of Arthur. Holmes suspects that the offeder stole into the room from [[skylight]] and he and Mary find small handprints around it on the roof. Watson tries to get up on the roof also but he falls and injures his cheek. Holmes sticks the broken pieces on Arthur's bed and finds it's a piece of paper with the sign of four people - Arthur, Bart, Thady, and Jonny. He asks [[Minor Sherlock Holmes characters#Langdale Pike|Langdale Pike]] to check who they are. Pike sells Watson two containers of [[Topical medication#ointment|ointment]] as substitute for search fee and Watson heals his injury with it.

According to Pike's search, Bart and Thady are the Sholto twins of [[Beeton School#Houses and uniform|Dealer House]] who form a vocal group ''the Treasures'' with Arthur, who makes a song titled ''Agra Treasure'' and his friend Jonny. Holmes, Watson and Mary visit Thady who will enter a musical competition between schools with the song but they are obliged to remove Jonny as a member because it is for pupils only and Jonny is not a pupil of [[Beeton School]]. And he says it's Jonny who attacks Arthur. At this moment, Bart returns to the room and makes them drive away. Holmes thinks that it seems to be another reason why Arthur is attacked and it is proved by Pike's another search that Jonny is a [[Mail carrier|mailman]]. Holmes identifies him as Jonathan Small, who entered 221B while singing with Mrs. Hudson one morning. That night Jonny hides himself in the school and commands his pet monkey Tonga to attack a room of Dealer House.
 
The Sholto twins are attacked. Holmes and Watson go to [[School nursing|nurse's office]] where the twins are treated and heard about the affair by Grimesby Roylott, who is in charge of giving life guidance. Holmes asks them who did it but they try to keep it a secret and insist that it's caused by the quarrel between them. School nurse [[Irene Adler]] tells Holmes and Watson that both the twins have scratch marks but their nails are short. She asks Holmes whether he is rewarded for solving affairs or not and Holmes answers her that the work is his reward. Holmes and Watson search the twin's room ignoring the warning of [[Inspector Lestrade|Gordon Lestrade]], member of life-guidance committee and find a piece of the score of ''Agra Treasure'' left with other ones.<ref>The words "Where does a wise man hide a leaf? In the forest." from "The Innocence of Father Brown" are quoted in this scene.</ref> It is clear that the offender is still in the school to steal it. He brings [[Minor Sherlock Holmes characters#Toby|Toby]] from [[Minor Sherlock Holmes characters#Toby|Sherman]]'s shed to the room for his search but it ends in failure. Then he gathers ''Baker House Irregulars'', a group of [[rat]]s led by Wiggins and at last they find Jonny with Tonga on the roof of Dealer House, where he goes up on to avoid Toby and make them fall. Holmes runs up to Jonny and says to him "The game is over, Jonathan Small".
 
The reason why Jonny attacks Arthur, Bart and Thady is not the exclusion from ''the Treasures''. Jonny tells them that ''Agra Treasure'' is his original tune whose title was ''You Are My Treasure'' and Arthur took down in musical notation for Jonny cannot write a score. Arthur, who once decided that he declined entering the music competition but he takes back what he said and changes its title and part of the words. It makes Jonny angry, so he steals the scores at midnight. Then Arthur appears before them and says that the discord among them doesn't bring good harmony. Finally Jonny is reconciled with him, Thady and Bart at nurse's office. Besides Holmes discloses the fact that Jonny sent [[Postcard|picture postcards]] to Mary every week before her entering to Beeton School for he loved and still loves her.
<ref>[http://www.nhk.or.jp/sh15/ {{nihongo|シャーロックホームズ}}(Sherlock Holmes)](Japanese)</ref><ref>[http://sherlockgakuen.jp/ {{nihongo|シャーロック学園}}(Sherlock Gakuen)](Japanese)</ref><ref>{{cite web|url=http://bakerhouse221b.blog.fc2.com/blog-entry-4.html|title=愉快な四人組の冒険1/The Adventure of the Cheerful Four 1 ベイカー寮221B/Baker House221B|accessdate=1 October 2015}}</ref><ref>{{cite web|url=http://bakerhouse221b.blog.fc2.com/blog-entry-5.html|title=愉快な四人組の冒険2/The Adventure of the Cheerful Four 2 ベイカー寮221B/Baker House221B|accessdate=1 October 2015}}</ref>

==Cast==
*[[Kōichi Yamadera]] as Sherlock Holmes, Toby, Tonga and Baker House Irregulars
*[[Wataru Takagi]] as John H. Watson
*[[Keiko Horiuchi]] as [[Minor Sherlock Holmes characters#Mrs. Hudson|Mrs. Hudson]]
*[[Daisuke Kishio]] as Gordon Lestrade and Stamford
*[[Rie Miyazawa]] as Irene Adler
*[[Kazuyuki Asano]] as Grimesby Roylott
*[[Kami Hiraiwa]] as Sherman
*[[Tomokazu Seki]] as Langdale Pike
*[[Anna Ishibashi]] as Mary Morstan
*[[Kenji Urai]] as Arthur Morstan
*[[Hiromasa Taguchi]] as Bartholomew and Thaddeus Sholto
*[[Catherine Seto]] as Mary Sutherland
*[[Tatsuya Fujiwara]] as James Windibank
*[[Fuminori Komatsu]] as Abdullah and Jabez Wilson
*[[Masachika Ichimura]] as Jonathan Small

==Citations==
{{Reflist}}

{{DEFAULTSORT:Adventure of the Cheerful Four}}
[[Category:2014 television episodes]]