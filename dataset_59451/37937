{{infobox scientist
  | name        = Ann Bishop
  | image       = Ann Bishop (biologist).jpg
  | birth_date  = {{birth date|df=y|1899|12|19}}
  | death_date  = {{death date and age|df=y|1990|05|07|1899|12|19}}
  | birth_place = [[Manchester]], England
  | death_place = [[Cambridge]], England
  | citizenship = United Kingdom
  | fields      = Biology, protozoology, parasitology
  | workplaces  = Girton College, Cambridge
  | alma_mater  = Manchester University and Cambridge University
  | thesis_year = 1926
  | known_for   = Studies of drug resistance in ''[[Plasmodium]]''
  }}

'''Ann Bishop''' (19 December 1899&nbsp;– 7 May 1990) was a [[British people|British]] [[Biology|biologist]] from [[Girton College, Cambridge|Girton College]] at the [[University of Cambridge]] and a [[Royal Society#Fellows|Fellow of the Royal Society]], one of the few [[List of female Fellows of the Royal Society|female Fellows of the Royal Society]]. She was born in [[Manchester]] but stayed at Cambridge for the vast majority of her professional life. Her specialties were [[protozoology]] and [[parasitology]]; early work with [[ciliate]] parasites, including the one responsible for [[blackhead disease]] in the [[domesticated turkey]], lay the groundwork for her later research. While working towards her [[doctorate]], Bishop studied parasitic [[amoeba]]e and examined potential [[chemotherapy|chemotherapies]] for the treatment of amoebic diseases including [[amoebic dysentery]].

Her best known work was a comprehensive study of ''[[Plasmodium]]'', the [[malaria]] parasite, and investigation of various chemotherapies for the disease. Later she studied [[drug resistance]] in this parasite, research that proved valuable to the British military in [[World War II]]. She discovered the potential for [[cross-resistance]] in these parasites during that same period. Bishop also discovered the protozoan ''[[Pseudotrichomonas keilini]]'' and worked with ''[[Aedes aegypti]]'', a malaria [[vector (epidemiology)|vector]], as part of her research on the disease. Elected to the [[Royal Society]] in 1959, Bishop was the founder of the [[British Society for Parasitology]] and served on the [[World Health Organization]]'s Malaria Committee.

==Life==
Bishop was born in Manchester, England on 19 December 1899.{{sfn|Ogilvie|2000|p=129}} Her father, James Kimberly Bishop, was a furniture-maker who owned a cotton factory inherited from his father. Her mother, Ellen Bishop (''née'' Ginger), was from nearby [[Bedfordshire]]. Bishop had one brother, born when she was 13.{{sfn|Goodwin|Vickerman|1992|p=29}} At an early age, Bishop wished to continue the family business, though her interests quickly turned to the sciences after her father encouraged her to go to university.{{sfn|Ogilvie|2000|p=129}} Appreciative of music from a young age, Bishop regularly attended performances of the [[The Hallé|Halle Orchestra]] in [[Manchester]].{{sfn|Goodwin|Vickerman|1992|p=36}} As a researcher, she was introverted and meticulous, preferring to work alone or with other scientists whom she considered to have high standards.{{sfn|Goodwin|Vickerman|1992|p=35}} She was a fixture at [[Girton College, Cambridge|Girton College]] for most of her life; ''[[The Guardian]]'' dubbed her "Girtonian of Girtonians" in her obituary. A keen [[cook (profession)|cook]], she was also known for her annoyance at the lack of scientific measures in [[recipe]]s she found.{{sfn|The Guardian–19 May 1990}}

Bishop was recognized at the College for her distinctive hats, which she would wear to breakfast every day before walking to the [[Molteno Institute for Parasite Biology|Molteno Institute]], a distance of {{convert|3.5|miles|km}}. She was skilled in [[needlework]] and appreciated the arts, though she did not like [[modern art]]. Her [[pastime]]s included walking and travelling, especially in the [[Lake District]]: however, she rarely left [[United Kingdom|Britain]]. She also spent time in [[London]] at the beginning of each year, attending the opera and ballet and visiting galleries.{{sfn|Goodwin|Vickerman|1992|p=36}} Towards the end of her life, when her mobility was limited by [[arthritis]], Bishop developed a fascination with the [[history of biology]] and [[history of medicine|medicine]], although she never published in that field.{{sfn|Ogilvie|2000|p=130}} Ann Bishop died of [[pneumonia]] at the age of 90 after a short illness.{{sfn|Ogilvie|2000|p=130}} Her [[memorial service]] was conducted in the College's [[chapel]] and was filled with her wide circle of friends.{{sfn|Goodwin|Vickerman|1992|p=37}}

==Education==
[[File:Girton College, Cambridge, England, 1890s.jpg|thumb|left|Girton College, Bishop's alma mater, approximately 30 years before she attended]]

[[Homeschooling|Educated at home]] until she was seven, Bishop then went to a private elementary school until the age of nine.{{sfn|Goodwin|Vickerman|1992|p=29}}{{sfn|Haines|2001|p=33}} In 1909, then ten years old, she entered the progressive [[Fielden School]] in her hometown of Manchester, where she studied for three years. She completed her high school education at the [[Manchester High School for Girls]]. Though Bishop intended to study [[chemistry]], her lack of education in [[physics]] meant that she could not pursue her preferred course in the Honours School of Chemistry. Instead, she matriculated at [[University of Manchester|Manchester University]] in October 1918 to study [[botany]], chemistry, and [[zoology]].{{sfn|Goodwin|Vickerman|1992|p=29}} That first-year course in zoology sparked her lifelong interest in and commitment to the field. She graduated with honours from the School of Zoology, receiving her [[Bachelor of Science]] degree in 1921; she received her [[master's degree]] in 1922. During her undergraduate years, under the tutelage of the [[helminthology|helminthologist]] R.A. Wardle and the [[protozoologist]] Geoffrey Lapage, Bishop studied [[ciliate]]s acquired from local [[pond]]s.{{sfn|Ogilvie|2000|p=129}}{{sfn|Haines|2001|p=33}}

Two years into her undergraduate career,{{sfn|Ogilvie|2000|p=129}} after winning the [[John Dalton]] [[Natural History]] Prize awarded by the University,{{sfn|Haines|2001|p=33}} she began work for another protozoologist, a [[List of Fellows of the Royal Society|Fellow of the Royal Society]], [[Sydney J. Hickson]].{{sfn|Ogilvie|2000|p=129}} In 1932, she received her [[D.Sc.]] from Manchester University,{{sfn|Goodwin|Vickerman|1992|p=33}} for her work with the blackhead parasite. She received her [[Sc.D.]] from the [[University of Cambridge]] in 1941, though it was in title only: women were not granted full degrees from [[Cambridge]] at this time.{{sfn|Ogilvie|2000|p=129}}

==Scientific career==

===Early work===
[[File:Entamoeba histolytica 01.jpg|thumb|right|An ''Entamoeba histolytica'' cyst, the subject of Bishop's early work]]

Bishop's undergraduate work with Hickson was her first major research effort, concerning the reproduction of ''[[Spirostomum|Spirostomum ambiguum]]'', a large ciliate that has been described as "wormlike".{{sfn|Ogilvie|2000|p=130}}{{sfn|Haines|2001|p=34}}{{sfn|Goodwin|Vickerman|1992|p=31}} In 1923, while working at Manchester University, Bishop was appointed an honorary [[research fellow]]. In 1924, she became a part-time instructor for the Department of Zoology at Cambridge,{{sfn|Ogilvie|2000|p=129}} one of only two women, both of whom were sometimes [[social exclusion|marginalised]].{{sfn|Haines|2001|p=34}} For example, she was not allowed to sit at the table with the men of the department at tea: instead, she sat on a first-aid kit.{{sfn|Goodwin|2004}} There, Bishop continued her work with ''Spirostomum'' as the only protozoologist on the faculty.{{sfn|Ogilvie|2000|p=130}}

She left that position in 1926, to work for [[Clifford Dobell]] at the [[National Institute for Medical Research]] where she stayed there for three years.{{sfn|Ogilvie|2000|p=129}} Under Dobell, Bishop studied [[parasitism|parasitic]] [[amoeba]]e found in the [[human gastrointestinal tract]],{{sfn|Ogilvie|2000|p=130}} focusing on the species responsible for [[amoebic dysentery]], ''[[Entamoeba histolytica]]''. Dobell, Bishop, and [[Patrick Laidlaw]] studied the effects of amoebicides like [[emetine]] for the purpose of treating amoebal diseases. Later in her career, she named the amoeba genus ''[[Dobellina]]'' after her mentor.{{sfn|Goodwin|Vickerman|1992|p=32}}

===Molteno Institute===
The majority of her career was spent at Cambridge's [[Molteno Institute for Parasite Biology]], where she returned in 1929.{{sfn|Ogilvie|2000|p=129}} Her work there was an extension of her research with Dobell, as she studied [[mitosis|nuclear division]] in parasitic [[flagellate]]s and amoebae of diverse species, including both vertebrates and invertebrates.{{sfn|Ogilvie|2000|p=130}} She isolated one type of protozoan, [[anaerobic organism|aerotolerant anaerobes]], from the digestive tract of ''[[Haemopis sanguisuga]]'' during this period. Bishop also discovered a new species, ''[[Pseudotrichomonas keilini]]'', which she named to acknowledge her colleague [[David Keilin]], as well as the parasite's resemblance to the genus ''[[Trichomonas]]''.{{sfn|Ogilvie|2000|p=130}}{{sfn|Goodwin|Vickerman|1992|p=33}} Her research at Manchester with H.P. Baynon concerned the identification, isolation, and study of the turkey blackhead parasite (''[[Histomonas meleagridis]]''); this study pioneered a technique for isolating and growing parasites from lesions on the liver.{{sfn|Goodwin|Vickerman|1992|p=33}} Bishop and Baynon were the first scientists to isolate ''Histomonas'' and then prove its role in blackhead.{{sfn|The Times–22 May 1990}} Bishop's expertise with parasitic protozoa translated into her best-known work, a comprehensive study of the malaria parasite (''[[Plasmodium]]'') and potential chemotherapies for the disease.{{sfn|Ogilvie|2000|p=130}}

[[File:Aedes aegypti.jpg|thumb|left|''Aedes aegypti'', a malaria vector]]

Between 1937 and 1938, Bishop studied the effects of various factors, including different substances in blood and different temperatures, on the feeding behaviour of the [[chicken]] [[malaria]] (''[[Plasmodium gallinaceum]]'') vector, ''[[Aedes aegypti]]''. She also examined factors that contributed to ''Plasmodium'' reproduction.{{sfn|Ogilvie|2000|p=130}}{{sfn|Goodwin|Vickerman|1992|p=34}} This work became the basis for subsequent ongoing research into a [[malaria vaccine]].{{sfn|Goodwin|Vickerman|1992|p=34}} Her subsequent work was spurred by the outbreak of the [[World War II|Second World War]]. During the war, she investigated alternative chemotherapies for malaria. Her research aided the British war effort because the most prevalent [[Antimalarial medication|antimalarial]], [[quinine]], was difficult to obtain due to the [[Military history of Japan#Showa Period and World War II|Japanese]] occupation of the [[Netherlands Antilles|Dutch West Indies]].{{sfn|Haines|2001|p=34}} From 1947 to 1964, she was in charge of the Institute's Chemotherapy Research Institute, associated with the Medical Research Council.{{sfn|The Times–22 May 1990}}

Bishop's work evolved to include studies of [[drug resistance]] in both the parasites and the [[host (biology)|host]] organisms, the studies that would earn her a place in the Royal Society. Significant work from this period of Bishop's life included a study showing that the parasite itself did not develop resistance to quinine, but that host organisms could develop resistance to the drug [[proguanil]]. Her ''[[in vitro]]'' research was proven accurate when the drugs she studied were used to treat patients suffering from [[malaria#Signs and symptoms|tertian malaria]], a form of the illness in which the [[paroxysmal attack|paroxysm]] of fever occurs every third day.{{sfn|Ogilvie|2000|p=130}}{{sfn|Haines|2001|p=34}} She also investigated the drugs [[pamaquine]] and [[quinacrine|atebrin]], along with [[proguanil]], though proguanil was the only one shown to cause the development of drug resistance.{{sfn|Haines|2001|p=34}}{{sfn|Bishop|1961|p=118}} Other studies showed that malaria parasites could develop [[cross-resistance]] to other antimalarial drugs.{{sfn|The Times–22 May 1990}} Bishop worked at Molteno until 1967. Her research and experimental protocols were later used in [[rodent]] and [[human]] studies, albeit with modifications.{{sfn|The Times–22 May 1990}}

===Honours and legacy===
Bishop received several [[Honorary title (academic)|honorary titles]] and fellowships during her career. In 1932, she was appointed a Yallow Fellow of [[Girton College]], an honour she held until her death in 1990. Bishop was also a Beit Fellow from 1929–1932.{{sfn|Ogilvie|2000|p=129}} The [[Medical Research Council (UK)|Medical Research Council]] awarded her a grant in 1937 that sparked her study of ''Plasmodium''.{{sfn|Ogilvie|2000|p=130}} In 1945 and 1947, she was involved in organising Girton College's Working Women's Summer School, an institution designed to provide intellectual fulfilment for women whose formal education ended at the age of 14.{{sfn|GCIP WWSS}} She was elected to the Royal Society in 1959,{{sfn|Ogilvie|2000|p=129}} and at one point was a member of the Malaria Committee of the [[World Health Organization]].{{sfn|The Times–22 May 1990}}

The [[British Society for Parasitology]] was founded in the 1950s, largely due to Bishop's efforts.{{sfn|Ogilvie|2000|p=130}} She was initially given only five pounds and a secretary to start the Society; in order to raise funds Bishop passed around a [[pudding basin]] at the Society's meetings.{{sfn|Goodwin|2004}} The society was originally a subgroup of the Institute of Biology at Cambridge, but it became an independent group in 1960 and was headed by Bishop.{{sfn|Haines|2001|p=34}} She was the president of the group, called the Institute of Biology Parasitology Group, from 1960-1962, the third overall leader of the group.{{sfn|BSP Presidents}} Later that decade, the Department of Biology asked her to be the department head, but she declined because of the public nature of the role.{{sfn|Haines|2001|p=34}} For 20 years, the scientific journal ''[[Parasitology (journal)|Parasitology]]'' had Bishop on staff as an editor.{{sfn|The Guardian–19 May 1990}} Her lifelong association with Girton College prompted the placement of a [[Commemorative plaque|plaque]] commemorating her life, whose inscription, quoted from [[Virgil]], reads "''[[Felix, qui potuit rerum cognoscere causas]]''", Latin for "Happy is the one who has been able to get to know the causes of things".{{sfn|Haines|2001|p=34}} In 1992, the British Society for Parasitology created a grant in Bishop's name, the Ann Bishop Travelling Award, to aid young parasitologists in travelling for field work where their parasites of interest are [[endemism|endemic]].{{sfn|Ranford-Cartwright|2006|p=2}}

==Selected publications==
*{{cite journal
  | title = Some observations upon ''Spirostomum ambiguum'' (Ehrenberg)
  | last = Bishop
  | first = Ann
  | journal = Quarterly Journal of the Microscopical Society
  | volume = 67
  | year = 1923
  | pages = 391–434
  | url = http://jcs.biologists.org/content/s2-67/267/391.full.pdf
  | format = PDF
  }}
*{{cite journal
  | title = The cytoplasmic structures of ''Spirostomum ambiguum'' (Ehrenberg)
  | last = Bishop
  | first = Ann
  | journal = Quarterly Journal of the Microscopical Society
  | volume = 71
  | year = 1927
  | pages = 147–172
  | url= http://jcs.biologists.org/content/s2-71/281/147.full.pdf
  | format = PDF
  }}
*{{cite journal
  | title = Further experiments on the action of emetine in cultures of ''Entamoeba histolytica''
  | last1 = Laidlaw
  | first1 = P. P.
  | last2 = Dobell
  | first2 = Clifford
  | last3 = Bishop
  | first3 = Ann
  | journal = Parasitology
  | volume = 20
  | issue = 2
  | year = 1928
  | pages = 207–220
  | doi = 10.1017/S0031182000011604
  }}
*{{cite journal
  | last1 = Bishop
  | first1 = Ann
  | last2 = Dobell
  | first2 = Clifford
  | title = Researches on the intestinal protozoa of monkeys and man. III: The action of emetine on natural amoebic infections in Macaques
  | journal = Parasitology
  | volume = 21
  | issue = 4
  | year = 1929
  | pages = 446–468
  | doi = 10.1017/S0031182000029334
}}
*{{cite journal
  | last = Bishop
  | first = Ann
  | title = Experiments on the action of emetine in cultures of ''Entamoeba coli''
  | journal = Parasitology
  | volume = 21
  | issue = 4
  | year = 1929
  | pages = 481–486
  | doi = 10.1017/S003118200002936X
}}
*{{cite journal
  | last = Bishop
  | first = Ann
  | title = The morphology and division of ''Trichomonas''
  | journal = Parasitology
  | volume = 23
  | issue = 2
  | year = 1931
  | pages = 129–156
  | doi = 10.1017/S0031182000013524
}}
*{{cite journal
  | title = ''Histomonas meleagridis'' in domestic fowls (''Gallus gallus''). Cultivation and experimental infection
  | year = 1938
  | last = Bishop
  | first = Ann
  | journal = Parasitology
  | volume = 30
  | issue = 2
  | pages = 181–194
  | doi = 10.1017/S0031182000025749
  }}
*{{cite journal
  | last = Bishop
  | first = Ann
  | title = Chemotherapy and avian malaria
  | journal = Parasitology
  | volume = 34
  | issue = 1
  | year = 1942
  | pages = 1–54
  | doi = 10.1017/S0031182000015985
}}
*{{cite journal
  | title = Experiments upon the feeding of ''Aëdes aegypti'' through animal membranes with a view to applying this method to the chemotherapy of malaria
  | last1 = Bishop
  | first1 = Ann
  | last2 = Gilchrist
  | first2 = Barbara M.
  | journal = Parasitology
  | volume = 37
  | issue = 1–2
  | year = 1946
  | doi = 10.1017/S0031182000013202
  | pages=85
  }}
*{{cite journal
  | title = Acquired resistance to paludrine in ''Plasmodium gallinaceum''
  | last1 = Bishop
  | first1 = Ann
  | last2 = Birkett
  | first2 = Betty
  | journal = Nature
  | volume = 159
  | issue = 4052
  | year = 1947
  | pages = 884–885
  | doi = 10.1038/159884a0
  | bibcode = 1947Natur.159..884B
  }}
*{{cite journal
  | title = Drug-resistance in ''Plasmodium gallinaceum'', and the persistence of paludrine-resistance after mosquito transmission
  | last1 = Bishop
  | first1 = Ann
  | last2 = Birkett
  | first2 = Betty
  | journal = Parasitology
  | volume = 39
  | issue = 1–2
  | year = 1948
  | pages = 125–137
  | doi = 10.1017/S0031182000083657
  | pmid = 18876887
  }}
*{{cite journal
  | title = Resistance to sulphadiazine and 'paludrine' in the malaria parasite of the fowl (p. Gallinaceum)
  | last1 = Bishop
  | first1 = Ann
  | last2 = McConnachie
  | first2 = Elspeth W.
  | journal = Nature
  | volume = 162
  | issue = 4118
  | pages = 541–543
  | year = 1948
  | doi = 10.1038/162541a0
  | pmid = 18884622
  | bibcode = 1948Natur.162..541B
  }}
*{{cite journal
  | title = Sulphadiazine-resistance in ''Plasmodium gallinaceum'' and its relation to other antimalarial compounds
  | last1 = Bishop
  | first1 = Ann
  | last2 = McConnachie
  | first2 = Elspeth W.
  | journal = Parasitology
  | volume = 40
  | issue = 1–2
  | year = 1950
  | pages = 163–174
  | doi = 10.1017/S0031182000017996
  | pmid = 15401181
  }}
*{{cite journal
  | title = Problems concerned with gametogenesis in Haemosporidiidea, with particular reference to the genus ''Plasmodium''
  | last = Bishop
  | first = Ann
  | journal = Parasitology
  | volume = 45
  | issue = 1–2
  | year = 1955
  | pages = 163–185
  | doi = 10.1017/S0031182000027542
  | pmid = 14370842
  }}
*{{cite journal
  | title = A study of the factors affecting the emergence of the gametocytes of ''Plasmodium gallinaceum'' from the erythrocytes and the exflagellation of the male gametocytes
  | last1 = Bishop
  | first1 = Ann
  | last2 = McConnachie
  | first2 = Elspeth W.
  | journal = Parasitology
  | volume = 46
  | issue = 1–2
  | year = 1956
  | pages = 192–215
  | doi = 10.1017/S0031182000026433
  | pmid = 13322465
  }}
*{{cite journal
  | last = Bishop
  | first = Ann
  | title = Drug resistance in protozoa
  | journal = Biological Reviews
  | volume = 34
  | issue = 4
  | year = 1959
  | pages = 334–500
  | doi = 10.1111/j.1469-185X.1959.tb01317.x
  }}
{{zoologist|Bishop}}

==References==
{{Reflist|30em}}

;Sources
* {{citation
  | last = Bishop
  | first = Ann
  | journal = New Scientist
  | title = Resistance to drugs by the malaria parasite
  | pages = 118–120
  | number = 231
  | date = 20 April 1961
  | ref = harv
  }}
* {{citation
  | publisher = British Society for Parasitology
  | ref = {{sfnRef|BSP Presidents}}
  | title = Past Presidents
  | year = 2012
  | url = http://www.bsp.uk.net/about-the-bsp/past-presidents/
  }}
* {{citation
  | work = Girton College Archive (Janus)
  | publisher = University of Cambridge
  | url = http://janus.lib.cam.ac.uk/db/node.xsp?id=EAD%2FGBR%2F0271%2FGCIP%20WWSS
  | title = Working Women's Summer School
  | ref = {{sfnRef|GCIP WWSS}}
  }}
* {{citation
  | title = Ann Bishop
  | journal = Biographical Memoirs of Fellows of the Royal Society
  | last1 = Goodwin
  | first1 = L.G.
  | last2 = Vickerman
  | first2 = K.
  | year = 1992
  | volume = 38
  | pages = 28–39
  | url = http://rsbm.royalsocietypublishing.org/content/38/28.full.pdf+html
  | ref = harv
  | doi=10.1098/rsbm.1992.0002
  }}
* {{citation
  | last = Goodwin
  | first = L.G.
  | year = 2004
  | title = Ann Bishop
  | work = Oxford Dictionary of National Biography
  | publisher = Oxford University Press
  | doi = 10.1093/ref:odnb/40061
  | url = http://www.oxforddnb.com/view/article/40061
  | ref = harv
  }}
* {{citation
  | journal = The Guardian
  | title = Obituary of Dr Ann Bishop Girton: Food and protozoa
  | date = 19 May 1990
  | ref = {{sfnRef|The Guardian–19 May 1990}}
  }}
* {{citation
  | title = International Women in Science: A Biographical Dictionary to 1950
  | last = Haines
  | first = Catherine M.C.
  | publisher = ABC-CLIO
  | year = 2001
  | isbn = 9781576070901
  | ref = harv
  | pages = 33–34
  }}
* {{citation
  | title = The Biographical Dictionary of Women in Science: Pioneering Lives From Ancient Times to the Mid-20th Century, Volume 1
  | volume = 1
  | last = Ogilvie
  | first = M.
  | publisher = Taylor & Francis US
  | year = 2000
  | pages = 129–130
  | isbn = 9780415920384
  | ref = harv
  }}
* {{citation
  | journal = Parasitology News: the Newsletter of the British Society for Parasitology
  | date = November 2006
  | last = Ranford-Cartwright
  | first = Lisa
  | publisher = The British Society for Parasitology
  | title = Society News
  | ref = harv
  }}
*{{citation
  | journal = The Times
  | ref = {{sfnRef|The Times–22 May 1990}}
  | date = 22 May 1990
  | title = Dr Ann Bishop
  }}

{{Good article}}

{{DEFAULTSORT:Bishop, Ann}}
[[Category:1899 births]]
[[Category:1990 deaths]]
[[Category:British biologists]]
[[Category:British women scientists]]
[[Category:Fellows of Girton College, Cambridge]]
[[Category:Female Fellows of the Royal Society]]
[[Category:British parasitologists]]
[[Category:Women biologists]]
[[Category:People from Manchester]]
[[Category:Alumni of the University of Manchester]]
[[Category:Deaths from pneumonia]]
[[Category:Alumni of Girton College, Cambridge]]
[[Category:Fellows of the Royal Society]]
[[Category:20th-century women scientists]]