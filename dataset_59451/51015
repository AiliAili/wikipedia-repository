{{Infobox journal
| title = Contributions to the History of Concepts
| cover = 
| editor = Sinai Rusinek, Margrit Pernau
| discipline = [[Conceptual history]]
| abbreviation = Contrib. Hist. Concepts
| publisher = [[Berghahn Journals]]
| country =
| frequency = Biannual
| history = 2005–present
| openaccess =
| impact = 
| impact-year = 
| website = http://journals.berghahnbooks.com/choc/
| link1 = http://berghahn.publisher.ingentaconnect.com/content/berghahn/coco
| link1-name = Online access
| ISSN = 1807-9326
| eISSN = 1874-656X
| JSTOR = 
| OCLC = 750524538
| LCCN = 2007240489
}}
'''''Contributions to the History of Concepts'''''  is a biannual [[peer-reviewed]] [[academic journal]] covering studies in [[conceptual history]]. It is an official journal of the [[History of Concepts Group]].<ref name="hpscg">{{cite web |url=http://www.historyofconcepts.org/ |title=Contributions |publisher=History of Concepts Group |date= |accessdate=2012-11-21}}</ref> It is published by [[Berghahn Journals]] and sponsored by the [[Van Leer Jerusalem Institute]].

== History ==
When the History of Political and Social Concepts Group (now named History of Concepts Group) was founded in 1998, it established a ''History of Concepts Newsletter''.<ref name="programmatic statement">{{cite web |url=http://www.jyu.fi/yhtfil/hpscg/index.html |title=History of Political and Social Concepts Group |publisher=History of Political and Social Concepts Group |date=1998-06-18 |accessdate=2012-11-21}}</ref> This newsletter was first published at the Huizinga Institute ([[University of Amsterdam]]) and then at the Renvall Institute for Area and Cultural Studies ([[Helsinki University]]). In 2005, the newsletter was replaced by ''Contributions to the History of Concepts''.<ref name="angenot and braw">[[Marc Angenot]], L'histoire des idées : problématiques, objets, concepts, enjeux, débats et méthodes [http://marcangenot.com/wp-content/uploads/2011/12/histoire-des-idées-volume-2.pdf]. Montréal, Discours social, 2011, XXXIII, p.12 note; {{cite web |url=http://www.svd.se/kultur/understrecket/i-orden-kan-vi-fa-syn-pa-var-historia_20403.svd |title=I orden kan vi få syn på vår historia |trans_title=In words, we catch sight of our history |language=sv |work=[[Svenska Dagbladet]] |date=27 September 2007 |first=Daniel |last=Braw}}</ref> The journal's founding [[editor-in-chief]] was João Feres, Jr. ([[Universidade Cândido Mendes]]). In its first years the journal was hosted and sponsored by the Instituto Universitário de Pesquisas do Rio de Janeiro (Universidade Cândido Mendes) and published by [[Brill Publishers]], until it moved to Berghahn journals in 2010 and was sponsored by the Van Leer Jerusalem Institute.

The journal is abstracted and indexed in [[Scopus]].

== References ==
{{Reflist}}

== Further reading ==
* [http://histcon.se/news/relaunch-of-contributions-to-the-history-of-concepts-journal/ Announcement in Time, Memory and Representation. A Multidisciplinary Program on Transformations in Historical Consciousness]

== External links ==
* {{Official website|http://www.historyofconcepts.org}}


[[Category:English-language journals]]
[[Category:Berghahn Books academic journals]]
[[Category:Publications established in 2005]]
[[Category:Biannual journals]]
[[Category:History journals]]