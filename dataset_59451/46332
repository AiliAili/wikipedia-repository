{{Infobox medical condition
| name            = Autosomal dominant cerebellar ataxia
| image           = Autosomal dominant - en.svg
| alt             = Autosomal Dominance
| caption         = Autosomal Dominant Hereditary Tree
| pronounce       =
| synonym         =
| field           = 
| DiseasesDB      = 
| ICD10           = {{ICD10|G11.1}}
| ICD9            = 
| ICDO            = 
| OMIM            = 
| MedlinePlus     = 
| MeSH            = 
| GeneReviewsNBK  = 
| GeneReviewsName = 
| Orphanet        = 99
}}
'''Autosomal dominant cerebellar ataxia''' ('''ADCA''') is a form of [[spinocerebellar ataxia]] inherited in an [[autosomal dominant]] manner. ADCA is a genetically [[Heredity|inherited]] [[Disease#Medical condition|condition]] that causes deterioration of the [[nervous system]] leading to disorder and a decrease or loss of function to regions of the body.<ref name="pat">{{Cite web|url=http://patient.info/doctor/autosomal-dominant-cerebellar-ataxia|title=Autosomal Dominant Cerebellar Ataxia information page. Patient {{!}} Patient|website=Patient|language=en-GB|access-date=2016-03-25}}</ref>

Degeneration occurs at the cellular level and in certain subtypes results in cellular death. Cellular death or dysfunction causes a break or faulty signal in the line of communication from the central nervous system to target muscles in the body. When there is impaired communication or a lack of communication entirely, the muscles in the body do not function correctly. Muscle control complications can be observed in multiple balance, speech, and motor or movement impairment symptoms. ADCA is divided into three types and further subdivided into subtypes known as SCAs ([[spinocerebellar ataxia]]s).<ref name="ojrd">{{cite journal|last=Whaley|first=Nathaniel|author2=Fujioka, Shinsuke|author3= Wszolek, Zbigniew K|title=Autosomal dominant cerebellar ataxia type I: A review of the phenotypic and genotypic characteristics|journal=Orphanet Journal of Rare Diseases|date=1 January 2011|volume=6|issue=1|pages=33|doi=10.1186/1750-1172-6-33|pmid=21619691|pmc=3123548}}</ref>

==ADCA types==
Currently there are 27 ''subtypes'' have been identified: SCA1-SCA4, SCA8, SCA10, SCA12- SCA14, SCA15/SCA16, SCA17- SCA23, SCA25, SCA27, SCA28, SCA32, SCA34- SCA37, autosomal dominant cerebellar ataxia and [[dentatorubral pallidoluysian atrophy]] .<ref name="orphan">{{Cite web|url=http://www.orpha.net/consor/cgi-bin/OC_Exp.php?lng=EN&Expert=94145|title=Orphanet: Autosomal dominant cerebellar ataxia type 1|last=RESERVED|first=INSERM US14 -- ALL RIGHTS|website=www.orpha.net|access-date=2016-03-25}}</ref>

===Type 1===
''Type I'' ADCA is characterized by different symptoms of [[ataxia]] as well as other conditions that are dependent on the subtype. Type 1 ADCA is divided into 3 subclasses based on pathogenesis of the subtypes each contain.<ref name="ojrd" /><ref name=orphan/><ref>{{Cite web|url=https://ghr.nlm.nih.gov/condition/spinocerebellar-ataxia-type-1|title=SCA1|date=2016-03-21|website=Genetics Home Reference|access-date=2016-03-25}}</ref>
[[File:L-Glutamin - L-Glutamine.svg|thumb|L-Glutamin]]
*'''Subtype 1''' -subtypes in the first subclass are caused by CAG [[nucleotide]] repeats in the [[DNA]], which code for the [[amino acid]] [[glutamine]]. This glutamine is toxic to the cell on the level of [[proteins]] and has degenerative effects. Within the first subclass of Type 1 are SCA1, SCA2, SCA3, SCA17, and DRPLA. This first subclass is the most common of Type 1 ADCAs with SCA3 being the most common subtype of all of Type 1. SCA3, [[Machado-Joseph disease]], is the most common because the [[mutation]] repeats more than 56 times while the regular length is around 13 to 31.<ref name="ojrd" />
*'''Subtype 2''' -the second subclass of Type 1 ADCA is also caused by the same nucleotide repeats but instead in [[RNA]] and in a region that does not code for proteins. [[Gene expression]] is affected instead of proteins in subtype two SCAs because of this. Subtype 2 contains SCA8, SCA10, and SCA12.<ref name="ojrd" />
*'''Subtype 3''' -the third subclass of Type 1 ADCA is caused by different [[mutations]] and deletions in genes. It comprises SCA13, SCA14, SCA15, SCA16, SCA27, and SCA28.<ref name="ojrd" />

===Type 2/3===
''Type II'' ADCA is composed of SCA7 and syndromes associated with pigmentary maculopathies.<ref name="ojrd" /> SCA7 is a disease that specifically displays [[retinal]] degeneration, along with the common degeneration of the [[cerebellum]]. Moving further into SCA7's pathology, a similar genetic process is described, while the function of ATXN7 (an ataxin gene) is much like a component of the SAGA complex. The SAGA complex uses two [[histone]]-modifying techniques to regulate [[transcription (genetics)|transcription]]. These activities are the Gcn5 histone [[acetyltransferase]] and the Usp22 deubiquitinase. Mutant ATXN7 in HAT activity causes an increase in activity, which was reported from an [[in-vivo]] analysis in the [[retina]]. There are also studies that show a loss in activity when human [[ATXN7]] in yeast was used.
The SCA7 autosomal-dominant [[inheritance]] pattern is similar to a mutant ATXN5-induced gain in Gcn5 HAT.<ref name="tjcb">{{cite journal|last=Orr|first=H. T.|title=The cell biology of disease: Cell biology of spinocerebellar ataxia|journal=The Journal of Cell Biology|date=16 April 2012|volume=197|issue=2|pages=167–177|doi=10.1083/jcb.201105092}}</ref> Spinocerebellar ataxia type 15 has been classified as an ADCA ''Type III'' as it has been noted to have postural and action tremor in addition to cerebellar ataxia.<ref name="ojrd" /> Additionally, spinocerebellar ataxia type 20 (SCA20) is organized in ADCA III that often exhibits disease-like symptoms at an earlier age, sometime starting at fourteen years old.<ref name="ojrd" /><ref>{{Cite book|url=http://www.ncbi.nlm.nih.gov/books/NBK1275/|title=Spinocerebellar Ataxia Type 2|last=Pulst|first=Stefan M.|date=1993-01-01|publisher=University of Washington, Seattle|editor-last=Pagon|editor-first=Roberta A.|location=Seattle (WA)|pmid=20301452|editor-last2=Adam|editor-first2=Margaret P.|editor-last3=Ardinger|editor-first3=Holly H.|editor-last4=Wallace|editor-first4=Stephanie E.|editor-last5=Amemiya|editor-first5=Anne|editor-last6=Bean|editor-first6=Lora JH|editor-last7=Bird|editor-first7=Thomas D.|editor-last8=Fong|editor-first8=Chin-To|editor-last9=Mefford|editor-first9=Heather C.}}Revised 2015</ref><ref>{{Cite journal|last=Fujioka|first=Shinsuke|last2=Sundal|first2=Christina|last3=Wszolek|first3=Zbigniew K|date=2013-01-18|title=Autosomal dominant cerebellar ataxia type III: a review of the phenotypic and genotypic characteristics|url=http://ojrd.biomedcentral.com/articles/10.1186/1750-1172-8-14|journal=Orphanet Journal of Rare Diseases|language=En|volume=8|issue=1|doi=10.1186/1750-1172-8-14|pmc=3558377|pmid=23331413|pages=14}}</ref>

==Symptoms/signs==
[[File:Cerebellum animation small.gif|thumb|Cerebellum]]
Symptoms typically are onset in the adult years, although, childhood cases have also been observed. Common symptoms include a loss of coordination which is often seen in walking, and slurred speech. ADCA primarily affects the [[cerebellum]], as well as, the [[spinal cord]].<ref>{{Cite book|url=http://www.ncbi.nlm.nih.gov/books/NBK1138/|title=Hereditary Ataxia Overview|last=Bird|first=Thomas D.|date=1993-01-01|publisher=University of Washington, Seattle|editor-last=Pagon|editor-first=Roberta A.|location=Seattle (WA)|pmid=20301317|editor-last2=Adam|editor-first2=Margaret P.|editor-last3=Ardinger|editor-first3=Holly H.|editor-last4=Wallace|editor-first4=Stephanie E.|editor-last5=Amemiya|editor-first5=Anne|editor-last6=Bean|editor-first6=Lora JH|editor-last7=Bird|editor-first7=Thomas D.|editor-last8=Fong|editor-first8=Chin-To|editor-last9=Mefford|editor-first9=Heather C.}}Last Revision: March 3, 2016.</ref> Some signs and symptoms are:<ref name=pat/>

*Episodes of altered level of [[consciousness]] 
*[[Neurological]] regression
*Multi-system involvement 
*Movement disorders.
*Cerebellar dysfunction 
*[[Ataxia]]

==Genetics==

In terms of the genetics of autosomal dominant cerebellar ataxia 11 of 18 known genes are caused by repeated expansions in corresponding proteins, sharing the same mutational mechanism. SCAs can be caused by conventional mutations or large rearrangements in genes that make [[glutamate]] and calcium signaling, channel function, tau regulation and [[mitochondrial]] activity or RNA alteration.<ref>Autosomal dominant cerebellar ataxias: polyglutamine expansions and beyond. Durr A. {{DOI|10.1016/S1474-4422(10)70183-6}}{{Subscription or libraries|sentence|via=[[ScienceDirect]]}}</ref>

The mechanism of Type I is not completely known, however Whaley, et al. suggest the polyglutamine product is toxic to the cell at a [[protein]] level, this effect may be done by [[transcriptional]] dysregulation and disruption of [[calcium]] homeostasis which causes [[apoptosis]] to occur earlier.<ref name=ojrd/>

==Diagnosis==
In diagnosing autosomal dominant cerebellar ataxia the individuals clinical history or their past health examinations, a current physical examination to check for any physical [[abnormalities]], and a genetic screening of the patients genes and the genealogy of the family are done.<ref>{{cite journal |pmid=17204042 | doi=10.1111/j.1399-0004.2006.00722.x | volume=71 | title=Diagnosis and management of early- and late-onset cerebellar ataxia | year=2007 | journal=Clin. Genet. | pages=12–24 |vauthors=Brusse E, Maat-Kievit JA, van Swieten JC }}</ref>  The large category of cerebellar ataxia is caused by a deterioration of [[neurons]] in the cerebellum, therefore [[magnetic resonance imaging]] (MRI) is used to detect any structural abnormality such as [[lesions]] which are the primary cause of the ataxia. [[Computed tomography]] (CT) scans can also be used to view neuronal deterioration, but the [[MRI]] provides a more accurate and detailed picture.<ref>{{Cite web|url=https://www.orpha.net/data/patho/GB/uk-SCA.pdf|title=Autosomal Dominant Cerebellar Ataxia|last=Ludger|first=Schols|date=2003|website=Orphanet|publisher=Orphanet|access-date=25 March 2016}}</ref>

==Treatments==
In terms of a cure there is currently none available, however for the disease to manifest itself, it requires mutant [[gene expression]].<ref name="tjcb" /> Manipulating the use of protein homoestasis regulators can be therapuetic agents, or a treatment to try and correct an altered function that makes up the [[pathology]] is one current idea put forth by Bushart, et al.<ref name="tjcb" /><ref>{{Cite journal|last=Bushart|first=David D.|last2=Murphy|first2=Geoffrey G.|last3=Shakkottai|first3=Vikram G.|date=2016-01-01|title=Precision medicine in spinocerebellar ataxias: treatment based on common mechanisms of disease|journal=Annals of Translational Medicine|volume=4|issue=2|doi=10.3978/j.issn.2305-5839.2016.01.06|issn=2305-5839|pmc=4731605|pmid=26889478}}</ref> There is some evidence that for SCA1 and two other polyQ disorders that the pathology can be reversed after the [[disease]] is underway.<ref name="tjcb" />  There is no effective treatments that could alter the progression of this disease, therefore care is given, like occupational and physical therapy for gait dysfunction and speech therapy.{{medical citation needed|date=March 2016}}

==Epidemiology/frequency==
In terms of frequency, is estimated at 2 per 100,000, it has identified in different regions of the world. Some clusters of certain types of autosomal dominant cerebellar ataxia reach a prevalence of 5 per 100,000.<ref name=pat/>

==References==
{{reflist|2}}

==Further reading==
*{{Cite journal|last=Burk|first=K|date=1996|title=Autosomal dominant cerebellar ataxia type I Clinical features and MRI in families with SCA1, SCA2 and SCA|url=http://brain.oxfordjournals.org/content/brain/119/5/1497.full.pdf|journal=Brain|doi=|pmid=|access-date=25 March 2016}}
*{{Cite book|url=https://books.google.com/books?id=5Kp3CgAAQBAJ|title=Merritt's Neurology|last=Louis|first=Elan D.|last2=Mayer|first2=Stephan A.|last3=Rowland|first3=Lewis P.|date=2015-08-31|publisher=Lippincott Williams & Wilkins|isbn=9781496321077|language=en}}
*{{Cite book|url=https://books.google.com/books?id=Qq0SfGR_R_QC|title=Movement Disorders: Genetics and Models|last=LeDoux|first=Mark S.|date=2005-01-25|publisher=Academic Press|isbn=9780080470566|language=en}}
{{Medicine}}
{{Diseases of the nervous system}}

[[Category:Autosomal dominant disorders]]
[[Category:Systemic atrophies primarily affecting the central nervous system]]
[[Category:Neurodegenerative disorders]]
[[Category:Cytoskeletal defects]]