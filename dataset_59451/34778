{{Good article}}
{{Infobox hurricane season
| Basin=Atl
| Year=1852
| Track=1852 Atlantic hurricane season summary map.png
| First storm formed=August 19, 1852
| Last storm dissipated=October 11, 1852
| Strongest storm name=[[#Hurricane One|One]]
| Strongest storm winds=105
| Strongest storm pressure=961
| Average wind speed=1
| Total storms=5
| Total hurricanes=5
| Total intense=1
| Damages=1
| Inflated=0
| Fatalities=100+ direct
| five seasons=[[1850 Atlantic hurricane season|1850]], [[1851 Atlantic hurricane season|1851]], '''1852''', [[1853 Atlantic hurricane season|1853]], [[1854 Atlantic hurricane season|1854]]
}}
The '''1852 Atlantic hurricane season''' was one of only three [[List of Atlantic hurricane seasons|Atlantic hurricane seasons]] in which every known [[tropical cyclone]] attained hurricane status.{{Atlantic hurricane best track}}  Five tropical cyclones were reported during the season, which lasted from late August through the middle of October; these dates fall within the range of most Atlantic tropical cyclone activity, and none of the cyclones coexisted with another.  Though there were officially five tropical cyclones in the season, hurricane scholar Michael Chenoweth assessed two of the cyclones as being the same storm.<ref name="cheno"/>  There may have been other unconfirmed tropical cyclones during the season, as meteorologist [[Christopher Landsea]] estimated that up to six storms were missed each year from the official database; this estimate was due to small tropical cyclone size, sparse ship reports, and relatively unpopulated coastlines.<ref>{{cite web|author=[[Chris Landsea]]|year=2007|title=Counting Atlantic Tropical Cyclones Back to 1900|publisher=American Meteorological Society|accessdate=2007-07-23|url=http://www.aoml.noaa.gov/hrd/Landsea/landsea-eos-may012007.pdf|format=PDF| archiveurl= https://web.archive.org/web/20070714114214/http://www.aoml.noaa.gov/hrd/Landsea/landsea-eos-may012007.pdf| archivedate= 14 July 2007 <!--DASHBot-->| deadurl= no}}</ref>
__TOC__
{{Clear}}

==Season summary==
Every tropical cyclone in the season was of hurricane status, or with winds at or exceeding 74&nbsp;mph (119&nbsp;km/h).  In only two other seasons did every cyclone attain hurricane status; those years were [[1858 Atlantic hurricane season|1858]] and [[1884 Atlantic hurricane season|1884]].<ref name="HURDAT"/>  All five cyclones affected land; the strongest was the first storm, which caused severe damage and loss of life when it made [[landfall (meteorology)|landfall]] near the border between [[Mississippi]] and [[Alabama]].  The second storm of the season struck [[Puerto Rico]], where it caused over 100&nbsp;deaths, primarily from flooding.  In the middle of September, the third storm moved across Florida with strong wind gusts and light rainfall, and a week later the fourth storm passed over or north of the [[Lesser Antilles|Lesser]] and [[Greater Antilles]].  The last storm hit the Florida Panhandle, though damage was less than expected.

==Timeline==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/08/1852 till:01/11/1852
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/08/1852

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:19/08/1852 till:30/08/1852 color:C3 text:"One (C3)"
  from:05/09/1852 till:06/09/1852 color:C1 text:"Two (C1)"
  from:09/09/1852 till:13/09/1852 color:C1 text:"Three (C1)"
  from:22/09/1852 till:30/09/1852 color:C1 text:"Four (C1)"
  from:06/10/1852 till:11/10/1852 color:C2 text:"Five (C2)"

  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/08/1852 till:01/09/1852 text:August
  from:01/09/1852 till:01/10/1852 text:September
  from:01/10/1852 till:01/11/1852 text:October

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

==Systems==

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1852 Atlantic hurricane 1 track.png
|Formed=August 19
|Dissipated=August 30
|1-min winds=100
|Pressure=961
}}
The first tropical cyclone of the year, also known as the ''Great Mobile Hurricane of 1852'', was first observed on August 19 about 140&nbsp;mi (230&nbsp;km) north of [[Puerto Rico]]. It moved on a west-northwest motion before passing through the [[Bahamas]] as it attained hurricane status on August 20. After paralleling the northern coast of [[Cuba]], the storm passed between the [[Dry Tortugas]] and [[Key West, Florida]] on August 22, and two days later it is estimated the hurricane attained peak winds of 115&nbsp;mph (185&nbsp;km/h). The storm slowed on August 25 before turning northward, and early on August 26 it made [[landfall (meteorology)|landfall]] near [[Pascagoula, Mississippi]] at peak strength, and the hurricane rapidly weakened to tropical storm status as it accelerated east-northeastward. On August 28 it emerged into the Atlantic Ocean from [[South Carolina]], and after turning to the northeast, it was last observed on August 30 about 130&nbsp;mi (200&nbsp;km) southeast of [[Cape Cod]].<ref name="meta"/>

In the [[Florida Keys]], rough waves forced several ships ashore, leaving some damaged.<ref>{{cite news|author=New York Daily Times|date=1852-09-03|title=Key West Hurricane Dispatch|accessdate=2008-06-17|url=http://www.thehurricanearchive.com/Viewer.aspx?img=2789659_clean&firstvisit=true&src=search&currentResult=5&currentPage=20}}</ref> Strong waves created four new channels in the [[Chandeleur Islands]], and the storm's passage also destroyed the island lighthouse; the three keepers were found three days later. Two schooners were also washed ashore along [[Cat Island (Mississippi)|Cat Island]].<ref>{{cite web|author=David M. Roth|year=1998|title=Louisiana Hurricane History: Late 19th Century|publisher=Lake Charles, Louisiana National Weather Service|accessdate=2008-06-17|url=http://www.srh.noaa.gov/lch/research/lalate19hu.php |archiveurl = https://web.archive.org/web/20080521144526/http://www.srh.noaa.gov/lch/research/lalate19hu.php |archivedate = May 21, 2008}}</ref> The hurricane produced an estimated [[storm tide]] of 12&nbsp;feet (3.7&nbsp;m) in [[Mobile, Alabama]],<ref name="meta"/> where strong winds damaged much of the city, leaving the majority of the houses destroyed. Trees were downed up to 30&nbsp;miles (50&nbsp;km) inland,<ref name="nyt831">{{cite news|author=New York Daily Times|date=1852-08-31|title=From the South: The Gale in Mobile, Alabama|accessdate=2008-06-17|url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9F0CE2DE1231E13BBC4852DFBF668389649FDE&oref=slogin | format=PDF|work=The New York Times}}</ref> and coastal areas were flooded. Damage along the coastline was estimated at around $1&nbsp;million (1852&nbsp;USD, $26&nbsp;million 2008&nbsp;USD), and several lives were lost.<ref>{{cite news|author=Weekly Wisconsin|date=1852-08-29|title=Destructive Storm at Mobile|accessdate=2008-06-17|url=http://www.thehurricanearchive.com/Viewer.aspx?img=33318539_clean&firstvisit=true&src=search&currentResult=8&currentPage=0}}</ref> While crossing the southeastern United States, the storm brought light rainfall but moderately strong winds; in [[Charleston, South Carolina]], the storm destroyed several bridges and crop fields.<ref name="meta"/>
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1852 Atlantic hurricane 2 track.png
|Formed=September 5
|Dissipated=September 6
|1-min winds=70
|Pressure=
}}
Early on September 5, a hurricane was first observed about 65&nbsp;mi (110&nbsp;km) southeast of [[Christiansted, United States Virgin Islands|Christiansted]] in the [[Danish Virgin Islands]].<ref name="HURDAT"/> One meteorologist assessed the hurricane as being located near [[Antigua]] on September 3.<ref name="cheno"/> Tracking steadily west-northwestward, it quickly moved ashore near [[Ponce, Puerto Rico]] with winds estimated at 80&nbsp;mph (130&nbsp;km/h). After crossing southwestern Puerto Rico, the hurricane emerged into the [[Mona Passage]] as a tropical storm. Late on September 5 it made landfall on eastern [[Dominican Republic]]; it quickly weakened over Hispaniola, dissipating on September 6 over the northwestern portion of the island.<ref name="HURDAT"/> An assessment by scholar Michael Chenoweth in 2006 indicated this storm was the same as the next hurricane, with it continuing northwestward and ultimately reaching the [[Gulf of Mexico]].<ref name="cheno">{{cite web|author=Michael Chenoweth|year=2006|title=A Reassessment of Historical Atlantic Basin Tropical Cyclone Activity, 1700–1855|publisher=Hurricane Research Division|accessdate=2008-05-25|url=http://www.aoml.noaa.gov/hrd/HURDAT/Chenoweth/chenoweth06.pdf|format=PDF| archiveurl= https://web.archive.org/web/20080528033816/http://www.aoml.noaa.gov/hrd/HURDAT/Chenoweth/chenoweth06.pdf| archivedate= 28 May 2008 <!--DASHBot-->| deadurl= no}}</ref> Due to not being considered the same cyclone in the [[Atlantic hurricane reanalysis|official hurricane database]],<ref name="HURDAT"/> this hurricane and the following hurricane are listed separately.

The cyclone is known as the ''San Lorenzo hurricane'', due to its impact in Puerto Rico.<ref name="meta">{{cite web|author=[[Hurricane Research Division]]|year=2008|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|publisher=National Oceanic and Atmospheric Administration|accessdate=2008-06-14|url=http://www.aoml.noaa.gov/hrd/HURDAT/metadata_master.html| archiveurl= https://web.archive.org/web/20080612071834/http://www.aoml.noaa.gov/hrd/HURDAT/metadata_master.html| archivedate= 12 June 2008 <!--DASHBot-->| deadurl= no}}</ref> There, the passage of the storm caused severe flooding, which destroyed large quantities of crops and damaged several roads. Storm damage was heaviest between [[Guayanilla, Puerto Rico|Guayanilla]] and [[Mayagüez, Puerto Rico|Mayagüez]].<ref name="pr">{{cite web|author=Orlando Perez|year=1970|title=Notes on the Tropical Cyclones of Puerto Rico|publisher=National Weather Service|accessdate=2008-06-14|format=PDF|url=http://www.aoml.noaa.gov/hrd/data_sub/perez_11_20.pdf| archiveurl= https://web.archive.org/web/20080528061139/http://www.aoml.noaa.gov/hrd/data_sub/perez_11_20.pdf| archivedate= 28 May 2008 <!--DASHBot-->| deadurl= no}}</ref> More than 100&nbsp;people were killed in Puerto Rico,<ref>{{cite web|author=Edward N. Rappaport and Jose Fernandez-Partagas|year=1995|title=The Deadliest Atlantic Tropical Cyclones, 1492–1996|publisher=National Hurricane Center|accessdate=2008-06-14|url=http://www.nhc.noaa.gov/pastdeadlyapp1.shtml?| archiveurl= https://web.archive.org/web/20080622205237/http://www.nhc.noaa.gov/pastdeadlyapp1.shtml| archivedate= 22 June 2008 <!--DASHBot-->| deadurl= no}}</ref> many of which due to flooding.<ref name="pr"/>
{{Clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1852 Atlantic hurricane 3 track.png
|Formed=September 9
|Dissipated=September 13
|1-min winds=70
|Pressure=985
}}
A hurricane was located in the central [[Gulf of Mexico]] on September 9,<ref name="HURDAT"/> potentially the same hurricane as the previous storm.<ref name="cheno"/> It tracked generally eastward toward the coast of Florida, with its hurricane intensity estimation based on two ship reports. At about 0000&nbsp;[[Coordinated Universal Time|UTC]] on September 12, it moved ashore near [[Clearwater, Florida]] as a minimal hurricane, with an estimated [[Atmospheric pressure|minimum barometric central pressure]] of 985&nbsp;[[bar (unit)|mbar]]. Accelerating east-northeastward while crossing the state, the cyclone emerged into the Atlantic Ocean as a weakened tropical storm before regaining hurricane status on September 13. Later that day, it was last observed about 250&nbsp;mi (400&nbsp;km) east-southeast of [[Cape Hatteras]].<ref name="HURDAT"/>

A post in [[Fort Meade, Florida]] reported at least 0.55&nbsp;in (14&nbsp;mm) of rainfall during the storm's passage. The hurricane was considered "violent", and gusts were estimated to have reached hurricane force.<ref name="meta"/> Rough seas and strong easterly winds beached a vessel near [[St. Augustine, Florida|St. Augustine]].<ref name="aoml">{{cite web|author=Al Sandrik & [[Chris Landsea]]|year=2003|title=Chronological Listing of Tropical Cyclones affecting North Florida and Coastal Georgia 1565–1899|publisher=[[Hurricane Research Division]]|accessdate=2008-06-15|url=http://www.aoml.noaa.gov/hrd/Landsea/history/index.html| archiveurl= https://web.archive.org/web/20080624185603/http://www.aoml.noaa.gov/hrd/Landsea/history/index.html| archivedate= 24 June 2008 <!--DASHBot-->| deadurl= no}}</ref>
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1852 Atlantic hurricane 4 track.png
|Formed=September 22
|Dissipated=September 30
|1-min winds=80
|Pressure=
}}
On September 22, a tropical storm was located about 200&nbsp;mi (330&nbsp;km) east of [[Guadeloupe]]. With a steady west-northwest path, the storm moved across the northern [[Lesser Antilles]] on September 23, during which it intensified into a hurricane. It passed a short distance north of [[Puerto Rico]] and the [[Dominican Republic]] as it reached its peak intensity of 90&nbsp;mph (150&nbsp;km). Late on September 26 the hurricane turned northwestward, bringing it through the [[Turks and Caicos Islands]] and eastern [[Bahamas]]. Recurving north-northeastward, the cyclone moved into open waters, and was last classified as a tropical cyclone on September 30 about 390&nbsp;mi (630&nbsp;km) east of [[Cape Hatteras]].<ref name="HURDAT"/> However, one hurricane researcher assessed the hurricane as lasting until October 3, with the cyclone turning eastward and dissipating near the [[Azores]].<ref name="cheno"/>
{{Clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1852 Atlantic hurricane 5 track.png
|Formed=October 6
|Dissipated=October 11
|1-min winds=90
|Pressure=969
}}
A moderately strong hurricane with winds of 105&nbsp;mph (165&nbsp;km/h) was first spotted on October 6 east of [[Jamaica]]. Passing a short distance south of the island, the hurricane tracked northwestward and brushed the [[Yucatán Peninsula]] before turning north-northeastward into the [[Gulf of Mexico]].<ref name="HURDAT"/> Late on October 9, it made landfall a short distance east of [[Apalachicola, Florida]] at peak winds with an estimated pressure of 969&nbsp;mbar.<ref>{{cite web|author=[[Hurricane Research Division]]|year=2008|title=Continental U.S. Hurricanes: 1851 to 1920|accessdate=2008-06-18|url=http://www.aoml.noaa.gov/hrd/HURDAT/usland1851-1920.html| archiveurl= https://web.archive.org/web/20080719104759/http://www.aoml.noaa.gov/hrd/HURDAT/usland1851-1920.html| archivedate= 19 July 2008 <!--DASHBot-->| deadurl= no}}</ref> Rapidly weakening to tropical storm status, the cyclone continued northeastward and emerged into the Atlantic Ocean from North Carolina on October 11. Later that day, it was last observed about 250&nbsp;mi (400&nbsp;km) southeast of Cape Cod.<ref name="HURDAT"/>

Heavy damage was reported in Jamaica. Upon making landfall in Florida, the hurricane produced a 7&nbsp;ft (2&nbsp;m) [[storm tide]], and in [[Georgia (U.S. state)|Georgia]], hurricane-force winds extended into the southwestern portion of the state,<ref name="HURDAT"/> while tropical storm force winds occurred along the coastline. In the state, moderate winds damaged trees and roofs, though the destruction was less than anticipated.<ref name="aoml"/>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of Atlantic hurricane seasons]]
*[[List of Atlantic hurricanes]]

==References==
{{Reflist|2}}

{{TC Decades|Year=1850|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1852 Atlantic Hurricane Season}}
[[Category:Atlantic hurricane seasons]]
[[Category:1850–1859 Atlantic hurricane seasons|*]]
[[Category:Articles which contain graphical timelines]]