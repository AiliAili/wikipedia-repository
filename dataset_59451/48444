{{advert|date=May 2015}}
{{Infobox company
|name  = HelioPower, Inc.
|logo  = [[File:HelioPower.jpg|220px]]
|foundation    = [[Fallbrook, California]]
|founder = Maurice Rousso
|location_city  = 25747 Jefferson Avenue, [[Murrieta, California]], 92562
|location_country = [[United States|U.S.]]
|locations = 4 (2014)
|num_employees = 120 (2013)
|key_people    = Ian Rogoff <br />'''Chairman'''<br />Scott Gordon<br />''' President'''<br />Maurice Rousso <br />'''COO'''<br />Vicki Zelfer <br />'''Treasurer'''<br />Steve Huang<br />'''VP of Business Operations'''<br />Jonah Liebes<br />'''SVP of Engineering & Operations'''<ref name="HelioPower">{{cite web|url=http://heliopower.com/about-us/management-team|title=Management Team|publisher=HelioPower.com|accessdate=2014-06-30}}</ref>
|homepage      = [http://heliopower.com/ www.heliopower.com]
}}
[[File:HPI HQ.jpg|thumb|250px|HelioPower, Inc. headquarters in Murrieta, CA]]
[[File:HPI Vehicles.JPG|thumb|250px|HelioPower, Inc.'s installation trucks & company vehicles]]
'''HelioPower, Inc.''' is a US-based provider of integrated energy [[solution]]s<ref>{{cite web|title=Client Spotlight: HelioPower|url=http://www.answerconnect.com/blog/client_spotlight/heliopower|publisher=AnswerConnect.com|accessdate=2014-06-30}}</ref> for residential, commercial, industrial, public sector and non-profit organizations. Founded in 2001, HelioPower also develops proprietary projects with third-party [[Project finance|offtakers]] and currently owns and operates more than 100 energy systems at facilities like the [[Tech Museum of Innovation]] in San Jose<ref>{{cite web|title=SAN JOSE’S TECH MUSEUM OF INNOVATION DEDICATES 185-KILOWATT SUNPOWER SOLAR SYSTEM|url=http://www.sierraangels.com/sjtechhmu|publisher=SierraAngels.com|accessdate=2014-06-30}}</ref><ref>{{cite web|first=San Jose Tech Museum|title=SJTM Solar System|url=http://www.thetech.org/about-us/media-room/san-joses-tech-museum-innovation-dedicates-185-kilowatt-sunpower-solar-system|publisher=TheTech.org|accessdate=2014-04-14}}</ref> & [[Ronald McDonald House]] in San Diego.<ref>{{cite news|last=San Diego|first=Ronald McDonald House|title=First Ronald McDonald House in California Goes Solar|url=https://www.bloomberg.com/apps/news?pid=conewsstory&tkr=AVX:GR&sid=aYmI.r44KiFM|accessdate=2014-06-30|newspaper=Bloomberg}}</ref><ref>{{cite web|last=Breslin|first=Mike|title=Ronald McDonald House in San Diego First in California to Go Solar|url=http://www.ecmag.com/section/miscellaneous/ronald-mcdonald-house-san-diego-first-california-go-solar|publisher=ECMag.com|accessdate=2014-06-30}}</ref>

== Products and Services ==

HelioPower operates 6 business lines:<ref>{{cite web|first=Business Week|title=Business Week Product Snapshot|url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=33106386|accessdate=2014-06-30|newspaper=Business Week}}</ref>
# Residential solar
# Energy analytics
# Engineering, procurement and construction ([[Engineering, procurement and construction|EPC]]) for distributed energy projects and facility improvements
# Energy project finance and advisory services
# Energy asset management including Operations & Maintenance (O&M) services
# PredictEnergy™, an energy analytics software platform

=== Residential Solar ===

HelioPower Inc., is a top 10 installer in California<ref>{{cite web|title=PV Solar Report 2013|url=http://heliopower.com/bc/PV_Solar_Report_Nov_2013.pdf|publisher=PV Solar Report|accessdate=2014-02-20}}</ref><ref>{{cite web|title=Consumer Report Ranking|url=http://murrieta.patch.com/groups/billie-raffertys-blog/p/bp--consumer-report-ranks-heliopower-the-value-leader1f28e04607|publisher=Murrieta.Patch.com|accessdate=2014-06-30}}</ref><ref>{{cite web|title=Consumer Report Ranks HelioPower the Value Leader in California Home Solar|url=http://www.bizjournals.com/prnewswire/press_releases/2012/03/22/LA74538|publisher=PRNewsWire.com|accessdate=2014-06-30}}</ref> having installed over 4,000 residential solar voltaic [[solar photovoltaic|PV]] systems in California and Nevada. They offer direct-to-customer or direct-to-partner services. Direct-to-customer services include complimentary energy audits, energy efficiency upgrades, and solar systems.<ref>{{cite web|title=One Block Off the Grid (1BOG) Offers San Diego Residents Lowest Solar Price Ever Through Group Discount Program|url=http://www.wlns.com/story/11476677/one-block-off-the-grid-1bog-offers-san-diego-residents-lowest-solar-price-ever-through-group-discount-program|publisher=WLNS.com|accessdate=2014-06-30}}</ref><ref>{{cite news|last=Naiman|first=Joe|title=HelioPower gets One Block Off the Grid contract|url=http://www.thevillagenews.com/story/44685|accessdate=2014-06-30|newspaper=The Village News|date=2010-1-14}}</ref>  HelioPower offers numerous residential financing options including leases and power purchase agreements, utility rebate programs, [[PACE financing|PACE financing programs]] including the [[HERO Program|HERO]] program,<ref>{{cite web|title=Supervisor Jeff Stone Takes Advantage of Solar HERO Program|url=http://murrieta.patch.com/groups/politics-and-elections/p/supervisor-jeff-stone-takes-advantage-of-solar-hero-program|publisher=Murrieta.Patch.com|accessdate=2014-06-30}}</ref><ref>{{cite news|title=Learn about HERO financing for solar energy|url=https://fridayflyer.com/2013/04/19/learn-about-hero-financing-for-solar-energy?print=1|accessdate=2014-06-30|newspaper=Friday Flyer|date=2014-04-19}}</ref> and State and Federal energy incentives.

Direct-to-partner services include full EPC offerings as well as third-party financing for qualified partners.<ref>{{cite web|title=HelioPower And SunRun Offer California Homeowners Affordable Solar|url=http://www.solardaily.com/reports/HelioPower_And_SunRun_Offer_California_Homeowners_Affordable_Solar_999.html|publisher=SolarDaily.com|accessdate=2014-06-30}}</ref>

=== Energy Analytics ===

Energy analytics synthesizes three distinct but inter-related pieces of information: (1) How much energy and power a business uses, and when and where it’s used; (2) what a business makes, moves or stores; where it performs this work, when it performs this work, and how this work changes and scales; and, (3) what a business pays, and how it procures, energy and power. HelioPower provides energy analytics consulting services to the water/waste-water, manufacturing and distribution industries.

=== Engineering, Procurement, & Construction (EPC) ===

HelioPower offers turnkey EPC solutions including solar PV, energy storage and facility improvement measures as well as systems integration solutions including CHP and bio-mass. HelioPower also decouples its EPC capabilities and offers discrete engineering, procurement and construction services to end users and industry partners. HelioPower has performed engineering and construction management services throughout the US and worldwide.

HelioPower operates with a structured project planning and control (PP&C) methodology based on PMI's (the [[Project Management Institute]]) [[PMBOK]].

=== Energy Project Finance & Advisory Services ===

HelioPower raises and syndicates project finance for proprietary and third-party energy projects and provides financing options for all its customer segments. Offerings include power purchase agreements (PPA),<ref>{{cite web|title=A SOLAR CHRISTMAS MIRACLE?|url=http://pvsolarreport.com/blog/item/1500-a-solar-christmas-miracle-in-california|publisher=PVSolarReport.com|accessdate=2014-06-30}}</ref> leases, and/or subsidized energy loans.  All financing products are structured to lower the overall cost of energy and significantly reduce or eliminate the upfront cost of purchasing an energy system. HelioPower also offers energy finance advisory services for avoided cost analyses, project modeling, project placement, and best fit analyses.

=== Asset Management Including Operations & Maintenance (O&M) ===

In addition to distributed generation asset management, HelioPower provides operations and maintenance services specifically for solar assets, battery backup systems, off-grid systems and solar manufacturer warranty and after-market support. HelioPower’s asset management capabilities grew out of its proprietary portfolio of distributed generation assets. HelioPower currently manages over 20 megawatts of commercial and industrial sector solar assets.<ref>{{cite web|title=Helio Power - Prather Ranch // Food Energy|url=http://vimeo.com/54580403|accessdate=2004-06-30}}</ref><ref>{{cite web|title=HACSB and HelioPower Complete Multifamily Affordable Solar Housing Project in California|url=http://www.azocleantech.com/news.aspx?newsID=15913|publisher=AZOCleanTech.com|accessdate=2014-06-30}}</ref>

=== PredictEnergy<sup>TM</sup> Energy Analytics Software ===

PredictEnergy<sup>TM</sup> is a cloud-based commercial and industrial energy analytics software platform that tracks, analyzes and predicts energy cost and energy intensity based on three dimensions:
# [[energy intensity|Energy Intensity]] - energy and power by date, time and location
# Business performance - production, distribution or processing volume by date, time and location
# Cost structure - [[Electric utility|electric utility tariff]], [[distributed generation]], work rules and [[demand response]] impact

The PredictEnergy<sup>TM</sup> software platform is specifically designed to address energy intensity in the manufacturing, distribution, and water/waste-water industries.

== Locations ==

HelioPower, Inc. was founded in [[Fallbrook, California]]. Since 2007, HelioPower has been headquartered in [[Murrieta, California]].<ref>{{cite web|title=Murrieta Solar Company HelioPower Announces Move to a Larger Location|url=https://www.prbuzz.com/energy/149197-murrieta-solar-company-heliopower-announces-move-to-a-larger-location.html|publisher=PRBuzz.com|accessdate=2014-06-30}}</ref> HelioPower has the following additional locations: [http://heliopower.com/areas/solar-san-diego/ San Diego County], [[San Francisco]], [[Concord, California|Concord]], and [[Coachella Valley]], California, as well as [[Incline Village, Nevada]]. This strategic coverage allows HelioPower the ability to service residential and commercial customers in California statewide.

== Helio Micro Utility and Acquisition of Greenzu, Inc. ==

In 2007 HelioPower spun out Helio Micro-Utility in order to provide specialized project finance services to energy investors, financial institutions, and end-users<ref>{{cite web|title=Leading Solar Asset Developer, Helio Micro Utility, Acquires Simple Energies LLC|url=http://www.prnewswire.com/news-releases/leading-solar-asset-developer-helio-micro-utility-acquires-simple-energies-llc-90735654.html|publisher=PRNewsWire.com|accessdate=2014-06-30}}</ref>
In 2008, Helio Micro-Utility developed the Helio Green Energy Fund with CitiBank for non-profit corporations and affordable housing communities.<ref>{{cite news|title=Citi and Helio Micro Utility Announce the Green Energy|url=https://www.bloomberg.com/apps/news?pid=conewsstory&refer=conews&tkr=C:US&sid=aK71OytVXWsw|accessdate=2014-03-10|newspaper=Bloomberg L.P.|date=2008-08-14}}</ref>
On August 20, 2013 HelioPower officially acquired Greenzu, Inc.,<ref>[http://www.prnewswire.com/news-releases/heliopower-acquires-greenzu-to-strengthen-energy-finance-services-220442751.html/ HelioPower Acquires Greenzu To Strengthen Energy Finance Services]</ref><ref>{{cite web|last=Wheeland|first=Matthew|title=Solar Consolidation|url=http://solarenergy.net/News/2013082301-solar-consolidation-heliopower-solarcity-real-goods-make-big-acquisitions/|publisher=solarenergy.net|accessdate=4/7/2014}}</ref><ref>{{cite web|title=Heliopower acquires ‘zero money down’ solar company Greenzu|url=http://www.pv-tech.org/news/heliopower_acquires_zero_money_down_solar_company_greenzu|publisher=PVTech.org|accessdate=2014-06-30}}</ref><ref>{{cite web|title=HelioPower Acquires Solar Finance Firm|url=http://solarindustrymag.com/e107_plugins/content/content.php?content.13118|publisher=SolarIndustryMag.com|accessdate=2014-06-30}}</ref> a national provider of solar finance solutions for commercial and non-profit customers. 
The acquisition expanded HelioPower's suite of energy financing products.

== Solar Water Funding Alliance ==

On May 28, 2014, HelioPower partnered with Panasonic Eco Solutions North America ([[Panasonic]]) and its strategic partner Coronal Group to create the Solar Water Funding Alliance,<ref>{{cite web|title=Coronal Group and Panasonic Eco Solutions North America Team With HelioPower to Create Solar Water Funding Alliance|url=https://finance.yahoo.com/news/coronal-group-panasonic-eco-solutions-130000573.html|publisher=Finance.Yahoo.com|accessdate=2014-06-30}}</ref><ref>{{cite web|title=Coronal Group and Panasonic Eco Solutions North America Team With HelioPower to Create Solar Water Funding Alliance|url=http://www.marketwatch.com/story/coronal-group-and-panasonic-eco-solutions-north-america-team-with-heliopower-to-create-solar-water-funding-alliance-2014-05-28|publisher=MarketWatch.com|accessdate=2014-06-30}}</ref> a multimillion-dollar effort focused solely on developing, building, financing and managing solar PV installations for California's water agencies. Recognizing the urgent need among water agencies to reduce expenses, this alliance enables these agencies to save significantly on their electric bill by converting to photovoltaic solar energy with zero upfront costs. As a result, water agencies are able to transfer the money they were paying the electric companies back into water projects to help their own ratepayers.

== Top Workplace Award==

On December 10, 2014 at the Victoria Club in [[Riverside, California]], HelioPower was awarded by [[the Press-Enterprise]]<ref>{{cite news|last=Buck|first=Fielding|title=The Inland Empire Top Workplaces 2014|url=http://www.topworkplaces.com/frontend.php/regional-list/list/pressenterprise|publisher=TopWorkplaces.com|accessdate=2014-12-16|newspaper=The Press Enterprise|date=2012-12-11}}</ref> as a Top Workplace for [[Inland Empire]]<ref>{{cite web|title=The Inland Empire Top Workplaces 2014|url=http://www.topworkplaces.com/frontend.php/regional-list/list/pressenterprise|publisher=TopWorkplaces.com|accessdate=2014-12-16}}</ref> and [[Riverside County]]. The award recognizes that Heliopower maintains a positive and productive work environment that allows them to outperform their peers through their attention to what matters in terms of employees, company culture, and contribution to their customers and their community. Through this honor, HelioPower is the number one Top Workplace in Solar and Services in 2014.

==See also==
*[[Efficient energy use]]
*[[Index of solar energy articles]]
*[[List of energy storage projects]]
*[[Outline of energy]]
*[[Solar power]]
*[[Solar power in California]]

== References ==

{{reflist}}

==External links==
{{Commons category|HelioPower}}
*[http://www.heliopower.com/ Official HelioPower, Inc. website]
*[http://www.predictenergy.com/ Official PredictEnergy™ website]
*[http://heliopower.com/solar-maintenance/ Official HelioPower, Inc. Solar Maintenance website]
*[[Wikipedia:WikiProject Energy|Wikipedia's Project Energy]]

{{Solar power in the United States}}

{{DEFAULTSORT:HelioPower, Inc.}}
[[Category:Solar energy companies of the United States]]
[[Category:Solar energy in California]]
[[Category:Energy companies established in 2001]]
[[Category:Renewable resource companies established in 2001]]
[[Category:2001 establishments in California]]
[[Category:Companies based in Riverside County, California]]
[[Category:Companies based in Contra Costa County, California]]