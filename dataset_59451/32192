{{Infobox Aircraft occurrence
|name             = 1955 MacArthur Airport United Airlines crash
|occurrence_type  = Accident
|image            = 6609-UAL-DC-6-NorthRampStapletonDEN.jpg
|image_size       = 240
|alt              = A photograph of an aircraft with four propellers in the air
|caption          = A United Airlines [[Douglas DC-6]], similar to ''Mainliner Idaho'', the aircraft involved<br />
in the accident
|date             = April 4, 1955<ref name=office/>
|type             = [[Pilot error]]<ref name=ASN>{{cite web|title=Accident description|url=http://aviation-safety.net/database/record.php?id=19550404-0|publisher=[[Aviation Safety Network]]|accessdate=October 22, 2010}}</ref>
|site             = [[Long Island MacArthur Airport]], [[Ronkonkoma, New York|Ronkonkoma]], [[Islip (town), New York|Islip]], [[New York (state)|New York]], [[United States]]<ref name=ASN/>
|coordinates      = ({{Coord|40|47|43|N|073|06|01|W|region:US|display=inline,title}})
|aircraft_type    = [[Douglas DC-6]]<ref name=ASN/>
|aircraft_name    = ''Mainliner Idaho''<ref name=Job45>Job 2001, p. 45.</ref>
|operator         = [[United Airlines]]
|tail_number      = N37512<ref name=office>{{cite web|title=Archives 1955 Jan-Jun|url=http://www.baaa-acro.com/archives/1955-JAN-JUN.htm|publisher=Aircraft Crashes Record Office|accessdate=December 26, 2010}}</ref>
|origin           = [[Long Island MacArthur Airport]], [[Ronkonkoma, New York|Ronkonkoma]], [[Islip (town), New York|Islip]], [[New York (state)|New York]], [[United States]]<ref name=ASN/>
|stopover         = 
|stopover0        = 
|stopover1        = 
|stopover2        = 
|stopover3        = 
|last_stopover    = 
|destination      = [[LaGuardia Airport]], [[New York City]], [[New York (state)|New York]], [[United States]]<ref name=ASN/>
|passengers       = 
|crew             = 3<ref name=Times/>
|injuries         = 0
|fatalities       = 3 (all)<ref name=Times/>
|survivors        = 0}}
On April 4, 1955, a [[United Airlines]] [[Douglas DC-6]] named ''Mainliner Idaho'' crashed shortly after taking off from [[Long Island MacArthur Airport]], in [[Ronkonkoma, New York|Ronkonkoma]], [[Islip, New York]], United States.

The flight was operated for the purpose of maintaining the currency of the [[instrument rating]] of two of the airline's pilots. Shortly after takeoff and only seconds after climbing through {{convert|150|ft|m}}, the plane began banking to the right. It continued to roll through 90&nbsp;degrees; the nose then dropped suddenly and moments later it struck the ground. All three members of the flight crew were killed upon impact.

A subsequent investigation found a simulated [[engine failure]] procedure was being conducted, which involved a member of the crew pulling back the [[throttle]] lever for [[Aircraft engine position number|engine No.]] 4 prior to taking off. Investigators found that if the throttle lever was pulled back too far, it would cause the propeller to [[controllable-pitch propeller|reverse]]&mdash;a feature designed to [[thrust reversal|slow the aircraft]] upon landing. Once the landing gear was raised, the crew would have to raise a metal flag in the cockpit to bring the propeller blades back into the correct position, since a safety device prevented electric power from operating the rotating mechanism at the roots of the blades unless the aircraft was on the ground or the flag was manually raised. The [[Civil Aeronautics Board]] (CAB) concluded one of the flight crew applied full power to No. 4 engine, thinking this would bring the aircraft out of the increasing bank. Because the blades were reversed and the flag was not raised, that increased the reverse thrust from No. 4 engine, causing the DC-6 to spiral out of control. Since the plane was so close to the ground, the suddenness of the bank and dive meant the flight crew had no chance to recover the aircraft before impact.

In the aftermath of the accident, the [[Civil Aeronautics Administration (United States)|Civil Aeronautics Administration]] (CAA) issued an [[Airworthiness Directive]] ordering all DC-6 and DC-6B aircraft to be fitted with a manual device which could prevent the inadvertent reversal of the propeller blades. United Airlines also stated they had begun installing reverse thrust indicator lights in the cockpits of their DC-6 aircraft, which would warn pilots when a propeller had reversed.

==History==
On April 4, 1955, a United Airlines check captain, Stanley C. Hoyt, age 45, was carrying out [[instrument rating]] checks on two of the airline's pilots. Hoyt had been employed by United Airlines since 1937, and had 9,763 flying hours experience, 549 of which were in a DC-6.<ref name="report"/> He was training the two pilots, Henry M. Dozier, age 40, and Vernis H. Webb, age 35,<ref name="report"/> so they would be able to retain an instrument rating qualification, allowing them to fly under [[instrument flight rules]].<ref name=Job45/> The aircraft was a [[Douglas DC-6]], [[aircraft registration|registration]] N37512, [[serial number]] 43001. The airframe had flown 22,068 flying hours, and had undergone an inspection 105 hours before the accident. The aircraft was powered by four [[Pratt & Whitney R2800-CB16]] engines, fitted with [[Hamilton Standard]] 43E60-317 propellers.<ref name="report"/>

The weather on the day of the accident was clear, although there was a strong wind of about {{convert|20|knots|km/h|}} hitting the airfield from the southwest, with occasional gusts of wind as fast as {{convert|30|knots|km/h|}}.<ref name=Job45>Job 2001, p. 45.</ref> The aircraft made several circuits, taking off and landing again, before eyewitnesses observed the aircraft standing at the end of the [[runway]] and then taking off at about 15:50 [[Eastern Time Zone|Eastern Standard Time]].<ref name=Job45/><ref name=Times>{{cite news |title=L. I. Crash Kills 3 Veteran Airline Pilots On Take-Off During Routine Check Hop |url=https://select.nytimes.com/gst/abstract.html?res=F40D11F93E5A107A93C7A9178FD85F418585F9 |accessdate=December 26, 2010 |newspaper=New York Times |date=April 5, 1955}} {{Subscription required}}</ref> The takeoff weight was around {{convert|61,000|lb|kg|}}, far below the aircraft's maximum permissible weight and the [[Center of gravity of an aircraft|center of gravity]] was within the prescribed limits for the model of aircraft.<ref name=Job45/><ref name="report"/>

Between {{convert|1500|ft|m|}} and {{convert|1800|ft|m|}} down the runway, the aircraft reached [[V speeds|take-off speed]], lifted off the ground, and began climbing normally as the crew retracted the [[landing gear]]. Upon climbing through {{convert|50|ft|m|}}, the aircraft began [[Banked turn|banking]] to the right. The climbing bank continued to increase at a rate which alarmed witnesses, and soon after the aircraft rolled through 90[[Degree (angle)|°]] (at which point the wings were vertical to the ground).<ref name=Job45/><ref name="report"/> At a height of around {{convert|150|ft|m|}}, with all four engines producing take-off [[thrust]], the nose began to fall. Moments later the right wing and nose impacted the ground, causing the fuselage to cartwheel over, before the aircraft came to rest, with the correct side up. It was immediately engulfed in flames. All three members of the flight crew were instantly killed. Although [[emergency services]] at Long Island MacArthur promptly responded to the crash, the aircraft was destroyed by the post-crash fire.<ref name=Job45/><ref name="report"/>

==Investigation==

===Wreckage examination===
The [[Civil Aeronautics Board]] (CAB), charged with investigating the accident, examined the wreckage at Long Island MacArthur Airport. Reports from witnesses of the crash indicated the aircraft appeared to have made a normal takeoff and began climbing normally. But, moments later it began banking sharply to the right. Investigators examined the four charred engines and concluded all were producing power at the point of impact.<ref name=Job45/><ref name="report"/> They could not conclusively determine the amount of power being produced, but stated there was no evidence found in the wreckage that suggested the engines might have suffered an operational failure.<ref name="report"/><ref name=Job46>Job 2001, p. 46.</ref>

They were also able to determine all of the [[flight control surfaces]], including the [[elevator (aeronautics)|elevators]], [[aileron]]s and [[rudder]], were functioning properly at the point of the crash, and there were no faults in the [[Aircraft flight control system|flight control system]]. The [[flap (aeronautics)|flaps]] were extended to between 15° and 20°, the standard setting for take-off. The propeller blades of No. 4 engine&mdash; on the far right side of the aircraft&mdash;were reversed&mdash;minus 8°, while the blades of Nos. 1, 2 and 3 engines were at 34° positive [[Blade pitch|pitch]] (also standard for take-off).<ref name="report"/><ref name=Job46/>

===Reversed thrust===
[[File:1946-02-21 New Airliner.ogv|thumb|alt=An aircraft with four propellers starts its engines. A member of the flight crew waves to the camera from inside the cockpit. The propellers of one engine rotate. A large crowd of people observes the aircraft taxiing and taking off from a runway. There are passengers inside; a stewardess hands one a magazine. A stewardess removes a food tray from a cupboard. Several passengers leave their seats before a stewardess converts them into two beds. A female passenger brushes her hair; a male passenger is seen shaving in a mirror. Two passengers are in separate beds, one above the other; they close the curtains surrounding the beds. A DC-6 then performs a flypast and the video cuts to black.|A video of a [[Douglas DC-6]].]]
The propellers of a DC-6 are designed to provide [[Thrust reversal|reverse thrust]] after the aircraft touches down. The pilot then retards the throttle levers to a point below idle speed and that directs the electric mechanisms in the propeller hub to [[controllable-pitch propeller|rotate the blades]] to a position in which they will provide reverse thrust.<ref name=Job46/> Should the pilot need to perform a [[go-around]], he moves the thrust levers forward to a positive position again and that will produce forward thrust, enabling the pilot to execute a go-around maneuver.<ref name=Job46/>

The [[Douglas Aircraft Company]] designed a system that would prevent the accidental reversal of propeller blades in-flight. During development of the DC-6, the company installed a system that cut electrical power to the mechanisms which rotated the blades while the airplane was in the air. When there was enough weight on the [[landing gear]] (which would only be the case when the aircraft was on the ground), a switch which supplied electrical power to the mechanisms was closed&mdash;meaning that when the aircraft touched down the blades could be reversed and thus the airplane could be slowed. When the switch was closed, a red flag would swing into view in the cockpit of the aircraft, warning the crew that the blades could be reversed. Should the switch fail to close upon landing, the flag could be raised manually and electrical power to the mechanisms would be restored. When the aircraft took off, electrical power would be cut to the mechanisms so that the propeller blades could not be inadvertently reversed, and the red flag swung out of sight. Reverse thrust warning lamps, which would have warned the crew if the propellers were reversed, were not fitted on ''Mainliner Idaho''.<ref name=Job46/>

===Flight tests===
The CAB carried out flight tests using a DC-6. They found that if the propellers were reversed prior to take-off they would not, if the flag was not raised, be rotated automatically again in the air to produce forward thrust if full power was applied. Tests performed by United Airlines showed that, if the propellers of just one engine were reversed and full power was applied to all four engines, then the aircraft would spiral into a dive. If METO (maximum except take-off) power was applied to Nos. 1, 2 and 3 engines, and full reverse thrust was applied to engine No. 4, then the aircraft would become uncontrollable.<ref name="report"/><ref name=Job47/>

If full left [[aileron]] was applied, the aircraft could be recovered for a short period of time, but a violent turn to the right would continue, and the competing forces would cause the aircraft to [[stall (flight)|stall]], and violently roll and pitch down. Flight tests, investigators said, accurately reproduced what happened to ''Mainliner Idaho'' during the accident sequence. The tests performed by United and by the investigators showed that if, after the aircraft became airborne, full power was applied to an engine whose propellers were reversed, the propellers would produce not positive thrust, but increased reverse thrust.<ref name="report"/><ref name=Job47>Job 2001, p. 47.</ref> One aviation author wrote of the crash,

{{quote|"The flight tests showed conclusively that, at take-off configuration, a DC-6 becomes uncontrollable with an outboard engine at full power with its propeller in reverse pitch. Control is lost so quickly that there is little the crew can do at low altitude. In the case of this accident, it was doubtful if there would have been time for forward thrust to be restored before control was lost."|[[Macarthur Job]]|''Air Disaster Vol. 4'', 2001<ref name=Job47/>}}

===Conclusions===
While the wreckage was being examined, investigators found that all four engines were producing thrust at the time of impact. There were only two ways that the propeller could be reversed during the take-off sequence. Investigators ruled out electrical malfunction since, after detailed examination of the engine hub, there was no evidence found of this happening.<ref name="report"/><ref name=Job49/> Therefore, it was concluded that the only way the propeller could have been reversed was through an unintentional crew action. Although there was no formal evidence that a simulated [[engine failure]] was being performed, statements submitted by witnesses suggested that it was likely this was the case. United Airlines procedure calls for No. 4 engine to be shut down in a simulated engine failure&mdash;the same engine which was found at the crash site with its propellers reversed.<ref name="report"/><ref name=Job49>Job 2001, p. 49.</ref>

[[File:DC-6B Hamilton Standard propeller.JPG|thumb|The propellers of a [[Douglas DC-6]] engine, similar to those installed on the engines of ''Mainliner Idaho''|alt=A set of propeller blades, unattached from an aircraft mounting.]]
The investigation concluded the accident sequence began when the check pilot, while the aircraft was on the ground, retarded the throttle lever for No. 4 engine past the idle position, and therefore reversed the propellers of that engine. Once the airplane took off and started banking to the right, it would have been a natural reaction for one of the flight crew to increase power to No. 4 engine, thinking that by doing so the engine would start producing positive thrust and the aircraft could be recovered. However, since the metal flag was not raised, there was no electrical power to the rotating mechanisms&mdash;and increasing power to No. 4 engine would only have created more reverse thrust.<ref name="report"/><ref name=Job49/>

The final accident report concluded there wasn't sufficient time for the crew to react, since the dive began suddenly while the plane was so close to the ground. "Control will be lost so quickly that there is little, if anything, that the pilot can do if it occurs at low altitude," the report stated. "He must recognize what is occurring, analyze it, and take action to unreverse in a very limited amount of time. It is doubtful that unreversing could have been accomplished in this instance before control was lost."<ref name="report">{{cite web |title=Accident Investigation Report; United Air Lines, Inc., MacArthur Field, Islip, New York |year=1955 |accessdate=January 12, 2011 |publisher=[[Civil Aeronautics Board]] |url=http://specialcollection.dotlibrary.dot.gov/Document?db=DOT-AIRPLANEACCIDENTS&query=%28select+611%29}}</ref>

On October 4, 1955, the CAB released the final accident report, which concluded the reversal of the propellers and subsequent increase in power of the No. 4 engine had caused the accident.<ref name="report"/>

{{quote|"The Board determines that the probable cause of this accident was unintentional movement of No. 4 throttle into the reverse range just before breaking ground, with the other three engines operating at high power output, which resulted in the aircraft very quickly becoming uncontrollable once airborne."|[[Civil Aeronautics Board]]|''Accident Investigation Report; United Air Lines, Inc., MacArthur Field, Islip, New York'', 1955<ref name="report"/>}}

==Aftermath==

===Technological advances===
Following the accident, the [[Civil Aeronautics Administration (United States)|Civil Aeronautics Administration]] (CAA) issued an [[Airworthiness Directive]] ordering all DC-6 and DC-6B aircraft to be fitted with a sequence gate latch, known as a Martin bar. The device is a metal bar which a crew would manually swing in front of the thrust levers over the idle line, physically preventing the thrust levers from being retarded into the reverse position. According to the CAB report, a United Airlines engineer told investigators the Martin bar should make propeller reversal "a more reliable and safer device [than the system fitted to ''Mainliner Idaho''] ... with its numerous switches, relays, and automatic operation."<ref name="report"/>

United Airlines issued a statement saying it had begun installing the device on its fleet of DC-6 and DC-6B aircraft one week before the accident, having used it successfully in service on their fleet of [[Douglas DC-7]] aircraft. A Martin bar had not yet been fitted on ''Mainliner Idaho''.<ref name=Job47/> United Airlines also said a program had begun to install reverse thrust indicator lights on all their DC-6 and DC-6B aircraft. The signals, fitted in the cockpit of the aircraft, would have warned the flight crew that the thrust lever had been pulled back too far, and the propellers had been reversed.<ref name="report"/>

===Similar accidents===
[[File:Boeing 767-3Z9-ER Lauda Air ZRH.jpg|thumb|A [[Lauda Air]] [[Boeing 767]], similar to the aircraft which [[Lauda Air Flight 004|crashed]] in 1991 after one of the [[thrust reverser]]s deployed in-flight.]]
Since the crash, there have been several [[Thrust reversal#Thrust reversal-related accidents|other accidents]] involving reverse thrust. A [[Douglas DC-8]] operating [[United Airlines Flight 859]] crashed in 1961 when the first officer attempted to reverse all four engines during the landing roll. The left engines remained in forward thrust, while the right engines went into reverse, causing the aircraft to veer rapidly to the right and collide with airport construction vehicles, killing 17 of the 122 people aboard and 1 person on the ground.<ref>{{cite web |url=http://dotlibrary.specialcollection.net/Document?db=DOT-AIRPLANEACCIDENTS&query=(select+736) |title=Aircraft Accident Report: United Airlines Flight 859|date=July 16, 1962|publisher=Civil Aeronautics Board}}</ref>
[[Japan Airlines Flight 350]], a DC-8, crashed in 1982 short of the runway in Tokyo, after the mentally ill captain attempted suicide during the final approach phase of the flight, by putting the inboard engines into reverse thrust. Of the 174 people aboard, 24 died.<ref name="Airdisaster">[http://www.airdisaster.com/cgi-bin/view_details.cgi?date=02091982&reg=JA8061&airline=Japan+Air+Lines Accident Database: Accident Synopsis 02091982]</ref><ref name="Cockpit Fight">[https://query.nytimes.com/gst/fullpage.html?sec=health&res=9803E2DF1F38F937A25751C0A964948260 COCKPIT FIGHT REPORTED ON JET THAT CRASHED IN TOKYO]", ''[[The New York Times]]''. February 14, 1982. Retrieved on June 24, 2011.</ref><ref name="Time story">{{cite news| title = Troubled Pilot| work=[[Time (magazine)|Time]]| url = http://www.time.com/time/magazine/article/0,9171,922801,00.html?iid=chix-sphere| accessdate = 2007-04-20| date=1982-03-01}}</ref><ref>[http://jtsb.mlit.go.jp/jtsb/aircraft/download/bunkatsu.html#7 Final Accident Report]</ref>
In 1991, [[Lauda Air Flight 004]], operated by a [[Boeing 767]], crashed after the left engine thrust reverser deployed in-flight for reasons that could not be determined.<ref>{{cite web|title=Lauda Air B767 Accident Report |url=http://www.rvs.uni-bielefeld.de/publications/Incidents/DOCS/ComAndRep/LaudaAir/LaudaRPT.html |publisher=Aircraft Accident Investigation Committee of Thailand |accessdate=January 23, 2011 |deadurl=yes |archiveurl=http://www.webcitation.org/5yzGW2x05?url=http%3A%2F%2Fwww.rvs.uni-bielefeld.de%2Fpublications%2FIncidents%2FDOCS%2FComAndRep%2FLaudaAir%2FLaudaRPT.html |archivedate=May 27, 2011 |df= }}</ref> The [[TAM Transportes Aéreos Regionais Flight 402|crash]] of a [[TAM Airlines]] [[Fokker 100]] in 1996 was attributed to the deployment of the thrust reverser on No. 2 engine. The aircraft rolled to the right and crashed in a populated area of [[São Paulo]], Brazil.<ref>{{cite web |title=Accident description |url=http://aviation-safety.net/database/record.php?id=19961031-0 |publisher=[[Aviation Safety Network]] |accessdate=January 23, 2010}}</ref>
{{Clear}}

==See also==
* [[1955 in aviation]]
* [[Aviation safety]]
* [[List of accidents and incidents involving commercial aircraft]]
{{Portal bar|1950s|Aviation|New York}}

==References==
; Notes
{{Reflist|colwidth=30em}}
; Bibliography
{{refbegin}}
* {{Cite book |authorlink=Macarthur Job |last=Job |first=Macarthur |title=Air Disaster |volume=4 |chapter=A fatal propeller reversal |year=2001 |publisher=Aerospace Publications |isbn=1-875671-48-X}}
{{refend}}

{{Aviation accidents and incidents in 1955}}

{{featured article}}

{{DEFAULTSORT:Macarthur Airport United Airlines Crash, 1955}}
[[Category:1955 in New York]]
[[Category:Accidents and incidents involving the Douglas DC-6]]
[[Category:Airliner accidents and incidents caused by pilot error]]
[[Category:Airliner accidents and incidents in New York]]
[[Category:Articles containing video clips]]
[[Category:Aviation accidents and incidents in the United States in 1955]]
[[Category:Islip (town), New York]]
[[Category:United Airlines accidents and incidents]]