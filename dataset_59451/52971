{{update|date=February 2017}}

{{Infobox Journal
| title = The Condor: Ornithological Applications 
| cover = [[File:TheCondor1922.jpg|200px|Cover art of ''The Condor'' in 1922.]]
| discipline = [[Ornithology]]
| abbreviation = Condor
| editor = Philip C Stouffer
| publisher = Central Ornithology Publication Office for the [[Cooper Ornithological Society]]
| country = [[United States]]
| frequency = Weekly
| website = http://www.aoucospubs.org/loi/cond
| history = 1899–present
| ISSN = 0010-5422
| eISSN = 1938-5129
}}
'''''The Condor: Ornithological Applications''''' is a [[Peer review|peer-reviewed]] weekly [[scientific journal]] covering [[ornithology]]. It is the official journal of the [[Cooper Ornithological Society]].

==History==
The journal was first published in 1899 as the ''Bulletin of the Cooper Ornithological Club'' by a group of biologists in California. The journal's scope was regional, covering the western United States. In 1900 the name was changed to ''The Condor''.  In 1947, the journal's subtitle was shortened to ''The Condor, Journal of the Cooper Ornithological Club''.<ref name="walsberg">{{cite journal|last=Walsberg|first=Glenn|year=1993|title=History of The Condor|journal=The Condor|publisher=Cooper Ornithological Society|volume=95|issue=3|pages=748–757|url=http://elibrary.unm.edu/sora/Condor/files/issues/v095n03/index.php|accessdate=March 25, 2010|doi=10.2307/1369626}}</ref>

The first editor was [[Chester Barlow]] from 1899 to 1902. From 1902 to 1905 it was [[Walter K. Fisher]], and from 1906 to 1939 [[Joseph Grinnell]] edited the journal. Charles A. Nace was publisher. <ref name="walsberg" /> 

An editorial board was established in 1951 to address increasing submissions to the journal. James King, of [[Washington State University]], instituted a system for external [[peer review]] of submissions. King became editor after [[Alden H. Miller]]'s death in 1965. Miller  replaced Grinnell as editor in 1939. King widened the scope of the journal, and by 1966, at least 40% of papers published in ''The Condor'' are written by scientists outside the United States.<ref name="walsberg" />

In Glenn Walsberg's 1993 "History of ''The Condor''", he concluded that "several thousand people have contributed to the success and development of this journal in its 95-year history. In 1992 alone, 653 scientists aided in its production in the roles of author, reviewer, or both."<ref name="walsberg" />

In 2014, the [[American Ornithologists' Union]] and the Cooper Ornithological Society combined their publishing operations to create the Central Ornithology Publication Office.

==1899 editorial==
In the prose style of the time period, the first issue's editorial sets out the focus of the journal as "its object being to represent generally the great West, and primarily the Cooper Ornithological Club. It is conceded that the West is rich in its possibilities of new discoveries, both in faunal forms and data regarding the life histories of many species …"

The editorial also comments on a newspaper story from the San Francisco ''Chronicle'' about a successful hunt by the Petaluma Sportsmen's Club: "The joint bag showed 821 bluejays and 51 hawks 'of various kinds' slaughtered on the plea that 'each would have destroyed at least five quail’s eggs during the next breeding season.'" The editorial added that "the ''Bulletin'' stands for bird protection, and will strenuously oppose wanton slaughter at ail times regardless of its source."<ref>{{cite journal|last=Barlow|first=Chester|author2=Taylor, Henry Reed |author3=Robertson, Howard |date=January 1899|title=[[s:The Condor/1 (1)/Editorial Notes|Editorial Notes]]|journal=Bulletin of the Cooper Ornithological Club|publisher=Cooper Ornithological Club|volume=1|issue=1|page=14|doi=10.2307/1360788}}</ref>

==References==
{{Reflist}}

== See also ==
* [[List of ornithology journals]]

== External links ==
{{commons category|The Condor}}
{{wikisource|The Condor}}
* BioOne: [http://www.bioone.org/perlserv/?request=get-archive&issn=0010-5422 ''The Condor'']. Vol. 102 (2000) onwards; free HTML abstracts, subscription required for PDF fulltexts. Retrieved 2006-NOV-26.
* SORA: [http://elibrary.unm.edu/sora/Auk/ ''The Condor'']. Vol. 1–102 (1899–2000) free PDF/DejaVu fulltexts. Retrieved 2006-NOV-26.

{{DEFAULTSORT:Condor}}

[[Category:Journals and magazines relating to birding and ornithology]]
[[Category:Publications established in 1899]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]