{{Infobox Journal
| title	=	Journal of Porphyrins and Phthalocyanines
| cover	=	[[image:JPPcover.jpg|180px]]
| discipline	=	[[Chemistry]]
| abbreviation	=	JPP
| editor = Karl M. Kadish
| publisher	=	[[World Scientific]]
| country	=	[[Singapore]]
| impact = 1.397
| impact-year = 2014
| history = 1997 - present
| website	=	http://www.worldscientific.com/worldscinet/jpp
| ISSN	= 1088-4246
| eISSN = 1099-1409
}}
The '''''Journal of Porphyrins and Phthalocyanines''''' (JPP) was founded in 1997 and is published by [[World Scientific]]. It covers "research in the chemistry, physics, biology and technology of [[porphyrins]], [[phthalocyanine]]s and related macrocycles".<ref>World Scientific. ''Journal Aims & Scope''. http://www.worldscinet.com/jpp/mkt/aims_scope.shtml</ref> The journal also deals with the synthesis, spectroscopy, processing and applications of such compounds. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.397, ranking it 83rd out of 157 journals in the category "Chemistry Multidisciplinary".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry Multidisciplinary |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.worldscientific.com/worldscinet/jpp JPP Journal Website]

[[Category:World Scientific academic journals]]
[[Category:Chemistry journals]]
[[Category:Publications established in 1997]]
[[Category:English-language journals]]