{{Infobox journal
| title = Comparative European Politics
| cover = 
| discipline = [[Political science]]
| abbreviation = Comp. Eur. Polit.
| editor = [[Colin Hay (political scientist)|Colin Hay]]<br />Ben Rosamond<br />Martin A. Schain
| publisher = [[Palgrave Macmillan]]
| country = [[United Kingdom]]
| frequency = Quarterly
| history = 2003–present
| impact = 1.261
| impact-year = 2015
| website = http://www.palgrave-journals.com/cep/index.html
| link1 = http://www.palgrave-journals.com/cep/archive/index.html
| link1-name = Online archive
| link2 = 
| link2-name = 
| ISSN = 1472-4790
| eISSN = 1740-388X
| OCLC = 52368423
| LCCN = 2003263923
}}
'''''Comparative European Politics''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] focusing on comparative politics and the political economy of the whole of contemporary Europe within and beyond the European Union.<ref name=About>{{cite web |title=Comparative European Politics: about this journal |url= http://www.palgrave-journals.com/cep/about.html |publisher= [[Palgrave Macmillan]] |accessdate=24 February 2016 }}</ref>

The journal is published by [[Palgrave Macmillan]] and the current joint editors-in-chief are [[Colin Hay (political scientist)|Colin Hay]] ([[Sciences Po]]), Ben Rosamond, ([[University of Copenhagen]]) and Martin A. Schain, ([[New York University]]).<ref name="About" />

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|2|
* ABI/INFORM
* European Sources Online
* Ex Libris / Primo Central
* [[International Bibliography of Book Reviews of Scholarly Literature and Social Sciences|International Bibliography of Book Reviews (IBR)]]
* [[International Bibliography of Periodical Literature|International Bibliography of Periodical Literature (IBZ)]]
* [[International Bibliography of the Social Sciences|International Bibliography of the Social Sciences (IBSS)]]
* [[International Political Science Abstracts]]
* [[SCOPUS]]
* [[Sociological Abstracts]]
* [[Worldwide Political Science Abstracts]]
}}

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.261, ranking it 47th out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|1=http://www.palgrave-journals.com/cep/index.html}}


{{DEFAULTSORT:Comparative European Politics}}
[[Category:English-language journals]]
[[Category:Palgrave Macmillan academic journals]]
[[Category:Political science journals]]
[[Category:Publications established in 2003]]
[[Category:Quarterly journals]]


{{Poli-journal-stub}}