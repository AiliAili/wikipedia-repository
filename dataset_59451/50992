{{Infobox journal
| title = Conflict Management and Peace Science
| cover = [[Image:CMPS vol 26 no 5.gif]]
| editor = Glenn Palmer
| discipline = [[International relations]], [[peace and conflict studies]]
| peer-reviewed = 
| former_names = Journal of Peace Science
| abbreviation = Conflict Manag. Peace Sci.
| publisher = [[SAGE Publications]]
| country = 
| frequency = 5/year
| history = 1973-present
| openaccess = 
| license = 
| impact = 0.682
| impact-year = 2010
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201906
| link1 = http://cmps.sagepub.com/content/current
| link1-name = Online access
| link2 = http://cmps.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 8055590
| LCCN = 83644168
| CODEN = 
| ISSN = 0738-8942
| eISSN = 1549-9219
}}
'''''Conflict Management and Peace Science''''' is a [[Peer review|peer-reviewed]] [[academic journal]] appearing five times a year that publishes scholarly articles and book reviews in the field of [[international relations]] (specifically [[peace and conflict studies]]) on topics such as international conflict, [[arms race]]s, [[international trade]], [[foreign policy analysis|foreign policy]], international [[mediation]], and [[conflict resolution]]. The journal is published under the auspices of the [[Peace Science Society]].<ref>{{cite web |url=http://pss.la.psu.edu/2010%20New%20site/publications.html |title=Publications |publisher=Peace Science Society (International) |work=Homepage |accessdate=2010-11-25}}</ref> The journal includes original and review articles.

== Abstracting and indexing ==
''Conflict Management and Peace Science'' is abstracted and indexed in [[EBSCO Industries|Current Abstracts]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 0.918, ranking it 29th out of 82 journals in the category "International Relations".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: International Relations |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201906}}

[[Category:International relations journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 1973]]
[[Category:English-language journals]]