{{about|the suburb of Elizabeth|the former (physically larger) local government area|City of Elizabeth}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Elizabeth
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 952
| pop_year            = {{CensusAU|2006}}
| pop_footnotes = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41401 |name=Elizabeth (State Suburb) |accessdate=20 April 2011 | quick=on}}</ref><br>811 <small>''(2001 Census)''</small><ref name=ABS2001>{{Census 2001 AUS |id =SSC41386 |name=Elizabeth (State Suburb) |accessdate=20 April 2011 | quick=on}}</ref>
| est                 = 1955
| postcode            = 5112<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/elizabeth |title=Elizabeth, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=20 April 2011}}</ref>
| area                = 
| dist1               = 24
| dir1                = N
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = City of Playford
| stategov            = [[Electoral district of Little Para|Little Para]] <small>''(2011)''</small><ref>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov              = [[Division of Wakefield|Wakefield]] <small>''(2011)''</small><ref>{{cite web|url=http://apps.aec.gov.au/esearch |title=Find my electorate |author= |date=15 April 2011 |work= |publisher=Australian Electoral Commission |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110430090535/http://apps.aec.gov.au/esearch |archivedate=30 April 2011 |df=dmy }}</ref>
| near-n              = [[Elizabeth North, South Australia|Elizabeth North]]
| near-ne             = [[Elizabeth Park, South Australia|Elizabeth Park]]
| near-e              = [[Elizabeth East, South Australia|Elizabeth East]]
| near-se             = [[Elizabeth East, South Australia|Elizabeth East]]
| near-s              = [[Elizabeth Grove, South Australia|Elizabeth Grove]]
| near-sw             = [[Elizabeth South, South Australia|Elizabeth South]]
| near-w              = [[Edinburgh, South Australia|Edinburgh]]
| near-nw             = [[Elizabeth West, South Australia|Elizabeth West]]
}}

'''Elizabeth''' is an outer northern [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Playford]].

==History==
{{Main|City of Elizabeth}}
Before the 1950s, most of the area surrounding today's suburb of Elizabeth was farming land. After the end of the Second World War with its shortage of materials, the state government decided that South Australia needed to grow and become industrialised.

A "satellite city" was planned for the Elizabeth area, and the [[South Australian Housing Trust]] initiated a housing development program in the area, with a purchase of {{convert|1200|ha|acre|abbr=off}} of rural land between the older "country towns" (now Adelaide suburbs) of [[Salisbury, South Australia|Salisbury]] and [[Smithfield, South Australia|Smithfield]].

The Suburb of Elizabeth was formed on 16 November 1955, being named after [[Elizabeth II|Queen Elizabeth II]], who visited the suburb in 1963.

As well as the [[Elizabeth Shopping Centre|Town Centre]],<ref>[http://www.elizabethshopping.com.au Elizabeth Shopping Centre website]</ref> originally with open air shopping malls there was a theatre (named "The Octagon"),<ref>[http://www.samemory.sa.gov.au/site/page.cfm?u=967&c=7435 Ariel rocks the Octagon], 1973, Treasures of the State Library, State Library of South Australia.<br />
:The Octagon Theatre Elizabeth was a multi-use auditorium specially designed for a wide range of activities. Built by the City of Elizabeth, it was completed in August 1965 and was a centre for live entertainment for the central and northern suburbs of Adelaide until it was closed in January 2003. The floorboards from the theatre were refurbished and used in the Great Hall of the City of Playford's new Civic Centre which opened in March 2004.</ref>

==Geography==
Elizabeth is the seat of the [[City of Playford|Playford]] local government area and thus acts as a [[central business district]] for the surrounding suburbs. It lies between the [[Gawler Central railway line]] and [[Main North Road]]. [[Defence Science and Technology Organisation|DSTO]] [[Edinburgh, South Australia|Edinburgh]] is located to the west of Elizabeth.<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 952 persons in the suburb of Elizabeth on census night. Of these, 52.9% were male and 47.1% were female.<ref name=ABS2006/>

The majority of residents (66.2%) were Australian born, with 13.2% born in [[England]].<ref name=ABS2006/>

The age distribution of Elizabeth residents is similar to that of the greater Australian population. 67.5% of residents were aged 25 or over in 2006, compared to the Australian average of 66.5%; and 32.5% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

The population of Elizabeth as recorded by this census, encompassing postcodes 5112, 5113 and 5114, was about 60,000.

==Community==
The local newspaper is the [[News Review Messenger]]. Other regional and national newspapers such as [[The Advertiser (Adelaide)|The Advertiser]] and [[The Australian]] are also available.<ref>{{cite web |url=http://www.newspapers.com.au/SA/ |title=South Australian Newspapers |author= |date= |work=Newspapers.com.au |publisher=Australia G'day |accessdate=15 April 2011}}</ref> The Bunyip Newspaper<ref>[http://www.bunyippress.com.au/ The Bunyip Newspaper], www.bunyippress.com.au</ref> also covers the Elizabeth area in its Playford Times section.<ref>[http://www.bunyippress.com.au/playford-times/ Playford Times], www.bunyippress.com.au</ref>

===Schools===
[[Playford International College]] (formerly Fremont-Elizabeth City High School) is on [[Philip Highway]]. [[Kaurna Plains School]] is on Ridley Road.<ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=20 April 2011}}</ref>

===Sport===
Elizabeth is the home of the [[Central District Bulldogs]], an [[Australian rules football]] team in the [[South Australian National Football League]] (SANFL). It is currently the strongest team in South Australian football, having (in 2010) won nine of the last eleven SANFL Premierships. They play all of their home games at [[Elizabeth Oval]] (currently named "Playford Alive Oval").

Elizabeth also has an association football club (soccer), the [[Playford City Patriots]], who play in the South Australian State League. However, their home stadium is Ramsay Park in [[Elizabeth West, South Australia|Elizabeth West]].

==Facilities and attractions==

The [[Elizabeth Shopping Centre]] is in the middle of the suburb.<ref name=UBD/> Formerly known as Elizabeth Town Centre, it has been progressively expanded since the 1960s. In its early days it featured open air malls, but today it comprises a single storey undercover mall. A major renovation and extension was completed in 2004.

===Parks===
Dauntsey Reserve is located between Winterslow Road and Woodford Road. Ridley Reserve is located on the suburb's southern boundary. There are other parks and reserves in the suburb.<ref name=UBD/>

==Transportation==

===Roads===
Elizabeth is serviced by [[Main North Road]], connecting the suburb to [[Adelaide city centre]], and by [[Philip Highway]].<ref name=UBD/>

===Public transport===
Elizabeth is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date=12 January 2011 |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

====Trains====
The [[Gawler Central railway line|Gawler]] railway line passes beside the suburb. The closest station is [[Elizabeth railway station|Elizabeth]].<ref name=Metro/>

====Buses====
Elizabeth is serviced by buses run by the [[Adelaide Metro]].<ref name=Metro/>

==See also==
*[[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
{{Commons category}}
*{{cite web |url=http://www.playford.sa.gov.au |title=City of Playford |author= |date= |work=Official website |publisher=City of Playford |accessdate=20 April 2011}}

{{Coord|-34.720|138.673|format=dms|type:city_region:AU-SA|display=title}}
{{City of Playford suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1955]]