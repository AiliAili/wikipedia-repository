{{Use dmy dates|date=March 2017}}
{{Use British English|date=March 2017}}
[[File:Westray to Papa Westray air route map.png|thumb|The flight goes between two islands in Orkney, Scotland: Westray and Papa Westray. At {{convert|1.7|mi|km|order=flip}}, it is the shortest scheduled airline flight in the world as of December, 2016.]]
The '''Loganair Westray to Papa Westray route''' is the shortest scheduled passenger flight in the world. Flights on this route are scheduled for two minutes, and actual flying time is closer to one minute. The record for the fastest flight is 53 seconds. The route is flown by [[Loganair]], a Scottish [[regional airline]] that serves Scotland's Highlands and islands. It costs £17.

==Background==
The route between the [[Orkney Islands]] of [[Westray]] and [[Papa Westray]] is a subsidized [[public service obligation]]. The [[Orkney Islands Council]] awards the route, along with several other routes throughout the islands, through a tendering process. The flights began in 1967, establishing the record for the world's shortest scheduled flights, and they have been continuously operated by [[Loganair]].<ref name=DM01>{{cite news|last1=Johnson|first1=Sarah|title=Right to operate world's shortest scheduled flight at just 47 seconds sparks bidding war between aviation firms|url=http://www.dailymail.co.uk/news/article-2260352/Right-operate-worlds-shortest-scheduled-flight-just-47-seconds-sparks-bidding-war-aviation-firms.html|accessdate=21 January 2016|agency=Daily Mail|date=10 January 2013}}</ref> In 2013, the contract was again awarded to Loganair over two competing bids.<ref name=OIC01>{{cite web|last1=Orkney Islands Council|title=Loganair awarded North Isles air service contract|url=http://www.orkney.gov.uk/OIC-News/loganair-awarded-north-isles-air-service-contract.htm|website=Orkney Islands Council News|accessdate=24 January 2016}}</ref>

==Flights==
[[File:Islander at Papa Westray - cropped.jpg|thumb|left|upright=1|The world's shortest airline flight, taxiing at Papa Westray airport]]
Flights between Westray and Papa Westray occur daily in both directions, except on Saturdays, when only flights from Westray to Papa Westray are available, and on Sunday, when only flights from Papa Westray to Westray are available.<ref name=Loganair02>{{cite web|last1=Loganair|title=Timetable from 25 October 2015 to 28 February 2016|url=http://www.loganair.co.uk/xtra_files/OrkneyWinter25Oct15-28Feb16.pdf|website=Loganair Orkney Inter-Isles Air Services|accessdate=21 January 2016}}</ref><ref name=Loganair01>{{cite web|last1=Loganair|title=Timetable from 29 February to 29 October 2016|url=http://www.loganair.co.uk/xtra_files/OrkneySummer29Feb16-29Oct16.pdf|website=Loganair Orkney Inter-Isles Air Services|accessdate=21 January 2016}}</ref> The total distance covered by the flights is {{convert|1.7|mi|km|order=flip}}, which is about the same length as the runway at [[Edinburgh airport]].<ref name=BBC01>{{cite news|last1=BBC News|title=Final trip for Orkney shortest flight pilot|url=http://www.bbc.com/news/uk-scotland-north-east-orkney-shetland-22668150|accessdate=21 January 2016|agency=BBC News|date=26 May 2013}}</ref>

Pilot Stuart Linklater flew the short hop a record more than 12,000 times, more than any other pilot, before he retired in 2013. Linklater set the record for the fastest flight between the islands at 53 seconds.<ref name=BBC01 />

===Passengers===
[[File:Papa Westray Airport.jpg|thumb|left|[[Britten-Norman Islander]] being loaded for departure from Papa Westray]]
[[File:Knap of Howar 02.jpg|thumb|Archaeological site in Papa Westray, Scotland: The Knap of Howar is possibly the oldest stone house in Northern Europe.]]
Many students and their teachers take these flights to study the 60 archaeological sites on Papa Westray, making up the majority of passengers. Occasionally health professionals are needed to assist one of the island's 90 residents, and patients will also take the flight from Papa Westray to medical facilities when needed. The flight has also become popular among tourists.<ref name=DM01 /><ref name=FC01>{{cite web|last1=Clarke|first1=Chris|title=The World's Shortest Commercial Flight Takes Less Than A Minute|url=http://flightclub.jalopnik.com/the-worlds-shortest-commercial-flight-takes-less-than-a-1697446932|website=Flight Club|accessdate=21 January 2016}}</ref>

===Aircraft===
Loganair operates this flight with one of its two [[Britten-Norman Islander|Pilatus Britten-Norman BN2B-26 Islander]] aircraft. The Islander is a high-wing, twin piston engine, propeller-driven aircraft. It is flown by a single pilot, and there is seating for eight passengers in the passenger cabin. One additional seat usually remains empty next to the pilot.

Loganair's chief executive, Jim Cameron, described the Islander as "robust" and "well suited to the vagaries of Scottish weather."<ref name=Guardian01>{{cite news|last1=Seenan|first=Gerard|title=Inquiry into crash of air ambulance|url=https://www.theguardian.com/uk/2005/mar/16/scotland|accessdate=31 December 2015|agency=The Guardian|date=15 May 2005}}</ref> Summarizing expert opinion of the Islander, Alastair Dalton of ''The Scotsman'' said the aircraft "had a good safety record and had proved versatile in operating from the shortest and roughest Highland runways."<ref name=Scotsman01>{{cite news|last1=Dalton|first1=Alastair|title=Family's tribute to pilot and paramedic lost in crash|url=http://www.scotsman.com/news/family-s-tribute-to-pilot-and-paramedic-lost-in-crash-1-739757|accessdate=31 December 2015|work=The Scotsman|date=16 March 2005}}</ref>

===Flight numbers===
The flight numbers change daily and repeat with a weekly cycle. Loganair Flight 312 departs from [[Westray Airport]] to [[Papa Westray Airport]] on Monday morning, and Flight 317 returns to Westray that afternoon. On Tuesdays through Fridays, the flight numbers to Papa Westray are 323, 333, 343, and 353. The return flight numbers are 328, 338, 348, and 358. Flight 362 or 363 is the Saturday flight from Westray to Papa Westray, and on Sundays, Flight 378 is the return flight to Westray.<ref name=Loganair01 /><ref name=Loganair02 />

==See also==
{{Portal|Aviation}}

*[[Flight length]]
*[[Flight distance record]]

==References==
{{Reflist|30em}}

[[Category:Airline routes]]
[[Category:Aviation records]]
[[Category:Civil aviation]]
[[Category:Commercial flights]]
[[Category:Westray]]
[[Category:Transport in Orkney]]