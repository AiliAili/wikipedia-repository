{{Orphan|date=February 2017}}

{{Infobox journal
| title = Brain and Behavior
| cover = [[File:2015 Brain and Behavior cover.gif]]
| former_name =
| abbreviation = Brain Behav.
| discipline = [[Neurology]], [[neuroscience]], [[psychology]], [[psychiatry]]
| editor = Andrei V. Alexandrov, Maryann Martone
| publisher = [[Wiley-Blackwell]]
| country =
| history = 2011-present
| frequency = Monthly
| openaccess = Yes
| license = [[Creative Commons Attribution License]]
| impact = 2.243
| impact-year = 2014
| ISSN =
| eISSN = 2162-3279
| CODEN =
| JSTOR =
| LCCN =
| OCLC = 671244355
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2157-9032
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2157-9032/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2157-9032/issues
| link2-name = Online archive
}}
'''''Brain and Behavior''''' is a monthly [[peer-reviewed]] [[open access]] [[scientific journal]] covering [[neurology]], [[neuroscience]], [[psychology]], and [[psychiatry]]. It was established in 2011 and is published by [[Wiley-Blackwell]]. The [[editors-in-chief]] are Andrei V. Alexandrov ([[University of Tennessee Health Science Center]]) and Maryann Martone ([[University of California, San Diego]]).

The journal accepts direct submissions as well as submissions referred to them by other Wiley-Blackwell journals.<ref>{{cite web |title=Overview: Aims and Scope |url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2157-9032/homepage/ProductInformation.html |publisher=[[Wiley-Blackwell]] |accessdate=5 July 2015}}</ref> Some of those journals are owned by [[learned society|societies]] and their members are eligible for discounted publication charges.<ref>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2157-9032/homepage/Society.html |title=Society Information |work=Brain and Behavior |publisher=[[Wiley-Blackwell]]}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Embase]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101570837 |title=Brain and Behavior |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-07-05}}</ref>
* [[ProQuest|ProQuest databases]]
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-05}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-05}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.243.<ref name=WoS>{{cite book |year=2015 |chapter=Brain and Behavior |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2157-9032}}

[[Category:Neuroscience journals]]
[[Category:Psychiatry journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Monthly journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Publications established in 2011]]
[[Category:English-language journals]]