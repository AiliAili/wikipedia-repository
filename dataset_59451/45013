{{Use dmy dates|date=August 2011}}
{{Use Australian English|date=August 2011}}
{{History of Australia}}
[[File:"Aboriginal Family Travelling" by W.A. Cawthorne.jpg|thumb|250px|''Aboriginal Family Travelling'' by W.A. Cawthorne.]]
[[Image:Advertisement 1835.jpg|thumb|210px|1835 advertisement.]]
[[Image:Portrait of col william light.jpg|thumb|170px|Colonel [[William Light]].]]
This article details the '''History of Adelaide''' from the first human activity in the region to the 20th century. [[Adelaide]] is a [[New town|planned city]] founded in 1836 and the capital of [[South Australia]].

==Aboriginal settlement==

:''For early human settlement of '''Australia''' see [[Prehistory of Australia]]''
The Adelaide plains were inhabited by the [[Kaurna people|Kaurna]] tribe before European settlement, their territory extending from what is now [[Cape Jervis, South Australia|Cape Jervis]] to [[Port Broughton, South Australia|Port Broughton]]. The Kaurna lived in family groups called ''yerta'', a word which also referred to the area of land which supported the family group. Each yerta was the responsibility of Kaurna adults who inherited the land and had an intimate knowledge of its resources and features. The Kaurna led a nomadic existence within the Yerta confines in large family groups of around 30.{{Clarify|date=April 2010}} The area where the [[Adelaide city centre]] now stands was called "[[Tarndanya]]",<ref>[http://kaurnaplacenames.com/primary.php?id=4625 "Tarndanya"], KauranaPlaceNames.com. Retrieved 2009-09-09.</ref> which translates as "male red kangaroo rock", an area along the south bank of what is now called the [[River Torrens]]. Kaurna numbers were greatly reduced by at least two devastating [[epidemic]]s of [[smallpox]] which preceded European settlement, having been transported downstream along the [[Murray River]]. When European settlers arrived in 1836, estimates of the Kaurna population ranged from 300 to 1000 people.{{Citation needed|date=September 2009}}

==British preparation for establishing a colony==

British Captain [[Matthew Flinders]] and French Captain [[Nicolas Baudin]] independently charted the southern coast of the Australian continent. In 1802 Flinders named [[Mount Lofty]], but recorded little of the area which is now Adelaide. In 1830 [[Charles Sturt]] explored the [[Murray River]] and was impressed with what he briefly saw, later writing:
:"Hurried ....as my view of it was, my eye never fell on a country of more promising aspect, or more favourable position, than that which occupies the space between the lake ([[Lake Alexandrina (South Australia)|Lake Alexandrina]]) and the ranges of the [[Gulf St Vincent]], and, continuing northerly from [[Mount Barker, South Australia|Mount Barker]] stretches away, without any visible boundary".

Captain [[Collet Barker]], sent by [[New South Wales]] Governor [[Ralph Darling]], conducted a more thorough survey of the area in 1831, as recommended by Sturt. After swimming the [[Murray Mouth|mouth of the Murray River]], Barker was killed by natives who may have had contact with sealers and escaped convicts in the region. Despite this, his more detailed survey led Sturt to conclude in his 1833 report:
:"It would appear that a spot has at last been found upon the south coast of [[New Holland (Australia)|New Holland]] to which the colonists might venture with every prospect of success ... All who have ever landed upon the eastern shore of the St. Vincent's Gulf agree as to the richness of its soil and the abundance of its pastures."

=== Edward Wakefield ===
A group in Britain led by [[Edward Gibbon Wakefield]] were looking to start a colony based on free settlement rather than convict labour. After problems in other Australian colonies arising from existing settlement methods, the time was right to form a more methodical approach to establishing a colony. In 1829 an imprisoned Wakefield wrote a series of letters about systematic colonisation which were published in a daily newspaper. 

Wakefield suggested that instead of granting free land to settlers as had happened in other colonies, the land should be sold. The money from land purchases would be used solely to transport labourers to the colony free of charge, who were to be responsible and skilled workers rather than paupers and convicts. Land prices needed to be high enough so that workers who saved to buy land of their own remained in the [[workforce]] long enough to avoid a [[labour shortage]].

=== The South Australian Association ===
[[Robert Gouger]], Wakefield's secretary promoted Wakefield's theories and organised societies of people interested in the scheme. In 1834 the South Australian Association, with the aid of such figures as [[George Grote]], [[Sir William Molesworth, 8th Baronet|William Molesworth]] and the [[Arthur Wellesley, 1st Duke of Wellington|Duke of Wellington]] persuaded British Parliament to pass the [[South Australia Act 1834|South Australian Colonisation act]], succeeding where two previous organisations had failed. 

Wakefield wanted the colony's capital to be called [[Wellington]], but [[William IV of the United Kingdom|King William IV]] preferred it to be named after his [[Queen consort|wife]], [[Adelaide of Saxe-Meiningen]]. The British government appointed a Board of Commissioners from people nominated by the South Australian Association, with the task of organising the new colony and meeting the condition of selling at least £3,500 worth of land. The province and its capital were named, planned, advertised and largely sold before a single settler had set foot in their new home.

Free passage was given to suitable labourers, generally men and women and animals
under 30 years of age who were healthy and of good character. They were expected to 
carry out a promise of working for wages until they had saved enough to buy land of
their own and employ others, a process taking at least 3 or 4 years. Land sales were encouraged by granting one acre (4,000 m²) of town land in Adelaide for every 80 acres (32 ha) of rural land sold. The largest buyer of land was the [[South Australia Company]] headed by [[George Fife Angas]], which bought enough land for South Australia to proceed, and continued to influence the colony's future development.

With the British government's conditions met, King William IV signed the [[Letters Patent establishing the Province of South Australia|Letters Patent]] and the first settlers and officials set sail in early 1836.

==British settlement (1836)==
[[File:Charles Hill - The Proclamation of South Australia 1836 - Google Art Project.jpg|thumb|350px|''The Proclamation of South Australia 1836'', Charles Hill.]]
[[Image:Adelaide North Tce 1839.jpg|thumb|270px|Adelaide in 1839, looking south-east from [[North Terrace, Adelaide|North Terrace]].]]
In February 1836 the ''John Pirie'' and the ''Duke of York'' set sail for South Australia. They were followed in March by the ''Cygnet'' and ''Lady Mary Pelham'', in April by the ''Emma'', in May by the ''Rapid'' (carrying Colonel Light) and then by the ''Africaine'' (carrying Robert Gouger) and ''Tam o' Shanter''. Most took supplies and settlers to [[Kangaroo Island]] on the present day site of [[Kingscote, South Australia|Kingscote]], to await official decisions on the location and administration of the new colony. By the time the ''Duke of York'' had arrived at Kangaroo Island, the ''[[HMS Buffalo (1813)|HMS Buffalo]]'' (carrying Governor [[John Hindmarsh]]) was on its way.

[[Surveying|Surveyor]] Colonel [[William Light]], who had two months to complete his tasks, rejected locations for the new settlement such as Kangaroo Island, [[Port Lincoln]] and [[Encounter Bay]]. He was required to find a site with a [[harbour]], [[arable land]], [[fresh water]], ready internal and external [[communication]]s, [[building material]]s and [[drainage]]. Most of the settlers were moved from Kangaroo Island to [[Glenelg, South Australia|Holdfast Bay]] the site of present-day Glenelg, with Governor Hindmarsh arriving on 28 December 1836 to [[Proclamation|proclaim]] the province of South Australia. 

Light had to work quickly as the settlers were eager to take possession of the land they had purchased and grew impatient waiting. The salt water [[Port River]] was sighted and deemed to be a suitable harbour, however there was no fresh water available nearby. The [[River Torrens]] was discovered to the south of the Port River and northeast of Holdfast Bay, and Light and his team set about determining the city's precise location and layout.

Light favoured a location on rising ground along the Torrens valley between the coast and hills which  would be free of floodwaters. Governor Hindmarsh upon arrival initially approved of the location, but changed his mind thinking that the site should instead be two miles (3&nbsp;km) closer to the harbour (an area unsuitable due to flooding). Other colonists thought [[Port Lincoln]] or [[Encounter Bay]] would be better sites. After much [[mud slinging]], mainly directed towards Light, a public meeting of landholders was called on 10 February 1837, where a vote was held resulting in 218 to 127 in Light's favour, settling the issue for the meantime.

The survey was completed on 11 March 1837, which was a considerable achievement given the time taken to complete comparable surveys. The city plan carefully fitted the [[topography]] of the area: the Torrens Valley [[Adelaide Parklands|was kept as parklands]] and town acres were planned on higher land to the north and south. Adelaide was divided into two districts north and south of the river with [[North Adelaide]] composed of 342 acres (1.4&nbsp;km²) and Adelaide {{convert|700|acre|km2}}, surrounded by 2,300 acres (9&nbsp;km²) set aside as parklands for recreation and public functions. 

The [[Grid plan|grid pattern of Adelaide's streets]] features a central square ([[Victoria Square, Adelaide|Victoria Square]]) and four smaller squares ([[Hindmarsh Square, Adelaide|Hindmarsh]], [[Hurtle Square, Adelaide|Hurtle]], [[Light Square, Adelaide|Light]] and [[Whitmore Square, Adelaide|Whitmore]]). North Adelaide features [[Wellington Square, North Adelaide|Wellington Square]]. Space for public buildings such as [[Government House, Adelaide|Government House]], government stores, [[Adelaide Botanic Garden|botanical gardens]], [[Royal Adelaide Hospital|hospital]], [[West Terrace Cemetery|cemetery]] and an aboriginal reserve were included within the parklands.{{Citation needed|date=January 2014}}

==First years==
[[Image:Governor John Hindmarsh.jpg|thumb|170px|Governor [[John Hindmarsh]].]]
[[Image:George Gawler.jpg|thumb|170px|Governor [[George Gawler]].]]
[[Image:George Edward Grey.jpg|thumb|170px|Governor [[George Edward Grey|George Grey]].]]

===Hindmarsh===

Colonists who had already purchased land before departing were given first choice on 23 March 1837, and the remaining areas were auctioned for between 2 and 14 [[British coin Guinea|guineas]]. Within a few weeks many of the same areas were selling for between 80 and 100 [[Pound sterling|pounds]] which was seen as a healthy sign. With the town survey completed, Light's poorly paid and ill-equipped surveying team were expected to begin another massive task of surveying at least 405&nbsp;km² of rural land. 

Light's deputy, [[George Strickland Kingston|George Kingston]] was sent back to London in October 1837 to ask for more staff and equipment to speed up the process, and to have the troublesome Hindmarsh recalled. Light, who was slowly succumbing to [[tuberculosis]], managed to complete 243&nbsp;km² by December 1837, by which time the population had increased to around 2,500. When Kingston returned in June 1838, 605.7&nbsp;km² had been completed. 

Light's requests were denied; instead he could change from the [[Trigonometry|trigonometric]] surveys to a faster (but inferior) [[running survey]], or hand control over to Kingston and confine himself to coastal surveys. Light resigned in protest. Hindmarsh was to be replaced, and left Adelaide on board the ''Alligator'' on 14 July 1838, some three months before the next governor, George Gawler, arrived via Kingscote KI, on 12 October 1838, aboard the ''[[Pestonjee Bomanjee]]'' from London.

The first [[sheep]] and other [[livestock]] in South Australia were brought in from [[Tasmania]]. Sheep were overlanded from New South Wales from 1838, with the wool industry forming the basis of South Australia's economy for the first few years. Vast tracts of land were [[lease]]d by "Squatters" until required for agriculture. Once the land was surveyed it was put up for sale and the Squatters had to buy their runs or move on. Most bought their land when it came up for sale, disadvantaging farmers who had a hard time finding good and unoccupied land.

Farms took longer to establish than sheep runs and were expensive to set up. Despite this, by 1860 [[wheat]] farms ranged from [[Encounter Bay]] in the south to the [[Clare Valley]] in the north.

The city was intended to develop around the central [[Victoria Square, Adelaide|Victoria Square]], with the intersecting [[Grote Street, Adelaide|Grote]] and [[King William Street, Adelaide|King William]] Streets planned as extra wide to allow for future development. Instead, development concentrated around two of the narrowest streets on the city plan, [[Rundle Street, Adelaide|Rundle]] and [[Hindley Street, Adelaide|Hindley]] Streets, due to their proximity to the city's water supply and to [[Port Road, Adelaide|Port Road]], which led directly to the [[Port Adelaide|port]]. Many empty blocks remained until the late 19th century.

===Gawler===

Adelaide's second Governor was Colonel [[George Gawler]] who arrived in October 1838 to a situation of almost no public finances, underpaid officials and 4,000 immigrants still living in makeshift accommodation. He was allowed a maximum of £12,000 expenditure a year, with an additional £5,000 credit for emergencies, but was given the impression by the [[Colonial Office]] back in London that self-sufficiency of the colony was of minor importance and that government support should be relied upon.

Gawler's first goal was to address delays over rural settlement and agriculture. He persuaded Sturt in New South Wales to work for him as surveyor-general, overseeing the surveys himself in the meantime. He appointed more colonial officials with higher wages, set up a [[police force]] and took part in explorations of the surrounding terrain. A governor's house, jail, police barracks, hospital, and customs house and wharf at [[Port Adelaide, South Australia|Port Adelaide]] were built, as well as houses for public officials and missionaries, and outstations for police and surveyors.

The land [[Business cycle|boom]] eased after 1839; cash and credit were scarce, explorations indicated limited good land, and British [[Speculation|speculators]] became interested in [[New Zealand]]. In 1840 there were crop failures in the other Australian colonies, upon which Adelaide still relied for food, and the cost of living increased rapidly. Gawler increased public expenditure to prevent an economic collapse, which resulted in [[bankruptcy]] and later, changes to the way the colony was run (see [[South Australia Act, 1842]]). Over £200,000 in bills had been amassed and the land fund in London had been exhausted.

The British Parliament approved a £155,000 loan (later made a gift) to bail-out the colony. A head had to roll and Captain [[George Edward Grey|George Grey]] was sent to replace Gawler. Despite having been recalled, Governor Gawler had put Adelaide on a firm footing, making South Australia agriculturally self-sufficient, building [[infrastructure]] such as the [[Adelaide Gaol]], and restoring [[public opinion|public confidence]].

===Grey===

Grey, 29 at the time, issued the news of Gawler's recall himself, from the steps of [[Government House, Adelaide|government house]] on 15 May 1841. He slashed public expenditure, turning public opinion against him (which Grey ignored). [[Silver]] was discovered at [[Glen Osmond, South Australia|Glen Osmond]] the same year, which lifted spirits and spurred on discoveries of other finds in the [[Mount Lofty Ranges]]. 

[[Copper]] was discovered near [[Kapunda, South Australia|Kapunda]] in 1842. In 1845 even larger deposits of copper were discovered at [[Burra, South Australia|Burra]] which brought wealth to the Adelaide shopkeepers who invested in the mine. With a series of good [[harvest]]s and expanding agriculture, Adelaide exported meat, wool, wine, fruit and wheat. 

[[John Ridley (inventor)|John Ridley]] invented a [[reaping machine]] in 1843 which changed farming methods throughout South Australia and the nation at large. By 1843, 93&nbsp;km² of land was growing wheat (contrasted with 0.08&nbsp;km² in 1838). Toward the end of the century South Australia became known as the "granary of Australia". From a low point in 1842 when 642 out of 1,915 houses were abandoned and there was talk of abandoning the settlement, Adelaide was a bustling city when Grey left to govern New Zealand in 1845.

==1850–1900==
[[Image:King william street, Adelaide 1889.jpg|thumb|250px|Adelaide's [[King William Street, Adelaide|King William street]] in 1889, looking south towards the [[Adelaide Town Hall]] and [[Victoria Square, Adelaide|Victoria Square]].]]

[[Victorian gold rush|Gold discoveries]] in [[Victoria (Australia)|Victoria]] in 1851 brought a severe [[Labor shortage|labour shortage]] due to the exodus of workers leaving to seek their fortunes on the goldfields. However, this also led to a high demand for South Australian wheat. The situation improved when prospectors returned with their gold finds.

South Australians were keen to establish trade links with Victoria and [[New South Wales]], however overland transport was too slow. A £4,000 prize was offered in 1850 by the South Australian government for the first two people to navigate the River Murray in an iron [[steamboat]] as far as its junction with the [[Darling River]].  In 1853 William Randell of [[Mannum, South Australia|Mannum]] and Francis Cadell of Adelaide, unintentionally making the attempt at the same time, raced each other to [[Swan Hill, Victoria|Swan Hill]] with Cadell arriving first.

South Australia became  a [[Self-governing colony]] in 1856 with the [[ratification]] of a new [[constitution]] by the British parliament. [[Secret ballot]]s were introduced, and a [[Bicameralism|bicameral]] parliament was elected on 9 March 1857, by which time 109,917 people lived in the province.

[[Premiers of South Australia|Premier]] [[Robert Torrens]] devised a [[Torrens title|land title system]] in 1858 which adapted the principles of shipping registers, and was emulated in the other Australian colonies and overseas in places such as [[Singapore]]. Further copper discoveries were made in 1859 at [[Wallaroo, South Australia|Wallaroo]] and in 1861 at [[Moonta, South Australia|Moonta]]. In 1860 the Thorndon Park reservoir was opened, finally providing an alternative water source to the [[turbidity|turbid]] River Torrens.

During [[John McDouall Stuart]]'s 1862 expedition to the north coast of Australia, he discovered 200,000&nbsp;km² of grazing territory to the west of [[Lake Torrens]] and [[Lake Eyre]]. South Australia was made responsible for the administration of the [[Northern Territory]].
In 1867 gas [[street light]]ing was implemented, the [[University of Adelaide]] was founded in 1874, the [[South Australian Art Gallery]] opened in 1881 and the [[Happy Valley Reservoir]] opened in 1896.

In the 1890s Australia was affected by a severe [[depression (economics)|economic depression]], ending a hectic era of land booms and tumultuous expansion. Financial institutions in [[Melbourne]] and banks in [[Sydney]] closed. The national [[fertility rate]] fell and immigration was reduced to a trickle. The value of South Australia's exports nearly halved. [[Drought]] and poor harvests from 1884 compounded the problems with some families leaving for [[Western Australia]]. Adelaide was not as badly hit as the larger gold-rush cities of Sydney and Melbourne, and silver and [[lead]] discoveries at [[Broken Hill]] provided some relief.
Only one year of [[Government budget deficit|deficit]] was recorded, but the price paid was retrenchments and lean public spending. [[Wine]] and copper were the only industries not to suffer a downturn.

==Twentieth century==
[[File:Currie Street, Adelaide, 1925.jpg|thumb|left|Currie Street, 1925]]

Historian F.W. Crowley examined the reports of visitors in the early 20th century:
{{Quotation|Many visitors to Adelaide admired the [[Light's Vision|foresighted planning]] of its founders, but deplored its Puritan tone. Others thought it combined all the best and all the worst features of life in an Australian city – an extremely wealthy [[Upper class|upper crust living]] in splendour in ''elite'' suburbs alongside grinding poverty in industrial slums; a nominal proclamation of Christian virtues on Sundays, yet a ruthless devotion to money-making on weekdays.|Author=F.K. Crowley|Title=Modern Australia in Documents: 1901 – 1939}}

Electric street lighting was introduced in 1900 and [[Trams in Adelaide|Adelaide's electric tram service]] began transporting passengers in 1909.

28,000 South Australians volunteered to fight during [[Military history of Australia during World War I|Australia's involvement in the First World War]]. Adelaide enjoyed a post-war boom, but with the return of droughts, entered the [[Great Depression|depression]] of the 1930s, later returning to prosperity with strong government leadership.

[[Secondary sector of industry|Secondary industries]] helped reduce the state's dependence on [[primary sector of industry|primary industries]]. The 1933 census recorded the state population at 580,949, which was less of an increase than other states due to the state's economic limitations.

In 1935 [[Goldsbrough Mort & Co|Goldsbrough Mort and Company]] built their multi-storey premises on [[North Terrace, Adelaide|North Terrace]].<ref>{{Cite book|title = The City of Adelaide: a thematic history|last = |first = |publisher = McDougall & Vines|year = 2006|isbn = |location = http://www.adelaidecitycouncil.com/assets/city_of_adelaide_thematic_history.pdf|pages = 21}}</ref>

[[World War II]] brought industrial stimulus and diversification to Adelaide under the leadership of [[Thomas Playford IV|Thomas Playford]]. 70,000 men and women enlisted and shipbuilding was expanded at [[Whyalla, South Australia|Whyalla]]. Adelaide's transformation from an agricultural service centre to a 20th-century city was complete.
[[File:Adelaide in 1935.jpg|thumb|Adelaide in 1935]]

===Post world war II===
[[File:Victoria Square in Adelaide, South Australia.jpg|thumb|Public Transport Centenary, Victoria Square. Prototype "H1" tram 381 - Sunday 11 June 1978]]
After the war, an assisted migration scheme brought 215,000 emigrants of many European nationalities to South Australia between 1947 and 1973. Electrical goods were manufactured in former munitions factories and [[Holden]] cars were assembled from 1948. A pipeline from [[Mannum, South Australia|Mannum]] brought [[River Murray]] water to Adelaide in 1954 and [[Adelaide Airport]] opened at [[West Beach, South Australia|West Beach]] in 1955. Adelaide gained a second university in 1966 with the opening of [[Flinders University]]. 

In 1968, a blueprint for the building an integrated system of [[freeways]] across Adelaide was released in the form of the [[Metropolitan Adelaide Transport Study]] (MATS). Bowing to opposition from the public, who feared freeways would create urban problems such as [[gridlock]]ed traffic and [[ghettos]], the [[Australian Labor Party|Labor]] government under [[Don Dunstan]] shelved MATS but retained the land in case [[public opinion]] changed in the future.<ref>[http://www.samemory.sa.gov.au/site/page.cfm?c=2023&mode=singleImage Eric Franklin, The Advertiser: Labour Will Revise MATS Plan], 6 May 1970</ref> In 1980, the [[Liberal party of Australia|Liberal party]] won government on a platform of [[fiscal conservatism]] and the premier [[David Tonkin]], deeming Adelaide road capacities sufficient for future needs, committed his government to selling off the land acquired for the MATS plan ensuring that even when needs or public opinion changed, the construction of most MATS proposed freeways would be impossible. <ref name="History">{{cite web | last =  | first =  | date =  | title = Adelaide’s Freeways: A History from MATS to the Port River Expressway | work = OZROADS | url =  http://www.ozroads.com.au/SA/freeways.htm | accessdate = 8 August 2011}}</ref> 

The [[Don Dunstan|Dunstan]] Government of the 1970s saw something of an Adelaide 'cultural revival' - establishing a wide array of social reforms and overseeing the city becoming a centre of the arts. Adelaide hosted the [[Australian Grand Prix]] between 1985 and 1996 on a street circuit in the city's east parklands, before losing it in a controversial move to [[Melbourne]]. The [[Adelaide Gaol|(Old) Adelaide Gaol]] was closed in 1988.

In 1989, the [[Australian Submarine Corporation]] naval [[shipyards]] were opened.<ref name=YW127>Yule & Woolner, ''The Collins Class Submarine Story'', p. 127</ref>

In 1991, the [[University of South Australia]] was formed from a merger of several state government education institutions. The 1992 [[State Bank of South Australia|State Bank]] collapse plunged both Adelaide and South Australia into economic recession, and its effects can still be felt today.

==Twenty-first century==
Recent years have seen the [[Clipsal 500]] [[V8 Supercar]] race use part of the former Formula One circuit and renewed economic confidence under the [[Mike Rann|Rann]] Government. The late first decade of the 21st century have seen extensions of the [[Glenelg Tram|remaining tram network]], the first growth after the decline of the system during the 1950s.

==See also==
* [[Timeline of Adelaide history]]
* [[Light's Vision]]
* [[Proclamation Day]]
* [[Australian Overland Telegraph Line]]
* [[Street Naming Committee (Adelaide)]]
* [[The Old Gum Tree]]
* [[Old Government House, South Australia|Old Government House]], [[Belair National Park|Belair]]

===People===
* [[Edward Charles Frome]]

==References==
{{Reflist}}
==Further reading==
*Elizabeth Kwan ''Living in South Australia: A Social History Volume 1: From Before 1836 to 1914'' (1987)
*Kathryn Gergett and Susan Marsden ''Adelaide: A brief History'' (1996)
*Derek Whitelock ''Adelaide: From Colony to Jubilee'' (1985)

==External links==
*[http://www.adelaidereview.com.au/archives/2004_07/issuesandopinion_story2.shtml  A History of the Kingston plan of Adelaide]

{{DEFAULTSORT:History Of Adelaide}}
[[Category:History of Adelaide| ]]
[[Category:History of South Australia|Adelaide, History of]]