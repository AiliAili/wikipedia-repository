<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 | name=S.1 Cockle
 | image=
 | caption=Cockle on the [[River Medway]], [[Rochester, Kent|Rochester]] in 1924 - original fin.
}}{{Infobox Aircraft Type
 | type=Sport flying boat
 | national origin=[[United Kingdom]]
 | manufacturer=[[Short Brothers]]
 | designer=
 | first flight=7 November 1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Short S.1 Cockle''' was a single-seat sport [[monoplane]] [[flying boat]], with a novel [[monocoque]] [[duralumin]] hull.  It was underpowered and so did not leave the water easily, but it proved that watertight and [[corrosion]]-resistant hulls could be built from metal.

==Development==
From about 1921, [[Oswald Short]] had been thinking about the construction of seaplane floats and flying boat hulls made from metal, specifically [[duralumin]], rather than the traditional wood.  The latter always suffered from water retention and did not last well in the tropics.  He assembled a team, including C. P. T. Liscomb who had extensive experience with that alloy to look into the hydrodynamics and corrosion characteristics of such hulls, and by 1924 was looking out for an opportunity to apply their results.  It came with an Australian order for an aircraft suitable for fishing trips around Botany Bay, which Short proposed should be a small flying boat.<ref name="Barnes"/>  It was named the '''Stellite''' and was the first aircraft to have a Short's  design index number, '''S.1'''.  When it was built and registered as ''G-EBKA'' the Air Ministry  objected to the name on the reasonable grounds that the Short Stellite might well be confused with the Short Satellite, built at much the same time; it was therefore renamed the Short S.1 Cockle.  It was the smallest flying boat ever built at that time.<ref name="Barnes">{{Harvnb|Barnes|1989|pages=180–3, 186}}</ref> A contemporary source<ref name=Flight24-220/> claimed it to be "the first light seaplane to be built <nowiki>[in the United Kingdom]</nowiki> and possibly in the world" and the first British all-metal flying boat.

The Cockle <ref name="Barnes"/><ref name=Flight24-220>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200220.html  ''Flight'' 17 April 1924 pp. 220-3]</ref> was an all-metal aircraft apart from the fabric-covered flying surfaces.  The hull was a duralumin monocoque structure with a concave V-shaped planing bottom incorporating two steps, the main one near the centre of gravity.  These steps were external to the monocoque to prevent step damage leading to water leakage into the hull.  The top of the hull was rounded, with a single-seat open cockpit near the nose.  The wings had steel spars and were mounted on the top of the fuselage, with pairs of bracing struts to the chines.  The wings carried full-span ailerons which could be drooped together, flap-like, for landing. There were stabilising floats near the wingtips in trouser-like fairings.  The two engines were mounted on top of the wing at about mid-chord, the twin-bladed propellers being driven via long extension shafts to the leading edge.  Originally the Cockle had a shallow triangular fin and rudder,<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200221.html  ''Flight'' 17 April 1924 p. 221]</ref><ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200778.html  ''Flight'' 11 December 1924 p. 778]</ref> but this was later extended upwards to a curved and slightly pointed profile which more than doubled the area, to cope better with single-engine flying.<ref name="Barnes"/> A tube ran transversely across the hull just above the main step, into which the axle of a pair of ground-handling wheels could be inserted.<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200223.html  ''Flight'' 17 April 1924 p. 223]</ref>

Deterred from using 32&nbsp;hp (24&nbsp;kW) [[Bristol Cherub]] flat-twin engines owing to vibration problems, the Cockle began with a pair of V-twin [[Blackburne Tomtit]]s.  Ungeared and so limited to the maximum 2,400 rpm of the propellers, the Tomtits could produce only 16&nbsp;hp (12&nbsp;kW).<ref name="Barnes"/>  It is not surprising that, when it came to the first flight, the Cockle was underpowered.  Before the aircraft was complete the bare hull (always Short's main concern) was floated for a day in April 1924 and found to be satisfactorily watertight.<ref>[http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200222.html  ''Flight'' 17 April 1924 p. 222]</ref>  Attempts to get it off the water began in September, but did not succeed until 7 November 1924, with its wing at a higher angle of incidence and its pilot lightly dressed.  It has been suggested that it flew only because the atmospheric pressure was exceptionally high that day.<ref name="Barnes"/>

Given the poor performance, it is not surprising that the Australian customer declined delivery.  In January 1925 the fin and rudder modifications were made and in March there were unsuccessful attempts to get certification.  In July it went on loan to the [[Air Ministry]] at [[RAF Felixstowe|Felixstowe]], with the serial ''N193''.  It was not easy to get into the air, but [[John Lankester Parker|John Parker]], Short's test pilot gave a demonstration in September.  Despite the performance limitations, the aircraft impressed because of its corrosion resistance.  In August 1926 the Cockle was returned to Short Brothers and re-engined with a pair of geared-down Cherubs.  It flew several times in June and July before being purchased by the Air Ministry and returned to Felixstowe.  It flew at least one more time, again with Parker as pilot, thereafter being used for corrosion testing.<ref name="Barnes"/>

Though not a successful flyer, the Cockle gave Short Brothers valuable experience in building metal hulls for flying boats.  Their first large hull, the [[Short S.2]] metal replacement for the wooden hull of a [[Felixstowe F5]] was started at the same time as that of the Cockle, but the smaller hull progressed faster and the solution to problems encountered  with it transferred to the S.2.  The S.2 experience led on to the successful [[Short Singapore|Singapore]] and [[Short Calcutta]] of 1926 and 1928.<ref name="Barnes"/>
<!--==Operational history==-->
<!--==Variants==-->
<!--==Units using this aircraft/Operators (choose)==-->

==Specifications (Cherub)==

{{aerospecs
|ref={{Harvnb|Barnes|1989|pages=186}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=one
|capacity=
|length m=7.52
|length ft=24
|length in=8
|span m=10.97
|span ft=36
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=19.50
|wing area sqft=210
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=400
|empty weight lb=880
|gross weight kg=57
|gross weight lb=1,205
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=2
|eng1 type=[[Bristol Cherub]] flat twin
|eng1 kw=24<!-- prop engines -->
|eng1 hp=32<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=117
|max speed mph=73
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[Short Mussel]]
*[[List of seaplanes and amphibious aircraft]]
}}

==References==
{{commons category|Short Cockle}}

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book |title= Shorts Aircraft since 1900|last= Barnes|first=C.H.|year=1989 |publisher=Putnam Publishing  |location=London |isbn= 0-87021-662-7|ref=harv}}
{{refend}}

{{Short Brothers aircraft}}

[[Category:Short Brothers aircraft|Cockle]]
[[Category:British sport aircraft 1920–1929]]
[[Category:Flying boats]]
[[Category:Twin-engined tractor aircraft]]
[[Category:High-wing aircraft]]