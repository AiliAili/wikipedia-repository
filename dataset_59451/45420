[[File:FEMA - 25592 - Photograph by Bill Koplitz taken on 08-10-2006 in District of Columbia.jpg|thumb|right|300px|Harvey E. Johnson Jr.]]
'''Harvey E. Johnson Jr.''', retired [[Vice Admiral]], [[United States Coast Guard]], is the Executive Director, Recovery Operations for the [[American Red Cross]]. Previously, Johnson was the former Vice President for National Preparedness and Response Solutions at BAE Systems and Deputy Administrator and Chief Operating Officer of the [[Federal Emergency Management Agency]] (FEMA).
<ref>{{cite web
  |url = http://www.redcross.org/about-us/media/subject-matter-experts/disaster-services
  |title = Subject Matter Experts - Disaster Services
  |work = 
  |publisher = American Red Cross
  |accessdate = 2015-08-15
}}</ref>

==Early life==
Johnson received a Bachelor of Science degree at the [[United States Coast Guard Academy|U.S. Coast Guard Academy]] in 1975, and an [[Master of Science|M.S.]] at the [[Naval Postgraduate School]] in 1983, and a Master of Science in Management  from the [[MIT Sloan School of Management]] as a [[Sloan Fellows|Sloan Fellow]] in 1993.<ref name="femabio" />

==Career==

===Coast Guard career===

Johnson began his career as a deck watch officer aboard the USCGC ''Steadfast'' (WMEC-623).  He then earned his [[United States Naval Aviator|Naval Aviator]] wings in 1977.  He flew the HH-52A helicopter at Coast Guard Air Station Houston, the HH-3F at Coast Guard Air Station Kodiak, the HH-65A in Brooklyn and Corpus Christi, and the HH-60J in San Diego. Staff assignments included: Aviation Assignment Officer in the Office of Personnel and Training; Program Reviewer and Analyst within the Office of the Chief of Staff; Deputy Chief, Programs Division within the Office of the Chief of Staff; and, member of the Streamlining Team.<ref name="femabio">
{{cite web
  |url = http://www.fema.gov/about/bios/hjohnson.shtm
  |title = Harvey E. Johnson, Jr.
  |work = Bios
  |publisher = Federal Emergency Management Agency
  |accessdate = 2007-10-28
|archiveurl = http://web.archive.org/web/20071016115918/http://www.fema.gov/about/bios/hjohnson.shtm <!-- Bot retrieved archive --> |archivedate = 2007-10-16}}</ref>

Johnson assumed the duties of Commander, Pacific Area in June 2004.  The Area of Operations for the Pacific Area encompasses over 73 million square miles west of the Rocky Mountains and throughout the Pacific Basin to the Far East.  Prior to this assignment, he was the Commander, Seventh Coast Guard District and served as the Director, [[Homeland Security]] Task Force-Southeast, where he directed Operation Able Sentry, the DHS response to the crisis in Haiti.  In addition to these duties, Johnson served as the Executive Director of the Coast Guard's transition into the Department of Homeland Security, Director of Operations Capability, and Director of Operations Policy.<ref name="femabio" />

Prior to promotion to flag rank in 2001, Johnson served as the Executive Assistant to the Commandant of the Coast Guard. Other assignments included: Commanding Officer of Air Station Brooklyn, and concurrently as Commanding Officer of Air Station San Diego and Commander, Activities San Diego. Johnson also served as a fellow at the [[Chief of Naval Operations Strategic Studies Group]] in Newport, Rhode Island.<ref name="femabio" />

===FEMA career===

On April 6, 2006 Homeland Security Secretary [[Michael Chertoff]] announced the appointment of Vice Admiral Johnson as FEMA deputy director.<ref name="Lipton">
{{cite news
  |first = Eric
  |last = Lipton
  |url = https://www.nytimes.com/2006/04/07/washington/07fema.html?n=Top/Reference/Times%20Topics/Organizations/H/Homeland%20Security%20Department
  |title = Nominations Made for Top Post at FEMA and Three Other Slots
  |work = [[New York Times]]
  |date = April 7, 2006
  |accessdate = 2007-10-28
}}</ref>

On September 7, 2007 President [[George W. Bush]] announced his intention to nominate Johnson to serve in his administration as Deputy Administrator and Chief Operating Officer of the Federal Emergency Management Agency at the Department of Homeland Security.<ref name="wh-2007-09-07">
{{cite web
  |date = September 7, 2007
  |url = http://georgewbush-whitehouse.archives.gov/news/releases/2007/09/20070907-10.html
  |title = Personnel Announcement
  |work = News & Policies: Nominations by Name
  |publisher = The [[White House]]
  |accessdate = 2007-10-28
}}</ref>

====Achievements====
After joining FEMA in the Spring of 2006, VADM Johnson spearheaded then-Administrator R. David Paulison's initiative to create a clear mission statement and execute his (Paulison's) vision for a "New FEMA."  With the full support of the White House and Department of Homeland Security, Johnson executed a series of assessments of the current status of FEMA from top to bottom following the much publicized failures after Hurricane Katrina.  Requiring a "business approach to FEMA," Johnson helped turn the Agency around, and greatly improved both its internal and external operations.  Just three years after Katrina, FEMA has received high marks from detractors such as Rep. Bennie Thompson (D-MS) and Sen. Joe Lieberman (I-CT), Chairmen of FEMA's oversight committees in Congress for performance in recent hurricanes such as Gustav and Ike, flooding in the midwest and severe winter storms in the northeast.

In January, 2009, Johnson's public service career ended with his retirement from government service.

==== Criticism ====

{{Wikinews|FEMA employees pose as fake reporters during press conference}}
On October 24, 2007, FEMA announced a news conference about the recent [[w:California wildfires of October 2007|California wildfires]]. But with no reporters on hand and an agency video camera providing a feed, carried live by some television networks, FEMA press employees posed as reporters and asked the questions for Deputy Administrator Johnson. Two days later, Johnson admitted that the press conference had been staged, saying, "We can and must do better, and apologize for this error in judgment."<ref name="Mikkelsen">
{{cite news
| first = Randall
| last = Mikkelsen
| url = http://www.reuters.com/article/latestCrisis/idUSN26366100
| title = US agency apologizes for news conference on fires
| publisher = Reuters
| date = October 26, 2007
| accessdate = 2007-10-28
}}</ref><ref name="Kamen">
{{cite news
| first = Al
| last = Kamen
| url = http://www.washingtonpost.com/wp-dyn/content/article/2007/10/25/AR2007102502488.html
| title = FEMA Meets the Press, Which Happens to Be... FEMA
| work = [[Washington Post]]
| page = A19
| date = October 26, 2007
| accessdate = 2007-10-28
}}</ref>

* A spokeswoman for Homeland Security Secretary [[Michael Chertoff]], who has authority over FEMA, called the incident "inexcusable and offensive to the secretary".<ref name="Mikkelsen" />
* The White House was not happy with FEMA's response. "It is not a practice that we would employ here at the White House", said Press Secretary [[Dana Perino]], mentioning three times that it was an "error in judgment". "It's not something I would have condoned, and they, I'm sure, will not do it again."<ref name="Yager">
{{cite news
| first = Jordy
| last = Yager
| url = http://www.latimes.com/news/nationworld/nation/la-na-fema27oct27,0,913215.story?coll=la-home-center
| title = FEMA blasted for 'news' conference
| work = [[Los Angeles Times]]
| date = October 27, 2007
| accessdate = 2007-10-28
}}</ref><ref name="Perino">
{{cite web
| date = October 26, 2007
| url = http://www.whitehouse.gov/news/releases/2007/10/20071026-6.html
| title = Press Briefing by Dana Perino
| work =
| publisher =
| accessdate = 2007-10-28
}}</ref>

* On Monday October 29, 2007 [[John "Pat" Philbin]], the former director of external affairs for FEMA, took responsibility for the [[w:California wildfires of October 2007|California wildfires]] fake press conference. “I should have cancelled it quickly. I did not have good situational awareness of what was happening,” he told CBS News. Philbin had previously planned to retire on Friday (October 26, 2007) before this situation came about.<ref name=" MichaelRey ">
{{cite news
| first = Michael
| last= Rey
| url = http://www.cbsnews.com/blogs/2007/10/29/primarysource/entry3428071.shtml
| title = I Should Have Cancelled" Fake FEMA Press Briefing
| publisher = CBS
| date = October 29, 2007
| accessdate = 2007-10-29
}}</ref>

* Johnson received criticism for choosing to live in a 6200-square-foot, four-bedroom, four-bath home located in the prestigious upscale beachfront gated community of Cocoplum in Coral Gables at a cost to the taxpayer of $116,000 per year in lease payments rather than taking up residence in the traditional home of the commander of District Seven, "flag quarters", located within the Coast Guard's Richmond Heights housing complex near Metrozoo in South Miami-Dade, at a cost of only $24,000 per year.<ref>{{cite news|last=Villano|first=David|title=The High Cost of Homeland Security|url=http://www.miaminewtimes.com/content/printVersion/246936/|accessdate=September 18, 2011|newspaper=Miami New Times|date=October 9, 2003}}</ref>

==Awards==
His major decorations include the [[Coast Guard Distinguished Service Medal]], [[Legion of Merit]] (3), the [[Meritorious Service Medal (United States)|Meritorious Service Medal]] (3), the Coast Guard Commendation Medal (2), and the [[Coast Guard Achievement Medal]].<ref name="femabio" />

*[[File:Coast Guard Distinguished Service ribbon.svg|60px]]&nbsp;&nbsp;[[Coast Guard Distinguished Service Medal]]
*[[File:Legion of Merit ribbon.svg|60px]]&nbsp;&nbsp;[[Legion of Merit]] with two award stars
*[[File:Meritorious Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Meritorious Service Medal (United States)|Meritorious Service Medal]] with two award stars
*[[File:Coast Guard Commendation ribbon.svg|60px]]&nbsp;&nbsp;[[Commendation Medal|Coast Guard Commendation Medal]] with award star
*[[File:Coast Guard Achievement ribbon.svg|60px]]&nbsp;&nbsp;[[Coast Guard Achievement Medal]]

==Personal==

Johnson is a native of [[Tampa, Florida]]. He is married to the former Janet L. Cronin of Boston, Massachusetts, and they have two children, Jennifer and Scott.<ref name="femabio" /> He also has one granddaughter, Kaitlin.

{{commons category}}

==References==
{{Portal|United States Coast Guard}}
{{Reflist}}

{{DEFAULTSORT:Johnson, Harvey Jr.}}
[[Category:American Red Cross personnel]]
[[Category:United States Coast Guard admirals]]
[[Category:Recipients of the Legion of Merit]]
[[Category:United States Coast Guard Academy alumni]]
[[Category:MIT Sloan School of Management alumni]]
[[Category:United States Naval Aviators]]
[[Category:Naval Postgraduate School alumni]]
[[Category:United States Coast Guard Aviation]]
[[Category:Living people]]
[[Category:Recipients of the Coast Guard Distinguished Service Medal]]
[[Category:American chief operating officers]]