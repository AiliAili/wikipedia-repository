{{multiple issues|
{{one source|date=July 2014}}
{{primary sources|date=July 2014}}
{{third-party|date=July 2014}}
}}
{{Infobox Journal
| title        = Philosophers' Imprint
| cover        = 
| editor       = [[J. David Velleman]], [[Stephen Darwall]]
| discipline   = [[Philosophy]]
| language     = [[English language|English]]
| abbreviation = the Imprint
| publisher    = University of Michigan Digital Library
| country      = U.S.
| frequency    = Irregular
| history      = 2001–present
| openaccess   = Yes
| impact       = 
| impact-year  = 
| website      = http://www.philosophersimprint.org/
| link1        = 
| link1-name   = 
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 45826937
| LCCN         = 2001-212257
| CODEN        = 
| ISSN         = 1533-628X
| eISSN        = 
}}
'''''Philosophers' Imprint''''' is a refereed [[philosophy]] journal, edited by [[Stephen Darwall]] and [[J. David Velleman]].  The journal is advised by an international board of editors and published on the Internet by the [[University of Michigan]] Digital Library. Unlike many other philosophy journals, the ''Imprint'' offers access to its published articles for free to anyone on the World Wide Web—no subscription or registration whatsoever is required. While articles are not published during regular intervals, readers can be notified of new publications by mailing list.

==The mission==
The idea behind ''Philosophers' Imprint'' was inspired by the [[Open Access movement]]. The goal is to start the foundation for a "future in which academic libraries no longer spend millions of dollars purchasing, binding, housing, and repairing printed journals, because they have assumed the role of publishers, cooperatively disseminating the results of academic research for free, via the Internet."<ref>[http://www.philosophersimprint.org/about.html Philosophers' Imprint Web Site]</ref>

==Notable articles==
The following is a partial (in both senses) list of some of the most notable articles in the ''Imprint'' (in date order):{{according to whom|date=July 2014}}
* "The Question of Realism" (2001) - [[Kit Fine]]
* "Normativity, Commitment, and Instrumental Reason" (2001) - [[R. Jay Wallace]]
* "Do Demonstratives Have Senses?" (2002) - Richard G. Heck
* "Thoroughly Modern McTaggart" (2002) - [[John Earman]]
* "The Role of Perception in Demonstrative Reference" (2002) - Susanna Siegel
* "Getting Told and Being Believed" (2005) - Richard Moran

==References==
{{Reflist}}

==External links==
* [http://www.philosophersimprint.org/ Philosophers' Imprint Web Site]

[[Category:Philosophy journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2001]]
[[Category:Contemporary philosophical literature]]
[[Category:Academic journals published by university libraries]]


{{philosophy-journal-stub}}