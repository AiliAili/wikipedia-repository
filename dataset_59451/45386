{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{more footnotes|date=December 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
|name= Lanoe George Hawker
|image=Lanoe Hawker.jpg
|caption=Lanoe Hawker
|birth_date={{birth date|df=yes|1890|12|30}}
|death_date={{death date and age|df=yes|1916|11|23|1890|12|30}}
|birth_place= [[Longparish]], Hampshire, England, United Kingdom
|death_place= [[Bapaume]], France
|nickname=
|allegiance= {{UK}}
|serviceyears=
|rank=Major
|unit=[[Royal Engineers]]<br>[[Royal Flying Corps]]
|commands=[[No. 24 Squadron RFC]]
|battles=[[First World War]]
*[[Western Front (World War I)|Western Front]]
|awards=[[Victoria Cross]]<br/>[[Distinguished Service Order]]
|relations=
|laterwork=
}}
'''Lanoe George Hawker''' [[Victoria Cross|VC]], [[Distinguished Service Order|DSO]] (30 December 1890&nbsp;– 23 November 1916) was a British [[First World War]] [[flying ace]]. Having [[List of World War I aces credited with 7 victories|seven credited victories]], he was the third pilot to receive the [[Victoria Cross]], the highest decoration for gallantry awarded to British and [[Commonwealth of Nations|Commonwealth]] servicemen. He was killed in a dogfight with the famous German flying ace [[Manfred von Richthofen]] ("The Red Baron"), who described him as "the British Boelcke".<ref name="Burrows p103">Burrows 1970, p. 103.</ref>

== Early life ==
Son of a distinguished military family, Hawker was born on 30 December 1890 at [[Longparish]], Hampshire, England. Lanoe was sent to [[Stubbington House School]] and at the age of 11 to the [[Britannia Royal Naval College|Royal Navy College in Dartmouth]], but although highly intelligent and an enthusiastic sportsman, his grades were disappointing.{{citation needed|date=September 2012}} As a naval career became more unlikely, he entered The [[Royal Military Academy, Woolwich|Royal Military Academy]] in Woolwich before joining the [[Royal Engineers]], as an officer cadet. A clever inventor, Hawker developed a keen interest in all mechanical and engineering developments. During the summer of 1910 he saw a film featuring the [[Wright Flyer]] and after attending an aircraft flying display at Bournemouth, he quickly found an interest in aviation, learning to fly at his own expense at [[Hendon Aerodrome|Hendon]]. On 4 March 1913, Hawker was awarded [[List of pilots awarded an Aviator's Certificate by the Royal Aero Club in 1913|Aviator's Certificate]] No. 435 by the [[Royal Aero Club]].

Promoted to 1st Lieutenant in October 1913 he was posted to [[Cork Harbour]] with the 33rd Fortress Company.<ref name="bowyer" /> His request for attachment to the [[Royal Flying Corps]] was granted and he reported to the [[Central Flying School]] at Upavon on 1 August 1914.<ref name="bowyer" />

== With the RFC ==
[[File:RAFBE2.jpg|left|thumb|150px|A BE2c]]
Hawker was posted to France in October 1914, as a [[Captain (land)|captain]] with [[No. 6 Squadron RFC|No. 6 Squadron]], [[Royal Flying Corps]], flying [[Henri Farman]]s. The squadron converted to the [[Royal Aircraft Factory B.E.2|B.E. 2c]] and he undertook numerous reconnaissance missions into 1915, being wounded once by ground fire. On 22 April he was awarded the [[Distinguished Service Order]] for attacking a German [[zeppelin]] shed at [[Gontrode]] by dropping [[hand grenade]]s at low level (below 200&nbsp;ft) from his B.E.2c. He used a tethered German balloon to help shield him from enemy ground fire as he made successive attacks. During the [[Second Battle of Ypres]], Hawker was wounded in the foot by ground fire. For the remainder of the battle he had to be carried to and from his aircraft, but refused to be grounded until the fight was over.{{citation needed|date=December 2012}}

Returning to 6 Squadron after hospitalisation, the squadron now received several [[Scout (aircraft)|single seat scouts]], and some early [[Royal Aircraft Factory F.E.2|F.E.2]] 'pushers'. One aircraft received was a [[Bristol Scout]] C, with RFC s/n 1609 that Hawker, with assistance from Air Mechanic [[Ernest Elton]] (who later became an Ace Pilot himself), equipped with their design of [[Lewis gun]] mount, enabling the machine gun to fire forward obliquely at an acute horizontal angle to the axis of flight, missing the propeller arc.<ref>Guttman 2009, p. 22.</ref>

While with No 6 squadron in 1915, Captain Hawker was a comrade of Captain [[Louis Strange]] [[Distinguished Service Order|DSO]] [[Order of the British Empire|OBE]] [[Military Cross|MC]] [[Distinguished Flying Cross (United Kingdom)|DFC*]]. The Squadron became pioneers of many aspects in military aviation at the time, driven largely by the imagination of Strange and the engineering talents of Hawker. Their talents led to various mountings for Lewis machine guns, one of which won Hawker the Victoria Cross, and one that nearly cost Strange his life.<ref>http://www.pbs.org/wgbh/nova/military/strange-captain.html</ref>

Hawker's innovative ideas at this time greatly benefited the still fledgling RFC. He helped to invent the Prideaux disintegrating link machine-gun belt feed, and initiated the practice of putting fabric protective coverings on the tips of wooden propellers, the use of fur-lined thigh boots, and devising a primitive 'rocking fuselage' for target practice on the ground. In 1916 he also developed (with W.L. French) the increased capacity 97-round 'double drum' for the Lewis machine gun. It was issued for trials in July and after modifications was issued generally to the RFC and RNAS.<ref>{{cite web|url=http://www.quarry.nildram.co.uk/Woodman.htm |title=Early A I Rcraft Armament |publisher=Quarry.nildram.co.uk |date= |accessdate=1 December 2012}}</ref>

== Victoria Cross ==
[[File:Lanoe Hawker's No 1611 Bristol Scout C.jpg|thumb|left|The [[Bristol Scout]] C, [[Royal Flying Corps|RFC]] serial no. 1611, flown by Hawker on 25 July 1915 in his Victoria Cross-earning engagement.]]

Following an initial air victory in June, on 25 July 1915 when on patrol over [[Passendale|Passchendaele]], Captain Hawker attacked three German aircraft in succession, flying a different [[Bristol Scout]] C, serial No. 1611, after his earlier No. 1609 had been written off, transplanting the custom Lewis gun mount onto No. 1611. The first aerial victory for Hawker that day occurred after he had emptied a complete drum of bullets from his aircraft's single [[Lewis machine gun]] into it, went spinning down. The second was driven to the ground damaged, and the third&nbsp;– an [[Albatros C.I]] of [[Feldflieger Abteilung|FFA]] 3<ref>[http://1914-1918.invisionzone.com/forums/index.php?showtopic=53657 German plane shot down 25 July 1915 – Great War Forum<!-- bot-generated title -->] at 1914–1918.invisionzone.com</ref>&nbsp;– which he attacked at a height of about 10,000 feet, burst into flames and crashed. (Pilot Oberleutnant Uebelacker and observer Hauptmann [[Hans Roser|Roser]] were both killed.) For this feat he was awarded the [[Victoria Cross]].<ref>Guttman 2009, pp. 22 – 23.</ref><ref>{{LondonGazette|issue=29273|supp=|startpage=8395|date=24 August 1915 |accessdate=15 May 2015}}</ref>

This particular [[sortie]] was just one of the many which Captain Hawker undertook during almost a year of constant operational flying and fighting. He claimed at least 3 more victories in August 1915, either in the Scout or flying an F.E.2.{{citation needed|date=December 2012}}

Hawker was posted back to England in late 1915, with some 7 victory claims (inc.1 captured, 3 destroyed, 1 'out of control' and 1 'forced to land') making him the first British flying ace, and a figure of considerable fame within the ranks of the RFC.

It has since been argued that shooting down three aircraft in one mission was a feat repeated several times by later pilots, and whether Hawker deserved his Victoria Cross has been questioned. However, in the context of the air war of mid-1915 it was unusual to shoot down even one aircraft, and the VC was awarded on the basis that all the enemy planes were armed with machine guns. More significantly, by the early summer of 1915, the German [[Feldflieger Abteilung]] two-seater observation units of the future [[Luftstreitkräfte]], had by this time, received examples of the [[Fokker Eindecker]] monoplane, with one Eindecker going to each unit, with a fixed, forward-firing machine gun fitted with a "[[synchronization gear]]" that prevented the bullets from striking the propeller. The first claim using this arrangement, though unconfirmed by the German Army, was by ''Leutnant'' [[Kurt Wintgens]] on 1 July 1915, some {{convert|225|mi|km}} over [[Lunéville]] distant from where Hawker had his three-victory success nearly a month later. Therefore, the German pilots like Wintgens and ''Leutnant'' [[Otto Parschau]], another pioneering Eindecker pilot, could employ the simple combat tactic of aiming the whole aircraft, and presenting a small target to the enemy while approaching from any angle, preferably from a blind spot where the enemy observer could not return fire.

Hawker flew before Britain had any workable synchroniser gear, so his Bristol Scout had its machine gun mounted on the left side of the cockpit, firing forwards and sideways at a 45&nbsp;degree angle to avoid the propeller. The only direction from which he could attack an enemy was from its right rear quarter&nbsp;– precisely in a direction from which it was easy for the observer to fire at him. Thus, in each of the three attacks, Hawker was directly exposed to the fire of an enemy machine gun.

== First Fighter Squadron ==
[[File:Airco D.H.2 ExCC.jpg|right|thumb|200px|Airco DH2]]
Promoted to major early in 1916. Hawker was placed in command of the RFC's first (single seater) fighter squadron, [[No. 24 Squadron RFC|Number 24]] based at [[Hounslow Heath Aerodrome]] and flying the [[Airco DH.2]] pusher. After two fatalities in recent flying accidents, the new fighter, which featured a forward-mounted Lewis machine gun, soon earned a reputation for spinning; its rear mounted rotary engine and sensitive controls made it very responsive. Hawker countered this worry by taking a DH.2 up over the squadron base and, in front of the squadron pilots, put the aircraft through a series of spins, each time recovering safely. After landing, he carefully described to all pilots the correct procedures to recover from a spin. Once the pilots became used to the DH.2's characteristics, confidence in the aircraft rose quickly, as they came to appreciate its manoeuvrability.<ref>Guttman 2009, p. 31 – 32.</ref><ref>'Somme Success', P. Hart, Pen & Sword, 2001; page 52</ref>

He then led the squadron back to [[Bertangles]], north of the Somme in February 1916, where the squadron quickly helped counter the [[Fokker Eindecker]] monoplanes of the Imperial [[German Army (German Empire)|German Army]]'s [[Luftstreitkräfte|''Fliegertruppe'']] which were dominant over the Western Front in the run up to the Somme offensive in July 1916.{{citation needed|date=December 2012}} Hawker's aggressive personal philosophy of "Attack Everything", was the entire text of his tactical order of 30 June 1916.<ref>Guttman 2009, p. 34.</ref> Spurred by his aggressiveness, 24 Squadron claimed some 70 victories by November at the cost of 12 of its own planes and 21 pilots killed, wounded or missing.
Around this time, Hawker developed a ring gunsight and created a clamp and spring-clip device to hold the Lewis in place on the D.H.2. He also designed [[sheepskin boots]] that reached to the upper thigh, known as "fug-boots," which became standard issue to combat the risk of frostbite at high altitude.{{citation needed|date=December 2012}}

By mid 1916, RFC policy was to ban squadron commanders from operational flying, Hawker included. However, he continued to make frequent offensive patrols and reconnaissance flights, particularly over the Somme battlefields.

As the year wore on, the Germans introduced far more potent fighters to the front, starting with the [[Luftstreitkräfte]]'s first biplane fighter, the single-gun armed [[Halberstadt D.II]], and shortly thereafter the even more advanced, twin-gunned [[Albatros D.I]], rapidly making the DH.2 obsolete.

== Death ==
[[File:Manfred von Richthofen.jpg|thumb|150px|right|[[Manfred von Richthofen]].]]
On 23 November 1916, while flying an [[Airco DH.2]] (Serial No. 5964), Hawker left [[Bertangles Aerodrome]] at 1300 hours as part of 'A' Flight, led by Capt J. O. Andrews and including Lt (later AVM) [[Robert Saundby|R.H.M.S Saundby]]. Andrews led the flight in an attack on two German aircraft over [[Achiet-le-Grand|Achiet]]. Spotting a larger flight of German aircraft above, Andrews was about to break off the attack, but spotted Hawker diving to attack. Andrews and Saundby followed him to back him up in his fight; Andrews drove off one of the Germans attacking Hawker, then took bullets in his engine and glided out of the fight under Saundby's covering fire. Losing contact with the other DH-2's, Hawker began a lengthy dogfight with an [[Albatros D.II]] flown by Leutnant [[Manfred von Richthofen]] of [[Jasta 2]]. The Albatros was faster than the DH2, more powerful and with a pair of [[MG 08#Aircraft versions|lMG 08 machine guns]], more heavily armed. Richthofen fired 900 rounds during the running battle. Running low on fuel, Hawker eventually broke away from the combat and attempted to return to Allied lines. The Red Baron's guns jammed 50 yards from the lines, but a bullet from his last burst struck Hawker in the back of his head, killing him instantly. His plane spun from {{convert|1,000|ft|abbr=on}} and crashed {{convert|200|m|yd|abbr=off}} east of [[Luisenhof Farm]], just south of [[Bapaume]] on the Flers Road, becoming the German [[Flying ace|ace]]'s 11th victim. German Grenadiers reported burying Hawker {{convert|250|yd|m|abbr=off}} east of Luisenhof Farm along the roadside.<ref>Guttman 2009, p. 46 – 48.</ref> Richthofen claimed Hawker's Lewis gun from the wreck as a trophy and hung it above the door of his quarters. Major Lanoe George Hawker is listed on the [[Arras Memorial|Arras Flying Services Memorial]] for airmen lost with no known grave.<ref>[http://www.cwgc.org/search/casualty_details.aspx?casualty=785816 CWGC entry]</ref>

== Legacy ==
Hawker's original VC was lost when the Hawker family belongings were left behind after the fall of France in 1940. On their return after the [[Second World War]], they found that their possessions, including the VC, had been stolen. A replacement was issued to Hawker's brother on 3 February 1960, and is now held by the [[Royal Air Force Museum]], [[Hendon]].<ref>{{cite web|url=http://www.victoriacross.org.uk/ccraf.htm|date=1 December 2010|title=The following Victoria Crosses are held by the Royal Air Force Museum|accessdate=13 September 2013|publisher=victoriacross.org}}</ref> Hawker was a first cousin of [[Arthur Bagot]], a naval officer in the First World War and [[Albert Medal (lifesaving)|Albert Medal]] recipient.<ref>Staunton 2005, p. 298.</ref>

A window (designed by [[Francis Skeat]]) commemorating Hawker was installed in St. Nicholas church, [[Longparish]] in 1967.<ref>{{cite web|last=Eberhard|first=Robert |title=Stained Glass Windows at St. Nicholas, Longparish, Hampshire |url=http://www.stainedglassrecords.org/Ch.asp?ChId=2467|publisher=Church Stained Glass Windows|accessdate=25 December 2010 |date=October 2009}}</ref> The design features [[St. Michael]] above an airfield with two pilots in the foreground.<ref>{{cite web|title=St. Nicholas, Longparish|url=http://www.hampshirechurchwindows.co.uk/churches/lp5.JPG|publisher=Hampshire Church Windows|accessdate=25 December 2010}}</ref> There is a copy of the window at the [[Museum of Army Flying]] at [[Middle Wallop]].<ref>{{cite web|title=St. Nicholas Church|url=http://www.longparish.org.uk/handbook/lvh20.php|publisher=Longparish Village Handbook|accessdate=25 December 2010|date=8 January 2008}}</ref>

===Hawker in film and fiction===
He was portrayed by [[Corin Redgrave]] in the 1971 film ''[[Von Richthofen and Brown]]''. In the 2008 film [[The Red Baron (2008 film)|''The Red Baron'']], he was played by [[Richard Krajčo]].

== References ==
{{Reflist|30em|refs=
<ref name="bowyer">{{cite journal|title=Hawker&nbsp;– Pioneer of air fighting | last= Bowyer | first= Chaz |journal = Aircraft Illustrated Extra| publisher=Ian Allan Ltd | issue=7 |pages=11–18}}</ref>
}}

== Bibliography ==
{{Refbegin}}
*[[Monuments To Courage]] (David Harvey, 1999)
*[[The Register of the Victoria Cross]] (This England, 1997)
*[[The Sapper VCs]] (Gerald Napier, 1998)
*[[VCs of the First World War - Air VCs]] (P G Cooksley, 1999)
*[[Windsock Datafile No.44, Bristol Scouts]] (J M Bruce & Albatross Publications,1994)
*{{Cite book | author=Barker, Ralph | title=The Royal Flying Corps in World War I | publisher=Robinson | year=2002 | isbn=1-84119-470-0}}
*{{Cite book | author=Guttman, Jon | title=Pusher Aces of World War I | publisher=Osprey Pub Co | year=2009 | isbn=978-1-84603-417-6| id=ISBN 1-84603-417-5 }}
* {{Cite book|last=Staunton|first=Anthony|title=Victoria Cross: Australia's Finest and the Battles they Fought|year=2005|publisher=Hardie Grant Books|location=Prahran, Victoria, Australia|isbn=1-74066-288-1}}
{{Refend}}

== External links ==
*[http://www.theaerodrome.com/aces/england/hawker.html Lanoe Hawker]
* {{Find a Grave|9485851|Lanoe George Hawker}}

{{wwi-air}}

{{Authority control}}
{{DEFAULTSORT:Hawker, Lanoe George}}
[[Category:1890 births]]
[[Category:1916 deaths]]
[[Category:Aviators killed by being shot down]]
[[Category:Aviation pioneers]]
[[Category:British Army personnel of World War I]]
[[Category:British military personnel killed in World War I]]
[[Category:British World War I flying aces]]
[[Category:British World War I recipients of the Victoria Cross]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:People from Longparish]]
[[Category:Royal Engineers officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Graduates of the Royal Military Academy, Woolwich]]
[[Category:British Army recipients of the Victoria Cross]]
[[Category:People educated at Stubbington House School]]
[[Category:English aviators]]