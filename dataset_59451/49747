{{other uses}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = News from Nowhere (or An Epoch of Rest)
| image        = Kelmscott Manor News from Nowhere.jpg
| caption      = Frontispiece
| author       = [[William Morris]]
| country      = United Kingdom
| language     = English
| genre        = Novel
| publisher    = NA
| release_date = 1890
| media_type   = Print (Hardback, Paperback)
| pages        = 186
}}
'''''News from Nowhere''''' (1890) is a classic work combining [[utopian socialism]] and [[soft science fiction]] written by the artist, designer and socialist pioneer [[William Morris]]. It was first published in serial form in the ''[[Commonweal (UK)|Commonweal]]'' journal beginning on 11 January 1890. In the novel, the narrator, William Guest, falls asleep after returning from a meeting of the [[Socialist League (UK, 1885)|Socialist League]] and awakes to find himself in a future society based on [[common ownership]] and democratic control of the [[means of production]]. In this society there is no private property, no big cities, no authority, no monetary system, no divorce, no courts, no prisons, and no class systems. This agrarian society functions simply because the people find pleasure in nature, and therefore they find pleasure in their work.

The novel explores a number of aspects of this society, including its organisation and the relationships which it engenders between people. Morris fuses [[Marxism]] and the romance tradition when he presents himself as an enchanted figure in a time and place different from Victorian England. As Morris, the romance character, quests for love and fellowship—and through them for a reborn self—he encounters romance archetypes in Marxist guises. Old Hammond is both the communist educator who teaches Morris the new world and the wise old man of romance. Dick and Clara are good comrades and the married lovers who aid Morris in his wanderings. The journey on the Thames is both a voyage through society transformed by revolution and a quest for happiness. The goal of the quest, met and found though only transiently, is Ellen, the symbol of the reborn age and the bride the alien cannot win. Ellen herself is a multidimensional figure: a working class woman emancipated under socialism, she is also a benign nature spirit as well as the soul in the form of a woman.<ref name="ReferenceA">Silver, Carole. The Romance of William Morris. Athens, Ohio: Ohio UP, 1982</ref> The book offers Morris' answers to a number of frequent objections to socialism, and underlines his belief that socialism will entail not only the abolishment of private property but also of the divisions between art, life, and work.

In the novel, Morris tackles one of the most common [[criticisms of socialism]]; the supposed lack of incentive to work in a communistic society. Morris' response is that all work should be creative and pleasurable. This differs from the majority of Socialist thinkers, who tend to assume that while work is a necessary evil, a well-planned equal society can reduce the amount of work needed to be done by each worker. ''News From Nowhere'' was written as a [[Libertarian socialist]] response to an earlier book called ''[[Looking Backward]]'', a book that epitomises a kind of [[state socialism]] that Morris abhorred. It was also meant to directly influence various currents of thought at the time regarding the tactics to bring about socialism.<ref>{{cite journal |title=Anarchism and Utopia: William Morris's News from Nowhere |author=Michael Holzman |journal=ELH |volume=51 |issue=3 |date=1984 |pages=589–603 |doi=10.2307/2872939 |jstor=2872939}}</ref>

== ''Looking Backward'' ==
Morris reviewed the novel ''[[Looking Backward]]'' in the ''[[Commonweal (UK)|Commonweal]]'' on 21 June 1889. In his review, Morris objects to Bellamy's portrayal of his imagined society as an authority for what socialists believe. Morris writes,
'In short a machine life is the best which Mr. Bellamy can imagine for us on all sides; it is not to be wondered at then that this, his only idea for making labour tolerable is to decrease the amount of it by means of fresh and ever fresh developments of machinery… I believe that this will always be so, and the multiplication of machinery will just multiply machinery; I believe that the ideal of the future does not point to the lessening of men's energy by the reduction of labour to a minimum, but rather the reduction of pain in labour to a minimum, so small that it will cease to be pain; a dream to humanity which can only be dreamed of till men are even more completely equal than Mr. Bellamy's utopia would allow them to be, but which will most assuredly come about when men are really equal in condition.'<ref>Morris, William. "Bellamy's Looking Backward." The William Morris Internet Archive Works (1889). 14 Apr 2008 <http://www.marxists.org/archive/morris/works/1889/backward.htm>.</ref>

Morris’s basic antipathy with Bellamy arose chiefly from his disagreement with Bellamy’s social values and aesthetic convictions. While Bellamy favoured the urban, Morris favoured the pastoral; while Bellamy lauded the [[Industrial Revolution]] and the power of the machine, Morris yearned for the restoration of an organic way of life which utilised machines only to alleviate the burdens which humans might find irksome; while Bellamy sought salvation through an omnipotent state, Morris wished for a time when it would have withered away.<ref name="ReferenceA"/>

More specifically, Morris criticised the limited nature of Bellamy's idea of life. He identifies five concerns – work, technology, centralisation, cities, arts – which demonstrates the "half change" advanced in ''Looking Backward''. Morris's review also contains an alternate future society in each of these instances. This was the framework based on which he would later attempt to elaborate his vision of an utopia in ''News From Nowhere''.<ref>Morris, W. (2003). ''News From Nowhere''. Leopold, D (Ed.). New York, Oxford University Press Inc., New York.</ref>

==Gender in Nowhere==
In ''News From Nowhere'' Morris describes women in the society as ‘respected as a child bearer and rearer of children desired as a woman, loved as a companion, un-anxious for the future of her children’ and hence possessed of an enhanced 'instinct for maternity'. The sexual division of labour remains intact. Women are not exclusively confined to domestic labour, although the range of work they undertake is narrower than that of man; but domestic labour is seen as something for which women are particularly fitted.<ref name="autogenerated1996">Levitas, Ruth. "Who Holds the Hose? Domestic Labor in the Work of Bellamy, Gillman, and Morris." Ebsco Host : Academic Search Premier 6 (1996). 15 Mar 2008 <http://corvette.salemstate.edu:2561/ehost/detail?vid=4&hid=3&sid=a7ff7a45-7fc8-4ef0-9c41-0ca33040fd09%40sessionmgr9>.</ref> Moreover, ‘The men have no longer any opportunity of tyrannising over the women, or the women over the men; both of those took place in old times. The women do what they can do best and what they like best, and the men are neither jealous nor injured by it.’<ref name=Nowhere/> 
The practice of women waiting on men at meals is justified on the grounds that, 
‘It is a great pleasure to a clever woman to manage a house skilfully, and to do so that all house-mates about her look pleased and are grateful to her. And then you know everybody likes to be ordered about by a pretty woman…’<ref name="autogenerated1996"/>

Morris presents us with a society in which women are free from the oppression of men; yet domestic work, though it is respected, remains gender-specific.

== Marriage ==
Morris offers a Marxian view of marriage and divorce. Dick and Clara were once married with two children. Then Clara ‘got it in her head she was in love with someone else,’ so she left Dick only to reconcile with him again.<ref>Marsh, Jan. "Concerning Love: News From Nowhere and Gender." William Morris & News from Nowhere: A Vision for Our Time. Eds. Stephen Coleman and Paddy O'Sullivan. (Bideford, Devon: Green Books, 1990): 107–125</ref> Old Hammond informs the reader that there are no courts in ''Nowhere'', no divorce in ''Nowhere'', and furthermore no contractual marriage in ''Nowhere''. When dealing with marriage and divorce Old Hammond explains,
‘You must understand once for all that we have changed these matters; or rather that our way of looking at them has changed…We do not deceive ourselves, indeed, or believe that we can get rid of all the trouble that besets the sexes… but we are not so mad as to pile up degradation on that unhappiness by engaging in sordid squabbles about livelihood and position, and the power of tyrannising over the children who have been the result of love or lust.'<ref name=Nowhere>{{cite book
| last=Morris
| first=William
| authorlink=William Morris
| title=News from Nowhere and Other Writings
| publisher=[[Penguin Classics]]
|date=January 1994
| origyear=1890
| isbn=0-14-043330-9
}}</ref>
In Nowhere people live in groups of various sizes, as they please, and the nuclear family is not necessary.

Concerning marriage, the people of ''Nowhere'' practice monogamy but are free to pursue romantic love because they are not bound by a contractual marriage.

== Education ==
Early in the novel we learn that though the people of ''Nowhere'' are learned there is no formal schooling for children. Although Oxford still exists as a place to study the 'Art of Knowledge', we learn that people are free to choose their own form of education. As for educating children, we learn that children in ''Nowhere''
‘often make up parties, and come to play in the woods for weeks together in the summer time, living in tents, as you see. We rather encourage them to do it; they learn to do things for themselves, and get to know the wild creatures; and you see the less they stew inside houses the better for them.’<ref name=Nowhere/>

Here Morris breaks away from the traditional institutions of 19th century England. Learning through nature is the best suited lifestyle for this agrarian society.

== How We Might Live ==
''News from Nowhere'' is a [[utopia]]n representation of Morris’ vision of an ideal society. "Nowhere" is in fact a literal translation of the word "utopia".<ref>{{OEtymD|utopia}}</ref> This Utopia, an imagined society, is idyllic because the people in it are free from the burdens of industrialisation and therefore they find harmony in a lifestyle that coexists with the natural world.
In a lecture "How We Live and How We Might Live" 1884, Morris gives his opinions about an ideal existence. This opinion is the bedrock for the novel. Morris writes,
'Before I leave this matter of the surroundings of life, I wish to meet a possible objection. I have spoken of machinery being used freely for releasing people from the more mechanical and repulsive part of necessary labour; it is the allowing of machines to be our masters and not our servants that so injures the beauty of life nowadays. And, again, that leads me to my last claim, which is that the material surroundings of my life should be pleasant, generous, and beautiful; that I know is a large claim, but this I will say about it, that if it cannot be satisfied, if every civilised community cannot provide such surroundings for all its members, I do not want the world to go on.'<ref>Salmon, Nicholas. "The William Morris Internet Archive: Works." Marxist Internet Archive. 14 Apr 2008 <http://www.marxists.org/archive/morris/works/index.htm>.</ref>

==Quotes==
{{Move section to wikiquote}}
* As he formed the words, the train stopped at his station, five minutes' walk from his own house, which stood on the banks of the [[Thames]], a little way above an ugly suspension bridge. He went out of the station, still discontented and unhappy, muttering "If I could but see it! if I could but see it!" but had not gone many steps towards the river before (says our friend who tells the story) all that discontent and trouble seemed to slip off him.
* "I think I know what you mean. You think that I have done you a service; so you feel yourself bound to give me something which I am not to give to a neighbour, unless he has done something special for me. I have heard of this kind of thing; but pardon me for saying, that it seems to us a troublesome and roundabout custom; and we don't know how to manage it. And you see this ferrying and giving people casts about the water is my business, which I would do for anybody; so to take gifts in connection with it would look very queer. Besides, if one person gave me something, then another might, and another, and so on; and I hope you won't think me rude if I say that I shouldn't know where to stow away so many mementos of friendship."
* All this seemed very interesting to me, and I should like to have made the old man talk more. But Dick got rather restive under so much ancient history: besides, I suspect he wanted to keep me as fresh as he could for his great-grandfather. So he burst out laughing at last, and said: "Excuse me, neighbours, but I can't help it. Fancy people not liking to work! — it's too ridiculous. Why, even you like to work, old fellow — sometimes," said he, affectionately patting the old horse with the whip. "What a queer disease! it may well be called Mulleygrubs!"
* "Man alive! how can you ask such a question? Have I not told you that we know what a prison means by the undoubted evidence of really trustworthy books, helped out by our own imaginations? And haven't you specially called me to notice that the people about the roads and streets look happy? and how could they look happy if they knew that their neighbours were shut up in prison, while they bore such things quietly? And if there were people in prison, you couldn't hide it from folk, like you can an occasional man-slaying; because that isn't done of set purpose, with a lot of people backing up the slayer in cold blood, as this prison business is. Prisons, indeed! O no, no, no!"
* "Then you suppose nonsense," said he. "I know that there used to be such lunatic affairs as divorce courts. But just consider; all the cases that came into them were matters of property quarrels: and I think, dear guest," said he, smiling, "that though you do come from another planet, you can see from the mere outside look of our world that quarrels about private property could not go on amongst us in our days."
* But of course I understand your point of view about education, which is that of times past, when 'the struggle for life,' as men used to phrase it (i.e., the struggle for a slave's rations on one side, and for a bouncing share of the slave-holders' privilege on the other), pinched 'education' for most people into a niggardly dole of not very accurate information; something to be swallowed by the beginner in the art of living whether he liked it or not, and was hungry for it or not: and which had been chewed and digested over and over again by people who didn't care about it in order to serve it out to other people who didn't care about it."
* "I must now shock you by telling you that we have no longer anything which you, a native of another planet, would call a government."
* "A terrible tyranny our Communism, is it not? Folk used often to be warned against this very unhappiness in times past, when for every well-fed, contented person you saw a thousand miserable starvelings. Whereas for us, we grow fat and well-liking on the tyranny; a tyranny, to say the truth, not to be made visible by any microscope I know. Don't be afraid, my friend; we are not going to seek for troubles by calling our peace and plenty and happiness by ill names whose very meaning we have forgotten!"
* "As a matter of fact, the history of the terrible period of transition from commercial slavery to freedom may thus be summarised. When the hope of realising a communal condition of life for all men arose, quite late in the nineteenth century, the power of the middle classes, the then tyrants of society, was so enormous and crushing, that to almost all men, even those who had, you may say despite themselves, despite their reason and judgement, conceived such hopes, it seemed a dream. So much was this the case that some of those more enlightened men who were then called Socialists, although they well knew, and even stated in public, that the only reasonable condition of Society was that of pure Communism (such as you now see around you), yet shrunk from what seemed to them the barren task of preaching the realisation of a happy dream. Looking back now, we can see that the great motive-power of the change was a longing for freedom and equality, akin if you please to the unreasonable passion of the lover; a sickness of heart that rejected with loathing the aimless solitary life of the well-educated man of that time: phrases, my dear friend, which have lost their meaning to us of the present day; so far removed we are from the dreadful facts which they represent."
* "Go back again, now you have seen us, and your outward eyes have learned that in spite of all the infallible maxims of your day there is yet a time of rest in store for the world, when mastery has changed into fellowship — but not before. Go back again, then, and while you live you will see all round you people engaged in making others live lives which are not their own, while they themselves care nothing for their own real lives — men who hate life though they fear death. Go back and be the happier for having seen us, for having added a little hope to your struggle. Go on living while you may, striving, with whatsoever pain and labour needs must be, to build up little by little the new day of fellowship, and rest, and happiness."

==Influence==
The title ''News from Nowhere'' has inspired many enterprises, including a [[Independent bookstore|political bookstore]] in Liverpool,<ref>{{cite web|author=News From Nowhere Co-operative Ltd |url=http://www.newsfromnowhere.org.uk/ |title=News From Nowhere Radical & Community Bookshop, Liverpool |publisher=Newsfromnowhere.org.uk |date= |accessdate=10 February 2013}}</ref> a theatre company<ref>{{cite web|url=http://www.newsfromnowhere.net/ |title=Tim Crouch news from nowhere -news from nowhere |publisher=Newsfromnowhere.net |date= |accessdate=10 February 2013}}</ref> and a short film describing a fictional trip by Morris up the [[River Thames]] exploring ideas of aesthetic and socialism.<ref>{{cite web|url=http://www.timeout.com/film/reviews/63268/news-from-nowhere.html |title=News from Nowhere &#124; review, synopsis, book tickets, showtimes, movie release date &#124; Time Out London |publisher=Timeout.com |date= |accessdate=10 February 2013}}</ref> A contemporary art exhibition at the [[Lucy Mackintosh]] Gallery in Lausanne, Switzerland, with six British artists: Michael Ashcroft, Juan Bolivar, Andrew Grassie, Justin Hibbs, Alistair Hudson, and [[Peter Liversidge]] during April–May 2005 was called ''News From Nowhere''.<ref>{{cite web|url=http://lucymackintosh.ch/archives.php?id=19 |title=Galerie Lucy Mackintosh |publisher=Lucymackintosh.ch |date= |accessdate=10 February 2013}}</ref>

Folk singer [[Leon Rosselson]]'s song "Bringing the News from Nowhere", from his eponymous 1986 album, is a tribute to Morris.

''News From Nowhere'' was an influencing factor in historian [[G. D. H. Cole]]'s conversion to socialism.<ref>Cole, Margaret. ''The Life of G.D.H. Cole'', p.33-4.</ref>

A track on ''[[Dig, Lazarus, Dig!!!]]'' by [[Nick Cave and the Bad Seeds]] (2008) is called "[[More News from Nowhere]]".

In 2008 Waltham Forest commissioned the composer, Mike Roberts, to create a new community symphony based on the story. Incorporating Morris' axiom of 'art for the people and by the people' the piece was written in collaboration with 180 primary school children - all composing small fragments of music that were weaved into the final piece.  The result was a 90-minute work for children's choir, orchestra and 10 other smaller ensembles.  The work is being recorded with the artistic support of The William Morris Gallery during 2014-15 for release in June 2015 to commemorate the novel's 125th anniversary.

The novel ''News from Gardenia'' (2012) by [[Robert Llewellyn]] was influenced by ''News from Nowhere''.

Korean artists Moon Kyungwon and Jeon Joonho were inspired by ''News from Nowhere'' in their collaborative project "News from Nowhere" (2012).<ref>{{cite web|url=http://www.newsfromnowhere.kr/ |title=Home : News from nowhere |publisher=Newsfromnowhere.kr |date= |accessdate=10 February 2013}}</ref>

In 2013 the English band [[Darkstar (band)|Darkstar]] released an album titled ''News from Nowhere''.

The book was adapted by Sarah Woods as a radio play, broadcast by [[BBC Radio 4]] on 25 May 2016.

==See also==
* ''[[Erewhon]]'' — 1872 utopian novel and satire on Victorian society by [[Samuel Butler (novelist)|Samuel Butler]]
* ''[[Looking Backward]]'' — 1887 novel by [[Edward Bellamy]] in which the American protagonist falls asleep in 1887 and awakes in a socialist utopia in 2000

==References==
{{reflist}}

==External links==
{{Wikisource}}
{{Gutenberg|no=3261|name=News from Nowhere}}
*{{Librivox book|title=News from Nowhere|author=William Morris}}
*[http://www.morrissociety.org/socialist.html William Morris Societies – Social thought portal]
*[http://www.marxists.org/archive/morris/works/index.htm The William Morris Internet Archive: Works. Marxist Internet Archive.]
*[http://www.icmus.org/ The News from Nowhere Audio Album]

{{William Morris}}

[[Category:1890 in the United Kingdom]]
[[Category:1890 novels]]
[[Category:1890s science fiction novels]]
[[Category:British science fiction novels]]
[[Category:Novels by William Morris]]
[[Category:Social science fiction]]
[[Category:Socialist League (UK, 1885)]]
[[Category:Utopian novels]]
[[Category:19th-century British novels]]
[[Category:Novels set in the future]]