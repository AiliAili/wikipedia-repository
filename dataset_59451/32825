{{featured article}}
{{Infobox officeholder
| name      = Mary Margaret O'Reilly
| image     = Mary M. O'Reilly was acting director of the Mints and Assay Office of the United States Department of the Treasury B.jpg
| office    = 1st Assistant Director of the United States Bureau of the Mint
| president =
  {{plainlist |
* [[Calvin Coolidge]]
* [[Herbert Hoover]]
* [[Franklin D. Roosevelt]]
  }}
| term_start = July 1, 1924
| term_end   = October 29, 1938
| 1blankname = Mint Director
| 1namedata  = {{Plainlist|
* [[Robert J. Grant]] (1924–33)
* [[Nellie Tayloe Ross]] (1933–38)
}}
| successor   = [[F. Leland Howard]]
| birth_date  = {{birth date|1865|10|14}}
| birth_place = [[Springfield, Massachusetts]], United States
| death_date  = {{death date and age|1949|12|6|1865|10|14}}
| death_place = Washington, DC
| nationality = American
}}

'''Mary Margaret O'Reilly''' (October 14, 1865&nbsp;– December 6, 1949) was an American [[civil service|civil servant]] who worked as the Assistant Director of the [[United States Mint|United States Bureau of the Mint]] from 1924 until 1938. One of the United States' highest-ranking female civil servants of her time, she worked at the Mint for 34 years, during which she often served as acting director during the [[Director of the United States Mint|Mint Director]]'s absence.

O'Reilly was born in [[Springfield, Massachusetts]] to an [[Irish American|Irish immigrant family]]. Growing up in that state, she left school around the age of 14 to help support both her widowed mother and her siblings. Likely starting work in the local [[textile mill]]s, she gained [[Clerk|clerical training]] at [[night school]] before working as a clerk in [[Worcester, Massachusetts|Worcester]] for eighteen years.  In 1904, O'Reilly gained a position at the Mint Bureau, resulting in a move to [[Washington, D.C]].  She rose rapidly in the bureau's hierarchy &ndash; an unusual feat for a woman at that time &ndash; and was frequently called upon to testify before the [[United States Congress]]. As many of the Mint's directors were political appointees who had little knowledge or interest in the bureau's operations, the task of running the institution often fell to her. In 1924 she was officially appointed Assistant Director.

In 1933, the Mint gained its first female Director, [[Nellie Tayloe Ross]], and despite initial mistrust between her and O'Reilly, they came to forge a strong bond. Although scheduled for mandatory retirement in 1935, O'Reilly was considered to be so indispensable to the bureau's operations that [[President of the United States|U.S. President]] [[Franklin D. Roosevelt]] postponed this until 1938. During her later years, O'Reilly remained in Washington D.C.; she no longer involved herself in Mint affairs, instead devoting much of her attention to [[Catholic Church|Catholic]] charitable work.

== Early life and career ==
Mary Margaret O'Reilly was born in [[Springfield, Massachusetts]],<ref name = "obit">{{cite news|title=Miss Mary O'Reilly, U.S. Mint Ex-Aide, 84|url=https://query.nytimes.com/mem/archive-free/pdf?res=9505E0D61E3BE23BBC4E53DFB4678382659EDE|newspaper=[[The New York Times]]|page=31|date=December 6, 1949}}{{subscription}}</ref> on October 14, 1865.{{sfn|Burdette 2014|p=23}} Her parents, James A. and Joanna O'Reilly, were immigrants from Ireland, and Mary was one of five children.{{sfn|Burdette 2014|p=23}}  The family lived in Springfield and nearby [[Chicopee, Massachusetts]], where James O'Reilly was a liquor wholesaler. He died after an illness in 1873.{{sfn|Burdette 2014|p=23}} As well as depriving the family of income, his death caused his family legal trouble: Austin O'Reilly,{{efn|Relationship unknown. See {{harvnb|Burdette 2014|p=23}}.}} a clerk in the now-closed O'Reilly business, tried to settle the estate by selling the remaining alcohol, but lacked a license to do so. Joanna O'Reilly denied any knowledge of business affairs. Austin's conviction for transporting liquor without a license was upheld by the [[Massachusetts Supreme Judicial Court]].<ref>{{cite court
 |litigants =Commonwealth v. Austin O'Reilly
 |vol =116
 |reporter =Mass.
 |opinion =14
 |pinpoint =14
 |court =[[Massachusetts Supreme Judicial Court]].
 |date =September 25, 1874
 |url= https://books.google.com/books?id=6B8QAAAAYAAJ&pg=RA1-PA14}}</ref>

Mary left school after [[student#first year|ninth grade]], at or soon after age 14, as her help was needed to support the family. She likely worked for one of the local [[textile mill]]s, and attended night school to train as a clerk and stenographer.{{sfn|Burdette 2014|p=23}}  From 1885 to 1903, she worked as a clerk, living in [[Worcester, Massachusetts|Worcester]] along with a brother, in a [[boarding house]] owned by their mother.{{sfn|Burdette 2014|p=23}}

== Mint career (1904–1938) ==

=== Rise to prominence ===
O'Reilly was hired by the [[United States Mint|United States Bureau of the Mint]] as a Class D temporary clerk in 1904, when she was 38 years old, older than most new employees.{{sfn|Burdette 2014|p=23}} She served in the Washington, D.C. headquarters of the Bureau of the Mint, where [[Director of the United States Mint|Mint Director]] [[George E. Roberts]] was impressed by her business experience and competence.  Initially having only temporary status, she was made a permanent employee in 1905, and was promoted again that year to Clerk Class I at a salary of $1,200. When [[Margaret Kelly (civil servant)|Margaret Kelly]] was commissioned Examiner of the Bureau of the Mint in 1911, the ripple of promotions in her wake included O'Reilly, who became adjuster of accounts.  This made her in effect the chief clerk of the Mint Bureau, with responsibility for reviewing all contracts.{{sfn|Burdette 2014|p=24}} According to Teva J. Scheer, biographer of [[Nellie Tayloe Ross]] (O'Reilly's final Mint Director before retirement) "it must have required an almost unprecedented combination of drive and intelligence for [O'Reilly] to have climbed so far up through the organization in her male-dominated work environment".{{sfn|Scheer|p=183}}

[[File:Roberts medal.png|thumb|left|Mint Director [[George E. Roberts]] (shown on his Mint medal by Chief Engraver [[Charles E. Barber]])]]
During the 1910s, O'Reilly continued to gain promotion, serving both as examiner and as computer of bullion.  She was frequently called upon to testify before [[United States Congress|Congress]]. In 1915, [[Robert W. Woolley]] was appointed Mint Director, and was likely O'Reilly's favorite of those who served in that position during her third of a century at the Mint.  She often concluded memoranda with personal good wishes, and Woolley reciprocated. After Woolley resigned in August 1916, O'Reilly served as acting director for part of the time until Woolley's successor [[Friedrich Johannes Hugo von Engelken]] took office the next month, though Adjuster of the Bureau of the Mint Fred H. Chafflin held the acting position for much of the interregnum.{{sfn|Burdette 2014|p=24}}{{sfn|Burdette 2005|pp=56–58}}

=== Assistant director ===
Most directors of the Mint of the early 20th century were political appointees, lacking previous experience with the bureau.{{sfn|Scheer|p=182}} Von Engelken during his six-month term as director in 1916 and 1917 left almost all supervision of the mints and assay offices to O'Reilly. The Mint eliminated production of [[proof coin]]s, popular among collectors, in 1916.  Although the suggestion that the bureau eliminate the special coins, on which it lost money, came from [[Philadelphia Mint]] Superintendent Adam M. Joyce, and was approved by von Engelken, O'Reilly signed many of the letters to numismatists, and thus was blamed for the change in policy.{{sfn|Burdette 2014|p=25}}{{sfn|Burdette 2005|pp=165–167}}

When von Engelken resigned in February 1917, his successor was [[Raymond T. Baker]], who foresaw that women would hold high government positions in increasing numbers, and gave O'Reilly a more public role.  Each year, Baker appeared before Congress to defend the bureau's appropriation requests, and O'Reilly sat behind him.  In 1920 and 1921, Baker tried to get Congress to formally designate O'Reilly, who then held the title of executive clerk, as Assistant Director, but without success. After the [[Presidency of Warren G. Harding|Harding administration]] took office, Baker was replaced in 1922 by [[Frank E. Scobey]], one of [[Warren G. Harding|Harding]]'s [[Ohio Gang]].  The new director had little interest in Mint affairs, and O'Reilly not only supervised the bureau's operations, but was the chief witness before Congress in 1922, defending both the appropriation request and the continuing drive to have herself designated Assistant Director.  This time, Congress was more amenable, and she gained the title effective from 1924.<ref name = "obit" />{{sfn|Burdette 2014|pp=24–28}}

In December 1921, a public relations crisis over the design of the new [[Peace dollar]] had erupted while Baker was on a three-day trip by train to the West Coast. [[Anthony de Francisci]], designer of the coin, had included a broken sword on the [[obverse and reverse|reverse]], which he intended as a sign of the end of war, but which many interpreted as a symbol of disgrace.  Anger at such a design resonated in a country deeply sensitive about such matters due to [[World War I]]. With Baker unreachable, O'Reilly realized the sword would have to be removed, and approached Treasury Undersecretary [[Seymour Parker Gilbert]], who as acting secretary approved a revised design.  The Mint's Chief Engraver, [[George T. Morgan]], skillfully removed the sword from the already-prepared coinage [[Glossary of numismatics#H|hubs]] even before Baker cabled his own approval of the revised design he had not seen.{{sfn|Burdette 2005|pp=35, 208–214}}

O'Reilly ran most Mint operations under Scobey and his successor, [[Robert J. Grant]].{{sfn|Burdette 2014|pp=28, 30}} Although the Mint Bureau was very busy in the booming economy of the 1920s, numismatic historian Roger Burdette points out that there were flaws in operations—for example, Philadelphia Mint officials, instead of setting aside gold coins from each batch delivered for inspecting and testing by the [[United States Assay Commission|annual Assay Commission]], took all assay coins from a bag set aside at the start of the year, increasing the likelihood that nonstandard coins would go undetected.{{efn|The Assay Commission, made up of officials and presidentially appointed members of the public, tested relatively few coins, but had detected that some 1920-dated quarters struck at Denver were made of silver that was too pure, something missed by the Denver Mint in internal checks. See {{harvnb|Mellon|p=631}}.}} O'Reilly did keep a close eye on coinage operations, warning the [[San Francisco Mint]] in November 1931 that it had produced fewer than 200,000 [[nickel (United States coin)|nickels]], a figure that if allowed to stand would have resulted in the issue being hoarded by collectors.  She directed the mint to strike nothing but nickels for the remainder of the year,{{sfn|Burdette 2014|p=30}} resulting in a total mintage for the 1931–S{{efn|That is, with a 1931 date, and bearing the S [[mint mark]] for San Francisco.}} of 1,200,000, still the second-lowest by date and mint mark in the [[Buffalo nickel]] series.{{sfn|Yeoman|pp=133–134}}

=== Roosevelt administration and retirement ===
When the Democratic [[Presidency of Franklin Delano Roosevelt|Roosevelt administration]] took office in 1933, O'Reilly was serving as acting director following Grant's resignation.{{sfn|Greenbaum|p=45}} President [[Franklin Delano Roosevelt|Franklin Roosevelt]] (FDR) appointed former Wyoming governor [[Nellie Tayloe Ross]] as Mint Director, the first woman to hold that position.{{sfn|Burdette 2014|p=30}} By then, O'Reilly was 67 years old, and appeared as a small, grandmotherly figure who was dubbed "the Sweetheart of the Treasury"—an appearance that hid her mental strength and determination.{{sfn|Scheer|p=182}}<ref name = "sweet" />  Ross's personal secretary, Edness Wilkins, described the Assistant Director of the Mint as "ruthless".{{sfn|Scheer|p=182}}<ref name = "sweet" /><!-- quote in Scheer -->

[[File:Nellie Tayloe Ross medal.jpeg|thumb|left|[[Nellie Tayloe Ross]], as seen on her Mint medal designed by Chief Engraver [[John R. Sinnock]]]]
Ross and O'Reilly had mutual suspicions to overcome.  Ross, who had recently endured poor relations with [[Eleanor Roosevelt]] and others on FDR's campaign, did not trust the career staff.  O'Reilly saw another political appointee with no experience at the Mint Bureau replacing Grant, who had been [[Denver Mint]] superintendent before his directorship.{{sfn|Scheer|p=182}} After a brief period, the two women came to appreciate each other's merits.{{sfn|Scheer|p=183}}

Among the issues that the Mint Bureau had to face in 1933 and 1934 was the calling-in of most gold coins. When the Treasury Department issued regulations allowing such coins to be surrendered at branches of the [[Federal Reserve Bank]], O'Reilly sent out a memorandum over her signature as acting director noting that the Fed had no facilities to accept any gold other than [[gold bar|bars]] with a government stamp.{{sfn|Burdette 2014|pp=30–31}} At the time, the Mint Bureau was one of the lowest-status branches of the Department of the Treasury, esteemed far less than the [[United States Secret Service|Secret Service]] and other law enforcement-related agencies that fell under the Treasury Secretary.{{sfn|Scheer|pp=178–179}} Burdette points out that the gold regulations showed a lack of basic Mint knowledge both by Roosevelt's appointees and the holdover senior officials from the [[Presidency of Herbert Hoover|Hoover administration]].{{sfn|Burdette 2014|pp=30–31}}

[[File:Oreilly before congress.jpg|thumb|O'Reilly and C.M. Hester, Treasury Assistant General Counsel, testify before Congress, August 1935]]
Ross and O'Reilly soon came to the usual division of labor between the director and assistant: the director would handle public affairs and make policy decisions as needed, while the assistant dealt with the day-to-day business of the bureau. Ross undertook a heavy travel schedule, visiting Mint facilities, making speeches backing Roosevelt, and campaigning for Democratic candidates in Wyoming.  This left O'Reilly running the Washington office as acting director.{{sfn|Scheer|pp=183–184}} The two women carried on a businesslike but warm correspondence during these times, with O'Reilly writing to Ross (who had embarked on a tour of the mints) "I am so anxious to have your mind at ease about the office here [in Washington] that I have resorted to rather frequent telegrams.  They are so much more direct and up to date than letters&nbsp;... my love to you and every good wish for the success of your visits to our beloved mint institutions."{{sfn|Scheer|pp=183–184}} Scheer suggests that O'Reilly would have found Ross's reports from the field valuable; they showed how the Mint recovered from the initial years of the Depression, when relatively few coins were produced, to the mid-1930s, when strong demand for coinage led the bureau to run the mints with two or even three shifts.{{sfn|Scheer|pp=176, 184}}

In 1935, O'Reilly reached the [[mandatory retirement|mandatory federal retirement]] age of 70.  Her knowledge of bureau affairs was so extensive, and was so badly needed, that she was exempted from mandatory retirement by special order of President Roosevelt, at the request of Ross, giving O'Reilly an extra year in the Mint Service.{{sfn|Burdette 2014|pp=32–33}}  Although Ross supported the extension, she could not be seen as unable to do her job without O'Reilly's assistance, and hired Frank Leland Howard of the [[University of Virginia]], who had a background in accounting, as O'Reilly's prospective replacement.{{sfn|Burdette 2014|pp=32–33}} Roosevelt approved a similar extension in 1936, a distinction considered so significant that Treasury Secretary [[Henry Morgenthau, Jr.]] hosted a luncheon in her honor. Roosevelt again extended her federal service by one year in late 1937, though warning that he would not exempt her again. An attempt by Morgenthau to further extend her tenure was turned down by the president the next July, and she retired on October 29, 1938, to be replaced by Howard.{{sfn|Burdette 2014|pp=32–33}}

At O'Reilly's request, there was no ceremony to mark her retirement, though her fellow employees chipped in to buy her a diamond-encrusted watch, which they persuaded her to accept.<ref name = "sweet" /> President Roosevelt and Secretary Morgenthau sent letters of appreciation for her service.{{sfn|Burdette 2014|p=33}} ''[[The New York Times]]'' carried word of her retirement, but no interview,<ref name=sweet>{{cite news |author= |coauthors= |title=Sweetheart Of The Treasury |url=https://select.nytimes.com/gst/abstract.html?res=FB0F11F93D5F1B7A93C5A9178AD95F4C8385F9  |newspaper=[[The New York Times]] |date=November 7, 1938 }}{{subscription}}</ref> and a week later editorialized that "there is modernity here, too.  An answer to America's challenge to women. It points to what women want out of life, and what women can get and give."<ref name = "sweet" />

== Retirement and death ==
After her retirement, O'Reilly continued to live in her rooms at the [[Hay–Adams Hotel|Hay–Adams residence]] in Washington. She did not involve herself in Mint affairs; though Morgenthau sent her a few letters, they did not mention business.{{sfn|Burdette 2014|p=33}} O'Reilly kept busy by organizing fundraising for Catholic charities.{{sfn|Burdette 2014|p=33}} She was not interviewed when the Mint in 1944 investigated how several [[1933 double eagle]]s, never officially released, had come onto the market, an omission Burdette finds unusual.{{sfn|Burdette 2014|p=33}}

O'Reilly died on December 6, 1949 in Washington.  Her ''New York Times'' obituary recalled that when she had been granted the first extension by Roosevelt, reporters had sought to interview her, only to be met with the following statement:

{{quote|
I am deeply grateful to the President for his extreme kindness.  Life without work does not remotely interest me. But do you have to print anything about me?<ref name = "obit" />
}}

== Notes ==
{{Notes}}

== References ==
{{Reflist|30em}}

== Sources ==
{{Refbegin}}
*{{cite book
  | last = Burdette
  | first = Roger
  | year = 2005
  | title = Renaissance of American Coinage, 1916–1921
  | publisher = Seneca Mill Press LLC
  | isbn = 978-0-9768986-0-3
  | ref = {{sfnRef|Burdette 2005}}
  }}
*{{cite journal|last=Burdette|first=Roger|journal=Journal of Numismatic Research|publisher=Seneca Mill Press LLC|title=The Women Who Ran the Mint|date=Winter 2014|pages=4–56|ref={{sfnRef|Burdette 2014}}}}
*{{cite journal
 | last = Greenbaum
 | first = Gary M.
 | journal = The Numismatist
 | date = October 2013
 | title = The Other Side of the Oregon Trail Half Dollar
 | publisher = [[American Numismatic Association]]
 | pages = 42–49
 | ref = {{sfnRef|Greenbaum}}
}}
*{{cite book
| last = Mellon
| first = Andrew W.
| authorlink = Andrew W. Mellon
| year = 1922
| title = Annual Report of the Secretary of the Treasury, 1922
| publisher = United States Government Printing Office
| location = Washington, DC
| url = https://books.google.com/books?id=IoNQAAAAYAAJ&dq=assay%20commission&pg=PA631#v=onepage&q=assay%20commission&f=false
|ref={{sfnRef|Mellon}}
}}
*{{cite book|title=Governor Lady: The Life and Times of Nellie Tayloe Ross|last=Scheer|first=Teva J.|publisher=University of Missouri Press|date=2005|isbn=978-0-8262-1626-7|url=https://books.google.com/books?id=bR0IPybCPwcC|ref={{sfnRef|Scheer}}}}
*{{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2014
  | title = [[A Guide Book of United States Coins]]
  | edition = 68th
  | publisher = Whitman Publishing LLC
  | isbn = 978-0-7948-4215-4
  | ref = {{sfnRef|Yeoman}}
  }}
{{Refend}}

{{DEFAULTSORT:OReilly, Mary Margaret}}
[[Category:1865 births]]
[[Category:1949 deaths]]
[[Category:People from Springfield, Massachusetts]]
[[Category:United States Department of the Treasury officials]]
[[Category:American people of Irish descent]]
[[Category:American women civil servants]]
[[Category:19th-century women]]
[[Category:20th-century women]]