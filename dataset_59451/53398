{{for|the computer programming concept|Bookkeeping code}}
{{Refimprove|date=December 2013}}
{{Bookkeeping}}[[File:Pacioli.jpg|thumb|right|250px|''Portrait of the Italian [[Luca Pacioli]]'', painted by [[Jacopo de' Barbari]], 1495, ([[Museo di Capodimonte]]). Pacioli is regarded as the Father of Accounting and 
.]]
'''Bookkeeping''' is the recording of financial transactions, and is part of the process of [[accounting]] in [[business]].<ref name="Weygandt, Kieso, Kimmel (2003)">{{cite book |author=Weygandt |author2=Kieso |author3=Kimmel |title=Financial Accounting|year=2003|publisher=Susan Elbe|isbn=0-471-07241-9|pages=6}}</ref>  Transactions include purchases, sales, receipts, and payments by an individual person or an organization/corporation. There are several standard methods of bookkeeping, such as the [[single-entry bookkeeping system]] and the [[double-entry bookkeeping system]], but, while they may be thought of as "real" bookkeeping, any process that involves the recording of financial transactions is a bookkeeping process.

Bookkeeping is usually performed by a '''bookkeeper'''. A bookkeeper (or book-keeper) is a person who records the day-to-day financial transactions of a business. He or she is usually responsible for writing the ''daybooks'', which contain records of purchases, sales, receipts, and payments. The bookkeeper is responsible for ensuring that all transactions whether it is cash transaction or credit transaction are recorded in the correct daybook, supplier's ledger, customer ledger, and [[general ledger]]; an accountant can then create reports from the information concerning the financial transactions recorded by the bookkeeper.

The bookkeeper brings the books to the [[trial balance]] stage: an accountant may prepare the [[income statement]] and [[balance sheet]] using the trial balance and ledgers prepared by the bookkeeper.

== History ==
The origin of book-keeping is lost in obscurity, but recent researches would appear to show that some method of keeping accounts has existed from the remotest times. Babylonian records have been found dating back as far as 2600 B.C., written with a stylus on small slabs of clay.<ref>{{cite EB1911|wstitle=Book-Keeping|volume=4|page=225}}</ref> 
The term "waste book" was used in colonial America referring to bookkeeping. The purpose was to document daily transactions including receipts and expenditures. This was recorded in chronological order, and the purpose was for temporary use only. The daily transactions would then be recorded in a daybook or account ledger in order to balance the accounts. The name "waste book" comes from the fact that once the waste book's data were transferred to the actual journal, the waste book could be discarded.<ref>{{cite web | publisher= Guides to Archives and Manuscript Collections at the University of Pittsburgh Library System |title= Pittsburgh Waste Book and Fort Pitt Trading Post Papers |url= http://digital.library.pitt.edu/cgi-bin/f/findaid/findaid-idx?c=ascead;cc=ascead;q1=General%20John%20Forbes;rgn=main;view=text;didno=US-PPiU-dar192503 |access-date= 2015-09-04 }}</ref>

== Process ==
The bookkeeping process primarily records the ''financial effects'' of transactions. The difference between a manual and any electronic accounting system results from the former's [[latency (engineering)|latency]] between the recording of a financial transaction and its posting in the relevant account. This delay—absent in electronic accounting systems due to nearly instantaneous posting into relevant accounts—is a basic characteristic of manual systems, thus giving rise to primary books of accounts such as Cash Book, Bank Book, Purchase Book, and Sales Book for recording the immediate effect of a financial transaction.

In the normal course of business, a document is produced each time a transaction occurs. Sales and purchases usually have [[invoice]]s or [[receipt]]s. Deposit slips are produced when lodgements (deposits) are made to a [[bank account]]. Checks (spelled "cheques" in the UK and several other countries) are written to pay money out of the account. Bookkeeping first involves recording the details of all of these ''source documents'' into multi-column ''journals'' (also known as ''books of first entry'' or ''daybooks''). For example, all credit sales are recorded in the sales journal; all cash payments are recorded in the cash payments journal. Each column in a journal normally corresponds to an account. In the [[single-entry accounting system|single entry system]], each transaction is recorded only once. Most individuals who balance their check-book each month are using such a system, and most personal-finance software follows this approach.

After a certain period, typically a month, each column in each [[Journal entry|journal]] is totalled to give a summary for that period. Using the rules of double-entry, these journal summaries are then transferred to their respective accounts in the [[ledger]], or ''account book''. For example, the entries in the Sales Journal are taken and a debit entry is made in each customer's account (showing that the customer now owes us money), and a credit entry might be made in the account for "Sale of class 2 widgets" (showing that this activity has generated revenue for us). This process of transferring summaries or individual transactions to the ledger is called  ''posting''. Once the posting process is complete, accounts kept using the "T" format undergo ''balancing'', which is simply a process to arrive at the balance of the account.

As a partial check that the posting process was done correctly, a working document called an ''unadjusted trial balance'' is created. In its simplest form, this is a three-column list. Column One contains the names of those accounts in the [[ledger]] which have a non-zero balance. If an account has a ''debit'' balance, the balance amount is copied into Column Two (the ''debit column''); if an account has a ''credit'' balance, the amount is copied into Column Three (the ''credit column''). The debit column is then totalled, and then the credit column is totalled. The two totals must agree—which is not by chance—because under the double-entry rules, whenever there is a posting, the debits of the posting equal the credits of the posting. If the two totals do not agree, an error has been made, either in the journals or during the posting process. The error must be located and rectified, and the totals of the debit column and the credit column recalculated to check for agreement before any further processing can take place.

Once the accounts balance, the accountant makes a number of adjustments and changes the balance amounts of some of the accounts. These adjustments must still obey the double-entry rule: for example, the ''[[inventory]]'' account and asset account might be changed to bring them into line with the actual numbers counted during a stocktake. At the same time, the ''expense'' account associated with usage of inventory is adjusted by an equal and opposite amount. Other adjustments such as posting [[depreciation]] and prepayments are also done at this time. This results in a listing called the ''adjusted trial balance''. It is the accounts in this list, and their corresponding debit or credit balances, that are used to prepare the financial statements.

Finally [[financial statement]]s are drawn from the trial balance, which may include:
* the [[income statement]], also known as the ''statement of financial results'', ''profit and loss account'', or ''P&L''
* the [[balance sheet]], also known as the ''statement of financial position''
* the [[cash flow statement]]
* the [[Statement of changes in equity]], also known as the ''statement of total recognised gains and losses''

== Entry systems ==
Two common bookkeeping systems used by businesses and other organizations are the [[single-entry bookkeeping system]] and the [[double-entry bookkeeping system]]. Single-entry bookkeeping uses only income and expense [[Account (accountancy)|account]]s, recorded primarily in a revenue and expense journal. Single-entry bookkeeping is adequate for many small businesses. In the double-entry accounting system, at least two accounting entries are required to record each financial transaction. These entries may occur in asset, liability, equity, expense, or revenue accounts.

===Single-entry system===
{{main article|single-entry bookkeeping system}}
<!-- NOTE TO EDITORS: Please remember that this is a summary of the topic covered in the article on single-entry bookkeeping. -->

The primary bookkeeping record in single-entry bookkeeping is the ''cash book'', which is similar to a checking account (UK: cheque account, current account) register, but allocates the income and expenses to various income and expense accounts. Separate account records are maintained for petty cash, accounts payable and receivable, and other relevant transactions such as [[inventory]] and travel expenses. These days, single-entry bookkeeping can be done with [[DIY]] bookkeeping software to speed up manual calculations.

===Double-entry system===
{{main article|double-entry bookkeeping system}}
<!-- NOTE TO EDITORS: Please remember that this is a summary of the topic covered in the article on double-entry bookkeeping. -->

A ''double-entry bookkeeping system'' is a set of rules for recording financial information in a [[financial accounting]] system in which every transaction or event changes at least two different nominal [[ledger]] accounts.

==Daybooks==
A ''daybook'' is a descriptive and chronological (diary-like) record of day-to-day [[financial transactions]] also called a ''book of original entry''. The daybook's details must be entered formally into journals to enable posting to ledgers. Daybooks include:

*Sales daybook, for recording all the sales invoices.
*Sales credits daybook, for recording all the sales credit notes.
*Purchases daybook, for recording all the purchase invoices.
*Purchases Debits daybook, for recording all the purchase Debit notes.
*Cash daybook, usually known as the cash book, for recording all money received as well as money paid out. It may be split into two daybooks: receipts daybook for money received in, and payments daybook for money paid out.
*General Journal daybook, for recording journals.

==Petty cash book==
A ''[[petty cash]]'' book is a record of small-value purchases before they are later transferred to the ledger and final accounts; it is maintained by a petty or junior cashier. This type of cash book usually uses the [[imprest system]]: a certain amount of money is provided to the petty cashier by the senior cashier. This money is to cater for minor expenditures (hospitality, minor stationery, casual postage, and so on) and is reimbursed periodically on satisfactory explanation of how it was spent.

==Journals==
''[[Journal entry|Journals]]'' are recorded in the general journal daybook. A journal is a formal and chronological record of [[financial transactions]] before their values are accounted for in the general ledger as [[debits and credits]]. A company can maintain one journal for all transactions, or keep several journals based on similar activity (e.g., sales, cash receipts, revenue, etc.), making transactions easier to summarize and reference later. For every [[debits and credits|debit]] journal entry recorded, there must be an equivalent [[debits and credits|credit]] journal entry to maintain a balanced accounting equation.<ref>{{cite book | last = Haber | first = Jeffry | title = Accounting Demystified | publisher = AMACOM | location = New York | year = 2004 | isbn = 0-8144-0790-0 | page = 15}}</ref>

==Ledgers==
A ''[[ledger]]'' is a record of [[Account (accountancy)|accounts]].The ledger is a permanent summary of all amounts entered in supporting Journals which list individual transactions by date. These accounts are recorded separately, showing their beginning/ending [[Balance (accounting)|balance]]. A journal lists [[financial transactions]] in chronological order, without showing their balance but showing how much is going to be charged in each account. A ledger takes each financial transaction from the journal and records it into the corresponding account for every transaction listed. The ledger also sums up the total of every account, which is transferred into the [[balance sheet]] and the [[income statement]]. There are three different kinds of ledgers that deal with book-keeping:
*Sales ledger, which deals mostly with the accounts receivable account. This ledger consists of the records of the financial transactions made by customers to the business.
*Purchase ledger is the record of the purchasing transactions a company does; it goes hand in hand with the Accounts Payable account.

==Abbreviations used in bookkeeping==
* A/C – Account
* Acc – Account
* A/R – Accounts receivable
* A/P – Accounts payable
* B/S – Balance sheet
* c/d – Carried down
* b/d – Brought down
* c/f – Carried forward
* b/f – Brought forward
* Dr – Debit side of a ledger. "Dr" stands for "'''D'''ebit '''r'''egister"
* Cr – Credit side of a ledger. "Cr" stands for "'''C'''redit '''r'''egister"
* G/L – General ledger; (or N/L – nominal ledger)
* PL – Profit and loss; (or I/S – income statement)
* P/R - Payroll
* PP&E – Property, plant and equipment
* TB – Trial Balance
* GST – [[Goods and Services Tax (disambiguation)|Goods and services tax]]
* VAT – [[Value added tax]]
* CST – Central sale tax
* TDS – Tax deducted at source
* AMT – Alternate minimum tax
* EBITDA – Earnings before interest, taxes, depreciation and amortisation
* EBDTA – Earnings before depreciation, taxes and amortisation
* EBT – Earnings before tax
* EAT – Earnings after tax
* PAT – Profit after tax
* PBT – Profit before tax
* Depr – Depreciation
* Dep – Depreciation
* CPO – Cash paid out
* CP - Cash Payment
*

==Chart of accounts==
A [[chart of accounts]] is a list of the [[Account (accountancy)|accounts]] codes that can be identified with numeric, alphabetical, or alphanumeric codes allowing the account to be located in the general ledger. The equity section of the chart of accounts is based on the fact that the legal structure of the entity is of a particular legal type. Possibilities include ''sole trader'', ''partnership'', ''trust'', and ''company''.<ref>Marsden,Stephen (2008). Australian Master Bookkeepers Guide. Sydney: CCH ISBN 978-1-921593-57-4</ref>

==Computerized bookkeeping==
Computerized bookkeeping removes many of the paper "books" that are used to record the financial transactions of an entity—instead, relational databases take their place, but they still typically enforce the [[double-entry bookkeeping system]] and methodology.

==See also==
* [[Accounting]]

{{wikiquote}}

==References==
{{Reflist|30em}}

{{Authority control}}

[[Category:Accounting systems]]
[[Category:Accounting]]