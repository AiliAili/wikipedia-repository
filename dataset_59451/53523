{{Multiple issues|
{{lead too long|date=December 2016}}
{{more footnotes|date=December 2016}}
}}

{{ForensicScience|criminal|image=Activity-based Costing.jpg}}
{{Accounting}}
'''Forensic accounting''', '''forensic accountancy''' or '''financial forensics''' is the specialty practice area of [[accounting]] that describes engagements that result from actual or anticipated disputes or [[litigation]]. "The work performed and reports issued will often provide answers to the how, where, what, why and who. However, ultimately the court decides." (David Malamed, Forensic Accountant, Toronto Ontario.) "[[Forensic science|Forensic]]" means "suitable for use in a court of law", and it is to that standard and potential outcome that forensic accountants generally have to work. Forensic accountants, also referred to as forensic auditors or investigative auditors, often have to give expert evidence at the eventual trial.<ref>{{cite book
  | last1 = Crumbley
  | first1 = D. Larry
  | last2 = Heitger
  | first2 = Lester E.
  | last3 = Smith
  | first3 = G. Stevenson
  | title = Forensic and Investigative Accounting
  | publisher = [[CCH Group]]
  | date = 2005-08-05
  | isbn = 0-8080-1365-3}}</ref> All of the larger accounting firms, as well as many medium-sized and boutique firms and various police and government agencies have specialist forensic accounting departments. Within these groups, there may be further sub-specializations: some forensic accountants may, for example, just specialize in [[insurance]] [[claim (legal)|claims]], [[personal injury]] claims, [[fraud]], [[Money Laundering|anti-money-laundering]], [[construction]],<ref>{{cite book
|last=Cicchella
|first=Denise
|title=Construction audit guide: overview, monitoring, and auditing
|year= 2005
|publisher=IIA Research Foundation|location=Altamonte Springs, FL|isbn=0-89413-587-2}}</ref> or [[Royalties|royalty]] [[audit]]s.<ref>{{cite book
|last1=Parr
|first1=Russell L.
|last2=Smith
|first2=Gordon V.
|title=Intellectual property: valuation, exploitation, and infringement damages.
|year=2010
|publisher=Wiley
|location=Hoboken, N.J.
|isbn=0-470-45703-1
|pages=Chapter 33}}</ref>

Financial forensic engagements may fall into several categories.  For example:
* [[Damages|Economic damages]] calculations, whether suffered through [[tort]] or [[breach of contract]];
* Post-acquisition disputes such as [[earnout]]s or breaches of [[warranty|warranties]];
* [[Bankruptcy]], [[insolvency]], and [[Corporate action|reorganization]];
* [[Securities fraud]];
* [[Tax fraud]];
* [[Money laundering]];
* [[Business valuation]]; and
* [[Computer forensics]]/[[Electronic discovery|e-discovery]].
Forensic accountants often assist in professional [[negligence]] claims where they are assessing and commenting on the work of other professionals.  Forensic accountants are also engaged in [[marital and family law]] of analyzing lifestyle for spousal support purposes, determining income available for child support and equitable distribution.

Engagements relating to criminal matters typically arise in the aftermath of fraud. They frequently involve the assessment of accounting systems and accounts presentation—in essence assessing if the numbers reflect reality.

Some forensic accountants specialize in forensic analytics which is the procurement and analysis of electronic data to reconstruct, detect, or otherwise support a claim of financial fraud.  The main steps in forensic analytics are (a) data collection, (b) data preparation, (c) data analysis, and (d) reporting.  For example, forensic analytics may be used to review an employee's purchasing card activity to assess whether any of the purchases were diverted or divertible for personal use.<ref>{{cite web
| last = Nigrini
| first = Mark
| title = Forensic Analytics: Methods and Techniques for Forensic Accounting Investigations
| publisher = John Wiley & Sons Inc.
| location = Hoboken, NJ
| ISBN = 978-0-470-89046-2
| date = June 2011
| url = http://www.wiley.com/WileyCDA/WileyTitle/productCd-0470890460.html
}}</ref>

==Forensic accountants==
{{refimprove section|date=December 2016}}
{{main|Forensic accountant}}
Forensic accountants, investigative accountants or expert accountants may be involved in recovering proceeds of crime and in relation to confiscation proceedings concerning actual or assumed proceeds of crime or [[money laundering]]. In the [[United Kingdom]], relevant legislation is contained in the [[Proceeds of Crime Act 2002]]. Forensic accountants typically hold the following qualifications; [[Certified Forensic Accounting Professional]] [Certified Forensic Auditors] (CFA - England & Wales) granted by the [http://www.facb.org.uk Forensic Auditors Certification Board of England and Wales] (FACB), [[Certified Fraud Examiner]]s (CFE - US / International),[http://www.icai.org/post.html?post_id=3797 Certificate Course on Forensic Accounting and Fraud Detection (FAFD)] by [http://www.icai.org/ Institute of Chartered Accountants of India (ICAI)], [[Certified Public Accountant]]s (CPA - US) with [[AICPA]]'s [Certified in Financial Forensics est. 2008] (CFF) Credentials, [[Chartered Accountant]]s (CA - Canada), [[Certified Management Accountant]]s (CMA - Canada), [[Chartered Professional Accountant]]s (CPA - Canada)), [[Association of Chartered Certified Accountants|Chartered Certified Accountant]]s (CCA - UK), or [http://www.iicfip.org Certified Forensic Investigation Professionals] (CFIP). In India there is a separate breed of forensic accountants called Certified Forensic Accounting Professionals.<ref>{{cite web|url=http://www.business-standard.com/article/management/forensic-auditor-courses-catching-up-in-india-114042301428_1.html|title=Forensic Auditor Courses catching up in India |publisher=business-standard.com |accessdate=2014-05-18}}</ref>

The '''Certified Forensic Accountant (CRFAC)''' program from the '''American Board of Forensic Accounting''' assesses Certified Public Accountants (CPAs) knowledge and competence in professional forensic accounting services in a multitude of areas. Forensic accountants may be involved in both litigation support (providing assistance on a given case, primarily related to the calculation or estimation of economic damages and related issues) and investigative accounting (looking into illegal activities). The American Board of Forensic Accounting was established in 1993. For more information please visit http://abfa.us/ For effective learning, professionals will need expert training in the practices of forensic accounting. The American Board of Forensic Accounting offers the "Forensic Accounting Review". http://abfa.us/far/ The CRFAC is a valuable credential and covers the broad base of forensic accounting knowledge. http://abfa.us/resources/ The American Board of Forensic Accounting offers many programs. http://abfa.us/programscourses/

In 2016, the Forensic Auditors Certification Board (FACB) of England and Wales was established by the major forensic auditing and accounting bodies from across the world with its registered address in London. FACB is a professional bodies membership body comprising the International Institute of Certified Forensic Accountants (IICFA) of USA, Institute of Forensic Auditors of Zimbabwe (IFA), Institute of Forensic Accountants of Pakistan (IFAP), Institute of Certified Forensic Accountants (ICFA) of USA and Canada and the Institute of Forensic Accountants of Nigeria (IFA). FACB plays several roles and one the roles is standardization of the examination and certification of forensic auditors globally. Forensic auditors and accountants sit for one examination that is set by FACB and upon passing and meeting all the professional requirements, are awarded the credential, Certified Forensic Auditor (CFA) or the Registered Forensic Auditor (RFA) for practitioners who intend to go into public practice. All certification is renewed on an annual basis. Apart from practitioners certification, FACB is an oversight body which accredits prospective member organization before admission as part of quality checks. Persons with the FACB credential can practice as forensic auditors on a global scale.

Large accounting firms often have a forensic accounting department.<ref>{{cite web|url=http://www.big4accountingfirms.org/forensic-accounting-firms/|title=Service of Forensic Accountants |publisher=big4accountingfirms.org |accessdate=2014-04-08}}</ref>

Forensic accounting and fraud investigation methodologies are different than internal auditing. Thus forensic accounting services and practice should be handled by forensic accounting experts, not by internal auditing experts. Forensic accountants may appear on the crime scene a little later than fraud auditors, but their major contribution is in translating complex financial transactions and numerical data into terms that ordinary laypersons can understand. That is necessary because if the fraud comes to trial, the jury will be made up of ordinary laypersons. On the other hand, internal auditors move on checklists that may not surface the evidence that the jury or regulatory bodies look for. The fieldwork may carry out legal risks if internal auditing checklists are employed instead asking to a forensic accountant and may result serious consultant malpractice risks.

Forensic accountants utilize an understanding of [[Economics|economic theories]], [[business information]], [[Financial statements|financial reporting]] systems, accounting and auditing standards and procedures, [[data management]] & [[electronic discovery]], [[data analysis techniques for fraud detection]], [[evidence]] gathering and investigative techniques, and litigation processes and procedures to perform their work. Forensic accountants are also increasingly playing more proactive risk reduction roles by designing and performing extended procedures as part of the statutory audit, acting as advisers to audit committees, [[fraud deterrence]] engagements, and assisting in investment analyst research.

==See also==
* [[Benford's law]]

==References==
{{reflist}}

==External links==
*[http://www.iicfip.org International Institute of Forensic Investigation Professionals Inc]
*[http://www.acfe.com Association of Certified Fraud Examiners]
*[http://www.aicpa.org/InterestAreas/ForensicAndValuation/Membership/Pages/Overview%20Certified%20in%20Financial%20Forensics%20Credential.aspx Certified in Financial Forensics]
*[http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2210016 Forensic Accountants, Forensic Accounting Certifications, and Due Diligence]

[[Category:Types of accounting]]
[[Category:Auditing]]
[[Category:Lawsuits]]
[[Category:Forensic disciplines|Accounting]]
[[Category:Accounting terminology]]