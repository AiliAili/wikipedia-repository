<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout and guidelines. -->
{|{{Infobox aircraft begin
 |name= FGFA / PMF
 |image= File:Sukhoi T-50 Beltyukov.jpg
 |caption= A Russian T-50, on which the FGFA is based.
 |alt= <!-- Needs a word description here. -->
}}{{Infobox aircraft type
 |type= Stealth [[Multirole combat aircraft|Multirole]]/[[air superiority fighter]]
 |national origin= [[Russia]] / [[India]]
 |manufacturer= [[Hindustan Aeronautics Limited]]
 |design group= [[Sukhoi]]
 |first flight= 
 |introduction= 
 |retired= 
 |status= Under development<ref>{{cite web |url= http://www.thehindu.com/news/international/fifth-generation-fighter-crosses-milestone/article4602836.ece |title= Fifth generation fighter crosses milestone |author= Vladimir Radyuhin |work= The Hindu |accessdate= 6 April 2015}}</ref>
 |primary user= [[Indian Air Force]]
 |more users= 
 |produced= 
 |number built= 
 |program cost= [[United States dollar|US$]]30 billion (projected)<ref name=$30B/>
 |unit cost= US$100 million (est.)<ref name="Sify.com">{{cite web |url= http://sify.com/news/india-to-jointly-develop-250-fifth-generation-fighters-news-national-kkerudjjcjc.html |title= India to jointly develop 250 fifth generation fighters |publisher= Sify |date= 2010-10-04 |accessdate= 2010-12-16}}</ref><ref>{{cite web |url= http://www.business-standard.com/article/economy-policy/delays-and-challenges-for-indo-russian-fighter-112051502009_1.html |title= Delays and challenges for Indo-Russian fighter |author= Ajai Shukla |date= 15 May 2012 |work= business-standard.com |accessdate= 6 April 2015}}</ref>
 |developed from= [[Sukhoi PAK FA]]
 |variants with their own articles= 
}}
|}

The '''Sukhoi/HAL Fifth Generation Fighter Aircraft''' ('''FGFA''') or '''Perspective Multi-role Fighter''' ('''PMF''') is a [[Fifth-generation jet fighter|fifth-generation]] [[fighter aircraft|fighter]] being developed by [[India]] and [[Russia]]. It is a derivative project of the Russian [[Sukhoi PAK FA|PAK FA]] (T-50 is the prototype) being developed for the [[Russian Air Force]]. FGFA was the earlier designation for the Indian version, while the combined project is now called the Perspective Multi-Role Fighter (PMF).<ref>Menon, Jay. [http://www.aviationweek.com/Article.aspx?id=/article-xml/asd_08_21_2012_p01-01-487983.xml "India And Russia To Ink R&D Phase Of T-50 Program."] ''Aerospace Daily & Defense Report'', 21 August 2012.</ref>

The completed FGFA will include a total of 43 improvements over the T-50, including stealth, [[supercruise]], advanced sensors, networking and combat avionics.<ref name="ainonline.com">Mathews, Neelam. [http://www.ainonline.com/aviation-news/ain-defense-perspective/2012-05-25/indias-version-sukhoi-t-50-delayed-two-years "India's Version of Sukhoi T-50 Delayed by Two Years."] ''AIN'', 25 May 2012.</ref><ref>{{cite web |url= http://www.defenceaviation.com/2008/10/sukhoihal-fgfa-a-indian-stealth-fighter.html |title= Sukhoi/HAL FGFA an Indian Stealth Fighter |date= 2008-10-27 |work= Defence Aviation |accessdate= 2008-10-28}}</ref> Two separate prototypes will be developed, one by Russia and a separate one by India. The Indian version will be a two-seater for pilot and co-pilot/[[Weapon systems officer|weapon systems operator]] (WSO).

==Development== 
Following the success of the [[BrahMos]] project, [[Russia]] and [[India]] agreed in early 2007 to jointly study and develop a Fifth Generation Fighter Aircraft (FGFA) programme.<ref>{{cite news |url= http://www.hindu.com/2007/02/08/stories/2007020806230500.htm |title= Indo-Russian agreement soon on PAK-FA |first= Rasheed |last= Kappan |date= 2007-02-08 |publisher= [[The Hindu]] |location= Chennai, India |accessdate= 2007-10-20}}</ref><ref>{{cite news |url= http://timesofindia.indiatimes.com/articleshow/1433785.cms |title= India, Russia to make 5th generation fighter jets |date= 2007-01-24 |publisher= [[Times of India]] |accessdate= 2007-10-20}}</ref> On 27 October 2007 Sukhoi's director Mikhail Pogosyan stated: "We will share the funding, engineering and [[intellectual property]] in a 50–50 proportion", in an interview with ''[[Asia Times Online|Asia Times]]''.<ref>{{cite web |url= http://www.atimes.com/atimes/South_Asia/IJ27Df01.html |title= India, Russia still brothers in arms |work= Asia Times |date= 27 October 2007 |accessdate= 2010-12-16}}</ref>

On 11 September 2010, it was reported that India and Russia had agreed on a preliminary design contract, subject to Cabinet approval.  The joint development deal would have each country invest $6&nbsp;billion and take 8–10&nbsp;years to develop the FGFA fighter.<ref>[http://www.business-standard.com/india/news/india-russia-to-ink-gen-5-fighter-pact/407746/ "India, Russia to Ink gen-5 fighter pact."] ''Business standard.'' Retrieved: 19 November 2012.</ref>  In December 2010, a memorandum of understanding for preliminary design of the Indo-Russian fighter was reportedly signed between Hindustan Aeronautics Ltd (HAL), and Russian companies [[Rosoboronexport]] and Sukhoi.<ref>{{cite web |url= http://www.orfonline.org/cms/sites/orfonline/modules/analysis/AnalysisDetail.html?cmaid=20815&mmacmaid=20816 |title= Medvedev Flair Firms Up Indo-Russian Relations |publisher= Observer Research Foundation |date= 2010-12-29 |accessdate= 2012-05-18}}</ref><ref>[http://en.rian.ru/mlitary_news/20101027/161108468.html "Russia, India to begin design of 5G-fighter in December."] ''[[RIA Novosti]]'', 27 October 2010. Retrieved: 19 November 2012.</ref>  The preliminary design will cost $295 million and will be complete within 18 months.<ref>[http://en.rian.ru/world/20101216/161800857.html "Russia and India fix T-50 fighter design contract cost at $295 mln."] ''[[RIA Novosti]]'', 16 December 2010.</ref>  On 17 August 2011, media reports stated that the new fighter will cost Russia and India $6 billion to develop, and India will pay about 35% of the cost.<ref>[http://en.rian.ru/military_news/20110816/165821999.htmel "New stealth fighter jet 'principal' for Russia, India"]. RIA Novosti, 16 August 2011.</ref><ref name=CNN_MAKS2011_T50>[http://news.blogs.cnn.com/2011/08/17/new-fighter-jet-to-bolster-russian-air-force/ "New fighter jet to bolster Russian air force."] ''CNN news blog.'' Retrieved: 19 November 2012.</ref>

The Indian version, according to the deal, will be different from the Russian version and specific to Indian requirements.<ref>{{cite news |url= http://articles.timesofindia.indiatimes.com/2007-10-30/europe/27951348_1_generation-fighter-maiden-flight-indo |title= Indo-Russian 5th generation fighter to take-off by 2012 |publisher= The Times of India |date= 30 Oct 2007 |accessdate= 2010-12-16}}</ref> While the Russian version will be a single-pilot fighter, the Indian variant will be based on its own operational doctrine which calls for greater radius of combat operations. The wings and control surfaces need to be reworked for the FGFA.<ref>{{cite web |first= Sandeep |last= Unnithan |url= http://indiatoday.digitaltoday.in/index.php?option=com_content&issueid=73&task=view&id=16398&sectionid=4&Itemid=1 |title= India, Russia to have different versions of same fighter plane |publisher= Indiatoday.digitaltoday.in |date= 2008-09-29 |accessdate= 2010-12-16}}</ref> Although, development work has yet to begin,{{update inline|date=August 2016}} the Russian side has expressed optimism that a test article will be ready for its maiden flight by 2009,{{update inline|date=August 2016}} one year after PAK FA scheduled maiden flight and induction into service by 2015.<ref>{{cite news |author= PTI, Oct 30, 2007 |url= http://articles.timesofindia.indiatimes.com/2007-10-30/europe/27951348_1_generation-fighter-maiden-flight-indo |title= 5Th Generation Fighter Project (based on PAK-FA) |publisher= The Times of India |date= 2007-10-30 |accessdate= 2010-12-16}}</ref>{{update inline|date=August 2016}}  By February 2009, as per Sukhoi General Director Mikhail Pogosyan, India will initially get the same PAK FA fighter of Russia and the only difference will be the software.<ref>{{cite web |url= http://www.aviationweek.com/Blogs.aspx?plckBlogId=%20Blog:27ec4a53-dcc8-42d0-bd3a-01329aef79a7&plckPostId=Blog:27ec4a53-dcc8-42d0-bd3a-01329aef79a7Post:a30f423d-9647-4277-b1b1-f400edddc114 |title= PAK-FA - Full Speed Ahead |last1= Sweetman |first1= Bill |date= 13 February 2009 |website= aviationweek.com |publisher= Penton |accessdate= 13 November 2013}}</ref>

In 2010, a total of 500 aircraft were planned with options for further aircraft. [[Russian Air Force]] will have 200 single-seat and 50 twin-seat PAK FAs while [[Indian Air Force]] will get 166 single seated and 48 twin-seated FGFAs.<ref name=India_develop_25_percent>{{cite web |url= http://www.business-standard.com/india/news/india-to-develop-25fifth-generation-fighter/381786/ |title= India to develop 25% of fifth generation fighter |publisher= Business-standard.com |date= 2010-01-06 |accessdate= 2010-12-16}}</ref><ref>{{cite web |url= http://www.business-standard.com/india/news/india-russia-close-to-pactnext-generation-fighter/381718/ |title= India, Russia close to PACT on next generation fighter |publisher= Business-standard.com |date= |accessdate= 2010-12-16}}</ref> At this stage, the Sukhoi holding is expected to carry out 80% of the work involved. Under the project terms, single-seat fighters will be assembled in Russia, while Hindustan Aeronautics will assemble two-seaters.<ref>{{cite web |url= http://indrus.in/articles/2011/11/11/prime_minister_putins_greatest_deals_13234.html |title= Prime Minister Putin's greatest deals |publisher= indrus.in |date= 2011-11-11 |accessdate= 2011-11-11}}</ref>  HAL negotiated a 25% share of design and development work in the FGFA programme. HAL's work share will include critical software including the mission computer, navigation systems, most of the cockpit displays, the counter measure dispensing (CMD) systems and modifying Sukhoi's prototype into fighter as per the requirement of the Indian Air Force (IAF).<ref>{{cite web |url= http://www.business-standard.com/india/news/india-to-develop-25fifth-generation-fighter/381786/ |title= India to develop 25% of fifth generation fighter |publisher= Business-standard.com |date= 2010-01-06 |accessdate= 2012-05-18}}</ref>

Sukhoi director Mikhail Pogosyan projected a market for 1,000 aircraft over the next four decades, 200 each for Russia and India and 600 for other countries in 2010.<ref>{{cite news |last= Bryanski |first= Gleb |url= http://in.reuters.com/article/topNews/idINIndia-46877820100312?sp=true |title= Russia to make 1,000 stealth jets, eyes India deal |publisher= In.reuters.com |date= 2010-03-12 |accessdate= 2010-12-16}}</ref>  Russian Trade Minister Viktor Khristenko said that the aircraft are to be jointly developed and produced with India and both countries will "share benefits from selling the plane not only on their domestic markets, but also on the markets of third countries."<ref>{{cite web |url= http://www.itar-tass.com/eng/level2.html?NewsID=14912167&PageNum=0 |title= Russia, India to sign contract for sketching 5th generation jet soon |publisher= Itar-tass.com |date= |accessdate= 2010-12-16}}</ref>  The Editor-in-chief of Natsionalnaya Oborona, Igor Korotchenko, said in February 2013 that exports of the jointly designed fighter should help Russia increase its share of arms exports to the world.<ref>{{Cite news |newspaper= RUVR |url= http://english.ruvr.ru/2013_02_18/Russian-aircraft-builders-successful-arms-traders/ |date= 2013-02-18 |title= Russian aircraft builders successful arms traders |place= [[Russia|RU]]}}</ref>

In 2011, it was reported that IAF would induct 148 single-seat as well as 66 twin-seat variants of the FGFA. IAF plans to induct the first lot of aircraft by 2017.<ref>{{cite web |url= http://articles.timesofindia.indiatimes.com/2011-10-03/india/30238152_1_k-browne-aerial-wargames-iaf |title= IAF to induct 214 variants of fighter aircraft |work= The Times of India |accessdate= 6 April 2015}}</ref> By 2012, this had been changed to 214 single seat aircraft.<ref>Mathews, Neelam. [http://www.ainonline.com/aviation-news/ain-defense-perspective/2012-09-21/indian-air-force-chief-outlines-fighter-jet-plans "Indian Air Force Chief Outlines Fighter Jet Plans."] ''AIN Online'', 21 September 2012.</ref>

===Project changes and delays===
In May 2012, the Indian Ministry of Defence (MoD) announced a two-year delay in the project's development. The then Defence Minister A K Antony had said that the FGFA would join the Indian Air Force by 2017. However, his deputy, M M Pallam Raju, told the Parliament that the fifth generation aircraft is scheduled to be certified by 2019, following which the series production will start.<ref name=Delays_challenges>{{cite web |url= http://business-standard.com/india/news/delayschallenges-for-indo-russian-fighter/474329/ |title= Delays and challenges for Indo-Russian fighter |publisher= Business-standard.com |date= 2012-05-15 |accessdate= 2012-05-18}}</ref> Ashok Nayak, who spoke on the record as HAL's chairman before retiring, explained that the IAF have required 40-45 improvements made from the PAK-FA to meet Indian needs. These changes were then formally agreed upon between India and Russia.<ref name=Delays_challenges/>

There is apprehension that the FGFA would significantly exceed its current $6 billion budget,{{update inline|date=August 2016}} because this figure reflects the expenditure on just the basic aircraft. Crucial avionics systems would cost extra. The Russian and Indian air forces each planned to purchase about 250 FGFAs, at an estimated $100 million per fighter for an $25 billion total, in addition to the development costs.<ref name=Delays_challenges/>  By October 2012, India had cut its total purchase size from 200 to 144 aircraft.  India's initial investment had grown from $5 billion to $6 billion, and the estimated total programme cost had grown to $30 billion in 2012.<ref name=$30B>Luthra, Gulshan. [http://www.indiastrategic.in/topstories1766_IAF_decides_144_fifth_generation_fighters.htm "IAF decides on 144 Fifth Generation Fighters."] ''India Strategic'', October 2012.</ref>{{update inline|date=August 2016}}

In 2013, it was revealed that the Russian and Indian fighters would be using the same avionics.<ref>{{cite web |url= http://en.ria.ru/military_news/20130206/179261409.html |title= India to Use Russian Avionics For Future Fighter - UAC Boss |author= Sputnik |date= 6 February 2013 |work= ria.ru |accessdate= 6 April 2015}}</ref> Alexander Fomin said that "Both sides involved in this project are investing a lot into it, and on equal terms."<ref>{{cite web |url= http://www.ainonline.com/aviation-news/ain-news-live-aeroindia/2013-02-07/russia-still-has-high-hopes-defense-sales-india |title= Russia Still Has High Hopes for Defense Sales to India |work= Aviation International News |accessdate= 6 April 2015}}</ref> Russia later admitted to huge delays and cost overruns in the project.<ref>{{cite web |url= http://timesofindia.indiatimes.com/india/5th-generation-fighter-plan-hits-hurdle-as-Russia-hikes-cost/articleshow/21075582.cms |title= 5th-generation fighter plan hits hurdle as Russia hikes cost |work= The Times of India |accessdate= 6 April 2015}}</ref> The first prototype delivery has been delayed by one or two years.{{update inline|date=August 2016}} The contract has not be finalised, and the IAF has accused HAL of giving away up to half of India's share of the development work.<ref>{{cite web |url= http://thediplomat.com/flashpoints-blog/2013/07/19/russia-delays-indias-5th-gen-fighter-program/ |title= Russia Delays India’s 5th-Gen. Fighter Program |author= J. Michael Cole |work= The Diplomat |accessdate= 6 April 2015}}</ref><ref>{{cite web|url=http://timesofindia.indiatimes.com/india/IAF-HAL-battle-threatens-to-shatter-military-aviation-indigenization/articleshow/21492297.cms|title=IAF-HAL tiff threatens to shatter indigenization quest|work=The Times of India}}</ref>  India contributes 15% of the research and development work, but provides half the cost.<ref>{{cite web |url= http://www.aviationweek.com/Article.aspx?id=/article-xml/asd_10_21_2013_p01-01-628343.xml |title= India Concerned About Fifth-Gen Fighter Work Share With Russia |last1= Menon |first1= Jay |date= 21 October 2013 |website= aviationweek.com |publisher= Aerospace Daily & Defense Report |accessdate= 23 October 2013}}</ref>

India has "raised questions about maintenance issues, the engine, stealth features, weapon carriage system, safety and reliability".<ref>{{cite news |url= http://www.dailymail.co.uk/indiahome/indianews/article-2738681/India-Russia-jet-deal-hits-turbulence-technical-worries.html |title= India-Russia jet deal hits turbulence over 'technical worries' |last1= Datt |first1= Gautam |date= 30 August 2014 |website= dailymail.co.uk |publisher= Associated Newspapers Ltd |accessdate= 30 August 2014}}</ref>  After repeated delays in the fighter's design and workshare arrangements, the Indian Defence Minister [[Manohar Parrikar]] said in January 2015, "We have decided to fast-track many of the issues."<ref name="auto">{{cite news |url= http://www.ndtv.com/article/india/india-fast-tracks-5th-generation-fighter-jet-project-with-russia-652005 |title= India Fast Tracks 5th Generation Fighter Jet Project With Russia |work= NDTV |date= 22 January 2015 |accessdate= 23 January 2015}}</ref>  The HAL is to receive three Russian prototypes, one per year from 2015 to 2017 for evaluation.<ref>{{cite web |url= http://www.business-standard.com/article/specials/delay-on-rafale-deal-to-give-fgfa-shot-in-the-arm-115022600876_1.html |title= Delay on Rafale deal to give FGFA shot in the arm |author= Praveen Bose |date= 26 February 2015 |work= business-standard.com |accessdate= 6 April 2015}}</ref>

On 9 March 2015, media outlets reported that the countries agreed to reduce the aircraft delivery time from 92 months to 36 months with the signing of the final agreement. India is also ready to forego a 50:50 work share to prevent further delays from absorption of a new technology; both countries agreed to manufacture the first batch of aircraft in Russia and for subsequent batches to be manufactured by HAL.<ref>{{cite web |url= http://timesofindia.indiatimes.com/india/Rafale-deadlock-gives-thrust-to-Russian-5th-gen-jet-project/articleshow/46497247.cms |title= Rafale deadlock gives thrust to Russian 5th-gen jet project |work= The Times of India |accessdate= 6 April 2015}}</ref><ref>{{cite web |url= http://www.defenseworld.net/news/12361/India_Eyes_Russian_FGFA_As_MMRCA_Deal_Delays#.VP3E62P0bQI |title= India Eyes Russian FGFA As MMRCA Deal Delays |work= defenseworld.net |accessdate= 6 April 2015}}</ref><ref>{{cite web |url= http://tass.ru/en/russia/781741 |title= TASS: Russia - India to invest $25 billion in fifth-generation fighter joint project with Russia — media |work= TASS |accessdate= 6 April 2015}}</ref>

By 2016, Indian interest in the project was fading after Russia cut back their own purchases.<ref>{{cite news |url=http://thediplomat.com/2016/01/india-and-russia-fail-to-resolve-dispute-over-fifth-generation-fighter-jet/ |title=India and Russia Fail to Resolve Dispute Over Fifth Generation Fighter Jet |last1=Gady |first1=Franz-Stefan |date=6 January 2016 |website=thediplomat.com |publisher=The Diplomat |accessdate=6 January 2016}}</ref>  On 25 January 2016, it was reported that Russia and India have agreed to develop FGFA and lower investment cost to $4 billion for each nation. They will invest $1 billion in the first year and another $500 million per year for the following six years.<ref>{{cite news |url=https://www.rt.com/business/330026-india-russia-fighter-aircraft/ |title=Moscow and Delhi to invest $8 billion in 5th generation fighters |date=25 January 2016 |publisher=RT |accessdate=30 January 2016}}</ref>

In September 2016 the two nations announced a detailed work-share agreement for joint production.<ref>{{cite news |url=http://economictimes.indiatimes.com/news/defence/india-and-russia-agree-on-details-of-new-joint-production-of-fifth-generation-fighter-aircraft/articleshow/54203573.cms |title=India and Russia agree on details of new joint production of fifth generation fighter aircraft |last1=Pubby |first1=Manu |date=9 September 2016 |website=indiatimes.com |publisher=Times Group |accessdate=13 September 2016}}</ref>

==Design==
[[File:PAK FA AESA maks2009.jpg|thumb|[[Radar]] with APAR for the PAK FA/FGFA is provided by [[Tikhomirov Scientific Research Institute of Instrument Design|NIIP]]]]
[[File:AESA L NIIP maks2009.jpg|thumb|[[active electronically scanned array|APAR]] in the [[leading edge slats]]]]
[[File:MAKS Airshow 2013 (Ramenskoye Airport, Russia) (523-43).jpg|thumb|Optical detection pod for the PAK FA/FGFA]]

Although there is no reliable information about the [[Sukhoi PAK FA|PAK FA]] and FGFA specifications yet, it is known from interviews with people in the Russian Air Force that it will be [[stealth aircraft|stealthy]], have the ability to [[supercruise]], be outfitted with the next generation of [[Air-to-air missile|air-to-air]], [[air-to-surface missile|air-to-surface]], and [[anti-ship missile|air-to-ship missile]]s, and incorporate an AESA ([[active electronically scanned array]]) radar. The PAK FA/FGFA will use on its first flights 2 Saturn 117 engines (about 147.1&nbsp;kN thrust each). The 117 is an advanced version of the AL-31F, but built with the experience gained in the AL-41F programme. The AL-41F powered the Mikoyan MFI fighter ([[Mikoyan Project 1.44]]). Later versions of the PAK FA will use a completely new engine (107&nbsp;kN thrust each, 176&nbsp;kN in full afterburner), developed by NPO Saturn or FGUP MMPP Salyut.

Three Russian companies will compete to provide the engines with the final version to be delivered in 2015–2016.<ref>{{cite web |url= http://www.brahmand.com/news/Russian-firms-competing-for-FGFA-engine-development-contract/3742/3/11.html |title= Russian firms competing for FGFA engine development contract |publisher= Brahmand.com |date= 2010-04-27 |accessdate= 2010-12-16}}</ref>

Russian expertise in titanium structures will be complemented by India's experience in composites like in the fuselage.<ref name=India_develop_25_percent/> HAL is to be contributing largely to composites, cockpits and avionics according to company statements made in September 2008. HAL is working to enter into a joint development mechanism with Russia for the evolution of the FGFA engine as an upward derivative of the AL-37.{{citation needed |date= January 2015}} Speaking to ''[[Flight International]]'', United Aircraft chief Mikhail Pogosyan said India is giving engineering inputs covering latest airframe design, Hi-Tech software development and other systems.<ref>{{cite web |url= http://www.flightglobal.com/articles/2011/06/20/357476/paris-russias-pak-fa-fighter-shows-promise.html |title= PARIS: Russia's PAK-FA fighter shows promise |publisher= Flightglobal.com |date= 2011-06-20 |accessdate= 2013-04-11}}</ref>

By August 2014, the United Aircraft Corporation (UAC) had completed the front end engineering design for the FGFA for which a contract had been signed with India's [[Hindustan Aeronautics Limited|HAL]] in 2010. Preparation of contract for full-scale development is in progress.<ref>{{cite web |url= http://www.samachar.com/Russia-prepares-5thgeneration-fighter-for-India-oiqch2bbcfe.html |title= Indian News - India Newspaper - India Latest News - News From India - India News Daily - Current India News |work= samachar.com |accessdate= 6 April 2015}}</ref>

===Differences for FGFA===
The FGFA will be predominantly armed with weapons of Indian origin such as the [[Astra (missile)|Astra]], a [[beyond-visual-range missile]] (BVR) being developed by India. Although in keeping with the Russian BVR doctrine of using a variety of different missiles for versatility and unpredictability to countermeasures, the aircraft is expected to have compatibility with various missile types. The FGFA may include systems developed by third parties.<ref>{{cite web |url= http://en.rian.ru/mlitary_news/20100302/158065429.html |title= Russia, India to develop joint 5G-fighter by 2016 |publisher= RIA Novosti |date= 2010-03-02 |accessdate= 2010-12-16}}</ref>

The completed joint Indian/Russian versions of the operational fighters will differ from the current flying prototypes through the addition of stealth, supercruise, sensors, networking, and combat avionics for a total of 43 improvements.<ref name="ainonline.com"/>

Russia agreed to the demand of the [[Indian Air force]] that it must be a two-seater fighter.<ref>{{cite news |url= http://archive.defensenews.com/article/20140915/DEFREG/309150023/Indo-Russian-Jet-Program-Finally-Moves-Forward |title= Indo-Russian Jet Program Finally Moves Forward |work= Defense News |date= 15 September 2014 |accessdate= 23 January 2015}}</ref> The Indian version will be a two-seater that will, "accommodate one pilot and a co-pilot who will function as a [[Weapon systems officer|weapon systems operator]] (WSO)."<ref name="auto"/>

==Specifications (PAK FA and FGFA - projected)==
Most of these figures are for the Sukhoi T-50 prototype and not the finished HAL FGFA.

{{aircraft specifications
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]].
     Please answer the following questions: -->
 |plane or copter= plane
 |jet or prop?= jet
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses).
If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully-formatted line beginning with an asterisk "*"
-->
 |ref= ''Aviation News'',<ref name="Aviation News July 2012 p48-52">Butowski 2012, p. 48-52.</ref> ''[[Aviation Week]]'',<ref>{{cite web |url= http://www.aviationweek.com/Article.aspx?id=/article-xml/AW_08_19_2013_p43-605528.xml |title= Sukhoi T-50 Shows Flight-Control Innovations |work= aviationweek.com |accessdate= 6 April 2015}}</ref> ''Air International''<ref>Butowski, Piotr. "Raptorski's Maiden Flight". ''[[Air International]]'',  Vol. 78, No 3, March 2010, pp.&nbsp;30–37. Stamford, UK: Key Publishing.</ref>
<!--
        General characteristics
-->
 |crew= 2<ref name="auto"/>
 |payload main= 
 |payload alt= 
 |length main= 19.8 m
 |length alt= 65.0 ft
 |span main= 13.95 m
 |span alt= 45.8 ft
 |height main= 4.74 m
 |height alt= 15.6 ft
 |area main= 78.8 m<sup>2</sup>
 |area alt= 848.1 ft<sup>2</sup>
 |empty weight main= 18,000 kg
 |empty weight alt= 39,680 lb
 |loaded weight main= 25,000 kg<!-- this should be the typical mission weight. note that this isn't at full fuel -->
 |loaded weight alt= 55,115 lb
 |loaded weight more= typical mission weight, 29,270 kg (64,530 lb) at full load <!-- empty weight + full fuel weight + 4 x R-77-1 (approx) + 2 x R-73 (approx)-->
 |max takeoff weight main= 35,000 kg
 |max takeoff weight alt= 77,160 lb
 |more general= '''Fuel capacity:''' {{convert|10300|kg|lb|abbr=on}}<ref name="warfare">[http://warfare.be/?lang= &linkid=2280&catid=255 "PAK-FA Sukhoi T-50."]{{dead link|date= November 2013}} ''warfare.ru.'' Retrieved: 26 January 2011.</ref>
<!--
        Powerplant
-->
 |engine (jet)= [[NPO Saturn]] [[Saturn AL-31#117|''izdeliye'' 117 (AL-41F1)]] for initial production, ''izdeliye'' 30 for later production<ref name="Air International October 2013 p79">Butowski 2013, p. 81.</ref>
 |type of jet= thrust vectoring [[turbofan]]
 |number of jets= 2
 |thrust main= 93.1 kN / 110 kN
 |thrust alt= 21,000 lbf / 24,300 lbf
 |afterburning thrust main= 147 kN / 176 kN
 |afterburning thrust alt= 33,067 lbf / 39,600 lbf
<!--
        Performance
-->
 |max speed main=<br>
**'''At altitude:''' [[Mach number|Mach]] 2.3 (2,440 km/h, 1,520 mph)
**'''[[Supercruise]]:''' Mach 1.6 (1,700 km/h, 1,060 mph)
 |max speed alt= 
 |cruise speed main= 
 |cruise speed alt= 
 |range main= 3,500 km
 |range alt= 2,175 mi
 |range more= subsonic<br>
**1,500 km (930 mi) supersonic<ref name= "Air International October 2013 p79"/>
 |ferry range main= 5,500 km
 |ferry range alt= 3,420 mi
 |ferry range more= with one in-flight refueling<ref name=Globalsecurity_specs>[http://www.globalsecurity.org/military/world/russia/pak-fa-specs.htm "T-50 / Project 701 / PAK FA specifications."] ''GlobalSecurity.org.'' Retrieved: 18 January 2013.</ref>
 |combat radius main= 
 |combat radius alt= 
 |ceiling main= 20,000 m
 |ceiling alt= 65,000 ft
 |climb rate main= 
 |climb rate alt= 
 |loading main= 317–444 kg/m<sup>2</sup>
 |loading alt= 65–91 lb/ft<sup>2</sup>
 |thrust/weight= <br>
**Saturn 117: 1.02 (1.19 at typical mission weight)
**''izdeliye'' 30: 1.23 (1.41 at typical mission weight) <!-- two times the thrust of the engines divided by fully loaded weight and typical loaded weight -->
 |more performance= *'''Maximum ''g''-load:''' +9.0 g<ref name="ruaviation.com">{{cite web |url= http://www.ruaviation.com/news/2013/7/9/1799/ |title= The pilots of T-50 fighters received new anti-G equipment - News - Russian Aviation |publisher= Ruaviation.com |date= |accessdate= 2013-11-16}}</ref>
<!-- Armament: -->
 |guns= 1 × 30 mm internal cannon
 |hardpoints= 6 internal, 6 on wings
 |missiles= 
 |rockets= 
 |bombs= 
 |avionics= 
*Sh121 multi-functional integrated radio electronic system (MIRES)
**N079 AESA radar<ref>Butowski, Piotr. "T-50 Turning and Burning over Moscow". ''[[Air International]]'',  Vol. 85, No 4, October 2013, pp.&nbsp;80. Stamford, UK: Key Publishing.</ref>
**L402 Himalayas ECM suite built by KNIRTI institute
*101KS Atoll electro-optical suite<ref name="Aviation News July 2012 p50">Butowski 2012, p. 50.</ref>
**101KS-O: Laser-based counter-measures against infrared missiles
**101KS-V: IRST for airborne targets
**101KS-U: Ultraviolet warning sensors
**101KS-N: Targeting pod
}}

==See also==
{{Portal|India|Military of India|Russia}}
{{aircontent
|see also=<!-- other closely related articles that have not already linked: -->
|related=<!-- designs which were developed into or from this aircraft: -->
* [[Sukhoi PAK FA]]
|similar aircraft=<!-- aircraft that are of similar Role, Era, and Capability as this design: -->
* [[Chengdu J-20]]
* [[Lockheed F-22]]
|lists=<!-- relevant lists that this aircraft appears in: -->
* [[List of fighter aircraft]]
* [[List of megaprojects#Aerospace projects|List of megaprojects, Aerospace]]
}}

==References==
{{reflist|group=N}}

{{reflist|30em}}

==External links==
'''News reports and articles:'''
* [http://www.air-attack.com/news/news_article/2332 Air-attack.com news]
* [http://articles.timesofindia.indiatimes.com/2007-10-19/india/27988899_1_sukhoi-design-bureau-fifth-generation-stealth-fighter-high-endurance-air-defence-capabilities India, Russia to develop fifth-generation stealth fighter]
* [http://livefist.blogspot.com/2008/09/hals-baweja-two-different-prototypes-of.html HAL's Baweja: Two different prototypes of 5th Gen fighter, etc]

{{HAL aircraft}}
{{Sukhoi aircraft}}

{{DEFAULTSORT:Sukhoi HAL FGFA}}
[[Category:Sukhoi aircraft]]
[[Category:Stealth aircraft]]
[[Category:HAL aircraft]]
[[Category:Proposed aircraft of India]]
[[Category:India–Russia relations]]