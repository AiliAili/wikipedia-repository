{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Count Robert of Paris
| orig title   =
| translator   =
| image        = Tales Volume 4.jpg
| image_size = 180px
| author       = Sir [[Walter Scott]]
| cover_artist = 
| country      = Scotland
| language     = English, [[Scots language|Lowland Scots]]
| series       = [[Tales of My Landlord]] (4th series)
| genre        = [[Historical novel]]
| publisher    = 
| release_date = 1832
| media_type   = Print ([[Hardcover|Hardback]] & [[paperback]])
| pages        = 
| preceded_by  = 
| followed_by  = 
}}

'''''Count Robert of Paris''''' (1832) was the second-last novel by [[Walter Scott]]. It is part of ''[[Tales of My Landlord]], 4th series''.

==Plot introduction==
Set in [[Constantinople]] at the time of the [[First Crusade]], ''Count Robert of Paris'' portrays the impact of  Western medieval values and attitudes on the sophisticated Romano-Greek classical society of the [[Byzantine Empire]]. The two main characters are Count Robert, a Frankish knight, and Hereward, an Anglo-Saxon refugee from the [[Norman conquest of England]], serving as a mercenary soldier in the [[Varangian Guard]] of the Emperor [[Alexios I Komnenos]]. Count Robert was an actual but minor historical figure who disrupted negotiations between the Crusader leaders and the Emperor by occupying the latter's throne when it was temporarily vacated.

==Plot summary==
[[File:Byzantiumforecrusades.jpg|thumb|right|The Byzantine Empire in 1090]]

''This section of the article includes text from the revised 1898 edition of Henry Grey's A Key to the Waverley Novels (1880), now in the public domain.''

At the end of the 11th century, the Byzantine capital of [[Constantinople]] was threatened by [[Seljuk Turks|Turkic nomads]] from the east, and by the [[Franks#Legacy|Franks]] from the west. Unable to rely on his Greek subjects to repel their incursions, the emperor was obliged to maintain a body-guard of [[Varangians]], or mercenaries from other nations, of whom the citizens and native soldiers were very jealous. One of these, the Anglo-Saxon Hereward, had just been attacked by Sebastes, when a Varangian officer, Tatius, intervened and led him to the palace. Here he was introduced to the imperial family, surrounded by their attendants; and the Princess [[Anna Komnene|Anna]] was reading a roll of history she had written, when her husband [[Nikephoros Bryennios the Younger|Brennius]] entered to announce the approach of the armies composing the first Crusade. Convinced that he was powerless to prevent their advance, the emperor offered them hospitality on their way; and, the leaders having agreed to acknowledge his sovereignty, the various hosts marched in procession before his assembled army.

As [[Alexios I Komnenos|Emperor Comnenus]], however, moved forward to receive the homage of [[Bohemond I of Antioch|Count Bohemond]], his vacant throne was insolently occupied by Count Robert of Paris, who was with difficulty compelled to vacate it, and make his submission. The defiant knight, accompanied by his wife Brenhilda, afterwards met the sage Agelastes, who related the story of an enchanted princess, and decoyed them to his hermitage overlooking the Bosphorus. Here they were introduced to the empress and her daughter, who, attended by Brennius, came to visit the sage, and were invited to return with them to the palace to be presented to the emperor. At the State banquet which followed, the guests, including Sir Bohemond, were pledged by their royal host, and urged to accept the golden cups they had used. On waking next morning, Count Robert found himself in a dungeon with a tiger, and that Ursel was confined in an adjoining one. Presently an aggressive [[orangutan]] descended through a trap-door, soon followed by the armed Sebastes. Both were overpowered by the Count, when Hereward made his appearance, and undertook to release his Norman adversary.

[[Image:Pammakaristos Church interior02.jpg|thumb|right|Interior of [[Pammakaristos Church]], Constantinople, built shortly before the First Crusade]]
A treasonable conference was meanwhile taking place between Tatius and Agelastes, who had failed in endeavouring to tamper with the Anglo-Saxon; and the countess had been unwillingly transported by the slave Diogenes to a garden-house for a secret interview with Brennius, whom she challenged to knightly combat in the hearing of her husband. Having hidden the count, Hereward encountered his sweetheart Bertha, who had followed Brenhilda as her attendant, and then obtained an audience of the imperial family, who were discussing recent events, including a plot in which Brennius was concerned for seizing the throne, and received permission to communicate with the Duke de Bouillon. Bertha volunteered to be his messenger, and, at an interview with the council of Crusaders at [[Üsküdar|Scutari]], she induced them to promise that fifty knights, each with ten followers, should attend the combat to support their champion.

Having made his confession to the Patriarch, while Agelastes was killed by the orangutan as he argued with Brenhilda respecting the existence of the devil, the emperor led his daughter to the cell in which Ursel was confined, with the intention of making him her husband, instead of Brennius. She had, however, been persuaded by her mother to intercede for the traitor, and Ursel was merely placed under the care of the slave doctor Douban to be restored to health after his long imprisonment. The emperor had decided that Brennius should fight the Count of Paris, instead of the countess, and all the preparations for the combat had been made, when the ships conveying the Crusaders hove in sight; and, after defeating the Greek fleet, they landed in sight of the lists. Brennius, in the meantime, was pardoned, and, in answer to shouts of discontent from the assembled crowd, Ursel was led forth to announce his restoration to liberty and the imperial favour, and the conspiracy was crushed. Hereward then appeared to do battle with Count Robert, and, saved from the knight's axe by Bertha, he joined the Crusaders, obtaining on his return the hand of his betrothed, and, ultimately, a grant of land from William Rufus, adjacent to the New Forest in Hampshire, where he had screened her when a girl from the tusk of a wild boar.

==Characters==
[[Image:Alexius I.jpg|thumb|right|Alexius I Comnenus]]
* '''[[Alexius I Comnenus]]''', Greek Emperor of Constantinople
* The Empress [[Irene Doukaina]], his wife
* Princess [[Anna Komnene|Anna]], their daughter
* [[Nikephoros Bryennios the Younger|Nicephorus '''Brennius''']], her husband.
* Astarte and Violante, her attendants
* Achilles '''Tatius''', officer of the Imperial Varangian Guard
* '''Hereward''', an Anglo-Saxon, his subaltern
* Stephanos Castor, a wrestler
* Lysimachus, a designer
* Harpax, centurion of the city guard
* Sebastes, a recruit in the "Immortals" - a Greek corps of the Imperial army
* Nicanor, commander-in-chief of the Greek army
* Zosimus, Greek patriarch
* Michael '''Agelastes''', an old sage
* ''Leaders of the First Crusade''
** [[Godfrey de Bouillon]]
** [[Peter the Hermit]]
** Count Baldwin (future [[Baldwin I of Jerusalem]])
** [[Hugh I of Vermandois|Count de Vermandois]]
** [[Bohemond I of Antioch]]
** Prince Tancred of Otranto (future [[Tancred, Prince of Galilee]])
** [[Raymond IV, Count of Toulouse]]
** '''Count Robert of Paris'''
* '''Brenhilda''', Countess of Paris.
* Toxartis, a Scythian chieftain.
* Agatha, afterwards '''Bertha''', Hereward's betrothed
* Diogenes, a Negro slave
* Zedekias Ursel, a rival for the throne
* Douban, a slave skilled in medicine
* Sylvan, an ourang-outang
{{Waverley Grey}}

{{Walter Scott}}

{{DEFAULTSORT:Count Robert Of Paris}}
[[Category:1832 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by Walter Scott]]
[[Category:Historical novels]]
[[Category:Novels set in the Byzantine Empire]]
[[Category:Novels set in the Crusades]]
[[Category:Novels set in Istanbul]]
[[Category:1090s in fiction]]
[[Category:Novels set in the 11th century]]