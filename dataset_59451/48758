{{Infobox organization
|name         = International Fertilizer Development Center
|image        = IFDC Logo.jpg
|size         = 
|alt          = Logo as distributed by IFDC's press kit
|map          = 
|msize        = 
|mcaption     = 
|abbreviation = IFDC
|motto        = 
|formation    = {{start date and age|1974|10}}
|founder      = 
|type         = Public International Organization
|status       = 
|purpose      = Research
|headquarters = [[Muscle Shoals, Alabama]], [[United States|USA]]
|location     = 
|region_served = Worldwide
|membership   = 
|language     =
|leader_title =  President and CEO
|leader_name  = Dr. J. Scott Angle
|affiliations = 
|num_staff    = 
|num_volunteers =
|budget       = 
|website      = {{URL|http://www.ifdc.org/}}
}}
The '''International Fertilizer Development Center''' (known as IFDC) is a science-based public international organization working to alleviate global hunger by introducing improved agricultural practices and fertilizer technologies to farmers and by linking farmers to markets. Headquartered in [[Muscle Shoals, Alabama]], [[United States|USA]], the organization has projects in over 25 countries.

== History ==
IFDC was established, in part, because by 1975, the [[Tennessee Valley Authority|Tennessee Valley Authority's]] National Fertilizer Development Center (NFDC) began receiving an amount of international assistance calls that exceeded the capabilities of the Center's staff to fulfill both international demand and domestic programs. A year earlier at the Sixth Special Session of the [[United Nations General Assembly]], [[Henry Kissinger|U.S. Secretary of State Henry Kissinger]] in his speech "The Challenge of Interdependence" urged the creation of an international fertilizer institute and promised U.S. contribution through facilities, technology and expertise.<ref>http://connection.ebscohost.com/c/speeches/9887758/challenge-interdependence "The Challenge of Interdependence" Henry Kissinger</ref> The result of Kissinger's urgency became the International Fertilizer Development Center, a non-profit organization incorporated under the state laws of Alabama, which began its service by answering the international calls once fielded to the NFDC.<ref>{{cite book |last=Nelson |first=Lewis B. |year=1990 |title=History of the U.S. Fertilizer Industry |location=Muscle Shoals, Alabama, USA |publisher= Tennessee Valley Authority|isbn=0870770047}}</ref><ref>http://www.apnewsarchive.com/1999/Organization-Helps-Fertilize-Fields/id-f322e3a47708dec72b145e1290680aec?SearchText=ifdc;Display_ "Organization Helps Fertilize Fields" AP</ref>
In March 1977, [[Jimmy Carter|U.S. President Jimmy Carter]] designated IFDC a public international organization "entitled to enjoy the privileges, exemptions, and immunities conferred by the International Organizations Immunities Act."<ref>http://www.presidency.ucsb.edu/ws/?pid=7176 "Executive Order 11977" The American Presidency Project</ref>

==Funding==
IFDC receives funding from various bilateral and multilateral development agencies, private enterprises, foundations and an assortment of other organizations. Additionally, long-term revenue is given to the Center through long-term, donor-funded market development projects involving the transfer of policy and technology improvements to emerging economies.

==President and CEO  {{as of|2016|3|df=us}}==
J. Scott Angle worked for 24 years as a professor and administrator at the University of Maryland. His research focused on macro and micronutrients and cycling within the agroecosystem. He also helped develop the concept of phytoremediation for clean-up of contaminated soil. From 2005 to 2015, Angle served as the dean of the College of Agricultural and Environmental Sciences at the University of Georgia. Currently, he is President and CEO of the International Fertilizer Development Center, where he oversees a staff of more than 800 employees and coordinates development projects around the world. He has been a Fulbright Scholar and is a fellow of the ASA and SSSA.  

==Board of Directors {{as of|2015|2|df=us}} ==
*[[Jimmy Cheek|Dr. Jimmy Cheek]], Chairman of the Board, United States
*Rudy Rabbinge, Co-Vice Chairperson, The Netherlands
*Rhoda Peace Tumusiime, Co-Vice Chairperson, Uganda
*[[M. Peter McPherson]], Chairman Emeritus, United States
*[[Margaret Catley-Carlson]], Canada
*Dr. Josué Dioné, Mali
*Douglas Horswill, Canada
*Dr. Agnes M. Kalibata, Rwanda
*[[Mark E. Keenum]], USA
*[[Steven Leath|Dr. Steven Leath]], United States
*William P. O’Neill, Jr., USA
*[[Võ Tòng Xuân|Dr. Võ Tòng Xuân]], Vietnam
*Dr. J. Scott Angle, Ex-Officio Member, IFDC President and CEO, United States
*Patrick J. Murphy, United States, Ex-Officio Member


==Divisions==

===East and Southern Africa Division===
Active Countries: [[Burundi]], [[Democratic Republic of the Congo]], [[Ethiopia]], [[Kenya]], [[Mozambique]], [[Rwanda]], [[South Sudan]], [[Tanzania]], [[Uganda]], [[Zambia]]

The East and South Africa Division (ESAFD) of IFDC handles areas where previous farming techniques are no longer adequate for the growing population they serve. ESAFD works to improve farmers’ access to quality seeds and fertilizers as well as markets to cover their costs. The effort also educates farmers in Integrated Soil Fertility Management (ISFM) to improve soil conditions.

===North and West Africa Division===
Active Countries: [[Benin]], [[Burkina Faso]], [[Cape Verde]], [[Chad]], [[Côte d’Ivoire]], [[Gambia]], [[Ghana]], [[Guinea]], [[Guinea Bissau]], [[Liberia]], [[Mali]], [[Niger]], [[Nigeria]], [[Senegal]], [[Sierra Leone]], [[Togo]]

The North and West Africa Division (NWAFD) of IFDC covers an area of Africa of about 520 million people, more than half of whom are directly affected by its programs. These programs include demonstrations fields where farmers receive hands-on training and experience with new and specialized fertilizer, seed, crop protection and irrigation research. Through the use of voucher programs called “smart subsidies,” farmers can receive quality supplies in a timely manner and be supported at harvest time.

===EurAsia Division===
Active Countries: [[Bangladesh]], [[Myanmar]]

The EurAsia Division (EAD) of IFDC focuses on countries with little land suitable for farming where farmers’ yields steadily decrease over time due to crop quality and quantity. EAD hopes to reverse these issues by addressing specific financial, market, and agricultural needs. The division teaches farmers about Fertilizer Deep Placement (FDP), a method which has previously raised crop yields by 20 percent and decreased nitrogen losses by 40 percent.

===Office of Programs===
The Office of Programs conducts research and development projects dedicated to improving fertilizer efficiency. It offers consultation to national governments as well as private sector organizations with regard to critical domains such as supply/demand and policy issues.

==Nations Currently Served by IFDC {{as of|2015|2|df=us}}==
{{columns-list|3| 
*[[Bangladesh]]
*[[Benin]]
*[[Burkina Faso]]
*[[Burundi]]
*[[Chad]]
*[[Democratic Republic of the Congo]]
*[[Ethiopia]]
*[[Ghana]]
*[[Kenya]]
*[[Liberia]]
*[[Mali]]
*[[Mozambique]]
*[[Myanmar|Burma]]
*[[Niger]]
*[[Nigeria]]
*[[Rwanda]]
*[[Senegal]]
*[[South Sudan]]
*[[Tanzania]]
*[[Uganda]]
*[[Zambia]]}}

==Nations Previously Served by IFDC==
{{columns-list|3| 
*[[Afghanistan]]
*[[Albania]]
*[[Algeria]]
*[[Angola]]
*[[Argentina]]
*[[Australia]]
*[[Azerbaijan]]
*[[Belgium]]
*[[Bolivia]]
*[[Brazil]]
*[[Burma]]
*[[Cambodia]]
*[[Cameroon]]
*[[Canada]]
*[[Cape Verde]]
*[[Chile]]
*[[Colombia]]
*[[Costa Rica]]
*[[Côte d’Ivoire|Ivory Coast]]
*[[Dominican Republic]]
*[[Ecuador]]
*[[Egypt]]
*[[Gambia]]
*[[Greece]]
*[[Guatemala]]
*[[Guinea]]
*[[Guinea-Bissau]]
*[[Guyana]]
*[[Haiti]]
*[[Honduras]]
*[[Hungary]]
*[[India]]
*[[Indonesia]]
*[[Israel]]
*[[Jamaica]]
*[[Jordan]]
*[[Kazakhstan]]
*[[Kosovo]]
*[[Kyrgyzstan]]
*[[Libya]]
*[[Madagascar]]
*[[Malawi]]
*[[Malaysia]]
*[[Mexico]]
*[[Morocco]]
*[[Nepal]]
*[[New Zealand]]
*[[Pakistan]]
*[[Paraguay]]
*[[People's Republic of China|China]]
*[[Peru]]
*[[Philippines]]
*[[Romania]]
*[[Saudi Arabia]]
*[[Sierra Leone]]
*[[Singapore]]
*[[South Africa]]
*[[South Korea]]
*[[Spain]]
*[[Sri Lanka]]
*[[Swaziland]]
*[[Syria]]
*[[Sudan]]
*[[Taiwan]]
*[[Tajikistan]]
*[[Thailand]]
*[[Togo]]
*[[Trinidad]]
*[[Tunisia]]
*[[Turkey]]
*[[Ukraine]]
*[[United Arab Emirates]]
*[[United States]]
*[[Uruguay]]
*[[Uzbekistan]]
*[[Venezuela]]
*[[Vietnam]]
*[[Yugoslavia]] (former)
*[[Zambia]]
*[[Zimbabwe]]}}

== Research and development ==
By 2050, 60 percent more food will need to be grown annually to keep up with a rapidly growing population.<ref>[http://www.fao.org/fileadmin/user_upload/esag/docs/AT2050_revision_summary.pdf World Agriculture Toward 2030/2050, FAO]</ref> According to Vaclav Smil, man-made nitrogen fertilizers are keeping about 40% of the world’s population alive.<ref>{{cite book |last=Smil |first=Vaclav |year=2004 |title=Enriching the Earth |location=Cambridge |publisher= MIT Press|isbn=0262693135}}</ref> IFDC conducts research to identify the most efficient use of fertilizer raw materials and develops processes to use these materials in the sustainable and cost-effective manufacture of various fertilizer products. In [[Bangladesh]], for example, IFDC introduced Urea Deep Placement (UDP) technology, an briquetted form of urea applied into the soil, which increases farmer incomes by an average of 20% and decreases nitrogen loss by up to 30%.<ref>http://aehof.eng.ua.edu/members/international-fertilizer-development-center/ Alabama Engineering Hall of Fame</ref><ref>http://www.sciencedaily.com/releases/2007/12/071218192026.htm Science Daily. "Bangladesh To Dramatically Expand Technology That Doubles Efficiency Of Urea Fertilizer Use"</ref> Applied research also includes the development of more efficient cropping technologies, decision support tools and the agronomic evaluation of these products and processes to ensure their long-term viability in a free-market environment.

===Fertilizer Deep Placement===
During the mid to late 1980s, IFDC began research in India on several [[fertilizer]] types, one being the IFDC-developed fertilizer deep placement (FDP) technology, which was shown at the time to decrease nitrogen losses by 9% on sorghum crops. In 1986, the Center introduced FDP in Bangladesh where IFDC has promoted the technology ever since. Farmers are now using the technology on 1.7 million acres in that country alone. In 2007, IFDC began a new FDP campaign, spreading the technology to [[sub-Saharan Africa]].<ref>https://green.blogs.nytimes.com/2010/03/10/increasing-yields-and-decreasing-fertilizer-waste-on-subsistence-farms/ {{webarchive|url=https://web.archive.org/web/20150910162104/http://green.blogs.nytimes.com/2010/03/10/increasing-yields-and-decreasing-fertilizer-waste-on-subsistence-farms/ |date=2015-09-10 }}</ref>

FDP involves "briquetting" nitrogen fertilizer by compacting prilled fertilizer into 1-3 gram briquettes. The briquettes (either urea- or [[NPK]]-based) are then placed in a plant's root zone, as opposed to the traditional application method of broadcasting. Trials have shown that FDP and UDP (when only urea is used) can increase crop production up to 36 percent, reduce fertilizer use by up to 38 percent, and reduce nitrogen losses by up to 40 percent.<ref>http://www.usaid.gov/bangladesh/press-releases/research-shows-improved-fertilizer-application-increases-rice-and</ref>

The technology, mainly promoted in lowland flooded rice, showed promising results in reducing nitrogen runoff, so in 2012, IFDC began research in Bangladesh to quantify [[Greenhouse gas|GHG]] emissions produced from using FDP. Through the [[USAID]]-funded Accelerating Agricultural Productivity Improvement project, which integrated the U.S. government’s Global Climate Change Initiative into its [[Feed the Future Initiative]], research is currently underway.<ref>http://www.farmchemicalsinternational.com/uncategorized/ifdc-programs-designed-to-improve-crop-yields/</ref>

===Peak Phosphorus===
Phosphorus is a key component of fertilizer, along with nitrogen and potassium. Predicting the future event of [[Peak phosphorus|peak phosphate]] in which production of phosphate rock begins to decline as resources dwindle, researchers estimated that world phosphorus supplies would be used up by 2030 if mined and processed at its present rates. Depletion of this material would be a major complication for the fertilizer production sector.

In 2010, IFDC geologist Steven Van Kauwenburgh estimated the world’s supply of phosphate rock at 60 billion metric tons<ref>http://www.producer.com/2013/08/phosphorus-not-scarce-just-use-wisely-expert/</ref> in the publication [http://ifdc.org/2010/09/22/ifdc-report-indicates-adequate-phosphorus-resources-available-to-meet-global-food-demands/ ''World Phosphate Rock Reserves and Resources'']. By his estimates, global resources of phosphate rock suitable to produce phosphate rock concentrate, phosphoric acid, phosphate fertilizers and other phosphate-based products will be available for several hundred years. His estimation overshadowed previous estimates of the U.S. Geological Survey (USGS) by 44 billion tons.<ref>http://www.reuters.com/article/2010/09/22/fertilizer-phosphate-idUSN2223379920100922</ref> Upon review and intense scrutiny of the information in the report, the USGS revised its world phosphate rock reserve and resource numbers to more closely reflect those stated in the report.<ref>http://globalpnetwork.net/news/2011-01-26/usgs-revises-world-phosphate-rock-reserve-estimates-based-ifdc-study</ref>

==Areas of Expertise==

===Capacity Building===
IFDC trains farmers to participate effectively in a global market while facing challenges in their specific local communities. This training works both with farming techniques on a hands-on agricultural level and with commercial concepts on an agribusiness level.

===Competitive Agricultural Systems and Enterprises (CASE)===
CASE consolidates local stakeholders to encourage innovation and growth while also developing a commodity value chain and involving public and private entities. IFDC developed CASE in 2004 to further promote agricultural intensification and strengthen the integration of farmers and local entrepreneurs.<ref>http://www.icra-edu.org/page.cfm?pageid=partnerifdc</ref>

===Decision Support Tools (DSTs)===
DSTs help farmers apply agricultural research based on geography and markets by using crop modeling and analyses of soil, weather and market information to increase yields and profits. IFDC has aided in the development of several tools, including the Decision Support System for Agrotechnology Transfer (DSSAT).<ref>http://dssat.net/contact-us</ref>

===Fertilizer Deep Placement (FDP)===
FDP involves "briquetting" nitrogen fertilizer by compacting prilled fertilizer into 1-3 gram briquettes. The briquettes (either urea- or [[NPK]]-based) are then placed in a plant's root zone, as opposed to the traditional application method of broadcasting. Trials have shown that FDP and UDP (when only urea is used) can increase crop production up to 36 percent, reduce fertilizer use by up to 38 percent, and reduce nitrogen losses by up to 40 percent.<ref>http://www.usaid.gov/bangladesh/press-releases/research-shows-improved-fertilizer-application-increases-rice-and</ref>

===Integrated Soil Fertility Management (ISFM)===
ISFM adapts agricultural practices to specific areas and their respective conditions to maximize agricultural efficiency and productivity.

===Market Development===
Market development efforts consist of developing output markets for farmers to sell their surplus produce, which can thus create an input market from which farmers can buy the necessary supplies such as seeds, fertilizers and crop protection products.

===Public-Private Partnerships (PPPs)===
PPPs accomplish tasks together that neither public sector institutions nor private sector organizations could accomplish individually.

==Initiatives==

===Africa Fertilizer Summit===
On June 9–13, 2006 heads of state and governments gathered in Abuja, Nigeria, for the Africa Fertilizer summit and called for the elimination of all taxes and tariffs on fertilizer in the historic [http://www.ifdc.org/About/Initiatives/Africa-Fertilizer-Summit/About-Abuja-Declaration/ “Abuja Declaration on Fertilizer for an African Green Revolution”]. Summit participants also agreed on 12 resolutions designed to increase fertilizer use five-fold in 10 years in the Abuja Declaration.<ref>http://news.ahibo.com/spip.php?article18</ref>
IFDC helped organize and to implement the Summit. Dr. Amit Roy, then president and CEO of IFDC, in a corporate report address on the Summit stated, “The obstacles to agricultural development in Africa are enormous and long-standing. Human, institutional and research capacity, as well as physical infrastructure, must be built to enable Africa to compete effectively. Policies should be changed to encourage business investment. Furthermore, as history has demonstrated, countries must take charge of their own futures if they are to build better futures for their children.”<ref>http://issuu.com/ifdcinfo/docs/afs_new</ref>
The Summit was attended by 1,100 participants including five African heads of state, 15 ministers of agriculture, 17 members of the Summit’s Eminent Persons Advisory Committee, and hundreds of leaders of international organizations, agricultural research centers and private sector companies.<ref>http://www.nepad.org/system/files/First%20Progress%20in%20Implementation%20of%20Abuja%20Declaration.pdf</ref> The Abuja Declaration was written at the conclusion of the Africa Fertilizer Summit on June 13, 2006, in Abuja, Nigeria.

===Global Transdisciplinary Processes for Sustainable Phosphorus Management (Global TraPs)===
The Global TraPs initiative brings together experts from a multitude of fields to build knowledge on how humans can make steps towards using [[phosphorus]] in a sustainable manner. The multi-stakeholder initiative is headed by Dr. Amit H. Roy, then IFDC president and CEO, and Dr. [[Roland W. Scholz]], of [[Fraunhofer Society|Fraunhofer IWKS]]. More than 200 other partners worldwide participate in the project.<ref>http://www.iwks.fraunhofer.de/en/cooperation/global-traps-.html</ref> Recently, Global TraPs published a [[Springer Science+Business Media|Springer]] book titled ''[http://www.springer.com/environment/sustainable+development/book/978-94-007-7249-6 Sustainable Phosphorus Management: A Global Transdisciplinary Roadmap]''. The book discusses the economic scarcity of phosphorus and ways to increase efficiency and reduce environmental impacts of [[anthropogenic]] phosphorus flows at every stage of production, supply and use.

===Virtual Fertilizer Research Center (VFRC)===
The VFRC is an IFDC research initiative designed to create and disseminate the “next generation” of fertilizers.<ref>http://www.prweb.com/releases/IFDC_virtual_research/fertilizer/prweb3642214.htm</ref> The initiative, through a virtual network, engages universities, public and private research laboratories and the global fertilizer and agribusiness industries in the development of new fertilizers. 
Current work, released in ''[http://vfrc.org/Research/VFRC_Reports VFRC Reports]'', focuses on biological solutions to plant and human nutrition.<ref>http://vfrc.org/Research/VFRC_Reports</ref>

== References ==
{{reflist}}

== External links ==
*{{official website|http://www.ifdc.org/}}
*[http://issuu.com/ifdcinfo/docs/ifdc_africa_fertilizer_summit_proceedings Africa Fertilizer Summit Proceedings]
*[http://www.globaltraps.ch Global TraPs Website]
*[http://www.vfrc.org VFRC Website]
{{AIRCA |state=expanded}}
[[Category:Agricultural organizations based in the United States]]
[[Category:Economic development organizations]]
[[Category:International development]]
[[Category:International non-profit organizations]]