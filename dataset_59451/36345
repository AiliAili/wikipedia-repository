{{Use mdy dates|date=May 2012}}
{{good article}}
{{Infobox station
| name = Anderson Street
| style = NJT Rail
| image = Anderson_Street_Station_April_2014.jpg
| image_size = 300px
| image_caption = Anderson Street station in April 2014, after completion of the new shelter that replaced the old 1869 station depot.
| address = Anderson Street and Linden Avenue<br>[[Hackensack, New Jersey]]
| coordinates =
| line = {{rail color box|system=NJT|line=Pascack Valley}}
| other = {{bus icon|12px|NJT Bus}} '''[[New Jersey Transit Bus Operations|NJT Bus]]''': [[List of New Jersey Transit bus routes (100-199)|175]] and [[List of New Jersey Transit bus routes (700-799)|770]]
| platform = 1 side platform
| tracks = 1 (formerly 2<ref name="yanosey" />)
| parking = 50 spaces (at Anderson Street and Linden Avenue)
| bicycle = Lockers available
| baggage_check =
| passengers = 359 (average weekday)<ref>{{cite web|url=http://media.nj.com/bergen_impact/other/1Q2013.pdf |title=QUARTERLY RIDERSHIP TRENDS ANALYSIS |publisher=New Jersey Transit |accessdate=January 4, 2013 |archiveurl=http://www.webcitation.org/6DEGzJnox?url=http%3A%2F%2Fmedia.nj.com%2Fbergen_impact%2Fother%2F1Q2013.pdf |archivedate=December 27, 2012 |deadurl=no |df=mdy }}</ref>
| pass_year = 2012
| pass_percent =
| opened = {{start date and age|1869|September|9|mf=yes|p=yes}}
| closed =
| rebuilt =
| ADA =
| code = 769 (Erie Railroad)<ref name="codes">{{cite web|url=http://www.jon-n-bevliles.net/RAILROAD/erie_docs/erie-losn16.html|title=List of Station Names and Numbers|date=May 1, 1916|publisher=[[Erie Railroad]]|location=[[Jersey City, New Jersey]]|accessdate=November 23, 2010}}</ref>
| owned = New Jersey Transit
| zone = 5
| services = {{s-rail|title=NJT}}
{{s-line|system=NJT|line=Pascack Valley|previous=New Bridge Landing|next=Essex Street|rows2=2}}
{{s-line|system=NJT|line=Pascack Valley|previous=Fairmount Avenue|next=Essex Street|note=(Closed 1983)|hide2=true}}
{{s-rail|title=Erie}}
{{s-line|system=Erie|line=njny|previous=Central Avenue|next=Fairmount Avenue}}
| nrhp =
  {{Infobox NRHP
  | embed = yes
  | name = Anderson Street Station
  | nrhp_type =
  | image = Anderson Station 1910 2.jpg
  | image_size = 300px
  | caption = Anderson Street station in 1910 looking to the northwest.
  | location = Anderson Street,<br>[[Hackensack, New Jersey]]
  | coordinates = {{coord|40|53|39|N|74|02|40|W|region:US-NJ_type:railwaystation|display=inline,title}}
| locmapin = USA New Jersey Bergen County
  | built = 1869
  | architect =
  | architecture = Carpenter Gothic
  | added = June 22, 1984
  | delisted=  May 18, 2011
  | area = {{convert|0.3|acre|ha|1}}
  | governing_body = Private
  | mpsub = {{NRHP url|id=64000496|title=Operating Passenger Railroad Stations TR}}
  | refnum = 84002520<ref name="nris">{{NRISref|version=2010a}}</ref>
  }}
}}

'''Anderson Street''' is a [[New Jersey Transit]] rail station on the [[Pascack Valley Line]]. The station is one of two rail stations in [[Hackensack, New Jersey|Hackensack]] (the other being [[Essex Street (NJT station)|Essex Street]]) and located at Anderson Street near Linden Street. All normal scheduled trains service this station seven days a week except for the [[Metro-North Railroad]] Express trains to [[Spring Valley, New York]]. 

The station house was built in 1869 (and opened on September 9, 1869) by the [[Hackensack and New York Railroad]] on a track extension from Passaic Street in Hackensack. The station was turned over to the [[Erie Railroad]] in 1896 and New Jersey Transit in 1983. The next year, the station was listed on the [[National Register of Historic Places listings in Bergen County, New Jersey|National Register of Historic Places]]. The station building, which was 139 years old, was destroyed in a three-alarm fire and explosion at 5:55&nbsp;a.m. on January 10, 2009. At the time the station house was the second-oldest (active service) in New Jersey (second to [[Ramsey (NJT station)|Ramsey's Main Street station]]). The station building was also the site for the Green Caboose Thrift Shop, a charity gift shop maintained by a branch of the [[Hackensack University Medical Center]] from 1962 until the station depot burned in 2009.

== Layout and service==
{{ja-rail-linem|pfn=1|span=2|first=2|nolinkindex=[[Pascack Valley Line]]|linecol=#{{NJT color|Pascack Valley}}|dir=toward [[Spring Valley (Metro-North station)|Spring Valley]] <small>([[New Bridge Landing (NJT station)|New Bridge Landing]])</small>}}
{{ja-rail-linem|pfn=1|first=3|nolinkindex=[[Pascack Valley Line]]|linecol=#{{NJT color|Pascack Valley}}|dir=toward [[Hoboken Terminal|Hoboken]] <small>([[Essex Street (NJT station)|Essex Street]])</small>}}
[[File:Anderson Street station.jpg|left|thumb|Anderson Street station facing Spring Valley-bound (northbound) in Hackensack]]
The Anderson Street station is located at the intersection with Anderson Street and Linden Avenue in Hackensack. The station has one track running through it, with one lone asphalt side platform appearing on the northbound side. The station has a nearby parking lot at the same intersection, with fifty parking spaces maintained by [[Park America]] (under lease from New Jersey Transit). Two of these fifty parking spaces are handicap accessible, although the station itself is not. These parking spots are permit-only, but are free to use on evenings and weekends. The station is located in New Jersey Transit's fifth fare zone, tickets may be purchased at the station. Except for the [[Metro-North Railroad]] Express routes to [[Spring Valley (Metro-North station)|Spring Valley]], all trains serve the Anderson Street station, and there is nearby access to the [[List of New Jersey Transit bus routes (100-199)|175]] and [[List of New Jersey Transit bus routes (700-799)|770]] New Jersey Transit bus lines. The station is 18 minutes from [[Secaucus Junction]], 21 minutes from [[Hoboken Terminal]], and 39 minutes from Spring Valley.
{{clear left}}

== History ==

=== Hackensack and New York Railroad ===
[[File:Anderson Street 1869.jpg|left|thumb|Anderson Street station in 1909]]
The original alignment of the Anderson Street station dates back to the chartering of the [[Hackensack and New York Railroad]] in 1856 by David P. Patterson and other investors. Their intent in creating the rail line was to help maintain a steam-powered train line in the [[Pascack Valley]] and have future ambitions to build the system northward. Construction on the new {{convert|21|mi|km|adj=on}} long line began in 1866, with trains heading from [[New York City]] to the Passaic Street station in Hackensack.<ref name="history">{{cite news|url=http://www.northjersey.com/news/78940177.html|title=Hillsdale's history 'tied' to the railroad |last=Mrnarevic|first=Karen R.|date=December 10, 2009|work=Pascack Valley Community Life|publisher=Community Life|accessdate=January 9, 2010|location=[[Hillsdale, New Jersey]]}}</ref> Although Hackensack was not a large hub, there were several rail lines serving the city, including the New Jersey Midland Line (now the [[New York, Susquehanna and Western Railroad]]) with stops at Main Street (at the Mercer Street intersection) and at Prospect Avenue.<ref name="threecenturies">{{cite book|title=Hackensack, Three Centuries of Prosperity|publisher=[[Hackensack, New Jersey]]|location=Hackensack, New Jersey|year=1993|pages=33}}</ref> During the 1860s, service was extended to north, terminating at [[Essex Street (NJT station)|Essex Street]]. Residents from the Anderson Street area donated $2,600 (1869 [[USD]]) to have a new station depot constructed along new tracks heading northward.<ref name="scudders">{{cite book|last=Scudders|first=George|title=Historic Facts About Hackensack|year=1915}}</ref> Although most Hackensack and New York trains ended at Passaic Street, service was extended northward on September 5, 1869, when that stop was abandoned in replacement for Anderson Street.<ref name="opening">{{cite news|url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9E0DE6D9123AEF34BC4153DFBF668382679FDE|title=Hackensack and New-York Railroad|date=September 9, 1869|work=The New York Times|publisher=TimeWarner|accessdate=January 9, 2010|location=New York, New York | format=PDF}}</ref> Just next year, service was extended northward on the Hackensack and New York Railroad Extension Railroad to Cherry Hill (now North Hackensack / New Bridge Landing) and onto [[Hillsdale (NJT station)|Hillsdale]].<ref name="jones">{{cite book|last=Jones|first=Wilson E.|title=The Pascack Valley Line - A History of the New Jersey and New York Railroad|publisher=Railroadians of America|location=[[East Hanover, New Jersey]]|year=1996|isbn=0-941652-14-9}}</ref>

The Anderson Street Station had a wood siding with a shingled roof, two brick chimneys off the roof and two asphalt platforms in both directions. The station also had a garage door on the southern side of the building. No official style of architecture was mentioned for Anderson Street in the 1920 Final Engineering Report due to lack of design.<ref name="yanosey" /> Nearby, a wooden watchman's shanty was constructed near the team track. The station had two tracks run through it (one main track and a team track) and had a rail crossing between tracks.<ref name="yanosey">{{cite book|last=Yanosey|first=Robert J.|title=Erie Railroad Facilities (In Color)|publisher=Morning Sun Books Inc.|location=[[Scotch Plains, New Jersey]]|year=2006|volume=1|pages=96–99|isbn=1-58248-183-0}}</ref> By 1870, the tracks had been extended northward to [[Hillsdale (NJT station)|Hillsdale]], and public service began on the line on March 4 of that year. Trains terminated at Hillsdale with fare of only $0.75 (1870 USD), but just one year later, the extension northward. The service was extended northward to the community of [[Haverstraw, New York]], and in 1896, the rail line was leased by the private company to the [[Erie Railroad]].<ref name="history" />

=== Erie Railroad station and restoration ===
After the leasing of the New Jersey and New York Railroad to the Erie Railroad, the history of Anderson Street station remained rather quiet, with minor changes to the station building and site occurring over the next sixty years. The Erie had repainted the station to a common green and white Erie Railroad paint scheme. By 1964, there were new asphalt pavement platforms on both the northbound main track and the southbound team track, crossing gates had been installed and the paint scheme was fading to a darker green. By September 1966, the [[Erie Lackawanna]] (a merge of the Erie Railroad and [[Delaware, Lackawanna and Western Railroad|Delaware, Lackawanna and Western]]) sold off the station building to become the site of the Green Caboose Thrift Shop,  and repainted a teal green color. The nearby watchman's shanty, closed on Sundays, were repainted to tan and green with a red roof. The team track was also being dismantled by this point. Later, in 1972, the station experienced minor changes, with the Green Caboose Thrift Shop remaining in service the station building being repainted by the Erie Lackawanna a dark green (with the Erie Lackawanna's red doors). The nearby watchman's shanty was not repainted, remaining the railroad's common red color and the team track had been long removed, with no remains were noticeable.<ref name="yanosey" />

In 1976, the Erie Lackawanna was combined with several other railroads to create the [[Consolidated Rail Corporation]], who continued maintenance of the New Jersey and New York Line for the next seven years, until the newly formed [[New Jersey Transit]] took over the station in 1983.<ref name="book">{{cite book|last=Jones|first=Wilson E.|title=The Pascack Valley Line: A History of the New Jersey and New York Railroad|publisher=Railroadians of America|year=1996|isbn=0-941652-14-9}}</ref> On March 17, 1984, the station building, now 114 years old, was added to the [[New Jersey Register of Historic Places]] and by that June, the station was added to the [[National Register of Historic Places]].<ref name="andersonnrhp">{{cite news|title=Anderson Street Station (Structure NRIS 84002520)|date=June 22, 1984|work=[[National Register of Historic Places]]|publisher=[[National Park Service]]|location=[[Washington D.C.]]}}</ref> The station building was restored in 2001 by contractors from Jablonski Building Conservation Incorporated in [[Midtown Manhattan]], who had experience restoring train stations.<ref name="conservation">{{cite web|url=http://www.jbconservation.com/projects.html#transit|title=Projects|year=2004|publisher=Jablonski Building Conservation Incorporated|accessdate=January 10, 2010|location=New York, New York| archiveurl= https://web.archive.org/web/20100105185448/http://www.jbconservation.com/projects.html| archivedate= January 5, 2010 <!--DASHBot-->| deadurl= no}}</ref> The building conservation repainted the old station's wooden siding yellow<ref name="image1">{{cite web|url=http://www.subwaynut.com/njt/andersonst/|title=Anderson Street 1 (Photo)|last=Cox|first=Jeremiah|year=2006|publisher=Cox, Jeremiah|accessdate=January 10, 2010|location=New York, New York}}</ref> and the bay windows to a brand new brown on the station's ground-level platform.<ref name="image2">{{cite web|url=http://www.subwaynut.com/njt/andersonst/|title=Anderson Street 2 (Photo)|last=Cox|first=Jeremiah|year=2006|publisher=Cox, Jeremiah|accessdate=January 10, 2010|location=New York, New York}}</ref>

=== Station building fire and explosion ===
[[File:Anderson Street day.jpg|left|thumb|Anderson Street station, one year after the station building caught fire]]
At approximately 5:55 a.m. on January 10, 2009, the station building for Anderson Street caught fire and ruptured two propane tanks, which caused the building to explode. Two nearby cars were damaged as well. The three-alarm fire destroyed the building, and causing damage to nearby apartment complex. Twelve fire companies were called to battle the blaze, including fire stations from [[Teaneck, New Jersey|Teaneck]], [[Ridgefield Park, New Jersey|Ridgefield Park]], [[Bogota, New Jersey|Bogota]] and [[South Hackensack, New Jersey|South Hackensack]]. Service on the Pascack Valley Line had to be stopped indefinitely until they could demolish the unsafe site of the former station building and inspect the area to allow train usage. Hackensack city manager Stephen Lo Iacono was notified of the fire and deemed it a "devastating loss for the community." At 11 a.m., city officials were digging up the area around the station to stop the gas line near the new station.<ref name="burning">{{cite web|url=http://www.northjersey.com/breakingnews/Fire_destroy_historic_train_station.html|title=UPDATE: Fire destroys historic train station |last=Gartland|first=Michael|date=January 10, 2009|work=The Record|publisher=North Jersey Media Group|accessdate=January 10, 2010|location=New Jersey}}</ref> The Green Caboose Thrift Shop, a charity gift shop run by a ladies auxiliary of the [[Hackensack University Medical Center]] which was housed in the station, received a major blow after the explosion, which destroyed all their merchandise.<ref name="burning2">{{cite news|url=http://www.nj.com/news/index.ssf/2009/01/fire_destroys_historic_hackens.html|title=Fire destroys historic Hackensack train station|last=Abdou|first=Nyier|date=January 10, 2009|publisher=The Star-Ledger|accessdate=January 10, 2010}}</ref> The Green Caboose has since moved to Orchard Street in Hackensack.<ref name="2010caboose">{{cite web|url=http://www.humcfoundation.com/site/PageServer?pagename=Auxiliary|title=The Auxiliary|year=2010|publisher=Hackensack University Medical Center|accessdate=January 11, 2010|location=Hackensack, New Jersey| archiveurl= https://web.archive.org/web/20100222083323/http://www.humcfoundation.com/site/PageServer?pagename=Auxiliary| archivedate= February 22, 2010 <!--DASHBot-->| deadurl= no}}</ref> On February 7, 2011 an application was filed to remove the destroyed structure from both the New Jersey and National Registers of Historic Places,<ref>{{cite web|last=Saunders|first=Daniel D.|title=Memorandum|url=http://www.state.nj.us/dep/hpo/1identify/nr_nominations_SRB_drafts/ber_hackensack_andersonst_20110224.pdf|work=www.state.nj.us|publisher=NJ Historic Preservation Office|accessdate=February 24, 2011}}</ref> to prevent a person from constructing a new station that would not be considered "historic".<ref name=delisting>{{cite news|title=State moves to remove Hackensack train station destroyed by fire from historic list|url=http://www.nj.com/news/index.ssf/2011/02/state_moves_to_remove_hackensa.html|accessdate=March 2, 2011|newspaper=The Star-Ledger|date=February 25, 2011}}</ref> On May 18, 2011, the station was removed from the National Register and its listing with the 51 other stations in the original 1984 package.<ref name=delisted>{{cite web|title=Weekly List for May 27, 2011|url=http://www.nps.gov/history/nr/listings/20110527.htm|work=[[National Register of Historic Places]]|publisher=[[National Park Service]]|accessdate=May 30, 2011|location=Washington D.C.|date=27 May 2011}}</ref><ref>[http://www.northjersey.com/news/151073315_Anderson_train_station_s_building_will_be_rebuilt__.html Hackensack train station's building will be rebuilt - News - NorthJersey.com<!-- Bot generated title -->]</ref>

===New train station===
In March 2013, construction began on a new station. The cost of the new station is $571,061. The new building will have a waiting room with three walls and ticket machines.<ref>{{cite web|title=NJ Transit breaks ground on new building at Anderson rail station in Hackensack|url=http://www.northjersey.com/news/200637721_NJ_Transit_breaks_ground_on_new_building_at_Anderson_rail_station_in_Hackensack.html|publisher=[[The Record (Bergen County)|The Record]]|accessdate=April 4, 2013|date=29 March 2013}}</ref>

== See also ==
{{Portal|New Jersey|Trains}}
* [[Operating Passenger Railroad Stations Thematic Resource (New Jersey)]]
* [[National Register of Historic Places listings in Bergen County, New Jersey]]
{{-}}

==References==
{{Reflist|colwidth=30em}}

==External links==
{{Commons category|Anderson Street (NJT station)}}
{{NJT links}}

{{NJT stations navbox}}
{{National Register of Historic Places}}

[[Category:1869 establishments in New Jersey]]
[[Category:Buildings and structures in Bergen County, New Jersey]]
[[Category:Former National Register of Historic Places in New Jersey]]
[[Category:Hackensack, New Jersey]]
[[Category:NJ Transit stations]]
[[Category:Railway stations opened in 1869]]
[[Category:Stations along Erie Railroad lines]]
[[Category:Demolished railway stations in the United States]]