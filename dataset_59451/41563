{{infobox military unit
|unit_name= Algerian National Navy<br>القوات البحرية الجزائرية
|image=
|caption=
|dates= 1516{{spaced ndash}}Present
|country= {{flag|Algeria|Flag of Algeria}}
|allegiance=
|branch= [[Navy]]
|type= 
|role= 
|size= 17000 personnel <ref>http://www.inss.org.il/upload/%28FILE%291311071604.pdf</ref>
|command_structure=
|garrison= '''L'AMIRAUTE''', Algiers
|garrison_label=
|nickname=
|patron= 
|motto=
|colors=
|colors_label=
|march=
|mascot=
|equipment= 80 vessels, 30 helicopters, 02 MPA Aircraft
|equipment_label=
|battles=
|anniversaries= February 2, 1967
|decorations=
|battle_honours=
|battle_honours_label=
|disbanded=
|flying_hours=
|website=
<!-- Commanders -->
|commander1=  Mohamed-Larbi Haouli
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|commander4=
|commander4_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:Naval Ensign of Algeria.svg|border|200px]]
|identification_symbol_label= Naval Ensign
|identification_symbol_2=[[File:Naval Jack of Algeria.svg|border|200px]]
|identification_symbol_3=
|identification_symbol_4=
|identification_symbol_2_label=Naval Jack
|identification_symbol_3_label=
|identification_symbol_4_label=
<!-- Aircraft -->
|aircraft_attack=
|aircraft_bomber=
|aircraft_electronic=
|aircraft_fighter=
|aircraft_helicopter=
|aircraft_helicopter_attack=
|aircraft_helicopter_cargo=
|aircraft_helicopter_multirole=
|aircraft_helicopter_observation=
|aircraft_helicopter_trainer=
|aircraft_helicopter_utility=
|aircraft_interceptor=
|aircraft_patrol=
|aircraft_recon=
|aircraft_trainer=
|aircraft_transport=
|aircraft_helicopter_transport=
}}
The '''Algerian National Navy''' ('''ANN'''; {{lang-ar|القوات البحرية الجزائرية}}) is the naval branch of the [[Military of Algeria]]. The navy operates from multiple bases along the country's nearly {{convert|1000|km|mi|abbr=on}} [[coastline]], fulfilling its primary role of monitoring and defending [[Algeria]]'s [[territorial waters]] against all foreign military or economic intrusion. Additional missions include [[coast guard]] and maritime [[safety]] missions as well a projection of marine forces (''fusillers marins''). Algerian forces are an important player in the [[Western Mediterranean]].

As with other Algerian military branches, the navy was built and structured with assistance from the [[Soviet Union]] during the [[Cold War]], but has also relied on other sources for equipment in some areas. Since the end of the Cold War, [[Russia]] has remained an important partner, but Algeria has increasingly sought additional sources for equipment as well as building its own shipbuilding capacity.

== History ==
The Algerian Navy played an important role in the western Mediterranean between the sixteenth and eighteenth centuries, where she was a leading military force that ensured not only the defense of the Regency of Algiers but also of international ships making a passage through the Mediterranean, led notably legendary sailors such as Barbarossa brothers or Kheir Edine and Hassan Agha.

'''The origins of the Algerian Navy (1147 - 1516))'''

At that time the Maghreb under the control of the Almohad dynasty which also ruled the current Spain "al-Andalus" and 1147 to 1269. The Navy was born with the installation by Abd El Moumen naval shipyards the Almohad empire in the ports of Oran and Honaine. But the reign of this great dynasty would soon be glazed at first by a few internal disagreements, mainly related to the difficulties of managing a vast territory. The situation worsened further when a part of the Iberian peninsula again passed under the control of Christian rulers in the wake of the Battle of Las Navas de Tolosa, the decomposition of the kingdom accelerated with the formation of three states in North Africa. After the total destruction of the kingdom Almohad in 1269 began a fierce battle between Muslims and Christians for control of various ports in the Western Mediterranean, this has resulted in the occupation by the Spanish in several regions algcomme The Peñon of Algiers, Oran and Bejaia, which led the indigenous peoples of these regions to call on pirates to free the Christian invaders, which was done through sending and Aroudj Hayreddin Barbarossa brothers in 1516, they managed to build a fleet.
[[File:Barbarossa_Hayreddin_Pasha.jpg|thumb|left|upright|Khayr ad-Din]]

'''The privateer Oruç Reis, eldest of Khayr ad-Din'''

Described by some historians as the greatest pirates in history, the Barbarossa brothers would probably Albanians, several hypotheses are cited in this regard but none of them have been confirmed definitively, however, the general trend that emerges from the various historical references tends to reinforce the former. In the description given to them, It is said that the youngest Kheire Edine was brighter and stronger corpulence as its big brother while Aroudj Kheir Edine was known for his knowledge in navigation and maritime battles, he was nicknamed Barbarossa . He engaged in piracy for the sole purpose of revenge crossed, especially following his imprisonment for several years in the jails of the Christian rulers, courage and great skill allowed him to escape, fleeing afterwards to Tunis where King Mohamed Ibn Hafss allowed him to build a naval base from which he managed to be the first steps of its military fleet. He moved later to head its fleet to Algeria to its liberation from the Spanish colonization. What he managed to do by releasing initially Algiers, which he conferred the status of capital of a new Algerian state, several years later, he reunites his fleet and left immediately to conquer the last bastion Spanish in Algeria Mers El Kebir, he managed to free.

'''Phase of construction and consolidation'''

After the liberation of all the regions that were under Spanish influence, it was therefore quite possible to consider the construction of the Algerian navy, which was undertaken by building initially of four small ships war, over time, a real military industry was born through several shipyards (especially in Cherchell, Bejaia and Algiers) who provided the Algerian Navy a considerable number of warships equipped with cannons developed entirely by skills Algerian. From there a new page was opened for Algeria which through its Marine could impose its leadership in the Mediterranean for nearly three centuries.

This rule also allowed him to repel several attacks from a number of European countries, starting with the one that was led by Charles V in October 1541, the troops of the latter were severely defeated by the Algerian fleet which was then under the command Hassan Agha, other attacks were carried out by the Spaniards in the 16th and 17th centuries, but they were all rejected by the Algerian Navy.

Other attacks of importance, the American expedition of 1815 and one that led the British and Dutch Marines on Algiers in August 1816, the latter suffered great losses and were prevented from landing at Algiers. However the Algerian armada also lost a large number of vessels.
[[File:Duquesne_fait_liberer_des_captifs_chretiens_apres_le_bombardement_d_Alger_en_1683.jpg|thumb|left|upright|Abraham Duquesne delivering Christian captives in Algiers after the bombing in 1683]]
[[File:Ottoman_cannon_end_of_16th_century_length_385cm_cal_178mm_weight_2910_stone_projectile_founded_8_October_1581_Alger_seized_1830.jpg|thumb|write|upright|Ornate Ottoman cannon found in Algiers on 8 October 1581 by Ca'fer el-Mu'allim. Length: 385 cm, cal:178 mm, weight: 2910 kg, stone projectile. Seized by France during the invasion of Algiers in 1830. Musée de l'Armée, Paris.]]
[[File:Sm_Bombardment_of_Algiers,_August_1816-Luny.jpg|thumb|left|upright|The bombardment of Algiers by Lord Exmouth, August 1816, painted by Thomas Luny]]

Some Algerian squadrons took part to the [[Greek war of independence]] from 1821 on, as suppletive forces to the [[Ottoman fleet]], and lost several ships in various engagements throughout the war.

In June 1827, Charles X launched a naval blockade on Algiers and three years later a military expedition which finally took the city in 1830. Algeria, endured for a century and 32 years of French rule until July 5, 1962, when the country regained its independence.

== Bases ==
Principal naval bases are located at [[Algiers]], [[Annaba]], [[Mers el-Kebir]], [[Oran]], [[Jijel]] and [[Tamentfoust]]. Mers el Kébir is home to the OMCN/CNE shipbuilding facilities where several Algerian vessels have been built. Algeria's naval academy at Tamentfoust provides officer training equivalent to that of the army and the air force academies. The navy also operates a technical training school for its personnel at Tamentfoust.

== Equipment ==

The bulk of the Algerian Navy is still based on Cold War designs, although work is being done to both acquire new platforms as well as modernize existing equipment. The surface fleet is equipped with a mixture of smaller ships well suited to coastal and [[Exclusive Economic Zone]] (EEZ) patrol work. The fleet is led by three [[Koni class frigate]]s which have been updated with more modern systems. These are due to be augmented in the coming years by a pair of [[MEKO|MEKO A-200]] frigates which will represent the most modern equipment of the navy when they enter service, also, Algeria signed a contract with China Shipbuilding Trading Company for the construction of three light frigates about 2,800 tons full load. A mixture of six corvettes and off-shore patrol vessels complement the frigates, while a large number of smaller boats cover the role of coastal patrol. Algeria had maintained a relatively large fleet of [[Osa class missile boat|Osa class]] [[fast attack craft]] by the end of the Cold War, but it is questionable whether any of these remain in operational use.

Algeria has had a small submarine presence in the Mediterranean with a pair of [[Kilo class submarine|Kilo class]] patrol submarines, though the recent acquisition of an additional two upgraded boats will expand this presence significantly. Their amphibious warfare capacity has traditionally been limited with a small group of landing ships essentially for coastal transport roles. This capacity will be greatly upgraded with the planned acquisition of an amphibious transport dock capable of supporting more robust operations. In the area of civil support, the purchase of seagoing rescue tugs will mark the first ability of an African nation to provide valuable services to economic and commercial operators in the Western Mediterranean.

The Algerian military has long maintained a strong veil of secrecy over its organization and equipment, making an exact accounting of operational vessels difficult to ascertain. Open sources are known to vary widely in their reports of several aspects of Algerian equipment.

=== Submarines ===
{| class=wikitable
|+
|-
! width="155" style="text-align:left;" |Class
! width="155" style="text-align:left;" |Photo
! width="50" style="text-align:center;" |[[Pennant number|No.]]
! width="155" style="text-align:left;" |Ship
! style="text-align:center;" |Year<br />[[Ship commissioning|Commissioned]]
! style="text-align:left;" |Note
|-
! style="background: lavender;" colspan="6" |
|-
|-
| [[Kilo-class submarine#Project 636 units|Kilo Class 636M]]
| [[Image:Kilo-class in SPb.JPG|150px|''Messli el Hadj 021'']]
| align="center" | 04
||  || align="center" |  ''2009-2019'' || <ref name="ReferenceA">http://www.navyrecognition.com/index.php?option=com_content&task=view&id=627</ref><ref name="vpk-news.ru">http://vpk-news.ru/news/2509/</ref>
<ref name="lenta.ru">http://lenta.ru/news/2012/09/19/subs/</ref><ref name="armstrade.org">http://www.armstrade.org/files/analytics/315.pdf</ref><ref name="km.ru">http://www.km.ru/economics/2012/09/19/692626-alzhir-zakazal-rosoboroneksportu-dve-podvodnye-lodki#.UFsuP5Fc3CM</ref> Two are active and two under construction , delivery starting in 2017.
|-
| [[Kilo-class submarine|Kilo Class 877EKM]]
| [[Image:Submarine-Kilo-Algeria.JPG|150px|''Rais Hadi Mubarek'']]
| align="center" | 02 || Rais Hadi Mubarek Class || align="center" | 1987 || Project 877EKM ''Paltus'' (Kilo) submarines built in [[Admiralty Shipyard]] in [[Saint Petersburg]]
modernized in 2009 , equipped with ClubS missile.
|}

=== Amphibious warfare vessels ===
{| class=wikitable
|-
! style="text-align:left;" width=155|Class
! style="text-align:left;" width=155|Photo
! style="text-align:center;" width=50|[[Pennant number|No.]]
! style="text-align:left;" width=155|Ship
! style="text-align:left;" width=100|Displacement
! style="text-align:center;"|Year<br />[[Ship commissioning|Commissioned]]
! style="text-align:left;"|Note
|-
! colspan="7" style="background: lavender;"| Amphibious transport dock (1+1option)
|-
| [[San Giorgio-class amphibious assault ship|Kalaat Béni Abbès Class]] 
|  [[File:BDSL-474.png|border|150px]]
|align=center| 1+(1) || [[Algerian amphibious transport dock Kalaat Béni Abbès|''Kalaat Béni Abbès'']] || align=center|9,000 tonnes ||align=center| 2015 || Improved ''San Giorgio'', Ordered in 2011(+1 in option)<ref>{{cite web |url=http://www.navyrecognition.com/index.php/news/defence-news/year-2014-news/january-2014-navy-naval-forces-maritime-industry-technology-security-global-news/1477-italian-shipyard-fincantieri-launched-algerian-navy-future-amphibious-ship-bdsl-program.html |title=Italian shipyard Fincantieri launched Algerian Navy future amphibious ship (BDSL program) |date=January 10, 2014}}</ref><ref>[http://www.meretmarine.com/fr/content/le-nouveau-batiment-de-projection-algerien Le nouveau bâtiment de projection algérien], meretmarine.com 13/09/2012</ref><ref>{{cite web |url=http://www.navyrecognition.com/index.php/news/defence-news/year-2014-news/september-2014-navy-naval-forces-maritime-industry-technology-security-global-news/1985-italian-shipyard-fincantieri-delivered-amphibious-ship-kalaat-beni-abbes-to-algerian-navy.html |title=Italian shipyard Fincantieri delivered amphibious ship Kalaat Beni-Abbes to Algerian Navy |date=September 7, 2014}}</ref>'
|-
! colspan="7" style="background: lavender;"| Landing Ships (2)
|-
|rowspan=2| [[Kalaat Beni Hammed class landing ship|''Kalaat Beni Hammed'']] ||rowspan=2|  ||align=center| 472 || ''Kalaat Beni Hammed'' || align=center|2,450 tonnes||align=center| 1984 || Built by [[Brooke Marine]] in [[Lowestoft]], [[United Kingdom|UK]]
|-
|align=center| 473 || ''Kalaat Beni Rached'' || align=center|2,450 tonnes ||align=center| 1984 || Built by [[Vosper Thornycroft]] in [[Woolston, Southampton|Woolston]], UK
|}

=== Surface combatants ===
{| class=wikitable
|-
! style="text-align:center;" width=155|Class
! style="text-align:center;" width=155|Photo
! style="text-align:center;" width=50|[[Pennant number|No.]]
! style="text-align:center;" width=155|Ship
! style="text-align:center;" width=100|Displacement
! style="text-align:center;" width=155|Year<br />[[Ship commissioning|Commissioned]]
! style="text-align:center;"|Note
|-
! style="background: lavender;" colspan="7" | Frigates (11)
|-
| [[MEKO 200|MEKO A200]] 
| [[File:MekoA200-Algeria.jpg|border|150px]]
|align=center|  2+(2)  ||align=center| Arradie "Deterrent" || 3,700 tonns ||align=center| ''2016'' || in Service (with an option for two more) <ref>{{cite web |url=http://www.navyrecognition.com/index.php/news/defence-news/2016/february-2016-navy-naval-forces-defense-industry-technology-maritime-security-global-news/3624-first-of-two-german-built-meko-a-200-an-frigate-commissioned-with-algerian-navy.html|title=First of Two German built MEKO A-200 AN Frigate Commissioned with Algerian Navy|date=February 29, 2016}}</ref>
|-
| [[C28A]]
| [[Image:C28A.jpg|150px]]
| align="center" |  3+(3)  || align="center" | Adhafir, El Fatih, Ezzadjer 
| 2,800 tonns || align="center" | ''2015-2017'' || in Service + 3 new version of C28A  <ref>http://www.eastpendulum.com/cssc-devoile-nouvelle-version-de-fregate-c28a</ref>
|-
|[[Koni-class frigate]]
|[[Image:MouradRais1986.jpg|150px|border|alt=901 ''Mourad Rais'']]
| align="center" | 3 || Rais Korfo 
|2000 tonns|| align="center" | 1985 || Ex-Soviet SKR-129, re-fitted in 2000 at [[Kronshtadt]], believed to have suffered an explosion 07 Dec 2015 while under overhaul at [[Kronstadt]]
|-
! colspan="7" style="background: lavender;"| Corvettes (13)
|-
| [[Steregushchy-class corvette|Project 20382 "Tigr" corvette]] <ref>[http://en.rian.ru/russia/20110630/164924783.html Russia to build two Tiger corvettes for Algerian navy | Russia | RIA Novosti<!-- Bot generated title -->]</ref><ref>http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=16804:algerian-navy-purchases-two-tiger-corvettes-from-russia&catid=51:Sea&Itemid=106</ref>
| [[File:Corvette Steregushchiy.jpg|border|150px]]
|align=center|  6<ref> http://armstrade.org/files/obrazecglava4.pdf</ref> ||  || 2200 Tonns ||align=center|''2017<ref>http://flotprom.ru/2016/%D0%A2%D0%BE%D1%80%D0%B3%D0%BE%D0%B2%D0%BB%D1%8F%D0%9E%D1%80%D1%83%D0%B6%D0%B8%D0%B5%D0%BC70/</ref>'' || Delivery starting in 2017.
|-
|[[Nanuchka-class corvette]]<ref name="autogenerated1">[http://en.rian.ru/world/20120405/172639453.html Russia to Upgrade Two Warships for Algerian Navy]</ref>
|[[Image:SalahRais.jpg|150px|border|alt=802 ''Salah Rais'']]
| align="center" | 3 || Rais Ali 
|660 tonns|| align="center" | 1982 || in service, Project 1234E built by Vympel Shipyards in [[Rybinsk]], modernized in 2012.
|-
|[[Djebel Chenoua Class corvette]]
|[[Image:Algerian corvette El Kirch in 2006.jpg|150px|border|alt=353 ''El Kirch'']]
| align="center" | 4 || El Kirch 
|540 tonns|| align="center" | 2002
|Built by OMCN / CNE in [[Mers-el-Kebir]], Algeria. 
Armed with 4[[C-802| C802]] ASM and AK630 CIWS.
|-
! style="background: lavender;" colspan="10" | Patrol Boats (43)
|-
|rowspan=1| [[Osa-class missile boat|Osa II class missile boat]]
|rowspan=1|
|align=center| 8 ||  ||||align=center|1978 ||
|-
|rowspan=1| [[Kebir class patrol boat]]
|rowspan=1|
|align=center| 14 || El Yadekh ||250||align=center| 1982 ||rowspan=1| the first two units built by [[Brooke Marine]]
|-
|rowspan=1| [[FPB98 MKI Ocea class patrol boat]] <ref name="meretmarine.com">http://www.meretmarine.com/fr/content/ocea-livre-le-dernier-des-21-patrouilleurs-algeriens</ref><ref name="globalsecurity.org">http://www.globalsecurity.org/military/world/algeria/navy-equipment.htm</ref>
|rowspan=1|
|align=center| 21 || Denebi ||||align=center| 2008-2011 || built by Ocea France 
|-
! style="background: lavender;" colspan="7" | Mine countermeasures (1)
|-
|rowspan=1| [[Lerici-class minehunter]] 
|rowspan=1| 
|align=center|  1(+2)  ||align=center| El-Kasseh 1 ||rowspan=1| 600 tonns ||align=center| ''2016'' || in Service.<ref>{{cite web |url=http://www.forcesdz.com/forum/viewtopic.php?f=20&t=537&start=105}}</ref>
|}

=== Fleet auxiliaries ===
{| class=wikitable
|-
! style="text-align:left;" width=155|Class
! style="text-align:left;" width=155|Photo
! style="text-align:center;" width=50|[[Pennant number|No.]]
! style="text-align:left;" width=155|Ship
! style="text-align:center;" width=100|Displacement
! style="text-align:center;"|Year<br />[[Ship commissioning|Commissioned]]
! style="text-align:left;"|Note
|-
! colspan="7" style="background: lavender;"| Survey ship
|-
| ''El Idrissi'' ||||align=center| 673 || El Idrissi || align=center|540 tonnes||align=center| 1980 || Ship built by [[Matsukara Zosen]] in [[Hirao]], [[Japan]]
|-
! colspan="7" style="background: lavender;"| Training ship
|-
| ''Soummam'' || [[File:ANS La Soummam-1.jpg|150px|''Soummam'']] ||align=center| 937 || Soummam || align=center|5,500 tonnes ||align=center| 2006 || 5500 tons (full load)
|-
|EL Mellah 
|
|938
|El Mellah ( the sailor)
|110 meters 
|2017
|A Three Mats  [[tall ship]] constructed In Poland Gdansk
|-
! colspan="7" style="background: lavender;"| Salvage ship
|-
| ''El Mourafik'' ||||align=center| 261 || El Mourafik || align=center|600 tonnes ||align=center| 1990 || Built in [[China]]
|-
! colspan="7" style="background: lavender;"| High Seas Tow Vessel
|-
|rowspan=3| ''El Mounjid'' ||align=center| 701 || El Mounjid || align=center|3,200 tonnes ||align=center| 2012 || The Ships are type UT 515 CD built in [[Norway]] and Motorization buy Rolls-Royce.
|-
| align=center | 702 || El Moussif || align=center|3,200 tonnes ||align=center| 2012 || 
|-
| align=center | 703 || El Moussanid || align=center|3,200 tonnes ||align=center| 2012 || 
|}

=== Aircraft ===
{| class=wikitable
|-
! style="text-align:left;"|Aircraft
! style="text-align:left;"|Mission
! style="text-align:center;"|In Service
<ref>{{Cite book|title = World Air Forces 2016|last = |first = |publisher = Flightglobal International|year = 2015|isbn = |location = |pages = 11|url = https://flightglobal.com/asset/6297/waf/}}</ref>
! style="text-align:left;"|Note{{Citation needed|date = December 2015}}
|-
! style="background: lavender;" colspan="4" | Helicopters
|-
| [[AgustaWestland AW101]] || Search and rescue ||align=center| 6 || Ordered in 2007, in service by 2011
|-
| [[AgustaWestland AW139]] || Search and rescue ||align=center| 8 || Ordered in 2010, in service by 2014
|-
| [[Westland Lynx#Super Lynx and Battlefield Lynx|Super Lynx Mk.130]] || Search and rescue ||align=center| 4 || In service 2011.
|-
| [[Westland Lynx#Super Lynx and Battlefield Lynx|Super Lynx Mk.140]] || ASW || align="center" | ||6 ordered in 2012<ref>[http://defense-arab.com/news/?p=2919 فرقاطتين المانيتين و 3 كورفيت صينية للجزائر<!-- Bot generated title -->]</ref>
|-
| [[Kamov Ka-27|Ka-32T]] || Maritime utility transport, search and rescue helicopter ||align=center| 5 || In service 1996.<ref name="armstrade.sipri.org">http://armstrade.sipri.org/armstrade/page/trade_register.php</ref>
|-
| [[Beechcraft King Air]] || Maritime patrol (operated by air force) ||align=center| 2 || In service 2013.<ref>http://www.forcesdz.com/forum/viewtopic.php?f=18&t=974&start=15</ref>
|}

=== Modernization ===
[[File:Algerian Sailors conduct Maritime Interdiction Operations (MIO).jpg|thumb|200px|Algerian Sailors conduct Maritime Interdiction Operations (MIO).]]
The Navy is currently being upgraded with the following technological developments: the existing units are being modernized, with the submarine force strengthened by two new [[Kilo-class submarine|Kilo class]] submarines(last generation).<ref>[http://www.defenceweb.co.za/index.php?option=com_content&task=view&id=26976&Itemid=106 Navantia to modernise Algerian Navy warships]</ref>

*One LPD form Italy in 2014.
*Four MEKO A200 frigates from Germany.
*Three corvettes C28A with option of three more produced locally. Radar and electronic equipment will be supplied by Thales, and mounted in Algeria. They will be built at Hudong Zhonghua Shipyard.<ref name="navyrecognition.com">http://www.navyrecognition.com/index.php/world-naval-forces/african-navies-vessels-ships-equipment/algeria-algerian-navy-vessels-ships-equipment.html</ref><ref name="defencetalk.com">http://www.defencetalk.com/dutch-ok-military-equipment-supply-to-algeria-48774/</ref><ref name="jeuneafrique.com">http://www.jeuneafrique.com/ArticlePersonnalite/ARTJAWEB20131115162408/armee-algerienne-a-quoi-va-servir-la-hausse-du-budget-de-la-defense.html</ref>
*Six tiger corvettes from Russia.
* 21 units of the type FPB98 MKI Ocean Patrol Boat. <ref name="meretmarine.com" /><ref name="globalsecurity.org" />
* 12 units of Alusafe 2000 high speed rescue and patrol vessel. <ref>http://www.maritime-partner.com/mp/News/13-06-20/Record_breaking_contract.aspx</ref>

=== Gallery ===

<gallery widths="250px" heights="250px" perrow="4">
|''Djebel Chenoua''-class corvette ''El Chihab'' (352) built by ECRN in [[Mers-el-Kebir]]
|Improved Koni II Frigate Rais Kellich 902
|Algerian Rescue Boat (Alusafe-2000).

</gallery>

==Munitions==

=== SAM ===
*[[Umkhonto (missile)]]<ref>http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=32171:denel-successfully-fires-land-based-umkhonto-surface-to-air-missile&catid=50:Land&Itemid=105</ref>
*[[Aster (missile family)]]<ref>http://www.navyrecognition.com/index.php?option=com_content&task=view&id=1985</ref>
*[[HQ-7|HQ-7A (FM-90)]] - on board [[C28A Class Corvette]] .
*[[9K33 Osa|9M33/SA-8]] - Osa-M (SA-N-4)  naval version for Koni (Mourad Rais) frigate

=== Anti-ship Missiles ===
*[[RBS-15|RBS-15 Mk. III]] <ref name="navyrecognition.com"/><ref>{{cite web|url=http://www.navyrecognition.com/index.php?option=com_content&task=view&id=525 |title=Algerian Navy signs deal with ThyssenKrupp Marine Systems for 2+2 Meko A200 Frigates: Details |publisher=Navyrecognition.com |date=2012-07-25 |accessdate=2013-09-02}}</ref><ref>http://www.meretmarine.com/fr/content/lalgerie-commande-deux-fregates-tkms</ref> III will equip the two [[MEKO 200]] frigates under construction for the Algerian National Navy. Delivery is scheduled for 2015-2016.
*[[Kh-35|SS-N-25]] <ref name="navyrecognition.com"/>
*[[C-802]] <ref name="navyrecognition.com"/>
*[[P-15 Termit|SSC-3 Styx]] <ref name="navyrecognition.com"/>
*[[3M-54 Klub|3M-14E]] <ref name="navyrecognition.com"/><ref>http://vpk.name/news/41707_indiya_pr...tyi_3m14e.html</ref>
*[[3M-54 Klub|P-900 Alfa]]
*[[P-15 Termit|SS-N-2C]]

=== Air to ground Missiles ===
*[[Mokopa]] - the Algerian Navy's six new Super Lynx 300-series helicopters are conducting flight tests armed with Mokopa anti-armour missiles.
*Raptor-2 Precision-Guided Glide Bomb series from [[south africa]]<ref name="armstrade.sipri.org"/>

== See also ==
* [[List of Algerian ships]]

== References ==
;Notes
{{reflist|group=Note}}

;Citations
{{reflist|2}}

;Bibliography
{{refbegin}}
* {{cite news |url=http://af.reuters.com/article/metalsNews/idAFL6E8F58US20120405 |last=Ahmed |first=Hamid Ould |editor1-last=Billingham |editor1-first=Erica |title=ThyssenKrupp wins 400 mln euro Algeria deal |accessdate=22 May 2012 |date=5 April 2012 |work=Reuters}}
* {{cite book |editor1-first=Raymond V. B. |editor1-last=Blackman |title=Jane's Fighting Ships 1967-68 |year=1968 |publisher=Jane's Information Group |location=United Kingdom }}
* {{cite book |editor1-first=Chris |editor1-last=Chant |title=The World's Navies |edition=1st |year=1979 |publisher=Chartwell Books Inc. |location=Secaucus, New Jersey |isbn=089009-268-0}}
* {{cite web |url=http://combatfleetoftheworld.blogspot.com/2012/03/future-of-algerian-navy_07.html |title=Future of the Algerian Navy |last1=Colombaro |first1=Mike |last2= |first2= |date=7 March 2012 |work=Combat Fleets of the World |publisher= |accessdate=13 May 2012}}
* {{cite web |url=http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=16804:algerian-navy-purchases-two-tiger-corvettes-from-russia&catid=51:Sea&Itemid=106 |title=Algerian Navy purchases two Tiger corvettes |date=1 July 2011 |author=DefenceWeb |accessdate=22 May 2012}}
* {{cite web |url=http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=24773:algeria-orders-two-meko-frigates&catid=51:Sea&Itemid=106 |title=Algeria orders two Meko frigates |author=DefenceWeb |date=4 April 2012 |accessdate=22 May 2012}}
* {{cite web |url=http://www.worldwarships.com/warships_algeria.htm |title=Algeria |author=Matterson Marine Pty Ltd |date=20 May 2006 |work=Warships |publisher= |accessdate=13 May 2012}}
* {{cite web |url=http://www.meretmarine.com/article.cfm?id=118280 |title=L'Algérie se paye trois sisterships de l'Abeille Bourbon |date=5 January 2012 |work= |author=Mer et Marine |accessdate=13 May 2012}}
* {{cite book |first=David |last=Miller |title=The World's Navies |year=1992 |publisher=Salamander Books Ltd. |isbn=0-517-05241-5}}
* {{cite web |url=http://en.rian.ru/russia/20110630/164924783.html |title=Russia to build two Tiger corvettes for Algerian navy |author=RIA Novosti |date=30 June 2011 |accessdate=22 May 2012}}
* {{cite web |url=http://russian-ships.info/eng/warships/project_1159.htm |title=Guard Ships - Project 1159 |author=Russian-Ships.info |date= |work= |publisher= |accessdate=13 May 2012}}
* {{cite web |url=http://russian-ships.info/eng/submarines/project_877.htm |title=Large submarines - Project 877 Paltus |author=Russian-Ships.info |date= |work= |publisher= |accessdate=13 May 2012}}
* {{cite web |url=http://russian-ships.info/eng/warships/project_1234.htm |title=Small Missile Ships - Project 1234 Ovod |author=Russian-Ships.info |date= |work= |publisher= |accessdate=13 May 2012}}
* {{cite web |url=http://www.strategypage.com/htmw/htsurf/articles/20120514.aspx |title=Surface Forces: MEKO A200 |author=StrategyWorld.com |accessdate=22 May 2012}}
* {{cite web |url=http://hazegray.org/worldnav/africa/algeria.htm |title=World Navies Today: Algeria |last1=Toppan |first1=Andrew |last2= |first2= |date=21 October 2001 |work= |publisher= |accessdate=13 May 2012}}
{{refend}}

== External links ==
* [http://www.navyrecognition.com/index.php/world-naval-forces/african-navies-vessels-ships-equipment/algeria-algerian-navy-vessels-ships-equipment.html Algerian Navy ships and equipment]

{{Algeria Military}}
{{Algerian security forces}}
{{Navies in Africa}}

[[Category:Algerian National Navy| ]]