{{Infobox journal
| cover = [[File:IEEESoftware.jpg|200px]]
| editor = [[Diomidis Spinellis]]
| discipline = [[Computer science]], [[Software]]
| abbreviation = IEEE Softw.
| publisher = [[IEEE Computer Society]]
| country =
| frequency = Bimonthly
| history = 1983–present
| openaccess =
| license = 
| impact = 0.820
| impact-year = 2015
| website = https://www.computer.org/software-magazine/
| link1 = http://ieeexplore.ieee.org/xpl/aboutJournal.jsp?punumber=52
| link1-name = Online access
| JSTOR = 
| OCLC = 10024196
| LCCN = 89644395
| CODEN = IESOEG
| ISSN = 0740-7459
| eISSN = 1937-4194
}}
'''''IEEE Software''''' is a bimonthly [[peer-reviewed]] [[magazine]] and [[scientific journal]] published by the [[IEEE Computer Society]] covering all aspects of [[software engineering]], processes, and practices. It was established in 1983 and is published by the [[IEEE Computer Society]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.820.<ref name=WoS>{{cite book |year=2016 |chapter=IEEE Software |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==Editors-in-chief==
The following individuals are or have been [[editor-in-chief]] of the journal:<ref>{{Cite journal |author=Warren Harrison |title=Our 20th Anniversary |year=2003 |journal=IEEE Software |volume=20|pages=5–7 |issue=6 |doi=10.1109/MS.2003.10026}}</ref><ref>{{Cite journal|author=Scott Laningham |title=developerWorks Interviews: IEEE Software Magazine's Hakan Erdogmus |year=2007 |journal=IBM developerWorks podcast |url=http://www.ibm.com/developerworks/podcast/dwi/cm-int120507.html |archiveurl=https://web.archive.org/web/20100417084916/http://www.ibm.com/developerworks/podcast/dwi/cm-int120507.html |archivedate=2010-04-17 |deadurl=yes |df= }}</ref><ref>{{Cite journal |author=Hakan Erdogmus |title=IEEE Software's 25th-Anniversary Top Picks |year=2009 |journal=IEEE Software |volume=26|pages=9–11 |doi=10.1109/MS.2009.13 |issue=1}}</ref>
{{columns-list|colwidth=30em|
* [[Diomidis Spinellis]] ([[Athens University of Economics and Business]]) 2014–present
* Forrest Shull, 2011–2014
* Hakan Erdogmus, 2007–2010
* Warren Harrison, 2003–2006
* [[Steve McConnell]], 1999–2002
* [[Alan M. Davis]], 1995–1998
* [[Carl Chang (computer scientist)|Carl Chang]], 1991–1994
* [[Ted Lewis (computer scientist)|Ted Lewis]], 1987–1990
* Bruce Shriver, 1983–1986
}}

==See also==
* ''[[IEEE Transactions on Software Engineering]]''
* ''[[IET Software]]''

==References==
{{Reflist}}

==External links==
* {{Official website|https://www.computer.org/software-magazine/}}

[[Category:IEEE academic journals|Software]]
[[Category:Computer science journals]]
[[Category:Software engineering publications]]
[[Category:Publications established in 1983]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]