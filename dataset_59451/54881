{{unreferenced|date=February 2008}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Operation Hell Gate
| title_orig    = 
| translator    = 
| image         = Operationhellgate.jpg<!--prefer 1st edition-->
| caption = First edition cover
| author        = [[Marc Cerasini]] 
| illustrator   = 
| cover_artist  = 
| country       = [[United States]]
| language      = [[English language|English]]
| series        = [[24: Declassified]] 
| genre         = [[Thriller novel]]
| publisher     = [[Harper (publisher)|Harper]]
| pub_date      = Oct 2005
| english_pub_date =
| media_type    = Print ([[Paperback]]
| pages         = 336 pp (first edition, paperback)
| isbn = 978-0-06-084224-6
| isbn_note = (first edition, paperback)
| oclc= 62081108
| preceded_by   = 
| followed_by   = 
}}

'''''Operation Hell Gate''''' (2005) by [[Marc Cerasini]] is the first of the [[24: Declassified]] novels based on the [[Fox Broadcasting Company|FOX]] [[television series]] ''[[24 (TV series)|24]]''. It takes place before [[24 (season 1)|season 1]], and [[Jack Bauer]]'s story is set in [[New York City]]. Co-authors for the novel are Joel Surnow and Robert Cochran.

== Plot summary ==
Jack Bauer has a bit of a problem with a recent mission. Subsequently, he must accompany the prisoner, FBI agent Frank Hensley (who was later revealed to be an Iraqi agent) and other government officials. The plane crashes and all chaos breaks loose - Frank Hensley frames Jack Bauer for the murder of two FBI agents, Ryan Chappelle has trouble helping Jack from LA, and a weapon has been stolen that could potentially release a virus deadly to the whole of America.
Jack Bauer must use all his skill and recruit the help of some Irish woman (Caitlin) in order to break down a highly complicated conspiracy that leads to the government.

== Characters ==
'''''Characters from the Show'''''

*'''Jack Bauer'''- The protagonist of 24, Jack goes undercover to take down the terrorist attack that threatens the country. A subplot involves Jack being framed by Frank Hensley for killing two Federal Marshals and helping a fugitive escape.
*'''Nina Myers'''- Nina provides CTU support for Jack in Los Angeles.
*'''Tony Almeida'''- Tony has his own subplot involving characters in Los Angeles related to the terrorist plot.
*'''Jamey Farrell and Milo Pressman'''- The main CTU tech analysts.
*'''Ryan Chappelle'''- Comes in from Division to help lead the operation on thwarting the days events.
*'''Richard Walsh'''- Appears in the Prologue and the Epilogue, debriefs Jack on Operation Hell Gate.

'''New Characters'''
*'''Caitlin O'Connor'''- An immigrant and girlfriend of Shamus, she finds herself in the middle of a conspiracy and ends up aiding Jack.
*'''Liam O'Connor'''- Caitlin's 15-year-old brother. Has his own sub-plot of delivering a case to Taj.
*'''Dae Soo Min (Doris)'''- A female Korean analyst brought in to help aid Jack.
*'''Georgi Timko'''- Russian immigrant who owns Tatiana's Tavern.  At the end of the novel, it is revealed Timko gave CTU-Los Angeles the tip-off about the attack at the beginning of the novel.
*'''Yuri'''- Timko's trusty henchman. He doesn't say much but is skilled at gun-fights.
*'''Saito'''- undercover police agent for the LAPD
*'''Captain Jessica Schneider'''- A former marine who is called into CTU for help working on the computer chip. She is also there for field work.

'''Villains'''
*'''Frank Hensley''' - A war hero who was thought to be killed in Iraq by the Iraqi Special Forces. One of his captors assumed his identity and joined the [[Federal Bureau of Investigation|FBI]], allowing them to attempt an act of terror.
*'''Taj Ali Kahlil'''- brother of Khan Ali Kahlil. An Afghan who wants revenge on America for betraying his people in the Soviet-Afghanistan War.
*'''Khan Ali Kahlil'''- brother of Taj Kahlil. When Jack pretends to be Shamus Lynch to meet Taj, Khan pretends to be Taj to test Jack. When Jack fails that test, he attempts to kill Jack by choking him. The attempt fails and Khan commits suicide by throwing himself over the bridge.
*'''Shamus and Griffin Lynch'''- Two brothers who have a rough past in their home country. They both have aliases in America and aides in terrorism on US soil. Shamus tries to provide for Caitlin's family.
*'''Omar Bayat'''- henchman of Taj Kahlil and assassin to kill Felix Tanner.
*'''Dante Arete'''- Part of a Spanish gang involved in terrorist activity to launch a missile strike. He is caught by Jack Bauer but escapes after the plane crash lands. He helps terrorists to earn money, but is killed when the bomb inside the suitcase full of money is activated.
*'''Dennis Spain'''- Chief of staff of Senator Cheever of New York.

==External links==
*[http://www.harpercollins.com/books/9780060842246/24_Declassified_Operation_Hell_Gate/index.aspx?WT.mc_id=WikiP9780060842246 Book excerpt and author information] (Official publisher web page)

==References==
{{reflist}}

{{24 (TV series)}}

[[Category:2005 American novels]]
[[Category:American thriller novels]]
[[Category:24 (TV series)]]
[[Category:Novels set in New York City]]