{{Italic title}}
The '''''Connecticut Journal of International Law''''' is a biannual student-edited [[international law|international]] [[law review]] published by the [[University of Connecticut School of Law]] since 1985. It publishes articles, essays, notes, and commentaries that cover a wide range of topics in international and [[comparative law]]. The journal also sponsors an annual symposium, with topics ranging from the Cambodian War Crimes Tribunals to the effect of [[Walmart|Wal-Mart]] in an international economy.

== Notable articles ==
The top five most-cited articles published in the journal are:{{says who|date=April 2012}}
# Anthony D'Amato, ''It's a Bird, It's a Plane, It's Jus Cogens'', 6 Conn. J. Int'l L. 1 (1990).  
# Matthew Lippman, ''Nuremberg: Forty Five Years Later'', 7 Conn. J. Int'l L. 1 (1991).
# Bryan F. MacPherson, ''Building an International Criminal Court for the 21st Century'', 13 Conn. J. Int'l L. 1 (1998).
# Patty Gerstenblith, ''The Public Interest in the Restitution of Cultural Objects'', 16 Conn. J. Int'l L. 197 (2001).
# Lance Compa, ''Going Multilateral: The Evolution of U.S. Hemispheric Labor Rights Policy Under GSP and NAFTA'', 10 Conn. J. Int'l L. 337 (1995).

== Editors-in-chief ==
The following persons have been [[editor-in-chief]] of the journal:
* Randall Blowers - vol. 28.2
* Michael Bradley - vol. 28.1
* Christopher A. Potts - vol. 27
* Geoffrey Ong - vol. 26
* Katayoun Sadeghi - vol. 25
* Alexandra M. Greene - vol. 24
* Marcelo Phillips - vol. 23
* James C. Goodfellow Jr. - vol. 22
* David Lawton - vol. 21
* Patricia M. O'Rourke - vol. 20
* Cara D. Cutler - vol. 19

== External links ==
* {{Official|http://www.cjil.org/}}

[[Category:American law journals]]
[[Category:International law journals]]
[[Category:University of Connecticut]]
[[Category:Publications established in 1985]]
[[Category:Biannual journals]]
[[Category:English-language journals]]
[[Category:Law journals edited by students]]