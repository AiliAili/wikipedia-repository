{{Redirect|Green children}}
[[File:WoolpitSign.jpg|thumb|200px|right|upright|Village sign depicting the two green children, erected in 1977{{r|ClarkPP209-229}}]]
The legend of the '''green children of Woolpit''' concerns two children of unusual skin colour who reportedly appeared in the village of [[Woolpit]] in [[Suffolk]], England, some time in the 12th century, perhaps during the reign of King [[Stephen, King of England|Stephen]]. The children, brother and sister, were of generally normal appearance except for the green colour of their skin. They spoke in an unknown language, and would only eat raw [[Vicia faba|broad beans]]. Eventually they learned to eat other food and lost their green pallor, but the boy was sickly and died soon after he and his sister were baptised. The girl adjusted to her new life, but she was considered to be "rather loose and wanton in her conduct".{{r|ODNB}} After she learned to speak English, the girl explained that she and her brother had come from Saint Martin's Land, a subterranean world inhabited by green people.

The only near-contemporary accounts are contained in [[William of Newburgh]]'s ''Historia rerum Anglicarum'' and [[Ralph of Coggeshall]]'s ''Chronicum Anglicanum'', written in about 1189 and 1220 respectively. Between then and their rediscovery in the mid-19th century, the green children seem to surface only in a passing mention in [[William Camden]]'s ''Britannia'' in 1586,{{r|ClarkPP209-229}} and in [[Francis Godwin|Bishop Francis Godwin]]'s fantastical ''[[The Man in the Moone]]'',{{r|LawtonPP23-55}} in both of which William of Newburgh's account is cited.

Two approaches have dominated explanations of the story of the green children: that it is a [[folklore|folktale]] describing an imaginary encounter with the inhabitants of another world, perhaps subterranean or even extraterrestrial, or it is a garbled account of a historical event. The story was praised as an ideal fantasy by the English [[anarchism|anarchist]] poet and critic [[Herbert Read]] in his ''English Prose Style'', published in 1931. It provided the inspiration for his only novel, ''[[The Green Child]]'', written in 1934.

==Sources==
The village of [[Woolpit]] is in the county of [[Suffolk]], East Anglia, about {{convert|7|mi|spell=in}} east of the town of [[Bury St Edmunds]]. During the Middle Ages it belonged to the [[Abbey of Bury St Edmunds]], and was part of one of the most densely populated areas in rural England. Two writers, [[Ralph of Coggeshall]] (died c.&nbsp;1226) and [[William of Newburgh]] (c.&nbsp;1136–1198), reported on the sudden and unexplained arrival in the village of two green children during one summer in the 12th century. Ralph was the abbot of a [[Cistercians|Cistercian]] monastery at [[Coggeshall]], about {{convert|26|mi}} south of Woolpit. William was a [[Canon (priest)|canon]] at the [[Augustinians|Augustinian]] [[Newburgh Priory]], far to the north in [[Yorkshire]]. William states that the account given in his ''Historia rerum Anglicarum'' (c.&nbsp;1189) is based on "reports from a number of trustworthy sources"; Ralph's account in his ''Chronicum Anglicanum'', written some time during the 1220s, incorporates information from Sir Richard de Calne of Wykes,{{efn|name=deCalne|Richard de Calne died in or before 1188. In some accounts he is named Richard de Caine.{{r|ClarkPP209-229}}}} who reportedly gave the green children refuge in his manor, {{convert|6|mi|spell=in}} to the north of Woolpit. The accounts given by the two authors differ in some details.{{r|ClarkPP209-229}}

==Story==
One day at harvest time, according to William of Newburgh during the reign of King Stephen<!--a day during the reign of Stephen, or William wrote this during Stephen's reign?--> (1135–1154),{{r|ClarkPP209-229}} the villagers of Woolpit discovered two children, a brother and sister, beside one of the [[trapping pit|wolf pits]] that gave the village its name.{{r|DBPN}}{{efn|name=WolfPit|A [[Trapping pit#Pits for trapping wolves|wolf pit]] was a deep pit into which carrion was thrown to attract wolves, and then covered over with branches.{{sfn|Cosman|Jones|2008|p=127|ps=}}}} Their skin was green, they spoke an unknown language, and their clothing was unfamiliar. Ralph reports that the children were taken to the home of Richard de Calne. Ralph and William agree that the pair refused all food for several days until they came across some raw [[Vicia faba|broad beans]], which they consumed eagerly.{{efn|name=Briggs|"It is to be noticed, too, that the habitual food of the children was beans, the food of the dead", observes K. M. Briggs.{{r|Briggs}} She had made the same observation about the food of the dead in her 1967 book ''The Fairies in English Tradition and Literature'',{{sfn|Briggs|1967|p=6|ps=}} but John Clark casts doubt on the supposed tradition that Briggs is referring to, commenting that "an identification of beans as the food of the dead is unwarranted".{{r|Clarke2006}}}} The children gradually adapted to normal food and in time lost their green colour.{{r|ClarkPP209-229}} The boy, who appeared to be the younger of the two, became sickly and died shortly after he and his sister were baptised.

After learning to speak English, the children&nbsp;– Ralph says just the surviving girl&nbsp;– explained that they came from a land where the sun never shone and the light was like twilight. William says the children called their home St Martin's Land; Ralph adds that everything there was green. According to William, the children were unable to account for their arrival in Woolpit; they had been herding their father's cattle when they heard a loud noise (according to William, the bells of Bury St Edmunds{{sfn|Cohen|2008|p=83|ps=}}) and suddenly found themselves by the wolf pit where they were found. Ralph says that they had become lost when they followed the cattle into a cave and, after being guided by the sound of bells, eventually emerged into our land.{{r|ClarkPP209-229}}

According to Ralph, the girl was employed for many years as a servant in Richard de Calne's household, where she was considered to be "very wanton and impudent". William says that she eventually married a man from [[King's Lynn]], about {{convert|40|mi}} from Woolpit, where she was still living shortly before he wrote.{{r|ClarkPP209-229}} Based on his research into Richard de Calne's family history, the astronomer and writer [[Duncan Lunan]] has concluded that the girl was given the name "Agnes" and that she married a royal official named [[Richard Barre]].{{r|Lunan}}

==Explanations==
Neither Ralph of Coggeshall nor William of Newburgh offer an explanation for the "strange and prodigious" event, as William calls it, and some modern historians have the same reticence: "I consider the process of worrying over the suggestive details of these wonderfully pointless miracles in an effort to find natural or psychological explanations of what 'really,' if anything, happened, to be useless to the study of William of Newburgh or, for that matter, of the Middle Ages", says Nancy Partner, author of a study of 12th-century historiography.{{sfn|Partner|1977|pp=121–22|ps=}} Nonetheless, such explanations continue to be sought and two approaches have dominated explanations of the mystery of the green children. The first is that the narrative descends from [[folklore]], describing an imaginary encounter with the inhabitants of a "fairy Otherworld".{{r|ClarkPP209-229}} In a few early{{r|ClarkPP209-229}} as well as modern readings, this other world is extraterrestrial, and the green children alien beings.{{r|Lunan}} The second is that it is a garbled account of a real event,{{r|ClarkPP209-229}} although it is impossible to be certain whether the story as recorded is an authentic report given by the children or an "adult invention".{{r|Orme}} His study of accounts of children and servants fleeing from their masters led Charles Oman to conclude that "there is clearly some mystery behind it all [the story of the green children], some story of drugging and kidnapping".{{r|Oman}} Jeffrey Jerome Cohen offers a different kind of historical explanation, arguing that the story is an oblique account of the racial difference between the contemporary English and the [[Britons (historical)|indigenous Britons]].{{sfn|Cohen|2008|p=90|ps=}}

===Folklore===
Scholars such as Charles Oman note that one element of the children's account, the entry into a different reality by way of a cave, seems to have been quite popular. [[Gerald of Wales]] tells a similar story of a boy who, after escaping his master, "encountered two pigmies who led him through an underground passage into a beautiful land with fields and rivers, but not lit by the full light of the sun".{{r|Oman}} But the [[Motif (folkloristics)|motif]] is poorly attested; E. W. Baughman lists it as the only example of his [[Aarne–Thompson classification systems|F103.1 category]] of English and North American folk tale motifs: "Inhabitants of lower world visit mortals, and continue to live with them".{{sfn|Baughman|1966|p=203|ps=}} Martin Walsh considers the references to St Martin to be significant, and sees the story of the green children as evidence that the feast of [[St. Martin's Day|Martinmas]] has its origins in an English aboriginal past, of which the children's story forms "the lowest stratum".{{r|Walsh}} {{nowrap|E. S. Alderson}} suggests a [[Celtic mythology|Celtic]] connection in a 1900 edition of ''[[Notes and Queries]]'': "&nbsp;'Green' spirits are 'sinless' in Celtic literature and tradition&nbsp;... It may be more than a coincidence that the green girl marries a 'man of [Kings] Lynn.' Here the original [Celtic word] would be ''lein'', evil, ''i.e.'' the pure fairy marries a sinful child of earth."{{r|Alderson}}

[[File:Babes in the Wood - 7 - illustrated by Randolph Caldecott - Project Gutenberg eText 19361.jpg|thumb|upright|Illustration of the abandoned Babes in the Wood by [[Randolph Caldecott]], 1879]]
In a modern development of the tale the green children are associated with the [[Babes in the Wood]], who were left by their wicked uncle to die; in this version the children's green colouration is explained by their having been poisoned with arsenic. Fleeing from the wood in which they were abandoned, possibly nearby [[Thetford Forest]], the children fell into the pits at Woolpit where they were discovered. Local author and folk singer Bob Roberts states in his 1978 book ''A Slice of Suffolk'' that "I was told there are still people in Woolpit who are 'descended from the green children', but nobody would tell me who they were!"{{r|ClarkPP209-229}}

Other commentators have suggested that the children may have been [[Extraterrestrial life|aliens]], or inhabitants of a world beneath the Earth. In a 1996 article published in the magazine ''[[Analog Science Fiction and Fact|Analog]]'', astronomer [[Duncan Lunan]] hypothesised that the children were accidentally transported to Woolpit from their home planet as the result of a "matter transmitter" malfunction.{{sfn|Haughton|2007|p=236|ps=}}  Lunan suggests that the planet from which the children were expelled may be trapped in [[synchronous orbit]] around its sun, presenting the conditions for life only in a narrow twilight zone between a fiercely hot surface and a frozen dark side. He explains the children's green colouration as a side effect of consuming the genetically modified alien plants eaten by the planet's inhabitants.{{r|Lunan}}

Lunan was not the first to state that the green children may have been extraterrestrials. [[Robert Burton (scholar)|Robert Burton]] suggested in his 1621 {{nowrap|''[[The Anatomy of Melancholy]]''}} that the green children "fell from Heaven", an idea that seems to have been picked up by [[Francis Godwin]], historian and Bishop of Hereford, in his speculative fiction ''[[The Man in the Moone]]'',{{r|ClarkPP209-229}} published posthumously in 1638.{{r|Hutton}}

===Historical explanations===
Many [[Flemish people|Flemish]] immigrants arrived in eastern England during the 12th century, and they were persecuted after [[Henry II of England|Henry II]] became king in 1154; a large number of them were killed near Bury St Edmunds in 1173 at the [[Battle of Fornham]] fought between Henry II and [[Robert de Beaumont, 3rd Earl of Leicester]]. Paul Harris has suggested that the green children's Flemish parents perished during a period of civil strife and that the children may have come from the village of [[Fornham St Martin]], slightly to the north of Bury St Edmunds, where a settlement of Flemish [[fulling|fuller]]s existed at that time. They may have fled and ultimately wandered to Woolpit. Disoriented, bewildered, and dressed in unfamiliar Flemish clothes, the children would have presented a very strange spectacle to the Woolpit villagers.{{sfn|Harris|1998|ps=}} The children's colour could be explained by [[Hypochromic anemia|green sickness]], the result of a dietary deficiency.{{r|ClarkPP209-229}} Brian Haughton considers Harris's explanation to be plausible, and the one most widely accepted,{{sfn|Haughton|2007|p=441|ps=}} although not without its difficulties. For instance, he suggests it is unlikely that an educated local man like Richard de Calne would not have recognised the language spoken by the children as being [[Flemish language|Flemish]].{{sfn|Haughton|2007|p=443|ps=}}

Historian Derek Brewer's explanation is even more prosaic:
{{quote|The likely core of the matter is that these very small children, herding or following flocks, strayed from their forest village, spoke little, and (in modern terms) did not know their own home address. They were probably suffering from chlorosis, a deficiency disease which gives the skin a greenish tint, hence the term "green sickness". With a better diet it disappears.{{sfn|Brewer|1998|p=182|ps=}}}}

Jeffrey Jerome Cohen proposes that the story is about racial difference, and "allows William to write obliquely about the Welsh":{{r|duckworth}} the green children are a memory of England's past and the violent conquest of the indigenous Britons by the [[Anglo-Saxons]] followed by the [[Norman conquest of England|Norman invasion]]. William of Newburgh reluctantly{{sfn|Cohen|2008|p=84|ps=}} includes the story of the green children in his account of a largely unified England, which Cohen juxtaposes with [[Geoffrey of Monmouth]]'s ''[[The History of the Kings of Britain]]'', a book that according to William is full of "gushing and untrammeled lying".{{sfn|Cohen|2008|p=80|ps=}} Geoffrey's history offers accounts of previous kings and kingdoms of various racial identities, whereas William's England is one in which all peoples are either assimilated or pushed to the boundaries. According to Cohen, the green children represent a dual intrusion into William's unified vision of England. On one hand they are a reminder of the racial and cultural differences between Normans and Anglo-Saxons, given the children's claim to have come from St Martin's Land, named after [[Martin of Tours]]; the only other time William mentions that saint is in reference to {{nowrap|[[Battle Abbey|St Martin's Abbey]]}} in [[Hastings]], which commemorates the Norman victory in 1066.{{sfn|Cohen|2008|pp=85–88|ps=}} But the children also embody the earlier inhabitants of the British Isles, the "Welsh (and Irish and Scots) who [had been] forcibly anglicized&nbsp;... The Green Children resurface another story that William had been unable to tell, one in which English paninsular dominion becomes a troubled assumption rather than a foregone conclusion."{{sfn|Cohen|2008|p=90|ps=}} The boy in particular, who dies rather than become assimilated, represents "an adjacent world that cannot be annexed&nbsp;... an otherness that will perish to endure".{{sfn|Cohen|2008|p=91|ps=}}

==Legacy==
The English [[anarchism|anarchist]] poet and critic [[Herbert Read]] describes the story of the green children in his ''English Prose Style'', published in 1931, as "the norm to which all types of fantasy should conform".{{r|Harder}} It was the inspiration for his only novel, ''[[The Green Child]]'', written in 1934.{{r|Leeds}} A 1994 adaptation of the story by [[Kevin Crossley-Holland]] tells it from the point of view of the green girl.{{r|ClarkPP209-229}}

Author John Macklin includes an account in his 1965 book, ''Strange Destinies'', of two green children who arrived in the Spanish village of Banjos in 1887.{{r|ClarkPP209-229}} Many details of the story very closely resemble the accounts given of the Woolpit children, such as the name of Ricardo de Calno, the mayor of Banjos who befriends the two children, strikingly similar to Richard de Calne.{{sfn|Fanthorpe|Fanthorpe|2010|p=311|ps=}} It therefore seems that Macklin's story is an invention inspired by the green children of Woolpit,{{r|ClarkPP209-229}} particularly as there is no record of any Spanish village called Banjos.{{sfn|Fanthorpe|Fanthorpe|2010|p=311|ps=}}

Australian novelist and poet [[Randolph Stow]] uses the account of the green children in his 1980 novel ''The Girl Green as Elderflower''; the green girl is the source for the title character, here a blond girl with green eyes. The green children become a source of interest to the main character, Crispin Clare, along with some other characters from the Latin accounts of William of Newburgh, Gervase of Tilbury, and others, and Stow includes translations from those texts: these characters "have histories of loss and dispossession that echo [Clare's] own".{{r|duckworth}} The green children are the subject of a 1990 community opera performed by children and adults, composed by [[Nicola LeFanu]] with a [[libretto]] written by Kevin Crossley-Holland.{{r|LeFanu}} In 2002 English poet [[Glyn Maxwell]] wrote a [[Verse drama and dramatic verse|verse play]] based on the story of the green children, ''Wolfpit'' (the earlier name for Woolpit{{r|Harvey}}), which was performed once in [[New York City]]. In Maxwell's version the girl becomes an indentured servant to the lord of the manor, until a stranger named Juxon buys her freedom and takes her to an unknown destination.{{r|Smith}}

==Notes and references==
'''Notes'''
{{notelist|notes=}}

'''References'''
{{reflist|30em|refs=
<ref name="Alderson">
{{citation|last=Alderson|first=E. S.|date=24 February 1900 |title=Green Fairies: Woolpit Green Children |journal=[[Notes and Queries]] |volume=5 |page=155}}
</ref>

<ref name="Briggs">
{{citation |last=Briggs |first=K. M. |title=The Fairies and the Realms of the Dead |journal=[[Folklore Society|Folklore]] |year=1970 |volume=81 |issue=2 |pages=81–96 |jstor=508383 |doi=10.1080/0015587X.1970.9716666}}
</ref>

<ref name="Clarke2006">
{{citation |last=Clark |first=John |title=Martin and the Green Children |journal=[[Folklore Society|Folklore]] |year=2006 |volume=117 |issue=2 |pages=207–214 |doi=10.1080/00155870600707904}}
</ref>

<ref name="ClarkPP209-229">
{{citation |last=Clark |first=John |title='Small, Vulnerable ETs': The Green Children of Woolpit |year=2006 |journal=Science Fiction Studies |volume=33 |issue=2 |pages=209–229 |jstor=4241432}}
</ref>

<ref name="DBPN">
{{citation |last=Mills |first=A. D. |contribution=Woolpit |title=A Dictionary of British Place-Names |year=2003 |url=http://www.oxfordreference.com/views/ENTRY.html?subview=Main&entry=t40.e14264 |publisher=Oxford University Press |accessdate=25 April 2009}} {{subscription required}}
</ref>

<ref name="duckworth">{{citation |last=Duckworth |first=Melanie |year=2011 |title=Grievous Music: Randolph Stow's Middle Ages |journal=[[Australian Literary Studies]] |volume=26 |issue=3/4 |pages=102–14}}
</ref>

<ref name="Harder">
{{citation |last=Harder |first=Worth T. |title=Crystal Source: Herbert Read's ''The Green Child'' |journal=The Sewanee Review |year=1973 |volume=81 |issue=4 |pages=714–738 |jstor=27542761}}
</ref>

<ref name="Harvey">
{{citation |last=Harvey |first=Nigel |title=Some Suffolk Superstitions |journal=[[Folklore (journal)|Folklore]] |year=1943 |volume=54 |issue=4 |pages=390–91 |jstor=1257174 |doi=10.1080/0015587X.1943.9717694}}
</ref>

<ref name="Hutton">
{{citation |last=Hutton |first=Sarah |title=''The Man in the Moone'' and the New Astonomy: Godwin, Gilbert, Kepler|journal=Etudes Epistémè |issue=7 |date=Spring 2005 |pages=3–13 |url=http://revue.etudes-episteme.org/IMG/pdf/ee_7_art_hutton-2.pdf |format=PDF}}
</ref>

<ref name="LawtonPP23-55">
{{citation |first=H. W. |last=Lawton |title=Bishop Godwin's Man in the Moone |journal=The Review of English Studies |volume=7 |issue=25 |date=January 1931 |pages=23–55 |jstor=508383 |doi=10.1093/res/os-vii.25.23}}
</ref>

<ref name="Leeds">
{{cite web |url=http://www.leeds.ac.uk/library/spcoll/virtualtour/herbert.htm |title=The Green Child by Herbert Read |publisher=Leeds University Library |accessdate=22 February 2011 |mode=cs2}}
</ref>

<ref name="LeFanu">
{{cite web |url=http://www.chesternovello.com/Default.aspx?TabId=2432&State_3041=2&workId_3041=1234 |title=Nicola LeFanu: The Green Children |publisher=[[Chester Novello]] |accessdate=5 July 2011 |mode=cs2}}
</ref>

<ref name="Lunan">
{{citation |last=Lunan |first=Duncan |title=Children from the Sky |date=September 1996 |magazine=[[Analog Science Fiction and Science Fact]] |volume=116 |issue=11 |pages=38–53}}
</ref>

<ref name="ODNB">
{{citation |last1=Simpson |first1=Jacqueline |last2=Roud |first2=Steve |contribution=Green Children |title=A Dictionary of English Folklore |url=http://www.oxfordreference.com/views/ENTRY.html?subview=Main&entry=t71.e435 |year=2000 |edition=online |publisher=Oxford University Press |accessdate=5 April 2009}} {{subscription required}}
</ref>

<ref name="Oman">
{{citation |last=Oman |first=C. C. |title=The English Folklore of Gervase of Tilbury |journal=[[Folklore Society|Folklore]] |year=1944 |volume=55 |issue=1 |pages=2–15 |jstor=1257623 |doi=10.1080/0015587X.1944.9717702}}
</ref>

<ref name="Orme">
{{citation |last=Orme |first=Nicholas |title=The Culture of Children in Medieval England |journal=[[Past & Present (journal)|Past & Present]] |year=1995 |volume=148 |issue=1 |pages=48–88 |jstor=651048 |doi=10.1093/past/148.1.48}}
</ref>

<ref name="Smith">
{{citation |last=Smith |first=Dinitia |title=Foundlings Wrapped in a Green Mystery |url=https://query.nytimes.com/gst/fullpage.html?res=9803E3DB1738F93BA25750C0A9649C8B63 |accessdate=3 March 2011 |newspaper=[[The New York Times]] |date=18 March 2002}}
</ref>

<ref name="Walsh">
{{citation |last=Walsh |first=Martin W. |title=Medieval English Martinmesse: The Archaeology of a Forgotten Festival |journal=[[Folklore Society|Folklore]] |year=2000 |volume=111 |issue=2|pages=231–254|jstor=1260605 |doi=10.1080/00155870020004620}}
</ref>
}}

'''Bibliography'''
{{refbegin}}
*{{citation |last=Baughman |first=E. W. |title=Type and Motif-Index of the Folktales of England and North America |year=1966 |publisher=Mouton}}
*{{citation |last=Brewer |first=Derek |contribution=The Colour Green |title=A Companion to the Gawain-Poet |editor1-last=Brewer |editor1-first=Derek |editor2-last=Gibson |editor2-first=Jonathan |year=1998 |publisher=D. S. Brewer |pages=181–190 |isbn=978-0-85991-433-8}}
*{{citation |last=Briggs |first= K. M. |authorlink=Katharine Mary Briggs |title=The Fairies in English Tradition and Literature |year=1967 |publisher=Routledge and Kegan Paul |isbn=978-0-415-29151-4}}
*{{citation |last=Cohen |first=Jeffrey Jerome |contribution=Green Children from Another World, or the Archipelago in England |title=Cultural Diversity in the British Middle Ages: Archipelago, Island, England |editor1-last=Cohen |editor1-first=Jeffrey Jeremy |series=The New Middle Ages |year=2008 |publisher=Palgrave |pages=75–94 |isbn=978-0-230-60326-4}}
*{{citation |last1=Cosman |first1=Pelner |last2=Jones |first2=Linda Gale |title=Handbook to Life in the Medieval World |year=2008 |publisher=Facts On File |isbn=978-0-8160-4887-8}}
*{{citation |last1=Fanthorpe |first1=Lionel |last2=Fanthorpe |first2=Patricia |title=The Big Book of Mysteries |year=2010 |publisher=Dundurn Group |isbn=978-1-55488-779-8}}
*{{citation |last=Harris |first=Paul |title=Fortean Studies: No. 4 |contribution=The Green Children of Woolpit: A 12th Century Mystery and its Possible Solution |pages=81–95 |publisher=John Brown Publishing |year=1998 |isbn=978-1-870870-96-2 |editor-last=Moore |editor-first=Steve}}
*{{citation |last=Haughton |first=Brian |title=Hidden History: Lost Civilizations, Secret Knowledge, and Ancient Mysteries |year=2007 |publisher=New Page Books |isbn=978-1-56414-897-1}}
*{{citation |last=Partner |first=Nancy F.| title=Serious Entertainments: The Writings of History in Twelfth-Century England |year=1977 |publisher=University of Chicago Press |isbn=978-0-226-64763-0}}

{{refend}}

==Further reading==
*{{citation |last=Lunan |first=Duncan |year=2012 |title=Children from the Sky |publisher=Mutus Liber |isbn=978-1-908097-05-7 |ref=none}}

==External links==
*[http://brian-haughton.com/articles/green-children-of-woolpit/ The Mystery of the Green Children of Woolpit], an extensive article on the green children mystery on Brian Haughton's website.
*[https://books.google.com/books?id=MAzbyC6BPjwC&lpg=PA747&ots=n2vhoTWR73&dq=william%20of%20newburgh%20historia%20anglicana&pg=PA92#v=onepage&q&f=false De Viridibus Pueris], William of Newburgh's account from ''Historia Anglicana'' (Latin) (Google Books)

{{Use dmy dates|date=March 2011}}

{{featured article}}

[[Category:12th century in England]]
[[Category:Medieval legends]]
[[Category:Suffolk folklore]]
[[Category:Forteana]]
[[Category:Mid Suffolk]]
[[Category:Feral children]]
[[Category:People whose existence is disputed]]