{{Infobox NCAA conference tournament
| name               = ACC Men's Soccer Tournament
| optional_subheader = Conference Soccer Championship
| defunct            = 
| image              = [[File:ACC Men's Soccer Tournament Logo.jpg|200px]]
| caption            = ACC Men's Soccer Tournament Logo
| sport              = [[College soccer]]
| conference         = [[Atlantic Coast Conference]]
| number_of_teams    = 10
| format             = [[Single-elimination tournament]]
| current_stadium    = [[Alumni Stadium (Notre Dame)|Alumni Stadium]]
| current_location   = [[Notre Dame, Indiana]]
| years              = 1987–present
| most_recent        = 
| current_champion   = [[Syracuse Orange men's soccer|Syracuse]]
| most_championships = [[Virginia Cavaliers|Virginia]] (10)
| television         = [[ESPN3]], [[ESPNU]]
| website            = [http://www.theacc.com/sports/m-soccer/acc-m-soccer-body.html theACC.com]
| sponsors           = 
| all_stadiums       = <!-- A list of all stadiums in which the tournament has been played -->
| all_locations      = <!-- A list of all locations in which the tournament has been played -->
}}

The '''ACC Men's Soccer Tournament''' is the conference championship tournament in [[soccer]] for the Atlantic Coast Conference (ACC).  The tournament has been held every year since 1987.  It is a [[single-elimination tournament]] and seeding is based on regular season records. The winner, declared conference champion, receives the conference's automatic bid to the [[NCAA Men's Division I Soccer Championship]].

==Champions==

=== Key ===

{| class="wikitable"
|-
| (2) 
| Title number
|-
|style="background-color:#FBCEB1"|*
|Match went to [[extra time]]
|-
|style="background-color:#cedff2"|†
|Match decided by a [[Penalty shootout (association football)|penalty shootout]] after extra time
|-
|'''Bold'''
|Winning team won '''regular season'''
|-
|bgcolor="#ddffdd"|^
|Winning team reached College Cup
|-
|bgcolor="#DDDDDD"|{{double-dagger}}
|Winning team lost National Championship
|-
|bgcolor="#FFEE99"|{{double-dagger}}
|Winning team won National Championship
|}

===By year===
{| class="wikitable sortable"
!Year
!Champion
!Score
!Runner-up
!Venue
!Tournament MVP
|-
| [[1987 ACC Men's Soccer Tournament|1987]]
| [[North Carolina Tar Heels men's soccer|North Carolina]]
| 4–3 (OT)
| [[NC State Wolfpack men's soccer|NC State]]
| [[Koskinen Stadium|Duke Soccer Stadium]] • [[Durham, North Carolina]]
| Derek Missimo <small>(UNC)</small>
|-
| 1988
| [[Virginia Cavaliers men's soccer|Virginia]]
| 2–1
| [[North Carolina Tar Heels men's soccer|North Carolina]]
| [[Riggs Field]] • [[Clemson, South Carolina]]
| ''none named''
|-
| 1989
| [[Wake Forest Demon Deacons men's soccer|Wake Forest]]
| 2–2 ([[Penalty shootout (association football)|PK]])
| [[NC State Wolfpack men's soccer|NC State]]
| [[Koskinen Stadium|Duke Soccer Stadium]] • [[Durham, North Carolina]]
| [[Neil Covone]] <small>(Wake)</small>
|-
| 1990
|bgcolor="#ddffdd"| [[NC State Wolfpack men's soccer|NC State]]
| 2–1
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[Koskinen Stadium|Duke Soccer Stadium]] • [[Durham, North Carolina]]
| [[Henry Gutierrez]] <small>(NC State)</small>
|-
| 1991
|bgcolor="#FFEE99"| [[Virginia Cavaliers men's soccer|Virginia]]{{double-dagger}}
| 3–1
| [[Wake Forest Demon Deacons men's soccer|Wake Forest]]
| [[Fetzer Field]] • [[Chapel Hill, North Carolina]]
| [[Claudio Reyna]] <small>(Virginia)</small>
|-
| 1992
|bgcolor="#FFEE99"| [[Virginia Cavaliers men's soccer|Virginia]]{{double-dagger}}
| 4–2
| [[Clemson Tigers men's soccer|Clemson]]
| [[Fetzer Field]] • [[Chapel Hill, North Carolina]]
| [[Brad Agoos]] <small>(Virginia)</small>
|-
| 1993
|bgcolor="#FFEE99"| [[Virginia Cavaliers men's soccer|Virginia]]{{double-dagger}}
| 2–1
| [[Clemson Tigers men's soccer|Clemson]]
| [[Fetzer Field]] • [[Chapel Hill, North Carolina]]
| [[Jaro Zawislan]] <small>(Clemson)</small>
|-
| 1994
|bgcolor="#FFEE99"| [[Virginia Cavaliers men's soccer|Virginia]]{{double-dagger}}
| 1–0
| [[Duke Blue Devils men's soccer|Duke]]
| [[Riggs Field]] • [[Clemson, South Carolina]]
| [[Mark Peters (American soccer)|Mark Peters]] <small>(Virginia)</small>
|-
| 1995
|bgcolor="#ddffdd"| [[Virginia Cavaliers men's soccer|Virginia]]^
| 1–0
| [[Clemson Tigers men's soccer|Clemson]]
| [[Koskinen Stadium|Duke Soccer Stadium]] • [[Durham, North Carolina]]
| [[Mike Fisher (soccer)|Mike Fisher]] <small>(Virginia)</small>
|-
| 1996
| [[Maryland Terrapins men's soccer|Maryland]]
| 2–0
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[Klöckner Stadium]] • [[Charlottesville, Virginia]]
| [[Pierre Venditti]] <small>(Maryland)</small>
|-
| 1997
|bgcolor="#DDDDDD"| [[Virginia Cavaliers men's soccer|Virginia]]{{double-dagger}}
| 2–0
| [[Maryland Terrapins men's soccer|Maryland]]
| [[Disney's Wide World of Sports]] • [[Orlando, Florida]]
| [[Ben Olsen]] <small>(Virginia)</small>
|-
| 1998
| [[Clemson Tigers men's soccer|Clemson]]
| 1–0
| [[Duke Blue Devils men's soccer|Duke]]
| [[W. Dennie Spry Soccer Stadium|Spry Soccer Stadium]] • [[Winston-Salem, North Carolina]]
| [[Josh Campbell]] <small>(Clemson)</small>
|-
| 1999
| [[Duke Blue Devils men's soccer|Duke]]
| 2–1 (OT)
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[W. Dennie Spry Soccer Stadium|Spry Soccer Stadium]] • [[Winston-Salem, North Carolina]]
| [[Troy Garner]] <small>(Duke)</small>
|-
| 2000
| [[North Carolina Tar Heels men's soccer|North Carolina]]
| 1–0 (OT)
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[W. Dennie Spry Soccer Stadium|Spry Soccer Stadium]] • [[Winston-Salem, North Carolina]]
| [[Caleb Norkus]] <small>(UNC)</small>
|-
| 2001
| [[Clemson Tigers men's soccer|Clemson]]
| 2–1
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[Riggs Field]] • [[Clemson, South Carolina]]
| [[Ian Fuller]] <small>(Clemson)</small>
|-
| 2002
|bgcolor="#ddffdd"| [[Maryland Terrapins men's soccer|Maryland]]^
| 3–0
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[SAS Soccer Complex]] • [[Cary, North Carolina]]
| [[Abe Thompson]] <small>(Maryland)</small>
|-
| 2003
| [[Virginia Cavaliers men's soccer|Virginia]]
| 1–1 (7–6 PK)
|bgcolor="#ddffdd"| [[Maryland Terrapins men's soccer|Maryland]]^
| [[SAS Soccer Complex]] • [[Cary, North Carolina]]
| [[Ryan Burke]] <small>(Virginia)</small>
|-
| 2004
| [[Virginia Cavaliers men's soccer|Virginia]]
| 2–1
|bgcolor="#ddffdd"| [[Maryland Terrapins men's soccer|Maryland]]^
| [[SAS Soccer Complex]] • [[Cary, North Carolina]]
| [[Jeremy Barlow]] <small>(Virginia)</small>
|-
| 2005
| [[Duke Blue Devils men's soccer|Duke]]
| 0–0 (5–4 PK)
| [[North Carolina Tar Heels men's soccer|North Carolina]]
| [[SAS Soccer Complex]] • [[Cary, North Carolina]]
| [[Blake Camp]] <small>(Duke)</small>
|-
| 2006
| [[Duke Blue Devils men's soccer|Duke]]
| 1–0 (OT)
|bgcolor="#ddffdd"| [[Wake Forest Demon Deacons men's soccer|Wake Forest]]^
| [[Maryland SoccerPlex]] • [[Germantown, Maryland]]
| [[Michael Videira]] <small>(Duke)</small>
|-
| 2007
| [[Boston College Eagles men's soccer|Boston College]]
| 2–1
|bgcolor="#FFEE99"| [[Wake Forest Demon Deacons men's soccer|Wake Forest]]{{double-dagger}}
| [[SAS Soccer Complex]] • [[Cary, North Carolina]]
| [[Sherron Manswell]] <small>(BC)</small>
|-
| 2008
|bgcolor="#FFEE99"| [[2008 Maryland Terrapins men's soccer team|Maryland]]{{double-dagger}}
| 1–0
| [[Virginia Cavaliers men's soccer|Virginia]]
| [[WakeMed Soccer Park]] • [[Cary, North Carolina]]
| [[Jeremy Hall (soccer)|Jeremy Hall]] <small>(Maryland)</small>
|-
| 2009
|bgcolor="#FFEE99"| [[Virginia Cavaliers men's soccer|Virginia]]{{double-dagger}}
| 1–0
| [[NC State Wolfpack men's soccer|NC State]]
| [[WakeMed Soccer Park]] • [[Cary, North Carolina]]
| [[Diego Restrepo]] <small>(Virginia)</small>
|-
| 2010
| [[Maryland Terrapins men's soccer|Maryland]]
| 1–0
| [[North Carolina Tar Heels men's soccer|North Carolina]]
| [[WakeMed Soccer Park]] • [[Cary, North Carolina]]
| [[Zac MacMath]] <small>(Maryland)</small>
|-
| [[2011 ACC Men's Soccer Tournament|2011]]
|bgcolor="#FFEE99"| [[2011 North Carolina Tar Heels men's soccer team|North Carolina]]{{double-dagger}}
| 3-1
| [[Boston College Eagles men's soccer|Boston College]] 
| [[WakeMed Soccer Park]] • [[Cary, North Carolina]]
| [[Ben Speas]] <small>(North Carolina)</small>
|-
| [[2012 ACC Men's Soccer Tournament|2012]]
|bgcolor="#ddffdd"| [[Maryland Terrapins men's soccer|Maryland]]^
| 2-1
| [[North Carolina Tar Heels men's soccer|North Carolina]] 
| [[Maryland SoccerPlex]] • [[Germantown, Maryland]]
| [[Patrick Mullins (soccer)|Patrick Mullins]] <small>(Maryland)</small>
|- 
| [[2013 ACC Men's Soccer Tournament|2013]]
|bgcolor="#DDDDDD"| [[2013 Maryland Terrapins men's soccer team|Maryland]]{{double-dagger}}
| 1-0
|bgcolor="#ddffdd"| [[2013 Virginia Cavaliers men's soccer team|Virginia]]^ 
| [[Maryland SoccerPlex]] • [[Germantown, Maryland]]
| [[Patrick Mullins (soccer)|Patrick Mullins]] <small>(Maryland)</small>
|- 
| [[2014 ACC Men's Soccer Tournament|2014]]
| [[Clemson Tigers men's soccer|Clemson]]
| 2-1 (OT)
| [[Louisville Cardinals men's soccer|Louisville]] 
| [[WakeMed Soccer Park]] • [[Cary, North Carolina]]
| [[Paul Clowes]] <small>(Clemson)</small>
|- 
| [[2015 ACC Men's Soccer Tournament|2015]]
|bgcolor="#ddffdd"| [[2015 Syracuse Orange men's soccer team|Syracuse]]^
| 1-0
| [[Notre Dame Fighting Irish men's soccer|Notre Dame]] 
| [[Alumni Stadium (Notre Dame)|Alumni Stadium]] • [[Notre Dame, Indiana]]
| [[Ben Polk]] <small>(Syracuse)</small>
|- 
| [[2016 ACC Men's Soccer Tournament|2016]]
|bgcolor="#DDDDDD"|[[2016 Wake Forest Demon Deacons men's soccer team|Wake Forest]]{{double-dagger}}
| 3-1
|[[2016 Clemson Tigers men's soccer team|Clemson]]
| [[MUSC Health Stadium]] • [[Charleston, South Carolina]]
| [[Ian Harkes]] (Wake Forest)
|}

===By school===
''Thru 2016''
{| class="wikitable sortable"
|-align=center
!align=left|School
! {{tooltip|Apps|Appearances}}
! W
! L
! T
! Pct
! Titles
! Title Years
|-align=center
|align=left| Boston College
| 10
| 8
| 8
| 0
| {{Winning percentage|8|8|0}}
| 1
|align=left| 2007
|-align=center
|align=left| Clemson
| 29
| 18
| 21
| 5
| {{Winning percentage|18|21|5}}
| 3
|align=left| 1998, 2001, 2014
|-align=center
|align=left| Duke
| 29
| 19
| 23
| 4
| {{Winning percentage|19|23|4}}
| 3
|align=left| 1999, 2005, 2006
|-align=center
|align=left|  Louisville 
| 2
| 3
| 2 
| 0
| {{Winning percentage|3|2|0}}
| 0 
|align=left| 
|-align=center
|align=left| Maryland
| 27
| 28
| 19
| 2
| {{Winning percentage|28|19|2}}
| 6
|align=left| 1996, 2002, 2008, 2010, 2012, 2013
|-align=center
|align=left| NC State
| 29
| 13
| 23
| 6
| {{Winning percentage|13|22|6}}
| 1
|align=left| 1990
|-align=center
|align=left| North Carolina
| 29
| 23
| 20
| 6
| {{Winning percentage|23|20|6}}
| 3
|align=left| 1987, 2000, 2011
|-align=center
|align=left| Notre Dame
| 2
| 1
| 1
| 1
| {{Winning percentage|2|1|1}}
| 0
|align=left| 
|-align=center
|align=left| Pittsburgh 
| 0
| 0
| 0
| 0
| {{Winning percentage|0|0|0}}
| 0
|align=left| 
|-align=center
|align=left| Syracuse
| 2 
| 4
| 1
| 1
| {{Winning percentage|4|1|1}}
| 1
|align=left| 2015
|-align=center
|align=left| Virginia
| 29
| 39
| 17
| 7
| {{Winning percentage|39|17|7}}
| 10
|align=left| 1988, 1991, 1992, 1993, 1994, 1995, 1997, 2003, 2004, 2009
|-align=center
|align=left| Virginia Tech
| 11
| 2
| 10
| 0
| {{Winning percentage|2|10|0}}
| 0
|align=left|
|-align=center
|align=left| Wake Forest
| 29
| 17
| 25
| 9
| {{Winning percentage|17|25|9}}
| 1
|align=left| 1989, 2016
|}

Florida State, Georgia Tech, and Miami do not sponsor men's soccer.

===Pre-tournament champions===
Prior to 1987, the champion was determined based on regular season play.

{| class="wikitable sortable"
! Season
! Champion
! Runner-up
|-
! 1953
| Maryland
| Duke
|-
! 1954
| Maryland
| North Carolina
|-
! 1955
| Maryland
| North Carolina
|-
! 1956
| Maryland
| Virginia
|-
! 1957
| Maryland
| Virginia
|-
! 1958
| Maryland
| North Carolina
|-
! 1959
| Maryland
| North Carolina
|-
! 1960
| Maryland
| Duke
|-
! 1961
| Maryland
| Duke
|-
! 1962
| Maryland
| North Carolina
|-
! 1963
| Maryland
| Virginia
|-
! 1964
| Maryland
| North Carolina
|-
! 1965
| Maryland
| North Carolina
|-
! 1966
| Maryland<br />North Carolina
| —
|-
! 1967
| Maryland
| North Carolina
|-
! 1968
| Maryland
| North Carolina
|-
! 1969
| Virginia
| Maryland
|-
! 1970
| Virginia
| Maryland
|-
! 1971
| Maryland
| Duke
|-
! 1972
| Clemson
| Duke
|-
! 1973
| Clemson
| Maryland
|-
! 1974
| Clemson
| Maryland
|-
! 1975
| Clemson
| North Carolina
|-
! 1976
| Clemson
| Maryland
|-
! 1977
| Clemson
| North Carolina
|-
! 1978
| Clemson
| North Carolina
|-
! 1979
| Clemson
| North Carolina<br />Virginia
|-
! 1980
| Duke
| Clemson<br />NC State
|-
! 1981
| Clemson
| Duke
|-
! 1982
| Clemson<br />Duke
| —
|-
! 1983
| Virginia
| Duke
|-
! 1984
| Virginia
| Clemson<br />NC State
|-
! 1985
| Clemson
| Virginia
|-
! 1986
| Virginia
| NC State
|}

==References==
*{{cite web|title=ACC Men's Soccer Annual Champions |url=http://grfx.cstv.com/photos/schools/acc/sports/m-soccer/auto_pdf/acc-records.pdf |publisher=Atlantic Coast Conference |accessdate=31 May 2011 |archiveurl=http://www.webcitation.org/5z5G2eTuF?url=http%3A%2F%2Fgrfx.cstv.com%2Fphotos%2Fschools%2Facc%2Fsports%2Fm-soccer%2Fauto_pdf%2Facc-records.pdf |archivedate=31 May 2011 |page=51 |deadurl=no |df= }}
*{{cite web|title=2010 ACC Men's Soccer Championship |url=http://www.theacc.com/championships/10-mens-soccer-championship.html |publisher=Atlantic Coast Conference |accessdate=1 June 2011 |archiveurl=http://www.webcitation.org/5z6uoDA53?url=http%3A%2F%2Fwww.theacc.com%2Fchampionships%2F10-mens-soccer-championship.html |archivedate=1 June 2011 |deadurl=no |df= }}

{{NCAA men's college soccer tournament navbox}}
{{Atlantic Coast Conference men's soccer navbox}}
{{Atlantic Coast Conference championships navbox}}

[[Category:ACC Men's Soccer Tournament| ]]
[[Category:NCAA Division I men's soccer conference tournaments]]