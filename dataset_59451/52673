{{Infobox journal
| title = Psychology of Violence 
| cover = [[File:Psychology_of_Violence_journal_cover.gif]]
| editor = Sherry Hamby
| discipline = [[Psychology]]
| abbreviation = Psychol. Viol.
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Quarterly
| history = 2010-present
| openaccess = 
| license =
| impact = 2.368
| impact-year = 2014
| website = http://www.apa.org/pubs/journals/vio/index.aspx
| link1 = http://content.apa.org/journals/vio
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 469106680
| LCCN = 2009208054
| CODEN =
| ISSN = 2152-0828
| eISSN = 2152-081X
}}
'''''Psychology of Violence ''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]]. It was established in 2010 and covers research on "identifying the causes of violence from a psychological framework, finding ways to prevent or reduce violence, and developing practical interventions and treatments".<ref>{{cite web |date=26 November 2012 | url=http://www.apa.org/pubs/journals/vio/index.aspx |title=Psychology of Violence|publisher=[[American Psychological Association]] |accessdate=2012-11-26}}</ref> The current [[editor-in-chief]] is Sherry Hamby ([[Sewanee: The University of the South]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by the [[Social Science Citation Index]] and SCOPUS. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.368, ranking it 7th out of 40 journals in the category "Family Studies", 8th out of 55 in the category "Criminology & Penology" and 34th out of 119 in the category "Psychology, Clinical."<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2015-08-26 |series=Web of Science |postscript=.}}</ref>  

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.apa.org/pubs/journals/vio/index.aspx}}

[[Category:2010 establishments in the United States]]
[[Category:Abnormal psychology journals]]
[[Category:American Psychological Association academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2010]]
[[Category:Quarterly journals]]
[[Category:Violence journals]]