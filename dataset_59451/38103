{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image= 
|Ship image size= 
|Ship caption=
}}
{{Infobox ship career
|Ship country=United Kingdom
|Ship flag={{shipboxflag|United Kingdom|naval}}
|Ship name=''Rosamund''
|Ship namesake= 
|Ship ordered=15 March 1943
|Ship builder=[[Port Arthur Shipbuilding Company]], [[Port Arthur, Ontario]], [[Canada]]
|Ship laid down=26 April 1944
|Ship launched=20 December 1944
|Ship completed=10 July 1945
|Ship decommissioned=1947
|Ship honours=
|Ship identification=[[Pennant number]]: J439
|Ship fate=Sold to the [[South African Navy]], 1947
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=South Africa
|Ship flag={{shipboxflag|South Africa|naval-1952}}
|Ship renamed=''Bloemfontein'', mid-1948
|Ship namesake=[[Bloemfontein]]
|Ship nickname=
|Ship reclassified=As a [[training ship]], April 1961
|Ship acquired=Purchased from the [[Royal Navy]], 1947
|Ship fate=Sunk as a target, 5 June 1967
|Ship status=Diveable wreck
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption= (as built)
|Ship class={{sclass-|Algerine|minesweeper}}
|Ship displacement=*{{convert|950|LT|t|0|abbr=on}} ([[Displacement (ship)#Standard displacement|standard]])
*{{convert|1250|LT|t|0|abbr=on}} ([[deep load]])
|Ship length={{convert|225|ft|m|1|abbr=on}} [[Length overall|o/a]]
|Ship beam={{convert|35|ft|6|in|m|1|abbr=on}} 
|Ship draught={{convert|12|ft|3|in|m|1|abbr=on}} 
|Ship power=*2 × [[Admiralty 3-drum boiler]]s
*{{convert|2400|ihp|abbr=on|lk=in}} 
|Ship propulsion=*2 shafts
*2 × [[Marine steam engine#Triple or multiple expansion|vertical triple-expansion steam engines]] 
|Ship speed={{convert|16.5|kn|lk=in}}
|Ship range={{convert|5000|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
|Ship complement=85
|Ship sensors=*[[Type 271 radar|Type 271]] surface-search [[radar]]
*[[List of World War II British naval radar#Type 291|Type 291]] air-search radar
|Ship EW=
|Ship armament=*1 × [[QF 4 inch Mk V naval gun|QF {{Convert|4|in|mm|abbr=on|0}} Mk V]] [[Dual-purpose gun|DP gun]]
*2 × twin and 2 × single [[Oerlikon 20 mm cannon|Oerlikon {{convert|20|mm|1|abbr=on}}]] [[AA gun]]s
|Ship notes=
}}
|}
'''HMSAS ''Bloemfontein''''' was an {{sclass-|Algerine|minesweeper}} built for the [[Royal Navy]] in Canada during [[World War II]]. The ship was originally named ''Rosamund'' ([[pennant number]]: J439) and spent several years clearing [[minefield]]s in Europe after she was completed in 1945 before she was placed in [[Reserve fleet|reserve]]. ''Rosamund'' was purchased by [[South Africa]] in 1947 and later renamed HMSAS ''Bloemfontein''.

The ship spent most of its early career in the [[South African Navy]] training or making good-will visits to foreign countries. She was [[laid up]] in the late 1950s and was [[Ship commissioning|recommissioned]] in 1961 to serve as a interim [[training ship]] until the [[Stone frigate|shore-based training establishment]] then under construction was completed. This occurred in 1963 and ''Bloemfontein'' returned to briefly returned to reserve before she was deemed surplus to requirements. The ship was sunk as a target in 1967.

== Description ==
''Bloemfontein'' displaced {{convert|950|LT|t|0}} at [[Displacement (ship)|standard load]] and {{convert|1250|LT|t|0}} at [[deep load]].<ref name=d7/> The ship had an [[length overall|overall length]] of {{convert|225|ft|m|1}}, a [[beam (nautical)|beam]] of {{convert|35|ft|6|in|m|1}} and a deep [[draft (hull)|draught]] of {{convert|12|ft|3|in|m|1}}. She was powered by a pair of [[Marine steam engine#Triple or multiple expansion|vertical triple-expansion steam engines]], each driving one propeller shaft, using steam provided by two [[Admiralty three-drum boiler]]s. The engines developed a total of {{convert|2400|ihp|lk=in}} which gave a maximum speed of {{convert|16.5|kn|lk=in}}.<ref name=l1/> The ship carried {{convert|230|LT|t|0}} of [[fuel oil]] that she had a range of {{convert|5000|nmi|lk=in}} at {{convert|10|kn}}. The ship was armed with a single [[QF 4 inch Mk V naval gun|four-inch (102&nbsp;mm) Mk V]] [[dual-purpose gun]] and two twin and two single mounts for [[Oerlikon 20 mm cannon|Oerlikon {{convert|20|mm|1|adj=on}}]] light [[anti-aircraft gun|anti-aircraft (AA) guns]]. For [[anti-submarine warfare|anti-submarine work]], ''Bloemfontein'' was fitted with two [[depth charge]] rails, and four depth charge throwers for 92 depth charges.<ref name=d7>du Toit, p. 187</ref> The ship was also equipped with a [[Type 271 radar|Type 271]] surface-search [[radar]] and a [[List of World War II British naval radar#Type 291|Type 291]] air-search radar. Her crew numbered 85 officers and [[naval rating|ratings]].<ref name=l1>Lenton, p. 261</ref>

==Construction and career==
''Bloemfontein'' was ordered on 15 March 1943 from the [[Port Arthur Shipbuilding Company]] of [[Port Arthur, Ontario]], Canada, and [[laid down]] on 26 April 1944 with the name of ''Rosamund''.<ref name=l4/> The ship was the first of her name to serve in the Royal Navy.<ref>Colledge, p. 298</ref> She was [[Ceremonial ship launching|launched]] on 20 December and completed six months later on 10 July 1945.<ref name=l4>Lenton, p. 264</ref> The ship was assigned to clear the coastal waters of Western Europe of minefields laid during the war and did so until she was laid up in 1947 at [[Devonport Royal Dockyard]]. ''Rosamund'' was purchased by the South African Navy later that year, together with her [[sister ship]], {{HMS|Pelorus|J291|2}}. The sisters departed England on 22 November after a refresher course at the minesweeping school at [[HMS Lochinvar (shore establishment)|HMS ''Lochinvar'']], [[Port Edgar]], Scotland. They arrived at [[Cape Town]] on 24 December, making stops at [[Gibraltar]], [[Freetown]] and [[Walvis Bay]] en route.<ref>du Toit, p. 183</ref> 

''Rosamund'' was rechristened as ''Bloemfontein'' in [[East London, Eastern Cape|East London]] during that city's centenary celebrations in mid-1948. In August she made her first supply run to [[Marion Island]]. In November of that year, the sisters exercised with the British 3rd Aircraft Carrier Squadron. Later that month, together with the [[frigate]] {{HMSAS|Natal|K10|2}}, they visited ports in [[Portuguese Mozambique]], returning to [[Durban]] on 12 December. The sisters were placed in reserve in the late 1950s, after the navy had purchased 10 {{sclass2-|Ton|minesweeper|2}}s. ''Bloemfontein'' was recommissioned in April 1961 to serve as a stationary training ship in [[Simon's Town]] until the navy's training facility [[List of South African military bases|SAS ''Simonsberg'']] was completed in July 1963. The navy decided that the ship was no longer needed and she was stripped of useful equipment before being sunk as a target in [[False Bay]] by the frigate {{SAS|President Kruger|F150|2}} and the minesweeper {{SAS|Johannesburg}} on 5 June 1967.<ref>du Toit, p. 184</ref>

==Citations==
{{reflist|30em}}

==References==
*{{colledge}}
*{{cite book|last1=Du Toit|first1=Allan|authorlink=Allan du Toit|title=South Africa's Fighting Ships: Past and Present|date=1992|publisher=Ashanti Publishing|location=Rivonia, South Africa|isbn=1-874800-50-2}}
*{{cite book|last1=Gardiner|first1=Robert|last2=Chumbley|first2=Stephen|last3=Budzbon|first3=Przemysław|title=Conway's All the World's Fighting Ships 1947–1995|year=1995|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=1-55750-132-7|lastauthoramp=y}}
* {{cite book|last=Lenton|first=H. T.|title=British & Empire Warships of the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1998|isbn=1-55750-048-7}}

==External links==
*[http://www.navy.mil.za/aboutus/history/ambassadors.htm Flag-showing cruises of the South African Navy]
*[[voy:Diving the Cape Peninsula and False Bay/SAS Bloemfontein|Dive site article for the SAS Bloemfontein]] on [[Wikivoyage]]

{{Algerine class minesweepers|others}}

{{DEFAULTSORT:Bloemfontein}}
[[Category:Algerine-class minesweepers of the Royal Navy]]
[[Category:1944 ships]]
[[Category:World War II minesweepers of the United Kingdom]]
[[Category:Algerine-class minesweepers of the South African Navy]]