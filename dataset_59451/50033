{{Update|date=August 2015}}
{{Infobox video game
| title         = Game of Thrones: Seven Kingdoms
| developer     = [[Bigpoint]]<br />Artplant
| publisher     = Bigpoint
| engine        = [[Unity (game engine)|Unity]]
| genre         = [[Massively multiplayer online role-playing game]]
| modes         = [[Multiplayer video game|Multiplayer]]
}}

'''''Game of Thrones: Seven Kingdoms''''' is a [[fantasy]] [[massively multiplayer online role-playing game]] (MMORPG), currently under development by [[video game developer]]s [[Bigpoint]] and Artplant, in collaboration with [[HBO]].<ref name="pressrelease">{{cite web|url=http://bigpoint.net/2012/02/bigpoint-presents-game-of-thrones-mmorpg-at-game-developers-conference/ |title=presents Game of Thrones MMORPG at Game Developers Conference |publisher=Bigpoint |date=2012-02-29 |accessdate=2012-12-01}}</ref> The game is based on the HBO TV series, ''[[Game of Thrones]]'', which is itself an adaptation of the ''[[A Song of Ice and Fire]]'' book series by [[George R. R. Martin]].

The game will be built atop the [[Unity (game engine)|Unity]] platform, and will be playable in the browser using the Unity Web Player plugin.<ref name="pressrelease"/>

==Gameplay==
''Game of Thrones: Seven Kingdoms'' is set within the fictional realm of Westeros,<ref name="pressrelease" /> and will use a third-person viewpoint. Gameplay will mainly be based around player vs player (PvP) combat, which will involve small group combat, one on one duels and siege battles, large scale battles in which players must capture keeps, forts and castles.<ref name="mmorpg">{{cite web|url=http://www.mmorpg.com/gamelist.cfm/game/790/feature/6418/Game-of-Thrones-Interview-with-Max-Pfaff.html |title=Game of Thrones: Seven Kingdoms (GoT) Interviews: Interview with Max Pfaff |publisher=MMORPG.com |date=2012-05-21 |accessdate=2012-12-01}}</ref> Player vs Environment (PvE) combat will be available at launch, although this is not a major priority.<ref>{{cite web|url=http://www.videogamer.com/news/game_of_thrones_from_novel_to_tv_series_to_mmo.html |title=Game of Thrones: From novel to TV series to MMO |publisher=VideoGamer.com |date= |accessdate=2012-12-01}}</ref> Combat will be realtime, with movement controlled using the WASD keys.<ref name="massively">{{cite web|author= |url=http://massively.joystiq.com/2012/03/09/gdc-2012-bigpoint-discusses-the-game-of-thrones-mmo/ |title=GDC 2012: Bigpoint discusses the Game of Thrones MMO &#124; Massively |publisher=Massively.joystiq.com |date=2012-03-09 |accessdate=2012-12-01}}</ref>

Players will be able to come together to form guilds, known as lesser houses. Lesser houses align themselves with one of the three major houses in-game: Baratheon, Stark or Lannister.<ref name="massively" /> Solo play will be possible, although group play will be encouraged.<ref name="now" />

Players will be able to customize their characters by changing features such as hair and clothing.<ref name="massively" />

==Development==
''Game of Thrones: Seven Kingdoms'' was announced in February 2012, with the first look being unveiled at the [[Game Developers Conference]] in March 2012.<ref name="pressrelease" /> The first gameplay footage was shown in a trailer released on July 12, 2012.<ref name="trailer">{{cite web|url=https://www.youtube.com/watch?v=11EVQcCsEbE&list=UUb7zo |title=Game of Thrones: Seven Kingdoms MMO Official Trailer &#124; Bigpoint HBO 2012 |publisher=YouTube |date=2012-07-12 |accessdate=2012-12-01}}</ref>

The game is under development by German videogame developer [[Bigpoint]], and Norwegian developer Artplant. The studios have previously worked together on [[Battlestar Galactica Online]], another browser based multiplayer game.

The game is being made in collaboration with [[HBO]]. Everything put in game is reviewed by HBO in advance,<ref name="now">[http://mvsnap.com/tv-shows/game-of-thrones-season-00]{{dead link|date=December 2012}}</ref> and Bigpoint is working with the HBO design teams. The look and feel of the weapons and armour, for example, are straight from the show.<ref>{{cite web|url=http://www.mmorpg.com/gamelist.cfm/game/790/feature/6167/Game-of-Thrones-GDC-2012-Game-of-Thrones-Goes-Sandbox-Warfare.html |title=Game of Thrones: Seven Kingdoms (GoT) Previews: GDC 2012: Game of Thrones Goes Sandbox Warfare |publisher=MMORPG.com |date=2012-03-08 |accessdate=2012-12-01}}</ref>

The game is being built on the Unity platform.

==References==
{{Reflist}}

==External links==
*[http://www.gameofthronesmmo.com Official website (defunct)]

{{ASOIAF}}

[[Category:Game of Thrones|Seven Kingdoms]]
[[Category:Browser-based multiplayer online games]]
[[Category:Video games based on A Song of Ice and Fire]]
[[Category:Unity (game engine) games]]