{{Infobox officeholder
| name                = Natalio Cabili Ecarma III
| image               = [[File:Natalio Ecarma III.jpg|200px|Natalio Ecarma UN Official Portrait]]
| imagesize           = 
| smallimage          =
|-
| order               =
| president           = [[Benigno Aquino III|Benigno S. Aquino III]]
| office              = [[Department of National Defense (Philippines)|Undersecretary of National Defense for Defense Operations, Department of National Defense (Philippines)]]
| term_start          = August 2013
| term_end            =June 30, 2016
| succeeding          =
| predecessor         = [[Honorio Azcueta]]
| successor           =
|-
| order2               =
| office2              = [[United Nations Disengagement Observer Force|17th Head of Mission and Force Commander, United Nations Disengagement Observer Force]]
| term_start2          = March 10, 2010
| term_end2            = August 13, 2013
| secretary general   = [[Ban Ki-Moon]]
| succeeding2          =
| predecessor2         = [[Wolfgang Jilke]] (Austria)
| successor2           = [[Iqbal Singha]] (India)
|-
| office3              = [[Philippine Marine Corps|Deputy Commandant, Philippine Marine Corps]]
| term_start3          = August 16, 2008
| term_end3            = January 2010
| succeeding3          =
| predecessor3         = 
| successor3           = 
|-
| birth_name          = Natalio Cabili Ecarma III
| birth_date          = {{birth date and age|1955|06|03}}
| birth_place         = [[Manila, Philippines|Manila]], Philippines
| death_date          =
| death_place         =
| nationality         = [[Philippines|Filipino]]
| party               =
| spouse              = Beverly R. Antonio, MD
| relations           =
| children            = Nathan Charles Ecarma, Nathan Christopher Ecarma
| residence           = Manila, Philippines
| alma_mater          = [[Philippine Military Academy]]
| occupation          =
| profession          = Government Official
Retired Military Officer
| religion            = Roman Catholic
| signature           = 
| website             = http://www.dnd.gov.ph
| footnotes           =
|allegiance= {{flagicon|PHI}} [[Republic of the Philippines]]
|branch= [[Philippine Navy]] [[Philippine Marine Corps|(Marine Corps)]]
|serviceyears= 1981 - 2012
|rank= {{nowrap|[[File:PHIL ARMY MGEN FD-Sh.svg|30px|Major General]] [[General officer|Major General]]}}
|awards= [[File:PHL Distinguished Service Star BAR.png|36px]] [[Distinguished Service Star]]<br/> [[File:PHL Bronze Cross.gif|35px]] [[Bronze Cross Medal]]<br /> [[File:PHL Military Merit Medal.gif|35px]] [[Military Merit Medal (Philippines)|Military Merit Medal]]<br /> [[File:PHL Military Commendation.gif|35px]] [[Military Commendation Medal]]<br /> [[File:PHL Military Civic Action Medal.gif|35px]] [[Military Civic Action Medal]]
}}
'''Natalio Cabili Ecarma III''' (born June 3, 1955) is a Filipino [[General]] who recently served as [[Undersecretary]] of the [[Department of National Defense (Philippines)|Department of National Defense]] of the [[Republic of the Philippines]].<ref>{{cite web|title=Department of National Defense Directory|url=http://www.pcoo.gov.ph/dir-dnd.htm|accessdate=1 January 2014}}</ref><ref>{{cite news|last=Porcalla|first=Delon|title=Former Peacekeeping Chief Named to DND|url=http://www.philstar.com/headlines/2013/09/02/1161671/former-peacekeeping-chief-named-dnd|newspaper=The Philippine Star|date=September 2, 2013|archivedate=September 2, 2013|location=Headlines}}</ref><ref>{{cite news|last=Joyce Panco|first=Panares|title=New Aquino Appointments Bared|url=http://manilastandardtoday.com/2013/09/02/new-aquino-appointments-bared/|newspaper=Manila Standard|date=September 2, 2013}}</ref> He is known for being the first Filipino appointed by UN Secretary General [[Ban Ki-Moon]], to serve as [[Head of Mission]] and Force Commander of the [[United Nations Disengagement Observer Force]] in the [[Golan Heights]],<ref>{{cite news|title=Secretary-General appoints Major General Natalio C. Ecarma of Philippines Head of Mission, Force COmmander of United Nations Disengagement Observer Force|url=https://www.un.org/News/Press/docs/2010/sga1218.doc.htm|date=February 1, 2010|author=United Nations Department of Public Information|author2=News and Media Division|agency=United Nations Organization|archivedate=February 1, 2010|location=Secretary General}}</ref> giving him the distinction of being the first Filipino to head a United Nations Peacekeeping Mission, with the rank of UN [[Assistant Secretary General]].<ref name=undof>{{cite news|last=Farolan|first=Ramon|title=UNDOF's Ecarma|url=http://opinion.inquirer.net/6983/undof%E2%80%99s-ecarma|newspaper=Philippine Daily Inquirer|date=June 27, 2011|archivedate=June 27, 2011|location=Opinion}}</ref><ref name="Tindog">{{cite web|last=Tindog|first=Cheryl|title=Retired Marine General Recalled for UN Peacekeeping Command|url=http://philippinemarinecorps.blogspot.com/2011/06/retired-marine-general-recalled-for-un.html|work=philippinemarinecorps.blogspot.com|publisher=Philippine Marine Corps|accessdate=July 8, 2012|archivedate=June 23, 2011}}</ref>

== Early Life and Family ==

Ecarma was born on June 3, 1955 to then Lieutenant Rodolfo A. Ecarma and Evelyn Cabili at the [[University of Santo Tomas]] Hospital in [[Manila]] and is the eldest of five children. He belongs to a distinguished line of military leaders and public servants.<ref>{{cite news|last=JMR|title=Ecarma is new Commander of UNDOF|url=http://www.sunstar.com.ph/manila/ecarma-new-commander-undof|newspaper=Sun Star Manila|date=February 13, 2010}}</ref> His maternal grandfather is the late [[Tomas Cabili|Senator Tomas Cabili]] of Iligan who served as [[Secretary of National Defense (Philippines)|Secretary of National Defense]] under President [[Sergio Osmeña]]. A hero of the guerrilla resistance movement during the Japanese occupation in World War II, Senator Cabili would die along with President [[Ramon Magsaysay]] in a plane crash on March 17, 1957 on Mt. Manunggal, Cebu.<ref name=undof/> His paternal grandfather, Col. Natalio Ecarma Sr., belongs to Class ’23 of the Philippine Constabulary (now [[Philippine National Police]]) Academy while his father, Brig. Gen. Rodolfo Ecarma, belongs to Class '54 of the [[Philippine Military Academy]] and retired as Chief of Air Staff of the [[Philippine Air Force]]. An uncle, Lt. Natalio Ecarma Jr., was a fighter pilot who flew Mustangs (P-51) in the 1950s.

== Education ==

Ecarma attended [[Ateneo De Manila University]], [[La Salle Academy (Iligan City)|De La Salle Iligan]] and [[De La Salle University]] in his younger years before deciding to pursue a career in the military. He later attained his Bachelor of Science degree at the [[Philippine Military Academy]] and graduated in 1981. He earned a [[Masters Degree]] in Military Studies at the US [[Marine Corps University]] in 2002 and in 2005, earned a [[National Defense College of the Philippines|Masters Degree in National Security Administration]] (MNSA) from the [[National Defense College of the Philippines]]. In the same year, he was granted a scholarship for a doctorate degree on Mainland China Studies at the [[National Sun Yat-sen University]] at [[Kaohsiung]], [[Taiwan]].

To further enhance his professional development as a civil servant, Ecarma took and passed the Career Executive Service Officer (CESO) Board exams given by the Career Executive Service Board of the [[Civil Service Commission]] last August 2011 and is currently CESO eligible (CESE).

== Military career ==

After graduating from the [[Philippine Military Academy]] in 1981 and completing the Naval Officer Qualifying Course, he became a member of the [[Philippine Marine Corps]]. He took the Scout Ranger Course and later, the [[Special Operations Command (Philippines)|Special Forces Operations Course]] of the [[Philippine Army]], excelling and graduating at the top of his class in both courses. Two years later in 1988, he completed and graduated from the [[Ranger School|U.S Army Ranger]] and [[Pathfinder (military)|Pathfinder]] Schools in Fort Benning, Georgia, U.S.A. A military SCUBA diver and [[Parachutist Badge (United States)|master parachutist]], he has had extensive experience in conducting hundreds of military freefall and static jumps.

Ecarma specializes in special operations, counter-insurgency and counter-terrorism. In 1995, then Major Ecarma created, organized and developed the special operations unit of the Philippine Marine Corps -the [[Philippine Marine Corps Force Recon Battalion|Force Reconnaissance Battalion]].<ref>{{cite news|last=Mangosing|first=Frances|title=Retired general disputes Querubin over test mission controversy|url=http://newsinfo.inquirer.net/416621/retired-general-disputes-querubin-over-test-mission-controversy|newspaper=Philippine Daily Inquirer|date=May 28, 2013|location=Nation}}</ref> This unit is now extensively used, not only by the Philippine Marine Corps, but by the Armed Forces of the Philippines (AFP) as well in special operations and counter-insurgency operations in Mindanao and other conflict areas of the Philippines.

In 1999, he became Battalion Commander of the Presidential Guards Battalion and Operations Officer of the [[Presidential Security Group]], making him responsible for protecting and securing two Philippine Presidents, numerous Heads of State and VIP’s both locally and internationally .

Ecarma has acquired extensive staff and line positions at all levels of command of the [[Philippine Marine Corps]]. Following his successful stint as Brigade Commander of the 3rd Marine Brigade in Patikul, Sulu in [[Mindanao]], he was appointed Deputy Commandant of the Philippine Marine Corps and concurrently Commanding General of all Marine Forces in the Southern Philippines.

=== Command Positions Held ===

Arranged in chronological order.

* '''Company Commander''', 61st Force Reconnaissance (Special Operations) Company 
* '''Director''', Marksmanship and Sniper Training Unit, Marine Corps Training Center 
* '''Battalion Commander''', Force Reconnaissance (Special Operations) Battalion 
* '''Assistant Chief of Staff for Intelligence''', Philippine Marine Corps 
* '''Battalion Commander''', Presidential Guards Battalion, Presidential Security Group
* '''Assistant Chief of Staff for Operations''', Presidential Security Group 
* '''Assistant Superintendent''', Marine Corps Training Center 
* '''Assistant Chief of Staff for Education and Training''', Philippine Marine Corps
* '''Chief of Staff''', Philippine Marine Corps 
* '''Brigade Commander''', 3rd Marine Brigade 
* '''Brigade Commander''', Combat and Service Support Brigade 
* '''Deputy Commandant''', Philippine Marine Corps and
* concurrent '''Commanding General''', Marine Forces Southern Philippines

=== Dates of Rank ===

=== Awards and Decorations ===

==== Medals ====

Ecarma is a recipient of 2 [[Distinguished Service Star]]s, 2 [[Bronze Cross Medal]]s, 17 [[Military Merit Medal (Philippines)|Military Merit Medals]], 6 [[Military Commendation Medal]]s, an Anti- Dissidence Campaign Medal, a Luzon Campaign Medal, a Visayan Campaign Medal, a Military Civic Action Medal and a Mindanao-Sulu Campaign Medal,

==== Badges ====

Ecarma’s badges include a [[Philippine Republic Presidential Unit Citation|Presidential Unit Citation Badge]], a General Staff Corps Badge, a Masters in National Security Administration Badge, a Flag Rank Command Badge, A Marine Silver Command Badge, a [[Combat Commander's Badge (Philippines)|Combat Commander’s Kagitingan Badge]], a US Ranger Tab, a Pistol Expert Badge, a Rifle Expert Badge, a US Army Honorary Parachutist Badge, a USA Pathfinder Badge, an [[AFP Parachutist Badge|AFP Master Parachutist Badge]], an [[AFP Election Duty Badge|Election Duty Badge]], several Marine Silver Command Badges and numerous Letters of Commendations and Plaques of Appreciation.

==== Other Awards ====

In January 2012, Ecarma was awarded the Outstanding Achievement Award for Prominently Meritorious Achievement by the Philippine Military Academy Alumni Association for having been appointed Head of Mission and Force Commander of a United Nations Peacekeeeping Mission. In November of the same year during the Philippine Marine Corps' Birthday, he received the Crossed Rifles and Anchor Award from Philippine Marine Corps Commandant Major General Rustico Guerrero for exemplifying the highest standards of military efficiency and soldierly values as Head of Mission and Force Commander of UNDOF.<ref>{{cite web|title=Marine Awardees for its 62nd Birthday|work=navy.mil.ph|publisher=Naval Public Affairs Office|archivedate=November 7, 2012|year=2012}}</ref>

== Peace and Development in Mindanao ==

Ecarma’s extensive experience in fighting communist insurgency in the Philippines helped him formulate his strategy for “winning the hearts and minds” Filipino Muslims in Sulu, a province in the island of Mindanao. In 2007, when he assumed as Brigade Commander of the 3rd Marine Brigade, he embarked on peace and development initiatives through inter-faith dialogue and activities to break the communication barrier and misperceptions that the military, Christians and Muslims had against each other. He required all men under his command, especially those who had just reported for duty in Sulu to undergo an “Understanding Islam and Tausug Culture” lecture conducted by a Muslim lecturer before reporting to their battalions. This was to ensure that his men understood their culture and would treat the civilians with respect. He reached out to all sectors of society, from religious leaders, politicians and local officials, to civil society organizations, engaging in both formal and informal dialogue in his quest for peace.

One of his major achievements towards achieving sustainable peace and equitable development was bringing over a Christian non-governmental Organization called [[Gawad Kalinga]] whose mandate is building sustainable communities based on good values for poor, homeless people. He successfully engaged almost all sectors of Sulu society to be involved in this inter-faith activity. This endeavor which happened for the first time in Sulu’s history proved that both Christians and Muslims desire peace and can work together regardless of religion, race or personal background.

Because of his peace and development initiatives in Sulu, various civil society organizations recommended him to the prestigious [[Asian Institute of Management]] to undergo a Bridging Leadership Fellowship program.<ref>{{cite web|title=Col. Natalio C. Ecarma III|url=http://bridgingleadership.aim.edu/alumni/bl-fellows-cohort-2/natalio-ecarma-iii|work=bridgingleadership.aim.edu|publisher=Asian Institute of Management|accessdate=June 3, 2010}}</ref> This program helped him form, systematize and ensure the sustainability of his peace and development projects through a multi-stakeholder co-ownership approach. This approach helped bridge the gap among various leaders of the different stakeholders in Sulu, making them work together to attain lasting peace in the area.

== United Nations ==

In February 2010, after a thorough selection process among numerous contenders of different nationalities,<ref>{{cite news|title=PGMA bids goodbye to Ecarma|url=http://balita.ph/2010/02/12/pgma-bids-goodbye-to-ecarma/|newspaper=balita.ph|date=February 12, 2010}}</ref> UN Secretary General [[Ban Ki Moon]] appointed him as the 17th [[Head of Mission]] and Force Commander of the [[United Nations Disengagement Observer Force]] (UNDOF) in the [[Golan Heights]]<ref>{{cite news|last=Balana|first=Cynthia|title=RP general heads peacekeeping forces in Golan Heights|url=http://globalnation.inquirer.net/news/breakingnews/view/20100121-248630/RP-general-heads-peacekeeping-forces-in-Golan-Heights|newspaper=Philippine Daily Inquirer|date=January 21, 2010|archivedate=January 21, 2010|location=Breaking News}}</ref> succeeding Major General [[Wolfgang Jilke]] of Austria.<ref>{{cite news|last=Felongco|first=Gilbert|title=New Chief for Peacekeeping Force|url=http://m.gulfnews.com/new-chief-for-peacekeeping-force-1.572610|newspaper=Gulf News|date=January 24, 2010|archivedate=January 24, 2010}}</ref> This made him the second Filipino after  [[Jaime de los Santos|Lt. Gen Jaime Delos Santos]] ([[United Nations Transitional Administration in East Timor|UNTAET]]) to be appointed Force Commander but the first Filipino in history to hold the position of [[Head of Mission]] of a [[United Nations peacekeeping|United Nations Peacekeeping Mission]] at the same time.<ref>{{cite news|last=Sayson|first=Celeste Frank|title=The Filipino Spirit: Promoting Peace Beyond Philippine Borders|url=http://www.army.mil.ph/e_publications/ATN%202011/3rd_qtr.pdf|accessdate=September 30, 2011|newspaper=Army Troopers News Magazine|date=September 2011|page=47}}</ref> He held the rank of UN [[Assistant Secretary General]] and was concurrently the [[Special Representative of the Secretary General]] (SRSG) to the area.

For nearly three years, he commanded a military peacekeeping force of six contingents composed of military peacekeepers contributed by Austria, India, Croatia, Canada, Japan and the Philippines and 78 Military Observers coming from numerous countries from the [[United Nations Truce Supervision Organization]] based in [[Jerusalem]]. He also supervised and managed civilian personnel coming from 29 different nations.

His responsibilities and duties as the first Filipino general in UNDOF<ref>{{cite news|last=Abella|first=Jerrie|title=Pinoy general to head peacekeeping force in Golan Heights|url=http://www.gmanetwork.com/news/story/182236/pinoyabroad/pinoy-general-to-head-peacekeeping-force-in-golan-heights|newspaper=GMA News|date=January 23, 2010|location=Pinoy Abroad}}</ref><ref>{{cite news|last=LBG|title=Pinoy peacekeepers in Golan Heights receive UN medals|url=http://www.gmanetwork.com/news/story/258715/pinoyabroad/pinoyachievers/pinoy-peacekeepers-in-golan-heights-receive-un-medals|newspaper=GMA News|date=May 19, 2012|archivedate=May 19, 2012}}</ref> extended from purely military operations to administration, management, and international diplomacy. These included frequent interactions and negotiations with the Ministries of Foreign Affairs of both [[Syria]] and [[Israel]], Ambassadors of the UN Security Council Permanent 5 (US, USSR, UK, China and France), Troop Contributing Countries of the UN and high-level civil and military officials of the United Nations, Israel, Syria, and other neighboring countries in the Middle East.

Ecarma was set to retire in June 2011. However, after being impressed with his performance in UNDOF, the United Nations requested the Philippine Government for an extension of his tour of duty.<ref name="Tindog"/><ref>{{cite news|last=Yap`|first=DJ|title=UN rehires retired Filipino general peacekeeping force commander|url=http://globalnation.inquirer.net/4771/un-rehires-retired-filipino-general-peacekeeping-force-commander|newspaper=Philippine Daily Inquirer|date=June 24, 2011|archivedate=June 24, 2011|location=Global Nation}}</ref> He served in the UN for another year before his term ended on August 13, 2012. Because of his experience, he was later on asked to be an adviser during the

{{s-start}}
{{succession box
| title=[[United Nations Disengagement Observer Force|Force Commander and Head of Mission of the United Nations Disengagement Observer Force]]
| before=[[Wolfgang Jilke]]
| after=[[Iqbal Singha]]
| years='''10 March 2010 –13 August 2012'''}}
{{s-end}}

== Department of National Defense ==

A year after his stint in the UN ended, he was called back to public service by President Aquino, this time as an [[Undersecretary]] of the [[Department of National Defense (Philippines)|Department of National Defense]].<ref>{{cite news|title=Appointments and Designations, August 23, 2013|url=http://www.gov.ph/2013/08/23/appointments-and-designations-august-23-2013/|newspaper=Republic of the Philippines Official Gazette|date=August 23, 2013}}</ref> As Undersecretary for Defense Operations, he advises and assists the Secretary of National Defense in formulating and implementing Department objectives and policies on matters pertaining to defense operations. His major duties include exercising functional supervision over the [[Armed Forces of the Philippines]], as well as the AFP Reserve Force, Citizen Armed Force, civilian volunteer organizations, and civil military operations. He also exercises functional supervision over matters concerning UN Peacekeeping Operations and Philippine Expeditionary Forces, Internal Security Operations and Counter Terrorism. Recently, he has been tasked with the responsibility of overseeing the security component of APEC Manila 2015 as its Deputy Director General for Security.<ref>{{cite news|last=Perez|first=Joseph John|title=APEC NOC assessment team, highly satisfied with Albay readiness to host APEC summit– Salceda|url=http://news.pia.gov.ph/index.php?article=2591393405114|accessdate=February 26, 2014|date=February 26, 2014|agency=Philippine Information Agency}}</ref><ref>{{cite news|last=Samillano|first=Chrysee|title=APEC Team impressed with city's preparedness|url=http://www.visayandailystar.com/2014/February/21/index.htm|accessdate=18 March 2014|newspaper=The Visayan Daily Star|location=Top Stories}}</ref><ref>{{cite news|last=Colina|first=Antonio IV|title=APEC Team impressed of Davao City|url=http://www.sunstar.com.ph/davao/business/2014/02/10/apec-team-impressed-davao-city-327664|accessdate=18 March 2014|newspaper=Sun Star Davao|date=February 10, 2014}}</ref><ref>{{cite news|last=Azue|first=Ranie|title=STRONG POINTS APEC-NOC Ocular Team cites strengths of Bacolod for Confab hosting readiness|url=http://www.watchmendaily.com/news/strong-points-apec-noc-ocular-team-cites-strengths-bacolod-confab-hosting-readiness/|newspaper=Watchmen Daily|date=February 21, 2014|location=News}}</ref> Last February 10, 2014, he was a guest at the formal opening of the Bangsamoro Transition Commission representing the Department of National Defense.<ref>{{cite web|last=CTP News|title=Top officials grace BTC office opening|url=http://afrim.org.ph/newafrim/tag/dnd-usec-natalio-ecarma-iii|work=alternate forum for research in Mindanao|publisher=Alternate Forum for Research in Mindanao|accessdate=February 10, 2014}}</ref>

== Personal life ==

Ecarma’s hobbies include reading and keeping fit by being involved in numerous sports. As a sports enthusiast, he is an avid shooter (combat and olympic events), and is also into skydiving, soccer, basketball, tennis, badminton, bowling, swimming, SCUBA diving and Pekiti-Tirsia Kali (Filipino Martial Arts).

Ecarma is married to the former Dr. Beverly Antonio. They have two sons: Nathan Charles and Nathan Christopher.<ref>{{cite web|title=Curriculum Vitae of Major General Natalio C. Ecarma III, Force Commander of United Nations Disengagement Observer Force (UNDOF)|url=http://www.un.org/ar/peacekeeping/missions/undof/documents/ecarma_bio.pdf|work=www.un.org|publisher=United Nations|accessdate=March 1, 2010}}</ref>

==See also==

*[[United Nations Disengagement Observer Force]]
*[[Special Representative of the Secretary General]]
*[[Department of National Defense (Philippines)]]
*[[United Nations Security Council Resolution 1934]]
*[[National Defense College of the Philippines]]
*[[AFP Peacekeeping Operations Center]]

== References ==

{{reflist}}

== External links ==

{{Commons category|Natalio C. Ecarma}}
*[http://www.dnd.gov.ph/transparency/dnd-officials-2.html/ Official Site of the Department of National Defense]
*[http://www.undof.unmissions.org/ Official Site of the United Nations Disengagement Observer Force]
*[http://www.un.org/ar/peacekeeping/missions/undof/documents/ecarma_bio.pdf/ UN Biography of Major General Natalio C. Ecarma III]
*[http://bridgingleadership.aim.edu/alumni/bl-fellows-cohort-2/natalio-ecarma-iii/ Asian Institute of Management Center for Bridging Leadership]

{{DEFAULTSORT:Ecarma, Natalio C., III}}
[[Category:Filipino generals]]
[[Category:Philippine Military Academy alumni]]
[[Category:Living people]]
[[Category:People from Manila]]
[[Category:Recipients of the Distinguished Service Star]]
[[Category:Recipients of the Bronze Cross Medal]]
[[Category:Recipients of the Military Merit Medal (Philippines)]]
[[Category:UNDOF Force Commanders]]
[[Category:1955 births]]
[[Category:Benigno Aquino III Administration personnel]]