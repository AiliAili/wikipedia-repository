{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Precaution
| title_orig    = 
| translator    = 
| image         = <!--prefer 1st edition-->
| caption = 
| author        = [[James Fenimore Cooper]]
| illustrator   = 
| cover_artist  = 
| country       = United States
| language      = English
| series        = 
| genre         = [[Novel]]
| publisher     = 
| release_date  = 1820
| english_release_date =
| media_type    = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| preceded_by   = 
| followed_by   = 
}}
'''''Precaution''''' (1820) is the [[debut novel|first novel]] written by American author [[James Fenimore Cooper]]. It was written in imitation of contemporary English domestic novels like those of [[Jane Austen]] and [[Amelia Opie]], and it did not meet with contemporary success.<ref name="bandw">{{cite web |url=http://www.kirjasto.sci.fi/jfcooper.htm |title=James Fenimore Cooper |website=Books and Writers ''(kirjasto.sci.fi)'' |first=Petri |last=Liukkonen |publisher=[[Kuusankoski]] Public Library |location=Finland |archiveurl=https://greencardamom.github.io/BooksAndWriters/jfcooper.htm |archivedate=10 February 2015 |dead-url=yes}}</ref> Cooper went on to have great success with works such as ''[[The Pathfinder (novel)|The Pathfinder]]'' (1841) and ''[[The Deerslayer]]'' (1840). The American reading public responded most to ''[[The Last of the Mohicans]]'' (1826).
{{TOC left}}

==Background==
It is thought that the novel was written after a challenge made by his wife. His biographer Warren Walker records it this way:

:''"... In the customary practice of the day he was reading aloud to his wife one evening from a current English novel, but found the story dull. Throwing it aside, he declared, "I could write a better book than that myself." And Susan's challenge to make good his boast resulted in his writing Precaution (1820). . ."''<ref name="bandw"/><ref>{{cite book | title=James Fennimore Cooper: An Introduction and Interpretation | last=Walker | first=Warren | publisher=Barnes & Noble | location=New York | edition=2nd | year=1963 | id= }}</ref> When Cooper's work was published without a name it was anonymously accredited to an English woman. It was also published in England and was well received among the set that was fond of this style of writing. The publisher, A.T. Goodrich, later surprised the public when it revealed that ''Precaution'' was authored by a gentleman from New York. This is the work that made Cooper realize his potential as a writer.<ref>[[#Phillips|Phillips, 1913]] James Fenimore Cooper  p.78</ref>

==References==
{{Reflist}}

==Sources==
*{{cite book |last=Phillips |first=Mary Elizabeth |title=James Fenimore Cooper |ref=Phillips |publisher=John Lane Company, New York, London |pages=368 |year=1913 }}[https://books.google.com/books?id=so4DAAAAYAAJ&source=gbs_navlinks_s Url]
*{{cite book | title=James Fennimore Cooper: Literature & Life Story | last=Long | first=Robert Emmet  | publisher=Continuum: A Fredrick Ungar Book | location= | edition= | year=1990 | isbn=0-8264-0431-6 }}
*{{cite book | title=James Fennimore Cooper | last=Spiller | first=Robert E. | publisher=North Central Publishing Company | location=Minnesota | edition= | year=1936 | id= }}
*{{cite book | title=James Fennimore Cooper: An Introduction and Interpretation | last=Walker | first=Warren | publisher=Barnes & Noble | location=New York | edition=2nd | year=1963 | id= }}

==External links==
{{Gutenberg|no=10365|name=Precaution}}

{{James Fenimore Cooper}}

{{DEFAULTSORT:Precaution (Novel)}}
[[Category:1820 novels]]
[[Category:Novels by James Fenimore Cooper]]
[[Category:19th-century American novels]]


{{1820s-novel-stub}}