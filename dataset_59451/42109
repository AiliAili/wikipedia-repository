[[File:ADVENT-engine.JPG|thumb|right|upright=1.25|Cut-away view of a prospective ADVENT engine]]
The '''ADaptive Versatile ENgine Technology''' (or '''ADVENT''') program is an [[aircraft engine]] development program run by the [[United States Air Force]] with the goal of developing an efficient [[variable cycle engine]] for next generation military aircraft in the 20,000&nbsp;lbf (89&nbsp;kN) thrust class.

==Objective==
The objective of ADVENT is to develop an engine that is optimized for several design points, rather than the traditional single point. Instead of having an engine that is designed solely for high speed (like many current fighter engines are) or for high fuel efficiency (like many current commercial engines are), the final ADVENT engine would be designed to operate at both those conditions.<ref name="wppress">Barr, Larine. [http://www.wpafb.af.mil/news/story.asp?id=123048376 "Air Force plans to develop revolutionary engine"] {{webarchive |url=https://web.archive.org/web/20110605114250/http://www.wpafb.af.mil/news/story.asp?id=123048376 |date=June 5, 2011 }}. US Air Force press release, 11 April 2007, Accessed: 20 October 2009.</ref> Specific goals include reducing average fuel consumption by 25% and reducing the temperature of cooling air produced by the engine.<ref name="rrpress">[http://www.defense-aerospace.com/articles-view/release/3/109107/rolls_royce-selected-for-advent-demonstrator.html "Rolls Royce Selected for ADVENT Demonstrator"], Press Release, 18 Oct 2009, Accessed 20 Oct 2009.</ref>

==Applications==
The ADVENT engine was originally targeted at the Air Force's 2018 [[Next-Generation Bomber]], but uncertainty in that program has led Rolls-Royce (RR), one of the primary developers involved with the project, to predict that the ADVENT engine will be better suited for a potential 2020 engine upgrade for the [[F-35 Lightning II]]. RR, who is partnered with GE Aviation on the embattled [[General Electric/Rolls-Royce F136|F136]] alternate engine for the F-35, has suggested that the ADVENT development contracts are all the more reason to continue the F136, as any engine upgrade from [[Pratt & Whitney]] (makers of the [[Pratt & Whitney F135|F135]] engine currently used in the F-35) would have to be separately funded, either internally or to additional government cost.<ref>Trimble, Steven. [http://www.flightglobal.com/articles/2009/06/11/327771/rolls-royce-f136-survival-is-key-for-major-f-35-engine.html "Rolls Royce: F136 Survival is key for major F-35 engine upgrade"] ''[[Flight International]]'', 11 June 2009, Accessed: 20 Oct 2009</ref>

==History==
The ADVENT program is one of several related development projects being pursued under the Air Force's [[Versatile Affordable Advanced Turbine Engines]] (VAATE) program. After being announced in April 2007, [[Rolls-Royce plc|Rolls-Royce]] and [[GE Aviation]] were awarded Phase I contracts in August 2007 to explore concepts, develop and test critical components, and begin preliminary designs of an engine.<ref name="wppress"/><ref>Trimble, Steven. [http://www.flightglobal.com/articles/2007/09/25/217104/pratt-whitney-loses-second-bid-for-usaf-engine-technology.html "Pratt & Whitney Loses Second Bit for USAF Technology Contracts"]. Flightglobal, 25 September 2007. Accessed: 20 October 2009.</ref>

In October 2009, Rolls-Royce was awarded the Phase II contract to continue component testing and integrate the developed technologies into a technology demonstrator engine.<ref name="rrpress"/> GE Aviation was also awarded funds to continue development of their technology demonstration core, which was unexpected as the ADVENT program had originally called for a single contractor to be selected for Phase II.<ref>Trimble, Steve. [http://www.flightglobal.com/articles/2009/10/15/333519/corrected-usaf-selects-general-electric-and-rolls-royce-to-continue-advent-work.html "USAF Selects General Electric and Rolls-Royce to Continue ADVENT work"]. ''Flight International'', 15 October 2009. Accessed: 20 Oct 2009.</ref>

USAF officials have denied that the program is an attempt to create a backup engine program for the F-35.<ref>Wright, Austin. [http://www.politico.com/news/stories/0512/76408.html "Lawmakers suspect jet engine end run."] ''Politico'', 16 May 2012.</ref>

With the threat of the GE/RR F136, Pratt & Whitney has funded an adaptive fan variant of its F135, that may qualify for the follow-on ''Adaptive Engine Technology Development'' (AETD) program under the US Air Force Research Laboratory.<ref>Majumdar, Dave. [http://www.flightglobal.com/news/articles/farnborough-pratt-to-test-new-adaptive-fan-f135-variant-next-year-374283/ "FARNBOROUGH: Pratt to test new adaptive fan F135 variant next year."] ''Flight International'', 12 July 2012.</ref> 

In 2012, GE was chosen to continue its ADVENT work into the AETD program.<ref>Brooks, Robert. [http://americanmachinist.com/news/usaf-taps-ge-develop-new-jet-engines "USAF Taps GE to Develop New Jet Engines."] ''American Machinist'', 21 October 2012.</ref>  GE and Pratt & Whitney were selected over Rolls Royce to continue the AETD program to mature fuel-efficient, high-thrust powerplants.<ref>Warwick, Graham.  [http://www.aviationweek.com/Article.aspx?id=/article-xml/asd_09_18_2012_p01-02-496815.xml "Pratt In, Rolls Out, GE Stays On AFRL Advanced Engine Demo."] ''Aviation Week'', 18 September 2012.</ref>  Operational testing of the engine was expected to begin in 2013.<ref name="rrpress"/>

In 2014, [[Chuck Hagel]] requested a $1 billion investment in the engine technology.<ref>{{cite web |url=http://www.militarytimes.com/article/20140224/NEWS05/302240043 |title=Pentagon, Air Force doubles down on engine technology |last1=Mehta |first1=Aaron |date=24 February 2014 |website= militarytimes.com |publisher=Gannett Government Media |accessdate=24 February 2014}}</ref> A [[Assistant Secretary of Defense for Research and Engineering|government official]] has warned that [[Budget sequestration in 2013|sequestration]] risks ending the program.<ref name=aw2015-04>{{cite news |first=Graham |last=Warwick |url=http://aviationweek.com/technology/budget-cuts-future-weapons-could-have-long-term-impact |title=Budget Cuts To Future Weapons Could Have Long-Term Impact |work=[[Aviation Week & Space Technology]] |date=3 April 2015 |accessdate=8 April 2015 }}</ref>

==See also==
{{aircontent
|see also=<!-- other related articles that have not already linked: -->
* [[Advanced Affordable Turbine Engine]] (AATE)
|related=<!-- designs which were developed into or from this aircraft: -->
* [[General Electric YF120]]
|similar engines=<!-- aircraft that are of similar role, era, and capability this design: -->
|lists=<!-- relevant lists that this aircraft appears in: -->
* [[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{reflist|35em}}
<!-- 
== External links == -->

[[Category:Aerospace engineering]]
[[Category:Jet engines]]