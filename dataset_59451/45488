{{no footnotes|date=February 2013}}
{{Infobox military person
|name=Russell Lowell Maughan
|birth_date= {{birth date|1893|3|28|mf=yes}}
|death_date= {{death date and age|1958|4|21|1893|3|28|mf=yes}}
|birth_place= [[Logan, Utah]], U.S. 
|death_place= [[San Antonio, Texas]], U.S. 
|placeofburial= [[Logan, Utah]], U.S.
|placeofburial_label= Place of burial
|image= Russell Lowell Maughan.jpg
|caption=
|allegiance={{flag|United States of America}}
|branch=[[File:Insignia signal.svg|20px]] [[Aviation Section, U.S. Signal Corps|Aviation Section, Signal Corps]]<br/>[[Image:Prop and wings.svg|20px]] [[United States Army Air Service|Air Service, United States Army]]<br/>[[Image:USAAC Roundel 1919-1941.svg|20px]] [[United States Army Air Corps]]<br>[[Image:US Army Air Corps Hap Arnold Wings.svg|20px]] [[United States Army Air Forces]]
|serviceyears= 1917–1946
|rank= Colonel
|awards=[[Distinguished Service Cross (United States)|Distinguished Service Cross]]}}

'''Russell Lowell Maughan''' (March 28, 1893 &ndash; April 21, 1958) was an officer in the [[United States Army]] and a pioneer aviator. His career began during [[World War I]], and spanned the period in which military aviation developed from a minor arm of the [[Signal Corps (United States Army)|Army Signal Corps]] to the huge [[United States Army Air Forces|Army Air Forces]] on the verge of becoming a separate service.

Maughan became a [[fighter pilot|pursuit pilot]] and served in combat in France in 1918 with the [[United States Army Air Service]]. Following the war, he remained in the Air Service and became a [[test pilot]]. In 1924 Maughan completed the first flight across the continental United States within the hours of daylight of a single calendar day.

==Biography==
Maughan was born March 28, 1893 in [[Logan, Utah]], to Peter W. and Mary (née Naef) Maughan. He graduated from [[Utah State University|Utah State Agricultural College]] in June 1917.

The United States had entered [[World War I]] and Maughan enlisted as an Army aviation cadet. Commissioned a [[first lieutenant#United States|first lieutenant]] in the Signal Officer Reserve Corps after flight training and [[USAF aeronautical rating|rated a Reserve Military Aviator]], he served in [[France]] with the [[139th Aero Squadron]], where he flew a [[Spad XIII]]. Maughan was credited with four aerial victories and awarded the [[Distinguished Service Cross (United States)|Distinguished Service Cross]] on October 27, 1918, the citation for which is given below.

He remained in the Air Service following the end of the war and was assigned to its Engineering Division at [[McCook Field]], [[Dayton, Ohio]], as a [[test pilot]]. Besides testing new designs, his responsibilities including public demonstrations of military aircraft and participation in air races. The Engineering Division had drawn the interest of Brig. Gen. [[Billy Mitchell]], Assistant Chief of the Air Service, who saw in it the opportunity for promoting the concept of an Air Force independent of the Army. On July 1, 1920, when the Air Service became a [[combat arm]] of the Army, Maughan received a Regular commission as a 1st lieutenant, Air Service. He transferred to [[Crissy Field]] at the [[Presidio of San Francisco]] in 1921 and joined the [[91st Network Warfare Squadron|91st Observation Squadron]], then engaged in aerial [[forest fire]] patrol.

In 1922 the [[National Air Race]]s were held at [[Selfridge Field]], [[Michigan]], where the Air Service entered the Pulitzer Trophy Race with ten aircraft it had solicited from various manufacturers for use as possible pursuit planes, with [[specification]] that they be capable of reaching a speed of {{convert|190|mi/h|km/h|abbr=on}} or greater. Flying a [[Curtiss R-6]] racer, a precursor of the [[Curtiss P-1 Hawk|PW-8]] design, Maughan won the Pulitzer race with an average speed of {{convert|205.86|mi/h|km/h|abbr=on}}, on October 14, 1922.<ref name=Berliner>{{Cite book |last=Berliner |first=Don |title=Airplane Racing |publisher=McFarland & Company, Inc., Publishers |year=2010 |ISBN=9780786443000 |page=60}}</ref>  On October 16, flying the 1-kilometer course, he averaged {{convert|229|mi/h|km/h|abbr=on}} for eight circuits, {{convert|232.22|mi/h|km/h|abbr=on}} for four, and reached {{convert|248.5|mi/h|km/h|abbr=on}} on one. This established a new international record, but it was not observed by [[Fédération Aéronautique Internationale]] (FAI) officials and was not officially recognized.

The following year Maughan officially set a new international speed record of {{convert|236.5|mi/h|km/h|abbr=on}}. He also made two attempts in July to fly coast-to-coast in a single day, using the new Curtiss PW-8, but mechanical problems thwarted both flights. On June 23, 1924, his third attempt succeeded, the first [[Dawn-to-dusk transcontinental flight across the United States]]. The flight was made in six legs, with an actual flying time of 18 hours and 20 minutes, at an average ground speed of more than 156&nbsp;mph.

Maughan served in the Philippines from 1930 to 1935, with duty as Secretary of Aviation and Consultant to the Philippine Cabinet from 1930 to 1932. In 1939 he surveyed and selected airfields in [[Greenland]] and [[Iceland]] for aircraft ferry routes to [[UK|Britain]].

Maughan, promoted to [[lieutenant colonel]], commanded the 60th Transport Group, [[Pope Air Force Base|Pope Field]], [[North Carolina]], from July 28, 1941 to April 15, 1942. Promoted again to [[Colonel (United States)|colonel]], he was advanced to command of the 51st Troop Carrier Wing from June 1, 1942 to October 20, 1942, during its deployment to England to drop [[airborne forces]] in the [[Operation Torch|invasion of North Africa]].

Colonel Maughan retired in 1946, and died April 21, 1958, at [[San Antonio, Texas]] during surgery. He is buried in the Logan City Cemetery near the Utah State University campus in Logan, Utah. He is a member of the Utah Aviation Hall of Fame and is honored with a plaque in the Hill Aerospace Museum, [[Hill Air Force Base]], [[Utah]]. A plaque commemorating the first "Dawn-to-dusk transcontinental flight across the United States was erected on the Utah State University campus in Logan, Utah, on Veteran's Day, 2006.

==Citation for Distinguished Service Cross==
MAUGHAN, RUSSELL L.
:First Lieutenant (Air Service), U.S. Army
:Pilot, 139th Aero Squadron, Air Service, A.E.F.
:Date of Action: October 27, 1918
:Citation:
:The Distinguished Service Cross is presented to Russell L. Maughan, First Lieutenant (Air Service), U.S. Army, for extraordinary heroism in action near [[Ardennes (département)|Sommerance]], [[France]], October 27, 1918. Accompanied by two other planes, Lieutenant Maughan was patrolling our lines, when he saw slightly below him an enemy plane ([[Fokker]] type). When he started an attack upon it he was attacked from behind by four more of the enemy. By several well-directed shots he sent one of his opponents to the earth, and, although the forces of the enemy were again increased by seven planes, he so skillfully maneuvered that he was able to escape toward his lines. While returning he attacked and brought down an enemy plane which was diving on our trenches.
:General Orders No. 46, W.D., 1919
:Birth: Logan, UT
:Home Town: Logan, UT

==References==
{{reflist}}
*{{Citation
 |last= Shiner
 |first= John F.
 |chapter= From Air Service to Air Corps: The Billy Mitchell Era
 |title= Winged Shield, Winged Sword: A History of the United States Air Force
 |editor-last=Nalty
 |editor-first=Bernard C.
 |volume= I
 |publisher= Air Force History and Museums Program, United States Air Force
 |year= 1997
 |isbn= 0-16-049009-X
 |doi= }}. Chapter 3.
*{{Citation
 |last= Maurer Maurer <!-- doubled. Google Books does not show title page with author name. -->
 |first= James Gilbert
 |title= Air Force Combat Units of World War II
 |origyear= 1961
 |year= 1980
 |publisher= Arno Press Inc.
 |series= Flight: Its First Seventy-Five Years
 |isbn= 0-405-12194-6
 |doi= }}. Originally published by USAF Historical Division.
*[https://web.archive.org/web/20070313010657/http://www.hilltoptimes.com:80/story.asp?edition=88&storyid=2366 Hill AFB biography article]
*[https://web.archive.org/web/20020718085046/http://www.generalaviationnews.com/editorial/articledetail.lasso?-token.key=2007&-token.src=column&-nothing General Aviation News articles--some inaccuracies]

==External links==
*[http://www.hill.af.mil/museum/history/maughan.htm Hill Aerospace Museum - Utah Aviation Hall of Fame - Colonel Russell Lowell Maughan USAF (United States Army Air Force)]
*[http://historytogo.utah.gov/people/utahns_of_achievement/russelllowellmaughan.html History To Go]

{{DEFAULTSORT:Maughan, Russell}}
[[Category:1893 births]]
[[Category:1958 deaths]]
[[Category:United States Army Air Service pilots of World War I]]
[[Category:American aviators]]
[[Category:Aviators from Utah]]
[[Category:Recipients of the Distinguished Service Cross (United States)]]
[[Category:American military personnel of World War I]]
[[Category:American military personnel of World War II]]
[[Category:United States Army Air Forces officers]]
[[Category:Utah State University alumni]]
[[Category:Air Corps Tactical School alumni]]