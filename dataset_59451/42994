<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = Arava
 |image = File:Arava Hatzerim 050804.jpg
 |caption = IAI Arava at the [[Israeli Air Force Museum]]
}}{{Infobox Aircraft Type
 |type = Transport
 |manufacturer = [[Israeli Aircraft Industries]]
 |designer =
 |first flight = 27 November 1969
 |introduced =
 |retired =
 |produced = 1972–1988
 |number built = 103
 |status = Active
 |unit cost =$450,000 (U.S.) in 1971
 |primary user = [[Israeli Air Force]]
 |more users = 14 other militaries
 |developed from =
 |variants with their own articles =
}}
|}

The '''Israeli Aircraft Industries Arava''' ({{lang-he-n|עֲרָבָה}}, "Willow" or "Steppe" of "Desert", named after the [[Arabah|Aravah]] of the [[Jordan Rift Valley]]) is a light [[STOL]] utility transport aircraft built in [[Israel]] by IAI in the late 1960s.

The Arava was IAI's first major aircraft design to enter production.  It was intended both for the military and civil market, but the aircraft was only built in relatively small numbers.  The customers were found mainly in third world countries, especially in [[Central America|Central]] and [[South America]] as well as [[Swaziland]] and [[Thailand]].

==Design and development==
The design work on the Arava began in 1965, and the design objectives included [[STOL]] performance, the ability to operate from rough strips and carry 20 passengers or bulky payloads.<ref name="AI Feb74 p57">Cohen 1974, p. 57.</ref>  To achieve this, the Arava design was of a relatively unusual configuration.

The Arava's [[fuselage]] was barrel-like, short but wide, and the rear of the fuselage was hinged and could swing open for easy loading and unloading. Its wingspan was long and the twin tails were mounted on booms that ran from the engine [[nacelle]]s.  It was fitted with a fixed [[tricycle landing gear|nosewheel undercarriage]] to save weight, while the chosen powerplant was two 715&nbsp;eshp (533&nbsp;kW) [[Pratt & Whitney Canada PT6]]A-27 [[turboprop]]s.<ref name="AI Feb74 p57,9">Cohen 1974, pp. 57, 59.</ref>

The first prototype Arava made its maiden flight on November 27, 1969. The second prototype was destroyed when a wing strut experienced flutter and failed during flight testing on November 19, 1970.<ref name="AI Feb74 p59">Cohen 1974, p. 59.</ref> The third prototype flew for the first time on May 8, 1971.<ref name="AI Feb74 p59"/><ref>{{cite journal|magazine=Air Progress|date=October 1971|page=22}}</ref> Three aircraft were commandeered for use by [[122 Squadron (Israel)|Squadron 122]] in the [[Yom Kippur War]],<ref name=GlobalSecurity1>{{cite web|url=http://www.globalsecurity.org/military/world/israel/arava.htm|title=IAI-201 Arava|author=John Pike|publisher=|accessdate=6 February 2015}}</ref><ref name=GlobalSecurity2>{{cite web |title=122 Squadron - The Dakota |url=http://www.globalsecurity.org/military/world/israel/122squadron.htm |publisher=Globalsecurity.org}}</ref> but were returned afterwards. The [[Israeli Air Force]] did not purchase the aircraft until 1983, when nine aircraft were bought.<ref name=GlobalSecurity1 /> Production ended in 1988.  103 aircraft were produced,<ref name=GlobalSecurity1 /> including 70 for the military market.  The IAF decided in 2004 to retire the aircraft.<ref name=GlobalSecurity1 /> It is still in operation in some countries.

==Variants==
* '''IAI 101''' - Civil-transport version
* '''IAI 102''' - Civil passenger aircraft for up to 20 people in airline-standard configuration or up to 12 passengers in VIP configuration
* '''IAI 102B''' - Civil transport version
* '''IAI 201''' - Military transport version
* '''IAI 202''' - Modified, longer version with modified wings

The military version could also be equipped with a range of weapons, in order to act in anti-submarine- or gunship roles.  The weapon configuration could include two machine guns in fuselage side packs (usually 0.5" Browning), plus a third gun on the rear fuselage, and two pods containing 6 x 82&nbsp;mm rocket pods or [[torpedo]]es or [[sonar]] [[buoy]]s on the fuselage sides.

Another less known military version is the 202B Electronic warfare model. This version was made in small numbers, and had distinct large radomes at each end of the fuselage. The radomes contained the Electronic Warfare mission systems.

==Operators==
[[File:IAI Arava Operators.png|thumb|400px|IAI Arava operators]]

; {{ARG}}
* [[Gobierno de Tierra Del Fuego]] - Dirección Provincial de Aeronáutica
; {{BOL}}
* [[Bolivian Air Force]] - Six purchased 1975–76. One seized by Nicaragua during delivery, one in use 1987.<ref>Siegrist 1987, p. 176.</ref>
; {{CMR}}
* [[Cameroon Air Force]]
; {{COL}}
* [[Colombian Air Force]] - One in operation, FAC1952.<ref>{{cite web|url=http://www.airliners.net/photo/Colombia---Air/Israel-Aircraft-Industries/2295435/&sid=404fafa14ce7953d257e26136204ec00|title=Photos: Israel Aircraft Industries IAI-201 Arava Aircraft Pictures - Airliners.net|publisher=|accessdate=6 February 2015}}</ref> 2 out of the original 3 have been retired.<ref>{{cite web|url=http://www.webinfomil.com/2011/05/iai-arava-201.html|title=IAI ARAVA 201|author=WebInfomil|publisher=|accessdate=6 February 2015}}</ref>
**[[Captain Germán Olano Moreno Air Base|Comando Aéreo de Combate No. 1]]
; {{ECU}}
* [[Ecuadorian Army]] - 2 in use as of March 2016. One other aircraft [[2016_Ecuadorian_Air_Force_Arava_crash|E-206]] was written off due to a crash<ref name="fi15 p37">Hoyle 2015, p. 37.</ref>
* [[Ecuadorian Navy]] - Former operator.
; {{SLV}}
* [[Air Force of El Salvador]] - 3 in use as of December 2015.<ref name="fi15 p37"/>
; {{GUA}}
* [[Guatemalan Air Force]] - 1 in use as of December 2015.<ref name="fi15 p39">Hoyle 2015, p. 39.</ref>
; {{HTI}}
* [[Armed Forces of Haiti]] 
; {{HON}}
* [[Honduran Air Force]] - 1 in use as of December 2015.<ref name="fi15 p39"/>
; {{ISR}}
* [[Israeli Air Force]]
; {{LBR}}
* [[Armed Forces of Liberia]]
; {{MEX}}
* [[Mexican Air Force]]
; {{NIC}}
* [[National Guard (Nicaragua)]]
* [[Nicaraguan Air Force]]
; {{PNG}}
* [[Papua New Guinea Defence Force]] - 3 in use as of December 2015.<ref name="fi15 p45">Hoyle 2015, p. 45.</ref>
; {{SWZ}}
* [[Military of Swaziland]]
; {{THA}}
* [[Royal Thai Air Force]] - 3 delivered from 1981.<ref>Pocock 1986, p. 115.</ref> 2 remain in use as of December 2015.<ref name="fi15 p50">Hoyle 2015, p. 50.</ref>
; {{VEN}}
* [[Army of Venezuela]] - 11 in use as of December 2015.<ref name="fi15 p53">Hoyle 2015, p. 53.</ref>
* [[Venezuelan National Guard]]
* [[Venezuelan Navy]] - Former operator.

==Specifications (IAI 201)==
[[File:IAI Arava 201 FAS-804 4X-IAQ Salvador LBG 07.06.75 edited-2.jpg|thumb|Arava 201 of the El Salvador Air Force displayed at the 1975 [[Paris Air Show]] prior to delivery]]
{{aircraft specifications
|jet or prop?=prop
|plane or copter?=plane
|ref=''Jane's All The World's Aircraft 1982-83.''<ref name="Janes 82 p123-4">Taylor 1982, pp. 123–124.</ref> 
|crew=2
|capacity=<br />
** 24 fully equipped troops ''or''
** 16 paratroopers
|payload main=2,351 kg
|payload alt=5,184 lb
|payload more=
|length main=12.69 m
|length alt=41 ft 6 in
|span main=20.96 m
|span alt=68 ft 9 in
|height main=5.21 m
|height alt=17 ft 1 in
|area main=43.68 m²
|area alt=470.2 ft<sup>2</sup>
|empty weight main=3,999 kg
|empty weight alt=8,816 lb
|loaded weight main=<!-- lb-->
|loaded weight alt=<!-- kg-->
|useful load main=
|useful load alt=
|max takeoff weight main=6,804 kg
|max takeoff weight alt=15,000 lb
|engine (prop)=[[Pratt & Whitney Canada PT6|Pratt & Whitney Canada PT6A-34]]
|type of prop=[[turboprop]]s
|number of props=2
|power main=559 kW
|power alt=750 shp
|max speed main=326 km/h
|max speed alt=176 knots, 202 mph
|max speed more=at 3,050 m (10,000 ft)
|cruise speed main=319 km/h
|cruise speed alt=172 knots, 198 mph
|cruise speed more=at 3,050 m (10,000 ft)
|stall speed main=115 km/h
|stall speed alt=62 knots, 71.5 mph)
|stall speed more=flaps down
|range main=1,056 km 
|range alt=570 [[nautical mile|nmi]], 656 mi
|range more=max fuel
|ceiling main=7,620 m
|ceiling alt=25,000 ft
|climb rate main=6.6 m/s
|climb rate alt=1,290 ft/min 
|loading main=<!-- lb/ft²-->
|loading alt=<!-- kg/m²-->
|power/mass main=<!-- hp/lb-->
|power/mass alt=<!-- W/kg-->

}}

==See also==
{{aircontent|
|related=
|similar aircraft=
*[[Antonov An-28]]
*[[CASA C-212]]
*[[de Havilland Canada DHC-6 Twin Otter]]
*[[Gotha Go 244]]
*[[Miles Aerovan]]
*[[Nord Noratlas]]
*[[PZL M-28]]
*[[Shorts SC.7 Skyvan]]
|lists=
|see also=
}}

==References==
{{commons category|IAI Arava}}
{{reflist}}

* Cohen, Irvine J. "Arava: Israel's first-born bids for world market". ''[[Air International|Air Enthusiast International]]'', February 1974, Vol 6, No 2. pp.&nbsp;55–61, 92–93.
* Hoyle, Craig. "World Air Forces 2015". ''[[Flight International]]'', 8–14 December 2015, Vol. 188, No. 5517. pp. 26–53. {{ISSN|0015-3710}}.
* Pocock, Chris. "Thailand Hones its Air Forces". ''Air International'', Vol. 31, No. 3, September 1986. pp. 113–121, 168. {{ISSN|0306-5634}}.
* Siegrist, Martin. "Bolivian Air Power — Seventy Years On". ''[[Air International]]'', Vol. 33, No. 4, October 1987. pp. 170–176, 194. {{ISSN|0306-5634}}.
*[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1982-83''. London:Jane's Yearbooks, 1982. ISBN 0-7106-0748-2.

{{IAI aircraft}}

[[Category:IAI aircraft|Arava]]
[[Category:Israeli military transport aircraft 1960–1969]]
[[Category:Twin-boom aircraft]]