{{other people|John Davis}}

{{Infobox Judge
| honorific-prefix    =
| name                = John Davis
| honorific-suffix    =
| image               = JohnDavis BostonAthenaeum6.png
| title1 = District Judge of the [[United States District Court for the District of Massachusetts]]
| term_start1 = February 20, 1801
| term_end1 = July 10, 1841
| predecessor1 = [[John Lowell]]
| successor1 = [[Peleg Sprague (Maine politician)|Peleg Sprague]]
| title2 = [[United States District Court for the District of Massachusetts#U.S. Attorneys|United States Attorney for the District of Massachusetts]]
| term_start2 = 1796
| term_end2 = 1801
| predecessor2 = [[Harrison Gray Otis (politician)|Harrison Gray Otis]]
| successor2 = [[George Blake (attorney)|George Blake]]
| state_house3        = Massachusetts
| district3           = [[Plymouth County, Massachusetts|Plymouth County]]
| term_start3         = 1795
| term_end3           = 1797
| preceded3           =<!-- Can be repeated up to eight times by changing the number -->
| succeeded3          =<!-- Can be repeated up to eight times by changing the number -->
| state_senate4       = Massachusetts
| district4           =<!-- Can be repeated up to seven times by adding a number, start at 3 -->
| term_start4         = 1789
| term_end4           = 1795
| preceded4           =<!-- Can be repeated up to seven times by adding a number, start at 3 -->
| succeeded4          =<!-- Can be repeated up to seven times by adding a number, start at 3 -->
| birth_date          = {{Birth date|1761|1|25}}
| birth_place         = [[Plymouth, Massachusetts]], United States
| death_date          = {{Death date and age|1847|1|25|1761|1|25}}
| death_place         = [[Boston, Massachusetts]], United States
| restingplace        =
| restingplacecoordinates =
| birthname           =
| nationality         = American
| party               =
| otherparty          = <!--For additional political affiliations -->
| spouse              =
| partner             = <!--For those with a domestic partner and not married -->
| relations           =
| children            =
| residence           =
| alma_mater          = [[Harvard College]]
| occupation          = [[Judge]]
| profession          =
| cabinet             =
| committees          =
| portfolio           =
| religion            =
| signature           =
| website             =
| footnotes           =
}}

'''John Davis''' (January 25, 1761 – January 14, 1847) was a lawyer, member of both the House of Representatives and the Senate of the Commonwealth of Massachusetts, comptroller, and [[United States federal judge|federal judge]].

==Early life==
Davis first received a private school education at Brookfield Academy like his father, before graduating from [[Harvard College]] in 1781, going on to [[read law]] and being admitted to the bar in 1786, before practicing private law in Plymouth.

==Career==

===Political career===
In 1788 he was selected as a delegate from Plymouth to the Massachusetts state convention, called to consider adoption of the [[United States Constitution|Federal Constitution]]. He was elected and served three times in the [[Massachusetts House of Representatives]], then in 1795 became state senator of [[Plymouth County, Massachusetts|Plymouth County]].

Later in 1795 he accepted President [[George Washington]]'s request to serve as [[Comptroller of the Treasury]] of the United States, a position he resigned from in 1796 over matters of salary. Washington then appointed him [[United States Attorney]] for the [[United States District Court for the District of Massachusetts|district of Massachusetts]], leaving the post in 1801. Subsequently he moved permanently to Boston.

===Judgeship===
In 1801 he was appointed by President [[John Adams]] as judge of the United States district court for the district of Massachusetts. His probable most noted achievement was his wise handling of the law in regards to commercial mercantile embarrassment of [[New England]] at the time of an [[embargo]] and the [[War of 1812]] which instilled the community's confidence in the law.

John Davis resigned this post on July 10, 1841, due to his advanced age and lived out his days in [[Boston, Massachusetts]].

==Other activities==
In addition to his legal career, he pursued an interest in scientific phenomena and was deeply interested in New England history and antiquity. He served as president of the [[Massachusetts Historical Society]] (1818-1835), and was said to be the first person to refer to the Plymouth colonists as [[Pilgrim (Plymouth Colony)|pilgrim]]s, in his ode to an anniversary celebration in 1794. He was elected a Fellow of the [[American Academy of Arts and Sciences]] in 1792,<ref name=AAAS>{{cite web|title=Book of Members, 1780–2010: Chapter D|url=https://www.amacad.org/multimedia/pdfs/publications/bookofmembers/ChapterD.pdf|publisher=American Academy of Arts and Sciences|accessdate=August 7, 2014}}</ref> He was also a [[Fellow]]{{clarify|post-text=of what?|date=August 2014}} (1803), treasurer (1810) and member of the board of overseers (1827-1836).  Davis was also elected a member of the [[American Antiquarian Society]] in 1813.<ref>[http://www.americanantiquarian.org/memberlistd American Antiquarian Society Members Directory]</ref>

==See also==
*[[Davis political family]]

==References==
{{reflist}}
*Johnson, Allen & Malone, Dumas (ed.'s). ''Dictionary of American Biography''. vol. III. Charles Scribner's Sons, New York, N.Y. 1959.
* {{FJC Bio|576}}

{{s-start}}
{{s-legal}}
{{s-bef|before=[[John Lowell]]}}
{{s-ttl|title=Judge of the [[United States District Court for the District of Massachusetts]]|years=1801–1841}}
{{s-aft|after=[[Peleg Sprague (Maine politician)|Peleg Sprague]]}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Davis, John}}
[[Category:1761 births]]
[[Category:1847 deaths]]
[[Category:Comptrollers of the United States Treasury]]
[[Category:Fellows of the American Academy of Arts and Sciences]]
[[Category:Members of the American Antiquarian Society]]
[[Category:Harvard University alumni]]
[[Category:Judges of the United States District Court for the District of Massachusetts]]
[[Category:Massachusetts State Senators]]
[[Category:Members of the Massachusetts House of Representatives]]
[[Category:Lawyers from Boston]]
[[Category:People from Plymouth, Massachusetts]]
[[Category:United States Attorneys for the District of Massachusetts]]
[[Category:United States federal judges appointed by John Adams]]
[[Category:18th-century judges]]
[[Category:People of colonial Massachusetts]]
[[Category:People admitted to the practice of law by reading law]]