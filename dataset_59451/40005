{{Infobox person | name =Arthur L. Welsh | image =Welsh 2162906189 312aca1fef o.jpg | image_size = | caption =Arthur L. Welsh and [[George William Beatty]] circa 1911 | birth_name =Laibel Wellcher | birth_date = {{Birth date|1881|8|14}} | birth_place =[[Russia|Russian Empire]] | death_date = {{Death date and age|1912|6|11|1881|8|14}} | death_place =[[United States Army Aviation School]]<br>[[College Park, Maryland]]| death_cause =[[Aircrash]] | resting_place = | resting_place_coordinates = | residence = | nationality = | other_names =Al Welsh | known_for = | education = | employer = | occupation = | title = | salary = | networth = | height = | weight = | term = | predecessor = | successor = | party = | boards = | religion = | spouse = | partner = | children = | parents = | relatives = | signature = | website = | footnotes = }}

'''Arthur L. "Al" Welsh''' (August 14, 1881 &ndash; June 11, 1912) was a Jewish, [[Russia]]n-born [[United States|American]] pioneer aviator who became the first flight instructor for the Wright Brothers. He was killed in an [[aircrash]] in 1912.

==Early life==
He was born as '''Laibel Welcher''' on August 14, 1881, in [[Kiev]], [[Ukraine]], which was then part of the [[Russian Empire]].<ref>Some sources list his birth as 1875</ref> He was one of six children of Abraham and Dvora Wellcher. In 1890, the family emigrated to [[Philadelphia]], speaking no English.<ref>{{cite web |url=http://www.jhsgw.org/exhibitions/online/arthurwelsh/1.html |title=Arthur Welsh |accessdate=2009-09-04 |quote= |publisher=[[Jewish Historical Society of Greater Washington]] }}</ref> He attended both public school and [[Hebrew school]] there. His father died when he was 13 years old and he was sent to [[Washington, D.C.]] to live with relatives shortly after his mother remarried. He was a top student who did best in math and mechanics, and was excellent at swimming.<ref name=JewishObserver/>

He changed his surname to "Welsh" when he joined the [[United States Navy]] as a 20-year-old, expecting greater success in the Navy with a name that did not sound "too Jewish". He received an [[honorable discharge]] after a tour of duty that lasted four years. He contracted [[typhoid fever]] one month after he was discharged and spent four months recovering in a hospital.<ref name=JewishObserver/>

==Pilot==
After his recuperation, Welsh moved back to [[Washington, D.C]]. While working as a bookkeeper at a local gas company, Welsh wrote a letter to the [[Wright brothers]] after seeing a flight demonstration in [[Virginia]], but did not receive a job offer with the company. He traveled to [[Dayton, Ohio]], convinced that he could make a positive impression in person. The brothers gave him a job in the [[Wright Company]]'s new flying exhibition division, even though he did not have the experience they were looking for.<ref name=JewishObserver/>

He began his orientation with the Wright Company in Dayton and traveled to the company's winter flying location in [[Montgomery, Alabama]] where he showed strong potential as a pilot with Orville as his instructor. Called back to Dayton, he was asked to help establish the company's flight school at [[Huffman Prairie]].<ref>Huffman Prairie later to become part of [[Wright-Patterson Air Force Base]].</ref> He worked there as an instructor and [[test pilot]] alongside pioneers [[Frank Trenholm Coffyn]] and [[Ralph Johnstone]]. There he taught students including [[Henry H. Arnold|Hap Arnold]], who would become a five-star general leading the [[United States Army Air Corps]] during [[World War II]]. He set multiple records for flight time and altitude and won several flying competitions.<ref name=JewishObserver/>

Student [[George William Beatty]] flew his [[first solo flight]] on July 23, 1911, and that same day flew as a passenger with Welsh to establish a new American two-man altitude record of 1,860 feet, one of Welsh's many such records.<ref name=EarlyAviators>{{cite web |url=http://www.earlyaviators.com/ebeatty.htm |accessdate=2009-09-04 |title=George W. Beatty |publisher=[[Earlyaviators.com]] }}</ref>

==Death==
He died in a crash on June 11, 1912, while flying with [[Leighton Wilson Hazelhurst, Jr.]] at the [[United States Army Aviation School]] in [[College Park, Maryland]] on a [[Wright Model C]] that had recently been purchased by the [[Aeronautical Division, U.S. Signal Corps]].<ref>Launius, Roger D.; and Bednarek, Janet Rose Daly. [https://books.google.com/books?id=TuqG9SgstIoC&pg=PA160&lpg=PA160 "Reconsidering a century of flight"], p. 160, [[University of North Carolina Press]], 2003, ISBN 0-8078-5488-3. Accessed September 5, 2009.</ref> The [[United States Army Signal Corps]] had established a series of ten acceptance tests for the aircraft, and Welsh and Hazelhurst were taking the Model C on a climbing test, the next to last in the series required by the Army. Shortly after takeoff, the plane pitched over while making a turn and fell {{convert|30|ft|m}} to the ground, killing both crew members. They had both been ejected from their seats, with Welsh suffering a crushed skull and Hazelhurst a [[cervical fracture|broken neck]].<ref name=NYTCrash/> ''[[The New York Times]]'' described Welsh as "one of the most daring professional aviators in America" and his flying partner Hazelhurst as being among the "most promising of the younger aviators of the army".<ref name=NYTCrash>{{cite news |first= |last= |authorlink= |coauthors= |title=Lieut. Hazelhurst and Al Welsh, Professional Aviator, Victims of Airship Test |url=https://query.nytimes.com/gst/abstract.html?res=9505E1D7153CE633A25751C1A9609C946396D6CF |quote=Lieut. Leighton W. Hazelhurst, Jr., of the Seventeenth Infantry, one of the most promising of the younger aviators of the army, and Al Welsh, one of the most daring professional aviators in America, were instantly killed in a flight at the Army Aviation School at College Park, Md., at 6:30 o'clock this evening. |work=[[The New York Times]] |date=June 12, 1912 |accessdate=2009-09-04 }}</ref>

A board of inquiry was formed by the [[United States Secretary of War]] [[Henry Lewis Stimson]], which concluded that Welsh was at fault in the crash, having risen to 150 feet, with the plan to dive at a 45-degree angle in order to gain momentum for a climb, but had made the dive too soon, with the board's results reported in the June 29, 1912 issue of ''[[Scientific American]]''. In a 2003 interview, a cousin of Welsh's reported the family's belief that the tests were run too rapidly and that Welsh was doomed to fail by carrying too much fuel and a passenger, giving a craft that would be unable to make the planned maneuver with the weight it was carrying.<ref name=JewishObserver/>

Former student [[George William Beatty]], who had set up his own flying instruction school on [[Long Island]], replaced Welsh as the government's test pilot at the College Park facility.<ref>Igoe, Kate. [http://www.nasm.si.edu/research/arch/findaids/pdf/BEATTY_Finding_Aid.pdf George W. Beatty Collection], [[National Air and Space Museum]], 1997. Accessed September 6, 2009.</ref>

==Personal==
Welsh's funeral was held on June 13 at [[Adas Israel Congregation (Washington, D.C.)|Adas Israel Congregation]] in [[Washington, D.C.]], then an [[Orthodox Judaism|Orthodox Jewish]] synagogue, with services led by the congregation's [[hazzan|cantor]] Joseph Glushak. The funeral was attended by [[Orville Wright]] and his sister Katharine, who had traveled from [[Dayton, Ohio]] and who were still in mourning for their brother [[Wilbur Wright|Wilbur]], who had died less than two weeks earlier. Welsh was buried at the Adas Israel Cemetery in the [[Douglass, Washington, D.C.|Douglass]] neighborhood of Washington, D.C.<ref name=JewishObserver>Thum, Robert. [http://www.jewishdayton.org/page.aspx?id=38000 "The first Jewish aviator"], ''[[The Dayton Jewish Observer]]'', 2003. Accessed August 5, 2009.</ref>

Welsh was survived by his wife Annie, who died in 1926, and by their two-year-old daughter Ailene. His daughter lived into her 90s, living in England and adopting the name Abigail and keeping the last name Welsh, and recalled in a 2003 her recollections of the warmth and kindness of members of what she called the "Wright Circle" and how she had crawled through the legs of Hap Arnold as a toddler when he visited the family home. She expressed her regret that "I wish I had known my father. I heard so many good things about him."<ref name=JewishObserver/>

In his 1949 book ''Global Mission'', Hap Arnold credited Welsh with having "taught me all he knew, or rather, he had taught me all he could teach. He knew much more."<ref name=JewishObserver/>

Welsh grew up in the same Washington D.C. neighborhood, [[Southwest, Washington, D.C.|Southwest]], as another Russian immigrant, Asa Yoelson. Asa grew up and changed his name to [[Al Jolson]] and became a legendary singer. It's not on record if the two ever met. Welch and Jolson however favor one another when photos are studied{{According to whom|date=March 2016}}.

==See also==
{{Portal|Biography}}
*[[List of aviators killed in aviation accidents or incidents before 1916]]
*[[List of accidents and incidents involving military aircraft (pre-1925)]]

==References==
{{reflist}}

==External links==
*[http://www.earlyaviators.com/ewelsh.htm Arthur L. Welsh] at [[Early Aviators]]

{{DEFAULTSORT:Welsh, Arthur}}
[[Category:1881 births]]
[[Category:1912 deaths]]
[[Category:American Jews]]
[[Category:Aviators killed in aviation accidents or incidents in the United States]]
[[Category:Imperial Russian emigrants to the United States]]
[[Category:People from Kiev]]
[[Category:People from Philadelphia]]
[[Category:People from Washington, D.C.]]
[[Category:United States Navy personnel]]
[[Category:Wright brothers]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots]]
[[Category:Wright Flying School alumni]]
[[Category:Accidental deaths in Maryland]]
<!--[[Category:Missing middle or first names]] L was most likely for for Laibel his birth name-->