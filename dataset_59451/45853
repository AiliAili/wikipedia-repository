{{about|the art movement}}
[[File:I was a Rich Man's Plaything 1947.jpg|thumb|upright=1.3|[[Eduardo Paolozzi]], ''I was a Rich Man's Plaything'' (1947). Part of his ''Bunk!'' series, this is considered the initial bearer of "pop art" and the first to display the word "pop".|alt=An image of a sexy woman smiles as a revolver aimed at her head goes "Pop!"]]
[[File:Campbell's Tomato Juice Box. 1964. Synthetic polymer paint and silkscreen ink on wood.jpg|thumb|upright=1.3|[[Andy Warhol]], ''Campbell's Tomato Juice Box'', 1964. Synthetic polymer paint and silkscreen ink on wood, 10 inches × 19 inches × 9½ inches (25.4 × 48.3 × 24.1 cm), [[Museum of Modern Art]], [[New York City]]|alt=A plain-looking box with the Campbell's label sits on the ground.]]
'''Pop art''' is an [[art movement]] that emerged in the mid-1950s in [[UK|Britain]] and the late 1950s in the [[United States]].<ref name="pop">Livingstone, M., ''Pop Art: A Continuing History'', New York: Harry N. Abrams, Inc., 1990</ref> Among the early artists that shaped the pop art movement were [[Eduardo Paolozzi]] and [[Richard Hamilton (artist)|Richard Hamilton]] in [[UK|Britain]], and [[Larry Rivers]], [[Robert Rauschenberg]] and [[Jasper Johns]] among others in the [[United States]]. Pop art presented a challenge to traditions of [[fine art]] by including imagery from [[popular culture]] such as advertising and news. In pop art, material is sometimes visually removed from its known context, isolated, and/or combined with unrelated material.<ref name="pop" /><ref name="ages">de la Croix, H.; Tansey, R., ''Gardner's Art Through the Ages'', New York: Harcourt Brace Jovanovich, Inc., 1980.</ref>

Pop art employs aspects of [[mass culture]], such as [[advertising]], [[comic books]] and mundane cultural objects. One of its aims is to use images of ''popular'' (as opposed to elitist) culture in art, emphasizing the banal or [[kitsch]]y elements of any culture, most often through the use of [[irony]].<ref name="ages" /> It is also associated with the artists' use of mechanical means of reproduction or rendering techniques.

Pop art is widely interpreted as a reaction to the then-dominant ideas of [[abstract expressionism]], as well as an expansion of those ideas.<ref name="iha" /> Due to its utilization of [[found objects]] and images, it is similar to [[Dada]]. Pop art and [[minimalism]] are considered to be art movements that precede [[postmodern art]], or are some of the earliest examples of postmodern art themselves.<ref>{{cite book| last=Harrison| first=Sylvia |title=Pop Art and the Origins of Post-Modernism| publisher=Cambridge University Press| date=2001-08-27}}</ref>

Pop art often takes imagery that is currently in use in advertising. Product labeling and logos figure prominently in the imagery chosen by pop artists, seen in the labels of ''[[Campbell's Soup Cans]]'', by [[Andy Warhol]]. Even the labeling on the outside of a shipping box containing food items for retail has been used as subject matter in pop art, as demonstrated by [[Andy Warhol|Warhol's]] ''Campbell's Tomato Juice Box,'' 1964 (pictured).

==Origins==
The origins of pop art in North America developed differently from Great Britain.<ref name="ages" /> In the United States, pop art was a response by artists; it marked a return to [[Hard-edge painting|hard-edged]] composition and [[Representation (arts)|representational art]]. They used impersonal, mundane reality, [[irony]], and [[parody]] to "defuse" the personal symbolism and "[[painterly]] looseness" of [[abstract expressionism]].<ref name="iha" /><ref name="high">Gopnik, A.; Varnedoe, K., ''High & Low: Modern Art & Popular Culture'', New York: The Museum of Modern Art, 1990</ref> In the U.S., some artwork by [[Larry Rivers]], [[Alex Katz]] and [[Man Ray]] anticipated pop art.<ref>{{cite web|url=http://www.smithsonianmag.com/arts-culture/alex-katz-is-cooler-than-ever-34462293/ |title=History, Travel, Arts, Science, People, Places &#124; Smithsonian |website=Smithsonianmag.com |date= |accessdate=2015-12-30}}</ref>

By contrast, the origins of pop art in [[Post-war|post-War]] Britain, while employing irony and parody, were more academic. Britain focused on the dynamic and paradoxical imagery of American [[popular culture|pop culture]] as powerful, manipulative symbolic devices that were affecting whole patterns of life, while simultaneously improving the prosperity of a society.<ref name="high" /> Early pop art in Britain was a matter of ideas fueled by [[American popular culture]] ''when viewed from afar''.<ref name="iha" />  Similarly, pop art was both an extension and a repudiation of [[Dadaism]].<ref name="iha" /> While pop art and Dadaism explored some of the same subjects, pop art replaced the destructive, satirical, and anarchic impulses of the Dada movement with a detached affirmation of the artifacts of mass culture.<ref name="iha" /> Among those artists in Europe seen as producing work leading up to pop art are: [[Pablo Picasso]], [[Marcel Duchamp]], and [[Kurt Schwitters]].

===Proto-pop===
[[File:NY Met demuth figure 5 gold.JPG|thumb|upright|[[Charles Demuth]], ''[[I Saw the Figure 5 in Gold]]'' 1928,  collection of the [[Metropolitan Museum of Art]], [[New York City]] ]]
It should be noted that, while the British pop art movement predated the American pop art movement, Marcel Duchamp and others in Europe like [[Francis Picabia]] and Man Ray predate the British; in addition there were some earlier American ''proto-pop'' origins which utilized "as found" cultural objects.<ref name="iha">Piper, David. ''The Illustrated History of Art'', ISBN 0-7537-0179-0, p486-487.</ref> During the 1920s, American artists [[Gerald Murphy]], [[Charles Demuth]] and [[Stuart Davis (painter)|Stuart Davis]] created paintings that contained pop culture imagery (mundane objects culled from American commercial products and advertising design), almost "prefiguring" the pop art movement.<ref>{{cite web|url=http://www.newyorker.com/arts/critics/artworld/2007/08/06/070806craw_artworld_schjeldahl |title=Modern Love |publisher=The New Yorker |date=2007-08-06 |accessdate=2015-12-30}}</ref><ref>Wayne Craven, ''American Art: History and .'' p.464.</ref>

==United Kingdom: the Independent Group==
[[File:Hamilton-appealing2.jpg|thumb|left|[[Richard Hamilton (artist)|Richard Hamilton]]'s collage ''[[Just what is it that makes today's homes so different, so appealing?]]'' (1956) is one of the earliest works to be considered "pop art".|alt=A collage of many different styles shows a mostly naked man and woman in a house.]]

The [[Independent Group]] (IG), founded in London in 1952, is regarded as the precursor to the pop art movement.<ref name="pop" /><ref name="mod">Arnason, H., ''History of Modern Art: Painting, Sculpture, Architecture'', New York: Harry N. Abrams, Inc. 1968.</ref> They were a gathering of young painters, sculptors, architects, writers and critics who were challenging prevailing modernist approaches to culture as well as traditional views of fine art. Their group discussions centered on pop culture implications from elements such as mass advertising, movies, product design, comic strips, science fiction and technology. At the first Independent Group meeting in 1952, co-founding member, artist and sculptor [[Eduardo Paolozzi]] presented a lecture using a series of [[collage]]s titled ''Bunk!'' that he had assembled during his time in Paris between 1947 and 1949.<ref name="pop" /><ref name="mod" /> This material of "found objects" such as advertising, comic book characters, magazine covers and various mass-produced graphics mostly represented ''American'' popular culture. One of the collages in that presentation was Paolozzi's ''I was a Rich Man's Plaything'' (1947), which includes the first use of the word "pop", appearing in a cloud of smoke emerging from a revolver.<ref name="pop" /><ref>{{cite web|url=http://www.tate.org.uk/art/artworks/paolozzi-i-was-a-rich-mans-plaything-t01462 |title='I was a Rich Man's Plaything', Sir Eduardo Paolozzi |publisher=Tate |date=2015-12-10 |accessdate=2015-12-30}}</ref> Following Paolozzi's seminal presentation in 1952, the IG focused primarily on the imagery of American popular culture, particularly mass advertising.<ref name="high" />

According to the son of [[John McHale (artist)|John McHale]], the term "pop art" was first coined by his father in 1954 in conversation with [[Frank Cordell]],<ref name="Warholstars.org">{{cite web|url=http://www.warholstars.org/articles/johnmchale/johnmchale.html |title=John McHale |website=Warholstars.org |date= |accessdate=2015-12-30}}</ref> although other sources credit its origin to British critic [[Lawrence Alloway]].<ref>"Pop art", ''A Dictionary of Twentieth-Century Art'', Ian Chilvers. Oxford University Press, 1998.</ref><ref>"Pop art", ''The Concise Oxford Dictionary of Art Terms'', Michael Clarke, Oxford University Press, 2001.</ref> (Both versions agree that the term was used in [[Independent Group]] discussions by mid-1955.)

"Pop art" as a moniker was then used in discussions by IG members in the Second Session of the IG in 1955, and the specific term "pop art" first appeared in published print in the article "But Today We Collect Ads" by IG members [[Alison and Peter Smithson]] in Ark magazine in 1956.<ref>Alison and Peter Smithson, "But Today We Collect Ads", reprinted on page 54 in ''Modern Dreams The Rise and Fall of Pop'', published by ICA and MIT, ISBN 0-262-73081-2</ref> However, the term is often credited to [[United Kingdom|British]] [[art critic]]/[[curator]] [[Lawrence Alloway]] for his 1958 essay titled ''The Arts and the Mass Media'', even though the precise language he uses is "popular mass culture".<ref>Lawrence Alloway, "The Arts and the Mass Media," Architectural Design & Construction, February 1958.</ref> Nevertheless, Alloway was one of the leading critics to defend the inclusion of the imagery of mass culture in the fine arts.

In London, the annual [[Royal Society of British Artists|Royal Society of British Artists (RBA)]] exhibition of young talent in 1960 first showed American pop influences. In January 1961, the most famous RBA-''Young Contemporaries'' of all put [[David Hockney]], the American [[R B Kitaj]], New Zealander [[Billy Apple]], [[Allen Jones (sculptor)|Allen Jones]], [[Derek Boshier]], [[Joe Tilson]], [[Patrick Caulfield]], [[Peter Phillips (artist)|Peter Phillips]] and [[Peter Blake (artist)|Peter Blake]] on the map; Apple designed the posters and invitations for both the 1961 and 1962 Young Contemporaries exhibitions.<ref name="Barton 2010 11–21">{{cite book|last=Barton|first=Christina|title=Billy Apple: British and American Works 1960-69|year=2010|publisher=The Mayor Gallery|location=London|isbn=978-0-9558367-3-2|pages=11–21}}</ref> Hockney, Kitaj and Blake went on to win prizes at the John-Moores-Exhibition in Liverpool in the same year. Apple and Hockney traveled together to New York during the Royal College's 1961 summer break, which is when Apple first made contact with Andy Warhol – both later moved to the United States and Apple became involved with the New York pop art scene.<ref name="Barton 2010 11–21" />

==United States==
[[File:Roy Lichtenstein Drowning Girl.jpg|thumb|left|[[Roy Lichtenstein]], ''[[Drowning Girl]]'', 1963, on display at the [[Museum of Modern Art, New York]].|alt=A woman's crying face is overwhelmed by waves as she thinks, "I don't care! I'd rather sink than call Brad for help!"]]
Although pop art began in the late 1950s, in America it was given its greatest impetus during the 1960s. The term "pop art" was officially introduced in December 1962; the occasion was a "Symposium on Pop Art" organized by the [[Museum of Modern Art]].<ref name="SchermanTony">Scherman, Tony. "When Pop Turned the Art World Upside Down." ''American Heritage'' 52.1 (February 2001), 68.</ref> By this time, American advertising had adopted many elements and inflections of modern art and functioned at a very sophisticated level. Consequently, American artists had to search deeper for dramatic styles that would distance art from the well-designed and clever commercial materials.<ref name="high" /> As the British viewed American popular culture imagery from a somewhat removed perspective, their views were often instilled with romantic, sentimental and humorous overtones. By contrast, American artists, bombarded every day with the diversity of mass-produced imagery, produced work that was generally more bold and aggressive.<ref name="mod" />
[[File:Cheddar Cheese crop from Campbells Soup Cans MOMA.jpg|thumb|The ''Cheddar Cheese'' canvas from [[Andy Warhol]]'s ''Campbell's Soup Cans'', 1962.]]
Two important painters in the establishment of America's pop art vocabulary were [[Jasper Johns]] and [[Robert Rauschenberg]].<ref name="mod" /> While the paintings of Rauschenberg have relationships to the earlier work of [[Kurt Schwitters]] and other [[Dada]] artists, his concern was for the social issues of the moment. His approach was to create art out of ephemeral materials. By using topical events in the life of everyday America, he gave his work a unique quality.<ref name="mod" /><ref>[[Irving Sandler|Sandler, Irving H.]] ''The New York School: The Painters and Sculptors of the Fifties,'' New York: Harper & Row, 1978. ISBN 0-06-438505-1 pp. 174–195, ''Rauschenberg and Johns''; pp. 103–111, ''Rivers and the gestural realists''.</ref> Johns' and Rauschenberg's work of the 1950s is classified as [[Neo-Dada]], and is visually distinct from the "classic" American pop art which began in the early 1960s.<ref>Robert Rosenblum, "Jasper Johns" ''Art International'' (September 1960): 75.</ref><ref>Hapgood,  Susan, ''Neo-Dada: Redefining Art, 1958-62''. New York: Universe Books, 1994.</ref>

[[Roy Lichtenstein]] is of equal importance to American pop art. His work, and its use of [[parody]], probably defines the basic premise of pop art better than any other.<ref name="mod" /> Selecting the old-fashioned comic strip as subject matter, Lichtenstein produces a hard-edged, precise composition that documents while also parodying in a soft manner. Lichtenstein used [[Oil paint|oil]] and [[Magna paint]] in his best known works, such as ''[[Drowning Girl]]'' (1963), which was [[Appropriation (art)|appropriated]] from the lead story in [[DC Comics]]' ''Secret Hearts'' #83. (''Drowning Girl'' is part of the collection of the [[Museum of Modern Art]].)<ref name="rlf-Hendrickson31">{{harvnb|Hendrickson| 1988| p= 31 }}</ref> His work features thick outlines, bold colors and [[Ben-Day dots]] to represent certain colors, as if created by photographic reproduction. Lichtenstein said, "[abstract expressionists] put things down on the canvas and responded to what they had done, to the color positions and sizes. My style looks completely different, but the nature of putting down lines pretty much is the same; mine just don't come out looking calligraphic, like Pollock's or Kline's."<ref>{{cite journal
 |url=https://query.nytimes.com/gst/fullpage.html?res=9B03E0DF103AF933A0575AC0A961958260&sec=&spon=&pagewanted=3
 |title=Roy Lichtenstein, Pop Master, Dies at 73
 |first=Michael
 |last=Kimmelman
 |date=September 30, 1997
 |journal=New York Times
 |accessdate=November 12, 2007
}}</ref> Pop art merges popular and mass culture with fine art while injecting humor, irony, and recognizable imagery/content into the mix.

The paintings of Lichtenstein, like those of Andy Warhol, [[Tom Wesselmann]] and others, share a direct attachment to the commonplace image of American popular culture, but also treat the subject in an impersonal manner clearly illustrating the idealization of mass production.<ref name="mod" /> Andy Warhol is probably the most famous figure in pop art. In fact, art critic Arthur Danto once called Warhol "the nearest thing to a philosophical genius the history of art has produced".<ref name="SchermanTony" />  Warhol attempted to take pop beyond an artistic style to a life style, and his work often displays a lack of human affectation that dispenses with the irony and parody of many of his peers.<ref>Michelson, Annette, Buchloh, B. H. D. (eds) ''Andy Warhol'' (October Files), MIT Press, 2001.</ref><ref>Warhol, Andy. ''The Philosophy of Andy Warhol, from A to B and back again''. Harcourt Brace Jovanovich, 1975</ref>

===Early U.S. exhibitions===
[[Claes Oldenburg]], [[Jim Dine]] and [[Tom Wesselmann]] had their first shows in the [[Judson Memorial Church#Sponsorship of the arts|Judson Gallery]] in 1959 and 1960 and later in 1960 through 1964 along with [[James Rosenquist]], [[George Segal (artist)|George Segal]] and others at the [[Green Gallery]] on 57th Street in Manhattan. In 1960, Martha Jackson showed [[Installation art|installations]] and [[Assemblage art|assemblages]], ''New Media - New Forms'' featured [[Hans Arp]], [[Kurt Schwitters]], [[Jasper Johns]], Claes Oldenburg, [[Robert Rauschenberg]], Jim Dine and [[May Wilson]]. 1961 was the year of Martha Jackson's spring show, ''Environments, Situations, Spaces''.<ref>{{cite web|url=http://www.moma.org/collection/browse_results.php?criteria=O:AD:E:4397&page_number=12&template_id=1&sort_order=1 |title=The Collection |website=MoMA.org |date= |accessdate=2015-12-30}}</ref><ref>{{cite web|url=http://www.tfaoi.com/newsm1/n1m651.htm |title=The Great American Pop Art Store: Multiples of the Sixties |website=Tfaoi.com |date= |accessdate=2015-12-30}}</ref> Andy Warhol held his first solo exhibition in Los Angeles in July 1962 at [[Ferus Gallery|Irving Blum's Ferus Gallery]], where he showed 32 paintings of Campell's soup cans, one for every flavor. Warhol sold the set of paintings to Blum for $1,000; in 1996, when the Museum of Modern Art acquired it, the set was valued at $15 million.<ref name="SchermanTony" />

Donald Factor, the son of [[Max Factor, Jr.]], and an art collector and co-editor of [[avant garde]] [[literary magazine]] [[Nomad (magazine)|''Nomad'']], wrote an essay in the magazine's last issue, ''Nomad/New York''. The essay was one of the first on what would become known as pop art, though Factor did not use the term. The essay, "Four Artists", focused on Roy Lichtenstein, [[James Rosenquist]], Jim Dine, and Claes Oldenburg.<ref name="Diggory">Diggory (2013).</ref>

In the 1960s, Oldenburg, who became associated with the pop art movement, created many ''[[happening]]s'', which were [[performance art]]-related productions of that time. The name he gave to his own productions was "Ray Gun Theater". The cast of colleagues in his performances included: artists [[Lucas Samaras]], [[Tom Wesselman]], [[Carolee Schneemann]], [[Oyvind Fahlstrom]] and [[Richard Artschwager]]; dealer Annina Nosei; [[art critic]] [[Barbara Rose]]; and screenwriter [[Rudy Wurlitzer]].<ref name="articles.latimes.com">Kristine McKenna (July 2, 1995), [http://articles.latimes.com/1995-07-02/entertainment/ca-19310_1_claes-oldenburg When Bigger Is Better: Claes Oldenburg has spent the past 35 years blowing up and redefining everyday objects, all in the name of getting art off its pedestal] ''[[Los Angeles Times]]''.</ref> His first wife, Patty Mucha, who sewed many of his early soft sculptures, was a constant performer in his happenings. This brash, often humorous, approach to art was at great odds with the prevailing sensibility that, by its nature, art dealt with "profound" expressions or ideas. In December 1961, he rented a store on Manhattan's [[Lower East Side]] to house ''The Store'', a month-long installation he had first presented at the Martha Jackson Gallery in New York, stocked with sculptures roughly in the form of consumer goods.<ref name="articles.latimes.com" />

Opening in 1962, [[Willem de Kooning|Willem de Kooning's]] New York art dealer, the [[Sidney Janis]] Gallery, organized the groundbreaking ''International Exhibition of the New Realists'', a survey of new-to-the-scene American, French, Swiss, Italian ''[[New Realism]]'', and British pop art. The fifty-four artists shown included [[Richard Lindner (painter)|Richard Lindner]], [[Wayne Thiebaud]], Roy Lichtenstein (and his painting [[Blam (Roy Lichtenstein)|Blam]]), Andy Warhol, Claes Oldenburg, [[James Rosenquist]], Jim Dine, [[Robert Indiana]], [[Tom Wesselmann]], [[George Segal (artist)|George Segal]], Peter Phillips, Peter Blake (''The Love Wall'' from 1961), [[Yves Klein]], [[Arman]], [[Daniel Spoerri]], [[Christo]] and [[Mimmo Rotella]]. The show was seen by Europeans [[Martial Raysse]], [[Niki de Saint-Phalle]] and [[Jean Tinguely]] in New York, who were stunned by the size and look of the American artwork. Also shown were [[Marisol Escobar|Marisol]], [[Mario Schifano]], [[Enrico Baj]] and [[Öyvind Fahlström]]. Janis lost some of his abstract expressionist artists when [[Mark Rothko]], [[Robert Motherwell]], [[Adolph Gottlieb]] and [[Philip Guston]] quit the gallery, but gained Dine, Oldenburg, Segal and Wesselmann.<ref>{{cite web|url=https://books.google.com/books?id=hLgyznMrORsC&pg=PA83&lpg=PA83&dq=New+Realism+at+Sidney+Janis&source=bl&ots=2CQXoBBdiS&sig=HBQNsOqvkZvHEEFnWhnhTls-EH4&hl=en&ei=07IbS67CA4TBlAeTt63vCQ&sa=X&oi=book_result&ct=result&resnum=10&ved=0CCEQ6AEwCQ#v=onepage&q=New%20Realism%20at%20Sidney%20Janis&f=false |title=Andy Warhol, Poetry, and Gossip in the 1960s |author=Reva Wolf |page=83 |website=Books.google.com |date= |accessdate=2015-12-30}}</ref> At an opening-night soiree thrown by collector Burton Tremaine, Willem de Kooning appeared and was turned away by Tremaine, who ironically owned a number of de Kooning's works. Rosenquist recalled: "at that moment I thought, something in the art world has definitely changed".<ref name="SchermanTony" /> Turning away a respected abstract artist proved that, as early as 1962, the pop art movement had begun to dominate art culture in New York.

A bit earlier, on the [[West Coast of the United States|West Coast]], Roy Lichtenstein, Jim Dine and Andy Warhol from New York City; [[Phillip Hefferton]] and [[Robert Dowd]] from Detroit; [[Edward Ruscha]] and [[Joe Goode]] from Oklahoma City; and Wayne Thiebaud from California were included in the ''[[New Painting of Common Objects]]'' show. This first pop art museum exhibition in America was curated by [[Walter Hopps]] at the [[Pasadena Art Museum]].<ref>{{cite web|url=http://www.nortonsimon.org/about/history.aspx |title=Museum History » Norton Simon Museum |website=Nortonsimon.org |date= |accessdate=2015-12-30}}</ref>  Pop art was ready to change the art world. New York followed Pasadena in 1963, when the [[Solomon R. Guggenheim Museum|Guggenheim Museum]] exhibited ''Six Painters and the Object'', curated by [[Lawrence Alloway]]. The artists were Jim Dine, Jasper Johns, Roy Lichtenstein, Robert Rauschenberg, James Rosenquist, and Andy Warhol.<ref>{{cite web|url=http://www.worldcat.org/oclc/360205683 |title=Six painters and the object. Lawrence Alloway [curator, conceived and prepared this exhibition and the catalogue] (Computer file) |website=WorldCat.org |date=2009-07-24 |accessdate=2015-12-30}}</ref> Another pivotal early exhibition was ''The American Supermarket'' organised by the Bianchini Gallery in 1964. The show was presented as a typical small supermarket environment, except that everything in it—the produce, canned goods, meat, posters on the wall, etc.—was created by prominent pop artists of the time, including Apple, Warhol, Lichtenstein, Wesselmann, Oldenburg, and Johns. This project was recreated in 2002 as part of the [[Tate Gallery]]'s ''Shopping: A Century of Art and Consumer Culture''.<ref>{{cite web|last=Gayford|first=Martin|title=Still life at the check-out|url=http://www.telegraph.co.uk/culture/3587307/Still-life-at-the-check-out.html|work=The Telegraph|publisher=Telegraph Media Group Ltd|accessdate=28 November 2012|date=2002-12-19}}</ref>

By 1962, pop artists started exhibiting in commercial galleries in New York and Los Angeles; for some, it was their first commercial one-man show. The [[Ferus Gallery]] presented Andy Warhol in Los Angeles (and [[Ed Ruscha]] in 1963). In New York, the [[Green Gallery]] showed Rosenquist, Segal, Oldenburg, and Wesselmann. The [[Stable Gallery]] showed R. Indiana and Warhol (in his first New York show). The [[Leo Castelli|Leo Castelli Gallery]] presented Rauschenberg, Johns, and Lichtenstein. Martha Jackson showed Jim Dine and Allen Stone showed Wayne Thiebaud. By 1966, after the Green Gallery and the Ferus Gallery closed, the Leo Castelli Gallery represented Rosenquist, Warhol, Rauschenberg, Johns, Lichtenstein and Ruscha. The Sidney Janis Gallery represented Oldenburg, Segal, Dine, Wesselmann and Marisol, while Allen Stone continued to represent Thiebaud, and Martha Jackson continued representing Robert Indiana.<ref>''Pop Artists: Andy Warhol, Pop Art, Roy Lichtenstein, Jasper Johns, Peter Max, Erró, David Hockney, Wally Hedrick, Michael Leavitt'' (May 20, 2010) Reprinted: 2010, General Books, Memphis, Tennessee, USA, ISBN 978-1-155-48349-8, ISBN 1-155-48349-9.</ref>

In 1968, the ''São Paulo 9 Exhibition – Environment U.S.A.: 1957–1967'' featured the "Who's Who" of pop art. Considered as a summation of the ''classical phase'' of the American pop art period, the exhibit was curated by William Seitz. The artists were [[Edward Hopper]], [[James Gill (artist)|James Gill]], [[Robert Indiana]], [[Jasper Johns]], [[Roy Lichtenstein]], [[Claes Oldenburg]], [[Robert Rauschenberg]], [[Andy Warhol]] and [[Tom Wesselmann]].<ref>Jim Edwards, William Emboden, David McCarthy: Uncommonplaces: The Art of James Francis Gill, 2005, p.54</ref>

==Spain==
In Spain, the study of pop art is associated with the "new figurative", which arose from the roots of the crisis of [[informalism]]. [[Eduardo Arroyo]] could be said to fit within the pop art trend, on account of his interest in the environment, his critique of our media culture which incorporates icons of both [[mass media]] communication and the history of painting, and his scorn for nearly all established artistic styles. However, the Spanish artist who could be considered most authentically part of "pop" art is Alfredo Alcaín, because of the use he makes of popular images and empty spaces in his compositions.

Also in the category of Spanish pop art is the "Chronicle Team" (''El Equipo Crónica''), which existed in [[Valencia (city in Spain)|Valencia]] between 1964 and 1981, formed by the artists [[Manolo Valdés]] and Rafael Solbes. Their movement can be characterized as "pop" because of its use of comics and publicity images and its simplification of images and photographic compositions. [[Film]]maker [[Pedro Almodóvar]] emerged from Madrid's "La Movida" subculture of the 1970s making low budget [[Super 8 mm film|super 8]] pop art movies, and he was subsequently called the Andy Warhol of Spain by the media at the time. In the book ''Almodovar on Almodovar'', he is quoted as saying that the 1950s film "Funny Face" was a central inspiration for his work. One pop trademark in Almodovar's films is that he always produces a fake commercial to be inserted into a scene.

==Japan==
In Japan, pop art evolved from the nation's prominent [[avant-garde]] scene. The use of images of the modern world, copied from magazines in the photomontage-style paintings produced by [[Harue Koga]] in the late 1920s and early 1930s, foreshadowed elements of pop art.<ref name="eskolahk">{{cite book|last=Eskola|first=Jack|title=Harue Koga: David Bowie of the Early 20th Century Japanese Art Avant-garde|year=2015|publisher=Kindle, e-book}}</ref> The work of [[Yayoi Kusama]] contributed to the development of pop art and influenced many other artists, including Andy Warhol.<ref>{{cite web|author= |url=http://www.timeout.com/london/feature/2175/interview-yayoi-kusama |title=Yayoi Kusama interview – Yayoi Kusama exhibition |website=Timeout.com |date=2013-01-30 |accessdate=2015-12-30}}</ref><ref>[http://art.sy/artist/yayoi-kusama ] {{webarchive |url=https://web.archive.org/web/20121101110936/http://art.sy/artist/yayoi-kusama |date=November 1, 2012 }}</ref> In the mid-1960s, graphic designer [[Tadanori Yokoo]] became one of the most successful pop artists and an international symbol for Japanese pop art. He is well known for his advertisements and creating artwork for pop culture icons such as commissions from [[The Beatles]], [[Marilyn Monroe]], and [[Elizabeth Taylor]], among others.<ref>{{cite web|url=http://www.adcglobal.org/archive/hof/2000/?id=205 |title=Tadanori Yokoo : ADC • Global Awards & Club |website=Adcglobal.org |date=1936-06-27 |accessdate=2015-12-30}}</ref> Another leading pop artist at that time was [[Keiichi Tanaami]]. Iconic characters from Japanese [[manga]] and [[anime]] have also become symbols for pop art, such as [[Speed Racer]] and [[Astro Boy]]. Japanese manga and anime also influenced later pop artists such as [[Takashi Murakami]] and his [[superflat]] movement.

==Italy==
In Italy, by 1964, pop art was known and took different forms, such as the "Scuola di Piazza del Popolo" in Rome, with pop artists such as [[Mario Schifano]], [[Franco Angeli]], [[Giosetta Fioroni]], [[Tano Festa]], [[Claudio Cintoli]], and some artworks by [[Piero Manzoni]], [[Lucio Del Pezzo]], [[Mimmo Rotella]] and [[Valerio Adami]].

Italian pop art originated in 1950s culture – the works of the artists [[Enrico Baj]] and [[Mimmo Rotella]] to be precise, rightly considered the forerunners of this scene. In fact, it was around 1958–1959 that Baj and Rotella abandoned their previous careers (which might be generically defined as belonging to a ''non-representational genre'', despite being thoroughly post-Dadaist), to catapult themselves into a new world of images, and the reflections on them, which was springing up all around them. Rotella's torn posters showed an ever more figurative taste, often explicitly and deliberately referring to the great icons of the times. Baj's compositions were steeped in contemporary ''kitsch'', which turned out to be a "gold mine" of images and the stimulus for an entire generation of artists.

The novelty came from the new visual panorama, both inside "domestic walls" and out-of-doors. Cars, road signs, television, all the "new world", everything can belong to the world of art, which itself is new. In this respect, Italian pop art takes the same ideological path as that of the international scene. The only thing that changes is the iconography and, in some cases, the presence of a more critical attitude toward it. Even in this case, the prototypes can be traced back to the works of Rotella and Baj, both far from neutral in their relationship with society. Yet this is not an exclusive element; there is a long line of artists, including [[Gianni Ruffi]], [[Roberto Barni]], [[Silvio Pasotti]], [[Umberto Bignardi]], and [[Claudio Cintoli]], who take on reality as a toy, as a great pool of imagery from which to draw material with disenchantment and frivolity, questioning the traditional linguistic role models with a renewed spirit of "let me have fun" à la [[Aldo Palazzeschi]].<ref>{{cite web|url=http://www.comune.modena.it/galleria/exhibitions/past-exhibitions/2005/pop-art-italia-1958-1968-1 |title=POP ART ITALIA 1958-1968 — Galleria Civica |website=Comune.modena.it |date= |accessdate=2015-12-30}}</ref>

==Belgium==
In [[Belgium]], pop art was represented by [[Paul Van Hoeydonck]], whose sculpture ''[[Fallen Astronaut]]'' was left on the moon during one of the moon missions. Internationally recognized artists such as [[Marcel Broodthaers]] ('' 'vous êtes doll? "'') and [[Panamarenko]] are indebted to the pop art movement; Broodthaers's great influence was [[George Segal]].  Another well-known artist, [[Roger Raveel]], mounted a birdcage with a real live pigeon in one of his paintings. By the end of the 1960s and early 1970s, pop art references disappeared from the work of these artists when they started to adopt a more critical attitude towards America because of the [[Vietnam War]]'s increasingly gruesome character. Panamarenko, however, has retained the irony inherent in the pop art movement up to the present day.

==Netherlands==
While there was no formal pop art movement in the [[Netherlands]], there were a group of artists that spent time in New York during the early years of pop art, and drew inspiration from the international pop art movement. Representatives of Dutch pop art include [[Daan van Golden]], [[Gustave Asselbergs]], [[Jacques Frenken]], [[Jan Cremer]], [[Wim T. Schippers]], and [[Woody van Amen]]. They opposed the Dutch [[petit bourgeois]] mentality by creating humorous works with a serious undertone. Examples of this nature include ''Sex O'Clock,'' by Woody van Amen, and ''Crucifix / Target'', by Jacques Frenken.<ref>{{cite web|url=http://www.8weekly.nl/artikel/2701/ |title=Dutch Pop Art & The Sixties - Weg met de vertrutting! |website=8weekly.nl |date= |accessdate=2015-12-30}}</ref>

==Russian Federation==
[[Russia]] was a little late to become part of the pop art movement, and some of the artwork that resembles pop art only surfaced around the early 1970s. Russia was a communist country at that point and bold artistic statements were closely monitored. Russia's own version of pop art was [[Soviet]]-themed and was referred to as ''[[Sots Art]]''. After 1991, the Communist Party lost its power and the Russian revolution was beginning, and with it came a freedom to express. That is when pop art in Russia took on another form, epitomised by [[Dmitri Vrubel]] with his painting titled ''[[My God, Help Me to Survive This Deadly Love]]'' in 1990. One might argue that the Soviet posters made in the 1950s to promote the wealth of the nation were in itself a form of pop art.<ref>[http://www.canofart.com]  {{webarchive |url=https://web.archive.org/web/20130607022802/http://www.canofart.com |date=June 7, 2013 }}</ref>

==Painting and sculpture examples==
<gallery widths="180px" heights="140px" perrow="4">
File:Jasper Johns's 'Flag', Encaustic, oil and collage on fabric mounted on plywood,1954-55.jpg|[[Jasper Johns]], 1954–1955 ''[[Flag (painting)|Flag]]''
File:Marilyndiptych.jpg|[[Andy Warhol]], ''[[Marilyn Diptych]]'' 1962
[[File:Warhol-Campbell Soup-1-screenprint-1968.jpg|[[Andy Warhol]], ''[[Campbell's Soup Cans|Campbell's Soup I]]'', 1968]]
File:Typewriter-eraser.JPG|[[Claes Oldenburg]] and [[Coosje van Bruggen]], ''[[Typewriter Eraser, Scale X]]'', 1999, painted stainless steel and fiberglass, [[National Gallery of Art]] Sculpture Garden, [[Washington, DC]]
<!-- Deleted image removed: Image:'The Robe Following Her - 4', oil on canvas painting by Jim Dine, 1984-5.jpg|[[Jim Dine]], 1984–1985 ''The Robe Following Her'' -->
</gallery>

==Notable artists==

{{columns-list|colwidth=20em|
*[[Billy Apple]]
*[[Evelyne Axell]]
*[[Peter Blake (artist)|Sir Peter Blake]]
*[[Derek Boshier]]
*[[Pauline Boty]]
*[[Patrick Caulfield]]
*[[Allan D'Arcangelo]]
*[[Jim Dine]]
*[[Burhan Dogancay]]
*[[Rosalyn Drexler]]
*[[Robert Dowd]]
*[[Ken Elias]]
*[[Erró]]
*[[Marisol Escobar]]
*[[James Gill (artist)|James Gill]]
*[[Bruce Gray (sculptor)]]
*[[Red Grooms]]
*[[Richard Hamilton (artist)|Richard Hamilton]]
*[[Keith Haring]]
*[[Jann Haworth]]
*[[David Hockney]]
*[[Dorothy Iannone]]
*[[Robert Indiana]]
*[[Jasper Johns]]
*[[Allen Jones (sculptor)|Allen Jones]]
*[[Alex Katz]]
*[[Corita Kent]]
*[[Konrad Klapheck]]
*[[Kiki Kogelnik]]
*[[Nicholas Krushenick]]
*[[Yayoi Kusama]]
*[[Gerald Laing]]
*[[Roy Lichtenstein]]
*[[Richard Lindner (painter)|Richard Lindner]]
*[[John McHale (artist)|John McHale]]
*[[Peter Max]]
*[[Marta Minujin]]
*[[Takashi Murakami]]
*[[Yoshitomo Nara]]
*[[Claes Oldenburg]]
*[[Julian Opie]]
*[[Eduardo Paolozzi]]
*[[Peter Phillips (artist)|Peter Phillips]]
*[[Sigmar Polke]]
*[[Hariton Pushwagner]]
*[[Mel Ramos]]
*[[Robert Rauschenberg]]
*[[Larry Rivers]]
*[[James Rizzi]]
*[[James Rosenquist]]
*[[Ed Ruscha]]
*[[Niki de Saint Phalle]]
*[[Peter Saul]]
*[[George Segal (artist)|George Segal]]
*[[Colin Self]]
*[[Marjorie Strider]]
*[[Aya Takano]]
*[[Wayne Thiebaud]]
*[[Joe Tilson]]
*[[Andy Warhol]]
*[[Idelle Weber]]
*[[John Wesley (artist)|John Wesley]]
*[[Tom Wesselmann]]
}}

==See also==
{{columns-list|colwidth=20em|
*[[Art pop]]
*[[Chicago Imagists]]
*[[Ferus Gallery]]
*[[Sidney Janis]]
*[[Leo Castelli]]
*[[Green Gallery]]
*[[New Painting of Common Objects]]
*[[Figuration Libre]] (art movement)
*[[Lowbrow (art movement)]]
*[[Nouveau réalisme]]
*[[Neo-pop]]
*[[Op art]]
*[[Plop art]]
*[[Retro#Retro art|Retro art]]
*[[Superflat]]
*[[SoFlo Superflat]]
}}

==Notes and references==
{{Reflist|30em}}

==Further reading==
*Diggory, Terence (2013) ''Encyclopedia of the New York School Poets'' (Facts on File Library of American Literature). ISBN 978-1-4381-4066-7
*Francis, Mark and Hal Foster. ''Pop''. London and New York: Phaidon, 2010.
*Haskell, Barbara. ''BLAM! The Explosion of Pop, Minimalism and Performance 1958-1964''. New York: W.W. Norton & Company, Inc. in association with the Whitney Museum of American Art, 1984.
*Lippard, Lucy R. ''Pop Art, with contributions by Lawrence Alloway, Nancy Marmer, Nicolas Calas,'' Frederick A. Praeger, New York, 1966.
*MoMa. "A symposium on Pop Art." ''Arts Magazine'', April 1963, pp.&nbsp;36–45. Transcript of symposium was held at The Museum of Modern Art in 1962. Transcript reprinted at [http://popartmachine.com/masters/article/190 popartmachine.com/masters/article/190].

==External links==
{{Commons category}}{{Wikiquote}}
*[https://www.moma.org/learn/moma_learning/themes/pop-art ''Pop Art: A Brief History'', MoMA Learning]
*[https://www.metmuseum.org/toah/hi/hi_mcpopart.htm ''Pop Art in Modern and Contemporary Art'', The Met]
*[http://www.brooklynmuseum.org/exhibitions/seductive_subversion/ Brooklyn Museum Exhibitions: Seductive Subversion: Women Pop Artists, 1958–1968, Oct. 2010-Jan. 2011]
*[http://www.brooklynmuseum.org/exhibitions/seductive_subversion/wiki/ Brooklyn Museum, Wiki/Pop] (Women Pop Artists)
*[http://www.tate.org.uk/learn/online-resources/glossary/p/pop-art Tate Glossary term for Pop art]

{{Westernart}}
{{Avant-garde}}
{{Modernism}}
{{Appropriation in the Arts}}

{{Authority control}}

[[Category:Pop art| ]]
[[Category:Avant-garde art]]
[[Category:Modern art]]
[[Category:Contemporary art]]
[[Category:Art movements]]
[[Category:Western art]]
[[Category:British art]]