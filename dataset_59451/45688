{{Infobox military person
|name= James C. Van Sice
|image= James C. Van Sice.jpg
|image_size= 250
|birth_date= 
|birth_place= 
|death_date= 
|death_place= 
|allegiance= [[United States]]
|branch= {{navy|United States|coast guard}}
|serviceyears= 1970&ndash;2007
|rank= [[Rear Admiral Upper Half]]
|commands=
{{plainlist|
*[[United States Coast Guard Academy]]
*Coast Guard Air Station Borinquen
}}
|battles=[[Cold War]]
|unit=
|awards=
{{plainlist|
*[[Defense Superior Service Medal]]
*[[Meritorious Service Medal (United States)|Meritorious Service Medal]]
*[[Coast Guard Commendation Medal]]
}}
}}

'''James C. Van Sice''' is a retired [[Rear Admiral]] (upper half) in the [[United States Coast Guard]]. He was the 38th [[Academic administration|Superintendent]] of the [[United States Coast Guard Academy]] in [[New London, Connecticut]], succeeding Rear Admiral [[Robert C. Olsen]]. He was succeeded by [[Rear Admiral]] J. Scott Burhoe.  Rear Admiral Van Sice retired from the Coast Guard in 2007. [http://www.navytimes.com/story.php?f=1-292925-2464225.php].

==Retirement==
In 2006 Van Sice requested retirement following an investigation. [http://tidewatermusings.peterstinson.com/2006/12/of-truth-and-of-consequences.html]. Cmdr. Jeff Carter, spokesman for the service, said information about Van Sice arose during the task force's review of the academy. Members of the task force decided the information was 'beyond the scope of their charter,' so they forwarded the matter to the Coast Guard chief of staff, Vice Adm. [[Robert Papp]], who had formed the task force.

Papp assigned another rear admiral, [[Larry L. Hereth]], to conduct an 'administrative investigation' into the matter. Carter said the administrative review didn't play into the job switch for Van Sice and that Van Sice's new job as director of personnel management shows that his superiors still have 'confidence he can effectively serve.' Van Sice's new job puts him in charge of pay, recruiting, housing and personnel policy for the entire 56,000 member Coast Guard. By the second week in January, Burhoe, who leaves a job as Assistant Commandant for governmental and public affairs in Washington, will be in charge of the school as it deals with the fallout from two recent sexual assault cases involving cadets. The timing of the job switch, according to a statement from Papp, is based on the expected release of the task force's findings next month. Burhoe will lend a 'fresh perspective' to act on the task force's recommendations for the school. Carter said Burhoe is somebody 'who has not been involved in any of the issues the task force is looking at.' It was after Van Sice took command of the 130-year-old school that an internal investigation resulted in sexual assault charges being brought against a cadet, Webster Smith.

==Education==
Van Sice graduated in 1974 from the United States Coast Guard Academy. He later earned a Master of Science in Engineering from [[Purdue University]] and a Master of Arts degree in business from [[Webster College]].

==Coast Guard assignments==
Rear Admiral Van Sice's first duty assignment was aboard the [[United States Coast Guard Cutter|Coast Guard Cutter]] [[USCGC Munro (WHEC-724)|''Munro'']] (WHEC 724) where he served as Anti-Submarine Warfare Officer and Deck Watch Officer. In 1976 he graduated from Naval Flight Training in [[Pensacola, Florida]].  An aircraft commander and instructor pilot in the [[C-131 Convair]] and [[HU-25 Falcon]] aircraft, Rear Admiral Van Sice has approximately 5,000 total hours in fixed and rotary wing aircraft.

Van Sice has served in a variety of command and staff assignments including Chief of Operations and Chief of Staff for the Eighth Coast Guard District in [[New Orleans, Louisiana]], Commander of [[Coast Guard Air Station Borinquen]], [[Puerto Rico]], and operational command experience on the [[Gulf of Mexico]] and in the [[Pacific Northwest]].

He served as the first Deputy Director of Operations, [[United States Northern Command]], [[Peterson Air Force Base, Colorado]]. In this position, he advised the Commander on operations to deter, prevent, defeat, and mitigate threats and aggression aimed at the United States, its territories and interests. He ensured effective air, land, and maritime defense throughout the assigned area of responsibility to include military assistance to civil authorities.

Prior to assuming the position of Superintendent at the Coast Guard Academy, Van Sice was the Director of Reserve and Training for the U.S. Coast Guard where he served as a member of the [[Reserve Forces Policy Board]], which advises the [[Secretary of Defense]] on Reserve matters.

==Awards and honors==
His awards include the [[Defense Superior Service Medal]], [[Meritorious Service Medal (United States)|Meritorious Service Medal]] (three awards), the [[Coast Guard Commendation Medal]], [[Coast Guard Achievement Medal]], and Commandant's Letter of Commendation Ribbon with Operational Distinguishing Device.

==Personal data==
Van Sice is a native of [[Wilmington, Delaware]]. He is married to the former Clarke Hutchinson of Wilmington, Delaware. Rear Admiral and Mrs. Van Sice have two children.

==References==
*[http://www.uscga.edu/about/super_bio.aspx Official Biography, from the Coast Guard Academy website]
{{Portal|United States Coast Guard}}


{{DEFAULTSORT:Van Sice, James}}
[[Category:Living people]]
[[Category:Year of birth missing (living people)]]
[[Category:United States Coast Guard admirals]]
[[Category:United States Coast Guard Academy alumni]]
[[Category:Purdue University alumni]]
[[Category:People from Wilmington, Delaware]]
[[Category:United States Coast Guard Aviation]]
[[Category:United States Naval Aviators]]
[[Category:Recipients of the Defense Superior Service Medal]]