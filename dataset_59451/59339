{{Distinguish|Pathological Society of Great Britain and Ireland}}

The '''Pathological Society of London''' was founded in 1846 for the "cultivation and promotion of [[Pathology]] by the exhibition and description of specimens, drawings, microscopic preparations, casts or models of morbid parts."<ref name=Dean824 />

Its first meeting was held in February 1847 at which C. J. B. Williams was elected as the society's first president and 106 members enrolled.<ref name=Dean823 /> Early members included [[Richard Bright (physician)|Richard Bright]], [[Golding Bird]], [[Sir William Gull, 1st Baronet|William Gull]], [[Sir William Jenner, 1st Baronet|William Jenner]], [[Henry Bence Jones]] and [[Richard Quain]].<ref name=Dean824 />

The society published 58 volumes of the ''[[Transactions of the Pathological Society of London]]''.

In 1907 it was merged with the [[Royal Medical and Chirurgical Society of London]] and other societies to become the [[Royal Society of Medicine]].

==Presidents==
{{columns-list|2|
*1906–1907 [[Philip Henry Pye-Smith]] (last President)
*1902–1906 [[John Burdon Sanderson]]
*1899-1902 Sir [[William Watson Cheyne]]
*1897–1899 [[Joseph Frank Payne]]
*1895–1897 Henry Trentham Butlin
*1893–1895 [[Frederick William Pavy]]
*1891-1893 [[George Murray Humphry]]
*1889–1891 [[William Howship Dickinson]]
*1887–1889 Sir [[James Paget]]
*1885–1887 [[John Syer Bristowe]]
*1883–1885 [[John Hulke]]
*1881–1883 [[Samuel Wilks]]
*1879–1880 [[Jonathan Hutchinson]]
*1877–1879 [[Charles Murchison (physician)|Charles Murchison]]
*1875–1877 [[George David Pollock|George D. Pollock]]
*1873–1875 [[William Jenner, 1st Baronet|Sir William Jenner]]
*1871–1873 [[John Hilton (surgeon)|John Hilton]]
*1869–1871 [[Richard Quain|Sir Richard Quain, 1st Baronet]]
*1867–1869 [[John Simon (pathologist)|Sir John Simon]]
*1865–1867 [[Thomas Bevill Peacock]]
*1863–1865 Sir [[Prescott Gardner Hewett]]
*1861-1863 [[James Copland (physician)|James Copland]]
*1859–1861 [[Sir William Fergusson, 1st Baronet|Sir William Fergusson]]
*1857–1859 [[Sir Thomas Watson, 1st Baronet]]
*1855–1857 James Moncrieff Arnott
*1853–1855 [[Benjamin Babington|Benjamin Guy Babington]]
*1852–1853 [[Caesar Hawkins|Caesar Henry Hawkins]]
*1850–1852 Peter Mere Latham
*1848–1850 [[Charles Aston Key]]
*1846-1848 Charles James Blasius Williams (first President)
}}

==References==
;Footnotes
{{Reflist|refs=
<ref name=Dean823>[[#Dean|Dean]], p. 823</ref>
<ref name=Dean824>[[#Dean|Dean]], p. 824</ref>
}}
;Sources
{{Refbegin}}
*{{Citation
  | last = Dean
  | first = H. R.
  | author-link = 
  | title = The Pathological Society of London
  | work = [[Proceedings of the Royal Society of Medicine]]
  | publisher = [[Royal Society of Medicine Press]]
  | date = October 1946
  | volume = 39
  | issue = 12
  | pmc = 2182434
  | ref = Dean
  | pmid=19993415
}}
*{{Citation
| url= https://archive.org/stream/transactionspat15unkngoog/transactionspat15unkngoog_djvu.txt
| title = Transactions of the Pathological Society
|accessdate = 2012-10-27}}
{{Refend}}

[[Category:Learned societies of the United Kingdom]]
[[Category:Medical and health organisations in London]]
[[Category:1846 establishments in England]]
[[Category:Medical associations based in the United Kingdom]]
[[Category:Organisations based in the City of London]]
[[Category:Pathology organizations]]
[[Category:Professional associations based in the United Kingdom]]
[[Category:Science and technology in London]]
[[Category:Scientific organizations established in 1846]]
[[Category:Scientific societies]]