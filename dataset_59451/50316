{{about|the manmade island|the natural island|Sir Bani Yas}}
{{refimprove|date=February 2012}}
[[File:Yas Marina Circuit + Ferrari World -Abu Dhabi.jpg|thumb|View of Yas Island]]
'''Yas Island''' ({{lang-ar|جزيرة ياس}}) is a man-made island in [[Abu Dhabi]], [[UAE]]. Yas Island can be reached by car, plane or boat.<ref>{{cite web|url=http://www.yasisland.ae/en/maps--tools/maps--directions/ |title=Yas island maps and directions |publisher=yasisland.ae |accessdate=2013-11-12}}</ref>

The island’s development project was initiated in 2006 by Abu Dhabi-based [[Aldar Properties]], with the aim of turning the island into a multi-purpose leisure, shopping and entertainment center at an estimated total cost of over $40 billion. This investment was planned as a multi-staged project to unfold in phases until 2018, with project stakeholders foreseeing the possibility of extending development by adding new venues and upgrading existing facilities.<ref>{{cite web|url=http://www.dubaifaqs.com/yas-island.php  |title=Yas Island leisure destination in Abu Dhabi |publisher=dubaifaqs.com |accessdate=2013-11-12}}</ref>

It occupies a total land area of 2,500 ha (25 km2), of which 1,700 ha will be claimed for development. Yas Island holds the [[Yas Marina Circuit]], which has hosted the [[Formula One]] Etihad Airways [[Abu Dhabi Grand Prix]] since 2009.<ref>{{cite web|url=http://www.dubaifaqs.com/yas-island.php  |title=Yas Island leisure destination in Abu Dhabi |publisher=dubaifaqs.com |accessdate=2013-11-12}}</ref>

Both business and leisure travelers visit Yas Island. Yas Island was named the world's leading tourism project at the [[World Travel Awards]] in November 2009.<ref>{{cite web|url=http://www.arabianbusiness.com/yas-island-named-world-s-leading-tourism-project-10964.html |title=Yas Island named World’s Leading Tourism Project |publisher=arabianbusiness.com |date=2009-11-11 |accessdate=2013-10-17}}</ref>

==Attractions==

=== Ferrari World, Abu Dhabi ===
{{main|Ferrari World}}
The Park’s foundation stone was laid on 3 November 2007. The development was completed in a little under three years, opening to the public for the first time on 27 October 2010.<ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/about-us/construction.aspx |title=Ferrari World Abu Dhabi Fun Facts |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>

[[Formula Rossa]], one of the park's most famous attractions, is the world’s fastest rollercoaster reaching a speed of 240&nbsp;kph (149mph).<ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/attractions/formula-rossa.aspx |title=World's Fastest roller coaster |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>

==== Restaurants and cafes ====
* Rosticceria Modena – Italian Restaurant <ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/shopping-and-dining/rosticceria-modena.aspx |title=Modena Meets Arabia |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>
* Il Podio – International Restaurant <ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/shopping-and-dining/il-podio.aspx |title=A Culinary Performance |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>
* Espresso Rosso – Café <ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/shopping-and-dining/espresso-rosso.aspx |title=
Where espresso is a way of life |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>
* Ristorante Cavallino – Italian Restaurant<ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/shopping-and-dining/ristorante-cavallino.aspx |title=Every bite tells a story |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>
* Mamma Rossella – Italian Restaurant<ref>{{cite web|url=http://www.ferrariworldabudhabi.com/en-gb/shopping-and-dining/mamma-rossella.aspx |title=At Home in Maranallo |publisher=ferrariworldabudhabi.com |accessdate=2013-10-17}}</ref>

=== Yas Waterworld ===

Yas Waterworld Surf’s up! Bubbles' Barrel slide has the largest surf-able sheet wave surf in the world.

==== Yas Waterworld Awards and Nominations ====
* Named the “second best waterpark in the world” by the Los Angeles Times - Global Ranking of the World’s Top 20 Waterparks feature in May 2013.<ref>{{cite web|url=http://www.latimes.com/travel/deals/themeparks/la-trb-top-20-water-parks-04201330-pg,0,3909060.photogallery?index=la-trb-top-20-water-parks-04201330-pg-002 |title=Photos: 20 best water parks in the world  |publisher=latimes.com |accessdate=2013-10-17}}</ref>
* Listed by CNN as one of the top 12 water parks in the world in July 2013.<ref>{{cite web|url=http://edition.cnn.com/2013/07/31/travel/worlds-best-water-parks/index.html  |title=12 of the world's best water parks  |publisher=http://edition.cnn.com |accessdate=2013-10-17}}</ref>
* Won the World Travel Awards award for the Middle East’s Leading Tourist Attraction of the year 2013.<ref>{{cite web|url=http://www.worldtravelawards.com/award-middle-easts-leading-tourist-attraction-2013 |title=Middle East's Leading Tourist Attraction 2013  |publisher=worldtravelawards.com |accessdate=2013-10-17}}</ref>
* Won the Leading Edge Award at the [[World Waterpark Association]]'s Annual Show, in Las Vegas, USA in October 2012.<ref>{{cite web|url=http://www.yaswaterworld.com/en/news/press-centre/yas-waterworld-abu-dhabi-takes-home-world-waterpark-associations-inaugural-leading-edge-award-1.html |title=Yas Waterworld Abu Dhabi takes home World Waterpark Association’s inaugural Leading Edge Award - Yas Waterworld  |publisher=yaswaterworld.com |accessdate=2013-10-17}}</ref>
* The Bandit Bomber is the first rollercoaster to be incorporated into a waterpark and was ranked amongst the world’s newest, biggest and baddest roller coasters for the summer of 2013, by CNN.<ref>{{cite web|url=http://edition.cnn.com/2013/05/27/travel/roller-coasters-2013/index.html |title=Newest, biggest, baddest roller coasters for summer  |publisher=http://edition.cnn.com |accessdate=2013-10-17}}</ref>

=== Yas Marina Circuit ===
{{main|Yas Marina Circuit}}
The full circuit length is 5.554&nbsp;km and can be configured in five different ways to accommodate a variety of motorsport events.  Yas Marina Circuit is the only motorsports venue in the world that offers covered and shaded grandstands throughout the facility, coupled with unique pit lanes that run partially beneath the track.<ref>{{cite web|url=http://www.yasmarinacircuit.com/en/about-yas-marina-circuit |title=ABOUT YAS MARINA CIRCUIT  |publisher=yasmarinacircuit.com |accessdate=2013-10-17}}</ref>

==== Other restaurants ====

Two charter boat companies operate from Yas Marina namely Captain Tony's and Azure Marine. Seawings operates flights to and from Dubai to Yas Marina and 20 minute Abu Dhabi tours from Yas Marina. 
<ref>{{cite web|url=http://www.cnmarinas.com/en/marinas/yas-marina |title=Welcome to Yas Marina, Abu Dhabi  |publisher=cnmarinas.com |accessdate=2013-10-17}}</ref>

=== Yas Links ===

Yas Links is an award-winning golf course designed by Kyle Philips, ranked 24th World’s best Golf courses outside the USA (Golf Digest USA, 2012). 1.8 million cubic metres of material were dredged to form the 18 Hole Championship Golf Course, Golf Club & Golf Shop, pool Area, air-conditioned swing studio and meeting and conference rooms for corporate events.<ref>{{cite web|url=http://www.yaslinks.com |title=Yas Links  |publisher=yaslinks.com |accessdate=2013-10-17}}</ref>

=== Yas Beach ===

Mangrove Tours launch daily from Yas Beach also courtesy of Noukhada Adventure Company.
<ref>{{cite web|url=http://www.yasbeach.ae  |title=Yas Beach|publisher=yasbeach.ae |accessdate=2013-10-17}}</ref>

=== Warner Bros. World Abu Dhabi ===
{{main|Warner Bros. World Abu Dhabi}}
Warner Bros. World Abu Dhabi<ref>{{cite web|url=http://www.wbworld.ae  |title=Warner Bros. World Abu Dhabi|publisher=wbworld.ae}}</ref> will bring together stories and characters from the studio's unparalleled portfolio of DC Comics super heroes universe including Batman, Superman and Wonder Women as well as Warner Bros. cartoons such as Bugs Bunny, Scooby-Doo and Tom and Jerry. Guests of all ages will be able to step inside Gotham City and Metropolis, and experience the cartoon worlds of Looney Tunes, Hanna-Barbera and more, all under one roof. The park is expected to be open in 2018, making it the world's third Warner Bros. theme park.<ref>http://www.themeparkinsider.com/flume/201604/5038/</ref>

=== SeaWorld Abu Dhabi ===
{{main article|SeaWorld Abu Dhabi}}
On December 13, 2016, [[SeaWorld Parks & Entertainment]] announced a new partnership with Miral to bring SeaWorld Abu Dhabi to Yas Island. The park, which will be situated to the northeast of [[Ferrari World]], is set to open in 2022 and will be the first SeaWorld without [[Killer whale|orcas]].<ref name="SeaWorld Abu Dhabi announcement">{{cite web|title=SeaWorld Announces Partnership with Miral to Develop SeaWorld Abu Dhabi|url=https://seaworldcares.com/seaworldabudhabi|website=seaworldcares.com|publisher=SeaWorld Parks & Entertainment|accessdate=13 December 2016}}</ref><ref name="SeaWorld AD-Orlando Sentinel">{{cite news|last1=Pedicini|first1=Sandra|title=SeaWorld announces new theme park in the Middle East, with no orcas|url=http://www.orlandosentinel.com/travel/attractions/seaworld/os-seaworld-theme-park-middle-east-abu-dhabi-20161213-story.html|accessdate=13 December 2016|work=OrlandoSentinel.com}}</ref>

== Hotels ==

=== Yas Viceroy Abu Dhabi ===
[[Yas Viceroy Abu Dhabi Hotel|Yas Viceroy Abu Dhabi]] is a five star hotel located on the Yas Marina and is the first hotel in world to be built over an [[F1]] race circuit.

=== Yas Plaza Hotels ===
The six Yas Plaza hotels includes:
* [[Crowne Plaza Abu Dhabi Yas Island]]
* [[Staybridge Suites Abu Dhabi Yas Island]]
* Radisson Blu Abu Dhabi Yas Island
* Park Inn Abu Dhabi Yas Island
* Rotana Yas Island
* Centro Yas Island

== Transportation ==

=== Yas Express ===

Yas express Saadiyat route shuttle service is the latest route (launched July 1) which interlinks St. Regis Saadiyat Island Resort and Park Hyatt Abu Dhabi with Yas Island’s attractions. Operating daily with 3 pickups in the morning and 3 drop offs in the afternoon, the shuttle service transports guests to Ferrari World Abu Dhabi and Yas Waterworld, Yas Island. The afternoon service offers three pickups from Ferrari World Abu Dhabi, dropping visitors back to each of the respective resorts on Saadiyat.<ref>{{cite web|url=http://www.yasisland.ae/en/maps--tools/yas-express-/ |title=Yas Express  |publisher=yasisland.ae |accessdate=2013-10-17}}</ref>

== Retail ==

=== Yas Mall ===
Yas Mall is one of Abu Dhabi's largest malls, located close to IKEA. In January 2017, Forbes recognized Yas Mall as one of the top five shopping malls in Abu Dhabi.<ref>{{cite web|url=http://www.forbes.com/sites/bishopjordan/2017/01/03/best-shopping-malls-abu-dhabi/ |title=The Five Best Shopping Malls In Abu Dhabi |accessdate=2017-01-08}}</ref>

=== IKEA ===
IKEA Yas Island is the flagship store and first retail outlet to open on Yas Island. The 33,000sqm store with a total sales area of 19,150sqm is the largest IKEA store in the [[MENA]] region and includes a play area for kids.<ref>{{cite web|url=http://www.ikea.com/ae/en/store/abu_dhabi |title=IKEA Yas Island   |publisher=ikea.com |accessdate=2013-10-17}}</ref> Previously located within the Marina Mall complex, the store was moved to Yas Island primarily due to limited room for expansion.

=== ACE Hardware ===
The ACE Hardware Yas Island is the UAE’s second largest ACE Hardware store, with 5,200 square meters of retail space.<ref>{{cite web|url= http://www.aceuae.com/Content/insidepage.aspx?pName=About_Us;Ace_Uae |title=Ace Hardware   |publisher=aceuae.com |accessdate=2013-10-17}}</ref>

==References==
{{reflist}}

==External links==
* [http://www.yasisland.ae/ Yas Island Official Website]

{{Developments in Abu Dhabi}}
{{Abu Dhabi}}

[[Category:Buildings and structures under construction in Abu Dhabi]]
[[Category:Islands of Abu Dhabi (emirate)]]
[[Category:Artificial islands of the United Arab Emirates]]