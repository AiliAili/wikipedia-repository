{{primary sources|date=May 2012}}
{{Infobox journal
| title = Academy of Management Learning & Education
| cover = 
| discipline = [[Management]]
| abbreviation = Acad. Manag. Learn. Edu.
| editor = Christine Quinn Trank
| publisher = [[Academy of Management]]
| country = United States
| frequency = Quarterly
| history = 2002–present
| impact = 2.458 
| impact-year = 2016
| openaccess =
| website = http://aom.org/amle/
| link1 = http://aom.org/amle/
| link1-name = Online access
| ISSN = 1537-260X
| eISSN = 1944-9585
| OCLC = 54659710
}}
'''''Academy of Management Learning and Education''''' is an [[academic journal]] sponsored by the [[Academy of Management]]. It covers management-related teaching, learning, and the management of business education.  According to the ''[[Journal Citation Reports]]'', the journal's 2013 [[impact factor]] is 2.121, ranking it 38th in the category "Management" (out of 172) and 18th in "Education & Educational Research" (out of 216).

== Editors ==
The founding [[editor-in-chief]] was Roy Lewicki ([[Ohio State University]]). The following editor was [[James R. Bailey]] ([[George Washington University]]). J. Ben Arbaugh ([[University of Wisconsin Oshkosh]]) was the third editor. Kenneth G. Brown ([[University of Iowa]], [[Tippie College of Business]]) finished his term in December 2014. The current editor is Christine Quinn Trank ([[Vanderbilt University]]).

== Controversy ==
The journal generated a great deal of controversy by publishing several articles by scholars like Jeffery Pfeffer, [[Henry Mintzberg]], and [[Sumantra Ghoshal]], that were critical of the efficacy of [[Master of Business Administration|MBA]] education. Other controversial publications include a critique of journal rankings<ref>{{cite web|url=http://journals.aomonline.org/amle/images/Adler2009.pdf |title=Archived copy |accessdate=2012-05-31 |deadurl=yes |archiveurl=https://web.archive.org/web/20120417131036/http://journals.aomonline.org/amle/images/Adler2009.pdf |archivedate=2012-04-17 |df= }}</ref> and a critique of self-assessments<ref>{{cite web|url=http://journals.aomonline.org/amle/images/Sitzmann2010.pdf |title=Archived copy |accessdate=2012-05-31 |deadurl=yes |archiveurl=https://web.archive.org/web/20120417111246/http://journals.aomonline.org/amle/images/Sitzmann2010.pdf |archivedate=2012-04-17 |df= }}</ref> as an [[assessment for learning]].

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://aom.org/amle/}}

[[Category:Business and management journals]]
[[Category:Education journals]]
[[Category:Publications established in 2002]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:2002 establishments in the United States]]