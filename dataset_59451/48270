{{advert|date=July 2016}}
[[File:Greg James in front of Tattoos Deluxe.jpg|frame|<div align="center">Greg James at Tattoos Deluxe on September 24, 2012</div>]]
'''Greg James''' is a well-known tattoo artist who is considered "one of America's finest."<ref name="influential">[http://tattooroadtrip.com/101-most-influential-people-in-tattoo-51-60/ 101 Most Influential People in Tattoo]</ref> Greg's tattoo designs and original artwork have appeared in magazines, books, documentaries, and at the [[Hard Rock Cafe]].

== Career ==
At the age of fourteen, Greg began drawing [[Flash (tattoo)|flash]] for his brother, Tennessee Dave, and eventually apprenticed with him in 1976.<ref name="Chronological Crue">{{cite web|url=http://members.ozemail.com.au/~cruekiss/gregjames.htm|title=Chronological Crue - Views - Mr. Cre Tattoo|work=ozemail.com.au}}</ref> From single-needle black-and-gray work to classic Americana tattoos and ultimately the large, custom Japanese artwork for which he has become famous, Greg tattooed continuously in Los Angeles (on Main Street in downtown L.A. and also the Pike, in Long Beach) for nine years as he continued to hone his craft.<ref name="influential" />

=== Sunset Strip ===
"I wanted to do superlative work. So, I made a choice. I would either quit tattooing or try to get in with [[Cliff Raven]] at Sunset Strip Tattoo. Cliff hired me. That's the beginning of the story."<ref name="influential" />

In 1985, Greg joined Sunset Strip Tattoo in Hollywood. Originally opened by [[Lyle Tuttle]] in the 1960s, the shop was then owned by Cliff Raven and subsequently Robert Benedetti, pioneers and innovators in the Japanese style of tattooing. For over twenty-five years, Greg played a large part in building the stature of the legendary studio, becoming "the real star at Sunset … the tattoo artist's tattooer."<ref name="gridskipper.com">{{cite web|url=http://gridskipper.com/archives/entries/61553/61553.php#pointmap%20|title=Marisa DiMattia's Guide to Getting Inked in LA - Gridskipper|work=gridskipper.com}}</ref>

During his almost forty-year career, Greg has also apprenticed numerous other tattoo artists, including Eric Blair<ref>[http://subculturetattoo.com/bioEric.htm Eric Blair Bio]</ref> of Subculture Tattoo and Dollar Bill of Sunset Strip Tattoo.

=== Notable clients ===

[[File:Russell Mitchell, CEO of Exile Cycles.jpg|thumb|alt=Russell Mitchell, CEO, Exile Cycles|Exile Cycles|300px|right|Russell Mitchell, CEO of Exile Cycles with tattoos by Greg James: full sleeves and two hikae, or chest panels, featuring a blue dragon and orange koi.]]
* Kirk Alley, owner of Eleven Eleven Tattoo<ref>{{cite web|url=http://www.bodyartweb.com/inked/maya.html|title=Artist of the Month - May|work=bodyartweb.com}}</ref>
* [[Pat Badger]], of Extreme<ref name="auto">HRH The Official Magazine of the Hard Rock Hotel & Casino, Summer/Fall 2004</ref>
* Frankie Banali, of [[Quiet Riot]]<ref name="auto"/>
* [[Nuno Bettencourt]], of Extreme<ref name="auto"/>
* Bobby Blotzer, of [[Ratt]]<ref name="auto"/>
* Chris Cashmore, owner of Tattoo Power, Canberra, Australia<ref>{{cite web|url=http://www.tattoo-guide-europa.de/in-chris.html|title=Tattoo Guide|date=4 May 2015|work=tattoo-guide-europa.de}}</ref>
* [[John Corabi]], of [[Mötley Crüe]]<ref>{{cite web|url=http://www.elizabethawhite.com/1997/12/15/john-corabi-the-union-asylum-interview/#.UGIoE6Req2x%20|title=John Corabi – The UNION Asylum Interview - Book Reviews & Editing by Elizabeth A. White - Crime Fiction, Thriller, Noir|work=elizabethawhite.com}}</ref>
* Mark Dektor, director<ref name="auto1">Tattoo Magazine, December 2012, Issue 280</ref>
* [[Sarah Michelle Gellar]], actress<ref name="tattoosymbol.com">{{cite web|url=http://www.tattoosymbol.com/greg.html|title=Greg James|work=tattoosymbol.com}}</ref>
* Pauly Greenwood, Drummer<ref>{{cite av media|url=https://www.youtube.com/watch?v=2RIcBs2qT2g%20|title=Pauly Greenwood @ Sunset Strip Tattoo Hollywood|date=21 February 2012|work=YouTube}}</ref>
* [[Tracii Guns]], of L. A. Guns<ref name="auto"/>
* Horicho, Japanese Tattoo Master<ref>{{cite web|url=http://www.bigtattooplanet.com/features/skin-deep/horisho-mindscape-tattoo-japan%20|title=Horisho Mindscape Tattoo, Japan|work=Big Tattoo Planet}}</ref>
* Johhny Hollywood, owner of 13 Roses Tattoo Parlour<ref>{{cite web|url=http://www.skinink.com/archives/0410/feature.html|title=Untitled Document|work=skinink.com}}</ref>
* [[Jesse James]], West Coast Choppers<ref name="tattoosymbol.com"/>
* [[Joan Jett]]<ref name="auto"/>
* [[Tommy Lee]], of [[Mötley Crüe]]<ref name="Chronological Crue" />
* Oliver Leiber, musician and producer<ref name="auto"/>
* [[Mick Mars]], of [[Mötley Crüe]]<ref name="Chronological Crue" />
* [[Vince Neil]], of [[Mötley Crüe]]<ref name="Chronological Crue" />
* Keith Nelson, of [[Buckcherry]]<ref name="auto"/>
* [[Ozzy Osbourne]]<ref name="auto"/>
* Ashley Purdy, of [[Black Veil Brides]]<ref>{{cite web|url=http://adella-music.blog.cz/|title=The Purdy Doll|author=adella-music|work=blog.cz}}</ref>
* [[Denise Richards]]<ref name="auto"/>
* [[Charlie Sheen]]<ref name="auto"/>
* [[Nikki Sixx]], of [[Mötley Crüe]]<ref name="Chronological Crue" />
* [[Rick Stratton]], makeup/special effects artist<ref name="youtube.com">{{cite av media|url=https://www.youtube.com/watch?v=TYWeXHukSQM|title=Greg James on Tattoo! Beauty, Art, and Pain|date=4 October 2012|work=YouTube}}</ref>
* [[Josh Todd|Joshua Todd]], of [[Buckcherry]]<ref name="auto"/>
* Léa Vendetta, owner of Léa's Lounge<ref>{{cite web|url=http://www.bigtattooplanet.com/features/artist-profile/l%C3%A9a-vendetta|title=Léa Vendetta|work=Big Tattoo Planet}}</ref>

=== Conventions ===

[[File:Custom Japanese dragon tattoo by Greg James.jpg|thumb|alt=Custom Japanese dragon tattoo by Greg James|Tattoos Deluxe|300px|right|Custom Japanese dragon tattoo by Greg James.]]

* Great Northwest Tattoo Convention
* Hollywood Park Tattoo Invitational<ref>{{cite av media|url=https://www.youtube.com/watch?v=O45Mte5k7J8|title=Hollywood park tattoo invitational 6/28/11 Greg James presented by hollywood keith tv|date=7 October 2011|work=YouTube}}</ref>
* Musink Tattoo Convention
* National Tattoo Association Convention<ref>{{cite web|url=http://www.nationaltattooassociation.com/artists.html|title=Artist List - Official National Tattoo Association Website|work=nationaltattooassociation.com}}</ref>
* New York City Tattoo Convention
* Tattoo Body Art Expo<ref>[http://www.bodyartexpo.com/HALLOFFAME.PHP Tattoo Body Art Expo Hall of Fame]</ref>
* Tokyo Tattoo Convention<ref>{{cite web|url=http://www.erikastanleyarts.com/#/tokyo-2000/4513209860|title=Erika Stanley Bio - Erika Stanley's Art & Tattoos|work=erikastanleyarts.com}}</ref>

=== Tattoos Deluxe ===

[[File:Custom Japanese backpiece tattoo by Greg James.jpg|thumb|alt=Custom Japanese backpiece tattoo by Greg James|Tattoos Deluxe|300px|right|Custom Japanese backpiece tattoo by Greg James of Tattoos Deluxe.]]

In September 2012, Greg opened his own tattoo studio, Tattoos Deluxe, in Sherman Oaks, CA (formerly Subculture Tattoo).<ref>{{cite web|url=http://tattoosdeluxe.com/|title=Greg James' Tattoos Deluxe|work=tattoosdeluxe.com}}</ref> Over time, Greg has emerged as a thoughtful and moderate spokesman for tattooing.<ref>{{cite web|url=http://www.filmoasis.com/about.html|title=FilmOasis: Company Bio|work=filmoasis.com}}</ref> Despite being at the top of the industry and known worldwide,<ref name="influential" /><ref name="gridskipper.com"/> Greg has also earned a reputation as "a genuinely nice guy",<ref name="tattoosymbol.com"/><ref>{{cite web|url=http://theselvedgeyard.wordpress.com/2010/04/20/ancient-art-of-the-japanese-tebori-tattoo-masters-ink-in-harmony/|title=ANCIENT ART OF THE JAPANESE TEBORI TATTOO MASTERS - INK IN HARMONY|work=The Selvedge Yard}}</ref> "quiet, modest and understated",<ref name="bigtattooplanet.com">{{cite web|url=http://www.bigtattooplanet.com/features/reader-profile/russell-mitchell-exile-ink%20|title=Russell Mitchell - Exile Ink|work=Big Tattoo Planet}}</ref> "…one of the nicest people I've ever met, and his work is truly amazing."<ref name="auto1"/>

Of tattooing, Greg says that "So much of what we do is really a personal thing between two people. It's not about the art so much, or the craft, or being the cool guy."<ref>{{cite av media|url=https://www.youtube.com/watch?v=TdOY0Exr4SE|title=SUNSET STRIP TATTOO|date=2 February 2009|work=YouTube}}</ref> "Tattooing to me is like welding. You have to lay the lines down. You're using tools. It [the tattoo] should look like it's always been there. Like it's part of your body."<ref name="vimeo.com">{{cite av media|url=http://vimeo.com/37031141|title=TATTOOED the Trailer|work=Vimeo}}</ref> "You know, the average person today thinks tattoo artists nowadays are rock stars. But we go through a lot. We really put a lot effort, time, and communication into what we do. And it's permanent. You can't tear it up and throw it away when you're done with it. All these tattoos that I've done for thirty years--I carry those around in my head with me."<ref name="youtube.com1">{{cite av media|url=https://www.youtube.com/watch?v=1dWCFScZWJc|title=Greg James of Tattoos Deluxe in Sherman Oaks, CA.|date=3 October 2012|work=YouTube}}</ref>

== Documentaries ==

* ''Skin Deep: Sunset Strip Tattoo''<ref name="youtube.com1"/>
* ''Tattoo! Beauty, Art, and Pain'' on the Discovery Channel.<ref name="tattoosymbol.com"/><ref name="youtube.com"/><ref>[http://www.filmoasis.com/about.html%20 Film Oasis]</ref>
* ''Tattooed'' by Brandon Green, an independent film currently in production by Evergreenimages.<ref name="vimeo.com"/>

== Featured artwork ==

[[File:Custom Bass of Nikki Sixx Hand Painted by Greg James.jpg|thumb|alt=Bass guitar of Nikki Sixx.|The Hard Rock Cafe|300px|right|Custom bass at the Hard Rock Cafe in Tampa, Florida, hand-painted by Greg James for Nikki Sixx of Mötley Crüe.]]

* Greg hand-painted a custom bass for Nikki Sixx of Mötley Crüe after tattooing the band while on tour with them. It's installed as Memorabilia in the Hard Rock Cafe of Tampa, Florida.<ref name="Chronological Crue" /><ref>{{cite web|url=http://pinterest.com/pin/226939268693176298/|title=Nikki Sixx of Mötley Crüe / Sixx: A.M. : "Famous tattoo artist Greg James hand painted this bass for me. It now hangs at Hard Rock in Tampa." #Gibs… - Pinterest|date=28 April 2012|work=Pinterest}}</ref>
* Guitar Pick Guard artwork for The Illustrated Note.
* ''The Tattoo Encyclopedia: A Guide to Choosing Your Tattoo'' (with Terisa Green, published by Simon & Schuster, 2003) - Greg illustrated hundreds of tattoo designs for this popular book, sold globally in four languages with 50,000 copies in print. It is perennially an Amazon Top 100 bestseller in Education & Reference > Encyclopedias > Art.<ref>{{cite web|url=http://www.amazon.com/gp/product/0743223292|title=The Tattoo Encyclopedia: A Guide to Choosing Your Tattoo: Terisa Green: 9780743223294: Amazon.com: Books|work=amazon.com}}</ref>
* ''Skin & Ink Magazine'' (2003 - 2005) - Featured column "What Tattoos Mean" with Dr. Green.<ref>{{cite web|url=http://www.skinink.com/old/Past_Issues/2004_Past_Issues/September2004/Contents_September_2004/body_contents_september_2004.html|title=Contents September 2004|work=skinink.com}}</ref>
* ''Ink: The Not-Just-Skin-Deep Guide to Getting a Tattoo'' (published by Penguin, 2005) - Contributing tattoo artist.<ref>[http://www.tattoosymbol.com/books/book-ink.html%20 Ink at TattooSymbol.com]</ref>
* ''Tattoo Savage Magazine'' - Black Veil Brides: Day of the Dead<ref>[http://breakawayradio.ca/post/26390688779/tattoo-savage-magazine Breakaway Radio, Tattoo Savage Magazine]</ref>
* ''Tattoo Magazine'' - The Art of Greg James.<ref>{{cite web|url=http://www.amazon.com/Magazine-largest-selling-magazine-premier/dp/B00527N6XE|title=TATTOO Magazine March 2000 No. 127 (World's largest selling tattoo magazine, Sunset Strip Ink, The art of Greg James, Crowe & Dwyer's premier tattoo party, Tattoos by Erick Lynch): Billy Tinney: Amazon.com: Books|work=amazon.com}}</ref>
* ''Tattoo Planet Magazine''<ref>{{cite web|url=http://www.amazon.com/TATTOO-Magazine-September-Worldwide-Giancario/dp/B004UU5TB6/%20|title=TATTOO PLANET Magazine September 1999 (The Worldwide Atomic Tattoo Magazine, Dr. Lakra, Mario Teli, Greg James, Giancario Capra): Robert Butcher: Amazon.com: Books|work=amazon.com}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
{{commons category}}
* [http://thirteenroses.squarespace.com/ 13 Roses Tattoo]
* Greg James, [https://www.facebook.com/greg.james.56 Facebook]
* [http://www.fkirkalley.com/ Kirk Alley's Eleven Eleven]
* [http://www.leaslounge.com/ Léa Vendetta]
* [http://www.paulygreenwood.com/#/photos/4547898112 Pauly Greenwood]
* [http://sunsetstriptattoo.com/ Sunset Strip Tattoo]
* [http://tattoosdeluxe.com/ Tattoos Deluxe]
* Tattoos Deluxe, [https://www.facebook.com/TattoosDeluxe Facebook]
* [http://www.tattoopower.com.au/ Tattoo Power]
* [http://tattoosymbol.com TattooSymbol.com]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:James, Greg}}
[[Category:Articles created via the Article Wizard]]
[[Category:American tattoo artists]]
[[Category:Living people]]
[[Category:Artists from California]]