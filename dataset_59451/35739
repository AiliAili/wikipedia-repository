{{EngvarB|date=April 2014}}
{{Good article}}
{{Infobox military conflict
|conflict=Action of 18 October 1806
|partof=the [[Napoleonic Wars]]
|image=[[File:Capture of Maria Riggersbergen.jpg|300px]]
|caption=''Capture of the Maria Riggersbergen, Octr. 18th 1806''<br/>[[Thomas Whitcombe]], 1817
|date=18 October 1806
|place=[[Batavia, Dutch East Indies|Batavia]], [[Java]], [[Dutch East Indies]]
|result=British victory
|combatant1= {{flagicon|United Kingdom}} United Kingdom
|combatant2=[[File:Flag of the Netherlands.svg|22px|link=]] [[Kingdom of Holland]]
|commander1=Captain Peter Rainier
|commander2=Captain Claas Jager
|strength1=[[Frigate]] [[HMS Caroline (1795)|HMS ''Caroline'']]
|strength2=[[Frigate]] [[Dutch frigate Maria Riggersbergen|''Maria Riggersbergen'']] and four smaller ships
|casualties1=Nine killed, 12 wounded. Four Dutch prisoners aboard were also killed.
|casualties2=50 casualties, ''Maria Riggersbergen'' captured
}}
{{Campaignbox Java campaign of 1806-1807}}
The '''Action of 18 October 1806''' was a minor naval engagement during the [[Napoleonic Wars]], fought between the British [[Royal Navy]] [[frigate]] [[HMS Caroline (1795)|HMS ''Caroline'']] and a Dutch squadron at the entrance to [[Batavia, Dutch East Indies|Batavia]] harbour on [[Java]] in the [[Dutch East Indies]]. During the battle the Dutch frigate [[Dutch frigate Maria Riggersbergen|''Maria Riggersbergen'']] was left unsupported by the remainder of the squadron and, isolated, was forced to surrender.<sup>[[#notea|[Note A]]]</sup> Captain Peter Rainier, the British commander, was subsequently free to remove his prize from within sight of the Dutch port when the remainder of the Dutch squadron refused to engage ''Caroline'' and their crews deliberately grounded the ships to avoid capture. He also returned many prisoners taken previously in a captured brig.

The action, and that of with the earlier [[Action of 26 July 1806]], demonstrated the weakness of the Dutch squadron in the East Indies and convinced Rear-Admiral [[Edward Pellew|Sir Edward Pellew]] to lead an operation against Batavia to eliminate the remainder of the Dutch squadron in November 1806. This second raid was only partially successful, and was followed a year later by a raid on the harbour of [[Gresik|Griessie]], in which the last Dutch warships in the East were eliminated.

==Background==
By 1806, the [[Linois' expedition to the Indian Ocean|French squadron]] under Rear-Admiral [[Charles-Alexandre Durand Linois|Charles Linois]] departed for the Atlantic Ocean and a British expeditionary force captured the [[Cape of Good Hope]].<ref name="RG81"/> Rear-Admiral [[Edward Pellew|Sir Edward Pellew]], commander of the British [[Royal Navy]] in the eastern half of the Indian Ocean at [[Madras]] in [[British India]] in the eastern half of the Indian Ocean was now able to concentrate on a major threat to British shipping in the region; the Dutch squadron based in the [[Dutch East Indies]], specifically on [[Java]] at the port of [[Batavia, Dutch East Indies|Batavia]].<ref name="RG81"/>

The Dutch squadron, which consisted of a number of old [[ships of the line]], three [[frigate]]s and a number of smaller warships, was primarily an anti-piracy force. However, their presence so close to the [[Straits of Malacca]], a major British trade route, was of concern to Pellew, particularly following the [[Battle of Pulo Aura]] in 1804, when Linois<nowiki>'</nowiki>s squadron intercepted a vital British convoy in the Strait, using Batavia both as a base to launch the operation and repair damage afterwards.<ref name="WLC336">Clowes, p. 336</ref> Determined to eliminate the Dutch squadron, Pellew despatched frigates to the region in the spring of 1806, under orders to disrupt trade and reconnoitre the Dutch harbours and bases. In July 1806, the frigate [[HMS Greyhound (1783)|HMS ''Greyhound'']] cruised the [[Molucca Islands]], and captured a Dutch frigate and convoy at the [[Action of 26 July 1806]] off [[Sulawesi|Celebes]], encouraging further expeditions.<ref name="WJ252">James, p. 252</ref> In October 1806, a second frigate, the 36-gun [[HMS Caroline (1795)|HMS ''Caroline'']] under Captain Peter Rainier (nephew of Admiral [[Peter Rainier, junior|Peter Rainier]] whom Pellew had replaced), cruised in the [[Java Sea]]. ''Caroline'' had a successful start to the operation, discovering that the Dutch ships of the line had left Batavia harbour and sailed eastwards.<ref name="LG1"/> He had also captured a number of Dutch ships so that by mid-October 57 of Rainier's crew, more than a fifth, were aboard prizes on the journey back to India, leaving ''Caroline'' with just 204 men and a large number of prisoners carried below decks.<ref name="JH79">Henderson, p. 79</ref>

==Battle==
On 18 October, Rainier was cruising in the Java Sea when he encountered and captured a small Dutch [[brig]] sailing from [[Bantam (city)|Bantam]]. From prisoners removed from this ship, Rainier learned that the Dutch frigate [[Dutch frigate Phoenix|''Phoenix'']] was undergoing repairs at the small island of Onrust in the [[Thousand Islands (Indonesia)|Thousand Islands]].<ref name="WLC392"/> Deciding that ''Phoenix'' was lying in an exposed position and could be easily attacked, Rainier sailed ''Caroline'' towards Onrust, but was spotted in the passage between Middlebey and Amsterdam Islands by two small Dutch warships. Rainier attacked the small vessels, seizing the 14-gun brig ''Zeerop'' without a shot fired. The other vessel ''Zee-Ploeg'' escaped into shallow coastal waters, where the deeper drafted frigate could not follow.  The delay allowed ''Phoenix'' to sail to Batavia ahead of ''Caroline''<nowiki>'</nowiki>s pursuit.<ref name="LG1"/>

As ''Caroline'' neared Batavia, ''Phoenix'' entered the well-defended harbour, making further pursuit impossible. However, Rainier then sighted a second frigate, lying at anchor in Batavia Roads, accompanied only by the 14-gun [[corvette]] ''William'', the elusive brig ''Zee-Ploeg'', and the 18-gun ship ''Patriot'' that belonged to the [[Dutch East India Company]].<ref name="LG1"/> Prisoners from ''Zeerop'' identified this ship as the 36-gun [[Dutch frigate Maria Riggersbergen|''Maria Riggersbergen'']] under Captain Claas Jager. Although this force was significantly stronger than ''Caroline'', and could call on the support of approximately 30&nbsp; gunboats anchored closer inshore, Rainier immediately gave orders to advance on the Dutch frigate. In his preparations for battle, Rainier ordered that springs be placed on his anchor cables, giving his ship the ability to easily turn at anchor to face new threats once engaged with the ''Maria Riggersbergen''.<ref name="WJ266">James, p. 266</ref>

As ''Caroline'' approached, Captain Jager ordered his men to open fire on the British frigate at extreme range, also calling on support from the other Dutch vessels anchored nearby. In response, Rainier gave orders for his men to hold fire, enduring the Dutch guns until his frigate was just {{convert|40|yd|m}} away before unleashing a full [[broadside]].<ref name="JH79"/> Jager responded, but the British fire was too strong and within half an hour the Dutch flag was [[striking the flag|struck]]. Taking possession of the Dutch frigate, Lieutenant Lemage discovered that 50&nbsp;of the 270&nbsp;men aboard had been killed or wounded and that the ship had suffered moderate damage to its masts and rigging.<ref name="WJ267">James, p. 267</ref> British casualties in the engagement were three killed outright and eighteen more wounded, six of whom subsequently died. Also killed were four Dutch prisoners who had been sheltering in the hold.<ref name="LG2"/>

While Lemage was boarding ''Maria Riggersbergen'', Rainier had turned his attention to the other ships in the bay. However the sea was shallow and crossed by shoals that prevented further advance without proper charts, which ''Caroline'' lacked. Although they had fired a number of shots at long range, the interference by the smaller Dutch vessels during the battle had been negligible.<ref name="RG81"/> Following the surrender of ''Maria Riggersbergen'', most of the shipping in the bay including all seven of the merchant vessels, the three small warships and ''Phoenix'', had deliberately beached themselves to avoid capture. Abandoning the idea of further operations off Batavia as too risky, Rainier ordered his ships to sail, placing most of the Dutch prisoners, including the wounded and sick, into the first brig captured that morning and ordering the ship to return to Batavia as a [[cartel (ship)|cartel]], with the officers placed under parole restrictions. With most of his prisoners removed, Rainier then ordered ''Maria Riggersbergen'' and ''Zeerop'' to return to [[Madras]].<ref name="LG2"/>

==Aftermath==
Ranier's action against what appeared to be superior power and numbers exposed the poor quality of the Dutch squadron at Batavia.  Pellew determined to make a decisive attack on the capital of the Dutch East Indies during 1806. In November he led a [[Raid on Batavia (1806)|powerful squadron to the harbour]], once again forcing the Dutch to their squadron ashore, where it was burnt by boarding parties led by Admiral Pellew's son, Captain [[Fleetwood Pellew]].<ref name="RG82">Gardiner, p. 82</ref> The following year, Admiral Pellew returned in search of the missing ships of the line, [[Raid on Griessie|discovering them at]] [[Gresik|Griessie]] and causing the Dutch to destroy them too. With the Dutch squadron eliminated, the threat to British trade routes was removed and attention returned to the French bases in the Indian Ocean, the British waiting until 1811 to force the surrender of the remaining Dutch colonies in the East Indies.<ref name="RG107">Gardiner, p. 107</ref>

''Maria Riggersbergen'' was recommissioned into the Royal Navy as HMS ''Java'', under Captain George Pigot. In his report after the battle, Rainier described the Dutch frigate as "launched in 1800 and is a fast sailing ship".<ref name="LG1">{{London Gazette|issue=16139|startpage=568|date=23 April 1808|accessdate=22 September 2009}}</ref> The {{convert|2500|nmi|km}} journey to Madras had revealed that she was in fact much older and very unstable at sea. ''Java'' and all hands disappeared six months later in a February 1807 hurricane in the west Indian Ocean while in convoy with the flagship of [[Thomas Troubridge|Sir Thomas Troubridge]] [[HMS Blenheim (1761)|HMS ''Blenheim'']] during a hurricane in the western Indian Ocean.<ref name="TG233">Grocott, p. 233</ref> Rainier remained in the Pacific for some time, capturing the valuable Spanish ship ''San Raphael'' in January 1807, but ultimately his career stalled on his return to Europe.<ref name="JH82">Henderson, p. 82</ref>

==Notes==
{{refbegin}}
<ol class="references" style="list-style-type:upper-alpha;">
<li id="notea">'''[[#inlinea|^]]''' There are a variety of ways in which ''Maria Riggersbergen'' has been spelt, including ''Maria Reygersbergen'',<ref name="LG2">{{London Gazette|issue=16139|startpage=569|date=23 April 1808|accessdate=22 September 2009}}</ref> ''Maria Reigersbergen'',<ref name="RW231">Woodman, p. 231</ref> ''Maria Reijersbergen'',<ref name="RG81">Gardiner, p. 81</ref> and ''Maria Reijgersbergen''.<ref name="WLC392">Clowes, p. 392</ref> The most commonly used name in English however is ''Maria Riggersbergen''</li>
</ol>
{{refend}}

==References==
{{reflist|colwidth=30em}}

==Bibliography==
*{{Cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997 |origyear=1900
 | chapter =
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume V
 | publisher = Chatham Publishing
 | location =
 | isbn = 1-86176-014-0
}}
*{{Cite book
 | editor =Gardiner, Robert 
 | authorlink =
 | year = 2001 |origyear=1998
 | chapter =
 | title = The Victory of Seapower
 | publisher = Caxton Editions
 | location =
 | isbn = 1-84067-359-1
}}
*{{Cite book
 | last = Grocott
 | first = Terence
 | authorlink =
 | year = 2002 |origyear=1997
 | chapter =
 | title = Shipwrecks of the Revolutionary & Napoleonic Era
 | publisher = Caxton Editions
 | location =
 | isbn = 1-84067-164-5
}}
*{{Cite book
 | last = Henderson CBE
 | first = James
 | authorlink =
 | year = 1994 |origyear=1970
 | chapter =
 | title = The Frigates
 | publisher = Leo Cooper
 | location =
 | isbn = 0-85052-432-6
}}
* {{Cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002 |origyear=1827
 | chapter =
 | title = The Naval History of Great Britain, Volume 4, 1805&ndash;1807
 | publisher = Conway Maritime Press
 | location =
 | isbn = 0-85177-908-5
}}
*{{Cite book
 | last = Woodman
 | first = Richard
 | authorlink = Richard Woodman
 | year = 2001
 | chapter =
 | title = The Sea Warriors
 | publisher = Constable Publishers
 | location =
 | isbn = 1-84119-183-3
}}

{{coord missing|Indonesia}}

{{Use dmy dates|date=April 2014}}

{{DEFAULTSORT:Action Of 18 October 1806}}
[[Category:Naval battles involving the Netherlands]]
[[Category:Naval battles involving the United Kingdom]]
[[Category:Naval battles of the Napoleonic Wars]]
[[Category:Conflicts in 1806]]
[[Category:October 1806 events]]