<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Griffon
 |image= File:Rolls Royce Griffon.jpg
 |caption=Preserved Rolls-Royce Griffon with cutaway sections.
}}{{Infobox Aircraft Engine
 |type=[[Piston]] [[V12 engine|V-12]] [[aero engine]]
 |manufacturer=[[Rolls-Royce Limited|Rolls-Royce]]
 |first run=November {{avyear|1939}}
 |major applications=[[Avro Shackleton]]<br />[[Fairey Firefly]]<br />[[Supermarine Spitfire]]
 |number built = 8,108
 |program cost =
 |unit cost =
 |developed from =
 |developed into =
 |variants with their own articles =
}}
|}

The '''Rolls-Royce Griffon''' is a British 37-[[litre]] (2,240&nbsp;[[cubic inch|cu in]]) [[Engine displacement|capacity]], 60-degree [[V12 engine|V-12]],  liquid-cooled [[Aircraft engine|aero engine]] designed and built by [[Rolls-Royce Limited]]. In keeping with company convention, the Griffon was named after a [[bird of prey]], in this case the [[griffon vulture]].

Design work on the Griffon started in 1938 at the request of the [[Fleet Air Arm]], for use in new aircraft designs such as the [[Fairey Firefly]]. In 1939 it was also decided that the engine could be adapted for use in the [[Supermarine Spitfire (Griffon powered variants)|Spitfire]]. However, development was temporarily put on hold to concentrate efforts on the smaller [[Rolls-Royce Merlin|Merlin]] and the 24-cylinder [[Rolls-Royce Vulture|Vulture]], and the engine did not go into production until the early 1940s.

The Griffon was the last in the line of V-12 aero engines to be produced by Rolls-Royce with production ceasing in 1955.<ref>Lumsden 2003, p.218.</ref> Griffon engines remain in [[Royal Air Force]] service today with the [[Battle of Britain Memorial Flight]] and power the last remaining airworthy [[Avro Shackleton]].

==Design and development==

===Origins===
[[File:Spitfire IV XII DP845.jpg|thumb|right|The first Griffon-powered Spitfire IV, ''DP845'']]
According to [[Arthur Rubbra]]'s memoirs, a de-rated version of the "[[Rolls-Royce R|R]]" engine, known by the name ''Griffon'' at that time, was tested in 1933. This engine, ''R11'',<ref>Eves 2001, p. 228.</ref> which was never flown, was used for "Moderately Supercharged Buzzard development" (which was not proceeded with until much later), and bore no direct relationship to the volume-produced Griffon of the 1940s.

In 1938 the Fleet Air Arm approached Rolls-Royce and asked whether a larger version of the Merlin could be designed. The requirements were that the new engine have good power at low altitude and that it be reliable and easy to service.<ref name="Flt309.">Flight 1945, p. 309.</ref> Work began on the design of the engine soon afterwards.{{#tag:ref|Although the ''Griffon'' from 1933 shared the R engine's [[Bore (engine)|bore]] and [[Stroke (engine)|stroke]] with the Griffon I of 1939, the Griffon I was a completely new design.<ref name="Flt309."/>|group=nb}} The design process was relatively smooth compared with that of the Merlin, and the first of three prototype Griffon Is first ran in the Experimental Department on 30 November 1939.<ref>Lumsden 2003, p. 216.</ref><ref name="Morgan and Shacklady 2000, p. 133.">Morgan and Shacklady 2000, p. 133.</ref>

Although the Griffon was designed for naval aircraft, on 8 November 1939 N E Rowe of the [[Air Ministry]] suggested fitting the Griffon in a [[Supermarine Spitfire|Spitfire]]. Three weeks later permission was given to [[Supermarine]] to explore the possibilities of adapting the Griffon to the Spitfire; in response Supermarine issued 'Specification 466' on 4 December. This decision led to a change in the disposition of the engine accessories to reduce the frontal area of the engine as much as possible.<ref name="Morgan and Shacklady 2000, p. 133."/> As a result the frontal area of the bare Griffon engine was {{convert|7.9|sqft|m2}} compared with {{convert|7.5|sqft|m2}} of the Merlin, despite the Griffon's much larger capacity.<ref>Morgan and Shacklady 2000, p. 134.</ref> This redesigned engine first ran on 26 June 1940 and went into production as the Griffon II.

In early-1940, on the orders of [[Max Aitken, 1st Baron Beaverbrook|Lord Beaverbrook]], [[Minister of Aircraft Production]], work on the new engine had been halted temporarily to concentrate on the smaller 27&nbsp;L ({{nowrap|1,650 cu in}}) Merlin which had already surpassed the output achieved with the early Griffon.

===Design===
[[file:Fairey Firefly Engine CWHM 1.jpg|thumb|right|upright|A Griffon installed on ''WH632'', an airworthy [[Fairey Firefly]] at the [[Canadian Warplane Heritage Museum]] (2014) ]]
Compared with earlier Rolls-Royce designs the Griffon engine featured several improvements, which meant that it was physically only slightly larger than the Merlin, in spite of its 36% larger capacity of 37-[[litre]]s (2,240&nbsp;[[cubic inch|cu in]]).<ref name="Flt309."/>

One significant difference was the incorporation of the [[camshaft]] and [[ignition magneto|magneto]] drives into the propeller [[Gear ratio|reduction gears]] at the front of the engine, rather than using a separate system of gears driven from the back end of the [[crankshaft]]; this allowed the overall length of the engine to be reduced as well as making the drive train more reliable and efficient.<ref name="Flt312.">Flight 1945, p.312.</ref>{{#tag:ref|Incorporating four sets of gears (two camshaft drives, plus propeller reduction gears and magneto drive) into one unit meant that the extra group of gears for the magnetos and camshafts, which were normally mounted at the rear of the crankshaft, could be omitted.|group=nb}} The drive for the supercharger was also taken off the front of the engine, which required a long shaft to run to its location at the back of the engine.<ref name="The Aeroplane 1945, pp. 1, 3, 6">The Aeroplane 1945, pp. 1, 3, 6.</ref>

The Griffon was the first Rolls-Royce production aero engine to use a hollow crankshaft as the means of lubricating the main and [[Crankpin|big end bearing]]s, providing a more even distribution of oil to each bearing.<ref name="Flt312."/> In another change from convention, one high efficiency [[British Thomson-Houston|B.T.H]]-manufactured dual magneto was mounted on top of the propeller reduction casing;<ref>Flight 1945, pp. 312, 314.</ref> earlier Rolls-Royce designs using twin magnetos mounted at the rear of the engine.<ref>Rubbra 1990, p. 118.</ref>

The Griffon 61 series introduced a two-stage supercharger and other design changes: the pressure oil pumps were now housed internally within the sump and an effort was made to remove as many external pipes as possible.<ref>The Aeroplane 1945, pp. 1, 8.</ref> In addition, the drive for the supercharger was taken from the crankshaft at the back of the engine, via a short torsion shaft, rather than from the front of the engine, using a long drive shaft as used by earlier Griffon variants.<ref name="The Aeroplane 1945, pp. 1, 3, 6"/>

Production of the aero version of the Griffon ended in December 1955, while a marine version, the '''Sea Griffon''', continued to be produced for the RAF's High Speed Launches.<ref>Flight 1956, p. 578.</ref>

===Basic component overview (Griffon 65)===
''From Jane's and Flight.''<ref>Bridgman 1989, pp. 279–280.</ref><ref>[Staff author] 20 September 1945. "[http://www.flightglobal.com/pdfarchive/view/1945/1945%20-%201852.html The Rolls-Royce Griffon]" ''Flight'', pp. 309-316. www.flightglobal.com. Retrieved: 29 October 2009.</ref>
[[File:RR Griffon.jpg|thumb|right|Griffon, cut away to show camshaft drives etc., at the Battle of Britain Museum at RAF Coningsby]]
;Cylinders
:Twelve cylinders consisting of high-carbon steel, floating [[wet liner]]s<ref name="Flt313.">Flight 1945, p.313.</ref> set in two, two-piece cylinder blocks of cast [[aluminium alloy]] having separate heads and skirts. Cylinder liners [[chromium]] plated in the bores for {{frac|2|1|2}} inches from the head.<ref name="Flt313."/> Cylinder blocks mounted with an included 60-degree angle onto inclined upper faces of a two-piece crankcase. Cylinder heads fitted with cast-iron inlet valve guides, [[phosphor bronze]] exhaust valve guides, and renewable "Silchrome" steel-alloy valve seats. Two diametrically opposed [[spark plug]]s protrude into each [[combustion chamber]].
;Pistons
:Machined from "[[Hiduminium|R.R.59]]" alloy [[forging]]s. Fully floating [[gudgeon pin]]s of hardened nickel-chrome steel. Two [[Internal combustion engine#Compression|compression]] and one drilled oil-control [[Piston ring|ring]] above the gudgeon pin, and another drilled oil-control ring below.
;Connecting rods
:H-section machined nickel-steel forgings, each pair consisting of a plain and a forked [[Connecting rod|rod]]. The forked rod carries a nickel-steel bearing block which accommodates steel-backed lead-bronze-alloy bearing shells. The "small-end" of each rod houses a floating phosphor bronze [[Bushing (bearing)|bush]].
;Crankshaft
:One-piece, machined from a [[Nitriding|nitrogen-hardened]] nickel-chrome [[molybdenum]] steel forging. [[Engine balance|Statically and dynamically balanced]]. Seven main bearings and six throws. Internal oilway, with feed from both ends, used to distribute lubricants to main and big end bearings.<ref name="Flt312."/> "Floating" front end bearing consisting of an internally toothed annulus bolted to crankshaft, meshing with and incorporating a semi-floating ring, internally [[Rotating spline|splined]] to a short [[coupling]] shaft. Coupling shaft splined at front end to driving wheel of propeller reduction gear.<ref name="Flt312."/> Clockwise rotation when viewed from rear.
;Crankcase
:Two aluminium-alloy castings joined together on the horizontal centreline. The upper portion bears the wheelcase, cylinder blocks and part of the housing for the [[Propeller speed reduction unit|airscrew reduction gear]]; and carries the crankshaft main bearings (split mild-steel shells lined with lead–bronze alloy). The lower half forms an oil sump and carries the main pressure oil pump, supercharger change-speed operating pump and two scavenge pumps. It also houses the main coolant pump which is driven through the same gear-train as the oil pumps.
;Wheelcase
:Aluminium-alloy casting fitted to rear of crankcase. Carries the supercharger; and houses drives to the supercharger, auxiliary gearbox coupling, engine speed indicator, airscrew constant-speed unit, intercooler pump and fuel pump, as well as the oil and coolant pumps in the lower half crankcase.
;Valve gear
:Two inlet and two exhaust [[poppet valve]]s of "[[Kayser, Ellison and Co Ltd|K.E.965]]" austenitic nickel-chrome steel per cylinder. Exhaust valves have [[sodium]]-cooled stems. "[[Brightray]]" (nickel-chromium) protective coating to the whole of the combustion face and seat of the exhaust valves, and to the seat only of the inlet valves. Each valve is held closed by a pair of concentric [[coil spring]]s. A single, seven-bearing camshaft, located centrally on the top of each cylinder head operates 24 individual steel [[Rocker arm|rockers]]; 12 pivoting from a rocker shaft on the inner, intake side of the block to actuate the exhaust valves, the others pivoting from a shaft on the exhaust side of the block to actuate the inlet valves.

===Engine capacity, mass flow, and supercharging===
Although it is common practice to compare different piston engines and their performance potential by referring to the [[Engine displacement|engine displacement or swept volume]] this does not give an accurate reading of an engine's capabilities. According to [[Cyril Lovesey|A C Lovesey]], who was in charge of the Merlin's development,{{#tag:ref|After helping design the two-stage, two-speed supercharger for the Merlin Lovesey went on to help develop the Rolls-Royce jet engines; although the quote refers to the Merlin it applies equally as well to the Griffon.|group=nb}} "The impression still prevails that the static capacity known as the swept volume is the basis of comparison of the possible power output for different types of engine, but this is not the case because the output of the engine depends solely on the mass of air it can be made to consume efficiently, and in this respect the supercharger plays the most important role."<ref>Lovesey 1946, p. 218.</ref>

[[File:Spiteful XVI RB518.jpg|thumb|The 494&nbsp;mph Spiteful XVI, [[United Kingdom military aircraft serials|serial number]] ''RB518'']]

Unlike the Merlin, the Griffon was designed from the outset to use a single-stage supercharger driven by a two-speed, hydraulically operated gearbox; the production versions, the Griffon II, III, IV, and VI series, were designed to give their maximum power at low altitudes and were mainly used by the Fleet Air Arm. The Griffon 60, 70, and 80 series featured two-stage supercharging and achieved their maximum power at low to medium altitudes. The Griffon 101, 121, and 130 series engines, collectively designated ''Griffon 3 SML'',<ref name="Fli46,34.">Flight 1946, p. 34.</ref> used a two-stage, three-speed supercharger, adding a set of "Low Supercharger (L.S)" gears to the already existing Medium and Full Supercharger (M.S and F.S) gears.<ref name="Fli46,34."/> Another modification was to increase the diameters of both impellers, thus increasing the rated altitudes at which maximum power could be generated in each gear.<ref name="Fli46,3435.">Flight 1946, pp. 34-35.</ref> While the 101 continued to drive a five-blade propeller, the 121 and 130 series were designed to drive contra-rotating propellers.<ref name="Fli46,34."/> In 1946 a Griffon 101 was fitted to the [[Supermarine Spiteful]] XVI, ''RB518'' (a re-engined production Mk.XIV); this aircraft achieved a maximum speed of 494&nbsp;mph (795&nbsp;km/h) with full military equipment.<ref>Morgan and Shacklady 2000, pp. 501, 503.</ref>

===Pilot transition===
Pilots who converted from the Merlin to the Griffon-engined Spitfires soon discovered that, because the Griffon engine's propeller rotated in the opposite direction to that of the Merlin, the fighter swung to the right on takeoff rather than to the left.<ref name="Lum217."/>{{#tag:ref|The propeller of the Griffon rotated to the left (counter-clockwise) when viewed from the cockpit.|group=nb}}  This tendency was even more marked with the more powerful 60 and 80 series Griffon engines, with their five-bladed propellers. As a result, pilots had to learn to apply left (port) trim on takeoff, instead of the right (starboard) trim they were used to applying.<ref>Price 1995, pp. 29, 32-33.</ref> On takeoff, the throttle had to be opened slowly, as the pronounced swing to the right could lead to "crabbing" and severe tyre wear.<ref>Air Ministry 1946, paragraph 49 (i), pp. 26-27</ref>

Some test Spitfire XIVs, 21s, and 24s were fitted with [[contra-rotating propellers]], which eliminated the torque effect. Early problems with the complex gearbox that was required for contra-rotating propellers prevented them from ever becoming operational in Spitfires,<ref>Gunston 1989, p.143.</ref> but they were used on later aircraft, including the [[Supermarine Seafire|Seafire]] FR. Mk 46 and F and FR.47, which were fitted with Griffon 87s driving contra-rotating propellers as standard equipment.<ref>Morgan and Shacklady 2000 pp. 573, 579, 580.</ref> The Griffon 57 and 57A series, installed in [[Power-egg#United Kingdom|Universal Power Plant]] (UPP) installations and driving contra-rotating propellers, was used in the [[Avro Shackleton]] maritime patrol aircraft.<ref name="Carv09">Carvell 2010, p.57.</ref>

==Variants==
The Griffon was produced in approximately 50 different variants, the '''Griffon 130''' being the last in the series. Details of representative variants are listed below:
* '''Griffon IIB'''
:1,730&nbsp;hp (1,290&nbsp;kW) at 750&nbsp;ft (230&nbsp;m) and 1,490&nbsp;hp (1,110&nbsp;kW) at 14,000&nbsp;ft (4,270&nbsp;m); Single-stage two-speed supercharger; impeller diameter 10&nbsp;in (25.4&nbsp;cm); gear ratios 7.85:1, 10.68:1.<ref name="Lum217.">Lumsden 2003, p. 217.</ref> Used on [[Fairey Firefly|Firefly Mk.I]] and [[Supermarine Spitfire (Griffon powered variants)|Spitfire XII]].
* '''Griffon VI'''
:Increased maximum boost pressure, 1,850&nbsp;hp (1,380&nbsp;kW) at 2,000&nbsp;ft (610&nbsp;m); impeller diameter 9.75&nbsp;in (24.7&nbsp;cm).<ref name="Lum217."/> Used on [[Supermarine Spitfire|Seafire]] Mk.XV and Mk. XVII, Spitfire XII.
[[File:Griffon58.JPG|thumb|right|A Rolls-Royce Griffon 58 displayed at the [[Shuttleworth Collection]] (2008)]]
* '''Griffon 57 and 57A'''
:1,960&nbsp;hp (1,460&nbsp;kW); 2,345&nbsp;hp (1,749&nbsp;kW) with water-methanol injection on take-off: used on [[Avro Shackleton]].<ref name="Carv09"/>
* '''Griffon 61'''
:Introduced a two-speed two-stage [[supercharger]] with aftercooler similar to that on Merlin 61; 2,035&nbsp;hp (1,520&nbsp;kW) at 7,000&nbsp;ft (2,100&nbsp;m) and 1,820&nbsp;hp (1,360&nbsp;kW) at 21,000&nbsp;ft (6,400&nbsp;m); used on [[Supermarine Spitfire (Griffon powered variants)|Spitfire F.Mk.XIV, Mk.21]].
* '''Griffon 65'''
:Similar to Griffon 61 with different propeller reduction gear; Impeller diameters 1st stage: 13.4&nbsp;in (34&nbsp;cm), 2nd stage: 11.3&nbsp;in (29&nbsp;cm);<ref>Flight 1945, p. 315.</ref> used on Spitfire F.Mk.XIV.
* '''Griffon 72'''
:Increased maximum boost pressure to take advantage of 150-[[octane rating|grade]] fuel; 2,245&nbsp;hp (1,675&nbsp;kW) at 9,250&nbsp;ft (2,820&nbsp;m).
* '''Griffon 74'''
:Fuel-injected version of Griffon 72; used on Firefly Mk.IV.
* '''Griffon 83'''
:Modified to drive [[contra-rotating propellers]]; 2,340&nbsp;hp (1,745&nbsp;kW) at 750&nbsp;ft (230&nbsp;m) and 2,100&nbsp;hp (1,565&nbsp;kW) at 12,250&nbsp;ft (3,740&nbsp;m).
* '''Griffon 85'''
:2,375&nbsp;hp (1,770&nbsp;kW); used on [[Supermarine Spiteful|Spiteful Mk.XIV]].
* '''Griffon 89'''
:2,350&nbsp;hp (1,755&nbsp;kW); used on Spiteful Mk.XV.
* '''Griffon 101'''
:2,420&nbsp;hp (1,805&nbsp;kW); Two-stage, three-speed supercharger using Low Supercharger (L.S), Moderate Supercharger (M.S), or Full Supercharger (F.S); reduction gear ratio 4.45; Rolls-Royce fuel injection system.<ref name="Fli46,34."/> Used on Spiteful Mk.XVI.
*'''Griffon 130'''
:2,420&nbsp;hp (1,805&nbsp;kW) at 5,000&nbsp;ft (1,524&nbsp;m) in L.S gear, 2,250&nbsp;hp (1,678&nbsp;kW) at 14,500&nbsp;ft (4,419&nbsp;m) M.S and 2,050&nbsp;hp (1,529&nbsp;kW) at 21,000&nbsp;ft (6,400&nbsp;m) F.S; reduction gear ratio 4.44; modified to drive contra-rotating propellers; Rolls-Royce fuel injection system.<ref name="Fli46,3435."/>
* '''Compound Griffon RGC.30.SM.'''; [[turbo-compound]] engine - cancelled 1949
* '''Turbo Griffon RGT.30.SM.'''; similar to [[Napier Nomad]] - as above <ref>http://www.secretprojects.co.uk/forum/index.php/topic,14613.15.html?PHPSESSID=quqcq3bo53niebchb4tgfgr6r6</ref>

==Applications==
''Note:''{{#tag:ref|Generic types given only, the Griffon was not the main powerplant for some of these aircraft.|group=nb}}{{#tag:ref|Beaufighter II, Griffon IIB <ref>Lumsden 2003, p.217.</ref>|group=nb}}
{{Col-begin}}
{{Col-break}}
*[[Avro Shackleton]]
*[[Blackburn B-54]]
*[[Bristol Beaufighter]]
*[[CAC CA-15]]
*[[Fairey Barracuda]]
*[[Fairey Firefly]]
*[[Folland Fo.108]]
*[[Hawker Sea Fury#Development|Hawker Fury]]<ref>One prototype (LA610) with a Griffon 85 engine and a Rotol six-blade contra-rotating propeller</ref> 
{{Col-break}}
[[File:Fairey Barracuda Mk III.jpg|thumb|right|Griffon-engined [[Fairey Barracuda]]]]
*[[Hawker Henley]]<ref>One aircraft as an engine test bed for the Griffon II</ref>
*[[Hawker Tempest|Hawker Tempest Mk III/IV]]
*[[Martin-Baker MB 5]]
*[[Supermarine Seafang]]
*[[Supermarine Seafire]]
*[[Supermarine Seagull (1948)|Supermarine Seagull]]
*[[Supermarine Spiteful]]
*[[Supermarine Spitfire]]
{{col-end}}
[[File:25 P 51XR Mustang N6WJ Precious Metal Reno Air Race 2014 photo D Ramey Logan.jpg|thumb|Griffon engine with contra-rotating propellers on the P 51XR Mustang N6WJ "Precious Metal" 2014 Reno Air Races]]
Several [[North American P-51 Mustang|North American Mustangs]] raced in the Unlimited Class races at the [[Reno Air Races]] have been fitted with Griffons.  These include the [[RB51 Red Baron]] (NL7715C),<ref>[http://www.airliners.net/photo/Michelob-Light/North-American-P-51D/1101386/L Picture of the North American P-51D Mustang aircraft] Retrieved 24 September 2011.</ref> "[[Precious Metal (aircraft)|Precious Metal]]" (N6WJ) <ref>[http://warbirdaeropress.com/articles/PMmods/PMmods.htm Its All in the Details] Retrieved 24 September 2011.</ref><ref>[http://www.airliners.net/photo/Untitled-(Lake-Air)/North-American-P-51D/1123175/L Picture of the North American P-51D Mustang aircraft] Retrieved 24 September 2011.</ref> and a Mustang/Learjet hybrid "Miss Ashley II" (N57LR).<ref>[http://www.airliners.net/photo/North-American-Rogers-P-51R/0763382/L Picture of the North American/Rogers P-51R aircraft] Retrieved 24 September 2011.</ref> In all cases, Griffons with contra-rotating propellers, taken from [[Avro Shackleton]] patrol bombers were used in these aircraft.  The RB51 Red Baron is noteworthy for holding the FAI piston-engine 3-kilometre world speed record from 1979 to 1989 (499.018&nbsp;mph).

In 1965, SFR Yugoslavia used Griffon engines as the main power unit for their first domestically produced [[Self-propelled artillery|self-propelled artillery system]], the S65, but the system was withdrawn from service in the early 1980s, because of poor fuel economy.{{citation needed|date=November 2013}}

The 1980 ''[[Miss Budweiser]]'' [[Hydroplane (boat)|Unlimited Hydroplane]] dominated the race circuit with a Rolls-Royce Griffon engine. It was the last of the competitive piston-engined boats, before [[turboshaft]] powerplants took over.

In modern day [[tractor pulling]], Griffon engines are also in use, a single or double, rated each at 3,500&nbsp;hp (2,600&nbsp;kW).<ref>{{cite web|url=http://www.greenspirit.info/tractoren.php |title=Green Spirit tractor pulling team |publisher=Greenspirit.info |date= |accessdate=2013-01-07}}</ref>

==Survivors==
The Griffon engine continues to be used in restored Fireflies and later mark Spitfires worldwide. The [[Royal Air Force]] [[Battle of Britain Memorial Flight]] is a notable current operator of the Griffon.

The sole remaining technically airworthy Avro Shackleton is based at the [[South African Air Force Museum, Air Force Base Ysterplaat, Cape Town]].<ref>[http://www.saafmuseum.co.za/shack.htm SAAF 1722 - Avro Shackleton] Retrieved: 29 July 2009</ref> However, at least one further airframe is in running condition at the [[Gatwick Aviation Museum]] and another is in running, taxiing condition at [[Coventry Airport]], intended to be flown in a short time.

==Engines on display==
Preserved Griffon engines are on public display at the:
*[[Atlantic Canada Aviation Museum]]
*[[Bournemouth Aviation Museum]]
*[[Kissimmee Air Museum]]
*[[Midland Air Museum]]
*[[Royal Air Force Museum London]]
*[[Shuttleworth Collection]]
*[[South African Air Force Museum]], [[AFB Ysterplaat]]
*[[Tangmere Military Aviation Museum]]
*[[Birmingham Museum Collection Centre]]

==Specifications (Griffon 65)==
[[File:RRGriffon.JPG|thumb|right|Side view showing supercharger and carburettor detail]]
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully formatted line with <li> -->
|ref=''Lumsden''<ref>Lumsden 2003, pp.216-219.</ref> ''The Aeroplane''<ref>The Aeroplane 1945, pp. 1-7.</ref>
|type=12-cylinder supercharged liquid-cooled 60° [[V12 engine|Vee]] aircraft piston engine
|bore=6&nbsp;in (152.4&nbsp;mm)
|stroke=6.6&nbsp;in (167.6&nbsp;mm)
|displacement=2,240&nbsp;in<sup>3</sup> (36.7&nbsp;L)
|length=81&nbsp;in (2,057&nbsp;mm)
|diameter=
|width=30.3&nbsp;in (770&nbsp;mm)
|height=46&nbsp;in (1,168&nbsp;mm)
|weight=1,980&nbsp;lb (900&nbsp;kg)
|valvetrain=Two intake and two exhaust valves per cylinder with [[sodium]]-cooled exhaust valve stems, actuated via an [[overhead camshaft]].
|supercharger=Two-speed, two-stage [[centrifugal type supercharger]], boost pressure automatically linked to the throttle, water-air [[intercooler]] installed between the second stage and the engine.
|turbocharger=
|fuelsystem=Triple-choke [[Bendix Corporation|Bendix-Stromberg]] updraught, [[Pressure carburetor|pressure-injection]] [[carburetor|carburettor]] with automatic mixture control
|fueltype= 100 Octane (150 Octane January to May 1945)
|oilsystem=[[Dry sump]] with one pressure pump and two scavenge pumps
|coolingsystem=70% water and 30% [[ethylene glycol]] coolant mixture, pressurised. Liquid-cooled Intercooler radiator with its own separate system, again using 70/30% water/glycol mix.
|power=<br />
*2,035&nbsp;hp (1,520&nbsp;&nbsp;kW) at 7,000&nbsp;ft (2,135&nbsp;m MS gear),{{#tag:ref|MS and FS refer to the supercharger blower speeds: Moderate/Fully Supercharged.|group=nb}} +18 psi boost pressure at 2,750 rpm
*2,220&nbsp;hp (1,655&nbsp;&nbsp;kW)  at 11,000&nbsp;ft (2,135&nbsp;m MS gear), +21 psi at 2,750 r.p.m using 150 Octane fuel
*1,820&nbsp;hp (1,360&nbsp;kW) at 21,000&nbsp;ft (6,400&nbsp;m) at 2,750 rpm
|specpower=0.91&nbsp;hp/in<sup>3</sup> (41.4&nbsp;kW/L)
|compression=6:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=1.03&nbsp;hp/lb (1.69&nbsp;kW/kg)
|designer=
|reduction_gear=0.51:1, left-hand tractor
|general_other=
|components_other=
|performance_other=
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=
*[[Rolls-Royce R]]
*[[Rolls-Royce aircraft piston engines]]
<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Allison V-1710]]
*[[Daimler-Benz DB 603]]
*[[Daimler-Benz DB 605]]
*[[Junkers Jumo 213]]
*[[Klimov VK-107]]
*[[Mikulin AM-38]]

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==Media==
{|
|-
| [[File:GriffonMk58.ogg|thumb|Griffon Mk 58]]
|}

==References==

===Footnotes===
{{reflist|group=nb}}

===Citations===
{{reflist|2}}

===Bibliography===
{{refbegin}}
* Air Ministry. ''Pilot's Notes For Spitfire XIV & XIX; Griffon 65 or 66 Engine''. London: Air Ministry, 1946.
* Bridgman, L, (ed.) (1989) ''Jane's fighting aircraft of World War II.'' Crescent. ISBN 0-517-67964-7
*Carvell, Roger. "Aeroplane Examines the Avro Shackleton." ''[http://www.aeroplanemonthly.co.uk/ Aeroplane]'' No. 2, Vol. 38, Issue 442, February 2010.
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
* Lovesey, Cyril. "Development of the Rolls-Royce Merlin from 1939 to 1945". ''Aircraft Engineering'' magazine. London: July 1946
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* Price, Alfred. ''The Spitfire Story: Second edition''. London: Arms and Armour Press Ltd., 1986. ISBN 0-85368-861-3.
* Rubbra, A.A. ''Rolls-Royce Piston Aero Engines - a designer remembers: Historical Series no 16'' :Rolls Royce Heritage Trust, 1990. ISBN 1-872922-00-7
*"A Classic Design; The Rolls-Royce Two-Stage Griffon (article and images)."  ''The Aeroplane'', 21 September 1945.
*"[http://www.flightglobal.com/PDFArchive/View/1945/1945%20-%201852.html  Rolls-Royce Griffon (65) (article and images).]" ''[[Flight International|Flight and the Aircraft Engineer]]'' No. 1917, Vol. XLVIII, 20 September 1945.
*"[http://www.flightglobal.com/pdfarchive/view/1946/1946%20-%200062.html  Rolls-Royce Griffon 130 (article and images).]" ''[[Flight International|Flight and the Aircraft Engineer]]'' No. 1933, Vol. XLIX, 10 January 1946.
* [http://www.flightglobal.com/pdfarchive/view/1956/1956%20-%200578.html British Aero Engines (article and images).]" ''[[Flight International|Flight and the Aircraft Engineer]]''  No. 2468, Vol. 69, 11 May 1956.
*"[http://thunderboats.ning.com/page/1980-u1-miss-budweiser.html 1980 U-1 Miss Budweiser (article).]" Skid Fin Magazine, 2003, Volume 1 Number 2.
{{refend}}

==Further reading==
* [[Jeffrey Quill|Quill, J.]] (1983) ''Spitfire - A Test Pilot’s Story.'' Arrow Books. ISBN 0-09-937020-4

==External links==
{{Commons category}}
*[http://freespace.virgin.net/john.dell/meandgr.htm]
*[https://www.youtube.com/watch?v=QdfO657zHNY Griffon 58 on YouTube]
*[http://www.flightglobal.com/pdfarchive/view/1945/1945%20-%201852.html "Rolls-Royce Griffon (65)"] a 1945 [[Flight International|''Flight'']] article on the Griffon 65
*[http://www.flightglobal.com/pdfarchive/view/1946/1946%20-%200062.html "Rolls-Royce Griffon 130"] a 1946 ''Flight'' article

{{RRaeroengines}}
{{Use dmy dates|date=March 2011}}

[[Category:Rolls-Royce aircraft piston engines|Griffon]]
[[Category:Aircraft piston engines 1930–1939]]
[[Category:Articles containing video clips]]