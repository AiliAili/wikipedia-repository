{{other ships|HMS Algerine{{!}}HMS ''Algerine''}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Algerine FL472.jpg|300px]]
|Ship caption=''Algerine'' in profile, with her pennant number visible
}}
{{Infobox ship career
|Ship country=United Kingdom
|Ship flag={{shipboxflag|United Kingdom|naval}}
|Ship name=HMS ''Algerine''
|Ship ordered=15 November 1940
|Ship awarded=
|Ship builder=[[Harland and Wolff]], [[Belfast]]
|Ship original cost=
|Ship yard number=1132<ref name=Tom>{{cite book|last1=McCluskie|first1=Tom|title=The Rise and Fall of Harland and Wolff|date=2013|publisher=The History Press|location=Stroud|isbn=9780752488615|page=151|accessdate=}}</ref>
|Ship way number=
|Ship laid down=15 March 1941
|Ship launched=22 December 1941
|Ship completed=24 March 1942<ref name=Tom/>
|Ship commissioned=24 March 1942
|Ship identification=[[Pennant number]]: J213
|Ship motto=
|Ship nickname=
|Ship honours=
|Ship fate=Sunk by a torpedo from the {{ship|Italian submarine|Ascianghi||2|up=yes}} on 15 November 1942
|Ship notes=
|Ship badge=[[File:HMS Algerine J213 badge.png|100px]]
}}
{{Infobox ship characteristics
|Ship class={{sclass-|Algerine|minesweeper}}
|Ship displacement=*{{convert|850|LT|t|abbr=on}} (standard)
*{{convert|1125|LT|t|abbr=on}} ([[deep load]])
|Ship length={{convert|225|ft|m|1|abbr=on}} [[Length overall|o/a]]
|Ship beam={{convert|35|ft|6|in|m|1|abbr=on}}
|Ship draught={{convert|11|ft|m|1|abbr=on}} (deep load)
|Ship power=*{{convert|2000|shp|kW|lk=in|abbr=on}}
*2 × [[Admiralty 3-drum boiler]]s 
|Ship propulsion=*2 × shafts
*2 × [[Parsons Marine Steam Turbine Company|Parsons]] geared [[steam turbine]]s
|Ship speed={{convert|16.5|kn|lk=in}}
|Ship range={{convert|5000|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
|Ship complement=85
|Ship sensors=*[[Type 271 radar|Type 271]] surface-search [[radar]]
*[[List of World War II British naval radar#Type 291|Type 291]] air-search radar
|Ship armament=*1 × single [[QF 4 inch Mk V naval gun|{{convert|4|in|mm|sing=on|0}} Mk V gun]]
*4 × twin [[Oerlikon 20 mm cannon|{{convert|20|mm|adj=on|1}} Oerlikon]] [[autocannon]]
*2 × [[depth charge]] rails and 4 depth charge throwers 
|Ship notes=
}}
|}
'''HMS ''Algerine''''' was the [[lead ship]] of her namesake class of [[minesweeper]]s built for the [[Royal Navy]] during World War II, the [[Algerine-class minesweeper]]s. Initially assigned to the [[North Sea]], she was transferred to lead the 12th Minesweeping Flotilla. The Flotilla were posted to the [[Mediterranean]] to assist with [[Operation Torch]]. In 1942, after a successful mine clearing operation off [[Béjaïa|Bougie]], she was torpedoed by the {{ship|Italian submarine|Ascianghi||2|up=yes}}, causing ''Algerine'' to sink, leaving only eight survivors.

== Description ==

''Algerine'' displaced {{convert|850|LT|t}} at [[Displacement (ship)|standard]] load and {{convert|1125|LT|t}} at [[deep load]]. The ship had an [[length overall|overall length]] of {{convert|225|ft|m|1}}, a [[beam (nautical)|beam]] of {{convert|35|ft|6|in|m|1}} and a [[draft (hull)|draught]] of {{convert|8|ft|6|in|m|1}}. She was powered by [[Parsons Marine Steam Turbine Company|Parsons]] geared [[steam turbine]]s, driving two shafts, which gave a maximum speed of {{convert|16.5|kn|lk=in}}.{{sfnp|Lenton|1998|p=261}}

The ship mounted one single [[QF 4 inch Mk V naval gun|4-inch (102&nbsp;mm) Mk V gun]]. ''Algerine'' had four single mounts for {{convert|20|mm|1|adj=on}} [[Oerlikon 20 mm cannon|Oerlikon 20 mm]] [[autocannon]], and she was fitted with two [[depth charge]] rails, and four depth charge throwers.{{sfnp|Lenton|1998|p=261}}

== Career ==

''Algerine'' was [[Keel laying|laid down]] on 15 March 1941,<ref name="UB "/> by [[Harland & Wolff]], [[Belfast]], and [[ship naming and launching|launched]] on 22 December 1941. She was the eighth ship of the Royal Navy to be named {{HMS|Algerine||2}}.<ref name="NH">{{cite web | url=http://www.naval-history.net/xGM-Chrono-22MS-Algerine-Algerine.htm | title=HMS Algerine (J 213) - Algerine-class Fleet Minesweeper | work=Naval History | accessdate=29 March 2014 | author=Lt-Cdr Mason, Geoffrey}}</ref> After being completed, the ship was [[ship commissioning|commissioned]] on 24 March 1942, and adopted by [[Sittingbourne]] due to a [[Warship Week]] campaign.<ref name="NH "/>

=== 1942 ===

''Algerine'' joined the 9th Minesweeping Flotilla in May 1942 and began action in [[minesweeping]], escorting, and patrolling duties on the east side of England.<ref name="NH "/> She was proposed as leader for the 12th Minesweeping Flotilla, which would participate in action abroad.<ref name="NH "/> Her [[sister ship]]s from the 9th Flotilla, {{HMS|Alarm|J140|2}} and {{HMS|Albacore|J101|2}}, joined her, as did {{HMS|Acute|J106|2}}, and {{HMS|Cadmus|J230|2}}.<ref name="NH "/> In October, she was put forward to go to the Mediterranean to assist [[Operation Torch]], but her departure was delayed due to repair work. The other four ships in her flotilla left for [[Gibraltar]] as escorts to a convoy.<ref name="NH "/> Four days after the other ships left, ''Algerine'' escorted convoy KMF1 to [[Oran]].<ref name="NH "/>

In early November, she helped recover the escort [[destroyer]] {{HMS|Cowdray|L52|2}} off [[Algiers]] after ''Cowdray'' was damaged by an aerial attack.<ref name="NH "/> On 15 November,<ref name="UB "/><ref name="NH "/> ''Algerine'' and ''Alarm'' were positioned off [[Béjaïa|Bougie]], clearing mines.<ref name="UB "/> The mission had been successful, with 46 mines cleared;<ref name="UB "/> but, ''Algerine'' was torpedoed by the Italian {{sclass-|Adua|submarine}} {{ship|Italian submarine|Ascianghi||2}},<ref name="NH "/> commanded by [[Lieutenant commander]] Rino Erler.{{sfnp|Rohwer|1992|p=174|ps=}} The submarine had first fired two torpedoes at the middle ship in the trio, then fired another two torpedoes at the final ship, ''Algerine'':<ref name="UB "/> ''Algerine'' suffered heavy casualties and sank.{{sfnp|Walsh|2004|p=92-93|ps=}} The converted merchant vessel {{MV|Pozarica|1937|2}} rescued 32 men, of whom only 8 survived, internal wounds killing 24.<ref name="UB "/> The survivors had been on a [[Carley raft]].<ref name="UB">{{cite web | url=http://www.uboat.net/allies/warships/ship/3751.html | title=HMS Algerine (J 213) | work=UBoat.net | accessdate=29 March 2014}}</ref>

''Algerine''{{'}}s wreck lies at {{convert|1100|ft|m|abbr=on}}<ref name="WS">{{cite web | url=http://www.wrecksite.eu/wreck.aspx?14153 | title=HMS Algerine (J 213) (+1942) | work=Wrecksite | accessdate=29 March 2014}}</ref> on the northern coast of [[Algeria]].<ref name="UB "/>

== References ==
{{Reflist}}
{{coord|36|45|N|05|11|E|display=title}}

== Bibliography ==

* {{cite book|ref=harv|last=Lenton|first=H. T.|title=British & Empire Warships of the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1998|isbn=1-55750-048-7}}
* {{cite book|ref=harv|last=Rohwer|first=Jürgen|title=Chronology of the War at Sea 1939–1945: The Naval History of World War Two|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1992|edition=Second Revised|isbn=1-59114-119-2}}
* {{cite book|ref=harv|last=Walsh|first=Ronald|title=In the Company of Heroes|publisher=Troubador Publishing|location=Leicester|year=2004|edition=First|isbn=1-904744-47-8}}

{{Algerine class minesweepers}}
{{November 1942 shipwrecks}}
{{good article}}

{{DEFAULTSORT:Algerine (J213)}}
[[Category:Algerine-class minesweepers of the Royal Navy]]
[[Category:World War II minesweepers of the United Kingdom]]
[[Category:Ships built in Belfast]]
[[Category:Ships built by Harland and Wolff]]
[[Category:World War II escort ships of the United Kingdom]]
[[Category:Ships sunk by Italian submarines]]
[[Category:World War II shipwrecks in the Mediterranean]]
[[Category:Maritime incidents in November 1942]]