{{Infobox company
| name             = ProQuest
| logo             = 
| type             = Private; Subsidiary
| foundation       = 1938
| location         = [[Ann Arbor]], [[Michigan]], [[United States|USA]]
| key_people       = {{unbulleted list|Andy Snyder, Chairman|Kurt Sanford, CEO|Jonathan Collins, CFO|Rich Belanger, CIO}}
| industry         = Information & Data Provider
| parent           = [[Cambridge Information Group]]
| homepage         = {{URL|http://www.proquest.com}}
}}

[[Image:ProQuest Building Ann Arbor2.JPG|thumb|300px|right|ProQuest headquarters]]

'''ProQuest LLC''' is an [[Ann Arbor, Michigan]]-based global information-content and technology company<ref name="ProQuestHome">[http://www.proquest.com ProQuest Home Page], retrieved May 21, 2014</ref> founded in 1938 as '''University Microfilms''' by [[Eugene B. Power]]. ProQuest provides solutions, applications, and products for libraries.<ref name="InfoToday">{{cite web |url=http://www.infotoday.com/it/apr14/Kaser--ProQuest-Goes-Global.shtml |title=ProQuest Goes Global |publisher= Information Today |year=2014}}</ref>  Its resources and tools support research and learning,<ref name="ProQuestHome"/> publishing and dissemination,<ref name="Dissertations">[http://www.proquest.com/products-services/dissertations/ ProQuest Dissertations Publishing], retrieved June 15, 2014</ref> and the acquisition, management and discovery of library collections.<ref name="ProQuestHome"/>

From its founding as a producer of [[microfilm]] products and then as an [[electronic publishing|electronic publisher]],<ref name="PrivCo">[http://www.privco.com/private-company/proquest-company "ProQuest LLC | Private Company Financial Research | PrivCo.com"], retrieved May 23, 2014</ref> the company has grown through acquisitions.<ref name="InfoToday"/> Today, the company provides tools for discovery and [[Reference management software|citation management]] and platforms that allow library users to discover, manage, use and share research<ref>[http://www.proquest.com/libraries/ "ProQuest - For Libraries"], retrieved May 23, 2014</ref> gained from authoritative content.<ref>[http://www.proquest.com/about/publishers-partners/ Welcome, Publishers and Partners], retrieved June 15, 2014</ref> Total content, including [[Thesis|dissertations and theses]], [[E-book|ebooks]], newspapers, periodicals, historical collections, governmental and cultural archives<ref name="JiscRelease">[http://www.proquest.com/about/news/2014/Jisc-and-ProQuest-Enable-Access-to-Essential-Digital-Content.html "Jisc and ProQuest Enable Access to Essential Digital Content"], retrieved May 21, 2014</ref> and other aggregated databases is estimated at over 125 billion [[digital page]]s.<ref name="Hoovers">[http://www.hoovers.com/company-information/cs/company-profile.ProQuest_LLC.11a83e5ad3d61201.html "ProQuest LLC | Company Profile from Hoover's"], retrieved May 21, 2014</ref> Content is accessed most commonly through library Internet gateways.<ref name="PrivCo"/> The current chief executive officer is Kurt P. Sanford.<ref>{{cite web |url=http://www.proquest.com/en-US/aboutus/pressroom/11/20110705.shtml |title=New CEO |publisher= ProQuest LLC |year=2011}}</ref> ProQuest is part of [[Cambridge Information Group]].

==Businesses==
ProQuest was founded as a microfilm publisher.<ref>[http://www.americanlibrariesmagazine.org/blog/microfilm-s-75th-anniversary Microfilm's 75th Anniversary], retrieved June 15, 2014</ref>  It began publishing doctoral dissertations in 1939,<ref>Thistlethwaite, Polly, "Publish. Perish?: The academic author and open access publishing", ''Media Authorship'' (edited by Cynthia Chirs, David A. Gerstner). Routledge, 2013, p. 274</ref> has published more than 3 million searchable dissertations and theses,<ref>[http://www.proquest.com/libraries/academic/dissertations-theses/ Dissertations & Theses], retrieved June 15, 2014</ref> and is designated as an offsite digital archive for the United States Library of Congress.<ref>[http://www.proquest.com/products-services/pqdtglobal.html ProQuest Dissertations & Theses Global], retrieved September 6, 2014</ref> The company's scholarly content includes dissertations and theses, primary source material, ebooks, scholarly journals, historical and current newspapers and periodicals, data sources, and other content of interest to researchers.<ref>[http://www.proquest.com/libraries/academic/ ProQuest - Academic], retrieved June 15, 2014</ref> These are made available through a variety of Web-based interfaces.<ref>[http://www.proquest.com/connect/ ProQuest - Connect], accessed June 15, 2014</ref> A recent offering, ProQuest Video Preservation and Discovery Service, allows libraries to preserve and provide access to their proprietary audio and video collections.<ref>[http://www.against-the-grain.com/2014/01/news-announcements-192014/ Against the Grain, News and Announcement 1/9/2014], retrieved June 15, 2014</ref>

ProQuest LLC also operates businesses under the following names:<ref name="JiscRelease"/>

*[[R.R. Bowker|Bowker]] provides bibliographic information management solutions to publishers, libraries and booksellers,<ref>[http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=1165718 "RR Bowker LLC - Businessweek"], retrieved May 21, 2014</ref> and is the [[International Standard Book Number|ISBN]] Agency for the United States.<ref>[http://www.bowker.com/en-US/products/default.shtml "Bowker - Products & Services"], retrieved May 21, 2014.</ref>
*[[Dialog (online database)|Dialog]] is an online information service with more than 1.4 billion unique records<ref>[http://www.cig.com/portfolio/information-services/dialog/ Dialog | Cambridge Information Group], retrieved May 22, 2014</ref> curated for corporate, business and government researchers, with a focus on pharmaceutical, engineering and patent research.<ref>[http://www.dialog.com/ Dialog, LLC home page] retrieved May 22, 2014</ref>
*EBL (Ebook Library), an ebook aggregator with a catalog of titles from academic publishers, serves academic, corporate and research libraries<ref>[http://www.cig.com/2013/05/15/proquest-completes-acquisition-of-ebl/ "ProQuest completes acquisition of EBL"], retrieved May 22, 2014</ref> while supporting emerging [[Library collection development|collection development]] models such as [[patron-driven acquisition]].<ref>[http://www.proquest.com/about/news/2014/EBL-Unveils-the-New-LibCentral.html "EBL Unveils the New LibCentral"], retrieved May 22, 2014</ref>
*[[ebrary]] offers access to ebook collections, by subscription or a [[perpetual access|perpetual archive]] model, in subject packages tailored for academic, corporate, government, public and high school libraries.<ref>[http://www.ebrary.com/corp/products.jsp "ebrary | Pre-Packaged Products"], retrieved May 22, 2014</ref>
*[[Serials Solutions]] delivers discovery and e-resource access and management services<ref>[http://www.serialssolutions.com/en/about/ "About Us|Serials Solutions"], retrieved May 21, 2014</ref> ([[ERAMS]]), using a Software-as-a-Service model.<ref>[http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=10738861 "Serials Solutions, Inc.: Private Company Information - Businessweek"], retrieved May 21, 2014</ref>

==History==
[[File:ASA conference 2008 - 26.JPG|thumb|2008 conference booth]]
[[Eugene Power]], a 1930 M.B.A. graduate of the [[Ross School of Business|University of Michigan]], founded the company as University Microfilms in 1938, preserving works from the [[British Museum]] on microfilm. By June 1938, Power worked in two rented rooms from a downtown Ann Arbor funeral parlor, specializing in microphotography to preserve library collections. In his autobiography ''Edition of One'', Power details the development of the company, including how University Microfilms assisted the [[Office of Strategic Services|OSS]] during [[World War II]].<ref>{{cite book |last1=Power |first1=Eugene B. |author2=Robert Anderson  |title=Edition of One: The Autobiography of Eugene B. Power |year=1990 |publisher=University Microfilms |location=Ann Arbor, Michigan |isbn=0-8357-0898-5 |pages=128–142}}</ref> This work mainly involved filming maps and European newspapers so they could be shipped back and forth overseas more cheaply and discreetly.

Power also noticed a [[niche market]] in [[dissertation]]s publishing. Students were often forced to publish their own works in order to finish their [[doctoral degree]]. Dissertations could be published more cheaply as microfilm than as books. ProQuest still publishes so many dissertations that its [[ProQuest Dissertations and Theses|Dissertations and Theses]] collection (formerly called Digital Dissertations) has been declared the official U.S. off-site repository of the [[Library of Congress]].<ref>{{cite press release|url=http://www.loc.gov/today/pr/1999/99-007.html |title=Library of Congress and Copyright Office Sign Landmark Agreement with UMI  |publisher=Library of Congress |date=January 22, 1999 |accessdate=2012-03-13}}</ref>

The idea of universal adoption of microfilm publication of doctoral dissertations was furthered considerably by two articles researched and written by a then recent recipient of the doctorate in History at Stanford University.  Vaughn Davis Bornet seized on the idea and published "Doctoral Dissertations and the Stream of Scholarship"<ref>College and University. October 1952, pp. 11–30</ref> and "Microfilm Publication of Doctoral Dissertations".<ref>''Bulletin of the American Association of University Professors''. Autumn, 1953. pp. 503-513.</ref>

As the dissertations market grew, the company expanded into filming newspapers and [[periodical]]s. The company's main newspaper database is [[ProQuest NewsStand]].

[[Xerox]] owned the company for a time in the 1970s and 1980s; the name of the company changed several times in this period, from '''University Microfilms''' to '''Xerox University Microfilms''', to '''University Microfilms International''', then shortened to '''UMI'''. In 1985 it was purchased from Xerox by [[Bell & Howell]].<ref>{{cite news|last1=Winter|first1=Christine|title=Bell, Howell Acquires Two Data Firms|url=http://articles.chicagotribune.com/1985-11-14/business/8503180622_1_umi-gerald-schultz-bekins-records-management|accessdate=Sep 10, 2014|work=Chicago Tribune|date=November 14, 1985}}</ref>

In the 1980s, UMI began producing [[CD-ROM]]s that stored [[Bibliographic database|databases]] of periodicals abstracts and indexes. At a time when modem connections were slow and expensive, it was more efficient to mail database CD-ROMs regularly to subscribing libraries, who installed the discs on dedicated PCs. The ProQuest brand name was first used for databases on CD-ROM. An online service called ProQuest Direct was launched in 1995; its name was later shortened to just ProQuest.<ref>{{cite web |url=http://www.proquest.com/brand/proquest.shtml |title=ProQuest (brand)|publisher=proquest.com}}</ref> The bibliographic databases are mainly sold to schools, universities and libraries.

In 1998, the company announced the "[[Digital Vault Initiative]]", purported to include 5.5 billion images digitized from UMI microfilm, including some of the best existing copies of major newspapers dating back 100 to 150 years, and Early English books dating back to the 15th century. While work continues to digitize the contents of the microfilm vault, ProQuest is already providing navigation of 125 billion digital pages, including nearly 20 million pages of newspaper content dating from pre-Revolutionary War America.

In 1999, the company name changed to '''Bell & Howell Information and Learning''', and then in 2001 to '''ProQuest Information and Learning'''; Bell & Howell renamed itself the ProQuest Company.<ref>{{cite web |url= http://newsbreaks.infotoday.com/nbReader.asp?ArticleId=17575 |title=Bell & Howell Becomes ProQuest Company |date= June 11, 2001 |publisher=Information Today |accessdate=June 20, 2014 }}</ref>

Also in 1999, the company acquired [[Chadwyck-Healey]], a one-time microfilm publishing company that was one of the first to produce full-text CD-ROM databases. This acquisition gave Proquest ownership of a 100+ person publishing operation based in Cambridge, England and became the basis for a substantial overseas expansion.

During the 2000–2004 fiscal years, as well as the first three quarters of the 2005 fiscal year, ProQuest systematically overstated its net income.  Cumulatively, pre-tax profits were overstated by 129.9 million dollars (or about 31 percent).<ref>{{cite web | url=http://www.sec.gov/litigation/complaints/2008/comp20650.pdf |title=SEC Complaint vs. Scott Hirth and ProQuest Company [in 2008] known as Voyager Learning Company |publisher=United States Securities and Exchange Commission |accessdate=October 30, 2014 }}</ref>  In 2008, Voyager Learning Company and ProQuest's CFO (from the time of the earnings overstatements) settled SEC "charges without admitting or denying the allegations of the SEC's complaint".<ref>{{cite web | url=http://www.sec.gov/news/press/2008/2008-147.htm |title=Press Release: SEC Charges Ann Arbor-Based Company and Former Executive in Accounting Fraud Scheme |publisher=United States Securities and Exchange Commission |accessdate=October 30, 2014 }}</ref>

In 2001 Proquest acquired Norman Ross Publishing, a small New York based microfilm publisher.<ref>http://www.rosspub.com/normanbio.html</ref>

In 2004, ProQuest Information and Learning acquired Seattle start-up [[Serials Solutions]], a venture providing access management and search services for content hosted by other companies.

Also in 2004, the company acquired Copley Publishing Group.<ref>{{cite web |url=http://findarticles.com/p/articles/mi_hb3328/is_/ai_n26177247 |title=ProQuest Information and Learning has acquired Copley Publishing Group, Inc. |date= May 2004 |publisher=findarticles.com}}</ref>

In 2006, ProQuest Company, then the parent company of ProQuest Information and Learning, sold it to [[Cambridge Information Group]], owner of [[R.R. Bowker]].<ref>{{cite web |title=Cambridge Information Group Timeline |archiveurl= https://web.archive.org/web/20141020173911/http://www.ulib.niu.edu/publishers/CIG.htm |deadurl=yes |archivedate=October 2014 |url=http://www.ulib.niu.edu/publishers/index.htm |work=The Academic Publishing Industry: A Story of Merger and Acquisition |author= Mary H. Munroe |year=2004 |via= Northern Illinois University }}</ref>  From the time of this sale, this article follows the history of ProQuest Information and Learning, not ProQuest Company.  (ProQuest Company subsequently renamed itself to Voyager Learning Company, and later became part of [[Cambium Learning Group]].<ref>{{cite web |url=http://cambiumlearning.investorroom.com/index.php?s=52&cat=1 |title=Cambium Learning Group Inc. - Investor Relations - Management Team |accessdate=October 30, 2014 }}</ref>)

In 2007, ProQuest Information and Learning was merged with [[CSA (database company)|CSA]] to form ProQuest CSA. Later that year it was renamed ProQuest LLC.

In 2008, ProQuest LLC acquired complete ownership of [[RefWorks]], a web-based [[citation manager]] of which it had been part owner since 2001. RefWorks was merged with ProQuest's existing COS business to form RefWorks/COS.<ref>{{cite web |url=http://www.proquest.co.uk/en-UK/aboutus/pressroom/08/20080118.shtml |title=ProQuest acquires RefWorks |publisher=ProQuest LLC  |date=January 18, 2008 |accessdate=February 2, 2012 }}</ref>

Also in 2008, ProQuest acquired [[Dialog (online database)|Dialog]], a major online database firm, from [[Thomson Reuters]].<ref>"{{cite press release |url=http://www.proquest.com/en-US/aboutus/pressroom/08/20080701.shtml |title=ProQuest acquires Dialog |publisher=ProQuest LLC. |date=July 1, 2008 |accessdate=March 6, 2009}}</ref><ref>{{cite web |url=http://scientific.thomsonreuters.com/press/2008/8458603/ |title=ProQuest signs agreement to acquire Dialog business from Thomson Reuters |publisher=scientific.thomsonreuters.com |date=June 12, 2008 |accessdate=January 18, 2009}}</ref>

In 2010, ProQuest acquired two properties from LexisNexis, Congressional Information Service (CIS) and University Publications of America (UPA). CIS produced one of the world's most exhaustive online collections of legislative content and highly respected statistical works, while UPA included deep historical content sets. The acquisition included digital products and an expansive microfilm vault that would leverage ProQuest's strength in conversion from film to searchable electronic formats.<ref>{{cite press release |url=http://www.proquest.com/en-US/aboutus/pressroom/10/20101130.shtml |title=ProQuest Acquires Acclaimed Congressional Information Service and University Publications of America from LexisNexis |publisher= ProQuest LLC. |date=November 30, 2010 |accessdate= December 7, 2010}}</ref>

In 2011, ProQuest acquired [[Ebrary]], an online digital library of full texts of over 170,000 scholarly [[e-book]]s.<ref>{{cite journal | title = ProQuest acquires ebrary | journal = American Libraries | date = January 8, 2011 | first = Sean | last = Fitzpatrick| id = | url = http://americanlibrariesmagazine.org/news/01082011/proquest-acquires-ebrary | accessdate = 2012-02-03}}</ref>

In 2013, ProQuest acquired Ebook Library (EBL), with plans to combine the strongest features of ebrary and EBL into a single, comprehensive e-book platform.<ref>{{cite press release |url=http://www.proquest.com/about/news/2013/ProQuest-Signs-Definitive-Agreement-to-Acquire-EBL.html |title=ProQuest Signs Definitive Agreement to Acquire EBL
 |publisher=ProQuest LLC  |date=January 22, 2013 |accessdate=June 20, 2014 }}</ref>
 
In 2014, ProQuest acquired Pi2 Solutions, a privately owned company specializing in Product Literature Database (PLD) systems for the biopharmaceutical industry. The company plans to align Pi2 with its ProQuest Dialog corporate information service.<ref>{{cite press release |url=http://www.proquest.com/about/news/2014/ProQuest-Acquires-Pi2.html |title=ProQuest Acquires Pi2 |publisher= ProQuest LLC. |date=May 1, 2014 |accessdate= May 2, 2014}}</ref>

In 2015, ProQuest acquired SiPX<ref>{{cite web |url=http://sipx.com/proquest-acquires-sipx/ |title=ProQuest Acquires SIPX |publisher= ProQuest LLC. |date=April 8, 2015 |accessdate= September 5, 2015}}</ref> and Coutts Information Services, including the MyiLibrary platform and the Online Acquisitions and Selection Information System (OASIS).<ref>{{cite press release |url=http://www.proquest.com/about/news/2015/ProQuest-to-Acquire-Coutts-Information-Services-and-MyiLibrary-from-Ingram-Content-Group.html |title=ProQuest to Acquire Coutts Information Services and MyiLibrary from Ingram Content Group |publisher= ProQuest LLC. |date=April 30, 2015 |accessdate= September 5, 2015}}</ref>

In October 2015, ProQuest acquired [[Ex Libris Group|Ex Libris]]. It was to merge the Workflow Solutions division of ProQuest, which included the former Serials Solutions, into Ex Libris, with the enlarged entity to be named "Ex Libris, a ProQuest Company".<ref>{{cite journal |last=Breeding |first=Marshall |date=6 October 2015 |title=ProQuest to Acquire Ex Libris |url= http://americanlibrariesmagazine.org/blogs/the-scoop/proquest-to-acquire-ex-libris/ |journal=[[American Libraries]] |location=Chicago |publisher=[[American Library Association]]}}</ref>

In June 2016, ProQuest acquired [[Alexander Street Press]], a provider of streaming videos and ebooks.<ref>{{cite press release |url=http://www.proquest.com/about/news/2016/Alexander-Street-Press-Joins-the-ProQuest-Family-of-Companies.html |title=Alexander Street Press Joins the ProQuest Family of Companies
 |publisher=ProQuest LLC  |date=June 22, 2016 |accessdate=June 22, 2016 }}</ref>

==Archived newspapers==
{{columns-list|colwidth=30em|
*''[[The American Hebrew & Jewish Messenger]]'' (1857–1922)
*''[[The American Israelite]]'' (1854–2000)
*''[[The Arizona Republic]]'' (1890–1922)
*''[[Atlanta Daily World]]'' (1931–2003)
*''[[Atlanta Journal-Constitution]]'' (1868–1945)
*''[[The Baltimore Afro-American]]'' (1893–1988)
*''[[The Baltimore Sun]]'' (1837–1988)
*''[[The Boston Globe]]'' (1872–1982)
*''[[Chicago Defender]]'' (1910–1975)
*''[[Chicago Tribune]]'' (1849–1990)
*''[[Christian Science Monitor]]'' (1908–2000)
*''[[The Cincinnati Enquirer]]'' (1841–1922)
*''[[Cleveland Call & Post]]'' (1934–1991)
*''[[Detroit Free Press]]'' (1831–1922)
*''[[The Globe and Mail]]'' (1844–2010)
*''[[The Guardian]]'' (1821–2003) and ''[[The Observer]]'' (1791–2003)
*''[[Hartford Courant]]'' (1764–1988)
*''[[Indianapolis Star]]'' (1903–1922)
*''[[Irish Times]]'' (1859–2012) and ''[[The Weekly Irish Times]]'' (1876–1958)
*''[[Jerusalem Post]]'' (1932–1988)
*''[[The Jewish Advocate]]'' (1905–1990)
*''[[Jewish Exponent]]'' (1887–1990)
*''[[Los Angeles Sentinel]]'' (1934–2005)
*''[[Los Angeles Times]]'' (1881–1990)
*''[[The Louisville Courier-Journal]]'' (1830–1922)
*''[[Minneapolis Tribune]]'' (1867–1922)
*''[[The Nashville Tennessean]]'' (1812–1922)
*''[[Newsday]]'' (1940–1985)
*''[[New York Amsterdam News]]'' (1922–1993)
*''[[The New York Times]]'' (1851–2010)
*''[[New York Tribune]]'' (1841–1962)
*''[[The Norfolk Journal & Guide]]'' (1921–2003)
*''[[The Philadelphia Tribune]]'' (1912–2001)
*''[[Pittsburgh Courier]]'' (1911–2002)
*''[[San Francisco Chronicle]]'' (1865–1922)
*''[[South China Morning Post]]'' (1903–current)
*''[[The Scotsman]]'' (1817–1950)
*''[[St. Louis Post-Dispatch]]'' (1874–1922)
*''[[The Times of India]]'' (1838–2004)
*''[[Toronto Star]]'' (1894–2012)
*''[[The Wall Street Journal]]'' (1889–1996)
*''[[The Washington Post]]'' (1877–1997)
*Chinese Newspapers Collection (1832–1953)
}}

==Products==
Among the products the company sells to clients such as public and research libraries are:<ref name="ProquestTitles">{{cite web |url=http://www.proquest.com/en-US/products/titlelists/tl-csa.shtml |title=Title List System |work=Products & Services |author=ProQuest |accessdate=July 1, 2012}}</ref><ref name="CSA">{{cite web |url=http://www.proquest.com/en-US/products/titlelists/tl-csa.shtm |title=CSA Title Lists |work=Products & Services |author=ProQuest |accessdate=July 1, 2012}}</ref><ref name="Chadwyck">{{cite web |url=http://www.proquest.com/en-US/products/titlelists/tl-ch.shtml |title=Chadwyck-Healey Title Lists |work=Products & Services |author=ProQuest |accessdate=July 1, 2012}}</ref><ref name="eLibrary">{{cite web |url=http://www.proquest.com/en-US/products/titlelists/tl-k12.shtml |title=eLibrary Title Lists |work=Products & Services |author=ProQuest |accessdate=July 1, 2012}}</ref><ref name="SIRS">{{cite web |url=http://www.proquest.com/en-US/products/titlelists/tl-sirs.shtml |title=SIRS Title Lists |work=Products & Services |author=ProQuest |accessdate=July 1, 2012}}</ref>
{{columns-list|colwidth=30em|
* ABI/INFORM Collection (including ABI/INFORM Dateline, ABI/INFORM Global, and ABI/INFORM Trade & Industry)
* Accounting, Tax & Banking Collection (can be limited to "Accounting & Tax Database" or "Banking Information Database")
* Advanced Technologies Database with Aerospace
* Aerospace Database
* AFI Catalog
* AIDS & Cancer Research Abstracts
* Algology Mycology and Protozoology Abstracts (Microbiology C)
* Alt-PressWatch
* Aluminium Industry Abstracts
* American Periodicals Series
* Animal Behavior Abstracts
* ANTE: Abstracts in New Technology & Engineering
* Applied Social Sciences Index and Abstracts (ASSIA)
* AquaBrowser
* AquaBrowser Liquid
* Aqualine
* ARTbibliographies Modern (ABM)
* Arts & Humanities Full Text
* ASFA: Aquatic Sciences and Fisheries Abstracts
* Asian & European Business Collection
* Bacteriology Abstracts (Microbiology B)
* Biological Sciences
* Biotechnology and BioEngineering Abstracts
* Biotechnology Research Abstracts
* Black Studies Center
* British Humanities Index (BHI)
* Calcium & Calcified Tissue Abstracts
* Canadian Newsstand
* CBCA
* Ceramic Abstracts
* Chemoreception Abstracts
* Civil Engineering Abstracts
* Computer and Information Systems Abstracts (Module)
* Copper Technical Reference Library
* Corrosion Abstracts
* Design and Applied Arts Index (DAAI)
* [[Dialog (online database)|Dialog]]
* Digital Sanborn Maps<ref name="Mann2015" />
* Early European Books
* Earthquake Engineering Abstracts
* [[Ebrary]]
* Ecology Abstracts
* Electronics & Communications Abstracts
* eLibrary
* eLibrary Canada
* eLibrary Curriculum Edition
* eLibrary Elementary
* eLibrary Science
* Engineered Materials
* Engineering Research Database
* Entomology Abstracts
* Environment Abstracts
* Environmental Engineering Abstracts
* Environmental Science and Pollution Management
* ERIC PlusText
* Ethnic NewsWatch
* FIAF International Index to Film Periodicals Plus
* Gannett Military Newspapers
* Gannett Newspapers
* GenderWatch
* Genetics Abstracts
* Health & Safety Science Abstracts
* HeritageQuest Online<ref name="Mann2015">{{cite book|author=Thomas Mann|title= Oxford Guide to Library Research|year=2015 |edition=4th |publisher=Oxford University Press|isbn=978-0-19-993106-4 |chapter=Special Subjects and Formats: Genealogy and Local History |chapterurl=https://books.google.com/books?id=lUZnBgAAQBAJ&pg=PA298 }}</ref>
* History Study Center
* Immunology Abstracts
* Industrial and Applied Microbiology Abstracts (Microbiology A)
* International Bibliography of Art (IBA)
* International Bibliography of the Social Sciences (IBSS)
* International Index to Black Periodicals
* International Index to Music Periodicals
* International Index to the Performing Arts
* Library and Information Science Abstracts (LISA)
* Linguistics and Language Behavior Abstracts (LLBA)
* Literature Online
* Literature Online Reference Edition
* Materials Business File
* Materials Research Database
* Mechanical & Transportation Engineering Abstracts
* MEDLINE with Full Text
* METADEX
* Meteorological & Geoastrophysical Abstracts
* Middle East Newsstand
* Neurosciences Abstracts
* Nucleic Acids Abstracts
* Oceanic Abstracts
* Oncogenes and Growth Factors Abstracts
* PAIS
* Periodicals Archive Online
* Periodicals Index Online
* Pharmaceutical News Index
* Physical Education Index
* Pollution Abstracts
* PRISMA (Publicaciones y Revistas Sociales y Humanísticas)
* Professional ProQuest Central
* ProQuest 5000
* ProQuest 5000 International
* ProQuest Advanced Technologies & Aerospace Collection
* ProQuest Agricultural Science Collection
* ProQuest ANZ Newsstand
* ProQuest African American Heritage
* ProQuest Aquatic Science Collection
* ProQuest Atmospheric Science Collection
* ProQuest Biological Science Collection
* ProQuest Career and Technical Education
* ProQuest Central
* ProQuest Computer Science Collection
* ProQuest Criminal Justice Periodicals Index
* ProQuest Discovery
* [[ProQuest Dissertations and Theses]] (formerly known as ''DAI, Dissertation Abstracts International'')
* ProQuest Earth Science Collection
* ProQuest Education Journals
* ProQuest Engineering Collection
* ProQuest Entrepreneurship
* ProQuest Environmental Science Collection
* ProQuest Family Health
* ProQuest Health & Medical Complete
* ProQuest Health Management
* ProQuest Historical Annual Reports
* ProQuest International Newswires
* ProQuest Library Science
* ProQuest Materials Science Collection
* ProQuest Medical Library
* ProQuest Military Collection
* ProQuest Natural Science Collection
* [[ProQuest NewsStand]]
* ProQuest Nursing & Allied Health Source
* ProQuest Obituaries
* ProQuest Political Science
* ProQuest Polymer Science Collection
* ProQuest Professional Education
* ProQuest Psychology Journals
* ProQuest Public Health
* ProQuest Religion
* ProQuest Research Library
* ProQuest Sanborn Maps Geo Edition
* ProQuest Science Journals
* ProQuest SciTech Collection
* ProQuest Social Science Journals
* ProQuest Sociology
* ProQuest Technology Collection
* Resource/One
* Risk Abstracts
* SIRS Decades
* SIRS Discoverer
* SIRS Government Reporter
* SIRS Issues Researcher
* SIRS Renaissance
* Social Services Abstracts
* Sociological Abstracts
* Solid State and Superconductivity Abstracts
* State Packages
* Summon
* Sustainability Science Abstracts
* Technology Research Database
* Toxicology Abstracts
* [[Udini]] by ProQuest
* US Hispanic Newsstand
* Virology and AIDS Abstracts
* Water Resources Abstracts
* Worldwide Political Science Abstracts
}}

== See also ==
{{main cat|ProQuest}}
{{columns-list|colwidth=30em|
* [[Annual Bibliography of English Language and Literature]]
* [[CSA (database company)]]
* [[Dissertations Abstracts]]
* [[Ex Libris Group]]
* [[International Bibliography of the Social Sciences]]
* [[R.R. Bowker]]
* [[RefWorks]]
* [[Serials Solutions]]
* [[Ulrich's Periodicals Directory]]
}}

== References ==
{{Reflist|30em}}

==Further reading==
* Eugene B. Power, ''Edition of One: The Autobiography of Eugene B. Power, Founder of University Microfilms'', 1990 ISBN 0-8357-0898-5.

==External links==
* {{Official website|http://www.proquest.com/}}
* [http://napubco.com/umi.html National Archive Publishing Company]

{{DEFAULTSORT:ProQuest}}
[[Category:ProQuest| ]]
[[Category:Bibliographic database providers]]
[[Category:Educational publishing companies]]
[[Category:Publishing companies based in Michigan]]
[[Category:Companies based in Ann Arbor, Michigan]]
[[Category:Publishing companies established in the 1930s]]
[[Category:Xerox]]