{{Multiple issues|
{{refimprove|date=August 2016}}
{{notability|Biographies|date=August 2016}}
}}

{{Use dmy dates|date=March 2011}}
{{Infobox military person
|name= Otto Bertram
|birth_date=30 April 1916
|death_date={{death-date and age|df=yes|8 February 1987|30 April 1916}}
|image=
|caption=
|birth_place=[[Wilhelmshaven]]
|death_place=[[Freiburg im Breisgau]]
|nickname=Otsch
|allegiance={{flag|Nazi Germany}} (to 1945)<br/>{{flag|West Germany}}
|branch={{Luftwaffe}}<br/>{{GAF}}
|serviceyears=1935–45 (Wehrmacht)<br/>1956–? [[Bundeswehr]]
|rank=Major (Wehrmacht)<br />Oberstleutnant (Bundeswehr)
|commands=III./[[JG 2]]<br/>I./[[JG 101]]<br/>I./[[JG 6]]
|unit= [[Condor Legion]]<br/>[[JG 2]]<br/>[[JG 101]]
|battles=[[Spanish Civil War]]
----
[[World War II]]
|awards=[[Spanish Cross]]<br/>[[Knight's Cross of the Iron Cross]]
|laterwork=[[Bundeswehr]]}}

'''Otto Bertram''' (30 April 1916 in [[Wilhelmshaven]] – 8 February 1987 in [[Freiburg im Breisgau]]) was a German [[Spanish Civil War]] and [[World War II]] fighter ace who served in the Luftwaffe from 1935 until the end of World War II. He later joined the Bundeswehr and served as an [[military attaché]].  He shot down a total of 22 enemy aircraft, nine of which were claimed during the Spanish Civil War.

==Spanish Civil War==
Born in 1916, Betram joined the Luftwaffe in 1935. Bertram joined the [[Condor Legion]] in March 1938, supporting [[Francisco Franco|Franco]]'s Nationalists in the Spanish Civil War. During the course of the war, Bertram, a [[leutnant]], claimed nine victories flying with 1./[[Jagdgruppe 88]], becoming one of the most successful fighter pilots in that conflict. On 4 October 1938, he was shot down by a Republican [[Polikarpov I-15|I-15]] fighter. After bailing out he was taken prisoner of war. For his accomplishments in Spain he was awarded the [[Spanish Cross]] in Gold with Diamonds.

==World War II==
On 26 October 1939, Bertram was appointed ''[[Staffelkapitän]]'' of 1./[[JG 2]]. Now an [[oberleutnant]], Bertram claimed his first aerial victory of World War II on 20 April 1940, when he downed a Morane 406 fighter over [[Saint-Avold]], flown by future French ace Sgt. Chef Antoine Casenobe. However, the claim was not confirmed. In total, he claimed four victories during the [[Battle of France]], although he was forced to crashland after gaining two victories on 19 May 1940.  He returned to his unit unhurt.

Bertram led 1./JG 2 during the opening phases of the [[Battle of Britain]]. He claimed seven [[Royal Air Force]] (RAF) fighters downed in five days between 2 September 1940 and 6 September with the unit. On 26 September 1940, he was promoted to [[hauptmann]] and appointed ''Gruppenkommandeur'' of III./JG 2. He claimed two further victories with the unit, two RAF [[Bristol Blenheim]] twin-engine bombers shot down near Le Havre on 9 October, to record his 21st and 22nd victories.

On 28 October 1940, Bertram was awarded the [[Knight's Cross of the Iron Cross]] for 13 victories in World War II and was banned from further combat flying and ordered to return to Germany.  Both of his brothers who were also serving in the Luftwaffe had recently been killed in action. Hans, ''Gruppenadjutant'' of I./JG 27, was shot down over England in September 1940, and Karl, a nightfighter pilot with 9./NJG 1, was killed when he crashed his [[Bf 110]] west of [[Kiel]] following an engagement with a RAF bomber on 28 October.

Bertram, as the sole surviving brother, was hence excused from further combat duty. He was to spend the remainder of the war in administrative posts or training units. He served at the Jagdfliegerschule 5 at Wien-Schwechat for the rest of 1940 and into 1941.  He was [[Gruppenkommandeur]] of I./[[JG 101]] from 6 January 1943 to 30 April 1944. From February 1945, promoted to [[major]], Bertram was ''Gruppenkommandeur'' of I./[[JG 6]], a position he held until the end of the war.

==Later life==
Bertram joined the [[Bundeswehr]] after the re militarisation of the [[Federal Republic of Germany]] in 1956. He held various positions in the Bundeswehr including that of press officer. Promoted to [[Oberstleutnant]] he served as a military attaché in [[Belgium]] and [[Luxembourg]].

Bertram died in [[Freiburg im Breisgau]], at the age of 70.

==Awards==
* [[Spanish Cross]] in Gold with Diamonds
* [[Wound Badge]] (1939) in Silver
* [[Pilot's Badge]]
* [[Iron Cross]] (1939) 2nd and 1st Class
* [[Knight's Cross of the Iron Cross]] on 28 October 1940 as ''[[Hauptmann]]'' and ''[[Gruppenkommandeur]]'' of the III./JG "Richthofen"<ref>Scherzer 2007, p. 218.</ref>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=Forsyth
  |first=Robert
  |year=2011
  |title=Aces of the Legion Condor
  |location=Oxford, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84908-347-8
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
{{Refend}}

{{Knight's Cross recipients of JG 2}}

{{Aces of the Condor Legion}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Bertram, Otto}}
[[Category:1916 births]]
[[Category:1987 deaths]]
[[Category:People from Wilhelmshaven]]
[[Category:People from the Province of Hanover]]
[[Category:Spanish Civil War flying aces]]
[[Category:German World War II flying aces]]
[[Category:German Air Force personnel]]
[[Category:Condor Legion personnel]]
[[Category:Recipients of the Spanish Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]