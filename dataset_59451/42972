[[File:Armstrong-Siddeley Special Six engine, in Hiduminium alloy (Autocar Handbook, 13th ed, 1935).jpg|thumb|Six cylinder, 5 litre, all-Hiduminium engine for the [[Armstrong Siddeley]] Special]]
The '''Hiduminium alloys''' or '''R.R. alloys''' are a series of high-strength, high-temperature [[aluminium alloy]]s, developed for aircraft use by [[Rolls-Royce Limited|Rolls-Royce]] ("RR") before [[World War II]].<ref name="Camm, R.R. Alloys" >{{cite book
  |author=FJ Camm  |authorlink=FJ Camm
  |chapter=R.R. Alloys
  |title=Dictionary of Metals and Alloys
  |edition=3rd 
  |date=January 1944
  |pages=102
  |ref=Camm, R.R. Alloys
}}</ref> They were manufactured and later developed by [[#High Duty Alloys Ltd.|High Duty Alloys Ltd.]].<ref name="Camm, R.R. Alloys" /> The name '''''Hi'''''-'''''Du'''''-Minium is derived from that of '''''Hi'''''gh '''''Du'''''ty Alu'''''minium''''' Alloys.

The first of these Hiduminium alloys was termed '' 'R.R.50' ''.<ref name="Camm, R.R. Alloys" /> This alloy was first developed for motor-racing [[piston]]s,<ref name="Camm, Hiduminium" >{{cite book
  |author=FJ Camm  |authorlink=FJ Camm
  |chapter=Hiduminium
  |title=Dictionary of Metals and Alloys
  |edition=3rd 
  |date=January 1944
  |pages=58
  |ref=Camm, Hiduminium
}}</ref> and was only later adopted for [[aircraft engine]] use. It was a development of the earlier  [[Y alloy]], the first of the [[nickel]]-containing light [[aluminium alloy]]s.<ref name="Murphy, J RAeS, 1966" >{{cite journal
  |journal=J. [[Royal Aeronautical Society]]
  |title=Materials in Aircraft Structures
  |last=Murphy  |first=A.J.
  |volume=70  |issue=661
  |year=1966
  |issn=0368-3931
  |pages=117
  |ref=Murphy, J RAeS, 1966
}}</ref> These alloys are one of the three main groups of high-strength aluminium alloys, the [[nickel-aluminium alloy]]s having the advantage of retaining strength at high temperatures, making them particularly useful for [[piston]]s.

[[File:RollsRoyceR(ScienceMuseum).JPG|thumb|A [[Rolls-Royce R]] engine]]

== Early adoption ==
The alloys were in limited use for aircraft by 1929, being used in the [[Rolls-Royce R]] engine that was successful in the [[Schneider Trophy]] seaplane races. They quickly spread to other manufacturers, in 1931 being adopted by [[ABC Motors|ABC]] for their [[ABC Hornet|Hornet]] engine.<ref name="Flight, 17 April 1931, ABC Hornet" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=ABC 'Hornet' Modified
  |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200359.html
  |format=PDF
  |date=17 April 1931
  |pages=335
  |ref=Flight, 17 April 1931, ABC Hornet
}}</ref> R.R.50 alloy was used for the crankcase, R.R.53 for the pistons.

Their first mass production use was in the [[Armstrong Siddeley|Armstrong Siddeley Special]] saloon car of 1933.<ref name="Camm, Hiduminium" /> Armstrong Siddeley already having had experience of the alloy, and financial investment in its manufacturer, from their aero engine business.

Advantages of these alloys were recognised worldwide. When 576 pistons in Hiduminium R.R.59 alloy were used for the Italian [[Italo Balbo|Marshal Balbo]]'s trans-Atlantic flight,<ref>A fleet of twenty-four [[Savoia-Marchetti S.55]] flying boats, each with two tandem V-12 engines, flew to the Chicago [[Century of Progress]] exposition.</ref> High Duty Alloys used it in their own advertising.<ref name="Flight, 14 September 1933, Balbo" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Another Triumph for Hiduminium 
  |url=http://www.flightglobal.com/pdfarchive/view/1933/1933%20-%200553.html
  |format=advert
  |date=14 September 1933
  |pages=
  |ref=Flight, 14 September 1933, Balbo
}}</ref>

== High Duty Alloys Ltd. ==
High Duty Alloys Ltd. was founded at Farnham Road, [[Slough]] in 1927,<ref name="Archiveweb, Cumbria, HDA" >{{cite web
  |url=http://www.archiveweb.cumbria.gov.uk/dserve/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqDb=Catalog&dsqCmd=Show.tcl&dsqSearch=(RefNo==%27YDB%2068%27)
  |title=High Duty Alloys Ltd, Distington
}}</ref> by [[Wallace Devereux|Colonel W. C. Devereux]].<ref >{{Cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Col. W. C. Devereux 
  |date=27 June 1952
  |pages=762–763
  |url=https://www.flightglobal.com/pdfarchive/view/1952/1952%20-%201762.html
}}</ref>

The company began from the ruins of the [[World War I]] aero engine builder, [[Peter Hooker|Peter Hooker Limited]] of [[Walthamstow]].<ref name="Banks, I Kept No Diary, 71" >{{cite book
  |title=I Kept No Diary
  |last=Banks  |first=Air Commodore F.R. (Rod) |authorlink=Francis Rodwell Banks
  |year=1978
  |publisher=Airlife
  |isbn=0-9504543-9-7
  |ref=Banks, I Kept No Diary
  |pages=71
}}</ref> Hookers licence-built the [[Gnome Monosoupape|Gnôme]] engine, amongst other things, and for the aero engines chose to be known as The British Gnôme and Le Rhône Engine Co.<ref name="Banks, I Kept No Diary, 63" >[[#Banks, I Kept No Diary|Banks, I Kept No Diary]], p.&nbsp;63</ref> They had become expert at working [[Y alloy]].<ref name="Camm, Y alloy" >{{cite book
  |author=FJ Camm  |authorlink=FJ Camm
  |chapter=Y alloy
  |title=Dictionary of Metals and Alloys
  |edition=3rd 
  |date=January 1944
  |pages=128
  |ref=Camm, Y alloy
}}</ref> The post-war reduction in demand, and the plentiful supply of war-surplus engines, made times hard for all engine and component makers. After buying it at the beginning of 1920 [[Birmingham Small Arms Company#Aviation|BSA]] reviewed its operations and decided Hooker's should be liquidated. After some years in [[voluntary liquidation]], Hooker's operations ended in late 1927 when its workshops were sold. 

About that time a large order was received, of some thousands of pistons for the [[Armstrong Siddeley Jaguar]] engine. [[Armstrong Siddeley]] had no other capable source for these pistons, so W.C. Devereux, works manager of Hooker, proposed to set up a new company to complete this order. [[John Siddeley, 1st Baron Kenilworth|John Siddeley]] loaned the money to re-purchase the necessary equipment and re-employ some of the staff from Hooker.<ref name="Banks, I Kept No Diary, 71" /> As the buildings had already been sold, the new company found premises in [[Slough]].

Demand from Rolls-Royce later led to expansion into a factory at [[Redditch]]. These materials were so crucial to aircraft production that with the outbreak of World War II a [[Factory#Shadow factories|shadow factory]] was established in the remote area of [[Cumberland]] (now [[Cumbria]]), at [[Distington]], near [[Whitehaven]].<ref name="Archiveweb, Cumbria, HDA" />

As well as producing ingots of raw alloy, manufacturing included the initial forging or casting processes. Finish machining would be undertaken by the customer. Hiduminium was so successful that during World War II it was in use by all of the major British aero engine makers.

In 1934 the [[Reynolds Technology|Reynolds Tube Co.]] began production of extruded structural components for airframes, using R.R.56 alloy supplied by High Duty Alloys. A new purpose-built plant was constructed at their works in [[Tyseley]], [[Birmingham]].<ref name="Flight, 11 October 1934, Reynolds" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Hiduminium for Aircraft
  |url=http://www.flightglobal.com/pdfarchive/view/1934/1934%20-%201068.html
  |format=PDF
  |date=11 October 1934
  |pages=1070
  |ref=Flight, 11 October 1934, Reynolds
}}</ref> In time, the post-war Reynolds company, already known for its steel [[bicycle frame]] tubes, would attempt to survive in the peacetime market by supplying Hiduminium alloy components for high-end aluminium [[crankset|bicycle crank]]s and [[bicycle brake|brake]]s.<ref>{{cite web
  |url=http://www.classiclightweights.co.uk/designs/hs-gbrakes.html
  |title=G B brakes (Gerry Burgess Cycle Components, 1948)
  |author=Hilary Stone
}}</ref>

The impeller (compressor) and compressor casing of the 1937 [[Power Jets WU]] [[jet engine]] was made from RR.56 and RR.55 respectively. In the subsequent [[Power Jets W.1]] the compressor material was changed to RR.59.<ref>http://www.imeche.org/docs/default-source/presidents-choice/jc12_1.pdf</ref> By 1943 the [[de Havilland Goblin]], the first British production jet engine to be built in large numbers, was in development. The [[centrifugal compressor]] for this began as a 500&nbsp;lb 'cheese' of RR.50, the largest forging made of it. After machining, these were reduced to 109&nbsp;lbs. The size of this forging was so great that cooling rates in its centre affected the metallurgical properties of the alloy; Devereux advised the reduction of the silicon content to below 0.25% and this low silicon RR.50 alloy was used throughout Goblin production.

The 1,600 [[Olympic torch|torch]]es for the 1948 [[1948 Summer Olympics|London Olympics]] were cast by the company.<ref name="Flight, 22 July 1948, 1948 Olympics" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=1948 Olympics
  |chapter=Here and There
  |url=http://www.flightglobal.com/pdfarchive/view/1948/1948%20-%201112.html
  |format=PDF
  |date=22 July 1948
  |pages=90
  |ref=Flight, 22 July 1948, Reynolds
}}</ref>

== Alloy composition ==
The [[Duralumin]] alloys had already demonstrated high-strength aluminium alloys. [[Y alloy]]'s virtue was its ability to maintain high strength at high temperatures. R.R alloys were developed by Hall & Bradbury at Rolls-Royce,<ref name="Murphy, J RAeS, 1966" /> partly to simplify the manufacture of components using them. A deliberate heat treatment process of multiple steps was used to control their physical properties.

In terms of composition, Y&nbsp;alloy typically contains 4%  of copper and 2% of nickel. R.R. alloys reduce each of these by half to 2% and 1%, and 1% of iron is introduced.

Example composition:
{|
|+
! R.R.56 <ref name="Camm, R.R. Alloys" />
|-
| Melting point ||635°C
|-
| Density || 2.75
|-
! colspan="2" align=center | Composition
|-
| [[Aluminium]] || 93.7%
|-
| [[Copper]] || 2.0%
|-
| [[Iron]] || 1.4% 
|-
| [[Nickel]] || 1.3%
|-
| [[Magnesium]] || 0.8%
|-
| [[Silicon]] || 0.7%
|-
| [[Titanium]] || 0.1%
|}

=== Heat treatment ===
As for many of the aluminium alloys, Y&nbsp;alloy [[age hardening|age hardens]] spontaneously at normal temperatures after [[quench|solution heat treating]]. In contrast, R.R. alloys remain soft afterwards, until deliberately heat treated again by [[precipitation hardening]] for artificial ageing.<ref name="Murphy, J RAeS, 1966" /> This simplifies their machining in the soft state, particularly where component blanks are made by a subcontractor and must be shipped to another site before machining. For R.R.56 the solution treatment is to quench from 530°C and ageing is carried out at 175°C.<ref name="Murphy, J RAeS, 1966" /> For R.R.50, the solution treatment may be omitted and the metal taken directly to precipitation hardening (155°C-170°C).<ref name="Higgins, Engineering Metallurgy, Y alloy" >{{cite book
  |last=Higgins  |first=Raymond A.
  |title=Part I: Applied Physical Metallurgy
  |work=Engineering Metallurgy
  |publisher=Hodder & Stoughton
  |edition=5th
  |year=1983
  |isbn=0-340-28524-9
  |ref=Higgins, Engineering Metallurgy
  |pages=435–438
}}</ref>

After solution treatment, the [[tensile strength]] of the alloy increases, but its [[Young's modulus]] decreases. The second stage of artificial aging increases the strength slightly, but also restores or improves the modulus.<ref name="Aircraft Engineer, 25 January 1934, Hiduminium R.R.53 B" >{{cite journal
  |journal=The Aircraft Engineer, (supplement to [[Flight (magazine)|Flight]]) 
  |title=Aircraft Engineer, 25 January 1934, Hiduminium R.R.53 B
  |url=http://www.flightglobal.com/pdfarchive/view/1934/1934%20-%201396.html
  |format=PDF
  |date=25 January 1934
  |pages=8
  |ref=Aircraft Engineer, 25 January 1934, Hiduminium R.R.53 B
}}</ref>

{|
|+
! R.R.53 B, chill cast <ref name="Aircraft Engineer, 25 January 1934, Hiduminium R.R.53 B" />
|-
! !! Maximum [[Stress (mechanics)|Stress]] <br>Tons/sq in. !! [[Strain (mechanics)|Strain]] <br>(elongation)
|-
| As cast || 14 || 3%
|-
| Solution treated || 22 || 6%
|-
| Solution treated <br>and artificially aged || 26 || 3%
|}

{|
|-
! colspan="2" align=center | Composition, R.R.53 B <ref name="Aircraft Engineer, 25 January 1934, Hiduminium R.R.53 B" />
|-
| Aluminium || 92.8%
|-
| Copper || 2.5%
|-
| Nickel || 1.5%
|-
| Iron || 1.2% 
|-
| Silicon || 1.2%
|-
| Magnesium || 0.8%
|}

== Alloy range ==
A range of alloys were produced in the R.R.50 range.<ref name="Flight, 22 January 1932, Hiduminium RR alloys" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Hiduminium R.R. alloys
  |url=http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200084.html
  |format=PDF
  |date=22 January 1932
  |pages=84
  |ref=Flight, 22 January 1932, Hiduminium RR alloys
}}</ref> These could be worked by casting or forging, but they were not intended for rolling as sheet or general machining from [[bar stock]].
{|
|-
| R.R. 50 || General-purpose [[sand casting]] alloy
|-
| R.R. 53 || [[Die casting|Die-cast]] piston alloy || Additional silicon content, to improve flow when machine casting
|-
| R.R. 56 || General-purpose [[forging]] alloy
|-
| R.R. 58 || Low-creep forging alloy for rotating impellers and compressors<ref>{{cite journal
  |title=Cooling air impeller forged in R.R. 58
  |journal=[[Flight (magazine)|''Flight'']]
  |page=16
  |date=1 January 1954
  |url=http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%200030.html
}}</ref>
|-
| R.R. 59 || Forged piston alloy
|}

The number of alloys expanded to support a range of applications and processing techniques. At the [[Paris Airshow]] of 1953, High Duty Alloys showed no less than eight different Hiduminium R.R. alloys: 20, 50, 56, 58, 66, 77, 80, 90.<ref name="Flight, 26 June 1953, Paris Airshow" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Britain at the Paris Airshow
  |url=http://www.flightglobal.com/pdfarchive/view/1953/1953%20-%200814.html
  |format=PDF
  |date=26 June 1953
  |pages=808
  |ref=Flight, 26 June 1953, Paris Airshow
}}</ref> Also shown were [[gas turbine]] compressor and [[turbine blade]]s in Hiduminium, and a range of their products in the [[Magnuminium]] alloy series.

R.R.58, also Aluminium 2618, comprising  2.5 copper, 1.5 magnesium, 1.0 iron, 1.2 nickel, 0.2 silicon, 0.1 titanium and the remainder aluminium, and originally intended for [[jet engine]] compressor blades, was used as the main structural material for the [[Concorde]] airframe, supplied by High Duty Alloys, it was also known as AU2GN to the French side of the project.<ref>http://heritageconcorde.com/?page_id=469</ref>

Later alloys, such as R.R.66, were used for sheet, where high strength was needed in an alloy capable of being worked  by [[deep drawing]].<ref name="Flight, 13 March 1959, Hiduminium R.R.66 Comet advert" >{{cite journal
  |journal=[[Flight (magazine)|Flight]] 
  |title=Hiduminium R.R.66 advert featuring DH Comet
  |url=http://www.flightglobal.com/pdfarchive/view/1959/1959%20-%200706.html
  |format=advert
  |date=13 March 1959
  |pages=
  |ref=Flight, 13 March 1959, Hiduminium R.R.66 Comet advert
}}</ref> This became increasingly important with the faster jet aircraft post-war, as issues such as transonic [[compressibility#Aeronautical dynamics|compressibility]] became important. It was now necessary for an aircraft's covering material to be strong, not merely the spar or framing beneath.

R.R.350, a sand-castable high temperature alloy, was used in the [[General Electric YJ93]] jet engine and was also used in the [[General Electric GE4]] intended for the later cancelled American [[Boeing 2707]] [[Supersonic transport|SST]] project.<ref>{{Cite web
  |title=Elevated Temperature Mechanical Properties of Two Cast Aluminum Alloys
  |last=Gunderson  |first=Allen W.
  |date=February 1969
  |id=AFML-TR-69-100
  |publisher=Air Force Materials Laboratory, [[Wright-Patterson AFB]]
  |url=https://docs.google.com/viewer?a=v&q=cache:xbW4lCRwD5YJ:www.dtic.mil/cgi-bin/GetTRDoc%3FLocation%3DU2%26doc%3DGetTRDoc.pdf%26AD%3DAD0860080+hiduminium&hl=en&gl=uk&pid=bl&srcid=ADGEEShjCYQN6TpRSAk3pzxsL5ryL7hYv0b6a5aBUoc6rInzHIz6fsst48eca3ZhIy6BJ2UH9ZRnXA7dgR_JVLtFaS7a9bpbwhSvZeyuLlPCLa9WSvHRRfVhUw_rM2JOs66kevfJ6Um9&sig=AHIEtbRvyW2JxqEU_JOUTp7ymGr2hHA9xQ
}}</ref>

== References ==
{{Reflist|colwidth=35em}}

==External links==
* [http://www.aviationancestry.com/Materials/Metals/Metals-HighDutyAlloys-1936-1.html A 1936 advertisement for High Duty Alloys Ltd ''RR 56'' alloy]
* [http://www.aviationancestry.com/Materials/Metals/Metals-Hiduminium-1941-2.html A 1941 advertisement for Hiduminium alloy]
* [http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%200983.html "The 10,000,000th blade produced in Hiduminium"] - a 1954 advertisement for Hiduminium as a [[turbine blade]] material

{{Aluminium alloys}}

[[Category:Aluminium alloys]]
[[Category:Nickel–aluminium alloys]]
[[Category:Aerospace materials]]
[[Category:Named alloys]]