{{Use mdy dates|date=July 2016}}
'''Lindsay Carter Warren''' (December 16, 1889 – December 28, 1976) was a [[United States Democratic Party|Democratic]] [[United States House of Representatives|U.S. Congressman]] from [[North Carolina]] between 1925 and 1940.

==Early life and education==
Born in [[Washington, North Carolina]], Warren was the son of prominent lawyer Charles F. Warren and the grandson of politician [[Edward J. Warren]].<ref>[http://www.ncbar.org/about/history/whyBoth.aspx About the NC Bar Association]</ref> Warren studied at [[Bingham School]] in [[Asheville, North Carolina|Asheville]] from 1903 to 1906. He attended the [[University of North Carolina at Chapel Hill]] from 1906 to 1908 and then from 1911 to 1912 (the second time studying law). He was admitted to the bar in 1912 and practiced law in his hometown of Washington.

==Political career==
Also in 1912, Warren was named county attorney for [[Beaufort County, North Carolina]], and elected the chairman of the executive committee for the county [[United States Democratic Party|Democratic Party]]; he would hold both posts until 1925. He was elected to the [[North Carolina Senate]] in 1917 and 1919, serving as [[President Pro Tempore of the North Carolina Senate|Senate president pro tem]] in 1919 and 1920, and as the chair of the special legislative commission on [[workmen's compensation]] acts. In 1920, Warren succeeded in preventing a Senate vote on ratification of the [[Nineteenth Amendment to the United States Constitution|19th Amendment]], which would guarantee [[women's suffrage]] (nevertheless, [[Tennessee]] ratified the amendment the next day, making the amendment effective throughout the country).<ref>[http://www.carolinawoman.com/flashback8-08.htm "Why N.C. didn't give women the vote"], reprinted in ''Carolina Woman'', August 2008</ref>

In 1923, Warren was sent to the [[North Carolina House of Representatives]] for a single term before being elected, in 1924, to the [[69th United States Congress]]. He was re-elected seven times, serving in the [[U.S. House of Representatives]] from March 4, 1925 until November 1, 1940, when he resigned from Congress. During the [[72nd United States Congress|72nd]] through [[76th United States Congress|76th]] Congresses, he was chairman of the [[United States House Committee on Accounts|Committee on Accounts]]. He was also a delegate to the [[Democratic National Convention]]s in 1932 and 1940, and chaired the North Carolina Democratic Convention in 1930 and 1934.

==Comptroller General==
Warren left Congress to accept the post of [[Comptroller General of the United States]], serving in that role for almost fourteen years, until May 1, 1954. President [[Franklin Roosevelt]] had offered Warren the post in 1936 and in 1938 but he had declined it. As Comptroller General, Warren led the [[General Accounting Office]] through [[World War II]] and worked with the [[Truman Committee]] to outlaw kickbacks by subcontractors to defense contractors. He oversaw an increase in the agency's workforce and extensively reorganized GAO.<ref>[http://archive.gao.gov/t2pbat7/145379.pdf GAO History: 1921-1991 by Roger R. Trask, GAO-OP-3-HP, Nov. 1991]</ref>

==Later life and legacy==
Warren returned to the North Carolina House of Representatives for two additional terms in 1959 and 1961, and died in 1976 in his hometown of Washington, North Carolina. His son, Lindsay, Jr., followed his father into law and into the North Carolina legislature.<ref>[http://www.ncspin.com/ncnotables.2006.03.09.php NC Spin: NC Notable Lindsay Warren, Jr.]</ref>

A 2.8-mile bridge, one of the longest in North Carolina, was built in 1960 over the [[Alligator River National Wildlife Refuge|Alligator River]] and is named in honor of Warren.<ref>[http://marinas.com/view/bridge/331 Lindsay C. Warren Bridge]</ref>

The M/V Lindsay Warren, a 25 car ferry, was also named for him. The 112 vessel was built in 1970 for the [[North Carolina Department of Transportation Ferry Division]] to cross [[Hatteras Inlet]] between [[Hatteras, North Carolina|Hatteras]] and [[Ocracoke, North Carolina|Ocracoke]] Islands on the outer banks of North Carolina. The M/V Lindsay Warren was taken out of service and sold by NCDOT.  Now renamed Marissa Mae Nicole it operates on [[Mobile Bay]].<ref>http://www.shipbuildinghistory.com/history/shipyards/5small/inactive/newbern.htm</ref>

==References==
{{reflist}}
{{CongBio|W000166}}
*[http://www.time.com/time/magazine/article/0,9171,764319,00.html "Watchdog," ''Time'' magazine, from 1940]

{{s-start}}
{{s-par|us-hs}}
{{USRepSuccessionBox |
  state=North Carolina|
  district=1 |
  before=[[Hallett Sydney Ward]] |
  after=[[Herbert C. Bonner]] | 
  years=1925–1940
}}
{{s-end}}

{{Comptroller General of the United States}}

{{Authority control}}

{{DEFAULTSORT:Warren, Lindsay Carter}}
[[Category:1889 births]]
[[Category:1976 deaths]]
[[Category:Members of the North Carolina House of Representatives]]
[[Category:Members of the United States House of Representatives from North Carolina]]
[[Category:North Carolina State Senators]]
[[Category:Comptrollers General of the United States]]
[[Category:North Carolina Democrats]]
[[Category:Democratic Party members of the United States House of Representatives]]
[[Category:20th-century American politicians]]