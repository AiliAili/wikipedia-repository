__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=HP.34 Hare
 |image=HPHare.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=Two-seat day bomber
 |national origin=United Kingdom
 |manufacturer=[[Handley Page]]
 |designer=
 |first flight=[[1928 in aviation|1928]]
 |introduced=
 |retired=[[1937 in aviation|1937]]
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=1
 |variants with their own articles=
}}
|}

The '''Handley Page HP.34 Hare''' was a [[United Kingdom|British]] two-seat high-altitude day [[bomber]] designed and built at Cricklewood by [[Handley Page]]. It was designed by Harold Boultbee to meet the requirements of [[List Of Air Ministry Specifications#1920-1929|Air Ministry Specification 23/25]] for a replacement for the [[Hawker Horsley]] in the day bomber role,<ref name="Mason Bomber p195-196">Mason 1994, p.195-196.</ref> competing against the [[Blackburn Beagle]], [[Hawker Harrier]], [[Gloster Goring]] and [[Westland Witch]]. The Hare was a conventional [[biplane]], with single-bay unequal-span [[Stagger (aviation)|staggered]] wings, of mixed wood and metal construction (although the specification required that any production aircraft be of all-metal construction). It had a crew of two with the pilot in an open [[cockpit]] aft of the wing with a gunner/bomb aimer behind him.

Only one aircraft was built, with the [[United Kingdom military aircraft serials|serial ''J8622'']]. It was first flown on 24 February 1928, powered by a [[Bristol Jupiter|Gnome-Rhône Jupiter]] as the planned Jupiter VIII was unavailable.<ref name="Barnes p.269">Barnes 1976, p.269.</ref> Testing showed that the aircraft had poor handling and was prone to vibration, and it was modified with a 2&nbsp;ft (0.61&nbsp;m) longer [[fuselage]] and a revised tail, which improved handling. It was decided to modify the aircraft so that it could meet the requirements of Specification 24/25 to replace the Horsely in its other role as a shore-based [[torpedo bomber]].<ref name="Barnes p.270">Barnes 1976, p.270.</ref>

The Hare was unsuccessful in meeting both competitions, with the day bomber competition being abandoned in favour of purchasing the more advanced [[Hawker Hart]] built to Specification 12/36, while the [[torpedo bomber]] requirement was met by the [[Vickers Vildebeest]].<ref name="Mason Bomber p196">Mason 1994, p.196.</ref> It remained in use with the [[Royal Air Force]] as a trials aircraft until 1932. It was then sold for a proposed long-distance flight by J.N. Addinsell and registered ''G-ACEL''. The Hare was flown to [[London Air Park|London Air Park, Hanworth]] in 1933 where it was redoped and painted in civilian colours. It never flew again and was scrapped in 1937.<ref name="Jackson civil v3 p343">Jackson 1973, p.343.</ref>

==Specifications==
{{aerospecs
|ref=Handley Page Aircraft since 1907 <ref name="Barnes p275">Barnes 1976, p.275.</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng
|crew=two
|capacity=
|length m=9.81
|length ft=32
|length in=2
|span m=15.24
|span ft=50
|span in=0
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=42.2
|wing area sqft=454
|empty weight kg=1,383
|empty weight lb=3,050
|gross weight kg=3,285
|gross weight lb=7,243
|eng1 number=1
|eng1 type=[[Bristol Jupiter]] VIII
|eng1 kw=362
|eng1 hp=485
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|max speed kmh=243
|max speed mph=152
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=1,609
|range miles=1,000
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=6,098
|ceiling ft=20,000
|climb rate ms=
|climb rate ftmin=
|armament1=Fixed forward firing machine gun
|armament2=Movable machine gun at rear cockpit
|armament3=Bombs or 2,000 lb (907 kg) torpedo
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[List of aircraft of the RAF]]
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* {{cite book |title= Handley Page Aircraft since 1907|last= Barnes|first= C.H.|authorlink= |coauthors= |year= 1976|publisher= Putnam|location=London |isbn=0-370-00030-7 |pages= }} 
* {{cite book |title= The British Bomber since 1914|last=Mason |first=Francis K. |authorlink= |coauthors= |year=1994 |publisher=Putnam |location=London |isbn=0-85177-861-5 |pages= }} 
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
*{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 2|year= 1974|publisher= Putnam|location= London|pages=382|isbn=0-370-10010-7 }}
{{refend}}

==External links==
{{commons category|Handley Page Hare}}
* [http://www.britishaircraft.co.uk/aircraftpage.php?ID=226 British Aircraft Directory]
{{Handley Page aircraft}}

[[Category:British bomber aircraft 1920–1929]]
[[Category:Handley Page aircraft|Hare]]