{{Infobox football club season
 | club                = [[Cheltenham Town F.C.|Cheltenham Town]]
 | season              = 2012–13
 |nextseason           = [[2013–14 Cheltenham Town F.C. season|2013-14]]
 |prevseason           = [[2011-12 Cheltenham Town F.C. season|2011-12]]
 | manager             = [[Mark Yates (footballer)|Mark Yates]]
 | chairman            = Paul Baker
 | team captain        = 
 | stadium             = [[Whaddon Road]]
 | league              = [[2012-13 Football League Two|League Two]]
 | league result       =
 | cup1                = [[2012–13 FA Cup|FA Cup]]
 | cup1 result         = Round 3 <small>''(lost 1-5 to [[Everton F.C.|Everton]])''</small>
 | cup2                = [[2012–13 Football League Cup|League Cup]]
 | cup2 result         = Round 1 <small>''(lost 3-5 on penalties after a 1-1 draw with [[Milton Keynes Dons F.C.|Milton Keynes Dons]])''</small>
 | cup3                = [[2012–13 Football League Trophy|Football League Trophy]]
 | cup3 result         = Round 2 <small>''(lost 2-4 to [[Oxford United F.C.|Oxford United]])''</small>
 | league topscorer    =
 | season topscorer    =
 | highest attendance  =
 | lowest attendance   =
 | average attendance  =
}}
The '''2012-13 season''' was 126th season of '''Cheltenham Town's''' existence, and their 13th in the [[Football League]] since promotion from the [[Football Conference|Conference National]] in 2000.

Cheltenham's first game of the 2012-13 season was at home to [[Milton Keynes Dons F.C.|Milton Keynes Dons]] in the [[Football League Cup|League Cup]], before then hosting [[Dagenham and Redbridge F.C.|Dagenham and Redbridge]] in their first [[Football League Two|League Two]] match of the season on 18 August 2012.

Cheltenham were in contention for automatic promotion until the final day of the season, but ultimately finished 5th and had to settle for the playoffs, in which they lost 2-0 on aggregate to [[Northampton Town F.C.|Northampton Town]].

==First team==
''As of 30 March 2013.''
{| class="wikitable" style="text-align: center; font-size:90%" width=80%
|-
!align=right| No.
!align=right| Name
!align=right| Nationality
!align=right| Age
!align=right| Club apps.
!align=right| Club goals
!align=right| Previous Club
!align=right| Notes
|-
!colspan=11 style="background: #DCDCDC" align=right| Goalkeepers
|- 
| 1
| [[Scott Brown (footballer born April 1985)|Scott Brown]] 
| {{flagicon|ENG}}
| {{Age|1985|4|26}}
| 158
| 0
| [[Bristol City F.C.|Bristol City]]
|
|-
| 12
| Connor Roberts
| {{flagicon|WAL}}
| {{Age|1992|12|8}}
| 0
| 0
| [[Everton F.C.|Everton]]
|
|-
!colspan=11 style="background: #DCDCDC" align=right| Defenders
|-
| 2
| [[Keith Lowe (footballer)|Keith Lowe]]
| {{flagicon|ENG}}
| {{Age|1985|9|13}}
| 67
| 2
| [[Hereford United F.C.|Hereford United]]
|
|-
| 3
| [[Billy Jones (footballer born 1983)|Billy Jones]]
| {{flagicon|ENG}}
| {{Age|1983|6|26}}
| 0
| 0
| [[Exeter City F.C.|Exeter City]]
| 
|-
| 5
| [[Harry Hooman]]
| {{flagicon|ENG}}
| {{Age|1991|4|27}}
| 2
| 0
| [[Shrewsbury Town F.C.|Shrewsbury Town]]
| 
|- 
| 6
| [[Steve Elliott (footballer born 1978)|Steve Elliott]]
| {{flagicon|ENG}}
| {{Age|1978|10|29}}
| 79
| 3
| [[Bristol Rovers F.C.|Bristol Rovers]]
|
|-
| 22
| [[Sido Jombati]]
| {{flagicon|POR}}
| {{Age|1987|8|20}}
| 36
| 3
| [[Bath City F.C.|Bath City]]
| 
|- 
| 27
| [[Michael Hector]]
| {{flagicon|ENG}}
| {{Age|1992|7|19}}
| 13
| 0
| [[Reading F.C.|Reading]]
|
|- 
| 
| [[Jack Deaman]]
| {{flagicon|ENG}}
| 
| 0
| 0
| [[Birmingham City F.C.|Birmingham City]]
|
|- 
!colspan=11 style="background: #DCDCDC" align=right| Midfielders
|- 
| 4
| [[Darren Carter]]
| {{flagicon|ENG}}
| {{Age|1983|12|18}}
| 32
| 6
| [[Preston North End F.C.|Preston North End]]
| 
|-
| 7
| [[Marlon Pack]]
| {{flagicon|ENG}}
| {{Age|1991|3|25}}
| 81
| 7
| [[Portsmouth F.C.|Portsmouth]]
| 
|-
| 8
| [[Sam Deering]]
| {{flagicon|ENG}}
| {{Age|1991|2|26}}
| 0
| 0
| [[Barnet F.C.|Barnet]]
| 
|-
| 11
| [[Jermaine McGlashan]]
| {{flagicon|ENG}}
| {{Age|1988|4|14}}
| 16
| 2
| [[Aldershot Town F.C.|Aldershot Town]]
| 
|-
| 16
| [[Russell Penn]]
| {{flagicon|ENG}}
| {{Age|1985|11|8}}
| 43
| 1
| [[Burton Albion F.C.|Burton Albion]]
| 
|-
| 21
| [[Bagasan Graham]]
| {{flagicon|ENG}}
| {{Age|1992|10|6}}
| 7
| 0
| [[Queens Park Rangers F.C.|Queens Park Rangers]]
|
|-
| 25
| [[Jason Taylor (English footballer)|Jason Taylor]]
| {{flagicon|ENG}}
| {{Age|1987|1|28}}
| 12
| 0
| [[Rotherham United F.C.|Rotherham United]]
| 
|-
!colspan=11 style="background: #DCDCDC" align=right| Forwards
|- 
| 9
| [[Darryl Duffy]]
| {{flagicon|SCO}}
| {{Age|1984|4|16}}
| 41
| 11
| [[Bristol Rovers F.C.|Bristol Rovers]]
| 
|-
| 14
| [[Shaun Harrad]]
| {{flagicon|ENG}}
| {{Age|1984|12|11}}
| 0
| 0
| [[Bury F.C.|Bury]]
| 
|-
| 19
| [[Paul Benson]]
| {{flagicon|ENG}}
| {{Age|1979|10|12}}
| 13
| 4
| [[Swindon Town F.C.|Swindon Town]]
| 
|-
| 23
| [[Kaid Mohamed]]
| {{flagicon|WAL}}
| {{Age|1984|7|23}}
| 45
| 11
| [[AFC Wimbledon]]
|
|-
| 29
| [[Byron Harrison (footballer)|Byron Harrison]]
| {{flagicon|ENG}}
| {{Age|1987|6|15}}
| 12
| 1
| [[A.F.C. Wimbledon|Wimbledon]]
| 
|}

===Transfers===

====In====
{| class="wikitable" style="text-align: center; font-size:90%" width=60%
!'''Date '''
!'''Player '''
!'''Position '''
!'''Transferred from'''
!'''Fee '''
!'''Ref '''
|-
| 5 July 2012
| {{flagicon|ENG}} [[Sam Deering]]
| MF
| {{flagicon|ENG}} [[Barnet F.C.|Barnet]]
| Free
| 
|-
| 11 July 2012
| {{flagicon|ENG}} [[Billy Jones (footballer born 1983)|Billy Jones]]
| DF
| {{flagicon|ENG}} [[Exeter City F.C.|Exeter City]]
| Free
| <ref>{{Cite web |url=http://www.ctfc.com/news/article/jones-wants-success-229474.aspx |title=Jones Wants Success |work=Cheltenham Town FC |date=11 July 2012}}</ref>
|-
| 2 August 2012
| {{flagicon|ENG}} [[Chris Zebroski]]
| FW
| {{flagicon|ENG}} [[Bristol Rovers F.C.|Bristol Rovers]]
| Undisclosed
| <ref>{{Cite web |url=http://www.ctfc.com/news/article/zebroski-signs-286770.aspx |title=Zebroski Signs |work=Cheltenham Town FC |date=2 August 2012}}</ref>
|-
| 3 August 2012
| {{flagicon|ENG}} [[Shaun Harrad]]
| FW
| {{flagicon|ENG}} [[Bury F.C.|Bury]]
| Season-Long Loan
| <ref>{{Cite web |url=http://www.ctfc.com/news/article/harrad-joins-robins-290456.aspx |title=Harrad Joins Robins |work=Cheltenham Town FC |date=3 August 2012}}</ref>
|-
| 9 August 2012
| {{flagicon|WAL}} Connor Roberts
| GK
| {{flagicon|ENG}} [[Everton F.C.|Everton]]
| Free
| 
|-
| 23 August 2012
| {{flagicon|ENG}} [[Darren Carter]]
| MF
| {{flagicon|ENG}} [[Preston North End F.C.|Preston North End]]
| Free, six-month contract
| 
|-
| 25 October 2012
| {{flagicon|ENG}} [[Lawson D'Ath]]
| MF
| {{flagicon|ENG}} [[Reading F.C.|Reading]]
| One month loan
| 
|-
| 25 October 2012
| {{flagicon|ENG}} [[Jake Taylor (footballer)|Jake Taylor]]
| MF
| {{flagicon|ENG}} [[Reading F.C.|Reading]]
| One month loan
|
|-
| 25 January 2013
| {{flagicon|NIR}} Luke McCullough
| DF
| {{flagicon|ENG}} [[Manchester United F.C.|Manchester United]]
| Loan, recalled 6 March 2013
| 
|-
| 28 January 2013
| {{flagicon|ENG}} [[Jason Taylor (English footballer)|Jason Taylor]]
| MF
| {{flagicon|ENG}} [[Rotherham United F.C.|Rotherham United]]
| Free
| 
|-
| 29 January 2013
| {{flagicon|ENG}} [[Paul Benson]]
| FW
| {{flagicon|ENG}} [[Swindon Town F.C.|Swindon Town]]
| Loan until end of season
| 
|-
| 30 January 2013
| {{flagicon|ENG}} [[Michael Hector]]
| DF
| {{flagicon|ENG}} [[Reading F.C.|Reading]]
| Loan until end of season
| 
|-
| 31 January 2013
| {{flagicon|ENG}} [[Byron Harrison (footballer)|Byron Harrison]]
| FW
| {{flagicon|ENG}} [[A.F.C. Wimbledon|Wimbledon]]
| Undisclosed fee
| 
|-
| 28 March 2013
| {{flagicon|ENG}} [[Jack Deaman]]
| DF
| {{flagicon|ENG}} [[Birmingham City F.C.|Birmingham City]]
| Loan until end of season
| 
|-
|}

====Out====
{| class="wikitable" style="text-align: center; font-size:90%" width=60%
!'''Date '''
!'''Player '''
!'''Position '''
!'''Transferred to'''
!'''Fee '''
|- 
| 3 July 2012
| [[Luke Summerfield]] 
| MF
| [[Shrewsbury Town F.C.|Shrewsbury Town]]
| Free
|- 
| 
| [[Brian Smikle]]
| MF
| [[Hereford United F.C.|Hereford United]]
| Free
|-
| 13 September 2012
| [[Danny Andrew]]
| DF
| [[Cambridge United F.C.|Cambridge United]]
| One month loan
|-
| 13 December 2012
| [[Danny Andrew]]
| DF
| [[Gloucester City A.F.C.|Gloucester City]]
| One month loan
|-
| 13 December 2012
| [[Bagasan Graham]]
| FW
| [[Gloucester City A.F.C.|Gloucester City]]
| One month loan
|-
| 4 January 2013
| [[Chris Zebroski]]
| FW
| [[Eastleigh F.C.|Eastleigh]]
| Free
|-
| 18 January 2013
| [[Danny Andrew]]
| DF
| [[Gloucester City A.F.C.|Gloucester City]]
| Free
|-
| 31 January 2013
| [[Alan Bennett (footballer born 1981)|Alan Bennett]]
| DF
| [[A.F.C. Wimbledon|Wimbledon]]
| Free
|-
| 31 January 2013
| [[Jeff Goulding]]
| FW
| [[Aldershot Town F.C.|Aldershot Town]]
| Free
|-
|}

<!--==Squad Statstics==
===Appearances, goals and cards===
:''As of 13 May 2012
{| class="wikitable" style="text-align:center; font-size:95%;"
|-
!rowspan="2" valign="bottom"|No.
!rowspan="2" valign="bottom"|Pos.
!rowspan="2"|Name
!colspan="2" width="110"|League
!colspan="2" width="110"|FA Cup
!colspan="2" width="110"|League Cup
!colspan="2" width="110"|League Trophy
!colspan="2" width="110"|Total
!colspan="2" width="110"|Discipline
|-
!Apps
!Goals
!Apps
!Goals
!Apps
!Goals
!Apps
!Goals
!Apps
!Goals
![[Image:Yellow card.svg|13px]]
![[Image:Red card.svg|13px]]
|-
|align="left"|−||align="left"|GK||align="left"|{{flagicon|ENG}} [[Scott Brown]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|DF||align="left"|{{flagicon|NIR}} [[Keith Lowe (footballer)|Keith Lowe]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|DF||align="left"|{{flagicon|ENG}} [[Steve Elliott(footballer born 1978)|Steve Elliott]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|MF||align="left"|{{flagicon|ENG}} [[Marlon Pack]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|MF||align="left"|{{flagicon|SCO}} [[Darryl Duffy]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|FW||align="left"|{{flagicon|ENG}} [[Jeff Goulding]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|MF||align="left"|{{flagicon|IRE}} [[Alan Bennett (Irish footballer|Alan Bennett]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|DF||align="left"|{{flagicon|ENG}} [[Russell Penn]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|MF||align="left"|{{flagicon|ENG}} [[Jermaine McGlashan]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|DF||align="left"|{{flagicon|ENG}} [[Bagasan Graham]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|FW||align="left"|{{flagicon|POR}} [[Sido Jombati]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|FW||align="left"|{{flagicon|WAL}} [[Kaid Mohamed]]
|0||0||0||0||1||1||0||0||0||0||0||0
|-
|align="left"|−||align="left"|GK||align="left"|{{flagicon|ENG}} [[Harry Hooman]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|MF||align="left"|{{flagicon|ENG}} [[Luke Summerfield]]
|0||0||0||0||0||0||0||0||0||0||0||0
|-
|align="left"|−||align="left"|FW||align="left"|{{flagicon|ENG}} [[Steve Book]]
|0||0||0||0||0||0||0||0||0||0||0||0
|}

===Top Scorers===
{| class="wikitable" style="font-size: 95%; text-align: center;"
|-
!width=150|Name
!width=80|League
!width=80|FA Cup
!width=80|League Cup
!width=80|League Trophy
!width=80|'''Total''
|-
|Player Name
|League Goals
|FA Cup Goals
|League Cup Goals
|League Trophy Goals
|Total
|}

<small>Updated to games played on 1 July 2012.<br>
Source: [http://www.soccerbase.com/teams/team.sd?team_id=579&teamTabs=stats Soccerbase]

'''Apps''' = Appearances made; '''Goals''' = Goals scored.
</small>-->

==Competitions==

===Overall===
{{fb overall competition header |curr=y }}
{{fb overall2 competition |bg= |curr=y |s=2012–13 |c=Football League Two |cprn= 5th |fprn= |dfm=18 August 2012 |dlm=27 April 2013 }}
{{fb overall2 competition |bg=y |curr=y |s=2012-13 |c=FL Cup |sr=R1 |cpr=R1 |fpr=R1 |dfm=11 August 2012  |dlm=11 August 2012 }}
{{fb overall2 competition |bg= |curr=y |s=2012–13 |c=FL Trophy |sr=R2 |cpr= |fpr= |dfm=  |dlm= }}
{{fb overall2 competition |bg=y |curr=y |s=2012-13 |c=FA Cup |sr=R1 |cpr= |fpr= |dfm=  |dlm= }}
{{fb overall competition footer |u=19 June 2012}}

=== League Two ===

==== Standings ====
{{2012–13 Football League Two table|showteam=CHL}}

==== Results summary ====
{{Fb_rs |hw=14 |hd=7 |hl=2 |hgf=34 |hga=16 |aw=6 |ad=8 |al=9 |agf=24 |aga=35}}
{{Fb_rs_footer |u=27 April 2013 |s=[http://www.ctfc.com/page/Fixtures/0,,10434,00.html] |date=June 2012}}

==== Results by round ====
{{Fb_rbr_header |nr=46}}
{{Fb_rbr_ground|m1=H|m2=A|m3=A|m4=H|m5=A|m6=H|m7=H|m8=A|m9=H|m10=A|m11=H|m12=A|m13=A|m14=H|m15=H|m16=A|m17=H|m18=A|m19=A|m20=H|m21=A|m22=H|m23=A|m24=H|m25=A|m26=H|m27=A|m28=H|m29=H|m30=H|m31=A|m32=A|m33=H|m34=A|m35=A|m36=H|m37=H|m38=A|m39=H|m40=A|m41=A|m42=H|m43=A|m44=H|m45=A|m46=H}}
{{Fb_rbr_result|m1=W|m2=D|m3=W|m4=L|m5=D|m6=L|m7=W|m8=D|m9=W|m10=W|m11=D|m12=W|m13=L|m14=W|m15=W|m16=D|m17=W|m18=L|m19=L|m20=W|m21=W|m22=D|m23=L|m24=W|m25=L|m26=D|m27=D|m28=D|m29=W|m30=D|m31=L|m32=W|m33=D|m34=D|m35=D|m36=W|m37=W|m38=L|m39=W|m40=D|m41=L|m42=W|m43=L|m44=W|m45=W|m46=D}}
{{Fb_rbr_position|r=23|m1=7|m2=5|m3=3|m4=10|m5=9|m6=15|m7=9|m8=11|m9=5|m10=5|m11=|m12=3|m13=4|m14=4|m15=3|m16=3|m17=3|m18=3|m19=3|m20=3|m21=3|m22=3|m23=3|m24=3|m25=4|m26=5|m27=4|m28=5|m29=3|m30=3|m31=5|m32=3|m33=4|m34=6|m35=7|m36=6|m37=3|m38=5|m39=4|m40=5|m41=5|m42=4|m43=4|m44=4|m45=4|m46=5}}
{{Fb_rbr_footer |u=27 April 2013 |s=[]|date=March 2013}}

=== Scores Overview ===
<small>Cheltenham Town score given first.</small>
{| class="wikitable"
|-
!width=40%|Opposition
!width=20%|Home Score
!width=20%|Away Score
|-
| style="text-align:center;"|[[Accrington Stanley F.C.|Accrington Stanley]]
| style="background:#fdd; text-align:center;"| 0-3
| style="background:#ffc; text-align:center;"| 2-2
|-
| style="text-align:center;"|[[AFC Wimbledon|Wimbledon]]
| style="background:#cfc; text-align:center;"| 2-1
| style="background:#cfc; text-align:center;"| 2-1
|-
| style="text-align:center;"|[[Aldershot Town F.C.|Aldershot Town]]
| style="background:#ffc; text-align:center;"| 1-1
| style="background:#cfc; text-align:center;"| 1-0
|-
| style="text-align:center;"|[[Barnet F.C.|Barnet]]
| style="background:#cfc; text-align:center;"| 1-0 
| style="background:#ffc; text-align:center;"| 0-0
|-
| style="text-align:center;"|[[Bradford City A.F.C.|Bradford City]]
| style="background:#fFC; text-align:center;"| 0-0
| style="background:#fdd; text-align:center;"| 1-3
|-
| style="text-align:center;"|[[Bristol Rovers F.C.|Bristol Rovers]]
| style="background:#ffc; text-align:center;"| 1-1
| style="background:#cfc; text-align:center;"| 1-0
|-
| style="text-align:center;"|[[Burton Albion F.C.|Burton Albion]]
| style="background:#cfc; text-align:center;"| 1-0 
| style="background:#fdd; text-align:center;"| 1-3
|-
| style="text-align:center;"|[[Chesterfield F.C.|Chesterfield]]
| style="background:#cfc; text-align:center;"| 1-0
| style="background:#fdd; text-align:center;"| 1-4
|-
| style="text-align:center;"|[[Dagenham & Redbridge F.C.|Dagenham & Redbridge]]
| style="background:#cfc; text-align:center;"| 2-0
| style="background:#fdd; text-align:center;"| 0-1
|-
| style="text-align:center;"|[[Exeter City F.C.|Exeter City]]
| style="background:#cfc; text-align:center;"| 3-0
| style="background:#cfc; text-align:center;"| 1-0
|-
| style="text-align:center;"|[[Fleetwood Town F.C.|Fleetwood Town]]
| style="background:#ffc; text-align:center;"| 2-2 
| style="background:#ffc; text-align:center;"| 1-1
|-
| style="text-align:center;"|[[Gillingham F.C.|Gillingham]]
| style="background:#cfc; text-align:center;"| 1-0
| style="background:#ffc; text-align:center;"| 1-1
|-
| style="text-align:center;"|[[Morecambe F.C.|Morecambe]]
| style="background:#cfc; text-align:center;"| 2-0
| style="background:#ffc; text-align:center;"| 0-0
|-
| style="text-align:center;"|[[Northampton Town F.C.|Northampton Town]]
| style="background:#cfc; text-align:center;"| 1-0
| style="background:#cfc; text-align:center;"| 3-2
|-
| style="text-align:center;"|[[Oxford United F.C.|Oxford United]]
| style="background:#cfc; text-align:center;"| 2-1
| style="background:#fdd; text-align:center;"| 0-1
|-
| style="text-align:center;"|[[Plymouth Argyle F.C.|Plymouth Argyle]]
| style="background:#cfc; text-align:center;"| 2-1
| style="background:#fdd; text-align:center;"| 0-2
|-
| style="text-align:center;"|[[Port Vale F.C.|Port Vale]]
| style="background:#ffc; text-align:center;"| 1-1
| style="background:#fdd; text-align:center;"| 2-3
|-
| style="text-align:center;"|[[Rochdale A.F.C.|Rochdale]]
| style="background:#ffc; text-align:center;"| 0-0
| style="background:#fdd; text-align:center;"| 1-4
|-
| style="text-align:center;"|[[Rotherham United F.C.|Rotherham United]]
| style="background:#cfc; text-align:center;"| 3-0
| style="background:#fdd; text-align:center;"| 2-4 
|-
| style="text-align:center;"|[[Southend United F.C.|Southend United]]
| style="background:#fdd; text-align:center;"| 1-3
| style="background:#cfc; text-align:center;"| 2-1
|-
| style="text-align:center;"|[[Torquay United F.C.|Torquay United]]
| style="background:#cfc; text-align:center;"| 2-1
| style="background:#ffc; text-align:center;"| 2-2
|-
| style="text-align:center;"|[[Wycombe Wanderers F.C.|Wycombe Wanderers]]
| style="background:#cfc; text-align:center;"| 4-0
| style="background:#ffc; text-align:center;"| 1-1
|-
| style="text-align:center;"|[[York City F.C.|York City]]
| style="background:#ffc; text-align:center;"| 1-1
| style="background:#ffc; text-align:center;"| 0-0
|}

==Matches==
{{legend2|#CCFFCC|Won|border=1px solid #AAAAAA}}
{{legend2|#FFFFCC|Drawn|border=1px solid #AAAAAA}}
{{legend2|#FFCCCC|Lost|border=1px solid #AAAAAA}}
{{legend2|#AAAAAA|Postponed|border=1px solid #AAAAAA}}

===Pre-season friendlies===
{{footballbox collapsible|#CCFFCC|
|date = 21 July 2012
|time = 15:00
|team1 = [[Evesham United F.C.|Evesham United]]
|score = 1-1
|report =
|team2 = Cheltenham Town
|goals1 = Hamilton (og) 57'
|goals2 = [[Tristan Plummer|Plummer]] 80'
|stadium = Spiers & Hartwell Jubilee Stadium
|attendance = 657
|result = D
}}

{{footballbox collapsible
|date = 24 July 2012
|time = 19:45
|team1 = [[Port Talbot Town F.C.|Port Talbot Town]]
|score = 1-1
|report =
|team2 = Cheltenham Town
|goals1 = John 15'
|goals2 = [[Darryl Duffy|Duffy]] 47'
|stadium = GenQuip Stadium
|attendance = 160
|result = D
}}

{{footballbox collapsible
|date = 28 July 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 1-0
|report =
|team2 = [[Birmingham City F.C.|Birmingham City]]
|goals1 = [[Harry Hooman|Hooman]] 12'
|goals2 = 
|stadium = [[Whaddon Road]]
|attendance = 2,636
|result = W
}}

{{footballbox collapsible
|date = 31 July 2012
|time = 19:45
|team1 = Cheltenham Town
|score = 0-4
|report =
|team2 = [[Cardiff City F.C.|Cardiff City]]
|goals1 = 
|goals2 = [[Andrew Taylor (footballer born 1986)|Taylor]] {{goal|27}}<br>[[Robert Earnshaw|Earnshaw]] {{goal|32}}<br>[[Craig Conway (footballer)|Conway]] {{goal|45}}<br>[[Peter Whittingham|Whittingham]] {{goal|53}}
|stadium = Whaddon Road
|attendance = 1,615
|result = L
}}

{{footballbox collapsible
|date = 4 August 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 0-4
|report =
|team2 = [[Burnley F.C.|Burnley]]
|goals1 = 
|goals2 = [[Charlie Austin|Austin]] {{goal|32|49}}<br>Mills {{goal|66}}<br>[[Ross Wallace|Wallace]] {{goal|70}}
|stadium = Whaddon Road
|attendance = 1,082
|result = L
}}

{{footballbox collapsible
|date = 7 August 2012
|time = 19:45
|team1 = [[Solihull Moors F.C.|Solihull Moors]]
|score = 1-2
|report = 
|team2 = Cheltenham Town
|goals1 =
|goals2 =
|stadium = [[Damson Park]]
|attendance =
|result = W
}}

=== League Two ===
{{see also|2012–13 Football League Two}}

{{footballbox collapsible
|date = 18 August 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 2-0
|report = 
|team2 = [[Dagenham and Redbridge F.C.|Dagenham and Redbridge]]
|goals1 = Harrad {{goal|45}}<br>McGlashan {{goal|49}}
|goals2 =
|stadium = Whaddon Road
|attendance = 2,655
|result = W
}}

{{footballbox collapsible
|date = 21 August 2012
|time = 19:45
|team1 = [[Torquay United F.C.|Torquay United]]
|score = 2-2
|report = 
|team2 = Cheltenham Town
|goals1 = [[Aaron Downes|Downes]] {{goal|22}}<br>[[Rene Howe|Howe]] {{goal|23}}
|goals2 = Harrad {{goal|15}}<br>Zebroski {{goal|80}}
|stadium = [[Plainmoor]]
|attendance = 2,517
|result = D
}}

{{footballbox collapsible
|date = 25 August 2012
|time = 15:00
|team1 = [[Aldershot Town F.C.|Aldershot Town]]
|score = 0-1
|report = 
|team2 = Cheltenham Town
|goals1 = 
|goals2 = Harrad {{goal|73}}
|stadium = [[Recreation Ground (Aldershot)|The Recreation Ground]]
|attendance = 2,166
|result = W
}}

{{footballbox collapsible
|date = 1 September 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 0-3
|report = 
|team2 = [[Accrington Stanley F.C.|Accrington Stanley]]
|goals1 = 
|goals2 = [[Padraig Amond|Amond]] {{goal|3}}<br>[[George Miller (footballer born 1991)|Miller]] {{goal|33}}<br>[[Romuald Boco|Boco]] {{goal|47}}
|stadium = Whaddon Road
|attendance = 2,708
|result = L
}}

{{footballbox collapsible
|date = 8 September 2012
|time = 15:00
|team1 = [[Wycombe Wanderers F.C.|Wycombe Wanderers]]
|score = 1-1
|report = 
|team2 = Cheltenham Town
|goals1 = Lowe {{goal|3|o.g.}}
|goals2 = Harrad {{goal|38|pen.}}
|stadium = [[Adams Park]]
|attendance = 3,667
|result = D
}}

{{footballbox collapsible
|date = 15 September 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 1-3
|report = 
|team2 = [[Southend United F.C.|Southend United]]
|goals1 = McGlashan {{goal|15}}
|goals2 = [[Sean Clohessy|Clohessy]] {{goal|44}}<br>[[Britt Assombalonga|Assombalonga]] {{goal|49}}<br>[[Mark Phillips|Phillips]] {{goal|52}}
|stadium = Whaddon Road
|attendance = 2,751
|result = L
}}

{{footballbox collapsible
|date = 18 September 2012
|time = 19:45
|team1 = Cheltenham Town
|score = 2-1
|report = 
|team2 = [[Oxford United F.C.|Oxford United]]
|goals1 = Carter {{goal|29}}<br>Harrad {{goal|60}}
|goals2 = [[Peter Leven|Leven]] {{goal|80|pen}}
|stadium = Whaddon Road
|attendance = 3,037
|result = W
}}

{{footballbox collapsible
|date = 22 September 2012
|time = 15:00
|team1 = [[York City F.C.|York City]]
|score = 0-0
|report = 
|team2 = Cheltenham Town
|goals1 = 
|goals2 = 
|stadium = [[Bootham Crescent]]
|attendance = 3,477
|result = D
}}

{{footballbox collapsible
|date = 28 September 2012
|time = 19:45
|team1 = Cheltenham Town
|score = 2-0
|report = 
|team2 = [[Morecambe F.C.|Morecambe]]
|goals1 = Carter {{goal|2||90+1}}
|goals2 = [[Kevin Ellison (footballer)|Ellison]] {{sent off|0|57}}
|stadium = Whaddon Road
|attendance = 2,563
|result = W
}}

{{footballbox collapsible
|date = 3 October 2012
|time = 19:45
|team1 = [[Bristol Rovers F.C.|Bristol Rovers]]
|score = 0-1
|report = 
|team2 = Cheltenham Town
|goals1 = 
|goals2 = Zebroski {{goal|39}}
|stadium = [[Memorial Stadium (Bristol)|The Memorial Stadium]]
|attendance = 5,747
|result = W
}}

{{footballbox collapsible
|date = 6 October 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 2-2
|report = 
|team2 = [[Fleetwood Town F.C.|Fleetwood Town]]
|goals1 = Goulding {{goal|64}}<br>Jombati {{goal|74}}<br>Bennett {{sent off|0|90+3}}
|goals2 = [[David Ball (footballer)|Ball]] {{goal|32}}<br>[[Junior Brown (footballer)|Brown]] {{goal|90+2}}
|stadium = Whaddon Road
|attendance = 2,702
|result = D
}}

{{footballbox collapsible
|date = 13 October 2012
|time = 15:00
|team1 = [[AFC Wimbledon]]
|score = 1-2
|report = 
|team2 = Cheltenham Town
|goals1 = [[Byron Harrison (footballer)|Harrison]] {{goal|90+1}}
|goals2 = Pack {{goal|25}}<br>Mohamed {{goal|56}}
|stadium = [[Kingsmeadow]]
|attendance = 4,409
|result = W
}}

{{footballbox collapsible
|date = 20 October 2012
|time = 15:00
|team1 = [[Bradford City A.F.C.|Bradford City]]
|score = 3-1
|report = 
|team2 = Cheltenham Town
|goals1 = [[Nahki Wells|Wells]] {{goal|45+4|pen|68}}<br>[[James Meredith (footballer)|Meredith]] {{goal|83}}
|goals2 = Pack {{goal|30|pen}}
|stadium = [[Valley Parade]]
|attendance = 9,648
|result = L
}}

{{footballbox collapsible
|date = 23 October 2012
|time = 19:45
|team1 = Cheltenham Town
|score = 2-1
|report = 
|team2 = [[Plymouth Argyle F.C.|Plymouth Argyle]]
|goals1 = Elliott {{goal|37}}<br>Mohamed {{goal|78}} 
|goals2 = [[Conor Hourihane|Hourihane]] {{goal|30}}
|stadium = Whaddon Road
|attendance = 3,058
|result = W
}}

{{footballbox collapsible
|date = 27 October 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 3-0
|report = 
|team2 = [[Exeter City F.C.|Exeter City]]
|goals1 = [[Lawson D'Ath|D'Ath]] {{goal|55}}<br>Lowe {{goal|62||87}} 
|goals2 = 
|stadium = Whaddon Road
|attendance = 3,545
|result = W
}}

{{footballbox collapsible
|date = 6 November 2012
|time = 19:45
|team1 = [[Gillingham F.C.|Gillingham]]
|score = 0-0
|report = 
|team2 = Cheltenham Town
|goals1 =  
|goals2 = 
|stadium = [[Priestfield Stadium|Priestfield]]
|attendance = 6,096
|result = D
}}

{{footballbox collapsible
|date = 10 November 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 1-0
|report = 
|team2 = [[Burton Albion F.C.|Burton Albion]]
|goals1 = Zebroski {{goal|86}} 
|goals2 = 
|stadium = Whaddon Road
|attendance = 2,804
|result = W
}}

{{footballbox collapsible
|date = 17 November 2012
|time = 15:00
|team1 = [[Rotherham United F.C.|Rotherham United]]
|score = 4-2
|report = 
|team2 = Cheltenham Town
|goals1 = [[Daniel Nardiello|Nardiello]] {{goal|4||29}}<br>[[Ben Pringle|Pringle]] {{goal|20}}<br>[[Lee Frecklington|Frecklington]] {{goal|90+2}}
|goals2 = Lowe {{goal|1}}|Zebroski {{goal|31}}
|stadium = [[New York Stadium]]
|attendance = 6,705
|result = L
}}

{{footballbox collapsible
|date = 20 November 2012
|time = 19:45
|team1 = [[Chesterfield F.C.|Chesterfield]]
|score = 4-1
|report = 
|team2 = Cheltenham Town
|goals1 = [[Chris Atkinson (footballer)|Atkinson]] {{goal|16}}<br>[[Sam Togwell|Togwell]] {{goal|45}}<br>[[Marc Richards|Richards]] {{goal|59}}<br>[[Jack Lester|Lester]] {{goal|75}}
|goals2 = Mohamed{{goal|57}}
|stadium = [[Proact Stadium]]
|attendance = 4,333
|result = L
}}

{{footballbox collapsible
|date = 24 November 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 1-0
|report = 
|team2 = [[Barnet F.C.|Barnet]]
|goals1 = Goulding {{goal|80}}
|goals2 = 
|stadium = Whaddon Road
|attendance = 2,591
|result = W
}}

{{footballbox collapsible
|date = 8 December 2012
|time = 15:00
|team1 = [[Northampton Town F.C.|Northampton Town]]
|score = 2-3
|report = 
|team2 = Cheltenham Town
|goals1 = [[Adebayo Akinfenwa|Akinfenwa]] {{goal|2}}<br>Jones {{goal|14|o.g.}}
|goals2 = Carter {{goal|25}}<br>Zebroski {{goal|28}}<br>Duffy {{goal|80}}
|stadium = [[Sixfields Stadium|Sixfields]]
|attendance = 4,625
|result = W
}}

{{footballbox collapsible
|date = 15 December 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 1-1
|report = 
|team2 = [[Port Vale F.C.|Port Vale]]
|goals1 = Carter {{goal|61}}
|goals2 = [[Louis Dodds|Dodds]] {{goal|67}}
|stadium = Whaddon Road
|attendance = 3,670
|result = D
}}

{{footballbox collapsible
|date = 21 December 2012
|time = 19:45
|team1 = [[Rochdale A.F.C.|Rochdale]]
|score = 4-1
|report = 
|team2 = Cheltenham
|goals1 = [[Bobby Grant (footballer)|Grant]] {{goal|38}}<br>[[Ashley Grimes (English footballer)|Ashley Grimes]] {{goal|45+2||67}}<br>[[Dele Adebola|Adebola]] {{goal|69}}
|goals2 = [[Phil Edwards (footballer)|Edwards]] {{goal|48|o.g.}}
|stadium = [[Spotland Stadium|Spotland]]
|attendance = 1,605
|result = L
}}

{{footballbox collapsible
|date = 26 December 2012
|time = 15:00
|team1 = Cheltenham Town
|score = 4-0
|report = 
|team2 = [[Wycombe Wanderers F.C.|Wycombe Wanderers]]
|goals1 = Jake Taylor {{goal|29}}<br>Goulding {{goal|32}}<br>Duffy {{goal|54}}<br>McGlashan {{goal|69}}
|goals2 = 
|stadium = Whaddon Road
|attendance = 3,501
|result = W
}}

{{footballbox collapsible
|date = 29 December 2012
|time = 
|team1 = Cheltenham Town
|score = P-P
|report = 
|team2 = [[Bristol Rovers F.C.|Bristol Rovers]]
|goals1 = 
|goals2 = 
|stadium = Whaddon Road
|attendance =
|result = P
}}

{{footballbox collapsible
|date = 1 January 2013
|time = 15:00
|team1 = [[Oxford United F.C.|Oxford United]]
|score = 1-0
|report = 
|team2 = Cheltenham Town
|goals1 = [[Peter Leven|Leven]] {{goal|16|pen}}
|goals2 = 
|stadium = [[Kassam Stadium]]
|attendance = 6,951
|result = L
}}

{{footballbox collapsible
|date = 12 January 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 1-1
|report = 
|team2 = [[York City F.C.|York City]]
|goals1 = Harrad {{goal|67|pen}}
|goals2 = [[Jason Walker (footballer)|Walker]] {{goal|84}}
|stadium = Whaddon Road
|attendance = 2,881
|result = D
}}

{{footballbox collapsible
|date = 18 January 2013
|time = 19:45
|team1 = [[Morecambe F.C.|Morecambe]]
|score = 0-0
|report = 
|team2 = Cheltenham Town
|goals1 = 
|goals2 = 
|stadium = [[Globe Arena]]
|attendance = 1,586
|result = D
}}

{{footballbox collapsible
|date = 25 January 2013
|time = 19:45
|team1 = Cheltenham Town
|score = 0-0
|report = 
|team2 = [[Rochdale A.F.C.|Rochdale]]
|goals1 = 
|goals2 = 
|stadium = Whaddon Road
|attendance = 2,348
|result = D
}}

{{footballbox collapsible
|date = 2 February 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 2-1
|report = 
|team2 = [[Torquay United F.C.|Torquay United]]
|goals1 = Lowe {{goal|33}}<br>Harrad {{goal|51}}
|goals2 = [[Rene Howe|Howe]] {{goal|88}}
|stadium = Whaddon Road
|attendance = 2,961
|result = W
}}

{{footballbox collapsible
|date = 5 February 2013
|time = 19:45
|team1 = Cheltenham Town
|score = 1-1
|report = 
|team2 = [[Bristol Rovers F.C.|Bristol Rovers]]
|goals1 = Harrad {{goal|45+3}}
|goals2 = [[Oliver Norburn|Norburn]] {{goal|90+5}}
|stadium = Whaddon Road
|attendance = 4,436
|result = D
}}

{{footballbox collapsible
|date = 9 February 2013
|time = 15:00
|team1 = [[Dagenham & Redbridge F.C.|Dagenham & Redbridge]]
|score = 1-0
|report = 
|team2 = Cheltenham Town
|goals1 = [[Luke Howell|Howell]] {{goal|58}}
|goals2 = 
|stadium = [[Victoria Road (Dagenham)|Victoria Road]]
|attendance = 1,526
|result = L
}}

{{footballbox collapsible
|date = 12 February 2013
|time = 19:45
|team1 = [[Southend United F.C.|Southend United]]
|score = 1-2
|report = 
|team2 = Cheltenham Town
|goals1 = [[Ben Reeves|Reeves]] {{goal|90+2}}
|goals2 = Pack {{goal|13|pen}}<br>Benson {{goal|38}}
|stadium = [[Roots Hall]]
|attendance = 3,908
|result = W
}}

{{footballbox collapsible
|date = 16 February 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 1-1
|report = 
|team2 = [[Aldershot Town F.C.|Aldershot Town]]
|goals1 = Benson {{goal|67}}
|goals2 = [[Danny Rose (footballer born 1988)|Rose]] {{goal|23}}
|stadium = Whaddon Road
|attendance = 3,119
|result = D
}}

{{footballbox collapsible
|date = 23 February 2013
|time = 15:00
|team1 = [[Accrington Stanley F.C.|Accrington Stanley]]
|score = 2-2
|report = 
|team2 = Cheltenham Town
|goals1 = [[James Beattie (footballer)|Beattie]] {{goal|40||64}}
|goals2 = Pack {{goal|32|pen|70}}
|stadium = [[Crown Ground]]
|attendance = 1,216
|result = D
}}

{{footballbox collapsible
|date = 26 February 2013
|time = 19:45
|team1 = [[Fleetwood Town F.C.|Fleetwood Town]]
|score = 1-1
|report = 
|team2 = Cheltenham Town
|goals1 = [[Junior Brown (footballer)|Brown]] {{goal|69}}
|goals2 = Pack {{goal|24}}
|stadium = [[Highbury Stadium, Fleetwood|Highbury]]
|attendance = 2,031
|result = D
}}

{{footballbox collapsible
|date = 2 March 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 2-1
|report = 
|team2 = [[A.F.C. Wimbledon|Wimbledon]]
|goals1 = Elliott {{goal|10}}<br>Carter {{goal|56}}
|goals2 = [[Ben Dickenson|Dickenson]] {{goal|63}}
|stadium = Whaddon Road
|attendance = 3,177
|result = W
}}

{{footballbox collapsible
|date = 5 March 2013
|time = 19:45
|team1 = Cheltenham Town
|score = 1-0
|report = 
|team2 = [[Chesterfield F.C.|Chesterfield]]
|goals1 = Benson {{goal|90+2}}
|goals2 = 
|stadium = Whaddon Road
|attendance = 2,195
|result = W
}}

{{footballbox collapsible
|date = 9 March 2013
|time = 15:00
|team1 = [[Burton Albion F.C.|Burton Albion]]
|score = 3-1
|report = 
|team2 = Cheltenham Town
|goals1 = [[Alex MacDonald (footballer born 1990)|MacDonald]] {{goal|22}}<br>[[Robbie Weir|Weir]] {{goal|26||48}}
|goals2 = Harrison {{goal|90+2}}
|stadium = [[Pirelli Stadium]]
|attendance = 2,680
|result = L
}}

{{footballbox collapsible
|date = 16 March 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 3-0
|report = 
|team2 = [[Rotherham United F.C.|Rotherham United]]
|goals1 = McGlashan {{goal|32}}<br>Pack {{goal|41}}<br>Elliott{{goal|84}}
|goals2 = 
|stadium = Whaddon Road
|attendance = 3,241
|result = W
}}

{{footballbox collapsible
|date = 23 March 2013
|time = 15:00
|team1 = [[Barnet F.C.|Barnet]]
|score = 0-0
|report = 
|team2 = Cheltenham Town
|goals1 = 
|goals2 = 
|stadium = [[Underhill Stadium|Underhill]]
|attendance = 2,400
|result = D
}}

{{footballbox collapsible
|date = 29 March 2013
|time = 15:00
|team1 = [[Port Vale F.C.|Port Vale]]
|score = 3-2
|report = 
|team2 = Cheltenham Town
|goals1 = [[Tom Pope|Pope]] {{goal|19||58||74}}
|goals2 = Mohamed{{goal|47}}<br>Benson {{goal|54}}
|stadium = [[Vale Park]]
|attendance = 5,867
|result = L
}}

{{footballbox collapsible
|date = 1 April 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 1-0
|report = 
|team2 = [[Northampton Town F.C.|Northampton Town]]
|goals1 = Elliott {{goal|44}}
|goals2 = 
|stadium = Whaddon Road
|attendance = 4,042
|result = W
}}

{{footballbox collapsible
|date = 6 April 2013
|time = 15:00
|team1 = [[Plymouth Argyle F.C.|Plymouth Argyle]]
|score = 2-0
|report = 
|team2 = Cheltenham Town
|goals1 = [[Reuben Reid|Reid]] {{goal|40}}<br>[[Michael Hector|Hector]] {{goal|54}} {{og}}
|goals2 = 
|stadium = [[Home Park]]
|attendance = 
|result = L
}}

{{footballbox collapsible
|date = 13 April 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 1-0
|report = 
|team2 = [[Gillingham F.C.|Gillingham]]
|goals1 = Hector {{goal|67}}
|goals2 = 
|stadium = Whaddon Road
|attendance = 
|result = W
}}

{{footballbox collapsible
|date = 20 April 2013
|time = 15:00
|team1 = [[Exeter City F.C.|Exeter City]]
|score = 1-0
|report = 
|team2 = Cheltenham Town
|goals1 = Penn {{goal|5}}
|goals2 = 
|stadium = [[St James Park]]
|attendance = 4,042
|result = W
}}

{{footballbox collapsible
|date = 27 April 2013
|time = 15:00
|team1 = Cheltenham Town
|score = 0-0
|report = 
|team2 = [[Bradford City F.C.|Bradford City]]
|goals1 = 
|goals2 = 
|stadium = Whaddon Road
|attendance = 
|result = D
}}

=== League Cup ===
{{see also|2012–13 Football League Cup}}

{{footballbox collapsible
|date = 11 August 2012
|time = 15:00
|round = [[2012–13 Football League Cup#First round|First Round]]
|team1 = Cheltenham Town
|score = 1-1
|aet = yes
|team2 = [[Milton Keynes Dons F.C.|Milton Keynes Dons]]
|goals1 = [[Kaid Mohamed|Mohamed]] {{goal|58}}
|goals2 = [[Dean Bowditch|Bowditch]] {{goal|45}}
|stadium = [[Whaddon Road]]
|attendance = 1,660
|result = L
|penalties1 = [[Billy Jones (footballer born 1983)|Jones]] {{pengoal}}<br>[[Darryl Duffy|Duffy]] {{penmiss}}<br>[[Shaun Harrad|Harrad]] {{pengoal}}<br>[[Kaid Mohamed|Mohamed]] {{pengoal}}
|penaltyscore = 3-5
|penalties2 = [[Dean Bowditch|Bowditch]] {{pengoal}}<br>[[Charlie MacDonald|MacDonald]] {{pengoal}}<br>[[Gary MacKenzie|MacKenzie]] {{pengoal}}<br>[[Antony Kay|Kay]] {{pengoal}}<br>[[Ryan Lowe|Lowe]] {{pengoal}}
}}

===Football League Trophy===
{{see also|2012–13 Football League Trophy}}

{{footballbox collapsible
|date = 9 October 2012
|time = 19:45
|team1 = Cheltenham Town
|score = 2 – 4 
|report = 
|team2 = [[Oxford United F.C.|Oxford United]]
|goals1 = [[Darryl Duffy|Duffy]] {{goal|31}} <br> [[Keith Lowe (footballer)|Lowe]] {{goal|45}}
|goals2 = [[Tom Craddock|Craddock]] {{goal|35}} <br> [[Harry Worley|Worley]] {{goal|67}} <br> [[James Constable|Constable]] {{goal|71}} <br>  [[Peter Leven]]{{goal|81|pen.}}
|stadium = Whaddon Road
|attendance = 1,236
|result = L
}}

<!--===FA Cup===
{{see also|2012–13 FA Cup}}-->

==See also==
*[[2012–13 in English football]]
*[[2012–13 Football League Two]]

==References==
{{Reflist|2}}

==External links==

{{Cheltenham Town F.C.}}
{{2012–13 in English football}}

{{DEFAULTSORT:2012-13 Cheltenham Town F.C. Season}}
[[Category:Cheltenham Town F.C. seasons]]
[[Category:2012–13 Football League Two by team|Cheltenham Town]]