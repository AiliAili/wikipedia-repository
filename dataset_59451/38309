{{good article}}
{{Infobox University Boat Race
| name= 135th Boat Race
| winner =Oxford
| margin =  2 and 1/2 lengths
| winning_time=  18 minutes 27 seconds
| date= {{Start date|1989|3|25|df=y}}
| umpire = Ronnie Howard<br>(Oxford)
| prevseason= [[The Boat Race 1988|1988]]
| nextseason= [[The Boat Race 1990|1990]]
| overall =69&ndash;65
| reserve_winner=Isis
| women_winner =Cambridge
}}
The 135th [[The Boat Race|Boat Race]] took place on 25 March 1989.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford won by two-and-a-half lengths.  It was the seventh occasion that the race was umpired by Ronnie Howard, and the first time in the history of the race that both crews were coxed by women.

In the reserve race, Oxford's Isis won, while Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 8 April 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race currently takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities, followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=7 April 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 5 July 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1988|1988 race]] by five-and-a-half lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 12 June 2014}}</ref> with Cambridge leading overall with 69 victories to Oxford's 64 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 6 June 2014}}</ref>  The event was sponsored by [[Beefeater Gin]]; prior to the race, it was announced that the company would be sponsors for the following three years in a deal worth £700,000.<ref name=stats/>  Former Oxford [[Blue (university sport)|Blue]] Ronnie Howard was the umpire for the race for the seventh occasion.<ref>{{Cite news | title = Examination day for Boat Race eights | work = [[The Times]] |issue = 63349 | date = 23 March 1989 | page = 48|first = Jim | last = Railton}}</ref>

Cambridge were coxed by Leigh Weiss while Oxford's cox was Alison Norrish &ndash; it was the first time in the history of the Boat Race that both crews had female coxes.<ref name=coxattack/>  Prior to the race, Oxford coach [[Patrick Sweeney (rower)|Pat Sweeney]] criticised Weiss: "Their cox is so useless she might hit us.  It's not her fault, but Cambridge should have chosen someone who knows the river."<ref name=coxattack>{{Cite news | title = Cambridge cox under attack | work = [[The Times]] | date = 25 March 1989 | first = Jim | last = Railton | page = 43 | issue = 63351}}</ref>  Weiss responded "I feel confident that I will make the decisions to make Cambridge win."<ref name=coxattack/>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

==Crews==
Cambridge were pre-race favourites,<ref name=rough/> as their crew weighed an average of almost {{convert|12|lb|kg}} per rower more than their opponents, the largest difference since [[The Boat Race 1829|the first Boat Race]].<ref name=stats/>  Cambridge's Toby Backhouse weighed in at 16&nbsp;st 11&nbsp;lb (106.3&nbsp;kg) making him the heaviest rower in the history of the event.<ref name=rough>{{Cite news | title = Rough water may upset Cambridge | first =Jim |last = Railton | page=43 | work = [[The Times]] | issue = 63351 | date = 25 March 1989}}</ref>  The Oxford boat featured three former [[Blue (university sport)|Blues]] while Cambridge's contained five.<ref name=rough/>  Cambridge's chief coach was Mike Lees while [[Oxford University Boat Club]] selected Sweeney as coach, and Steve Royle to be their full-time director.<ref name=rough/>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] ||Guy Blanchard || [[Oriel College, Oxford|Oriel]] || 13 st 8 lb || Ian Clarke || [[Fitzwilliam College, Cambridge|Fitzwilliam]] || 13 st 4 lb
|-
| 2 ||  Paul Gleeson || [[Hertford College, Oxford|Hertford]] || 15 st 0 lb || M J K Smith || [[Magdalene College, Cambridge|Magdalene]] || 14 st 12 lb
|-
| 3 || G Cheveley || [[Pembroke College, Oxford|Pembroke]] || 13 st 3 lb ||Paddy Mant|| [[Selwyn College, Cambridge|Selwyn]] || 15 st 3 lb
|-
| 4 || Cal Maclennan || [[Keble College, Oxford|Keble]] || 13 st 7 lb || Matthew Brittin (P) || [[Robinson College, Cambridge|Robinson]] || 14 st 12 lb
|-
| 5 || Terry Dillon || [[Oriel College, Oxford|Oriel]] || 14 st 12 lb || Toby Backhouse || [[Magdalene College, Cambridge|Magdalene]] || 16 st 11 lb
|-
| 6 || Mike Gaffney || [[Hertford College, Oxford|Hertford]] || 14 st 9 lb || Nick Justicz || [[Sidney Sussex College, Cambridge|Sidney Sussex]]  || 14 st 8 lb
|-
| 7 || [[Jonny Searle]] || [[Christ Church, Oxford|Christ Church]] || 13 st 8 lb || Jim Garman || [[St John's College, Cambridge|St John's]] ||  14 st 11 lb
|-
| [[Stroke (rowing)|Stroke]] || Richard Thorp || [[St John's College, Oxford|St John's]] || 13 st 2 lb || [[Guy Pooley]] || [[St John's College, Cambridge|St John's]]  || 13 st 6 lb
|-
| [[Coxswain (rowing)|Cox]] || Alison Norrish || [[University College, Oxford|University]] || 7 st 13 lb || Leigh Weiss || [[Emmanuel College, Cambridge|Emmanuel]]  || 7 st 6 lb
|-
!colspan="7"|Source:<ref name=stats>{{Cite news | title = Statistics place a burden on Oxford | first = Jim | last = Railton | work = [[The Times]] | page = 48 | issue = 63347 | date = 21 March 1989}}</ref><br>(P) &ndash; boat club president
|}

==Races==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge won the toss and elected to start from the Surrey station.<ref name=ruin>{{Cite news | title = Cambrdige go all out to ruin their Boat Race chances | first = Jim| last = Railton | work = [[The Times]] | date = 27 March 1989| page = 28 | issue = 63352}}</ref>  After an early clash of blades, Oxford crept ahead and held a lead of three seconds by the Mile Post.  Two minutes later Oxford had a clear water advantage and continued to pull away, holding a six-second lead at [[Hammersmith Bridge]].  The lead had extended slightly by the Chiswick Steps; Cambridge failed to make any ground on Oxford who swept under [[Barnes Railway Bridge|Barnes Bridge]] eight seconds ahead.  Oxford maintained the lead to pass the finish post two-and-a-half lengths clear.<ref name=ruin/>

In the reserve race, Oxford's Isis won by one-and-a-quarter lengths, their first victory in three years.<ref name=results/> while Cambridge won the 44th [[Women's Boat Race]] by one length in a time of 6 minutes and 20 seconds, their second victory in three years.<ref name=results/>

==Reaction==
Umpire Ronnie Howard said "It was a damaging race, and I was looking for possible breakages".<ref name=blame/>  Oxford's stroke Richard Thorp explained "We expect it [clashing] but Ali [Norrish] has so much more experience of the Tideway".<ref name=blame/> Former Oxford coach [[Daniel Topolski|Dan Topolski]] suggested that "the race is a battle of guts and willpower, and Cambridge settled to a steady rhythm too soon."<ref name=blame>{{Cite news | title = All blame must not lie with Cambridge cox | first = David |last = Miller | work = [[The Times]] | page = 34 | issue = 63352 | date = 27 March 1989}}</ref>

==References==
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1989}}
[[Category:The Boat Race]]
[[Category:1989 in English sport]]
[[Category:1989 in rowing]]