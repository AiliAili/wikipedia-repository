{{good article}}
{{Infobox road
|state=NV
|type=SR
|route=28
|map=Nevada 28 map.svg
|alternate_name= Tahoe Boulevard, Lakeshore Boulevard
|length_mi=16.174
|length_round=3
|length_ref=<ref name=NDOT_Log>{{cite web |url=http://www.nevadadot.com/doing-business/about-ndot/ndot-divisions/planning/roadway-systems/state-maintained-highways-descriptions-index-maps|title=Nevada's State Maintained Highways: Descriptions, Index and Maps |year=2013 |publisher=[[Nevada Department of Transportation]] |accessdate=2013-12-26 }}</ref>
|established=1948
|direction_a=South
|terminus_a={{jct|state=NV|US|50}} near [[Glenbrook, Nevada|Glenbrook]]
|junction={{jct|state=NV|SR|431}} in [[Incline Village, Nevada|Incline Village]]
|direction_b=North
|terminus_b={{jct|state=CA|SR|28}} at [[Kings Beach, CA]]
|counties=[[Douglas County, Nevada|Douglas]], [[Carson City, Nevada|Carson City]], [[Washoe County, Nevada|Washoe]]
|previous_type=I
|previous_route=15
|next_type=US
|next_route=50
|tourist=[[File:MUTCD D6-4.svg|20px|alt=|link=]] Lake Tahoe - Eastshore Drive<ref name=byways/><br>North Shore Road<ref name=NVbyways>{{Cite web|url=http://nevadadot.com/Traveler_Info/Scenic_Byways/Nevada_Scenic_Byways.aspx|title=Nevada's Scenic Byways|publisher=Nevada Department of Transportation|accessdate=2013-07-31}}</ref>
}}

'''State Route&nbsp;28''' ('''SR&nbsp;28''') is a {{convert|16.2|mi|km|adj=on}} road that runs along the northeastern shore of [[Lake Tahoe]]. SR&nbsp;28 starts at [[U.S. Route 50 in Nevada|US&nbsp;50]] and ends at the [[California]] state line at [[Crystal Bay, Nevada|Crystal Bay]], continuing across the border as [[California State Route 28|SR&nbsp;28]]. SR&nbsp;28 is part of the [[National Scenic Byway]] system since September&nbsp;1996, and the [[Nevada Scenic Byways|state scenic byway]] system since June&nbsp;1994.

The highway serves [[Douglas County, Nevada|Douglas County]] and [[Washoe County, Nevada|Washoe County]] as well as a rural part of [[Carson City, Nevada|Carson City]]. SR&nbsp;28 was designated in 1948, and has not significantly changed since it was first paved.

==Route description==
[[File:2015-10-28 14 21 23 View north from the south end of Nevada State Route 28 in Douglas County, Nevada.jpg|thumb|left|View north from the south end of SR 28]]
SR 28 begins at [[U.S. Route 50 in Nevada|U.S. Route 50]] (US&nbsp;50) in Douglas County, Nevada.<ref name=GoogleMaps>{{Google Maps|url=https://www.google.com/maps?q=Nevada+State+Route+28&hl=en&sll=39.171782,-119.960518&sspn=0.420538,0.891953&hnear=Nevada+28,+Nevada&t=m&z=12|title=Nevada State Route 28|accessdate=2013-07-30}}</ref> It heads north from there, forming part of the boundary of [[Lake Tahoe – Nevada State Park]] until crossing into [[Carson City, Nevada|Carson City]]. The highway then enters [[Washoe County, Nevada|Washoe County]], where the highway enters [[Humboldt-Toiyabe National Forest]]. It then turns northwest, running through [[Incline Village, Nevada|Incline Village]].<ref name=GoogleMaps/> In western Incline Village, SR&nbsp;28 junctions with [[Nevada State Route 431|SR&nbsp;431]], which is a direct link to [[Reno, Nevada|Reno]] to the northeast. Just short of the California state line, it turns south to run parallel to the line and finally crosses into [[California]] at [[Crystal Bay, Nevada|Crystal Bay]]. The road continues west of the California border as [[California State Route 28]].<ref name=GoogleMaps/>

[[Image:LaketahoeSR28.jpg|thumb|right|[[Rest area]] along SR 28 with view of [[Lake Tahoe]] and [[Incline Village, Nevada|Incline Village]] ]]
The route has been designated as the north piece of the Lake Tahoe–Eastshore Drive Scenic Byway, as part of the [[National Scenic Byway]] program.<ref name=byways>{{cite web
|url = http://www.byways.org/explore/byways/2456/|title =  Lake Tahoe – Eastshore Drive|publisher = [[U.S. Department of Transportation]] National Scenic Byways Program|accessdate=2013-07-30}}</ref> SR&nbsp;28 was also designated as a state scenic byway.<ref name=NVbyways/> SR&nbsp;28 is not part of the [[National Highway System (United States)|National Highway System]].<ref name="NHS">{{cite map | title=National Highway System: Nevada | date=March 2013 | publisher=[[Federal Highway Administration]] |url=http://www.fhwa.dot.gov/planning/national_highway_system/nhs_maps/nevada/nv_nevada.pdf | format=PDF|accessdate=2013-07-30}}</ref> Around&nbsp;11,300 cars use SR&nbsp;28 [[Annual Average Daily Traffic|on average each day]].<ref name=ATR>{{Cite web |url=http://www.nevadadot.com/uploadedFiles/NDOT/About_NDOT/NDOT_Divisions/Planning/Traffic/2012ATRs.pdf |title=2012 Annual Traffic Report |accessdate=2013-07-30|publisher=Nevada Department of Transportation}}</ref>

==History==
[[Image:Laketahoeatsunset.JPG|thumb|[[Lake Tahoe]] at sunset as seen from SR 28]]
The road that became SR&nbsp;28 was paved around 1932,<ref name=1932NV>{{Cite map|url=http://contentdm.library.unr.edu/cdm4/document.php?CISOROOT=/hmaps&CISOPTR=569&REC=10|publisher=Nevada Department of Highways|title=1932 Map of Nevada|year=1932|accessdate=2013-07-31|cartography=Nevada Department of Highways}}</ref> and has been used for [[flume]]s in the timber industry since 1880.<ref name=marker>{{cite web|url=http://nvshpo.org/historical-markers-by-county/178-washoe-countyreno/385-nevada-historical-marker-246.html|accessdate=2013-07-30|publisher = State of Nevada, State Historic Preservation Office|title = The Great Incline of the Sierra Nevada}}</ref> The route first appeared in 1948,<ref name=1948NVmap>{{Cite map|url=http://www.nevadadot.com/uploadedFiles/NDOT/Traveler_Info/Maps/1948HwyMapDuplexWeb.pdf|title= 1948 Map of Nevada|publisher=Nevada Department of Highways|cartography=Nevada Department of Highways|accessdate=2013-07-29|year=1948}}</ref> with the same general alignment as it has today.<ref name=2013NVMap>{{Cite map|url=http://www.nevadadot.com/uploadedFiles/NDOT/Traveler_Info/Maps/NDOT%20Highway%20Map%202013-2014.pdf|publisher=Nevada Department of Transportation|cartography=Nevada Department of Transportation|accessdate=2013-07-29|inset=Lake Tahoe|title=Official Nevada State Map}}</ref> The highway gained fame for many years as the location of the [[Ponderosa Ranch]], filming location of the television series [[Bonanza]].<ref name=Ponderosa>{{Google maps |url=https://maps.google.com/maps?q=Ponderosa+Ranch,+Ponderosa+Ranch+Road,+Incline+Village,+NV&hl=en&sll=40.113478,-83.145924&sspn=0.207425,0.445976&oq=Ponderosa+Ranch,In&hq=Ponderosa+Ranch,+Ponderosa+Ranch+Road,+Incline+Village,+NV&t=m&z=15|title=Ponderosa Ranch|accessdate=2013-07-29}}</ref> On June 7, 1994, the [[Nevada Department of Transportation]]&nbsp;(NDOT) designated SR&nbsp;28 as a scenic byway, named North Shore Road.<ref name=NVbyways/> Later in September 1996, SR&nbsp;28 and part of US&nbsp;50 was designated as Lake Tahoe - Eastshore Drive, a National Scenic Byway.<ref name=byways/>

== Major intersections ==
:Note: [[Milepost]]s in Nevada reset at county lines; the start and end mileposts for each county are given in the county column.
{{jcttop
|length_ref=<br/><ref name=NDOT_Log/><ref name=GoogleMaps/><ref name="NDOT_SHDescription">{{cite web |title=2013 Nevada State Maintained Highways, Descriptions, Index and Maps |author=Nevada Department of Transportation |url=http://www.nevadadot.com/uploadedFiles/NDOT/About_NDOT/NDOT_Divisions/Planning/Roadway_Systems/DI_2013.pdf |year=2013 |accessdate=2013-07-30}}</ref>
}}
{{NVint
|county=Douglas
|county_note=0.00–1.23
|location=none
|lspan=2
|mile=0.00
|road={{Jct|state=NV|US|50|city1=Carson City|city2=Stateline}}
}}
{{jctco|state=NV |county=Carson City|county_note=0.00–3.95}}
{{NVint
|location=Incline Village
|lspan=2
|county=Washoe
|county_note=0.00–10.99
|cspan=3
|mile=4.98
|road=Lakeshore Boulevard
}}
{{NVint
|mile=8.08
|road={{Jct|state=NV|SR|431|name1=Mount Rose Highway|city1=Reno}}
|notes=A [[roundabout]]
}}
{{NVint
|location=Crystal Bay
|mile=10.99
|road={{Jct|state=CA|SR|28|city1=Tahoe City}}
}}
{{jctbtm}}

==See also==
*{{portal-inline|Nevada}}
*{{portal-inline|U.S. Roads}}

==References==
{{Reflist|30em}}

==External links==
{{AttachedKML|display=title,inline}}

*{{commons category-inline}}
*[http://www.aaroads.com/west/nv-028.html AARoads: Nevada 28]

{{DEFAULTSORT:028}}
[[Category:State highways in Nevada]]
[[Category:National Scenic Byways]]
[[Category:Nevada Scenic Byways]]
[[Category:Transportation in Douglas County, Nevada|Nevada State Route 28]]
[[Category:Transportation in Carson City, Nevada|Nevada State Route 28]]
[[Category:Transportation in Washoe County, Nevada|Nevada State Route 28]]
[[Category:Lake Tahoe]]