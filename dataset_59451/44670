{{italic title}}
{{Infobox Journal
| cover = [[Image:Angewandte Chemie journal cover.gif]]
| discipline = [[Chemistry]]
| editor = Peter Gölitz
| frequency = Weekly
| language = [[German language|German]], [[English language|English]]
| abbreviation = Angew. Chem. Int. Ed. (English), Angew. Chem. (German)
| link1 = http://www.angewandte.de
| link1-name = German edition
| link2 = http://www.angewandte.org
| link2-name = English edition
| publisher = [[Wiley-VCH]]
| country = [[Germany]]
| history = 1887–present (in German), 1962–present (in English)
| ISSN = 1433-7851
| eISSN = 1521-3773
| CODEN = ACIEF5 (International), ANCEAD (German)
| impact = 11.709
| impact-year = 2015
}}
'''''Angewandte Chemie''''' ({{IPA-de|ˈʔaŋɡəˌvantə çeˈmiː}}, meaning "Applied Chemistry") is a weekly [[Peer review|peer-reviewed]] [[scientific journal]] that is published by [[Wiley-VCH]] on behalf of the [[German Chemical Society]] (Gesellschaft Deutscher Chemiker). Publishing formats include feature length [[Scientific journal#Types of articles|reviews]], short highlights, [[Scientific journal#Types of articles|research communications]], minireviews, essays, book reviews, meeting reviews, correspondences, corrections, and obituaries. This journal contains review articles covering all aspects of [[chemistry]]. Its current [[impact factor]] is 11.709 (2015).<ref>Journal Citation Reports, July 2016</ref>

== Editions ==

The journal appears in two editions with separate volume and page numbering: a German edition, ''Angewandte Chemie'' ({{ISSN|0044-8249}} (print), {{ISSN|1521-3757}} (online)), and a fully English-language edition, '''''Angewandte Chemie International Edition''''' ({{ISSN|1433-7851}} (print), {{ISSN|1521-3773}} (online)). The editions are identical in content with the exception of occasional reviews of German-language books or German translations of [[IUPAC]] recommendations.

== Business model ==
''Angewandte Chemie'' is available online and in print. It is a [[hybrid open access journal]] and authors may choose to pay a fee to make articles available free of charge. ''Angewandte Chemie'' provides free access to supporting information.

== Publication history ==

In 1887, [[Ferdinand Fischer]] founded the '''''Zeitschrift für die Chemische Industrie''''' ('''''Journal for the Chemical Industry'''''). In 1888, the title was changed to '''''Zeitschrift für Angewandte Chemie''''' (''Journal of Applied Chemistry''), and volume numbering started over. This title was kept until the end of 1941 when it was changed to '''''Die Chemie'''''. Until 1920, the journal was published by [[Springer Science+Business Media|Springer Verlag]] and by [[Springer Science+Business Media|Verlag Chemie]] starting in 1921. Due to [[World War II]], the journal did not publish from April 1945 to December 1946. In 1947, publication was resumed under the current title, ''Angewandte Chemie''.<ref>{{ cite journal | author = P. Gölitz | journal = Angewandte Chemie | year = 1988 | volume = 100 | pages =  3 | title = Editorial | doi = 10.1002/ange.19881000105 }} ; {{ cite journal | author = P. Gölitz | journal = Angewandte Chemie International Edition | year = 1988 | volume = 27 | pages =  3 | title = Editorial | doi = 10.1002/anie.198800031}}</ref>

In 1962, the English-language edition was launched as '''''Angewandte Chemie International Edition in English''''' ({{ISSN|0570-0833}}, [[CODEN]] ACIEAY, abbreviated as '''''Angew. Chem. Int. Ed. Engl.'''''), which has a separate volume counting. With the beginning of Vol. 37 (1998) "in English" was dropped from the journal name.

Several journals have merged into ''Angewandte'', including '''''Chemische Technik'''''/'''''Chemische Apparatur''''' in 1947 and '''''Zeitschrift für Chemie''''' in 1990.

== Criticism ==
While it has been suggested that Angewandte's [[impact factor]] is as high as it is in comparison to other [[chemistry journals]] because the journal contains reviews, the editors claim this effect is too small to explain the difference or affect the ranking of the journal in its subject group.<ref>{{cite journal | author = P. Gölitz | journal = Angewandte Chemie | year = 2006 | volume = 118 | pages = 5152–5154 | doi = 10.1002/ange.200602672 | title = Der Druck steigt }} ; {{cite journal | author = P. Gölitz | journal = Angewandte Chemie International Edition | year = 2006 | volume = 45 | pages = 5030–5032 | title = The Heat Is On| doi = 10.1002/anie.200602672}}</ref>

== References ==
{{reflist}}

[[Category:Chemistry journals]]
[[Category:German-language journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1887]]
[[Category:Wiley-Blackwell academic journals]]