{{Infobox journal
| title = Nature Photonics
| cover = [[Image:Naturephotonics.gif]]
| discipline = [[Photonics]]
| abbreviation = Nat. Photon.
| editor = Rachel Won, Oliver Graydon, David Pile & Noriaki Horiuchi
| publisher = [[Nature Publishing Group]]
| country = United Kingdom
| history = 2007-present
| frequency = Monthly
| impact = 31.167
| impact-year = 2015
| website = http://www.nature.com/naturephotonics
| ISSN = 1749-4885
| eISSN = 1749-4893
| LCCN = 2007212985
| OCLC = 78160603
}}
'''''Nature Photonics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[Nature Publishing Group]]. The [[editor-in-chief]] is Oliver Graydon. The journal covers research related to [[optoelectronics]], [[laser]] science, imaging, communications, and other aspects of [[photonics]] and was established in January 2007. ''Nature Photonics'' publishes [[review article]]s, research papers, News and Views pieces, and research highlights summarizing the latest scientific findings in optoelectronics. This is complemented by a mix of articles dedicated to the business side of the industry covering areas such as technology commercialization and market analysis. The papers that have been published in this journal are internationally acclaimed for maintaining high research standards. The journal is regarded as top-ranking in the field of [[photonics]].

''Nature Photonics'' is indexed in the [[NASA Astrophysics Data System]] and [[Science Citation Index]].

== External links ==
* {{Official website|http://www.nature.com/naturephotonics}}

{{Georg von Holtzbrinck Publishing Group}}

[[Category:Nature Publishing Group academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2007]]
[[Category:Optics journals]]