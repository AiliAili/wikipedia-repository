{{Infobox journal
| title = PhysChemComm
| cover = [[File:CoverIssuePhysChemComm.jpg]]
| discipline = [[Chemistry]]
| abbreviation = PhysChemComm
| impact = 1.500
| impact-year = 2005
| editor = Susan Appleyard
| website = http://pubs.rsc.org/en/Journals/JournalIssues/qu
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| history = 1998-2003
| frequency = 
| formernames = 
| ISSN = 
| eISSN = 1460-2733
| CODEN = PHCCFX
| LCCN = sn99047155
| OCLC = 40310988
}}
'''''PhysChemComm''''' was a [[peer-reviewed]] [[scientific journal]] that was published by the [[Royal Society of Chemistry]] between 1998 and 2003. It covered all aspects of [[physical chemistry]] and [[chemical physics]], and their interfaces with condensed matter, physics and biological, materials and surface science. The journal was abstracted and indexed in [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-10-07}}</ref> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-10-07}}</ref> According to the ''[[Journal Citation Reports]]'', the journal's last [[impact factor]] of 1.500 was issued in 2005.<ref name=WoS>{{cite book |year=2016 |chapter=PhysChemComm |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==See also==
* ''[[Physical Chemistry Chemical Physics]]''

==References==
{{Reflist}}

==External links==
*{{Official website|http://pubs.rsc.org/en/Journals/JournalIssues/qu}}

{{Royal Society of Chemistry|state=collapsed}}

[[Category:Chemistry journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 1998]]
[[Category:English-language journals]]
[[Category:Publications disestablished in 2003]]
[[Category:Defunct journals of the United Kingdom]]


{{chem-journal-stub}}