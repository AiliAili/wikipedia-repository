'''Velarde''' is a [[census-designated place]] (CDP) in [[Rio Arriba County, New Mexico|Rio Arriba County]], [[New Mexico]], [[United States]]. The population was 502 at the time of the 2010 census.  Velarde is located on [[New Mexico State Road 68]], in the [[Rio Grande Rift]], right at the point where the road enters the [[Rio Grande Gorge]].

==History==

The name Velarde first appeared when a U.S. post office was opened at the location in 1855.<ref name="books.google">Bullock, Alice (1981) [https://books.google.com/books?id=64lvz56LCX4C&pg=PA71&lpg=PA71&dq=David+Velarde+postmaster&source=bl&ots=MqmuoAr774&sig=ZCfkzeOXDVCtfx-3kIX2yuI7vac&sa=X&ei=jwU2UN7gCIjXsgbm8YCgDg&ved=0CBYQ6AEwAg#v=onepage&q&f=false "Mountain Villages"] (Santa Fe: Sunstone Press), pg. 71</ref> The first postmaster, David Velarde, named the postal location in honor of his Spanish-born ancestor, Juan Antonio Pérez Velarde, who settled near [[Ciudad Juárez|Guadalupe del Paso]] (present Ciudad Juárez, Mexico) in 1725. His move to this [[Rio Grande]] settlement encouraged other members of his family to move north into colonial New Mexico in the mid-18th century.<ref>[http://www.newmexicohistory.org/filedetails.php?fileID=22511 "New Mexico Office of the State Historian page on the Velarde family"]</ref><ref>[http://www.triptrivia.com/step4.php?Submit=Submit&State=32&StartCity=25103 "Trip Trivia: Velarde"]</ref><ref name="RobertHixson">Robert Hixson. "The Place Names of New Mexico" (Albuquerque, NM: University of New Mexico Press)</ref>

However, the town was organized only in 1875 by Matias Velarde, who established the town as "La Jolla", the name most likely originating from a 1712 Spanish land grant given to Sebastian Martin in the post-[[Pueblo Revolt]] resettlement of the region.<ref name="books.google_a">Pike, David (2004) [https://books.google.com/books?id=SSXRGB21WH4C&pg=PA84&lpg=PA84&dq=Velarde+La+Jolla+1875&source=bl&ots=inKkDeMggD&sig=kpw8yTCQ6GEBTF8wdd7WbGagF-I&sa=X&ei=jzE2ULbWBMvesgax-4CQCw&ved=0CBsQ6AEwAw#v=onepage&q&f=false "Roadside New Mexico: A Guide to Historic Markers"] (Albuquerque, NM: University of New Mexico Press)</ref><ref>Nostrand, Richard L. (1992) [https://books.google.com/books?id=zlvElL6-WK4C&pg=PA41&lpg=PA41&dq=Velarde+La+Jolla+1712&source=bl&ots=cwMN2qxB9E&sig=HMM6Jkoupyv_0OQ5yBOHo89wGcs&sa=X&ei=aUE2UIetAobBtAbM3IG4BA&ved=0CA8Q6AEwADgK#v=onepage&q&f=false "The Hispano Homeland"] (Norman, OK: University of Oklahoma Press)</ref> This was likely a misspelling of "La Joya," which translates from Spanish as "The Jewel". The [[Tewa language|Tewa]] name for the location was Phahu'bu'u, or Tsigubu'u, which meant "basin of the [[Lycium pallidum|chico bush]] or rabbit thorn".<ref name="RobertHixson" />

[[File:Embudo Pass inverted.jpg|thumb|222px|Map drawn by the U.S. Army Corps of Engineers in 1847. "Joya" is in the south. The "Table Land" in the center is currently known as "Black Mesa," while the town of "Embudo" to the north is currently called "Dixon".]]
The community, located at the mouth of the Rio Grande canyon along the [[New Mexico State Road 68|Low Road]] to [[Taos, New Mexico|Taos]] (not really part of the [[El Camino Real de Tierra Adentro|Camino Real]], despite the ancient pathway over Embudo Pass being designated so by a historical marker <ref name="books.google_a" />), had through its Spanish-settled history served mostly as a peaceful resting spot for passing travelers, especially for those heading north out of the low river country.

During the [[Taos Revolt]] in the latter part of the [[Mexican-American War]], however, it was part of the scene of the [[Battle of Embudo Pass]], which took place on Jan. 29, 1847. Despite the shelter of "dense masses of cedar and large fragments of rock" that were formed into defensive positions, the [[Tewa people|Tewa]] warriors and [[Second Federal Republic of Mexico|Mexican]] defenders in the battle were routed by U.S. Col. [[Sterling Price]], military governor, and his Missouri Mounted Volunteers, leading to the U.S. [[Siege of Pueblo de Taos|siege of Taos Pueblo]] less than a week's forced march later. Etches of crosses still mark the rocks near where the 20 local fighters had fallen.  A map drawn at that time by the invading Americans referred to the settlement simply as "Joya."

Sometime after the battle, Col. [[Edwin Vose Sumner]], who later served as military governor of New Mexico between 1851 and 1853, sent a Major Gordon with a detachment of infantry to establish a post below the Rio Grande canyon in present Velarde, but no sooner had his men settled in, the unit was ordered to return to Taos.<ref name="books.google" />

Although originally a part of the [[Dixon, New Mexico|San Antonio de Embudo]] parish (based at Dixon),<ref>[http://www.docstoc.com/docs/121971320/DIXON-D-1934-1955 "Dixon Church Records"], St. Anthony Parish 1934 – 1955 Death Records</ref> the village contains a mission church, called the "Iglesia de la Virgen de Guadalupe." Burials sometimes took place here, but mostly were conducted at the Velarde Cemetery.<ref>New Mexico Genealogical Society. [http://www.rootsweb.ancestry.com/~nma/rioarriba/velarde_cemetery.htm "Survey of the Velarde Cemetery, Rio Arriba County"] New Mexico Genealogist, Vol. XXVI, No. 3, September 1987, page 73</ref> Marriages were conducted sometimes at the San Antonio church in Dixon, but more often were held in the Catholic church of San Juan de los Caballeros (present [[Ohkay Owingeh, New Mexico|Ohkay Owingeh]]).

Orchards and vineyards were mentioned in an 1892 news report, but the first commercial apple-growing enterprises emerged only in the late 1920s.<ref name="books.google_a" /> Roadside stands greeted passing travelers when a modern highway was constructed to Taos through the village along the Rio Grande.

Author Rubén Martinez, says of Velarde, where he wrote Desert America, "I might add that I live in one of the poorest villages in one of the poorest counties in one of the poorest states and that the region’s heroin addiction is higher than anywhere else in the country, rural or urban.” <ref>Martinez, Rubén, ‘’ Desert America’’, Metropolitan Books, New York,  2012, p. 103</ref>

==Demographics==

As of the census of 2010, there were 502 people, 222 households, and 136 families residing in the CDP. There were 271 housing units. The racial makeup of the CDP was 61.16% White, 3.59% Native American, 31.27% from other races, and 3.98% from two or more races. Hispanic or Latino of any race were 84.86% of the population.

There were 222 households out of which 25.7% had children under the age of 18 living with them, 42.3% were married couples living together, 12.2% had a female householder with no husband present, 6.8% had a male householder with no wife present, and 38.7% were non-families. 31.5% of all households were made up of individuals living alone (58.6% male, and 41.4% female) and 11.7% had someone living alone who was 65 years of age or older (38.5% male, and 61.5% female). The average household size was 2.26 and the average family size was 2.82.

In the CDP, the population was spread out with 22.1% under the age of 18, 7.8% from 18 to 24, 23.7% from 25 to 44, 30.5% from 45 to 64, and 15.9% who were 65 years of age or older. The median age was 42 years. For every 100 females there were 102 males. For every 100 females age 18 and over, there were 110 males.

The median income for a household in the CDP was $32,917, and the median income for a family was $53,103. Males with full-time year-round income were paid a median of $37,300 versus $9,833 for females. The per capita income for the CDP was $19,502. About 13.8% of the population were below the poverty line, including 40% of those age 65 or over.<ref>[http://factfinder2.census.gov/faces/nav/jsf/pages/index.xhtml "American FactFinder"]. United States Census Bureau. Retrieved 2012-08-23.</ref>

==Education==
Velarde has 1 elementary school which is part of the [[Espanola Public Schools]]
*Velarde Elementary

==Local attractions==
*[[Black Mesa Winery]]

==References==
{{reflist}}

{{Rio Arriba County, New Mexico}}

{{coord|36|09|32|N|105|58|29|W|type:city_region:US-NM_source:GNIS-enwiki|display=title}}

[[Category:Census-designated places in Rio Arriba County, New Mexico]]
[[Category:Census-designated places in New Mexico]]