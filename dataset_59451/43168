__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=M.24
 | image=Macchi_M.24.gif
 | caption=
}}{{Infobox Aircraft Type
 | type=Flying boat bomber
 | national origin=Italy
 | manufacturer=[[Macchi]]
 | designer=
 | first flight=1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Macchi M.24''' was a [[flying boat]] produced in [[Italy]] during the 1920s. Originally intended as a [[bomber]], it was eventually produced for civilian use as well.<ref name="JEA">Taylor 1989, 617</ref><ref name="WAIF">''World Aircraft Information Files'' File 901 Sheet 02</ref> The M.24 resembled a scaled-up version of earlier Macchi flying boat bombers such as the [[Macchi M.9|M.9]] and [[Macchi M.18|M.18]], sharing their [[biplane]] configuration and [[Warren truss]]-style [[interplane strut]]s. However, while these earlier aircraft were single-engine types, the M.24 had twin engines mounted in a [[Push-pull configuration|tractor-pusher]] pair on struts in the interplane gap.<ref name="JEA" /><ref name="WAIF" /> Also like the M.18, it featured an open position in the bow for a gunner, but added a second such position amidships as well.<ref name="JEA" /><ref name="WAIF" />

Two M.24s made a demonstration flight in 1925 from Macchi's home on [[Lake Varese]], crossing the [[Alps]] to [[Amsterdam]], [[Copenhagen]], [[Stockholm]], [[Leningrad]] and home again.<ref name="JEA" /> This feat was followed by [[torpedo]]-launching experiments.<ref name="WAIF" /> The M.24 saw extensive use with the Italian Navy, and several were purchased by the Spanish Navy.<ref name="WAIF" />

A civil version with equal-span wings was developed in 1927 as the '''M.24''bis'''''. This featured an enclosed cabin within the forward hull that could seat eight passengers. ''[[Aero Espresso Italiana]]'' flew these on its [[Brindisi]]-[[Athens]]-[[Constantinople]] route,<ref name="JEA" /> and [[Società Incremento Turistico Aereo Roma|SITAR]] operated them on routes in the Mediterranean as well.<ref name="WAIF" />
<!-- ==Development== -->
<!-- ==Operational history== -->

==Operators==
;{{flag|Kingdom of Italy}}
*[[Regia Marina]]

==Variants==
* '''M.24''' - initial production version - sesquiplane with [[Fiat A.12]] engines
* '''M.24''bis''''' - equal-span version with Lorraine-Dietrich or [[Isotta-Fraschini Asso]] engines, produced in both military and civil subtypes
* '''M.24''ter''''' - military sesquiplane version with Isotta-Fraschini Asso engines
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (M.24''ter'')==
{{aerospecs
|ref=<!-- reference -->''World Aircraft Information Files'' File 901 Sheet 02
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew=3
|capacity=
|length m=14.63
|length ft=48
|length in=0
|span m=22.00
|span ft=72
|span in=2
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.65
|height ft=15
|height in=3
|wing area sqm=
|wing area sqft=
|empty weight kg=3,730
|empty weight lb=8,223
|gross weight kg=5,500
|gross weight lb=12,125
|eng1 number=2
|eng1 type=[[Isotta Fraschini Asso 500]]
|eng1 kw=380
|eng1 hp=510
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|max speed kmh=185
|max speed mph=115
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=700
|range miles=435
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=4,000
|ceiling ft=13,000
|climb rate ms=
|climb rate ftmin=
|armament1=1 × 7.7 mm (.303 in) [[Vickers machine gun]] in open position in bow
|armament2=1 × 7.7 mm (.303 in) Vickers machine gun in open position amidships
|armament3=800 kg (1,874 lb) of bombs
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
* [[List of Interwar military aircraft]]
*[[List of seaplanes and flying boats]]<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Macchi M.24}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
* {{cite book |title=World Aircraft Information Files |publisher=Bright Star Publishing|location=London |pages= }}
<!-- ==External links== -->

{{Macchi aircraft}}

[[Category:Italian bomber aircraft 1920–1929]]
[[Category:Flying boats]]
[[Category:Macchi aircraft|M.24]]
[[Category:Biplanes]]
[[Category:Single-engine aircraft]]
[[Category:Twin-engined push-pull aircraft]]