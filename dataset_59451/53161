{{Italic title}}
{{Infobox Journal
| cover =        [[File:ZGW-Cover.png|200px]]
| editor       = Horst Kämpf
| discipline = [[Geology]], [[biogeology]], [[geochemistry]], [[paleontology]]
| language = [[English language|English]], [[German language|German]]
| website = http://www.zgw-online.de/en/index.php
| publisher = [[Verlag für Geowissenschaften Berlin]]
| country = [[Germany]]
| frequency     = Bimonthly
| abbreviation = Z. geol. Wiss.
| history = 1973-present
| OCLC          = 1791558
| LCCN          = 74-640867
| CODEN         = ZGWSA7
| ISSN = 0303-4534
| eISSN         =
}}
The '''''{{lang|de|Zeitschrift für Geologische Wissenschaften}}''''' ({{lang-en|Journal for the Geological Sciences}}) is a [[Peer review|peer-reviewed]] German [[scientific journal]] established in 1973, that publishes preferably original papers in [[German language|German]] and [[English language|English]] dealing with [[geology]] and related sub-disciplines. Since 2005 the journal has been published bimonthly by the [[Verlag für Geowissenschaften Berlin]]. The journal was formerly an official periodical of the national [[East Germany|East German]] [[Gesellschaft für Geologische Wissenschaften]], the East German geological sciences society, and was published by the scientific and academic publishing house [[Akademie Verlag]]. The journal was regarded as the most important geological sciences periodical in East Germany with an international reputation.<ref>{{cite book|author1 =Ehmke, Gerhard |year=1996 |chapter=Geologische Zeitschriften in Ostdeutschland (1945 bis heute) = Geological Journals in Eastern Germany (1945 to the present) |chapterurl=http://www.geologie.ac.at/filestore/download/BR0035_099_A.pdf |format=[[Portable Document Format|PDF]] |title=Das kulturelle Erbe geowissenschaftlicher und montanwissenschaftlicher Bibliotheken: internationales Symposium, 1993, Freiberg, Sachsen, Deutschland |trans_title==Cultural heritage collected in libraries of geoscience, mining, and metallurgy: international symposium, 1993, Freiberg, Saxony, Germany |editor1=Tillfried Cernajsek |editor2=Lieselotte Jontes |editor3=Peter Schmidt |series=Berichte der Geologischen Bundesanstalt, Bd. 35 |location=Vienna |publisher=[[Geologische Bundesanstalt]] |page=99|oclc=36081427|issn=1017-8880}}</ref><ref>{{cite book |author=Hartmann, Olaf |author2=Martin Guntau |author3=Werner Pälchen  |year=2007 |chapter=Editorial |chapterurl=http://www.wz4s2vtv1.homepage.t-online.de/Schrr16.pdf |format=[[Portable Document Format|PDF]] |editor1=Olaf Hartmann |editor2=Martin Guntau |editor3=Werner Pälchen |title=Zur Geschichte der Geowissenschaften in der DDR |series=Schriftenreihe für Geowissenschaften, No. 16  |location=Ostklüne, Usedom, Germany |publisher=Verlag Störr |pages=7–10|isbn=978-3-937040-15-8 |oclc=240235366 }}</ref> The journal is indexed in: [[GeoRef]], [[Chemical Abstracts Service]], and Geoline.

== Journal contents and structure ==
The journal publishes articles in the following categories:
* ''Articles'', longer scientific reports of national or international interest
* ''Short Communications'' used for rapid publication of preliminary scientific results, finding information, or descriptions of outcrops
* ''Discussions'' on papers previously published in the journal

== References ==
{{reflist}}

{{DEFAULTSORT:Zeitschrift Fur Geologische Wissenschaften}}
[[Category:Geology journals]]
[[Category:Publications established in 1973]]
[[Category:Multilingual journals]]
[[Category:Bimonthly journals]]