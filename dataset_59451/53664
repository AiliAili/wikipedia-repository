{{Infobox Government agency
|agency_name = National Association of State Boards of Accountancy
|nativename = NASBA
|abbreviation = NASBA
|seal            = 
|seal_width      = 140px
|seal_caption    =
|chief1_name = Ken Bishop
|chief1_position = ''President and Chief Executive Officer''
|chief2_name = Colleen Conrad, CPA
|chief2_position = ''Executive Vice President and Chief Operating Officer''
|chief3_name = Michael Bryant, CPA
|chief3_position = ''Senior Vice President and Chief Financial Officer''
|chief4_name = Ed Barnicott
|chief4_position = ''Vice President of Strategic Planning and Program Management''
|chief5_name = Dan Dustin, CPA
|chief5_position = ''Vice President of State Board Relations''
|chief6_name = Alfonzo Alexander
|chief6_position = ''Chief Relationship Officer and NASBA Center for the Public Trust President''
|chief7_name = Maria-Lisa Caldwell, Esq.
|chief7_position = ''Chief Legal Officer and Director of Compliance Service''
|chief8_name = Louise Dratler Haberman
|chief8_position = ''Vice President of Information & Research''
|chief9_name = Cheryl Farrar
|chief9_position = ''Chief Information Officer''
|budget          =
|website         = [https://www.nasba.org/ Official Website]
|footnotes       =
}}

The '''National Association of State Boards of Accountancy''' (NASBA) is an association dedicated to serving the 55 state boards of accountancy. These are the boards that regulate the accountancy profession in the [[United States of America]].

There is one board for each of the 50 states, plus the [[District of Columbia]], [[Puerto Rico]], [[U.S. Virgin Islands]], [[Guam]] and the [[Northern Mariana Islands]].

==Structure of the U.S. accounting profession==
In the United States, the designation of [[Certified Public Accountant]] (CPA) is granted at state level. Individual CPAs are not required to belong to the [[American Institute of Certified Public Accountants]] (AICPA), although many do.

NASBA acts primarily as a forum for the state boards themselves, as opposed to AICPA which represents CPAs as individuals.

==Role of NASBA==
NASBA's primary role is to:

* Act as a forum for state boards to discuss issues of common concern
* Encourage reciprocal recognition of the CPA qualification between states
* Enable state boards to speak with one voice in dealing with AICPA, the Federal Government, and other stakeholders

NASBA is a member of the [[International Federation of Accountants]].

==Uniform CPA Examination==
Responsibility for the [[Uniform Certified Public Accountant Examination]] is shared between state boards of accountancy, the AICPA and NASBA:

* State boards of accountancy are responsible for assessing eligibility of candidates to sit for the CPA examination. Boards are also the final authority on communicating exam results received from NASBA to candidates.
* The AICPA is responsible for setting and scoring the examination, and transmitting scores to NASBA.
* NASBA maintains the National Candidate Database and matches score data received from the AICPA with candidate details. Most states offer online score reporting on NASBA's website at www.nasba.org. NASBA also maintains records for those who have passed the exam.

The AICPA and NASBA also coordinate and maintain ''mutual recognition agreements'' with foreign accountancy institutes. As of January 2012, the only countries with such agreements are [[Australia]], [[Canada]], [[Hong Kong]], [[Ireland]], [[Mexico]] and [[New Zealand]]. Accountants from these countries who meet the specified criteria may be able to sit for the [[International Qualification Examination]] (IQEX) as an alternative to the Uniform CPA Exam. IQEX is also jointly administered by the AICPA and NASBA; however, state boards are not involved at the examination stage (only at licensure).

== Boards of Accountancy ==

List of Boards of Accountancy<ref>{{Cite web|url=https://nasba.org/stateboards/|title=Boards of Accountancy  {{!}}  NASBA|last=|first=|date=|website=nasba.org|publisher=|language=en-US|access-date=2017-01-28}}</ref>
* Alabama State Board of Public Accountancy<ref>{{Cite web|url=http://www.asbpa.alabama.gov/|title=Alabama State Board of Public Accountancy|website=www.asbpa.alabama.gov|access-date=2017-01-28}}</ref><ref>{{Cite web|url=http://www.asbpa.alabama.gov/laws.aspx|title=Alabama State Board of Public Accountancy|website=www.asbpa.alabama.gov|access-date=2017-01-28}}</ref>
* Alaska Board of Public Accountancy<ref>{{Cite web|url=https://www.commerce.alaska.gov/web/cbpl/ProfessionalLicensing/BoardofPublicAccountancy.aspx|title=Board of Public Accountancy, Professional Licensing, Division of Corporations, Business and Professional Licensing|website=www.commerce.alaska.gov|language=en-US|access-date=2017-01-28}}</ref><ref>{{Cite web|url=https://www.commerce.alaska.gov/web/cbpl/ProfessionalLicensing/BoardofPublicAccountancy/StatutesRegulations.aspx|title=Statutes & Regulations, Board of Public Accountancy, Professional Licensing, Division of Corporations, Business and Professional Licensing|website=www.commerce.alaska.gov|language=en-US|access-date=2017-01-28}}</ref>
* Arizona State Board of Accountancy<ref>{{Cite web|url=http://www.azaccountancy.gov/|title=Arizona Board of Accountancy|last=Sietz|first=Richard|website=www.azaccountancy.gov|language=en|access-date=2017-01-28}}</ref><ref>{{Cite web|url=http://www.azaccountancy.gov/AboutUs/ArizonaRevisedStatutes.aspx|title=Arizona Revised Statutes|last=Sietz|first=Richard|website=www.azaccountancy.gov|language=en|access-date=2017-01-28}}</ref>
* Arkansas State Board of Public Accountancy
* California Board of Accountancy<ref>{{Cite web|url=http://www.dca.ca.gov/cba/index.shtml|title=Board of Accountancy|last=California|first=State of|website=www.dca.ca.gov|language=en|access-date=2017-01-28}}</ref><ref>{{Cite web|url=http://www.dca.ca.gov/cba/about-cba/laws-and-rules.shtml|title=CBA-Laws and Rules|last=California|first=State of|website=www.dca.ca.gov|language=en|access-date=2017-01-28}}</ref>
* Commonwealth of the Northern Mariana Islands
* Colorado State Board of Accountancy
* Connecticut State Board of Accountancy
* Delaware State Board of Accountancy
* District of Columbia Board of Accountancy
* Florida Board of Accountancy
* Georgia State Board of Accountancy
* Guam Board of Accountancy
* Hawaii Board of Public Accountancy
* Idaho State Board of Accountancy
* Illinois Board of Examiners
* Illinois Department of Financial And Professional Regulation Public Accountancy Section
* Indiana Board of Accountancy
* Iowa Accountancy Examining Board
* Kansas Board of Accountancy
* Kentucky Board of Accountancy
* State Board of CPAs of Louisiana
* Maine Board of Accountancy
* Maryland Board of Public Accountancy
* Massachusetts Board of Public Accountancy
* Michigan State Board of Accountancy
* Minnesota State Board of Accountancy
* Mississippi State Board of Public Accountancy
* Missouri State Board of Accountancy
* Montana Board of Public Accountants
* Nebraska State Board of Public Accountancy
* Nevada State Board of Accountancy
* New Hampshire Board of Accountancy
* New Jersey State Board of Accountancy
* New Mexico Public Accountancy Board
* New York State Board for Public Accountancy
* North Carolina State Board of CPA Examiners
* North Dakota State Board of Accountancy
* Accountancy Board of Ohio
* Oklahoma Accountancy Board
* Oregon Board of Accountancy
* Pennsylvania State Board of Accountancy
* Puerto Rico Board of Accountancy
* Rhode Island Board of Accountancy
* South Carolina Board of Accountancy
* South Dakota Board of Accountancy
* Tennessee State Board of Accountancy
* Texas State Board of Public Accountancy
* Utah Board of Accountancy
* Vermont Board of Public Accountancy
* Virgin Islands Board of Public Accountancy
* Virginia Board of Accountancy
* Washington State Board of Accountancy
* West Virginia Board of Accountancy
* Wisconsin Accounting Examining Board
* Wyoming Board of Certified Public Accountants

==See also==
* [[American Institute of Certified Public Accountants]]
* [[Florida Board of Accountancy]]
* [[Florida Institute of CPAs]]

==References==
{{Reflist}}

==External links==
* [http://www.nasba.org National Association of State Boards of Accountancy]

[[Category:Accounting organizations]]
[[Category:Accounting in the United States]]