{{Infobox journal
| title         = Contemporary Jewry
| cover         = 
| editor        = Harriet Hartman
| discipline    = Sociology, social sciences
| former_names  = Jewish Sociology and Social Research
| abbreviation  = 
| publisher     = Springer, Association for the Social Scientific Study of Jewry
| country       = United States
| frequency     = Three issues per year
| history       = 
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 1977-present
| website       = 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 61124234
| LCCN          = 
| CODEN         = 
| ISSN          = 0147-1694
| eISSN         = 
}}

'''''Contemporary Jewry''''' is a [[peer reviewed]] [[academic journal]] published by the [[Association for the Social Scientific Study of Jewry]].<ref>[http://www.csa.com/factsheets/supplements/paispeer.php "PAIS International Peer Reviewed Journals List."] ''PAIS International''. 2009. Accessed May 13, 2014.</ref>

The journal mostly publishes articles on the subject of the [[sociology of Jewry]], however, articles on Jews and Judaism based on other social sciences as well as history are published as well.<ref>[http://www.contemporaryjewry.org/index.php/journal "Journal."] ''Association for the Social Scientific Study of Jewry''. 2014. Accessed May 13, 2014.</ref><ref>[http://www.springer.com/social+sciences/religious+studies/journal/12397 "Contemporary Jewry."] ''Springer''. Accessed May 13, 2014.</ref>

==History==
''Contemporary Jewry'' was initially published semiannually from 1977-1985. From 1986-2008, the journal published annually.<ref>[http://www.trove.nla.gov.au/work/11495960?selectedversion=NBD1070519 "1977-2013, English, Periodical, Journal, magazine, other edition: Contemporary Jewry."] ''Trove''. National Library of Australia. Accessed May 13, 2014.</ref> Since 2009, the journal has published three issues per year. The publication was originally titled ''Jewish Sociology and Social Research''.<ref>Dashefsky, Arnold M. [http://www.hirr.hartsem.edu/ency/ASSJ.htm "ASSOCIATION FOR THE SOCIAL SCIENTIFIC STUDY OF JEWRY (ASSJ)."] {{webarchive |url=https://web.archive.org/web/20131011220501/http://www.hirr.hartsem.edu/ency/ASSJ.htm |date=October 11, 2013 }} ''Encyclopedia of Religion and Society''. Ed. William H. Swatos, Jr. Hartford Institute for Religion Research. Accessed May 12, 2014.</ref>

==References==
{{reflist}}

==External links==
*[http://www.contemporaryjewry.org/index.php/journal ''Contemporary Jewry'' homepage]

{{Portal bar|Judaism|Sociology}}

[[Category:English-language journals]]
[[Category:Annual journals]]
[[Category:Sociology journals]]
[[Category:Academic journals associated with learned and professional societies]]