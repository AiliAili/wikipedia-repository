{{Infobox person
| honorific_prefix          =
| name                      = Beth Doane
| honorific_suffix          =
| image                     = BethDoaneTedx.jpg
| image_size                =
| alt                       = Beth Doane
| caption                   =
| birth_name                =
| birth_date                = {{Birth date and age|1983|07|20}} <!-- {{Birth date|YYYY|MM|DD}} -->
| birth_place               =
| disappeared_date          = <!-- {{Disappeared date and age|YYYY|MM|DD|YYYY|MM|DD}} (disappeared date then birth date) -->
| disappeared_place         =
| disappeared_status        =
| death_date                = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place               =
| death_cause               =
| body_discovered           =
| resting_place             =
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|display=inline}} -->
| monuments                 =
| nationality               = [[United States|American]]
| other_names               =
| alma_mater                =
| occupation                = Writer, Speaker, Advocate
| years_active              =
| employer                  =
| organization              = Main and Rose, [[Raintees]]
| home_town                 = [[Whitehouse, Ohio|Whitehouse]], [[Ohio]], [[United States]]
| salary                    =
| net_worth                 =
| height                    =
| weight                    =
| spouse                    =
| partner                   =
| children                  =
| parents                   =
| relatives                 =
| awards                    =
| website                   = www.bethdoane.com
| signature                 =
| signature_size            =
| signature_alt             =
| ethnicity                 =
| religion                  =
| denomination              =
| box_width                 =
}}

'''Beth Doane''' (born July 20, 1983) is an American writer, speaker and advocate.<ref>{{Cite web|url=http://www.huffingtonpost.com/2012/05/11/beth-doane-activist_n_1507415.html|title=How A Young Entrepreneur Is Helping To Save The Rainforest|last=|first=|date=|website=The Huffington Post|publisher=|access-date=}}</ref> She grew up on a small farm in [[Whitehouse, Ohio]]. She attended [[Eastern Michigan University]] from 2001 to 2004 and graduated magna cum laude.

== Career ==

=== Andira International ===

At age 22, after living across Europe, Beth created Andira International as a fashion import and distribution company.<ref>{{Cite news|url=http://www.boemagazine.com/2013/07/entrepreneur-beth-doane-talks-fashion-philanthropy/|title=Entrepreneur Beth Doane Talks Fashion Philanthropy|last=|first=|date=|work=|newspaper=BOE Magazine|language=en-US|access-date=|via=}}</ref><ref>{{Cite web|url=http://blindfoldmag.com/view/1574ba3a920f69/Beth_Doane:_Changing_the_World_Through_Education,_Reforestation_and_Ethical_Production/|title=Beth Doane: Changing the World Through Education, Reforestation and Ethical Production|last=|first=|date=|website=Blindfold Magazine|publisher=|access-date=}}</ref> She launched exclusive European brands, such as Demano, across the U.S. market and debuted Demano at [[Los Angeles Fashion Week|Mercedes-Benz Fashion Week]] in Los Angeles in 2008. 

=== Raintees ===

After witnessing human rights violations and the prevalence of child labor and environmental pollution in fashion manufacturing, Beth began to speak out about what she was experiencing and encourage industry leaders to take action.

In 2008, she created the apparel line, Raintees, which features children's artwork.<ref>{{Cite news|url=http://business.financialpost.com/entrepreneur/fp-startups/15-successful-entrepreneurs-share-the-most-important-lesson-they-learned-in-their-20s|title=15 successful entrepreneurs share the most important lesson they learned in their 20s|last=|first=|date=|work=|newspaper=Financial Post|language=en|access-date=|via=}}</ref> Raintees launched in the [[Fall 2008 fashion weeks]] and has been featured in prominent media, including [[People (magazine)|''People'']], [[Glamour (magazine)|''Glamour'']], ''[[The Huffington Post]]'' and [[Seventeen (magazine)|''Seventeen'']].[[Beth Doane#cite note-1|<sup>[1]</sup>]] Beth was featured as a "Hero" in the December 2010 edition of ''[[Hemispheres (magazine)|Hemispheres]] Magazine'' for her environmental achievements through the brand and photographed for the book 100 Making a Difference by celebrity photographer John Russo, alongside public figures such as [[Steven Spielberg]], [[Prince Edward, Earl of Wessex|Prince Edward]], [[Michelle Obama]] and [[Al Gore]].<ref>[http://www.hemispheresmagazine.com/2010/12/01/high-tee/ “High Tee”]</ref><ref>[http://100makingadifference.com/ "100 Making A Difference"]</ref>

Celebrity fans and contributors to Raintees include actresses [[Alicia Silverstone]],<ref>[http://www.thekindlife.com/post/my-interview-with-rain-tees "my interview with rain tees"]</ref> [[Rosario Dawson]],<ref>[http://www.raintees.com/share/whos-saving-trees "Who's Saving Trees: Rosario Dawson"] {{webarchive|url=https://web.archive.org/web/20120203045518/http://www.raintees.com/share/whos-saving-trees|date=February 3, 2012}}</ref> [[Jenna Dewan]],<ref>[http://www.raintees.com/share/whos-saving-trees "Who's Saving Raintees: Jenna Dewan"] {{webarchive|url=https://web.archive.org/web/20120203045518/http://www.raintees.com/share/whos-saving-trees|date=February 3, 2012}}</ref> [[Amanda Peet]],<ref>[http://www.raintees.com/share/whos-saving-trees "Who's Saving Raintees: Amanda Peet"] {{webarchive|url=https://web.archive.org/web/20120203045518/http://www.raintees.com/share/whos-saving-trees|date=February 3, 2012}}</ref> Latin [[Celebrity|pop star]] [[Debi Nova]],<ref>[http://wn.com/Debi_Nova_talks_Rain_Tees "Debi Nova Talks Rain Tees"]</ref><ref>[http://www.raintees.com/about/contributors/debi-nova/ "Raintees Contributors: Debi Nova"] {{webarchive|url=https://web.archive.org/web/20110113023937/http://www.raintees.com/about/contributors/debi-nova/|date=January 13, 2011}}</ref> supermodels [[Josie Maran]] and [[Kate Dillon Levin]],<ref>[http://www.raintees.com/about/contributors/kate-dillon/ "Raintees Contributors: Kate Dillon"] {{webarchive|url=https://web.archive.org/web/20110113024003/http://www.raintees.com/about/contributors/kate-dillon/|date=January 13, 2011}}</ref><ref>[http://blog.raintees.com/2012/02/rain-tees-exclusive-with-josie-maran/ "Raintees Exclusive with Josie Maran"] {{webarchive|url=https://web.archive.org/web/20120418100911/http://blog.raintees.com/2012/02/rain-tees-exclusive-with-josie-maran/|date=April 18, 2012}}</ref> and more.

== Writer/Author ==

Beth is the author of a children’s book entitled ''From the Jungle'', illustrated by acclaimed artist and award-winning graphic designer [[Mel Lim]]. The book won an American Graphic Design Award (GDUSA) in 2010.<ref>[http://www.mellim.com/2010/09/11/mel-lim-design-celebrating-7th-year-anniversary-with-gd-usa-awards-2010/ "Mel Lim Design celebrating 7th year anniversary with GD USA Awards 2010"]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

Beth is a contributing writer for ''[[Forbes]]'',<ref>{{Cite news|url=http://www.forbes.com/sites/yec/2017/01/17/whiskey-business-five-lessons-you-can-learn-from-the-industry/#59b21199343c|title=Whiskey Business: Five Lessons You Can Learn From The Industry|last=|first=|date=|work=|newspaper=Forbes|access-date=|via=}}</ref><ref>{{Cite news|url=http://www.forbes.com/sites/yec/2016/12/27/is-the-future-still-female/#3b2ab46410a9|title=Is The Future Still Female?|last=|first=|date=|work=|newspaper=Forbes|access-date=|via=}}</ref> ''[[Darling (magazine)|Darling Magazine]]'',<ref>{{Cite web|url=http://darlingmagazine.org/author/beth-doane/|title=Beth Doane, Author at Darling Magazine|last=|first=|date=|website=Darling Magazine|publisher=|language=en-US|access-date=}}</ref> ''[[The Huffington Post]]''<ref>{{Cite web|url=http://www.huffingtonpost.com/yasamin-beitollahi/entrepreneurs-funding-glo_b_6888414.html|title=Entrepreneurs Funding Global Change|last=|first=|last2=|first2=|date=|website=The Huffington Post|publisher=|access-date=|last3=Speaker|first3=Public|last4=Equality|first4=Passionate about Gender}}</ref> and Beth has been featured in ''[[Business Insider]]'',<ref>{{Cite news|url=http://www.businessinsider.com/best-lessons-entrepreneurs-learned-in-their-20s-2015-1|title=20 successful entrepreneurs share the most important lesson they learned in their 20s|last=|first=|date=|work=|newspaper=Business Insider|language=en|access-date=|via=}}</ref> [[National Geographic (magazine)|''National Geographic'']],<ref>{{Cite web|url=http://newswatch.nationalgeographic.com/2012/02/24/interview-with-eco-chic-expert-beth-doane/|title=Interview with Eco-Chic Expert–Beth Doane|last=Schaul|first=Jordan Carlton|date=|website=National Geographic Society|publisher=|access-date=}}</ref><ref>{{Cite web|url=http://newswatch.nationalgeographic.com/2012/04/18/an-eco-friendly-fashion-statement-an-interview-with-fashion-designer-beth-doane/|title=An Eco-Friendly Fashion Statement–An Interview With Fashion Designer Beth Doane|last=Schaul|first=|date=|website=National Geographic Society|publisher=|access-date=}}</ref> [[Glamour (magazine)|''Glamour'']], ''[[In Style]]'', and other top tier magazines. 

In 2010, she was asked to write as an ambassador for the L'Oreal Company [[Kiehl's]] dedicated global following on her journey through the Amazon for the launch of their new naturally based skin-care product, Kiehl's Midnight Recovery Concentrate.<ref>[http://blog.raintees.com/2010/05/rain-tees-for-kiehls/ “Rain Tees for Kiehl’s”] {{webarchive |url=https://web.archive.org/web/20110715151449/http://blog.raintees.com/2010/05/rain-tees-for-kiehls/ |date=July 15, 2011 }}</ref>

== Speaker and Consultant ==

Beth is mangaging partner of the communications firm Main & Rose and speaks nationally about her journey as an entrepreneur and branding expert.<ref>{{Cite news|url=https://www.entrepreneur.com/article/274275|title=8 Entrepreneurs Making a Fortune -- and Giving it Back|last=Brustein|first=Darrah|date=|work=|newspaper=Entrepreneur|language=en|access-date=|via=}}</ref><ref>{{Cite news|url=http://www.inc.com/ilya-pozin/9-branding-experts-to-look-out-for-in-2017.html|title=9 Branding Experts to Look Out For In 2017|last=|first=|date=|work=|newspaper=Inc.com|language=en|access-date=|via=}}</ref> She has spoken at the United Nations and on the National Mall in Washington, DC for Earth Day Network's annual event in 2012 alongside celebrities, politicians and scientists about climate change.<ref>[http://www.huffingtonpost.com/starre-vartan/chevrons-terrestrial-oil_b_566773.html “Huffington Post: Chevron’s Terrestrial Oil Spill - Less Media, More Insidious Than Gulf Slick”]</ref><ref>[http://blog.raintees.com/2012/05/rain-tees-beth-doane-earth-day-1632/ "Raintees Founder Beth Doane Attends Earth Day"] {{webarchive |url=https://web.archive.org/web/20120626042748/http://blog.raintees.com/2012/05/rain-tees-beth-doane-earth-day-1632/ |date=June 26, 2012 }}</ref>

She has also spoken at Google, TEDx, and at the inaugural Leaders Causing Leaders leadership conference held in Long Beach, California in November, 2010, where she founded a women’s panel on executive leadership.<ref>[http://www.leaderscausingleaders.com/wordpress2/mission/1427 “Leaders Causing Leaders: Beth Doane”]</ref> 

== Awards ==

In 2011, Beth was nominated  “Outstanding Women of the Year Nominee” by Los Angeles magazine,<ref>[http://www.lamag.com/lawomen/nominate/View.aspx?ID=260807 "L.A. Women"] {{webarchive |url=https://web.archive.org/web/20131022000652/http://www.lamag.com/lawomen/nominate/View.aspx?ID=260807 |date=October 22, 2013 }}</ref> a “Gold Alumni” of Eastern Michigan University and awarded the “Outstanding Young Alumna Award." In 2009, Beth received the Toledo area 20 Under 40 Leadership Award.
== References ==
{{Reflist}}

== External links ==
* [https://web.archive.org/web/20110208170948/http://andiraintl.com/ Andira International Official Website]

{{DEFAULTSORT:Doane, Beth}}
[[Category:Living people]]
[[Category:American women writers]]
[[Category:1983 births]]