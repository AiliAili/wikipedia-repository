{{about||adaptations of the novel|The Old Curiosity Shop (disambiguation)|the Seattle business|Ye Olde Curiosity Shop}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = The Old Curiosity Shop
| title_orig     =
| translator     =
| image          = Masterclock serial cover.jpg
| caption        = Cover, serial of [[Master Humphrey's Clock]], 1840
| author         = [[Charles Dickens]]
| illustrator    = [[George Cattermole]]<br/>[[Hablot Knight Browne]] ([[Phiz]])<br/>[[Samuel Williams (illustrator)|Samuel Williams]]<br/>[[Daniel Maclise]]
| cover_artist   = [[George Cattermole]]
| country        = England
| language       = English
| genre          = [[Novel]]
| publisher      = [[Chapman & Hall]] London
| published  = Serialised April 1840 – November 1841; book format 1841
| media_type     = Print 
| pages          =
| isbn           =
| dewey=
| congress=
| oclc=
| preceded_by    =
| followed_by    =
}}

'''''The Old Curiosity Shop''''' is a novel by [[Charles Dickens]]. The plot follows the life of Nell Trent and her grandfather, both residents of The Old Curiosity Shop in London.

''The Old Curiosity Shop'' was one of two novels (the other being ''[[Barnaby Rudge]]'') which Dickens published along with short stories in his weekly serial ''[[Master Humphrey's Clock]]'', which lasted from 1840 to 1841. It was so popular that New York readers stormed the wharf when the ship bearing the final instalment arrived in 1841.<ref>{{cite web|last=Garber|first=Megan|title=Serial Thriller|url=https://www.theatlantic.com/magazine/archive/2013/03/serial-thriller/309235/|work=The Atlantic|publisher=[[The Atlantic Media Company]]|accessdate=16 June 2013}}</ref> ''The Old Curiosity Shop'' was printed in book form in 1841.

[[Queen Victoria]] read the novel in 1841, finding it "very interesting and cleverly written."<ref>{{cite web |url=http://www.queenvictoriasjournals.org/search/filter.do?grouping=pq_index_person_nav&groupName=Dickens%2c+Charles+%281812-1870%29&sequence=0&QueryType=articles&ResultsID=2738809599926 |title=Queen Victoria's Journals |work=Princess Beatrice's Copies  |date=5 March 1841 | publisher=RA VIC/MAIN/QVJ (W) |accessdate=24 May 2013}}</ref>

==Plot summary==
''The Old Curiosity Shop'' tells the story of Nell Trent, a beautiful and virtuous young girl of "not quite fourteen". An orphan, she lives with her maternal grandfather (whose name is never revealed) in his shop of odds and ends. Her grandfather loves her dearly, and Nell does not complain, but she lives a lonely existence with almost no friends her own age. Her only friend is Kit, an honest boy employed at the shop, whom she is teaching to write. Secretly obsessed with ensuring that Nell does not die in poverty as her parents did, her grandfather attempts to provide Nell with a good inheritance through gambling at cards. He keeps his nocturnal games a secret, but borrows heavily from the evil [[Quilp|Daniel Quilp]], a malicious, grotesquely deformed, hunchbacked dwarf moneylender. In the end, he gambles away what little money they have, and Quilp seizes the opportunity to take possession of the shop and evict Nell and her grandfather. Her grandfather suffers a [[nervous breakdown|breakdown]] that leaves him bereft of his wits, and Nell takes him away to the [[English Midlands|Midlands]] of England, to live as beggars.

Convinced that the old man has stored up a large and prosperous fortune for Nell, her wastrel older brother, Frederick, convinces the good-natured but easily led Dick Swiveller to help him track Nell down, so that Swiveller can marry Nell and share her supposed inheritance with Frederick. To this end, they join forces with Quilp, who knows full well that there is no fortune, but sadistically chooses to 'help' them to enjoy the misery it will inflict on all concerned. Quilp begins to try to track Nell down, but the fugitives are not easily discovered. To keep Dick Swiveller under his eye, Quilp arranges for him to be taken as a clerk by Quilp's lawyer, [[Sampson Brass|Mr. Brass]]. At the Brass firm, Dick befriends the mistreated maidservant and nicknames her 'the [[Marchioness]]'. Nell, having fallen in with a number of characters, some villainous and some kind, succeeds in leading her grandfather to safety in a far-off village (identified by Dickens as [[Tong, Shropshire]]), but this comes at a considerable cost to Nell's health.

Meanwhile, Kit, having lost his job at the curiosity shop, has found new employment with the kind Mr and Mrs Garland. Here he is contacted by a mysterious 'single gentleman' who is looking for news of Nell and her grandfather. The 'single gentleman' and Kit's mother go after them unsuccessfully, and encounter Quilp, who is also hunting for the runaways. Quilp forms a grudge against Kit and has him framed as a thief. Kit is sentenced to [[penal transportation|transportation]]. However, Dick Swiveller proves Kit's innocence with the help of his friend the Marchioness. Quilp is hunted down and dies trying to escape his pursuers. At the same time, a coincidence leads Mr Garland to knowledge of Nell's whereabouts, and he, Kit, and the single gentleman (who turns out to be the younger brother of Nell's grandfather) go to find her. Sadly, by the time they arrive, Nell has died as a result of her arduous journey. Her grandfather, already mentally infirm, refuses to admit she is dead and sits every day by her grave waiting for her to come back until, a few months later, he dies himself.

=== Background ===

The events of the book seem to take place around 1825. In Chapter 29 Miss Monflathers refers to the death of [[George Gordon Byron, 6th Baron Byron|Lord Byron]], who died on 19 April 1824. When the inquest rules (incorrectly) that [[Quilp]] committed suicide, his corpse is ordered to be [[burial at cross-roads|buried at a crossroads]] with a stake through the heart, a practice banned in 1823.<ref>http://www.historyextra.com/qa/end-road</ref> Nell's grandfather, after his breakdown, fears that he shall be sent to a madhouse, and there chained to a wall and whipped; these practices went out of use after about 1830.{{citation needed|date=June 2014}} In Chapter 13, the lawyer Mr. Brass is described as "one of '''Her''' Majesty's attornies" {{sic}}, putting him in the reign of [[Queen Victoria]], which began in 1837, but given all the other evidence, and the fact that Kit, at his trial, is charged with acting "against the peace of our Sovereign Lord the King" (referring to [[George IV of the United Kingdom|George IV]]), this must be a slip of the pen.

==Framing device==

''[[Master Humphrey's Clock]]'' was a weekly serial that contained both short stories and two novels (''The Old Curiosity Shop'' and ''Barnaby Rudge''). Some of the short stories act as [[frame story|frame stories]] to the novels.

Originally the conceit of the story was that [[Master Humphrey]] was reading it aloud to a group of his friends, gathered at his house around the grandfather clock in which he eccentrically kept his manuscripts. Consequently, when the novel begins, it is told in the first person, with Master Humphrey as the narrator. However, Dickens soon changed his mind about how best to tell the story, and abandoned the first-person narrator after chapter three. Once the novel was ended, ''Master Humphrey's Clock'' added a concluding scene, where Master Humphrey's friends (after he has finished reading the novel to them) complain that the 'single gentleman' is never given a name; Master Humphrey tells them that the novel was a true story, that the 'single gentleman' was in fact Master Humphrey himself, and that the events of the first three chapters were fictitious, intended only to introduce the characters. This was Dickens' [[retroactive continuity|after-the-fact explanation]] of why the narrator disappeared and why (if he was their near relation) he gave no sign in the first three chapters of knowing who they were. It is a clumsy device, and at least one editor thinks "it need not be taken seriously."<ref>The editor of the Penguin Classic Books edition.</ref>

Dickens's original artistic intent was to keep the short stories and the novels together, and the short stories and the novels were published in 1840 in three bound volumes under the title ''Master Humphrey's Clock'', which retains the original full and correct ordering of texts. However, Dickens himself cancelled ''Master Humphrey's Clock'' before 1848, and describes in a preface to ''The Old Curiosity Shop'' that he wishes the story to not be tied down to the miscellany within which it began.<ref>Charles Dickens – Preface to ''The Old Curiosity Shop'', 1848 Cheap Edition</ref> Most later anthologies published the short stories and the novels separately.

==Characters in ''The Old Curiosity Shop''==

===Major characters===
[[File:The Old Curiosity Shop At Rest.jpg|upright|thumb|right|"At Rest" Illustration by George Cattermole]]

*'''Nell Trent''', the novel's main character. Portrayed as infallibly good and angelic, she leads her grandfather on their journey to save them from misery. She gradually becomes weaker throughout the journey, and although she finds a home with the help of the schoolmaster, she sickens and dies before her friends in London find her.
*'''Nell's grandfather''', Nell's guardian. After losing both his wife and daughter, he sees Nell as the embodiment of their good spirits. His grandson Fred is seen as the successor to his son-in-law, who he felt unworthy of his daughter. As such, he shows him no affection. He is paranoid about falling into poverty and gambles to try to stave that off; as his money runs out, he turns to Quilp for loans to continue to furnish for Nell the life he feels she deserves. After believing Kit has revealed his secret addiction he falls ill and is mentally unstable afterwards. Nell then protects him as he had done for her. Although he knows Nell is dead he refuses to acknowledge it and does not recognise his brother whom he had protected in their childhood. He dies soon after Nell, and is buried beside her.
*'''Christopher 'Kit' Nubbles''', Nell's friend and servant. He watches out for Nell when she is left in the shop alone at night (although she doesn't know he's there) and will 'never come home to his bed until he thinks she's safe in hers'. After Quilp takes over the shop, he offers him a place in his house. His mother is concerned about his attachment to Nell, and at one point jokes, 'some people would say that you'd fallen in love with her', at which Kit becomes very bashful and tries to change the subject. He is later given a position at the Garlands' house, and becomes an important member of their household. His dedication to his family earns him the respect of many characters, and the resentment of Quilp. He is framed for robbery, but is later released and joins the party traveling to recover Nell.
[[File:Quilp by Kyd 1889.jpg|160px|thumb|right|Quilp by [[Joseph Clayton Clarke|'Kyd']] (1889)]]
*'''[[Quilp|Daniel Quilp]]''', the novel's primary villain. He mistreats his wife, Betsy, and manipulates others to his own ends through a false charm he has developed over the years. He lends money to Nell's grandfather and takes possession of the curiosity shop during the old man's illness (which he had caused by revealing his knowledge of the old man's bad gambling habit). He uses sarcasm to belittle those he wishes to control, most notably his wife, and takes a sadistic delight in the suffering of others. He eavesdrops so as to know all of 'the old man's' most private thoughts, and teases him, saying 'you have no secrets from me now'. He also drives a wedge between Kit and the old man (and as a result between Kit and Nell) by pretending it was Kit who told him about the gambling.
*'''Richard 'Dick' Swiveller''', in turn, Frederick Trent's manipulated friend; Sampson Brass's clerk; and the Marchioness' guardian and eventual husband. He delights in quoting and adapting literature to describe his experiences. He is very laid-back and doesn't seem to worry about anything, despite the fact that he owes money to just about everybody. Following Fred's departure from the story, he becomes more independent and eventually is seen as a strong force for good, securing Kit's release from prison and the Marchioness' future. His transformation from an idle and vacant youth to a key helpmate bridges the depiction of the main characters that are either mostly villainous or goodly in nature.
*The '''single gentleman''', who is never named, is the estranged younger brother of Nell's grandfather. He leads the search for the travelers after taking lodging in Sampson Brass's rooms and befriending Dick, Kit and the Garlands.

===Other characters===

*'''Mrs. Betsy Quilp''', Quilp's mistreated wife. She is mortally afraid of her husband, but appears to love him in spite of everything, as she was genuinely worried when he disappeared for a long period.
*'''[[Sampson Brass | Mr. Sampson Brass]]''', an attorney (what would now be called a solicitor) of the Court of the King's Bench. A grovelling, obsequious man, he is an employee of Mr. Quilp, at whose urging he frames Kit for robbery.
*'''Miss Sarah ('Sally') Brass''', Mr. Brass's obnoxious sister and clerk. She is the real authority in the Brass firm. She is occasionally referred to as a "dragon", and she mistreats the Marchioness. Quilp makes amorous advances towards her, but is rebuffed.
*'''Mrs. Jarley''', proprietor of a travelling waxworks show, who takes in Nell and her grandfather out of kindness. However, she only appears briefly.
*'''Frederick Trent''', Nell's worthless older brother, who is convinced that his grandfather is secretly wealthy (when in actuality he was the primary cause of the old man's poverty, according to the single gentleman). Initially a major character in the novel and highly influential over Richard Swiveller, he is dropped from the narrative after chapter 23. Briefly mentioned as travelling to Great Britain and the wider world following his disappearance from the story, before being found injured and drowned in the [[Seine|River Seine]] after the story's conclusion. The character was named after the novelist's younger brother, [[Frederick Dickens]].<ref>[[Peter Ackroyd]] 'Dickens' Published by [[Sinclair-Stevenson]] (1990) pg 266</ref>
*'''Mr. Garland''', a kind-hearted man, father of Abel Garland and employer of Kit.
*The '''small servant''', Miss Brass's maidservant. Dick Swiveller befriends her and, finding that she does not know her age or name (Sally Brass simply refers to her as "Little Devil") or parents, nicknames her '''The Marchioness''' and later gives her the name Sophronia Sphynx. In the original manuscript it is made explicit that the Marchioness is in fact the illegitimate daughter of Miss Brass, possibly by Quilp, but only a suggestion of this survived in the published edition.
*'''Isaac List and Joe Jowl''', professional gamblers. They are fellow guests at the public house where Nell and her grandfather, unable to get home, pass a stormy night. Nell's grandfather is unable to resist gambling with them, and fleeces Nell of what little money she has to this end. That same night, he also robs her of even more money.
*'''Mr. Chuckster''', the [[dogsbody]] of the notary Mr. Witherden, who employs Mr. Abel Garland. He takes a strong dislike to Kit after Mr. Garland overpays Kit for a job and Kit returns to work off the difference; he shows his dislike at every opportunity, calling Kit 'Snobby'.
*'''Mr. Marton''', a poor schoolmaster. He befriends Nell and later inadvertently meets her and her grandfather on the roads. Nell approaches him to beg for alms, not realising who he is. She faints from a combination of shock and exhaustion, and, realising she is ill, he takes her to an inn and pays for the doctor, and then takes her and her grandfather to live with him in the distant village where he has been appointed parish clerk.
*'''Thomas Codlin''', proprietor of a travelling [[Punch and Judy]] show.
*'''Mr. Harris''', called 'Short Trotters', the puppeteer of the Punch and Judy show.
*'''Barbara''', the maidservant of Mr. and Mrs. Garland and future wife of Kit.
*'''The Bachelor''', brother of Mr. Garland. Lives in the village where Nell and her grandfather end their journey, and unknowingly alerts his brother to their presence through a letter.
*'''Mrs. Jiniwin''', Mrs. Quilp's mother, and Quilp's mother-in-law.  She resents Quilp for the way he treats her daughter, but is too afraid to stand up to him.

==Literary significance and criticism==
Probably the most widely repeated criticism of Dickens is the remark reputedly made by [[Oscar Wilde]] that 'One would have to have a heart of stone to read the death of little Nell without dissolving into tears...of laughter.' (Nell's deathbed is not actually described, however.) Of a similar opinion was the poet [[Algernon Swinburne]], who called Nell "a monster as inhuman as a baby with two heads."<ref>{{cite journal|last=Swinburne|first=Algernon Charles|title=The Greatness of Dickens|journal=The Bookman|year=1914|volume=Charles Dickens. A Bookman extra number|pages=183|url=https://archive.org/stream/charlesdickensbo00lond#page/182/mode/2up}}</ref>

The Irish leader [[Daniel O'Connell]] famously burst into tears at the finale, and threw the book out of the window of the train in which he was travelling.<ref>[http://www.irishtimes.com/newspaper/weekend/2012/0107/1224309920765.html "Britain is celebrating the great writer's bicentenary, but where in the Dickens are the Irish?" ''Irish Times'', 7 January 2012. Accessed 13 December 2012]</ref>

The hype surrounding the conclusion of the series was unprecedented; Dickens fans were reported to have stormed the piers in New York City, shouting to arriving sailors (who might have already read the final chapters in the United Kingdom), "Is Little Nell alive?" In 2007, many newspapers claimed that the excitement at the release of the last installment of ''The Old Curiosity Shop'' was the only historical comparison that could be made to the excitement at the release of the last [[Harry Potter]] novel, ''[[Harry Potter and the Deathly Hallows]]''.<ref>{{cite web|url=http://blog.wlbooks.com/2007/06/before-harry-potter-it-was-little-nell.html|title=Wessel & Lieberman: Before Harry Potter It Was Little Nell|publisher=|accessdate=2 December 2014}}</ref>

The [[Norwegian people|Norwegian]] author [[Ingeborg Refling Hagen]] is said to have buried a copy of the book in her youth, stating that nobody deserved to read about Nell, because nobody would ever understand her pain. She compared herself to Nell, because of her own miserable situation at the time.

==Allusions to actual history, geography==
[[Image:OldCuriosityShop.JPG|float|right|thumb|The Old Curiosity Shop, London]]
{{refimprove section|date=April 2016}}
A shop named 'The Old Curiosity Shop' can be found at 13–14 Portsmouth Street, Westminster, London, WC2A 2ES, amongst the buildings of the [[London School of Economics]]. The building dates back to the sixteenth century, but this name was added after the novel was released, as it was thought to be the inspiration for Dickens's description of the antique shop. At one time it functioned as a dairy on an estate given by [[Charles II of England|King Charles II]] to one of his many mistresses. It was built using timber from old ships, and survived the bombs of the [[Second World War]]. 

Nell and her grandfather meet Codlin and Short in a churchyard in [[Aylesbury]]. The horse races where Nell and her grandfather go with the show people are at [[Banbury]]. The village where they first meet the schoolmaster is [[Warmington, Warwickshire]]. They meet Mrs. Jarley near the village of [[Gaydon, Warwickshire]]. The town where they work at Jarley's Waxworks is [[Warwick]]. The heavily industrialised town where Nell spends the night by the furnace is [[Birmingham]] (after they have travelled on the [[Warwick and Birmingham Canal]]). The town in which Nell faints and is rescued by the school master is [[Wolverhampton]] in the [[Black Country]]. The village where they finally find peace and rest and where Nell dies is [[Tong, Shropshire]].<ref>The England of Dickens by Walter Dexter – London: Cecil Palmer 1925. Pages 172 to 188</ref>

Other real locations used in the novel include [[London Bridge]], [[Bevis Marks]], [[Finchley]], and [[Minster-on-Sea]].

==Adaptations ==
* There were several [[silent film]] adaptations of the novel including two directed by [[Thomas Bentley]]:
** ''[[The Old Curiosity Shop (1914 film)|The Old Curiosity Shop]]'' (1914)
** ''[[The Old Curiosity Shop (1921 film)|The Old Curiosity Shop]]'' (1921)
* ''Nelly'', an opera based on the novel, by Italian composer [[Lamberto Landi]], was composed in 1916; it premiered in Lucca in 1947.<ref name= "Traccani">[http://www.treccani.it/enciclopedia/lamberto-landi_%28Dizionario-Biografico%29/ Biography] at treccani.it</ref>
* The first [[talkie]] version was a 1934 British [[The Old Curiosity Shop (1934 film)|film]] starring [[Hay Petrie]] as [[Quilp]].
* The novel was serialised for television by the [[BBC]] in 1960.
* A British [[musical film|musical]] version of ''[[The Old Curiosity Shop (1975 film)|The Old Curiosity Shop]]'' (titled ''Mr. Quilp'' in the United States) was released in 1975. The filmmakers were hoping to cash in on the recent success of ''[[Oliver! (film)|Oliver!]]'', which was also based on a Dickens classic, but the film was notably unsuccessful.
* A Japanese animation series ''Sasurai no Shoujo Nell'' created in 1979-80.<ref>[http://www.animenewsnetwork.com/encyclopedia/anime.php?id=1726 Sasurai no Shoujo Nell]</ref>
* In 1979, a [[The Old Curiosity Shop (miniseries)|nine-part miniseries]] was created by the BBC and later released on DVD. There was no Frederick character and the story ends with the grandfather mourning at Nell's grave.
* An adaptation for [[BBC Radio 4]], first broadcast in 1990, was narrated by [[Alex Jennings]], with Emily Chenery (Nell), Phil Daniels ([[Quilp]]), Daniel Bliss (Kit), [[Trevor Peacock]] (grandfather), [[Clive Swift]], [[Anna Massey]] and [[Julia McKenzie]].
* A second [[BBC Radio 4]] adaptation was broadcast in 1998. The production starred [[Tom Courtenay]] as [[Quilp]], [[Denis Quilley]], [[Michael Maloney]] and [[Teresa Gallagher]].
* In 1995, [[Tom Courtenay]] and [[Peter Ustinov]] starred in a [[The Walt Disney Company|Disney]] made-for-television film<ref>[http://www.imdb.com/title/tt0108886/ The Old Curiosity Shop (1995) (TV)<!-- Bot generated title -->]</ref> adaptation as [[Quilp]] and the grandfather, with Sally Walsh<ref>[http://us.imdb.com/name/nm0909848/ Sally Walsh (II)<!-- Bot generated title -->]</ref> as Nell.
* A [[The Old Curiosity Shop (2007 film)|television film adaptation]] was produced by [[ITV (TV network)|ITV]], broadcast in the UK on 26 December 2007, and repeated on 14 December 2008.<ref>{{cite news| url=http://news.independent.co.uk/media/article2246071.ece | work=The Independent | location=London | title=How ITV aims to lure viewers with quality drama | first=Sarah | last=Shannon | date=7 February 2007 | accessdate=25 May 2010}}</ref>
* Little Nell is featured in the [[Philadelphia, Pennsylvania]], sculptural group [[Dickens and Little Nell (Elwell)|''Dickens and Little Nell'']] (1890).
* Nell and her grandfather are featured prominently in the BBC's 2015 Christmas drama [[Dickensian (TV series)|''Dickensian'']], which  brings together many of Dickens' iconic characters in one story.

==Major editions==
*1840–1841, UK, Chapman and Hall, Pub date (88 weekly parts) April 1840 to November 1841, Serial as part of ''Master Humphrey's Clock''
*1841, UK, Chapman and Hall (ISBN not used), Pub date ? ? 1841, Hardback (first edition)
*1870, UK, Chapman and Hall (ISBN not used), Hardback
*1904, NY, Thomas Y. Crowell (ISBN not used), Pub date ? ? 1904, Leatherbound
*1995, USA, Everyman's Library ISBN 0-460-87600-7, Pub date ? ? 1995, Paperback
*1997, UK, Clarendon Press (Oxford University Press) ISBN 0-19-812493-7, Pub date 13 November 1997, Hardback. This is considered the definitive edition of the book.
*2001, UK, Penguin Books Ltd ISBN 0-14-043742-8, Pub date 25 January 2001, Paperback (Penguin Classic)

==References==
{{Reflist|30em}}

==External links==
{{wikisource}}

'''Online editions'''
*[https://archive.org/stream/theoldcuriositys00dickiala#page/n7/mode/2up ''The Old Curiosity Shop''] at [[Internet Archive]].
*[https://books.google.com/books?as_brr=1&q=intitle%3A%22old+curiosity+shop%22&btnG=Search+Books ''The Old Curiosity Shop''] at [[Google Books]] (scanned books original editions illustrated)
* {{gutenberg|no=700|name=The Old Curiosity Shop}} (plain text and HTML)
*[http://www.19thnovels.com/theoldcuriosityshop.php ''The Old Curiosity Shop''] – complete book in HTML one page for each chapter.
* [http://etext.library.adelaide.edu.au/d/dickens/charles/d54oc/ ''The Old Curiosity Shop''] – Easy to read HTML version.
* {{librivox book | title=The Old Curiosity Shop | author=Charles Dickens}}

'''Other sites'''
* [http://charlesdickenspage.com/images/Little-Nells-Journey2.jpg ''The Old Curiosity Shop''] Map of Nell and her grandfather's journey from London through the Midlands to journey's end in Tong, Shropshire.
*[http://thelectern.blogspot.com/2007/03/old-curiosity-shop-charles-dickens.html ''The Old Curiosity Shop''] Review from 'The Lectern', March 2007.
* [http://www.londontown.com/LondonInformation/Shopping/The_Old_Curiosity_Shop/6740/ ''The Old Curiosity Shop''] London Information

{{Charles Dickens}}
{{The Old Curiosity Shop}}

{{DEFAULTSORT:Old Curiosity Shop, The}}
[[Category:1841 novels]]
[[Category:English novels]]
[[Category:Novels by Charles Dickens]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in Master Humphrey's Clock]]
[[Category:Victorian novels]]
[[Category:Novels set in London]]
[[Category:Chapman & Hall books]]
[[Category:The Old Curiosity Shop|*]]
[[Category:British novels adapted into films]]
[[Category:Novels about orphans]]
[[Category:Novels adapted into television programs]]