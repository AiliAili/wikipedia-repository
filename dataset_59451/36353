{{Good article}}
{{Infobox pageant titleholder
| name        = Rebecca Anderson
| photo       = Miss Oregon 2014 Rebecca Anderson 141207-Z-CH590-144 (cropped).jpg
| alt         = A young woman with brown hair, wearing a crown on her head and a Miss Oregon sash over her dress, singing into a microphone 
| caption     = Rebecca Anderson, December 2014
| title       = Miss Cascade 2011<br>Miss Columbia Gorge 2012<br>Miss Portland 2013<br>Miss Cascade 2014<br>[[Miss Oregon]] 2014
| competitions= [[Miss America 2015]]
| birth_name  = 
| birth_date  = {{Birth year and age|1991|05}}
| birth_place = 
| eye_color   = 
| hair_color  = 
| height      = 5 ft 4 in<ref name="poac140708"/>
| education   = [[Portland State University]]
| homepage    = 
}}

'''Rebecca Anderson''' (born May 1991) is an American [[beauty pageant]] [[title of honor|titleholder]] from [[Oregon City, Oregon]]. She won a series of local titles beginning in 2011 and was crowned [[Miss Oregon]] 2014.

Entering her first pageant at age 19, Anderson competed successfully in local pageants until, on her fourth attempt, she won the state title in June 2014. She made appearances across the state and represented Oregon at [[Miss America 2015]] in September 2014 but was not a finalist for the national crown. After completing her year as Miss Oregon, Anderson resumed her academic pursuits.

==Early life and education==
Anderson is a native of [[Oregon City, Oregon]], and a 2009 graduate of [[Oregon City High School]].<ref name="pltr130624"/> While at OCHS, she participated in the drama program for four years and was a member of the school's dance team for two years.<ref name="pltr110629"/> Her father is Ted Anderson, a wholesale nursery operator and licensed [[tax consultant]].<ref name="pltr140630"/><ref name="shandabt"/>  {{As of|2015}}, mother Shari Anderson is the elected County Treasurer of [[Clackamas County, Oregon]], an office she has held since 1999.<ref name="pltr140630"/><ref name="orgn140430"/> Anderson has one younger sister, Malea, who is also an occasional pageant competitor.<ref name="pltr130624"/>

Anderson is a 2013 graduate of [[Portland State University]] where she earned a bachelor's degree in [[environmental science]].<ref name="poac140708"/><ref name="pltr130624"/> In 2014, in addition to her Miss Oregon duties, Anderson worked part-time as a "cute" clown for an event company.<ref name="poac140708"/> In August 2015, Anderson moved to North Dakota to begin pursuing a bachelor's degree in [[atmospheric science]] at the [[University of North Dakota]].<ref name="msam15nc"/>

==Pageant career==
===Early pageants===
Anderson began entering pageants at age 19, waiting until she felt was "responsible enough" to hold a pageant title.<ref name="pltr110629"/><ref name="pltr140828"/> In early 2011, Anderson won the Miss Cascade 2011 title.<ref name="msca11wi"/><ref name="pltr110629"/> She competed in the 2011 [[Miss Oregon]] pageant with the platform "Environmental Sustainability: Education for a Greener World" and a vocal performance of "[[Feeling Good]]" from the musical ''[[The Roar of the Greasepaint – The Smell of the Crowd]]'' in the talent portion of the competition.<ref name="pltr110629"/><ref name="msam2011"/> She finished in the top ten but was not a Top-5 finalist for the state title.<ref name="msca11wi"/><ref name="pltr130624"/>

In early 2012, Anderson won the Miss Columbia Gorge 2012 title.<ref name="mscg12wi"/> She competed in the 2012 Miss Oregon pageant with the platform "Environmental Education" and a vocal performance in the talent portion of the competition.<ref name="msam2012"/> She finished in the top ten but was not a Top-5 finalist for the state crown.<ref name="pltr140514"/>

In March 2013, Anderson won the Miss Portland 2013 title. She qualified for the 2013 Miss Oregon pageant as one of 22 entrants, including her younger sister Malea, competing as Miss Clackamas 2013.<ref name="pltr130624"/> Rebecca Anderson competed with a platform of "American Red Cross: Red for Life" and a vocal performance of "Astonishing" from the musical ''[[Little Women (musical)|Little Women]]'' in the talent portion of the competition.<ref name="msam2013"/> She was named third runner-up to winner [[Allison Cook (Miss Oregon)|Allison Cook]].<ref name="pltr140514"/>

===Miss Oregon 2014===
On April 26, 2014, Anderson was crowned Miss Cascade 2014.<ref name="pltr140514"/><ref name="msca14wi"/> She entered the [[Miss Oregon]] pageant in June 2014 as one of 23 qualifiers for the state title.<ref name="msam2014"/> Anderson's competition talent was a vocal performance of "[[Let It Go (Disney song)|Let it Go]]" from the Disney movie ''[[Frozen (2013 film)|Frozen]]''.<ref name="pltr140630"/> Her platform was "American Red Cross: Red for Life".<ref name="msam2014"/> Her on-stage interview question asked whether discrimination was a factor in the [[gender pay gap in the United States]]. Anderson agreed but countered that "women need to be more assertive" and strive to close the gap themselves rather than wait for a government-mandated solution.<ref name="pltr140630"/>

[[File:Miss Oregon 2014 Rebecca Anderson 140821-Z-CH590-029.jpg|thumb|right|Rebecca Anderson in an [[McDonnell Douglas F-15 Eagle|F-15 Eagle]] at the [[Portland Air National Guard Base]], August 2014]]
Anderson won the competition on Saturday, June 28, 2014, when she received her crown from outgoing Miss Oregon titleholder [[Allison Cook (Miss Oregon)|Allison Cook]].<ref name="pltr140630"/> She earned more than $10,000 in scholarship money and other prizes from the state pageant.<ref name="poac140708"/>

As Miss Oregon, Anderson's activities included public appearances across the state of Oregon.<ref name="pltr140828"/> Notable appearances included participating in local pageants,<ref name="dast150202"/> welcoming [[Air National Guard]] members home to Oregon,<ref name="kptv141208"/> and serving as a judge for the Just For Men World Beard and Moustache Championships.<ref name="orgn141025"/><ref name="kgwt141027"/> Anderson's reign as Miss Oregon continued until June 27, 2015, when she crowned her successor, [[Ali Wallace]], Miss Oregon 2015.<ref name="pltr150702"/>

===Miss America 2015 contestant===
Anderson was Oregon's representative at the [[Miss America 2015]] pageant in [[Atlantic City, New Jersey]], in September 2014.<ref name="msam2014"/><ref name="saen140914"/> During the pageant's "Show Us Your Shoes" parade, Anderson wore a [[bicycle helmet]] and her shoes had tiny bicycles on top of the laces to tout [[Portland, Oregon]], as a "bike-friendly" city.<ref name="koin140913"/><ref name="nwst140914"/> She was not a Top-16 semi-finalist for the national crown.<ref name="usat140914"/>

== References ==
{{reflist|30em|refs=
<ref name="msam2011">{{cite web |publisher=[[Miss America]] |title=State Contestants: Oregon - 2011 |url=http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2011&GO=GO%21 |year=2011 |accessdate=August 15, 2015 |archiveurl=https://web.archive.org/web/20151011072042/http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2011&GO=GO%21 |archivedate=October 11, 2015}}</ref>
<ref name="msca11wi">{{cite web |publisher=Miss Cascade Scholarship Program |title=Miss Cascade 2011 - Rebecca Anderson |url=http://www.missthreerivers.org/miss_cascade_2011.html |year=2011 |accessdate=August 16, 2015}}</ref>
<ref name="pltr110629">{{cite news |first=Ellen |last=Spitaleri |newspaper=[[Portland Tribune]] |publisher=[[Pamplin Media Group]] |location=[[Portland, OR]] |title=CC women vie for Miss Oregon title |url=http://pamplinmedia.com/component/content/article?id=8883 |date=June 29, 2011 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20151011072236/http://pamplinmedia.com/component/content/article?id=8883 |archivedate=October 11, 2015}}</ref>
<ref name="msam2012">{{cite web |publisher=[[Miss America]] |title=State Contestants: Oregon - 2012 |url=http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2012&GO=GO%21 |year=2012 |accessdate=August 15, 2015 |archiveurl=https://web.archive.org/web/20150907234841/http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2012&GO=GO! |archivedate=September 7, 2015}}</ref>
<ref name="mscg12wi">{{cite web |publisher=Miss Three Rivers Scholarship Program |title=Miss Columbia Gorge 2012 - Rebecca Anderson |url=http://www.missthreerivers.org/miss_columbia_gorge_2012.html |year=2012 |accessdate=August 16, 2015}}</ref>
<ref name="msam2013">{{cite web |publisher=[[Miss America]] |title=State Contestants: Oregon - 2013 |url=http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2013&GO=GO%21 |year=2013 |accessdate=August 15, 2015 |archiveurl=https://web.archive.org/web/20151011072456/http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2013&GO=GO%21 |archivedate=October 11, 2015}}</ref>
<ref name="pltr130624">{{cite news |first=Ellen |last=Spitaleri |newspaper=[[Portland Tribune]] |publisher=[[Pamplin Media Group]] |location=[[Portland, OR]] |title=OCHS graduate sisters compete for state pageant crown |url=http://portlandtribune.com/pt/9-news/155225-ochs-graduate-sisters-compete-for-state-pageant-crown |date=June 24, 2013 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20130701004611/http://portlandtribune.com/pt/9-news/155225-ochs-graduate-sisters-compete-for-state-pageant-crown |archivedate=July 1, 2013}}</ref>

<ref name="msam2014">{{cite web |publisher=[[Miss America]] |title=State Contestants: Oregon - 2014 |url=http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2014&GO=GO%21 |year=2014 |accessdate=August 15, 2015 |archiveurl=https://web.archive.org/web/20151011072941/http://www.missamerica.org/competition-info/state-contestants.aspx?state=Oregon&year=2014&GO=GO%21 |archivedate=October 11, 2015}}</ref>
<ref name="msca14wi">{{cite web |publisher=Miss Cascade Scholarship Program |title=Miss Cascade 2014 - Rebecca Anderson |url=http://www.missthreerivers.org/miss_cascade_2014.html |year=2014 |accessdate=August 16, 2015}}</ref>
<ref name="orgn140430">{{cite news |first=Molly |last=Harbarger |newspaper=[[The Oregonian]] |publisher=[[Advance Publications]] |location=[[Portland, OR]] |title=Clackamas County elections: Treasurer Shari Anderson draws challengers for first time in 15 years |url=http://www.oregonlive.com/clackamascounty/index.ssf/2014/04/clackamas_county_elections_tre.html |date=April 30, 2014 |accessdate=October 11, 2015 |archiveurl=https://web.archive.org/web/20151011080551/http://www.oregonlive.com/clackamascounty/index.ssf/2014/04/clackamas_county_elections_tre.html |archivedate=October 11, 2015}}</ref>
<ref name="shandabt">{{cite web |publisher=Shari Anderson, Certified Public Accountant |title=About Us |url=http://www.shariandersoncpa.com/about-us.html |accessdate=October 11, 2015 |archiveurl=https://web.archive.org/web/20151011081204/http://www.shariandersoncpa.com/about-us.html |archivedate=October 11, 2015}}</ref>
<ref name="pltr140514">{{cite news |first=Ellen |last=Spitaleri |newspaper=[[Portland Tribune]] |publisher=[[Pamplin Media Group]] |location=[[Portland, OR]] |title=Two locals vie for titles, scholarships |url=http://pamplinmedia.com/cr/24-news/220440-80890-two-locals-vie-for-titles-scholarships |date=May 14, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20151011073034/http://pamplinmedia.com/cr/24-news/220440-80890-two-locals-vie-for-titles-scholarships |archivedate=October 11, 2015}}</ref>
<ref name="pltr140630">{{cite news |first=Ellen |last=Spitaleri |newspaper=[[Portland Tribune]] |publisher=[[Pamplin Media Group]] |location=[[Portland, OR]] |title=Anderson crowned Miss Oregon 2014 |url=http://portlandtribune.com/pt/9-news/225624-88026-anderson-crowned-miss-oregon-2014- |date=June 30, 2014 |accessdate=August 14, 2015 |archiveurl=https://web.archive.org/web/20150403001359/http://portlandtribune.com/pt/9-news/225624-88026-anderson-crowned-miss-oregon-2014- |archivedate=April 3, 2015}}</ref>
<ref name="poac140708">{{cite news |first=Trudi |last=Gilfillian |newspaper=[[The Press of Atlantic City]] |publisher=[[BH Media]] |location=[[Pleasantville, NJ]] |title=Miss Oregon Rebecca Anderson |url=http://www.pressofatlanticcity.com/missamerica/contestants/miss-oregon-rebecca-anderson/article_38108854-06ad-11e4-85de-0019bb2963f4.html |date=July 8, 2014 |accessdate=August 15, 2015 |archiveurl=http://www.webcitation.org/6cC4hOXzF |archivedate=October 11, 2015}}</ref>
<ref name="pltr140828">{{cite news |first=Ellen |last=Spitaleri |newspaper=[[Portland Tribune]] |publisher=[[Pamplin Media Group]] |location=[[Portland, OR]] |title=Miss Oregon will seek national-level crown |url=http://portlandtribune.com/pt/11-features/231362-95095-miss-oregon-will-seek-national-level-crown |date=August 28, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20140908091517/http://portlandtribune.com/pt/11-features/231362-95095-miss-oregon-will-seek-national-level-crown |archivedate=September 8, 2014}}</ref>
<ref name="msam15nc">{{cite web |publisher=[[Miss America]] |title=2015 National Contestants: Miss Oregon |url=http://www.missamerica.org/competition-info/national-contestants.aspx?state=Oregon&year=2015&GO=GO%21 |year=September 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20151011073545/http://www.missamerica.org/competition-info/national-contestants.aspx?state=Oregon&year=2015&GO=GO%21 |archivedate=October 11, 2015}}</ref>
<ref name="koin140913">{{cite news |agency=[[Associated Press]] |publisher=[[KOIN-TV]] |location=[[Portland, OR]] |title=Miss Oregon wears bicycle shoes in Miss America parade |url=http://koin.com/2014/09/13/miss-oregon-wears-bicycle-shoes-in-miss-america-parade/ |date=September 13, 2014 |accessdate=August 14, 2015 |archiveurl=https://web.archive.org/web/20150801221257/http://koin.com/2014/09/13/miss-oregon-wears-bicycle-shoes-in-miss-america-parade/ |archivedate=August 1, 2015}}</ref>
<ref name="nwst140914">{{cite news |agency=[[Associated Press]] |newspaper=[[The News-Star]] |publisher=[[Gannett]] |location=[[Monroe, LA]] |title=Miss La. shows off Mardi Gras-influenced shoes |url=http://www.thenewsstar.com/story/news/nation/2014/09/14/miss-la-shows-mardi-gras-influenced-shoes/15613329/ |date=September 14, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20151011085254/http://www.thenewsstar.com/story/news/nation/2014/09/14/miss-la-shows-mardi-gras-influenced-shoes/15613329/ |archivedate=October 11, 2015}}</ref>
<ref name="saen140914">{{cite news |agency=[[Associated Press]] |first=Julio |last=Cortez |newspaper=[[San Antonio Express-News]] |publisher=[[Hearst Corporation|Hearst]] |location=[[San Antonio, TX]] |title=The 2015 Miss America pageant (9 of 64) |url=http://www.mysanantonio.com/news/local/slideshow/The-2015-Miss-America-pageant-93668/photo-6865864.php |date=September 14, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20141223164420/http://www.mysanantonio.com/news/local/slideshow/The-2015-Miss-America-pageant-93668/photo-6865864.php |archivedate=December 23, 2014}}</ref>
<ref name="usat140914">{{cite news |first=Ann |last=Oldenburg |newspaper=[[USA Today]] |publisher=[[Gannett]] |title=Here she is ... Miss America 2015! |url=http://www.usatoday.com/story/life/people/2014/09/14/miss-america-2015-crowns-winner-miss-new-york-kira/15650945/ |date=September 14, 2014 |accessdate=August 16, 2015 |archiveurl=http://www.webcitation.org/6cCBMaL8E |archivedate=October 11, 2015}}</ref>
<ref name="orgn141025">{{cite news |first=Jamie |last=Hale |newspaper=[[The Oregonian]] |publisher=[[Advance Publications]] |location=[[Portland, OR]] |title=World Beard and Moustache Championships draws huge crowd to Portland, spirals into chaos onstage |url=http://www.oregonlive.com/entertainment/index.ssf/2014/10/world_beard_and_moustache_cham_1.html |date=October 25, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20150910091555/http://www.oregonlive.com/entertainment/index.ssf/2014/10/world_beard_and_moustache_cham_1.html |archivedate=September 10, 2015}}</ref>
<ref name="kgwt141027">{{cite news |first=Cassidy |last=Quinn |publisher=[[KGW-TV]] |location=[[Portland, OR]] |title=Portland beard wins World Championship in Portland |url=http://www.kgw.com/story/news/features/2014/10/27/beard-moustache-championships/18008541/ |date=October 27, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20141213005005/http://www.kgw.com/story/news/features/2014/10/27/beard-moustache-championships/18008541 |archivedate=December 13, 2014 }}</ref>
<ref name="kptv141208">{{cite news |publisher=[[KPTV]] |location=[[Portland, OR]] |title=Oregon governor and Miss Oregon welcome returning Air National Guard squadron |url=http://www.kptv.com/story/27573896/oregon-governor-and-miss-oregon-welcome-returning-air-national-guard-squadron |date=December 8, 2014 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20150406061702/http://www.kptv.com/story/27573896/oregon-governor-and-miss-oregon-welcome-returning-air-national-guard-squadron |archivedate=April 6, 2015}}</ref>
<ref name="pltr150702">{{cite news |first=Ellen |last=Spitaleri |newspaper=[[Portland Tribune]] |publisher=[[Pamplin Media Group]] |location=[[Portland, OR]] |title=Miss Portland upgrades her title to Miss Oregon |url=http://portlandtribune.com/pt/11-features/265623-138817-miss-portland-upgrades-her-title-to-miss-oregon |date=July 2, 2015 |accessdate=July 27, 2015 |archiveurl=https://web.archive.org/web/20151011090653/http://portlandtribune.com/pt/11-features/265623-138817-miss-portland-upgrades-her-title-to-miss-oregon |archivedate=October 11, 2015}}</ref> 
<ref name="dast150202">{{cite news |newspaper=[[The Daily Astorian]] |publisher=[[East Oregonian|EO Media Group]] |location=[[Astoria, OR]] |title=Poise, talent, beauty: Alexis Mather and Hannah Garhofer take top titles at Miss Clatsop County pageant |url=http://www.dailyastorian.com/Local_News/20150202/poise-talent-beauty |date=February 2, 2015 |accessdate=August 16, 2015 |archiveurl=https://web.archive.org/web/20150705101709/http://www.dailyastorian.com/Local_News/20150202/poise-talent-beauty |archivedate=July 5, 2015}}</ref>
}}

==External links==
{{Portal|Biography|Oregon}}
*{{cite web |url=http://www.missoregon.org |title=Miss Oregon official website |archiveurl=https://web.archive.org/web/20151011075333/http://www.missoregon.org/ |archivedate=October 11, 2015}}

{{s-start}}
{{s-ach}}
{{succession box
| before=[[Allison Cook (Miss Oregon)|Allison Cook]]
| title=[[Miss Oregon]]
| years=2014
| after=[[Ali Wallace]]
}}
{{s-end}}
{{Miss America 2015 Delegates}}
{{Oregon pageant winners}}

{{DEFAULTSORT:Anderson, Rebecca}}
[[Category:Living people]]
[[Category:1991 births]]
[[Category:American beauty pageant winners]]
[[Category:Miss America 2015 delegates]]
[[Category:Miss Oregon winners]]
[[Category:People from Oregon City, Oregon]]
[[Category:Portland State University alumni]]