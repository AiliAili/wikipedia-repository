{{Infobox company
| name = Leonard Curtis
| logo = [[File:Leonard Curtis logo.gif]]
| type = [[Partnership]]
| foundation =
| location = [[Manchester]], UK
| num_employees = > 100
| industry = [[Accountancy]]
| products = [[Insolvency]], [[corporate recovery]], [[restructuring]]
| revenue = £14 million (2011)<ref>{{cite news|last=Reed|first=Kevin|title=Leonard Curtis lines up more growth after strong 2010|url=http://www.accountancyage.com/aa/news/2093054/leonard-curtis-lines-growth-strong-2010|accessdate=13 July 2011|newspaper=Accountancy Age|date=12 July 2011}}</ref>
| operating_income = £4.6 million (2011)
| homepage = [http://www.leonardcurtis.co.uk/ leonardcurtis.co.uk]
}}
'''Leonard Curtis''' is one of the largest independent firms in the [[United Kingdom|UK]] specialising in [[corporate recovery]], [[insolvency]] and business [[restructuring]].

It is a national practice with 14 directors and more than 100 staff based at offices in [[Manchester]], Bristol, [[Bury]], [[Blackburn]], [[Wolverhampton]], [[Birmingham]], [[Newcastle-upon-Tyne]] and [[London]].

==History==
The firm was founded by Leonard Curtis, who retired in 1983.<ref name=indy1998/>

In 1997 the firm considered and rejected a merger with Levy Gee, a general accountancy firm that went on to form [[Numerica]]. Leonard Curtis partners stated that they preferred to remain a specialist firm.<ref>[http://www.financialdirector.co.uk/accountancyage/news/2016160/mergers-acquisitions-levy-gee-leonard-curtis-scrap-merger Levy Gee and Leonard Curtis scrap merger], ''Accountancy Age'', 27 Aug 1997</ref>

In 1998, two offices in Manchester and Liverpool with 30 staff left the firm for [[BDO Stoy Hayward]].<ref name=indy1998>[http://www.independent.co.uk/news/business/people--business-1148098.html People & Business], The Independent, 3 March 1998</ref>

Later in 1998, Leonard Curtis formed a joint venture, Fisher Curtis, with the insolvency arm of H. W. Fisher.<ref>[http://www.financialdirector.co.uk/accountancyage/news/2018028/news-brief-october-2 News in brief], ''Accountancy Age'', 27 Oct 1998</ref> The arrangement lasted only two years before the firms separated again.<ref>[http://www.financialdirector.co.uk/accountancyage/news/2022423/curtis-split-fisher Curtis split from Fisher], ''Accountancy Age'', 30 Jun 2000</ref>

==Awards==
Leonard Curtis won the mid-sized firm prize as Corporate Recovery Firm of the Year at the inaugural Insolvency and Rescue Awards 2008, run by ''Credit Today'' magazine.<ref>[http://www.leonardcurtis.co.uk/news_archive/lc_wins_corporate_recovery_firm_of_the_year LC wins Corporate Recovery Firm of the Year], 31 October 2008 (on official website)</ref>

The firm was awarded Business Recovery Specialist of the Year at the 2010 Business [[Moneyfacts]] Awards. The judging panel said: "By providing companies with positive strategic advice, Leonard Curtis has enabled many of them to retain control of their business. The Corporate Strategies team continues to focus on the critical issues, providing clear recommendations together with restructuring and refinancing services to both growing and underperforming businesses."<ref>[http://www.creditman.biz/uk/members/news-view.asp?newsviewID=11478 Leonard Curtis named National Business Recovery Specialist of the Year], Business Credit Management UK, 29 Mar 2010</ref>

==Controversy==
"The Liquidation Game", a 1996 edition of [[Channel 4]]'s investigative documentary ''[[Dispatches (TV series)|Dispatches]]'', made allegations of sharp practice in the insolvency profession. Keith Goodman, then a partner in Leonard Curtis (later both Senior Partner of the firm and President of the [[Insolvency Practitioners Association|IPA]]),<ref>[http://www.financialdirector.co.uk/accountancyage/news/2026312/ipa-appoints-president IPA appoints new president], ''Accountancy Age'', 15 May 2001</ref> claimed that a case of his was presented in a biased way, and sought an apology. However, the [[Broadcasting Complaints Commission (UK)|Broadcasting Complaints Commission]] rejected his complaint, saying that his case was sufficiently well set apart from other more serious allegations in the programme.<ref>[http://www.financialdirector.co.uk/accountancyage/news/2014851/climax-practitioner-case-against-dispatches Climax of practitioner's case against ''Dispatches''], ''Accountancy Age'', 6 Feb 1997</ref><ref>[http://www.financialdirector.co.uk/accountancyage/news/2016073/insolvency-goodman-loses-c4-fight Goodman loses C4 fight], ''Accountancy Age'', 8 Aug 1997</ref>

==Notable clients==
In 1993, the firm was appointed by the liquidators of [[Bank of Credit and Commerce International|BCCI]] as receivers to the Manchester-based Kumar brothers, selling their holdings including [[Birmingham City F.C.#Ownership|Birmingham City F.C.]].<ref>[http://www.independent.co.uk/news/business/birmingham-football-sale-nears-1476894.html Birmingham football sale nears], The Independent, 6 January 1993</ref>

Other past clients include lobbyist [[Ian Greer (lobbyist)|Ian Greer]],<ref>[[Christian Wolmar]], [http://www.independent.co.uk/news/greers-lobby-empire-crashes-1315281.html Greer's lobby empire crashes], ''The Independent'', 20 December 1996</ref> [[Metrocab]], the [[Hastings Pier]] Company, [[Poole Pottery]] and [[Evesham Technology]].<!-- citations are included in the linked articles -->

===Recent clients===
Leonard Curtis sold [[Sofa Workshop]] in 2009, protecting customer orders but with significant job losses.

Later that year, the firm saved two thirds of the business of fashion retailer [[MK One]] by a sale to Internacionale Retail.

The firm was appointed [[administration (law)|administrator]] of [[Stockport County F.C.]] in 2009. It [[History of Stockport County F.C.|took a long time]] to find a buyer for the club, leading to criticisms from its fans.<ref>[http://www.accountancyage.com/accountancyage/news/2255222/stockport-county-administrators Stockport County administrators defend fees], ''Accountancy Age'', 17 Dec 2009</ref>

==References==
{{Reflist}}

==External links==
*[http://www.leonardcurtis.co.uk/ Official website]
{{Use dmy dates|date=September 2010}}

[[Category:Companies based in Manchester]]
[[Category:Insolvency and corporate recovery firms]]