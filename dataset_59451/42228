<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=GA-II Chiricahua
 | image=Applebay GA-11 Chiricahua N9413.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=[[United States]]
 | manufacturer=
 | designer=[[George Applebay]]
 | first flight=1970
 | introduced=
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=one
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Applebay GA-II Chiricahua''' is an [[United States|American]] [[high-wing]], single-seat, [[FAI Standard Class]] [[Glider (sailplane)|glider]] that was designed and constructed by [[George Applebay]], first flying in 1970.<ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=58|title = Chiricahua GA-II Applebay |accessdate = 8 August 2011|last = Activate Media|authorlink = |year = 2006}}</ref>

==Design and development==
Applebay started the Chiricahua as a standard class sailplane in 1959, but the aircraft was not completed for 11 years, first flying in 1970.<ref name="SD" /> It was named for the [[Chiricahua people]], a group of [[Apache]] Native Americans.

The aircraft is made from wood and covered in a combination of [[plywood]] and [[Aircraft dope|doped]] [[Ceconite]]. Its {{convert|15|m|ft|1|abbr=on}} span wing employs a Göttingen 549 [[airfoil]] and features [[Schempp-Hirth]] style top surface [[Air brake (aircraft)|airbrakes]]. As originally specified for the standard class, the landing gear was a fixed monowheel.<ref name="SD" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 8 August 2011|last = Lednicer |first = David |authorlink = |year = 2010| archiveurl= https://web.archive.org/web/20110719211726/http://www.ae.illinois.edu/m-selig/ads/aircraft.html| archivedate= 19 July 2011 <!--DASHBot-->| deadurl= no}}</ref>

Only one example was built.<ref name="SD" />

==Operational history==
On 7 July 1974 at [[New River, Arizona]] the prototype, N9413, was involved in an accident and substantially damaged. The aircraft was on a soaring flight, ran out of lift, made an attempted landing on a road in a {{convert|20|kn|km/h|0|abbr=on}} crosswind and struck a tree. The 24-year-old pilot, who had 32 hours of flying time total, including 11 hours on type, was not injured.<ref name="NTSB">{{Cite web|url =http://www.ntsb.gov/aviationquery/brief.aspx?ev_id=31197&key=0|title = NTSB Identification: LAX75DUJ07|accessdate = 8 August 2011|last = [[National Transportation Safety Board]]|authorlink = |date=August 2011}}</ref> The aircraft has since been re-registered as N53MB.<ref name="FAA 53MB">{{cite web|title=FAA Registry N Number Inquiry|url=http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=53MB&x=0&y=0|publisher=FAA|accessdate=30 October 2012}}</ref>

==Specifications (GA-II) ==
{{Aircraft specs
|ref=Sailplane Directory<ref name="SD" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=15
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=148.2
|wing area note=
|aspect ratio=16.2:1
|airfoil=Gottingen 549
|empty weight kg=
|empty weight lb=590
|empty weight note=
|gross weight kg=
|gross weight lb=819
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|g limits=
|roll rate=
|glide ratio=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=5.47
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
* [[List of gliders]]
}}

==References==
{{reflist}}

==External links==
*[http://www.ae.illinois.edu/m-selig/ads/afplots/goe549.gif Gŏttingen 549 airfoil]
{{Applebay Sailplanes}}

[[Category:United States sailplanes 1970–1979]]
[[Category:Homebuilt aircraft]]
[[Category:Applebay aircraft|GA-II]]