{{Other people|Francis Turner}}
{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Francis McDougall Charlewood Turner
| image         =
| caption       =
| birth_date    = {{Birth date|df=yes|1897|3|17}}
| death_date    = {{Death date and age|df=yes|1982|6|18|1897|3|17}} 
| birth_place   = [[Hastings]], [[Sussex]], England
| death_place   = [[Chichester]], [[West Sussex]], England
| placeofburial_label =
| placeofburial =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = United Kingdom
| branch        = British Army<br/>Royal Air Force
| serviceyears  = 1916–1920<br/>1940–1944
| rank          = [[Squadron Leader]]
| unit          = [[No. 55 Squadron RAF|No. 55 Squadron RFC]]<br/>[[No. 57 Squadron RAF]]
| commands      =
| battles       =
| awards        = [[Military Cross]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
| relations     =
| laterwork     = Fellow and President of [[Magdalene College, Cambridge]]
}}
Squadron Leader '''Francis McDougall Charlewood Turner''', {{post-nominals|country=GBR|size=100%|MC|DFC|sep=yes}} (17 March 1897 – 18 June 1982) was a British [[World War I|First World War]] [[flying ace]] credited with [[List of World War I aces credited with 7 victories|seven aerial victories]].<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/turner2.php |title=Francis McDougall Charlewood Turner |work=The Aerodrome |year=2016 |accessdate=23 March 2016}}</ref> He returned to military service during the Second World War, and in between had a distinguished academic career as a Fellow and President of [[Magdalene College, Cambridge]].

==Early life and family background==
Turner was born in [[Hastings]], [[Sussex]], one of nine children born to [[Charles Turner (bishop)|Charles Henry Turner]], who at that time was [[Rector (ecclesiastical)|rector]] of the parish of [[St George in the East (parish)|St George-in-the-East]] and an [[Honorary Chaplain to the Queen]], who served as [[Suffragan bishop|Suffragan]] [[Bishop of Islington]] from 1898 until 1923, and Edith Emma, the daughter of the [[Right Reverend]] [[Francis McDougall]], [[Bishop of Kuching|Bishop of Labuan and Sarawak]].<ref>{{cite web |url= http://venn.lib.cam.ac.uk/cgi-bin/search-2016.pl?sur=&suro=w&fir=&firo=c&cit=&cito=c&c=all&z=all&tex=TNR859CH&sye=&eye=&col=all&maxcount=50 |title=Turner, Charles Henry |work=ACAD – A Cambridge Alumni Database |year=2016 |accessdate=23 March 2016}}</ref><ref name="CULSC">{{cite web |url=https://specialcollections.blog.lib.cam.ac.uk/?p=6438 |title=Recent acquisitions from the Mendham sale |first=Liam |last=Sims |work=Cambridge University Library Special Collections |date=5 December 2013 |accessdate=23 March 2016}}</ref>

==World War I==
Turner was commissioned as a second lieutenant (on probation) on 3 January 1916 to serve in the [[Royal Flying Corps]],<ref>{{London Gazette |date=11 January 1916 |issue=29434 |startpage=458 |nolink=yes}}</ref> and after completing his flight training, he was appointed a flying officer and confirmed in his rank on 17 June.<ref>{{London Gazette |date=11 July 1916 |issue=29660 |startpage=6855 |endpage=6857 |nolink=yes}}</ref> He was promoted to lieutenant on 1 March 1917,<ref>{{London Gazette |date=27 March 1917 |issue=30002 |startpage=3005 |nolink=yes}}</ref> and on 7 May was appointed a [[flight commander]] with the temporary rank of captain.<ref>{{London Gazette |date=8 June 1917 |supp=yes |issue=30124 |startpage=5722 |nolink=yes}}</ref>

Turner gained his first aerial victories on 13 August 1917, while serving in [[No. 55 Squadron RAF|No. 55 Squadron]], flying an [[Airco DH.4]] two-seater day bomber. With Second Lieutenant R. Brett as his observer, he drove down out of control two [[Albatros D.V]] fighters over [[Roulers]].<ref name="theaerodrome"/>
 
On 26 September 1917 he was awarded the [[Military Cross]],<ref>{{London Gazette |date=25 September 1917 |supp=yes |issue=30308 |startpage=9980 |nolink=yes}}</ref> the citation for which was eventually published on 8 January 1918. It read:
:Lieutenant (Temporary Captain) Francis McDougall Charlewood Turner, Royal Flying Corps, Special Reserve.
::"For conspicuous gallantry and devotion to duty as leader of long distance bombing raids and on photographic reconnaissances. He has taken part in many successful operations, in twenty-four of which he has acted as leader, and by his skill and determination has invariably done good work in spite of very adverse weather conditions, and though more than once attacked by enemy formations in greatly superior numbers to his own. He was the first officer to carry out a single machine long distance reconnaissance successfully, and has been more than once congratulated for the excellence of his photographic work."<ref>{{London Gazette |date=8 January 1918 |supp=yes |issue=30466 |startpage=648 |nolink=yes}}</ref>

On 1 April 1918, the Army's Royal Flying Corps (RFC) and the Royal Naval Air Service were merged to form the Royal Air Force. Turner, now serving in [[No. 57 Squadron RAF|No. 57 Squadron]] marked the occasion by shooting down three [[Fokker Dr.I]] fighters over [[Irles]], with Second Lieutenant A. Leach as his observer, taking his total to five and making him an ace. On 28 July he shared with the crews of two other aircraft in the driving down of a [[Fokker D.VII]] fighter over [[Vaulx-Vraucourt]], with Sergeant S. G. Sowden as his observer, and on 8 August he and observer Second Lieutenant H. S. Musgrove destroyed another D.VII over [[Moislains]] Airfield, to bring his total to seven.<ref name="theaerodrome"/>

Turner was subsequently awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]], which was [[gazetted]] on 1 November 1918. His citation read:
:Captain Francis McDougal Charlewood Turner, MC.
::"While leading a formation of ten machines on a bombing raid this officer was attacked by twenty Fokker biplanes. In face of this strong attack he led his formation down to 1,000 feet, and two sheds were set on fire and billets were riddled with machine-gun fire; three enemy machines were also accounted for, Captain Turner himself crashing one. The success of this raid was mainly due to the brilliant and daring leadership of this officer."<ref>{{London Gazette |date=1 November 1918 |supp=yes |issue=30989 |startpage=12974 |endpage=12975 |nolink=yes}}</ref>

Turner eventually relinquished his RAF commission on 12 March 1920, due to ill-health contracted on active service, and was permitted to retain his rank.<ref>{{London Gazette |date=19 March 1920 |issue=31830 |startpage=3436 |nolink=yes}}</ref>

===List of aerial victories===
{| class="wikitable" style="font-size:90%;"
|-
|+Combat record<ref name="theaerodrome"/>
|-
!No.
! width="100" |Date/Time
! width="100" |Aircraft/<br/>Serial No.
! width="100" |Opponent
! width="120" |Result
! width="130" |Location
!Notes
|-
! colspan="7"|No. 55 Squadron RFC
|-
|1 || rowspan="2"|13 August 1917<br/>@ 0815–0820 || rowspan="2"| [[Airco DH.4|DH.4]]<br/>(B3962) || [[Albatros D.V]] || Out of control || [[Roulers]] || rowspan="2"| Observer: Second Lieutenant R. Brett.
|-
|2 || Albatros D.V || Out of control || South-east of Roulers
|-
! colspan="7"|No. 57 Squadron RAF
|-
|3 || rowspan="3"|1 April 1918<br/>@ 1145 || rowspan="3"|DH.4<br/>(A7901) || [[Fokker Dr.I]] || Destroyed in flames || rowspan="3"| [[Irles]] || rowspan="3"| Observer: Second Lieutenant A. Leach.
|-
|4 || Fokker Dr.I || Destroyed
|-
|5 || Fokker Dr.I || Out of control
|-
|6 || 28 July 1918<br/>@ 1720 || DH.4 || [[Fokker D.VII]] || Out of control || [[Vaulx-Vraucourt]] || Observer: Sergeant S. G. Sowden.<br/>Shared with Lieutenant W. J. Pitt & Second Lieutenant T. C. Danby and Lieutenant E. M. Coles & Sergeant McDonald.
|-
|7 || 8 August 1918<br/>@ 0815 || DH.4<br/>(D8419) || Fokker D.VII || Destroyed || [[Moislains]] Airfield || Observer: Second Lieutenant H. S. Musgrove.
|-
|}

==Inter-war career==
After the war Turner turned to an academic career, graduating from [[Magdalene College, Cambridge]], and winning the La Bas Prize in 1924 for his essay ''The Element of Irony in English Literature'', which was published in 1926,<ref>{{cite web |url=https://openlibrary.org/books/OL6692360M/The_element_of_irony_in_English_literature |title=The Element of Irony in English Literature |first=F. McD. C. |last=Turner |work=Open Library |year=1926 |accessdate=23 March 2016}}</ref> and then becoming a [[fellow]] of the college.<ref name="GMartin">{{cite web |url=http://www.gedmartin.net/martinalia-mainmenu-3/196-magdalene-college-cambridge-and-the-first-world-war |title=Magdalene College Cambridge and the First World War |first=Ged |last=Martin |work=gedmartin.net |date=April 2014 |accessdate=23 March 2016}}</ref>

==World War II==
Turner returned to military service during the Second World War, being commissioned as a [[pilot officer]] "for the duration of hostilities" in the Administrative and Special Duties Branch of the [[Royal Air Force Volunteer Reserve]] on 31 October 1940.<ref>{{London Gazette |date=19 November 1940 |supp=yes |issue=34996 |startpage=6639 |endpage=6640 |nolink=yes}}</ref> He was promoted to the [[war substantive]] rank of flying officer on 27 May 1941,<ref>{{London Gazette |date=19 September 1941 |issue=35279 |startpage=5441 |endpage=5442 |nolink=yes}}</ref> and was appointed a temporary squadron leader on 1 July 1944.<ref>{{London Gazette |date=18 July 1944 |supp=yes |issue=36618 |startpage=3402 |nolink=yes}}</ref> He resigned his commission on 4 November 1944, retaining the rank of squadron leader.<ref>{{London Gazette |date=17 November 1944 |supp=yes |issue=36802 |startpage=5333 |nolink=yes}}</ref>

==Post-war career==
Turner returned to Magdalene, eventually becoming [[Pepys Library|Pepys Librarian]]<ref name="GMartin"/> and President of the College.<ref name="CULSC"/> His contemporaries included [[Dennis Babbage]] and [[C. S. Lewis]],<ref>{{cite journal |url=http://files.magdalenecambridge.com/pdfs/Publications/College%20Magazine%202011-12.pdf |first=John |last=Hudson |title=Magdalene Memories |journal=Magdalene College Magazine |number=56 |year=2012 |pages=72–73 |accessdate=23 March 2016}}</ref> and he became a close friend of [[T. S. Eliot]].<ref>{{cite journal |url=http://www.luc.edu/eliot/newsletter/85%20spr%2015.pdf |first=M. E. J. |last=Hughes |title=Bequest from Eliot's Library to Magdalene College, Cambridge |journal=Time Present: The Newsletter of the T. S. Eliot Society |number=85 |year=2015 |pages=8–9 |accessdate=23 March 2016}}</ref>

Turner died in [[Chichester]], [[West Sussex]], on 18 June 1982.

==References==
{{reflist|30em}}

{{DEFAULTSORT:Turner, Frank}}
[[Category:1897 births]]
[[Category:1982 deaths]]
[[Category:People from Hastings]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:Recipients of the Military Cross]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Alumni of Magdalene College, Cambridge]]
[[Category:Fellows of Magdalene College, Cambridge]]