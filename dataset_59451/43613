__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Forssman
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type= [[Bomber]]
 | national origin=Germany
 | manufacturer= [[Siemens-Schuckert]]
 | designer= [[Villehad Forssman]]<ref name="gt570">Grey & Thetford 1962, p.570</ref><ref name="hg163">Haddow & Grosz 1963, p.163</ref>
 | first flight=  Spring 1915<ref name="gt570"/>
 | introduced=
 | retired=
 | status=
 | primary user=[[Luftstreitkräfte]]<ref name="gt571">Grey & Thetford 1962, p.571</ref><ref name="hg170">Haddow & Grosz 1963, p.170</ref>
 | number built= 1<ref name="hg170"/><ref name="gt">Grey & Thetford 1962, p.570–71</ref><ref name="jea">Taylor 1989, p.808</ref>
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Siemens-Schuckert Forssman''' was a prototype bomber aircraft designed and built in Germany in 1914 and 1915.<ref name="gt"/><ref name="jea"/><ref name="hg164">Haddow & Grosz 1963, p.164</ref> When its performance proved inadequate for its intended role, even after numerous modifications, the German Inspectorate of Flying Troops (the ''[[Idflieg]]'') eventually accepted it into service as a trainer.<ref name="gt571"/><ref name="hg170"/> Shortly after its acceptance into military service, the aircraft's fuselage fractured while on the ground, ending its career.<ref name="hg170"/> The aircraft is sometimes known as the '''Siemens-Schuckert Forssman&nbsp;R''',<ref>for example, by Haddow & Grosz, and by the Royal Air Force Museum Aircraft Thesaurus</ref> the "R" alluding to the designation that the ''Idflieg'' assigned to multi-engine aircraft in late 1915.<ref name="hg1">Haddow & Grosz 1963, p.1</ref> However, the Forssman did not meet all the criteria of the "R" designation — for example, its engines were not serviceable in flight — and the "R" designation only came into use after the Forssman was built.<ref name="hg172">Haddow & Grosz 1963, p.172</ref> If the Forssman received any military designation at all, that designation is not now known.<ref name="hg172"/>

== Design and development ==
The Forssman was a large, four-engined, thee-bay biplane with an extensively glazed, fully enclosed cabin.<ref name="hg164"/> The wings were unstaggered and of uneven span,<ref name="hg165">Haddow & Grosz 1963, p.165</ref> and the undercarriage consisted of two mainwheels carried on a common axle, plus a tailskid.<ref name="hg165"/> The tail was of conventional layout with a single fin.<ref name="hg164"/> Power was supplied by {{convert|110|hp|kW|0|abbr=on|adj=on}} [[Mercedes D.III]] engines mounted on short struts on the lower mainplane.<ref name="gt570"/><ref name="hg164"/> The design bore such a strong resemblance to the contemporary [[Sikorsky Ilya Muromets]] bombers produced in Russia that ''Jane's Encyclopedia of Aviation'' describes it as "virtually a copy" of that aircraft,<ref name="jea"/> and in their book on German multi-engine types of World War I, Haddow and Grosz comment that designer Villehad Forssman "was content to copy the Sikorsky configuration almost line-for-line".<ref name="hg162">Haddow & Grosz 1963, p.162</ref>

Construction commenced in October 1914, and was completed by spring 1915.<ref name="gt570"/><ref name="hg164"/> Early test flights, probably limited to short hops, revealed multiple shortcomings in the design.<ref name="hg164"/> As originally completed, the wings were braced between the second and third cell by only a single strut.<ref name="hg164"/> Reinforcements to the wings added a second strut aft of this position, together with a set of diagonal struts to support the overhang of the upper wings<ref name="hg164"/> Other modifications at this time included rigging the wings with a slight dihedral to improve lateral stability,<ref name="hg164"/> adding a second tailfin and rudder to improve control,<ref name="hg164"/> and grafting an open gun position onto the front of the fuselage to improve the aircraft's centre of gravity.<ref name="gt570"/><ref name="hg164"/>

Even with these modifications in place, the Forssman could not fulfil the ''Idflieg'''s acceptance criteria, and Villehad Forssman severed his connections with Siemens-Schuckert owing to the failure of his design.<ref name="hg166">Haddow & Grosz 1963, p.166</ref> With Siemens-Schuckert still eager to recoup its investment in the design, the firm assigned [[Harald Wolff]] to improve the aircraft. Wolff deleted the tacked-on pulpit from the nose and redesigned the forward fuselage to taper to a sharp point, surmounted by a teardrop-shaped cupola for the pilot.<ref name="hg166"/> The redesign also replaced the two inner D.III engines with more powerful [[Mercedes D.IVa]] engines mounted on faired struts.<ref name="hg166"/>  The outer engines remained unchanged, but were moved from their original position to mountings on faired struts midway in the interplane gap, to improve propeller efficiency.<ref name="hg166"/> Gun positions were added in the nose and in dorsal and ventral locations in the fuselage, although all these positions remained faired-over during testing and there is no evidence that armament was ever fitted.<ref name="hg166"/> In September 1915, and after another pilot had refused to fly the aircraft following some ground tests, Lt [[Walter Höndorf]] agreed to test the redesigned Forssman.<ref name="hg166"/> After a one or two hops, the aircraft turned over on its nose while alighting.<ref name="hg166"/> The nose was crushed, and the leading-edge spar in the upper wing was fractured.<ref name="hg166"/>

Despite all setbacks, Siemens-Schuckert still wanted to sell the aircraft, and commenced another rebuild.<ref name="hg166"/> The nose was redesigned again, into a rounded, blunt configuration with a gun position atop.<ref name="hg166"/> The pilot's position was relocated either behind the nose windows or to an open cockpit on top of the fuselage.<ref name="hg166"/> By now, the aircraft had acquired the nickname ''Ladenhüter'' (literally, "shelf warmer" or, idiomatically, "white elephant") and pilots refused to fly it.<ref name="hg166"/> Siemens-Schuckert director [[Walter Reichel]] negotiated a price discount for the ''Idflieg'' if they would accept the aircraft to a lower specification.<ref name="hg167">Haddow & Grosz 1963, p.167</ref> The ''Idflieg'' accepted, and reduced the acceptance criteria to the aircraft reaching {{convert|2000|m|ft|0|abbr=on}} in 30 minutes while carrying a useful load of {{convert|1000|kg|lb|0|abbr=on}} and enough fuel for an endurance of 4 hours.<ref>Haddow & Grosz 1963, p.167–68</ref> Reichel now offered [[Bruno and Franz Steffen]] ten percent of the sale price if they could perform an acceptance flight to meet the ''Idflieg'''s requirements. The Steffen brothers examined the Forssman and Bruno agreed to make the flight, against the recommendations of friends and associates.<ref name="hg168">Haddow & Grosz 1963, p.168</ref> Franz's calculations based on the construction drawings showed the aircraft safe to fly, although the fuselage to be structurally weak immediately aft of the wings.<ref name="hg168"/>

After a successful {{convert|300|m|ft|0|adj=on}} hop,<ref name="hg169">Haddow & Grosz 1963, p.169</ref> Bruno Steffen planned to fly the Forssman on its acceptance flight with four passengers aboard, as he had for his own [[Siemens-Schuckert R.I|R.I]] design.<ref name="hg170"/> However, everyone whom he invited declined, including the members of the ''Idflieg'' acceptance committee, and so he made the flight alone.<ref name="hg170"/>  Carrying the required 1,000-kilogram payload, Steffen reached 2,000&nbsp;metres in 28 minutes, and then climbed another {{convert|100|m|ft|0}} before returning to the ground.<ref name="hg170"/> While descending, one engine failed, followed shortly by the other three.<ref name="hg170"/> Nevertheless, Steffen made a safe landing and the acceptance criteria were verified.<ref name="hg170"/>

Although now obsolescent,<ref name="gt571"/> the ''Idflieg'' accepted the Forssman into service in April 1916 as a trainer.<ref name="gt571"/><ref name="hg170"/> Further development of the design was halted, although the aircraft's ballast was moved to counter tail heaviness that Steffen encountered on the acceptance flight.<ref name="hg170"/> Shortly after acceptance, the aircraft's fuselage fractured just aft of the wings, due to engine vibrations while running the engines on the ground.<ref name="hg170"/> The Forssman was dismantled at that point,<ref name="hg170"/> and Bruno Steffen expressed relief at the news, as the aircraft would not be able to endanger other lives.<ref name="hg172"/>

<!-- ==Operational history== -->
<!-- == Variants == -->
<!-- ==Operators (choose)== -->

==Specifications (as modified in late 1915)==
{{aerospecs
|ref=Gray & Thetford 1962, p.571
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew= 4+
|capacity=
|length m= 16.5
|length ft= 54
|length in= 1-¾
|span m= 24
|span ft= 78
|span in=9
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m= 
|height ft= 
|height in= 
|wing area sqm= 140
|wing area sqft= 1,512
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes-->  
|wing profile=<!-- sailplane --> 
|empty weight kg= 4,000
|empty weight lb= 8,800
|gross weight kg= 5,200
|gross weight lb= 11,400
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number= 2
|eng1 type=  [[Mercedes D.III]]
|eng1 kw=<!-- prop engines --> 82
|eng1 hp=<!-- prop engines --> 110
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=2
|eng2 type= [[Mercedes D.IVa]]
|eng2 kw=<!-- prop engines -->164
|eng2 hp=<!-- prop engines -->220
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
 
|max speed kmh= 120
|max speed mph= 75
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown --> 
|cruise speed mph=<!-- if max speed unknown --> 
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km= 
|range miles= 
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m= 
|ceiling ft= 
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->38.5
|climb rate ms= 1.15
|climb rate ftmin= 226
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1= 
|armament2= 
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Siemens-Schuckert aircraft}}
* {{cite book| last=Gray |first=Peter |author2=Owen Thetford  |title=German Aircraft of the First World War |publisher=Putnam |location=London |year=1962}}
* {{cite book| last=Haddow |first=G.W. |author2=Peter M. Grosz  |title=The German Giants: The Story of the R-planes 1914–1919 |publisher=Putnam |location=London |year=1962}}
* {{cite book| last=Sollinger |first=Günther |title=Villehad Forssman: Constructing German Bombers 1914–1918 |publisher=Rusavia |location=Moscow |year=2009}}
* {{cite book| last=Taylor |first=Michael J.H. |title=Jane's Encyclopedia of Aviation |publisher=Studio Editions |location=London |year=1989}}
* {{cite web| last=Ward |first=Kevin |work=Royal Air Force Museum Aircraft Thesaurus |title=Siemens-Schuckert |publisher=Collections Trust |location=London |year=2002 |url=http://www.collectionstrust.org.uk/aircraft/16274.htm |accessdate=2011-04-25}}
<!-- ==External links== -->

{{Siemens-Schuckert aircraft}}

[[Category:German bomber aircraft 1910–1919]]
[[Category:Siemens-Schuckert aircraft|Forssman]]