{{other people}}
{{Infobox character
|series=[[24 (TV series)|24]]
|image = Jack Bauer.jpg
|caption = [[Kiefer Sutherland]] as Jack Bauer
|color = #B3001B
|color text = yellow
|name =Jack Bauer
|portrayer =[[Kiefer Sutherland]]
|family=Phillip Bauer (father)<br />[[Graem Bauer]] (brother)<br />Josh Bauer (nephew)<br />Stephen Wesley (son-in-law)<br />Teri Wesley (granddaughter) 
|spouse = [[Teri Bauer]]
|children = [[Kim Bauer]]
|significantother = [[Audrey Raines]]<br />[[Nina Myers]]<br />[[List of minor characters in 24#Other characters#24: Season 3#Claudia Hernandez|Claudia Hernandez]]<br />[[Kate Warner (character)|Kate Warner]]<br />[[Renee Walker]]
|lbl1=Days
|first=[[12:00 a.m. – 1:00 a.m.|Day 1 – Episode 1]]
|last=[[24: Live Another Day|Live Another Day – Episode 12]]
|data1=[[24 (season 1)|1]], [[24 (season 2)|2]], [[24 (season 3)|3]], [[24 (season 4)|4]], [[24 (season 5)|5]], [[24 (season 6)|6]], [[24 (season 7)|7]], [[24 (season 8)|8]], [[24: Live Another Day|9]]
|lbl2=Other appearances
|data2=''[[24: The Game]]''<br />''[[24: Redemption]]''
}}

'''Jack Bauer''' is a fictional character and the lead [[protagonist]] of the [[Fox Broadcasting Company|Fox]] television series ''[[24 (TV series)|24]]''. His character has worked in various capacities on the show, often as a member of the Counter Terrorist Unit (CTU) based in [[Los Angeles]], and working with the FBI in Washington, D.C. during season 7.
Within the ''24'' storyline, Bauer is a key member of the CTU, actually its director in Season 1, and is often portrayed as their most capable agent. Bauer's job usually involves helping prevent major terrorist attacks on the United States, saving both civilian lives and government administrations. On many occasions, Jack does so at great personal expense, as those he thwarts subsequently target him and his loved ones. He is not a crooked agent; however, Bauer's frequent use of torture to gather information has [[Critical reaction to 24#Torture|generated much controversy and discussion]].

Actor [[Kiefer Sutherland]] portrays Jack Bauer in the television show<ref>Ken Tucker, “''24'': Mondays, 9 p.m., premiering Sunday, Jan. 11, at 8 p.m.,” ''Entertainment Weekly'' 1o30 (January 16, 2009): 56.</ref> and video game. The television series was originally set to end on May 24, 2010 after eight successful seasons but was renewed for a ninth season, which premiered on May 5, 2014.<ref>{{cite web|url=http://tvline.com/2014/01/13/24-live-another-day-premiere-date/|title=Fox Announces Spring Premiere Dates for ''24: Live Another Day'', ''Surviving Jack'' and More|publisher=TVLine|first=Michael|last=Ausiello|date=January 13, 2014|accessdate=February 6, 2014}}</ref> A feature film was set to be released; however, discussions ended over a contract dispute with Fox.

[[TV Guide]] ranked him #49 on their list of "TV's Top 50 Heroes" and [[Sky 1]] listed Jack as #1 on their list of
"TV's toughest men". ''[[Entertainment Weekly]]'' named Jack Bauer one of ''The 20 All Time Coolest Heroes in Pop Culture''.<ref>{{cite web|url=http://www.ew.com/ew/gallery/0,,20268279_5,00.html |title=''Entertainment Weekly's'' 20 All Time Coolest Heroes in Pop Culture |publisher=Entertainment Weekly |date= |accessdate=2010-05-21}}</ref> In June 2010, ''Entertainment Weekly'' also named him one of the ''100 Greatest Characters of the Last 20 Years''.<ref>{{cite web|title=The 100 Greatest Characters of the Last 20 Years: Here's our full list!|url=http://popwatch.ew.com/2010/06/01/100-greatest-characters-of-last-20-years-full-list/|work=Entertainment Weekly|publisher=Time Inc.|accessdate=July 7, 2012|author=Adam B. Vary|date=June 1, 2010}}</ref> Jack is also the only character in the show to have appeared in all episodes.

==Concept and creation==
''24'' co-creator [[Joel Surnow]] commented that they did not have any actors in mind for the part; "We didn't really know who it was. We were casting a lot of people and then we heard Kiefer Sutherland's name and thought, that's Jack Bauer."<ref name="phase">{{cite web | author = Stephen M. Silverman| title = Kiefer Sutherland: $40 Million Man | work = |date = 2004-10-06| url = http://www.people.com/people/article/0,,1181709,00.html| accessdate = 2008-04-28}}</ref> In 2000, Sutherland was contacted by his friend, director [[Stephen Hopkins (director)|Stephen Hopkins]], who was working on the pilot for the experimental real-time TV show and offered him the lead.<ref>{{cite web | last=Hedegaard|first=Erik | title = Kiefer Sutherland: Heart of Darkness| work = Rolling Stone Magazine |date = 2006-04-20| url = http://www.linter.org/stories/keifergood.html| accessdate = 2008-07-04}}</ref> Initially Sutherland had reservations about playing Bauer, stating, "I thought, 'This is really clever and different, so there's no way they're going to pick it up. But I could use the money, and no one will ever see it'."<ref name="interview">{{cite web | author =Joel Schumacher | title = Kiefer Sutherland: he's been around Hollywood for more than 20 years, but these days, this bold talent has them counting the minutes | work = Interview |date = 2004-02-01| url = http://www.findarticles.com/p/articles/mi_m1285/is_1_34/ai_112482982/pg_2| accessdate = 2008-07-04}}{{dead link|date=August 2016|bot=medic}}{{cbignore|bot=medic}}</ref>

Sutherland must produce around 24 hours of film each season, "which is like making 12 movies, so there are going to be mistakes along the way, but I am incredibly surprised by how many things work well as a result of working at that pace."<ref name="interview"/>
In 2006, Sutherland signed a contract to play the role of Bauer for three seasons following [[24 (season 5)|season five]]. The contract was reported to be worth $40 million.<ref name="phase" /> Sutherland is also an [[executive producer]] of ''24''.<ref name="phase2">{{cite web | author = Jenna Fryer| title = 24 SEASON 3 Q&A with JOEL SURNOW, ROBERT COCHRAN and HOWARD GORDON | work = Phase 9 Movies |date = 2004-01-01| url = http://www.phase9.tv/moviefeatures/24season3q&a-joelsurnow2.shtml| accessdate = 2008-07-04}}</ref>

==Characterization==
Jack Bauer was born in [[Santa Monica]], California,<ref>{{cite book|last=Cerasini|first=Marc|title=24: The House Special Subcommittee's Findings atCTU|publisher=Harper Collins|year=2003|edition=First|page=7|isbn=0-06-053550-4}}</ref> on February 18, 1966,<ref name="debrief">{{cite episode|title = Debrief #2: 09:42:22 | series = 24 | serieslink = 24 (TV series) | season = 6 Debrief  |airdate=2007-05-28|credits = Writer: [[Howard Gordon]], director: [[Brad Turner (director)|Brad Turner]]}}</ref> to [[Phillip Bauer]], who placed his livelihood in his company, BXJ Technologies. The name of Jack's mother is unknown. Jack had one brother, [[Graem Bauer]]. Phillip originally planned to give the company to Jack,<ref name="s6e07">{{cite episode|title = 12:00 pm - 1:00 pm | series = 24 | serieslink = 24 (TV series) | season = 6 | number = 127|airdate=2007-02-05|credits = Writer: Howard Gordon, director: [[Jon Cassar]]}}</ref> but as Jack said in Day 6, "I just had to go my own way."<ref name="s6e10">{{cite episode|title = 3:00 pm - 4:00 pm | series = 24 | serieslink = 24 (TV series) | season = 6 | number = 130|airdate=2007-02-19|credits = Writer: Howard Gordon, director: Brad Turner}}</ref>

Jack has a [[Bachelor of Arts]] degree in [[English Literature]] from the [[University of California, Los Angeles]] and a [[Master of Science]] degree in [[Criminology]] and Law from the [[University of California, Berkeley]].<ref>{{cite web |url=http://fox.com/24/profiles/jb.htm |title=FOX Broadcasting Company: 24 |archiveurl=https://web.archive.org/web/20080404011647/http://www.fox.com/24/profiles/jb.htm |archivedate=2008-04-04}}</ref> He enlisted in the U.S. Army and later graduated from [[Officer Candidate School (United States Army)|Officer Candidate School]]. His branch was [[Infantry Branch (United States)|infantry]].  He first served in the [[US Army Special Forces|Green Berets]], and was later a member of the 1st Special Forces Operational Detachment-Delta (1st SFOD-D), popularly known as [[Delta Force]]. Among his awards and decorations are the [[Silver Star]], the [[Purple Heart]], and the [[Legion of Merit]]. Apart from Special Forces training, he received [[United States Army Airborne School|Airborne]], [[United States Army Air Assault School|Air Assault]], and [[Ranger School|Ranger]] training. He left the Army with the rank of [[Captain (United States O-3)|Captain]] after fifteen years of service.

While in the U.S. Army, he married Teri Bauer and had one child, Kim Bauer. Following his military career, Jack worked for both the [[Los Angeles Police Department]]'s [[Special Weapons and Tactics]] unit and for the Central Intelligence Agency as a case officer in the [[clandestine service]]. He was recruited into the Counter Terrorist Unit by [[Christopher Henderson]].<ref name="s5e10">{{cite episode|title = 4:00 pm - 5:00 pm | series = 24 | serieslink = 24 (TV series) | season = 5 | number = 106|airdate=2006-02-27|credits = Writer: Joel Surnow, director: Tim Iacofano}}</ref>

He has demonstrated being at the peak of physical and mental condition. He has also shown a high proficiency with firearms (typically using a [[SIG P228]] as his weapon of choice for the first and second seasons, then switching to a [[USP Compact]], until after Season 8 he switches again, this time to a [[Heckler & Koch P30|P30]]).  He also can handle explosives and electronic devices, and has a high resistance to torture (after being kidnapped by Chinese agents and tortured for almost two years, it is revealed he didn't speak a single word the whole time). He is fluent in German (Season 8) and has demonstrated some ability to either speak or understand Spanish (Seasons 1 and 3), Russian (Season 6), Arabic (Season 8), and Serbian (Season 1). He is also shown to be capable of flying planes (Season 2) and helicopters (Seasons 3, 5, 8, and 9).  Jack is a formidable hand-to-hand combatant from his years in the United States Army. He appears to be a daring and seemingly indestructible agent.

Jack Bauer wants to have a normal life and to be happy, but his commitment to his country often brings him into conflict with that goal. He is required to serve a sometimes-hypocritical bureaucracy that acknowledges his service, but will not hesitate to hold him responsible for the more extreme actions that they themselves may directly or indirectly authorize.

Jack has lost his wife Teri, murdered by a traitor who had infiltrated CTU. His relationship with his daughter Kim is at times tumultuous (Season 2, Season 5) while at other times accepting of who he is and what he must do (Season 7, Season 8). In his goal to find peace, he gets involved with Audrey Raines, the daughter of Secretary of Defense Heller, whom Jack works for. The relationship with the Secretary's daughter is all but severed when Jack makes the decision to save a material witness over Audrey's ex-husband. He lost his best friend, Tony Almeida, and former boss, Michelle, to a car bomb. He lost his friend, former President Palmer, to the same assassin who killed his friends. His girlfriend Renee Walker was killed by an assassin's bullet. His own government that he has sworn to protect has turned on him (former President Logan, President Taylor), and at the beginning of Season 7 he is on trial for his interrogation tactics.  Finally, the friend he thought was dead (Tony Almeida), betrays him when he turns to terrorism only to seek revenge for the person who killed his wife, Michelle.

Jack's final dialogue with [[Renee Walker]] in Season 7<ref name="s7e24">{{cite episode|title = 7:00 am - 8:00 am | series = 24 | serieslink = 24 (TV series) | season = 7 | number = 168|airdate=2009-05-18|credits = Writer: [[Manny Coto]] & [[Brannon Braga]], director: [[Jon Cassar]]}}</ref> offers insight into his perspective on torture and its ramifications:
{{cquote|I see fifteen people held hostage on a bus, and everything else goes out the window.  I will do whatever it takes to save them, and I mean whatever it takes. ... Laws were written by much smarter men than me. And in the end, these laws have to be more important than the 15 people on the bus.  I know that's right.  In my mind, I know that's right.  I just don't think my heart could ever have lived with it.}}

==In other media==
As the principal character in 24, Jack plays a prominent role in the television series as well as the video game. Jack is the lead protagonist of the 24 series and the books, and has appeared in every episode to date. Kiefer Sutherland has portrayed Jack Bauer in these episodes, including the ''prequels'' and the ''webisodes''. Additionally, he voiced the same character in ''24: The Game'', ''24: Day Zero'' and ''24: DVD Board Game''.

===24 Prequels===
Jack Bauer is also featured in all four prequels, that can be found on the 24 DVD releases and various websites. These prequels are designed to bridge the gap between seasons. They provide backstory into story arcs for upcoming seasons, namely insight into what Jack's actions have been leading up to the next season. Prequels have been made for Seasons 4, 5, 6, and 7.

===24 Webisodes===
'''24: Day 6 Debrief''' takes place 35 hours after the nuclear device exploded in [[Valencia, California]]. The series consists of 5 segments, no longer than three minutes in length each. Two agents, Agent Ramirez and Agent Moss, track Jack down to a hotel room and ask him to come to District for debriefing about the recovery of the tactical nukes. The Debrief consists of Agent Ramirez and other agents attempting to discover more about the supposed-death of an undercover agent, Marcus Holt, who disappeared shortly after Jack's imprisonment in China at the hands of [[Cheng Zhi]]. Since Holt was involved with the Chinese government, it is believed that Jack leaked information that led to his identity being discovered and execution. At the end of the series, the agents announce that they have no conclusive evidence about his involvement, but Ramirez promises to keep a close eye on him until he is certain. Jack tells Ramirez that if he ever sees him again, he better say "[[The Lord's Prayer]]", because it will mean he's come to kill him.

With that, Bauer is released from custody and the interrogation ends.

===24: The Game===
[[File:24thegamejackbauer2.jpg|right|thumb|120px|Bauer in ''[[24: The Game]]'', which takes place six months after Day 2.]]
''24: The Game'' takes place between Day 2 and 3. Jack begins waiting outside a ship where terrorists are going to release a ricin bomb in the water supply. A CTU Team triggers an alarm causing Jack and his team to storm the ship. Jack and his team find the whole ship's crew dead in a cargo hold. He runs into Peter Madsen, an enemy from his past. It is unclear what the story is between them, but it is stated that he framed Jack's family for a vile crime, this is hinted with Jack saying to him: "Eight years ago, my family was not involved". Madsen also states that he no longer takes orders from Jack. It's hinted that Madsen was under Jack's command either in the army or in a SWAT team, and Madsen betrayed Jack. Madsen kidnaps Jack's daughter Kim, and later Kate Warner. Jack finally kills Madsen near the end of the game when he tries to escape by shooting up his speedboat with a [[Zastava M80]] assault rifle, causing it to explode. He also kills Max, who was holding Kate hostage, saving her life. However Max managed to shoot Jack once before dying, the second of two times he was shot in the final hour of the game, the first time by Madsen. As a result, Chase Edmunds takes Jack to the hospital via helicopter.

===24 Toys===
Diamond Select Toys released 1/6 scale figures based on Jack Bauer:
*2008- Jack Bauer 8:00 AM
*2009- Jack Bauer 3:00 PM Season 1
*2009- Jack Bauer 9:00 PM Season 1

Diamond Select Toys released 1/24 scale Minimates based on 24:
*2007- Season 1 Box Set (Jack Bauer, Nina Myers, David Palmer, Kim Bauer)
*2007- End of Day 1 Two-Pack, PX Exclusive (Jack Bauer, Andre Drazen)
*2007- Season 2 Box Set (Season 2 Jack Bauer, Tony Almeda, Michelle Dessler, George Mason)
*2007- End of Day 2 Two-Pack, Suncoast/FYE Exclusive (Stretcher Jack Bauer, Prisoner Nina Myers)
*Canceled- Season 3 Box Set (Undercover Jack Bauer, Sherry Palmer, Chloe O'Brien, Chase Edmunds)
*Canceled- End of Day 3 Two-Pack (Jack Bauer, Stephen Saunders)

Enterbay released 1/6 scale figures based on 24:
*2009- Jack Bauer
*2009- President David Palmer

[[McFarlane Toys]] released 1/12 scale figures based on Jack Bauer:
*2007- Jack Bauer Boxed Set 1
*2007- Jack Bauer Boxed Set 2

Medicom Toy (Japan) released 1/6 scale figures based on Jack Bauer in their Real Action Heroes line:
*2005- Jack Bauer (Suit) Season 4 7:00 am – 8:00 am
*2005- Jack Bauer (Tac) Season 4 11:00 am – 12:00 pm
*2007- Jack Bauer Season 5

===24 comic books===
IDW has released a series of comic books based on the adventures of Jack Bauer and other members of the 24 Cast. 24's Comic License is published by [[IDW Publishing]]
*24: One Shot (takes place on Jack's first day on the job at the Los Angeles Unit of the Counter Terrorist Unit)
*24: Midnight Sun (Jack has to stop a rough environmentalist group from using explosives to get their point across)
*24: Stories (takes place in the 3-year gap between Seasons 2&3. In Stories, Jack is newly undercover with [[Ramon Salazar (24 character)|Ramon Salazar]].
*24: Nightfall&nbsp;— A tale shedding more light on the background and characters of Day 1, including Victor Drazen. Written by J. C. Vaughn and Mark L. Haynes
*24: Cold Warriors&nbsp;— An original tale of intrigue featuring Jack Bauer and Chloe O'Brian, set in Alaska. Written by [[Beau Smith]] and Steve Bryant

OTHER MEDIA:

===''The Simpsons''===
Jack Bauer appeared in the [[The Simpsons (season 18)|season 18]] episode [[24 Minutes]] of ''[[The Simpsons]]''. In the episode, he is accidentally called by [[Bart Simpson|Bart]] in the middle of a shooting, who then plays a crank call on him (as he did to Moe Syzlack in the early seasons of "The Simpsons"). Enraged, he reappears at the end of the episode, having called all agents of his organization and abandoning his prior mission so that Jack could hunt Bart down, and arrest him.  A nuclear bomb then goes off in the distance, but everyone sighs with relief after Bauer reassures them that the nuclear bomb went off in [[Springfield (The Simpsons)#Shelbyville|Shelbyville]].

==Critical reception==

{{See also|Critical reaction to 24}}

Having won an [[Emmy Award]] (with 5 other nominations), a [[Golden Globe]] (with 4 other nominations), two [[SAG Awards]] (with 3 other nominations) and two [[Satellite Awards]], Kiefer Sutherland's performance as Jack Bauer became (award-wise) the third most acclaimed Male Lead Dramatic TV performance of the 2000s behind [[James Gandolfini]] (with 3 Emmys, 1 Golden Globe and 3 SAG Awards) and [[Bryan Cranston]] (with 4 Emmys, 1 Golden Globe, 2 SAG Awards, and 4 Satellite Awards). He is also one of only two Male Lead Drama TV Actors ever to have won all 4 awards, the other being [[Bryan Cranston]] for his role as [[Walter White (Breaking Bad)|Walter White]] on [[Breaking Bad]].

''[[Entertainment Weekly]]'' put it on its end-of-the-decade, "best-of" list, saying, "When Kiefer Sutherland's 24 superagent barks "Dammit, Chloe—we're running out of time!" America's ass is about to be saved in some new, heart-stopping way."<ref>Geier, Thom; Jensen, Jeff; Jordan, Tina; Lyons, Margaret; Markovitz, Adam; Nashawaty, Chris; Pastorek, Whitney; Rice, Lynette; Rottenberg, Josh; Schwartz, Missy; Slezak, Michael; Snierson, Dan; Stack, Tim; Stroup, Kate; Tucker, Ken; Vary, Adam B.; Vozick-Levinson, Simon; Ward, Kate (December 11, 2009), "THE 100 Greatest MOVIES, TV SHOWS, ALBUMS, BOOKS, CHARACTERS, SCENES, EPISODES, SONGS, DRESSES, MUSIC VIDEOS, AND TRENDS THAT ENTERTAINED US OVER THE PAST 10 YEARS".

Also, in the web series Key of Awesome, the main character has a man crush on Jack Bauer, but is not gay.
Entertainment Weekly. (1079/1080):74-84</ref>

Some American politicians and lawyers have used Jack Bauer and his actions to frame the debate on American [[Enhanced interrogation techniques|interrogation techniques]] (in this case, "torture"), which have become an object of intense controversy.<ref name="torture 1">{{cite web|url=http://www.washingtonpost.com/wp-dyn/content/blog/2008/04/21/BL2008042101378_pf.html|title=Duped About Torture|last=Froomkin|first=Dan|date=2008-04-21|publisher=The Washington Post|accessdate=2009-01-24}}</ref><ref name="torture 2">{{cite news|url=https://www.theguardian.com/world/2008/apr/19/humanrights.interrogationtechniques|title=Stress, hooding, noise, nudity, dogs | last = Sands | first = Phillip | date = 2008-04-19 | publisher=Guardian |accessdate=2009-01-24}}</ref>  As well, at a 2007 legal conference in [[Ottawa]], Canada, Jack Bauer's tactics were raised during a panel discussion about "terrorism, torture and the law". Panel members, including U.S. Supreme Court Justice [[Antonin Scalia]], discussed the use of torture "in times of great crisis".<ref>{{cite news|url=http://blogs.wsj.com/law/2007/06/20/justice-scalia-hearts-jack-bauer/|title=Justice Scalia Hearts Jack Bauer|last=Lattman|first=Peter|date=June 20, 2007|publisher=Wall Street Journal|accessdate=2009-12-09}}</ref> Scalia was quoted as saying: "Jack Bauer saved Los Angeles. He saved hundreds of thousands of lives. Is any jury going to convict Jack Bauer? I don't think so." <ref>{{cite news|url=http://www.theglobeandmail.com/news/national/what-would-jack-bauer-do/article687726/|title=What would Jack Bauer do?|last=Freeze|first=Colin|date=June 16, 2007|publisher=The Globe and Mail|accessdate=2017-01-26}}</ref>

Several philosophers have used Jack Bauer's character to settle challenging moral questions.  Twenty philosophers contributed to a 2007 book, ''24 and Philosophy: The World According to Jack'', which assesses Bauer through the eyes of famous philosophers.<ref>[http://www.amazon.com/24-Philosophy-World-According-Jack/dp/1405171049 Link to Amazon page on '24 and Philosophy: the World According to Jack' (book)], accessed July 20th 2014.</ref> For the ''[[24: Live Another Day]]'' series, in which anti-hero Jack Bauer and anti-villain Margot Al-Harazi have similar ethical viewpoints, some philosophers have resorted to [[How to Make Good Decisions and Be Right All the Time|Iain King's quasi-utilitarian approach]] to determine that Bauer is morally superior.<ref>{{cite web |url=http://andphilosophy.com/2014/07/16/24-and-philosophy/ |title=24 and Philosophy |publisher=The Blackwell Philosophy and Pop Culture Series |accessdate=July 20, 2014}}</ref>

==References==
{{reflist|2}}

==External links==
*{{cite web |url=http://www.fox.com/24/characters/#bio:jack-bauer |title=Jack Bauer's Official Character Profile |archiveurl=https://web.archive.org/web/20080404011647/http://www.fox.com/24/profiles/jb.htm |archivedate=2008-04-04}}

{{24 (TV series)}}

{{DEFAULTSORT:Bauer, Jack}}
[[Category:24 (TV series) characters]]
[[Category:Fictional American people of German descent]]
[[Category:Fictional American people of Canadian descent]]
[[Category:Fictional characters from Los Angeles]]
[[Category:Fictional characters introduced in 2001]]
[[Category:Fictional vigilantes]]
[[Category:Fictional Krav Maga practitioners]]
[[Category:Fictional government employees]]
[[Category:Fictional United States Army Delta Force personnel]]
[[Category:Fictional United States Army Special Forces personnel]]
[[Category:Fictional military captains]]
[[Category:Fictional secret agents and spies]]
[[Category:Fictional government agents]]
[[Category:Fictional torturers]]