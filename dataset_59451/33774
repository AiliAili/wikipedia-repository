{{featured article}}
{{Infobox Wrestling event
|name       = SummerSlam (2003)
|image      = SummerSlam 2003 poster.jpg
|alt        = 
|caption    = Promotional poster featuring [[Sable (wrestler)|Sable]]
|theme      = "[[St. Anger (song)|St. Anger]]" by [[Metallica]]<ref>{{cite web|url=http://www.wrestleview.com/news/1060371085.shtml|title=Full WWE SmackDown Results – 08/07/03 (Brock Lesnar turns heel and more)|last=Ouellette|first=Christopher|date=2003-08-07|publisher=WrestleView|accessdate=2008-07-10|quote=Metallica's 'St Anger' is the theme song of SS this year.}}</ref>
|promotion  = [[World Wrestling Entertainment]]
|sponsor    = Stacker 2
|brand      = [[WWE Raw|Raw]]<br />[[WWE SmackDown|SmackDown!]]
|date       = August 24, 2003
|venue      = [[America West Arena]]
|city       = [[Phoenix, Arizona]]
|attendance = 16,113
|lastevent  = [[Vengeance (2003)]]
|nextevent  = [[Unforgiven (2003)]]
|event      = [[SummerSlam]]
|lastevent2 = [[SummerSlam (2002)]]
|nextevent2 = [[SummerSlam (2004)]]
}}
'''SummerSlam (2003)''' was a [[professional wrestling]] [[pay-per-view]] (PPV) event produced by [[World Wrestling Entertainment]] (WWE) and [[Sponsor (commercial)|presented]] by Stacker 2's YJ Stinger. It took place on August&nbsp;24, 2003, at the [[America West Arena]] in [[Phoenix, Arizona]].<ref>{{cite web|url=http://www.wwe.com/shows/summerslam/history/2003/venue/ |title=SummerSlam (2003) Venue |publisher=WWE |accessdate=2008-07-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20070630015620/http://www.wwe.com/shows/summerslam/history/2003/venue/ |archivedate=June 30, 2007 }}</ref> It was the 16th annual [[SummerSlam]] event and starred [[List of WWE personnel|wrestlers]] from the [[WWE Raw|Raw]] and [[WWE SmackDown|SmackDown!]] [[WWE Brand Extension|brands]].

Nine [[professional wrestling]] matches were set on the event's [[supercard]], a scheduling of multiple high-level matches. The first [[card (sports)#main event|main event]] was an [[Elimination Chamber]] match, in which [[World Heavyweight Championship (WWE)|World Heavyweight Champion]] [[Triple H]] defeated [[Chris Jericho]], [[Bill Goldberg|Goldberg]], [[Kevin Nash]], [[Randy Orton]], and [[Shawn Michaels]] to retain his championship. In the other main event, defending [[WWE Championship|WWE Champion]] [[Kurt Angle]] defeated [[Brock Lesnar]] in a [[professional wrestling match types#Variations of singles matches|standard wrestling match]]. The [[undercard]] included [[Glenn Jacobs|Kane]] defeating [[Rob Van Dam]] in a [[Professional wrestling match types#No Holds Barred match|No Holds Barred match]], and [[Eddie Guerrero]] defending his [[WWE United States Championship|United States Championship]] against [[Chris Benoit]], [[Rhino (wrestler)|Rhyno]] and [[Yoshihiro Tajiri|Tajiri]].

The event marked the second time the Elimination Chamber format was used by WWE; the first was at [[Survivor Series (2002)|Survivor Series 2002]]. SummerSlam (2003) grossed over $715,000 ticket sales from an attendance of 16,113 and received about 415,000&nbsp;pay-per-view buys, more than the [[SummerSlam (2004)|following year's event]]. This event helped WWE increase its pay-per-view revenue by $6.2&nbsp;million from the previous year.

==Storylines==
{{see also|Professional wrestling}}
The [[Professional wrestling match types|professional wrestling matches]] at SummerSlam featured professional wrestlers performing as [[Character (arts)|characters]] in [[Kayfabe|scripted events]] pre-determined by the hosting [[Professional wrestling promotion|promotion]], World Wrestling Entertainment (WWE).<ref>{{cite web|url=http://entertainment.howstuffworks.com/pro-wrestling.htm|title=How Pro Wrestling Works|last=Grabianowski|first=Ed|work=HowStuffWorks|publisher=[[Discovery Communications]]|accessdate=March 5, 2012<!--|archiveurl=http://www.webcitation.org/6LDla2zy5|archivedate=November 18, 2013-->}}</ref><ref>{{cite web|url=http://corporate.wwe.com/company/events.jsp|title=Live & Televised Entertainment|publisher=[[WWE]]|accessdate=March 21, 2012|archiveurl=http://www.webcitation.org/6LDleY7Lr|archivedate=November 18, 2013}}</ref> Storylines between the characters were produced on WWE's weekly television shows ''Raw'' and ''SmackDown!'' with the Raw and SmackDown! brands—storyline divisions in which WWE assigned its employees to different programs.<ref>{{cite press release|url=http://corporate.wwe.com/news/2002/2002_03_27.jsp|title=WWE to make Raw and SmackDown! distinct TV brands|publisher=[[WWE]]|date=March 27, 2002|accessdate=April 5, 2012|archivedate=April 17, 2010|archiveurl=https://web.archive.org/web/20100417115226/http://corporate.wwe.com/news/2002/2002_03_27.jsp}}</ref>

[[File:Triple H-WorldHeavyweight-Champ@Commons.jpg|thumb|left|upright|World Heavyweight Champion Triple H]]
On July 22, 2003, during the SummerSlam [[News conference|press conference]], [[Professional wrestling authority figures#General Managers|authority figure]] [[Eric Bischoff]] announced that [[Triple H]] would defend the [[World Heavyweight Championship (WWE)|World Heavyweight Championship]] against Goldberg at the event.<ref name="Raw7.22a">{{cite web|url= http://www.wrestleview.com/news/1059451065.shtml|title=Shane McMahon returns to confront Kane|last=Martin|first=Adam|date= 2003-07-28|publisher=WrestleView|accessdate=2008-07-03}}</ref><ref name="Raw7.22b">{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_4709.shtml|title=7/28 WWE Raw: Powell's virtual time coverage (hour one)|last=Powell|first=Jason|date= 2003-07-28|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref> On the August 4, 2003 episode of ''Raw'', Bischoff made the match stipulations [[Professional wrestling match types#No Disqualification match|No Disqualifications]]. Later that night, another authority figure, [[Stone Cold Steve Austin|Steve Austin]], altered Bischoff's announcement, stating that the title would be contested in the [[Elimination Chamber]], with Triple H defending his title against Goldberg, Chris Jericho, Kevin Nash, Randy Orton, and Shawn Michaels.<ref name="Raw8.4a">{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_4800.shtml|title=Keller's Raw Report 8/4: Ongoing "virtual time" analysis of live show |last=Keller|first=Wade|date=2003-08-04|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref><ref name="Raw8.4b">{{cite web|url=http://raw.wwe.com/results/080403/results.html|title=Big Red Assault on Shane-O Mac|date=2003-08-04|publisher=WWE|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20031205114124/http://raw.wwe.com/results/080403/results.html|archivedate=2003-12-05}}</ref> On the August 18, 2003 episode of ''Raw'' the [[Feud (professional wrestling)|feud]] among the six men intensified during a [[Pop (professional wrestling)|promotional interview segment]], where each superstar in the Elimination Chamber discussed the match and taunted the other wrestlers. During the main event, where Orton faced Goldberg, Nash interfered in the match and attacked Goldberg. Michaels then entered the ring, but as he was about to hit Triple H with the World Heavyweight Championship belt, Jericho ran into the ring and hit Michaels with a [[Folding chair|chair]].<ref name="Raw8.18a">{{cite web|url=http://www.wrestleview.com/news/1061266449.shtml|title=Full WWE Raw Results −8/18/03 -Chris Jericho vs. Kevin Nash (Hair vs. Hair)|last=Williams|first=Jeff|date=2003-08-18|publisher=WrestleView|accessdate=2008-07-03}}</ref><ref name="Raw8.18b">{{cite web|url=http://raw.wwe.com/results/081803/results.html|title=Out of Control|date=2003-08-18|publisher=WWE|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20031205115544/http://raw.wwe.com/results/081803/results.html|archivedate=2003-12-05}}</ref>

[[File:KurtAngleSs05.jpg|thumb|right|WWE Champion Kurt Angle]]
On the July 31, 2003 episode of ''SmackDown!'', during an interview promotion in the ring, [[Brock Lesnar]] challenged [[Kurt Angle]] to a [[WWE Championship]] rematch from [[Vengeance (2003)|Vengeance]], WWE's previous pay-per-view event. The WWE Chairman, [[Vince McMahon]], decided that Lesnar would have to earn his rematch by competing in a [[Cage match|steel cage match]] against Vince himself the next week on SmackDown!, with Angle officiating as a [[Professional wrestling match types#Special referee|special guest referee]].<ref name="SD7.31a">{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_4758.shtml|title=7/31 WWE Smackdown review: Angle & Lesnar test their friendship|last=Radican|first= Sean|date=2003-07-31|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref><ref name="SD7.31b">{{cite web|url=http://smackdown.wwe.com/results/073103/results.html|title=Caged Aggression|date=2003-07-31|publisher=WWE|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20041022042036/http://smackdown.wwe.com/results/073103/results.html|archivedate=2004-10-22}}</ref> The Steel Cage match resulted in a no-contest after McMahon and Lesnar attacked Angle.<ref name="SD8.7a">{{cite web|url=http://smackdown.wwe.com/results/080703/results.html|title=Caged Destruction|date=2003-08-07|publisher=WWE|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20041009210228/http://smackdown.wwe.com/results/080703/results.html |archivedate=2004-10-09}}</ref><ref name="SD8.7b">{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_4869.shtml|title=8/7 WWE Smackdown review: Radican's reaction to program|last=Radican|first=Sean|date=2003-08-10|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref> On August 14, 2003 during an episode of ''SmackDown!'', McMahon announced that Angle would defend the championship against Lesnar at SummerSlam.<ref name="SD8.14a">{{cite web|url=http://smackdown.wwe.com/results/081403/results.html|title=Stephanie Derailed!|date=2003-08-14|publisher=WWE|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20041022235521/http://smackdown.wwe.com/results/081403/results.html|archivedate=2004-10-22}}</ref><ref name="SD8.14b">{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_4934.shtml|title=8/14 WWE Smackdown review: Steph vs. A-Train headlines|last=Radican|first=Sean|date=2003-08-14|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref>

On the June 23, 2003 episode of ''Raw'', [[Kane (wrestler)|Kane]] removed his mask and exposed his face in front of [[Rob Van Dam]] and the crowd after losing to [[Triple H]] during a [[World Heavyweight Championship (WWE)|World Heavyweight Championship]] in a Mask vs. Title match. Two weeks later on ''Raw'', Kane attacked Van Dam backstage.<ref>{{cite web|url= http://pwtorch.com/artman2/publish/TV_Reports_9/article_4513.shtml|title=7/14 WWE Raw: Powell's virtual time coverage (hour one)|last=Powell|first=Jason|date=2003-07-07|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref><ref>{{cite web|url=http://raw.wwe.com/results/070703/results.html|title=Time to Move On|date=2003-07-07|publisher=WWE|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20031205111901/http://raw.wwe.com/results/070703/results.html|archivedate=2003-12-05}}</ref> The next week on Raw, Eric Bischoff gave Van Dam a match against Kane,<ref>{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_4515.shtml|title=7/14 WWE Raw: Powell's virtual time coverage (hour two)|last=Powell|first=Jason|date=2003-07-14|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref><ref>{{cite web|url=http://raw.wwe.com/results/071403/results.html|title=Kane sets Fire to J.R|date=2003-07-14|publisher=WWE ([[Internet Archive]])|accessdate=2008-07-03|archiveurl=https://web.archive.org/web/20031204052244/http://raw.wwe.com/results/071403/results.html|archivedate=2003-12-04}}</ref> which took place the next week on ''Raw'' and ended in a no-contest. On August 4, 2003, [[Shane McMahon]], Mr. McMahon's son, scheduled a No Disqualification match between both men for SummerSlam.

On the August 7, 2003 episode of ''SmackDown!'', [[WWE United States Championship|United States Champion]] [[Eddie Guerrero]] faced [[Chris Benoit]]. However, during the match, [[Rhino (wrestler)|Rhyno]] and [[Yoshihiro Tajiri|Tajiri]] interfered, thus the match ended in a no-contest. [[Sgt. Slaughter]], a WWE official, scheduled Guerrero and Benoit to face Rhyno and Tajiri in a tag team match, which Guerrero and Benoit won. The next week on ''SmackDown!'', a SummerSlam advertisement announced that Guerrero would defend the United States Championship against Benoit, Rhyno, and Tajiri.

==Event==
{| class="wikitable collapsible collapsed" style="float:right; border:1px; font-size:90%; margin-left:1em;"
! colspan="2" style="width:250px;"|Other on-screen talent<ref name="Facts">{{cite web|url=http://www.hoffco-inc.com/wwe/ppv/ppv/sum03.html|title=SummerSlam (2003) Information|publisher=Hoff Co.|accessdate=2008-07-11}}</ref>
|-
!Role:
!Name:
|-
|rowspan="4"|[[Sports caster|Commentator]]
|[[Jerry Lawler]] {{small|(Raw)}}
|-
|[[Jim Ross]] {{small|(Raw)}}
|-
|[[Michael Cole (wrestling)|Michael Cole]] {{small|(SmackDown!)}}
|-
|[[Tazz]] {{small|(SmackDown!)}}
|-
|rowspan="8"|Referee
|[[Mike Chioda]] {{small|(SmackDown!)}} 
|-
|[[Jack Doan]] {{small|(Raw)}}
|-
|[[Brian Hebner]] {{small|(SmackDown!)}}
|-
|[[Earl Hebner]] {{small| (Raw)}}
|-
|[[Nick Hamilton|Nick Patrick]] {{small| (Raw)}}
|-
|[[Chad Patton]] {{small| (Raw)}}
|-
|[[Charles Robinson (referee)|Charles Robinson]] {{small| (Raw)}}
|-
|[[Mike Sparks]] {{small| (SmackDown!)}}
|-
|rowspan="2"|[[Ring announcer]]
|[[Howard Finkel]] {{small| (Raw)}}
|-
|[[Tony Chimel]] {{small| (SmackDown!)}}
|}
===Sunday Night Heat===
Before the event aired [[live television|live]] on [[pay-per-view]], [[Matt Hardy]] faced [[Zach Gowen]] on ''[[WWE Heat|Sunday Night Heat]]'', one of WWE's secondary TV programs. Gowen, however, was unable to compete due to [[legit (professional wrestling)|legit]] injuries he sustained on the August 21, 2003 episode of ''SmackDown!''. In result, Hardy was declared the winner via forfeit. 

In the next match, [[Rey Mysterio]] faced [[Shannon Moore]] for the [[WWE Cruiserweight Championship (1991-2007)|WWE Cruiserweight Championship]], in which [[Rey Mysterio]]. Mysterio [[pin (professional wrestling)|pinned]] Moore to retain the title after he performed [[Tiger feint kick|619]].<ref>{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_5031.shtml|title=8/24 WWE Heat review: Detailed rundown of pre-Summerslam show |last=Giebink|first=Dusty|date=2003-08-24|publisher=Pro Wrestling Torch|accessdate=2008-07-03}}</ref>

===Preliminary matches===
After ''Sunday Night Heat'', the pay-per-view event began with a [[tag team#Tag team match rules|tag team match]] for the [[World Tag Team Championship (WWE)|World Tag Team Championship]]. The champions, [[La Résistance (professional wrestling)|La Résistance]] ([[René Goguen|René Duprée]] and [[Sylvain Grenier]]), defended their titles against [[The Dudley Boyz]] ([[Mark LoMonaco|Bubba Ray]] and [[Devon Hughes|D-Von]]). Throughout the match, both teams performed many offensive maneuvers, though The Dudley Boyz were able to gain the upper hand when they executed a [[Professional wrestling double-team maneuvers#Death Drop|3D]] on Duprée. As D-Von [[Lateral press|covered]] Duprée, [[Rob Conway]], who was disguised as a cameraman, hit D-Von with a camera while the referee was distracted. Duprée then covered D-Von for a successful pinfall to retain the championship.<ref name="WrestleView">{{cite web|url=http://www.wrestleview.com/news/1061780525.shtml|title=Full WWE SummerSlam PPV Results – 8/24/03 – Phoenix, Arizona|last=Martin|first=Adam|date=2003-08-24|publisher=WrestleView|accessdate=2008-07-03}}</ref><ref name="Slam">{{cite web|url=http://slam.canoe.ca/Slam/Wrestling/2003/08/25/168663.html|title=Triple H retains at SummerSlam|last=Plummer|first=Dale|author2=Nick Tylwalk |publisher=Canoe: Slam Wrestling|accessdate=2008-07-03}}</ref>

[[File:Team3dlockdown.jpg|thumb|left|The Dudley Boyz faced La Résistance for the World Tag Team Championship]]
The following match pitted [[The Undertaker]] against [[Matt Bloom|A-Train]] in a standard match. In the early stages both competitors wrestled inconclusively before The Undertaker gained the advantage. He attempted to lift A-Train [[Tombstone piledriver]]. A-Train countered it, in the process knocking the referee down. He attempted to take advantage of the situation by trying to hit The Undertaker with a [[folding chair|chair]]. The Undertaker, however, countered the attack with his boot, causing the chair to hit A-Train in the face. The Undertaker then [[chokeslam]] A-Train and, since the referee had recuperated, covered his opponent for the pinfall.<ref name="Almanac">{{cite book|title= 2007 Wrestling Almanac & Book of Facts|publisher=Kappa Publishing|edition=Wrestling's Historical Cards|volume=2007|pages=113–114}}</ref>

The third contest had Shane McMahon against Eric Bischoff in a standard match. McMahon and Bischoff began by brawling on the arena ramp, as [[Jonathan Coachman]] appeared from the backstage area and hit McMahon with a folding chair. Bischoff grabbed a microphone and announced that the match would be contested under [[Falls count anywhere match|no disqualification, falls count anywhere regulations]]; as a result, Bischoff could not be disqualified for Coachman's interference. Coachman and Bischoff performed [[Professional wrestling double-team maneuvers|double-team]] attacks on McMahon until Steve Austin interfered by performing a [[Stunner (professional wrestling)|Stone Cold Stunners]] on Coachman and Bischoff. After Austin's interference, McMahon positioned Bischoff on the television commentators' table, performed a [[Diving elbow drop|Leap of Faith]] onto Bischoff's chest, thereby breaking the table and covered Bischoff for the pinfall.<ref name="WrestleView"/><ref name="PWTorch">{{cite web|url=http://pwtorch.com/artman2/publish/PPV_Reports_5/article_5029.shtml|title=8/24 WWE SummerSlam PPV: Ongoing "virtual time" results of live event|authorlink=Wade Keller|last=Keller|first=Wade|date=2003-08-24|publisher=Pro Wrestling Torch|accessdate=2008-07-06}}</ref>

[[File:Eddie Guerrero on SmackDown cropped.jpg|thumb|right|Eddie Guerrero defended the WWE United States Championship in a Fatal Four Way match]]
The next match involved four wrestlers from the Smackdown! brand in a match for the WWE United States Championship; Eddie Guerrero defended the title against Chris Benoit, Rhyno, and Tajiri. The match began with Guerrero wrestling with Tajiri, while Benoit wrestled with Rhyno. During the encounter Guerrero applied a [[Texas cloverleaf|Lasso From El Passo]] on Tajiri, while Benoit employed a [[Crippler Crossface]] on Rhyno. Afterwards, Tajiri applied a [[Boston crab#Rope hung Boston crab|Tarantula]] on Benoit. The hold distracted the referee, which allowed Guerrero to hit Rhyno with the United States Championship belt. Tajiri then attempted to hit Benoit with a [[roundhouse kick|Buzzsaw Kick]], but Benoit countered the maneuver by lifting and sitting Tajiri onto his shoulders. Tajiri, however, countered by tossing both himself and Benoit over the top rope onto the arena floor. Capitalizing on the situation, Guerrero then performed a [[Frog splash]] on Rhyno, after which he scored the pinfall, thus retaining the WWE United States Championship.<ref name="Results">{{cite web|url=http://www.wwe.com/shows/summerslam/history/2003/results/ |title=SummerSlam (2003) Results |publisher=WWE |accessdate=2008-07-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20070619060949/http://www.wwe.com/shows/summerslam/history/2003/results/ |archivedate=June 19, 2007 }}</ref>

===Main event matches===
The fifth match was the main event from the SmackDown brand, a standard match for the WWE Championship, in which Kurt Angle defended the title against Brock Lesnar. At the beginning of the match, Lesnar tried to walk away from the ring, but Angle brought him back. There, Angle performed many offensive maneuvers, including [[DDT (professional wrestling)|DDT]] and [[Professional wrestling throw#Olympic slam|Angle Slam]]. He then applied an [[Professional wrestling holds#Ankle lock|ankle lock]] on Lesnar. During this tussle, Lesnar countered the hold but knocked down the referee. Angle applied a [[Professional wrestling holds#Guillotine choke|guillotine choke]] on Lesnar, which brought Lesnar down onto his knees and allowed Angle to perform another ankle lock. Mr. McMahon, who had accompanied Lesnar to the ring, came into the ring and hit Angle's back with a folding chair to break the submission hold. Because the referee was incapacitated, Lesnar was not disqualified for the interference. Afterwards, Lesnar twice attempted to lift Angle onto his [[Facebuster#F-5|F-5]]. During the second attempt, however, Angle countered the throw into another ankle lock, which forced Lesnar to [[Professional wrestling#Submission|submit]]. As a result, Angle retained the WWE Championship.<ref name="Slam" /><ref name="Main events">{{cite web|url=http://www.wwe.com/shows/summerslam/history/2003/mainevent/ |title=SummerSlam (2003): The Main Events |publisher=WWE |accessdate=2008-07-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20070620181530/http://www.wwe.com/shows/summerslam/history/2003/mainevent/ |archivedate=June 20, 2007 }}</ref>

In a preliminary match from the Raw brand, Kane faced Rob Van Dam in a No Disqualification match. Both wrestlers used a ladder to their advantage early in the match. After Kane used the ladder on Van Dam, he attempted to hit Van Dam with a [[Diving clothesline|flying clothesline]]. However, Van Dam moved out of the way and Kane landed on the arena barricade. Van Dam performed a variation of a [[Rolling Thunder (professional wrestling)|rolling thunder]] on Kane using a folding chair. Following this, Van Dam attempted to hit Kane with a [[Professional wrestling attacks#Spinning heel kick|Van Daminator]] with a chair, but Kane rolled out of the ring to avoid the attack. Van Dam then attempted an aerial technique from inside the ring towards Kane, but Kane caught Van Dam in mid-air and executed a tombstone piledriver, after which he covered Van DAm for the pinfall.<ref name="Results" />

[[File:Elimination chamber nyr06.jpg|left|thumb|The Elimination Chamber structure]]
The main event from the Raw brand was the Elimination Chamber match for the World Heavyweight Championship, in which Triple H defended the title against Chris Jericho, Goldberg, Kevin Nash, Randy Orton, and Shawn Michaels. The match began with Jericho and Michaels in the ring, while Goldberg, Nash, Orton, and Triple H were locked in the chambers. Michaels and Jericho wrestled, with neither of them gaining the advantage over the other. Orton and Nash were the third and fourth entrants into the match, respectively. Nash was the first wrestler eliminated from the match after Michaels executed a [[superkick]] and Chris Jericho covered him for a pinfall. Triple H and Goldberg were the fifth and sixth entrants. Michaels performed another superkick on Triple H as soon as he exited his chamber, and as a result, Triple H was knocked back into his chamber. As soon as Goldberg entered the match, he performed a [[Spear (wrestling)|spear]] for a pinfall to eliminate Orton. Next, Goldberg performed a [[Suplex#Suplex slam|Jackhammer]] on Michaels and Jericho, eliminating both via pinfall. This left Goldberg alone with and Triple H, who at the time had remained inside the chamber. Goldberg performed a spear on him through the glass of the chamber, in the process pushing him out of the chamber. [[Ric Flair]], who was managing both Triple H and Orton, then handed  Triple H a [[sledgehammer]]. Goldberg attempted another spear on Triple H, who countered the maneuver by hitting Goldberg with the sledgehammer and covered him for the pinfall. Triple H therefore retained the World Heavyweight Championship.<ref name="Slam" /><ref name="Main events" />

==Aftermath==
[[File:Bill goldberg.jpg|thumb|right|Goldberg, who feuded with Triple H after SummerSlam]]
During an episode of ''Raw'' after SummerSlam, Goldberg challenged Triple H to another match for the World Heavyweight Championship. This match took place at the [[Unforgiven (2003)|Unforgiven]] pay-per-view on September 21, 2003 with a stipulation that, should he lose, Goldberg would retire from WWE.<ref name="Raw8.25b">{{cite web|url=http://raw.wwe.com/results/082503/results.html |title=Burn in Hell|publisher=WWE ([[Internet Archive]])|accessdate=2008-07-10|archiveurl=https://web.archive.org/web/20031204052244/http://raw.wwe.com/results/082503/results.html|archivedate=2003-12-04}}</ref><ref name="Raw9.8a">{{cite web|url=http://www.wrestleview.com/news/1063082079.shtml|title=Full WWE Raw Results – 9/08/03 – Huntsville, AL (Kane vs. RVD in a cage)|last=Lyle|first=Stephanie|date=2003-09-08|publisher=WrestleView|accessdate=2008-07-10}}</ref> Goldberg defeated Triple H to become the new champion.<ref>{{cite web|url=http://www.wwe.com/shows/unforgiven/history/2003/mainevent/ |title=Triple H vs. Goldberg for the World Heavyweight Championship |publisher=WWE |accessdate=2008-07-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20081222160552/http://www.wwe.com/shows/unforgiven/history/2003/mainevent/ |archivedate=December 22, 2008 }}</ref><ref>{{cite web|url=http://www.wwe.com/shows/unforgiven/history/2003/results/ |title=Unforgiven 2003 Results |publisher=WWE |accessdate=2008-07-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20080331203746/http://www.wwe.com/shows/unforgiven/history/2003/results/ |archivedate=March 31, 2008 }}</ref> After SummerSlam, Kurt Angle focused his attention on The Undertaker, whom he wrestled in a match for the WWE Championship during an episode of ''SmackDown!'' on September 4, 2003. During the match, Lesnar attacked both wrestlers with a folding chair,<ref name="SD9.4a">{{cite web|url=http://smackdown.wwe.com/results/090403/results.html|title=Vince unlocks a monster|publisher=WWE|accessdate=2008-07-11|archiveurl=https://web.archive.org/web/20040603055059/http://smackdown.wwe.com/results/090403/results.html|archivedate=2004-06-03}}</ref><ref name="SD9.4b">{{cite web|url=http://www.wrestleview.com/news/1062736326.shtml|title=Full WWE SmackDown Results – 9/4/03 ('Taker/Angle for WWE Title, more)|last=Sicilliano|first=Mike|date=2003-09-04|publisher=WrestleView|accessdate=2008-07-11}}</ref> leading to an [[Iron Man match]] between Angle and Lesnar. Lesnar won five falls during the match, while Angle won four, and as a result Lesnar won the title.<ref name="SD9.18a">{{cite web|url=http://smackdown.wwe.com/results/091803/results.html|title=Brock tops Kurt|publisher=WWE|accessdate=2008-07-11|archiveurl=https://web.archive.org/web/20040701041730/http://smackdown.wwe.com/results/091803/results.html |archivedate=2004-07-01}}</ref><ref name="SD9.18b">{{cite web|url=http://www.wrestleview.com/news/1063942068.shtml|title=Full WWE SmackDown Results – 9/18/03 – Raleigh, NC (New WWE champion)|last=Sicilliano|first=Mike|date=2003-09-18|publisher=WrestleView|accessdate=2008-07-11}}</ref>

The rivalry between Kane and Rob Van Dam stopped, as Kane engaged in a feud against Shane McMahon. In a scenario on the August 25, 2003 episode of ''Raw'', Kane attempted to throw McMahon into a dumpster that was set on fire, but McMahon avoided it and threw Kane into the dumpster.<ref name="Raw8.25b"/> On September 8, 2003 during an episode of ''Raw'', Eric Bischoff scheduled a [[Last Man Standing match]] between Kane and McMahon for Unforgiven.<ref name="Raw9.8a"/> In that match, Kane defeated McMahon after McMahon was unable to respond to a ten count.<ref>{{cite web|url=http://pwtorch.com/artman2/publish/PPV_Reports_5/article_5379.shtml|title=9/21 WWE Unforgiven PPV results: Keller's "virtual time" results of live event|last=Keller|first=Wade|date=2003-09-21|publisher=Pro Wrestling Torch|accessdate=2008-07-11}}</ref><ref>{{cite web|url=http://www.wrestleview.com/news/1064199479.shtml|title=Full WWE Unforgiven (RAW) PPV Results – 9/21/03 – Hershey, Pennsylvania|last=Martin|first=Adam|date=2003-09-21|publisher=WrestleView|accessdate=2008-07-11}}</ref> After SummerSlam, Eddie Guerrero began a rivalry with [[John Cena]] over the WWE United States Championship. Guerrero retained the championship in two title defenses that took place on ''SmackDown!''.<ref name="SD9.4b"/><ref name="SD8.28a">{{cite web|url=http://pwtorch.com/artman2/publish/TV_Reports_9/article_5095.shtml|title=8/28 WWE Smackdown review: Radican's detailed report on show|last=Radican|first=Sean|publisher=PW Torch|accessdate=2008-07-11}}</ref> Guerrero then engaged in a feud with [[Big Show]]. At [[No Mercy (2003)|No Mercy]], Big Show defeated Guerrero via pinfall to win the WWE United States Championship.<ref>{{cite web|url=http://www.wrestleview.com/news/1066618308.shtml|title=Full WWE No Mercy (SmackDown) PPV Results – 10/19/03 – Baltimore, MD|last=Martin|first=Adam|date=2003-10-13|publisher=WrestleView|accessdate=2008-07-11}}</ref><ref>{{cite web|url=http://www.wwe.com/shows/nomercy/history/2003/results/ |title=WWE No Mercy Results |publisher=WWE |accessdate=2008-07-11 |deadurl=yes |archiveurl=https://web.archive.org/web/20081210125038/http://www.wwe.com/shows/nomercy/history/2003/results/ |archivedate=December 10, 2008 }}</ref>

===Reception===
The America West Arena has a maximum capacity of 19,000, but that was reduced for SummerSlam 2003. The event grossed over US$715,000 in ticket sales from an attendance of 16,113, the maximum allowed.<ref>{{cite web|url=http://www.usairwayscenter.com/public/2006-2007%20Suns%20Seating%20Season.pdf |format=PDF |title=U.S. Airways Center Seating Chart (Basketball) |publisher=[[U.S. Airways Center]] |accessdate=2008-07-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20070302134018/http://www.usairwayscenter.com/public/2006-2007%20Suns%20Seating%20Season.pdf |archivedate=March 2, 2007 }}</ref><ref>{{cite web|url=http://www.buyselltix.com/nba/stadium/sunsStadium.php |title=U.S. Airways Arena |publisher=BuySellTix |accessdate=2008-07-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20080515161211/http://www.buyselltix.com/nba/stadium/sunsStadium.php |archivedate=May 15, 2008 }}</ref> This was later confirmed by [[Linda McMahon]], WWE [[Chief executive officer|CEO]], in a press release on August&nbsp;26, 2003.<ref>{{cite web|url=http://www.accessmylibrary.com/coms2/summary_0286-858117_ITM|title=Q1 2004 World Wrestling Entertainment Inc. Earnings Conference Call – Final.|date=2003-08-26|quote=It is hot in Phoenix. For any of you who may not be located here I think for SummerSlam not only did we have a hot event in the arena, but it was 116 degrees outside. But we had a sold out event with very, very enthusiastic fans cheering throughout the early evening and night and they even stood outside in the parking lot in that heat for about 3 or 4 hours in the afternoon. So it's good to see that kind of enthusiasm for our product.|publisher=Access Library|accessdate=2008-07-21}}</ref> The event resulted in 415,000&nbsp;pay-per-view buys (a 0.88 pay-per-view buyrate). The promotion's pay-per-view revenue was $24.7&nbsp;million.<ref>{{cite web|url=http://corporate.wwe.com/news/2004/docs/2Q_2005_Press_Release_final.pdf |format=PDF |title=World Wrestling Entertainment, Inc. Reports Q2 Results |date=2004-11-23 |publisher=WWE |accessdate=2008-07-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20080516080849/http://corporate.wwe.com/news/2004/docs/2Q_2005_Press_Release_final.pdf |archivedate=May 16, 2008 }}</ref>

[[Canadian Online Explorer]]'s professional wrestling section rated the entire event a 7 out of 10 stars.<ref name="Slam" /> The rating was higher than the [[SummerSlam (2004)|SummerSlam event in 2004]], which was rated a 5 out of 10 stars.<ref>{{cite web|url=http://slam.canoe.ca/Slam/Wrestling/PPVReports/2004/08/16/585826.html|title=Orton-Benoit-Guerrero-Angle save SummerSlam|last=Clevett|first=Jason|publisher=WWE|accessdate=2008-07-20}}</ref> The Elimination Chamber main event match from the Raw brand was rated an 8.5 out of 10 stars, with an additional rating of 1 out of 10 stars for the process in which the match ended. The SmackDown! brand's main event, a standard match for the WWE Championship, was rated a 9 out of 10 stars, a better reception than the Raw brand's main event.<ref name="Slam"/> [[Wade Keller]] reviewed the event for the ''Pro Wrestling Torch''. He rated the Angle-Lesnar match 4-and-a-half out of 5 stars, declaring it an "excellent match".<ref name="Keller">{{cite web|url=http://www.pwtorch.com/artman2/publish/Torch_Flashbacks_19/article_43047.shtml|title=WWE SumerSlam Flashback Series – 2003 Report: Triple H wins Elimination Chamber over big names, Kurt Angle vs. Brock Lesnar|date=August 11, 2010|last=Keller|first=Wade|authorlink=Wade Keller|accessdate=September 8, 2013|publisher=Pro Wrestling Torch}}</ref> The Elimination Chamber match received a rating of 3 stars.<ref name="Keller"/> The event was released on [[DVD]] on September&nbsp;23, 2003<ref>{{cite web|url=http://www.fye.com/WWE--Summer-Slam-2003-WWE_stcVVproductId2428911VVcatId515097VVviewprod.htm |title=WWE SummerSlam DVD at F.Y.E |publisher=[[Trans World Entertainment#f.y.e.|For Your Entertainment]] |accessdate=2008-07-21 |deadurl=yes |archiveurl=https://web.archive.org/web/20080417000130/http://www.fye.com/WWE--Summer-Slam-2003-WWE_stcVVproductId2428911VVcatId515097VVviewprod.htm |archivedate=April 17, 2008 }}</ref> by [[Sony Music Entertainment]].

==Results==
{{Pro Wrestling results table
|results = <ref name="Slam" /><ref name="Almanac" />
|times = <ref name="Facts"/>
|heat1  = yes
|match1 = [[Rey Mysterio]] (c) defeated [[Shannon Moore]]
|stip1  = [[Professional wrestling match types#Variations of singles matches|Singles match]] for the [[WWE Cruiserweight Championship]]
|time1  = 02:03
|match2 = [[La Résistance (professional wrestling)|La Résistance]] ([[René Duprée]] and [[Sylvain Grenier]]) (c) (with [[Rob Conway]]) defeated [[The Dudley Boyz]] ([[Mark LoMonaco|Bubba Ray]] and [[Devon Hughes|D-Von]]) (with [[Spike Dudley]])
|stip2 = [[Tag team match]] for the [[World Tag Team Championship (WWE)|World Tag Team Championship]]
|time2 = 07:49
|match3 = [[The Undertaker]] defeated [[Matt Bloom|A-Train]] (with [[Sable (wrestler)|Sable]])
|stip3  = Singles match
|time3  = 09:19
|match4 = [[Shane McMahon]] defeated [[Eric Bischoff]]
|stip4  = [[Falls count anywhere match|Falls Count Anywhere match]]
|time4  = 10:36
|match5 = [[Eddie Guerrero]] (c) defeated [[Chris Benoit]], [[Rhino (wrestler)|Rhyno]], and [[Yoshihiro Tajiri|Tajiri]]
|stip5  = [[Triple Threat Match|Fatal 4-Way match]] for the [[WWE United States Championship]]
|time5  = 10:50
|match6 = [[Kurt Angle]] (c) defeated [[Brock Lesnar]] by submission
|stip6  = Singles match for the [[WWE Championship]]
|time6  = 20:48
|match7 = [[Kane (wrestler)|Kane]] defeated [[Rob Van Dam]]
|stip7  = [[Professional wrestling match types#Hardcore-based variations|No Holds Barred match]]
|time7  = 12:49
|match8 = [[Triple H]] (c) (with [[Ric Flair]]) defeated [[Bill Goldberg|Goldberg]], [[Chris Jericho]], [[Shawn Michaels]], [[Randy Orton]] and [[Kevin Nash]]
|stip8  = [[Elimination Chamber|Elimination Chamber match]] for the [[World Heavyweight Championship (WWE)|World Heavyweight Championship]]
|time8  = 19:12
}}

===Elimination Chamber entrances and eliminations===
{| class="wikitable"
|-
!'''Elimination number'''
!'''Wrestler'''
!'''Entered'''
!'''Eliminated by'''
!'''Method of elimination'''
!'''Time'''
|-
|1
|Kevin Nash
|4
|Chris Jericho
|[[Pin (professional wrestling)#Roll-up|Roll-up]]
|08:05
|-
|2
|Randy Orton
|3
|Goldberg
|[[Spear (wrestling)|Spear]]
|13:01
|-
|3
|Shawn Michaels
|2
|Goldberg
|[[Powerslam#Suplex powerslam|Jackhammer]]
|15:19
|-
|4
|Chris Jericho
|1
|Goldberg
|Jackhammer
|16:03
|-
|5
|Goldberg
|6
|Triple H
|[[Sledgehammer]] [[Professional wrestling attacks#Weapon shot|shot]] to the back of the head
|19:12
|-
|'''Winner'''
|Triple H
|5
|Winner
|Winner
|19:12
|}

==References==
{{reflist|30em}}

==External links==
{{Spoken Wikipedia-3|2008-08-26|SummerSlam2003 Intro 12 260808.ogg|SummerSlam2003 34 260808.ogg|SummerSlam2003 56 260808.ogg}}
* {{Official website|https://web.archive.org/web/20110605011917/http://www.wwe.com/shows/summerslam/history/2003/}}
* [http://www.insidearenas.com/western/USAirwaysCenter.htm Inside the U.S. Airways Arena (America West Arena)]

{{2003 WWE pay-per-view events}}
{{WWEPPV|SummerSlam}}

{{DEFAULTSORT:Summerslam 2003}}
[[Category:2003 in professional wrestling]]
[[Category:Sports in Phoenix, Arizona]]
[[Category:SummerSlam|2003]]
[[Category:2003 in Arizona]]