{{Infobox journal
| title = Aggression and Violent Behavior
| cover = 
| former_name = <!-- or |former_names= -->
| abbreviation = Aggress. Violent Behav.
| discipline = [[Criminology]]
| editor = Vincent van Hasselt
| publisher = [[Elsevier]]
| country = 
| history = 1996-present
| frequency = Bimonthly
| openaccess = 
| license = 
| impact = 1.912
| impact-year = 2015
| ISSN = 1359-1789
| eISSN =
| CODEN = AVBEFZ
| JSTOR = 
| LCCN = 96640730
| OCLC = 644028319
| website = http://www.journals.elsevier.com/aggression-and-violent-behavior
| link1 = http://www.sciencedirect.com/science/journal/13591789
| link1-name = Online archive
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
}}
'''''Aggression and Violent Behavior''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering the study of [[Violence|violent behavior]]. It was established in 1996 and is published by [[Elsevier]]. The [[editor-in-chief]] is Vincent van Hasselt ([[Nova Southeastern University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Current Contents]]/Social & Behavioral Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-19}}</ref>
* [[Social Sciences Citation Index]]<ref name=ISI/>
* [[Sociological Abstracts]]
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-19}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.912.<ref name=WoS>{{cite book |year=2016 |chapter=Aggression and Violent Behavior |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.journals.elsevier.com/aggression-and-violent-behavior}}

[[Category:Criminology journals]]
[[Category:Elsevier academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1996]]
[[Category:English-language journals]]
[[Category:Psychology journals]]
[[Category:Violence journals]]