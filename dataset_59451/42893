<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = F2F
 |image = File:F2F-1 2-F-6 NAN1-76.jpg
 |caption = An F2F-1 of fighter squadron [[VF-6|VF-2B]], aboard {{USS|Lexington|CV-2|2}}
}}{{Infobox Aircraft Type
 |type =[[naval aviation|Naval fighter]]
 |manufacturer =[[Grumman]]
 |designer = [[Leroy Grumman]]
 |first flight = 18 October 1933 <ref>Graff 2009, p. 18.</ref>
 |introduced =[[1935 in aviation|1935]]
 |retired =[[1940 in aviation|1940]]
 |primary user =[[United States Navy]]
 |produced = 1934-1935
 |number built = 55
 |unit cost = $12,000 (as of 1935)<ref name="greatac">Cacutt 1989, pp. 155–162.</ref>
 |developed from = [[Grumman FF]]
 |variants with their own articles = [[Grumman F3F]]
}}
|}

The '''Grumman F2F''' was a single-engine, [[biplane]] [[fighter aircraft]] with retractable [[Landing gear|undercarriage]], serving as the standard fighter for the [[United States Navy]] between 1936 and 1940. It was designed for both carrier- and land-based operations.

==Design and development==
Grumman's success with the two-seat [[Grumman FF|FF-1]], which was significantly faster than even the single-seat fighters of its time, resulted in a contract for the single-seat '''XF2F-1'''.<ref name=Jordan2>Jordan, Corey C.  [http://www.planesandpilotsofww2.webs.com/Grumman2.html "Grumman's Ascendency: Chapter Two."] ''Planes and Pilots Of World War Two,'' 2000. Retrieved: 22 July 2011.</ref> Armed with two .30 caliber (7.62&nbsp;mm) machine guns above the cowl, the new design also incorporated watertight compartments to reduce weight and improve survivability in the event of a water landing.<ref name="greatac"/> The prototype first flew on 18 October 1933, equipped with the experimental {{convert|625|hp|kW|0|abbr=on}} [[Pratt & Whitney Twin Wasp Junior|XR-1534-44 Twin Wasp Junior]] [[radial engine]], and reached a top speed of {{convert|229|mph|km/h|0|abbr=on}} at {{convert|8400|ft|m|abbr=on}} - {{convert|22|mph|km/h|abbr=on}} faster than the FF-1 at the same altitude.<ref name="greatac"/> Maneuverability also proved superior to the earlier two-seat aircraft.<ref name=Jordan2/>
[[File:F2F-1s VF-2 NAN11-80.jpg|thumb|Three F2F-1s in service with fighter squadron VF-2B]]

==Operational history==
The Navy ordered 54 '''F2F-1''' fighters on 17 May 1934, with the first aircraft delivered 19 January 1935. An additional aircraft was ordered to replace one which crashed on 16 March 1935, bringing the total to 55, with the final F2F-1 delivered on 2 August 1935. The F2F-1 had a relatively long service life for the time, serving in front-line squadrons from 1935 to late [[1939 in aviation|1939]], when squadrons began to receive the [[Grumman F3F|F3F-3]] as a replacement. By September 1940, the F2F had been completely replaced in fighter squadrons and was relegated to training and utility duties.<ref name="greatac"/>

==Variants==
;XF2F-1
:United States Navy designation for the Grumman G.6 prototype with a {{convert|625|hp|kW|0|abbr=on}} [[Pratt & Whitney Twin Wasp Junior|XR-1534-44 Twin Wasp Junior]] [[radial engine]], one built.
;F2F-1
:Production variant with a {{convert|700|hp|kW|0|abbr=on}} [[Pratt & Whitney Twin Wasp Junior|R-1535-72 Twin Wasp Junior]] [[radial engine]], 55 built.

==Operators==
;{{USA}} 
* [[United States Navy]]

==Specifications (Grumman F2F-1)==
[[File:Grumman F2F-1 NAN1-76 drawing.jpg|thumb|Three-view drawing of Grumman F2F-1]]
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=United States Navy Aircraft since 1911 <ref name="Swan Navy p201">Swanborough and Bowers 1976, p. 201.</ref>
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |ref=, |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specification]]. -->
|crew=One
|length main=21 ft 5 in
|length alt=6.53 m
|span main=28 ft 6 in
|span alt=8.69 m
|height main=9 ft 1 in
|height alt=2.77 m
|area main=230 ft²
|area alt=21.4 m²
|empty weight main=2,691 lb
|empty weight alt=1,221 kg
|loaded weight main=
|loaded weight alt=
|max takeoff weight main=3,847 lb
|max takeoff weight alt=1,745 kg
|more general=
|engine (prop)=[[Pratt & Whitney R-1535-72 Twin Wasp Junior]]
|type of prop=[[radial engine]]
|number of props=1
|power main=700 hp
|power alt=522 kW
|max speed main=231 mph
|max speed alt=201 knots, 372 km/h
|range main=985 mi
|range alt=857 [[nautical mile|nmi]], 1,585 km
|ceiling main=27,100 ft
|ceiling alt=8,260 m
|climb rate main=2,050 ft/min
|climb rate alt=10.4 m/s
|loading main= 
|loading alt=  
|power/mass main= 
|power/mass alt=  
|more performance=
|guns=2× .30-in machine guns
}}

==See also==
{{aircontent
|related=
* [[Grumman F3F]]
|similar aircraft=
|lists=
* [[List of fighter aircraft]]
* [[List of US Naval aircraft]]
|see also=
}}

==References==
===Citations===
{{Reflist}}

===Bibliography===
{{Refbegin}}
* Cacutt, Len, ed. “Grumman Single-Seat Biplane Fighters.” ''Great Aircraft of the World''. London: Marshall Cavendish, 1989. ISBN 1-85435-250-4.
* Graff, Cory. ''F6F Hellcat at War''. New York: Zenith Imprint, 2009. ISBN 978-0-76033-306-8.
* Swanborough, Gordon and Peter M. Bowers. ''United States Navy Aircraft since 1911''. London: Putnam, Second edition, 1976. ISBN 0-370-10054-9.
{{Refend}}

==External links==
{{commons category-inline|Grumman F2F}}

{{USN fighters}}
{{Grumman aircraft}}

[[Category:Grumman aircraft|F02F]]
[[Category:United States fighter aircraft 1930–1939]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Carrier-based aircraft]]