{{good article}}
{{Infobox rail accident
|image    = Silver Spring collision aerial.png
|image_upright = 1.35
|image_alt = Snow-covered railway line lined by trees. Two wrecked trains.
|caption  = Aerial photograph of the collision. The MARC commuter train is on the left, the ''Capitol Limited'' on the right.
|title    = 1996 Maryland train collision
|date     = February 16, 1996
|time     = 5:39 pm 
|location = [[Silver Spring, Maryland]]
|coordinates = {{Coord|39|0|1.7|N|77|2|30.75|W|type:landmark||display=inline}}
|country  = United States
|line     = 
|operator = [[MARC Train|MARC]]<br />[[Amtrak]]
|type     = Collision
|cause    = MARC crew failure to reduce speed at an approach signal
|trains   = 2
|pax      = 184
|deaths   = 11
|injuries = 26
}}
The '''1996 Maryland train collision''' occurred on February 16, 1996, when a [[MARC Train|MARC commuter train]] collided with [[Amtrak]]'s ''[[Capitol Limited (Amtrak train)|Capitol Limited]]'' [[passenger train]] in [[Silver Spring, Maryland]]. An investigation by the [[National Transportation Safety Board]] (NTSB) found that the crew of the MARC train had forgotten the indication of an [[approach signal]] which they had passed before a station stop, and as a consequence could not slow down in time after encountering a stop signal. The collision killed three crew and eight passengers on the MARC train; a further eleven passengers on the MARC train and fifteen passengers and crew on the ''Capitol Limited'' were injured. Total damage was estimated at $7.5 million. The crash led to the creation of comprehensive [[Federal government of the United States|federal]] rules for passenger car design, the first in the history of passenger service in the United States,<ref name="wp19990511" /> as well as changes to operating rules.

==Accident==
The railway line between [[Brunswick (MARC station)|Brunswick, Maryland]], and [[Union Station (Washington, D.C.)|Union Station]] in [[Washington, D.C.]], is owned entirely by [[CSX Transportation]] (save for approaches to Union Station) and is known as the [[Metropolitan Subdivision]]. The entire line is [[double-track]]ed. [[MARC Train|MARC]] operates commuter services, known as the [[Brunswick Line]], from Washington to Brunswick and points west. [[Amtrak]] operates the ''[[Capitol Limited (Amtrak train)|Capitol Limited]]'', a Washington–[[Chicago]] overnight train, over the route as well, though it makes fewer stops.<ref name="ett" /><ref name="ntsb" />{{rp|22}}

MARC #286 departed Brunswick at 4:30 PM [[Eastern Time Zone|Eastern time]] on February 16, 1996. #286 was a Brunswick–Washington, D.C., [[commuter rail|commuter train]] with a scheduled arrival at [[Union Station (Washington, D.C.)|Union Station]] of 5:30 PM. The NTSB described the conditions that day as a "blowing snowfall" with a {{convert|5|in|adj=on}} accumulation. At the time CSX provided the operating crew for MARC commuter trains under contract to the [[Maryland Transit Administration|Maryland Mass Transit Administration]]; aboard were an engineer, conductor, and assistant conductor.<ref name="ntsb" />{{rp|1}} The train consisted of an [[GP39H-2|EMD GP39H-2]] [[diesel locomotive]], two passenger coaches, and a [[Control car (rail)|control car]]. The train operated in [[Push–pull train|push-pull mode]], meaning that the locomotive was on the rear of the train and the locomotive engineer controlled operations from the control car in the front.<ref name="ntsb" />{{rp|6}} MARC #286 had twenty passengers on board.<ref name="ntsb" />{{rp|1}}

Amtrak #29, the ''Capitol Limited'', departed Union Station at 5:25 PM, bound for Chicago. That day the ''Capitol Limited'' consisted of two diesel locomotives, a [[GE P40DC]] and an [[EMD F40PHR]]; six material handling cars; a [[baggage car]]; a [[Superliner (railcar)|Superliner]] [[Superliner (railcar)#Transition Sleeper|transition sleeping car]]; two Superliner [[sleeping car]]s; a Superliner [[dining car]]; a Superliner Sightseer Lounge car; two Superliner coaches; and a [[Hi-Level]] [[Combine car|dormitory-coach]]. The ''Capitol Limited'' had four crew, fourteen service personnel, and 164 passengers on board.<ref name="ntsb" />{{rp|1–6}}

{{Stack|
{{multiple image
| align     = right
| direction = vertical
| header    = Collision damage
| width     = 200

| image1    = Silver Spring collision cab car.png
| alt1      = Railway car knocked off the tracks with its right side sheared off
| caption1  = MARC II cab control car #7752

| image2    = Silver Spring collision amtrak locomotive.png
| alt2      = Derailed diesel locomotive with significant damage in the front
| caption2  = Amtrak EMD F40PHR #255
}}
}}
The two tracks of the Metropolitan Subdivision are numbered 1 and 2. MARC #286 was on track 2, having made a [[flag stop]] at [[Kensington (MARC station)|Kensington]] to pick up two passengers. The ''Capitol Limited'' was also on track 2, having passed a stopped [[freight train]] on track 1. Both trains were approaching Georgetown Junction, where the ''Capitol Limited'' was to switch to track 1. The signal protecting Georgetown Junction indicated STOP on track 2, which would have the effect of stopping MARC #286 and permitting the ''Capitol Limited'' to change tracks. Before reaching Kensington MARC #286 passed an APPROACH signal. The purpose of that signal was to warn the train's crew that the next signal would be a stop signal and that maximum speed was restricted to {{convert|30|mph}}. For reasons unknown the crew of MARC #286 did not obey this restriction and after departing Kensington the train reached {{convert|66|mph}} before the crew applied the emergency brakes. The ''Capitol Limited'' had reached Georgetown Junction and begun crossing over to track 1. MARC #286 struck the Amtrak train at approximately {{convert|38|mph}} at 5:39 PM.<ref name="ntsb" />{{rp|1–6}}

All three crew members aboard the MARC train were killed along with eight passengers. 26 people were injured. The collision destroyed both MARC passenger cars and the control car as well one of the two Amtrak locomotives, EMD F40PHR #255. The [[Railroad switch|turnouts]] at Georgetown Junction were damaged and had to be replaced. The total damage was estimated at $7.5 million.<ref name="ntsb" />{{rp|7–8}}

==Investigation==
[[File:1996 Silver Spring, Maryland, train collision diagram.jpg|thumb||alt=Track diagram of the collision area|Track diagram of the collision area]]
It was clear from the outset that MARC #286 had disregarded the approach signal and consequently overrun the stop signal, making the crash inevitable.<ref name="wp19960217" /> The deaths of all three CSX crew members in the collision meant that the reason for the failure would remain unknown; in its report NTSB ascribed it to "the apparent failure of the engineer and the traincrew because of multiple distractions to operate MARC train 286..."<ref name="ntsb" />{{rp|vii}} The focus of the investigators and the public shifted to safety systems which could have prevented the crash and the design of the commuter rail cars themselves.

One of the crew members and seven of the eight MARC passengers who died were killed not by the collision itself but by a fire started when the exposed diesel fuel tanks on the Amtrak locomotive ruptured. Emergency responders were unable to open the doors on the lead passenger car. Passengers on other cars reported trying to break the window glass but being unable to do so.<ref name="tt19960220" /> The NTSB found the existing emergency egress standards for passenger cars were inadequate, and recommended multiple changes, including:<ref name="ntsb" />{{rp|74–75}}
* that passenger cars be required to have "quick release" mechanisms for exterior doors
* that passenger car windows be removable in the event of an emergency
* that emergency exits be marked with "luminescent or retroreflective material"
Prior to the NTSB issuing its report MARC had taken steps in that direction, including the installation of more emergency windows, the improvement of door release mechanisms, and improved emergency signage.<ref name="wp19960523" />

Much criticism focused on the role of [[human error]] in the collision; once the engineer had forgotten (or missed) the approach signal there was no automatic system to prevent the collision.<ref name="wp19960220" /> The NTSB noted that had a [[positive train control]] system been in place on the Metropolitan Subdivision the collision would have been less likely: the system would have detected MARC #286's unauthorized speed and stopped the train.<ref name="ntsb" />{{rp|73}} The NTSB also strongly criticized CSX and the [[Federal Transit Administration]] for the removal of a signal between Kensington and Georgetown Junction as part of capacity improvements on the Metropolitan Subdivision. The NTSB argued that having this signal after Kensington would have reduced the likelihood of the sort of human error which caused the crash, and in the NTSB's view CSX and the FTA did not properly assess the effects of removing the signal.<ref name="ntsb" />{{rp|53–63}}

==Aftermath==
[[File:Brunswick train crash memorial front.jpg|thumb|upright|alt=Gravestone showing the crew who died|Crew fatalities]]
{{Stack|[[File:Brunswick train crash memorial back.jpg|thumb|upright|alt=Gravestone showing the passengers who died|Passenger fatalities]]}}
Following the accident, the engineer and conductor of the ''Capitol Limited'' filed lawsuits against Amtrak, CSX and the state of Maryland for $103 million (1996 [[United States dollar|USD]]) alleging negligence, singling out the removal of the signal between Kensington and Georgetown Junction and the operator error by the MARC engineer. Both men claimed that the injuries they sustained in the crash "prevent[ed] them from returning to work."<ref name="tbs19970401" /> In 1999, responding to the crash, the [[Federal Railroad Administration]] issued comprehensive rules for passenger car design, "the first ...in the 169-year history of rail passenger service." The new rules required that new control cars and multiple units be built to higher crashworthiness standards.<ref name="wp19990511" />

Several memorials were erected to commemorate the dead. In Silver Spring, a plaque was placed on a bridge above the crash site. In Brunswick, there is a stone with the names and pictures of the three CSX crewmen engraved on it. The stone was paid for by private donations.<ref name="wp19960922" /> The eight passengers who died were all students at the [[Harpers Ferry, West Virginia|Harpers Ferry]] [[Job Corps]] Center. Students there erected a memorial flanked by [[Weeping tree|weeping]] [[cherry]] trees.<ref name="cg20060217" />

{{clear}}

==See also==
*[[1987 Maryland train collision]]
*[[List of rail accidents (1990–99)]]

==References==
{{Reflist|30em|refs=
<ref name="cg20060217">{{cite web|title=Harpers Ferry Jobs Corps remembers '96 train wreck|url=http://www.highbeam.com/doc/1P2-13915582.html|publisher=''[[The Charleston Gazette]]'' {{Subscription required|via=HighBeam}}|accessdate=13 July 2014|date=February 17, 2006}}</ref>
<ref name="ett">{{cite web | title=NORTHERN REGION: BALTIMORE DIVISION TIMETABLE NO. 4 | date=January 1, 2005 | author=CSX Transportation | url=http://www.multimodalways.org/docs/railroads/companies/CSX/CSX%20ETTs/CSX%20Baltimore%20Div%20ETT%20%234%201-1-2005.pdf}}</ref>
<ref name="ntsb">{{cite web|last=National Transportation Safety Board|title=Collision and Derailment  of Maryland Rail Commuter MARC Train 286 and National Railroad Passenger Corporation Amtrak Train 29 Near Silver Spring, Maryland on February 16, 1996|url=https://commons.wikimedia.org/wiki/File:RAR9702.pdf|work=Railroad Accident Report|publisher=NTSB}}</ref>
<ref name="tbs19970401">{{cite news | newspaper=[[The Baltimore Sun]] | url=http://articles.baltimoresun.com/1997-04-01/news/1997091024_1_amtrak-marc-train-train-signal | title=Engineer, conductor file suits for $103 million in train crash | first=Marina | last=Sarris| date=April 1, 1997}}</ref>
<ref name="tt19960220">{{cite news | newspaper=[[The Tech (newspaper)|The Tech]] | url=http://tech.mit.edu/V116/N5/traincrash.5w.html | title=Eight of 11 Train Crash Victims Died of Fire, Not Crash Injuries | date=February 20, 1996 | first1=David | last1=Montgomery | first2=Alice | last2=Reid}}</ref>
<ref name="wp19960217">{{cite web|title=Simple Switching Maneuver Somehow Went Badly Awry; Location of Crash Suggests MARC Train Ran Signal, but Question Is Why |url=http://www.washingtonpost.com/wp-srv/local/longterm/library/marcwreck/switch.htm |work=[[Washington Post]] |accessdate=13 July 2014 |date=February 17, 1996 |first=Don |last=Phillips |archiveurl=http://www.webcitation.org/6TnTEX89x?url=http%3A%2F%2Fwww.washingtonpost.com%2Fwp-srv%2Flocal%2Flongterm%2Flibrary%2Fmarcwreck%2Fswitch.htm |archivedate=November 3, 2014 |deadurl=no |df= }}</ref>
<ref name="wp19960220">{{cite web|title=Crash Sharpens Focus on Train Control, Safety|url=http://www.highbeam.com/doc/1P2-763042.html|publisher=''[[Washington Post]]'' {{Subscription required|via=HighBeam}}|accessdate=13 July 2014|date=February 20, 1996|first=Don|last=Phillips}}</ref>
<ref name="wp19960523">{{cite web|title=MARC Agency Promises Improved Safety by July|url=http://www.highbeam.com/doc/1P2-776049.html|publisher=''[[Washington Post]]'' {{Subscription required|via=HighBeam}}|accessdate=13 July 2014|date=May 23, 1996}}</ref>
<ref name="wp19960922">{{cite web|title=A Railroad Town Remembers and Mourns; Memorial to Victims of MARC Crash Dedicated in Brunswick, Md|url=http://www.highbeam.com/doc/1P2-793992.html|publisher=''[[Washington Post]]'' {{Subscription required|via=HighBeam}}|accessdate=13 July 2014|date=September 22, 1996|first=Justin|last=Gillis}}</ref>
<ref name="wp19990511">{{cite web|title=New Rules Set for Passenger Trains; U.S. Agency Aims to Reduce Damage in Accidents Like the Fiery 1996 MARC Crash|url=http://www.highbeam.com/doc/1P2-588947.html|publisher=''[[Washington Post]]'' {{Subscription required|via=HighBeam}}|accessdate=13 July 2014|date=May 11, 1999|first1=Don|last1=Phillips|first2=Alan|last2=Sipress}}</ref>
}}

==External links==
* {{commonscat-inline|1996 Silver Spring, Maryland, train collision}}

{{1996 railway accidents}}

[[Category:1996 disasters in the United States]]
[[Category:1996 in Maryland]]
[[Category:Accidents and incidents involving Amtrak]]
[[Category:Accidents and incidents involving MARC Train]]
[[Category:Railway accidents in 1996]]
[[Category:Railway accidents in Maryland]]
[[Category:Railway accidents involving a disregarded signal]]
[[Category:Silver Spring, Maryland]]
[[Category:Train collisions in the United States]]