{{Infobox
 |name        = Center for Subjectivity Research (CFS)
 | abovestyle = background:#ddd;
 | above = Center for Subjectivity Research
 |  image      = [[Image:Nyekua.jpg|thumb]]
 |caption      = 
 |labelstyle  =
 |datastyle   = 
 | label1 = Category
 |  data1 = Research Center
 | label2 = Location
 |  data2 = [[Copenhagen University]], Faculty of Humanities
 | label3 = Foundation
 | data3 = 2002
 | label4 = Director
 |  data4 = Dan Zahavi
 | label5 = Web-site
 |  data5 = http://cfs.ku.dk }}

The '''Center for Subjectivity Research''' (CFS) is an interdisciplinary research center at the [[University of Copenhagen]], directed by [[Dan Zahavi]]. They work on a number of different topics:[[subjectivity]], [[intentionality]], [[empathy]], [[action (philosophy)|action]], [[perception]], [[embodied cognition|embodiment]], [[naturalism (philosophy)|naturalism]], [[self-consciousness]], [[self-disorder]]s, [[schizophrenia]], [[autism]], [[cerebral palsy]], [[norm (philosophy)|norm]]ativity, [[anxiety]], and [[trust (social sciences)|trust]], and do scholarly work on classical thinkers such as [[Kant]], [[Hegel]], [[Kierkegaard]], [[Franz Brentano|Brentano]], [[Husserl]], [[Heidegger]], [[Wittgenstein]], [[Merleau-Ponty]], [[Levinas]], and [[Ricoeur]]. They put a variety of philosophical and empirical perspectives on subjectivity into play to obtain mutual enlightenment, and methodological and conceptual pluralism. Hence, they have had collaborations within different disciplines such as [[phenomenology (psychology)|phenomenology]], [[analytic philosophy]], [[hermeneutics]], [[psychiatry]], [[neuroscience]], [[philosophy of religion]], [[Asian philosophy]], [[developmental psychology]], [[clinical psychology]], and [[cognitive science]].

== Introduction ==

=== History ===

CFS was established in 2002, by [[Dan Zahavi]] (Professor of Philosophy), [[:DK:Josef Parnas|Josef Parnas]] (Professor of Psychiatry) and Arne Grøn (Professor of Ethics and Philosophy of Religion) to allow a “systematic exploration of subjectivity” through “an ‘interphilosophical’ and ‘interdisciplinary’ approach”.<ref name="2002-2012">[http://www.e-pages.dk/ku/657/ Center for Subjectivity Research 2002-2012]</ref> In the period 2002-2012, it was funded as a [http://dg.dk/centers-of-excellence/ "Center of Excellence"] by the Danish National Research Foundation.<ref name="2002-2012" /> Since 2012 CFS has been hosting a number of externally funded research projects, and is currently part of the Department of Media, Cognition, and Communication, at the University of Copenhagen. CFS has received funding from, e.g., the [[European Science Foundation]], The Danish Independent Research Council, The European Commission (6th and 7th Framework Program), the [[Carlsberg Foundation]], the [http://veluxfoundation.dk/C12576AB00426565/0/4C05C456014EDFD5C1256E9F00371B87?OpenDocument VELUX Foundation], and the University of Copenhagen Excellence Program for Interdisciplinary Research.<ref>[http://cfs.ku.dk/about/annual_reports/ CFS Annual Reports]</ref>

=== Theoretical framework: Subjectivity ===
The notion of Subjectivity (rather than for instance [[consciousness]]) was chosen as essential to the center, because it has a long and complex history in Western thought, thus providing a strong connection to tradition.<ref name="2002-2012" /> Since [[Descartes]], and in particularly since Kant, subjectivity has been of ongoing concern to many philosophers working within the German and French traditions ([[continental philosophy]]). In the period from Kant to Hegel, occasionally labeled as the reign of the philosophy of subjectivity, subjectivity was considered to constitute at least one of the most important themes and principles of philosophy.<ref>''The Oxford Companion to Consciousness'', "Subjectivity" section, ed. Tim Bayne, Axel Cleeremans, Patrick Wilken. Oxford University Press 2009</ref> In [[20th century philosophy]], this theoretical orientation probably found its most significant continuation in phenomenology.

=== Honor and awards ===
In 2006 Dan Zahavi was awarded the Elite Research Prize of the Danish Ministry of Science, Technology and Innovation. The prize is given to an outstanding Danish researcher for an excellent contribution to Danish science. 
In 2009 Claudia Welz, then Post.doc at CFS, received the Templeton Award for Theological Promise. 
In 2010 Andrea Raballo, then Early Stage Researcher at CFS, won the [http://www.epa-congress.org European Psychiatric Association's] Research Prize 2010 in the category "Clinical Psychopathology and refinement of psychiatric diagnostic categories." 
In 2011, Adrian Alsmith (currently postdoc at CFS), first won the [http://www.barbara-wengeler-stiftung.de/preis.html Barbara Wengeler Prize] for his PhD-dissertation and then the [http://ufm.dk/en/research-and-innovation/funding-programmes-for-research-and-innovation/find-danish-funding-programmes/dff-sapere-aude Sapere Aude] - DFF Young Elite Researcher award. 
In 2011, Dan Zahavi received the Carlsberg Foundation's Research Prize from the Royal Danish Academy of Sciences and Letters.
In 2012, Josef Parnas received the Kurt Schneider Scientific Award.<ref name="2002-2012" />

== Projects and collaboration ==

=== Advisory board ===
CFS has an advisory board composed of the following scholars: [[Ingolf U. Dalferth]] (Professor of Systematic Theology and Philosophy of Religion, Universität Zürich, Switzerland), [[:DE:Günter Figal|Günter Figal]] (Professor of Philosophy, Freiburg Universität, Germany), [[Shaun Gallagher]] (Professor, Department of Philosophy, University of Memphis, USA), [[:DK:Axel Honneth|Axel Honneth]] (Professor of Philosophy, University of Frankfurt, Germany), [[Alva Noë]] (Professor of Philosophy, University of California, Berkeley, USA), [[Philippe Rochat]] (Professor of Psychology, Emory University, USA), Yves Rossetti (Professor of Neuropsychology, Lyon Medical School, France), [[Louis Sass]] (Professor of Clinical Psychology, Rutgers University, USA), [[Galen Strawson]] (Professor of Philosophy, University of Reading, UK), [[Evan Thompson]] (Professor of Philosophy, University of British Columbia, Canada).

=== Academic activities ===
Dan Zahavi, director of CFS is co-editor in chief of [http://www.springer.com/philosophy/philosophical+traditions/journal/11097I Phenomenology and the Cognitive Sciences], an international journal at the intersections between phenomenology, empirical science, and analytic philosophy of mind.<ref>[http://link.springer.com/journal/11097 Springer site: ''Phenomenology and the Cognitive Sciences'']</ref>

Highlights among different recent research projects<ref>[http://cfs.ku.dk/research-activities/ CFS Research Activities]</ref>
* '''Disorders and Coherence of the Embodied Self (DISCOS) (2007-2010)'''
* '''Agency, Self and Other: An interdisciplinary investigation (2007-2010)'''
* '''Normativity, Self and Sociality (2011-2014)'''
* '''Towards an Embodied Science of InterSubjectivity (TESIS) (2011 - 2015)'''. A collaborative project between researchers from Germany, Spain, Denmark, UK, and Italy that has EU funding for a Marie Curie Initial Training Network (ITN) entitled [https://tesisnetwork.wordpress.com TESIS: "Towards an Embodied Science of InterSubjectivity"]. The network coordinator is [[:de:Thomas Fuchs (Psychiater)|Thomas Fuchs]] from Department of Psychiatry, [[Heidelberg University|University of Heidelberg]]. The associated partners include [[Lego]].
* '''Empathy and Interpersonal Understanding (2011-2016)'''. A project to describe what empathy is and what role it plays in interpersonal understanding, and also to what extent interpersonal understanding presupposes a common social and cultural background. The project is funded by VELUX Foundation.
* '''The disrupted "we": Shared intentionality and its psychopathological distortions (2013-2016)'''. A project that investigates shared intentionality and the nature of we-perspective, funded under the University of Copenhagen's Excellence Program for Interdisciplinary Research.
* '''Self-understanding and self-alienation: Existential hermeneutics and psychopathology (2014-2016)'''. The project explores what experiences of self-alienation tell us about human self-understanding, and conversely how a more nuanced approach to questions of self-understand might help reach a better understanding of experiences of self-alienation. The project is supported by a grant from the VELUX Foundation.

=== Practical application ===
CFS also works in projects extending outside of academic boundaries, e.g., projects with clinical application or art projects.

Examples of projects with clinical application:
* Through Professor Josef Parnas CFS has a collaboration with the psychiatric ward at [[Hvidovre Hospital]] on issues related to research on schizophrenia. Together with other researchers, Parnas and Zahavi developed [http://www.easenet.dk EASE]: Examination of Anomalous Self-Experience, a semi-structured qualitative and semi-quantitative psychometric instrument that targets anomalies of subjective self-experience, which are characteristic of the schizophrenia spectrum disorders, especially in their early phases.
* Collaboration with the [http://elsasscenter.dk Helene Elsass Center], a research and development center for rehabilitation of people with cerebral palsy, CFS worked with applying ideas from phenomenology and neuroscience in [[physiotherapy]], [[occupational therapy]], and psychology, to develop new rehabilitation strategies and technologies.<ref>Frank, Lone & Martiny, Kristian M. "Man må ha' det i benene", [[Weekendavisen]], 15 June 2012</ref>

Examples of collaborations with artists and on art projects:
* Organizing a network for Danish researchers and film directors, to developing a closer relation between research and film,CFS worked with the [[Danish Film Institute]], Creative Media Desk Denmark, and [[Signe Byrge Sørensen]] from [[Final Cut for Real]]. Film directors include [[:DK:Phie Ambo|Phie Ambo]], [[Joshua Oppenheimer]], and [[Janus Metz]].<ref>[http://videnskab.dk/kultur-samfund/kan-verden-vejes-visuelt Martiny et al. ”Kan Verden ’vejes’ Visuelt”, ''Videnskab.dk'', 19 June 2014]</ref>
* As a scientific advisor Kristian Moltke Martiny, then PhD fellow at CFS, was part of the team behind the documentary film [http://www.dfi.dk/faktaomfilm/film/en/92783.aspx?id=92783 ''Natural Disorder''] (premiered fall 2015), about living with cerebral palsy, directed by Danish documentarist [[:DK:Christian Sønderby Jepsen|Christian Sønderby Jepsen]] and with Jacob Nossell (known from [[The Red Chapel]]) as the lead character. Martiny also worked as scientific advisor on an experimental theater play called "Human Afvikling", which premiered on September 21, 2014 at the [[Royal Danish Theater]]. It was directed by [[:DK:Thomas Corneliussen|Thomas Corneliussen]] and also had Jacob Nossell as lead character. The play depicts the social experiences of living with bodily disabilities.<ref>[http://videnskab.dk/kultur-samfund/ny-teater-forestilling-er-et-videnskabeligt-eksperiment Ebdrup, Niels. ”Ny teater forestilling er et videnskabeligt eksperiment”, ''Videnskab.dk'', 19 September 2014]</ref>
* In his PhD project on "expert musicianship" (2012-2015), then PhD student at CFS Simon Høffding, had a long standing collaboration with the [[Danish Quartet|Danish string quartet]].<ref>Radio Program: "Nørderne Kommer – Om musikken og Sindet”, ''Radio DR P2'', 16 December 2013</ref>
* Artists such as [[Olafur Eliasson]] and [[Siri Hustvedt]] have engaged in conversation and collaboration with Dan Zahavi.

== Education and student relation ==

=== Master in Phenomenology and Philosophy of Mind ===
Through the Department of Media, Cognition and Communication CFS is involved in and offers courses on a 2-year master specialization in Phenomenology and Philosophy of Mind.

=== Summer school ===
Since 2010 during August it has hosted the Copenhagen Summer School in Phenomenology and Philosophy of Mind.<ref name="2002-2012" /> The summer school which is partially funded by the PhD School at the Faculty of Humanities, University of Copenhagen, typically attracts around 80-100 PhD students from all over the world, but is also open to advanced MA students.

CFS engages in research training for PhD-students, full-degrees, and visiting students.

== Selected bibliography ==
* Gallagher, S., Zahavi, D.: The Phenomenological Mind. 2nd Edition. London: Routledge, 2012. (288 pp.)
* Grøn, A.: The Concept of Anxiety in Søren Kierkegaard. Translated by Jeanette B.L. Knox. Macon, Georgia: Mercer University Press, 2008 (166 pp.).
* Kendler, K. & Parnas, J. (eds.): Philosophical Issues in Psychiatry: Explanation, Phenomenology, and Nosology. Baltimore: The Johns Hopkins University Press, 2008. (424 p.)
* Overgaard, S.: Husserl and Heidegger on Being in the World. Phaenomenologica 173. Kluwer Academic Publishers, Dordrecht, 2004. (225 pp.)
* Overgaard, S.: Wittgenstein and Other Minds: Rethinking Subjectivity and Intersubjectivity with Wittgenstein, Levinas, and Husserl. New York and London: Routledge, 2007. (xiii+201 pp.)
* Overgaard, S., Gilbert, P. and Burwood, S.: An Introduction to Metaphilosophy. Cambridge: Cambridge University Press, 2013 (vii + 240 pp.).
* Parnas, J., Møller, P., Kircher, T., Thalbitzer, J., Jansson, L., Handest, P., Zahavi, D.: "EASE: Examination of Anomalous Self-Experience". Psychopathology 38, 2005, 236-258.
* Siderits, M., Thompson, E., Zahavi, D. (eds.): Self, No Self? Perspectives from Analytical, Phenomenological, & Indian Traditions.  Oxford: Oxford University Press, 2011. (337 p.)
* Welz, C.: Love’s Transcendence and the Problem of Theodicy. Tübingen: Mohr Siebeck, 2008. (437 pp.)
* Zahavi, D.: Husserl‘s Phenomenology. Stanford University Press, Stanford, 2003. (x + 178 pp).
* Zahavi, D.: Subjectivity and Selfhood: Investigating the First-Person Perspective. The MIT Press, Cambridge, MA., 2005. (viii + 265 pp.)
* Zahavi, D. (ed.): The Oxford Handbook of Contemporary Phenomenology. Oxford, Oxford University Press, 2012. (640 p.).
* Zahavi, D.: Self and Other: Exploring Subjectivity, Empathy, and Shame. Oxford University Press 2014 (296 pages)

== References ==
{{reflist}}

== External links ==
* {{Official website|http://cfs.ku.dk}}

[[Category:Education in Denmark]]