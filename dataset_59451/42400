<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name=Boeing EC-135
  |image=File:Boeing EC-135E 60-0374 USAF-Museum.jpg
  |caption=EC-135E nicknamed "Bird of Prey" at the [[National Museum of the United States Air Force]]
}}{{Infobox Aircraft Type
  |type=Tracking and [[Telemetry]] Platform
  |manufacturer=[[Boeing]]
  |first flight=
  |introduced=1968
  |retired=2000
  |status=
  |primary user=[[United States Air Force]]
  |more users = <!--up to three more. please separate with <br/>.-->
  |produced =
  |number built =
  |unit cost =
  |developed from = [[C-135 Stratolifter]]
  |variants with their own articles = 
}}
|}

The '''Boeing EC-135''' was a [[command and control]] version of the [[Boeing C-135 Stratolifter]]. Modified for the [[Operation Looking Glass]] mission, during the [[Cold War]], EC-135 were airborne 24 hours a day to serve as flying command platforms for the military in the event of nuclear war. The EC-135N variant served as a tracking aircraft for the [[Apollo program]].

==Missions==
===Advanced Range Instrumentation Aircraft===<!-- This section is linked from [[Boeing 707]] -->

The '''Advanced Range Instrumentation Aircraft''' are '''EC-135B'''s, modified [[C-135 Stratolifter|C-135]]B [[cargo aircraft]] and [[Boeing 707#Military|EC-18B]] (former [[American Airlines]] [[Boeing 707|707-320]]) passenger aircraft that provided tracking and telemetry information to support the US space program in the late 1960s and early 1970s.

During the early 1960s, [[NASA]] and the [[US Department of Defense|Department of Defense]] (DoD) needed a very mobile tracking and telemetry platform to support the [[Apollo space program]] and other unmanned space flight operations. In a joint project, NASA and the DoD contracted with the [[McDonnell Douglas]] and the [[Bendix Corporation|Bendix]] Corporations to modify eight [[Boeing]] C-135 Stratolifter cargo aircraft into '''EC-135N''' '''Apollo / Range Instrumentation Aircraft''' (A/RIA). Equipped with a steerable seven-foot [[Parabolic antenna|antenna dish]] in its distinctive "Droop Snoot" or "Snoopy Nose", the '''EC-135N''' A/RIA became operational in January 1968, and was often known as the "[[Jimmy Durante]]" of the Air Force. The [[45th Space Wing|Air Force Eastern Test Range]] (AFETR) at [[Patrick AFB]], Florida, maintained and operated the A/RIA until the end of the Apollo program in 1972, when the USAF renamed it the Advanced Range Instrumentation Aircraft (ARIA).

Since Patrick AFB was located, literally, on the Atlantic Ocean, salt water and salt air-induced corrosion issues and associated aircraft maintenance challenges were problematic for the ARIA while based there.  Transferred to the [[4950th Test Wing]] at [[Wright-Patterson AFB]], Ohio, in December 1975 as part of an overall consolidation of large test and evaluation aircraft, the ARIA fleet underwent numerous conversions, including a re-engining that changed the EC-135N to the '''EC-135E'''. In 1994, the ARIA fleet relocated again to [[Edwards AFB]], California, as part of the [[412th Test Wing]]. However, taskings for the ARIA dwindled because of high costs and improved satellite technology, and the USAF transferred the aircraft to other programs such as [[E-8 J-STARS]].

Over its thirty-two year career, the ARIA supported the United States space program, gathered telemetry, verified international treaties, and supported [[cruise missile]], [[ballistic missile]] defense tests, and the [[Space Shuttle]].<ref>''This section uses public domain text from the National Museum of the United States Air Force. http://www.wpafb.af.mil/museum/outdoor/od30.htm''{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
</ref>

===Looking Glass===
[[File:Boeing EC-135C (717-166), USA - Air Force AN1155085.jpg|thumb|EC-135C Looking Glass]]
Officially known as "[[Operation Looking Glass]]", at least 11 '''EC-135C''' command post aircraft were provided to the Commander in Chief, [[Strategic Air Command]] (CINCSAC), and were based at various locations throughout the United States and worldwide. Operations began in 1961 with the [[34th Air Refueling Squadron]] at [[Offutt Air Force Base]], [[Nebraska]], initially using EC-135As until the dedicated EC-135Cs entered service in 1963. Other Offutt-based units included the [[38th Strategic Reconnaissance Squadron]] (1966–1970), the [[2d Airborne Command and Control Squadron]] (1970–1994), and the [[7th Airborne Command and Control Squadron]] (1994–1998).<ref>http://www.sac-acca.org/documents/PACCS%20History.html</ref> Other units operating the Looking Glass mission included the following:

*[[913th Air Refueling Squadron]] at [[Barksdale Air Force Base]], [[Louisiana]] (1963–1970)
*3d Airborne Command & Control Squadron at [[Grissom Air Force Base]], [[Indiana]] (1970–1974) 
*4th Airborne Command & Control Squadron at [[Ellsworth Air Force Base]], [[South Dakota]] (1970–1991)
*[[99th Air Refueling Squadron]], [[Westover Air Reserve Base|Westover Air Force Base]], [[Massachusetts]] (1963–1970)

Other EC-135 aircraft (including EC-135A, G, and L models) supporting the Looking Glass missions (communications relay and Minuteman airborne missile launch centers) were flown by the [[906th Air Refueling Squadron]] at [[Minot Air Force Base]], [[North Dakota]] (1963–1970){{ref|1|1}}, the [[70th Air Refueling Squadron]] at Grissom AFB (1975–1993), and the [[301st Air Refueling Squadron]] at [[Rickenbacker Air National Guard Base|Lockbourne Air Force Base]], [[Ohio]] (1963–1970).  All aircraft have been retired or repurposed.

The [[United States|U.S.]] nuclear strategy depends on its ability to command, control, and communicate with its [[nuclear weapon|nuclear forces]] under all conditions. An essential element of that ability is Looking Glass; its crew and staff ensure there is always an [[aircraft]] ready to direct bombers and missiles from the air should ground-based command centers be destroyed or rendered inoperable. Looking Glass is intended to guarantee that U.S. strategic forces will act only in the manner dictated by the [[President of the United States|President]]. It took the nickname "Looking Glass" because the mission mirrored ground-based command, control, and communications. Besides being the program name, "Looking Glass" is the official name for the "C" model aircraft of the EC-135. It has a crew of at least 15, including at least one or more [[general officer]]s.

The [[Strategic Air Command]] (SAC) began the Looking Glass mission on February 3, 1961 and  Looking Glass aircraft were continuously airborne 24 hours a day for over 29 years, accumulating more than 281,000 accident-free flying hours.  On July 24, 1990, "The Glass" ceased continuous airborne alert, but remained on ground or airborne alert 24 hours a day. The EC-135A flew the Command Post mission until EC-135C were delivered
starting in 1963.  The aircraft were delivered to Offutt AFB and as well as one aircraft to each of the Stateside Numbered Air Force Headquarters - Second Air Force at Barksdale AFB, LA; Eighth Air Force at Westover AFB, MA; and Fifteenth Air Force at March AFB CA. EC-135s flew all the missions except one, on March 4, 1980, when an [[Boeing E-4|E-4B]] was tested on an operational mission, flying a double sortie as the replacement aircraft could not launch due to weather. About a week after the flight, Washington deleted the funds for additional E-4 aircraft.

On June 1, 1992, [[Strategic Air Command|SAC]] was inactivated and replaced by the [[United States Strategic Command]], which now controls the Looking Glass.<ref>{{Cite web|url=https://www.stratcom.mil/factsheets/1/Airborne_Command_Post/|title=E-6B Airborne Command Post (ABNCP)|last=|first=|date=|website=|publisher=|access-date=}}</ref><ref>[http://www.fas.org/nuke/guide/usa/c3i/ec-135.htm FAS WMD page]</ref>
On October 1, 1998, the [[United States Navy|Navy's]] [[E-6 Mercury]] [[TACAMO]] replaced the [[United States Air Force|USAF's]] EC-135C in the Looking Glass mission. One former Looking Glass aircraft remains in service as a [[Boeing WC-135 Constant Phoenix|WC-135C Constant Phoenix]].<ref>http://www.joebaugher.com/usaf_serials/1962.html</ref>

</center>
;Notes
* <small>{{note|1|1}}Ellsworth AFB maintained additional EC-135 aircraft on Satellite Alert at Minot AFB to monitor the North Dakota missile silos.</small>

===Silk Purse===
{{further information|Operation Silk Purse}}
Operation Silk Purse program provided four '''EC-135H''' command post aircraft to the Commander, [[U.S. European Command]] (USEUCOM), which were based at [[RAF Mildenhall]] in the United Kingdom.  Flown by the 10th Airborne Command and Control Squadron 1970-87.  Onboard secure/non-secure communications and avionics equipment was maintained by the 513th Avionics Maintenance Squadron.  Aircraft S/Ns 61-0282, 285, 286 and 291.

===Scope Light===

Operation Scope Light provided five '''EC-135J/P''' command post aircraft to the Commander in Chief, [[U.S. Atlantic Command]] (CINCLANT), which were based at [[Langley AFB]], VA.  Operated by the 6th Airborne Command and Control Squadron 1972-92.

===Blue Eagle===
{{Primary sources|date=November 2016|section}}
{{Story|date=November 2016|section}}
Operation Blue Eagle provided five '''EC-135J/P''' command post aircraft to the Commander in Chief, U.S. Pacific Command (USCINCPAC), which were based at [[Hickam AFB]], HI.  Operated by the 9th Airborne Command and Control Squadron 1969-92. Communications, secure/unsecure voice and teletype, handled by the 1957th Communications Group, Hickam AFB, HI (1969-1992)

"Upkeep" was the call sign for the EC135 flying in southeast Asia during 1969 to 1971, based out of Hickam AFB Hawaii.  It was under the direction of PACAF of which 5th AF in Fuchu AS, Tokyo 
Japan handled their voice communications both unsecure and secure.
<1956 Comm Gp USAF 1969 to 1971>

Blue Eagle was formed in 1965 and started 24/7 operation in October 1965 and continued until disbanded in 1992.

===Nightwatch===

Operation Nightwatch provided three '''EC-135J''' command post aircraft to the President of the United States which were based at [[Andrews AFB]], MD. All three aircraft were transferred to other ABNCP missions.

Nightwatch was initiated in the mid-1960s utilizing the three '''EC-135J''' aircraft, modified from KC-135Bs, as command post aircraft. The three Nightwatch aircraft were ready to fly the President and the [[National Command Authority]] (NCA) out of Washington in the event of a nuclear attack. The '''[[Boeing E-4|E-4]]''' aircraft (a modified Boeing 747-200) came on line with the Nightwatch program in 1974 replacing the EC-135s on this mission.<ref name="heilig5">[http://www.clubhyper.com/reference/c135jh_5.htm C-135 Variants - Part 5 by Jennings Heilig]</ref>

===USCENTCOM Support===

The [[310th Airlift Squadron]], part of the [[6th Air Mobility Wing]] at [[MacDill AFB]], Florida, operated two '''NKC-135'''s that were reconfigured as  '''EC-135Y''' aircraft from 1989 to 2003 as executive transport and command & control platforms to support the Commander, [[United States Central Command]].  These aircraft have since been replaced with three [[C-37A]] [[Gulfstream V]] aircraft.

==Variant summary==
*EC-135A - KC-135A modified for airborne national command post role
*EC-135B - C-135B modified with large nose for ARIA mission
*EC-135C - purpose-built C-135 variant for airborne command post role, "Looking Glass"
*EC-135E - re-engined EC-135N
*EC-135G - KC-135A modified for airborne national command post role
*EC-135H - KC-135A modified for airborne national command post role, "Silk Purse"
*EC-135J - KC-135B modified for airborne national command post role, "Nightwatch"
*EC-135K - KC-135A modified for deployment control duties
*EC-135L - KC-135A modified for radio relay and amplitude modulation dropout capability "Cover All"
*EC-135N - ARIA aircraft with the so-called "droop snoot" radome housing a large parabolic telemetry gathering antenna.
*EC-135J/P - KC-135A modified for airborne command post role, "Blue Eagle" and "Scope Light"
*EC-135Y - NKC-135 reconfigured as C3 aircraft for Commander-in-Chief, United States Central Command

==Accidents==

*On 13 June 1971, USAF EC-135N, (AF Serial Number ''61-0331''), of 4950th Test Wing, Space and Missile Systems Organization (SAMSO), [[Wright-Patterson AFB]], OH disappeared while en route from [[Pago Pago]], American Samoa to [[Hickam AFB]], HI after monitoring a French atmospheric test conducted on the previous day.  The aircraft disappeared about 70 miles south of Hawaii near [[Palmyra Island]].  Twelve military personnel and twelve civilians died.  Cause of the mishap is unknown.  Only small bits of wreckage were found.<ref>http://aviation-safety.net/database/record.php?id=19710613-1</ref>
*On 14 September 1977, USAF EC-135K, (AF Serial Number ''62-3536)'', crashed on takeoff from [[Kirtland Air Force Base]], NM for a Higher-Headquarters Directed (HHD) mission.  After a long crew duty period, the crew started its takeoff roll at a few minutes prior to midnight.  The aircraft impacted the ground 8&nbsp;km (5 miles) east of the departure base because it lacked sufficient power to either climb above or turn to avoid rapidly rising terrain in that area.  All 20 occupants of this [[Tactical Air Command]] (TAC) operated aircraft were killed in the crash and subsequent fire at about 8,500 feet up the Manzano Mountain Range east of [[Albuquerque]], NM.<ref>http://aviation-safety.net/database/record.php?id=19770914-0</ref>
*On 2 January 1980, USAF EC-135P, (AF Serial Number ''58-0007''), was destroyed on the ground at [[Langley AFB]], VA when an electrical short occurred in the water injection tank heater wiring on the J-57-P/F-59W equipped aircraft.  There were no injuries as the [[Tactical Air Command]] (TAC) aircraft was unoccupied at the time of the mishap.<ref>http://www.aviation-safety.net/database/record.php?id=19800102-0</ref>
*On 6 May 1981, USAF EC-135N, (AF Serial Number ''61-0328''), crashed during a scheduled Advanced Range Instrumented Aircraft (ARIA) navigator and Primary Mission Electronic Equipment (PMEE) training mission from [[Wright-Patterson Air Force Base]], OH. For an unexplained reason, the aircraft pitch trim was moved to the full nose-down position, which exceeded the ability of the autopilot to control, and the aircraft pitched over abruptly.  The abrupt pitch over caused the generators to trip off line and the loss of AC electrical power prevented the pitch trim from being operated normally.  The aircraft became uncontrollable and exploded at about 1,500&nbsp;ft MSL.  The crash occurred near Walkersville, MD at 10:50L.  All seventeen crew members and four passengers on board the aircraft were killed.<ref>http://aviation-safety.net/database/record.php?id=19810506-0</ref>
*On 29 May 1992, USAF EC-135J, (AF Serial Number ''62-3584''), landed long at [[Pope AFB]], NC and overshot the runway.  The undercarriage collapsed and the fuselage broke in two.  Although none of the 14 occupants were seriously injured, the aircraft was written off as damaged beyond repair and the remains were removed to [[Davis-Monthan AFB]], AZ for disposal.<ref>http://www.aviation-safety.net/database/record.php?id=19920529-1</ref>
*On 2 September 1997, USAF EC-135C, (AF Serial Number ''63-8053''), was heavily damaged on landing at [[Pope AFB]], NC when the nose wheel collapsed.  None of the 11 occupants was injured significantly, but the [[Air Combat Command]] (ACC) aircraft was 32 years and 10 months old at the time of the accident and was written off as damaged beyond repair.<ref>http://aviation-safety.net/database/record.php?id=19970902-1</ref>

==Aircraft on display==
* 60-0374 ''The Bird of Prey'' – EC-135E on static display at the [[National Museum of the United States Air Force]] at [[Wright-Patterson Air Force Base]] in [[Dayton, Ohio]]. The aircraft is a former Apollo / Range Instrumentation Aircraft (ARIA), and is displayed in the museum's outside Air Park.<ref>{{cite web|title=Boeing EC-135E ARIA|url=http://www.nationalmuseum.af.mil/Visit/MuseumExhibits/FactSheets/Display/tabid/509/Article/197557/boeing-ec-135e-aria.aspx|website=National Museum of the US Air Force|accessdate=14 November 2016|date=10 April 2015}}</ref> The aircraft was flown to the museum on November 3, 2000, by a flight crew from the [[Air Force Flight Test Center]] (AFFTC), and was delivered with full Prime Mission Electronic Equipment intact.{{citation needed|date=November 2016}}
* 61-0262 – EC-135C on static display at the [[South Dakota Air and Space Museum]] in [[Box Elder, South Dakota]]. It was last assigned to the 4th Airborne Command and Control Squadron (4th ACCS), [[28th Bomb Wing]] at Ellsworth.<ref>{{cite web|title=Airframe Dossier - Boeing EC-135C, s/n 61-0262 USAF, c/n 18169|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=59995|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=14 November 2016}}</ref>
* 61-0269 ''Excaliber''{{citation needed|date=November 2016}} – EC-135L on static display at the [[Grissom Air Museum]] near [[Peru, Indiana]].<ref>{{cite web|title=EC-135 STRATOTANKER|url=http://www.grissomairmuseum.com/gallery/refuelingcommand/plane-17|website=Grissom Air Museum|accessdate=14 November 2016}}</ref><ref>{{cite web|title=Airframe Dossier - Boeing EC-135L, s/n 61-0269 USAF, c/n 18176|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=43397|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=14 November 2016}}</ref> The aircraft was last assigned to the [[305th Air Refueling Wing]] and retired in 1992, at the end of the [[Cold War]]. It was delivered to the Air Force on 8 December 1961. Assigned to [[Grissom AFB]] in 1970, the aircraft flew many missions during [[Operation Just Cause]], [[Operation Desert Shield]] and [[Desert Storm]]. For the latter, it performed radio relay operations leading to the elimination of two Iraqi aircraft, over 60 tank kills, and 27 Scud missile strikes.{{citation needed|date=November 2016}}
* 61-0287 – EC-135C on static display at Zorinsky Memorial Air Park at [[Offutt Air Force Base]] in [[Bellevue, Nebraska]].<ref>{{cite web|title=Airframe Dossier - Boeing EC-135C, s/n 61-0287 USAF, c/n 18194|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=49317|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=14 November 2016}}</ref>
* 63-8049 – EC-135C under restoration at the [[Strategic Air Command & Aerospace Museum]] in [[Ashland, Nebraska]].<ref>{{cite web|title=EC-135 “Looking Glass”|url=http://sacmuseum.org/what-to-see/aircraft/ec-135-looking-glass|website=Strategic Air Command & Aerospace Museum|publisher=Strategic Air Command & Aerospace Museum|accessdate=14 November 2016}}</ref> It operated the last continuous Looking Glass airborne alert mission on July 24, 1990.{{citation needed|date=November 2016}}
* 63-8057 – EC-135J on static display at the [[Pima Air and Space Museum]] in [[Tucson, Arizona]].<ref>{{cite web|title=STRATOTANKER|url=http://www.pimaair.org/aircraft-by-name/item/boeing-ec-135j-stratotanker|website=Pima Air & Space Museum|publisher=Pimaair.org|accessdate=14 November 2016}}</ref>

==References==
{{commons category|EC-135}}
{{Portal|United States Air Force}}
{{Reflist}}
{{refbegin}}
*Reference for the Variant Summary list: DoD 4120.14L, ''Model Designation of Military Aerospace Vehicles'', May 12, 2004
{{refend}}

==External links==
*{{HAER |survey=NE-9-B |id=ne0074 |title=Offutt Air Force Base, Looking Glass Airborne Command Post, Looking Glass Aircraft, On Operational Apron covering northeast half of Project Looking Glass Historic District, Bellevue, Sarpy County, NE}}

{{707 military variants}}

[[Category:Boeing aircraft|EC-0135, Boeing]]
[[Category:Military communications]]
[[Category:Nuclear warfare]]
[[Category:Strategic Air Command]]
[[Category:Telemetry]]
[[Category:United States experimental aircraft 1960–1969|C-135E, Boeing]]
[[Category:United States military reconnaissance aircraft 1960–1969]]
[[Category:Quadjets]]
[[Category:Historic American Engineering Record in Nebraska]]