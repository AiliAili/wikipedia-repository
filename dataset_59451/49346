{{Infobox Criminal organization
| name                = Henry Loftus and Harry Donaldson
| image               =
| caption             =
| founded on          = November 1937
| founding location   = [[Deming, New Mexico]]
| founded by          = Henry Loftus and Harry Donaldson
| years active        = 1937
| territory           = [[El Paso, Texas]]
| ethnic makeup       =
| membership est      = 2
| criminal activities = [[Train robbery]]
| allies              =
| rivals              =
}}
'''Henry (Lorenz) Loftus''' (born 1915) and '''Harry (Dwyer) Donaldson''' (born 1910) were two young men who made national headlines for their unsuccessful attempt to rob the [[Southern Pacific Railroad]]'s ''Apache Limited'' in 1937. The last major train robbery in the United States, the two have been referred to as "the last of America's classic train robbers". Though they were not professional criminals, their unhealthy fixation with the [[American Wild West]] prompted them to rob the train which resulted in the intentional wounding of one passenger and the accidental death of another.

==History==
Henry Loftus (or Lorenz) was born and raised in [[Manitowoc, Wisconsin]] to German immigrant Conrad Lorenz. As a bored teenager living in a small town, Loftus developed an interest in [[dime novels]] and the [[American Old West]]. This interest eventually evolved into what his family later called a "Wild West complex" though they humored him hoping he would outgrow it. Loftus was still living at home when he committed his first arrest for [[burglary]]. The local town judge sympathized with the young man, [[suspended sentence|suspended his sentence]] and released him on [[probation]].<ref name="Newton">Newton, Michael. ''The Encyclopedia of Robberies, Heists, and Capers''. New York: Facts On File Inc., 2002. (pg. 179-180) ISBN 0-8160-4488-0</ref>

Shortly afterwards, Loftus' father moved the family to [[Chicago]] where he opened a shoe store in 1936. Loftus stayed for only a few months and, after living in [[Brooklyn, New York]] for a time, announced that he was going west "to seek fame and fortune in the wide open spaces". His companion on this journey was Harry Donaldson (or Dwyer), a young Canadian he had recently befriended, who had a similar interest in the Wild West and its outlaws in particular. A year later, they booked a train on the [[Santa Fe Railroad]] to [[El Paso, Texas]]. Upon their arrival, they were disappointed to see that El Paso had long since left behind its frontier past and [[History of El Paso, Texas#The roaring 20s to World War II|had become a modern city]] with paved streets and automobiles much like they left behind in Chicago. While in El Paso, they purchased [[Cowboy#Attire|cowboy outfits]], including [[chaps]] and [[Ten-gallon hats]], and soon found themselves a source of amusement to the local residents. Loftus and Donaldson eventually decided to leave the city by purchasing two horses and heading westward hoping to find something left of the American frontier.<ref name="Newton"/><ref name="Article1">"Youth's, Held In Slaying, Find Story Books Were Wrong". ''[[Pittsburgh Press]]''. 13 December 1937.</ref> 

After two days on the trail, they arrived at [[Deming, New Mexico]]. Confronted with the fact that [[American Old West#Closing out the century|the frontier no longer existed in the American Southwest]], the two formed a plan to rob a train. Selling their horses, they used the money to buy old fashioned six-shooters with hand-tooled holsters and the rest for train tickets back to El Paso.<ref name="Newton"/> 

On November 24, 1937, Loftus and Donaldson were travelling as passengers on the [[Southern Pacific Railroad]]'s westbound ''Apache Limited'' out of El Paso. About an hour into the trip, Loftus sat up from their seat and drew his pistol on [[Conductor (transportation)|conductor]] W.M. Holloway. Holloway later recalled that he "thought [Loftus] was drunk. I didn't think it was a holdup. I was afraid that gun was going off when the amateur's hands started shaking". Regardless, Loftus covered the conductor while Donaldson moved through the passengers collecting several men's watches but no cash. Growing frustrated and nervous, a male passenger made a sudden move causing Donaldson to panic and shot the man in the hip. When Loftus went back to help his partner, he was tackled by an off-duty [[brakeman]], W.L. Smith, who was travelling as a passenger.<ref name="Newton"/> 

Loftus and Smith began wrestling on the floor when the gun suddenly went off killing Smith. At this point, the 20 passengers attacked both men "punching and kicking them in a frenzy". One witness said "If it hadn't been that we had women passengers on the coach, these robbers would have been beaten to death". Lashing the would-be robbers to the seats, they turned them over to the sheriff in Deming, then transferred to [[Las Cruces, New Mexico]] for trial.<ref name="Article1"/><ref>"Train 'Bandits' Held For Trial; Bail Denied". ''[[Sarasota Herald]]''. 21 Dec 1937.</ref> Concerns arose that the two were [[mental disorder defence|unfit for trial]] as then District Attorney Martin Threet had to enter not guilty pleas on their behalf. His father and sister travelled from Chicago to see Loftus, who read passages from the Bible given to him by his father, and his sister Margaret told the press that Loftus "does not seem to grasp the meaning of the charges against him". Neither had funds to help Loftus but said they would return for the trial if they were able. Donaldson's mother, a Mrs. Joseph Thibault, traveled all the way from [[Concession, Nova Scotia]] after using his last 95 cents to telegraph her.<ref name="Article1"/> Facing the death penalty, they pleaded guilty to second-degree murder and were sentenced to serve 50–75 years imprisonment in February 1938.<ref>"Train Robbers Sentenced; Two Youths Get 50 to 75 Years for Death in Hold-Up". ''[[New York Times]]''. 21 Feb 1938.</ref>

Both men were charged and swiftly convicted of robbery and murder, however the circumstances of the crime received national attention. It was the last major train robbery in the United States, the two being referred to as "the last of America's classic train robbers", and officially ended the Old West-style [[train robbery]] started by the [[Reno Brothers]] 70 years before.<ref name="Newton"/>

==References==
{{Reflist}}

==Further reading==
*Long, Haniel, John Kane and Ronald Caplan. ''If He Can Make Her So''. Pittsburgh: Frontier Press, 1968.
*Mottram, Eric. ''Blood on the Nash Ambassador: Investigations in American Culture''. London: Hutchinson Radius, 1989. ISBN 0-09-182354-4
*Simmons, Marc. ''When Six-Guns Ruled: Outlaw Tales of the Southwest''. Santa Fe, New Mexico: Ancient City Press, 1990. ISBN 0-941270-63-7

{{DEFAULTSORT:Loftus, Henry}}
[[Category:Year of birth missing]]
[[Category:Year of death missing]]
[[Category:American criminals]]
[[Category:Train robbers]]
[[Category:Depression-era gangsters]]