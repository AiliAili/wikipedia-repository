{{About|the common cereal}}
{{Redirect|Oats|other cultivated and wild species of the [[genus]]|Avena}}
{{taxobox
 | name                = Oat
 | image               = Avena sativa L.jpg
 | image_caption       = Oat plants with [[inflorescence]]s
 | regnum              = [[Plantae]]
 | unranked_divisio    = [[Angiosperms]]
 | unranked_classis    = [[Monocots]]
 | unranked_ordo       = [[Commelinids]]
 | ordo                = [[Poales]]
 | familia             = [[Poaceae]]
 | genus               = ''[[Avena]]''
 | species             = '''''A. sativa'''''
 | binomial            = ''Avena sativa''
 | binomial_authority  = [[Carl Linnaeus|L.]] (1753)
}}

The '''oat''' (''Avena sativa''), sometimes called the '''common oat''', is a [[species]] of [[Cereal|cereal grain]] grown for its seed, which is known by the same name (usually in the plural, unlike other cereals and [[pseudocereals]]). While oats are suitable for human consumption as [[oatmeal]] and [[rolled oats]], one of the most common uses is as [[livestock]] feed.

==Origin==
The wild ancestor of ''Avena sativa'' and the closely related minor crop, ''A. byzantina'', is the [[Polyploidy|hexaploid]] wild oat ''A. sterilis''. Genetic evidence shows the ancestral forms of ''A. sterilis'' grew in the [[Fertile Crescent]] of the [[Near East]]. Domesticated oats appear relatively late, and far from the Near East, in [[Bronze Age]] Europe. Oats, like [[rye]], are usually considered a [[secondary crop]], i.e., derived from a weed of the primary [[cereal]] domesticates [[wheat]] and [[barley]]. As these cereals spread westwards into cooler, wetter areas, this may have favored the oat weed component, and have led to its domestication.<ref>{{cite journal | last1 = Zhou | first1 = X. | last2 = Jellen | first2 = E.N. | last3 = Murphy | first3 = J.P. | year = 1999 | title = Progenitor germplasm of domesticated hexaploid oat | url = | journal = Crop science | volume = 39 | issue = | pages = 1208–1214 | doi=10.2135/cropsci1999.0011183x003900040042x}}</ref>

==Cultivation==
{| class="wikitable" style="float:right; clear:left;"
|-
! colspan=2|Top Ten Oats Producers—2013<br />(Thousand Metric Tons)
|-
| {{RUS}} || style="text-align:right;"| 4,027
|-
| {{CAN}} || style="text-align:right;"| 2,680
|-
| {{POL}} || style="text-align:right;"| 1,439
|-
| {{FIN}} || style="text-align:right;"| 1,159
|-
| {{AUS}} || style="text-align:right;"| 1,050
|-
| {{USA}} || style="text-align:right;"| 929
|-
| {{ESP}} || style="text-align:right;"| 799
|-
| {{GBR}} || style="text-align:right;"| 784
|-
| {{SWE}} || style="text-align:right;"| 776
|-
| {{GER}} || style="text-align:right;"| 668
|-
| '''World Total''' || style="text-align:right;"| 20,732
|-
|colspan=2|''Source:<ref>{{cite web|title=World oats production, consumption, and stocks|url=http://www.fas.usda.gov/psdonline/psdreport.aspx?hidReportRetrievalName=BVS&hidReportRetrievalID=401&hidReportRetrievalTemplateID=7|work=United States Department of Agriculture|accessdate=18 March 2013}}</ref>
|}
Oats are best grown in [[temperate]] regions. They have a lower summer heat requirement and greater tolerance of rain than other cereals, such as wheat, [[rye]] or barley, so are particularly important in areas with cool, wet summers, such as Northwest Europe and even Iceland. Oats are an [[annual plant]], and can be planted either in autumn (for late summer harvest) or in the spring (for early autumn harvest).

[[Image:OatsYield.png|thumb|200px|left|Worldwide oat production]]
{{Clear}}

==Uses==
[[Image:Avena-sativa.jpg|thumb|right|Closeup of oat florets (small flowers)]]
{{ref improve|section|date=December 2016}}
Oats have numerous uses in foods; most commonly, they are [[rolled oats|rolled]] or crushed into [[oatmeal]], or ground into fine oat [[flour]]. Oatmeal is chiefly eaten as [[porridge]], but may also be used in a variety of baked goods, such as [[oatcake]]s, [[oatmeal cookie]]s and oat bread. Oats are also an ingredient in many cold cereals, in particular [[muesli]] and [[granola]].

Historical attitudes towards oats have varied. [[Oat bread]] was first manufactured in Britain, where the first oat bread factory was established in 1899. In Scotland, they were, and still are, held in high esteem, as a mainstay of the national diet.

In Scotland, a dish was made by soaking the husks from oats for a week, so the fine, floury part of the meal remained as sediment to be strained off, boiled and eaten.<ref name="Gauldie1981">{{cite book|last=Gauldie|first=Enid|title=The Scottish country miller, 1700–1900: a history of water-powered meal milling in Scotland|year=1981|publisher=J. Donald|location=Edinburgh|isbn=0-85976-067-7}}</ref> Oats are also widely used there as a thickener in soups, as barley or rice might be used in other countries.

Oats are also commonly used as feed for horses when extra carbohydrates and the subsequent boost in energy are required. The oat hull may be crushed ("rolled" or "crimped") for the horse to more easily digest the grain,{{citation needed|date=June 2013}} or may be fed whole. They may be given alone or as part of a blended food pellet. Cattle are also fed oats, either whole or ground into a coarse flour using a [[roller mill]], [[burr mill]], or [[hammer mill]].

Winter oats may be grown as an off-season [[groundcover]] and ploughed under in the spring as a [[green fertilizer]], or harvested in early summer. They also can be used for pasture; they can be grazed a while, then allowed to head out for grain production, or grazed continuously until other pastures are ready.<ref>{{cite web|url=http://www.extension.org/pages/13262/grazing-of-oat-pastures |title=Grazing of Oat Pastures |publisher=eXtension |date=2008-02-11 |accessdate=2013-03-27}}</ref>

Oat [[straw]] is prized by cattle and horse producers as bedding, due to its soft, relatively dust-free, and absorbent nature. The straw can also be used for making [[corn dolly|corn dollies]]. Tied in a muslin bag, oat straw was used to soften bath water.

Oats are also occasionally used in several different drinks. In Britain, they are sometimes used for brewing [[beer]]. [[Oatmeal stout]] is one variety brewed using a percentage of oats for the [[wort (brewing)|wort]]. The more rarely used oat malt is produced by the Thomas Fawcett & Sons Maltings and was used in the Maclay Oat Malt Stout before [[Maclays Brewery]] ceased independent brewing operations. A cold, sweet drink called [[Avena (drink)|''avena'']] made of ground oats and milk is a popular refreshment throughout Latin America. Oatmeal [[caudle]], made of ale and oatmeal with spices, was a traditional British drink and a favourite of [[Oliver Cromwell]].<ref>''[[The Compleat Housewife]]'', p. 169, Eliza Smith, 1739</ref><ref>''Food in Early Modern Europe'', Ken Albala, Greenwood Publishing Group, 2003, ISBN 0-313-31962-6</ref>

Oat extract can also be used to soothe skin conditions.

Oat grass has been used traditionally for medicinal purposes, including to help balance the [[menstrual cycle]], treat [[dysmenorrhea|dysmenorrhoea]] and for [[osteoporosis]] and [[urinary tract infections]].<ref>{{cite book|url=https://books.google.com/books?id=8AJkBmPDRUUC|title=James A. Duke, ''Handbook of medicinal herbs'', CRC Press, 2002|publisher=|isbn=9781420040463|author1=Duke|first1=James A|date=2002-06-27}}</ref>

[[File:Oatnoodles.jpg|thumb|youmian，steamed oat noodles and rolls]]
In China, particularly in western Inner Mongolia and Shanxi province, oat (''Avena nuda'') flour called "youmian" is processed into noodles or thin-walled rolls, and is consumed as [[staple food]].{{citation needed|date=December 2016}}

==Health==

===Nutrient profile===
{{nutritionalvalue | name=Oats
| kJ=1628
| protein=16.9 g
| fat=6.9 g
| carbs=66.3 g
| fiber=10.6 g
| calcium_mg=54
| iron_mg=5
| magnesium_mg=177
| phosphorus_mg=523
| potassium_mg=429
| sodium_mg=2
| zinc_mg=4
| manganese_mg=4.9
| thiamin_mg=0.763
| riboflavin_mg=0.139
| niacin_mg=0.961
| pantothenic_mg=1.349
| vitB6_mg=0.12
| folate_ug=56
| opt1n=[[Oat beta-glucan|β-glucan (soluble fibre)&nbsp;]]
| opt1v=4 g
| note=[http://ndb.nal.usda.gov/ndb/foods/show/6507?fg=&man=&lfacet=&count=&max=&sort=&qlookup=&offset=&format=Full&new=&measureby= Full Report of USDA Database entry]
| right=1}}
Oats are generally considered healthy due to their rich content of several [[essential nutrients]] (table). In a 100 gram serving, oats provide 389 [[calories]] and are an excellent source (20% or more of the [[Daily Value]], DV) of [[protein]] (34% DV), [[dietary fiber]] (44% DV), several [[B vitamins]] and numerous [[dietary mineral]]s, especially [[manganese]] (233% DV) (table). Oats are 66% [[carbohydrates]], including 11% dietary fiber and 4% [[beta-glucans]], 7% [[fat]] and 17% protein (table).

The established property of their [[cholesterol]]-lowering effects<ref name="ajcn">{{cite journal|journal=Am J Clin Nutr|year=2014|volume=100|issue=6|pages=1413–21|doi=10.3945/ajcn.114.086108|url=http://ajcn.nutrition.org/content/100/6/1413.long|title=
Cholesterol-lowering effects of oat β-glucan: a meta-analysis of randomized controlled trials|authors=Whitehead A, Beck EJ, Tosh S, Wolever TM|pmid=25411276}}</ref> has led to acceptance of oats as a [[health food]].<ref name="cdc">{{cite web|url=http://www.cdc.gov/nutrition/everyone/basics/carbs.html|title=Nutrition for everyone: carbohydrates|publisher=Centers for Disease Control and Prevention, US Department of Health and Human Services|date=2014|accessdate=8 December 2014}}</ref>
[[Image:Haverkorrels Avena sativa.jpg|right|thumb|Oat grains in their [[husk]]s]]
[[File:Salvado de avena.jpg|thumbnail|A sample of oat [[bran]]]]

===Soluble fiber===
Oat bran is the outer casing of the oat. Its daily consumption over weeks lowers [[Low-density lipoprotein|LDL]] ("bad") and total [[cholesterol]], possibly reducing the risk of [[heart disease]].<ref name="ajcn"/><ref name="WebMD">{{cite web|url=http://www.webmd.com/cholesterol-management/features/the-new-cholesterol-diet-oatmeal-oat-bran|title=LDL Cholesterol and Oatmeal|publisher=WebMD|date=2 February 2009}}</ref>

One type of soluble fiber, [[Oat beta-glucan|beta-glucans]], has been proven to lower cholesterol.<ref name="ajcn"/>

After reports of research finding that dietary oats can help lower cholesterol, the United States [[Food and Drug Administration]] (FDA) issued a final rule<ref name="fda">{{cite web|url=http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfcfr/cfrsearch.cfm?fr=101.81|title=Title 21--Chapter 1, Subchapter B, Part 101 - Food labeling - Specific Requirements for Health Claims, Section 101.81: Health claims: Soluble fiber from certain foods and risk of coronary heart disease (CHD) (revision 2015)|publisher=US Department of Health and Human Services, Food and Drug Administration|date=1 April 2015|accessdate=10 November 2015}}</ref> that allows food companies to make [[health claims on food labels]] of foods that contain soluble fiber from whole oats (oat bran, oat flour and rolled oats), noting that 3.0&nbsp;grams of soluble fiber daily from these foods may reduce the risk of [[heart disease]]. To qualify for the health claim, the whole oat-containing food must provide at least 0.75&nbsp;grams of soluble fiber per serving.<ref name=fda/>

Beta-D-glucans, usually referred to as beta-glucans, comprise a class of indigestible [[polysaccharide]]s widely found in nature in sources such as grains, [[barley]], [[yeast]], [[bacteria]], [[algae]] and [[mushroom]]s. In oats, barley and other cereal grains, they are located primarily in the [[endosperm]] [[cell wall]]. The oat beta-glucan health claim applies to [[oat bran]], [[rolled oats]], whole oat [[flour]] and oatrim, a soluble fraction of [[alpha-amylase]] [[hydrolyzed]] oat bran or whole oat flour.<ref name=fda/>

Oat beta-glucan is a [[viscous]] polysaccharide made up of units of the monosaccharide D-[[glucose]]. Oat beta-glucan is composed of mixed-linkage polysaccharides. This means the [[chemical bond|bonds]] between the D-glucose or D-glucopyranosyl units are either beta-1, 3 linkages or beta-1, 4 linkages. This type of beta-glucan is also referred to as a mixed-linkage (1→3), (1→4)-beta-D-glucan. The (1→3)-linkages break up the uniform structure of the beta-D-glucan molecule and make it soluble and flexible. In comparison, the indigestible polysaccharide [[cellulose]] is also a beta-glucan, but is not soluble because of its (1→4)-beta-D-linkages.{{citation needed|date=December 2014}} The percentages of beta-glucan in the various whole oat products are: oat bran, having from 5.5% to 23.0%; rolled oats, about 4%; and whole oat flour about 4%.

=== Fat ===
Oats, after corn ([[maize]]), have the highest [[lipid]] content of any cereal, e.g., greater than 10% for oats and as high as 17% for some maize cultivars compared to about 2–3% for wheat and most other cereals.{{citation needed|date=December 2014}} The polar lipid content of oats (about 8–17% glycolipid and 10–20% phospholipid or a total of about 33%) is greater than that of other cereals, since much of the lipid fraction is contained within the endosperm.{{citation needed|date=December 2014}}

===Protein===

Oats are the only cereal containing a [[globulin]] or [[legume]]-like protein, [[avenalin]], as the major (80%) storage protein.<ref>{{cite web|url=http://www.plantcell.org/cgi/reprint/7/7/945.pdf |title=Seed Storage Proteins: Structures 'and Biosynthesis |publisher=Plantcell.org |accessdate=2013-03-27}}</ref> Globulins are characterised by solubility in dilute saline as opposed to the more typical cereal proteins, such as [[gluten]] and [[zein]], the [[prolamin]]es (prolamins). The minor protein of oat is a prolamine, avenin.

Oat protein is nearly equivalent in quality to [[soy protein]], which [[World Health Organization]] research has shown to be equal to meat, milk and egg protein.<ref>{{cite book|last=Lasztity|first=Radomir|year=1999|title=The Chemistry of Cereal Proteins|publisher=Akademiai Kiado |isbn=978-0-8493-2763-6}}</ref> The protein content of the hull-less oat kernel ([[groats|groat]]) ranges from 12 to 24%, the highest among cereals.

===Celiac disease===
{{main|Oat sensitivity|Gluten-related disorders}}

[[Coeliac disease|Celiac disease (coeliac disease)]] is a permanent intolerance to [[gluten]] proteins that appears in genetically predisposed people. Gluten is present in wheat, [[barley]], [[rye]], oat, and all their species and hybrids<ref name=Biesiekierski2017>{{cite journal| author=Biesiekierski JR| title=What is gluten? | journal=J Gastroenterol Hepatol | year= 2017 | volume= 32 Suppl 1 | pages= 78-81 | pmid=28244676 | doi=10.1111/jgh.13703 | url= http://onlinelibrary.wiley.com/doi/10.1111/jgh.13703/full | type=Review }}{{open access}}</ref><ref name=TovoliMasi>{{cite journal | vauthors = Tovoli F, Masi C, Guidetti E, Negrini G, Paterini P, Bolondi L| title = Clinical and diagnostic aspects of gluten related disorders| journal = World J Clin Cases| volume = 3| issue = 3| pages = 275–84| date = Mar 16, 2015| pmid = 25789300|pmc= 4360499| doi = 10.12998/wjcc.v3.i3.275}}</ref> and contains hundreds of proteins, with high contents of [[prolamin]]s.<ref name=LamacchiaCamarca2014>{{cite journal|vauthors=Lamacchia C, Camarca A, Picascia S, Di Luccia A, Gianfrani C| title=Cereal-based gluten-free food: how to reconcile nutritional and technological properties of wheat proteins with safety for celiac disease patients|journal=Nutrients|volume=6| issue=2| pages=575–90| date=Jan 29, 2014| pmid=24481131|pmc= 3942718| doi=10.3390/nu6020575}}</ref> 

Oats prolamins are called [[avenin]]s.<ref name=CominoMoreno2013>{{cite journal|vauthors=Comino I, Moreno Mde L, Real A, Rodríguez-Herrera A, Barro F, Sousa C|title=The gluten-free diet: testing alternative cereals tolerated by celiac patients|journal=Nutrients|volume=5|issue=10|pages=4250–68|date=Oct 23, 2013|pmid=24152755|doi=10.3390/nu5104250|pmc=3820072|type=Review}}</ref> Their toxicity in celiac people depends on the oat [[cultivar]] consumed because of prolamin genes, protein amino acid sequences, and the [[immunoreactivity|immunoreactivities]] of toxic prolamins which are different among oat varieties.<ref name=CominoMoreno2015>{{cite journal | vauthors = Comino I, Moreno Mde L, Sousa C | title = Role of oats in celiac disease | journal = World J Gastroenterol | volume = 21 | issue = 41 | pages = 11825–31 | date = Nov 7, 2015 | pmid = 26557006 |pmc= 4631980 | doi = 10.3748/wjg.v21.i41.11825 |quote= It is necessary to consider that oats include many varieties, containing various amino acid sequences and showing different immunoreactivities associated with toxic prolamins. As a result, several studies have shown that the immunogenicity of oats varies depending on the cultivar consumed. Thus, it is essential to thoroughly study the variety of oats used in a food ingredient before including it in a gluten-free diet.}}</ref><ref name=PenaginiDilillo>{{cite journal | vauthors = Penagini F, Dilillo D, Meneghin F, Mameli C, Fabiano V, Zuccotti GV| title = Gluten-free diet in children: an approach to a nutritionally adequate and balanced diet | journal = Nutrients | volume = 5| issue = 11| pages = 4553–65| date = Nov 18, 2013| pmid = 24253052|pmc= 3847748| doi = 10.3390/nu5114553}}</ref> Also, oats are frequently cross-contaminated with other gluten-containing cereals during grain harvesting, transport, storage or processing.<ref name=CominoMoreno2015 /><ref name=PenaginiDilillo /><ref name=DeSouzaDeschenes2016>{{cite journal| vauthors=de Souza MC, Deschênes ME, Laurencelle S, Godet P, Roy CC, Djilali-Saiah I| title=Pure Oats as Part of the Canadian Gluten-Free Diet in Celiac Disease: The Need to Revisit the Issue. | journal=Can J Gastroenterol Hepatol | year= 2016 | volume= 2016 | issue=  | pages= 1576360 | pmid=27446824 | doi=10.1155/2016/1576360 | pmc=4904650 | type= Review }} </ref>

Pure oat refers to oats uncontaminated with other gluten-containing cereals.<ref name=CominoMoreno2015 /> Some cultivars of pure oat could be a safe part of a [[gluten-free diet]], requiring knowledge of the oat variety used in food products for a gluten-free diet.<ref name=CominoMoreno2015>{{cite journal | vauthors = Comino I, Moreno Mde L, Sousa C | title = Role of oats in celiac disease | journal = World J Gastroenterol | volume = 21 | issue = 41 | pages = 11825–31 | date = Nov 7, 2015 | pmid = 26557006 |pmc= 4631980 | doi = 10.3748/wjg.v21.i41.11825 |quote= It is necessary to consider that oats include many varieties, containing various amino acid sequences and showing different immunoreactivities associated with toxic prolamins. As a result, several studies have shown that the immunogenicity of oats varies depending on the cultivar consumed. Thus, it is essential to thoroughly study the variety of oats used in a food ingredient before including it in a gluten-free diet.}}</ref> Nevertheless, the long-term effects of pure oats consumption are still unclear<ref name=HaboubiTaylor2006 /> and further studies identifying the cultivars used are needed before making final recommendations on their inclusion in the gluten-free diet.<ref name=DeSouzaDeschenes2016 /> Celiac people who choose to consume oats need a more rigorous lifelong follow-up, possibly including periodic performance of [[coeliac disease#Endoscopy|intestinal biopsies]].<ref name=HaboubiTaylor2006>{{cite journal|vauthors=Haboubi NY, Taylor S, Jones S|title=Coeliac disease and oats: a systematic review|journal=Postgrad Med J|volume=82|issue=972|pages=672–8|date=Oct 2006|pmid=17068278|pmc=2653911|doi=10.1136/pgmj.2006.045443|type=Review}}</ref>

==Agronomy==
[[File:2013 07 13 Avoine noire d'Epinal.JPG|thumb|left|''Noire d'Epinal'', an ancient oat variety.]]
[[File:SK-OatAvena-sativa.JPG|thumb|200px|Oats in [[Saskatchewan]] near harvest time]]
Oats are sown in the spring or early summer in colder areas, as soon as the soil can be worked. An early start is crucial to good fields, as oats go dormant in summer heat. In warmer areas, oats are sown in late summer or early fall. Oats are cold-tolerant and are unaffected by late frosts or snow.

===Seeding rates===
Typically, about 125 to 175&nbsp;kg/ha (between 2.75 and 3.25 [[bushels]] per acre) are sown, either [[Broadcast spreader|broadcast]] or [[seed drill|drilled]]. Lower rates are used when [[interseeding]] with a legume. Somewhat higher rates can be used on the best soils, or where there are problems with weeds. Excessive sowing rates lead to problems with lodging, and may reduce yields.

===Fertilizer requirements===
Oats remove substantial amounts of [[nitrogen]] from the soil. They also remove phosphorus in the form of P<sub>2</sub>O<sub>5</sub> at the rate of 0.25 pound per bushel per acre (1 bushel = 38 pounds at 12% moisture).{{Citation needed|date=December 2007}} Phosphate is thus applied at a rate of 30 to 40&nbsp;kg/ha, or 30 to 40&nbsp;lb/acre. Oats remove potash (K<sub>2</sub>O) at a rate of 0.19 pound per bushel per acre, which causes it to use 15–30&nbsp;kg/ha, or 13–27&nbsp;lb/acre. Usually, 50–100&nbsp;kg/ha (45–90&nbsp;lb/ac) of nitrogen in the form of [[urea]] or [[ammonia|anhydrous ammonia]] is sufficient, as oats use about one pound per bushel per acre. A sufficient amount of nitrogen is particularly important for plant height and hence, straw quality and yield. When the prior-year crop was a legume, or where ample manure is applied, nitrogen rates can be reduced somewhat.

===Weed control===
The vigorous growth of oats tends to choke out most weeds. A few tall [[broadleaf]] weeds, such as [[ragweed]], [[goosegrass]], [[wild mustard]], and [[buttonweed]] (velvetleaf), occasionally create a problem, as they complicate harvest and reduce yields. These can be controlled with a modest application of a broadleaf herbicide, such as [[2,4-D]], while the weeds are still small.

===Pests and diseases===
Oats are relatively free from diseases and pests with the exception being leaf diseases, such as leaf rust and [[stem rust]]. However, ''[[Puccinia coronata]]'' var. avenae is a pathogen that can greatly reduce crop yields.<ref>{{Cite web|url = http://www.ars.usda.gov/Main/docs.htm?docid=9919|title = Oat crown rust|date = 18 April 2008|accessdate = 15 November 2015|website = Cereal Disease Laboratory|publisher = United States Department of Agriculture{{!}} Agricultural Research Service|last = |first = }}</ref> A few [[lepidoptera]]n [[caterpillar]]s feed on the plants—e.g. [[rustic shoulder-knot]] and [[Setaceous Hebrew Character|setaceous Hebrew character moths]], but these rarely become a major pest. See also [[List of oat diseases]].

===Harvesting===
[[Image:Havreskjering Fossheim Lindahl.jpeg|thumb|Harvesting of oats in [[Jølster]], Norway ''ca.'' 1890 <br /> ''(Photo: [[Axel Lindahl]]/Norwegian Museum of Cultural History)'']]
Harvest techniques are a matter of available equipment, local tradition, and priorities. Farmers seeking the highest yield from their crops time their harvest so the kernels have reached 35% moisture, or when the greenest kernels are just turning cream-colour. They then harvest by [[swather|swathing]], cutting the plants at about {{convert|10|cm|in|abbr=on}} above ground, and putting the swathed plants into [[windrows]] with the grain all oriented the same way. They leave the windrows to dry in the sun for several days before combining them using a [[pickup header]]. Finally, they bale the straw.

Oats can also be left standing until completely ripe and then [[combine harvester|combined]] with a grain head. This causes greater field losses as the grain falls from the heads, and to harvesting losses, as the grain is threshed out by the reel. Without a [[draper head]], there is also more damage to the straw, since it is not properly oriented as it enters the combine's throat. Overall yield loss is 10–15% compared to proper swathing.

Historical harvest methods involved cutting with a scythe or sickle, and [[threshing]] under the feet of cattle. Late 19th- and early 20th-century harvesting was performed using a [[Reaper-binder|binder]]. Oats were gathered into shocks, and then collected and run through a stationary [[threshing machine]].

===Storage===
After combining, the oats are transported to the farmyard using a grain truck, semi, or [[road train]], where they are [[Screw conveyor|auger]]ed or [[conveyor|conveyed]] into a [[grain bin|bin]] for storage. Sometimes, when there is not enough bin space, they are augered into portable grain rings, or piled on the ground. Oats can be safely stored at 12-14% moisture; at higher moisture levels, they must be [[aerate]]d or dried.

===Yield and quality===
In the United States, No.1 oats weigh {{convert|42|lb/USbu|0}}; No.3 oats must weigh at least {{convert|38|lb/USbu|0|abbr=on}}. If over {{convert|36|lb/USbu|0|abbr=on}}, they are graded as No.4 and oats under {{convert|36|lb/USbu|0|abbr=on}} are graded as "light weight".
[[File:Avena sativa 004.JPG|thumb|Oat seeds]]
In Canada, No.1 oats weigh {{convert|42.64|lb/USbu|0|abbr=on}}; No.2 oats must weigh {{convert|40.18|lb/USbu|0|abbr=on}}; No.3 oats must weigh at least {{convert|38.54|lb/USbu|0|abbr=on}} and if oats are lighter than {{convert|36.08|lb/USbu|0|abbr=on}} they do not make No.4 oats and have no grade.<ref>{{cite web|url=http://www.grainscanada.gc.ca/oggg-gocg/07/oggg-gocg-7e-eng.htm|title=Oats – Chapter 7 – Official Grain Grading Guide – 5 / 7 |publisher=Grainscanada.gc.ca |date=2012-07-27 |accessdate=2013-03-27}}</ref>

Note, however, that oats are bought and sold and yields are figured, on the basis of a bushel equal to {{convert|32|lb|1|disp=x| (}} or {{convert|32|lb/USbu|abbr=on|0|disp=output only}}) in the United States and a bushel equal to {{convert|34|lb|1|disp=x| (}} or {{convert|34|lb/USbu|abbr=on|0|disp=output only}}) in Canada.  "Bright oats" were sold on the basis of a bushel equal to {{convert|48|lb|1|disp=x| (}} or {{convert|48|lb/USbu|abbr=on|0|disp=output only}}) in the United States.

Yields range from {{convert|60|to-|80|USbu/acre}} on marginal land, to {{convert|100|to-|150|USbu/acre}} on high-producing land. The average production is 100 bushels per acre, or 3.5 tonnes per hectare.

Straw yields are variable, ranging from one to three tonnes per hectare, mainly due to available nutrients and the variety used (some are short-strawed, meant specifically for straight combining).

==Processing==

{{Refimprove section|date=October 2013}}
[[File:Porridge oats.JPG|thumb|right|[[Porridge]] oats before cooking]]

Oats processing is a relatively simple process:

===Cleaning and sizing===
Upon delivery to the milling plant, chaff, rocks, other grains and other foreign material are removed from the oats.

===Dehulling===
Centrifugal acceleration is used to separate the outer hull from the inner oat groat. Oats are fed by gravity onto the centre of a horizontally spinning stone, which accelerates them towards the outer ring. Groats and hulls are separated on impact with this ring. The lighter oat hulls are then aspirated away, while the denser oat groats are taken to the next step of processing. Oat hulls can be used as feed, processed further into insoluble oat fibre, or used as a biomass fuel.

===Kilning===
The unsized oat groats pass through a heat and moisture treatment to balance moisture, but mainly to stabilize them. Oat groats are high in fat (lipids) and once removed from their protective hulls and exposed to air, enzymatic (lipase) activity begins to break down the fat into free fatty acids, ultimately causing an [[Off-flavours|off-flavour]] or [[rancidity]]. Oats begin to show signs of enzymatic rancidity within four days of being dehulled if not stabilized. This process is primarily done in food-grade plants, not in feed-grade plants. Groats are not considered raw if they have gone through this process; the heat disrupts the germ and they cannot sprout.

===Sizing of groats===
Many whole oat groats break during the dehulling process, leaving the following types of groats to be sized and separated for further processing: whole oat groats, coarse steel cut groats, steel cut groats, and fine steel cut groats. Groats are sized and separated using screens, shakers and indent screens. After the whole oat groats are separated, the remaining broken groats get sized again into the three groups (coarse, regular, fine), and then stored. "Steel cut" refers to all sized or cut groats. When not enough broken groats are available to size for further processing, whole oat groats are sent to a cutting unit with steel blades that evenly cut groats into the three sizes above.

===Final processing===
Three methods are used to make the finished product:

====Flaking====
This process uses two large smooth or corrugated rolls spinning at the same speed in opposite directions at a controlled distance. Oat flakes, also known as [[rolled oats]], have many different sizes, thicknesses and other characteristics depending on the size of oat groats passed between the rolls. Typically, the three sizes of steel cut oats are used to make instant, baby and quick rolled oats, whereas whole oat groats are used to make regular, medium and thick rolled oats. Oat flakes range in thickness from 0.36&nbsp;mm to 1.00&nbsp;mm.

====Oat bran milling====
This process takes the oat groats through several roll stands to flatten and separate the bran from the flour (endosperm). The two separate products (flour and bran) get sifted through a gyrating sifter screen to further separate them. The final products are oat bran and debranned oat flour.

====Whole flour milling====
This process takes oat groats straight to a grinding unit (stone or hammer mill) and then over sifter screens to separate the coarse flour and final whole oat flour. The coarser flour is sent back to the grinding unit until it is ground fine enough to be whole oat flour. This method is used often in India and other countries. In India whole grain flour of oats ('''jai''') used to make Indian bread known as '''jarobra''' in [[Himachal Pradesh]].

====Oat flour preparation at home====
Oat flour can be purchased, but one can grind for small scale use by pulsing [[rolled oats]] or old-fashioned (not quick) oats in a [[food processor]] or [[spice mill]].<ref>The Sparkpeople cookbook: love your food, lose the weight, Galvin, M., Romnie, S., May House Inc, 2011, ISBN 978-1-4019-3132-2, page 98.</ref><ref>["http://www.wisegeek.com/what-is-oat-flour.htm "What is out flour?"] wiseGeek.com. Retrieved 7 July 2013.</ref>

==Naming==
In [[Scottish English]], oats may be referred to as '''corn'''.<ref>
{{cite book
|title=Usage and Abusage: A Guide to Good English
|first=Eric
|last=Partridge
|editor=Janet Whitcut
|isbn=0-393-03761-4
|location=New York
|publisher=W.W. Norton, 1995
|year=1995
|edition=1st American
|page=82
|url=https://books.google.com/?id=icnKIlILT4oC&pg=PA82&vq=corn
}}</ref>  (In the English language, the major staple grain of the local area is often referred to as "corn".<ref name="Shorter Oxford English Dictionary">{{cite book|last1=NA|title=Shorter Oxford English Dictionary|date=2007|publisher=Oxford University Press|location=Oxford|isbn=978-0-19-920687-2|page=522}}</ref> In the US, "corn" originates from "Indian corn" and refers to what others call "maize".)<ref name="Shorter Oxford English Dictionary"/>

==Oats futures==
Oats [[Futures exchange|futures]] are traded on the [[Chicago Board of Trade]] and have delivery dates in March (H), May (K), July (N), September (U) and December (Z).<ref>[[Wikinvest:List of Commodity Delivery Dates|List of Commodity Delivery Dates on Wikinvest]]</ref>

==See also==

===Oat Products and Derivatives===
{{div col|colwidth=30em}}
*[[Export hay]]
*[[Muesli]]
*[[Oat bread]]
*[[Oatcake]]
*[[Oatmeal]]
*[[Oat milk]]
*[[Parkin (cake)]]
*[[Porridge]]
*[[Rolled oats]]
*[[Steel-cut oats]]
{{div col end}}

===Major Oat Businesses===
*[[Jordans (company)]]
*[[Mornflake]]
*[[Quaker Oats Company]]

==References==
{{reflist|2}}

== External links ==
{{commons|Avena sativa}}

{{Cereals}}
{{Agriculture country lists}}
{{Bioenergy}}

[[Category:Oats| ]]
[[Category:Avena]]
[[Category:Cereals]]
[[Category:Demulcents]]
[[Category:Fodder]]
[[Category:Medicinal plants]]
[[Category:Plants described in 1753]]
[[Category:Staple foods]]