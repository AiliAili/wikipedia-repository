In [[queueing theory]], a discipline within the mathematical [[probability theory|theory of probability]], a '''heavy traffic approximation''' (sometimes '''heavy traffic limit theorem'''<ref name="halfin-whitt">{{Cite journal | last1 = Halfin | first1 = S. | last2 = Whitt | first2 = W. | authorlink2 = Ward Whitt| doi = 10.1287/opre.29.3.567 | title = Heavy-Traffic Limits for Queues with Many Exponential Servers | journal = Operations Research | volume = 29 | issue = 3 | pages = 567 | year = 1981 | url = http://www.columbia.edu/~ww2040/HalfinWW1981.pdf| pmid =  | pmc = }}</ref> or '''diffusion approximation''') is the matching of a queueing model with a [[diffusion process]] under some [[limit (mathematics)|limiting]] conditions on the model's parameters. The first such result was published by [[John Kingman]] who showed that when the utilisation parameter of an [[M/M/1 queue]] is near 1 a scaled version of the queue length process can be accurately approximated by a [[reflected Brownian motion]].<ref>{{Cite journal | last1 = Kingman | first1 = J. F. C. | authorlink = John Kingman| doi = 10.1017/S0305004100036094 | author2 = <!-- (exclude bad crossref data) --> | last2 = Atiyah | title = The single server queue in heavy traffic | journal = [[Mathematical Proceedings of the Cambridge Philosophical Society]]| volume = 57 | issue = 4 | pages = 902 | date=October 1961 | jstor = 2984229| pmid =  | pmc = }}</ref>

==Heavy traffic condition ==
Heavy traffic approximations are typically stated for the process ''X''(''t'') describing the number of customers in the system at time ''t''. They are arrived at by considering the model under the limiting values of some model parameters and therefore for the result to be finite the model must be rescaled by a factor ''n'', denoted<ref name="gautam">{{cite book | first = Natarajan | last = Gautam | title = Analysis of Queues: Methods and Applications | publisher = CRC Press | year = 2012 | isbn = 9781439806586}}</ref>{{rp|490}}

::<math>\hat X_n(t) = \frac{X(nt) - \mathbb E(X(nt))}{\sqrt{n}}</math>

and the limit of this process is considered as ''n''&nbsp;→&nbsp;∞.

There are three classes of regime under which such approximations are generally considered.

# The number of servers is fixed and the traffic intensity (utilization) is increased to 1 (from below). The queue length approximation is a [[reflected Brownian motion]].<ref name="On Queues in Heavy Traffic">{{cite journal | last1 = Kingman | first1 = J. F. C. | authorlink1 = John Kingman | year = 1962 | title = On Queues in Heavy Traffic | journal = Journal of the Royal Statistical Society. Series B (Methodological) | volume = 24 | issue = 2 | pages = 383–392  | publisher = Wiley |jstor=2984229}}</ref><ref>{{cite journal | last1 = Iglehart | first1 = Donald L. | last2 = Ward | first2 = Whitt | authorlink2 = Ward Whitt | year = 1970 | title = Multiple Channel Queues in Heavy Traffic. II: Sequences, Networks, and Batches | journal = Advances in Applied Probability | volume = 2 | issue = 2 | pages = 355–369  | publisher = Applied Probability Trust | jstor = 1426324 | accessdate = 30 Nov 2012 | url = http://www.columbia.edu/~ww2040/MultipleChannel1970II.pdf}}</ref><ref>{{cite journal | last1 = Köllerström | first1 = Julian | year = 1974 | title = Heavy Traffic Theory for Queues with Several Servers. I | journal = Journal of Applied Probability  | volume = 11 | issue = 3 | pages = 544–552 | publisher =  Applied Probability Trust | jstor = 3212698}}</ref>
# Traffic intensity is fixed and the number of servers and arrival rate are increased to infinity. Here the queue length limit converges to the [[normal distribution]].<ref>{{cite journal | last1 = Iglehart | first1 = Donald L. | year = 1965 | title = Limiting Diffusion Approximations for the Many Server Queue and the Repairman Problem | journal = Journal of Applied Probability  | volume = 2 | issue = 2 | pages = 429–441 | publisher = Applied Probability Trust | jstor = 3212203}}</ref><ref>{{Cite journal | last1 = Borovkov | first1 = A. A. | title = On limit laws for service processes in multi-channel systems | doi = 10.1007/BF01040651 | journal = Siberian Mathematical Journal | volume = 8 | issue = 5 | pages = 746–763 | year = 1967 | pmid =  | pmc = }}</ref><ref>{{cite journal | last1 = Iglehart | first1 = Donald L. | year = 1973 | title = Weak Convergence in Queueing Theory | journal = Advances in Applied Probability | volume = 5 | issue = 3 | pages = 570–594 | publisher = Applied Probability Trust | jstor = 1425835}}</ref>
# A quantity ''β'' is fixed where 
::<math>\beta = (1-\rho)\sqrt{s}</math>
::with ''ρ'' representing the traffic intensity and ''s'' the number of servers. Traffic intensity and the number of servers are increased to infinity and the limiting process is a hybrid of the above results. This case, first published by Halfin and Whitt is often known as the Halfin–Whitt regime<ref name="halfin-whitt" /><ref>{{Cite journal | last1 = Puhalskii | first1 = A. A. | last2 = Reiman | first2 = M. I. | doi = 10.1239/aap/1013540179 | title = The multiclass GI/PH/N queue in the Halfin-Whitt regime | journal = Advances in Applied Probability | volume = 32 | issue = 2 | pages = 564 | year = 2000 | pmid =  | pmc = }}</ref><ref>{{Cite journal | last1 = Reed | first1 = J. | title = The G/GI/N queue in the Halfin–Whitt regime | doi = 10.1214/09-AAP609 | journal = The Annals of Applied Probability | volume = 19 | issue = 6 | pages = 2211 | year = 2009 | pmid =  | pmc = }}</ref> or quality-and-efficiency-driven (QED) regime.<ref>{{Cite journal | last1 = Whitt | first1 = W. | authorlink1 = Ward Whitt| title = Efficiency-Driven Heavy-Traffic Approximations for Many-Server Queues with Abandonments | doi = 10.1287/mnsc.1040.0279 | journal = [[Management Science (journal)|Management Science]]| volume = 50 | issue = 10 | pages = 1449–1461 | year = 2004 | jstor = 30046186| url = http://www.columbia.edu/~ww2040/efficiencyCOR.pdf| pmid =  | pmc = }}</ref>

==Results for a G/G/1 queue ==
'''Theorem 1.''' <ref>{{Cite book | last1 = Gross | first1 = D. | last2 = Shortie | first2 = J. F. | last3 = Thompson | first3 = J. M. | last4 = Harris | first4 = C. M. | chapter = Bounds and Approximations | doi = 10.1002/9781118625651.ch7 | title = Fundamentals of Queueing Theory | pages = 329–368| year = 2013 | isbn = 9781118625651 | pmid =  | pmc = }}</ref> Consider a sequence of [[G/G/1 queue]]s indexed by <math>j</math>. 
<br>
For queue <math>j</math> 
<br> 
let <math>T_j</math> denote the random inter-arrival time,  <math>S_j</math> denote the random service time;
let <math>\rho_j=\frac{\lambda_j}{\mu_j}</math> denote the traffic intensity with <math>\frac{1}{\lambda_j}=E(T_j)</math> and <math>\frac{1}{\mu_j}=E(S_j)</math>;  
let <math>W_{q,j}</math> denote the waiting time in queue for a customer in steady state; 
Let  <math>\alpha_j=-E[S_j-T_j]</math> and <math>\beta_j^2=\operatorname{var}[S_j-T_j];</math>

Suppose that <math>T_j\xrightarrow{d} T</math>, <math>S_j\xrightarrow{d} S</math>, and <math>\rho_j \rightarrow  1</math>.  then 
<br>
: <math> \frac{2\alpha_j}{\beta_j^2}W_{q,j}\xrightarrow{d} \exp(1)</math>

provided that:

(a) <math>\operatorname{Var}[S-T]>0</math>  
(b) for some <math>\delta > 0</math>, <math>E[S_j^{2+\delta}]</math> and <math>E[T_j^{2+\delta}]</math> are both less than some constant <math>C</math> for all <math>j</math>.

==Heuristic argument==
* '''Waiting time in queue'''
Let <math>U^{(n)}=S^{(n)}-T^{(n)}</math> be the difference between the nth service time and the nth inter-arrival time;
Let <math>W_q^{(n)}</math> be the waiting time in queue of the nth customer;

Then by definition:
:<math>W_q^{(n)}=\max(W_q^{(n-1)}+U^{(n-1)},0)</math>
After recursive calculation, we have:
:<math>W_q^{(n)}=\max(U^{(1)}+\cdots+U^{(n-1)},U^{(2)}+\cdots+U^{(n-1)}, \ldots,U^{(n-1)},0)</math>

* '''Random walk'''
Let <math> P^{(k)}=\sum_{i=1}^{k}U^{(n-i)}</math>, with <math>U^{(i)}</math> are i.i.d; 
Define <math>\alpha=-E[U^{(i)}]</math> and <math>\beta^2=\operatorname{var}[U^{(i)}]</math>;

Then we have 
:<math>E[P^{(k)}]=-k\alpha</math>
:<math>\operatorname{var}[P^{(k)}]=k\beta^2</math>
:<math>W_q^{(n)}=\max_{n-1\geq k \geq 0}P^{(k)};</math>
we get <math>W_q^{(\infty)}=\sup_{k \geq 0} P^{(k)}</math> by taking limit over <math>n</math>.

Thus the waiting time in queue of the nth customer <math>W_q^{(n)}</math> is the supremum of a [[random walk]] with a negative drift.

* '''Brownian motion approximation'''
[[Random walk]] can be approximated by a [[Brownian motion]] when the jump sizes approach 0 and the times between the jump approach 0.

We have <math>P^{(0)}=0</math> and <math>P^{(k)}</math> has independent and stationary increments. When the traffic intensity <math>\rho</math> approaches 1 and k approach to <math>\infty</math>, we have <math>P^{(t)} \ \sim\ \N(-\alpha t, \beta^2 t )</math>  after replaced k with continuous value t according to [[functional central limit theorem]].<ref>{{Cite book | last1 = Chen | first1 = H. | last2 = Yao | first2 = D. D. | chapter = Technical Desiderata | doi = 10.1007/978-1-4757-5301-1_5 | title = Fundamentals of Queueing Networks | series = Stochastic Modelling and Applied Probability | volume = 46 | pages = 97–124| year = 2001 | isbn = 978-1-4419-2896-2 | pmid =  | pmc = }}</ref>{{rp|110}} Thus the waiting time in queue of the nth customer can be approximated by the supremum of a [[Brownian motion]] with a negative drift.

* '''Supremum of Brownian motion''' 
'''Theorem 2.'''<ref>{{Cite book | last1 = Chen | first1 = H. | last2 = Yao | first2 = D. D. | chapter = Single-Station Queues | doi = 10.1007/978-1-4757-5301-1_6 | title = Fundamentals of Queueing Networks | series = Stochastic Modelling and Applied Probability | volume = 46 | pages = 125–158| year = 2001 | isbn = 978-1-4419-2896-2 | pmid =  | pmc = }}</ref>{{rp|130}} Let <math>X</math> be a [[Brownian motion]] with drift <math>\mu</math> and standard deviation <math>\sigma</math> starting at the origin, and let <math>M_t\sup_{0\leq s\leq t} X(s)</math>

if <math>\mu \leq 0</math>
:<math>\lim_{t\rightarrow \infty}P(M_t > x)=\exp(2\mu x/\sigma^2 ), x \geq 0; </math>
otherwise 
:<math>\lim_{t\rightarrow \infty} P(M_t\geq x)=1, x \geq 0. </math>

== Conclusion ==
:<math> W_q^{(\infty)}\thicksim \exp\left(\frac{2\alpha}{\beta^2}\right)</math>  under heavy traffic condition
Thus, the heavy traffic limit theorem (Theorem 1) is heuristically argued. Formal proofs usually follow a different approach which involve [[characteristic function (probability theory)|characteristic function]]s.<ref name="On Queues in Heavy Traffic"/><ref>{{Cite book | doi = 10.1007/0-387-21525-5_10 | first = S. R. | last = Asmussen| chapter = Steady-State Properties of GI/G/1 | title = Applied Probability and Queues | series = Stochastic Modelling and Applied Probability | volume = 51 | pages = 266–301 | year = 2003 | isbn = 978-0-387-00211-8 | pmid =  | pmc = }}</ref>

== Example ==
Consider an [[M/G/1 queue]] with arrival rate <math>\lambda</math>,the mean of the service time <math>E[S]=\frac{1}{\mu}</math>, and the variance of the service time <math>\operatorname{var}[S]=\sigma_{B}^2</math>. What is average waiting time in queue in the [[steady state]]?

The exact average waiting time in queue in [[steady state]] is give by:
: <math>W_q=\frac{\rho^2+\lambda^2\sigma_B^2}{2\lambda(1-\rho)}</math>
The corresponding heavy traffic approximation:  
:<math>W_q^{(H)}=\frac{\lambda(\frac{1}{\lambda^2}+\sigma_B^2)}{2(1-\rho)}.</math>
The relative error of the heavy traffic approximation:  
: <math>\frac{W_q^{(H)}-W_q}{W_q}=\frac{1-\rho^2}{\rho^2+\lambda^2\sigma_B^2}</math>
Thus when <math>\rho\rightarrow 1</math>, we have : 
:<math>\frac{W_q^{(H)}-W_q}{W_q} \rightarrow 0. </math>

==External links==
* [http://math.nsc.ru/LBRT/v1/foss/gg1_2803.pdf The G/G/1 queue] by Sergey Foss

==References==
{{reflist}}

{{Queueing theory}}

[[Category:Traffic simulation]]
[[Category:Queueing theory]]