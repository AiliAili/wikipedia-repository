{{Orphan|date=June 2016}}

{{Infobox scientist
| name = Catherine K. King
| image = Photo - Catherine K King.jpg
| image_size = 250px
| caption = 
| birth_name = 
| birth_date = 
| death_date = 
| nationality = [[Australia]]n
| fields = Antarctic [[ecotoxicology]]
| workplaces = [[Australian Antarctic Division]]
| alma_mater = [[University of Sydney]]
| known_for = 
| awards = 2006 [[CSIRO]] Medal for Research Achievement
| website = [http://www.antarctica.gov.au/science/meet-our-scientists/dr-catherine-king Catherine King at antarctica.gov.au]
}}

'''Catherine K. King''' is an Australian [[Ecotoxicology|ecotoxicologist]] who studies [[Subantarctic|sub-Antarctic]] and [[Antarctic]] regions, with a focus on [[climate change]] and the impacts of contaminants and environmental stressors in terrestrial and [[Marine ecosystem|marine ecosystems]].<ref>{{Cite web|url=http://www.antarctica.gov.au/science/meet-our-scientists/dr-catherine-king|title=Dr Catherine King|last=|first=|date=|website=antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref>

== Early life and education ==
King was raised and educated in [[Sydney]], NSW, Australia. She studied at the [[University of Sydney]] where she obtained both her BSc Hons (1992) and PhD (1999) degrees. For her PhD, King investigated the impact of metals and organic contaminants on the development of marine invertebrates from the Sydney region. King also completed a Grad. Dip Education at the [[University of New England (Australia)|University of New England]], NSW in 2003.

== Career and impact ==
King is an environmental scientist with over 25 years’ experience in the field of ecotoxicology and environmental risk assessment. She is a Senior Research Scientist at the [[Australian Antarctic Division]] (AAD) leading the Ecotoxicology Research group within the Antarctic Conservation and Management Program. She is an Adjunct Senior Lecturer at the Institute of Marine and Antarctic Studies (IMAS),<ref>{{Cite web|url=http://www.imas.utas.edu.au|title=Home|last=|first=|date=|website=imas.utas.edu.au|publisher=Institute for Marine and Antarctic Studies|access-date=2016-06-19}}</ref> [[University of Tasmania]], a member of the Australian Marine Sciences Association (AMSA),<ref>{{Cite web|url=https://www.amsa.asn.au|title=Australian Marine Sciences Association|last=|first=|date=|website=amsa.asn.au|publisher=Australian Marine Sciences Association|access-date=2016-06-19}}</ref> and the Tasmanian representative for the Australasian chapter of the Society of Environmental Toxicology and Chemistry (SETAC-AU).<ref>{{Cite web|url=http://australasia.setac.org|title=SETAC Australasia|last=|first=|date=|website=australasia.setac.org|publisher=Society of Environmental Toxicology and Chemistry|access-date=2016-06-19}}</ref> She has supervised nearly 30 postgraduate research students.<ref>{{Cite web|url=https://www.uow.edu.au/unispeak/UOW195214.html|title=Journey to the Great White Desert|last=|first=|date=|website=uow.edu.au|publisher=University of Wollongong|language=en-au|access-date=2016-06-19}}</ref><ref>{{Cite web|url=http://www.antarctica.gov.au/science/information-for-scientists/phd-program-in-quantitative-antarctic-science/supervisors|title=Supervisors|last=|first=|date=|website=antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref>

King’s multi-disciplinary ecotoxicology research program focuses on the ecotoxicity of metals, fuels, contaminant mixtures and other environmental stressors associated with a changing climate, on Antarctic and sub-Antarctic species.<ref>{{Cite web|url=http://www.antarctica.gov.au/magazine/2011-2015/issue-27-december-2014/science/cleaning-up-fuel-spills-in-antarctica/how-clean-is-clean-enough|title=How clean is clean enough?|last=|first=|date=|website=antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref><ref>{{Cite web|url=http://www.antarctica.gov.au/magazine/2006-2010/issue-17-2009/science/science-dives-into-dirty-issue|title=Science dives into dirty issue|last=|first=|date=|website=antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref> Her goal is to develop environmental risk assessment and remediation guidelines for Antarctic and sub-Antarctic marine and terrestrial environments. She delivers strategically important robust scientific research, which contributes to evidenced-based decision making in policy and operations, both for the Australian Antarctic program, and the Committee for Environmental Protection (CEP).<ref>{{Cite web|url=http://www.ats.aq/e/cep.htm |archive-url=http://webarchive.loc.gov/all/20100505214718/http://www.ats.aq/e/cep.htm |dead-url=yes |archive-date=2010-05-05 |title=Committee for Environmental Protection |last= |first= |date= |website=ats.aq |publisher=Secretariat of the Antarctic Treaty |access-date=2016-06-19 }}</ref><ref>{{Cite web|url=http://www.antarctica.gov.au/magazine/2001-2005/issue-3-autumn-2002/feature/the-committee-for-environmental-protection|title=The Committee for Environmental Protection|last=|first=|date=|website=www.antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref>

King has also acted as the Manager of the Science Planning and Coordination section at the [[Australian Antarctic Division|AAD]], which oversees the administration and governance of all projects within the Australian Antarctic science program. This primarily involves the coordination of project applications, assessments, approvals, planning and reporting, as well as providing research, governance and communications for the Science Branch.<ref>{{Cite web|url=http://www.antarctica.gov.au/science|title=Science|last=|first=|date=|website=www.antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref>

Previous to her role at the Australian Antarctic Division, King was a post-doctoral researcher at the Centre for Environmental Contaminant Research at [[Commonwealth Scientific and Industrial Research Organisation|CSIRO]], where her research in ecotoxicology contributed to the ''Handbook for Sediment Quality Assessment'' for Australia (2005).<ref>{{Cite web|url=https://publications.csiro.au/rpr/download?pid=procite:9b5d8b41-e8e1-4602-b58c-13bd21e96e73&dsid=DS1|title=Handbook for sediment quality assessment|last=|first=|date=2005|website=publications.csiro.au|publisher=CSIRO|access-date=2016-06-19}}</ref> King has been working in Antarctic since her first summer at [[Casey Station]] in 1997 where she researched the impact of leachates from a legacy rubbish tip and wastewater discharge, on benthic communities.<ref>{{Cite web|url=http://www.antarctica.gov.au/magazine/2006-2010/issue-17-2009/technology/marine-animals-downsize-in-sea-change|title=Marine animals downsize in sea change|last=|first=|date=2009|website=www.antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref>

== Awards and honours ==
King has been a Chief Investigator and Co-investigator on over 20 Australian Antarctic Science (AAS) Research Grants.<ref>{{Cite web|url=http://www.antarctica.gov.au/science/information-for-scientists/aas-grants-awarded|title=Australian Antarctic Science Grants awarded|last=|first=|date=2013|website=www.antarctica.gov.au|publisher=Australian Antarctic Division|language=en-au|access-date=2016-06-19}}</ref>

King received the 2006 CSIRO Medal for Research Achievement, for her research advances in assessment and regulation of contaminants in aquatic sediments.<ref>{{Cite web|url=https://csiropedia.csiro.au/csiro-medal-for-research-achievement/#CSIROMedalforResearchAchievement-2006|title=CSIRO Medal for Research Achievement|last=|first=|date=2006|website=|publisher=CSIRO|language=en-US|access-date=2016-06-19}}</ref>

King was part of the CSIRO’s Centre for Environmental Contaminants Research (CECR) team that was awarded the Australian Museum Eureka Prize for Water Research in 2006. This was awarded in recognition of the contribution to research advancing the assessment and regulation of contaminants in aquatic sediments.<ref>{{Cite web|url=http://ww2.setac.org/sapau/newsletter/13_1.pdf|title=Endpoint SETAC newsletter vol 13.1|last=|first=|date=2006|website=setac.org|publisher=Society of Environmental Toxicology and Chemistry|access-date=}}</ref>

King has been selected as Conference Chair for the SETAC-AU 2016 Conference to be held in Hobart.<ref>{{Cite web|url=http://www.setachobart2016.com.au/|title=Welcome|last=|first=|date=|website=setachobart2016.com.au|publisher=SETAC AU|access-date=2016-06-19}}</ref>

== References ==
{{Reflist|35em}}

== External links ==
* [http://www.antarctica.gov.au/science/meet-our-scientists/dr-catherine-king Catherine King's webpage]
* [https://scholar.google.com.au/citations?user=YG9xiecAAAAJ&hl=en&oi=ao Catherine King] on [[Google scholar]]

{{Authority control}}

<!-- Categories for this template, but not the final article -->

<!-- Categories for the final article, but not this template -->

{{DEFAULTSORT:King, Catherine}}
[[Category:Created via preloaddraft]]
[[Category:Australian women scientists]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Antarctic scientists]]
[[Category:Toxicologists]]
[[Category:Australian ecologists]]
[[Category:Women Antarctic scientists]]