{{Infobox journal
| title = Mississippi Quarterly
| italic title = force
| cover = 
| editor = Ted Atkinson
| discipline = [[Southern literature]]
| former_names = Social Science Bulletin
| abbreviation = Miss. q.
| publisher = [[Mississippi State University]]
| country = [[United States]]
| frequency = Quarterly
| history = 1948-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.missq.msstate.edu/
| link1 = 
| link1-name =
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 01758368
| LCCN = 59040435
| CODEN = 
| ISSN = 0026-637X
| eISSN = 
}}
The '''''Mississippi Quarterly: The Journal of Southern Cultures''''' is a [[peer-reviewed]] scholarly journal that mainly covers [[Southern United States|Southern]] [[History of the Southern United States|history]] and [[Southern literature|literature]]. Originally entitled ''Social Sciences Bulletin'', it was established in 1948 by [[John K. Bettersworth]], who was associated with the journal until his death in 1991.<ref name="Phillipps">Phillipps, Robert L., Jr. "Mississippi Quarterly" in Flora, Joseph M, Lucinda H. MacKethan, and Todd Taylor (eds.) (2002), ''[https://books.google.com/books?id=rl5_5u3tiRkC&pg=PA507&lpg=PA507&dq=history+of+the+mississippi+quarterly+1948&source=bl&ots=8L7lpezZOl&sig=D4MusoLgCJ6b4cSvhikX7hS99II&hl=en&ei=2FodTYufCoGKlwev0eXYCw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CBoQ6AEwAA#v=onepage&q=history%20of%20the%20mississippi%20quarterly%201948&f=false The Companion to Southern Literature: Themes, Genres, Places, People, Movements, and Motifs]''
. [[Louisiana State University Press]]. pp. 507-08. ISBN 0-8071-2692-6. Google Books. Retrieved December 30, 2010.</ref> While it began with a very wide focus, initially covering a variety of topics that fell under the umbrella of the social sciences, starting in 1953 the ''Bulletin'' gradually narrowed its academic range.<ref name="Phillipps"/> Changing its title to its current state in that same year, the newly christened ''Quarterly'' soon began to focus almost solely on Southern literature. In 1968 it adopted its current subtitle, further cementing its reputation as a humanities journal.<ref name="Phillipps"/> In that year it also began its cooperation with the [[Society for the Study of Southern Literature]] through which was produced the "Annual Checklist of Scholarship in Southern Literature". Its editors have included [[Robert B. Holland]], [[Scott C. Osborn]], [[Peyton W. Williams, Jr.]],  [[Robert L. Phillipps, Jr.]], and Noel Polk. Its current editor is Ted Atkinson and its Associate and Managing editors are [[Robert M. West]] and [[Laura West]] respectively.<ref>[http://www.missq.msstate.edu/index.php Mississippi Quarterly Home Page]. Retrieved December 30, 2010.</ref>

== Notes ==
{{reflist}}

== External links ==
*[http://www.missq.msstate.edu/index.php Mississippi Quarterly Home Page]

[[Category:American Southern literary magazines]]
[[Category:Publications established in 1948]]
[[Category:Magazines published in Mississippi]]
[[Category:Quarterly journals]]


{{history-journal-stub}}
{{US-lit-mag-stub}}