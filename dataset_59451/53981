{{Use Australian English|date=May 2013}}
{{LDS Temple/Adelaide Australia Temple |format= Infobox LDS Temple }}
{{Use dmy dates|date=March 2011}}
The '''Adelaide Australia Temple''' is the 89th operating [[Temple (LDS Church)|temple]] of [[The Church of Jesus Christ of Latter-day Saints]] (LDS Church).

Plans to build an LDS temple in [[Adelaide, Australia|Adelaide]] were announced on 17 March 1999.<ref>{{citation |url= http://www.ldschurchnewsarchive.com/articles/35432/Six-more-temples-announced-total-now-108.html |title= Six more temples announced; total now 108 |date= 27 March 1999 |newspaper= [[Church News]] }}</ref> Up until this time, LDS members had to travel between fifteen and twenty hours one-way to visit the closest temple in [[Sydney Australia Temple|Sydney]]. The LDS Church has seen rapid growth in [[Australia]] in recent years. In 1955 there were only 3,000 members in Australia, today there are more than 100,000 members. Census statistics show that the LDS Church is the fastest-growing Christian faith in Australia. This rapid growth prompted church leaders to announce new temples across Australia; one in [[Melbourne Australia Temple|Melbourne]], one in [[Brisbane Australia Temple|Brisbane]], one in [[Perth Australia Temple|Perth]], and the one in Adelaide.

A groundbreaking ceremony and site dedication were held on 29 May 1999. [[Vaughn J. Featherstone]], a member of the [[Seventy (LDS Church)|Seventy]], led the ceremony and gave the site dedication prayer. Despite heavy rains, more than 500 people gathered to witness the groundbreaking and site dedication. Many were involved in the groundbreaking including the Mayor of Adelaide, other government officials, and children.<ref>{{citation |url= http://www.ldschurchnewsarchive.com/articles/35873/Rain-clouds-in-Adelaide-do-not-dampen-spirits-during-groundbreaking.html |title= Rain, clouds in Adelaide do not dampen spirits during groundbreaking |first= Phillip |last= Howes |date= 5 June 1999  |newspaper= [[Church News]] }}</ref>

The LDS temple sits on {{convert|6.94|acre|m2}} just a few miles away from the centre of the city of Adelaide. The temple was built of the finest materials including an exterior finish of snow-white granite from Campolonghi, [[Italy]]. The community was very interested in the progress on the temple and numerous stories were printed in the media.

The Adelaide Australia Temple was open to the public from 3–10 June 2000. On the first day of the open house more than 5,000 people visited the temple and it continued to be busy; nearly 50,000 people were able to take a tour through the temple.

The temple was dedicated on 15 June 2000 by LDS Church president [[Gordon B. Hinckley]].<ref>{{citation |url= http://www.ldschurchnewsarchive.com/articles/38044/ADELAIDE-AUSTRALIA-Rejoicing-on-both-sides-of-the-veil.html |title= Adelaide Australia: 'Rejoicing on both sides of the veil' |date= 24 June 2000 |newspaper= Church News }}</ref> Hinckley dedicated four different temples in the same trip—the first time this had occurred in church history. The Adelaide temple was the third temple to be dedicated on this trip.<ref>{{citation |url= http://www.ldschurchnewsarchive.com/articles/38016/Four-temples-dedicated-in-one-overseas-tour.html |title= Four temples dedicated in one overseas tour |first= Richard |last= Hunter |first2= Alan |last2= Wakeley |date= 24 June 2000 |newspaper= Church News }}</ref> Four dedicatory sessions were held, which allowed for more than 2,500 members to be present at the temple's dedication.

The Adelaide Australia Temple has a total of {{convert|10700|sqft|m2}}, two ordinance rooms, and two sealing rooms.<ref>{{citation |url= http://www.ldschurchnewsarchive.com/articles/38011/Facts-and-figures-Adelaide-Australia-Temple.html |title= Facts and figures: Adelaide Australia Temple |date= 24 June 2000 |newspaper= Church News }}</ref>

==See also==
{{Wikipedia books
 |1=Temples of The Church of Jesus Christ of Latter-day Saints
}}
{{Portal|LDS Church|Book of Mormon}}
* [[Comparison of temples of The Church of Jesus Christ of Latter-day Saints]]
* [[List of temples of The Church of Jesus Christ of Latter-day Saints]]
* [[List of temples of The Church of Jesus Christ of Latter-day Saints by geographic region]]
* [[Temple architecture (Latter-day Saints)]]
* [[The Church of Jesus Christ of Latter-day Saints in Australia]]

==Notes==
{{Reflist}}

==References==
* {{citation |url= http://www.ldschurchnewsarchive.com/articles/37321/Temple-dates-announced-postponed.html |title= Temple dates announced, postponed |date= 11 March 2000 |newspaper= Church News }}
* {{citation |url= http://www.ldschurchnewsarchive.com/articles/37582/Temple-dedications-planned.html |title= Temple dedications planned |date= 8 April 2000 |newspaper= [[Church News]] }}
* {{citation |url= http://www.ldschurchnewsarchive.com/articles/38007/Spiritual-sanctuaries-for-faithful-Adelaide-Melbourne-members.html |title= 'Spiritual sanctuaries' for faithful Adelaide, Melbourne members |date= 24 June 2000 |newspaper= Church News }}

==External links==
* [http://www.lds.org/church/temples/adelaide-australia Official Adelaide Australia Temple page]
* [https://web.archive.org/web/20070417193056/http://www.ldschurchtemples.com:80/adelaide/ Adelaide Australia Temple page]

{{List LDS Temple Oceania}}

[[Category:20th-century Latter Day Saint temples]]
[[Category:Places of worship in Adelaide]]
[[Category:Religious buildings completed in 2000]]
[[Category:Temples (LDS Church) in Australia]]
[[Category:2000 establishments in Australia]]