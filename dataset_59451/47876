{{Infobox fashion designer
|name    = Douglas Ferguson
|image   =
|caption = Douglas Ferguson
|nationality= [[United States|American]]
|birth_date= {{Birth date and age|1951|9|7|df=y}}
|birth_place= [[United States|America]]
|death_date= 
|death place= 
|education= 
|label_name=
|awards=
|}}
'''Douglas Ferguson''' (born 1951) is a multidisciplinary artist mainly known for his fashion designs using enamelled metal mesh and hand-painted leather which received widespread exposure in the 1980s. Since starting out in the 1970s as [[Diana Vreeland]]'s voluntary assistant at the [[Metropolitan Museum of Art]]'s [[Costume Institute]], he has also worked as an interior designer and film-maker.

==Early life and education==
Douglas Richard Ferguson was born in 1951.<ref name=goddess>{{cite book|last=Koda|first=Harold|title=Goddess : the classical mode |date=2003|publisher=Metropolitan Museum of Art|location=New York, NY |isbn=9780300098822 |page=190 |url=https://books.google.com/books?id=haAAxvwEmMMC&pg=PA190#v=onepage&q&f=false}}</ref> He studied fine art in Rome,<ref name=ny>{{cite news|last=La Ferla|first=Ruth|title=Vintage Alchemy|url=https://www.nytimes.com/2012/03/15/fashion/douglas-fergusons-chain-mail-fashions-on-display-at-frock.html?_r=2&adxnnl=1&ref=style&adxnnlx=1332221101-rHy%2FZlk1BbuVGJnEXQ6DOg& |accessdate=21 March 2013 |newspaper=The New York Times|date=March 14, 2012}}</ref> and in Philadelphia, but dropped out after three years, saying that he wanted to 'try everything' rather than focus on painting as his tutors wished.<ref name=stormy>{{cite news|last=Wintour|first=Anna|title=Stormy Leather|url=https://books.google.com/books?id=JtgBAAAAMBAJ&pg=PA70#v=onepage&q&f=false|accessdate=6 May 2014|newspaper=New York Magazine|date=21 February 1983}}</ref>

Among other jobs, he worked as a model, a stitcher-in of [[label]]s, and a [[taxicab driver]], but most notably worked as a personal assistant to [[Diana Vreeland]] at the [[Costume Institute]].<ref name=stormy/> As this was a volunteer role, he subsidized it by driving a [[Checker Taxi]] at night.<ref name=ny/> He went on to work as [[Diana Vreeland]]'s exhibition assistant for the 1979-81 Costume Institute shows ''Fashions of the Hapsburg Era'' and ''The Manchu Dragon''.<ref>{{cite book|last=Blum|first=Stella|title=Fashions of the Hapsburg Era: Austria-Hungary |date=1979 |publisher=Metropolitan Museum of Art |location=New York |page=27 |url=https://books.google.com/books?id=ovQm_TWOIzgC&pg=PA27&lpg=PA27#v=onepage&q&f=false}}</ref><ref name=fqb>{{cite web|title=Full Quote Bergman|url=http://www.genderbender.it/eng/gb05/dettaglio.asp?id=14|publisher=Gender Bender|accessdate=6 May 2014|date=2005}}</ref>

==Career==
Ferguson launched his career at the turn of the 1970s/80s with hand-painted leather goods and furniture.<ref name=ny/><ref name=stormy/> On 20 August 1980, ''[[Harper's Bazaar]]'' featured a photograph by [[Francesco Scavullo]] of the model [[Kelly Le Brock]] wrapped in Ferguson's painted leathers.<ref>{{cite journal|journal=Harper's Bazaar|date=20 August 1980}}</ref> One of Ferguson's first successes was a decorative wooden [[flapper]] doll for the [[Mudd Club]], which attracted the attention of the designer [[Patricia Field]] and led to her commissioning his hand-painted leather and [[chiffon (fabric)|chiffon]] dresses for her boutique.<ref name=ny/> He also made painted leather garments, including [[shawl]]s and [[kilt]]s for [[Maud Frizon]], Carol Rollo at 'Riding High', and Johnny Ward.<ref name=stormy/> His designs were also stocked by [[Robert Lee Morris]] in his ''Artwear'' shop, a showcase for artisan jewelry and accessories.<ref name=ny/> In 1981 Ferguson was highlighted by ''[[The New York Times]]'' as one of a new wave of up-and-coming British and American designers alongside [[Vivienne Westwood|'Vivien Westwood']]''[sic]'', [[Anna Sui]], and [[Mary Jane Marcasiano]].<ref name=duka>{{cite news|last=Duka|first=John|title=Fashion: The New Underground|url=https://www.nytimes.com/1981/04/26/magazine/fashion-the-new-underground.html|accessdate=6 May 2014|newspaper=The New York Times|date=26 April 1981}}</ref> Although his hand-painted suede and chiffon work was described as initially seeming 'more like rags than fashion,' once it was on a figure, it came together to create a look that 'seemed at once ancient and modern'.<ref name=duka/> 

Soon afterwards, Ferguson developed a technique of painting [[chain mail]] with [[automotive paint]], which enabled him to apply medieval, Chinese or [[Art Deco]] motifs to the metal.<ref name=ny/> The mesh he used had originally been developed by [[Whiting & Davis]] in the 1920s as a material for [[Handbag|evening bags and purses]], but his techniques were unique to himself.<ref name=goddess/> These mesh designs, which he made up into dresses, shawls, and other garments, proved popular and were featured on a number of magazine covers, including two covers for ''[[Cosmopolitan (magazine)|Cosmpolitan]]'' in 1984 and 1985,<ref>{{cite journal|journal=Cosmopolitan|date=August 1984}}</ref><ref>{{cite journal|journal=Cosmopolitan|date=November 1985}}</ref> a 1984 cover for [[New York Magazine]],<ref>{{cite journal|journal=New York Magazine|date=27 August 1984}}</ref> and a ''[[Vogue (magazine)|Vogue]]'' supplement showing a mesh dress emblazoned with the [[Statue of Liberty]].<ref name=ny/> [[Tina Turner]] also wore a Ferguson mesh dress for a ''[[Vanity Fair (magazine)|Vanity Fair]]'' cover.<ref name=ny/>

He designed the costumes for the 1983 premiere of [[William Forsythe (dancer)|William Forsythe]]'s  production of ''Square Deal'' ([[Joffrey Ballet]]).<ref>{{cite news|last=Kisselgoff|first=Anna|title=Ballet: Joffrey Premiere|url=https://www.nytimes.com/1983/11/04/arts/ballet-joffrey-premiere.html|accessdate=6 May 2014|newspaper=The New York Times|date=4 November 1983}}</ref>

In 2012 Ferguson was designing for private clients.<ref name=ny/>

==Films==
Ferguson has also worked in film-making on a small scale. His films include:

*''The Box'', a [[Super 8 film|Super 8]] college of [[parrot]]s and dolls.<ref name=fqb/>
*''Bogus Yoga'', a video installation made whilst in Switzerland as an [[artist-in-residence]].<ref name=fqb/>
*''Free Fall'' (1994), produced with Gea Kalthegener, a feature-length documentary on the actor [[Ron Vawter]], who died during filming. The [[New York Film Festival]] and the [[Filmfest München]] both included it in their official selections for 1994.<ref name=fqb/> Jill Godmilow, who was also making a film about Vawter's work at the time (''Roy Cohn/Jack Smith'', 1994) summed up ''Free Fall'' as a record of Vawter's last days, saying 'If you want to ''die'' with Ron... you can see ''that'' film.'<ref>{{cite book|last=MacDonald|first=Scott|title=A Critical Cinema: Interviews with Independent Filmmakers, Volume 4|date=2005|publisher=Univ. of California Press|location=Berkeley [u.a.]|isbn=9780520242715|page=149|url=https://books.google.com/books?id=XFCJrLHhxWcC&pg=PA149&lpg=PA149#v=onepage&q&f=false|chapter=Jill Godmilow (and Haroun Farocki)}}</ref>
*''Full Quote Bergman'' (2003), a collaboration with Brock Labrenz and Richard Siegal, commissioned and produced by [[William Forsythe (choreographer)|William Forsythe]]'s Ballet Frankfurt, and premiered in Frankfurt in January 2004.<ref name=fqb/> Described as an 'inquiry into 21st century cannibalism,' Ferguson described it as a 'literary thesis in the form of a film that happens to be a ballet,' citing [[Ingmar Bergman]]'s  ''[[Persona (film)|Persona]]'' and [[Susan Sontag]]'s article on that film as influences.<ref name=fqb/> It was also screened at the Italian ''Gender Bender'' film festival.<ref name=fqb/>

==Exhibitions==
* 1987: ''East Village: The New Vanguard'', [[Fashion Institute of Technology]], New York. Hand-painted leather cape and kilt.<ref>{{cite news|last=Feldkamp|first=Phyllis|title=East Village crackles with creativity. |url=http://www.csmonitor.com/1986/0314/heast.html/%28page%29/2 |accessdate=7 May 2014|newspaper=The Christian Science Monitor|date=14 March 1986}}</ref>
* 1989: ''The Historical Mode: Fashion and Art in the 1980s'', Fashion Institute of Technology, New York. Mesh evening dresses.<ref>{{cite book|last=Cunningham|first=Patricia A.|title=Fashioning the Future: Our Future from our Past|date=1996|publisher=Ohio State University, Department of Consumer and Textile Sciences|location=Ohio State University|page=10|url=https://kb.osu.edu/dspace/bitstream/handle/1811/44672/Fashioning_the_future_sm.pdf?sequence=2|chapter=Classical Revivals in Dress}}</ref>
* 2003: ''Goddess: The Classical Mode'', [[Metropolitan Museum of Art]]. Three of Ferguson's 1985 enamelled mesh outfits.<ref name=goddess/><ref name=ny/>
* 2004: ''Goddess'', [[ModeMuseum Provincie Antwerpen|MoMu]], Antwerp, Belgium. Travelling version of above exhibition.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.fergusonandcie.com/|www.fergusonandcie.com:- Ferguson's official website}}

[[Category:American fashion designers]]
{{DEFAULTSORT:Ferguson, Douglas Richard}}
[[Category:1951 births]]
[[Category:Living people]]
[[Category:American filmmakers]]