{{Use Australian English|date=August 2015}}
{{Infobox recurring event
| name          = Adelaide Festival of Arts
| logo          = Adelaide_Festival_of_Arts_2017_Corporate_Logo.jpg
| image         = 
| image_size    = 
| caption       = 
| date          = 
| begins        = 
| ends          = 
| frequency     = Annual
| location      = [[Adelaide]], [[South Australia]]
| years_active  = 
| first         = {{start date|df=yes|1960|03}}
| last          = 
| prev          = 
| next          = 
| participants  = 
| attendance    = 
| organised     = 
| website       = {{URL|http://www.adelaidefestival.com.au}}
| footnotes     = 
}}
{{Coord|-34.911625|138.635101|display=title}}

The '''Adelaide Festival of Arts''', also known as the '''Adelaide Festival''', is an [[arts festival]] held annually in the [[South Australia]]n capital of [[Adelaide]]. It is considered to be one of the world's major celebrations of the arts, and a pre-eminent cultural event in Australia.<ref>{{cite web |last=Starick |first=Paul |accessdate=13 June 2006 |url=http://www.theadvertiser.news.com.au/common/story_page/0,5936,18505821%255E30877,00.html |title=Festival back as best in nation |date=18 March 2006 |work=[[The Advertiser (Australia)|The Advertiser]]}}</ref>

Begun in 1960, the Adelaide Festival is held in the autumnal month of March. It is actually made up of several events, but overall features include opera, theatre, dance, classical and contemporary music, [[cabaret]], literature, visual art and [[new media]].

The festival is based in the city centre, principally in venues along the cultural boulevard of [[North Terrace, Adelaide|North Terrace]], but also elsewhere in the city and its [[Adelaide Park Lands|parklands]]. The [[Adelaide Festival Centre]] and [[River Torrens]] usually form the nucleus of the event, and in recent years [[Elder Park, Adelaide|Elder Park]] has played host to opening ceremonies. The popularity of the event is sometimes attributed to the city's unique design (known as [[Light's Vision]]) that locates many pleasant settings within short distance of each other.

Originally presented biennially, the festival has been held annually since 2012.<ref>{{cite web |url=http://www.adelaidenow.com.au/entertainment/festivals/adelaide-festival-of-arts-to-go-annual-sa-premier-mike-rann-promises/story-fn489cz4-1225834689784 |title=Adelaide Festival of Arts to go annual |date=26 February 2010 |work=[[The Advertiser (Australia)|The Advertiser]]}}</ref>

==History==
The Adelaide Festival began with efforts by [[Lloyd Dumas|Sir Lloyd Dumas]] in the late 1950s to establish a major arts festival that would bring to [[South Australia]] world-class cultural exhibitions. In 1958, Sir Lloyd organised a gathering of prominent members of the Adelaide business, arts and government community. The proposal for an event similar to the [[Edinburgh International Festival]] was supported and the first Festival Board of Governors was formed.

The event began to take form when Sir Lloyd partnered with [[John Bishop (academic)|John Bishop]], Professor of Music at the [[University of Adelaide]]. The two gained the support of the Lord-Mayor and [[City of Adelaide|Adelaide City Council]] and a financial backing of 15,000 pounds.<ref name="How it started">{{cite web|accessdate=13 June 2006 |year=2006 |url=http://festival.stg.jaba.com.au/history/started.asp |title=How it started |work=History of the Festival |publisher=Adelaide Festival of Arts |deadurl=yes |archiveurl=https://web.archive.org/web/20071009193003/http://festival.stg.jaba.com.au/history/started.asp |archivedate=9 October 2007 |df=dmy }}</ref> A number of leading businesses sponsored the first festival including ''[[The Advertiser (Australia)|The Advertiser]]'', the [[Bank of Adelaide]], [[John Martin & Co.]], the [[Adelaide Steamship Company]], and [[Kelvinator]].<ref name="How it started" />

The inaugural Adelaide Festival of Arts ran from 12–26 March 1960 and was directed by Professor Bishop with some assistance from [[Ian Hunter (impresario)|Ian Hunter]], the Artistic Director of the Edinburgh Festival. There were 105 shows covering almost all aspects of the arts.

The Adelaide Festival continued to grow in successive years with the support of the [[Government of South Australia|South Australian Government]]. It has developed a number of incorporated events including [[Adelaide Writers' Week]], Australia's original literary festival; [[WOMADelaide]], an iconic world music festival; Artists' Week; the Adelaide International, a specially curated international contemporary visual arts program; and, the [[Adelaide Festival of Ideas]], an internationally renowned talkfest. It also spawned the [[Adelaide Fringe Festival]] which has become the largest event of its kind in the world after the [[Edinburgh Fringe]]. The Adelaide Festival is seen as a template for other arts festivals, and it has been replicated to some extent by other Australian cities.{{citation needed|date=December 2012}}

In February 2010, to celebrate its 50th anniversary, South Australian Premier Mike Rann announced that the Adelaide Festival would be held annually from 2012, with total government funding to be more than doubled.<ref>Adelaide Advertiser, 26 Feb 2010</ref>

==Past Festivals==
[[File:Robert Helpmann.jpg|200px|thumb|right|[[Robert Helpmann|Sir Robert Helpmann]] directed the Festival of 1970, in which he premiered his world famous ballet ''[[Don Quixote]]''.]]
The Adelaide Festival has had 19 Artistic Directors in its history, two of whom resigned and four of whom have directed the festival more than once. [[Anthony Steel (Arts Leader)|Anthony Steel]] holds the record for most stints as director, being at the head of 5 festivals. There were no directors for the festivals of 1966 and 1968, with an advisory board taking on the responsibility. [[Peter Sellars]]' brief directorship of the [[2002 Adelaide Festival]] remains the most controversial and he was eventually replaced by [[Sue Nattrass]].

{| class="wikitable"
|-
! Year
! Director
! Featured Artists / Performances
|-
| 1960
| [[John Bishop (academic)|John Bishop]]
| A visit by the festival’s patron [[Queen Elizabeth the Queen Mother]]; the inaugural Adelaide Writers’ Week; [[T. S. Eliot]]’s ''Murder in the Cathedral'', [[Dave Brubeck]]’s jazz quartet; the Sydney and Victorian symphony orchestras; [[Donald Wolfit|Sir Donald Wolfit]]; [[Edwin Hodgeman]]; [[Ruth Cracknell]], and poet  [[Max Harris (poet)|Max Harris]].
|-
| 1962
| [[John Bishop (academic)|John Bishop]]
| [[Yehudi Menuhin|Yehudi]] and [[Hephzibah Menuhin]]; the [[London Philharmonic Orchestra]] conducted by [[Sir Malcolm Sargent]]; the Bhaskar Dance Company from India; [[Benjamin Britten]]’s ''[[Noye's Fludde]]'', [[Zoe Caldwell]] in [[George Bernard Shaw|Shaw]]’s [[Saint Joan (play)|''St Joan'']]; the [[Dave Brubeck Quartet]] and [[David Attenborough]].
|-
| 1964
| [[John Bishop (academic)|John Bishop]]
| ''[[Henry V (play)|Henry V]]'' (staged in a tent in the parklands); [[Marie Collier]] and [[Richard Lewis (tenor)|Richard Lewis]] in [[Sir William Walton]]’s opera ''[[Troilus and Cressida]]''; the world premiere of [[Sir Robert Helpmann]]’s ballet ''[[The Display]]'' performed by [[Australian Ballet Company|The Australian Ballet Company]] and designed by [[Sidney Nolan]], and The Black Theatre of Prague.
|-
| 1966
| Advisory Board
| [[The Queen Mother]] visited during the second week attending various events including the Flower Day celebrations and a variety show at Elder Park; ''[[The Royal Hunt of the Sun]]''; [[Dame Judith Anderson]] performing four classical drama recitals; the ''[[Berlioz Requiem]]''; the [[London Symphony Orchestra]]; Die Kammermusiker of Switzerland, and [[Yevgeny Yevtushenko]] at Adelaide Writers’ Week.
|-
| 1968
| Advisory Board
| Opera singers [[Tito Gobbi]] and [[Marie Collier]]; [[Acker Bilk]] and his [[Paramount Jazz Band]]; [[Mahler]]’s monumental ''[[Mahler 8|Eighth Symphony]]'', and the [[Salzburg Marionette Theatre]]; [[United States of America|American]] [[poet]] [[James Dickey]].
|-
| 1970
| [[Robert Helpmann|Sir Robert Helpmann]]
| [[Benjamin Britten]] conducting the [[Adelaide Symphony Orchestra|South Australian Symphony Orchestra]]; [[The Australian Ballet]] choreographed by [[Sir Robert Helpmann]] CBE and featuring [[Rudolf Nureyev]]; [[Royal Shakespeare Company|Royal Shakespeare]]’s productions ''[[The Winter's Tale|Winter’s Tale]]'' and ''[[Twelfth Night]]'' featuring [[Donald Sinden]] and [[Judi Dench]]; the [[Warsaw Philharmonic Orchestra]]; the Bartók String Quartet (Hungary), [[Larry Adler]] and [[Rolf Harris]]. [[Ivor Hele]]'s second and last solo exhibition.
|-
| 1972
| [[Louis van Eyssen]]
| [[Leo McKern]]; [[Timothy West]]; [[Timothy Dalton]] and [[Frank Thring]]; ''[[Jesus Christ Superstar]]'';  [[State Theatre Company of South Australia|The South Australian Theatre Company]]’s production of ''[[The Alchemist (play)|The Alchemist]]'', and American poets [[Lawrence Ferlinghetti]] and [[Allen Ginsberg]] at Adelaide Writers' Week.
|-
| 1974
| [[Anthony Steel (Arts Leader)|Anthony Steel]]
| Recitals by [[André Tchaikowsky]]; baritone [[Hans Hotter]]; Brazialian guitarists, the ''Abreu Brothers''; the [[Hungarian State Symphony Orchestra]]; [[Jacques Loussier Trio]]: Stratford National Theatre of Canada, and Premier [[Don Dunstan]]’s reading of [[Ogden Nash]]’s verses to [[Saint-Saëns]]’ ''[[Carnival of the Animals]]''.
|-
| 1976
| [[Anthony Steel (Arts Leader)|Anthony Steel]]
| [[John Cage]], [[Leo Sayer]], [[Herbie Mann]], [[Sonny Terry]] and [[Brownie McGhee]]. Adelaide Writers' Week hosted [[Kurt Vonnegut]], [[Susan Sontag]], [[James Baldwin (editor and author)|James Baldwin]] and [[Wole Soyinka]].
|-
| 1978
| [[Anthony Steel (Arts Leader)|Anthony Steel]]
| [[Sir Michael Tippett]]’s first opera ''[[The Midsummer Marriage]]''; the [[Israel Philharmonic Orchestra]] under [[Zubin Mehta]]; [[Roger Woodward]]’s series of [[Beethoven]]’s 32 piano sonatas performed in 12 days; Compagnie Philippe Genty; [[Kabuki Theatre]], and ''[[East (play)|East]]'' by [[Steven Berkoff]].  Prominent authors at Writers’ Week included [[Manning Clark]], [[Barry Humphries]] and [[Frank Moorhouse]], American writer [[Chaim Potok]] and Irish poet [[Richard Murphy (poet)|Richard Murphy]].
|-
| 1980
| [[Christopher Hunt]]
| [[Spike Milligan]]; ''Futuresight'', an exhibition from the New York Museum of Holography; Peter Brook’s ''Centre for International Theatre Creations''; The Acting Company of New York; La Claca Theatre Company of Catalonia; the Ballet of the [[Komische Oper Berlin|Komische Oper]] (Berlin); the Prague Chamber Ballet and the [[Warsaw National Philharmonic Orchestra]].
|-
| 1982
| [[Jim Sharman]]
| Opening night parade including more than 10,000 participants, followed by a concert in [[Elder Park]] with [[Nuova Compagnia di Canto Popolare]] and the [[Grimethorpe Colliery Band]] on a floating stage; [[Pina Bausch]]’s [[Tanztheater Wuppertal]], and the premiere of [[Patrick White]]’s play ''Signal Driver''.
|-
| 1984
| [[Elijah Moshinsky]] (resigned) /[[Anthony Steel (Arts Leader)|Anthony Steel]]
| [[Vladimir Ashkenazy]] leading [[Philharmonia Orchestra|London’s Philharmonia Orchestra]] performing [[Beethoven]]’s nine symphonies and five piano concertos presented as a complete cycle; Theatre Tenkei Genijyo; the [[The Pointer Sisters|Pointer Sisters]], and the [[Polish Chamber Orchestra Camerata-Wroclaw|Polish Chamber Orchestra]].
|-
| 1986
| [[Anthony Steel (Arts Leader)|Anthony Steel]]
| A high-wire walk from the [[Adelaide Festival Theatre|Festival Theatre]] roof across the River Torrens; the world premiere of [[Richard Meale]]’s opera ''[[Voss]]''; [[Philip Glass]]; [[Nederlands Dans Theater|The Nederlands Dance Theater]]; [[Jan Fabre]]; [[The Wooster Group]] and [[Gidon Kremer]].
|-
| 1988
| [[George Lascelles, 7th Earl of Harewood]]
| [[Peter Brook]]’s all-night production of ''[[The Mahabharata]]'' at The Quarry; [[Chicago Symphony Orchestra]]; Twyla Tharp Dance and [[Sarah Vaughan]].
|-
| 1990
| [[Clifford Hocking]]
| A landmark Australian production of ''[[Tristan and Isolde]]''; the [[Vienna Singverein]]; [[Peter Schreier]] and [[Mercedes Sosa]]; [[Lyon Opera Ballet]]’s ''[[Cinderella]]'' and French Circus Archaos.
|-
| 1992
| [[Rob Brookman]]
| The inaugural [[WOMADelaide]]; ''[[Nixon in China]]''; [[Peter Schreier]] and the [[Adelaide Symphony Orchestra]]’s performance of Bach’s ''[[St Matthew Passion]]''; Maguy Marin’s May B and [[Cheek by Jowl]]’s ''[[As You Like It]]''.
|-
| 1994
| [[Christopher Hunt]]
| [[Mark Morris Dance Group]]; the Frankfurt Ballet;  [[Penny Arcade]]; William Yang; [[The Four Horsemen of the Apocalypse (film)|The Four Horsemen of the Apocalypse]]; Hakutobo and the Wuhan Acrobats.
|-
| 1996
| [[Barrie Kosky]]
| Late-night club ''Red Square'', built from 120 shipping containers; the [[Batsheva Dance Company]] of Israel; [[Maly Theatre (St.Petersburg)|Maly Theatre]] of St Petersburg; theatre performance [[La Fura dels Baus]] (Barcelona); Hotel Pro Forma (Denmark), [[DV8 Physical Theatre]]; Latin jazz musician [[Tito Puente]] and [[Annie Sprinkle]].
|-
| 1998
| [[Robyn Archer]]
| Opening night spectacular ''Flamma Flamma''; stage adaptation of [[TS Eliot]]’s ''[[The Waste Land]]''; the Australian-Japanese production of ''Masterkey''; Ex-Machina’s seven-hour performance of ''The Seven Streams of the River'' ''Ota'' by Robert Le Page; the Andalusian opera ''[[Carmen]]'', and Sequentia’s performances of the ''Canticles of Ecstasy'', composed by [[Hildegard von Bingen]].
|-
| 2000
| [[Robyn Archer]]
| [[Peter Greenaway]]’s opera ''Writing to Vermeer''; Ishina presenting ''Mizumachi (The Water City)'' in an open-air theatre; Les Ballets C de la B's tribute to Bach in his 250th year with ''Iets Op Bach'', and 5 new Australian works commissioned for the festival.
|-
| 2002
| [[Peter Sellars]] (resigned)/[[Sue Nattrass]]
| ''Kaurna Palti Meyunna'' in [[Victoria Square, Adelaide|Victoria Square]]; a film program with titles commissioned including ''[[The Tracker]]'', ''[[Australian Rules (film)|Australian Rules]]'' and ''[[Walking on Water (film)|Walking on Water]]'', [[Uppalapu Srinivas]] (India), Black Swan, and many community events.
|-
| 2004
| [[Stephen Page]]
| An indigenous Awakening Ceremony; [[David Gulpilil]]; [[Bangarra Dance Theatre]]; [[Windmill Performing Arts|Windmill]]’s ''RiverlanD'' and ''Body Dreaming''; [[Bryn Terfel]]; the Prague Chamber Orchestra; large-scale theatrical event ''The Overcoat'', La Carnicería Teatro (Madrid) with'' I Bought a Spade at Ikea to Dig My Own Grave''; [[Circus Oz]], and late night club ''Universal Playground''.
|-
| 2006
| [[Brett Sheehy]]
| Italian company Compagnia di Valerio Festi's Il Cielo che Danza (The Dancing Sky); Berlin’s [[Schaubühne]]'s Nora; the world premiere of a theatrical music event inspired by the phenomenon of [[Imelda Marcos]], ''[[Here Lies Love]]''; the opera [[Flight (opera)|''Flight'']]; highlights of the [[Venice Biennale]], and the open air festival club Persian Garden.
|-
| 2008
| [[Brett Sheehy]]
| The opera ''[[Ainadamar]]'', ''Northern Lights'', [[Akram Khan (dancer)|Akram Khan]]’s ''Sacred Monsters'' with [[Sylvie Guillem]], [[Leonard Cohen]] and [[Philip Glass]]’ Buddhist-inspired ''Book of Longing'', the Indian/Sri Lankan ''[[A Midsummer Night’s Dream]]''; the [[Mahavishnu Orchestra]]’s ''Meeting of the Spirits'' and [[John Adams (composer)|John Adams]]’ Buddhist/Kerouac-inspired ''Dharma at Big Sur''; an examination of the great living of composers including [[Arvo Pärt|Pärt]], [[Kats-Chernin]], [[Górecki]], Farr, [[John Adams (composer)|Adams]], [[Peter Sculthorpe|Sculthorpe]], Hosokawa, [[Philip Glass|Glass]], Adès, Golijov, Hindson, [[Nigel Westlake|Westlake]], MacMillan, [[Richard Meale|Meale]] and Roumain.
|-
| 2010
| [[Paul Grabowsky]]<ref>{{cite web |url=http://www.adelaidefestival.com.au/Blog/archive/2008/01/04.aspx |title=Paul Grabowsky announced as new Artistic Director! |accessdate=1 March 2008 |date=4 January 2008 |publisher=Adelaide Festival Corporation}} {{Dead link|date=September 2010|bot=H3llBot}}</ref>
| The opera production of ''Le Grand Macabre'' by [[György Ligeti]], the [[Wayne Shorter]] Quartet; ''Good Morning Mr Gershwin'' (Montalvo-Hervieu);  ''[[The Sound and the Fury]]'' ([[Elevator Repair Service]]); and ''[[The Walworth Farce]]'' (Druid); an expansion of ''Northern Lights''; Groupe F’s pyrotechnic spectacle ''A Little More Light'', and ''[[Mahler 8]]'' featuring the [[Adelaide Symphony Orchestra]] and the [[Tasmanian Symphony Orchestra]].
|-
| 2012
| [[Paul Grabowsky]]
| ''Raoul'' starring [[James Thiérrée]]; [[Harold Pinter]]’s ''[[The Caretaker]]'' starring [[Jonathan Pryce]];  [[Leonard Bernstein]]’s ''[[Mass (Leonard Bernstein)|Mass]]'' featuring [[Jubilant Sykes]]; Force Majeure and [[Sydney Theatre Company]]’s production ''Never Did Me Any Harm''; [[Charles Bradley (singer)|Charles Bradley]]; [[Roky Erickson]]; Ariel Pink’s Haunted Graffiti; [[Michael Rother]] and [[Jane Birkin]].
|-
| 2013
| [[David Sefton]]
| [[Kronos Quartet]] and [[Laurie Anderson]]; [[Van Dyke Parks]]; [[Sylvie Guillem]]; [[National Theatre of Scotland]]'s ''The Strange Undoing of Prudencia Hart''; Ontroerend Goed's ''Trilogy''; [[Neil Finn]] and [[Paul Kelly (Australian musician)|Paul Kelly]]; Unsound Adelaide; [[Severed Heads]]; [[Nick Cave and the Bad Seeds]] and the return of the festival club, Barrio.
|-
| 2014
| [[David Sefton]]
| [[Toneelgroep Amsterdam]]'s ''Roman Tragedies''; [[John Zorn]] in an exclusive concert series comprising the ''Masada Marathon'', ''Classical Marathon'', ''Triple Bill'' and ''Zorn@60'' with guest artists including [[Mike Patton]], [[Joey Baron]], [[Bill Laswell]], [[Marc Ribot]], [[Sofia Rei]], [[Dave Lombardo]], [[Greg Cohen]], [[Jesse Harris]] and more; [[Batsheva Dance Company]]'s ''Sadeh21''; [[Isabella Rossellini]]'s ''Green Porno''; [[John Waters]]; [[Robert Lepage]]'s ''Needles and Opium''; ''An Iliad'' starring [[Denis O'Hare]], Kid Creole and the Coconuts, Charles Bradley; Unsound Adelaide; [[Ilan Volkov]] and the festival club, Lola's Pergola.
|-
| 2015
| [[David Sefton]]
| Spectacular digital illuminations marked the opening with ''Blinc''. This festival also included [[Danny Elfman]]’s Music from the Films of Tim Burton, unique festival shows like Tommy and the return of Unsound. New York’s [[Cedar Lake Contemporary Ballet]] made their Australian debut performing works by [[Crystal Pite]], [[Hofesh Shechter]], [[Jiří Kylián]] and [[Sidi Larbi Cherkaoui]].
|-
| 2016
| [[David Sefton]]
| French maestros of light and fire Groupe F open the festival with ''À Fleur de Peau'' at the Adelaide Oval. The festival also included The James Plays by [[Rona Munro]], [[Tanztheater Wuppertal Pina Bausch]]'s ''Nelken'', [[Romeo Castellucci]]’s ''Go Down, Moses'', Canadian dance company [[Holy Body Tattoo|The Holy Body Tattoo]]’s monumental accompanied live by Godspeed You! Black Emperor (CAN), and the award-winning 1927’s multi-disciplinary dystopian fable Golem (UK).
|-
| 2017–2019
| [[Neil Armfield]] and [[Rachel Healy]]
| 2017 festival program to be announced October 2016
|}

==References==
{{reflist}}

==External links==
* [http://www.adelaidefestival.com.au/ Adelaide Festival Official Website]
* [http://www.adelaidefestival.com.au/about/past_festivals/ Past Adelaide Festival programs]
* [http://www.womadelaide.com.au/ WOMADelaide]
* [http://www.afct.org.au/ Adelaide Festival Theatre]
* [http://www.adelaidefestivalofideas.com.au/ Adelaide Festival of Ideas]

{{Adelaide Festivals}}

{{Use dmy dates|date=June 2013}}

[[Category:Arts festivals in Australia]]
[[Category:Festivals in Adelaide]]
[[Category:Recurring events established in 1960]]
[[Category:1960 establishments in Australia]]
[[Category:Annual events in Australia]]