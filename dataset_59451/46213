[[File:NeisseriaGonorrhoeae.jpg|thumb|Gram stain of ''Neisseria gonorrhoeae'' showing characteristic diplococci morphology]]
The Gonorrhea bacterium [[Neisseria gonorrhoeae]] has developed antibiotic resistance to many antibiotics.

== Penicillins ==

[[File:Penicillin cures gonorrhea.jpg|thumb| Old advertisement for penicillin treatment]]

Beta-lactams like penicillin were widely used to treat gonorrhea in the 1940s.  There are three general mechanisms that may allow bacteria to become resistant to beta-lactam antibiotics:
# inability to access/target penicillin-binding protein (PBP) enzyme
# inhibition of binding to PBP via modification of the enzyme
# hydrolysis/inactivation of the antibiotic by beta-lactamases.<ref name="Murray">Murray, Patrick R., Ken S. Rosenthal, and Michael A. Pfaller. Medical Microbiology. 6th ed. Philadelphia: Mosby/Elsevier, 2009. Print</ref>
Overuse of penicillin contributed to ''Neisseria gonorrhoeae'' developing high resistance to penicillin through two main mechanisms: chromosomally mediated resistance (CMRNG) and penicillinase-mediated resistance (PPNG).<ref name="Tapsall">Tapsall (2001) Antimicrobial resistance in ''Niesseria gonorrhoeae''. World Health Organization.</ref>

Chromosomally mediated resistance occurred through step-wise changes over many years.  Chromosomal mutations in the ''penA'', ''mtr'', and ''penB'' genes are the major mechanisms for CMRNG.  The ''penA'' gene encodes an alternative penicillin-binding protein, PBP-2.<ref name="Tapsall" />  This mechanism falls under the second general mechanism for beta-lactam resistance.  PBPs, also known as transpeptidases, are targets for beta-lactams.  These enzymes (PBPs) are involved in peptidoglycan synthesis which is a major component of the bacterial cell wall.  PBPs cross-link the amino acid strands of peptidoglycan during synthesis.  Normally, beta-lactams bind the PBPs and thereby inhibit the cross-linking of peptidoglycan.  When this occurs, the cell wall of the bacterium is compromised and often results in cell death.<ref name="Murray" />  When ''N. gonorrhoeae'' encodes ''penA'', the new PBP-2 that is synthesized is no longer recognized by the beta-lactams rendering the bacterium resistant.

The ''mtr'' (multiple transferable resistance) gene encodes for an efflux pump.<ref name="Rouquette-Loughlin">Rouquette-Loughlin, Dunham, Kuhn, Balthazar, Shafer (2003) The NorM Efflux Pump of ''Neisseria gonorrhoeae'' and ''Neissera meningitidis'' Recognizes Antimicrobial Cationic Compounds. Journal of Bacteriology 185:1101–1106
</ref>  Efflux pumps mediate resistance to a variety of compounds including antibiotics, detergents, and dyes.<ref name="Tapsall" />  This mechanism falls under the first general resistance mechanism to beta-lactams.  ''mtr'' encodes for the protein MtrD which is the efflux pump for ''N. gonorrhoeae''.<ref name="Tapsall" />  MtrD is among the Resistance Nodulation Division (RND) efflux pump superfamily.  These pumps are proton antiporters where the antibiotic is pumped out of the cell while a proton is pumped into the cell.<ref name="Van Bambeke">Van Bambeke, Balzi, Tulkens (2000) Antibiotic Efflux Pumps. Biochemical Pharmacology 60:457–470</ref>

The cell wall of ''N. gonorrhoeae'' contains porins which are holes within the cell wall in which some molecules are able to diffuse into or out of the cell membrane.  This mechanism falls under the first general mechanism for beta-lactam resistance.  The ''penB'' gene encodes the porins for ''N. gonorrhoeae'' and when this gene undergoes mutations, there is a decrease in permeability of the cell wall to hydrophilic antibiotics like penicillin.<ref name="Tapsall" />

Penicillinase-mediated resistance in ''N. gonorrhoeae'' is mediated by the plasmid borne TEM-1 type beta-lactamase which falls under the third general mechanism for beta-lactam resistance.<ref name="Tapsall" />  There have been over 200 beta-lactamases described and some of them are antibiotic specific.<ref name="Murray" />  TEM-1 is a penicillinase specific for penicillins.  This enzyme will bind to the beta-lactam ring which is a structural characteristic for beta-lactams and hydrolyze the ring.  This renders the antibiotic inactive.  The spread of the penicillinase resistance was much faster compared to the chromosomal-mediated resistance mechanisms.  The plasmids containing TEM-1 could be passed from bacterium to bacterium via conjugation <ref name="Tapsall" />

== Quinolones ==

Quinolones are a class of synthetic antibiotics that inhibit DNA replication, recombination, and repair by interacting with the bacterial DNA gyrase and/or topoisomerase IV.<ref name="Murray" />  Second generation quinolones like ciprofloxacin and ofloxacin have been widely used to treat ''N. gonorrhoeae'' infections.  Resistance to these antibiotics has developed over the years with chromosomal resistance being the primary mechanism.<ref name="Tapsall" />

Low-level quinolone resistance has been linked to changes in cell permeability and efflux pumps.  The NorM efflux pump is encoded by the ''norM'' gene and provides resistance to fluoroquinolones.<ref name="Rouquette-Loughlin" />  The NorM efflux pump is a member of the MATE (multidrug and toxic compound extrusion) family and functions by a Na+ antiporter.  It is also known that a point mutation upstream of the ''norM'' gene will causes overexpression of NorM, and mediate elevated resistance.<ref name="Rouquette-Loughlin" />

High-level resistance to quinolones has been seen through target modification acting on the DNA gyrase and topoisomerase IV.  Multiple amino acid substation mutations in the ''gyrA'' gene, which encodes for the DNA gyrase, have been seen extensively.  DNA gyrase is an enzyme that binds to DNA and introduces negative supercoiling.<ref name="Drlica">Drlica, Zhao (1997) DNA gyrase, topoisomerase IV, and the 4-quinolones. Microbiology and Molecular Biology Reviews 61:377–392
</ref>  This helps unwind the DNA for replication.  If there is a mutation in the DNA gyrase, then the quinolone will not be able to bind to it resulting in the activity of DNA gyrase not being inhibited.  Multiple mutations have also been noted in the ''parC'' gene which encodes for the topoisomerase IV.  Topoisomerase IV acts similarly to DNA gyrase and is involved in unwinding DNA for replication.<ref name="Drlica" />

== Cephalosporins ==

[[File:Beta-lactam antibiotics example 1.svg|thumb|Basic structures of penicillins(1) and cephalosporins(2) with beta-lactam ring highlighted in red]]

[[Ceftriaxone]] and [[cefixime]] are third generation cephalosporins and are often used as treatments for ''N. gonorrhoeae infections''.<ref name="Tapsall" />  The cephalosporins are part of a larger beta-lactam family of antibiotics.<ref name="Wilson">Wilson, Brenda A., Abigail A. Salyers, Dixie D. Whitt, and Malcolm A. Winkler. Bacterial Pathogenesis: A Molecular Approach. 3rd ed. Washington DC: ASM Press, 2011. Print.</ref>  The newly discovered H041 strain of ''N. gonorrhoeae'', originally isolated from a commercial sex worker in Japan, was shown to be resistant to this antibiotic.<ref name="Golparian">Unemo, Golparian, Nicholas, Ohnishi, Gallay, Sednaoui (2011) High-Level Cefixime- and Ceftriaxone-Resistant ''Neisseria gonorrhoeae'' in France: Novel ''penA'' Mosaic Allele in a Successful International Clone Causes Treatment Failure. Antimicrobial Agents and Chemotherapy 1273–1280
</ref>

The possible mechanisms of resistance to this antibiotic are as follows:
# an alteration of more than four amino acids in the C-terminal end of the PBP-2,<ref name="Unemo">Unemo (2008) Real-time PCR and subsequent pyrosequencing for screeing of ''penA'' mosaic alleles and prediction of reduced susceptibility to expanded-spectrum cephalosorins in ''Neisseria gonorrhoeae''. Acta Pathologica, Microbiologica et Immunologica Scandinavica 116:1001–1008
</ref> which would result in the antibiotic being unable to bind to its target
# mutations in the promoter regions of ''mtr'', resulting in the overexpression of genes that code for efflux pumps
# mutations in the ''penB'' gene that encodes for the bacterial porin.  This form of resistance has only been observed with ceftriaxone which is administered through an intramuscular injection.<ref name="Tapsall" />

== Tetracyclines ==

Tetracyclines are a class of antibiotics that inhibit protein synthesis by binding to the 30s ribosomal subunit of bacterial cells, keeping transcription of the bacterial genome from occurring.<ref name="Wilson" />  Tetracyclines are bacteriostatic, which means that the growth of the bacterium will be slowed.<ref name="Murray" />  Tetracyclines are not often recommended for the treatment of ''N. gonorrhoeae'' because the treatment regimen requires many doses, which may affect compliance and contribute to resistance.<ref name="Tapsall" />  Tetracycline is still used as treatment for this infection in developing countries because the cost for the drug is low <ref name="Tapsall" />

As with the penicillin resistance, the ''penB'' (porin formation) and ''mtr'' (efflux pump formation) mutations mediate chromosomal resistance.  These adaptations will also affect the ability of the antibiotic to get into, or stay in the bacterial cell.  High level resistance of ''N. gonorrhoeae'' to tetracyclines was first reported in 1986 with the discovering of the ''tetM'' determinant.<ref name="Tapsall" />  The mechanism of resistance is still unknown.

== Aminoglycosides ==

''N. gonorrhoeae'' has also shown resistance to the aminoglycoside class of antibiotics. These antibiotics bind to the 16s rRNA of the 30S subunit of the bacterial ribosome,<ref name="Wilson" /> thereby stopping transcription of the bacterial genome.  Resistance appears to be acquired through porin-related mechanisms, much like the cephalosporin resistance mechanism.  This mechanism would result in the access of the antibiotic to the bacterial cell being inhibited.  There is a possibility of future enzymes (made by the bacterium) that will be able to break down and inactivate the aminoglycosides.<ref name="Tapsall" />

== See also ==

* ''[[Neisseria gonorrhoeae]]''
* ''[[Antibiotic resistance]]''

== References ==

{{reflist|2}}

[[Category:Sexually transmitted diseases and infections]]
[[Category:Antibiotic-resistant bacteria]]