{{Use mdy dates|date=November 2014}}
{{Infobox musical artist
| name                = Arli Liberman <br/>אהרלה ליברמן  
| image                 = Arli Liberman TFM NZ 2014.jpg
 | caption            = Arli Liberman at TFM, New Zealand 2014.
| image_size          = 300px
| native_name_lang    = "heb"
| origin              = [[Israel]]
| birth_date          = {{birth date and age|1986|12|17}}
| occupation = Musician/producer
|instrument           = Electric guitar, acoustic guitar electric bass, keyboards, percussion, [[bağlama]], drums
| genre               =   [[World Music|World]], [[instrumental rock]], [[progressive rock]], [[Middle Eastern music|middle eastern]]
| years_active = 2002–present
| associated_acts     = [[Whiteflag Project]], Zaviot
| notable_instruments = '''Guitars:'''<br>Langcaster LTA-13 <br>Langcaster LTA-14<br>[[Yamaha LLX16]]
| background=non_vocal_instrumentalist
| website             = [http://www.arliliberman.com/ arliliberman.com]}}

'''Arli Liberman''' ({{lang-he|אהרלה ליברמן}}; born December 17, 1986) is an Israeli guitarist and record producer of [[Jewish]] [[Israelites|origin]].

==Biography==

Liberman, the youngest of six children, was born in [[Beersheba]], Israel. He began guitar at 13 years old and at age 16 began working as a session guitarist with a variety of recording artists in Israel. At age 16 Liberman was discovered and mentored by gold and platinum selling English record producer and bassist [[Mark Smulian]].<ref>{{cite web|url=http://www.marksmulian.in |title=Archived copy |accessdate=2012-01-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20130724132010/http://www.marksmulian.in/ |archivedate=July 24, 2013 |df=mdy }}</ref> Mark became a prime influence not only in artistic and musical direction but together with sound engineer Ram Czudnowski also provided pragmatic education in [[Audio engineering|sound engineering]] and [[record producer|production skills]] at Digihipi [[recording studios]].

===Collaborations===
From the years 2005 to 2009 Liberman was a key member of the band [[Whiteflag Project]]<ref>http://www.whiteflagmusic.com/</ref> – one of the Middle East's first  [[World music|world]] [[Fusion (music)|fusion]] [[Crossover (music)|cross-over]] bands made up of [[Palestinian people|Palestinian]] and [[Israelis|Israeli]] musicians.<ref>[http://www.elmundo.es/elmundo/2009/05/21/orienteproximo/1242897737.html La 'bandera blanca' Palestina-Israelí ]</ref> He co-produced, edited and played on their latest album ''Talk'' released by [[Phonag Records]] in 2005. Whiteflag Project performed at various festivals worldwide such as the [[Montreux Jazz Festival]] in Switzerland and the Creation of Peace Festival in Russia. 
In 2005, Whiteflag Project went on to create an award winning documentary ''Playing with the Enemy''<ref>[https://www.youtube.com/watch?v=hW1GtuM90Bg Abrahami – Netz Productions] National Swiss Television</ref> broadcast live on National Swiss Television. In 2008 the band made an appearance for [[Amnesty International]] and in 2008 they performed at the Global Peace Festival.

In 2007 Liberman was on the production team and played guitar for the world music project called ''Ras Hasatan – A Sinai Journey''<ref>[https://www.youtube.com/watch?v=dX4XSqnurIo Maktoob by Sigal Soliman] Ras Hasatan Official in [[Egypt]]</ref> where 50 musicians from all over the world came together in the [[Sinai Peninsula|Sinai]] Desert, [[Egypt]] and created a unique world music album – featuring Israeli artist [[Mosh Ben-Ari]] – one of the songs went to the compilation album [[Putumayo World Music|Putumayo]].<br>
At the age of 17 he performed with one of Israel's most prolific artists [[Shalom Hanoch]] who is considered to be the father of Israeli rock.

[[File:Arli and Shalom Live 2004.jpg|thumb|Arli Liberman performing with [[Shalom Hanoch]] in [[Givat Haim (Ihud)|Givat Haim]], [[Israel]] in 2004.]]

Since 2008 he has been a member of the band [[Zaviot]]<ref>[http://www.haaretz.com/general/it-s-important-to-be-a-neanderthal-1.240349 Haaretz] Zaviot in the Israeli paper Haaretz</ref><ref>[http://www.haaretz.co.il/gallery/1.1307777 It's important to be a Neanderthal] Hebrew</ref> – one of Israel's premier jazz groups. Zaviot was the most successful, original jazz group in Israel during the 1980s with performances worldwide – featuring [[clarinet]] player [[Harold Rubin]]. Zaviot performed in the first [[Red Sea Jazz Festival]]<ref>http://www.jpost.com/Arts-and-Culture/Music/Festival-Review-Rompin-at-the-Red-Sea</ref><ref>[http://www.redseajazzeilat.com/en/history/2012/?artist=94 Zaviot at the Red Sea Jazz Festival] {{webarchive |url=https://web.archive.org/web/20130303043124/http://www.redseajazzeilat.com/en/history/2012/?artist=94 |date=March 3, 2013 }}</ref> and won an award for the most original Israeli jazz group.

==Artistic style==
Liberman's signature sound is a blend of [[Western culture#Music|Western]] and [[Middle Eastern music|Middle Eastern]] influences from a [[blues]] and [[rock and roll]] perspective. He specializes in unique [[Musical tone|tones]] and [[Phrase (music)|phrasing]], eastern [[Musical scale|scales]], alternate [[chord (music)|chord]] voicings, [[Arrangement (music)|song arrangement]] and [[Phrase (music)|phrase]] [[Loop (music)|looping]]. In contrast with many players his [[Effects unit|effects]] enhance the music rather than his playing. He is constantly experimenting with new sounds through spontaneity and originality while being innovative and progressive through [[Musical improvisation|improvisation]] and [[Musical expression|expression]].

==Discography==

===Solo===

* 2013 – ''Arli Liberman '' – Self-titled

===Collaborations===

* 2005 – ''Whiteflag – Exile'' – Guitarist
* 2005 – ''Shai Naus – The Time has Come'' – Guitarist, editor & recording engineer 
* 2006 – ''Eyal Kofman – One second before''<ref>{{cite web|url=http://www.israel-music.com/eyal_kofman/one_second_before/ |title=One Second Before by Eyal Kofman |publisher=Israel-music.com |date= |accessdate=2011-11-12}}</ref> – Guitarist
* 2006 – ''Chuloo – Oldspice''<ref>{{cite web|url=http://mooma.mako.co.il/Discs.asp?ArtistId=25470&AlbumId=41128 |title=MOOMA – המוסיקה של ישראל |publisher=Mooma.mako.co.il |date= |accessdate=2011-11-12}}</ref> – Guitarist
* 2006 – ''Har Ephraim '' – Guitarist & co-producer
* 2006 – ''Amir Estline – Reflections<ref>[http://amirestlein.bandcamp.com/album/- אמיר אסטליין-השתקפויות]</ref>'' – Guitarist, recording engineer, editor & co-producer
* 2006 – ''SoulJa '' – Recording engineer
* 2007 – ''Anger Boys '' – Guitarist
* 2007 – ''Nagoa '' – Editor & assistant sound engineer 
* 2007 – ''Theatre Broadway Manhattan, NY '' – Editor, recording engineer  & sound engineer 
* 2007 – ''[[Rockfour]] – Memories of the never happened'' – Editor & assistant sound engineer
* 2007 – ''Ras Hasatan (A Sinai Journey) '' – Guitarist, co-producer, sound engineer & editor
* 2008 – ''Kol Ha Ella (Voice of the Goddess)'' – Guitarist & editor
* 2008 – ''[[Rami Kleinstein]]'' Tribute song for [[Gilad Shalit]] –  Sound engineer
* 2009 – ''Adi Dagan'' – Guitarist & assistant sound engineer
* 2009 – ''[[Whiteflag Project]] – Talk''  – Guitarist, co-producer & editor
* 2012 – ''Ngatapa Black – I Muri Ahiahi'' – Producer, arranger & guitarist<ref>http://www.indies.co.nz/imnz/imnz-newsletter-charts-to-march-10-introducing-the-taite-music-prize-finalists/</ref> – Nominated Best Maori Album of the Year at the [[New Zealand Music Awards]] 2013)<ref>http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=11144180</ref>

==TV/film appearances==
*2005 — Playing With The Enemy – with Whiteflag Project 
*2012  — Neighbourhood – Satellite Media

==References==
{{Reflist}}

{{DEFAULTSORT:Liberman, Arli}}
[[Category:Progressive rock guitarists]]
[[Category:Israeli musicians]]
[[Category:Israeli guitarists]]
[[Category:Israeli composers]]
[[Category:Lead guitarists]]
[[Category:Israeli rock guitarists]]
[[Category:1986 births]]
[[Category:Jewish musicians]]
[[Category:Israeli record producers]]
[[Category:Living people]]