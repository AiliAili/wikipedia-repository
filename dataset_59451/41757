{{Infobox national military
|country=Rwanda
|name=Rwanda Defence Force
|native_name=Ingabo z'u Rwanda
|image=
|caption=
|image2=
|caption2=
|founded=1962
|current_form=1994
|disbanded=
|branches=Army, Air Force, Reserve Force
|headquarters=Post Box 23, [[Kigali]]<ref name=WDARwanda>{{cite journal |last= |first= |authorlink= |year=2008 |title=World Defence Almanac  |journal=Military Technology |volume= XXXII |issue= 1 |pages= |publisher=Monch Publishing Group |location=Bonn, Germany |issn=0722-3226}}</ref>
<!-- Leadership -->
|commander-in-chief=[[Paul Kagame]]
|commander-in-chief_title={{nowrap|[[President of Rwanda]]}}
|minister=Gen. [[James Kabarebe]]
|minister_title=[[Ministry of Defence (Rwanda)|Minister of Defence]]
|commander={{nowrap|Gen. [[Patrick Nyamvumba]]}}
|commander_title={{nowrap|Chief of Defence Staff}}
<!-- Manpower -->
|age=
|conscription=
|manpower_data=2010 est.
|manpower_age=16–49<ref name=CIA>{{cite web|url=http://www.cia.gov/library/publications/the-world-factbook/geos/rw.html|title=CIA World Factbook|publisher=|accessdate=23 October 2014}}</ref> 
|available=2,625,917 
|available_f=2,608,110
|fit=1,685,066
|fit_f=1,749,580
|reaching=
|reaching_f=
|active=33,000{{sfn|IISS|2016|page=462}}
|ranked=
|reserve=
|deployed=
<!-- Financial -->
|amount=$91 million (2015){{sfn|IISS|2016|page=462}}
|percent_GDP= 1.1% (2015){{sfn|IISS|2016|page=462}}

<!-- Industrial -->
|domestic_suppliers=
|foreign_suppliers=
|imports=
|exports=
|history= '''[[List of wars involving Rwanda|Military history of Rwanda]]'''<br/>[[Rwandan Civil War]] <br> [[First Congo War]] <br> [[Second Congo War]] <br> [[Six-Day War (2000)]] <br> [[Dongo conflict]]<br/>[[2009 Eastern Congo offensive]]
}}

The '''Rwanda Defence Force (RDF)''' (in [[Kinyarwanda]]: ''Ingabo z'u Rwanda'') is the national army of [[Rwanda]]. The country's armed forces were originally known as the [[Rwandan Armed Forces]] (FAR), but following the victory of the [[Rwandan Patriotic Front]] in the country's [[Rwandan Civil War|civil war]] in 1994, it was renamed to the '''Rwandan Patriotic Army''' (RPA), and later to its current name.

The RDF comprises:<ref>Rwandan Ministry of Defence, [http://www.mod.gov.rw/?Law-establishing-Rwanda-Defence Law Establishing Rwanda Defence Forces], LAW N° 19/2002 of 17/05/2002, J.O. n° 13 of 01/07/2002</ref>

* the High Command Council of the RDF
* the General Staff of the RDF
* the Rwanda Land Force
* the Rwanda Air Force
* specialised units

In November 2002 [[Emmanuel Habyarimana]] was removed{{by whom|date=October 2015}} from his post as [[Minister of Defence (Rwanda)|Minister of Defence]], an action which government spokesperson Joseph Bideri attributed to his "extreme pro-Hutu" views.<ref name=bbc20030401a>{{cite news|url= http://news.bbc.co.uk/1/hi/world/africa/2905501.stm
| title= Rwanda ex-minister defects
| publisher= BBC News
| date= 2003-04-01
| accessdate= 2015-10-15
| quote = [General Habyarimana] was defence minister from 2000 until November last year, when Rwandan spokesman Joseph Bideri said he was sacked for his "extreme pro-Hutu" views. 
}}</ref>  [[Marcel Gatsinzi]] became Minister of Defence (in office 2002-2010) in succession to Habyarimana.

After it conquered the country in July 1994 in the aftermath of the [[Rwandan Genocide]] of April to July 1994, the [[Rwandan Patriotic Front]] (RPF) decided to split into a political division (which retained the RPF name) and a military division, which would serve as the official army of the Rwandan state.

Defence spending continues to represent an important share of the national budget, largely due to continuing security problems along Rwanda's frontiers with the [[Democratic Republic of the Congo]] and [[Burundi]], and lingering concerns about [[Uganda]]'s intentions towards its former ally. The Rwandan government launched an ambitious plan to demobilize thousands of soldiers, resulting in a standing military of 33,000 and another 2,000-strong paramilitary force - a decrease from 70,000 in just a decade. 

During the [[First Congo War|First]] and [[Second Congo War]]s of 1996-2003, the RPF committed wide-scale human-rights violations and crimes against humanity in the Democratic Republic of the Congo, according to the United Nations Mapping Report.<ref>
[http://www.ohchr.org/en/Countries/AfricaRegion/Pages/RDCProjetMapping.aspx United Nations Mapping Report: DRC 1993–2003]
</ref>

==Historical outline 1962–1994==
{{Hutu militants}}
The U.S. Army's ''Area Handbook for Rwanda'', compiled in 1968-9, describes the security forces of Rwanda in 1969 as the 2,500 strong National Guard and the National Police, about 1,200 strong.<ref>Richard F,. Nyrop, 'Area Handbook for Rwanda,' DA 550-84, research completed April 1, 1969, p.184-185</ref> The National Guard had been established two years before independence and had gained experience by repelling small [[Tutsi]] invasions in 1963 and 1964. It was under the direction of the Minister of Defence, [[Juvénal Habyarimana]], who also held the function of Chief of Staff of the National Guard in mid-1969. At that time it was composed of a headquarters, an intervention group (effectively an infantry battalion), five other rifle companies, and five independent rifle platoons.

The ''Forces armées rwandaises'' (FAR) was the national army of [[Rwanda]] until July 1994, when the government collapsed in the aftermath of the [[Genocide against Tutsi]] and the war with the [[Rwandan Patriotic Front]]. The FAR was estimated at 7,000 strong, including approximately 1,200 members of the [[Gendarmerie]]. Elite troops included the Presidential Guard, estimated at between 1,000-1,300 troops, as well as the Paracommando and Reconnaissance units.<ref>Des Forges, 1999, p.43</ref> These two units were of battalion strength by 1994, and then counted a total of 800 troops.<ref>Des Forges, 1999, p.194</ref>

In response to the RPF invasion of 1990, the 5,000-man FAR rapidly expanded, with French training assistance (as many as 1,100 French troops were in Rwanda at a time<ref>Prunier, The Rwanda Crisis, p.163, cited in Des Forges, 1999, p.118</ref>), to some 30,000 by 1992.<ref>[[Alison Des Forges]], 'Leave None to Tell the Story,' [[Human Rights Watch]], March 1999, ISBN 1-56432-171-1, p.60</ref>

The [[Arusha Accords]], signed on August 4, 1993, laid out a detailed plan for the integration of the Rwandan Government and [[Rwandan Patriotic Front]] military forces.<ref>See [http://www.incore.ulst.ac.uk/services/cds/agreements/pdf/rwan1.pdf Arusha Accords], hosted at [[University of Ulster]], pages 49-71</ref> The Rwandan government was to provide 60% of the troops for the new integrated army, but would have to share command positions with the RPF down to the level of battalion. The new army was to consist of no more than 19,000 soldiers and 6,000 [[Gendarmerie]].<ref>Des Forges, 1999, p.124-125</ref> However radical elements within the Rwandan government were implacably opposed to implementation of the Accords and, instead, began the planning that would lay the foundations for the genocide.

The Reconnaissance Battalion's commander, [[François-Xavier Nzuwonemeye]], and his subordinates played a key role during the genocide. Together with the Reconnaissance Battalion, the Paracommando Battalion under Major Aloys Ntabakuze and the Presidential Guard under Major [[Protais Mpiranya]] became the three most significant ''genocidare'' units.

Col. [[Marcel Gatsinzi]] was briefly named chief of staff of the armed forces from April 6 to April 16, 1994, but was replaced by [[Augustin Bizimungu]], who was quickly promoted to major general, since Col. Gatsinzi opposed the genocide.<ref>Des Forges, 1999, p.264</ref> Bizimungu was only briefly chief of staff before fleeing the country. Many soldiers of the FAR have since been implicated by the [[International Criminal Tribunal for Rwanda]] in the genocide, including its leader during the genocide, Col. [[Théoneste Bagosora]], who was chief of the ''cabinet'' (private office) of the Ministry of Defence prior to the genocide.  Other top leaders in the FAR were implicated in the assassination of the Rwandan president, [[Juvénal Habyarimana]], which sparked the genocide.

Many elements of the former Rwandan regime, including soldiers of the FAR, fled to eastern [[Zaire]] after the RPF victory, where they formed the [[Rassemblement Démocratique pour le Rwanda]] (RDR), which later became the [[Democratic Forces for the Liberation of Rwanda]] (FDLR), which is still active in eastern [[Democratic Republic of Congo|Congo]]'s [[North Kivu]] Province.

==Land Forces==
A number of sources, including [[Gérard Prunier]], document U.S. aid to the RPA before the [[First Congo War]].<ref>[[Gerard Prunier]], From Genocide to Continental War, 2009, p.126-127 and [http://cncblog.congonewschannel.net/2009/09/rwanda-installing-us-protectorate-in.html]</ref> The officially admitted part of the training was [[Joint Combined Exchange Training]]. Prunier strongly implies the United States supplied communications equipment, vehicles, boots, and medicines to the RPA before the war began and after it broke out, delivered second-hand [[Warsaw Pact]] weapons and ammunition either directly to [[Goma]] or by airdrop along the AFDL front lines. He reports that after the war's outbreak, the [[United States Air Force|U.S. Air Force]] had switched from using [[C-141 Starlifter]]s and [[C-5 Galaxy]]s to deliver the non-lethal aid to [[Kigali Airport]] and [[Entebbe Airport]], to airdrops by [[C-130 Hercules]] aircraft.<ref>[[Gerard Prunier]], ''From Genocide to Continental War: The "Congolese" Conflict and the Crisis of Contemporary Africa'', C. Hurst & Co, 2009, ISBN 978-1-85065-523-7, p.127, citing author's direct personal observation and several interviews with journalists, both local and foreign, in Kigali and Kampala, 1995 and 1996, for the Kigali/Entebbe report, and interviews with [[DGSE]] officers, Paris, May 1997, and UPDF officers, Kampala, November 1997 for the C-130 airdrop report.</ref>

From July 1994 until December 1997 the RPA had six brigades, as designated in the Arusha Accords: 402nd in Kigali and Kigali Rurale Prefecture; 201st in Kibungo, Umatura, and Byumba Prefectures; 301st in Butare, Gikongoro, and Cyangugu Prefectures; 305th in Gitatama and Kibuye Prefectures; and 211th in Gisenyi and Ruhengeri Prefectures. The brigade boundaries mirrored the political administrative boundaries, which often complicated military operations.<ref>Rick Orth (former [[United States Army]] attache in Rwanda), Rwanda's Hutu Extremist genocidal Insurgency: An Eyewitness Perspective, Small Wars & Insurgencies, Volume 12, Number 1, Spring 2001, pp. 76-109 (34), note 67, page 108</ref> During the [[First Congo War]] the brigade headquarters remained inside Rwanda but directed operations inside the [[Democratic Republic of the Congo]].<ref>Orth, 2001, note 67, page 108</ref>

Jane's World Armies said in July 2009 that 'the RDF is deployed to protect the country's borders and defend against external aggression. There are four divisions, each deploying three brigades:

* 1 Division, based at [[Kigali]], covers the central and east region;
* 2 Division, based at [[Byumba]], covers the north and east region;
* 3 Division, based at [[Gisenyi]], covers the northwest region; and
* 4 Division, based at [[Butare]], covers the southwest region.'<ref>Jane's World Armies: Rwanda, Role and Deployment,' July 2009</ref>

The [[Cyangugu]] Military Camp has been reported to house the 31st Brigade of the 4th Division.<ref>[http://www.geocities.com/bgratianus/honore_gbanda_nzambo.pdf Honor&#233; Ngbanda Nzambo] {{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>

Gen. Patrick Nyamvumba has been Chief of Defence Staff since June 2013.

Many soldiers from the former Rwandan Armed Forces (FAR), the national army under the previous regime, have been incorporated into the RDF since 1994. This process began soon after the genocide in January 1995, when several former FAR officers were given senior positions in the new armed forces: Col. (later Gen.) Marcel Gatsinzi became the Deputy Chief of Staff of the RPA, Col. Balthazar Ndengeyinka became commander of the 305th Brigade, Lt. Col. Laurent Munyakazi took command of the 99th Battalion, and Lt. Col. (later Brig. Gen.) Emmanuel Habyarimana became an RPA Member of Parliament and Director of Training in the Ministry of Defence. Gen. Gatsinzi later became Director of Security and then Minister of Defence in 2002.<ref>Orth 2001</ref>
[[File:Mil Mi-17 Rwanda Air Force - Darfur support, U.S. Army Africa, Kigali, Rwanda 090114.jpg|right|thumb|Rwandan soldiers carrying equipment at Kigali International Airport]]
Circa 2000 during the [[Second Congo War]], the Rwanda Patriotic Army unofficially admitted to having 4,000 to 8,000 troops deployed in the Congo, according to the [[Economist Intelligence Unit]], but this was believed to be a substantial understatement.<ref>Economist Intelligence Unit Country Profile 2000, via http://www.willum.com/dissertation/3rpa.PDF.</ref> The [[International Crisis Group]] estimated that the RPA has between 17,000 and 25,000 troops deployed in the Congo. In April 2001, a [[United Nations]] report on the exploitation of the Congo, said the RPA had a minimum of 25,000 troops in the Congo, an estimate the report attributes to “military specialists with a great deal of experience in the region."<ref>[[United Nations Security Council]], Report of the Panel of Experts on the Illegal Exploitation of Natural Resources and Other Forms of Wealth of the Democratic Republic of the Congo, S/2001/357, 12 April 2001</ref> During the deployment on DRC, Rwandan forces fought the so-called "[[Six-Day War (2000)|Six-Day War]]" against [[Uganda]]n forces over the city of [[Kisangani]], leaving at least 1,000 dead.

On 17 September 2002 the first Rwandan soldiers were withdrawn from the eastern DRC. On 5 October Rwanda announced the completion of its withdrawal; [[MONUC]] confirmed the departure of over 20,000 Rwandan soldiers.

The RDF reentered the DRC twice in 2009; to partake in a [[2009 Eastern Congo offensive|joint offensive]] against the [[Democratic Forces for the Liberation of Rwanda|FDLR]], and to assist the DRC in putting down the [[Dongo conflict|Dongo Rebellion]]. Both deployments were militarily successful.

==Air Force==
[[File:Roundel of Rwanda.svg|thumb|right|150px|Roundel of Rwanda]]
After achieving independence in 1962, the air arm (''Force aérienne rwandaise'') was formed with Belgian help.<ref>World Aircraft Information Files Brightstar Publishing London File 338 sheet 4</ref> By 1972 the first modern equipment started to arrive in the form of seven [[Alouette III]]s. Other deliveries included SA 342L Gazelles, [[Britten-Norman Islander]]s, [[Nord Noratlas]], [[SOCATA]] Guerrier armed light planes and AS 350B Ecureuils. After fighting began between the RPA and the government in 1990 most aircraft were shot down, destroyed on the ground or crashed. Few survived.

==Equipment==
Army Equipment.
{| class="wikitable"
! style="text-align: left;"|Type
! style="text-align: left;"|Make
! style="text-align: left;"|Quantity.<ref>{{cite web|url=http://www.armyrecognition.com/rwanda_rwandan_army_uk/rwanda_rwandan_army_land_ground_forces_military_equipment_armoured_vehicle_pictures_information_desc.html|title=Rwanda Rwandan army land ground forces military equipment armoured vehicle pictures information desc - Army Recognition|publisher=|accessdate=23 October 2014}}</ref>
|-
| [[Tank]]
| [[T-54/55]]
| 34{{sfn|IISS|2016|page=463}}
|-
| [[Infantry Fighting Vehicle]]
| [[BMP-1]]
| ?{{sfn|IISS|2016|page=463}}
|-
| [[Infantry Fighting Vehicle]]
| [[Ratel IFV]]
| 35{{sfn|IISS|2016|page=463}}
|-
| [[Armoured Personnel Carrier]]
| [[BTR-60]]
| ?{{sfn|IISS|2016|page=463}}
|-
| [[Armoured Personnel Carrier]]
| [[WZ-551]]
| 20{{sfn|IISS|2016|page=463}}
|-
| [[Armored car (military)|Armoured Car]]
| [[Panhard AML]]
| 12
|-
| [[Armored car (military)|Armoured Car]]
| [[Panhard M3]]
| 16
|-
| [[Armored car (military)|Armoured Car]]
| [[Véhicule Blindé Léger|VBL]]
| 16{{sfn|IISS|2016|page=463}}
|-
| [[Armored car (military)|Armoured Car]]
| [[RG-31 Nyala]]
| 36{{sfn|IISS|2016|page=463}}
|-
| [[Multiple Rocket Launcher]]
| [[RM-70]]
| 5{{sfn|IISS|2016|page=463}}
|-
| [[Multiple Rocket Launcher]]
| [[LAR-160]]
| 5{{sfn|IISS|2016|page=463}}
|-
| [[Artillery]]
| [[122 mm howitzer 2A18 (D-30)|122mm D-30]]
| 6{{sfn|IISS|2016|page=463}}
|-
| [[Artillery]]
| [[M101 howitzer|105mm M101]]
| ?{{sfn|IISS|2016|page=463}}
|-
| [[Artillery]]
| [[152 mm howitzer M1943 (D-1)|152mm D-1]]
| 29{{sfn|IISS|2016|page=463}}
|-
| 
| 
| 
|-
| [[Assault Rifle]]
| [[AK-47]]
| ?
|-
| [[Assault Rifle]]
| [[IWI Tavor]]<ref>http://www.gunsandammo.com/reviews/jazz-singer-iwi-tavor-review/</ref>

| ?
|-
| [[Machine Gun]]
| [[PK machine gun|PKM]]
| ?
|-
| [[Rocket Launcher]]
| [[RPG-7]]
| ?
|}

Air Force Equipment.
{{avilisthead|nonstandard}}
|-
! Type
! Manufacturer
! Origin
! Class
! Role
! style="text-align: left;"|In service<ref name="WAF2017">{{cite journal|title=World Air Forces 2017|journal=Flightglobal|page=14|url=https://www.flightglobal.com/asset/14484|accessdate=10 February 2017}}</ref>
! style="text-align: left;"|Notes
|-
| [[Mil Mi-24|Mi-24]]
| [[Mil Moscow Helicopter Plant|Mil]]
| Soviet Union
| Helicopter
|
| 5
| 
|-
| [[Mil Mi-8|Mi-8 Hip]]
| [[Mil Moscow Helicopter Plant|Mil]]
| Soviet Union
| Helicopter
|
| 11
| 
|-
| [[AgustaWestland AW109|AW109]]
| [[Agusta]]
| Italy
| Helicopter
|
| 1
| 
|-
| [[AgustaWestland AW139|AW139]]
| [[AgustaWestland]]
| Italy
| Helicopter
|
| 1
| 
|}

==Notes==
{{reflist|2}}

==References==
* {{cite book |ref={{harvid|IISS|2016}}
|last=IISS |title=The Military Balance 2016|year=2016 |publisher=Routledge |isbn=978-1857438352}}
*[[Alison Des Forges]], 'Leave None to Tell the Story,' [[Human Rights Watch]], March 1999, ISBN 1-56432-171-1
*Richard F. Nyrop, Lyle E. Brenneman, Roy V. Hibbs, Charlene A. James, Susan MacKnight, Gordon C. McDonald; Army Area Handbook for Rwanda, U.S. Government Printing Office, 1969. Research and writing completed April 1, 1969.
*Rick Orth (former [[United States Army]] attache in Rwanda), Rwanda's Hutu Extremist genocidal Insurgency: An Eyewitness Perspective, Small Wars & Insurgencies, Volume 12, Number 1, Spring 2001
*[[Gérard Prunier]], ''From Genocide to Continental War: The "Congolese" Conflict and the Crisis of Contemporary Africa'', C. Hurst & Co, 2009, ISBN 978-1-85065-523-7,

==Further reading==
*Patrick Lefèvre, Jean-Noël Lefèvre, [http://www.racine.be/content/racine/fondsen/histoire/memoires/2/index.jsp?titelcode=12522&fondsid=22 Les militaires belges et le Rwanda 1916-2006], Racine, 2006
*Richard Muhirwa, [http://oai.dtic.mil/oai/oai?verb=getRecord&metadataPrefix=html&identifier=ADA380247 Rwandese Patriotic Army Logistics Unit (G4) Assessment and Recommendations for Change], Master's thesis, Naval Postgraduate School Monterey CA, 2000
*http://allafrica.com/stories/200810230362.html

==External links==
*[http://www.mod.gov.rw/ Ministry of Defence, Republic of Rwanda]

{{Rwanda topics}}
{{Military of Africa}}

[[Category:Military of Rwanda|*]]
[[Category:Rwandan genocide]]
[[Category:Military units and formations established in 1994]]