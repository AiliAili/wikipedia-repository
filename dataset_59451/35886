{{good article}}
{{Other uses}}
[[File:Wien T%C3%BCrkenschanzpark (2376611600).jpg|thumb|right|300px|The [[:de:Türkenschanzpark|Türkenschanzpark]] in [[Vienna]] during the early afternoon]]
[[File:Rooftop Bar, Metropolitan Museum Of Art (5894065780).jpg|thumb|right|300px|Visitors to the [[Metropolitan Museum of Art]] in [[New York City]] during the late afternoon]]
'''Afternoon''' is the time of the [[day]] between [[noon]] and [[evening]]. It is the time when the [[sun]] is descending from its [[zenith]] in the [[sky]] to somewhat before its [[Azimuth|terminus]] at the [[horizon]] in the west direction. In [[human]] life, it occupies roughly the latter half of the standard [[working time|work]] and school day. It is also associated with a number of concerns related to health, safety, and economic productivity: generally, the early afternoon, after most people have eaten lunch, corresponds to decreased work performance, decreased alertness, and a higher incidence of [[traffic collision|motor vehicle accidents]]. It is usually from 12 PM to 4 PM, but in some countries it may be from 12 PM to 8 PM.

== Terminology ==
Afternoon is the time occurring between [[noon]] and [[evening]].<ref name="mw">{{cite web|title=Afternoon|url=http://www.merriam-webster.com/dictionary/afternoon|publisher=[[Merriam-Webster]]|accessdate=October 9, 2014}}</ref> The specific range of time this encompasses varies in one direction: noon is currently defined as a constant 12:00 pm,<ref>{{cite web|title=Noon|url=http://www.merriam-webster.com/dictionary/noon|publisher=Merriam-Webster|accessdate=October 9, 2014}}</ref> but when afternoon ends is dependent on when evening begins, for which there is no standard definition.<ref name="evening" /> However, before a period of transition from the twelfth to fourteenth centuries, ''noon'' instead referred to 3 pm—possible explanations include shifting times for prayers and midday meals, along which one concept of ''noon'' was defined—and so ''afternoon'' would have referred to a narrower timeframe.<ref>{{cite web|url=http://www.etymonline.com/index.php?term=noon|work=[[Online Etymology Dictionary]]|date=2001|title=noon (n.)|accessdate=October 10, 2014}}</ref>

The word ''afternoon'', which derives from ''after'' and ''noon'', has been attested from about the year 1300; [[Middle English]] contained both ''afternoon'' and the synonym ''aftermete''. The standard [[locative case|locative]] marking for the word was ''at afternoon'' in the fifteenth and sixteenth centuries but has shifted to ''in the afternoon'' since then.<ref>{{cite web|url=http://www.etymonline.com/index.php?term=afternoon|work=Online Etymology Dictionary|date=2001|title=afternoon (n.)|accessdate=October 10, 2014}}</ref> In [[Southern American English|Southern U.S.]] and [[Midland American English]], the word ''evening'' is sometimes used to encompass all times between noon and [[night]].<ref name="evening">{{cite web|url=http://www.merriam-webster.com/dictionary/evening|title=Evening|publisher=Merriam-Webster|accessdate=October 9, 2014}}</ref> The [[Irish language|Irish]] language contains four different words to mark time intervals from late afternoon to nightfall, this period being considered mystical.<ref>{{Harvnb|Ekirch|2006|p=xxxii}}</ref> Metaphorically, the word ''afternoon'' refers to a relatively late period in the expanse of time or in one's life.<ref name="mw" />

The term should not be confused with "after noon" (two separate words), which is a translation of the Latin ''post meridiem'' (p.m.), meaning a time between 12:00 [[midday]] and 12:00 [[midnight]].

== Events ==
[[File:Atlantic Coast (2308232123).jpg|thumb|right|The coastline of the [[Atlantic Ocean]] in the afternoon]]
Afternoon is a time when the [[sun]] is descending from its daytime peak. During the afternoon, the sun moves from roughly the center of the sky to deep in the west. In late afternoon, [[sunlight]] is particularly bright and glaring, because the sun is at a low angle in the [[sky]].<ref name="Aggarwal 172" /> The standard [[working time]] in most industrialized countries goes from the morning to the late afternoon or evening—archetypally, 9 am to 5 pm—so the latter part of this time takes place in the afternoon.<ref>{{cite web|title=Nine-to-fiver|url=http://www.merriam-webster.com/dictionary/nine-to-fiver|publisher=Merriam-Webster|accessdate=October 9, 2014}}</ref> Schools usually let out during the afternoon as well.

== Effects on living organisms ==
=== Hormones and body temperature ===
In [[diurnality|diurnal]] animals, it is typical for blood levels of the hormone [[cortisol]]—which is used to increase [[blood sugar]] and aid [[metabolism]] and is also produced in response to [[stress (psychological)|stress]]—to be most stable in the afternoon after decreasing throughout the morning. However, cortisol levels are also the most reactive to environmental changes unrelated to sleep and daylight during the afternoon. As a result, this time of day is considered optimal for researchers studying stress and hormone levels.<ref>{{Harvnb|Blaskovich|2011|p=74}}</ref> Plants generally have their highest [[photosynthesis|photosynthetic]] levels of the day at noon and in the early afternoon, owing to the sun's high angle in the sky. The large proliferation of [[maize]] crops across Earth has caused tiny, harmless fluctuations in the normal pattern of atmospheric [[carbon dioxide]] levels, since these crops photosynthesize large amounts of carbon dioxide during these times and this process sharply drops down during the late afternoon and evening.<ref>{{Harvnb|Sinclair|Weiss|2010|p=118}}</ref>

In [[human]]s, [[Thermoregulation|body temperature]] is typically highest during the mid to late afternoon.<ref name="Refinetti 556" /> However, human athletes being tested for physical vigor on exercise machines showed no statistically significant difference after lunch.<ref name="Refinetti 556">{{Harvnb|Refinetti|2006|p=556}}</ref> Owners of [[factory farm]]s are advised to use buildings with an east–west (as opposed to north–south) orientation to house their [[livestock]], because an east–west orientation generally means thicker walls on the east and west to accommodate the sun's acute angle and intense glare during late afternoon. When these animals are too hot, they are more likely to become belligerent and unproductive.<ref name="Aggarwal 172">{{Harvnb|Aggarwal|Upadhyay|2013|p=172}}</ref>

=== Alertness ===
[[File:Car accident poland 2008.jpg|thumb|left|[[Traffic collision|Motor vehicle accidents]] like this one in [[Poland]] are common during the afternoon, especially [[rush hour]].]]
The afternoon, especially the early afternoon, is associated with a dip in a variety of areas of human cognitive and productive functioning. Notably, [[traffic collision|motor vehicle accidents]] occur more frequently in the early afternoon, when drivers presumably have recently finished lunch.<ref>{{Harvnb|McCabe|2004|p=588}}</ref> A study of motor accidents in [[Sweden]] between 1987 and 1991 found that the time around 5 pm had by far the most accidents: around 1,600 at 5 pm compared to around 1,000 each at 4 pm and 6 pm. This trend may have been influenced by the afternoon [[rush hour]], but the morning rush hour showed a much smaller increase.<ref>{{Harvnb|Refinetti|2006|p=559}}</ref> In [[Finland]], accidents in the [[agriculture]] industry are most common in the afternoon, specifically Monday afternoons in September.<ref>{{Harvnb|McCabe|2004|p=471}}</ref>

One psychology professor studying [[circadian rhythm]]s found that his students performed somewhat worse on exams in the afternoon than in the morning, but even worse in the evening. Neither of these differences, however, was statistically significant.<ref name="Refinetti 556" /> Four studies carried out in 1997 found that subjects who were given tests on differentiating [[traffic sign]]s had longer [[reaction time]]s when tested at 3:00 pm and 6:00 pm than at 9:00 am and 12:00 pm. These trends held across all four studies and for both complex and abstract questions.<ref>{{Harvnb|McCabe|2004|p=590}}</ref> However, one UK based researcher failed to find any difference in exam performance on over 300,000 A-level exam papers sat in either the morning or afternoon. <ref>{{cite journal|last1=Quartel|first1=Lara|title=The effect of the circadian rhythm of body temperature on A-level exam performance|journal=Undergraduate Journal of Psychology|date=2014|volume=27|issue=1|url=https://journals.uncc.edu/ujop/article/view/283/300}}</ref>

Human productivity routinely decreases in the afternoon. [[Power station|Power plants]] have shown significant reductions in productivity in the afternoon compared to the morning, the largest differences occurring on Saturdays and the smallest on Mondays.<ref>{{Harvnb|Ray|1960|p=11}}</ref> One 1950s study covering two female factory workers for six months found that their productivity was 13 percent lower in the afternoon, the least productive time being their last hour at work. It was summarized that the differences came from personal breaks and unproductive activities at the workplace.<ref>{{Harvnb|Ray|1960|p=12}}</ref> Another, larger study found that afternoon declines in productivity were greater during longer work shifts.<ref>{{Harvnb|Ray|1960|p=18}}</ref>

It is important to note, however, that not all humans share identical circadian rhythms. One study across [[Italy]] and [[Spain]] had students fill out a questionnaire, then ranked them on a "morningness–eveningness" scale. The results were a fairly standard [[normal distribution|bell curve]]. Levels of alertness over the course of the day had a significant correlation with scores on the questionnaire. All categories of participants—evening types, morning types, and intermediate types—had high levels of alertness from roughly 2 pm to 8 pm, but outside this window their alertness levels corresponded to their scores.<ref>{{Harvnb|Refinetti|2006|p=561}}</ref>

== See also ==
* [[Morning]]
* [[Sunset]]
* [[Twilight]]
* [[Forenoon]]
* [[12-hour clock]]

== Notes ==
{{reflist|30em}}

== References ==
* {{cite book|ref=harv|last=Aggarwal|first=Anjali|last2=Upadhyay|first2=Ramesh|title=Heat Stress and Animal Productivity|publisher=[[Springer Publishing]]|isbn=978-8-132-20879-2|date=2013}}
* {{cite book|ref=harv|last=Blaskovich|first=Jim|title=Social Psychophysiology for Social and Personality Psychology|date=2011|isbn=978-0-85702-405-3}}
* {{cite book|ref=harv|last=Ekirch|first=A. Roger|title=At Day's Close: Night in Times Past|isbn=978-0-393-32901-8|publisher=W. W. Norton & Company|date=2006}}
* {{cite book|ref=harv|last=McCabe|first=Paul T.|title=Contemporary Ergonomics|date=2004|publisher=CRC Press|isbn=0-8493-2342-8}}
* {{cite book|ref=harv|last=Ray|first=James T.|title=Human Performance as a Function of the Work–Rest Cycle|publisher=[[National Academy of Sciences]]|date=1960}}
* {{cite book|ref=harv|last=Refinetti|first=Roberto|title=Circadian Physiology|date=2006|edition=2nd|publisher=Taylor & Francis Group|isbn=978-0-8493-2233-4}}
* {{cite book|ref=harv|last=Sinclair|first=Thomas M.|last2=Weiss|first2=Albert|date=2010|title=Principles of Ecology in Plant Production|publisher=[[University of Nebraska, Lincoln]]/[[University of Florida]]}}


== External links ==
* {{commonscat-inline|Afternoon}}
* {{wiktionary-inline|afternoon}}

{{Parts of a day}}

{{Authority control}}
[[Category:Parts of a day]]