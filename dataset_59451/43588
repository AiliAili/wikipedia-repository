<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 | name=S.10 Gurnard
 | image=Short Gurnard.jpg
 | caption=The Gurnard II as an amphibian
}}{{Infobox Aircraft Type
 | type=Fleet [[Fighter aircraft|fighter]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Short Brothers]]
 | designer=
 | first flight=16 April 1929
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Short Gurnard''' was a single-engined two-seat [[biplane]] naval [[Fighter aircraft|fighter]], built in the [[United Kingdom]] to an [[Air Ministry]] specification in 1929.  It failed to win production orders and only two flew.

==Design and development==

The duralumin-framed [[Short S.6 Sturgeon|Short Sturgeon]] had never been expected to achieve production status, but the [[Air Ministry]] were sufficiently impressed with its behaviour on water and in the air to invite Shorts to tender for [[List of Air Ministry Specifications#1920-1929|Air Ministry specification O.22/26]], a two-seat fleet fighter. The specification sought a [[Fairey Flycatcher]] replacement, an aircraft that would spend much time in fleet spotter and reconnaissance roles, though without the third crew member the navy usually thought necessary for the latter task; but it also was to have the speed and rate of climb to catch bombers. It was required to be able to perform as a deck lander or a seaplane, and to be strong enough in either configuration to be launched by catapult. Shorts were advised that their duralumin monocoque fuselage was not suitable for below deck maintenance. Their tender was rewarded with an order for two prototypes of the S.10 Gurnard.<ref name="CHB">{{harvnb|Barnes|James|1989|pp=211–16}}</ref>

As a result of the Air Ministry comments, the fuselage of the Gurnard was quite different from Shorts recent series of single-engined monocoques. It was a welded steel tube structure with duralumin detachable panels forward and fabric covering aft. The two separate open cockpits were very close together, the pilot sitting in front just under a visibility cut-out in the trailing edge of the upper wing, raised up for a better view over the nose.  A 0.303 in (7.7&nbsp;mm) Vickers machine gun operated by the pilot was mounted on the decking in front of him, slightly to port.  The rear cockpit was fitted with a Lewis gun on a [[Scarff ring]]. The steel fuselage stopped at the rear with an attachment for the only monocoque part of the Gurnard, the empennage-carrying tail cone. The fin and rudder leading edge was rounded, the trailing edge of the horn balanced rudder straight.  The latter extended below the fuselage, with additional fixed surface in front of it. These surfaces were fabric covered.<ref name="CHB"/>

The two Gurnard prototypes had different engines. The Gurnard II, the first to be completed, had a 525&nbsp;hp (392&nbsp;kW) [[Rolls-Royce Kestrel]] IIS supercharged water-cooled inline and the Gurnard I a 525&nbsp;hp [[Bristol Jupiter#Variants|Bristol Jupiter X]] supercharged radial engine. Accordingly, the Gurnard II had a smooth and pointed cowling, with a drum-shaped radiator between the undercarriage legs, whilst the radial engine had a narrow chord [[Townend ring]], with the rear part of its cylinders visible from the side.<ref name="CHB"/>

The Gurnard was a single-bay biplane. It had straight-edged, constant chord wings, the lower one being slightly shorter in span and markedly narrower in chord. Both wings carried dihedral, the upper plane the most. The wings were duralumin structures, fabric covered, with simple, near-parallel interplane struts between the spars. The centre section was supported by a pair of struts from the upper fuselage on each side.  Long [[Aileron#Frise ailerons|Frise ailerons]] were fitted to the upper wing only.<ref name="CHB"/>

The wheeled undercarriage was a simple single-axle arrangement, with [[oleo (shock absorber)|oleo legs]] forward to the engine bulkhead and rearward bracing struts to the root of the rear wing spar. There was a rather long tailskid to protect the downward-extended fin. The seaplane version used a pair of floats, their low drag profile refined via [[Schneider Trophy]] experience, cross braced with a pair of horizontal transverse struts. They were mounted with N-shaped struts which sloped strongly outwards from the fuselage.<ref name="CHB"/>

The Gurnard II, fitted with floats, was the first to fly, taking off from the [[Medway]] off Shorts' [[Rochester, Kent|Rochester]] factory on 16 April 1929. The pilot was [[John Lankester Parker]]. In May both Gurnards flew as landplanes from [[Lympne Airport|Lympne]], where Shorts maintained a base.  By early June both aircraft were at [[RAF Martlesham Heath]] for evaluation against the specification, though they both returned briefly to Shorts for some modifications. The water-based testing was done at the [[Marine Aircraft Experimental Establishment]] at [[Felixstowe]]. The Gurnards flew well and met the specifications, but were out-performed by the [[Hawker Osprey]], a close relative of the RAF's established bomber the [[Hawker Hart]], so no more Gurnards were produced. Shorts did get a useful order for large numbers of their floats for the Ospreys.<ref name="CHB"/>

The Gurnard II returned from Martlesham to Rochester in 1931 for conversion into an amphibian. This involved the fitting of a single, central float which carried a pair of wheels using an axle that passed through its top. The wheels were on stubs offset from the axle, so that in one position they were lifted above the water, but rotated through 180° from the pilot's cockpit they reached the ground. A similar, smaller version had been used successfully on the [[Short Mussel]] and on a converted [[de Havilland Moth|Moth]].<ref>{{harvnb|Barnes|James|1989|pp=191–5}}</ref>  The central float was again mounted with a pair of N-shaped struts, now vertical. There was a pair of outboard stabilising floats beyond the interplane struts, each mounted on a pair of vertical legs. The floats were each directly braced with a pair of struts to the interplane struts from below, and from above by a pair of struts from the top of the rear interplane strut to the tops of the legs. It first flew in this configuration in June 1931 and returned to Martlesham within the month, appearing in that year's Hendon RAF Display.<ref name="CHB"/>

After that it came back for while to Rochester, where it was used to investigate cooling of the Kestrel engine. At this time it was flying with a tall pillar radiator between fuselage and float. In October the float was modified, and later that month six flights were made between Lympne airfield (land) and the Medway (water) in a total of 90 minutes.<ref>The sites are about 37 miles apart</ref> In December it returned to Felixstowe, where it served as an engine and cooling system test-bed.<ref name="CHB"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Gurnard I, landplane)==
{{Aircraft specs
|ref={{harvnb|Barnes|James|1989|p=216}}
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=
|length ft=28
|length in=7
|length note=
|span m=
|span ft=37
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=429
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=3086
|empty weight note=
|gross weight kg=
|gross weight lb=4785
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Bristol Jupiter#Variants|Bristol Jupiter X]]
|eng1 type=9-cylinder radial
|eng1 kw=<!-- prop engines -->
|eng1 hp=525<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=160
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=3.5 hrs<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
<!-- add bulleted list here or if you want to use the following specific parameters, remove this parameter-->
|guns=1× 0.303 in (7.7 mm) [[Vickers machine gun]] firing forward from the top decking in front of the pilot and 1× 0.303 in (7.7 mm) [[Lewis gun]] on a [[Scarff ring]] in rear cockpit  
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[Blackburn Nautilus]]
*[[Fairey Fleetwing]]
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[List of fighter aircraft]]
*[[List of seaplanes and amphibious aircraft]]
}}

==References==

===Citations and notes===
{{commons category|Short Gurnard}}
{{reflist}}

===Cited sources===
{{refbegin}}
*{{cite book |title= Shorts Aircraft since 1900|last= Barnes|first=C.H. |last2 =James|first2= D. N.|year=1989 |publisher=Putnam Publishing  |location=London |isbn= 0-87021-662-7|ref=harv}} 
{{refend}}

<!-- ==External links== -->
{{Short Brothers aircraft}}

[[Category:British military aircraft 1920–1929]]
[[Category:Short Brothers aircraft|Gurnard]]