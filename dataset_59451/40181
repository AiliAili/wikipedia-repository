{{Infobox scientist
|name              = Hugo J. Bellen
|image             = [[File:Hugo J. Bellen.jpg]]
|image_size        = 
|caption           = 
|birth_date        = {{Birth year and age|1953}}
|birth_place       = 
|death_date        = 
|death_place       = 
|residence         = 
|citizenship       = USA
|nationality       = Belgium
|ethnicity         = 
|fields            = [[Genetics]], [[Developmental biology|Developmental Biology]], [[Neuroscience]]
|workplaces        = [[Baylor College of Medicine]], [[Howard Hughes Medical Institute]]
|alma_mater        = [[Vrije Universiteit Brussel]]; [[University of Antwerp]]; [[Ghent University]]; [[University of California, Davis]]
|doctoral_advisor  = John A. Kiger Jr.
|academic_advisors = [[Walter J. Gehring]], postdoctoral advisor
|doctoral_students = 
|notable_students  =
|known_for         = 
|author_abbrev_bot = 
|author_abbrev_zoo = 
|influences        = 
|influenced        = 
|awards            = {{no wrap|
[[Fulbright Program|Fulbright Award]] {{small|(1983)}}<br>
N.A.T.O. Fellowship {{small|(1986-1987)}}<br>
Michael E. DeBakey, M.D., Excellence in Research Award {{small|(1995)}}
<ref>{{cite web|url=https://www.bcm.edu/research/office-of-research/debakey-awards/recipients/bellen-hugo | title =Bellen, Hugo J. - DeBakey Awards - Baylor College of Medicine, Houston, Texas}}</ref>
<br>
The Dean's Faculty Award for Excellence in Graduate Education {{small|(1999)}}<br>
Charles Darwin Chair in Genetics {{small|(1999)}}<br>
The March of Dimes Chair in Developmental Biology {{small|(2000)}}<br>
Distinguished Service Professor of BCM {{small|(2010)}}<br>
Distinguished Alumnus Award, University of California, Davis {{small|(2011)}}
<ref>{{cite web|url=http://www.dbs.ucdavis.edu/alumni_and_friends/alumni_awards_2011.html | title=Alumni Awards}}</ref>
<br>
The Linda and Jack Gill Distinguished Neuroscience Investigator Award {{small|(2012)}}
<ref>{{cite web|url=http://newsinfo.iu.edu/news-archive/23076.html | title=IU's Gill Center honors Hugo J. Bellen and Guoping Feng for achievements in neuroscience}}</ref><br>
[[George W. Beadle Award]] {{small|(2014)}}
<ref>{{cite journal|last1=Bellen|first1=HJ|title=Survival of the fittest tools|journal=Genetics|date=2014 |volume=198|pages=427–8|pmid=25316776|doi=10.1534/genetics.114.169110|pmc=4196594}}</ref>
<br>
Miegunyah Distinguished Fellowship of the University of Melbourne {{small|(2015)}}
<ref>{{cite web|url=http://sciencematters.unimelb.edu.au/what-flies-tell-us-about-human-neurodegenerative-disease | title=Science Matters-Blog Archive-What flies tell us about human neurodegenerative disease}}</ref>
|religion          = 
|signature         =  
|footnotes         = 
}}
}}

'''Hugo J. Bellen''' is a professor at [[Baylor College of Medicine]] and an investigator at the [[Howard Hughes Medical Institute]]<ref name="HHMI">{{cite web|url=http://www.hhmi.org/research/demise-neurons |title=Howard Hughes Medical Institute Investigators: Hugo J. Bellen, D.V.M., Ph.D. |accessdate=2014-08-24 }}</ref>   who studies [[genetics]] and [[neurobiology]] in the [[model organism]], ''[[Drosophila melanogaster]]'', the fruit fly.

==Education==
Bellen obtained an [[MBA]] from the [[University of Brussels]], a [[D.V.M.]] from the Department of Veterinary Medicine of [[Ghent University]] and a [[Ph.D.]] in Genetics from the [[University of California, Davis]]. He performed his postdoctoral studies in the laboratory of [[Walter J. Gehring]] at the Biozentrum in the [[University of Basel]].

==Research==
===Neurodegeneration===

Dr. Bellen’s current research focuses on an effort to decipher the mechanisms by which mutations in specific genes cause [[neurodegeneration]], and to this end, he and his colleagues performed unbiased [[forward genetics|forward genetic]] screens in fruitflies that detect the progressive decline in function and morphology of photoreceptor neurons.<ref>{{cite journal|vauthors=Yamamoto S, Jaiswal M, Charng WL, Gambin T, Karaca E, Mirzaa G, Wiszniewski W, Sandoval H, Haelterman NA, Xiong B, Zhang K, Bayat V, David G, Li T, Chen K, Gala U, Harel T, Pehlivan D, Penney S, Vissers LE, de Ligt J, Jhangiani SN, Xie Y, Tsang SH, Parman Y, Sivaci M, Battaloglu E, Muzny D, Wan YW, Liu Z, Lin-Moore AT, Clark RD, Curry CJ, Link N, Schulze KL, Boerwinkle E, Dobyns WB, Allikmets R, Gibbs RA, Chen R, Lupski JR, Wangler MF, Bellen HJ|title=A Drosophila genetic resource of mutants to study mechanisms underlying human genetic diseases|journal=Cell|date=2014|volume=159|pages=200–14|pmid=25259927|doi=10.1016/j.cell.2014.09.002|pmc=4298142}}</ref> To date over 165 genes that cause a neurodegenerative phenotype when mutated have been uncovered by Dr. Bellen’s group using this strategy.<ref>{{cite journal|vauthors=Haelterman NA, Jiang L, Li Y, Bayat V, Sandoval H, Ugur B, Tan KL, Zhang K, Bei D, Xiong B, Charng WL, Busby T, Jawaid A, David G, Jaiswal M, Venken KJ, Yamamoto S, Chen R, Bellen HJ|title=Large-scale identification of chemically induced mutations in Drosophila melanogaster.|journal=Genome Res.|volume=24|pages=1707–18|pmid=25258387|doi=10.1101/gr.174615.114|pmc=4199363|year=2014}}</ref> Many of these genes encode homologues of human genes that are known to cause neurodegenerative diseases, including [[Amyotrophic Lateral Sclerosis]] (ALS) (Lou Gehrig's disease),<ref>{{cite journal|vauthors=Tsuda H, Han SM, Yang Y, Tong C, Lin YQ, Mohan K, Haueter C, Zoghbi A, Harati Y, Kwan J, Miller MA, Bellen HJ|title=The amyotrophic lateral sclerosis 8 protein VAPB is cleaved, secreted, and acts as a ligand for Eph receptors|journal=Cell|date=2008|volume=133|pages=963–77|pmid=18555774|doi=10.1016/j.cell.2008.04.039|pmc=2494862}}</ref> [[Charcot-Marie-Tooth]] (CMT),<ref>{{cite journal|vauthors=Sandoval H, Yao CK, Chen K, Jaiswal M, Donti T, Lin YQ, Bayat V, Xiong B, Zhang K, David G, Charng WL, Yamamoto S, Duraine L, Graham BH, Bellen HJ|title=Mitochondrial fusion but not fission regulates larval growth and synaptic development through steroid hormone production|journal=eLife|date=2014|volume=3|page=e03558|pmid=25313867|doi=10.7554/eLife.03558|pmc=4215535}}</ref> [[Parkinson’s disease]] (PD),<ref>{{cite journal|vauthors=Wang S, Tan KL, Agosto MA, Xiong B, Yamamoto S, Sandoval H, Jaiswal M, Bayat V, Zhang K, Charng WL, David G, Duraine L, Venkatachalam K, Wensel TG, Bellen HJ|title=The retromer complex is required for rhodopsin recycling and its loss leads to photoreceptor degeneration|journal=PLoS Biol.|date=2014|volume=12|page=e1001847|pmid=24781186|doi=10.1371/journal.pbio.1001847|pmc=4004542}}</ref> [[Alzheimer’s disease]] (AD), [[Leigh syndrome]],<ref>{{cite journal|vauthors=Zhang K, Li Z, Jaiswal M, Bayat V, Xiong B, Sandoval H, Charng WL, David G, Haueter C, Yamamoto S, Graham BH, Bellen HJ|title=The C8ORF38 homologue Sicily is a cytosolic chaperone for a mitochondrial complex I subunit|journal=J Cell Biol|date=2013|volume=200|pages=807–20|doi=10.1083/jcb.201208033|pmid=23509070|pmc=3601355}}</ref> and others, and these studies will help provide a much better understanding of the molecular mechanisms by which neurodegeneration occurs. A prevailing theme among these mutants seems to be dysfunction of the neuronal [[mitochondria]] and an increasing inability to deal with [[oxidative stress]], which manifests as [[lipid droplet]]s.<ref>{{cite journal|vauthors=Liu L, Zhang K, Sandoval H, Yamamoto S, Jaiswal M, Sanz E, Li Z, Hui J, Graham BH, Quintana A, Bellen HJ|title=Glial lipid droplets and ROS induced by mitochondrial defects promote neurodegeneration|journal=Cell|date=2015|volume=160|pages=177–90|pmid=25594180|doi=10.1016/j.cell.2014.12.019|pmc=4377295}}</ref>

===Technology===

Dr. Bellen has pioneered the development of novel technologies that accelerate Drosophila research and are currently used by the majority of fly labs today. Bellen was a leader in the development of ''P'' element-mediated enhancer detection which allows for discovery and manipulation of genes and was the impetus for a collaborative and ongoing project to generate an insertion collection for the community. Furthermore, Bellen and colleagues devised a new transformation technology that permits site-specific integration of very large DNA fragments,<ref>{{cite journal|vauthors=Venken KJ, He Y, Hoskins RA, Bellen HJ|title=P[acman]: a BAC transgenic platform for targeted insertion of large DNA fragments in D. melanogaster|journal=Science|date=2006|volume=314|pages=1747–51|pmid=17138868|doi=10.1126/science.1134426}}</ref> which led to the generation of a collection of flies carrying molecularly defined duplications for more than 90% of the Drosophila X-chromosome.<ref>{{cite journal|vauthors=Venken KJ, Popodi E, Holtzman SL, Schulze KL, Park S, Carlson JW, Hoskins RA, Bellen HJ, Kaufman TC|title=A molecularly defined duplication set for the X chromosome of Drosophila melanogaster|journal=Genetics|date=2010|volume=186|pages=1111–25|pmid=20876565|doi=10.1534/genetics.110.121285|pmc=2998297}}</ref> Hundreds of Drosophila researchers utilize this collection. Most recently his lab created a new [[transposable element]] (MiMIC)<ref>{{cite journal|vauthors=Venken KJ, Schulze KL, Haelterman NA, Pan H, He Y, Evans-Holm M, Carlson JW, Levis RW, Spradling AC, Hoskins RA, Bellen HJ|title=MiMIC: a highly versatile transposon insertion resource for engineering Drosophila melanogaster genes|journal=Nat Methods|date=2011|volume=8|pages=737–43|pmid=21985007|doi=10.1038/nmeth.1662|pmc=3191940}}</ref> that permits even more downstream manipulations via RMCE ([[recombinase-mediated cassette exchange]]), such as protein tagging and knockdown <ref>{{cite journal|vauthors=Nagarkar-Jaiswal S, Lee PT, Campbell ME, Chen K, Anguiano-Zarate S, Gutierrez MC, Busby T, Lin WW, He Y, Schulze KL, Booth BW, Evans-Holm M, Venken KJ, Levis RW, Spradling AC, Hoskins RA, Bellen HJ|title=A library of MiMICs allows tagging of genes and reversible, spatial and temporal knockdown of proteins in Drosophila|journal=eLife|date=2015|volume=4|page=e05338|pmid=25824290|doi=10.7554/elife.05338|pmc=4379497}}</ref><ref>{{cite journal|vauthors=Nagarkar-Jaiswal S, DeLuca SZ, Lee PT, Lin WW, Pan H, Zuo Z, Lv J, Spradling AC, Bellen HJ|title=A genetic toolkit for tagging intronic MiMIC containing genes|journal=eLife|date=2015|volume=4|page=e08469|pmid=26102525|doi=10.7554/elife.08469|pmc=4499919}}</ref> and large scale [[homologous recombination]].  His research constantly evolves with the changing technology to meet the needs of the Drosophila community.

===Neurotransmitter release===

Dr. Bellen has made numerous important contributions in the field of [[synaptic transmission]] in Drosophila. Through unbiased forward genetic screens designed to detect perturbations in neuronal function, he has uncovered many genes involved in synaptic transmission and has used [[reverse genetics]] to help to establish their function. His lab was the first to provide ''in vivo'' evidence that [[Synaptotagmin 1]] functions as the main Calcium sensor in synaptic transmission <ref>{{cite journal|vauthors=Littleton JT, Stern M, Schulze K, Perin M, Bellen HJ|title=Mutational analysis of Drosophila synaptotagmin demonstrates its essential role in Ca(2+)-activated neurotransmitter release|journal=Cell|date=1993|volume=74|pages=1125–34|pmid=8104705|doi=10.1016/0092-8674(93)90733-7}}</ref> and that [[STX1A|Syntaxin-1A]] plays a critical role in synaptic vesicle (SV) fusion ''in vivo''.<ref>{{cite journal|vauthors=Schulze KL, Broadie K, Perin MS, Bellen HJ|title=Genetic and electrophysiological studies of Drosophila syntaxin-1A demonstrate its role in nonneuronal secretion and neurotransmission|journal=Cell|date=1995|volume=80|pages=311–20|pmid=7834751|doi=10.1016/0092-8674(95)90414-x}}</ref> His lab showed that Endophilin<ref>{{cite journal|vauthors=Verstreken P, Kjaerulff O, Lloyd TE, Atkinson R, Zhou Y, Meinertzhagen IA, Bellen HJ|title=Endophilin mutations block clathrin-mediated endocytosis but not neurotransmitter release|journal=Cell|date=2002|volume=109|pages=101–12|pmid=11955450|doi=10.1016/s0092-8674(02)00688-8}}</ref> and [[Synaptojanin]]<ref>{{cite journal|vauthors=Verstreken P, Koh TW, Schulze KL, Zhai RG, Hiesinger PR, Zhou Y, Mehta SQ, Cao Y, Roos J, Bellen HJ|title=Synaptojanin is recruited by endophilin to promote synaptic vesicle uncoating|journal=Neuron|date=2003|volume=40|pages=733–48|pmid=14622578|doi=10.1016/s0896-6273(03)00644-5}}</ref> control uncoating of SVs, that the V0 component of the [[v-ATPase]] affects SV fusion,<ref>{{cite journal|vauthors=Hiesinger PR, Fayyazuddin A, Mehta SQ, Rosenmund T, Schulze KL, Zhai RG, Verstreken P, Cao Y, Zhou Y, Kunz J, Bellen HJ|title=The v-ATPase V0 subunit a1 is required for a late step in synaptic vesicle exocytosis in Drosophila|journal=Cell|date=2005|volume=121|pages=607–20|pmid=15907473|doi=10.1016/j.cell.2005.03.012|pmc=3351201}}</ref> that synaptic mitochondria control SV dynamics,<ref>{{cite journal|vauthors=Verstreken P, Ly CV, Venken KJ, Koh TW, Zhou Y, Bellen HJ|title=Synaptic mitochondria are critical for mobilization of reserve pool vesicles at Drosophila neuromuscular junctions|journal=Neuron|date=2005|volume=47|pages=365–78|pmid=16055061|doi=10.1016/j.neuron.2005.06.018}}</ref> and in addition discovered a novel calcium channel involved in SV biogenesis.<ref>{{cite journal|vauthors=Yao CK, Lin YQ, Ly CV, Ohyama T, Haueter CM, Moiseenkova-Bell VY, Wensel TG, Bellen HJ|title=A synaptic vesicle-associated Ca2+ channel promotes endocytosis and couples exocytosis to endocytosis|journal=Cell|date=2009|volume=138|pages=947–60|pmid=19737521|doi=10.1016/j.cell.2009.06.033|pmc=2749961}}</ref> His pioneering work on synaptic vesicle trafficking molecules was later confirmed in the mouse.

===Neuronal Development===

Bellen and colleagues made important contributions to our understanding of Drosophila peripheral nervous system development and the fine-tuning of aspects of [[Notch signaling]] during this process. These discoveries were made by carrying out multiple forward genetic screens using the mutagen, [[ethyl methane sulfonate]], as well as ''P'' elements. They discovered the protein Senseless<ref>{{cite journal|vauthors=Nolo R, Abbott LA, Bellen HJ|title=Senseless, a Zn-finger transcription factor, is necessary and sufficient for sensory organ development in Drosophila|journal=Cell|date=2000|volume=102|pages=349–62|pmid=10975525|doi=10.1016/s0092-8674(00)00040-4}}</ref> that is required for the development of the peripheral nervous system by boosting the action of [[Proneural genes|proneural]] proteins and suppressing the action of Enhancer of split proteins.<ref>{{cite journal|vauthors=Jafar-Nejad H, Acar M, Nolo R, Hacin H, Pan H, Parkhurst SM, Bellen HJ|title=Senseless acts as a binary switch during sensory organ precursor selection|journal=Genes Dev|date=2003|volume=17|pages=2966–78|pmid=14665671|doi=10.1101/gad.1122403|pmc=289154}}</ref> They also discovered the protein Rumi<ref>{{cite journal|vauthors=Acar M, Jafar-Nejad H, Takeuchi H, Rajan A, Ibrani D, Rana NA, Pan H, Haltiwanger RS, Bellen HJ|title=Rumi is a CAP10 domain glycosyltransferase that modifies Notch and is required for Notch signaling|journal=Cell|date=2008|volume=132|pages=247–58|pmid=18243100|doi=10.1016/j.cell.2007.12.016|pmc=2275919}}</ref> and determined it was required for [[O-glycosylation]] of Notch at many different sites and that it these sites affect the cleavage of Notch at the membrane. Their research also uncovered a critical amino acid of the Notch protein that modulates its binding with Serrate.<ref>{{cite journal|vauthors=Yamamoto S, ((Charng W-L)), Rana NA, Kakuda S, Jaiswal M, Bayat V, Xiong B, Zhang K, Sandoval H, David G, Wang H, Haltiwanger RS, Bellen HJ|title=A mutation in EGF repeat 8 of Notch discriminates between Serrate/Jagged and Delta family ligands|journal=Science|date=2012|volume=338|pages=1229–32|pmid=23197537|doi=10.1126/science.1228745|pmc=3663443}}</ref>  Finally, they helped elucidate the role of several other proteins in Notch signaling, including the role of Wasp/Arp2/3,<ref>{{cite journal|vauthors=Rajan A, Tien AC, Haueter CM, Schulze KL, Bellen HJ|title=The Arp2/3 complex and WASp are required for apical trafficking of Delta into microvilli during cell fate specification of sensory organ precursors|journal=Nat Cell Biol|date=2009|volume=11|pages=815–24|pmid=19543274|doi=10.1038/ncb1888|pmc=3132077}}</ref> Sec15,<ref>{{cite journal|vauthors=Jafar-Nejad H, Andrews HK, Acar M, Bayat V, Wirtz-Peitz F, Mehta SQ, Knoblich JA, Bellen HJ|title=Sec15, a component of the exocyst, promotes Notch signaling during the asymmetric division of Drosophila sensory organ precursors|journal=Dev Cell|date=2005|volume=9|pages=351–63|pmid=16137928|doi=10.1016/j.devcel.2005.06.010}}</ref> Tempura,<ref>{{cite journal|vauthors=Charng WL, Yamamoto S, Jaiswal M, Bayat V, Xiong B, Zhang K, Sandoval H, David G, Gibbs S, Lu HC, Chen K, Giagtzoglou N, Bellen HJ|title=Drosophila Tempura, a novel protein prenyltransferase α subunit, regulates Notch signaling via Rab1 and Rab11|journal=PLoS Biol|date=2014|volume=12|page=e1001777|pmid=24492843|doi=10.1371/journal.pbio.1001777|pmc=3904817}}</ref> and EHBP-1<ref>{{cite journal|vauthors=Giagtzoglou N, Yamamoto S, Zitserman D, Graves HK, Schulze KL, Wang H, Klein H, Rogiers F, Bellen HJ|title=dEHBP1 controls exocytosis and recycling of Delta during asymmetric divisions|journal=J Cell Biol|date=2012|volume=196|pages=65–83|pmid=22213802|doi=10.1083/jcb.201106088|pmc=3255984}}</ref> in Delta processing and signaling.

{{Authority control}}

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using<ref></ref> tags, these references will then appear here automatically -->
{{Reflist}}

== External links ==
Bellen Lab website [http://flypush.imgen.bcm.tmc.edu]

BCM Graduate Program in Developmental Biology [http://db-bcm.org]

Gene Disruption Project website [http://flypush.imgen.bcm.tmc.edu/pscreen]

P[acman] Recombineering resources website [http://pacmanfly.org]

FlyBase [http://flybase.org]

{{DEFAULTSORT:Bellen, Hugo J.}}
[[Category:Baylor College of Medicine faculty]]
[[Category:American geneticists]]
[[Category:Howard Hughes Medical Investigators]]
[[Category:1953 births]]
[[Category:Living people]]
[[Category:University of California, Davis alumni]]