{{Italic title}}
{{refimprove|date=January 2017}}
The '''''Columbia Journal of Transnational Law''''' ([[Bluebook]] abbreviation: Colum. J. Transnat'l L.) is a [[law review]] established in 1961 focusing on issues of [[international law]]. It is produced by students at [[Columbia University School of Law]].  The journal is highly regarded in legal academia, having received an "A-" grade in the 2011 Siemslegal World Law Journal Ranking,<ref>{{cite web|title=World Law Journal Ranking 2011|url=http://siemslegal.blogspot.com/2011/03/world-law-journal-ranking-2011.html|publisher=Siemslegal}}</ref> the 2nd highest rating among the 14 journals at [[Columbia Law School]].

== History ==
The ''Columbia Journal of Transnational Law'', originally named the ''Bulletin of the Columbia Society of International Law'', was created by [[Wolfgang G. Friedmann]] and a group of Columbia law students belonging to the Columbia Society of International Law. The first volume, containing two issues, was a forum for the informal discussion of international legal questions; the second volume, published in 1963 under the title ''International Law Bulletin'', aspired to the tradition of the scholarly law review.

During its second decade, the journal expanded publication to three issues per year, experimented with theme issues and published some of the early proceedings of the Friedmann Conference held annually at Columbia Law School. By the beginning of its third decade, the journal’s theme issues—entire issues dedicated to the examination of current international law problems—had become regular publications. These topical issues have examined international taxation, international trade embargoes and boycotts, China’s legal development, sovereign debt rescheduling, socialist law and international satellite communications.

== Organizational structure and staff ==
The ''Columbia Journal of Transnational Law'' is published by The Columbia Journal of Transnational Law Association, Inc., a New York corporation since 1969. The corporation is overseen by a [[board of directors]] of 18 members. The journal is further assisted by a board of advisors consisting of 11 members. The 2012-2013 editorial staff consists of 23 student editors and 55 staff members.

== Citations ==
The journal has been cited by the [[Supreme Court of the United States]],<ref>{{cite web|title=Morrison v. Nat'l Australia Bank Ltd., 561 U.S. 247, 260 (2010)|url=http://www.leagle.com/decision/In%20SCO%2020100624F37.xml/MORRISON%20v.%20NATIONAL%20AUSTRALIA%20BANK%20LTD.|website=Leagle|publisher=Supreme Court of the United States}}</ref><ref>{{cite web|title=Exxon Shipping Co. v. Baker, 554 U.S. 471, 496 (2008)|url=http://www.supremecourt.gov/opinions/07pdf/07-219.pdf|website=Supreme Court of the United States}}</ref><ref>{{cite web|title=Verlinden B.V. v. Cent. Bank of Nigeria, 461 U.S. 480 (1983)|url=https://supreme.justia.com/cases/federal/us/461/480/case.html|website=Justia|publisher=Supreme Court of the United States}}</ref> the [[United States Court of Appeals for the First Circuit|First Circuit Court of Appeals]],<ref>{{cite web|title=Quaak v. Klynveld Peat Marwick Goerdeler Bedrijfsrevisoren, 361 F.3d 11, 18 (1st Cir. 2004)|url=http://law.justia.com/cases/federal/appellate-courts/F3/361/11/581974/|website=Justia|publisher=United States Court of Appeals for the First Circuit}}</ref> the [[United States Court of Appeals for the Second Circuit|Second Circuit Court of Appeals]],<ref>{{cite web|title=NML Capital, Ltd. v. Banco Cent. de la Republica Argentina, 652 F.3d 172, 189 (2d Cir. 2011)|url=https://casetext.com/case/nml-capital-ltd-v-bcra|publisher=United States Court of Appeals for the Second Circuit}}</ref><ref>{{cite web|title=ITT World Commc'ns, Inc. v. F.C.C., 595 F.2d 897, 900 (2d Cir. 1979)|url=http://openjurist.org/595/f2d/897/itt-world-communications-inc-rca-v-federal-communications-commission|publisher=United States Court of Appeals for the Second Circuit}}</ref><ref>{{cite web|title=United States v. Amer, 110 F.3d 873, 881 (2d Cir. 1997)|url=http://openjurist.org/110/f3d/873|publisher=United States Court of Appeals for the Second Circuit}}</ref> and numerous other federal appellate and district courts.

== Wolfgang Friedmann Memorial Award ==
The Wolfgang Friedmann Memorial Award is presented annually to an individual who has made outstanding contributions to the field of international law. The award is given in memory of the journal’s founder. Past recipients of the award include Senators [[George J. Mitchell]] and [[Daniel Patrick Moynihan]], Justice [[Sandra Day O'Connor]], [[Giuliano Amato]], [[Louis Henkin]], [[Hans Blix]], and [[Boutros Boutros-Ghali]]. The 43rd Annual Wolfgang Friedmann Memorial Award will be presented to Associate Justice of the Supreme Court of the United States [[Stephen Breyer]], on April 12, 2017.

{{Columbia}}

==References==
{{Reflist}}

[[Category:American law journals]]
[[Category:Triannual journals]]
[[Category:Publications established in 1961]]
[[Category:English-language journals]]