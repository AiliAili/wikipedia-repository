{{Infobox prime minister
| honorific-prefix = [[Italian honorifics#State rules|The Honourable]]
| name = Maria Rosaria "Mara" Carfagna
| image = Maria Rosaria Carfagna daticamera.jpg
| order = Italian Minister for Equal Opportunity
| term_start = May 8, 2008
| term_end = November 16, 2011
| predecessor = [[Barbara Pollastrini]]
| successor = [[Elsa Fornero]]
| primeminister = [[Silvio Berlusconi]]
| birth_date = {{Birth date and age|mf=yes|1975|12|18}}
| birth_place = [[Salerno]], Italy
| nationality = Italian
| religion = [[Roman Catholic Church|Roman Catholic]]<ref>{{cite web|url=http://www.maracarfagna.net/interviste/panorama1912.pdf |title=Ebbene sì, sono una Bondi girl |last=Liuzzo |first=Romana |date=2007-12-19 |publisher=''[[Panorama (Italian magazine)|Panorama]]'' |accessdate=2008-05-09 |language=it |deadurl=yes |archiveurl=https://web.archive.org/web/20090106150548/http://www.maracarfagna.net/interviste/panorama1912.pdf |archivedate=January 6, 2009 }}</ref>
| alma_mater = [[University of Salerno]]
| residence =
| profession =
| party = [[Forza Italia (2013)|Forza Italia]]<br><small>(2013–present)</small><br> [[The People of Freedom]]<br><small>(2009–2013)</small><br>[[Forza Italia]]<br><small>(2004–2009)</small>
| spouse =
| children =
|constituency2 = [[Campania|Campania 2]] 
|office2 = Member of the <br> [[Italian Chamber of Deputies]]
|term_start2 = April 28, 2006
|term_end2 =
|}}

'''Maria Rosaria "Mara" Carfagna''' (born December 18, 1975) is an [[Italy|Italian]] politician and former [[showgirl]] and model. After obtaining a degree in law,<ref>[http://www.independent.ie/world-news/europe/berlusconi-blitzes-back-to-power-with-old-friends-and-a-showgirl-1370120.html Berlusconi blitzes back to power with old friends and a showgirl]</ref> Carfagna worked for several years on Italian television shows and as a model. She later entered politics and was elected to the [[Chamber of Deputies]] for [[Forza Italia]] party in 2006. From 2008 to 2011 she served as Minister for Equal Opportunity in [[Berlusconi IV Cabinet]]. Carfagna had been named "the most beautiful minister in the world",<ref name="Bild">{{Cite news|url=http://www.bild.de/BILD/news/politik/2008/05/08/berlusconis-heisse/familienministerin,geo=4483258.html|archive-url=https://web.archive.org/web/20081204004246/http://www.bild.de:80/BILD/news/politik/2008/05/08/berlusconis-heisse/familienministerin,geo=4483258.html|dead-url=yes|archive-date=2008-12-04|title=Berlusconi hat die schönste Ministerin: Mara Carfagna |date=2008-05-09|publisher=''[[Bild]]''|accessdate=2008-05-10|language=de}} </ref><ref>{{Cite news|url=http://www.ilgiornale.it/a.pic1?ID=260204|title=Mara Carfagna? È il ministro più bello del mondo|last=Veronese|first=Massimo M.|date=2008-05-09|publisher=''[[Il Giornale]]''|accessdate=2008-05-10|language=it}}</ref> and was ranked number one on [[Maxim (magazine)|''Maxim'']]'s "World´s Hottest Politicians".<ref>[http://www.maxim.com/slideshow/girls-of-maxim/14682?slide=8]</ref> Spokeswoman of the parliamentary group of Forza Italian at  Chamber of Deputies.<ref>http://www.forza-italia.it</ref> In the last municipal elections in Naples was the most voted councilor in Italy, with more than six thousand personal preferences.<ref>http://www.corriere.it/politica/cards/elezioni-comunali-2016-candidati-preferenze-boom-maria-stella-gelmini-flop-simona-tagli-altri-vip/mara-carfagna.shtml
</ref>

==Background==
Carfagna was born in [[Salerno]], where she attended the [[Liceo scientifico]] ''[[John of Procida|Giovanni da Procida]]''.<ref name="MC">{{cite web|url=http://www.maracarfagna.net/index.php?option=com_content&task=view&id=14&Itemid=26|title=Mara Carfagna - Chi Sono|last=Carfagna|first=Mara|publisher=Mara Carfagna|accessdate=2008-05-09|language=it}}</ref> In 2001 she graduated in [[law]] from the [[University of Salerno]], with a [[thesis]] on information law and [[broadcasting]] systems.<ref name="MC"/>

==Career as showgirl and model==
After having studied dance and piano, she participated in the [[Miss Italia|Miss Italy]] contest in 1997, finishing in sixth place.<ref name="MC"/> About the experience she later said: "That competition makes you as a woman, it matures you...all that stress, that desire to win, it makes you understand who you are."<ref name="DT Aus">{{Cite news|url=http://www.news.com.au/dailytelegraph/story/0,22049,23671053-5001021,00.html|title=Mara Carfagna is Italy's sexiest minister|last=Pisa|first=Nick|date=2008-05-09|publisher=''[[The Daily Telegraph (Australia)]]''|accessdate=2008-05-09}}</ref>

Later she started working in television for the company [[Mediaset]], controlled by the family of [[Silvio Berlusconi]].<ref name="DM">{{cite web|url=http://www.dailymail.co.uk/pages/live/articles/news/worldnews.html?in_article_id=564760&in_page_id=1811|title=Former topless model joins Berlusconi's cabinet as Italy's equalities minister|last=Pisa|first=Nick|date=2008-05-09|publisher=[[Daily Mail]]|accessdate=2008-05-09}}</ref> From 2000 to 2006 she participated as a [[showgirl]] in the television program ''La domenica del villaggio'' ("Sunday in the Village") with [[Davide Mengacci]]. In 2006 she led the program ''Piazza grande'' ("Main Square") together with [[Giancarlo Magalli]].<ref>{{it icon}} [http://www.corriere.it/Primo_Piano/Politica/2006/03_Marzo/08/cavalli.shtml Mara Carfagna, pasionaria azzurra], corriere.it, 30-01-2007.</ref> Carfagna has also been part of the television programs ''I cervelloni'', ''Vota la voce'' and  ''[[Domenica in]]''.<ref>{{it icon}} [http://gossip.excite.it/news/2014/Mara-Carfagna Mara Carfagna], gossip.excite.it, 30-04-2004. {{webarchive |url=https://web.archive.org/web/20080510192204/http://gossip.excite.it/news/2014/Mara-Carfagna |date=May 10, 2008 }}</ref>

Mara Carfagna has in the past posed [[Nude photography|nude]] for magazine named ''[[Maxim (magazine)|Maxim]]''.<ref>{{cite web|url=http://stylemens.typepad.com/photos/uncategorized/2008/05/09/maxim_v.jpg|title=Maxim cover|accessdate=2008-05-10}}</ref> Reluctant to talk about her modeling past, she has nevertheless suggested that she had certain reservations about the work. On one occasion she said: "I am a bit of a prude and I found getting undressed in front of a camera not a pleasant experience."<ref name="DM"/> She states that she is a firm believer in family values, and claimed in an interview that she once refused to take part in a movie directed by erotic filmmaker [[Tinto Brass]], when she was twenty years old.<ref>{{it icon}} [http://www.tgcom.mediaset.it/gossip/articoli/articolo299960.shtml Rubino,Fiorella all'occhiello di FI], tgcom.mediaset.it, 09-03-2006.</ref>

==Political career==
[[File:Napolitano Carfagna.jpg|thumb|left|200xp|Mara Carfagna and the Minister of Youth Policy [[Giorgia Meloni]] with the [[President of the Italian Republic]] [[Giorgio Napolitano]] in 2009]]
Carfagna entered politics in 2004, and became responsible for the women's movement in the political party [[Forza Italia]] (presently [[The People of Freedom]]).<ref name="MC"/><ref>{{it icon}} [http://www.lastampa.it/redazione/cmsSezioni/politica/200710articoli/26836girata.asp "Silvio premia Mara: veleni rosa in Fi"], lastampa.it, 20-10-2007.</ref> In the [[Italian general election, 2006|elections of 2006]] she was elected into the [[Italian Chamber of Deputies|Chamber of Deputies]] for Forza Italia, and in the [[Italian general election, 2008|2008 elections]] – running as the third candidate from The People of Freedom in the district "[[Campania]] 2" – she was reelected.<ref>{{it icon}} [http://www.corriere.it/Primo_Piano/Politica/2006/05_Maggio/31/carfagna.shtml Quote rosa, lite Carfagna-Prestigiacomo], corriere.it, 31-05-2006.</ref> When she first entered parliament Berlusconi jokingly commented that Forza Italia practiced the law of [[Droit du seigneur|primae noctis]]; the right of a [[Feudalism|feudal]] lord to take the [[virginity]] of his female subjects.<ref>{{Cite news|url=http://www.timesonline.co.uk/tol/news/politics/article3970224.ece|title=Mara Carfagna: showing her slip|last=Owen|first=Richard|date=2008-05-21|publisher=The Times|accessdate=2008-06-05 | location=London}}</ref> As a deputy she was secretary of the Commission for Constitutional Affairs,<ref name="MC"/> and has been described as a diligent, hard-working parliamentarian.<ref>{{Cite news|url=http://ricerca.repubblica.it/repubblica/archivio/repubblica/2008/05/09/039tendenza.html|title=Tendenza Carfagna|last=Ceccarelli|first=Filippo|date=2008-05-09|publisher=[[La Repubblica]]|accessdate=2008-06-07|language=it}}</ref> On May 8, 2008 she was appointed Minister for Equal Opportunity,<ref>This is a ministry without portfolio: {{cite web|url=http://www.governo.it/Governo/Ministeri/ministri_gov.html|title=I Ministri del Governo Berlusconi IV |publisher=The Italian Government|accessdate=2008-05-14|language=it}}</ref> in the [[Berlusconi IV Cabinet|fourth cabinet]] of [[Silvio Berlusconi]], an appointment that was widely publicised internationally, with focus on her special background.<ref name="Bild"/><ref name="DT Aus"/><ref name="DT">{{Cite news|url=http://www.telegraph.co.uk/news/worldnews/europe/italy/1936102/Silvio-Berlusconi-apoints-former-showgirl-to-cabinet.html|title=Berlusconi apoints<!--SIC--> former showgirl to cabinet|last=Moore|first=Malcolm|date=2008-05-08|publisher=''The Daily Telegraph''|accessdate=2008-05-09 | location=London}}</ref>

Carfagna has been vocal on certain issues, such as the level of crime in her home town of Salerno, after having herself been the victim of burglary on three different occasions.<ref>{{cite web|url=http://lacittadisalerno.repubblica.it/dettaglio/Salerno-Mara-Carfagna-lancia-lallarme-Ho-subito-tre-furti-altro-che-citta-sicura/1389845|title=Salerno, Mara Carfagna lancia l'allarme: "Ho subito tre furti, altro che città sicura"|last=Giannattasio|first=Gianni|date=2007-11-19|publisher=La Città di Salerno|accessdate=2008-05-09|language=it}}</ref> She describes herself as an [[Antifeminism|antifeminist]], as she believes that "liberty" depends not on independence, but on rules and discipline.<ref>{{cite web|url=http://www.maracarfagna.net/interviste/tempi2709.pdf |title=Mara Carfagna |last=Piccinini |first=Pietro |date=2007-09-27 |publisher=[[Il Giornale]] |accessdate=2008-05-09 |language=it |deadurl=yes |archiveurl=https://web.archive.org/web/20080807135722/http://www.maracarfagna.net/interviste/tempi2709.pdf |archivedate=August 7, 2008 }}</ref> She opposes [[gay marriage]], and says that matrimonial rights should be tied to reproduction.<ref>{{cite web|last=Signore|first=Adalberto|title=Gli italiani non vogliono questi finti matrimoni|url=http://www.ilgiornale.it/interni/gli_italiani_non_vogliono_questi_finti_matrimoni/31-01-2007/articolo-id=153309-page=0-comments=1|publisher=[[Il Giornale]]|accessdate=18 September 2010|language=Italian|date=31 January 2007}}</ref>  
Soon after her accession she refused to back a [[gay pride]] march, arguing that discrimination was no longer a problem for homosexuals in Italy because the homophobia was just a thinking offence, a statement that was strongly criticised by gay rights groups.<ref>{{Cite news|url=http://www.iht.com/articles/2008/05/19/europe/italy.php|title=Italian equal opportunities minister rejects 'gay pride' march|date=2008-05-19|publisher=International Herald Tribune|accessdate=2008-06-05}}</ref>

In September 2008, Carfagna introduced proposal for a new law making [[street prostitution]] a crime, with fines for both clients and prostitutes. The bill was her first major initiative as a minister. She said that at present in Italy, "as in the great majority of Western countries", brothels and the exploitation of prostitutes by pimps were illegal but prostitution as such was not. She described street prostitution as a "shameful phenomenon".<ref name="new law">{{Cite news|title=Former showgirl Mara Carfagna comes under fire for anti-prostitution law |url=http://www.timesonline.co.uk/tol/news/world/europe/article4733570.ece |publisher=''[[The Times]]'' |date=September 12, 2008 |accessdate=2008-09-15 | location=London | first=Richard | last=Owen}}</ref> Carfagna was criticized by prostitutes' representatives and other charities for introducing the bill. However, some Catholic charities praised her for having the courage to "take on prostitution as a serious social evil".<ref name="new law"/>

In 2009 she became the first political promoter of the law against stalking offence. This law was finally approved on 23 February 2009, introduced as a package of bills known as the ''Decreto Maroni''. In the same year she signed a campaign against homophobia in Italy, with television spots, images on magazines and wall attachments on cities.<ref>{{Cite news|url=http://blog.panorama.it/foto/2009/11/10/mara-carfagna-lancia-la-campagna-contro-lomofobia|title=Italian equal opportunities minister starts the "Antiomofobia" campaign|date=2009-11-10|publisher=[[Panorama]]|accessdate=2011-04-03}}</ref> She also proposed a bill against homophobia, in which homophobia was considered as an aggravating circumstance in bullying events. This bill was next refused by the Parliament.

He has participated in many international conferences, met the UN Secretary General, has intervened four times to the General Assembly, where he promoted an international moratorium against FGM. He organized the first international conference on violence against women in the context of the G8, which was held in the city of L'Aquila, in Italy, in July 2009.<ref>http://www.ilgiornale.it/news/stupro-contro-i-diritti-umani.html</ref>

In 2010 during political debate for the 8th March celebration she claimed that women gained the right to vote in Italy in 1960 (while they did in 1946) and that the law that rules intrahousehold relationship was reformed in 1970 (while it was in 1975)<ref>{{Cite news|url=http://antefatto.ilcannocchiale.it/glamware/blogs/blog.aspx?id_blog=96578&id_blogdoc=2453338&title=2453338 |deadurl=yes |archiveurl=https://web.archive.org/web/20100729150532/http://antefatto.ilcannocchiale.it/glamware/blogs/blog.aspx?id_blog=96578 |archivedate=July 29, 2010 }}</ref>
It has passed a law that provides for quotas for women on the boards of companies, which has allowed to involve a larger number of women in the Italian economic system. It approved funding for childcare facilities and in support of motherhood and family that made it possible to increase by a few percentage points the availability of places for working mothers.<ref>
http://www.ilfattoquotidiano.it/2011/06/28/le-quote-rosa-nei-cda-sono-legge-si-bipartisan-alla-camera-carfagna-decisione-storica/129959/</ref>

==Controversies==
[[File:Paweł Rogaliński, Nyamko Sabuni, Mara Carfagna.JPG|thumb|left|200xp|[[Paweł Rogaliński]], [[Nyamko Sabuni]] and Mara Carfagna during the Third Equality Summit in Stockholm in 2009.]]
[[File:Carfagna Catania Pride.jpg|thumb|Gay pride protest against Carfagna in [[Catania]], 2008.]]
In January 2007, Carfagna was at the center of a controversy that received international attention. On the evening of the [[Telegatto]] award show, Berlusconi said about Carfagna that "If I was not already married I would have married her immediately". The comment caused Berlusconi's wife, [[Veronica Lario]], to demand an apology through a national newspaper, something which she also received.<ref name="DT Aus"/> Carfagna herself has later described the comment as "gallant and harmless," and said that she did not quite understand Lario's reaction.<ref name="danza">{{Cite news|url=http://www.ilgiornale.it/a.pic1?ID=237117|title=Carfagna: nella politica ho la stessa disciplina che dedicavo alla danza|last=Perna|first=Giancarlo|date=2008-01-28|publisher=[[Il Giornale]]|accessdate=2008-05-10|language=it}}</ref>

On July 5, 2008, the [[Argentina|Argentine]] journal ''[[Clarín (Argentine newspaper)|Clarín]]'' reported about telephone wiretap records authorized for an anti-corruption investigation. Reporter Julio Algañaraz wrote that Carfagna and [[Silvio Berlusconi]] engaged in a telephone conversation with explicit allusions to [[oral sex]].<ref>{{cite web
|url=http://www.clarin.com/diario/2008/07/05/elmundo/i-01708762.htm
|title=Sexgate a la italiana: el escándalo salpica a Berlusconi y una ministra
|language=Spanish
|accessdate=07-08-2009
|editor=Clarín
|editor-link=Clarín (Argentine newspaper)
}}</ref> The wiretap transcripts have not been published, but the Italian newspaper ''[[La Repubblica]]'' interviewed the former vice-minister of Foreign Affairs in the [[Berlusconi II Cabinet]] and socialist executive [[Margherita Boniver]], who admitted the existence of some messages. She later apologized to Mara Carfagna and the case was closed. Carfagna has instead sued for libel newspapers which had mentioned the alleged conversations and had shown some offensive statements. Following the reports, October 9, 2012 the Civil Court of Rome condemned Sabina Guzzanti to compensation of 40,000 euro to the former minister, ensuring, as all authorities, that those conversations never existed.<ref>http://www.ilgiornale.it/news/cronache/sabina-guzzanti-diffam-carfagna-condannata-risarcimento-845053.html</ref><ref>
{{cite web
|language = Italian
|author = Carmelo Lopapa
|url = http://ricerca.repubblica.it/repubblica/archivio/repubblica/2008/07/02/quelle-telefonate-stanno-per-uscire-alla-camera.html
|title = Quelle telefonate stanno per uscire Alla Camera l'incubo Grande Fratello
|publisher = [[La Repubblica]]
|date = 07-02-2008
|page = 6
|accessdate = 2008-10-16
}}</ref><ref>http://www.ilsole24ore.com/art/notizie/2012-10-09/sabina-guzzanti-dovra-pagare-153812.shtml?uuid=AbpYUzqG</ref>

==References==
{{Reflist|30em}}

==External links==
* [https://web.archive.org/web/20080512124602/http://www.governo.it/Governo/Biografie/ministri/Carfagna_Mariarosaria.html Ministry biography]
* [http://www.maracarfagna.net/ Personal blog]
* [http://www.huffingtonpost.com/2009/07/08/mara-carfagna-former-topl_n_228052.html Mara Carfagna] - slideshow and video by ''The Huffington Post''

{{S-start}}
{{S-off}}
{{S-bef|before=[[Barbara Pollastrini]]}}
{{S-ttl|title=Italian Minister of Equal Opportunities|years=2008–2011}}
{{S-aft|after=[[Elsa Fornero]]}}
{{s-par|it-lwr}}
{{S-bef|before=Title jointly held}}
{{S-ttl|title=[[Italian Chamber of Deputies|Member of the Italian Chamber of Deputies]] <br> Legislatures <br>XV, XVI, XVII|years=2006–present}}
{{S-inc}}
{{S-end}}

{{Berlusconi IV Cabinet}}

{{Good article}}

{{Authority control}}

{{DEFAULTSORT:Carfagna, Mara}}
[[Category:1975 births]]
[[Category:Living people]]
[[Category:People from Salerno]]
[[Category:Italian female models]]
[[Category:The People of Freedom politicians]]
[[Category:21st-century Italian politicians]]
[[Category:Forza Italia politicians]]
[[Category:Italian women in politics]]
[[Category:Italian Roman Catholics]]
[[Category:Government ministers of Italy]]
[[Category:University of Salerno alumni]]
[[Category:Women government ministers]]
[[Category:21st-century women politicians]]