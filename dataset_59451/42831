<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 | name= Model 2A Flivver
 | image=Ford Flivver Replica.jpg
 | caption=Ford Flivver replica
}}{{Infobox aircraft type
 | type=[[Light aircraft]]
 | national origin=United States
 | manufacturer=[[Stout Metal Airplane Division of the Ford Motor Company]]
 | designer=[[Otto C. Koppen]]
 | first flight=
 | introduced=1927
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=5
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Ford Flivver''' was a single-seat aircraft introduced by [[Henry Ford]] as the "Model T of the Air".{{#tag:ref|Henry Ford also called the Ford Flivver a "Sky Flivver", again building on the connection to the ground-breaking Model T, which was commonly called a "flivver."<ref>Sillery, Bob. [http://www.popsci.com/military-aviation-space/article/2001-12/looking-back-henry-fords-flivver "Looking back at Henry Ford's Flivver: A plane-car for the man of average means."] ''Popular Science,'' December 17, 2001.</ref>|group=N}} After a fatal crash of a prototype into the ocean off [[Melbourne, Florida]], U.S., production plans were halted.
{{TOC limit|limit=2}}

==Development==
The [[Ford Trimotor]] was Henry Ford's first successful commercial aircraft venture in 1925. Following the [[Ford Model T]] as an "everyman's" vehicle, the Ford Flivver was designed to be a mass-produced "everyman's" aircraft.<ref>[http://www.thehenryford.org/exhibits/heroes/entrepreneurs/flivver.asp "Entrepreneurs: The Planes; 1926 Ford Flivver."] ''The Henry Ford Museum.'' Retrieved: August 4, 2012.</ref> The idea was first proposed to [[William Bushnell Stout]], manager of Ford's acquired aircraft division in 1926. Both Stout and [[William Benson Mayo]], head of [[Stout Metal Airplane Division of the Ford Motor Company|Ford's Aircraft Division]] wanted nothing to do with the aircraft and it was built in a nearby museum building in the Ford Laboratories.<ref>Ford 1997, pp. 168–169.</ref>
[[File:Ford Flivver aircraft.jpg|thumb|left|Ford Flivver c. 1927. Note: Full-span ailerons.]]
The single-seat aircraft was designed with Mr. Ford's instructions that it "fit in his office".<ref name="Trex"/> The first example was displayed at the 1926 [[Ford National Reliability Air Tour]].<ref>Pauley, Robert F. "The Ford Flivver." ''Sport Aviation,'' July 1961.</ref> The press and public flocked to see "Ford's Flying Car," a single-seat aircraft that had very little in common with the popular Model T "Flivver." Comedian [[Will Rogers]] posed for press photos in the aircraft (although he never flew one).<ref>Davis and Wagner 2002, p. 49.</ref> A ''New York Evening Sun'' columnist wrote the following poem showing excitement for the future flying Fords.
:''I dreamed I was an angel''
:''And with the angels soared''
:''But I was simply touring''
: ''The heavens in a Ford.''<ref>Corn 2002, p. 95.</ref>

==Design==
The aircraft was a welded steel tube fuselage, with wood wing construction with fabric covering. The steerable rudder mounted tailwheel was also the only wheel with a brake. The exhaust was routed through a special manifold to a stock [[Ford Model T|Model T]] exhaust. The steel landing gear was fastened to the wing and used rubber doughnuts in compression for shock absorption. The designer of the aircraft, [[Otto C. Koppen|Otto Koppen]], went on to design the [[Helio Courier]].<ref>Peterson, Norm. "There's a Ford in your Future." ''Sport Aviation,'' August 1991.</ref>

[[File:Ford Flivver in air.jpg|thumb|Harry Brooks piloting the first Ford Flivver, c. 1927]]

==Operational history==
Ford unveiled the Flivver on his 63rd birthday, July 30, 1926. Ford's chief test pilot was [[Harry J. Brooks]], a young employee who had become a favorite of Ford. Brooks flew the Flivver regularly from his home garage to work at the Ford Laboratory, and later, used the second Flivver to move about the Ford properties. He once flew the aircraft in a race against [[Gar Wood]] in ''Miss America V'' on the Detroit River during the [[Harmsworth Cup|Harmsworth Trophy Races]].<ref name="EAA"/>

In an attempt to draw on his popularity, Charles Lindbergh was invited to fly the Flivver on a visit to Ford field, August 11, 1927, and was the only other pilot to fly the Flivver prototypes.<ref>Pauley 2009, p. 60.</ref> He later described the Flivver as "one of the worst aircraft he ever flew".<ref>Taylor, James. ''Sport Aviation,'' April 1990.</ref>{{#tag:ref|Throughout the brief flight trials and "real world" assessments, the main deficiency of being underpowered remained unresolved.<ref name="Trex">Trex, Ethan. [http://www.mentalfloss.com/blogs/archives/100314 "The Flying Flivver: Henry Ford’s Attempt to Make Us All Pilots."] ''mentalfloss.com,'' September 14, 2011. Retrieved: August 4, 2012.</ref>|group=N}}

A third prototype, [[aircraft registration|tail number]] 3218, with "long" wings<ref>[http://www.aerofiles.com/_ford.html "Ford, Ford-Stout."] ''Aerofiles.'' Retrieved: August 5, 2012.</ref> was built to win a long distance record for light planes in {{convert|440|to|880|lb|kg|abbr=on}} "C" class.{{#tag:ref|The Ford Flivver would compete in the FAI C-1a/0 class (piston-engined aircraft of less than 300 kg).<ref>[http://www.fai.org/record-powered-aeroplanes "Powered Aeroplanes World Records."] ''Fédération Aéronautique Internationale.'' Retrieved: August 5, 2012.</ref>|group=N}}  The race was set from Ford Field in [[Dearborn Michigan]] to [[Miami, Florida]]. A first attempt launched on 24 January 1928, witnessed by Henry Ford, landed short in [[Asheville, North Carolina]]. A second attempt, flying the second prototype, witnessed by Edsel Ford, Brooks launched from Detroit on February 21, 1928 but landed {{convert|200|mi|km|abbr=on}} short in [[Titusville, Florida]], where the propeller was bent, but still achieved a {{convert|972|mi|km|abbr=on}} record.<ref>"Ford Flivvers Forever." ''Skyways,'' October 1995.</ref>

During his overnight stay at Titusville, Brooks had repaired the aircraft, using the propeller from the aircraft involved in the forced landing. He had also placed wooden toothpicks in the vent holes on his fuel cap to prevent moist air from entering and condensing overnight. On February 25, Brooks took off to complete the flight, circled out over the Atlantic where his motor quit and he went down off [[Melbourne, Florida]]. The wreckage of the Ford Flivver washed up, but the pilot was never found. Investigation of the wreckage disclosed that the toothpicks had plugged the fuel cap vent holes, causing an engine stoppage.<ref>[http://www.latimes.com/business/la-fi-0420-flying-cars-pictures-001,0,782048.photo "The Ford Flivver."] ''Los Angeles Times,'' April 19, 2012.</ref>

Following the death of Brooks, Henry Ford was distraught at the loss of his friend, and light aircraft development was stopped under the Ford brand. In 1931, a new "Air Flivver" or [[Stout Skycar|Sky Car]] was marketed by the Stout division of Ford.<ref>"Air Flivver ready to fly, weighs only 1000 lbs." ''Popular Science,'' June 1931.</ref> Ford went back into light plane development in 1936 with the two-seat [[Ford Model 15-P|Model 15-P]]. The prototype crashed during flight testing and did not go into production.
[[File:Ford Flivver Henry Ford Museum.jpg|thumb|Ford Flivver on display at the [[Henry Ford Museum]]]]
[[File:Ford Flivver EAA.JPG|thumb|Ford Flivver replica on display at the [[EAA AirVenture Museum]]]]

==Variants==
* Original '''Flivver 2A prototype''': Designed around a {{convert|15|ft|m|sing=on|abbr=on}} wingspan, also built with full-span ailerons that could act as flaps, as well as shortened versions, powered by a {{convert|36|hp|adj=on}} three-cylinder Anzani; two built.<ref>[http://www.flightglobal.com/pdfarchive/view/1927/1927%20-%200097.html "The Ford Aerial 'Flivver'."] ''Flight,'' February 17, 1927.</ref>
* '''Flivver 2A''' (Flivver 3218) The third prototype was larger with a {{convert|22|ft|m|sing=on|abbr=on}} wingspan, had a fabric-covered steel frame, featured wing struts, a 50-gallon fuel tank, a dihedral increase, and a custom {{convert|143|cuin|adj=on}}-Ford designed, horizontally opposed two-cylinder engine using Wright Whirlwind components that produced {{convert|40|hp|adj=on}}.<ref>[http://www.floridamemory.com/items/show/435 "Ford Fliver."] ''Florida Memory.'' Retrieved: August 5, 2012.</ref> The final three prototypes had this engine. Crash investigations were based on the pieces of this aircraft that washed ashore.

==On Display==
A surviving Flivver resides in the [[Henry Ford Museum]]. In 1991, [[Experimental Aircraft Association|EAA]] Chapter 159 from [[Midland, Michigan]] donated a replica to the [[EAA AirVenture Museum]]. The replica was built in 1989 from careful inspection of the original prototype and advice from [[Otto C. Koppen]], the original designer, although it was powered by a two-cylinder Franklin engine.<ref name="EAA">[http://www.airventuremuseum.org/collection/aircraft/Ford-EAA%20Chapt%20_159%20Flivver.asp "Ford-EAA Flivver Chapter 159– 268."] ''Air Venture Museum''. Retrieved: August 3, 2012.</ref> A second replica is on display at the [[Florida Air Museum]].<ref>[http://www.airport-data.com/aircraft/photo/747314.html "Aircraft N3218, 1994 Florida Aviation Historical Society Ford Flivver, C/N: FAHS-2."] ''airport-data.com.'' Retrieved: August 24, 2012.</ref>

==Specifications (Ford Model 2A Flivver)==
[[File:Ford Flivver001.jpg|thumb|Side profile]]
{{Aircraft specs
|ref=Pauley, ''Sport Aviation''
<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=1
|length m=
|length ft=15
|length in=6
|length note=
|span m=
|span ft=21
|span in=9
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=Gottingen 387
|empty weight kg=
|empty weight lb=500
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Anzani
|eng1 type=Radial
|eng1 kw=<!-- prop engines -->
|eng1 hp=36<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=72<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=90
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=30<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Szekely Flying Dutchman]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
*[[Volksflugzeug]]

==References==

===Notes===
{{reflist|group=N}}

===Citations===
{{Reflist|2}}

===Bibliography===
{{Refbegin}}
* Corn, Joseph J. ''The Winged Gospel: America's Romance with Aviation.'' Baltimore, Maryland: The Johns Hopkins University Press, 2002. ISBN 978-0-80186-962-4.
* Davis, Michael W. R. and James K. Wagner. ''Ford Dynasty: A Photographic History.'' Mount Pleasant, South Carolina: Arcadia Publishing, 2002. ISBN 978-0-7385-2039-1.
* Ford, Richardson Bryan. ''Beyond the Model T: The Other Ventures of Henry Ford'' (Great Lakes Books Publication). Detroit: Wayne State University Press, 1997. ISBN 978-0-81432-682-4.
* O'Callaghan, Timothy J. ''The Aviation Legacy of Henry & Edsel Ford'' (Michigan). Livonia, Michigan: First Page Publications, 2001. ISBN 978-1-92862-301-4.
* Pauley, Robert F. ''Michigan Aircraft Manufacturers'' (Images of Aviation). Mount Pleasant, South Carolina: Arcadia Publishing, 2009. ISBN 978-0-73855-218-7.
{{Refend}}

==External links==
{{commons category|Ford Flivver}}
* [http://www.thehenryford.org/exhibits/heroes/entrepreneurs/flivver.asp The Planes: 1926 Ford Flivver]
* [https://www.youtube.com/watch?v=J2rAFY-IZas&feature=relmfu Ford Flivver 3218 (1928)]

[[Category:Ford aircraft]]
[[Category:United States civil utility aircraft 1920–1929]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]