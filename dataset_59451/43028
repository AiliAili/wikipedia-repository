<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Hols der Teufel
 | image=Jacobs Hols der Teufel.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Club [[glider (sailplane)|glider]]
 | national origin=[[Germany]]
 | manufacturer=Plans from [[Hans Jacobs]], complete from [[Alexander Schleicher]]
 | designer=Hans Jacobs, [[Alexander Lippisch]] and Alexander Schleicher
 | first flight=c.1928
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Jacobs Hols der Teufel''' ({{lang-en|Devil take it}}) was a single seat [[training aircraft|trainer]] [[glider (sailplane)|glider]] produced in complete and plan forms in [[Germany]] from 1928. It was built and used worldwide.

==Design and development==
The first glider to be named the Hols der Teufel was the influential Djävaler Anamma, designed by [[Alexander Lippisch]] in 1923. The name was associated with the cursing of two [[Sweden|Swedish]] students in the [[Wasserkuppe]] workshops whose favourite phrase it was.  It translates into German as Hols der Teufel. Its key structural feature was an A-frame which carried wire braced wings and linked to a flat girder rear [[fuselage]].  It later evolved through the [[Schneider Grunau 9]] into the very popular [[Zögling]], which avoided the controversial "skullsplitter" forward member of the A-frame with a vertical strut behind the pilot, and was related to the secondary [[RRG Prüfling]] glider that replaced wire bracing with rigid [[lift strut]]s.<ref name=SimonsIa/>  The Hols der Teufel of 1928, designed by Lippisch and [[Hans Jacobs]], possibly with the assistance of [[Alexander Schleicher]] had the Zögling girder frame and the strut braced wings, making it very different from its ancestral namesake.<ref name=SimonsIb/>

The Hols der Teufel was built (and named) by Alexander Schleicher in his factory in the valley below the Wasserkuppe. Detailed plans of it for amateur use were included in a book on glider building published by Jacobs in 1932 and led to its widespread construction.  The complete Schleicher version differed somewhat from the plans in several details, for example more rounded wing tips and span, different girder frame cross bracing and a wire, rather than strut braced tailplane.<ref name=SimonsIb/>

The Jacobs plans show the Hols der Teufel had a simple, constant [[chord (aircraft)|chord]] wing much like that of the Zögling though of greater span, built around two spars and fabric covered except at the [[leading edge]] which was [[plywood]] skinned.  Its broad, constant chord [[ailerons]] were longer than those of the Zögling and its round cornered, tapered tips were less angular.  As before, the wing was supported by the forward members of its girder fuselage; a vertical strut near the leading edge and an inverted V-strut behind.  As the wing was strut rather than wire braced there was no longer a need to extend these struts above the wing into a pylon.  The two [[aircraft fairing|faired]], parallel [[lift strut]]s on each side ran from the lower forward fuselage to the wing at about mid-span.<ref name=SimonsIb/>

Like some Zöglings, the Hols der Teufel had a light [[nacelle]], ending under the wing at a forward leaning vertical knife edge around the aft central wing strut, enclosing both the pilot's seat and the other supporting struts.  Aft, the cross braced girder had a horizontal upper beam onto which the [[tailplane]] was mounted, strut braced from below. Its leading edge was straight and strongly swept; the cropped, parallel chord [[elevator (aircraft)|elevator]]s had a central cut out for [[rudder]] movement. The [[fin]] was under the tailplane, formed by fabric covering between the last two vertical fuselage fames. Its upright, near rectangular rudder was hinged on the extended, final vertical frame member. The Hols der Teufel landed on a rubber sprung skid which ran from the nose to a little beyond the aft lift struts.<ref name=SimonsIb/>

==Operational history==

The ready availability of detailed plans and the fact that with its low [[wing loading]] the Hols der Teufel was one of only a few training gliders with the soaring capability to take a pilot to a C-badge resulted in widespread amateur construction worldwide, though overall numbers are hard to establish, not least in Germany.<ref name=SimonsIb/><ref name=SS/>  Twelve were registered in [[Hungary]], one imported in 1929 and the rest locally built between 1931 and 1938.<ref name=fega/>  Seven appear in the UK on the [[British Gliding Association|BGA]] register, including one at least that flew with the [[London Gliding Club]] for several years.<ref name=Flight/><ref name=Flight2/> During [[World War II]] some served with the [[Air Training Corps|ATC]].<ref name=ATC/> Two were built by a [[Switzerland|Swiss]] immigrant in [[Brazil]],<ref name=Brazil/> one of which is on display in the [[Museu Aeroespacial]] in [[Rio de Janeiro]], the only recorded surviving original example<ref name=Ogden/> One was built in the 1950s in [[Australia]].<ref name=oz/>

In 1990 a reproduction Hols der Teufel, built from Jacobs' plans was flown from [[Dunstable]]; it is now in the Gliding Museum on the [[Wasserkuppe]].<ref name=SimonsIb/>

The [[Slingsby Kirby Cadet|Slingsby Kadet]] was the result of a conscious "crossing" of the low wing loading soaring performance of the Hols der Teufel with the better handling of the Prüfling, particularly by improving the former's poor lateral control with better ailerons.<ref name=SS/>

==Variants==

;Jacobs "Hols der Teufel": The original Lippisch and Jacobs design, popularized via Jacobs' plans in his 1932 book Segelflugzeug.
;Schleicher Hols der Teufel: A slightly modified version produced by Alexander Schleicher from 1928.

==Specifications (Jacobs)==
{{Aircraft specs
|ref=Sailplanes 1920-1945 (2006)<ref name=SimonsIb/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One

|length m=6.50
|length note=
|span m=12.568
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=19.52
|aspect ratio=8.1
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Performance
-->
|perfhide=Y<!--remove this line if performance figures become available-->

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|more performance=

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|30em|refs=

<ref name=SimonsIa>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9806773 4 6|pages=38–44}}</ref>

<ref name=SimonsIb>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition= |year=2006|pages=104–6}}</ref>

<ref name=Ogden>{{cite book |title= Aviation Museums and Collections of the Rest of the World|last=Ogden|first=Bob| year=2008|edition=|publisher=Air-Britain (Historians)  |location=Tonbridge, Kent |isbn= 978-0-851-30-394-9|page=525}}</ref>

<ref name=Flight>{{cite magazine |last= |first= |authorlink= |coauthors= |date=9 October 1931  |title= Gliding.|magazine= [[Flight International|Flight]]|volume=XXIII |issue=44 |page=1020  |url= http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%201090.html }}</ref>

<ref name=Flight2>{{cite magazine |last= |first= |authorlink= |coauthors= |date=15 March 1934  |title= London Gliding Club.|magazine= [[Flight International|Flight]]|volume=XXVI |issue=11 |page=1020  |url= http://www.flightglobal.com/pdfarchive/view/1934/1934%20-%200249.html }}</ref>

<ref name=SS>{{cite web |url= http://www.scalesoaring.co.uk/VINTAGE/Documentation/SlingsbyCadet/Slingsby%20Cadet.htm|title=The Slingsby Type 7 Cadet |author=John Stanley Sproule |date= |work= |publisher= |accessdate=5 February 2014}}</ref>

<ref name=fega>{{cite web |url=http://gliders-fega.freeweb.hu/holci.html|title= GLIDERS IMPORTED TO OR BUILT UNDER LICENCE IN HUNGARY 1929-1945 |author= |date= |work= |publisher= |accessdate=5 February 2014}}</ref>

<ref name=Brazil>{{cite web |url=http://www.grunaubaby.nl/Brazil%20Baby.htm |title= The Grunau Baby in Brazil |author= |date= |work= |publisher= |accessdate=5 February 2014}}</ref>

<ref name=oz>{{cite web |url=http://www.gliding.com.au/sx/clubhistfull.htm |title= Southern Cross Gliding Club |author= |date= |work= |publisher= |accessdate=5 February 2014}}</ref>

<ref name=ATC>{{cite web |url=http://www.pilotspace.eu/hols-der-teufel_604?action=dictionary |title=Hols der Teufel |author= |date= |work= |publisher= |accessdate=5 February 2014}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->
{{Hans Jacobs aircraft}}
{{Lippisch aircraft}}
{{Schleicher}}

[[Category:German sailplanes 1920–1929]]