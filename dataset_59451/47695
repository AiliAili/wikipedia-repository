{{Infobox university
| name = ELISAVA Barcelona School of Design and Engineering
| established = 1961
| city = [[Barcelona]]
| country = Spain
| campus = Urban
|students        = 1,868<ref name="memoria1213">''[http://www.elisava.net/sites/default/files/documents/memoria-elisava_12-13_ang.pdf Memòria 2012-13 ELISAVA]''  Barcelona : Fundació Privada ELISAVA Escola Universitària, 2013</ref>
| type = Private
| website = www.elisava.net [http://www.elisava.net]
| coor = {{Coord|41|22|43.46|N|2|10|32.91|E|display=title}}
|}}

'''ELISAVA''' is first school of design, an internationally oriented educational and research institution affiliated with [[Pompeu Fabra University]]  <ref name="disenoindustrialespana">Galán, Julia; Gual, Jaume; Marín, Joan M.; et al. ''El diseño industrial en España''. Madrid: Cátedra, 2010.</ref> The school is situated in Barcelona and is home to around 2,200 students and more than 800 teachers.

ELISAVA has been merged with [[Pompeu Fabra University]] since 1995. In 2000, ELISAVA won a National Innovation and Design Award.<ref>Premios Nacionales de Diseño 2000 = National Design Prizes : 2000. [Barcelona] : Fundación BCD : Ministerio de Ciencia y Tecnología, DL 2002, p. 133-148.</ref> In 2013, the magazine [[Domus (magazine)|Domus]] ranked it among the top design and architecture schools in Europe.<ref>Europe's TOP 100 Schools of Architecture and Design 2013. Milano: Editoriale Domus, 2012, p. 174-175.</ref>

== History ==

=== Origins ===
Founded in Barcelona in 1961, ELISAVA was launched to create a new venue for teaching design, as part of the CIC’s Cultural Institution Foundation.<ref>[http://tdd.elisava.net/coleccion/13?set_language=en&cl=en El recorregut pedagògic de l'escola Elisava] ' Temes de Disseny 13 (1996), p.10. {{ISSN|0213-6023}}</ref> The school gradually came to   oriented itself    theoretical and practical training  when tackling and resolving design problems, as opposed to repeating problems that have already been solved (as in artisan education).<ref>Mañá, Jordi. ''[http://www.raco.cat/index.php/CuadernosArquitecturaUrbanismo/article/view/111052/160787 La pedagogía del diseño en Barcelona ]'' Cuadernos de Arquitectura y Urbanismo 82(1971). Barcelona: Col•legi d'Arquitectes de Catalunya.</ref> In 1997 the school added a programme in Industrial Design Engineering, and in 1998 it launched its Architecture programme.<ref name="escoladissenybasic">Escola de Disseny Bàsic : miscel•lània : 1971-2000. Barcelona : Elisava, 2000.</ref>

=== 1991, Master’s and postgraduate programmes ===
ELISAVA’s postgraduate workshops got underway in 1990 with the aid of such outstanding designers as Francesco Binfarré, Achile Castiglioni, Isao Hosoe, Santiago Miranda, Jonathan de Pas and Denis Santachiara.<ref>''[http://tdd.elisava.net/coleccion/13/blanch-en/view?set_language=en International Relations]'' Temes de Disseny 13 (1996). {{ISSN|0213-6023}}</ref>  The postgraduate workshops continued until they became postgraduate and Master’s programmes in academic year 1991-1992 as part of the cooperation agreement that ELISAVA signed with the [[University of Barcelona]].<ref>''[http://tdd.elisava.net/coleccion/6/-bricall-en/view?set_language=en The tension necessary in the teaching of design. The diversified option of the school Elisava]'' Temes de Disseny 13 (1996). {{ISSN|0213-6023}}</ref> Through ELISAVA’s affiliation with the Pompeu Fabra University (UPF), today, it offers around 20 Master’s and 40 postgraduate programmes related professional practice in all areas of design and engineering in conjunction with the UPF’s IDEC (Institute of Continuing Education).<ref>'[http://www.idec.upf.edu/en/seccions/oferta_formativa/masters_programes/area.php?num=12 Design and Architecture]  Barcelona: Universitat Pompeu Fabra, 2013.</ref>

=== 2003, Private Foundation ELISAVA University School ===
To promote the school and making it more autonomous, in 2003 the CIC’s Cultural Foundation set up the private foundation,  ELISAVA University School.<ref>[http://www.ccfundacions.cat/fundacions/fundacio-elisava-escola-universitaria Fundació Elisava Escola Universitària] . Barcelona: Coordinadora Catalana de Fundacions, 2011.</ref> Its original mission was:  to promote education, knowledge, research, development, innovation and the use of new technologies in the fields of science, technology, business and the arts; to boost and consolidate the quality of the degree programmes and services offered; to increase the range of continuing education programmes and to enhance student job placement; to adapt the range of programmes and services to the needs of an ever-changing society and business world; to attain international outreach; to contribute to the social, cultural and economic progress of society; and to foster the personal development of the members of the ELISAVA community.<ref name="memoria0304">''MEMÒRIA Curs 2003-2004 ELISAVA''. Barcelona : Fundació Privada ELISAVA Escola Universitària, 2004.</ref>

=== 2009, integration into the new European university framework ===
In 2009, ELISAVA’s educational programme became fully integrated into the [[European Higher Education Area]] by offering its Bachelor’s Degree in Design, its Bachelor’s Degree in Industrial Design Engineering and its Bachelor’s Degree in Building Engineering for the first time. Prior to that, it had been offering a University Master’s in Communication and Design since 2008.<ref name="memoria0910">''[http://www.elisava.net/sites/default/files/documents/elisava_-_memoria_2009_-2010_1.pdf ELISAVA Memòria 09-10]''. Barcelona : Fundació Privada ELISAVA Escola Universitària, 2010.</ref>

=== Previous campus locations ===
ELISAVA’s current modern campus was preceded by various locations due to its rising popularity and steady growth in the number of students and instructors.<ref name="escoladissenybasic" /> The actual campus has excellent study and research facilities, special tools for modeling, workspace for students, lecture facilities, conference rooms, and spacious exhibition facilities.

== Fields of knowledge ==
ELISAVA offers educational programmes in:<ref>''Studies | ELISAVA'' [online]. Barcelona : ELISAVA Escola Superior de Disseny i Enginyeria de Barcelona, 2013. Available at: http://www.elisava.net/en/studies</ref>
*Graphic Design and Communication
*Product Design
*Industrial Engineering Design
*Interior Space Design
*Architecture
*Fashion
*Design Strategy

== Network of international relations ==
ELISAVA has agreements with more than 60 universities in Europe, the United States, Latin America, Asia and Australia.<ref>''List of exchanges| ELISAVA''. Barcelona : ELISAVA Escola Superior de Disseny i Enginyeria de Barcelona, 2013. Available at: http://www.elisava.net/en/international-relations/exchanges/list-exchanges</ref> It is a member of the Cumulus network, which brings together more than 100 renowned institutions from all over the world to promote international cooperation.<ref>''[http://www.cumulusassociation.org/members/full-members/view/100 Escola Superior de Disseny Elisava]''. Helsinki: Cumulus International Association of Universities and Colleges of Art, Design and Media, 2013</ref> It is also a member of the International Association for Exchange of Students for Technical Experience (IAESTE). It works with the Council on International Educational Exchange, a non-profit organisation designed by the U.S. State Department with the mission of managing international student exchange programmes. It also participates in the LifelongLearning/Erasmus programme promoted by the European Union to bring students closer to Europe and Europe closer to students.<ref name="memoria1011">''[http://www.elisava.net/sites/default/files/documents/elisava_mem.2010-11_0.pdf Memòria 2010-11 ELISAVA]''  Barcelona : Fundació Privada ELISAVA Escola Universitària, 2011.</ref>

== Innovation and business ==

=== Relations with businesses ===
ELISAVA is a university that is interested in generating and transferring knowledge to companies, forging direct links with the business world and generating real-life learning situations for its students. To date, it has conducted projects and reached agreements with companies and institutions like [[3M]], [[Adobe Systems Incorporated]], [[Apple Inc]], [[BMW]], Decathlon, [[Desigual]], [[DuPont]], [[Ericsson]], Fabrica ([[Benetton Group|Benetton]]), [[Generalitat de Catalunya]], [[Grupo Agbar]], [[Henkel]], [[Hewlett-Packard]], Hospital Clínic de Barcelona, Iguzzini, [[IKEA]], LABCO, [[ICFO]], [[La Caixa]], [[Lego]], Mango, [[Philips]], Roca Sanitarios, [[Sony]],<ref name="memoria1112">''[http://www.elisava.net/sites/default/files/documents/af-cat-memoria_2011-12.pdf Memòria 2011-12 ELISAVA]''  Barcelona : Fundació Privada ELISAVA Escola Universitària, 2012</ref> [[Vueling Airlines]]<ref name="memoria1011" /> Fundació Vila Casas, Happy Pills and Smart Design.<ref name="memoria1213" />

=== Applied research ===
ELISAVA conducts research projects applied to design and engineering with the goal of pursuing innovation in business.<ref name="escoladissenybasic" /> The research is organised into three main strands: social innovation, innovation in new materials and new manufacturing technologies, and innovation in business. It conducts projects in the fields of visual communication, product design and engineering, mobility and transports, retail space, new materials, sustainability and healthcare. It has worked with the [[Pasqual Maragall Foundation]],<ref>ACN. ''[http://www.fpmaragall.org/recerca/programa-innovacio.html Programa d'innovació]''. Barcelona: Fundació Pasqual Maragall, [2013]</ref><ref>Europa Press. ''[http://www.vilaweb.cat/ep/ultima-hora/3855497/20110302/fundacio-pasqual-maragall-elisava-dissenyaran-productes-malalts-dalzheimer.html La Fundació Pasqual Maragall i Elisava dissenyaran productes per a malalts d'Alzheimer]''. Barcelona: Vilaweb, 2011</ref> ASCER,<ref>''[http://www.ascer.es/prensaNoticias.aspx?id=984 Los estudiantes de ELISAVA muestran nuevos usos de la cerámica en interiores]''. Castellón: Ascer, 2011</ref> ALSTOM/ FGC, Rucker-Lypsa,<ref>Centre Específic de Recerca per a la millora i Innovació de les Empreses. ''[http://cerpie.upc.edu/proyectos/MULTI-MATHERIA.asp MULTI-MATHERIA]''. Barcelona: Universitat Politècnica de Catalunya, [2011]</ref> [[Bayer Material Science]] <ref>ACN. ''[http://www.bayer.es/ebbsc/cms/es/noticias/news0032.html Bayer premia a los jóvenes más creativos de ELISAVA Escola Superior de Disseny]''. [Madrid]: Bayer España, 2008</ref> and Fabrica.

== Temes de Disseny magazine ==
Since 1986, ELISAVA has published the magazine ELISAVA Temes de Disseny, a trilingual scholarly research publication which debates issues related to design and its relationship with technology, communication, culture and the economy.<ref name="disenoindustrialespana" /> http://tdd.elisava.net

== Elisava Alumni ==
The ELISAVA Alumni Association was founded in 2003 as Elisava Professionals (EP). It is a non-profit organisation resulting from the commitment and effort of a group of students and alumni who wished to disseminate and promote the identity and values of the individuals who make up the ELISAVA community.<ref name="memoria0304" />
In 2012, the association embarked upon a new stage to match its new name, Elisava Alumni, and it organised a competition to design its new corporate image. The winning submission, designed by the alumnus Albert Ibanyez, was awarded three Laus prizes organised Foment de les Arts i el Disseny in the categories of Graphic Design-Naming (silver),<ref>ADG-FAD. ''[http://www.laus.cat/en/proyecto?ap=10&project_page=3 Laus Elisava Alumni Bold]''. Barcelona: ADG-FAD, [2013]</ref> Entities (silver) <ref>ADG-FAD. ''[http://www.laus.cat/en/projecte?aw=3&project_page=47 Laus Elisava Alumni]''. Barcelona: ADG-FAD, [2013]</ref> and Graphic Design-Logo (bronze).<ref>ADG-FAD. ''[http://www.laus.cat/en/projecte?aw=4&project_page=90 Laus Elisava Alumni]''. Barcelona: ADG-FAD, [2013]</ref>
The corporate image of Elisava Alumni harks back to the 12th century banner of Saint Otto, which contains one of the first female signatures in Catalan: “Elisava me fecit” (Elisava made me).<ref>Xarxa Vives d'Universitats. ''[http://www.dbd.cat/index.php?option=com_biografies&view=biografia&id=423 Diccionari Biogràfic de Dones]''. Castelló de la Plana: Xarxa Vives d'Universitats; Universitat Jaume I, [2013]. Varela Rodríguez, M. Elisa. Biografia de Elisava - Brodadora noble</ref> Spanish-German popstar [[Álvaro Soler]] is an alumni at ELISAVA,he studied Design Emgineering gfrom 2009-20013.

== Activities ==
ELISAVA promotes and disseminates knowledge related to design and engineering through a busy schedule of activities. In 1991, it began to organise and host a variety of activities as part of Primavera del Disseny, a biannual design fair consisting of a theoretical reflection on the different issues being posed in the world of design.<ref>Manzano, Emilio. "El diseño protagoniza la primavera cultural de Barcelona". La Vanguardia, 4 de abril de 1991, p.33. Disponible en: http://hemeroteca.lavanguardia.com/preview/1991/04/04/pagina-33/33473100/pdf.html?search=primavera%20del%20disseny</ref>

ELISAVA has hosted a number of conferences related to the different fields of knowledge taught at the school. In 2012, it hosted the European EPIC, Ethnographic Praxis in Industry Conference, on ethnographic research in industry.<ref>EPIC Europe Network. ''EPIC Europe Barcelona Meeting''. Available at: http://epiceuropenetwork.wordpress.com/</ref> In 2013, it hosted the first edition of Entretipos, a series of workshops and lectures on typography;<ref>FAD. ''Entretipos''. Available at: http://www.fad.cat/activities/view/575</ref> the international web design conference WebVisions;<ref>WebVisions. ''Web & Mobile Design, Technology and UX Conference''. Available at: http://www.webvisionsevent.com/barcelona/</ref> and the fourth edition of Algomad, the seminar on generative methods in architecture and design.<ref>Algomad. ''Algomad 2013 – Enfoque en Sagrada Familia''. Available at: http://www.algomad.org/algomad-2013/</ref>

ELISAVA regularly welcomes international lecturers and leading professionals from different fields, such as [[Zaha Hadid]], [[Philippe Starck]],<ref>Moix, Llàtzer. ''Philippe Stark propugna una nueva dimensión moral para el diseño''. La Vanguardia, 12 abril 1991, p.44. Available at: http://hemeroteca.lavanguardia.com/preview/1991/04/04/pagina-44/33476719/pdf.html?search=primavera%20del%20disseny</ref> [[Andrea Branzi]], [[Ronan Bouroullec]], [[Xavier Mariscal]], or [[Lidewij Edelkoort]].

== References ==
{{reflist|2}}

== External links ==
* [http://www.upf.edu/postgrau/en/centres-adscrits/elisava.html Official web site]

[[Category:Education in Barcelona]]