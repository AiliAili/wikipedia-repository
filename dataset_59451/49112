{{Eastern name order|Kovács Ákos}}
{{Infobox scientist
| name        = Ákos Kovács
| native_name = Kovács Ákos
| native_name_lang = hun
| image       =
| image_size  = 
| alt         = 
| caption     = Akos Kovacs, Hungarian radiologist
| birth_date  = {{birth date |1903|4|7}}
| birth_place = [[Budapest]], Hungary
| death_date  = {{death date and age |1980|11|3 |1903|4|7}}
| death_place = 
| death_cause = 
| resting_place = 
| resting_place_coordinates =  <!--{{coord|LAT|LONG|type:landmark|display=inline,title}}-->
| other_names = 
| residence   = 
| citizenship = 
| nationality = Hungarian
| fields      = Radiology
| workplaces  = 
| patrons     = 
| education   = 
| alma_mater  = 
| thesis_title =        <!--(or  | thesis1_title =  and  | thesis2_title = )-->
| thesis_url  =         <!--(or  | thesis1_url  =   and  | thesis2_url  =  )-->
| thesis_year =         <!--(or  | thesis1_year =   and  | thesis2_year =  )-->
| doctoral_advisor =    <!--(or  | doctoral_advisors = )-->
| academic_advisors = 
| doctoral_students = 
| notable_students = 
| known_for   = 
| influences  = 
| influenced  = 
| awards      = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| spouse      =         <!--(or | spouses = )-->
| partner     =         <!--(or | partners = )-->
| children    = 
| signature   =         <!--(filename only)-->
| signature_alt = 
| website     =         <!--{{URL|www.example.com}}-->
| footnotes   = 
}}

'''Ákos Géza Kovács''' (April 7, 1903 - November 3, 1980) was an internationally renowned Hungarian [[Radiology|radiologist]].

== Biography ==

After finishing the Medical School of the [[Pázmány Péter Catholic University]] in Budapest, Kovács became interested in radiology (a very new diagnostic field at that time), and studied its methods in different laboratories in Hungary and abroad. He was involved in the development of new radiological methods with the aim of overcoming the most important shortcoming of standard plain radiography that produces a single-directional image. His results were published in major international scientific journals of radiology.<ref>http://www.orvostortenet.hu/tankonyvek/tk-05/pdf/4.18/1969_051_053_zsebok_zoltan_evolution_radiology.pdf</ref><ref>{{cite web|url=http://mek.oszk.hu/00300/00355/html/ABC07165/08506.htm|title=Magyar Életrajzi Lexikon 1000-1990|publisher=|accessdate=22 February 2015}}</ref>

Kovács who worked for the St. John's Hospital (1929-1952) and the St. Rokus Hospital (1952-1974) in Budapest, Hungary. He is credited with discovering a new method of X-ray imaging of the lowermost [[Lumbar vertebrae|lumbar intervertebral foramen]] (named after him as the Kovacs method).<ref>{{cite web|last1=Anonymous|first1=A|title=Alphabetical List of Named Radiographic Projections|url=http://scrsl.weebly.com/uploads/5/1/3/0/5130772/alphabetical_list_of_named_radiographic_projections.doc|website=http://scrsl.weebly.com|publisher=Sri Lanka School of Radiography|accessdate=15 February 2015}}</ref><ref name="McConnell2011">{{cite book|author=Jonathan McConnell|title=Index of Medical Imaging|url=https://books.google.com/books?id=Fvmul7J2Gl4C&pg=PT80|date=8 April 2011|publisher=John Wiley & Sons|isbn=978-1-4443-4097-6|pages=80–}}</ref>

== Selected publications ==

*{{cite journal|last1=Kovacs|first1=A|title=Herniated Disks and Vertebral Ligaments on Native Roentgenograms|journal=Acta Radiologica|date=1949|volume=32|issue=4|pages=287–303|doi=10.3109/00016924909136238|url=http://acr.sagepub.com/content/os-32/4/287.refs|accessdate=15 February 2015}}
*{{cite journal|last1=Kovacs|first1=A|title=X-ray examination of the exit of the lowermost lumbar root|journal=Radiol Clin|date=1950|volume=19|issue=1|pages=6–13|pmid=15402775}}<!--|accessdate=15 February 2015-->
*{{cite journal|last1=Kovacs|first1=A|title=Subluxation and Deformation of the Cervical Apophyseal Joints|journal=Acta Radiol|date=1955|volume=43|issue=1|pages=1–16|pmid=14349741}}
*{{cite journal|last1=Kovacs|first1=Akos|title=Kephalalgia e Subluxatione artic. Cervicalis|journal=Fortschr Roentgenstr|date=1956|volume=85|issue=8|pages=142–153|doi=10.1055/s-0029-1212960|url=https://www.thieme-connect.com/products/ejournals/abstract/10.1055/s-0029-1212960|accessdate=15 February 2015}}
*{{cite journal|last1=Kovacs|first1=A|title=Roentgen Physiology of the Larynx|journal=Acta Radiol|date=1961|volume=56|issue=December|pages=433–438|pmid=14458912}}
*{{cite journal|last1=Kovacs|first1=A|title=Roentgen therapy of glioma of the eye|journal=Klin Monbl Augenheilkd Augenarztl Fortbild.|date=1953|volume=122|issue=1|pages=43–51|pmid=13053688}}
*{{cite journal|last1=Kovacs|first1=A|title=Asymmetric roentgenography of the vocal chords|journal=Acta Radiol|date=1960|volume=53|issue=June|pages=426–432|pmid=14411384}}
*{{cite journal|last1=Kovacs|first1=A|title=Roentgenologic study of the laryngeal function in singers|journal=Acta Radiol Diagn (Stockh).|date=1967|volume=6|issue=6|pages=548–560|pmid=6080080}}
*{{cite journal|last1=Kovacs|first1=Akos|title=Observation of the Cervical Segment of the Spinal Canal by An Extension Device|journal=Acta Radiol|date=1974|volume=15|issue=January|pages=33–42|doi=10.1177/028418517401500104|url=http://acr.sagepub.com/content/15/1/33.refs|accessdate=15 February 2015}}

== References ==
{{Reflist}}

{{DEFAULTSORT:Kovacs, Akos}}
[[Category:1903 births]]
[[Category:1980 deaths]]
[[Category:Hungarian people]]
[[Category:Radiologists]]