{{Use British English|date=February 2012}}
{{Use Australian English|date=July 2011}}
{{Use dmy dates|date=February 2012}}
{{Infobox prison
| prison_name    = Yatala Labour Prison
| image          = Yatala prison rear 2008.JPG
| caption = Rear of the prison complex
| location       = Peter Brown Drive, [[Northfield, South Australia]]
| coordinates    = {{coord|34|50|40|S|138|37|43|E|region:AU-SA|display=inline,title}}
| status         = Operational
| classification = Maximum security
| capacity       = 468<ref>[http://www.corrections.sa.gov.au/prisons/yatala_labour_prison.htm Yatala Labour Prison]</ref>
| opened         = 1854
| closed         = Currently active
| managed_by     = South Australian, Department for Correctional Services
|}}

'''Yatala Labour Prison''' is a high-security men's prison located in the north-eastern part of the northern suburb [[Northfield, South Australia|Northfield]] in [[Adelaide]], [[South Australia]], [[Australia]]. It was built in 1854 to enable prisoners to work at the creek, quarrying rock for roads and construction. Originally known as ''The Stockade'' it acquired its current name from a local [[Indigenous Australians|Aboriginal]] word.

The prison has been expanded many times but still has functioning buildings that date to the 1850s. It remains Adelaide's main male prison and although it was scheduled to be closed by 2011, it has remained open due to the Global Financial Crisis.

==Geography and naming==
Yatala prison, originally called ''The Stockade'', was named after the cadastral [[Hundred of Yatala]]. The word is from the [[Weira]] group of the [[Kaurna people|Kaurna]] Aboriginal people, meaning ''water running by the side of a river''.<ref>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/xyz/y2.htm#yatala |title=Place Names of South Australia – XYZ (Yatala) |publisher=State Library of South Australia |accessdate=28 February 2007}}</ref> It is known as a labour prison by virtue of its vast industries complex and the use of [[convict]] labour in construction.

It is sited in Adelaide's northern, but north-eastern part, suburb of [[Northfield, South Australia|Northfield]], {{convert|10|km|mi|0}} north of Adelaide's central business district and between [[Grand Junction Road, Adelaide|Grand Junction Road]] and [[Dry Creek, South Australia|Dry Creek]]. The prison sits on an [[escarpment]] of the [[Geologic fault|Para Fault Block]] overlooking the Adelaide plains.<ref>Department of Mines.''Geology and Underground Water Resources of the Adelaide Plains Area'' Adelaide: Geological Survey of South Australia. Bulletin No. 27, 1952 (As cited in Settlers on the Hill, City of Salisbury Publication, 1985)</ref> [[Dry Creek (South Australia)|Dry Creek]], a watercourse usually dry in summer, flows through a deep [[gully]] immediately north of the prison boundary. It features outcrops of exposed [[pre-Cambrian]] rocks that were once extensively [[Quarry|quarried]] as part of prison activity.

==History==
For the first five years of [[European settlement of South Australia|South Australian settlement]] there was no permanent prison. Prisoners were kept locked in [[Ball and chain|irons]] onboard {{HMS|Buffalo|1813|6}} until its sailing in 1837, and in temporary jails subsequently.<ref>Lewis, H. (1985), p. 170.</ref> 1841 saw the first permanent prison built in Adelaide, with the [[Adelaide Gaol]] on the banks of the [[River Torrens]], the building of which severely strained the new colony's finances.<ref>{{cite web|url=http://www.slsa.sa.gov.au/manning/adelaide/gaols/gaols.htm | title=Adelaide – Gaols, Reformatories and the Law |publisher=State library of South Australia |accessdate=28 February 2007}}</ref>

In the 19th century, [[incarceration]] in South Australia was seen as a [[Punishment|punitive]] more than preventative measure. The labour of prisoners was used for [[public works]] and hard labour seen as an integral part of imprisonment. In this light, [[Charles Simeon Hare]] [[South Australian Legislative Council|<small>(member of the legislative council)</small>]] wrote an 1853 letter to the ''[[Adelaide Observer]]'', advocating prisoners be usefully employed, and further that a {{convert|160|acre|km2|sing=on}} reserve beside [[Dry Creek (South Australia)|Dry Creek]] could be used for this purpose. The reserve had an abundant supply of stone that prisoners could convert into building and road material.<ref>Lewis, H. (1985), pp. 174–5</ref> September that year saw Hare move, in the council, that [[Pound sterling|£5,000]] be set aside to enable a prison be constructed next to a quarry, whether at Dry Creek or elsewhere. This would enable the labour of the prisoners to remunerate the country.<ref name="Lewis175" /> Hare later became superintendent of the prison and maintained a colourful register describing prisoners.<ref>{{cite web |url=http://www.archives.sa.gov.au/files/publications_newsletter_2002_mar.pdf |format=PDF|title= Indecent tattoos and other notations, Yatala prison registers in the 1800s |pages =8 |publisher=State Records of South Australia |accessdate=1 March 2007}}</ref>

=== 19th century ===
Twenty five prisoners were sent to the Dry Creek site to work in the quarries in July 1854, living at night in an iron house. Dry Creek prison was officially declared a [[gaol]] on 10 Aug 1854 and an act then passed commuting sentences, formerly of transportation to [[New South Wales]] or [[Van Diemen's Land]], into imprisonment with hard labour, though transport to the latter had been stopped by the [[Her Majesty's Government|Imperial Government]] in 1852.  The Prison began as an iron house with surrounding [[palisade]] and became known as ''The Stockade'', a name maintained in 2007 by the adjacent [[Botanical garden|botanic park]]. Hare requested construction of a stone building, and by October 1854 this was completed using locally quarried [[bluestone]], with accommodation for 60 prisoners.<ref name="Lewis175">Lewis, H. (1985), p. 175.</ref>

[[Image:Yatala prison powder magazine.JPG|right|thumb|Yatala's powder magazine, used in quarrying]]
In its early years rock-cracking, [[hard labour]] and [[solitary confinement]] were the notable features of life at the prison. It was seen that hard worked prisoners would not wish to return to the prison, with solitary confinement giving them time to reflect on past misdeeds.<ref name="OARS">{{cite web| title=Romantic Beginnings |url=http://www.oars.org.au/Downloadable%20Documents/romantic%20beginnings.pdf |archive-url=https://web.archive.org/web/20030724190518/http://www.oars.org.au:80/Downloadable%20Documents/romantic%20beginnings.pdf |dead-url=yes |archive-date=24 July 2003 |format=PDF|publisher=Offenders Aid & Rehabilitation Services of S.A. inc | isbn=0-9596363-5-8 |year=1991 |accessdate=13 August 2006 }}</ref> From inception prisoners main task was the breaking of one cubic yard of rock per day. Until the middle of the 20th century the prisons department's philosophy remained punitive with much reliance on obedience to rules and regulations.<ref name="Lewis177">Lewis, H. (1985), p.177</ref>

The first batch of 24 convicts was sent to the prison from Adelaide gaol on 9 February 1855 wearing the characteristic [[broad arrow]] pattern prison clothes of the time,<ref>''The Observer'', 10 February 1855, Adelaide, South Australia, p. 5.</ref> and the first [[Prison escape|escape]] from the prison took place October 1855 with 8 escapees. The Prisoners were captured, chained in solitary confinement within the prison, then subsequently punished with [[Flagellation|50 lashes]] for the escape and other disciplinary issues.<ref name="Lewis175" />

For the prison, [[water supply]] was a constant issue, with carriage required from distant [[Port Adelaide]]. A [[Water well|well]] was bored in 1856 through {{convert|60|ft|m|0}} of [[limestone]] but soon ran dry. For storage of [[rainwater]], in 1860 a {{convert|300000|impgal|L USgal|-3|sing=on}} reservoir was constructed under the main courtyard. Water supplies continued to be inadequate until the 1878 construction of a [[Pipeline transport|pipeline]] to the [[Hope Valley Reservoir]].<ref name="Lewis175" />

The first significant expansion of the prison buildings occurred in 1858 with the construction of '''B Division'''. Built in the centre of the prison with 123 [[Prison cell|cells]] and, in the 19th century, designed to hold 300 prisoners.<ref name="Lewis175" /> A new wing was added in 1872 with 36 cells, guard accommodation and a wall separating it from the rest of the prison, with 37 more cells added in 1878. By 1880 the accommodation was seen as insufficient for the 280 prisoners then held, with up to three per cell and eighteen per [[dormitory]] room. A ''T'' shaped building was constructed in 1884 with 96 cells over three floors, and the walled area expanded.  The building included a chapel, offices and three dark underground cells used for solitary confinement.  Known as '''A Division''', it was built by prisoners at the jail as part of their enforced labour.<ref name="Lewis176">Lewis, H. (1985), p. 176.</ref>

=== 20th century ===
Prisoners moved from rock breaking to goods production with trades including [[boot]] making, tailoring, [[tin]] [[Smith (metalwork)|smithing]], [[blacksmith]]ing, carpentry and [[masonry]]. There was public opposition as the free labour of prisoners was seen as unfair competition against [[private industry]], consequently Government departments used most products. During the 1960s small industries were established north of the prison walls with facilities for [[spray painting]], [[sheet metal]]work and brick making.  A decision was made in 1968 to build a new industries complex. Construction was from 1977 to 1982, with the complex opening in November 1984.<ref name="Lewis177" />

'''C Division''' was created in 1957, as a minimum-security building, outside the main prison walls with a dining room added in 1967. '''B Division''' was redesigned and reequipped in 1958. The special education section of the education department opened a school at the prison in 1976 and [[Technical and Further Education]] began participating in prisoner education at Yatala from 1979.<ref name="Lewis177" />

Although a high security prison, there have been some significant prisoner escapes. Four prisoners escaped in 1930, and lead the police on a [[car chase]] with whom they were involved in a [[shootout]]. After escaping into school grounds they were recaptured, with two of the police injured.<ref>{{ cite web |url=http://www.prospect.sa.gov.au/site/page.cfm?u=1057 |title=History Timeline |publisher=[[City of Prospect]] |year=2006 |accessdate=1 March 2007}}</ref> Six prisoners escaped from the jail in 1979 after an attempted mass break-out by thirty. A wall that was under repair and covered in [[scaffolding]] was used as part of the escape, but all six escapees were soon recaptured.<ref>{{cite news |work=The Illawarra Mercury|date=25 August 1979 |pages=21 |title=News}}</ref> There was poor morale amongst inmates in the 1980s leading to a major [[prison riot]]. Sixty prisoners went on a rampage on 22 March 1983, and lit fires, destroying the roof of '''A division'''. The government saw this as an opportunity to restructure Yatala, rather than simply repairing the damage,  and on 21 December announced that '''A division''' would be demolished. The former Enfield Council strongly objected due to the building's historic value but demolition began on 6 February 1984.<ref name="Lewis180">Lewis H. (1985), p. 180</ref>

==Yatala Labour Prison today==
The prison holds high, medium and low security prisoners, and is South Australia's main induction and reception prison for male prisoners. It still retains industry facilities that are the largest in the South Australian prison system, and is run by the [[Government of South Australia|South Australian government's]] Department for Correctional Services. Some of the original buildings and parts of old equipment can still be seen from a creek level walking trail, between the prison and new suburb of [[Walkley Heights, South Australia|Walkley Heights]]. These include guard towers, [[quarry|quarries]], a [[blacksmith]]'s shop and a [[gunpowder]] [[Magazine (artillery)|magazine]]<ref>{{cite web |url=http://www.corrections.sa.gov.au/history/ |publisher=Department for correctional services, South Australia |accessdate=1 March 2007 |title=The History of Correctional Services}}</ref>

The prison is divided into four units:<ref>{{cite web |url=http://www.corrections.sa.gov.au/prisons/yatala_labour_prison.htm |publisher=Department of Justice Correctional Services, South Australia |title=Prison and Prisoner Management, Yatala Labour Prison |accessdate=1 March 2007}}</ref>
* B-Division – High and medium security mainstream prisoners, clothing colour dark Green.
* E-Division – Protection Unit, clothing colour light Blue. 
* F-Division – Working division, which includes industries such as Souwester, Panda, Assembly, Laundry, Joiners etc.  This division has a clothing colour of dark blue.
* G-Division – The highest security section of the prison.  Prisoners are under total separation.
* E Unit 4 - It is a small unit accommodating 16 prisoners.
* Holden Hill - Due to the critical bed-space crisis in South Australian prisons, Holden Hill became functional in 2016.  It can accommodate 19 prisoners.


Yatala is reported as having 603 prisoners in a facility designed for 341. The prison was planned to be closed when a new prison at [[Mobilong Prison|Mobilong]] was completed, though some buildings will be retained for their historic values.<ref>{{cite news|last=Kelton|first=Greg|work=The Advertiser|date=21 September 2006 |pages=1–2 |title=Yatala to go for new homes}}</ref> It was expected that the closure will happen by 2011, and the land developed for residential housing but this has been cancelled. No current funding has been put in place for the new prison, therefore the land redevelopment will not occur as previously noted.

The new roster was implemented on 2 July 2011 despite a widespread outcry and criticism by the staff.

Mr. Stephen Mann was the previous General Manager of Yatala Labour Prison.  He is succeeded by Mr. Brenton Williams, also known as Mr. BJ.

Mr. Peter Forrest is the PSA representative for Echo Division.

The new gatehouse was opened in March, 2013 by Mr. David Brown, the Chief Executive of Department for Correctional Services.  The reception area includes two state of the art turnstile machines, which can detect any concealed drugs and metal objects.  It is also equipped with an X -ray scanning machine.  After checking through the turnstiles, visitors and staff must go through an Iris scan and fingerprint detecting booths to gain entry into the prison.

<ref>{{cite news |title = State Budget 2006 Jailbreak that sets free our suburbs | work = The Advertiser | page = 22 | date = 23 September 2006}}</ref>

== Notable prisoners ==
* [[Alan Bond (businessman)|Alan Bond]]{{spaced ndash}} (1938-2015):Australia businessman, spent a brief time in Yatala during his fraud trial in the early 1990s.
* [[John Bunting (serial killer)|John Bunting]]{{spaced ndash}} serial killer, ringleader in the [[Snowtown murders]], and his accomplices [[Robert Wagner (serial killer)|Robert Wagner]], [[James Vlassakis]], and [[Mark Haydon]].
* Jason Alexander Downie, repeatedly stabbed Andrew, Rose and Chantelle Rowe in a frenzied attack at their home in Kapunda.
* [[Bevan Spencer von Einem]]{{spaced ndash}}convicted murderer and suspected serial murderer.
* [[Jean Eric Gassy]]{{spaced ndash}}deregistered Sydney psychiatrist who shot dead South Australia's head of mental health in October 2002.
* [[David Hicks]]<ref>{{cite news|url=http://www.boston.com/news/world/latinamerica/articles/2007/03/30/hicks_sentence_limited_to_7_years/ |title=Australian Gitmo detainee gets 9 months |work=Boston Globe |date=30 March 2007 |deadurl=yes |archiveurl=https://web.archive.org/web/20070630113042/http://www.boston.com/news/world/latinamerica/articles/2007/03/30/hicks_sentence_limited_to_7_years/? |archivedate=30 June 2007 }}</ref> Hicks was released 29 December 2007.<ref>{{cite news |url=http://www.abc.net.au/news/stories/2007/12/29/2128751.htm?section=australia |title=Hicks wants privacy following release |publisher=Australian Broadcasting Corporation |date=29 December 2007}}</ref>{{spaced ndash}}the Australian [[Guantanamo Bay detainment camp|Guantánamo Bay]] detainee convicted by US military tribunal of [[providing material support for terrorism]].
* James William Miller<ref>{{cite web |url=http://www.crimelibrary.com/serial_killers/predators/worrell/index_1.html |title=The Truro Serial Murders |last=Kidd |first =Paul B. | publisher=Courtroom Television Network LLC}}</ref>{{spaced ndash}}served six consecutive life sentences for murder in relation to the [[Truro Murders]]. Died of cancer in 2008.
* [[Bradley John Murdoch]]{{spaced ndash}}convicted murderer of [[Peter Falconio]], was held briefly in the prison before being extradited.
* [[Max Stuart|Rupert Maxwell (Max) Stuart]]{{spaced ndash}} (1932-2014): an [[Australian Aborigine]] whose 1959 conviction for murder lead to a [[Royal Commission]] and a [[Black and White (2002 film)|2002 film]]; since paroled in 1973.

== Notes ==
{{reflist|2}}

== References ==
* {{cite book|title=ENFIELD and The Northern Villages|last=Lewis|first=H. John|publisher=The corporation of the city of Enfield|year=1985| isbn=0-85864-090-2}}

==See also==
* [[Hundred of Yatala]]
* [[District Council of Yatala]]

{{SouthAustralianPrisons}}

[[Category:1854 establishments in Australia]]
[[Category:Maximum security prisons in Australia]]
[[Category:Prisons in Adelaide]]
[[Category:South Australian Heritage Register]]