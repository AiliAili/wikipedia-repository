<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = A.W.35 Scimitar
  |image = AWScimitar.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Fighter
  |manufacturer = [[Armstrong Whitworth Aircraft|Armstrong Whitworth]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |national origin=United Kingdom
  |first flight = 1935
  |introduced = 1936<!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Norway]]
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 6
  |unit cost = 
  |developed from = [[Armstrong Whitworth A.W.16]]
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Armstrong Whitworth A.W.35 Scimitar''' was a [[United Kingdom|British]] single-engine [[biplane]] [[fighter aircraft]] designed and built by [[Armstrong Whitworth Aircraft]]. Four Scimitars were produced for the [[Norwegian Army Air Service]] and were delivered in 1936.

==Design and development==
The '''A.W.35 Scimitar''' was a development of [[Armstrong Whitworth Aircraft|Armstrong Whitworth]]'s earlier [[Armstrong Whitworth A.W.16]] fighter, powered by an [[Armstrong Siddeley Panther]] engine, with a lowered nose decking and an enlarged [[Vertical stabilizer|fin]] and [[rudder]]. The first prototype ''(G-ACCD)'' was a modification of the second A.W.16, and first flew in this form on 29 April 1935.<ref name="mason fighter">{{Cite book|author=Mason, Francis K|title=The British Fighter since 1912|publisher=Naval Institute Press|year=1992|isbn= 1-55750-082-7}}</ref> A second prototype (''G-ADBL'') was constructed by conversion of an A.W.16.

==Operational history==
Four Scimitars were ordered for the Norwegian Army Air Service. After testing of two of the production aircraft by the [[Aeroplane and Armament Experimental Establishment|A & AEE]] at [[RAF Martlesham Heath|Martlesham Heath]] in late 1935, they were delivered to Norway in 1936.<ref name="mason fighter"/> When the [[Norwegian Campaign|Germans invaded]] in 1940 the Scimitars were all undergoing maintenance and could not be made operational in time to see combat.

The second prototype Scimitar was preserved by Armstrong Whitworth at its Whitley factory until 1958, when it was scrapped.<ref name="jackson1">{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 1|year= 1974|publisher= Putnam|location= London|isbn=0-370-10006-9 }}</ref>

==Operators==
;{{flag|Norway}}
* [[Norwegian Army Air Service]]

==Specifications (A.W.35)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=The British Fighter since 1912 <ref name="mason fighter"/> 
|crew=One
|capacity=
|length main= 25 ft 0 in
|length alt= 7.62 m
|span main= 33 ft 0 in
|span alt= 10.06 m
|height main= 12 ft 0 in
|height alt= 3.66 m
|area main= 261 ft²
|area alt=24.3 m²
|airfoil=
|empty weight main= 2,956 lb
|empty weight alt= 1,341 kg
|loaded weight main= 4,100 lb
|loaded weight alt= 1,860 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)= [[Armstrong Siddeley Panther]] X
|type of prop= 14-cylinder [[radial engine]]
|number of props=1
|power main= 735 hp
|power alt= 548 kW
|power original=
|max speed main= 192 kn
|max speed alt= 221 mph, 356 km/h
|cruise speed main= 161 kn 
|cruise speed alt=185 mph, 298 km/h  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main=  
|range alt=  
|ceiling main=  
|ceiling alt=  
|climb rate main=  
|climb rate alt=  
|loading main= 15.7 lb/ft²
|loading alt= 76.8 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.179 hp/lb
|power/mass alt= 0.294 kW/kg
|more performance=* '''Climb to 10,000 ft:''' 5 min 15 sec
|armament=
* 2 × forward-firing [[.303 British|.303 in]] (7.7 mm) [[Vickers machine gun]]s
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=
*[[Armstrong Whitworth A.W.16]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{commons category|Armstrong Whitworth Scimitar}}
{{Reflist}}

{{Armstrong Whitworth aircraft}}

[[Category:British fighter aircraft 1930–1939]]
[[Category:Armstrong Whitworth aircraft|Scimitar]]
[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:Norwegian military aircraft 1930–1939]]