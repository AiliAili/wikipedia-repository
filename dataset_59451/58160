{{italic title}}
{{Infobox Journal
| title        = Journal of Health Care for the Poor and Underserved
| cover        = [[Image:Journal of health care for the poor and underserved.gif]]
| editor       = Virginia Brennan
| discipline   = [[Medicine]], [[health]], [[economics]], [[politics]]
| abbreviation = J. Health Care Poor Underserved
| publisher    = [[Johns Hopkins University Press]]
| country      = United States
| frequency    = Quarterly
| history      = 1990-present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = http://www.press.jhu.edu/journals/journal_of_health_care_for_the_poor_and_underserved/
| link1        = http://muse.jhu.edu/journals/journal_of_health_care_for_the_poor_and_underserved/
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 39082573
| LCCN         = 
| CODEN        = 
| ISSN         = 1049-2089
| eISSN        = 1548-6869
}}
The '''''Journal of Health Care for the Poor and Underserved''''' is an [[academic journal]] founded in 1990 by [[David Satcher]], then President of [[Meharry Medical College]] who later became the 16th [[Surgeon General of the United States]]. ''JHCPU'' is published by the [[Johns Hopkins University Press]] for Meharry and is affiliated with the [http://www.clinicians.org/ Association of Clinicians for the Underserved]. 

The journal covers the health and health care of medically underserved populations in North and Central America and the Caribbean, and topics such as access to health care, quality, costs, regulation, legislation, and [[disease prevention]]. Articles take the form of scholarly research and expert opinion, as well as policy analyses and book reviews. Each issue also contains an ACU Column and a Heroes and Great Ideas Column. 

The current editor is [[Virginia Brennan]] of [[Meharry Medical College]]. The journal is published quarterly in February, May, August, and November, with occasional supplemental issues. It is listed as one of the nation's leading ''Health Policy'' journals by the [[Kaiser Family Foundation]] and as an essential core journal in ''Public Health Practice'' by the Medical Library Association’s Core Public Health Journals Project.

==See also==
*[[Health disparities]]
*[[Health care delivery]]
*[[Health care system]]
*[[Meharry Medical College]]
*[[Historically Black Colleges and Universities]]

== External links ==
*[http://www.press.jhu.edu/journals/journal_of_health_care_for_the_poor_and_underserved/ ''JHCPU'' on the JHU Press website]
*[http://muse.jhu.edu/journals/journal_of_health_care_for_the_poor_and_underserved/ ''JHCPU'' ] at [[Project MUSE]]
*[http://phha.mlanet.org/activities/corejournal.html Medical Library Assn. Core Public Health Journal Project]

[[Category:Public health journals]]
[[Category:Research on poverty]]
[[Category:Quarterly journals]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1990]]

{{med-journal-stub}}