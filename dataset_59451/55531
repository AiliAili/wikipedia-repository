{{Infobox journal
| title = Andean Geology
| formernames = Revista Geológica de Chile
| cover =
| discipline = [[Geology]], [[Earth sciences]]
| abbreviation = Andean Geol.
| language = English, Spanish
| editor = Waldo Vivallo
| publisher = [[National Geology and Mining Service]]
| country = Chile
| history = 1974–present
| frequency = Triannually
| website = http://www.andeangeology.cl/
| link1 = http://www.andeangeology.cl/index.php/revista1/issue/current
| link1-name = Online access
| link2 = http://www.andeangeology.cl/index.php/revista1/issue/archive
| link2-name = Online archive
| link3 = http://www.scielo.cl/scielo.php?script=sci_serial&pid=0718-7106&lng=en&nrm=iso
| link3-name = Journal page at [[SciELO]]
| ISSN = 0716-0208
| eISSN = 0717-618X
| OCLC = 54108625
| LCCN = 2009255275
}}
'''''Andean Geology''''' (formerly '''''Revista Geológica de Chile''''') is a [[Peer review|peer-reviewed]] [[scientific journal]] published three times per year by the [[National Geology and Mining Service]], [[Chile]]'s geology and mining agency. The journal covers the field of [[geology]] and related [[earth science]]s, primarily on issues that are relevant to [[South America]], [[Central America]], and [[Antarctica]] with a particular focus on the [[Andes]].<ref name="autores">{{cite web|title=Instrucciones a Los Autores|url=http://www.andeangeology.cl/index.php/revista1/about/submissions|archive-url=https://web.archive.org/web/20120426041922/http://www.andeangeology.cl/index.php/revista1/about/submissions|dead-url=yes|archive-date=26 April 2012|work=Andean Geology|publisher=SERNAGEOMIN|accessdate=9 November 2011}}</ref> The journal was established in 1974 and articles are published in English and Spanish. The [[editor-in-chief]] is Waldo Vivallo (National Geology and Mining Service).

== References ==
{{Reflist}}

== External links ==
* {{Official website|https://web.archive.org/web/20120426041922/http://www.andeangeology.cl/}}

{{Use dmy dates|date=January 2012}}

[[Category:Geology journals]]
[[Category:Geology of South America]]
[[Category:Geology of Antarctica]]
[[Category:Publications established in 1974]]
[[Category:Andes]]
[[Category:Multilingual journals]]
[[Category:Open access journals]]
[[Category:Triannual journals]]
[[Category:Academic journals published by governments]]
[[Category:Academic journals published by non-profit organizations of Chile]]

{{Geology-stub}}
{{Chile-stub}}