{{good article}}
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name = A Puro Dolor
| Cover = A puro dolor single.jpg
| Artist = [[Son by Four]]
| Album = [[Son by Four (album)|Son by Four]]
| Released = February 1, 2000
| Format = [[Promo single]]
| Recorded = 1999
| Genre = [[Latin pop]], [[Salsa music|salsa]]
| Length = {{duration|m=3|s=32}} <small>(ballad version)</small> <br>{{duration|m=4|s=23}} <small>(original version)</small>
| Label = [[Sony Discos]]
| Writer = [[Omar Alfanno]]
| Producer =  Omar Alfanno
| Last single = "No Hay Razón" <br>(1998)</small>
| This single = "'''A Puro Dolor'''" <br>(2000)</small>
| Next single = "Miss Me So Bad" <br>(2000)</small> 
}}

"'''A Puro Dolor'''" is a song recorded by Puerto Rican band [[Son by Four]]. It was written by [[Omar Alfanno]] and released as the first single of the [[Son by Four (album)|second studio album]] of the band in 2000. Two versions of the track were produced for the album; one as a [[salsa music|salsa]] and the other as a ballad. The ballad version was arranged by Alejandro Jaén.

The song reached number-one on [[Top Latin Songs|''Billboard'' Top Latin Songs chart]], and became the longest running chart topper of its history, spending 20 weeks at the top; this record was broken five years later by Colombian singer-songwriter [[Shakira]] with "[[La Tortura]]" which spent 25 weeks at number-one. "A Puro Dolor" also reached the [[Billboard Hot 100|''Billboard'' 100]]; this led to the recording of an English-language version of the track "'''Purest of Pain'''", which was also charted in the United States.

The composition also met with critical acclaim receiving the [[Lo Nuestro|Lo Nuestro Award]], the [[Billboard Music Award]] and a [[Latin Grammy Awards|Latin Grammy Award]] nomination. In 2009, "A Puro Dolor" was named the best-performing Latin single of the 2000s decade in the United States. Two music videos were made for the ballad and the English versions. The record was [[cover version|covered]] by Mexican group Dinora y la Juventud and Brazilian boy band [[KLB]] both of which received radio airplay.

==Background and composition==
{{quote box|width=30%|quote=I wrote it in ten minutes, under deadline, with a bottle of water by my side.|source=Omar Alfanno, ''[[Billboard (magazine)|Billboard]]''.<ref name="mag">{{cite journal| last = Cobo | first = Leila | date = February 17, 2001|title = Son by Four, Anthony Top Latin Awards Finalists | journal = [[Billboard (magazine)|Billboard]] | publisher = [[Nielsen Company|Nielsen Business Media, Inc.]] | volume = 113 | issue = 7 | page = 77
| url = https://books.google.com/books?id=KBQEAAAAMBAJ&pg=PA77&dq=a+puro+dolor&cd=3#v=onepage&q=a%20puro%20dolor&f=false | accessdate =March 4, 2010}}</ref>
}} 
In 1998, [[Son by Four]] released their first album, ''Prepárense''. The success of the singles "Nada" and "No Hay Razón", distributed in Puerto Rico by the independent [[record label|label]] RJO, captured the attention of the [[Sony Music]] [[CEO]] Oscar Llord, who made an agreement with RJO to re-release the album and start the recording sessions of the [[Son by Four (album)|next album]]. The first single taken from the following album was "A Puro Dolor", written and arranged by Panamanian [[record producer]] [[Omar Alfanno]], with a ballad version arranged by Alejandro Jaén.<ref name="bio">{{cite web|title=Son by Four: Grupo Puertorriqueño de Música Pop|url=http://www.prpop.org/biografias/s_bios/son_by_four.shtml|accessdate=March 4, 2010 |language=Spanish|publisher=Fundación Nacional para la Cultura Popular}}</ref> Alfanno, well known for his work with [[salsa (music)|salsa]] recording singers [[Marc Anthony]], [[Víctor Manuelle]], [[Jerry Rivera]] and [[Gilberto Santa Rosa]] was in charge of the album production.<ref name="alfano">{{cite web|title=Ángel López, "es la voz"|url=http://www.pa-digital.com.pa/periodico/edicion-anterior/ey-interna.php?story_id=894398|accessdate=March 3, 2010|language=Spanish|archiveurl=https://web.archive.org/web/20120220094509/http://www.panamaamerica.com.pa/periodico/edicion-anterior/ey-interna.php?story_id=894398|archivedate=February 20, 2012|publisher=Grupo Editorial Epasa}}</ref> Alfanno declared that he wrote the song in ten minutes, and that after the success of the track, he analyzed the composition, since he did not understand how "A Puro Dolor" became that popular.<ref name="mag"/> He named it "the Cinderella of all his songs."<ref name="mag"/>

==Critical reception==
{{Listen
|filename=Son by Four - Purest of Pain.ogg
|title=Son by Four "Purest of Pain"
|description=A 21 second sample of the song, "Purest of Pain", the English version of "A Puro Dolor" by Son by Four. This version was recorded after the group's unexpected success with the original song.
|format=[[Ogg]]
}}
The song was released in the United States and Puerto Rico in February 2000. It became very successful with the ballad and tropical/salsa versions setting an airplay record for a Latin single with 23.4 million impressions in those countries. The reaction to the song was intense, and Son by Four was asked to record an English-language version of the track.<ref name="billboardmag1">{{cite journal| date = June 17, 2000| title = Son by Four: Turning Hard Work into Gain with "A Puro Dolor"| journal = [[Billboard (magazine)|Billboard]]| publisher = [[Nielsen Company|Nielsen Business Media, Inc.]] | volume = 112 | issue = 25| page = LM-6| url = https://books.google.com/books?id=mw8EAAAAMBAJ&pg=PA76&dq=a+puro+dolor&cd=2#v=onepage&q=a%20puro%20dolor&f=false
| accessdate = March 4, 2010}}</ref> This version, titled "Purest of Pain", was serviced to English-speaking radio stations in May 2000. The success of the song took by surprise the leader of the band, Ángel López, who told [[Billboard (magazine)|''Billboard'']] magazine: "When they told us we were going to be signed by [[Columbia Records|Columbia]] for the single, we flipped."<ref name="billboardmag2">
{{cite journal| date = June 17, 2000 | title = Son by Four
| journal = [[Billboard (magazine)|Billboard]] | publisher = [[Nielsen Company|Nielsen Business Media, Inc.]] | volume = 112
| issue = 25 | page = LM-14 | url = https://books.google.com/books?id=mw8EAAAAMBAJ&pg=PA84&dq=a+puro+dolor&cd=2#v=onepage&q=a%20puro%20dolor&f=false
| accessdate = March 4, 2010}}</ref> A [[ranchera]] version was also recorded for the [[Regional Mexican]] audience.<ref>{{cite web|url=http://www.allmusic.com/album/ranchera-hits-mw0000222527|title=Ranchera Hits - Various Artists: Overview|work=[[AllMusic]]|publisher=[[Rovi Corporation]]|accessdate=October 25, 2013}}</ref> About the song, López also declared that "has something everybody can relate to. We're adults, we've been all through that [rejection]. The way the song reaches out to people is outstanding."<ref name="billboardmag2"/> In his album review for [[AllMusic]], Evan C. Gutierrez declared that "A Puro Dolor" set "an impossibly high standard for the rest of the playlist."<ref>{{cite web|title=((( Son by Four > Overview ))) |url={{Allmusic|class=album|id=son-by-four-mw0000609100|pure_url=yes}}|accessdate=March 3, 2010|work=[[AllMusic]]|first=Evan C|last=Gutierrez|publisher=[[Rovi Corporation]]}}</ref>

Son by Four received seven [[Billboard Latin Music Awards|''Billboard'' Latin Music Award]] nominations in 2001 for "Hot Latin Track of the Year", "Pop Track of the Year" and "Tropical/Salsa Track of the Year" ("A Puro Dolor"); "Tropical/Salsa Album of the Year, New Artist" and "Tropical/Salsa Album of the Year, Group" (''Son by Four''); and "Billboard Latin 50 Artist of the Year" and "Hot Latin Track Artist of the Year". With further nominations for Alfanno as "Songwriter of the Year" and Jaén for "Producer of the Year".<ref name="mag"/> Son by Four won all the awards, and Alfanno won the award for the writing of the song.<ref>{{cite web|title=Son by Four Wins Big at Billboard Latin Awards|url=http://www.billboard.com/articles/news/79959/son-by-four-wins-big-at-billboard-latin-awards|accessdate=March 3, 2010|date=April 25, 2001|work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]]}}</ref>

At the [[Premio Lo Nuestro 2001|Lo Nuestro Awards of 2001]], the group earned six awards; among them "[[Lo Nuestro Award for Pop Song of the Year|Pop Song of the Year]]" and "[[Lo Nuestro Award for Tropical Song of the Year|Tropical Song of the Year]]".<ref name="wins-LN-2001">{{cite web |url=http://especiales.univision.com/contentuvn/rinconlatino/plnuestro/premiados_temp.html|title=Premios Lo Nuestro: Alfombra Roja:  Lista completa de los ganadores de Premio Lo Nuestro 2001 |work=[[Univision]] |publisher=[[Univision Communications|Univision Communications Inc.]]|accessdate=August 14, 2013|year=2001}}</ref> Alfanno also received a [[Latin Grammy Awards|Latin Grammy Award nomination]] for the [[Latin Grammy Award for Best Tropical Song|Best Tropical Song]], which it lost to "[[El Niágara en Bicicleta]]" by [[Juan Luis Guerra]].<ref>{{cite web|url=http://articles.latimes.com/2000/jul/08/entertainment/ca-49420/2|title=The Full List of Nominations|work=Los Angeles Times|publisher=Tribune Company|date=July 8, 2000|accessdate=November 21, 2013}}</ref><ref>{{cite web|url=http://www.mtv.com/news/articles/1431652/santana-luis-miguel-man225-lead-latin-grammy-winners.jhtml|title=Santana, Luis Miguel, Maná Lead Latin Grammy Winners|last=Basham|first=David|work=[[MTV]]|publisher=[[Viacom Media Networks]]|date=September 14, 2000|accessdate=November 21, 2013}}</ref> "A Puro Dolor" was given three [[Billboard Music Awards|''Billboard'' Music Award]] in 2000 for Latin Track of the Year, Latin Pop Track of the Year, and Tropical/Salsa Track of the Year.<ref>{{cite news|url=http://www.lasvegassun.com/news/2000/dec/06/billboard-winners/|title=Billboard winners|work=[[Las Vegas Sun]]|publisher=[[The Greenspun Corporation]]|date=December 6, 2000|accessdate=October 20, 2013}}</ref> At the 2001 [[ASCAP]] Latin Music Awards, Alfanno was awarded "Song of the Year" and "Pop/Balada Song of the Year" for his composition of the song and was named Songwriter of the Year.<ref>{{cite journal
|title=El Premio ASCAP Awards Arturo Sandoval, Songwriter Omar Alfanno
|url=https://books.google.com/?id=BhQEAAAAMBAJ&pg=PA50&dq=intitle:billboard+premio+ascap#v=onepage&q&f=false|work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]] |volume=112
|issue=21 |page=50 |date=May 19, 2001|accessdate=September 27, 2013}}</ref> In 2015, "A Puro Dolor" ranked #35 on ''Billboard'''s Best Latin Songs of All Time.<ref>{{cite web|title=The 50 Greatest Latin Songs of All Time|url=http://www.billboard.com/articles/events/greatest-of-all-time/6760654/best-latin-songs-of-all-time|website=Billboard|publisher=Prometheus Global Media|accessdate=November 12, 2015|date=November 12, 2015}}</ref>

==Promotion==
To promote the single, Son by Four opened a [[Ricky Martin]] live performance in the [[Madison Square Garden]] in New York City.<ref name="bio"/> They also did a concert with the [[R&B]] trio [[Destiny's Child]], and were presented in the [[Latin Grammy Awards of 2000|1st Latin Grammy Awards]] along with the [[boy band]] [[N'Sync]], performing the [[medley (music)|medley]] "A Puro Dolor"/"Purest of Pain" with "[[This I Promise You]]"/"Yo Te Voy a Amar".<ref name="bio"/> The song was featured in the Mexican telenovela ''Frente al Espejo'', broadcast by [[Televisa]].<ref name="bio"/> In 2000, due to the success of the track in the American market, they were asked to perform on ''[[The Tonight Show]]'' hosted by [[Jay Leno]].<ref name="bio"/> The salsa version of the song was included on their greatest hits album ''Salsa Hits'' (2001) and ''Lo Esencial'' (2008) and on the compilation album ''Salsahits 2001'' (2000).<ref>{{cite web|url=http://www.allmusic.com/song/a-puro-dolor-mt0004158664|title=A Puro Dolor - Son by 4|work=[[AllMusic]]|publisher=[[Rovi Corporation]]|accessdate=October 25, 2013}}</ref>

Two music videos were made for the song; the first one for the ballad version, and the other for the English version.<ref name="APuroVideo">{{cite web|url=https://www.youtube.com/watch?v=kAKVT1HWNsg|title=Son by Four&nbsp;– A Puro Dolor|publisher=[[YouTube]]|date=October 29, 2009|accessdate=May 23, 2013}}</ref><ref name="Purest Video">{{cite web|url=https://www.youtube.com/watch?v=YUGUySygZuU|title=Son by Four&nbsp;– Purest of Pain (A Puro Dolor)|publisher=[[YouTube]]|date=October 29, 2009|accessdate=May 23, 2013}}</ref> The ballad version was directed by Felipe Niño.<ref name="bio"/> In the video, the group is shown performing the song along with an orchestra behind them. It is filmed on a stage where shows López singing his verses, while the others members of the group, follow up with the chorus. At the end of the video the people of the orchestra starts to applaud when the group ends to sing.<ref name="APuroVideo"/> In the English version of the video, the plot is completely different. The video starts in a cloudy afternoon after a storm, where the group is situated in a white room. It shows López singing in a ladder when it later starts to fill with water. At the last part of the video, it shows Son by Four singing out of the house after the storm has ended.<ref name="Purest Video"/>

==Chart performance==
{{further|Billboard Top Latin Songs Year-End Chart}}
"A Puro Dolor" reached the top spot of the [[Top Latin Songs|''Billboard'' Top Latin Songs]], Latin Pop Songs and Tropical/Salsa charts with the song spending 20 weeks at number-one on the former chart.<ref name="son"/><ref name="caliente"/> This chart run led the song to be named the best-performing Latin single of the 2000s decade.<ref>{{cite journal|url=https://books.google.com/?id=EX4yD1pJCxUC&pg=RA11-PA160&dq=intitle:billboard+a+puro+dolor+te+quiero+me+enamora#v=onepage&q&f=false|title=The Decade in Music|work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]] |date=October 3, 2009|accessdate=October 20, 2013|volume=121|issue=39|page=160}}</ref> In the aforementioned chart only four songs have had similar week runs at the top: "[[Me Enamora]]" by [[Juanes]], "[[Te Quiero (Flex song)|Te Quiero]]" by [[Flex (singer)|Flex]], and "[[Bailando (Enrique Iglesias song)|Bailando]]" by [[Enrique Iglesias]] featuring [[Descemer Bueno]] and [[Gente De Zona]] with 20 weeks each, and "[[La Tortura]]" by [[Shakira]] featuring [[Alejandro Sanz]] with 25 non-consecutive weeks at the top.<ref name="caliente">{{cite web |title=Más Calientes |work=[[Telemundo]] |publisher=[[NBCUniversal]] |year=2010|language=Spanish |url=http://msnlatino.telemundo.com/entretenimiento/Billboard_en_Espanol/article/2010-09/Mas_calientes |accessdate=October 20, 2010}}</ref> "Purest of Pain", the English version of the track, peaked at number twenty-six in the [[Billboard Hot 100|''Billboard'' Hot 100]] chart and ranked 61 at the [[Billboard Year-End Hot 100 singles of 2000|''Billboard'' Year-End Hot 100 singles of 2000]].<ref name="son">{{cite web|title=((( Son by Four > Charts & Awards > Billboard Singles > ))) |url={{Allmusic|class=artist|id=son-by-4-mn0000036702|tab=awards|pure_url=yes}}|accessdate=March 4, 2010|work=[[AllMusic]]|publisher=[[Rovi Corporation]]}}</ref><ref name="usyearend"/> This exposure led to the parent album to receive a [[Gold certification]] in Argentina,<ref name="capif">{{cite web|url=http://www.capif.org.ar/Default.asp?PerDesde_MM=0&PerDesde_AA=0&PerHasta_MM=0&PerHasta_AA=0&interprete=&album=&LanDesde_MM=1&LanDesde_AA=1980&LanHasta_MM=12&LanHasta_AA=2010&Galardon=O&Tipo=1&ACCION2=+Buscar+&ACCION=Buscar&CO=5&CODOP=ESOP|archiveurl=https://web.archive.org/web/20110706084844/http://www.capif.org.ar/Default.asp?PerDesde_MM=0&PerDesde_AA=0&PerHasta_MM=0&PerHasta_AA=0&interprete=&album=&LanDesde_MM=1&LanDesde_AA=1980&LanHasta_MM=12&LanHasta_AA=2010&Galardon=O&Tipo=1&ACCION2=+Buscar+&ACCION=Buscar&CO=5&CODOP=ESOP|archivedate=July 6, 2011|title = Discos de oro y platino|language=Spanish|accessdate=November 4, 2012|publisher=[[Cámara Argentina de Productores de Fonogramas y Videogramas]]}}</ref> Mexico,<ref name="amprfon">{{Certification Cite|region=Mexico|title=Son by Four|certyear=2000|accessdate=October 20, 2013}}</ref> and the United States.<ref name="riaa">{{Certification Cite|region=United States|title=Son by Four|artist=Son by Four|accessdate=October 20, 2013}}</ref> "A Puro Dolor" ranked 1st at the Hot Latin Songs 25th Anniversary chart.<ref>{{cite web|url=http://msnlatino.telemundo.com/entretenimiento/Billboard_en_Espanol/photo_gallery/2011-10/25_anos_de_musica_caliente |title=25 años de música caliente|language=Spanish |work=[[Telemundo]] |publisher=[[NBCUniversal]] |accessdate=January 14, 2014}}</ref> "Purest of Pain" was also a modest success in Europe where it peaked at number seven and eighteen in the Flanders and Wallonia regions of Belgium and number three in the Netherlands.<ref name="Flanders"/><ref name="Wallonia"/><ref name="Dutch"/>

== Formats and track listings ==
{{Track listing
| headline        = CD, Maxi-Single <ref>{{cite web |url=http://www.cduniverse.com/search/xx/music/pid/1507753/a/productinfo.asp?pid=8810109&style=music&refcode=rf_FOGERTY|title=Son by Four – Purest of Pain (CD single) |work=[[CD Universe]] |accessdate=October 22, 2013}}</ref>
| title1          = Purest of Pain (A Puro Dolor) [Spanglish]
| length1         = 3:30
| title2          = Purest of Pain (A Puro Dolor) [Bilingual]
| length2         = 3:30
| title3          = Purest of Pain (A Puro Dolor) [Living without you]
| length3         = 3:30
}}

==Charts==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable sortable"
!align="left"|Chart (2000)
!align="left"|Peak<br />position
|-
|align="left"|US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref name="son"/>
<small>''"Purest of Pain", English version''</small>
|align="center"|26
|-
|align="left"|US [[Hot Latin Songs]] (''Billboard'')<ref name="son"/>
|align="center"|1
|-
|align="left"|US [[Latin Pop Songs]] (''Billboard'')<ref name="son"/>
|align="center"|1
|-
|align="left"|US [[Regional Mexican Songs]] (''Billboard'')<ref name="son"/>
|align="center"|34
|-
|align="left"|US [[Tropical Songs]] (''Billboard'')<ref name="son"/>
|align="center"|1
|-
|}

{| class="wikitable sortable"
!align="left"|Chart (2001)
!align="left"|Peak<br />position
|-
{{singlechart|Flanders Tip|7|artist=Son by Four|song=Purest of Pain|refname="Flanders"|note="Purest of Pain"|accessdate=October 21, 2013}}
|-
{{singlechart|Wallonia|18|artist=Son by Four|song=Purest of Pain|refname="Wallonia"|note="Purest of Pain"|accessdate=October 21, 2013}}
|-
{{singlechart|Dutch100|3|artist=Son by Four|song=Purest of Pain|refname="Dutch"|note="Purest of Pain"|accessdate=October 21, 2013}}
|-
|}
{{col-2}}

===Year-end charts===
{| class="wikitable sortable"
!align="left"|Chart (2000)
!align="center"|Position
|-
|US ''Billboard'' Hot 100<ref name="usyearend">{{cite journal|url=https://books.google.com/books?id=ehEEAAAAMBAJ&dq=intitle%3Abillboard+puro+dolor&q=purest+of+pain#v=onepage&q&f=false|title=2000: The Year in Music|work=Billboard|publisher=Prometheus Global Media|volume=112|issue=53|pages=YE–48, 72, 74, 78|date=December 20, 2000|accessdate=October 20, 2013}}</ref><br><small>''"Purest of Pain", English version</small><br />
|align="center"|61
|-
|US Hot Latin Songs (''Billboard'')<ref name="usyearend"/>
|align="center"|1
|-
|US Latin Pop Songs (''Billboard'')<ref name="usyearend"/>
|align="center"|1
|-
|US Tropical Songs (''Billboard'')<ref name="usyearend"/>
|align="center"|1
|-
|}

{| class="wikitable "
!align="left"|Chart (2001)
!align="center"|Position
|-
|Netherlands (Mega Single Top 100)<ref name="dutchyearend">{{cite web|url=http://dutchcharts.nl/jaaroverzichten.asp?year=2001&cat=s|title=Jaaroverzichten - 2001|language=Dutch|year=2001|accessdate=October 20, 2013|work=[[MegaCharts]]|publisher=HungMedia}}</ref>
|align="center"|20
|-
|US Hot Latin Songs (''Billboard'')<ref name="yearend2001">{{cite journal|url=https://books.google.com/books?id=sBIEAAAAMBAJ&dq=billboard&q=son+by+four#v=onepage&q&f=false|title=The Year in Music 2001|work=Billboard|publisher=Prometheus Global Media|volume=112|issue=53|page=YE-37,39|date=December 29, 2001|accessdate=April 28, 2013}}</ref>
|align="center"|17
|-
|US Latin Pop Songs (''Billboard'')<ref name="yearend2001"/>
|align="center"|10
|-
|}
{{col-end}}

== Credits and personnel ==
The credits are adapted from the ''Son by Four'' liner notes.<ref name="onthe6liner">{{cite AV media notes |title=Son by Four| |others=Son by Four |date=2000 |type=Album liner notes|language=Spanish |publisher=Sony Discos| location=Puerto Rico|asin=B00004TRVY}}</ref>

*Oscar Llord - executive producer
*[[Omar Alfanno]] – [[songwriting]], [[record producer|production]]
*Ángel López – [[singing|lead vocals]]
*Son by Four – chorus
*E. Ender - songwriting ("Purest of Pain")
*M. Llord - songwriting ("Purest of Pain")
{{col-begin}}
{{col-2}}
;Ballad version
*J. Salazar – [[arranger|arranging]]
*R. Bravo – [[drum kit]], [[Percussion instrument|percussion]]
*L. Quintero – [[acoustic guitar]], [[electric guitar]]
*Miami Symphonic Services – [[String instrument|string arrangement]]
*A. Oliva – string coordinator
*Richie Perez - [[Audio engineer|engineering]]
*M. Couzzi - [[Audio mixing|mixing]]
*Alejandro Jaén - co-production, [[Music director|direction]]
*[[Lewis Martineé]] - vocal production, mixing ("Purest of Pain")
{{col-2}}
;Salsa version
*Ceferino Caban – arrangement
*Jose David Carrion – [[piano]]
*Jose Gazmey – [[bass guitar]]
*David Marcano – [[timbales]]
*Eluid Velazquez – [[conga]], [[bongos]]
*Gustavo Lopez - [[trumpet]]
*Danny Fuentes - [[trombone]]
{{col-end}}

==Other recordings==
In 2000, Mexican group Dinora y la Juventud performed a cover "A Puro Dolor" in [[cumbia]] music. Their version peaked at number forty-six on the Hot Latin Songs chart and number fifteen on the [[Regional Mexican Songs|''Billboard'' Regional Mexican Songs]] chart.<ref name="dinoracharts">{{cite web|url={{BillboardURLbyName|artist=dinora y la juventud|chart=all}}|title=Dinora y la Juventud - Chart history|work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]] |accessdate=October 20, 2013}}</ref> The success of their cover led to the group receiving a [[Lo Nuestro Awards|Lo Nuestro]] Award for [[Lo Nuestro Award for Regional Mexican New Artist of the Year|New Regional Mexican Artist of the Year]].<ref name="wins-LN-2001"/> A music video for the song was filmed for the group.<ref>{{cite video | people = Dinora y la Juventud (performer)| title = A Puro Dolor| medium = Television| publisher = [[Fonovisa Records|Fonovisa]]| |year = 2000|language=Spanish }}</ref>

Piska, a Brazilian producer and composer, wrote the lyrics for a cover version of the song, which was recorded by the boy band [[KLB]], and released in their [[KLB (2000)|eponymous debut album]] under the title "A Dor Desse Amor".<ref>{{cite journal | url=http://www.istoe.com.br/reportagens/15505_ANONIMOS+DE+SUCESSO | title=Anônimos de sucesso | date=March 6, 2002 | number=1692 | journal=[[ISTOÉ]] | accessdate=January 14, 2014 | language=Portuguese}}</ref><ref>{{cite web | url=http://www.torropublicidade.com.br/klb3d/discografia/10/klb-2000 | title=Discografia - KLB 2000 | publisher=KLB official site | accessdate=January 14, 2014 | language=Portuguese}}</ref> It was later released as a single, and became the group's first single to play in radio stations in the country.<ref>{{cite web | url=http://www1.folha.uol.com.br/fsp/ribeirao/ri2004200122.htm | title=Romântico, KLB faz show no Comercial | date=April 20, 2001 | work=[[Folha de S.Paulo]] | publisher=[[Grupo Folha]] | accessdate=January 14, 2014 | language=Portuguese}}</ref> The cover became a hit; it was the tenth most-played song in Brazilian radios in 2000.<ref>{{cite web | url=http://www1.folha.uol.com.br/folha/pensata/magaly_20001230_100_musicas.html | title=As 100 mais tocadas  nas rádios brasileiras | author=Prado, Magaly | work=Folha de S.Paulo | publisher=Grupo Foha | accessdate=January 14, 2014 | language=Portuguese}}</ref> A music video, directed by [[Preta Gil]], was filmed for KLB.<ref>{{cite web | url=https://www.youtube.com/watch?v=4IedtotgByk | title=KLB&nbsp;– Clip A dor desse amor | date=February 2, 2007 | publisher=YouTube | accessdate=March 6, 2014}}</ref>

In 2009, Son by Four recorded a new version of the track for their studio album ''Abba Nuestro'' which did not have López's involvement as he left the band in 2003.<ref>{{cite web|url=http://www.allmusic.com/album/abbanuestro-mw0002079475|title=Abba Nuestro - Son by 4: Overview|work=[[AllMusic]]|publisher=[[Rovi Corporation]]|accessdate=October 20, 2013}}</ref><ref name="sonbyfour-musica">{{cite web |url=http://www.peopleenespanol.com/article/son-four-regresa-con-musica-cristiana|title=Son by Four regresa con música cristiana|work=[[People (magazine)|People]] |publisher=[[Time Inc.]]|accessdate=January 13, 2014|date=November 30, 2007|language=Spanish }}</ref> López himself recorded a new version of the track on his studio album ''Historias de Amor'' (2010) which features a cover of Alfanno's other compositions which was produced by Alfanno.<ref>{{cite web|url=http://www.allmusic.com/album/historias-de-amor-mw0001957264|title=Historias de Amor - Angel Lopez: Overview|work=[[AllMusic]]|publisher=[[Rovi Corporation]]|accessdate=October 20, 2013}}</ref> He also recorded it as a duet with fellow Puerto Rican singer [[Ana Isabelle]] on her studio album ''Mi Sueño'' (2010).<ref>{{cite web|url=http://www.allmusic.com/album/mi-sue%C3%B1o-mw0001959226|title=Mi Sueño - Ana Isabelle: Overview|work=[[AllMusic]]|publisher=[[Rovi Corporation]]|accessdate=October 20, 2013}}</ref>

==See also==
* [[Billboard Top Latin Songs Year-End Chart]]
* [[List of number-one Billboard Hot Latin Tracks of 2000]]
* [[List of number-one Billboard Hot Latin Pop Airplay of 2000]]
* [[List of number-one Billboard Latin Tropical Airplay of 2000]]

==References==
{{Reflist|2}}

[[Category:2000s ballads]]
[[Category:2000 singles]]
[[Category:Billboard Hot Latin Songs number-one singles]]
[[Category:Billboard Hot Latin Songs number-one singles of the year]]
[[Category:Billboard Tropical Songs number-one singles]]
[[Category:Son by Four songs]]
[[Category:Songs written by Omar Alfanno]]
[[Category:Spanish-language songs]]
[[Category:Pop ballads]]
[[Category:Sony Discos singles]]
[[Category:Telenovela theme songs]]
[[Category:2000 songs]]