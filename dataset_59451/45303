{{Use dmy dates|date=April 2012}}
{{Infobox military person
| name          =George Montague Cox
| image         =Sopwith F-1 Camel 2 USAF.jpg
| image_size    =280
| caption       =Sopwith Camel bi-plane fighter aircraft
| birth_date          = <!-- {{Birth date and age|df=yes|YYYY|MM|DD}} -->31 October 1892
| death_date          = <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} -->19 February 1977 (aged 84)
| placeofburial_label =
| placeofburial =
| birth_place  =[[Calcutta]], [[India]]
| death_place  =[[Huntingdon]], [[Cambridgeshire]], [[England]]
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    =England
| branch        =Flying service
| serviceyears  =
| rank          =[[Wing commander (rank)|Wing Commander]]
| unit          =[[No. 65 Squadron RAF|No. 65 Squadron]]
| commands      =
| battles       =
| awards        =[[Member of the Order of the British Empire]]<br>[[Military Cross]]<br>[[Air Force Cross (United Kingdom)|Air Force Cross]]
| relations     =
| laterwork     =
}}
'''George Montague Cox''' [[MBE]], [[MC]], [[Air Force Cross (United Kingdom)|AFC]], [[Order of the Crown (Belgium)]] was a British [[World War I]] [[flying ace]] credited with five aerial victories.<ref name=aerodrome>{{cite web |url=http://www.theaerodrome.com/aces/england/cox2.php |title=George Cox |work=The Aerodrome |accessdate=4 February 2010}}</ref>

==Early life==
Cox was born in [[Calcutta, India]], on 31 October 1892. He was the son of Horace and Alice Emily Cox.{{Citation needed|date=December 2015}}

==Military career==
Cox served originally in the [[Royal Berkshire Regiment]], being commissioned on 18 December 1914.<ref>{{LondonGazette|issue=29015|startpage=10931|date=22 December 1914}}</ref> In 1916, he transferred to the [[Royal Flying Corps]]. He received [[Royal Aero Club]] Aviator's Certificate 3847 in a [[Maurice Farman]] biplane at Military School, [[Catterick Bridge]] on 20 November 1916. Posted to [[No. 65 Squadron RAF|No. 65 Squadron]] in 1917, where he scored five victories flying the [[Sopwith Camel]].<ref>Shores (1990), p,124</ref>

After training as a fighter pilot, he was posted in 1917 to 65 Squadron where he flew Sopwith Camels; particularly no. B2411. He tallied his first victory on 15 November 1917.<ref name="Henshaw 1995, pp.249–250">Henshaw (1995), pp.249–250</ref> He scored four more in the period to 17 May 1918, with his last one recorded while flying Camel no. C8272.<ref>Bowyer (1998), p.169, 199, 225, 230</ref> Cox was awarded the [[Military Cross]] for his bravery and success.<ref name="ReferenceA">{{LondonGazette|issue=13296|startpage=2662|date=29 July 1918|city=e}}</ref><ref>[https://www.flightglobal.com/FlightPDFArchive/1918/1918%20-%200852.PDF  Flight Archive – G M Cox]</ref>
[[File:Sopwith F.1 Camel drawing.jpg|thumb|305px|right|Sopwith Camel.]]
{| class="wikitable" style="font-size: 9pt; text-align:center"
|-
! scope="col" | Date
! scope="col" | Time
! scope="col" | Squadron
! scope="col" | Aircraft
! scope="col" | Opponent
! scope="col" | Destroyed or Out of Control
! scope="col" | Location
|-
|15 November 1917<ref name="Henshaw 1995, pp.249–250"/> ||0740 ||65 || Sopwith Camel (B2411)|| Albatros D.III|| (OOC) ||Dadizeele
|-
|14 January 1918<ref>Bowyer (1998), p.199</ref> ||1155 ||65|| Sopwith Camel (B2411) ||C ||(DES) ||Westroosebeke
|-
|9 March 1918<ref>Bowyer (1998), p.225</ref> ||1010|| 65|| Sopwith Camel (B2411)|| Fokker DR.I ||(DES)|| Becelaere-Dadizeele
|-
|13 March 1918<ref>Bowyer (1998), p.230</ref>|| 1605 ||65 ||Sopwith Camel (B2411) ||C ||(OOC)|| Houthem
|-
|17 May 1918<ref name="Shores 1990, p.124">Shores (1990), p.124</ref>|| 1045 ||65|| Sopwith Camel (C8272)|| Pfalz D.III|| (DES)|| Demuin
|-
|}

He then was transferred within theatre to command No. 2 Test Flight RAF. Cox married Marjorie Evans on 16 April 1919 at [[Acton, London|Acton]] in [[Middlesex]], they had two sons. He was transferred to the unemployed list on 16 June 1919.<ref>{{LondonGazette|issue=31788|startpage=2074|date=20 February 1920}}</ref>

Cox was commissioned as [[flying officer]] in the General Duties Branch, Class A of the [[Royal Air Force]] Reserve of Air Force Officers on 6 March 1928,<ref>{{LondonGazette|issue=33363|startpage=1583|date=6 March 1928}}</ref> being confirmed in the rank on 6 September 1928,<ref>{{LondonGazette|issue=33422|startpage=6098|date=18 September 1928}}</ref> and [[flight lieutenant]] on 6 August 1929.<ref>{{LondonGazette|issue=33523|startpage=5149|date=6 August 1929}}</ref>   On 6 March 1933 he was transferred to Class C of the Reserve.<ref>{{LondonGazette|issue=33921|startpage=1781|date=14 March 1933}}</ref>
[[File:THE Military Cross MOD 45147519.jpg|thumb|right|105px|Military Cross.]]
[[File:MBE George 6th.jpg|thumb|MBE in its presentation case.]]
During [[World War II]] Cox was serving with the [[Royal Air Force]] General Duties Branch of the Reserve of Air Force Officers as a flying instructor when his tireless work was recognized with the award of an [[Air Force Cross (United Kingdom)|Air Force Cross]].<ref name="ReferenceB">{{LondonGazette|issue=34966|startpage=5959|date=11 October 1940}}</ref> He was promoted to [[squadron leader]] in March 1941,<ref>{{LondonGazette|issue=35102|startpage=1451|date=11 March 1941}}</ref> and [[Wing commander (rank)|wing commander]] on 1 June 1942.<ref>{{LondonGazette|issue=35618|startpage=2927|date=3 July 1942}}</ref>

Cox retired as wing commander on 10 February 1954.<ref>{{LondonGazette|issue=40198|supp=yes|startpage=3408|date=4 June 1954}}</ref>

==Later life==
In June 1958, having been Assistant Airport Manager, Prestwick Airport and Ministry of Transport and Civil Aviation, George Montague Cox was appointed an Ordinary Member of the Civil Division of the [[Most Excellent Order of the British Empire]] (MBE).<ref name="ReferenceC">{{LondonGazette|issue=41404|supp=yes|startpage=3527|date=3 June 1958}}</ref> He died on 19 February 1977, aged 84, at Huntingdon, in Cambridgeshire.<ref name="Shores 1990, p.124"/><ref>England & Wales, Registry of Deaths, 1977</ref>

==Honours and awards==
* [[Military Cross]] (MC) awarded in July 1918 as T./Capt. George Montague Cox, Gen. List and R.A.F.<ref name="ReferenceA"/>

 For conspicuous gallantry and devotion to duty. On numerous occasions during recent operations he has descended to very low altitudes, and has attacked with bombs and machine-gun fire enemy troops forming up for attack. Thanks to his dash and intrepidity hostile bodies of troops have suffered very severe casualties and have been scattered in all directions. He has in all destroyed four hostile machines, and has at all times displayed the greatest gallantry.<ref name=aerodrome/>

* [[Air Force Cross (United Kingdom)|Air Force Cross]]  (AFC) awarded in October 1940 as [[Flight Lieutenant]] George Mantague Cox, MC (70144), Reserve of Air Force Officers.<ref name="ReferenceB"/>
* Officer of the [[Order of the Crown (Belgium)]] awarded in March 1951.<ref>{{LondonGazette|issue=39175|startpage=1443|date=16 March 1951}}</ref>
* Ordinary Member of the Civil Division of the [[Most Excellent Order of the British Empire]] (MBE).<ref name="ReferenceC"/>

==References==
{{reflist|2}}

===Sources===
* [http://www.flightglobal.com/pdfarchive/view/1918/1918%20-%200852.html Flightglobal Archive Flight 1 August 1918]
* [http://www.london-gazette.co.uk/issues/35102/pages/1451/page.pdf The London Gazette 11 March 1941]

==Bibliography==
* {{cite book |last=Bowyer |first=Chaz |year=1998 |title=Royal Flying Corps Communiques 1917–18 |publisher=Grub Street |location=London |isbn=1-898697-79-5}}
* {{cite book |last=Henshaw |first=Trevor |year=1995 |title=The Sky Their Battlefield |publisher=Grub Street |location=London |isbn=1-898697-30-2}}
* {{cite book |last=Shores |first=Christopher |year=1990 |title=Above The Trenches |publisher=Grub Street |location=London |isbn=0-948817-19-4}}

{{DEFAULTSORT:Cox, George M.}}
[[Category:1892 births]]
[[Category:1977 deaths]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:People from Huntingdon]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:Recipients of the Military Cross]]
[[Category:Members of the Order of the British Empire]]