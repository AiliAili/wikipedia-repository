<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin 
|name =RWD-16
|image =Rwd16.jpg
|caption =RWD-16 in its initial shape
}}{{Infobox Aircraft Type 
|type =Sports plane 
|manufacturer =[[Doświadczalne Warsztaty Lotnicze|DWL]]
|national origin = Poland
|designer = [[RWD (aircraft manufacturer)|RWD]] team
|first flight =[[1936 in aviation|1936]]
|introduced =
|retired =
|status = prototype
|primary user =Poland
|more users = 
|produced =
|number built =1
|unit cost =
|variants with their own articles = [[RWD-21|RWD-16bis]]
}}
|} 
The '''RWD-16''' was a [[Poland|Polish]] two-seat low-wing sports plane of [[1936 in aviation|1936]], constructed by the [[RWD (aircraft manufacturer)|RWD]] team, that remained a prototype.

==Development==
The aircraft was designed in [[1935 in aviation|1935]] by Andrzej Anczutin of the [[RWD (aircraft manufacturer)|RWD]] bureau, as a light and economical sports plane. The plane was a wooden low-wing monoplane, with two seats side-by-side in a closed cockpit, powered by 50&nbsp;hp [[Walter Mikron]] I straight engine.<ref name=glass>Glass, A., op.cit., p. 322-323.</ref>

The prototype was built and first flown in early [[1936 in aviation|1936]] (registration SP-AXY), funded by the Polish division of [[Osram]] factory. The plane did not appear a successful design, though. Test revealed lack of directional stability, therefore its rudder was much enlarged, the wings were fitted with fixed [[Leading edge slats|slats]] and a windshield was redesigned. It did not improve the situation much, and in 1937-1938 the prototype was rebuilt and fitted with stronger 60&nbsp;hp Avia 3 engine in a lengthened nose, while the vertical stabilizer and rudder were made smaller. Most significant feature became a front windshield with a negative slope.<ref name=glass/>

After final changes, the prototype still was not satisfactory, but it served as a basis for the [[RWD-21|RWD-16bis]] design, which was a new, redesigned aircraft, produced as the [[RWD-21]]. The prototype RWD-16 was given then to a known touring aviator [[Zbigniew Babiński]].<ref name=glass/>

==Description==
Wooden construction low-wing cantilever [[monoplane]], conventional in layout, with a fixed landing gear and a closed cockpit. The fuselage was semi-[[monocoque]], plywood-covered. Single-piece trapezoid wings with rounded tips, two-spar, plywood (in front) and canvas covered. Conventional cantilever [[empennage]], plywood (fins) and canvas (elevators and rudder) covered. Two seats side-by-side, with twin controls, under a common canopy, with a fixed windshield. Conventional fixed [[landing gear]] with a rear skid.<ref name=glass/>

{{convert|50|hp|abbr=on}} [[Walter Mikron]] I inline engine in front, with two-blade wooden propeller Szomański, 1.8 m diameter. In later variant, {{convert|60|hp|abbr=on}} Avia 3 inline engine was installed.

==Specifications ==

{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Glass, A. (1977), p. 323
|crew=1, pilot
|capacity=1, passenger or co-pilot
|payload main=
|payload alt=
|length main= 7.51 m
|length alt= 
|span main= 11.8 m
|span alt= 
|height main= 2.75 m
|height alt= 
|area main= 15.3 m²
|area alt= 
|airfoil=
|empty weight main= 325 kg
|empty weight alt= 
|loaded weight main= 610 kg
|loaded weight alt= 
|useful load main=285 kg
|useful load alt=
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)= [[Avia 3]]
|type of prop=4-cylinder air-cooled inverted [[straight engine]]
|number of props=1
|power main= 60 hp
|power alt= kW
|power original=
|max speed main=145 km/h
|max speed alt= 
|cruise speed main=120 km/h
|cruise speed alt=
|stall speed main=67 km/h
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|range main= 750 km
|range alt= 
|ceiling main=
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main= 40 kg/m²
|loading alt=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==References==
{{reflist}}
{{refbegin}}
*Andrzej Glass: "Polskie konstrukcje lotnicze 1893-1939" (''Polish aviation constructions 1893-1939''), WKiŁ, Warsaw 1977, p.&nbsp;322-323 {{pl icon}}
{{refend}}

<!-- ==See also== -->
{{aircontent|
|related=
|similar aircraft=
|lists=
|see also=
}}

==External links==
{{commons category|RWD-16}}
*[http://www.airwar.ru/enc/law1/rwd16.html Photos and drawings at Ugolok Neba]

{{RWD aircraft}}

[[Category:Polish sport aircraft 1930–1939]]
[[Category:RWD aircraft|RWD-16]]