<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Me 109
 | image=File:Mecklenburger Ultraleicht Peak Aerospace Messerschmitt Me109 R.jpg
 | caption=Me 109R
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[Germany]]
 | manufacturer=[[ Peak Aerospace]]<br />[[Classic Planes]]
 | designer=Tassilo Bek
 | first flight=1991
 | introduced=
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= 
 | number built= 
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]40,990 (assembled, 2015)
 | developed from= [[Messerschmitt Bf 109]]
 | variants with their own articles=
}}
|}
[[File:ILA 2008 PD 726.JPG|right|thumb|Me 109R]]
[[File:ILA 2008 PD 756.JPG|right|thumb|Me 109R]]
[[File:ILA 2008 PD 757.JPG|right|thumb|Me 109R]]
[[File:ILA 2008 PD 759.JPG|right|thumb|Me 109R]]
The '''Peak Aerospace Me 109R''' is a family of [[Germany|German]] replica [[warbird]] [[ultralight aircraft]] that was designed by Tassilo Bek, and originally produced by [[Peak Aerospace]] of [[Pasewalk]]. The company since changed its name to [[Classic Planes GmbH]] . The design, first flown in 1991, is an 80% scale replica of the [[Second World War]] [[Messerschmitt Bf 109]] and is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', pages 68 and 115. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', pages 38 and 104. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
The aircraft was designed to comply with the [[Fédération Aéronautique Internationale]] microlight rules. It features a cantilever [[low-wing]], a single-seat enclosed cockpit, retractable [[conventional landing gear]] and a single engine in [[tractor configuration]].<ref name="WDLA11" /><ref name="WDLA15"/>

The first replica was inspired by Bek's inspection of a [[Loehle 5151 Mustang]] scale replica fighter at [[AirVenture]] in the late 1980s. He was impressed with the scale warbird, but wanted a German aircraft and so returned home and set out to design an Me 109 replica.<ref name="Hist">{{cite web|url=http://www.meck-ul.de/index.php/en/me-109 |title=Me 109 |publisher=Meck-ul.de |date=2012-07-17 |accessdate=2012-08-13}}</ref>

The first prototype, registered D-MBAK, was of all wooden construction, powered by a [[Hirth 2704]] producing only {{convert|40|hp|kW|0|abbr=on}} and first flew in 1991. This aircraft achieved {{convert|190|km/h|mph|0|abbr=on}} on its low power output, but Bek started a second prototype based on lessons learned in 1992. The second aircraft, registered D-MYBB, first flew in 1992, but was lost in an accident that same year. Bek's third prototype, D-MNBP, was flown in about 1994 and resulted in some kits being sold.<ref name="Hist" />

Bek flew a homebuilt category version of the Me 109 in 1996, powered by a [[Hirth F30]] and later a [[Subaru]] automotive conversion, which became the prototype of the Me 109R kit aircraft. Bek then sold the company and the new owner ceased development of the Me 109. In 2003 Christian Engelen purchased the project and continued work on the aircraft, officially re-launching it in 2004. Since then production has continued on a demand basis.<ref name="Hist" />

The current production version, the Me 109R is made from composites. Its {{convert|8.10|m|ft|1|abbr=on}} span wing has an area of {{convert|10.53|m2|sqft|abbr=on}} and [[Flap (aircraft)|flaps]]. Standard engines available include the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] [[two-stroke]], the {{convert|70|hp|kW|0|abbr=on}} [[Weber Motor MPE 750]] and the {{convert|80|hp|kW|0|abbr=on}} [[D-Motor LF26]] [[four-stroke]] powerplants.<ref name="WDLA11" /><ref name="WDLA15"/><ref name="spec">{{cite web|url=http://www.meck-ul.de/index.php/en/me-109/spezifikationen |title=Description |publisher=Meck-ul.de |date=2012-07-17 |accessdate=2012-08-13}}</ref>
<!-- ==Operational history== -->

==Variants==
;Me 109R Microlight
:Version for the European microlight class with a gross weight of {{convert|300|kg|lb|abbr=on}}<ref name="spec" />
;Me 109R Experimental
:Version for the homebuilt aircraft class with a gross weight of {{convert|500|kg|lb|abbr=on}}, stressed for +6/-4[[g-force|g]] and intended for [[aerobatics]]. Powered by a {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912UL]], a {{convert|70|to|120|hp|kW|0|abbr=on}} MPE 750 or {{convert|80|hp|kW|0|abbr=on}} [[D-Motor LF26]] boxer engine.<ref name="WDLA11" /><ref name="WDLA15"/><ref name="spec" />
<!-- ==Aircraft on display== -->

==Specifications (Me 109R) ==
{{Aircraft specs
|ref=Bayerl and Peak Aerospace<ref name="WDLA11" /><ref name="spec" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=6.3
|length ft=
|length in=
|length note=
|span m=8.10
|span ft=
|span in=
|span note=
|height m=1.5
|height ft=
|height in=
|height note=
|wing area sqm=10.53
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=190
|empty weight lb=
|empty weight note=
|gross weight kg=322
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|52|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type=twin cylinder, liquid-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=48<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=3
|prop name=[[Helix-Carbon]] composite
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=190
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=175
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=59
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=190
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=6
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=30.6
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
*[[Roland Me 109 Replica]] - a similar replica
*[[W.A.R. Bf109]]

==References==
{{reflist}}

==External links==
{{Commons category}}
*{{Official website|http://www.classic-planes.de/}}

[[Category:German ultralight aircraft 1990–1999]]
[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]