<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 | name=Parasol
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Aerodynamic [[experimental aircraft]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Parnall|George Parnall and Company]]
 | designer=Harold Bolas<ref name="Wix"/>
 | first flight=1930
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Parnall Parasol''' was an [[Experimental aircraft|experimental]] [[parasol wing]]ed aircraft design to measure the aerodynamic forces on wings in flight.  Two were built and flown in the early 1930s in the [[UK]].

==Design and development==

There have always been problems in getting an understanding of full-scale aircraft behavior from wind tunnel data. Most of these problems arise because small scale models in atmospheric pressure tunnels operate at much lower [[Reynolds number]]s than real aircraft, making the data hard to scale up.<ref>{{harvnb|Anderson|1997|p=301}}</ref>   In the late 1920s there was only one tunnel big enough to take full size aircraft propellers,<ref>{{harvnb|Anderson|1997|pp=328–330}}</ref> and one which by running at high pressure could reach realistic Reynolds numbers with aircraft models;<ref>{{harvnb|Anderson|1997|pp=301–4}}</ref> both of these were recent and in the USA.  It is therefore not surprising that some aircraft were designed specifically to undertake aerodynamic studies of particular components.  In the UK, the [[Bristol Type 92|Bristol "Laboratory"]] was built to study the drag of different cowlings for radial engines and the Parnall Parasol to measure the forces acting on wings of different section in flight.

The two main problems facing Parnall when given an [[Air Ministry]] contract to produce such an aircraft were to ensure that, so far as possible the behaviour of the wing was unaffected by interference with the flow over the fuselage or by prop wash, and to devise a way of measuring lift and drag forces whilst in flight.  They decided on a parasol wing design, since this would avoid complications like wing root interference, and made the wing mounting structure into a single unit with some freedom to deflect internally so that the force could be measured.  A typical parasol wing aircraft has lift struts from the lower fuselage to the wing plus some upper fuselage struts to support the wing near the centre line. The Parasol had two lift struts under each wing, with their lower ends joined within the fuselage to a horizontal rectangular frame. Two inverted V members were also attached to this frame, one at the front and one at the back, rising almost vertically to the front and rear wing spars.  The V-struts were hinged top and bottom, allowing the wing to move parallel to the horizontal frame under aerodynamic loads. The force was measured by a dynamometer ahead of the front cockpit, via a linkage which ran down and forward from the wing at an angle of about 22°, then turned back to the instrument via a crank.<ref name="Wix">{{harvnb|Wixey|1990|pages=175–180}}</ref><ref name="Flight">[http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200353.html ''Flight'' 17 February 1931  p.329-333]</ref>

This single force measurement could not resolve the drag and lift components.  This was done by making two force measurements on different flights and with slightly different support structure geometries, one with the parallel V-struts leaning a little forwards, a second with them leaning back; a knowledge of the two net forces and of the precise geometry allowed the desired resolution.  Rather than fix the rectangular frame directly to the lower longerons and provide alternative locations to change the geometry, Parnall made its readjustment simple by hanging the frame on hinged, near vertical struts from the upper longerons and fixing it in place with four further locating struts from frame to lower longerons.  The angle could then be changed by using locating struts of different lengths.  Though two angles were sufficient to get the lift and drag, a third measurement at another angle was often made as a cross check.  The locating struts lengths were designed so that the wing stayed in the same place and at the same angle of incidence throughout.  However, other choices of location strut lengths allowed the wing to be flown at different angles of incidence, for example the high angle experienced by slotted wings. The lift struts entered the fuselage via short horizontal slots to allow from the geometry changes.<ref name="Wix"/><ref name="Flight"/>

The two different wings known to have been fitted to the Parasol both had constant chord and very square tips.  They were of fabric covered wood construction.   The quite slender fuselage was a mixture of steel tubes and spruce,<ref>although Flight has it as a "wooden girder structure"</ref> square sided aft of the cockpits and rounded forwards.  The supercharged 226&nbsp;hp (167&nbsp;kW) [[Armstrong Siddeley Lynx]] radial engine was smoothly cowled but with its cylinder heads exposed.  The observer sat in the front cockpit, allowing him direct access to the dynamometer.  This had a control wheel and dial for the force readings, plus a lever with which he could free, read or lock the instrument.  The pilot's cockpit, behind and with a view unobstructed by the wing had two unusual controls associated with the measurements.  He could engage a pair of dynamometer cams which limited its movement and that of the wings to the 6&nbsp;mm necessary for a force measurement in case of a dynamometer failure.  He also controlled an hydraulic brake which was used to stop the engine during measurements and avoid prop wash interference.  After a period of gliding flight, the engine could be restarted with a gas starter.<ref name="Wix"/><ref name="Flight"/>

The rest of the aircraft was conventional.  It had a finless, comma shaped rudder, a tailplane mounted on top of the fuselage, braced from below and carrying separate elevators.  Tailplane incidence was adjustable from the cockpit.  The undercarriage was the divided type with wide splayed main [[Oleo (shock absorber)|oleo]] legs joining the fuselage below and between the wing struts. There were bracing struts forward to the engine bulkhead and the axles sloped inwards and upwards to a post below the fuselage, all rather like a strengthened version of the on the [[Parnall Elf]].<ref name="Wix"/><ref name="Flight"/>

The Parasol first flew during 1930, two being built in quick succession and both going to the [[Royal Aircraft Establishment]] at [[Farnborough Airfield|Farnborough]] in September and October.  The standard height for measurement flights was about 8,000&nbsp;ft (2,440 m) and the supercharged engine took the aircraft to this altitude in a little over 12 minutes.  The first of the Parasols (RAF serial ''K1228'') was initially fitted with a fully slotted wing of the well-used RAF28 section. At one stage the upper surface of its wing was covered in wool tufts and a camera was fitted to a high pylon just in front of the tail to record their behaviour. The second (''K1229'') flew with wings having full-span split trailing edges and out-rigged ailerons.<ref name="Wix"/> 1930 Farnborough records log the two as in the Aerodynamic Flight, with ''K1228'' doing "flight path recorder" work and ''K1229'' "test of wing section".<ref>{{harvnb|Cooper|2006|p=203}}</ref>  The flight air log was lowered in flight below the port wing.  The second Parasol remained in use at Farnborough until August 1936 and the first flew on until January 1937.<ref name="Wix"/>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (initial RAF28 wing)==
{{Aircraft specs
|ref={{harvnb|Wixey|1990|pages=180}}<!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=2
|capacity=
|length m=
|length ft=30
|length in=4
|length note=
|span m=
|span ft=42
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=9
|height in=6
|height note=
|wing area sqm=
|wing area sqft=294
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=2222
|empty weight note=
|gross weight kg=
|gross weight lb=2869
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Armstrong Siddeley Lynx]] IV
|eng1 type=7-cylinder supercharged radial
|eng1 kw=<!-- prop engines -->
|eng1 hp=226<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=118
|max speed kts=
|max speed note=at 8,000 ft (2,438 m)
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=56<!-- aerobatic -->
|stall speed kts=
|stall speed note=at 8,000 ft (2,438 m)
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=29200
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=12.4 min to 8,000 ft (2,438 m)
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|armament=<!-- add bulleted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Bibliography==
{{commons category|Parnall}}

===Citations===
{{reflist}}

===Cited sources===
{{refbegin}}
*{{cite book |title= A History of Aerodynamics|last=Anderson|first=John D. Jnr.| year=1997|volume=|publisher=Cambridge University House |location=Cambridge |isbn=0-521-66955-3|ref=harv}}
*{{cite book |title= Farnborough - 100 years of British aviation|last=Cooper|first=Peter J| year=2006|publisher=Midland Publishing |location=Hinkley, England |isbn=1-85780-239-X |ref=harv}}
*{{cite book |title= Parnall Aircraft since 1914|last=Wixey|first=Kenneth |coauthors= |edition= |year= 1990|publisher= Naval Institute Press|location=Annopolis|isbn= 1-55750-930-1|ref=harv}}
{{refend}}

<!-- ==External links== -->
{{Parnall aircraft}}

[[Category:British experimental aircraft 1930–1939]]
[[Category:Parnall aircraft|Parasol]]