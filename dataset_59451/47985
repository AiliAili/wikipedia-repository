'''Fred Skolnik''' is an American-born writer and editor. Born in [[New York City]], he has lived in [[Israel]] since 1963, working mostly as an editor and translator. Best known as the editor in chief of the 22-volume second edition of the ''[[Encyclopaedia Judaica]]''<ref>Skolnik, F., & Berenbaum, M. (2007). ''Encyclopaedia Judaica''. Detroit: Macmillan Reference USA in association with the Keter Pub. House.</ref><ref>http://www.encyclopediajudaica.us/</ref> winner of the 2007 Dartmouth Medal and hailed by the ''[[Library Journal]]'' as a "landmark achievement," he is also the author of two novels and over a hundred stories and essays.

==Works==

===Encyclopaedia Judaica===
Skolnik commenced work on the Encyclopaedia in August 2003 and brought the entire project to conclusion editorially in the beginning of 2006, supervising the work of over 50 divisional editors and around 1,200 contributors from his Jerusalem office. More than half of the 20,000 first edition (1972) entries were revised or updated and over 2,500 new entries were produced, together totaling 4.7 million new words. In addition, around 30,000 new bibliographical items were added. Among the renowned international scholars serving as editors were [[Michael Berenbaum]] (Holocaust, United States, as well as executive editor), Israel Prize laureates [[Menachem Elon]] (Jewish Law), Aviezer Ravitzky (Jewish Philosophy), [[Moshe Idel]] (Kabbalah and Hasidism), Jacob Landau (Islam and Muslim Countries), and Ziva Amishai Maisels (Art). [[Judith R. Baskin]] headed the entirely new Women and Gender division while Shamma Friedman oversaw the revamping of the Talmud division in accordance with the revolution in talmudic studies. [[Dina Porat]] (Antisemitism), [[Sergio DellaPergola]] (Demography), and [[Jonathan Sarna]] (United States) served as consulting editors. In its award citation the Dartmouth Committee called the new ''Judaica'' "an authoritative, interdisciplinary and comprehensive examination of all aspects of Jewish life, history and culture."
Among other award-winning projects, Skolnik had also worked on ''[http://www.judaism.com/display.asp?etn=BAADG The New Encyclopedia of Judaism]'' (co-editor, 2002)<ref>Wigoder, G., Skolnik, F., & Himelstein, S. (2002). ''The New Encyclopedia of Judaism''. New York: New York University Press.</ref> and the 3-volume ''[http://www.amazon.com/Encyclopedia-Jewish-Before-During-Holocaust/dp/0814793568 Encyclopedia of Jewish Life Before and During the Holocaust]'' (senior editor, 2001).

===The Other Shore===
Skolnik's first novel is set in Israel in the 1980s, between the Lebanese War and the first Intifada – a pivotal time in the country's recent history that saw its final transformation from a Zionist-socialist society to a Western-style consumer society. The novel follows the lives of representational characters across the entire spectrum of Israeli society but focuses on two families – the Shachars, a kibbutz family, and the Goldsteins, representing the emerging Israeli middle class. In effect the protagonists vie to win the heart and soul of Israel itself. 
Ranen Omer-Sherman (author of ''Diaspora and Zionism in Jewish American Literature'') has called the novel a "triumph of both the aesthetic and moral imagination." Lewis Fried (author of ''Handbook of American Jewish Literature'') calls it "not only an important addition to Jewish literature but also a necessary one."
A 700-page epic, ''The Other Shore'' was published in June 2011 by [https://www.amazon.com/Other-Shore-Fred-Skolnik/dp/0982673450/ref=asap_bc?ie=UTF8 Aqueous Books].

===Death===
Skolnik's second novel (Spuyten Duyvil, 2015) is a multilayered narrative ostensibly set in a Hollywood milieu and containing elements of the thriller. It unfolds in a dreamlike atmosphere where ancient and modern mythologies are woven together as the unnamed hero seeks to evade his pursuers and escape his destiny.

===Writing as Fred Russell===
Skolnik has also published two novels under his Fred Russell pen name, both in 2014: ''Rafi's World'' (Fomite Press), dealing with Israel's emerging criminal class, and ''The Links in the Chain'' (CCLaP), a thriller set in New York against an Arab-Israel background.

==References==
{{reflist}}

{{DEFAULTSORT:Skolnik, Fred}}
[[Category:American Jews]]
[[Category:American male writers]]
[[Category:Living people]]