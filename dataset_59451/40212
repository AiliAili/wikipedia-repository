{{Use dmy dates|date=May 2016}}
{{Infobox academic
| name        = David A Brading
| image       = Professor David Brading.jpg
| image_size  =
| caption     = Professor David Brading in the courtyard of Condumex, Mexico
| birth_name  = David Anthony Brading
| birth_date  = {{Birth date and age|df=yes|1936|8|26}}
| birth_place = London, England
| death_date  = 
| death_place =
| other_names = D.A. Brading
| nationality = British
| religion = Catholic
| occupation  = Historian, writer
| organizations = [[Cambridge University]]<br> [[Yale University]] <br> [[University of California, Berkeley]] 
| alma_mater  = [[Cambridge University]] ([[Bachelor of Arts|BA]], 1960; [[University College London]] PhD, 1965)
| known_for   = [[Mexican History]] ,<br> [[Religion and politics|Church and State]] <br> [[Economic history of Mexico]]
| notable_works = Mexican Phoenix. [[Our Lady of Guadalupe]]: Image and Tradition Across Five Centuries, <br> The First America: The Spanish Monarchy, Creole Patriotism and the Liberal State 1492–1867
| awards      = [[Herbert Eugene Bolton|Bolton Prize]] (1972) <br> [[Order of the Aztec Eagle]] (2002)<br> Medalla de Honor del Congreso Peru (2011)
| spouse      = {{marriage|[[Celia Wu Brading]]|1966|}}
| children    = Christopher James Brading
}}

'''David Anthony Brading''' [[Litt.D]], [[Royal Historical Society|FRHistS]], [[British Academy|FBA]]<ref name="DAB1">{{cite web
| author= [[Contemporary Authors Online]]
| year = 2003
| url = http://go.galegroup.com/ps/i.do?id=GALE%7CH1000011258&v=2.1&u=txshracd2544&it=r&p=CA&sw=w&asid=96ad21330547ae79a8c5e0ed0a63918d
| title = D. A. Brading
| publisher= Gale
| location = Detroit
| accessdate = 16 May 2016
}}</ref><ref name="DAB2">{{cite web
| author= [[Who's Who (UK)]]
| year = 2016
| url =  http://ukwhoswho.com/view/article/oupww/whoswho/U8453
| title = BRADING, Prof. David Anthony, PhD, LittD; FBA 1995
| publisher=   A & C Black, an imprint of Bloomsbury Publishing plc, 2016; online edn, Oxford University Press, 2015
| location = [[Oxford]]
| accessdate = 16 May 2016
}}</ref><ref name="DAB3">{{cite web
| author= [[British Academy]]
| year = 2016
| url =  http://www.britac.ac.uk/fellowship/directory/ord.cfm?member=2980
| title = BRADING,David Emeritus Professor of Mexican History, University of Cambridge 
| publisher= The British Academy
| location = London
| accessdate = 16 May 2016
}}</ref><ref name="DAB4">{{cite web
| author= [[Cambridge University]]
| year = 2016
| url =  http://www.latin-american.cam.ac.uk/staff/associatestaff#David%20Brading
| title = BRADING,David Emeritus Professor of Mexican History, University of Cambridge 
| publisher= Cambridge University 
| location = [[Cambridge]]
| accessdate = 16 May 2016
}}</ref> (born 26 August 1936), is a British historian and Professor Emeritus 
<ref name="DAB5">{{cite web
| author= [[Cambridge University]]
| year = 2016
| url =  http://www.admin.cam.ac.uk/reporter/2015-16/special/05/section6.shtml#heading2-14 
| title = Cambridge University Reporter Special No 5 Tuesday 15 December 2015 Vol cxlvi
| publisher= Cambridge University 
| location = [[Cambridge]]
| accessdate = 16 May 2016
}}</ref> of [[Mexican History]] at the [[University of Cambridge]], where he is an Emeritus Fellow of [[Clare Hall, Cambridge|Clare Hall]] and a Honorary Fellow of [[Pembroke College, Cambridge|Pembroke College]]. His work has been recognized with several awards,including the [[Herbert Eugene Bolton|Bolton Prize]] in 1972 
<ref name="DAB6">{{cite web
| author= 
| year = 2016
| url =  http://clah.h-net.org/?page_id=174 
| title = Bolton-Johnson Prize
| publisher=  Conference on Latin American History
| location = 
| accessdate = 16 May 2016
}}</ref> the [[Order of the Aztec Eagle]] in 2002 from the Mexican government .<ref name="DAB2"/><ref name="DAB19">{{cite news|last1=JUAN MANUEL|first1=VENEGAS|title=El Aguila Azteca para Brading|url=http://www.jornada.unam.mx/2002/11/13/02an1cul.php?origen=index.html|accessdate=18 May 2016|agency=Desarrollo de Medios S.A. de C.V.|publisher=La Jornada|date=12 November 2002|language=Spanish|quote=el presidente Fox distinguiera al profesor David Brading con el Aguila Azteca,}}</ref><ref name="DAB20">{{cite news|last1=Ruiz|first1=José Luis|title=Inauguran muestra prehispánica|url=http://archivo.eluniversal.com.mx/primera/13446.html|accessdate=18 May 2016|publisher=El Universal|date=13 November 2002}}</ref> and the Medal of Congress from the Peruvian government in 2011.<ref name="DAB2"/><ref>{{cite web|title=CONGRESO CONDECORÓ A HISTORIADOR BRADING |url=http://www4.congreso.gob.pe/heraldo/index_fotoh1.asp?fecha=20110606 |website=Congreso de la República Peru |publisher=Congreso de la República Peru |accessdate=18 May 2016 |language=Spanish }}</ref>{{cbignore}} Brading has received honorary degrees from several universities, including [[Peru|Universidad del Pacifico]], [[Universidad de Lima]]<ref name="DAB2"/> and the [[Universidad Michoacana de San Nicolás de Hidalgo]]<ref name="DAB26">{{cite web|title=Historia de México, legado de nuestros antepasados que no debe morir|url=http://noticias.universia.net.mx/vida-universitaria/noticia/2005/06/09/93702/historia-mexico-legado-antepasados-debe-morir.html|website=Universia|publisher=Universia|accessdate=20 May 2016|language=Spanish|date=9 June 2005|quote=Por sus contribuciones a la historia de México, nombran Doctor Honoris Causa a Enrique Florescano, Friedrich Katz y David A. Brading en la Universidad Michoacana de San Nicolás de Hidalgo}}</ref>

He is regarded as one of the foremost historians of Latin America in the United Kingdom.<ref name="DAB7">{{cite book|last1=Deans-Smith|first1=Susan|last2=Young|first2=Eric Van|title=Mexican soundings : Essays in honour of David A. Brading|date=2007|publisher=Institute for the Study of the Americas, University of London|location=London|isbn=978-1900039734|url=https://books.google.com/books/about/Mexican_soundings.html?id=-nx6AAAAMAAJ&redir_esc=y|accessdate=16 May 2016}}</ref>
<ref name="DAB8">{{cite book|last1=Moya|first1=Jose C|title=The Oxford Handbook of Latin American History|date=2010|publisher=Oxford University Press|isbn=9780195166200 |page=43 |url=https://books.google.com/books?id=gHyw6lhds7QC&pg=PA43&lpg=PA43&dq=David+Brading+is+one+of+the+foremost+historians&source=bl&ots=WHdMyDCAYF&sig=IofVOOGOXZznAHI9eZRv5Bne9Ec&hl=en&sa=X&ved=0ahUKEwjKzvHThtLMAhWMCMAKHUpKBboQ6AEIKzAD#v=onepage&q=David%20Brading%20is%20one%20of%20the%20foremost%20historians&f=false}}</ref><ref>{{cite web|title=by world renowned historian, Professor David Brading FBA |url=http://www.britac.ac.uk/intl/Simon_Bolivar.cfm |website=British Academy |accessdate=16 May 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20150919164408/http://www.britac.ac.uk/intl/Simon_Bolivar.cfm |archivedate=19 September 2015 |df=dmy }}</ref> and was the most widely cited British Latin Americanist.<ref name="DAB22"/><ref name="DAB23">{{cite journal|last1=Craske|first1=Nikki|last2=Lehmann|first2=David|title=Fifty Years of Research in Latin American Studies in the UK|journal=European Review of Latin American and Caribbean Studies|date=April 2002|volume=No. 72|pages=61–80|jstor=25675968|publisher=Celda}}</ref>

==Early life and education==
David Brading was born in London, England and educated at [[St Ignatius' College]] and [[Pembroke College, Cambridge]] where he read History and obtained a BA (Hons) Double first-class honours in 1960. He was an Exhibitioner and Foundation Scholar at [[Cambridge University]] where he attended the lectures of [[David Knowles (scholar)|David Knowles]], [[Geoffrey Elton]], and [[Michael Postan]]. In 1961 he was awarded a [[Henry Fellowship]] to [[Yale University]].But it was later that year, whilst in Mexico, that Brading’s fascination with the country began, “I have now found my field of study: sixteenth-century Spain and Latin America”…The more I think of it, the more Latin America seems attractive. Sixteenth–century Spain, looking back to the Reconquista and forward to the Counter Reformation and the decadence. The nature of its Catholicism, its mysticism, the history of its expansion, the Jesuits, its art, architecture and poetry. Latin America with its archaeology and anthropology, the nature of its liberalism and its revolutions.”<ref name="DAB7"/>

After working for several months in the [[British civil service]] as Assistant Principal at the Board of Trade, he received his M.A. from [[Cambridge University]]<ref>{{cite web|title=The Cambridge MA: : Student Registry|url=http://www.cambridgestudents.cam.ac.uk/your-course/graduation-and-what-next/cambridge-ma|website=Cambridge University|accessdate=16 May 2016}}</ref> and enrolled for a PhD at [[University College, London|University College]], under the supervision of [[John Lynch (historian)|John Lynch]].

Deciding to investigate silver mining in New Spain, Brading spent 15 months engaged in archival research, starting in the [[Archive of the Indies]] , [[Biblioteca Nacional de España]] and the [[Archivo Histórico Nacional]] before continuing in Mexico in the [[National Library of Mexico]], [[General National Archive (Mexico)|General Archive of the Nation]]  and finally the archive of Guanajuato.<ref name="DAB7"/>
The fruition of this research was the completion in 1965 of his doctoral thesis,<ref name="DAB2"/> entitled "Society and Administration in Late Eighteen Century Guanajuato with especial reference to the Silver Mining Industry"<ref name="DAB15">{{cite web|last1=Brading|first1=David|title=Society and administration in late eighteenth century Guanajuato : with especial reference to the silver mining industry|url=http://copac.jisc.ac.uk/id/2423219?style=html&title=Society%20and%20administration%20in%20late%20eighteenth%20century|website=copac.jisc.ac.uk|accessdate=17 May 2016}}</ref> which was examined by [[Charles Boxer]] and [[J. H. Parry|John Parry]].

Returning to the United States as Assistant Professor at the [[University of California, Berkeley]], Brading delivered three sets of lectures dealing with Mexico, Peru and Argentina, before moving to [[Yale University]] as Associate Professor in 1971.

Brading's first book, ''Miners and Merchants in Bourbon Mexico 1765–1810'' was published in 1971. It dealt with the general history of the silver industry in Mexico with a comprehensive study of [[Guanajuato]] and it's mines, population and leading families. A review in the [[Annals of the American Academy of Political and Social Science]] called it "landmark of dissertation research and organization"<ref>{{cite journal|last1=Bernstein|first1=Harry|title=Reviewed Work: Miners and Merchants in Bourbon Mexico, 1763–1810 by D. A. Brading|journal=The Annals of the American Academy of Political and Social Science|date=1972|volume= 400|pages=197–198|jstor=1039991|publisher=Sage Publications, Inc}}</ref> while [[Fernand Braudel]] who is considered one of the greatest of the modern historians found it a "fascinating book".<ref name="DAB25">{{cite book|last1=Braudel|first1=Fernand|title=Civilization and Capitalism, 15th–18th Century: The perspective of the world, Volume 3|date=1979|publisher=Fontana|isbn=9780520081161|page=402|url=https://books.google.com/books?id=xMZI2QEer9QC&pg=PA402|accessdate=19 May 2016|quote=From D.A. Brading's fascinating book on eightteenth-century New Spain}}</ref> It won the [[Herbert Eugene Bolton|Bolton Prize]] in 1972<ref name="DAB6"/>

In 1973, Brading returned to [[Cambridge University]] as a University Lecturer in [[Latin American History]] and become Director of the Centre of Latin American Studies at [[Cambridge University]] from 1975 – 1990. He was a [[fellow]] of [[St Edmund's College, Cambridge|St Edmund's College]] from 1975–1988. In 1991 a [[LittD]] was awarded to Brading and he was made [[Reader (academic rank)|Reader]] in Latin American History at [[Cambridge University]].  The following year he was the Leverhulme Research Fellow in Mexico, received an honorary doctorate from the [[University of Lima]] in Peru.<ref name="DAB2"/> and was elected membership of the [[European Academy of Sciences and Arts]] of which he is one of only ten British members in the humanities, the others being [[Roger Scruton]], [[Richard Overy]] , [[Norman Davies]] and [[Timothy Garton Ash]] among others.<ref name="DAB24">{{cite web|title=European Academy of Sciences and Arts Members|url=http://www.euro-acad.eu/members?klass=1&land=United+Kingdom&sort=&way=|website=European Academy of Sciences and Arts|accessdate=19 May 2016}}</ref>
In 1999, Brading was made Professor in Latin American History at [[Cambridge University]].

==Works==
In 1992, Brading's book ''The First America: The Spanish Monarchy, Creole Patriots, and the Liberal State, 1492–1867'' was published. Its central thesis was that Spaniards born in the New World (creoles) had an American cultural identity, a creole consciousness, distinct from those born and raised in Spain (''peninsulares''). A review in the journal [[History (journal)|History]] declared it to be a book of major importance on the topic,<ref>{{cite journal|last1=Fisher|first1=John|title=Review Reviewed Work: The First America: The Spanish Monarchy, Creole Patriots, and the Liberal State, 1492–1867 by D. A. Brading|journal=History|date=1992|volume= 77| issue =  251|pages=464–465|jstor=24422114|publisher=Wiley}}</ref> as did a review in the [[Journal of Latin American Studies]].<ref>{{cite journal|last1=Carr|first1=Raymond|title=Reviewed Work: The First America: The Spanish Monarchy, Creole Patriots, and the Liberal State 1492–1867 by D. A. Brading|journal=Journal of Latin American Studies|date=1992|volume= 24| issue =  2|pages=437–439|jstor=157074}}</ref> The Mexican literary magazine [[Letras Libres]] "said it occupies a place of honor in the library of neophytes and scholars."<ref name="DAB17"/>

In 2001, Brading published ''Mexican Phoenix, Our Lady of Guadalupe: Image and Tradition across Five Centuries'', a detailed history of the most important religious icon in Latin America –  the [[Virgin of Guadalupe]]. [[Foreign Affairs]] magazine commented in a review saying that it was "brilliant"... and having " remarkable insight" [13]<ref name="DAB17">{{cite web|last1=Domínguez Michael|first1=Christopher|title=La Virgen de Guadalupe: imagen y tradición, de David A. Brading|url=http://www.letraslibres.com/revista/libros/la-virgen-de-guadalupe-imagen-y-tradicion-de-david-brading|publisher=Letras Libres|accessdate=17 May 2016}}</ref><ref name="DAB9">{{cite web|title=Mexican Phoenix, Our Lady of Guadalupe: Image and Tradition Across Five Centuries; Images at War:|url=https://www.foreignaffairs.com/reviews/capsule-review/2002-01-01/mexican-phoenix-our-lady-guadalupe-image-and-tradition-across-five|website=Foreign Affairs|accessdate=16 May 2016}}</ref>

==Festschrift==
In 2007 a Festschrift was published in his honour, entitled  ''Mexican Soundings: Essays in Honour of David A. Brading.''<ref name="DAB10">{{cite journal|last1=Radding|first1=Cynthia|title=Review of Mexican Soundings: Essays in Honour of David A. Brading|journal=Journal of Latin American Studies|date=2008|volume= 40|issue=2|pages=341–343|jstor=40056672}}</ref><ref name="DAB11">{{cite journal|last1=Rankin|first1=Monica A|title=Mexican Soundings: Essays in Honour of David A. Brading.|journal=Estudios Interdisciplinarios de América Latina y el Caribe|date=2009|volume= 20|issue=2|url=http://www7.tau.ac.il/ojs/index.php/eial/article/view/381|accessdate=16 May 2016 |publisher=Tel Aviv University}}</ref><ref name="DAB12">{{cite journal|last1=Anna|first1=Timothy E|title=Mexican Soundings: Essays in Honour of David A. Brading|journal=The Americas|date=2008|volume=64 (3)|pages=452–454|jstor=30139155|publisher=Cambridge University Press}}</ref><ref name="DAB13">{{cite journal|last1=Tutino|first1=J|title=Mexican Soundings: Essays in Honour of David A. Brading|journal=Hispanic American Historical Review|date=2009|volume= 89|issue=1|pages=165–167|doi=10.1215/00182168-2008-059|url=http://hahr.dukejournals.org/content/89/1/165.full.pdf+html|accessdate=16 May 2016|publisher=Duke University Press}}</ref><ref name="DAB14">{{cite journal|last1=Buve|first1=Raymund|title=Mexican Soundings. Essays in Honour of David A. Brading|journal=Iberoamericana|date=2009|volume=Año 9, No. 35|pages=285–287|jstor=41676953|publisher=Iberoamericana Editorial Vervuert|location=Frankfurt}}</ref> [[The Hispanic American Historical Review]] in a review of the book said "Brading's contributions to Mexican history are equaled by few and exceeded by none."<ref name="DAB13"/> while an essay by [[Eric Van Young]] praised Brading as the " leading scholar of intellectual history and the Catholic Church for colonial Mexico."<ref name="DAB7"/>

Contributors included [[Alan Knight (historian)|Alan Knight]] , [[Enrique Florescano]], [[Eric Van Young]], Susan Deans-Smith, Ellen Gunnarsdóttir, Brian Hamnett, Marta Eugenia García Ugarte, and Guy P.C. Thomson.

== Books ==

* ''Miners and Merchants in Bourbon Mexico, 1763–1810'' (Cambridge University Press, 1971) ISBN 9780521078740
* ''Haciendas and Ranchos in the Mexican Bajio: Leon 1700–1863'' (Cambridge University Press, 1978) ISBN 9780521222006
* ''Prophecy and Myth in Mexican History" (Cambridge University Press, 1984) ISBN 9789681672294
* ''The Origins of Mexican Nationalism'' (Cambridge University Press, 1985) ISBN 0904927474
* ''The First America: The Spanish Monarchy, Creole Patriotism and the Liberal State 1492–1867'' (Cambridge University Press, 1991) ISBN 9780521391306
* ''Church and State in Bourbon Mexico. The Diocese of Michoacan, 1749–1810'' (Cambridge University Press, 1994) ISBN 9780521523011
* ''Mexican Phoenix. Our Lady of Guadalupe: Image and Tradition Across Five Centuries'' (Cambridge University Press, 2001) ISBN 9780521801317
* ''Octavio Paz y la poética de la historia mexicana'' (Fondo de Cultura Económica, 2002)
* ''Mexican soundings : essays in honour of David A. Brading,'' Edited by Susan Deans-Smith and Eric Van Young (London : Institute for the Study of the Americas, 2007.) ISBN 9781900039734

==Academic achievements, awards, and honors==

In the Spring 1998 newsletter of the [[Conference on Latin American History]] published by the [[University of North Carolina at Charlotte]], [[William McGreevey]] in a study of the 11 volumes of [[The Cambridge History of Latin America]], bibliographic essays, demonstrated that David Brading was  "cited more frequently than that of any other writer on Latin American history".<ref name="DAB22">{{cite journal|last1=Mcgreevey|first1=William|title=Leading Scholars of Latin American History|journal=Conference on Latin American History|date=1998|volume= 34 Spring|url=http://clah.h-net.org/|accessdate=19 May 2016|publisher=University of North Carolina at Charlotte}}</ref>

*Henry Fellow, Yale University, 1960–1961
*Herbert Eugene Bolton Prize, 1972
*Visiting Fellow, University of Tokyo, Japan, 1985
*Directeur d'Etudes at I'Ecole de Hautes Etudes en Science Sociales, 1989
*Professor Honorario (Honoris Causa), University of Lima, Peru, 1993
*Fellow of Academia Scientiarum et Artium Europaea, 1993
*Leverhulme Research Fellow in Mexico,1993
*Visiting Scholar, Centro de Estudios de Historia de Mexico, Condumex,1993
*Member of [[Academia Scientiarum et Artium Europaea]] 
*Fellow of Clare Hall, Cambridge University, 1995
*Fellow of the [[British Academy]] 1995
*Julio Cortázar Visiting Professor, University of Guadalajara, 1996
*Miembro Honorario, Instituto Riva-Agüero, Escuela de Altos Estudios, Pontificia Universidad Catolica del Peru, 1997
*Miembro Fundador, La Sociedad Mexicana de Bibliofilos, A.C. 1997
*Miembro Correspondiente, La Academia Nacional de la Historia, Lima, Perú, 1998 
*Andrew W. Mellon Senior Research Fellow, John Carter Brown Library, Brown University, 2000
*Academico Corresponsal, Academia Mexicana de la Historia, 2008
* Honorary Fellow of Pembroke College, Cambridge University, 2008

==References==
{{reflist|30em}}

==External links==
* [http://www.ukwhoswho.com/view/article/oupww/whoswho/U8453/BRADING Prof. David Anthony],''Who's Who 2016'', A & C Black, 2016; online edn, Oxford University Press, 2016
* [http://www.britac.ac.uk/fellowship/directory/ord.cfm?member=2980 British Academy biography]
* [http://www.latin-american.cam.ac.uk/staff/associatestaff#David%20Brading.html Centre of Latin American Studies, Cambridge University]
* [https://www.fce.com.ar/ar/autores/autor_detalle.aspx?idAutor=455.html Author Page, Fondo de Cultura Economica]
* [https://www.youtube.com/watch?v=fUJJG6FZZEQ.html Audio/Video recordings] David Brading discussing De la region a la globalidad
*  [http://www.history.ac.uk/podcasts/latin-american-history/mexican-nationalism-history-and-theory Audio/Video recordings] Mexican Nationalism: History and Theory Podcast, Institute of Historical Research (IHR)4 October 2011 
{{Authority control}}

{{DEFAULTSORT:Brading, David}}
[[Category:Historians of Mexico]]
[[Category:British historians]]
[[Category:British Hispanists]]
[[Category:Latin Americanists]]
[[Category:Historians of Spain]]
[[Category:Historians of Peru]]
[[Category:1936 births]]
[[Category:Living people]]
[[Category:Historians of Latin America]]
[[Category:Insignia propers of the Order of the Aztec Eagle]]
[[Category:Members of the European Academy of Sciences and Arts]]
[[Category:Fellows of Clare Hall, Cambridge]]
[[Category:Historians of Mesoamerica]]
[[Category:English historians]]
[[Category:Alumni of Pembroke College, Cambridge]]
[[Category:Alumni of University College London]]
[[Category:Academics of the University of Cambridge]]
[[Category:English Roman Catholics]]
[[Category:English Roman Catholic writers]]
[[Category:British Roman Catholic writers]]
[[Category:Members of the University of Cambridge faculty of history]]
[[Category:20th-century English historians]]
[[Category:Fellows of the Royal Historical Society]]
[[Category:Fellows of the British Academy]]
[[Category:Fellows of Pembroke College, Cambridge]]
[[Category:English academics]]
[[Category:Yale University faculty]]
[[Category:Recipients of the Order of the Aztec Eagle]]
[[Category:21st-century historians]]
[[Category:21st-century Mesoamericanists]]
[[Category:20th-century Mesoamericanists]]
[[Category:Historians of the Roman Catholic Church]]