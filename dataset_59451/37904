{{Infobox animal
| name                    = Binky
| image                   = Binky.jpg
| caption                 = Binky with tourist's shoe in his mouth
| species                 = [[Polar bear]]
| gender                  = Male
| birth_date               = 1975
| birth_place              = [[Cape Beaufort]] on Alaska's [[Alaska North Slope|North Slope]] 
| death_date               = July 20, 1995 (age 20<ref name=mourn/>)
| death_place              = [[Anchorage]], [[Alaska]]
}}
'''Binky''' (1975 – July 20, 1995) was a [[polar bear]] who lived at the [[Alaska Zoo]] in [[Anchorage, Alaska|Anchorage]]. He was originally orphaned near Cape Beaufort, close to the Chukchi Sea, but was found by a driller in Northwest Alaska named David Bergsrud. The area where Binky was located is known to those living outside of Alaska as the North Slope. Alaska Fish and Game was contacted shortly after Binky's discovery, and arrangements were being made to find a zoo in the Lower 48. Anchorage had a small zoo at the time, with an elephant that a lady had won and a few other animals.  When word got around that a polar bear cub had been found,  folks started hunting for ways to stall the effort of sending Binky outside of Alaska.  Time was needed to find a sponsor to fund an enclosure at the Alaska Children's Zoo for Binky. Alaska Fish and Game employees came up with the idea of flying Binky to a number of the inland North Slope villages.  School was let out in these villages so all the children could come to the airstrips to see Binky. These received major news coverage. Finally things fell into place to allow the Anchorage zoo to take Binky. Binky  quickly became one of its most popular attractions. He became a local hero and received international news coverage after mauling two zoo visitors in separate incidents in 1994. Binky died in 1995 from [[sarcocystosis]], a parasitic disease.

==Early life==
Binky was found orphaned near Cape Beaufort, on Alaska's North Slope, in late April, 1975 by an oilfield worker. Efforts were made to locate his mother to no avail.  By early May 1975 Alaska Fish and Game were contacted and began arrangements  to find a zoo in the "lower 48" as the continental US is called, that would get Binky.  Anchorage had a small zoo at the time, with an elephant that a lady had won and a few other animals. As these arrangements were being made to find a zoo in the "lower 48" as the continental US is called, word got around that a polar bear cub had been found, the communities near Nome and people in  Anchorage  petitioned Alaska Fish and Game to let Binky stay in Alaska. Anchorage had a small zoo at the time, with an elephant that one of the founders had won  and a few other donated animals.   AF&G found ways to stall sending Binky outside of Alaska.  Time was needed to find a "sponsor" to fund an enclosure at the Alaska Children's Zoo for Binky. Alaska Fish and Game employees  came up with the idea of flying Binky to a number of the inland North Slope villages.  School was let out in these villages so all the children could come to the airstrips to see Binky. These received major news coverage. Finally things fell into place  so the zoo located in Anchorage could take Binky.   [[Alaska Department of Fish and Game]].<ref name=uncertain/><ref name=open/><ref name=meet/> He was then given to the Alaska Children's Zoo (later the [[Alaska Zoo]]) in Anchorage, where he quickly became one of the zoo's most popular attractions.<ref name=uncertain/><ref name=meet/> His keeper commented in 1976 that Binky was a performer and cried in the evenings when his applauding, laughing visitors left for the day.<ref name=uncertain/>

Binky was initially placed in a 13 foot by 20 foot oval cage, which he quickly outgrew.<ref name=uncertain/> The prospect of raising the estimated $150,000 needed for a new, larger enclosure was uncertain, and zoo officials feared Binky would have to be sent to the [[Milwaukee Zoo]].<ref name=uncertain/><ref name=work/> A fundraiser and open house were held to raise money for the effort,<ref name=bash/> and a number of schools and businesses participated.<ref name=work/> Ultimately, the greatest contribution to the zoo's effort was the city's purchase of the zoo land for $100,000, which the zoo agreed to [[Leaseback|buy back]] in 55 annual installments of $2,500.<ref name=work/> Binky's new enclosure opened in May 1977.<ref name=delight/> That year, Binky made an appearance as [[Cal Worthington#My Dog Spot ads|"my dog Spot"]] in one of [[Cal Worthington]]'s car dealership commercials.<ref name=cal/>

As Binky approached sexual maturity, zoo officials negotiated for the purchase of a female polar bear named Mimi from the [[Tulsa Zoo]] in [[Oklahoma]].<ref name=belle/><ref name=prolonged/> As the transfer was being finalized, however, Mimi died from a viral disease in Tulsa.<ref name=prolonged/> In February 1979, young polar bear twins (Nuka, a female, and Siku, a male) joined Binky in his enclosure.<ref name=anchorage/><ref name=job/> Binky got along poorly with Siku, however, so Siku was given to a zoo in [[Morelia]], [[Mexico]] in 1981.<ref name=safari/><!--showing that Siku was still at the zoo as of 1981--><ref name=spanish/>

As a full-grown bear, Binky weighed 1,200 pounds.<ref name=job/> He was an aggressive bear; in 1980, he bit off a zoo employee's finger.<ref>{{cite book|author=Larry Kanuit|title=Some Bears Kill: True-Life Tales of Terror|url=https://books.google.com/books?id=VB2Yg0sYi9YC&pg=PR6|year=2007|publisher=Larry Kaniut|isbn=978-1-57157-293-6|page=198}}</ref> His keeper commented in 1983, "Binky is stubborn [and] independent, and he likes to play games. When he's really feeling obstinate, he walks halfway into his den and sits down. He knows I can't close it. He's a very smart bear."<ref name=job/>

==Maulings, celebrity, and death==
In July 1994, 29-year-old Australian tourist Kathryn Warburton jumped over two safety rails to get a close-up photograph of Binky in his cage.  When Binky stuck his head through the bars and grabbed her,<ref name="zoo"/><ref name="folkloristics"/><ref name=mauls/> she suffered a broken leg and bite wounds.<ref name=mauls/> Another tourist caught the event on tape.<ref name=mauls/>  Binky kept the woman's shoe for three days before it could be retrieved by zoo officials,<ref name="zoo"/> and the day after the attack ''[[Alaska Star]]'' photographer Rob Layman took a photo of Binky, holding the shoe in his mouth, that was printed in almost every press account of the incident.<ref name=folkloristics/><ref name=breese/> Warburton gave the other shoe to the Bird House, a bar in nearby Bird Creek that has since burned down.<ref>{{cite news|last1=Warburton|first1=Kathryn|title=Letter to the Editor|work=[[Anchorage Daily News]]|date=August 22, 1994}}</ref><ref>{{cite news|last1=Epler|first1=Patti|title=Bird House Tavern, Risen from the Ashes|url=http://www.adn.com/article/bird-house-tavern-risen-ashes|work=[[Alaska Dispatch News]]|date=May 25, 2011}}</ref>

Six weeks later, Binky was involved in another mauling. Drunken local teenagers approached the bear's enclosure, apparently hoping to swim in its pool, and one 19-year-old was hospitalized with leg [[lacerations]] after he was mauled.<ref name=adnteens/> The zoo did not confirm that Binky was the attacker, but the bear had blood on his face following the incident.<ref name=suspected/>

After these attacks, Binky received international news coverage.<ref name=zoo/><ref name=folkloristics/><ref name=enge/><ref name=herald/><ref name=uphere>{{cite news|last1=Hames|first1=Elizabeth|title=A killer bear - and the state that loved him|url=http://archive.uphere.ca/node/881|accessdate=July 22, 2015|work=[[Up Here (magazine)|Up Here]]|date=April 2013}}</ref> Binky merchandise was created, including T-shirts, mugs, and bumper stickers, often adorned with the shoe photo or with the slogan "Send another tourist, this one got away".<ref name="zoo"/><ref name="folkloristics"/><ref name="herald"/><ref name=uphere/> Local [[letters to the editor]] supported Binky during both incidents, most often arguing that polar bears' dangerousness should be respected.<ref name="folkloristics"/>  The Zoo's director, Sammye Seawell, criticized Warburton's actions in the ''[[Anchorage Daily News]]'', saying "[s]he violated the rules and jeopardized the bear's life."<ref name=mauls/> Though Seawell initially insisted that the attack would not change how the zoo was run,<ref name=mauls/> security around Binky's cage was upgraded to keep zoo visitors out.<ref name=cheers/>

In 1995, Binky's cagemate Nuka suddenly became sick with the parasitic disease [[sarcocystosis]], dying from associated [[Acute liver failure|liver failure]] on July 14, a week after her symptoms began.<ref name=mystifies/><ref name=bug/><ref name=sarco/> Shortly thereafter, Binky showed signs of the disease.<ref name=mourn/> On the morning of July 20, he went into convulsions and died.<ref name=mourn/> Zoo visitors left bouquets of flowers outside the bears' empty enclosure,<ref name=old/> and the zoo's memorial service saw a high turnout despite pouring rain.<ref name=annabelle/> The bears were buried on zoo grounds.<ref name=jackie/>

==See also==
* [[Bear danger]]
*[[Bear attack]]
* [[List of individual bears]]

==References==
{{reflist|30em|refs=
<ref name=adnteens>{{cite news|title=Metro news: mauled teen recovering|newspaper=[[Anchorage Daily News]]|date=September 16, 1994}}</ref>
<ref name=anchorage>{{cite book|author=Elizabeth Tower|title=Anchorage: From Its Humble Origins as a Railroad Construction Camp|url=https://books.google.com/books?id=MQ-TjoRSav4C&pg=PA142|year=1999|publisher=Epicenter Press|isbn=978-0-945397-80-9|page=142}}</ref>
<ref name=annabelle>{{cite news|last=Sullivan|first=Patty|title=Zoo to hold open service for Annabelle|newspaper=[[Anchorage Daily News]]|date=December 20, 1997|page=D1}}</ref>
<ref name=bash>{{cite news|title=Binky's bash nets $2,500|url=https://news.google.com/newspapers?id=-3o0AAAAIBAJ&sjid=kr4EAAAAIBAJ&pg=4170,2833926|newspaper=[[Anchorage Daily News]]|date=May 27, 1976|page=2}}</ref>
<ref name=belle>{{cite news|title=Southern belle for Binky bear|url=https://news.google.com/newspapers?id=Sm80AAAAIBAJ&sjid=dcAEAAAAIBAJ&pg=6159,1764139|newspaper=[[Anchorage Daily News]]|date=September 19, 1977|page=1}}</ref>
<ref name=breese>{{cite news|last=Breese |first=Darrell L. |title=Former publisher recalls the Star's early years |url=http://www.alaskastar.com/stories/011410/New_img_001_011410.shtml |accessdate=October 14, 2010 |newspaper=[[Alaska Star]] |date=January 14, 2010 |archiveurl=https://web.archive.org/web/20101013195617/http://alaskastar.com/stories/011410/New_img_001_011410.shtml |archivedate=October 13, 2010 |deadurl=yes |df= }}</ref>
<ref name=bug>{{cite news|last=Jones|first=Stan|title=Which bug killed zoo bears?|newspaper=[[Anchorage Daily News]]|date=July 22, 1995 |page=A1}}</ref>
<ref name=cal>{{cite news|title=Bear joins ad team|newspaper=[[Times-News (Hendersonville, North Carolina)|The Times-News]]|location=[[Hendersonville, North Carolina]]|date=February 25, 1977}}</ref>
<ref name=cheers>{{cite news|title=Cheers and jeers|newspaper=[[Anchorage Daily News]]|date=October 27, 1994}}</ref>
<ref name=delight>{{cite news|title=Polar delight|url=https://news.google.com/newspapers?id=ZzMeAAAAIBAJ&sjid=RMAEAAAAIBAJ&pg=4720,2747175|newspaper=[[Anchorage Daily News]]|date=May 26, 1977|page=1}}</ref>
<ref name=enge>{{cite news|last=Enge|first=Marilee|title=Binky's victim blames herself: 'It was the dumbest thing I've ever done'|newspaper=[[Anchorage Daily News]]|date=August 2, 1994}}</ref>
<ref name=folkloristics>{{cite news|last=Partnow|first=Patricia H.|title=Ursine urges and urban ungulates: Anchorage asserts its Alaskanness|newspaper=[[Western Folklore]]|date=Winter 1999}}</ref>
<ref name=herald>{{cite news|last=Badger|first=T.A.|title=When it's bear vs. tourist, Alaskans prefer the bear|newspaper=[[Miami Herald]]|date=September 29, 1994|agency=[[Associated Press]]}}</ref>
<ref name=jackie>{{cite news|last=Phillips|first=Natalie|title=Jackie the brown bear, ailing with cancer, is euthanized|newspaper=[[Anchorage Daily News]]|date=October 8, 1996|page=B3}}</ref>
<ref name=job>{{cite news|last=McCoy|first=Kathleen|title=He's got a bear of a job|url=https://news.google.com/newspapers?id=Ni0oAAAAIBAJ&sjid=nacEAAAAIBAJ&pg=4346,7382299|newspaper=[[Anchorage Daily News]]|date=December 29, 1983|page=E1}}</ref>
<ref name=mauls>{{cite news|last=Komarnitsky|first=S.J|title=Zoo's polar bear mauls tourist who climbed over two fences|newspaper=[[Anchorage Daily News]]|date=July 30, 1994}}</ref>
<ref name=meet>{{cite news|title=Meet Binky the polar bear at Alaska Children's Zoo|url=https://news.google.com/newspapers?id=-no0AAAAIBAJ&sjid=kr4EAAAAIBAJ&pg=2318,2648909|newspaper=Anchorage Daily News|date=May 26, 1976|location=Visitor's Guide Summer 1976 insert}}</ref>
<ref name=mourn>{{cite news|last=Jones|first=Stan|title=Binky fans mourn|newspaper=[[Anchorage Daily News]]|date=July 21, 1995|page=A1}}</ref>
<ref name=mystifies>{{cite news|last=Jones|first=Stan|title=Zoo bear's death mystifies officials|newspaper=[[Anchorage Daily News]]|date=July 18, 1995|page=A1}}</ref>
<ref name=old>{{cite news|title=Some Alaska Zoo animals getting old|url=http://www.peninsulaclarion.com/stories/120300/ala_120300alapm0030001.shtml|newspaper=[[Peninsula Clarion]]|date=December 3, 2000}}</ref>
<ref name=open>{{cite news|title=Zoo to open for Binky's party|url=https://news.google.com/newspapers?id=8Ho0AAAAIBAJ&sjid=kr4EAAAAIBAJ&pg=4219,1280429|newspaper=[[Anchorage Daily News]]|date=May 13, 1976|page=2}}</ref>
<ref name=prolonged>{{cite news|title=Prolonged bachelorhood for Binky|url=https://news.google.com/newspapers?id=hjEeAAAAIBAJ&sjid=Ub4EAAAAIBAJ&pg=4038,2029351|newspaper=[[Anchorage Daily News]]|date=December 16, 1977|page=2}}</ref>
<ref name=safari>{{cite news|last=Stephens|first=Jodi|title=Go on safari to the zoo|url=https://news.google.com/newspapers?id=44g1AAAAIBAJ&sjid=pJ4FAAAAIBAJ&pg=1656,982305|newspaper=[[Anchorage Daily News]]|date=April 4, 1981|page=G7}}</ref>
<ref name=sarco>{{cite news|last=Stan|first=Jones|title=Bears' death traced to sarcocystis, a rare parasite|newspaper=[[Anchorage Daily News]]|date=July 28, 1995 |page=A1}}</ref>
<ref name=spanish>{{cite news|last=Barcus|first=Gwen|title=Spanish comes in mighty handy for moving bears, making tacos|url=https://news.google.com/newspapers?id=wCAuAAAAIBAJ&sjid=YacEAAAAIBAJ&pg=4275,3797841|newspaper=[[Anchorage Daily News]]|date=July 12, 1981|page=F2}}</ref>
<ref name=suspected>{{cite news|title=Zoo bear suspected in mauling|url=https://news.google.com/newspapers?id=cX4VAAAAIBAJ&sjid=-usDAAAAIBAJ&pg=4109,3237598|newspaper=[[Eugene Register-Guard]]|date=September 13, 1994|pages=3A}}</ref>
<ref name=uncertain>{{cite news|last=Jones|first=Sally W.|title=Binky the polar bear faces uncertain future|url=https://news.google.com/newspapers?id=63o0AAAAIBAJ&sjid=kr4EAAAAIBAJ&pg=2808,608434|page=2|newspaper=[[Anchorage Daily News]]|date=May 7, 1976}}</ref>
<ref name=work>{{cite news|last=Nightingale|first=Suzan|title=Work to start on home for Binky|url=https://news.google.com/newspapers?id=9G80AAAAIBAJ&sjid=ksAEAAAAIBAJ&pg=2777,379467|newspaper=[[Anchorage Daily News]]|date=August 5, 1976|page=2}}</ref>
<ref name=zoo>{{cite web|title=Binky and Nuka memorial|url=http://www.alaskazoo.org/willowcrest/binky.htm|publisher=[[Alaska Zoo]]|archiveurl=https://web.archive.org/web/20060927210410/http://www.alaskazoo.org/willowcrest/binky.htm|archivedate=September 27, 2006}}</ref>
}}

==External links==
*[https://www.youtube.com/watch?v=8wGbCNDw-m0 Animal Planet video report on the first mauling], including footage of the attack itself
*[https://www.youtube.com/watch?v=7JUr6LRqWYk Associated Press footage of first mauling]

{{good article}}

{{DEFAULTSORT:Binky (Polar Bear)}}
[[Category:1975 animal births]]
[[Category:1995 animal deaths]]
[[Category:Animal attacks]]
[[Category:History of Anchorage, Alaska]]
[[Category:Individual polar bears]]
[[Category:Male mammals]]
[[Category:Individual animals in the United States]]