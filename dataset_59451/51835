{{redirect|JEL|the motto "Jaungoikoa eta Lagi-zaŕa"|Basque Nationalist Party}}
{{Infobox journal
| title = Journal of Economic Literature
| cover =
| discipline = [[Economics]]
| abbreviation = J. Econ. Lit.
| formernames = Journal of Economic Abstracts
| editor = Steven Durlauf
| publisher = [[American Economic Association]]
| country = United States
| frequency = Quarterly
| history = 1963–present
| website = https://www.aeaweb.org/journals/jel
| link1 = https://www.aeaweb.org/journals/jel/issues
| link1-name =Online archive
| ISSN = 0022-0515
| CODEN = JECLB3
| LCCN = 73646621
| OCLC = 01788942
| JSTOR = 00220515
}}
The '''''Journal of Economic Literature''''' is a [[peer-reviewed]] [[academic journal]], published by the [[American Economic Association]], that surveys the academic literature in [[economics]]. It was established in 1963 as the ''Journal of Economic Abstracts'',<ref name=about>[https://www.aeaweb.org/journals/jel/about-jel Journal of Economic Literature: About JEL], retrieved 6 May 2011.</ref><ref name=JoEA>{{cite web |url=http://www.jstor.org/journal/jeconabst |title=Journal of Economic Abstracts |publisher=American Economic Association |work=[[JSTOR]] |accessdate=December 17, 2010}}</ref> and is currently one of the highest ranked journals in economics.<ref>[https://ideas.repec.org/top/top.journals.simple.html IDEAS/RePEc Simple Impact Factors for Journals]</ref> As a [[review journal]], it mainly features essays and reviews of recent economic theories (as opposed to the latest research). The [[editor-in-chief]] is [[Steven Durlauf]].<ref name=about />

The journal originated a widely used [[JEL classification codes|classification system]] for publications in the field of economics.

==See also==
*[[JEL classification codes]]

== References ==
{{Reflist}}

== External links ==
*{{Official website|https://www.aeaweb.org/journals/jel}}

{{Authority control}}
[[Category:Economics journals]]
[[Category:Publications established in 1963]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Academic journals published by learned and professional societies of the United States]]


{{econ-journal-stub}}