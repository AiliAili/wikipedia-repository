{{Infobox company
|name = ZALA Aero
|logo = Zala Aero Group logo.jpg
|type = 
|slogan =
|foundation = 2003
|location = [[Izhevsk]]
|key_people = Aleksandr Zakharov
|num_employees = 
|industry = [[Aerospace]]
|products = Unmanned aerial vehicles
|revenue =
|parent = [[Kalashnikov Concern]]
|homepage = [http://zala.aero/en zala.aero]
}}
'''ZALA Aero''' (also called '''A-Level Aerosystems''') is a company specialising in [[unmanned aerial vehicle]] development, located in [[Izhevsk]], [[Russia]]. ZALA Aero has provided UAV systems for several sectors of the Russian government, including the [[Ministry of Defence (Russia)|Ministry of Defence]], and has also won contracts to supply UAVs to foreign countries.<ref name="shephard">{{cite news|url=http://www.shephard.co.uk/news/uvonline/zala-aero-to-deliver-uavs-to-ministry-of-internal-affairs-of-turkmenistan/1682/|title=Zala Aero To Deliver UAVs To Ministry of Internal Affairs of Turkmenistan|publisher=Shephard|date=2010-02-06}}</ref><ref name="about">{{cite news|url=http://zala.aero/en/1283322211/|title=About|publisher=ZALA Aero}}</ref>

== History ==
Founded in 2003 by [[Aleksandr Zakharov (businessman)|Aleksandr Zakharov]], ZALA Aero delivered its first production [[unmanned aerial vehicle|UAVs]] to the Russian [[Ministry of Internal Affairs (Russia)|Ministry of Internal Affairs]] in 2006. In addition to providing aircraft for defense and military purposes, the company actively markets its products to the energy sector, and has signed a contract with [[Gazprom]] to provide UAVs that will be used to monitor over 2,000&nbsp;km of the company's pipeline network.  ZALA Aero has also cooperated with Gazprom's [[Gazprom Space Systems|Space Systems]] division in a development project to use UAVs to transmit real-time video communications over [[satellite]] channels.<ref name="pdf"/>

In 2008, the [[ZALA 421-06]], an unmanned helicopter, and the [[ZALA 421-08]], a more conventional UAV, completed test-flying, and entered operational service. The aircraft were trialled aboard an [[icebreaker]], participating in reconnaissance to assist the ship's work.<ref name="pdf">{{cite web|url=http://zala.aero/media/zalaaero-eng.pdf|title=A-Level Aerosystems - ZALA Aero}}</ref> In 2009, ZALA Aero reached an agreement with [[Turkmenistan]]'s Ministry of Internal Affairs to supply the country with the [[ZALA 421-12]] UAV system.<ref name="shephard"/>

Currently a large-scale project for UAVs is being developed for tender to the Russian government. Originally, twelve Russian companies submitting 22 different types of unmanned aircraft were involved in the competition. In November 2010, however, the selection was narrowed down to three submissions, one of which is the [[ZALA 421-04M]], designed by ZALA Aero in association with the [[Vega Radio Engineering Corporation]]. The three UAVs selected as finalists are currently undergoing trials before the final selection of a winner in the competition.<ref name="aviationweek">{{cite news|url=http://www.aviationweek.com/aw/generic/story.jsp?id=news/awx/2010/11/11/awx_11_11_2010_p0-268691.xml&headline=Russia%20Selects%20Domestic%20UAV%20Designs&channel=defense|title=Russia Selects Domestic UAV Designs|publisher=Aviation Week|date=2010-11-12}}</ref>

== Company profile ==
ZALA Aero designs and produces unmanned aerial vehicles (UAV), as well as providing after-sale maintenance for their systems. The company's in-house design and production projects include a variety of systems related to UAV design, manufacture and operation, including autopilots, airframes, mechanical and pneumatic catapults, launchers, payloads and communication technologies.<ref name="about"/> ZALA Aero is also the only company in Russia currently producing unmanned helicopters.<ref name="rian_helicopters">{{cite news|url=http://en.rian.ru/russia/20080715/114024189.html|title=Russian company showcases unmanned police helicopter|publisher=RIA Novosti|date=2008-07-15}}</ref> The company is privately owned and located in [[Izhevsk]], [[Russia]],<ref name="about"/> and is still operated by its founder and current company president [[Aleksandr Zakharov (businessman)|Aleksandr Zakharov]].<ref name="gulftimes">{{cite news|url=http://www.gulf-times.com/site/topics/article.asp?cu_no=2&item_no=269918&version=1&template_id=39&parent_id=21|title=Russia using drones to nab illegal immigrants|publisher=Gulf Times|date=2009-01-31}}</ref>

== See also ==
*[[Aircraft industry of Russia]]
*[[Defence industry of Russia]]

== References ==
{{reflist}}

== External links ==
*[http://zala.aero/en/ Company website]

{{ZALA aircraft}}
[[Category:Unmanned_aerial_vehicle_manufacturers]]
[[Category:Aircraft manufacturers of Russia]]
[[Category:Defence companies of Russia]]
[[Category:Companies established in 2003]]