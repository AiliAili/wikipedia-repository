<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox Aircraft Begin
  |name = L.1C Bluebird IV 
  |image = Blackburn Bluebird IV.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Tourer /Trainer
  |manufacturer = [[Blackburn Aircraft]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = [[1929 in aviation|1929]]
  |introduced = 1929
  |retired = [[1947 in aviation|1947]]
  |status = 
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = [[1929 in aviation|1929]]-[[1931 in aviation|1931]]
  |number built = 58
  |unit cost = 
  |developed from = [[Blackburn Bluebird]]<!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = [[Blackburn B-2]]<!-- variants OF the topic type -->
}}
|}

The '''Blackburn Bluebird IV''' was a single-engine [[biplane]] [[Trainer (aircraft)|light trainer]]/tourer [[biplane]] with side-by-side seating designed by [[Blackburn Aircraft]]. It was an all-metal development of the wooden [[Blackburn Bluebird|Blackburn Bluebird I, II and III aircraft]].

==Design and development==
In 1929, Blackburn completely redesigned the wooden Bluebird side-by-side trainer aircraft with an all-metal structure as the '''L.1C Bluebird IV'''. With its metal structure, the Bluebird IV was larger and heavier than its wooden predecessors, and was fitted with a nearly rectangular balanced [[rudder]], without a fixed [[Vertical stabilizer|fin]] to replace the rounded fin and rudder assembly of the wooden Bluekbirds. It could be fitted with a variety of engines, with the [[de Havilland Gipsy]], [[Cirrus Engine|ADC Cirrus]] or [[Cirrus Hermes]] engines available as standard, and could also be fitted with [[floatplane|float]]s. The first Bluebird IV flew in early [[1929 in aviation|1929]], and was used to fly its owner home to [[South Africa]]  in March 1929, completing the journey between [[Croydon Airport|Croydon]] and [[Durban]] between 7 March and 15 April 1929.<ref name="Jacksonv1">{{cite book |last= Jackson|first= A.J. |title= British Civil Aircraft since 1919, Volume 1|year= 1974|publisher= Putnam|location= London|isbn=0-370-10006-9 }}</ref> A further two aircraft were built by Blackburn, who were busy fulfilling orders for military aircraft, so further construction was subcontracted to [[Saunders-Roe]], who built a further 55 aircraft <ref name="Janes aviation">{{cite book |last= Taylor |first= M J H (Editor)  |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= 158}}</ref> with [[Boulton & Paul Ltd]] producing the wings.<ref>{{cite web|url=http://daveg4otu.tripod.com/iowweb/sar.html |title=Archived copy |accessdate=2008-09-07 |deadurl=yes |archiveurl=https://web.archive.org/web/20080718113733/http://daveg4otu.tripod.com/iowweb/sar.html |archivedate=2008-07-18 |df= }}</ref>

==Operational history==
Like the wooden Bluebirds, the Bluebird IV was heavily used by flying clubs, and unfortunately also suffered high attrition, with several being lost in fatal crashes, including a number of unexplained dives into the ground from normal cruising flight.<ref name="Jacksonv1"/>

Privately owned Bluebird IVs undertook a number of pioneering long-distance flights, the most famous of which was the round-the-world trip by [[The Hon Mrs Victor Bruce]], and also included a number of flights to [[Australia]] and Africa.<ref name="Jacksonv1"/>
No Bluebirds survive today, the last being scrapped in 1947.<ref name="Jacksonv1"/>

==Operators==
;{{AUS}}
*[[Royal Australian Air Force]]
;{{flag|Canada|1921}}
*[[Royal Canadian Air Force]]
;{{flagicon|India|British}} [[British Raj|British India]]
;{{UK}}
*[[Royal Air Force]]

==Specifications (Bluebird IV (Gipsy I engine))==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=British Civil Aircraft since 1919, Volume 1 <ref name="Jacksonv1"/>
|crew=two
|capacity=
|length main= 23 ft 2 in
|length alt= 7.06 m
|span main= 30 ft 0 in
|span alt= 9.15 m
|height main= 9 ft 0 in
|height alt= 2.74 m
|area main= 246 ft²
|area alt= 22.9 m²
|airfoil=
|empty weight main= 1,070 lb
|empty weight alt= 486 kg
|loaded weight main= 1,750 lb
|loaded weight alt= 795 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[de Havilland Gipsy]] I
|type of prop=Four cylinder inline engine
|number of props=1
|power main= 100 hp
|power alt= 75 kW
|power original=
|max speed main= 104 kn
|max speed alt= 120 mph, 193 km/h
|cruise speed main= 74 kn
|cruise speed alt= 85 mph, 137 km/h
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 278 nmi
|range alt= 320 mi, 515 km
|ceiling main=  
|ceiling alt=  
|climb rate main= 730 ft/min
|climb rate alt= 3.7 m/s
|loading main= 7.11 lb/ft²
|loading alt= 34.7 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.057 hp/lb
|power/mass alt= 0.094 kW/kg
|more performance=
|armament=
|avionics=
}}

==See also==
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Blackburn Bluebird]]
*[[Blackburn B-2]]
|similar aircraft=*[[Avro Avian]]  
*[[de Havilland Moth]]
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{Reflist}}

==External links==
{{commons category|Blackburn Bluebird IV}}
*[http://www.ivyandmartin.demon.co.uk/v_bluebird.htm The Hon Mrs Victor Bruce's Bluebird]
*[http://www.britishaircraft.co.uk/aircraftpage.php?ID=595 British Aircraft Directory]

{{Blackburn aircraft}}

{{DEFAULTSORT:Blackburn Bluebird Iv}}
[[Category:British sport aircraft 1920–1929]]
[[Category:Blackburn aircraft|Bluebird IV]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]