{{for|the 1951 novel of a similar name|The Caine Mutiny}}
{{Infobox Simpsons episode
|episode_name=The Canine Mutiny
|image=[[Image:Thecaninemutiny.png|235px]]
|image_caption=Bart and Laddie dispose of the credit card.
|episode_no=173
|prod_code=4F16
|airdate=April 13, 1997
|show runner=[[Bill Oakley]]<br>[[Josh Weinstein]]
|writer=[[Ron Hauge]]
|director=[[Dominic Polcino]]
|blackboard="A fire drill does not demand a fire."<ref name="book">{{cite book |last=Groening |first=Matt |authorlink=Matt Groening |editor1-first=Ray |editor1-last=Richmond |editor1-link=Ray Richmond |editor2-first=Antonia |editor2-last=Coffman |title=[[The Simpsons episode guides#The Simpsons: A Complete Guide to Our Favorite Family|The Simpsons: A Complete Guide to Our Favorite Family]]  |edition=1st |year=1997 |location=New York |publisher=[[HarperPerennial]] |lccn=98141857 |ol=433519M |oclc=37796735 |isbn=978-0-06-095252-5 |page=233 |ref={{harvid|Richmond & Coffman|1997}}}}.</ref>
|couch_gag=The couch is folded out into a bed with Grampa asleep on it. Grampa can only utter a cry of “Huh?” before The Simpsons fold him into the couch as they sit down as normal.<ref name="BBC">{{cite web |url=http://www.bbc.co.uk/cult/simpsons/episodeguide/season8/page20.shtml |title=The Canine Mutiny
|accessdate=2007-03-30 |author1=Martyn, Warren |author2=Wood, Adrian |year=2000 |publisher=BBC}}</ref>
|guest_star=[[Frank Welker]] as Laddie
|commentary=[[Josh Weinstein]]<br>[[Dominic Polcino]]<br>[[George Meyer]]
|season=8
}}
"'''The Canine Mutiny'''" is the twentieth episode of ''[[The Simpsons]]''<nowiki>'</nowiki> [[The Simpsons (season 8)|eighth season]]. It originally aired on the [[Fox Broadcasting Company|Fox network]] in the United States on April 13, 1997.<ref name="book"/> It was written by [[Ron Hauge]] and directed by [[Dominic Polcino]].<ref name="book"/> Bart applies for a credit card and goes on a spending spree when it arrives, including an expensive trained dog called 'Laddie'. It guest stars voice actor [[Frank Welker]] as Laddie,<ref name="BBC"/> a [[parody]] of [[Lassie]]. The episode's title references the novel ''[[The Caine Mutiny]]''.

==Plot==
When [[Bart Simpson|Bart]] complains he never gets any mail, [[Marge Simpson|Marge]] gives him the family's [[Advertising mail|junk mail]]. One piece contains a [[credit card]] application, which Bart fills out under the name of his dog [[Santa's Little Helper]], whom he claims to have the occupation "[[proctologist|butt doctor]]". The company misreads the name as "Santos L. Halper", and the credit card application is approved. Before long, Bart receives a credit card. Bart goes on a shopping spree, buying the family some very expensive gifts from a mail order catalog: [[Vancouver]] smoked [[salmon]] and a [[radio]]-[[frying pan]] for Marge, a [[golf]] shirt with corporate logo for [[Homer Simpson|Homer]], "Trucker's Choice Stay-Alert Capsules" for [[Lisa Simpson|Lisa]] and many things for himself. The item that Bart anticipates the most is a finely-bred, pre-trained [[Rough Collie|collie]]. Not letting its [[United States dollar|US$]]1,200 purchase price stand in the way, Bart promptly orders one. Upon its arrival Bart learns the dog's name is Laddie, and that he has been trained to perform a wide variety of tasks.  The rest of the family fall in love with the new dog, while Santa's Little Helper goes unnoticed.

Bart fails to pay off "Santos L. Halper's" credit card bill, and it is not long before he gets a call from a debt [[collection agency]] demanding payment.  When the calls and collection letters persist, Bart enlists Laddie to help him bury his ill-gotten credit card.  Later, [[repossession|repo men]] arrive to take back all of the things Bart has purchased.  Lisa demands an explanation, and Bart is forced to admit the truth.  When a repossessor asks for the $1,200 dog to be returned, Bart identifies Santa's Little Helper as the dog he purchased.  The [[greyhound]] is herded into the truck and Bart sadly watches as it drives away.  Noticing that Santa's Little Helper is gone, the family begins to bond with Laddie, except for Bart, who fears for Santa's Little Helper's fate.  When an exhausted Bart gives Laddie yet another walk, the collie saves the life of [[List of recurring The Simpsons characters#Baby Gerald|Baby Gerald]].  At the ceremony honoring Laddie's heroism, [[Chief Wiggum]] decides that Laddie would make the perfect [[police dog]].  Bart gives him to the Springfield police force and is forced to explain why the family now has no dog at all, while [[cry|sobbing]].  Homer instructs Bart to do whatever it takes to get Santa's Little Helper back and he goes hunting around the town, trying to find his old pal.  Bart eventually learns from [[Reverend Timothy Lovejoy|Reverend Lovejoy]] that the dog was given to a [[parishioner]] named Mr. Mitchell.

Bart visits Mr. Mitchell, who is [[blindness|blind]] and [[loneliness|lonely]], and asks for his dog back, but when he sees how the man and Santa's Little Helper have bonded, he grows heartsick and leaves.  Still determined to get his dog back, Bart makes a late-night visit to Mr. Mitchell's home in hopes of retrieving Santa's Little Helper.  The pair are reunited, but Bart traps himself in a closet.  Mr. Mitchell calls the police and demands that Bart be charged with burglary, but Bart explains that he's just a boy, and that Santa's Little Helper was his dog to begin with.  To solve the problem, Bart and Mitchell let Santa's Little Helper decide which owner he prefers, by having both of them call him.  He chooses Bart.  Chief Wiggum arrives with Laddie, who immediately sniffs out a bag of [[marijuana]] in Mr. Mitchell's pocket.  Bart and Santa's Little Helper head home, leaving the police to "finish up" with Mitchell, as more officers arrive with beer and dates.

==Production==
The episode uses the full opening sequence because the story came out short.<ref name=Weinstein>{{cite video |people=Weinstein, Josh |date=2006 |title=The Simpsons The Complete Eighth Season DVD commentary for the episode "The Canine Mutiny" |medium=DVD |publisher=20th Century Fox}}</ref> Despite this, a large sequence was cut from the middle of the episode,<ref name=Weinstein/> with half of the episode having to be re-written after the [[Storyboard#Animatics|animatic]] had been finished.<ref name=Polcino>{{cite video |people=Polcino, Dominic |date=2006 |title=The Simpsons The Complete Eighth Season DVD commentary for the episode "The Canine Mutiny" |medium=DVD |publisher=20th Century Fox}}</ref> The main plot of the episode came from an original idea that the family would be issued a credit card in the name "Hobart Simpson" and that Bart would use that.<ref name=Weinstein/> An original side-story was that Lisa would become addicted to the "Trucker's Choice" [[pep pills]].<ref name=Polcino/> Originally, instead of going to the dog park, the family took Laddie to a waterfall and he performed a series of dives,<ref name=Polcino/> but it was scrapped as it had already been proven that Laddie was a form of "superdog".<ref name=Meyer>{{cite video |people=Meyer, George |date=2006 |title=The Simpsons The Complete Eighth Season DVD commentary for the episode "The Canine Mutiny" |medium=DVD |publisher=20th Century Fox}}</ref> Likewise, Laddie rescuing Baby Gerald was originally a complicated rescue scene, but was cut into showing the aftermath.<ref name=Weinstein/>

Laddie was designed to resemble a real dog.<ref name=Weinstein/> The catalog Bart uses is a combination of the ''[[Lillian Vernon]]'' catalog and ''[[The Sharper Image]]''.<ref name=Weinstein/> The opening stemmed from the fact that the show had not had a sequence where the family received mail, and the writers wanted to create a joke about the different types of mail each of the family get.<ref name=Weinstein/> After Bart's "dog burning" fantasy, when he hears a ship's horn in the distance, there was originally going to be a faint cry of "more dogs", but it was deemed that it took the joke too far.<ref name=Weinstein/> [[Hank Azaria]] ad-libbed the entire sequence during the credits in which Chief Wiggum and Lou sing along to "Jammin'".<ref name=Weinstein/>

==Cultural references==
The title is a reference to the [[The Caine Mutiny|novel]] and [[The Caine Mutiny (film)|film]] ''The Caine Mutiny''. The dog "Laddie" is a play on [[Lassie]], in terms of name, appearance and uncanny intelligence.<ref name="BBC"/><ref name=Weinstein/> Marge listens to the song "[[You Really Got Me]]" by [[The Kinks]] played on the frying pan radio.<ref name="BBC"/> At the end of the episode, the song "[[Jamming (song)|Jamming]]" by [[Bob Marley]] is played.<ref name="BBC"/> The design of the "Repo Depot" is based very loosely on the repossession agency from ''[[Repo Man (1984 film)|Repo Man]]''.<ref name=Weinstein/> The address of Mr. Mitchell's house 57 Mt. Aubum Street is one of addresses of the [[Harvard Lampoon]].<ref name=Weinstein/> Mr. Mitchell having a dead parrot that he believes to still be alive is a reference to the "[[Dead Parrot sketch|Dead Parrot]]" [[Monty Python]] sketch.<ref name=Meyer/>

==Reception==
In its original broadcast, "The Canine Mutiny" finished 43rd in ratings for the week of April 7–13, 1997, with a [[Nielsen ratings|Nielsen rating]] of 8.1, equivalent to approximately 7.9 million viewing households. It was the fourth highest-rated show on the Fox network that week, following ''[[The X-Files]]'', ''[[King of the Hill]]'', and ''[[Beverly Hills, 90210]]''.<ref>{{cite news |title=ABC ratings take a record nose dive |work=Sun-Sentinel |author=Associated Press |page=4E |date=April 17, 1997}}</ref>

The episode's ending with Chief Wiggum and Lou singing along to "Jamming'" by Bob Marley is often cited as one of the best endings in the history of the show.<ref name=Weinstein/> The authors of the book ''I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide'', Warren Martyn and Adrian Wood, called it "A sweet episode".<ref name="BBC"/> Homer's line "There, there, shut up boy" is one of [[Josh Weinstein]]'s favorites.<ref name=Weinstein/>

==References==
{{Reflist}}

==External links==
{{wikiquote|The_Simpsons/Season_8#The_Canine_Mutiny|"The Canine Mutiny"}}
{{portal|The Simpsons}}
*[http://www.thesimpsons.com/#/recaps/season-8_episode-20 "The Canine Mutiny"] at The Simpsons.com
*{{Snpp capsule|4F16}}
*{{tv.com episode|the-simpsons/the-canine-mutiny-1458}}
*{{imdb episode|0701229}}

{{The Simpsons episodes|8}}
{{good article}}

{{DEFAULTSORT:Canine Mutiny, The}}
[[Category:The Simpsons (season 8) episodes]]
[[Category:1997 American television episodes]]