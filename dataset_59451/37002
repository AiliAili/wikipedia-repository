{{good article}}
{{Infobox TV channel| 
name = BabyFirst
| logosize  = 230px
| logofile  = Babyfirst-logo.png
| logoalt   = 
| logo2     = 
| launch    = May 11, 2006
| picture format   = [[480i]] ([[SDTV]])<br>[[720p]] ([[HDTV]]) [[16:9]] Letterbox
| share            = 
| share as of      = 
| share source     = 
| network          = 
| language         = [[English language|English]]<br> [[Spanish language|Spanish]]<br>[[French language|French]]<br>[[Turkish language|Turkish]]<br>[[German language|German]]<br> [[Portuguese language|Portuguese]]<br>[[Polish language|Polish]]<br>[[Thai language|Thai]]
| owner         = [[Regency Enterprises]]<br>Kardan N.V.<br>Bellco Capital, LLC
| slogan           = ''Watch Your Baby Blossom''
| country          = 
| broadcast area   = United States<br>Mexico<br> Europe<br>Asia
| headquarters     = [[Los Angeles, California]]
| former names     = 
| timeshift names  = 
| web              = [http://www.babyfirsttv.com/ BabyFirstTV.com]
| sat serv 1       = [[DirecTV]] (United States)
| sat chan 1       = 293 (SD)
| sat serv 2       = [[Dish Network]] (United States)
| sat chan 2       = 823
|sat serv 3 = [[Sky]] (Latin America)
|sat chan 3 = 327
|sat serv 4 = [[Digiturk]] (Turkey)
|sat chan 4 = 64
|sat serv 5 = [[Dish Home]] (Nepal)
|sat chan 5 = 802
| cable chan 1     = Channel 314
| cable serv 1     = [[XFINITY]]/[[Comcast]]
| cable serv 2      =[[Time Warner Cable]]
| cable chan 2      = 256
|cable serv 3 = [[Rogers Cable|Rogers]] (Canada)
|cable chan 3 = 233
| cable serv 4      =[[UPC Polska]] (Poland)
| cable chan 4      = 667
| cable serv 5      =[[ZON TVCabo]] (Portugal)
| cable chan 5      = 46
| cable serv 6      =[[First Media]] (Indonesia)
| cable chan 6      = 128
| cable serv 7      = [[SkyCable]] (Philippines)
| cable chan 7      = 121
| cable serv 8      = [[Parasat Cable TV]] (Cagayan de Oro)
| cable chan 8      = 103
| iptv serv 1      =[[Orange (telecommunications)|Orange]] (Spain)
| iptv chan 1      =Mobile
| iptv serv 2     =[[Vodafone]] (Spain)
| iptv chan 2      =Mobile
| iptv serv 3     =[[Turkcell]] (Turkey)
| iptv chan 3      =Mobile
| iptv serv 4     =[[HyppTV]] (Malaysia)
| iptv chan 4       551
| iptv serv 5   =[[StarHub TV]](Singapore)
| iptv chan 5 =  301
| iptv serv 6 =  [[CHT MOD]] (Taiwan)
| iptv chan 6 =  122
| iptv serv 7     =[[:zh:澳門有線電視|Macau Cable TV]] ([[Macau]])
| iptv chan 7 =  32
| iptv serv 8      =[[Verizon FiOS]] (United States)
| iptv chan 8      = 765 (HD)<br>1719 (Spanish feed)
}}
'''BabyFirst''' is a media company that produces and distributes content for babies through television, the internet and mobile apps. The content is intended to develop a baby's skills, such as color recognition, counting and vocabulary. There are about 90 BabyFirst TV shows and 41 apps for mobile devices. As of 2014, the network is distributed to 81 million homes, and is based in [[Los Angeles, California]].

BabyFirst was founded in 2004 by Guy Oranim and Sharon Rechter. Its first broadcast was through [[DirecTV]] in 2006. It was founded by [[Regency Enterprises]], Kardan, and Bellco Capital. Distribution expanded through agreements with the Echostar [[Dish Network]], [[Comcast]], [[AT&T U-verse]] and others. It also developed a premium BabyFirst YouTube channel, and mobile apps. One app developed with AT&T U-verse allows babies to interact with the television programming by drawing on a mobile device.

==History==

===Origins===
BabyFirst was founded in 2004<ref name="canal"/> by Guy Oranim and Sharon Rechter.<ref name="lat"/> The network was launched on May 11, 2006 on DirecTV and was later made available through EchoStar's Dish Network that June.<ref name="eleven"/><ref name="one">{{cite news |newspaper=The New York Post|date=May 12, 2006 |title='Screen Test' Toddler - Kid & Folks Rate Baby TV |pp=8 |last=Robinson |url=http://nypost.com/2006/05/12/screen-test-toddler-kid-folks-rate-baby-tv/}}</ref> It was made available through EchoStar's Dish Network that June.<ref name="four"/> The BabyFirst network is based in [[Los Angeles]] and was initially funded by Regency Enterprises (a Hollywood movie studio), Kardan (a holding company) and Bellco Capital (a private fund).<ref name="four"/> BabyFirst was controversial when it was introduced, because it was the first 24-hour channel for children six months to three years in age.<ref name="four"/><ref name="Itzkoff">{{cite news|last=Itzkoff|first=Dave|title=TV Moves A Step Closer To the Womb|work=The New York Times|page=1|date=May 21, 2006|url=https://www.nytimes.com/2006/05/21/arts/21itzk.html}}</ref> However, the channel was popular among parents<ref name="seven">{{cite news|title=Diaper Demographic; TV, Video Programming for the Under-2 Market Grows Despite Lack of Clear Educational Benefit|newspaper=The Washington Post|date=February 24, 2007|first=Annys|last=Shin}}</ref><ref>Karen B. TV for tots a turnoff. Courier Mail, The (Brisbane) [serial online]. October 14, 2009;:33. Available from: Newspaper Source Plus, Ipswich, MA. Accessed May 22, 2014.</ref><ref name="Clemetson">{{cite news|last=Clemetson|first=Lynette|title=Parents Making Use of TV Despite Risks|work=The New York Times|page=16|date=25 May 2006|url=https://www.nytimes.com/2006/05/25/us/25crib.html}}</ref> and grew quickly.<ref name="eleven"/>

===Distribution expansion===
In 2007, BabyFirst obtained agreements to distribute the channel in the [[United Kingdom]] through the BSkyB satellite network as well as in Mexico through Sky Mexico and Cablevision.<ref name="odlo">{{cite news|title=BabyFirstTV crawls its way to U.S.|date=March 20, 2007|first=Steve|last=Brennan|newspaper=The Hollywood Reporter|accessdate=May 23, 2014|url=http://www.hollywoodreporter.com/news/babyfirsttv-crawls-way-us-132343}}</ref> A French version was introduced with CanalSat in 2007.<ref name="canal">{{cite news|title=Fox, CanalSat members of a baby boom |date=October 10, 2007 |first1=Elizabeth |last=Guid |first2=Rebecca |last2=Leffler|publisher=Hollywood Reporter|url=http://www.hollywoodreporter.com/news/fox-canalsat-members-a-baby-152187|accessdate=June 17, 2014}}</ref> By the end of 2007, it had arranged broadcasting agreements throughout [[Europe]],<ref name="fifteen">{{cite news|title=24-hour TV for kids under 3 is on the air|publisher=Winnipeg Free Press|date=July 27, 2007|first=Shannon|last=Proudfoot}}</ref> the [[Middle-East]]<ref name="odlo"/><ref name="fifteen"/> and [[Canada]]<ref>{{cite news|title=BabyFirst crawling onto Canadian TV|date=July 26, 2007|first=Etan|last=Vlessing|newspaper=The Hollywood Reporter}}</ref> among others.

BabyFirst also started broadcasting in ten territories in the Asia Pacific, such as China and Korea.<ref>{{cite news|newspaper=Television Asia|title=Crossing the channels: despite the economic crisis, this year has seen a slew of new channels roll out in the region, with some still set to launch|date=November 1, 2008 | last=Wong|first=Christine}}</ref> In October 2008, SingTel started distributing the channel to the Singapore audience.<ref name="nine">{{cite news|publisher=Television Asia|title=BabyFirstTV on SingTel's mio TV|date=October 1, 2008}}</ref> It was also being broadcast in Africa and Latin America.<ref name="eleven">{{cite news|title=What can TV do for your baby? 2 channels specialized in child fare are thriving, but critics cite risks of too much viewing|newspaper=International Herald Tribune|date=May 19, 2008 | first=Doreen|last=Carvaja}}</ref> In May 2008, it signed a distribution agreement with [[Time Warner Cable]].<ref name="eleven"/><ref>{{cite news|title=Time Warner to carry BabyFirst|first=Michael|last=Schneider|url=http://variety.com/2008/more/news/time-warner-to-carry-babyfirst-1117985425/|date=May 9, 2008|accessdate=May 23, 2014}}</ref> In 2009, HBO Asia became the exclusive distributor for the channel in Asia.<ref>{{cite news|title=HBO Asia strikes agreement to represent BabyFirst, WarnerTV across Asia|publisher=Television Asia|date=December 1, 2009}}</ref> A bilingual Latin/English channel, BabyFirst Americas, was launched with [[Comcast]] in 2012.<ref>{{cite news|title=Comcast to start new minority-owned cable channels|publisher=Associated Press|date=February 21, 2012 |first=Frazier|last=Moore}}</ref><ref>{{cite news|title= Comcast Outlines Plan to Carry 4 Minority-Owned Channels|work=The New York Times|page=2|url=https://query.nytimes.com/gst/fullpage.html|accessdate=August 28, 2014}}</ref> A premium BabyFirst [[YouTube]] channel was introduced in June 2013.<ref name="lat">{{cite news|date=June 6, 2013|url=http://articles.latimes.com/2013/jun/06/business/la-fi-ct-babyfirst-youtube-channel-new-investor-20130606|newspaper=Los Angeles Times|first=DiAngelea |last=Miller |title=BabyFirst, with premium YouTube channel and new investor, expands}}</ref>

In the early 2000s, the [[Federal Trade Commission]] responded to a complaint by the Campaign for a Commercial Free Childhood alleging that BabyFirst's advertising that it helped babies develop skills was misleading. The FTC did not impose any sanctions.<ref name="eleven"/><ref name="twelve">{{cite news|last=Lafayette|first=Hayes|title=McPherson Seeks More Carriage for Kid-TV Net|newspaper=Broadcasting & Cable |date=September 2, 2013|accessdate=May 23, 2014}}</ref> As of 2014, it has 81 million viewers<ref>{{cite news|title=KneeBouncers Joins BabyFirst to Develop Interactive TV Series|first=Cherese |last=Jackson|date=February 22, 2014|url=http://guardianlv.com/2014/02/kneebouncers-joins-with-babyfirst-to-develop-interactive-tv-series/#zhFveRZ1PCYc57YY.99|accessdate=June 2, 2014}}</ref> and is broadcast in 33 countries, in ten languages.<ref name="baby" />

===Recent history===
In 2013, former [[American Broadcasting Company|ABC Network]] President Steven McPherson<ref name="lat"/> and Rich Frank, the former chairman of [[Disney Channel]]<ref name="disney">{{cite news|title=With Rich Frank on-board, BabyFirst kicks into ad mode|date=December 9, 2013|first=Wendy|last=Getzler|url=http://kidscreen.com/2013/12/09/with-rich-frank-on-board-babyfirst-kicks-into-ad-mode/#ixzz2n0AqVT8H|publisher=Kidscreen|accessdate=June 3, 2014}}</ref> became investors and board members as the company worked to develop new content and improve advertising revenues.<ref name="disney" /> In May 2014, BabyFirst and [[AT&T U-verse]] released a co-developed second-screen app for mobile devices that allows children to interact with the television programming through tablets or smartphones.<ref name="olop">{{cite news|title=AT&T, BabyFirst Team On U-verse App|date=May 8, 2014|first=Jeff|last=Baumgartner|url=http://www.multichannel.com/news/tv-apps/att-babyfirst-team-u-verse-app/374423|newspaper=Multichannel News}}</ref>

==Programming==
[[File:BabyFirst video content.ogv|thumb|right|A sample of BabyFirst programming]]BabyFirst's television channel provides 24-hour programming for babies.<ref name="three">{{cite news |title=Ok, I admit it: Treehouse is a parent's dream |first=Kate |last=Taylor |newspaper=Globe and Mail|date=August 8, 2007|accessdate=May 22, 2014|url=http://www.theglobeandmail.com/arts/ok-i-admit-it-treehouse-is-a-parents-dream/article18142827/}}</ref> About 90 percent of the 90 shows BabyFirst produces are original content created at BabyFirst's studios.<ref name="four"/><ref name="baby"/> It produces and broadcasts short videos three to five minutes in length that are either live-action or animated.<ref name="Itzkoff"/><ref name="baby"/>

''The New York Times'' described the content as "decidedly unhurried" and said it makes extensive use of bright colors and upbeat music.<ref name="Itzkoff"/> Programming development is guided by child psychology experts and is designed to encourage a child's skills development, such as counting, vocabulary and color recognition.<ref name="lat"/><ref name="four">{{cite news|title=EchoStar to offer BabyFirst channel|newspaper=Rocky Mountain News|date=June 14, 2006|first=Joyzelle|last=Davis|url=http://www.highbeam.com/doc/1G1-146996029.html|accessdate=May 22, 2014}}</ref><ref name="fifteen" /><ref>{{cite news|newspaper=The Star Phoenix |date=July 27, 2007 |title=New network for the newly born; Commercial-free, 24-hour station for babies to launch in Canada |first=Shannon |last=Proudfoot |pp=B8 |url=http://www.canada.com/story.html?id=da6d6cd3-43a5-4da1-8a9f-7b97ae147f73 |deadurl=yes |archiveurl=https://web.archive.org/web/20140809190507/http://www.canada.com/story.html?id=da6d6cd3-43a5-4da1-8a9f-7b97ae147f73 |archivedate=2014-08-09 |df= }}</ref> The BabyFirst logo in the corner changes colors to indicate the skills a segment is intended to develop. Late-night programming is intended to lull viewers to sleep.<ref name="Itzkoff" />

There are also 41 BabyFirst apps for mobile devices.<ref name="baby">{{cite news|title=Baby Boom: Profile: BabyFirst|date=Spring 2014}}</ref> An app available to AT&T U-verse viewers allows children to draw on a mobile device and have the drawing appear on the television screen.<ref name="olop" />

Some experts argue that exposing children to television at such an early age is taking technology too far or that parents are using BabyFirst as a digital babysitter. Parents in-turn refute that argument, claiming that experts have lost touch with the realities of raising a child.<ref name="Brooks2008">{{cite book|author=Karen Brooks|title=Consuming Innocence: Popular Culture and Our Children|url=https://books.google.com/books?id=7mmFBXf-m7YC&pg=PA129|year=2008|publisher=Univ. of Queensland Press|isbn=978-0-7022-3645-7|pages=129}}</ref> BabyFirst suggests the programming is intended to be watched by parents and their children together in an interactive way.<ref name="thirteen">{{cite news|title=BabyFirst develops baby's first apps|first=Nicole|last=Villalpando|url=http://www.statesman.com/news/lifestyles/parenting/babyfirst-develops-babys-first-apps-1/nRNjW/|date=August 24, 2012 |accessdate=May 23, 2014|publisher=The Statesman}}</ref> The [[American Academy of Pediatrics]] recommends against exposing children under the age of two to television, while a 2003 study by the [[Kaiser Family Foundation]] found that most children under two years of age are already watching TV.<ref name="four" /> According to ''The Washington Post'', very little is known about whether young children watching television has a negative or positive effect on them.<ref name="seven" />
It has some acquired programs such as [[Juno Baby]], [[Mio Mao]], [[Musti (TV series)|Musti]], [[Suzy's Zoo]], and [[Tec the Tractor]].

It shows Original Programs like [[Tillie Knock Knock]], [[VocabuLarry!|VocabuLarry,]] [[Peek-a-boo I See You]], [[The Numbers Farm]], and more.

==References==
{{reflist|30em}}

==External links==
* [http://www.babyfirsttv.com/ Official website]
* [http://www.babyfirsttv.com/tv/around-the-globe Current list of distributors and channels]

{{U.S. premium television services (variety)}}
{{U.S. family-oriented television channels}}
{{Children's Television Channels in Portugal}}
{{Children's channels in UK & Ireland}}

{{DEFAULTSORT:Babyfirsttv}}
[[Category:American television networks]]
[[Category:Commercial-free television networks]]
[[Category:Children's television networks in the United States]]
[[Category:Television channels and stations established in 2003]]
[[Category:Early childhood education]]
[[Category:Articles containing video clips]]