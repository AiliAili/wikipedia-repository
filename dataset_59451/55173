{{Infobox journal
| title = Academic Medicine
| cover =
| editor = David P. Sklar
| formernames =Bulletin of the Association of American Medical Colleges, Journal of the Association of American Medical Colleges, Medical Education, Journal of Medical Education
| discipline = [[Academic medicine]]
| abbreviation = Acad. Med.
| publisher = [[Association of American Medical Colleges]]
| country = United States
| frequency = Monthly
| history = 1926-present
| openaccess = 
| license =
| impact = 2.934
| impact-year = 2014
| website = http://journals.lww.com/academicmedicine/pages/default.aspx
| link1 = 
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC =
| LCCN = 
| CODEN = 
| ISSN = 1040-2446
| eISSN = 1938-808X
}}
'''''Academic Medicine''''' is the monthly [[peer-reviewed]] [[medical journal]] of the [[Association of American Medical Colleges]].

== History ==
The journal was established in 1926 as the ''Bulletin of the Association of American Medical Colleges''. It was renamed ''Journal of the Association of American Medical Colleges'' in 1929. In 1951 it briefly became ''Medical Education'' then ''Journal of Medical Education''. In 1989 it took its current name of ''Academic Medicine''.<ref name=AboutAcadMed>{{cite web |url=http://journals.lww.com/academicmedicine/Pages/journaltimeline.aspx |title=About the Journal | publisher=Academic Medicine |accessdate=8 October 2015}}</ref>

In the course of its history, the journal has had nine editors. David P. Sklar is the present [[editor-in-chief]], appointed in 2013.<ref name=AboutAcadMed/>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8904605 |title=Academic Medicine |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=8 October 2015}}</ref>
* [[Science Citation Index]]

== References ==
{{reflist|30em}}

== External links ==
* {{Official website}}

{{medical-journal-stub}}
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:General medical journals]]
[[Category:Academic journals published by learned and professional societies of the United States]]
[[Category:Publications established in 1926]]