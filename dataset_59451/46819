{{other people||David Campbell (disambiguation)}}
{{Infobox artist
| bgcolour      = #76A8FF
| name          = David Campbell
| image         = 
| imagesize     = 200px
| birth_name    = David Campbell
| birth_date    = {{Birth date|1936|03|31}}
| birth_place   = [[Takoma Park, Maryland]]
| nationality   = American
| field         = [[Oil painting]]<br>[[Drawing]]<br>[[Watercolor painting]]
| movement      = [[Realism (visual arts)|Realism]]
}}
'''David Campbell''' (born 1936) is an [[United States|American]] [[realist painter]], [[poet]], and faculty member at the [[Maine College of Art]].  Many of his [[oil paintings]], [[watercolors]], and [[drawings]] feature urban and industrial scenes rendered meticulously over the course of months and even years of [[plein-air]] sessions, deftly imbuing quotidian subjects with abstract significance.  A large portion of Campbell's work captures the vibrancy and intricacy of [[urban area|urban]] and [[suburban]] vistas, especially around his former residence in [[Somerville, Massachusetts]].<ref>Gerold Wunderlich & Company, ''David Campbell'', introduction by Josiah Lee Auspitz, (New York: Gerold Wunderlich & Co., 1990), p. 5.</ref>

== Early life ==
Born in [[Takoma Park]], [[Maryland]] on March 31, 1936, Campbell studied at the [[Art Students League of New York]] from 1957-1961.<ref name="greenhutgalleries">http://www.greenhutgalleries.com/pdfs/images/20111025001714C.pdf</ref>    Following a stint in the [[U.S. Army]] in [[Korea]], Campbell spent six years in [[Italy]] before moving back to the [[United States]] during the turbulent year of 1968.<ref>Pat Davidson Reef, "Frost Gully is Serious about good art," ''Sun Journal'' (Lewiston, MA), November 13, 1998, p. 12.</ref>

== Work ==
Critical evaluations of his work are as varied as his subjects, which range from fallow fields, to crowded streets and industrial parks framed by piles of scrap metal and junk.  While one reviewer noted a recurring "theme of desolation, which when coupled with the subject matter can also be understood as comment on America's ever diminishing industrial base and the lives it once sustained," others have found wellsprings of different emotions.  Referring to Campbell's 1980's work "Reading," in which "a woman sits on her claustrophobic back porch, closed in by household junk on either side...the neighborhood houses are close behind, and an ugly apartment building blocks the sky," art critic Linda Bamber nonetheless finds that the woman's "posture speaks of her complete absorption in the book on her lap...The smoke from the woman's cigarette could be her spirit ascending, completely unconstrained by all the enclosures."<ref name="Bamber">Linda Bamber, "Made in Somerville," ''Boston Review'', Dec. 1986, p. 15.</ref>

Often returning to the same location throughout the year to capture seasonal changes, Campbell insists on rendering all his works through [[plein-air]] sessions.  "Some people work from photographs," Campbell explained in an interview for the [[Boston Review]],
::"and that's fine; but for my part I'd rather bag groceries in the supermarket than do that.  For me the whole part of it is to make contact with my surroundings.  I want to get to a caring level about where I am.  How can you do that with a photograph?  It's more than just the visual act.  You have to immerse yourself in the scene."<ref name="Bamber"/>

This attention to detail and place is what gives Campbell's paintings "scientific, cerebral, and spiritual" significance, as author [[Michael Kammen]] wrote in his book ''A Time to Every Purpose: The Four Seasons in American Culture''.<ref>Michael Kammen, ''A Time to Every Purpose: The Four Seasons in American Culture'' (Chapel Hill, NC: The University of North Carolina Press, 2004), 244.</ref>  Campbell described his work ''Summer'' (1987), which depicts a rolling field in [[North Andover, MA]], in an interview with the author:
::"Because I see nature as the basis of life, it is sacred to me...The fact that I live in a city makes me feel especially exhilarated when I visit this field.  The look of things is so thoroughly different through the seasons that my industrial home neighborhood is changeless by comparison.  Important cycles other than the cycle of the seasons are recorded in the series."<ref>Michael Kammen, ''A Time to Every Purpose: The Four Seasons in American Culture'' (Chapel Hill, NC: The University of North Carolina Press, 2004), 244-245.</ref>

Complex and dominating cloudscapes also feature prominently in Campbell's work, often occupying two-thirds or more of the canvas.  One review in the artsMEDIA section of the ''[[Boston Globe]]'' found that his "heavy, colorless skys, which foretell an impending doom" appeared to be "angry; and they seem ready to burst with a fury powerful enough to mercifully finish off the slow destruction taking place below."<ref>Adam Lambert, "Streets and Roads," ''Boston Globe'', artsMEDIA section, May 1997, p. 23.</ref>

Recently, Campbell has found inspiration in [[Sea|maritime]] scenes around his home in [[Portland, Maine]] prompting him to paint a series of [[oil tankers]] to fill out his corpus of industrial and [[cityscapes]].  "I've always been attracted to complexity in whatever subject I am painting," Campbell explained in an interview for the ''[[Maine Sunday Telegram]]'', "And let's face it, there's a lot of complexity in these tankers.  There are a lot of elements, and a lot of things going on."<ref>Bob Keyes, "Size Matters," ''Maine Sunday Telegram'', March 14, 2010, D1-2.</ref>  His paintings of these behemoth ships have appeared in an exhibition at the [[Maine Maritime Museum]] in [[Bath, Maine|Bath]], titled "Some Burdensome: Big Ships, Big Cargoes," and are scheduled to appear in a new exhibition at the [[Portland Public Library]] in August 2013.

In 1993, the Somerville-based Home First organization recognized Campbell at its "Somerville Pride Night," the first artist to be granted the distinction.<ref>"Somerville painter first 'Home First' honoree," ''The Somerville Journal'', Oct. 1993, p. 1.</ref>  Indeed, the prominence of [[Somerville, Massachusetts|Somerville]] throughout Campbell's paintings has led art critic Nancy Curtis to comment that "David Campbell has done for Somerville what [[Vermeer]] did for [[Delft]]," a reference to the 17th century [[Netherlands|Dutch]] painter whose work immortalized the middle-class lifestyle of his city.<ref>Nancy Curtis, "Six From Around Town," ''Boston Observer'', August 1983, p. 4.</ref>

Beginning in 1982, Campbell taught at various art institutes and universities around [[New England]].  His teaching experience includes [[The Massachusetts College of Art]] (1982), [[Montserrat College of Art]] (1989), [[New England School of Art and Design]] (1991), [[Museum of Fine Arts, Boston|Museum of Fine Arts]] (1990-2001), and the [[Maine College of Art]] (2002–present).<ref name="greenhutgalleries" />

== Exhibitions ==
Campbell's work has been featured in numerous solo and group exhibitions around the [[United States]] and internationally.  His solo exhibitions include appearances at the Galleria III in [[Florence, Italy]], [[Boston City Hall]], and [[Babson College]].

In group exhibitions, his artwork has been displayed at the [[Smithsonian Institution]] (1959),  [[Pennsylvania Academy of Fine Arts]] (1982), [[San Francisco Museum of Modern Art]] (1986), [[Boston Center for the Arts]] (1987, 1988, 1991), [[National Academy of Design]] (1994, 1996, 2006), [[Art Institute of Chicago]] (1999), [[Maine Maritime Museum]] (2010), [[New Hampshire Institute of Art]] (2007), as well as numerous universities.<ref name="greenhutgalleries" />

Works by Campbell reside in the public collections of the [[Metropolitan Museum of Art]] in [[New York City]], the [[Art Institute of Chicago]], [[Boston Public Library]], [[Maine Maritime Museum]], [[Portland Public Library]], and the [[Boston Museum of Fine Art|Museum of Fine Art]] in [[Boston, Massachusetts|Boston]].<ref>http://www.meca.edu/uploads/visual_edit/cs2013-1.pdf</ref>

His work as appeared alongside fellow realists [[Philip Pearlstein]], [[John Moore (painter)|John Moore]], [[Rackstraw Downes]], [[Richard Maury (painter)|Richard Maury]].

== Publications ==
[[Poetry]] by Campbell has appeared in the ''[[Beloit Poetry Journal]]'', ''Dan River Anthology'', "Goose River Anthology," ''Sensations Magazine'', and ''Struggle Magazine''.  "Looking at Trees," an [[essay]] by the artist, appeared in the November, 1982 issue of [[The Sun (magazine)|The Sun]].<ref name="greenhutgalleries" />

== Notes and sources ==
{{Reflist|32em}}

{{DEFAULTSORT:Campbell, David}}
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:21st-century American painters]]
[[Category:Realist painters]]
[[Category:1936 births]]
[[Category:Living people]]
[[Category:Painters from Maryland]]
[[Category:Writers from Somerville, Massachusetts]]
[[Category:People from Takoma Park, Maryland]]
[[Category:Poets from Massachusetts]]