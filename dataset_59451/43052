{{Distinguish2|the [[Kawasaki C2]] motorcycle}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = C-2
  |image = File:JASDF XC-2(18-1202) at Gifu Air Base October 25, 2015 b.JPG
  |caption = The 2nd prototype C-2 in 2015
}}{{Infobox Aircraft Type
  |type = [[Military transport aircraft]]
  |national origin = [[Japan]]
  |manufacturer = [[Kawasaki Aerospace Company]]
  |first flight = 26 January 2010
  |introduced = 30 June 2016<ref name = "2016 intro">{{cite press release|url=http://global.kawasaki.com/en/corp/newsroom/news/detail/?f=20160630_8823|title=Kawasaki Delivers First Mass-produced C-2 Transport Aircraft to JASDF|publisher=Kawasaki Heavy Industries|date=30 June 2016|accessdate=30 June 2016}}</ref>
  |retired =
  |status = In service
  |primary user = [[Japan Air Self-Defense Force]]
  |more users =
  |produced =
  |number built = 5 (40+ planned to be built <ref name="平成２１年度ライフサイクルコスト管理年次報告書.">[http://image02w.seesaawiki.jp/l/n/live_doraemon/cdc38a6728c32e73.pdf "平成２１年度ライフサイクルコスト管理年次報告書."] ''Equipment Procurement and Construction Office''.</ref>)
  |unit cost = approx. 14 billion yen / 136 million US dollars
  |variants with their own articles=
}}
|}

The '''Kawasaki C-2''' (previously '''XC-2''' and '''C-X''') is a mid-size, twin-[[Turbofan|turbofan engine]], long range, high speed [[military transport aircraft]] developed and manufactured by [[Kawasaki Aerospace Company]]. In June 2016, the C-2 formally entered service with the [[Japan Air Self-Defense Force]] (JASDF).<ref name = "2016 intro"/><ref>[http://www.mod.go.jp/j/news/2010/01/25d.html "次期輸送機試作１号機の初飛行について."] ''[[Ministry of Defense (Japan)|Ministry of Defense]]'', 25 January 2010. {{dead link|date=December 2016}}</ref><ref name="unveils">{{cite news|url=http://www.japantimes.co.jp/news/2016/03/15/national/air-self-defense-force-unveils-c-2-next-generation-transport-aircraft/|title=Air Self-Defense Force unveils C-2 next-generation transport aircraft|publisher=[[Japan Times]]|date=15 March 2016|accessdate=9 December 2016}}</ref>

==Development==
After researching foreign aircraft such as the [[C-130J Super Hercules]], [[C-17 Globemaster III]], and [[Airbus A400M]], the [[Ministry of Defense (Japan)|Japanese Ministry of Defense]] concluded that there was no aircraft in production that possessed the capabilities that the JASDF required. In response to this need, the Japanese MOD commenced the production of plans to develop an indigenously designed and manufactured transport aircraft. In 1995, Kawasaki appealed to the [[Japanese Defense Agency]] (JDA) to issue funding for the development of a domestically-built C-X transport aircraft.<ref>Lewis, Paul. [https://www.flightglobal.com/news/articles/kawasaki-presses-jda-for-transport-launch-cash-22442/ "Kawasaki presses JDA for transport-launch cash."] ''Flight International'', 18 January 1995.</ref> In 2000, the JDA began forming its requirement for the replacement military airlifer; early determinations for the proposed project included that it would be powered by [[turbofan]] engines, possess the range to reach [[Hawaii]] from Japan, and carry double the payload of the C-130.<ref>Jeziorski, Andrzej. [https://www.flightglobal.com/news/articles/japanese-fine-tune-military-transport-requirement-63731/ "Japanese fine tune military transport requirement."] ''Flight International'', 28 March 2000.</ref>

In May 2001, the MOD formally issued a [[request for proposals]] in regards to the new transport aircraft, referred to as the C-X program; at the time, the MOD planned to procure 40 aircraft to replace its aging [[Kawasaki C-1]] and [[C-130 Hercules]] fleets.<ref name="平成２１年度ライフサイクルコスト管理年次報告書.">[http://image02w.seesaawiki.jp/l/n/live_doraemon/cdc38a6728c32e73.pdf "平成２１年度ライフサイクルコスト管理年次報告書."] ''Equipment Procurement and Construction Office''.</ref><ref>Doyle, Andrew. [https://www.flightglobal.com/news/articles/japans-hopes-for-cxmpx-commonality-fade-132424/ "Japan's hopes for CX/MPX commonality fade."] ''Flight International'', 26 June 2001.</ref> In December 2001, it was announced that [[Kawasaki Aerospace Company]], the aerospace division of [[Kawasaki Heavy Industries]], had been selected by the JDA as the prime contractor to develop the C-X.<ref name = "skyhigh">[http://www.khi.co.jp/scope/pdf_e/scope73.pdf "Sky-High Expectations for Japan’s P-X and C-X Aircraft."] ''Kawasaki Heavy Industries Scope Quarterly Newsletter'', No.73. October 2007.</ref><ref>[https://www.flightglobal.com/news/articles/kawasaki-to-lead-the-way-in-c-xp-x-development-139737/ "Kawasaki to lead the way in C-X/P-X development ."] ''Flight International'', 4 December 2001.</ref>

Kawasaki developed the C-X in parallel with the ''[[Kawasaki P-1|P-X]]'', which it had also been selected to develop. As a cost saving measure, major airframe parts and system components were shared between the two aircraft.<ref>Jeziorski, Andrzej. [https://www.flightglobal.com/news/articles/japan-unveils-plan-to-develop-transport-and-maritime-123266/ "Japan unveils plan to develop transport and maritime aircraft."] ''Flight International'', 28 November 2000.</ref> They use the same basic wing structure, although it is installed at different [[wing sweep|sweep angle]] and dihedral on the two versions, with different high lift devices and powerplant attachments. Common components include the cockpit windows, outer wings, horizontal stabilizer, and other systems. Internal shared parts include the [[auxiliary power unit]], cockpit panel, flight control system computer, anti-collision lights, and gear control unit.<ref name = "skyhigh"/> As of 2007, the total development cost for the two aircraft has been 345 billion [[yen]] (or roughly equal to $2.9 billion), which is low compared to similar programs. As an example: the development contract for the [[Boeing P-8 Poseidon]] alone was $3.89 billion.<ref>[http://www.aviationweek.com/aw/blogs/defense/index.jsp?plckController=Blog&plckBlogPage=BlogViewPost&plckPostId=Blog%3a27ec4a53-dcc8-42d0-bd3a-01329aef79a7Post%3a49835293-e38b-4ebb-9a0b-c463eddc7b67&plckScript=blogScript&plckElementId=blogDest] ''Aviation Week Ares Blog'', 27 January 2010.</ref><ref name="boeing_20040514">"[http://www.boeing.com/news/releases/2004/q2/nr_040614n.html "Boeing Team Wins $3.89 Billion Multi-Mission Maritime Aircraft Program."] ''[[Boeing]]'', 14 May 2004.</ref>

Several powerplants were considered for the C-X, including the [[Rolls-Royce Trent|Rolls-Royce Trent 800]], the [[General Electric CF6|General Electric CF6-80C2L1F]] and the [[Pratt & Whitney PW4000]].<ref>Doyle, Andrew. [https://www.flightglobal.com/news/articles/jda-studies-turbofan-for-c-x-programme-159676/ "JDA studies turbofan for C-X programme."] ''Flight International'', 31 December 2002.</ref><ref>Lewis, Paul. [https://www.flightglobal.com/news/articles/ge-eyes-c-5m-type-engine-for-c-x-169091/ "GE eyes C-5M-type engine for C-X."] ''Flight International'', 22 July 2013.</ref> In May 2003, Ishikawajima-Harima Heavy Industries (IHI) announced its support of General Electric's CF6-80C2L1F engine, having formed an arrangement to locally manufacture the powerplant.<ref>Sobie, Brendan. [https://www.flightglobal.com/news/articles/ihi-backs-ges-cf6-engine-proposal-165985/ "IHI backs GE's CF6 engine proposal."] ''Flight International'', 27 May 2003.</ref> That same year, the CF6-80C2L1F was selected to power the type. In August 2003, it was announced that the C-X project had passed a preliminary JDA design review, allowing for prototype manufacturing to proceed.<ref>Sobie, Brendan. [https://www.flightglobal.com/news/articles/c-x-p-x-projects-enter-production-170306/ "C-X, P-X projects enter production."] ''Flight International'', 19 August 2003.</ref>

During the construction of the first prototype, there was a problem discovered with some American-made rivets which delayed the roll-out to 4 July 2007 along with its cousin aircraft [[Kawasaki P-1|P-X]] (since designated as the P-1).<ref>[https://www.flightglobal.com/news/articles/production-fault-hits-kawasaki-transport-and-mpa-pro-212042/ "Production fault hits Kawasaki transport and MPA projects."] ''Flight International'', 13 February 2007.</ref> During structural testing, deformation of the XP-1 / XC-2 horizontal stabiliser was found, as well as cracking in the XC-2 undercarriage trunnion structure and parts of the fuselage; the cracking problem was reportedly difficult to address.<ref name = "kawa civ"/> The C-X program was embroiled in further controversy when allegations that [[bribe]]ry had been involved in the purchase of five [[General Electric CF6|General Electric CF6-80C2]] engines, used to power the aircraft, were made by prosecutors.<ref name = "kawa civ"/>

In 2008, according to the ''[[Chunichi Shimbun]]'', the C-2 was set to cost about 10 billion yen per aircraft (about US$80 million).<ref>[http://www.chunichi.co.jp/hold/2008/ntok0011/list/200711/CK2007111002063769.html "中日新聞:＜蜜月の終焉＞　ミライズに固執:防衛利権　蜜月の構図."], ''[[Chunichi Shimbun]]'', 10 November 2007.</ref> Postponement of the F-X program and the need to increase funding of the [[Mitsubishi F-15J|F-15J]] fleet modernisation program have necessitated the implementation of a one-year delay in the C-X program.<ref>[http://www.mod.go.jp/j/news/2010/01/25d.html] ''Japanese Ministry of Defence'', 2010.</ref><ref>[http://www.mod.go.jp/j/news/2010/01/26a.html] ''Japanese Ministry of Defence'', 2010.</ref> In 2014, the aircraft was delayed again following the failure of the rear cargo door during pressure tests. The delay will increase the program cost by 40 billion yen ($390 million) to 260 billion yen in addition to delays to the program.<ref>{{cite news |last1=Kubo |first1=Nobuhiro |title=Japan again delays C-2 cargo plane, could hamper overseas push |url=http://in.reuters.com/article/2014/07/04/japan-defense-delay-idINL4N0PF1UN20140704|accessdate=7 July 2014 |work=Reuters |issue=Online |publisher=Reuters |date=4 July 2014}}</ref><ref name = "E&T 2014"/> In March 2016, it was reported that the C-2 program faced delays of five years from its initial schedule due to technical problems, while development costs were then estimated to hit ¥264.3 billion, ¥80 billion more than initially projected.<ref name = "times 2016"/>

Kawasaki has also studied the development of a civil-orientated version of the C-2 with ambitions to sell the type to commercial operators. This variant, tentatively designated as the YC-X, little modification is envisioned from the C-2, making use of transferrable technologies from the military airlifter, although the intended payload is likely to be increased from the C-2's 26-ton maximum to 37-tons.<ref name = "kawa civ">Govindasamy, Siva. [https://www.flightglobal.com/news/articles/kawasaki-talks-civil-316324/ "Kawasaki talks civil."] ''Flight International'', 23 September 2008.</ref> In 2007, it was stated that the development of a civilian derivative of the C-2 has been given a lower priority than the completion of the military airlifter.<ref>Francis, Leithen. [https://www.flightglobal.com/news/articles/japan-to-make-commercial-cargo-aircraft-215332/ "Japan to make commercial cargo aircraft."] ''Flight International'', 4 July 2007.</ref> In late 2012, Kawasaki was in the process of consulting potential customers on the topic of the YC-X for transporting [[outsize cargo]]; based upon customer feedback, Kawasaki forecast an estimated demand for up to 100 freighters capable of handling bulky cargoes between 2020 and 2030.<ref>Waldron, Greg. [https://www.flightglobal.com/news/articles/kawasaki-seriously-exploring-commercial-potential-of-377428/ "Kawasaki seriously exploring commercial potential of C-2 airlifter."] ''Flight International'', 9 October 2012.</ref>

On 27 March 2017, Japanese Ministry of Defense announced that development of C-2 was terminated.<ref>ATLA [http://www.mod.go.jp/atla/pinup/pinup290327.pdf "次期輸送機（ＸＣ－２）の開発完了について"] ''ATLA', 29 March 2017.</ref>

==Design==
[[File:Kawasaki XC-2.jpg|thumb|The first prototype C-2 in flight during 2014]]
The Kawasaki C-2 is a long range twin-engine transport aircraft. In comparison with the older C-1 that it replaces, the C-2 can carry payloads up to four times heavier, such as [[MIM-104 Patriot]] [[surface-to-air missile]] (SAM) batteries and [[Mitsubishi H-60]] helicopters, and possesses six times the range.<ref name = "E&T 2014">Pultarova, Tereza. [http://eandt.theiet.org/news/2014/jul/kawasaki-c-2-delay.cfm "Japan's new military cargo plane needs re-engineering."] ''Engineering & Technology Magazine'', 4 July 2014.</ref><ref name = "times 2016">[http://www.japantimes.co.jp/news/2016/03/15/national/air-self-defense-force-unveils-c-2-next-generation-transport-aircraft/#.V3gujm9TGUk "Air Self-Defense Force unveils C-2 next-generation transport aircraft."] ''The Japan Times'', 15 March 2016.</ref>

The C-2 is being developed to meet the following requirements of the Ministry of Defense: a minimum payload of 26 tonnes, 120 [[metric ton]] (264,552&nbsp;lb 132.275 [[short ton]]) take-off weight, ability to takeoff/land on short runways, (Requirement: 500m, almost same as C-1,<ref name="requirementforstol">"Requirements for C-X" ''Japanese Defense Agency'', 2001.</ref> e.g. [[Tachikawa Airfield|Tachikawa]]—900 m, [[Kamigoto Airport|Kamigoto]]—800 m, [[Hateruma Airport|Hateruma]]—800 m), a maximum payload of 37,600&nbsp;kg whilst taking off from a 2,300 m Take-off Field Length at a 141 tonnes (310,851&nbsp;lb 155.42 short ton) take-off weight, ability to fly international airroutes (Requirement: Mach 0.8+; JDA ruled out C-17 as a candidate by its lower cruising speed.<ref>[http://www.mod.go.jp/j/approach/hyouka/seisaku/results/13/chukan/youshi/16.pdf "次期輸送機（次期固定翼哨戒機及び次期輸送機（その２） 政策評価書（要旨） (Policy assessments for coming transporter aircraft)"] ''Japanese Defense Agency'', 2001.</ref> Conventional cargo aircraft cannot cruise at optimum altitude on commercial airroutes because of their lower cruising speed and are often assigned to lower altitude by [[Air traffic control|ATC]].<ref>[http://www.mod.go.jp/j/approach/agenda/meeting/kaihatsukokuki/sonota/pdf/01/005-2.pdf "防衛省大型機の民間転用構想について (Research for commercializing XC-2 Cargo Aircraft)"] ''Kawasaki Heavy Industries'', 23 April 2010.</ref>), in-flight [[aerial refuelling]] and [[forward looking infrared]] systems.

The C-2 is powered by a pair of [[General Electric CF6|General Electric CF6-80C2K]] [[turbofan]] engines.<ref name="flight test underway"/> While sharing fuselage components with the Kawasaki P-1, the fuselage of the C-2's is substantially larger to accommodate a vast internal cargo deck, which is furnished with an automated loading/unloading system to reduce workloads on personnel and ground equipment.<ref name = "skyhigh"/> The forward fuselage and [[horizontal stabilizer]] is made of an internally-designed [[composite material]], KMS6115. A tactical [[flight management system]] and [[head-up display]] is installed to reduce the challenges involved in flying at low altitudes or close to mountainous terrain.<ref name = "skyhigh"/> The C-2 is equipped with a full [[glass cockpit]], [[fly-by-wire]] flight controls, a high-precision navigation system, and [[countermeasure|self protection]] systems.<ref>hikita, Atsushi. [http://www.stimson.org/images/uploads/research-pdfs/The_Meaning_of_JASDFs_Airlift_Operation_in_Iraq_Hikita.ppt "The meaning of  JASDF’s airlift operation in Iraq and JASDF’s future challenge."] ''Japan Air Self Defense Force'', Retrieved: 3 July 2016.</ref>

==Operational history==
{{expand section|date=July 2016}}
[[File:XC-2（18-1202） 正面.JPG|thumb|Head-on view of a XC-2, 2012]]
On 26 January 2010, the [[maiden flight]] of the XC-2 took place from [[Gifu Air Field]], [[Chūbu region]], Japan; this first flight was reportedly carried out without any problems occurring. Prior to the first flight, the aircraft was re-designated as the ''C-2''.<ref>[http://www.khi.co.jp/english/news/detail/ba_c3070704-1.html "P-X and C-X Test Aircraft Rolled Out."] ''Kawasaki Heavy Industries'', 4 July 2007.</ref><ref>[http://www.khi.co.jp/english/news/detail/ba_c3100126_2.html "XC-2 Test Aircraft for Japanese Defense Ministry Completes First Flight."] ''Kawasaki Heavy Industries'', 26 January 2010.</ref> On 30 March 2010, the first aircraft was delivered to the Japanese Ministry of Defence.<ref>[http://www.khi.co.jp/english/news/detail/ba_c3100330_1.html "XC-2 Test Aircraft Delivered to Japanese Defense Ministry."] ''Kawasaki Heavy Industries''.</ref>

On 24 February 2016, 1st prototype "08-1201" was redelivered from reinforced body exchange programme, and 2nd prototype "18-1202" is undergoing the programme from May.<ref>[http://flyteam.jp/photo/1887553] ''flyteam'', 24 May 2016</ref>

In March 2016, the JASDF announced that the C-2 airlifter had formally entered operational service.<ref name="unveils"/>

On 30 June 2016, 1st production model "68-1203" was delivered to Air Development and Test Wing at Gifu Air Field.<ref name="2016 intro"/>
[[File:C-2 (08-1201,68-1203).jpg|thumb|1st production model "68-1203" and 1st prototype "08-1201", 2016]]

On 20 October 2016, the maiden flight of 2nd production model "68-1204" was held.<ref>[http://flyteam.jp/photo/2022511] ''flyteam'', 20 October 2016</ref>

On 28 March 2017, first 3 C-2s were dispatched to 403rd Tactical Airlift Squadron in [[Miho%E2%80%93Yonago_Airport|Miho Air Base]].<ref>The Yomiuri Shimbun [http://www.yomiuri.co.jp/national/20170328-OYT1T50098.html "搭載量４倍に、空自次期輸送機「Ｃ２」が初配備"] ''The Yomiuri Shimbun', 29 March 2017.</ref>

==Overseas sales==
Kawasaki has been reportedly keen to seek sales of the C-2 to overseas customers in addition to the domestic market.<ref>Francis, Leithen. [http://aviationweek.com/defense/japan-eager-generate-military-exports "Japan Eager To Generate Military Exports."] ''Aviation Week'', 5 June 2015.</ref> In the [[strategic airlift]] role, the C-2 is one of only a few aircraft in production that can perform its role; others include the [[Airbus A400M Atlas]] and the [[Ilyushin Il-76]], and thus has few competitors on the world market.<ref>Aboulafia, Richard. [http://aviationweek.com/defense/opinion-after-c-17-tier-choices?NL=AW-05&Issue=AW-05_20151005_AW-05_878&sfvc4enews=42&cl=article_1&utm_rid=CPEN1000001198820&utm_campaign=3983&utm_medium=email&elq2=dae4d21643eb44569a542d6102ddd57c "Opinion: After the C-17, A Tier Of Choices."] ''Aviation Week & Space Technology'', 5 October 2015.</ref><ref>O'Doherty, John. [http://www.ft.com/cms/s/0/6e37fe78-f3f8-11e0-b221-00144feab49a.html#axzz4DISv7qmb "Military Transport: US Prepares to Lose its Lead on the Heavy Airlifters."] ''Financial Times'', 11 November 2011.</ref> In response to plans by the Japanese government to lower historic [[Japanese Arms Export Ban|defense export restrictions]], Kawasaki began actively promoting the C-2 to foreign customers in 2015.<ref>Waldron, Greg. [https://www.flightglobal.com/news/articles/dubai-kawasaki-talks-up-its-aerospace-portfolio-418772/ "DUBAI: Kawasaki talks up its aerospace portfolio."] ''Flight International'', 8 November 2015.</ref><ref>Bitzinger, Richard A. [http://atimes.com/2016/05/japans-disappointing-entree-into-the-global-arms-market/ "Japan’s disappointing entrée into the global arms market."] ''Asia Times'', 20 May 2016.</ref> New Zealand has expressed interest in the aircraft for the [[Royal New Zealand Air Force]] and also in the [[Kawasaki P-1]] maritime patrol aircraft.<ref>{{cite news|url=http://asia.nikkei.com/Politics-Economy/Policy-Politics/Japan-in-talks-with-New-Zealand-for-defense-aircraft|title=Japan in talks with New Zealand for defense aircraft|publisher=Nikkei Asian Review|date=2017-01-03}}</ref>

==Specifications (C-2)==
[[File:JASDF XC-2(18-1202) Front body at Gifu Air Base 20141123.JPG|thumb|Forward fuselage of a C-2]]
[[File:XC-2%EF%BC%8818-1202%EF%BC%89_%E4%B8%BB%E8%84%9A.JPG|thumb|Main landing gear of a C-2]]
[[File:JASDF XC-2(18-1202) CF6-80C2K1F at Gifu Air Base 20141123.JPG|thumb|CF6-80C2K1F engine of a C-2]]
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=jet
|ref=Japanese Ministry of Defense<ref name="JMoD">[http://www.mod.go.jp/j/yosan/2011/yosan.pdf "我が国の防衛と予算平成２３年度予算の概要."] ''Japanese Ministry of Defense'', Retrieved: 2 July 2016.</ref> Flight International<ref name="flight test underway">Francis, Leithen. [https://www.flightglobal.com/news/articles/japan39s-test-programme-for-maritime-patrol-and-transport-aircraft-under-340035/ "Japan's test programme for maritime patrol and transport aircraft under way."] ''Flight International'', 29 March 2010.</ref> ATLA<ref name="ATLA">[http://www.mod.go.jp/atla/en/research/C-2.html "C-2 Cargo Aircraft"] ''ATLA'', 12 October 2016.</ref> 
|crew=3: 2 pilots, 1 loadmaster
|capacity=<br />
** Field Operation System ''or''
** Truck Crane ''or''
** 8 [[463L master pallet|463L Pallets]] ''or''
** 1 [[Mitsubishi H-60|UH-60JA]] helicopter or
** 1 [[Maneuver Combat Vehicle]] wheeled tank destroyer
|payload main=32 t w/ 2.5G limit., 36 t w/ 2.25G limit.
|payload alt=70,480 lb w/ 2.5G limit., 79,300 lb w/ 2.25G limit.
|payload more =<ref name="ATLA" />
|max payload main=37.6 t
|max payload alt=84,000 lb
|length main=43.9 m
|length alt=144 ft
|span main=44.4 m
|span alt=145 ft 8 in
|height main=14.2 m
|height alt=46 ft 7 in
|area main=
|area alt=
|airfoil=
|empty weight main=60,800 kg 
|empty weight alt=133,920 lb
|loaded weight main=
|loaded weight alt=
|useful load main= 
|useful load alt= 
|normal takeoff weight main=
|max takeoff weight main=141,400 kg 
|max takeoff weight alt=311,734 lb 
|more general=
|engine (jet)=GE
|type of jet=[[General Electric CF6|CF6-80C2K1F]]<ref>[http://rgl.faa.gov/Regulatory_and_Guidance_library/rgMakeModel.nsf/0/706579a7e83efab48625727b00751aff/$FILE/E13NE.pdf "Type Certificate Data Sheet: E13NE."] ''[[Federal Aviation Administration]]'', 6 February 2007.</ref>
|number of jets=2
|thrust main=59,740 lbf 
|thrust alt=266 kN
|thrust original=
|afterburning thrust main=
|afterburning thrust alt= 
|engine (prop)=
|type of prop=
|number of props=
|power main= 
|power alt=
|power original=
|max speed main= Mach 0.82
|max speed alt= 570 mph, 917 km/h
|max speed more=<ref name="ATLA" />
|cruise speed main= Mach 0.8
|cruise speed alt= 550 mph, 890 km/h
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 7,600 km w/ 20t, 5,700 km w/ 30t, 4,500 km w/ 36t
|range alt= 4,100 nm w/ 20t, 3,070 nm w/ 30t, 2,430 nm w/ 36t
|range more =<ref name="ATLA" />
|ferry range main= 9,800 km 
|ferry range alt= 5,300 nm
|ferry range more =<ref name="ATLA" />
|ceiling main= 40,000 ft
|ceiling alt= 12,200 m
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==See also==
{{External media|topic= |width=20% |align=right |video1=[https://www.youtube.com/watch?v=iL6MS-KZPrA Video of the XC-2's First Flight in 26 January 2010] |video2=[https://www.youtube.com/watch?v=zrUzmoK3Gto Footage of XC-2s on the ground and taking off] |video3=[https://www.youtube.com/watch?v=I_R2TkxPkLw A C-2 Performing an Airdrop Test, 2015]}}
{{aircontent
|related=
* [[Kawasaki P-1]]
* [[Kawasaki YPX]]
|similar aircraft=
* [[Airbus A400M Atlas]]
* [[Antonov An-70]]
* [[Embraer KC-390]]
* [[Lockheed Martin C-130J Super Hercules]]
* [[UAC/HAL Il-214]]
|lists=
* [[List of military aircraft of Japan]]
|see also=
}}

==References==
{{Reflist}}

==External links==
{{commons category|Kawasaki C-2}}
* [http://global.kawasaki.com/en/mobility/air/aircrafts/xc_2.html XC-2 Transport Aircraft – Kawasaki Heavy Industries]
* [http://www.mod.go.jp/trdi/en/programs/air/air.html Department of Air Systems Development]
* [http://www.airliners.net/photo/Japan-Air-Force/Kawasaki-XC-2/1852320 Images of the XC-2 on airliners.net] 
* [http://www.mod.go.jp/atla/en/research/C-2.html C-2 Cargo Aircraft, ATLA]

{{Kawasaki aircraft}}

[[Category:Kawasaki aircraft]]
[[Category:Japanese military transport aircraft 2000–2009]]
[[Category:High-wing aircraft]]
[[Category:T-tail aircraft]]
[[Category:Twinjets]]