'''''Germelshausen''''' is a 1860 story by [[Friedrich Gerstäcker]] concerning a cursed village that sank into the earth long ago and is permitted to appear for only one day every century. The protagonist is a young artist (Arnold) who happens to traverse the locale at the time for the town's appearance. He encounters, and becomes smitten with, a young woman (Gertrud) from Germelshausen. The romantic tale ends with his leaving the vicinity just in time to avoid being entombed with the village and its denizens, but thereby being sundered from his love forever.

The basic concept is an old German motif that appears in works of [[Max Müller|Mueller]], [[Heinrich Heine|Heine]], [[Ludwig Uhland|Uhland]] and others. The curse may affect a town, a castle or even a single house, but the narratable content remains largely unchanged.

Germelshausen is noteworthy for the affecting nature of the story, and because it is widely credited as having inspired the musical ''[[Brigadoon (musical)|Brigadoon]]''. But the author of Brigadoon, Alan Jay Lerner, appears to deny this in his memoirs, ''The Street Where I Live'', writing "Nevertheless, to this day chroniclers of the musical theater invariably state Brigadoon was based on a folk tale and give [[George Jean Nathan|George Nathan]] as their authority."

The general background to this is that the intention was to produce the film based on the original storyline, but in 1947 filming in war-ravaged Germany was impossible.  It was then decided to reset the tale in Scotland, perhaps inspired by the medieval [[Brig o'Doon]] in the [[Robert Burns]] poem [[Tam o' Shanter (poem)|Tam o' Shanter]].  Unfortunately Scotland's inclement weather would have made film production impractical, so it was finally decided to film using studio sets in California.<ref>"Bizarre Scotland" (Page 163) by David Long (ISBN 978-1472117465)</ref>

==References==
{{Reflist}}
*{{cite book|title=Germelshausen|last=Gerstäcker|first=Friedrich|others=Introduction and English notes by Carl Osthaus, M.A.|location=Boston|year=1893|publisher=Carl Schoenhof|url=https://books.google.com/books?hl=en&id=UykQAAAAYAAJ&dq=germelshausen&printsec=frontcover&source=web&ots=go5reU32ZJ&sig=vxN2GcQuYfFPTDL4T84dQoe9l0M&sa=X&oi=book_result&resnum=3&ct=result#PPP5,M1|accessdate=26 August 2008|chapter=Introduction}}
*{{cite web|title=Zoetrope|url=http://www.all-story.com/issues.cgi?action=show_story&story_id=136|accessdate=26 August 2008}}

[[Category:German short stories]]
[[Category:1860 short stories]]
[[Category:Fictional populated places in Germany]]


{{story-stub}}