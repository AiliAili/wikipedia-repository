{{Italic title}}
{{Infobox journal
| title         = Journal of Systems and Software 
| cover         = [[File:Jnl of System Software cover.gif]] 
| editor        = H. van Vliet  
| discipline    = Computing [[Software systems]] 
| formernames   = 
| abbreviation  = J. Syst. Software 
| publisher     = [[Elsevier]] 
| country       = The [[Netherlands]] 
| frequency     = Monthly
| history       = 1979–present 
| openaccess    = 
| license       = 
| impact        = 1.352
| impact-year   = 2014
| website       = http://www.elsevier.com/wps/find/journaldescription.cws_home/505732/description#description 
| link1         = http://www.sciencedirect.com/science/journal/01641212
| link1-name    = Online access
| link2         = 
| link2-name    = 
| RSS1          = http://rss.sciencedirect.com/publication/science/5651 
| atom          = 
| JSTOR         = 
| OCLC          = 4583109 
| LCCN          = 
| CODEN         = 
| ISSN          = 0164-1212 
| eISSN         = 
}}

The '''''Journal of Systems and Software''''' is a [[computer science]] [[scientific journal|journal]] in the area of [[software systems]], established in 1979 and published by [[Elsevier]].

The journal publishes [[Scientific journal#Types of articles|research papers]], [[state-of-the-art]] [[survey article|surveys]], and practical experience [[report]]s. It includes papers covering issues of [[programming methodology]], [[software engineering]], and [[computer hardware|hardware]]/[[software]] [[system]]s. Topics include: "[[software systems]], [[prototyping]] issues, high-level [[specification]] techniques, [[procedural programming|procedural]] and [[functional programming]] techniques, [[data-flow]] concepts, [[multiprocessing]], [[real-time computing|real-time]], [[distributed system|distributed]], [[concurrent system|concurrent]], and [[telecommunications]] systems, [[software metrics]], [[Reliability engineering|reliability]] models for software, [[performance]] issues, and [[management]] concerns."<ref name=home>[http://www.elsevier.com/wps/find/journaldescription.cws_home/505732/description ''Journal of Systems and Software'' Homepage]</ref>

==Impact factor==
According to the 2015 [[Journal Citation Reports]] the ''Journal of Systems and Software'' has an impact factor of 1.352.<ref name=home/>

==Notable articles==
A few of the most notable (downloaded) articles are:<ref name=home/>
* ''A distributed server architecture supporting dynamic resource provisioning for bpm-oriented workflow management systems''. 
*''Per-flow Optimal Service Selection for Web Services Based Processes''. 
*''Requirement-based approach for groupware environments design''.

==References==
{{Reflist}}

==External links==
* {{Official website|www.elsevier.com/wps/find/journaldescription.cws_home/505732/description}}
* [http://www.sciencedirect.com/science/journal/01641212 Online access]

[[Category:Publications established in 1979]]
[[Category:Computer science journals]]
[[Category:Software engineering publications]]
[[Category:Systems engineering]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]