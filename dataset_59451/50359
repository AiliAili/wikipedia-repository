{{Use dmy dates|date=June 2013}}
{{Infobox journal
| title = Acta Radiologica
| formernames = Acta Radiologica: Diagnosis
| cover = [[File:Ard front cover.jpg]]
| discipline = [[Radiology]]
| abbreviation = Acta Radiol.
| editor = Arnulf Skjennald
| publisher = [[Sage Publications]] in association with the Nordic Society of Medical Radiology
| country =
| history = 1960–present
| frequency = 10/year
| impact = 1.603
| impact-year = 2014
| website = http://acr.sagepub.com/
| link1 = http://acr.sagepub.com/content/current
| link1-name = online access
| link2 = http://acr.sagepub.com/content/by/year
| link2-name = Online archive
| CODEN = ACRAE3
| OCLC = 15802741
| ISSN = 0284-1851
| eISSN = 1600-0455
}}
'''''Acta Radiologica''''' is a [[Peer review|peer-reviewed]] [[medical journal]] covering the field of [[radiology]], including [[diagnostic radiology|diagnostic]] and [[interventional radiology]], [[clinical radiology]], experimental investigations in animals, and all other research related to [[medical imaging|imaging]] procedures.<ref>{{Cite web| url=http://www.uk.sagepub.com/journals/Journal202175?siteId=sage-uk&prodTypes=any&q=acta+radiol&fs=1#tabview=aimsAndScopeCode=ard |title=Aims and Scope |accessdate= 7 November 2013 |publisher=Sage Publications |work=Acta Radiologica}}</ref> ''Acta Radiologica'' is published by [[Sage Publications]] in association with the Nordic Society of Medical Radiology, a federation of societies of Medical Radiology in Denmark, Finland, Iceland, Norway and Sweden. The journal is edited by Arnulf Skjennald ([[Ullevål University Hospital]], Oslo, Norway).<ref>{{Cite web| url=http://www.sagepub.com/journals/Journal202175/boardsCode=ard |title=Editorial Board |accessdate= 7 November 2013 |publisher=Sage Publications |work=Acta Radiologica}}</ref>

''Acta Radiologica'' was established in 1921 and was originally published in German; it is now in English.It was founded by  [[Gösta Forssell]], who served as editor until his death in 1950.<ref>{{cite journal |title=Gösta Forssell, M.D. |journal=[[The British Medical Journal]] |volume=1 |issue=4698 |date=20 January 1951 |page=143 |jstor=25359672 }}</ref> According to the ''[[Journal Citation Reports]]'', it has a 2014 [[impact factor]] of 1.603, ranking it 72nd out of 125 journals in the category "[[Radiology]], Nuclear Medicine & Medical Imaging".<ref>Thomson Reuters Journal Citation Reports 2015</ref>

== Article types ==
Examples of published items include:
* Review articles
* Short communications
* Technical and instrumental notes.

== References ==
{{Reflist|colwidth=30em}}

== External links ==
* {{Official website|http://acr.sagepub.com/}}

[[Category:Radiology and medical imaging journals]]
[[Category:Publications established in 1960]]
[[Category:English-language journals]]