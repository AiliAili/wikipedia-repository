{{Infobox Journal
| title 	= Kaogu
| cover 	= 
| editor 	= [[Institute of Archaeology, Chinese Academy of Social Sciences]]
| discipline 	= [[archaeology]]
| language 	= Chinese
| abbreviation 	= 
| publisher 	= Kexue Chubanshe, Wenwu Chubanshe
| country 	= China
| frequency 	= monthly
| former_names  = Kaogu Tongxun 考古通讯
| openaccess 	= 
| license 	= 
| impact 	=  
| impact-year 	= 
| website       = http://www.kaogu.cn/en/Publication/Journals/Kaogu__Archaeology_/
| link1 	= http://oversea.cnki.net/kns55/oldNavi/n_item.aspx?NaviID=48&Flg=local&BaseID=KAGU&NaviLink=Search%3a%E8%80%83%E5%8F%A4-%2fkns55%2foldNavi%2fn_list.aspx%3fNaviID%3d48%26Field%3dcykm%24%25%2522%7b0%7d%2522%26selectIndex%3d0%26Value%3d%25e8%2580%2583%25e5%258f%25a4%7cArchaeology
| link1-name 	= China Academic Journals Database
| ISSN 		= 0453-2899
}}
{{Italic title}}
'''''Kaogu''''' ({{zh|c=考古|l=Archaeology}}) is a [[Peer review|peer-reviewed]] monthly [[academic journal]] of [[Archaeology of China|Chinese archaeology]], published by the [[Institute of Archaeology, Chinese Academy of Social Sciences|Institute of Archaeology]] of the [[Chinese Academy of Social Sciences]].<ref name=kaogu>{{cite web |url=http://www.kaogu.cn/en/About_IA_CASS/Introduction/2013/1025/31855.html |title=Introduction of the Institute of Archaeology, CASS |date=2005-04-28 |publisher=Institute of Archaeology, CASS}}</ref>

==History==
The predecessor to what would become Kaogu was published from 1934-1937, with each issue containing only a couple of articles.<ref name=report>{{cite web|title=《Archaeology》 Publishing report|url=http://oversea.cnki.net/kns55/oldNavi/n_YearStats.aspx?NaviID=48&Flg=local&BaseID=KAGU&NaviLink=Search%3a%E8%80%83%E5%8F%A4-%2fkns55%2foldNavi%2fn_list.aspx%3fNaviID%3d48%26Field%3dcykm%24%25%2522%7b0%7d%2522%26selectIndex%3d0%26Value%3d%25e8%2580%2583%25e5%258f%25a4%7cArchaeology-%2fkns55%2foldNavi%2fn_item.aspx%3fNaviID%3d48%26Flg%3dlocal%26BaseID%3dKAGU|publisher=China Academic Journals Full-text Database|accessdate=31 January 2017}}</ref> The earliest version of the journal was published in 1955,<ref name=CNKI>{{cite web|title=Archaeology|url=http://oversea.cnki.net/kns55/oldNavi/n_item.aspx?NaviID=48&Flg=local&BaseID=KAGU&NaviLink=Search%3a%E8%80%83%E5%8F%A4-%2fkns55%2foldNavi%2fn_list.aspx%3fNaviID%3d48%26Field%3dcykm%24%25%2522%7b0%7d%2522%26selectIndex%3d0%26Value%3d%25e8%2580%2583%25e5%258f%25a4%7cArchaeology|publisher=China Academic Journals Full-text Database|accessdate=31 January 2017}}</ref> however it appeared irregularly until 1959. Regular publication was temporarily suspended between 1966–1971, during the [[Cultural Revolution]].<ref name=report/>

==Content==
The journal publishes summarized descriptions of excavations across China, but more recently research articles have also been included. Following cultural heritage laws, the work of foreigners on China must first be published in [[Chinese language|Chinese]], and so ''Kaogu'' is also the main repository of data on international joint research between Chinese and non-Chinese that intensified in the 1990s. Most articles contain short English summaries.

==References==
{{reflist}}
* Barnes, Gina L. ''China, Korea, and Japan: The Rise of Civilization in East Asia''. Thames and Hudson, London, 1993.

==External links==
* {{official website|http://www.kaogu.cn/}}

[[Category:Archaeology journals]]
[[Category:Archaeology of China]]
[[Category:Chinese-language journals]]
[[Category:Publications established in 1955]]
[[Category:Monthly journals]]


{{china-hist-stub}}
{{archaeology-journal-stub}}