{{Good article}}
{{Use British English|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox University Boat Race
| name= 15th Boat Race
| winner = Cambridge
| margin = 7 and 1/2 lengths
| winning_time= 21 minutes 23 seconds
| overall = 9–6
| umpire =[[Joseph William Chitty]]<br>(Oxford)
| date= {{Start date|1858|3|27|df=y}}
| prevseason= [[The Boat Race 1857|1857]]
| nextseason= [[The Boat Race 1859|1859]]
}}

The '''15th Boat Race''' took place on the [[River Thames]] on 27 March 1858.  Typically held annually, the event is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]]. The 1858 race, disrupted by poor rowing and a collision with a barge, was won by Cambridge who defeated Oxford by seven-and-a-half lengths in a time of 21 minutes 23 seconds.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 8 April 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  Oxford went into this year's race as reigning champions, having defeated Cambridge by eleven lengths in the [[The Boat Race 1857|previous year's race]].  Cambridge however led overall with eight wins to Oxford's six.<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher= The Boat Race Company Limited | title = Boat Race – Results| accessdate = 26 October 2014}}</ref>

Cambridge were coached for the fifth time by [[Thomas Selby Egan]], (who had coxed the Light Blues in the [[The Boat Race 1836|1836]], [[The Boat Race 1839|1839]] and [[The Boat Race 1840|1840 races]]),<ref>MacMichael, pp. 238&ndash;239</ref><ref>Burnell, p. 110</ref> while Oxford's coach was Alfred Shadwell (cox for the Dark Blues in the [[The Boat Race 1842|1842 race]] and coach for the fourth time).<ref>Burnell, pp. 54, 111</ref>  The race was umpired by [[Joseph William Chitty]] who had rowed for Oxford twice in 1849 (in the [[The Boat Race 1849 (March)|March]] and [[The Boat Race 1849 (December)|December races]]) and the [[The Boat Race 1852|1852 race]], while the starter was Edward Searle.<ref name=drink54>Drinkwater, p. 54</ref><ref>Burnell, pp. 49, 97</ref>

==Crews==
The Oxford crew weighed an average of 11&nbsp;[[Stone (unit)|st]] 8.875&nbsp;[[Pound (mass)|lb]] (73.7&nbsp;kg), {{convert|1|lb|kg|1}} per rower more than their opponents. [[Archibald Levin Smith]], Robert Wharton and [[Robert Lewis-Lloyd]] (who was rowing his third Boat Race for the Light Blues)<ref name=burn56>Burnell, p. 56</ref> had featured in Cambridge's 1857 crew.  Oxford's crew included five participants who had competed in the previous race,<ref name=mac248/> including J. T. Thorley, who was making his third appearance in the event.<ref name=burn56/>
[[File:SirArchibaldLevinSmith.jpg|right|upright|thumb|[[Archibald Levin Smith]] rowed at number two for Cambridge.]]
{| class=wikitable
|-
! rowspan="2" |Seat
! colspan="3" |Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
! colspan="3" |Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
|-
| Name
| College
| Weight
| Name
| College
| Weight
|-
| [[Bow (rowing)|Bow]] || H. H. Lubbock || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 11 st 4 lb || R. W. Risley || [[Exeter College, Oxford|Exeter]] || 11 st 8 lb 
|-
| 2 || [[Archibald Levin Smith|A. L. Smith]] || [[Trinity College, Cambridge|1st Trinity]] || 11 st 4 lb || [[John Arkell|J. Arkell]] || [[Pembroke College, Oxford|Pembroke]] || 11 st 3 lb 
|-  
| 3 || W. J. Havart || [[Lady Margaret Boat Club]] || 11 st 4 lb || C. G. Lane || [[Christ Church, Oxford|Christ Church]] || 11 st 10 lb
|-
| 4 || D. Darroch ||  [[Trinity College, Cambridge|1st Trinity]] || 12 st 1 lb || H. Austen || [[Magdalen College, Oxford|Magdalen]] || 12 st 7 lb
|-
| 5 || H. Williams || [[Lady Margaret Boat Club]] || 12 st 4 lb || [[Ernald Lane|E. Lane]] || [[Balliol College, Oxford|Balliol]] || 11 st 10 lb 
|-
| 6 || [[Robert Lewis-Lloyd|R. L. Lloyd]] (P)  || [[Magdalene College, Cambridge|Magdalene]] || 11 st 13 lb || W. H. Wood || [[University College, Oxford|University]] || 12 st 0 lb 
|-
| 7 || A. H. Fairbairn ||  [[Trinity College, Cambridge|2nd Trinity]] || 11 st 12 lb || [[Edmond Warre|E. Warre]] || [[Balliol College, Oxford|Balliol]] || 13 st 2 lb 
|-
| [[Stroke (rowing)|Stroke]] || J. Hall || [[Magdalene College, Cambridge|Magdalene]] || 10 st 7 lb || J. T. Thorley (P) || [[Wadham College, Oxford|Wadham]] || 10 st 3 lb 
|-
| [[Coxswain (rowing)|Cox]] ||  R. Wharton || [[Magdalene College, Cambridge|Magdalene]] || 9 st 2 lb || H. S. Walpole || [[Balliol College, Oxford|Balliol]] || 9 st 5 lb
|-
!colspan="7"|Source:<ref name=mac248>MacMichael, p. 248</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is competed]]
Cambridge won the [[coin flipping|toss]] and elected to start from the Middlesex station, handing the Surrey side of the river to Oxford.<ref>MacMichael, p. 242</ref>  The race commenced at 1pm and almost immediately the Oxford boat club president and [[stroke (rowing)|stroke]] J. T. Thorley "[[catch a crab|caught a crab]]"<ref>{{Cite web | work = The Independent Rowing News| title = Ask Doctor Rowing: British crabs and wooden schoenbrods| first =Andy|last= Anderson| date =15 October 2000 |page = 30 |url = https://books.google.com/books?id=G00EAAAAMBAJ&pg=PA30&lpg=PA30&dq=%22catch+a+crab%22+rowing }}</ref> which "completely brought their eight to standstill."<ref name=drink45>Drinkwater, p. 45</ref>  Cambridge took the lead but were caught following a clash of their port-side oars with a barge.  The boats were level at the Crab Tree pub but here Cambridge began to draw ahead and passed under [[Hammersmith Bridge]] with a length-and-a-half lead.<ref>MacMichael, p. 243</ref>  The Light Blues continued to increase their lead and passed the flag-boat at [[Mortlake]] seven-and-a-half lengths ahead of Oxford in a time of 21 minutes 23 seconds.<ref name=results/> 

It was the fastest time since the [[The Boat Race 1846|1846 race]] (which was held on the ebb tide), and took the overall record in the event to 9&ndash;6 in Cambridge's favour.<ref>{{Cite web | url = http://theboatraces.org/results | title = Results | publisher = The Boat Race Company Limited|accessdate = 29 July 2014}}</ref>  Although it was hoped that a rematch would be conducted at the [[Henley Royal Regatta]], Oxford failed to make up a crew, and Cambridge went on to defeat [[Leander Club]] before winning the [[Grand Challenge Cup]] against [[London Rowing Club]].<ref>Drinkwater, p. 46</ref><ref>Burnell, p. 86</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}
*{{Cite book | url = https://books.google.com/books?id=uJ8CAAAAYAAJ&dq=%22boat%20race%22%20oxford%20cambridge&pg=PA37 | title = The Oxford and Cambridge Boat Races: From A.D. 1829 to 1869| first = William Fisher | last = MacMichael | publisher = Deighton | date = 1870}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1858)}}
[[Category:1858 in sports]]
[[Category:The Boat Race]]
[[Category:March 1858 sports events]]