{{Use dmy dates|date=February 2015}}
{{Use British English|date=February 2015}}
{{Infobox award
| name        = Food Photographer of the Year
| image       =
| imagesize   =150px
| caption     = Awards logo
| current_awards =
| description = Creative excellence in food photography
| presenter   = [http://www.thefoodawardscompany.co.uk/home The Food Awards Company]
| country     = Worldwide
| year        = 2012
| website     = [http://www.FoodPhotographerofTheYear.co.uk FoodPhotographerofTheYear.co.uk]
}}

'''''Food Photographer of the Year''''' is a set of awards presented by The Food Awards Company and title sponsor Pink Lady Apples, given to amateur and professional photographers for excellence in [[food photography]]. The Award supports [[Action Against Hunger]], a humanitarian aid organisation specialising in saving the lives of malnourished children in the world’s poorest countries, and has also supported the [[Great Ormond Street Hospital Children's Charity]].<ref>{{cite web|title=The Telegraph|url=http://www.telegraph.co.uk/foodanddrink/foodanddrinkpicturegalleries/8867706/Pink-Lady-Food-Photographer-of-the-Year-2012.html|publisher=Telegraph Media|accessdate=8 May 2013}}</ref>

== Format ==
The format of the award emphasises a variety of applications of food photography, including [[portraiture photography|portraiture]], editorial, [[advertising]] and personal blogging. 

The award's judging panel has comprised notable figures from the photographic and food industries, including [[Blur (band)|Blur]] bassist [[Alex James (musician)|Alex James]],  television presenter and [[The Observer|Observer]] food critic [[Jay Rayner]], chef [[James Martin (chef)|James Martin]] as well as restaurateurs [[Tom Aikens]], [[Antonio Carluccio]], [[Bill Granger]] and [[Prue Leith]].<ref>{{cite web|title=Metro|url=http://metro.co.uk/2013/04/25/gallery-pink-lady-food-photographer-3667098/|work=Food Photographer of the Year 2013|publisher=Associated Newspapers|quote="Alexandrina Paduretu, an amateur photographer from a remote area in Romania, was named the 2013 Pink Lady Food Photographer of the Year at a prestigious awards event held at the Mall Galleries, in London on Tuesday, 23rd April. The 2013 judging panel was chaired by Jay Rayner (journalist, food critic and regular on The One Show) and also included James Martin, Antonio Carluccio, Tom Aikens, Prue Leith and David Loftus (food photographer to Jamie Oliver). They were particularly struck by the integrity and truthfulness of Alexandrina’s image."|accessdate=8 May 2013}}</ref>
Categories include: ''Food and Its Place'', for images celebrating food's geographical essence; ''Food In the Street''; ''Food for Celebration'' and commemorative categories such as the ''Philip Harben Award for Food in Action'', in memory of [[Philip Harben]], the UK's first TV chef.<ref>{{cite web|title=ArtFeeder|url=http://www.artfeeder.com/blog/2012/may/food_photographer_of_the_year_2012|work=Food Photographer Of The Year 2012|accessdate=8 May 2013|authors=Pili & Bry Garcia-Wilkinson|quote=The award was in memory of Philip Harben, the first TV chef in the UK, who also started out as a photographer. A passionate communicator of the art of cooking, he encouraged post-war Britain to regain its enthusiasm for food and cookery. The category invited images of people cooking anywhere and everywhere, whether at home, in professional kitchens, over a campfire, or in a ship’s galley.}}</ref>
The 2013 competition received over 5500 entries; the website attracted visits from 140 countries.<ref>{{cite web|last=Keeble|first=Andy|title=Guy’s picture pips the competition|url=http://www.northdevongazette.co.uk/news/guy_s_picture_pips_the_competition_1_2170430|work=North Devon Gazette|publisher=Archant Community Media|accessdate=8 May 2013|quote=The international competition, in its second year attracted more than 5,500 images from around the world.}}</ref><ref>{{cite web|title=Metro|url=http://metro.co.uk/2013/04/25/gallery-pink-lady-food-photographer-3667098/|work=Food Photographer of the Year 2013|publisher=Associated Newspapers|accessdate=8 May 2013}}</ref>
The Award was created by The Food Awards Company, a food industry events consultancy.<ref>{{cite web|title=Food Awards Company web site|url=http://www.thefoodawardscompany.co.uk/home|accessdate=16 July 2013}}</ref>

=== Overall Winner 2013 ===
Alexandrina Paduretu - amateur photographer from Romania.

The first prize in 2013 was £5000.<ref>{{cite web|title=The Telegraph|url=http://www.telegraph.co.uk/foodanddrink/foodanddrinkpicturegalleries/9781101/The-Pink-Lady-Food-Photographer-of-the-Year-2013-competition.html|work=Food Photographer of the Year 2013 competition|publisher=Telegraph Media|accessdate=8 May 2013}}</ref>  Individual category winners receive a trophy, camera equipment and other sponsor-related items. Short-listed entrants have their work displayed at The Mall Galleries prior to the award ceremony, in [[London]], [[England]].

== See also ==
*[[List of prizes, medals, and awards]]
*[[Food photography]]
*[[Food blogging]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.food-photographer-of-the-year.co.uk/2013/}}
*[http://vimeo.com/65084421 Head judge Jay Rayner on Klaus Einwanger's entry]
*[http://www.thefoodawardscompany.co.uk/home The Food Awards Company]
*{{dmoz|Arts/Photography|Photography}}

{{Photography}}

[[Category:Food and drink awards]]
[[Category:Photography awards]]