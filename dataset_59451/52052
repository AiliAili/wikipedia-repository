{{Infobox journal
| title = Journal of Research on Adolescence
| cover = [[File:Journal_of_Research_on_Adolescence_cover.gif]]
| editor = Nancy G. Guerra
| discipline = [[Developmental psychology]]
| abbreviation = J. Res. Adolesc.
| publisher = [[Wiley-Blackwell]] on behalf of the [[Society for Research on Adolescence]]
| country =
| frequency = Quarterly
| history = 1991-present
| openaccess =
| impact = 1.989
| impact-year = 2011
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1532-7795
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1532-7795/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1532-7795/issues
| link2-name = Online archive
| CODEN =
| ISSN = 1050-8392
| eISSN = 1532-7795
| LCCN = 91643986
| OCLC = 21653571
}}
'''''Journal of Research on Adolescence''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published quarterly by [[Wiley-Blackwell]] on behalf of the [[Society for Research on Adolescence]]. The [[editor-in-chief]] is Nancy G. Guerra ([[University of Delaware]]). The journal covers research on [[adolescence]] using "both quantitative and qualitative methodologies applied to cognitive, physical, emotional, and social development and behavior".<ref>{{cite web |work=Journal of Research on Adolescence |url=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1532-7795/homepage/ProductInformation.html |title=Overview |publisher=Wiley-Blackwell |accessdate=8 August 2012}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 1.989, ranking it 6th out of 38 journals in the category "Family Studies"<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Family Studies |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-10 |series=[[Web of Science]] |postscript=.}}</ref> and  27th out of 67 journals in the category "Psychology, Developmental".<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Psychology, Developmental |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-10 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1532-7795}}
* [http://www.s-r-a.org/ Society for Research on Adolescence]

[[Category:Adolescence]]
[[Category:Developmental psychology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1991]]
[[Category:Quarterly journals]]
[[Category:Wiley-Blackwell academic journals]]