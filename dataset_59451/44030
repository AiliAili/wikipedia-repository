<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Adour
 |image=RRTurbomecaAdour.JPG
 |caption= An Adour Mk 102 at the [[Royal Air Force Museum Cosford]]
}}{{Infobox Aircraft Engine
 |type= [[Turbofan]]
 |national origin = 
 |manufacturer= [[Rolls-Royce Turbomeca Limited]] 
 |first run={{avyear|1968}}
 |major applications= [[BAE Hawk]] <br> [[McDonnell Douglas T-45 Goshawk]] <br>[[Mitsubishi F-1]] <br>[[SEPECAT Jaguar]]
 |number built = >2,800 
 |program cost = 
 |unit cost = 
 |developed from = 
 |developed into = 
 |variants with their own articles =
}}
|}

The '''Rolls-Royce Turbomeca Adour''' is a two-shaft low bypass [[turbofan]] [[aircraft engine]] developed by [[Rolls-Royce Turbomeca Limited]], a joint subsidiary of [[Rolls-Royce Limited|Rolls-Royce]] (UK) and [[Turbomeca]] (France). The engine is named after the [[Adour]], a river in south western France.<ref>Gunston 1989, p.155.</ref>

== History ==
The Adour is a turbofan engine developed primarily to power the Anglo-French [[SEPECAT Jaguar]] fighter-bomber, achieving its first successful test run in 1968. It is produced in versions with or without [[afterburner|reheat]].

As of July 2009 more than 2,800 Adours have been produced, for over 20 different armed forces with total flying hours reaching 8 million in December 2009.<ref>[http://www.rolls-royce.com/defence/products/combat_jets/adour.jsp Rolls-Royce PLC -Adour product page] Retrieved: 21 July 2009</ref> The U.S. military designation for this engine is the '''F405-RR-401''' (a derivative of the Adour Mk 871), which is currently used to power the fleet of [[Boeing]] / [[BAE Systems]] [[T-45 Goshawk]] trainer jets of the [[United States Navy]].

== Variants ==
; Bench engines
: Ten prototype engines were built for testing by both Rolls-Royce and Turbomeca.<ref name="FI73" />

; Flight development engines
: Development engines for the Jaguar prototypes, 25 built.<ref name="FI73" />

=== Reheated ([[Afterburning]]) ===
[[File:Adour Mk 811 by Augustus.jpg|thumb|Adour Mk 811 displayed at [[HAL Aerospace Museum]]]]
* Adour Mk 101 - First production variant for the Jaguar, 40 built.<ref name="FI73">{{cite journal | title= Development of the Adour | journal= [[Flight International]] | pages= 649–650| url=http://www.flightglobal.com/pdfarchive/view/1973/1973%20-%201098.html| date= 1973-04-26}}</ref>
* Adour Mk 102 - Second production variant with the addition of part-throttle reheat.<ref name="FI73" />
* Adour Mk 104 - Much more powerful version available in early '80s, with higher operating temperature (700° vs 640), capable of about 5,500 kgf dry and 8,000 lb with max A/B (static). While it was only marginal better in military thrust at take off, this engine improved the Jaguar low power-to weight ratio and gave much better performances, with 10% more thrust at take off (with after burner engaged), and up to 27% more thrust in high subsonic cruise, helping Jaguar in their typical flight envelope (low level, high speed attack). 
* Adour Mk 106 - Replacement for the Jaguar's Mk104 engine (developed from the Adour 871) with a reheat section. The [[Royal Air Force|RAF]] refitted its fleet with this engine as part of the GR3 upgrade.<ref>{{cite web| url=http://www.rolls-royce.com/defence_aerospace/products/combat/adour/tech.jsp |title =Adour, power for the Hawk, Goshawk & Jaguar |publisher =Rolls-Royce plc, Dated: 1 April 2006}}</ref> In May 2007, following the retirement of the last 16 Jaguars from [[No. 6 Squadron RAF]], based at [[RAF Coningsby]], the Adour 106 has been phased out of RAF service.<ref>{{cite web| url=http://www.raf.mod.uk/equipment/jaguar.cfm |title =RAF Jaguar GR3/GR3A |publisher =Royal Air Force, Dated: May 2007}}</ref>
* Adour Mk 801 - For [[Mitsubishi F-1]] & [[Mitsubishi T-2|T-2]] ([[Japan Air Self-Defense Force|JASDF]])
* TF40-IHI-801A - Licence-built version of Mk 801 by [[IHI Corporation|Ishikawajima-Harima]] for Mitsubishi F-1 & T-2 (JASDF)
* Adour Mk 804 - Licence-built by HAL for Indian Air Force phase 2 Jaguars
* Adour Mk 811 - Licence-built by HAL for Indian Air Force phase 3 to 6 Jaguars; rated at 8400 lbs of maximum thrust.[http://www.bharat-rakshak.com/IAF/aircraft/specs/560-bae-jaguar.html] BAe-built Jaguars were initially powered with two Adour 804E turbofans.
* Adour Mk 821 - Engine upgrade of Mk804 and Mk811 engines, currently under development, for Indian Air Force Jaguar aircraft.

=== Dry (Non-afterburning) ===
* Adour Mk 151-01 Used by the Royal Air Force training aircraft fleet
* Adour Mk 151-02 - Used by the [[Red Arrows]], 
* Adour Mk 851
* Adour Mk 861
* Adour Mk 871 - Used by Hawk 200<ref name="Article in Air Force Technology">{{cite web |title=Hawk Trainer Aircraft |publisher= Air Force Technology |year= 2009 |url=http://www.airforce-technology.com/projects/hawk/ |accessdate=2014-08-07}}</ref>
* F405-RR-401 - Similar configuration to Mk 871, for US Navy T-45 Goshawk.
* Adour Mk 951 - Designed for the latest versions of the [[BAE Hawk]] and powering the [[BAE Taranis]]  and [[Dassault nEUROn]] [[Unmanned combat air vehicle|UCAV]] technology demonstrators.<ref name="Article in FlugRevue">{{cite web |title=BAE Systems Taranis |publisher= FlugRevue |year= 2007 |url=http://www.flug-revue.rotor.com/frtypen/FRTaran.htm |accessdate=2006-12-09}}</ref> The Adour Mk 951 is a more fundamental redesign than the Adour Mk 106, with improved performance (rated at {{convert|6500|lbf|abbr=on}} thrust) and up to twice the service life of the 871.<ref>{{cite web|title=Rolls-Royce Adour Factsheet|url=http://www.rolls-royce.com/Images/Adour_tcm92-6701.pdf}}</ref>  It features an all-new fan and combustor, revised HP and LP turbines, and introduces Full Authority Digital Engine Control (FADEC).{{Citation needed|date=March 2009}}  The Mk 951 was certified in 2005.
* F405-RR-402 - Upgrade of F405-RR-401, incorporating Mk 951 technology, certified 2008.  Did not enter into service due to funding issues.

===Higher bypass===
* A higher-bypass version built around the core of the Adour and intended as a [[Rolls-Royce Spey|Spey]] replacement was developed by Rolls-Royce in 1967 as the [[Rolls-Royce RB.203 Trent]]

== Applications ==
* [[Aermacchi MB-338]] (not-built)
* [[BAE Hawk]]
* [[BAE Taranis]] (UCAV development aircraft)
* [[Dassault nEUROn]] (UCAV development aircraft)
* [[McDonnell Douglas T-45 Goshawk]]
* [[SEPECAT Jaguar]]

=== Licence-built ===
; [[IHI Corporation|Ishikawajima-Harima]] TF40-IHI-801A
* [[Mitsubishi F-1]]
* [[Mitsubishi T-2]]

== Specifications (Adour Mk 106) ==
{{jetspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=''Rolls-Royce''<ref>[http://www.rolls-royce.com/Images/Adour_tcm92-6701.pdf Rolls-Royce Adour fact sheet] Retrieved: 21 July 2009</ref>
|type=[[Turbofan]]
|length=114 inches (2.90 m)
|diameter=22.3 inches (0.57 m)
|weight=1,784 lb (809 kg)
|compressor=2-stage LP, 5-stage HP
|combustion=
|turbine=1-stage LP, 1-stage HP
|fueltype=
|oilsystem=
|power=
|thrust=6,000 lb (27.0 KN) dry / 8,430 lb (37.5 KN) with reheat
|compression=10.4
|bypass=0.75-0.8
|aircon=
|turbinetemp=
|specfuelcon=
|power/weight=
|fuelcon= dry {{convert|0.81|tsfc}} 
|thrust/weight=4.725:1
}}

==See also==
{{aircontent
<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
* [[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- other related articles that have not already linked: -->
|see also=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
;Notes
{{Reflist}}
;Bibliography
{{Refbegin}}
*{{cite book|last=Bill|first=Gunston|title=World Encyclopedia of Aero Engines|year=1989|publisher=Patrick Stephens Limited|location=Cambridge, United Kingdom|isbn=978-1-85260-163-8}}
{{Refend}}

== External links ==
{{Commons category}}
* [http://www.raf.mod.uk/equipment/jaguar.cfm RAF Jaguar Specs]
* [http://www.globalsecurity.org/military/systems/aircraft/systems/f405.htm F405 Adour engine]

{{Joint development aeroengines}}
{{USAF gas turbine engines}}

[[Category:Low-bypass turbofan engines]]
[[Category:Rolls-Royce aircraft gas turbine engines|Adour]]
[[Category:Turbofan engines 1960–1969]]