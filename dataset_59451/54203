{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Evanston Gardens
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 770
| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41466 |name=Evanston Gardens (State Suburb) |accessdate=26 April 2011 | quick=on}}</ref><br>787 <small>''(2001 Census)''</small><ref name=ABS2001>{{Census 2001 AUS |id =SSC41451 |name=Evanston Gardens (State Suburb) |accessdate=26 April 2011 | quick=on}}</ref>
| est                 = 
| postcode            = 5116<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/evanston+gardens |title=Evanston Gardens, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=26 April 2011}}</ref>
| area                = 
| dist1               = 35
| dir1                = NE
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = Town of Gawler
| stategov            = [[Electoral district of Light|Light]] <small>''(2011)''</small><ref>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=26 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 }}</ref>
| fedgov              = [[Division of Wakefield|Wakefield]] <small>''(2011)''</small><ref>{{cite web|url=http://apps.aec.gov.au/esearch |title=Find my electorate |author= |date=15 April 2011 |work= |publisher=Australian Electoral Commission |accessdate=26 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110430090535/http://apps.aec.gov.au/esearch |archivedate=30 April 2011 |df=dmy }}</ref>
| near-n              = [[Hillier, South Australia|Hillier]]
| near-ne             = [[Evanston, South Australia|Evanston]]
| near-e              = [[Evanston, South Australia|Evanston]], [[Evanston South, South Australia|Evanston South]]
| near-se             = [[Evanston South, South Australia|Evanston South]]
| near-s              = [[Kudla, South Australia|Kudla]], [[Evanston South, South Australia|Evanston South]]
| near-sw             = [[Kudla, South Australia|Kudla]]
| near-w              = [[Hillier, South Australia|Hillier]]
| near-nw             = [[Hillier, South Australia|Hillier]]
| latd = 34.624
| longd = 138.722
| use_lga_map = yes
}}
'''Evanston Gardens''' is an outer northern [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[Town of Gawler]].

==Geography==
The suburb lies astride [[Angle Vale Road, Adelaide|Angle Vale Road]] and is bounded on the east by the [[Sturt Highway]] (Gawler Bypass).<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 770 persons in Evanston Gardens on census night. Of these, 50.6% were male and 49.4% were female.<ref name=ABS2006/>

The majority of residents (78.3%) are of Australian birth, with an additional 8.4% identifying [[England]] as their country of birth.<ref name=ABS2006/>

The age distribution of Evanston Gardens residents is similar to that of the greater Australian population. 62.6% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 37.4% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Community==
Local newspapers include the [[News Review Messenger]] and The Bunyip. Other regional and national newspapers such as [[The Plains Producer]], [[The Advertiser (Adelaide)|The Advertiser]] and [[The Australian]] are also available.<ref>{{cite web |url=http://www.newspapers.com.au/SA/ |title=South Australian Newspapers |author= |date= |work=Newspapers.com.au |publisher=Australia G'day |accessdate=26 April 2011}}</ref>

===Schools===
<!-- Primary and secondary schools only. Tertiary institutions can be added elsewhere. -->
Evanston Gardens Primary School is located on Angle Vale Road.<ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=26 April 2011}}</ref>

==Facilities and attractions==
===Parks===
'''Karbeethon Reserve''' lies on Angle Vale Road. There are parks elsewhere in the suburb, particularly on Hindmarsh Boulevard.<ref name=UBD/>

==Transportation==
===Roads===
Evanston Gardens is serviced by [[Angle Vale Road, Adelaide|Angle Vale Road]], indirectly connecting the suburb to both [[Port Wakefield Road]] and the [[Sturt Highway]].<ref name=UBD/>

===Public transport===
Evanston Gardens is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date=12 January 2011 |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=26 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

====Trains====
The [[Gawler Central railway line|Gawler]] railway line passes through the suburb. The closest station is [[Tambelin railway station|Tambelin]].<ref name=Metro/>

==See also==
*[[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.gawler.sa.gov.au |title=Town of Gawler |author= |date= |work=Official website |publisher=Town of Gawler |accessdate=26 April 2011}}

{{Town of Gawler suburbs}}

[[Category:Suburbs of Adelaide]]