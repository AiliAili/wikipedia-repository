{{Use mdy dates|date=August 2014}}
{{Infobox person
|name          = Joe Humphreys 
|image         = JoeHumphreys.jpg
|image_size    = 
|caption       = 
|birth_name    = Joseph Edward Humphreys
|birth_date    = {{Birth date|1872|10|18}}
|birth_place   = [[New York City, New York]], [[United States]]
|death_date    = {{death date and age|1936|7|10|1872|10|18}}
|death_place   = [[Fair Haven, New Jersey]]
|death_cause   = [[Stroke]]
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[Irish-American]]
|other_names   = Joe the Beaut
|known_for     = Popular fight announcer and boxing official during the early 20th century. 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Boxing announcer
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = 
|spouse        = 
|partner       = 
|children      = George Humphreys
|parents       = 
|relations     = Terry Humphreys, granddaughter 
|signature     = 
}}

'''Joseph "Joe" Edward Humphreys''' (October 17, 1872-July 10, 1936) was an American boxing official and announcer. He was one of the most popular fight announcers from the turn of the 20th century up until the 1930s. In his near 50-year career, Humphreys was estimated to have announced over 20,000 boxing matches and officiated many of the top prize fights of the era as the longtime official ring announcer at the old [[Madison Square Garden]] from 1925 up to his death in 1936.

==Biography==
Joseph Edward Humphreys was born in [[New York City]] on October 17, 1872. He grew up at 54 Oliver Street in the [[Lower East Side]], not far from the birthplace of future [[Governor of New York]] [[Alfred E. Smith]], and who later was a childhood friend. His father died when he was 11 years old and became a [[Newspaper hawker|newsboy]] to help support his family. During his years as a newsboy, he was known as a "lusty-lunged youngster" whose voice led him to obtain a position at the [[New York Produce Exchange]] as a broker's page. To make some extra money, Humphreys also began singing in clubs, smokers and benefits. At age 15, he was a bartender and later became a mascot of the old Nonpareil Athletic Club where he was originally introduced to boxing. It was while performing at Gus Maisch's ''Little Casino'' in 1888, a popular establishment near New Bowery, that he agreed to be a last minute replacement as the night's official ring announcer when the regular man was unable to appear. Humphreys became an instant success and became a full-time announcer.<ref name="Article">"Joe Humphreys, 63, Sports Figure, Dies; Boxing Announcer 43 Years, He Officiated at Famous Championship Fights. Began Work As Newsboy; Boyhood Chum of Ex-Gov. Smith, He Was Singer Before Voice Won Him Chance in Ring". <u>New York Times.</u> 11 July 1936</ref> 

By the early 20th century, Humphreys had become the single most popular boxing announcer in the New York-area. Harry Grayson, boxing writer for the ''[[New York World-Telegram]]'' once wrote, "Joe Humphreys had voice, presence, personality, tact, and razorblade Irish wit". He was particularly known for his distinctive and colorful announcing style, mannerisms and catchphrases, most notably, "the win-ah and new champion!" when announcing a title change. He was also able to silence noisy and unruly crowds by shouting "Quiet please!" following with a "mollifying spread of his arms".<ref name="Article"/>

In August 1910, he and actor [[Raymond Hitchcock (actor)|Raymond Hitchcock]] purchased the original Chinese Theatre in [[Chinatown, Manhattan]] and turned it into the first [[movie theatre]] in the area. The movie house failed to catch on in the [[Chinese-American]] community, partly due to the ongoing [[Tong wars]], and they eventually sold the property to the New York Rescue Society for use as a mission.<ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 286, 289) ISBN 1-56025-275-8</ref>

He also developed both close friendships and friendly rivalries with fellow announcers [[Tim Hurst (boxing announcer)|Tim Hurst]] and [[Charles J. Harvey]], outlasting Harvey when he retired from announcing to become [[secretary]] of the [[New York State Athletic Commission|New York Boxing Commission]] following the passage of the [[Frawley Law]] in 1911 and eventually became a manager. When [[Tex Rickard]] began promoting boxing at the old [[Madison Square Garden]] in 1925, Humphreys was hired as official ring announcer and was present at many of the top prize fights held at the venue including Jack Dempsey's bouts against [[Georges Carpentier]], [[Luis Firpo]] and [[Gene Tunney]]. He was a major fan of Gene Tunney and Jack Dempsey, regarding the latter as "the most devastating fighter ever to step in the ring". He often claimed that Dempsey's bout against Firpo was "the most exciting he had ever witnessed".<ref name="Article"/>

Humphreys was also highly skilled at using current events during his performances. For example, during the [[Jack Sharkey]] vs. [[Jim Maloney]] on May 20, 1927, he asked the crowd of 40,000 at [[Yankee Stadium]] to stand and observe a moment of silence for [[Charles Lindberg]] who had left earlier that day on his transatlantic flight from New York to Paris.<ref>Mosley, Leonard. ''Lindbergh: A Biography''. Courier Dover Publications, 2000. (pg. 104) ISBN 0-486-40964-3</ref>

Humphreys also became involved behind the scenes and was associated with Sam H. Harris in the management of then world bantamweight and featherweight champion [[Terry McGovern (boxer)|Terry McGovern]]. Upon his request, his granddaughter Terry was named in McGovern's honor. In 1929, he publicly stated that he intended to remain an announcer for 50 years. He was never known to take any special care for his voice, save chewing on a [[cough drop]] or when his throat became dry, and was opposed to using [[microphones]] when they were first introduced at Madison Square Garden.<ref name="Article"/>

Humphreys suffered a serious stroke in June, 1933 which sidelined him for only two months. He resumed his role August 17th of that yea at Madison square garden. He made a few remarks to an entusiastic crowd. At the third meeting between [[Barney Ross]] and [[Jimmy McLarnin]] in 1935, he was unable to enter the ring and had to do his introductory announcements from ringside. However, he was so ill that he was unable to attend the championship bout between [[Max Baer (boxer)|Max Baer]] and [[James J. Braddock]] on June 13, 1935 and missed the first major fight in his career.<ref name="Article"/>

He spent a month in the hospital and made his in-ring return at Yankee Stadium, welcomed by the 95,000 fans in attendance, to announce Max Baer-[[Joe Louis]] fight on September 24, 1935. Humphreys continued working at Madison Square Garden during the 1935-36 season, but announced only the main events. Although he lived most of his life in New York City, where he exclusively performed during his career, Humphreys spent his later years in [[Red Bank, New Jersey]] and [[New Rochelle, New York]].<ref name="Article"/>

In late-June 1936, Humphreys fell seriously ill and was confined to his home in [[Fair Haven, New Jersey]] for three weeks. On the morning of July 10, he apparently collapsed from heat and was unable to speak coherently. Falling into a coma at around 2:00 pm, he died an hour and a half later. His son and daughter-in-law, as well as his grand daughter Terry, were at his bedside at the time of his death.<ref name="Article"/>

In 1997, Humphreys was inducted into the [[International Boxing Hall of Fame]] as a "non-participant".

==References==
{{Reflist}}

==Further reading==
*Durso, Joseph. ''Madison Square Garden, 100 Years of History: One Hundred Years of History''. New York: Simon and Schuster, 1979. ISBN 0-671-24425-6

==External links==
*[http://www.ibhof.com/humphrey.htm IBOHF - Joe Humphreys]

{{DEFAULTSORT:Humphreys, Joe}}
[[Category:1872 births]]
[[Category:1936 deaths]]
[[Category:American people of Irish descent]]
[[Category:Boxing managers]]
[[Category:Public address announcers]]
[[Category:People from Manhattan]]
[[Category:People from New Rochelle, New York]]
[[Category:People from Fair Haven, New Jersey]]