{{Infobox scientist
| name                    = Peter Brusilovsky
| residence               = Pittsburgh, PA
| citizenship             = USA
| nationality             = USA
| field                   = [[Information Science]], [[Computer Science]]
| work_institutions       = [[University of Pittsburgh]]
| alma_mater              = 
}}

'''Peter Brusilovsky'''<ref>{{cite web |last = Brusilovsky |first = Peter |title = website |url = http://www.pitt.edu/~peterb/ |accessdate = 2015-02-10 }}</ref> is a professor of [[Information science]]<ref>{{cite web |title = School of Information Sciences Personnel Directory, University of Pittsburgh |url = http://www.ischool.pitt.edu/people/directory.php |accessdate = 2015-02-10 }}</ref>
and [[University of Pittsburgh Intelligent Systems Program|Intelligent Systems]] ([[Artificial intelligence]])<ref>{{cite web |title = Faculty of Intelligent Systems Program, University of Pittsburgh |url = http://isp.pitt.edu/people/faculty |accessdate = 2015-02-10 }}</ref>  at the [[University of Pittsburgh]]. He is known as one of the pioneers of [[Adaptive hypermedia]],<ref name="article1">
{{cite journal |last = Brusilovsky |first = Peter |title = Methods and Techniques of Adaptive Hypermedia |journal = User Modeling and User-Adapted Interaction |year = 1996 |volume = 6 |issue = 2–3 |pages = 87–129 |doi = 10.1007/bf00143964 }}
</ref> Adaptive Web,<ref>{{cite book| author = Peter Brusilovsky|author2=Alfred Kobsa|author3=Wolfgang Nejdl| title = The Adaptive Web: Methods and Strategies of Web Personalization| date = 2007-06-11| publisher = Springer| isbn = 978-3-540-72078-2 }}</ref>
and Web-based [[Adaptive learning]]<ref name="article2">
{{cite journal |last = Brusilovsky |first = Peter |title = Adaptive and Intelligent Web-based Educational Systems |journal = International Journal of Artificial Intelligence in Education |year = 2003 |volume = 13 |issue = 2–4 |pages = 159–172 |url = http://iospress.metapress.com/content/bg3183vb8pxdnrkh/ }}
</ref>
He also published numerous articles in [[user modeling]], [[personalization]], [[educational technology]], [[intelligent tutoring systems]], and [[information access]]. Brusilovsky is ranked as #1 in the world in the area of Computer Education<ref>{{cite web |title = Top authors in Computer Education, Microsoft Academic Search |url = http://academic.research.microsoft.com/RankList?entitytype=2&topDomainID=2&subDomainID=23&last=0&start=1&end=100 |accessdate = 2015-02-10 }}</ref> and #21 in the world in the area of World Wide Web<ref>{{cite web |title = Top authors in World Wide Web, Microsoft Academic Search |url = http://academic.research.microsoft.com/RankList?entitytype=2&topdomainid=2&subdomainid=15&last=0&orderby=6 |accessdate = 2015-02-10 }}</ref> by Microsoft Academic Search. According to Google Scholar, he has over 20,000 citations and [[h-index]] of 64.<ref>{{cite web |title = Brusilovsky Scholar profile |year = 2016 |url = https://scholar.google.com/citations?user=s6RpNfAAAAAJ&hl=en |accessdate = 2017-02-23 }}</ref> Brusilovsky's group has been awarded best paper awards at Adaptive Hypermedia, User Modeling, Hypertext, ICALT, and EC-TEL<ref>{{cite web |title = Brusilovsky and colleagues win best paper at EC-TEL 2014, SIS News |year = 2014 |url = http://www.ischool.pitt.edu/news/09-20-2014.php |accessdate = 2015-02-10 }}</ref> conference series. Among these awards are four prestigious James Chen Best Student paper awards.<ref>{{cite web |title = James Chen Best Student Paper Award Winners, UM.org |year = 2015 |url = http://um.org/awards/james-chen-best-student-paper-awards |accessdate = 2015-02-10 }}</ref>

Brusilovsky studied applied mathematics and computer science at the [[Moscow State University]]. His doctoral advisor was [[Lev Nikolayevich Korolyov]]. He received postdoctoral training at [[University of Sussex]], [[University of Trier]], and [[Carnegie Mellon University]] under the guidance of Ben du Boulay, Gerhard Weber, and [[John Robert Anderson (psychologist)|John Anderson]]. This research was supported by competitive fellowships from [[Royal Society]], [[Alexander von Humboldt Foundation]], and [[James S. McDonnell Foundation]].  Since 2000 he worked as an Assistant Professor, Associate Professor, and finally Full Professor at the [[University of Pittsburgh School of Information Sciences]]. He is also [[Editor-in-Chief]] of [[IEEE Transactions on Learning Technologies]]. Brusilovsky is a recipient of Fulbright-Nokia Distinguished Chair in Information and Communications Technologies.<ref>{{cite web |title = Brusilovsky receives Fulbright honor, SIS News |year = 2013 |url = http://www.ischool.pitt.edu/news/08-01-2013.php |accessdate = 2015-02-10 }}</ref> He also holds a [[honoris causa]] degree from [[Slovak University of Technology in Bratislava]].<ref>{{cite web |title = Brusilovsky awarded degree of Doctor honoris causa, SIS News |year = 2009 |url = http://www.ischool.pitt.edu/sisint/archives/news/y2009/03_27_2009.html |accessdate = 2015-02-10 }}</ref>

== References ==
{{reflist}}

== External links ==
*[http://www.pitt.edu/~peterb/ Brusilovsky's personal page at the University of Pittsburgh]
*[http://www.ischool.pitt.edu/ School of Information Sciences, University of Pittsburgh]
*[http://www.isp.pitt.edu/ Intelligent Systems Program, University of Pittsburgh]
*[http://www.computer.org/web/tlt IEEE Transactions on Learning Technologies]

{{DEFAULTSORT:Brusilovsky, Peter}}
[[Category:Living people]]
[[Category:Moscow State University alumni]]
[[Category:Year of birth missing (living people)]]
[[Category:20th-century births]]
[[Category:University of Pittsburgh faculty]]
[[Category:American computer scientists]]
[[Category:Alexander von Humboldt Fellows]]
[[Category:Fulbright Distinguished Chairs]]