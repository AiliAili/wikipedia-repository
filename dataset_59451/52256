{{For|the medical journal established in 1922 and published by Lippincott Williams & Wilkins|Medicine (Lippincott Williams & Wilkins journal)}}
{{Infobox journal
| title = Medicine
| cover =
| editor = Allister Vale, John Mucklow
| discipline = [[Internal medicine]]
| former_names =
| abbreviation = Medicine (Abingdon)
| publisher = [[Medicine Publishing]]
| country =
| frequency = Monthly
| history = 1972-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.medicinejournal.co.uk
| link1 = http://www.sciencedirect.com/science/journal/13573039
| link1-name = Online archive
| link2 = http://www.elsevier.com/journals/medicine/1357-3039
| link2-name = Journal page at publisher's website
| JSTOR =
| OCLC = 60618547
| LCCN =
| CODEN =
| ISSN = 1357-3039
| eISSN =
}}

'''''Medicine''''' is a continually updated, [[evidence-based]] [[medical journal|medical]] [[review journal]] covering [[internal medicine]] and its specialties. It was established by Simon Campbell-Smith in 1972 and is published by [[Medicine Publishing]]. The [[editor-in-chief]] is Allister Vale ([[City Hospital, Birmingham]]).

== Scope ==
The journal aims to cover the fundamentals of internal medicine in a systematic way during a recurring four-year cycle – it can be seen as a general medicine textbook that is published "a chapter at a time". It covers the topics at a level appropriate to the non-specialist, providing clinicians with up-to-date, understandable clinical information. It is aimed specifically at trainees in internal medicine and its specialties who are preparing for postgraduate examinations. The journal is abstracted and indexed by [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=[http://www.elsevier.com/online-tools/scopus/content-overview Scopus coverage lists] |accessdate=2014-12-14}}</ref> and [[Embase]].

== Editors-in-chief ==
The following persons have been editor-in-chief ("chairman of the board") of the journal:
* Sir John McMichael FRS (1972–1978) 
* Sir John Badenoch (1979–1996)<ref>Ledingham JGG. Obituary:Sir John Badenoch. ''[[Br. Med. J.]]'' 1996;3:906</ref><ref>Ledingham JGG. Obituary:Sir John Badenoch. ''[[The Times]]'' 12 Feb 1996</ref><ref>Ledingham JGG. Obituary:Sir John Badenoch. ''[[The Independent]]'' 27 Jan 1996</ref>
* Alasdair Geddes (1996–2002)
* Allister Vale (2003–present)

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.medicinejournal.co.uk/}}

[[Category:Internal medicine journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1972]]