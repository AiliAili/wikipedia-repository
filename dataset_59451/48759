{{Use Indian English|date=May 2014}}
{{Use dmy dates|date=May 2014}}
{{cleanup rewrite|date=January 2017}}
{{Infobox recurring event
 |name = International kites Festival
 |native_name = Makar Sankranti
 |logo          =
 |image         = MANJA 2667855f.jpg
 |caption       = Manja (the thread with which the kite is flown) Maker in a kite market, 13 January 2007
 |location      = [[Gujarat, India]]
 |years_active  = 1989— present
 |frequency     = Annually
 |dates         = 14 January every year
 |genre         = [[kite]]s
 |website       = [http://www.gujarattourism.com www.gujarattourism.com]
}}

Every year, [[Gujarat]] celebrates more than 200 festivals. The '''International Kite Festival (Uttarayan)''' is  regarded as one of the biggest festivals celebrated.<ref>{{cite web|last=Subhamoy|first=Das|title=Uttarayan & the Kite Festival of Gujarat|url=http://hinduism.about.com/cs/festivals/a/aa011103a.htm|accessdate=3 November 2012}}</ref> Months before the festival, homes in Gujarat begin to manufacture kites for the festival.

The festival of Uttarayan marks the day when winter begins to turn into summer, according to the [[Hindu calendar|Indian calendar]]. It is the sign for farmers that the sun is back and that harvest season is approaching which is called [[Makara Sankranti]]. This day is considered to be one of the most important harvest day in India. Many cities in Gujarat organize kite competition between their citizens where the people all compete with each other. In this region of Gujarat and many other states, Uttarayan is such a huge celebration that it has become a [[Public holidays in India#Hindu holidays|public holiday in India]] for two days.<ref>{{cite web|last=Subhamoy|first=Das|title=Uttarayan & the Kite Festival of Gujarat|url=http://hinduism.about.com/cs/festivals/a/aa011103a.htm|publisher=www.about.com/|accessdate=24 October 2012}}</ref> During the festival, local food such as [[Undhiyu]] (a mixed vegetable including yam and beans), sesame seed brittle and [[Jalebi]] is served to the crowds.<ref>{{cite book|last=Desai|first=Anjali|title=India Guide Gujarat|year=2007|publisher=India Guide Publication|location=India|pages=66|url=https://books.google.com/books?id=gZRLGZNZEoEC&pg=PA66&dq=international+kite+festival+gujarat#v=onepage&q=international%20kite%20festival%20gujarat&f=false|isbn=9780978951702}}</ref><ref>{{cite web|title=International Kite Festival Ahmedabad|url=http://www.khazano.com/aspx/FestivalFair.asp|accessdate=3 November 2012}}</ref> Days before the festival, the market is filled with participants buying their supplies. In 2012, the Tourism Corporation of Gujarat mentioned that the International Kite Festival in Gujarat was attempting to enter the [[Guinness World Records]] book due to the participation of 42 countries in it that year.<ref>{{cite web|last=Sahu|first=Deepika|title=Gujarat kite festival to go global|url=http://articles.timesofindia.indiatimes.com/2012-01-11/travel/30612255_1_international-kite-festival-uttarayan-global-brand|accessdate=3 November 2012}}</ref>

==Location==

The International Kite Festival takes place in [[Gujarat]] [[India]]. The festival is called [http://sheokhanda.wordpress.com/2010/07/16/uttarayan-%E2%80%93-the-international-kite-festival/ Uttarayan]. The festival is celebrated in many cities of Gujarat, Telangana and Rajasthan like Ahmedabad, Jaipur, Udaipur,Jodhpur, [[Surat]], [[Vadodara]], [[Rajkot]], [[Hyderabad]], [[Nadiad]]. However, the International Kite Event takes place in [[Ahmedabad#Festivals|Ahmedabad]] (Kite capital of Gujarat) which accommodates visitors from many international destinations.<ref>{{cite news|title=Ahmedabad sky thrilled with colourful kites, Modi spellbound|url=http://news.oneindia.in/2012/01/12/kite-festival-starts-ahmedabad-narendra-modi-enthralled.html|accessdate=3 November 2012|newspaper=www.oneindia.in|date=12 January 2012|author=Nairita}}</ref><ref>WebIndia. [http://www.webindia123.com/city/andhra/hyderabad/destinations/fairs_festivals/internationalkitefestival.htm "Hyderabad Kites Festival"]. Retrieved 15 January 2016.</ref>

The best place to enjoy this festival is the [[Sabarmati Riverfront]] (its sabarmati river bank with capacity of over 500,000 people) <ref>{{cite web|title=Sabarmati River Front Ahmedabad|url=http://www.ahmedabadkiteflyers.org/international-kite-festival-ahmedabad-gujarat/|accessdate=31 December 2015}}</ref> or the Ahmedabad Police Stadium, where people lay down to see the sky filled with thousands of kites <ref>{{cite web|last=Solanki|first=Paavan|title=International Kite Festival Gujarat 2016|url=http://www.ahmedabadkiteflyers.org/international-kite-festival-ahmedabad-gujarat/|publisher=Royal Kite Flyers Club}}</ref>

During the festival week the markets are flooded with kite buyers and sellers. In the heart of Ahmedabad, there is one of the most famous Kite markets - ''Patang Bazaar'', which during the festive week opens 24 hours a day with buyers and sellers negotiating and buying in bulk.<ref>{{cite web|author=Rob|title=Uttarayan International Kite Festival Gujarat|url=http://www.1000lonelyplaces.com/carnivals/uttarayan-international-kite-festival-gujarat-2011/|accessdate=3 November 2012}}</ref>

Moreover, many families in Ahmadabad start making kites at home and setup small shops in their own homes.<ref>{{cite web|title=Kite Museum|url=http://www.journeymart.com/de/india/gujarat/ahmedabad/kite-museum.aspx|accessdate=4 November 2012}}</ref>

There is also a Kite Museum, which is located at Sanskar Kendra in [[Paldi]] area of Ahmedabad. It was established in 1985, which has a numerous collection of unique kites.<ref>{{cite web|title=About Kite Museum|url=http://www.holidayiq.com/Kite-Museum-Ahmedabad-Sightseeing-256-258.html|accessdate=4 November 2012}}</ref>

However, in Delhi and other parts of India also celebrates Kite festival. In Delhi 15 August and in Bihar's most of the districts 14 April. It is said because they just prepare new crop wheat. People are offer pray, eat Sattu (made from new crop wheat) and new mangoes (baby mango also known as ''Tikola''). People use to worship with  ''Aam ka Pallo''.

==Dates==
The festival takes place on 14 January of each year during the [[Makar Sankranti]] and continues until 15 January. This date marks the end of winter and the return of a more clement weather for farmers of the Gujarat region. These days have also become a public holiday within the Gujarat state of India so that everyone can take part in the celebration.

==History==
<!-- Deleted image removed: [[File:Masjid (Mosque) Uttarayan in Ahmedabad.jpg|thumb|left|200px|Kite flying on the roof of a mosque in Ahmedabad for the Uttarayan celebration, 14 January 2010]] -->

The symbolism of this festival is to show the awakening of the Gods from their deep sleep. Through India's history, it is said that India created the tradition of kite flying due to the kings and Royalties later followed by [[Nawabs]] who found the sport entertaining and as a way to display their skills and power. It began as being a sport for kings, but over time, as the sport became popular, it began to reach the masses. Kite flying has been a regional event in Gujarat for several years. However the first International Festival was celebrated in 1989 when people from all across the globe participated and showcased their innovative kites.<ref>{{cite web|title=International Kite festival|url=http://www.mapsofindia.com/gujarat/fairs-and-festivals/international-kite-festival.html|accessdate=24 October 2012}}</ref><ref>{{cite web|last=Subhamoy|first=Das|title=Uttarayan & the Kite Festival of Gujarat|url=http://hinduism.about.com/cs/festivals/a/aa011103a.htm|publisher=About.com Guide|accessdate=24 October 2012}}</ref>
In the recent 2012 event, The International Kite Festival was inaugurated by Prime Minister [[Narendra Modi]] in the presence of Governor [[Kamla Beniwal|Dr. Kamla]].

==Participants==
[[File:Colored Kites.jpg|thumb|right|200px|Pile of colored kites, prepared for the Uttarayan festival]]

Although the idea of flying kites to celebrate Uttrayan was introduced by Muslims from Persia,{{fact|date=April 2017}} today regardless of your background or beliefs, you are welcome to fly kites with everyone else in Gujarat in January. Most visitors arrive from around India, from Gujarat itself or another state.  In major cities of Gujarat, kite flying starts as early as 5 am and goes until late night where approximately 8-10 million people participate in the whole festival.<ref>{{cite web|last=Aggarwal|first=Priya|title=International Kite festival|url=http://www.easydestination.net/blog/?itemid=2569|publisher=Travel Events|accessdate=24 October 2012}}</ref>

However, many visitors are international who come from around the world, such as Japan, Italy, UK, Canada, Brazil, Indonesia, Australia, the USA, Malaysia, Singapore, France, China, and many more to take part in the celebration.

The kite festival has been strongly influenced by its international participants, in the recent events, for instance:<ref>{{cite web|title=Uttarayan: International Kite Festival in Gujarat|url=http://www.cnngo.com/mumbai/play/uttarayan-international-kite-festival-gujarat-460042|publisher=CNN GO|accessdate=24 October 2012|author=CNN|date=15 January 2008}}</ref>
* Malaysia brought wau-balang kites
* Indonesia brought llayang-llayanghave
* USA brought giant banner kites
* Japan brought [[Rokkaku dako|Rokkaku]] fighting kites
* Italy brought Italian sculptural kites
* Chinese brought Flying Dragon kites
* For other kites, see list of [[Kite types]]

At the same time, the festival is the occasion for many public entities such as famous dancers, singers, actors or politicians who make an appearance and entertain the population. In 2004, for example, the [[Bollywood]] actress [[Juhi Chawla]] was part of the celebration and performed a [[Garba (dance)]] which is very popular in India.<ref>{{cite web|last=Deccan|first=Herald|title=International Kite Festival, Ahmedabad|url=http://www.carnetdevol.org/actualite/inde/india.htm|publisher= www.carnetdevol.org|date= 13 January 2004|accessdate=3 November 2012}}</ref>

==Types of kites==

During the event, kite markets are set up alongside food stalls and performers. The kites are usually made with materials such as plastic, leaves, wood, metal, nylon and other scrap materials but the ones for Uttarayan are made of light-weight paper and bamboo and are mostly rhombus shaped with central spine and a single bow.<ref>{{cite web|title=Kite Basics|url=http://www.drachen.org/learn/kite-basics|publisher=www.drachen.org|accessdate=3 November 2012}}</ref> Dye and paint are also added  to increase the glamour of the kite. The lines are covered with mixtures of glue and ground glass which when dried, rolled up and attached to the rear, also known as firkees, become sharp enough to cut skin.<ref>{{cite news|last=Herald|first=Deccan|title=International Kite Festival Ahmedabad (Gujarat - Inde)|url=http://www.carnetdevol.org/actualite/inde/india.htm|accessdate=24 October 2012}}</ref> These types of sharp lines are used on [[fighter kite]]s known in India as ''patangs'' to cut down other kites during various kite fighting events. During the night, on the second day of the festival, illuminated kites filled with lights and candles known as tukals or tukkals are launched creating a spectacle in the dark sky.<ref>{{cite web|title=FAIRS & FESTIVALS OF AHMEDABAD|url=http://www.indiainfoweb.com/gujarat/ahmedabad/festive-fun.html|accessdate=3 November 2012}}</ref>

==List of Other Kite Festivals==
[[File:Cherry Blossom Kite Festival.jpg|thumb|right|200px|The Blossom Kite Festival also known as Smithsonian Kite Festival on Washington Monument D.C. in the U.S.A., 3 March 2012]]

Kites are a real part of the culture in Asia, which is why most of the kite festivals around the world take place in those areas. Here are  the Most Popular Kites Festival of the World:<ref>{{cite web|title=5 Most Popular Kites Festival of the World|url=http://www.thetravel-guide.com/festivals/5-most-popular-kites-festival-of-the-world|publisher=www.thetravel-guide.com |date=19 August 2010|accessdate=3 November 2012}}</ref>

*Japan Kite Festival in [[Uchinada, Ishikawa]] <ref>{{cite web|last=Malcolm|first=Goodman|title=JAPANESE KITE HISTORY 1|url=http://www.kiteman.co.uk/JAPANESE%20HISTORY4.html|accessdate=3 November 2012}}</ref>
*China Kite Festival called [[Weifang International Kite Festival]] <ref>{{cite web|last=Liu|first=Zhiping|title=Distinctive Features of the 28th Weifang International Kite Festival|url=http://www.weifangkite.com/index_1.htm|publisher=www.weifangkite.com|accessdate=3 November 2012}}</ref>
*Jakarta Kite Festival in [[Pangandaran]] <ref>{{cite web|title=Pangandaran International Kite Festival 2012|url=http://indonesia.travel/en/event/detail/463|publisher=indonesia.travel|date=August 2012 |accessdate=3 November 2012}}</ref>
*Washington D.C. International Kite Festival formerly called Smithsonian Kite Festival, and now known as The [[Blossom Kite Festival]] <ref>{{cite web|title=Blossom Kite Festival |url=http://www.nationalcherryblossomfestival.org/2011/07/15/blossom-kite-festival/ |publisher=www.nationalcherryblossomfestival.org |date=31 March 2012 |accessdate=3 November 2012 |deadurl=yes |archiveurl=http://www.webcitation.org/6DycfiStO?url=http%3A%2F%2Fwww.nationalcherryblossomfestival.org%2F2011%2F07%2F15%2Fblossom-kite-festival%2F |archivedate=27 January 2013 |df=dmy-all }}</ref>
*Indonesia Kite Festival in Bali island, [[Bali Kite Festival]] <ref>{{cite web|title=Bali Kite Festival|url=http://www.dancingfrog.net/Bali_kite_festival/index.html|publisher=www.dancingfrog.net|accessdate=3 November 2012|author=Bob Harris}}</ref>
*United-Kingdom Kite Festival in Bristol city, [[Bristol International Kite Festival]] <ref>{{cite web|title=Bristol International Kite Festival|url=http://www.kite-festival.org.uk/|publisher=www.kite-festival.org.uk|accessdate=3 November 2012|author=Avril Baker}}</ref>

{{Clear}}

==References==
{{reflist|30em}}

{{DEFAULTSORT:International Kite Festival in Gujarat - Uttarayan}}
[[Category:Kite festivals]]
[[Category:Festivals in Gujarat]]
[[Category:January events]]
[[Category:Sports festivals in India]]