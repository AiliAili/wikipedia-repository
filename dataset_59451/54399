{{refimprove|date=April 2016}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
| name     = Maylands
| city     = Adelaide
| state    = sa
| image    = 
| caption  = 
| lga      = City of Norwood Payneham St Peters
| postcode = 5069
| est      = 1876
| pop      = 1506	
| pop_year      = {{censusAU|2011}}
| pop_footnotes = <ref name= Census2011>{{Census 2011 AUS|id= SSC40413|name= Maylands |accessdate=9 April 2016|quick=on}}</ref>
| area     = 0.5
| stategov = [[Electoral district of Dunstan|Dunstan]]
| fedgov   = [[Division of Adelaide|Adelaide]]
| near-nw  = [[Evandale, South Australia|Evandale]]
| near-n   = [[Evandale, South Australia|Evandale]]
| near-ne  = [[Payneham South, South Australia|Payneham South]]
| near-w   = [[Stepney, South Australia|Stepney]]
| near-e   = [[Trinity Gardens, South Australia|Trinity Gardens]]
| near-sw  = [[Norwood, South Australia|Norwood]]
| near-s   = [[Norwood, South Australia|Norwood]]
| near-se  = [[Beulah Park, South Australia|Beulah Park]]
| dist1    = 4
| location1= [[Adelaide]]
}}

'''Maylands''' is a suburb of [[Adelaide]] located within the [[City of Norwood Payneham St Peters]], and bounded by the main roads Portrush Road and Magill Road.

== History ==
Maylands was named by William Wadham (who married Jane Cooper circa 1852 in Adelaide) and Luke M Cullen, early settlers who sold the land that made up the original town in 1876.  There is a Mayland in the English county of Essex from whence the Cooper family emigrated.

Maylands consists of mostly medium density residential properties, predominantly in an east-west elevation with most streets in the suburb oriented north-south.  Many of its houses having been built in the late 19th century and early 20th century, there are South Australian examples of colonial, Federation and some art deco architecture. Unlike [[Stepney, South Australia|Stepney]], to Maylands' west, the dwellings are usually larger with some remaining homes attached to small market gardens. There are few "workers cottages", but most are found in the north-western section of the suburb.

Some heritage preservation has occurred in the suburb. The then Metropolitan Tramways Trust (or MTT) was converted into a set of unit accommodation by the South Australian Housing Trust. The suburb is also known for the antique stores along Magill Road.

== Landmarks ==
One of the most notable landmarks includes the ''Maylands Hotel'', which is located opposite ''Cruickshank Reserve'' on the south eastern corner of the Phillis and Clifton Streets intersection. The hotel remains whereas the Tip-Top Bakery site on the south-western corner of the same intersection was demolished and replaced by townhouses.

The ''Maylands Hotel'' has become a popular eatery, with it arguably being off the beaten track and hidden in leafy suburbia. Up until the 1990s, the hotel had a beer garden. With the introduction of poker machines into South Australia, this beer garden was replaced by an extended dining area and poker machine area.

== Politics ==
Maylands is represented in the [[Electoral district of Dunstan|Dunstan electorate]] in the [[Parliament of South Australia]], and within the [[Division of Adelaide]] in the [[Australian House of Representatives]].

==References==
{{Reflist}}
{{Commons category}}

{{Coord|-34.911|138.637|format=dms|type:city_region:AU-SA|display=title}}
{{City of Norwood Payneham St Peters suburbs}}

[[Category:Suburbs of Adelaide]]


{{adelaide-geo-stub}}