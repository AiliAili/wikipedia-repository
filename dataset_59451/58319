{{italictitle}}
{{Infobox Journal
| cover = [[File:Journal of Modern African Studies.jpg]]
| discipline   = [[African studies]]
| editor =  Christopher Clapham
| website      = http://journals.cambridge.org/action/displayJournal?jid=MOA
| link1        =
| link1-name   =
| publisher    = [[Cambridge University Press]]
| country      = United Kingdom
| impact       = 0.692 (ISI, Area Studies)
| impact-year  = 2014
| abbreviation = 
| history      = 1963-present
| frequency    = Quarterly
| ISSN         = 0022-278X
| eISSN        = 1469-7777
| Jstor        =
| OCLC         = 48535892
| LCCN         = 2001-227388
}}
The '''''Journal of Modern African Studies''''' is a quarterly [[academic journal]] covering developments in modern [[Africa]]n [[politics]] and society. Its main emphasis is on current issues in African politics, economies, societies, and [[international relations]]. It publishes approximately 25 articles and 30 book reviews a year.

The journal is published by [[Cambridge University Press]] and its current editor is Christopher Clapham from the [[University of Cambridge]]. From 1963 to 1997, David Kimble from the [[National University of Lesotho]] served as its editor. 

== Abstracting and indexing ==
According to the 2008 [[Journal Citation Reports]], the journal has an [[impact factor]] of 1.041, ranking it 4th out of 38 in the category "Area Studies". The journal is currently abstracted and indexed in: 
*[[EBSCO Publishing|EBSCOhost]]
*Humanities International Index
*[[International Bibliography of the Social Sciences]]
*[[ProQuest]] 
*[[Scopus]]
*[[Social Sciences Citation Index]]
*Sociological Abstracts

== Notable articles ==
* "Explaining the 1994 genocide in Rwanda"  - Helen M. Hintjens, Jun 1999 [http://journals.cambridge.org/action/displayIssue?jid=MOA&decade=1990&volumeId=37&issueId=02&iid=17524  37:2, pp 241-286]
* "China's engagement in Africa: scope, significance and consequences"  - Denis M. Tull, Sep 2006 [http://journals.cambridge.org/action/displayIssue?jid=MOA&decade=2000&volumeId=44&issueId=03&iid=459845 44:3, pp 459-479]
* "Business environment and entrepreneurial activity in Nigeria: implications for industrial development" - Mary Agboli and Chikwendu Christian Ukaegbu, Mar 2006  [http://journals.cambridge.org/action/displayIssue?jid=MOA&decade=2000&volumeId=44&issueId=01&iid=399915 44:1, pp 1-30]
* "Diamonds or development? A structural assessment of Botswana's forty years of success" - Ellen Hillbom, Jun 2008 [http://journals.cambridge.org/action/displayIssue?jid=MOA&decade=2000&volumeId=46&issueId=02&iid=1874724 46:2, pp 191-214]

[[Category:African studies journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1963]]
[[Category:Cambridge University Press academic journals]]
[[Category:20th century in Africa]]
[[Category:21st century in Africa]]