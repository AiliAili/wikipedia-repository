{{good article}}
{{For|other songs of the same name|Call the Police (disambiguation){{!}}Call the Police}}
{{For|the [[old-time radio]] program Call the Police|Call the Police (radio program)}}
{{Infobox single
| Name = Call the Police 
| Cover = Call The Police (cover).jpeg
| Border = yes
| Alt = The cover of the single sees a thumbnail of each band member rendered into one sepia frame.
| Artist = [[G Girls]]
| Released = 14 June 2016
| Format = [[Music download|Digital download]]
| Genre = [[Eurodance]]
| Length = {{duration|m=3|s=08}}
| Label = {{flat list|
*Global
*[[Roton (label)|Roton]]
*Ego
}}
| Writer = {{flat list|
*[[Eric Turner (singer)|Eric Turner]]
*[[J-Son|Julimar Santos Oliveira Neponunceno]]
}}
| Producer = {{flat list|
*[[Play & Win|Marcel Botezan]]
*[[Play & Win|Sebastian Barac]]
}}
| Last single = 
| This single = "'''Call the Police'''"<br />(2016)
| Next single = "Milk & Honey"<br />(2017) 
{{External music video|{{YouTube|Ww1fR5OeWZo|"Call the Police"}}}}
}}
"'''Call the Police'''" is a song recorded by Romanian girl band [[G Girls]]. It was made available for [[Music download|digital consumption]] on 14 June 2016 through Global Records and [[Roton (label)|Roton]] worldwide, while being distributed in Italy on 15 July 2016 by Ego. Serving as the group's debut recording, the track was written by [[Eric Turner (singer)|Eric Turner]] and [[J-Son]], while production was handled by [[Play & Win|Marcel Botezan and Sebastian Barac]]. Musically, "Call the Police" is a "typical" [[eurodance]] song which musically incorporates "Romanian music dance vibe".<ref name="beebee"/>

[[Music journalism|Music critics]] were positive towards "Call the Police", noting the track as being catchy and comparing it to band member [[Inna]]'s works. For promotion, an accompanying music video for the single was shot by Roman Burlaca at Palatul Noblesse. Commercially, "Call the Police" experienced moderate success on music charts; while it peaked at number six on Poland's [[Polish Society of the Phonographic Industry|Airplay Top 100]], the track reached position 64 in Romania.

==Background and release==
G Girls is a four-part girl band started by label Global Records, consisting of Romanian singers [[Alexandra Stan]], [[Antonia Iacobescu|Antonia]], [[Inna]] and Lori; the latter previously participated at Romanian reality singing competition ''[[Vocea României]]''.<ref name="topromanesc"/> "Call the Police" was written by [[Eric Turner (musician)|Eric Turner]] and [[J-Son|Julimar Santos Oliveira Neponunceno]], while the production process was handled by [[Play & Win|Marcel Botezan]] and [[Play & Win|Sebastian Barac]].<ref name="topromanesc"/> On 14 June 2016, the recording was distributed in multiple regions through Global and [[Roton (label)|Roton]],<ref name="ro"/> while being made available for purchase in Italy on 15 July 2016 by Ego.<ref name="it"/> At the time of the track's premiere, Stan and Antonia were promoting "[[Écoute (Alexandra Stan song)|Écoute]]" and "Sună-mă", respectively, and Inna would release "[[Heaven (Inna song)|Heaven]]".<ref name="topromanesc"/>

==Composition and reception==
{{Listen|pos=left|filename=Call The Police (sample).ogg|title="Call The Police"|description= A 17-second sample of "Call the Police", an [[eurodance]] song with a "Romanian music dance vibe".<ref name="beebee"/>|format =[[Ogg]]}} 
According to magazine ''Beebee'', the recording is a "typical" [[eurodance]] track which musically incorporates "Romanian music dance vibe" in its composition.<ref name="beebee">{{cite news|url=http://www.beebee.info/musicreviews/call-the-police-the-g-girls-have-arrived|title="Call The Police" the G Girls have arrived|author=Iko|date=2 June 2016|accessdate=28 June 2016|publisher=''Beebee''}}</ref> Jonali particularly called the single a "club-ready summer jam" and an "infectiously island-infused groove".<ref name="jonali"/> Iko, writing for magazine ''Beebee'', described the release of the single and the forming of the "hottest girl group out there" a surprise, while comparing the track to [[Inna]]'s works.<ref name="beebee"/> He further noted the band's commercial appeal, with him praising the accompanying music video for "Call the Police".<ref name="beebee"/> Jonali from his own music website named the song's hooks as being catchy, pointing out its refrain, "Somebody better call the police on me, somebody better call the police"; he as well drew comparisons to Inna.<ref name="jonali">{{cite news|url=http://jonalisblog.com/2016/06/01/g-girls-call-the-police-alexandra-stan-inna-form-new-romanian-pop-group/|title=G Girls "Call The Police": Alexandra Stan & Inna Team Up (Again) & Form Romanian Pop Group!|author=Jonali|date=1 June 2016|accessdate=28 June 2016|publisher=JonAli}}</ref> Japanese portal TV Groove saw the single as an "exquisite catchy club tune that is perfect for summer, packed with an addictive island groove".<ref name="tv groove">{{cite news|url=http://www.tvgroove.com/news/article/ctg/1/nid/29931.html|publisher=TV Groove|language=Japanese|title=アレスタ＆インナがガールズ・グループ結成！ ルーマニア発 最強美女集団「G Girls（ジー・ガールズ）」デビュー曲の配信がスタート|trans_title=Alexandra Stan and Inna formed a girl group! A new song by Romania's most-beautiful group G Girls|date=22 August 2016|accessdate=29 November 2016}}</ref>

Upon its release, "Call the Police" experienced moderate success on record charts. It reached a peak position at number six and five on Poland's [[Polish Society of the Phonographic Industry|Airplay Chart]] and [[Polish Society of the Phonographic Industry|Dance Top 50]], respectively.<ref name="Poland Airplay"/><ref name="Poland Dance"/> In native Romania, the single debuted at number 64 on the [[Kiss FM (Romania)|Airplay 100]] on the week ending 24 July 2016; the single also claimed the same position in its second and last week on the chart.<ref name="KissFM"/><ref name="KissFM2">{{cite news|url=http://www.kissfm.ro/airplay100/|title=Romania's Airplay 100 – 31 July 2016|date=|publisher=[[Kiss FM (Romania)|Kiss FM]]|language=Romanian|accessdate=31 July 2016}}</ref>

==Music video==
[[File:RO B - Banca Națională a României, corp vechi.jpg|thumb|200px|right|The accompanying music video for "Call the Police" was shot by Roman Burlaca in [[Bucharest]]. (''pictured'')<ref name="topromanesc"/>]]
An accompanying video for the song was shot by Roman Burlaca at Palatul Noblesse in [[Bucharest]],<ref name="infomusic">{{cite news|url=http://www.infomusic.ro/videoclip/g-girls-antonia-inna-alexandra-stan-lori-c-call-the-police/|title=Videoclip: G Girls (Antonia, Inna, Alexandra Stan & Lori C) - Call The Police|author=Tanasă, Raluca|date=1 June 2016|accessdate=29 June 2016|publisher=Info Music|language=Romanian}}</ref> and was uploaded onto the [[YouTube]] channel of label Global Records on 1 June 2016.<ref name="video">{{cite news|url=https://www.youtube.com/watch?v=Ww1fR5OeWZo&feature=youtu.be|title=G Girls – Call The Police (Official Music Video)|date=1 June 2016|accessdate=29 June 2016|publisher=[[YouTube]]}}</ref> Marius Panduru served as the [[director of photography]], while make-up was managed by Tania Cozma, Andra Manea and Dana Pertina, and hair styling by Alex Ifimov and Sorin Stratulat.<ref name="topromanesc"/> 

The video commences with Inna and Antonia entering the building after nodding to each other. Afterwards, the latter walks to a hall full of partying people while taking and developing photographs. Meanwhile, Inna is shown sitting on a mask-designed chair surrounded by males in tie in a white room. Stan, dressed in red lingerie, is displayed with a man which she later [[Bondage (BDSM)|ties up]] to a chair in the video and steals his money. Fellow band member Lori then walks in a neon-green room where both [[lesbian]] and [[gay]] couples are shown kissing, with the latter being portrayed in a bathtub letting a glass of water falling on one's body. She subsequently writes "911" on a mirror, and all the band members are shown dressing neon and sparkling outfits in order to be visible in the dark. After Lori mixes up some pink drinks, the visual ends with them walking out of the building while throwing all the frames that were taken by Antonia in the beginning on the floor. Scenes interspersed through the main video show the band performing to the track close to each other in light or dark places.<ref name="video"/>

==Track listing==
{{Track listing
| headline  = Digital download<ref name="ro"/>
| title1  = Call the Police
| length1  = 3:08
}}

==Charts==
{|class="wikitable sortable plainrowheaders" style="text-align:center"
|-
! scope="col"| Chart (2016) 
! scope="col"| Peak<br>position
|-
{{singlechart|Poland|6|year=2016|chartid=2162|rowheader=true|accessdate=5 December 2016|refname="Poland Airplay"}}
|-
{{singlechart|Polishdance|5|chartid=2146|rowheader=true|accessdate=16 November 2016|refname=Poland Dance}}
|-
! scope="row"| Poland ([[Polish Music Charts|Polish TV Airplay Chart]])<ref>{{cite web|url=http://bestsellery.zpav.pl/airplays/pozostale/archiwum.php?year=2016&typ=tv&idlisty=2138#title|title=Listy bestsellerów, wyróżnienia :: Związek Producentów Audio-Video|publisher=[[Polish Music Charts|Polish TV Airplay]]|accessdate=10 November 2016}}</ref>
| align="center"| 2
|-
! scope="row"|Romania ([[Romanian Radio Airplay Chart|Airplay 100]])<ref name="KissFM">{{cite news|url=http://www.kissfm.ro/airplay100/|title=Romania's Airplay 100 – 24 July 2016|date=|publisher=[[Kiss FM (Romania)|Kiss FM]]|language=Romanian|accessdate=24 July 2016}}</ref>
|align=center| 64
|}

==Credits and personnel==
Credits adapted from Top Românesc.<ref name="topromanesc">{{cite news|url=http://topromanesc.ro/2016/06/01/ggirls-proiect-global-records-lanseaza-call-the-police-colaborare-intre-inna-antonia-alexandra-stan-lori-c/|title=GGirls, proiect Global Records, lansează "Call the Police" – colaborare între Inna, Antonia, Alexandra Stan & Lori C|date=1 June 2016|accessdate=29 June 2016|publisher=Top Românesc|language=Romanian|trans_title=GGirls, a Global Records project, release "Call The Police" – a collaboration between Inna, Antonia, Alexandra Stan & Lori C|archiveurl=https://web.archive.org/web/20160910055248/http://topromanesc.ro/2016/06/01/ggirls-proiect-global-records-lanseaza-call-the-police-colaborare-intre-inna-antonia-alexandra-stan-lori-c/|archivedate=10 September 2016}}</ref>

{{col-begin}}
{{col-2}}
;Vocal and technical credits
*G Girls – [[vocals]]
**Alexandra Stan – main vocals
**Antonia – main vocals
**Inna – main vocals
**Lori – main vocals
*Sebastian Barac – [[Record producer|producer]]
*Marcel Botezan – producer
*Julimar Santos Oliveira Neponunceno – [[songwriter]]
*Eric Turner – songwriter
{{col-2}}
;Visual credits
*Roman Burlaca – director
*Tania Cozma – [[make-up]]
*Alex Ifimov – hair styling
*Andra Manea – make-up
*Sorin Stratulat – hair styling
*Marius Panduru – [[director of photography]]
*Dana Pertina – make-up
{{col-2}}
{{col-end}}

==Release history==
{|class="wikitable unsortable plainrowheaders"
|-
! Country
! Date
! Format
! Label
|-
! scope="row"|Denmark<ref name="dk">{{cite news|url=https://itunes.apple.com/dk/album/call-police-radio-edit-single/id1124052441|title=Call The Police (Radio Edit) – Single by G Girls on iTunes|date=14 June 2016|accessdate=29 June 2016|publisher=iTunes Store Denmark}}</ref>
|rowspan="5"|14 June 2016
|rowspan="6"|Digital single
|rowspan="5"|Global/<br>Roton
|-
! scope="row"|Germany<ref name="de">{{cite news|url=https://itunes.apple.com/de/album/call-police-radio-edit-single/id1124052441|title=Call The Police (Radio Edit) – Single von G Girls in iTunes|date=14 June 2016|accessdate=29 June 2016|publisher=iTunes Store Germany}}</ref>
|-
! scope="row"|Romania<ref name="ro">{{cite news|url=https://itunes.apple.com/ro/album/call-police-radio-edit-single/id1124052441|title=Call The Police (Radio Edit) – Single by G Girls on iTunes|date=14 June 2016|accessdate=29 June 2016|publisher=iTunes Store Romania}}</ref>
|-
! scope="row"|United Kingdom<ref name="gb">{{cite news|url=https://itunes.apple.com/gb/album/call-police-radio-edit-single/id1124052441|title=Call The Police (Radio Edit) – Single by G Girls on iTunes|date=14 June 2016|accessdate=29 June 2016|publisher=iTunes Store United Kingdom}}</ref>
|-
! scope="row"|United States<ref name="us">{{cite news|url=https://itunes.apple.com/us/album/call-police-radio-edit-single/id1124052441|title=Call The Police (Radio Edit) – Single by G Girls on iTunes|date=14 June 2016|accessdate=29 June 2016|publisher=iTunes Store United States}}</ref>
|-
! scope="row"|Italy<ref name="it">{{cite news|url=https://itunes.apple.com/it/album/call-the-police-single/id1132153960|title=Call The Police – Single di G Girls su iTunes|date=15 July 2016|accessdate=15 July 2016|publisher=iTunes Store Italy}}</ref>
|rowspan="1"|15 July 2016
|Ego
|}

==References==
{{reflist|2}}

{{Alexandra Stan}}
{{Inna}}
[[Category:2016 singles]]
[[Category:2016 songs]]
[[Category:English-language Romanian songs]]
[[Category:Eurodance songs]]
[[Category:Alexandra Stan songs]]
[[Category:Inna songs]]
[[Category:Songs written by Eric Turner (singer)]]