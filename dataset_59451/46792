[[File:YitzhakBuxbaumBySophieTabak.jpg|alt=Smiling portrait of Yitzhak Buxbaum c. 2015, wearing glasses, colourful Mizrahi-style kippah, white jacket|thumb|Yitzhak Buxbaum]]<!-- Do not remove this line! -->

'''Yitzhak Buxbaum''' is an American Jewish author and [[maggid]] (preacher/storyteller).

== Published work ==
Most of Buxbaum's books and articles relate to  [[Hasidic Judaism|Hasidism]], especially its storytelling tradition, and [[Neo-Hasidism]]. He has authored the following books.

*''Jewish Spiritual Practices'' Jason Aronson, 1991  ISBN 978-1-56821-206-7 
*''Storytelling and Spirituality in Judaism'' Jason Aronson, 1994 ISBN 978-1-56821-173-2
*''Real Davvening: Jewish Prayer as a Spiritual Practice and a Form of Meditation for Beginning and Experienced Davveners'' |Jewish Spirit Booklet Series , 1996  ISBN 978-1-5114-3313-6
*''An Open Heart: The Mystic Path of Loving People'' . Jewish Spirit Booklet Series , 1997  ISBN 978-0-9657-1122-7
* ''A Tu BeShvat Seder: The Feast of Fruits from the Tree of Life'' Jewish Spirit Booklet Series , 1998  ISBN 978-0-9657-1123-4
*''The Life and Teachings of Hillel'' 376 pp. Jason Aronson, 2000 ISBN 978-1-56821-049-0  According to [[WorldCat]], the book is held in 168 libraries<ref>[http://www.worldcat.org/title/life-and-teachings-of-hillel/oclc/28549607&referer=brief_results WorldCat book entry]</ref>
*''A Person is Like a Tree: A Sourcebook for Tu BeShvat'' Jason Aronson, 2000 ISBN 978-0-7657-6128-6
*''Storytelling and Spirituality in Judaism''  Jason Aronson, 2001 ISBN 978-0-7657-6166-8
*''Jewish Tales of Mystic Joy'' Jossey-Bass, 2002  ISBN 978-0-7879-6272-2
*''Jewish Tales of Holy Women'' Jossey-Bass, 2002  ISBN 978-1-118-10443-9
* ''The Light and Fire of the Baal Shem Tov'' Continuum, 2005 ISBN 978-0-8264-1772-5
*''Serach at the Seder: A Haggadah Supplement'', illustrations by Shoshannah Brombacher  Jewish Spirit, 2012
Reviews of Buxbaum's work have appeared in Jewish publications with a variety of perspectives,<ref>{{Cite journal|last=On The Bookshelf (column)|first=|date=July 11, 1996|title=The art of davvening is meaningfully taught in new booklet by Jewish Spirit|url=|journal=Sentinel: The Voice of Chicago Jewry|doi=|pmid=|access-date=}}</ref> including [[Algemeiner Journal|The Algemeiner Journal]],<ref name=":2">{{Cite journal|last=Kurzweil|first=Arthur|date=|title=Light and Fire|url=|journal=The Algemeiner Journal|issue=December 15, 2006|doi=|pmid=|access-date=}}</ref> [[Hadassah Women's Zionist Organization of America|Hadassah Magazine]],<ref name=":6">{{Cite web|url=http://www.hadassahmagazine.org/2013/03/27/serach-seder-haggadah-supplement/|title=Serach at the Seder: A Haggadah Supplement|last=Shluker|first=Zelda|date=2013-03-27|website=Hadassah Magazine|publisher=|access-date=2016-05-20}}</ref> [[The Jewish Chronicle]],<ref name=":4">{{Cite web|url=http://website.thejc.com/home.aspx?AId=46028&ATypeId=1&search=true2&srchstr=demant&srchtxt=0&srchhead=1&srchauthor=0&srchsandp=0&scsrch=0|title=The Jewish Chronicle - Getting to the heart of Chasidism|last=Demant|first=Jason|date=7 September 2006|website=website.thejc.com|publisher=|access-date=2016-05-20}}</ref> and [[Tikkun (magazine)|Tikkun]].<ref name=":3">{{Cite journal|last=Frankel|first=Estelle|date=|title=Stories That Heal the Soul|url=|journal=Tikkun|issue=May/June 2006|page=73|doi=|pmid=|access-date=}}</ref> His books have been reviewed for broader audiences in the journal [[Parabola (magazine)|Parabola]]<ref name=":5">{{Cite journal|last=Kurzweil|first=Arthur|last2=|date=Fall 2004|title=The Light and Fire of the Baal Shem Tov, Yitzhak Buxbaum (book review)|url=|journal=Parabola|publisher=|issue=31:3|doi=|doi-broken-date=|pmid=|access-date=}}</ref> and the website Spirituality and Practice.<ref>{{Cite web|url=http://www.spiritualityandpractice.com/book-reviews/view/10180/the-light-and-fire-of-the-baal-shem-tov|title=The Light and Fire of the Baal Shem Tov, 2005 S & P Award Winner {{!}} Book Reviews {{!}} Books {{!}} Spirituality & Practice|last=Brussat|first=Frederic and Mary Ann|website=www.spiritualityandpractice.com|access-date=2016-05-24}}</ref><ref>{{Cite web|url=http://www.spiritualityandpractice.com/books/reviews/view/5217|title=Jewish Tales of Mystic Joy {{!}} Book Reviews {{!}} Books {{!}} Spirituality & Practice|last=Brussat|first=Frederic and Mary Ann|website=www.spiritualityandpractice.com|access-date=2016-05-24}}</ref>

Manuscripts and drafts of ''The Light and Fire of the Baal Shem Tov'' are archived at Cornell University Library.<ref name=":7" />

== Storytelling ==
Buxbaum tells stories "in Jewish and non-Jewish settings to Jewish and non-Jewish audiences", with a focus on "the spiritual nature of storytelling."<ref>Horowitz, Rosemary, ed. ''Elie Wiesel and the Art of Storytelling'' (Jefferson, North Carolina: McFarland & Company, 2006), 5.</ref><ref name=":0">{{Cite web|url=http://static.record-eagle.com/2007/may/12gretchen.htm|title=The Best of the Besht: Brooklyn man is master of thousands of tales|website=static.record-eagle.com|access-date=2016-04-17}}</ref> He has been grouped among "the most active tellers in the Jewish world."<ref>{{Cite book|title=Chosen Tales: Stories Told by Jewish Storytellers|last=Schram|first=Peninnah|publisher=Jason Aronson|year=1995|isbn=1-56821-352-2|location=Northvale, New Jersey|pages=xxxvi}}</ref>

== Maggid training program ==
Building on his ordination as a maggid by [[Shlomo Carlebach (musician)|Shlomo Carlebach]],<ref>{{Cite web|url=http://www.jweekly.com/article/full/67523/tale-of-the-maggid-jewish-storytelling-enters-a-new-era/|title=Tale of the maggid: Jewish storytelling enters a new era  {{!}}  j. the Jewish news weekly of Northern California|last=silvers|first=emma|website=www.jweekly.com|access-date=2016-04-17}}</ref> Buxbaum established a program to train women and men as maggidim (plural of maggid).<ref name=":1">{{Cite web|url=http://www.thejewishweek.com/news/new-york-news/tales-maggid|title=Tales of a Maggid: Sacred Hints for Davening and Dance.|website=The Jewish Week {{!}} Connecting The World To Jewish News, Culture & Opinion|access-date=2016-04-17}}</ref> Graduates include Shoshana Litman, described as [[Canada|Canada's]] first ordained female Jewish storyteller,<ref>{{Cite web|url=http://www.timescolonist.com/opinion/maggidah-is-a-canadian-first-1.2180683|title=Maggidah is a Canadian First|last=Lee|first=Jan|website=Times Colonist|access-date=2016-04-17}}</ref> and Tamir Zaltsman, who states that he is the first ordained Russian-speaking maggid.<ref>{{Cite web|url=http://events.r20.constantcontact.com/register/event?llr=a8cdaziab&oeidk=a07e5h6fbmbaa7dd746|title=First Ever Russian-Speaking Maggid Graduation Party|last=|first=|date=February 5, 2012|website=|publisher=|access-date=}}</ref> Some graduates are themselves training maggidim.<ref>{{Cite web|url=https://darshanyeshiva.org/maggid-staff-and-instructors/|title=Maggid Staff and Instructors: Bruce "Dov" Forman, Shoshannah Brombacher|last=|first=|date=|website=|publisher=|access-date=}}</ref>

== Background and personal life ==
Buxbaum graduated from [[Cornell University]] (class of 1964).<ref name=":7">{{Cite web|url=https://beta.worldcat.org/archivegrid/collection/data/122681565|title=ArchiveGrid : Yitzhak Buxbaum papers, 1991-2005.|website=beta.worldcat.org|access-date=2016-05-27}}</ref>

He has told interviewers that as a young man, he identified as an [[Atheism|atheist]] and felt disconnected from his Jewish roots. But a time of intense soul-searching, and encounters with Rabbi Shlomo Carlebach, led him to devote his life to Jewish spirituality.<ref>{{Cite web|url=http://www.tabletmag.com/jewish-life-and-religion/1323/radical-mystic|title=Radical Mystic - Tablet Magazine – Jewish News and Politics, Jewish Arts and Culture, Jewish Life and Religion|website=Tablet Magazine|access-date=2016-04-17}}</ref>

In 2007, Buxbaum was one of six spiritual leaders from different faiths who opened the memorial celebration for [[Sri Chinmoy]] at the [[United Nations]].<ref>{{Cite web|url=http://www.srichinmoybio.co.uk/news/sri-chinmoy/un-sri-chinmoy/|title=Celebration of the Life of Sri Chinmoy at the United Nations – Sri Chinmoy Centre News|website=www.srichinmoybio.co.uk|access-date=2016-05-26}}</ref>

Buxbaum lives in [[Brooklyn]]. He is married to actor and storyteller Carole Forman.<ref>{{Cite web|url=http://caroleforman.com/meet.html|title=Meet Carole Forman Storytelling and Acting|website=caroleforman.com|access-date=2016-04-25}}</ref>

==References==
{{reflist|30em}}

== External links ==
* [http://jewishspirit.com/index.html Yitzhak Buxbaum website]

Article by Yitzhak Buxbaum: [http://thejewishreview.org/articles/?filename=buxbaum4_3&route=fromsearch Real Davening: Chasidic Answers to the Crisis in Prayer]. ''The Jewish Review: A Journal of Torah, Judaism, Philosophy, Life and Culture'' 4:3 (March 1991 / Adar 5751)
* [http://www.chabad.org/tools/rss/author_rss.xml?kid=10194 Articles by Yitzhak Buxbaum at chabad.org]
* [http://www.bloomsbury.com/us/the-light-and-fire-of-the-baal-shem-tov-9780826417725/ Publisher's website on ''The Light and Fire of the Baal Shem Tov'']
{{authority control}}

{{DEFAULTSORT:Buxbaum, Yitzhak}}
[[Category:21st-century American writers]]
[[Category:Jewish American writers]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]