{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
'''Carnevale in Adelaide''' is an annual Italian festival held in [[Adelaide]], [[South Australia]] which follows the centuries-old Christian tradition of Carnevale which is the last celebration before Ash Wednesday and the beginning of [[Lent]] - see [[Carnival]].

== Background and History ==
The first Carnevale in Adelaide was held in 1976 in [[Rundle Mall]]. A parade travelled from [[Victoria Square, Adelaide|Victoria Square]] to [[Elder Park]] before ending with celebrations in Rundle Mall. It was originally called The Italian Festival and is now known as Carnevale in Adelaide. The festival has been held in a number of locations over the years including Rundle Mall,  Elder Park, [[Norwood Oval|Norwood]] and [[Adelaide Oval]]s, [[Rymill Park]]. In 2015 it was held at [[Adelaide Showgrounds|The Adelaide Showground]]<ref>{{cite web |url=http://www.carnevale-adelaide.com/history.htm |title=Carnevale Italian Festival - History |accessdate = 17 April 2015 |publisher=Co-ordinating Italian Committee}}</ref>

It is a means of inspiring young  people to embrace their Italian heritage and for the wider community to  experience Italian culture. The event also raises funds to cater for welfare needs of the elderly in the Italian community through the Co-ordinating Italian Committee (CIC)<ref>{{Cite web|url=http://www.cicinc.com.au/ |title=Coordinating Italian Committee (CIC) |date= |accessdate=17 April 2015 |website=Coordinating Italian Committee |publisher=CIC |last= |first= |deadurl=yes |archiveurl=https://web.archive.org/web/20150411061327/http://www.cicinc.com.au:80/ |archivedate=11 April 2015 |df=dmy }}</ref> and supports other worthy charities and community organisations.<ref>{{Cite web|url = http://www.carnevale-adelaide.com/cic.htm|title = Carnevale Italian Festival - CIC Inc.|date = |accessdate = 17 April 2015|website = Carnevale Italian Festival|publisher = Coordinating Italian Committee|last = |first = }}</ref>

=== 1976 ===
Inaugural Italian Festival held in Rundle Mall.

=== 2007 ===
Held 10–11 February at [[Rymill Park]].<ref>{{Cite web|url = http://www.catalog.slsa.sa.gov.au:80/record=b2308300~S3|title = Carnevale in Adelaide [poster]|date = |accessdate = 17 April 2015|website = State Library of South Australia|publisher = |last = |first = }}</ref>

=== 2012 ===
Held 11–12 February at The Adelaide Showground.<ref>{{Cite web|url = http://compleattraveller.com/australia/south-australia/february-2012-events-in-adelaide/|title = February 2012 Events in Adelaide|date = 5 February 2012|accessdate = 17 April 2012|website = The Compleat Traveller blog|publisher = |last = Lesses|first = Jim}}</ref>

=== 2013 ===
Held 9–10 February at The Adelaide Showground.<ref>{{Cite web|url = http://www.aeec.com.au/events/2012/02/11/carnevale.jsp|title = Events - Carnevale|date = |accessdate = 17 April 2015|website = Adelaide Event and Exhibition Centre|publisher = Adelaide Event and Exhibition Centre|last = |first = }}</ref>

=== 2014 ===
Held 8–9 February at The Adelaide Showground.<ref>{{Cite web|url = http://www.weekendnotes.com/carnevale-italian-festival-adelaide/|title = Italian Carnevale Adelaide 2014|date = |accessdate = 17 April 2014|website = Weekend Notes|publisher = |last = Samis|first = Betty}}</ref>

=== 2015 ===
Held 7–8 February at The Adelaide Showground.<ref>{{Cite web|url = https://www.facebook.com/carnevale.adelaide|title = Carnevale Adelaide Facebook Page|date = |accessdate = 17 April 2015|website = Facebook|publisher = |last = |first = }}</ref>

=== 2016 ===
Will be held 13–14 February at The Adelaide Showground.

== Program ==
The program includes live music, activities for children, dance, comedy, Mr and Miss Carnevale competition, fireworks, food and drink, cooking demonstrations, Italian cars on display, fashion parades, the national sausage making competition and screenings of Italian films and documentaries.<ref>{{Cite web|url=http://www.samemory.sa.gov.au/site/page.cfm?c=3515 |title=Carnevale in Adelaide [poster] |date=1996 |accessdate=17 April 2015 |website=SA Memory |publisher= |last= |first= |deadurl=yes |archiveurl=https://web.archive.org/web/20150417082829/http://www.samemory.sa.gov.au/site/page.cfm?c=3515 |archivedate=17 April 2015 |df=dmy }}</ref>

==References==
{{reflist}}

[[Category:Festivals in Adelaide]]