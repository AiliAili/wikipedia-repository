{{Infobox journal
| title = Journal of Microscopy
| cover =
| formernames = Transactions of The Microscopical Society & Journal
| discipline = [[Microscopy]]
| abbreviation = J. Microsc.
| editor = Tony Wilson
| publisher = [[Wiley-Blackwell]] on behalf of the [[Royal Microscopical Society]]
| country = United Kingdom
| impact = 2.15
| impact-year = 2013
| history = 1841-present
| frequency = Monthly
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2818
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2818/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2818/issues
| link2-name = Online archive
| ISSN = 0022-2720
| eISSN = 1365-2818
| CODEN = JMICAR
| LCCN = 75613422
| OCLC = 746955555
}}
The '''''Journal of Microscopy''''' is the [[peer-reviewed]] [[scientific journal]] of the [[Royal Microscopical Society]] which covers all aspects of [[microscopy]] including spatially resolved [[spectroscopy]], compositional mapping, and [[image analysis]]. This includes technology and applications in [[physics]], [[chemistry]], [[material science]], and the life sciences. It is published by [[Wiley-Blackwell]] on behalf of the Society. The [[editor-in-chief]] is Tony Wilson, an Engineering Science professor at [[Oxford University]].<br/>
The journal publishes [[review article]]s, original research papers, short communications, and [[letters to the editor]]. It was established in 1841 as the ''Transactions of The Microscopical Society & Journal'', obtaining its current name in 1869, with volume numbering restarting at 1.

== Abstracting and indexing ==
[[File:Quarterly journal of microscopical science (1853) (14770593094).jpg|thumbnail|left|A plate from the 1853 volume of the Journal illustrating Professor [[Arthur Henfrey]]'s paper on Fresh-water Con-fervoid Algae.]]
The journals is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Biological Abstracts]]
*[[BIOSIS Previews]]
*[[Current Contents]]/Life Sciences
*[[Science Citation Index]]
*[[Academic Search Premier]]
*[[CAB Direct (database)|CAB Direct]]
*[[CAB Abstracts]]
*[[CSA Illumina]]
*[[Current Index to Statistics]]
*[[Chemical Abstracts Service]]
*[[Embase]]
*[[Global Health]]
*[[Mathematical Reviews]]
*[[MEDLINE]]/[[PubMed]]
*[[VINITI Database RAS]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of  1.633.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Microscopy |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2818}}

[[Category:Microscopy]]
[[Category:Royal Microscopical Society]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1841]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Optics journals]]