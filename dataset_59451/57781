{{Italic title}}
{{Use Irish English|date=July 2015}}
'''''Irish Political Studies''''' is a quarterly [[peer-reviewed]] [[academic journal]] and the official journal of the [[Political Studies Association of Ireland]] covering research on [[Irish politics]] including politics of the [[Republic of Ireland]] and [[Northern Ireland]] and politics concerning relations with the [[United Kingdom]] and the [[European Union]]. The [[editors-in-chief]] are Thomas Hennessey (Canterbury) and R. Kenneth Carty (UBC). The journal was established in 1986 and is published by [[Routledge]].

''Irish Political Studies'' sponsors a conference paper prize at the annual conference of the Political Studies Association of Ireland each year. Previous winners include: Sean McGraw (Notre Dame), Jane Suiter (DCU), and Emmanuelle Schon-Quinlivan (UCC).

==Abstracting and indexing==
The journal is abstracted and indexed in [[Current Contents]]/Social & Behavioral Sciences,<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-02-24}}</ref> Scopus<ref name=Scopus>{{cite web |url=https://www.scopus.com/sourceid/5800170866 |title=Source details: Irish Political Studies |publisher=[[Elsevier]] |work=Scopus preview |accessdate=2017-02-24}}</ref> and the Social Sciences Citation Index.,<ref name=ISI/> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.633.<ref name=WoS>{{cite book |year=2016 |chapter=Irish Political Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

== External links ==
* {{Official|1=http://www.tandfonline.com/action/journalInformation?journalCode=fips20}}
*Print: {{ISSN|0790-7184}}
*Online: {{ISSN|1743-9078}}

[[Category:Politics of Ireland]]
[[Category:Politics of Northern Ireland]]
[[Category:Political science journals]]
[[Category:Routledge academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1986]]