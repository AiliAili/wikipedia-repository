{{about|the novella|other uses}}
{{Infobox book
| name          = Carmilla
| author        = [[Sheridan Le Fanu]]
| language      = English
| country       = Isle of Man
| genre         = [[Gothic novel|Gothic]]
| publisher     =
| isbn          =
| 
| title_orig    =
| translator    =
| image         = Carmilla.jpg
| caption       = Illustration from ''The Dark Blue'' by [[D. H. Friston]], 1872
| illustrator   =
| cover_artist  =
| series        =
| release_date  = 1871-72
| english_release_date =
| media_type    =
| pages         =108
| preceded_by   =
| followed_by   =
}}

'''''Carmilla''''' is a [[Gothic novel]]la by [[Sheridan Le Fanu|Joseph Sheridan Le Fanu]] and one of the early works of [[vampire fiction]], predating [[Bram Stoker]]'s ''[[Dracula]]'' (1897) by 26 years. First published as a [[Serial (literature)|serial]] in ''[[The Dark Blue]]'' (1871–72), the story is narrated by a young woman preyed upon by a female [[vampire]] named Carmilla, later revealed to be Mircalla, Countess Karnstein (Carmilla is an anagram of Mircalla). The story is often [[Anthology|anthologized]] and has been adapted many times in film and other media.

==Publication==
''Carmilla,'' serialized in the literary magazine ''The Dark Blue'' in late 1871 and early 1872,<ref>The story ran in one issue of 1871 (December, pp. 434–448) and in three issues of 1872 (January, pp. 592–606; February, pp. 701–714; and March, pp. 59–78).</ref> was reprinted in Le Fanu's short story collection ''[[In a Glass Darkly]]'' (1872). Comparing the work of the two [[illustrators]], [[David Henry Friston]] and Michael Fitzgerald, whose work appears in the magazine but not in modern printings of the book, reveals inconsistencies in the characters' depictions. Consequently, confusion has arisen relating the pictures to the plot.{{citation needed|date=October 2015}} Isabella Mazzanti illustrated the book's 2014 anniversary edition, published by Editions Soleil and translated by Gaid Girard.<ref>{{cite book|author=Le Fanu, Sheridan & Girard, Gaid (Translator) & Mazzanti, Isabella (Illustrator)|title=Carmilla|date=2014|language=French|publisher=Editions Soleil}}</ref><ref>{{cite news|url=https://bookillustrations.quora.com/Isabella-Mazzantis-Illustrations-for-Carmilla|website=Quora|title=Book Artists and Their Illustrations:  Isabella Mazzanti's Illustrations for "Carmilla"|author=Stein, Rivka|date=January 21, 2014 }}</ref>

==Plot summary==
Le Fanu presents the story as part of the casebook of Dr. Hesselius, whose departures from medical orthodoxy rank him as the first [[occult doctor]] in literature.<ref>{{cite web|website=gwthomas.org|url=http://www.gwthomas.org/hesselius.htm |title=Dr Martin Hesselius}}</ref>

Laura, the protagonist, narrates, beginning with her childhood in a "picturesque and solitary" castle amid an extensive forest in [[Styria]], where she lives with her father, a wealthy [[English people|English]] widower retired from service to the [[Austrian Empire]]. When she was six, Laura had a vision of a beautiful visitor in her bedchamber. She later claims to have been punctured in her breast, although no wound was found.

Twelve years later, Laura and her father are admiring the sunset in front of the castle when her father tells her of a letter from his friend, General Spielsdorf. The General was supposed to bring his niece, Bertha Rheinfeldt, to visit the two, but the niece suddenly died under mysterious circumstances. The General ambiguously concludes that he will discuss the circumstances in detail when they meet later.

Laura, saddened by the loss of a potential friend, longs for a companion. A carriage accident outside Laura's home unexpectedly brings a girl of Laura's age into the family's care. Her name is Carmilla. Both girls instantly recognize the other from the "dream" they both had when they were young.

Carmilla appears injured after her carriage accident, but her mysterious mother informs Laura's father that her journey is urgent and cannot be delayed. She arranges to leave her daughter with Laura and her father until she can return in three months. Before she leaves, she sternly notes that her daughter will not disclose any information whatsoever about her family, past, or herself, and that Carmilla is of sound mind. Laura comments that this information seems needless to say, and her father laughs it off.

Carmilla and Laura grow to be very close friends, but occasionally Carmilla's mood abruptly changes. She sometimes makes romantic advances towards Laura. Carmilla refuses to tell anything about herself, despite questioning by Laura. Her secrecy is not the only mysterious thing about Carmilla; she never joins the household in its prayers, she sleeps much of the day, and she seems to sleepwalk outside at night.

Meanwhile, young women and girls in the nearby towns have begun dying from an unknown malady. When the funeral procession of one such victim passes by the two girls, Laura joins in the funeral hymn. Carmilla bursts out in rage and scolds Laura, complaining that the hymn hurts her ears.

When a shipment of restored [[heirloom]] paintings arrives, Laura finds a portrait of her ancestor, Mircalla, Countess Karnstein, dated 1698. The portrait resembles Carmilla exactly, down to the mole on her neck. Carmilla says she might be descended from the Karnsteins even though the family died out centuries before.

During Carmilla's stay, Laura has nightmares of a large cat-like beast entering her room and biting her on the chest. The beast then takes the form of a female figure and disappears through the door without opening it. In another nightmare, Laura hears a voice say, "Your mother warns you to beware of the assassin," and a sudden light reveals Carmilla standing at the foot of her bed, her nightdress drenched in blood. Laura's health declines, and her father has a doctor examine her. He finds a small blue spot on her chest and speaks privately with her father, only asking that Laura never be unattended.

Her father then sets out with Laura, in a carriage, for the ruined village of Karnstein, three miles distant. They leave a message behind asking Carmilla and one of the governesses to follow once the perpetually late-sleeping Carmilla wakes. En route to Karnstein, Laura and her father encounter General Spielsdorf. He tells them his own ghastly story.

At a costume ball, Spielsdorf and his niece Bertha had met a young woman named Millarca and her enigmatic mother. Bertha was immediately taken with Millarca. The mother convinced the General that she was an old friend of his and asked that Millarca be allowed to stay with them for three weeks while she attended to a secret matter of great importance.

[[Image:Fitzgerald, funeral from Carmilla.jpg|thumb|right|250px|''Funeral'', illustration by Michael Fitzgerald for ''Carmilla'' in ''The Dark Blue'' (January 1872)]]

Bertha fell mysteriously ill, suffering the same symptoms as Laura. After consulting with a specially ordered priestly doctor, the General realized that Bertha was being visited by a vampire. He hid with a sword and waited until a large black creature crawled onto his niece's bed and to her neck. He leapt from his hiding place and attacked the beast, which took the form of Millarca. She fled through the locked door, unharmed. Bertha died immediately afterward.

Upon arriving at Karnstein, the General asks a woodman where he can find the tomb of Mircalla Karnstein. The woodman says the tomb was relocated long ago by the hero who vanquished the vampires that haunted the region.

While the General and Laura are alone in the ruined chapel, Carmilla appears. The General and Carmilla both fly into a rage upon seeing each other, and the General attacks her with an axe. Carmilla disarms the General and disappears. The General explains that Carmilla is also Millarca, both anagrams for the original name of the vampire Mircalla, Countess Karnstein.

The party is joined by Baron Vordenburg, the descendant of the hero who rid the area of vampires long ago. Vordenburg, an authority on vampires, has discovered that his ancestor was romantically involved with the Countess Karnstein before she died and became one of the undead. Using his forefather's notes, he locates Mircalla's hidden tomb. An Imperial Commission exhumes the vampire's body. Immersed in blood, it seems to be breathing faintly, its heart beating, its eyes open. A stake is driven through its heart, and it gives a corresponding shriek; then the head is struck off. The body and head are burned to ashes, which are thrown into a river.

Afterwards, Laura's father takes his daughter on a year-long tour through Italy to recover from the trauma and regain her health, which she never fully does.

==Sources==
[[File:Dom Augustin Calmet.jpeg|thumb|right|<center>[[Dom Calmet]].]]
As with ''[[Dracula]]'', critics have looked for the sources used in the writing of ''Carmilla''. One source used was from a dissertation on magic, vampires and the apparitions of spirits written by [[Antoine Augustin Calmet|Dom Augustin Calmet's]] titled ''[[Traité sur les apparitions des esprits et sur les vampires ou les revenans de Hongrie, de Moravie, &c.]]'' (1751). This is evidenced by a report analyzed by Calmet, from a priest who learned information of a town being tormented by a vampiric entity 3 years earlier. Having traveled to the town to investigate and collecting information of the various inhabitants there, the priest learned that a vampire had tormented many of the inhabitants at night by coming from the nearby cemetery and would haunt many of the residents on their beds. An unknown Hungarian traveler came to the town during this period and helped the town by setting a trap at the cemetery and decapitating the vampire that resided there, curing the town of their torment. This story was retold by LeFanu and adapted into the thirteenth chapter of Carmilla <ref>{{cite book|last1=LeFanu|first1=J. Sheridan|title=Carmilla: Annotated with Notes|date=2016|isbn=978-1-5394-0617-4|pages=114-115;146-147}}</ref><ref>{{cite book|author=Calmet, Antoine Augustin|date=1746|url=https://archive.org/details/dissertationssu00conggoog |title=Dissertations sur les apparitions des anges, des demons et des esprits, et sur les revenants et vampires de Hongrie, de Boheme, de Moravie, et de Silesie|location=Paris|publisher= De Bure l'aîné|language=French}}</ref><ref>{{cite book|last1=Calmet|first1=Augustin|title=Treatise on the Apparitions of Spirits and on Vampires or Revenants: of Hungary, Moravia, et al. The Complete Volumes I & II. 2016|isbn=978-1-5331-4568-0}}</ref> <ref>{{cite book|last1=J. Sheridan LeFanu|title=Carmilla: Annotated|isbn=978-1539406174}}</ref>

According to Matthew Gibson, [[Sabine Baring-Gould|the Reverend Sabine Baring-Gould]]'s ''The Book of Were-wolves'' (1863) and his account of [[Elizabeth Báthory]], [[Samuel Taylor Coleridge|Coleridge]]'s ''[[Christabel (poem)|Christabel]]'' (Part 1, 1797 and Part 2, 1800), and Captain [[Basil Hall]]'s ''[[Schloss]] [[Leitersdorf im Raabtal|Hainfeld]]; or a Winter in [[Lower Styria]]'' (London and Edinburgh, 1836) are other sources for Le Fanu's ''Carmilla''. Hall's account provides much of the Styrian background and, in particular, a model for both Carmilla and Laura in the figure of [[Joseph von Hammer-Purgstall|Jane Anne Cranstoun, Countess Purgstall]].<ref>{{cite journal|author=Gibson, Matthew |url=http://www.jslefanu.com/gibson.html |title=Jane Cranstoun, Countess Purgstall: A Possible Inspiration for Le Fanu's ''Carmilla'' |journal=Le Fanu Studies |date=November 2007 |issn=1932-9598 |deadurl=yes |archiveurl=https://web.archive.org/web/20090304171734/http://www.jslefanu.com:80/gibson.html |archivedate=2009-03-04 |df= }}</ref><ref>{{cite book|author=Gibson, Matthew |date=2006|title=Dracula and the Eastern Question: British and French Vampire Narratives of the Nineteenth-century Near East|isbn=1-4039-9477-3 }}</ref>

==Influence==
Carmilla, the title character, is the original prototype for a legion of female and [[lesbian vampire]]s. Though Le Fanu portrays his vampire's [[Human sexuality|sexuality]] with the circumspection that one would expect for his time, it is evident that [[lesbian]] attraction is the main dynamic between Carmilla and the narrator of the story:<ref>{{cite book|author=Keesey, Pam |date=1993|title=Daughters of Darkness: Lesbian Vampire Stories|publisher= Cleis|isbn= 0-939416-78-6}}</ref><ref>{{Cite web|url=https://www.academia.edu/7342924/Female_Sexuality_as_Vampiric_in_Le_Fanu_s_Carmilla|title=Female Sexuality as Vampiric in Le Fanu’s Carmilla|access-date=2016-09-04}}</ref>

{{quotation|Sometimes after an hour of apathy, my strange and beautiful companion would take my hand and hold it with a fond pressure, renewed again and again; blushing softly, gazing in my face with languid and burning eyes, and breathing so fast that her dress rose and fell with the tumultuous respiration. It was like the ardour of a lover; it embarrassed me; it was hateful and yet overpowering; and with gloating eyes she drew me to her, and her hot lips travelled along my cheek in kisses; and she would whisper, almost in sobs, "You are mine, you ''shall'' be mine, and you and I are one for ever". (''Carmilla'', Chapter 4).}}

When compared to other literary vampires of the 19th century, Carmilla is a similar product of a culture with strict sexual mores and tangible religious fear. While Carmilla selected exclusively female victims, she only becomes emotionally involved with a few. Carmilla had [[nocturnal]] habits, but was not confined to the darkness. She had unearthly beauty, and was able to change her form and to pass through solid walls. Her animal alter ego was a monstrous black cat, not a large dog as in ''Dracula''. She did, however, sleep in a coffin. ''Carmilla'' works as a [[Gothic fiction|Gothic]] horror story because her victims are portrayed as succumbing to a perverse and unholy temptation that has severe metaphysical consequences for them.<ref>{{Cite book |last=Auerbach |first=Nina |title=Our Vampires, Ourselves |publisher=Chicago: University of Chicago Press |year=1995 |location= |page=42 |isbn= 0-2260-3202-7}}</ref>

Some critics, among them [[William Veeder]], suggest that ''Carmilla'', notably in its outlandish use of narrative frames, was an important influence on [[Henry James]]' ''[[The Turn of the Screw]]'' (1898).<ref>{{cite book|last1=Veeder|first1=William|title=Carmilla: The Art of Repression|date=1980|publisher=University of Texas Press|edition=Vol. 22, No. 2|jstor=40754606}}</ref>

===Bram Stoker's ''Dracula''===
Although ''Carmilla'' is a lesser known and far shorter [[Gothic fiction|Gothic]] vampire story than the generally considered master work of that genre, ''Dracula'', the latter is heavily influenced by Le Fanu's novella. Stoker's posthumously published [[short story]] "[[Dracula's Guest and Other Weird Stories|Dracula's Guest]]" (1914), known as the deleted first chapter to ''Dracula'', shows a more obvious and intact debt to ''Carmilla'':
* In the narrative, an Englishman finds himself in Germany midway upon his journey from England to the castle of Dracula; stumbles upon a tomb of a female vampire whose inscription reads: Countess Dolingen of Gratz / in Styria / sought and found death / 1801. 
* Both stories are told in the [[first-person narrative|first person]]. ''Dracula'' expands on the idea of a first person account by creating a series of journal entries and logs of different persons and creating a plausible background story for their having been compiled. 
* Both authors indulge the air of mystery, though Stoker takes it further than Le Fanu by allowing the characters to solve the enigma of the vampire along with the reader.
* The descriptions of the title character in ''Carmilla'' and of Lucy in ''Dracula'' are similar, and have become archetypes for the appearance of the waif-like victims and seducers in vampire stories as being rosy-cheeked, slender, languid, and with large eyes, full lips, and soft voices.{{citation needed|date=October 2015}} Additionally, both women [[Sleepwalking|sleepwalk]].
*Stoker's Dr. [[Abraham Van Helsing]] is a direct parallel to Le Fanu's vampire expert Baron Vordenburg: both characters used to investigate and catalyse actions in opposition to the vampire, and symbolically represent knowledge of the unknown and stability of mind in the onslaught of chaos and death.<ref>{{cite news|title=Le Fanu, J.S.|author=Sullivan, Jack (Editor)|work=The Penguin Encyclopedia of Horror and the Supernatural|page= 260}}</ref>

==In popular culture==

===Books===
(Alphabetical by first author's surname)
* In the Japanese [[light novel]] series [[High School DxD]], written by [[Ichiei Ishibumi]] and illustrated by [[Miyama-Zero]], the vampires are depicted as having a society divided among two major factions: The Tepes and the Carmilla. The Carmilla faction favors a matriarchal society for the world of vampires while the Tepes prefer a patriarchal government.
* ''Carmilla: A Dark Fugue'' is a short book by David Brian. Although the story is primarily centered around the exploits of General Spielsdorf, it nonetheless relates directly to events which unfold within ''Carmilla: The Wolves of Styria''.{{citation needed|date=October 2015}}
* The novel ''Carmilla: The Wolves of Styria'' is a re-imagining of the original story. It is a derivative re-working, listed as being authored by [[J.S. Le Fanu]] and David Brian.<ref>{{cite book|authors=Brian, David & Le Fanu, J.S.|date=2013|title= The Wolves of Styria|publisher= Night-Flyer|isbn= 978-1481952217}}</ref>
* [[Rachel Klein (novelist)|Rachel Klein's]] 2002 novel "[[The Moth Diaries]]" features several excerpts from "Carmilla," as the novel figures into the plot of Klein's story, and both deal with similar subject matter and themes.
* ''Carmilla: The Return'' by [[Kyle Marffin]] is the sequel to ''Carmilla''.<ref>{{cite book|author=Marffin, Kyle |date=1998|title= The Return|publisher= Design Image Group|isbn= 1-891946-02-1}}</ref>
*[[Ro McNulty]]'s novella, ''Ruin: The Rise of the House of Karnstein,'' is a sequel to Le  Fanu's novella and takes place over 100 years later. Carmilla continues to play games with mortals, inserting herself into their lives and breaking them to her will. She settles herself around a teacher and his family, feeding on his baby daughter.{{citation needed|date=October 2015}} 
* A vampire named Baron Karnstein appears in [[Kim Newman]]'s novel ''[[Anno Dracula series#Anno Dracula|Anno Dracula]]'' (1992). Carmilla herself is mentioned several times as a former (until her death at the hands of vampire hunters) friend of the book's vampire heroine Geneviève. Some short stories set in the Anno Dracula series universe have also included Carmilla.
* Author [[Anne Rice]] has cited ''Carmilla'' as an inspiration for [[The Vampire Chronicles]]; a series of bestselling vampire books she wrote from 1976 to 2003.
* Christopher Thomason identified ''Carmilla'' as a key influence for his 2015 novel ''For The Want Of Beauty.''

===Comics===
(Alphabetical by series)
* In 1991, Aircel Comics published a six-issue black and white miniseries of ''Carmilla'' by Steven Jones and [[John Ross (artist)|John Ross]]. It was based on Le Fanu's story and billed as "The Erotic Horror Classic of Female Vampirism". The first issue was printed in February 1991. The first three issues adapted the original story, while the latter three were a sequel set in the 1930s.<ref>{{cite web|url=http://fuziondigital.com/SPJ/creditscomics.htm |author=Jones, Steven Philip |title=Previous Credits in comics |website=Fuzi On Digital |deadurl=yes |archiveurl=https://web.archive.org/web/20080704062031/http://fuziondigital.com/SPJ/creditscomics.htm |archivedate=2008-07-04 |df= }}</ref><ref>{{cite web|url=http://www.comics.org/series.lasso?SeriesID=28117 |website=The Grand Comics Database Team|title= Carmilla (1991 Series)}}</ref>
* In the first story arc of [[Dynamite Entertainment]]'s revamp of [[Vampirella]], a villainous vampire, named Le Fanu, inhabits the basement of a Seattle nightclub called Carmilla.

===Film===
(Chronological)
* [[Danes|Danish]] director [[Carl Dreyer]] loosely adapted ''Carmilla'' for his film ''[[Vampyr]]'' (1932) but deleted any references to lesbian sexuality.<ref>{{Cite book |last=Grant |first=Barry Keith |authorlink= |last2=Sharrett |first2=Christopher |author2-link= |title=Planks of Reason: Essays on the Horror Film |publisher=Scarecrow Press |year=2004 |location= |page=73 |url =https://books.google.com/books?id=QHg7m_ESR54C&printsec=frontcover#v=onepage&q&f=false |isbn=0-8108-5013-3}}</ref> The credits of the original film say that the film is based on ''In a Glass Darkly''. This collection contains five tales, one of which is ''Carmilla''. Actually the film draws its central character, Allan Gray, from Le Fanu's Dr. Hesselius; and the scene in which Gray is buried alive is drawn from "The Room in the Dragon Volant".
* [[French people|French]] director [[Roger Vadim]]'s ''[[Blood and Roses|Et mourir de plaisir]]'' (literally ''And to die of pleasure'', but actually shown in the UK and US as ''Blood and Roses'', 1960) is based on ''Carmilla'' and is considered one of the greatest of the vampire genre. The Vadim film thoroughly explores the lesbian implications behind Carmilla's selection of victims, and boasts cinematography by [[Claude Renoir]]. The film's lesbian eroticism was however significantly cut for its US release. [[Mel Ferrer]] stars in the film.
* A more-or-less faithful adaptation starring [[Christopher Lee]] was produced in Italy in 1963 under the title ''[[La cripta e l'incubo]]'' (''Crypt of the Vampire'' in English). The character of Laura is played by Adriana Ambesi, who fears herself possessed by the spirit of a dead ancestor.
* The British [[Hammer Film Productions]] also produced a fairly faithful adaptation of ''Carmilla'' titled ''[[The Vampire Lovers]]'' (1970) with [[Ingrid Pitt]] in the lead role, [[Madeline Smith]] as her victim/lover, and Hammer's regular [[Peter Cushing]]. It is the first installment of the [[Karnstein Trilogy]].
* ''[[The Blood Spattered Bride]]'' (''La Novia Ensangrentada'') is a 1972 Spanish [[horror film]] written and directed by [[Vicente Aranda]], is based on the text. The film has reached cult status for its mix of horror, vampirism and seduction with lesbian overtones.
* ''Carmilla'' (1980) is a black-and-white made-for-television adaptation from Poland, starring singer [[Izabela Trojanowska]] in the title role and Monika Stefanowicz as Laura.
* The 2000 Japanese anime film ''[[Vampire Hunter D: Bloodlust]]'' features Carmilla "the Bloody Countess" as its primary antagonist. Having been slain by Dracula for her vain and gluttonous tyranny, Carmilla's ghost attempts to use the blood of a virgin to bring about her own resurrection. She was voiced by [[Julia Fletcher]] in English and Beverly Maeda in Japanese.
* In the direct-to-video movie, ''[[The Batman vs. Dracula]]'' (2005), Carmilla Karnstein is mentioned as [[Count Dracula]]'s bride, who had been incinerated by sunlight years ago. Dracula hoped to revive her by sacrificing [[Vicki Vale]]'s soul, but the ritual was stopped by the [[Batman]].
* The book is directly referenced several times in the 2011 film, ''[[The Moth Diaries (film)|The Moth Diaries]]'', the film version of [[Rachel Klein (novelist)|Rachel Klein's]] novel. There are conspicuous similarities between the characters in "Carmilla" and those in the film, and the book figures into the film's plot. 
* Carmilla is featured as the main antagonist in the movie ''[[Lesbian Vampire Killers]]'' (2009), a comedy starring [[Paul McGann]] and [[James Corden]], with [[Silvia Colloca]] as Carmilla.
* ''[[The Unwanted]]'' (2014) from writer/director Brent Wood relocates the story to the contemporary southern United States. 
* ''The Curse of Styria'' (2014), alternately titled ''Angels of Darkness'' is an adaptation of the novel set in late 1980s with [[Julia Pietrucha]] as Carmilla and [[Eleanor Tomlinson]] as Lara.<ref>{{cite web|url=http://m.imdb.com/title/tt1764614/ |author=IMDB |title= Styria (2014) |website=IMDB}}</ref>

===Music===

====Opera====
* A [[chamber opera]] version of ''Carmilla'' appeared in ''Carmilla: A Vampire Tale'' (1970), music by [[Ben Johnston (composer)|Ben Johnston]], script by [[Wilford Leach]]. Seated on a sofa, Laura and Carmilla recount the story retrospectively in song.<ref>{{cite news|last1=Rockwell|first1=John|title=Music: 'Carmilla: a Vampire Tale' at La Mama|url=https://www.nytimes.com/1986/10/05/arts/music-carmilla-a-vampire-tale-at-la-mama.html|accessdate=January 25, 2017|publisher=The New York Times|date=October 5, 1986}}</ref>

====Rock music====
(Alphabetical by group/artist)
* [[Jon English]] released a song named ''Carmilla'', inspired by the book, on his 1980 album ''Calm Before the Storm''.
* British [[extreme metal]] band [[Cradle of Filth]]'s lead singer [[Dani Filth]] has often cited [[Sheridan Le Fanu]] as an inspiration to his lyrics.{{citation needed|date=October 2015}}  For example, their EP, ''[[V Empire or Dark Faerytales in Phallustein]]'' (1994), includes a track titled "Queen of Winter, Throned", which contains the lyrics: "Iniquitous/I share Carmilla's mask/A gaunt mephitic voyeur/On the black side of the glass". Additionally, the album ''[[Dusk... and Her Embrace]]'' (1996) was largely inspired by ''Carmilla'' and Le Fanu's writings in general. The band has also recorded an instrumental track titled "Carmilla's Masque", and in the track "A Gothic Romance", the lyric "Portrait of the Dead Countess" may reference the portrait found in the novel of the Countess Mircalla.
* The [[Discipline (band)|Discipline]] band's 1993 album Push & Profit included a ten-minute song entitled Carmilla, based on Le Fanu's character. 
* The lyrics for "Blood and Roses", [[LaHost]]'s track on the [[EMI]] compilation album ''Fire in Harmony'' (1985), are loosely based on the [[Roger Vadim]] film version of ''Carmilla''.{{citation needed|date=October 2015}}
* The title track of  the album ''[[Symphonies of the Night]]'' (2013), by the German/Norwegian band [[Leaves' Eyes]], was inspired by ''Carmilla''.<ref>{{cite web|website=LeavesEyes.de|url=http://www.leaveseyes.de/the-stories-of-symphonies-of-the-night/|title=The Stories of ''Symphonies of the Night''}}</ref>
* [[Alessandro Nunziati]], better known as Lord Vampyr and the former vocalist of Theatres des Vampires, has a song named "Carmilla Whispers from the Grave" in his debut solo album, ''[[De Vampyrica Philosophia]]'' (2005).
* [[Theatres des Vampires]], an Italian [[extreme gothic metal]] band, has produced a video single called "Carmilla" for its album ''[[Moonlight Waltz]]''. They also reference the novel in innumerous other songs.{{citation needed|date=October 2015}}

===Periodicals===
* A Japanese lesbians' magazine is named after Carmilla, as Carmilla "draws hetero women into the world of love between women".<ref>{{cite journal|url=http://wwwsshe.murdoch.edu.au/intersections/issue12/welker2.html |title=Celebrating Lesbian Sexuality: An Interview with Inoue Meimy, Editor of Japanese Lesbian Erotic Lifestyle Magazine ''Carmilla'' |journal=Intersections|issue=12}}</ref>

===Radio===
* The ''[[Columbia Workshop]]'' presented an adaptation (CBS, July 28, 1940, 30 min.). [[Lucille Fletcher]]'s script, directed by Earle McGill, relocated the story to contemporary New York state and allows Carmilla ([[Jeanette Nolan]]) to claim her victim Helen ([[Joan Tetzel]]).<ref>{{Cite web|title = GENERIC RADIO WORKSHOP OTR SCRIPT: Columbia Workshop|url = http://www.genericradio.com/show.php?id=VXUQ0YE52|website = www.genericradio.com|accessdate = 2015-11-10}}</ref> 
* The character Dr. Hesselius is featured as an occult sleuth in the ''[[The Hall of Fantasy]]'' episode, "[[The Shadow People (radio drama)|The Shadow People]]" (September 5, 1952), broadcast on the [[Mutual Broadcasting Network]].   
* In 1975, the ''CBS Radio Mystery Theater'' broadcast an adaptation by [[Ian Martin (writer)|Ian Martin]] (CBS, July 31, 1975, rebroadcast December 10, 1975). [[Mercedes McCambridge]] played Laura Stanton, [[Marian Seldes]] played Carmilla.<ref>{{Cite book|title = The CBS Radio Mystery Theater: An Episode Guide and Handbook to Nine Years of Broadcasting, 1974–1982|url = https://books.google.com/books?id=wSqSCgAAQBAJ|publisher = McFarland|date = 2004-01-01|isbn = 9780786418909|first = Gordon|last = Payton|first2 = Martin Grams, Jr|last2 = Jr|page = 105}}</ref>  
* [[Vincent Price]] hosted an adaption (reset to 1922 Vienna) by Brainard Duffield, produced and directed by [[Fletcher Markle]], on the ''[[Sears Radio Theater]]'' (CBS, March 7, 1979), with [[Antoinette Bower]] and Anne Gibbon.<ref>{{Cite web|title = Sears Radio Theater|url = http://www.radiogoldindex.com/cgi-local/p2.cgi?ProgramName=Sears+Radio+Theatre|website = www.radiogoldindex.com|accessdate = 2015-11-12}}</ref><ref>{{Cite book|title = Horror Stars on Radio: The Broadcast Histories of 29 Chilling Hollywood Voices|url = https://books.google.com/books?id=1_-EtQwxAP4C&pg=PA82&lpg=PA82&dq=antoinette+bower+carmilla&source=bl&ots=eqVNC0lOzJ&sig=TIiTHY9FW7xOL7wHhlyM4lms_nY&hl=en&sa=X&ved=0CC4Q6AEwBmoVChMI2dvu6r-LyQIVRetjCh1ksg5l#v=onepage&q=antoinette%2520bower%2520carmilla&f=false|publisher = McFarland|date = 2010-01-11|isbn = 9780786457298|first = Ronald L.|last = Smith|page = 82}}</ref>  
* On November 20, 1981, the [[CBC Radio]] series ''[[Nightfall (CBC)|Nightfall]]'' aired an adaptation of ''Carmilla'' written by [[Graham Pomeroy]] and [[John Gavin Douglas|John Douglas]].
* [[BBC Radio 4]] broadcast Don McCamphill's ''[[Afternoon Play]]'' dramatization June 5, 2003, with [[Anne-Marie Duff]] as Laura, [[Brana Bajic]] as Carmilla.<ref>{{Cite web|title = Afternoon Play: Carmilla - BBC Radio 4 FM - 5 June 2003 - BBC Genome|url = http://genome.ch.bbc.co.uk/20cba3e633dd4eaa92586f059b61cc4f|website = genome.ch.bbc.co.uk|accessdate = 2015-11-14}}</ref>

===Stage===
(Chronological) 
* In [[Elfriede Jelinek]]'s play ''[[Illness or Modern Women]]'' (1984), a woman, Emily, transforms another woman, Carmilla, into a vampire, and both become lesbians and join together to drink the blood of children.
* A [[German language]] adaptation of ''Carmilla'' by Friedhelm Schneidewind, from Studio-Theatre Saarbruecken, toured [[Germany]] and other European countries (including [[Romania]]) from April 1994 until 2000.<ref>[http://www.carmilla.de/ www.carmilla.de]</ref>
* The [[Theatre in Chicago#Itinerant theater companies|Wildclaw Theater]] in [[Chicago]] performed a full-length adaptation of ''Carmilla'' by Aly Renee Amidei in January and February 2011.<ref>{{cite news|url=http://chicagotheaterbeat.com/2011/01/18/carmilla-review-wildclaw-theatre-chicago/ |work=Chicago Theater Beat|date=January 18, 2011|title=''Carmilla'' Review, Wildclaw Theatre Chicago}}</ref>
* Zombie Joe's Underground Theater Group in [[North Hollywood]] performed an hour-long adaptation of ''Carmilla'', by David MacDowell Blue, in February and March 2014.<ref>{{cite web|url=http://losangeles.bitter-lemons.com/2014/03/03/carmilla-zombies-joes-underground-83-sweet/ |title=Carmilla: Zombie Joe's Underground Theater Group|website=Los Angeles Bitter Lemons|date=March 3, 2014}}</ref>
* Carmilla was also show cased at Cayuga Community College in Auburn, New York by Harelquin Productions with Meg Owren playing Laura and Dominique Baker-Lanning playing Carmilla.
* The David MacDowell Blue adaptation of ''Carmilla'' was performed by The Reedy Point Players of [[Delaware City]] in October 2016.<ref>[http://www.thereedypointplayers.com/about-us/past-shows The Reedy Point Players - Past Shows]</ref> This production was directed by Sean McGuire, produced by Gail Springer Wagner, assistant director Sarah Hammond, technical director Kevin Meinhaldt and technical execution by Aniela Meinhaldt. The performance  featured Mariza Esperanza, Shamma Casson and Jada Bennett with appearances by Wade Finner, David Fullerton, Fran Lazartic, Nicole Peters Peirce, Gina Olkowski and Kevin Swed.

===Television===
(Alphabetical by series title)

* The ''[[Doctor Who]]'' serial arc ''[[State of Decay]]'' (1980) features a vampire named Camilla (not Carmilla) who, in a brief but explicit moment, finds much to "admire" in the Doctor's female travelling companion Romana, who finds she has to turn away from the vampire's intense gaze.
* A television version for the British series ''Mystery and Imagination'' was transmitted on 12 November 1966. [[Jane Merrow]] played the title role, [[Natasha Pyne]] her victim.
* In 1989, [[Gabrielle Beaumont]] directed Jonathan Furst's adaptation of ''Carmilla'' as an episode of the [[Showtime (TV network)|Showtime]] television series ''[[Nightmare Classics]]'', featuring [[Meg Tilly]] as the vampire and [[Ione Skye]] as her victim Marie. Furst relocated the story to an American [[History of the Southern United States|antebellum]] southern [[Plantations in the American South|plantation]].<ref>{{Cite web|title = Carmilla - Trailer - Cast - Showtimes - NYTimes.com|url = https://www.nytimes.com/movies/movie/8295/Carmilla/overview|website = www.nytimes.com|accessdate = 2015-11-10}}</ref>
* In episode 36 of ''[[The Return of Ultraman]]'', the monster of the week in the episode, Draculas, originates from a planet named Carmilla. He possesses the corpse of a woman as his human disguise.
* In season 2, episodes 5 and 6 of the HBO TV series ''[[True Blood]]'', Hotel Carmilla, in [[Dallas, Texas]], has been built for vampires. It features heavy-shaded rooms and provides room service of human "snacks" for their vampire clientele, who can order specific blood types and genders.

===Web series===
* ''[[Carmilla (series)|Carmilla]]'' is a [[web series]] on [[YouTube]] starring [[Natasha Negovanlis]] as Carmilla and [[Elise Bauman]] as Laura. First released on August 19, 2014, it is a comedic, modern adaptation of the novella which takes place at a modern-day university, where both girls are students. They become roommates after Laura's first roommate mysteriously disappears and Carmilla moves in, taking her place.

===Videogames===
* The vampiress Carmilla is an antagonist in the [[Castlevania]] series. She is a key figure in the classic "Castlevania II: Simon's Quest" in which she tries to resurrect Lord Dracula. In the time distorted fighting game "Castlevania: Judgement" she is a playable character battling to protect her master, and in [[Hideo Kojima]]'s "Lords Of Shadow" reimagining she is a recurring boss and former leader of the heroic Brotherhood Of Light. In every game she is portrayed as having great admiration for Dracula that borders on obsessive devotion.
* In the Japanese action game series "[[OneeChanbara]]" Carmilla is the matriarch of the Vampiric clan. She appears in the 2011 title "Oneechanbara Z ~ Kagura ~" as the manipulator & main antagonist of sister heroines Kagura and Saaya, first using them to attack her rivals before trying (and failing) to eliminate them as pawns.
* The main antagonist in ''[[Ace Combat Infinity]]'' is a mysterious girl only known as the "Butterfly Master", who pilots the fictional QFA-44 "Carmilla" aircraft. The Butterfly Master herself is situated in a low-orbit satellite, controlling the Carmilla aircraft through a "COnnection For Flight INterface", or "COFFIN".

==See also==
{{portal|Novels}}
* [[LGBT themes in speculative fiction|Homosexuality in speculative fiction]]

==References==
{{Reflist|30em}}

==External links==
{{Wikisource}}
{{Commons category}}
* {{gutenberg|no=10007|name=Carmilla}}
* {{imdb character|0060845}}
* {{cite book|url=http://www.inkitt.com/stories/10565 |title=Carmilla|edition= Mobile|publisher=Inkitt.com}}
* {{librivox book | title=Carmilla | author=Joseph Sheridan LE FANU}}

{{Authority control}}

[[Category:Gothic novels]]
[[Category:Novels with lesbian themes]]
[[Category:Novels by Sheridan Le Fanu]]
[[Category:Vampire novels]]
[[Category:1870s fantasy novels]]
[[Category:1872 novels]]
[[Category:LGBT speculative fiction novels]]
[[Category:Irish novels adapted into films]]
[[Category:Novels adapted into television programs]]