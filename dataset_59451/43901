{{Advert|date=August 2016}}
{{Infobox company
|name     = Worldwide Aeros Corporation
|logo     = File:Worldwide_Aeros_Corp_logo.png
|type     = Airship Manufacturer
|genre            = 
|foundation       = 1987<ref>[http://www.janes.com/extracts/extract/jawa/jawa1562.html Janes guide to Aeros Corp]</ref>
|founder          =Igor Pasternak <small>([[Chairman]], [[President#Presidents in business|President]] & [[Chief Executive Officer|CEO]])</small> 
|location_city    = [[Montebello, California]]
|location_country = USA
|location         = <!-- this parameter modifies "Headquarters" -->
|key_people       =Igor Pasternak 
|area_served      =Worldwide 
|industry         = Aerospace, Aviation, Defense and Advanced Technologies
|products         =Aeroscraft,<br>
Sky Dragon Airship, Tactical Aerostats, Early Warning System
|revenue          = 
|operating_income = 
|net_income       = 
|num_employees    = 
|parent           = 
|divisions        =Aeroscraft Corporation
North American Defense Advanced Technology Solutions (NADATS)
|subsid           = 
|owner            = 
|slogan   ="Innovation never stops" 
|homepage         = http://www.aeroscraft.com
|dissolved        = 
|footnotes        = 
}}
'''Worldwide Aeros Corp''' is an American manufacturer of [[airship]]s based in [[Montebello, California]]. It was founded in 1992 by the current CEO and Chief Engineer, Igor Pasternak, who came to America from Ukraine. It currently employs more than 100 workers.

The company's current products are [[blimp|non-rigids]] aimed at both the military and commercial markets, including transport, [[surveillance]], [[broadcasting]] and [[advertising]]. The company's best-selling ship is called the Sky Dragon.<ref name=":4">{{Cite news|url=http://www.newyorker.com/magazine/2016/02/29/a-new-generation-of-airships-is-born|title=Helium Dreams|last=Laskas|first=Jeanne Marie|date=2016-02-29|newspaper=The New Yorker|issn=0028-792X|access-date=2016-03-08}}</ref>

Worldwide Aeros Corp. is also developing what it calls the Aeroscraft, a rigid airship with a number of innovative features, the most important of which is a method of controlling the airship's static lift, which can be reduced by pumping helium from the internal gasbags and storing it under pressure: conversely lift can be increased by reinflating the gasbags using the stored gas.<ref name=":0">{{cite web|url=http://aviationweek.com/awin/pelican-demonstrator-aimed-airlift|title=Pelican Demonstrator Aimed At Airlift|website=Aviationweek.com|author=Sweetman,Bill|date=15 October 2012|accessdate=16 July 2014}}</ref>
The company has received $60 million from the U.S. Department of Defense to develop the concept,<ref name=":1">{{cite web|url=http://www.businessweek.com/articles/2013-06-13/worldwide-aeros-aims-to-turn-blimps-into-cargo-craft|title=Worldwide Aeros Aims to Turn Blimps Into Cargo Craft|date=13 June 2013|publisher=Bloomberg Businessweek  |accessdate=17 June 2014}}</ref> resulting in a prototype named [[Dragon Dream]] which underwent systems tests and some tethered flights in late 2013. This prototype was subsequently damaged when part of the roof of the hangar at the [[Marine Corps Air Station Tustin|former Marine Corps Air Station in Tustin]], in which it was constructed, collapsed on 7 October 2013.<ref name=":2">{{cite web|url=http://www.ocregister.com/articles/base-529815-hangar-inside.html|publisher=Orange County Register|title=Roof failure at Tustin base damages airship|accessdate=17 June 2014}}</ref>

==History==
Igor Pasternak founded a design bureau in USSR in 1981 at the [[Lviv Polytechnic University]]. The private company Aeros was created in 1986 as a result of [[Perestroika|Soviet reforms]] and started producing [[moored balloon]]s. It moved to the USA in 1992.

After the [[September 11 attacks|September 11th terrorist attacks]], the company shifted its focus from advertising to surveillance, as its large ships can hold 1,000 pounds of radar-surveillance equipment. The blimps have such varied uses as monitoring oil pipelines in Mexico to performing surveillance for the Ukrainian government along the Russian border.<ref name=":4" />

In 2005, Aeros was granted the largest contract under [[DARPA]]'s project [[Walrus HULA]]. Project Walrus was not renewed in 2010.<ref name="PopSci">{{cite web |url=http://www.popsci.com/technology/article/2012-06/plan-airships-might-finally-take |title=A Plan For Airships That Might Finally Take Off |first=Josh|last=Bearman |date=July 2, 2012 |website=popsci.com |publisher=Popular Science |accessdate=16 July 2013}}</ref> However, the Pentagon continued to fund Aeros through the Rapid Reaction Technology Office, contracting with them in 2010 to build a prototype that could demonstrate key technologies.<ref name="AviationWeekOct">{{cite web |url=http://www.aviationweek.com/Article.aspx?id=/article-xml/AW_10_15_2012_p46-504677.xml |title=Pelican Demonstrator Aimed At Airlift |first=Bill|last=Sweetman |date=October 15, 2012 |website=aviationweek.com |publisher=Aviation Week |accessdate=16 July 2013}}</ref> This prototype became known as the "Pelican."

[[The Pentagon]] has provided $50 million in funding for the development of the "Pelican" prototype.<ref name="Wired">{{cite web |url=http://www.wired.co.uk/magazine/archive/2013/01/start/blimps-retake-the-sky |title=The Aeroscraft airship could change the very concept of flying |first=Madhumita|last=Venkataramanan |date=January 11, 2013 |website=wired.co.uk |publisher=Wired Magazine |accessdate=16 July 2013}}</ref>

===Corporate timeline===

* '''1981 -''' Igor Pasternak establishes a volunteer airship design bureau at [[Lviv Polytechnic|Lviv Polytechnic University]] in USSR.
* '''1986 -''' Aeros becomes one of the first private aerospace and engineering companies permitted under Gorbachev's Perestroika reforms.
* '''1987 -''' Aeros launches a production line of tethered aerostats and begins worldwide deliveries.
* '''1989 -''' Aeros begins the research and development on heavy lift airship concept.
* '''1991 -''' Aeros begins its expansion and opens its first international office in Sofia, Bulgaria.
* '''1994 -''' Aeros relocates to the United States to pursue its dream to advance "lighter than air" technologies.
* '''1995 -''' Aeros develops its first airship, the Aeros 50' that marks start of the Aeros airship excellence.
* '''1997 -''' Aeros develops the Aeros 25M tethered aerostat system for the U.S. Department of Defense.
* '''1999 -''' Aeros launches airship model Aeros 40A. 
* '''2000 -''' Aeros develops the 40B "Sky Dragon" airship equipped with a fly-by-wire system, and receives an unrestricted type certification to fly worldwide over populated areas.
* '''2001 -''' Aeros 40B is delivered to Germany. Aeros 18M was produced for the Canadian market.
* '''2003 -''' Aeros receives the contract award from the U.S. Department of Defense for the development of a high altitude airship.
* '''2004 -''' Aeros delivers the 1170 tethered aerostat for the U.S. military. For the first time, Aeros creates a new type of rigid variable buoyancy air vehicle called the Aeroscraft.
* '''2005 -''' The Pentagon’s Defense Advanced Research Projects Agency (DARPA) contracts Aeros to develop a buoyancy assisted strategic airlifter.
* '''2006 -''' For the first time ever, Aeros demonstrates a new technology that allows buoyant vehicles to operate without ballast.
* '''2007 -''' Aeros develops a new 40D "Sky Dragon" airship that receives an FAA type certificate.
* '''2008 -''' The United States Federal Aviation Administration recognizes Aeros’ product quality and awards a Production Certificate. For the 1st time in aviation history, Aeros successfully demonstrates the ability to control buoyancy in flight.
* '''2009 -''' Aeros successfully demonstrates the rigid aero-structure that allows buoyancy assisted air vehicles to have greater military utility. Aeros begins construction of the Aeroscraft.
* '''2010 -''' Aeros develops a new advanced surveillance tethered aerostat system, in support of the Republic of Korea Armed Forces. Aeros puts its focus on the technology development for the Wind Energy Industry to achieve 10MW power output.
* '''2011 -''' Aeros continues integration of the Aeroscraft.
* '''2012 -''' Aeros deliveres on time and under budget on a $35 million contract with the DOD and DARPA, proving all of the technologies and capabilities of the Aeroscraft.
* '''2013 -''' The Dragon Dream had its first float on January 3. The Pentagon declared the tests were a success. On July 4, the Dragon Dream rolled out of the hangar for the first time and on September 11, the first flight of the Dragon Dream occurred.
* '''2014 -''' Aeros launches 40D 'Sky Dragon' S/N 22 into service following Congressional christening ceremony; completes 'design freeze' for ML 866 (66-ton) Aeroscraft, initiates production of new 40E 'Sky Dragon' airship; develops and deploys a new tactical aerostat design.
* '''2015 -''' Aeros receives patent from USPTO for COSH buoyancy management system; launches the North American Defense Advanced Technology Solutions (NADATS) division, readies launch of advanced 40E ‘Sky Dragon’ airship, and continues production on the world’s first cargo airship, the Aeroscraft.<ref name="aeroscraft.com">http://aeroscraft.com/history/4575665539</ref>

==Aeroscraft ==
[[File:Dragon Dream.jpg|left|thumb|Dragon Dream outside Tustin hangar]]
The Aeroscraft is a heavy-lift, variable-buoyancy cargo airship featuring an onboard buoyancy management system, rigid structure, vertical takeoff and landing performance, and operational abilities at low speed, in hover, and from unprepared surfaces.

Worldwide Aeros Corp. is also developing what it calls the "Aeroscraft", a rigid airship with a number of innovative features, the most important of which is a method of controlling the airship's static lift, which can be reduced by pumping helium from the internal gasbags and storing it under pressure: conversely lift can be increased by reinflating the gasbags using the stored gas.<ref name=":02">{{cite web|url = http://aviationweek.com/awin/pelican-demonstrator-aimed-airlift|title = Pelican Demonstrator Aimed At Airlift|website = Aviationweek.com|author = Sweetman,Bill|date = 15 October 2012|accessdate = 16 July 2014}}</ref>

This prototype was subsequently damaged when part of the roof of the hangar at the [[Marine Corps Air Station Tustin|former Marine Corps Air Station in Tustin]] in which it was constructed collapsed on 7 October 2013.<ref name=":22">{{cite web|url = http://www.ocregister.com/articles/base-529815-hangar-inside.html|publisher = Orange County Register|title = Roof failure at Tustin base damages airship|accessdate = 17 June 2014}}</ref>

===Project Pelican and Dragon Dream===
[[File:Dragon_Dream_experimental_cargo_air_ship.jpg|thumb|Half-scale prototype "[[Dragon Dream]]"]]'''Project Pelican''' was a US government-funded project to build and test a half-scale prototype of the proposed full-size Aeroscraft, using representative structure and avionics.<ref name="GizMag">{{cite web |url = http://www.gizmag.com/pasternak-aeroscraft-aeros/25425/|title = Interview: Aeros CEO Igor Pasternak|first = Leon|last = Gettler|date = December 12, 2012|website = gizmag.com|publisher = GizMag|accessdate = 15 July 2013}}</ref> Named '''[[Dragon Dream]]''' and having a length of {{convert|266|ft|m}} and design speed of {{convert|60|kn|km/h}}, it does not carry a payload.<ref name="AerosFleet">{{cite web|url = http://www.aeroscraft.com/fleet/4576270098|title = FLEET - Aeroscraft|publisher = Aeros|accessdate = 15 Jul 2013}}</ref> The company received $60 million from the U.S. Department of Defense to develop the concept,<ref name=":12">{{cite web|url = http://www.businessweek.com/articles/2013-06-13/worldwide-aeros-aims-to-turn-blimps-into-cargo-craft|title = Worldwide Aeros Aims to Turn Blimps Into Cargo Craft|date = 13 June 2013|publisher = Bloomberg Businessweek|accessdate = 17 June 2014}}</ref> resulting in a prototype named [[Dragon Dream]] which underwent systems tests and some tethered flights in late 2013. The flight of the 'Dragon Dream. With funding from the U.S. Department of Defense, the first floating took place on January 3, 2013 at [[Tustin, California]], where it hovered indoors at a height of 12 feet for several minutes.<ref>[http://militarytimes.com/news/2013/01/ap-high-tech-cargo-ship-being-built-california-013013/ High-tech cargo airship undergoing tests] - Militarytimes.com, January 30, 2013</ref> The Pentagon has declared that the tests of the Pelican were a "success", with the craft meeting its demonstration objectives.<ref>"DOD: Rigid-Hull Hybrid Air Vehicle Technology Demo Achieved Objectives." InsideDefense.com. 3 July 2013. Accessed 15 July 2013. http://aeroscorp.com/download/i/mark_dl/u/4011780344/4595063755/Inside%20Defense.pdf</ref> The Pelican was rolled out of its hangar on July 4, 2013.<ref name="BlimpInfo">{{cite web |url = http://www.blimpinfo.com/uncategorized/aeroscrafts-project-pelican-demonstrator-seen-outside-hangar/|title = Aeroscraft’s Project Pelican Demonstrator moves outside hangar|author = The Lighter Than Air Society|date = July 15, 2013|website = blimpinfo.com|accessdate = 16 July 2013}}</ref><ref name="Aviation Week">{{cite web|url = http://www.aviationweek.com/blogs.aspx?plckblogid=blog:27ec4a53-dcc8-42d0-bd3a-01329aef79a7&plckpostid=blog%3A27ec4a53-dcc8-42d0-bd3a-01329aef79a7post%3A119fbb0b-5c10-4b47-86d9-848a10cc6032|title = Aeros Tests Pelican Variable-Buoyancy Airship|date = 3 Jan 2013|publisher = Aviation Week|accessdate = 15 Jul 2013}}</ref> The company sued the Navy for $65 million in 2015 after the hangar structure fell on the aircraft.<ref name="avw2015-03">{{cite web |first = Russ|last = Niles|url = http://www.avweb.com/avwebflash/news/Airship-Maker-Suing-Over-Hangar-Collapse-223699-1.html|title = Airship Maker Suing Over Hangar Collapse|work = avweb.com|agency = Aviation Publishing Group|date = 15 March 2015|accessdate = 19 March 2015}}</ref>

===Planned full-scale craft===
The company is beginning production of two examples, an ML866 and an ML868 model.<ref>http://www.aerospace-technology.com/projects/aeroscraft-ml866-rigid-variable-buoyancy-air-vehicle-us/{{Unreliable source?|reason=domain on WP:BLACKLIST|date=June 2016}}</ref> A model capable of lifting 500 tons, the ML86X, is also proposed.<ref name="AerosFleet"/>

The ML866 model will be {{convert|555|ft|m}} in length, have a payload capacity of 66 tons, a top speed of 120 [[Knot (unit)|knots]] (222&nbsp;km/h), a range of {{convert|3100|nmi|km|abbr = on}}, and an [[Ceiling (aeronautics)|altitude ceiling]] of {{convert|12000|ft|m|abbr = on}}. The larger ML868 model will be {{convert|770|ft|m}} in length and carry 250 tons, with the same speed and altitude ceiling as the ML866.<ref name="AerosFleet" /> The company ultimately plans to build a ML86X with a length of 920 feet, a height of 215 feet, and a width of 355 feet, with the capacity to carry 500 tons.<ref name=":4" />

Aeros is currently seeking $3 billion to fund the construction of 24 Aeroscraft vehicles, including the 250-ton capacity ML868 model.<ref name="BusinessWeek">{{cite web |url = http://www.businessweek.com/articles/2013-06-13/worldwide-aeros-aims-to-turn-blimps-into-cargo-craft|title = Worldwide Aeros Aims to Turn Blimps Into Cargo Craft|first = Nick|last = Taborek|date = June 13, 2013|website = businessweek.com|publisher = Bloomberg Businessweek|accessdate = 16 July 2013}}
</ref> The CEO has stated that he aims to have a global fleet operating by 2023.<ref name=":4" />

===Capabilities ===

====Vertical takeoff and landing (VTOL) ====
Another way to understand the Aeroscraft’s VTOL capability is to compare the Aeroscraft to a submarine. For example, when a submarine needs to dive into the water, it takes on water to make it heavier. When the submarine needs to surface, it releases that water to become lighter. Similarly, the Aeroscraft can control its weight by releasing and taking on air, controlling the heaviness or lightness of the vehicle.

One obstacle that conventional and hybrid airships face is their inability to control buoyancy without venting helium. Another operational challenge faced by airships and hybrid airships is the inability to control or adjust static lift during operations. Traditional airships' requirement for external ballast exchange and existing ground infrastructure has significantly limited their cargo utility. Once the cargo is off-loaded, traditional helium-dependent airships become extremely light, and this static lift causes them to float away. To combat these forces, they require external ballast exchange, using rocks, ice, water, or lead bags, to keep the airship anchored to the ground.

Because the Aeroscraft is equipped with VTOL capability, it can deliver cargo directly from point-of-origin to point-of-need. Furthermore, other hybrid airships are runway dependent at higher operating weights, but the Aeroscraft does not need a runway, even at full payload. Because of its COSH technology, its computer-controlled virtually-automated directional thrust and station-keeping technology facilitates off and on-loading stores while in hover.<ref name=":3">{{Cite web|title = Capabilities copy - Aeros|url = http://aeroscraft.com/capabilities-copy/4580476906|website = aeroscraft.com|accessdate = 2015-10-27}}</ref>

====Oversized cargo bay ====
The cargo bay is located at the bottom of the aircraft cavity and is loaded by using a pulley system to load the cargo from the ground. At 1.8 million cubic feet, the cargo bay of the largest Aeroscraft design is much larger than that of any existing commercial freight aircraft (including the Boeing 747-8F and the Antonov 124 aircraft).<ref name=":3" />

===Design===
The Aeroscraft is a [[rigid airship]], having an internal structure to maintain its shape. As such it can reach otherwise difficult or inaccessible locations and can hover indefinitely at zero airspeed and with a full payload on board.<ref name="AerosTechnology">{{cite web |url = http://www.aeroscraft.com/technology/4576266262|title = Technology - Aeroscraft|publisher = Aeros|accessdate = 15 July 2013}}</ref> The design incorporates cargo bays that are larger than any current air, truck or rail transport, while the payload capacity is significantly more than the current 16-ton maximum for helicopters.<ref name="AerosFleet" /><ref>http://www.autoevolution.com/news/the-largest-transport-helicopters-in-the-world-24549.html</ref>

Propulsion is provided by conventional propellers, and in addition the Aeroscraft design has six downward-pointing [[turbofan]] jet engines that assist in [[VTOL|vertical take-off and landing]].<ref name="howstuffworks" /> These turbofans, together with the Aeros "COSH" buoyancy control system, make the Aeroscraft capable of taking off and landing vertically without the need for a [[runway]], a ground crew, or external [[ballast]].<ref name="CBS">{{cite web |url = http://www.cbsnews.com/8301-205_162-57566714/aeroscraft-the-hi-tech-half-blimp-half-hovercraft/|title = Aeroscraft: The hi-tech half blimp, half hovercraft|author = Jae C. Hong|date = January 30, 2013|website = cbsnews.com|publisher = CBS/AP|accessdate = 16 July 2013}}</ref>

Like any airship, the Aeroscraft may be used to transport cargo to remote or difficult locations and to hover over uneven terrain, in both civil and military use.<ref name="GizMag"/><ref>[http://www.armedforces-int.com/news/battlefield-cargo-airship-nears-first-flight.html Battlefield Cargo Airship Nears First Flight] - Armedforces-Int.com, January 7, 2013</ref> Its operational independence from runways, ground crews or other infrastructure makes it especially suited to military and emergency relief operations. The large cargo bays would allow [[outsize cargo]] such as wind turbines or large aerospace parts to be stored internally.

The manufacturer also envisions the delivery of large amounts of commercial merchandise from a centralized location.<ref>Tompkins, Joshua. [http://www.popsci.com/popsci/whatsnew/18ac893302839010vgnvcm1000004eecbccdrcrd.html The Flying Luxury Hotel]. ''Popular Science''. Accessed on 15-10-2007.</ref>

===Technology ===

====Control of static heaviness (COSH)====

Aeros has developed a technology to avoid the need for ballast, which they call "control of static heaviness (COSH)". The main gas bag is inflated with helium to create lift for takeoff, then on landing some of the gas is re-compressed into a storage tank to partially deflate the gas bag and reduce lift.<ref name="AerosTechnology" /><ref name="howstuffworks">{{cite web
|url = http://science.howstuffworks.com/aeroscraft.htm|title = How the Aeroscraft Will Work|accessdate = 2007-05-15|last = Grabianowski|first = Ed|work = [[How Stuff Works]]|archiveurl = https://web.archive.org/web/20070519191859/http://science.howstuffworks.com/aeroscraft.htm|archivedate = 19 May 2007 <!--DASHBot-->|deadurl = no}}</ref><ref>http://aviationspaceindia.com/content/aeroscraft</ref>

The COSH system also avoids the need to re-ballast when taking on or dropping off payload, and consequently also the associated ground crew.

Worldwide Aeros was awarded a patent of their control of static heaviness system (COSH) internal buoyancy management system in May 2015. The COSH system internally ballasts the non-flammable helium into the aircraft’s helium pressure envelopes (HPEs), helping the vehicle manage buoyant lift. The HPE units contain and control the compressed helium and allow the overall helium volume envelope to be reduced or increased, enabling the air vehicle to become heavy or buoyant in a controlled manner. The compression of helium into the HPE’s creates a negative pressure within the Aeroscraft Aeroshell, permitting air-expansion chambers to fill with readily-available environmental ballast (air), which acts in concert with reduced Helium static lift to make the Aeroscraft heavier, when desired, to compensate for adjustments in payload.<ref name="Technology copy - Aeros">{{Cite web|title = Technology copy - Aeros|url = http://aeroscraft.com/technology-copy/4580412172|website = aeroscraft.com|accessdate = 2015-10-27}}</ref>

====Ceiling suspension cargo deployment system ====

The Aeroscraft’s cargo system provides the aircraft with unmatched volume and flexibility when deploying cargo to virtually any point on the planet, empowering the aircraft to pick-up and off-load cargo in more efficient ways, even from hover. The internal cargo handling system has been designed to facilitate cargo loading, sorting, and unloading in a more innovative and efficient manner, overcoming pre-deployment requirements for ground handling cargo equipment in austere environments. The system affixes containers and cargo pallets to rails in the fuselage ceiling, rather than on the floor; adjusts cargo positioning to accommodate changes in center of gravity, such as when other cargo is loaded and unloaded; facilitates access to any piece of cargo at any time, eliminating unneeded cargo movements and reducing ground time; and eliminates labor costs with traditional cargo handling and weight-and-balance requirements.<ref name="Technology copy - Aeros" />

====Rigid structure ====

The Aeroscraft is the only rigid structure variable buoyancy air vehicle of its kind.

Its rigid structure is made from ultra-light aluminium and carbon fiber materials. It consists of transverse bulkheads which are connected to longitudinal members. It is reinforced with high strength composite tensioned cables.

This rigid structure provides an excellent range of hard points for mounting engines, canards, cockpit, propulsion systems, and other auxiliary systems both inside and outside of the hull.<ref name="Technology copy - Aeros" />

====Landing system====
Replacing the more familiar landing gear found on airplanes, the Aeroscraft is equipped with landing cushions that aid the vehicles unique functionality. They aid landing on unimproved surfaces, even water, and perform like a hovercraft during taxi by pushing air through them. In addition, the landing cushions are equipped with very powerful gripping/suction capabilities that ensure the vehicle stays grounded and in place when not in flight. This reversible airflow helps hold the vehicle to the ground, even when the Aeroscraft arrives at destination for cargo offloading, allowing it to operate in heavier wind conditions.<ref name="Technology copy - Aeros" />

====Vectored thrust engines====
The Aeroscraft is equipped with vectored thrust engines that rotate and allow maneuverability. In addition to aiding helicopter-like vertical take-off and landing capability, the vectored thrust propels the vehicle in forward flight and aids the vehicle with ground-based taxiing.<ref name="Technology copy - Aeros" />

=====Low speed control (LSC) =====

When in forward flight, the Aeroscraft is controlled by the aerodynamic control surfaces (vertical stabilizers, empennages and canards); however, the low speed control system aids the pilot in lower wind conditions such as during VTOL and hover. The LSC system acts as a rear thruster to propel the vehicle in forward flight, and permits the thrust to be redirected while in hover to help the vehicle maintain desired positioning and orientation.<ref name="Technology copy - Aeros" />

==Fleet==
 
{| class="wikitable"
|-
!  !! Pelican !! ML 866 !! ML 868 !! ML 86X
|-
| Payload || 0 tons || 66 tons || 250 tons || 500 tons
|-
| Length || 266&nbsp;ft || 555&nbsp;ft || 770&nbsp;ft || 920&nbsp;ft
|-
| Wingspan || 96&nbsp;ft || 177&nbsp;ft || 296&nbsp;ft || 355&nbsp;ft
|-
| Height || 51&nbsp;ft || 120&nbsp;ft || 183&nbsp;ft || 215&nbsp;ft
|-
| Max speed || 60 knots || 120 knots || 120 knots || 120 knots
|-
| Cruise speed || 40 knots || 100 knots || 100 knots || 100 knots
|-
| Range || n/a  || 3,100&nbsp;nm || 5,100&nbsp;nm || 5,100&nbsp;nm
|-
| Altitude ceiling || 9,800&nbsp;ft || 12,000&nbsp;ft || 12,000&nbsp;ft || 12,000&nbsp;ft
|-
| Cargo dimensions || n/a || 220&nbsp;ft x 40 x 30 || 380&nbsp;ft x 61 x 45 || 455&nbsp;ft x 74 x 54
|}
<ref>http://aeroscraft.com/fleet-copy/4580475518</ref>

== Airships ==

'''Aeros 50:''' Aeros developed its first airships, the ‘Aeros 50'

'''40A Sky Dragon:''' Aeros launched airship model ’40A Sky Dragon,’ with increased payload and capabilities

'''40B Sky Dragon:''' Aeros developed the ’40B Sky Dragon’ equipped with a fly-by-wire system, and received FAA type certification

'''40D Sky Dragon:''' Aeros launched ’40D Sky Dragon,’ to global operations and receives type certification from{{Clarify|date=June 2016}} the FAA

'''Aeroscraft:''' Aeros’ successful technology demonstration vehicle for the 66-ton Aeroscraft, nicknamed ‘Dragon Dream’ takes flight. Called RAVB as part of its development under Project Pelican for the Department of Defense

== NADATS (North American Defense Advanced Technology Solutions) ==
NADATS is a division of Worldwide Aeros Corp which serves operators with elevated early warning systems, tactical ISR solutions and custom, mission-specific equipment for actionable intelligence in hostile environments, for border and coast guard, for facility and event security, and for related missions.<ref>http://nadats.com/home/</ref>

===Elevated early warning systems===

<ref>http://nadats.com/home/?page_id=529</ref>

===Sky Dragon airship surveillance system===
The Sky Dragon is a series of airship that have been used anywhere from military, surveillance and commercial use. The first Sky Dragon 40A was completed in 1999. Following the 40A, the 40B was delivered to Germany in 1997. In 2007, Aeros Corp developed the 40D recognized as the worldwide ‘system of choice’, which received a FAA type certification. The 40D was later delivered to Mexico in 2014.<ref name="aeroscraft.com"/>
<ref>http://nadats.com/home/?page_id=506</ref>

Aeros’ contemporary 40E ‘Sky Dragon’ airship has a useful payload greater than one ton, range exceeding 500&nbsp;km, 24-hour mission duration, and can observe up to 70,000 square kilometers at a time.

===Multi-payload tethered aerostats===

===Tactical towers===

<ref>http://nadats.com/home/?page_id=535</ref>

===Tactical aerostat===

<ref>http://nadats.com/home/?page_id=537</ref>

==See also==
* [[Cargolifter AG]]
* [[ESTOLAS]]
* [[P-791]]
* [[WALRUS HULA]]
* [[LEMV]]

==References==
{{Reflist|40em}}

==External links==
* [http://www.aeroscraft.com Worldwide Aeros website]
* [http://www.aerosml.com/ Worldwide Aeros Corporation official site]
* [http://www.popsci.com/popsci/whatsnew/18ac893302839010vgnvcm1000004eecbccdrcrd.html Popular Science article]
* [http://www.aeroscraft.com/ aeroscraft.com]
* [https://www.youtube.com/user/WorldwideAeros?feature=watch Aeroscraft's "WorldwideAeros" Youtube channel.]
* [http://www.wired.co.uk/magazine/archive/2013/01/start/blimps-retake-the-sky "The Aeroscraft airship could change the very concept of flying", ''Wired'']

{{Emerging technologies}}

[[Category:Companies based in Los Angeles County, California]]
[[Category:Defense companies of the United States]]
[[Category:Aircraft manufacturers of the United States]]
[[Category:Montebello, California]]
[[Category:Airships of the United States]]
[[Category:Commercial item transport and distribution]]
[[Category:Cargo aircraft]]
[[Category:Emerging technologies]]

[[de:Aeros Corporation]]