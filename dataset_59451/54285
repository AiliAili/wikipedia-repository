{{Use dmy dates|date=October 2011}}
{{Use Australian English|date=October 2011}}
{{Infobox lake
| name = Happy Valley Reservoir
| image = Happy_Valley_Reservoir_20070223.jpg
| caption = Aerial view of Happy Valley Reservoir in early 2007
| image_bathymetry = 
| caption_bathymetry = 
| location = [[South Australia]]
| coords = {{coord|35.073|S|138.572|E|region:AU_type:waterbody|display=inline,title}}
| type = [[Reservoir]]
| inflow = 
| outflow = 
| catchment = 
| basin_countries = Australia
| length = 
| width = 
| area = {{convert|1.88|km2|abbr=on}}
| depth = 
| max-depth = 
| volume = 
| residence_time = 
| shore = 
| elevation = 
| islands = 
| cities = 
}}
The '''Happy Valley Reservoir''' is a [[water reservoir]] located in [[Adelaide, Australia]]. Constructed when the total population of Adelaide numbered 315,200 (1893 census), the Happy Valley Reservoir now supplies over a half a million people, from Adelaide's southern extent to the [[City of Adelaide|city-centre]].<ref name="Postcards"/> It is also home to lots of wildlife including many kangaroos.

==Construction==
Built between 1892 and 1897 at a cost of [[Australian dollar|A$]]1.8 million<ref name="SA Water">{{cite web | accessdate =24 September 2005 | url =http://www.sawater.com.au/SAWater/Education/OurWaterSystems/Water+Storage+%28Reservoirs%29.htm | title = Water Storage (Reservoirs): Happy Valley Reservoir | year =2005 | publisher =[[SA Water]] }}</ref> it was the third [[reservoirs|reservoir]] constructed in [[South Australia]] as a supplement to the [[Thorndon Park Reserve and Reservoir|Thorndon Park Reservoir]] (built 1860) and the [[Hope Valley Reservoir]] (built 1872).

The original Happy Valley township, school and cemetery were completely flooded by the new reservoir requiring their relocation. The township was moved to the east while the cemetery, which is still in use today, was moved to the west and relocated alongside the base of the dam wall. The school, originally located on Candy road, was relocated south to two acres of land on Red Hill Road (later renamed Education road) which was donated by local farmer Harry Mason. While some students attended O'Halloran Hill or Clarendon schools for the 18 months that the Happy Valley school was closed, some did not attend any school until it was re-opened on 26 September 1898. The school closed in December 1979 and re-opened on a new site on the other side of the road directly opposite.<ref>[http://www.happyvalley.sa.edu.au/history.htm Happy Valley school history]</ref>

The reservoir acts as a 'holding pond' for water directed to it from the [[Clarendon Weir]] via a five km long underground tunnel. The 1.8 m diameter tunnel was bored simultaneously from both ends and when meeting had a deviation of 25&nbsp;mm. Its deepest point underground is 122 m where it passes through a hill. On 7 August 1896 the tunnel's inlet valve was opened by the [[Governor of South Australia]], [[Sir Thomas Buxton, 3rd Baronet|Sir Thomas Fowell Buxton]] and the reservoir began filling.<ref>[http://www.slsa.sa.gov.au/manning/adelaide/water/water.htm Adelaide Water Supply.] The Manning index of [[South Australia]]n history. [[State Library of South Australia]]</ref>

==Continuing use==
Initially 15 kilometres from [[Adelaide]], the reservoir is now largely enveloped by the city's southern suburbs, of which the relocated [[Happy Valley, South Australia|Happy Valley]] village is now one.<ref name="Postcards">{{cite web | last =Conlon | first =Keith | authorlink =Keith Conlon |accessdate =24 September 2005 | url =http://www.postcards.sa.com.au/features/happy_valley.html | title =Open Day at Happy Valley | work=[[Postcards (TV series)|Postcards]] | publisher =[[Nine Network]] }}</ref> Although the reservoir is relatively small in capacity; holding only 11,500 megalitres and is dwarfed by [[Mount Bold Reservoir]] which is at least four times larger,<ref name="Postcards"/> it is the site of the biggest water treatment plant in Adelaide and is responsible for providing more than 40% of the city's water.<ref name="Yurrebilla">{{cite web | date=21 March 2005 | accessdate =24 September 2005 | url =http://www.parks.sa.gov.au/yurrebilla/parkparticipants/water/index.htm | title = 
SA Water Reserves: Happy Valley| publisher =South Australian Department for Environment and Heritage }}  {{webarchive |url=https://web.archive.org/web/20060917202729/http://www.parks.sa.gov.au/yurrebilla/parkparticipants/water/index.htm |date=17 September 2006 }}</ref>

With public access prohibited, the natural "[[The bush|bush]]" of the reservoir's enclosed catchments are home to [[kangaroo]]s, [[echidna]]s and [[koala]]s. Several areas have also been planted with managed pine [[plantation]]s to reduce soil erosion and provide an income from harvesting. As a result of the plantations being located within what are now inner city suburbs they have become almost iconic. In the early 2000s, plans to harvest the mature pines were blocked following public protests over the visual impact the clearance and replanting would have on the surrounding suburbs.

Water from the dam was originally supplied to Adelaide through a tunnel under Black Road. In the early 1960s, the original intake tunnel from the Clarendon Weir was increased in size to allow access by maintenance vehicles, and a second outlet tunnel was constructed under [[South Road]]. In 1986 this new tunnel became the sole outlet for the reservoir when the original outlet was abandoned.<ref>[http://www.parliament.sa.gov.au/NR/rdonlyres/E81F9DE9-3ACB-4815-8EC6-7F5864EABFE1/3088/hansard3july2002.pdf Parliament of south Australia official Hansard Report] Happy Valley Reservoir Rehabilitation Project Wednesday 3 July 2002 pdf</ref> At the same time the Thorndon Park Reservoir was decommissioned and reestablished as a recreational park.

==Upgrade==
Between 2002 and 2004, the Reservoir underwent a major renovation as part of A$22 million rehabilitation project aimed at enhancing the Reservoir to meet guidelines of best practice for dam management at both international and national levels.<ref name="SA Water"/> The reservoir's earth wall was particularly susceptible to piping failure (a small leak, called a pipe, gets larger until the dam collapses) and the renovations lowered the risk of dam wall failure from 1 in 1,200 to an estimated 1 in 100,000.  Part of this project included an upgrade of the dam wall designed to also increase flood storage capacity by 165% and reduce risk of damage in the event of an earthquake.<ref>{{cite web | accessdate =24 September 2005 | url =http://www.sawater.com.au/NR/rdonlyres/7DF9CFDB-90D2-4864-86CD-6DC8F8C932A6/0/HVBrochure.pdf | format = PDF | title = 
Happy Valley Reservoir Wall Upgrade | publisher =[[SA Water]] |archiveurl = http://web.archive.org/web/20050717181822/http://www.sawater.com.au/NR/rdonlyres/7DF9CFDB-90D2-4864-86CD-6DC8F8C932A6/0/HVBrochure.pdf <!-- Bot retrieved archive --> |archivedate = 17 July 2005}}</ref>

With the lowering of the water level during renovations exposing the original Happy Valley township for the first time, archaeologists took the opportunity to excavate the site. Despite the township being entirely intact and undamaged when flooded in 1896, very little was found apart from scattered bricks and the foundations of several buildings of which only the Post Office was identified.<ref>Article in [[The Advertiser (Adelaide)|The Advertiser]] in 2002 (help required: date unknown at this time) As nothing of interest was found no research was published.</ref>
*Capacity: 11.5 Gigalitres
*Length of wall: 1,155 m
*Height of wall: 23.6 m
*Type of wall: Earth with clay core
*Area of water spread: 1.88&nbsp;km²

==See also==
*[[List of reservoirs and dams in Australia]]

==References==
{{Reflist}}

[[Category:1897 establishments in Australia]]
[[Category:Reservoirs in South Australia]]
[[Category:Dams in South Australia]]
[[Category:South Australian Heritage Register]]