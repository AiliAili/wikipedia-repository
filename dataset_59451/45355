{{Infobox military person
|name=Farrukh agha Gayibov
|birth_date={{Birth date|1891|10|02}}
|death_date={{death date and age|1916|09|12|1891|10|02}}
|image=Fərrux ağa Qayıbov-2.jpg
|caption=
|birth_place= Girag Salahli, [[Qazakh Rayon|Qazax]], [[Azerbaijan]]
|death_place= [[Belastok Region|Belostok]]-[[Grodno]] Oblast, Boruni
|placeofburial=
|placeofburial_label=
|nickname=
|allegiance={{flag|Russian Empire}}
|branch=[[Imperial Russian Air Force]], [[Artillery]]
|serviceyears=1913—1916
|rank=[[Lieutenant]]
|unit=
|commands=39th artillery brigade of the 1st Caucasian Army Corps, Airships' squadron of the Western Front
|battles=
|awards=[[Order of St. George]], [[Order of St. Anna]], [[Order of Saint Stanislaus]]
|relations=[[Ali-Agha Shikhlinski]], [[Nigar Shikhlinskaya]]
|laterwork=
}}
'''Farrukh-agha Mammad Kerim-agha oglu Gayibov''' {{lang-az|Fərrux ağa Məmmədkərim ağa oğlu Qayıbov}} (October 2, 1891—September 12, 1916) was a [[Russia]]n [[Aviator|pilot]] of [[Azerbaijan]]i ancestry, and participant in [[World War I]]. He is considered to be the first Azerbaijani pilot.

==Childhood and youth==
Farrukh's father, Mammad Kerim agha, was as [[cadet]] in the 4th [[Muslim]] (Azerbaijanis) Life Guards of the [[Caucasus|Caucasian]] [[Squadron (army)|squadron]] of the Emperors own convoy, in the 1870s. After the completion of his military service, he was transferred to be a warrant officer of a [[militia]]. In subsequent years, Mammad Kerim agha worked as head of the court in [[Kazakhsky Uyezd]] of the [[Elisabethpol Governorate]], a police officer at the [[Dzhevanshirsky Uyezd]] of Elisabethpol Governorate. On August 30, 1894, Mammad Kerim agha was awarded the Third Class [[Order of Saint Stanislaus]] for exceptional military service.<ref>''Shamistan Naizrli'' Расстрелянные генералы Азербайджана p. 420</ref> His father died before his birth.

Farrukh agha Gayibov was born on October 2, 1891, in the Girag Salahly village of Kazakhsky Uyezd. He was raised by his uncle on his father's side, the prominent academic and public figure Samad agha Gayibov. Farukh agha studied five years at the Russian-Azerbaijan school in his native village, and then he continued his education at [[Tbilisi]] Cadet Corps, following [[Ali-Agha Shikhlinski]]'s advice.<ref>''Shamistan Naizrli'' Первый военный лётчик Азербайджана</ref> After his graduation on June 16, 1910, with honors, he entered Konstantinovskiy [[Artillery]] School. He was distinguished for being observant, having courage, and skill with firing guns. There Farrukh agha was awarded a golden watch of the [[Switzerland|Swiss]] company "Pavel Bure". Graduating from Artillery School, he was commissioned a second [[lieutenant]] and was transferred to the second artillery [[battalion]] of the 39th [[artillery brigade]] of the 1st [[Russian Caucasus Army (World War I)|Caucasian Army]] Corps of Jelaus tract, where he was assigned as junior officer of the 4th [[Artillery battery|battery]].<ref>Памятная книжка и адрес-календарь Карсской области на 1914 год, page 126</ref>

==Participation in World War I==
Farrukh agha took part in the Caucasus Campaign of World Was I. On August 31, 1915, he was promoted to first lieutenant. On February 3, 1916, Gayibov was sent to the [[Western Front (World War I)|Western Front]] and attached to the squadron of airships. On May 21, he was assigned as artillery officer of "[[Sikorsky S-16|Ilya Muromets № 16]]" airship, which had been built by [[Igor Sikorsky|I.I. Sikorsky]] in [[Saint Petersburg]], a [[Russo-Balt]] factory.

On the eve of the war, Russia had the largest air fleet among the belligerents: 244 [[Fixed-wing aircraft|airplane]]s in 39 squadrons.<ref>[http://www.airwar.ru/history/af/russia1w/russia1w.html Состояние российской авиации накануне войны]</ref> By the beginning of the hostilities, there were 221 pilots, 170 officers, 35 lower rank officers and 16 volunteers in the Russian air fleet.

The logistics of the beginning of the war proved to be inefficient with [[Gasoline|petrol]], [[castor oil]], spares, tents and other aviation supplies. Airplanes routinely wrecked in the dry field circumstances, especially with bad weather, when there was scarcity of tents and moveable [[hangar]]s, usage of areas of little avail for [[Aerodrome|airdrome]]s. After the first months of the war, many squadrons had to be taken back to home to be supplied with new equipment and retrained.

Gayibov was in combat fights on board the "Ilya Muromets № 16" airship, and raided enemy positions.

==Last Flight==
On September 12, 1916, a raid was conducted under the command of colonel Brant's General Staff, with an air squadron of two airplanes including the "Ilya Muromets", and 13 instruments. The aim of the raid was Borun, 12 [[verst]]s from the enemy front line, and a nearest district, where, according to reports, the staff of a [[Germany|German]] [[Division (military)|division]] was based, at the junction of a narrow gauge railway, together with artillery and quartermaster's storehouses and airdrome.<ref>[http://www.airwar.ru/history/av1ww/murom/murom_01.html Безвозвратные потери самолетов «Илья Муромец» во время Первой Мировой и Гражданской войн]</ref>

Farrukh's squadron penetrated into Krevo and dropped 100-pound bombs, forcing the enemy to retreat.

Six days later, on September 18, 1916, The Petrogradskiye Izvestiya newspaper wrote: "General headquarters report that our airplanes invaded the enemy's rear in the Western front in [[Boruń|Borun]]-Krevo district. Various posts were exploded by accurate bomb attacks, storehouses of the enemy were set on fire. Transportation facilities, railway stations and automobiles were also destroyed. Farrukh agha Gayibov and his team entered the battle with the enemy and destroyed four German airplanes. After destroying two [[Albatros D.II|Albatros]] airplanes, they fell into enemy's territory and perished".

==Awards==
On November 8, 1915, Farrukh agha Gaibov was awarded the Third Class Order of Saint Stanislaus with swords and bow, and on December 14, the Fourth Class [[Order of St. Anna]] with the "For Bravery" legend on it.

In 1916, he was awarded the Second Class Order of Saint Stanislaus with swords. On January 26, 1917, F. Gayibov was awarded the Second Class Order of St. Anna with swords (posthumously). On March 14 he was awarded the Third Class Order of St. Anna. On March 25, as a lieutenant of 39th artillery brigade who died in air battle with his enemy, Farrukh agha Gayibov was posthumously awarded the Fourth Class [[Order of St. George]].

==Memory of the hero==
By tradition of pilots, a note had been thrown down from a German airplane and it is reported that Germans buried the crew of the airplane with military honours.<ref>[http://militera.lib.ru/h/finne_kn2/07.html Финне К. Н. Русские воздушные богатыри И. И. Сикорского]</ref><ref>[http://www.retroplan.ru/encyclopaedia.html?sobi2Task=sobi2Details&sobi2Id=341 РетропланЪ: Авиация от Первой мировой до Второй мировой войны]</ref>

Recently, the cemetery of the fallen airplane crew was found in a German cemetery in Borun village. Apparently, Russian pilots were reburied in this cemetery in the 1930s, when the [[Poland|Polish]] government provided German military burials, with the participation Germans. These words were written on gravestone cross in [[Polish language|Polish]]: «4 NIEZNANYCH ROS.LOTN.25.16», which means: "4 unknown Russian pilots were buried 25.16". The date was written according to the [[Europe]]an calendar. The month of burial was missing in the inscription, but it wasn't written on other gravestone crosses, either. There lay the heroes and pilots:

:*Lieutenant Maksheev Dmitri Dmitriyevich;

:*Lieutenant Rakhlin Mitrofan Alekseyevich;

:*Lieutenant Gayibov Farrukh;

:*Lieutenant Karpov Oleg Sergryevich.

In 1971, [[Pavel Stepanovich Kutakhov|Pavel Kutakhov]], the Chief Aviation Marshall, was elected as deputy of the [[Supreme Soviet of the Soviet Union]] from the [[Qazakh Rayon|Gazakh]]-[[Tovuz Rayon|Tovuz]] electoral district. After archival cross-referencing and adjustments, Pavel Kutakhov sent the hero to his motherland. He also arranged for the erection of a monument to the [[Mikoyan-Gurevich MiG-15|Mig-15]] fighter, with a memorial plaque which read: "In memory of the first Azerbaijani pilot Farrukh agha Gayibov". Later, the inscription on the plaque was changed to: "In memory of those killed in the [[Eastern Front (World War II)|Great Patriotic War]]".

==References==
{{commonscat|Fərrux ağa Qayıbov}}
{{reflist}}

{{DEFAULTSORT:Gayibov, Farrukh}}

[[Category:Aerial warfare pioneers]]
[[Category:Azerbaijani people of World War I]]
[[Category:1891 births]]
[[Category:1916 deaths]]
[[Category:Russian aviators]]
[[Category:Imperial Russian Air Force personnel]]
[[Category:Recipients of the Order of St. George of the Fourth Degree]]
[[Category:People from Qazakh]]
[[Category:Russian people of Azerbaijani descent]]