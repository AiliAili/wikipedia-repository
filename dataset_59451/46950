{{use dmy dates|date=December 2014}}
{{Infobox scientist
| name        = Robert Chapman
| honorific_suffix = {{post-nominals|country=NZL|CMG|size=85%}}
| image       =   
| imagesize   = 
| alt         = 
| caption     = 
| birth_date  = {{birth date|1922|10|30|df=yes}}
| birth_place = [[Takapuna]], Auckland
| death_date  =  {{death date and age |2004|05|26|1922|10|30|df=yes}}
| death_place = Auckland
| other_names = 
| residence   = 
| citizenship = 
| nationality = New Zealand
| fields      = [[Political science]]
| workplaces  = [[University of Auckland]]
| patrons     = 
| education   = 
| alma_mater  = [[University of Auckland]]
| thesis_title =  <!--(or  | thesis1_title =  and  | thesis2_title = )-->
| thesis_url  =   <!--(or  | thesis1_url  =   and  | thesis2_url  =  )-->
| thesis_year =   <!--(or  | thesis1_year =   and  | thesis2_year =  )-->
| doctoral_advisor = 
| academic_advisors = 
| doctoral_students =
| notable_students = [[Helen Clark]], [[Phil Goff]], [[Mike Moore (New Zealand politician)|Mike Moore]]
| known_for   = 
| influences  = 
| influenced  = 
| awards      = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| spouse      =  Noeline Chapman
| partner     =   <!--(or | partners = )-->
| children    = 3
| signature   =   <!--(filename only)-->
| signature_alt = 
| website     =   <!--{{URL|www.example.com}}-->
| footnotes   = 
}}

'''Robert (Bob) McDonald Chapman''' {{post-nominals|country=NZL|CMG|size=85%}} (30 October 1922 – 26 May 2004) was a New Zealand political scientist and historian.

==Early life==
[[File:Robert Chapman and family.png|thumbnail|Chapman as a small child (right) with his mother and sister in 1927.]]
Chapman was born in 1922 in Takapuna, Auckland. He attended [[Auckland Grammar]]. He later attended Auckland teacher's training college, and also studied at Auckland University College, where he received scholarships.<ref name="NZH obit">{{cite news | url=http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=3570757 | title=Obituary: Robert Chapman | work=The New Zealand Herald | date=5 June 2004 | accessdate=8 October 2014 | author=Aimer, Peter, Phoebe Falconer and Brian Rudman}}</ref>

For his Master's research project in history, he analysed the 1928 general election in New Zealand.<ref name="NZH obit" />

He married Noeline Amy Chapman, a teacher, in 1948.<ref name="NZH obit" />

==Career==

Chapman was first appointed to the History Department at the University of Auckland in 1948. He was interested in "New Zealand history as an expression of the nation's social development."<ref name="NZH obit" /> According to the ''[[New Zealand Herald]]'' obituary of Chapman:

<blockquote>"He was one of a pioneer group of teachers at the university - among them historian Sir Keith Sinclair and poet Allen Curnow - who, in the 1960s, proudly asserted that New Zealand had its own history, its own politics, its own literature, which was every bit as important as that of Britain."<ref name="NZH obit" /></blockquote>

Chapman was interested in fields outside political science and history, and was involved in the development of New Zealand literature and poetry. He co-edited, with Jonathan Bennett, ''An Anthology of New Zealand Verse'' (1956). His interest in other areas was part of a wider pattern among New Zealand public intellectuals:
<blockquote>"Literary criticism sometimes functioned as cultural criticism for intellectuals whose formal expertise lay in other fields. Robert Chapman’s 1954 ''Landfall'' essay "Fiction and the social pattern" charted the ways recent novels had revealed Pākehā social mores – a subject at some remove from Chapman’s research as a lecturer in political science."<ref>{{cite web|author=Hilliard, Chris|title=Intellectuals - Literature and the modern New Zealand intellectual|work=[[Te Ara: The Encyclopedia of New Zealand]] - the Encyclopedia of New Zealand|date=13 August 2014}}</ref></blockquote>

Chapman participated in the first television coverage of the [[New Zealand general election, 1966|New Zealand general election in 1966]]. He also helped with election coverage in the [[New Zealand general election, 1969|1969]] and [[New Zealand general election, 1972|1972]] elections.<ref name="NZH obit" />

Over his career Chapman had an interest in broadcasting issues, and was appointed in 1973 to a ministerial committee to look into restructuring broadcasting in New Zealand. Later, in 1984, he was appointed chairman of a Royal Commission into Broadcasting, which reported in 1986.<ref name="NZH obit" />

In 1966, lecturers Keith Sinclair and Chapman established the University of Auckland Art Collection, beginning with the purchase of several paintings and drawings by [[Colin McCahon]]. The Collection is now managed by the Centre for Art Research, based at the Gus Fisher Gallery.{{citation needed|date=October 2014}}

===Awards===
In the [[1987 Birthday Honours|1987 Queen's Birthday Honours]] Chapman was appointed a [[Order of St Michael and St George|Companion of the Order of St Michael and St George]] for public services.<ref>{{London Gazette |issue= 50950|date=12 June 1987 |startpage=31|supp=yes |accessdate=27 November 2016}}</ref>

==Legacy==

The New Zealand journal ''Political Science'', in an editorial, noted Chapman's death:

<blockquote>"Finally, the fifth article in this issue ... laments the passing of Professor Robert Chapman, truly a rangatira of New Zealand political science. Significant among his landmark publications was his contribution to the 1986 ''Report of the Royal Commission on the Electoral System: Towards a Better Democracy'', entitled "Voting in the Maori Political Sub-System, 1935-1984"."<ref>{{cite journal | url=http://intl-pnz.sagepub.com/content/56/1/3.full.pdf+html | title=Editorial | author=Levine, Stephen | author2=Roberts, Nigel|journal=Political Science |date=June 2004  | volume=56 | issue=1 | pages=3 | doi=10.1177/003231870405600101}}</ref></blockquote>

The Robert Chapman Prize in Politics and International Relations is awarded annually at the University of Auckland.<ref>{{cite web | url=http://www.arts.auckland.ac.nz/en/about/schools-in-the-faculty-of-arts/school-of-social-sciences/scholarships-and-awards.html | title=Faculty of Arts - School of Social Sciences - Te Pokapū Pūtaiao Pāpori | publisher=University of Auckland | accessdate=8 October 2014}}</ref>

===The Chapman Archive===
The Chapman Archive began as a personal collection of broadcast recordings made by Professor Robert Chapman and his wife Noeline in the mid 1960s. It later became a resource of the University of Auckland's Department of Political Studies before moving to the University Library in 2011.<ref>{{cite web|title=TV and Radio - About|url=https://www.library.auckland.ac.nz/tv-radio/about| publisher=University of Auckland | accessdate=24 September 2015}}</ref>

Now part of the University Library, the Archive is the University Library's largest audiovisual collection. Its focus is on building a collection that reflects New Zealand's political, social, cultural and economic history as shown through broadcast media. This focus has allowed the Chapman Archive to develop a comprehensive collection of news and current affairs programming depicting defining moments in New Zealand's history.

The collection covers broadcasting from the New Zealand Broadcasting Corporation, Television New Zealand, commercial broadcasters, including TV3 from 1989. Also included is an extensive collection of Radio New Zealand broadcasts.

In a speech at the Chapman memorial lecture in 2000, former Prime Minister [[Helen Clark]] highlighted the contribution made by Noeline:
<blockquote>"In an unpaid capacity, Noeline [Chapman] clipped the newspapers and recorded the news and current affairs day in, day out. That work formed the basis for the Noeline Chapman archive in the Political Studies Department - supplemented, of course, by the ceaseless flow of parliamentary material from Jonathan Hunt for the last 34 years!"<ref name="Clark chapman lecture 2000" /> </blockquote>

===Chapman memorial lecture===
In honour of Robert and Noeline Chapman, the University of Auckland established the Chapman memorial lecture.

In 2000, Prime Minister [[Helen Clark]], a student of Chapman's, gave the first Chapman memorial lecture.<ref name="Clark chapman lecture 2000">{{cite web | url=http://www.beehive.govt.nz/speech/chapman-lecture-series-honour-emeritus-professor-robert-chapman | title=The Chapman Lecture Series, in honour of Emeritus Professor Robert Chapman | publisher=New Zealand Government | work=Beehive.govt.nz | date=20 November 2000 | accessdate=8 October 2014 | author=Clark, Helen}}</ref> She gave another memorial lecture in his honour in 2013.<ref>{{cite web | url=https://www.youtube.com/watch?v=FssUHDsSkKI | title=2013 Robert Chapman Lecture: The Rt Hon Helen Clark | publisher=University of Auckland Arts Faculty | work=Youtube.com | accessdate=8 October 2014}}</ref>

In 2004, the Lecture was presented by Associate Professor Elizabeth McLeay, on "Studying New Zealand Politics".

In 2005, Hon. Bill English, who went on to become NZ Deputy Prime Minister for 8 years and later, Prime Minister (2016-17), [http://www.scoop.co.nz/stories/PA0510/S00015.htm presented] the Chapman Lecture.

In 2011, Professor Stephen Levine presented the Chapman Lecture, speaking on '[https://mediastore.auckland.ac.nz/uploaded/public/01-2012/60824635AF904144A219067327423839.preview New Zealand Politics: Democracy and the Semi-Sovereign People]'.

The 2012 memorial lecture was given by Associate Professor Ralph Chapman, a son of Robert Chapman, and was titled "Averting dangerous climate change: accelerating the energy transition".<ref>{{cite web | url=http://www.victoria.ac.nz/sgees/about/staff/staff-publications/publications-ralph-chapman | title=Publications Ralph Chapman | publisher=Victoria University of Wellington | accessdate=8 December 2014}}</ref>

The 2014 memorial lecture was by Professor Lucy Sargisson, and was titled "Utopianism in the Twenty-First Century".<ref>{{cite web | url=http://www.arts.auckland.ac.nz/en/about/events/events-2014/2014/07/robert-chapman-lecture--utopianism-in-the-twenty-first-century.html | title=Robert Chapman Lecture: Utopianism in the Twenty-First Century | publisher=University of Auckland | accessdate=8 October 2014}}</ref>

In 2015, Professor of Politics from Princeton University, [https://www.auckland.ac.nz/en/about/news-events-and-notices/news/news-2015/07/ancient-lessons-for-modern-dilemmas.html Mellissa Lane], gave the Lecture.

==Selected works==
* Chapman, Robert and Jonathan Bennet, ''An Anthology of New Zealand Verse'' (London: Oxford University Press, 1956)
* Chapman, Robert, ed, ''Ends and Means in New Zealand Politics'' (Auckland: University of Auckland, 1961)
* Chapman, Robert, Keith Jackson, and Austin Vernon Mitchell, ''New Zealand Politics in Action: The 1960 General Election'' (London: Oxford University Press, 1962)
* Chapman, Robert and E P Malone, ''New Zealand in the Twenties: Social Change and Material Progress'' (Auckland: Heinemann Educational Books, 1969)
* Chapman, Robert, "From Labour to National" in ''The Oxford History of New Zealand'', ed, W.H.Oliver and B.R.Williams (Oxford and Wellington: The Clarendon Press and Oxford University Press, 1981), chapter 13, pp.&nbsp;333–368 ISBN 0-19-558062-1
* McLeay, Elizabeth, ed, ''[http://vup.victoria.ac.nz/new-zealand-politics-and-social-patterns/ New Zealand Politics and Social Patterns: Selected Works by Robert Chapman]'' (Wellington: Victoria University Press, 1999) ISBN 9780864733610

== References ==
{{Reflist|30em}}

{{Authority control}}

{{DEFAULTSORT:Chapman, Robert}}
[[Category:1922 births]]
[[Category:2004 deaths]]
[[Category:New Zealand political scientists]]
[[Category:University of Auckland alumni]]
[[Category:University of Auckland faculty]]
[[Category:New Zealand historians]]
[[Category:New Zealand Companions of the Order of St Michael and St George]]
[[Category:Psephologists]]
[[Category:20th-century historians]]