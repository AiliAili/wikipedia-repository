{{good article}}
{{Taxobox
| image = Boletopsis nothofagi habitat.jpg
| image_width = 240px
| image_caption =
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Thelephorales]]
| familia = [[Bankeraceae]]
| genus = ''[[Boletopsis]]''
| species = '''''B. nothofagi'''''
| binomial = ''Boletopsis nothofagi''
| binomial_authority = J.A.Cooper & P.Leonard (2012)
| status = EN
| trend = down
| status_system = iucn3.1
| status_ref =<ref name=iucn>
{{IUCN | id = 80188388| taxon = Boletopsis nothofagi| assessor = Leonard, P.L.| assessor2 = McMullan-Fisher, S.| assessor3 = May, T.| assessor4 = Buchanan, P.| last-assessor-amp = yes| assessment_year = 2015| version =2015.4 | accessdate = May 7, 2016}}</ref>
}}
'''''Boletopsis nothofagi''''' is a [[fungus]] in the [[Family (biology)|family]] [[Bankeraceae]]. The fungus forms grey [[basidiocarp|fruit bodies]] that grow in clusters. Like all species of ''[[Boletopsis]]'', it has a porous [[hymenophore|spore-bearing surface]] on the underside of the [[pileus (mycology)|cap]], but differs from other species of ''Boletopsis'' by having characteristics such as elongated [[spore]]s and a green discoloration when stained with [[potassium hydroxide]]. '' Boletopsis nothofagi'' is [[endemic]] to [[New Zealand]]  and has a [[mycorrhiza]]l association with red beech (''[[Nothofagus fusca]]''). It is unknown when exactly the fungus forms its fruit body, but it has so far been found solely in May, during autumn in the Southern Hemisphere.

The first [[species description|description]] of ''B.&nbsp;nothofagi'' was published in 2012 by Jerry A. Cooper and Patrick Leonard. [[molecular phylogenetics|DNA studies]] of the fungus suggest that it is a somewhat [[basal (phylogenetics)|basal]] member of the genus ''Boletopsis''. The fungus is most likely a [[native species]] of New Zealand and was present there before the arrival of Europeans. As it is very rare and possibly [[threatened species|threatened]], ''B.&nbsp;nothofagi'' is listed in the [[Red List of Threatened Species]] as an [[endangered species]].<ref name=iucn>
{{IUCN | id = 80188388| taxon = Boletopsis nothofagi| assessor = Leonard, P.L.| assessor2 = McMullan-Fisher, S.| assessor3 = May, T.| assessor4 = Buchanan, P.| last-assessor-amp = yes| assessment_year = 2015| version =2015.4 | accessdate = May 7, 2016}}</ref>

==Taxonomy==
{{Cladogram
| Title =
|cladogram={{Clade | style = white-space: nowrap; font-size: 85%; line-height: 100%
    | Label1 ='' [[Boletopsis (subgenus)|Boletopsis subgen. Boletopsis]]''
    | 1 = {{clade
           | 1 ='' [[Boletopsis leucomelaena|B. leucomelaena]]'' (USA)
           | 2 = {{clade
                  | 1 ='''''B. nothofagi'''''
                  | 2 = {{clade
                         | 1 ='' B. leucomelaena'' (Finland)
                         | 2 = {{clade
                                | 1 ='' [[Boletopsis perplexa|B. perplexa]]''
                                | 2 ='' [[Boletopsis subquamosa|B. subsquamosa]]''
                            }}
                     }}
                  | 3 = {{clade
                         | 1 =''Boletopsis'' sp. (Unknown origin)
                         | 2 = {{clade
                                | 1 ='' [[Boletopsis grisea|B. grisea]]''
                                | 2 ='' B. leucomelaena'' (Korea)
                            }}
                     }}
              }}
       }}
}}
}}

In 2009, an unknown species of ''[[Boletopsis]]'' was discovered in the [[Orongorongo River|Orongorongo]] valley near Wellington, New Zealand. In 2010, the fungus was found again in the same place and also discovered on the South Island. [[morphology (biology)|Morphological]] comparisons and [[molecular phylogenetics|molecular analysis]] of other species of the genus suggested that the fungus could not be attributed to any known representative of the genus, and so it was described by [[Mycology|mycologists]] Jerry A. Cooper and Patrick Leonard as a new species. The [[species description]] of ''Boletopsis nothofagi'' appeared in the journal ''MycoKeys'' in 2012. The two authors chose the [[Botanical name|epithet]] ''nothofagi'' based on the characteristic of the fungus as mycorrhizal [[symbiont]] of ''Nothofagus fusca''. Swollen [[hypha]]e and smooth [[spore]]s show that ''B.&nbsp;nothofagi'' is a member of the [[subgenus]] ''Boletopsis'' in the genus ''Boletopsis''.<ref Name="cooper2012-13-20" />

''Boletopsis nothofagi'' is a genetically clearly differentiated representative of the genus ''Boletopsis'', which according to the investigations of Cooper and Leonard separated relatively early from the precursor of most other known species. Only a North American species, ''[[Boletopsis leucomelaena|B.&nbsp;leucomelaena]]'', branches off from their [[phylogenetic tree]] even earlier. However, the relationships between many of the species were not fully resolved in the study, so in the future, new species may be described.<ref name="cooper2012-16-17" />

==Description==
[[File:Boletopsis nothofagi fruitbodies.jpg|thumb|upright=1.2|The fruit bodies of'' Boletopsis nothofagi'' usually grow in tufts.]]The [[basidiocarp|fruit bodies]] of ''Boletopsis nothofagi'' usually grow in tufts and only rarely individually. They have a centrally stalked [[Pileus (mycology)|cap]]. The cap is convex, measuring {{convert|10|–|80|mm|in|1|abbr=on}} wide and {{convert|5|–|22|mm|in|1|abbr=on}} high. In young specimens, the cap's edge is slightly bent, whereas the cap of older fruit bodies often curl. The [[pileipellis|cap cuticle]] is gray in color, and its texture ranges from smooth to slightly fibrous. Pressure- or scrape-spots are stained darker and eventually blacken.<ref name="cooper2012-18"/>

The [[stipe (mycology)|stipe]]s are club-shaped to cylindrical, slightly tapering towards both base and cap, with a height of about {{convert|20|–|60|mm|in|1|abbr=on}} and a thickness of {{convert|10|–|25|mm|in|1|abbr=on}}. The stipe is smooth and dry on the surface and has a firm texture on the inside. The stipes have a similar color as the cap and shows the same responses to damage.<ref name="cooper2012-18"/>

The white, porous [[hymenium]] has a thickness of 1–2&nbsp;mm and turns brown when bruised. Per millimeter, there are two to three square pores. When dried, the hymenium's color becomes pinkish-brown. The hymenium extends slightly down the stipe, and is sharply defined. Dried tissue smells similar to [[fenugreek]]. The morphology of the [[mycorrhiza]] has not yet been described; however, as with all other types of ''Boletopsis'' it is likely to be [[ectomycorrhiza]]l.<ref name="cooper2012-18"/>

===Microscopic characteristics===
''Boletopsis nothofagi'' has a [[Hypha#Types of Hyphae|monomitic]] hyphal structure, whereby all hyphae are ''generative'' hyphae, which serve the growth of the fungus. The cap, when viewed under a microscope, is clearly differentiated and consists of a [[cutis (mycology)|cutis]], a layer of oriented hyphae lying radially. They are up to 2&nbsp;[[micrometre|µm]] thick, [[pigment (biology)|pigmented]] brown and covered with small, irregularly shaped [[granule (cell biology)|granules]]. They become green when stained with [[potassium hydroxide]] (KOH), a diagnostic characteristic of the genus. The subcutis consists of swollen hyphae up to 6&nbsp;µm thick. These are thin-walled, filled with oil droplets and have [[clamp connections]] in the [[septum|septa]]. The hymenial layer has porous [[cystidium]] structures measuring 4 by 80&nbsp;µm. The [[basidia]] of '' B.&nbsp;nothofagi'' are pleurobasidia arising on the sides of the hyphae. They are cylindrical to club shaped, 5–10 by 20–30&nbsp;µm in size, and clamped at the base. The basidia always have four [[sterigma]]ta, on which light brown, thin [[basidiospore|spores]] are situated. The spores are uneven, with flattened ends and elongated in shape. On average, they measure 5.3 by 4.1&nbsp;µm.<ref Name="cooper2012-18" />
{{clear}}
{| class="wikitable" style="width:640px; margin: 1em auto; text-align: center;"
|+ style="background-color:#FFF; border:1px solid #CCC; border-bottom:none;" | 
Microscopic structures
| [[File:Boletopsis nothofagi basidia details.jpg|{{#expr: (200 * 531 / 680) round 0}}px|alt=]]
| [[File:Boletopsis nothofagi cystidia-like structures.jpg|{{#expr: (200 * 523 / 680) round 0}}px|alt=]]
| [[File:Boletopsis nothofagi hyphae details.jpg|{{#expr: (200 * 732 / 484) round 0}}px|alt=]]
| [[File:Boletopsis nothofagi spores.jpg|{{#expr: (200 * 730 / 684) round 0}}px|alt=]]
|-
| [[Basidium]]
| [[Cystidium|Cystidia]]-like structures
| [[Hypha]]e with [[clamp connection]]s
| Spores
|}

==Distribution==
[[File:Boletopsis nothofagi distribution.svg|thumb|Distribution of ''B. nothofagi'' in New Zealand]]

The known range of ''Boletopsis nothofagi'' is limited to two narrowly defined areas of New Zealand, one on the [[North Island]] and the other on the [[South Island]]. These areas are in [[Rimutaka Forest Park]] near [[Wellington]], and [[Saint Arnaud, New Zealand|Saint Arnaud]] in the northern part of the South Island. These locations are relatively far away from each other and isolated, which, together with its absence in the rest of New Zealand, makes it unlikely that the species is a recent import. It is more likely that the species is [[endemic|native]] to New Zealand and has been overlooked in earlier surveys due to its rarity.<ref name="cooper2012-16-18 " />

''Boletopsis nothofagi'' is the most southern member of the genus ''Boletopsis'', and as of 2013 the sole known member of the genus in the Southern Hemisphere; its closest relatives are found in Asia and [[Costa Rica]].<ref name="cooper2012-16-18 " />

==Ecology==
The occurrence of ''Boletopsis nothofagi'' seems to be strongly connected to the occurrence of the [[Nothofagus|southern beech]] ''[[Nothofagus fusca]]'', a species of [[Fagales]] that is endemic to New Zealand. ''B.&nbsp;nothofagi'' has been found exclusively in ''N.&nbsp;fusca'' forests spread through New Zealand below 37[[Latitude|° S]]. The fungus forms a mycorrhizal association with the trees of ''N.&nbsp;fusca'', in which the hyphae of the fungal [[mycelium]] wrap around the roots of the tree and penetrate the [[Cortex (botany)|cortex]], but not its cells. Subsequently, ''B.&nbsp;nothofagi'' takes over the function of the [[root hair]] and directs water and soil nutrients to the tree. In return, the fungus can, through contact with the root tissue, access the products of the tree's [[photosynthesis]]. The fruit bodies of the species have so far always been found in May, the end of autumn in the [[Southern Hemisphere]].<ref Name="cooper2012-18" />

Little is known about the habitat requirements&nbsp;– such as humidity, temperature, soil composition and water content&nbsp;– of ''B.&nbsp;nothofagi''. However, as the species seems to only occur together with ''N.&nbsp;fusca'', it should largely conform to their demands. The tree species prefers lowlands and hills along river valleys and usually grows on nutrient-rich, well-drained soil. The species is more likely to be found inland than in the coastal regions.<ref Name="Nelson 2006"/>

==Status==
According to Cooper and Leonard, the fact that ''Boletopsis nothofagi'' was only found 200 years after the [[History of New Zealand#European settlement|European settlement of New Zealand]] illustrates the rarity of this species, although it is also possible that the late discovery was caused by rare or infrequent fructification. The authors assume that the species occurs very sparsely and that this cannot be attributed to human activity. Although no data on population trends or historical distribution of the fungus exists, Cooper and Leonard consider the species in accordance to the [[New Zealand Threat Classification System]] as "naturally uncommon".<ref name="cooper2012-18-21" />

==References==
{{Reflist|refs=

<ref Name="Nelson 2006">{{cite report |author=Nelson  |author2=Marlborough Conservancy |title=Beech forest – Biodiversity |publisher=Department of Conservation. Government of New Zealand |year=2006 |url=http://www.doc.govt.nz/documents/about-doc/concessions-and-permits/conservation-revealed/beech-forest-lowres.pdf |format=PDF}}</ref>

<ref name="cooper2012-13-20">Cooper & Leonard (2012) pp. 13–20.</ref>
<ref name="cooper2012-16-17">Cooper & Leonard (2012) pp. 16–27.</ref>
<ref name="cooper2012-16-18">Cooper & Leonard (2012) pp. 16–18.</ref>
<ref name="cooper2012-18">Cooper & Leonard (2012) p. 18</ref>
<ref name="cooper2012-18-21">Cooper & Leonard (2012) pp. 18–21.</ref>

}}

===Cited literature===
* {{cite journal
| doi = 10.3897/mycokeys.3.2762
| issn = 1314-4057
| volume = 3
| issue =
| pages = 13–22
| last = Cooper
| first = Jerry
| author2 = Patrick Leonard
| title = ''Boletopsis nothofagi'' sp. nov. associated with ''Nothofagus'' in the Southern Hemisphere
| journal = MycoKeys
| accessdate =
| year = 2012
| url =
}}{{open access}}

==External links==
{{Commons category|Boletopsis nothofagi|''Boletopsis nothofagi''}}
*{{IndexFungorum|550039}}

{{taxonbar}}

[[Category:Thelephorales]]
[[Category:Fungi described in 2012]]
[[Category:Fungi of New Zealand]]