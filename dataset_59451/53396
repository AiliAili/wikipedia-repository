The '''Big Four''' are the four largest [[professional services networks]] in the world, offering [[audit]], [[assurance services]], [[tax]]ation, [[management consulting]], advisory, [[actuarial]], corporate finance and legal services. They handle the vast majority of audits for [[Public company|publicly traded companies]] as well as many [[Private company|private companies]]. It is reported that the Big Four audit 99% of the companies in the [[FTSE 100 Index|FTSE 100]], and 96% of the companies in the [[FTSE 250 Index]], an [[Stock market index|index]] of the leading [[Market capitalization|mid-cap]] listing companies.<ref name="FTSE">{{cite news|url=https://www.wsj.com/article/SB10001424052748703806304576232231353594682.html |title=U.K. Auditors Criticized on Bank Crisis |date=2011-03-30 |author=Mario Christodoulou |publisher=Wall Street Journal }}</ref>

The Big Four firms are shown below, with their latest publicly available data.
{| class="wikitable sortable"
|-
! Firm !! Revenues !! Employees !! Revenue per employee !! Fiscal year !! Headquarters !! Source
|-
| [[Deloitte]] ||$36.8 bn|| 244,400 || $150,573 ||2016|| United States || <ref>{{cite web|url=http://www2.deloitte.com/global/en/pages/about-deloitte/articles/global-revenue-announcement.html?id=gx:2sm:3fb:4GR2016:5awa:6About%20Deloitte:20160907100130::Global&linkId=28466974|title=2016 Global Report - Deloitte - Global Reports, insights, services, and solutions|work=Deloitte|accessdate=7 September 2015}}</ref>
|-
| [[PricewaterhouseCoopers]] || $35.9 bn || 223,468 || $160,649 || 2016 || United Kingdom || <ref>{{cite web|url=http://www.pwc.com/gx/en/about/global-annual-review-2016.html|title=Global Annual Review 2016|author=PricewaterhouseCoopers|work=PwC}}</ref>
|-

| [[Ernst & Young]] || $29.6 bn || 231,000 || $128,139 || 2016 || United Kingdom || <ref>http://www.ey.com/gl/en/newsroom/news-releases/news-ey-reports-record-global-revenues-in-2016-up-by-9-percent</ref>
|-
| [[KPMG]] || $25.4 bn || 188,982 || $134,510 || 2016 || Netherlands  || <ref>{{cite web|url=https://home.kpmg.com/xx/en/home/campaigns/2016/12/international-annual-review.html#financials/financials-global-revenue|title=2016 KPMG International Annual Review|accessdate=13 December 2016}}</ref>
|-
|}
This group was once known as the "Big Eight", and was reduced to the "Big Six" and then "Big Five" by a series of [[merger]]s. The Big Five became the Big Four after the fall of [[Arthur Andersen]] in 2002, following its involvement in the [[Enron scandal]].

==Legal structure==
None of the Big Four firms is a single firm; rather, they are [[professional services networks]]. Each is a network of firms, owned and managed independently, which have entered into agreements with other member firms in the network to share a common name, brand and quality standards. Each network has established an entity to co-ordinate the activities of the network. In one case (KPMG), the co-ordinating entity is Swiss, and in three cases (Deloitte Touche Tohmatsu, PricewaterhouseCoopers and Ernst & Young) the co-ordinating entity is a UK [[limited company]]. Those entities do not themselves perform external professional services, and do not own or control the member firms. They are similar to [[law firm network]]s found in the legal profession.

In many cases each member firm practices in a single country, and is structured to comply with the regulatory environment in that country. In 2007, KPMG announced a merger of four member firms

Ernst & Young also includes separate legal entities which manage three of its four areas: [[Americas]], EMEIA (Europe, The Middle East, India and Africa), and [[Asia-Pacific]]. (Note: the Japan area does not have a separate area management entity). These firms coordinate services performed by local firms within their respective areas but do not perform services or hold ownership in the local entities.<ref>{{Cite web|url=http://www.ey.com/GL/EN/home/legal|title=Legal Disclaimer|website=www.ey.com|access-date=2016-10-11}}</ref>

The figures in this article refer to the combined revenues of each network of firms.

==Mergers and the big auditors==
Since 1989, [[merger]]s and one major scandal involving Arthur Andersen have reduced the number of major professional-services firms from eight to four.

===Big Eight===
The firms were called the '''Big Eight''' for most of the 20th century, reflecting the international dominance of the eight largest firms (presented here in alphabetical order):
* [[Arthur Andersen]] (until its closure in 2002 for a conviction related to the [[Enron]] scandal which was later overturned by the US Supreme Court)<ref>ARTHUR ANDERSEN LLP, PETITIONER v. UNITED STATES 544 U.S. 696</ref>
* [[Coopers and Lybrand]] (until 1973 Cooper Brothers in the UK and Lybrand, Ross Bros., & Montgomery in the United States)
* [[Ernst & Young|Ernst & Whinney]] (until 1979 Ernst & Ernst in the United States and Whinney Murray in the UK)
* [[Deloitte Touche Tohmatsu|Deloitte Haskins & Sells]] (until 1978 Haskins & Sells in the United States and Deloitte & Co. in the UK)
* [[KPMG|Peat Marwick Mitchell]] (later Peat Marwick, then KPMG)
* [[Price Waterhouse]]
* [[Touche Ross]]
* [[Arthur Young (accountant)|Arthur Young]]
Most of the Big Eight originated in an alliance formed between British and U.S. audit firms in the 19th or early 20th centuries. Price Waterhouse was a UK firm which opened a U.S. office in 1890 and subsequently established a separate U.S. partnership. The UK and U.S. Peat Marwick Mitchell firms adopted a common name in 1925. Other firms used separate names for domestic business, and did not adopt common names until much later: Touche Ross in 1960, Arthur Young (at first Arthur Young, McLelland Moores) in 1968, Coopers & Lybrand in 1973, Deloitte Haskins & Sells in 1978 and Ernst & Whinney in 1979.<ref>{{cite web|url=http://www.icaew.com/index.cfm?route=155661|title=What's in a name: Firms' simplified family trees on the web|work=icaew.com}}</ref>

The firms' initial international expansion was driven by the needs of British and U.S.-based [[Multinational corporation|multinationals]] for worldwide service. They expanded by forming local partnerships or by forming alliances with local firms.

[[Arthur Andersen]] had a different history. The firm originated in the United States, and expanded internationally by establishing its own offices in other markets, including the United Kingdom.

In the 1980s the Big 8, each now with global branding, adopted modern marketing and grew rapidly. They merged with many smaller firms. One of the largest of these mergers was in 1987, when Peat Marwick merged with the Klynveld Main Goerdeler (KMG) group to become KPMG Peat Marwick, later known simply as KPMG.

===Big Six===
Competition among these firms intensified and the Big&nbsp;8 became the '''Big&nbsp;Six''' in 1989 when Ernst & Whinney merged with Arthur Young to form [[Ernst & Young]] in June, and Deloitte, Haskins & Sells merged with Touche Ross to form [[Deloitte & Touche]] in August.

Confusingly, in the [[United Kingdom]] the local firm of Deloitte, Haskins & Sells merged instead with Coopers & Lybrand. For some years after the merger, the merged firm was called Coopers & Lybrand Deloitte and the local firm of Touche Ross kept its original name. In the mid 1990s however, both UK firms changed their names to match those of their respective international organizations. On the other hand, in [[Australia]] the local firm of Touche Ross merged instead with KPMG.<ref>http://www.deloitte.com/dtt/article/0,1002,cid%253D12279,00.html</ref><ref>{{cite news |url=https://query.nytimes.com/gst/fullpage.html?res=950DE7DD103BF936A35751C1A96F948260 |date=1989-12-05 |title=Deloitte, Touche Merger Done |publisher=New York Times |author=Alison Leigh Cowan }}</ref> It is for these reasons that the Deloitte & Touche international organization was known as DRT International (later DTT International), to avoid use of names which would have been ambiguous (as well as contested) in certain markets.

===Big Five===
The Big 6 became the '''Big&nbsp;Five''' in July 1998 when Price Waterhouse merged with Coopers & Lybrand to form [[PriceWaterhouseCoopers]].

The [https://big4accountingfirms.com/big-5-accounting-firms/ Big 5 Accounting Firms] were:

1. Ernst & Young

2. Deloitte & Touche

3. Arthur Andersen

4. KPMG

5. PriceWaterhouseCoopers

===Big Four===
The [[Enron]] collapse and ensuing investigation prompted scrutiny of their financial reporting, which was audited by [[Arthur Andersen]]. Arthur Andersen was eventually indicted for obstruction of justice for shredding documents related to the audit in the [[Timeline of the Enron scandal|2001 Enron scandal]]. The resulting conviction, though later overturned, still effectively meant the end for Arthur Andersen. Most of its country practices around the world have been sold to members of what is now the '''Big Four'''—notably [[Ernst & Young]] globally; [[Deloitte & Touche]] in the UK, Canada, Spain, and Brazil; and [[PricewaterhouseCoopers]] (now known as [[PwC]]) in China and Hong Kong.

2002 saw the passage of the [[Sarbanes–Oxley Act]] into law in the US. It aims to enforce strict compliance to rules for both businesses and their auditors.

In 2010 Deloitte with its 1.8% growth was able to beat PricewaterhouseCoopers with its 1.5% growth to gain first place and become the largest firm in the industry. In 2011, PwC re-gained the first place with 10% revenue growth. In 2013, these two firms still claim the top two spots with only $200 million or 0.5% revenue difference. However, Deloitte has seen faster growth than PwC over the last few years (largely due to acquisitions) and reclaimed the title of largest of the Big Four in Fiscal Year 2016.

===Branding List===
A year at the end indicates year of formation through merger or adoption of single brand name.

* '''Arthur Andersen''' (1913-2001)
* '''EY''' (1989) (''Ernst & Young'' until 2013)
** '''Arthur Young''' (1906)
** '''Ernst & Whinney''' (1979)
*** '''Ernst & Ernst (US)''' (1903)
*** '''Whinney, Smith & Whinney (UK)''' (1849)
* '''PwC''' (1998) (''PricewaterhouseCoopers'' until 2010)
** '''Coopers & Lybrand''' (1973)
*** '''Cooper Brothers (UK)''' (1854)
*** '''Lybrand, Ross Bros, Montgomery (US)''' (1898)
** '''Price Waterhouse (US)''' (1849)
* '''Deloitte Touche Tohmatsu''' (1989) (''Deloitte & Touche'' until 1993)
** '''Deloitte Haskins & Sells''' (1978)
*** '''Deloitte & Co. (UK)'''
*** '''Haskins & Sells (US)'''
** '''Touche Ross''' (1975)
*** '''Touche Ross''' (1960) (''Touche, Ross, Bailey & Smart'' until 1969)
**** '''Ross (Canada)'''
**** '''George A. Touche (UK)'''
**** '''Touche, Niven, Bailey & Smart (US)''' (1947)
***** '''Touche Niven''' (1900)
***** '''Bailey & Smart''' (1947)
*** '''Tohmatsu & Co. (Japan)''' (1968)
* '''KPMG''' (1987) (''KPMG Peat Marwick'' before 1995)
** '''Peat Marwick''' (1925) (originally ''Peat Marwick Mitchell'')
*** '''William Barclay Peat (UK)''' (1870)
*** '''Marwick Mitchell (US)''' (1897)
*** '''Edwin Gurthie''' (1875-1955)
*** '''Beevers & Adgie''' (1849-1967)
** '''KMG''' (1979) (officially ''Klynveld Main Goerdeler'')
*** '''Klynveld Kraayenhof (Netherlands)''' (1917)
*** '''[[McLintock Main Lafrentz]]''' (1964)
**** '''Thomson McLintock (UK)''' (1877)
***** '''Martin Farlow''' (1882-1968)
***** '''Grace Ryland''' (1967-1969)
****** '''Grace, Darbyshire, & Todd''' (1818)
****** '''CJ Ryland''' (1910)
**** '''Main Lafrentz (US)''' (c.1880)
*** '''Deutsche Treuhand-Gesellschaft (Germany)''' (1890)
** '''Armitage & Norton''' (1869-1987)

==Criticism==
===Tax avoidance===
According to Australian taxation expert George Rozvany, the Big Four are "the masterminds of multinational [[tax avoidance]] and the architects of tax schemes which cost governments and their taxpayers an estimated $US1 trillion a year". At the same time they are advising governments on [[tax reform]]s, they are advising their multinational clients how to avoid taxes.<ref>{{cite news|publisher=The NEWDAILY|title=‘Tax avoidance’ masters revealed|date=2016-07-11|url=http://thenewdaily.com.au/money/finance-news/2016/07/11/architects-global-tax-avoidance-revealed/}}</ref><ref>{{cite news|publisher=The Independent|quote=Regulators fail to act as they are dominated by the companies they are supposed to police, say critics|url=http://www.independent.co.uk/news/uk/crime/emb-0000-big-four-audit-firms-never-examined-over-illegal-tax-plans-a6818126.html|date=2016-01-18|title=‘Big Four’ audit firms never examined over illegal tax plans}}</ref>

===Policy issues concerning industry concentration===
In the wake of [[industry concentration]] and individual firm failure, the issue of a credible alternative industry structure has been raised.<ref>{{cite web|ssrn=928482|title=Too Big to Fail: Moral Hazard in Auditing and the Need to Restructure the Industry Before it Unravels|work=ssrn.com}}</ref> The limiting factor on the growth of additional firms is that although some of the firms in the next tier have become quite substantial, and have formed international networks, effectively all very large public companies insist on having a "Big Four" audit, so the smaller firms have no way to grow into the top end of the market.

Documents published in June 2010 show that some UK companies' [[Contractual term|banking covenants]] required them to use one of the Big Four. This approach from the lender prevents firms in the next tier from competing for audit work for such companies. The [[British Bankers' Association]] said that such clauses are rare.<ref>{{cite web|url=http://www.accountancyage.com/accountancyage/news/2264992/big-four-clauses-rare-bba |title=Big-Four-only clauses are rare: BBA |work=accountancyage.com |deadurl=yes |archiveurl=https://web.archive.org/web/20100621014525/http://www.accountancyage.com:80/accountancyage/news/2264992/big-four-clauses-rare-bba |archivedate=2010-06-21 |df= }}</ref> Current discussions in the UK consider outlawing such clauses.

In 2011,The UK [[House of Lords]] completed an inquiry into the financial crisis, and called for an [[Office of Fair Trading]] investigation into the dominance of the Big Four.<ref>{{cite web|url=http://www.ft.com/cms/s/0/358b366e-59fa-11e0-ba8d-00144feab49a.html|title=Auditors criticised for role in financial crisis|work=Financial Times}}</ref> It is reported that the Big Four audit all but one of the companies that constitute the FTSE 100, and 240 of the companies in the FTSE 250, an index of the leading mid-cap listing companies.<ref name="FTSE"/>

In [[Ireland]], the Director of Corporate Enforcement, in February 2011 said, auditors "report surprisingly few types of company law offences to us", with the so-called "big four" auditing firms reporting the least often to his office, at just 5pc of all reports.<ref>http://www.independent.ie/business/irish/appleby-and-revenue-query-work-of-auditors-2544309.htm</ref>

==Global member firms==

<!--The information regarding Ernst & Young member firms is out of date. For a correct list of member firms please see ey.com-->

{{Refimprove section|date=April 2009}}

{| class="wikitable sortable" border="1"
|-
! Region
! Deloitte Touche Tohmatsu
! PwC
! Ernst & Young
! KPMG
|-
| Australia
| Deloitte Australia<ref>{{cite web|url=http://www2.deloitte.com/au/en/pages/about-deloitte/articles/about-deloitte-australia.html|title=About Deloitte Australia|date=20 February 2015|work=Deloitte Australia|accessdate=6 October 2015}}</ref>
| PricewaterhouseCoopers
| Ernst & Young
| KPMG
|-
| Argentina
| Deloitte & Co. S.R.L
| Pricewaterhouse & Co. S.R.L.
| Ernst & Young
| KPMG
|-
| Bangladesh
| None
| Pricewaterhouse & Co. Limited
|A. Qasem & Co
EY Bangladesh
|Rahman Rahman Huq, KPMG in Bangladesh
|-
| Brazil
| Deloitte
| PwC
| EY
| KPMG
|-
| Bulgaria
| Deloitte
| PwC
| EY
| KMPG
|-
| China
| Deloitte Hua Yong
| PricewaterhouseCoopers Zhong Tian
| Ernst & Young Hua Ming
| KPMG Hua Zhen
|-
| Egypt
| Kamel Saleh
| Mansour & Co.
| Allied for Accounting and Auditing (Emad Ragheb)
| Hazem Hassan
|-
| El Salvador
| DTT El Salvador, S.A. de C.V.
| PricewaterhouseCoopers El Salvador
| Ernst & Young El Salvador, S.A. de C.V.
| KPMG, S.A.
|-
| Finland
| Deloitte & Touche Oy
| PricewaterhouseCoopers Oy
| Ernst & Young Oy
| KPMG Oy Ab
|-
| Ghana
| Deloitte
| PwC
| EY
| KPMG
|-
| Hong Kong
| Deloitte
| PricewaterhouseCoopers
| Ernst & Young
| KPMG
|-
| India
| Deloitte Haskins & Sells, Deloitte Haskins & Sells LLP, P C Hansotia, C C Chokshi & Co, S.B. Billimoria, M.Pal & Co., Fraser & Ross and Touche Ross & co and A.F Ferguson, Deloitte Touche Tohmatsu, Deloitte & Touche Consulting, Deloitte Audit & Enterprise Risk Services
| Price Waterhouse, Price Waterhouse & Co., Lovelock & Lewes, and Dalal & Shah, PricewatershouseCoopers, PricewaterhouseCoopers Service Delivery Centre
| S.R.Batliboi & Co. LLP, S.R.Batliboi & Associates LLP, S.V.Ghatalia & Associates LLP, S R B C & CO LLP, Ernst & Young LLP, PDS Legal
| KPMG (Registered), KPMG India Private Limited, KPMG Advisory Services Pvt Ltd, BSR & Co LLP, BSR & Associates LLP, BSR And Company, BSR & Co., BBSR and Co., BSSR & Co., BSR And Associates, Advaita Legal, SMA & Associates
|-
| Indonesia
| KAP Satrio Bing Eny & Rekan
| KAP Tanudiredja, Wibisana, Rintis & Rekan
| KAP Purwantono, Sungkoro & Surja
| KAP Sidharta Widjaja & Rekan
|-
| Israel
| Deloitte Brightman Almagor Zohar
| Kesselman & Kesselman, PwC Israel
| Kost, Forer, Gabbay & Kasierer (Ernst & Young Israel)
| KPMG Somekh Chaikin
|-
| Italy
| Deloitte Touche Tohmatsu
| PricewaterhouseCoopers
| Reconta Ernst & Young SpA, Ernst & Young Financial Business Advisors SpA,
| KPMG S.p.A., KPMG Advisory S.p.A., KPMG Fides S.p.A.
|-
| Japan
| Deloitte Touche Tohmatsu<br />''Kansa Houjin Tohmatsu''
| PricewaterhouseCoopers Aarata<br />''Aarata Kansa Houjin''
PricewaterhouseCoopers Kyoto<br/>
| Ernst & Young ShinNihon LLC<br />''ShinNihon Yugen-sekinin Kansa Houjin''
| KPMG AZSA LLC (formerly KPMG AZSA & Co.)<br />''Azsa Kansa Houjin''
|-
| Jordan
| Deloitte Touche (M.E)
| PwC
| Ernst & Young
| KPMG
|-
| Kazakhstan
| Deloitte
| PwC
| EY
| KPMG
|-
| Kenya
| Deloitte & Touche (E.A)
| PwC
| Ernst & Young
| KPMG
|-
| Kyrgyzstan
| Deloitte
| -
| EY
| KPMG
|-
| Lebanon
| Deloitte Touche (M.E)
| PwC
| Ernst & Young
| KPMG PCC
|-
| Malaysia
| Deloitte KassimChan
| PricewaterhouseCoopers
| Ernst & Young
| KPMG
|-
| Malta
| Deloitte
| PwC
| EY Malta
| KPMG
|-
| Mexico
| Galaz, Yamazaki, Ruiz Urquiza, S.C.
| PricewaterhouseCoopers México
| Mancera S.C.
| KPMG Cárdenas Dosal, S.C.
|-
| Mongolia
| Deloitte Onch
| PwC
| Ernst & Young Mongolia
| KPMG
|-
| Morocco
| Deloitte Touche (M.E)
| PwC
| Ernst & Young
| KPMG
|-
| Nigeria
| Akintola Williams Deloitte
| PwC Nigeria
| Ernst & Young
| KPMG
|-
| Pakistan
| Deloitte Yousuf Adil
| A. F. Ferguson & Co.
| EY Ford Rhodes
| KPMG Taseer Hadi & Co.
|-
| Palestinian Territories
| Deloitte Touche (M.E)
| PwC
| Ernst & Young
| none
|-
| Peru
| DELOITTE & TOUCHE SRL
| PRICEWATERHOUSECOOPERS S.CIVIL DE R.L.
| ERNST & YOUNG SRL
| KPMG SAC
|-
| Philippines
| Navarro Amper & Co (formerly Manabat Delgado Amper & Co.)
| Isla Lipana & Co.
| SyCip Gorres Velayo & Co.
| R.G. Manabat & Co. (formerly Manabat Sanagustin & Co.)
|-
| Poland
| Deloitte
| PwC
| EY
| KPMG
|-
| Romania
| Deloitte Audit S.R.L., Deloitte Tax S.R.L., Deloitte Consultanta S.R.L., Deloitte Evaluare S.R.L. Deloitte Fiscal Representative S.R.L. and Reff & Associates SCA (jointly referred to as "Deloitte Romania")<ref>http://www.deloitte.com/view/en_RO/ro/about/index.htm</ref>
| PricewaterhouseCoopers Audit SRL, PricewaterhouseCoopers Tax Advisors & Accountants SRL, PricewaterhouseCoopers Management Consultants SRL, PricewaterhouseCoopers Business Recovery Services IPURL, PricewaterhouseCoopers Servicii SRL<ref>{{cite web|url=http://www.pwc.ro/en/about_us/site_provider.jhtml|title=About site provider|author=PricewaterhouseCoopers|work=PwC}}</ref>
| Ernst & Young Romania SRL<ref>{{cite web|url=http://www.ccer.ro/Ernst-Young-Romania-SRL*memberID_49-members_details |title=Members :: CCE-R |work=ccer.ro |deadurl=yes |archiveurl=https://web.archive.org/web/20140810160142/http://www.ccer.ro/Ernst-Young-Romania-SRL*memberID_49-members_details |archivedate=2014-08-10 |df= }}</ref>
| KPMG Romania S.R.L.<ref>{{cite web|url=http://www.kpmg.com/ro/en/pages/default.aspx|title=Home - KPMG - RO|work=kpmg.com}}</ref>
|-
| Saudi Arabia
| Deloitte & Touche Bakr Abulkhair & Co
| PricewaterhouseCoopers LLP
| Ernst & Young Saudi Arabia
| KPMG Al Fozan & Partners
|-
| South Africa
| Deloitte
| PwC
| EY
| KPMG
|-
| South Korea
| Anjin LLC
| Samil LLC
| Hanyoung LLC
| Samjong LLC
|-
| Sri Lanka
| SJMS Associates (independent correspondent firm)
| PwC
| Ernst & Young
| KPMG
|-
| Sweden
| Deloitte Touche Tohmatsu
| Öhrlings PricewaterhouseCoopers
| Ernst & Young
| KPMG
|-
| Syria
| Deloitte (M.E) - Nassir Tamimi Chartered Accountant
| PricewaterhouseCoopers
| Abdul Kader Hussarieh and partners
| Mejanni & Co. Chartered Accountants and Consultants LLC
|-
| Thailand
| Deloitte Touche Tohmatsu Jaiyos
| PricewaterhouseCoopers
| Ernst & Young
| KPMG Phoomchai
|-
| Taiwan
| Deloitte
| PricewaterhouseCoopers Taiwan
| Ernst & Young
| KPMG
|-
| Turkey
| DRT Bagimsiz Denetim ve S.M.M. A.S.
| Basaran Nas Bagimsiz Denetim ve SMMM A.S.
| Güney Bağımsız Denetim ve S.M.M. A.Ş., Kuzey Y.M.M. Denetim A.Ş., Ernst Young Kurumsal Finansman Danışmanlık A.Ş., BEY S.M.M. A.Ş.
| Akis Bagimsiz Denetim ve S.M.M. A.S.
|-
|Uzbekistan
|Deloitte Touche Tohmatsu
|ASC PricewaterhouseCoopers
|Ernst & Young LLC 
|
|-
|Venezuela
|Lara Marambio & Asociados - Deloitte 
|Espiñeira Pacheco & Asociados - PricewaterhouseCoopers
|EY
|Rodriguez Velasquez & Asociados - KPMG
|-
| Uganda
| Deloitte
| PwC
| EY
| KPMG
|-
| Zimbabwe
| Deloitte
| PwC
| EY
| KPMG
|-
|New Zealand
|Deloitte
|PwC
|EY
|KPMG
|}

==See also==
* [[Luxembourg leaks]]

==References==
{{reflist|30em}}

==External links==
* {{Official website|http://www.deloitte.com/ }} [[Deloitte]]
* {{Official website|http://www.ey.com}} [[Ernst & Young]]
* {{Official website|http://www.pwc.com}} [[PriceWaterhouseCoopers]]
* {{Official website|http://www.kpmg.com/ }} [[KPMG]]
* [[International Consortium of Investigative Journalists]], [http://www.icij.org/project/luxembourg-leaks/big-4-audit-firms-play-big-role-offshore-murk Big 4 Audit Firms Play Big Role in Offshore Murk],
* [https://www.youtube.com/watch?x-yt-cl=84503534&x-yt-ts=1421914688&v=xV3eHv_DbyA G8 And Tax Avoidance - Spinwatch Exclusive on the role of the Big Four in lawmaking]

{{Big4}}
{{Consulting}}

{{DEFAULTSORT:Big Four Auditors}}
[[Category:Accounting firms]]
[[Category:Tax avoidance]]