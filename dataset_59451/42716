{|{{Infobox Aircraft Begin
 | name=Eagle DW.1
 | image=Eagle DW.1 N8801X Grangeville ID 24.06.94R.jpg
 | caption=1980-built DW.1 agricultural biplane at [[Grangeville, Idaho]] in June 1994
}}{{Infobox Aircraft Type
 | type=[[Agricultural aircraft|Agricultural]] [[biplane]]
 | national origin=[[United States]]
 | manufacturer=Eagle Aircraft Company
 | designer=Dean Wilson
 | first flight=1977
 | introduced=1979
 | retired=
 | status=Production completed (1983)
 | primary user=Crop-spraying firms
 | number built=95
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Eagle DW.1''' is an [[United States|American]]-built single-seat agricultural biplane of the late 1970s.
 
==Development==

The DW.1 was designed by Dean Wilson of the [[Eagle Aircraft Company]] of [[Boise]], [[Idaho]] and the first example first flew in 1977.  The Eagle is a single-seat agricultural [[biplane]] with tapered long-span wings, an enclosed single-seat cockpit and fixed tailwheel undercarriage. The prototype was fitted with a [[Jacobs R-755|Jacobs R-755-B2]] radial engine but later examples were fitted with other more modern powerplants.

Production was sub-contracted to [[AviaBellanca Aircraft|Bellanca Aircraft]] of [[Alexandria, Minnesota]].<ref>Simpson, 2001, p. 204</ref> The type certificate was sold to Alexandria Aircraft LLC in 2002, but no further production has been undertaken.<ref>Simpson, 2005, p. 121</ref>

==Operational history==

95 examples of the DW.1 were built between 1979 and 1983.  Their use has been predominantly in the agricultural aviation field as crop dusters and sprayers.  In 2001, over 40 examples remained in use throughout the United States.<ref>Simpson, 2001, p. 204</ref>

==Variants==

;Eagle 220 : {{convert|220|hp|kW|0|abbr=on}} [[Continental W670-6N]] radial engine;
;Eagle 300 : {{convert|300|hp|kW|0|abbr=on}} [[Lycoming IO-540-M1B5D]] flat-six engine.

==Specifications (Eagle 220)==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aerospecs
|ref=Simpson, 2001, p. 204
|met or eng?=eng
|crew=one
|capacity=
|length m=
|length ft=26
|length in=0
|span m=
|span ft=55
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=10
|height in=7
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=2,650
|gross weight kg=
|gross weight lb=5,400
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Continental W670-6N]]
|eng1 kw=164<!-- prop engines -->
|eng1 hp=220
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=
|max speed mph=110
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=90
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=180
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

}}

==References==
;Notes
{{reflist}}
;Bibliography
{{Commons category|Eagle Aircraft Eagle}}
{{refbegin}}
*{{cite book|last=Simpson|first=Rod|title=Airlife's World Aircraft|year=2001|publisher=Airlife Publishing Ltd|ISBN=1-84037-115-3}}
*{{cite book|last=Simpson|first=Rod|title=General Aviation Handbook|year=2005|publisher=Midland Publishing|ISBN=1-85780-222-5}}
{{refend}}

==External links==

[[Category:United States civil utility aircraft 1970–1979]]
[[Category:United States agricultural aircraft 1970–1979]]