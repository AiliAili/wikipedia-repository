{{Infobox horse
|name= Cerbat Mustang
|image=Whb.Par.3815.Image.200.198.1.gif.jpeg
|image_caption=Cerbat Mustangs on the Cerbat HMA, Arizona
|features= Build is similar to the classic Andalusian horse. 
|altname= 
|nickname=
|country=[[United States|USA]]
|group1= 
|std1=
|group2=
|std2=
}}
The '''Cerbat Mustang''' is a [[feral horse]] breed that originated in Arizona and can still be found on the [[List of BLM Herd Management Areas|Cerbat HMA]] in that state. Their main coat colors are [[Chestnut (coat)|chestnut]], [[Bay (horse)|bay]], and [[Roan (color)|roan]]. While their [[phenotype]] is similar to the classic [[Colonial Spanish Horse]], the actual origin of Cerbat Mustangs is unclear, but they have been identified by DNA testing as of Colonial Spanish Horse ancestry and they are recognized by the [[Spanish Mustang]] registry.<ref name=Registry>{{cite web|url= https://books.google.com/books?id=lW0Qv3vFQIAC&pg=PA196&dq=Cerbat+Mustang&hl=en&sa=X&ved=0ahUKEwjNusq22tzKAhUP62MKHfZXBlIQ6AEIOTAF#v=onepage&q=Cerbat%20Mustang&f=false|title=Knowing horses: Q and As to Boost your Equine IQ}}</ref>  Cerbats possess the ability to [[ambling|gait]].<ref name=Gait>{{cite web|url= https://books.google.com/books?id=WJCTL_mC5w4C&pg=PA365&dq=Cerbat+Mustang&hl=en&sa=X&ved=0ahUKEwjNusq22tzKAhUP62MKHfZXBlIQ6AEIJzAC#v=onepage&q=Cerbat%20Mustang&f=false |title= The Encyclopedia of Historic and Endangered Livestock and Poultry breeds}}</ref>

==Characteristics==
The Cerbat Mustang is said to be similar to the classic [[Andalusian horse]] in [[equine conformation|conformation]],<ref name=Ryden/> and has characteristics consistent with Spanish horse type.<ref name="Storey"/>  They are {{hands|14|to|15}} tall,<ref name=Hendricks>{{cite web|url=https://books.google.com/books?id=CdJg3qXssWYC&pg=PA115|title=International Encyclopedia of Horse Breeds|last=Hendricks|first=Bonnie L.|publisher=University of Oklahoma Press|date=2007|isbn=9780806138848|accessdate=31 January 2016}}</ref> and average 750 to 800 pounds in weight.<ref name=BLM/> The first generation in captivity, however, was said to stand only {{hands|13.2}}.<ref name=Dutson>{{cite book|title=Storey's Illustrated Guide to 96 Horse Breeds of North America|author=Dutson, Judith|publisher=Storey Publishing|year=2005|isbn=1580176135|pages=217–219}}</ref>  Cerbats are most commonly bay and roan, but there are some [[gray (horse)|grays]], [[black (horse)|black]]s, [[chestnut (coat)|sorrel]]s, and [[dun gene|duns]] found on the Cerbat HMA in Arizona.<ref name=BLM/>  Those in private hands are said to only be bay, roan or chestnut. [[horse markings|White markings]] on legs and heads are common.<ref name=Hendricks/>  Roan Cerbat foals are born roan, unlike some roan [[foal]]s of other breeds that are born a dark color and "roan out" as they get older, fading in color.<ref name="Storey">{{cite web|url=https://books.google.com/books?id=PS6zop4lVSUC&pg=PA96|title=Storey's Illustrated Guide to 96 Horse Breeds of North America|publisher=|accessdate=31 January 2016}}</ref>  They are considered calm, quiet and intelligent.<ref name=Hendricks/> Some Cerbats can perform intermediate ambling gaits.<ref name=Gait/>

They have been identified as being descended from the [[Colonial Spanish Horse]] and the [[feral horse]] bands today are found in [[Arizona]].<ref name=BLM>{{cite web|url=http://www.blm.gov/az/st/en/prog/whb/hmas/cerbat.print.html|title=Cerbat HA - BLM Arizona|date=22 May 2015|publisher=|accessdate=31 January 2016}}</ref> For this reason, they are accepted by the Spanish Mustang registry.<ref name=Hendricks/> There is no formal Cerbat horse registry at present and the breed is very rare, with a high number of 45 horses registered,<ref name="Storey"/> and a stable number of about 70 found on the Cerbat HMA.<ref name=BLM/>

==History==
[[File:Andalusian 2.jpg|thumb|The [[Andalusian horse]], shown above, is believed to be an ancestor of the Cerbat Mustang]]

It is hypothesized that the Cerbat Mustang is descended from [[Colonial Spanish Horse|Spanish Mustangs]] brought to the US in the 1500s.  Other hypotheses are that they arrived in the area in the 1700s or were abandoned by ranchers in the early 1800s.<ref name=BLM/> They have been blood tested and are determined to be of [[Colonial Spanish horse]] ancestry and are described as of "classic Andalusian" type.<ref name=Ryden>{{cite web|url=https://books.google.com/books?id=bE-iL3YtmlkC&pg=PA320|title=America's Last Wild Horses|last=Ryden|first=Hope|publisher=Globe Pequot|date=2005|isbn=9781592288731| accessdate=31 January 2016}}</ref> The Cerbat herds were documented as well-established by 1860.  In 1971, numbers dropped dramatically during a drought when livestock [[rancher]]s shot many free-roaming horses, believing that they were competing with [[cattle]] for scarce water resources.  At that time, about 18 Cerbats were captured and preserved in private herds.  After the passage of time, about 20 horses were found to remain in the area.  In 1990, another remnant feral population was discovered by the [[Bureau of Land Management]] and blood testing determined that they were related to the animals that had been preserved in private herds.<ref name=Hendricks/><ref name=Bureau>{{cite web|url=https://www.highbeam.com/doc/1P1-79289519.html|title=Badlands new home for wild herd}}</ref>
Cerbat stallions in captivity have been bred to mares of other Spanish Mustang ancestry to keep the bloodline alive without [[inbreeding]].<ref name=Dutson/>

Their name comes from the [[Cerbat Mountains]] where the Cerbat herd is located.<ref name=Hendricks/>
{{clear}}

==Uses==
Cerbat Mustangs are used in many ways, including [[endurance riding]], [[eventing]], [[Trail (horse show)|trail classes]], [[Ranch|ranch and cattle work]], [[team penning]], [[Team roping|roping]], and other [[western riding|western]] competition.<ref name=Riding>{{cite web|url=https://books.google.com/books?id=pd1Kbhs2ltwC&pg=PA199&dq=Cerbat+Mustang&hl=en&sa=X&ved=0ahUKEwiYi9y6n9rKAhXBKGMKHZ55ARUQ6AEIJDAB#v=onepage&q=Cerbat%20Mustang&f=false|title=Horse breeds}}</ref>

== References ==
{{Reflist}}

== External links ==
*http://lenjohnsonproductions.blogspot.com/2014/10/cerbat-spanish-mustang-history.html
*http://www.angelfire.com/az/xochitl/Cerbats.html

{{equine}}

[[Category:Horse breeds]]
[[Category:Feral horses]]
[[Category:Horse breeds originating in the United States]]