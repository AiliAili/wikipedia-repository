{{Orphan|date=December 2015}}

{{Infobox journal
| title = Field Methods
| cover = [[File:Field Methods.tif]]
| editor = H. Russell Bernard
| discipline = [[Research Methods]]
| former_names = 
| abbreviation = Field Methods
| publisher = [[SAGE Publications]]
| country = United Kingdom
| frequency = Quarterly
| history = 1989–present
| openaccess = 
| license = 
| impact = 0.875  
| impact-year = 2013
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200810
| link1 = http://fmx.sagepub.com/content/current
| link1-name = Online access
| link2 = http://fmx.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 41850329 
| LCCN = sn99008858 
| CODEN = 
| ISSN = 1525-822X 
| eISSN = 1552-3969 
}}

'''''Field Methods''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Social Sciences]]. The journal's [[Editor-in-Chief|editor]] is H. Russell Bernard ([[University of Florida]]). It has been in publication since 1989 and is currently published by [[SAGE Publications]].

== Scope == 
''Field Methods'' is a source of information for scholars, students and professionals alike. The journal publishes articles including descriptions of methodological advances, advice on the use of specific field techniques and help with both qualitative and quantitative methods. The journal also contains essays and book and software reviews.

== Abstracting and indexing ==
''Field Methods'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.875, ranking it 34 out of 93 journals in the category ‘Social Sciences, Interdisciplinary’.<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Social Sciences, Interdisciplinary |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref> and 35 out of 82 journals in the category ‘Anthropology’.<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Anthropology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://fmx.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]