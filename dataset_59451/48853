{{Infobox brewery
| name = Janssen & Bechly Bierbrauerei
| logo = Janssen&Bechly.jpg
| logo_size = 150
| location = [[Neubrandenburg]], Germany
| owner = Hans Janssen, Friedrich Bechly
| other_products = Bochbier, Edelbrau, Kronenbrau, Malzbier, Pilsner, Tafelbier
| opened = 1911
| closed = 1995
}}

'''Janssen & Bechly Brewery''' produced beer at [[Neubrandenburg]], [[Germany]] from 1912 until the company was nationalized by [[East Germany]] in 1949. The brewery continued to produce beer under the Neubrandenburg and Nordbräu brand names until the company ended production in 1995. The Nordbräu brand was sold to the [[Mecklenburg]] brewery Lübzer in 1996.

== History ==

The origin of the Janssen & Bechly Brewery began with an older Neubrandenburg brewery established by Franz Moncke in 1847 and purchased by Friedrich Bechly about 1900. Franz Moncke's brewery was located on Treptower Strasse, Neubrandenburg, and was popular for its Bavarian style [[bottom-fermenting]] beer.<ref name="nordbrau" /> Friedrich and Hans Janssen owned another brewery on Badstuberstrabe in [[Rostock]], Germany. The entrepreneurs joined efforts and founded the Janssen & Bechly Brewery at Neubrandenburg in 1911.<ref name="nordbrau">[http://kingsizebox.livejournal.com/186157.html '''''Nordbrau''''', Russian Federation article on LiveJournal.com, May 21, 2012]</ref><ref name="history">[http://www.eventom.com/locations/brauerei.html Neubrandenburg brewery history on Eventom.com]</ref> A new brewery constructed at 49 Demminer Street, Neubrandenburg was opened in 1912.<ref name="Demminer">[http://neubrandenburg-touristinfo.de/?id=21 Official Neubrandenburg website noting the opening of the new Janssen & Bechly Brewery in 1912]</ref>
The Janssen & Bechly Brewery was established as an [[Aktiengesellschaft]] (joint-stock company) on January 25, 1922; capitalized with 1100 bearer shares of 1000 marks each. Capital was increased in 1923, and the stock was split 5:1 on January 13, 1925. Capital was increased again in 1941, but due to the impact of World War II the last annual meeting of the company was August 12, 1943.<ref name="capital">[http://www.albert-gieseler.de/dampf_de/firmen4/firmadet47026.shtml Janssen & Bechly stock information]</ref>
The company was [[nationalized]] in 1949 as a [[Volkseigener Betrieb]] (state-owned operation) and renamed VEB Brauerei Neubrandenburg, producing beer under the Neubrandenburg brand name.<ref name="nordbrau" /><ref name="history" /> The company was later reformed as VEB Nordbräu Neubrandenburg<ref name="Einheit">'''Einheit: '''Journal for Theory and Practice of Scientific Socialism, Vol. 34, page 620, 1978</ref> and produced beer under the Nordbräu brand name.
Following [[German reunification]] in 1990, the brewery was sold to private investor Peter Roth in 1991, and subsequently failed, closing business in 1995. The Nordbräu brand was sold for 8 million Marks to Mecklenburg brewery Lübzer in 1996.<ref name="nordbrau" /><ref name="history" />
The original brewery building on Demminer Street, which had stood for nearly 100 years, was demolished in 2009 to make way for a shopping center.<ref name="demolish">[http://www.grehsin.de/files/blog/probono/denkmal/nb/brauerei/nk-13_09-05-06.pdf  ''Die "Alte Brauerei" wird abgerissen'', Neubrandenburg Zeitung, May 6, 2009, page 13]</ref> Only the capstone showing the brewery logo was saved.<ref name="capstone">[http://www.grehsin.de/files/blog/probono/denkmal/nb/brauerei/nk-18_10-04-10_10042010.jpg  ''Schlussstein aus Brauerei-Schutt gerettet'', Neubrandenburg Zeitung, October 4, 2010, page 18]</ref>

== Beer types produced ==

The Janssen & Bechly Brewery has been known to produce [[Bockbier]], Edelbrau, Kronenbrau, [[Malzbier]], [[Pilsner]], Tafelbier, and Weiberbock styles of beer.<ref name="coaster1" />

== Memorabilia ==

The types of historical items available to collectors include bottles & caps,<ref name="caps">[http://www.plopsite.de/_de/pk_seiten/n/neubrandenburg_bierbrauerei_ag_janssen_und_bechly3_2758.html Janssen & Bechly bottle cap examples]</ref> bar coasters,<ref name="coaster1">[http://www.bieretikettenkatalog.de/HTML_IN/N/N078T01.HTM Janssen & Bechly bar coaster examples]</ref><ref name="coaster2">[http://www.klausehm.de/Page937.html Janssen & Bechly bar coaster examples (cont.)]</ref> metal signs<ref name="signs">[http://www.schilderjagd.de/?tag=janssen-bechly Janssen & Bechly tavern sign examples]</ref> and stock certificates.<ref name="stock">[http://www.hwph.de/historische-wertpapiere/artnr-KP01301-janssen-und-bechly-bierbrauerie.html Janssen & Bechly stock certificate example]</ref>

== See also ==

* [[Brewery]]
* [[Microbrewery]]
* [[Beer in Germany]]

== References ==

{{Reflist}}

== External links ==
* {{Official website|http://www.Neubrandenburg.de|City of Neubrandenburg official website}}
* [http://www.eventom.com/locations/brauerei.html Neubrandenburg brewery history on Eventom.com]
* [[:de:Mecklenburgische Brauerei Lübz|Lübzer Brewery article on German Wikipedia]]

[[Category:Beer brewing companies in Germany]]
[[Category:Beer brands of Germany]]