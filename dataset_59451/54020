{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox ANZC team
 |team = Adelaide Thunderbirds
 |logo = Adelaide Thunderbirds.jpg
 |imagesize = 140px
<!--FRANCHISE-->
 |years = 1997–present
 |former_name = 
 |base = [[Adelaide]], Australia
 |region = South Australia
 |main_venue = [[Priceline Stadium]] (<small>3,200</small>)<br><span style=font-size:95%>[[Titanium Security Arena]] (<small>8,000</small>)</span>
 |main_sponsor = Beach Energy
<!--ANZ CHAMPIONSHIP--> 
 |head_coach = Dan Ryan
 |asst_coach = Jane Searle
 |captain =  Erin Bell
 |titles = Two (2010, 2013)
 |best_season = 2010, 2013
 |best_finish = Champions
 |previous_season = 2016
 |previous_finish = 10th<br>5th (Australian Conference)
<!--NETBALL KIT-->
 |body1 = #{{ANZC team colours|Adelaide Thunderbirds|1}}
 |pattern_b1 = thinwhitesides
 |pattern_sk1 = 
 |skirt1 = #{{ANZC team colours|Adelaide Thunderbirds|1}}
<!--FOOTER-->
 |url = http://www.adelaidethunderbirds.com.au
}}

The '''Adelaide Thunderbirds''' are an [[Australia]]n [[netball]] team based in [[Adelaide]] that compete in the Australian [[Suncorp Super Netball]]. The Thunderbirds were formed as one of the foundation teams of the [[Commonwealth Bank Trophy]] (CBT), previously the premier netball league in Australia, which was contested from 1997–2007. After the retirement of the CBT, the Thunderbirds were one of five Australian franchises included in the [[ANZ Championship]]. After the disbanding of the trans-tasman league, Adelaide joined the new look [[Suncorp Super Netball]]. To date, the Thunderbirds have won two CBT titles (1998, 1999) and two ANZ Championship titles (2010, 2013); in addition, they have finished in the top three placings throughout their competitive history.

The Thunderbirds are currently coached by Dan Ryan. The team was captained by veteran wing defence [[Renae Hallinan]] until the conclusion of the 2015 season after which she announced a year off netball due to pregnancy. The current captain is Erin Bell, a two-time netball world championship gold-medalist and multiple ANZ premier. The Home games in the ANZ Championship are usually played at [[Priceline Stadium]] in Adelaide. The team uniform currently is based on pink and white.

== History ==

===Commonwealth Bank Trophy era===
The Thunderbirds became [[South Australia]]'s only team in the competition after the axing of the Adelaide Ravens in 2003, and were based out of [[Priceline Stadium]] in the [[Adelaide]] suburb of [[Mile End, South Australia|Mile End]]. Until 2006 The Thunderbirds were coached by Margaret Angove and captained by [[Peta Scholz]] and [[Laura von Bertouch]]. In 2007 the coach was Tanya Obst with a leadership group of comprising captain [[Laura von Bertouch]], [[Natalie von Bertouch]], Fiona Pointon and [[Kristen Hughes]]. Their team colours in the CBT were silver, pink and blue.

The Thunderbirds were one of the strongest teams in the CBT during that competition's existence, in its early years possibly the strongest, winning the competition twice in that period. While the club slipped to a degree in later years due to increased competition from the [[Melbourne Phoenix]] and [[Sydney Swifts]], it has still never finished lower than third overall.

In 2006, the team's forward line of Hughes, Medhurst, and the von Bertouch sisters were developing in the Australian squad.  The end of the season was disappointing, perhaps partly explained by the sidelining of Scholz (who was in top form) due to pregnancy, when they first dropped a game to the seventh placed [[Hunter Jaegers]], and then were decimated by the [[Sydney Swifts]] in the major semifinal and the grand final.

=== ANZ Championship era ===
The Thunderbirds are South Australia's representative in the ANZ Championship. The club announced a new sponsor and a new look for the 2008 season, aligning themselves with the [[Port Adelaide Football Club]] in a sponsorship deal in which the Thunderbirds wear Port Adelaide's colours of Black, White, Teal and Silver.<ref>[https://archive.is/20120717185415/http://www.anz-championship.com/adelaide-thunderbirds/ Adelaide Thunderbirds - South Australia’s ANZ Championship Team<!-- Bot generated title -->]</ref> The Thunderbirds recruited former [[Melbourne Phoenix]] coach Lisa Alexander to their coaching staff, plus Australian and former [[Sydney Swifts]] player [[Mo'onia Gerrard]] and English International representative [[Geva Mentor]] to their playing squad.<ref>[http://thunderbirds.netball.asn.au/newsitem.asp?id=4880&orgID=628 Adelaide Thunderbirds<!-- Bot generated title -->]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

In the first three years of the ANZ Championship, the Thunderbirds have reached the finals stage. In 2008, they progressed to the preliminary final before being defeated by the Magic in New Zealand. The following year they defeated the Magic to contest the grand final, which the Thunderbirds lost to the Melbourne Vixens. In 2010, the Thunderbirds finished second after the regular season, but defeated minor premiers the [[New South Wales Swifts]] to book themselves a grand final at home. For the 2010 grand final, the Thunderbirds moved to the [[Adelaide Entertainment Centre]], which had a seating capacity of 10,000. Tickets for the 2010 final sold out in 12 minutes. In front of a crowd of 9,300, the Adelaide Thunderbirds defeated the [[Waikato Bay of Plenty Magic]] by 52–42 to claim the 2010 title.<ref>{{cite news |last=Larkin |first=Steve |title=Thunderbirds beat Waikato Bay of Plenty Magic to win ANZ netball Championship Grand Final |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |url=http://www.adelaidenow.com.au/sport/netball/thunderbirds-beat-waikato-bay-of-plenty-to-win-anz-netball-championship-grand-final/story-e6frecvl-1225890415385 |date=2010-07-11 |accessdate=2010-11-12}}</ref> Before the 2011 [[ANZ Championships]] the team lost key players including shooter [[Kate Beveridge]] and defender [[Geva Mentor]], both to the [[Melbourne Vixens]] as well as defender [[Mo'onia Gerrard]] to the [[New South Wales Swifts]]. The team had a disappointing start to the season with several consecutive losses but managed to find form late in the season to finish a respectable 6th place. For the 2012 season the team had retained the core of the playing group while also gaining internationals [[Renae Hallinan]] from the [[Melbourne Vixens]] and Rebecca Bully from the [[New South Wales]]. The team had an extension to name their 12th contracted player although there was talk that Jamaican International [[Janelle Fowler]] could be the late addition to the team, it ended up being Carla Borrego.

== Players ==
{{ANZC roster header |year=2017 |team=Adelaide Thunderbirds}}

<!-- List of players -->
{{ANZC roster player |nat=ENG |name=[[Ama Agbeze]] |pos=GD, GK |dob=12-11-1982 |height=1.83}}
{{ANZC roster player |nat=AUS |name=[[Karyn Bailey]] |pos=GS |dob=28-07-1986 |height=1.92}}
{{ANZC roster player |nat=AUS |name=[[Erin Bell]] |pos=GA, GS, WA |dob=30-04-1987 |height=1.78}}
{{ANZC roster player |nat=AUS |name=Emily Burgess |pos=WD, C |dob=1996 |height=}}
{{ANZC roster player |nat=ENG |name=[[Jade Clarke]] |pos=WD, C, WA |dob=17-10-1983 |height=1.74}}
{{ANZC roster player |nat=AUS |name=Jane Cook |pos=GS |dob=29-10-1997 |height=1.99}}
{{ANZC roster player |nat=JAM |name=Malysha Kelly |pos=GD, GK |dob=14-01-1990 |height=}}
{{ANZC roster player |nat=AUS |name=[[Chelsea Locke]] |pos=C, WA, GA |dob=08-06-1988 |height=1.81}}
{{ANZC roster player |nat=AUS |name=Hannah Petty |pos=C, WA |dob=17-05-1997 |height=1.75}}
{{ANZC roster player |nat=AUS |name=Fiona Themann |pos=GD, GK |dob=18-04-1990 |height=1.84}}


<!-- Coaching staff -->
{{ANZC roster footer
| head_coach  = Dan Ryan 
| assistant coach = Jane Searle
| development_coach (Thunderbabes)  = {{flagicon|AUS}} [[Tania Obst]]
| manager     = Kathy Rogers
| physio      = Scott Smith
| doctor     = Angela Morgan 
| str_Cond    = Michael Riggs
| high_performace_program_manager   = Kate Mayfield
| performance_analyst   = Time Rawlings
| performance_psychologist   = Geof Boylan-Marsland
| dietician   = Adam Zemski
| leadership_consultant   = Karen Slape
| wellbeing_consultant   = Louise Small
| updated     = 28 February 2016
| url_team    = http://adelaidethunderbirds.com.au/team/2016-adelaide-thunderbirds/{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
| url_anzc    = http://anz-championship.com/Teams/adelaide-thunderbirds
}}

==Competitive record==

=== Commonwealth Bank Trophy ===
{| class="navbox wikitable" style="font-size:95%; text-align: center; width: 85%; margin:1em auto;"
|-
! rowspan=2 style="background:#eee; width: 10%;"| Season
! rowspan=2 style="background:#eee; width: 12%;" | Standings
! colspan=3 style="background:#eee; width: 15%;"| Regular season
! rowspan=2 style="background:#eee; width: 30%;"| Finals 
! rowspan=2 style="background:#eee; width: 13%;"| Head coach
|-
! style="background:#eee; width: 5%;"|W
! style="background:#eee; width: 5%;"|D
! style="background:#eee; width: 5%;"|L
|-
! colspan=7  style="background:#1C39BB; color:#9370DB;"| Adelaide Thunderbirds  
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|1997]]
| 2nd
| 9
| 0
| 3
| '''Lost''' Semi Final ([[Melbourne Phoenix|Melbourne]], 42–58) <br> '''Won''' Preliminary Final ([[Sydney Swifts|Sydney]], 65–39) <br> '''Lost''' Grand Final ([[Melbourne Phoenix|Melbourne]], 48–58)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|1998]]
| 1st
| 11
| 1
| 2
| '''Won''' Semi Final ([[Melbourne Phoenix|Melbourne]], 61–53) <br> '''Won''' Grand Final ([[Sydney Swifts|Sydney]], 48–42)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|1999]]
| 1st
| 11
| 1
| 2
| '''Won''' Semi Final ([[Sydney Swifts|Sydney]], 54–36) <br> '''Won''' Grand Final ([[Adelaide Ravens|Adelaide]], 62–30)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2000]]
| 1st
| 12
| 0
| 2
| '''Lost''' Semi Final ([[Melbourne Phoenix|Melbourne]], 52–61) <br> '''Won''' Preliminary Final ([[Sydney Swifts|Sydney]], 51–49) <br> '''Lost''' Grand Final ([[Melbourne Phoenix|Melbourne]], 51–52)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2001]]
| 1st
| 12
| 0
| 2
| '''Lost''' Semi Final ([[Sydney Swifts|Sydney]], 59–63) <br> '''Won''' Preliminary Final ([[Melbourne Phoenix|Melbourne]], 57–40) <br> '''Lost''' Grand Final ([[Sydney Swifts|Sydney]], 32–57)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2002]]
| 2nd
| 12
| 0
| 2
| '''Lost''' Semi Final ([[Melbourne Phoenix|Melbourne]], 38–53) <br> '''Won''' Preliminary Final ([[Sydney Swifts|Sydney]], 46–42) <br> '''Lost''' Grand Final ([[Melbourne Phoenix|Melbourne]], 44–49)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2003]]
| 3rd
| 10
| 0
| 4
| '''Won''' Semi Final ([[Melbourne Kestrels|Melbourne]], 51–46) <br> '''Lost''' Preliminary Final ([[Melbourne Phoenix|Melbourne]], 43–59) 
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2004]]
| 2nd
| 12
| 0
| 2
| '''Lost''' Semi Final ([[Sydney Swifts|Sydney]], 43–49) <br> '''Lost''' Preliminary Final ([[Melbourne Phoenix|Melbourne]], 43–57) 
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2005]]
| 3rd
| 12
| 0
| 2
| '''Won''' Semi Final ([[Melbourne Kestrels|Melbourne]], 68–45) <br> '''Lost''' Preliminary Final ([[Sydney Swifts|Sydney]], 43–63) 
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2006]]
| 2nd
| 11
| 0
| 3
| '''Lost''' Semi Final ([[Sydney Swifts|Sydney]], 42–52) <br> '''Won''' Preliminary Final ([[Melbourne Phoenix|Melbourne]], 60–53) <br> '''Lost''' Grand Final ([[Sydney Swifts|Sydney]], 36–65)
| Margaret Angove
|- 
! scope="row" style="background:#eee;"| [[Commonwealth Bank Trophy|2007]]
| 3rd
| 8
| 0
| 6
| '''Lost''' Semi Final ([[Sydney Swifts|Sydney]], 43–54)
| Tanya Obst
|-
!colspan=2 style="background:#eee;"|Regular season
| style="background:#eee;" | 120
|style="background:#eee;"| 2
| style="background:#eee;" | 30
|colspan=2 style="background:#eee;"| 4 Minor Premierships
|-
!colspan=2 style="background:#eee;"|Finals
|style="background:#eee;"| 11
|style="background:#eee;"| –
|style="background:#eee;"| 15
|colspan=2 style="background:#eee;"| '''2 Commonwealth Bank Trophies'''
|}

=== ANZ Championship ===
{| class="navbox wikitable" style="font-size:95%; text-align: center; width: 85%; margin:1em auto;"
|-
! rowspan=2 style="background:#eee; width: 10%;"| Season
! rowspan=2 style="background:#eee; width: 12%;" | Standings
! colspan=3 style="background:#eee; width: 15%;"| Regular season
! rowspan=2 style="background:#eee; width: 30%;"| Finals 
! rowspan=2 style="background:#eee; width: 13%;"| Head coach
|-
! style="background:#eee; width: 5%;"|W
! style="background:#eee; width: 5%;"|D
! style="background:#eee; width: 5%;"|L
|-
! colspan=7  style="background:#00AAE4; color:#D3D3D3;"| Adelaide Thunderbirds  
|- 
! scope="row" style="background:#eee;"| [[2008 ANZ Championship season|2008]]
| 3rd
| 9
| 0
| 4
| '''Won''' Semi Final ([[Melbourne Vixens|Melbourne]], 53–48) <br> '''Lost''' Preliminary Final ([[Waikato Bay of Plenty Magic|Waikato]], 49–51)
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2009 ANZ Championship season|2009]]
| 3rd
| 10
| 0
| 3
| '''Won''' Semi Final ([[Southern Steel|Southern]], 51–45) <br> '''Won''' Preliminary Final ([[Waikato Bay of Plenty Magic|Waikato]], 51–37) <br>  '''Lost''' Grand Final ([[Melbourne Vixens|Melbourne]], 46–54)
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2010 ANZ Championship season|2010]]
| 2nd
| 9
| 0
| 4
| '''Won''' Semi Final ([[New South Wales Swifts|NSW]], 52–38) <br> '''Won''' Grand Final ([[Waikato Bay of Plenty Magic|Waikato]], 52–42)  
| <small> Jane Woodlands-Thompson </small>
|-
! colspan=7  style="background:#FF1493; color:#FFFFFF;"| Adelaide Thunderbirds  
|- 
! scope="row" style="background:#eee;"| [[2011 ANZ Championship season|2011]]
| 6th
| 5
| 0
| 8
| Did not qualify
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2012 ANZ Championship season|2012]]
| 4th
| 9
| 0
| 4
| '''Lost''' Semi Final ([[Waikato Bay of Plenty Magic|Waikato]], 48–49)
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2013 ANZ Championship season|2013]]
| 1st
| 12
| 0
| 1
| '''Won''' Semi Final ([[Melbourne Vixens|Melbourne]], 49–39) <br> '''Won''' Grand Final ([[Queensland Firebirds|Queensland]], 50–48)  
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2014 ANZ Championship season|2014]]
| 8th
| 5
| 0
| 8
| Did not qualify
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2015 ANZ Championship season|2015]]
| 7th
| 4
| 0
| 7
| Did not qualify
| <small> Jane Woodlands-Thompson </small>
|- 
! scope="row" style="background:#eee;"| [[2016 ANZ Championship season|2016]]
| 10th
| 2
| 0
| 11
| Did not qualify
| Michelle Den Dekker
|-
!colspan=2 style="background:#eee;"|Regular season
| style="background:#eee;" | 55
|style="background:#eee;"| 0
| style="background:#eee;" | 62
|colspan=2 style="background:#eee;"| 1 Minor Premiership
|-
!colspan=2 style="background:#eee;"|Finals
|style="background:#eee;"| 7
|style="background:#eee;"| –
|style="background:#eee;"| 3
|colspan=2 style="background:#eee;"| '''2 ANZ Championship titles'''
|}

=== Suncorp Super Netball ===
{|class="navbox wikitable" style="font-size:95%; text-align: center; width: 85%; margin:1em auto;"
|-
! rowspan=2 style="background:#eee; width: 10%;"| Season
! rowspan=2 style="background:#eee; width: 12%;" | Standings
! colspan=3 style="background:#eee; width: 15%;"| Regular season
! rowspan=2 style="background:#eee; width: 30%;"| Finals 
! rowspan=2 style="background:#eee; width: 13%;"| Head coach
|-
! style="background:#eee; width: 5%;"|W
! style="background:#eee; width: 5%;"|D
! style="background:#eee; width: 5%;"|L
|-
! colspan=7  style="background:#FF1493; color:#000000;"| Adelaide Thunderbirds
|- 
! scope="row" style="background:#eee;"| [[2017 Suncorp Super Netball season|2017]]
| TBD
| 0
| 0
| 0 
| TBD
| Dan Ryan
|-
!colspan=2 style="background:#eee;"|Regular season
| style="background:#eee;" | 0
|style="background:#eee;"| 0
| style="background:#eee;" | 0
|colspan=2 style="background:#eee;"| 0 Minor Premierships
|-
!colspan=2 style="background:#eee;"|Finals
| style="background:#eee;" | 0
|style="background:#eee;"| 0
| style="background:#eee;" | 0
|colspan=2 style="background:#eee;"| '''0 Super Netball titles'''
|}

== Franchise ==

=== Venues ===
Home games are usually played at [[Priceline Stadium]] in Adelaide, which has a seating capacity of 3,200. Some matches have been played at alternate venues, including the 8,000 seat [[Adelaide Arena]], and the 10,000 seat [[Adelaide Entertainment Centre]]

With its greater spectator capacity, the Entertainment Centre hosted the ANZ Championship Grand Final in both [[2010 ANZ Championship season#Grand Final|2010]] and [[2013 ANZ Championship season#Grand Final|2013]]. In 2010 the Thunderbirds defeated the [[Waikato Bay of Plenty Magic]] 52-42 in front of 9,300 fans, while the 2013 decider, won 50-48 by the Thunderbirds over the [[Queensland Firebirds]], attracted 9,000 fans.

=== Team colours ===
The Thunderbirds adopted primarily silver during their years in the Commonwealth Bank Trophy, from 1997–2007. When the team joined the ANZ Championship, they changed their playing colours to teal with silver and black, in line with the colours of the [[Port Adelaide Power]] AFL team. After 2010, the Thunderbirds changed again, this time adopting pink with white and black. After 2011, the Thunderbirds' main colours became just pink and white.

=== Logo ===
<gallery>
File:Adelthunderbirdslogo.png|Commonwealth Bank Trophy: 1997–2007
File:Adelaide Thunderbirds 2008.jpg|ANZ Championship: {{nowrap|2008–2010}}
File:Adelaide Thunderbirds.jpg|ANZ Championship: from&nbsp;2011
</gallery>

=== Sponsors ===
In 2015 Priceline was named as Premier Partner of the Adelaide Thunderbirds. 

== References ==
{{reflist}}

== External links ==
* [http://www.adelaidethunderbirds.com.au Official website]

{{s-start}}
{{s-sports}}
{{s-bef|before=[[Melbourne Phoenix]]}}
{{s-ttl|title=[[Commonwealth Bank Trophy|Commonwealth Bank Trophy Champions]]|years=1998&ndash;1999}}
{{s-aft|after=[[Melbourne Phoenix]]}}
{{s-bef|before=[[Melbourne Vixens]]}}
{{s-ttl|title=[[ANZ Championship|ANZ Championship Winners]]|years=2010}}
{{s-aft|after=[[Queensland Firebirds]]}}
{{s-bef|before=[[Waikato Bay of Plenty Magic]]}}
{{s-ttl|title=[[ANZ Championship|ANZ Championship Winners]]|years=2013}}
{{s-aft|after=[[Melbourne Vixens]]}}
{{end}}

{{Adelaide Sports Teams}}
{{Suncorp Super Netball}}
{{ANZChampionshipTeams}}
{{CommonwealthBankTrophyTeams}}
{{Adelaide Thunderbirds}}

[[Category:Adelaide Thunderbirds| ]]
[[Category:ANZ Championship teams]]
[[Category:Australian netball teams]]
[[Category:1997 establishments in Australia]]
[[Category:Sports clubs established in 1997]]
[[Category:Sporting clubs in Adelaide]]