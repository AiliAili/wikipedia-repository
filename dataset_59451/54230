{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Fulham
| city                = Adelaide
| state               = sa
| image               = House in suburban Adelaide, SA.JPG
| caption             = A post-war residential house in Fulham.
| pop                 = 2,611
| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41541 |name=Fulham (State Suburb) |accessdate=20 April 2011 | quick=on}}</ref><br>2,571 <small>''(2001 Census)''</small><ref name=ABS2001>{{Census 2001 AUS |id =SSC41526 |name=Fulham (State Suburb) |accessdate=20 April 2011 | quick=on}}</ref>
| est                 = 
| postcode            = 5024<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/fulham |title=Fulham, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=20 April 2011}}</ref>
| area                = 
| dist1               = 8
| dir1                = W
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = City of West Torrens
| stategov            = [[Electoral district of Colton|Colton]] <small>''(2011)''</small><ref>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov              = [[Division of Hindmarsh|Hindmarsh]] <small>''(2011)''</small><ref>{{cite web|url=http://apps.aec.gov.au/esearch |title=Find my electorate |author= |date=15 April 2011 |work= |publisher=Australian Electoral Commission |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110430090535/http://apps.aec.gov.au/esearch |archivedate=30 April 2011 |df=dmy }}</ref>
| near-n              = [[Fulham Gardens, South Australia|Fulham Gardens]]
| near-ne             = [[Lockleys, South Australia|Lockleys]]
| near-e              = [[Lockleys, South Australia|Lockleys]]
| near-se             = [[Lockleys, South Australia|Lockleys]]
| near-s              = [[West Beach, South Australia|West Beach]]
| near-sw             = [[West Beach, South Australia|West Beach]]
| near-w              = [[Henley Beach South, South Australia|Henley Beach South]]
| near-nw             = [[Henley Beach, South Australia|Henley Beach]]
}}
[[File:Weetunga, Fulham.JPG|thumb|''Weetunga'', Fulham, built in 1878<br /> by Samuel White, son of John White]]

'''Fulham''' is a western [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of West Torrens]].

==History==
The area incorporating the current suburb of Fulham was purchased c. 1836 by John White (? –30 December 1860), who named it ''Fulham Farm'' after the suburb of [[Fulham]] in his native [[London]].<ref name=Manning>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/f/f5.htm#fulham |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=20 April 2011}}</ref> The White family home, ''[[Weetunga]]'', built by his son, Samuel White (1835?–16 November 1880), father of [[Samuel Albert White]] (1870–1954) which took three years to build,<ref>Linn, Rob. (1989): ''Nature's Pilgrim. The life and journeys of Captain S.A.White, naturalist, author and conservationist''. SA Government Printer: Adelaide. ISBN 0724365486</ref> remained with the family until placed on the market in 2014,<ref>[http://www.adelaidenow.com.au/realestate/news/iconic-fulham-mansion-weetunga-placed-on-the-market-for-the-first-time-in-its-135year-history/story-fni0ci8n-1226872688883 Iconic Fulham mansion 'Weetunga' placed on the market for the first time in its 135-year history] ''The Advertiser'', 2 April 2014. Retrieved 24 May 2014.</ref> and sold to another South Australian resident for $2.5 million in August 2015.<ref>[http://www.adelaidenow.com.au/messenger/west-beaches/why-ive-sold-the-house-uncle-john-built-135-years-ago/story-fni9llx9-1227477634619 Why I’ve sold the house Uncle John built 135 years ago] ''The Weekly Times'', 10 August 2015. Retrieved 10 August 2015.</ref> The new owner of the home plans on restoring the home back to its former glory and building a wing to the east of the home.

Both Weetunga and ''[[The Oaks, Fulham|The Oaks]]'' in Henley Beach Road are listed on the [[South Australian Heritage Register]].<ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=9134 | title=Dwelling ('Weetunga'), including Main House, former Kitchen, Servants' Quarters, Museum and Laundry | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=11 September 2016}}</ref><ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=9133 | title=Dwelling ('The Oaks') and row of cottages | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=11 September 2016}}</ref>

==Geography==
Fulham sits on a bend in the [[River Torrens]]. The suburb also sits astride the intersection of [[Tapleys Hill Road]] and [[Henley Beach Road]].<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 2,611 persons in Fulham on census night. Of these, 49.8% were male and 50., 2% were female.<ref name=ABS2006/>

The majority of residents (73.5%) are of Australian birth, with other common census responses being [[Italy]] (5.4%) and [[England]] (5.1%).<ref name=ABS2006/>

The age distribution of Fulham residents is skewed towards an older population than the greater Australian population. 74.1% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 25.9% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Community==
The local newspaper is the [[Weekly Times Messenger]]. Other regional and national newspapers such as ''[[The Advertiser (Adelaide)|The Advertiser]]'' and ''[[The Australian]]'' are also available.<ref>{{cite web |url=http://www.newspapers.com.au/SA/ |title=South Australian Newspapers |author= |date= |work=Newspapers.com.au |publisher=Australia G'day |accessdate=20 April 2011}}</ref>

==Facilities and attractions==

===Shopping and dining===
* [[BP]] with car wash, groceries, and espresso coffee
* [[Subway (restaurant)]]
* Lockleys Romeo's [[Foodland (South Australia)|Foodland]]
* [[Hungry Jack's]]
* [[Dominos Pizza]]

===Parks===
The largest greenspace in Fulham is '''[[Torrens Linear Park|Linear Park]]''', lying along the [[River Torrens]] on the suburb's southern boundary.<ref name=UBD/>

==Transportation==

===Roads===
Fulham is serviced by [[Henley Beach Road]], connecting the suburb to [[Adelaide city centre]], and [[Tapleys Hill Road, Adelaide|Tapleys Hill Road]], one of the major [[arterial road]]s in the western suburbs of Adelaide.<ref name=UBD/>

===Public transport===
Fulham is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date=12 January 2011 |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>
*H22 to Henley Beach South
*H30 to West Lakes
*H32 to Henley Beach South
*H31 to Henley Square
*All run through Fulham

===Bicycle routes===
A bicycle path extends along [[Torrens Linear Park|Linear Park]].<ref name=UBD/>

==See also==
*[[List of Adelaide suburbs]]

==References==
{{Reflist}}|40em

==External links==
{{commons category|Fulham, South Australia}}
*{{cite web |url=http://www.wtcc.sa.gov.au |title=City of West Torrens|author= |date= |work=Official website |publisher=City of West Torrens |accessdate=20 April 2011}}

{{Coord|-34.927|138.512|format=dms|type:city_region:AU-SA|display=title}}
{{City of West Torrens suburbs}}

[[Category:Suburbs of Adelaide]]