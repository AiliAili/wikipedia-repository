{{more footnotes|date=August 2014}}

The '''bilingual method''' of [[foreign language teaching]] was developed by  C.J. Dodson (1967) as a counterpart of the [[audiovisual method]]. In both methods the preferred basic [[Writing|texts]] are [[dialogues]] accompanied by a picture strip. The bilingual method, however, advocates two revolutionary principles based on the results of scientifically controlled [[experiment]]s in [[Primary school|primary]] and [[secondary schools]]. In contrast to the audiovisual method and the [[direct method (education)|direct method]], the printed text is made available from the very beginning and presented simultaneously with the spoken sentence to allow learners to see the shape of individual [[word]]s. Also, from the outset meanings are conveyed bilingually as utterance equivalents in the manner of the [[sandwich technique]], thus avoiding meaningless and hence tedious parroting of the learning input. The pictures are seen primarily as an aid to recall and practice of the related dialogue sentences rather than as conveyors of meaning. The [[mother tongue]] is again used in the oral manipulation of [[grammatical]] structures, i.e. in bilingual pattern drills.

== Structure ==

The architecture of the bilingual method is best understood  as a traditional three-phase structure of [[presentation]] – [[Practice (learning method)|practice]] – [[Production (education)|production]]. A lesson cycle starts out with the reproduction of a dialogue, moves on to the oral variation and recombination of the dialogue sentences, and ends up with an extended application stage reserved for [[message-oriented communication]].<ref>Byram, Michael, and Hu, Adelheid.  eds. (2013: 89) Routledge Encyclopedia of Language Teaching and Learning. London/New York: Routledge</ref> The method is listed in Eppert’s ''Lexikon'' (1973: 171) under the headword ''Konversation'', where its eight teaching steps are described <ref>Eppert, Franz (1973) Lexikon des Fremdsprachenunterrichts. Bochum: Ferdinand Kamp</ref> "The eight steps lead from [[imitation]] to [[free conversation]]," i.e. unlike the [[grammar-translation method]],  but like the [[direct method (education)|direct method]] and the [[audio-lingual method]] it focuses on the development of oral skills.<ref>Alexander, L. R. (1978) An Introduction to the Bilingual Method of Teaching Foreign Languages. In: Foreign Language Annals, 11: 305–313. doi: 10.1111/j.1944-9720.1978.tb00043.x</ref>

== Classroom research ==

Dodson’s experimental data – several modes of presenting dialogues were tested – have been confirmed by subsequent [[research]], for example by a school-year long experiment of teaching [[French language|French]] to [[Dutch people|Dutch]] learners (Meijer 1974), which compared the bilingual method with an audiovisual approach. A laboratory study with [[Japanese people|Japanese]] learners of [[English language|English]] also confirmed  Dodson’s results (Ishii et al. 1979). Similar results were reported by Sastri (1970) and Walatara (1973). Feasibility studies were undertaken by Kaczmarski (1979) in [[Poland]], by [[Wolfgang Butzkamm]] (1980) for the teaching of English to [[German language|German]] speakers in secondary schools, by Kasjan (1995) for the teaching of German to Japanese learners at [[university]] level, and by Moorfield (2008) for the teaching of the [[Māori language|Maori language]].

== History ==

Although Dodson’s work inspired researchers from various [[countries]], the bilingual method has been neglected by the [[mainstream]], as evidenced in the absence of any mother tongue role in recognised overviews of [[L2 approaches]] and methods such as Richards & Rodgers (1987).<ref>Richards, J.C. & Rogers, T.S. (1987) "The nature of approaches and methods in language teaching". In: Approaches and Methods in Language Teaching. CUP, pp 14-30</ref> However, Butzkamm & Caldwell (2009) have taken up Dodson’s seminal ideas and called for a [[paradigm shift]] in foreign language teaching. This call was repeated by Hall & Cook in their state-of-the-art article (2012:  299): "The way is open for a major paradigm shift in language teaching and learning"<ref>Hall, Graham & Cook, Guy  (2012)  "Own-language use in language teaching and learning: the state of the art." In: Language Teaching 45.3, 271 – 308</ref>

== Principles of The Bilingual Method ==
* The understanding of words and sentences in foreign languages can be made easier by the use of mother tongue.
* There is no need to create artificial situations for explaining the meaning of words and sentences of the target language.
* Bilingual method is the combination of the [[Direct method (education)|Direct method]] and the [[Grammar translation method]].<ref>{{cite web|title=Bilingual method|url=http://www.tetsuccesskey.com/2015/01/bilingual-method-ctet.html|website=www.tetsuccesskey.com|accessdate=27 September 2015}}</ref>

== Advantages of The Bilingual Method ==

'''Students become functional bilinguals'''

When the students aim to become fully bilingual in terms of language learning, this method is considered to be the appropriate one. When the students begin with the language learning process, their success in the learning depends upon the competence and confidence of their language teacher. As she moves from L1 to L2, the students imitate her and learn.

'''Never miss out on a Lesson'''

According to this method acquisition of the mother tongue is very important for language learning process. When the mother tongue is firmly established in the minds of the students by the age of 7 or 8, it becomes easy to learn difficult words and grammar. Thus this method helps to save time by not creating artificial situations unnecessarily to explain or convey meanings in English.

'''Give importance to other languages'''

In this method, importance is given to the mother tongue and its culture. Thus it does not lead to substitution of one means of communication for another.

'''Accessibility'''

This method of language learning ensures accessibility. When students start learning a language using this method, they find a level of familiarity. Through the use of the mother tongue, the teacher ensures that the learning is happening .

'''Discipline'''

Many new English language teachers face the difficulty to handle the students and make them feel as comfortable as with the local teacher. Learning the local language is considered as the sure way to improve behavior management skills. It also helps in delivering the instructions related to lesson activities. If the concepts are explained in students' L1, then the new learners to English language will be able to grasp more knowledge about grammar and vocabulary. Thus it helps the students to be more efficient and faster.

'''It's the teacher's tool '''

In the bilingual method, as the native language is used in the classroom, it is important to note that it's predominantly the teacher who makes use of L1. The students will not be using their native tongue much in the classroom.

'''Builds strong foundation for reading, right from the start'''

The bilingual method uses written form of the language which allows students learn the shapes of the words as they repeat the words orally.<ref>{{cite web|title=Bilingual method|url=http://purwarno-linguistics.blogspot.in/2006/01/bilingual-method_19.html|accessdate=27 September 2015}}</ref><ref>{{cite web|title=Bilingual method|url=https://aiiulistya.wordpress.com/2013/06/24/the-bilingual-method/}}</ref>

== Disadvantages of The Bilingual Method ==
* If the teacher fails to carry out this method properly, then it can degenerate into pure translation method.
* This method can confuse the learner while contrasting the features of two languages.
* The teacher must be fluent in both the languages in order to make the concepts clear.
* Students may develop dependency on their mother tongue.
* Slows down learning process and takes longer time to learn and be proficient in the target language.<ref>{{cite web|title=Bilingual method of teaching english|url=http://www.fluentu.com/english/educator/blog/bilingual-method-of-teaching-english/|website=www.fluentu.com|accessdate=27 September 2015}}</ref>

== References ==
{{reflist}}
* Alexander, Loren & Butzkamm, Wolfgang  (1983)  Progressing from imitative to creative exercises. A presentation of the bilingual method. In: British Journal of Language Teaching 21.1, 27-33.
* Butzkamm, Wolfgang (1980), Praxis und Theorie der bilingualen Methode. Heidelberg: Quelle & Meyer.
* Butzkamm, Wolfgang & Caldwell, John A.W. (2009) The Bilingual Reform: A Paradigm Shift in Foreign Language Teaching. Tübingen: Narr.
* Caldwell, John A.W. (1990) "Analysis of the theoretical and experimental support for Carl Dodson’s bilingual method" In: Journal of multilingual and multicultural development, 11.6, 459-479.
* Dodson, C.J. (1967/1972) Language Teaching and the Bilingual Method. London: Pitman.
* Ishii, Takeo / Kanemitsu, Y. / Kitamura, M. / Masuda, HZ. / Miyamoto, H. (1979) "An Experiment on the Acquisition and Retention of Sentence-Meaning and the Imitation Performance". In: Journal of the Kansai Chapter of the Japan English Language Education Society, 3, 52-59.
* Kaczmarski, S. P. (1979)  "A bilingual approach to foreign language teaching" In: Glottodidactica 12, 127 – 136.
* Kasjan, Andreas (1995)  "Die bilinguale Methode im Deutschunterricht für japanische Studenten I: Die Einführung in die Aussprache und das unterrichtliche Funktionsvokabular", Dokufutsu Bungaku Kenkyû, 45, 159-171.
* Meijer, Tjeerd (1974) De globaal-bilinguale en de visualiserende procedure voor de betekenisoverdracht . Een vergelijkend methodologisch onderzoek op het gebied van het aanvangsonderwijs frans (Academisch Proefschrift). Amsterdam: Vrije Universiteit te Amsterdam.
* Sastri, H.N.L.  (1970) "The Bilingual Method of Teaching English – an Experiment" In: RELC Journal, 2, 24-28.
* Walatara, Douglas (1973) "An Experiment with the Bilingual Method for Teaching English as a Complementary Language" In: Journal of the National Science Council of Sri Lanka, 1, 189-205.

[[Category:Language-teaching methodology]]