[[File:GD09 Poster Session.jpg|thumb|300px|The poster session at Graph Drawing 2009 in [[Chicago]].]]
The '''International Symposium on Graph Drawing (GD)''' is an annual [[academic conference]] in which researchers present [[peer review]]ed papers on [[graph drawing]], [[information visualization]] of [[Network theory|network]] information, [[geometric graph theory]], and related topics.

==Significance==
The Graph Drawing symposia have been central to the growth and development of graph drawing as a research area: as Herman et al. write, "the Graph Drawing community grew around the yearly Symposia."<ref>{{citation|first1=Ivan|last1=Herman|first2=Guy|last2=Melançon|first3=M. Scott|last3=Marshall|title=Graph visualization and navigation in information visualization: A survey|journal=IEEE Transactions in Information Visualization and Computer Graphics|volume=6|issue=1|year=2000|pages=24–43|doi=10.1109/2945.841119}}.</ref> Nguyen<ref>{{citation|first=Quang Ving|last=Nguyen|title=Space-Efficient Visualization of Large Hierarchies|series=Ph.D. thesis|publisher=Univ. of Technology, Sydney|year=2005|hdl=2100/315}}.</ref> lists Graph Drawing as one of "several good conferences which directly or indirectly concern with information visualization", and Wong et al.<ref>{{citation|first1=Pak Chung|last1=Wong|last2=Chin|first2=G.|last3=Foote|first3=H.|last4=Mackey|first4=P.|last5=Thomas|first5=J.|contribution=Have Green – A Visual Analytics Framework for Large Semantic Graphs|title=IEEE Symposium On Visual Analytics Science And Technology|year=2006|pages=67–74|doi=10.1109/VAST.2006.261432|url=http://www.purdue.edu/discoverypark/vaccine/publications/pdf/Have%20Green.pdf}}.</ref> report that its proceedings "provide a wealth of information". In a 2003 study the symposium was among the top 30% of computer science research publication venues, ranked by [[impact factor]].<ref>[http://citeseer.ist.psu.edu/impact.html Estimated impact of publication venues in Computer Science], CiteSeer, May 2003.</ref>

==History==
The first symposium was held in Marino, near [[Rome]], Italy, in 1992, organized by Giuseppe Di Battista, [[Peter Eades]], [[Pierre Rosenstiehl]], and [[Roberto Tamassia]]. The first two symposia did not publish proceedings, but reports are available online.<ref>[http://graphdrawing.org/literature/gd92-report.ps.Z Report from 1992 symposium] and [http://graphdrawing.org/literature/gd93-v2.ps.Z 1993 symposium].</ref> Since 1994, the proceedings of the symposia have been published by [[Springer-Verlag]]'s [[Lecture Notes in Computer Science]] series.<ref>[http://www.informatik.uni-trier.de/~ley/db/conf/gd/index.html Listing of GD conference proceedings] in DB&amp;LP.</ref>

Countries in which the conference has been held include Australia, Austria, Canada, the [[Czech Republic]], France, Germany (twice), Greece, Ireland, Italy (three times), and the United States (five times).

==Citation data and its analysis==
A [[citation graph]] having vertices representing the papers in the 1994–2000 Graph Drawing symposia and having edges representing citations between these papers was made available as part of the graph drawing contest associated with the 2001 symposium.<ref>{{citation|first1=T. C.|last1=Biedl|first2=F. J.|last2=Brandenburg|contribution=Graph-drawing contest report|title=Graph Drawing, 9th Int. Symp., GD 2001|volume=2265|series=Lecture Notes in Computer Science|pages=513–522|publisher=Springer-Verlag|year=2002}}.</ref> 
The largest connected component of this graph consists of 249 vertices and 642 edges; clustering analysis reveals several prominent subtopics within graph drawing that are more tightly connected, including three-dimensional graph drawing and orthogonal graph drawing.<ref>{{citation|first1=U.|last1=Brandes|first2=T.|last2=Willhalm|contribution=Visualization of bibliographic networks with a reshaped landscape metaphor|title=Proc. Symp. Data Visualisation 2002|year=2002|publisher=Eurographics Association|pages=159–164|url=http://portal.acm.org/citation.cfm?id=509765}}.</ref>

== See also ==
* The [[list of computer science conferences]] contains other academic conferences in computer science.

==References==
{{reflist|2}}

==External links==
* [http://graphdrawing.org/ graphdrawing.org], the official web site of the conference series.
* [http://www.informatik.uni-trier.de/~ley/db/conf/gd/ the DBLP entry]  (with list of articles).

[[Category:Theoretical computer science conferences]]
[[Category:Mathematics conferences]]
[[Category:Graph drawing]]
[[Category:Visualization (graphic)]]