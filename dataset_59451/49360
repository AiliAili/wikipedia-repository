The Hawaii [[longline fishing|longline fishery]] is managed under [[Western Pacific Regional Fishery Management Council]]’s (WPRFMC's)[http://www.wpcouncil.org/fishery-plans-policies-reports/pelagics_fe/ Pelagics Fisheries Ecosystem Plan] ([http://www.wpcouncil.org/fishery-plans-policies-reports/former-fishery-management-plans/pelagics-fishery-management-plan/ formerly Pelagics Fisheries Management Plan]). Through this plan, the WPRFMC has introduced logbooks, [[fishery observer|observers]], [[vessel monitoring system]]s, fishing gear modifications and spatial management for the Hawaii longline fishery. Until relatively recently, the main driver for management of the Hawaii longline fishery has been [[bycatch]] and not fishery resources.

The revival of the Hawaii longline fleet in the late 1980s meant that larger ocean-going longline vessels began operating from Honolulu.<ref name="HI Pelagic Fisheries">{{cite journal|last1=Boggs|first1=C.|last2=Ito|first2=R.|title=Hawaii's pelagic fishereis|journal=NOAA-NMFS Marine Fisheries Review|volume=55|issue=2|pages=69–82}}</ref> The advent of the new fleet was driven primarily by targeting [[swordfish]], which meant using [[squid]] bait on hooks deployed in relatively shallow depths (<30 m) and with light sticks attached to the branch lines. Observers began to be employed on vessels in 1994 and it soon became apparent that in the shallow set fishery there were catches of sea turtles and seabirds. The principal seabirds caught were [[black-footed albatross|black-footed]] and [[Laysan albatross]], and for the turtles, [[loggerhead sea turtle|loggerheads]] and [[leatherbacks]]. There were turtle and seabird interactions in the deep set fishery also, but these were one to two orders of magnitude lower than in the shallow set fishery.

== Seabird Bycatch Mitigation Development ==

Prior to 2001, 1380 black footed albatross and 1163 Laysan albatrosses were caught annually by the Hawaii longline fishery.<ref name="EIS Seabird Bycatch FMP" />  The WPRFMC's response to the volume of seabirds being caught was to mount a project through 1998 and 1999 to test various seabird mitigation methods.<ref name="Hawaii Longline Seabird Mortality Mitigation Project">{{cite book|last1=NcNamara|first1=B.|last2=Torre|first2=L.|last3=Kaaialii|first3=G.|title=Final Report, Hawaii Longline Seabird Mortality Mitigation Project|date=1999|publisher=Garcia & Associates|location=Honolulu|page=99 pp +Appendices}}</ref> It was found that during gear setting operations, blue dyed baits were the most successful mitigation method, followed by strategic offal discards. Tori lines and a towed buoy system also proved to be effective mitigation measures during the set. During hauling operations, blue dyed baited and tori lines were found to be equally effective mitigation strategies, followed by the towed buoy. Retaining offal on the vessel during the haul increased seabird interactions.

The [[National Marine Fisheries Service]] Pacific Islands Fisheries Science Center (NMFS PIFSC) also tested tori lines, blue dyed bait and weighted hooks in 1999,<ref name="Deterring albatrosses">{{cite book|last1=Boggs|first1=Christofer H.|editor1-last=Melvin|editor1-first=Edward F.|editor2-last=Parrish|editor2-first=Julia K.|title=Seabird bycatch: trends, roadblocks and solutions; proceedings of the Symposium Seabird Bycatch: Trends, Roadblocks, and Solutions, February 26–27, 1999, Blaine, Washington; annual meeting of the Pacific Seabird Group|chapter=Deterring albatrosses from contacting baits during swordfish longline sets. Seabird bycatch : Trends, Roadblocks, and solutions|date=2001|publisher=Univ. of Alaska Sea Grant|location=Fairbanks, Alaska|isbn=1-56612-066-7|edition=AK-SG-01}}</ref> They found that baits dyed blue and baits with additional weight reduced the number of interactions with both black-footed and Laysan albatross. Tori lines reduced contact between baits and albatrosses by 70%

The WPRFMC’s plan for implementing seabird mitigation measures was for an Fishery Management Plan(FMP) amendment where fishermen could choose the measures from a selected list of proven mitigation methods. However, this was forestalled by a 2000 US Fish and Wildlife Service Biological Opinion (BiOp)<ref name=BiOP>{{cite book|title=Biological Opinion of the US Fish and Wildlife Service for the Effects of the Hawaii-Based Domestic Longline Fleet on the Short-Tailed Albatross (Phoebastria albatrus)|date=2000|publisher=USFWS|page=107}}</ref> on the endangered Short-tailed albatross  in, which prescribed what seabird mitigation measures would be used by the tuna-targeting (deep sets) and by swordfish (shallow sets) as follows:

=== Summary of seabird deterrent measures by set type ===

{| class="wikitable"
|-
! Seabird deterrent measure
! Tuna (deep sets)
! Swordfish (shallow sets)
|-
| Thawed baits
| Required
| Required
|-
| Blue dyed baits
| Required for all baits
| Required for all baits
|-
| Discharge offal
| Required
| Required
|-
| Night Sets
| Optional
| Required
|-
| Line setting machine with weighted branch lines (minimum weight = 45 g)
| Required
| Optional
|-
| Weighted branch lines (minimum weight = 45 g)
| Optional
| Optional
|-
| Towed deterrent
| Optional
| Optional
|}
The WPRFMC incorporated these measures into a Pelagics FMP amendment in 2002, requiring that these seabird mitigation measures be used when fishing north of 23 deg N.<ref name="Fed Register 1">{{cite journal|journal=US Federal Register|date=May 14, 2002|volume=67|issue=93|pages=34408–34413}}</ref> This measure was further refined in 2006 by an FMP amendment that allowed operators of Hawaii-based longline vessels fishing north of 23 degrees north latitude, as well as those targeting swordfish south of 23 degrees north, to utilize side-setting to reduce seabird interactions in lieu of the seabird mitigation already measures required.<ref name="US Fed Register 2">{{cite journal|journal=US Federal Register|date=December 19, 2005|volume=70|issue=242|pages=75075–75080}}</ref>

The implementation of the seabird measures caused a massive drop in seabird interactions by more than 90% in the Hawaii longline fishery.
[[File:Total Albatross Bycatch Take by Year for Hawaii Longline Fishery.jpg|thumb|center|600px|The graph illustrates the drop of seabird interaction with the Hawaii Longline Fishery when utilizing bycatch mitigation techniques]]

== Sea Turtle Bycatch Mitigation Development ==

Despite low observer coverage, usually 5% or less, it was estimated that prior to 2001, a total of 666 turtles were caught annually in the Hawaii longline fishery: 418 [[loggerhead turtle|loggerheads]], 146 [[olive ridley turtle|olive ridleys]], 112 [[leatherback turtle|leatherbacks]] and 40 [[green turtle]]s.<ref name="EIS Seabird Bycatch FMP">{{cite journal|last1=NMF|title=Final Environmental Impact Statement. Seabird Interaction Avoidance Methods and Pelagic Squid Fishery Management under the FMP for the Pelagic Fisheries of the Western Pacific Region and High Seas Fishing Compliance Act|journal=National Marine Fisheries Service|date=2005|page=568|location=Honolulu}}</ref>

Unlike the seabird issue, the solutions for sea turtles were propelled initially by litigation by environmental organizations which resulted in a complete closure of the shallow set longline fishery between 2001 and 2004. Over these years, the Hawaii fishery was only permitted to target tunas. An FMP amendment in 2002 incorporated reasonable and prudent alternative of the March 2001 Biological Opinion issued by NMFS.<ref name="US Fed Register 3">{{cite journal|journal=US Federal Register|date=June 12, 2002|volume=67|issue=113|pages=40232–40238}}</ref>  This amendment prohibited shallow set pelagic longlining north of the equator and closed waters between 0° and 15° N from April–May annually to longline fishing. It instituted sea turtle handling requirements for all vessels using hooks to target pelagic species in the region’s EEZ waters and extended the protected species workshop requirement to include the operators of vessels registered to longline general permits<ref name="Sea Turtle Interactions/swordfish">{{cite book|last1=Gilman|first1=E.|last2=Kobayashi|first2=D.|last3=Swenarton|first3=T.|last4=Dalzell|first4=P.|last5=Kinan|first5=I.|last6=Brothers|first6=N.|title=Efficacy and Commercial Viability of Regulations Designed to Reduce Sea Turtle Interactions in the Hawaii-Based Longline Swordfish Fishery|date=2006|publisher=Western Pacific Regional Fishery Management Council|location=Honolulu|isbn=1-934061-02-6}}</ref>

[[File:Pelagic Longline Hooks - J Hook by a Circle Hook.jpg|thumb]]

Salvation was at hand, however, for the shallow-set longline fishery, based on hook research by NMFS Fisheries Engineering Laboratory in Pascagoula, Mississippi. This research found that large 18/0 circle hooks combined with mackerel type fish bait could sharply reduce loggerhead and leatherback interactions of longline vessels fishing on the Grand Banks for swordfish. The WPRFMC operationalized this technology in an FMP amendment<ref name="US Fed Register 5">{{cite journal|journal=US Federal Register|date=April 2, 2004|volume=69|issue=64|pages=17329–17354}}</ref> which established a limited Hawaii-based shallow-set swordfish fishery using circle hooks with mackerel bait.

Fishing effort in the shallow-set swordfish fishery was limited to 50% of the 1994-1999 annual average number of sets (just over 2,100 sets) allocated between fishermen applying to participate in the fishery. A ‘hard’ limit on the number of leatherback (16) and loggerhead (17) turtle interactions that could occur in the swordfish fishery was implemented; the fishery closed for the remainder of the calendar year if either limit was reached. The amendment re-implemented earlier sea turtle handling and resuscitation requirements and included conservation projects to protect sea turtles in their nesting and coastal habitats. This rule implemented the requirement for night setting imposed by the USFWS Biological Opinion on Hawaii-based longline vessels targeting swordfish north of 23 degrees north latitude. The new measures resulted in a >90% reduction in sea turtles interactions in the Hawaii longline fishery. A later amendment to the Fishery Ecosystem Plan (FEP) removed the set limits and increased the loggerhead and leatherback hard caps of 34 and 26 respectively.<ref>{{cite journal|journal=US Federal Register|date=December 10, 2009|volume=74|issue=236|pages=65460–65480}}</ref><ref>{{cite book|last1=Gilman|first1=edited by Eric|title=Proceedings of the Technical Workshop on Mitigating Sea Turtle Bycatch in Coastal Net Fisheries, 20-22 January 2009, Honolulu, Hawaii USA|date=2009|publisher=Western Pacific Regional Fishery Management Council|location=Honolulu, Hawaii|isbn=1-934061-40-9}}</ref>
<!-- Deleted image removed:  [[File:Sea Turtle bycatch in the Hawaii longline fishery, 1994-2011.jpg|thumb|center|600px|This graph illustrates Sea Turtle bycatch in the Hawaii Longline Fishery from 1994-2011.]] -->

=== Sea turtle bycatch in American Samoa ===

The deployment of observers in the American Samoa longline fleet revealed that the fishery was interacting with green sea turtles. Most of the turtle hooking’s occurred on hooks close to the float, i.e. the shallower hooks on the longline catenary. The WPRFMC response was to amend the FEP and required fishermen on vessels longer than 40&nbsp;ft to use float lines that are at least 30 meters long, and maintain a distance between float lines and adjacent branch lines with hooks of at least 70 meters.<ref>{{cite journal|journal=US Federal Register|date=August 24, 2011|volume=76|issue=164|pages=52888–52889}}</ref> Fishermen on these longer vessels are required to deploy at least 15 branch lines between floats.

The new measure reduced green sea turtle interactions by about 75%, but the concentration of hooks at deeper depths meant that the American Samoa fishery had higher interaction rates with deep diving leatherback and olive ridley sea turtles.

== Marine Mammal Bycatch Mitigation Development ==

[[Marine mammal]]s have become a problem for the Hawaii longline fishery due to a limited population of island associated [[false killer whale]]s (FKWs) in the Main Hawaiian Islands(MHI) and interactions with a wider ‘pelagic’ population within the US EEZ around Hawaii. The island associated population was listed as ‘endangered’ under the Endangered Species Act in 2012. The interactions with the wider pelagic population drove the creation of the False Killer Whale Take Reduction Team (FKWTRT) in 2010, which developed a Take Reduction Plan (TRP) published towards the end of 2012<ref>{{cite journal|journal=US Federal Register|date=November 29, 2012|volume=77|issue=230|pages=71260–71286}}</ref> and with some provisions being implemented in early 2013.

The TRP required a number of different actions as follows:
*Require the use of circle hooks that have a maximum wire diameter of 4.5&nbsp;mm (0.177&nbsp;in), 10 degree offset or less, containing round (non-flattened) wire that can be measured with a caliper or other appropriate gauge in the Hawaii-based deep-set fishery;
*Establish a minimum 2.0&nbsp;mm (0.079&nbsp;in) diameter for monofilament leaders and branch lines, and a minimum breaking strength of 400 pounds (181&nbsp;kg) for any other material used in the construction of a leader or branch line in the Hawaii-based deep-set longline fishery;
*Establish a longline exclusion zone around the MHI that is closed to longline fishing year-round; the 282,796 km2 (82,450 nmi2) area has the same name and boundary as the February–September boundary of the MHI Longline Prohibited Area;
*Expand the content of the existing, mandatory Protected Species Workshop for the Hawaii-based longline fishery to include new information on marine mammal interaction mitigation techniques;
*Require a NMFS-approved marine mammal handling and release informational placard to be posted onboard all Hawaii-based longline vessels;
*Require the captain of the longline vessel to supervise the handling and release of any hooked or entangled marine mammal;
*Require a NMFS-approved placard that instructs the vessel crew to notify the captain in the event of a marine mammal interaction be posted onboard all Hawaii-based longline vessels; and
*Establish a “Southern Exclusion Zone” (SEZ) that will be closed to the commercial Hawaii-based deep-set longline fishery for varying periods of time whenever specific levels of serious injuries or mortalities of false killer whales are observed within the U.S. EEZ around Hawaii.

Solving the FKW problem remains a major challenge to the Hawaii longline fishery. It is unlikely that the fishery interacts with the MHI FKW population, though some level of take is attributed to this population from observed takes of FKW. Interactions with the larger (1500 animals) pelagic population may continue to be greater than the Potential Biological Removals for this population.

== Positive Side Notes ==

Dealing with bycatch in the Hawaii longline fishery has had some unintended spin-offs including the reduction of shark catches by about 50%.<ref name="Shark Bycatch decrease">{{cite journal|last1=Walsh|first1=W.|last2=Bigelow|first2=K.A.|last3=Sender|first3=K.L.|title=Decreases in shark catches and mortality in the Hawaii-Based longline fishery as documented by fishery observers|journal=Marine and Coastal Fisheries: Dynamics, Management and Ecosystem Science|volume=1|pages=270–282}}</ref>

== References ==

<!--- See [[Wikipedia:Footnotes]] on how to create references using<ref></ref> tags, these references will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.example.com www.example.com]

<!--- Categories --->
[[Category:Fishing|longline]]