{{Infobox journal
| title = Clinical Interventions in Aging
| cover = 
| editor = Richard F. Walker
| discipline = [[Gerontology]]
| abbreviation = Clin. Interv. Aging
| publisher = [[Dove Medical Press]]
| country =
| frequency = Upon acceptance
| history = 2006–present
| openaccess = Yes
| impact = 2.651
| impact-year = 2012
| website = http://www.dovepress.com/clinical-interventions-in-aging-journal
| ISSN = 1176-9092
| eISSN = 1178-1998
| CODEN = CIALBC
| LCCN =
| OCLC = 317918656
}}
'''''Clinical Interventions in Aging''''' is a [[peer review|peer-reviewed]] [[open access]] [[medical journal]] covering research in [[gerontology]]. The journal was established in 2006 and is published by [[Dove Medical Press]]. The [[editor-in-chief]] is Richard F. Walker.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Chemical Abstracts Service]]
* [[Science Citation Index Expanded]]
* [[Current Contents]]/Clinical Medicine
* [[Embase]]
* [[EMCare]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 2.651.<ref name=WoS>{{cite book |year=2013 |chapter=Clinical Interventions in Aging |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.dovepress.com/clinical-interventions-in-aging-journal}}

[[Category:English-language journals]]
[[Category:Open access journals]]
[[Category:Dove Medical Press academic journals]]
[[Category:Publications established in 2006]]
[[Category:Gerontology journals]]