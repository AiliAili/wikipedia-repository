{{Infobox journal
| title = Epilepsia
| editor = [[Astrid Nehlig, PhD]], [[Michael Sperling, MD]] and [[Gary W. Mathern, MD]],
| discipline = [[epileptologist|Epileptology]] and [[neurology]]
| abbreviation = Epilepsia
| publisher = [[Wiley-Blackwell]] on behalf of the [[International League Against Epilepsy]]
| country =
| frequency = Monthly
| history = 1909-present
| openaccess =
| license =
| impact = 4.706
| impact-year = 2015
| website = http://www.epilepsia.com/
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291528-1167
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291528-1167/issues
| link2-name = Online archive
| cover =Epilepsia March 2017 cover.gif
| formernames =
| link3 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1528-1167
| link3-name = Journal page at publisher's website
| JSTOR =
| OCLC = 43274235
| LCCN =
| CODEN = EPILAK
| ISSN = 0013-9580
| eISSN = 1528-1167
}}
'''''Epilepsia''''' is a [[peer review|peer-reviewed]] [[medical journal]] focusing on all aspects of [[epilepsy]]. The journal was established in 1909. It is the official journal of the [http://www.ilae.org/ International League Against Epilepsy (ILAE)], is published by [[Wiley-Blackwell]], and is edited by [http://www.ilae.org/Visitors/Publications/Epilepsia-Editors.cfm Astrid Nehlig, PhD,] [http://www.ilae.org/Visitors/Publications/Epilepsia-Editors.cfm Michael R. Sperling, MD] and [http://www.ilae.org/Visitors/Publications/Epilepsia-Editors.cfm Gary Mathern, MD].

==Ab, MDstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Biological Abstracts]]
* [[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-14}}</ref>
* [[Chemical Abstracts]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-14 }}</ref>
* [[Global Health]]<ref name= CABGH>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/global-health/ |title= Serials cited |work= [[Global Health]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-14}}</ref>
* [[CAB Abstracts]]<ref name= CABAB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title= Serials cited |work= [[CAB Abstracts]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-14}}</ref>
* [[Current Awareness in Biological Sciences]]
* [[Current Contents]]/Life Science<ref name=ISI/>
* Current Contents/Clinical Medicine<ref name=ISI/>
* [[EMBASE|Excerpta Medica]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/248130 |title= ''Epilepsia'' |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-12-14}}</ref>
* [[PASCAL (database)|PASCAL]]
* [[PsychINFO]]<ref>{{citation |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2014-12-14}}</ref>
* [[Psychological Abstracts]]
* [[Science Citation Index]]/Science Citation Index Expanded<ref name=ISI/>
}}

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.706, ranking it 25th out of 193 journals in the category "Clinical Neurology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Clinical Neurology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official|http://www.epilepsia.com/}}
* [http://www.ilae.org/ International League Against Epilepsy]
[[Category:Epilepsy journals]]
[[Category:Publications established in 1909]]
[[Category:Monthly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]