{{for|the newspaper|Appeal to Reason (newspaper)}}
{{good article}}
{{Infobox album <!-- See Wikipedia:WikiProject Albums -->
| Name        = Appeal to Reason
| Type        = studio
| Artist      = [[Rise Against]]
| Cover       = ATRFinal.jpg
| Alt         = Cover features various drawing including one of a man in a gasmask and another of a very young child.
| Released    = {{Start date|2008|10|7}}
| Recorded    = January–June 2008 at the [[Blasting Room]], [[Fort Collins, Colorado|Fort Collins]], [[Colorado]]
| Genre       = [[Punk rock]], [[melodic hardcore]], [[alternative rock]]
| Length      = 48:23
| Label       = [[DGC Records|DGC]], [[Interscope Records|Interscope]]<br /><small>B0012145-02</small>
| Producer    = [[Bill Stevenson (musician)|Bill Stevenson]], Jason Livermore
| Reviews     = 
| Last album  = ''[[This Is Noise]]''<br />(2007)
| This album  = '''''Appeal to Reason'''''<br />(2008)
| Next album  = ''[[Another Station: Another Mile]]''<br />(2010)
| Misc        = {{Singles
  | Name           = Appeal to Reason
  | Type           = studio
  | single 1       = [[Re-Education (Through Labor)]]
  | single 1 date  = August 25, 2008
  | single 2       = [[Audience of One (Rise Against song)|Audience of One]]
  | single 2 date  = January 15, 2009
  | single 3       = <!--Please do not add "Hero of War" as the third single. It was never released as a single. See http://www.billboard.com/bbcom/news/rise-against-ready-to-hit-the-road-with-1003977421.story -->[[Savior (Rise Against song)|Savior]]
  | single 3 date  = June 3, 2009
}}
}}

'''''Appeal to Reason''''' is the fifth [[studio album]] by American [[punk rock]] band [[Rise Against]]. After touring in support of their previous album, ''[[The Sufferer & the Witness]]'', Rise Against began recording ''Appeal to Reason'' in January 2008 at the [[Blasting Room]] in [[Fort Collins, Colorado]]. Recording and production were finished in June, and the album was released in North America on October 7, 2008. The album is the band's first release with guitarist [[Zach Blair]]. The album has been certified Gold by the [[Recording Industry Association of America|RIAA]] and platinum by the [[Canadian Recording Industry Association|CRIA]].

''Appeal to Reason'' was Rise Against's highest charting album until the release of ''[[Endgame (Rise Against album)|Endgame]]'', debuting at number three on the [[Billboard 200|''Billboard'' 200]] chart and selling 64,700 copies in its first week of release. It received generally favorable reviews from critics. The album produced three singles: "[[Re-Education (Through Labor)]]", "[[Audience of One (Rise Against song)|Audience of One]]", and "[[Savior (Rise Against song)|Savior]]".

Although commercially successful, Rise Against was greatly criticized by many long-term fans for producing an album that is a dramatic departure compared to Rise Against's previous fast-paced works. Despite this, the album has sold over 600,000 copies in the USA, with one of the songs going Platinum, and another going Gold; it is their most successful album to date.

==Writing and recording==
In May 2007, it was reported that [[Rise Against]] was planning to return to the studio after touring in support of their previous album, ''[[The Sufferer & the Witness]]'', to begin work on their next album.<ref name="next album">{{cite web | date=May 18, 2007 | title=Rise Against Plan Next Album | publisher=[[Ultimate Guitar Archive|Ultimate-Guitar.com]] | url=http://www.ultimate-guitar.com/news/upcoming_releases/rise_against_plan_next_album.html | accessdate=2009-04-11| archiveurl= https://web.archive.org/web/20090321174649/http://www.ultimate-guitar.com/news/upcoming_releases/rise_against_plan_next_album.html| archivedate= 21 March 2009 <!--DASHBot-->| deadurl= no}}</ref> The band headlined a North American tour supporting ''The Sufferer & the Witness'' throughout July and August 2007, instead of attending that year's [[Warped Tour]].<ref name="next album" /> When asked in July about the band's plans for a new album, guitarist [[Zach Blair]] told ThePunkSite.com that Rise Against would "start writing and recording the record" after touring and would be "writing for a few months" before returning to the studio. He also predicted a summer 2008 release date for the album.<ref>{{cite web |date=July 18, 2007 |title=Rise Against Interview |publisher=The Punk Site |url=http://www.thepunksite.com/interviews.php?page=riseagainst2 |archiveurl=https://web.archive.org/web/20070927182809/http://www.thepunksite.com/interviews.php?page=riseagainst2 |archivedate=2007-09-27 |accessdate=2007-07-24}}</ref> In an interview with bassist [[Joe Principe]] in August 2007, he stated recording would likely begin around early 2008, although he said "everything could change", but that was "the plan right now".<ref>{{cite news |last=Koroneos |first=George |title=Interview: Rise Against |url=http://lifeinabungalo.com/2007/08/14/interview-rise-against/ |archiveurl=https://web.archive.org/web/20080908115312/http://lifeinabungalo.com/2007/08/14/interview-rise-against/ |archivedate=2008-09-08 |publisher=Life In A Bungalo |date=August 14, 2007 |accessdate=2008-05-21}}</ref> The band continued to tour throughout the rest of 2007, playing several shows in the [[Taste of Chaos]] tour and supporting its headliner, [[The Used]].<ref>{{cite news |last=Moran |first=Jonathon |author2=James Wigney |title=Why Used is new again |url=http://www.news.com.au/adelaidenow/story/0,22606,21718972-5006343,00.html |publisher=[[News Limited]] |date=May 13, 2007 |accessdate=2009-04-11}}</ref>

On January 7, 2008, Rise Against announced on their website that they had begun writing and demoing for their next album.<ref>{{cite web |title=Happy 08 |url=http://www.riseagainst.com/diary/default.aspx/nid/12367 |date=January 7, 2008 |accessdate=2009-04-11 |archiveurl=https://web.archive.org/web/20080212085659/http://www.riseagainst.com/diary/default.aspx/nid/12367 |archivedate=2008-02-12}}</ref> When asked in May what the status of the album was, frontman [[Tim McIlrath]] told the [[Los Angeles]] modern rock radio station, [[KROQ-FM|KROQ]], that the band was in the middle of the recording process. He also stated that the album would be recorded at the [[Blasting Room]] in [[Fort Collins, Colorado]] and produced by [[Bill Stevenson (musician)|Bill Stevenson]] and Jason Livermore, who had produced ''The Sufferer & the Witness''.<ref name="KROQinterview">{{cite web | date=May 20, 2007 | title=KROQ talks to Rise Against | publisher=[[Punknews.org]] | url=http://www.punknews.org/article/28957 | accessdate=2007-05-21}}</ref><ref>{{cite web |title=Rise Against Interview @ KROQ Weenie Roast Y Fiesta 2008 |url=http://www.kroq-data.com/wah/wah/dcvideo/avc-view.aspx?videoid=2908 |archive-url=https://archive.is/20090223213526/http://www.kroq-data.com/wah/wah/dcvideo/avc-view.aspx?videoid=2908 |dead-url=yes |archive-date=February 23, 2009 |publisher=KROQ-FM |date=May 19, 2008 |accessdate=2009-04-11}}</ref> Also in May 2008, Rise Against posted a blog on their website, stating that they were back in the studio working on the album. It explained that they had "spent many weeks in Chicago throughout the end of winter writing new songs" in their "rehearsal space".<ref>{{cite web | date=May 2008 | title=Rise Against Back In The Studio!
 | publisher=RiseAgainst.com | url=http://www.riseagainst.com/studio/ | accessdate=2008-05-30 |archiveurl = https://web.archive.org/web/20080513125756/http://www.riseagainst.com/studio/ |archivedate = May 13, 2008}}</ref><ref>{{cite news |title=Rise Against begin studio blog |url=http://www.punknews.org/article/29078 |publisher=[[Punknews.org]] |date=May 30, 2008 |accessdate=2009-03-30}}</ref> Asked later about the writing and recording process, McIlrath said, "We kind of blocked a month off over the winter and said, 'Let's all get together, get the rehearsal space, and start putting some ideas together,' which is what we did. And then, we also blocked off a week or two at the Blasting Room after we arrived in Fort Collins to just kind of jam stuff out, get some new ideas going."<ref name="AbsolutePunk.net interview"/>

In a June 2008 interview, Luisa Mateus of Gigwise.com asked McIlrath about the new album. He stated that it was "mostly finished" but that a name and release date were still undetermined. The only hint given on the musical style of the album was Mateus' statement that the band said that they were "happy keeping their sound organic".<ref>{{cite news |last=Mateus |first=Luisa |title=Rise Against Reveal All About Their New Album At Download |url=http://www.gigwise.com/news/43954/rise-against-reveal-all-about-their-new-album-at-download |publisher=Gigwise.com |date=June 18, 2008 |accessdate=2009-03-30| archiveurl= https://web.archive.org/web/20090212232013/http://www.gigwise.com/news/43954/Rise-Against-Reveal-All-About-Their-New-Album-At-Download| archivedate= 12 February 2009 <!--DASHBot-->| deadurl= no}}</ref> On July 14, 2008, it was reported on [[Punknews.org]] that the album would be titled ''Appeal to Reason''. The name is taken from a [[left-wing politics|leftist]] [[Appeal to Reason (newspaper)|newspaper]] from 1897.<ref>{{cite news |last=DeRogatis |first=Jim |title=Will Rise Against be the next big Chicago band? |url=http://www.suntimes.com/entertainment/weekend/1290994,WKP-News-live21.article |publisher=''[[Chicago Sun-Times]]'' |date=November 21, 2008 |accessdate=2009-04-22}}{{dead link|date=August 2011}}</ref>

==Musical style and themes==
''Appeal to Reason'' is considered by critics to be one of Rise Against's most accessible and melodic albums, both musically and lyrically. Jon Pareles of ''[[The New York Times]]'' felt that the band's "righteousness grows more tuneful with every album".<ref>{{cite news |last=Pareles |first=Jon |title=New CDs |url=https://www.nytimes.com/2008/10/13/arts/music/13choi.html?ref=music |publisher=''[[The New York Times]]'' |date=October 12, 2008 |accessdate=2009-05-04}}</ref> John Hanson of [[Sputnikmusic]] said that the album is "‘appealing’ to a larger audience than old fans will be comfortable with".<ref name="sputnikmusic">{{cite web |last=Hanson |first=John A. |title=Rise Against - Appeal To Reason (album review) |url=http://www.sputnikmusic.com/review/27486/Rise-Against-Appeal-to-Reason/ |publisher=[[Sputnikmusic]] |date=October 7, 2008 |accessdate=2009-06-06}}</ref> In an October 2008 interview with Tony Pascarella of [[AbsolutePunk.net]], bassist Joe Principe said, "''Appeal to Reason'' sounds like a Rise Against album but there's still something new that we're offering. I think we've grown as songwriters and as a band, and it shows on the record."<ref name="AbsolutePunk.net interview">{{cite news |last=Pascarella |first=Tony |title=Rise Against - 10.06.08 - Interview |url=http://www.absolutepunk.net/showthread.php?t=625382 |publisher=[[AbsolutePunk.net]] |date=October 19, 2009 |accessdate=2009-05-04}}</ref> According to Bill Stewart of [[PopMatters]], "''Appeal to Reason'' is a Rise Against album. If you possess more than a passing familiarity with the band, I wouldn’t even bother scrolling through the rest of this review, and I’d certainly avoid checking out the rating at the end of it—because that first sentence, for better or worse, says everything that needs to be said about this album."<ref name="PopMatters">{{cite news |last=Stewart |first=Bill |title=Rise Against: Appeal to Reason < Music |url=http://www.popmatters.com/pm/review/rise-against-appeal-to-reason |publisher=[[PopMatters]] |date=November 7, 2008 |accessdate=2009-05-07}}</ref>

{{Listen |filename=Re-Education (Through Labor).ogg |title="Re-Education (Through Labor)" |description=A sample of the album's lead single, "[[Re-Education (Through Labor)]]" which shows the band's movement toward more melodic and mainstream music.}}
{{Listen |filename=Hero of War.ogg |title="Hero of War" |description=A sample of the acoustic song "[[Hero of War]]", which tells the tragedy of war on a soldier.}}

The album includes one acoustic song, "[[Hero of War]]", which is about an [[Iraq War]] Veteran looking back on his war experiences. It is described by ''[[Rolling Stone]]'' as an "ambivalent aggro-folk track".<ref name="RS"/> McIlrath said of the song, "I wanted to take the perspective  of 'What is the war going to be looked back on as?'"<ref name="Rise Against Interview [2008]">{{cite news |last=McKibbin |first=Adam |title=Rise Against Interview [2008] |url=http://www.theredalert.com/features/riseagainst2.php |publisher=The Red Alert |date=October 2008 |accessdate=2012-05-23}}</ref> In another interview McIlrath stated, "It was a way to document what's going on, like other artists documented for their generation and for generations to come."<ref name="Interview: Rise Against">{{cite news |last=Sciarretto |first=Amy |title=Interview: Rise Against |url=http://www.artistdirect.com/nad/news/article/0,,4894898,00.html |publisher=Artistdirect.com |date=December 14, 2008 |accessdate=2012-05-23}}</ref> He went on to say, "There are not many songs...talking about what's going on during eight years of occupation in Iraq. That, combined with meeting active soldiers and retired soldiers at our shows and hearing those stories about what is going on on the ground amid all the bullshit, showed me the differences from what is really happening to what is happening in the news media. I just thought that this needed to go into a song."<ref name="Interview: Rise Against"/>

Much of the rest of the album deals with political issues in the United States as well. Jeff Miers of ''[[The Buffalo News]]'' calls the album "a response to the oppressive vacuousness of the Bush years".<ref>{{cite news |last=Miers |first=Jeff |title=Rise Against stands firm in punk rock history |url=http://www.buffalonews.com/entertainment/story/650021.html |work=[[The Buffalo News]] |date=May 6, 2009 |accessdate=2009-05-06}}{{dead link|date=October 2013}}</ref> Dealing with specific tracks on the album, [[AllMusic]] states that Rise Against "rages against the moral decay rotting the core of the U.S. on the opening 'Collapse (Post-Amerika),' just as they strike out against the slow [[dumbing down]] of America on 'Re-Education (Through Labor)'".<ref name="allmusic">{{cite web |last=Erlewine |first=Stephen |title=Appeal to Reason - Rise Against |url={{Allmusic|class=album|id=r1439728|pure_url=yes}} |publisher=[[AllMusic]] |accessdate=2009-05-04}}</ref> McIlrath said in an interview with The Red Alert, "All of our songs are 'that' song that we won't dilute. They always have been. I've never written a song, until "Hero of War," with a specific goal in mind." When asked about how ''Appeal to Reason'' continues Rise Against's tradition of making politically charged music, he said: "The reason I started this band, and the reason I still do it, is that I still open a paper and say, "Holy Shit! Are you kidding me? Is this really happening? Are people voting for things like Proposition 8? Is this America? Are we still in Iraq and in a place that people think a white versus a black president is a big deal?" There is so much to address through music. There is plenty we need to learn from."<ref name="Interview: Rise Against"/> Nevertheless, the band has stated that their songs don't only focus on politics. In one interview, Principe said, "The political side of this band is just that -- it's a side. There are political lyrics. There are social awareness and there are lyrics about the environment. I think if people take the time to read the lyrics, they'll know we're not strictly force feeding you our politics."<ref>{{cite news |last=Fuoco-Karasinski |first=Christina |title=Rise Against continues its 'Appeal to Reason' through live show |url=http://www.mlive.com/entertainment/flint/index.ssf/2008/11/rise_against_continues_its_app.html |publisher=[[Booth Newspapers]] |date=November 22, 2008 |accessdate=2009-05-05}}</ref>

In the liner notes of the album, it recommends the reading of [[A People's History of the United States]] by [[Howard Zinn]]. It also recommends the documentaries [[Wal-Mart: The High Cost of Low Price]], [[The Ground Truth]], [[The Future of Food]], [[An Inconvenient Truth]] by [[Al Gore]], and [[Sicko]] by [[Michael Moore]].

==Promotion and release==
[[File:Zach Blair Tim McIlrath Rise Against live 2008.jpg|alt=A man with a shaved head playing a guitar with an intense expression. A harmonica player is in the background.|thumb|Guitarist Zach Blair and vocalist Tim McIlrath (right) playing on the ''Appeal to Reason'' tour on October 11, 2008.]]
Rise Against filmed the music video for ''Appeal to Reason'''s first single, "[[Re-Education (Through Labor)]]" with director [[Kevin Kerslake]].<ref>{{cite news |last=DeAndrea |first=Joe |title=Rise Against to Shoot New Music Video |url=http://absolutepunk.net/showthread.php?t=455791 |publisher=AbsolutePunk.net |date=August 5, 2008 |accessdate=2008-08-12| archiveurl= https://web.archive.org/web/20080823213820/http://absolutepunk.net/showthread.php?t=455791| archivedate= 23 August 2008 <!--DASHBot-->| deadurl= no}}</ref> The single and its music video were released digitally on August 25.<ref>{{cite news |last=DeAndrea |first=Joe |title=Rise Against Single Radio Date |url=http://www.absolutepunk.net/showthread.php?t=460761 |publisher=AbsolutePunk.net |date=August 7, 2008 |accessdate=2009-01-17}}</ref> In December 2008, it was reported that Rise Against would be shooting a music video for their second single, "[[Audience of One (Rise Against song)|Audience of One]]", with director [[Brett Simon]].<ref>{{cite web |date=December 10, 2008 |title=BOOKED: Rise Against - Brett Simon, director |publisher=VideoStatic |url=http://www.videostatic.com/vs/2008/12/booked-rise-aga.html |accessdate=2008-12-10| archiveurl= https://web.archive.org/web/20081211233029/http://www.videostatic.com/vs/2008/12/booked-rise-aga.html| archivedate= 11 December 2008 <!--DASHBot-->| deadurl= no}}</ref> The music video for "Audience of One" premiered on MySpace Music on January 15, 2009.<ref>{{cite news |title=Behind Rise Against's "Audience of One" Video: Political Punks Rock in Miniature |url=http://www.rollingstone.com/music/news/behind-rise-againsts-audience-of-one-video-political-punks-rock-in-miniature-20090128 |publisher=[[Rolling Stone]] |date=January 28, 2009|accessdate=Dec 13, 2015}}</ref> The music video was filmed in [[Los Angeles]] in December and features the band performing in a miniature world on the [[White House]] lawn.<ref>{{cite news |title=Rise Against's 'Audience Of One' Video Premiers On MySpace Music |url=http://www.starpulse.com/news/index.php/2009/01/15/rise_against_s_audience_of_one_video_pre |publisher=Starpulse.com |date=January 15, 2009 |accessdate=2009-01-17| archiveurl= https://web.archive.org/web/20090204041440/http://www.starpulse.com/news/index.php/2009/01/15/rise_against_s_audience_of_one_video_pre| archivedate= 4 February 2009 <!--DASHBot-->| deadurl= no}}</ref> The video for the song "[[Hero of War]]" was released on May 20, 2009, although the song itself was never released as an official single.<ref>{{cite web |title=Rise Against: Behind "Hero of War" and America’s "Broader Disease" |url=http://www.rollingstone.com/rockdaily/index.php/2009/05/20/rise-against-behind-hero-of-war-and-americas-broader-disease/ |publisher=''[[Rolling Stone]]'' |date=May 20, 2009 |accessdate=2009-06-06}}</ref> Radio stations were sent copies of the album's third single, "[[Savior (Rise Against song)|Savior]]", on June 3.<ref name=sacbee>{{cite news |last=Shields |first=Mel |title=You go, girl - across the country |url=http://www.sacbee.com/entertainment/story/1997223.html |work=[[The Sacramento Bee]] |date=July 5, 2009 |accessdate=2009-07-09}} {{Dead link|date=September 2010|bot=H3llBot}}</ref> 

Rise Against began a U.S. tour with [[Thrice]], [[Alkaline Trio]], and [[The Gaslight Anthem]] to promote the album on October 2, 2008, in [[Cleveland, Ohio]].<ref>{{cite news |last=Cohen |first=Jane |author2=Bob Grossweiner |title=Rise Against announces 29-date tour with Alkaline Trio, Thrice |url=http://www.ticketnews.com/Rise-Against-announces-29-date-tour-with-Alkaline-Trio-Thrice08804448 |publisher=TicketNews |date=August 4, 2008 |accessdate=2009-05-03}}</ref> The band co-headlined a 2009 tour with [[Rancid (band)|Rancid]] throughout the summer months.<ref>{{cite news |last=Carman |first=Keith |title=Billy Talent Return For Album Number ''III'' |url=http://www.exclaim.ca/articles/generalarticlesynopsfullart.aspx?csid1=115&csid2=844&fid1=38035 |publisher=''[[Exclaim!]]'' |date=April 23, 2009 |accessdate=2009-05-03}}</ref><ref>{{cite news |title=Billy Talent complete "III" |url=http://www.punknews.org/article/33190 |publisher=Punknews.org |date=April 21, 2009 |accessdate=2009-05-03| archiveurl= https://web.archive.org/web/20090424042736/http://www.punknews.org/article/33190| archivedate= 24 April 2009 <!--DASHBot-->| deadurl= no}}</ref> That was followed by a short tour of the [[United Kingdom|UK]] in November, which was supported by the bands [[Thursday (band)|Thursday]] and [[Poison the Well (band)|Poison the Well]].<ref>{{cite web | author=alex101 | date = May 13, 2009 | title=Rise Against / Thursday / Poison the Well (UK) | publisher=Punknews.org | url=http://www.punknews.org/article/33527 | accessdate=May 18, 2009| archiveurl= https://web.archive.org/web/20090516180506/http://www.punknews.org/article/33527| archivedate= 16 May 2009 <!--DASHBot-->| deadurl= no}}</ref>

==Reception and sales==
{{Album ratings
 |MC = 65/100<ref name="MC">{{cite web|url=http://www.metacritic.com/music/appeal-to-reason|title=Appeal To Reason Reviews, Ratings, Credits, and More at Metacritic|publisher=[[Metacritic]]|accessdate=2013-10-29}}</ref>
 |rev1 = [[AbsolutePunk.net]]
 |rev1Score = 82%<ref name="abspunk">{{cite web |last=Fallon |first=Chris |title=Rise Against - Appeal to Reason |url=http://www.absolutepunk.net/showthread.php?t=595372 |publisher=[[AbsolutePunk.net]] |date=October 6, 2008 |accessdate=December 30, 2009}}</ref>
 |rev2 = [[AllMusic]]
 |rev2Score= {{Rating|3|5}}<ref name="allmusic"/>
 |rev3 = ''[[The A.V. Club]]''
 |rev3Score = B<ref>{{cite web |last=Burgess |first=Aaron |title=Rise Against: Appeal to Reason |url=http://www.avclub.com/articles/rise-against-appeal-to-reason,6806/ |publisher=''[[The A.V. Club]]'' |date=October 6, 2008 |accessdate=December 30, 2009| archiveurl= https://web.archive.org/web/20081206061217/http://www.avclub.com/content/music/rise_against| archivedate=December 6, 2008<!--DASHBot-->| deadurl= no}}</ref>
 |rev4 = ''[[Entertainment Weekly]]''
 |rev4Score = C+<ref>{{cite web |last=Weingarten |first=Marc |title=Appeal to Reason Review |url=http://www.ew.com/ew/article/0,,20230088,00.html |work=[[Entertainment Weekly]] |date=October 1, 2008 |accessdate=December 30, 2009}}</ref>
 |rev5 = ''[[Los Angeles Times]]''
 |rev5Score = {{Rating|2|4}}<ref>{{cite web |last=Brown |first=August |title=Not very revolutionary |url=http://articles.latimes.com/2008/oct/07/entertainment/et-recordrack7 |publisher=''[[Los Angeles Times]]'' |date=October 7, 2008 |accessdate=December 8, 2012}}</ref>
 |rev6 = [[PopMatters]]
 |rev6Score = 5/10<ref name="PopMatters"/>
 |rev7 = ''[[Rolling Stone]]''
 |rev7Score = {{Rating|3.5|5}}<ref name="RS">{{cite web |last=Anderson |first=Kyle |title=Rise Against: Appeal To Reason : Music Reviews |url=http://www.rollingstone.com/artists/riseagainst/albums/album/23226244/review/23306227/appeal_to_reason |publisher=''[[Rolling Stone]]'' |date=October 16, 2008 |accessdate=December 8, 2012 |archiveurl=https://web.archive.org/web/20081005074134/http://www.rollingstone.com/artists/riseagainst/albums/album/23226244/review/23306227/appeal_to_reason |archivedate=October 5, 2008 |deadurl=yes}}</ref>
 |rev8 = ''[[Slant Magazine]]''
 |rev8Score = {{Rating|3.5|5}}<ref>{{cite web |last=Adams |first=Nate |title=Rise Against: Appeal to Reason |url=http://www.slantmagazine.com/music/review/rise-against-appeal-to-reason/1523 |publisher=''[[Slant Magazine]]'' |date=October 7, 2008 |accessdate=December 8, 2012}}</ref>
 |rev9 = ''[[Spin (magazine)|Spin]]''
 |rev9Score = 5/10<ref>{{cite web |last=Peisner |first=David |title=Rise Against, 'Appeal to Reason' (DGC/Interscope) |url=http://www.spin.com/#reviews/rise-against-appeal-reason-dgcinterscope |publisher=''[[Spin (magazine)|Spin]]'' |date=October 13, 2008 |accessdate=December 8, 2012}}</ref>
 |rev10 = [[Sputnikmusic]]
 |rev10Score = {{Rating|3.5|5}}<ref name="sputnikmusic"/>
}}
''Appeal to Reason'' received generally favorable reviews from music critics. It attained a score of 65 out of 100 on Metacritic's average of ten professional reviews.<ref name="MC"/> In his review giving the album an 82% rating, Chris Fallon of [[AbsolutePunk.net]] said, "''Appeal to Reason'' is essentially focused on one big thing: intelligence. There is no fluff here -- the band has put together a fast, smart and generally focused piece of work here.<ref name="abspunk"/> ''[[Rolling Stone]]'' magazine tells of the band's further emergence into the mainstream with ''Appeal to Reason'', "Rise Against may be nervous about leaving the underground behind, but with sharp songs like these, they're ready for the rest of the world."<ref name="RS"/>  [[IGN]] gave the album an 8.2 out of 10 and said, "Rise Against has taken all of its protest attitude and all of its social leanings and has given America another truly great album. ''Appeal To Reason'' is both a wake-up call for the country and a song of hope for those who can see a clear path on the horizon. While the songs may not be as driven and hard as on albums past, they lack nothing in definition and power of the word. And when it comes down to it, Rise Against want you to know that there is power in the meaning."<ref>{{cite web |last=Stewart |first=Bill |title=Rise Against: Appeal to Reason |url=http://www.ign.com/articles/2008/10/07/rise-against-appeal-to-reason |publisher=IGN |date=October 7, 2008 |accessdate=December 30, 2010}}</ref> In his Consumer Guide, however, [[Robert Christgau]] gave the album a "dud" rating ({{Rating-Christgau|dud}}),<ref>{{cite web |last=Christgau |first=Robert |authorlink=Robert Christgau |title=CG: Rise Against |url=http://www.robertchristgau.com/get_artist.php?name=rise+against |publisher=RobertChristgau.com |accessdate=December 8, 2012}}</ref> calling it "a bad record whose details rarely merit further thought."<ref>{{cite web |last=Christgau |first=Robert |title=CG 90s: Key to Icons |url=http://www.robertchristgau.com/xg/bk-cg90/grades-90s.php |publisher=RobertChristgau.com |accessdate=December 8, 2012}}</ref>

''Appeal to Reason'' is Rise Against's second highest charting album to date. It peaked at number three on the U.S. [[Billboard 200|''Billboard'' 200]], selling 64,700 copies in its first week of release.<ref>{{cite news |last=Harris |first=Chris |title=T.I. Continues to Rule The Charts With ''Paper Trail'' |url=http://www.mtv.com/news/articles/1597101/20081015/t_i_.jhtml |publisher=MTV |date=October 15, 2008 |accessdate=2009-04-20| archiveurl= https://web.archive.org/web/20090412131013/http://www.mtv.com/news/articles/1597101/20081015/t_i_.jhtml| archivedate= 12 April 2009 <!--DASHBot-->| deadurl= no}}</ref> The singles released from ''Appeal to Reason'' also charted higher on the U.S. music charts than any of the band's previous releases. "Re-Education (Through Labor)" reached number 22 on ''Billboard''<nowiki>'</nowiki>s Hot Mainstream Rock Tracks chart and number three on the Hot Modern Rock Tracks (now Alternative Songs) chart,<ref name="chart history">[{{BillboardURLbyName|artist=rise against|chart=all}} Artist Chart History - Rise Against]</ref><ref name="Re-Education charts">{{cite web |title=Re-Education (Through Labor) - Rise Against |url={{BillboardURLbyName|artist=rise against|chart=all}} |work=[[Billboard (magazine)|Billboard]] |accessdate=2009-10-12}}</ref> making it Rise Against's highest charting single on a U.S. rock chart, until it was surpassed by "Savior". "Audience of One" reached number four on the Hot Modern Rock Tracks chart.<ref name="chart history" /> "Savior" peaked at number 3 on ''Billboard''<nowiki>'</nowiki>s [[Rock Songs]] chart and at number 3 on the [[Alternative Songs]] (formerly Hot Modern Rock Tracks) chart, making it the highest-charting single to date. "Savior" has also spent the longest of any Rise Against song on the U.S. Rock Charts, with over a year on both the [[Rock Songs]] and [[Alternative Songs]] charts.<ref name="Savior charts">{{cite web|title=Savior - Rise Against |url={{BillboardURLbyName|artist=rise against|chart=all}} |work=[[Billboard (magazine)|Billboard]] |accessdate=2009-08-01 |archiveurl=https://web.archive.org/web/20090802031044/http://www.billboard.com/ |archivedate=2 August 2009 |deadurl=no |df= }}</ref> All three appeared on the [[Canadian Hot 100]] chart.<ref name="Re-Education charts" /><ref name="Savior charts" /><ref name="Audience of One charts">{{cite web |title=Audience of One - Rise Against |url={{BillboardURLbyName|artist=rise against|chart=all}} |work=[[Billboard (magazine)|Billboard]] |accessdate=2009-10-12}}</ref> In December 2010, the album had sold 482,000 copies.<ref>{{cite web|last=O'Donnell|first=Kevin|url=http://www.spin.com/2010/12/first-look-rise-againsts-upcoming-album/3/|title=First Look at Rise Against’s Upcoming Album|publisher=''Spin''|date=December 22, 2010|accessdate=September 24, 2010}}</ref>

==Track listing==
{{tracklist
| collapsed       = no
| headline        = Tracks
| total_length    = 48:23
| all_lyrics      = [[Tim McIlrath]]
| all_music       = [[Tim McIlrath]], [[Joe Principe]], [[Brandon Barnes]] and [[Zach Blair]]
| title1          = Collapse (Post-Amerika)
| length1         = 3:19
| title2          = Long Forgotten Sons
| length2         = 4:01
| title3          = [[Re-Education (Through Labor)]]
| length3         = 3:42
| title4          = The Dirt Whispered
| length4         = 3:09
| title5          = Kotov Syndrome
| length5         = 3:05
| title6          = From Heads Unworthy
| length6         = 3:42
| title7          = The Strength to Go On
| length7         = 3:27
| title8          = [[Audience of One (Rise Against song)|Audience of One]]
| length8         = 4:05
| title9          = Entertainment
| length9         = 3:34
| title10         = [[Hero of War]]
| length10        = 4:13
| title11         = [[Savior (Rise Against song)|Savior]]
| length11        = 4:02
| title12         = Hairline Fracture
| length12        = 4:02
| title13         = Whereabouts Unknown
| length13        = 4:02
}}

{{tracklist
| collapsed       = yes
| headline        = International bonus track
| title14         = Historia Calamitatum
| length14        = 3:23
}}

{{tracklist
| collapsed       = yes
| headline        = Japanese (exclusive) bonus track
| title15         = Sight Unseen <ref>{{cite web|url=http://rateyourmusic.com/release/album/rise_against/appeal_to_reason_f5/|title=Appeal To Reason [Japanese Version] by RiseAgainst|publisher=www.rateyourmusic.com|date=2009-07-13}}</ref>
| length15       = 3:56
}}

{{tracklist
| collapsed       = yes
| headline        = [[iTunes Store|iTunes]] bonus tracks
| title16         = Elective Amnesia
| length16        = 3:54
| title17        = [[Prayer of the Refugee]]
| note17          = live
| length17        = 4:12
}}

==Personnel==
{{col-start}}
{{col-2}}
;Rise Against
* [[Tim McIlrath]] – [[Singer|Lead vocals]], [[rhythm guitar]]
* [[Joe Principe]] – [[Bass guitar]], [[backing vocals]]
* [[Brandon Barnes]] – [[Drum kit|Drums]]
* [[Zach Blair]] – [[Lead guitar]], backing vocals

;Additional musicians
*[[Matt Skiba]] (of [[Alkaline Trio]]) – Additional backing vocals on ''Hairline Fracture''
*[[Chad Price]] (of [[ALL (band)|ALL]]) – Additional backing vocals
;Artwork
*Todd Russell – [[Art Direction]], Design
*Tim Marrs – Artwork, Illustrations
*Tim Harmon – [[Photography]]

{{col-2}}
;Production
* [[Bill Stevenson (musician)|Bill Stevenson]] – [[record producer|Producer]], [[Audio engineering|engineer]]
* Jason Livermore – Producer, engineer
* Andrew Berlin – Engineer, additional production
* Thom Panunzio – [[A&R]]
* Evan Peters – A&R coordinator
* Felipe Patino – Additional engineering
* Lee Miles – Additional engineering
* [[Chris Lord-Alge]] – [[Audio engineering|Mixer]]
* Keith Armstrong – Assistant mixer
* Nik Karen – Assistant mixer
* Brad Townsend – Additional engineering
* [[Ted Jensen]] – [[Audio mastering|Mastering]]
* Cliff Feiman – Production manager

;Management
*Missy Worth – Management
*Stacy Fass – Legal
*Corrie Christopher – Booking agent
*Paul Orescan – Marketing
*Marlen Tsuchii – International booking agent

{{col-end}}

==Charts==
{|class="wikitable sortable"
|-
!Chart (2008)
!Peak<br>position
|-
|[[ARIA Charts|Australian Albums Chart]]<ref name="ACharts.us">{{cite web|title=Rise Against - Appeal To Reason - Music Charts|url=http://acharts.us/album/38278|publisher=ACharts.us|accessdate=2009-05-15}}</ref>
| style="text-align:center;"|7
|-
|[[Ö3 Austria Top 40|Austria Albums Chart]]<ref name="ACharts.us"/>
| style="text-align:center;"|34
|-
|[[Ultratop|Belgium Albums Chart]] ([[Flanders]])<ref>{{cite web|url=http://www.ultratop.be/nl/showitem.asp?interpret=Rise+Against&titel=Appeal+To+Reason&cat=a|title=Belgium (Flanders) chart positions|publisher=ultratop.be|language=Dutch|accessdate=2013-10-29}}</ref>
| style="text-align:center;"|55
|-
|[[Canadian Albums Chart]]<ref name="billboard">{{cite web|url={{BillboardURLbyName|artist=rise against|chart=all}}|title=Appeal to Reason - Rise Against|publisher=''[[Billboard (magazine)|Billboard]]''|accessdate=2009-05-15}}</ref>
| style="text-align:center;"|1
|-
|[[Media Control Charts|Germany Albums Chart]]<ref name="ACharts.us"/>
| style="text-align:center;"|21
|-
|[[Recording Industry Association of New Zealand|New Zealand Albums Chart]]<ref name="ACharts.us"/>
| style="text-align:center;"|34
|-
|[[Sverigetopplistan|Swedish Albums Chart]]<ref name="ACharts.us"/>
| style="text-align:center;"|51
|-
|[[Swiss Music Charts|Swiss Albums Chart]]<ref name="ACharts.us"/>
| style="text-align:center;"|44
|-
|[[UK Albums Chart]]<ref name="ACharts.us"/>
| style="text-align:center;"|68
|-
|[[Billboard 200|US ''Billboard'' 200]]<ref name="billboard"/>
| style="text-align:center;"|3
|-
|US ''Billboard''  [[Top Rock Albums]]<ref name="billboard"/>
| style="text-align:center;"|2 
|-
|US ''Billboard''  Top Modern Rock/Alternative Albums<ref name="billboard"/>
| style="text-align:center;"|2
|-
|US ''Billboard'' Digital Albums<ref name="billboard"/>
| style="text-align:center;"|1
|}

==Certifications==
{|class="wikitable"
|-
!Country
![[Music recording sales certification|Certifications]]
!Sales
|-
|[[Australian Recording Industry Association|Australia]]
| Gold<ref name="ARIA Charts - Accreditations - 2014 Albums">{{cite web|url=http://www.aria.com.au/pages/httpwww.aria.com.aupagesaria-charts-accreditations-albums-2014.htm |title=ARIA Charts - Accreditations - 2014 Albums |publisher=[[Australian Record Industry Association]] |accessdate=2014-05-18 |deadurl=yes |archiveurl=http://www.webcitation.org/6QbUzXLwK?url=http%3A%2F%2Fwww.aria.com.au%2Fpages%2Fhttpwww.aria.com.aupagesaria-charts-accreditations-albums-2014.htm |archivedate=2014-06-25 |df= }}</ref>
|35,000
|-
|[[Canadian Recording Industry Association|Canada]]<ref>{{cite web|url=http://www.musiccanada.com/GPSearchResult.aspx?st=&ica=False&sa=rise%20against&sl=&smt=0&sat=-1&ssb=Cert.%20Date|title=CRIA Database Search for Rise Against|publisher=[[Canadian Recording Industry Association]]|accessdate=2013-10-29}}</ref>
|Platinum
|100,000+
|-
|[[Bundesverband Musikindustrie|Germany]]<ref>{{cite web|url=http://www.musikindustrie.de/no_cache/gold_platin_datenbank/#topSearch |title=Gold-/Platin-Datenbank |publisher=[[Bundesverband Musikindustrie]] |accessdate=2013-10-29 |deadurl=yes |archiveurl=https://web.archive.org/web/20131202022835/http://www.musikindustrie.de:80/no_cache/gold_platin_datenbank/ |archivedate=2013-12-02 |df= }}</ref>
|Gold
|100,000+
|-
|[[Recording Industry Association of America|United States]]<ref>{{cite web|url=http://www.riaa.com/goldandplatinumdata.php?t&artist=Rise%20Against|title=RIAA - Gold & Platinum|publisher=[[Recording Industry Association of America]]|accessdate=2013-10-29}}</ref>
|Gold
|600,000+
|}

==Release history==
{|class="wikitable"
|-
!Region
!Date
!Record Label
|-
|[[United Kingdom]]<ref>{{cite web |title=Appeal To Reason: Rise Against: Amazon.co.uk: Music |url=http://www.amazon.co.uk/dp/B001ESYAOE |publisher=Amazon.co.uk |accessdate=2009-06-06}}</ref>
|October 2, 2008
|[[Polydor Records|Polydor]]
|-
|[[Australia]]<ref>{{cite web |last=Weston |first=Cameron |title=Rise Against: Appeal To Reason |url=http://sydney.citysearch.com.au/music/1137614475672/Rise+Against:+Appeal+To+Reason |archiveurl=https://web.archive.org/web/20110807151852/http://citysearch.com.au/events/rise%20against%3A%20appeal%20to%20reason |archivedate=2011-08-07 |publisher=Citysearch Sydney |date=October 2008 |accessdate=2009-06-06}}</ref>
|October 4, 2008
|[[Universal Records|Universal]]
|-
|[[United States]]<ref name="abspunk" />
|rowspan="2"| October 7, 2008
|rowspan="2"| [[Geffen Records|Geffen]], [[DGC Records|DGC]]/[[Interscope Records|Interscope]]
|-
|[[Canada]]<ref>{{cite web |title=Appeal To Reason: Rise Against: Amazon.ca: Music |url=http://www.amazon.ca/dp/B001FBSMMQ |publisher=Amazon.ca |accessdate=2009-06-06}}</ref>
|-
|[[Germany]]<ref>{{cite web |author=Diadium |url=http://www.whiskey-soda.de/review.php?id=21058 |publisher=Whiskey Soda |title=Review |accessdate=2009-06-06}}</ref>
|October 10, 2008
|[[Rough Trade Records|Rough Trade]]
|}

==References==
{{Reflist|30em}}

{{Rise Against}}

==External links==
<!-- This is a licensed stream for the album, which is allowed under Wikipedia polices -->
*[https://www.youtube.com/playlist?list=PL4cH_oiNvdmAcnv9NWzeCnXBrfzMBz6HU ''Appeal to Reason''] at [[YouTube]] (streamed copy where licensed)

{{DEFAULTSORT:Appeal To Reason}}
[[Category:2008 albums]]
[[Category:Albums produced by Bill Stevenson (musician)]]
[[Category:English-language albums]]
[[Category:DGC Records albums]]
[[Category:Interscope Records albums]]
[[Category:Rise Against albums]]