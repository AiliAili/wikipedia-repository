{{For|the Native American author of the same name|Mourning Dove (author)}}
{{pp-move-indef}}
{{Taxobox
|name = Mourning Dove
|status = LC
|status_system = IUCN3.1
|status_ref = <ref>{{IUCN|id=22690736 
|title=''Zenaida macroura'' 
|assessor=BirdLife International 
|assessor-link=BirdLife International 
|version=2013.2 
|year=2012 
|accessdate=26 November 2013}}</ref>
|image = Mourning Dove 2006.jpg
|image_width = 
|image2 = Zenaida macroura vocalizations - pone.0027052.s009.oga
|image2_caption = Mourning Dove vocalizations
|regnum = [[Animal]]ia
|phylum = [[Chordate|Chordata]]
|classis = [[Bird|Aves]]
|ordo = [[Columbiformes]]
|familia = [[Dove|Columbidae]]
|genus = ''[[Zenaida Doves|Zenaida]]''
|species = '''''Z. macroura'''''
|binomial = ''Zenaida macroura''
|binomial_authority = ([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])
|subdivision_ranks = [[Subspecies]]
|subdivision = See text
|synonyms = * ''Columba carolinensis'' <small>Wilson</small>
* ''Ectopistes carolinensis'' <small>Audubon</small>
|range_map = MourningDoveRange.png
|range_map_width = 
|range_map_caption = {{leftlegend|#00FF00|Summer only range|outline=gray}}{{leftlegend|#008000|Year-round range|outline=gray}}{{leftlegend|#0000FF|Winter only range|outline=gray}}}}

The '''mourning dove''' (''Zenaida macroura'') is a member of the [[dove]] [[Family (biology)|family]], [[Columbidae]]. The bird is also known as the '''American mourning dove''' or the '''rain dove''', and erroneously as the '''turtle dove''', and was once known as the '''Carolina pigeon''' or '''Carolina turtledove'''.<ref>Torres, J.K. (1982) ''The Audubon Society Encyclopedia of North American Birds'', Alfred A. Knopf, New York, p. 730, ISBN 0517032880</ref> It is one of the most abundant and widespread of all North American birds. It is also a leading gamebird, with more than 20 million birds (up to 70&nbsp;million in some years) shot annually in the U.S., both for sport and for meat. Its ability to sustain its population under such pressure is due to its prolific breeding; in warm areas, one pair may raise up to six [[Offspring|broods]] of two young each in a single year. The wings make an unusual whistling sound upon take-off and landing, a form of [[sonation]]. The bird is a strong [[Bird flight|flier]], capable of speeds up to {{Convert|88|kph|abbr = on}}.<ref>{{cite journal|author = Bastin, E.W.|year = 1952|title = Flight-speed of the Mourning Dove|journal = Wilson Bulletin|volume = 64|issue = 1|page = 47|url = http://sora.unm.edu/node/127141}}</ref>

Mourning doves are light grey and brown and generally muted in color. Males and females are similar in appearance. The species is generally [[monogamous]], with two squabs (young) per brood. Both parents incubate and care for the young. Mourning doves eat almost exclusively seeds, but the young are fed [[crop milk]] by their parents.

== Taxonomy ==
The mourning dove is closely related to the [[eared dove]] (''Zenaida auriculata'') and the [[Socorro dove]] (''Zenaida graysoni''). Some authorities describe them as forming a superspecies and these three birds are sometimes classified in the separate genus ''Zenaidura'',<ref name="AOU" /> but the current classification has them as separate species in the genus ''[[Zenaida doves|Zenaida]]''. In addition, the Socorro dove has at times been considered conspecific with the mourning dove, although several differences in behavior, call, and appearance justify separation as two different species.<ref name="Checklist225" /> While the three species do form a subgroup of ''Zenaida'', using a separate genus would interfere with the [[monophyly]] of ''Zenaida'' by making it [[Paraphyly|paraphyletic]].<ref name="AOU" />

There are five [[subspecies]] of mourning dove:

* Eastern ''Z. m. carolinensis'' ([[Carl Linnaeus|Linnaeus]], 1766)
* Clarion Island ''Z. m. clarionensis'' (C.H.Townsend, 1890)
* West Indian ''Z. m. macroura'' ([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])
* Western ''Z. m. marginella'' (Woodhouse, 1852)
* Panama ''Z. m. turturilla'' Wetmore, 1956

The ranges of most of the subspecies overlap a little, with three in the United States or Canada.<ref name="National Geographic" /> The West Indian subspecies is found throughout the [[Greater Antilles]].<ref name="NRCS p3">NRCS p3</ref> It has recently invaded the [[Florida Keys]].<ref name="National Geographic" /> The eastern subspecies is found mainly in eastern North America, as well as [[Bermuda]] and the [[Bahamas]]. The western subspecies is found in western North America, including parts of Mexico. The Panamanian subspecies is located in [[Central America]]. The Clarion Island subspecies is found only on [[Clarion Island]], just off the Pacific coast of Mexico.<ref name="NRCS p3" />

The mourning dove is sometimes called the "American mourning dove" to distinguish it from the distantly related [[mourning collared dove]] (''Streptopelia decipiens'') of Africa.<ref name="AOU" /> It was also formerly known as the "Carolina turtledove" and the "Carolina pigeon".<ref name="Birds of America">{{cite book|title = [[Birds of America (book)|Birds of America]]|authorlink = John James Audubon|author = Audubon, John James|chapter = Plate CCLXXXVVI|chapter-url = http://www.abirdshome.com/Audubon/VolV/00506.html|accessdate = 2006-10-18|isbn = 1-55859-128-1}}</ref> The genus name was bestowed in 1838 by French zoologist [[Charles Lucien Bonaparte|Charles L. Bonaparte]] in honor of his wife, [[Zénaïde Laetitia Julie Bonaparte|Princess Zénaide]], and ''macroura'' is from [[Ancient Greek]] ''makros'', "long" and ''oura'', "tail".<ref name=job>{{cite book | last= Jobling | first= James A | year= 2010| title= The Helm Dictionary of Scientific Bird Names | publisher= Christopher Helm | location = London | isbn = 978-1-4081-2501-4 | pages = 236, 414}}</ref> The "mourning" part of its [[common name]] comes from its call.<ref name="Encyclopedia">{{cite encyclopedia|title = Pigeon|encyclopedia = Encarta Online|publisher = Microsoft|url = http://encarta.msn.com/encyclopedia_761569160/Pigeon.html|accessdate = 2007-02-17|archiveurl = http://www.webcitation.org/5kx6FJCY1|archivedate = 2009-11-01|deadurl = yes}}</ref>

The mourning dove was thought to be the [[passenger pigeon]]'s closest living relative, based on morphological grounds.<ref>{{cite encyclopedia | last = Blockstein | first = David E. | editor-last = Poole | editor-first = Alan| editor2-last = Gill | editor2-first = Frank | title = Passenger Pigeon ''Ectopistes migratorius'' | encyclopedia = The Birds of North America | volume = 611 | publisher = The Birds of North America, Inc. | location = Philadelphia | year = 2002|p = 4}}</ref><ref>{{cite conference |first = Miller|last = Wilmer J.|conference = The Biology and Natural History of the Mourning Dove|title = Should Doves be Hunted in Iowa?|publisher = Ames [[Audubon Society]]|date = 16 January 1969|location = Ames, IA|url = http://www.ringneckdove.com/Wilmer's%20WebPage/mourning__doves.htm|accessdate = 23 April 2013}}</ref> The mourning dove was even suggested to belong to the same genus, ''Ectopistes'', and was listed by some authors as ''E. carolinensis''.<ref>{{cite book | last=Brewer | first=Thomas Mayo | authorlink=Thomas Mayo Brewer | title=Wilson's American Ornithology: with Notes by Jardine; to which is Added a Synopsis of American Birds, Including those Described by Bonaparte, Audubon, Nuttall, and Richardson | publisher=Otis, Broaders, and Company | year=1840 | location=Boston | url=https://books.google.com/?id=bFs3AQAAMAAJ&pg=PA717&lpg=PA717&dq=Ectopistes+carolinensis#v=onepage&q=Ectopistes%20carolinensis&f=false | p=717}}</ref>

== Distribution ==
[[File:Zenaida Macroura.JPG|left|thumb|In Hermosillo, Sonora, Mexico]]
The mourning dove has a large [[Range (biology)|range]] of nearly {{Convert|11000000|km2|abbr = on}}.<ref name="Birdlife International">{{cite web |url = http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=2554&m=0|title = Mourning Dove – BirdLife Species Factsheet|author = Birdlife International|author-link = Birdlife International|accessdate = 2006-10-08}}</ref> The species is resident throughout the [[Greater Antilles]], most of [[Mexico]], the [[Continental United States]], southern [[Canada]], and the Atlantic archipelago of [[Bermuda]]. Much of the Canadian prairie sees these birds in summer only, and southern Central America sees them in winter only.<ref name="NRCS p2">{{cite web |url = http://www.sc.nrcs.usda.gov/intranet/Dick%20Yetter%20Information/Tech%20Notes%20for%20Web%20Site/Biology%20Tech%20Note_31_MourningDove.pdf|title = Mourning Dove ''(Zenaida macroura)''|format = PDF|page = 2|publisher = National Resources Conservation Services (NRCS)|work = Fish and Wildlife Habitat Management leaflet '''31'''|date = February 2006|accessdate = 2006-10-08}}</ref> The species is a [[Vagrancy (biology)|vagrant]] in northern Canada, Alaska,<ref name="Kaufman">{{cite book|title = Lives of North American Birds|author = Kaufman, Kenn|year = 1996|page = 293|publisher = Houghton Mifflin|isbn = 0-395-77017-3}}</ref> and South America.<ref name="AOU">{{cite web|work=A classification of the bird species of South America |author=South American Classification Committee American Ornithologists' Union |title=Part 3. Columbiformes to Caprimulgiformes |url=http://www.museum.lsu.edu/~Remsen/SACCBaseline03.html |accessdate=2006-10-11 |deadurl=unfit |archiveurl=https://web.archive.org/web/20100109122036/http://www.museum.lsu.edu/~Remsen/SACCBaseline03.html |archivedate=January 9, 2010 }}</ref> It has been spotted as an accidental at least seven times in the [[Western Palaearctic|Western Palearctic]] with records from the British Isles (5), the Azores (1) and Iceland (1).<ref name="National Geographic">{{cite book|title = National Geographic Complete Birds of North America|page = 303|editor = Jonathan Alderfer|isbn = 0-7922-4175-4}}</ref> In 1963, the mourning dove was [[Introduced species|introduced]] to [[Hawaii]], and in 1998 there was still a small population in [[Kona, Hawaii|North Kona]].<ref name="Checklist224">{{cite web|url = http://www.aou.org/checklist/pdf/AOUchecklistPter-Apod.pdf|format = PDF|page = 224|title = Check-list of North American Birds|year = 1998|publisher = [[American Ornithologists' Union]]|accessdate = 2007-06-29}}</ref> The mourning dove also appeared on [[Socorro Island]], off the western coast of Mexico, in 1988, sixteen years after the Socorro dove was [[extirpated]] from that island.<ref name="Checklist225">{{cite web|url = http://www.aou.org/checklist/pdf/AOUchecklistPter-Apod.pdf|format = PDF|page = 225|title = Check-list of North American Birds|year = 1998|publisher = [[American Ornithologists' Union]]|accessdate = 2007-06-29}}</ref>

== Description ==
[[File:Mourning Dove on seawall.ogv|left|thumb]]
[[File:Zenaida macroura -California-8-2c.jpg|right|thumb|In [[California]]]]
[[File:Mourning Dove (Zenaida macroura).jpg|thumb|In Guelph, Ontario, Canada.]]
The mourning dove is a medium-sized, slender dove approximately {{Convert|31|cm|abbr = on}} in length. Mourning doves weigh {{Convert|112|-|170|g|abbr = on}}, usually closer to {{Convert|128|g|abbr = on}}.<ref>{{cite web|url = http://www.ringneckdove.com/Wilmer's%20WebPage/mourning__doves.htm|title = The biology and Natural History of the Mourning Dove|accessdate = 2008-04-14|last = Miller|first = Wilmer J.|date = 1969-01-16|quote = Mourning doves weigh 4–6 ounces, usually close to the lesser weight.}}</ref> The [[Bird flight|elliptical wings]] are broad, and the head is rounded. Its tail is long and tapered ("macroura" comes from the [[Greek language|Greek]] words for "large" and "tail"<ref>{{cite book
|author = Borror, D.J.|year = 1960|title = Dictionary of Word Roots and Combining Forms|location = [[Palo Alto]]|publisher = National Press Books|isbn = 0-87484-053-8}}
</ref>). Mourning doves have perching feet, with three toes forward and one reversed. The legs are short and reddish colored. The [[beak]] is short and dark, usually a brown-black hue.<ref name="National Geographic" />

The [[plumage]] is generally light gray-brown and lighter and pinkish below. The wings have black spotting, and the outer tail [[feather]]s are white, contrasting with the black inners. Below the eye is a distinctive crescent-shaped area of dark feathers. The eyes are dark, with light skin surrounding them.<ref name="National Geographic" /> The adult male has bright purple-pink patches on the neck sides, with light pink coloring reaching the breast. The crown of the adult male is a distinctly bluish-grey color. Females are similar in appearance, but with more brown coloring overall and a little smaller than the male. The iridescent feather patches on the neck above the shoulders
are nearly absent, but can be quite vivid on males. Juvenile birds have a scaly appearance, and are generally darker.<ref name="National Geographic" />

All five subspecies of the mourning dove look similar and are not easily distinguishable.<ref name="National Geographic" /> The nominate subspecies possesses shorter wings, and is darker and more buff-colored than the "average" mourning dove. ''Z. m. carolinensis'' has longer wings and toes, a shorter beak, and is darker in color. The western subspecies has longer wings, a longer beak, shorter toes, and is more muted and lighter in color. The Panama mourning dove has shorter wings and legs, a longer beak, and is grayer in color. The Clarion Island subspecies possesses larger feet, a larger beak, and is darker brown in color.<ref name="NRCS p3" />

== Habitat ==
The mourning dove occupies a wide variety of open and semi-open habitats, such as urban areas, farms, prairie, grassland, and lightly wooded areas. It avoids [[swamp]]s and thick [[forest]].<ref name="Kaufman" /> The species has adapted well to areas altered by humans. They commonly nest in trees in cities or near farmsteads.
[[File:Mother Dove and Squabs Nesting.jpg|left|thumb|Adult and squabs in cactus-protected nest, [[High Desert (California)]]]]

== Migration ==
Most mourning doves [[Bird migration|migrate]] along [[flyway]]s over land. On rare occasions, mourning doves have been seen flying over the Gulf of Mexico, but this is exceptional. Spring migration north runs from March to May. Fall migration south runs from September to November, with immatures moving first, followed by adult females and then by adult males.<ref name="NRCS p2" /> Migration is usually during the day, in flocks, and at low altitudes.<ref name="Kaufman" /> However, not all individuals migrate. Even in Canada some mourning doves remain through winter, sustained by the presence of [[bird feeder]]s.

== Sounds ==
This species' call is a distinctive, plaintive cooOOoo-woo-woo-woooo, uttered by males to attract females, and may be mistaken for the call of an [[owl]] at first. (Close up, a grating or throat-rattling sound may be heard preceding the first coo.) Other sounds include a nest call (''cooOOoo'') by paired males to attract their mates to the nest sites, a greeting call (a soft ''ork'') by males upon rejoining their mates, and an alarm call (a short ''roo-oo'') by either male or female when threatened. In flight, the wings make a fluttery whistling sound that is hard to hear. The wing whistle is much louder and more noticeable upon take-off and landing.<ref name="National Geographic" />

== Reproduction ==
[[File:Mourning Dove on Easter day.jpg|left|thumb|upright|Adult]]
[[File:Zenaida macroura MWNH 0568.JPG|thumb|Egg, Collection [[Museum Wiesbaden]]]]
[[Courtship#Courtship in the animal kingdom|Courtship]] begins with a noisy flight by the male, followed by a graceful, circular glide with outstretched wings and head down. After landing, the male will approach the female with a puffed-out breast, bobbing head, and loud calls. Mated pairs will often [[Personal grooming|preen]] each other's feathers.<ref name="Kaufman" />

The male then leads the female to potential nest sites, and the female will choose one. The female dove builds the nest. The male will fly about, gather material, and bring it to her. The male will stand on the female's back and give the material to the female, who then builds it into the nest.<ref name="Cornell">{{cite web|url = http://www.birds.cornell.edu/AllAboutBirds/BirdGuide/Mourning_Dove_dtl.html|title = Mourning Dove|publisher = [[Cornell University|Cornell]] Lab of Ornithology|accessdate = 2006-10-18}}</ref> The nest is constructed of twigs, [[Pinophyta#Foliage|conifer needles]], or [[Poaceae#Structure and growth|grass blades]], and is of flimsy construction.<ref name="NRCS p3" /> Mourning doves will sometimes requisition the unused nests of other mourning doves, other birds, or arboreal mammals such as [[squirrel]]s.<ref name="NRCS p4">NRCS p. 4</ref>

Most nests are in [[trees]], both deciduous and coniferous. Sometimes, they can be found in [[shrub]]s, [[vine]]s, or on artificial constructs like [[buildings]],<ref name="NRCS p3" /> or hanging flower pots.<ref name="Cornell" /> When there is no suitable elevated object, mourning doves will nest on the ground.<ref name="NRCS p3" />

See link below for: courtship dance and mating.

The clutch size is almost always two [[Egg (biology)|eggs]].<ref name="Cornell" /> Occasionally, however, a female will lay her eggs in the nest of another pair, leading to three or four eggs in the nest.<ref name="NRCS p1">NRCS p. 1</ref> The eggs are white, {{Convert|6.6|ml|abbr = on}}, {{Convert|2.57|-|2.96|cm|abbr = on}} long, {{Convert|2.06|-|2.30|cm|abbr = on}} wide, {{Convert|6|-|7|g|abbr = on}} at laying (5–6% of female body mass). Both sexes incubate, the male from morning to afternoon, and the female the rest of the day and at night. Mourning doves are devoted parents; nests are very rarely left unattended by the adults.<ref name="Cornell" /> When flushed from the nest, an incubating parent may perform a nest-distraction display, or a [[Distraction display|broken-wing display]], fluttering on the ground as if injured, then flying away when the predator approaches it.

<center id="mwAY4">
{| id="mwAY8" class="wikitable"

! id="mwAZI" colspan="4" | Hatching and growth
|- id="mwAZM"
| id="mwAZQ" |[[File:Mourning Dove Egg.JPG|160x160px]]
| id="mwAZg" |[[File:Mourning Dove Nesting 20060630.JPG|160x160px]]
| id="mwAZw" |[[File:Mourning Dove Chicks 20060701.JPG|160x160px]]
| id="mwAaA" |[[File:Zenaida macroura2.jpg|160x160px]]
|- id="mwAaQ"
| id="mwAaU" |<center><small id="mwAaY">Egg in nest</small>
| id="mwAac" |<center><small id="mwAag">Nesting in progress</small>
| id="mwAak" |<center><small id="mwAao">Squabs</small>
| id="mwAas" |<center><small id="mwAaw">A juvenile</small>
|}
</center>

[[Avian incubation|Incubation]] takes two weeks. The hatched young, called squabs, are strongly [[altricial]], being helpless at hatching and covered with [[Down feather|down]].<ref name="Cornell" /> Both parents feed the squabs [[Crop milk|pigeon's milk]] (dove's milk) for the first 3–4 days of life. Thereafter, the crop milk is gradually augmented by seeds. [[Fledging]] takes place in about 11–15 days, before the squabs are fully grown but after they are capable of digesting adult food.<ref name="NRCS p4" /> They stay nearby to be fed by their father for up to two weeks after fledging.<ref name="Kaufman" />

Mourning doves are prolific breeders. In warmer areas, these birds may raise up to six broods in a season.<ref name="Kaufman" /> This fast breeding is essential because mortality is high. Each year, mortality can reach 58% a year for adults and 69% for the young.<ref name="NRCS p1" />

The mourning dove is monogamous and forms strong pair bonds.<ref name="NRCS p1" /> Pairs typically reconvene in the same area the following breeding season, and sometimes may remain together throughout the winter. However, lone doves will find new partners if necessary.

== Ecology ==
[[File:Zenaida macroura -Mesa, Arizona, USA -parent and chicks-8a.jpg|right|thumb|Parent and two chicks in [[Arizona]]]]
Mourning doves eat almost exclusively [[seed]]s, which make up more than 99% of their diet.<ref name = "Cornell"/> Rarely, they will eat [[snail]]s or [[insect]]s. Mourning doves generally eat enough to fill their [[Crop (anatomy)|crops]] and then fly away to digest while resting. They often swallow grit such as fine [[gravel]] or [[sand]] to assist with [[digestion]]. The species usually forages on the ground, walking but not hopping.<ref name="Kaufman" /> At bird feeders, mourning doves are attracted to one of the largest ranges of seed types of any North American bird, with a preference for [[canola]], corn, [[millet]], [[safflower]], and [[sunflower]] seeds. Mourning doves do not dig or scratch for seeds, though they will push aside ground litter; instead they eat what is readily visible.<ref name="NRCS p3" /><ref name = "Cornell"/> They will sometimes perch on plants and eat from there.<ref name="Kaufman" />

Mourning doves show a preference for the seeds of certain species of plant over others. Foods taken in preference to others include [[pine]] nuts, [[Liquidambar|sweetgum]] seeds, and the seeds of [[pokeberry]], [[amaranth]], [[canary grass]], [[Maize|corn]], [[Sesame seed|sesame]], and [[wheat]].<ref name="NRCS p3" /> When their favorite foods are absent, mourning doves will eat the seeds of other plants, including [[buckwheat]], [[rye]], [[goosegrass]] and [[smartweed]].<ref name="NRCS p3" />

Mourning doves can be afflicted with several different [[parasite]]s and [[Animal diseases|diseases]], including [[tapeworm]]s, [[nematode]]s, [[mite]]s, and [[Bird louse|lice]]. The mouth-dwelling parasite ''[[Trichomonas gallinae]]'' is particularly severe. While a mourning dove will sometimes host it without symptoms, it will often cause yellowish growth in the mouth and esophagus that will eventually [[Starvation|starve]] the host to death. [[Avian pox]] is a common, insect-[[Vector (epidemiology)|vectored]] disease.<ref name="NRCS p6">NRCS p. 6</ref>

The primary predators of this species are diurnal [[Bird of prey|birds of prey]], such as [[falcon]]s and [[hawk]]s. During nesting, [[corvid]]s, [[grackle]]s, [[housecat]]s, or [[rat snake]]s will prey on their eggs.<ref name="NRCS p1" /> [[Cowbird]]s rarely [[Brood parasitism|parasitize]] mourning dove nests. Mourning doves reject slightly under a third of cowbird eggs in such nests, and the mourning dove's vegetarian diet is unsuitable for cowbirds.<ref name="Peer and Bollinger">{{cite journal|url = http://sora.unm.edu/sites/default/files/journals/auk/v115n04/p1057-p1062.pdf|title = Rejection of Cowbird eggs by Mourning Doves: A manifestation of nest usurpation?|format = PDF|journal = [[The Auk]]|year = 1998|author1=Peer, Brian  |author2=Bollinger, Eric |lastauthoramp=yes |volume = 115|issue = 4|pages = 1057–1062|doi = 10.2307/4089523}}</ref>

== Behavior ==
Like other columbids, the mourning dove drinks by suction, without lifting or tilting its head. It often gathers at drinking spots around dawn and dusk.

Mourning doves sunbathe or rainbathe by lying on the ground or on a flat tree limb, leaning over, stretching one wing, and keeping this posture for up to twenty minutes. These birds can also waterbathe in shallow pools or bird baths. [[Dust bath|Dustbathing]] is common as well.

[[File:Zenaida macroura pair.jpg|thumb|Pair of doves in late winter in Minnesota]]
Outside the breeding season, mourning doves roost communally in dense deciduous trees or in conifers. During sleep, the head rests between the shoulders, close to the body; it is not tucked under the shoulder feathers as in many other species. During the winter in Canada, roosting flights to the roosts in the evening, and out of the roosts in the morning, are delayed on colder days.<ref>{{cite journal|author1=Doucette, D.R. |author2=Reebs, S.G. |lastauthoramp=yes |year = 1994|title = Influence of temperature and other factors on the daily roosting times of Mourning Doves in winter|journal = Canadian Journal of Zoology|volume = 72|pages = 1287–1290|doi = 10.1139/z94-171|issue = 7}}</ref>

== Conservation status ==
[[File:Zenaida macrouraAWP17AA.jpg|thumb|upright|Audubon's Carolina pigeon]]
The number of individual mourning doves is estimated to be approximately 475 million.<ref>Mirarchi, R.E., and Baskett, T.S. 1994. Mourning Dove (''Zenaida macroura''). ''In'' The Birds of North America, No. 117 (A. Poole and F. Gill, eds.). Philadelphia: The Academy of Natural Sciences; Washington, DC: The American Ornithologists' Union.</ref> The large population and its vast range explain why the mourning dove is considered to be of [[least concern]], meaning that the species is not at immediate risk.<ref name="Birdlife International" /> As a [[Game (food)|gamebird]], the mourning dove is well-managed, with more than 20 million (and up to 40–70 million) shot by hunters each year.<ref>Sadler, K.C. (1993) Mourning Dove harvest. ''In'' Ecology and management of the Mourning Dove (T.S. Baskett, M.W. Sayre, R.E. Tomlinson, and R.E. Mirarchi, eds.) Harrisburg, PA: Stackpole Books, ISBN 0811719405.</ref>

== As a symbol and in the arts ==
The eastern mourning dove (''Z. m. carolinensis'') is [[Wisconsin]]'s official [[symbol]] of peace.<ref name="Wisconsin">{{cite web|author = Wisconsin Historical Society|url = http://www.wisconsinhistory.org/Content.aspx?dsNav=Ny:True,Ro:0,Nrc:id-5,N:4294963828-4294963805&dsNavOnly=N:1099&dsRecordDetails=R:CS2908&dsDimensionSearch=D:symbols,Dxm:All,Dxp:3&dsCompoundDimensionSearch=D:symbols,Dxm:All,Dxp:3|title = Wisconsin State Symbols|accessdate = 2014-07-30}}</ref> The bird is also [[Michigan]]'s state bird of peace.<ref name="USA Today">{{cite news |title = Dove hunting finds place on Mich. ballot|author = Audi, Tamara|url = http://www.usatoday.com/news/nation/2006-10-16-dove-hunting_x.htm|publisher = [[USA Today]]|accessdate = 2006-10-25|date = 2006-10-16}}</ref>

The mourning dove appears as the Carolina turtle-dove on plate 286 of [[John James Audubon|Audubon]]'s ''[[Birds of America (book)|Birds of America]]''.<ref name="Birds of America" />

References to mourning doves appear frequently in Native American literature. Mourning dove imagery also turns up in contemporary American and Canadian poetry in the work of poets as diverse as [[Robert Bly]], [[Jared Carter]],<ref>[[Jared Carter|Carter, Jared]] (1993) [http://www.jaredcarter.com/poems/6/ "Mourning Doves"], in ''After the Rain'', Cleveland State Univ Poetry Center, ISBN 0914946978</ref> [[Lorine Niedecker]],<ref>{{cite web |url = http://www.lorineniedecker.org/poems.cfm|title = Poetry|accessdate = 25 November 2012|publisher = Friends of Lorine Niedecker}}</ref> and [[Charles Wright (poet)|Charles Wright]].<ref>[http://archive.salon.com/books/feature/2000/04/14/national_poetry_month/print.html Meditation on Song and Structure]
from ''Negative Blue: Selected Later Poems'' by [[Charles Wright (poet)|Charles Wright]]</ref>

== Closest related species ==
The mourning dove is a related species to the [[passenger pigeon]] (''Ectopistes migratorius''), which was hunted to [[extinction]] in the early 1900s.<ref>[http://www.savethedoves.org/facts.html Facts]. Save The Doves. Retrieved on 2013-03-23.</ref><ref>[http://www.ringneckdove.com/Wilmer's%20WebPage/mourning__doves.htm The Biology and natural history of the Mourning Dove]. Ringneckdove.com. Retrieved on 2013-03-23.</ref><ref>[https://web.archive.org/web/20100325170740/http://www.mdc.mo.gov/nathis/birds/doves/ The Mourning Dove in Missouri]. the Conservation Commission of the State of Missouri (1990) mdc.mo.gov</ref> For this reason, the possibility of using mourning doves for [[De-extinction|cloning]] the passenger pigeon has been discussed.<ref>[http://tiger_spot.mapache.org/Biology/extinct2.html Cloning Extinct Species, Part II]. Tiger_spot.mapache.org. Retrieved on 2013-03-23.</ref>
{{clear}}

== References ==<!-- Condor58:107 -->
{{reflist|30em}}

== External links ==
{{commons category|Zenaida macroura}}
{{Wikispecies|Zenaida macroura}}
{{Spoken Wikipedia|Mourning_Dove.ogg|2008-06-09}}
{{featured article}}
* {{InternetBirdCollection|american-mourning-dove-zenaida-macroura|Mourning dove}}
* [http://www.mbr-pwrc.usgs.gov/id/framlst/i3160id.html Mourning dove - ''Zenaida macroura''] - USGS Patuxent Bird Identification InfoCenter
* [http://www.birds.cornell.edu/AllAboutBirds/BirdGuide/Mourning_Dove.html Mourning dove Species Account] – Cornell Lab of Ornithology
* [http://tolweb.org/movies/Zenaida_macroura/90990 Mourning dove Movies] (Tree of Life)
* [http://doves.uptock.com/ 28 Mourning Dove Photos]
* [http://www.flmnh.ufl.edu/wwwsounds/birds/hardy20sh.wav Mourning dove Bird Sound] at Florida Museum of Natural History
* [https://www.youtube.com/watch?v=ifLq66WDpJo Mourning dove courtship dance and mating] 
* {{VIREO|mourning+dove|Mourning dove}}

{{North American Game}}
{{portalbar|Birds|Animals|Biology}}
{{taxonbar}}
{{Authority control}}

[[Category:Zenaida]]
[[Category:Game birds]]
[[Category:Birds described in 1758]]
[[Category:Birds of Europe|Dove, Mourning]]
[[Category:Birds of North America|Dove, Mourning]]
[[Category:Birds of Central America|Dove, Mourning]]
[[Category:Birds of Saint Pierre and Miquelon|Dove, Mourning]]
[[Category:Birds of the Bahamas|Dove, Mourning]]
[[Category:Birds of the Cayman Islands|Dove, Mourning]]
[[Category:Birds of Cuba|Dove, Mourning]]
[[Category:Birds of the Dominican Republic|Dove, Mourning]]
[[Category:Birds of Haiti|Dove, Mourning]]
[[Category:Birds of Jamaica|Dove, Mourning]]
[[Category:Birds of the Greater Antilles|Dove, Mourning]]
[[Category:Birds of the Turks and Caicos Islands|Dove, Mourning]]
[[Category:Birds of Colombia|Dove, Mourning]]
[[Category:Birds of the United States|Dove, Mourning]]
[[Category:Urban animals|Dove, Mourning]]
[[Category:Fauna of the Sonoran Desert]]
[[Category:Articles containing video clips]]
[[Category:Fauna of the San Francisco Bay Area]]
[[Category:Animal dance]]