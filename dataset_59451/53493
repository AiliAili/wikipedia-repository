{{Refimprove|date=August 2013}}
{{Accounting}}

A [[company]]'s '''[[Profit (accounting)|earnings]] before [[interest]], [[taxes]], [[depreciation]], and [[amortization (tax law)|amortization]]''' (commonly abbreviated '''EBITDA''',<ref>{{cite web|url= http://glossary.reuters.com/index.php/EBITDA |title= EBITDA - Financial Glossary|publisher=Reuters |date=2009-10-15 |accessdate=2012-02-09}}</ref> pronounced {{IPAc-en|iː|b|ɪ|t|'|d|ɑː}},<ref>Professional English in Use Finance, Cambridge University Press</ref> {{IPAc-en|ə|'|b|ɪ|t|d|ɑː}},<ref>{{cite web|url=http://www.howjsay.com/index.php?word=ebitda |title=Pronunciation of ebitda - how to pronounce ebitda correctly |publisher=Howjsay.com |date=2006-10-29 |accessdate=2012-01-21}}</ref> or {{IPAc-en|'|ɛ|b|ɪ|t|d|ɑː}}<ref>{{cite web|url=http://www.alphadictionary.com/goodword/word/EBITDA |title=EBITDA - alphaDictionary – Free English On-line Dictionary |publisher=Alphadictionary.com |date=2001-05-03 |accessdate=2012-01-22}}</ref>) is an [[accounting]] measure calculated using a company's net earnings, before interest expenses, taxes, depreciation and amortization are subtracted, as a proxy for a company's current operating profitability (i.e., how much profit it makes with its present assets and its operations on the products it produces and sells, as well as providing a proxy for cash flow).

==Usage==
Although EBITDA is not a financial measure recognized in [[generally accepted accounting principles]], it is widely used in many areas of finance when assessing the performance of a company, such as securities analysis.  It is intended to allow a comparison of profitability between different companies, by discounting the effects of interest payments from different forms of financing (by ignoring interest payments), political jurisdictions (by ignoring tax), collections of assets (by ignoring depreciation of assets), and different takeover histories (by ignoring amortization often stemming from [[goodwill (accounting)|goodwill]]). EBITDA is a financial measurement of cash flow from operations that is widely used in [[mergers and acquisitions]] of small businesses and businesses in the [[middle-market company|middle market]]. It is not unusual for adjustments to be made to EBITDA to normalize the measurement allowing buyers to compare the performance of one business to another.<ref>{{cite web|url=http://exitpromise.com/adjusted-ebitda/|title=Adjusted EBITDA Definition - Free Tool - ExitPromise|date=4 April 2014|publisher=}}</ref>

A negative EBITDA indicates that a business has fundamental problems with profitability and with cash flow. A positive EBITDA, on the other hand, does not necessarily mean that the business generates cash. This is because EBITDA ignores changes in [[working capital]] (usually needed when growing a business), in capital expenditures (needed to replace assets that have broken down), in taxes, and in interest.

Some analysts do not support omission of capital expenditures when evaluating the profitability of a company: capital expenditures are needed to maintain the asset base which in turn allows for profit. [[Warren Buffett]] famously asked: "Does management think the tooth fairy pays for [[capital expenditures]]?"<ref name="forbesebitda">
{{cite web
| url= http://www.forbes.com/sites/tedgavin/2011/12/28/top-five-reasons-why-ebitda-is-a-great-big-lie/
| title= Top Five Reasons Why EBITDA Is A Great Big Lie 
| work= Forbes | date=2011-12-28 
| accessdate= 2012-11-15
}}
</ref>

== Margin ==
'''EBITDA margin''' refers to EBITDA divided by total revenue (or "total output", "output" differing "revenue" by the changes in inventory).<ref>{{cite web|url= http://www.businessnewsdaily.com/4461-ebitda-formula-definition.html |title= What is EBITDA?|publisher=BusinessNewsDaily |date=2013-05-09 |accessdate=2014-11-15}}</ref>

==Misuse==
Over time, EBITDA has mostly been used as a calculation to describe the performance in its intrinsic nature, which means ignoring every cost that does not occur in the normal course of business. In spite of the fact this simplification can be quite useful, it is often misused, since it results in considering too many cost items as unique, and thus boosting profitability. Instead, in case these sort of unusual costs get downsized, the resulting calculation ought to be called "adjusted EBITDA" or similar.<ref>{{cite web |url=http://www.tigerlogic.com/tigerlogic/company/press/03FY12/reportd.html |title=EBITDA Calculations and Reconciliation |accessdate=2014-02-08 |website=TigerLogic}}</ref>

Because EBITDA (and its variations) are not measures generally accepted under U.S. [[Generally accepted accounting principles|GAAP]], the [[U.S. Securities and Exchange Commission]] requires that companies registering securities with it (and when filing its periodic reports) reconcile EBITDA to net income in order to avoid misleading investors.<ref name="forbesebitda" />

==See also==
* [[Earnings before interest and taxes]] (EBIT)
* [[Earnings before interest, taxes, and amortization]] (EBITA)
* [[Earnings before interest, taxes, and depreciation]] (EBITD)
* [[Earnings before interest, taxes, depreciation, amortization, and restructuring or rent costs]] (EBITDAR)
* [[EV/EBITDA]]
* [[Gross profit]]
* [[Net income]]
* [[Net profit]]
* [[Operating income before depreciation and amortization]] (OIBDA)
* [[Operating margin]]
* [[P/E ratio]]
* [[Revenue]]

==References==
{{Reflist}}

==Further reading==
* {{cite book |last1=McLaney |first1=Eddie |last2=Atrill |first2=Peter |date=2012-06-14 |title=Accounting: an Introduction |publisher=Pearson |edition=6 |isbn=978-0273771838 }}

==External links==
* [http://www.investopedia.com/terms/e/ebitda.asp Investopedia definition of EBITDA]

[[Category:Fundamental analysis]]
[[Category:Profit]]
[[Category:Private equity]]