[[File:Guadeloupe map.png|thumb|right|250px|Guadeloupe, illustrating the locations of the island's principal settlements. The action on 18 December 1809 took place in a bay to the east of [[Sainte-Anne, Guadeloupe|Sainte-Anne]]]]'''Roquebert's expedition to the Caribbean''', was an unsuccessful operation by a [[French Navy|French naval]] squadron to transport supplies to [[Guadeloupe]] in December 1809 at the height of the [[Napoleonic Wars]]. Over the previous year, British [[Royal Navy]] squadrons had isolated and defeated the French Caribbean colonies one by one, until by the autumn Guadeloupe was the only colony remaining in French hands. Cut off from the rest of the world by British blockade squadrons that intercepted all ships coming to or from the island, Guadeloupe was in a desperate situation, facing economic collapse, food shortages and social upheaval, as well as the impending threat of British invasion. In an effort to reinforce and resupply the colony, the French government sent four vessels to the West Indies in November 1809 under Commodore [[François Roquebert]]. Two of the ships were 20-gun [[en flute|flûtes]] carrying supplies and troops. The two others were 40-gun frigates, ordered to protect the storeships on their journey from the British forces operating off both the French and Guadeloupe coasts.

The squadron almost reached the Caribbean without encountering any of patrolling British warships sent to watch for French reinforcements, but was spotted and intercepted by the frigate {{HMS| Junon|1809|6}} on 13 December. ''Junon''{{'}}s captain did not realise the size of the French squadron until it was too late and, despite fierce resistance, his ship was captured and he was mortally wounded. Continuing with the mission, Roquebert successfully delivered the flûtes [[Loire-class flûte#Loire|''Loire]] and [[Loire-class flûte#Seine|''Seine]] to within sight of Guadeloupe and then left them, his frigates making their way back to France without ensuring the safe arrival of their convoy. On 15 December, the small British [[brig]] [[French brig Observateur (1800)|HMS ''Observateur'']], which had witnessed the defeat of ''Junon'', brought news of the French arrival to the blockade squadron anchored off [[Basse-Terre]]. Summoning ships from the surrounding region, the British commander, Captain [[Volant Vashon Ballard]] amassed a significant squadron and forced the French flûtes to anchor in a protected bay at Anse à la Barque, on the southeastern coast of Guadeloupe.

Ineffectual efforts were made to capture the flûtes, but it was not until the arrival of the 74-gun {{HMS|Sceptre|1802|6}} under Captain [[Samuel James Ballard]] that an attack was made in earnest. As small ships engaged the batteries on shore, two frigates entered the bay and attacked the flûtes, causing both to surrender in an hour-long engagement. Both French ships were badly damaged and a fire started during the engagement was able to rapidly spread through them, destroying both ships in succession. Many of the French crew escaped ashore from the wrecks, while others were picked up by British ships. The blockade of Guadeloupe was successfully maintained, and the island was captured in a coordinated invasion a month later. Roquebert's remaining ships were intercepted on 16 January by a patrolling British frigate, but successfully escaped pursuit and eventually returned undamaged to France.

==Background==
During the [[Napoleonic Wars]], the [[Royal Navy]] rapidly and decisively seized control of the war at sea, driving French ships into protected harbours and laying heavy blockades on ports held by the [[First French Empire|French Empire]] and her allies to strangle communications and overseas trade.<ref name="RG17">Gardiner, p. 17</ref> This had a devastating effect on the French [[West Indian]] colonies, particularly the islands of [[Martinique]] and [[Guadeloupe]]. With trade impossible, their economies stagnated while social upheaval and limited food supplies reduced their ability to resist invasion by the large British forces maintained in the region. In the summer of 1808, desperate messages were sent to France from the islands, prompting a succession of French efforts to supply food, reinforcements and trading opportunities during the latter part of 1808 and the first months of 1809.<ref name="WJ206">James, p. 206</ref>  These efforts were  entirely unsuccessful: the few ships that did safely reach the [[Caribbean Sea]] and successfully landed supplies were all intercepted and captured on the return journey, costing the French four [[frigate]]s and numerous smaller ships by the end of February 1809.<ref name="RG75">Gardiner, p. 75</ref>

The British blockade squadrons had intercepted a number of the messages sent from the islands during 1808, and a large expeditionary force was built up on [[Barbados]] with orders to invade and capture the French colonies as swiftly as possible. Their first target was Martinique, which was [[Invasion of Martinique (1809)|invaded and captured]] during February 1809.<ref name="WLC283">Clowes, p. 283</ref> Outlying islands were captured over the next few months and [[Troude's expedition to the Caribbean|a major French reinforcement squadron]] was trapped and then defeated near the [[Îles des Saintes]] in April: the French lost a [[ship of the line]], and two more frigates were captured in June and July as they tried to return to France.<ref name="WJ167">James, p. 167</ref> With such heavy losses, the French took time preparing their next effort while the British were distracted by the [[Reconquista (Santo Domingo)|Reconquista]] in [[Santo Domingo]], a Spanish campaign to drives the French out of the island of [[Hispaniola]] that was eventually concluded in July 1809 with British naval assistance.<ref name="RW243">Woodman, p. 243</ref> By the autumn of 1809, the British commander, Vice-Admiral [[Alexander Cochrane|Sir Alexander Cochrane]], was again developing an expeditionary force, this time aimed at Guadeloupe. He had strengthened the blockade squadron off the island's principal port [[Basse-Terre]], and placed heavier forces at Martinique in case they were required. Individual ships were dispersed in the approaches to the French island, ready to intercept any approaching reinforcement. Other ships operated against ships already anchored off Guadeloupe: one squadron seized the [[corvette]] ''Nisus'' from [[Deshaies]] on 12 December.<ref name="WLC446">Clowes, p. 446</ref>

In the months since Troude's failure, the French had only sent small supply ships to Guadeloupe, while carefully preparing a major expedition at [[Nantes]]. Two French flûtes, 'Loire'', under the command of ''lieutenant de vaisseau'' Joseph Normand-Kergré, and 'Seine'', under the coommand of ''lieutenant de vaisseau'' Bernard Vincent, took on board large quantities of food supplies and over 200 military reinforcements each. To protect these ships two frigates were detailed to escort the convoy to Guadeloupe: ''[[HMS Java (1811)|Renommée]]'', under Commodore [[François Roquebert]], and ''[[French frigate Clorinde (1808)|Clorinde]]'', under Captain [[Jacques Saint-Cricq]].<ref name="WJ186">James, p. 186</ref> The force departed on 15 November 1809 and made rapid progress across the Atlantic, avoiding all contact with British warships. Of the small ships despatched around the same time, none reached Guadeloupe; all were captured in the Western Atlantic or Caribbean by warships sent by Cochrane to patrol for approaching French reinforcements.<ref name="WJ186"/>

==Destruction of HMS ''Junon''==
{{Infobox military conflict
|conflict=Roquebert's expedition to the Caribbean:<br/>Action of 13 December 1809
|partof=the [[Napoleonic Wars]]
| image=
| caption=
|date=13 December 1809
|place=East of [[Antigua]], {{coord|17|18|N|57|00|W}}
|result= French victory
|combatant1={{flagicon|France}} [[First French Empire|French Empire]]  
|combatant2={{flagicon|UK}} [[United Kingdom]]
|commander1=Commodore [[François Roquebert]] 
|commander2=Captain [[John Shortland]] {{KIA}}
|strength1=[[frigate]]s ''[[HMS Java (1811)|Renommée]]'' and ''[[French frigate Clorinde (1808)|Clorinde]]'', flûtes ''[[Loire-class flûte#Loire|Loire]]'' and ''[[Loire-class flûte#Seine|Seine]]''. 
|strength2=[[frigate]] {{HMS|Junon|1809|6}} and brig [[French brig Observateur (1800)|HMS ''Observateur'']] 
|casualties1=21 killed, 18 wounded 
|casualties2=20 killed, 40 wounded, ''Junon'' destroyed.
}}
{{Campaignbox Napoleonic Wars: West Indies}}

Among the ships despatched by Cochrane was the 40-gun frigate {{HMS|Junon|1809|6}} under Captain [[John Shortland]]. ''Junon'' had been captured from the French less than a year earlier at the [[Action of 10 February 1809]], following an unsuccessful attempt to return to France from Guadeloupe.<ref name="EPB376">Brenton, p. 376</ref> Hastily repaired, she had been commissioned into the Royal Navy and added to Cochrane's fleet, from where Cochrane had ordered her to patrol to the east of [[Antigua]] for ships attempting to evade the British blockade. On 13 December, Shortland, in company with the 16-gun [[brig]] [[French brig Observateur (1800)|HMS ''Observateur'']] under Captain Frederick Wetherall, stopped an American merchant ship and boarded her in search of contraband.<ref name="WJ186"/> As he was engaged in the search, four sails appeared to the north. Shortland immediately gave chase, and by 16:00 realised his quarry was a squadron of four frigates. Firing warning shots in an effort to force the frigates to identify themselves, the strange ships raised [[Spanish flag|Spanish colours]]. Initially unconvinced, Shortland raised the recognition signal for Spanish vessels and received the correct reply from the lead frigate. Closing with the ships during the afternoon, ''Junon'' was only a short distance from the lead ship at 17:30 when French colours replaced the Spanish and Roquebert ordered his ships to open fire.<ref name="WLC446"/>

Roquebert had learned the correct signals for Spanish shipping from captured Spanish merchant ships and, having lured ''Junon'' within range, opened a destructive fire from ''Renommée'' that caused severe damage to ''Junon''{{'}}s rigging.<ref name="WJ187">James, p. 187</ref> Unable to manoeuvere away from the French due to the damage suffered in the opening broadside, Shortland returned fire as best he could while closing with ''Renommée'' to inflict maximum damage. As ''Junon'' closed with the flagship, ''Clorinde'' attacked her from the other side and the ''flûtes'' took up stations fore and aft, repeatedly [[raking fire|raking]] the British ship.{{#tag:ref|James, in his report on the engagement with ''Junon'', reports that ''Loire'' and ''Seine'' both fired their guns at ''Junon'', one from in front and one from behind the British frigate, and that the French soldiers discharged their small arms, almost sweeping ''Junon''{{'}}s deck.<ref>James and Chamier (1837), Vol. 5, pp.186-87.</ref> Troude does not mention small arms, but insists that the two flûtes neither fired their guns nor received any cannon fire.<ref>Troude (1867), Vol. 6, p.79.</ref> James also gives the two flûtes heavier guns than do French records.|group=Note}} ''Observateur'' had been some distance behind ''Junon'' when the action began, and was thus not directly engaged by any of the French ships. Wetherall initially fired at ''Clorinde'' from extreme range, but soon recognised that he could do nothing to aid ''Junon'' in the face of overwhelming French numbers and so sailed westward to find and warn other British ships of the approaching French squadron.<ref name="WLC447">Clowes, p. 447.</ref> As ''Observateur'' escaped, ''Junon'' was pounded from all sides, Roquebert's ship coming so close to the British frigate that their rigging tangled and they collided, inflicting further damage. The French squadron was so close to ''Junon'' that the soldiers carried aboard for the garrison on Guadeloupe were able to fire their [[musket]]s at the British top deck, killing many of the sailors manning the guns.<ref name="WJ187"/>

Within ten minutes of the first shot, ''Junon''{{'}}s top deck was almost completely cleared and Shortland had been taken below, his leg broken by grapeshot and his body pierced by several large wooden splinters. Lieutenant Samuel Bartlett Deecker assumed command, and successfully beat off a boarding attack from ''Clorinde''.<ref name="WJ187"/> Pulling ''Junon'' clear off the two full-strength French frigates, Deecker tried to escape but found his ship unresponsive. Within minutes, Roquebert and Saint-Cricq had regained their positions on either side of the British frigate and boarded simultaneously, forcing Deecker to surrender at 18:15.<ref name="RW243"/> The British had suffered heavy losses, with 20 killed and 40 wounded, including Shortland. The prisoners were dispersed among the French squadron and rather than spare the men required to repair and crew the battered ''Junon'', Roquebert had the frigate set on fire and abandoned. French losses were also severe, ''Renommée'' losing 15 killed and three wounded and ''Clorinde'' six killed and 15 wounded. ''Loire'', ''Seine'', and HMS ''Observateur'' suffered no losses at all.<ref name="WJ188">James, p. 188</ref>

==Operations off Guadeloupe==
Although ''Junon'' had been destroyed, ''Observateur'' had escaped from the French squadron and immediately sought out the blockade force off Guadeloupe, the only place that the French squadron could be destined for. Arriving at Basse-Terre at 13:00 on 15 December, Captain Wetherall telegraphed the senior officer on the station, Captain [[Volant Vashon Ballard]] in the frigate [[HMS Hebe (1782)|HMS ''Blonde'']], of the impending arrival of Roquebert's squadron.<ref name="WJ189">James, p. 189</ref> Ballard swiftly gathered his squadron, the frigate [[HMS Thetis (1782)|HMS ''Thetis'']] under Captain George Miller and the [[sloop]]s {{HMS|Hazard|1794|6}} and {{HMS|Cygnet|1804|6}}, and positioned them in the channel between Guadeloupe and the [[Îles des Saintes]], through which Roquebert's ships would have to pass.<ref name="WLC447"/> Urgent messages were sent to all nearby ships and bases, and the following day the sloops {{HMS|Ringdove|1806|6}} and {{HMS|Scorpion|1803|6}} joined the squadron. Ballard placed ''Hazard'' and ''Ringdove'' to watch Basse-Terre while the rest of squadron patrolled to the south of the island.<ref name="WJ189"/>

On the same day that ''Observateur'' arrived off Basse-Terre, the frigate [[HMS Castor (1785)|HMS ''Castor'']] under Captain William Roberts had recaptured the merchant brig ''Ariel'' near [[La Désirade]], taken by Roquebert's squadron two weeks earlier. Roberts also discovered two other ships in the distance to the north and had closed to investigate, discovering the French convoy. Darkness fell soon afterwards and ''Castor'' separated from the French ships, as Roberts sailed westwards as fast as possible to notify Ballard of his sighting, arriving at 04:00 on 17 December.<ref name="WJ189"/> In the aftermath of the destruction of ''Junon'' and the encounter with ''Castor'', Roquebert decided to separate from the convoy and return to France, turning north with ''Clorinde'' and leaving the supply ships to make the journey to Guadeloupe independently. As a result, ''Loire'' and ''Seine'' were sailing unprotected straight towards Ballard's squadron.<ref name="RW244">Woodman, p. 244</ref>

At daylight on 17 December, ''Blonde'' sighted the French flûtes approaching Basse-Terre from the northwest and Ballard advanced on them, blocking them from reaching Basse-Terre. Retreating northwest along the southern coastline of Guadeloupe, the flûtes entered a sheltered cove named Anse la Barque at 10:00, sheltering under two [[shore battery|batteries]] on either side of the bay. Lieutenants Normand-Kergré and Vincent then anchored their ships parallel with the shore, so that they had the maximum number of cannon aimed at the entrance to the cove.<ref name="WJ189"/> As Ballard's ships cruised along the coast in light winds, seeking a way into the well-protected anchorage, other batteries opened fire, one striking ''Ringdove'' off Pointe Lizard. Captain William Dowers of ''Ringdove'' then landed a shore party from his ship and stormed the battery, capturing it in 15 minutes. He demolished the position and withdrew to his ship, rejoining Ballard off Anse la Barque.<ref name="WLC448">Clowes, p. 448</ref> Ballard then tested the feasibility of an attack on the French ships, ordering the 12-gun [[schooner]] {{HMS|Elizabeth|1805|6}} to assess the depth of the entrance to the bay while he in ''Blonde'' attacked the batteries directly at 16:00. Discovering that the entrance was navigable. ''Blonde'' and ''Elizabeth'' withdrew out of range. Operations were then suspended for the evening to allow additional reinforcements to come up. During the night the frigate {{HMS|Freya|1807|6}} (or ''Freija'') under Captain [[John Hayes (Royal Navy officer)|John Hayes]] joined Ballard's squadron.<ref name="WJ190">James, p. 190</ref>

==Destruction of ''Loire'' and ''Seine''==
{{Infobox military conflict
|conflict=Roquebert's expedition to the Caribbean:<br/>Action of 18 December 1809
|partof=the [[Napoleonic Wars]]
| image=
| caption=
|date=18 December 1809
|place=Anse la Barque, [[Guadeloupe]]<br/>{{coord|16|14|25|N|61|19|25|W}}
|result= British victory
|combatant1={{flagicon|France}} [[First French Empire|French Empire]]  
|combatant2={{flagicon|UK}} [[United Kingdom]]
|commander1=Lieutenant Joseph-Normand Kergré and Lieutenant Bernard Vincent
|commander2=Captain [[Samuel James Ballard]]
|strength1=Flûtes ''[[Loire-class flûte#Loire|Loire]]'' and ''[[Loire-class flûte#Seine|Seine]]'' with support from batteries on shore 
|strength2=[[frigate]]s [[HMS Thetis (1782)|HMS ''Thetis'']] and [[HMS Hebe (1782)|HMS ''Blonde'']] with support from [[HMS Sceptre (1802)|HMS ''Sceptre'']], [[HMS Freija (1807)|HMS ''Freija'']], [[HMS Ringdove (1806)|HMS ''Ringdove'']], {{HMS|Hazard|1794|6}}, [[HMS Cygnet (1804)|HMS ''Cygnet'']] and {{HMS|Elizabeth|1805|6}}
|casualties1=Unknown, ''Loire'' and ''Seine'' destroyed
|casualties2=At least 9 killed, 22 wounded
}}
{{Campaignbox Napoleonic Wars: West Indies}}

At 08:30 on the morning of 18 December, a small boat sailed from Anse la Barque with a message offering the British a temporary truce. Simultaneously the British [[ship of the line]] [[HMS Sceptre (1802)|HMS ''Sceptre'']] arrived from [[Fort-de-France|Fort Royal]] on Martinique under the command of Captain [[Samuel James Ballard]], who immediately assumed command of the diverse squadron assembled at the entrance to the bay.<ref name="WLC448"/> Ballard dismissed the French negotiators and ordered an immediate attack on the anchored frigates. His plan was simple: ''Blonde'' and ''Thetis'' would enter the harbour and engage the flûtes directly, while ''Sceptre'' and ''Freija'' would engage the gun batteries to prevent them targeting the small brigs bringing up the rear. The brigs would be towing boats full of sailors and [[Royal Marines]], who would storm the French ships and gun positions as they were engaged with the larger warships.<ref name="RW244"/>

The plan was initially frustrated by light winds, but by 14:25 ''Blonde'' and ''Thetis'' were within range of the gun batteries and fifteen minutes later they were able to open fire on the flûtes, although still at quite a distance. Becalmed in the bay, ''Blonde'' was forced to engage one of the forts instead, fire from the shore causing some damage but not enough to endanger the ship. ''Thetis'' was luckier, and Captain Miller was able to close with one of the French ships, although their identities during the engagement are uncertain in historical accounts and it is not clear which one was first into the action.<ref name="WJ190"/> By 15:35, ''Thetis'' had dismasted her opponent, which surrendered. Moving against the second ship, ''Thetis'' was then also becalmed and was forced to engage the forts instead. As ''Sceptre'' led the remainder of the squadron into the bay, fire was seen spreading through the surrendered ship and, in the face of overwhelming opposition, the second flûte also surrendered at 16:20.<ref name="WJ190"/>

By 17:10, ''Thetis'' and ''Blonde'' began to withdraw from the bay as the fire took hold of the first frigate. The rest of the British squadron, despite heavy cannon fire from a fort on shore, successfully landed their troops and stormed and captured the defences. There were heavy casualties in the storming parties, including Hugh Cameron, captain of ''Hazard'', who was killed by grapeshot.<ref name="WJ191">James, p. 191</ref> At 17:20, the fire reached the magazines of the burning flûte, and the ensuing explosion hurled burning wreckage across the bay. The British ships were largely untouched, but the second French flûte was struck by a large piece of flaming timber, which ignited her mainmast and destroyed her as well. The operations successfully completed, the British ships embarked their landing parties, who had demolished the fortifications around the bay, and returned to open water.{{#tag:ref|With respect to the engagement at Anse à la Barque, Troude reports that the French vessels started to unload their cargo. When the British attacked, Vincent, on ''Seine'', cut her masts and opened her scuttles to flood her. The water did not enter fast enough, so he fired her guns into the hold. This had the effect of starting a fire that led to the explosion that destroyed her, and set fire to ''Loire'', destroying her also. Troude also reports that the British landing party attacked Battery Choppard, of four guns, which repelled them.<ref>Troude (1867), Vol. 6, pp.80-81.</ref>|group=Note}}

In total the British had lost eight killed and 16 wounded on ''Blonde'', six wounded on ''Thetis'', and an uncertain number lost in the [[amphibious operation]], although casualty figures are not known. French losses in the engagement are also uncertain, although most of the crews of ''Loire'' and ''Seine'' were able to quite easily reach the shore.<ref name="WLC448"/> Among those that did make land were a section of prisoners from ''Junon'', including the grievously wounded Captain Shortland. Transported across the island on a wagon in the full glare of the sun, Shortland's condition rapidly worsened and he died on 21 January 1810 without regaining consciousness. He was buried at Basse-Terre with full military honours.<ref name="WJ191"/> Four decades later the battle was among the actions recognised by the clasp "ANSE LA BARQUE 18 DECR. 1809" attached to the [[Naval General Service Medal (1847)|Naval General Service Medal]], awarded upon application to all British participants still living in 1847.<ref name="LG4">{{LondonGazette|issue=20939|startpage=236|endpage=245|date=26 January 1849|accessdate=19 July 2009}}</ref>

==Aftermath==
Roquebert's remaining frigates turned north after parting from the storeships, sighting the British squadron in the distance and grounding on a sandbar off Antigua in their haste to escape. Throwing overboard their guns and stores, the ships were lightened enough to regain open water.<ref name="WLC448"/> They then returned to European waters, avoiding all contact with British shipping until 16 January 1810 at position {{coord|40|50|N|12|09|W}}, approximately {{convert|200|nmi|km}} west of the Portuguese coast, when they encountered the frigate [[HMS Virginie (1796)|HMS ''Virginie'']] under Captain [[Edward Brace]]. Brace shadowed the French frigates for two days, but was unable to close with them and Roquebert made no attempt to use his superior strength against the British frigate. Eventually, Roquebert outran ''Virginie'' and on 23 January reached [[Brest, France|Brest]] safely.<ref name="WJ192">James, p. 192</ref> Within a year, Roquebert and Saint-Cricq would be despatched on another mission to resupply a French colony, sailing with the frigate ''[[HMS Madagascar (1811)|Néréide]]'' to [[Mauritius|Île de France]] in December 1810. Unknown to the French authorities, a British expeditionary force had [[Invasion of Île de France|already captured the island]], and Roquebert's squadron was ambushed in May 1811 and brought to battle off [[Tamatave]] in [[Madagascar]]. ''Néréide'' and ''Rénomee'' were both captured and Roquebert killed in action. ''Clorinde'' only escaped by deserting the other ships in the middle of the engagement, fleeing north and eventually reaching France.<ref name="RG99">Gardiner, p. 99</ref>

In the West Indies, the failure of the main resupply effort resulted in a further drop in morale among the defenders of Guadeloupe. Other smaller ships sent with supplies were captured during the operations against Roquebert's squadron, including the brig ''Béarnais'' captured  on 14 December and ''Papillion'' on 19 December. In January 1810, the blockade tightened: ''Scorpion'' captured the brig ''Oreste'' from inside the harbour at Basse-Terre and ''Freija'' seized several coastal vessels in [[Baie-Mahault]].<ref name="WJ186"/> By 27 January, Cochrane's preparations for the [[Invasion of Guadeloupe (1810)|invasion of Guadeloupe]] were complete and 7,000 men were landed under Lieutenant-General [[George Beckwith (British Army officer)|George Beckwith]]. The French garrison largely deserted, and by 6 February all resistance was defeated and the governor, General [[Manuel Louis Jean Augustin Ernouf|Manuel Ernouf]], surrendered. Over the rest of the month, the few remaining colonies belonging to France and the Netherlands were seized without opposition and the entire Caribbean was either under British or Spanish control, with the exception of the independent state of [[Haiti]].<ref name="RLA333">Adkins, p. 333</ref>

== Notes, citations, and references ==
===Notes===
{{reflist|group=Note}}

===Citations===
{{reflist|2}}

===References===
* {{cite book
 | last = Adkins
 | first = Roy & Lesley
 | authorlink = 
 | year = 2006
 | chapter = 
 | title = The War for All the Oceans
 | publisher = Abacus
 | location = 
 | isbn = 0-349-11916-3 
}}
* {{cite book
 | last = Brenton
 | first = Edward Pelham
 | authorlink = Edward Pelham Brenton
 | year = 1825
 | chapter = 
 | title = The Naval History of Great Britain, Vol. IV
 | url = https://books.google.com/books?id=AqQNAAAAQAAJ&printsec=frontcover&dq=edward+pelham+brenton#PPA398,M1 
 | publisher = 
 | location = 
}}
* {{cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997 |origyear=1900
 | chapter = 
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume V
 | publisher = Chatham Publishing
 | location = 
 | isbn = 1-86176-014-0
}}
* {{cite book
 | last =  Gardiner (ed.)
 | first = Robert
 | authorlink = 
 | year = 2001 |origyear=1998
 | chapter = 
 | title = The Victory of Seapower
 | publisher = Caxton Editions
 | location = 
 | isbn = 1-84067-359-1
}}
* {{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002 |origyear=1827
 | chapter = 
 | title = The Naval History of Great Britain, Volume 5, 1808&ndash;1811
 | publisher = Conway Maritime Press
 | location = 
 | isbn = 0-85177-909-3
}}
* {{cite book | title      = Batailles navales de la France | last       = Troude | first      = Onésime-Joachim | authorlink = Onésime-Joachim Troude | year       = 1867 | publisher  = Challamel ainé | location   = | isbn       = | pages      = | url        = https://books.google.ch/books?id=zfFrDVTkcpsC&oe=UTF-8&redir_esc=y | volume     = }}
* {{cite book
 | last = Woodman
 | first = Richard
 | authorlink = Richard Woodman
 | year = 2001
 | chapter = 
 | title = The Sea Warriors
 | publisher = Constable Publishers
 | location = 
 | isbn = 1-84119-183-3
}}

{{good article}}

{{DEFAULTSORT:Caribbean, Roquebert's expedition}}
[[Category:Naval battles of the Napoleonic Wars]]
[[Category:Conflicts in 1809]]
[[Category:Battles involving France]]
[[Category:Battles involving the United Kingdom]]
[[Category:1809 in the French colonial empire]]
[[Category:1809 in the Caribbean]]