{{Infobox Journal
| cover	=	
| discipline	=	[[Chemistry]]
| language	=	Portuguese, some English
| abbreviation	=	''J. Braz. Chem. Soc.''
| impact = 1.129
| impact-year = 2014
| website	=	http://jbcs.sbq.org.br/ 
| publisher	=	[[Sociedade Brasileira de Química]]
| country	=	[[Brazil]]
| history	=	{{Start date|1990}}–present
| ISSN	=	0103-5053
| eISSN	=	1678-4790
| CODEN	=	JOCSET
}}

The '''Journal of the Brazilian Chemical Society''' (print {{ISSN|0103-5053}}, eISSN {{ISSN|1678-4790}}, [[CODEN]] JOCSET) is a Brazilian [[scientific journal]] in chemistry. It was founded in 1990 and is published by the ''[[Brazilian Society of Chemistry]]'' (''Sociedade Brasileira de Química''), located at the Instituto de Química da [[Universidade de São Paulo]]. The journal is online, and the full text is freely available. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.129, ranking it 100th out of 157 journals in the category "Chemistry Multidisciplinary".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry Multidisciplinary |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

The ''{{lang|pt|[[Sociedade Brasileira de Química|Brazilian Society of Chemistry]]}}'' publishes other chemistry journals with the titles ''[[Química Nova]]'' and ''Química Nova na Escola'' (QNEsc).

The '''Journal of the Brazilian Chemical Society''' should not be confounded with publications from the ''Associação Brasileira de Química'' (ABQ) in Rio de Janeiro. The latter publishes the ''[[Anais da ABQ]]''.

== See also ==
* [[Anais da ABQ]]
* [[Brazilian Journal of Chemical Engineering]]
* [[Química Nova]]
* [[Revista Brasileira de Chímica]]
* [[Revista Brasileira de Engenharia Química, Caderno de Engenharia Química]]

== References ==
{{Reflist}}

== External links ==

* Homepage of the journal: [http://jbcs.sbq.org.br/ J Braz Chem Soc]
* Homepage of the ''Sociedade Brasileira de Química'' (SQB): [http://www.sbq.org.br/ SQB]
* Homepage of the ''Associação Brasileira de Química'' (ABQ): [http://www.abq.org.br/ ABQ]

[[Category:Chemistry journals]]
[[Category:Academic journals published by learned and professional societies of Brazil]]