{{italic title}}
'''''Oxford Studies in Ancient Philosophy''''' is a [[peer-review]]ed [[academic journal]] devoted to the study of [[ancient philosophy]].  The journal is indexed by [[PhilPapers]] and the [[Philosopher's Index]].<ref>[http://philpapers.org/journals PhilPapers Publications List]</ref><ref name=phindex>[http://philindex.org/downloads/PIC_Alphabetical_Coverage.pdf Philosopher's Index Publications List]</ref>  Each volume however is assigned an [[ISBN]] on its own,<ref name=phindex /> and the volumes have been described as being rather more like an [[anthology]] than a journal issue.<ref>Pakaluk, M., "Review: David Sedley, ''Oxford Studies in Ancient Philosophy'', Volume XXV, Winter 2003",  ''Bryn Mawr Classical Review'' 2006.06.18  http://bmcr.brynmawr.edu/2006/2006-06-18.html</ref>

''Oxford Studies in Ancient Philosophy'' was started in 1983 by [[Julia Annas]].  At the time of its founding, it was commended as a supplement or even rival to the journal [[Phronesis (journal)|''Phronesis'']].<ref>Price, A. W., "Review: ''Oxford Studies in Ancient Philosophy'': Volume I", ''Philosophical Books'', vol. 25, no. 2 (April, 1984), pp. 75–77.</ref> It was also criticized for using [[transliteration]]s of the ancient Greek language texts rather than the original alphabet.<ref>Glucker, John, "Review: ''Oxford Studies in Ancient Philosophy'': Volume I", ''Philosophia'', vol. 16, no. 3–4, (1986), p. 434.</ref>  It is one of the major journals for ancient philosophy.<ref>Vanhaelen, M., "Philosophy" in Gagarin, M. (ed.), ''The Oxford Encyclopedia of Ancient Greece and Rome'' ([[Oxford University Press]], 2010), p. 2053.</ref> The journal is published by [[Oxford University Press]] and the current editor is Victor Caston at the [[University of Michigan]].  Apart from Annas, previous editors were Brad Inwood, C. C. W. Taylor and [[David Sedley]].<ref>[http://ukcatalogue.oup.com/category/academic/series/philosophy/osap.do Oxford Studies in Ancient Philosophy - Philosophy Series - Series - Academic, Professional, & General - Oxford University Press]</ref>

==Notes==
{{reflist}}

==External links==
*{{Official website|http://ukcatalogue.oup.com/category/academic/series/philosophy/osap.do}}
*[http://philpapers.org/pub/2482?pub=2482 PhilPapers listing for ''Oxford Studies in Ancient Philosophy'']

[[Category:Ancient philosophy journals]]
[[Category:Publications established in 1983]]


{{philo-journal-stub}}