{{about|the drama by Lady Mary Wroth}}
{{italic title}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}

'''''Love's Victory''''' is a [[Jacobean era]] [[pastoral]] [[closet drama]] written circa 1620 by English [[Renaissance]] writer [[Lady Mary Wroth]]. The play is the first known original pastoral drama and the first original dramatic comedy written by a woman.<ref name=Wynne>{{cite book|last=Wynne-Davies|first=Marion|title=Women Poets of the Renaissance|year=1999|publisher=Routledge|location=New York, NY|isbn=978-0-415-92350-7|pages=361–364}}</ref> It is written primarily in rhyming [[couplet]]s. There are only two known manuscripts of ''Love's Victory'', one of which is an incomplete version located in the [[Huntington Library]] in [[San Marino, California]]. The other version is complete, and is the Penshurst Manuscript which is owned by [[Viscount De L'Isle]], indicating continued ownership by the Sydney family since its creation.<ref name=Renaissance>{{cite book|title=Renaissance Drama by Women: Texts and Documents|year=1996|publisher=Routledge|location=New York, NY|isbn=0-415-09807-6|pages=91–126}}</ref> The play is not as widely read as Wroth's prose work ''[[Urania (novel)|Urania]]'' or her romantic [[sonnet sequence]] ''[[Pamphilia to Amphilanthus]]'', but has been receiving more attention with the increasing interest in [[early modern]] women writers.

==Synopsis==

''Love's Victory'' begins with the goddess [[Venus (mythology)|Venus]] commanding her son [[Cupid]] to cause a group of shepherds and shepherdesses in [[Cyprus]]<ref name=Hannay>{{cite book|last=Hannay|first=Margaret P.|title=Mary Sidney, Lady Wroth|year=2010|publisher=Ashgate Publishing Company|location=Burlington, VT|isbn=978-0-7546-6053-8|pages=220}}</ref> heartache and suffering for not showing her enough reverence. The action then shifts to how Cupid has affected the various characters. After a series of misunderstandings and deceptions among the characters, Venus and Cupid show themselves and reveal their work in achieving, in the end, "Love's Victory."<ref name=Osherow>{{cite web|last=Osherow|first=Michelle|title=Plot Summary for Lady Mary Wroth's Love's Victory|url=http://terpconnect.umd.edu/~knelson/lvplot.html|accessdate=15 September 2012}}</ref> Venus's priests act as a [[Greek chorus|chorus]] throughout the play.

==Characters==
[[file:Maria Ann Lovell as Lady Diana.jpg|thumb|[[Maria Ann Lovell]] as Lady Diana (1826)<ref name=npg>[http://www.npg.org.uk/collections/search/portraitLarge/mw195840/Maria-Ann-Lovell-ne-Lacy-as-Princess-Diana-in-Loves-Victory Maria Ann Lovell (née Lacy) as Princess Diana in 'Love's Victory'], 1826, NPG, Retrieved 25 July 2016</ref>]]
===Shepherds===
Philisses<br/>
Lissius<br/>
Forester<br/>
Lacon<br/>
Rustic<br/>
Arcas

===Shepherdesses===
Musella<br/>
Simeana<br/>
Silvesta<br/>
Climeana<br/>
Dalina<br/>
Phillis<br/>
Musella's Mother

===Temple of Love===
Venus<br/>
Cupid<br/>
Priests

==Plot==

===Act I===
Venus is upset because of the shepherds' and shepherdesses' apparent recent lack of attention towards her, and orders her son Cupid to make them victims to his will. In doing so, she hopes to regain their reverence. Philisses is soon affected by Cupid's arrows, and forlornly swoons for Musella, whom he loves, but who he believes loves Lissius. Lissius recognises Philisses's anguish as heartache, and vows never to fall victim to it. Meanwhile, Silvesta, who has recently been rejected by Philisses, has taken a vow of chastity to become a follower of the goddess [[Diana (mythology)|Diana]], much to the chagrin of Forester, who loves her. Philisses, Dalina, Rustic, Lacon and Climeana decide to play a game in which each person reveals through song their past loves. Venus re-enters berating Cupid for not causing enough characters pain, especially Lissius, who is openly scornful of love. Cupid vows to do so.

===Act II===
Several of the shepherds and shepherdesses witness Forester being rejected by Silvesta, who explains to him her vow of chastity to Diana. Arcas enters and asks the other characters to draw fortunes that he brought. Philisses leaves and is followed by Lissius. When Lissius catches up to him, Philisses reveals his fear that Musella's love lies with Lissius. Lissius, in turn, reveals that it is not Musella he loves, but Philisses's sister, Simeana. Philisses, now relieved, promises to help Lissius win Simeana. The act ends with Venus' priests proclaiming Cupid's power.

===Act III===
Musella questions Silvesta's chaste lifestyle, and claims that true chastity is found in love. Silvesta responds by pitying Musella, saying that she's blinded by love. Musella admits that she loves Philisses, and tells Silvesta that she knows Philisses thinks she loves Lissius. Silvesta tells Musella that Philisses walks in a certain spot every morning, and that she should meet him there one morning and be kind to him. Philisses continues to bemoan his situation. Dalina, Climeana, Simeana and Phillis decide to play a game in which they confess their secrets about who they love. Dalina claims to have been in love two unknown shepherds at the same time. Phillis admits to being in love with Philisses. Climeana proclaims that she is still in love with her ex-lover, Lissius, while Simeana admits that she, too, is in love with Lissius. This leads to a verbal fight between the two of them. Lissius enters and dismisses Climeana's immediate advances. Climeana, Phillis and Dalina leave, and Lissius reveals to Simeana his heartache caused by his love for her. Simeana at first appears sceptical of his love, but soon relinquishes and admits her love for him. Soon after, Venus tells Cupid that Lissius had not suffered enough prior to obtaining his love. Cupid agrees to cause Lissius more suffering.

===Act IV===
Musella hides in the place where Silvesta said Philisses would be. Philisses enters and continues to decry his situation. Musella reveals herself to him, and they both admit their love for each other, but decide to keep it a secret to all but Silvesta. Rustic enters, but soon leaves with Philisses. Lissius and Simeana join Musella, but Simeana quickly leaves, suddenly angry. Simeana begins to distrust Lissius' vows of love, saying that he made the same vows to Climeana. Musella convinces Simeana to trust Lissius, and they make up. More shepherds and shepherdesses enter, and they decide to play a game of riddles. Rustic is unable to form a riddle. They agree to all meet up the next day. Venus and Cupid re-enter and proclaim that they are almost done testing the characters.

===Act V===
Musella is in great distress and is complaining to Simeana. Musella's mother is forcing her to marry Rustic. Musella reveals that her mother claims it is written her father's will that she must marry Rustic. Philisses enters and is confronted with the sad news. Musella and Philisses agree to go to the temple of Venus and "bind our lives, or else our lives make free". Rustic happily encounters Lissius, Dalina and Arcas and tells them of his upcoming marriage to Musella. Arcas reveals that Musella rejected his love in the past, and Lissius and Dalina begin to suspect he's somehow responsible for the marriage. Meanwhile, Climeana and Lacon tell Silvesta of Musella's and Rustic's marriage. Silvesta leaves to intercept Philisses and Musella at the temple. Philisses and Musella are at the temple praying to Venus and Cupid, and prepare to kill themselves when Silvesta shows up and gives them a potion which she says will give them an easy death. Philisses and Musella drink the potion and seemingly die. Simeana enters and sees the bodies, and proclaims that Silvesta must die for what she has done. The rest of the shepherds and shepherdesses arrive at the temple. They grieve for Philisses and Musella. Musella's mother reveals that Arcas told her Musella wantonly sought Philisses' love, which is what coerced her into forcing Musella into marrying Rustic. Later, Silvesta is about to be burned at the stake for the deaths of Philisses and Musella when Forester enters and offers to take her place, which Silvesta accepts. However, Venus and Cupid quickly enter, waking Philisses and Musella from their sleep. The play closes with Forester vowing to continue to love Silvesta, even if it is unrequited, Philisses and Musella promising to wed, as well as Lissius and Simeana, and Dalina and Rustic. Venus promises Arcas that, for his treachory, he will be punished.<ref name="Renaissance"/><ref name="Osherow"/>

==Connections==
One often discussed aspect of ''Love's Victory'' is its relation to Wroth's other works, the works of her family members, and the family members themselves. Wroth, who in life had an affair with her cousin [[William Herbert, 3rd Earl of Pembroke|William Herbert]], recreated her love on the page in more than one venue. For instance the title characters in ''Pamphilia and Amphilanthus'' and Musella and Philisses in this play. Further, it is believed that the character of Rustic, who is unrefined and dull, is based on Wroth's husband, Sir Robert Wroth, whom she didn't get along with.<ref name=Findlay>{{cite book|last=Findlay|first=Alison|title=Playing Spaces in Early Women's Drama|year=2006|publisher=Cambridge University Press|location=Cambridge, UK|isbn=978-0-521-83956-3|pages=88–90}}</ref> Further, some comparisons can be drawn to her uncle's, Sir [[Philip Sydney]], own romantic sonnet sequence, ''[[Astrophel and Stella]]'', specifically in the characters of Musella, Forester and Silvesta.<ref name="Renaissance" /> Even the title characters of Wroth's own sonnet sequence are identical to the protagonists of her prose romance ''Urania''.<ref name="Wynne"/>

===Types of Love===
At the end of the play, four different couples are paired up, each signifying a different kind of love.<ref name="Renaissance"/> <br/>
Musella + Philisses = True love <br/>
Simeana + Lissius = Flawed love <br/>
Silvesta + Forester = Chaste love <br/>
Dalina + Rustic = Comic love <br/>
If the connections to Wroth's life are charted here, then she considered her marriage to her husband Robert (Rustic) comical, whereas her extended affair with her cousin William Herbert (Philisses) was true love.

==References==
{{reflist}}

[[Category:English Renaissance plays]]
[[Category:1600s plays]]