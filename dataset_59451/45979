{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
[[File:Adrian W. Moore.jpg|thumb|A. W. Moore in 2007 (by Guy Burt)]]'''Adrian William Moore''' (born 1956) is a Professor of Philosophy at the [[University of Oxford]] and Tutorial Fellow of [[St Hugh's College]], Oxford. His main areas of interest are: [[Immanuel Kant|Kant]], [[Wittgenstein]], [[history of philosophy]], [[metaphysics]], [[philosophy of mathematics]], [[philosophy of logic]] and [[Philosophy of language|language]], ethics and [[philosophy of religion]].

Described as "one of our very best... contemporary philosophers",<ref>"A. Montefiore (2004) "Adrian Moore Reviewed by Alan Montefiore", in [http://www.balliol.ox.ac.uk/alumni-and-friends/balliol-college-annual-record-2004 "[[Balliol College]] Annual Record"].</ref> his first book, ''The Infinite'', published by [[Routledge]] in 1990, was considered an "authoritative overview of a topic of considerable philosophical importance",<ref>"[[Roger Penrose|R. Penrose]] (1990) "Things Beyond Saying", in ''The Times Literary Supplement'' 26 October, 4569: 1155."</ref> a "fine book... admirably clear... [subtle and] sensitive to the philosophical issues."<ref>"L. Angel (1991) "A. W. Moore, ''The Infinite''", in ''Philosophy in Review'' 11(3): 220–2.</ref> The book was also reviewed favourably in ''Philosophia Mathematica'',<ref>"[[Thomas Tymoczko|T. Tymoczko]] (1994) "A. W. Moore, ''The Infinite'', in ''Philosophia Mathematica'' 2(1):81-6."</ref> ''International Philosophical Quarterly'',<ref>"[[William Craig (philosopher)|W. L. Craig]] (1992) "Feature Book Review: ''The Infinite'', by A. W. Moore, in ''International Philosophical Quarterly'' 32(2): 253–256."</ref> ''Times Higher Education Supplement'',<ref>"[[Mary Tiles|M. Tiles]] (1991) "A. W. Moore, ''The Infinite''", in ''Times Higher Education Supplement''."</ref> and ''Choice''.

This was followed by ''Points of View'' (Oxford University Press 1997): "... a superb book. It brings the rigour, clarity and precision of the best analytical philosophy to bear on a topic that has until now been of pointedly little concern within analytical philosophy."<ref>"[[Mark Sacks]] (1999) "Transcendental Idealism: Between Reproof and Celebration", in ''International Journal of Philosophical Studies'' 7(3): 373–402. See also Nicholas Bunnin's review in ''Philosophy'' (1999) 74(2): 282–95."</ref> His third book, ''Noble in Reason, Infinite in Faculty: Themes and Variations in Kant's Moral and Religious Philosophy'' was reviewed very laudatorily in ''[[Mind (journal)|Mind]]'', ''[[Times Literary Supplement]]'' and ''[[Kantian Review]]''.

His most recent book was published in 2012 by Cambridge University Press with the title ''The Evolution of Modern Metaphysics: Making Sense of Things''. It has been called an “important and remarkable book”<ref>"I. Johansson (2013) "Review: A. W. Moore, The Evolution of Modern Metaphysics: Making Sense of Things", in ''Metaphysica'' 14(1): 127–42.</ref> and said to represent “an extremely impressive achievement.”<ref>"L. Braver (2012) "A. W. Moore: The Evolution of Modern Metaphysics: Making Sense of Things", in ''Notre Dame Philosophical Reviews'' [http://ndpr.nd.edu/news/29679-the-evolution-of-modern-metaphysics-making-sense-of-things [[2012.03.24]]].</ref>

== Education ==
A. W. Moore was educated at The [[Manchester Grammar School]].  He graduated with a B.A. in Philosophy from [[King’s College, Cambridge]], after which he went to Oxford where he studied at [[Balliol College, Oxford|Balliol College]] for his B.Phil and D.Phil in Philosophy, completing the latter with a thesis on ''Language, Time and Ontology'' under the supervision of [[Michael Dummett]].  During his time as a postgraduate at Oxford Moore was awarded the John Locke Prize in Mental Philosophy (in 1980).

== Academic career ==
After receiving his doctorate, Moore spent three years as a Lecturer at [[University College, Oxford]], where he also acted as the Junior Dean.  He then returned to Cambridge as a Junior Research Fellow at King's College.  Since 1988 he has been a Tutorial Fellow at St Hugh's College, Oxford and a University Lecturer in Philosophy. Since 2004, he has been a University Professor of Philosophy. He has also been the Chairman of the Oxford University Philosophical Society (1995–96), Chairman of the Sub-faculty of Philosophy (1997–99), and President of the Aristotelian Society (2014–2015). 

He is currently a Delegate of the [[Oxford University Press]] and he has been editor of the journal ''[[Mind (journal)|MIND]]'' since 2015, joint with Lucy O'Brien. 

Moore was awarded the [[Mind Association]] Research Fellowship (1999–2000) and a [[Leverhulme Trust|Leverhulme]] Major Research Fellowship (2006–09).

In September 2016 he presented a ten-part BBC Radio 4 series entitled ''A History of the Infinite''.

== Philosophical directions ==
{{BLP sources|date=April 2015}}
One of Moore's distinctive contributions to the area of contemporary metaphysics is a bold defence of the idea that it is possible to think about the world 'from no point of view'. This defence is presented in his book, ''Points of View'', which is at the same time a study of ineffability and nonsense. Drawing on Kant and Wittgenstein, he considers transcendental idealism which, he argues, is nonsense resulting from the attempt to express certain inexpressible insights. He applies this idea to a wide range of fundamental philosophical issues, including the nature of persons, value, and God.

Moore's most recent work has been devoted to a thorough study of the history of metaphysics since Descartes, published under the title ''The Evolution of Modern Metaphysics: Making Sense of Things''. Taking as its definition of metaphysics ‘the most general attempt to make sense of things’, the study charts the evolution of metaphysics through various competing conceptions of its possibility, scope and limits: it deals with the early modern period, the late modern period in the analytic tradition and the late modern period in non-analytic traditions. Moore challenges the still prevalent conviction that there is some unbridgeable gulf between analytic philosophy and philosophy of other kinds. He also advances his own distinctive conception of what metaphysics is and why it matters.

Moore is well known not only for his work in the areas or metaphysics and history of philosophy, but also for his contributions to the philosophy of logic and the philosophy of mathematics. In particular, Moore has done much work on the nature of infinity which illustrates his ramified interests. In his book ''The Infinite'', Moore offers a thorough discussion of the idea of infinity and its history, and a defence of finitism. He engages with a wide range of approaches and issues in the history of thought about the infinite, including various paradoxes, as well as the problems of human finitude and death.

In the areas of ethics and religious philosophy, one of the main questions addressed by Moore is this: Is it possible for ethical thinking to be grounded in pure reason? ''In Noble in Reason, Infinite in Faculty: Themes and Variations in Kant's Moral and Religious Philosophy'', Moore looks at Kant’s moral and religious philosophy and uses it to arrive at a distinctive way of understanding and answering this question. Moore identifies three Kantian themes – morality, freedom and religion –  and offers variations on each of these themes in turn. He concedes that there are difficulties with the Kantian view that morality can be governed by ‘pure’ reason, but defends a closely related view involving a notion of reason culturally and socially conditioned.

Moore also has a special interest in the work of [[Bernard Williams]], with whom he was a colleague in Cambridge and about whom he has written extensively. After Williams’ death in 2003 Moore was appointed as one of his literary executors. He edited one of Williams’ posthumously published collections of essays, ''Philosophy as a Humanistic Discipline''.

== Publications ==

=== Books ===
* 1990 ''The Infinite'' (London: Routledge). A revised second edition, with a new preface, was published in 2001.
* 1997 ''Points of View'' (Oxford: Oxford University Press)
* 2003 ''Noble in Reason, Infinite in Faculty: Themes and Variations in Kant's Moral and Religious Philosophy'' (London: Routledge)
* 2012 ''The Evolution of Modern Metaphysics: Making Sense of Things'' (Cambridge: Cambridge University Press)

=== Edited anthologies ===
* 1993 (ed.) ''Meaning and Reference'' (Oxford: Oxford University Press)
* 1993 (ed.) ''Infinity'' (Aldershot: Dartmouth)
* 2006 (ed.) ''Bernard Williams, Philosophy as a Humanistic Discipline'' (Princeton: Princeton University Press)
* 2012 (co-ed.) ''Contemporary Kantian Metaphysics: New Essays on Space and Time'' (Basingstoke: Macmillan)

==References==
{{reflist}}

== External links ==
* [http://users.ox.ac.uk/~shug0255/ Moore’s homepage]
* [http://www.st-hughs.ox.ac.uk/senior-members/events/past-events/adrian-moore-lecture-2009 Lecture on infinity to the St Hugh’s College Association of Senior Members]
* [http://nigelwarburton.typepad.com/philosophy_bites/2008/09/adrian-moore-on.html Podcast on Kant for Philosophy Bites]
* [http://www.bbc.co.uk/programmes/b07wr1lz Episodes of BBC Radio 4 series A History of the Infinite ]

{{DEFAULTSORT:Moore, AW}}
[[Category:Living people]]
[[Category:1956 births]]
[[Category:British philosophers]]
[[Category:Alumni of King's College, Cambridge]]
[[Category:Alumni of Balliol College, Oxford]]