<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = MB.200
 |image = Bloch MB-200.gif
 |caption = 
}}{{Infobox Aircraft Type
 |type = [[Bomber]]
 |manufacturer = [[Societé des Avions Marcel Bloch]]
 |designer = 
 |first flight = 
 |introduction =[[1935 in aviation|1935]]
 |retired =
 |status = 
 |primary user = [[French Air Force]]
 |more users = [[Czechoslovak Air Force]]<br />[[Bulgarian Air Force]]<br />''[[Luftwaffe]]''
 |produced = 1933-1939
 |number built = 332
 |unit cost = 
 |developed from = 
 |variants with their own articles = 
}}
|}
[[File:Bloch MB.200.png|thumb|Aero MB 200]]
The '''MB.200''' was a [[France|French]] [[bomber]] aircraft of the 1930s designed and built by [[Societé des Avions Marcel Bloch]].  A twin-engined high-winged [[monoplane]] with a fixed undercarriage, over 200 MB.200s were built for the [[French Air Force]], and the type was also licence built by [[Czechoslovakia]], but it soon became obsolete, and was largely phased out by the start of the [[World War II|Second World War]].

==Development and design==
The Bloch MB.200 was designed in response to a [[1932 in aviation|1932]] requirement for a new day/night bomber to equip the French Air Force. It was a high-winged all-metal [[Cantilever#In aircraft|cantilever]] [[monoplane]], with a slab-sided fuselage, powered by two [[Gnome & Rhône 14Kirs]] [[radial engine]]s. It had a fixed [[Conventional landing gear|tailwheel undercarriage]] and featured an enclosed cockpit for the pilots. Defensive [[machine gun]]s were in nose and dorsal gun turrets and an under fuselage gondola.<ref name="Taylor p23">Taylor 1981, p.23.</ref>

The first of three [[prototype]]s flew on 26 June 1933.<ref name="Taylor p23"/><ref name="Dassault">[http://www.dassault-aviation.com/en/passion/aircraft/military-bloch-aircraft/mb-200.html?L=1 "Military Bloch aircraft : MB 200"]. ''Dassault Aviation''. Retrieved 23 August 2008.</ref> As one of the winning designs for the competition, (the other was the larger [[Farman F.220|Farman F.221]]),<ref name="Taylor p23"/> an initial order for 30 MB.200s was placed on 1 January 1934,<ref name="Dassault"/> entering service late in that year. Further orders followed, and the MB.200 equipped 12 French squadrons by the end of 1935.<ref name="Taylor p23"/> Production in France totalled over 208 aircraft (4 by Bloch, 19 by [[Breguet Aviation|Breguet]], 19 by [[Ateliers et Chantiers de la Loire|Loire]], 45 by [[Hanriot]], 10 by [[SNCASO]] and 111 by [[Potez]].<ref name="Angel p155">Angelucci 1981, p.155.</ref>

==Operational history==

[[File:Z Brna operovaly těžké bombardéry Licenční MB 200.gif|thumb]]
[[Czechoslovakia]] chose the MB.200 as part of a modernisation program for its air force of the  mid-1930s.  Although at the rate of aircraft development at that time, the MB.200 would quickly become obsolete, the Czechoslovakians needed a quick solution involving the license production of a proven design, as their own aircraft industry did not have sufficient development experience with such a large aircraft, or with [[Aluminium|all-metal]] [[airframe]]s and [[Stressed skin|stressed-skin]] construction, placing an initial order for 74 aircraft. After some delays, both [[Aero Vodochody|Aero]] and [[Avia]] began license-production in [[1937 in aviation|1937]], with  a total of about 124 built.<ref name="Taylor p23"/> Czechoslovakian MB.200s were basically similar to their French counterparts, with differences in defensive armament and other equipment.

The gradual [[Nazi Germany|German]] conquest of Czechoslovakia meant that MB.200s eventually passed under their control, including aircraft that were still coming off the production line. As well as serving in the German ''[[Luftwaffe]]'', some bombers were distributed to [[Bulgaria]].

[[Vichy France]] deployed a squadron of MB.200s against the Allied [[Syria-Lebanon Campaign|invasion of Lebanon and Syria]] in 1941, carrying out at least one daylight bombing mission against British shipping.<ref name="Shoresp1 p245">Shores and Ehrengardt ''Air Pictorial'' July 1970, p. 245.</ref><ref name="Shoresp2 p284">Shores and Ehrengardt ''Air Pictorial'' August 1970, p.284.</ref>

==Variants==
;MB.200.01
:single [[prototype]] -
;MB.200B.4
:main production version - 2x [[Gnome-Rhône 14Kirs]]
;MB.201
:two [[Hispano-Suiza 12Ybrs]] engines<ref name="dassault-aviation.com">http://www.dassault-aviation.com/fr/passion/avions/bloch-militaires/mb-200.html</ref>
;MB.202
:four [[Gnome-Rhône 7Kdrs]] engines<ref name="dassault-aviation.com"/>
;MB.203
:two [[Clerget 14F]] diesel engines<ref name="dassault-aviation.com"/>

==Operators==
[[File:MB-200 Bulharsko.jpg|thumb|Aero MB.200, [[Bulgarian Air Force]], 1941]]
;{{flag|Bulgaria|1878}}: [[Bulgarian Air Force]] - Purchased 12 ex-Czech MB.200s from Germany in 1939, using them as [[Trainer (aircraft)|trainer]]s.<ref name="Balkan p65-6">Green and Swanborough 1989, pp65-66.</ref>
;{{CZS}}: [[Czechoslovak Air Force]]
;{{FRA}}: ''[[Armée de l'Air]]'' (from 1935)
;{{flag|Germany|Nazi}}: ''[[Luftwaffe]]'' (captured)
;{{flagicon|Spain|1931}} [[Spain]]: [[Spanish Republican Air Force]] received at least one aircraft.<ref>[http://members.fortunecity.es/potez/bloch.htm BLOCH 200/210] {{webarchive |url=https://web.archive.org/web/20120319161616/http://members.fortunecity.es/potez/bloch.htm |date=March 19, 2012 }}</ref>

==Specifications (MB.200B.4)==
{{Aircraft specs
|ref=
|prime units?=met
<!--
        General characteristics
-->
|crew=4
|length m=16
|span m=22.45
|height m=3.9
|wing area sqm=62.5
|empty weight kg=4,300
|max takeoff weight kg=7,480
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Gnome-Rhône 14Kirs]]
|eng1 type=14-cyl. 2-row air-cooled radial piston engines
|eng1 kw=649<!-- prop engines -->
<!--
        Performance
-->
|max speed kmh=285
|range km=1,000
|ceiling m=8,000
|climb rate ms=4.33
<!--
        Armament
-->
|guns=3 × {{convert|7.5|mm|in|abbr=on|3}} [[MAC 1934]] machine guns (one for each defensive post). 
|bombs= {{convert|1,200|kg|lb|abbr=on|0}} of bombs
}}

==See also==
{{Portal|Aviation}}
{{Aircontent|
|related=* [[Bloch MB.210|MB.210]]
|similar aircraft=* [[LWS-6 Żubr]]
|lists=
* [[List of Interwar military aircraft]]
* [[List of aircraft of World War II]]
* [[List of aircraft of the French Air Force during World War II]]
* [[List of aircraft of the Spanish Republican Air Force]]
* [[List of bomber aircraft]]
|see also=
}}

==References==
;Notes
{{reflist}}
;Bibliography
* Angelucci, Enzo. ''World Encyclopedia of Military Aircraft''. London, Jane's Publishing, 1981. ISBN 0-7106-0148-4.
* Green, William and Gordon Swanborough. "Balkan Interlude - The Bulgarian Air Force in WWII". ''[[Air Enthusiast]]''. Issue 39, May–August 1989. Bromley, Kent: Tri-Service Press, pp.&nbsp;58–74. ISSN 0143-5450.
* Shores, Christopher S. and Cristian-Jacques Ehrengardt. "Syrian Campaign, 1941: Part 1; Forestalling the Germans; air battles over S. Lebanon". ''Air Pictorial'', July 1970. pp.&nbsp;242–247.
* Shores, Christopher S. and Cristian-Jacques Ehrengardt. "Syrian Campaign, 1941: Part 2; Breaking the back of Vichy air strength; conclusion". ''Air Pictorial'', August 1970. pp.&nbsp;280–284.
* Taylor, Michael J.H. ''Warplanes of the World 1918-1939''. London:Ian Allen, 1981. ISBN 0-7110-1078-1.

==External links==
{{commons category|Bloch MB.200}}
*[http://www.dassault-aviation.com/en/passion/aircraft/military-bloch-aircraft/mb-200.html?L=1 "Military Bloch aircraft : MB 200"]. ''Dassault Aviation''. Retrieved 23 August 2008.
*[http://www.dassault-aviation.com/fr/passion/avions/bloch-militaires/mb-200.html]

{{Bloch aircraft}}
{{Aero Vodochody aircraft}}

[[Category:Bloch aircraft|MB.200]]
[[Category:French bomber aircraft 1930–1939]]