<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= Scorpion 
 |image= File:Textron Airland Scorpion - RIAT 2014.jpg
 |caption=Textron AirLand Scorpion demonstration flight at the 2014 [[Royal International Air Tattoo]]
 |alt=
}}{{Infobox aircraft type
 |type= Military [[attack aircraft|attack]] and [[reconnaissance aircraft]]
 |national origin= [[United States]]
 |manufacturer= [[Textron AirLand, LLC]] 
 |designer=
 |first flight= 12 December 2013
 |introduced=
 |retired=
 |status= Under development
 |primary user=  
 |more users= <!--Limited to three in total; separate using <br> -->
 |produced= <!--years in production-->
 |number built= 2
 |program cost= <!--Total program cost-->
 |unit cost= less than [[US$]]20 million (forecast, December 2013)
 |developed from= 
 |variants with their own articles=
}}
|}

The '''Textron AirLand Scorpion''' is an American jet aircraft proposed for sale to perform [[Attack aircraft|light attack]] and [[Intelligence, surveillance and reconnaissance]] (ISR) duties.  It is being developed by [[Textron AirLand]], a joint venture between [[Textron]] and [[AirLand Enterprises]]. A prototype was secretly constructed by [[Cessna]] at their [[Wichita, Kansas]] facility between April 2012 and September 2013 and first flown on 12 December 2013.<ref name= "avweb1">{{cite web|last=Niles |first=Russ |url=http://www.avweb.com/avwebflash/news/Cessna-Unveils-Military-Jet220588-1.html |title= Cessna Unveils Military Jet |publisher=AVweb |accessdate= 2013-09-19}}</ref><ref name="firstflight">{{Citation |url= http://www.flightglobal.com/news/articles/low-cost-scorpion-fighter-starts-flight-tests-394085/ |title= Low-cost Scorpion fighter starts flight tests |newspaper= Flight global |date= 12 December 2013}}.</ref><ref name= "avweb3">{{cite web|last=Niles |first=Russ |url= http://www.avweb.com/avwebflash/news/Scorpion-First-Flight-As-Expected221111-1.html |title=Scorpion First Flight 'As Expected' |publisher= AVweb |date= 12 December 2013 |accessdate= 14 December 2013}}</ref>

==Development==
===Background and design phase===
In October 2011, AirLand Enterprises approached Textron with the concept of building the "world’s most affordable tactical jet aircraft."  The two companies created a joint venture called Textron AirLand and development of an aircraft began in January 2012. Neither Textron nor its subsidiaries had much experience designing fixed-wing combat aircraft. Textron saw a market for the type: while military aircraft typically grew more expensive, defense budgets declined.<ref name= "info1">{{Citation |url= http://www.aviationweek.com/Article.aspx?id=/article-xml/AW_09_16_2013_p22-615375.xml |title= Textron Unveils Scorpion Light Attack, Recce Jet |newspaper= Aviation week |date= 16 September 2013}}.</ref><ref name= "info2">{{Citation |url= http://www.flightglobal.com/news/articles/pictures-cessna-parent-textron-pushes-new-strike-jet-390584/ |title= Cessna parent Textron pushes new strike jet |newspaper= Flight global |date= 16 September 2013}}.</ref><ref name= "ainonline18Nov13">{{Citation |url= http://www.ainonline.com/aviation-news/dubai-air-show/2013-11-18/textron-scorpion-aims-december-first-flight |title= Textron Scorpion Aims For December First Flight |newspaper= AIN |date= 18 November 2013}}.</ref> Named Scorpion, the first concept had a single engine. In early 2012, engineers reviewed over 12 design configurations that would meet their goals and shortlisted four designs; the team eventually settled on the tandem-seat, twin-engine configuration.

The aircraft was kept secret, being identified by the code name ''SCV12-1'', or simply "the project".  At its peak, the production team was 200 people, which eventually decreased to 170, including 120 engineers. The outside contours were made in May 2012, and wing production started in August 2012. Unconventionally, wind tunnel tests were performed after wing parts were already being made.<ref name= "interestgrows">{{cite web |url= http://www.kansas.com/2013/12/26/3196546/interest-grows-in-no-longer-secret.html |title= Interest grows in no-longer-secret Scorpion tactical jet |publisher= Kansas.com |date= 25 December 2013}}</ref> In a traditional aircraft development program, the [[U.S. Department of Defense |Department of Defense]] or a military service would issue detailed requirements, potentially hundreds of pages long. Instead, Textron AirLand did a market and capability analysis to determine what domestic and foreign forces required but did not have.

The design team made up of personnel from Textron, Cessna, and Bell Helicopter was assembled in one building with everyone focused on the task, enabling decisions to be made in hours instead of days. To not alert any potential competitors, development was kept secret through [[non-disclosure agreements]], obtaining parts from local suppliers, and the natural close-knit, "small town" nature of [[Wichita, Kansas]]. Technology from the Cessna inventory or other existing, readily-available components and hardware were used.<ref>{{Citation |url= http://www.bizjournals.com/wichita/blog/2014/01/textron-airland-scorpion-teams.html |title= Newsmakers: Textron Airland Scorpion team's biggest achievement: Silence |publisher= Biz journals |place= Wichita |date= 30 January 2014}}.</ref> In November, Textron spokesman David Sylvestre confirmed that Cessna had been involved in building the prototype Scorpion, but may not build any production models. Sylvestre stated, "depending on demand and manufacturing capacity needs, the final site of Scorpion manufacturing beyond the initial low rate production (2015) is yet to be decided. It may be built 'at' Cessna, but by the joint venture called Textron AirLand."<ref name="avweb2">{{cite web|last= Niles |first=Russ |url=http://www.avweb.com/avwebflash/news/Scorpion-May-Not-Be-Built-By-Cessna221062-1.html |title= Scorpion May Not Be Built By Cessna |publisher=AVweb |date=1 December 2013 |accessdate= 2 December 2013}}</ref>

The Scorpion was unveiled on 16 September 2013.<ref name= "avweb1" /><ref name="info1"/><ref name="info2"/><ref>{{cite web |url= http://www.airforcemag.com/DRArchive/Pages/2013/September%202013/September%2017%202013/Textron-AirLand-Unveils-Scorpion-Light-Attack-Jet.aspx |title= Daily Report, Textron AirLand Unveils Scorpion Light Attack Jet |work= Air Force Magazine |date= 17 September 2013 |accessdate=8 December 2014}}</ref> In 2014, the development-to-flight time was expected to take 4–5 years, the goal of the first flight within at least 24 months was achieved. The phrase "speed is paramount" serves as impetus for the program, with the objective of creating the plane, flying it, and selling it as fast as possible to not miss opportunities.<ref name="interestgrows"/> If a customer can be found, production could begin in 2015, and deliveries from 15–18 months after an order is received.<ref name= "interestgrows" />  The plan is to secure a contract first, then begin low-rate production and transition to full-rate production.<ref>{{Citation |url= http://www.kansas.com/2014/03/03/3322304/textron-lets-40-contract-workers.html |title= Textron lets 40 contract workers go from Scorpion program |publisher= Kansas |date= 3 March 2014}}.</ref> Textron AirLand sees a market for up to 2,000 Scorpion jets.<ref>{{Citation |place= Wichita |url= http://www.bizjournals.com/wichita/blog/2014/04/textron-airland-sees-2-000-plane-market-for.html |title= Textron AirLand sees 2,000-plane market for Scorpion jet |publisher= Biz journals |date= 2 April 2014}}.</ref>

===Initial flight testing===
The Scorpion demonstrator completed pre-flight taxi trials on 25 November 2013 in preparation for its first flight.<ref>{{Citation |url= http://www.flightglobal.com/news/articles/video-scorpion-light-strike-aircraft-completes-pre-flight-taxi-trial-393589/ |title= Video: Scorpion light strike aircraft completes pre-flight taxi trial |newspaper= Flight global |date= 27 November 2013}}.</ref><ref name= "Niles26Nov13">{{cite news|url= http://www.avweb.com/avwebflash/news/Scorpion-First-Flight-Planned-For-Next-Week221038-1.html |title= Scorpion First Flight Planned For Next Week|accessdate= 26 November 2013 |last= Niles|first= Russ |date= 26 November 2013 |work= AVweb}}</ref><ref>{{cite web|last=Pew |first=Glenn |url= http://www.avweb.com/avwebflash/news/Textron-Scorpion-Jet-First-Flight-Expected-Tuesday221087-1.html |title=Textron Scorpion Jet First Flight Expected Tuesday |publisher=AVweb |date=7 December 2013 |accessdate= 7 December 2013}}</ref> The Scorpion first flew on 12 December 2013 for 1.4 hours. The aircraft has the [[Aircraft registration |civilian registration]] N531TA and is designated as a Cessna E530.<ref name= "FAAReg">{{cite web|url= http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=531TA |title= N-Number Inquiry Results|accessdate= 15 June 2016| publisher= [[Federal Aviation Administration]] |date= 15 June 2016}}</ref> The flight occurred 23 months after the aircraft's conception, and the flight certification program will last two years. Textron AirLand aimed to complete 500 flight hours and verify basic performance features by the end of 2014.<ref name= "firstflight"/><ref name= "avweb3"/> Initial flight tests showed positive results in evaluations of performance and mechanical and electronic systems.<ref>{{Citation |url= https://finance.yahoo.com/news/textron-airland-scorpion-completes-additional-214400089.html |title= Textron AirLand’s Scorpion Completes Additional Test Flights and Participates in Industry Conferences |publisher= Yahoo |date= 14 February 2014}}.</ref> On 9 April 2014, Textron AirLand announced that the Scorpion had reached 50 flight hours over 26 flights. It was flown as high as {{convert |30,000|ft|m|abbr=on}}, at speeds up to {{convert|310|knot|mph km/h|abbr=on}} and {{convert |430|knot|mph km/h|abbr=on}}, and subjected to accelerations ranging from 3.7 to &minus;0.5 g. Stall speed was identified at slower than {{convert |90|knot|mph km/h|abbr=on}}.  Other tests performed included single-engine climbs and in-flight engine shutdown and restart.  Pilots reported that the Scorpion was nimble, agile, and powerful even when flown on one engine, with good low speed characteristics. It also demonstrated an intercept of a [[Cessna 182]]. Few issues were encountered, attributed to the use of mature, non-developmental systems.<ref>{{Citation |url= https://online.wsj.com/article/PR-CO-20140409-908382.html |title= Textron AirLand's Scorpion ISR/Strike Aircraft Reaches 50 Test Flight Hours and Achieves Mach 0.72 Air Speed |newspaper= WSJ |date= 9 April 2014}}.</ref><ref>{{cite web |last= Durden |first=Rick |url= http://www.avweb.com/avwebflash/news/Scorpion-Prototype-Demonstrates-GA-Intercept-Capability221848-1.html |title= Scorpion Prototype Demonstrates GA Intercept Capability |publisher= AVweb |date=13 April 2014 |accessdate=14 April 2014}}</ref>

[[File:FIA2014 (15495938531).jpg|thumb|A Scorpion at the Farnborough International Airshow, July 2014]]
The Scorpion had flown 76.4 hours in 41 test flights by 19 May 2014; no planned flights were cancelled due to mechanical or maintenance issues. Incremental improvements were to be made to the aircraft over the course of testing. Participation in the [[Farnborough Airshow |Farnborough International Airshow]] in 2014 accelerated changes; modifications included an engine inlet ice protection system and a metal inlet leading edge in place of the composite one for flying in a broader range of weather conditions, a cockpit ladder so the pilot does not need a ground crew ladder, an onboard oxygen-generating system in place of oxygen bottles, and other non-urgent items. The modified Scorpion resumed flights on 1 June 2014.<ref>{{Citation |url= http://www.kansas.com/2014/05/19/3464090/textron-making-changes-in-scorpion.html |title= Textron making changes in Scorpion tactical jet, will fly it to airshow in England |publisher= Kansas |date= 19 May 2014}}.</ref> In July 2014, the Scorpion made its first public appearance at Farnborough Airshow.<ref name=bbc-20140715>{{cite news |url= http://www.bbc.co.uk/news/business-28260781 |title=Farnborough Airshow: The Scorpion in search of a customer |first =Russell |last= Hotten |publisher=BBC |date= 15 July 2014 |accessdate=15 July 2014}}</ref>

The first production-standard aircraft first flew on the 22 December 2016. It has a simplified landing gear, increased wing sweep and new avionics including [[HOTAS|hands-on-throttle-and-stick]] controls.<ref>{{cite web|url=https://www.flightglobal.com/news/articles/picture-first-production-standard-scorpion-gets-air-432726/ |title=First production-standard Scorpion gets airborne|date=23 December 2016|last=Hoyle|first=Craig|website=flightglobal.com}}</ref><ref>{{Citation |url=http://investor.textron.com/news/news-releases/press-release-details/2016/First-Production-Conforming-Scorpion-Jet-Completes-Successful-First-Flight/default.aspx |title= First production conforming Scorpion jet compete successful maiden flight |newspaper= Textron news |date= 22 December 2016}}.</ref>

==Design==
[[File:Textron AirLand Scorpion pre flight check.JPG|thumb|right|Textron AirLand Scorpion during pre-flight check at Bulgarian air base [[Graf Ignatievo Air Base|Graf Ignatievo]]]]
The Scorpion is a [[tandem-seat]] [[twinjet]] aircraft with an all-[[composite material]] fuselage designed for light attack and [[Intelligence, surveillance and reconnaissance|intelligence, surveillance, and reconnaissance]] missions.  Production costs were minimized by using common [[commercial off the shelf]] technology, manufacturing resources and components developed for Cessna's business jets; such as the flap drive mechanism is from the [[Cessna Citation Excel|Cessna Citation XLS]] and [[Cessna Citation Mustang]], the [[aileron]] drive mechanism is from the [[Cessna Citation X |Citation X]].<ref name="firstflight"/><ref name="info1"/><ref name= "info2" /><ref name="ainonline18Nov13"/><ref name="Armedforces">{{Citation |url= http://www.armedforces-int.com/news/scorpion-low-cost-multirole-jet-design-unveiled.html |title= Scorpion Low-Cost Multirole Jet Design Unveiled |newspaper= Armed forces Int. |date= 17 September 2013}}.</ref> Textron AirLand calls the Scorpion an ISR/[[strike aircraft]], instead of a "light attack" aircraft.  The joint venture also states the Scorpion is intended to handle "non-traditional ISR" flights such as those performed by U.S. fighters in Iraq and Afghanistan. The Scorpion is designed to cheaply perform armed reconnaissance using sensors to cruise above 15,000&nbsp;ft, higher than most ground fire can reach, and still be rugged enough to sustain minimal damage.<ref>{{Citation |url= http://aviationweek.com/blog/sting-tail |title= Sting In The Tail |newspaper= Aviation week |date= 17 July 2014}}.</ref>

The Scorpion is designed to be affordable, costing US$ 3,000 per flight hour, with a unit cost expected to be below US$ 20 million.<ref name= bbc-20140715 />  Although it is a two-seat aircraft, it can be flown by a single pilot. Textron AirLand selected [[Cobham plc]] to design the cockpit, which will feature modern flat-panel displays. The aircraft will not have [[fly-by-wire]] to keep costs down and simplify the design.  The demonstrator, as well as production versions, are powered by two [[Garrett TFE731|Honeywell TFE731]] [[turbofan]]s producing {{convert |8,000|lb|kg|abbr=on}} of thrust total. According to Textron AirLand, endurance is optimized for spending 5 hours carrying out a loiter up to 150 miles from base.<ref>{{cite web|url= http://www.scorpionjet.com/missions/irregular-warfare-support/ |title= Irregular Warfare Support|work= Scorpion jet |publisher= Textron AirLand |accessdate= 27 July 2015}}</ref>  Kaman Composites, a subsidiary of [[Kaman Aircraft|Kaman Aerosystems]], provided several components for the Scorpion prototype, including the wing assembly, vertical and horizontal stabilizers, wing fuel access panels, main landing gear doors, and several closeout panels.<ref>{{Citation |url= http://www.marketwatch.com/story/kaman-aerosystems-provides-products-for-the-textron-airland-scorpion-program-2013-10-15 |title= Kaman Aerosystems Provides Products for the Textron AirLand Scorpion Program |newspaper= Market watch |date= 15 October 2013}}.</ref>

Except for the landing gear and engine fittings and mounts, the [[airframe]] is all-composite with an anticipated service life of 20,000 hours. The Scorpion is to have a {{convert |3,000|lb|kg|abbr=on}} payload of precision and non-precision munitions or intelligence-collecting equipment in a simplified and reconfigurable internal bay. The {{convert|14.4|m|ft|abbr=on}} wings are largely unswept and have six hardpoints. A modular design allows for the wings to be removed and replaced by different wing designs.<ref name="firstflight"/><ref name= "info1"/><ref name="info2"/><ref name="ainonline18Nov13" /><ref name= "Armedforces" /> The internal payload bay has a payload capacity of {{convert |3000|lb|kg|abbr=on}}.<ref name=dims>{{cite web|url= http://www.scorpionjet.com/aircraft-features/#tab15a1041a |title= Aircraft Features |work= Scorpion jet |publisher= Textron AirLand |accessdate= 8 December 2014}}</ref>  The external hardpoints have a payload capacity of {{convert|6200|lb|kg|abbr=on}}.<ref name="analysis"/><ref name= ext_stores>{{cite web |url= http://www.scorpionjet.com/aircraft-features/#tab2eaa05e4 |title=Aircraft Features|work= Scorpion jet |publisher= Textron AirLand|accessdate=8 December 2014}}</ref>

==Operational history==

===Later flight testing===
In August 2014, the Scorpion participated in a scenario which involved a mock large chemical spill, requiring cleanup and search-and-rescue operations. A Textron test pilot flew the Scorpion, which circled the area for a few hours while transmitting full motion video to U.S. [[Air National Guard]] members. The purpose was to demonstrate the aircraft's intelligence and reconnaissance capabilities to fill a niche for Air National Guard missions, and be a promotional exercise.<ref>{{Citation |url= http://www.nationaldefensemagazine.org/archive/2014/August/Pages/TextronAirLandTargetsNationalGuardforPotentialSales.aspx |title= Textron AirLand Targets National Guard for Potential Sales |newspaper= National defense magazine |date= Aug 2014}}.</ref> The Scorpion achieved 100 percent mission availability, providing color HD full motion video and communications with other aircraft and ground stations.<ref>{{Citation |url= http://investor.textron.com/newsroom/news-releases/press-release-details/2014/Textron-AirLand-Scorpion-Participates-in-USNORTHCOMs-VIGILANT-GUARD-Exercise/ |title= Textron AirLand Scorpion Participates in USNorthCom’s Vigilant Guard Exercise |publisher= Textron |type= press release |date= 14 August 2014}}.</ref>

===Sales opportunities===
The aircraft is intended to handle mission profiles typically performed by the U.S. Air National Guard, including domestic interdiction, quick-reaction natural disaster support, air sovereignty patrols, and low-threat battlefield missions. The manufacturer claims it to be low-cost and operate for about [[United States dollar|US$]]3,000 per hour. The light attack and reconnaissance roles are typically filled by [[turboprop]] airplanes and [[Unmanned aerial vehicle|UAVs]], often at lower cost.<ref name="avweb1"/><ref name="info1"/><ref name="info2" /><ref>{{cite web|last=Bertorelli |first= Paul |url= http://www.avweb.com/blogs/insider/Cessna-Builds-a-Fighter-220589-1.html |title= Cessna Builds a Fighter |publisher=AVweb |accessdate= 2013-09-19}}</ref> A concept for U.S. military adoption revolves around the [[Lockheed Martin F-35 Lightning II]], a high-cost aircraft for high-threat missions; a requirement could emerge for the low-cost Scorpion to handle low-threat missions.<ref>{{Citation |url= http://www.aviationweek.com/Blogs.aspx?plckBlogId=Blog:27ec4a53-dcc8-42d0-bd3a-01329aef79a7&plckPostId=Blog%3a27ec4a53-dcc8-42d0-bd3a-01329aef79a7Post%3ab3f8b066-408d-4185-8f95-f72e1b481f25 |title= Textron's Scorpion to Take Flight |newspaper= Aviation week |date= 22 October 2013}}.</ref> It could reopen the historically small market for tactical aircraft; a projected 60 nations may require tactical aircraft but cannot afford high-end types. Nations operating turboprop aircraft may view the Scorpion as a cost-effective jet replacement, and F-16 operators may see a less capable aircraft as being able to meet many of their requirements.<ref name="interestgrows" />

The target market is the U.S. Air National Guard and foreign nations that cannot afford the F-35, but want an aircraft to perform ISR and light attack missions better than turboprop planes.<ref name= "analysis">{{Citation |url= http://www.flightglobal.com/news/articles/analysis-textrons-scorpion-will-struggle-to-find-a-niche-390725/ |title= Analysis: Textron's Scorpion will struggle to find a niche |newspaper= Flight global |date= 19 September 2013}}.</ref> Buying and sustaining the Scorpion would cost less than A-10 or F-16 upgrades. For air patrol, the Scorpion requires radar and the capability of supersonic flight, similar to the unsuccessful 1980s-era [[Northrop F-20 Tigershark]]. The market for light fixed-wing attack jets had declined in the 1980s as richer countries opted for more capable aircraft and poorer countries pursued turboprops and [[attack helicopter]]s. It is uncertain if the Scorpion will be cheaper or outperform turboprops or remotely piloted aircraft (RPA) in terms of range, endurance, low-altitude performance, and sensors.<ref>{{Citation |url= http://www.aviationweek.com/Blogs.aspx?plckBlogId=Blog:27ec4a53-dcc8-42d0-bd3a-01329aef79a7&plckPostId=Blog%3a27ec4a53-dcc8-42d0-bd3a-01329aef79a7Post%3acd3eec88-15ea-4e56-8e5c-215e04d4edad |title= Textron's Scorpion: Bold Move, But Is There a Market? |newspaper= Aviation week |date= 7 October 2013}}.</ref>

The [[U.S. Air Force]] has made plans to retire the A-10 Thunderbolt II, with its [[close air support]] mission to be initially covered by F-16s and F-15Es until it can transition to the F-35A. An inexpensive replacement aircraft may be considered to perform CAS against enemies without sophisticated air defenses.<ref>{{Citation |url= http://www.flightglobal.com/news/articles/one-week-study-re-affirms-a-10-retirement-decision-409888/ |title= One-week study re-affirms A-10 retirement decision: USAF |newspaper= Flight global |date= 6 March 2015}}.</ref> Analysts believe that the Scorpion will be difficult to sell to the Air Force; Textron AirLand believes it can sell without a requirement or lengthy competition. Budget cuts make new programs unattractive, and its missions of irregular warfare, border patrol, maritime surveillance, emergency relief, counter narcotics, and air defense operations are performed by RPAs.<ref name="analysis" /> However, the Air Force pursued fully developed aircraft, excluding the Scorpion that lacked data on the cost of sustainment.<ref>{{Citation |url= https://www.flightglobal.com/news/articles/usaf-seeks-two-new-close-air-support-aircraft-427769/ |title= USAF seeks two new close-air support aircraft |newspaper= Flight global |date= 24 July 2016}}.</ref>

The Air National Guard has been under pressure by active Air Force officials to replace aging and costly F-16s and A-10s, and promoted unmanned aircraft. Air National Guard leaders feel losing manned aircraft to remotely piloted types would leave them ill-equipped for domestic emergencies, such as natural disasters and homeland security crises. While potentially politically motivated, some state governments have voiced apprehension of drones, fearing regulatory restrictions that could cripple a drone's ability to respond during disasters.<ref>{{Cite web |url=http://www.nationaldefensemagazine.org/blog/Lists/Posts/Post.aspx?ID=1287 |title=As Military Fighter Fleet Shrinks, National Guard Eyes Commercial Alternatives |last =Erwin |first =Sandra I. |publisher= National Defense |date=30 September 2013}}</ref>

Following the first flight, discussions were scheduled with an unnamed foreign customer.<ref name="firstflight" />  U.S. military components and at least one more foreign country are also interested in discussions.<ref name="firstflight"/> The company stated that interest from military and paramilitary organizations had been positive and that they intended to sell the aircraft for under [[US$]]20M each.<ref name= "Niles14Dec13">{{cite news |url= http://www.avweb.com/avwebflash/news/Scorpion-Drawing-Interest221118-1.html |title= Scorpion Drawing Interest |accessdate= 16 December 2013 |last= Niles |first= Russ |date= 14 December 2013 |work= AVweb}}</ref> Preliminary discussion were held with the militaries of [[Malaysia]], [[Brunei]], the [[Philippines]], [[Indonesia]], [[Bahrain]], [[Qatar]], and [[Saudi Arabia]]. In November 2014, sources confirmed that the [[United Arab Emirates]] had held discussions on using the Scorpion for the [[Al Fursan]] aerobatic squadron; Textron believes this could lead to an expanded military role. However, the UAE is reluctant to be the launch customer for a new aircraft and wants another customer to be found first before officially signing on. A deal may be secured by 2016.<ref>{{Citation |url= http://www.defensenews.com/article/20141102/DEFREG04/311020007/UAE-Negotiating-Possible-Scorpion-Purchase |title= UAE Negotiating Possible Scorpion Purchase |newspaper= Defense news |date= 2 November 2014}}.</ref>

In November 2014, the [[Nigerian Air Force]] expressed interest in up to a squadron’s worth of Scorpions to counter the [[Boko Haram]] insurgency.  The Scorpion would combine surveillance and effective strike capabilities in one airframe. Nigeria operates the unarmed [[ATR 42]] to detect targets, which are then relayed to a [[Chengdu F-7]]Ni, which are armed but lack precision guided weapons. Given a previous rejection for attack helicopters, approval for Nigeria may not be guaranteed.<ref>{{Citation |url= http://www.flightglobal.com/news/articles/nigerian-air-force-eyes-scorpion-combat-surveillance-406203/ |title= Nigerian air force eyes Scorpion combat surveillance aircraft |newspaper= Flight global |date= 18 November 2014}}.</ref>

On 27 April 2015, the Scorpion made a series of display flights for the [[Colombian Air Force]] at the [[Captain Luis F. Gómez Niño Air Base |Apiay Air Base]].<ref>{{cite web |language= es |trans-title= Textron Air land demonstrates its Scorpion fighter airplane in Colombia |url= http://www.webinfomil.com/2015/04/textron-airland-scorpion-demostracion-colombia.html |date= Apr 2015 |title= Textron AirLand demuestra su avión de combate Scorpion en Colombia |work= WebInfomil |accessdate= 2 May 2015}}</ref>  Colombia is currently looking to replace their fleet of [[Cessna A-37 Dragonfly]] with similar aircraft.<ref>{{cite web |language= es |trans-title= The imminent retirement of the Colombian A-37 reactivates Central American interest in these veteran airplanes |url= http://defensa.com/index.php?option=com_content&view=article&id=13358:la-inminente-baja-de-los-a-37-colombianos-reactiva-el-interes-en-centroamerica-por-estos-veteranos-aviones&catid=55:latinoamerica&Itemid=163 |title=La inminente baja de los A-37 colombianos reactiva el interés en Centroamérica por estos veteranos aviones| first =Alvaro |last= Diaz|work= Defensa|accessdate= 2 May 2015}}</ref>

[[U.S. Secretary of Defense]] [[Ashton Carter]] was expected to offer the Scorpion to the [[Indian Air Force]] during his visit to the country in June 2015.  Although designed for reconnaissance and light strike, India has expressed interest in using it as an intermediate jet trainer due to repeated delays to the HAL [[HJT-36 Sitara]] jet trainer aircraft.<ref>{{Citation |url= http://thediplomat.com/2015/05/us-to-offer-india-new-tactical-aircraft/ |title= US To Offer India New Tactical Aircraft |newspaper= The diplomat |date= 5 May 2015}}.</ref>

On 12 July 2016, QinetiQ, Thales and Textron AirLand announced a collaboration to bid for the UK Ministry of Defence's upcoming Air Support to Defence Operational Training (ASDOT) program.  The three companies’ CEOs met at Farnborough International Airshow to announce the signing of their Memorandum of Understanding (MOU) setting the foundation for the bid.  The operational training activities that will comprise the ASDOT program are being fulfilled by a number of providers, both military and civilian. This team plans to propose an innovative, cost effective, technologically advanced, and reliable managed service using the Textron AirLand Scorpion equipped with Thales and QinetiQ sensors to provide a broad spectrum of training for all three armed services. The competitive contract, expected to be awarded in September 2018 with a service delivery start in Jan 2020, is anticipated to be worth up to £1.2 billion over 15 years.<ref>{{cite web|url=http://www.janes.com/article/62203/farnborough-2016-qinetiq-and-thales-select-textron-airland-scorpion-jet-for-asdot-bid|title=Farnborough 2016: QinetiQ and Thales select Textron AirLand Scorpion jet for ASDOT bid |work= IHS Jane's 360| publisher= Jane’s |accessdate=17 July 2016}}</ref>

==Variants==
===Trainer===
On 23 August 2014, Textron AirLand confirmed that the Scorpion would be entered in the U.S. Air Force's [[T-X program|T-X trainer program]] competition. Only small modifications would be made, including shortening the wings to less than {{convert|47|ft|m|1|abbr=on}} and making them more aerodynamic, as well as increasing the engine's thrust at the expense of fuel efficiency for greater maneuverability; the twin-engine, twin-tail design would be retained. The trainer variant could also help secure international orders. The Scorpion's per hour flight cost is relatively close to the $2,200 per hour cost of the [[Beechcraft T-6 Texan II|T-6 Texan]] propeller trainer and international markets have a history of using one aircraft type to perform both training and light attack missions.<ref>{{Citation |url= http://www.defensenews.com/article/20140826/TSJ/308260019/Textron-AirLand-Developing-Scorpion-Trainer-Variant |title= Textron AirLand Developing Scorpion Trainer Variant |newspaper= Defense news |date= 26 August 2014}}.</ref> However, in September 2015 the company revealed that they would not offer a modification of the Scorpion for the T-X, given the change in Air Force requirements favoring a high-performance aircraft.<ref>{{Citation |url= http://www.airforcetimes.com/story/defense/air-space/support/2015/09/21/textron-scorpion-tx-new-design/72573396/ |title= Textron AirLand Considers Clean-Sheet T-X Offering |newspaper= Air force times |date= 21 September 2015}}.</ref>

==Specifications==
{{Aircraft specs
|ref=Manufacturer<!-- for giving the reference for the data --><ref>{{cite web|url=http://www.scorpionjet.com/aircraft-features/ |title= Aircraft Features|work= Scorpion jet |publisher= Textron AirLand |accessdate=8 December 2014}}</ref><ref>{{Citation |publisher= Textron |year= Sep 2013 |url= http://www.scorpionjet.com/wp-content/uploads/2013/09/Scorpion-Facts-Sheet-WEB.pdf |format= [[Portable document format |PDF]] |title= Scorpion Fact Sheet |accessdate= 19 September 2013}}.</ref>
|prime units?=kts
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft=43
|length in=6
|length note=
|span m=
|span ft=47
|span in=4
|span note=
|height m=
|height ft=14
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=11800
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=21250
|max takeoff weight note=
|fuel capacity= 6 000 lb (2 722 kg)
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Garrett TFE731|Honeywell TFE731]]
|eng1 type=turbofan
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->4000
|eng1 note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=450
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=95
|stall speed note=(max)<ref>{{Citation |newspaper= Insight |type= [[World Wide Web]] log |url= http://aerosociety.com/News/Insight-Blog/2358/QA-with-the-Scorpion-King |title= Q&A with the Scorpion King |publisher= [[Royal Aeronautical Society]] |date= 7 August 2014 |accessdate= 5 September 2014}}.</ref>
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=2400
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=45000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=*
<!--
        Armament
-->
|guns= 
|bombs= precision and non-precision munitions
|rockets= 
|missiles=

|hardpoints=6
|hardpoint capacity= {{convert|6200|lb|kg|abbr=on}},<ref name= ext_stores /> and an internal bay with a {{convert |3000|lb|kg|abbr=on}}<ref name=dims/> of armaments and other stores
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
|similar aircraft=
* [[Alenia Aermacchi M-346 Master]]
* [[BAE Systems Hawk]]
* [[KAI T-50 Golden Eagle]]
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}

{{refbegin}}
<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->

==External links==
{{commons category|Textron AirLand Scorpion}}
* [http://www.scorpionjet.com/ Official Textron AirLand Scorpion page]
* [https://www.youtube.com/watch?v=wXRajxHb530 Official video of first taxi tests]

{{Textron aircraft}}
{{Cessna}}

[[Category:Textron aircraft|Scorpion]]
[[Category:United States attack aircraft 2010–2019]]
[[Category:Twinjets]]
[[Category:High-wing aircraft]]