{{Infobox journal
| title = Biotechnology Journal
| cover = 
| editor = Alois Jungbauer, Sang Yup Lee
| discipline = [[Biotechnology]]
| formernames =
| abbreviation = Biotechnol. J.
| publisher = [[Wiley-Blackwell]]
| country =
| frequency = Monthly
| history = 2006-present
| openaccess =
| license =
| impact = 3.446
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1860-7314
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1860-7314/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1860-7314/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 62770111
| LCCN = 2006205072
| CODEN = BJIOAM
| ISSN = 1860-7314
| eISSN = 1860-6768
}}
The '''''Biotechnology Journal''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[biotechnology]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Elsevier BIOBASE]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Biotechnology & Bioengineering Abstracts]]
* [[CAB Abstracts]]
* [[CAB HEALTH]]
* [[CABDirect]]
* [[Chemical Abstracts Service]]
* [[ChemInform]]
* [[CSA Biological Sciences Database]]
* [[CSA Environmental Sciences & Pollution Management Database]]
* [[EMBASE]]
* [[Global Health]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 3.446.<ref name=WoS>{{cite book |year=2013 |chapter=Biotechnology Journal |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1860-7314}}

[[Category:Wiley-Blackwell academic journals]]
[[Category:Biotechnology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2006]]
[[Category:Monthly journals]]