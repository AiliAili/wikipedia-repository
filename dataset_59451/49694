{{other uses}}
{{Refimprove|date=January 2014}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] and/or [[Wikipedia:WikiProject Books]] -->
| name          = The Island of Doctor Moreau
| image         = IslandOfDrMoreau.JPG
| caption       = First edition cover of ''The Island of Doctor Moreau''.
| author        = [[H. G. Wells]]
| illustrator   =
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        =
| genre         = Science fiction
| published     = 1896 ([[Heinemann (book publisher)|Heinemann]], Stone & Kimball<ref>{{cite web|url=http://www.s4ulanguages.com/hgwells.html|title=HGWells|publisher=}}</ref>)
| preceded_by   = [[The Wonderful Visit]]
| followed_by   = [[The Wheels of Chance]]
}}
'''''The Island of Doctor Moreau''''' is an 1896 [[science fiction]] novel, by English author, [[H. G. Wells]].

The text of the novel is the narration of Edward Prendick, a shipwrecked man rescued by a passing boat who is left on the island home of Doctor Moreau, who creates [[Human-animal hybrid|human-like hybrid beings]] from animals via [[vivisection]]. The novel deals with a number of philosophical themes, including [[pain]] and cruelty, moral responsibility, human identity, and human interference with nature.<ref>{{cite web|url=http://www.barnesandnoble.com/w/the-island-of-doctor-moreau-h-g-wells/1120353655?ean=9781499744446|title=The Island of Doctor Moreau: Original and Unabridged|author=Barnes & Noble|work=Barnes & Noble}}</ref> Wells described the novel as "an exercise in youthful blasphemy".<ref>Wells's description of ''The Island of Dr. Moreau'' as ''youthful blasphemy'' comes from his introduction to ''The Scientific Romances of H. G. Wells'' (1933; published in the United States as ''Seven Famous Novels by H. G. Wells'', 1934). This ''Preface to the Scientific Romances'' is reprinted as a chapter of editors Patrick Parrinder and Robert M. Philmus's ''H. G. Wells's Literary Criticism'' (Sussex: The Harvester Press Limited, and New Jersey: Barnes & Noble Books, 1980), see p. 243 for the line quoted.</ref>

''The Island of Doctor Moreau'' is a classic of early science fiction<ref>See Mason Harris's introduction and notes for the 2009 Broadview Books edition of ''The Island of Dr. Moreau''</ref> and remains one of Wells's best-known books. It has been adapted to film and other media on many occasions.

==Plot==
''The Island of Doctor Moreau'' is the account of Edward Prendick, an Englishman with a scientific education who survives a shipwreck in the southern [[Pacific Ocean]]. A passing ship takes him aboard, and a man named Montgomery revives him. Prendick also meets a grotesque bestial native named M'ling, who appears to be Montgomery's manservant. The ship is transporting a number of animals which belong to Montgomery. As they approach the island, Montgomery's destination, the captain demands Prendick leave the ship with Montgomery. Montgomery explains that he will not be able to host Prendick on the island. Despite this, the captain leaves Prendick in a [[dinghy]] and sails away. Seeing that the captain has abandoned Prendick, Montgomery takes pity and rescues him. As ships rarely pass the island, Prendick will be housed in an outer room of an enclosed compound.

The island belongs to Dr. Moreau. Prendick remembers that he has heard of Moreau, formerly an eminent physiologist in London whose gruesome experiments in [[vivisection]] had been publicly exposed and has fled England as a result of his exposure.

The next day, Moreau begins working on a [[Cougar|puma]]. Prendick gathers that Moreau is performing a painful experiment on the animal, and its anguished cries drive Prendick out into the jungle. While he wanders, he comes upon a group of people who seem human but have an unmistakable resemblance to [[Pig|swine]]. As he walks back to the enclosure, he suddenly realises he is being followed by a figure in the jungle. He panics and flees, and the figure gives chase. As his pursuer bears down on him, Prendick manages to stun him with a stone and observes the pursuer is a monstrous hybrid of animal and man. When Prendrick returns to the enclosure and questions Montgomery, Montgomery refuses to be open with him. After failing to get an explanation, Prendick finally gives in and takes a [[Hypnotic|sleeping draught]].

Prendick awakes the next morning with the previous night's activities fresh in his mind. Seeing that the door to Moreau's operating room has been left unlocked, he walks in to find a humanoid form lying in bandages on the table before he is ejected by a shocked and angry Moreau. He believes that Moreau has been vivisecting humans and that he is the next test subject. He flees into the jungle where he meets an [[Ape]]-Man who takes him to a colony of similarly half-human/half-animal creatures. Their leader is a large grey thing named the Sayer of the Law who has him recite a strange litany called the Law that involves prohibitions against bestial behavior and praise for Moreau.

Suddenly, Dr. Moreau bursts into the colony looking for Prendick, but Prendick escapes to the jungle. He makes for the ocean, where he plans to drown himself rather than allow Moreau to experiment on him. Moreau explains that the creatures called the Beast Folk were not formerly men, but rather animals. Prendick returns to the enclosure, where Moreau explains that he has been on the island for eleven years and has been striving to make a complete transformation of an animal to a human. He explains that while he is getting closer to perfection, his subjects have a habit of reverting to their animal form and behaviour. Moreau regards the pain he inflicts as insignificant and an unavoidable side effect in the name of his scientific experiments.

One day, Prendick and Montgomery encounter a half-eaten rabbit. Since eating flesh and tasting blood are strong prohibitions, Dr. Moreau calls an assembly of the Beast Folk and identifies the [[Leopard]]-Man (the same one that chased Prendick the first time he wandered into the jungle) as the transgressor. Knowing that he will be sent back to Dr. Moreau's compound for more painful sessions of vivisection, the Leopard-Man flees. Eventually, the group corners him in some undergrowth, but Prendick takes pity and shoots him to spare him from further suffering. Prendick also believes that although the Leopard-Man was seen breaking several laws, such as drinking water bent down like an animal, chasing men (Prendick), and running on all fours, the Leopard-Man was not solely responsible for the deaths of the rabbits. It was also the [[Hyena]]-[[Pig|Swine]], the next most dangerous Beast Man on the island. Dr. Moreau is furious that Prendick killed the Leopard-Man but can do nothing about the situation.

As time passes, Prendick becomes inured to the grotesqueness of the Beast Folk. However one day, the half-finished puma woman rips free of its restraints and escapes from the lab. Dr. Moreau pursues it, but the two end up fighting each other which ends in a mutual kill. Montgomery breaks down and decides to share his alcohol with the Beast Folk. Prendick resolves to leave the island, but later hears a commotion outside in which Montgomery, his servant M'ling, and the Sayer of the law die after a scuffle with the Beast Folk. At the same time, the compound burns down because Prendick has knocked over a lamp. With no chance of saving any of the provisions stored in the enclosure, Prendick realizes that during the night Montgomery has also destroyed the only boats on the island.

Prendick lives with the Beast Folk on the island for months after the deaths of Moreau and Montgomery. As the time goes by, the Beast Folk increasingly revert to their original animal instincts, beginning to hunt the island's rabbits, returning to walking on all fours, and leaving their shared living areas for the wild. They cease to follow Prendick's instructions and eventually the Hyena-Swine kills his faithful companion, a Dog-Man created from a [[St. Bernard (dog)|St. Bernard]]. Prendick then shoots the Hyena-Swine in self-defence with the help of the Sloth Creature. Luckily for Prendick ever since his efforts to build a raft have been unsuccessful, a boat that carries two corpses drifts onto the beach (perhaps the captain of the ship that picked Prendick up and a sailor).<ref>[http://abbott-1001books.blogspot.com/2011/12/463-island-of-dr-moreau-hg-wells.html abbott (2011) ''463. The Island of Dr. Moreau – H.G. Wells '' Retrieved on February 11, 2015]</ref> Prendick uses the boat to leave the island and is picked up three days later. But when he tells his story he is thought to be mad, so he feigns amnesia.

Back in England, Prendick is no longer comfortable in the presence of humans who seem to him to be about to revert to the animal state. He leaves London and lives in near-solitude in the countryside, devoting himself to chemistry as well as astronomy in the studies of which he finds some peace.

==Main characters==
* '''Edward Prendick''' – The narrator and protagonist.
* '''Dr. Moreau''' – A vivisectionist who has fled upon his experiments being exposed and has moved to a remote island in the Pacific to pursue his research of perfecting his Beast Folk.
* '''Montgomery''' – Dr. Moreau's assistant and Prendick's rescuer. A medical doctor who enjoyed a measure of happiness in England. An alcoholic who feels some sympathy for the Beast Folk.
* '''Beast Folk''' – Animals which Moreau has experimented upon, giving them human traits via vivisection for which the surgery is extremely painful. They include:
** '''M'ling''' – Montgomery's servant who does the cooking and cleaning. Moreau combined a [[bear]], a [[dog]], and an [[ox]] to create him. As Prendick describes M'ling, he states that M'ling is a "complex trophy of Moreau's skill, a bear, tainted with dog and ox, and one of the most elaborately made of all the creatures." He also sports glow-in-the-dark eyes and furry ears. M'ling later dies protecting Montgomery from the other Beast Folk on the beach.
** '''Sayer of the Law''' – A large, grey animal of unspecified combinations that recites Dr. Moreau's teachings about being men to the other Beast Folk. The Sayer of the Law serves as a governor and a priest to the Beast Folk. He is later killed in an unseen scuffle between Montgomery, M'ling, and the Beast Folk.
** '''[[Hyena]]-[[Pig|Swine]]''' – A [[carnivore|carnivorous]] hybrid of hyena and pig who becomes Prendick's enemy in the wake of Dr. Moreau's death. He is later killed by Prendick in self-defence.
** '''[[Leopard]]-Man''' – A leopard-based rebel who breaks the Law by running on all fours, drinking from the stream, and chasing Prendick. The Leopard-Man is later killed by Prendick much to the dismay of Dr. Moreau.
** '''[[Dog]]-Man''' – A Beast Man created from a [[St. Bernard (dog)|St. Bernard]] who, near the end of the book, becomes Prendick's faithful companion. He is so like a domestic dog in character that Prendick is barely surprised when he reverts to a more animalistic form. Dog-Man is later killed by the Hyena-Swine.
** '''[[Satyr]]-Man''' – A goat creature. Prendrick describes him as unsettling and "[[Satan]]ic" in form.
** '''[[Fox]]-[[Bear]] [[Witchcraft|Witch]]''' – A female hybrid of a fox and a bear who passionately supports the Law. Prendick quickly takes a dislike to her.
** '''[[Sloth]] Creature''' – A small, pink sloth-based creation described by Prendick as resembling a flayed child. He helped Prendick kill the Hyena-Swine before fully regressing.
** '''Ape-Man''' – A [[monkey]] or [[ape]] creature that considers himself equal to Prendick and refers to himself and Prendick as "Five Men", because they both have five fingers on each hand, which is uncommon among the Beast Folk. He is the first Beast Man other than M'ling whom Prendick speaks to. He has what he refers to as "Big Thinks" which on his return to England, Prendick likens to a priest's sermon at the pulpit.
** '''Half-Finished [[Cougar|Puma]]-Woman''' – The last beast-person created by Moreau. She is halfway through her process of being turned into one of the Beast Folk, but is in so much pain from the surgery that she uses her strength to break free of his restraints and escapes from the House of Pain. Moreau then chases after her with a [[revolver]]. He and the creature fight each other which ends in a mutual kill.
** '''Mare-Rhinoceros Man''' – A hybrid between a [[horse]] and a [[Javan rhinoceros]] who appeared during Prendrick's introduction to the Beast Folk.
** '''[[Ox]]-[[Wild boar|Boar]]''' – A hybrid of an ox and a boar who appears after Montgomery's death.
** '''[[Pig|Swine]]-Men and Swine-Woman''' – A group of pig-based Beast Folk who appear during Prendrick's introduction to the Beast Folk.
** '''[[Gray wolf|Wolf]]-[[Grizzly bear|Bear]]''' – A hybrid of a gray wolf and a grizzly bear who is briefly mentioned during the hunt for the Leopard-Man.

==Historical context==
At the time of the novel's publication in 1896, there was growing discussion in Europe regarding [[Degeneration theory|degeneration]] and animal [[vivisection]]. Several [[interest group]]s were formed to oppose vivisection, the two largest being the [[National Anti-Vivisection Society]] in 1875 and the [[British Union for the Abolition of Vivisection]] in 1898.<ref>{{cite web|url=http://www.politics.co.uk/opinion-formers/buav-british-union-for-the-abolition-of-vivisection|title=Welcome|work=politics.co.uk}}</ref> ''The Island of Dr. Moreau'' reflects these themes, along with ideas of [[Darwinian evolution]] which were gaining popularity and controversy in the late 1800s.

==Adaptations==
The novel has been adapted into films and other works, on multiple occasions:

* ''Ile d'Epouvante'' (1913, ''The Island of Terror''), a French silent film<ref>{{cite web|url=http://www.tcm.com/this-month/article/139119%7C139123/Island-of-Lost-Souls.html|title=Island of Lost Souls|work=Turner Classic Movies}}</ref> (also spelled ''L'Ile d'Epouvante'' and ''Isle d'epouvante''). The 23-minute two-reeler film was directed by Joe Hamman in 1911 and then released in 1913. By late 1913, the film had been picked up by US distributor George Kleine and renamed ''The Island of Terror'' for its release in Chicago.<ref>{{cite web|url=http://monsterkidclassichorrorforum.yuku.com/topic/34093|title=L'ile d'Epouvante (1913)  in Silent Horror Forum|work=Yuku}}</ref>
*''Die Insel der Veschollenen'' (1921), German silent adaption
* [[Island of Lost Souls (1932 film)|''Island of Lost Souls'']] (1932), with [[Charles Laughton]] and [[Bela Lugosi]].
* ''[[Terror Is a Man]]'' (1959), with [[Francis Lederer]], Greta Thyssen, and [[Richard Derr]]. This Filipino film, directed by Gerardo de Leon, was reissued in the United States as ''Blood Creature'' (1964). Leon partnered with Eddie Romero to direct and release two follow-up films in 1968: ''Brides of Blood'' and ''Mad Doctor of Blood Island''. All three were produced by Lynn-Romero Productions.
* At the age of 13, [[Tim Burton]] made an amateur adaptation of Wells' novel, ''[[The Island of Doctor Agor]]'' (1971).<ref>
{{cite web|url=http://www.imdb.com/title/tt0309721|website=imdb.com|title=The Island of Doctor Agor}}</ref>
* ''[[The Twilight People]]'' (1972), starring [[John Ashley (actor)|John Ashley]] and with an early role for [[Pam Grier]], was Eddie Romero's version of the original story.
* Joseph Silva turned [[The Island of Dr. Moreau (1977 film)|''The Island of Dr. Moreau'']] (1977), with [[Burt Lancaster]] and [[Michael York (actor)|Michael York]],  [[novelization|into a derivative]] published by Ace.
*In [[The Island of Dr. Moreau (1996 film)|''The Island of Dr. Moreau'']] (1996), with [[Marlon Brando]], [[Val Kilmer]], [[David Thewlis]], [[Fairuza Balk]], and [[Ron Perlman]], Dr. Moreau introduced human [[DNA]] into the animals in his possession in order to make them more human.
* Seattle, Washington's [[Taproot Theatre Company]] performed Sean Gaffney's theatrical adaptation of the novel in 1999. The performance was filmed by Globalstage Productions and is available on video.{{citation needed|date=July 2013}}
* ''[[The Simpsons]]'' annual Halloween special adapted the novel as a segment in their "[[Treehouse of Horror XIII]]" episode called '''The Island of Dr. Hibbert''', in which the doctor invites unsuspecting Springfield residents to his island resort, and turns them into human-animal hybrids.
* The film ''[[Dr. Moreau's House of Pain]]'' (2004), made by cult horror studio [[Full Moon Pictures]], is billed as a sequel to the novel.<ref>{{cite web |url=http://www.fullmoondirect.com/DR-MOREAUS-HOUSE-OF-PAIN-DVD_p_47.html |title=DR. MOREAU'S HOUSE OF PAIN (DVD) |publisher=FullMoonDirect.com }}</ref>
* In the third season of science fiction thriller TV series, ''[[Orphan Black]]'', the book plays an important role containing Professor Duncan's key to human cloning.
* In the game "Heathen" from indie french studio Frog Factory, where they use the original story as the plot of the game.
* "The Madman's Daughter" tells the story from the point of view of Juliet Moreau, Dr. Moreau's deer-hybrid daughter.

==Inspirations and popular culture==
The story, as well as the Litany of the Law, have inspired multiple derivative works and popular culture references.

===In literature===
* ''Moreau's Other Island'' (1980), by [[Brian Aldiss]], is an updating of the original to a near-future setting. US Under-Secretary of State Calvert Madle Roberts is cast ashore on the eponymous island where he discovers the cyborgised [[Thalidomide]] victim Mortimer Dart carrying on Moreau's work. It transpires that Dart's work is intended to produce a ‘replacement’ race that can survive a post-nuclear environment, and that Roberts approved Dart's funding.

* ''The Madman's Daughter'' trilogy, written by Megan Shepherd, tells the story of Dr. Moreau's daughter Juliet. However, each book is based on a different classic novel: the first book is based on this novel by [[H.G. Wells]], the second one on [[Robert Louis Stevenson]]'s ''[[The Strange Case of Dr Jekyll and Mr Hyde]]'' (1886), and the final book is based on [[Mary Shelley]]'s ''[[Frankenstein]]'' (1818).<ref>{{cite web|url=http://shelf-life.ew.com/2013/01/29/the-madmans-daughter-megan-shepherd-interview/|title='The Madman's Daughter' author Megan Shepherd on her 'Lost' inspiration and plans for a movie -- EXCLUSIVE
}}</ref>

* In chapter 61 of ''[[The Fallen (Higson novel)|The Fallen]]'' (2013), book five of [[Charlie Higson]]'s [[post-apocalyptic]] horror series, [[The Enemy (Higson novel)|The Enemy]], the expedition party from the museum encounters a strange set of malformed children at the biomedical company Promithios, who recite the Litany of the Law.<ref>{{cite book|author=Higson, Charlie| title=The Fallen|date=2013|publisher=Hyperion|location=US}}</ref>

* ''The Isles of Dr. Moreau'' (2015), by [[Heather O'Neill]] in her short story collection ''Daydreams of Angels'' tells of a grandfather who, when he was young, meets an eccentric, albeit humane named Dr. Moreau on "the Isle of Noble and Important and Respectable Betterment of Homo sapiens and Their Consorts." Moreau's experiments involve combining animal DNA with human DNA and the story unfolds as the grandfather meets (and dates) several of these humanoid creatures.<ref>{{cite web|url=https://thewalrus.ca/the-isles-of-dr-moreau/|title=The Isles of Dr. Moreau|publisher=}}</ref>

===In comics===
* ''The Only Living Boy'' the young-adult graphic novel by [[David Gallaher]] and [[Steve Ellis comics]] is described by the author as a hybrid of ''[[The Jungle Book]]'' and ''The Island of Doctor Moreau''. The character, Doctor Once, is based on the Moreau.
* ''[[The League of Extraordinary Gentlemen]] volume 2,'' a graphic novel by [[Alan Moore]] and Kevin O'Neill, uses Dr. Alphonse Moreau as a secondary character who has been moved to England by the British Government to develop hybrids. He was given the first name of Alphonse by Alan Moore. Teddy Prendick is featured (although he is named 'Prendrick' in the comic and seems to have gone mad) and recounts his time on the island with Dr. Moreau. The storyline also ties in with aspects of Wells's other novels, ''[[The Invisible Man]]'', ''[[The War of the Worlds]]'' and ''[[The Time Machine]]''.
* ''[[Van Helsing: From Beneath the Rue Morgue]]'', a one-shot comic book by [[Joshua Dysart]] and [[Jason Shawn Alexander]] which is set after Gabriel Van Helsing kills Mr. Hyde and meets Dr. Moreau.
* ''Legenderry: A Steampunk Adventure'', a steampunk comic series published by Dynamite Entertainment. The Island of Doctor Moreau was included in issue 3 and three beast-men hybrids bordered The Victory (Captain Victory's ship) while it was docked there.

===In music===
*''[[Are We Not Men? We Are Devo!]]''
*''[[Are We Not Men? We Are Diva!]]'', titled in response to the DEVO album
*''[[Are We Not Horses]]''
*''No Spill Blood'', from Oingo Boingo's ''[[Good For Your Soul]]'' album
*''[[House of Pain]]'' is an American hip hop group whose name is a reference to the novel- a reference carried further by the naming of their 2011 tour "He Who Breaks the Law".  Dialogue samples from the 1977 movie are used as snippets within the album
*The Song ''Toes'' by the alternative band [[Glass Animals]] is based on the book's story<ref>{{cite web|url=https://www.youtube.com/watch?v=YUTaKn79O6A|title=Glass Animals Coachella Interview: Inspirations for New Record, "Black Mambo" & "Hazey"|first=|last=Billboard|date=12 April 2015|publisher=|via=YouTube}}</ref>

===In television===
* The story has been used as a plot device in the [[BBC America]] series ''[[Orphan Black]]''.
* The Island of Doctor Moreau was parodied in in ''[[The Simpsons]]'' episode "[[Treehouse of Horror XIII]]."
* The cartoon series ''[[Spliced (TV series)|Spliced]]'' is a lighthearted take on the concept.

===In gaming===
* ''[[Vivisector: Beast Within]]'' is a Ukrainian developed first-person shooter game released in the [[Commonwealth of Independent States|CIS]] in 2005 and later in the rest of Europe in 2006. The game is heavily inspired by the novel, originally developed as a [[Duke Nukem]] title.
* The video game ''[[Champions Online]]'' features Dr. Phillippe Moreau, the grandson of Dr. Henry Moreau and member of VIPER (short for Venomous Imperial Party of the Eternal Reptile). Phillippe used his technology to perfect his father's work where he created the Manimals.
* Doctor Merlot, the main villain of the 2016 game ''[[RWBY#Video game|RWBY: Grimm Eclipse]]'', is heavily inspired by Doctor Moreau.

==Scientific plausibility==
In the short essay "[[The Limits of Individual Plasticity]]" (1895), H.G. Wells expounded upon his firm belief that the events depicted in ''The Island of Doctor Moreau'' are entirely possible should such vivisective experiments ever be tested outside the confines of [[science fiction]]. However, modern medicine has shown that animals lack the necessary brain structure to emulate human faculties like speech.<ref>citation needed</ref>

==References==
{{reflist|30em}}

==Further reading==
* Canadas, Ivan. "Going Wilde: Prendick, Montgomery and Late-Victorian Homosexuality in ''The Island of Doctor Moreau''." ''JELL: Journal of the English Language and Literature Association of Korea'', 56.3 (June 2010): 461–485.
* Hoad, Neville. “Cosmetic Surgeons of the Social: Darwin, Freud, and Wells and the Limits of Sympathy on ''The Island of Dr. Moreau''”, in: ''Compassion: The Culture and Politics of an Emotion'', Ed. Lauren Berlant. London & New York: Routledge, 2004. 187–217.
* Reed, John R., “The Vanity of Law in ''The Island of Doctor Moreau''”, in: ''H. G. Wells under Revision: Proceedings of the International H. G. Wells Symposium: London, July 1986'', Ed. Patrick Parrinder & Christopher Rolfe. Selinsgrove: Susquehanna UP / London and Toronto: Associated UPs, 1990. 134-44.
* Wells, H. G. ''The Island of Dr. Moreau'', Ed. Steven Palmé. Dover Thrift Editions. New York: Dover Publications, 1996.
* Wells, H. G. ''The Island of Doctor Moreau: A Critical Text of the 1896 London First Edition, with Introduction and Appendices'', Ed. Leon Stover. The Annotated H.G. Wells, 2. Jefferson, N.C., and London: McFarland, 1996.

==External links==
{{wikisource|The Island of Doctor Moreau}}
{{commons category}}
* {{gutenberg|no=159|name=The Island of Doctor Moreau}}
* [https://archive.org/search.php?query=The%20Island%20of%20Doctor%20Moreau%20AND%20mediatype%3Atexts ''The Island of Doctor Moreau''] at [[Internet Archive]] (scanned books original editions)
* {{Librivox book | stitle=The Island of Moreau | dtitle=The Island of Doctor Moreau | author=H. G. Wells}}
* [http://www.everythingisundercontrol.org/nagtloper/write/moreauscript.html A draft of the 1996 films screenplay, dated 26 April 1994]
* {{IMDb title|id=0024188|title=The Island of Lost Souls (1932)}}
* {{IMDb title|id=0076210|title=The Island of Dr. Moreau (1977)}}
* {{IMDb title|id=0116654|title=The Island of Dr. Moreau (1996)}}
* {{cite journal
| first =Daniele
| last =Jörg
| authorlink =
| coauthors =
| year =2003
| month =
| title =The Good, the Bad and the Ugly—Dr. Moreau Goes to Hollywood
| journal =Public Understanding of Science
| volume =12
| issue =3
| pages =297–305
| id =
| url = http://pus.sagepub.com/cgi/content/abstract/12/3/297
| doi =10.1177/0963662503123008
}} Compares the three adaptations of the novel, focuses on the scientists and the science in the film, considering the year of the production and what was known about genes and cells at the time.
* [http://www.litreact.com/reactions/island%20of%20dr%20moreau_wells_singson.html Analysis of ''The Island of Dr. Moreau'' on Lit React]

{{H. G. Wells}}
{{The Island of Dr. Moreau}}

{{DEFAULTSORT:Island Of Doctor Moreau, The}}
[[Category:The Island of Doctor Moreau| ]]
[[Category:1896 novels]]
[[Category:British science fiction novels]]
[[Category:1890s science fiction novels]]
[[Category:Novels by H. G. Wells]]
[[Category:Fictional islands|Doctor Moreau]]
[[Category:Literature featuring anthropomorphic characters]]
[[Category:Castaways in fiction]]
[[Category:Novels set on islands]]
[[Category:Heinemann (publisher) books]]
[[Category:Novels adapted into comics]]
[[Category:British novels adapted into films]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into plays]]
[[Category:Human and non-human experimentation in fiction]]