{{Use British English|date=April 2015}}
{{Use dmy dates|date=April 2015}}
{{Infobox military award
| name             = The Africa Star
| image            = [[File:WW2 Africa Star.jpg|x300px]]
| caption          = Awarded to a South African, 314134 J.A. Jooste
| awarded_by       = [[George VI|the Monarch of the United Kingdom and the Dominions of the British Commonwealth, and Emperor of India]]
| country          = [[File:Flag of the United Kingdom.svg|x14px]] [[United Kingdom]]
| type             = Military campaign medal
| eligibility      = All Ranks
| for              = Entry into operational service
| campaign         = North Africa 1940–1943
| status           =
| description      =
| motto            =
| clasps           = NORTH AFRICA 1942–43<br>8th ARMY<br>1st ARMY
| post-nominals    =
| established      = 8 July 1943
| first_award      = 1943
| last_award       =
| total            =
| posthumous       =
| recipients       =
| precedence_label = Order of wear
| individual       =
| higher           = [[Air Crew Europe Star]]
| same             =
| lower            = [[Pacific Star]]
| related          = 
| image2           = [[File:Ribbon - Africa Star.png|x29px]]<br>Ribbon bar<br/>[[File:Ribbon - Africa Star & Rosette.png|x29px]] [[File:Ribbon - Africa Star & 8.png|x29px]] [[File:Ribbon - Africa Star & 1.png|x29px]]
| caption2         = North Africa 1942–43, 8th Army and 1st Army insignia
}}
{{otheruses|Star of Africa (disambiguation)}}
The '''Africa Star''' is a military campaign medal, instituted by the [[United Kingdom]] on 8 July 1943 for award to subjects of the [[Commonwealth of Nations|British Commonwealth]] who served in the [[Second World War]], specifically in North Africa between 10 June 1940 and 12 May 1943 inclusive.<ref name="Stratford"/><ref name="Hansard2"/>

Three clasps were instituted to be worn on the medal ribbon, the '''North Africa 1942–43 Clasp''', the '''8th Army Clasp''' and the '''1st Army Clasp'''.<ref name="Stratford"/>

==The Second World War Stars==
Altogether eight campaign stars and nine clasps were initially instituted for campaign service during the Second World War. On 8 July 1943, the [[1939–1945 Star]] and the Africa Star were the first two of these Stars to be instituted. One more campaign star, the [[Arctic Star]], and one more clasp, the Bomber Command Clasp, were belatedly added on 26 February 2013, more than sixty-seven years after the end of the war.<ref name="Stratford">[http://www.stephen-stratford.co.uk/wwii_medals.htm Stephen Stratford Medals site - British Military & Criminal History - 1900 to 1999 - Atlantic Star] (Access date 1 April 2015)</ref><ref name="Hansard2">[http://hansard.millbanksystems.com/commons/1943/aug/03/war-service-decorations War Service (Decorations) - Statement in the House of Commons by Winston Churchill on 3 August 1943 (HC Deb 03 August 1943 vol 391 cc2091-3)] (Access date 9 April 2015)</ref><ref name="BomberCommand">[http://webarchive.nationalarchives.gov.uk/20140805133045/http://www.veterans-uk.info/arctic_star_index.htm The National Archives - Ministry of Defence - Arctic Star and Bomber Command Clasp] (Access date 1 April 2015)</ref>

Only one of these campaign stars, the 1939–1945 Star, covered the full duration of the Second World War from its outbreak on 3 September 1939 to the victory over Japan on 2 September 1945.<ref name="NewZealand1939-45"/>

No-one could be awarded more than five (now six) campaign stars and no-one could be awarded more than one clasp to any one campaign star. Five of the nine (now ten) clasps were the equivalents of their namesake campaign stars and were awarded for the same respective campaigns as those stars, to be worn on the ribbon of that campaign star of the applicable group that had been earned first. The maximum of six possible campaign stars are the following:<ref name="Stratford"/><ref name="BomberCommand"/><ref name="Regulations">{{cite web|last=Committee on the Grant of Honours, Decorations and Medals in Time of War|title=Campaign Stars and the Defence Medal (Regulations)|url=http://www.northeastmedals.co.uk/britishguide/hmso/campaign_stars_defence.htm|publisher=HM Stationery Office|accessdate=2010-08-01|location=London|date=May 1945}}</ref>
* The 1939–1945 Star with, if awarded, either the [[Battle of Britain]] Clasp or the Bomber Command Clasp.<ref name="NewZealand1939-45">[http://medals.nzdf.mil.nz/warrants/h9-reg.html New Zealand Defence Force - The 1939-45 Star Eligibility Rules] (Access date 12 April 2015)</ref>
* Only one of the [[Atlantic Star]], [[Air Crew Europe Star]] or [[France and Germany Star]] and, if awarded, the first to be earned respectively of the Air Crew Europe Clasp, France and Germany Clasp or Atlantic Clasp, to be worn on the ribbon of that one of these three campaign stars to have been first earned and awarded.<ref name="NewZealandAtlantic">[http://medals.nzdf.mil.nz/warrants/h10-reg.html New Zealand Defence Force - The Atlantic Star Eligibility Rules] (Access date 4 April 2015)</ref><ref name="NewZealandACE">[http://medals.nzdf.mil.nz/warrants/h11-reg.html New Zealand Defence Force - The Air Crew Europe Star Eligibility Rules] (Access date 12 April 2015)</ref><ref name="NewZealandF&G">[http://medals.nzdf.mil.nz/warrants/h16-reg.html New Zealand Defence Force - The France and Germany Star Eligibility Rules] (Access date 12 April 2015)</ref>
* The [[Arctic Star]].<ref name="BomberCommand"/><ref name="NewZealandArctic">[http://medals.nzdf.mil.nz/news/index.html#arctic New Zealand Defence Force - The Arctic Star] (Access date 12 April 2015)</ref>
* The '''Africa Star''' with, if awarded, the first to be earned of the North Africa 1942–43 Clasp, 8th Army Clasp or 1st Army Clasp.<ref name="NewZealandAfrica">[http://medals.nzdf.mil.nz/warrants/h12-reg.html New Zealand Defence Force - The Africa Star Eligibility Rules] (Access date 12 April 2015)</ref>
* Either the [[Pacific Star]] or [[Burma Star]] or, if awarded, either the Burma Clasp or Pacific Clasp respectively, to be worn on the ribbon of that one of these two campaign stars to have been first earned and awarded.<ref name="NewZealandBurma">[http://medals.nzdf.mil.nz/warrants/h14-reg.html New Zealand Defence Force - The Burma Star Eligibility Rules] (Access date 12 April 2015)</ref><ref name="NewZealandPacific">[http://medals.nzdf.mil.nz/warrants/h13-reg.html New Zealand Defence Force - The Pacific Star Eligibility Rules] (Access date 9 April 2015)</ref>
* The [[Italy Star]].<ref name="NewZealandItaly">[http://medals.nzdf.mil.nz/warrants/h15-reg.html New Zealand Defence Force - The Italy Star Eligibility Rules] (Access date 12 April 2015)</ref>

==Institution==
Between 10 June 1940 and 12 May 1943 British forces fought in North Africa against the Germans and Italians, who had taken control of large areas of Egypt, Libya and Tunisia and therefore also of the [[Suez Canal]] and the sea lanes approaching it. During the desert conflict the balance of power alternated between the two sides, until the British eventually secured victory on 12 May 1943 when the remaining German forces surrendered at [[Tunis]]. Some historians consider the British victory over the German forces in North Africa to have been the turning point in the war which led to the eventual defeat of Germany.<ref name="GovUK">[https://www.gov.uk/medals-campaigns-descriptions-and-eligibility#africa-star GOV.UK - Defence and armed forces – guidance - Medals: campaigns, descriptions and eligibility - Africa Star] (Access date 9 April 2015)</ref>

The institution of the Africa Star was announced on 8 July 1943 and in August it was announced that the first uniform ribbons would be issued to qualifying personnel later in that year. The medals themselves were not intended to be available until after the cessation of hostilities. Some ribbon issues to overseas troops were delayed, but many had been received by the end of 1943 and were worn by recipients throughout the remainder of the war.<ref name="Hansard2"/><ref name="Hansard1">[http://hansard.millbanksystems.com/commons/1943/jul/08/overseas-service-recognition Overseas Service (Recognition) - Statement in the House of Commons by Clement Attlee on 8 July 1943 (HC Deb 08 July 1943 vol 390 c2250)]</ref>

Three clasps were instituted, the North Africa 1942–43 Clasp, the 8th Army Clasp and the 1st Army Clasp, of which only the first to be earned may be worn on the ribbon of the Africa Star.<ref name="NewZealandAfrica"/><ref name="Birkenhead"/>

==Award criteria==
===Medal===
The Africa Star was awarded for a minimum of one day's service in an operational area of North Africa between 10 June 1940 and 12 May 1943. The operational area includes the whole of the area between the [[Suez Canal]] and the [[Strait of Gibraltar]], together with [[Malta]], [[Ethiopian Empire|Abyssinia]], [[Kenya]], the [[Sudan]], both [[Somaliland]]s and [[Eritrea]]. Areas not bordering on the Mediterranean only qualified for the Africa Star between 10 June 1940 and 27 November 1941 inclusive.<ref name="Stratford"/><ref name="Regulations"/>
* Royal Navy and Merchant Navy personnel qualified for the award of the Africa Star through service in the Mediterranean between these two dates, or for service in the campaigns in Abyssinia, Somaliland and Eritrea between 10 June 1940 and 27 September 1941. Merchant Navy personnel also qualified with service in operations off the Moroccan coast between 8 November 1942 and 12 May 1943. For sea-going service there was no condition that the 1939-45 Star should already have been earned before the Africa Star could be awarded.<ref name="Stratford"/><ref name="NewZealandAfrica"/>
* Army personnel had to enter North Africa on the establishment of an operational unit, while service in Abyssinia, Sudan, Somaliland and Eritrea also qualified.<ref name="Stratford"/>
* Air Force personnel had to land in or have flown over any of the operational areas. The Africa Star was also awarded to crews of transport aircraft that flew over certain specified routes.<ref name="Stratford"/><ref name="Birkenhead"/>
* Members of the Australian Imperial Force qualified for the award of the Africa Star for service in [[Syria]] between 8 June and 11 July 1941.<ref name="Australia">Commonwealth of Australia Gazette No. S134 dated 10 April 1995</ref>

Service in West Africa did not qualify for the award of the Africa Star.<ref name="Stratford"/><ref name="NewZealandAfrica"/>

===Clasps===
Regulations issued in 1945 only allow one clasp, the first one qualified for, to be worn with the Africa Star. Despite this, both the 8th Army and 1st Army clasps were awarded to and worn by, inter alia, [[Dwight Eisenhower|General Dwight Eisenhower]] and [[Harold Alexander, 1st Earl Alexander of Tunis|Field Marshal Harold Alexander]].<ref name="Regulations"/><ref name="Alexander1">[http://www.churchill-society-london.org.uk/1942JFMA.html The Churchill Society, London - As visible in Alexander's photograph on the Churchill Society website]</ref><ref name="Alexander2">[http://bp0.blogger.com/_QbsEkeU-AUY/Rqn8WF0ZzEI/AAAAAAAAAa0/kR-Qv1HLrl4/s1600-h/alexander.jpg As visible in a photograph of Field Marshal Alexander]</ref>
* The North Africa 1942–43 Clasp was awarded for service with the [[18th Army Group]] Headquarters between 15 February 1942 and 12 February 1943 inclusive, for Navy and Merchant Navy personnel in shore service, or for Air Force service in specified areas from 23 October 1942 to 12 May 1943 inclusive. In undress, a silver rosette worn on the ribbon bar denotes the award of this clasp.<ref name="Regulations"/>
* The 8th Army Clasp was awarded for service with the [[Eighth Army (United Kingdom)|Eighth Army]] between 23 October 1942 and 12 May 1943 inclusive. An Arabic numeral "8" is worn on the ribbon bar in undress to denote the award of this clasp.<ref name="Regulations"/>
* The 1st Army Clasp was awarded for service with the [[First Army (United Kingdom)#Second World War|First Army]] between 8 November 1942 and 12 May 1943 inclusive. An Arabic numeral "1" is worn on the ribbon bar in undress to denote the award of this clasp.<ref name="Regulations"/>

==Order of wear==
Campaign medals and stars are not listed by name in the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], but are all grouped together as taking precedence after the [[Queen's Medal for Chiefs]] and before the [[Polar Medal]]s, in order of the date of the campaign for which awarded.<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3352|date=17 March 2003}}</ref>

The order of wear of the nine campaign stars was determined firstly by their respective campaign start dates, secondly by the campaign's duration and thirdly by their dates of institution.<ref name="London Gazette"/>
* The 1939–1945 Star, from 3 September 1939 to 2 September 1945, the full duration of the [[World War II|Second World War]].<ref name="NewZealand1939-45"/>
* The Atlantic Star, from 3 September 1939 to 8 May 1945, the duration of the [[Battle of the Atlantic]] and the War in Europe.<ref name="NewZealandAtlantic"/>
* The Arctic Star, from 3 September 1939 to 8 May 1945, the duration of the Battle of the Atlantic and the War in Europe.<ref name="NewZealandArctic"/>
* The Air Crew Europe Star, from 3 September 1939 to 5 June 1944, the period until [[Normandy landings|D-Day]] minus one.<ref name="NewZealandACE"/>
* The '''Africa Star''', from 10 June 1940 to 12 May 1943, the duration of the [[North African Campaign]].<ref name="NewZealandAfrica"/>
* The Pacific Star, from 8 December 1941 to 2 September 1945, the duration of the [[Pacific War]].<ref name="NewZealandPacific"/>
* The Burma Star, from 11 December 1941 to 2 September 1945, the duration of the [[Burma Campaign]].<ref name="NewZealandBurma"/>
* The Italy Star, from 11 June 1943 to 8 May 1945, the duration of the [[Italian Campaign (World War II)|Italian Campaign]].<ref name="NewZealandItaly"/>
* The France and Germany Star, from 6 June 1944 to 8 May 1945, the duration of the [[North-West Europe Campaign of 1944–1945|Northwest Europe Campaign]].<ref name="NewZealandF&G"/>

[[File:Ribbon - Air Crew Europe Star.png|x37px|Air Crew Europe Star]] [[File:Ribbon - Africa Star.png|x37px|Africa Star]] [[File:Ribbon - Pacific Star.png|x37px|Pacific Star]] 
* Preceded by the [[Air Crew Europe Star]].
* Succeeded by the [[Pacific Star]].

==Description==
The set of nine campaign stars was designed by the [[Royal Mint]] engravers. The stars all have a ring suspender that passes through an eyelet formed above the uppermost point of the star. They are six–pointed stars, struck in yellow copper zinc alloy to fit into a 44 millimetres diameter circle, with a maximum width of 38 millimetres and 50 millimetres high from the bottom point of the star to the top of the eyelet.<ref name="Birkenhead">[http://www.birkenheadrsa.com/medals-details.php?MedalNumber=255 Birkenhead Returned Services Association - Military Medals - The Africa Star] (Access date 9 April 2015)</ref>

;Obverse
The obverse has a central design of the [[Royal Cypher]] "GRI VI", surmounted by a crown. A circlet, the top of which is covered by the crown, surrounds the cypher and is inscribed "THE AFRICA STAR".<ref name="Birkenhead"/>

;Reverse
The reverse is plain and, as with the other Second World War campaign medals, a no engraving policy was applied by all but three British Commonwealth countries. The recipient's name was impressed on the reverse for Australians, Indians and South Africans. In the case of Indians this consisted of the recipient's force number, rank, initials, surname and service arm or corps, and in the case of South Africans of the force number, initials and surname, in block capitals.<ref name="Stratford"/><ref name="Birkenhead"/><ref name="Bonarjee">[http://www.bharat-rakshak.com/IAF/History/1940s/VSCBonarjee.html Memoirs - My Days With The I.A.F (1940-48) -  V S C Bonarjee, IAS] (Access date 14 April 2015)</ref><ref name="BonarjeeGongs">[http://www.bharat-rakshak.com/IAF/History/1940s/Images/Bonarjee-Medals-Rear.jpg Rear Side of the Medals] (Access date 14 April 2015)</ref>

;Clasps
[[File:Ribbon - Africa Star & North Africa 1942-43.png|x37px|North Africa 1942-43 Clasp]] [[File:Ribbon - Africa Star & 8th Army.png|x37px|8th Army Clasp]] [[File:Ribbon - Africa Star & 1st Army.png|x37px|1st Army Clasp]]

All three clasps were struck in yellow copper zinc alloy and have a frame with an inside edge that resembles the perforated edge of a postage stamp. They are inscribed "NORTH AFRICA 1942–43", "8th ARMY" and "1st ARMY" respectively and were designed to be sewn onto the medal's ribbon. Regulations only allow one clasp, the first earned, to be worn with the Star. When the ribbon is worn alone, a silver Arabic numeral "8", numeral "1" or [[Rosette (design)|rosette]] is worn on the ribbon bar to denote the award of the respective clasp.<ref name="Stratford"/><ref name="Regulations"/><ref name="Birkenhead"/>

;Ribbon
The ribbon is 32 millimetres wide, with a 5 millimetres wide pale buff band, a 1½ millimetres wide Navy blue band, a 5 millimetres wide pale buff band, a 9 millimetres wide Army red band, a 5 millimetres wide pale buff band, a 1½ millimetres wide Air Force blue band and a 5 millimetres wide pale buff band. The pale buff represents the sand of the [[Sahara Desert]] while the [[Royal Navy]] and Merchant Navy, the Armies and the Air Forces are represented by the dark blue, red and light blue bands respectively.<ref name="Birkenhead"/>

The ribbons for this medal and the [[Defence Medal (United Kingdom)|Defence Medal]] as well as those of the other Second World War campaign stars, with the exception of the Arctic Star, were devised by [[George VI|King George VI]].<ref name="Regulations"/><ref name="WarRecords">[https://www.forces-war-records.co.uk/medals/1939-45-star/ Forces War Records - Medals - 1939-1945 Star] (Access date 2 April 2015)</ref>

==References==
{{Reflist|30em}}
{{British campaign medals}}
{{Australian Campaign Medals}}
{{New Zealand campaign medals}}
{{South African military decorations and medals}}

[[Category:British campaign medals]]
[[Category:Australian campaign medals]]
[[Category:New Zealand campaign medals]]
[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|402.19395]]
[[Category:Military decorations and medals of South Africa pre-1952]]