<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          =Rudolf Windisch
| image         =
| caption       =
| birth_date          = 27 January 1897
| death_date          = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->After 27 May 1918
| placeofburial_label =
| placeofburial =
| birth_place  =[[Dresden]], [[Germany]]
| death_place  =Missing in action in vicinity of [[Gouvrelles]], [[France]]
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    =Germany
| branch        =Infantry, then aerial service
| serviceyears  =1914 - 1918?
| rank          =[[Leutnant]]
| unit          =[[Infantry Regiment 177]], FEA 6, FFA 62, [[Kagohl II]], [[Jasta 32]], [[Jasta 50]]
| commands      =[[Jasta 66]]
| battles       =
| awards        =Prussian [[Order of the Crown (Prussia)|Order of the Crown]] (4th Class with Swords), [[Pour le Merite]], [[Royal House Order of Hohenzollern]], [[Iron Cross]], Gold Medal of the [[Military Order of St. Henry]]
| relations     =
| laterwork     =
}}

[[Leutnant]] '''Rudolf Friedrich Otto Windisch''' was a [[World War I]] [[fighter ace]] credited with 22 victories.<ref name="theaerodrome.com">http://www.theaerodrome.com/aces/germany/windisch.php</ref>

==Early life and service==
Rudolf Friedrich Otto Windisch was born in Dresden, Germany, the son of Bruno Windisch, who owned a pastry shop. During his childhood, Rudolf was very interested in aviation. He built model airplanes, and then a glider.<ref name="translate.google.com">https://translate.google.com/translate?hl=en&sl=de&u=http://www.flieger-album.de/logbuch.php&sa=X&oi=translate&resnum=1&ct=result&prev=/search%3Fq%3Dwww.flieger-album.de/logbuch.php%26hl%3Den%26safe%3Doff%26client%3Dsafari%26rls%3Den-us</ref>

On 14 September 1914, at the age of 17, he volunteered for a year's service with [[Infantry Regiment 177]]. After a short period of training, he was off to war on the [[Western Front (World War I)|Western Front]]. On 21 November, he was wounded by shrapnel and removed from front line duty. He recuperated first at a hospital in [[Laon]], [[France]], then at the Reserve Military Hospital in his home town of Dresden.<ref name="translate.google.com"/>

In February 1915, he was assigned to the Military Aviation School in [[Leipzig-Lindenthal]]. He was promoted to sergeant and was a flight instructor<ref name="translate.google.com"/> with FEA 6.<ref name="theaerodrome.com"/>

1 May 1916 brought a front line flying assignment with FA 62 on the Russian Front.<ref name="translate.google.com"/> On the night of 2/3 October 1916, he set out on what is arguably the first case of air-supported espionage. He landed behind Russian lines and dropped off Oberleutnant [[Maximilian von Cossel]] (1893-1967) near the [[Rowno]] to [[Brody]] rail line. Cossel destroyed a railroad bridge that was of strategic importance to the Russians. Windisch swooped in on the 3rd to pick Cossel up and carry him back to safety. This feat earned him the Prussian [[Order of the Crown (Prussia)|Order of the Crown]] (4th Class with Swords); the Kaiser himself presented it on 18 October 1916. Windisch would be the only pilot so honored.<ref name="theaerodrome.com"/>

In November, he would transfer to [[Kagohl II]] to fly recon missions on the [[Western Front (World War I)|Western Front]]. On 20 February 1917, he moved up to flying fighters with Royal Bavarian [[Jasta 32]].<ref name="theaerodrome.com"/><ref>http://www.theaerodrome.com/services/germany/jasta/jasta32.php</ref>

==Aerial combat career==
Even before his feat of espionage derring-do, Windisch had scored his first aerial victory. On 25 August 1916, while still flying a two-seater reconnaissance airplane, he became a [[balloon buster]] by shooting down one of the Russian observation balloons southeast of Brody.<ref name="theaerodrome.com"/>

His next victory would be almost a year later, after he transferred to [[Jasta 32]] on the French Front. On 18 September 1917, he shot down an [[Arkhangelsky Ar-2|AR2]] near Fleury, France. That was the first of his five triumphs during 1917. The last of them, a [[Société Pour L'Aviation et ses Dérivés|S]]pad shot down near [[Laval, Mayenne|Laval]], brought his count to six.<ref name="theaerodrome.com"/>

He shot down another balloon on 3 January 1918, and another Spad the following day. On 10 January, he was transferred to Royal Prussian [[Jasta 50]] for seasoning before taking command of Royal Prussian [[Jasta 66]] on the 24th.<ref name="theaerodrome.com"/>

Sometime during this period, Windisch wangled flying time in a captured [[Spad VII]]. It is unknown if he flew it in combat.<ref name="Spad VII Aces of World War I">{{cite book |title= Spad VII Aces of World War I |page= 38 }}</ref>

He scored his first win with his new squadron on 15 March 1918. He had six victories in that month, including a triple on 24 March 1918. He scored three more times in April, and five in May, to bring his tally to 22. No fewer than 16 of his wins were over Spad fighters. A [[Sopwith Aviation Company|Sopwith]] and an AR2 completed his list of fighter planes. He also destroyed three balloons and a single two-seater reconnaissance plane.<ref name="theaerodrome.com"/> The fact that he was one of the original pilots of Germany's best fighter, the [[Fokker D.VII]], gave him tactical advantage over his foes.<ref name="Spad VII Aces of World War I"/>

==Disappearance==
His last victory was on the afternoon of 27 May 1918.<ref name="theaerodrome.com"/> Immediately after he shot down this last Spad, he was jumped by several other enemy scouts. A bullet through the gas tank forced him to land behind French lines, about 50 meters from his final victim.<ref name="translate.google.com"/>

The [[International Red Cross]] reported Windisch was a prisoner of war on several occasions. French pilots who fell into German hands reported him in a French prison. On the assumption he was alive, he was awarded the [[Pour le Merite]] on 6 June 1918. The reports differed on whether or not he had been injured, with some rumors saying he had died in captivity.<ref name="theaerodrome.com"/><ref name="translate.google.com"/>

Nothing more would ever be heard of Windisch.<ref name="theaerodrome.com"/><ref name="translate.google.com"/>
Date 	Time 	Unit 	Aircraft 	Opponent 	Location
1 	25 Aug 1916 		FA 62 	  	Balloon 	SE of Brody
2 	18 Sep 1917 	1547 	Jasta 32 	  	AR2 	Fleury
3 	27 Sep 1917 	1028 	Jasta 32 	  	SPAD 	Betheville
4 	01 Nov 1917 		Jasta 32 	  	SPAD 	W of Bray
5 	07 Nov 1917 		Jasta 32 	  	SPAD 	Brancourt
6 	18 Nov 1917 		Jasta 32 	  	SPAD 	Laval
7 	03 Jan 1918 		Jasta 32 	  	Balloon 	Villers
8 	04 Jan 1918 		Jasta 32 	  	SPAD 	S of Staubecken
9 	15 Mar 1918 		Jasta 66 	  	SPAD 	Vitry-Reims
10 	17 Mar 1918 		Jasta 66 	  	SPAD 	Vitry-Reims
11 	23 Mar 1918 		Jasta 66 	  	Sopwith 2 	Le Bruin Ferme
12 	24 Mar 1918 		Jasta 66 	  	SPAD 2 	Bretigny
13 	24 Mar 1918 		Jasta 66 	  	SPAD 	Tergnier
14 	24 Mar 1918 		Jasta 66 	  	SPAD 	Tergnier
15 	11 Apr 1918 		Jasta 66 	  	Bréguet 14 	S of Noyon
16 	21 Apr 1918 		Jasta 66 	  	SPAD 	Guy
17 	21 Apr 1918 		Jasta 66 	  	SPAD 	Guy
18 	03 May 1918 		Jasta 66 	  	Balloon 	Juvigny
19 	04 May 1918 		Jasta 66 	  	SPAD 	Carlepont
20 	15 May 1918 		Jasta 66 	  	SPAD 2 	Trosly-Loire
21 	16 May 1918 		Jasta 66 	  	SPAD 	Thiescourt
22 	27 May 1918 		Jasta 66 	  	SPAD 2 	Couvrelles-Lesges
==Sources of information==
{{reflist}}

==References==
* ''Spad VII Aces of World War I''. Jon Guttman. Osprey Publishing, 2001. ISBN 1-84176-222-9, ISBN 978-1-84176-222-7.

{{wwi-air}}

{{DEFAULTSORT:Windisch, Rudolf}}
[[Category:German World War I flying aces]]
[[Category:1897 births]]
[[Category:1918 deaths]]
[[Category:People from Dresden]]
[[Category:People from the Kingdom of Saxony]]
[[Category:Military personnel of Saxony]]
[[Category:Luftstreitkräfte personnel]]
[[Category:Missing aviators]]
[[Category:German military personnel killed in World War I]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Recipients of the Pour le Mérite (military class)]]
[[Category:Missing in action of World War I]]