'''Edward Kosner''' (born July 26. 1937)<ref name="WhosWho">Marquis Who’s Who</ref> is an American journalist and author who served as the top editor of ''[[Newsweek]]'', ''[[New York (magazine)|New York]]'' and ''[[Esquire (magazine)|Esquire]]'' magazines and the ''[[New York Daily News]]'' during a forty-five-year career. He is the author of a memoir, “It’s News to Me,” published in 2006,<ref name="NewsToMe">“It’s News to Me,” New York, Thunder’s Mouth Press, 2006</ref><ref name="NYConfidential">”New York Confidential,” NY Times Sunday Book Review, Oct. 1, 2006.</ref> and is a frequent book reviewer for the ''Wall Street Journal''. He lives in New York and on Amelia Island on the Atlantic coast of northern [[Florida]].

== Early life ==

Kosner was born in [[New York City]], the son of Sidney Kosner, a salesman for a men’s and boy’s outerwear manufacturer, and Annalee Fisher Kosner, a housewife. Growing up in [[Washington Heights, Manhattan|Washington Heights]] in upper [[Manhattan]], he was the editor of his elementary and junior high school newspapers. At 16, he enrolled at [[City College of New York|City College]], where was editor-in-chief of the undergraduate newspaper, the ''Campus'', and the CCNY correspondent for ''[[The New York Times]]''.<ref name="NewsToMe" />

== Journalism career ==

On graduation from CCNY in 1958, Kosner joined the ''[[New York Post]]'', then a liberal tabloid owned by [[Dorothy Schiff]].,<ref name="NewsToMe" /><ref name="FBI">“1959 F.B.I. Search of Room of a Newsman is Reported.” NY Times, July 21. 1975</ref> He spent five years at the paper, working on night rewrite, as a series writer, and as an assistant city editor.
In 1963, he was hired by ''[[Newsweek]]'' as a writer in the National Affairs section. His first cover story was on [[Jacqueline Kennedy]]’s new life after the assassination of her husband.<ref name="JackieO">“Jacqueline Kennedy—Looking Ahead,” Newsweek, January 6, 1964</ref> Over the next fifteen years, Kosner wrote more than a score of cover stories, started a section on urban problems, and held all the top editorial positions on the magazine under [[Osborn Elliott]]. He directed the magazine’s extensive coverage of the [[Watergate scandal]] that led to the resignation of President [[Richard Nixon]] in 1974.<ref name="NewsToMe" />

In 1975, at 37, he was named editor of ''Newsweek''.<ref name="Newsweek1">“Kosner Appointed Newsweek Editor,” NY Times, July 31, 1975</ref> During his editorship, the magazine set records for advertising and circulation. But he was dismissed by the magazine’s owner, [[Katharine Graham]] in 1979,<ref name="Newsweek2">“Newsweek Names New Editor,” NY Times, June 28, 1979</ref> one of five editors of ''Newsweek'' she would fire between 1970 and 1984.

Early in 1980, [[Rupert Murdoch]] hired Kosner to edit ''[[New York (magazine)|New York]]'' magazine, which he had taken over from founder [[Clay Felker]] three years earlier.<ref name="NewYork1">“Kosner, Former Editor of Newsweek, Chosen as Editor of New York,” NY Times, March 1, 1980</ref> Kosner ran ''New York'' for thirteen years, responsible for the business side as well as the editorial side for the second half of his tenure. During his time at ''New York'', the magazine set records for advertising sales and profits and won several [[National Magazine Awards]]. While at ''New York'', Kosner served a two-year term as president of the [[American Society of Magazine Editors]]. In 1991, Murdoch sold ''New York'' and other magazines to a group headed by financier [[Henry Kravis]], Two years later, Kosner left to take the editorship of ''[[Esquire (magazine)|Esquire]]'', the men’s magazine, which he ran until 1997.<ref name="Esquire1">“Three Editors Go to  Esquire,” NY Times, Dec.6, 1993</ref><ref name="Esquire2">“Hearst Names New Top Editor to Turn Esquire Around,” NY Times, May 30, 1997</ref>

The next year, Kosner joined the ''[[New York Daily News]]'', the largest tabloid in the U.S., to create and edit a new Sunday edition.<ref name="DailyNews1">“Sunday News Gets Bigger,” NY Times, Feb. 22, 1999</ref> In 2000, Mortimer B. Zuckerman, the ''News''’s owner, promoted him to editor-in-chief of the daily paper.<ref name="DailyNews2">Jacques Steinburg, [https://www.nytimes.com/2003/07/23/business/editor-of-daily-news-to-retire-in-march.html “Editor of Daily News to Retire in March”], New York Times. July 23, 2003</ref> Over the next four years, Mr. Kosner  oversaw the tabloid’s coverage of a run of major stories, including the “tied” [[2000 Presidential election]] between [[George W. Bush]] and [[Al Gore]], on page one for forty consecutive days, the [[9/11 attack]] and its aftermath, and the wars in [[Afghanistan]] and [[Iraq]]. Mr. Kosner retired from the ''News'' in 2004 after a falling out with Mr. Zuckerman.<ref name="DailyNews2" /> In 2006, he published his journalistic memoir, “It’s News to Me.”  He began reviewing books for the ''[[Wall Street Journal]]'' in 2007.<ref name="WSJ">“Celebrity Society’s Secret History,” Wall Street Journal, April 12. 2007</ref>

== Personal life ==

Kosner married Alice Nadel in 1959. They had two children, John, born in 1960, and Anthony, born in 1962. The couple was divorced in 1977.<ref name="WhosWho" /> Since 1978, Kosner has been married to Julie Baumgold, a novelist and magazine writer.<ref name="Baumgold">”Edward Kosner of Newsweek Weds Julie Baumgold, Writer,” NY Times, Nov. 20, 1978</ref> Their daughter, Lily, was born in 1981. He has six grandchildren.

== References ==

{{Reflist}}

== External links ==
*{{C-SPAN|Ed Kosner}}

{{DEFAULTSORT:Kosner, Edward}}
[[Category:Articles created via the Article Wizard]]
[[Category:1937 births]]
[[Category:Newsweek people]]
[[Category:New York Post people]]
[[Category:Esquire (magazine)]]
[[Category:New York Daily News people]]
[[Category:Living people]]
[[Category:Watergate scandal investigators]]