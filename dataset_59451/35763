{{good article}}
{{Infobox military conflict
|conflict=Action of 31 May 1809
|partof=the [[Napoleonic Wars]]
|image=<div style="position: relative">[[File:Bay of Bengal map 1800s.png|250px]]{{Image label|x=0.630|y=0.570|scale=250|text=[[File:Red pog.svg|8px|Action]]}}</div>
|caption=Location of the Action of 31 May 1809
|date=31 May 1809
|place=[[Bay of Bengal]], [[Indian Ocean]].
|result= French victory
|combatant1=[[File:Flag of France.svg|22px]] [[First French Empire|French Empire]]
|combatant2=[[File:Flag of the British East India Company (1801).svg|22px]] [[Honourable East India Company]]
|commander1= [[Jean-Baptiste-Henri Féretier]]
|commander2= John Dale
|strength1= [[frigate]] [[French frigate Caroline|''Caroline'']]
|strength2= [[East Indiamen]] ''Streatham'', ''Europe'' and ''Lord Keith''
|casualties1= 1 killed, 4 wounded
|casualties2= 6 killed, 4 wounded, ''Streatham'' and ''Europe'' captured
}}{{coord|9|15|N|90|30|E|display=title|type:event}}{{Campaignbox Mauritius campaign of 1809–1811}}
The '''Action of 31 May 1809''' was a naval skirmish in the [[Bay of Bengal]] during the [[Napoleonic Wars]]. During the action, an [[Honourable East India Company]] convoy carrying goods worth over £500,000 was attacked and partially captured by the French [[frigate]] [[French frigate Caroline|''Caroline'']]. The three [[East Indiamen]] that made up the convoy fought against their opponent with their own batteries of [[cannon]] but ultimately were less powerful, less manoeuvrable and less trained than their opponent and were defeated one by one; only the smallest of the three escaped. The action was the first in a string of attacks on important convoys in the [[Indian Ocean]] by French cruisers operating from [[Mauritius|Île de France]] and [[Île Bonaparte]] during a concerted campaign against British shipping in the region.

==Background==
In November 1808, a squadron of powerful French frigates sailed for [[Mauritius|Île de France]] under Commodore [[Jacques Félix Emmanuel Hamelin|Jacques Hamelin]]. This squadron was under orders to attack and capture or destroy British shipping in the [[Indian Ocean]], particularly the heavily armed convoys of [[East Indiamen]] that carried millions of [[Pound sterling|pounds]] worth of trade goods from [[British India]] and the Far East to Britain.<ref name="RG92">Gardiner, p. 92</ref> These convoys were operated by the [[Honourable East India Company]] (HEIC), which ran British India and maintained a private army and navy to secure the colony and its trade routes. During the late [[Napoleonic Wars]], French naval strategy focused on the disruption of this trade with the use of fast and well-armed frigates to operate independently along British trade routes and capture British merchant ships. This affected the British economy, which was already severely stretched by the war, and forced the [[Royal Navy]] to divert resources to distant parts of the world to protect British trade.<ref name="RG59">Gardiner, p. 59</ref>

During the late spring of 1809, following the end of the Indian Ocean hurricane season, Hamelin ordered his ships to operate in the [[Bay of Bengal]]. One of these frigates was the 40-gun [[French frigate Caroline|''Caroline'']], which was built in [[Antwerp]] in 1806 and weighed 1,078 tons. ''Caroline'' was commanded by [[Jean-Baptiste-Henri Féretier]], newly promoted following the sudden death of her previous captain.<ref name="WJ199">James, p. 199</ref> Féretier was the first of Hamelin's captains to find a British convoy, spotting three sails on the horizon on 31 May. These belonged to a Britain-bound convoy of East Indiamen, which had departed the [[Hooghly River]] on 2 May. Laden with over £500,000 worth of [[silk]] and other trade goods, these ships were an important asset to the HEIC and had originally been part of a larger convoy, guarded by the [[sloop]] [[HMS Victor (1808)|HMS ''Victor'']] and consisting of five Indiamen and several smaller vessels.<ref name="WJ193"/> On 24 May a storm divided the convoy; ''Victor'', the small ships and two of the Indiamen ''Monarch'' and ''Earl Spencer'' were separated from the remainder, the ''Streatham'', ''Europe'' and ''Lord Keith'' after ''Monarch'' sprang a leak.<ref name="ST215">Taylor, p. 215</ref>

The Indiamen were not unprotected: each one was large and powerfully built and carried a number of cannon. ''Streatham'' and ''Europe'' weighed over 800 tons each and carried 30 cannon, whereas the smaller ''Lord Keith'' was 600 tons and carried 12 guns.<ref name="WJ193">James, p. 193</ref> Four years earlier, a convoy of East Indiamen [[Battle of Pulo Aura|had driven off a French]] [[ship of the line]] and attached frigates under [[Charles-Alexandre Léon Durand Linois|Admiral Linois]] in similar waters by forming a [[battle line]] and firing on their opponents as they closed.<ref name="RLA185">Adkins, p. 185</ref> The crews of these East Indiamen were not of Royal Navy standard, however, with insufficient training and large numbers of Chinese and [[lascar]] seamen, who proved unreliable in combat.<ref name="EB398">Brenton, p. 398</ref>

==Battle==
One of the smaller ships from the convoy, an American merchant ship named ''Silenus'', had separated from the main body in the storm and arrived at the [[Nicobar Islands]]. There she had encountered ''Caroline'' and the American captain had reported the location and value of the convoy to Féretier.<ref name="ST217">Taylor, p. 217</ref> Setting all sail, Féretier took ''Caroline'' to the north-west, and sighted the convoy at 05:30, only a few days after leaving the Nicobar Islands. The British ships, under the loose command of John Dale in ''Streatham'', originally mistook the French frigate for the missing ''Victor'' and it was not until another half-hour had passed that Dale realised the danger his ships were in. Ordering the Indiamen to form a line of battle, Dale placed his ship in the centre, with the small ''Lord Keith'' ahead and ''Europe'' behind.<ref name="WJ194"/> However, the lack of naval experience on the British ships resulted in the Indiamen sailing too far from one another in line, thus leaving them unable to provide effective mutual support.<ref name="WJ194"/>

Able to attack the HEIC ships individually, ''Caroline'' pulled alongside ''Europe'' at 06:30 and began a heavy fire into the merchant ship, which intermittently replied with her available guns. Within 30 minutes, ''Europe''<nowiki>'</nowiki>s rigging was tattered, many of her guns dismounted and a number of her crew wounded or killed.<ref name="WJ194">James, p. 194</ref> Moving past his now disabled opponent, Féretier next attacked ''Streatham'', which had slowed in an unsuccessful attempt to support ''Europe''. Now alone against the frigate, ''Streatham'' came under heavy fire at 07:00 and by 08:00 was badly damaged, with casualties in her crew, her guns all dismounted and her lascars hiding below decks.<ref name="ST217"/> With further resistance hopeless, Dale hauled down the company flag and surrendered.<ref name="EB398"/>

During the engagement between ''Streatham'' and ''Caroline'', ''Lord Keith'' and ''Europe'' had fired sporadically at the French ship with little effect. Pulling away from his surrendered opponent, Féretier then fired on ''Lord Keith'', whose captain, Peter Campbell, realised that his ship stood no chance against the frigate and turned eastward, running before the wind to escape despite suffering severe damage to ''Lord Keith''<nowiki>'</nowiki> rigging as he did so.<ref name="WJ194"/> William Gelston, captain of ''Europe'', also attempted to flee, but his battered ship was in no condition to outrun the virtually untouched frigate, and he surrendered at 10:00. ''Lord Keith'' eventually arrived safely at [[Penang]] on 9 June.<ref name="ST218">Taylor, p. 218</ref> Casualties on the British ships were six killed and at least four wounded, while the French lost one killed and three wounded.<ref name="WJ194"/>

==Aftermath==
Féretier repaired his captures at sea and returned to Île de France, arriving two months later on 22 July. Discovering the presence of a British frigate squadron under [[Josias Rowley]] off [[Port Louis]], Féretier diverted to [[Saint-Paul, Réunion|Saint Paul]] on Île Bonaparte.<ref name="RG93"/> Among the goods removed from the ships were the £500,000 worth of silk, which was stored in warehouses near the docks. In the British [[raid on Saint Paul]] on 21 September 1809, these warehouses and their contents were burnt and ''Caroline'', ''Streatham'' and ''Europe'' all captured by the raiding force.<ref name="RG93">Gardiner, p. 93</ref> Despite these subsequent losses, Féretier was highly commended for his leadership in the action and received a promotion from Governor [[Charles Mathieu Isidore Decaen|Charles Decaen]]. He also received letters from the captains of ''Streatham'' and ''Europe'', thanking him for his attention and courtesy to their crews and passengers during their period of captivity.<ref name="WJ195">James, p. 195</ref>

==Notes==
{{reflist|2}}

==References==
*{{cite book
 | last = Adkins
 | first = Roy & Lesley
 | authorlink = 
 | year = 2006
 | chapter = 
 | title = The War for All the Oceans
 | publisher = Abacus
 | location = 
 | isbn = 0-349-11916-3 
}}
*{{cite book
 | last = Brenton
 | first = Edward Pelham
 | authorlink = Edward Pelham Brenton
 | year = 1825
 | chapter = 
 | title = The Naval History of Great Britain, Vol. IV
 | url = https://books.google.com/books?id=AqQNAAAAQAAJ&printsec=frontcover&dq=edward+pelham+brenton#PPA398,M1 
 | publisher = 
 | location = 
}}
*{{cite book
 | last = Gardiner
 | first = Robert
 | authorlink = 
 | year = 2001 |origyear=1998
 | chapter = 
 | title = The Victory of Seapower
 | publisher = Caxton Editions
 | location = 
 | isbn = 1-84067-359-1
}}
*{{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002 |origyear=1827
 | chapter = 
 | title = The Naval History of Great Britain, Volume 5, 1808&ndash;1811
 | publisher = Conway Maritime Press
 | location = 
 | isbn = 0-85177-909-3
}}
*{{cite book
 | last = Taylor
 | first = Stephen
 | authorlink = 
 | year = 2008
 | chapter = 
 | title = Storm & Conquest: The Battle for the Indian Ocean, 1809
 | publisher = Faber & Faber
 | location = 
 | isbn = 978-0-571-22467-8
}}

[[Category:Conflicts in 1809]]
[[Category:Naval battles of the Napoleonic Wars]]
[[Category:Naval battles involving France]]
[[Category:Naval battles involving the United Kingdom]]
[[Category:May 1809 events]]