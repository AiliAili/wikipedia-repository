{{Infobox journal
| title = Developmental Biology
| cover = [[File:Developmental Biology (journal).gif]]
| discipline = [[Developmental biology]]
| abbreviation = Dev. Biol.
| editor = R. Krumlauf
| publisher = [[Elsevier]]
| frequency = Biweekly
| history = 1959-present
| impact = 3.155
| impact-year = 2015
| openaccess = [[Delayed open access|Delayed]], after 12 months
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/622816/description#description
| link1 = http://www.sciencedirect.com/science/journal/00121606
| link1-name = Online access
| ISSN = 0012-1606
| eISSN = 1095-564X
| CODEN = DEBIAO
| OCLC = 01718504
| LCCN = a61000440
}}
'''''Developmental Biology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]]. It was established in 1959 and is the official journal of the [[Society for Developmental Biology]]. It publishes research on the mechanisms of development, differentiation, and growth in animals and plants at the molecular, cellular, and genetic levels. The journal is published twice a month by [[Elsevier]].

== Abstracting and indexing ==
The journal is abstracted an indexed in:
{{columns-list|colwidth=30em|
* [[Elsevier BIOBASE|BIOBASE]] 
* [[Biological & Agricultural Index]] 
* [[Biological Abstracts]] 
* [[Biosis Previews]] 
* [[Chemical Abstracts]] 
* [[Current Awareness in Biological Sciences]] 
* [[Current Contents]]
* [[Elsevier BIOBASE]] 
* [[EMBASE]] 
* [[EMBiology]] 
* [[Genetics Abstracts]] 
* [[MEDLINE]] 
* [[Science Citation Index]] 
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 4.069.<ref name=WoS>{{cite book |year=2013 |chapter=Developmental Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-03-15 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/622816/description#description}}

[[Category:Developmental biology journals]]
[[Category:English-language journals]]
[[Category:Elsevier academic journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1959]]
[[Category:Biweekly journals]]