'''Post-money valuation''' is the value of a company after an investment has been made. This value is equal to the sum of the [[pre-money valuation]] and the amount of new equity.<ref>[http://www.markpeterdavis.com/getventure/2008/06/venture-valuati.html Get Venture by Mark Peter Davis: Venture Valuation Overview<!-- Bot generated title -->]</ref>

External investors, such as [[venture capital]]ists and [[angel investor]]s, will use a [[pre-money valuation]] to determine how much equity to demand in return for their cash injection to a company.  The implied post-money valuation is calculated as the dollar amount of investment divided by the equity stake gained in an investment.

==Example 1==
If a company is worth $100 million (pre-money) and an investor makes an investment of $25 million, the new, post-money valuation of the company will be $125 million. The investor will now own 20% of the company. 

This basic example illustrates the general concept.  However, in actual, real-life scenarios, the calculation of post-money valuation can be more complicated—because the capital structure of companies often includes convertible loans, warrants, and option-based management incentive schemes.

Strictly speaking, the calculation is the price paid per share multiplied by the total number of shares existing after the investment&mdash;i.e., it takes into account the number of shares arising from the conversion of loans, exercise of [[in-the-money]] [[Warrant (finance)|warrants]], and any in-the-money [[Option (finance)|options]]. Thus it is important to confirm that the number is a fully diluted and fully converted post-money valuation.

In this scenario, the pre-money valuation should be calculated as the post-money valuation minus the total money coming into the company&mdash;not only from the purchase of shares, but also from the conversion of loans, the nominal interest, and the money paid to exercise in-the-money options and warrants.

==Example 2==
Consider a company with 1,000,000 shares, a convertible loan note for $1,000,000 converting at 75% of the next round price, warrants for 200,000 shares at $10 a share, and a granted [[employee stock ownership plan]] of 200,000 shares at $4 per share.  The company receives an offer to invest $8,000,000 at $8 per share. 

The post-money valuation is equal to $8 times the number of shares existing after the transaction&mdash;in this case, 2,366,667 shares.  This figure includes the original 1,000,000 shares, plus 1,000,000 shares from new investment, plus 166,667 shares from the loan conversion ($1,000,000 divided by 75% of the next investment round price of $8, or $1,000,000 / (.75 * 8) ), plus 200,000 shares from in-the-money options.  The fully converted, fully diluted post-money valuation in this example is $18,933,336.

The pre-money valuation would be $9,133,336&mdash;calculated by taking the post-money valuation of $18,933,336 and subtracting the $8,000,000 of new investment, as well as $1,000,000 for the loan conversion and $800,000 from the exercise of the rights under the ESOP. Note that the warrants cannot be exercised because they are not in-the-money (i.e. their price, $10 a share, is still higher than the new investment price of $8 a share).

==See also==
*[[Pre-money valuation]]

==References==
{{Reflist}}{{Dead link|date=September 2012}}

==External links==
*[http://www.investopedia.com/ask/answers/114.asp Forbes Investopedia: What's the difference between pre-money and post-money? ]
*[http://www.thestartuplawyer.com/venture-capital/what-is-a-pre-money-and-post-money-valuation Ryan Roberts: What is a Pre-money and Post-money Valuation?]
*[http://www.socaltech.com/Insights/showarticle.php?id=00040 Samuel Wu: Venture Capital 101 for Startups - Valuation]
*[https://www.vcexperts.com/buzz_articles/96| Joseph W. Bartlett: A Missing Piece of the Valuation Puzzle]

{{Private equity and venture capital}}

[[Category:Private equity]]
[[Category:Venture capital]]
[[Category:Valuation (finance)]]