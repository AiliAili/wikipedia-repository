{{all plot|date=February 2016}}
{{Italic title}}
'''''Sans Famille''''' (Translation: ''Without Family'' English title: ''Nobody's Boy'') is an 1878 French [[novel]] by [[Hector Malot]]. The most recent English translation is ''Alone in the World'' by Adrian de Bruyn, 2007. The novel was reportedly inspired by the Italian street musicians of the XIX century, in particular the harpists from [[Viggiano]], [[Basilicata]].<ref>{{cite web|title=L’arpa perduta - L’identità dei musicanti girovaghi |url=http://www.consiglio.basilicata.it/consiglionew/files/docs/28/54/62/DOCUMENT_FILE_285462.pdf|publisher=consiglio.basilicata.it|author=Eva Bonitatibus |language = Italian, English| date=|accessdate=June 22, 2016}}</ref>

==Plot==

===First Volume===
[[File:AodW01a.jpg|thumb|left|Sans Famille, first volume]]

==== In Chavanon and Paris ====
Jerome Barberin lives with his wife in a little French town, Chavanon (on the edge of the Central Massif, near the origins of the river Loire). He usually works in Paris as a mason. One day he finds a baby boy in Paris. The boy wears very fine clothes, so apparently his parents are rich. Barberin offers to take care of the child, hoping to get a good reward. He gives the boy to his wife, and calls him Rémi.

Barberin gets injured in an accident. He blames his employer and hopes to receive financial compensation in a trial. The trial costs a lot of money, and Barberin tells his wife to sell her cow (her main source of wealth) and to get rid of Rémi. She does the former.

When Rémi is eight years old, and this is where the story starts, Barberin comes home unexpectedly, bitter and penniless, having lost his trial. He sees that Rémi is still there and decides to get rid of him when he meets a travelling artist, Signor Vitalis, in the local pub, who travels with three dogs - Capi, Zerbino and Dolce - and a monkey, Joli-Cœur. Vitalis offers to take Rémi on as apprentice for money.

==== Travelling with Vitalis ====
Rémi leaves his childhood home, without even a chance to say goodbye to his foster mother (who would have done anything to prevent the transaction) and starts a journey of the roads of France. Vitalis is a kind man, certainly better company than Barberin and teaches Rémi to play the harp and to read. Often both go hungry and without a roof over their heads; but in the animals, especially in Capi, Rémi gains dear friends, and in Vitalis the father he lacks. Together they earn a living by giving musical and stage performances in villages, towns and cities.

They travel West, via Murat (where Vitalis tells him of the prince of Naples, brother in law of Napoleon, who came from there). Next stop is Ussel where Rémi is outfitted for his new life, including shoes, which he has never owned before. The first big city going south is Bordeaux after which they cut right through the morass of Les landes towards Pau.

==== Meeting the Milligans ====
When they are in Toulouse, Vitalis is put in jail after an incident with a policeman who is rough with Rémi. It is not easy for a ten-year-old to feed himself and four animals under his care, and they nearly starve, when they meet the "Swan" - a little river ship owned by Mrs. Milligan and her ill son Arthur. Rémi is taken in to entertain the sick boy, and he becomes almost part of the family. They travel towards Montpellier and the Mediterranean on the Canal du Sud. Rémi learns the story about her dead husband and brother-in-law, who under the English law, was to inherit all of his brother's fortune if he died childless. An earlier child had disappeared and was never found (under the charge of this James Milligan) however soon after the husband’s death, Arthur was born.

Two months later Vitalis is released from jail, Mrs. Milligan pays for him to take the train to Cette. Rémi and the Milligans would like to stay together, but Vitalis thinks it is better for Rémi to be free, and so they say goodbye. However, Mrs. Milligan judges that Vitalis is a very kind and honest man.

==== Three animals of Vitalis die ====
They travel via Tarascon, Montelimar, Valence, Tournon, Vienne, Lyon, Dyon, and Chalon on the way to Paris but winter catches up to them 30 miles from Troyes and in a snowstorm Zerbino and Dolce are eaten by wolves in the woods, and Joli-Cœur catches pneumonia.

In an attempt to raise money for the doctor, Rémi and Vitalis give a performance and Vitalis sings. Rémi has never before heard Vitalis sing and not only Rémi is bewildered: a young and apparently rich lady tells Vitalis that she is amazed to hear his wonderful voice. Vitalis reacts angrily. He explains his skill to the lady by telling that he used to be a singer's servant. He shows no appreciation when the lady gives a gold coin to Capi. They return to Joli-Cœur with the money, but it's too late, Joli-Cœur is dead.

==== Garofoli ====
They now continue their journey to Paris. Vitalis decides to leave Rémi with another "padrone" for the winter, while he trains new animals with the proceeds. This "padrone" is a man who keeps a group of boys, sold by their poverty-stricken parents, working for him (also see the English story “Oliver Twist” by Charles Dickens). Garofoli isn't home, and Vitalis tells Rémi to wait there, and that he will be back soon. Rémi passes two horrible hours in the Rue Lourcine house - waiting for Garofoli and talking to an ill-looking boy, Mattia, who keeps house because Garofoli believes him too stupid and incapable of working outside, he keeps the soup pot locked so that Mattia could not eat from it.

When the other boys and Garofoli return, Rémi witnesses how terribly Garofoli abuses those who do not bring home the amount of money required: he beats and starves them. When Vitalis comes back and sees how the boys are being flogged, he tells Garofoli that he could go to the police, but Garofoli threatens back to tell "some people just one name which will make Vitalis red from shame". Vitalis takes the wondering and grateful Rémi away. This act of love costs Vitalis his life. That night, unable to find a place to stay, Vitalis and Rémi collapse in the snowstorm under a fence after fruitlessly searching access to a stone quarry shelter.

==== With the Acquins ====
Rémi wakes up in a bed, with people standing around him: a man, the gardener Pierre Acquin two boys Alexis and Benjamin and two girls Étienette (Martha) and little mute Lise, who is about 5–6 years old, and watches Rémi with “speaking eyes”. Rémi learns the terrible truth: Vitalis is dead. In an attempt to discover his identity, the police officers take Rémi to Garofoli, who reveals the truth: Vitalis used to be the famous Italian singer Carlo Balzani. When he aged, he lost his voice and was too proud to sing in lesser venues. He decided to disappear, changing his identity to Vitalis.

The family take Rémi and Capi in. Rémi especially adores Lise. He teaches her to read and plays the harp for her. Lise loves a Neapolitan song in particular. Rémi becomes a gardener, and two years of hard work and merry Sundays follow. Then a terrible hailstorm ruins the glass in the greenhouse, and Acquin is in debt to the man he borrowed from to buy his business. He cannot pay and has to enter a debt jail. The children are to go to uncles and aunts, in several French towns. Although the children insist that Rémi also belongs to the family, none of the uncles and aunts is willing or able to take care of Rémi. Broken-hearted, vowing to his siblings to visit and bring the father news from them, Rémi takes his harp and Capi and takes to the road.

=== Second Volume ===

==== Mattia ====
Rémi decides to head south towards Fontainebleau but hasn't gone long when he meets a companion, Mattia, the boy from Garofoli, starving near a church on the streets of Paris. Garofoli is in prison for beating another boy to death. Mattia pleads with Rémi to take him into his troupe. Rémi is scared: with him, Mattia might die of hunger as much as alone. But Mattia convinces him that two will never die of hunger because one helps the other. Thus, "Rémi troupe" consists now of two twelve-year-old musicians and a dog.

Mattia turns out to be a gifted violinist, he plays other instruments too, and he learned some tricks while working for some time in a circus. The boys do well in the spring at weddings and festivals, their talents are appreciated and Rémi takes up the plan to buy a cow and visit Mother Barberin.

==== Mining ====
Since a cow costs a lot of money Rémi plans a route via Corbeil, Montgaris, Gien, Bourges, St. Amand and MontluÇon where they make a lot of money on their way to visit Alexis, who now lives with his Uncle Gaspard (Father Acquin's brother) in the mining town Varses, and works in the mine with his uncle. When Alexis is wounded and unable to work for a while, Rémi volunteers to replace him. One of the miners is nicknamed magister; he is an old and wise man. He becomes a good friend and he explains the history of coal.

One day the mine is flooded, by the river Divonne which flows overhead. Seven miners, including Uncle Gaspard, the magister and Rémi, find shelter, but are trapped. They are waiting to be rescued, but have no idea of the amount of time passing in hunger and fear. One of the men confesses a crime, blames himself for the disaster and commits suicide. They end up spending a fortnight underground - and at last are saved. Capi is mad with happiness; Mattia is in tears. He says he never believed that Rémi could be dead, and Rémi is proud of his friend's strong belief in him. This incident shows the terrible state of child labour in 19th Century France.

Rémi wants Mattia to learn music and they visit a barber/musician. Mr. Espinassous is amazed by Mattia's great talent and tries to convince him to stay and learn, but Mattia does not want to leave Rémi.

==== A cow for Mother Barbarin ====
The boys now head towards their visit to Rémi's foster mother. First they decide to visit Clermont Ferrand, and south westerly, the mineral bath towns Saint Nectaire, Mont-Dore, Royat and Bourboule where they can make good money, towards the cow for mother Barberin. When they pass through Ussel, not far from Chavanon, they make sure that they will not buy a bad cow, and ask a vet for help. The vet is very friendly and the boys buy a wonderful cow.

In the next town the boys are accused of stealing the cow. Why would two street musicians have a cow, after all? They explain their story to the mayor. The mayor knows Mother Barberin, he heard about the accident in the mine, and he is willing to believe that the boys are honest. To make sure, the vet is called to testify, and the boys can continue their journey.

Rémi and Mother Barberin finally meet again. Mother Barberin tells Rémi that Barberin is in Paris in search of Rémi, because his real parents appear to be in search of him. However, Mother Barberin knows very little, because Barberin never told her any details. Rémi is eager to know his real parents. Rémi and Mattia decide to return to Paris and find Barberin. On the way to Paris, they pass through Dreuzy, where they pay a visit to Lise Acquin. Rémi and Lise are very fond of each other.

==== The Driscolls ====
When the boys arrive in Paris, they learn that Barberin has died. Rémi writes a letter to Mother Barberin. Mother Barberin replies and she encloses a letter that was sent by Barberin before he died. It mentions the address of the lawyer's office in London, in charge of the search for Rémi. So the boys take a boat to London, where they are led straight to Rémi’s parents. Their name is Driscoll.

Rémi is terribly disappointed: the Driscolls are cold to him; his father keeps the boys locked up. They turn out to be a band thieves and use Capi to help them in their work.
The Driscolls receive a visitor, a man who seems interested in Rémi, but Rémi does not understand English well enough. The visitor does not meet Mattia, but Mattia overhears their conversation. James Milligan appears to be Arthur's uncle. He hopes that Arthur will die, so that he will inherit the fortune of his late brother. The boys agree that Mrs. Milligan must be warned, but they have no idea where to find her. Mattia meets Bob a clown/musician from the circus. Bob turns out to be a fine friend.

==== Searching for the Milligans ====
When Rémi is accused of a robbery committed by his parents, he and Mattia help him escape from prison. With the help of Bob's brother, a sailor, they return to France to search for Mrs. Milligan, in order to warn her about her brother-in-law. They start by going up the Seine, since the "Swan", is a remarkable boat; they soon hear that people have seen her. They follow the trail along rivers and canals.

On their way they pass through Dreuzy where they hope to meet Lise again. However, they hear that Lise's uncle has died, and that a kind English lady, who journeyed on a boat, has offered to take care of Lise. Mrs. Milligan! Rémi and Mattia trace the "Swan" across France to near the Swiss border. They find the boat deserted, finding out that it was unable to journey further up the river, and the family continued their journey by coach, probably to Vevey. When they get to the town where "the English woman with the ill boy and the mute girl" are supposed to be, they start singing near every fence. It takes several days.

One day, Rémi sings his Neapolitan song, and hears a scream and a weak voice that continues the song. They run to the voice and find Lise, whose voice has returned to her when she heard her long-lost Rémi. The boys now find that James Milligan is there too, and Rémi is afraid to meet him, but James does not know Mattia so he is able to tell Mrs. Milligan their story. Mrs. Milligan guesses that Rémi might be her lost eldest son, but tells Mattia to keep it secret until she is sure. She arranges for the boys to stay in a hotel, where they can have plenty of food, comfortable beds, and are visited by a barber and a tailor.

==== Home at last ====
After a few days Mrs. Milligan invites the boys to her villa where they meet Mother Barberin, whom Mrs. Milligan sent for. Mother Barberin shows Rémi’s baby clothes which Mrs. Milligan recognises as the clothes her boy wore when he was stolen. Mrs. Milligan happily declares that Rémi is her son, to join his "mother, brother and those - she pointed at Lise and Mattia - who loved you in your misery". It is clear that Mr. Driscoll had stolen the boy as a job for James Milligan.

This story has a happy ending: Rémi finds his family, and discovers he is the heir of a fortune. Mattia's dear little sister Cristina is sent for from Italy and they all grow up together. Arthur gets well and becomes a gentleman athlete, Mattia a famous violinist. Rémi marries Lise and they have a son named Mattia, and Mother Barberin becomes his nanny.

The book ends with the score of the Neapolitan song.

==Film and television==
Several [[Film|movie]]s were made after the novel:
* ''Sans famille'' (F 1934)<ref>http://www.imdb.com/title/tt0025744/</ref>
* ''Senza famiglia'' (I 1946)<ref>http://www.imdb.com/title/tt0037259/</ref>
* ''Le Théâtre de la jeunesse: Sans famille'' (made for TV, F 1965)<ref>http://www.imdb.com/title/tt0207720/</ref>
* ''[[Nobody's Child (Hong Kong film)]]'' ('''苦兒流浪記''', 1961)<ref>http://www.lcsd.gov.hk/CE/CulturalService/HKFA/form/7-2-1.pdf</ref>
* ''[[Chibikko Remi to Meiken Kapi]]'' (Japan 1970) – feature film by [[Toei Animation]]
* ''[[Nobody's Boy: Remi]]'' (''Ie Naki Ko'') (Japan 1977-78) – 51-episode [[anime]] TV series by [[Tokyo Movie Shinsha]]
* ''{{Interlanguage link multi|Sans famille (1981)|fr|3=Sans famille (téléfilm, 1981)|lt=Sans famille}}'' (France 1981) – 6-part TV series by TF1 starring [[Petula Clark]] and [[Fabrice Josso]]<ref>http://www.imdb.com/title/tt0081926/</ref>
* ''Bez semyi'' (Soviet Union 1984) {{ru icon}}<ref>[[:ru:Без семьи (фильм, 1984)]]</ref>
* {{Nihongo|''[[Remi, Nobody's Girl]]''|家なき子レミ|Ie Naki Ko Remi}} (Japan) – 26-episode anime TV series, the final installment in [[Nippon Animation]]'s ''World Masterpiece Theatre'' series. This version made major changes to the storyline, transforming Remi (voiced by legendary pop star/voice actress [[Mitsuko Horie]]) into a girl and making her a child singer. The series was later translated into English by the anime television network, [[Animax]], who aired the complete series (all 26 episodes) across its respective networks worldwide, including [[Southeast Asia]] and [[South Asia]]<ref>http://pollyanna.fjcu.org/year.html</ref>
* ''Sans famille/Das Findelkind'' (F/D/CZ, 2000)<ref>http://www.imdb.com/title/tt0261964/</ref>

==References==
{{Reflist}}

==External links==
{{Commons category|Sans Famille}}
*{{fr icon}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/malot-hector-sans-famille.html/ Sans Famille, audio version] [[Image:Speaker Icon.svg|20px]]
*{{librivox book | title=Nobody's Boy | author=Hector Malot}}  

[[Category:1878 novels]]
[[Category:French bildungsromans]]
[[Category:19th-century French novels]]
[[Category:Works by Hector Malot]]
[[Category:Novels about orphans]]
[[Category:Novels set in France]]

[[ru:Без семьи (фильм, 2000)]]