{{good article}}
{{infobox military conflict
| conflict    = 2nd Parachute Brigade in Southern France
| partof      = [[Operation Dragoon]]
| image       = [[File:Anvildragoon.png|300px]]
|caption     = Map of the Dragoon landings, the airborne landings Operation Rugby highlighted in red
| date        = 15–26 August 1944
| place       = [[Southern France]]
| coordinates =  {{coord|43|28|19|N|6|33|59|W|region:FR_type:landmark|display=title}}
| territory   =
| result      = Allied success
|  status      =
| combatant1  = {{flag|United Kingdom}}
| combatant2  = {{flag|Nazi Germany}}
| combatant3  =
| commander1  =Charles Hilary Vaughan Pritchard
| commander2  =[[Johannes Baessler]]
|commander3  =
| units1      =[[2nd Parachute Brigade (United Kingdom)|2nd Parachute Brigade]]
| units2      =[[242nd Infantry Division (Wehrmacht)|242nd Infantry Division]]
| units3  =
| casualties1 =51 dead<br>130 wounded<br>181 missing ^
| casualties2 =Unknown but around 350 prisoners taken<ref name=haggerman119/>
| casualties3=
| notes       =^ Some of the missing later reported for duty
| campaignbox ={{Campaignbox British airborne forces operations}}
}}

The British [[2nd Parachute Brigade (United Kingdom)|2nd Parachute Brigade]] was part of the [[Operation Rugby]] airborne landings in August 1944. The operation was carried out by an ad hoc airborne formation called the [[1st Airborne Task Force]]. Operation Rugby was itself part of the [[Operation Dragoon]] invasion of Southern France by the American [[United States Army Europe|7th Army]]. The airborne task force landed in the [[River Argens]] valley with the objective of preventing German reinforcements from reaching the landing beaches. The landings were mainly an American operation and the [[brigade]] was the only [[British Army]] formation involved.<ref name=rottman16/>

The brigade's [[Pathfinder (military)|pathfinders]] landed accurately and set up their homing beacons, but the main body of the brigade landed over a large area, some {{convert|20|mi}} away. The following [[military glider|glider]] force also had problems; weather conditions forced the brigade's anti-tank unit to turn back for Italy and  return with the second wave later that day. Eventually the brigade captured their primary objectives, but the lack of manpower meant that their secondary objective of Le Muy was given to the American airborne forces. The seaborne and airborne landings linked up within two days, and the brigade became the reserve formation for the operation.

As a British withdrawal from France at the earliest opportunity had been the original intent, the brigade boarded ships bound for Italy eleven days after landing. The operation had cost the brigade 362 casualties, but this did not hinder their next mission in Greece two months later. Only 126 replacements were required to bring the brigade back up to full strength.

==Background==

===2nd Parachute Brigade===
[[File:4 Para mortar team Italy 1944.jpg|thumb|alt=Four men in berets and shirtsleeves in a mortar pit|[[4th Parachute Battalion]] [[mortar (weapon)|mortar]] team in action, Italy 1944]]

Commanded by [[Brigadier]] Charles Hilary Vaughan Pritchard, the [[2nd Parachute Brigade (United Kingdom)|2nd Parachute Brigade]] formed in 1942 and was assigned to the [[1st Airborne Division (United Kingdom)|1st Airborne Division]].<ref name=tugwell222>Tugwell, p.222</ref> It comprised three parachute battalions, with their own artillery and engineer support.<ref name=rottman16/> Although the brigade had not been involved in the division's operations in Sicily, it took part in [[Operation Slapstick]], the landing at [[Taranto]] in Italy. At the end of 1943, when the 1st Airborne Division returned to the United Kingdom, the brigade remained behind fighting in the [[Italian Campaign (World War II)|Italian Campaign]].<ref name=rottman15/> Since then, the brigade had taken part in patrols probing German lines, and the battles at [[Battle of Cassino|Cassino]] and along the [[Sangro]] river at the [[Bernhardt Line]]. Previously, their only parachute mission had been [[Operation Hasty]], which required only sixty men from the brigade.<ref>Tugwell, pp.222–223</ref>

===Operation Dragoon===
The Allied invasion of [[Southern France]] during the Second World War was given the [[code name]] [[Operation Dragoon]]. It would take place in the South of France, between [[Cannes]] and [[Toulon]] in August 1944. The landings would be undertaken by the [[Seventh United States Army|U.S. Seventh Army]], with the [[First Army (France)|French First Army]] landing after the beachhead was secure.<ref>Zaloga, p.26</ref> Following the success of the [[Normandy landings|Normandy airborne landings]], [[Allies of World War II|Allied]] planners wanted to use an airborne force to support the invasion.<ref name=rottman14/> However the divisions in Europe, the American [[82nd Airborne Division|82nd]] and [[101st Airborne Division]], had participated in the [[Normandy landings]] and were refitting for further operations to support the advance in France and the [[Low Countries]].<ref name=rottman14/> The two British divisions were also unavailable; the 1st Airborne Division was preparing for operations in Northern Europe and the [[6th Airborne Division (United Kingdom)|6th Airborne Division]] was still fighting in Normandy.<ref name=rottman14>Rottman, p.14</ref>

The only other Allied airborne divisions were three American units. However, the [[11th Airborne Division (United States)|11th Airborne Division]] was fighting in the [[Pacific War]], while the [[13th Airborne Division (United States)|13th]] and [[17th Airborne Division (United States)|17th Airborne Divisions]] were back in the United States training.<ref name=rottman14/>

==Prelude==

===1st Airborne Task Force===
Lack of an available airborne division forced the Allies to assemble an ad hoc formation, the [[1st Airborne Task Force]], in Italy on 1&nbsp;July&nbsp;1944.<ref name="Warren">Warren, p.90</ref> Its objective was to land in the [[River Argens]] valley between [[Le Muy]] and [[Le Luc]] to prevent any German reinforcements reaching the landing beaches.<ref>Wright and Greenwood, p.42</ref>

The task force initially comprised the American [[517th Parachute Regimental Combat Team]], reinforced by the [[509th Parachute Infantry Battalion|509th]], and the [[551st Parachute Infantry Battalion (United States)|1st/551st]] parachute infantry battalions and their only [[glider infantry]] battalion, the [[550th Airborne Infantry Battalion (United States)|550th]]. Command of the task force was given to [[Brigadier General]] [[Robert T. Frederick]], who had previously commanded the [[1st Special Service Force]].<ref name=rottman16/> To provide additional manpower, the experienced 2nd Parachute Brigade, was attached to the task force. But they would only stay in France until the beachhead had been established and the seaborne and airborne elements had linked up.<ref name=rottman15>Rottman, p.15</ref><ref>Warren, p.91</ref> Initially two Free French parachute battalions had been assigned to the task force, but they were withdrawn for a potential French airborne landing in the [[Massif Central]]. When the Americans and British refused to endorse the French plan, as it offered no support to operations in Normandy or to Dragoon, the French commander [[Charles de Gaulle|General de Gaulle]] would not allow them to be used in the landings.<ref name=warren82/>

In the initial plan for Operation Rugby, the codename for the airborne landings, the 2nd Parachute Brigade would land to the west of Le Muy, only {{convert|10|mi}} from the coast. The American 517th Regimental Combat Team would land beside Le Luc, and the 509th and 551st Battalions near [[Carnoules]]. They would then hold the high ground and junctions along the main road through the Argens valley from [[Frejus]] to [[Toulon]]. This plan had the units widely separated and any  German resistance or counterattack could cut off and destroy the task force piecemeal before help arrived from the landing beaches.<ref>Warren, p.81</ref>

A revised plan produced on 12&nbsp;July had the 2nd Parachute Brigade landing at the same place, but all the Americans would land at a large [[drop zone]] between Le Muy and [[Grimaud, Var|Grimaud]], only {{convert|2|mi}} to the west of [[St Tropez]].<ref name=warren82>Warren, p.82</ref> The task force's staff were still not satisfied with the mission and on 15&nbsp;July proposed a third plan. The new plan placed all the task force drop zones around {{convert|2|mi}} from Le Muy. In the first wave of landings, the 2nd Parachute Brigade drop zone was to the north of Le Muy, with their objective to capture Le Muy and the surrounding area including the village of [[La Motte, Var|La Motte]]. At the same time the 517th Regiment would land to the west of Le Muy and the 509th Battalion to the southeast.<ref name=warren83>Warren, p.83</ref><ref>Harclerode, pp.414–415</ref>

A shortage of aircraft played a role in transporting the task force to France, which had to be divided into two large groups and a small glider group for the British.<ref>Warren, pp.81–83</ref><ref name=harclerode423/> On [[D-Day (military term)|D-Day]] the first large group would be transported to France by 394 aircraft and eighty gliders. A second group of 325 aircraft and 270 gliders would arrive in the afternoon.<ref>Warren, pp.81 and 84</ref> The smaller British group consisting of the 2nd Parachute Brigade's glider forces would arrive shortly after the first group in the morning.<ref name=harclerode423/> In total 9,099 troops, 221 vehicles and 213 artillery guns would be delivered to France during Operation Rugby.<ref>Wilson, pp.107–108</ref>

===German forces===

The German forces in the area of the landings were under the command of [[Army Group G]],<ref>Tugwell, p.223</ref> and also divided between the [[1st Army (Wehrmacht)|1st Army]] in the west and the [[19th Army (Wehrmacht)|19th Army]] in the east. In June 1944, these three headquarters commanded three [[panzer division]]s, around thirteen infantry divisions and some smaller formations.<ref>Clarke et al, p.59</ref> By this stage of the war, many of these divisions were lacking modern equipment, inexperienced and under-trained. The troops were mainly elderly, or not fit for service in a first class division. Some divisions had several complete battalions formed by the [[Ostlegionen]] (conscripted foreigners from Poland and Russia).<ref>Clarke et al, p.60</ref> Four of the divisions were static or garrison divisions and five were reserve formations that had only been used in an [[internal security]] role against the [[French Resistance]].<ref>Clarke et al, p.61</ref>

After the [[Normandy landings]] many of these units moved north. Replacements that joined Army Group G came to refit after the fighting in Normandy or on the [[Eastern Front (World War II)|Eastern Front]]. They were under strength, with little or no equipment.<ref name=clarke65/>

By August 1944, the 19th Army comprised the [[11th Panzer Division (Wehrmacht)|11th Panzer Division]], the [[198th Infantry Division (Wehrmacht)|198th]], [[242nd Infantry Division (Wehrmacht)|242nd]], [[244th Infantry Division (Wehrmacht)|244th]], [[338th Infantry Division (Wehrmacht)|338th]], [[716th Static Infantry Division (Wehrmacht)|716th]] infantry divisions and the [[148th Reserve Division (Germany)|148th]], [[8th Mountain Division|157th]] and [[189th Infantry Division (Wehrmacht)|189th]] reserve divisions, all of which were still under strength and lacking equipment.<ref>Kaufmann and Kaufmann, p .282</ref> In the Argens valley area, where the 2nd Parachute Brigade would be landing, was the 242nd Infantry Division commanded by [[Generalleutnant]] [[Johannes Baessler]].<ref name=clarke65>Clarke et al, p.65</ref>

==Operation Rugby==

===D-Day===
[[File:Two USAAF C-47A Skytrains.jpg|thumb|Paratroop carrying [[Douglas C-47 Skytrain|C-47s]] en route to the South of France for [[Operation Dragoon]]]]

At 03:34 on 15 August 1944, the brigade's [[Pathfinder (military)|pathfinders]] from the 23rd Independent Parachute Platoon landed on Drop Zone 'O'.<ref name=haggerman116/> The brigade drop zone was in a valley {{convert|2|mi}} long and around {{convert|1.5|mi}} wide running north to south, bordered on the south by the [[River Naturby]]. Their primary objective, Le Muy, was only around {{convert|400|yd}} from the southern edge of the drop zone, but they would have to cross the river to reach it.<ref name=warren83/>

By 04:30 the pathfinders had set up two [[Rebecca/Eureka transponding radar|Eureka beacons]] and landing lights to guide the transport planes to the correct drop zone.<ref name=haggerman116>Haggerman, p.116</ref>{{#tag:ref|Eureka beacons had a maximum range of {{convert|42|mi}}, but their average range was only {{convert|24|mi}}.<ref name="Warren" />|group=nb}} When the main force of [[C-47|C-47 aircraft]] arrived around 04:45 only seventy-three dropped their parachutists on the correct drop zone. The other fifty-three aircraft scattered their loads, mostly from the [[5th (Scottish) Parachute Battalion]], over the countryside, some landing {{convert|20|mi}} away at [[Cannes]] and [[Fayence]].<ref>Tugwell, p.224</ref>{{#tag:ref|The lead plane carrying the 5th (Scottish) Parachute Battalion had an electrical failure and was unable to pick up the Eureka signals,<ref name=saunders271/> and only 'B' Company landed on the correct drop zone.<ref name=saunders271/>|group=nb}} Only Brigade Headquarters arrived at the drop zone in one piece, Brigadier Pritchard landing only {{convert|15|yd}} from the Eureka beacons.<ref name=saunders271>Saunders, p.271</ref> The [[4th Parachute Battalion]] had around forty per cent of its manpower and the [[6th (Royal Welch) Parachute Battalion]] sixty per cent.<ref name=harclerode422>Harclerode, p.422</ref> By 06:30 Brigade Headquarters was established at [[Le Mitan]] and radio contact made with the American [[36th Infantry Division (United States)|36th Infantry Division]], which at the time was still on their transports out at sea. On the drop zone the pathfinders and [[Royal Engineers]] of the 2nd Parachute Squadron were removing obstacles, preparing for the arrival of their [[military glider|gliders]] later in the day, while the under strength parachute battalions headed for their objectives.<ref name=harclerode422/>

The 4th Parachute Battalion secured its first objectives, the high ground overlooking the village of Le Muy from the north and the village of [[Les Serres]], by 04:30. However the men left defending the village had to fight off several small German counterattacks throughout the day. The battalion then assaulted and captured the bridge over the River Naturby, which carried the road to Le Muy, taking twenty-nine prisoners. The fighting cost the battalion seven dead and nine wounded.<ref name=harclerode423/>

Most of the 5th (Scottish) Parachute Battalion landed in several groups, some distance from their drop zone. One large group comprised the [[commanding officer]], most of the battalion headquarters and 'C' Company. A second group was formed by 'D' Company. Twenty-one men, including the battalion intelligence officer, formed a third smaller group.<ref name=harclerode423/> The first group divided into three smaller units, all of which arrived at brigade headquarters by 22:30 on the same day.<ref name=harclerode424>Harclerode, p.424</ref>

The second group occupied the village of [[Tourrettes, Var|Tourettes]], and soon after attacked a German convoy, damaging several vehicles and causing heavy casualties among the occupants. Then they met with the 3rd Battalion of the 517th Parachute Infantry Regiment and both units headed south towards Le Muy.<ref name=harclerode424/>

[[File:AWM SUK12892 Airborne Southern France.jpg|thumb|British Gliders towed by C47s over Southern France]]

One result of the brigades' scattered landing was that several small groups of men caused havoc among the Germans, ambushing men and vehicles. One lone sergeant arrived at brigade headquarters with eighty prisoners, having convinced them that they were surrounded and should surrender.<ref>Harclerode, pp.424–425</ref> The 5th (Scottish) Parachute Battalion's 'B' Company, which had landed on the correct drop zone, ambushed a convoy of five trucks, killing twelve Germans, then assaulted a château, killing another thirty men before the surviving fifty surrendered.<ref>Harclerode, p.425</ref>

The 6th (Royal Welch) Parachute Battalion lost some men to German fire as they descended and, once on the ground, had to fight their way to the battalion rendezvous. Then headed for their first objectives capturing [[La Motte, Var|La Motte]], which became the first village liberated in the South of France. By 12:00, they had taken all of their objectives, capturing over 100 prisoners.<ref>Harclerode, pp.425–426</ref>

The first of the brigade's glider units, forty [[Waco glider]]s carrying the 64th Airlanding Light Battery [[Royal Artillery]] and their [[M116 howitzer|75&nbsp;mm pack howitzers]], landed at 09:20. The thirty-five larger [[Airspeed Horsa|Horsa gliders]], transporting the 300th Airlanding Anti-Tank Battery and its equipment, failed to arrive, turning back to Italy due to an overcast sky at the landing zone.<ref name=harclerode423>Harclerode, p.423</ref> The Horsas returned with the afternoon's second wave landing at 15:04.<ref>Wilson, p.103</ref>

By 22:15 the brigade had secured all of its day one objectives and some of the missing men had arrived in the brigade area. However, there were many still unaccounted for. The 4th Parachute Battalion numbered around 200 men with almost no heavy weapons. The 6th (Royal Welch) Parachute Battalion had 317 men. There were only enough men of the 5th (Scottish) Parachute Battalion to guard the approach roads leading to the drop zone from the north.<ref>Harclerode, p.426</ref>

===Link up with seaborne landings===

The scattered nature of the airborne landings caused some confusion among the German high command. At the [[LXII Corps (Germany)|LXII Corps]] headquarters, several reports of the landings exaggerated their strength, causing the Germans to believe they were faced with a far greater force.<ref>Harclerode, pp.428–429</ref> Also, 600 dummy paratroops that had landed to the north and west of [[Toulon]], as part of the Allied deception plan, convinced the Germans another landing had taken place there.<ref>Harclerode, p.429</ref>

The 2nd Parachute Brigade had a secondary objective of capturing Le Muy. It was supposed to have been assaulted by the 4th Parachute Battalion with the 5th (Scottish) Parachute Battalion providing fire support. With the forces he had at hand, Pritchard was unable to carry out the task.{{#tag:ref|Pritchard had also been ordered by the commander of the [[15th Army Group]] [[General]] [[Harold Alexander]] to keep his casualties to a minimum, as the brigade was needed for another parachute mission in Greece ([[Operation Manna]]) soon afterwards.<ref>Harclerode, p.432</ref>|group=nb}} The objective was then given to the 550th Glider Infantry Battalion, which had arrived with the second wave of aircraft. At 02:15 16 August the American battalion moved through the British positions and assaulted the village. The village was defended in strength by the Germans and the attack failed, causing several casualties to the Americans. A second attempt at 11:40 was more successful, killing or wounding 300 Germans and taking 700 prisoners.<ref>harclerode, pp.432–434</ref>

Further north, the missing men of the 5th (Scottish) Parachute Battalion were still trying to reach the brigade area. The smaller group commanded by the battalion intelligence officer observed a German convoy heading south towards Le Muy. They quickly set up an ambush and destroyed several vehicles before being forced to withdraw into the hills by the stronger German force.<ref>Harclerode, p.433</ref>

On 17 August the leading units of the 36th Infantry Division reached Le Muy, which should have effectively ended the brigade's participation in the operation; however, two of the brigade's companies defending roads in the area were attacked by retreating Germans and forced to withdraw to the high ground. The brigade sent reinforcements which successfully counterattacked the Germans, taking ninety-seven prisoners.<ref>Harclerode, p.437</ref>

Early on 18 August the brigade elements to the north-east of Le Muy were relieved by the 36th Infantry Division,<ref>Warren, p.110</ref> and the brigade became the Operation Dragoon reserve formation. Then problems on the far right of the landings between [[Grasse]] and [[Cannes]] resulted in the brigade being sent to that area. Having no transport other than a small number of [[Willys MB|Jeeps]], mostly to tow their artillery guns, the brigade commandeered several trucks, tractors, buses and horse-drawn wagons and was in place within twenty-four hours.<ref>Harclerode, pp.427–438</ref> Cannes was liberated on  25&nbsp;August. On the next day, the brigade left France to return to Italy.<ref>Harclerode, p.439</ref>

==Aftermath==
[[File:Paras from 5th (Scots) Parachute Battalion, 2nd Parachute Brigade, take cover on a street corner in Athens during operations against members of ELAS, 6 December 1944. NA20515.jpg|thumb|Men of the [[5th (Scottish) Parachute Battalion]] in Athens, December 1944]]

The 2nd Parachute Brigade arrived by sea at [[Naples]] on 28&nbsp;August. From there they moved to Rome on 3 September, and on 8&nbsp;September, to a camp near Taranto in preparation for their next mission.<ref name=cole65/> During Operation Rugby the 2nd Parachute Brigade recorded 362 casualties; 51 dead, 130 wounded and 181 missing. Most of the missing eventually returned to the brigade and only 126 replacements were required to bring the brigade back up to strength.<ref name=haggerman119>Haggerman, p.119</ref>

The brigade's next mission was [[Operation Manna]]. This time they would be landing in Greece.<ref name=cole65>Cole, p.65</ref> The [[Soviet Union|Soviet]] [[Red Army]] advance in the east had forced the Germans to withdraw from the country or risk being cut off from any support or reinforcements.<ref name=ferg14>Ferguson, p.14</ref> Over the night of 12/13 October 1944, the first of the brigade's units parachuted onto [[Megara]] airfield near [[Athens]] to prepare a landing zone for the follow-up waves from the rest of the brigade.<ref>Cole, p.67</ref> The brigade moved into Athens and then over three months fought their way to [[Salonika]] and the border with [[Bulgaria]].<ref>Ferguson, p.15</ref>

When the brigade left the 1st Airborne Task Force, the 1st Special Service Force replaced them.<ref name=haggerman119/> The task force advanced eastwards and reached the Franco-Italian border in the [[Menton]] area and the French [[Alps]] along part of what was the [[Maginot line]]. Here they halted in a defensive position for three months before being pulled out. The task force was now surplus to requirements and was disbanded on 23 November 1944. Its surviving manpower was used as reinforcements for the other American airborne formations.<ref>Werner, p.26</ref>

==Brigade order of battle==
* Brigade headquarters
* [[4th Parachute Battalion]]
* [[5th (Scottish) Parachute Battalion]]
* [[6th (Royal Welch) Parachute Battalion]]
* [[127th (Parachute) Field Ambulance]]
* 64th Airlanding Light Battery [[Royal Artillery]]
* 300th Airlanding Anti-Tank Battery Royal Artillery
* 2nd Parachute Squadron [[Royal Engineers]]
* 751st Parachute Brigade Company [[Royal Army Service Corps]]
* 2nd Parachute Brigade Group Workshop [[Royal Electrical and Mechanical Engineers]]
* 2nd Parachute Brigade Group Signals [[Royal Corps of Signals]]
* 1st Independent [[Glider Pilot Regiment|Glider Pilot Squadron]] [[Army Air Corps (United Kingdom)|Army Air Corps]]
* 23rd Independent Parachute Platoon <ref name=rottman16>Rottman, p.16</ref>

==Notes==
;Footnotes
{{reflist|group=nb}}
;Citations
{{reflist|3}}

==References==
{{refbegin}}
* {{cite book|title=German Infantry in World War II|first=Chris|last=Bishop|publisher=MBI Publishing Company|year=2008|location=Norwark, CT|isbn=978-0-7603-3187-3}}
* {{cite book|title=The Encyclopedia of Codenames of World War II|first=Christopher|last=Chant|publisher=Routledge|location=Oxford|year=1986|isbn=978-0-7102-0718-0}}
* {{cite book|first1=Jeffrey J.|last1=Clarke|first2=Robert Ross|last2=Smith|title=United States Army in World War II European Theater of Operations|publisher=Diane Pub Co|year=1993|location=Darby, PA|isbn=978-0-7567-6486-9}}
* {{cite book|last=Cole|first=Howard N|title=On Wings of Healing: the Story of the Airborne Medical Services 1940–1960|publisher=William Blackwood|location=Edinburgh|year=1963|oclc=29847628}}
* {{cite book|last=Ferguson|first=Gregor|title=The Paras 1940–84|series=Volume 1 of Elite series
|publisher=Osprey Publishing|location=Oxford|year=1984|isbn=0-85045-573-1}}
* {{cite book|title=USA Airborne:50th Anniversary|first=Bart|last=Hagerman|publisher=Turner Publishing Company|location=Nashville, TN|year=1990|isbn=978-0-938021-90-2}}
* {{Cite book|last=Harclerode|first=Peter|title=Wings Of War&nbsp;– Airborne Warfare 1918–1945|publisher=Weidenfeld & Nicolson|location=London|year=2005|isbn=0-304-36730-3}}
* {{cite book|title=The American GI in Europe in World War II|first1=J. E.|last1=Kaufmann|first2=H. W.|last2=Kaufmann|publisher=Stackpole Books|location=Mechanicsburg, PA|year=2009|isbn=978-0-8117-0526-4}}
* {{cite book|title=US Airborne Units in the Mediterranean Theater 1942–44|issue=22 of Battle Orders|first=Gordon L|last=Rottman|publisher=Osprey Publishing|location=Oxford|year=2006|isbn=978-1-84176-920-2}}
* {{cite book|last=Saunders|first=Hilary St George|title=The Red Beret|publisher=New English Library|location=London|year=1971|isbn=978-0-450-01006-4}}
* {{cite book|last=Tugwell|first=Maurice|title=Airborne to Battle a History of Airborne Warfare1918–1971|publisher=William Kimber|year=1971|location=London|isbn=978-0-7183-0262-7}}
* {{cite book|title=Airborne Missions in the Mediterranean, 1942–1945 |first=John C|last=Warren |year=1955|publisher=USAF Historical Study No. 74|location=Maxwell AFB, AL|isbn=978-0-89126-023-3}}
* {{cite book|title=First Special Service Force 1942–44|series=Issue 145 of Elite Series|first=Bret|last=Werner|publisher=Osprey Publishing|location=Oxford|year=2006|isbn=978-1-84176-968-4}}
* {{cite book|title=Airborne Forces at War: from Parachute Test Platoon to the 21st Century|first1=Robert K|last1=Wright|first2=John T|last2=Greenwood|publisher=Naval Institute Press|location=Annapolis, MA|year=2007|isbn=978-1-59114-028-3}}
* {{cite book|title=Operation Dragoon 1944: France's Other D-Day|series=Campaign Series|first=Steven|last=Zaloga|publisher=Osprey Publishing|year=2009|location=Oxford|isbn=978-1-84603-367-4}}
{{refend}}

[[Category:Glider Pilot Regiment operations]]
[[Category:Battles and operations of World War II involving Germany]]
[[Category:Military units and formations of the British Army in World War II]]
[[Category:Airborne operations of World War II]]
[[Category:Parachute Regiment (United Kingdom)]]
[[Category:1944 in France]]
[[Category:Conflicts in 1944]]