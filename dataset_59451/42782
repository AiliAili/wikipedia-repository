<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Feiro I
 | image=FEIRO-I.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Four seat civil transport
 | national origin=[[Hungary]]
 | manufacturer=Feigl and Rotter
 | designer=Lajos Rotter
 | first flight=late 1923-early 1924
 | number built=1
 | developed into= Feiro Daru
 | variants with their own articles=
}}
|}

The 1923 '''Feiro I''' was the first [[Hungary|Hungarian]] designed and built civil transport aircraft, modified in 1925 by an engine change into the '''Feiru Daru''' ([[Crane (bird)|Crane]]). Neither was a commercial success.

==Design and development==
The Feiro I was the first design of Lajos Rotter in his collaboration with the brothers Gyula and László Feigl.<ref name=Rotter/> It was also the first civil transport to be designed in Hungary, flying in the winter of 1923-4. It had four seats and was powered by a {{convert|120|hp|kW|abbr=on|0}} [[Le Rhone 9J]] [[rotary engine]], though it was intended that this would be replaced by [[Haake (engines)|Haake]] or [[Siemens-Schuckert|Siemens-Halske]] [[radial engine]]s of similar power in production aircraft.<ref name=Flight/>

It was a [[high wing]] [[monoplane]], with an aerodynamically thick (thickess/[[chord (aeronautics)|chord ratio]] 14%) [[Nikolay Yegorovich Zhukovsky|Joukowski-Göttingen]] [[Joukowsky transform|"tadpole shaped"]] [[airfoil]] over the whole span.  The two piece, [[plywood|3-ply]] covered wing was built around twin [[spruce]] [[spar (aeronautics)|flanged box spars]] with 3-ply webs.  In plan it had constant chord and was unswept; the [[wingtip]]s were angled and the short [[aileron]]s tapered slightly outboard. Each wing was braced to the [[fuselage]] with a parallel pair of airfoil section struts from the wing spars to the lower fuselage [[longeron]]s.<ref name=Flight/><ref name=Laero/>

Behind the engine the fuselage was rectangular in cross-section, with four longerons and 3-ply covered. The Le Rhone rotary was partially enclosed within an open-bottomed [[aircraft fairing#Engine cowlings|engine cowling]] and was mounted on steel tube bearings. Aluminium sheet, rounded on the upper surface, covered the fuselage rearwards to the cabin.  There were two firewalls between engine and cabin and the [[carburetter]], gravity-fed fuel from a tank in the central wing, was placed in the ventilated space between them.<ref name=Flight/>

The deep cabin of the Feiro I had two rows [[tandem#Side-by-side seating|side-by-side seats]]; the front pair could both be equipped with flight controls or one of then could serve as a third passenger seat. Because the high engine fairing reached the underside of the wing [[leading edge]], there was no central forward view from the controls; instead, there were deep openings on either side.  Access to the cabin was via a port side door. Behind the cabin the fuselage tapered to the tail where a short, broad [[fin]] carried a deep [[rudder]]. The [[tailplane]] was also mounted on the fin, just above the fuselage so that it could be used to trim the aircraft by altering its [[angle of incidence (aerodynamics)|angle of incidence]]. It had swept leading edges, square tips and carried [[Elevator (aeronautics)|elevator]]s with a cut-out for rudder movement. The fixed tail surfaces were ply skinned and the control surfaces [[aircraft fabric covering|fabric covered]].<ref name=Flight/>

The Feiro I had a [[landing gear|tailskid undercarriage]] with mainwheels {{convert|2|m|ftin|abbr=on|0}} apart and rubber sprung on a single axle, its ends supported by longitudinal V-struts and positioned laterally by a steel V-strut; all struts were from the lower fuselage longerons.<ref name=Flight/><ref name=Lailes2/>

The first flight was late in 1923 or in January 1924, though the exact date is not known. Some testing had been done by mid-February, establishing good handling and a take-off distance of around {{convert|150|ft|m|abbr=on|order=flip|0}} but detailed performance figures had yet to be established.<ref name=Flight/> By March an estimated maximum speed of {{convert|160|km/h|mph|abbr=on|0}} had been confirmed.<ref name=Laero/> Feiro's resources were limited and they had difficulty obtaining some important raw materials, even in small quantities, receiving no public support for the Feiro I's development.<ref name=Lailes2/> Hungary's manufacturers lost many of their material suppliers when the country's boundaries were shrunk by the [[Treaty of Trianon]] after the end of [[World War I]].

Despite these problems, two years later and after their innovative high [[aspect ratio (wing)|aspect ratio]] [[biplane]] [[trainer aircraft]], the [[Feiro Dongó]], Feiro flew a Feiro I production development, largely unchanged but with a more modern, {{convert|180|hp|kW|abbr=on|0}} [[Hispano-Suiza 8A]] water-cooled [[V-8 engine]] in a revised nose which provided better forward visibility through a front-facing windscreen.<ref name=Lailes2/> This was given the name '''Feiro Daru'''. Other differences between it and the Feiro I were the addition of about 2º of sweep to the wing, moving the [[center of pressure (terrestrial locomotion)|center of pressure]] rearwards, and a revised tail similar in shape to that used on the Feiro Dongó, with a straight edged [[fin]] and deep, curved, [[balanced rudder]]. Its empty weight was increased by {{convert|70|kg|lb|abbr=on|0}} but its useful load also increased by {{convert|30|kg|lb|abbr=on|0}}. It was faster, with a maximum speed increased by {{convert|10-20|km/h|mph|abbr=on|0}}, climbed to {{convert|1000|m|ft|abbr=on|0}} in 8 minutes and had a ceiling of {{convert|1000|m|ft|abbr=on|0}}.<ref name=Lailes2/> Despite the improvements the Daru still failed to find customers and only one was built; this may have originally been the sole Feiro I.

After these commercial failures with powered designs, Rotter became instead a successful glider pilot and designer. Flying his first such design, the 1933 [[Karakan]], Rotter became Hungary's first "Silver C" glider pilot.<ref name=SimonsI/>

==Variants==
[[File:FEIRO-Daru.jpg|thumb|Feiro Daru]]
;Feiro I: 1923 version with {{convert|120|hp|kW|abbr=on|0}} [[Le Rhone 9J]] rotary engine.<ref name=Flight/>
;Feiro Daru: 1925 version with {{convert|180|hp|kW|abbr=on|0}} [[Hispano-Suiza 8A]] water-cooled V-8 engine, a slightly swept wing and revised vertical tail.

==Specifications (Feiro I) ==
{{Aircraft specs
|ref=Flight 14 February 1924, pp.86-7<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One pilot
|capacity=Three passengers
|length m=8.83
|length note=
|span m=14.25
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=26
|wing area note=
|aspect ratio=
|airfoil=[[Nikolay Yegorovich Zhukovsky|Joukowski-Göttingen]], thickness/chord ratio=14%<ref name=Laero/>
|empty weight kg=750
|empty weight note=
|gross weight kg=1200
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Le Rhone 9J]]
|eng1 type=9-cylinder [[rotary engine]]
|eng1 hp=120
|eng1 note=
|power original=
|more power=

|prop blade number=two
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=160
|max speed note=<ref name=Laero/>
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=6 min to{{convert|1000|m|ft|abbr=on}}<ref name=Lailes1/>
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
*'''Take-off distance:''' {{convert|45|m|ft|abbr=on}}<ref name=Lailes1/>

}}

==References==
{{reflist|refs=

<ref name=SimonsI>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9806773 4 6|pages=192–4}}</ref>

<ref name=Flight>{{cite magazine |date=14 February 1924 |title= The Feiro I Commercial Monoplane|magazine= [[Flight International|Flight]]|volume=XVI|issue=7|pages=86–7 |url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200086.html  }}</ref>

<ref name=Rotter>{{cite web |url=http://gliders-fega.freeweb.hu/Rotter.htm|title= Hungarian Gliders 1933-2000|author=|accessdate=17 November 2012}}</ref>

<ref name=Laero>{{cite journal |last= |first= |authorlink= |coauthors= |date=March 1924 |title=La vie aéronautique - un avion de transport hongrois|journal=L'Aéronautique|volume=6 |issue=68|page=65|url=http://gallica.bnf.fr/ark:/12148/bpt6k6567587w/f33 }}</ref>

<ref name=Lailes1>{{cite journal |date=21 February 1924 |title=Le monoplan Feiro, type "Daru"|journal=Les Ailles|volume=4 |issue=1400|page=2|url=http://gallica.bnf.fr/ark:/12148/bpt6k65560865/f1}}</ref>

<ref name=Lailes2>{{cite journal |date=12 November 1925 |title=L'Avion Feiro, type "Daru"|journal=Les Ailles|volume=5 |issue=230|page=2|url=http://gallica.bnf.fr/ark:/12148/bpt6k6568262b/f2 }}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Lajos Rotter aircraft}}

[[Category:Hungarian aircraft 1920–1929]]