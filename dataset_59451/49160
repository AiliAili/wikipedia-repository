{{Infobox company
| name             = Lagan Technologies
| logo             = [[File:Lagan Technologies Logo Lagan-logo.jpg]]
| caption          = 
| type             = Private
| genre            = 
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = <!-- {{Start date|YYYY|MM|DD}} -->
| founder          = Colin Chambers, David Moody, John Montgomery, Tom Montgomery
| defunct          =
| location_city    = Sunnyvale, CA
| location_country = 
| location         = 
| locations        = Sunnyvale CA, Chicago IL, Washington, DC, Belfast Norther Ireland, Newbury England
| area_served      = 
| key_people       = Mark Duffell (CEO), Jeff Wylie (GM of International Operations, David Moody (VP Solutions), Jon Montgomery (VP Global Development, Philipb Murray (VP International Sales)
| industry         = Government CRM
| products         = Lagan Government CRM, Lagan Enterprise Case Management (ECM), Lagan OnDemand
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| aum              = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = Over 300
| parent           = [[Kana Software|KANA Software]]
| divisions        = 
| subsid           = 
| homepage         = <!-- {{URL|www.lagan.com}} -->
| footnotes        = 
| intl             = 
}}

'''Lagan Technologies''', part of [[Kana Software|KANA Software]], provides [[G2C]] (government to citizen) technology that is claimed to enable governments and citizens to communicate more effectively. Lagan has over 200 public sector customers worldwide.  Lagan’s solutions for government [[Customer relationship management|Customer Relationship Management (CRM)]] and Enterprise Case Management (ECM) are offered through several delivery methods including on-premises, on-demand and hosted.

The company was founded in 1994, and was acquired by [[Kana Software|KANA Software]]<ref>{{cite news | url= http://www.bizjournals.com/sanfrancisco/stories/2010/10/04/daily32.html|title= 
KANA Software to buy Ireland’s Lagan Technologies|publisher=The San Francisco Business Times|date=2010-11-06| accessdate=2011-01-24|first=Steven E.F.|last=Brown}}</ref> in November 2010.  Lagan operates in both [[North America]] and the [[UK]] with its North American headquarters in [[Sunnyvale, CA]] and its European headquarters in [[Belfast]], [[Northern Ireland]].

== Company history ==
Lagan Technologies was first established in 1994 with the launch of its first product, Frontline<ref>{{cite web | url= http://www.crescentcapital.co.uk/our-investments/lagan-technologies/|title=Lagan Technologies|year=2002| accessdate=2011-01-31}}</ref> in 1998, a CRM solution designed to be technology-independent, with an open architecture, fast to implement and flexible to maintain.

Since 1999 Lagan has been wholly focused on delivering CRM, city service and interaction management solutions for government. Frontline fast became the leading CRM solution in the UK local government market, and was being adopted across the wider public sector in areas such as shared services and non-emergency call handling for police authorities.<ref>{{cite web | url= http://www.esriuk.com/industries/pressrelease.asp?pid=147&indID=7&subid=/|title=Lagan and CAPS Solutions partner to provide the first out-of-box, integrated CRM and business systems connectivity|date=2004-03-20| accessdate=2011-01-31}}</ref>

In 2004, Lagan extended this approach to the non-emergency / 311 and city service sector in the US and Canada, with the release of Frontlink, specifically designed to meet the specialized needs of the North American market.

In 2005 the [[City of Minneapolis]] rolled out as the company’s first North American local government customer with Lagan 311.<ref>{{cite web | url= http://www.allbusiness.com/government/government-bodies-offices-regional/5188891-1.html|title=Minneapolis Adopts Lagan Technologies' Frontlink CRM Solution in New 3-1-1 Call Center|date=2006-01-03| accessdate=2011-01-31}}</ref>

In 2006, Lagan acquired Peter Martin Associates (PMA), a leader in the development of human services software and the first to offer [[commercial off-the-shelf]] (COTS) collaborative case management and eligibility screening solutions specifically tailored to the needs of human services agencies.<ref>{{cite news | url= http://www.businesswire.com/news/home/20060530005589/en/Lagan-Acquires-Leading-Human-Services-Software-Vendor|title=Lagan Acquires Leading Human Services Software Vendor, Peter Martin Associates from Affiliated Computer Services, Inc|date=2006-05-30| accessdate=2011-01-31|work=Business Wire}}</ref>

In 2007 Lagan Announced its first statewide deployment of Lagan Human Services with customer, Tennessee Department of Human Services, Adult Protective Services Division.  Lagan also received its first “Strong Positive” Rating in Leading Analyst Firm’s Local Government CRM Products MarketScope Report and has been recognized in 2008, 2009 and 2010 with the same rating.

In 2009 released initial [[Cloud software]] offering Lagan OnDemand with customers that include: Bermuda;<ref>{{cite web | url= http://www.plp.bm/node/3052|title=E-Government Awards Contract to Lagan Technologies to Provide Enterprise Case Management Software for the Bermuda Government|date=2010-07-19| accessdate=2011-01-31}}</ref> Cobb County, GA<ref>{{cite web | url= http://www.newswire.ca/en/releases/archive/May2010/18/c4709.html|title=Cobb County, GA Selects Lagan Cloud-based CRM Contact Center Solution for Transparency In and Across Departments Countywide|date=2010-05-18| accessdate=2011-01-31}}</ref> and Pasadena, CA.<ref>{{cite web | url= http://www.prnewswire.com/news-releases/city-of-pasadena-ca-selects-lagan-cloud-based-crm-contact-center-solution-to-improve-and-streamline-service-delivery-to-citizens-89110607.html|title=City of Pasadena, CA Selects Lagan Cloud-based CRM Contact Center Solution to Improve and Streamline Service Delivery to Citizens|date=2010-05-25| accessdate=2011-01-31}}</ref>  Lagan also launched Citizen Mobile Application to Promote Increased Citizen Engagement and Self Service.<ref>{{cite web | url= http://www.publicservice.co.uk/feature_story.asp?id=15690|title= 
It will never be the same again|publisher=www.publicservice.co.uk|date=2010-11-24| accessdate=2011-01-31}}</ref><ref>{{cite web | url= http://www.govtech.com/e-government/G7-Big-City-CIOs-Work-to-Develop-Open-Source-IT-Solutions.html?page=1|title= 
G7: CIOs From Seven Big-Cities Work Together to Develop Open-Source IT Solutions|date=2010-10-18| accessdate=2011-01-31}}</ref><ref>{{cite web | url= http://searchitchannel.techtarget.com/news/1515385/Cloud-SaaS-on-the-horizon-for-state-and-local-governments|title=Cloud, SaaS on the horizon for state and local governments|date=2010-06-21| accessdate=2011-01-31}}</ref><ref>{{cite web | url= http://americancityandcounty.com/technology/iphone-crm-system-201002/|title=Eyes on the streets=Solutions|date=2010-02-01| accessdate=2011-01-31}}</ref>

In 2010, Lagan entered the Australian market with Lagan Government CRM customer, [[Brisbane City Council]].<ref>{{cite web | url= http://www.call-center-international.com/News/Industrynews/385/15797/Lagan-Technologies-to-provide-CRM-for-Brisbane.html|title=Lagan Technologies to provide CRM for Brisbane|date=2010-11-08| accessdate=2011-01-31}}</ref>  Lagan was acquired by [[Kana Software|KANA Software]] in November 2010 and remains focused on the public sector.

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.lagan.com/ Lagan Technologies Home Page]
* [http://www.kana.com/ KANA Software Home Page]

[[Category:Articles created via the Article Wizard]]
[[Category:Software companies based in California]]
[[Category:Information technology companies of the United States]]
[[Category:CRM software companies]]
[[Category:Multinational companies]]
[[Category:Companies established in 1994]]
[[Category:Companies based in Sunnyvale, California]]