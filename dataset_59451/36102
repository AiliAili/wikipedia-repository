{{good article}}
'''Elizabeth Alkin''' ({{circa}} 1600 – {{circa}} 1655){{efn|Although Maureen Bell, writing for the ''Oxford Dictionary of National Biography'', puts the date of death as 1655,{{sfn|Bell|2004}} the historians [[Diane Purkiss]] and Marcus Nevitt separately estimate the date as 1654.{{sfn|Purkiss|2007|p=410}}{{sfn|Nevitt|2006|p=93}}}} was a publisher, nurse and spy for the [[Roundhead|Parliamentarian forces]] during the [[English Civil War]]. Among the many derogatory names she was called by royalist sympathisers, that of '''Parliament Joan''' is one by which she is also commonly known.{{sfn|Bell|2004}}

==Background and Civil War activities==
Nothing is known of Alkin until 1645; because of comments of her age made later in life, her date of birth is taken to be around 1600.{{sfn|Bell|2004}} She was the wife of Francis Alkin, a spy for the [[Roundhead|Parliamentarians]] who was hanged early in the [[English Civil War]] by [[Cavalier|royalist forces]] for his activities.{{sfn|Purkiss|2007|p=410}} She was the mother of three children.{{sfn|Bell|2004}}

In 1645 Alkin was employed by the earl of Essex, [[William Waller|Sir William Waller]], to be a spy for the [[Roundhead|Parliamentarian]]s. She received a similar commission from [[Thomas Fairfax|Sir Thomas Fairfax]] two years later.{{sfn|Bell|2004}} Parliamentary records show that in 1645 she received payment from the [[Committee for the Advance of Money]] for uncovering the activities of George Mynnes, a Surrey-based iron merchant, who was supplying metal to the royalist forces.{{sfn|Nevitt|2006|pp=95–96}}

In the seventeenth century, daily news was published in [[newsbook]]s which tended to be small eight-page publications, the forerunners of newspapers. They were usually sold on the street by what the historian Bob Clarke describes as "semi-destitute female hawkers, known as Mercury Women".{{sfn|Clarke|2004|pp=23–24}} Those publications supporting the royalist cause were closed down and the publishers prosecuted, and Alkin became involved in uncovering those behind the publication. In 1648 the royalist newsbooks the ''Mercurius Melancholicus'' and the ''Parliament Kite'' both referred to her attempts to uncover them, and the following year the ''[[Marchamont Nedham#Mercurius Pragmaticus|Mercurius Pragmaticus]]'' called her an "old Bitch" who could "smell out a Loyall-hearted man as soon as the best Blood-hound in the Army".{{sfn|Raymond|2007|p=95}}

Although Alkin acted as one of the newsbook sellers, between 1650 and 1651 she published several short-lived newsbooks,{{sfn|Raymond|2006|p=306}} including ''The Impartial Scout'', ''The Moderne Intelligencer'', ''Mercurius Anglicus'' (formerly a royalist title which she appropriated) and ''Mercurius Scoticus, or, The Royal Messenger''.{{sfn|Bell|2004}} Clarke believes Alkin may have used formerly royalist titles, or royalist-sounding names to win the confidence of royalist sympathisers, and get them to reveal the location of illicit printers.{{sfn|Clarke|2004|p=24}} The historian Marcus Nevitt disagrees, and argues that Alkin was "reappropriating Royalist titles for Parliamentarian consumption".{{sfn|Nevitt|2013|p=101}} In total she produced ten notebook issues of differing titles.<ref name="Lumbers: review" />

One of those she uncovered was [[William Dugard]], who ran four presses at the [[Merchant Taylors' School, Northwood|Merchant Taylors' School]] in London; Dugard was imprisoned in February 1650. The following year she was paid £10 for discovering the printers of Edward Hall's work ''Manus testium lingua testium'', and received further recompense from the Committee for the Advance of Money for other, unknown services.{{sfn|Bell|2004}}

==Post Civil War== 
In 1653, during the [[First Anglo-Dutch War]], Alkin assisted [[Daniel Whistler]] in setting up a network of casualty reception stations in [[Portsmouth]], [[Harwich]] and [[East Anglia]]. The stations treated both English and Dutch casualties.<ref name="NHS: Military" />

Alkin made financial claims from the state for her nursing, some of which were paid, although a petition of 1654 refers to her severe illness. The same letter stated that she had had to sell many of her possessions, including her bed. A petition for financial relief from May 1655 is the last recorded note on her, and it is presumed that she died soon afterwards.{{sfn|Bell|2004}}

==Notes and references==
===Notes===
{{notes}}

===References===
{{reflist|colwidth=25em|refs=
<ref name="NHS: Military">
{{cite web|title=Military Medicine Timeline|url=http://www.nhs.uk/Tools/Documents/Military%20history%20timeline%20read-only.htm|website=[[National Health Service]]|accessdate=4 May 2016}}</ref>

<ref name="Lumbers: review">
{{cite journal|last1=Lumbers|first1=A C|journal=The English Historical Review|title=Book Reviews|date=April 2008|volume=123|issue=501|pages=465–66|jstor=20108494}}</ref>
}}

===Sources===
{{refbegin|30em}}

* {{cite journal|last=Bell|first=Maureen|title=Alkin, Elizabeth (c.1600–1655?)|url= http://www.oxforddnb.com/view/article/57433|work=Oxford Dictionary of National Biography|publisher=Oxford University Press|accessdate=28 April 2016|doi=10.1093/ref:odnb/57433|year=2004|ref={{sfnRef|Bell|2004}}}} {{ODNBsub}}
* {{cite book|last1=Clarke|first1=Bob|title=From Grub Street to Fleet Street|year=2004|publisher=Routledge|location=London|isbn=978-0-7546-5007-2|ref=harv}}
* {{cite book|last=Nevitt|first=Marcus|title=Women and the Pamphlet Culture of Revolutionary England, 1640–1660|url=https://books.google.com/books?id=ohbc1oYKxPIC&pg=PP1|year=2006|publisher=Ashgate Publishing|location=Aldershot, Hants|isbn=978-0-7546-4115-5|ref=harv}}
* {{Cite book|last1= Nevitt |first1= Marcus |contribution=Women in the Business of Revolutionary news: Elizabeth Alkin, "Parliament Joan" and the Commonwealth Newsbook|editor-last= Raymond |editor-first= Joad |title= News, Newspapers and Society in Early Modern Britain |pp=109–140|year=2013|location=London|publisher= Routledge |isbn= 978-1-134-57199-4 |ref=harv}}
* {{cite book|last1=Purkiss|first1=Diane|authorlink=Diane Purkiss|title=The English Civil War|year=2007|publisher=Harper|location=London|isbn=978-0-0071-5062-5|ref=harv}}
* {{cite book|last=Raymond|first=Joad|title=Pamphlets and Pamphleteering in Early Modern Britain|url=https://books.google.com/books?id=SHUb7uBsxwkC&pg=PR1|year=2006|publisher=Cambridge University Press|location=Cambridge|isbn=978-0-521-02877-6|ref=harv}}
* {{cite book|last1=Raymond|first1=Joad|title= News Networks in Seventeenth Century Britain and Europe|year=2007|publisher=Routledge|location=London|isbn=978-0-4154-6411-6|ref=harv}}
{{refend}}

{{Authority control}}

{{DEFAULTSORT:Alkin, Elizabeth}}
[[Category:1600 births]]
[[Category:1654 deaths]]
[[Category:1655 deaths]]
[[Category:English nurses]]
[[Category:English printers]]
[[Category:English spies]]
[[Category:17th-century spies]]