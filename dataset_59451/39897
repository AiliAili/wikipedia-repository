{{Infobox person
| name        = Archie William League
| image       = Archumb.jpg
| image_size  = 
| caption     = Archie League is shown while on duty during the summer at [[Lambert-St. Louis International Airport]]. His equipment included rolled-up flags in the wheelbarrow, the dangling lunch box, a folding chair, drinking water, and a pad for taking notes.
| birth_name  = 
| birth_date  = {{birth date|mf=yes|1907|8|19}}
| birth_place = [[Poplar Bluff, Missouri]]
| death_date  = {{death date and age|mf=yes|1986|10|1|1907|8|19}}
| death_place = [[Annandale, Virginia]]
| death_cause = 
| resting_place = 
| resting_place_coordinates = 
| residence   = 
| nationality = 
| other_names = 
| known_for   = Generally considered the first [[air traffic controller]].
| education   = Degree in [[aeronautical engineering]] from [[Washington University in St. Louis]]
| employer    = [[Federal Aviation Administration]]
| occupation  = 
| title       = 
| salary      = 
| networth    = 
| height      = 
| weight      = 
| term        = 
| predecessor = 
| successor   = 
| party       = 
| boards      = 
| religion    = 
| spouse      = 
| partner     = 
| children    = 
| parents     = 
| relatives   = 
| signature   = 
| website     = [http://www.faa.gov/about/history/photo_album/air_traffic_control/index.cfm?cid=begins Air Traffic Control Begins]
| footnotes   = 
}}

'''Archie William League''' (August 19, 1907 &ndash; October 1, 1986) is generally considered the first [[air traffic controller]].<ref name=ATC_History>{{cite web | url= http://www.atcguild.com/tour/atctour02.asp | title= History of Air Traffic Control | publisher= Air Traffic Controllers' Guild (India) | accessdate=2007-07-29 }}</ref>
[[File:Archie League.jpg|thumb|Archie League in the San Francisco Presidio sometime during World War II (his half-brother Daniel League had taken this picture) ]]
League had been a licensed pilot, and licensed engine and aircraft mechanic. He had [[Barnstorming|barnstormed]] around in [[Missouri]] and [[Illinois]] with his "flying circus," prior to [[St. Louis]] hiring him as the first U.S. air traffic controller in 1929.<ref name=100_Years_of_Flight>{{cite web | url= http://www.natca.org/100years/LuckyLindy_ArchieLeague.pdf | title= 1926-1935: Lucky Lindy and Archie League | publisher= The National Air Traffic Controllers Association (NATCA) | accessdate=2007-07-29 }}</ref> He was stationed at the airfield in St. Louis, Missouri (now known as [[Lambert-St. Louis International Airport]]). Before the installation of a radio tower, he was a flagman who directed traffic via flags. His first "control tower" consisted of a wheelbarrow on which he mounted a beach umbrella for the summer heat. In it he carried a beach chair, his lunch, water, a note pad and a pair of signal flags to direct the aircraft. He used a checkered flag to indicate to the pilot "GO", i.e. proceed, or a red flag to indicate the pilot should "HOLD" their position. He kept warm out on the field in the winters by wearing a padded flying suit. When a radio tower was installed in the early 1930s, he became the airport's first radio controller.<ref name=Centenial_of_Flight>{{cite web|url=http://www.centennialofflight.gov/essay/Evolution_of_Technology/landing_navig/Tech32.htm |title=Aircraft Landing Technology |author=Mola, Roger |publisher=U. S. Centennial of Flight Commission |accessdate=2007-07-24 |deadurl=yes |archiveurl=https://web.archive.org/web/20070930031538/http://www.centennialofflight.gov/essay/Evolution_of_Technology/landing_navig/Tech32.htm |archivedate=2007-09-30 |df= }}</ref>

League went on to earn a degree in [[Aerospace engineering|aeronautical engineering]] from [[Washington University in St. Louis]]. League joined the Federal service in 1937 at the [[Bureau of Air Commerce]] (the precursor to the [[Civil Aeronautics Authority]], and the [[Federal Aviation Administration]]). He rose rapidly through the ranks as an Air Traffic controller, served as a pilot in [[World War II]] (where he rose to the rank of [[Colonel]]) then progressed to his first top management position in 1956, as Assistant Regional Administrator of the Central Region. He next went to [[Washington DC|Washington]] headquarters as Chief of the Planning Division (Planning and Development Office) in 1958. After a short assignment as Director, Bureau of National Capital Airports, he moved to [[Fort Worth, Texas|Fort Worth]] as the Director of Southwest Region. His next assignment was in May 1965, relocating to Washington headquarters as Director of Air Traffic Services, where he became head of the staff responsible for the safe and efficient operation of the nation’s air traffic control system. He eventually became FAA's Air Traffic Service director and retired as an Assistant Administrator for Appraisal in 1973.<ref name=FAA_SW_Logistics>{{cite web |url = http://www.faa.gov/ASW/asw050/league.html  |title = Archie William League |accessdate = 2007-09-09 |publisher = [[Federal Aviation Administration]]: Southwest Region Logistics Division |archiveurl = https://web.archive.org/web/20041111222326/http://www.faa.gov/ASW/asw050/league.html |archivedate = 2004-11-11}}</ref><ref name=FAA_History>{{cite web | url= http://www.faa.gov/about/history/photo_album/air_traffic_control/index.cfm?cid=begins | title= Photo Album - Air Traffic Control: Air Traffic Control Begins | publisher= [[Federal Aviation Administration]] | accessdate=2007-07-24 }}</ref> During his 36-year career he helped develop the federal air traffic control system.<ref name=100_Years_of_Flight/> The National Air Traffic Controllers Association ([[National Air Traffic Controllers Association|NATCA]]) named the [[Archie League Medal of Safety Awards]] after him.<ref name=Safety_Award>{{cite web | url= http://natca.unionlaborworks.com/press_releases.aspx?aID=311 | title= NATCA Honors Top Controller Flight Assists with Archie League Medal of Safety Awards  | publisher= [[National Air Traffic Controllers Association]] | date = 2010-03-22 | accessdate=2010-10-09 }}</ref>

He was born in 1907 at [[Poplar Bluff, Missouri]] in [[Butler County, Missouri|Butler County]].<ref name=Census_1910_Butler_County>{{cite web |url = http://files.usgwarchives.net/mo/butler/census/c1910/pg-254a-257a.txt |title = Butler Co., Missouri 1910 Federal Census - pg 254a-257a.txt |accessdate = 2007-09-09 |author = Rodgers, Brenda (transcription) |author2 = Hudson, Mary (proofreading) |year = 2000 |work = Transcription of [[United States Census, 1910|1910 Census]] |publisher = rootsweb |pages = 256a line 64}}</ref> He died on October 1, 1986 at the age of 79 in [[Annandale, Virginia]].<ref name=Archie_W_League>{{cite web | url= http://wanderinglearner.com/ah/EllenRoz2.doc | title= Archie W. League | author= Roz, Ellen | publisher= Spartan College of Aeronautics and Technology | accessdate=2007-07-29 }}</ref>

==See also==
{{Portal|Biography}}
*[http://www.faa.gov/news/communications/controller_remembered/?print=go First Air Traffic Controller Remembered]

==References==
{{Reflist|2}}

{{DEFAULTSORT:League, Archie W.}}
[[Category:1907 births]]
[[Category:1986 deaths]]
[[Category:People from Poplar Bluff, Missouri]]
[[Category:People from St. Louis]]
[[Category:Washington University in St. Louis alumni]]
[[Category:Aviation pioneers]]
[[Category:American World War II pilots]]
[[Category:Federal Aviation Administration personnel]]
[[Category:Air traffic controllers]]