{{Infobox boxer
| name        = Roberto Bolonti
| image       = <!-- only free-content images are allowed for depicting living people - see [[WP:NONFREE]] -->
| realname    = Roberto Feliciano Bolonti
| nickname    = La Bestia
| weight      = [[Light Heavyweight]]
| height      = {{height|ft=5|in=11}}
| reach       = 
| nationality = Argentine
| birth_date  = {{birth date and age|1978|12|30}}
| birth_place = [[Buenos Aires]], Argentina
| style       = Orthodox
| total       = 44
| wins        = 37
| KO          = 25
| losses      = 6
| draws       = 0
| no contests = 1
}}

'''Roberto Feliciano Bolonti''' (born March 2, 1984) is an Argentine [[professional boxing|professional boxer]] who fights at [[Light Heavyweight]]. He is the current [[World Boxing Council|WBC]] Latino light heavyweight champion.

==Professional career==
On 17 November 2012, Bolonti was beat by [[Tony Bellew]] for the WBC silver title. Bellew was cut badly but went the full 12 rounds to a win unanimous decision.

June 7, 2014 on a fight for the [[World Boxing Association|WBA]] World light heavyweight title, Bolonti lost in a Unanimous Decision against [[Juergen Braehmer]].<ref>{{cite web|url=http://espn.go.com/boxing/story/_/id/11048617/juergen-braehmer-outpoints-roberto-feliciano-bolonti-retain-wba-light-heavyweight-belt|title=Juergen Braehmer outpoints Roberto Feliciano Bolonti to retain WBA light heavyweight belt|work=ESPN.com}}</ref>

December 6, 2014 his fight against [[Jean Pascal]]<ref>{{cite web|url=http://o.canada.com/sports/pascal-bolonti-bout-ends-in-no-contest-after-canadians-unintentional-foul|title=Pascal-Bolonti bout ends in no contest after Canadian's unintentional foul – canada.com|work=canada.com}}</ref> ended in no contest after Canadian's unintentional foul.<ref>{{cite web|url=http://www.boxinginsider.com/headlines/jean-pascal-versus-roberto-beast-bolonti-ruled-no-contest/|title=Jean Pascal versus Roberto "the Beast" Bolonti Ruled "No Contest" – BoxingInsider.com|work=BoxingInsider.com}}</ref>

On 19 August 2015, Roberto will face [[Danny Green (boxer)|Danny Green]] on his comeback after almost 3 years outside the ring.<ref>{{cite web|url=http://www.perthnow.com.au/sport/boxing-mma/danny-greens-comeback-isnt-panning-out-the-way-he-expected-it-to/story-fnhq5ibz-1227479441899 |title=Danny Green's comeback isn’t panning out the way he expected it to |publisher=Perthnow.com.au |accessdate=2015-08-14}}</ref>

==Professional boxing record==
{{S-start}}
| style="text-align:center;" colspan="8"|'''37 wins''' (25 knockouts), '''6 losses''', '''0 draws'''
|-  style="text-align:center; background:#e3e3e3;"
|  style="border-style:none none solid solid; "|'''Res.'''
|  style="border-style:none none solid solid; "|'''Record'''
|  style="border-style:none none solid solid; "|'''Opponent'''
|  style="border-style:none none solid solid; "|'''Type'''
|  style="border-style:none none solid solid; "|'''Rd., Time'''
|  style="border-style:none none solid solid; "|'''Date'''
|  style="border-style:none none solid solid; "|'''Location'''
|  style="border-style:none none solid solid; "|'''Notes'''
|- align=center
|{{no2}}Loss
|37-6
|align=left|{{flagicon|South Africa}} Kevin Lerena
|{{small|UD}}
|{{small|10 (10)}}
|{{small|2016-06-11}}
|align=left|{{small|{{flagicon|South Africa}} Emperors Palace, Kempton Park, Gauteng, South Africa}}
|align=left|
|- align=center
|{{yes2}}Win
|37-5
|align=left|{{flagicon|ARG}} Walter David Cabral
|{{small|KO}}
|{{small|6 (8)}}
|{{small|2016-01-29}}
|align=left|{{small|{{flagicon|ARG}} Club Social y Deportivo Santa Clara, Santa Clara del Mar, [[Buenos Aires]], [[Argentina]]}}
|align=left|
|- align=center
|{{no2}}Loss
|36-5
|align=left|{{flagicon|FRA}} [[Youri Kayembre Kalenga]]
|{{small|KO}}
|{{small|9 (10), 1:35}}
|{{small|2015-11-07}}
|align=left|{{small|{{flagicon|MON}} Salle des Étoiles, [[Monte Carlo]], [[Monaco]]}}
|align=left|
|- align=center
|{{no2}}Loss
|36-4
|align=left|{{flagicon|AUS}} [[Danny Green (boxer)|Danny Green]]
|{{small|UD}}
|{{small|12}}
|{{small|2015-08-19}}
|align=left|{{small|{{flagicon|AUS}} [[Hisense Arena]], [[Melbourne]], Australia}}
|align=left|
|- align=center
|{{yes2}}Win
|36-3
|align=left|{{flagicon|VEN}} Williams Ocando
|{{small|TKO}}
|{{small|2 (10)}}
|{{small|2015-07-25}}
|align=left|{{small|{{flagicon|ARG}} GAP Disco, Mar del Plata, Buenos Aires, Argentina}}
|align=left|{{small|Won vacant WBC Latino light heavyweight title}}
|- align=center
|style="background:#ddd;"|NC
|35-3
|align=left|{{flagicon|CAN}} [[Jean Pascal]]
|{{small|NC}}
|{{small|2 (10)}}
|{{small|2014-12-06}}
|align=left|{{small|{{flagicon|CAN}} [[Bell Centre]], [[Montreal]], Canada}}
|align=left|
|- align=center
|{{no2}}Loss
|35-3
|align=left|{{flagicon|GER}} [[Juergen Braehmer]]
|{{small|UD}}
|{{small|12 (12)}}
|{{small|2014-06-07}}
|align=left|{{small|{{flagicon|GER}} [[Sport- und Kongresshalle|Sport and Congress Center]], [[Schwerin]], [[Mecklenburg-Vorpommern]]}}
|align=left|{{small|For [[World Boxing Association|WBA]] World light heavyweight title}}
|- align=center
|{{yes2}}Win
|35-2
|align=left|{{flagicon|ARG}} José Alberto Clavero
|{{small|KO}}
|{{small|6 (10)}}
|{{small|2014-01-24}}
|align=left|{{small|{{flagicon|ARG}} Polideportivo Municipal, Pinamar, Buenos Aires, Argentina}}
|align=left|{{small|Retain WBC Latino light heavyweight title.}}
|- align=center
|{{yes2}}Win
|34-2
|align=left|{{flagicon|COL}} Manuel Banquez
|{{small|KO}}
|{{small|5 (10)}}
|{{small|2013-11-15}}
|align=left|{{small|{{flagicon|ARG}} GAP Disco, Mar del Plata, Buenos Aires, Argentina}}
|align=left|{{small|Retain WBC Latino light heavyweight title.}}
|- align=center
|{{yes2}}Win
|33-2
|align=left|{{flagicon|CHI}} Rodrigo Osvaldo Chavez
|{{small|KO}}
|{{small|4 (8)}}
|{{small|2013-09-13}}
|align=left|{{small|{{flagicon|ARG}} GAP Disco, Mar del Plata, Buenos Aires, Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|32-2
|align=left|{{flagicon|URU}} Jorge Rodriguez Olivera
|{{small|KO}}
|{{small|3 (10)}}
|{{small|2013-03-06}}
|align=left|{{small|{{flagicon|ARG}} Club Social y Deportivo Santa Clara, Santa Clara, Buenos Aires, Argentina}}
|align=left|{{small|Retain WBC Latino light heavyweight title.}}
|- align=center
|{{yes2}}Win
|31-2
|align=left|{{flagicon|ARG}} Franco Raul Sanchez
|{{small|RTD}}
|{{small|8 (10)}}
|{{small|2013-01-19}}
|align=left|{{small|{{flagicon|ARG}} Club Social y Deportivo, Santa Clara, Buenos Aires, Argentina}}
|align=left|{{small|Retain WBC Latino light heavyweight title.}}
|- align=center
|{{no2}}Loss
|30-2
|align=left|{{flagicon|UK}} [[Tony Bellew]]
|{{small|UD}}
|{{small|12 (12)}}
|{{small|2011-04-02}}
|align=left|{{small|{{flagicon|UK}} [[Capital FM Arena]], [[Nottingham]]}} 
|align=left|{{small|For vacant WBC Silver light heavyweight title}}
|- align=center
|{{yes2}}Win
|30-1
|align=left|{{flagicon|ARG}} José Alberto Clavero
|{{small|KO}}
|{{small|8 (12)}}
|{{small|2012-09-22}}
|align=left|{{small|{{flagicon|ARG}} [[Luna Park, Buenos Aires|Estadio Luna Park]], [[Buenos Aires]], Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|29-1
|align=left|{{flagicon|COL}} Evert Bravo
|{{small|TKO}}
|{{small|3 (10)}}
|{{small|2012-05-12}}
|align=left|{{small|{{flagicon|ARG}} Club América, General Pirán, Buenos Aires, Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|28-1
|align=left|{{flagicon|ARG}} Jose Emilio Mazurier
|{{small|KO}}
|{{small|6 (10)}}
|{{small|2011-12-10}}
|align=left|{{small|{{flagicon|ARG}} General Piran, Buenos Aires, Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|27-1
|align=left|{{flagicon|BRA}} Jose Hilton Dos Santos
|{{small|TKO}}
|{{small|4 (10)}}
|{{small|2011-10-14}}
|align=left|{{small|{{flagicon|ARG}} [[Luna Park, Buenos Aires|Estadio Luna Park]], [[Buenos Aires]], Argentina}}
|align=left|{{small|Retain WBC Latino light heavyweight title.}}
|- align=center
|{{yes2}}Win
|26-1
|align=left|{{flagicon|ARG}} Jose Emilio Mazurier
|{{small|TKO}}
|{{small|6 (10)}}
|{{small|2011-08-20}}
|align=left|{{small|{{flagicon|ARG}} Ce.De.M. N° 1, Caseros, Buenos Aires, Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|25-1
|align=left|{{flagicon|ARG}} Martin David Islas
|{{small|UD}}
|{{small|10 (10)}}
|{{small|2011-05-01}}
|align=left|{{small|{{flagicon|ARG}} Club América, General Pirán, Buenos Aires, Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|24-1
|align=left|{{flagicon|ARG}} Martin David Islas
|{{small|UD}}
|{{small|8 (8)}}
|{{small|2011-03-26}}
|align=left|{{small|{{flagicon|ARG}} Polideportivo Gabriel Atilio, Buenos Aires, Argentina}}
|align=left|
|- align=center
|{{yes2}}Win
|23-1
|align=left|{{flagicon|ARG}} Martin David Islas
|{{small|TKO}}
|{{small|2 (6)}}
|{{small|2010-12-18}}
|align=left|{{small|{{flagicon|ARG}} Club Once Unidos, Mar del Plata, Buenos Aires, Argentina}}
|align=left|
|- align=center
{{s-end}}

==References==
{{Reflist}}

==External links==
*{{boxrec|id=322907}}

{{DEFAULTSORT:Bolonti, Roberto}}
[[Category:1984 births]]
[[Category:Living people]]
[[Category:Argentine male boxers]]