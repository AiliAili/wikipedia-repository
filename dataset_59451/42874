<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=TSR.38
 | image=GTSR.38.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=3-seat torpedo/spotter/reconnaissance 
 | national origin=United Kingdom
 | manufacturer=[[Gloster Aircraft Company|Gloucestershire Aircraft Company]]
 | designer=
 | first flight=April 1932
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Gloster TSR.38''' was a single-engined three-seat [[biplane]] designed as a naval torpedo/spotter/reconnaissance aircraft in the early 1930s.  It did not reach production and only one was built.

==Development==
[[Gloster Aircraft Company|Gloster]] built what was at first known as the FS.36 (FS standing for fleet spotter) to [[List of Air Ministry Specifications#1920-1929|Air Ministry specification]] S.9/30 which called for a three-seat torpedo bomber/spotter/reconnaissance aircraft, starting work in late 1930 but proceeding slowly so it did not fly until early April 1932. This aircraft, powered by a 600&nbsp;hp (450&nbsp;kW) [[Rolls-Royce Kestrel]] IIMS engine was flown to [[RAF Martlesham Heath]] for initial testing.  RAF pilots reported good handling at higher speeds but criticised its low speed handling and braking, important parameters for a carrier landing aircraft.  It then returned to Gloster's for modification, partly to meet these criticisms and partly to cover a new specification S.15/33, a melding of the early specification with M.1/30.  Re-engined with a [[Rolls-Royce Goshawk]] VIII of 690&nbsp;hp (515&nbsp;kW), and with modified wings of greater area the aircraft emerged in mid-1933 as the TSR.38, TSR standing for torpedo/spotter/reconnaissance.<ref name="DJN">{{harvnb|James|1971|pp=200–4}}</ref>

The TSR.38 was a fabric covered all metal machine with steel sparred wings with [[duralumin]] and steel ribs. It was a single bay biplane with [[stagger (aviation)|staggered]] wings of almost equal span and sharp, 10° sweepback.  The airfoils were thick, the lower thicker and the upper wings carried leading edge automatic [[Leading edge slot|slot]]s for low speed control.  The wings could be folded from the centre section for carrier stowage. The lower wing centre section had [[Dihedral (aircraft)#Anhedral|anhedral]] to allow a shorter undercarriage and also carried a pair of underwing radiators.  The undercarriage was of the fixed, split axle type required for torpedo launches, with [[torpedo crutches]] between the legs and there was a small, [[Aircraft fairing|spatted]] tailwheel.  For carrier landing there was a hinged [[arrestor hook]] ending just in front of the tailwheel. The three crew sat in separate open cockpits, the pilot in front under a cut-out in the upper wing trailing edge, his navigator behind him and furthest aft the gunner with a [[Lewis gun]] on a [[Scarff ring]].  The fin was large and rounded with a generous rudder, a little like that of the later [[Gladiator]], with the tailplane braced from above to the fin.<ref name="DJN"/>

==Operational history==
After flight trials at Gloster's the TSR.38 went to [[HMS Sultan (establishment)|RAF Gosport]] where it made dummy carrier landing before joining {{HMS|Courageous|50|6}} in August 1934 for real naval tests.  It then returned to Martlesham for a thorough evaluation.  However, its rather dilatory development now caught up with it in the form  of a superior, later private venture from Fairey, which was to become the famous [[Fairey Swordfish]] and further work on the TSR.38 was ended.<ref name="DJN"/> 
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (TSR.38, reconnaissance configuration)==

{{aerospecs
|ref={{harvnb|James|1971|pp=203–4}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=3
|capacity=
|length m=11.4
|length ft=37
|length in=4½
|span m=14.0
|span ft=46
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.5
|height ft=11
|height in=6
|wing area sqm=56.6
|wing area sqft=611
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=1,968
|empty weight lb=4,340
|gross weight kg=3,220
|gross weight lb=7,100
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=12-cylinder V-12 evaporatively cooled supercharged [[Rolls-Royce Goshawk]]  
|eng1 kw=515<!-- prop engines -->
|eng1 hp=690<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=244
|max speed mph=at sea level 152
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=5.21
|climb rate ftmin=to 10,000 ft (3,048 m) 1026
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=1×forward firing synchronised 0.303 in (7.7 mm) [[Vickers machine gun]]
|armament2=1×0.303 in (7.7 mm) ring-mounted [[Lewis gun]]
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Gloster TSR.38}}

===Notes===
{{reflist}}
{{refbegin}}

===Bibliography===
*{{cite book |title= Gloster Aircraft since 1917|last= James|first=Derek N. |authorlink= |coauthors= |year=1971 |publisher=Putnam Publishing |location=London |isbn= 0-370-00084-6|page= |pages= |url=|ref=harv }} 
{{refend}}
<!-- ==External links== -->
{{Gloster aircraft}}

[[Category:British aircraft 1930–1939]]
[[Category:Gloster aircraft|TSR.38]]