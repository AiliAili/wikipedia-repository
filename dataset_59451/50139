{{Infobox military conflict
| conflict    =Battle of Sukhumi (1992) 
| partof      =[[War in Abkhazia (1992–93)|War in Abkhazia (1992-1993)]] 
| date        =August 14, 1992-August 18
| place       =Sukhumi, Abkhazia
| result      =Georgian victory, Abkhaz separatist retreat to [[Gudauta]] 
| combatants_header = 
| combatant1  =[[File:Flag of Georgia (1990-2004).svg|22px]] [[National Guard of Georgia|Georgian National Guard]] 
| combatant2  ={{flagicon|Abkhazia}} Abkhaz separatists and local volunteers 
| commander1  =[[Tengiz Kitovani]]<ref name="HRW-1995" />
| strength1   =1000 est. 
| strength2   =Unknown
| casualties1 = 
| casualties2 = 
| casualties3 = 
| notes       = 
| campaignbox = 
}}

{{Campaignbox
|name=Campaignbox War in Abkhazia
|title=[[War in Abkhazia (1992–93)|War in Abkhazia]]
|listclass = hlist
|battles =
*1st Sukhumi
* [[Siege of Tkvarcheli|Tkvarcheli]]
* [[Battle of Gagra|Gagra]]
* [[March Battle]]
* [[Kamani massacre|Kamani]]
* [[Sukhumi massacre|2nd Sukhumi]]
}}

The '''Battle of Sukhumi''' took place in 1992 between Abkhaz separatists and the Georgian National Guard. The battle marked the start of one of the bloodiest wars in Post-Soviet Georgia.

==Background==
In July 1992, Georgian officials were taken hostage by Pro-Gamsukhurdia (separatists) forces known as [[Zviadists]] in [[Mingrelia]] and later in the Gali Region. Several more were taken when Georgian officials attempted to negotiate with them.<ref name="HRW-1995">{{Cite journal|year=1995|title=Georgia/Abkhazia: Violations of the Laws of War and Russia's Role in the Conflict: Acts of Lawlessness During the First Two Months of Fighting, August to September 1992|journal=Human Rights Watch Reports|volume=7|issue=7|url=https://www.hrw.org/reports/1995/Georgia2.htm|archiveurl=https://web.archive.org/web/20010220133323/http://www.hrw.org/reports/1995/Georgia2.htm|archivedate=20 February 2001|deadurl=no}}</ref>  In response, Georgian police created assault units to help free the hostages. The Abkhaz Interior Ministry said that Georgian and Abkhaz Units would cooperate to help free the hostages. In August, a National Guard unit was deployed in Gali to help release the hostages and the hostages were freed by August 19 without full-scale combat.

==Entering Sukhumi==
However, the 1,000 man strong assault unit created by the police did not stop at Gali. They then launched an attack on the capital, Sukhumi to retake the city for Georgia. They took the city's airport, which was 25 kilometers from the city.<ref name="HRW-1995" /> A news blockade was imposed on journalists, and by noon they were forcibily entering Sukhumi. Georgian Tanks and APC's moved through the streets, battling Abkhaz militias who were armed with machine guns and formed barricades in the streets, while hurling Molotov cocktails as they lacked heavy anti-tank weapons.<ref name="HRW-1995" /> Georgian units used artillery against Abkhaz separatists in the places they controlled within the city.

Despite Abkhaz resistance, the Georgian units were heavily armed and took the city within a matter of days. On August 18, the Parliament of Abkhazia was stormed by the Georgian National Guard, and the Georgian flag was raised on the Council of Ministers buildings. All of Sukhumi was taken by August 18.<ref name="HRW-1995" />

==Casualties and Aftermath==
19 people were killed and 39 severely injured.<ref name="Refworld">{{Cite web|date=16 January 2004|title=Georgia: Violent events in Sukhumi and Tsalenjikha between April and August 1992; persons in charge of Georgian national security and law enforcement; roles of Colonels Kalandia and Maisuradze in Sukhumi and Tsalenjikha|publisher=Immigration and Refugee Board of Canada|url=http://www.refworld.org/docid/403dd1f214.html|archiveurl=https://web.archive.org/web/20140517152135/http://www.refworld.org/docid/403dd1f214.html|archivedate=17 May 2014|deadurl=no}}</ref> Georgian units were accused of looting, assault, murder and other ethnically based war crimes.<ref name="HRW-1995" /> Abkhaz separatits retreated to Guduata, and started to arm themselves for a counterattack.<ref name="HRW-1995" /><ref name="Refworld" />

==References==
{{Reflist}}
{{-}}

{{DEFAULTSORT:Sukhumi, Battle of}}
[[Category:Conflicts in 1992]]
[[Category:Georgian–Abkhazian conflict]]