{{For|the novel by Rex Warner|Rex Warner}}
{{Refimprove|date=December 2013}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Professor
| title_orig    =
| translator    =
| image         = The Professor 1857.jpg
| caption = Title page of the 1857 first edition.
| author        = [[Charlotte Brontë]]
| illustrator   =
| cover_artist  =
| country       = England
| language      =
| series        =
| genre         =
| publisher     =
| release_date  = 1857
| english_release_date =
| media_type    =
| pages         =
| isbn          =
| preceded_by   =
| followed_by   =
}}

'''''The Professor''''' was the [[Debut novel|first novel]] by [[Charlotte Brontë]]. It was originally written before ''[[Jane Eyre]]'' and rejected by many publishing houses, but was eventually published posthumously in 1857 by approval of Arthur Bell Nicholls, who accepted the task of reviewing and editing of the novel.

==Plot introduction==
The book is the story of a young man, William Crimsworth, and is a first-person narrative from his perspective. It describes his maturation, his loves and his eventual career as a professor at an all-girls school.

The story starts off with a letter William has sent to his friend Charles, detailing his refusal to his uncle's proposals to become a clergyman, as well as his first meeting with his rich brother Edward. Seeking work as a tradesman, William is offered the position of a clerk by Edward. However, Edward is jealous of William's education and intelligence and treats him terribly. By the actions of the sympathetic Mr. Hunsden, William is relieved of his position and gains a new job at an all-boys boarding school in Belgium.

The school is run by the friendly M. Pelet, who treats William kindly and politely. Soon, William's merits as a professor reach the ears of the headmistress of the neighbouring girls school. Mlle. Reuter offers him a position at her school, which he accepts. Initially captivated by Mlle. Reuter, William begins to entertain ideas of falling in love with her, only to have them crushed when he overhears her and M. Pelet talk about their upcoming marriage.

Slightly heartbroken, he now treats Mlle. Reuter with a cold civility and begins to see the underlying nature of her character. Mlle. Reuter, however, continues to try to draw William back in, pretending to be benevolent and concerned. She goes so far as to plead him to teach one of her young teachers, Frances, who hopes to improve her skill in languages. William sees in this pupil promising intelligence and slowly begins to fall in love with her as he tutors her English.

Jealous of the attention Frances is receiving from William, Mlle. Reuter takes it upon herself to casually dismiss Frances from her school and hide her address from William. It is revealed that as she was trying to make herself amiable in William's eyes, Mlle. Reuter accidentally fell in love with him herself. Not wanting to cause a conflict with M. Pelet, William leaves his establishment and moves out, in hopes of finding Frances.

Eventually bumping into his beloved pupil in a graveyard, the two reconcile. William gets a new position as a professor at a college, with an exceedingly high wage. The two eventually open a school together and have a child. After obtaining financial security, the family travels all around England and settle in the countryside next to Mr. Hunsden.

==Characters==

*'''William Crimsworth''': The protagonist, an orphaned child who is educated at [[Eton College]] after being taken in by his uncles. Rejecting their offering of a role as a clergyman – as he does not believe himself good enough for the role, he severs ties with them to walk in his late father's shoes and become a tradesman. His time in Yorkshire as a clerk for his cruel elder brother is short and he departs for Belgium, where he becomes a teacher/professor and meets his wife-to-be, who is a pupil of his. He is educated, religious and healthy, though not handsome.
*'''Lord Tynedale / Hon. John Seacombe / Mr. Seacombe''': William's maternal uncles who attempt to set William up as a rector of Seacombe-cum-Scaife and attempt to marry him off to one of his own cousins, "all of whom [he] greatly disklike[s]." William severs all ties with these aristocrats and little is heard from them in the rest of the book.
*'''Charles''': Seemingly William's only friend at Eton. William writes a letter to him detailing of activities since Eton and just after his first meeting with Edward at Crimsworth Hall. This letter serves as an introduction to the book. He does not reply to the letter as he has already set off for one of the colonies. He is an [[unseen character]].
*'''Edward Crimsworth''': William's tyrannical elder brother. He is an accomplished tradesman, owner of a [[Yorkshire]] mill, married and more handsome than his brother. Jealous of his sibling's education, he treats William cruelly. He later loses his wealth and wife, only to become rich again by the end of the book.
*'''Hunsden Yorke Hunsden''': The man who frees William from his brother's clutches. He sets him up with contacts in Brussels and the two become good friends. He is a unique but not unattractive man who has a similar taste in women as William, though he remains a lifelong bachelor.
*'''Monsieur Francois Pelet''': The French headmaster of a boys' school in Belgium who employs William and becomes a good friend. He later betrays him to ensure the affection of Zoraïde Reuter, who he later marries.
*'''Mademoiselle Zoraïde Reuter''': The Catholic headmistress of the school in Belgium. William is initially attracted to Reuter, though she is destined to marry Monsieur Pelet.
*'''Frances Evans Henri''': A pupil-teacher at the school in Belgium where William Crimsworth finds himself. After the two fall in love they get married and eventually move to England. She is a [[Swiss people|Swiss]] orphan of half English extraction who was raised by her aunt.
*'''Madame Reuter''': Zoraïde's mother
*'''Madame Pelet''': Monsieur Pelet's mother
*'''Eulalie''', '''Hortense''' and *'''Caroline''': Three coquettish students at Mademoiselle Reuter's school
*'''Sylvie''': Another student
*'''Jules Vanderkelkov''': A student at Monsieur Pelet's school
*'''Victor Crimsworth''': Son of William and Frances Evans Henri.

==Themes==

===Religion===
Throughout the novel, William looks down upon [[Catholic]]s and "Romish wizardcraft". Brontë pictures the two main Catholic characters as treacherous and untrustworthy persons. William believes the Catholic upbringing has a negative influence on the young girls at his school.

===Nationalism===
The charming Hunsden character has little patriotism and finds himself in contrast to the national pride Frances holds in her native [[Switzerland]] and the England where her mother's family came from and which she longs to see.

William has a certain snobbery against the [[Flemish people|Flemish]] and is disgusted by the way they butcher the English language as he attempts to teach them.

==Context==
The novel is based upon Charlotte Brontë's experiences in [[Brussels]], where she studied as a language student and was a teacher in 1842. Much of the subject matter of ''The Professor'' was later reworked, from the perspective of a female teacher, into Brontë's novel ''[[Villette (novel)|Villette]]'', which received critical acclaim.

==Bibliography==
*Brontë, Charlotte ''The Professor''. New York: Barnes and Noble, 2005.
*Butterworth, Robert. "The Professor and the Contemporary Working Milieu." Brontë Studies 35.3 (2010): 215–221.
*Butterworth, RD. "The Professor and the Modern Experience of Work." Brontë Studies 36 (2011): 255-262.
*Longmuir, A. "Reader, Perhaps You Were Never in Belgium?: Negotiating British Identity in Charlotte Brontë's ''The Professor'' and ''[[Villette (novel)|Villette]]''." Nineteenth-Century Literature, 64.2 (2009): 163–188.
*Lonoff, Sue. "The Three Faces of Constantin Heger." Brontë Studies 36.1 (2011): 28–37.
*O'Toole, T. Introduction. ''The Professor''. By Charlotte Brontë. New York: Barnes and Noble, 2005: vii–xiii.
*Pearson, Sara L. “Constructing Masculine Narrative: Charlotte Brontë’s The Professor.” Women Constructing Men: Female Novelists and Their Male Characters, 1750-2000. Ed. Sarah S.G. Frantz and Katharina Rennhak. Lanham, MD: Lexington, 2010. 83-99. 
*Peterson, Linda. "Triangulation, Desire, and Discontent in the Life of Charlotte Brontë." SEL: Studies in English Literature 47.4 (2007): 901–920.
*"Playing with the Professor. (Charlotte Brontë's Novel)." CLA Journal 37.3 (1994): 348.

==External links==
{{Gutenberg|no=1028|name=The Professor}}
* {{librivox book | title=The Professor | author=Charlotte Brontë}}
*[http://www.enotes.com/professor-criticism/professor-charlotte-bronte www.enotes.com]
*[http://www.victorianweb.org/authors/bronte/cbronte/brontbio.html www.victorianweb.org]
*[http://madbibliophile.wordpress.com/2010/03/13/review-the-professor-by-charlotte-bronte-1857/ madbibliophile.wordpress.com]
*[http://flyhigh-by-learnonline.blogspot.com/2010/01/professor-by-charlotte-bronte.html flyhigh-by-learnonline.blogspot.com]

{{Brontë sisters}}

{{DEFAULTSORT:Professor, The}}
[[Category:1857 novels]]
[[Category:Novels by Charlotte Brontë]]
[[Category:English novels]]
[[Category:Novels published posthumously]]
[[Category:19th-century British novels]]
[[Category:Novels set in schools]]