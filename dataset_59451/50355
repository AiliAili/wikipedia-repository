{{Infobox journal
| title = Acta Palaeontologica Polonica
| cover =
| editor = [[Richard L. Cifelli]], [[Jarosław Stolarski]]
| discipline = [[Paleontology]], [[paleobiology]]
| former_names =
| abbreviation = Acta Palaeontol. Pol.
| publisher = Institute of Paleobiology, [[Polish Academy of Sciences]]
| country = Poland
| frequency = Quarterly
| history = 1956–present
| openaccess = Yes
| license = [[Creative Commons Attribution License]]
| impact = 1.949
| impact-year = 2010
| website = http://www.app.pan.pl/home.html
| link1 = http://www.app.pan.pl/issue.html?issue=current
| link1-name = Online access
| link2 = http://www.app.pan.pl/archives.html
| link2-name = Online archives
| JSTOR =
| OCLC = 02051833
| LCCN = 60040714
| CODEN = APGPAC
| ISSN = 0567-7920
| eISSN = 1732-2421
}}
'''''Acta Palaeontologica Polonica''''' is a quarterly [[Peer review|peer-reviewed]] [[open access journal|open access]] [[scientific journal]] of [[paleontology]] and [[paleobiology]]. It was established by [[Roman Kozłowski]] in 1956. It is published by the Institute of Paleobiology of the [[Polish Academy of Sciences]] and [[Editor-in-chief|edited]] by [[Richard L. Cifelli]] and [[Jarosław Stolarski]].

== Abstracting and indexing ==
''Acta Palaeontologica Polonica'' is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Biological Abstracts]]
*[[Current Contents]]/Physical, Chemical and Earth Sciences
*[[GeoArchive]]
*[[GEOBASE (database)|Geological Abstracts]]
*[[GeoRef]]
*[[PASCAL (database)|PASCAL]]
*[[Petroleum Abstracts]]
*[[Polish Scientific Journals Contents]]
*[[Referativny Zhurnal]]
*[[Research Alert]]
*[[Science Citation Index|Science Citation Index Expanded]]
*[[The Zoological Record]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 1.949, ranking it 11th out of 48 journals in the category "Paleontology".<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Paleontology |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
{{commons category|Media from Acta Palaeontologica Polonica|Acta Palaeontologica Polonica}}
* {{Official website|http://www.app.pan.pl/home.html}}
* [http://www.paleo.pan.pl/ Institute of Paleobiology]

[[Category:Paleontology journals]]
[[Category:Publications established in 1956]]
[[Category:Polish Academy of Sciences academic journals]]
[[Category:Open access journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]