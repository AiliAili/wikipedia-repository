{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Around the Moon
| title_orig    = Autour de la Lune
| translator    = [[Lewis Page Mercier|Louis Mercier]] & [[Eleanor Elizabeth King|Eleanor E. King]] (1873); [[Edward Roth]] (1874); [[Thomas H. Linklater]] (1877); [[Idrisyn Oliver Evans|I. O. Evans]] (1959), [[Jacqueline Baldick|Jacqueline]] and [[Robert Baldick]] (1970), [[Harold Salemson]] (1970)
| image         = 'Around the Moon' by Bayard and Neuville 01.jpg
| caption = 
| author        = [[Jules Verne]]
| illustrator   = [[Émile Bayard|Émile-Antoine Bayard]] and [[Alphonse-Marie-Adolphe de Neuville|Alphonse-Marie de Neuville]]
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #7
| genre         = [[Science fiction]] novel
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 1870
| english_pub_date = 1873
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| preceded_by   = [[Twenty Thousand Leagues Under the Sea]]
| followed_by   = [[A Floating City]]
}}
'''''Around the Moon''''' ({{lang-fr|'''Autour de la Lune'''}}, 1870), [[Jules Verne]]'s sequel to ''[[From the Earth to the Moon]]'', is a [[science fiction]] novel which continues the trip to the moon which was only partially described in the  previous novel. It was later combined with ''From the Earth to the Moon'' to create ''A Trip to the Moon and Around It''. ''From the Earth to the Moon'' and ''Around the Moon'' served as the basis for the film ''[[A Trip to the Moon]]''.

==Plot==

Having been fired out of the giant [[Columbiad]] [[space gun]], the Baltimore Gun Club's bullet-shaped projectile, along with its three passengers, Barbicane, Nicholl and Michael Ardan, begins the five-day trip to the moon. A few minutes into the journey, a small, bright asteroid passes within a few hundred yards of them, but does not collide with the projectile. The asteroid had been captured by the Earth's gravity and had become a second moon. 
[[File:'Around the Moon' by Bayard and Neuville 32.jpg|thumb|left|180px|An illustration from Jules Verne's novel "Around the Moon" drawn by [[Émile Bayard|Émile-Antoine Bayard]] and [[Alphonse de Neuville]], September 16, 1872]]

The three travelers undergo a series of adventures and misadventures during the rest of the journey, including disposing of the body of a dog out a window, suffering intoxication by gases, and making calculations leading them, briefly, to believe that they are to fall back to Earth. During the latter part of the voyage, it becomes apparent that the gravitational force of their earlier encounter with the asteroid has caused the projectile to deviate from its course.

The projectile enters lunar orbit, rather than landing on the moon as originally planned. Barbicane, Ardan and Nicholl begin geographical observations with [[opera glasses]]. The projectile then dips over the northern hemisphere of the moon, into the darkness of its shadow. It is plunged into extreme cold, before emerging into the light and heat again. They then begin to approach the moon's southern hemisphere. From the safety of their projectile, they gain spectacular views of [[Tycho (crater)|Tycho]], one of the greatest of all craters on the moon. The three men discuss the possibility of life on the moon, and conclude that it is barren. The projectile begins to move away from the moon, towards the '[[Lagrangian point|dead point]]' (the place at which the gravitational attraction of the moon and Earth becomes equal). Michel Ardan hits upon the idea of using the rockets fixed to the bottom of the projectile (which they were originally going to use to deaden the shock of landing) to propel the projectile towards the moon and hopefully cause it to fall onto it, thereby achieving their mission.

When the projectile reaches the point of neutral attraction, the rockets are fired, but it is too late. The projectile begins a fall onto the Earth from a distance of 160,000 miles, and it is to strike the Earth at a speed of 115,200 miles per hour, the same speed at which it left the mouth of the Columbiad. All hope seems lost for Barbicane, Nicholl and Ardan. Four days later, the crew of a US Navy vessel, [[USS Susquehanna (1850)|USS ''Susquehanna'']], spots a bright meteor fall from the sky into the sea. This turns out to be the returning projectile, and the three men inside are found to be alive and are rescued. They are treated to lavish homecoming celebrations as the first people to leave Earth.

== References ==
* {{cite web | url = http://epguides.com/djk/JulesVerne/works.shtml | title = ''The Works of Jules Verne'' | accessdate = August 27, 2006 | date = 13 July 2006 | first = Dennis | last = Kytasaari| archiveurl= https://web.archive.org/web/20060818230948/http://epguides.com/djk/JulesVerne/works.shtml| archivedate= 18 August 2006 <!--DASHBot-->| deadurl= no}}

== External links ==
{{wikisourcehas|the 1874 translation by Mercier & King)}}
{{wikisourcelang|fr|Autour de la Lune}}
{{Commons category|Around the Moon}}

* {{gutenberg|no=83|name=From the Earth to the Moon; and, Round the Moon}} &mdash; This is the original translation of Mercier and King published by Sampson Low et al. in 1873 and deletes about 20% of the original French text, along with numerous other errors.
* [http://jv.gilead.org.il/pg/round/ ''Round the Moon''] &mdash; Gut. text #83 in HTML format.
* [http://www.ibiblio.org/pub/docs/books/sherwood/R-II-d.htm ''Round the Moon''] &mdash; This is the original translation of Lewis Page Mercier and Eleanor E. King  published by Sampson Low et al. in 1873, revised and reconstituted by Christian Sánchez and Norman Wolcott. The parts Mercier and King left out are shown in red type.
* [http://www.gutenberg.org/etext/12901 Project Gutenberg's ''The Moon Voyage''] &mdash; This the version of both parts of ''Earth to the Moon'' and ''Round the Moon'' as published by Ward Lock in London in 1877. The translation is more complete than the Mercier version, but still has flaws, referring to the space capsule as a "bullet".
* [http://www.gutenberg.org/etext/16457 Project Gutenberg's ''All Around the Moon''] &mdash; This is the translation of Edward Roth first published in 1876 by King and Baird, Philadelphia. This translation has been vilified by Verne scholars for the large amount of additional non-Verne material included. However the book does contain the first printed corrected equation of motion for moon travel and also the first correct printed derivation of the formula for the escape velocity for a space capsule to leave the earth for the moon.
* [http://www.sil.si.edu/imagegalaxy/imageGalaxy_MoreImages.cfm?book_id=SIL28-090 Gallery of images], from the 1874 edition, from the [[Smithsonian Institution]]
* {{librivox book | title=Round the Moon | author=Jules Verne}}
*[[Image:Speaker Icon.svg|20px]] [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/verne-jules-autour-de-la-lune.html/ Autour de la lune, audio version] {{fr icon}}

{{Verne's Moon Novels |state=expanded}}
{{Verne}}

{{Authority control}}

{{DEFAULTSORT:Around The Moon}}
[[Category:1870 novels]]
[[Category:Novels by Jules Verne]]
[[Category:1870s science fiction novels]]
[[Category:Moon in fiction]]
[[Category:French science fiction novels]]
[[Category:Sequel novels]]
[[Category:Space exploration novels]]
[[Category:French novels adapted into films]]