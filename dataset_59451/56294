{{Infobox journal
| title = Clinical Risk
| cover = [[File:Clinical Risk journal front cover image.jpg]]
| editor = Hilary Merrett 
| discipline = [[Clinical practice]]
| former_names = 
| abbreviation = Clin. Risk
| publisher = [[SAGE Publications]]
| country = 
| frequency = Bimonthly
| history = 1995-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = https://sagepub.com/en-us/nam/journal/clinical-risk
| link1 = http://cri.sagepub.com/current.dtl
| link1-name = Online access
| link2 = http://cri.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1356-2622
| eISSN = 1758-1028
| OCLC = 48008875
| LCCN = 
}}
'''''Clinical Risk''''' is a bimonthly [[peer-reviewed]] [[medical journal]] covering the field of [[clinical practice]]. The [[editor-in-chief]] is Hilary Merrett ([[University of Brighton]]). It was established in 1995 and is published by [[SAGE Publications]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
*[[CINAHL]]<ref name=CINAHL>{{cite web |url=https://www.ebscohost.com/nursing/products/cinahl-databases/cinahl-complete |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2016-08-13}}</ref>
*[[EBSCO Information Services|EBSCO databases]]
*[[ProQuest|ProQuest databases]]
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-08-13}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|https://sagepub.com/en-us/nam/journal/clinical-risk}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Clinical practice journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1995]]