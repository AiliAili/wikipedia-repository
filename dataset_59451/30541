{{other people||William Cooley (disambiguation)}}
{{Use mdy dates|date=February 2012}}
{{Infobox person
| name = William Cooley
| image = William Cooley Attack+Cleaned.png
| caption = New River Massacre
| birth_name =
| birth_date = 1783
| birth_place = State of [[Maryland]]
| death_date = {{death year and age|1863|1783}}
| death_place = [[Hillsborough County, Florida|Hillsborough County]], Florida
| death_cause =
| residence =
| other_names =
| known_for = Florida Pioneer
| education =
| employer =
| occupation = [[Justice of the Peace]], Politician, Farmer, [[Merchant]], Soldier
| title =
| salary =
| networth =
| height =
| weight =
| term =
| predecessor =
| successor =
| party = [[Democratic Party (United States)|Democratic]]
| boards =
| religion =
| spouse = Nancy Dayton (c. 1805–1836)
| partner =
| children = Almonock Cooley {{nowrap|(c. 1827–1836)}}<br /> Montezuma Cooley (c. 1835–1836)<br />Unnamed Daughter (c. 1825–1836)
| parents =
| relatives =
| signature = William Cooley Signature.png
| website =
| footnotes =
}}

'''William Cooley''' (1783–1863) was one of the first American [[settler]]s, and a regional leader, in what is now known as [[Broward County, Florida|Broward County]], in the US state of Florida. His family was killed by [[Seminole]]s in 1836, during the [[Second Seminole War]]. The attack, known as the "New River Massacre", caused immediate abandonment of the area by whites.<ref name=BrowardLegacyKirk1>{{cite journal
 | author = Cooper Kirk
 |date=March 1976
 | title = William Cooley: A Broward Legend
 | journal = Broward Legacy
 | volume = 1
 | issue = 1
 | url = http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;q1=SN01480340;rgn=idno;subtype=bib;list=titles;sid=c5d122e177aadb6d033ea190302dc0c5;cc=fhp;idno=SN01480340_0001_001;node=SN01480340_0001_001%3A7;a=45;view=section;pdf=%2FDLData%2FSN%2FSN01480340%2F0001_001%2Ffile7.pdf
 | format = PDF
 | accessdate =June 22, 2007
|publisher=Broward County Historical Commission.
 }}</ref>

Cooley was born in [[Maryland]], but little else is known about his life prior to 1813, when he arrived in [[East Florida]] as part of a military expedition. He established himself as a farmer in the northern part of the territory before moving south, where he traded with local [[Indigenous peoples of the Americas|Indians]] and continued to farm. He sided with natives in a land dispute against a merchant who had received a large grant from the [[King of Spain]] and was evicting the Indians from their lands. Unhappy with the actions of the Spanish, he moved to the [[New River (Broward County, Florida)|New River]] area in 1826 to get as far as possible from the Spanish influence.<ref name=BrowardLegacyKirk1/>

In New River, Cooley sustained himself as a salvager and farmer, cultivating and milling [[Zamia integrifolia|arrowroot]]. His fortune and influence grew: he became the first lawman and judge in the settlement, besides being a land appraiser. Local Indians held him responsible for what they saw as a misjudgment involving the murder of one of their chiefs and attacked the settlement in revenge on January 4, 1836.<ref name=BrowardLegacyKirk1/>

Cooley survived the attack and lived for a further {{nowrap|twenty-seven years}}. He held administrative positions in [[Miami-Dade County, Florida|Dade County]],<ref name=BrowardLegacyKirk2>{{cite journal
 | author = Cooper Kirk
 |date=March 1976
 | title = William Cooley: A Broward Legend Part Two
 | journal = Broward Legacy
 | volume = 1
 | issue = 2
 | url = http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;q1=SN01480340;rgn=idno;subtype=bib;list=titles;sid=241ea938f42215f5f0ca4c8dfc361896;cc=fhp;idno=SN01480340_0001_002;node=SN01480340_0001_002%3A7;a=45;view=section;pdf=%2FDLData%2FSN%2FSN01480340%2F0001_002%2Ffile24.pdf
 | format = PDF
 | accessdate =June 22, 2007
|publisher=Broward County Historical Commission.
 }}</ref> moved to [[Tampa, Florida|Tampa]] in 1837, and had a short stint working for the [[United States Army|U.S. Army]] as a guide and courier.<ref name=BrowardLegacyKnetch1>{{cite journal
 | author = Joe Knetsch
 |date=March 1989
 | title = William Cooley explores the Everglades
 | journal = Broward Legacy
 | volume = 12
 | issue = 2
 | url=http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?xc=1;xg=1;q=cooley;rgn=full%20text;a=45;c=eew;c=ent;c=entarticle;c=feol;c=fhp;c=fhq;c=fshs;c=fshsarticle;c=ftl;c=jon;c=jonarticle;c=juv;c=mca;c=medi;c=mediarticle;c=misc;c=mundi;c=nema;c=nemaarticle;c=psa;c=rte;c=swf;c=usvi;c=ycb;sid=738e3290654748493dd622b273289d38;q1=cooley;cc=fhp;node=SN01480340_0012_001%3A6;idno=SN01480340_0012_001;view=section;pdf=%2FDLData%2FSN%2FSN01480340%2F0012_001%2Ffile6.pdf
 | format = PDF
 | accessdate =June 22, 2007
 |publisher=Broward County Historical Commission.
 }}</ref> He moved to the [[Homosassa Springs Wildlife State Park|Homosassa River]] area in 1840, where he became the first postmaster and was a [[Hernando County, Florida|Hernando County]] candidate for the [[Florida House of Representatives]]. Returning to Tampa in 1847, he was one of the first city councilors, serving {{nowrap|three terms}}<ref name=tampacouncil>{{cite web|title=Archives, Tampa City Council Members February 1856 – June 1904|url=http://www.tampagov.net/dept_City_Clerk/Information_resources/archives/previous_city_council_members/Tampa_City_Council_Members_February-1856%20_June-1904.asp
|accessdate=June 22, 2007}}</ref> before he died in 1863.<ref name=BrowardLegacyKirk2/><ref name=BrowardLegacyKnetch1/><ref name=tampacouncil/>

==Early life and arrival in East Florida==
Cooley was born in Maryland in 1783;<ref name=BrowardLegacyKirk1/><ref name=1850Census>[[:Image:Cooley 1850 Census.jpg]]</ref> little else is known about him prior to 1813. Cooley has been referred to as William Cooley Jr.,<ref name=TampaJournal1>{{cite journal
 | author = J. Allison DeFoor, II
 |date=March 1986
 | title = Odet Philippe in South Florida
 | journal = Tampa Bay History
 | volume = 8
 | issue = 1
 | url = http://www.lib.usf.edu/ldsu/digitalcollections/T06/journal/v08n1_86/v08n1_86_28.pdf
 | format = PDF
 | accessdate =June 22, 2007
 | quote = That Philippe was a contemporary of William Cooley, Jr., in the early settlement of New River is borne out by surviving records.
 | pages = 32
 }}</ref> William Coolie,<ref name=RiverGrass>{{cite book|title=The Everglades: River of Grass|author=Marjory Stoneman Douglas|publisher=Pineapple Press |year=1997 |origyear=1947 |chapter=9 |page=216 |url=https://books.google.com/?id=sPjDLXqemQ0C&pg=PA1&dq=The+Everglades:+River+of+Grass | isbn=978-1-56164-135-2}}</ref> William Colee<ref name=newtimes>{{cite news |last=Gaines |first=Jim |url=http://www.browardpalmbeach.com/2002-04-25/news/fort-nowhere/ |title=Fort Nowhere |date=April 25, 2002 |work=[[New Times Broward-Palm Beach]] |accessdate=June 24, 2007}}</ref> and William Cooly.<ref name=newtimes/>

Cooley arrived in [[East Florida]] in 1813, during a joint campaign of [[Tennessee]] and [[Georgia (U.S. state)|Georgia]] forces. Some sources give credit to the hypothesis that Cooley fought with the Tennessee Volunteers under [[John Williams (Tennessee)|Colonel John Williams]];<ref name=BrowardLegacyKirk1/><ref name=WarRecords>{{cite web|url=http://search.ancestry.com/search/db.aspx?dbid=4281|work=War of 1812 Service Records|location=Provo, UT|publisher=Ancestry.com|year=1999|author=National Archives and Records Administration|title=Index to the Compiled Military Service Records for the Volunteer Soldiers Who Served During the War of 1812|id=M602|quote=Name:William Cooley; Company:2nd Regiment West Tennessee Militia; Rank – Induction:Private; Rank – Discharge:Private; Roll Box:45; Roll Exct:602}} Although this is a good indicative, Colonel Williams was the leader of the Mounted Volunteers of East Tennessee; the 2nd Regiment West Tennessee Volunteer Mounted Gunmen was headed by Thomas Williamson. [http://tennessee.gov/tsla/history/military/1812reg.htm]</ref> other sources say he was a lieutenant<ref name=PioneersGeorgia>{{cite book|author=Folks Huxford|publisher=Folks Huxford|year=1951|pages=116–117;188–189|title=Pioneers of Wiregrass Georgia; a biographical account of some of the early settlers of that portion of Wiregrass Georgia embraced in the original counties of Irwin, Appling, Wayne, Camden, and Glynn|volume=1}} The references put him alternatively building forts to protect the [[Telfair County, Georgia|Telfair]] and [[Tattnall County, Georgia|Tattnall]] Counties in Georgia against Indians in 1813.</ref><ref name=bio1850CooleyLieutenant>{{cite book|title=Biographical census of Hillsborough County, Florida, 1850|author=Gordon, Julius J|publisher=J.J. Gordon|year=1989|accessdate=July 10, 2007|pages=112–113|url=http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;cc=fhp;q=Gordon%252C%2520Julius%2520J.;rgn=works;a=45;sid=a266c6de377f0b615c362dc36daeb7da;q1=Gordon%252C;rgn1=full%2520text;q2=Julius;op2=and;rgn2=full%2520text;q3=J.;op3=and;rgn3=full%2520text;node=SF00000358%3A6.3;idno=SF00000358;view=section;pdf=%2FDLData%2FSF%2FSF00000358%2Ffile9.pdf| format = PDF}}</ref> in the [[Georgia Militia]], fighting under Colonel Samuel Alexander<ref name=GeorgiaColonel>{{cite web|url=http://www.uflib.ufl.edu/EastFlorida/Card.asp?ID=23621|title=East Florida Papers-Col Samuel Alexander through Adjutant William Cooley|accessdate=June 24, 2007}} Colonel Samuel Alexander was part of the same campaign, but from the Georgia counterpart. The units from Tennessee and Georgia met in East Florida.</ref> from Georgia.<ref name=SamuelGeorgian>{{cite web|url=http://dlg.galileo.usg.edu/zlna/id%3Atcc132 |title=Letter enclosing a report, 1813 May 8, Twiggs County, (Georgia to) David B. Mitchell, Governor of Georgia, Milledgeville, Georgia – Samuel Alexander|author=Samuel Alexander|date=May 8, 1813|accessdate=July 11, 2007}}</ref> Cooley acquired property in Girt's Landing on the [[St. Marys River (Florida/Georgia)|St. Marys River]],<ref name=BrowardLegacyKirk1/> close to where the military units crossed [[East Florida]] that same year.<ref name=georgiainflorida>{{cite web|url=http://web.uflib.ufl.edu/digital/collections/teachers/time.htm|publisher=Florida Humanities Council|title=A Comparative Timeline of General American History and Florida History, 1492 to 1823|accessdate=July 11, 2007| archiveurl = https://web.archive.org/web/20080108031942/http://web.uflib.ufl.edu/digital/collections/teachers/time.htm| archivedate = January 8, 2008}}</ref><ref>{{cite web|url=http://tennessee.gov/tsla/history/military/tn1812.htm|title=Brief History of TN in the War of 1812 – East Florida Campaign|accessdate=June 22, 2007}}</ref> Later, he went to the west bank of the [[St. Johns River]], settling in an area {{convert|30|mi|km|0}} south of modern [[Jacksonville, Florida|Jacksonville]].<ref name=BrowardLegacyKirk1/>

Cooley later moved to Alligator Pond (near present-day [[Lake City, Florida|Lake City]], Florida), where he set up a farm and traded with the local [[Seminole]] tribe led by Chief [[Micanopy]].<ref name=BrowardLegacyKirk1/> The territory of [[East Florida]] was formally transferred from Spain to the United States in 1819, under the [[Adams-Onis Treaty]]. In 1820, Spanish merchant Don Fernando de la Maza Arredondo<ref name=arrgrant>{{cite web|url=http://growth-management.alachua.fl.us/historic/natural.htm|title= Natural and Historic Sites in Alachua County|accessdate=June 22, 2007| archiveurl = https://web.archive.org/web/20070618095407/http://growth-management.alachua.fl.us/historic/natural.htm| archivedate = June 18, 2007}}</ref> began settlement of a {{convert|280000|acre|km2|-1|sing=on}} claim in the [[Alachua County, Florida|Alachua territory]], which had been granted to him by [[Ferdinand VII of Spain|King Ferdinand VII of Spain]]. Cooley negotiated with Don Fernando on behalf of the displaced Indians but was unsuccessful.<ref name=BrowardLegacyKirk1/> Cooley moved away in 1823<ref name=BrowardLegacyKnetch1/>—possibly to escape the Spanish influence—to the north bank of the [[New River (Broward County, Florida)|New River]].<ref name=BrowardLegacyKirk1/>

{{clear}}

==New River settlement==
[[File:William Cooley Map.png|300px|left|thumb|William Cooley timeline, mapped]]

Like the other New River settlers, Cooley did not buy land; he simply occupied the land in hope that the United States would eventually survey the area and grant ownership to the present settlers. The settlement was primarily populated by [[Bahamas|Bahamians]], who survived by [[Turtling (hunting)|turtling]], fishing, [[shipbuilding]] and [[Wrecking (shipwreck)|wrecking]].<ref name=BrowardLegacyKirk1/>

In 1830, Frankee Lewis, who in 1788 had been one of the area's first settlers,<ref name=frankee>{{cite web|url=http://www.broward.org/history/timeline.htm|title=Broward Milestones|author=Broward County Historical Commission|accessdate=June 23, 2007| archiveurl = https://web.archive.org/web/20070427135734/http://www.broward.org/history/timeline.htm| archivedate = April 27, 2007}}</ref> sold her business interests to Richard Fitzpatrick. After Fitzpatrick's arrival, the settlement of approximately {{nowrap|70 people}} prospered with the introduction of a [[Plantation (settlement or colony)|plantation]] regimen based on [[Slavery in Colonial America#The first African slaves|black slavery]].<ref name=BrowardLegacyKirk1/>

Cooley's main occupation was gathering, processing and shipping [[arrowroot]], a starch made from the root of the [[Zamia integrifolia|coontie]] plant. Arrowroot was used to make bread dough, wafers and biscuits; its resistance to spoilage made it especially favored for use on ships. Pulp remaining after processing was used as a fertilizer or for animal rations. Favorable conditions for arrowroot cultivation contributed to the presence of several hundred Indians in the area—arrowroot being a staple of their diet.<ref name=BrowardLegacyKirk1/> The market price for the starch was {{nowrap|between 8¢ (US) and 16¢ per pound}} (between 17¢ and 35¢ per kg), and the geography of the river and the good performance of his machinery—the output was close to {{nowrap|450 lb (200 kg) per day}}—brought Cooley great prosperity. His good fortune allowed him to dedicate much of his time to exploration of the area as far north as [[Lake Okeechobee]] and brought him increasing political influence.<ref name=BrowardLegacyKirk1/> It is likely that he married Nancy Dayton, a former Indian captive, on December 2, 1830.<ref name=MarrRecord>{{cite web|url=http://www.ancestry.com|quote=WILLIAM COOLEY/NANCY DAYTON/2 Dec 1830/Monroe/FL|title=Dodd, Jordan R, et al. Florida Marriages, 1822–1850 (database on-line). Provo, UT|accessdate=June 22, 2007}}</ref><ref name=1830Census>[[:Image:Cooley 1830 Census.jpg]]</ref>
Richard Fitzpatrick, by that time the owner of a successful plantation with coconut and lime trees, [[Plantain (cooking)|plantain]]s, and [[sugarcane]], pressed for the appointment of Cooley as [[Justice of the Peace]] in 1831,<ref name=TerrBooksXXIV>{{cite book
 | last = Carter
 | first = Clarence Edwin
 | author2 = John Porter Bloom; United States. Dept. of State.
 | title = The Territorial papers of the United States (VOL XXIV)
 | edition = 1962
 | publisher = National Archives and Records Service
 | oclc = 23262569
 | quote = confirmed as of May 29, 1831
 | pages = 485
 | year = 1934
 }}</ref> making Cooley responsible for adjudicating disputes of persons and property, punishment of minor offenders by fines and whippings, and oversight of the activities of wreckers. Serious offenders were jailed in [[Key West]].<ref name=TerrBooksXXIV_2>{{cite book
 | last = Carter
 | first = Clarence Edwin
 | author2 = John Porter Bloom; United States. Dept. of State.
 | title = The Territorial papers of the United States (VOL XXIV)
 | edition = 1962
 | publisher = National Archives and Records Service
 | oclc = 23262569
 | pages = 417
 | year = 1934
 }}</ref> By that time, Cooley owned a [[schooner]] and took trips not only to take prisoners but to trade coontie, sugarcane, and tropical fruit with [[Cape Florida Light|Cape Florida]], [[Indian Key]], Key West and [[Havana]].<ref name=BrowardLegacyKirk1/>

[[File:Frankee Lewis Lands New River FL.PNG|thumb|Frankee Lewis lands, New River Settlement]]

While trade and farming activities were prominent, wrecking was the most important economic activity in the settlement. Northern newspapers started a campaign against wrecking in 1832, claiming that the activity was just a disguise for piracy; the {{nowrap|33 percent salvager's fee}} underscored their claim. Cooley, already in charge of overseeing [[Wrecking (shipwreck)|wrecking]], received a territorial appointment as [[appraiser]] of the sunk vessels and their cargoes. The strength of [[hurricane]] seasons affected the activity, and the especially active [[1830-1839 Atlantic hurricane seasons#1835 Atlantic hurricane season|1835 season]] brought even bigger profits.<ref name=BrowardLegacyKirk1/>

By 1835, Cooley had two sons and one daughter. The boys were named Almonock and Montezuma, after two local Indian chiefs. His ten-year-old daughter and his nine-year-old son were tutored by the couple Mary E. Rigby and Joseph Flinton.<ref name=BrowardLegacyKirk1/>

Cooley was appointed as an appraiser of property and [[slavery|slaves]] for Union Bank of Florida. His ally, Richard Fitzpatrick, purchased his coontie and [[citrus]] plantation on the Miami River for $2,500. Subsequently, Fitzpatrick was elected as representative for [[Monroe County, Florida|Monroe County]] to the [[Florida Legislature|Territorial Legislative Council]]. The unanimous vote for Fitzpatrick in New River was questioned by the ''Key West Inquirer''. Cooley's conduct was implicitly questioned as well, since as Justice of the Peace, Cooley conducted the non-secret [[ballot]]ing. In [[Key West, Florida|Key West]], Fitzpatrick lost to William Hackley.<ref name=BrowardLegacyKirk1/>

The Cooley property in New River had a house that was "twenty feet by fifty feet [6 by 15&nbsp;m], one story high, built of [[cypress]] logs, sealed and floored with 1-1/2 inch [4&nbsp;cm] planks".<ref name=BrowardLegacyKirk1/> At least three black slaves and several Indians cultivated sugar cane, corn, potatoes, pumpkins and other vegetables on the twenty-acre (eight-hectare) property, which also had a pen with eighty [[Pig|hogs]]. The coontie [[watermill]] was twenty-seven by fourteen&nbsp;feet {{nowrap|(eight by four meters)}}. His Key West holdings included a factory, two storage houses, kitchen and slave quarters; [[coconut]], [[Lime (fruit)|lime]] and orange trees; and domesticated and wild [[fowl]].<ref name=BrowardLegacyKirk1/>

==New River Massacre==

===Buildup===
{{see also|Second Seminole War|Dade Massacre}}
[[File:Wreckers Florida Keys.jpg|thumb|[[Florida Keys]] wreckers]]

Cooley maintained friendly relations and trade with the Seminole Indians in the area. In the early nineteenth century, [[Creek people|Creek Indians]] had moved from Alabama and joined the Seminoles. In 1835, white settlers killed Creek chief Alibama and burned his hut in a dispute. As Justice of the Peace, Cooley jailed the settlers, but they were released due to insufficient evidence after a hearing at the [[Monroe County, Florida|Monroe County]] Court in Key West. The Creek people blamed Cooley, saying he withheld evidence. The growing uneasiness between the Creeks and the whites led to the Creeks' emigration to the Okeechobee area.<ref name=BrowardLegacyKirk1/>

[[Francis L. Dade|Major Francis L. Dade]], military commandant at Key West, received intelligence that [[Cuba]] and Spain were arming the Indians; investigations did not confirm the rumor. Reports coming from [[Fort Brooke]], near present-day [[Tampa]], noted that Indians in the area were resisting orders from the federal government to emigrate to [[Mississippi]], contradicting the assertions made by the federal authorities that the Indians had agreed to emigrate peacefully. Dade, two companies of soldiers, and all of the available arms were sent to Fort Brooke at [[Tampa Bay]], the port designated for the commencement of the Indians' emigration. The Indians answered by concentrating all of their forces in the New River region.<ref name=BrowardLegacyKirk1/> On December 28, 1835, Dade and 107&nbsp;soldiers were ambushed en route from Tampa Bay to [[Fort King]], near present-day [[Ocala]]. Only three soldiers survived; the attackers lost only three men.<ref>{{cite book|title=Indian Wars of Mexico, Canada and the United States, 1812–1900|author=Bruce Vandervort|publisher=Bruce Vandervort |year=2006 |chapter=5 |pages=132–134|url=https://books.google.com/?id=VilwWL9RMywC&pg=PA132&dq=dade+massacre | isbn=978-0-415-22472-7}}</ref>

===Attack===
[[File:William Cooley Attack+Cleaned.png|thumb|Attack on New River Settlement in a contemporary rendition]]

Six days later, Cooley led a large expedition to free the ''Gil Blas'', a ship that had beached the previous year. The scale of the operation required all of the settlement's able men. On the next day, January 4, 1836, the Indians attacked the settlement.<ref name=BrowardLegacyKirk1/>

Between fifteen and twenty Indians invaded the Cooley house, overpowering the tutor and [[scalping]] him. Cooley's wife grabbed their infant son and tried to run to the river, but was shot about {{convert|170|yd|m|0}} from the house. The shot killed her and the baby. Cooley's nine-year-old son died from a fractured skull, and his daughter was shot. Two of Cooley's black slaves disappeared.<ref name=BrowardLegacyKirk1/><ref name=TrueAccount>{{cite book
 | title = A true and authentic account of the Indian war in Florida: giving the particulars respecting the murder of the Widow Robbins, and the providential escape of her daughter Aurelia, and her lover, Mr. Charles Somers, after suffering almost innumerable hardships. The whole compiled from the most authentic sources ...
 | url = http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;idno=FS00000034;sid=4875695f3783413a72efff07b427e089;rgn=full%20text;cc=fhp;node=FS00000034%3A4;a=45;view=section;pdf=%2FDLData%2FFS%2FFS00000034%2Ffile5.pdf
 | accessdate =June 25, 2007
 | publisher = Saunders & Van Welt
 | location = New York
 | pages = 10–11
 | chapter = 1
| format = PDF
 | chapterurl=http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;idno=FS00000034;sid=4875695f3783413a72efff07b427e089;rgn=full%20text;cc=fhp;node=FS00000034%3A4;a=45;view=section;pdf=%2FDLData%2FFS%2FFS00000034%2Ffile6.pdf
 | year = 1836
 }}</ref>

The tutor's son heard the screams by the river and came back to retrieve his mother and two younger sisters. He managed to escape, going south by boat to the [[Cape Florida Light]]house. Along the way, he warned the people at [[Arch Creek, Florida|Arch Creek]] and [[Miami River (Florida)|Miami River]] of the attack, prompting them to flee as well.<ref name=BrowardLegacyKirk1/>

===Aftermath===
{{see also|Cape Florida Light}}
[[File:Cape Florida 1830.jpg|thumb|Cape Florida Lighthouse c. 1830|right]]

After the attack, the Indians torched the house and left without attacking other dwellings. The next day, Cooley came back to bury the dead; it is unclear who alerted the salvager's team to the attack.<ref name=BrowardLegacyKirk1/> After staying at the settlement for three days, Cooley went to the [[Cape Florida Light]]house. One of the missing slaves appeared, reporting that he recognized the assailants as having been acquaintances of the Cooley family. The slave had heard the Indians ascribing the massacre to an act of revenge for Cooley's having failed to obtain the conviction of Chief Alibama's murderers.<ref name=BrowardLegacyKirk1/>

Cooley took charge of the [[lighthouse]] encampment. Richard Fitzpatrick sent sixty slaves from his Miami plantation to the lighthouse.<ref name=BrowardLegacyKirk2/> Fearing more attacks and aware of the precarious safety of the lighthouse, the settlers and slaves boarded Cooley's schooner and smaller boats and escaped to [[Indian Key]], {{convert|100|mi|km|0}} north of [[Key West]].<ref name=BrowardLegacyKirk2/> Judge Marvin, a Key West justice, accused Seminole (or [[Calusa]], depending on the source) chief Chakaika of leading the New River Settlement raiding group. This was not proved, but it is known that Chakaika was an important leader who coordinated the devastating attack on Indian Key in 1840.<ref name=FloridaQuarterly1>{{cite journal|author = George E. Buker|date=January 1979|title = The Mosquito Fleet's Guides and the Second Seminole War|journal = The Florida Historical Quarterly|volume = 57|publisher = Florida Historical Society|issue = 3|format = PDF|url = http://fulltext10.fcla.edu/cgi/t/text/text-idx?type=simple;sid=a266c6de377f0b615c362dc36daeb7da;xc=1;g=floridagrp;xg=1;q=cooley;rgn=div1;a=38;c=feol;c=fhp;c=fhq;c=ftl;c=mca;c=rte;c=swf;q1=cooley;ALLSELECTED=1;sort=occur;cc=fhq;node=SN00154113_0057_003%3A5;idno=SN00154113_0057_003;start=1;size=25;view=toc;subview=pdftoc
 |accessdate =July 10, 2007|pages = 311–326}}</ref>

When Cooley arrived at Indian Key, he was informed that Indians had attempted to acquire arms and munition but had been repelled by the garrison in the island's fort. Meanwhile, more than {{nowrap|two hundred people}} from nearby sought refuge in the fort. Cannons were salvaged from the ''Gil Blas'';<ref name=BrowardLegacyKirk2/> the ship was later burned to deny the Indians a chance to recover anything from it.<ref name=gilblasburn>{{cite journal
 | author = House of Representatives of the United States
 | date = June 30, 1856
 | title = A Century of Lawmaking for a New Nation: U.S. Congressional Documents and Debates, 1774–1875
 | journal = Bills and Resolutions, Senate, 34th Congress, 1st Session
 | volume = 34
 | url = http://memory.loc.gov/ll/llsb/034/1300/13520000.tif
 | format = TIF
 | accessdate =June 22, 2007
 | pages = 355
 }}</ref> Difficult sea conditions and fear of imminent attacks terrorized the islanders. Cooley asked for construction of [[forts]] at New River and [[Cape Sable]], but news soon came from the [[Miami River (Florida)|Miami River]] reporting the total destruction of all white property, stalling all new initiatives.<ref name=BrowardLegacyKirk2/>

Cooley went back to New River and discovered the Indians had returned to loot the settlement and had burned several other houses and plantations. A claim for restitution of his losses was denied in 1840 by the [[United States House of Representatives]].<ref name=congressclaim1>{{cite journal
 | author = House of Representatives of the United States
 | date = May 19, 1840
 | title = A Century of Lawmaking for a New Nation: U.S. Congressional Documents and Debates, 1774–1875
 | journal = Journal of the House of Representatives of the United States
 | volume = 34
 | url = http://memory.loc.gov/ll/llhj/034/0900/09630961.tif
 | format = TIF
 | accessdate =June 22, 2007
 | pages = 961
 }}</ref><ref name=congressclaim2>{{cite journal
 | author = House of Representatives of the United States
 | date = May 19, 1840
 | title = A Century of Lawmaking for a New Nation: U.S. Congressional Documents and Debates, 1774–1875
 | journal = Journal of the House of Representatives of the United States
 | volume = 34
 | url = http://memory.loc.gov/ll/llhj/034/1200/12591257.tif
 | format = TIF
 | accessdate =June 22, 2007
 | pages = 1257
 }}</ref> Arriving at Key West on January 16, 1836, aboard the [[steamboat]] ''Champion'', he was appointed temporary lighthouse keeper, staying until April of that year.<ref name=BrowardLegacyKirk2/>

==After New River==
[[File:Indian key map.jpg|thumb|Indian Key c. 1840]]

Cooley resumed his life as a wrecker. Later that same year, he worked again as justice of the peace and assumed a position as a legislatively-appointed auctioneer.<ref name=BrowardLegacyKirk2/>

Constant attacks and rumor-spreading amplified the demands of Floridian community leaders, forcing the Navy to send Lieutenant Levin M. Powell to Key West. Lieutenant Powell built a small force of {{nowrap|fifty seamen}}, {{nowrap|ninety-five marines}}, and {{nowrap|eight officers}}, reinforced by {{nowrap|two schooners}} and the United States Cutter
''Washington'', commanded by Captain Day. Powell called Cooley to be his guide in the enterprise because of his knowledge of Indian leaders and customs. Powell had mixed success, although by December 1836 the situation was under control at the coasts. Cooley went back to his usual duties in Indian Key ([[Miami-Dade County, Florida|Dade County]] Seat); not long after, he moved to Tampa<ref name=BrowardLegacyKirk2/> but still worked occasionally as a guide.

General [[Thomas Jesup]], headquartered in Fort Dade, made Cooley an express rider in early 1837 to deliver messages between [[Tampa Bay]] and [[Middleburg, Florida#History|Fort Heilman]], a corridor of {{convert|170|mi|km|-1}}.<ref name=BrowardLegacyKnetch1/> That same year, reports circulated that Cooley was spreading rumors about a Seminole chief leading a rebellion involving black slaves and Indians. Afraid that Cooley could be directly involved, the general had him interrogated. Afterwards, a disgusted Cooley resigned his position.<ref name=BrowardLegacyKirk2/>

{{clear}}

==Politician==
[[File:Cooley Store Cropped (Small).png|It is believed that this was Cooley's store in Tampa.|thumb]]

Cooley befriended Captain William Bunce, a retailer striving to keep Indians in the area, as they represented a source of cheap labor. He became involved again in local politics, this time against General Jesup, who wanted to remove all Indians from Florida. Judge Steele, a newcomer from Connecticut, was Cooley's ally in this fight.<ref name=BrowardLegacyKirk2/>

By 1840, he lived in [[Leon County, Florida|Leon County]], Florida, with a single slave.<ref name=1840Census>[[:Image:Cooley 1840 Census.jpg]]</ref> Cooley was living near the [[Homosassa Springs Wildlife State Park|Homosassa River]],<ref name=HernandoCounty>{{cite web|url=http://fivay.org/hernando1.html|title=A Hernando County Timeline (to 1887)|accessdate=June 23, 2007}}</ref> where the [[Armed Occupation Act]] of 1842 allowed the distribution of {{convert|160|acre|ha|0|sing=on}} land grants. His leadership enabled him to get not only his own permit but permits for {{nowrap|28 other settlers}}. A lengthy correspondence with the [[General Land Office]] was eventually concluded satisfactorily for him and the other settlers.<ref name=BrowardLegacyKnetch2>{{cite journal
 | author = Joe Knetsch
 |date=March 1993
 | title = William Cooley and the Land Office: A Note on Frontier Settlement
 | journal = Broward Legacy
 | volume = 16
 | issue = 2
 | url =http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;q1=SN01480340;rgn=idno;subtype=bib;list=titles;sid=794087550164d6bcbe5d0e2819ffd7ab;cc=fhp;a=45;idno=SN01480340_0016_001;node=SN01480340_0016_001%3A4;view=section;pdf=%2FDLData%2FSN%2FSN01480340%2F0016_001%2Ffile4.pdf
 | format = PDF
 | accessdate =June 22, 2007
|publisher=Broward County Historical Commission.
 }}</ref> In 1843, he was a candidate for a seat in the [[Florida House of Representatives]] for the newly created [[Hernando County, Florida|Hernando County]], but he lost to James Gibbons. Two years later, he became the first postmaster in [[Homosassa]]<ref name=HernandoCounty/> and County Commissioner of Fisheries.<ref name=fishnomination>{{cite journal|title=Journal of the Florida House of Representatives. 1845-11-17 – 1845-12-29|author=Florida House of Representatives|date=December 27, 1845|url=http://ufdc.ufl.edu/UF00027786/00005|accessdate=July 11, 2007|pages=238}}</ref> He sold his land grant to Senator [[David Levy Yulee]] sequentially between 1846 and 1847 and moved back to Tampa.<ref name=LandPatentSearch>{{cite web|url=http://www.glorecords.blm.gov/PatentSearch/|title=Land Patent Search, "Cooley, William" – Florida|accessdate=June 24, 2007}}<!-- Note: Zip code may be asked, but there is not a login. The user have to search for "Cooley, William" tog et the document. --></ref>

From 1848 to 1860, Cooley acquired several properties in the Tampa region,<ref name=LandPatentSearch/> including one at [[Safety Harbor, Florida#Early history|Worth's Harbor]].<ref name=TampaJournal1/> By 1850, he lived with seven slaves<ref name=1850Slave1>[[:Image:Cooley 1850 Slave Schedule.jpg]]</ref><ref name=1850Slave2>[[:Image:Cooley 1850 Slave Schedule 2.jpg]]</ref> and was a [[Captain (United States)|Captain]] of the "Silver Grays"—a [[militia]] for the home defense of Tampa in the 1850s.<ref name=TampaJournal3>{{cite journal
 | author = Joe Knetsch
 |date=June 1995
 | title = John Darling, Indian Removal, and Internal Improvements in South Florida, 1848–1956
 | journal = Tampa Bay History
 | volume = 17
 | issue = 2
 | url = http://kong.lib.usf.edu:1801/metsviewer/archobj?DOCCHOICE=80048.xml&dvs=1190402196496~266&locale=en&usePid1=true&usePid2=true
 | format = PDF
 | accessdate =June 24, 2007
 }}</ref> He owned a [[general store]] in the city, eventually sold to a member of the [[Masonic Temple No. 25|Tampa Masonic Lodge]].<ref name=bio1850>{{cite book|title=Biographical census of Hillsborough County, Florida, 1850|author=Gordon, Julius J|publisher=J.J. Gordon|year=1989|format = PDF|accessdate=July 10, 2007|url=http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?sid=a266c6de377f0b615c362dc36daeb7da;xc=1;g=floridagrp;xg=1;q=gordon;rgn=author;a=45;c=feol;c=fhp;c=fhq;c=ftl;c=mca;c=rte;c=swf;q1=gordon;ALLSELECTED=1;idno=SF00000358;cc=fhp;node=SF00000358%3A6.13;view=section;pdf=%2FDLData%2FSF%2FSF00000358%2Ffile19.pdf|pages=448}}</ref> He was nominated Port Warden of Tampa in 1853.<ref name=portnomination>{{cite journal|title=Journal of the proceedings of the House of Representatives. 1852-11-22|author=Florida House of Representatives|date=January 3, 1853|url=http://ufdc.ufl.edu/UF00027786/00005|accessdate=July 11, 2007|pages=286}}</ref> By 1855, Cooley had become a leader in local politics; he was the chairman at a meeting of the [[Democratic Party (United States)|Democratic Party]] in Tampa, with {{nowrap|sixty-five}} members enrolled, on August 4, 1855.<ref>{{cite web
 | title = The Know-Nothings of Hillsborough County
 | url = http://www.lib.usf.edu/ldsu/digitalcollections/S57/journal/v19n1_93/v19n1_93_003.pdf
 | format = PDF
 | accessdate =June 22, 2007
 }}</ref> He was brought in as an alternate councilman for two months in the first [[Tampa]] council, served a full-year term beginning in February 1857, and returned in 1861 for another full term.<ref name=tampacouncil/> Cooley estimated his personal wealth at $10,060 in 1860.<ref name=1860Census>[[:Image:Cooley 1860 Census.jpg]]</ref>

==Death and legacy==
Cooley died in 1863 in [[Hillsborough County, Florida]]. His [[will (law)|will]] was written in 1862 but recorded only after Cooley's death, filed by Francis Matthews, who identified himself as his son-in-law.<ref>[[:Image:Willian Cooley Preface Will.png]]</ref> In the document, Cooley is referred to as William ''Cooly''. Cooley left his estate to friends, charities, a woman called Fanny Anne listed as his daughter (wife of Francis Matthews), and three grandsons and four granddaughters,<ref>[[:Image:William Cooley Will.png]]</ref><ref name=bio1850CooleyHeirs>{{cite book|title=Biographical census of Hillsborough County, Florida, 1850|author=Gordon, Julius J|publisher=J.J. Gordon|year=1989|accessdate=July 10, 2007|pages=112–113|format = PDF|url=http://fulltext10.fcla.edu/cgi/t/text/pageviewer-idx?c=fhp;cc=fhp;q=Gordon%252C%2520Julius%2520J.;rgn=works;a=45;sid=a266c6de377f0b615c362dc36daeb7da;q1=Gordon%252C;rgn1=full%2520text;q2=Julius;op2=and;rgn2=full%2520text;q3=J.;op3=and;rgn3=full%2520text;node=SF00000358%3A6.3;idno=SF00000358;view=section;pdf=%2FDLData%2FSF%2FSF00000358%2Ffile9.pdf}} Although this source says that the heirs lived in his house in 1850, this is not confirmed by the 1850 census, which says he is the only free man in his house. The assertion he had a wife called Christen is not confirmed, and neither is the affirmation that two of his daughters survived the attack in 1836. He did not die in 1860.</ref> but there is no evidence that they were his blood relatives. Colee Hammock Park in Fort Lauderdale is located near the site of his old home in the New River Settlement.<ref name=newtimes/>

<gallery>
Image:William Cooley Armed Occupation Act.png |Cooley's Armed Occupation Act permit
Image:Cooley letter 1851.PNG |Letter to [[Thomas Brown (Florida politician)|Florida Governor Thomas Brown]], 1851
Image:William Cooley Buy Slave.PNG|Bill of Sale of a slave to Cooley, 1834
</gallery>

==See also==
*[[History of Fort Lauderdale, Florida]]

==References==
{{Reflist|30em}}

==External links==
*[http://fcit.usf.edu/Florida/docs/k/keys14.htm Floripedia: Key West: Indian Hostilities]

{{featured article}}

{{DEFAULTSORT:Cooley, William}}
[[Category:1783 births]]
[[Category:1863 deaths]]
[[Category:American people of the Seminole Wars]]
[[Category:Florida city council members]]
[[Category:Florida postmasters]]
[[Category:Florida settlers]]
[[Category:Florida Democrats]]
[[Category:History of Fort Lauderdale, Florida]]
[[Category:American justices of the peace]]