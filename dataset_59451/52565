{{Infobox journal
| cover = [[File:Physics of the Earth and Planetary Interiors.gif]]
| editor = S. D. Bader
| discipline = [[Planetary science]], [[geodesy]], [[geophysics]]
| abbreviation = Phys. Earth Planet. Inter.
| publisher = [[Elsevier]]
| country = 
| frequency = Biweekly
| history = 1967–present
| openaccess = 
| license = 
| impact = 2.319
| impact-year = 2011
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/503356/description#description
| link1 = http://www.sciencedirect.com/science/journal/00319201
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 878793
| LCCN = sf80001253
| CODEN = PEPIAM
| ISSN = 0031-9201
| eISSN = 
}}
'''''Physics of the Earth and Planetary Interiors''''', established in October 1967, is a biweekly [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Elsevier]]. The [[Editor-in-chief|co-editors]] are G. Helffrich ([[University of Bristol]]), K. Hirose ([[Tokyo Institute of Technology]]), M. Jellinek ([[University of British Columbia]]), and K. Zhang ([[University of Exeter]]).

The journal covers the physical and chemical processes of planetary interiors. Topical coverage broadly encompasses planetary physics, [[geodesy]], and [[geophysics]]. Publishing formats include [[original research|original research papers]], [[review article]]s, short communications and [[book review]]s on a regular basis. Occasional special issues are set aside for proceedings of conferences. 

The journal has a 2010 [[impact factor]] of 2.640.<ref>[[Web of Science]] database</ref>

==Abstracting and indexing==
This journal is indexed in the following bibliographic databases:<ref name=lccn>
{{Cite web
 | title =Catalog records (online) 
 | work =Bibliographic information for this journal 
 | publisher =Library of Congress 
 | date =September 2010 
 | url =http://lccn.loc.gov/sf80001253 
 | format =journal established October 1967 
 | accessdate =2010-09-30
}}</ref><ref name=CASSI>
{{Cite web
 | title =CAS Source Index (CASSI) 
 | work =Bibliographic information for this journal
 | format = journal established in October 1967
 | publisher =American Chemical Society 
 | date =September 2010 
 | url =http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfRM-MYn4b3in5mQM2hY0t4TLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfJtP_ZuUnT7QFGT2oteXk-g 
 | accessdate =2010-09-30
}}</ref><ref name=indexing>
{{Cite web
 | title =Abstracting and Indexing
 | work =List of databases which list this journal 
 | publisher =Elsevier
 | date =September 2010 
 | url =http://www.elsevier.com/wps/find/journalabstracting.cws_home/503356/abstracting 
 | format = 
 | accessdate =2010-09-30
}}</ref><ref name=Thom-Reu>
{{Cite web
 | title =Master Journal List (search) 
 | work =Bibliographic information for this journal 
 | publisher =Thomson Reuters
 | date =September 2010 
 | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0031-9201
 | format = 
 | accessdate =2010-09-30
}}</ref>

* [[Science Citation Index]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[Chemical Abstracts Service|Chemical Abstracts Service – CASSI]]
* [[GeoRef|AGI's Bibliography and Index of Geology]]
* [[GEOBASE (database)|GEOBASE]]
* [[Inspec]]
* [[PASCAL (database)|PASCAL]]
* [[Physics Abstracts]]
* [[Scopus]]

== See also ==
* ''[[Earth and Planetary Science Letters]]''
* ''[[Astronomy & Geophysics]]''

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/503356/description#description}}

[[Category:Geophysics journals]]
[[Category:English-language journals]]
[[Category:Biweekly journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1967]]