{{Infobox non-profit
| name            = Animal Liberation
| image           = 
| caption         = 
| founder         = Christine Townend
| type            = 
| tax_id          = 
| registration_id =
| founded_date    = 1976 <!-- {{Start date|YYYY|MM|DD}} -->
| location        = [[Sydney|Sydney, Australia]]
| coordinates     = <!-- {{Coord|LAT|LON|display=inline,title}} -->
| origins         = 
| key_people      = [[Lynda Stoner]]
| area_served     = 
| products        = 
| services        =
| focus           = [[Animal rights]]
| mission         = Working to end the suffering of exploited and confined animals through legislation, consumer advocacy, action and humane education
| method          = 
| revenue         = 
| endowment       = 
| num_volunteers  = 
| num_employees   = 
| num_members     = 
| subsid          = 
| motto           = 
| former name     =
| homepage        = http://www.animal-lib.org.au/
| dissolved       = 
| footnotes       = 
}}

'''Animal Liberation''' is a nonprofit animal rights organisation based in [[Sydney|Sydney, Australia]], founded by Christine Townend and led by current CEO [[Lynda Stoner]]. It was formed in 1976, one year after the release of ''[[Animal Liberation (book)|Animal Liberation]]'' by Australian philosopher [[Peter Singer]]. Animal Liberation's primary campaigns are to advocate against the use of animals for [[animal source foods|food]] ([[intensive animal farming|factory farmed]] or [[free range]]), [[animal fiber|clothing]], [[animal testing|research]], [[animals in sport|sport]] and [[entertainment#animals|entertainment]], by promoting a [[veganism|vegan]] lifestyle.

==Activities and campaigns{{anchor | Activities & Campaigns}}==
Animal Liberation's activities and campaigns include:

* Petitioning for political changes to prevent cruel practices against animals.
* Public protests for various issues including animal circuses, fur, live animal export, and rodeos.
* Direct street canvassing and distribution of organisation position materials.
* Investigating animal cruelty cases through calls received on an anonymous animal cruelty hotline.
* Festival stalls.
* Educational talks in schools, universities, and community groups.
* Investigation into cruel practices against animals for TV and print media [[investigative journalism|exposés]].
* Encouraging prosecution of perpetrators of animal abuse.

Animal Liberation continues to oppose [[live export]], with campaigns dating back over 30 years with original founder Christine Townend.<ref>{{cite news|last1=O'Sullivan|first1=Siobhan|title=The live export of animals will always be a bloody business|url=https://theconversation.com/the-live-export-of-animals-will-always-be-a-bloody-business-10547|accessdate=19 July 2015|work=The Conversation|date=6 November 2012}}</ref>

In 1995, [[Mark Pearson (politician)|Mark Pearson]] and 32 others chained themselves to sow cages at Parkville piggery, part-owned by then prime minister [[Paul Keating]]. The result of this action was a subsequent ban on the tethering of sows.<ref>{{cite news|last1=Munro|first1=Peter|title=Perched atop the pecking order: lunch with Animal Justice Party MP Mark Pearson|url=http://www.smh.com.au/nsw/perched-atop-the-pecking-order-lunch-with-animal-justice-party-mp-mark-pearson-20150522-gh1fxs.html|accessdate=19 July 2015|work=Sydney Morning Herald|date=23 May 2015}}</ref>

Animal Liberation maintains an active campaign against pig-dogging since its legal status was declared in 2006. In 2010, [[Lynda Stoner]] went undercover to a two-day workshop on pig-dog hunting to gather information and footage of the practice.<ref>{{cite news|last1=Jensen|first1=Erik|title=Going undercover to reveal the bloody truths of pig dogging|url=http://www.smh.com.au/environment/going-undercover-to-reveal-the-bloody-truths-of-pig-dogging-20100604-xkls.html|accessdate=19 July 2015|work=Sydney Morning Herald|date=5 June 2010}}</ref> Stoner has remained outspoken against all forms of hunting.  In May 2013, Stoner compared hunting photos to images of child pornography, bestiality, snuff murders, rape and torture.<ref>{{cite news|last1=Taylor|first1=Andrew|title=Death threats for artist and hunting model|url=http://www.smh.com.au/entertainment/art-and-design/death-threats-for-artist-and-hunting-model-20130504-2izpm.html#ixzz2SO2ObFi0|accessdate=19 July 2015|work=Sydney Morning Herald|date=5 May 2013}}</ref>

The organisation consistently holds protests outside circuses with performing animals, such as lions and monkeys. One goal of this campaign is to encourage local councils to amend their policies to exclude animal circuses from public land, such as in 2013 when members of Gosford City Council put forward a motion for a ban in that area.<ref>{{cite news|last1=Collins|first1=Terry|title=Protesters gather near Stardust Circus at Gosford to protest use of live animals|url=http://dailytelegraph.com.au/newslocal/central-coast/protesters-gather-near-stardust-circus-at-gosford-to-protest-use-of-live-animals/story-fngr8h0p-1227175634163|accessdate=19 July 2015|work=Daily Telegraph|date=6 January 2015}}</ref> In July 2013, circus campaign spokesperson Phillip Hall drew media attention to the life of captivity awaiting newborn lion cubs born into Stardust Circus.<ref>{{cite news|last1=Sharwood|first1=Anthony|title=Cruel or kind? Emotions run roar over captive circus lion cubs|url=http://www.news.com.au/national/cruel-or-kind-emotions-run-roar-over-captive-circus-lion-cubs/story-fncynjr2-1226678801730|accessdate=19 July 2015|work=news.com.au|date=13 July 2013}}</ref> The escape of a performing monkey from Lennon Bros Circus in July 2014 was also heavily criticized by Animal Liberation.<ref>{{cite news|last1=Barlass|first1=Tim|title=Circus monkey escapee prompts debate about exotic animals under the Big Top|url=http://www.smh.com.au/nsw/circus-monkey-escapee-prompts-debate-about-exotic-animals-under-the-big-top---20140726-zwtpn#ixzz38oppFKxe|accessdate=19 July 2015|work=Sydney Morning Herald|date=27 July 2014}}</ref>

Animal Liberation has spoken out against the pet industry breeding animals for profit in puppy farms, resulting in tens of thousands of dogs and cats being euthanised per year.<ref>{{cite news|title=The Tail End: Why are we killing so many pet dogs and cats?|url=http://www.sbs.com.au/news/insight/tvepisode/tail-end|accessdate=19 July 2015|work=SBS Insight|date=25 September 2012}}</ref>

==Investigations==
Animal Liberation has also conducted multiple undercover investigations on the conditions of animals raised for food in Australia.

In 2001 footage received by Animal Liberation from an unknown person contained images of the stunning and slaughter of possums at Lenah Game Meats in northern Tasmania. This was passed onto the [[Australian Broadcasting Corporation|Australian Broadcasting Corporation (ABC)]] who broadcast the footage. Lenah Game Meats took action against ABC, with the Supreme Court of Tasmania granting an injunction against the footage being broadcast again.<ref>{{cite news|last1=Tierney|first1=Judy|title=Court ruling allows footage obtained by trespass|url=http://www.abc.net.au/7.30/content/2001/s417973.htm|accessdate=19 July 2015|work=The 7.30 Report|date=15 November 2001}}</ref> This decision was overturned on appeal by the [[High Court of Australia|High Court]] in ''Australian Broadcasting Corporation v Lenah Game Meats Pty Ltd'' [2001] HCA 63.<ref>{{cite web|title=ABC v Lenah Game Meats Pty Ltd [2001] HCA 63|url=http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/2001/63.html|website=Austlii|accessdate=19 July 2015}}</ref> The judgment confirmed that journalists can use material obtained illegal by others in the public interest. The case is frequently cited as a landmark decision.

In February 2012 Animal Liberation gained footage shot undercover over six days at Hawkesbury Valley Abattoir, depicting animals having their throats slit without being properly stunned, and pigs pummelled in the head with an iron bar. The footage was passed onto authorities, resulting in the New South Wales Food Authority closing the abattoir.<ref>{{cite news|last1=Cannane|first1=Steve|title=NSW abattoir closed over slaughter practices|url=http://www.abc.net.au/lateline/content/2012/s3427271.htm|accessdate=19 July 2015|work=ABC Lateline|date=9 February 2012}}</ref>

Later the same year in August an investigation conducted over three months at Wally's Piggery at Murrumbateman captured footage of illegal animal husbandry practices, and back-room slaughter at a facility that was not an authorised slaughterhouse.<ref>{{cite news|title=Piggery footage reveals horrendous cruelty|url=http://www.abc.net.au/news/2012-08-03/leaked-footage-prompts-piggery-raid/4176424|accessdate=19 July 2015|work=ABC News|date=4 August 2012}}</ref> Animal cruelty charges were laid following a raid as a result of the footage, but all charges were subsequently dropped in 2014.<ref>{{cite news|last1=Belot|first1=Harry|title=Animal cruelty charges dropped against now defunct Wally's Piggery|url=http://www.smh.com.au/environment/animals/animal-cruelty-charges-dropped-against-now-defunct-wallys-piggery-20141121-11r8c9.html|accessdate=19 July 2015|work=Sydney Morning Herald|date=21 November 2014}}</ref>

In June 2012 Campaign Director Emma Hurst released footage from an investigation into duck farming sheds from so-called free range duck meat producers. The footage showed ducks confined indoors without access to sunlight or ground water.<ref>{{cite news|last1=Herbert|first1=Bronwyn|title=Disturbing footage prompts calls for duck farming changes|url=http://www.abc.net.au/7.30/content/2012/s3528922.htm|accessdate=19 July 2015|work=The 7.30 Report|date=19 June 2012}}</ref> This investigation formed the basis of complaints to the [[Australian Competition and Consumer Commission|Australian Competition and Consumer Commission (ACCC)]] against two large duck producers. In December 2012, the Federal Court ordered Pepe's Ducks to pay $400,000 for false, misleading and deceptive conduct.<ref>{{cite news|title=Pepe's Ducks to pay $400,000 arising from false, misleading and deceptive conduct|url=https://www.accc.gov.au/media-release/pepes-ducks-to-pay-400000-arising-from-false-misleading-and-deceptive-conduct|accessdate=19 July 2015|work=Australian Competition and Consumer Commission|date=19 December 2012}}</ref> Luv-a-Duck was ordered to pay $360,000 for misleading claims.<ref>{{cite news|title=Court orders Luv-a-Duck to pay $360,000 for misleading claims|url=http://www.accc.gov.au/media-release/court-orders-luv-a-duck-to-pay-360000-for-misleading-claims|accessdate=19 July 2015|work=Australian Competition and Consumer Commission|date=1 November 2013}}</ref>

In 2013 Animal Liberation received 140 hours of footage taken at an Inghams turkey processing plant in Tahmoor over a two-week period. The videos contained scenes of turkeys being bashed, kicked and stomped on. The footage sparked a renewed call from Animal Liberation for mandatory CCTV monitoring in all Australian abattoirs.<ref>{{cite news|last1=Levy|first1=Megan|title='Torture for fun': police given shocking abattoir footage|url=http://www.smh.com.au/national/torture-for-fun-police-given-shocking-abattoir-footage-20130320-2ggm2.html|accessdate=19 July 2015|work=Sydney Morning Herald|date=21 March 2013}}</ref>

==Notable members==

Current Chief Executive Officer [[Lynda Stoner]] was previously known for several roles on [[Australian television]] in the late 1970s and early 1980s. Stoner had leading regular roles in the soap opera ''[[The Young Doctors]]'' from 1977 to 1979 and followed this with the police drama  ''[[Cop Shop]]''.<ref>[http://blog.televisionau.com/2009/04/1979-april-21-27.html "Busted!  Linda Stoner’s day in a real cop shop "], ''Television.au'' 21 April 1979. Retrieved 10 September 2013.</ref> In 1985 she played the glamorous villain Eve Wilder in the cult soap opera ''[[Prisoner (TV series)|Prisoner]]'' and her character was spectacularly lynched during the infamous episode 600 riot, screened in 1986. This was followed by a guest role in the raunchy drama serial ''[[Chances (TV series)|Chances]]'' in 1991 where she played a sex therapist. Stoner became a prominent spokesperson for animal rights issues in the early 1980s and subsequently left acting to focus on this area.

[[Mark Pearson (politician)|Mark Pearson MLC]] formerly served as Executive Director of Animal Liberation. His main focus was on farm animals, particularly [[factory farming]]. Pearson was involved in advocating for the abolition of [[mulesing]] in sheep and convincing the [[Russian Federation]] to ban imports of kangaroo meat by exposing cruelty and hygiene problems.<ref name="DT election">{{cite news|url=http://www.dailytelegraph.com.au/news/animal-rights-campaigner-mark-pearson-beats-no-land-tax-partys-peter-jones-to-seat-in-the-upper-house/story-fni0cx4q-1227308058251|title=Animal Rights campaigner Mark Pearson beats No Land Tax Party’s Peter Jones to seat in the Upper House|work=[[Daily Telegraph (Australia)|The Daily Telegraph]]|location=Australia|publisher=|date=17 April 2015|accessdate=17 April 2015|author=Wood, Alicia}}</ref> He has been a Member of the [[New South Wales Legislative Council]] since 2015, representing the [[Animal Justice Party]], giving the party its first parliamentary representation.<ref name=nswlc>{{cite news|title=NSW Election 2015: Animal Justice Party wins seat in NSW Upper House|url=http://www.abc.net.au/news/2015-04-17/animal-justice-party-wins-seat-in-nsw-upper-house/6400492|accessdate=17 April 2015|work=ABC News|date=17 April 2015}}</ref>

==References==
{{reflist}}

==External links==
* [http://www.animal-lib.org.au/ Official website]

[[Category:Animal rights advocates]]
[[Category:Animal rights organisations]]
[[Category:Animal welfare organisations in Australia]]
[[Category:1976 establishments in Australia]]