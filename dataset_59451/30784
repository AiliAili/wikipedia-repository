{{Use dmy dates|date=July 2013}}
{{Infobox religious biography
|background     = #FFA500
|name           = <small>[[Dharma name|Thích]]</small><br/>Quảng Đức
|image          = Thich Quang Duc.png
|religion       = [[Mahayana|Mahayana Buddhism]]
|alias          = Bồ Tát Thích Quảng Đức<br/>(''Bodhisattva Thích Quảng Đức'')
|location       = [[South Vietnam]]
|Title          = [[bhikkhu|Buddhist monk]]
|Period         = 1917–1963
|ordination     = 1917
|post           = Chairman of the Panel on Ceremonial Rites of the Congregation of Vietnamese Monks<br/>Abbot of the Phước Hòa pagoda
|birth_date     = 1897
|birth_place    = [[Hội Khánh]], [[French Indochina]]
|death_date     = {{Death date and age|df=yes|1963|6|11|1897}}
|death_place    = [[Saigon]], [[South Vietnam]]}}<!-- Saigon was not officially called Ho Chi Minh City in 1963!! -->

{{hatnote|Thích is a Buddhist honorary title and ''Quảng Đức'' is descriptive of meritorious attributes: see [[dharma name]].}}
'''Thích Quảng Đức''' ({{IPA-vi|tʰǐc kʷâːŋ ɗɨ̌k|lang}}; 1897{{mdash}}11 June 1963, [[given name#Née|born]] '''Lâm Văn Túc'''), was a [[Vietnam]]ese [[Mahayana|Mahayana Buddhist]] [[bhikkhu|monk]] who [[self-immolation|burned himself to death]] at a busy [[Ho Chi Minh City|Saigon]] road intersection on 11 June 1963.<ref>"Monk Suicide by Fire in Anti-Diem Protest," ''New York Times'', 11 June 1963, 6.; David Halberstam, "Diem Asks Peace in Religion Crisis," ''New York Times'' 12 June 1963. 3.; Marilyn B. Young, ''The Vietnam Wars: 1945-1990'', New York: Harper Collins Publishers, 1990. 95-96.</ref> Quang Duc was protesting the persecution of Buddhists by the [[South Vietnam]]ese government led by [[Ngô Đình Diệm]]. Photographs of his self-immolation were circulated widely across the world and brought attention to the policies of the Diệm government. [[John F. Kennedy]] said in reference to a photograph of Đức on fire, "No news picture in history has generated so much emotion around the world as that one."<ref>Zi Jun Toong, "Overthrown by the Press: The US Media's Role in the Fall of Diem," ''Australasian Journal of American Studies'' 27 (July 2008), 56-72.</ref> [[Malcolm Browne]] won a [[Pulitzer Prize]] for his photograph of the monk's death.<ref name="k297"/><ref name="j148">
{{harvnb|Jacobs|2006|p=148}}</ref>

Quảng Đức's act increased international pressure on Diệm and led him to announce reforms with the intention of mollifying the Buddhists. However, the promised reforms were not implemented, leading to a deterioration in the dispute. With protests continuing, the [[Army of the Republic of Vietnam Special Forces|ARVN Special Forces]] loyal to Diệm's brother, [[Ngô Đình Nhu]], launched [[Xá Lợi Pagoda raids|nationwide raids]] on Buddhist [[pagoda]]s, seizing Quảng Đức's heart and causing deaths and widespread damage. Several Buddhist monks followed Quảng Đức's example, also immolating themselves. Eventually, an [[Army of the Republic of Vietnam|Army]] [[1963 South Vietnamese coup|coup]] toppled Diệm, who was [[Arrest and assassination of Ngo Dinh Diem|assassinated]] on 2 November 1963.

==Biography==
Accounts of the life of Quảng Đức are derived from information disseminated by Buddhist organizations. He was born in the village of [[Hội Khánh]], in [[Vạn Ninh District]] of [[Khánh Hòa Province]] in central Vietnam as '''Lâm Văn Túc''', one of seven children of Lâm Hữu Ứng and his wife, Nguyễn Thị Nương. At the age of seven, he left to study Buddhism under Hòa thượng <!--is this a title? please translate?--><!-- I added a footnote above --> Thích Hoằng Thâm, who was his maternal uncle and spiritual master. Thích Hoằng Thâm raised him as a son and Lâm Văn Túc changed his name to '''Nguyễn Văn Khiết'''. At age 15, he took the [[Śrāmaṇera|samanera]] ([[novice]]) vows and was ordained as a [[bhikkhu|monk]] at age 20 under the [[dharma name]] '''Thích Quảng Đức'''. The [[Vietnamese name]] ''Thích'' ([[wikt:釋|釋]]) is from "Thích Ca" or "Thích Già" ([[wikt:釋迦|釋迦]]), means "of the [[Shakya]] clan."<ref name="Dung, Thay Phap 2006">{{cite web |url=http://orderofinterbeing.org/docs/TNH-Tradition-Lineage.pdf |title=A Letter to Friends about our Lineage |author=Dung, Thay Phap |year=2006 |work=PDF file on the Order of Interbeing website |publisher= |accessdate=23 November 2014}}</ref> After ordination, he traveled to a mountain near [[Ninh Hòa]], vowing to live the life of a solitary Buddhism-practicing [[hermit]] for three years. He returned in later life to open the Thien Loc pagoda at his mountain retreat.<ref name="qd">{{harvnb|Thích Nguyên Tạng|2005}}.</ref><ref name="hm">{{harvnb|Huỳnh Minh|2006|pp=266–267}}.</ref>

After his self-imposed isolation ended, he began to travel around central Vietnam expounding the [[dharma]]. After two years, he went into [[retreat (spiritual)|retreat]] at the Sac Tu Thien An pagoda near [[Nha Trang]]. In 1932, he was appointed an inspector for the Buddhist Association in Ninh Hòa before becoming the inspector of monks in his home province of Khánh Hòa. During this period in central Vietnam, he was responsible for the construction of 14 temples.<ref name="hm2">{{harvnb|Huỳnh Minh|2006|p=268}}</ref> In 1934, he moved to southern Vietnam and traveled throughout the provinces spreading Buddhist teachings. During his time in southern Vietnam, he also spent two years in [[Cambodia]] studying the [[Theravada]] Buddhist tradition.

After his return from Cambodia, he oversaw the construction of a further 17 new temples during his time in the south. The last of the 31 new temples that he was responsible for constructing was the [[Avalokiteśvara|Quan The Am]] pagoda in the [[Phú Nhuận District]] of [[Gia Định Province]] on the outskirts of [[Saigon]].<ref name="hm2"/> The street on which the temple stands was later renamed Quảng Đức Street by Communists in 1975. After  the temple-building phase, Đức was appointed to serve as the Chairman of the Panel on Ceremonial Rites of the Congregation of Vietnamese Monks, and as [[abbot]] of the Phuoc Hoa pagoda, which was the initial location of the Association for Buddhist Studies of Vietnam (ABSV).<ref name="hm2"/> When the office of the ABSV was relocated to the [[Xá Lợi Pagoda]], the main pagoda of Saigon, Đức resigned.<ref name="qd"/>

==Self-immolation==

===Religious background===
{{Main|Huế Phật Đản shootings}}
[[File:Thích Quảng Đức Memorial.JPG|thumb|upright|A memorial to Quảng Đức located on the site of his death]]
In a country where surveys of the religious composition at the time estimated the Buddhist majority to be between 70 and 90 percent,<ref>{{harvnb|Gettleman|1966|pp=275–276, 366}}</ref><ref name="TIME1963">{{harvnb|Unattributed|1963a}}</ref><ref>{{harvnb|Tucker|2000|pp=49, 291, 293}}</ref><ref name="PentagonPapers">{{harvnb|Ellsberg|1963|pp=729–733}}</ref> President [[Ngo Dinh Diem|Diệm]] was a member of the [[Roman Catholicism in Vietnam|Catholic]] minority, and pursued discriminatory policies favoring Catholics for public service and military promotions, as well as in the allocation of land, business arrangements and tax concessions.<ref>{{harvnb|Tucker|2000|p=291}}</ref> Diệm once told a high-ranking officer, forgetting that the officer was from a Buddhist family, "Put your Catholic officers in sensitive places. They can be trusted."<ref name="gettle"/> Many officers in the [[Army of the Republic of Vietnam]] converted to Roman Catholicism as their military prospects depended on it.<ref name="gettle">{{harvnb|Gettleman|1966|pp=280–282}}</ref> Additionally, the distribution of firearms to village self-defense militias saw weapons given only to Roman Catholics, with some Buddhists in the army being denied promotion if they refused to convert to Roman Catholicism.<ref name="sv">{{harvnb|Harrison|1963b|p=9}}</ref>

Some Roman Catholic priests ran their own private armies,<ref>{{harvnb|Warner|1963|p=210}}</ref> and there were [[forced conversion]]s and looting, shelling, and demolition of pagodas in some areas, to which the government turned a blind eye.<ref>{{harvnb|Fall|1963|p=199}}</ref> Some Buddhist villages converted ''en masse'' to receive aid or avoid being forcibly resettled by Diệm's regime.<ref>{{harvnb|Buttinger|1967|p=993}}.</ref> The "private" status that was imposed on Buddhism by the French, which required official permission to be obtained by those wishing to conduct public Buddhist activities, was not repealed by Diệm.<ref>{{harvnb|Karnow|1997|p=294}}</ref> Catholics were also ''de facto'' exempt from [[corvée]] labor, which the government obliged all citizens to perform, and United States aid was distributed disproportionately to Catholic majority villages by Diệm's regime.<ref name="j91"/>

The [[Roman Catholic Church]] was the largest landowner in the country and enjoyed special exemptions in property acquisition, and land owned by the Roman Catholic Church was exempt from land reform.<ref>{{harvnb|Buttinger|1967|p=933}}.</ref> The white and gold [[Flag of Vatican City|Vatican flag]] was regularly flown at all major public events in South Vietnam,<ref name="crusade">{{harvnb|Harrison|1963a|pp=5–6}}</ref> and Diệm dedicated his country to the [[Mary (mother of Jesus)|Virgin Mary]] in 1959.<ref name="j91">{{harvnb|Jacobs|2006|p=91}}</ref>

Buddhist discontent erupted following a ban in early May on flying the [[Buddhist flag]] in [[Huế]] on [[Vesak]], the birthday of [[Gautama Buddha]]. Just days before, Catholics had been encouraged to fly the [[Flag of Vatican City|Vatican flag]] at a celebration for Archbishop [[Ngô Đình Thục]] of Huế, Diệm's elder brother. A large crowd of Buddhists protested the ban, defying the government by flying Buddhist flags on the Buddhist holy day of [[Vesak]] and marching on the government broadcasting station. Government forces fired into the crowd of protesters, killing nine people. Diệm's refusal to take responsibility — he blamed the [[Viet Cong]] for the deaths — led to further Buddhist protests and calls for religious equality.<ref>{{harvnb|Jacobs|2006|pp=140–50}}</ref> As Diem remained unwilling to comply with Buddhist demands, the frequency of protests increased.

Thích Quảng Đức had formed a suicide pact with a colleague in North Vietnam who subsequently carried out his pledge in protest of North Vietnamese mistreatment of Buddhists. Once the South Vietnamese Buddhist crisis began, Đức was urged by several fellow monks and the leaders of the Buddhist protests to fulfill his part of the pact.<ref>{{cite book |last=Shaw |first=Geoffrey |date=2015 |title=The Lost Mandate of Heaven: The American Betrayal of Ngo Dinh Diem, President of Vietnam |location=San Francisco, California, USA |publisher=Ignatius Press |page=216 |isbn=978-1-58617-935-9}}</ref>

===Day of the act===
[[File:Thích Quảng Đức self-immolation.jpg|300px|left|thumb|Journalist [[Malcolm Browne]]'s photograph of Quảng Đức during his [[self-immolation]]; a similar photograph won the 1963 [[World Press Photo of the Year]].<ref>{{harvnb|Browne|1963}}</ref>]]

On 10 June 1963, U.S. correspondents were informed that "something important" would happen the following morning on the road outside the Cambodian embassy in Saigon.<ref name="j147"/> Most of the reporters disregarded the message, since the [[Buddhist crisis]] had at that point been going on for more than a month, and the next day only a few journalists turned up, including [[David Halberstam]] of ''[[The New York Times]]'' and [[Malcolm Browne]], the Saigon bureau chief for the [[Associated Press]].<ref name="j147">{{harvnb|Jacobs|2006|p=147}}</ref> Đức arrived as part of a procession that had begun at a nearby pagoda. Around 350 monks and nuns marched in two phalanxes, preceded by an [[Austin Westminster|Austin Westminster sedan]], carrying banners printed in both English and Vietnamese. They denounced the Diệm government and its policy towards Buddhists, demanding that it fulfill its promises of religious equality.<ref name="j147"/> Another monk offered himself, but Đức's seniority prevailed.<ref name="k297">{{harvnb|Karnow|1997|p=297}}</ref>

The act occurred at the intersection{{ref label|b|b|none}} of Phan Đình Phùng Boulevard (now Nguyễn Đình Chiểu Street) and Lê Văn Duyệt Street (now Cách Mạng Tháng Tám Street) a few blocks Southwest of the Presidential Palace (now the Reunification Palace). Đức  emerged from the car along with two other monks. One placed a cushion on the road while the second opened the trunk and took out a five-gallon petrol can. As the marchers formed a circle around him, Đức calmly sat down in the traditional Buddhist meditative [[lotus position]] on the cushion. A colleague emptied the contents of the petrol container over Đức's head. Đức rotated a string of wooden [[prayer beads]] and recited the words ''{{lang|vi|[[Nianfo|Nam mô A Di Đà Phật]]}}'' ("Homage to [[Amitābha]] [[Buddhahood|Buddha]]") before the colleague struck a match and dropped it on the petrol trail leading to Đức. Flames consumed his robes and flesh, and black oily smoke emanated from his burning body.<ref name="j147"/><ref>{{harvnb|Jones|2003|p=268}}</ref>

Đức's last words before his [[self-immolation]] were documented in a letter he had left: {{quote|Before closing my eyes and moving towards the vision of the Buddha, I respectfully plead to President [[Ngô Đình Diệm]] to take a mind of compassion towards the people of the nation and implement religious equality to maintain the strength of the homeland eternally. I call the venerables, reverends, members of the [[Sangha (Buddhism)|sangha]] and the lay Buddhists to organize in solidarity to make sacrifices to protect Buddhism.<ref name="qd"/>}}

[[David Halberstam]] wrote: {{quote|I was to see that sight again, but once was enough. Flames were coming from a human being; his body was slowly withering and shriveling up, his head blackening and charring. In the air was the smell of burning human flesh; human beings burn surprisingly quickly. Behind me I could hear the sobbing of the Vietnamese who were now gathering. I was too shocked to cry, too confused to take notes or ask questions, too bewildered to even think ... As he burned he never moved a muscle, never uttered a sound, his outward composure in sharp contrast to the wailing people around him.<ref>{{harvnb|Halberstam|1965|p=211}}</ref>}}

[[File:Austin de monako.JPG|thumb|The car in which Quảng Đức traveled to his self-immolation; Huế, [[Thiên Mụ Pagoda]]. |alt=A higher resolution image of the car, on display at the temple, 15 December 2011.]]

The spectators were mostly stunned into silence, but some wailed and several began praying. Many of the monks and nuns, as well as some shocked passersby, prostrated themselves before the burning monk. Even some of the policemen, who had orders to control the gathered crowd, prostrated before him.<ref name="k297"/>

In English and [[Vietnamese language|Vietnamese]], a monk repeated into a microphone, ''"A Buddhist priest burns himself to death. A Buddhist priest becomes a martyr."'' After approximately ten minutes, Đức's body was fully immolated and it eventually toppled backwards onto its back. Once the fire subsided, a group of monks covered the smoking corpse with yellow robes, picked it up and tried to fit it into a coffin, but the limbs could not be straightened and one of the arms protruded from the wooden box as he was carried to the nearby Xá Lợi Pagoda in central Saigon. Outside the pagoda, students unfurled bilingual banners which read: ''"A Buddhist priest burns himself for our five requests."''<ref name="j147"/>

By 1:30 pm, around one thousand monks had congregated inside to hold a meeting while outside a large crowd of pro-Buddhist students had formed a human barrier around it. The meeting soon ended and all but a hundred monks slowly left the compound. Nearly one thousand monks, accompanied by [[Householder (Buddhism)|laypeople]], returned to the cremation site. The police lingered nearby. At around 6:00 pm, thirty nuns and six monks were arrested for holding a prayer meeting on the street outside Xá Lợi. The police encircled the pagoda, blocking public passage and giving observers the impression that an armed siege was imminent by donning riot gear.<ref name="j270">{{harvnb|Jones|2003|p=270}}</ref>

===Funeral and aftermath===
After the self-immolation, the U.S. put more pressure on Diệm to re-open negotiations on the faltering agreement. Diệm had scheduled an emergency cabinet meeting at 11:30 on 11 June to discuss the Buddhist crisis which he believed to be winding down. Following Đức's death, Diệm canceled the meeting and met individually with his ministers. Acting U.S. Ambassador to South Vietnam [[William Trueheart]] warned [[Nguyễn Đình Thuận]], Diệm's Secretary of State, of the desperate need for an agreement, saying that the situation was "dangerously near breaking point" and expected Diệm would meet the Buddhists' five-point manifesto. [[United States Secretary of State]] [[Dean Rusk]] warned the Saigon embassy that the [[White House]] would publicly announce that it would no longer "associate itself" with the regime if this did not occur.<ref>{{harvnb|Jones|2003|p=272}}.</ref> The [[Joint Communiqué]] and concessions to the Buddhists were signed on 16 June.<ref name="h149"/>

15 June was set as the date for the funeral, and on that day 4,000 people gathered outside the Xá Lợi pagoda, only for the ceremony to be postponed. On 19 June, his remains were carried out of Xá Lợi to a cemetery {{convert|16|km|mi|sp=us}} south of the city for a re-cremation and funeral ceremony. Following the signing of the Joint Communiqué, attendance was limited by agreement between Buddhist leaders and police to approximately 500 monks.<ref name="h149">{{harvnb|Hammer|1987|p=149}}.</ref>

===Intact heart and symbolism===
[[File:Thich quang duc heart.gif|left|thumb|upright|The heart [[relic]] of Quảng Đức]]
The body was [[cremation|re-cremated]] during the funeral, but Đức's heart remained intact and did not burn.<ref name="k297"/> It was considered to be holy and placed in a glass chalice at [[Xá Lợi Pagoda]].<ref name="j148"/> The intact heart [[Śarīra|relic]]<ref name="k297"/> is regarded as a symbol of compassion. Đức has subsequently been revered by Vietnamese Buddhists as a ''[[bodhisattva]]'' (''Bồ Tát''), and accordingly is often referred to in Vietnamese as Bồ Tát Thích Quảng Đức.<ref name="qd"/><ref name="hm1">{{harvnb|Huỳnh Minh|2006|p=266}}</ref> On 21 August, the [[Army of the Republic of Vietnam Special Forces|ARVN Special Forces]] of Nhu attacked Xá Lợi and other Buddhist pagodas across Vietnam. The secret police intended to confiscate Đức's ashes, but two monks had escaped with the urn, jumping over the back fence and finding safety at the U.S. Operations Mission next door.<ref>
{{harvnb|Jones|2003|pp=307–308}}</ref> Nhu's men managed to confiscate Đức's charred heart.<!-- What happened to the heart? --><ref>{{harvnb|Unattributed|1963b}}</ref>

The location chosen for the self-immolation, in front of the Cambodian embassy, raised questions as to whether it was coincidence or a symbolic choice. Trueheart and embassy official Charles Flowerree felt that the location was selected to show solidarity with the Cambodian government of Prince [[Norodom Sihanouk]]. South Vietnam and Cambodia had strained relations: in a speech on 22 May, Sihanouk had accused Diệm of mistreating Vietnamese and ethnic minority [[Khmer people|Khmer]] Buddhists. The pro-Diệm ''[[Times of Vietnam]]'' published an article on 9 June which claimed that Cambodian monks had been encouraging the Buddhist crisis, asserting it was part of a Cambodian plot to extend its neutralist foreign policy into South Vietnam. Flowerree noted that Diệm was "ready and eager to see a fine Cambodian hand in all the organized Buddhist actions".<ref>{{harvnb|Jones|2003|p=271}}.</ref>

===Diệm reaction===
[[File:Ngo Dinh Diem - Thumbnail - ARC 542189.png|thumb|Ngo Dinh Diem|alt=A portrait of a middle-aged man, looking to the left in a half-portrait/profile. He has chubby cheeks, parts his hair to the side and wears a suit and tie.]]
Diệm made a radio address at 19:00 on the day of Đức's death, asserting that he was profoundly troubled by the event. He appealed for "serenity and patriotism", and announced that stalled negotiations would resume with the Buddhists. He claimed that negotiations had been progressing well and in a time of religious tension emphasized the role of the Roman Catholic philosophy of [[personalism]] in his rule. He alleged that extremists had twisted the facts and he asserted that the Buddhists can "count on the Constitution, in other words, me."<ref name="j270"/>

The [[Army of the Republic of Vietnam]] responded to the appeal, putting on a show of solidarity behind Diệm to isolate dissident officers. Thirty high-ranking officers headed by General [[Lê Văn Tỵ]] declared their resolve to carry out all missions entrusted to the army for the defense of the constitution and the Republic. The declaration was a veneer which masked a developing plot to oust Diệm.<ref name="hammer 147"/> Some of the signatories were to become personally involved in Diệm's overthrow and death in November. Generals [[Dương Văn Minh]] and [[Trần Văn Đôn]], the presidential military advisor and the chief of the army who were to lead the coup, were overseas.<ref name="hammer 147">{{harvnb|Hammer|1987|p=147}}</ref>

[[Madame Nhu]], a Catholic convert from Buddhism and the wife of Diệm's younger brother and chief adviser [[Ngô Đình Nhu]], who was regarded as the First Lady of South Vietnam at the time (as Diệm was a bachelor), said she would "clap hands at seeing another monk barbecue show".<ref>{{harvnb|Langguth|2002|p=216}}</ref> Later that month, Diệm's government charged that Đức had been drugged before being forced to commit suicide.<ref>{{harvnb|Jones|2003|p=284}}</ref> The regime also accused Browne of bribing Đức to burn himself.<ref name="p309"/>

===Political and media impact===
Photographs taken by [[Malcolm Browne]] of the self-immolation quickly spread across the [[News agency|wire service]]s and were featured on the front pages of newspapers worldwide. The self-immolation was later regarded as a turning point in the [[Buddhist crisis]] and a critical point in the collapse of the Diệm regime.<ref name="j269">
{{harvnb|Jones|2003|p=269}}</ref>

Historian [[Seth Jacobs]] asserted that Đức had "reduced America's Diệm experiment to ashes as well" and that "no amount of pleading could retrieve Diệm's reputation" once Browne's images had become ingrained into the psyche of the world public.<ref name="j149">{{harvnb|Jacobs|2006|p=149}}</ref> [[Ellen Hammer]] described the event as having "evoked dark images of persecution and horror corresponding to a profoundly Asian reality that passed the understanding of Westerners."<ref name="h145">{{harvnb|Hammer|1987|p=145}}</ref> [[John Mecklin]], an official from the U.S. embassy, noted that the photograph "had a shock effect of incalculable value to the Buddhist cause, becoming a symbol of the state of things in Vietnam."<ref name="j269"/> [[William Colby]], then chief of the [[Central Intelligence Agency]]'s Far East Division, opined that Diệm "handled the Buddhist crisis fairly badly and allowed it to grow. But I really don't think there was much they could have done about it once that [[bhikkhu|bonze]] burned himself."<ref name="j269"/>

[[File:John F Kennedy.jpg|thumb|upright|U.S. President [[John F. Kennedy]] said that "no news picture in history has generated so much emotion around the world as that one."]]
[[File:RageAgainsttheMachineRageAgainsttheMachine.jpg|thumb|right|175px|''Rage Against the Machine'' album cover.]]
President [[John F. Kennedy]], whose government was the main sponsor of Diệm's regime, learned of Đức's death when handed the morning newspapers while he was talking to his brother, [[United States Attorney General|Attorney General]] [[Robert F. Kennedy]], on the phone. Kennedy reportedly interrupted their conversation about segregation in [[Alabama]] by exclaiming "Jesus Christ!" He later remarked that "no news picture in history has generated so much emotion around the world as that one."<ref name="j149"/> U.S. Senator [[Frank Church]] (D-ID), a member of the [[United States Senate Committee on Foreign Relations|Senate Foreign Relations Committee]], claimed that "such grisly scenes have not been witnessed since the [[Persecution of Christians in the Roman Empire|Christian martyrs]] marched hand in hand into the Roman arenas."<ref name="h145"/>

In Europe, the photographs were sold on the streets as postcards during the 1960s, and [[China|communist China]] distributed millions of copies of the photograph throughout Asia and Africa as evidence of what it called "US imperialism".<ref name=p309/><!--who is being quoted? -- the press release I guess. It didn't say --> One of Browne's photographs remains affixed to the sedan in which Đức was riding and is part of a tourist attraction in [[Huế]].<ref name="p309">{{harvnb|Prochnau|1995|p=309}}.</ref>
For Browne and the [[Associated Press]] (AP), the pictures were a marketing success. Ray Herndon, the [[United Press International]] (UPI) correspondent who had forgotten to take his camera on the day, was harshly criticized in private by his employer. UPI estimated that 5,000 readers in Sydney, then a city of around 1.5–2 million, had switched to AP news sources.<ref>{{harvnb|Prochnau|1995|p=316}}</ref>

Diệm's English-language mouthpiece, the ''Times of Vietnam'', intensified its attacks on both journalists and Buddhists. Headlines such as "Xá Lợi politburo makes new threats" and "Monks plot murder" were printed.<ref name=p320/> One article questioned the relationship between the monks and the press by posing the question as to why "so many young girls are buzzing in and out of Xá Lợi early [in the day]" and then going on to allege that they were brought in for sexual purposes for the U.S. reporters.<ref name=p320>{{harvnb|Prochnau|1995|p=320}}</ref>

Browne's award-winning photograph of Đức's death has been reproduced in popular media for decades, and the incident has been used as a touchstone reference in many films and television programs.

A still photograph of Đức's self-immolation taken by Browne was used for the cover of American [[rap metal]] band [[Rage Against the Machine]]'s [[Rage Against the Machine (album)|debut album]] which came out in 1992, as well as the cover of their single "[[Killing in the Name]]".

===Precedents and influence===
Despite the shock of the Western public, the practice of Vietnamese monks self-immolating was not unprecedented. Instances of self-immolations in Vietnam had been recorded for centuries, usually carried out to honor [[Gautama Buddha]]. The most recently recorded case had been in North Vietnam in 1950. The [[French Indochina|French colonial]] authorities had tried to eradicate the practice after their conquest of Vietnam in the nineteenth century, but had not been totally successful. They did manage to prevent one monk from setting fire to himself in Huế in the 1920s, but he managed to starve himself to death instead. During the 1920s and 1930s, Saigon newspapers reported multiple instances of self-immolations by monks in a matter-of-fact style. The practice had also been seen in the Chinese city of [[Harbin]] in 1948 when a monk seated down in the lotus position on a pile of [[sawdust]] and [[soybean]] oil and set fire to himself in protest against the treatment of Buddhism by the communists of [[Mao Zedong]]. His heart remained intact, as did that of Đức.<ref>{{harvnb|Hammer|1987|p=146}}</ref>

[[File:Venerable Thích Quảng Đức Monument where he performed his self-immolation.JPG|thumb|right|alt=statue in a small park|The ''Venerable Thich Quảng Đức Monument'' at the intersection where Quảng Đức performed his self-immolation, [[Phan Đình Phùng]] 
(now [[Nguyễn Đình Chiểu]]) Street and [[Lê Văn Duyệt]] (now [[August Revolution|Cach Mạng Thang Tam]]) Street ({{Coord|10.775159|106.686864|type:event|display=inline}})]]
After Đức, five more Buddhist monks self-immolated up until late October 1963 as the Buddhist protests in Vietnam escalated.<ref>{{harvnb|Jacobs|2006|pp=152, 168, 171}}.</ref> On 1 November, the [[Army of the Republic of Vietnam]] overthrew Diệm in a [[1963 South Vietnamese coup|coup]]. Diệm and Nhu were [[Arrest and assassination of Ngo Dinh Diem|assassinated the next day]].<ref>{{harvnb|Jacobs|2006|pp=173–180}}</ref> Monks have followed Đức's example since for other reasons.<ref>{{harvnb|Hammer|1987|p=318}}</ref>

Đức's actions were copied by United States citizens [[Opposition to the U.S. involvement in the Vietnam War|in protests against the Vietnam War]]:

* [[Norman Morrison]], a 31-year-old Quaker pacifist, poured [[kerosene]] over himself and set himself alight below the third-floor window of [[United States Secretary of Defense|Secretary of Defense]] [[Robert McNamara]] at [[the Pentagon]] on 2 November 1965. 
* [[Alice Herz]], an 82-year-old woman, also burned herself that year in [[Detroit|Detroit, Michigan]].<ref name="Zinn_2003_486">{{harvnb|Zinn|2003|p=486}}.</ref>
* [[Roger Allen LaPorte]] self-immolated outside the United Nations building in New York City on 9 November 1965. 
* [[Florence Beaumont]] burned herself to death outside the Federal Building in Los Angeles on 15 October 1967. 
* [[George Winne, Jr.]], a student, self-immolated on 10 May 1970 on the campus of the [[University of California, San Diego]] and died the following day.

In an apparently non-political case of imitation of Quảng Đức, the young son of an American officer based at the U.S. Embassy in Saigon doused himself with gasoline and set himself on fire. He was seriously burned before the fire was extinguished and later could only offer the explanation that "I wanted to see what it was like."<ref name="p310">{{harvnb|Prochnau|1995|p=310}}</ref>

{{Clear}}

==See also==
{{Portal|Vietnam|Biography|Buddhism|1960s}}
* [[List of civil rights leaders]]
* [[List of political self-immolations]]
* [[Nguyễn Văn Lém]]
* [[Phan Thi Kim Phuc]]

==Notes==
{{Refbegin}}
•&nbsp;a){{note label|a|a|none}} Hòa thượng means "The Most Venerable" in Vietnamese.

•&nbsp;b){{note label|b|b|none}} In the satellite image ({{Coord|10.775159|106.686864|type:event|display=inline}}) of the Saigon intersection where Quảng Đức performed his self-immolation, Phan Đình Phùng (now Nguyễn Đình Chiểu) Street runs NE-SW and Lê Văn Duyệt (now Cách Mạng Tháng Tám) Street runs NW–SE. On the western corner of the intersection stands a memorial to Quảng Đức. For many years a Petrolimex fuel station stood on the northern corner, but this was replaced with a memorial park for Quảng Đức.
{{Refend}}

==References==
<!--
  As per Wiki policy all should be in<ref> form.
  See http://en.wikipedia.org/wiki/Wikipedia:Footnotes for a
  discussion of different citation methods and how to generate
  footnotes using the <ref>, </ref> and<reference /> tags
------------------------------------------------------------- -->
{{Reflist|colwidth=19em}}

==Bibliography==
{{Refbegin}}
*{{Citation|authorlink=Malcolm Browne|last=Browne|first=Malcolm|title=World Press Photo 1963|publisher=World Press Photo|location=Amsterdam|year=1963|accessdate=23 October 2007|url= http://www.worldpressphoto.org/index.php?option=com_photogallery&task=view&id=170&Itemid=115&bandwidth=high}}
*{{Citation|title=Vietnam: A Dragon Embattled|authorlink=Joseph Buttinger|first=Joseph|last=Buttinger|year=1967|publisher=Praeger Publishers|url=https://books.google.com/?id=ZVMPAAAAMAAJ}}
*{{Citation|date=10 July 1963|editor-last=Ellsberg|editor-first=Daniel|chapter=The Situation in South Vietnam – SNIE 53-2-63|title=[[Pentagon Papers|The Pentagon Papers]], Gravel Edition, Volume 2|chapter-url=http://www.mtholyoke.edu/acad/intrel/pentagon2/doc125.htm|location=Boston|publisher=Beacon Press|accessdate=21 August 2007|editor-link=Daniel Ellsberg}}
*{{Citation|last=Fall|first=Bernard|title=The Two Viet-Nams|year=1963|publisher=Praeger Publishers|url=https://books.google.com/?id=MPVAAAAAIAAJ|author-link=Bernard B. Fall}}
*{{Citation|title=Vietnam: History, documents and opinions on a major world crisis|first=Marvin E.|last=Gettleman|year=1966|publisher=Penguin Books|location=New York|url=https://books.google.com/?id=6HUzAAAAMAAJ}}
*{{Citation|last=Halberstam|first=David|year=1965|title=The Making of a Quagmire|publisher=Random House|location=New York|authorlink=David Halberstam|url=https://books.google.com/?id=tXAaAAAAMAAJ}}
*{{Citation|title=A Death in November: America in Vietnam, 1963| first=Ellen J.|last=Hammer|year=1987|publisher=E. P. Dutton|location=New York City|isbn=0-525-24210-4|author-link=Ellen Hammer}}
*{{Citation|editor-last=Harrison|editor-first=Gilbert|title=Diem's other crusade|periodical=[[The New Republic]]|year=1963a|issue=22 June 1963}}
*{{Citation|page=9|editor-last=Harrison|editor-first=Gilbert|title=South Vietnam: Whose funeral pyre?|periodical=[[The New Republic]]|year=1963b|issue=29 June 1963}}
*{{Citation|first=Seth|last=Jacobs|year=2006|title=Cold War Mandarin: Ngo Dinh Diem and the Origins of America's War in Vietnam, 1950–1963| publisher=Rowman & Littlefield|location=Lanham| isbn=0-7425-4447-8}}
*{{Citation|first=Howard|last=Jones|year=2003|title=Death of a Generation: how the assassinations of Diem and JFK prolonged the Vietnam War|location=New York|publisher=Oxford University Press|isbn=0-19-505286-2}}
*{{Citation|last=Karnow|first=Stanley|title=Vietnam: A history|year=1997|location=New York|publisher=Penguin Books|isbn=0-670-84218-4|author-link=Stanley Karnow}}
*{{Citation|last=Langguth|first=A. J.|title=Our Vietnam: the war, 1954–1975|publisher=Simon & Schuster|location=New York|year=2002|isbn=0-7432-1231-2}}
*{{Citation|author=Huỳnh Minh|year=2006|title=Gia Định Xưa|place=Ho Chi Minh City|publisher=Văn Hóa-Thông Tin Publishing House|language=Vietnamese}}
*{{Citation|author=Thích Nguyên Tạng|title= Tiểu Sử Bổ Tát Thích Quảng Dức|url=http://quangduc.com/p4608a6669/7/tieu-su-bo-tat-thich-quang-duc|year=2005|publication-date=1 May 2005|accessdate=20 August 2007|publisher=Quảng Đức Monastery|location=Fawker|language=Vietnamese}}
*{{Citation|first=William|last=Prochnau|title=Once upon a Distant War|year=1995|location=New York|publisher=Times Books|isbn=0-8129-2633-1|author-link=William Prochnau}}
*{{Citation|first=Jerrod L.|last=Schecter|title=The New Face of Buddha: Buddhism and Political Power in Southeast Asia|year=1967|location=New York|publisher=Coward-McCann|url=https://books.google.com/?id=hxZDAAAAIAAJ}}
*{{Citation|first=Geoffrey |last=Shaw |date=2015 |title=The Lost Mandate of Heaven: The American Betrayal of Ngo Dinh Diem, President of Vietnam |location=San Francisco, California, USA |publisher=Ignatius Press |isbn=978-1-58617-935-9}}
*{{Citation|title=Encyclopedia of the Vietnam War|first=Spencer C.|last=Tucker|year=2000|publisher=ABC-CLIO|location=Santa Barbara|isbn=1-57607-040-9}}
*{{Citation|author=Unattributed|issue=14 June 1963|url=http://www.time.com/time/printout/0,8816,874816,00.html|title=The Religious Crisis|periodical=[[Time (magazine)|Time Magazine]]|accessdate=21 August 2007|date=14 June 1963a}}
*{{Citation|author=Unattributed|issue=30 August 1963| url=http://www.time.com/time/magazine/article/0,9171,940704-1,00.html|title=The Crackdown|periodical=[[Time (magazine)|Time Magazine]]|accessdate=23 October 2007|date=30 August 1963b}}
*{{Citation|last=Warner|first=Denis|title=The Last Confucian|year=1963|publisher=Macmillan|location=New York|url=https://books.google.com/?id=XGo1AAAAIAAJ}}
*{{Citation|first=Howard|last=Zinn|authorlink=Howard Zinn|title=[[A People's History of the United States]]|publisher=HarperCollins|location=New York|year=2003|isbn=0-06-052842-7}}
{{Refend}}

==External links==
{{Commons category|Thich Quang Duc}}
*[http://www.quangduc.com – Official Buddhist monastery dedicated to Thích Quảng Đức]

{{Buddhist crisis}}
{{Featured article}}

{{Authority control}}

{{DEFAULTSORT:Duc, Thich Quang}}
[[Category:1897 births]]
[[Category:1963 deaths]]
[[Category:Date of birth unknown]]
[[Category:People from Khanh Hoa Province]]
[[Category:Vietnamese Buddhist monks]]
[[Category:Buddhist crisis]]
[[Category:History of South Vietnam]]
[[Category:Mahayanan Buddhist monks]]
[[Category:Suicides in Vietnam]]
[[Category:Buddhist martyrs]]
[[Category:1963 in Vietnam]]
[[Category:Male suicides]]
[[Category:Filmed suicides]]
[[Category:Activists who committed suicide]]
[[Category:Vietnamese people of the Vietnam War]]
[[Category:Self-immolations by Buddhists]]
[[Category:Articles containing video clips]]
[[Category:Subjects of iconic photographs]]
[[Category:20th-century Buddhists]]
[[Category:Suicides by self-immolation]]