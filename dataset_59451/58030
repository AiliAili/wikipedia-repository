{{Infobox journal
| title = Journal of Consumer Research
| cover =
| discipline = [[Economics]]
| abbreviation = J. Cons. Res.
| editor = Darren Dahl, Eileen Fischer, Gita V. Johar, Vicki Morwitz
| publisher = [[Oxford University Press]]
| country = 
| frequency = Bimonthly
| history = 1974-present
| impact = 3.125
| impact-year = 2014
| website = http://www.ejcr.org/
| link1 = http://jcr.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://jcr.oxfordjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 0093-5301
| eISSN = 1537-5277
| JSTOR = 00935301
}}
The '''''Journal of Consumer Research''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering research on all aspects of consumer behavior, including [[psychology]], [[marketing]], [[sociology]], [[economics]], [[anthropology]], and communications. It was established in 1974 and originally published by the [[University of Chicago Press]]. Since 2015 it is published by [[Oxford University Press]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.125, ranking it 16th out of 115 journals in the category "Business".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Political Science |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.ejcr.org/}}

{{DEFAULTSORT:Journal of Consumer Research}}
[[Category:Oxford University Press academic journals]]
[[Category:Economics journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1974]]