'''Barbara Crane''' (born 1928) is an American artist photographer born in [[Chicago, IL]]. Crane works with a variety of materials including [[Instant film|Polaroid]], [[gelatin silver]], and [[platinum prints]] among others. She is known for her experimental and innovative work that challenges the [[straight photograph]] by incorporating [[sequencing]], [[layered negatives]], and [[repeated frames]]. [[Naomi Rosenblum]] notes that Crane "pioneered the use of repetition to convey the mechanical character of much of contemporary life, even in its recreational aspects."<ref>Rosenblum, Naomi. ''A History of Women Photographers.'' New York: Abbeville Press Publishers, 2000. 279.</ref>

Crane has been the recipient of [[scholarship|fellowship]]s and [[grant (money)|grant]]s, such as the [[Illinois Arts Council Artists Fellowship]] (2001), [[National Endowment for the Arts Grant]] (1988, 1974), [[Polaroid Corporation Materials Grants]] (1979-1995), and [[John Simon Guggenheim Memorial Fellowship in Photography]] (1979).<ref>Heller, Jules and Nancy G. Heller, eds. ''North American Women Artists of the Twentieth Century.'' New York: Routledge, 2013.</ref> Crane has also been an honored educator for the [[National Society for Photographic Education]] (1993); a distinguished artist at the [[Union League Club]] in Chicago, Illinois (2006); and a distinguished artist at [[Brown University]], Providence, Rhode Island (2006). Her awards also include the [[YWCA Outstanding Achievement Award]] (1987) and the [[Ruth Horwich Award to a Famous Chicago Artist]] (Chicago, Illinois, 2009). In 2013, Barbara Crane was named artist honoree at the [[Hyde Park Art Center]] in Chicago, Illinois.

Crane's work is represented in numerous public collections including the [[International Center for Photography, New York City]]; the [[International Museum of Photography, George Eastman House, Rochester]]; the [[Art Institute of Chicago]]; the [[J. Paul Getty Museum, Los Angeles]]; the [[National Museum of Modern Art, Kyoto, Japan]]; the [[Museum of Contemporary Art, Chicago]]; the [[Museum of Fine Arts, Houston]]; and the [[WestLicht Museum of Photography, Vienna, Austria]].<ref>Crane, Barbara. http://barbaracrane.desordre.net/</ref><ref>Heller, Jules and Nancy G. Heller, eds. ''North American Women Artists of the Twentieth Century.'' New York: Routledge, 2013.</ref>

Crane's archive resides at the [[Center for Creative Photography at the University of Arizona in Tucson, AZ]].

==Education and teaching career==
Crane studied at [[Mills College in Oakland, California]]

In 1950, she received her [[Bachelor of Arts|BA]] in art history from [[New York University]]. After recommencing her career in photography, Barbara Crane showed a portfolio of her work to [[Aaron Siskind]] in 1964 and was admitted to the Graduate Program in Photography at the [[Institute of Design at the Illinois Institute of Technology]].<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> Crane then studied under Siskind at the Institute of Design, Illinois Institute of Technology, and received her MS from the Institute in 1966.<ref>Heller, Jules and Nancy G. Heller, eds. ''North American Women Artists of the Twentieth Century.'' New York: Routledge, 2013.</ref><ref>Rosenblum, Naomi. ''A History of Women Photographers.'' New York: Abbeville Press Publishers, 2000. 317.</ref>

Crane’s [[master’s degree]] thesis focused on “sculptural patterns through abstractions of the human body.”<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> The images for this series depict bodies against white or black backgrounds – the overexposed, overdeveloped nature of the film turns these bodies into abstract outlines. John Rohrbach states, “each body almost dissolves, becoming a sinuous river flowing across a snowy landscape. This unnerving disconnect between what is seen and what is known would become a central theme of her career.”<ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>

While in the MA program at the [[Institute of Design at the Illinois Institute of Technology]], Crane was hired to start and chair the photography program at [[New Trier High School in Winnetka, Illinois]] where she taught for three years. Eighteen months after graduating from ID, she secured a position to teach photography at the [[School of the Art Institute of Chicago]] which she held from 1967 (becoming a full professor in 1978) through 1995.<ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> Throughout her teaching career, Crane also traveled as both a visiting professor and artist for institutions including [[Bezalel Academy of Art and Design in Jerusalem]] (1987), [[Cornell University in Ithaca, New York]] (1983), the [[School of the Museum of Fine Arts of Boston]] (1979), [[University of the Arts in Philadelphia]] (1977), and the [[Institute of Design at the Illinois Institute of Technology in Chicago]] (1969).

In 1971, Crane visited [[Ansel Adams]] at his home to show him a selection of her work. Adams told an assistant “See I told you photographers could still do something different” upon viewing her Repeats series.<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> After this encounter, Adams hired Crane to teach workshops at [[Yosemite]] between 1977-1980.<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> During Crane’s [[Guggenheim Fellowship]] (1979), she collaborated with the [[Center for Creative Photography in Tucson, Arizona]] to create a career retrospective of her work. During her time in Boston, she formed a relationship with the Polaroid Corporation and through the [[Polaroid Artist Support Program]] she experimented with Polaroid black & white and color photographic materials in numerous series.

In 1995, Crane became [[Professor Emeritus]] at the School of the Art Institute of Chicago.<ref>Crane, Barbara. http://barbaracrane.desordre.net/</ref>

==Projects==
* Official Photographer for [[Commission on Chicago Historical and Architectural Landmarks]] (1972-1979). During this seven-year project, Crane photographed the design and contextual relationships of buildings in the Chicago area.<ref>Rosenblum, Naomi. ''A History of Women Photographers.'' New York: Abbeville Press Publishers, 2000. 239.</ref>
* ''Neon Series'' (1969) is a series “juxtaposing neon lights over faces like ceremonial urban masks”.<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> The photographs capture people's faces exiting the Carson Pirie Scott Department store. John Rohrbach notes that the overlay of neon on the shoppers’ faces suggests “commerce’s domination” over them.<ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>
* ''People of the North Portal'' (1970-1971) captures the reactions of people exiting Chicago’s Museum of Science and Industry – this series focuses on the human experience.<ref>Museum of Contemporary Photography, Columbia: http://www.mocp.org/detail.php?type=related&kv=7005&t=people</ref>
* ''Chicago Beaches and Parks'' (1972) series takes after the [[stylizations]] of street photography. Crane photographed unposed individuals during their visits to parks and beaches around Chicago. [[John Rohrbach]] notes, “they deliver a grand sociological study of public activity: dancing, wooing, tending to children, showing off, catching sun and even taking snapshots”.<ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>
* ''Wrightsville Beach, North Carolina'' (1974) combines multiple film formats (4x5 and half-frame 35&nbsp;mm film). This project depicts the residents and all the houses on the [[Wrightsville Beach peninsula]] at sunrise and sunset.
* ''Baxter Labs'' (1974-1975) is a corporate commission for the [[Baxter Travenol Laboratories]], a pharmaceutical company located in [[Deerfield, Illinois]]. The company approached Crane to create twenty-six murals measuring approximately 8 x 10 feet each that montage lab equipment with scenes of Crane’s choosing. The murals no longer exist, but Crane retains transparency copies.<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>
* ''Repeats'' (1969-1978) and “Petites Choses” (1974-1975) combine series of repeated images into patterns by printing strips of 35mm film frames directly onto photographic paper. Crane refers to music as an influence in these series stating “while doing the ‘Repeats’ and ‘Petites Choses’” series, I was taking notes at the symphony as visual diagrams of the crescendos, legatos, and staccatos in order to widen my visual experience.”<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref> These series form a tapestry of images influenced by her musical interests.
* ''Chicago Loop'' (1976-1978) contains images of building walls in [[Chicago’s downtown Loop]]. The photographs were taken with a 5 x 7 inch view camera and visualized on the ground glass of the camera so as to abstract the interplay and adjacencies of the layers of buildings and emphasize the repetition, layering and confusion of shapes and forms.<ref>http://higherpictures.com/press/barbara-crane-3/</ref>
* ''Commuter Discourse'' (1978) focuses on light and shadow. Crane took photographs of rush-hour crowds using a 35mm Leica with a 21mm wide-angle lens. The resulting images depict exaggerated body parts and shadows that extend to the edges of the frame. The focus of the series lay in the bodies rather than the identities of the individuals.<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref><ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>
* ''Monster Series, Chicago Dry Docks'' (1983) utilizes a [[close-up perspective]] and on-camera flash to distort boat details.
* ''Visions of Enarc'' (1983-1986) experiments with wide angle, close up, flash and overexposure influencing the later “Wipe Outs”. Crane’s statement notes that Visions of Enarc “reveals a natural word that I have twisted, thereby producing an alternative world simultaneously surreal, ominous, and romantic.”<ref>Crane, Barbara. http://barbaracrane.desordre.net/texts/general_statement.pdf</ref>
* ''Wipe Outs'' (1986) also experiments with flash and overexposure. The overexposure erases individual identity but captures common human behaviors.<ref>Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>
* ''Objets Trouvé'' (1982-1988) stems from Crane’s interest in discarded objects, their life cycles and transformations. She notes that the series “embodies a suggestion of human evolution and human fatalities”.<ref>Crane, Barbara. http://barbaracrane.desordre.net/texts/general_statement.pdf</ref>
* ''Coloma to Covert'' (1987-2013) is a series comprising 26 years of work. These photographs were taken in a wooded area along Lake Michigan in southwestern Michigan between the two small towns of Coloma and Covert where Crane purchased a cabin in 1987. The series leaves the viewer with an uneasy feeling and focuses on form, abstraction, light and other elements of the nature of the area. 
* ''Sand Findings'' (1992-1993) is a series that uses very limited focus to place parts of the image in focus and others out of focus. The resulting images make space difficult to determine.<ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>
* ''Schisms'' (2001) is a series that combines color prints from the forest to create interesting and unique pairings.  
* ''Still Lifes'' (1997-2002) isolates dead animals and found natural objects against a black background.
* ''Inner Circles'' (2003-2004) These photographs consist of multiple circular frames reminiscent of peepholes with repeated images of nature or the city taken in both Chicago and the woodlands.<ref>Rohrbach, John. “Seeing Life Differently” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Department of Cultural Affairs, 2009.</ref>

==Selected Retrospective and Solo Exhibitions==
* 2014 ''Barbara Crane: Chicago Loop 1976 – 1978'', Higher Pictures, New York, NY.
* 2013 ''Barbara Crane and John Miller: a Colorful Couple'', Chicago Photography Gallery, two-person, Chicago, IL.
* 2012 ''Barbara Crane: Challenging Vision'', Brierly Gallery, New Trier High School, Winnetka, IL.
* 2011 ''Barbara Crane: Challenging Vision'', Griffin Museum of Photography, Winchester, MA.
* 2011 ''Barbara Crane: An Infernal Beauty'', ThinkArt, Chicago, IL.
* 2010 ''Barbara Crane: Private Views'', Stephen Wirtz Gallery, San Francisco, CA.
* 2010 ''Barbara Crane: Challenging Vision'', Galerie François Paviot, Paris, France.
* 2009-2010 ''Barbara Crane: Challenging Vision'', Chicago Cultural Center, Chicago, IL.
* 2009-2010 ''Barbara Crane: Repeats'', Higher Pictures, New York, NY.
* 2009-2010 ''Private Views'', Aperture Foundation, New York, NY.
* 2009-2010 ''Barbara Crane: Then/Now'', Illinois Institute of Technology, Chicago, IL.
* 2009 ''Barbara Crane: Challenging Vision'', Amon Carter Museum, Fort Worth, TX.
* 2009 ''Barbara Crane: Private Views – Public Spaces'', Stephen Daiter Gallery, Chicago, IL.
* 2008-2009 ''Barbara Crane: Selected Early Works'', Higher Pictures, New York, NY.
* 2005 ''Barbara Crane: Snap to Grid'', FLATFILE Gallery, Chicago, IL.
* 2004 ''Barbara Crane: Still Lifes – Natures Mortes'', Hellenic American Union, Athens, Greece.
* 2004 ''Barbara Crane: Still Lifes – Natures Mortes'', Chicago Cultural Center, Chicago, IL.
* 2003 ''Barbara Crane: Mid-Career at Seventy-Five'', Stephen Daiter Gallery, Chicago, IL.
* 2003 ''Barbara Crane: Natures Mortes, Photosynkyria'', Macedonia Museum of Modern Art, Thessaloniki, Greece.
* 2002 ''Barbara Crane: Urban Anomalies'', Museum of Contemporary Photography, Chicago, IL.
* 2001-02 ''Barbara Crane: Chicago Loop'', LaSalle Bank, and Museum of Contemporary Photography, Chicago, IL.
* 1997 ''Barbara Crane'', Lallak+Tom, Chicago, IL.
* 1997 ''Barbara Crane: Woodland Vestiges: From Coloma to Covert'', Mills College Art Gallery, Oakland, CA.
* 1994 ''Sticks and Stones'', Gallery 954, Chicago, IL.
* 1994 ''Barbara Crane: Photographie'', Galerie Suzel Berna, Paris, France.
* 1993 ''Harmonic Distortions'', Prague House of Photography, Prague, Czech Republic.
* 1993 ''Transformations and Aberrations'', Gallery 954, Chicago, IL.
* 1992-93 ''Barbara Crane: Israeli Suite'', Spertus Museum, Chicago, IL.
* 1992 ''Coloma to Covert, Etc'', Middle Tennessee State University Photo Gallery, Murfreesboro, TN.
* 1990 ''Barbara Crane Photographs: Misrepresentations'', May Gallery, Sverdrup Technology Center, Webster University, St Louis, MO.
* 1990 ''Barbara Crane,'' Photography Center Gallery, The 92nd St Y, New York, NY.
* 1989 ''Barbara Crane: Hanmadang Gallery'', Seoul, Korea.
* 1989 ''Barbara Crane: New Work'', Jones Troyer Fitzpatrick Gallery, Washington, DC.
* 1987 ''Barbara Crane: Distortion by Exaggeration'', Museum of Contemporary Photography, Columbia College, Chicago, IL.
* 1987 Bezalel Academy of Art & Design, Mt. Scopus, Jerusalem, Israel.
* 1986 Tweed Museum of Art, Univ. of Minnesota, Duluth, MN
* 1985 ''Barbara Crane: Photographs'', Jones Troyer Gallery, Washington, DC.
* 1983 ''Barbara Crane'', Nexus Gallery, Atlanta, GA.
* 1983 ''Barbara Crane'', Herbert E. Johnson Museum of Art, Cornell Univ., Ithaca, NY.
* 1983 ''Barbara Crane'', Douglas Elliot Gallery, San Francisco, CA
* 1983 ''Objet Trouve'', Int'l Museum of Photography, George Eastman House, Rochester, NY.
* 1981 ''Recent Work'', Young Hoffman Gallery, Chicago, IL.
* 1980 ''Barbara Crane'', Vision Gallery, Boston, MA.
* 1978 ''Barbara Crane'', Center for Photographic Studies, Louisville, KY.
* 1975 ''Barbara Crane'', Friends of Photography, Carmel, CA.
* 1974 ''Barbara Crane'', F22 Gallery, Sante Fe, NM.
* 1973 ''Barbara Crane'', University of Iowa Museum, Iowa City, IA.
* 1972 ''People of the North Portal'', Museum of Science and Industry, Chicago, IL.
* 1969 ''Barbara Crane'', Friends of Photography, Carmel, CA.
* 1968 ''Barbara Crane'', Hunter Gallery, Aspen, CO.

==Publications==
* ''Barbara Crane: Challenging Vision'', retrospective monograph, Chicago Department of Cultural Affairs, July 2009, essays by John Rohrbach and Abigail Foestner, 285 images, 252 pgs.
* ''Barbara Crane: Private Views'', monograph, Aperture Foundation Foundation, Feb 2009, essay by Barbara Hitchcock, 103 images, 112 pgs.
* ''Barbara Crane: Grids'', exhibition catalog, May 2005, essay by Claire Cass, 11 images, 12 pgs.
* ''Barbara Crane Still Lifes: Natures Mortes'', exhibition catalog, Chicago Cultural Center, 2004, essayby Sofia Zutautas, 13 images, 16 pgs.
* “Barbara Crane: Human Forms”, Camera Obscura LV (#55), Winter 2003, 16 pg portfolio insert.
* ''Barbara Crane & John F. Miller: Together'', exhibition catalog, FLATFILE Gallery, Chicago, IL, Sept 2002, essays by Claire Wolf Krantz and Michael A. Weinstein, 6 images, 16 pgs.
* ''Barbara Crane: Urban Anomalies'', exhibition catalog, Museum of Contemporary Photography, Chicago, IL, 2002, essay by Michael A. Weinstein, 20 images, 48 pgs.
* ''Barbara Crane: Chicago Loop'', monograph, LaSalle Bank, Chicago, IL, Feb 2002, essay by Sarah Anne McNear, 40 images, 60 pgs.
* ''Barbara Crane: 1948-1980'', retrospective monograph, Center for Creative Photography, University of Arizona, Tucson, AZ, 1981, essays by Estelle Jussim and Paul Vanderbilt, 129 images, 138 pgs.
* ''Barbara Crane: The Evolution of a Vision'', exhibition catalog, Albin O. Kuhn Library & Gallery, University of Maryland Baltimore County, Polaroid Corporation, April 1983, essay by Tom Beck, 25 images, 22 pgs.

==Collections==
Crane’s work is held in numerous collections both nationally and internationally. The following is a short selection of those collections:
* [[Amon Carter Museum, Fort Worth, TX]]
* Art Institute of Chicago, Chicago, IL
* [[Bibliotheque Nationale, Paris, France]]
* [[California Museum of Photography, University of California at Riverside, Riverside, CA]]
* [[Centre de photographie de Lectoure, Lectoure, France]]
* Center for Creative Photography, University of Arizona, Tucson, AZ
* [[Fonds Nationale D’Art Contemporaine, France]]
* [[Hammer Museum, Grunwald Center, University of California, Los Angeles, CA]]
* [[The JP Morgan Chase Art Collection]]
* J. Paul Getty Museum, Los Angeles, CA
* [[Harry Ransom Center, University of Texas, Austin, TX]]
* [[High Museum of Art, Atlanta, GA]]
* [[International Center for Photography, New York, NY]]
* International Museum of Photography, George Eastman House, Rochester, NY
* [[John D. and Catherine T. MacArthur Foundation, Chicago, IL]]
* [[John Simon Guggenheim Memorial Foundation, New York, NY]]
* Kiyosato Museum of Photographic Art, Tokyo, Japan
* National Museum of Modern Art, Kyoto, Japan
* [[Library of Congress, Washington, DC]]
* Los Angeles County Museum of Art, Los Angeles, CA
* [[Minneapolis Institute of Arts, Minneapolis, MN]]
*[[Musee de L’Elysee, Lausanne, France]]
* [[Museet Moderna, Stockholm, Sweden]]
* Museum of Contemporary Art, Chicago, IL
* Museum of Contemporary Art, Los Angeles, CA
* Museum of Contemporary Photography at Columbia College, Chicago, IL
* Museum of Fine Arts, Houston, TX
* Museum of Modern Art, New York, NY
* [[National Museum of American Art, Smithsonian Institution, Washington, DC]]
* [[The Nelson-Atkins Museum of Art, Kansas City, MO]]
* [[New York Public Library, New York, NY]]
* [[Norton Simon Museum, Pasadena, CA]]
* San Francisco Museum of Modern Art, San Francisco, CA
* [[Santa Barbara Museum of Art, Santa Barbara, CA]]
* [[Seattle Art Museum, Seattle, WA]]
* [[Sir Elton John Photography Collection]]
* [[Victoria and Albert Museum, London, England]]
* WestLicht Museum of Photography, Vienna, Austria

==References==
{{reflist}}

* Rosenblum, Naomi. ''A History of Women Photographers''. New York: Abbeville Press Publishers, 2000.
* Hirsch, Robert. ''Seizing the Light: A History of Photography''. Boston: McGraw Hill, 2000.
* Heller, Jules and Nancy G. Heller, eds. ''North American Women Artists of the Twentieth Century''. New York: Routledge, 2013.
* Crane, Barbara. http://barbaracrane.desordre.net/
* “Barbara Crane” Stephen Daiter Gallery. https://web.archive.org/web/20121208120631/http://www.stephendaitergallery.com/dynamic/artist.asp?ArtistID=10
* Museum of Contemporary Photography, Columbia College Chicago: http://www.mocp.org/detail.php?type=related&kv=7005&t=people
* Rohrbach, John. “Seeing Life Differently” in ''Barbara Crane: Challenging Vision''. Chicago: City of Chicago, Department of Cultural Affairs, 2009. 
* Foerstner, Abigail. “The Path to the Perfect Photograph” in Barbara Crane: Challenging Vision. Chicago: City of Chicago, Cultural Affairs, 2009.
* Higher Pictures. “Barbara Crane - Chicago Loop, 1976-1978.” Higherpictures.com. http://higherpictures.com/press/barbara-crane-3/ 
*Higher Pictures. ''Repeats 1969 – 1978.” Higherpictures.com. http://higherpictures.com/press/barbara-crane-2/
* “Barbara Crane”  ''Artsy''. https://artsy.net/artist/barbara-crane
* Bergeron, Chris. “Barbara Crane: Pioneer Photographer” “MetroWest Daily News“. 20 February 2011. http://www.metrowestdailynews.com/x410098641/Barbara-Crane-Pioneer-photographer
* “Barbara Crane.” “Museum of Contemporary Photography“. http://www.mocp.org/detail.php?type=related&kv=7005&t=people
* Viera, Lauren. “Photographer Barbara Crane Flies Higher than Ever.” “Chicago Tribune“. 6 September 2009. http://articles.chicagotribune.com/2009-09-06/news/0909050290_1_cultural-center-flies-retrospective
* “Photography as a Journey: The Bold Artistry of Barbara Crane.” “IIT Magazine“. 29 April 2014. http://iit.edu/magazine/online_exclusive/photography_as_journey.shtml
* “Private Views by Barbara Crane.” “Digital Photography Review“. 5 January 2012. http://www.dpreview.com/articles/8904604253/private-views-by-barbara-crane

{{Authority control}}

{{DEFAULTSORT:Crane, Barbara}}
[[Category:1928 births]]
[[Category:Living people]]
[[Category:American photographers]]
[[Category:American women photographers]]