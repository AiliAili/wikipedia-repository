{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name = Sydney Carlin
|image = 
|image_size = 
|alt = 
|caption = 
|birth_date = 1889
|death_date = 9 May {{Death year and age|1941|1889}}
|birth_place = [[Kingston upon Hull]], Yorkshire, England
|death_place = [[Peterborough]], Cambridgeshire, England
|placeofburial = 
|placeofburial_label = 
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname = Timbertoes
|birth_name = 
|allegiance = United Kingdom 
|branch = British Army<br/>Royal Air Force
|serviceyears = 1908–1909<br/>1915–1924<br/>1940–1941
|rank = [[Pilot Officer]]
|servicenumber = 
|unit =  {{Plainlist|
* [[18th Royal Hussars]] 
* [[Royal Engineers]]
* [[No. 74 Squadron RAF|No. 74 Squadron RFC]]
* [[No. 264 Squadron RAF]] 
* [[No. 151 Squadron RAF]]
}}
|commands =
|battles = [[World War I]]<br/>{{*}}[[Battle of the Somme]]<br/>[[World War II]]<br/>{{*}}[[Battle of Britain]]
|awards = [[Military Cross]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]<br/>[[Distinguished Conduct Medal]] 
|relations = 
|laterwork = 
}}
Pilot Officer '''Sydney "Timbertoes" Carlin''', {{post-nominals|country=GBR|MC|DFC|DCM|sep=yes}} (1889–1941) was a British [[World War I]] [[flying ace]], despite having previously lost a leg during the Battle of the Somme. He returned to the [[Royal Air Force]] in World War II, serving as an [[air gunner]] during the [[Battle of Britain]].

==Early life==
Sydney Carlin was born in Hull, the son of William Carlin, a [[drysalter]]. By 1901 he was a boarder at a small private school in the village of Soulby, [[Kirkby Stephen]], [[Westmorland|Westmoreland]]. He enlisted with the [[18th Royal Hussars]] in 1908, but he bought himself out and resigned in December 1909 for the sum of £18. In 1911 he was working as a farm labourer at Frodingham Grange, [[North Frodingham]], Yorkshire.

==World War I==
He re-enlisted on 8 August 1915; the army refunded half (£9) of the money he had bought himself out with in 1909. Serving in Belgium with the 18th Royal Hussars he was awarded the Distinguished Conduct Medal on 5 August 1915, and was later commissioned as a [[second lieutenant]] in September 1915<ref>{{London Gazette |date=24 September 1915 |issue=29308 |supp=yes |startpage=9525 |nolink=yes }}</ref> and made a [[lieutenant]] in May 1916.<ref>{{London Gazette |date=16 June 1916 |issue=29627 |supp=yes |startpage=6057 |nolink=yes }}</ref> He lost a leg serving in the [[Battle of Delville Wood|Battle of Longueval/Delville Wood]], on the Somme in 1916, while commanding a Royal Engineers Field Company section holding a trench against repeated German counter-attacks. For this action he was awarded the [[Military Cross]] in October.

Extraordinarily, he joined the [[Royal Flying Corps]] in 1917, following his recovery. On 12 March 1918, Carlin was seconded from the Royal Engineers to the RFC.<ref>{{London Gazette |date=8 May 1918 |issue=30674 |supp=yes |startpage=5552 |nolink=yes }}</ref><ref>{{London Gazette |date=15 April 1918 |issue=30630 |supp=yes |startpage=4508 |nolink=yes }}</ref> After serving as an instructor at the [[Central Flying School]], he was posted in May 1918 to [[No. 74 Squadron RAF|No. 74 Squadron RFC]] flying [[Royal Aircraft Factory S.E.5|S.E.5As]], where he earned his nickname "Timbertoes". Carlin is recorded as an [[ace]] [[balloon buster]], with five balloons downed; he was also an ace against aircraft, with four machines claimed destroyed, and one aircraft 'driven down out of control'. His exploits earned him the Distinguished Flying Cross.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/carlin.php |title=Sydney Carlin |work=The Aerodrome |year=2014 |accessdate=28 September 2014 }}</ref>

On 9 August 1918 Lieutenant Carlin was promoted to temporary captain.<ref>{{London Gazette |date=27 August 1918 |issue=30868 |startpage=10007 |nolink=yes }} Note: This promotion was customarily linked to appointment to a [[flight commander]].</ref> In early September he was involved in a mid-air collision with his commanding officer, Major [[Keith Caldwell]], but was relatively unscathed.

On 21 September Carlin was shot down over [[Hantay]] by ''Unteroffizier'' Siegfried Westphal of [[Jasta 29]] and held as a [[prisoner of war]]. He was repatriated on 13 December 1918 and admitted to the [[RAF Central Hospital]] on Christmas Day 1918. Carlin relinquished his commission on "account of ill-health contracted on active service" on 7 August 1919.<ref>{{London Gazette |date=8 August 1919 |issue=31495 |startpage=10092 |nolink=yes }}</ref> and retained the rank of lieutenant.<ref>{{London Gazette |date=17 October 1919 |issue=31606 |supp=yes |startpage=12857 |nolink=yes }}</ref>

==Inter-war years==
On 1 January 1924 Carlin was promoted from flight lieutenant to [[squadron leader]].<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200013.html |title=Royal Air Force Appointments |issue=784 |volume=XVI |page=13 |work=[[Flight International|Flight]] |date=3 January 1924 |accessdate=28 September 2014}}</ref> Nevertheless, in 1924, Carlin departed Britain for [[Mombasa]] aboard the SS ''Madura''.  He was listed on the passenger list as an "agriculturist". He farmed for some years in Kenya.

From 20 May 1931 to 8 August 1935 Carlin served as the [[Justice of the Peace]] for [[Kisumu-Londiani District]], Kenya.<ref>(''Kenya Gazette,'' 13 August 1935) p. 902.</ref>

==World War II==
On re-enlistment to the RAF he was graded as a probationary [[pilot officer]] on 27 July 1940. He made pilot officer in September 1940,<ref>{{London Gazette |date= 20 September 1940 |issue=34949 |startpage=5587 |nolink=yes }}</ref> flying as an air gunner in [[Boulton Paul Defiant|Defiant]] aircraft with [[No. 264 Squadron RAF]] and later [[No. 151 Squadron RAF]]. He also made several unofficial trips as an air gunner with No. 311 (Czech) Squadron, flying Wellingtons. 

Carlin was injured in action at [[RAF Wittering]] during an enemy bombing raid on 7/8 May 1941, and died in Peterborough on 9 May 1941. He  is memorialized on the Screen Wall, Panel 1, Hull Crematorium.<ref>{{cite web |url= http://www.cwgc.org/find-war-dead/casualty/2402203/CARLIN,%20SYDNEY |title=Carlin, Sydney |work=[[Commonwealth War Graves Commission]] |year=2014 |accessdate=28 September 2014}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Find a Grave|64637642|Pilot Officer Sydney 'Timbertoes' Carlin (1889–1941)}}

{{DEFAULTSORT:Carlin, Sydney}}
[[Category:1889 births]]
[[Category:1941 deaths]]
[[Category:Military personnel from Kingston upon Hull]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force officers]]
[[Category:British Army personnel of World War I]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:Recipients of the Military Cross]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Recipients of the Distinguished Conduct Medal]]

[[Category:The Few]]