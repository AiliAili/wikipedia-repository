{{Use dmy dates|date=May 2011}}
{{good article}}
{{Infobox military unit
|unit_name= 45th Infantry Brigade Combat Team
|image= [[File:45thIBCTSSI.png|200px|alt=The “Thunderbird” is an Indian symbol meaning sacred bearer of happiness unlimited.]]
|caption=Shoulder sleeve insignia
|dates= 1968–present
|country= {{flag|United States}}
|allegiance= 
|branch={{army|United States}}
|type= [[Infantry]] [[brigade combat team]]
|role= [[Light Infantry]]
|size= [[Brigade]]
|command_structure= [[Oklahoma Army National Guard]]
|garrison=[[Norman, Oklahoma]]
|nickname= ''Thunderbird'' ([[special designation]])<ref name = SUD>{{cite web| title = Special Designation Listing| url = http://www.history.army.mil/html/forcestruc/spdes-123-arng.html| publisher=[[United States Army Center of Military History]]| date = 21 April 2010| accessdate =14 July 2010| archiveurl= https://web.archive.org/web/20100609010028/http://www.history.army.mil/html/forcestruc/spdes-123-arng.html| archivedate= 9 June 2010 <!--DASHBot-->| deadurl= no}}</ref>
|motto= ''Semper Anticus''<br>[[Latin]]: "Always Forward"<ref name="Wilson663"/>
|battles=Sheygal<br/>Alingar<br/>Sangar Valley<br/>Route Iowa
|notable_commanders=
|identification_symbol=[[Image:45thIBCTDUI.png|250px|The DUI of the 45th IBCT is one of only a few that are authorized a mirror image.]]
|identification_symbol_label=Distinctive unit insignia
|identification_symbol_2=
|identification_symbol_2_label=
}}
The '''45th Infantry Brigade Combat Team''' ("'''Thunderbird'''"<ref name = SUD/>) is a modular [[infantry]] [[brigade combat team]] of the [[United States Army]] headquartered in [[Norman, Oklahoma|Norman]], [[Oklahoma]]. It is a part of the [[Oklahoma Army National Guard]].

The 45th Infantry Brigade was formed from existing elements of the disbanded [[45th Infantry Division (United States)|45th Infantry Division]] which had seen action during [[World War II]] and the [[Korean War]]. Concurrently, the [[45th Fires Brigade (United States)|45th Field Artillery Group]] (today's 45th Fires Brigade) and [[90th Troop Command]],<ref>{{cite news|last=Talley|first=Tim|title=Legislature Honors 45th Infantry Brigade|url=http://www.durantdemocrat.com/view/full_story/1162202/article-Legislature-honors-45th-Infantry-Brigade|accessdate=9 July 2012|newspaper=Durant Democrat}}</ref><ref>{{cite news|title=Home At Last--National Guardsmen Return Home|url=http://www.tulsabeacon.com/?p=5899|accessdate=9 July 2012|newspaper=Tulsa Beacon|date=12 April 2012}}</ref> were also formed in the same way. The 45th Infantry Brigade was activated in 1968 and assigned to training duties for active duty army units until 1994 when the 45th was selected as one of 15 "enhanced brigades". The brigade deployed as part of the UN peacekeeping force in the wake of the [[Bosnian War]], with C Company, 1-179th Infantry being among the first National Guard units to see duty there. In 2003, A Co 1-179 deployed to Saudi Arabia while B 1-179 deployed to Kuwait to provide security for Patriot missile sites. During the invasion of Iraq, B Co 1-179 pushed North of Baghdad establishing a foothold in Taji Iraq. Later that year, the 45th deployed to [[Afghanistan]] to train soldiers of the Afghan National Army which was followed by a deployment to [[Iraq]] to assist in the turning over of American military bases to Iraqi forces. A second brigade deployment to Afghanistan in 2011 assigned the brigade to full-spectrum operations for the first time since the 1950s.

The brigade received all heraldry, lineage and honors from the 45th Infantry Division, including its [[Shoulder Sleeve Insignia|shoulder sleeve insignia]] and [[campaign streamers]] for combat in World War II and Korea. It has since received several of its own decorations for participation in the subsequent conflicts.

==Organization==
The [[brigade]] is a subordinate unit of the [[Oklahoma Army National Guard]], headquartered in Norman, OK.<ref name="bdehist"/> The brigade commands six [[battalion]]s.<ref name="GSO"/> These units are the 1st Squadron, [[180th Cavalry Squadron (United States)|180th Cavalry Regiment]], [[700th Support Battalion (United States)|700th Brigade Support Battalion]], 1st Battalion, [[179th Infantry Regiment (United States)|179th Infantry Regiment]], 1st Battalion, [[160th Field Artillery Regiment]], 1st Battalion, [[279th Infantry Regiment (United States)|279th Infantry Regiment]], and [[45th Brigade Special Troops Battalion]].<ref name="bdehist"/>

==45th Infantry Division==
{{See also|45th Infantry Division (United States)}}
The history of the 45th Brigade Combat Team can be traced back to 1890 with the formation of the Militia of the Territory of Oklahoma.<ref name="bdehist">{{cite web|url=http://www.carson.army.mil/UNITS/F7ID/45th/45theSB_History.htm |title=45th Infantry Brigade Homepage: History |publisher=45th Infantry Brigade |accessdate=3 May 2009}}</ref> That militia was mobilized in 1898 during the [[Spanish–American War]] but never deployed. In 1916 the First Oklahoma Infantry Regiment deployed for border security duty during the Mexican Border Conflict. In 1917, the First Oklahoma Infantry Regiment, reassigned as part of the [[142nd Infantry Regiment]] of the [[36th Infantry Division (United States)|36th Division]] fought in the final month of [[World War I]].<ref name="bdehist" />

On 19 October 1920, the Oklahoma State militia was organized as the 45th Infantry Division of the [[Oklahoma Army National Guard]] and organized with troops from [[Arizona]], [[Colorado]], [[New Mexico]], and Oklahoma.<ref name="McGrath234">McGrath, p. 234.</ref> The division was organized and federally recognized as a US Army unit on 3 August 1923 in [[Oklahoma City]], Oklahoma.<ref name="Wilson663">Wilson, p. 663.</ref> Prior to [[World War II]], the division was called on many times to maintain order in times of disaster and to keep peace during periods of political unrest. Oklahoma Governor [[John C. Walton]] used division troops to prevent the State Legislature from meeting when they were preparing to impeach him in 1923. Governor [[William H. Murray]] called out the guard several times during the depression to close banks, distribute food and once to force the State of Texas to keep open a free bridge over the [[Red River of the South|Red River]] which Texas intended to collect tolls for, even after federal courts ordered the bridge not be opened.

The division would go on to see combat in [[World War II]].<ref name="bdehist" /> The division was active for over five years, participating in eight campaigns, four [[amphibious assault]]s, for a total of 511 days of combat.<ref name="bdehist" /> Following [[World War II]] the division became an all-Oklahoma organization. In 1950, the division was also called into service during the [[Korean War]], as one of four national guard divisions active during the war and participating in four campaigns and fighting for 429 days.<ref name="bdehist" />

==Cold War years==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:whitesmoke; color:black; width:21em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |
'''Units of the 45th Infantry Brigade (Separate), 1968-1994'''  
*HHC 45th Infantry Brigade
*1st Bn 179th Infantry
*1st Bn 180th Infantry
*1st Bn 279th Infantry
*1st Bn 160th Field Artillery
*700th Forward Support Bn
*Troop E 145th Cavalry
*245th Military Intelligence Co
*245th Engineer Co
|}
In 1968, the division was disbanded and the 45th Infantry Brigade (Separate) was formed in its place.<ref name="Wilson664">Wilson, p. 664.</ref> The 45th Brigade assumed all of the 45th Division's lineage and campaign participation credit, including its [[shoulder sleeve insignia]] featuring a [[Thunderbird (mythology)|Thunderbird]], a common [[Indigenous peoples of the Americas|Native American]] symbol, as a tribute to the south-western United States region which had a large population of Native Americans.<ref name="TIOH">{{cite web|url=http://www.tioh.hqda.pentagon.mil/Inf/45th%20Infantry%20Brigade.htm |archive-url=https://web.archive.org/web/20060911012357/http://www.tioh.hqda.pentagon.mil:80/Inf/45th%20Infantry%20Brigade.htm |dead-url=yes |archive-date=11 September 2006 |title=The Institute of Heraldry: 45th Infantry Brigade |publisher=[[The Institute of Heraldry]] |accessdate=3 May 2009 }}</ref> The brigade also assumed the division's nickname, "Thunderbirds".<ref name="SUD"/> The division's three subordinate brigades were disbanded as a part of the organization, and were not affiliated with the 45th Infantry Brigade (Separate).<ref>McGrath, p. 202.</ref> The brigade's headquarters was subsequently relocated to [[Edmond, Oklahoma]].<ref name="Wilson664"/> In 1971 the brigade received its [[distinctive unit insignia]].<ref name="TIOH"/>

The brigade did not participate in any overseas combat operations through the 1970s or 1980s, as the size of the [[active duty]] force negated the need for National Guard formations to be deployed during the relatively small contingencies of that period.  The 45th did participate in [[Exercise Reforger|REFORGER]] (Certain Strike) in September 1987.<ref>{{cite web|last1=Mihalko|first1=Ron|title=Reforger|url=http://mihalko-family.com/REFORGER.htm|website=Mihalko Family|accessdate=16 July 2014}}</ref> Instead, the brigade was primarily used to train active duty units, and other general peacetime missions within the United States.<ref name="bdehist"/> In 1991, the brigade became affiliated with the [[1st Cavalry Division (United States)|1st Cavalry Division]], providing training services for the division soldiers.<ref name="GSO">{{cite web|url=http://www.globalsecurity.org/military/agency/army/45in-bde.htm |title=GlobalSecurity.org: 45th Infantry Brigade|publisher=GlobalSecurity|accessdate=3 May 2009}}</ref>

==Desert Storm==
While the Infantry Brigade would only supply individuals for service in Desert Storm, other Oklahoma elements would be called upon.  On 19 September 1990, the 2120th Supply and Service Company located in Wewoka, Oklahoma was called up for active duty in support of Desert Shield. This was the first of fourteen units from the Oklahoma National Guard called up in support of the action. Many of the units were sent to Saudi Arabia to provide support for the regular army in Desert Shield and Desert Storm.

==Enhanced brigade==
{|class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:whitesmoke; color:black; width:21em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |
'''Units of the 45th Infantry Brigade (Separate)(Enhanced), 1994–2006'''
*HHC 45th Infantry Brigade
*1st Bn 179th Infantry
*1st Bn 180th Infantry
*1st Bn 279th Infantry
*1st Bn 160th Field Artillery
*700th Forward Support Bn
*Troop E 145th Cavalry
*245th Military Intelligence Co
*245th Engineer Co
*E Btry 202nd Air Defense Artillery (ILARNG) (1994–2000)
|}
In 1994, the brigade was selected as one of fifteen "enhanced" separate brigades of the [[Army National Guard]], featuring authorization to recruit 10% above required manning levels and a requirement to attend one of the Combat Training Centers not less than once every eight years, and ready to deploy within 90 days in case of emergencies.<ref name="bdehist" /><ref>{{cite web|last=Merridith|first=Bill|title=Enhanced Brigade Readiness Improves but Personnel and Workload Problems|url=http://www.gao.gov/new.items/ns00114.pdf|publisher=US General Accounting Office}}</ref> In 1997, the brigade was integrated under the command structure of the [[7th Infantry Division (United States)|7th Infantry Division]], allowing the 7th Division to provide oversight and support for the brigade's activities should it be deployed, and potentially command and control when deployed, but that was never tested.<ref name="GSO" /> In 1996, the brigade's garrison was relocated back to Oklahoma City.<ref name="McGrath234" />

In 2000–2001 several hundred soldiers of the brigade were deployed to [[Bosnia]] in support of NATO forces seeking to stabilize the country in the wake of the [[Bosnian War]]. Soldiers of the brigade were among the first National Guard units to see front-line patrolling duty in the conflict, a job held exclusively by active duty units until that time.<ref>{{cite web|url=http://www.globalsecurity.org/military/agency/army/30in-bde.htm |title=GlobalSecurity.org: 30th Infantry Brigade |publisher=GlobalSecurity |accessdate=3 May 2009}}</ref>

The brigade trained for a rotation in the [[Fort Polk#JRTC moves to Polk|Joint Readiness Training Center]] at [[Fort Polk, Louisiana]] throughout 2000 and 2001, before deploying to the center throughout 2002 and early 2003.<ref name="GSO" /> The brigade received praise from center commanders as performing the mission better than many brigades before it.<ref name="GSO" /> After its rotation, the brigade trained the [[39th Infantry Brigade (United States)|39th Infantry Brigade]] of the [[Arkansas Army National Guard]], which saw the next rotation in the JRTC. The 39th Brigade was also under the command of the 7th Infantry Division.<ref name="GSO" />

==Iraq and Afghanistan==
In January 2003, components of the 45th Infantry Brigade were deployed to Saudi Arabia and Kuwait. Approximately 230 light infantry soldiers from A Company and B Company, 1st Battalion, 179th Infantry Regiment comprised Task Force Ironhorse under the United States Army Central Command ([[ARCENT]]).<ref>{{cite web|url=http://www.globalsecurity.org/military/library/report/2004/onpoint/cflcc.htm/ |title=Combined Forces Land Component command CFLCC |publisher=GlobalSecurity.org |accessdate=14 November 2009}}</ref><ref name="The Daily Oklahoman">{{cite web|url=http://www.encyclopedia.com/doc/1G1-160753591.html/ |title=State's military leaders prepare for larger deployment |publisher=The Daily Oklahoman |accessdate=14 November 2009}}</ref>  Their primary mission leading up to the invasion of Iraq was to provide security for Patriot missile sites defending the respective countries from impending SCUD missile attacks.<ref name="The Daily Oklahoman"/>  In March 2003, Company A was ordered from the area in and around Riyadh to the northern border cities of Tabuk and Arar, Saudi Arabia in defense of Iraqi retaliation and security of strategically redeployed Patriot Missile sites. Company B was ordered to advance into Iraq from the Kuwaiti border to provide security for ammo caches and forward operating Patriot missile sites. Task Force Ironhorse was the first deployment of Oklahoma National Guard infantry soldiers to a combat zone since the Korean War (members of the Oklahoma National Guard deployed during Desert Storm as Field Artillery units). Task Force Ironhorse completed their mission and returned in August 2003.

In fall of 2003, the 45th Infantry Brigade was deployed to [[Afghanistan]] in support of [[Operation Enduring Freedom]], assuming command of [[Task Force Phoenix|Task Force Phoenix II]] from [[2nd Brigade, 10th Mountain Division (United States)|2nd Brigade, 10th Mountain Division]].<ref name="GSO"/> The purpose of the soldiers' deployment was to assist in training Afghan security forces. Over the next few years, soldiers of the 45th Infantry Brigade, including its [[Headquarters and Headquarters Company]], would deploy in support of this mission. In April 2004, 350 soldiers from the brigade's 1st Battalion, 279th Infantry Regiment also deployed to Joint Task Force Phoenix.<ref name="GSO"/> During this rotation, the brigade grew the size of the [[Afghan National Army]] to over 14,000 as well as fielding a [[corps]]-sized force ahead of schedule.<ref name="defendamerica"/> In August 2004, the brigade was replaced in this mission by the [[76th Infantry Brigade (United States)|76th Infantry Brigade]], and subsequently returned home to the United States.<ref name="defendamerica">{{cite web|url=http://www.defendamerica.mil/articles/aug2004/a080904g.html |title=Hoosiers Replace Sooners in Afghan Mission |publisher=Defend America News |accessdate=3 May 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20071013103202/http://defendamerica.mil/articles/aug2004/a080904g.html |archivedate=13 October 2007 |df=dmy }}</ref> The brigade spent three years back home, and in that time transformed into an infantry [[brigade combat team]] as a part of a new [[transformation of the United States Army|transformation plan for the Army]].<ref name="ArmyTimes">{{cite web|url=http://www.armytimes.com/news/2007/08/ap_brigademobilized_070822/ |title=45th Infantry troops mobilized for Iraq duty |publisher=Army Times |accessdate=3 May 2009}}</ref>

{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:whitesmoke; color:black; width:21em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |
'''Units of the 45th Infantry Brigade Combat Team, 2006–Present'''  
*HHC 45th Infantry Brigade
*1st Bn 179th Infantry
*1st Bn 180th Cavalry
*1st Bn 279th Infantry
*1st Bn 160th Field Artillery
*700th Brigade Support Bn
*Special Troops Bn, 45th Bde
|}
In March 2006, the 180th Cavalry (still infantry in '06) deployed as part of Task Force Phoenix V. They were attached to the 41st BCT (Oregon ARNG). They returned in June 2007. 
In April 2007, the brigade was alerted that it could be deployed to [[Iraq]] in support of [[Operation Iraqi Freedom]] by the end of the year. Four months later they were alerted that they would be heading to Iraq in 2008.<ref name="ArmyTimes"/> The brigade mobilized in October of that year and trained in infantry techniques at army posts in Oklahoma and Arkansas. The [[39th Infantry Brigade Combat Team (United States)|39th Infantry Brigade]] was also alerted for deployment during this time<ref name="ArmyTimes"/> and deployed to Iraq in late 2007.<ref name="Gov"/> During its rotation, the brigade was charged with turning over military facilities and [[Forward Operating Bases]] to the [[Iraqi Army]] as well as the [[Iraqi Police Force]]. The brigade returned to the United States in October 2008.<ref name="Gov">{{cite web|url=http://www.governor.state.ok.us/display_article.php?article_id=1152&article_type=0 |title=Governor's Welcome Home |publisher=Oklahoma State Government |accessdate=3 May 2009}}</ref>
The 45th IBCT deployed to Afghanistan in 2011 and reunited with the 201st Corps of the ANA, as partners this time, in combined combat operations against insurgent forces in Eastern Afghanistan<ref>{{cite web|url=http://www.tulsaworld.com/news/article.aspx?subjectid=511&articleid=20120125_511_0_LAGHMA164900 |title=45th Infantry joined forces with Afghan National Army to disrupt insurgency in the Nurilam Valley |last=Goble |first=Leslie |date=25 January 2012 |accessdate=12 June 2012 |publisher=''[[Tulsa World]]''}}</ref> suffering the loss of 14 Soldiers<ref>{{cite web|url=http://www.tulsaworld.com/webextra/continuingcoverage/default.aspx/45th_Infantry_in_Afghanistan/28 |title=45th Infantry in Afghanistan |publisher=''Tulsa World'' |accessdate=12 June 2012}}</ref> but making significant progress in disrupting and destroying insurgent operations while continuing to mentor the ANA and progressively handing off security missions to them. The full brigade mobilized in April 2011, but a late change in the mission diverted the 180th Cavalry and 160th Field Artillery to separate missions to support Iraq operations from Kuwait.

Additionally, elements of the 45th Brigade have deployed to [[Egypt]] (1–180th Infantry [[Multinational Force and Observers]] (MFO)), [[Kuwait]] (245th Military Intelligence Co [[Operation Iraqi Freedom|OIF]]), and for separate rotations to [[Iraq]] (245th Engineer Co [[Operation Iraqi Freedom|OIF]]) and [[Afghanistan]] (1–180th Infantry [[Task Force Phoenix]] V) as well as various homeland security missions.

==Brigade commanders==
* MG David C. Matthews        1 February 1968 to 30 June 1970
* BG George M. Donovan        1 July 1970 to 11 September 1973
* BG James C. Duaghtery      12 September 1973 to 31 January 1976
* BG Buster E. Smith          1 February 1976 to 24 June 1978
* BG Lawrence F. Roy         25 June 1978 to 30 June 1983
* BG Curtis W. Miligan        1 July 1983 to 15 February 1984
* BG James J. Wasson         16 February to 28 August 1987
* BG Donald G. Smith         29 August 1987 to 13 March 1991
* BG Allan F. Mc Gilbra      14 March 1991 to 6 December 1993
* BG James E. Walker          7 December 1994 to 10 February 1995
* BG Bradley D. Gambill      11 February 1995 to 10 July 1998
* MG Jerry W. Grizzle        11 July 1998 to 12 May 2001
* BG Thomas P. Mancino       13 May 2001 to 2 December 2004
* BG Myles L. Deering         3 December 2004 to 12 November 2008
* COL Lawrence I. Fleishman  13 November 2008 to 5 February 2010
* COL Joel P. Ward            6 February 2010 to 2 June 2012
* COL Van L. Kinchen          3 June 2012 to 2 August 2015
* COL David Jordan.           3 August 2015 to Present

==Lineage and Honors<ref>{{cite web|title=Lineag and Honors Headquarters Company, 45th Infantry Brigade Combat Team|url=http://www.history.army.mil/html/forcestruc/lineages/branches/div/0045inbdect.htm|website=United States Army Center of Military History|publisher=US Army|accessdate=5 January 2015}}</ref>==

===Lineage===
*Constituted 19 October 1920 as Headquarters, 45th Division (to be organized with troops from Arizona, Colorado, New Mexico, and Oklahoma).
*Organized and federally recognized 3 August 1923 at Oklahoma City, Oklahoma; Headquarters Detachment organized and federally. recognized 1 July 1924 at Oklahoma City, Oklahoma.
*Headquarters and Headquarters Detachment, 45th Division, inducted into federal service 16 September 1940 at Oklahoma City.
*Redesignated 23 February 1942 as the 45th Infantry Division.
*Reorganized and redesignated 23 February 1942 as Headquarters, 45th Infantry Division.
*Inactivated 7 December 1945 at Camp Bowie, Texas.
*Reorganized and federally recognized 5 September 1946 in the Oklahoma National Guard at Oklahoma City.
*Ordered into active federal service 1 September 1950 at Oklahoma City.
*(Headquarters, 45th Infantry Division [NGUS], organized and federally recognized 15 September 1952 at Oklahoma City).
*Released from active federal service 30 April 1954 and reverted to state control; federal recognition concurrently withdrawn from Headquarters, 45th Infantry Division (NGUS).
*Reorganized and redesignated 1 February 1968 as Headquarters, 45th Infantry Brigade, and location changed to Edmond (Headquarters Company, 45th Infantry Brigade, concurrently reorganized and redesignated from Headquarters Company, 1st Battalion, 179th Infantry).
*Location changed 1 October 1996 to Oklahoma City.
*Ordered into active Federal service 19 September 2003 at Oklahoma City; released from active Federal service 17 September 2004 and reverted to state control.
*Ordered into active Federal service 19 October 2007 at Oklahoma City.
*Reorganized and redesignated 1 September 2008 as Headquarters, 45th Infantry Brigade Combat Team.
*Released from active Federal service 21 November 2008 and reverted to state control.
*Location changed 1 July 2010 to Norman.
*Ordered into active Federal service 27 March 2011 at Norman; released from active Federal service 29 April 2012 and reverted to state control.

===Honors===
The brigade received all of the honors previously accorded to the 45th Infantry Division, including its [[campaign streamers]], which give credit for participation in combat. Additionally, several of these streamers contain the [[Arrowhead device]], signifying the division's participation in [[amphibious assault]]s.<ref name="Wilson664"/>
*As the 45th Infantry Division:
{| class="wikitable" style="float:center;"
|- style="background:#efefef;"
! Conflict
! Streamer
! Inscription
! Year(s)
|-
||[[File:Streamer FCDG WWII.png|200px|alt=A red ribbon with four vertical dark green stripes in the center.]]
||[[Croix de guerre 1939–1945 (France)|French Croix de Guerre]], World War II (With Palm)
||Embroidered "[[Acquafondata]]"  
||1943–1944
|-
|rowspan="8"| [[File:European-African-Middle Eastern Campaign Medal streamer.png|200px]]
|rowspan="8"| [[European-African-Middle Eastern Campaign Medal|European-African-Middle Eastern Campaign Streamer]]
|| [[Sicily]] (with [[Arrowhead (device)|Arrowhead]])
|| 1943
|-
|| [[Naples]]–[[Foggia]] (with Arrowhead)
|| 1943
|-
|| [[Anzio]] (wirth Arrowhead)
|| 1943
|-
|| Rome–[[Arno]]
|| 1944
|-
|| [[France|Southern France]] (with Arrowhead)
|| 1944
|-
|| [[Rhineland]]
|| 1944–1945
|-
|| [[Ardennes-Alsace|Ardennes–Alsace]]
|| 1944–1945
|-
|| Central Europe
|| 1945
|-
||[[File:Streamer KPUC.PNG|200px|alt=A white ribbon with vertical green and red stripes on its edges and a red and blue circle in the middle]]
||[[Republic of Korea Presidential Unit Citation]]
||For service in Korea
||1952–1953
|-
|rowspan="4"|[[File:Korean Service Medal - Streamer.png|200px]]
|rowspan="4"|[[Korean Service Medal|Korean Service Campaign Streamer]] 
|| Second Korean Winter
|| 1951–1952
|-
|| Korea, Summer–Fall 1952
|| 1952
|-
|| Third Korean Winter
|| 1952–1953
|-
|| Korea, Summer 1953
|| 1953
|}
* As 45th Infantry Brigade Combat Team
{| class="wikitable" style="float:center;"
|- style="background:#efefef;"
! Conflict
! Streamer
! Inscription
! Year(s)
|-
||[[File:Streamer MUC Army.PNG|200px]]
||[[Meritorious Unit Commendation]], HHC Only
||For service in [[Afghanistan]]  
||2011—2012
|-
|rowspan="4"|[[File:Streamer gwotE.PNG|200px]]
|rowspan="4"| [[Global War on Terror - OEF]]
|| Afghanistan, Consolidation I
|| 2003–2004, 2006
|-
|| Afghanistan, Consolidation II
|| 2006–2007
|-
|| Afghanistan, Consolidation III
|| 2011
|-
|| Afghanistan, Transition I
|| 2011–2012
|-
|rowspan="3"|[[File:Streamer IQCS.PNG|200px]]
|rowspan="3"| [[Global War on Terror - OIF/OND]]
|| Liberation of Iraq
|| 2003
|-
|| Iraq Surge
|| 2007–2008
|-
|| [[Iraq War#2010: U.S. drawdown and Operation New Dawn|Operation New Dawn]]
|| 2011–2012
|}
{{Clear}}

==References==
{{reflist|2}}

== Sources ==
* {{cite book|first=John J. |last=McGrath |title= The Brigade: A History: Its Organization and Employment in the US Army |year=2004 |publisher=Combat Studies Institute Press |isbn=978-1-4404-4915-4}}
* {{cite book|first=John B. |last=Wilson |title=Armies, Corps, Divisions, and Separate Brigades |year=1999 |publisher=[[United States Army Center of Military History]] |asin=B000OJKX1S| id = CMH Pub 60-7-1| url = http://www.history.army.mil/BOOKS/Lineage/ACDSB/ACDSB-FM.htm }}

==External links==
*[http://www.ok.ngb.army.mil/45InfBde/index.htm 45th Infantry Brigade homepage]
*[http://www.history.army.mil/html/forcestruc/lineages/branches/div/0045inbdect.htm Lineage] at the [[United States Army Center of Military History]]
*[http://www.texasmilitaryforcesmuseum.org/36division/archives/wwi/white/chap1.htm History of the 36th Division]

[[Category:Brigade combat teams of the United States Army|45]]
[[Category:Infantry brigades of the United States Army|45]]
[[Category:Brigades of the United States Army National Guard|45]]
[[Category:Military units and formations in Oklahoma|045]]