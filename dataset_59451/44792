{{For|a broader class of journals|Academic journal}}
{{redirect|Science journal|the journal named 'Science'|Science (journal)|the defunct magazine named 'Science Journal'|New Scientist}}
{{broader|Scientific literature}}
{{Lead too long|date=January 2015}}
[[File:Nature cover, November 4, 1869.jpg|thumb|Cover of the first issue of ''[[Nature (journal)|Nature]]'', 4 November 1869]]
In [[academic publishing]], a '''scientific journal''' is a [[periodical publication]] intended to further the progress of [[science]], usually by reporting new [[research]]. There are thousands of scientific journals in publication, and many more have been published at various points in the past (see [[list of scientific journals]]). Most journals are highly specialized, although some of the oldest journals such as ''[[Nature (journal)|Nature]]'' publish articles and [[scientific paper]]s across a wide range of scientific fields. Scientific journals contain articles that have been [[Scholarly peer review|peer review]]ed, in an attempt to ensure that articles meet the journal's standards of quality, and scientific [[validity]]. If the journal's editor considers the paper appropriate, at least two researchers preferably from the same field check the paper for soundness of its scientific argument, and must agree to publish it. Although scientific journals are superficially similar to [[professional]] [[magazine]]s, they are actually quite different. Issues of a scientific journal are rarely read casually, as one would read a magazine. The publication of the results of research is an essential part of the [[scientific method]]. If they are describing experiments or calculations, they must supply enough details that an independent researcher could repeat the experiment or calculation to verify the results. Each such journal article becomes part of the permanent scientific record.

The history of scientific journals dates from 1665, when the French ''[[Journal des sçavans]]'' and the English ''[[Philosophical Transactions of the Royal Society]]'' first began systematically publishing research results.  Over a thousand, mostly [[ephemeral]], were founded in the 18th century, and the number has increased rapidly after that.<ref>D. A. Kronick, ''History of Scientific and Technical Periodicals'', 2nd ed. Scarecrow, 1976</ref>

Articles in scientific journals can be used in research and higher education. Scientific articles allow researchers to keep up to date with the developments of their field and direct their own research. An essential part of a scientific article is citation of earlier work. The impact of articles and journals is often assessed by counting citations ([[citation impact]]). Some classes are partially devoted to the explication of classic articles, and [[seminar]] classes can consist of the presentation by each student of a classic or current paper. Schoolbooks and textbooks have been written usually only on established topics, while the latest research and more obscure topics are only accessible through scientific articles. In a scientific research group or [[academic department]] it is usual for the content of current scientific journals to be discussed in [[journal club]]s. Academic credentials for promotion into academic ranks are established in large part by the number and impact of scientific articles published, and many doctoral programs allow for [[thesis by publication]], where the candidate is required to publish a certain number of scientific articles.

The standards that a journal uses to determine publication can vary widely. Some journals, such as ''[[Nature (journal)|Nature]]'', ''[[Science (journal)|Science]]'', ''[[PNAS]]'', and ''[[Physical Review Letters]]'', have a reputation of publishing articles that mark a fundamental breakthrough in their respective fields.{{Citation needed|date=September 2014}} In many fields, an informal hierarchy of scientific journals exists; the most prestigious journal in a field tends to be the most selective in terms of the articles it will select for publication, and will also have the highest [[impact factor]].{{Citation needed|date=September 2014}}{{Original research inline|date=January 2015}} It is also common for journals to have a regional focus, specializing in publishing papers from a particular country or other geographic region, like ''[[African Invertebrates]]''.

Articles tend to be highly technical, representing the latest theoretical research and experimental results in the field of science covered by the journal. They are often incomprehensible to anyone except for researchers in the field and advanced students. In some subjects this is inevitable given the nature of the content. Usually, rigorous rules of [[scientific writing]] are enforced by the editors; however, these rules may vary from journal to journal, especially between journals from different publishers. Articles are usually either original articles reporting completely new results or reviews of current literature. There are also scientific publications that bridge the gap between articles and books by publishing thematic volumes of chapters from different authors.

==Types of articles==
{{further information|Scientific paper}}
{{see also|Academic publishing#Categories of papers{{!}}Categories of academic articles}}

[[File:1665 phil trans vol i title.png|thumb|Cover of the first volume of the ''[[Philosophical Transactions of the Royal Society]]'', the first journal in the world exclusively devoted to science]]

There are several types of journal articles; the exact terminology and definitions vary by field and specific journal, but often include:
*'''Letters''' (also called ''communications'', and not to be confused with ''letters to the editor'') are short descriptions of important current research findings that are usually fast-tracked for immediate publication because they are considered urgent.
*'''Research notes''' are short descriptions of current research findings that are considered less urgent or important than ''Letters''.
*'''Articles''' are usually between five and twenty pages and are complete descriptions of current original research findings, but there are considerable variations between scientific fields and journals – 80-page articles are not rare in [[mathematics]] or [[theoretical computer science]].
*'''Supplemental articles''' contain a large volume of tabular [[data]] that is the result of current research and may be dozens or hundreds of pages with mostly numerical data. Some journals now only publish this data electronically on the Internet.
*'''[[Review article]]s''' do not cover original research but rather accumulate the results of many different ''articles'' on a particular topic into a coherent narrative about the state of the art in that field. Review articles provide information about the topic and also provide journal references to the original research. Reviews may be entirely narrative, or may provide quantitative summary estimates resulting from the application of [[meta-analysis|meta-analytical methods]].
*'''[[Data paper]]s''' are articles dedicated to describe datasets. This type of article is becoming popular and journals exclusively dedicated to them have been established, e.g. ''[[Scientific Data]]'' and ''[[Earth System Science Data]]''.
*'''[[Video paper]]s''' are a recent addition to practice of scientific publications. They most often combine an online video demonstration of a new technique or protocol combined with a rigorous textual description.<ref>http://www.jove.com/</ref><ref>{{Cite web|url=http://videonauka.ru/|title=Научный журнал "Видеонаука"|date=|website=Scientific journal "Videonauka"|publisher=|access-date=}}</ref>

The formats of journal articles vary, but many follow the general [[IMRAD]] scheme recommended by the [[International Committee of Medical Journal Editors]]. Such articles begin with an ''[[Abstract (summary)|abstract]]'', which is a one-to-four-paragraph summary of the paper. The ''introduction'' describes the background for the research including a discussion of similar research. The ''materials and methods'' or ''experimental'' section provides specific details of how the research was conducted. The ''results and discussion'' section describes the outcome and implications of the research, and the ''conclusion'' section places the research in context and describes avenues for further exploration.

In addition to the above, some scientific journals such as ''Science'' will include a news section where scientific developments (often involving political issues) are described. These articles are often written by science journalists and not by scientists. In addition, some journals will include an editorial section and a section for letters to the editor. While these are articles published within a journal, in general they are not regarded as scientific journal articles because they have not been peer-reviewed.

==Electronic publishing==
{{Main article|Eprint|Electronic article|Electronic journal}}
Electronic publishing is a new area of information [[dissemination]]. One definition of electronic publishing is in the context of the scientific journal. It is the presentation of scholarly scientific results in only an electronic (non-paper) form. This is from its first write-up, or creation, to its publication or dissemination. The electronic scientific journal is specifically designed to be presented on the internet. It is defined as not being previously printed material adapted, or re-tooled, and then delivered electronically.<ref name=Heller/><ref name= Boyce-Dalterio/>

Electronic publishing will exist alongside paper publishing, because printed paper publishing is not expected to disappear in the future. Output to a screen is important for browsing and searching but is not well adapted for extensive reading. Paper copies of selected information will definitely be required{{Citation needed|date=January 2016}}. Therefore, the article has to be transmitted electronically to the reader's local printer. Formats suitable both for reading on paper, and for manipulation by the reader's computer will need to be integrated.<ref name=Heller/><ref name= Boyce-Dalterio/> Many journals are electronically available in formats readable on screen via [[web browsers]], as well as in portable document format [[PDF]], suitable for printing and storing on a local desktop or laptop computer. New tools such as [[JATS]] and [[Utopia Documents]] provide a 'bridge' to the 'web-versions' in that they connect the content in PDF versions directly to the [[WorldWideWeb]] via hyperlinks that are created 'on-the-fly'. The PDF version of an article is usually seen as the version of record, but the matter is subject to some debate.<ref name="Pettifer 2011">{{Cite journal |author1=Pettifer, S. |author2=McDermott, P. |author3=Marsh, J. |author4=Thorne, D. |author5=Villeger, A. |author6=Attwood, T.K. |title=Ceci n'est pas un hamburger: modelling and representing the scholarly article |journal=Learned Publishing |year=2011 |volume=24 |issue=3 |pages=207–220 |url=http://www.ingentaconnect.com/content/alpsp/lp/2011/00000024/00000003/art00009 |doi=10.1087/20110309}}</ref>

Electronic counterparts of established print journals already promote and deliver rapid dissemination of peer reviewed and edited, "published" articles. Other journals, whether spin-offs of established print journals, or created as electronic only, have come into existence  promoting the rapid dissemination capability, and availability, on the Internet. In tandem with this is the speeding up of peer review, copyediting, page makeup, and other steps in the process to support rapid dissemination.<ref>{{Cite web|url=https://scholarworks.iu.edu/dspace/bitstream/handle/2022/148/WP02-12B.html;jsessionid=8E99F087A1E6FFEC3DBC59015C33BAF1|title=The Internet and the Velocity of Scholarly Journal Publishing|last=Swygart-Hobaugh|first=Rob Kling, Amanda J.|website=scholarworks.iu.edu|access-date=2016-10-26}}</ref>

Other improvements, benefits and unique values of electronically publishing the scientific journal are easy availability of supplementary materials (data, graphics and video), lower cost, and availability to more people, especially scientists from non-developed countries. Hence, research results from more developed nations are becoming more accessible to scientists from non-developed countries.<ref name=Heller/>

Moreover, electronic publishing of scientific journals has been accomplished without compromising the standards of the refereed, [[peer review]] process.<ref name=Heller>
{{Cite encyclopedia| last =Heller| first =Stephen, R.| title =Electronic Publishing of Scientific Manuscripts| encyclopedia =[[Encyclopedia of Computational Chemistry]]| volume =02| pages =871–875| publisher =John Wiley & Sons| year =1998
| url =http://www.hellers.com/steve/resume/p146.html| accessdate =2010-06-16}}</ref><ref name= Boyce-Dalterio>{{Cite journal| last =Boyce| first =Peter B.|author2=Heather Dalterio | title =Electronic Publishing of Scientific Journals| journal =[[Physics Today]]| volume =49| issue =1| pages =42| publisher =American Institute of Physics| date =January 1996| url =http://aas.org/~pboyce/epubs/pt-art.htm#list| format =Article available to the public in HTML.
  | doi =10.1063/1.881598 |bibcode = 1996PhT....49a..42B }}</ref>

One form is the online equivalent of the conventional paper journal. By 2006, almost all scientific journals have, while retaining their peer-review process, established electronic versions; a number have moved entirely to electronic publication. In similar manner, most academic libraries buy the electronic version, and purchase a paper copy only for the most important or most-used titles.

There is usually a delay of several months after an article is written before it is published in a journal, making paper journals not an ideal format for announcing the latest research. Many journals now publish the final papers in their electronic version as soon as they are ready, without waiting for the assembly of a complete issue, as is necessary with paper. In many fields in which even greater speed is wanted, such as [[physics]], the role of the journal at disseminating the latest research has largely been replaced by [[preprint]] databases such as [[arXiv.org]]. Almost all such articles are eventually published in traditional journals, which still provide an important role in [[quality control]], archiving papers, and establishing scientific credit.

==Cost==
{{main article|Academic publishing#Publishers and business aspects}}
{{see also|Academic journal#Costs}}

Many scientists and librarians have long protested the cost of journals, especially as they see these payments going to large for-profit publishing houses {{Citation needed|date=April 2013}}. To allow their researchers online access to journals, many universities purchase ''site licenses'', permitting access from anywhere in the university, and, with appropriate authorization, by university-affiliated users at home or elsewhere. These may be quite expensive, sometimes much more than the cost for a print subscription, although this may reflect the number of people who will be using the license - while a print subscription is the cost for one person to receive the journal; a site-license can allow thousands of people to gain access.{{Citation needed|date=September 2014}}

Publications by [[scholarly societies]], also known as not-for-profit-publishers, usually cost less than commercial publishers, but the prices of their scientific journals are still usually several thousand dollars a year. In general, this money is used to fund the activities of the scientific societies that run such journals, or is invested in providing further scholarly resources for scientists; thus, the money remains in and benefits the scientific sphere.

Despite the transition to electronic publishing, the [[serials crisis]] persists.<ref>{{Cite journal |title=Harvard University says it can't afford journal publishers' prices |first=Ian |last=Sample |journal=The Guardian |date=24 April 2012 |url=https://www.theguardian.com/science/2012/apr/24/harvard-university-journal-publishers-prices}}</ref>

Concerns about cost and open access have led to the creation of free-access journals such as the [[Public Library of Science]] (PLoS) family and partly open or reduced-cost journals such as the ''[[Journal of High Energy Physics]]''. However, professional editors still have to be paid, and PLoS still relies heavily on donations from foundations to cover the majority of its operating costs; smaller journals do not often have access to such resources.

Based on statistical arguments, it has been shown that electronic [[publishing]] online, and to some extent [[open access]], both provide wider dissemination and increase the average number of citations an article receives.<ref>{{cite web |url=http://citeseer.ist.psu.edu/online-nature01/ |title=Online Or Invisible? |authorlink=Steve Lawrence (computer scientist) |first=Steve |last=Lawrence |publisher=[[NEC Research Institute]]}}</ref>

==Copyright==
Traditionally, the author of an article was required to transfer the [[copyright]] to the journal publisher. Publishers claimed this was necessary in order to protect authors' rights, and to coordinate permissions for reprints or other use. However, many authors, especially those active in the [[Open access (publishing)|open access]] movement, found this unsatisfactory,<ref name=Di_Cosmo>{{Cite journal
| issn = 1684-5285
| volume = 7
| issue = 3
| pages = 41–8
| last = Di Cosmo
| first = Roberto
| authorlink= Roberto Di Cosmo
| title = The Role of Public Administrations in The ICT Era
| journal = UPGRADE: the European Journal for the Informatics Professional
| date = June 2006
| url = http://www.cepis.org/upgrade/files/full-III-06.pdf
}}</ref> and have used their influence to effect a gradual move towards a license to publish instead. Under such a system, the publisher has permission to edit, print, and distribute the article commercially, but the authors retain the other rights themselves.

Even if they retain the copyright to an article, most journals allow certain rights to their authors. These rights usually include the ability to reuse parts of the paper in the author's future work, and allow the author to distribute  a limited number of copies. In the print format, such copies are called reprints; in the electronic format, they are called [[postprints]]. Some publishers, for example the [[American Physical Society]], also grant the author the right to post and update the article on the author's or employer's website and on free e-print servers, to grant permission to others to use or reuse figures, and even to reprint the article as long as no fee is charged.<ref>{{cite web |url=http://forms.aps.org/author/copyfaq.html |title=APS Copyright Policies and Frequently Asked Questions}}</ref> The rise of open access journals, in which the author retains the copyright but must pay a publication charge, such as the [[Public Library of Science]] family of journals, is another recent response to copyright concerns.

==See also==
* [[List of scientific journals]]
* [[Academic authorship]]
* [[Academic conference]]
* [[Citation index]]
* [[Copyright policies of scientific publishers]]
* [[Mega journal]]
* [[Open access journal]]
* [[Publish or perish]]
* [[Scientific writing]]
* [[San Francisco Declaration on Research Assessment]]

==References==
{{Reflist}}
{{refbegin}}
* A.J. Meadows, ed. The Scientific Journal. London : Aslib, c1979. ISBN 0-85142-118-0
* R.E. Abel et al. "Scholarly Publishing: Books Journals, Publishers, and Libraries in the Twentieth Century" N.Y.: Wiley, 2002. ISBN 0-471-21929-0
* D.W. King et al. "Scientific Journals in the United States: their Production, Use, and Economics. Stroudsberg, PA: Hutchinson-Ross, 1981 ISBN 0-87933-380-4
{{refend}}

==External links== <!-- section name is referenced above -->
* [http://openwetware.org/wiki/Publication_fees The cost of publishing in a scientific journal, some examples and recommended reading] from OpenWetWare life scientists' wiki

{{Academic publishing}}

[[Category:Technical communication]]
[[Category:Scientific journals| ]]