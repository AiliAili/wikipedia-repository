{{Infobox military person
|name= Guy M. Townsend III
|birth_date= {{birth date|1920|10|25}}
|death_date= {{Death date and age|2011|3|28|1920|10|25}}
|birth_place= [[Columbus, Mississippi]]
|death_place= [[Mercer Island, Washington]]
|image=  Col Guy Townsend EAFB 1964.jpg
|caption= Colonel Guy Townsend at Edwards Air Force Base in 1964
|nickname= 
|allegiance={{flag|United States of America}}
|branch= [[File:Seal of the US Air Force.svg|25px]] [[United States Air Force]] 
|serviceyears= 1941&ndash;70 (29 years)
|rank= [[File:US-O7 insignia.svg|25px]] [[Brigadier General (United States)|Brigadier general]]
|current position= 
|commands= Chief of Bomber Test, WPAFB <br/> Director Materiel, 93rd Bomb Wing <br/> Deputy Commander, ''Op. Power Flite'' <br/>Chief of Requirements Division, SAC <br/> Test Force Director, XB-70 <br/> Director of Flight Test, AFFTC <br/> Director, C-5 SPO <br/> Director, B-1 SPO
|unit= 
|battles= [[World War II]] <br/> [[Cold War]]
|awards= [[Legion of Merit]] <br/> [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] <br/> [[Air Medal]] <br/> [[Presidential Unit Citation (United States)|Distinguished Unit Citation Emblem]] <br/> [[Presidential Unit Citation (United States)|Presidential Unit Citation]] <br/> [[James H. Doolittle Award]] <br/> Pathfinder Award <br/> [[Aerospace Walk of Honor]]
|relations=
|laterwork= Aerospace Executive
}}
'''Guy Mannering Townsend III''' (October 25, 1920 &ndash; March 28, 2011)<ref>{{cite web |url=http://www.legacy.com/obituaries/seattletimes/obituary.aspx?n=guy-m-townsend&pid=150125514 |title=Guy M. Townsend III Obituary |author= |work= |publisher=Legacy.com |accessdate=April 19, 2011}}</ref> was a retired [[United States Air Force]] [[Brigadier General (United States)|brigadier general]], [[test pilot]], and combat veteran. As an Air Force officer, he served as chief of bomber test at [[Wright-Patterson Air Force Base]], flew as co-pilot on the first flight of the [[B-52 Stratofortress]], was test force director for the [[XB-70 Valkyrie]], and served as program director for the [[C-5 Galaxy]] and [[B-1 Lancer]]. He was the first military pilot to fly the [[B-47 Stratojet]], [[B-50 Superfortress]], B-52 Stratofortress, and the prototype of the [[KC-135 Stratotanker]]. During his years at [[Boeing]], he was the head of the [[Boeing 2707|Supersonic Transport]] operations organization.

==Biography==

===Early years===
Guy Townsend was born in [[Columbus, Mississippi]] in 1920 and graduated from [[San Jacinto High School (Texas)|San Jacinto High School]] in [[Houston, Texas]] in June 1939.<ref name=GMT_USAF_Bio>{{cite web | title = Biographies: Brigadier General Guy M. Townsend | url = http://www.af.mil/AboutUs/Biographies/Display/tabid/225/Article/105436/brigadier-general-guy-m-townsend.aspx}}</ref> He attended [[Texas A&M University]] in [[College Station, Texas]] where he studied [[Aerospace engineering|aeronautical engineering]] from September 1939 to June 1941.<ref name=GMT_USAF_Bio/>

===Military career===
Townsend's military service began as an aviation cadet in the [[United States Army Air Corps|Army Air Corps]] in October 1941.<ref name=GMT_USAF_Bio/> He received pilot training at [[Ontario, California]] and [[Victorville, California]] and was commissioned a [[Second Lieutenant#United States|second lieutenant]] in May 1942.<ref name=GMT_USAF_Bio/> During [[World War II]], Townsend flew 450 combat hours in [[B-17 Flying Fortress|B-17]]s and [[B-29 Superfortress|B-29]]s in the [[Asiatic-Pacific Theater|Pacific Theater of Operations]].<ref name=GMT_AWOH>{{cite web | url = http://www.cityoflancasterca.org/Index.aspx?page=205#townsend | title = Aerospace Walk on Honor, 1995 Honorees, Guy M. Townsend}}</ref> He returned home in October 1945 to serve for three years as pilot and flight test officer at Wright-Patterson Air Force Base near [[Dayton, Ohio]].<ref name=GMT_USAF_Bio/> Promoted to major, he attended the Flight Performance School (later renamed the [[U.S. Air Force Test Pilot School]]) and graduated with Class 46F.<ref name=TPS50>(1994) ''USAF Test Pilot School 50 Years and Beyond'', p. 250</ref> Townsend demonstrated how the B-29 could be flown to 40,000 feet, well above its intended service ceiling.<ref name=GMT_AWOH/> In 1948, he was promoted to chief of bomber test and served in this capacity for his remaining three years at Wright-Patterson.<ref name=GMT_USAF_Bio/> In 1951, he was assigned to the [[Air Force Flight Test Center]] (AFFTC) at [[Edwards Air Force Base]] in California, although his duties kept him primarily at Boeing in [[Seattle, Washington]].<ref name=GMT_USAF_Bio/>

====B-47 Stratojet====
[[File:B47B with chute.jpg|left|thumb|B-47 landing with deployed drag chute]]
In July 1948, Major Townsend started as the lead military pilot for the Air Force evaluation of the Boeing B-47 jet bomber. A result of B-47's sleek design, the aircraft required a high landing speed and an excessively long distance to stop. Townsend suggested the addition of a parachute, deployed shortly before touch-down, to shorten the landing distance. Equally dangerous, landing at a reduced engine power setting left the pilot unable to quickly command additional thrust due to the slow acceleration characteristics of early jet engines. A fellow B-47 test pilot, Major [[Russell E. Schleeh]], proposed a second parachute, deployed while in the [[Airfield traffic pattern|landing pattern]], to permit a higher engine power setting during the approach.{{sfnp|Tegler|2000|p=15}}

The prototype B-47's long, swept wing was prone to twisting during [[Aircraft principal axes#Longitudinal axis (roll)|roll maneuvers]] which caused a dangerous behavior known as [[Control reversal|aileron reversal]] to occur at airspeeds lower than expected. Based on experience with earlier aircraft, Townsend proposed the use of spoiler ailerons on the B-47 to reduce this unwanted behavior. The use of spoiler ailerons to reduce wing twisting was first tested on the B-47, and although not used on production B-47s, became standard issue for lateral control on many later jets.{{sfnp|Abzug|2002|p=291}}

{{Quote box
|align=right
|width=35%
|quote=Guy did a fantastic job of demonstrating the aircraft to K.B. It was Guy Townsend that really sold the airplane.
|source=Bob Robbins, B-47 test pilot{{sfnp|Tegler|2000|pp=16–17}}
|}}
Although the B-47 held great promise, the Air Force was not particularly interested in the aircraft as its capabilities exceeded those specified for a medium bomber and fell short of those specified for a heavy bomber. Colonel Henry E. "Pete" Warden of Wright Field wanted to convince the head of the Air Force's Bomber Production, Major General K. B. Wolfe, of the merits of the B-47. He persuaded Wolfe to take a trial flight with Townsend, who put on a spectacular demonstration. After the flight, Wolfe was convinced and lent his support to the new bomber.{{sfnp|Boyne|2001|p=200}} Boeing eventually built over two thousand B-47s,{{sfnp|Boyne|2007|p=104}} more than any other United States bomber manufactured under peacetime conditions.<ref name=NASA_History>{{cite book | last = Loftin |title=Quest for Performance: The Evolution of Modern Aircraft | year = 2004 | chapter = Chapter 12: Jet Bomber and Attack Aircraft | chapterurl = http://www.hq.nasa.gov/pao/History/SP-468/ch12-2.htm}}</ref>

====B-52 Stratofortress====
[[File:YB-52sideview.jpg|left|thumb|YB-52 in flight]]
Townsend transitioned to the B-52 program, an aircraft General [[Nathan Farragut Twining|Nathan Twining]] called "the long rifle of the air age."{{sfnp|Knaack|1988|p=230}} He participated with contractor pilots in Phase I and conducted Phases II and IV.<ref name=MOF_GMT_Pathfinder/> At the time, the Air Force divided aircraft testing into seven numbered phases including: Phase 1, to determine contractor compliance with the aircraft held to 80% of design limits; Phase II, similar to Phase I but performed by Air Force pilots; and Phase IV, performance and stability testing that expanded the flight envelope to 100% of design limits.{{sfnp|Knaack|1988|p=225}}

On April 15, 1952, Boeing pilot [[Alvin M. Johnston|"Tex" Johnston]] and co-pilot [[Lieutenant colonel (United States)|Lt. Col.]] Guy Townsend flew the first flight of the YB-52 prototype.<ref name=B52_FF>{{cite web | title = B-52 First Flight: Drama Equals Broadway Opening | url = http://www.boeing.com/defense-space/military/b52-strat/b52_50th/ff.htm}}</ref> The flight from [[Boeing Field]] to [[Larson Air Force Base|Moses Lake Army Air Base]] (later renamed Larson Air Force Base) near [[Moses Lake, Washington]] lasted two hours and fifty-one minutes.{{sfnp|Dorr|2000|p=25}} At the time, this was the longest and most successful first flight in Boeing history.<ref name=B52_FF/> The pilots reported only relatively minor problems that were quickly corrected by Boeing engineers.{{sfnp|Lake|2004|p=8}}

[[File:B-52-castleafb-1957.jpg|right|thumb|Three B-52Bs of the 93rd Bomb Wing prepare to depart [[Castle AFB]], California, for their record-setting round-the-world flight in 1957]]
In 1955, Townsend left flight test for a time when was assigned as the deputy director of operations (later director of materiel) of the [[93d Air-Ground Operations Wing|93rd Bomb Wing]] at [[Castle Air Force Base]] in central California.<ref name=GMT_USAF_Bio/> The 93rd was the first wing to receive B-52 and KC-135 type aircraft.<ref name=GMT_USAF_Bio/> In January 1957, Townsend was deputy commander for ''[[Operation Power Flite]]'', a group of three B-52Bs that made a nonstop flight around the world in 45 hours with several in-flight refuelings.{{sfnp|Gates|2003}} Unfortunately, the refueling receptacle on Townsend's B-52, ''La Vittoria'', froze solid forcing him to abort to [[CFB Goose Bay|Goose Bay Air Base]] in [[Labrador]].{{sfnp|Bowman|2005|p=34}} The 93rd Bomb Wing received the [[Mackay Trophy]] for their accomplishment.<ref name=Mackay>{{cite web | url = http://www.naa.aero/html/awards/index.cfm?cmsid=191 | title = Mackay 1950-1959 Winners | accessdate =June 11, 2009 | work = NAA Awards | publisher=National Aeronautic Association}}</ref> In February 1958, Townsend reported to the Directorate of Operations in the headquarters of [[Strategic Air Command]] at [[Offutt Air Force Base]], Nebraska as deputy chief of the Requirements Division.<ref name=GMT_USAF_Bio/> In less than a year, he was named chief of the Requirements Division.<ref name=GMT_USAF_Bio/>

Townsend returned to the AFFTC in March 1962 as the test force director of the XB-70 Valkyrie program.<ref name=GMT_USAF_Bio/> In December 1963 he became the center's director of flight test and in July 1964 the deputy for systems test.<ref name=GMT_USAF_Bio/> Promoted to Brigadier General, Townsend was assigned in September 1965 as director of the C-5 Galaxy system program office at the [[Aeronautical Systems Center|Aeronautical Systems Division]] of [[Air Force Systems Command]] located at Wright-Patterson Air Force Base, Ohio.<ref name=GMT_USAF_Bio/> In July 1968, he was assigned as deputy for systems management and in November 1969 as the systems program director of the B-1 Lancer program.<ref name=GMT_USAF_Bio/> Townsend retired from the Air Force on October 1, 1970.<ref name=GMT_USAF_Bio/>

===Boeing career===
After retiring from the Air Force, Townsend joined the Boeing Company as the head of the Supersonic Transport (SST) operations organization.<ref name=MOF_GMT_Pathfinder/> After the SST program was cancelled in 1971, he supported a number of other Boeing efforts including the [[Boeing E-4|E-4 Advanced Airborne Command Post]], the [[Boeing YC-14|YC-14]], the [[Microwave Landing System]], and the [[B-2 Spirit]].<ref name=MOF_GMT_Pathfinder/> Townsend retired from Boeing in 1986.<ref name=MOF_GMT_Pathfinder/>

===Later years===
Townsend remained active in aviation after his second retirement. In 2003 and at 82 years of age, he regularly flew aerobatics as co-owner of a private plane.{{sfnp|Gates|2003}} He continued flying until 2007 when failing eyesight forced him to stop. Townsend shared his flight test experiences at many events including:
* ''B-52 50th Anniversary'' &mdash; A speech to 5,000 current and former Boeing employees, armed forces veterans, and others. The event was held on April 12, 2002 at the Boeing Integrated Defense Systems plant in [[Wichita, Kansas]] to commemorate the 50th anniversary of the first flight of the B-52.<ref name=BA_B52_50th>{{cite web | title = Boeing Wichita Celebrates 50 Years of B-52s | url = http://www.boeing.com/defense-space/military/b52-strat/b52_50th/celebration.htm |date = April 17, 2002 | accessdate = June 14, 2009 | publisher=Boeing}}</ref>
* ''The B-47: "A Revolution in Aviation"'' &mdash; A panel discussion on the design and operation of the B-47.  The event was held on December 8, 2007 at the [[Museum of Flight]] in Seattle, Washington to mark the 60th anniversary of the first flight of the XB-47.<ref name=MOF_B47_60th>{{cite web | title = The B-47: "A Revolution in Aviation" | url = http://www.museumofflight.org/event/b-47 | accessdate = June 14, 2009 | publisher=Museum of Flight}}</ref>
* ''The Boeing B-52 Stratofortress-A Story of Design Longevity'' &mdash; A panel presentation on the B-52's exceptional service life. The event was held on February 21, 2009 at the Museum of Flight in Seattle, Washington.<ref name=MOF_B52_Longevity>{{cite web | title = The Boeing B-52 Stratofortress--A Story of Design Longevity | url = http://www.museumofflight.org/press/boeing-b-52-stratofortress-story-design-longevity |date = February 12, 2009 | accessdate =June 14, 2009 | publisher=Museum of Flight}}</ref>

Townsend and his wife Ann lived in the Covenant Shores community on [[Mercer Island, Washington]].{{sfnp|Gates|2003}}

Guy Mannering Townsend III died in his sleep on March 28, 2011.

== Honors ==
Townsend was awarded the following medals for his military service: [[Legion of Merit]], [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]], [[Air Medal]], [[Presidential Unit Citation (United States)|Distinguished Unit Citation Emblem]], [[Presidential Unit Citation (United States)|Presidential Unit Citation]], [[National Defense Service Medal]], and the [[Air Force Longevity Service Award|Air Force Longevity Service Award Ribbon]].<ref name=GMT_AWOH/>

In 1969, the [[Society of Experimental Test Pilots]] presented Townsend with the [[James H. Doolittle Award]] recognizing outstanding accomplishment in technical management or engineering achievement in aerospace technology.<ref name=SETP_GMT_DA>{{cite web | title = J. H. Doolittle Award | url = http://www.setp.org/winners/j-h-doolittle-recipients.html | accessdate = September 25, 2010 | publisher=Society of Experimental Test Pilots}}</ref> In 1994, the Museum of Flight presented him with the Pathfinder Award recognizing those individuals with ties to the Pacific Northwest who have made significant contributions to the development of the aerospace industry.<ref name=MOF_GMT_Pathfinder>{{cite web | title = Pathfinder Awards 1994, Guy M. Townsend | url = http://www.museumofflight.org/files/video/PathFinders/}}</ref> In 1995, Townsend was inducted into the [[Aerospace Walk of Honor]] in [[Lancaster, California]] that recognizes test pilots who have contributed to aviation and space research and development.<ref name=GMT_AWOH/> On October 25, 2002, the Flight Test Historical Foundation recognized Townsend and five others as distinguished flight test pioneers for their work on the B-52 flight test program.{{sfnp|Fergione|2002}}

Townsend was the first military pilot to fly the B-47 Stratojet, B-50 Superfortress, B-52 Stratofortress, and the 367-80 prototype of the KC-135 Stratotanker.<ref name=GMT_USAF_Bio/> He also test flew the [[Convair B-36]], [[B-45 Tornado|North American B-45]], [[Convair XB-46]], [[Martin XB-48]], and the [[Martin XB-51]].<ref name=GMT_USAF_Bio/>  As of October 1968, he had logged more than 8,000 hours of flying time&mdash;5,000 in experimental flight testing in many different aircraft.<ref name=GMT_USAF_Bio/>

==Notes==
{{reflist|2}}

==References==
*{{cite book
| last=Abzug 
| first=Malcolm J. 
| authorlink=
|author2=E. Eugene Larrabee
 | title= Airplane stability and control: a history of the technologies that made aviation possible
| edition=2nd
| publisher=Cambridge University Press
| year=2002
| isbn= 978-0-521-80992-4}}
*{{cite web
 | url = http://www.cityoflancasterca.org/Index.aspx?page=205#townsend
 | title = Aerospace Walk of Honor, 1995 Honorees, Guy M. Townsend
 | archiveurl = http://web.archive.org/web/20140527215439/http://www.cityoflancasterca.org/index.aspx?page=205
 | archivedate = May 27, 2014
 | accessdate =February 26, 2017
 | work = Aerospace Walk of Honor
 | publisher=City of Lancaster, California}}
*{{cite web
 | title = B-52 First Flight: Drama Equals Broadway Opening
 | archiveurl = https://web.archive.org/web/20130104150725/http://www.boeing.com/defense-space/military/b52-strat/b52_50th/ff.htm
 | archivedate = January 4, 2013
 | url = http://www.boeing.com/defense-space/military/b52-strat/b52_50th/ff.htm
 | accessdate = July 18, 2015
 | publisher=Boeing}}
*{{cite web
 | title = Biographies: Brigadier General Guy M. Townsend 
 | url = http://www.af.mil/AboutUs/Biographies/Display/tabid/225/Article/105436/brigadier-general-guy-m-townsend.aspx
 | access-date = June 5, 2016 
 | publisher=United States Air Force}}
*{{cite book
| last=Bowman 
| first=Martin W. 
| authorlink= 
| title= Stratofortress: The Story of the B-52
| publisher=Pen & Sword Aviation
| location=Barnsley, South Yorkshire
| year=2005
| isbn= 978-1-84415-234-6
| url=https://books.google.com/?id=78KfAAAACAAJ
| accessdate=July 17, 2009}}
*{{cite journal | last = Boyne | first = Walter J. | authorlink =  Walter J. Boyne | title = Airpower Classics, B-47 Stratojet | journal = Air Force Magazine | page = 104 | publisher = Air Force Association |date=August 2007 | url = http://www.airforce-magazine.com/MagazineArchive/Magazine%20Documents/2007/August%202007/0807classics.pdf | archiveurl = https://web.archive.org/web/20120219010529/http://www.airforce-magazine.com/MagazineArchive/Magazine%20Documents/2007/August%202007/0807classics.pdf |archivedate = February 19, 2012 | format = PDF | accessdate = July 18, 2015}}
*{{cite book
| last=Boyne 
| first=Walter J. 
| authorlink= Walter J. Boyne
| title= The Best of Wings Magazine
| publisher=Brassey's
| year=2001
| isbn= 1-57488-368-2
| url=https://books.google.com/?id=i984H9SGXGgC
| accessdate=May 31, 2009}}
*{{cite book
| last=Dorr 
| first=Robert F.
| authorlink=Robert F. Dorr
|author2=Lindsay T. Peacock |author3=Tony Holmes
 | title= B-52 Stratofortress: Boeing's Cold War Warrior
| publisher=Osprey Publishing, Limited
| year=2000
| isbn= 978-1-84176-097-1
| url=https://books.google.com/?id=MjoNAAAACAAJ
| accessdate=June 11, 2009}}
*{{cite web
 | last = Fergione 
 | first = John A. 
 | url = http://www.flighttest.org/gatheringOfEagles.htm 
 | date = August 22, 2002
 | archiveurl = https://web.archive.org/web/20021017005619/http://www.flighttest.org/gatheringOfEagles.htm
 | archivedate=October 17, 2002
 | title = The Flight Test Historical Foundation Gathering of Eagles
 | accessdate = June 13, 2009
 | publisher = Flight Test Historical Foundation}}
*{{cite news
 | title = Last of Boeing's fabled bombers is still wars' workhorse
 | first = Dominic
 | last = Gates
 | url = http://seattletimes.nwsource.com/html/iraq/134665266_b52plane30.html
 | agency = 
 | work = The Seattle Times
 | publisher = The Seattle Times Company
 | location = Seattle, Washington
 | isbn = 
 | issn = 
 | oclc = 
 | pmid = 
 | pmd = 
 | bibcode = 
 | doi = 
 | id = 
 | date = March 30, 2003
 | page = 
 | pages = 
 | accessdate = June 7, 2009
 | archiveurl = https://web.archive.org/web/20030413043604/http://seattletimes.nwsource.com/html/iraq/134665266_b52plane30.html
 | archivedate = April 13, 2003
 }}
*{{cite book |last= Knaack | first= Marcelle Size
|authorlink= |title= Post-World War II Bombers, 1945-1973 |publisher= Office of Air Force History |location=Washington, D.C. |year=1988 |isbn=0-16-002260-6 |url= http://www.airforcehistory.hq.af.mil/Publications/fulltext/encyclopedia_postww2_bombers.pdf 
|archiveurl = http://web.archive.org/web/20110609020358/http://www.airforcehistory.hq.af.mil/Publications/fulltext/encyclopedia_postww2_bombers.pdf
|archivedate = June 9, 2011
|accessdate=February 26, 2017}}
*{{cite book |last= Lake | first= Jon
|authorlink= |title= B-52 Stratofortress Units in Combat 1955-1973 |publisher= Osprey Publishing |year=2004 |isbn=978-1-84176-607-2}}
*{{cite book
 | last = Loftin 
 | first = Laurence K., Jr.
 | url = http://www.hq.nasa.gov/pao/History/SP-468/cover.htm
 | work = NASA SP-468
 | date = August 6, 2004
 | title = Quest for Performance: The Evolution of Modern Aircraft
 | accessdate = June 13, 2009
 | publisher = National Aeronautics and Space Administration History Office
 | isbn = 9997398939}}
*{{cite web | title = Pathfinder Awards 1994, Guy M. Townsend | url = http://www.museumofflight.org/files/video/PathFinders/ | accessdate =June 3, 2009 | publisher=Museum of Flight}}
*{{cite book |last= Tegler | first= Jan
|authorlink= |title= B-47 Stratojet: Boeing's Brilliant Bomber |publisher= McGraw-Hill Professional |year=2000 |isbn=978-0-07-135567-4 |url=https://books.google.com/?id=1QOkMX3Xc4cC |accessdate=June 5, 2006}}
*{{cite book |last= |first= |title=USAF Test Pilot School 50 Years and Beyond |publisher=Privately Published |year=1994 |id=}}

==External links==
*{{cite web
 | last = 
 | first = 
 | url = http://www.boeing.com/defense-space/military/b52-strat/b52_50th/photo6.html
 | archiveurl = https://web.archive.org/web/20121010153404/http://www.boeing.com/defense-space/military/b52-strat/b52_50th/photo6.html
 | archivedate = October 10, 2012
 | date = April 17, 2002
 | title =Retired U.S. Air Force Brig. Gen. Guy Townsend autographs a B-52 photo
 | accessdate = July 18, 2015
 | publisher = Boeing}}
*{{cite web
 | last = 
 | first = 
 | url = http://www.b-47.com/How%20it%20started.htm
 | archiveurl = https://web.archive.org/web/20111021055431/http://www.b-47.com/How%20it%20started.htm
 | archivedate = October 21, 2011
 | title =How the B-47 Grew from the Start
 | accessdate = July 18, 2015
 | publisher = B-47 Stratojet Association}}
*{{cite journal
 | last        = 
 | first       = 
 | authorlink  = 
 |date=Summer 2003
 | title       = A Townsend Flew the First B-52 Bomber
 | journal     = Townsend Genealogical Journal
 | volume      = I
 | issue       = III
 | pages       = 5&ndash;6
 | publisher   = Townsend Society of America
 | location    = Oyster Bay, New York
 | url         = http://www.townsendsociety.org/members_only/newsletters/newsletters/summer%202003.pdf
 | archiveurl = https://web.archive.org/web/20070101131459/http://www.townsendsociety.org/Members_Only/Newsletters/newsletters/Summer%202003.pdf
 | archivedate=January 1, 2007
 | format      = [[PDF]]
 | accessdate  = July 18, 2015
}}
*{{cite web
 | url = http://www.aiaa.org/content.cfm?pageid=260&period=1950s
 | archiveurl = https://web.archive.org/web/20110616200219/http://www.aiaa.org/content.cfm?pageid=260&period=1950s
 | archivedate = June 16, 2011
 | title = The History Of Flight From Around The World 1950s
 | accessdate = July 18, 2015
 | publisher = American Institute of Aeronautics}}
*{{cite web
 | url = http://www.check-six.com/Museum/1st-flights-m.htm
 | title = Cover Flown On First Flight of the Boeing YB-52 "Stratofortress"
 | accessdate = June 14, 2009
 | publisher = Check Six}}
*{{cite web
 | last = Puffer 
 | first = Raymond 
 | url = http://www.af.mil/news/Apr2002/n20020412_0571.shtml
 | date = April 12, 2002
 | archiveurl = https://web.archive.org/web/20030624060601/http://www.af.mil/news/Apr2002/n20020412_0571.shtml
 | archivedate = June 24, 2003
 | title = The venerable B-52: 50 years and counting
 | accessdate = June 13, 2009
 | work = Air Force Link
 | publisher = United States Air Force}}
*{{cite web
 | last = Wagner 
 | first = Ray 
 | url = http://www.americancombatplanes.com/b52_1.html
 | title = Designing the B-52
 | accessdate = June 14, 2009
 | publisher = American Combat Planes}}

{{DEFAULTSORT:Townsend, Guy M.}}
[[Category:1920 births]]
[[Category:2011 deaths]]
[[Category:American aviators]]
[[Category:American military personnel of World War II]]
[[Category:American test pilots]]
[[Category:Aviators from Mississippi]]
[[Category:People from Mercer Island, Washington]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Air Medal]]
[[Category:Texas A&M University alumni]]
[[Category:United States Air Force generals]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:U.S. Air Force Test Pilot School alumni]]