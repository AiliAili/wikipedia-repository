{{good article}}
{{Infobox hurricane season full
| Basin= Atl
| Name = 1857 Atlantic hurricane season
| Year=1857
| Track=1857 Atlantic hurricane season map.png
| First storm formed=June 30, 1857
| Last storm dissipated=September 30, 1857
| Strongest storm name=[[#Hurricane Two|Two]] and [[#Hurricane Four|Four]]
| Strongest storm pressure=
| Strongest storm winds=90
| Total depressions=
| Total storms=4
| Total hurricanes=
| Total intense=
| Damages=
| Fatalities=424
| Inflation=1
| five seasons=[[1855 Atlantic hurricane season|1855]], [[1856 Atlantic hurricane season|1856]], '''1857''', [[1858 Atlantic hurricane season|1858]], [[1859 Atlantic hurricane season|1859]]
}}
The '''1857 Atlantic hurricane season''' was the earliest season documented by [[HURDAT]] &ndash; the official Atlantic hurricane database &ndash; to feature no major hurricane.{{#tag:ref|A major hurricane is defined as a tropical cyclone reaching Category&nbsp;3 or stronger on the modern day [[Saffir–Simpson hurricane wind scale]]<ref name="ACE"/>|group="nb"}} A total of four [[tropical cyclone]]s were observed during the season, three of which strengthened into hurricanes. However, in the absence of modern satellite and other remote-sensing technologies, only storms that affected populated land areas or encountered ships at sea are known, so the actual total could be higher. An undercount bias of zero to six tropical cyclones per year between 1851 and 1885 has been estimated.<ref>{{cite book|title=Hurricanes and Typhoons: Past, Present and Future |chapter=The Atlantic hurricane database re-analysis project: Documentation for the 1851–1910 alterations and additions to the HURDAT database |author =Christopher W. Landsea|year=2004|publisher=Columbia University Press|isbn=0-231-12388-4 |pages=177–221  }}</ref> Additionally, documentation by Jose Fernandez-Partagas and Henry Diaz included a fifth tropical cyclone near [[Port Isabel, Texas]];<ref name="partagas-1">{{cite book|author1=José Fernández-Partagás |author2=Henry F. Diaz |title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources 1851-1880 Part 1: 1851-1870|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/part1.htm
|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|location=Miami, Florida|year=1995|accessdate=March 5, 2014}}</ref> this storm has since been removed from HURDAT as it was likely the same system as the fourth tropical cyclone.<ref name="meta">{{cite report|year=2008|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|publisher=National Oceanic and Atmospheric Administration, Atlantic Oceanographic and Meteorological Laboratory, Hurricane Research Division|accessdate=March 5, 2014|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html#1863_1}}</ref>

The first storm was tracked beginning on June&nbsp;30 offshore [[North Carolina]]. It moved eastward and was last noted on the following day. However, no tropical cyclones were reported in the remainder of July or August. Activity resume when another tropical storm was located southeast of the [[Bahamas]] on September&nbsp;6. It intensified into a hurricane before making [[Landfall (meteorology)|landfall]] in North Carolina and was last noted over the north Atlantic Ocean on September&nbsp;17. The ''[[SS Central America]]'' sank offshore, drowning 424 passengers and crew members. Another hurricane may have existed east of [[South Carolina]] between September&nbsp;22 and October&nbsp;26, though little information is available. The final documented tropical cyclone was initially observed east of [[Lesser Antilles]] on September&nbsp;24. It traversed the [[Caribbean Sea]] and the [[Gulf of Mexico]], striking the [[Yucatán Peninsula]] and later Port Isabel, Texas. The storm dissipated on September&nbsp;30. In Texas, damage was reported in several towns near the mouth of the [[Rio Grande|Rio Grande River]].

The season's activity was reflected with a low [[accumulated cyclone energy]] (ACE) rating of 43.<ref name="ACE">{{cite report|work=[[Hurricane Research Division]]|publisher=National Oceanic and Atmospheric Administration|date=March 2011|title=Atlantic basin Comparison of Original and Revised HURDAT|accessdate=March 5, 2014|url=http://www.aoml.noaa.gov/hrd/hurdat/Comparison_of_Original_and_Revised_HURDAT_mar11.html}}</ref> ACE is, broadly speaking, a measure of the power of the hurricane multiplied by the length of time it existed, so storms that last a long time, as well as particularly strong hurricanes, have high ACEs. ACE is only calculated for full advisories on tropical systems at or exceeding 34&nbsp;[[knot (unit)|knots]] (39&nbsp;mph, 63&nbsp;km/h) or tropical storm strength.<ref>{{cite report|author =David Levinson|date=August 20, 2008|title=2005 Atlantic Ocean Tropical Cyclones|work=[[National Climatic Data Center]]|publisher=National Oceanic and Atmospheric Administration|accessdate=March 5, 2014|url=http://www.ncdc.noaa.gov/oa/climate/research/2005/2005-atlantic-trop-cyclones.html}}</ref>
__TOC__

==Systems==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/06/1857 till:01/11/1857
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/06/1857

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:30/06/1857 till:01/07/1857 color:TS text:"One (TS)"
  from:06/09/1857 till:17/09/1857 color:C2 text:"Two (C2)"
  from:22/09/1857 till:26/09/1857 color:C1 text:"Three (C1)"
  from:24/09/1857 till:30/09/1857 color:C2 text:"Four (C2)"

  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/06/1857 till:01/07/1857 text:June
  from:01/07/1857 till:01/08/1857 text:July
  from:01/08/1857 till:01/09/1857 text:August
  from:01/09/1857 till:01/10/1857 text:September
  from:01/10/1857 till:01/11/1857 text:October

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

===Tropical Storm One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1857 Atlantic tropical storm 1 track.png
|Formed=June 30
|Dissipated=July 1
|1-min winds=50
}}
The ship ''Star of the South'' experienced heavy gales offshore the [[East Coast of the United States]] on June&nbsp;30.<ref name="partagas-1"/> HURDAT lists the first tropical cyclone of the season beginning at 0000&nbsp;UTC, while located about {{convert|100|mi|km}} southeast of [[Cape Hatteras]], North Carolina.{{Atlantic hurricane best track}} The storm moved slightly north of due east with winds of 60&nbsp;mph (95&nbsp;km/h).{{Atlantic hurricane best track}} It was last noted about 265&nbsp;miles (425&nbsp;km) north-northwest of [[Bermuda]] by the bark ''Virginia'' late on July&nbsp;1.<ref name="partagas-1"/>{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1857 North Carolina hurricane track.png
|Formed=September 6
|Dissipated=September 17
|1-min winds=90
|Pressure=961
}}
A tropical storm was first observed east of the Bahamas on September&nbsp;6. It moved slowly northwestward towards the coast of the United States and attained hurricane strength early on September&nbsp;9. The cyclone continued travelling northwest along the US coast, becoming a Category 2 hurricane whilst off the coast of Georgia on September 11. On September 13 the cyclone made landfall near [[Wilmington, North Carolina]], but then quickly weakened to a tropical storm and turned eastward into the Atlantic on September&nbsp;14. Throughout September&nbsp;15, whilst over water, the storm regained hurricane strength and continued northward before becoming extratropical in the mid-Atlantic on September&nbsp;17.{{Atlantic hurricane best track}}

The hurricane caused much coastal damage particularly in the [[Cape Hatteras]] area during September 9 and September 10 and then to other parts of the North Carolina coast. Flooding was reported at [[New Bern]].<ref name="NCar">{{cite report|author =James E. Hudgins|year=2000|title=Tropical cyclones affecting North Carolina since 1586 - An Historical Perspective|work=[[National Weather Service]]|publisher=National Oceanic and Atmospheric Administration|url=http://www.erh.noaa.gov/er/rnk/Research/NC_Tropical_Cyclone_History.pdf|accessdate=March 5, 2014}}</ref> Considerable wind damage also occurred. An article from the ''Wilmington Journal'' reported that, "It looked as though everything that could be blown down, was down. Fences were prostrated in all directions, and the streets filled with the limbs and bodies of trees up-rooted or twisted off.".<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_dec12.html|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|author1=Christopher W. Landsea |author2=Craig Anderson |author3=William Bredemeyer |author4=Cristina Carrasco |author5=Noel Charles |author6=Michael Chenoweth |author7=Gil Clark |author8=Sandy Delgado |author9=Jason Dunion |author10=Ryan Ellis |author11=Jose Fernandez-Partagas |author12=Steve Feuer |author13=John Gamache |author14=David Glenn |author15=Andrew Hagen |author16=Lyle Hufstetler |author17=Cary Mock |author18=Charlie Neumann |author19=Ramon Perez Suarez |author20=Ricardo Prieto |author21=Jorge Sanchez-Sesma |author22=Adrian Santiago |author23=Jamese Sims |author24=Donna Thomas |author25=Lenworth Woolcock |author26=Mark Zimmer |date=2012|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=National Oceanic and Atmospheric Administration|accessdate=March 5, 2014|location=Miami, Florida}}</ref> Several ships were caught in rough seas of the East Coast of the United States. The ''Norfolk'' was abandoned in pieces ten miles south of [[Chincoteague, Virginia|Chincoteague]] early on the morning of September 14.<ref name="partagas-1"/> Further south, on September&nbsp;11, the hurricane struck the steamer ''[[SS Central America|Central America]]'' which sprung a leak and eventually sunk on the night of September&nbsp;12 with the loss of 424 passengers and crew.<ref name ="toll">{{cite report|author1=Edward N. Rappaport |author2=Jose Fernandez-Partagas |title=The Deadliest Atlantic Tropical Cyclones, 1492–1996: Cyclones with 25+ deaths|work=National Hurricane Center|publisher=National Oceanic and Atmospheric Administration|year=1996|accessdate=March 5, 2014|url=http://www.nhc.noaa.gov/pastdeadlyapp1.shtml?}}</ref> Also on board the ship were 30,000&nbsp;pounds of gold, the loss of which contributed to the financial [[Panic of 1857]].<ref>{{cite news|url=http://seattle.cbslocal.com/2013/09/17/why-serious-coins-collectors-wont-miss-the-long-beach-expo/|title=Why Serious Coin Collectors Won’t Miss The Long Beach Expo|date=September 17, 2013|newspaper=[[KIRO-TV]]|accessdate=March 5, 2014}}</ref>
{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1857 Atlantic hurricane 3 track.png
|Formed=September 22
|Dissipated=September 26
|1-min winds=70
}}
Based on reports bark ''Aeronaut'' and the schooner ''Alabama'' indicating a severe gale, Partagas and Diaz identified a Category&nbsp;1 hurricane about 405&nbsp;miles (650&nbsp;km) east of [[Charleston, South Carolina]] between September&nbsp;22 and September&nbsp;26.<ref name="partagas-1"/>{{Atlantic hurricane best track}} Sustained wind speeds of 80&nbsp;mph (130&nbsp;km/h) were observed.{{Atlantic hurricane best track}} No evidence was found for a storm track so the hurricane was assigned a stationary position, at latitude 32.5°N, 3.5°W. Among the ships which encountered the hurricane was the brig ''Jerome Knight'', which sprung a leak and sunk on the night of September&nbsp;22.<ref name="partagas-1"/>
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1857 Atlantic hurricane 4 track.png
|Formed=September 24
|Dissipated=September 30
|1-min winds=90
}}
The final tropical cyclone was first observed at 0000&nbsp;UTC on September&nbsp;24, while located about {{convert|420|mi|km}} east of [[Guadeloupe]]. Initially a tropical storm, it strengthened slightly before crossing the [[Leeward Islands]] on September&nbsp;25.{{Atlantic hurricane best track}} In Guadeloupe, several ships at the port in [[Basseterre]] were swept out to sea.<ref name="partagas-1"/> Continuing eastward, the storm soon entered the Caribbean Sea. Early on September&nbsp;26, the system strengthened into a hurricane.{{Atlantic hurricane best track}} By September 28, it was west of the [[Cayman Islands]] and had reached Category&nbsp;2 strength. The storm weakened to a tropical storm after passing [[Cancun]] early on September&nbsp;29 and impacted the Gulf coastline, near the United States&ndash;Mexico border, at that strength the next day before dissipating.{{Atlantic hurricane best track}} At [[Port Isabel, Texas]], several hundred homes were swept away, and several towns near the mouth of the [[Rio Grande]] also sustained damage.<ref name ="rotx">{{cite report|author =David W. Roth|publisher=National Oceanic and Atmospheric Administration; National Weather Service|date=February 4, 2010|title=Texas Hurricane History|work=[[Weather Prediction Center]]|accessdate=March 5, 2014|url=http://www.wpc.ncep.noaa.gov/research/txhur.pdf|format=PDF}}</ref>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
* [[List of tropical cyclones]]
* [[List of Atlantic hurricane seasons]]

==Notes==
{{Reflist|group=nb}}

==References==
{{Reflist}}

{{TC Decades|Year=1850|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1856 Atlantic Hurricane Season}}
[[Category:1850–1859 Atlantic hurricane seasons|*]]
[[Category:Atlantic hurricane seasons]]
[[Category:Articles which contain graphical timelines]]