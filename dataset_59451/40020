[[File:Sopwith F-1 Camel 2 USAF.jpg|right|thumb||[[Sopwith Camel]].Type of [[First World War]] aircraft flown by V. M.    Yeates]]'''Victor Maslin Yeates''' (30 September 1897 — 15 December 1934), often abbreviated to '''V. M. Yeates''', was a [[British people|British]] fighter pilot in [[World War I]]. He wrote ''Winged Victory'', a semi-autobiographical work widely regarded as one of the most realistic and moving accounts of aerial combat and the futility of war.

==Life==
Yeates was born at [[Dulwich]] and educated at [[Colfe's School]], where according to [[Henry Williamson]] he read [[Keats]] under the desk during Maths, explored woods, fields and ponds and kept a tame [[tawny owl]].<ref name="WilliamsonP3">Williamson, Henry "Tribute to V. M. Yeates", in {{cite book |last=Yeates |first=V. M. |title=Winged Victory |publisher=Jonathan Cape |location=London |year=1962 |page=3}}</ref> 

Yeates joined the Inns of Court [[Officer Training Corps]] in 1916 and transferred to the [[Royal Flying Corps]] (later the [[Royal Air Force]]) in May 1917. Yeates married in July 1917, aged only 19 and while still training, to the total disapproval of his parents. <ref>http://www.henrywilliamson.co.uk/bibliography/a-lifes-work/winged-victory</ref> Initially serving with [[No. 46 Squadron RAF|No. 46 Squadron]], to which he was posted in February 1918, by then he had logged twelve hours of dual flying instruction and fifty-three hours solo, of which the last thirteen hours were in a Sopwith Camel. 

The German Spring Offensive began just over a month later, and he flew intensively, many operational flights consisting of highly dangerous ground strafing and bombing. Yeates was posted to 80 Sqn on 9 August from 46 Sqn and joined B Flight, and left the Squadron on 31 August after the move to a new airfield at Allonville. <ref>http://www.rap.airwar1.org.uk/80%20sqn%205.htm</ref> He was invalided back to England suffering from Flying Sickness D (for Debility), brought about by the strain and conditions of constant flying. He was given one month’s sick leave, further extended to 7 November 1918, when he was transferred to TDS Fairlop as an assistant flying instructor, being finally discharged on 23 May 1919. 

Yeates flew 110 operations, 248 hours in [[Sopwith Camel]]s, force landed four times, was shot down by ground fire and scored five victories (two enemy aircraft destroyed,a share in three others and with Capt. DR MacLaren brought down a balloon in flames), thereby achieving "ace" status.<ref name="Atkin">{{cite book |last=Atkin |first=Gordon |title=Winged Victor |publisher=Springwater Books |location=[[Ramsbottom]], Bury |year=2004 |isbn=9780954688103}}</ref>

After the war, Yeates died of [[tuberculosis]], in Fairlight Sanatorium at [[Hastings]] in 1934. He was survived by his wife Norah Phelps Yeates (née Richards) and his four children Mary, Joy Elinor, Guy Maslin  and Rosalind; all of whom had lived with Yeates in a small house in Kent on the Sidcup by-pass of the Dover Road.<ref name="WilliamsonP3"/><ref>Great-grand daughter.</ref>

==''Winged Victory''==
Yeates is now best known for his semi-autobiographical book ''[[Winged Victory (novel)|Winged Victory]]'', which remains well regarded, as an authentic depiction of World War I aerial combat.<ref name="Atkin"/> Yeates's school friend [[Henry Williamson]] contributed a foreword to a republished edition of ''Winged Victory''. There he supported a review in the ''New York Saturday Review of Literature'', that it was "one of the great books of our time."<ref>Williamson, Henry "Tribute to V. M. Yeates", in {{cite book |last=Yeates |first=V. M. |title=Winged Victory |publisher=Jonathan Cape |location=London |year=1962 |page=1}}</ref>

Yeates wrote in the flyleaf of Williamson's copy of ''Winged Victory'' that: "I started [writing the book] in April 1933 in Colindale Hospital. I could not write there, so walked out one morning, the doctor threatening death. I wrote daily till the end of the year. My chief difficulty was to compromise between truth and art, for I was writing a novel that was to be an exact reproduction of the period and an exact analysis and synthesis of a state of mind."<ref>Williamson, Henry "Tribute to V. M. Yeates", in {{cite book |last=Yeates |first=V. M. |title=Winged Victory |publisher=Jonathan Cape |location=London |year=1962 |page=6}}</ref>

===Descriptions of aerial combat===
[[File:Fokker D. VII USAF.jpg|right|thumb|[[Fokker DVII]].Type of [[First World War]] aircraft attacked by VM Yeates.]]''Winged Victory'' is remarkable for its depictions of World War I aerial combat, such as the following excerpt:

{{quote|They flew over the ghastly remains of [[Villers-Bretonneux]] which were still being tortured by bursting shells upspurting in columns of smoke and debris that stood solid for a second and then floated fading away in the wind. All along the line from [[Beaumont-Hamel|Hamel]] to Hangard Wood the whiter puff-balls of [[Shrapnel shell|shrapnel]] were appearing and fading multitudinously and incessantly...A flaming meteor fell out of a cloud close by them and plunged earthwards. It was an aeroplane going down in flames from some fight above the clouds. Where it fell the atmosphere was stained by a thanatognomonic black streak...Tom sitting there in the noise and the hard wind had the citied massy earth his servant tumbler, waiting upon his touch of stick or rudder for its guidance; instantly responding, ready to leap and frisk a lamb-planet amid the steady sun-bound sheep...Some of the cloud peaks thrust up to ten thousand feet; in the blue fields beyond there was an occasional flash of a tilting wing reflecting the sun. Mac climbed as fast as he could. In a few minutes they were at ten thousand, and the Huns, a mile above them, were discernable as aeroplanes, bluely translucent...Rattle of guns and flash of tracers and the Fokker in a vertical turn, red, with extension on the top planes ([[Fokker DVII]]). Tom hated those extensions. He was doing a very splitarse turn for a Hun, but tracers seemed to be finding him. Got him, oh got him: over, flopping over, nose dropping, spinning.<ref>{{cite book |last=Yeates |first=V. M. |title=Winged Victory |publisher=Jonathan Cape |location=London |year=1962 |pages=138, 146, 201, & 410}}</ref>}}

The novel's discursiveness and realism make it one of the most intriguing descriptions of life on the Western Front: the interactions with French residents, the diet of the officer's mess, travel on home leave and recuperation at Army Medical facilities; what officers talked about, the tunes on the gramophone, the food on offer, the narrator's heroic drinking.  Yeates is also interested in the management styles of the series of squadron commanders who pass through and their efforts to make the narrator a more aggressive fighter pilot.  The narrator is tormented by his inability to match the out-and-out warriors and aces who constitute the ideal.  He is deeply honest about the growing stress and debilitation as his friends die one by one and he longs for his tour of duty to reach its end.

===Philosophy about war===
In ''Winged Victory'' Yeates regularly expressed disillusion with the war, with his senior officers and with the causes of [[war]], more typical of the 1930s than of the time he describes: {{quote|"For there's one thing financiers cannot or will not see. They have visions of a frontierless world in which their operations will proceed without hindrance and make all human activities dependent on them; but their [[world state]] is impossible because finance is sterile, and a state living by finance must always have neighbours from which to suck blood, or it is like a dog eating its own tail...an intense war-fever inoculation was carried out by the press. It took rather less than three months, I believe, to make the popular demand for war irresistible...There'll be a famous orgy of money snatching over our bones."<ref>{{cite book |last=Yeates |first=V. M. |title=Winged Victory |publisher=Jonathan Cape |location=London |year=1962 |pages=54–55}}</ref>}}

The novel is occasionally over-written or unduly discursive but contains a realistic portrait of RFC and then RAF life and operations on the Western Front, starting with the launch of [[Operation Michael]], the gigantic German Spring Offensive on 21 March 1918. The narrator and his squadron are ground down by ground attack operations against the German army, as the Allied Armies fight for their lives, while faster scouts (fighters) such as the [[Royal Aircraft Factory S.E.5|S.E.5]] and the [[Bristol F.2 Fighter|Bristol Fighter]] dogfight with German fighters. The Camel role is unglamorous and very dangerous, machine gunning trenches and approach routes at {{convert|100|mph|kph}} a few hundred feet up in un-armoured aircraft, with a constant threat from machine-gun fire from the soldiers beneath them.

==References==
{{reflist}}

==External links==
* {{cite web |url= http://www.theaerodrome.com/aces/england/yeates.php |title=Victor Maslin Yeates |work=The Aerodrome |accessdate=13 December 2015}}

{{wwi-air}}
{{Royal Flying Corps}}

{{Authority control}}

{{DEFAULTSORT:Yeates, Victor Maslin}}
[[Category:1897 births]]
[[Category:1934 deaths]]
[[Category:People from Dulwich]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force officers]]
[[Category:British Army personnel of World War I]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:20th-century deaths from tuberculosis]]
[[Category:People educated at Colfe's School]]
[[Category:Infectious disease deaths in England]]
[[Category:English male novelists]]
[[Category:20th-century English novelists]]