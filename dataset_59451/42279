<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Type 627 Mailplane
  |image = Avro 627.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Light transport
  |manufacturer = [[Avro]]
  |national origin=United Kingdom
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 1931
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = Prototype<!--in most cases, this field is redundant; use it sparingly-->
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 1
  |unit cost = 
  |developed from = [[Avro Antelope]] 
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Avro 627 Mailplane''' was a [[United Kingdom|British]] [[biplane]] developed in 1931 by [[Avro]] from the [[Avro Antelope]] bomber as a [[mail plane]] for use in [[Canada]]. Only one was built which ended up being used as a [[testbed|test bed]].

==Development==
The '''Avro 608 Hawk''' was a proposed two-seater [[fighter aircraft|fighter]] variant of the [[Avro Antelope|Antelope]], which was planned to be powered by a [[Bristol Jupiter]] radial engine. Although construction of a prototype began, it was incomplete when it was redesigned with a 540&nbsp;hp (400&nbsp;kW) [[Armstrong Siddeley Panther]] engine as the '''Avro 622'''.<ref name="jackson avro">{{cite book |last= Jackson|first= A J |title=Avro Aircraft since 1908 |edition= 2nd|year= 1990 |publisher= Putnam Aeronautical Books|location= London |isbn= 0-85177-834-8}}</ref>

Following interest by Canadian Airways, who had a possible requirement for a mail plane, [[Roy Chadwick]] again redesigned the incomplete prototype to the '''Avro 627 Mailplane '''. This was a single-engine, single-bay biplane, powered by a 525&nbsp;hp (391&nbsp;kW) Panther engine and fitted for wheel or float operation. The sole prototype (''G-ABJM'') was certificated on 2 August 1931,<ref name="jackson avro"/> and was shipped to Canada for operational trials. While these trials were successful, the Canadian government cut the subsidy for civil aviation and so Canadian Airways was unable to afford new aircraft, so the mailplane was returned to England.<ref name="jackson avro"/>

The Mailplane was converted in 1933 to a high-speed test bed for the [[Armstrong Siddeley Tiger]] engine, the revised aircraft being known as the '''Avro 654'''.<ref name="jackson avro"/>

==Operational history==
On return from Canada, the Mailplane was entered in the 1932 [[King's Cup Race]], where it recorded the fastest speed of 176&nbsp;mph (283&nbsp;km/h),<ref name="jacksonv1">{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 1|year= 1974|publisher= Putnam|location= London|isbn=0-370-10006-9 }}</ref> although owing to the handicap system, it came 29th.<ref name="jackson avro"/>

After conversion to the Avro 654, the aircraft operational life was short, it being dismantled at [[Woodford Aerodrome]] in 1934.<ref name="jacksonv1"/>

==Variants==
;Avro 608 Hawk: Proposed two-seat fighter, powered by 425&nbsp;hp (317&nbsp;kW) Bristol Jupiter engine. Prototype incomplete.
;Avro 622: Redesigned Avro 608, powered by 540&nbsp;hp (400&nbsp;kW) Armstrong Siddeley Panther II engine. Incomplete prototype converted to Avro 627.
;Avro 627 Mailplane: Single-seat mailplane, powered by 525&nbsp;hp (391&nbsp;kW) Armstrong Siddeley Panther IIA engine. One prototype built.
;Avro 654: Conversion of Avro 627 as high-speed test bed.

==Specifications (Avro 627 Mailplane)==
{{Aircraft specifications
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Avro Aircraft since 1908 <ref name="jackson avro"/> 
|crew=One
|capacity=40 ft³ mail compartment
|length main= 30 ft 10 in
|length alt= 9.40 m
|span main= 36 ft 0 in
|span alt= 10.98 m
|height main= 10 ft 10 in
|height alt= 3.30 m
|area main= 381 ft²
|area alt= 35.4 m²
|airfoil=
|empty weight main= 3,077 lb
|empty weight alt= 1,399 kg
|loaded weight main= 5,150 lb
|loaded weight alt= 2,341 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Armstrong Siddeley Panther]] IIA 
|type of prop=14 cylinder two-row [[radial engine]]
|number of props=1
|power main= 525 hp
|power alt= 391 kW
|power original=
|max speed main= 148 kn
|max speed alt= 170 mph, 274 km/h
|cruise speed main= 128 kn
|cruise speed alt= 147 mph, 237 km/h
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 487 nmi
|range alt= 560 mi, 902 km
|ceiling main= 19,000 ft
|ceiling alt= 5,800 m
|climb rate main= 1,200 ft/min
|climb rate alt= 6.1 m/s
|loading main= 13.5 lb/ft²
|loading alt= 66.1 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.102 hp/lb
|power/mass alt= 0.167 kW/kg
|more performance=
|armament=
|avionics=
}}

==See also==
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=
* [[Avro Antelope]]
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{commons category|Avro 627 Mailplane}}
{{Reflist}}

{{Avro aircraft}}

[[Category:British mailplanes 1930–1939]]
[[Category:Avro aircraft|Mailplane]]
[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]