{{Infobox
|name         = Infobox/doc
|title        = RSA Conference

|image        = [[File:RSA Conference Logo, square.jpg|200px|alt=RSA Conference logo]]
|headerstyle  = background:#ccf;
|labelstyle   = background:#ddf; width:33%
|datastyle    =

|label1  = Founded
|data1   = 1991
|label2  = Locations
|data2   = {{ubl|[[San Francisco, California|San Francisco]]|[[Singapore]]|[[Amsterdam]]|[[Abu Dhabi]]}}
|label3  = Website
|data3   = http://www.rsaconference.com
}}

'''RSA Conference''' is a [[cryptography]] and information security-related [[Academic conference|conference]]. Its flagship event is held annually in [[San Francisco, California|San Francisco]], [[California]], [[United States]]. In 2013, events were also held in [[Singapore]] and [[Amsterdam]].<ref name=RSAConf />

RSA Conference started in 1991 as "Cryptography, Standards & Public Policy", a forum for cryptographers to gather and share the latest knowledge and advancements in the area of Internet security. In 1993 it became an annual event and was renamed the RSA Data Security Conference, then by 2000, simply, the 'RSA Conference'.  The conference is vendor-independent and managed by [[RSA Security|RSA]], the Security Division of [[EMC Corporation|EMC]], with support from leaders in the information security industry.  The multiple day event consists of two entities: the conference, which hosts speakers for a variety of topics, and a vendor expo.  The conference consists of a multitude of presentations on the topic of Internet Security.  Individuals may submit topic proposals to RSA Conference online, which are reviewed and selected to partake in the conference by an independent panel of judges that make up the Program Committee.<ref name="RSAConf">{{cite news|url=http://www.rsaconference.com/
|title=RSA Conference |accessdate=2012-04-25}}</ref>

==Topics==
The conference presentations are organized into themed “tracks,” each covering a specific field in information security.  At the 2012 USA Conference, there were 17 tracks:

*[[Application security]]
*Association Special Topics
*[[Cloud security]]
*[[Cryptography]]
*Data Security
*Governance
*Risk & Compliance
*[[Hacker (computer security)|Hackers]] & Threats
*Hot Topics
*Industry Experts
*Law
*[[Mobile Security]]
*Policy & Government
*Professional Development
*Security Trends
*Sponsor Case Studies
*Strategy & Architecture
*Technology Infrastructure 

This is a large increase in topics from the first European conference in 2000, which had only 5 tracks:<ref name="forrester">{{cite news|url=http://brazil.rsa.com/press_release.aspx?id=148
|title= E-Security Leaders to Gather in Munich for First European RSA Conference|date=2000-03-06|accessdate=2012-04-25}}</ref>

*Cryptographers’ Track
*Developers’ Track
*Implementers’ Track
*New Products Track
*RSA Products Track

The tracks cover different topics for their respective fields with the RSA Conference program committee selecting presentations for each track.  During the conference, multiple tracks will run simultaneously, giving attendees the option of jumping from track to track to sit in on various presentations.

==International expansion==
RSA Conference originally started as an annual event held in the San Francisco Bay Area in California.  Since then, the need for Internet Security has increased, and RSA has added conferences in other countries.  In 2000, RSA had its first European conference, held at the Hilton Munich Park Hotel in Munich, Germany from April 10–13.  In 2002, Japan hosted its first RSA conference.  In 2010, RSA expanded to China, hosting its first conference there from October 21–22 of that year.:<ref name="Kaspersky">{{cite news|url=http://me.kaspersky.com/en/about/news/events/2008/Kaspersky_Labs_Japan_to_be_a_Silver_sponsor_of_RSA_Conference_Japan_2008
|title= Kaspersky Labs Japan to be a Silver Sponsor of RSA Conference Japan 2008|date=2008-04-11|accessdate=2012-04-25}}</ref>

==RSA Expo==
[[File:2010 RSA Conference - Security Expo.jpg|thumb|Security Expo at the 2010 RSA Conference]]
	The expo at the conference allows for many companies involved in the network security field to showcase their products.  Vendors gather in a designated area and set up booths to present and/or demo products and ideas they have been working on.  Past expos have included companies such as [[Microsoft]], [[Safenet]], [[Barracuda Networks]], [[ESET]], [[McAfee]], [[Verizon]], [[Akamai Technologies]], [[Websense]], [[Intel]], [[Cisco]], and [[Hewlett-Packard]].<ref name="RSAConference2012">{{cite news|url=http://rsaconference.com/index.htm
|title= RSA Conference 2012 |accessdate=2012-04-25}}</ref>

==Attendance==
With the widespread use of security and the necessity of information security, attendees hail from all different types of industries including Banking, Computer Software Development, Finance, Government, Healthcare, Manufacturing, and Pharmaceuticals.  All levels of security professionals can be found in attendance, from C-Level executives to low level IT staff.
Individuals above the age of 18 are allowed to purchase one of three passes: A full conference pass, a one-day pass, and an expo only pass.  Regular attendees are allowed to be a part of the Members Circle program, which gives special perks such as discounted prices and priority seating.:<ref name="RSAConference2012"/>

Finnish [[F-Secure]] researcher [[Mikko Hyppönen]] cancelled his planned speech at RSA Conference 2014 because of the "NSA backdoor" that was revealed in December 2013,<ref>{{cite web|url=http://www.f-secure.com/weblog/archives/00002651.html|title=An Open Letter to the Chiefs of EMC and RSA}}</ref> and so have several others.<ref>{{cite web|url=http://news.cnet.com/8301-1009_3-57616842-83/rsa-conference-speakers-begin-to-bail-thanks-to-nsa/|title=C-net news}}</ref> The boycott of the conference, because of the company's NSA ties, is growing among security experts - although in the event only 12 speakers pulled out of the 2014 Conference out of a total of some 500.<ref>[http://www.washingtonpost.com/blogs/the-switch/wp/2014/01/07/at-least-six-security-experts-boycott-prominent-security-conference-over-nsa-ties/ At least eight security experts boycott prominent security conference over NSA ties]</ref><ref>[http://rt.com/usa/researchers-boycott-rsa-conference-nsa-299/ Growing number of security experts boycott RSA conference for NSA ties — RT USA]</ref><ref>[http://arstechnica.com/security/2014/01/more-researchers-join-rsa-conference-boycott-to-protest-10-million-nsa-deal/ More researchers join RSA conference boycott to protest $10 million NSA deal | Ars Technica]</ref><ref>[http://www.securitycurrent.com/en/news/ac_news/growing-boycott-of-rsa-conference Growing Boycott of RSA Conference 2014]</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Rsa Conference}}
[[Category:Cryptography conferences]]
[[Category:Computer security conferences]]