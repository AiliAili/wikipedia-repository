{{Infobox NFL biography
| name                = Deon King
| image               = 
| image_size          = 
| alt                 = 
| caption             = 
| current_team        = Indianapolis Colts
| number              = 50
| position            = [[Linebacker]]
| birth_date          = {{birth date and age|1993|07|02}}
| birth_place         = [[Reston, Virginia]]
| death_date          = 
| death_place         = 
| height_ft           = 6
| height_in           = 0
| weight_lbs          = 225
| high_school         = [[South Lakes High School]]
| college             = [[Norfolk State Spartans football|Norfolk State]]
| undraftedyear       = 2016
| pastteams           = 
* [[Dallas Cowboys]] ({{NFL Year|2016}})*
* [[San Diego Chargers]] ({{NFL Year|2016}})
* [[Indianapolis Colts]] ({{NFL Year|2016}}–present)
| pastteamsnote       = yes
| status              = Active
| highlights          = 
* [[Buck Buchanan Award]] (2015)
| statseason          = 2016
| statweek            = 
| statlabel1          = [[Tackle (football move)|Tackles]]
| statvalue1          = 2
| statlabel2          = [[Quarterback sack|Sacks]]
| statvalue2          = 0.0
| statlabel3          = [[Interception]]s
| statvalue3          = 0
| statlabel4          = [[Fumble|Forced fumbles]]
| statvalue4          = 0
| nflnew              = deonking/2556575
}}

'''Deon King''' (born July 2, 1993) is an [[American football]] [[linebacker]] for the [[Indianapolis Colts]] of the [[National Football League]] (NFL). He played [[college football]] at [[Norfolk State Spartans football|Norfolk State]].

==Early years==
King attended [[South Lakes High School]] in [[Reston, Virginia]], where he was a member of both the football and track & field teams.<ref name="King NSU_Bio">{{cite web|title=Deon King Bio|url=http://www.nsuspartans.com/roster.aspx?rp_id=2554|website=nsuspartans.com|publisher=NSU Athletics|accessdate=11 January 2016}}</ref>

==College career==

===Freshman year===
In 2012, King transferred from [[Fork Union Military Academy]] to [[Norfolk State Spartans football|Norfolk State]]. Joining the Spartans as a true freshman, he played in all 11 games that season as an outside linebacker and special teams.<ref name="Deon King_Bio">{{cite web|title=Deon King Bio|url=http://www.nsuspartans.com/roster.aspx?rp_id=2554|website=nsuspartans.com|publisher=Norfolk State University Athletics|accessdate=17 February 2016}}</ref> In his debut game against in-state rival Virginia State, King put up a season high 5 tackles, with 3 of them being for loss and 2 sacks. Of those 2 sacks, one was on the final play of the game giving Norfolk State their first shutout since 2001.<ref>{{cite web|last1=Michalec|first1=Matt|title=Spartans Post 1st Shutout Since 2001, 24-0 over Virginia State|url=http://www.nsuspartans.com/news/2012/9/1/FB_0901124758.aspx?path=football|website=nsuspartans.com|publisher=NSU Sports Information|accessdate=11 January 2016}}</ref> In the 2012 season finale against conference member [[Morgan State Bears football|Morgan State]], King earned 2 tackles and his career first interception, which he returned for 15 yards for a touchdown. His performance during the game earned him MEAC Rookie of the Week honors and topped off his freshman season with 19 tackles, with 12 of them being solo efforts.<ref name="King NSU_Bio"/><ref>{{cite web|last1=Matt|first1=Michalec|title=Lambert, King Garner Weekly MEAC Football Honors|url=http://www.nsuspartans.com/news/2012/11/12/FB_1112125631.aspx?path=football|website=nsuspartans.com|publisher=NSU Sports Information|accessdate=11 January 2016}}</ref>

===Sophomore year===
In the 2013 season opener against Maine, King had five tackles, one of them a quarterback sack. The following week against [[Rutgers Scarlet Knights football|Rutgers]] he earned 9.5 sacks against the scarlet knights. For the next 5 games, King would make at least 8 tackles.<ref name="Deon King_Bio"/> During the annual meeting between [[Battle of the Bay (Hampton–Norfolk State)|rival]] [[Hampton Pirates football|Hampton]], King would make a season high 13 tackles, with 10 of them being solo efforts.<ref>{{cite web|last1=Michalec|first1=Matt|title=Hampton Pulls Away Late for 27-17 Win over NSU in State Farm Battle of the Bay|url=http://www.nsuspartans.com/news/2013/10/19/FB_1019133006.aspx?path=football|website=nsuspartans.com|publisher=Norfolk State University Athletics|accessdate=18 February 2016}}</ref> King ended the season earning All-MEAC second team honors and spots on both Norfolk’s Athletics Director’s Honor Roll and the MEAC Commissioner’s All-Academic Team.<ref name="Deon King_Bio"/> He also ranked second in the conference for both tackles for loss and sacks, with 18.5 tackles and 9.5 sacks. King’s performance also gained national recognition by ranking 8th in FCS in tackles for loss per game with an average of 1.5 and 16th in sacks with an average of 0.79 per game. Kings sacking average would tie him for the third-most in a single season since the program’s transition to [[NCAA Division I|Division-I]] in 1997.<ref name="Deon King_Bio"/>

===Junior year===
During his junior season, King started as an outside linebacker for all 12 of the Spartan’s games that season. During the season opener against Maine, King would make 9 tackles and 1 sack. King would also have a strong showing against Morgan State where he earned 7 tackles, 3 quarterback hurries, broke up 1 pass and forced 1 fumble.<ref name="Deon King_Bio"/> In the annual rivalry game against Hampton, King was able to rack up 15 stops helping the Spartans earn a victory. King would prove to be an asset to the  Spartans in their meeting with Florida A&M as he completed eight tackles, forced a fumble, and recovered a fumble.<ref name="Deon King_Bio"/> The following week, against Bethune-Cookman, King earned a season-high 19 tackles, 1 interception, and forced a fourth-quarter fumble, which the Spartans were able to recover.<ref>{{cite web|last1=Michalec|first1=Matt|title=Wildcats Rally Past NSU 13-7 in 1st-Place Showdown|url=http://www.nsuspartans.com/news/2014/11/6/FB_1106143331.aspx?path=football|website=nsuspartans.com|publisher=NSU Athletics|accessdate=18 February 2016}}</ref> King’s performance during the game tied him at the top of the MEAC for the most tackles during the season.
In the season finale against South Carolina State, King would set a career high 9 tackles, one of them a sack.
King’s season would end with a total 106 tackles, with 46 of them being solo efforts and 60 assists. King also received national recognition when he ranked in the top 50 in fumble recoveries, tackles for loss, total tackles, and sacks.<ref name="Deon King_Bio"/> He was also named the team’s Defensive MVP and awarded a spot on the 2014 MEAC first team.<ref name="King STATS_Bio">{{cite web|title=Deon King|url=http://www.fcs.football/cfb/players.asp?id=220661|website=STATS FCS Football|publisher=Stats FCS|accessdate=11 January 2016}}</ref>

===Senior year===
In King’s senior season, he was named to the preseason All-MEAC first team and opened the season with 12 tackles and 1 pass breakup against Rutgers.<ref name="Deon King_Bio"/><ref>{{cite web|title=MEAC Announces 2015 Preseason All-MEAC Awards|url=http://www.meacsports.com/ViewArticle.dbml?ATCLID=210243015|website=Mid-Eastern Athletic Conference|publisher=MEACMedia Relations|accessdate=18 February 2016}}</ref> In the following weeks, against FBS opponents Old Dominion and Marshall, King would have back to back 21 tackle games.<ref name="Deon King_Bio"/> He followed up that performance the following week with a total 10 tackles half a tackle for loss and 1 interception in a victory against Hampton at the Battle of the Bay rivalry game.<ref>{{cite web|last1=Michalec|first1=Matt|title=Strong Second-Half Surge Lifts NSU over Hampton, 24-14|url=http://www.nsuspartans.com/news/2015/9/26/FB_0926150748.aspx?path=football|website=nsuspartans.com|publisher=NSU Athletics|accessdate=18 February 2016}}</ref> His performance during that game would earn him his first MEAC Defensive Player of the Week award.<ref name="Deon King_Bio"/>
Later in the season King posted 15 tackles and 1.5 tackles for loss against [[North Carolina A&T Aggies football|North Carolina A&T]]; 15 tackles against Bethune-Cookman and 14 tackles, with 9 solos, against North Carolina Central.<ref name="Deon King_Bio"/> The following weeks, against Savannah State and South Carolina State, King would earn back to back MEAC Defensive Player of the Week honors for his performance. King posted 17 tackles and one tackle for loss against Savannah State and 18 tackles; including 12 solo efforts, 3 Tackles for loss and 2 sacks against SC State.<ref name="Deon King_Bio"/> King also earned other postseason honors such as being named a Boxtorow HBCU All American;<ref>{{cite news|title=NSU linebacker King receives national honor|url=http://pilotonline.com/sports/nsu-linebacker-king-receives-national-honor/article_f2b938ac-dfce-5f89-a35a-7eb322302a6e.html|accessdate=30 August 2016|work=The Virginian-Pilot|date=9 Dec 2015}}</ref> STATS, AP FCS All-American;<ref>{{cite web|last1=Michalec|first1=Matt|title=King Selected to STATS, AP FCS All-America 1st Teams|url=http://www.nsuspartans.com/news/2015/12/15/FB_1215154513.aspx|website=nsuspartans.com|publisher=NSU Athletics|accessdate=18 February 2016}}</ref><ref>{{cite news|last1=Hall|first1=David|title=NSU linebacker Deon King named to AP team|url=http://pilotonline.com/sports/college/norfolk-state/football/nsu-linebacker-deon-king-named-to-ap-team/article_4c2ad89b-b99e-5ce3-ad03-3effe35f7011.html|accessdate=30 August 2016|work=The Virginian-Pilot|date=16 Dec 2015}}</ref> AFCA and SBN Sports Black College All-American.<ref name="Hall 21_Dec15">{{cite news|last1=Hall|first1=David|title=Linebacker King is Norfolk State’s 1st consensus FCS All-American|url=http://pilotonline.com/sports/college/norfolk-state/football/linebacker-king-is-norfolk-state-s-st-consensus-fcs-all/article_ed21bbc1-328c-5695-989a-05dbcca62afc.html|accessdate=30 August 2016|work=The Virginian-Pilot|date=21 Dec 2015}}</ref><ref>{{cite web|title=King Named to 2nd HBCU All-America Team|url=http://www.nsuspartans.com/news/2016/1/22/FB_0122162130.aspx|website=nsuspartans.com|publisher=NSU Sports Information|accessdate=18 February 2016}}</ref> King was also awarded the [[Buck Buchanan Award]] as the top defensive player in FCS for 2015.<ref>{{cite news|last1=Hall|first1=David|title=Norfolk State’s King wins Buchanan Award|url=http://pilotonline.com/sports/college/norfolk-state/football/norfolk-state-s-king-wins-buchanan-award/article_dd911ba2-1614-5270-aa69-223afac1168c.html|accessdate=30 August 2016|work=The Virginian-Pilot|date=22 Dec 2015}}</ref>

==Professional career==
===Dallas Cowboys===
King was signed by the [[Dallas Cowboys]] as an undrafted free agent in 2016.<ref>{{cite news|last1=Hall|first1=David|title=Norfolk State’s Deon King agrees to deal with Dallas Cowboys|url=http://pilotonline.com/sports/college/norfolk-state/football/norfolk-state-s-deon-king-agrees-to-deal-with-dallas/article_4a57d209-18c9-5431-96e0-0ffef8c3b0b3.html|accessdate=2 September 2016|work=The Virginian-Pilot|date=1 May 2016}}</ref> On September 3, 2016, he was released by the Cowboys as part of final roster cuts and was signed to the practice squad the next day, then released three days later on September 6, 2016.

===San Diego Chargers===
On October 4, 2016, King was signed to the [[San Diego Chargers]]' practice squad.<ref>{{cite web|title=Bolts Add Trio to Practice Squad
|url=http://www.chargers.com/news/2016/10/04/bolts-add-trio-practice-squad|author=Henne, Ricky|website=Chargers.com|date=October 4, 2016}}</ref> He was promoted to the active roster on November 5, 2016.<ref>{{cite web|title=Chargers Make Four Roster Moves; Place Dexter McCluster on NFL-I List|url=http://www.chargers.com/news/2016/11/05/chargers-make-four-roster-moves-place-dexter-mccluster-nfl-i-list|author=Henne, Ricky|website=Chargers.com|date=November 5, 2016}}</ref> He was released on November 14, 2016.

===Indianapolis Colts===
King was claimed off waivers by the Colts on November 15, 2016.<ref>{{cite web|title=Indianapolis Colts Make Roster Moves|url=http://blogs.colts.com/2016/11/15/indianapolis-colts-make-roster-moves-173/|website=Blogs.Colts.com|date=November 15, 2016}}</ref>

==References==
{{reflist|30em}}

==External links==
*[http://www.dallascowboys.com/team/players/roster/deon-king  Dallas Cowboys bio]
*[http://www.nsuspartans.com/roster.aspx?rp_id=2554  Norfolk State Spartan bio]

{{Buck Buchanan Award |state=autocollapse}}
{{Indianapolis Colts roster navbox}}

{{DEFAULTSORT:King, Deon}}
[[Category:Living people]]
[[Category:1993 births]]
[[Category:Players of American football from Virginia]]
[[Category:Norfolk State Spartans football players]]
[[Category:Dallas Cowboys players]]
[[Category:San Diego Chargers players]]
[[Category:Indianapolis Colts players]]
[[Category:Undrafted National Football League players]]
[[Category:People from Reston, Virginia]]