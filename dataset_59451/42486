<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Freedom 40
 | image=Brutsche_Freedom_40.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Brutsche Aircraft Corporation]]
 | designer=Neal H. Brutsche
 | first flight=
 | introduced=
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=Probably just one prototype
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]3,000 (built from plans, less engine, instruments and propeller, 1998)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Brutsche Freedom 40''' was an [[United States|American]] [[homebuilt aircraft]] that was designed by Neal H. Brutsche and produced by [[Brutsche Aircraft Corporation]] of [[Salt Lake City, Utah]]. The aircraft was intended to be supplied in the form of plans for amateur construction, with a partial kit available.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', pages 64 and 129. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

==Design and development==
The Freedom 40 featured a [[strut-braced]] [[high-wing]], a single-seat enclosed cockpit with a door, fixed [[conventional landing gear]] and a single engine in [[tractor configuration]].<ref name="Aerocrafter" />

The aircraft was made from [[pop rivet]]ted sheet [[aluminum]]. Its {{convert|28.00|ft|m|1|abbr=on}} span wing had a wing area of {{convert|112.0|sqft|m2|abbr=on}} and could be folded for ground transport or storage. The cabin width was {{convert|28|in|cm|abbr=on}}. The acceptable power range was {{convert|28|to|42|hp|kW|0|abbr=on}} and the standard engine used was the {{convert|40|hp|kW|0|abbr=on}} [[Hirth 2702]] [[two-stroke]] powerplant.<ref name="Aerocrafter" />

The Freedom 40 had a typical empty weight of {{convert|330|lb|kg|abbr=on}} and a gross weight of {{convert|600|lb|kg|abbr=on}}, giving a useful load of {{convert|270|lb|kg|abbr=on}}. With full fuel of {{convert|10|u.s.gal}} in the aircraft's wing tanks the payload for the pilot and baggage was {{convert|210|lb|kg|abbr=on}}.<ref name="Aerocrafter" />

To simplify construction, the design had no complex parts to make and no compound curves to form. In 1998 the plans sold for [[US$]]250.00. Completion cost for the [[airframe]] alone was estimated at US$3000.00 in 1998.<ref name="Aerocrafter" />

==Operational history==
In December 2013 there were no examples [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]] and it is unlikely that any exist any more.<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=FREEDOM+40&PageNo=1|title = Make / Model Inquiry Results|accessdate = 18 December 2013|last = [[Federal Aviation Administration]]|date = 18 December 2013}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Freedom 40) ==
{{Aircraft specs
|ref=AeroCrafter<ref name="Aerocrafter" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=18
|length in=0
|length note=
|span m=
|span ft=28
|span in=0
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=112
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=330
|empty weight note=
|gross weight kg=
|gross weight lb=600
|gross weight note=
|fuel capacity={{convert|10|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hirth 2702]]
|eng1 type=two cylinder, air-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=
|eng1 hp=40

|prop blade number=2
|prop name=wooden
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=87
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=30
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=18500
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=1040
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=5.4
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
*[http://www.homebuiltairplanes.com/forums/attachments/aircraft-design-aerodynamics-new-technology/5234d1261269204-hybrid-construction-freedom28.jpg Photo of a Freedom 40]
{{Brutsche aircraft}}

[[Category:Brutsche aircraft|Freedom 40]]
[[Category:United States sport aircraft 1990–1999]]
[[Category:United States ultralight aircraft 1990–1999]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Homebuilt aircraft]]