{{Other ships|USS Blakely}}
{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:USS Blakeley (DD-150).jpg|300px|USS Blakeley (DD-150)]]
|Ship caption=USS ''Blakeley'' in September 1942, after modernization
}}
{{Infobox ship career
|Hide header=
|Ship country=United States
|Ship flag={{USN flag|1945}}
|Ship name=
|Ship namesake=[[Johnston Blakeley]]
|Ship ordered=
|Ship builder=[[William Cramp and Sons]]
|Ship original cost= $1,448,367.50 (hull & machinery)<ref>{{cite journal |url= https://books.google.com/books?id=mZEqAAAAYAAJ&pg=PA762#v=onepage&q&f=false |title= Table 21 - Ships on Navy List June 30, 1919 |journal= Congressional Serial Set |publisher= U.S. Government Printing Office |year= 1921 |page= 762 }}</ref> 
|Ship laid down=26 March 1918
|Ship launched=19 September 1918
|Ship acquired=
|Ship commissioned=8 May 1919
|Ship decommissioned=21 July 1945
|Ship in service=
|Ship out of service=
|Ship struck=13 August 1945
|Ship reinstated=
|Ship honours=
|Ship fate=Sold for scrapping, 30 November 1945
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=[[Wickes class destroyer|''Wickes''-class destroyer]]
|Ship displacement=1,154&nbsp;tons
|Ship length=314&nbsp;ft 5&nbsp;in (95.83&nbsp;m)
|Ship beam=31&nbsp;ft 8&nbsp;in (9.65&nbsp;m)
|Ship draft=9&nbsp;ft (2.74&nbsp;m)
|Ship propulsion=
|Ship speed=35&nbsp;knots (65 km/h)
|Ship range=
|Ship complement=122 officers and enlisted
|Ship sensors=
|Ship EW=
|Ship armament=*4 × 4" (102&nbsp;mm)
*2 × 3" (76&nbsp;mm)
*12 × 21" (533&nbsp;mm) torpedo tubes
|Ship armor=
|Ship aircraft=
|Ship aircraft facilities=
|Ship notes=
}}
|}

The second '''USS ''Blakeley'' (DD–150)''' was a [[Wickes class destroyer|''Wickes''-class]] [[destroyer]] in the [[United States Navy]], named for Captain [[Johnston Blakeley]].

Built in 1918, she saw patrol duty along the [[East Coast of the United States]] during the [[interwar era]]. Decommissioned for several years, she returned to duty at the outset of [[World War II]]. She spent much of the war on convoy patrol duty in the [[Caribbean]]. On 25 May 1942, while on patrol, she was struck by a torpedo fired by [[Nazi Germany|German]] [[submarine]] {{GS|U-156|1941|2}}, which blew off her forward {{convert|60|ft}}. Fitted with temporary measures, she steamed to [[Philadelphia Naval Yard]] where she was fitted with the forward section of sister ship {{USS|Taylor|DD-94|6}}. She spent much of the rest of the war on convoy patrol duty before being sold for scrap in 1945.

== Design and construction ==
{{main|Wickes-class destroyer}}

''Blakely'' was one of 111 [[Wickes-class destroyer|''Wickes''-class destroyer]]s built by the [[United States Navy]] between 1917 and 1919. She, along with 20 of her sisters, were constructed at [[William Cramp and Sons]] shipyards in [[Philadelphia]] using specifications and detail designs drawn up by [[Bath Iron Works]].{{sfn|Gardiner|Gray|1984|p=124}}{{sfn|Friedman|2003|p=40}}

She had a [[standard displacement]] of {{convert|1,154|t}} an [[length overall|overall length]] of {{convert|314|ft|5|in}}, a [[beam (nautical)|beam]] of {{convert|31|ft|8|in}} and a [[draft (hull)|draught]] of {{convert|9|ft}}. On trials, ''Blakeley'' reached a speed of {{convert|35|knots}}. She was armed with four [[4"/50 caliber gun]]s, two [[3"/23 caliber gun]], and twelve [[Mark 15 torpedo|21-inch torpedo tube]]s. She had a regular crew complement of 122 officers and enlisted men.{{sfn|DANFS|1991|p=301}} She was driven by two [[Curtis steam turbine]]s powered by four [[Yarrow boiler]]s.{{sfn|Gardiner|Gray|1984|p=124}}

Specifics on ''Blakeley'''s performance are not known, but she was one of the group of ''Wickes''-class destroyers known unofficially as the 'Liberty Type' to differentiate them from the destroyers constructed from detail designs drawn up by [[Bethlehem Steel]], which used Parsons or Westinghouse turbines. The 'Liberty' type destroyers deteriorated badly in service, and in 1929 all 60 of this group were retired by the Navy. Actual performance of these ships was far below intended specifications especially in [[fuel efficiency|fuel economy]], with most only able to make {{convert|2,300|nmi}} at {{convert|15|knots}} instead of the design standard of {{convert|3,100|nmi}} at {{convert|20|knots}}.{{sfn|Gardiner|Gray|1984|p=124}}{{sfn|Friedman|2003|p=41}} The class also suffered problems with turning and weight.{{sfn|Friedman|2003|p=46}}

She was the second ship to be named for [[Johnston Blakeley]], the [[USS Blakely (TB-27)|first]] was a [[torpedo boat]] commissioned in 1904. A subsequent [[USS Blakely (FF-1072)|USS ''Blakely'']] would be commissioned, this one a [[Knox-class frigate|''Knox''-class frigate]]. This third ship would also be named for [[Charles Adams Blakely]].{{sfn|DANFS|1991|p=301}}

==History==
''Blakeley'' was launched 19 September 1918 by William Cramp and Sons Ship and Engine Building Company in Philadelphia and [[ship sponsor|sponsored]] by the wife of Charles Adams Blakeley. The ship was [[ship commissioning|commissioned]] 8 May 1919, under the command of Commander [[W. Brown, Jr.]]. She immediately joined the [[U.S. Atlantic Fleet|Atlantic Fleet]]. Blakely patrolled along the [[East Coast of the United States]] until she was decommissioned on 29 June 1922, and returned to Philadelphia. She was recommissioned from 1932 to 1937 to serve with the [[Scouting Fleet]], and then was again decommissioned in Philadelphia.{{sfn|DANFS|1991|p=301}} Low military budgets were the cause of these periods of inactivity, as the Navy did not have the funds or manpower to maintain a number of ships, including ''Blakeley''.{{sfn|Bonner|1996|p=10}}

''Blakeley'' was again commissioned on 16 October 1939. She then joined the [[Neutrality Patrol]] until the [[Empire of Japan|Japanese]] [[Battle of Pearl Harbor|attack on Pearl Harbor]] and the U.S. entrance into [[World War II]]. She then began convoy duty in the [[Caribbean]], including a February 1942 mission to guard a convoy carrying troops to garrison [[Curaçao]] in the [[Netherlands West Indies]].{{sfn|DANFS|1991|p=301}}

[[File:USS Blakeley ohne Bug.jpg|thumb|left|The heavily damaged USS ''Blakeley'' after the attack by ''U-156'']]
On 25 May 1942, ''Blakeley'' was on a patrol off [[Martinique]], inspecting all incoming ships for evidence of activities by [[Vichy French]] collaborators alongside her sister {{USS|Ellis|DD-154|6}}.{{sfn|Marley|2008|p=1013}} At 08:30&nbsp;a.m., she altered course to pursue a sound ping on her [[sonar]]. Nothing was found at the site of the ping, and the crew assumed it was caused by a school of [[black sea bass|blackfish]]. As the ship turned to resume its course, it was struck by a torpedo fired by the unnoticed [[Nazi Germany|German]] [[submarine]] {{GS|U-156|1941|2}} under the command of [[Werner Hartenstein]]. The torpedo struck between frames 18 and 24 at about {{convert|4|ft}} below her water line. The force of the impact blew off {{convert|60|ft}} of her forward [[bow (ship)|bow]] and [[forecastle]]. After several minutes, the crew determined they could still operate the ship, and it was brought back under control and sailed for [[Fort-de-France]]. The ship was steered with a combination of rudder and varying shaft speeds, and four hours after the attack, she was moored in Fort-de-France. Six men died and twenty one were wounded during the attack.{{sfn|Bonner|1996|p=11}} Hartstein radioed a U-boat headquarters in [[Lorient]] requesting permission to finish ''Blakeley'' off, but permission was denied. Destroyers {{USS|Breckinridge|DD-148|2}}, {{USS|Greer|DD-145|2}}, {{USS|Tarbell|DD-142|2}} and two [[Consolidated PBY Catalina|PBY Catalina]] planes from [[VP-53]] were scrambled to assist the stricken ''Blakeley''.{{sfn|Marley|2008|p=1013}}

At Fort-de-France, she was fitted with a wooden [[bulkhead (partition)|bulkhead]] to cover the area blown off by the torpedo, and an [[anchor]] was improvised out of a truck's [[axle]] and [[differential housing]]. She then sailed under her own power to [[San Juan, Puerto Rico]] where a steel stub bow was attached. From there, the steamed for [[Philadelphia Naval Yard]] for permanent repairs. During mid-1942, ''Blakeley'' was fitted with the forward section of her decommissioned sister ship, {{USS|Taylor|DD-94|2}}.{{sfn|Friedman|2003|p=62}} She was also fitted with newer weapons and electronics systems, such as updated radar. Repairs were completed in September 1942 and she resumed her convoy duties in the Caribbean.{{sfn|Bonner|1996|p=11}}

''Blakeley'' spent most of the rest of the war on convoy escort duty in the [[Caribbean Sea Frontier]], except for two short deployments in the [[Atlantic Ocean]].{{sfn|Bonner|1996|p=11}} On 1 January to 23 February 1943 she was assigned to [[Hunter-killer Group|hunter-killer]] duty with Task Group 21.13 in the [[North Atlantic]], and from 24 March to 11 May 1943, she escorted a convoy to [[Bizerte, Tunisia]].{{sfn|DANFS|1991|p=301}} From 18 March to 13 June 1945,{{sfn|DANFS|1991|p=301}} she was stationed in [[New London, Connecticut]], training U.S. submarines in [[Long Island Sound]] to avoid destroyers.{{sfn|Bonner|1996|p=11}}

Following this duty, ''Blakeley'' was decommissioned at Philadelphia Naval Yard on 21 July 1945 and sold for scrap on 30 November 1945. She received one [[battle star]] for her wartime convoy duty.{{sfn|DANFS|1991|p=301}}

==See also==
*[[List of United States Navy destroyers]]

==References==

=== Notes ===
{{Reflist|2}}

=== Sources ===
*{{DANFS|http://www.history.navy.mil/research/histories/ship-histories/danfs/b/blakeley-ii.html}}
* {{citation|title=Dictionary of American naval fighting ships / Vol.1, Historical sketches : letters A through B |publisher=[[Department of the Navy]] |url=http://www.history.navy.mil/danfs/ |location=[[Washington, D.C.]] |year=1991 |oclc=551573855 |ref= {{harvid|DANFS|1991}} }}
* {{citation|last=Bonner |first=Kermit H. |title=Final Voyages |publisher=[[Turner Publishing Company]] |location=[[Paducah, Kentucky]] |isbn= 978-1-56311-289-8 |year=1996}}
*{{citation|last=Friedman |first=Norman |title=United States Destroyers: An Illustrated Design History |publisher=[[Naval Institute Press]] |location=[[Annapolis, Maryland]] |year=2003 |isbn=978-1-55750-442-5}}
*{{citation|last1=Gardiner |first1=Robert |last2=Gray |first2=Randal |title=Conway's All the World's Fighting Ships: 1906–1921, Volume 2 |publisher=[[Naval Institute Press]] |location=[[Annapolis, Maryland]] |year=1984 |isbn=978-0-87021-907-8}}
*{{citation|last=Marley |first=David |title=Wars of the Americas: A Chronology of Armed Conflict in the Western Hemisphere |publisher=[[ABC-CLIO]] |location=[[Santa Barbara, California]] |isbn=978-1-59884-100-8 |year=2008}}

==External links==
* [http://www.navsource.org/archives/05/150.htm NavSource Photos]

{{Wickes class destroyer}}

{{DEFAULTSORT:Blakeley (Dd-150)}}
[[Category:Wickes-class destroyers]]
[[Category:World War II destroyers of the United States]]
[[Category:Ships built in Philadelphia]]
[[Category:1918 ships]]