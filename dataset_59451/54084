{{Use dmy dates|date=October 2012}}
{{Infobox Australian place| type = suburb
| name     = Blackwood
| city     = Adelaide
| state    = sa
| image    = Blackwoodwarmemorial.jpg
| caption  = Blackwood Soldiers' Memorial
| lga      = City of Mitcham
| postcode = 5051
| pop            = 4,053
| pop_year = {{CensusAU|2011}}
| pop_footnotes  = <ref name=Census2011>{{Census 2011 AUS | id = SSC40051 | name = Blackwood (State Suburb)|quick = on | accessdate=16 February 2015}}</ref>
| pop2           = 3,800
| pop2_year = {{CensusAU|2006}}
| pop2_footnotes = <ref name=Census2006>{{Census 2006 AUS | id=SSC41141 | name=Blackwood (State Suburb) | quick=on |accessdate=19 July 2010}}</ref>
| est      = 1879
| area     = 
| stategov = [[Electoral district of Davenport|Davenport]]
| fedgov   = [[Division of Boothby|Boothby]]
| near-nw  = [[Panorama, South Australia|Panorama]]
| near-n   = [[Belair, South Australia|Belair]]
| near-ne  = [[Glenalta, South Australia|Glenalta]]
| near-w   = [[Eden Hills, South Australia|Eden Hills]]
| near-e   = [[Hawthorndene, South Australia|Hawthorndene]]
| near-sw  = [[Bellevue Heights, South Australia|Bellevue Heights]]
| near-s   = [[Craigburn Farm, South Australia|Craigburn Farm]]
| near-se  = [[Coromandel Valley, South Australia|Coromandel Valley]]
| dist1    = 
| location1= [[Adelaide]]
}}

'''Blackwood''' is a south eastern [[suburbs and localities (Australia)|suburb]] located in the foothills of [[Adelaide, South Australia]]. It is part of the local government area of the [[City of Mitcham]].

==History==
The first use of the name "Blackwood" appears to have been before 1880, in reference  to the "Blackwood Inn", a small hotel located where the [[Belair, South Australia|Belair]] Hotel now stands. It was probably derived from the [[Acacia melanoxylon|Australian Blackwood tree]] which grows in the area.<ref>{{cite web|publisher=State Library, South Australia|accessdate=9 July 2008|title=The Manning Index of South Australian History - Blackwood|url=http://www.slsa.sa.gov.au/manning/pn/b/b17.htm}}</ref>

Land was first subdivided for housing in 1879, a [[Methodist Church of Australasia|Methodist church]] was built in 1881, and the [[Blackwood railway station]] opened in 1883.<ref>{{cite web|publisher=Blackwood Business Network|accessdate=12 July 2008|title=History of Blackwood|url= http://www.blackwoodbiz.com.au/index2.php?option=com_content&do_pdf=1&id=17 |archiveurl = https://web.archive.org/web/20080719035535/http://blackwoodbiz.com.au/index2.php?option=com_content&do_pdf=1&id=17 |archivedate = 19 July 2008}}</ref>
''Belair'' Post Office opened on 3 April 1859 and was renamed ''Blackwood'' in 1881, when the ''Belair'' office was moved some distance away.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Places of interest==
Blackwood Soldiers' Memorial was erected in 1921 in the most prominent place in Blackwood: the five-ways roundabout. It is a place of Local Heritage, listed for being "a notable landmark in the area".{{Citation needed|date=February 2017}}

[[Wittunga Botanic Garden]], originally a private property with English gardens established in 1901, is located in Blackwood and features displays of Australian and South African plants.

[[Blackwood High School]] and Blackwood Primary School are located nearby in [[Eden Hills, South Australia|Eden Hills]].

[[Uniting Church in Australia|Uniting]] (former [[Methodist Church of Australasia|Methodist]]), [[Anglican Church of Australia|Anglican]], [[Churches of Christ in Australia|Church of Christ]], [[Baptist Union of Australia|Baptist]], [[Roman Catholic Church in Australia|Roman Catholic]] and [[Lutheran Church of Australia|Lutheran]] churches are located in Blackwood, as well as [[Pentecostalism|Pentecostal]] churches including the Hills Christian Family Centre and Frontier Christian centre.

Since the late nineteenth century Main Road Blackwood and surrounding areas have formed a vibrant and diverse food and retail centre.{{Citation needed|date=February 2017}}

[[image:Blackwood Roundabout looking SE Jan 2012.jpg|thumb|right|Blackwood Roundabout looking South-East, January 2012]]
[[image:Blackwood roundabout B1 bus.jpg|thumb|right|B1 train substitute bus at Blackwood roundabout]]

== Transport==
[[Blackwood railway station]] is a transport hub with buses to Adelaide and the outer Southern suburbs, and trains to Adelaide and Belair.

==Residents==
The suburbs of Blackwood, Glenalta and Craigburn Farm had a combined population of 6,379 in 2,596 households in 2001.<ref>{{cite web|url= http://www.id.com.au/mitcham/commprofile/default.asp?id=104&gid=190&pg=1|publisher=City of Mitcham|title=Community Profile|accessdate=12 July 2008}}</ref>

[[Norman Tindale]], an anthropologist, archaeologist and entomologist, lived in his Blackwood residence "Kurlge" from 1955 to 1969.

The [[Hip hop music|hip hop]] group the [[Hilltop Hoods]] and [[country music|country]] singer [[Beccy Cole]] (whose song ''Blackwood Hill'' references the suburb) <ref>{{cite web|publisher=Postcards SA|url=http://www.postcards.sa.com.au/features/beccy_cole_dvd.html|title=Beccy Cole|accessdate=12 July 2008}}</ref> are from the area. Award-winning screen composer and filmmaker [[Milton Trott]] grew up in Blackwood.{{Citation needed|date=February 2017}} Independent Hip-Hop artist [[Allday]] also grew up in Blackwood.

==Politics==
Blackwood is in the state electorate of [[Electoral district of Davenport|Davenport]], represented in the [[South Australian House of Assembly]] by Liberal MP [[Sam Duluk]] and in the federal electorate of [[Division of Boothby|Boothby]], represented in the [[Australian House of Representatives]] by Liberal MP [[Nicolle Flint]]. Blackwood has a relatively high vote for the [[Australian Greens|Greens]] when compared to the rest of its electorate and state.<ref>http://results.aec.gov.au/13745/Website/HousePollingPlaceFirstPrefs-13745-6862.htm</ref>

==Notes==
{{reflist}}

==External links==
*[http://www.mitchamcouncil.sa.gov.au City of Mitcham]
*[http://www.bhs.sa.edu.au Blackwood High School]
*[https://web.archive.org/web/20120925215657/http://users.tpg.com.au/corowood/ All Hallows' Anglican Church]
*[http://www.bhbc.org.au/ Blackwood Hills Baptist]
{{Commons category|Blackwood, South Australia}}

{{Coord|-35.016667|138.600|format=dms|type:city_region:AU-SA|display=title}}
{{City of Mitcham suburbs}}

[[Category:Suburbs of Adelaide]]