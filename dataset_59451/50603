{{Infobox journal
| title = Asia Pacific Journal of Human Resources
| cover = [[File:Asia Pacific Journal of Human Resources (journal cover).gif]]
| editors = Timothy Bartram, Malcolm Rimmer 
| discipline = [[Management studies]]
| former_names = Journal of the Institute of Personnel Management, Personnel Management, Human Resource Management Australia, Asia Pacific Human Resource Management
| abbreviation = Asia Pac. J. Hum. Resour.
| publisher = [[Wiley-Blackwell]] on behalf of the [[Australian Human Resources Institute]]
| country = 
| frequency = Quarterly
| history = 1966-present
| openaccess = 
| license = 
| impact = 0.548
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1744-7941
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1744-7941/currentissue
| link1-name = Online access
| link2 = http://apj.sagepub.com/content
| link2-name = Online archive
| OCLC = 52029127
| LCCN = sn92024094 
| CODEN = 
| ISSN = 1038-4111
| eISSN = 1744-7941
}}
The '''''Asia Pacific Journal of Human Resources''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers research, theoretical and conceptual developments, and examples of current practice in [[human resources]]. The journal was established in 1966 and is the official journal of the [[Australian Human Resources Institute]].<ref>[http://www.ahri.com.au/ AHRI homepage]. AHRI. Retrieved 16 May 2011.</ref> Until 2011, the journal was published by [[Sage Publications]]. As of 2012, it is published by [[Wiley-Blackwell]] on behalf of the [[Australian Human Resources Institute]].

== Abstracting and indexing ==
The ''Asia Pacific Journal of Human Resources'' is abstracted and indexed by [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.548, ranking it 17th out of 24 journals in the category "Industrial Relations & Labor"<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Industrial Relations & Labor |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences  |series=[[Web of Science]] |postscript=.}}</ref> and 141st out of 172 journals in the category "Management".<ref name=WoS2>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Management |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1744-7941}}

[[Category:Publications established in 1966]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Human resource management journals]]


{{management-journal-stub}}