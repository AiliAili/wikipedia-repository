{{Infobox journal
| title = Journal of Family Issues
| cover = [[File:Journal of Family Issues.tif]]
| editor = Constance L. Shehan
| discipline = [[Sociology]]
| former_names = 
| abbreviation = J. Fam. Issues
| publisher = [[SAGE Publications]]
| country =
| frequency = Monthly
| history = 1980-present
| openaccess = 
| license = 
| impact = 1.264 
| impact-year = 2010
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200912
| link1 = http://jfi.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jfi.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 5035642
| LCCN = 80643987
| CODEN = JFISDT
| ISSN = 0192-513X
| eISSN = 1552-5481
}}
'''''Journal of Family Issues''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[home economics|family studies]]. The journal's [[editor-in-chief]] is Constance L. Shehan  ([[University of Florida]]). It was established in 1980 and is currently published by [[SAGE Publications]].

== Impact ==
According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 1.264, ranking it 15 out of 39 journals in the category "Family Studies".<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Family Studies|title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== Abstracting and indexing ==
''Journal of Family Issues'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]].

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://jfi.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Family therapy journals]]
[[Category:Sociology journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1980]]