{{Primary sources|date=June 2015}}
{{Infobox journal
| title         = New Letters
| cover         = [[File:New Letters (magazine) Fall 2006 cover.jpg|200px|Fall 2006 cover]]
| editor        = Robert Stewart 
| discipline    = 
| peer-reviewed = 
| language      = English
| former_names  = The University Review, The University of Kansas City Review
| abbreviation  = 
| publisher     = [[University of Missouri–Kansas City]]
| country       = [[United States]]
| frequency     = Quarterly
| history       = 1934-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.newletters.org
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 1759882
| LCCN          = 
| CODEN         = 
| ISSN          = 0146-4930
| eISSN         = 
| boxwidth      = 
}}

'''''New Letters''''', the name it has been published under since 1970, is one of the oldest literary magazines in the United States and continues to publish award-winning poems and fiction. The magazine is based in [[Kansas City, Missouri]].<ref>{{cite web|title=Literary Journals|url=http://www.missouribooks.org/Journals|work=Missouri Center for the Book|accessdate=December 10, 2015}}</ref>

==History & editors==
'''''The University Review''''' was founded in 1934 at the University of Kansas City, a small, private school that later became part of the [[University of Missouri]] system. In its first two years, the periodical published a discussion on "Art and Social Struggle", including contributions from [[Thomas Hart Benton (painter)|Thomas Hart Benton]] and [[Diego Rivera]], a story by [[Vance Randolph]], a poem by [[Edgar Lee Masters]], and a personal note from [[Pearl S. Buck]].<ref name=hist>[http://www.newletters.org/history.asp''New Letters'' history Web page], at the ''New Letters'' Web site, accessed February 5, 2007</ref> 

Starting with the Spring 1938 issue, Alexander P. Cappon became editor and remained in that post for the next 33 years. In 1944 the magazine's name was changed to '''''The University of Kansas City Review'''''. In that time the magazine published work by [[May Sarton]], [[J.D. Salinger]], [[E.E. Cummings]], [[Marianne Moore]], [[May Swenson]], [[James T. Farrell]], [[Kenneth Rexroth]].<ref name=hist/>

In 1971, David Ray took over as editor and the magazine's name was changed again, this time to ''New Letters''. Ray published work by [[Robert Bly]], [[Cyrus Colter]], [[Anselm Hollo]], [[Joyce Carol Oates]], [[Richard Hugo]], [[Robert Peters]] and [[Josephine Jacobsen]].<ref name=hist/>

In 1986, James McKinley became editor, and under his editorship the magazine published new work by [[Amiri Baraka]], [[Thomas Berger (novelist)|Thomas Berger]], former President [[Jimmy Carter]], [[Annie Dillard]], [[Tess Gallagher]], [[William Gass]], [[Charles Simic]], [[John Updike]], and [[Miller Williams]].<ref name=hist/> 

Robert Stewart took over the post of editor-in-chief for ''New Letters'', ''New Letters on the Air'', and their affiliate, [[BkMk Press]] in September 2002. Since becoming editor, the magazine has published such writers as [[Brian Doyle (writer)|Brian Doyle]], [[Quincy Troupe]], [[Daniel Woodrell]], [[Sherman Alexie]], [[Marilyn Hacker]], [[Maxine Kumin]] and [[Charlotte Holmes]].<ref name=hist/>

''New Letters'' won the [[National Magazine Award]] for the essay on May 1, 2008 at Lincoln Center in New York.  The essay "I Am Joe's Prostate" by Thomas E. Kennedy appears in Volume 73, Issue 4.

==''New Letters on the Air''==
In 1977, editor David Ray and his wife, Judy, began the audio literature program ''New Letters on the Air'', a half-hour radio program featuring writers reading from their work and talking about it.<ref name=hist/>

Rebekah Presson produced and hosted the show for many years until 1996 when Angela Elam took over. The program is now the longest continuously-running national literary radio series, having broadcast more than 1,200 programs.<ref name=hist/> The show is now heard on radio stations worldwide, and is available to even more listeners as a podcast.

==New Letters Literary Awards==
The New Letters Literary Awards program was begun in 1986.<ref name=hist/> It consists of prizes for poetry, essays and short stories:<ref>[http://www.newletters.org/default.asp Home page], ''New Letters'' Web site, accessed February 5, 2007</ref>
* New Letters Poetry Prize &mdash; $1,500 for the best group of three to six poems 
* Dorothy Churchill Cappon Essay Prize &mdash; $1,500 for the best essay
* Alexander Patterson Cappon Fiction Prize &mdash; $1,500 for the best short story

==See also==
*[[List of literary magazines]]

==References==
<references />

==External links==
* [http://www.newletters.org/ ''New Letters'' website]
* [http://www.newletters.org/writers-wanted/ ''New Letters'' literary awards web page]
* [http://www.newletters.org/on-the-air/ ''New Letters On the Air'' website]
{{University of Missouri–Kansas City}}
{{coord|39|2|11|N|94|34|41|W|region:US-MO|display=title}}

{{DEFAULTSORT:New Letters}}
[[Category:American literary magazines]]
[[Category:American quarterly magazines]]
[[Category:Magazines established in 1934]]
[[Category:Poetry literary magazines]]
[[Category:University of Missouri–Kansas City]]
[[Category:Magazines published in Missouri]]