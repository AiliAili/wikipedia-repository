{{Infobox journal
| title = Journal of Morphology
| cover =
| editor = J. Matthias Starck
| discipline = [[Anatomy]], [[morphology (biology)|morphology]]
| abbreviation = J. Morphol.
| publisher = [[John Wiley & Sons]]
| country =
| frequency = Monthly
| history = 1887–present
| openaccess = 
| license = 
| impact = 1.735
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291097-4687
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 0362-2525
| eISSN = 1097-4687
}}
The '''''Journal of Morphology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] of [[anatomy]] and [[morphology (biology)|morphology]] featuring primary research articles, review articles, and meeting abstracts.<ref>[http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291097-4687 ''Journal of Morphology'' Product Information page]</ref> The journal was established in 1887 by [[zoology|zoologist]] and morphologist [[Charles Otis Whitman|C. O. Whitman]] and underwent reorganization in 1907. It is currently edited by J. Matthias Starck.<ref>''Am. Zool. (1979) 19 (4): 1251-1253.''</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.735, ranking it 8th out of 20 journals in the category "Anatomy & Morphology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Anatomy & Morphology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |work=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1= http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291097-4687}}

{{DEFAULTSORT:Journal Of Morphology}}
[[Category:Publications established in 1887]]
[[Category:Anatomy journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]