{{Other uses|Blackening (disambiguation)}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = The Blackening
| Type        = studio
| Artist      = [[Machine Head (band)|Machine Head]]
| Cover       = Machine Head - The Blackening.jpg
| Alt         = Cover shows a skeleton wearing a cloak and a ball and chain. He holds up a plaque with backwards lettering, EHT / RVORRIM / hcihw / srettalF / ton
| Released    = March 27, 2007
| Length      = 61:04
| Recorded    = Aug – Nov 2006
| Studio      = Sharkbite Studios, [[Oakland, California|Oakland]], [[California|CA]]
| Genre       = [[Thrash metal]], [[groove metal]]
| Label       = [[Roadrunner Records|Roadrunner]]
| Producer    = [[Robb Flynn]]
| Last album  = ''[[Elegies (Machine Head DVD)|Elegies]]''<br>(2005)
| This album  = '''''The Blackening'''''<br>(2007)
| Next album  = ''[[Unto the Locust]]''<br>(2011)
| Misc        = {{Singles
  | Name           = The Blackening
  | Type           = studio
  | single 1       = [[Aesthetics of Hate]]
  | single 1 date  = April 3, 2007
  | single 2       = Now I Lay Thee Down
  | single 2 date  = October 2, 2007
  | single 3       = [[Halo (Machine Head song)|Halo]]
  | single 3 date  = May 13, 2008
  }}
 }}

'''''The Blackening''''' is the sixth album by American [[groove metal]] band [[Machine Head (band)|Machine Head]]. Released on March 27, 2007 in the U.S., ''The Blackening'' sold 16,000 units in its first week, and became Machine Head's second highest charting release at number 54 on the [[Billboard 200|''Billboard'' 200]], and charted in the Top 20 throughout many countries in the rest of the world. ''The Blackening'' has been certified silver by the BPI for sales in the UK in excess of 60,000 copies.<ref>{{cite web |title=Album Certification |publisher=Blabbermouth |url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=150193 |accessdate=2010-12-07}}</ref>

The album's first single "[[Aesthetics of Hate]]", is a retaliation to an article written by William Grim for the website Iconoclast. Titled "Aesthetics Of Hate: R.I.P. Dimebag Abbott, & Good Riddance", the article praised the murder of guitarist [[Dimebag Darrell]]. Machine Head frontman [[Robb Flynn]] said the song was written as a "fuck you" to Grim and tribute to Dimebag. The song received a [[Grammy Award]] nomination for "Best Metal Performance" at the [[50th Grammy Awards]]. The text in the mirror of the album's cover reads "the mirror which flatters not." The song "Beautiful Mourning" has been featured as a playable song in the video game ''[[Guitar Hero: Metallica]]''.

==Background==
The inaugural song written for Machine Head's follow up to their 2003 release ''[[Through the Ashes of Empires]]'' was entitled "Godfather 4". This was in reference to a [[guitar riff]] in the song which vocalist [[Robert Flynn]] felt had "a [[The Godfather|Godfather]] type vibe". The song was shelved, although the riff resurfaced in a b-side.<ref name="Midwest Metal"/> "Slanderous" was the first song finished that was in the final track listing. By February 2005, Machine Head had penned rough versions of "Beautiful Mourning" and "Aesthetics of Hate".<ref name="Midwest Metal">{{cite web |title="Machine" head |publisher=Midwest Metal |url=http://www.midwestmetalmagazine.com/interviewmachinehead_2.html |accessdate=2007-08-21}}</ref>

==Recording==
A November 2005 demo contained thirteen songs, which included rough versions of "Aesthetics of Hate" and "Halo". "Aesthetics of Hate" contained what Flynn described as a "totally fucking lame "[[Angel of Death (Slayer song)|Angel of Death]]" rip off. I hated it every time we played it so I was glad to see that part go!"<ref name="Midwest Metal"/> In mid August 2006, Machine Head announced that the title of their sixth studio effort would be ''The Blackening''.<ref>{{cite web |title=machine head announces new album title |publisher=[[Blabbermouth.net]] |date=2006-08-17 |url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=56646  |accessdate=2007-08-18}}</ref> Flynn and drummer [[Dave McClain (drummer)|Dave McClain]] spent August 18 and 19 [[jam (music)|jamming]] together, fine tuning songs chosen for the album and performing [[pre-production]].<ref name="ExcitedRecording">{{cite web | title="Machine" head Frontman: "I Can't remember ever being this excited about Recording"|publisher= [[Blabbermouth.net]]|date=2006-08-22|url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=56862 |accessdate=2007-08-18}}</ref> [[Phil Demmel]] resumed [[lead guitar]] duties and [[Adam Duce]] returned as [[bassist]].

Machine Head entered Sharkbite Studios in [[Oakland, California]] on August 21 to begin recording ''The Blackening'', with Flynn assuming production duties for the second time. From twenty six written songs, the selection was whittled down to eight.<ref name="ExcitedRecording"/> McClain completed the drum tracks to six of the eight tracks on August 25, followed by the remaining two the next day.<ref name="CompletedDrums">{{cite web|title="Machine" head: drum tracks completed |publisher=Machinehead1 |date=2006-08-28 |url=http://www.machinehead1.com/diary_2006.html |accessdate=2007-08-19 |archiveurl=https://web.archive.org/web/20070701025651/http://www.machinehead1.com/diary_2006.html |archivedate=2007-07-01 |deadurl=yes |df= }}</ref> Tracking concluded on November 16, 2006.<ref name="CompletedRecording">{{cite web | title="Machine" head frontman on new CD: 'Guitars are Massive!'|publisher= [[Blabbermouth.net]]|date=2006-11-24|url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=62724 |accessdate=2007-08-21}}</ref>

The band was surprised at the length of "A Farewell to Arms" and "Clenching the Fists of Dissent" running over ten minutes, and wondered if their fans would "be able to get their heads around" what the band was doing. Record company Roadrunner Records hesitated the band's decision to open the album with a ten and a half minute song ("Clenching the Fists of Dissent"), and asked the band questions similar to "Are you sure you don’t want to get into something a little more direct?"<ref name=" Robb Flynn of Machine Head: 'The Fans Carried Us Through'"/> The band disregarded the comment and believed the song set the [[Tone (literature)|tone]] musically for the remainder of the album.<ref name=" Robb Flynn of Machine Head: 'The Fans Carried Us Through'">{{cite web |title= Robb Flynn of Machine Head: 'The Fans Carried Us Through' |author=Kelly, Amy |publisher=Ultimate-Guitar.com |date=2007-04-21 |url=http://www.ultimate-guitar.com/interviews/interviews/robb_flynn_of_machine_head_the_fans_carried_us_through.html |accessdate=2007-11-04}}</ref>

Flynn said "Clenching the Fists of Dissent" and "Wolves" were the most difficult songs to construct.<ref name="Interview Robb Flynn Machine Head"/> For "Clenching", he attributed the difficulty to the amount of [[multitrack recording|tracks]]; the introduction of the song has 90, including multiple three-part [[harmony]] with two guitars, 20 tracks of [[snare drum]], four tracks of [[kick drum]], and military marching [[cymbal]]s. The song continues with five electric guitars, three [[Steel-string acoustic guitar|acoustic guitars]] and two bass guitars. Flynn also described the song "Wolves" as difficult because "it's all got to be super tight; there were so many riffs that had to be just locked on down picking".<ref name="Interview Robb Flynn Machine Head"/> Mark Keaton provided [[audio engineering]], and [[Colin Richardson]] [[Production sound mixer|mixed]] the record in London.<ref name="Interview Robb Flynn Machine Head">{{cite web |title=Interview Robb Flynn Machine Head |author=Omowale, Karma |publisher=fourteeng.net |date=2007-03-17 |url=http://www.fourteeng.net/interviews/machinehead.html |accessdate=2007-11-04}}</ref>

Flynn believes the band was naive about the length of songs, and was unconcerned if they would be suitable for radio or [[MTV]], as the band purposely did not want to receive airplay. "Now I Lay Thee Down" has a chorus Flynn describes as "[[pop music|poppy]]" and added "fucked up lyrics" about one person killing another, and then committing suicide.<ref name="Robb Flynn Interview - Metal Theater"/> This was to make the song not suitable for airplay as the band wanted to make a "dark epic record".<ref name="Robb Flynn Interview - Metal Theater">{{cite web |title=Robb Flynn Interview - Metal Theater |publisher=Metaltheater.com |date=2007-11-02 |url=http://www.metaltheater.com/interviews/MachineHead.asp |accessdate=2007-11-04}}</ref>

==Lyrical themes==
Lyrical themes explored on ''The Blackening'' include love, war, [[organized religion]], anger in society and Machine Head's "winner take all" spirit. The album's first single, "[[Aesthetics of Hate]]", is a retaliation that captures the band's anger towards an article written by William Grim for the conservative web site Iconoclast. Titled "Aesthetics of Hate: R.I.P. [[Dimebag|Dimebag Abbott]], & Good Riddance", the article praised the murder of Dimebag Darrell by [[Nathan Gale]], while Darrell was performing with [[Damageplan]] on December 8, 2004. Grim wrote Darrell was "an ignorant, barbaric, untalented possessor of a guitar" who looks "more [[simian]] than human" and is "part of a generation that has confused sputum with art and involuntary reflex actions with emotion".<ref name="Long Live Dimebag Darrell"/> After reading the article, Flynn was furious and wrote the song to send a message to Grim implying "fuck you", and pay tribute to Dimebag. He created a post on the band's message board soon after the event describing his friendship with Dimebag.<ref name="Long Live Dimebag Darrell">{{cite web|title=Machine Head's Robert Flynn: "Long Live Dimebag Darrell In The Hearts Of Us All" |author=Flynn, Robert |publisher=Blabbermouth.net |date=2004-12-20 |url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=30543 |accessdate=2007-08-04 |deadurl=yes |archiveurl=https://web.archive.org/web/20080402132040/http://www.roadrunnerrecords.com:80/blabbermouth.net/news.aspx?mode=Article&newsitemID=30543 |archivedate=2008-04-02 |df= }}</ref>

{{Quote|"What would YOU know about love or values? What would YOU know about giving to the world? All that you know is teaching prejudice, and your heart is as black as the 'ignorant, filthy, and hideously ugly, heavy metal fans' you try and paint in your twisted, fictitious ramblings. It's because of people like YOU, that there are [[Nathan Gale]]'s in this world, NOT the Dimebags and metal musicians who work to unite people through music".<ref name="Long Live Dimebag Darrell"/> |Robb Flynn speaks about William Grim who wrote an article praising Dimebag Darrell's murder.}}

The songs "Clenching the Fists of Dissent", "A Farewell to Arms" (probably a reference to Ernest Hemingway's novel of the same title [[A Farewell to Arms]], which is set during the first World War), and "Halo" deal with politics, the [[war in Iraq]] and organized religion, respectively. The band wrote the lyrics about the [[Iraq war]] after conducting research and found that "a lot of stuff does not add up",<ref name="Interview Robb Flynn Machine Head"/> according to Flynn, which angered the band. Machine Head's debut album, ''[[Burn My Eyes]]'', featured a similar song titled "A Thousand Lies" which dealt with the [[Gulf War]]. "Slanderous" deals with hate that still exists throughout society and "Wolves" addresses the band's competitive "winner takes all" spirit.<ref name="Machine Head new album 'The Blackening' out soon!">{{cite web |title=Machine Head new album ''The Blackening'' out soon! |publisher=Roadrunner records |url=http://www.roadrunnerrecords.co.za/page/News_item?select=News_Collection&rowid=9903 |accessdate=2007-08-03}}</ref> "Now I Lay Thee Down" features a ''[[Romeo and Juliet]]''-esque love story about one person killing another person, and then killing themself. The song relates to the William Shakespeare play, ''Romeo & Juliet'', where at the end of the story, two lovers kill themselves because they assume that their lover is dead. The original lyrics to the chorus of the song were considered "poppy" by Flynn and his band-mates so they intentionally changed them into the lyrics they are now so they wouldn't get radio play.

==Reception==
{{Album ratings
|rev1 = [[About.com]]
|rev1score = {{rating|4.5|5}}<ref name="about.com">{{cite web|url=http://heavymetal.about.com/od/cdreviews/fr/machineheadblac.htm|title=Machine Head - The Blackening|publisher=[[About.com]]|first=Chad|last=Bowar|accessdate=2012-10-31}}</ref>
|rev2 = [[Allmusic]]
|rev2score = {{Rating|4.5|5}}<ref name="Allmusic"/>
|rev3 = [[Blabbermouth.net]]
|rev3score = 9.5/10<ref name="Blabbermouth"/>
|rev4 = [[Blender (magazine)|''Blender'']]
|rev4score = {{Rating|3.5|5}}<ref name="Blender">{{cite web|title=Blender - The Blackening |author=J.D. Considine |publisher=''Blender'' |date=2007-04-17 |url=http://www.blender.com/guide/reviews.aspx?id=4497 |accessdate=2007-08-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20070927195128/http://www.blender.com/guide/reviews.aspx?id=4497 |archivedate=September 27, 2007 }}</ref>
|rev5 = [[IGN]]
|rev5score = 8.4/10<ref name="IGN">{{cite web|url=http://uk.ign.com/articles/2007/05/05/machine-head-the-blackening|title=Machine Head - The Blackening|publisher=[[IGN]]|first=Ed|last=Thompson|date=2007-05-04|accessdate=2012-10-31}}</ref>
|rev6 = ''[[Kerrang!]]''
|rev6score = {{Rating|5|5}}{{citation needed|date=May 2012}}
|rev7 = [[MusicOMH]]
|rev7score = {{rating|5|5}}<ref name="musicomh">{{cite web|url=http://www.musicomh.com/albums/machine-head-2_0307.htm|title=Machine Head - The Blackening|publisher=[[MusicOMH]]|first=Tom|last=Day|accessdate=2012-10-31}}</ref>
|rev9 = [[PopMatters]]
|rev9score = 8/10<ref name="popmatters">{{cite web|url=http://www.popmatters.com/pm/review/machine-head-the-blackening/|title=Machine Head: The Blackening|publisher=[[PopMatters]]}}</ref>
}}
''The Blackening'' received critical acclaim. [[Blabbermouth.net]] reviewer Don Kaye awarded the album an almost perfect score of 9.5 out of 10, saying: "one of the purest, finest, most powerful expressions of modern heavy metal released".<ref name="Blabbermouth"/> Kaye praised the guitar work of Flynn and Demmel on the tracks "Beautiful Mourning" and "Aesthetics of Hate", and thought the band members surpassed their musical ability in an "intense and dynamic way".<ref name="Blabbermouth">{{cite web|title=Blabbermouth - The Blackening Review |first=Don |last=Kate |publisher=[[Blabbermouth.net]] |url=http://www.roadrunnerrecords.com/blabbermouth.net/showreview.aspx?reviewID=1066 |accessdate=2007-08-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20071015224258/http://roadrunnerrecords.com:80/blabbermouth.net/showreview.aspx?reviewID=1066 |archivedate=2007-10-15 |df= }}</ref>

Thom Jurek of [[Allmusic]] described the album as "an over the top rage and pummelfest with all the qualities that earned the group its enormous fan base by touring and recording" and that the thrash metal element "rivals [[Slayer]] at their best".<ref name="Allmusic">{{cite web|title=Allmusic - The Blackening|first=Thom|last=Jurek|publisher=Allmusic|url=http://www.allmusic.com/album/the-blackening-r1023773|accessdate=2012-05-03}}</ref> ''[[Rock Sound]]'' magazine reviewer Eleanor Goodman awarded the album 9 out of 10, praising the first two minutes of the opening song, "Clenching the Fists of Dissent", as "a full-on old-skool thrash attack".<ref name="Rock Sound - The Blackening">{{cite web|title=Rock Sound - The Blackening|first=Eleanor|last=Goodman|publisher=''Rock Sound''|url=http://www.rock-sound.net/view_review.php?bandname=MACHINE-HEAD&review=244|archive-url=https://web.archive.org/web/20071014070547/http://rock-sound.net:80/view_review.php?bandname=MACHINE-HEAD&review=244|dead-url=yes|archive-date=2007-10-14|accessdate=2007-08-03}}</ref>

"Aesthetics of Hate" received a Grammy Award nomination for "Best Metal Performance" at the [[50th Grammy Awards]], which took place on February 12, 2008. Though heavily favored, they controversially lost to eventual winners [[Slayer]] who had won the previous year for the same album. Other nominees in the category were [[King Diamond]], [[Shadows Fall]], and [[As I Lay Dying (band)|As I Lay Dying]].<ref name="Best Metal Performance ">{{cite web|title=Best Metal Performance |publisher=Grammy.com |url=http://www.grammy.com/GRAMMY_Awards/50th_Show/list.aspx#04 |accessdate=2007-12-07 |archiveurl=https://web.archive.org/web/20071208155421/http://www.grammy.com/GRAMMY_Awards/50th_Show/list.aspx |archivedate=2007-12-08 |deadurl=yes |df= }}</ref> At the fifth annual [[Metal Hammer Golden Gods Awards]] and at the ''[[Kerrang!]]'' Awards in 2007, ''The Blackening'' won best album over albums by [[Slayer]] and [[Lamb of God (band)|Lamb of God]].<ref name="Bullet booed at Metal Awards">{{cite web|title=Bullet booed at Metal Awards|author=Mendoza, Nadia|publisher=''The Sun''|date=2007-06-12|url=http://www.thesun.co.uk/article/0,,2004580002-2007270171,00.html|archive-url=https://web.archive.org/web/20070616191551/http://www.thesun.co.uk:80/article/0,,2004580002-2007270171,00.html|dead-url=yes|archive-date=2007-06-16|accessdate=2007-08-27}} </ref>

Three years after the release of ''The Blackening'', Machine Head wrapped their touring cycle for the album, finishing in Sydney, Australia on 28 March. In February 2010, ''The Blackening'' was awarded the Album of the Decade award from ''Metal Hammer''.<ref>{{cite web|url=http://www.metalhammer.co.uk/news/albums-of-the-decade-machine-head-the-blackening/ |title=Blog Archive » Albums Of The Decade: Machine Head – ‘The Blackening’ |publisher=Metal Hammer |date=2009-07-23 |accessdate=2011-10-01}}</ref> On October 8, 2011, the album was voted [[Roadrunner Records]] "Album of the Century" in a poll.<ref name="albumcentury">{{cite web|url=http://www.roadrunnerrecords.co.uk/page/News?news_id=115195|title=ROADRUNNER RECORDS ALBUM OF THE CENTURY - THE COUNTDOWN 5-1|publisher=[[Roadrunner Records]]|date=2011-10-08|accessdate=2012-05-03}}</ref>

==Chart performance==
[[File:RobFLynn.jpg|alt=A man in a black sleeveless shirt plays the guitar on a stage.|thumb|150px|right|Flynn performing at a concert promoting ''The Blackening'']]
''The Blackening'' was leaked on to the Internet two weeks before its release by an American journalist who received an advanced copy. Demmel thought it unfortunate, but that it created a "pre-buzz", as did Flynn who thought the buzz was nothing like the band had before; "There’s internet street buzz vs. record company [[Promotion (marketing)|hype]] and if the hype matches up to the street buzz, then you’re seeing a lot of bands selling a lot of records in the first week".<ref name="It isn’t the sound that’s coming out of the speakers; it’s the sound that is coming out of that person’s soul!">{{cite web
|title=It isn’t the sound that’s coming out of the speakers; it’s the sound that is coming out of that person’s soul!
|author=Karma
|publisher=Fourteeng
|url=http://www.fourteeng.net/interviews/machinehead.html
|accessdate=2007-08-03}}</ref> ''The Blackening'' became Machine Head's highest charting release in the United States, where it entered the Top Rock albums at No. 9, and the [[Billboard 200|''Billboard'' 200]] at No. 54 (their highest charting and first week ever), with sales of almost 16,000 copies. This improved on the band's 2004 release, ''Through the Ashes of Empires'', which sold 12,000 units in its first week, and entered the ''Billboard'' 200 at No. 88.

The album did exceptionally well overseas where it entered the Mainstream Top 20 in the German and Belgium charts at No. 12, No. 14 in Australia, No. 16 in the United Kingdom and No. 19 in Sweden. Charting positions below the Top 20 consisted of Ireland at No. 23, No. 29 in the Netherlands, No. 34 in France and No. 55 in Italy.<ref name="MACHINE HEAD: 'The Blackening' Enters Italian Chart At No. 55">{{cite web |title=Machine Head: 'The Blackening' Enters Italian Chart At No. 55 |publisher=Blabbermouth |date=2007-04-23 |url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=71033 |accessdate=2007-08-03}}</ref> ''The Blackening'' sold more copies in two weeks than ''Through the Ashes of Empires'' sold in three years.<ref name="Machine Head: Colour Our World Blackened">{{cite web |title=Machine Head: Colour Our World Blackened |author=Donnelly, Justin |publisher=The Metal Forge |date=2007-08-02 |url=http://www.themetalforge.com/modules.php?name=News&file=article&sid=1209 |accessdate=2007-08-03}}</ref>

==Cover art and special edition==
The artwork for ''The Blackening'' was designed by Robb Flynn, longtime Machine Head collaborator Paul Brown, and Deanna Alcorn. Based on a metal carving in the 16th century, these carvings were used to scare people about going to [[Hell]] and for [[propaganda]] purposes. It features a skeleton on a throne that is standing on top of the world, and the words "The mirror which flatters not." Flynn thought "this whole record is like holding up a mirror to ourselves. It's talking about things that are going on now, but it's not necessarily specific to this time."<ref name="MACHINE HEAD: 'The Blackening' Artwork Revealed ">{{cite web |title=Rob Flynn (Machine Head) - 'The mirror which flatters not' |publisher=The Gauntlet |date=2007-12-01 |url=http://www.thegauntlet.com/article/267/6520/Machine-Head.html |accessdate=2007-08-17}}</ref>

Three special editions of ''The Blackening'' were released: a two-LP [[gatefold]] [[gramophone record|vinyl]], a two-disc special edition packaged in a silver foil-enhanced slipcase, with a bonus [[Metallica]] cover added to the end of the album.  The second disc was a [[DVD]] featuring a video entitled ''The Making of The Blackening'', and a 2006 tour diary  of the Sounds of the Underground Tour. Both of these versions were released in 2007.{{citation needed|date=May 2009}}

The third version, released in 2008, was a 3-disc set which came with the original album, a CD featuring various covers the band had recorded in the past, demos and other unreleased material and a DVD which featured various live performances of the band at festivals, the ''Burn My Eyes'' 10th anniversary show and music videos from ''The Blackening'' accompanied by their "making's of".{{citation needed|date=May 2009}}

==Track listing==
{{tracklist
| headline        = 
| music_credits = yes 
| lyrics_credits = yes
| title1          = Clenching the Fists of Dissent
| lyrics1         = [[Robert Flynn]]
| music1          = Flynn, [[Dave McClain (drummer)|Dave McClain]], [[Phil Demmel]]
| length1         = 10:37
| title2          = Beautiful Mourning
| lyrics2         = Flynn
| music2          = Flynn, Demmel
| length2         = 4:46
| title3          = [[Aesthetics of Hate]]
| lyrics3         = Flynn, [[Adam Duce]]
| music3          = Flynn
| length3         = 6:30
| title4          = Now I Lay Thee Down
| lyrics4         = Flynn
| music4          = Flynn, Demmel
| length4         = 5:35
| title5          = Slanderous
| lyrics5         = Flynn
| music5          = Flynn, Demmel
| length5         = 5:17
| title6          = [[Halo (Machine Head song)|Halo]]
| lyrics6         = Flynn
| music6          = Flynn, McClain, Duce, Demmel
| length6         = 9:03
| title7          = Wolves
| lyrics7         = Flynn
| music7          = Flynn, Demmel, Duce
| length7         = 9:01
| title8          = A Farewell to Arms
| lyrics8         = Flynn, Duce, Demmel
| music8          = Flynn, Demmel
| length8         = 10:13
}}
{{tracklist
| headline = Deluxe edition bonus track
| collapsed = yes
| title9 = [[Hallowed Be Thy Name]]
| note9 = [[Iron Maiden]] cover
| length9 = 7:27
| title10 = [[Battery (song)|Battery]]
| note10 = [[Metallica]] cover
| length10 = 5:02
}}
{{tracklist
| headline = 2008 bonus disc
| collapsed = yes
| title1 = Alan's on Fire
| note1 = [[Poison Idea]] cover
| length1 = 3:59
| title2 = Negative Creep
| note2 = [[Nirvana (band)|Nirvana]] cover
| length2 = 2:40
| title3 = Seasons Wither
| length3 = 6:15
| title4 = My Misery
| length4 = 4:38
| title5 = House of Suffering
| note5 = [[Bad Brains]] cover
| length5 = 2:09
| title6 = The Possibility of Life's Destruction
| note6 = [[Discharge (band)|Discharge]] cover
| length6 = 1:26
| title7 = Ten Ton Hammer
| note7 = Extended Original Mix
| length7 = 5:06
| title8 = Hole in the Sky
| note8 = [[Black Sabbath]] cover
| length8 = 3:32
| title9 = [[Colors (Ice-T song)|Colors]]
| note9 = [[Ice-T]] cover
| length9 = 4:41
| title10 = Hard Times
| note10 = Live [[Cro-Mags]] cover
| length10 = 2:02
| title11 = Halo (I Want Your Soul)
| note11 = Demo
| length11 = 6:34
| title12 = Aesthetics of Hate (Thrash-Terpiece)
| note12 = Demo
| length12 = 4:49
}}
{{tracklist
| headline = 2007 special edition DVD
| collapsed = yes
| title1 = The Making of The Blackening
| length1 = 24:28
| title2 = Sounds of the Underground 2006 Tour Diary
| length2 = 10:37
}}
{{tracklist
| headline = 2008 special edition DVD
| collapsed = yes
| title1 = Clenching the Fists of Dissent
| note1 = Live at With Full Force 2008
| length1 = 8:44
| title2 = Now I Lay Thee Down
| note2 = Live at With Full Force 2008
| length2 = 5:30
| title3 = Halo
| note3 = Live at With Full Force 2008
| length3 = 9:20
| title4 = Aesthetics of Hate
| note4 = Live at Rock In Rio 2008
| length4 = 6:07
| title5 = [[Davidian (song)|Davidian]]
| note5 = Live at Rock In Rio 2008
| length5 = 6:35
| title6 = [[Imperium (Machine Head song)|Imperium]]
| note6 = Live at Download 2007
| length6 = 6:05
| title7 = [[Old (song)|Old]]
| note7 = Live at Download 2007
| length7 = 5:02
| title8 = A Thousand Lies
| note8 = Live at Burn My Eyes 10th Anniversary Show 2004
| length8 = 7:04
| title9 = The Rage to Overcome
| note9 = Live at Burn My Eyes 10th Anniversary Show 2004
| length9 = 4:33
| title10 = Death Church
| note10 = Live at Burn My Eyes 10th Anniversary Show 2004
| length10 = 5:43
| title11 = Blood for Blood
| note11 = Live at Burn My Eyes 10th Anniversary Show 2004
| length11 = 5:54
| title12 = Halo
| note12 = Uncensored & Unedited Music Video
| length12 = 5:10
| title13 = Now I Lay Thee Down
| note13 = Uncensored & Unedited Music Video
| length13 = 5:36
| title14 = Aesthetics of Hate
| note14 = Uncensored & Unedited Music Video
| length14 = 6:09
| title15 = The Making of Halo
| length15 = 6:32
| title16 = The Making of Now I Lay Thee Down
| length16 = 4:54
| title17 = The Making of Aesthetics of Hate
| length17 = 3:26
}}

== Credits ==
Writing, performance, and production credits were adapted from the album's [[liner notes]].<ref>{{Cite AV media notes |title=The Blackening |others=[[Machine Head (band)|Machine Head]] |year=2007 |publisher=[[Roadrunner Records]]}}</ref>

=== Personnel ===
==== Machine Head ====
* [[Robb Flynn]] – [[Singing|vocals]], [[rhythm guitar]]
* [[Phil Demmel]] – [[lead guitar]], backing vocals
* [[Adam Duce]] – [[Bass guitar|bass]], [[backing vocalist|backing vocals]]
* [[Dave McClain (drummer)|Dave McClain]] – [[Drum kit|drums]]

==== Production ====
* Robb Flynn – [[Record producer|production]]
* Mark Keaton – [[Audio engineering|engineering]]
* Vincent Wojno – additional engineering, [[Pro Tools]] editing
* [[Colin Richardson]] – [[Audio mixing (recorded music)|mixing]]
* Matt Hyde – mixing engineer
* Lee Slater – assistant engineer
* Rohan Onraet – assistant engineer
* Eddy Schreyer – [[Audio mastering|mastering]]

==== Visual art ====
* Alex Solca – [[photography]]
* Bau-da Design – package design
* Deanna Alcorn – package design
* Robb Flynn – package design

=== Studios ===
* Sharkbite Studios, [[Oakland, California|Oakland, CA]], USA – [[Sound recording and reproduction|recording]]
* Strongroom Studios, [[London]], UK – mixing
* [[Metropolis Group|Metropolis Studios]], London, UK – mixing
* VIP Studios, Oakland, CA, USA – Pro Tools editing
* Oasis Mastering, [[Burbank, California|Burbank, CA]], USA, mastering

== Charts ==

{| class="wikitable sortable plainrowheaders"
|-
! Chart
! Peak<br>position
|-
{{Albumchart|Australia|14|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Austria|19|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Flanders|52|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Wallonia|74|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Netherlands|29|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Finland|33|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|France|36|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Germany|12|artist=Machine Head}}
|-
{{Albumchart|Ireland|23|year=2007 |week=13}}
|-
{{Albumchart|Italy|55|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Oricon|59|M|url=http://www.oricon.co.jp/prof/161643/products/700990/1/ |title=ザ・ブラッケニング - マシーン・ヘッド}}
|-
{{Albumchart|Norway|36|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Spain|79|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Sweden|19|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|Switzerland|29|artist=Machine Head |album=The Blackening}}
|-
{{Albumchart|UK|16|artist=Machine Head}}
|-
{{Albumchart|Billboard200|54|artist=Machine Head}}
|-
|}

==Certifications==
{{Certification Table Top}}
{{Certification Table Entry |region=United Kingdom |artist=Machine Head |title=The Blackening |award=Silver |type=album}}
{{Certification Table Bottom}}

==References==
{{Reflist|40em}}

== External links ==
* {{AllMusic |class=album |id=mw0000477937 |label=''The Blackening''}}
* [http://roadrunnerrecords.co.uk/releases/the-blackening/ ''The Blackening''] at [[Roadrunner Records|Roadrunner]] UK

{{Machine Head}}
{{good article}}

{{DEFAULTSORT:Blackening, The}}
[[Category:2007 albums]]
[[Category:Machine Head (band) albums]]
[[Category:Roadrunner Records albums]]