{{Infobox journal
| cover = [[File:J Mater Chem B cover.gif]]
| discipline = [[Materials science]]
| abbreviation = J. Mater. Chem. B
| editor = Fiona McKenzie
| impact =4.872
| impact-year =2015
| website = http://www.rsc.org/Publishing/Journals/TB/
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| frequency = Weekly
| history = 2013-present
| ISSN =
| eISSN = 2050-7518
| CODEN = JMCBDV
| OCLC = 874322778
}}
The '''''Journal of Materials Chemistry B''''' is a weekly [[peer-reviewed]] [[scientific journal]] covering the properties, applications, and synthesis of new materials related to [[biology]] and [[medicine]]. It is one of the three journals that were created after the ''[[Journal of Materials Chemistry]]'' was split at the end of 2012. The first issue was published in January 2013.<ref>{{cite journal |last= |title=Journal of Materials Chemistry A, B & C – a new beginning |journal=Journal of Materials Chemistry B |year=2013 |volume=1 |issue=1 |pages=8 |url=http://pubs.rsc.org/en/content/articlelanding/2013/tb/c2tb90001g |doi=10.1039/c2tb90001g}}</ref> It is published by the [[Royal Society of Chemistry]]. The other two parts of the [[Journal of Materials Chemistry]] family are ''[[Journal of Materials Chemistry A]]'' and ''[[Journal of Materials Chemistry C]]'', which cover different materials science topics. The [[editor-in-chief]] is Fiona McKenzie.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Science Citation Index]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-22}}</ref>

== See also ==
* [[List of scientific journals in chemistry]]
* [[Soft Matter (journal)|''Soft Matter'']]

== References ==
<references/>

== External links ==
* {{Official website|http://www.rsc.org/Publishing/Journals/tb/}}

{{Royal Society of Chemistry|state=collapsed}}

[[Category:Chemistry journals]]
[[Category:Materials science journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 2013]]
[[Category:English-language journals]]
[[Category:Weekly journals]]