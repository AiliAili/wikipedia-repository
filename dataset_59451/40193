{{Infobox software| name = Big Brother
| logo =
| screenshot =
| caption = Big Brother Screen capture
| author = Sean MacGuire
| developer = Sean MacGuire, Robert-Andre Croteau
| released = November 1996<ref>http://web.archive.org/web/19981212015406/http://maclawran.ca/</ref>
| latest release version = 4.60
| latest release date = {{release date|2011|12|13}}<ref>[https://support.quest.com/Search/SearchDocumentation.aspx?dsNav=Ns:P_DateLastModifiedSortable|101|-1|,N:268437930&Dt=Big%20Brother Big Brother - Product Documentation]</ref>
| operating system =    [[Unix]] 
[[Linux]] 
[[Microsoft Windows|Windows]]
| genre = [[Network monitoring]]
| license = Commercial
| website = [http://www.bb4.com/ Official site]
}}
'''Big Brother''' (alias '''BB''') is a tool for systems and [[network monitoring]], generally used by [[system administrator]]s.  The advent of the [[dynamic web page]] allowed Big Brother to be one of the first monitoring systems to use the web as its [[user interface]].  Prior to this, monitoring tools were generally console based, or required graphic terminals such as [[X Window System|X Window]] to operate.  Big Brother produces [[HTML]] pages containing a simple matrix of hosts and tests with red and green dots to denote system status.

Big Brother was named after [[George Orwell]]'s character from his novel ''[[Nineteen Eighty-Four]]''.  E-mail from Big Brother originated from the Big Brother [[Ministry of Truth]], and users of the software were called Brothers.<ref name="Commercial Version of bb?">
{{Cite mailing list
  |last=MacGuire
  |first=Sean
  |authorlink=Sean MacGuire
  |title=Commercial Version of bb?
  |mailinglist=Big Brother
  |date=3 March 1997
  |url=http://support.bb4.com/archive/199707/msg00014.html
  |accessdate=3 March 2010 }}
</ref>

The application was designed to allow non-technical users to understand system and network status information through a simple interface and presentation, using a matrix to display status information for overhead displays in Network Operations Centers (NOCs).   It was designed to monitor [[computer]] systems and [[computer network|networks]], and for this reason does not use [[Simple Network Management Protocol|SNMP]] natively, instead using a [[client–server model]] and its own network [[communications protocol]].  Clients send status information over port [[List of TCP and UDP port numbers|TCP port 1984]] every 5 minutes.  Since the clients only send information to a specific monitoring server, its creators claim it is more secure than SNMP-based protocols which poll clients for information.  For this reason, Big Brother was featured at [[SANS Institute]] security conferences in 1998.<ref>{{Cite conference
  | first = Sean
  | last = MacGuire
  | authorlink = Sean MacGuire
  |author2=Robert Andre Croteau
  | title = Big Brother is Watching
  | publisher = SANS Conference
  | date = May 1999
  | location = Monterey, CA
  | url = http://bb4.org/bbsans98.pdf
  | accessdate = }}</ref> 1999,<ref>{{Cite conference
  | first = Sean
  | last = MacGuire
  | authorlink = Sean MacGuire
  |author2=Robert Andre Croteau
  | title = Big Brother is (Still) Watching
  | publisher = SANS Conference
  | date = May 1999
  | location = Baltimore, MD
  | url = http://bb4.org/bbsans99.pdf
  | accessdate = }}</ref> and at a SANSFIRE conference in 2001.<ref>{{Cite conference
  | first = Sean
  | last = MacGuire
  | authorlink = Sean MacGuirere
  |author2=Robert Andre Croteau
  | title = System, Network and Security Monitoring using Big Brother
  | publisher = SANSFIRE Conference
  | date = August 2001
  | location = Washington, DC
  | url = http://bb4.org/bbsans98.pdf
  | accessdate = }}</ref>

Big Brother has also been cited in a number of books on [[system administrator|system administration]],<ref>{{cite book
  |last = Horwitz
  |first = Jeff
  |authorlink = Jeff Horwitz
  |title = Unix system management: primer plus
  |publisher = [[SAMS Publishing|Sams Publishing]]
  |year = 2002
  |page = 169
  |url = https://books.google.com/books?id=-sue_SyjuCMC&pg=PA169&dq=big+brother+bb4.com&cd=3#v=onepage&q=big%20brother%20bb4.com&f=false
  |isbn = 0-672-32372-9}}</ref>
<ref>{{cite book
  |last = Well
  |first = Nicholas
  |authorlink = Nicholas Wells
  |title = Guide to Linux installation and administration
  |publisher = [[Cengage Learning EMEA]]
  |year = 2000
  |page = 548
  |url = https://books.google.com/books?id=jQE-iUCjUKAC&pg=PA548&dq=big+brother+bb4.com&cd=9#v=onepage&q=big%20brother%20bb4.com&f=false
  |isbn = 0-619-00097-X}}</ref><ref>{{cite book
  |last = Bookman
  |first = Charles
  |authorlink = Charles Bookman
  |title = Linux Clustering: Building and Maintaining Linux Clusters
  |publisher = [[SAMS Publishing|Sams Publishing]]
  |year = 2003
  |page = 178
  |url = https://books.google.com/books?id=5W2a5RIrn3sC&pg=PA178&dq=big+brother+bb4.com&cd=6#v=onepage&q=big%20brother%20bb4.com&f=false
  |isbn = 1-57870-274-7}}</ref><ref>{{cite book
  |last = Mancil
  |first = Tony
  |authorlink = Tony Mancill
  |title = Linux routers: a primer for network administrators
  |publisher = [[Prentice Hall PTR]]
  |year = 2002
  |page = 248
  |url = https://books.google.com/books?id=0LBQAAAAMAAJ&q=big+brother+bb4.com&dq=big+brother+bb4.com&lr=&cd=19
  |isbn = 0-13-009026-3}}</ref> [[computer security]],<ref>{{cite book
  |last = Andrés
  |first = Stephen
  |authorlink = Steven Andrés
  |title = Security Sage's guide to hardening the network infrastructure
  |publisher = [[Syngress]]
  |year = 2004
  |page = 252
  |url = https://books.google.com/books?id=mlsr0D3fbFIC&lpg=PA253&dq=big%20brother%20bb4.com&pg=PA252#v=onepage&q=big%20brother%20bb4.com&f=false
  |isbn = 1-931836-01-9}}</ref>
<ref>{{cite book
  |last = Northcutt
  |first = Stephen
  |authorlink = Stephen Northcutt
  |title = Inside network perimeter security
  |publisher = [[New Riders Publishing|New Riders]]
  |year = 2003
  |page = 678
  |url = https://books.google.com/books?id=x_1C2AROPbQC&q=big+brother+bb4.com&dq=big+brother+bb4.com&cd=8
  |isbn = 978-0-672-32737-7}}</ref> and [[Computer network|networking]].<ref>{{cite book
  |last = Mauro
  |first = Douglas
  |authorlink = Douglas R. Mauro
  |title = Essential SNMP
  |publisher = [[O'Reilly Media]]
  |year = 2005
  |page = 400
  |url = https://books.google.com/books?id=65_0d25EpB4C&pg=PA400&dq=big+brother+bb4.com&cd=2#v=onepage&q=big%20brother%20bb4.com&f=false
  |isbn = 0-596-00840-6}}</ref>

The application supports [[Redundancy (engineering)|redundancy]] via multiple displays as well as [[failover]]. Network elements can be tested from multiple locations and users can write custom tests.

An [[open source]] version of the project exists: between 2002 and 2004 it was called '''bbgen toolkit''', between 2005 and 2008 it was called '''Hobbit''', but to avoid breach of trademark, it was renamed [[Xymon]].<ref>http://www.xymon.com/xymon/help/about.html</ref>

== Background ==
Sean MacGuire wrote Big Brother in 1996 after he received what he believed to be an overpriced quote for network-monitoring software. He introduced it in an article for ''Sys Admin'' magazine in October 1996.<ref>{{Citation
  | last = MacGuire
  | first = Sean
  | title = Big Brother: A Web-based Unix Network Monitoring and Notification System
  | pages = 1–6
  | magazine = Sys Admin
  | date = October 1996 }}</ref>
In August 1997, it was mentioned in an article by Paul Sittler in ''[[Linux Journal]]''<ref>{{Citation
  | last = Sittler
  | first = Paul
  | title =  Big Brother Network Monitoring System
  | url = http://www.linuxjournal.com/article/2225
  | magazine = Linux Journal
  | date = August 1, 1997 }}</ref>
Shortly after the initial release, Robert-Andre Croteau joined MacGuire and added notification rules, which he described in a ''Sys Admin'' article published in September 1998,<ref>{{Citation
  | last = Croteau
  | first = Robert-Andre
  | title =  BBWARN: A Notification Extension for Big Brother
  | pages = 1–6
  | magazine = Sys Admin
  | volume = 7
  | issue = 9
  | date = September 1998 }}
</ref> and created the [[Microsoft Windows|Windows]] version.

In 1999 MacGuire and Croteau started the company BB4 Technologies http://bb4.com, to commercialize Big Brother.  They licensed the product under what they called the "Better than Free" or BTF license - "better" because 10% of the license fee went to the charity of the purchaser's choice.<ref name="charities">{{Cite web
  |last=MacGuire
  |first=Sean
  |authorlink=Sean MacGuire
  |title=better than free / charities
  |year=2001
  |url=http://maclawran.ca/~sean/bb-dnld/charity.html
  |accessdate=4 March 2010
  |archiveurl=https://web.archive.org/web/20010212095439/http://maclawran.ca/~sean/bb-dnld/charity.html
  |archivedate=12 February 2001}}</ref>
In 2001 Quest Software acquired BB4 Technologies.<ref name="Big Brother Purchase">{{cite web
| url         = http://findarticles.com/p/articles/mi_m0EIN/is_2002_March_13/ai_83728629/ bnet
| title       = Quest Software Acquires BB4 Technologies, Creators of Big Brother System and Network Monitor; Acquisition Strengthens Quest Software's Monitoring Business
| accessdate  = 2010-03-03
| date        = 2002-03-13
| work        = Press release
| publisher   = Business Wire
}}</ref>
MacGuire and Croteau, the only employees of BB4, later went to work at [[Quest Software]] and continued to work on the product.  The Big Brother Professional Edition (BBPE) was released shortly thereafter. In January 2012, MacGuire left Quest software and is no longer associated with the product he created. Quest Software was acquired by Dell in 2012.

== Versions ==

There are two versions of Big Brother available: the BTF version (source-code visible), and the pre-compiled, fully commercial, professionally supported Big Brother Professional Edition (BBPE). In 2009, they released the "Big Brother - Modern Edition," an [[Adobe Flash]]-based display for Big Brother, and formally added graphing and trend monitoring support.

== Testing ==

* Network services - Any TCP network service can be tested for availability, including ([[Internet Control Message Protocol|ICMP]] (Ping), [[Hypertext Transfer Protocol|HTTP]], [[Post Office Protocol|POP3]], [[Simple Mail Transfer Protocol|SMTP]], [[File Transfer Protocol|FTP]], [[Secure Shell|SSH]])
* System Information including ([[central processing unit|processor]] 5-minute load average, [[hard disk drive|disk]] usage, messages critical) on all versions of UNIX, Linux and Windows [[operating system]]s, via native clients.
* [[Simple Network Management Protocol|SNMP]] tests and traps are supported natively.
* Custom tests, generally as [[Bash (Unix shell)|bash scripts]], although other languages such as [[Perl]] are supported.

== References ==
{{reflist}}

== External links ==
{{wikibooks|System Monitoring with Xymon}}
* [http://www.bb4.com Official Big Brother site]
* [http://www.bb4.org Big Brother BTF version website]
* [http://www.deadcat.net Deadcat—thousands of community-donated plugins/addons]
* [http://quest.bb4.com/bb/level1/bb.html Live demonstration of Big Brother]
* [http://bb4.org/audio/background.mp3 Audio history of Big Brother by Sean MacGuire]
* [http://bb4.org/audio/notification.mp3 Audio history of the Notification subsystem by Robert-Andre Croteau]
* [http://bb4.org/audio/deadcat.mp3 Audio history of deadcat.net by Sean MacGuire and Robert-Andre Croteau]

=== Additional publications ===
* [http://www.usenix.org/publications/login/2000-8/features/monitoring.html :login;] ''System and network monitoring using Big Brother''
* [http://www.itmanagement.com/features/10-open-source-network-tools-052407/ IT Management] IT Management: ''10 great open source network tools''
* [http://www.techrepublic.com/article/big-brother-is-watching-your-network/ Techrepublic] ''Big Brother is watching your network''
* [http://www.techrepublic.com/article/automate-your-network-monitoring-process/ Techrepublic] ''Automate your network monitoring process''
* [http://sunsite.uakom.sk/sunworldonline/swol-07-1998/swol-07-security.html Sun World] ''Report from SANS '98''
* [http://www.patentstorm.us/patents/6347339.html Cisco Patent] US Patent 6347339 ''Detecting an active network node using a login attempt''
* [http://www.networkcomputing.de/netzwerk/server-clients/artikel-4549.html networkcomputing.de] ''Big Brother fürs Netzwerk''

[[Category:Internet Protocol based network software]]
[[Category:Network management]]