{{Infobox journal
| title = Journal of Automated Reasoning
| cover = 
| editor = [[Tobias Nipkow]]
| discipline = [[Computer science]]
| abbreviation = J. Autom. Reason.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = 8/year
| history = 1983-present
| openaccess = 
| license =
| impact = 0.714
| impact-year =2011
| website = http://www.springer.com/computer/theoretical+computer+science/journal/10817
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=0168-7433&issue=current
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 263592661
| LCCN = sf93093541
| CODEN = JAREEW
| ISSN = 0168-7433
| eISSN = 1573-0670
}}
The '''''Journal of Automated Reasoning''''' was established in 1983 by [[Larry Wos]] who was its [[editor in chief]] until 1992.<ref>{{citation |url=https://books.google.com/books?id=BG6uMcioN7EC&pg=PR7 |title=Automated reasoning and its applications |author=Robert Veroff}}</ref> It covers research and advances in [[automated reasoning]] &mdash; mechanical verification of [[theorem]]s and other deductions in classical and non-classical [[logic]].<ref>{{citation |url=https://books.google.com/books?id=9IFMCsQJyscC&pg=SA61-PA24 |title=Computer science handbook |author=Allen B. Tucker}}</ref>

The journal is published by [[Springer Science+Business Media]]. As of 2010, the editor-in-chief is Tobias Nipkow. The journal's 2011 [[impact factor]] is 0.714, and it is indexed by several science indexing services, including the [[Science Citation Index Expanded]] and [[Scopus]].<ref>{{cite web |title=Journal of Automated Reasoning |url=http://www.springer.com/computer/theoretical+computer+science/journal/10817 |publisher=Springer|accessdate=2 August 2010}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official|1=http://www.springer.com/computer/theoretical+computer+science/journal/10817}}

[[Category:Computer science journals]]
[[Category:Logic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1983]]
[[Category:Logic in computer science]]
[[Category:Formal methods publications]]
[[Category:Springer Science+Business Media academic journals]]