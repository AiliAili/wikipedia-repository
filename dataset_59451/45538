{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Francis James Ralph
| image         = 
| caption       = 
| birth_date    = {{Birth date|1892|12|9|df=y}}
| death_date    = {{Death date and age|1918|9|3|1892|12|9|df=y}}
| birth_place   = [[Cheltenham]], Gloucestershire, England
| death_place   = [[Pas-de-Calais]], France{{KIA2}}
| placeofburial_label = 
| placeofburial = [[Villers-Bretonneux]] Military Cemetery, [[Somme (department)|Somme]], France
| placeofburial_coordinates = {{coord|49|53|13|N|2|30|43|E|display=inline,title}}
| nickname      = 
| allegiance    = United Kingdom
| branch        = British Army<br />Royal Air Force
| serviceyears  = 1915–1918
| rank          = [[Second Lieutenant]]
| unit          = [[No. 20 Squadron RAF]]
| commands      = 
| battles       = 
| awards        = [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
| relations     = 
}}
Second Lieutenant '''Francis James Ralph''' {{post-nominals|country=GBR|DFC}} (9 December 1892 – 3 September 1918) was a British World War I [[flying ace|ace]] credited with 13 confirmed aerial victories over German fighter aircraft while flying as an [[Air observer|aerial observer]].<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/ralph.php |title=Francis James Ralph |work=The Aerodrome |year=2015 |accessdate=31 October 2015 }}</ref>

==Early life==
Francis James Ralph was born in [[Cheltenham]] on 9 December 1892. After completion of his education, he worked as a [[draughtsman]] for the Co-op Gas Company in [[Thurlaston, Leicestershire]], before his enlistment in 1915.{{sfnp|Guttman|Dempsey|2007|pp=53–54}}

==Military career==
From cadet he was appointed a temporary second lieutenant (on probation) on 12 February 1918, on the General List to serve in the [[Royal Flying Corps]].<ref>{{London Gazette |date=1 March 1918 |supp=yes |issue=30556 |startpage=2764}}</ref> He was confirmed in his rank as an observer officer on 27 May,<ref>{{London Gazette |date=18 June 1918 |issue=30752 |startpage=7210 |endpage=7211 |nolink=yes}}</ref> by which time the Army's Royal Flying Corps and the Royal Naval Air Service had merged to form the Royal Air Force. By then Ralph was already  serving as a gunner/observer in the rear seat of a [[Bristol F.2 Fighter]] in No. 20 Squadron, {{sfnp|Guttman|Dempsey|2007|pp=53–54}} as on 9 May, flying with pilot Lieutenant David Smith, he had gained his first victory, shooting down in flames a [[Fokker Dr.I]] west of [[Lille]]. He then went on to fly with a number of pilots. On 31 May, he and Lieutenant Leslie Capel destroyed an [[Albatros D.V]] north of [[Laventie]]. Then, flying with Lieutenant [[William McKenzie Thomson|William Thomson]], he destroyed a [[Pfalz D.III]] over [[Comines, Nord|Comines]]–[[Comines-Warneton|Houthem]] on 9 June, and drove down out of control a [[Fokker D.VII]] over [[Boezinge]] on 17 June, a victory shared with Captain [[Dennis Latimer]] & Lieutenant [[Tom Cecil Noel|Tom Noel]]. On 26 June, for his fifth victory, which made him an ace, he drove down a Pfalz D.III over [[Armentières]] with Lieutenant [[Paul Thayer Iaccaci|Paul Iaccaci]]. From then on all his victories were made with Captain [[Horace Percy Lale|Horace Lale]] in the pilot's seat. They scored two Fokker D.VII's north of Comines on 25 July, one shared with Lieutenant Thomson & Sergeant D. D. C. Summers and Lieutenants Smith & John Hills, and on 14 August drove down a  Fokker D.VII over Dadizele. Late on 21 August they accounted for a Pfalz D.III, then an Albatros D.V, and two Fokker D.VII north-east of [[Geluwe]], the first Fokker being shared with Captain Latimer & Sergeant [[Arthur Ernest Newland|Arthur Newland]] and Lieutenant John Colbert & 2nd Lieutenant [[Harold Leslie Edwards|Harold Edwards]]. Ralph's 13th and final victory came on 3 September, driving down a Fokker D.VII south of Havrincourt Wood, but appears to have come at the cost of his life as he died the same day.

His award of the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] was [[gazetted]] posthumously on 2 November 1918. His citation read:
; Distinguished Flying Cross
: Second Lieutenant Francis James Ralph.
:: "A brave and skilful observer who has taken part in many combats with enemy aircraft, invariably displaying cool courage and presence of mind. On a recent occasion our patrol of eleven machines, after bombing a railway junction, was attacked by fifteen enemy scouts. One of these, which was engaged by this officer's pilot, crumpled and fell. The formation was then attacked by seven Fokkers from above, one of which was shot down by 2nd Lt. Ralph."<ref>{{London Gazette |date=1 November 1918 |supp=yes |issue=30989 |startpage=12971 |nolink=yes}}</ref>

Ralph was originally buried in the Cemetery of the Military Hospital at [[Dury, Somme|Dury]], near Amiens, but in 1927 his remains were exhumed and re-interred at the Military Cemetery at [[Villers-Bretonneux]].<ref name="CWGC">{{cite web |url=http://www.cwgc.org/find-war-dead/casualty/576605/RALPH,%20FRANCIS%20JAMES |title=Casualty Details: Francis James Ralph |work=[[Commonwealth War Graves Commission]] |year=2015 |accessdate=31 October 2015}}</ref>

==References==
; Notes
{{reflist|2}}
; Bibliography
* {{cite book |ref=harv |title=Bristol F2 Fighter Aces of World War I |first1=Jon |last1=Guttman |first2=Harry |last2=Dempsey |location=Oxford, UK |publisher=Osprey Publishing |year=2007 |isbn=978-1-84603-201-1 |lastauthoramp=yes}}

{{DEFAULTSORT:Ralph, Francis James}}
[[Category:1892 births]]
[[Category:1918 deaths]]
[[Category:People from Cheltenham]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:British military personnel killed in World War I]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]