{{About|the journal|the philosophical term|Metaphilosophy}}
{{Infobox journal
| title         = Metaphilosophy
| cover         = 
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = 
| subject       = [[Metaphilosophy]]
| peer-reviewed = 
| language      = English
| editor        = Armen T. Marsoobian
| publisher     = [[Wiley-Blackwell|Wiley]]
| country       = 
| history       = 1970-present
| frequency     = 
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| ISSNlabel     = 
| ISSN          = 
| eISSN         = 1467-9973
| CODEN         = 
| JSTOR         = 
| LCCN          = 
| OCLC          = 49883085
| website       = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291467-9973
| link1         = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291467-9973/issues
| link1-name    = Online archive
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}
'''''Metaphilosophy''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering [[metaphilosophy]]. It is abstracted and indexed by [[PhilPapers]] and the [[Philosopher's Index]].<ref>[http://philpapers.org/journals PhilPapers Publications List]</ref><ref>[http://philindex.org/downloads/PIC_Alphabetical_Coverage.pdf Philosopher's Index Publications List]</ref>

''Metaphilosophy'' was established in 1970 by [[Terry Bynum]] and Richard Reese.<ref>Gross, N., ''Richard Rorty: The Making of an American Philosopher'' (Chicago University Press, ), p. 150, footnote 13.</ref> "Metaphilosophy" was given a working definition in the first issue of the journal as "the investigation of the nature of philosophy, with the central aim of arriving at a satisfactory explanation of the absence of uncontested philosophical claims and arguments."<ref>Lazerowitz, M., "A Note on 'Metaphilosophy'", ''Metaphilosophy'', vol. 1, no. 1 (1970), p. 91.</ref> The journal is published by [[John Wiley & Sons]] and the [[editor-in-chief]] is Armen T. Marsoobian ([[Southern Connecticut State University]]).<ref>[http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-9973/homepage/EditorialBoard.html Metaphilosophy - Editorial Board - Wiley Online Library]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-9973}}
* [http://philpapers.org/pub/672 PhilPapers listing for ''Metaphilosophy'']

[[Category:Metaphilosophy]]
[[Category:Philosophy journals]]
[[Category:Publications established in 1970]]