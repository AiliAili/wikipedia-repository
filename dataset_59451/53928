{{redir|Whispering Gallery|the magazine|Dorothy Dunnett Society#Publications and Communications}}
[[file:St Paul's Cathedral Whispering Gallery.jpg|thumb|The Whispering Gallery of St Paul's Cathedral]]

A '''whispering gallery''' is usually a circular, hemispherical, elliptical or ellipsoidal enclosure, often beneath a [[dome]] or a [[Vault (architecture)|vault]], in which [[Whispering|whispers]] can be heard clearly in other parts of the gallery. Such galleries can also be set up using two parabolic dishes. Sometimes the phenomenon is detected in caves.

== Theory ==

A whispering gallery is most simply constructed in the form of a circular wall, and allows [[whispering|whispered]] communication from any part of the internal side of the circumference to any other part. The sound is carried by waves, known as [[whispering-gallery wave]]s, that travel around the circumference clinging to the walls, an effect that was discovered in the whispering gallery of [[St Paul's Cathedral]] in London.<ref>[https://archive.org/stream/scientificpapers05rayliala#page/n638/mode/1up Lord Rayleigh, The problem of the whispering gallery, Philos. Mag. 20, 1001,1910.]</ref> The extent to which the sound travels at St Paul's can also be judged by clapping in the gallery, which produces four echoes.<ref>[http://mag.digitalpc.co.uk/Olive/ODE/physicsworld/LandingPage/LandingPage.aspx?href=UEhZU1dvZGUvMjAxMi8wMi8wMQ..&pageno=MzM.&entity=QXIwMzMwMA..&view=ZW50aXR5 O. Wright, Gallery of Whispers, Physics World 25, No. 2, Feb. 2012, p. 31.]</ref> Other historical examples<ref name=raman>[[C. V. Raman]], [http://repository.ias.ac.in/69841/1/69841.pdf On whispering galleries, Proc. Indian Ass. Cult. Sci. 7, 159, 1921-1922.]</ref><ref name=sab>[https://archive.org/details/collectedpaperso00sabi W. C. Sabine, Collected Papers on Acoustics (Harvard University Press, Cambridge MA) 1922, p. 255.]</ref><ref name=Peiping>Peiping (American National Red Cross, American Red Cross Embassy Club) 1946, p. 17.</ref> are the [[Gol Gumbaz]] mausoleum in Bijapur and the Echo Wall of the [[Temple of Heaven]] in Beijing. A [[sphere|hemispherical]] enclosure will also guide whispering gallery waves. The waves carry the words so that others will be able to hear them from the opposite side of the gallery.

The gallery may also be in the form of an [[ellipse]] or [[ellipsoid]],<ref name=sab /> with an accessible point at each [[focus (optics)|focus]]. In this case, when a visitor stands at one focus and whispers, the line of sound emanating from this focus reflects directly to the focus at the other end of the gallery, where the whispers may be heard. In a similar way, two large concave [[parabolic loudspeaker|parabolic dishes]], serving as [[acoustic mirror]]s, may be erected facing each other in a room or outdoors to serve as a whispering gallery, a common feature of science museums. Egg-shaped galleries, such as the [[Golghar]] Granary at Bankipore,<ref name=raman/> and irregularly shaped smooth-walled galleries in the form of caves, such as the [[Ear of Dionysius]] in Syracuse,<ref name=sab /> also exist.

== Examples ==
{{example farm|date=August 2015}}
{{Refimprove section|date=January 2012}}

=== India ===

* The [[Gol Gumbaz]] in [[Bijapur, Karnataka|Bijapur]], [[India]].<ref name=raman/>
* The [[Golghar]] Granary in [[Bankipore]], [[India]].<ref name=raman/>
* The [[Victoria Memorial (India)|Victoria Memorial]] in [[Kolkata]].<ref name=raman/>

=== United Kingdom ===

* [[St Paul's Cathedral]] in [[London]] is the place where [[whispering-gallery wave]]s were first discovered by [[Lord Rayleigh]] {{circa|1878}}.<ref>Lord Rayleigh, Theory of Sound, vol. II, 1st edition, (London, MacMillan), 1878.</ref>
* The library of [[Dollar Academy]] in Scotland.
* The entrance gallery of the Aston Webb Great Hall at the [[University of Birmingham]].
* [[Hamilton Mausoleum]] in [[Hamilton, South Lanarkshire]], [[Scotland]].

=== United States ===

* [[The Battle House Hotel]] in [[Mobile, Alabama]] has a whispering arch in the front lobby.
* [[Cincinnati Museum Center at Union Terminal]].
* [[Grand Central Terminal]] in [[New York City]]: the gallery in front of the [[Oyster Bar]] restaurant.
* The [[Mapparium]] at The Mary Baker Eddy Library in Boston allows visitors to enter the interior of a reflecting surface forming a nearly complete sphere.
* A whispering gallery can be found on the main floor of the [[Museum of Science and Industry (Chicago)]].
* [[National Statuary Hall|Statuary Hall]] in the [[United States Capitol]].<ref name=raman/>
* [[Salt Lake Tabernacle]] in [[Salt Lake City]], [[Utah]].<ref name=raman/>
* The rotunda at [[San Francisco City Hall]].
* Curved stone benches on either side of the [[Smith Memorial Arch]] in [[Fairmount Park]], [[Philadelphia, Pennsylvania]].
* Centennial fountain in front of Green Library at [[Stanford University]] in [[California]]
* The rotunda of the [[Texas State Capitol]] and the [[Missouri State Capitol]].
* Gates Circle, Buffalo, New York.{{citation needed|date=April 2014}}
* The Whispering Arch in [[Union Station (St. Louis)|St. Louis Union Station]]<ref name=waymarking>Waymarking.com, [http://www.waymarking.com/waymarks/WM8T35_St_Louis_Union_Station_Whispering_Arch_St_Louis_Missouri St. Louis Union Station Whispering Arch]</ref>

=== Other parts of the world ===

* [[Barossa Reservoir]], [[Williamstown, South Australia]].
* [[Cathedral of Brasília]] in [[Brazil]].
* [[Martello tower]]s.
* The Echo Wall in the [[Temple of Heaven]] in [[Beijing]].<ref name=Peiping />
* [[Masjed-e Imam]] in [[Esfahān]], [[Iran]].
* [[Basilica of St. John Lateran]], [[Rome]].<ref name=sab />
* [[Santa Maria del Fiore]], [[Florence Cathedral]]
* The [[Church of the Holy Sepulcher]], [[Jerusalem]].
* [[Leaning Tower of Nevyansk]], [[Sverdlovsk Oblast]].
* [[Selimiye Mosque (Edirne)|Selimiye Mosque]] in [[Edirne]], [[Turkey]].
* [[St. Peter's Basilica]] in the [[Vatican City]].
* [[Monument to the Negev Brigade]] in [[Beersheba]], [[Israel]].
* The Salle de Cariatides in the [[Louvre]], [[Paris]], [[France]].<ref name=sab />
* The [[Treasury of Atreus]], [[Greece]]
* Secret's Chamber in [[El Escorial]] in [[Madrid]], [[Spain]].
* The Whispering Gallery in the [[Alhambra]] in [[Granada]], [[Spain]].
* Cleopatra's Bath in the [[Siwa Oasis]], [[Egypt]].
* [[Ear of Dionysius]] cave in [[Province of Syracuse|Syracuse]], [[Sicily]].<ref name=sab />
* [[Meštrović Pavilion]] in [[Zagreb, Croatia]]
* [[Royal BC Museum]] in [[Victoria, British Columbia, Canada]]
* [[Sacred Heart Cathedral, Wellington]], New Zealand
* The Town Ball, [[Christchurch]], New Zealand
* The Whispering Arch (Flüsterbogen) in [[Görlitz]], [[Germany]].

== Other uses ==

The term 'whispering gallery' has been borrowed in the physical sciences to describe other forms of [[whispering-gallery wave]]s such as [[light]] or [[matter wave]]s.{{citation needed|date=August 2015}}

== See also ==

* [[Acoustic mirror]]
* [[Parabolic loudspeaker]]
* [[Room acoustics]]
* [[Whispering-gallery wave]]

== References ==

{{reflist}}

== External links ==

* [http://www.sonicwonders.org/?p=1086 Ear of Dionysius]: visiting information, videos and sounds of this cave.
* [http://www.sonicwonders.org/?p=426 Grand Central Station]: visiting information, videos and sounds of the whispering gallery.
* [http://www.sonicwonders.org/?p=57 St Paul's Cathedral]: visiting information, videos and sounds of the whispering gallery.

[[Category:Acoustics]]
[[Category:Rooms]]