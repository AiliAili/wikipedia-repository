{{Infobox military person   
| name          = Kevin Lacz
| image         = KevinLacz.jpg {{!}}border
| caption       = Lacz in April 2014
| birth_date    = {{Birth date and age|1981|12|26|mf=yes}}
| birth_place   = [[Meriden, Connecticut]], U.S.
| nickname      = "Dauber"
| birth_name    = Kevin Lacz 
| allegiance    = {{flagdeco|United States|1960|size=23px}} United States
| branch        = {{flag|United States Navy|size=23px}}
| serviceyears  = 2002&ndash;2010
| rank          = SO1 (SEAL)
| unit          = [[File:U.S. Navy SEALs Special Warfare insignia.png|25px]] [[United States Navy SEALs|U.S. Navy SEALs]]
*[[SEAL Team 3]]
| commands      =
| battles       = [[Iraq War]]
| awards        = 
[[File:Bronze Star Medal ribbon with "V" device, 1st award.svg|23px|border]] [[Bronze Star Medal]]<br/>[[File:Navy and Marine Corps Commendation ribbon.svg|23px|border]] [[Commendation Medal|Navy and Marine Corps Commendation Medal]] (2)<br/>[[File:Navy and Marine Corps Achievement ribbon.svg|23px|border]] [[Achievement Medal|Navy and Marine Corps Achievement Medal]] (2)<ref name=MiddletownPress>{{cite news|url=http://www.middletownpress.com/general-news/20140401/middlefield-native-navy-seal-playing-himself-in-eastwoods-american-sniper|title=Middlefield native, Navy SEAL playing himself in Eastwood’s ‘American Sniper’|author=Gecan, Alex|date=April 1, 2014|accessdate=June 15, 2016|work=The Middletown Press}}</ref>
|relations= Peter Lacz (father)<br />Marlene Lacz (mother)<br />Mark Lacz (brother)<br />Michael Lacz (brother)<br />Children: 2
| spouse        = {{marriage|Lindsey Lacz|2009|}}
| laterwork     = ''The Last Punisher'' (2016)<ref name=TheLastPunisher>{{cite book|title=The Last Punisher|date=July 2016|publisher=Threshold Editions|authors=Kevin Lacz, Ethan E. Rocke & Lindsey Lacz|url=http://books.simonandschuster.com/The-Last-Punisher/Kevin-Lacz/9781501127243|isbn=978-1501127243}}</ref>
| signature     = 
}}

'''Kevin “Dauber” Lacz''' is a [[United States Navy SEAL]] veteran who served two tours in the [[Iraq War]] with [[SEAL Team 3]]. His platoon's 2006 deployment to [[Ramadi]] has been discussed in several books, including [[Dick Couch]]’s ''The Sheriff of Ramadi'', Jim DeFelice’s ''Code Name: Johnny Walker'', and [[Chris Kyle]]'s [[New York Times best-selling]] autobiography, [[American Sniper (book)|''American Sniper'']]. Lacz’s presence in the book led to his involvement in the production of and eventual casting in the [[Clint Eastwood]]-directed [[American Sniper|Oscar-winning biopic]] of the same name (starring [[Bradley Cooper]]).<ref name=TheDailyBeast>{{cite web|last1=Stern|first1=Marlow|title=The ‘American Sniper’ I Knew: Kevin Lacz on Fellow Navy SEAL Chris Kyle and Movie Criticisms|url=http://www.thedailybeast.com/articles/2015/02/12/the-american-sniper-i-knew-kevin-lacz-on-fellow-navy-seal-chris-kyle-and-movie-criticisms.html|website=The Daily Beast|accessdate=15 June 2016}}</ref>

== Early life ==

Lacz was born in [[Meriden, Connecticut]], where he lived until the age of 12, at which point he moved to [[Middlefield, Connecticut]]. He attended [[Xavier High School (Connecticut)|Xavier High School]] in [[Middletown, Connecticut]], an all-male private Catholic school, graduating in 2000. He enrolled at [[James Madison University]] in 2000 to pursue a career in medicine.<ref name=MiddletownPress></ref>

== Military career ==

When the terrorist attacks of [[September 11, 2001]] claimed the life of a good friend’s father, Lacz decided to leave school in favor of the military. A [[SEALs]] poster on the wall at a [[United States Navy|Navy]] recruiter’s office inspired Lacz to enlist in the [[United States Navy|Navy]] with orders for [[BUD/S|Basic Underwater Demolition SEAL school (BUD/S)]].<ref name=MiddletownPress></ref>

He started [[BUD/S]] Class 245, but suffered a back injury that forced him to roll back to the next class. He went on to graduate with [[BUD/S]] Class 246 in 2003. As a [[Hospital Corpsman]], Lacz also attended 18-D Special Operations Combat Medic School at [[Fort Bragg]] before checking into [[SEAL Team 3]] in [[Coronado, California]]. Soon after, he attended [[United States Army Sniper School|Army Sniper School]] and returned to Charlie Platoon where he began preparing for his 2006 deployment with [[Chris Kyle]], Marc Lee, Ryan Job, and [[Mike Monsoor]] (who was in the same Task Unit, but from Delta Platoon).<ref name=TheLastPunisher></ref>

In 2006, Lacz deployed to [[Ramadi, Iraq]] with Charlie Platoon, Task Unit Bruiser.<ref name=MiddletownPress></ref> The work he did as a platoon sniper and medic contributed to his task unit becoming the most highly decorated special operations unit of the [[Iraq War]].<ref>{{cite web|last1=Feloni|first1=Richard|title=A former Navy SEAL officer reveals the 11-point checklist he used to prepare for combat|url=http://www.businessinsider.com/former-navy-seal-checklist-combat-2015-10|website=Business Insider|accessdate=16 June 2016}}</ref> Lacz personally conducted numerous sniper overwatches, direct action missions, raids, and tribal engagements in support of the effort to halt the spread of violence through [[Ramadi]]. For his actions on his 2006 deployment, including acquiring numerous enemy kills and braving enemy fire to carry a fallen comrade to safety, Lacz was awarded a [[Bronze Star Medal]] with a [["V" Device|Combat ‘V.’]]<ref name=MiddletownPress></ref>

[[File:KevinLacz1.JPG|thumb|left|Kevin Lacz and Chris Kyle at a SEAL Team 3 awards ceremony in Coronado, CA, on October 7, 2007]]

In 2008, Lacz returned to [[Iraq]], this time as a member of Delta Platoon. Joining [[Chris Kyle]] for another deployment, his focus was the Iraqi border with [[Syria]] and the interception of foreign fighters trying to infiltrate the country.<ref name=TheDailyBeast></ref>

As a [[SEAL]], Lacz gained extensive experience in Special Operations Combat Medicine (SOCM), Special Operations Dive Chamber Medicine, Military Free-Fall HALO and HAHO Operations, Long-Range Target Interdiction Sniper work, Survival Evasion Resistance Escape (SERE) Training, Battlefield Interrogations, Close Quarters Combat (CQC), Counter-Terrorism Operations, and Naval Special Warfare Lead Breaching Operations.<ref name=IMDb>{{cite web|title=Kevin Lacz Biography|url=http://www.imdb.com/name/nm6373651/bio|website=IMDb|accessdate=16 June 2016}}</ref>

In addition to his [[Bronze Star Medal|Bronze Star]], Lacz was awarded two [[Commendation Medal|Navy and Marine Corps Commendation Medals]] and two [[Achievement Medal|Navy and Marine Corps Achievement Medals]], among other awards.<ref name=MiddletownPress></ref>

== Post-military life ==

Upon completing his enlistment, Lacz was honorably discharged from the [[United States Navy|Navy]]. He returned to [[Connecticut]] and enrolled at the [[University of Connecticut]] with the intention of continuing his career in medicine. He graduated [[magna cum laude]] with a degree in political science in 2011 and began the application process for [[physician assistant]] school.<ref>{{cite web|last1=Ross|first1=Dalton|title=UConn Graduate Kevin Lacz Plays Himself in “American Sniper”|url=http://whus.org/2015/02/15/kevin-lacz-plays-himself-american-sniper/|website=WHUS Radio|accessdate=16 June 2016}}</ref> In 2012, Lacz moved his family to [[Winston-Salem, North Carolina]] to pursue his Masters of Medical Sciences at [[Wake Forest University]]. He graduated in August 2014.<ref>{{cite web|title="American Sniper" Actor: PA Grad and Former Navy SEAL|url=http://www.wakehealth.edu/Academic-Programs/Features/PA-Grad-in-American-Sniper.htm|website=Wake Forest School of Medicine|accessdate=16 June 2016}}</ref>

Because of his close working and personal relationship with [[Chris Kyle]], Lacz was asked to contribute to his autobiography, ''[[American Sniper (book)|American Sniper]]'', through interviews. [[Chris Kyle|Kyle]] also discussed Lacz frequently when referencing the 2006 and 2008 deployments (as “Dauber”), laying the groundwork for Lacz’s involvement in the production.<ref>{{cite book|author1=Kyle, Chris |author2=McEwen, Scott |author3=DeFelice, Chris |title=American Sniper|publisher=Harper Collins|date=February 5, 2012|ISBN=0-06-208235-3}}</ref> He was hired to provide [[SEAL]] technical advising for the film. [[Bradley Cooper]] also convinced [[Clint Eastwood]] to allow Lacz to audition for the role of “Dauber,” after which he was cast to play himself.<ref>{{cite web|last1=Tedder|first1=Michael|title=Bradley Cooper on ‘American Sniper’: Training Was Basically A Science Experiment|url=http://variety.com/2014/scene/vpage/bradley-cooper-on-american-sniper-training-was-basically-a-science-experiment-1201380951/|website=Variety|accessdate=16 June 2016}}</ref>

Lacz is the spokesperson for Hunting for Healing, a 501(c)(3) charitable organization he founded with his wife, Lindsey. After taking a trip to [[Africa]] together in May 2015, the couple decided to launch a charity with a mission to take service-disabled veterans and their spouses on hunting, fishing, and outdoor excursions.<ref>{{cite web|title=Who We Are|url=http://www.huntingforhealing.org/about-us.html|website=Hunting for Healing|accessdate=16 June 2016}}</ref>

Lacz was appointed by [[Rick Scott|Governor Rick Scott]] of [[Florida]] to the District Board of Trustees for [[Pensacola State College]] in 2015.<ref>{{cite web|title=Governor Rick Scott Appoints Two to Pensacola State College District Board of Trustees|url=http://www.flgov.com/2015/03/31/governor-rick-scott-appoints-two-to-pensacola-state-college-district-board-of-trustees/|website=Rick Scott, 45th Governor of Florida}}</ref> Lacz is also a professional speaker represented by [[Greater Talent Network (GTN)]].<ref>{{cite web|title=Why Kevin Lacz Is Captivating Audiences|url=http://www.greatertalent.com/kevinlacz/|website=Greater Talent Network}}</ref> His memoir about his 2006 deployment to [[Ramadi]], ''The Last Punisher'', will be published on July 12, 2016.<ref name=TheLastPunisher></ref>

Currently, Lacz works as a physician assistant in northwest [[Florida]] and southeastern [[Alabama]], where he is co-owner of Regenesis, a lifestyle and performance medicine medical practice.<ref>{{cite web|title=Regenesis|url=http://www.ilifestylegulfcoast.com/services/Regenesis|website=iLifestyle Gulf Coast|accessdate=16 June 2016}}</ref> His past military service influences him greatly as he also seeks to actively support service members and veterans by tending to their medical needs.

==References==
{{reflist|30em}}
{{authority control}}
{{DEFAULTSORT:Lacz, Kevin}}
[[Category:1981 births]]
[[Category:Living people]]
[[Category:American military personnel of the Iraq War]]
[[Category:Combat medics]]
[[Category:United States Navy sailors]]
[[Category:University of Connecticut alumni]]
[[Category:United States Navy SEALs personnel]]
[[Category:Wake Forest University alumni]]
[[Category:People from Meriden, Connecticut]]
[[Category:People from Middlefield, Connecticut]]