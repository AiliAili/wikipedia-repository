{{Orphan|date=December 2013}}

{{Infobox writer <!-- For more information see [[:Template:Infobox Writer/doc]]. --> 
| name          = Lal Singh Dil <!-- Deleting this line will use the article title as the page name. -->
| image         = Lal-singh-dil-photo-by-amarjit-chandan 4.jpg 
| image_size    = 
| alt           = Photograph of Punjabi revolutionary poet Lal Singh Dil
| caption       = at Samrala, 1993 by Amarjit Chandan
| pseudonym     = 
| birth_name    = Lal Singh
| birth_date    =  {{Birth date |1943|04|11|df=yes}}  
| birth_place   = Ghungrali Sikhãn , a village near [[Samrala]], Punjab,  [[British India]]
| death_date    = {{Death date and age|2007|08|14|1943|04|11|df=yes}}  
| death_place   = [[Ludhiana]],Indian Punjab
| resting_place = 
| occupation    = Poet, wage labourer, watchman, farm worker, cook , tea vendor 
| language      = Punjabi
| nationality   = 
| ethnicity     = Belonged to an ‘outcaste’ community of tanners ([[chamar]]), subscribed to [[Sikhism]], converted to [[Islam]] later under the name Muhammad Bushra  
| citizenship   = Indian
| education     = Passed High school, studied for a college degree, for a teachers’ training course and an honours course in Punjabi literature but completed none of these
| alma_mater    = 
| period        = 
| genre         = 
| subject       = 
| movement      = 
| notableworks  = Three collections of poetry - ''Setluj Di Hawa'', ''Bahut Sarrey Suraj'', and ''Satthar'',  autobiography, ''Dastaan'', and a long poem, ''Aj Billa Phir Aaya''
| spouse        = Ummarried
| partner       = 
| children      = 
| relatives     = 
| influences    = 
| influenced    = 
| awards        = honoured and awarded by small town literary organizations of Indian Punjab
| signature     = 
| signature_alt = 
| website       = <!-- www.example.com -->
| portaldisp    = 
}}

'''Lal  Singh Dil''' (Punjabi ਲਾਲ ਸਿੰਘ ਦਿਲ) (11 April 1943 – 14 August 2007) was one of the major revolutionary Punjabi poets emerging out of the [[Naxalite]] (Maoist-Leninist) Movement  in the [[Indian Punjab]] towards the late sixties of the 20th century. The Movement was a political failure and died down quickly, but it brought in revolutionary changes in the subject matter, language and idiom, tone and tenor of [[Punjabi poetry]]. Referring to the impact of the Naxalite Movement in Punjab, [http://department.gndu.ac.in/department/socio/paramjit_judge.pdf Paramjit S. Judge] says, ‘The consequences of the Naxalite movement have been almost ephemeral and have hardly made an impact on the social and political spheres… Its positive contribution is that it has revolutionized Punjabi poetry which can never be traditional and romantic again.’ <ref>Judge 1992, p.156</ref> 
'The prominent poets belonging to this school are: Pash, Lal Singh Dil, Harbhajan Halvarvi, Darshan Khatkar, Amarjit Chandan and [[Sant Ram Udasi]],’ says Paramjit S Judge.<ref>Judge 1992, p.154</ref>  About Lal Singh Dil, [http://www.hum.leiden.edu/lias/organisation/south-asian-tibetan/ramr.html%20 Prof Ronki Ram] says, ‘He was one of the most popular and serious poets of the Naxal Movement in Punjab of the late 1960s.' 
<ref>Ronki Ram,15 August 2007</ref>

== Biography ==

=== Birth and family background ===
Lal Singh Dil was born on11 April in 1943 in a [[Ramdasia]] [[Chamar]], (an ‘outcaste’ community of tanners) family in Ghungrali Sikhãn (Punjabi text: 'ਜਨਮ ਸਥਾਨ: ਘੂੰਗਰਾਲੀ ਸਿੱਖਾਂ')<ref name="Dilsee">Dil 1998,see “About the Author” (ਲੇਖਕ ਬਾਰੇ) by publisher/editor pp.221-22</ref> near [[Samrala]], a small town in Punjab ([[Malwa, Punjab|Malwa]] region) in then [[British India]], now Indian Punjab.  His was a family without money, without land, without education, without any financial and intellectual resources that could give Lal Singh a start for upward social or economic mobility. The family was fitted to perform only manual and menial agricultural labour, and Lal Singh’s father, almost throughout his life, worked as an agricultural labourer on someone’s land. The family,like most of the Ramadasia community in the Malwa region of Punjab, formally subscribed to [[Sikhism]].<ref>Ghai,2 November 2011</ref>{{full|date=September 2015}}

=== Education ===
Lal Singh Dil passed his high school examination from the Government School, Samrala in 1960-61,being the first in his clan to pass tenth standard, while working as a wage labourer, and go to college.<ref>Dutt 2012, Introduction p.xxxiv</ref> He joined A.S. College at another town, [[Khanna, Ludhiana|Khanna]], close by, but dropped out after one year. He joined Junior Teachers’ Training course in 1964 at SHS College in Bahilolpur, another close-by town, but gave up after two years without completing the course. He spent a year studying for Gayani, an honours course in [[Punjabi literature]], but gave up without completing the course. During this period Lal Singh Dil supported himself by working as a wage labourer and herder, and by giving tuitions. (Punjabi text:'ਵਿੱਦਿਆ: ਮੈਟਿ੍ਕ (1960–61) ਸਰਕਾਰੀ ਹਾਈ ਸਕੂਲ, ਇਕਸਾਲ ਏ. ਐਸ ਕਾਲਜ ਖੱਨਾ ਵਿਚ ਪੜਿਆ, ਦੋ ਸਾਲ ਸ.ਹ.ਸ ਬਹਿਲੋਲਪੁਰ `ਚ ਜੂਨੀਅਰ ਬੇਸਿਕ ਟ੍ਰੇਨਿੰਗ ਕਰਦਿਆਂ ਲਾਏ,ਇਕ ਸਾਲ ਗਿਆਨੀ ਦੀ ਤਿਆਰੀ ਕਰਦਿਆਂ ।…ਪੜਦਿਆਂ ਖੇਤ ਮਜ਼ਦੂਰੀ ਕੀਤੀ, ਰਾਜ ਮਿਸਤ੍ਰੀਆਂ ਨਾਲ ਦਿਹਾੜੀ ਕੀਤੀ…')  <ref name="Dilsee" />

=== Discrimination ===
Right from his childhood he faced discrimination because he belonged to a ‘[[lower caste]]’. This is what he writes in the opening paragraph of his autobiography, ''Dastaan'': ‘I have endured the ordeal by fire time and again in my life, and it is a miracle that I have been able to emerge unscathed.’<ref>Dutt 2012, p.3</ref>  What that fire was he illustrates with an example in the next few lines of the same opening paragraph. While as a small boy of  five or six, out of innocence, he dares to bathe at  a [[jat]] farmer’s well. He is immediately dragged away, whip-lashed thrice and, driven out by the farmer’s son. As   a ‘chamar’ he is not permitted to bathe at the well of an upper caste. He is pushed into this ‘fire’ again and again, at school, at college where he dares to fall in love with an upper caste girl, and even within the egalitarian Naxalite movement.‘Lal Singh Dil was born in a 'chamar' family. Reference to this occurs at many places in his poetry. In his autobiography he has written freely about the prevalence of the arrogance of caste superiority in his locality, at his school, and in the Naxalite organizations and among the police,’ says Amarjit Chandan. (Punjabi text:‘ਲਾਲ ਸਿੰਘ ਦਿਲ ਚਮਾਰਾਂ ਦੇ ਘਰ ਜੰਮਿਆ । ਇਹ ਗੱਲ ਇਹਦੀ ਕਵਿਤਾ ਵਿਚ ਕਈ ਥਾਈਂ ਆਈ ਹੈ । ਸਵੈਜੀਵਨੀ ਵਿਚ ਇਹਨੇ ਆਪਣੇ ਵਿਹੜੇ, ਸਕੂਲ ਤੇ ਨਕਸਲੀ ਪਾਰਟੀ ਅਤੇ ਫੇਰ ਪੁਲਸ ਦੇ ਜ਼ਾਤ ਅਭੀਮਾਨ ਬਾਰੇ ਖੁੱਲ਼ ਕੇ ਲਿਖਿਆ ਹੈ’)<ref>Chandan 1998, p.6</ref>

=== In the Naxalite Movement ===
Lal Singh was introduced to the [[Marxist ideology]] by one Comrade Jagjit Singh Baghi of his own town in 1968 (Punjabi text:'ਪ੍ਰੇਰਨਾ: ਸਾਹਿਤ ਤੇ ਸਮਾਜਵਾਦੀ ਸੋਚ ਦੀ ਪਹਿਚਾਣ 1968 `ਚ ਕਾਮਰੇਡ ਜਗਜੀਤ ਸਿੰਘ ਬਾਗੀ, ਸਮਰਾਲਾ ਨੇ ਕਰਾਈ ।')<ref>Dil 1998, see “About the Author” (ਲਖਕ ਬਾਰੇ) by publisher/editor pp.221-22</ref> This is how he himself describes his enthusiasm for the Naxalite movement: ‘The news of Naxalbari spread like wildfire. Those days I was working as a labourer on daily wages. I carried heavy loads up and down a ladder, and all this activity gave me strange energy. I felt now I would be able to accomplish what I could have achieved had I been present during the upsurge in Vietnam. I felt I was on the threshold of realizing the imminent Revolution.<ref>Dutt 2012, P.53</ref>  He took part in a Naxlite agitation (Birla Seed Farm Agitation,<ref>Judge 1992, pp.81-82</ref> [[Ropar]])  in 1969 and later, in the same year, was part of a group of Naxalites who unsuccessfully raided the police station in the town of [[Chamkaur]] on 30 April 1969.<ref>	Judge 1992, p.82</ref> He fled from the scene but was soon arrested and faced severe police torture during a long police remand. He was tried and sentenced to six months imprisonment with hard labour, in his own words (Punjabi text:‘ਛੇ ਮਹੀਨੇ ਦੀ ਬਾਮੁਸ਼ੱਕਤ ਕੈਦ…’).<ref>Dil 1998, p.115</ref> He remained in jail for sometime between middle of 1969 and 1971. After release from jail in 1971, fearing police persecution, facing neglect from family, friends and comrades, and lacking support from any quarter he fled to [[Uttar Pradesh]], another Indian province. ‘Pushed out of Punjab, Lal Singh left for Uttar Pradesh towards the end of 1971. The main reasons for running away from Punjab were his disillusionment with the political Movement and the fear of police persecution.’ (Punjabi text:‘ਲਾਲ 1971 ਦੇ ਅਖੀਰ ਪੰਜਾਬ ਦਾ ਧੱਕਿਆ ਯੂ.ਪੀ ਚਲੇ ਗਿਆ। ਓਥੇ ਜਾਨ ਦਾ ਵਡਾ ਕਾਰਨ ਸਿਆਸੀ ਲਹਿਰ ਤੋਂ ਹੋਇਆ ਮੋਹਭੰਗ ਸੀ ਤੇ ਪੁਲਿਸ ਦਾ ਡਰ ਵੀ।‘ )<ref name="Chandan">Chandan 1998, p.8</ref>

=== In Uttar Pradesh ===
In Uttar Pradesh he moved from one town or village to another, seeking one or another kind of subsistence employment about which he talks extensively in his autobiography. His friend, and story writer, [[Prem Parkash]] says: ‘I received one letter from [[Lakhimpur Kheri]]. It was evident from his letters, many of which are lost, that sometimes he worked with the [[Imam]] in the mosque, and sometimes he was caretaker of the factory, sometimes he looked after the orchards, sometimes he became a cloth vendor moving from village to village. He also lived in a timber godown.’ <ref>Dutt 2012, see Foreword by Prem Parkash](1998).p.xiii . (translated from Punjabi by Nirupama Dutt)</ref> Sometime in 1972 he had converted to [[Islam]] because he believed there was no caste discrimination in Islam; and also in the hope of marrying, a hope that did not materialize. This is what he says to his friend Amarjit Chandan in an undated long letter written in 1973: ‘…First of all let me tell you that I have converted to Islam. This happened about a year ago.’ (Punjabi text:'... ਸਭ ਤੋਂ ਪਹਿਲਾਂ ਮੈਂ ਦਸੱ ਦਿਆਂ  ਕਿ ਮੈਂ  ਮੁਸਲਮਾਨ ਹੋ ਗਿਆ ਹਾਂ । ਇਸ ਨੂੰ ਸਾਲ ਹੋ ਗਿਆ ...'  )<ref>Chandan 1998, p.9</ref>

While he was a student at college he had become enamoured of a few girls but it all remained a one-sided affair, his caste always acting as the insurmountable barrier. In Uttar Pradesh, after his conversion to Islam, he hoped his caste would not come in the way and he would be able to find a woman who would be willing to marry him. But this never happened. This is what Prem Parkash writes: ‘Lal has not written about any love affair with a woman in his memoirs, he has only given indications of his fantasies. He even talks in abstractions. Comrades accuse him of several things but I am sure he has never seen a woman unclothed.’<ref>Dutt 2012, see Foreword by Prem Parkash p.xvi (1998) (Translated from Punjabi by Nirupama Dutt)</ref> Even while in Uttar Pradesh he did not give up writing poetry. He was in contact with [[Urdu]] poets  and wrote  many [[Ghazals]] in Urdu, but he continued to write in Punjabi. ‘Between 1972 and 1983 I came into contact with Urdu poets of the town [[Mohammadi, Uttar Pradesh|Mohammadi]],’ writes Dil. (Punjabi text:‘ਬਹੱਤਰ ਤੋਂ ਤਿਰਿਆਸੀ ਤਕ ਮੈਂ ਕਸਬਾ ਮੁਹਮੰਦੀ ਦੇ ਉਰਦੂ ਸ਼ਾਇਰਾਂ ਦੇ ਸੰਪਰਕ `ਚ ਆਇਆ ।‘)<ref>Dil 2006, P.30</ref> ‘While here, in Uttar Pradesh, he wrote his poems published as ''Bahut Sarrey Suraj'' (So many Suns) ’ (Punjabi text:‘ਓਥੇ ਰਹਿੰਦਿਆਂ ਇਹਨੇ 'ਬਹੁਤ ਸਾਰੇ ਸੂਰਜ'  ਵਾਲੀਆਂ ਕਵਿਤਾਵਾਂ ਲਿਖਿਆਂ ।‘)<ref name="Chandan" /> in 1982.

=== Back home and a tea vendor at a bus station,death ===
He returned to his home town Samrala sometime in 1983. Like a devout Muslim he continued to say his daily [[namaaz]] but also remained addicted to drinking. He kept on writing poetry under his old name, but he could not find any regular employment or source of income. ‘The comrades of his revolutionary days were now editors, executives, professors, businessmen or expatriates. The spring thunder was over and everyone had returned to the comfort zone of their class structures. Dil had his kachcha home in the run-down Kang Mohalla or Chamarian (a chamar ghetto), as some referred to it. He was at a lose end and did not know what to do,’ writes Niupama Dutt. He did receive some financial support from a few friends. Finally, he ended up as a tea vendor at a bus terminal close to his hometown, Samarala.<ref>Dutt 2012, Introduction p.xxii</ref>  He died in 2007 in a hospital in Ludhiana, an addict, perhaps mentally unstable, and a sick man.<ref>Ronki Ram, 5 August 2007</ref>

== Writings ==

=== Poetry and autobiography ===
Lal Singh Dil had started writing poetry even while at school.Some of his poems were published  in well-known Punjabi magazines, [http://rananayar.wordpress.com/2009/10/18/of-%E2%80%98little-magazines%E2%80%99-in-punjabi-a-historical-overview/%20 ''Lakeer'', ''Preetlari'' and ''Nagmani''], even before his first collection of poetry was published.  He published three collections of poetry:  ''Setluj Di Hawa''  (Breeze from  the [[Sutlej]]) 1971; ''Bahut Sarey Suraj'' (So  Many  Suns) 1982; and ''Satthar'' (A Sheaf) 1997. A collected volume of all his poems titled ''Naglok''  (The World of the [[Nāga]]s) was published in 1998 and 2007. He also wrote a long poem titled ‘''Aj Billa Phir Aaya''’ (Billa Came Again Today), which was published in 2009, posthumously.

He published his autobiography, ''Dastaan'', in 1998, containing a Foreword by Amarjit Chandan and an Afterword by Prem Parkash.

Although he had converted to Islam,he continued to publish his works under his pre-conversion name.

== Translations of his works ==
I  English translations of five of his poems were published  in the '[http://'Modern%20Poetry%20in%20Translation(MPT) 'Modern Poetry in Translation(MPT)]'', Third Series Number Eighteen, ''Transitions'', in 2012.

II  An English translation by [[Nirupama Dutt]] of his autobiography, ''Dastaan'', along with his 20 poems, has been published as: ''[http://www.penguinbooksindia.com/en/content/poet-revolution Poet of the Revolution: The Memoirs and Poems of Lal Singh Dil'' (Viking Penguin, 2012)<nowiki>]</nowiki>.]

== About his poetry ==
His contemporary revolutionary poets, [[Pash]] foremost among them, sang of a revolution round the corner, using blood and thunder imagery to denounce, frighten and challenge the ‘[[class enemies]]’, predicting the imminent fall of the ‘[[comprador]] [[Bourgeoisie|bourgeois state]]’. Lal Singh Dil shared their optimism, but in most of his poetry he remained a poet of the understatement. His poetry is revolutionary in another very important sense. It focuses, for the first time in Punjabi poetry, on the lives of men and women and children who are absolutely at the lowest rungs of the Indian society, the social and economic out-castes - the [[Dalits]], the landless labourers and farm workers, the daily wagers, and many nomadic and wandering 'non-Aryan' tribes (who he believes were the original inhabitants of India, and whom he calls the ''Naglok'' through the title of the collected work of his poems) conquered by the invading [[Aryan]]s; the people to be exploited and humiliated, to be used and abused and kept at the margins of everything decent and worthwhile. His poetry is a narrative of the wretched of the earth told by one of them.<ref>Ghai 2012, pp.50-51</ref>

Dil was not a great reader of books.He just did not have the means to buy or access many books or other written materials. His understanding of life, of history, religion and society, has been mostly picked up from his environment and experience, which makes his poetry distinctive and different, for it has a quality of the raw, the unformed and the [[People|folk]]. His poetry gives voice to the voiceless, the ignored and in their language, through their unsophisticated mixture of truth, prejudice, anger, bitterness and humour, desire for revenge, and for transcendence as in [[fairy tales]].

== Awards and  recognition ==
During the heyday of the Naxalite Movement in Punjab  people came in their hundreds to listen to him and other revolutionary poets. He was honoured and rewarded by a number of small town literary organizations in Punjab, but received no award of national importance.

== Notes ==
<references />

== References ==
•	Judge, Paramjit S.  ''Insurrection to Agitation: The Naxalite Movement in Punjab''. Popular Prakashan. 1992. ISBN 81 7154 527 0

•	Ronki Ram (Dr). “Lal Singh Dil (April 11, 1943 – August 14, 2007)” (an obituary)   
http://www.ambedkartimes.com/ronkiram.htm#lal . 15 August 2007

•	Dil, Lal Singh.  ''Naglok'' (Poems). Chetna Prakashan 1998. ISBN 81-7883-341-7 (Punjabi). see “About the Author” (ਲੇਖਕ ਬਾਰੇ) by publisher/editor pp.&nbsp;221–22

•	Ghai, Trilok Chand] “Lal Singh Dils’ ''Dastaan'': An Autobiography of Absences”. (A Review)  http://ghai-tc.blogspot.in/2011/11/lal-singh-dils-dastaan.html .  2 November 2011

•	Dutt, Nirupama (tr.). ''Poet of the Revolution: The Memoirs and Poems of Lal Singh Dil''. Penguin Viking. (2012).  ISBN 9780670086559

•	Chandan, Amarjit.1998:  “A Complete Story of an Incomplete Journey” (ਅਧੂਰੇ ਸਫ਼ਰ ਦੀ ਪੂਰੀ ਦਾਸਤਾਨ), Introduction. Dil, Lal Singh. ''Dastaan: An autobiography (ਦਾਸਤਾਨ: ਸਵੈ-ਜੀਵਨੀ)''. Chetna Prakashan, Ludhiana (Punjabi).

•	Dil, Lal Singh. ''Dastaan: An autobiography (ਦਾਸਤਾਨ: ਸਵੈ-ਜੀਵਨੀ)''. Chetna Prakashan, Ludhiana.1998. p.&nbsp;115
•	
•	Dil, Lal Singh. “My Urdu Milieu” (ਮੇਰਾ ਉਰਦੂ ਮਾਹੌਲ). Tarsem s. Dr.(ed.) (ਤਰਸੇਮ ਐਸ. ਡਾ.)  ''Lal Singh Dil: Biographical and Critical Essays (ਲਲ ਸਿਂਘ ਦਿਲ: ਸੰਕਲਪ ਤੇ ਸਮੀਖਿਆ)'' Lokgeet Prakashan,Chandigarh.2006. P30. ISBN 81-7142-025-7  (Punjabi)

•	Ghai, Trilok Chand. 2012. “Lal Singh Dil, five poems, translated from Punjabi”, ''Modern Poetry in Translation'', Third series – Number Eighteen, Transitions. pp.&nbsp;50–51 ISBN 978-0-9572354-0-3

== External links ==
* [http://lalsinghdil.wordpress.com/ Lal Singh Dil’s Weblog]
* [http://www.google.co.in/search?q=lal+singh+dil+poetry&hl=en&safe=off&tbo=u&tbm=isch&source=univ&sa=X&ei=0bwU Lal Singh Dil images]
* {{YouTube|3rvCC9AJnhw|A clip from the film ''Kitte mil ve mahi'', Containing a fascinating conversation with the poet}}. by [http://ajaybhardwaj.in/%20%20%20 Ajay Bhardwaj]
* [http://ghai-tc.blogspot.in/2010/11/lal-singh-dil.html The Women of Kudeli, a poem by Dil in English translation]
* [http://apnaorg.com/articles/review_of_poet_of_the_revolution/index.shtml Life and  poetry of  a wandering heart: A Review of Nirupama Dutt’s translation of Dil’s autobiography]
* [http://sikhspectrum.com/2012/08/sadhu-sunddar-singh-re-examining-his-case/ Rich Heritage of Punjabi Dalit Literature and its Exclusion from Histories: Dil in the context of Dalit Literature]

== Further reading ==
* Tarsem S  (Ed.) (2006): ''Lal Singh Dil : Sankalp te  Samikhiya'' (biographical and critical essays) ; Lokgeet Parkashan, ISBN 81-7142-025-7  (Punjabi) 
* Lal Singh Dil: ''Naglok'' (Poems) (with critical essays by Punjabi Scholars)  Chetna Prakashan, Ludhiana 2007 (Punjabi) ISBN 81 7883 341 7

{{DEFAULTSORT:Dil, Lal Singh}}
[[Category:Punjabi poets]]
[[Category:1943 births]]
[[Category:2007 deaths]]
[[Category:Naxalite–Maoist insurgency]]