{{orphan|date=March 2014}}
{{Infobox Journal
| title             = Communicatio Socialis
| subtitle          = Journal for Media Ethics and Communication in Church and Society
| editors           = Klaus-Dieter Altmeppen, Andreas Büsch, [[Alexander Filipović]]
| discipline        = Media ethics, communication and media sciences
| abbreviation      = ComSoc
| language          = German
| publisher         = Matthias-Grünewald-Verlag der Schwabenverlag AG
| country           = [[Germany]]
| history           = 1968-present
| frequency         = Quarterly
| executive editor  = Renate Hackel-de Latour
| website           = http://ejournal.communicatio-socialis.de/
| link1             = http://ejournal.communicatio-socialis.de/index.php/cc/issue/archive
| link1-name        = Online archive
| ISSN              = 0010-3497
| eISSN             = 2198-3852
| zdb               = 215821-8
| CODEN             = 
}}

'''Communicatio Socialis''' is a specialized communication and media sciences publication bearing the subtitle “Journal for Media Ethics and Communication in Church and Society”. It addresses communication and [[Media ethics|media ethical]] issues in addition to topics related to “Religion, Church and Communication”. Whereas media ethics is usually understood as a philosophical ethical field and thereby as a branch of philosophy, the publication applies a more broadly defined concept of media ethics and includes, in addition to philosophy, theoretical and empirical works as well as contributions from the social sciences.<ref name="Communicatio Socialis">cf. [http://ejournal.communicatio-socialis.de/index.php/cc/about/editorialPolicies#focusAndScope self-concept of journal Communicatio Socialis]</ref>

The journal is published on a quarterly basis by the [[Matthias-Grünewald-Verlag]] (a part of the consortium Schwabenverlag AG). The individual issues are approximately 120 pages in length. In addition to the print editions the articles also appear as e-journals.<ref name="EJournal der Zeitschrift Communicatio Socialis">[http://ejournal.communicatio-socialis.de/ EJournal of the journal Communicatio Socialis]</ref>

== EJournal ==
With the media ethical realignment as of issue 3/4 (2013) the publication also provides its content on-line.  New issues are published there simultaneously with the appearance of the print issues. Within the first 12 months of publication texts appear subject to a charge. At the end of these 12 months all content of the publication is freely accessible.([[Open access]]).

== Purpose and objective ==
The journal understands the ongoing media transition as an ethical challenge. With the mediatization and digitalization of social communication the relevance of critical and ethical reflection in these fields increases. According to its self-understanding, the central theme of Communicatio Socialis should be an interdisciplinary and “defining site for media ethical discussion and research.".<ref>Altmeppen, Klaus-Dieter; Büsch, Andreas; Filipović, Alexander: Medienethik als Aufgabe und Verpflichtung. Zur Neuausrichtung von Communicatio Socialis. In: Communicatio Socialis 46/2013. issues #3/#4, S. 280–287. Online available at http://ejournal.communicatio-socialis.de/index.php/cc/article/view/64, S. 281</ref>

The media ethical orientation of the periodical stands in keeping with a world-view characterized by the Christian faith. Media ethics is understood as a Christianly motivated contemporaneous service to society. The subject area “Religion, Church and Communication”, respectively Roman Catholic-related journalism, is the second thematic priority of the journal upon which, until 2013, the primary focus lay.<ref name="Communicatio Socialis" />

== History ==
In 1968 Franz-Josef Eilers, in connection with Michael Schmolke<ref name="MS" /> and [[Bergmoser + Höller Verlag#Karl R. Höller|Karl R. Höller]] founded the professional journal for communication in religion, Church and society “Communicatio Socialis”. The title of the publication is derived from the title of the conciliar decree Decretum de instrumentis Communicationis socialis „[[Inter mirifica]]“''. This was the first decree of the Catholic Church concerning means of communication.<ref>Cf. Schmolke, Michael: Franz-Josef Eilers wurde 75. Eine Collage als Hommage für den Gründer von „Communicatio Socialis“. In: Communicatio Socialis. Internationale Zeitschrift für Kommunikation in Religion, Kirche und Gesellschaft. 40/2007, issue #3.</ref> 
From its theological orientation, the journal stands for the spirit of this Council and feels committed to its declarations and ecumenical perspective.<ref>Eilers, Franz-Josef: Publizistik als Aufgabe. In: Communicatio Socialis 1/1968, issue #1, p. 1–5, especially p. 3.</ref> 
The founder, Franz-Josef Eilers SVD, saw a special task for the journal in the collection and summarization of news concerning events in the sphere of church-related journalism throughout the world. Communicatio Socialis should serve all those, who feel committed towards the journalistic tasks and responsibilities as a source of information and to stimulate discussion. Recently, Communicatio Socialis has developed in the direction of a general communications and media science publication with a media-ethical and ecclesiastic-religious focus.

From 1968 until 1993 the journal bore the subtitle “Publication for Journalism in the Church and World”. In the issue 26/1993 this changed to “International Publication for Communication in Religion, Church and Society”. With the double-issue 3/4 (2013) the journal underwent a media-ethical realignment and now bears the subtitle “Publication for Media Ethics and Communication in the Church and Society”.
Further information concerning the history of the journal can be found in volume 45/2012, Number 4.<ref>Schmolke, Michael: Abschied und Dank. Ein Herausgeber blickt auf 45 Jahrgänge zurück. In: Communicatio Socialis 45/2012, Heft 4, S. 341–346. Online available at http://ejournal.communicatio-socialis.de/index.php/cc/article/view/30</ref>

== Publishers and editors ==

List of previous and current publishers:
* [[Bergmoser + Höller Verlag#Karl R. Höller|Karl R. Höller]] (since its founding, issues 01/1968 until 35/2002)
* Franz-Josef Eilers SVD (since 01/1968 until 35/2002)
* Michael Schmolke<ref name="MS">Cf. [http://www.salzburg.com/wiki/index.php/Michael_Schmolke article in the Salzburg-Wiki]</ref> (since 01/1968 until 45/2012)
* Kees Verhaak (since 04/1971 until 16/1983)
* Peter Düsterfeld (since 22/1989 until 26/1993)
* Reinhold Jacobi (since 26/1993 until 35/2002)
* [[Walter Hömberg]] (since 36/2003 until 43/2010)
* Matthias Kopp (since 36/2003 until 37/2004)
* Ute Stenert (since 38/2005 until 45/2012)
* Klaus-Dieter Altmeppen (since 44/2011 until now)
* [[Alexander Filipović]] (since 45/2012 until now)
* Andreas Büsch (since 46/2013 until now)

The members of the current editorial team are Renate Hackel-de Latour (Executive Editor), Annika Franzetti, Petra Hemmelmann, Christian Klenk and Christoph Sachs.

== External links ==
* [http://d-nb.info/01126389X Title data in the catalogue of the German National Library]
* [http://ejournal.communicatio-socialis.de Publication Ejournal]

== References ==
{{Reflist}}

[[Category:Ethics journals]]
[[Category:Quarterly journals]]