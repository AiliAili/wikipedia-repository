<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=M 35
 |image=Messerschmitt 35.png
 |caption=
}}{{Infobox Aircraft Type
 |type=Two seat sports plane
 |national origin=[[Germany]]
 |manufacturer=Bayerische Flugzeugwerke (BFW)
 |designer=[[Willy Messerschmitt]]
 |first flight=1933
 |introduced=1934
 |retired=
 |status=
 |primary user=
 |number built=15
 |developed from=[[BFW M.23|M.23]]
 |variants with their own articles=
}}
|}

The '''BFW M.35''', sometimes known as the '''Messerschmitt M 35''', was a [[Germany|German]] sports plane of the early 1930s. It was the last of a line designed by [[Willy Messerschmitt]].

==Development==
Over the period 1927-33 Messerschmitt designed a series of six  sports plane planes, the single seat [[BFW M.17|M.17]] and [[BFW M.19|M.19]], and the two-seat [[BFW M.23|M.23]], [[BFW M.27|M.27]] [[BFW M.31|M.31]] and finally the '''M.35'''.<ref>{{harvnb|Smith|1971|pp=18–34}}</ref> With the exception of the M.23, none sold in large numbers. They were all single-engine low-wing [[Cantilever#In aircraft|cantilever]] [[monoplane]]s with open [[cockpit]]s and fixed [[Landing gear|undercarriage]]. The M.35 kept the extended [[fuselage]] of the M.27 and combined it with an undercarriage of single leg, spatted form.<ref name="JRS">{{harvnb|Smith|1971|pp=33–4}}</ref>

Two different engines were used. The M35a had a 112&nbsp;kW (150&nbsp;hp), seven-cylinder [[Radial engine|radial]] [[Siemens Sh 14]]a and the M.35b a 100&nbsp;kW (135&nbsp;hp) four-cylinder [[Inline engine (aviation)|inline]] inverted air cooled [[Argus As 8]]b. The former was the shorter and faster of the two. The aircraft first flew in 1933.<ref name="JRS"/>

==Operational history==
The aircraft was first shown to the public and potential buyers at the 1934 ''Aerosalon'' in Geneva. In that year, [[Rudolph Hess]] won the ''Zugspitz'' trophy in a M.35. In 1934-1935, [[Wilhelm Stör]] won the German Aerobatic Championship in a M.35b, and in 1935 the women's prize was taken by [[Vera von Bissing]] in a similar machine.<ref name="JRS"/>

Despite these successes and strong performances at other venues in the late 1930s, only 15 M.35s were built, 13 registered in Germany, one in Spain<ref>http://www.goldenyears.ukf.net/</ref> and reputedly one in Romania.<ref name="HistMess"/> Though the M.35a was faster, the M.35b was commoner; only two M.35as are definitely identified.   
<!-- ==Variants== -->

==Operators==
;{{flag|Spain|1931}}
*[[Spanish Republican Air Force]]<ref name="RAPL">[http://www.ejercitodelaire.mde.es/stweb/ea/ficheros/pdf/F2C87BA1ED6FEF12C1257C7E004EA9CD.pdf Rafael Ángel Permuy López, ''La identificación y denominación de los aviones militares españoles'' (1911 - 1936)]</ref>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (M.35a)==
{{aerospecs
|ref={{harvnb|Smith|1971|p=34}}
|met or eng?=met
|crew=2
|capacity=
|length m=7.48
|length ft=24
|length in=6
|span m=11.57
|span ft=37
|span in=11⅓
|height m=2.75
|height ft=9
|height in=0
|wing area sqm=<ref name="HistMess">http://www.histaviation.com/Messerschmitt_M_35.html.</ref> 17.0
|wing area sqft=183
|empty weight kg=500
|empty weight lb=1,102
|gross weight kg=800
|gross weight lb=1,764
|eng1 number=1
|eng1 type=[[Siemens Sh 14]]a 7-cylinder radial 
|eng1 kw= 110 
|eng1 hp=150
|eng2 number=
|eng2 type=
|eng2 kw= 
|eng2 hp=
|max speed kmh=230
|max speed mph=143
|cruise speed kmh=<ref name="HistMess"/> 195
|cruise speed mph=122
|stall speed kmh=
|stall speed mph=
|range km=700
|range miles=435
|endurance h=
|endurance min=
|ceiling m=<ref name="HistMess"/> 5,800
|ceiling ft=19,000
|climb rate ms=<ref name="HistMess"/> to 1,000 m 5.1
|climb rate ftmin=994
}}

==See also==
{{aircontent
|see also=
|related=
|similar aircraft=
|lists=*[[List of aircraft of the Spanish Republican Air Force]]
}}

==References==
{{commons category|BFW M.35}}
;Citations
{{reflist}}

;Cited sources
*{{cite book |title= Messerschmitt an aircraft album|last=Smith|first = J Richard|year =1971|publisher = Ian Allan |location = London|isbn=0-7110-0224-X|ref=harv}}
{{refbegin}}
{{refend}}
<!-- ==External links== -->

{{Messerschmitt aircraft}}
{{aerobatics}}

[[Category:German sport aircraft 1930–1939]]
[[Category:Messerschmitt aircraft|M 35]]
[[Category:Aerobatic aircraft]]