{{Infobox journal
| title = Bulletin of Engineering Geology and the Environment
| cover = [[File:BEGEcover.png]]
| editor = A. B. Hawkins 
| discipline = [[Geoscience]]
| abbreviation = Bull. Eng. Geol. Environ.
| publisher = [[Springer Science+Business Media]] on behalf of the [[International Association of Engineering Geology and the Environment]]
| country =
| frequency = Quarterly
| history = 1970–present
| openaccess = 
| impact = 0.667
| impact-year = 2011
| website = http://www.springer.com/journal/10064
| link1 = http://www.springerlink.com/content/1435-9529
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 768021032
| LCCN = 
| CODEN = 
| ISSN = 1435-9529
| eISSN = 1435-9537
}}
The '''''Bulletin of Engineering Geology and the Environment''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering research in [[geoscience]]. It is the official journal of the [[International Association of Engineering Geology and the Environment]] and published on their behalf by  [[Springer Science+Business Media]].<ref name="Springer">{{cite web |url=http://www.springer.com/earth+sciences/journal/10064 |title=Bulletin of Engineering Geology and the Environment |format= |work= |accessdate=2009-05-23}}</ref><ref name="IAEG">{{cite web|url=http://www.iaeg.info/Bulletin/tabid/56/Default.aspx |title=International Association of Engineering Geology and the Environment - Bulletin |format= |work= |accessdate=2009-05-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20090430113229/http://www.iaeg.info:80/Bulletin/tabid/56/Default.aspx |archivedate=2009-04-30 |df= }}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.667.<ref name=WoS>{{cite book |year=2013 |chapter=Bulletin of Engineering Geology and the Environment |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-23 |series=[[Web of Science]] |postscript=.}}</ref>

== Subjects covered ==
Areas of research frequently published in ''Bulletin of Engineering Geology and the Environment'' include: study and solution of engineering and environmental problems arising from the interaction between [[geology]] and the activities of humanity. [[Geomorphology]], structure, [[stratigraphy]], [[lithology]], and [[groundwater]] conditions of geological formations; characterization of the mineralogical, physico-geomechanical, chemical and hydraulic properties of earth materials involved in construction, [[resource recovery]] and environmental change; assessment of the mechanical and hydrological behavior of soil and rock masses; prediction of changes to the above properties with time; determination of the parameters to be considered in analyzing the stability of engineering works and earth masses; maintenance of the environmental condition and properties of the terrain.<ref name="Springer"/>

==See also==
*[[Engineering Geology (journal)|''Engineering Geology'']]

== References ==
{{reflist}}

== External links ==
* {{Official|http://www.springer.com/journal/10064}}

{{DEFAULTSORT:Bulletin Of Engineering Geology And The Environment}}
[[Category:English-language journals]]
[[Category:Publications established in 1970]]
[[Category:Geology journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]