'''Industrial Developments International''' (IDI) is a privately held [[Real_estate_investment_trust|real estate investment trust (REIT)]]. In 2013, the company was valued at $1.8 billion, with nearly 7 million square feet under development in nine states<ref name="comappeal">{{cite web |url=http://www.commercialappeal.com/news/2013/may/06/logistics-firm-signs-lease-for-117181-square-in/|date=2013-05-06|title=Logistics firm signs lease for 117,181 square feet in Olive Branch|publisher=The Commercial Appeal|accessdate=2013-06-25}}</ref> and ownership or interests in millions of square feet of investment grade assets.<ref name="miramar">{{cite web |url=http://atlanta.citybizlist.com/article/idi-makes-waves-35236-sf-lease-miramar-centre|date=2013-05-09|title=IDI Makes Waves with 35,236 SF Lease at Miramar Centre|publisher=CityBizList|accessdate=2013-06-25}}</ref> In addition to leasing and investment services, IDI also constructs industrial facilities in the U.S., Canada and Mexico.<ref name="miramar"/>

{{Infobox company
| name             = IDI
| logo             = 
| caption          =
| trading_name     = <!-- d/b/a/, doing business as - if different from legal name above -->
| native_name      = <!-- Company's name in home country language -->
| native_name_lang = <!-- Use ISO 639-2 code, e.g. "fr" for French. If there is more than one native name, in different languages, enter those names using {{tl|lang}}, instead. -->
| romanized        =
| former type      = 
| type             = 
| traded_as        = 
| industry         = [[Industrial real estate development]]
| genre            = <!-- Only used with media and publishing companies -->
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = {{Start date|1989}}
| founder          = 
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[Atlanta]], [[Georgia (U.S. state)|Georgia]]
| location_country = U.S.
| locations        = 
| area_served      = {{ubl|[[Atlanta]]|[[Chicago]]|[[Cincinnati]]|[[Dallas]]|[[South Florida]]|[[Los Angeles]]|[[Memphis]]|[[Northern New Jersey]]}}
| products         = 
| production       = 
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| aum              = <!-- Only used with financial services companies -->
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|www.brookfieldlogisticsproperties.com}}
| footnotes        = 
| intl             =
| bodystyle        =
}}

== History ==

IDI was established in 1989 in Atlanta by Henry "Greg" Gregory and a few partners, including former president and chief executive officer Timothy Gunter.<ref name="abc">{{cite web |url=http://www.bizjournals.com/atlanta/stories/2007/12/10/story9.html?page=all|title=Developer, conservationist Gregory retiring from IDI|date=2007-12-10|publisher=Atlanta Business Chronicle|accessdate=2013-04-20}}</ref><ref name="nrei">{{cite web |url=http://nreionline.com/mag/real_estate_thinking_big|title=Thinking Big|date=2000-04-01|publisher=National Real Estate Investor|accessdate=2013-04-21}}</ref> The company started with $35 million in investments from Japan-based parent company, [[Kajima]], and a bridge loan from [[First Union National Bank]], now [[Wells Fargo]].<ref name="nrei"/> IDI was one of the first companies to approach industrial development from a national standpoint to meet customers' needs.<ref name="nreibig">{{cite web |url=http://nreionline.com/mag/real_estate_idi_reaps_big|title=IDI reaps big rewards from fundamental vision|date=1996-02-01|publisher=National Real Estate Investor|accessdate=2013-04-20}}</ref>

In 2013, IDI was acquired by [[Brookfield_Asset_Management|Brookfield Asset Management, Inc.]] (BAM). In 2014, IDI and Gazeley, a European industrial real estate company, were co-branded by Brookfield as IDI Gazeley Brookfield Logistics Properties.<ref name="Retuers">{{cite web |url=http://www.reuters.com/article/2014/05/28/ny-idi-gazeley-idUSnBw285542a+100+BSW20140528|title=Brookfield Announces Creation of New Brand for $3.5 Billion Global Logistics Property Platform|date=2014-05-28|publisher=Reuters|accessdate=2015-06-23}}</ref>

== Services ==

IDI offers development, investment management, property management and leasing services. IDI has developed approximately 115 million square feet of industrial space, with projects ranging from 80,000 square feet to more than 1 million square feet throughout North America.<ref name="nrei"/> Projects include buildings for Mitsubishi Electric Cooling & Heating, Speedway Distribution Center and Riverside Business Center in Atlanta.<ref name="citybizlist">{{cite web |url=http://atlanta.citybizlist.com/article/atlanta-based-idi-sees-strongest-level-development-recession|title=Atlanta-based IDI Sees Strongest Level of Development Since Recession|date=2012-07-27|publisher=CityBizList Atlanta|accessdate=2013-04-23}}</ref> In 2013, the company had about 7 million square feet in development throughout nine states.<ref name="rejournal">{{cite web |url=http://www.rejournals.com/2013/04/18/idi-celebrates-strong-start-to-2013-cincinnati-memphis-and-chicago-markets-remain-strong/|title=IDI celebrates strong start to 2013; Cincinnati, Memphis and Chicago markets remain strong|date=2013-04-18|publisher=Midwest Real Estate News|accessdate=2013-04-23}}</ref> IDI's specific development offerings include the following:  multimarket program planning, design, market research and site selection, site review, LEED/Sustainable design consulting and construction management.<ref name="idi">{{cite web |url=http://www.idi.com/development |title=IDI: Development|publisher=IDI|accessdate=2013-04-22}}</ref>

* Development: At any given time, IDI has 8 to 12 million square feet of move-in-ready industrial, office and distribution facilities available across North America.<ref name="idi"/>
* Investment Management: IDI has ownership or interests in 36 million square feet of investment grade assets.<ref name="miramar"/>
* Property Management: IDI manages 50 million square feet throughout North America.<ref name="nreion">{{cite web |url=http://nreionline.com/industrial/idi-s-leasing-activity-surpasses-33-million-sq-ft-first-half-2011|title=IDI’s Leasing Activity Surpasses 3.3 Million Sq. Ft. in First Half of 2011|date=2011-08-30|publisher=National Real Estate Investor|accessdate=2013-06-25}}</ref> 
* Leasing services: During the first fiscal quarter of 2013, IDI leased more than 2 million square feet across eight markets. IDI leased the most properties in [[Memphis]] during this quarter and obtained its largest lease in [[Philadelphia]].<ref name="pass">{{cite web |url=http://www.passfail.com/news/yahoo/idi-secures-more-than-2-million-square-feet-of-leases-in-q1-quarter-saw-memphis-and-philadelphia-markets-lead-volume-in-2013/idi-secures-more-than-2-million-square-feet-of-leases-in-q1-quarter-saw-memphis-and-philadelphia-markets-lead-volume-in-2013-8329727.htm|title=IDI Secures More Than 2 Million Square Feet of Leases in Q1: Quarter Saw Memphis and Philadelphia Markets Lead Volume in 2013|date=2013-04-10|publisher=Yahoo|accessdate=2013-06-25}}</ref>

== Locations ==

IDI is headquartered in Atlanta and has eight offices across North America including Memphis, Chicago, Cincinnati, Southern California, Dallas, South Florida and Northern New Jersey.<ref name="nreibig"/>

== References ==

{{Reflist}}

== External links ==
* [http://www.brookfieldlogisticsproperties.com IDI's website]

[[Category:Articles created via the Article Wizard]]
[[Category:Companies established in 1989]]
[[Category:Commercial real estate]]