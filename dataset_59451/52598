{{Infobox journal
| title = Political Research Quarterly
| cover = [[File:Political Research Quarterly.tif]]
| editor = J.S. Maloy, Jeanette Mendez  
| discipline = [[Political science]]
| former_names = Western Political Quarterly
| abbreviation = Polit. Res. Q.
| publisher = [[Sage Publications]] on behalf of the [[University of Utah]]
| country = United States
| frequency = Quarterly
| history = 1948-present
| openaccess = 
| license = 
| impact = 1.116
| impact-year = 2015
| website = http://www.sagepub.com/journals/Journal201839/title
| link1 = http://prq.sagepub.com/content/current
| link1-name = Online access
| link2 = http://prq.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1065-9129
| eISSN = 1938-274X
| OCLC = 638770729
| LCCN = 93657077
}}
'''''Political Research Quarterly''''' is a quarterly [[peer-reviewed]] [[academic journal]] that covers the field of [[political science]]. The [[editors-in-chief]] are Clarissa Hayward ([[Washington University in St. Louis]]), Jeanette Mendez ([[Oklahoma State University]]), and James Scott ([[Texas Christian University]]). It was established in 1948 and is published by [[Sage Publications]] on behalf of the [[University of Utah]]. It is the official journal of the [[Western Political Science Association]].

== History ==
Originally named the ''Western Political Quarterly'', it served as the official journal of the Western Political Science Association since it was first published by the [[University of Utah]] in March, 1948. The relationship between the association and the journal was formalized in 1971. The journal was renamed ''Political Research Quarterly'' in 1992.<ref>{{cite journal |doi=10.1177/1065912909346698 |title=Where We Came from: Defining the Western Political Science Association: A Brief, Very Selective Organizational History Sampled from the Archives of the WPSA|jstor=25594452|year=2009 |last1=Moulds |first1=E. |journal=Political Research Quarterly |volume=62 |issue=4 |pages=840}}</ref>

== Abstracting and indexing ==
The journal  is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.116, ranking it 59th out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201839/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Political science journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1948]]
[[Category:University of Utah]]
[[Category:Academic journals associated with universities and colleges of the United States]]
[[Category:Academic journals associated with learned and professional societies of the United States]]