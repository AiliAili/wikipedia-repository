{{Infobox journal
| title         = Journal of the Evangelical Theological Society
| cover         = [[image:Journal of the Evangelical Theological Society.jpg|200px]]
| caption       = 
| former_name   = Bulletin of the Evangelical Theological Society
| abbreviation  = JETS
| discipline    = [[Christian theology]]
| peer-reviewed = 
| language      = [[English language|English]]
| editor        = [[Andreas J. Köstenberger]]
| publisher     = [[Evangelical Theological Society]]
| country       = United States
| history       = {{Start date|1958}}-present
| frequency     = Quarterly
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| ISSNlabel     = 
| ISSN          = 0360-8808
| eISSN         = 1745-5251
| CODEN         = 
| JSTOR         = 
| LCCN          = 
| OCLC          = 2244860
| website       = http://www.etsjets.org/JETS
| link1         = 
| link1-name    = 
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}

The '''''Journal of the Evangelical Theological Society''''' is a [[Scholarly peer review|refereed]] [[List of theological journals|theological journal]] published by the [[Evangelical Theological Society]]. It was first published in 1958 as the ''Bulletin of the Evangelical Theological Society'', and was given its present name in 1969.

The journal's circulation now regularly reaches 4000 copies and is distributed worldwide.<ref name=JETSmp>{{cite web|title=The Evangelical Theological Society|url=http://www.etsjets.org/jets|website=JETS main page|accessdate=7 October 2015}}</ref>

== History ==
The journal has been published constantly over the years since 1958 when as the "Bulletin of the ..." it was original issued as a single article. This was [[Ned B. Stonehouse]]'s (of Westminster Theological Seminary) notable presidential address to the society's annual meeting, entitled "The Infallibility of Scripture and Evangelical Progress."<ref name=JETSmp>{{cite web|title=The Evangelical Theological Society|url=http://www.etsjets.org/jets|website=JETS main page|accessdate=7 October 2015}}</ref> In 1969 the publication took the current name as "Journal ..." and had grown considerably larger both in terms of circulation and page count.<ref name=JETSmp>{{cite web|title=The Evangelical Theological Society|url=http://www.etsjets.org/jets|website=JETS main page|accessdate=7 October 2015}}</ref>

== Editors ==
The editors were compiled by referencing the JETS archives.<ref>{{cite web|title=The Evangelical Theological Society|url=http://www.etsjets.org/?q=jets_pdf_archive|website=JETS PDF Archives|accessdate=19 June 2014}}</ref>
{| class="wikitable"
|-
! Year !! Editor !! Volumes
|-
| 1958–1959 || [[Stephen Barabas]]<ref name=Schultz>Samuel J. Schultz, "[http://www.etsjets.org/files/JETS-PDFs/12/12-1/12-1-pp001-002_JETS.pdf Editorial]", ''JETS'' 12.1 (1969), p. 1.</ref> || 1–2
|-
| 1960–1961 || [[John Luchies]]<ref name=Schultz /> || 3–4
|-
| 1962–1975 || [[Samuel J. Schultz]] || 5–18
|-
| 1976–1999 || [[Ronald Youngblood]] || 19–42
|-
| 2000–current || [[Andreas J. Kostenberger]] || 43–current
|}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.etsjets.org/JETS}}
* [http://www.etsjets.org/?q=jets_pdf_archive Archives] in PDF format {{subscription required}}

{{DEFAULTSORT:Journal of the Evangelical Theological Society}}
[[Category:Academic journals published by learned and professional societies]]
[[Category:English-language journals]]
[[Category:Publications established in 1958]]
[[Category:Quarterly journals]]
[[Category:Protestant studies journals]]


{{reli-journal-stub}}
{{Christian-book-stub}}