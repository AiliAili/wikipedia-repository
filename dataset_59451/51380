{{selfref|For information on how to contribute to the Gene Wiki at Wikipedia, please see [[Portal:Gene Wiki]].}}
The '''Gene Wiki''' was established to transfer information on human [[gene]]s from scientific resources to [[Wikipedia]] stub articles.<ref name="pmid18613750">{{cite journal |vauthors=Huss JW, Orozco C, Goodale J, Wu C, Batalov S, Vickers TJ, Valafar F, Su AI | title = A Gene Wiki for Community Annotation of Gene Function | journal = PLoS Biol. | volume = 6 | issue = 7 | pages = e175 |date=July 2008 | pmid = 18613750 | pmc = 2443188 | doi = 10.1371/journal.pbio.0060175 | url =  }}</ref><ref name="SciVee_Pubcast">{{cite web|url=http://www.scivee.tv/pubcast/18613750 |title=SciVee Pubcast: A Gene Wiki for Community Annotation of Gene Function |vauthors=Huss JW, Orozco C, Goodale J, Wu C, Batalov S, Vickers TJ, Valafar F, Su AI |date= |format= |work= |publisher= |pages= |language= |archiveurl=https://web.archive.org/web/20100724055742/http://www.scivee.tv:80/pubcast/18613750 |archivedate=2010-07-24 |quote= |accessdate=2008-11-19 |deadurl=yes |df= }}</ref><ref name="pmid19755503">{{cite journal | author = Huss JW | title = The Gene Wiki: community intelligence applied to human gene annotation | journal = Nucleic Acids Res. | volume = 38 | issue = Database issue | pages = D633–9 |date=January 2010 | pmid = 19755503 | pmc = 2808918 | doi = 10.1093/nar/gkp760 | url = http://nar.oxfordjournals.org/cgi/pmidlookup?view=long&pmid=19755503 | author2 = Lindenbaum P | author3 = Martone M | display-authors = 3 | last4 = Roberts | first4 = D. | last5 = Pizarro | first5 = A. | last6 = Valafar | first6 = F. | last7 = Hogenesch | first7 = J. B. | last8 = Su | first8 = A. I.}}</ref>

The Gene Wiki project also initiated publication of gene-specific review articles in the journal ''Gene'', together with the editing of the gene-specific pages in Wikipedia.<ref>{{cite journal|vauthors = Tsueng G, Good BM, Ping P, Golemis E, Hanukoglu I, van Wijnen AJ, Su AI |title = Gene Wiki Reviews-Raising the quality and accessibility of information about the human genome. | journal = Gene | volume = 592 | issue = 2 | pages = 235–8 |date = 2 May 2016| pmid=27150585 | doi = 10.1016/j.gene.2016.04.053 }}</ref>

==Project goals and scope==

===Number of gene articles===

The [[human genome]] contains an estimated 20,000–25,000 [[gene|protein-coding genes]].<ref name="pmid18040051">{{cite journal |vauthors=Clamp M, Fry B, Kamal M, Xie X, Cuff J, Lin MF, Kellis M, Lindblad-Toh K, Lander ES | title = Distinguishing protein-coding and noncoding genes in the human genome | journal = Proc. Natl. Acad. Sci. U.S.A. | volume = 104 | issue = 49 | pages = 19428–33 |date=December 2007 | pmid = 18040051 | pmc = 2148306 | doi = 10.1073/pnas.0709013104 | url =  }}</ref>  The goal of the Gene Wiki project is to create seed articles for every [[WP:notable|notable]] human gene, that is, every gene whose function has been assigned in the peer-reviewed scientific literature. Approximately half of human genes have assigned function, therefore the total number of articles seeded by the Gene Wiki project would be expected to be in the range of 10,000–15,000. To date, approximately 11,000 articles have been created or augmented to include Gene Wiki project content.<ref name="Gene_Wiki_Pages">{{cite web | url =https://tools.wmflabs.org/templatecount/index.php?lang=en&name=PBB&namespace=10#bottom | title =  Gene Wiki Pages | work = Pages that link to {{tl|PBB}} | publisher = Wikipedia, the free encyclopedia | accessdate = 2015-01-27}}</ref>

===Expansion===

Once seed articles have been established, the hope and expectation is that these will be [[annotation|annotated]] and expanded by editors ranging in experience from the lay audience to students to professionals and academics.<ref name="pmid18613750"/>

===Proteins encoded by genes===

The majority of genes encode [[protein]]s hence understanding the function of a gene generally requires understanding of the function of the corresponding protein. In addition to including basic information about the gene, the project therefore also includes information about the protein encoded by the gene.

==Gene Wiki generated content==

Stubs for the Gene Wiki project are created by a [[Internet bot|bot]] and contain links to the following primary gene/protein databases:

* [[HUGO Gene Nomenclature Committee]] – official gene name
* [[Entrez]] – Gene database
* [[OMIM]] (Mendelian Inheritance in Man) – database that catalogues all the known diseases with a genetic component
* Amigo – [[Gene Ontology]]
* [[HomoloGene]] – gene [[Homology (biology)|homologs]] in other species
* [[SymAtlasRNA]] – [[gene expression]] pattern in [[tissue (biology)|tissues]]<ref name="pmid15075390">{{cite journal |vauthors=Su AI, Wiltshire T, Batalov S, Lapp H, Ching KA, Block D, Zhang J, Soden R, Hayakawa M, Kreiman G, Cooke MP, Walker JR, Hogenesch JB | title = A gene atlas of the mouse and human protein-encoding transcriptomes | journal = Proc. Natl. Acad. Sci. U.S.A. | volume = 101 | issue = 16 | pages = 6062–7 |date=April 2004 | pmid = 15075390 | pmc = 395923 | doi = 10.1073/pnas.0400782101 | url =  }}</ref>
* [[Protein Data Bank]] – 3D [[tertiary structure|structure]] of protein encoded by the gene
* [[Uniprot|UniProt]]  ('''uni'''versal '''prot'''ein resource) – a central repository of protein data

== See also ==
* [[Portal:Gene Wiki/Other Wikis]] – a list of Gene Wikis external to Wikipedia

==References==
{{Reflist|2}}

==Further reading==
{{refbegin|2}}
* {{cite journal |vauthors=Good BM, Howe DG, Lin SM, Kibbe WA, Su AI | title = Mining the Gene Wiki for functional genomic knowledge | journal = BMC Genomics | volume = 12 | issue = | pages = 603 | year = 2011 | pmid = 22165947 | pmc = 3271090 | doi = 10.1186/1471-2164-12-603 }}
* {{cite journal |vauthors=Good BM, Clarke EL, de Alfaro L, Su AI | title = The Gene Wiki in 2011: community intelligence applied to human gene annotation | journal = Nucleic Acids Res. | volume = 40 | issue = Database issue | pages = D1255–61 |date=January 2012 | pmid = 22075991 | pmc = 3245148 | doi = 10.1093/nar/gkr925 }}
* {{cite journal |vauthors=Good BM, Clarke EL, Loguercio S, Su AI | title = Linking genes to diseases with a SNPedia-Gene Wiki mashup | journal = J Biomed Semantics | volume = 3 Suppl 1 | issue = | pages = S6 | year = 2012 | pmid = 22541597 | pmc = 3337266 | doi = 10.1186/2041-1480-3-S1-S6 }}
* {{cite journal |vauthors=Su AI, Good BM, van Wijnen AJ | title = Gene Wiki Reviews: marrying crowdsourcing with traditional peer review | journal = Gene | volume = 531 | issue = 2 | pages = 125 |date=December 2013 | pmid = 24012870 | doi = 10.1016/j.gene.2013.08.093 }}
{{refend}}

==External links==
* {{cite web | url = http://abcnews.go.com/Technology/story?id=5353376&page=1 | title = : Edit your DNA: `Gene wiki' to debut on Wikipedia | author = Spagat E | date = 2008-07-09| format = | work = | publisher = The Associated Press (in ABC News | pages = | language = | archiveurl = | archivedate = | quote = | accessdate = 2011-09-29}}
* {{cite web | url = http://www.medicalnewstoday.com/articles/113891.php | title = Archiving Information About The Human Genome Using Wikipedia | author = McKenney AS | date = 2008-07-08 | format = | work = IT / Internet / E-mail News | publisher = Medical News Today  | pages = | language = | archiveurl = | archivedate = | quote = | accessdate = 2008-07-19}}
* {{cite web | url = http://www.itnews.com.au/News/80128,wikipedia-hosts-human-gene-repository.aspx | title = Wikipedia hosts human gene repository | author = Tay L | date = 2008-07-10 | format = | work = iTnews Australia | publisher = Haymarket Media | pages = | language = | archiveurl =https://web.archive.org/web/20080713045146/http://www.itnews.com.au:80/News/80128,wikipedia-hosts-human-gene-repository.aspx| archivedate =2008-07-13| quote = | dead-url = yes | accessdate = 2008-07-19}}
* {{cite web | url = http://www.nature.com/news/2008/080903/full/455022a.html | title = Big data: Wikiomics | author = Mitch Waldrop  | date = 3 September 2008 | work = Nature | pages =22–25 }}

[[Category:History of Wikipedia]]
[[Category:Wikis]]
[[Category:Genes|*]]