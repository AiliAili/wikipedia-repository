{{Infobox journal
| title = Molecular Pharmacology
| cover = [[File:Mol Pharmacol August 2009 cover.png]]
| editor = [[Stephen F. Traynelis]]
| discipline = [[Pharmacology]]
| abbreviation = Mol. Pharmacol.
| publisher = [[American Society for Pharmacology and Experimental Therapeutics]]
| country = United States
| frequency = Monthly
| history = 1965–present
| openaccess = 
| license = 
| impact = 4.128
| impact-year = 2014
| website = http://molpharm.aspetjournals.org/
| link1 = 
| link1-name = 
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 0026-895X
| eISSN = 1521-0111
}}
'''''Molecular Pharmacology''''' is a [[peer review|peer-reviewed]] [[scientific journal]] published by the [[American Society for Pharmacology and Experimental Therapeutics]] since 1965. All issues are available online. It is currently indexed in [[MEDLINE]], [[Scopus]], and other databases.<ref>{{cite web |url=http://molpharm.aspetjournals.org |title=''Molecular Pharmacology'' |date=2009 |accessdate=2009-07-30 |publisher=American Society for Pharmacology and Experimental Therapeutics}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal received a 2014 [[impact factor]] of 4.128, ranking it 38th out of 254 journals in the category ''Pharmacology & Pharmacy''.<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Pharmacology & Pharmacy |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |series=Web of Science |postscript=.}}</ref> 

== History ==
''Molecular Pharmacology'' was established by [[Avram Goldstein]] in 1965. {{As of|2012}}, the [[editor-in-chief]] is [[Stephen F. Traynelis]], ([[Emory University]]). 

== References ==
{{Reflist}}

== External links ==
* {{Official|http://molpharm.aspetjournals.org/}}

[[Category:Pharmacology journals]]
[[Category:Publications established in 1965]]
[[Category:Monthly journals]]
[[Category:English-language journals]]