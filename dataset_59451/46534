{{Infobox album
| Name = Big Band Bossa Nova
| Type = studio
| Artist = [[Stan Getz]]
| Cover = BigBandBossaNovaStanGetz.jpg
| Released = End of October 1962<ref>{{cite web|url=https://books.google.it/books?id=CBgEAAAAMBAJ&pg=PT36&dq=Stan+Getz+Big+Band+Bossa+Nova+Bossa+Nova+8494&hl=it&sa=X&ved=0CCgQ6AEwADgUahUKEwj3rvTM3dPHAhVFjSwKHeYTCbw#v=onepage&q=Stan%20Getz%20Big%20Band%20Bossa%20Nova%20Bossa%20Nova%208494&f=false |title=Billboard |website=Books.google.it |date=1962-11-03 |accessdate=2016-04-20}}</ref>
| Recorded = August 27-28, 1962<br><small>[[CBS 30th Street Studio]], [[New York City]]
| Genre = [[Bossa nova]], [[jazz]], [[cool jazz]]
| Length = {{Duration|m=33|s=30}}
| Label = [[Verve Records|Verve]]<br><small> V6-8494
| Producer = [[Creed Taylor]]
| Last album = ''[[Jazz Samba]]''<br />(1962)
| This album = '''''Big Band Bossa Nova'''''<br />(1962)
| Next album = ''[[Jazz Samba Encore!]]''<br/ >(1963)
}}
{{Album ratings
| rev1= ''[[Down Beat]]''
| rev1Score = {{Rating|5|5}}<ref name="Down Beat">''[[Down Beat]]'': December 6, 1962, vol. 29, no. 30</ref>
| rev2 = [[Allmusic]]
| rev2Score = {{Rating|4|5}}<ref>{{cite web|author=Richard S. Ginell |url=http://www.allmusic.com/album/mw0000196996 |title=Big Band Bossa Nova - Stan Getz &#124; Songs, Reviews, Credits |publisher=[[AllMusic]] |date= |accessdate=2016-04-20}}</ref>
}}

'''''Big Band Bossa Nova''''' is a 1962 album by saxophonist [[Stan Getz]] with the Gary McFarland Orchestra. The album was arranged and conducted by [[Gary McFarland]] and produced by [[Creed Taylor]] for [[Verve Records]]. This was Stan's second [[bossa nova]] album for Verve following ''[[Jazz Samba]]'', his very successful collaboration with guitarist [[Charlie Byrd]].

The music was recorded at the [[CBS 30th Street Studio]] in New York City on August 27 and 28, 1962.

==Music==
The music for the album consists of four songs by Brazilian composers and four original compositions by McFarland. The instrumentation chosen by McFarland eschews the traditional big band format of eight brass and five saxophones for a smaller ensemble featuring four woodwinds and French horn as well as three trumpets and two trombones. The four piece rhythm section is augmented by two percussionists.

McFarland freely mixes his instrumental colors to provide a constantly shifting palette in support of Getz' tenor.  [[Jim Hall (musician)|Jim Hall]], [[Hank Jones]], [[Doc Severinsen]] and [[Bob Brookmeyer]] are each featured in short solos.

Noted jazz critic Don DeMichael, writing in the December 6, 1962 issue of ''[[Down Beat]]'' magazine, awarded the album the top rating of five stars. DeMichael went on to say: "Getz' melodic gift was never more evident; even the way he plays "straight" melody is masterful. Few jazzmen have had this gift... and it has to do with singing by means of an instrument, for Getz doesn't just play a solo, he ''sings'' it, as can be heard on any of these tracks, most evidently on ''Triste'' and ''Saudade''.<ref name="Down Beat"/>

About the writing DeMichael says: "McFarland shares in the artistic success of the album. His writing is peerless... he knows the proper combination of instruments to achieve certain sounds and he has the taste not to use all the instruments at hand all the time. His sparing use of the ensemble allows the beauty of the soloist and the material to shine through".<ref name="Down Beat"/>

==Reception==
Although not reaching the chart heights of its predecessor, the album performed respectably on the charts.  On the ''[[Billboard (magazine)|Billboard]]'' Top LP chart, it reached position #13, staying on for 23 weeks.<ref name=whitburn>The Billboard Albums, 6th ed. Joel Whitburn. 2006. Record Research Inc. p. 407. ISBN 0-89820-166-7</ref>

==Track listing==
# "Manha de Carnival" ''(Morning of the Carnival)''  ([[Luiz Bonfá]]) – 5:48
# "Balanço no Samba" ''(Street Dance)''  ([[Gary McFarland]]) – 2:59
# "Melancólico" ''(Melancholy)''  (Gary McFarland) – 4:42
# "Entre Amigos" ''(Sympathy Between Friends)'' (Gary McFarland) – 2:58
# "[[Chega de Saudade]]" ''(No More Blues)''  ([[Antônio Carlos Jobim]], [[Vinicius de Moraes]]) – 4:10
# "Noite Triste" ''(Night Sadness)''  (Gary McFarland) – 4:56
# "[[Samba de Uma Nota Só]]" ''(One Note Samba)''  (Antônio Carlos Jobim, [[Newton Mendonça]]) – 3:25
# "Bim Bom" ([[João Gilberto]]) – 4:31<ref name="Liner notes to Verve V6-8494">Liner notes to Verve V6-8494</ref>

==Personnel==
*Stan Getz - tenor saxophone
*[[Doc Severinsen]], [[Bernie Glow]] or Joe Ferrante and [[Clark Terry]] or [[Nick Travis]] - trumpet
*Ray Alonge - French horn
*[[Bob Brookmeyer]] or [[Willie Dennis]] - trombone
*Tony Studd - bass trombone
*Gerald Sanfino or Ray Beckenstein - flute
*Ed Caine - alto flute
*Ray Beckenstein and/or Babe Clark and/or Walt Levinsky - clarinet
*Romeo Penque - bass clarinet
*[[Jim Hall (musician)|Jim Hall]] - unamplified guitar
*[[Hank Jones]] - piano
*[[Tommy Williams (musician)|Tommy Williams]] - bass 
*[[Johnny Rae]] - drums
*José Paulo - tambourine
*Carmen Costa - cabassa
*[[Gary McFarland]] - arranger, conductor<ref name="Liner notes to Verve V6-8494"/>

==Production==
*Produced by [[Creed Taylor]]
*Engineered by George Kneurr and Frank Laico

==References==
{{Reflist}}

{{Stan Getz}}

[[Category:Stan Getz albums]]
[[Category:Bossa nova albums]]
[[Category:1962 albums]]
[[Category:albums arranged by Gary McFarland]]
[[Category:albums produced by Creed Taylor]]
[[Category:Verve Records albums]]