{{copyedit|date=March 2017}}
{{Infobox royalty
| name = Fatima bint Mubarak Al Ketbi
| title = [[Sheikh]]a
| image =
| image_size = 
| caption = 
| spouse = [[Zayed bin Sultan Al Nahyan]]
| issue = [[Mohammed bin Zayed Al Nahyan|Sheikh Mohammed]]<br>[[Hamdan bin Zayed bin Sultan Al Nahyan|Sheikh Hamdan]]<br>[[Hazza bin Zayed bin Sultan Al Nahyan|Sheikh Hazza]]<br>[[Tahnoun bin Zayed bin Sultan Al Nahyan|Sheikh Tahnoun]]<br>[[Mansour bin Zayed Al Nahyan|Sheikh Mansour]]<br>[[Abdullah bin Zayed Al Nahyan|Sheikh Abdullah]]<br>[[Shamma bint Zayed Al Nahyan|Sheikha Shamma]]<br>[[Alyazia bint Zayed Al Nahyan|Sheikha Alyazia]]
| full name = 
| styles = 
| succession = 
| reign = 
| reign-type = 
| predecessor =  
| father = Mubarak Al Ketbi
| mother = 
| birth_date = 
| birth_place = Al Hayer, [[Al Ain]]
| death_date = 
| death_place =
| house = [[House of Al Nahyan]] (by marriage)
| religion = [[Islam]]
| website = 
}}
{{Abu Dhabi Princely Family}}
'''Fatima bint Mubarak Al Ketbi''' (فاطمة بنت مبارك الكتبي) is the third wife of the founder and the first president of [[the United Arab Emirates|the United Arab Emirates (UAE)]] and the late emir (ruler) of Abu Dhabi, Sheikh [[Zayed bin Sultan Al Nahyan]]. She is firstly called mother of sheikhs and then mother of the UAE or nation.<ref name=ayouth/><ref name=999april2011/><ref name=ClausAbad2009>{{cite book|author1=Isabell A. Claus|author2=Diana Abad|author3=Kasim Randeree|title=Leadership and the Emirati woman: Breaking the glass ceiling in the Arabian Gulf|url=https://books.google.com/books?id=K5WDECaMtukC&pg=PA15|accessdate=16 April 2013|date=1 April 2009|publisher=LIT Verlag Münster|isbn=978-3-643-10251-5|pages=15}}</ref>

==Early life==
[[Sheikh]]a Fatima was born in Al Hayer in [[Al Ain]], being the only daughter to her parents.<ref name=swar2002june>{{cite journal|last=Swaroop|first=Sangeetha|title=National Heroine and International Champion of Women Rights|journal=Al Shindagah|date=June–July 2002|issue=76|url=http://www.alshindagah.com/shindagah76/En/HerHighness.htm|accessdate=16 April 2013}}</ref> Her family are [[bedouin]] and religious,<ref name=ayouth/> like a traditional Emirati family. Her father died when she was young so her mother remarried and had two more sons, Suheil and Owad.{{Citation needed|date=December 2016}}

==Activities==
Sheikha Fatima is one of the women rights supporters in the country.<ref name=swar2002june/> She is the supreme chairperson of the family development foundation.<ref name=999april2011>{{cite journal|title=Civil Defence honours Mother of the Nation |journal=999 |date=April 2011 |issue=484 |page=10 |url=http://www.moi.gov.ae/portal/En/Publication/PDF/103120111055202343750.pdf |accessdate=16 April 2013 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref name=almoj2009july>{{cite journal|title=Tunisian President confers Grand Cordon of Order of November 7 on Sheikha Fatima|journal=Almojtama|date=July 2009|issue=22|page=7|url=http://www.takafulgov.com/uploaded/english.pdf?page_id=42|accessdate=16 April 2013}}</ref> She significantly contributed to the foundation of the first women’s organization in 1976, the Abu Dhabi society for the awakening of women.<ref name=swar2002june/> She was also instrumental in a nationwide campaign towards girls' education.<ref name=swar2002june/> In addition, Sheikha Fatima heads the United Arab Emirate's women federation, which she founded in 1975.<ref name=999april2011/><ref name="Maddy-Weitzman2002">{{cite book|author=Bruce Maddy-Weitzman|title=Middle East Contemporary Survey: 1999|url=https://books.google.com/books?id=zs57d0logH8C&pg=PA629|accessdate=16 April 2013|date=1 August 2002|publisher=The Moshe Dayan Center|isbn=978-965-224-049-1|pages=629}}</ref> She is also president of motherhood and childhood supreme council.<ref name=ayouth>{{cite web|title=Biography|url=http://arabyouthawards.net/en/pages/biography.aspx|publisher=Arab Youth Awards|accessdate=16 April 2013}}</ref> At the end of the 1990s, she publicly announced that women should be member of [[Federal National Council|the federal national council]] of the Emirates.<ref name="Maddy-Weitzman2002"/>

She also supports efforts concerning adult literacy and provision of free public education to girls.<ref name=999april2011/> Annually an exclusive award named the Sheikha Fatima Award for Excellence has been presenting in her honour since 2005.<ref name=zaw10may/> It is awarded for the outstanding academic performance and commitment to the environment and world citizenship of the female recipients.<ref name=zaw10may>{{cite news|title=Outstanding female students honoured at the Sheikha Fatima Bint Mubarak Award for Excellence|url=http://www.zawya.com/story/ZAWYA20100510121532/|accessdate=16 April 2013|newspaper=Zawya|date=10 May 2010}}</ref> It entitles winners to a full-tuition scholarship and extends across schools in [[the Middle East]] and was expanded to [[India]] as well in 2010.<ref name=zaw10may/> She has constantly supported women and has initiated an award called Sheikha Fatima bint Mubarak Award for Woman Athletes has been given to female athletes .<ref>{{cite news|title=The judging panel of Sheikha Fatima bint Mubarak Award for Woman Athletes holds an introductory meeting |url=http://www.fbmwsa.ae/en/readnew.aspx?id=168 |accessdate=16 April 2013 |newspaper=FBMWSA |date=27 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20140219011918/http://fbmwsa.ae/en/readnew.aspx?id=168 |archivedate=19 February 2014 |df= }}</ref> Sheikha Fatima bint Mubarak has created a Ladies sports academy called Fatima Bint Mubarak Ladies Academy in Abu Dhabi.{{citation needed|date=December 2016}}

===Awards===
In 1997, five different organizations of the United Nations, namely [[UNICEF|the UNICEF]], [[WHO]], [[UNIFEM]], [[UNFP]] and [[UNFPA]], awarded Sheikha Fatima for her significant efforts for women's rights.<ref name=ClausAbad2009/> Furthermore, the UNIFEM stated "she is the champion of women's right."<ref name=ClausAbad2009/> She was also awarded the Grand Cordon of the Order by the then Tunisian president [[Zine El Abidine Ben Ali|Zine el Abidine ben Ali]] in 2009 for her contributions to raise the status of Arab women.<ref name=almoj2009july/> Much more significantly she was given by [[UNESCO|the UNESCO]] the Marie Curie Medal for her efforts in education, literacy and women's rights, being the third international and the first Arab recipient of the award.<ref name=ClausAbad2009/>
Shaikha Fatima Institute of Nursing and Health Sciences Lahore Pakistan is named after Her Highness.

==Marriage and children==
Fatima bint Mubarak Al Ketbi was married to Zayed when he was the ruler of the Eastern region in the 1960s.<ref name=ayouth/><ref name=anthony>{{cite web|last=Anthony|first=John Duke|title=Succession in Abu Dhabi|url=http://ncusar.org/publications/Publications/1999-08-30-Succession-In-Abu-Dhabi-and-The-UAE.pdf|publisher=NCUSAR|accessdate=11 April 2013|date=30 August 1999}}</ref> She was spotted by her husband when engaging in a traditional dance.<ref name="UAE FIRST LADY: BEHIND-THE-SCENES PLAYER">{{cite web|title=UAE First Lady: Behind-the-Scenes Player|url=https://wikileaks.org/plusd/cables/04ABUDHABI3527_a.html|publisher=United Arab Emirates Abu Dhabi|accessdate=11 December 2016|language=English|date=6 October 2004}}</ref> Fatima was the only spouse that remained married to Sheikh Zayed Bin Sultan until the end.<ref name=tel4nov>{{cite news|title=Sheikh Zayed bin Sultan Al Nahyan|url=http://www.telegraph.co.uk/news/obituaries/1475775/Sheikh-Zayed-bin-Sultan-Al-Nahyan.html|accessdate=18 April 2013|newspaper=The Telegraph|date=4 November 2004}}</ref> They moved to [[Abu Dhabi (city)|Abu Dhabi city]] when Sheikh Zayed became the ruler in August 1966.She was his most influential and favorite spouses because of her influential personality.<ref name="wleaks004" /><ref name="gsn12nov">{{cite journal|title=With MBZ’s promotion, Sheikha Fatima sons take centre stage|journal=Gulf States Newsletter|date=12 November 2003|volume=724|url=http://www.gsn-online.com/with-mbz%E2%80%99s-promotion-sheikha-fatima-sons-take-centre-stage-20031112|accessdate=16 April 2013}}</ref> She is the mother of [[Mohammed bin Zayed Al Nahyan|Sheikh Mohammed]] (born 1961), the current Crown Prince of Abu Dhabi; [[Hamdan bin Zayed bin Sultan Al Nahyan|Sheikh Hamdan]] (born 1963), [[Hazza bin Zayed bin Sultan Al Nahyan|Sheikh Hazza]], Sheikh Tahnoun, [[Mansour bin Zayed Al Nahyan|Sheikh Mansour]], [[Abdullah bin Zayed Al Nahyan|Sheikh Abdullah]], Sheikha Shamma and Sheikha Alyazia.<ref name="wleaks004">{{cite news|title=UAE Succession Update: The Post-Zayed Scenario |url=http://www.cablegatesearch.net/cable.php?id=04ABUDHABI3410 |accessdate=16 April 2013 |newspaper=Wikileaks |date=28 September 2004 |deadurl=yes |archiveurl=https://web.archive.org/web/20130603051751/http://cablegatesearch.net/cable.php?id=04ABUDHABI3410 |archivedate=3 June 2013 |df= }}</ref> They are the most powerful block in the ruling family of Abu Dhabi, the [[Al Nahyan family|Al Nahyans]].<ref name="ft5may">{{cite news|title=Abu Dhabi’s family business|url=http://www.ft.com/intl/cms/s/0/197e16f2-399b-11de-b82d-00144feabdc0.html#axzz2QYbDPdDK|accessdate=16 April 2013|newspaper=Financial Times|date=5 May 2009}}</ref>

==References==
{{Reflist|33em}}

{{DEFAULTSORT:Fatima bint Mubarak Al Ketbi}}
[[Category:Living people]]
[[Category:Spouses of Presidents of the United Arab Emirates]]
[[Category:House of Al Nahyan]]
[[Category:People from Al Ain]]
[[Category:Year of birth missing (living people)]]
[[Category:Emirati activists]]
[[Category:Leaders of organizations]]
[[Category:Women's rights activists]]