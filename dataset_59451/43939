<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 | name=Zeppelin-Staaken E-4/20
 | image=Staaken E.4 20.jpg
 | caption=
}}{{Infobox aircraft type
 | type= Airliner
 | national origin= Germany
 | manufacturer=[[Zeppelin-Staaken]] (Zeppelin-Werke G.m.b.H., Staaken, Berlin)
 | designer=[[:de:Adolf Rohrbach|Adolf Rohrbach]]
 | first flight=30 September 1920<ref name="schule-bw.de">{{cite web|title=Die Staaken E.4/20 - das fortschrittlichste Verkehrsflugzeug seiner Zeit|url=http://www.schule-bw.de/unterricht/faecher/geschichte/unterricht/unterrichtsekII/themen/friedrichshafen/zeppelins-flieger2.html/staaken_e4/|website=schule-bw.de|accessdate=30 January 2017|language=German}}</ref>
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Zeppelin-Staaken E-4/20''' was a revolutionary four-engine all-metal passenger monoplane designed in 1917 by [[Adolf Rohrbach]] and completed in 1919 at the [[Zeppelin-Staaken]] works outside [[Berlin]], [[Germany]]. The E-4/20 was the first four-engine, all-metal stressed skin heavier-than-air airliner built.

==Design and development==
At a time when most aircraft were small, single-engine biplanes made of wood and canvas, the E-4 was a large (102-foot wingspan), all-metal, four-engine, [[Stressed skin|stress-skinned]], [[semi-monocoque]], [[Cantilever#Aircraft|cantilevered-wing]] [[monoplane]], with an enclosed [[cockpit]], accommodation for 18 passengers plus a crew of five, including two pilots, a radio operator, an engineer and a steward, as well as radio-telegraph communications, a toilet, a galley and separate baggage and mail storage. With a maximum speed of 143&nbsp;mph, cruising speed of {{convert|131|mph|abbr=on|disp=flip}}, a range of about {{convert|750|mi|km|abbr=on|disp=flip}}, and a fully loaded weight of {{convert|18739|lb|abbr=on|disp=flip}}, it outperformed most other airliners of its day.<ref name=Haddow/>

The E-4 included numerous innovations, including its all-metal [[monocoque]] construction, onboard facilities such as lavatory, kitchen and radio communications, as well as its notable and sturdy monoplane load-bearing box-girder wing constructed of [[Duralumin|dural]] metal which formed both the wing's main girder and the structure of the wing itself.  Skinned with thin sheets of dural metal to give the aerofoil shape necessary for a wing, the girder section wing had fabric covered leading and trailing edges attached to it. This superb and innovative wing was robust and self-supporting.<ref name=Haddow/>

The E-4 was completed in 1919 and test flown between 30 September 1920 and 1922 when it was broken up on the orders of the [[Military Inter-Allied Commission of Control|Inter-Allied Commission]].<ref name=Haddow/>

==Background==
The Zeppelin-Staaken E-4/20 was a product of the innovative Zeppelin Airship company. Count [[Ferdinand von Zeppelin]], founder of the [[Luftschiffbau Zeppelin]] GmbH (Zeppelin Airship Construction Co.) was himself a major aeronautical innovator, creator of the groundbreaking giant aluminium alloy framed [[Zeppelin]] lighter than air [[dirigible airship]]s and later developer of a series of [[Riesenflugzeug|R-Planes]].<ref name=Haddow/>

Zeppelin was one of the first aeronautical pioneers to apply stringent scientific principals to the design of aircraft, focusing on issues like power-to-weight ratios of engines and using the then new metal alloy aluminium for structural components. Zeppelin heard of the success in Russia of [[Igor Sikorsky]]'s pioneering 4-engined ''[[Sikorsky Russky Vityaz|Le Grand]]'' and ''[[Sikorsky Ilya Muromets|Ilya Muromets]]'' aircraft. From the outbreak of [[World War I|war]] in 1914 the 4-engined Ilya Muromets class of aircraft were used as heavy bombers. The German government saw the potential for large strategic bombers and issued a design standard which was used by several manufacturers to produce [[Riesenflugzeug]]e ("Giant Aircraft") or R-Planes. The most successful design and manufacturing company of R-Planes was Zeppelin which was also the only company to manufacture them in series production, the [[Zeppelin-Staaken R.VI|R-VI]].<ref name=Haddow/>

==Legacy==
Adolf Rohrbach went on to found his own aircraft company, ''[[Rohrbach Metall-Flugzeugbau]]'' where he designed and built a number of innovative civil all-metal airliners, such as the [[Rohrbach Roland|Ro-VIII]] [[trimotor]] as well as some groundbreaking [[flying boat]]s.

The [[Smithsonian Institution]]'s "[[Air & Space/Smithsonian|Airspace Magazine]]" suggested that Rohrbach could have been Germany's [[Boeing]] or [[Douglas Aircraft Company|Douglas]] but that the Inter-Allied Commission deemed the E-4/20 too much of a threat as a potential bomber to be allowed to go into serial production and ordered its destruction, even declining offers to sell it to allied countries.

The 1932 [[Armstrong Whitworth Atalanta]] used a very similar configuration, differing in construction details and more powerful engines.<ref name="Tapper">{{cite book|last1=Tapper|first1=Oliver|title=Armstrong Whitworth Aircraft since 1913|date=1973|publisher=Putnam & Company Limited|location=London|isbn=9780370100043|pages=219-235}}</ref>

==Specifications==
{{Aircraft specs
|ref=<ref name=Haddow>{{cite book |last=Haddow |first=G.W. |author2=Peter M. Grosz |title=The German Giants - The German R-Planes 1914-1918 |publisher=Putnam |location=London |year=1988 |edition=3rd |isbn=0-85177-812-7|pages=289-293}}</ref><ref name="all-aero">{{cite web|title=Zeppelin-Staaken E.4/20|url=http://www.all-aero.com/index.php/home2/12129-zeppelin-staaken-e420|website=all-aero|accessdate=30 January 2017}}</ref><ref name="flight170321p185">{{cite journal|title=THE ZEPPELIN-STAAKEN ALL-METAL MONOPLANE: And a New Smaller Edition|journal=Flight|date=17 March 1921|pages=185-186|url=http://www.flightglobal.com/pdfarchive/view/1921/1921%20-%200185.html|accessdate=30 January 2017}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=3-5
|capacity=12-18 pax
|length m=16.6
|span m=31
|height m=4.5
|height note=approx.
|wing area sqm=106
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=6072
|gross weight kg=8500
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=4
|eng1 name=[[Maybach Mb.IVa]]
|eng1 type=6-cyl water-cooled in-line pistonengines
|eng1 hp=245

|prop blade number=2
|prop name=fixed pitch wooden propellers
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|max speed kmh=225
|cruise speed kmh=200
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1200
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=5-6 hours
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2=80
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=0.09 kW/kg (0.055 hp/lb)

|more performance=
}}

==References==
{{Reflist}}
<!--==Further reading==-->
==External links==
{{commons category|Zeppelin-Staaken E-4/20}}
*[http://www.airwar.ru/enc/cw1/zse420.html]
*[http://www.airspacemag.com/multimedia/photos/?c=y&articleID=18766954&page=6 Smithsonian photo]

{{Zeppelin aircraft}}
{{Idflieg R-class designations}}
{{wwi-air}}

{{DEFAULTSORT:Zeppelin-Staaken E-4 20}}
[[Category:Zeppelin-Staaken]]
[[Category:German airliners 1910–1919]]
[[Category:Individual aircraft]]