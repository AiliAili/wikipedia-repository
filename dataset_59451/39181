{{good article}}
{{Use mdy dates|date=July 2013}}
{{Infobox road
|state=CA
|type=SR
|route=905
|section=632
|maint=[[California Department of Transportation|Caltrans]]
|map_notes=SR&nbsp;905 highlighted in red
|length_mi=8.964
|length_ref=<ref name=trucklist/>
|length_round=3
|established=1986 (from SR 117)<ref name="CA1986" />
|direction_a=West
|terminus_a=Oro Vista Road&nbsp;/ Tocayo Avenue in [[San Diego, California|San Diego]]
|junction={{jct|state=CA|I|5}} in San Diego<br>{{jct|state=CA|I|805}} in San Diego<br />{{jct|state=CA|SR|125|SR|11}} in [[Otay Mesa, San Diego|Otay Mesa]]
|direction_b=East
|terminus_b={{jct|state=BCN|FH|2}} at [[Mexico-United States border|Mexican border]] near Otay Mesa
|previous_type=I
|previous_route=880
|next_type=I
|next_route=980
|counties=[[San Diego County, California|San Diego]]
}}
'''State Route 905''' ('''SR 905''') is a [[state highway]] in [[San Diego, California]], that connects [[Interstate 5 (California)|Interstate 5]] (I-5) and [[Interstate 805 (California)|Interstate 805]] in [[San Ysidro, San Diego|San Ysidro]] to the [[United States – Mexico border]] at [[Otay Mesa]]. The entire [[highway]] from I-5 to the international border is a [[freeway]] with a few exits that continues east from the I-805 interchange before turning southeast and reaching the border.

SR&nbsp;905 was formerly routed on '''Otay Mesa Road''', which had been in existence since at least 1927. Before it was SR 905, the route was first designated as part of [[SR 75 (CA)|SR 75]], before it was redesignated as '''SR 117'''. The freeway was completed between I-5 and Otay Mesa Road in 1976. The border crossing opened in 1985 after several delays in obtaining funding for the construction of what would become SR&nbsp;905. After becoming SR&nbsp;905 in 1986, the highway was converted to first an expressway in 2000 and then a freeway in 2010 and 2011. Plans are for this highway to eventually become known as '''Interstate 905'''.

==Route description==
SR 905 begins at the intersection of Tocayo Avenue and Oro Vista Road in [[Nestor, San Diego|Nestor]]. It begins as a freeway, intersecting with [[Interstate 5 in California|I-5]] at a [[partial cloverleaf interchange]]. After interchanges with Beyer Boulevard and Picador Boulevard, the freeway then intersects [[Interstate 805 (California)|I-805]]. Following this, SR 905 veers southeast to parallel Otay Mesa Road, with interchanges at Caliente Avenue (in [[Pacific Gateway Park]]), Britannia Boulevard, and La Media Road. The route interchanges with the [[California State Route 11|SR 11]] freeway, which is planned to be a toll facility that will serve a new border crossing east of Otay Mesa.<ref>{{cite news|title=Hearing Set on Border Highway and Crossing|url=http://www.sandiegouniontribune.com/news/2011/jan/10/hearing-set-sr-11-linking-new-border-crossing/|newspaper=San Diego Union-Tribune|date=January 11, 2011|last=Hawkins|first=Robert J.|page=B2|access-date=February 20, 2016}}</ref> Immediately after, SR 905 turns south to its final interchange at Siempre Viva Road before the route ends at the [[Otay Mesa Port of Entry]],<ref name="gmaps">{{google maps|url=https://maps.google.com/?ll=32.55318,-117.038383&spn=0.148454,0.282211&t=m&z=12|accessdate=August 10, 2013|title=SR&nbsp;905}}</ref> which truck traffic must use to cross the border.<ref name="funds">{{cite news | title=Funds pursued to extend I-905 to border | work=San Diego Union-Tribune | date=August 5, 1997 | author=Arner, Mark | page=B2}}</ref>

SR&nbsp;905 is part of the [[California Freeway and Expressway System]]<ref name="cafes">{{CAFESystem}}</ref> and part of the [[National Highway System (United States)|National Highway System]].<ref name=fhwa-nhs>{{FHWA NHS map|region=sandiego}}</ref><ref name=NHS-FHWA>{{FHWA NHS}}</ref> In 2013, SR&nbsp;905 had an [[annual average daily traffic]] (AADT) of 58,000 between Beyer Boulevard and I-805, and 18,400 around the I-5 junction, the former of which was the highest AADT for the freeway.<ref name=traffic>{{Caltrans traffic|year=2013|start=505|end=980}}</ref>

==History==
[[File:I-805 Northbound at CA 905.jpg|thumb|240x240px|I-805 northbound at the SR 905 interchange]]
What was known as the "Otay Mesa road" existed as a dirt road by 1927.<ref>{{cite news | title=Tijuana River Flood Passes Peak; No Danger to Track | work=The San Diego Union | date=February 17, 1927 | author=Staff | page=14}}</ref> A paved road connected San Ysidro to Brown Field and the easternmost ranches in the Otay Mesa area by 1935.<ref>{{cite map|title=San Diego County|publisher=[[Automobile Club of Southern California]]|year=1935|section=H5–H6}}</ref> Discussions were held between San Diego County and [[National City, California|National City]] over the maintenance of the road in 1950, since it was used by trucks travelling to the landfill.<ref>{{cite news | title=Otay Mesa Road Offered to County | work=The San Diego Union | date=January 1, 1950 | author=Staff | page=A3}}</ref> The majority of SR 905, running in parallel with Otay Mesa Road from [[Interstate 5]] to [[SR 125 (CA)|SR 125]], was added to the state highway system and the California Freeway and Expressway System in 1959 as '''Legislative Route 281''',<ref>{{cite CAstat|year=1959|ch=1062|p=3115, 3121}}</ref> and became part of [[State Route 75 (California)|SR 75]] in the [[1964 renumbering (California)|1964 renumbering]].<ref>{{cite CAstat|year=1963|ch=385|p=1177}}</ref>

Planning was underway for the extension of SR&nbsp;75 east to the then-proposed SR&nbsp;125 by 1963.<ref>{{cite news | title=State Hearing Slated On South Bay Freeway | work=The San Diego Union | date=January 31, 1963 | author=Staff | page=G6}}</ref> The California Highway Commission endorsed the routing for SR&nbsp;75 in 1965 along Otay Mesa Road, away from future residential developments.<ref>{{cite news | title=County Freeway Routes OK'd | work=The San Diego Union | date=April 30, 1965 | author=Staff | page=B14}}</ref> There were plans as early as 1970 to have a highway heading southwest to a new border crossing that would bypass the Tijuana area.<ref>{{cite news | title=City Council Airs New 805 Route | work=The San Diego Union | date=August 26, 1970 | author=Staff | page=B1}}</ref> The next year, James Moe, the state public works director, subsequently asked the [[California State Legislature]] to lengthen SR&nbsp;75 to connect to this new crossing, rather than using I-5 to make the connection.<ref>{{cite news | title=Route 75 Extension Recommend As A Link To New Port Of Entry | work=The San Diego Union | date=August 15, 1971 | author=Staff | page=B14}}</ref>

Following this, in 1972, the legislature added a new SR&nbsp;117, which extended this part of SR&nbsp;75 southwest to the [[Mexican border (US)|Mexican border]] near [[Border Field State Park]], to the state highway system, and a southerly extension of SR&nbsp;125 to the border at [[Otay Mesa]] to the state highway and Freeway and Expressway systems.<ref>{{cite CAstat|year=1972|ch=1216|p=2345, 2351}}</ref> Two years later, planning began for the construction of the Otay Mesa crossing and the construction of SR&nbsp;75 to connect it to I-5 and I-805.<ref>{{cite news | title=Road Studies For Border Entry Urged | work=The San Diego Union | date=February 16, 1974 | author=Staff | page=B2}}</ref> Later, the Comprehensive Planning Organization (CPO), the local association of municipal governments, recommended using $4&nbsp;million of federal funding for the construction of SR&nbsp;75.<ref>{{cite news | title=CPO Backs 11 Area Road Jobs | work=The San Diego Union | date=April 24, 1974 | author=Staff | page=B8}}</ref><ref>{{cite news | title=New Name For CPO Supported | work=The San Diego Union | date=April 24, 1974 | author=Staff | page=B8}}</ref> The CPO later endorsed expediting construction of the freeway before completion of the border crossing, so that the freeway would primarily serve border traffic, thus preventing land speculation in Otay Mesa.<ref>{{cite news | title=CPO Asks Delay On Border Gate | work=The San Diego Union | date=October 6, 1974 | author=Staff | page=B1}}</ref> While Mexican authorities wanted the crossing constructed in 1975, the CPO indicated that the funding for SR&nbsp;75 would not be available until at least 1980, or even 1985.<ref>{{cite news | title=Otay Mesa Gates Face Long Delay | work=The San Diego Union | date=July 3, 1975 | author=Murphy, Vi | page=B5}}</ref> Following this, Representative Lionel Van Deerlin attempted to accelerate the construction of the crossing, even though there was no funding for the highway.<ref>{{cite news | title=Van Deerlin Seeks To Push Second Border Crossing | work=The San Diego Union | date=July 20, 1975 | author=Staff | page=B3}}</ref>

Construction began on the southern portion of SR&nbsp;75 in mid-1974. In January 1976, the part of SR&nbsp;75 between I-805 and Otay Mesa Road was opened to traffic.<ref name="open">{{cite news | title=Segment Of State 75 Completed | work=The San Diego Union | date=January 29, 1976 | author=Staff | page=B3}}</ref> On April 6, the next portion of the freeway opened. However, there were concerns about what to call the freeway, citing confusion with the northern portion of SR&nbsp;75. The entire cost of the project was $6.3&nbsp;million.<ref>{{cite news | title=Freeway Link Opens | work=The San Diego Union | date=April 7, 1976 | author=Staff | page=B1}}</ref> SR&nbsp;117 was extended east to SR&nbsp;125, replacing the southerly segment of SR&nbsp;75, by the Legislature in 1976;<ref>{{cite CAstat|year=1976|ch=1354|p=6177}}</ref> this took effect at the beginning of 1977.<ref>{{cite news | title=State 75 Freeway Becomes 117 | work=The San Diego Union | date=January 9, 1977 | author=Staff | page=B6}}</ref> Estimates for completing the freeway ran from $13.8&nbsp;million to $28.5&nbsp;million.<ref>{{cite news | title=2nd Border Gate Project Pushed | work=The San Diego Union | date=January 9, 1977 | author=Staff | page=B2}}</ref>

In late 1977, the CPO made plans to push for adding SR&nbsp;117 to the Interstate Highway System, to obtain additional federal funding.<ref>{{cite news | title=CPO Eyes State 117 Action | work=The San Diego Union | date=November 22, 1977 | author=Staff | page=B1}}</ref> By 1979, both San Diego city and county had allocated $6&nbsp;million to construct a temporary way to access the border crossing along Otay Mesa and Harvest Roads.<ref>{{cite news | title=An Old Border Problem Seen In New Gates | work=The San Diego Union | date=September 10, 1979 | author=Staff | pages=B3}}</ref> Two years later, the City of San Diego indicated that the upgrade of Otay Mesa Road to a four-lane road would be the preferred option;<ref>{{cite news | title=County Reaffirms Commitment To 2nd Border Crossing | work=The San Diego Union | date=March 12, 1981 | author=Rangel, Jesus | page=B8}}</ref> the state agreed to allocate $2&nbsp;million towards the $10&nbsp;million project, with the city contributing $6.4&nbsp;million and the county adding $2.3&nbsp;million.<ref>{{cite news | title=State Vows $2 Million For Border Crossing Road | work=The San Diego Union | date=November 2, 1982 | author=Kozub, Linda | page=B1}}</ref> The [[Federal Highway Administration]] approved the continuous roadway via SR&nbsp;117 and SR&nbsp;125 from I-5 to the border at Otay Mesa as a non-chargeable (not eligible for federal Interstate Highway construction dollars) part of the [[Interstate Highway System]] in October&nbsp;1984.<ref>[[California Department of Transportation]], [http://www.dot.ca.gov/hq/tsip/hseb/products/state_highway_routes_selected_information_1995_revised.pdf State Highway Routes: Selected Information], 1994 with 1995 revisions</ref> The Otay Mesa border crossing opened on January 24, 1985.<ref>{{cite news | title=Otay Mesa gateway opens | work=Evening Tribune | date=January 25, 1985 | author=Romero, Fernando | location=San Diego | page=B1}}</ref> The route number was legislatively changed to 905 in 1986,<ref name="CA1986" /> and signs were updated in 1988. This change was to apply for other federal funding.<ref>{{cite news | title=Freeway gets new name in quest for more funds | work=Evening Tribune | date=December 30, 1988 | author=Hughes, Joe | location=San Diego | page=B2}}</ref> The original piece of SR&nbsp;117, west of I-5, also became SR&nbsp;905 with the rest of SR&nbsp;117,<ref name="CA1986">{{cite CAstat|year=1986|ch=928|p=3216}}</ref> but Caltrans has not constructed it.<ref name="gmaps" />

Efforts were underway in 1997 to secure federal funding for the highway and other infrastructure near the Mexican border, largely supported by Senator [[Barbara Boxer]] and Representative [[Bob Filner]],<ref>{{cite news | title=Bill is aimed at border 'choke points' | work=San Diego Union-Tribune | date=March 4, 1997 | author=Braun, Gerry | page=A3}}</ref> and Representative [[Bud Schuster]], the chairman of the [[House Transportation and Infrastructure Committee]], visited the region before giving informal approval to the proposal.<ref name="funds" /> In 1999, Governor [[Gray Davis]] approved allocating $27&nbsp;million in federal funding to complete the freeway.<ref>{{cite news | title=Governor backs federal funds for construction of Route 905 | work=San Diego Union-Tribune | date=June 17, 1999 | author=Staff | page=B11}}</ref> Concerns were expressed by local residents and municipal officials regarding the apparent priority of SR&nbsp;125 over SR&nbsp;905 in terms of funding, especially since SR&nbsp;125 was to be constructed as a toll road, and SR&nbsp;905 would compete with the toll road enterprise.<ref>{{cite news | title=Route 125 appears winner in freeway war | work=The San Diego Union | date=March 15, 1998 | author=Arner, Mark | page=B1}}</ref> In May 1998, Congress approved $54&nbsp;million in funding for the completion of SR&nbsp;905.<ref>{{cite news | title=$100 million OK'd for area road projects | work=San Diego Union-Tribune | date=May 23, 1998 | author=Meinert, Dori and Otto Kreisher | page=B1}}</ref>

Otay Mesa Road was widened to six lanes in 2000, at a cost of $20.5&nbsp;million. Before, it had 50&nbsp;percent more traffic than it was designed to handle;<ref>{{cite news | title=Two road projects in Otay Mesa complete | work=San Diego Union-Tribune | date=March 23, 2000 | author=Arner, Mark | page=B8}}</ref> it was considered by the [[San Diego Union-Tribune]] as "California's busiest trade route with Mexico", and traffic had increased by a factor of ten, with the number of people dying in traffic accidents approaching five times the state average. The widening of Otay Mesa Road was considered a temporary fix for the problem.<ref>{{cite news | title=Fear and loathing defines driving on old Otay Mesa Road | work=San Diego Union-Tribune | date=June 1, 1997 | author=Arner, Mark | page=B1}}</ref> The next year, the California Transportation Commission allocated $25&nbsp;million of state funding towards completing the freeway.<ref>{{cite news | title=$25 million OK'd for South Bay freeway | work=San Diego Union-Tribune | date=June 8, 2001 | author=Staff | page=B2}}</ref> The interchange with Siempre Viva Road opened on December 8, 2004.<ref>{{cite news | title=1st segment of state Route 905 makes its debut - Roadway to help ease border traffic | work=San Diego Union-Tribune | date=December 9, 2004 | author=Branscomb, Leslie | page=B7}}</ref> Delays in the [[U.S. Congress]] approving federal funding in 2005 resulted in a delayed start to construction for the rest of SR&nbsp;905.<ref>{{cite news | title=Federal highway aid tie-ups delay cures for congestion | work=San Diego Union-Tribune | date=May 23, 2005 | author=Wilkie, Dana | page=A1}}</ref>

Construction began on the part of SR&nbsp;905 between Britannia Boulevard and Siempre Viva Road in April 2008, and from Brittania Boulevard to I-805 in July 2009.<ref name="fact" /> Efforts were made to keep construction going despite a shortfall in funding from state bonds in 2009.<ref>{{cite news | title=Freeway work to go on despite crunch | work=San Diego Union-Tribune | date=February 11, 2009 | author=Schmidt, Steve | page=B3}}</ref> The part between Britannia Boulevard and Siempre Viva Road opened in December 2010.<ref name="fact" /> The interchange with I-805 began to be upgraded in April 2011,<ref>{{cite news | title=Overhaul Launched For 805-905 Interchange | work=The San Diego Union | date=April 6, 2011 | author=Staff | page=B2}}</ref> and the construction, which used $20&nbsp;million in federal funding,<ref>{{cite news | title=$20 Million Marked for 805-905 Junction | work=San Diego Union-Tribune | date=October 5, 2010 | author=Staff | page=B2}}</ref> finished in February 2012.<ref name="fact">{{cite web|url=http://sandag.org/uploads/publicationid/publicationid_9_1562.pdf |title=SR 905 Fact Sheet |publisher=San Diego Association of Governments |work=Fact Sheets |date=July 2012 |accessdate=August 23, 2013 |author=Staff |archiveurl=http://www.webcitation.org/6J6TDBH8N?url=http://sandag.org/uploads/publicationid/publicationid_9_1562.pdf |archivedate=August 24, 2013 |format=PDF |deadurl=yes |df=mdy }}</ref> The final freeway segment of SR&nbsp;905 between I-805 and Britannia Boulevard opened on July 30, 2012. The entire cost of the project connecting I-805 to the border crossing was $441&nbsp;million.<ref>{{cite news |title=New Border Freeway Set To Open To The Public |author=Jose Luis Jiménez |url=http://www.kpbs.org/news/2012/jul/27/new-border-freeway-set-open-monday/ |newspaper=[[KPBS (TV)|KPBS]]|date=July 27, 2012 |accessdate=August 2, 2012}}</ref>

==Future==
{{Infobox road small
|header_type=UC
|state=CA
|type=I
|route=905
|location=[[San Diego, California|San Diego]]
}}
Plans are for SR&nbsp;905 to become Interstate&nbsp;905;<ref>{{cite news|title=Freeway gets new name in quest for more funds|newspaper=Evening Tribune|author=Hughes, Joe|location=San Diego|page=B2}}{{Date missing}}</ref> however, it could not be constructed with the same funds that were used for constructing the rest of the [[Interstate Highway System]].{{#tag:ref|I-905 was not constructed as of 1978, when the Surface Transportation Assistance Act of 1978 provided that all Interstate construction authorized under previous amendments to the system would be funded by the federal government but additional highway mileage added under {{usc|23|103(c)(4)(A)}} would not be funded from the same highway fund.<ref>Surface Transportation Assistance Act of 1978, {{USPL|99|599}}</ref><ref name="FHWA2">{{cite web |first= Tony |last= DeSimone |date= June 4, 2012 |url= http://www.fhwa.dot.gov/reports/routefinder/#s05 |work= Route Log and Finder List |title= Expansion of Mileage |publisher=Federal Highway Administration |accessdate= July 22, 2012}}</ref>|group = lower-alpha}} Previously, SR&nbsp;905 had a direct connection with [[California State Route 125|SR 125]] via two at-grade intersections on Otay Mesa Road.<ref name="tgsd">{{Cite map| publisher=Thomas Brothers| title=San Diego County Street Atlas| year=2009}}</ref> With the completion of the newest freeway segment of SR&nbsp;905 and the freeway-to-freeway connection to SR 125 unconstructed, traffic on SR 905 must exit at La Media Road (exit 7), head north on La Media and head east on Otay Mesa Road to make the connection.<ref name="gmaps" /> A freeway-to-freeway interchange is planned for the connection between SR&nbsp;125 and SR&nbsp;905, and an additional interchange is planned for Heritage Road.<ref>{{cite news|title=New Freeway Segment at Border Celebrated|url=http://www.sandiegouniontribune.com/news/2012/jul/19/tp-new-freeway-segment-at-border-celebrated/|newspaper=U-T San Diego|date=July 19, 2012|last=Kuhney|first=Jen Lebron|page=B2|access-date=February 20, 2016}}</ref> Ramps from SR 11 westbound and eastbound to SR 125 northbound were completed on November 30, 2016, at a cost of over $21 million.<ref>{{Cite news |title=South County Freeway Connector Work Done |last=Stewart |first=Joshua |date=December 3, 2016 |work=San Diego Union-Tribune |page=6}}</ref>

==Exit list==
{{CAinttop|exit|post-1964=yes|county=San Diego|location=San Diego|based_on=the unconstructed western end at the Mexican border<ref name=bridgelog/> and the rest of the alignment that existed at the time|post_ref=<br><ref name=trucklist /><ref name=traffic /><ref name=bridgelog>{{Caltrans bridgelog}}</ref>|exit_ref=<br><ref name="calnexus">{{cite web|title=Interstate 905 Freeway Interchanges|work=California Numbered Exit Uniform System|url=http://www.dot.ca.gov/trafficops/exit/docs/905.pdf|publisher=California Department of Transportation|accessdate=February 5, 2009|date=January 2, 2008|format=PDF|author=Warring, K.S.|location=Sacramento}}</ref>
}}
{{CAint|exit
|postmile=2.84
|road=Oro Vista Road&nbsp;/ Tocayo Avenue
|notes=[[At-grade intersection]]; west end of SR 905}}
{{CAint|exit
|postmile=3.19
|exit=1
|road={{jct|state=CA|I|5}}
|notes=Signed as exits 1A (north) and 1B (south); no exit numbers eastbound; exit 3 on I-5}}
{{CAint|exit
|postmile=3.82
|exit=1C
|road=Beyer Boulevard
|notes=Signed as exit 1 eastbound}}
{{CAint|exit
|postmile=4.41
|exit=2A
|road=Picador Boulevard&nbsp;/ Smythe Avenue
|notes=}}
{{CAint|exit
|postmile=5.16
|exit=2B
|road={{jct|state=CA|I|805}}
|notes=Exit 1B on I-805}}
{{CAint|exit
|postmile=6.43
|exit=3
|road=Caliente Avenue
|notes=Signed as exit 4 westbound}}
{{CAint|exit
|postmile=7.72
|exit=5
|type=unbuilt
|road=Heritage Road
|notes=Proposed interchange}}
{{CAint|exit
|postmile=8.70
|exit=6
|road=Britannia Boulevard
|notes=}}
{{CAint|exit
|postmile=9.70
|exit=7
|road=La Media Road
|notes=}}
{{CAint|exit
|postmile=10.50
|pmspan=2
|type=incomplete
|exit=8
|espan=2
|road={{jct|state=CA|SR|11|dir1=east}}
|notes=Eastbound exit and westbound entrance; future toll road}}
{{CAint
|mile=none
|type=incomplete
|road={{jct|state=CA|SR-Toll|125|dir1=north|name1=[[South Bay Expressway]]}}
|notes=Interchange under construction; eastbound access via SR 11; westbound exit only}}
{{CAint|exit
|postmile=11.60
|exit=9
|road=Siempre Viva Road
|notes=Last [[United States|U.S.]] exit eastbound}}
{{CAint|exit
|postmile=11.80
|exit=–
|road=[[Otay Mesa Port of Entry]]
|notes=East end of SR 905; continues beyond the [[Mexico–United States border|Mexican border]] as Blvd. Garita de Otay}}
{{jctbtm|keys=incomplete,unbuilt}}

==Footnotes==
{{reflist |group=lower-alpha}}

==See also==
*{{portal-inline|California Roads}}

==References==
{{reflist|30em}}

==External links==
{{Commons category|California State Route 905}}
{{Attached KML|display=title,inline}}
* [http://www.aaroads.com/california/ca-905.html California @ AARoads.com - State Route 905]
* [http://www.cahighways.org/880-980.html#905 CAhighways.org - State Route 905]
*[http://www.dot.ca.gov/hq/roadinfo/sr905 Caltrans: Route 905 highway conditions]

{{3di|5}}

[[Category:State highways in California|905]]
[[Category:Southern California freeways|905]]
[[Category:Roads in San Diego County, California|State Route 905]]
[[Category:Baja California–California border crossings]]
[[Category:Transportation in San Diego]]