{{Infobox Journal
| title        = Fluid Phase Equilibria
| cover        = [[Image:Fluid Phase Equilibria cover.gif]]
| discipline   = [[Thermodynamics]], [[Physical Chemistry]]
| abbreviation = FFE
| publisher    = [[Elsevier]]
| country      = [[Netherlands]]
| frequency    = 24 issues per year
| history      = 
| openaccess   =
| website      = http://www.elsevier.com/wps/find/journaldescription.cws_home/502684/description#description 
| ISSN         =0378-3812
}}
'''''Fluid Phase Equilibria''''' is a [[Peer review|peer-reviewed]] scientific journal on [[physical chemistry]] and [[thermodynamics]] that is published by [[Elsevier]]. The articles deal with experimental, theoretical and applied research related to properties of pure components and mixtures, especially phase equilibria, caloric and transport properties of fluid and solid phases. It had an [[impact factor]] of 1.846 (2015).

== Editors ==
The current editors are 
* [[Peter Cummings]] - Department of Chemical and Biomolecular Engineering of the Vanderbilt University in Nashville, U.S.
* [[Theo de Loos]] - Process and Energy Department of the Delft University of Technology, Netherlands
* [[John O'Connell (scientist)|John O'Connell]] - Department of Chemical Engineering, University of Virginia in Charlottesville, U.S.

== Availability ==
Fluid Phase Equilibria can be obtained in print or in electronic form. 

== External links ==
* [http://www.elsevier.com Elsevier Publisher]
* [http://www.elsevier.com/wps/find/journaldescription.cws_home/502684/description#description Fluid Phase Equilibria Homepage]

[[Category:Chemistry journals]]