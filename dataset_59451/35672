{{About||the Radio Festival|ABU Radio Song Festival 2012}}
{{good article}}
{{active editnotice}} <!-- See [[Wikipedia:Editnotice]] -->
{{Infobox Song Contest
| name       = ABU TV Song Festival
| year       = 2012
| image      = 
| theme      = Beyond the Wave!
| logo       = ABU TV Song Festival 2012 logo.svg
| final      = 14 October 2012<ref name=Seoul2012>{{cite web|title=Asia Song Festival - Seoul 2012 |url=http://www.abu2012seoul.com/s61.html |accessdate=16 August 2012 |date=16 August 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20130218021614/http://www.abu2012seoul.com:80/s61.html |archivedate=18 February 2013 |df=dmy }}</ref>
| presenters = {{unbulleted list|Han Seok Joon|Jamaica dela Cruz}}<ref name="ABU TV"/>
| director   =
| exsupervisor = Hanizah Hamzah<ref name=RunningOrder>{{cite web|last=Hamzah|first=Hanizah|title=ABU TV Song Festival|url=http://www.abu.org.my/Programme-@-ABU_TV_Song_Festival.aspx|publisher=[[Asia-Pacific Broadcasting Union]]|accessdate=4 October 2012|archiveurl=https://web.archive.org/web/20120920081328/http://www.abu.org.my/Programme-@-ABU_TV_Song_Festival.aspx|archivedate=20 September 2012}}</ref>
| winner     = 
| vote       =
| host       = [[Korean Broadcasting System]] (KBS)<ref>{{cite web|last=Kenny|first=Luke|title=Korea to host ABU radio and TV song festivals|url=http://www.radioandmusic.com/content/editorial/news/korea-host-abu-radio-and-tv-song-festivals-next-year|publisher=radioandmusic.com|accessdate=17 August 2012|date=7 November 2011}}</ref> 
| venue      = {{unbulleted list|[[KBS Hall|KBS Concert Hall]],|[[Seoul]], [[South Korea]]<ref name=Seoul2012/>}}
| entries    = 11<!-- ** ATTENTION: Please keep this number consistent with the ones in the "Participating countries" section AND in the lead (introduction). ** -->
| debut      = {{unbulleted list|{{Abu|Afghanistan|TV}}|{{Abu|Australia|TV}}|{{Abu|China|TV}}|{{Abu|Hong Kong|TV}}|{{Abu|Indonesia|TV}}|{{Abu|Japan|TV}}|{{Abu|Malaysia|TV}}|{{Abu|Singapore|TV}}|{{Abu|South Korea|TV}}|{{Abu|Sri Lanka|TV}}|{{Abu|Vietnam|TV}}}}
| return     = 
| withdraw   = 
| null       =
| interval   = MIJI, "[[Fly Me to the Moon]]"<ref name=acts>{{cite web|last=Mikheev|first=Andy|title=Special guests|url=http://esckaz.com/2012/abu_tv.htm|work=ABU TV Song Festival 2012|publisher=ESCKaz|accessdate=22 October 2012|date=22 October 2012}}</ref> 
| opening    = Chongdong Theatre, "Miso"<ref name=acts/>
| reprise    = Joint song, "[[Heal the World]]"<ref name=acts/>
<!-- Map Legend Colours -->
| Green    = Y 
| Green SA = 
| Purple   = 
| Red      = 
| Yellow   = 
}}
The '''ABU TV Song Festival 2012''' was the first annual edition of the [[ABU TV Song Festival]]s. The festival, which was non-competitive, took place in the [[KBS Hall|KBS Concert Hall]], located in the [[South Korea]]n capital of [[Seoul]] and coincided with the 49th general assembly of the [[Asia-Pacific Broadcasting Union]] (ABU).<ref>{{cite web|title=ABU Upcoming Events|url=http://www.abu.org.my/event_List_Upcoming.aspx|publisher=[[Asia-Pacific Broadcasting Union]]|date=16 August 2012|accessdate=16 August 2012}}</ref>  Eleven<!-- ** ATTENTION: Please keep this number consistent with the ones in the "Participating countries" section AND in the infobox. ** --> countries confirmed their participation in the first edition of the competition.<ref name="ABU TV">{{cite web|last=Mikheev|first=Andy|title=ABU TV Song Festival 2012 Participants|url=http://esckaz.com/2012/abu_tv.htm|publisher=ESCKaz|accessdate=4 October 2012|date=4 October 2012}}</ref>

The Asia-Pacific Broadcasting Union (ABU) had previously run international contests, inspired by the Eurovision Song Contest, for their members during 1985 &ndash; 1987.<ref>{{cite web|last=Kamarul-Baharin|first=Ross|title=Biggest Party in Europe|url=http://ecentral.my/news/story.asp?file=/2007/6/25/music/18041299&sec=music|publisher=The Star Online|accessdate=17 August 2012|date=25 June 2007}}</ref>  The European Broadcasting Union had proposed a partnership with the ABU in 2008 to establish the Asiavision Song Contest, however these talks never produced any results.  The ABU announced in 2011 that they would organise their own ABU Radio and TV Song Festivals.<ref name=EBUABU>{{cite web|url=http://www.ebu.ch/fr/union/news/2008/tcm_6-63136.php|title=Eurovision Song Contest goes to Asia|publisher=''European Broadcasting Union''|accessdate=23 October 2012|date=18 September 2008}}</ref>

The format used was different from that of the [[Eurovision Song Contest]], as there were two festivals that took place.  The ABU TV Song Festival was a non-competitive musical gala orientated, while the Radio Song Festival was the competitive.  The festival was not aired live by any of the competing broadcasters, but was scheduled to be broadcast between October and November 2012.

== History ==
{{Main article|ABU Song Festivals}}
The [[Asia-Pacific Broadcasting Union]] (ABU) had already run an international song contest for its members inspired by the [[Eurovision Song Contest]] in 1985 &ndash; 1987, called the ''ABU Popular Song Contest'', with 14 countries of the Asia-Pacific region competing.<ref name=Participants>{{cite web|last=Mikheev|first=Andy|title=Participants - ABU TV and Radio Song Festivals 2012|url=http://esckaz.com/2012/abu.htm|publisher=escKaz.com|accessdate=17 August 2012|date=16 August 2012}}</ref> The show had a similar concept to the current festivals with winners being chosen by a professional jury. [[South Korea]], [[New Zealand]] and [[Australia]] celebrated victories in this competition. In 1989 &ndash; 1991 ABU co-produced the ''ABU Golden Kite World Song Festival'' in Malaysia with participation of Asia-Pacific countries, as well as [[Yugoslavia]] and [[Finland]].<ref name=Participants/>

In 2008, the [[European Broadcasting Union]] (EBU) proposed a partnership with ABU on the establishment of an ''Asiavision Song Contest'',<ref name=Participants/> however these talks didn't produce any result, and in September 2008 it was announced that the [[Eurovision Song Contest]] format for Asian production had been sold to a private company from [[Singapore]], Asiavision Pte. Ltd.<ref name=EBUABU/> The original name intended for that event was ''Asiavision Song Contest'', but it was later changed to ''Our Sound - The Asia-Pacific Song Contest'' following a request from the ABU, who uses the [[Asiavision]] name for their news exchange service.<ref name=Participants/><ref>{{cite web|url=http://www.asiavision.tv/press_releases/OurSoundPR040309.pdf |title=Our Sound – The Asia-Pacific Song Contest defies economic slump |publisher=asiavision.tv |date=4 March 2009 |accessdate=23 August 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120224001529/http://www.asiavision.tv/press_releases/OurSoundPR040309.pdf |archivedate=24 February 2012 }}</ref> Initially, the contest (which was supposed to be a two program live broadcast TV show with public voting) was set to premiere in 2009, but it was later rescheduled for March 2010 in [[Macao]] and then for November 2010 in [[Mumbai]], at the end being postponed indefinitely "due to the ongoing issues between the organizers and EBU".<ref>{{cite web|last=Mikheev|first=Andy|title=Our Sound - The Asia-Pacific Song Contest|url=http://esckaz.com/2010/news3.htm#oursound|publisher=ESCKAZ.com|accessdate=17 August 2012|date=16 August 2012}}</ref>

Shortly before launching the ABU Song Festival, the ABU had been considering the possibility to organize the ''ABU ASEAN TV Song Festival'' in Thailand.<ref name=Participants/> Historically, [[ASEAN]] song contests had been organized in periods between 1981 and 1997, however since 2011 the ASEAN Festival had been organized between local Radio stations as ''Bintang Radio ASEAN''.

In November 2011, the [[Asia-Pacific Broadcasting Union|ABU]] announced that they would organize its own TV and Radio Song Festivals to take place in [[Seoul]], the South Korean capital, in time with 49th General Assembly in October 2012.<ref>{{cite web|title=ABU to launch 'Asiavision Song Contest'|url=http://www.eurovision.tv/page/news?id=39913&_t=abu_to_launch_asiavision_song_contest|publisher=[[EBU]]|date=8 November 2011|accessdate=9 June 2012}}</ref> The name ''Asiavision Song Contest'' was initially mentioned as a possibility, but they were later officially titled ''[[ABU TV Song Festival]]'' and ''[[ABU Radio Song Festival]]''. According to the ABU, the deadline for participation applications for ABU TV Song Festival was 18 May 2012.<ref name=RunningOrder/><ref>{{cite web|title=ABU to launch 'Asiavision Song Contest'|url=http://www.eurovision.tv/page/news?id=39913&_t=abu_to_launch_asiavision_song_contest|publisher=[[EBU]]|date=8 November 2011|accessdate=9 June 2012}}</ref>

== Location ==
{{details|topic=the host city|Seoul}}
{{location map+|South Korea
| width   = 150
| float   = left
| caption = Location of the host city in [[South Korea]].
| places  =
{{location map~|South Korea|lat=37.566536|long=126.977969|label=[[Seoul]]|position=right}}
}}

Seoul, officially the Seoul Special City, is the capital and largest [[metropolis]] of [[South Korea]]. A [[megacity]] with a population of over 10 million, it is the [[List of cities proper by population|largest city proper]] in the [[OECD]] [[developed world]].<ref name="CitypopKoreaSouth">{{cite web|url=http://www.citypopulation.de/KoreaSouth-MunRegPop.html|first=Thomas|last=Brinkhoff|publisher=www.citypopulation.de|title=South Korea, The registered population of the South Korean provinces and urban municipalities|date=31 December 2007|accessdate=23 October 2012}}</ref> The [[Seoul National Capital Area]] is the world's [[List of metropolitan areas by population|second largest metropolitan area]] with over 25&nbsp;million<ref>{{cite web|url=http://www.index.go.kr/egams/stts/jsp/potal/stts/PO_STTS_IdxMain.jsp?idx_cd=2729|title=Current population of the Seoul National Capital Area|publisher=Statistics Korea|accessdate=23 October 2012|date=9 July 2012}}</ref> inhabitants, which includes the surrounding [[Incheon]] metropolis and [[Gyeonggi-do|Gyeonggi]] province. Almost a quarter of South Koreans live in Seoul, half of South Koreans live in the metropolitan area, along with over 275,000 international residents.<ref>{{cite web|title=About Seoul|url=http://english.seoul.go.kr/gtk/about/fact.php?pidx=1|publisher=Infinitely yours, Seoul|date=10 May 2012|accessdate=25 August 2012}}</ref>

Located on the [[Han River (Korea)|Han River]], Seoul has been a major settlement for over 2,000 years, with its foundation dating back to 18 [[Anno Domini|B.C.]] when [[Baekje]], one of the [[Three Kingdoms of Korea]], established its capital in what is now south-east Seoul. It continued as the capital of [[Korea]] during the [[Joseon Dynasty]] and the [[Korean Empire]]. The Seoul National Capital Area is home to four [[UNESCO World Heritage Sites]]: [[Changdeokgung]], [[Hwaseong Fortress]], [[Jongmyo|Jongmyo Shrine]] and the [[Royal Tombs of the Joseon Dynasty]].<ref>{{cite web|url=http://whc.unesco.org/en/statesparties/kr|title=Lists: Republic of Korea |publisher=[[UNESCO]]|accessdate=23 October 2012|date=26 June 2009}}</ref>
{{clear}}

===National host broadcaster===
[[Korean Broadcasting System]] (KBS) was the host broadcaster for the first edition of the annual TV Song Festival, which was staged in the [[KBS Hall|KBS Concert Hall]]. The host broadcaster offered to cover costs for staging the show as well as the accommodation for the participants of ABU TV Song Festival.<ref name=Seoul2012/><ref name=ASF2012>{{cite web|title=ABU Song Festival 2012|url=http://aburadiosongfestival.asia|publisher=[[Asia-Pacific Broadcasting Union]]|accessdate=16 August 2012}}</ref>

== Format ==
[[File:Havana-Brown2012.jpg|150px|thumb|right|Havana Brown, participant from Australia]]
[[File:Kpop World Festival 113.jpg|200px|thumb|right|TVXQ, participant from South Korea]]
Unlike the format used in the [[Eurovision Song Contest]] there are two versions of the ABU Song Festival.  The [[ABU Radio Song Festival 2012]] and the ABU TV Song Festival 2012, which were both scheduled to take place between 11 and 17 October 2012 during the 49th ABU General Assembly.<ref>{{cite web|title=KBS to Host 49th ABU General Assembly|url=http://rki.kbs.co.kr/english/news/news_Cu_detail.htm?No=91427&id=Cu|publisher=[[KBS World]]|accessdate=17 August 2012|date=2 July 2012}}</ref>  During the TV Festival, participants from eleven countries performed a song from their [[repertoire]] in a musical gala presentation.<ref name=Seoul2012/>  The theme for the festivals was 'Beyond the Wave', which has been inspired by the digital evolution changes in the global media.<ref>{{cite web|title=ABU 2012 Seoul…’Beyond the Wave’|url=http://www.theasian.asia/archives/25076|publisher=The Asian|accessdate=17 August 2012|date=24 July 2012}}</ref>

== Participating countries ==
{{Details|ABU TV Song Festival#Participation}}
Eleven entries participated in the final of the ABU TV Song Festival (as shown in the table below).<ref name="ABU TV"/> [[Mongolia]] had initially selected Naran with the song "Nudnii shil (Shades)", but subsequently withdrew their participation on 14 September 2012.<ref name="ABU TV"/>
{| class="sortable wikitable"
|-
! Draw<ref name=RunningOrder/>
! Country
! Language
! Artist
! Song
! {{nowrap|English translation}}
|-
| 01
| {{Abu|Singapore|TV}}
| [[Malay language|Malay]]
| [[Taufik Batisah]]
| "Usah Lepaskan"
| Don't Let Go
|-
| 02
| {{Abu|Australia|TV}}
| [[English language|English]]
| [[Havana Brown (DJ)|Havana Brown]]
| "[[We Run the Night]]"
| —
|-
| 03
| {{Abu|Sri Lanka|TV}}
| [[Sinhala language|Sinhalese]]
| Arjuna Rookantha <br>& Shanika Madhumali
| "Me Jeewithe (මේජීවිතේ)"
| This Life
|-
| 04
| {{Abu|Malaysia|TV}}
| [[Malay language|Malay]]
| [[Hafiz (Malaysian singer)|Hafiz]]
| "Awan Nano"
| Nano-Clouds
|-
| 05
| {{Abu|Vietnam|TV}}
| [[Vietnamese language|Vietnamese]]
| Lê Việt Anh
| "Mây"
| Cloud
|-
| 06
| {{Abu|Japan|TV}}
| [[Japanese language|Japanese]], English
| [[Perfume (Japanese band)|Perfume]]
| "[[Spring of Life (Perfume single)|Spring of Life]]"
| —
|-
| 07
| {{Abu|Hong Kong|TV}}
| [[Cantonese]]
| [[Alfred Hui]] (許廷鏗)
| "Ma Ngai "
| The Ant
|-
| 08
| {{Abu|Indonesia|TV}}
| [[Indonesian language|Indonesian]]
| Maria Calista
| "Karena Ku Sanggup"
| Because I Can
|-
| 09
| {{Abu|China|TV}}
| [[Standard Chinese|Mandarin]]
| Cao Fujia (曹芙嘉)
| "Qian gua (牵挂)"
| Don't Worry
|-
| 10
| {{Abu|Afghanistan|TV}}
| [[Persian language|Persian]]{{ref|a|1}}
| Hameed Sakhizada (حمید سخی زاده)
| "Folk Music" (Malestani)
| My Eye
|-
| 11
| {{Abu|South Korea|TV}}
| [[Korean language|Korean]], English 
| [[TVXQ]] (동방신기)
| "[[Catch Me (TVXQ song)|Catch Me]]"
| —
|-
|}
'''Notes'''
: 1.{{note|a}}Specifically [[Hazaragi dialect|Hazaragi]], a [[Persian language|Persian]] dialect spoken in [[Hazarajat]], in central Afghanistan.

== International broadcasts ==
Each participating country were invited to broadcast both events across their respective networks and provide commentary in the native languages to add insight and description to the shows.<ref name=Participants/>  The festival was not broadcast live, although each broadcaster has stated that they will broadcast the festival between October and November 2012 with an estimated audience of 2 billion people, twenty times the audience that is reached by the [[Eurovision Song Contest]] which reaches an audience of approximately 100 million people.<ref name="Broadcast schedules">{{cite web|last=Mikheev|first=Andy|title=Broadcasting schedules|url=http://esckaz.com/2012/abu.htm#news|work=ABU TV Song Festival 2012|publisher=ESCKaz|date=16 October 2012|accessdate=21 October 2012}}</ref>

{{Div col|cols=2}}
* {{flagicon|Afghanistan}} Afghanistan &ndash; [[Radio Television Afghanistan]]
* {{flagicon|Australia}} Australia &ndash; [[SBS One]] (28 October 2012) / [[SBS Two]] (1 November 2012)<ref name="Broadcast schedules"/>
* {{flagicon|China}} China &ndash; [[China Central Television]]
* {{flagicon|Hong Kong}} Hong Kong &ndash; [[TVB|Television Broadcasts Limited]] (10 November 2012)
* {{flagicon|Indonesia}} Indonesia &ndash; [[TVRI|Televisi Republik Indonesia]] (3 November 2012)
* {{flagicon|Japan}} Japan &ndash; [[NHK|Japanese Broadcasting Corporation]]
* {{flagicon|Malaysia}} Malaysia &ndash; [[Radio Televisyen Malaysia]] (4 January 2013)
* {{flagicon|Singapore}} Singapore &ndash; [[MediaCorp Suria]] (November 2012)<ref name="Broadcast schedules"/>
* {{flagicon|South Korea}} South Korea &ndash; [[KBS 1TV]] (21 October 2012)<ref name="Broadcast schedules"/>
* {{flagicon|Sri Lanka}} Sri Lanka &ndash; [[MTV Channel]]
* {{flagicon|Vietnam}} Vietnam &ndash; [[Vietnam Television]]
{{Div col end}}
<!-- NOTE: Any addition to this section must be sourced. -->

== See also ==
* [[ABU Song Festivals]]
* [[ABU Radio Song Festival 2012]]
* [[Asia-Pacific Broadcasting Union]]
* [[Eurovision Song Contest]]
* [[Eurovision Song Contest 2012]]
* [[Junior Eurovision Song Contest 2012]]

== References ==
{{Reflist|30em}}

== External links ==
*{{cite web |url=http://www.abu2012seoul.com/ |title=Archived copy |accessdate=17 August 2012 |deadurl=unfit |archiveurl=https://web.archive.org/web/20130303011118/http://www.abu2012seoul.com/ |archivedate=3 March 2013 }} 
* [http://esckaz.com/2012/abu_tv.htm ABU TV Song Festival at ESCKaz]

{{Navboxes
|title = Articles related to ABU TV Song Festival
|list  =
{{ABU TV Song Festival}}
{{ABU TV Song Festival 2012}}
}}
{{Music festivals in South Korea}}
{{Use dmy dates|date=May 2012}}

[[Category:2012 festivals in South Korea]]
[[Category:ABU Song Festivals]]
[[Category:21st century in Seoul]]
[[Category:2012 song contests]]
[[Category:Music festivals in South Korea]]