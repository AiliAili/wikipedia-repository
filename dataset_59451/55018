'''Sefer Ali-Bey Shervashidze''' (also known by the [[Christian]] name of '''Giorgi Shervashidze''') was a prince of the [[Principality of Abkhazia]] in 1810–21.  He was the youngest son of [[Kelesh Ahmed-Bey Shervashidze]].  

[[File:Sharvashidze.BMP.jpg|thumb|Coat of arms of the princely family of Shervashidze]]

After Sefer Ali-Bey’s brother, [[Aslan-Bey Shervashidze|Aslan-Bey]], killed his father<ref>George Hewitt, The Abkhazians, 1998, page 71, calls this a Russian fabrication. He presents Aslan-Bey as a popular ruler and Sefer-Bey as a foreign-backed usurper. He also calls Sefer-Bey illegitimate</ref>  to take over the throne of [[Abkhazia]],  Sefer Ali-Bey was forced to hide out in neighboring [[Principality of Mingrelia|Mingrelia]] under the protection of the Mingrelian princess regent [[Nino, Princess of Mingrelia|Nino]].  With the help of the Mingrelian nobility, Sefer Ali-Bey tried unsuccessfully, to usurp the throne of Abkhazia.  In 1809, Shervashidze asked the [[Tsarist]] [[Russian Empire]] to take Abkhazia under its protection, with the condition that Ali-Bey be established as the new ruler of the Principality. After decisive Russian victories during the [[Russo-Turkish War (1806–1812)|Second Russo-Turkish War]], the Russian forces were able to expel pro-Turkish Abkhazians as well as the remaining Turkish forces from the region. [[Tsar Alexander I]] established Sefer Ali-Bey Shervashidze as the new ruler of Abkhazia on 17 February 1810. He died in 1821 and was buried at the [[Lykhny Church]].

==References==
* ''Georgian State (Soviet) Encyclopedia.'' 1983. Book 10. p.&nbsp;689.
{{reflist}}
{{s-start}}
{{s-hou|House of [[Shervashidze]]/Chachba}}
{{s-reg|}}
{{succession box | title=[[Principality of Abkhazia|Prince of Abkhazia]] | before=[[Aslan-Bey Shervashidze|Aslan-Bey]] | after=[[Dmitry, Prince of Abkhazia |Dmitry]] | years=1810–1821}}
{{s-end}}

{{DEFAULTSORT:Shervashidze, Sefer Ali-Bey}}
[[Category:Year of birth missing]]
[[Category:1821 deaths]]
[[Category:Abkhazian former Muslims]]
[[Category:Princes of Abkhazia]]
[[Category:Converts to Eastern Orthodoxy from Islam]]


{{abkhazia-bio-stub}}