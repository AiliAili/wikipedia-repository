{{Use mdy dates|date=August 2016}}
<!--This article uses the Cite.php citation mechanism. If you would like more information on how to add references to this article, please see http://meta.wikimedia.org/wiki/Cite/Cite.php (Please format according to [[:Template:Cite web]], and [[Wikipedia:Citing sources]]) Thanks, the members of WikiProject Chicago.-->
{{Infobox person
| image = 
| image_size = 150px |
| name        = Peter Bynoe
| caption     = 
| birth_name  = 
| birth_date  = {{Birth date and age|1951|3|20}}
| birth_place = [[Boston, Massachusetts]]
| death_date  = 
| death_place = 
| death_cause = 
| resting_place = 
| resting_place_coordinates = 
| residence   = [[Chicago, Illinois]]
| nationality = 
| other_names = 
| known_for   = 
| education   = [[Juris Doctor|J.D.]], [[Harvard Law School]]<br>[[M.B.A.]], [[Harvard Business School]]<br>[[Bachelor of Arts|B.A.]], [[Harvard College]]
| employer    = [[Rewards Network]] 
| occupation  = [[Lawyer]], businessman
| title       = [[CEO]] 
| salary      = 
| networth    = 
| height      = 
| weight      = 
| term        = 
| predecessor = 
| successor   = 
| party       = 
| boards      = [[Frontier Communications Corporation]], [[Covanta Holding Company]], Signature Group Holdings, Inc. 
| religion    = 
| spouse      = Linda Jean Walker (November 20, 1987)
| partner     = 
| children    = 
| parents     = Ethel May Stewart and Victor Cameron Bynoe, Sr.
| relatives   = 
| signature   = 
| website     = 
| footnotes   = 
}}
'''Peter Charles Bernard Bynoe''' (born March 20, 1951) is a Chicago [[Lawyer|attorney]] and businessman, formerly the only African-American equity partner in the Chicago office of DLA Piper.<ref>{{cite news|title=Diversity matters as minority students aim for the big firms|author=Kantzavelos, Maria|work=Chicago Lawyer|date=July 2007|page= 219}}</ref>  In 1989, he and his business partner Bertram Lee were the first African-Americans to buy a controlling interest in a [[National Basketball Association]] (NBA) team, when they purchased a 37.5% share of the [[Denver Nuggets]] [[basketball]] team,<ref name=PBB>{{cite web|url=http://www.thehistorymakers.com/timeline?year=1989&month_day=0710&month=07&day=10|accessdate=November 1, 2007|publisher=The HistoryMakers|title=Peter Bynoe Biography|date=March 28, 2000}}</ref> and he is among the most influential minority figures in sports law and management.<ref>{{cite web|url=http://www.ivyleaguesports.com/article.asp?intID=2408|accessdate=October 31, 2007|title=The Ivy Dozen|date=May 23, 2003|publisher=ivyleaguesports.com |archiveurl = https://web.archive.org/web/20070430173952/http://www.ivyleaguesports.com/article.asp?intID=2408 |archivedate = April 30, 2007}}</ref><ref>{{cite web|url=http://www.timewarner.com/corp/newsroom/pr/0,20812,1091282,00.html|accessdate=November 1, 2007|title=Fortune Announces 2005 Diversity List|date=August 8, 2005|publisher=Time Warner}}</ref>

Bynoe kept the [[Chicago White Sox]] from leaving Chicago by developing a [[New Comiskey Park]] (now known as [[U.S. Cellular Field]]).  He has become a negotiator for professional sports teams' venues.  In addition, he was involved in the development of the [[1996 Summer Olympics|1996]] and [[2012 Summer Olympics]]. Bynoe serves on several boards of directors.

==Personal background==
Born in [[Boston, Massachusetts]],<ref name=BBPCBB>{{cite web|url=http://www.answers.com/topic/peter-c-b-bynoe|title=Black Biography: Peter C. B. Bynoe|accessdate=September 2, 2008|publisher=Answers Corporation|work=[[Answers.com]]}}</ref> Bynoe came from a well-respected family.  His father Victor C. Bynoe, had emigrated from his native [[Barbados]] at age 13, and after graduating from [[Northeastern University]] with degrees in [[Civil Engineering]] and [[Law]], became a successful [[Lawyer|attorney]]. (At one point, he represented [[Boston Celtics]] star [[Bill Russell]].)<ref name=LLN/> At a time when few blacks were being promoted to important positions in government, Victor Bynoe was named to [[Boston Mayor]] [[John Hynes (politician)|John B. Hynes]]' cabinet in 1950, serving on the Street Commission.<ref>{{cite news|title=Gets High City Post in Boston|work=[[Chicago Defender]]|date=January 7, 1950|page=1}}</ref>  He later became commissioner of Veterans' Services in Boston, and served on the board of the Boston Housing Authority.<ref>{{cite news|title=Victor C. Bynoe, 80, Was Lawyer, Engineer, Boston Public Servant.|work=[[Boston Globe]]|date=August 13, 1994|page=19}}</ref>  Peter's mother Ethel was [[United States|American]];<ref name=PBB/> she was praised by the black newspapers for her community involvement and volunteerism.  Peter also had an uncle John Bynoe, who was active in the fight for [[civil rights]], serving as the regional civil rights director for the [[United States Department of Health, Education and Welfare]].<ref>{{cite news|title=Boston's Lee Gets Nuggets|work=[[Boston Globe]]|date=July 11, 1989|page=61}}</ref>

Bynoe attended William Lloyd Garrison Elementary School, and graduated from [[Boston Latin School]] in 1968.<ref>{{cite news|title=Class Acts:  Most Likely To...|author=Tong, Betsy Q.M. |work=[[Boston Globe]]|date=June 12, 1994|page=14}}</ref>  He credited his parents with instilling high standards and expecting him to achieve in school.<ref name=BBPCBB/>  He also had a love of sports, growing up as a regular [[Boston Celtics]] and [[Boston Red Sox]] fan; but although both teams had black players, racial relations in Boston at that time were still tense, and black fans like Bynoe were hesitant to attend the games.<ref>{{cite news|title=In Racism's Shadow|author=Fainaru, Steve |work=[[Boston Globe]]|date=August 4, 1991|page=1}}</ref>

Bynoe's entire post-secondary education was at [[Harvard University]] in [[Cambridge, Massachusetts]].  He obtained his [[bachelor's degree]] ''[[cum laude]]'' at [[Harvard College]] in 1972.<ref name=DLAP>{{cite web|url=http://www.dlapiper.com/peter_bynoe/|accessdate=October 31, 2007|publisher=DLA Piper|title=Our People:  Peter C.B. Bynoe, Partner}}</ref> He then received his [[master's degree]], an [[M.B.A.]] in 1976, with an emphasis on [[finance]] and [[marketing]], from [[Harvard Business School]]. His [[Juris Doctor]] from [[Harvard Law School]] had a focus on corporate planning and regulation. In addition to being admitted to the practice of law before the Illinois State Bar, Bynoe is a licensed [[real estate broker]] in Illinois.<ref name=DLAP/>

In late November 1987, he married Linda Jean Walker, who was then vice-president of the [[Fixed income|fixed-income]] division of [[Morgan Stanley]] in New York.<ref>{{cite news|title=Wedding Planned by Linda Walker|work=[[New York Times]]|date=October 4, 1987|page=66}}</ref>

==Business career==
[[File:U.S. Cellular Field14.jpg|thumb|Bynoe oversaw the development of [[U.S. Cellular Field]] to keep the [[Chicago White Sox]] from leaving Chicago.]]
Bynoe began his professional career in 1976 at [[Citibank]].<ref name=PBB/> After deciding he didn't like working for a large corporation, he left New York and moved to Chicago in 1977. He was hired by James Lowry, a management consultant whom he had met at Harvard.  While with Lowry's firm, Bynoe made a name for himself as someone who could break down racial barriers and get diverse groups to work together.  He also became known for his work in minority business development.<ref name=EBafsibl>{{cite news|title=Energetic Bynoe aims for stardom in 'big leagues'|author=Merrion, Paul |work=Crain's Chicago Business|date=August 14, 1989|page=13}}</ref>  By 1982, Bynoe passed the Illinois [[bar examination]].<ref name="Illinois lawyer registration">{{cite web|url=http://www.iardc.org/ldetail.asp?id=437883198 |title=Lawyer Search: Attorney's Registration and Public Disciplinary Record for Peter Charles Bernard Bynoe  |publisher=Attorney Registration & Disciplinary Commission of the Supreme Court of Illinois |date=May 16, 2008 |accessdate=May 18, 2008}}</ref> He left Lowry and Associates in 1982, at which time he founded and managed Telemat Ltd., a business consulting firm.

His career path changed in 1987 when [[Harold Washington]], whose election campaign he had worked for and contributed to, called Bynoe to inform him that the Chicago White Sox were threatening to leave Chicago.<ref name=LLN>Picher, Keith, ''Leading Lawyers Network'', "Peter Bynoe: When It Comes to Sports Stadiums, He Takes The Ball And Runs With It," September 2007</ref> From January 1988 to June 1992, Bynoe served as the Executive Director of the Illinois Sports Facilities Authority, a joint venture of the City of Chicago and State of [[Illinois]], which gave him complete supervisory, planning and executing responsibility for the $250 million construction of [[New Comiskey Park]] for the Chicago White Sox, which was completed as scheduled and well within its budget.<ref name=DLAP/>  He also became known in Chicago for his business relationships with such influential sports stars as [[Michael Jordan]], a former client with whom he remained friends.<ref>{{cite news|title=Leading Normal Life Comes With Practice|author=Drell, Adrienne |work=[[Chicago Sun-Times]]|date=March 24, 1995|page=30}}</ref><ref>{{cite news|title=Lee, Bynoe Seek New Era in Denver|author=MacMullan, Jackie |work=[[Boston Globe]]|date=July 19, 1989|page=47}}</ref><ref>{{cite news|title=Blacks Get Executive-Suite Toehold: Nuggets purchase could start wave of minority-owned teams|author=Wayne, Jamie |work=[[The Financial Post]]|date=July 11, 1989|page=43|quote=You have Probably never heard of Bertram Lee and Peter Bynoe, but they have earned a spot in sports history alongside Jackie Robinson, the first black man admitted to major league baseball. The duo head a group of black investors that bought the Denver Nuggets of the National Basketball Association for US$65 million, breaking down the color barrier to sports executive suites. The Nuggets are now the first minority-owned major league professional franchise in the U.S.}}</ref><ref>{{cite news|title=Economic Lessons|work=[[The New York Times]]|publisher=[[The New York Times Company]]|date=July 16, 1989|page=7|url=https://query.nytimes.com/gst/fullpage.html?res=950DE0D91F39F935A25754C0A96F948260}}</ref><ref>{{cite news|title=Denver Nuggets Become First Pro Sports Team Owned By Blacks|work=[[Christian Science Monitor]]|date=July 12, 1989|page=7|publisher=[[Associated Press]]|quote=Bertram Lee and Peter Bynoe became the first black owners of a major professional sports franchise by purchasing the Denver Nuggets basketball team. Former Nuggets owner Sidney Shlenker on Monday acknowledged the $65 million sale, which must be confirmed by National Basketball Association owners. Mr. Lee owns television and radio stations in Washington, D.C., Utah, and Nebraska and is also chairman of BML Associates Inc., an investment holding company in Boston. Mr. Bynoe was the executive director of the Illinois Sports Facilities Authority, which is building the new home of the Chicago White Sox.}}</ref>

In November 1989, Bynoe and Bertram Lee purchased the Denver Nuggets. Lee and Bynoe had worked together in Boston years earlier, and Bynoe thought of Lee as a mentor.  They had kept in touch, and were involved in several real estate dealings together.<ref name=EBafsibl/> But the Nuggets deal was problematic from the start.  In fact, it almost fell through because Lee did not have the resources everyone, Bynoe among them, thought he had.  Media reports depicted Lee as a wealthy and successful businessman who was a financier with offices in two cities, and formerly part owner of Boston's [[WNEV-TV]].<ref>{{cite news|title=Blacks Get Executive Suite Toe-Hold|author=Wayne, Jamie |work=[[Toronto Financial Post]]|date=July 12, 1989|page=43}}</ref>  But Lee and Bynoe were unable to meet the deadline to purchase the team from then-owner [[Sidney Shlenker]].  It would later be revealed that Lee had problems raising the needed capital.<ref name=NPOEFHBA>{{cite news|title=Nuggets Part Owner Evicted From His Boston Apartment|author=Cooper, Barry |work=[[Pittsburgh Courier]]|date=May 11, 1991|page=6}}</ref>  It was Bynoe, with the assistance of [[David Stern]], [[Jay Pritzker]], and [[Jerrold Wexler]], who salvaged the transaction over the course of six months.<ref name=LLN/> The mainstream media reported that Lee and Bynoe were now the first two blacks to own a major sports team, but several of the black newspapers pointed out that the two men were actually managing general partners rather than owners, because the revised agreement to purchase the team meant they owned only a 37.5% stake in the team. Bynoe sold his interest in the franchise in August 1992.<ref name=DLAP/> [[Communications Satellite Corporation]]'s (COMSAT) Video Enterprises subsidiary put up 62.5% of the [[U.S. Dollar|$]]65 million needed to purchase the Nuggets.<ref name=ESPN>{{cite web |url=http://static.espn.go.com/nba/news/2002/1217/1478643.html |accessdate=November 2, 2007 |publisher=ESPN.com |title=Johnson will be NBA's first black majority owner |date=December 18, 2002}}</ref>

Bynoe was hired in 2008 by Chicago-based Loop Capital Markets LLC.<ref>{{cite web|url=http://www.bondbuyer.com/issues/117_150/-293202-1.html|title=New Hires at Loop Capital Markets|accessdate=January 6, 2010|date=August 7, 2008|author=Shields, Yvette|work=[[The Bond Buyer]]}}</ref>  Bynoe serves as Managing Director of Corporate Finance.<ref>{{cite web|url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=3157486|title=Loop Capital Markets, LLC|accessdate=January 6, 2010|publisher=[[Bloomberg L.P.]]}}</ref>

In August 2013, Bynoe left his executive role at Loop Capital Markets LLC to become CEO of [[Rewards Network]], a company controlled by investor [[Sam Zell]] that offers loyalty programs for restaurants.<ref>{{cite web|url=http://www.chicagobusiness.com/article/20130830/NEWS07/130839986/zell-controlled-firm-names-peter-bynoe-ceo|title=Zell-controlled firm names Peter Bynoe CEO|access-date=December 16, 2015}}</ref>

==Other achievements==
[[File:Turner field Braves.jpg|thumb|Bynoe consulted on the development of [[Centennial Olympic Stadium]], which eventually was converted into [[Turner Field]], shown here.]]
Bynoe served as a consultant to the Atlanta-Fulton County Recreation Authority and the Atlanta Committee to Organize the Olympic Games to plan the 85,000 seat [[Centennial Olympic Stadium]] within a $210 million construction budget.  The stadium has been converted to the 45,000 seat [[Turner Field]] baseball stadium for the [[Atlanta Braves]] who use it under a lease agreement Bynoe negotiated.<ref name=DLAP/>   Bynoe became a partner at DLA Piper Rudnick Gray Cary LLP (US) in 1995 when he joined the firm and remains a member of the firm's Executive Committee.<ref name=CH/> Bynoe as head of DLA Piper's Sports Facilities Practice Group negotiated new facilities for the [[Cincinnati Reds]] and [[Cincinnati Bengals|Bengals]], [[Miami Heat]], [[Washington Redskins]], [[Milwaukee Brewers]], and [[Columbus Blue Jackets]].<ref name=DLAP/> He specialized in [[infrastructure]] projects and represented institutional clients such as: [[The Boeing Company]], [[Sara Lee Corporation]], [[Essence Communications]], and [[CNA Insurance]].<ref name=DLAP/>  Bynoe also was involved in the [[2012 Summer Olympics]], and was a backer of [[Barack Obama]].<ref name=LLN/>

In addition, Bynoe is a Director of Frontier Communications Corporation [http://www.frontier.com/], Covanta Holding Company [[www.covantaenergy.com]] and Signature Group Holdings, Inc. [http://www.signaturegroupholdings.com]. He is a Trustee of the Goodman Theatre, the Rush University Medical Center and The CORE Center for the Research and Cure of Infectious Diseases. He formerly served as Chairman of the Chicago Plan Commission [[Chicago Plan Commission]], the Chicago Landmarks Commission [[Commission on Chicago Landmarks|Chicago Commission Landmarks]],  and the Illinois Sports Facilities Authority, and was a director of Jacor Communications and Blue Chip Broadcasting.  Bynoe has been a director of [[Covanta]] since 2004.<ref name=CH>{{cite web|url=http://www.covantaholding.com/site/management/peter-cb-bynoe.html|accessdate=November 1, 2007|title=Peter C.B. Bynoe, Director}}</ref><ref>{{cite news|url=http://www.forbes.com/finance/mktguideapps/personinfo/FromPersonIdPersonTearsheet.jhtml?passedPersonId=935191|accessdate=October 31, 2007|title= Peter C Bynoe |publisher=Forbes.com LLC}} {{Dead link|date=September 2013|bot=RjwilmsiBot}}</ref> In 2007, Bynoe was announced as a director <ref name=PCB/> for Citizens Communications (which later changed its name to [[Frontier Communications]]).  In the past, Bynoe has been a member of the [[Harvard Board of Overseers]].<ref>{{cite web|url=http://myway.frontiernet.net/downloads/BynoePR.pdf |accessdate=November 1, 2007 |title=Citizens Communications Appoints Peter C.B. Bynoe to Board of Directors |publisher=Citizens Communications |date=July 30, 2007 |author=Smith, Brigid |format=PDF |deadurl=yes |archiveurl=https://web.archive.org/web/20081029064311/http://myway.frontiernet.net/downloads/BynoePR.pdf |archivedate=October 29, 2008 |df=mdy }}</ref>  He has also served as a director for Uniroyal Technology Corporation, [[Jacor Communications]] (acquired by Clear Channel Communications), J&G Industries, Huffman-Koos, Inc., the River Valley Savings Bank, and Blue Chip Broadcasting (acquired by [[Radio One (company)|Radio One, Inc]]).<ref name=DLAP/>

== Honors ==
Bynoe has been designated an Illinois "Super Lawyer" for 2005 and 2006 based on research jointly conducted by ''Law & Politics'' and ''[[Chicago magazine]]''s.<ref name=PCB>{{cite news|url=http://www.forbes.com/finance/mktguideapps/personinfo/FromPersonIdPersonTearsheet.jhtml?passedPersonId=1124435|accessdate=November 1, 2007|publisher=Forbes.com LLC|title=Peter C Bynoe|year=2007}} {{Dead link|date=September 2013|bot=RjwilmsiBot}}</ref>

==See also==
*[[List of JD/MBAs]]

==Notes==
{{Reflist|2}}

{{good article}}

{{DEFAULTSORT:Bynoe, Peter}}
[[Category:1951 births]]
[[Category:Living people]]
[[Category:African-American lawyers]]
[[Category:African-American sports executives and administrators]]
[[Category:Denver Nuggets owners]]
[[Category:Harvard Business School alumni]]
[[Category:Harvard Law School alumni]]
[[Category:Harvard University alumni]]
[[Category:National Basketball Association executives]]
[[Category:National Basketball Association owners]]
[[Category:Lawyers from Boston]]
[[Category:Lawyers from Chicago]]