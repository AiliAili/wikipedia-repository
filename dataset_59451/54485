{{ Infobox book
| name          = A Dream of John Ball
| image         = William.Morris.John.Ball.jpg
| caption       = Artwork from the original 1888 edition.
| author        = [[William Morris]]
| illustrator   = [[Edward Burne-Jones]] (frontispiece)
| cover_artist  = 
| country       = United Kingdom
| language      = English
| genre         = [[Historical fantasy]]<br>[[Science fiction]]<br>[[Time travel]]
| published     = 1888
| media_type    = Print ([[Hardcover]] & [[Paperback]]) & [[E-book]]
| pages         = c. 300 pp (may change depending on the publisher and the size of the text)
| isbn          =
}}

'''''A Dream of John Ball''''' ([[1888 in literature|1888]]) is a novel by English author [[William Morris]] about the [[English peasants' revolt of 1381|Great Revolt of 1381]], conventionally, but incorrectly, called "the Peasants' Revolt", since few of the participants were actual peasants.  It features the rebel priest [[John Ball (priest)|John Ball]], who was accused of being a [[Lollard]]. He is famed for his question "When Adam delved and Eve span, who was then the gentleman?"<ref name=Froissart>Froissart, although unsympathetic to the rebels, gives a detailed, albeit imaginary (see Dobson, R.B. ''The Peasants' Revolt of 1381'', (Macmillan, 2nd ed, p. 369)), account of Ball's preaching.
* ''Et, se venons tout d'un père et d'une mere, Adam et Eve, en quoi poent il dire ne monstrer que il sont mieux signeur que nous, fors parce que il nous font gaaignier et labourer ce que il despendent?  Il sont vestu de velours et de camocas fourés de vair et de gris, et nous sommes vesti de povres draps.  Il ont les vins, les espisses et les bons pains, et nous avons le soille, le retrait et le paille, et buvons l'aige.  Ils ont le sejour et les biaux manoirs, et nous avons le paine et le travail, et le pleue et le vent as camps, et faut que de nous viengne et de nostre labeur ce dont il tiennent les estas.''
** If we all spring from a single father and mother, Adam and Eve, how can they claim or prove that they are lords more than us, except by making us produce and grow the wealth which they spend?  They are clad in velvet and [[w:Camlet|camlet]] lined with squirrel and ermine, while we go dressed in coarse cloth.  They have the wines, the spices and the good bread: we have the rye, the husks and the straw, and we drink water.  They have shelter and ease in their fine manors, and we have hardship and toil, the wind and the rain in the fields.  And from us must come, from our labour, the things which keep them in luxury
Book 2, p. 212. Froissart. [[q:Jean Froissart]]</ref>

== Depiction of the Middle Ages ==
Morris draws on [[Froissart]] for information on the fourteenth century,<ref name=Froissart /> but has a different attitude towards the revolting peasants from the chronicler who, [[Sir Walter Scott]] once remarked, had "marvelous little sympathy" for the "villain churls."<ref>[[w:John Graham, 1st Viscount Dundee|Claverhouse]], in [[Walter Scott]]'s ''[[w:Old Mortality|Old Mortality]]'' (1816), ch. 35.</ref>
Morris was also aware of interpretations of the Peasants' Revolt as representing a socialist tradition.  In 1884 he had written an article in which he stated that "we need make no mistake about the cause for which [[Wat Tyler]] and his worthier associate John Ball fell; they were fighting against the fleecing then in fashion, viz.; [[serfdom]] or villeinage, which was already beginning to wane before the advance of the industrial gilds."<ref name="Salmon">{{cite web | url=http://www.morrissociety.org/publications/JWMS/SP01.14.2.SalmonBall.pdf | title=A reassessment | accessdate=9 December 2013 | author=Salmon}}</ref>

The novel describes a dream and time travel encounter between the [[medieval]] and modern worlds, thus contrasting the ethics of medieval and contemporary culture. A time-traveller tells Ball of the decline of [[feudalism]] and the rise of the Industrial Revolution.  Ball realizes that in the nineteenth century his hopes for an egalitarian society have yet to be fulfilled.
A parallel can be drawn with the novel's close contemporary—''[[A Connecticut Yankee in King Arthur's Court]]'' (1889) by [[Mark Twain]]. Unlike Twain's novel, which depicted [[History of Anglo-Saxon England|early-Medieval England]] as a violent and chaotic [[Dark Age]], Morris depicts the [[Middle Ages]] in a positive light, seeing it as a golden, if brief, period when peasants were prosperous and happy and guilds protected workers from exploitation. 
This positive portrayal of the Medieval period is a recurring theme in Morris' literary and artistic oeuvre, from the largely pastoral and craftsman based economies of the prose romances, to his similar dream vision of Britain's utopian future, ''[[News from Nowhere]]'' (1889).<ref>'''Bull, M''' (2005) ''Thinking Medieval: An Introduction to the Study of the Middle Ages'' London:Palgrave ISBN 978-1-4039-1294-7</ref>

==Publication history==
The story was originally published in serial format in the socialist weekly ''[[Commonweal (UK)|The Commonweal]]'', November 13, 1886 - January 22, 1887. It appeared in book form in 1888.

Kelmscott, Morris's [[private press]], published, in 1892, ''A Dream of John Ball and A King's Lesson''.<ref>''A king's lesson'' is a short story based on the life of [[Matthias Corvinus]], King of Hungary,</ref>

==Reaction==
John Ball continues to be cited as an example of an English socialist.<ref>[http://www.tes.co.uk/teaching-resource/John-Ball-s-Speech-to-Peasants-Influences-6193153/ John Ball's Speech to Peasants]</ref>

==See also==
{{portal|Novels}}

*''[[A Connecticut Yankee in King Arthur's Court]]'' (1889) by [[Mark Twain]]
*''[[Looking Backward]]'' (1888), by [[Edward Bellamy]]

==Footnotes==
{{reflist}}

==External links==
* [https://archive.org/search.php?query=title%3A%28John%20Ball%29%20creator%3A%28morris%2C%20william%29%20%20AND%20mediatype%3Atexts ''A Dream of John Ball''], scanned illustrated books from [[Internet Archive]] and [[Project Gutenberg]]

{{William Morris}}

{{DEFAULTSORT:Dream of John Ball, A}}
[[Category:1888 novels]]
[[Category:Novels about rebels]]
[[Category:Novels by William Morris]]
[[Category:Utopian novels]]
[[Category:Time travel novels]]
[[Category:19th-century British novels]]