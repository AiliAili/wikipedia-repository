{{Infobox book|<!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
|name=Der Schimmelreiter
|title_orig=
|image=Storm schimmelreiter cover.jpg
|caption=First edition cover
|author=[[Theodor Storm]]
|cover_artist=
|country=Germany
|language=German
|series=
|pages=150 (paperback)
|genre=Novella
|publisher=Paetel
|pub_date=1888
}}
'''''The Rider on the White Horse''''' (German: '''''Der Schimmelreiter''''') is a [[novella]] by German writer [[Theodor Storm]]. It is his last complete work, first published in 1888, the year of his death. The novella is Storm's best remembered and most widely read work, and considered by many to be his masterpiece.

It has been translated into English under titles ''The Dykemaster'', ''The Dikegrave'', and the most literal, ''The Rider on the White Horse'' ("[[Schimmel]]" being the German word for a [[Gray (horse)|gray or white horse]]).

== Plot summary ==
The novella tells the story of Hauke Haien, allegedly related to the author by a schoolmaster in a small town in [[North Frisia|Northern Frisia]]. Hauke is the son of a farmer and licensed surveyor, and does his best to learn his father's trade. He even learns [[Dutch language|Dutch]] so he can read a Dutch print of [[Euclid]]'s work on mathematics and geometry. Over time, he becomes very familiar with the [[Levee|dykes]] along the local coast, and begins to wonder if it would not be better to make them flatter on the sea side so as to reduce their [[windage]] during [[flood]]s.

When local ''[[Dijkgraaf (official)|Deichgraf]]'' Tede Volkerts fires one of his hands, Hauke applies for the job and is accepted. He soon becomes a great help for Volkerts, which makes Ole Peters, the senior hand, dislike him. Tensions rise even more when Hauke begins to show interest in the Deichgraf's daughter, Elke. Hauke even proposes marriage, but she wants to wait.

After the unexpected deaths of both Hauke's and Elke's fathers, the people of the village must choose a new Deichgraf. Hauke is actually already doing the work, but does not hold the necessary lands required for the position. However, Elke announces that they are engaged, and that he will soon hold her family's lands as well. With the traditionalists satisfied, Hauke becomes the new Deichgraf. However, the people soon start talking about his white horse, which they believe is a resurrected skeleton that used to be visible on a small island, but is now gone.

Meanwhile, Hauke begins to implement the changes to the form of the dykes that he envisioned since childhood. However, during a [[storm surge]] several years later, the older dykes break and Hauke has to witness Elke and their daughter, Wienke, being swept away by the water. In agony, he drives his white horse into the sea, yelling, "''Lord, take me, spare the others!''"

The novella ends with the schoolmaster recounting that after the flood, the mysterious horse skeleton was once again seen lying on the small island off the coast. Hauke Haien's dyke still stands, and has saved many lives in the hundred years since its creator's tragic demise. And the older ones in the village say that, on stormy nights, a ghostly rider on a white horse can sometimes be seen patrolling the dyke.

=== Characters ===
* '''Hauke Haien''', the main character, based on mathematician and astronomer [[Hans Momsen]]
* '''Elke Haien''' (née '''Volkerts'''), the old dyke master's daughter, and Hauke's wife
* '''Wienke Haien''', Hauke and Elke's mentally challenged daughter
* '''Tede Volkerts''', Elke's father, and dyke master prior to Hauke
* '''Ole Peters''', the old dyke master's senior hand, and Hauke's rival
* '''The Schoolmaster''', a man from the town who tells the story to the author a hundred years later

==Reception==
[[Thomas Mann]] called it a "tremendous tale, with which Storm took his conception of the novella, as epic sister of drama, to unprecedented heights".<ref>[http://www.angelclassics.com/storm/the-dykemaster-der-schimmelreiter/ angelclassics.com article on ''Der Schimmelreiter'']</ref> [[Michael Dirda]] says that it is arguably the greatest 19th century German novella.<ref>{{cite web| url=http://www.washingtonpost.com/wp-dyn/content/article/2009/03/04/AR2009030403793.html |title=Dirda Book Review: 'The Rider on the White Horse,' by Theodor Storm |publisher=Washington Post |author=Michael Dirda |date=March 5, 2009 |accessdate=April 30, 2012 }}</ref>

== Adaptations ==
''Der Schimmelreiter'' has been adapted into a movie three times, [[The Rider on the White Horse (1934 film)|in 1934]], in 1978, and again for television in 1984. There is also a stage version as an opera by [[Wilfried Hiller]], first performed in [[Kiel]] in 1998.

== References ==
{{Reflist}}

===Editions ===
*''The Rider on the White Horse and Selected Stories''. Translated by [[James Wright (poet)|James Wright]], [[New York Review of Books Classics]], 2009. Reprinted from Signet's Classics, 1964.
*''The Dykemaster'', translated by Denis Jackson, 1996.
*[https://archive.org/stream/harvardclassicss15elio#page/178/mode/2up ''The Rider on the White Horse''], translated by Margarete Münsterberg, [https://archive.org/details/harvardclassicss15elio ''Harvard Classics Shelf of Fiction, Vol XV''], 1917. Pg. 179
*[https://archive.org/stream/germanclassicsof11franuoft#page/224/mode/2up ''The Rider on the White Horse''], translated by Muriel Almon, [https://archive.org/details/germanclassicsof11franuoft ''The German Classics of the Nineteenth and Twentieth Centuries, Vol. 11''],  1914. Pg. 225

== External links ==
* [http://www.gutenberg.org/ebooks/19790 ''Der Schimmelreiter''] at [[Project Gutenberg]] (German)
* [http://tonysreadinglist.blogspot.com/2011/08/rider-on-storm.html ''Der Schimmelreiter'' on Tony's Reading List]
* {{librivox book | title=The Rider on the White Horse | author=Theodor STORM}}

{{DEFAULTSORT:Rider on the White Horse}}
[[Category:German novellas]]
[[Category:German novels]]
[[Category:German novels adapted into films]]
[[Category:1888 novels]]
[[Category:Gothic novels]]
[[Category:Novels by Theodor Storm]]
[[Category:Novels set in Germany]]
[[Category:North Frisia]]