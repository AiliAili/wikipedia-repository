The '''Botanical Society of Britain and Ireland''' ('''BSBI''') is a scientific society for the study of [[flora]], plant distribution and [[taxonomy (biology)|taxonomy]] relating to [[Great Britain]], [[Ireland]], the [[Channel Islands]] and the [[Isle of Man]]. The society was founded as the '''Botanical Society of London''' in 1836, and became the '''Botanical Society of the British Isles''', eventually changing to its current name in 2013. It includes both professional and amateur members and is the largest organisation devoted to [[botany]] in the [[British Isles]]. Its history is recounted in David Allen's book ''The Botanists''.<ref>{{Cite book |last1=Allen |first1=D.E. |year=1986 |title=The Botanists |publisher=St Paul's Biographies |location=Winchester |isbn=9780906795361 }}</ref>

The society publishes handbooks and journals, conducts national surveys and training events, and hosts conferences. It also awards grants and  bursaries, sets professional standards (with Field Identification Skills Certificates (FISCs)), and works in an advisory capacity for governments and [[NGO]]s.<ref>{{cite web|url=http://bsbi.org/ |title=BSBI |publisher=BSBI |date= |accessdate=2014-01-29}}</ref>

The society is managed by a council of elected members, and is a Registered Charity in England & Wales (212560) and Scotland (SC038675).

==Publications==
The BSBI has produced two [[Atlas of the British Flora|atlas]]es covering the distribution of vascular plants in Britain and Ireland.{{sfn|Perring|Walters|1962}}<ref>Preston, C.D., Pearman, D.A. & Dines, T. 2002. Atlas of the British & Irish Flora. Oxford University Press, Oxford.</ref>

It publishes a [[newsletter]], BSBI News (ISSN 0309-930X), that is distributed to members three times a year and is available online.<ref>{{cite web|url=http://www.watsonia.org.uk/html/bsbi_news_1.html |title=BSBI News 1 |publisher=Watsonia.org.uk |date= |accessdate=2014-01-29}}</ref>

The BSBI publishes a scientific periodical, ''[[New Journal of Botany]]'' (formerly ''Watsonia''). The journal has  a north-western European scope covering vascular plants, their taxonomy, biosystematics, ecology, distribution and conservation, as well as topics of a more general or historical nature".<ref name="bsbi">{{cite web|url=http://bsbi.org/publications |title=Publications |publisher=Bsbi.org |date= |accessdate=2014-01-29}}</ref>

The society produced the ''[[Atlas of the British Flora]]'' in 2002, the [[Vice-county Census Catalogue of the Vascular Plants of Great Britain]] in 2003, and publishes the ''BSBI Handbooks'' series.

===Handbook series===
The following Handbooks have been produced, with more promised for the future.<ref name="bsbi" />

# Sedges. (3rd edition) 2007. A.C. Jermy, D.A. Simpson, M.J.Y. Foley & M.S. Porter
# Umbellifers. 1980. [[T.G. Tutin]]
# Docks and Knotweeds. 1981. J.E. Lousley & D.H. Kent
# Willows and Poplars. 1984. R.D. Meikle
# Charophytes. 1986. J.A. Moore
# Crucifers. 1991. T.C.G. Rich
# Roses. 1993. G.G. Graham & A.L. Primavesi
# Pondweeds. 1995. C.D. Preston
# Dandelions. 1997. A.A. Dudman & A.J. Richards
# Sea Beans & Nickar Nuts. 2000. E.C. Nelson
# Water-starworts of Europe. 2008. R.V. Lansdown
# Fumitories. 2009. R.J. Murphy
# Grasses. 2009. T. Cope & A. Gray
# Whitebeams, Rowans and Service Trees. 2010. T.C.G. Rich, L. Houston, A. Robertson & M.C.F. Proctor.
# British Northern Hawkweeds. 2011. T.C.G. Rich & W. Scott.

===Publications dealing with rare plants===

The BSBI's attitudes to publication of details of locations of rare plants have changed over time. In 1991, publicly criticised the author John Fisher, for writing "A Colour Guide to Rare Wild Flowers", a book which gave details of the locations of a selection of rare plants, stating that it was not in the interests of conservation.<ref>Perring, Franklyn H. (1991) "Conservation News: A Colour Guide to Rare Wild Flowers" ''[http://archive.bsbi.org.uk/BSBINews58.pdf BSBI News No. 58]'' page 43</ref> Following this criticism, Fisher resigned his membership of the BSBI. Fourteen years later, David Pearman, the Society's General Secretary, contrasted the way in which Fisher was, as Pearman termed it, "hounded out", with the more open attitudes that had then taken hold.<ref>Pearman, David A. (2005) "Book Notes" ''[http://archive.bsbi.org.uk/BSBINews98.pdf BSBI News No. 98]'' pages 54-55</ref> In more recent times, the BSBI has produced or supported the production of a number of County Rare Plant Registers, books which list all known locations for all rare plants in their county of coverage.

==External links==
*[http://bsbi.org/ BSBI website]
*{{EW charity|212560}}
*[http://www.facebook.com/pages/Botanical-Society-of-the-British-Isles/107660322636898/ BSBI Facebook Group]

==References==
{{Reflist|30em}}

== Bibliography ==
{{refbegin}}
* {{cite book|last1=Perring|first1=Franklyn|last2=Walters|first2=Stuart Max|authorlink2=Max Walters|title=Atlas of the British flora|date=1962|publisher=[[Botanical Society of the British Isles]]|url=https://books.google.ca/books?redir_esc=y&id=nGU9AQAAIAAJ|ref=harv}}
{{refend}}

[[Category:Scientific societies]]
[[Category:Charities based in Bristol]]
[[Category:Botanical societies]]
[[Category:British Isles]]
[[Category:1836 establishments in the United Kingdom]]
[[Category:Scientific organizations established in 1836]]
[[Category:Scientific organisations based in the United Kingdom]]