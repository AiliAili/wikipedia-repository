{{Infobox journal
| title =  Analyst
| cover = [[File:CoverIssueAnalyst.jpg]]
| formernames = The Analyst
| discipline = [[Analytical chemistry]]
| editor = May Copsey
| abbreviation = Analyst
| website = http://pubs.rsc.org/en/journals/journalissues/an
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| impact = 4.107
| impact-year = 2014
| history = 1876–present
| frequency = Biweekly
| ISSN = 0003-2654
| eISSN = 1364-5528
| CODEN = ANALAO
| OCLC = 01481074
}}
'''''Analyst''''' is a biweekly [[peer-reviewed]] [[scientific journal]] covering all aspects of [[analytical chemistry]], [[bioanalysis]], and detection science. It is published by the [[Royal Society of Chemistry]]. The [[Editor-in-chief|executive editor]] is May Copsey.

''Analyst'' was established in 1876 by the [[Society for Analytical Chemistry]] as ''The Analyst'' and obtained its current name in 2009. According to the ''[[Journal Citation Reports]]'', it has a 2014 [[impact factor]] of 4.107.<ref name=WoS>{{cite book |year=2015 |chapter=Analyst |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> It is abstracted and indexed in [[MEDLINE]] and [[Analytical Abstracts]].

In 1999, the society closed the journal ''Analytical Communications'' because it felt that the material submitted to that journal would be best in a new communications section of ''Analyst''. Predecessor journals of ''Analytical Communications'' were:
* ''Proceedings of the Society for Analytical Chemistry'', 1964–1974
* ''Proceedings of the Analytical Division of the Chemical Society'', 1975–1979
* ''Analytical Proceedings'', 1980–1993
* ''Analytical Proceedings including Analytical Communications'', 1994–1995

== References ==
<references/>

== External links ==
* {{Official website|http://pubs.rsc.org/en/journals/journalissues/an}}

{{Royal Society of Chemistry|state=collapsed}}
{{DEFAULTSORT:Analyst}}
[[Category:Chemistry journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 1876]]
[[Category:English-language journals]]
[[Category:Biweekly journals]]
[[Category:1876 establishments in the United Kingdom]]