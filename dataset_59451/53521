'''James Wheeler Fleming''' (February 15, 1867, in [[Troy, New York|Troy]], [[Rensselaer County, New York]] &ndash; April 27, 1928, in Troy, Rensselaer County, New York) was an [[United States|American]] businessman, banker and politician.

==Life==
He was the son of James Fleming, a political lieutenant of Senator [[Edward Murphy, Jr.]] The Flemings were in the wholesale [[liquor]] business, but James W. Fleming sold it after the death of his father. He attended Troy Academy, and graduated from [[Rensselaer Polytechnic Institute]]. 

He was a director of the Manufacturers' National Bank of Troy, and of the Troy Gas Company. He was vice president of the Manhattan Navigation Company, and of the Casualty Company of America. 

On June 15, 1911, he was appointed New York State Forest, Fish and Game Commissioner to replace [[Thomas Mott Osborne]] who had resigned. On July 17, 1911, he was appointed one of three commissioners of the [[New York State Conservation Commission]], a body into which the Forest, Fish and Game Commission had been merged with the Water Supply Commission, the Forest Purchasing Board and the Black River Power Commission.

He was a delegate to the [[1912 Democratic National Convention|1912]], [[1920 Democratic National Convention|1920]] and [[1924 Democratic National Convention]]s. He was Mayor of [[Troy, New York]] in 1920 and 1921. He was [[New York State Comptroller]] from 1923 to 1924, elected in [[New York state election, 1922|1922]], but defeated for re-election in [[New York state election, 1924|1924]] by [[Vincent B. Murphy]].

==Sources==
*[http://politicalgraveyard.com/bio/fleming.html] Political Graveyard
*[https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9A05E0D61439E333A25755C1A9609C946096D6CF] Appointed Forest, Fish and Game Commissioner, in NYT on June 16, 1911
*[https://query.nytimes.com/mem/archive-free/pdf?res=9402EFD61431E233A2575BC1A9619C946096D6CF] Appointed Conservation Commissioner, in NYT on July 18, 1911
*[https://select.nytimes.com/gst/abstract.html?res=F50F1EF83D58127A93CAAB178FD85F4C8285F9] Obit in NYT on April 28, 1928 (subscription required)

{{start box}}
{{s-off}}
{{succession box | title = [[New York State Comptroller]] | before = [[William J. Maier]] | after = [[Vincent B. Murphy]] | years = 1923&ndash;1924}}
{{end box}}

{{NYSComptroller}}

{{DEFAULTSORT:Fleming, James Wheeler}}
[[Category:1867 births]]
[[Category:1928 deaths]]
[[Category:New York State Comptrollers]]
[[Category:People from Troy, New York]]
[[Category:Rensselaer Polytechnic Institute alumni]]
[[Category:American bankers]]