{{Infobox book
| name          = Le Chef-d’œuvre inconnu
| title_orig    =
| translator    =
| image         = BalzacUnknownMasterpiece.jpg
| caption = Illustration by Pierre Vidal of a scene from ''Le Chef-d’œuvre inconnu''
| author        = [[Honoré de Balzac]]
| illustrator   = Pierre Vidal
| cover_artist  =
| country       = [[France]]
| language      = [[French language|French]]
| series        = [[La Comédie humaine]]
| subject       =
| genre         =
| publisher     = Charles-Béchet
| pub_date      = [[1831 in literature|1831]]
| english_pub_date =
| media_type    =
| pages         =
| isbn          =
| oclc          =
| preceded_by   =
| followed_by   =
}}

'''''Le Chef-d’œuvre inconnu''''' ([[English language|English]] "The Unknown Masterpiece") is a [[short story]] by [[Honoré de Balzac]]. It was first published in the newspaper ''[[L'Artiste]]'' with the title "Maître Frenhofer" (English: "Master Frenhofer") in August [[1831 in literature|1831]]. It appeared again later in the same year under the title "Catherine Lescault, conte fantastique."  It was published in Balzac's  ''Études philosophiques'' in [[1837 in literature|1837]] and was integrated into the ''[[La Comédie humaine|Comédie humaine]]'' in [[1846 in literature|1846]].

"Le Chef-d’œuvre inconnu" is a reflection on art, and has had an important influence on modernist artists.

==Plot summary==
Young [[Nicolas Poussin]], as yet unknown, visits the painter [[Frans Pourbus the younger|Porbus]] in his workshop.  He is accompanied by the old master Frenhofer who comments expertly on the large tableau that Porbus has just finished. The painting is of [[Mary of Egypt]], and while Frenhofer sings her praises, he hints that the work seems unfinished.  With some slight touches of the paintbrush, Frenhofer transforms Porbus' painting such that Mary the Egyptian appears to come alive before their very eyes.  Although Frenhofer has mastered his technique, he admits that he has been unable to find a suitable model for his own masterpiece, which depicts a beautiful courtesan called Catherine Lescault, known as ''La Belle noiseuse''. He has been working on this future masterpiece, that no one has yet seen, for ten years. Poussin offers his own lover, Gillette, as a model. Gillette is so beautiful that Frenhofer is inspired to finish his project quickly. Poussin and Porbus come to admire the painting, but all they can see is part of a foot that has been lost in a swirl of colors. Their disappointment drives Frenhofer to madness, and he destroys the painting and dies that night.

==Influence on artists==
[[Paul Cézanne]] strongly identified with Frenhofer, once saying "Frenhofer, c’est moi" (I am Frenhofer). Critic Jon Kear argues that Cézanne's own attempts to paint the nude were heavily influenced by Balzac's portrayal of Frenhofer's work.<ref>Kear, John, "Cézanne’s Nudes and Balzac’s Le Chef-d’œuvre inconnu", ''Cambridge Quarterly'', Volume 35, Issue 4, pp. 345-360.</ref>

In 1921, [[Ambroise Vollard]] asked [[Picasso]] to illustrate ''Le Chef-d’œuvre inconnu.'' Picasso was fascinated by the text and identified with Frenhofer so much that he moved to the rue des Grands-Augustins in [[Paris]] where Balzac located Porbus' studio. There he painted his own masterpiece, ''[[Guernica (painting)|Guernica]]''. Picasso lived here during [[World War II]].

==Adaptations==
[[Sidney Peterson]]'s 1949 avant-garde film ''Mr Frenhofer and the Minotaur'' was based on the  link between the short story and the work of Picasso. It draws on Picasso's ''[[Minotauromachy]]'', bringing Picasso's work to life with the characters of Gillette, Poussin and Porbus participating.<ref>P. Adams, ''Visionary Film : The American Avant-Garde, 1943-2000: The American Avant-Garde 1943-2000'', Oxford University Press, 2003.</ref>

"Le Chef-d’œuvre inconnu" inspired the film ''[[la Belle Noiseuse]]'' by [[Jacques Rivette]] ([[1991 in film|1991]]).

==References==
{{reflist}}

==External links==
*[http://www.gutenberg.org/etext/23060 "The Unknown Masterpiece", translation (by Ellen Marriage?) at Project Gutenberg (full text)]
*[http://www.gutenberg.org/etext/1553 "The Hidden Masterpiece", translation by Katharine Prescott Wormeley at Project Gutenberg (full text)]
*[http://www.nybooks.com/shop/product-file/48/theu48/introduction.pdf Introduction to the book]
*[http://www.wga.hu/index1.html Nicolas Poussin paintings]
*{{fr}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/balzac-honore-de-le-chef-doeuvre-inconnu.html/ ''Le Chef-d'œuvre inconnu'', audio version]

{{Wikisourcelang|fr|Le Chef-d’œuvre inconnu}}

{{Honoré de Balzac}}

{{Authority control}}
{{DEFAULTSORT:Chef-Doeuvre Inconnu}}
[[Category:1831 short stories]]
[[Category:French short stories]]
[[Category:Books of La Comédie humaine]]
[[Category:Works originally published in L'Artiste]]
[[Category:Short stories adapted into films]]