{{lowercase}}
'''''mBio''''' is a peer-reviewed journal published by the [[American Society for Microbiology]] (ASM). ASM has been publishing microbiology research since 1916,<ref>{{Cite web|url=http://www.asm.org/index.php?option=com_content&view=article&id=285&Itemid=238 |title=About ASM &#124; Unknown |publisher=Asm.org |date= |accessdate=2010-07-12}}</ref> and ''mBio'' represents ASM’s first cross-discipline, [[Open access publishing|open-access]] publication.

The scope of ''mBio'' includes all aspects of the microbiological sciences, including virology, bacteriology, parasitology, mycology, and allied fields, which may include immunology, ecology, geology, population biology, computational biology, anti-infectives and vaccines, public health, etc.<ref>{{Cite web|url=http://mbio.asm.org/site/misc/journal-ita_sco.xhtml |title=mBio Instructions to Authors, Scope |publisher=Mbio.asm.org |date= |accessdate=2010-07-12}}</ref>

mBio complements ASM’s 9 primary research journals, which serve more specific disciplines.

According to the Journal Citation Reports for 2014 (Thomson Reuters), mBio has an impact factor of 6.786, which ranks it 11th by impact factor out of 119 journals in the Microbiology category.<ref>http://mbio.asm.org/site/misc/about.xhtml</ref>

==History==

===Genesis===
The ASM journals program has historically provided a venue for the publication of a wide spectrum of microbiological research.  ASM publishes 11 other journals<ref>{{Cite web|url=http://journals.asm.org/ |title=ASM Journals |publisher=Journals.asm.org |date= |accessdate=2010-07-12}}</ref> that focus on narrower areas of microbiology such as bacteriology and virology. ''mBio'' was conceived (i) to offer a publication vehicle for more cutting-edge research of broader interest and (ii) to serve as a laboratory to test new publishing technologies.<ref>{{Cite web|author=Vincent Racaniello |authorlink=Vincent Racaniello|url=http://www.microbeworld.org/index.php?option=com_jlibrary&view=article&id=3674&Itemid=54/ |title=Interview with Tom Shenk about ''mBio'', ASM's new open access journal |publisher=MicrobeWorld |date=2010-05-15 |accessdate=2010-07-12}}</ref>

With major funding agencies and foundations adopting policies mandating [[Open access publishing|open access]],<ref>{{Cite web|url=http://publicaccess.nih.gov/policy.htm |title=NIH Public Access Policy |publisher=Publicaccess.nih.gov |date= |accessdate=2010-07-12}}</ref><ref>{{Cite web|last=Suber |first=Peter |url=http://www.earlham.edu/~peters/fos/2008/02/text-of-harvard-policy.html |title=Peter Suber, Open Access News |publisher=Earlham.edu |date=2008-02-12 |accessdate=2010-07-12}}</ref><ref>{{Cite web|url=http://www.wellcome.ac.uk/About-us/Policy/Spotlight-issues/Open-access/Policy/index.htm |title=Position statement in support of open access to published research &#124; Wellcome Trust |publisher=Wellcome.ac.uk |date= |accessdate=2010-07-12}}</ref> ASM is also using ''mBio'' as a venue to test an open-access business model.

===Name===
In naming the journal, ASM coined the novel term "mBio." This term is also frequently used as an abbreviation for the field of microbiology (e.g., in college course catalogs).

===Announcement===
mBio was officially launched on September 13, 2009, at a reception at the 49th ICAAC meeting in San Francisco, CA.<ref>{{Cite web|url=http://www.eurekalert.org/pub_releases/2009-09/asfm-asf091109.php |title=American Society for Microbiology to launch new open-access journal |publisher=Eurekalert.org |date=2009-09-11 |accessdate=2010-07-12}}</ref> Founding Editor in Chief [[Arturo Casadevall]]<ref>{{Cite web|url=http://mbio.asm.org/site/misc/profile_casadevall.xhtml |title=mBio Founding Editor in Chief professional profile |publisher=Mbio.asm.org |date= |accessdate=2010-07-12}}</ref><ref>{{Cite web|url=http://www.einstein.yu.edu/home/faculty/profile.asp?id=3478 |title=Einstein Faculty: Arturo Casadevall, M.D., Ph.D |publisher=Einstein.yu.edu |date= |accessdate=2010-07-12}}</ref> took questions about the journal and unveiled the ''mBio'' website. ''mBio'' was launched in May 2010,<ref>{{Cite web|url=http://www.asm.org/index.php?option=com_content&view=article&id=91471 |title=ASM Launches New Open Access Journal &#124; News Room |publisher=Asm.org |date= |accessdate=2010-07-12}}</ref> with several notable articles appearing in the inaugural issue.<ref>{{Cite web|url=http://mbio.asm.org/content/1/1.toc |title=Table of Contents — April 2010, 1 (1) — ''mBio'' |publisher=Mbio.asm.org |date=2010-05-18 |accessdate=2010-07-12}}</ref><ref>{{Cite news|author=<!--start post navigation--> Previous post Next post |url=https://www.wired.com/wiredscience/2010/05/universal-flu-vaccine/ |title=New Flu Vaccines Could Protect Against All Strains &#124; Wired Science |publisher=Wired.com |date= 2010-05-25|accessdate=2010-07-12}}</ref><ref>{{Cite news|author=ANI, Jun 2, 2010, 12.00am IST |url=http://timesofindia.indiatimes.com/life/health-fitness/health/Using-sari-to-filter-water-can-protect-against-cholera/articleshow/5953372.cms |title=Using sari to filter water can protect against cholera - Health - Health & Fitness - Life & Style - The Times of India |publisher=Timesofindia.indiatimes.com |date=2010-06-02 |accessdate=2010-07-12}}</ref>

==Editorial details==

===Streamlined decisions===
mBio offers streamlined decisions. The intent is for authors to receive decisions on their manuscripts more quickly and more transparently. Editors either accept or reject manuscripts, and request only minor revisions; editors generally do not require authors to make extensive modifications or perform additional experiments.

This editorial philosophy is meant to contrast with publication models in which authors are required to perform additional experiments with no guarantee of acceptance. Although the “clear decision” model means that manuscripts needing further experiments are more likely to be rejected, it is intended to improve the publishing experience for authors.<ref name="about mBio">{{Cite web|url=http://mbio.asm.org/site/misc/about.xhtml |title=about ''mBio'' |publisher=Mbio.asm.org |date= |accessdate=2010-07-12}}</ref>

===Importance===
Research Article and Observation article types in ''mBio'' include a short (150 words or less), non-technical explanation of why the work is important.<ref name=editor>{{Cite web|url=http://mbio.asm.org/site/misc/journal-ita_org.xhtml#01 |title=mBio Instructions to Authors - Organization and Format |publisher=Mbio.asm.org |date= |accessdate=2010-07-12}}</ref> (The evaluation of the importance of a piece of research is based on the essay by [[Arturo Casadevall|A. Casadevall]] and F. C. Fang, Important Science – It's All about the SPIN, Infect. Immun. 77:4177-4180.)<ref>A. Casadevall and F. C. Fang. 2009. Important Science – It's All about the SPIN. Infect. Immun. '''77:'''4177-4180. http://iai.asm.org/cgi/content/abstract/77/10/4177</ref>

===Article-level commenting===
The ''mBio'' website offers commenting functionality on each full-text article page. The commenting feature is intended to offer a venue for post-publication commentaries and exchanges by readers. ''mBio'' also continues to publish Letters to the Editor as a venue for more-formal exchanges.<ref name=editor/>

==Association with the academy==
mBio is published in association with the [[American Society for Microbiology#American Academy of Microbiology|American Academy of Microbiology]] (AAM), which is the honorific leadership group within the American Society for Microbiology. AAM Fellows work with the ''mBio'' Editor in Chief by serving as editors and reviewers for manuscripts submitted in their areas of expertise.

AAM Fellows are also entitled to submit one paper to ''mBio'' per calendar year via a special, accelerated submission path.  This path allows Fellows to obtain two external reviews prior to submission and communicate the paper plus reviewer feedback and author responses to ''mBio''. The ''mBio'' editor handling these submissions maintains the prerogative of deciding whether or not the external reviews should be considered and can recommend modification, additional review, or rejection.<ref name="about mBio"/>

==References==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

==External links==
* [http://mbio.asm.org/ ''mBio'' home page.]
* [http://academy.asm.org/ AAM home page.]
* [http://journals.asm.org/ ASM journals home page.]

{{DEFAULTSORT:Mbio}}
[[Category:Microbiology journals]]
[[Category:Publications established in 2009]]