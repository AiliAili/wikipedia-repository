'''ISA 230 Audit Documentation''' is one of the [[International Standards on Auditing]]. It serves to direct the documentation of [[audit working papers]] in order to assist the [[audit planning]] and performance; the [[Supervisor|supervision]] and [[review]] of the [[audit work]]; and the recording of [[audit evidence]] resulting from the audit work in order to support the auditor's opinion.

==ISA 230 statements==
The auditor should prepare, on a timely basis, audit documentation that provides:

#A sufficient and appropriate record of the basis for the audit report
#Evidence that the audit was performed in accordance with ISA's and applicable legal and  regulatory requirements (Paragraph 2).<ref name="ISA">ISA 230 (UK)</ref>

The auditor should prepare the audit documentation so as to enable an experienced auditor, having no previous connection with the audit, to understand:

# The nature, timing, and extent of audit procedures performed to comply with ISAs and applicable legal and regulatory requirements;
# The results of the audit procedures and the audit evidence obtained;
# Significant matters arising during the audit and the conclusions reached thereon. (Paragraph 9)<ref name="ISA" />

Oral explanations by the auditor, on their own, do not represent adequate support for the work the auditor performed or conclusions the auditor reached, but may be used to clarify or explain information contained in the audit documentation. (Paragraph 11)<ref name="ISA" /> This is not true, Oral explanations are only referenced on paragraph 11 of AU section 339 therefore does not refer to ISA.

The audit file must effectively stand on its own. Whilst the auditor may clarify what has been documented the facility to explain detailed aspects of the audit has gone, so this must mean more extensive working papers in some areas.

In documenting the nature, timing and extent of audit procedures performed, the auditor should record the identifying characteristics of the specific items or matters being tested. (Paragraph 12)<ref name="ISA" />

In documenting the nature, timing and extent of audit procedures performed, the auditor should record:

# Who performed the audit work and the date such work was completed; and
# Who reviewed the audit work performed and the date and extent of such review. (Paragraph 23)<ref name="ISA" />

The standard also establishes clear responsibilities for the auditor to assemble the final audit file on a timely basis and sets out specific requirements regarding deletions, modifications or additions to audit documentation after the date of the auditor's report. 

There are also documentation requirements in the exceptional circumstance when an auditor judges it necessary to depart from a basic principle or essential procedure that is relevant in the circumstances of the audit. The auditor should document how the alternative procedures performed achieve the objective of the audit and, unless otherwise clear, the reasons for the departure.

==Methods of recording==
* [[Narrative notes]]
* [[Organisation chart]]s
* [[Internal control questionnaires (ICQs)]]
* [[Flowcharts]]

== References ==
{{reflist}}


[[Category:Auditing standards]]