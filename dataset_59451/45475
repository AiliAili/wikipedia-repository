{{Redirect|John Gillespie Magee|the missionary, John Gillespie Magee, Sr.|John Magee (missionary)}}
{{Redirect|High Flight|the 1957 British film|High Flight (film)}}

{{Infobox military person|name=John Gillespie Magee, Jr.
|image=John Gillespie Magee, Jr.jpg
|caption=Official Royal Canadian Air Force picture of Pilot Officer John Gillespie Magee, Jr.
|birth_date=9 June 1922
|death_date=11 December 1941 (age 19)
|birth_place=[[Shanghai]], [[Republic of China (1912–49)|Republic of China]]
|death_place=Killed in a flying accident over [[Lincolnshire]]
|placeofburial=Holy Cross Cemetery, [[Scopwick]], [[Lincolnshire]]
|placeofburial_label= Place of burial
|nickname=
|serviceyears=1940 &ndash; 1941
|rank=[[Pilot Officer]]
|branch= [[Image:Royal Canadian Air Force Ensign (1941-1968).svg|22px]] [[Royal Canadian Air Force]]
|unit=[[No. 412 Squadron RCAF]]
|battles=[[World War II]]
|awards=
|laterwork=
}}

'''John Gillespie Magee, Jr.''' (9 June 1922 – 11 December 1941)<ref name="BB">{{cite book |title= Battle of Britain – Pilot and Aircrew Manual – Ceremony 2007 |author= Government of Canada |year= 2007 |publisher= Government of Canada |location= Ottawa}}</ref><ref name="RAF Digby">{{cite web |url=http://www.raf.mod.uk/rafdigby/aboutus/johngillespiemageejr.cfm |title= RAF Digby – John Gillespie Magee Jr |accessdate=2 March 2008 }}</ref><ref name="wood"/> was an Anglo-American [[aviation|aviator]] and [[poet]], made famous for his poem ''High Flight''. Magee served in the [[Royal Canadian Air Force]], which he joined before the [[United States]] entered the war; he died in a mid-air collision over Lincolnshire in 1941.

== Early life ==

{{quote box
| width = 33%
| title = ''Sonnet to Rupert Brooke''
| title_bg = BlanchedAlmond
| title_fnt = SaddleBrown
| bgcolor = Cornsilk
| align = right
| halign = left
|source = 
| quote = : "We laid him in a cool and shadowed grove
: One evening in the dreamy scent of thyme
: Where leaves were green, and whispered high above —
: A grave as humble as it was sublime;

: There, dreaming in the fading deeps of light —
: The hands that thrilled to touch a woman's hair;
: Brown eyes, that loved the Day, and looked on Night,
: A soul that found at last its answered Prayer...

: There daylight, as a dust, slips through the trees.
: And drifting, gilds the fern around his grave —
: Where even now, perhaps, the evening breeze
: Steals shyly past the tomb of him who gave

: New sight to blinded eyes; who sometimes wept —
: A short time dearly loved; and after, — slept."}}

John Gillespie Magee was born in [[Shanghai]], [[China]], to an American father and a British mother, who both worked as [[Anglican]] [[missionaries]].<ref name="RAF Digby"/><ref name="wood"/> His father, [[John Magee (missionary)|John Magee, Sr.]], was from a family of some wealth and influence in [[Pittsburgh, Pennsylvania]]. Magee Senior chose to become an [[Episcopal Church in the United States of America|Episcopal]] [[priest]] and was sent as a missionary to China. Whilst there he met his future wife, Faith Emmeline Backhouse, who came from [[Helmingham]] in [[Suffolk]] and was a member of the [[Church Missionary Society]]. Magee's parents married in 1921, and their first child, John Junior, was born 9 June 1922, the eldest of four brothers.

Magee began his education at the American School in [[Nanjing|Nanking]] in 1929. In 1931 he moved with his mother to the [[UK]] and spent the following four years at St. Clare, a boarding school for boys, near [[Walmer]], in [[Kent]].

He attended [[Rugby School]] from 1935 to 1939.  He developed his poetry whilst at the school and in 1938 he won the school's Poetry Prize.  He was deeply moved by the roll of honour of Rugby pupils who had fallen in the [[First World War]]. This list of the fallen included the celebrated war poet [[Rupert Brooke]] (1887–1915), whose work Magee greatly admired. Brooke had won the school poetry prize thirty-four years prior to Magee. The prize-winning poem by Magee referred to Brooke's burial at 11 o'clock at night in an olive grove on the [[Greece|Greek]] island of [[Skyros]].

Whilst at Rugby, Magee met and fell in love with [[Elinor Lyon]], the daughter of [[P. H. B. Lyon]], the headmaster. She became the inspiration for many of Magee's poems.<ref>''Sunward I've Climbed.'' Hermann Hagedorn, The Macmillan Company, New York, 1942. (In this biography, Elinor was referred to as "Diana.")</ref> Though his love was not returned, he remained friends with Elinor and her family.

Magee visited the United States in 1939.  Because of the outbreak of [[World War II]], he was unable to return to Rugby for his final school year, and instead attended [[Avon Old Farms School]] in [[Avon, Connecticut]].<ref>{{cite web|url=http://www.macla.co.uk/scopwick/magee.php |title=Pilot Officer John Gillespie Magee |website=Macla.co.uk |date= |accessdate=2016-01-27}}</ref>  He earned a scholarship to [[Yale University]] in July 1940, but did not enroll, choosing instead to enlist in the [[Royal Canadian Air Force]] in October of that year.

== Career ==
Magee joined the RCAF in October 1940 and received flight training in [[Ontario]] at No. 9 EFTS (Elementary Flying Training School), located at [[St. Catharines/Niagara District Airport|RCAF Station St. Catharines]] ([[St. Catharines, Ontario|St. Catharines]]), and at No. 2 SFTS (Service Flying Training School) at [[RCAF Station Uplands]] ([[Ottawa]]). He passed his Wings Test in June 1941.

Shortly after his promotion to the rank of [[Pilot Officer]], after having been awarded his [[Aircrew brevet|wings]], Magee was sent to Britain.  He was posted to No. 53 [[Operational Training Unit]] (OTU) at [[RAF Llandow]], in [[Wales]]. After graduating from No. 53 OTU, Magee was assigned to [[No. 412 Squadron RCAF|No. 412 (Fighter) Squadron, RCAF]],<ref name="BB"/> which was formed at [[RAF Digby]] on 30 June 1941, and where he became a qualified Spitfire pilot.

== Death ==
[[Image:Magee Grave.JPG|right|thumb|100px|Magee's grave]]
[[File:John Gillespie Magee Memorial.JPG|thumb|left|The plaque at St. Catharines/Niagara District Airport that commemorates J G. Magee, Jr.]]
Magee was killed at the age of 19, while flying Spitfire coded VZ-H, serial number AD291. He had taken off with other members of 412 Squadron from [[RAF Wellingore]] (near [[Navenby]] & [[RAF Digby]], and about three miles northwest of [[RAF Cranwell]]), which has now reverted to agriculture. The aircraft was involved in a mid-air collision with an [[Airspeed Oxford]] trainer from Cranwell, flown by Leading Aircraftman Ernest Aubrey Griffin. The two aircraft collided just below the cloud base at about 1,400&nbsp;feet AGL, at 11:30, over the hamlet of [[Roxholme]], which lies between RAF Cranwell and RAF Digby, in [[Lincolnshire]].<ref name="RAF Digby"/> Magee was descending at high speed through a break in the clouds with three other aircraft.

At the inquiry afterwards a farmer testified that he saw the Spitfire pilot struggling to push back the canopy.<ref name="RAF Digby"/> The pilot stood up to jump from the plane but was too close to the ground for his parachute to open, and died on impact.<ref name="RAF Digby"/><ref name="wood"/> Griffin was also killed.<ref>{{cite web|author= |url=http://www.cwgc.org/find-war-dead/casualty/2421617/GRIFFIN,%20ERNEST%20AUBREY |title=Casualty Details |website=CWGC.org |date= |accessdate=2016-01-27}}</ref>

Magee was buried at Holy Cross Cemetery, [[Scopwick]] in Lincolnshire, England.<ref name="RAF Digby"/><ref name="wood">{{cite web |url=http://www.woodiescciclub.com/high-flight.htm |title= High Flight Poem – John Gillespie Magee Jr |accessdate=2 March 2008 }}</ref> On his grave are inscribed the first and last lines from his poem ''High Flight''. Part of the official letter to his parents read, "Your son's funeral took place at Scopwick Cemetery, near Digby Aerodrome, at 2.30pm, on Saturday, 13 December 1941, the service being conducted by Flight Lieutenant S. K. Belton, the Canadian padre of this Station. He was accorded full Service Honours, the coffin being carried by pilots of his own Squadron".

== ''High Flight'' ==
{{quote box
| width = 33%
| title = ''High Flight''
| title_bg = BlanchedAlmond
| title_fnt = SaddleBrown
| bgcolor = Cornsilk
| align = right
| halign = left
|source = 
| quote = :<poem> "Oh! I have slipped the surly bonds of Earth
: And danced the skies on laughter-silvered wings;
: Sunward I’ve climbed, and joined the tumbling mirth 
: of sun-split clouds, — and done a hundred things 
: You have not dreamed of — wheeled and soared and swung 
: High in the sunlit silence. Hov’ring there, 
: I’ve chased the shouting wind along, and flung 
: My eager craft through footless halls of air....

: Up, up the long, delirious, burning blue
: I’ve topped the wind-swept heights with easy grace.
: Where never lark, or even eagle flew —
: And, while with silent, lifting mind I've trod
: The high untrespassed sanctity of space,
: – Put out my hand, and touched the face of God."
</poem>}}

Magee's posthumous fame rests mainly on his [[sonnet]] ''High Flight'', which he started on 18 August 1941, just a few months before his death, whilst he was based at No. 53 OTU.  In his seventh flight in a [[Supermarine Spitfire Variants|Spitfire Mk I]], he had flown up to 33,000 feet. As he orbited and climbed upward, he was struck by words he had read in another poem — "To touch the face of God." He completed his verse soon after landing.

Purportedly, the first person to read Magee's poem later that same day in the officers' mess, was his fellow Pilot Officer Michael Le Bas (later [[Air Vice-Marshal]] M H Le Bas, Air Officer Commanding [[No. 1 Group RAF]]<ref>{{cite web|url=http://www.rafweb.org/Biographies/LeBas_MH.htm |title=M H Le Bas |website=Rafweb.org |date= |accessdate=2016-01-27}}</ref>), with whom Magee had trained.

Magee enclosed the poem in a letter to his parents, dated 3 September 1941. His father, then curate of [[St. John's Episcopal Church, Lafayette Square (Washington, D.C.)|Saint John's Episcopal Church]] in [[Washington, DC]], reprinted it in church publications. The poem became more widely known through the efforts of [[Archibald McLeish]], then [[Librarian of Congress]], who included it in an exhibition of poems called "Faith and Freedom" at the [[Library of Congress]] in February 1942. The [[manuscript]] copy of the poem remains at the Library of Congress.

=== Sources of inspiration ===

The last words of ''High Flight'' — "...and touched the face of God" — can also be found in a poem by Cuthbert Hicks, published three years earlier in ''Icarus: An Anthology of the Poetry of Flight''. <!-- (Macmillan, London, 1938), compiled by R de la Bere and three flight cadets of the [[Royal Air Force College Cranwell|Royal Air Force College, Cranwell]]. --> The last two lines in Hicks' poem, ''The Blind Man Flies'', are:
:For I have danced the streets of heaven,
:And touched the face of God.

The anthology includes the poem ''New World'', by G. W. M. Dunn, which contains the phrase "on laughter-silvered wings". Dunn wrote of "the lifting mind", another phrase that Magee used in ''High Flight'', and refers to "the shouting of the air", in comparison to Magee's line, "chased the shouting wind". Another line by Magee, "The high untrespassed sanctity of space", closely resembles "Across the unpierced sanctity of space", which appears in the anthology in the poem ''Dominion over Air'', previously published in the ''RAF College Journal''.

=== Uses of the poem ===
[[File:Reagan Space Shuttle Challenger Speech.ogv|thumb|thumbtime=15|right|President [[Ronald Reagan]] addresses the nation after the Challenger disaster.]]

During April and May 1942, Hollywood stars such as [[Laurel and Hardy]], [[Cary Grant]], [[Bing Crosby]], and [[Bob Hope]] joined the [[Hollywood Victory Caravan]] as it toured the United States on a mission to raise war bonds.  Famed actress [[Merle Oberon]] recited ''High Flight'' as part of this show.<ref>{{cite web|url=http://collections.oscars.org/onesearch/ |title=Search Library Databases &#124; Margaret Herrick Library &#124; Academy of Motion Picture Arts & Sciences |website=Collections.oscars.org |date= |accessdate=2016-01-27}}</ref>  During the performance on April 30, 1942, at the Loew's Capitol Theatre in Washington, D.C., and before her recitation of ''High Flight'', Oberon acknowledged the attendance of his father, the missionary [[John Magee (missionary)|John Magee]], and brother Christopher Magee.

[[Orson Welles]] recited the poem during an episode of [[Command Performance (radio)|Command Performance]] on December 21, 1943.<ref>{{cite web|url=https://archive.org/details/OrsonWellesWartimeBroadcasts |title=Archived copy |accessdate=March 15, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20130210224411/http://archive.org/details/OrsonWellesWartimeBroadcasts |archivedate=February 10, 2013 }}</ref>

''High Flight'' has been a favourite poem amongst both [[aviators]] and [[astronauts]]. It is the official poem of the [[Royal Canadian Air Force]] and the [[Royal Air Force]] and has to be recited from memory by fourth class cadets at the [[United States Air Force Academy]], where it can be seen on display in the Cadet Field House.<ref>[http://www.usafa.edu/superintendent/pa/highflight.htm ] {{webarchive |url=https://web.archive.org/web/20090406013849/http://www.usafa.edu/superintendent/pa/highflight.htm |date=April 6, 2009 }}</ref> Portions of the poem appear on many of the headstones in the [[Arlington National Cemetery]].<ref>{{cite web|author=Michael Patterson |url=http://www.arlingtoncemetery.net/highflig.htm |title=High Flight - John Gillespie Magee, Jr |website=Arlingtoncemetery.net |date= |accessdate=2016-01-27}}</ref> It is displayed on panels at the [[Canadian War Museum]] in [[Ottawa]], the [[National Air Force Museum of Canada]], in [[Trenton, Ontario]]. It is the subject of a permanent display at the [[National Museum of the United States Air Force]], in [[Dayton, Ohio]].<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=1349 ] {{webarchive |url=https://web.archive.org/web/20090528145603/http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=1349 |date=May 28, 2009 }}</ref> General [[Robert Lee Scott, Jr.]] included it in his book ''God is My Co-Pilot''.

Astronaut [[Michael Collins (astronaut)|Michael Collins]] brought an index card with the poem typed on it on his [[Gemini 10]] flight and included the poem in his autobiography ''Carrying The Fire''. Former NASA Flight Director [[Gene Kranz]] quoted the first line of the poem in his book ''Failure Is Not An Option''. U.S. President [[Ronald Reagan]] used part of ''High Flight'' in [[Ronald Reagan#Second term, 1985–1989|a speech]] written by [[Peggy Noonan]], that followed the [[Challenger disaster]] on January 28, 1986.<ref>{{cite web |last = Reagan | first = Ronald |first2= Peggy |last2=Noonan |title = Address to the nation on the Challenger disaster |publisher = Ronald Reagan Presidential Library Foundation |date = 1986-01-28| url = http://www.reaganfoundation.org/reagan/speeches/challenger.asp |accessdate = 2008-05-03 |archiveurl = http://web.archive.org/web/20071227225412/http://www.reaganfoundation.org/reagan/speeches/challenger.asp <!-- Bot retrieved archive --> |archivedate = 2007-12-27}}</ref>

At RAF Scampton in Lincolnshire, a memorial to Flight lieutenants Jon Egging, killed 20 August 2011, and Sean Cunningham, killed 8 November 2011, bears an interpretation of the poem on a brass plaque atop a wooden plinth in front of a 'gate guardian' aircraft outside the RAF Aerobatics Team hangar. The plaque reads "...they have slipped the surly bonds of Earth / Put out their hands and touched the face of God... / In memory of / Flt Lt Jon Egging - 20th August 2011 / Flt Lt Sean Cunningham - 8th November 2011" <ref>Royal Air Force</ref>

=== Musical adaptations ===

[[Miklós Rózsa]] composed the earliest known setting of ''High Flight'', for [[tenor]] voice, in 1942. It was later published as one of his ''Five Songs'' in 1974. Canadian composer and Royal Canadian Air Force veteran Robert J. B. Fleming wrote a through-composed musical setting of the poem for the Divine Services Book of the Canadian Armed Forces published in 1950. The composer [[Bill Pursell]] wrote his own arrangement with narration for the [[United States Air Force]] Band, which was broadcast on their radio show in the late 1940s. Several songs and symphonic compositions have been based on Magee's text, including [[Bob Chilcott]]'s 2008 setting, premiered on 1 May 2008 by the [[King's Singers]].<ref>{{cite web|url=http://www.bbc.co.uk/pressoffice/proginfo/radio/wk19/tue.shtml |title=Press Office - Network Radio Programme Information Week 19 Tuesday 6 May 2008 |publisher=[[BBC]] |date= |accessdate=2016-01-27}}</ref>

The poem has been set to music by several composers, including by [[John Denver]]<ref>{{cite web|url=https://www.youtube.com/watch?v=AjzcdvF3gDc |title=John Denver sings High Flight |publisher=YouTube |date=2012-08-01 |accessdate=2016-01-27}}</ref> as performed on the Bob Hope television show and is included in his 1983 album [[It's About Time (John Denver album)|''It's About Time'']] and by [[Christopher Marshall (composer)|Christopher Marshall]], whose composition was commissioned for and premiered by The Orlando Chorale with [[Saxophone|saxophonist]] George Weremchuk ([[Orlando, Florida]]) in March 2009, under the direction of Gregory Ruffer. The first performance of a setting of words, known as "Even Such Is Time", from [[Gabriel Faure|Fauré]]’s [[Requiem (Fauré)|Requiem]], plus additional non-liturgical texts that included ''High Flight'', was performed by the Nantwich Choral Society, conducted by John Naylor, on Saturday 26 March 2011, in St Mary’s Church, [[Nantwich]], [[Cheshire]]. The music was written by Andrew Mildinhall, the former [[organist]] at the church, who accompanied the performance with the Northern Concordia Orchestra.

The American composer [[James Curnow]] was commissioned by the Graduates Association of Tenri High School Band in [[Nara, Japan]] to write a piece for [[concert band]] in honor of the 50th anniversary of its association.  The piece is entitled ''Where Never Lark or Eagle Flew'' with the subtitle "Based on a poem by John Gillespie Magee, Jr."{{citation needed|date=February 2012}} In 2012, the Australian composer Daniel Walker was commissioned by [[North Sydney Boys High School]] to compose a piece for the school's centenary celebrations. This composition, 'Through Footless Halls of Air', which was written for choir and symphonic winds, features the poem in the lyrics.

British composer [[Jonathan Dove]] included the poem in his 2009 oratorio ''There Was a Child'', written as a memoriam to Robert Van Allen, who also died at the age of nineteen.  It has also been set by British composer Nicholas Scott Burt as a short motet and dedicated to the choir of Rugby Parish Church.

In 2014, Canadian composer [[Vince Gassi]] composed a piece for concert band entitled ''Chase The Shouting Wind''.

=== Other use in the media===
Many U.S. [[television]] viewers were introduced to ''High Flight'' when some TV stations ended (and sometimes also began) their programming day with short films based on it.<ref>https://www.youtube.com/watch?v=6uGP74qRzSc</ref> The sign-off film occasionally used by [[KCRA]]-TV in [[Sacramento, California]] featured the spoken poem played [[United States Air Force|Air Force]] footage.<ref>{{YouTube|sJeVePBjm78|An example of a "High Flight"/TV sign-off film}}</ref>
Sung by Phill Driscoll. Phil is a trumpet player, Christian artist, and singer. In his rendition of the song, he alludes to being caught up to be with God.<ref>{{cite web|url=http://www.amazon.com/High-Flight/dp/B0058ZL8RQ |title=High Flight: Phil Driscoll: MP3 Downloads |website=Amazon.com |date= |accessdate=2016-01-27}}</ref> Other examples of the use of the poem in television programs, films include:

* The popular comic strip [[Bloom County]], which used the poem on July 8, 1984, to illuminate the Earth-bound frustrations of Opus, a flightless waterfowl.<ref>{{cite web|url=http://www.gocomics.com/bloomcounty/1984/07/08/ |title=Bloom County Comic Strip, July 08, 1984 on |website=Gocomics.com |date= |accessdate=2016-01-27}}</ref>
*Featured in an episode of NBC's The Blacklist season 3 episode 6 "Sir Crispin Crandall" originally aired November 5, 2015
* An episode of the UK [[archaeology]] documentary series ''[[Time Team]]'' that featured the [[excavation (archaeology)|excavation]] of a crashed Spitfire in France, when the poem was read during the end credits.
* The penultimate episode "[[Daybreak (Battlestar Galactica)|Daybreak]]" of ''[[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]'', where the poem is paraphrased.<ref name="reagan.utexas.edu">{{cite web|url=http://www.reagan.utexas.edu/archives/speeches/1986/12886b.htm |title=Address to the Nation on the Explosion of the Space Shuttle Challenger |website=Reagan.utexas.edu |date=1986-01-28 |accessdate=2016-01-27}}</ref>
* The 1993 [[Russell Crowe]] movie [[Russell Crowe#Filmography|''For the Moment'']], where the poem is recited by Crowe's character, Lachlan Curry.
* The 1989 [[science fiction film|science fiction]] [[adventure film|adventure]] [[film]] ''[[Slipstream (1989 film)|Slipstream]]'', which made frequent use of the poem, most notably by [[Mark Hamill]] and [[Bob Peck]].
* The film ''[[Snow Walker]]'', in which [[James Cromwell]] recites the poem.
* Pilot and composer [[Max Conrad]]'s second LP of ''Flight Inspired Music'', which features the poem on the cover.
* [[Scott O'Grady]]'s book ''Return With Honor'', which has a full transcript of the poem.
* ''One Small Step'', a children's novel by [[Philip Kerr]], reprints the poem in full before the Author's Note.
* "The Crackpots and These Women" 5th episode from Season 1 of The West Wing. "...with outstretched fingers, we touched the face of God." President Bartlet
* A French translation of High Flight is told by Bernard Chabert in an episode of "Pégase", a French TV documentary dedicated to the Spitfire.
* English independent filmmakers James Walker and John Wallace produced the documentary film ''High Flight'' in 2016, which takes its name from the poem, and documents Magee's story, the origin of the poem and the poem's place in the legacy of World War Two iconography, as well as the cultural impact of the era upon the "baby boomer" generation. The film is due for release and distribution in late 2016.

== ''Per Ardua'' ==
{{quote box
| width = 33%
| title = ''Per Ardua''
| title_bg = BlanchedAlmond
| title_fnt = SaddleBrown
| bgcolor = Cornsilk
| align = right
| halign = left
|source = 
| quote = :<poem> (To those who gave their lives to England during the Battle of 
:Britain and left such a shining example to us who follow,
:these lines are dedicated.)

:They that have climbed the white mists of the morning; 
:They that have soared, before the world's awake, 
:To herald up their foeman to them, scorning 
:The thin dawn's rest their weary folk might take; 
:Some that have left other mouths to tell the story 
:Of high, blue battle, quite young limbs that bled, 
:How they had thundered up the clouds to glory, 
:Or fallen to an English field stained red. 
:Because my faltering feet would fail I find them 
:Laughing beside me, steadying the hand 
:That seeks their deadly courage – 
:Yet behind them 
:The cold light dies in that once brilliant Land .... 
:Do these, who help the quickened pulse run slowly, 
:Whose stern, remembered image cools the brow, 
:Till the far dawn of Victory, know only 
:Night's darkness, and Valhalla's silence now?
</poem>}}

Shortly after Magee's first combat action on November 8, 1941, he sent his family part of another poem, referring to it as "another trifle which may interest you". It is possible that the poem, ''Per Ardua'', is the last that Magee wrote. There are several corrections to the poem, made by Magee, which suggest that the poem was not completed when he sent it. ''[[Per ardua ad astra]]'' ("Through adversity to the stars") is the motto of a number of [[Commonwealth of Nations|Commonwealth]] air forces, such as the [[Royal Air Force]], [[RAAF]], [[RNZAF]] and the [[RCAF]]. It was first used in 1912 by the newly formed [[Royal Flying Corps]].

==References==
;Notes
{{Reflist}}
;Bibliography
* ''The Complete Works of John Magee, The Pilot Poet, including a short biography by Stephen Garnett''. Cheltenham, Gloucestershire: This England Books, March 1989.
* ''Icarus: An anthology of the poetry of flight''. Macmillan, London, 1938.
* ''Sunward I've Climbed''. Hermann Hagedorn, The Macmillan Company, New York, 1942.
* ''High Flight: A Story of World II''. Linda Granfield, Tundra Books, August 1999.
* ''High Flight: The Life and Poetry of Pilot Officer John Gillespie Magee''. Roger Cole, Fighting High Publishing, January 2014.
* ''Touching the Face of God: The Story of John Gillespie Magee, Jr. and his poem High Flight''. Ray Haas, High Flight Productions, North Carolina, September 2014.
* ''A Day in Eternity''. Kathryn Gabriel Loving, SoulJourn Books, September 2016. (A novel involving the story of John Gillespie Magee, Jr..)

==External links==
* A [http://www.highflightproductions.com/high_flight_productions/JohnMagee.html website devoted to Magee], by High Flight Productions.
* A {{YouTube|xTlWC7kfpDE|video of the original 1960s "High Flight" television station sign-off}}.
* {{Librivox author |id=9976}}

{{Authority control}}

{{DEFAULTSORT:Magee, John Gillespie}}
[[Category:1922 births]]
[[Category:1941 deaths]]
[[Category:20th-century American poets]]
[[Category:20th-century English poets]]
[[Category:Royal Canadian Air Force personnel]]
[[Category:Canadian military personnel killed in World War II]]
[[Category:American World War II pilots]]
[[Category:British World War II pilots]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]
[[Category:People educated at Rugby School]]
[[Category:Aviation media]]
[[Category:Articles containing video clips]]
[[Category:American male poets]]
[[Category:English male poets]]