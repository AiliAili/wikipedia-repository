{{About|the 2010 song by Ivy Queen and Wisin & Yandel|the 2004 song by Queen and Mickey Perfecto|Real (Ivy Queen album)}}
{{Infobox song
| Name = Acércate
| Cover = Wisin & Yandel - No Te Equivoques.jpg
| Caption = Alternate version with [[Wisin & Yandel]] only.
| Artist = [[Ivy Queen]] featuring [[Wisin & Yandel]]
| Album = [[Drama Queen (Ivy Queen album)|Drama Queen]]
| Released = July 13, 2010
| Recorded = 2009&ndash;10 <br> [[Carolina, Puerto Rico]] <br> <small>([[Mas Flow Inc.|Mas Flow Studios]])</small>
| Genre = [[Reggaeton|Reggaetón]]
| Length = {{Duration|m=3|s=28}}
| Label = [[Machete Music|Machete]], [[Drama Records|Drama]], [[WY Records|WY]]
| Writer = [[Ivy Queen|Martha I. Pesante]], [[Wisin & Yandel|Juan Luis Morera Luna]], [[Wisin & Yandel|Llandel Veguilla Malavé]], [[Tainy|Marcos Masis]]
| Producer = [[Luny Tunes]], [[Tainy]]
| Misc           = {{Extra track listing 
| Album          = [[Drama Queen (Ivy Queen album)|Drama Queen]]
| Type           = song
| prev_track     = "I Do"
| prev_no        = 6
| this_track     = "Acércate"
| track_no       = 7
| next_track     = "[[La Vida Es Así]]"
| next_no        = 8
}}
}}

"'''Acércate'''" ({{lang-en|"Come Closer")}} is a song recorded by Puerto Rican [[Reggaeton|reggaetón]] recording artist  [[Ivy Queen]] and duo [[Wisin & Yandel]] for Queen's seventh studio album ''[[Drama Queen (Ivy Queen album)|Drama Queen]]'' (2010). It was composed by Queen and [[Tainy|Marcos Masis]] alongside the duo, while being produced by [[Luny Tunes]] and [[Tainy]]. Originally entitled "No Te Equivoques", the song was leaked onto the Internet prior to the album's release, which prompted Ivy Queen and Wisin & Yandel to re-record the song.

Wisin & Yandel released a solo version of the song with lead vocals performed by them under the original title. While failing to chart on main Latin songs charts in ''[[Billboard (magazine)|Billboard]]'' magazine, it did manage to debut and peak at number sixteen on the ''Billboard'' Latin Rhythm Digital Songs chart, charting simultaneously with the [[lead single]] off the album "[[La Vida es Así|La Vida Es Así]]" which obtained the number two position. The song brings together the first studio album released by Ivy Queen in three years and first for [[Machete Music]], after being with [[Univision Records]] since 2005.

==Background==
After the success of her 2007 effort ''[[Sentimiento (album)|Sentimiento]]'', which was certified Platinum by the United States [[Recording Industry Association of America]] (RIAA), a substantial live album was distributed by [[Machete Music|Machete]] in 2008.{{Certification Cite Ref|region=United States|title=Sentimiento|artist=Ivy Queen|type=album|award=Platinum|Spanish=yes|accessdate=2011-04-15}}  Subsequently, Queen signed a new record deal with the label in April 2010, as they celebrated their fifth anniversary.<ref name="Signing">{{cite web|url=http://www.billboard.biz/bbbiz/content_display/industry/news/e3ib6c66237fa7a658d912e04772eee0124|title=Ivy Queen Signs With Machete Music|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|author=Leila Cobo|date=2010-04-05| accessdate=2012-12-21}}</ref> The signing, described as a 360 deal, includes profit sharing in tours, sponsorships and  merchandising.<ref name="Signing"/> Ivy Queen was previously signed to a distribution deal with [[Univision Records]], which in turn was acquired by Machete's parent company [[Universal Music Latin Entertainment]] in 2008.<ref name="Signing"/>

President of [[Universal Music Latino]] and Machete, Walter Kolm, commented in a press release that: "It's a privilege to have Ivy Queen apart of over artistic roster. Ivy is an extraordinary woman with incomparable talent, and she's number one in her genre. We're happy to be able to work with her on her new album as well as future projects".<ref name="Signing"/> "I'm very proud to be a part of Machete Music. They are a young, vibrant company that has created a name for itself in Latin music in the United States and the world. They are a strong and important company that has been recognized for nurturing their artists’ creative talents," said Ivy Queen, regarding the partnership.<ref>{{cite web|url=http://www.prweb.com/releases/2010/04/prweb3835924.htm|title=Ivy Queen Signs With Machete Music|work=PR Web|publisher=Vocus PRW Holdings, LLC|author=Carla Lopez|date=2010-04-07| accessdate=2013-01-10}}</ref> Ivy Queen told ''Efe'' that the composition process started while she was heartbroken at home. Her emotions then burst out in the recording studio. She added the song is one of 26 songs she wrote during this period.<ref>{{cite web|url=http://noticias.terra.com/noticias/ivy_queen_desahoga_todos_sus_sentimientos_en_su_nuevo_disco_drama_queen/act2277159|title=Ivy Queen desahoga todos sus sentimientos en su nuevo disco "Drama Queen"|language=Spanish|work=[[Terra Networks]]|publisher=[[Telefónica]]|author=Jorge J. Muñiz Ortiz|date=2010-04-09|accessdate=2012-11-03}}</ref>

==Music and lyrics==
[[Image:Wisin & Yandel.jpg|thumb|right|220px|[[Wisin & Yandel]] collaborated on the song with Ivy Queen and [[Tainy|Marcos Masis]].]]

The song was composed by Queen, [[Tainy|Marcos Masis]], [[Wisin & Yandel|Juan Luis Morera Luna]] and [[Wisin & Yandel|Llandel Veguilla Malavé]]; the duo known as [[Wisin & Yandel]].<ref>{{cite web|url=http://www.allmusic.com/album/drama-queen-mw0002006785|title=Drama Queen - Ivy Queen : Songs, Reviews, Credits, Awards : Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2012-12-21}}</ref><ref>{{cite web|url=http://www.allmusic.com/song/acérate-mt0036495128|title=Acércate - Ivy Queen : Listen, Appearances, Song Reviews : Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2012-12-21}}</ref> Musically, it features minor key tonality and synthesizers.<ref>{{cite web|url=http://www.pandora.com/ivy-queen/drama-queen/acercate|title=Acercate - Ivy Queen|work=[[Pandora]]|publisher=Pandora Media Inc.|date=2010-07-10|accessdate=2012-11-25}}</ref> Production was handled by the Dominican-born duo of [[Luny Tunes]] and [[Tainy]]. Frances Tirado from ''Primera Hora'' described the song as being pure reggaeton and as a song that brings out the figure in Ivy Queen.<ref>{{cite web|url=http://www.reggaetonline.net/ivy-queen-20100714_news|title=Ivy Queen reina con su reguetón|language=Spanish|work=Reggaetonline|author=Frances Tirado|publisher=Pokus Web Solutions|date=2010-07-14|accessdate=2012-11-25}}</ref> "Acércate", along with the rest of the album, was recorded at [[Mas Flow Inc.|Mas Flow Studios]] in [[Carolina, Puerto Rico]].<ref>{{cite AV media notes|title=Drama Queen|titlelink=Drama Queen (Ivy Queen album)|others=[[Ivy Queen]]|year=2010|type=CD liner|publisher=[[Machete Music]]|id=0257-44031}}</ref><ref>{{cite web|url=http://www.cduniverse.com/productinfo.asp?pid=8227765&style=music&fulldesc=T|title=Ivy Queen - Drama Queen CD Album at CD Universe|work=[[CD Universe]]|publisher=Muze Inc|accessdate=2012-11-25}}</ref> A version with lead male vocals by Wisin & Yandel without lead vocals from Ivy Queen remains unreleased, with a running time of two minutes and fifty-three seconds. It too was produced by Luny Tunes and Tainy.<ref>{{cite web|url=http://www.nme.com/nme-video/youtube/id/opD23dR-II0|title=Wisin & Yandel - No Te Equivoques [Unreleased] (Sin Ivy Queen) [Prod. Luny Tunes & Tainy]|work=[[NME|New Musical Express]]|publisher=[[IPC Media|International Publishing Corporation]]|date=2010-05-05|accessdate=2013-06-21}}</ref>

"Acércate" was leaked onto the Internet prior to the album's release; a first in Queen's 15-year career.<ref name="Pirating">{{cite web|url=http://elnuevodiario.com.do/app/article.aspx?id=205431|title=Ivy Queen asegura vivir doble vida entre el rechazo al maltrato y la música|language=Spanish|publisher=''El Nuevo Diario''|date=2010-07-08| accessdate=2012-11-25}}</ref><ref>{{cite web|url=http://www.primerahora.com/nota-399327.html|title=Ivy Queen dice que "no soy una infeliz en el amor"|language=Spanish|author=Frances Tirado|publisher=''Primera Hora''|date=2010-07-08|accessdate=2012-11-25}}</ref> Originally entitled "No Te Equivoques", it was re-recorded in response to the infringement and included on the album retitled "Acércate".<ref name="Pirating"/> Reflecting on the situation Queen stated: "Sometimes they try to hurt you are when you do well. I'm proud and grateful that Wisin & Yandel have gone with me to the studio to record the song. We have good chemistry and friendship. We tried to change the song and lyrics, but with their agenda, which is tight, and mine too, we could not do it again. We have no idea who hacked, all the music was in a studio and to mobilize it to another was what someone did."<ref>{{cite web|url=http://www.primerahora.com/ivyqueendefiendealamujer-399584.html|title=Ivy Queen defiende a la mujer|language=Spanish|author=Frances Tirado|publisher=''Primera Hora''|date=2010-07-10| accessdate=2012-11-25}}</ref> The collaboration stemmed from Queen's previous collaboration with Wisin & Yandel on their seventh studio album ''[[La Revolución]]'' (2009) on "Perfecto" which also featured Yaviah.<ref>{{cite web|url=http://www.allmusic.com/album/la-revoluci%C3%B3n-mw0000818400|title=La Revolución - Wisin & Yandel : Songs, Reviews, Credits, Awards : AllMusic|work=[[Allmusic]]|publisher=Rovi Corporation|date=2009-05-26| accessdate=2012-12-12}}</ref>

==Credits and personnel==
*Credits adapted from [[Allmusic]]<ref>{{cite web|url=http://www.allmusic.com/album/drama-queen-mw0002006785/credits|title=Ivy Queen : Credits : Allmusic|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=2012-12-21}}</ref>

'''Recording'''
*Recorded at Mas Flow Studios in Carolina, Puerto Rico

'''Personnel'''
*José Cotto — Mixing
*Ivy Queen — Primary Artist, Composer
*Marcos "Tainy" Masis — Composer, Producer
*Francisco Saldaña — Composer
*Luny Tunes — Producer
*Wisin & Yandel — Composers, Featured Artist

==Charts==
While failing to chart on main Latin songs charts in ''Billboard'' magazine, it did manage to debut and peak at number sixteen on the ''Billboard'' Latin Rhythm Digital Songs chart, for the week of July 31, 2010.<ref name="LatinRhythmDigitalSongs">{{cite web|url=http://www.billboard.biz/bbbiz/charts/chart-search-results/singles/3121891|title=Latin Rhythm Digital Songs 2010-07-31|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=2010-07-31|accessdate=2012-09-03}}</ref> It charted simultaneously with the [[lead single]] "[[La Vida es Así|La Vida Es Así]]", which was at number two on the chart.<ref>{{Cite web|url=http://www.billboard.com/biz/charts/2010-07-31/latin-rhythm-digital-songs|title=Latin Rhythm Digital Songs: July 31, 2010 - Billboard Chart Archive|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=2010-07-31|accessdate=2013-05-02}}</ref>

{| class="wikitable plainrowheaders" style="text-align:center;"
|-
!scope="col"|Chart (2010)
!scope="col"|Peak <br> Position
|-
!scope="row"|US [[Latin Rhythm Airplay|Latin Rhythm Digital Songs]] (''[[Billboard (magazine)|Billboard]]'')<ref>{{Cite web|url={{BillboardURLbyName|artist=ivy queen|chart=Latin Rhythm Digital Songs}}|title=Ivy Queen - Chart History: Latin Rhythm Digital Songs|work=Billboard|publisher=Prometheus Global Media|accessdate=2013-02-16}}</ref>
| style="text-align:center;"|16
|-
|}

==References==
{{Reflist|30em}}

{{Ivy Queen singles}}
{{Wisin & Yandel singles}}

{{good article}}

{{DEFAULTSORT:Acercate}}
[[Category:2010 songs]]
[[Category:Ivy Queen songs]]
[[Category:Wisin & Yandel songs]]
[[Category:Spanish-language songs]]
[[Category:Songs written by Ivy Queen]]
[[Category:Song recordings produced by Luny Tunes]]
[[Category:Vocal duets]]
[[Category:Songs written by Wisin]]
[[Category:Songs written by Tainy]]
[[Category:Songs written by Yandel]]