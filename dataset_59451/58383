{{Infobox journal
| title = Journal of Pain & Palliative Care Pharmacotherapy
| cover = [[File:Journal of Pain and Palliative Care Pharmacotherapy.gif|150px]]
| editor = Arthur G. Lipman 
| discipline = [[Pain]], [[palliative care]], [[pharmacotherapy]]
| formername = Journal of Pharmaceutical Care in Pain & Symptom Control
| abbreviation = J. Pain Palliat. Pharmacother.
| publisher = [[Informa Healthcare]]
| country =
| frequency = Quarterly
| history = 1993-present
| openaccess = 
| license =
| impact = 
| impact-year = 2008
| website = http://informahealthcare.com/journal/ppc
| link1 = http://informahealthcare.com/toc/ppc/current
| link1-name = Online access
| link2 = http://informahealthcare.com/loi/ppc
| link2-name = Online archive
| JSTOR =
| OCLC = 47283753
| LCCN = 2001211724
| CODEN =
| ISSN = 1536-0288
| eISSN = 1536-0539
}}
The '''''Journal of Pain & Palliative Care Pharmacotherapy''''' is a quarterly [[peer-reviewed]] [[medical journal]] covering advances in acute, chronic, and [[end-of-life care|end-of-life]] symptom management. It is published by [[Informa Healthcare]] and the [[editor in chief]] is Arthur G. Lipman ([[University of Utah]] Health Sciences Center). The journal was established in 1993 as the ''Journal of Pharmaceutical Care in Pain & Symptom Control'', obtaining its current title in 2001.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|

* [[Academic Search Premier]]
* [[EBSCO Publishing|EBSCO databases]]
* [[CINAHL]]<ref name=CINAHL>{{cite web |url=http://www.ebscohost.com/titleLists/ccf-coverage.htm |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2015-04-18}}</ref>
* [[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/online-tools/embase/about |title=Journal titles covered in Embase |publisher=[[Elsevier]] |work=Embase |accessdate=2015-04-18}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101125608 |title=Journal of Pain & Palliative Care Pharmacotherapy |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-04-18}}</ref>
* [[PsycINFO]]<ref>{{citation |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2015-04-18}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-04-18}}</ref>
* [[PASCAL (database)|PASCAL]]
* ''[[Referativnyi Zhurnal]]''
}}

== References ==
{{reflist}}

== External links ==
* {{Official website|http://informahealthcare.com/journal/ppc}}

[[Category:Publications established in 1993]]
[[Category:Anesthesiology and palliative medicine journals]]
[[Category:Quarterly journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]