{{Infobox political party
|country = Abkhazia
|name = United Abkhazia
|native_name = Аҧсны Акзаара <br /> Единая Абхазия
|logo = Logo_ua.jpg
|leader = [[Sergei Shamba]]
|foundation = March 25, 2004
|position = [[Centre-right politics|Centre-right]]<ref>{{cite web|title=ROAR: United Russia “determined itself as a right-wing party”|url=https://www.rt.com/politics/medvedev-putin-united-russia/|accessdate=1 September 2016|language=en-EN}}</ref>
|ideology = [[Social Conservatism]]<ref>{{cite web|title=In Abkhazia the opposition demanding the resignation of the interior Minister, broke into the building Department. The kidnapped Secretary of the clone of the "United Russia"|url=http://en.reporter-ua.ru/in-abkhazia-the-opposition-demanding-the-resignation-of-the-interior-minister-broke-into-the-building-department-the-kidnapped-secretary-of-the-clone-of-the-united-russia.html|website=en.reporter-ua.ru|accessdate=1 September 2016}}</ref><ref>{{cite web|title=Russia parliament elections: How the parties line up - BBC News|url=http://www.bbc.com/news/world-europe-15939801|accessdate=1 September 2016}}</ref><br />[[Republicanism]]<ref>{{cite web|title=Абхаз Авто. Абхазия. Abkhaz-auto.ru {{!}} Новости Абхазии, Абхазия сегодня, Сергей   Шамба   избран   председателем   РПП   "диная   Абхазия"|url=http://abkhaz-auto.ru/news/3/5016/|website=Абхаз Авто|accessdate=15 July 2016}}</ref><ref>{{cite web|title=«Единая Абхазия»: уход в оппозицию (Голос России)|url=https://abh-n.ru/%D0%B5%D0%B4%D0%B8%D0%BD%D0%B0%D1%8F-%D0%B0%D0%B1%D1%85%D0%B0%D0%B7%D0%B8%D1%8F-%D1%83%D1%85%D0%BE%D0%B4-%D0%B2-%D0%BE%D0%BF%D0%BF%D0%BE%D0%B7%D0%B8%D1%86%D0%B8%D1%8E-%D0%B3%D0%BE%D0%BB%D0%BE/|website=abh-n.ru|accessdate=15 July 2016}}</ref>
|international =
|colours = Red
|colorcode = red
|headquarters =
|website = 
|seats1_title = [[People's Assembly of Abkhazia|People's Assembly]]
|seats1 = {{Composition bar|3|35|green}}
}}

'''United Abkhazia''' ({{lang-ab|Аҧсны Акзаара}}, {{lang-ru|Единая Абхазия}}) is a [[political party]] in [[Abkhazia]]. United Abkhazia was founded on March 25, 2004 as a socio-political movement, with the specific goal of presenting a single opposition candidate for the [[Abkhazian presidential election, 2004|October 2004 presidential elections]].<ref name="crossroads">[http://www.ingentaconnect.com/content/brill/ic/2005/00000009/00000001/art00011 Abkhazia at a Crossroads: On the Domestic Political Situation in the Republic of Abkhazia.] Alexander Skakov. In ''Iran and the Caucasus'', Volume 9, Number 1, 2005 , pp. 159-186. Retrieved on July 30, 2007.</ref>

It entered into an alliance with the [[Amtsakhara]] and [[Aitaira]] movements and the Federation of Independent Trade Unions and they named former Prime Minister [[Sergei Bagapsh]] as their joint candidate on July 20, 2004. Bagapsh won the elections by a small margin, but new elections were held because of a dispute over the results with runner up, former Prime Minister [[Raul Khadjimba]]. Bagapsh and Khadjimba agreed to run together on a national unity ticket and won [[Abkhazian presidential election, 2005|the rerun election in 2005]].

[[Artur Mikvabia]], chairman of United Abkhazia since it had first been created, announced 25 July 2007 that he would resign his post and retire from politics, but members of the party stated that they would not accept this, and Mikvabia remained chairman.<ref name="resignation">[https://web.archive.org/web/20070928081033/http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=7299 Политсовет Единой Абхазии с пониманием отнесся к моему решению завершить политическую карьеру&nbsp;– Артур Миквабия.]  Retrieved on August 7, 2007. {{ru icon}}</ref>

In 2008, United Abkhazia signed a cooperation agreement with Russian party [[United Russia]].<ref name="apress_rpp">{{cite news|last1=Jarsalia|first1=Said|title=РПП "Единая Абхазия" принимает участие в съезде "Единой России"|url=http://apsnypress.info/news/rpp-edinaya-abkhaziya-prinimaet-uchastie-v-sezde-edinoy-rossii/|accessdate=30 July 2016|agency=[[Apsnypress]]|date=27 June 2016}}</ref>

On 27 January 2009, almost five years after its foundation, United Abkhazia was transformed into a political party. [[Daur Tarba]], who had headed the [[Ochamchira district]] before, became the new chairman, Alkhas Kardava First Deputy Chairman and Zurab Kajaia Deputy Chairman.<ref name=apsnypress090127>{{cite web
|url = http://www.apsnypress.info/news2009/January/27.htm
|title = ОБЩЕСТВЕННО-ПОЛИТИЧЕСКОЕ ДВИЖЕНИЕ 'ЕДИНАЯ АБХАЗИЯ' ПРЕОБРАЗОВАНО В РЕСПУБЛИКАНСКУЮ ПОЛИТИЧЕСКУЮ ПАРТИЮ
|publisher = [[Apsnypress]]
|date = 27 January 2009
|accessdate = 29 January 2009
}}</ref>

United Abkhazia held its fourth congress on 27 January 2012, which was attended by 359 delegates, and during which 11 candidates for the [[Abkhazian parliamentary election, 2012|upcoming Parliamentary elections]] were chosen (the maximum number allowed).<ref name=ekho24465526>{{cite news|last=Galustyan|first=Levon|title=Анкваб не пришел|url=http://www.ekhokavkaza.org/content/article/24465526.html|accessdate=11 August 2013|newspaper=[[Echo of the Caucasus]]|date=27 January 2012}}</ref>

At its fifth congress, held on 12 June 2013 and attended by 330 delegates, United Abkhazia decided to pass into the opposition, with Tarba stating that President Ankvab, whose candidacy the party had supported in 2011, had failed to deliver meaningful achievements.<ref name=ekho25015343>{{cite news|last=Zavodskaya|first=Elena|title="Единая Абхазия" пополнила оппозицию|url=http://www.ekhokavkaza.org/content/article/25015343.html|accessdate=11 August 2013|newspaper=[[Echo of the Caucasus]]|date=12 June 2013}}</ref>

On 10 July 2013, United Abkhazia signed a cooperation agreement with fellow opposition parties and a number of social movements, forming the [[Coordinating Council of Political Parties and Public Organisations]].<ref name=apress9506>{{cite news|title=Ряд политических партий и общественных движений Абхазии подписали Соглашение о сотрудничестве и совместную декларацию|url=http://apsnypress.info/news/9506.html|accessdate=29 July 2013|newspaper=[[Apsnypress]]|date=10 July 2013}}</ref>

Daur Tarba resigned as Chairman of United Abkhazia on 1 October 2015 in a letter to its political council, in which he identified excessive formalism and a lack of internal political debates, and called for the party's rejuvenation. The political council accepted his resignation and appointed [[Aleksei Tania]] as acting Chairman.<ref name=apress_daur>{{cite news|title=Даур Тарба сложил с себя полномочия председателя РПП «Единая Абхазия»|url=http://www.apsnypress.info/news/daur-tarba-slozhil-s-sebya-polnomochiya-predsedatelya-rpp-edinaya-abkhaziya/|accessdate=11 October 2015|agency=[[Apsnypress]]|date=1 October 2015}}</ref> The following day, Tamaz Khashba, Spartak Kapba and Aleksandr Tsyshba, Chairmen of the Tquarchal, Ochamchira and Gagra regional branches reacted to the announcement by also resigning.<ref name=apress_rukovoditeli>{{cite news|title=Руководители трех региональных отделений РПП «Единая Абхазия» заявили о сложении своих полномочий|url=http://www.apsnypress.info/news/rukovoditeli-trekh-regionalnykh-otdeleniy-rpp-edinaya-abkhaziya-zayavili-o-slozhenii-svoikh-polnomoch/|accessdate=11 October 2015|agency=[[Apsnypress]]|date=2 October 2015}}</ref>

On 27 January 2016, United Abkhazia held its sixth congress, which was attended by 375 delegates. It elected MP and former Prime Minister [[Sergei Shamba]] as its Chairman, as well as MP [[Robert Yaylyan]], jurist [[Viktor Vasiliev]] and [[Dato Kajaia]] as Deputy Chairmen and 43 members of its political council. It was said in the sidelines of the congress that Tarba had stepped down so as to make room for his long-time friend Shamba.<ref name=ainform3053>{{cite news|title=СЕРГЕЙ ШАМБА ИЗБРАН ПРЕДСЕДАТЕЛЕМ ПАРТИИ «ЕДИНАЯ АБХАЗИЯ»|url=http://abkhazinform.com/item/3053-sergej-shamba-izbran-predsedatelem-partii-edinaya-abkhaziya|accessdate=31 January 2016|agency=[[Abkhazia Inform]]|date=27 January 2016}}</ref><ref name="ekho27515439">{{cite news|last1=Sharia|first1=Vitali|title=Сергей Шамба – во главе «Единой Абхазии»|url=http://www.ekhokavkaza.com/content/article/27515439.html|accessdate=6 February 2016|agency=[[Echo of the Caucasus]]|date=27 January 2016}}</ref> During the congress, some speakers criticised the government and on 9 February, United Abkhazia announced that it was leaving the Coordinating Council of Political Parties and Public Organisations, claiming that its role had been marginalised by the government.<ref name="ainform3132">{{cite news|title=«ЕДИНАЯ АБХАЗИЯ» НЕ МОЖЕТ МИРИТЬСЯ С РОЛЬЮ СТАТИСТА В КООРДИНАЦИОННОМ СОВЕТЕ И ЗАЯВЛЯЕТ О СВОЕМ ВЫХОДЕ ИЗ НЕГО|url=http://abkhazinform.com/item/3132-edinaya-abkhaziya-ne-mozhet-miritsya-s-rolyu-statista-v-koordinatsionnom-sovete-i-zayavlyaet-o-svoem-vykhode-iz-nego|accessdate=14 February 2016|agency=[[Abkhazia Inform]]|date=10 February 2016}}</ref><ref name="ekho27545787">{{cite news|last1=Sharia|first1=Vitali|title=«Единая Абхазия»: новая смена курса|url=http://www.ekhokavkaza.com/content/article/27545787.html|accessdate=14 February 2016|agency=[[Echo of the Caucasus]]|date=11 February 2016}}</ref> United Abkhazia proceeded to initiate, together with the [[Party for the Economic Development of Abkhazia]], the [[Council for the National Unity of the Republic of Abkhazia]], uniting political forces neither pro-government nor pro-opposition, which was formally established on 29 February.<ref name="ainform3236">{{cite news|title=В АБХАЗИИ СОЗДАН СОВЕТ НАЦИОНАЛЬНОГО ЕДИНСТВА РА|url=http://abkhazinform.com/item/3236-v-abkhazii-sozdan-sovet-natsionalnogo-edinstva-ra|accessdate=7 March 2016|agency=[[Abkhazia Inform]]|date=29 February 2016}}</ref><ref name="ainform3237">{{cite news|title=СЕРГЕЙ ШАМБА О СНЕРА: «МЫ НЕ ПРОВЛАСТНЫЕ, НО МЫ И НЕ ОППОЗИЦИОННЫЕ ПОЛИТИЧЕСКИЕ СИЛЫ»|url=http://abkhazinform.com/item/3237-sergej-shamba-o-snera-my-ne-provlastnye-no-my-i-ne-oppozitsionnye-politicheskie-sily|accessdate=7 March 2016|agency=[[Abkhazia Inform]]|date=29 February 2016}}</ref>

On 8 July, United Abkhazia signed a cooperation agreement with South Ossetian party [[United Ossetia]].<ref name="apress_mezhdu">{{cite news|last1=Jarsalia|first1=Said|title=Между «Единой Абхазией» и «Единой Осетией» подписано межпартийное Соглашение|url=http://apsnypress.info/news/mezhdu-edinoy-abkhaziey-i-edinoy-osetiey-podpisano-mezhpartiynoe-soglashenie/|accessdate=30 July 2016|agency=[[Apsnypress]]|date=8 July 2016}}</ref>

On 5 December, United Abkhazia's political council deliberated about the confrontation between the government and the opposition, and in particular [[Amtsakhara]]'s call for President [[Raul Khajimba]] to resign by 15 December. A proposal by Gagra party branch Head [[Teimuraz Khishba]] to call upon Khajimba to resign to reduce tension received support only from the party's Deputy Chairman [[Data Kajaia]] and Gudauta party branch Head [[Vitali Jenia]]. Khishba subsequently resigned as party branch head.<ref name="ainform4987">{{cite news|title=В ПОЛИТИЧЕСКОЙ ПАРТИИ «ЕДИНАЯ АБХАЗИЯ ПО-РАЗНОМУ ОТНОСЯТСЯ К ТРЕБОВАНИЮ БЛОКА ОППОЗИЦИОННЫХ СИЛ О ДОСРОЧНОЙ ОТСТАВКЕ ПРЕЗИДЕНТА РАУЛЯ ХАДЖИМБА|url=http://abkhazinform.com/item/4987-v-politicheskoj-partii-edinaya-abkhaziya-po-raznomu-otnosyatsya-k-trebovaniyu-bloka-oppozitsionnykh-sil-o-dosrochnoj-otstavke-prezidenta-raulya-khadzhimba|accessdate=11 December 2016|agency=[[Abkhazia Inform]]|date=5 December 2016}}</ref>

==Election results==
===Presidential===
{| class="wikitable"
|-
! Year !! Candidate !! First Round !! Second Round !! Result
|-
| [[Abkhazian presidential election, 2004|2004]] || [[Sergei Bagapsh]] ||  50.08% || - ||  {{yes2|Won}}
|-
| [[Abkhazian presidential election, 2005|2005]] || [[Sergei Bagapsh]] || 91.54% || - ||  {{yes2|Won}}
|-
| [[Abkhazian presidential election, 2009|2009]] || [[Sergei Bagapsh]] || 61.16% || - ||  {{yes2|Won}}
|-
| [[Abkhazian presidential election, 2011|2011]] || [[Alexander Ankvab]] || 54.90% || - ||  {{yes2|Won}}
|-
| [[Abkhazian presidential election, 2014|2014]] || [[Raul Khajimba]] || 50.60% || - ||  {{yes2|Won}}
|}
===Parliamentary===
{| class="wikitable"
|-
! Year !! Seats !! Places
|-
| [[Abkhazian parliamentary election, 2002|2002]]<ref>Since 2004 as Unity faction</ref> || 15 || 1st
|-
| [[Abkhazian parliamentary election, 2007|2007]] || 28 || 1st
|-
| [[Abkhazian parliamentary election, 2012|2012]]  || 3 || 2nd
|}
==References==
{{reflist|1}}

{{Abkhazian political parties}}

[[Category:Political parties in Abkhazia]]
[[Category:2004 establishments in Abkhazia]]