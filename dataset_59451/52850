{{Infobox journal
| title = Social & Legal Studies
| cover = [[File:Social & Legal Studies journal front cover image.jpg]]
| editor = Carl F. Stychin, David Campbell, David Cowan, Marie B. Fox 
| discipline = [[Sociology]]
| peer-reviewed = 
| language = 
| former_names = 
| abbreviation = Soc. Leg. Stud.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1992-present
| openaccess = 
| license = 
| impact = 0.673 
| impact-year = 2010
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200832
| link1 = http://sls.sagepub.com/content/current
| link1-name = Online access
| link2 = http://sls.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 25566142
| LCCN = 94644509
| CODEN = SLSTEK
| ISSN = 0964-6639
| eISSN = 1461-7390
}}
'''''Social & Legal Studies''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers [[feminist]], [[post-colonial]], and [[socialist]] economic perspectives to the study of [[law]] and [[criminology]]. The [[editors-in-chief]] are Carl Stychin ([[University of Reading]]), David Campbell ([[University of Leeds]]), David Brown ([[University of Bristol]]), and Marie Fox ([[University of Birmingham]]). It was established in 1992 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''Social & Legal Studies'' is abstracted and indexed in [[Academic Search Premier]], the [[Legal Journals Index]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 0.673, ranking it 28th out of 46 journals in the category "Criminology and Penology",<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Criminology and Penology |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> 76 out of 133 journals in the category "Law",<ref name=WoS1>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Law |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> and 40th out of 84 journals in the category "Social Sciences, Interdisciplinary".<ref name=WoS2>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Social Sciences, Interdisciplinary |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://sls.sagepub.com/}}

{{DEFAULTSORT:Social and Legal Studies}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1992]]
[[Category:Criminology journals]]
[[Category:Quarterly journals]]