[[File:MossiCavalry.jpg|thumb|250px|Mossi cavalry, a formidable force in the region.]]
'''Wobogo''' (died 1904) was the [[Mogho Naba]] (ruler) of [[Ouagadougou]] from 1890 to 1897, at the time of the French colonial conquest of [[French Upper Volta|Upper Volta]].
Wobogu was originally called Boukary Koutou, but dropped those names on his accession and assumed the name "Wobogu", meaning elephant.<ref name=LipschutzRasmussen1989/>

==Years of independence==

Ouagadougou was the main [[Mossi Kingdoms|Mossi Kingdom]] in what is now [[Burkina Faso]], with its origins in the 15th century. The [[Mossi Kingdoms|Mossi]] were a warlike nation with formidable cavalry, who had successfully resisted all past invaders. 
With the decline of the central state in the 18th century, [[Yatenga]], with its capital at [[Ouahigouya]], had become independent of Ouagadougou, as had other places. 
During the 19th century there was sporadic warfare among the Mossi.<ref name=MansonKnight2006/>
 
Boukary Koutou's father, ruler of Ouagadougou, died in 1850 when Boukary was a young man.
Boukary competed unsuccessfully with his brother Sanum to become ruler but was disallowed due to his youth.<ref name=LipschutzRasmussen1989/>
After fomenting a civil war against his brother, he was exiled to Banema on the border of the kingdom.<ref name=BruyasGutsche2001/>
There he made a living through slave trading, capturing the slaves in mounted [[Raid (military)|razzia]]s directed at the villages of the [[Gurunsi|Gurunsi people]].<ref name="Bazémo2007"/>

When Sanum died in 1890, he left no male heir, and all his brothers were in competition for the throne.
The elders were reluctant to make Boukary the [[Mogho Naba]], since his rebellion had made him ineligible, but were persuaded when he surrounded their meeting place with soldiers.<ref name=LipschutzRasmussen1989/><ref name=BruyasGutsche2001/>
Although now the principal Mossi ruler, Wobogo was not in full control. At the time of Dr. Francois Crozat's visit in September 1890, the [[Yako]] and [[Yatenga]] people were engaged in civil war. Wobogo attempted to halt the fighting to avoid giving a poor impression to his European visitor, but was not successful.<ref name=Skinner1964a/>

==Character==

The French explorer [[Louis Gustave Binger]] visited the Mossi in 1888. He described Boukary Koutou, whom he found in exile at Bassawarga, as a tall and handsome man in his forties, neatly dressed, with a round face decorated with tattoos and a small goatee. 
He said of the future ruler "His whole appearance denotes intelligence. He seems to be a just man, but firm in his resolutions".
Binger saw a resemblance to Iamory Ouattara, ruler of the [[Kong Empire|Kong]] state of Knniara, but said Boukary had much finer features.<ref>{{cite web
 |url=http://www.journalbendre.net/spip.php?article3423
 |author=Bendr&eacute;
 |title=Binger arrive chez Boukary Koutou en exil à Bassawarga
 |work=Hebdomadaire d’information et de réflexion - Burkina-Faso
 |language=French
 |date=2010-06-30
 |accessdate=2010-10-13}}</ref>

However, Crozat was not impressed by Wobogo upon meeting him in 1890.  He described him in his reports as "timid, shaky" and living "confined to his palace with his servants and wives, not daring to go out".  Crozat reported that Wobogo "spent his time consulting holy men who had him under their control and who used up the resources of the realm to contrive the oddest and most expensive charms in order to allay the ruler's fear that he would be assassinated".
Crozat reported that Wobogo had a hundred wives, although he did not see this many when he visited since many lived in neighboring villages.<ref name=Skinner1964b/>

==Defeat and exile==

Wobogo ruled at a time when the French and British were taking control of the region, and intensely distrusted both.<ref name=LipschutzRasmussen1989/>
When the French explorer [[Parfait-Louis Monteil]] visited Ouagadougou in 1891, hoping Wobogo would agree to a French protectorate, Wogobo refused to receive him. Monteil was forced to make a hurried departure.<ref name=Englebert1999/><ref name=Abaka2004/>
In 1894, Wobogo signed a treaty of friendship with [[George Ekem Ferguson]], a [[Fante people|Fante]] representative of the British in the [[Gold Coast (British colony)|Gold Coast]] (now [[Ghana]]).<ref name=Abaka2004/>
In 1896, the French invaded Ouagadougou, and when he attempted to resist his town was burned down.
The French commander Voulet deposed Wobogo in 1897, replacing him with his brother Sighiri.
Wobogo appealed to the British without success.
After further fighting, he fled to [[Zongoiri]] just across the border in the northeast of the Gold Coast, where he died in 1904.<ref name=LipschutzRasmussen1989/>

==References==
{{reflist|1|refs=

<ref name=Abaka2004>{{harvnb|Abaka|2004|}}</ref>
<ref name="Bazémo2007">{{harvnb|Bazémo|2007|url=https://books.google.com/books?id=7JvLvYqA05IC&pg=PA48|pp=48}}</ref>
<ref name=BruyasGutsche2001>{{harvnb|Bruyas|Gutsche|2001|url=https://books.google.com/books?id=ydhiFAHza50C&pg=PA195|pp=195ff}}</ref>
<ref name=Englebert1999>{{harvnb|Englebert|1999|pp=18}}</ref>
<ref name=LipschutzRasmussen1989>{{harvnb|Lipschutz|Rasmussen|1989|pp=249}}</ref>
<ref name=MansonKnight2006>{{harvnb|Manson|Knight|2006|url=https://books.google.com/books?id=sFCmqM_xUnoC&pg=PA18|pp=18ff}}</ref>
<ref name=Skinner1964a>{{harvnb|Skinner|1964|pp=62ff}}</ref>
<ref name=Skinner1964b>{{harvnb|Skinner|1964|pp=144}}</ref>

}}

=== Bibliography for references ===
{{refbegin|1}}
*{{cite encyclopaedia|ref=harv|chapterurl=http://www.bookrags.com/tandf/collaboration-as-resistance-tf/
 |article=Collaboration as Resistance
 |encyclopedia=[[Encyclopedia of African History]]
 |volume=1 A&ndash;G
 |first=Edmund|last=Abaka
 |editor=Kevin Shillington
 |isbn=978-0-203-48386-2
 |year=2004
 |oclc=62895404
 |publisher=Taylor and Francis
 |accessdate=2010-10-10}}
*{{cite book|ref=harv
 |title=Esclaves et esclavage dans les anciens pays du Burkina Faso
 |first=Maurice|last=Bazémo
 |publisher=Editions L'Harmattan
 |year=2007
 |ISBN=2-296-04393-3
 |accessdate=2010-10-13}}
*{{cite book|ref=harv|title=Les sociétés traditionnelles de l'Afrique noire
 |language=French
 |first1=Jean|last1=Bruyas|first2=Claudia|last2=Gutsche
 |publisher=Editions L'Harmattan
 |year=2001
 |ISBN=2-7475-0505-7
 |accessdate=2010-10-13}}
*{{cite book|ref=harv|title=Burkina Faso: Unsteady Statehood In West Africa
 |first=Pierre
 |last=Englebert
 |publisher=Westview Press
 |year=1999
 |url=https://books.google.com/books?id=nRun6jow05kC&pg=PA18
 |ISBN=0-8133-3680-5
 |accessdate=2010-10-10}}
*{{cite book|ref=harv|title=Dictionary of African historical biography
 |first1=Mark R.
 |last1=Lipschutz
 |first2=R. Kent
 |last2=Rasmussen
 |publisher=University of California Press
 |year=1989
 |url=https://books.google.com/books?id=QYoPkk04Yp4C&pg=PA249
 |ISBN=0-520-06611-1}}
*{{cite book|ref=harv|title=Burkina Faso: The Bradt Travel Guide
 |first1=Katrina|last1=Manson|first2=James|last2=Knight
 |publisher=Bradt Travel Guides
 |year=2006
 |ISBN=1-84162-154-4
 |accessdate=2010-10-13}}
*{{cite book|ref=harv|title=The Mossi of the Upper Volta: the political development of a Sudanese people
 |first=Elliott Percival
 |last=Skinner
 |publisher=Stanford University Press
 |year=1964
 |url=https://books.google.com/books?id=b56aAAAAIAAJ&pg=PA62
 |ISBN=0-8047-0166-0}}
{{refend}}

{{DEFAULTSORT:Wobogo}}
[[Category:Year of birth missing]]
[[Category:1904 deaths]]
[[Category:History of Burkina Faso]]
[[Category:People of French West Africa]]