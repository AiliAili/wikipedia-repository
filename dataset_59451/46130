{{Infobox military unit
|unit_name= Allied Maritime Command Naples
|image=[[File:Allied Maritime Command Naples.png|175px]]
|caption=Coat of arms
|dates=1999 &ndash; 2013
|country=
|allegiance={{nowrap|{{flagicon|NATO}} [[NATO|North Atlantic Treaty Organization]]}}
|branch=
|type=
|role=
|size=
|command_structure=[[Supreme Headquarters Allied Powers Europe|Allied Command Operations]], Casteau, Belgium
|garrison=[[Naples]], [[Italy]]
|garrison_label=HQ
|motto=
|colors=
|colors_label=
|march=
|mascot=
|equipment=
|equipment_label=
|battles=[[2011 military intervention in Libya|Military intervention in Libya]]
|anniversaries=
|decorations=
|battle_honours=
<!-- Commanders -->
|commander1=
|commander1_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=
|identification_symbol_2_label=
}}
'''Allied Maritime Command (MC) Naples''' ('''MC Naples''') was a subordinate command of [[Allied Joint Force Command Naples]]. MC Naples operated from the island of Nisida in the Gulf of Pozzuoli and its commander reported directly to the Commander Allied Joint Force Command Naples (Com JFC Naples).<ref>[http://www.manp.nato.int/index.htm Allied Maritime Command Naples]</ref> The command was deactivated in March 2013.

==History==
In June 1967, [[Allied Forces Mediterranean]] was deactivated and a new force, Allied Naval Forces Southern Europe (NAVSOUTH), was created. NAVSOUTH was a subordinate command under the Commander-in-Chief, [[Allied Forces Southern Europe]] (CINCAFSOUTH). The NAVSOUTH area of responsibility was broken down into four geographical subordinate commands: Gibraltar/Mediterranean (COMGIBMED, commanded by the commodore in charge of [[British Forces Gibraltar]]), Central Mediterranean (COMEDCENT, under an Italian admiral), Eastern Mediterranean (COMEDEAST, under a Greek admiral), and North-eastern Mediterranean (COMEDNOREAST, under a Turkish admiral). A further reorganization resulted in the Commander U.S. Fleet Air Wing Mediterranean and the U.S. Commander Submarine Flotilla 8 being re-designated as, respectively, Maritime Air Forces Mediterranean (COMMARAIRMED) and Commander, Submarines Mediterranean (COMSUBMED).

In May, 1999, a permanent naval mine countermeasures unit was created for the Mediterranean. The new unit was designated the Mine Counter Measures Force Mediterranean (MCMFM) and was subordinate to the Com NAVSOUTH.

On 1 September 1999, the NATO force structure was reorganized. As result of this reorganization four Mediterranean maritime commands (COMEDCENT, COMEDEAST, COMEDNOREAST, COMGIBMED) were stood down, and COMSUBMED and COMMARAIRMED were transformed in special entities of NAVSOUTH, as COMSUBSOUTH and COMMARAIRNAPLES.

In July 2004, the designation NAVSOUTH was changed to Allied Maritime Component Command Naples (CC-Mar Naples). Along with name change came new expanded roles and missions.

A further re-designation occurred on 1 January 2005, when STANAVFORMED and MCMFORMED were renamed, respectively, Standing NATO Maritime Group 2 (SNMG2) and Standing NATO Mine Countermeasures Force Group 2 (SNMCMG2).<ref>[http://www.manp.nato.int/Factsheets/Factsheet_History.html Allied Maritime Command Naples - History]</ref> 

Israel signed an Exchange of Letters (EOL) on 4 October 2006, regarding [[Operation Active Endeavour]], followed on 6 June 2007 by a Tactical Memorandum of Understanding (TMOU) between CC-Mar Naples and the Israeli Navy. An Israeli Liaison Officer was assigned to CC-Mar Naples on 29 January 2008.

On 1 March 2010, Allied Maritime Component Command Naples (CC-Mar Naples) was renamed Allied Maritime Command (MC) Naples.<ref>[http://www.manp.nato.int/Factsheets/mc_naples1.htm Maritime Command Naples]</ref>

Allied Maritime Command Naples was disbanded on 27 March 2013 as part of a program to streamline NATO's command structure. Its responsibilities were transferred to [[Allied Maritime Command]], located at Northwood in the United Kingdom.<ref>{{cite web|title=Deactivation ceremony of Allied Maritime Command Naples|url=http://www.jfcnaples.nato.int/page11122031/2013/deactivation-ceremony-of-allied-maritime-command-naples.aspx|publisher=Headquarters Allied Joint Force Command Naples|accessdate=16 April 2013}}</ref>

==Mission==

The mission of COM MC Naples was to "plan, conduct and support the full range of military operations anywhere in the world in order to deter aggression, contribute to effective crisis management and promote peace, security and stability." <ref>[http://www.manp.nato.int/Factsheets/factsheet_missi.htm Allied Maritime Command Naples - Mission]</ref> As such, it was responsible for the oversight, planning, coordination and execution of the full range of maritime-related tasks that could include humanitarian and disaster relief operations, maritime security or full-scale military operations.

On 24 October 2008, COM MC Naples was given responsibility for [[Operation Allied Provider]], which was NATO's maritime operation to combat piracy off Somali coast in support to World Food Program (WFP) chartered vessels. This operation ended 12 December 2008, after the EU [[Operation Atalanta]] became operational.

In February 2011, due to the civil war occurring in Libya, the United Nations authorized an arms embargo against Libya. To enforce this embargo, NATO started [[2011 military intervention in Libya|Operation Unified Protector]] which included both air and maritime elements. The maritime portion of the operation fell under the command of COM MC Naples. Operation Unified Protector officially ended on 31 October 2011, after the Libyan rebels had formed a transitional government.<ref name="Ibid">[ibid]</ref>

==Structure==

MC Naples was one of three subordinate commands of [[Joint Force Command Naples]]. It was located on [[Nisida Island]], in the Gulf of Pozzuoli, which in turn is part of the Bay of Naples. It was commanded by an Italian three-star Admiral with staff coming from 17 countries including Albania, Bulgaria, Canada, France, Germany, Greece, Italy, the Netherlands, Norway, Poland, Portugal, Romania, Slovenia, Spain, Turkey, the United Kingdom and the United States. Israel also provided a liaison officer.

COM MC Naples controlled two maritime Immediate Reaction Forces; [[Standing NATO Maritime Group 2]] (SNMG2) and the [[Standing NATO Mine Countermeasures Group 2]] (SNMCMG2). Besides SNMG2 and SNMCMG2, two further commands, Commander Maritime Air Naples (COMARAIRNAPLES) and Commander Submarines Allied Naval Forces South (COMSUBSOUTH) were also subordinate to COM MC Naples.<ref name="Ibid" />

==References ==
{{Reflist}}

[[Category:Formations of the NATO Military Command Structure]]
[[Category:Military units and formations established in 1999]]