{{Other uses}}
{{Distinguish|Debit}}
{{Use dmy dates|date=April 2016}}
{{Use American English|date=April 2016}}

{{Refimprove|date=January 2009}}

[[File:Payday loan shop window.jpg|thumb|right|[[Payday loan]] businesses lend money to customers, who then owe a debt to the payday loan company.]]
{{Corporate finance}}
{{Finance sidebar}}
{{Personal finance}}

'''Debt''' is [[money]] owed by one party, the [[borrower]] or [[debtor]], to a second party, the lender or [[creditor]]. The borrower may be a [[sovereign state]] or country, [[local government]], [[company]], or an individual.  The lender may be a [[bank]], [[credit card]] company, [[payday loan]] provider, or an individual. Debt is generally subject to [[contractual term]]s regarding the amount and timing of repayments of [[Principal (finance)|principal]] and [[interest]].<ref>{{cite web|url=http://www.investopedia.com/terms/d/debt.asp|title=Debt Definition|publisher=Investopedia|accessdate=16 May 2012}}</ref> [[Loan]]s, [[bond (finance)|bonds]], notes, and [[mortgage loan|mortgages]] are all types of debt.  The term can also be used [[metaphor]]ically to cover [[morality|moral]] obligations and other interactions not based on [[economic value]].<ref>{{OED|debt}}</ref> For example, in Western cultures, a person who has been helped by a second person is sometimes said to owe a "debt of gratitude" to the second person.

== Terms ==

=== Interest ===
[[Interest]] is the fee paid by the borrower to the lender.  Interest is calculated as a percentage of the outstanding principal, which percentage is known as an [[interest rate]], and is generally paid periodically at intervals, such as monthly or semi-annually.

Interest rates may be [[Fixed interest rate loan|fixed]] or [[Floating interest rate#floating rate loan|floating]]. In floating-rate structures, the rate of interest that the borrower pays during each time period is tied to a benchmark such as [[LIBOR]] or, in the case of [[inflation-indexed bond]]s, [[inflation]].

There are many different conventions for calculating interest.  Depending on the terms of the debt, [[compound interest]] may accumulate at a specific interval.  In addition, different [[day count convention]]s exist, for example, sometimes each month is considered to have exactly thirty days, such that the interest payment due is the same in each calendar month.  The [[annual percentage rate]] (APR) is a standardized way to calculate and compare interest rates on an annual basis.  Quoting interest rates using APR is required by regulation for most loans to individuals in the United States and United Kingdom.  

For some loans, the amount actually loaned to the debtor is less than the principal sum to be repaid. This may be because upfront fees or [[point (mortgage)|points]] are charged, or because the loan has been structured to be [[Islamic banking|sharia-compliant]]. The additional principal due at the end of the term has the same economic effect as a higher interest rate. This is sometimes referred to as a [[wikt:banker's dozen|banker's dozen]], a play on "[[baker's dozen]]" – owe twelve (a dozen), receive a loan of eleven (a banker's dozen). Note that the effective interest rate is not equal to the discount: if one borrows $10 and must repay $11, then this is ($11{{ndash}}$10)/$10 = 10 percent interest; however, if one borrows $9 and must repay $10, then this is ($10–$9)/$9 = 11-1/9 percent interest.<ref>Formally, a discount of ''d''% results in effective interest of <math>d/(1-d)\%.</math></ref>

=== Repayment ===
There are three main ways repayment may be structured: the entire principal balance may be due at the maturity of the loan; the entire principal balance may be [[Amortization (business)|amortized]] over the term of the loan; or the loan may partially amortize during its term, with the remaining principal due as a "[[balloon payment]]" at maturity. Amortization structures are common in [[mortgage]]s and [[credit card]]s.

===Default provisions===

Debtors of every type [[default (finance)|default]] on their debt from time to time, with various consequences depending on the terms of the debt and the law governing default in the relevant jurisdiction.  
If the debt was secured by specific [[collateral (finance)|collateral]], such as a car or home, the creditor may seek to repossess the collateral.  In more serious circumstances, individuals and companies may go into [[bankruptcy]].

Riskier borrowers must generally pay higher rates of interest to compensate lenders for taking on the additional risk of default.  Debt investors assess the risk of default prior to making a loan, for example through credit scores and corporate and sovereign ratings.

== Types of borrowers ==

=== Individuals ===
Common types of debt owed by individuals and households include [[mortgage loan]]s, car loans, and [[credit card]] debt. For individuals, debt is a means of using anticipated [[income]] and future [[purchasing power]] in the present before it has actually been earned. Commonly, people in industrialized nations use consumer debt to purchase houses, cars and other things too expensive to buy with cash on hand.

People are more likely to spend more and get into debt when they use credit cards vs. cash for buying products and services.<ref>Chatterjee, P., & Rose, R. L. (2012). Do payment mechanisms change the way consumers
perceive products? Journal of Consumer Research, 38(6), 1129–1139.</ref><ref>Pettit, N. C., & Sivanathan, N. (2011). The plastic trap. Social Psychological and Personality Science, 2(2), 146-153.</ref><ref name="auto">Prelec, D. & Loewenstein, G. (1998). The red and the black: Mental accounting of savings and debt. Marketing Science, 17(1), 4-28.</ref><ref name="auto1">Raghubir, P. & Srivastava, J. (2008), Monopoly money: The effect of payment coupling and form on spending behavior. Journal of Experimental Psychology: Applied, 14 (3), 213–25.</ref><ref>Soman, D. (2003). The effect of payment transparency on consumption: Quasi
experiments from the field. Marketing Letters, 14, 173–183.</ref> This is primarily because of the transparency effect and consumer’s "pain of paying."<ref name="auto" /><ref name="auto2">Soman, D. (2003). The effect of payment transparency on consumption: Quasi experiments from the field. Marketing Letters, 14, 173–183.</ref> The transparency effect refers to the fact that the further you are from cash (as in a credit card or another form of payment), the less transparent it is and the less you remember how much you spent.<ref name="auto2" /> The less transparent or further away from cash, the form of payment employed is, the less an individual feels the “pain of paying” and thus is likely to spend more.<ref name="auto" /> Furthermore, the differing physical appearance/form that credit cards have from cash may cause them to be viewed as [[Monopoly money#As a phrase|“monopoly” money]] vs. real money, luring individuals to spend more money than they would if they only had cash available.<ref name="auto1" /><ref>Chatterjee, P., & Rose, R. L. (2012). Do payment mechanisms change the way consumers perceive products? Journal of Consumer Research, 38(6), 1129–1139.</ref>

Besides these more formal debts, private individuals also lend informally to other people, mostly relatives or friends. One reason for such informal debts is that many people, in particular those who are poor, have no access to affordable credit. Such debts can cause problems when they are not paid back according to expectations of the lending household. In 2011, 8 percent of people in the [[European Union]] reported their households has been in arrears, that is, unable to pay as scheduled "payments related to informal loans from friends or relatives not living in your household".<ref>{{cite web|url=http://www.eurofound.europa.eu/sites/default/files/ef_files/pubdocs/2013/73/en/2/EF1373EN.pdf|title=Household over-indebtedness in the EU: The role of informal debts.|last=|first=|date=2013|website=eurofound.europa.eu|publisher=Publications Office of the European Union, Luxembourg|access-date=19 April 2016}}</ref>

=== Businesses ===
A [[company]] may use various kinds of debt to [[finance]] its [[Business operations|operations]] as a part of its overall [[corporate finance]] strategy.

A [[term loan]] is the simplest form of corporate debt. It consists of an agreement to lend a fixed amount of money, called the [[principal sum]] or principal, for a fixed period of time, with this amount to be repaid by a certain date. In commercial loans [[interest]], calculated as a percentage of the principal sum per year, will also have to be paid by that date, or may be paid periodically in the interval, such as annually or monthly. Such loans are also colloquially called "[[bullet loan]]s", particularly if there is only a single payment at the end – the "bullet" – without a "stream" of interest payments during the life of the loan.

A [[syndicated loan]] is a loan that is granted to companies that wish to borrow more money than any single lender is prepared to risk in a single loan. A syndicated loan is provided by a group of lenders and is structured, arranged, and administered by one or several commercial banks or investment banks known as arrangers. Loan syndication is a [[risk management]] tool that allows the lead banks [[underwriting]] the debt to reduce their risk and free up lending capacity.

A company may also issue [[Bond (finance)|bonds]], which are debt [[security (finance)|securities]]. Bonds have a fixed lifetime, usually a number of [[year]]s; with long-term bonds, lasting over 30 years, being less common. At the end of the bond's life the money should be repaid in full. Interest may be added to the end payment, or can be paid in regular installments (known as [[coupon (bond)|coupons]]) during the life of the bond.

A [[letter of credit]] or LC can also be the source of payment for a transaction, meaning that redeeming the letter of credit will pay an exporter. Letters of credit are used primarily in international trade transactions of significant value, for deals between a supplier in one country and a customer in another. They are also used in the [[land development]] process to ensure that approved public facilities (streets, sidewalks, stormwater ponds, etc.) will be built. The parties to a letter of credit are usually a beneficiary who is to receive the money, the issuing bank of whom the applicant is a client, and the [[advising bank]] of whom the beneficiary is a client. Almost all letters of credit are irrevocable, i.e., cannot be amended or canceled without prior agreement of the beneficiary, the issuing bank and the confirming bank, if any. In executing a transaction, letters of credit incorporate functions common to [[giro]]s and [[traveler's cheque]]. Typically, the documents a beneficiary has to present in order to receive payment include a [[commercial invoice]], [[bill of lading]], and a document proving the shipment was insured against loss or damage in transit. However, the list and form of documents is open to imagination and negotiation and might contain requirements to present documents issued by a neutral third party evidencing the quality of the goods shipped, or their place of origin.

Companies also use debt in many ways to leverage the [[investment]] made in their [[asset]]s, "leveraging" the return on their [[Stock|equity]]. This [[leverage (finance)|leverage]], the proportion of debt to equity, is considered important in determining the riskiness of an investment; the more debt per equity, the riskier.

=== Governments ===
Governments issue debt to pay for ongoing expenses as well as major capital projects. Government debt may be issued by sovereign states as well as by local governments, sometimes known as municipalities.

The overall level of indebtedness by a government is typically shown as a ratio of debt-to-GDP. This ratio helps to assess the speed of changes in government indebtedness and the size of the debt due.

== Assessments of creditworthiness ==

=== Income metrics ===

The [[debt service coverage ratio]] is the ratio of income available to the amount of debt service due (including both interest and principal amortization, if any).  The higher the debt service coverage ratio, the more income is available to pay debt service, and the easier and lower-cost it will be for a borrower to obtain financing.

=== Value metrics ===

The [[loan-to-value ratio]] is the ratio of the total amount of the loan to the total value of the [[collateral (finance)|collateral]] securing the loan.

=== Collateral and recourse ===
A debt obligation is considered secured if creditors have recourse to specific [[collateral (finance)|collateral]]. Collateral may include claims on tax receipts (in the case of a government), specific assets (in the case of a company) or a home (in the case of a consumer). Unsecured debt comprises financial obligations for which creditors do not have recourse to the [[asset]]s of the borrower to satisfy their claims.

=== Role of rating agencies ===
Specific bond debts owed by both governments and private corporations are rated by [[Credit rating agency|rating agencies]], such as [[Moody's]], [[Standard & Poor's]], [[Fitch Ratings]], and [[A. M. Best]]. The government or company itself will also be given its own separate rating. These agencies assess the ability of the debtor to honor his obligations and accordingly give him or her a [[credit rating]]. Moody's uses the letters ''Aaa Aa A Baa Ba B Caa Ca C'', where ratings ''Aa-Caa'' are qualified by numbers 1-3. S&P and other rating agencies have slightly different systems using capital letters and +/- qualifiers.

A change in ratings can strongly affect a company, since its cost of [[refinancing]] depends on its [[creditworthiness]]. Bonds below Baa/BBB (Moody's/S&P) are considered [[High-yield debt|junk]] or high-risk bonds. Their high risk of default (approximately 1.6 percent for Ba) is compensated by higher interest payments. Bad Debt is a loan that can not (partially or fully) be repaid by the debtor. The debtor is said to [[Default (finance)|default]] on his debt. These types of debt are frequently repackaged and sold below face value. Buying junk bonds is seen as a risky but potentially profitable investment.


== Debt markets ==

=== Market interest rates ===
{{Main|Bond valuation}}

=== Loans versus bonds ===
[[Bond (finance)|Bonds]] are debt [[security (finance)|securities]], tradeable on a [[bond market]]. A country's regulatory structure determines what qualifies as a security. For example, in North America, each [[security (finance)|security]] is uniquely identified by a [[CUSIP]] for trading and settlement purposes. In contrast, [[loan]]s are not [[security (finance)|securities]] and do not have [[CUSIP]]s (or the equivalent). Loans may be sold or acquired in certain circumstances, as when a bank [[syndicated loan|syndicates]] a loan.

Loans can be turned into securities through the [[securitization]] process.  In a securitization, a company sells a pool of assets to a securitization trust, and the securitization trust finances its purchase of the assets by selling [[Debt securities|securities]] to the market. For example, a trust may own a pool of home [[mortgage loan|mortgages]], and be financed by [[residential mortgage-backed security|residential mortgage-backed securities]].  In this case, the asset-backed trust is a debt issuer of [[residential mortgage-backed security|residential mortgage-backed securities]].

=== Role of central banks ===
[[Central bank]]s, such as the U.S. [[Federal Reserve System]], play a key role in the debt markets. Debt is normally denominated in a particular [[currency]], and so changes in the valuation of that currency can change the effective size of the debt. This can happen due to [[inflation]] or [[deflation]], so it can happen even though the borrower and the lender are using the same [[currency]].

== Criticisms ==
{{Main|Criticism of debt}}

Some argue against debt as an instrument and institution, on a personal, family, social, corporate and governmental level. [[Islamic banking|Islam forbids lending with interest]] even today. In hard times, the cost of servicing debt can grow beyond the debtor's ability to pay, due to either external events (income loss) or internal difficulties (poor management of resources).

Debt will increase through time if it is not repaid faster than it grows through interest. This effect may be termed [[usury]], while the term "usury" in other contexts refers only to an excessive rate of interest, in excess of a reasonable profit for the [[risk]] accepted.

In international legal thought, [[odious debt]] is debt that is incurred by a regime for purposes that do not serve the interest of the state. Such debts are thus considered by this doctrine to be personal debts of the regime that incurred them and not debts of the state.  International [[Third World debt]] has reached the scale that many [[economist]]s are convinced that [[debt relief]] or [[debt cancellation]] is the only way to restore global equity in relations with the [[developing nation]]s.{{Citation needed|date=January 2012}}

Excessive debt accumulation has been blamed for exacerbating economic problems. For example, before the [[Great Depression]], the [[debt-to-GDP ratio]] was very high. Economic agents were heavily indebted. This excess of debt, equivalent to excessive expectations on future returns, accompanied asset bubbles on the stock markets. When expectations corrected, deflation and a [[credit crunch]] followed. [[Deflation]] effectively made debt more expensive and, as Fisher explained, this reinforced deflation again, because, in order to reduce their debt level, economic agents reduced their [[Consumption (economics)|consumption]] and investment. The reduction in demand reduced business activity and caused further unemployment. In a more direct sense, more [[bankruptcy|bankruptcies]] also occurred due both to increased debt cost caused by deflation and the reduced demand.

At the household level, debts can also have detrimental effects. In particular when households make spending decisions assuming income to increase, or remain stable, for the years to come. When households take on credit based on this assumption, life events can easily change indebtedness into over-indebtedness. Such life events include unexpected unemployment, relationship break-up, leaving the parental home, [[business failure]], illness, or home repairs. Over-indebtedness has severe social consequences, such as financial hardship, poor physical and mental health,<ref>{{cite journal | title=The relationship between debt and mental health: a systematic review. | author=Fitch | journal=Mental Health Review Journal | year=2011 | volume=16 | issue=4 | pages=153–166 | doi=10.1108/13619321111202313|display-authors=etal}}</ref> family stress, stigma, difficulty obtaining employment, exclusion from basic financial services ([[European Commission]], 2009), work accidents and industrial disease, a strain on social relations (Carpentier and Van den Bosch, 2008), absenteeism at work and lack of organisational commitment (Kim ''et al.'', 2003), feeling of insecurity, and relational tensions.<ref>{{cite web | url=http://eurofound.europa.eu/sites/default/files/ef_files/pubdocs/2010/70/en/2/EF1070EN.pdf | title=Managing household debts: Social service provision in the EU. Working paper. Dublin: European Foundation for the Improvement of Living and Working Conditions | publisher=European Foundation for the Improvement of Living and Working Conditions | date=2010 | accessdate=20 February 2015 | author=Dubois, Hans | author2=Anderson, Robert}}</ref>

== Levels and flows ==
{{Main|Debt levels and flows}}

Global debt [[underwriting]] grew 4.3 percent year-over-year to {{US$|5.19{{nbsp}}trillion|link=yes}} during 2004. It is expected to rise in the coming years if the spending habits of millions of people worldwide continue the way they do.

== History ==
{{Main|History of money}}

Traditions in some cultures demand that debt be forgiven on a regular (often annual) basis, in order to prevent systemic inequities between groups in society, or anyone becoming a specialist in holding debt and coercing repayment. An example is the Biblical [[Jubilee (Biblical)|Jubilee year]], described in the [[Book of Leviticus]].

=== Etymology ===
The word debt is based on the [[Latin]] word ''debitum'' (meaning: something owed/thing owed) whose [[Participle|past participle]] ''debere'' means owe/to owe. In [[Old French]] this Latin word was changed to ''dete'' and in [[Middle English]] to ''dette'' with its modern-day [[French language|French]] and [[English language|English]] spelling being debt.

== See also ==
{{Portal|Economics}}
* [[Debt theory of money]]

== References ==
{{Reflist|30em}}

{{Debt}}
{{authority control}}

[[Category:Debt|*]]
[[Category:Credit]]
[[Category:Personal financial problems]]