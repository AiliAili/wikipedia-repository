{{Infobox company
| name             = RSM UK
| logo             = RSM_Logo_ggb.jpg
| type             = Accounting firm
| foundation       =
| location         =  [[London]], [[United Kingdom]]
| industry         = [[Accounting]]
| products         = [[Financial audit|Audit]] and assurance<br />[[Business consulting]]<br />[[Outsourcing]]<br />[[Restructuring]]<br />Risk advisory<br />[[Tax]]
| num_employees    = 
| revenue          = 
| homepage         = {{url|http://www.rsmuk.com}}
}}

'''RSM UK''', is a partnership of chartered accountants in the United Kingdom. It is currently the seventh largest accountancy and business advisory firm in the United Kingdom, with around 3,500 people, 330 partners, thirty five locations and fees of £300 million.<ref>{{cite web|url=http://www.accountancyage.com/resources/top50 |title=Top 50 plus 50 accounting firms |publisher=Accountancy Age |date= |accessdate=2014-07-28}}</ref> 

The firm offers audit and assurance, risk advisory, outsourcing, tax, consulting, restructuring, transactions and legal services. RSM work with a wide range of public, private and not for profit companies. The current management team includes the Chairman Martin Rodgers, Managing Director Laurence Longe and Chief Operating Officer David Gwilliam.<ref>http://rsmuk.com/who-we-are/meet-our-management-team</ref>

==History==
The firm traces its origins back to 1865, to the firm of Walter Howard.  Historical name changes and mergers with many different firms have brought the partnership to where it is today. The name Baker Tilly was created in 1988, through the merger between Howard Tilly and [[Reginald Baker (film producer)#Notes|Baker Rooke]].<ref>{{cite web|url=http://www.bakertilly.co.uk/about/history.aspx |title=Our history |publisher=Baker Tilly |date= |accessdate=2014-07-28}}</ref>

The firm has continued to grow in size and strength through a number of subsequent mergers, one of the most notable being its purchase of the trading subsidiaries of [[RSM Tenon]] Group in September 2013.<ref>{{cite web|url=http://www.bakertilly.co.uk/media/news/agreement-to-acquire-the-trading-operations-of-RSM-Tenon-Group-plc.aspx |title=UK Holdings reaches agreement to acquire the trading operations of RSM Tenon Group plc. |publisher=Baker Tilly |date=2013-08-22|accessdate=2014-07-28}}</ref> In April 2014, following the purchase of RSM Tenon, Baker Tilly became the member firm of [[RSM International]], having served notice on [[Baker Tilly International]],<ref>{{cite web|url=http://www.bakertilly.co.uk/media/news/Baker-Tilly-in-the-UK-to-join-RSM-International-Network.aspx|title=Top 50 plus 50 accounting firms|publisher=Baker Tilly|date=2014-04-15|accessdate=2014-12-02}}</ref> the international network to which it previously belonged.

The firm changed its name on 26 October 2015, and adopted RSM as a common brand name, to unite under a single global brand with more than one hundred other firms within the network RSM International.<ref>http://economia.icaew.com/news/october-2015/rsm-launches-global-rebrand</ref>

==Awards==
The firm has won a number of awards in recent years, most particularly relating to its work with companies listed on the [[Alternative Investment Market]] (AIM).<ref>{{cite web|title=Awards|url=http://www.bakertilly.co.uk/about/awards.aspx|publisher=Baker Tilly|accessdate=24 March 2015}}</ref>

;UK Pensions Awards

*UK Pension Scheme ''Accountant of the Year'' – Winner 2005, 2008
 
;The FDs' Excellence Awards

*Auditor of the Year (Outside the Big 4) – Winner 2006, Highly Commended 2008<ref>{{cite web|title=Real FD/CBI FDs’ Excellence Awards: the scores|url=http://realbusiness.co.uk/article/778-real_fdcbi_fdsandrsquo_excellence_awards_the_scores|accessdate=24 March 2015|date=16 April 2008}}</ref>

;Growth Company Awards

* AIM Accountant of the Year – Winner 2003,<ref>{{cite web|title=Top 50 Movers and Shakers|url=http://www.growthcompany.co.uk/features/5880/top-50-movers-and-shakers-uncovered.thtml|publisher=Growth Company Investor|accessdate=24 March 2015|date=1 December 2003}}</ref> 2004, 2005, 2006, 2011 (joint)<ref>{{cite web|title=On a winning streak|url=http://www.growthcompany.co.uk/features/1630523/on-a-winning-streak.thtml|publisher=Growth Company Investor|accessdate=24 March 2015|date=13 June 2011}}</ref>

==Sponsorships==
In January 2016, RSM announced their sponsorship of European Tour player [[Andy Sullivan (golfer)|Andy Sullivan]].

==References==
{{Reflist}}

==External links==
*{{official website|http://www.rsmuk.com}}
*

[[Category:RSM]]
[[Category:Baker Tilly]]
[[Category:Accounting firms of the United Kingdom]]
[[Category:1988 establishments in the United Kingdom]]
[[Category:Companies established in 1988]]