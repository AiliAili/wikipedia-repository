{{featured article}}

{{Infobox Bach composition
| title                 = {{lang|de|Komm, du süße Todesstunde}}
| bwv                   = 161
| image                 = File:BWV161-P124-staatsbibliothek-berlin.jpeg
| image_upright         = 1.2
| alt                   = 
| caption               = Manuscript of BWV 161 with subheading and dynamical markings added by Bach
| type                  = [[Church cantata (Bach)|Church cantata]]
| occasion              = {{plainlist|
* 16th Sunday after [[Trinity Sunday|Trinity]]
* [[Purification of the Virgin|Purification]]
}}
| performed             = {{Timeline-event|date={{Start date|1716|09|27|df=y}}|location=[[Weimar]]}}
| movements             = 6
| text_poet             = [[Salomon Franck]]
| chorale               = {{based on|"{{lang|de|[[Herzlich tut mich verlangen]]}}"|[[Christoph Knoll]]}}
| vocal                 = {{plainlist|
*[[alto]] and [[tenor]] soloists
*[[SATB|{{abbr|SATB|soprano, alto, tenor and bass}}]] choir
}}
| instrumental          = {{hlist | 2 recorders | 2 violins | viola | continuo }}
}}
'''''{{lang|de|Komm, du süße Todesstunde}}''''' (Come, you sweet hour of death),{{sfn|Dürr|2006|p=542}} '''{{nowrap|BWV 161}}''',{{efn|"BWV" is [[Bach-Werke-Verzeichnis]], a thematic catalogue of Bach's works.{{sfn|Jones|2007|p=xiii}}}} is a [[Bach cantata|church cantata]] composed by [[Johann Sebastian Bach]] in [[Weimar]] for the 16th Sunday after [[Trinity Sunday|Trinity]], probably first performed on 27 September 1716.

Bach had taken up regular cantata composition two years before when he was promoted to concertmaster at the Weimar court, writing one cantata per month to be performed in the ''{{lang|de|Schlosskirche}}'', the court chapel in the ducal [[Schloss Weimar|''Schloss'']]. The text of ''{{lang|de|Komm, du süße Todesstunde}}'', and of most other cantatas written in Weimar, was provided by court poet [[Salomon Franck]]. He based it on the prescribed gospel reading about the [[young man from Nain]]. His text reflects on longing for death, seen as a transition to a life united with Jesus. The text includes as a closing [[chorale]] the fourth stanza of the [[hymn]] "{{lang|de|[[Herzlich tut mich verlangen]]}}" by [[Christoph Knoll]].

The cantata in six movements opens with a sequence of alternating [[aria]]s and [[recitative]]s leading to a chorus and a concluding chorale. The chorale tune, known as "{{lang|de|[[O Sacred Head, Now Wounded|O Haupt voll Blut und Wunden]]}}", appears in the first movement, played by the organ, and musical [[Motif (music)|motif]]s of the arias are derived from it, providing an overall formal unity to the composition. Bach scored the work for two vocal parts ([[alto]] and [[tenor]]), a [[SATB|four-part choir]], and a [[Baroque instruments|Baroque chamber ensemble]] of recorders, strings and continuo. In the alto recitative (movement 4), accompanied by all instruments, Bach creates the images of sleep, of waking up, and of funeral bells, the latter in the recorders and [[pizzicato]] of the strings.

While the [[libretto]] was published in a collection in 1715, Bach probably did not perform it until 27 September 1716, due to a period of public mourning of six months in the [[Saxe-Weimar|Duchy of Weimar]] from August 1715. Bach revived the cantata when he was ''[[Thomaskantor]]'' in Leipzig, but not for [[Bach cantata#Leipzig|his cantata cycles]], which included three new works for the 16th Sunday after Trinity. He performed ''{{lang|de|Komm, du süße Todesstunde}}'' with minor changes between 1737 and 1746. He also assigned it to the occasion of [[Purification of the Virgin|Purification]], a feast with a similar topic.
{{TOC limit|3}}

== Background ==
{{for|details on Bach's promotion|Erschallet, ihr Lieder, erklinget, ihr Saiten! BWV 172#Background}}
{{for|the series of monthly cantatas|O heilges Geist- und Wasserbad, BWV 165 #Monthly cantatas from 1714 to 1715}}

[[File:Schlosskirche Weimar 1660.jpg|thumb|upright|alt=The interior of the church Schloßkirche is painted, viewed along the nave towards the altar, showing two balconies and the organ on a third level above the altar|The [[Schlosskirche, Weimar|''Schlosskirche'' in Weimar]]]]
Born in 1685, Bach established his reputation as an outstanding organist while in his teens. He moved to Weimar in 1708 to take up a position as court organist to the co-reigning dukes [[William Ernest, Duke of Saxe-Weimar|Wilhelm Ernst]] and [[Ernest Augustus I, Duke of Saxe-Weimar-Eisenach|Ernst August]] of [[Saxe-Weimar]]. He had already begun to compose cantatas at his previous posts at [[Arnstadt]] and [[Mühlhausen]], and his reasons for moving included disappointment with the standard of singing at the churches where he had worked. He was appointed concertmaster of the Weimar court capelle on 2 March 1714. In that position, he assumed principal responsibility for composing new works. Specifically, he was tasked with providing cantatas for the ''Schlosskirche'' (palace church) on a monthly schedule, which would result in [[Church cantata (Bach)|a complete annual cycle]] for the [[liturgical year]] within four years. While Bach had composed vocal music only for special occasions until his promotion, the chance to regularly compose and perform a new work resulted in a program into which Bach "threw himself wholeheartedly", as the Bach scholar [[Christoph Wolff]] notes.{{sfn|Wolff|2002|p=156}}

=== Cantatas of 1716 ===
The cantatas which were probably first performed in 1716 used texts by the Weimar court poet [[Salomon Franck]], published in his collections ''{{lang|de|Evangelisches Andachts-Opffer}}'' (1715) and ''{{lang|de|Evangelische Sonn- und Festtages-Andachten}}'' (1717).{{sfn|Wolff|2002|p=162}} Fewer cantatas have survived from this period than from the years before; possibly some were lost, and possibly some proposed cantatas were never written, reflecting Bach's loss of interest.{{sfn|Wolff|2002|p=167}}

From the start of the liturgical year on the first Sunday in Advent, Bach composed prolifically. He wrote works for three consecutive Sundays in Advent, prompted probably by the death of the Kapellmeister [[Johann Samuel Drese]] on 1 December 1716.{{sfn|Wolff|2002|p=162}} When Bach's hope to become Drese's successor was not realized, he ceased to compose cantatas for the Weimar court.{{sfn|Dürr|2006|p=15}}

These works were performed by Bach as concertmaster in 1716, according to Wolff and [[Alfred Dürr]], an authority on Bach's cantatas:{{sfn|Wolff|2002|p=162}}{{sfn|Dürr|2006|p=14}}

{| class="wikitable sortable plainrowheaders"
|-
|+ Bach's cantatas in 1716
|-
! scope="col" | Date
! scope="col" | Occasion
! scope="col" | BWV
! scope="col" | Incipit
! scope="col" | Text source
|-
| scope="row" style="text-align: right;" | {{sortdate|19 Jan 1716}} || {{hs|N04}} 2nd Sunday after [[Epiphany (holiday)|Epiphany]] || scope="row" style="text-align: right;" | 155 || ''{{lang|de|[[Mein Gott, wie lang, ach lange? BWV 155|Mein Gott, wie lang, ach lange]]}}'' || Franck 1715
|-
| scope="row" style="text-align: right;" | {{sortdate|27 Sep 1716}} || {{hs|N05}} 16th Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 161 || ''{{lang|de|Komm, du süße Todesstunde}}'' || Franck 1715
|-
| scope="row" style="text-align: right;" | {{sortdate|25 Oct 1716}} || {{hs|N06}} 20th Sunday after Trinity || scope="row" style="text-align: right;" | 162 || ''{{lang|de|[[Ach! ich sehe, itzt, da ich zur Hochzeit gehe, BWV 162|Ach! ich sehe, itzt, da ich zur Hochzeit gehe]]}}'' || Franck 1715
|-
| scope="row" style="text-align: right;" | {{sortdate|6 Dec 1716}} || {{hs|N01}} 2nd Sunday in [[Advent]] || scope="row" style="text-align: right;" | {{hs|070}}70a || ''{{lang|de|[[Wachet! betet! betet! wachet! BWV 70|Wachet! betet! betet! wachet!]]}}'' || Franck 1717
|-
| scope="row" style="text-align: right;" | {{sortdate|13 Dec 1716}} || {{hs|N02}} 3rd Sunday in Advent || scope="row" style="text-align: right;" | {{hs|186}} 186a || ''{{lang|de|[[Ärgre dich, o Seele, nicht, BWV 186|Ärgre dich, o Seele, nicht]]}}'' || Franck 1717
|-
| scope="row" style="text-align: right;" | {{sortdate|20 Dec 1716}}? || {{hs|N03}} 4th Sunday in Advent || scope="row" style="text-align: right;" | {{hs|147}} 147a || ''{{lang|de|[[Herz und Mund und Tat und Leben, BWV 147a|Herz und Mund und Tat und Leben]]}}'' || Franck 1717
|-
|}

== Readings and text ==

Bach wrote ''{{lang|de|Komm, du süße Todesstunde}}'' for the [[Church cantata (Bach)#Trinity XVI|16th Sunday after Trinity]]. The prescribed readings for that Sunday were from the [[Epistle to the Ephesians]], about the strengthening of faith in the congregation of [[Ephesus]] ({{Sourcetext|source=Bible|version=King James|book=Ephesians|chapter=3|verse=13|range=–21}}), and from the [[Gospel of Luke]], about the raising from the dead of the [[young man from Nain]] ({{Sourcetext|source=Bible|version=King James|book=Luke|chapter=7|verse=11|range=–17}}).{{sfn|Dürr|2006|p=545}} In Bach's time the story pointed at the resurrection of the dead, expressed in words of desire to die soon.{{sfn|Isoyama|1997|p=5}}{{sfn|Gardiner|2006|p=3}}

Franck's text was published in ''{{lang|de|Evangelisches Andachts-Opffer}}'' in 1715. He included as the closing chorale the fourth stanza of the hymn "{{lang|de|Herzlich tut mich verlangen}}" (1611) by [[Christoph Knoll]].{{sfn|Dürr|1971|p=448}} Franck wrote a libretto full of biblical references, including (in the first movement) "feeding on honey from the lion's mouth", based on {{Sourcetext|source=Bible|version=King James|book=Judges|chapter=14|verse=5|range=–9}}.{{sfn|Dürr|2006|p=544}} Dürr summarizes that Franck wrote "a deeply felt, personal confession of longing for Jesus".{{sfn|Dürr|2006|p=544}} The Bach scholar [[Richard D. P. Jones]] notes that the cantata is "one of the most richly inspired of all Bach's Weimar cantatas", and sees the text as a part of the inspiration, with its "mystical longing for union with Christ".{{sfn|Jones|2007|p=281}}

== Performances ==
[[File:Young Bach2.jpg|thumb|upright|alt=disputed portrait of the young Bach, with brown curled hair, dressed festively|Portrait of the young Bach (disputed)<ref>{{cite web|last=Towe|first=Teri Noel|title=The Portrait in Erfurt Alleged to Depict Bach, the Weimar Concertmeister|work=The Face Of Bach|url=http://www.npj.com/thefaceofbach/09w624.html|archiveurl=https://web.archive.org/web/20110716074347/http://www.npj.com/thefaceofbach/09w624.html|archivedate=14 July 2011|accessdate=28 April 2014}}</ref>]]
Bach led the first performance, but its date has been debated. Dürr concluded initially (in the first edition of his book ''Die Kantaten von Johann Sebastian Bach'' of 1971) that the cantata was first performed on 6 October 1715,{{sfn|Dürr|1971|p=448}} but this date fell in a period of public mourning in Weimar. In August 1715 the brother of Duke Ernst August died, only 18 years old, and the duke proclaimed six months of mourning in the [[Saxe-Weimar|Duchy of Weimar]]. Cantata performances were resumed sooner, with the 21st Sunday after Trinity on 10 November 1715.{{sfn|Wolff|2002|p=177}} Now, the first performance of the work is generally accepted as the same occasion the following year, when the 16th Sunday after Trinity fell on 27 September 1716,{{sfn|Bach digital 1|2016}} by Wolff, the publisher [[Carus-Verlag]],{{sfn|Carus|2010}} and Dürr in the revised and translated edition of 2006.{{sfn|Dürr|2006|p=544}} Richard D. P. Jones notes in his book ''The Creative Development of Johann Sebastian Bach'' that "technical novelties" also suggest that the cantata was composed in 1716, according to a recent study.{{sfn|Jones|2007|p=281}}

In 1723, his first year as ''[[Thomaskantor]]'' in Leipzig, Bach composed a new cantata for the 16th Sunday after Trinity, {{lang|de|[[Christus, der ist mein Leben, BWV 95|''Christus, der ist mein Leben'', BWV&nbsp;95]]}}. A year later he wrote a [[Chorale cantata (Bach)|chorale cantata]] for his [[Chorale cantata cycle|second cantata cycle]], {{lang|de|[[Liebster Gott, wenn werd ich sterben? BWV 8|''Liebster Gott, wenn werd ich sterben?'' BWV&nbsp;8]]}}, and for his third cantata cycle there he composed {{lang|de|[[Wer weiß, wie nahe mir mein Ende? BWV 27|''Wer weiß, wie nahe mir mein Ende?'' BWV&nbsp;27]]}}. He revived ''{{lang|de|Komm, du süße Todesstunde}}'' in Leipzig, but only later, in a version dated sometime between 1737 and 1746, with minor changes to the scoring.{{sfn|Bach digital 2|2016}} He even performed it for a different liturgical occasion, the feast of the [[Church cantata (Bach)#Purification|Purification of Mary]] on 2 February.{{sfn|Bischof|2010}} The prescribed readings for the Purification included [[Simeon (Gospel of Luke)|Simeon]]'s [[canticle]] ''[[Nunc dimittis]]'' ({{Sourcetext|source=Bible|version=King James|book=Luke|chapter=2|verse=22|range=–32}}), which with its line "now lettest thou thy servant depart in peace" has a similar theme.{{sfn|Wolff|1995|p=23}}

== Music ==

=== Structure and scoring ===

The cantata is structured in six movements: a series of alternating [[aria]]s and [[recitative]]s leads to a chorus and a concluding chorale.{{sfn|Wolff|1995|p=23}} As with several other cantatas based on words by Franck, it is scored for a small ensemble: [[alto]] soloist (A), [[tenor]] soloist (T), a [[SATB|four-part choir]] and a [[Baroque instruments|Baroque chamber ensemble]] of two [[recorder (musical instrument)|recorders]] (Fl), two [[violin]]s (Vl), [[viola]] (Va), [[Organ (music)|organ]] (Org) and [[basso continuo]] (Bc).{{sfn|Bischof|2010}} The title page reads simply: "Auf den sechzenden Sontag nach Trintatis" (For the sixteenth Sunday after Trinity). The duration is given as 19 minutes.{{sfn|Dürr|2006|p=542}}

One structural element is the anticipation of the closing chorale in the first movement, where the chorale melody is used as a [[cantus firmus]]. Bach also used this approach to unify the structure in two other Weimar cantatas, {{lang|de|[[Alles, was von Gott geboren, BWV 80a|''Alles, was von Gott geboren'', BWV 80a]]}}, and {{lang|de|[[Barmherziges Herze der ewigen Liebe, BWV 185|''Barmherziges Herze der ewigen Liebe'', BWV 185]]}}. He later used the juxtaposition of a chorale cantus firmus against vocal music on a grand scale in his ''[[St Matthew Passion]]'', in both [[St Matthew Passion structure#1|the opening chorus]] and [[St Matthew Passion structure#29|the movement concluding Part I]].{{sfn|Dürr|2006|pp=545–546}} The use of recorders in ''{{lang|de|Komm, du süße Todesstunde}}'' is reminiscent of the early cantata ''Actus tragicus'', {{lang|de|[[Gottes Zeit ist die allerbeste Zeit, BWV 106|''Gottes Zeit ist die allerbeste Zeit'', BWV 106]]}}.{{sfn|Dürr|2006|p=545}}

At the Leipzig performances of the cantata, the first verse of the chorale was probably sung by a [[soprano]], instead of using an instrumental rendition of the chorale tune in the first aria.{{sfn|Isoyama|1997|p=7}} The cantata was transposed from [[C major]] to [[E-flat major]] at Leipzig,{{sfn|Dürr|2006|p=542}} where the recorders may have been replaced by [[flauto traverso|transverse flutes]].{{sfn|Dürr|2006|p=545}}

In the following table of the movements, the scoring and [[Key (music)|keys]] are given for the version performed in Weimar in 1716. The keys and [[time signature]]s are taken from Dürr, using the C symbol for common time (4/4).{{sfn|Bischof|2010}}{{sfn|Dürr|2006|pp=542–544}} The instruments are shown separately for winds and strings, while the continuo, playing throughout, is not shown.

{{Classical movement header | show_text_source = yes | work = ''Komm, du süße Todesstunde'', BWV 161 | instruments1 = Winds | instruments2 = Strings | instruments3 = Other }}
{{Classical movement row
| number = [[#1|1]]
| title = ''{{lang|de|Komm, du süße Todesstunde}}''
| text_source = Franck
| type = Aria
| vocal = A
| instruments1 = 2Fl
| instruments2 = 
| instruments3 = Org
| key = {{nowrap|[[C major]]}}
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#2|2]]
| title = ''{{lang|de|Welt, deine Lust ist Last}}''
| text_source = Franck
| type = Recitative
| vocal = T
| instruments1 = 
| instruments2 = 
| instruments3 = 
| key = 
| time = 3/4
}}
{{Classical movement row
| number = [[#3|3]]
| title = ''{{lang|de|Mein Verlangen ist, den Heiland zu umfangen}}''
| text_source = Franck
| type = Aria
| vocal = T
| instruments1 = 
| instruments2 = 2Vl Va
| instruments3 = 
| key = {{nowrap|[[A minor]]}}
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#4|4]]
| title = ''{{lang|de|Der Schluß ist schon gemacht}}''
| text_source = Franck
| type = Recitative
| vocal = A
| instruments1 = 2Fl
| instruments2 = 2Vl Va
| instruments3 = 
| key = 
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#5|5]]
| title = ''{{lang|de|Wenn es meines Gottes Wille}}''
| text_source = Franck
| type = Chorus
| vocal = SATB
| instruments1 = 2Fl
| instruments2 = 2Vl Va
| instruments3 = 
| key = [[C major]]
| time = 3/8
}}
{{Classical movement row
| number = [[#6|6]]
| title = ''{{lang|de|Der Leib zwar in der Erden}}''
| text_source = Knoll
| type = Chorale
| vocal = SATB
| instruments1 = 2Fl ([[Obbligato|obbl.]])
| instruments2 = 2Vl Va
| instruments3 = 
| key = A minor
| time = {{music|common-time}}
}}

{{End}}

=== Movements ===

A [[Phrygian mode|Phrygian]] chorale melody, well known as the melody of "{{lang|de|[[O Sacred Head, Now Wounded|O Haupt voll Blut und Wunden]]}}", provides the musical theme of the cantata, appearing in movement one in both its original form and the alto line derived from it. The themes of the two other [[aria]]s are taken from the same melody, providing formal unity.{{sfn|Wolff|1995|p=23}} The same melody appears five times in chorales of Bach's ''St Matthew Passion''.{{sfn|Isoyama|1997|p=7}}

==== 1 ====
The opening aria for alto, "{{lang|de|Komm, du süße Todesstunde}}" ("Come, o sweet hour of death"{{sfn|Dellal|2012}} or "Come, thou sweet hour of parting"{{sfn|Breitkopf|2016}}) is accompanied by the recorders. They move in the [[ritornello]] in parallel thirds and sixths.{{sfn|Jones|2007|p=281}} The organ serves not only as a bass instrument but supplies the chorale melody.{{sfn|Isoyama|1997|p=7}} In Weimar, Bach seems to have expected the congregation to know the words of the first stanza of Knoll's hymn.
{{columns|width=auto
| col1 = <poem>
  Herzlich tut mich verlangen
  Nach einem selgen End,
  Weil ich hie bin umfangen
  Mit Trübsal und Elend.
  Ich hab Lust abzuscheiden
  Von dieser bösen Welt,
  Sehn mich nach himml'schen Freuden,
  O Jesu, komm nur bald!</poem>
| col2 = <poem>
  I yearn from my heart
  for a peaceful end,
  since here I am surrounded
  by sorrow and wretchedness.
  I wish to depart
  from this evil world,
  I long for heavenly joys,
  O Jesus, come quickly!{{sfn|Dellal|2012}}</poem>
}}
Jones points out that the cantus firmus of the organ seems "objective",{{sfn|Jones|2007|p=281}} in contrast to the subjective "display of personal feeling"{{sfn|Jones|2007|p=281}} of the voice and the complexity of the other parts.{{sfn|Jones|2007|p=281}}

In a later performance in Leipzig, a soprano sang the stanza with the organ.{{sfn|Isoyama|1997|p=7}}

==== 2 ====
The tenor recitative, "{{lang|de|Welt, deine Lust ist Last}}" (World, your pleasure is a burden),{{sfn|Dellal|2012}} begins as a [[Recitative#Secco|secco recitative]], but ends in an [[arioso]] as the words paraphrase a biblical verse from {{Sourcetext|source=Bible|version=King James|book=Philippians|chapter=1|verse=23}}, "{{lang|de|Ich habe Lust abzuscheiden und bei Christo zu sein}}" to "Ich habe Lust, bei Christo bald zu weiden. Ich habe Lust, von dieser Welt zu scheiden" (I desire to pasture soon with Christ. I desire to depart from this world).{{sfn|Dellal|2012}} Dürr notes that the development from secco to arioso is frequent in Bach's early cantatas, and is here especially motivated to highlight the biblical paraphrase.{{sfn|Dürr|2006|p=546}}

==== 3 ====
The aria for tenor, "{{lang|de|Mein Verlangen ist, den Heiland zu umfangen}}" (My longing
is, to embrace my Savior),{{sfn|Dellal|2012}} is the first movement with the strings, adding depth to the emotional expression.{{sfn|Isoyama|1997|p=7}} It returns to the hope for union with Jesus of the first movement, expressed in an agitated way, with [[Syncope (music)|syncopies]] for "longing" and flowing [[Motif (music)|motif]]s for "embracing". The middle section is mostly accompanied by the continuo only, but at times interjected by the strings playing the "longing"-motifs.{{sfn|Dürr|2006|p=546}}

==== 4 ====
The alto recitative, "{{lang|de|Der Schluß ist schon gemacht}}" (The end has already come),{{sfn|Dellal|2012}} is accompanied by all instruments, creating images of sleep (in a downward movement, ending in long notes), awakening (in fast movement upwards), and funeral bells in the recorders and [[pizzicato]] of the strings.{{sfn|Gardiner|2006|pp=2–3}} The musicologist Tadashi Isoyama notes: "In this movement the anticipation of death appears to be fulfilled, and the alto's declamation, welcoming death and the ringing of the funeral bells, is filled with a pathos amounting almost to obsession."{{sfn|Isoyama|1997|p=7}}

==== 5 ====
The first choral movement 5,"{{lang|de|Wenn es meines Gottes Wille}}" (If it is my God's will),{{sfn|Dellal|2012}} is marked ''aria'' by Franck. Bach set it for four parts, using song-like [[homophony]]. Wolff compares the style to Thuringian [[motet]]s of around 1700.{{sfn|Wolff|1995|p=23}} The first part is not repeated [[da capo]], in keeping with the last words "{{lang|de|Dieses sei mein letztes Wort}}" (May this be my last word).{{sfn|Dellal|2012}} While a textual da capo is impossible, Bach composed a musical da capo, giving the movement a structure of ABB'A'.{{sfn|Dürr|2006|p=546}} Dürr notes that "Arnold Schering has drawn attention to the increasing rapture".{{sfn|Dürr|2006|p=546}}

==== 6 ====
The closing chorale, "{{lang|de|Der Leib zwar in der Erden}}" (The body, indeed, in the earth),{{sfn|Dellal|2012}} is illuminated by a fifth part of the two recorders playing a lively counterpoint in [[unison]].{{sfn|Dürr|2006|pp=545–546}}
{{columns|width=auto
| col1 = <poem>
  Der Leib zwar in der Erden
  Von Würmen wird verzehrt,
  Doch auferweckt soll werden,
  Durch Christum schön verklärt,
  Wird leuchten als die Sonne
  Und leben ohne Not
  In himml'scher Freud und Wonne.
  Was schadt mir denn der Tod?</poem>
| col2 = <poem>
  The body, indeed, in the earth
  will be consumed by worms,
  yet it shall be resurrected,
  beautifully transfigured through Christ,
  it will shine like the sun
  and live without grief
  in heavenly joy and delight.
  What harm can death do me then?{{sfn|Dellal|2012}}</poem>
}}

The "soaring descant" of the recorders has been interpreted as "creating the image of the flesh transfigured".{{sfn|Isoyama|1997|p=5}}

=== Summary ===
Wolff summarizes: "Cantata 161 is one of the most delicate and jewel-like products of Bach's years in Weimar. The writing in up to ten parts is extraordinarily subtle. ... The recorders additionally contribute in no small way to the spiritualised emotion and positive feelings associated with the 'sweet hour of death{{'"}}.{{sfn|Wolff|1995|p=23}} Jones writes: "Bach's arrival at full maturity by about the middle of his Weimar period (1713–17) is attested by the stylistic and technical assurance, and the consistently high standard, of his writing at that time."{{sfn|Jones|2007|p=309}} He counts the cantata as one of several that reached a level of mastery unsurpassed in later years, along with the [[Orgelbüchlein]] and the cantata [[Ich hatte viel Bekümmernis, BWV 21|''Ich hatte viel Bekümmernis'', BWV 21]], among others.{{sfn|Jones|2007|p=309}}

== Publication ==
The cantata was edited by [[Franz Wüllner]] for the [[Bach Gesellschaft Ausgabe]], the first complete edition of Bach's works, in a volume published in 1887. The [[New Bach Edition]] (Neue Bach-Ausgabe, NBA) published the score of both the Weimar and the Leipzig version in 1982, edited by {{ill|Helmuth Osthoff|de}}, with the critical commentary following in 1984.{{sfn|Bach digital 1|2016}}{{sfn|Bach digital 2|2016}}

== Later performances ==
[[John Eliot Gardiner]] performed the cantata twice in the Bach year 2000 as part of the [[Bach Cantata Pilgrimage]] with the Monteverdi Choir. One performance was on the anniversary of Bach's death, 28 July, at [[Iona Abbey]], and the other on the 16th Sunday after Trinity (8 October) at the church of the [[Convent of San Domingos de Bonaval]] in [[Santiago de Compostela]].{{sfn|Gardiner|2006|p=3}}

== Selected recordings ==
The table entries are excerpted from the selection on the Bach-Cantatas website.{{sfn|Oron|2015}} Choirs with one voice per part ([[OVPP]]) and ensembles playing on period instruments in [[historically informed performance]]s are marked by green background.

{| class="wikitable sortable plainrowheaders"
|-
|+ Recordings of ''Komm, du süße Todesstunde'', BWV 161
|-
! scope="col" | Title
! scope="col" class="unsortable" | Conductor / Choir / Orchestra
! scope="col" class="unsortable" | Soloists
! scope="col" | Label
! scope="col" | Year
! scope="col" | Choir type
! scope="col" | Orch. type
|-

{{Cantata discography row
| id = Prohaska
| title = ''J. S. Bach: Cantatas BWV 52 & BWV 161''
| conductor = {{sortname|Felix|Prohaska}}
| choir = Choir of the Bach Guild
| orchestra = Orchestra of the Bach Guild
| soloists = {{plainlist|
* [[Hilde Rössel-Majdan]]
* [[Waldemar Kmentt]]
}}
| label = Bach Guild
| year = {{Start date|1952}}
| choir_type = 
| orchestra_type = 
}}
{{Cantata discography row
| id = Göttsche
| title = ''J. S. Bach: Cantatas BWV 52 & BWV 161''
| conductor = {{hs|Göttsche, Heinz Markus}} {{nowrap|[[Heinz Markus Göttsche]]}}
| choir = Mannheim Bach Choir
| orchestra = Heidelberger Kammerorchester
| soloists = {{plainlist|
* [[Sabine Kirchner]]
* [[Theophil Maier]]
}}
| label = Oryx Recordings
| year = {{Start date|1964}}
| choir_type = 
| orchestra_type = 
}}
{{Cantata discography row
| id = Schröder
| title = ''J. S. Bach: {{lang|de|Kantaten · Cantatas Nr. 89, Nr. 90, Nr. 161}}''
| conductor = {{sortname|Jaap|Schröder}}
| choir = [[Monteverdi-Chor]]
| orchestra = [[Concerto Amsterdam]]
| soloists = {{plainlist|
* [[Helen Watts]]
* [[Kurt Equiluz]]
}}
| label = [[Telefunken]]
| year = {{Start date|1965}}
| choir_type = 
| orchestra_type = 
}}
{{Cantata discography row
| id = Rilling
| title = ''{{lang|de|Die Bach Kantate Vol.}} 50''
| conductor = {{sortname|Helmuth|Rilling}}
| choir = [[Frankfurter Kantorei]]
| orchestra = [[Bach-Collegium Stuttgart]]
| soloists = {{plainlist|
* [[Hildegard Laurich]]
* [[Adalbert Kraus]]
}}
| label = [[Hänssler Classic|Hänssler]]
| year = {{Start date|1976}}
| choir_type = 
| orchestra_type = 
}}
{{Cantata discography row
| id = Leonhardt
| title = ''J. S. Bach: {{lang|de|[[Bach cantatas (Teldec)|Das Kantatenwerk]]}}&nbsp;– Sacred Cantatas Vol. 8''
| conductor = {{sortname|Gustav|Leonhardt}}
| choir = [[Tölzer Knabenchor]]
| orchestra = [[Concentus Musicus Wien]]
| soloists = {{plainlist|
* [[Paul Esswood]]
* [[Kurt Equiluz]]
}}
| label = [[Teldec]]
| year = {{Start date|1986}}
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Koopman
| title = ''J. S. Bach: Complete Cantatas Vol. 3''
| conductor = {{sortname|Ton|Koopman}}
| orchestra = [[Amsterdam Baroque Orchestra & Choir]]
| soloists = {{plainlist|
* {{nowrap|[[Elisabeth von Magnus]]}}
* [[Paul Agnew]]
}}
| label = [[Antoine Marchand]]
| year = {{Start date|1995}}
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Suzuki
| title = ''J. S. Bach: Cantatas Vol. 5 – BWV 18, 143, 152, 155, 161''
| conductor = {{sortname|Masaaki|Suzuki}}
| orchestra = [[Bach Collegium Japan]]
| soloists = {{plainlist|
* [[Yoshikazu Mera]]
* [[Makoto Sakurada]]
}}
| label = [[BIS Records|BIS]]
| year = {{Start date|1997}}
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Gardiner
| title = ''Bach Cantatas Vol. 28: Altenburg/Warwick''
| conductor = {{sortname|John Eliot|Gardiner}}
| choir = [[Monteverdi Choir]]
| orchestra = [[English Baroque Soloists]]
| soloists = {{plainlist|
* [[Robin Tyson]]
* [[Mark Padmore]]
}}
| label = [[Soli Deo Gloria (record label)|Soli Deo Gloria]]
| year = {{Start date|2000}}
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Pierlot
| title = ''J. S. Bach: Funeral Cantatas''
| conductor = {{sortname|Philippe|Pierlot}}
| choir = 
| orchestra = [[Ricercar Consort]]
| soloists = {{plainlist|
* [[Katharine Fuge]]
* [[Carlos Mena]]
* [[Julian Podger]]
* [[Stephan MacLeod]]
}}
| label = Rapidshare
| year = {{Start date|2009}}
| choir_type = OVPP
| orchestra_type = Period
}}
{{Cantata discography row
| id = Lutz
| title = ''{{lang|de|J. S. Bach: Kantate BWV 161 "Komm, du süsse Todesstunde"}}''
| conductor = {{sortname|Rudolf|Lutz}}
| choir = Chor der J. S. Bach-Stiftung
| orchestra = Orchester der J. S. Bach-Stiftung
| soloists = {{plainlist|
* [[Alex Potter (countertenor)|Alex Potter]]
* [[Daniel Johannsen]]
}}
| label = Gallus Media
| year = {{Start date|2009}}
| choir_type = 
| orchestra_type = Period
}}
|-
|}

== Notes ==
{{notelist}}

== References ==
{{reflist|30em}}

== Bibliography ==
''General''
* {{IMSLP2|work=Komm, du süsse Todesstunde, BWV 161 (Bach, Johann Sebastian)|cname=Komm, du süsse Todesstunde, BWV 161}}
* {{cite web
  | url = http://www.bach-digital.de/receive/BachDigitalWork_work_00000195?lang=en
  | title = Komm, du süße Todesstunde (1st version) BWV 161; BC A 135a / Sacred cantata (16th Sunday after Trinity)
  | publisher = Bach digital website, managed by [[Bach Archive]], [[Saxon State and University Library Dresden|SLUB]], [[Berlin State Library|SBB]] and [[Leipzig University]]
  | year = 2016
  | accessdate = 15 June 2016
  | ref = {{sfnRef|Bach digital 1|2016}}
  }}
* {{cite web
  | url = http://www.bach-digital.de/receive/BachDigitalWork_work_00000196?lang=en
  | title = Komm, du süße Todesstunde (2nd version) BWV 161; BC A 135b / Sacred cantata (16th Sunday after Trinity)
  | publisher = Bach digital website
  | year = 2016
  | accessdate = 15 June 2016
  | ref = {{sfnRef|Bach digital 2|2016}}
  }}

''Books''
* {{cite book
  | last = Dürr
  | first = Alfred
  | authorlink = Alfred Dürr
  | title = Die Kantaten von Johann Sebastian Bach
  | year = 1971
  | publisher = Deutscher Taschenbuchverlag
  | isbn = 978-3-423-04080-8
  | edition = 4
  | language = German
  | ref = harv
  }}
* {{cite book
  | last = Dürr
  | first = Alfred
  | authorlink = Alfred Dürr
  | others = Translated by [[Richard D. P. Jones]]
  | url = https://books.google.com/books?id=m9JuwslMcq4C&pg=PA542
  | title = The Cantatas of J. S. Bach: With Their Librettos in German-English Parallel Text
  | publisher = [[Oxford University Press]]
  | year = 2006
  | isbn = 978-0-19-929776-4
  | ref = harv
  }}
* {{cite book
  | last = Jones
  | first = Richard D. P.
  | authorlink = Richard D. P. Jones
  | url = https://books.google.com/books?id=-Pdssru1i8oC&pg=PA281
  | title = The Creative Development of Johann Sebastian Bach, Volume I: 1695–1717: Music to Delight the Spirit
  | publisher = Oxford University Press
  | year = 2007
  | isbn = 978-0-19-816440-1
  | ref = harv
  }}
* {{cite book
  | last = Wolff
  | first = Christoph
  | authorlink = Christoph Wolff
  | url = https://books.google.com/books?id=NHpL3cQg0_UC&pg=PA147
  | title = Johann Sebastian Bach: The Learned Musician
  | publisher = Oxford University Press
  | isbn = 978-0-393-32256-9
  | year = 2002
  | ref = harv
  }}

''Online sources''

The complete recordings of Bach's cantatas are accompanied by liner notes from musicians and musicologists; Gardiner commented on his [[Bach Cantata Pilgrimage]], Isoyama wrote for [[Masaaki Suzuki]], and Wolff for [[Ton Koopman]].
* {{cite web
  | last = Bischof
  | first = Walter F.
  | url = http://webdocs.cs.ualberta.ca/~wfb/cantatas/161.html
  | title = Komm, du süße Todesstunde
  | year = 2010
  | publisher = [[University of Alberta]]
  | accessdate = 15 September 2010
  | ref = harv
  }}
* {{cite web
  | last = Dellal
  | first = Pamela
  | authorlink = Pamela Dellal
  | url = http://www.emmanuelmusic.org/notes_translations/translations_cantata/t_bwv161.htm#pab1_7
  | title = BWV 161 – Komm, du süße Todesstunde
  | year = 2012
  | publisher = Emmanuel Music
  | accessdate = 4 October 2014
  | ref = harv
  }}
* {{cite web
  | last = Gardiner
  | first = John Eliot
  | authorlink = John Eliot Gardiner
  | url = http://www.bach-cantatas.com/Pic-Rec-BIG/Gardiner-P08c%5Bsdg104_gb%5D.pdf
  | title = Cantatas for the Sixteenth Sunday after Trinity / Santo Domingo de Bonaval, Santiago de Compostela
  | publisher = Bach-Cantatas
  | year = 2006
  | accessdate = 12 September 2015
  | ref = harv
  }}
* {{cite web
  | last = Isoyama
  | first = Tadashi
  | url = http://www.bach-cantatas.com/Pic-Rec-BIG/Suzuki-C05c%5BBIS-CD841%5D.pdf
  | title = BWV 161: Komm, du süße Todesstunde (Come, thou sweet death's hour)
  | publisher = Bach-Cantatas
  | year = 1997
  | accessdate = 12 September 2015
  | ref = harv
  }}
* {{cite web
  | last = Oron
  | first = Aryeh
  | url = http://www.bach-cantatas.com/BWV161.htm
  | title = Cantata BWV 161 Komm, du süße Todesstunde
  | publisher = Bach-Cantatas
  | year = 2015
  | accessdate = 12 September 2015
  | ref = harv
  }}
* {{cite web
  | last = Wolff
  | first = Christoph
  | authorlink = Christoph Wolff
  | url = http://www.bach-cantatas.com/Pic-Rec-BIG/Koopman-C03-1c%5BErato-3CD%5D.pdf
  | title = Komm, du süße Todesstunde, BWV 161
  | publisher = Bach-Cantatas
  | year = 1995
  | accessdate = 12 September 2015
  | ref = harv
  }}
* {{cite web
  | url = http://www.carus-verlag.com/index.php3?selSprache=1&BLink=KKArtikel&ArtikelID=8675
  | title = Johann Sebastian Bach: Komm, du süße Todesstunde
  | publisher = [[Carus-Verlag]]
  | accessdate = 15 September 2010
  | ref = {{sfnRef|Carus|2010}}
  }}
* {{cite web
  | url = https://www.breitkopf.com/work/463/kantate-bwv-161-komm-du-susse-todesstunde
  | title = Cantata BWV 161 Come, thou blessed hour of parting
  | publisher = [[Breitkopf & Härtel|Breitkopf]]
  | accessdate = 19 June 2016
  | ref = {{sfnRef|Breitkopf|2016}}
  }}

== External links ==
* {{cite web
  | last = Grob
  | first = Jochen
  | url = http://www.s-line.de/homepages/bachdiskographie/textkangeist/bwv161_bca135atext.html
  | title = BWV 161 / BC A 135a
  | publisher = s-line.de
  | year = 2014
  | language = German
  | accessdate = 30 August 2015
  }}
* {{cite web
  | last = Koster
  | first = Jan
  | url = http://www.let.rug.nl/Linguistics/diversen/bach/weimar2.html
  | title = Weimar 1708–1717
  | publisher = let.rug.nl
  | year = 2011
  | accessdate = 16 December 2011
  }}
* {{cite web
  | last = Mincham
  | first = Julian
  | url = http://www.jsbachcantatas.com/documents/chapter-69-bwv-161.htm
  | title = Chapter 69 BWV 161 Komm, du süsse Todessunde / Come sweet hour of death.
  | publisher = jsbachcantatas.com
  | year = 2010
  | accessdate = 14 September 2010
  }}
* {{cite web
  | url = http://www.uvm.edu/~classics/faculty/bach/BWV161.html
  | title = BWV 161 Komm, du süße Todesstunde
  | publisher = [[University of Vermont]]
  | year = 2016
  | accessdate = 15 June 2016
  }}

{{Church cantatas by Johann Sebastian Bach}}
{{Bach cantatas|state=expanded}}
{{authority control}}
{{DISPLAYTITLE:''Komm, du süße Todesstunde'', BWV 161}}

{{DEFAULTSORT:Komm Du Susse Todesstunde Bwv 161}}
[[Category:Cantatas by Johann Sebastian Bach]]
[[Category:1710s in music]]