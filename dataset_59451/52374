{{italic title}}
{{Infobox Journal
| cover 	= [[File:Naturephysics.gif|130px]]
| editor 	= Andrea Taroni
| discipline 	= pure and [[applied physics]]
| abbreviation 	= Nature Phys.
| publisher 	= [[Nature Publishing Group]]
| country 	= United Kingdom 
| frequency 	= Monthly
| history 	= 2005 to present
| openaccess 	= 
| license 	= 
| impact 	= 18.791
| impact-year 	= 2015
| website 	= http://www.nature.com/nphys/
| link1 	= 
| link1-name 	= 
| link2 	= 
| link2-name 	= 
| RSS 		=http://www.nature.com/nphys/newsfeeds.html 
| atom	 	= 
| JSTOR 	= 
| OCLC 		= 61856917
| LCCN 		= 2006208901 
| CODEN 	= NPAHAX
| ISSN 		= 1745-2473
| eISSN 	= 1745-2481
| boxwidth 	= 
}}
'''''Nature Physics''''', is a monthly,  [[peer review]]ed, [[scientific journal]] published by the [[Nature Publishing Group]]. It was first published in October 2005 (volume 1, issue 1). The Chief Editor is Andrea Taroni, who is a full-time professional editor employed by this journal.<ref name=guide/>

Publishing formats include letters, articles, reviews, news and views, research highlights, commentaries, book reviews, and correspondence.<ref name=guide>
{{Cite web
  | title =Guide to Authors 
  | publisher =Nature Publishing group 
  | date =July 2010 
  | url =http://www.nature.com/nphys/authors/index.html 
  | accessdate =2010-07-29}}</ref>

==Scope==
''Nature Physics'' publishes both pure and applied research from all areas of [[physics]].<ref name=guide/> Subject areas covered by the journal include [[quantum mechanics]], [[condensed-matter physics]], [[optics]], [[thermodynamics]], [[particle physics]], and [[biophysics]].

==Abstracting and indexing==
Nature Physics is indexed in the following databases:<ref name=cassi>{{Cite web
  | title =''Nature Physics'' 
  | work =Chemical Abstracts Service Source Index (CASSI) (Displaying Record for Publication) 
  | publisher =[[American Chemical Society]]
  | date =July 2010 
  | url =   http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXf-HLJD596fIunAwRW1JNgkDLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfPlAFTKaY2YD99_Fj2vdr_g
  | accessdate =2010-07-29}}</ref><ref name=masterList>{{Cite web
  | title =Mater Journal List search 
  | work =database listings
  | publisher =Thomson Reuters 
  | date =July 2010 
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1745-2473
  | accessdate =2010-07-29}}</ref>  
*[[Chemical Abstracts Service]] – [[CASSI]]
*[[Science Citation Index]]
*[[Science Citation Index Expanded]]
*[[Current Contents]] – Physical, Chemical & Earth Sciences

==References==
{{Reflist}}

==External links==
* [http://www.nature.com/nphys/index.html Nature Physics Journal Website]

{{Georg von Holtzbrinck Publishing Group}}
{{Portal bar|Physics}}

[[Category:Physics journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Publications established in 2005]]
[[Category:Monthly journals]]
[[Category:English-language journals]]