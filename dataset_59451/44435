{{Infobox NCAA team season
  |Year=1938
  |Team=Alabama Crimson Tide
  |Image=
  |ImageSize=
  |Conference=Southeastern Conference
  |ShortConference=SEC
  |Record=7–1–1
  |ConfRecord=4–1–1
  |APRank=13
  |HeadCoach=[[Frank Thomas (American football)|Frank Thomas]]
  |HCYear = 8th
  |Captain =Lew Bostick
  |StadiumArena=[[Bryant–Denny Stadium|Denny Stadium]]<br/>[[Legion Field]]
}}
{{1938 SEC football standings}}
The '''1938 Alabama Crimson Tide football team''' (variously "Alabama", "UA" or "Bama") represented the [[University of Alabama]] in the [[1938 college football season]]. It was the Crimson Tide's 45th overall and 6th season as a member of the [[Southeastern Conference]] (SEC). The team was led by head coach [[Frank Thomas (American football)|Frank Thomas]], in his eighth year, and played their home games at [[Bryant–Denny Stadium|Denny Stadium]] in [[Tuscaloosa, Alabama|Tuscaloosa]] and [[Legion Field]] in [[Birmingham, Alabama|Birmingham]], [[Alabama]]. They finished the season with a record of seven wins, one loss and one tie (7–1–1 overall, 4–1–1 in the SEC).

The Crimson Tide opened the season with a 19–7 victory in an intersectional contest against [[1938 USC Trojans football team|USC]] at [[Los Angeles]]. They then followed up the win with consecutive [[shutouts]], home victories over non-conference opponents {{cfb link|year=1938|team=Howard Bulldogs|title=Howard}} and {{cfb link|year=1938|team=NC State Wolfpack|title=NC State}} on [[homecoming]]. However, Alabama then was shut out 13–0 by [[1938 Tennessee Volunteers football team|Tennessee]], their first loss against the Volunteers since 1932. The Crimson Tide then rebounded with victories against {{cfb link|year=1938|team=Sewanee Tigers|title=Sewanee}}, [[1938 Kentucky Wildcats football team|Kentucky]] and [[1938 Tulane Green Wave football team|Tulane]]. After a 14–14 tie against [[1938 Georgia Tech Yellow Jackets football team|Georgia Tech]], Alabama defeated [[1938 Vanderbilt Commodores football team|Vanderbilt]] in their season finale.

With a final record of 7–1–1, Alabama was ranked No. 13 in the final [[AP Poll]] of the season. Additionally, after the season the Associated Press recognized Alabama as having the best record (40–4–3) and highest winning percentage (.909) of any major college team for the five-year period between 1934 and 1938. Statistically, the defense was one of the most dominant in school history and still holds numerous defense records.

==Schedule==
On December 5, 1937, Frank Thomas announced the 1938 schedule.<ref name="ScheduleRel">{{cite news |title=Three home games on 1938 schedule of Alabama grid team |url=https://news.google.com/newspapers?id=rNY-AAAAIBAJ&sjid=10wMAAAAIBAJ&pg=6619%2C7041674 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=December 5, 1937 |page=6 |accessdate=April 15, 2012}}</ref> The intersectional game against [[1938 USC Trojans football team|USC]] was announced in August 1937 and was the first between the two football powers.<ref name="USC2">{{cite news |title=Alabama, Troy clash in Coliseum next year |first=Braven |last=Dyer |publisher=ProQuest Historical Newspapers |newspaper=Los Angeles Times |date=August 8, 1937 |page=A9}}</ref> The remaining schedule included road games at [[1938 Kentucky Wildcats football team|Kentucky]] and [[1938 Georgia Tech Yellow Jackets football team|Georgia Tech]] with the remaining three games split evenly between [[Bryant–Denny Stadium|Denny Stadium]] and [[Legion Field]].<ref name="ScheduleRel"/>

{{CFB Schedule Start | time = no | rankyear = 1938 | tv = no | attend = yes }}
{{CFB Schedule Entry
| w/l          = w
| date         = September 24
| away         = yes
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = [[1938 USC Trojans football team|USC]]
| site_stadium = [[Los Angeles Memorial Coliseum]]
| site_cityst  = [[Los Angeles|Los Angeles, CA]]
| tv           = no
| score        = 19–7
| attend       = 70,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 1
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1938|team=Howard Bulldogs|title=Howard}}
| site_stadium = [[Bryant–Denny Stadium|Denny Stadium]]
| site_cityst  = [[Tuscaloosa, Alabama|Tuscaloosa, AL]]
| tv           = no
| score        = 34–0
| attend       = 8,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 8
| nonconf      = yes
| homecoming   = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1938|team=NC State Wolfpack|title=NC State}}
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 14–0
| attend       = 10,000
}}
{{CFB Schedule Entry
| w/l          = l
| date         = October 15
| time         = no
| rank         = 
| opponent     = [[1938 Tennessee Volunteers football team|Tennessee]]
| site_stadium = [[Legion Field]]
| site_cityst  = [[Birmingham, Alabama|Birmingham, AL]]
| gamename     = [[Third Saturday in October]]
| tv           = no
| score        = 0–13
| attend       = 25,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 22
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1938|team=Sewanee Tigers|title=Sewanee}}
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 32–0
| attend       = 5,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 29
| away         = yes
| time         = no
| rank         = 18
| opponent     = [[1938 Kentucky Wildcats football team|Kentucky]]
| site_stadium = [[Stoll Field/McLean Stadium|McLean Stadium]]
| site_cityst  = [[Lexington, Kentucky|Lexington, KY]]
| tv           = no
| score        = 26–6
| attend       = 15,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 5
| time         = no
| rank         = 15
| opponent     = [[1938 Tulane Green Wave football team|Tulane]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| tv           = no
| score        = 3–0
| attend       = 19,000
}}
{{CFB Schedule Entry
| w/l          = t
| date         = November 12
| away         = yes
| time         = no
| rank         = 16
| opponent     = [[1938 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| site_stadium = [[Bobby Dodd Stadium|Grant Field]]
| site_cityst  = [[Atlanta|Atlanta, GA]]
| tv           = no
| score        = 14–14
| attend       = 35,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 24
| time         = no
| rank         = 
| opponent     = [[1938 Vanderbilt Commodores football team|Vanderbilt]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| tv           = no
| score        = 7–0
| attend       = 25,000
}}
{{CFB Schedule End
| rank     = 
| timezone = 
| poll     = [[AP Poll]]
| hc       = yes
}}
*<small>Source: Rolltide.com: 1938 Alabama football schedule<ref name="1938schedule">{{cite web| url=http://www.rolltide.com/sports/m-footbl/archive/m-footbl-results-archive.html#1938 |title=1938 Alabama football schedule |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics| accessdate=April 12, 2012}}</ref></small>

==Game summaries==

===USC===
{{See also|1938 USC Trojans football team}}
{{AFB game box start
|Visitor='''Alabama'''
|V1=0 |V2=13 |V3=0 |V4=6
|Host=USC
|H1=0 |H2=0 |H3=0 |H4=7
|Date=September 24
|Location=Los Angeles Memorial Coliseum<br/>Los Angeles, CA
|Attendance=70,000
}}
*'''Source:'''<ref name="USC1">{{cite news |title=Valiant Tide pours over Trojans, 19–7 |url=https://news.google.com/newspapers?id=aeY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6121%2C3631354 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 25, 1938 |page=1 |accessdate=April 12, 2012}}</ref>
{{AFB game box end}}
In August 1937, university officials announced Alabama would open the 1938 season in [[Los Angeles]] against the [[University of Southern California]] (USC).<ref name="USC2"/> Looking for "revenge" after their January loss in the [[1938 Rose Bowl|Rose Bowl]], their first loss on the [[West Coast of the United States|West Coast]], the Crimson Tide defeated the [[USC Trojans football|Trojans]] 19–7 at the [[Los Angeles Memorial Coliseum]].<ref name="USC1"/><ref name="USC3">{{cite news |title=Tide enjoys her day of revenge |url=https://news.google.com/newspapers?id=aeY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6370%2C3667062 |first=Robert |last=Meyers |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 25, 1938 |page=8 |accessdate=April 12, 2012}}</ref><ref name=a1>1938 Season Recap</ref> After a scoreless first quarter, Alabama scored two touchdowns in the second quarter to take a 13–0 halftime lead. The scores came on a pair of Herschel Mosley touchdown passes, the first on a seven-yard pass to Billy Slemons and the second on an 18-yard pass to Gene Blackwell.<ref name="USC1"/> The Trojans responded after the first Alabama touchdown with their deepest drive into Crimson Tide territory of the game. On the drive, Robert Peoples connected with [[Grenny Lansdell]] for a 36-yard gain to the Alabama 22.<ref name="USC4">{{cite news |title=Alabama wallops California to avenge Rose Bowl defeat |url=https://news.google.com/newspapers?id=t_9fAAAAIBAJ&sjid=mwkNAAAAIBAJ&pg=3715%2C1768940 |first=Ronald |last=Wagoner |agency=UPI |publisher=Google News Archives |newspaper=The News & Courier |date=September 25, 1938 |page=6 |accessdate=April 12, 2012}}</ref> However the Alabama defense held, and USC failed to score after they turned the ball over on downs at the Alabama 13-yard line.<ref name="USC4"/>

After they held their 13–0 lead through the third quarter, Hal Hughes [[Interception|intercepted]] an Oliver Day pass and returned it 25-yards for an Alabama touchdown to make the score 19–0 after [[Vic Bradford]] missed his second [[Conversion (gridiron football)|extra point]] of the game.<ref name="USC1"/> Later in the fourth, the Trojans scored their only points of the game. The one-yard Day touchdown run was set up after [[Al Krueger]] recovered [[Charley Boswell]] [[fumble]]d [[Punt (gridiron football)|punt]] at the Alabama one-yard line.<ref name="USC4"/> The victory was their first all-time against USC.<ref name="USCAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Southern California |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3035 |accessdate=April 10, 2012}}</ref> Over 6,000 fans greeted the team at the [[Alabama Great Southern Railroad]] station in downtown Tuscaloosa upon their arrival the following Tuesday to celebrate their victory.<ref name="USC5">{{cite news |title=Welcome home accorded Tide |url=https://news.google.com/newspapers?id=bOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6350%2C3797329 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 28, 1938 |page=7 |accessdate=April 12, 2012}}</ref>
{{clear}}

===Howard===
{{See also2|{{cfb link|year=1938|team=Howard Bulldogs}}}}
{{AFB game box start
|Visitor=Howard
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=7 |H2=13 |H3=7 |H4=7
|Date=October 1
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=8,000
}}
*'''Source:'''<ref name="HU1">{{cite news |title=Alabama reveals power; Davis paces Crimson Tide in rout of Howard by 34–0 |url=https://select.nytimes.com/gst/abstract.html?res=F40814F83B581A7A93C0A9178BD95F4C8385F9 |publisher=The New York Times Archive |newspaper=The New York Times |date=October 1, 1938 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
A week after their intersectional victory over USC to open the season, Alabama hosted Howard (now [[Samford University]]) in their home opener.<ref name="HU2">{{cite news |title=Alabama makes home debut on Saturday |url=https://news.google.com/newspapers?id=buY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=4690%2C3891443 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 30, 1938 |page=10 |accessdate=April 14, 2012}}</ref> In the game, the Crimson Tide outgained the [[Howard Bulldogs football|Bulldogs]] in rushing yards 354 to 8 in their 34–0 [[shutout]] at Denny Stadium.<ref name=a1/><ref name="HU1"/><ref name="HU3">{{cite news |title=Tide wallops Howard in home debut, 34–0 |url=https://news.google.com/newspapers?id=b-Y-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=5018%2C3950251 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 2, 1938 |page=6 |accessdate=April 14, 2012}}</ref> Alabama scored their first touchdown on a 15-yard Billy Slemons run to take a 7–0 first quarter lead.<ref name="HU3"/> In the second quarter touchdowns were scored by, George Zivich on a 43-yard run and by Alvin Davis on a 56-yard run to extend the Alabama lead to 20–0 at halftime.<ref name="HU3"/> The Crimson Tide then closed the game with a pair of second half touchdowns for the 34–0 victory. Davis scored in the third on a two-yard run and [[Charlie Holm]] scored in the fourth on a three-yard run.<ref name="HU3"/> Davis starred for Alabama in the game with his 153 yards rushing on 15 attempts with a pair of touchdowns.<ref name="HU3"/> The victory improved Alabama's all-time record against Howard to 16–0–1.<ref name="HAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Samford |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2867 |accessdate=April 14, 2012}}</ref>
{{clear}}

===NC State===
{{See also2|{{cfb link|year=1938|team=NC State Wolfpack}}}}
{{AFB game box start
|Visitor=NC State
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=0 |H2=14 |H3=0 |H4=0
|Date=October 8
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=10,000
}}
*'''Source:'''<ref name="NCS1">{{cite news |title=Battling Wolves hold Tide to 14–0 score |first=Jay |last=Thornton |url=https://news.google.com/newspapers?id=deY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6376%2C4264704 |publisher=Google News Archives |newspaper=The Tuscaloosa News |page=10 |date=October 9, 1938 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
In their third and final non-conference game of the season, Alabama hosted [[North Carolina State University]] (NC State) in their annual [[homecoming]] contest.<ref name="NCS2">{{cite news |title=Tiders prepared for homecoming tussle |url=https://news.google.com/newspapers?id=dOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6386%2C4200247 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 7, 1938 |page=7 |accessdate=April 14, 2012}}</ref> In the game, the Crimson Tide's two second touchdowns were enough in their defeat of the [[NC State Wolfpack football|Wolfpack]] in their 14–0 [[shutout]] at Denny Stadium.<ref name=a1/><ref name="NCS1"/> After they were held without a first down in the opening quarter, Alabama scored the only points of the game with their two second-quarter touchdowns.<ref name="NCS1"/> The first was on a 28-yard Herschel Mosley pass to Erin Warren and the second on a seven-yard Mosley touchdown run.<ref name="NCS1"/> The Alabama defense dominated the Wolfpack offense and allowed negative rushing yardage (minus four) and zero yards passing.<ref name="NCS3">{{cite news |title=Alabama statistics |url=https://news.google.com/newspapers?id=deY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=5223%2C4263892 |publisher=Google News Archives |newspaper=The Tuscaloosa News |page=10 |date=October 9, 1938 |accessdate=April 14, 2012}}</ref> On offense, Mosley starred for the Crimson Tide with his 123 rushing yards on 15 attempts and one passing and rushing touchdown.<ref name="NCS3"/> The victory was their first all-time against NC State.<ref name="NCSAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs North Carolina State |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2287 |accessdate=April 10, 2012}}</ref>
{{clear}}

===Tennessee===
{{See also|1938 Tennessee Volunteers football team}}
{{AFB game box start
|Title=[[Third Saturday in October]]
|Visitor='''Tennessee'''
|V1=6 |V2=0 |V3=7 |V4=0
|Host=Alabama
|H1=0 |H2=0 |H3=0 |H4=0
|Date=October 15
|Location=Legion Field<br/>Birmingham, AL
|Attendance=25,000
}}
*'''Source:'''<ref name="UT1">{{cite news |title=Vol-Cano erupts, buries Alabama's hopes 13–0 |url=https://news.google.com/newspapers?id=e-Y-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6343%2C4539143 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 16, 1938 |page=6 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
In Birmingham, Alabama was [[upset]] by [[Third Saturday in October|rival]] [[University of Tennessee|Tennessee]] 13–0 at Legion Field.<ref name=a1/><ref name="UT1"/> Leonard Coffman scored both of the Volunteers' touchdowns on one-yard runs in the first and third quarters.<ref name="UT1"/> [[George Cafego]] also starred for Tennessee with his 120 rushing yards on 17 attempts that included separate runs of 48 and 33 yards.<ref name="UT1"/> The loss was Alabama's first against Tennessee since the [[1932 Alabama Crimson Tide football team#Tennessee|1932 season]], and brought Alabama's all-time record against Tennessee to 13–6–2.<ref name="UT2">{{cite news |title=Vols crush Tide 13–0 |url=https://news.google.com/newspapers?id=2WgxAAAAIBAJ&sjid=UU0DAAAAIBAJ&pg=5505%2C5303066 |first=Kenneth |last=Gregory |agency=Associated Press |publisher=Google News Archives |newspaper=St. Petersburg Times |date=October 16, 1937 |page=9 |accessdate=April 14, 2012}}</ref><ref name="TNAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tennessee |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3180 |accessdate=April 14, 2012}}</ref>
{{clear}}

===Sewanee===
{{See also2|{{cfb link|year=1938|team=Sewanee Tigers}}}}
{{AFB game box start
|Visitor=Sewanee
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=0 |H2=7 |H3=6 |H4=19
|Date=October 22
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=5,000
}}
*'''Source:'''<ref name="SU1">{{cite news |title=Tide out-mans Tigers after late start, 32–0 |url=https://news.google.com/newspapers?id=gOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6322%2C4873924 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 23, 1938 |page=6 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
A week after their loss to Tennessee, Alabama defeated the [[Sewanee: The University of the South|Sewanee]] [[Sewanee Tigers football|Tigers]] 32–0 at Denny Stadium.<ref name=a1/><ref name="SU1"/> After a scoreless first quarter, Alabama took a 7–0 lead in the second after [[Vic Bradford]] scored on a one-yard [[quarterback sneak]]. Later in the quarter, a 51-yard Alvin Davis touchdown run was called back due to a [[Holding (American football)|holding penalty]], and he Crimson Tide led 7–0 at the half. After Dallas Wicke scored on a one-yard run in the third, Alabama scored 19 fourth quarter points for the 32–0 win. In the fourth, [[Charley Boswell]] had a pair of rushing touchdowns and thew a third to Erin Warren in the win.<ref name="SU1"/> The victory improved Alabama's all-time record against Sewanee to 17–10–3, in what was their last all-time meeting as Sewanee withdrew from SEC following the [[1940 college football season|1940 season]] and de-emphasized athletics.<ref name="SEWAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Sewanee |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2947 |accessdate=April 10, 2012}}</ref><ref name="SU2">{{cite news |title=Sewanee quits Southeastern |first=Romney |last=Wheeler |agency=Associated Press |url=https://news.google.com/newspapers?id=VuU-AAAAIBAJ&sjid=_kwMAAAAIBAJ&pg=4638%2C4829781 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=December 13, 1940 |page=1 |accessdate=April 14, 2012}}</ref>
{{clear}}

===Kentucky===
{{See also2|[[1938 Kentucky Wildcats football team]]}}
{{AFB game box start
|Visitor= #18 '''Alabama'''
|V1=14 |V2=0 |V3=6 |V4=6
|Host= Kentucky
|H1=0 |H2=6 |H3=0 |H4=0
|Date=October 29
|Location=McLean Stadium<br>Lexington, KY
|Attendance=15,000
}}
*'''Source:'''<ref name="KY1">{{cite news |title=Crimson Tide swamps Wildcats, 26 to 6 |first=S. V. |last=Stiles |url=https://news.google.com/newspapers?id=hOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6417%2C5215523 |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 30, 1938 |page=10 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
As Alabama entered their contest against [[University of Kentucky|Kentucky]], they entered the rankings at No. 18 in the weekly AP Poll.<ref name="KY2">{{cite news |title=Pitt continues editors choice |agency=Associated Press |url=https://news.google.com/newspapers?id=gOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6394%2C5035833 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 25, 1938 |page=7 |accessdate=April 14, 2012}}</ref> In the game, the Crimson Tide defeated the [[Kentucky Wildcats football|Wildcats]] 26–6 on [[homecoming]] at McLean Stadium.<ref name=a1/><ref name="KY1"/> Alabama opened the game with a pair of touchdowns to take a 14–0 lead in the first quarter. [[Charlie Holm]] scored first on a one-yard run and [[Vic Bradford]] scored the second on a 31-yard touchdown reception from Herschel Mosley.<ref name="KY1"/> Kentucky responded in the second with their only points on a 71-yard Dave Zoeller touchdown run to cut the Alabama lead to 14–6 at the half.<ref name="KY1"/> The Crimson Tide then scored on a pair of Mosley touchdown passes in the second half. The first came on a six-yard pass to Bradford in the third and the second on a nine-yard pass to Erin Warren in the fourth.<ref name="KY1"/> The victory improved Alabama's all-time record against Kentucky 17–1.<ref name="KYAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Kentucky |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1628 |accessdate=April 14, 2012}}</ref>
{{clear}}

===Tulane===
{{see also|1938 Tulane Green Wave football team}}
{{AFB game box start
|Visitor= Tulane
|V1=0 |V2=0 |V3=0 |V4=0
|Host= #15 '''Alabama'''
|H1=0 |H2=0 |H3=0 |H4=3
|Date=November 5
|Location=Legion Field<br/>Birmingham, AL
|Attendance=19,000
}}
*'''Source:'''<ref name="TU1">{{cite news |title=Bradford's 17-yard field goal drops Tulane for Tide, 3 to 0 |url=https://news.google.com/newspapers?id=iuY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6306%2C5549451 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 6, 1938 |page=6 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
After their victory over Kentucky, the Crimson Tide moved up three positions to the No. 15 spot in the weekly poll.<ref name="TU2">{{cite news |title=Bears to face Trojan eleven |agency=Associated Press |url=https://news.google.com/newspapers?id=huY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=3768%2C5378042 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 1, 1938 |page=7 |accessdate=April 14, 2012}}</ref> In the game, the Crimson Tide defeated the [[Tulane University|Tulane]] [[Tulane Green Wave football|Green Wave]] 3–0 after [[Vic Bradford]] converted a game-winning, 17-yard [[Field goal (American and Canadian football)|field goal]] late in the fourth quarter.<ref name=a1/><ref name="TU1"/> The victory improved Alabama's all-time record against Tulane to 12–3–1.<ref name="TUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tulane |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3267 |accessdate=April 14, 2012}}</ref>
{{clear}}

===Georgia Tech===
{{see also|1938 Georgia Tech Yellow Jackets football team}}
{{AFB game box start
|Visitor= #16 Alabama
|V1=0 |V2=0 |V3=7 |V4=7
|Host= Georgia Tech
|H1=14 |H2=0 |H3=0 |H4=0
|Date=November 12
|Location=Grant Field<br>Atlanta, GA
|Attendance=35,000
}}
*'''Source:'''<ref name="GT1">{{cite news |title=Crimson Tide rises in second half to tie with Tech Tornado, 14–14 |url=https://news.google.com/newspapers?id=kOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6364%2C5862938 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 13, 1938 |page=10 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
After their close victory over Tulane, the Crimson Tide dropped one position to the No. 16 spot in the weekly poll.<ref name="GT2">{{cite news |title=Texas Christian places first in A. P. grid poll |agency=Associated Press |url=https://news.google.com/newspapers?id=jOY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=3678%2C5671948 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 8, 1938 |page=7 |accessdate=April 14, 2012}}</ref> In their game against [[Georgia Institute of Technology|Georgia Tech]] Alabama fell behind 14–0 after the first quarter, but a pair of second half touchdowns gave the Crimson Tide a 14–14 tie against the [[Georgia Tech Yellow Jackets football|Yellow Jackets]] at Grant Field.<ref name=a1/><ref name="GT1"/> Georgia Tech took an early 14–0 lead after W. C. Gibson threw a 16-yard touchdown pass to George Smith and W. H. Ector scored on a two-yard run.<ref name="GT2a">{{cite news |title=Georgia Tech rises up to tie Alabama, 14–14 |url=https://news.google.com/newspapers?id=yTVPAAAAIBAJ&sjid=U00DAAAAIBAJ&pg=1708%2C1533727 |agency=Associated Press |publisher=Google News Archives |newspaper=St. Petersburg Times |date=November 13, 1938 |accessdate=April 14, 2012}}</ref> Still down 14–0 as they entered the third quarter, Alabama scored their first points of the game on a three-yard Alvin Davis touchdown run to cap a 57-yard drive.<ref name="GT1"/> The Crimson Tide then tied the game in the fourth when they executed a [[hook and lateral]] play, with Davis crossing the endzone line for a 66-yard touchdown.<ref name="GT1"/> Alabama was then in position to attempt a game-winning field goal from the Jackets' 15; however, time expired before they could get a play off which resulted in the 14–14 tie.<ref name="GT1"/> The tie brought Alabama's all-time record against Georgia Tech to 11–10–3.<ref name="GTAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Georgia Tech |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1273 |accessdate=April 14, 2012}}</ref>
{{clear}}

===Vanderbilt===
{{See also|1938 Vanderbilt Commodores football team}}
{{AFB game box start
|Visitor= Vanderbilt
|V1=0 |V2=0 |V3=0 |V4=0
|Host= '''Alabama'''
|H1=0 |H2=0 |H3=0 |H4=7
|Date=November 24
|Location=Legion Field<br>Birmingham, AL
|Attendance=25,000
}}
*'''Source:'''<ref name="VU1">{{cite news |title=Crimson Tide subdues Vandy, 7 to 0 in impressive finish |url=https://news.google.com/newspapers?id=m-Y-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6355%2C6367163 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 25, 1938 |page=9 |accessdate=April 14, 2012}}</ref>
{{AFB game box end}}
In their season finale against the [[Vanderbilt University|Vanderbilt]] [[Vanderbilt Commodores football|Commodores]], Alabama won 7–0 at Legion Field on [[Thanksgiving (United States)|Thanksgiving Day]].<ref name=a1/><ref name="VU1"/> The only scoring drive began in the third and ended early in the fourth with a two-yard [[Vic Bradford]] touchdown run.<ref name="VU1"/> Bradford's [[Conversion (gridiron football)|extra point]] was then blocked, but George Zivich recovered it and took it in for the point to give Alabama the 7–0 lead.<ref name="VU1"/> The victory improved Alabama's all-time record against Vanderbilt to 11–9.<ref name="VUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Vanderbilt |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3363 |accessdate=April 14, 2012}}</ref>
{{clear}}

==After the season==
After all of the regular season games were completed, the final [[AP Poll]] was released in early December.<ref name="FinalPoll">{{cite news |title=T.C.U., Vols place one, two |first=Drew |last=Middleton |agency=Associated Press |url=https://news.google.com/newspapers?id=ouY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=6358%2C6945036 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=December 6, 1938 |page=7 |accessdate=April 14, 2012}}</ref> In the final poll, Alabama held the No. 13 position.<ref name="FinalPoll"/> Alabama was also recognized by the Associated Press for having the best record (40–4–3) and highest winning percentage (.909) of any major, college team for the five-year period between 1934 and 1938.<ref name="FinalRecord">{{cite news |title=Alabama leads nation in five-year grid record |first=Herbert W. |last=Barker |agency=Associated Press |url=https://news.google.com/newspapers?id=ouY-AAAAIBAJ&sjid=IU0MAAAAIBAJ&pg=4963%2C6906127 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=December 5, 1938 |page=7 |accessdate=April 14, 2012}}</ref><ref name="FinalRecord2">{{cite news |title=Alabama Crimson Tide leads nation's major grid teams in survey of five-year record |agency=Associated Press |url=https://news.google.com/newspapers?id=i-FXAAAAIBAJ&sjid=P_UDAAAAIBAJ&pg=6997%2C1138648 |publisher=Google News Archives |newspaper=Spokane Daily Chronicle |date=December 5, 1938 |page=11 |accessdate=April 14, 2012}}</ref>

Statistically, the 1938 defense was one of the best in school history. The 1938 squad still holds numerous defensive records that include:<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=Season Team Records – Defense |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |page=5 |url=http://grfx.cstv.com/photos/schools/alab/sports/m-footbl/auto_pdf/record-book.pdf |accessdate=April 17, 2012 |format=PDF}}</ref>
* Fewest total yards allowed in a season with 701
* Fewest total yards allowed per game with an average of 77.9
* Fewest total yards allowed per play with an average of 1.2
* Fewest first downs allowed in a season with 26
* Fewest rushing yards allowed in a season with 305
* Fewest rushing yards allowed per game with an average of 33.9
* Fewest rushing yards allowed per play with an average of 0.95
* Fewest passing attempts allowed per game with an average of 9.8
* Fewest passing completions allowed per game with an average of 3.4
* Fewest passing yards allowed in a season with 291
* Fewest passing yards allowed per game with an average of 32.7

===NFL Draft===
Several players that were [[Letterman (sports)|varsity lettermen]] from the 1938 squad were drafted into the [[National Football League Draft|National Football League (NFL)]] between the 1939 and 1941 drafts.<ref>{{cite web |url=http://www.pro-football-reference.com/colleges/alabama/drafted.htm |title=Alabama Drafted Players/Alumni |accessdate=April 7, 2012 |work=Sports Reference, LLC |publisher=Pro-Football-Reference.com}}</ref><ref name="NFLDraft">{{cite web |publisher=National Football League | url=http://www.nfl.com/draft/history/fulldraft?abbr=A&collegeName=Alabama&abbrFlag=0&type=school | title=Draft History by School–Alabama|accessdate=March 16, 2013}}</ref> These players included the following:
{| class="wikitable sortable" style="text-align:center"
! scope="col" | Year
! scope="col" | Round
! scope="col" | Overall
! scope="col" | Player name
! scope="col" | Position
! scope="col" | NFL team
|-
| rowspan=2|[[1939 NFL Draft|1939]]
| 3 
| 23 
! {{Sortname|Charlie|Holm}}
| Back 
| [[1939 Washington Redskins season|Washington Redskins]]
|-
| 9 
| 73 
! {{Sortname|Lew|Bostick|nolink=1}}
| Guard 
| [[1939 Cleveland Rams season|Cleveland Rams]]
|-
| rowspan=4|[[1940 NFL Draft|1940]]
| 4 
| 30 
! {{Sortname|Bobby|Wood|Bobby Wood (American football)}}
| Tackle 
| [[1940 Cleveland Rams season|Cleveland Rams]]
|-
| 5 
| 34 
! {{Sortname|Walt|Merrill|nolink=1}} 
| Tackle 
| [[1940 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 11 
| 93 
! {{Sortname|Cary|Cox|nolink=1}}
| Center 
| [[1940 Pittsburgh Steelers season|Pittsburgh Steelers]]
|-
| 11 
| 138 
! {{Sortname|Hayward|Sanford|Sandy Sanford}}
| End 
| [[1940 Washington Redskins season|Washington Redskins]]
|-
| rowspan=3|[[1941 NFL Draft|1941]]
| 3 
| 25 
! {{Sortname|Fred|Davis|Fred Davis (defensive lineman)}}
| Tackle 
| [[1941 Washington Redskins season|Washington Redskins]]
|-
| 7 
| 58 
! {{Sortname|Hal|Newman|nolink=1}}
| End 
| [[1941 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 10 
| 90 
! {{Sortname|Ed|Hickerson|nolink=1}}
| Guard 
| Washington Redskins
|}

==Personnel==
{{Col-begin}}
{{Col-2}}

===Varsity letter winners===
{| class="wikitable" border="1"
|-;
! Player
! Hometown
! Position
|-
| Warren Averitte
| [[Greenville, Mississippi]]
| [[Center (American football)|Center]]
|-
| Silas Beard
| [[Guntersville, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Gene Blackwell
| [[Blytheville, Arkansas]]
| [[End (American football)|End]]
|-
| Lewis Bostick
| [[Birmingham, Alabama]]
| [[Guard (American football)|Guard]]
|-
| [[Charley Boswell]]
| [[Birmingham, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| [[Vic Bradford]]
| [[Memphis, Tennessee]]
| [[Quarterback]]
|-
| Carey Cox
| [[Bainbridge, Georgia]]
| [[Center (American football)|Center]]
|-
| Alvin Davis
| [[Green Forest, Arkansas]]
| [[Fullback (American football)|Fullback]]
|-
| [[Fred Davis (defensive lineman)|Fred Davis]]
| [[Louisville, Kentucky]]
| [[Tackle (American football)|Tackle]]
|-
| Jess Foshee
| [[Clanton, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Jack Gornto
| [[Valdosta, Georgia]]
| [[End (American football)|End]]
|-
| Grover Harkins
| [[Gadsden, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Ed Hickerson
| [[Ventura, California]]
| [[Guard (American football)|Guard]]
|-
| [[Charlie Holm]]
| [[Birmingham, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Hal Hughes
| [[Pine Bluff, Arkansas]]
| [[Quarterback]]
|-
| Walter Merrill
| [[Andalusia, Alabama]]
| [[Tackle (American football)|Tackle]]
|-
| Herschel Mosley
| [[Blytheville, Arkansas]]
| [[Halfback (American football)|Halfback]]
|-
| Hal Newman
| [[Birmingham, Alabama]]
| [[End (American football)|End]]
|-
| Jake Redden
| [[Vernon, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Perron Shoemaker
| [[Birmingham, Alabama]]
| [[End (American football)|End]]
|-
| Billy Slemons
| [[Orlando, Florida]]
| [[Halfback (American football)|Halfback]]
|-
| Joseph Sugg
| [[Russellville, Alabama]]
| [[Guard (American football)|Guard]]
|-
| W. L. Waites
| [[Tuscaloosa, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Erin Warren
| [[Montgomery, Alabama]]
| [[End (American football)|End]]
|-
| Dallas Wicke
| [[Pensacola, Florida]]
| [[Quarterback]]
|-
| [[Bobby Wood (American football)|Bobby Wood]]
| [[McComb, Mississippi]]
| [[Tackle (American football)|Tackle]]
|-
| George Zivich
| [[East Chicago, Indiana]]
| [[Halfback (American football)|Halfback]]
|-
|colspan="3" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Tide Football Lettermen |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=127–141}}</ref>
|}
{{Col-2}}

===Coaching staff===
{| class="wikitable" border="1" style="font-size:90%;"
|-
! Name !! Position !! Seasons at<br />Alabama !! Alma Mater
|-
| [[Frank Thomas (American football)|Frank Thomas]] || [[Head coach]] ||align=center| 8 || [[Notre Dame Fighting Irish football|Notre Dame]] (1923)
|-
| [[Bear Bryant]] || Assistant coach ||align=center| 3 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Paul Burnum]] || Assistant coach ||align=center| 9 || [[Alabama Crimson Tide football|Alabama]] (1922)
|-
| [[Tilden Campbell]] || Assistant coach ||align=center| 3 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Hank Crisp]] || Assistant coach ||align=center| 18 || [[Virginia Tech Hokies football|VPI]] (1920)
|-
| [[Harold Drew]] || Assistant coach ||align=center| 8 || [[Bates Bobcats football|Bates]] (1916)
|-
| Joe Kilgrow || Assistant coach ||align=center| 1 || [[Alabama Crimson Tide football|Alabama]] (1937)
|-
|colspan="4" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Assistant Coaches |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=142–143}}</ref>
|}
{{Col-2}}
{{Col-end}}

==References==
'''General'''
{{refbegin}}
* {{cite web |url=http://www.rolltide.com/datadump/fls_files/files/football/1930s/1938.pdf |title=1938 Season Recap |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics |accessdate=April 12, 2012 |format=PDF}}
{{refend}}

'''Specific'''
{{Reflist|30em}}

{{Alabama Crimson Tide football navbox}}

[[Category:Alabama Crimson Tide football seasons]]
[[Category:1938 Southeastern Conference football season|Alabama]]
[[Category:1938 in Alabama|Crimson Tide]]