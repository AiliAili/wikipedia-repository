{{About|the academic journal|the branch of biomedical science|Immunology}}
{{Infobox journal
| title = Immunology
| cover =
| editor = Daniel Altmann
| discipline = [[Immunology]]
| former_names =
| abbreviation = Immunology
| publisher = [[Wiley-Blackwell]] on behalf of the [[British Society for Immunology]]
| country =
| frequency = Monthly
| history = 1958-present
| openaccess =
| license =
| impact = 3.705
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2567
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2567/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2567/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 265405229
| LCCN = 59037544
| CODEN = IMMUAM
| ISSN = 0019-2805
| eISSN = 1365-2567
}}
'''''Immunology''''' is a monthly [[peer-reviewed]] [[medical journal]] covering all aspects  of [[immunology]]. The [[editor-in-chief]] is Daniel Altmann ([[Imperial College London]]). It was established in 1958 and is published by [[Wiley-Blackwell]] on behalf of the [[British Society for Immunology]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic Search]]
* [[AGRICOLA]]
* [[Aquatic Sciences & Fisheries Abstracts]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[CAB Abstracts]]
* [[CABDirect]]
* [[Current Contents]]/Clinical Medicine
* [[Embase]]
* [[Global Health]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Index Veterinarius]]
* [[Science Citation Index]]
* [[Veterinary Bulletin]]
* [[VINITI Database RAS]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 3.705.<ref name=WoS>{{cite book |year=2013 |chapter=Immunology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== See also ==
* ''[[Clinical and Experimental Immunology]]''

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2567}}

[[Category:Immunology journals]]
[[Category:Publications established in 1958]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]