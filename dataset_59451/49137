{{advert|date=August 2014}}

{{Infobox comedian
| name = Steven R Kutcher
| image = File:Kutcher Steven with Rosie the tarantula CU.png
| imagesize = 216px
| alt = Steven R Kutcher, "The Bug Man of Hollywood"
| caption = Steven Kutcher with Rosie the tarantula, at his home, in Arcadia, California, in 2009.
| pseudonym = The Bug Man of Hollywood
| birth_name =
| birth_date = {{Birth date|1944|1|9|mf=y}}
| birth_place = [[Manhattan]], [[New York (state)|New York]], U.S.
| death_date =
| death_place =
| medium = Film<br/>Television<br/>Radio<br/>Live appearances<br/>Watercolor
| nationality =
| active =
| genre = Bug wrangling, bug art
| subject =
| influences = Rachel Carson, George Washington Carver, Aldo Leopold, John Muir, Theodore Roosevelt, Carl Sagan, Paul Cezanne, Edgar Degas, Max Liebermann, Pablo Picasso, Jackson Pollock, George Tooker
| influenced =
| spouse =
| domesticpartner =
| notable_work = ''Arachnophobia'', ''Jurassic Park'', ''Spider-Man'' (2002)
| signature =
| website = [http://bugsaremybusiness.com http://BugsAreMyBusiness.com]<br/>[http://bugartbysteven.com http://BugArtBySteven.com]
| footnotes =
| current_members =
| past_members =
}}

'''Steven R. Kutcher''' (born January 9, 1944) is an American [[entomologist]] who has worked for decades as a "[[Wrangler (film)|wrangler]]" of insects and other arthropods in some of the highest-grossing productions and with some of the most famous people in the entertainment industry.<ref name="imdb">[http://www.imdb.com/name/nm0476472/ Steven R. Kutcher in IMDB]</ref><ref name="bugsare">[http://bugsaremybusiness.com/ Bugs Are My Business]</ref> In doing so, he has himself gained media attention worldwide as "The Bug Man of Hollywood."<ref name="kcal">[http://losangeles.cbslocal.com/video/7991854-bug-man-of-hollywood-shares-tips-how-to-train-bugs-for-the-big-screen/ Bug Man of Hollywood Shares Tips on How to Train Bugs for the Big Screen], KCAL/KCBS TV.</ref><ref name="npr">{{cite web|last1=NPR Staff|title=Hollywood Bug Man Understands How Cockroaches Think|url=http://www.scpr.org/programs/madeleine-brand/2012/02/01/22350/meet-steven-kutcher-hollywoods-bug-wrangler/|website=Weekend Edition Saturday|publisher=NPR|date=8 March 2014}}</ref><ref name="mail">{{cite news|work=Daily Mail|title=The Mail on Sunday (London)|date=October 2007}}</ref>
In recent years, Kutcher has attracted additional notice<ref name="The Washington Post">{{cite web|last1=Thomas|first1=Nick|title=He Lets Creepy-Crawlies Get Their Feet Wet as Painters|url=http://www.washingtonpost.com/wp-dyn/content/article/2007/08/17/AR2007081700603.html|publisher=The Washington Post|date=19 August 2007}}</ref> by using insects as "living brushes" to create "Bug Art," while continuing his work as a naturalist and an educator.

== Background, education, and training ==

Born in Manhattan, New York, Steven R. Kutcher as a young child collected fireflies in the [[Catskill Mountains]]. Later growing up in a suburb of Los Angeles, California, Kutcher collected insects around his home, in fields, and in the [[Santa Monica Mountains]]. At age 19, Kutcher traveled 3000 miles around Mexico, exploring desert to tropical ecosystems.

Kutcher received a bachelors degree in entomology from the [[University of California, Davis]], in 1968; and a Master of Science degree in biology from the [[California State University, Long Beach]], in 1975. His formal studies focused on insect behavior — in particular the aggregating behavior of the [[milkweed bug]], ''Oncopeltus fasciatus''<ref>{{cite journal|last1=Kutcher|first1=Steven|title=Two Types of Aggregation Grouping in the Large Milkweed Bug, Oncopeltus fasciatus (Hemiptera: Lygaeidae)|journal=Bulletin of the Southern California Academy of Sciences|date=1971|volume=70|issue=2|pages=85–90}}</ref> — as he observed in the field, in laboratory experiments, and in time-lapse cinematography.

In 1970, Kutcher began his work in the entertainment industry as "Larry J. Felix" in ''[[Stein and Illes|The Stein and Illes Radio Show]]'', a comedic "underground" radio show on [[KUSC]], in Southern California; [[James R. Stein]] and [[Robert Illes]] would both become Emmy Award–winning TV writers and producers. Kutcher received comedic training from Bill Cosby and once had Robin Williams as an audience.<ref name="bugsare" />

== "The Bug Man of Hollywood" ==

Since 1977, Kutcher has manipulated the instinctive behaviors of arthropods, and the instinctive reactions of audiences, mostly in the horror, thriller, fantasy, and comedy genres. He has worked on over 100 feature films with [[Arthropods in film|a "bug" in the story line]], including ''Spider-Man'' (2002), ''Jurassic Park'', and ''Arachnophobia''. Kutcher has also worked on numerous popular television shows—including  ''CSI: NY'', ''MacGyver'', and ''The X-Files'' – as well as TV commercials and online advertising for Fortune 500 corporations. (See Filmography and other credits,<ref name="imdb"/><ref name="bugsare"/> with featured "bugs," below).

In film, on TV and radio, and in music videos, Kutcher has notably worked with some of the most famous people in the entertainment industry, including Paula Abdul, Christina Aguilera, Steve Allen, Halle Berry, Carol Burnett, Marlon Brando, Richard Burton, Bill Cosby, Wes Craven, M.C. Hammer, Janet Jackson, Michael Jackson, James Earl Jones, David Lynch, Carl Reiner, Steven Spielberg, Denzel Washington, Sigourney Weaver, Robin Williams, and Stevie Wonder.

As "The Bug Man of Hollywood," Kutcher has himself been the subject of numerous interviews. He has appeared, with "bugs," on late-night TV talk shows, including [[List of The Tonight Show Starring Johnny Carson episodes (1990)|''The Tonight Show Starring Johnny Carson'']] and ''[[Late Night with David Letterman]]''. In 1992, Kutcher appeared as a guest on the Emmy-nominated "Spider Episode"<ref name="spiderEpisode">{{cite web|title=The Larry Sanders Show (TV Series) The Spider Episode (1992)|url=http://www.imdb.com/title/tt0625417/fullcredits?ref_=tt_ov_st_sm|publisher=IMDb}}</ref> of the TV talk show parody ''[[The Larry Sanders Show]]'', starring Garry Shandling. In 1998, Kutcher appeared on the British TV children's show ''The Scoop'', which won a BAFTA award.<ref name="bafta">[http://awards.bafta.org/award/1998/childrens/factual 1998 BAFTA Award for Children's Factual production]</ref>
Kutcher has also been interviewed or featured in numerous publications in print and online, including ''Entertainment Weekly'', ''Guinness Book of Records'', ''Los Angeles Times'', ''National Enquirer'', ''National Geographic World'', ''Nature'', ''Newsweek'', ''New York Times'', ''Popular Science'', ''Ripley's Believe It or Not'', ''Time'', ''The Wall Street Journal'', ''Weekly Reader'', and ''Wired'' as well as periodicals in Australia, Germany, Italy, Japan, and the United Kingdom. (See Further reading, below.)

== Manipulating insect behavior ==

Applying his academic and professional studies of arthropod behaviors, Kutcher manipulates instinctive responses—such as species-specific, positive or negative sensitivity to light, air pressure, or gravity—to make "bugs" perform scripted "tricks" on cue,<ref name="bugsare" /> such as:

* A live wasp flies harmlessly into the mouth of actor [[Roddy McDowell]].
* A cockroach runs across the floor and then, "hitting its mark", flips over on its back.
* A spider crawls across a room and then into a slipper.
* A cockroach crawls out of a shoe, walks up a bag of snack food and onto a surfing magazine, and then stops upon a picture of a surfboard.
* A praying mantis, a scorpion, and beetles power-up a cell phone as part of a "Bug Circus" in online ad<ref>{{cite web|title=Snapdragon Apresenta - The Bug Circus Generator|url=https://www.youtube.com/watch?v=xouZ8-XLV_o}}</ref> (a la a traditional [[flea circus]]).
* Hundreds of bees or thousands of locusts swarm on camera as called for in the script.

== "Bug Art" ==

In the 1980s, for a Steven Spielberg television project, Steven Kutcher made a fly walk through ink and leave footprints as directed.<ref name="bugart">[http://BugArtBySteven.com Bug Art by Steven], official Web site</ref>
Since 2000, Steven Kutcher has been creating "Bug Art," using various [[arthropods]] as "living brushes" to apply [[gouache]] and other nontoxic paints on watercolor paper.<ref name="bugartvideo">{{YouTube|YDm5jX7-Nh4|Bug Art video}}</ref>
"I use water-based, nontoxic paints that easily wash off", he says. "I have to take good care of them. After all, they are artists!"<ref name="The Washington Post"/>
The [[abstract art|abstract]] to [[surrealistic]] compositions are shaped by Kutcher's methods of manipulating insect movements, and are often influenced by the works of [[Impressionist]] and other master painters.<ref>{{cite web|last1=Thomas|first1=Nick|title=Exhibit Reviewed: The art of arthropods|url=http://www.nature.com/nature/journal/v450/n7170/full/450613a.html|publisher=Nature|date=28 November 2007}}</ref>
Kutcher's bug art has been exhibited in art galleries, museums, universities, libraries, and online and offline publications worldwide.<ref>{{cite web|last1=Myall|first1=Steve|title=Meet Van Moth, the artist who paints with bugs|url=http://www.mailonsunday.co.uk/news/article-486077/Meet-Van-Moth-artist-paints-bugs.html|publisher=Mail (London)|date=8 October 2007}}</ref>

== Contributions as scientist, naturalist, and educator ==

Steven Kutcher has appeared in person to give talks and live-insect demonstrations at hundreds of film festivals, seminars and workshops, museums and libraries, and preschools through graduate schools.<ref name="bugsare" />
Kutcher has been instrumental in creating annual insect fairs, as at the [[Los Angeles County Museum of Natural History]] and [[Los Angeles County Arboretum and Botanic Garden|Los Angeles County Arboretum]], which have been attended by more than 100,000 children and adults. Kutcher also served as a consultant in the development of the interactive "bug" exhibits at the [[Kidspace Children's Museum]],<ref>{{cite web|url=http://www.kidspacemuseum.org/|title=Kidspace Children's Museum|author=|date=}}</ref> in Pasadena, California.

Kutcher has taught outdoor education workshops for such environmental organizations as the [[Audubon Society]], the [[Sierra Club]], and [[Tree People]]. With a milkweed [[Butterfly gardening|butterfly garden]] of his own, Kutcher is on the board of the Monarch Program<ref>{{cite journal|last1=Kutcher|first1=Steven|title=Two Incredible Journeys|journal=The Monarch Quarterly|date=2002|volume=XII|issue=2|page=3}}</ref>
Kutcher has consulted on the biology and control of arthropods for major corporations and government agencies, such as the [[Greater Los Angeles County Vector Control District]].

For over 30 years, Kutcher has taught entomology, zoology, and biology courses at several community colleges in the greater Los Angeles area.

== Filmography and other credits (in part) ==

=== Theatrical films ===

In addition to serving as "bug wrangler" or entomology consultant for numerous student and independent films, Kutcher has worked on many feature films from major studios and production companies:<ref name="imdb"/><ref name="bugsare"/>
{| class="wikitable sortable"
|-
! Film !! Year !! Featured "bugs"
|-
| ''[[We Bought a Zoo]]'' || 2011 || Swarm of honey bees
|-
| ''[[G-Force (film)|G-Force]]'' || 2009 || Tarantula, cockroach (test shots for animation)
|-
| ''[[National Treasure: Book of Secrets]]'' || 2007 || Beetles etc.
|-
| ''[[The Hitcher (2007 film)|The Hitcher]]'' || 2007 || Spiders, scorpion
|-
| ''[[Antwone Fisher (film)|Antwone Fisher]]'' || 2002 || Grasshoppers
|-
| ''[[Spider-Man (2002 film)|Spider-Man]]'' || 2002 || Spiders (wrangled live spiders and consulted on [[Computer-generated imagery|cgi]] spiders, as "The Spider Man Behind ''Spider-Man''"<ref name="natlgeo">{{cite web|last1=Trivedi|first1=Bijal P.|title=The Spider Man Behind Spider-man|url=http://news.nationalgeographic.com/news/2002/05/0502_020502_TVspiderman.html|website=National Geographic Today|date=2 May 2002}}</ref>)
|-
| ''[[Wild Wild West]]'' || 1998 || Tarantula
|-
| ''[[Lost Highway (film)|Lost Highway]]'' || 1997 || Spider, moths
|-
| ''[[Mimic (film)|Mimic]]'' || 1997 || Ants, termites
|-
| ''[[Alien 4|Alien: Resurrection]]'' || 1997 || Spider (with web)
|-
| ''[[L.A. Confidential]]'' || 1996 || Maggots (on body under house)
|-
| ''[[D3: The Mighty Ducks]]'' || 1996 || Ants
|-
| ''[[Jack (1996 film)|Jack]]'' || 1996 || Monarch butterflies
|-
| ''[[James and the Giant Peach (film)|James and the Giant Peach]]'' || 1996 || Dwarf tarantulas (in costume)
|-
| ''[[A Very Brady Sequel]]'' || 1996 || Tarantula
|-
| ''[[Copycat (film)|Copycat]]'' || 1995 || Carpenter ants (covering Sigourney Weaver)
|-
| ''[[Matilda (1996 film)|Matilda]]'' || 1995 || Cockroach (and newt)
|-
| ''[[A Walk in the Clouds]]'' || 1995 || Butterflies
|-
| ''[[Leprechaun 2]]'' || 1994 || Cockroaches (green) and tarantulas
|-
| ''[[Jurassic Park (film)|Jurassic Park]]'' || 1993 || Mosquitoes (live and "prehistoric," simulated by crane fly in "[[amber]]")
|-
| ''[[The Temp (film)|The Temp]]'' || 1993 || Wasps, scorpions, cockroaches
|-
| ''[[Meet the Applegates]]'' || 1991 || Praying mantis, tarantula
|-
| ''[[Arachnophobia (film)|Arachnophobia]]'' || 1990 || Spiders, crickets, etc.
|-
| ''[[Back to the Future 2|Back to the Future Part II]]'' || 1989 || Various insects (pinned etc. in displays)
|-
| ''[[The Burbs|The 'Burbs]]'' || 1989 || Bees
|-
| ''[[Fright Night Part 2|Fright Night II]]'' || 1989 || Mealworms etc.
|-
| ''[[Police Academy 6: City Under Siege]]'' || 1989 || Cabbage white butterflies (as "moths")
|-
| ''[[A Nightmare on Elm Street 3: Dream Warriors]]'' || 1987 || Dragonfly, fly
|-
| ''[[The Golden Child]]'' || 1986 || Monarch butterfly
|-
| ''[[The Goonies]]'' || 1985 || Leech (scene cut)
|-
| ''[[Exorcist II: The Heretic]]'' || 1977 || Locusts (grasshoppers)
|}

=== TV movies and series<ref name="bugsare" /> ===

{| class="wikitable sortable"
|-
! TV production !! Year !! Featured "bugs"
|-
| ''[[An Inconvenient Woman#Television adaptation|An Inconvenient Woman]]'' || c. 1991 || Fly
|-
| ''[[Bernie Mac Show|Bernie Mac Show, The]]'' || 2003 || Mealworm beetles
|-
| ''[[Boy Meets World]]'' || c. 1997, 1999 || Snails, bees
|-
| ''[[Buck Rogers in the 25th Century (TV series)|Buck Rogers in the 25th Century]]'' || c. 1980 || Dragonflies
|-
| ''[[Chicago Hope]]'' || 2000 || Tarantula
|-
| ''[[Criminal Minds]]'' || 2012, 2013, 2013 || Maggots, flies, waxworms, praying mantis
|-
| ''[[CSI: New York|CSI: NY]]'' || 2006 || Mealworms
|-
| ''[[Family Matters]]'' || c. 1995 || Giant mealworms
|-
| ''[[Kung Fu: The Movie]]'' || 1986 || Grasshopper
|-
| [[Larry Sanders Show#Season 1|''Larry Sanders Show'', the (Emmy-nominated "The Spider Episode")]] || 1996 || Tarantulas
|-
| ''[[Laverne & Shirley|Laverne and Shirley]] Reunion'' || 2002 || Cockroaches etc.
|-
| [[Life with Bonnie|''Life with Bonnie'' (''The Bonnie Hunt Show'')]] || 2004 || Honey bee
|-
| ''[[MacGyver]]'' || 1991 || Cockroaches
|-
| ''[[The Mentalist|Mentalist, The]]'' || 2013 ||
|-
| ''[[Monk (TV series)|Monk]]'' || 2005 || Bees
|-
| ''[[Power Rangers]]'' || 1994 || Praying mantis, cockroaches
|-
| ''[[Tarantulas: The Deadly Cargo]]'' || 1977 || Tarantulas
|-
| ''[[The Women of Brewster Place (TV miniseries)|Women of Brewster Place, The]]'' || 1989 || Cockroach
|-
| ''[[Wonder Woman (TV series)|Wonder Woman]]'' || 1978 || Ants
|-
| ''[[The X-Files]]'' || 1999, 2005 || Flies, moths
|-
| ''[[The Young and the Restless]]'' || 1991 || Ants
|}

=== Music videos<ref name="bugsare" /> ===

{| class="wikitable sortable"
|-
! Artist !! Project !! Year !! Featured "bugs"
|-
| [[Paula Abdul]] || ||  || Butterflies (in display case)
|-
| [[Christina Aguilera]] || "[[Fighter (song)|Fighter]]" || 2002 || Moths
|-
| [[Alice Cooper]] || ''[[Welcome to My Nightmare]]'' || || Scorpions, mealworms
|-
| [[Godsmack]] || "[[I Stand Alone (Godsmack song)|I Stand Alone]]" || 2002 || Scorpion
|-
| [[M.C. Hammer]] || || 1992 || Butterflies
|-
| [[Billy Idol]] || "[[L.A. Woman (song)|L.A. Woman]]" || 1990 || Cockroach, mealworm
|-
| [[Janet Jackson]] || "[[Together Again (Janet Jackson song)|Together Again]]" (Deeper Remix) || 1997 || Butterfly
|-
| [[Michael Jackson]] || "[[Stranger in Moscow]]" || 1996 || Wasp
|-
| [[Korn]] || || 1996 || Cockroaches
|-
| [[No Doubt]] || ''[[Don't Speak]]'' || 1996 || Fly, mealworms
|}

=== TV and online commercials<ref name="bugsare" /> ===

{| class="wikitable"
|-
! Market segment !! Brands !! Featured "bugs"
|-
| Automobiles || Chrysler, Dodge, Fiat, Honda, Hyundai, Jeep, Kia, Lexus, Mini Cooper, Mitsubishi, Peugeot, Subaru, Toyota, Volkswagen || Bees, beetles, butterflies, dragonflies, flies, grasshoppers, ladybugs, scorpions, spider webs
|-
| Consumer electronics || Apple, Dell, Goldstar, Hewlett Packard, Hitachi, Kodak, Nintendo Game Boy, Panasonic, Polaroid, Sega, Sony, TDK, VCR Plus || Ants, bees, butterflies, flies, moths, praying mantids, snails
|-
| Consumer goods (misc.) || Adidas, All, Anderson Windows, Avia, Dial Soap, Dockers, Evinrude, Galoop Toys, Gladlock, Hallmark, K-Mart, Kleenex, Levi Strauss, Lowes, Nike, Northern Bathroom Tissue, Omega Watch, Revlon, Sears, Snuggles, Standard Brands Paint, Stainmaster, Sunlight Detergent, Swatch || Bees, beetles, butterflies, caterpillars, cockroaches, flies, ladybugs, mosquitoes, moths, spiders
|-
| Energy || Chevron, Florida Power and Light, Mobil, Sempra || Ants, beetles, butterflies, flies, ladybugs, spider webs
|-
| Entertainment || Cartoon Network, PGA, Virginia Lottery, WCW || Ants, beetles, cockroaches, flies, mealworms
|-
| Fast food and other restaurants || Applebee's, Carl's Jr., Dunkin' Donuts, KFC, McDonald's, Seven-Eleven, Souplantation, Taco Bell || Bees, butterflies, moth cocoons, praying mantids
|-
| Finance and insurance || Blue Cross, Capital One, Chase, Fuji Bank, Hartford Insurance, Interstate Bank, Premier Insurance, Tri-County Health, Barclay's || Bees, butterflies, caterpillars, ladybugs, millipedes, walkingsticks
|-
| Food and beverages || Gerber's, Bud Light, Dr. Pepper, Dryer's, Gallo, Jolly Rancher, Mauna Loa, Michelob, Milk Advisory Board ("Got Milk?"), Moet, Orida potatoes, Pepsi, Planter's, Reese's Pieces, Rath Blackhawk bacon, Smith's Markets, Snickers, Zima || Ants, bees, butterflies, caterpillars, flies, Jerusalem crickets, moths, tarantulas and other spiders, wasps
|-
| Industrial and commodities || Alcoa, Georgia Pacific, Monsanto || Ants, butterflies, moths, spider webs
|-
| Pest control || Combat, Orkin, Ortho, Scott's || Ants, butterflies, cockroaches, grubs, termites
|-
| Public service announcements (PSAs) || AD Council, Partnership for a Drug-Free America || Bugs (misc.), leeches
|-
| Telecom || AT&T, Atlantic Bell, Bell Canada, Qualcomm, Telecom Italia, Verizon || Bees, beetles, butterflies, cockroaches, mealworms, mosquitoes, praying mantids, scorpions, tarantulas
|}

== See also ==

* [[Animal training]], citing "niche" filled by Steven Kutcher.
* [[Arthropods in film]], citing work by Steven R. Kutcher.
* [[List of The Tonight Show Starring Johnny Carson episodes (1990)|List of ''The Tonight Show Starring Johnny Carson'' episodes (1990)]], Jay Leno (guest host), with Steve Kutcher as guest, Ep. No. 4201, August 7, 1990.
* ''[[Steatoda grossa]]'', citing work by Steven Kutcher with that species of spider in ''[[Spider-Man (2002 film)|Spider-Man]]''.

== References ==

{{reflist}}

== External links ==

* {{cite web|last1=Heltzel|first1=Paul|title=Bugs Make Art: Photos|url=http://news.discovery.com/animals/insects/bugs-make-art-pictures-130304.htm|publisher=Discovery News|date=4 March 2013}}
* {{cite web|title=Steven R. Kutcher|url=http://www.imdb.com/name/nm0476472/|publisher=IMDb|accessdate=4 June 2014}}
* {{cite web|last1=Mitchell|first1=Sandra|title='Bug Man of Hollywood' Shares Tips [on] How to Train Bugs for The Big Screen|url=http://news.discovery.com/animals/insects/bugs-make-art-pictures-130304.htm|publisher=KCBS-TV|accessdate=5 June 2014}}
* {{Citation | url = http://news.nationalgeographic.com/news/2002/05/0502_020502_TVspiderman.html | title = The Spider Man Behind Spider-man | first = Bijal | last = Trivedi | publisher = National Geographic Society | date=2 May 2002}}
* {{cite web|last1=NPR|first1=Staff|title=Hollywood Bug Man Understands How Cockroaches Think|url=http://www.npr.org/2014/03/08/287296168/hollywood-bug-man-understands-how-cockroaches-think|publisher=NPR|accessdate=8 March 2014}}
* {{cite web|last1=Solomon|first1=Michael|title=Wild at Art: Astonishing Paintings, Sculptures, and Photographs by Animal Artists|url=http://newsfeed.time.com/2013/06/05/wild-at-art-astonishing-paintings-sculptures-and-photographs-by-animal-artists/|publisher=Time|date=5 June 2013}}
* {{Citation | url = http://www.washingtonpost.com/wp-dyn/content/article/2007/08/17/AR2007081700603.html | title = He Lets Creepy-Crawlies Get Their Feet Wet as Painters | first = Nick | last = Thomas | publisher = The Washington Post | date=19 August 2007}}
* {{cite web|last1=Marquardt|first1=Bridget|title=The Bug Wrangler|url=http://shine.yahoo.com/animal-nation/bug-wrangler-220500025.html|publisher=Yahoo Animal Nation Shine}}
* {{cite web|last1=Kutcher|first1=Steven|title=E-Zip Bug Collector|url=https://www.youtube.com/watch?v=kGmdUsJiFa0|publisher=YouTube}}

== Further reading ==

===Periodicals===

* {{cite news|work=Wired Magazine|date=May 2012|page=113}}
* {{cite news|work=Le Republica (Italy)|date=July 2009}}
* {{cite news|work=Velvet Magazine (Italy)|date=July 2009}}
* {{cite news|work=Muy Interesante|date=November 2008|pages=112–115}}
* {{cite news|work=Pasadena Star News (Earth Day Supplement)|date=22 April 2008}}
* {{cite news|work=Washington Post|date=19 August 2007|page=N01}}
* {{cite news|work=Nature|date=29 November 2007|page=613}}
* {{cite news|work=The Mail on Sunday (London)|date=October 2007}}
* {{cite news|work=ÇA M'Interesse (France)|date=June 2007|page=106}}
* {{cite news|work=K Club (Germany)|date=August 2004|page=36}}
* {{cite news|work=The Santa Fe New Mexican ("Steven Hutcher" [sic])|date=6 December 2002|page=B-1}}
* {{cite news|work=Current Science|date=25 October 2002|page=Front Page}}
* {{cite news|work=The Beach Review (California State University, Long Beach)|date=Fall 2002|page=29}}
* {{cite news|work=Los Angeles Business Journal|date=30 September 2002|page=23}}
* {{cite news|work=Cineflex|date=July 2002|pages=29–31}}
* {{cite news|work=National Enquirer|date=11 June 2002|page=39}}
* {{cite news|work=West Australian|date=6 June 2002}}
* {{cite news|work=Popular Science|date=June 2002|page=76}}
* {{cite news|work=New York Times|date=May 2002}}
* {{cite news|work=Sydney Herald|date=May 2002}}
* {{cite news|work=Chicago Sun Times|date=May 2002}}
* {{cite news|work=Sunday Mirror|date=May 2002}}
* {{cite news|work=Press Telegram (Long Beach, California)|date=11 May 2002|page=Front Page}}
* {{cite news|work=Teen Newsweek|date=6 May 2002|pages=5–6}}
* {{cite news|work=National Geographic World|date=May 2002|pages=26–27}}
* {{cite news|work=Neüe Zurcher Zeitung, NZZ Folio|date=July 2001|pages=50–53}}
* {{cite news|work=Wall Street Journal|date=29 March 2001|page=Front Page}}
* {{cite news|work=Newsweek|date=8 January 2001|page=9}}
* {{cite news|work=Boy's Life|date=12 October 1998}}
* {{cite news|work=San Jose Mercury News (Silicon Valley Life)|date=11 October 1998|page=1}}
* {{cite news|work=San Francisco Chronicle|date=5 May 1998}}
* {{cite news|work=Los Angeles Times Magazine|date=4 April 1998}}
* {{cite news|work=Los Angeles Business Journal|date=4 April 1998}}
* {{cite news|work=Los Angeles Times (San Fernando Valley Weekend)|date=28 August 1997|page=7}}
* {{cite news|work=Boston Herald (Life Styles)|date=30 May 1997}}
* {{cite news|work=National Enquirer|date=31 December 1996|page=39}}
* {{cite news|work=Los Angeles Times (San Gabriel Valley Weekly)|date=September 1996}}
* {{cite news|work=Various Newspapers (via Associated Press)|date=June 1996}}
* {{cite news|work=U.C., Davis Magazine|date=Spring 1996|pages=24–25}}
* {{cite news|work=Telegraph (Sydney, Australia)|date=7 May 1995|page=43}}
* {{cite news|work=Los Angeles Magazine|date=August 1995|pages=60–67}}
* {{cite news|work=Scholastic Math Power|date=February 1993|pages=8–9}}
* {{cite news|work=Boy's Life|date=April 1992}}
* {{cite news|work=Disney Adventures Magazine|date=February 1992|pages=40–43}}
* {{cite news|work=Press Telegram (Long Beach, California)|date=23 October 1991|page=D-1}}
* {{cite news|work=Los Angeles Times (L.A. Times Magazine)|date=29 September 1991|page=8}}
* {{cite news|work=Science World (Cover Article)|date=8 March 1991|pages=4–7}}
* {{cite news|work=Los Angeles Times|date=23 March 1991|page=B-3}}
* {{cite news|work=Woman's Day|date=30 October 1990|page=152}}
* {{cite news|work=Variety|date=8 October 1990|page=N-1}}
* {{cite news|work=Pest Control Technology|date=September 1990|pages=36–37}}
* {{cite news|work=Expressen Fredag|date=28 September 1990|page=6}}
* {{cite news|work=Weekly Reader|date=14 September 1990|page=7}}
* {{cite news|work=L.A. Weekly|date=31 August 1990|page=41}}
* {{cite news|work=Sun-Sentinel (Fort Lauderddale, Florida)|date=7 August 1990|page=1-E}}
* {{cite news|work=New Hampshire Sunday News|date=5 August 1990|page=5-E}}
* {{cite news|work=The Yuma Daily Sun|date=29 July 1990|page=25}}
* {{cite news|work=Entertainment Weekly|date=27 July 1990|page=37}}
* {{cite news|work=The Philadelphia Inquirer|date=22 July 1990|page=H-1}}
* {{cite news|work=Los Angeles Times (Calendar)|date=18 July 1990|page=F-6}}
* {{cite news|work=Wall Street Journal|date=12 December 1989|page=Front Page}}
* {{cite news|work=Sky (Delta Air Lines)|date=September 1989|page=20}}
* {{cite news|work=Premiere|date=June 1989|page=46}}
* {{cite news|work=Katso (Finland)|date=May 1989}}
* {{cite news|work=Los Angeles Business Journal|date=7 November 1988|page=19}}
* {{cite news|work=Los Angeles Times|date=14 February 1988}}
* {{cite news|work=Piccolo (Sweden)|date=1988 or 1989}}
* {{cite news|work=3-2-1 Contact|date=November 1986|pages=10–13}}
* {{cite news|work=Los Angeles Magazine|date=May 1986|page=21}}
* {{cite news|work=Herald Examiner (California Living)|date=13 April 1986|page=15}}
* {{cite news|work=Los Angeles Times|date=22 December 1985|page=4:27}}
* {{cite news|work=Huntington Beach Independent (Orange County News)|date=22 February 1979|page=23}}
* {{cite news|work=Los Angeles Times|date=9 April 1978|page=7}}
* {{cite news|work=El Vaquero (Glendale Community College)|date=5 November 1976|page=4}}

=== Journals and books ===

* {{cite book|last=Baron |first=Angela|title=Squirrely Over Nuts (caterpillar footprints) |year=2010 }}
* {{cite book|last1=Berenbaum|first1=May|title=Bugs in the System|date=1995}}
* {{cite book|last=Bhagwat |first=Abha|title=Who We Are 'Olkha Aamhi Kon?' (ant footprints) |year=2012 }}
* {{cite book|last1=Dale|first1=N.|title=Flowering Plants: The Santa Monica Mountains Coastal and Chaparral Regions of Southern California|date=1986}}
* {{cite book|last1=Gerani|first1=Garry|title=Death Ship (cover, by Cliff Nielsen)|date=1995}}
* {{cite book|last1=Gordon|first1=D.|title=The Complete Cockroach|date=1996}}
* {{cite book|last1=Gordon|first1=D.|title=The Eat-a-Bug Cookbook|date=1998}}
* {{cite book|title=Guinness Book of Records|year=2009}}
* {{cite book|last1=Hogue|first1=Charles|title=Cultural Entomology|journal=Annual Review of Entomology|date=1987}}
* {{cite book|last1=Hogue|first1=Charles|title=Insects of the Los Angeles Basin|year=1993}}
* {{cite book|last1=Jackson|first1=Donna|title=The Bug Scientists|year=2002|publisher=Houghton Mifflin and Company|pages=87–90}}
* {{cite journal|last1=Mertins|first1=James|title=Arthropods on the Screen|journal=Bulletin of the Entomological Society of America|date=1986|issue=Summer|pages=85–90}}
* {{cite book|last1=Pallenberg|first1=Barbara|title=The Making of The Exorcist II: The Heretic|year=1977}}
* {{cite book|last1=Rothstein|first1=Barry|last2=Rothstein|first2=Betsy|title=Eye-Popping 3-D Bugs|year=2011}}
* {{cite book|title=Ripley's Believe It or Not. Prepared to be Shocked!|year=2008|page=21}}
* {{cite book|last1=Starcher|first1=A.|title=Good Bugs for Your Garden|year=1995}}
* {{cite book|last1=Tekulsy|first1=M.|title=The Butterfly Garden|year=1985}}
* {{cite book|title=Time for Kids. Big Book of How|year=2011|page=19}}
* {{cite book|last1=Cotta Vaz|first1=Mark |authorlink=Mark Cotta Vaz|title=Behind the Mask of Spider-Man|year=2002|publisher=Del Rey, Ballantine Publishing Group|pages=48–50}}

{{DEFAULTSORT:Kutcher, Steven R.}}
[[Category:1944 births]]
[[Category:Living people]]
[[Category:American entomologists]]
[[Category:Animal trainers]]
[[Category:University of California, Davis alumni]]
[[Category:California State University, Long Beach alumni]]