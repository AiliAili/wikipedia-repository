{|{{Infobox Aircraft Begin 
|name =RWD 7
|image =RWD7 Drzewiecki Wedrychowski.jpg 
|caption =Jerzy Drzewiecki and Jerzy Wędrychowski by the RWD 7
}}{{Infobox Aircraft Type 
|type =Sports plane 
|manufacturer =[[Warsaw University of Technology]] workshops
|designer = [[RWD (aircraft manufacturer)|RWD]] team
|first flight =July 1931 
|introduced =1931 
|retired =1938 
|status = 
|primary user =Polish civilian aviation 
|more users = 
|produced =
|number built =1
|unit cost = 
|variants with their own articles = 
}}
|} 
The '''RWD 7''' was a [[Poland|Polish]] sports plane of [[1931 in aviation|1931]], constructed by the [[RWD (aircraft manufacturer)|RWD]] team.

==Development==
The RWD 7 was constructed by the [[RWD (aircraft manufacturer)|RWD]] team of [[Stanisław Rogalski]], [[Stanisław Wigura]] and [[Jerzy Drzewiecki]] in [[Warsaw]]. It was based upon their earlier designs, especially the [[RWD 2]] and [[RWD 4]]. The RWD 7 was meant to be a record-beating plane, so it had a more powerful engine, while its mass was reduced. From its predecessors, it took the same fish-shaped fuselage without a direct view towards forward from the pilot's seat.<ref name=glass>Glass, A., op.cit., p.298-299</ref>

The only RWD 7 built (registration SP-AGH) was flown in July  [[1931 in aviation|1931]] by its designer Jerzy Drzewiecki. On August 12, 1931, Drzewiecki and Jerzy Wędrychowski established an international [[Fédération Aéronautique Internationale|FAI]] speed record of 178&nbsp;km/h (111&nbsp;mph) in the light touring plane class, (below 280&nbsp;kg / 616&nbsp;lb empty weight).<ref name=glass/> On September 30, [[1932 in aviation|1932]], Drzewiecki and [[Antoni Kocjan]] set a height record of 6,023 m (19,755&nbsp;ft). The RWD 7 was used in Warsaw Aero Club, among others, for [[aerobatics]], then in 1936 it was bought by a known aviator [[Zbigniew Babiński]] for touring flights and used until [[1938 in aviation|1938]].<ref name=glass/>

The RWD 7 was known for its extremely short take-off run: with a single crew member only 18&nbsp;m (59&nbsp;ft), with two crew members, 30&nbsp;m (98&nbsp;ft).<ref name=glass/>

==Description==
The RWD 7 was a wooden construction, conventional in layout, high-wing cantilever [[monoplane]]. The fuselage was rectangular in cross-section (narrowing in upper part), plywood-covered, apart from the engine section, which was aluminium sheet-covered. The wings were trapezoid, single-spar, single part, canvas and plywood covered. A crew of two was sitting in [[tandem]], with a pilot in the rear cab. The crew cabs were open on upper sides, and had doors on the right side. The engine was 5-cylinder [[Armstrong Siddeley Genet II]] [[radial engine]], 56&nbsp;kW (75&nbsp;hp) nominal power. Two-blade wooden propeller of a fixed pitch. The plane had a [[conventional landing gear]], with a rear skid. A 30 l fuel tank was in central part of wing. A cruise fuel consumption was 18 l/hour.<ref name=glass/>

==Specifications==
{{Aircraft specifications|
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Glass, A. (1977), p. 299
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|crew=1 
|capacity=1
|length main=9.8 m 
|length alt=32 ft 2 in
|span main=6.3 m 
|span alt=20 ft 8 in 
|height main=2.0 m 
|height alt=6 ft 7 in
|area main=13.60 m²
|area alt= 146 ft²
|empty weight main= 246 kg 
|empty weight alt= 541 lb
|loaded weight main= 440 kg 
|loaded weight alt= 968 lb
|useful load main= 194 kg
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt=
|engine (prop)=[[Armstrong Siddeley Genet]] II 
|type of prop= air-cooled 5-cylinder radial engine
|number of props=1
|power main=75 hp 
|power alt=56 kW

|max speed main=186 km/h
|max speed alt=116 mph
|cruise speed main= 160 km/h
|cruise speed alt=
|stall speed main= 65 km/h
|stall speed alt= 
|range main=260 km
|range alt=163 miles
|ceiling main=6,000 m
|ceiling alt=19,680 ft
|climb rate main=6.2 m/s
|climb rate alt=1,220 ft/min
|loading main=32 kg/m² 
|loading alt= 6.6 lb/ft²
|power/mass main= 0.23 kW/kg
|more performance=
*Take-off run: 18-30 m
}}

==See also==

{{Aircontent|
|related=
*[[RWD 1]]
*[[RWD 2]]
*[[RWD 3]]
*[[RWD 4]]

|similar aircraft=<!-- similar or comparable aircraft -->

|lists=<!-- related lists -->

|see also=<!-- other relevant information -->
}}

==References==
{{commons category|RWD-7}}
{{reflist}}
*Andrzej Glass: "Polskie konstrukcje lotnicze 1893-1939" (''Polish aviation constructions 1893-1939''), WKiŁ, Warsaw 1977 (Polish language, no ISBN)

{{RWD aircraft}}

[[Category:Polish sport aircraft 1930–1939|RWD 07]]
[[Category:RWD aircraft|RWD 07]]