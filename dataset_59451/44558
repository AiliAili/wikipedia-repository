{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book| 
| name          = Tess of the d'Urbervilles:<br />A Pure Woman Faithfully<br />Presented
| title_orig    =
| translator    =
| image         = Tess.jpg
| caption       = The front cover of an 1892 edition of ''Tess of the d'Urbervilles: A Pure Woman Faithfully Presented'', published by Harper & Bros, NY.
| author        = [[Thomas Hardy]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| genre         =
| publisher     = James R. Osgood, McIlvaine & Co.
| published     = 1891
| pages         = 592
}}
'''''Tess of the d'Urbervilles: A Pure Woman Faithfully Presented''''' is a novel by [[Thomas Hardy]]. It initially appeared in a [[Book censorship|censored]] and [[Serialized novel|serialised]] version, published by the British illustrated newspaper ''[[The Graphic]]'' in 1891<ref>''Tess of the d'Urbervilles'', ''Graphic'', XLIV, July–December 1891</ref> and in book form in 1892. Though now considered a major nineteenth-century English novel and possibly Hardy's fictional masterpiece,<ref>{{cite journal|title=Review: ''Tess of the d'Urbervilles: a Pure Woman Faithfully Presented'' by Thomas Hardy|journal=[[Athenaeum (British magazine)|The Athenaeum]]|date=January 9, 1892|number=3350|pages=49–50|url=https://books.google.com/books?id=wYNAAQAAMAAJ&pg=PA49}}</ref> ''Tess of the d'Urbervilles'' received mixed reviews when it first appeared, in part because it challenged the [[sexual morals]] of late [[Victorian England]].

== Summary of the novel ==
[[File:Hardy - Tess d'Urbervilles, 1891 - 3657925F.jpg|thumb|''Tess of the d'Urbervilles'', title page of the 1891 edition]]

=== Phase the First: The Maiden (1–11) ===
The novel is set in impoverished rural England, [[Thomas Hardy's Wessex|Thomas Hardy's fictional Wessex]], during the [[Long Depression]] of the 1870s. Tess is the oldest child of John and Joan Durbeyfield, uneducated peasants. However, John is given the impression by Parson Tringham that he may have noble blood, as "Durbeyfield" is a corruption of "D'Urberville", the surname of an extinct noble [[Normans|Norman]] family. Knowledge of this immediately goes to John's head.

That same day, Tess participates in the village [[Maypole|May Dance]], where she meets Angel Clare, youngest son of Reverend James Clare, who is on a [[walking tour]] with his two brothers. He stops to join the dance and partners several other girls. Angel notices Tess too late to dance with her, as he is already late for a promised meeting with his brothers. Tess feels slighted.

Tess's father gets too drunk to drive to the market that night, so Tess undertakes the journey herself. However, she falls asleep at the reins, and the family's only horse encounters a speeding wagon and is fatally wounded. Tess feels so guilty over the horse's death and the economic consequences for the family that she agrees, against her better judgement, to visit Mrs d'Urberville, a rich widow who lives in a rural mansion near the town of Trantridge, and "claim kin". She is unaware that, in reality, Mrs d'Urberville's husband Simon Stoke adopted the surname even though he was unrelated to the real d'Urbervilles.

Tess does not succeed in meeting Mrs d'Urberville, but chances to meet her [[libertine]] son, Alec, who takes a fancy to Tess and secures her a position as poultry keeper on the estate. Although Tess tells them about her fear that he might try to seduce her, her parents encourage her to accept the job, secretly hoping that Alec might marry her. Tess dislikes Alec but endures his persistent unwanted attention to earn enough to replace her family's horse. Despite his often cruel and manipulative behaviour, the threat that Alec presents to Tess's virtue is sometimes obscured for Tess by her inexperience and almost daily commonplace interactions with him. Late one night, walking home from town with some other Trantridge villagers, Tess inadvertently antagonizes Car Darch, Alec's most recently discarded favourite, and finds herself in physical danger. When Alec rides up and offers to "rescue" her from the situation, she accepts. Instead of taking her home, however, he rides through the fog until they reach an ancient grove in a forest called "The Chase", where he informs her that he is lost and leaves on foot to get his bearings. Alec returns to find Tess asleep, and it is implied that he rapes her, although there remains a degree of ambiguity.<ref>{{cite book|last1=Watts|first1=Cedric|title=Thomas Hardy 'Tess of the d'Urbervilles'|date=2007|publisher=Humanities-Ebooks|location=Penrith|isbn=9781847600455|pages=32–3}}</ref>

Mary Jacobus, a commentator on Hardy's works, speculates that the ambiguity may have been forced on the author to meet the requirements of his publisher and the "[[Mrs Grundy|Grundyist]]" readership of his time.<ref>{{cite journal|last1=Jacobus|first1=Mary|title=Tess's Purity|journal=Essays in Criticism|publisher=[[Oxford University Press]]|date=1976|volume=XXVI|issue=4|pages=318–338|doi=10.1093/eic/XXVI.4.318}}</ref>
[[File:Hambledon Hill towards Stourton Tower 20070730.jpg|thumb|400px|left|The [[Vale of Blackmore]], the main setting for ''Tess''. [[Hambledon Hill]] towards [[King Alfred's Tower|Stourton Tower]]]]

=== Phase the Second: Maiden No More (12–15) ===
Tess goes home to her father's cottage, where she keeps almost entirely to her room, apparently feeling both traumatized and ashamed of having lost her virginity. The following summer, she gives birth to a sickly boy who lives only a few weeks. On his last night alive, Tess [[baptism|baptises]] him herself, because her father would not allow the parson to visit, stating that he did not want the parson to "pry into their affairs". The child is given the name 'Sorrow', but despite the baptism Tess can only arrange his burial in the "shabby corner" of the churchyard reserved for unbaptised infants. Tess adds a homemade cross to the grave with flowers in an empty marmalade jar.

=== Phase the Third: The Rally (16–24) ===
More than two years after the Trantridge debacle, Tess, now twenty, has found employment outside the village, where her past is not known. She works for Mr. and Mrs. Crick as a [[milkmaid]] at Talbothays Dairy. There, she befriends three of her fellow milkmaids, Izz, Retty, and Marian, and meets again Angel Clare, now an [[apprentice]] farmer who has come to Talbothays to learn dairy management. Although the other milkmaids are in love with him, Angel singles out Tess, and the two fall in love.

=== Phase the Fourth: The Consequence (25–34) ===
[[File:Tess1891.jpg|thumb|"He jumped up from his seat... and went quickly toward the desire of his eyes." 1891 illustration by Joseph Syddall]]
Angel spends a few days away from the dairy, visiting his family at Emminster. His brothers Felix and Cuthbert, both ordained Church of England ministers, note Angel's coarsened manners, while Angel considers them staid and narrow-minded. The Clares have long hoped that Angel would marry Mercy Chant, a [[piety|pious]] schoolmistress, but Angel argues that a wife who knows farm life would be a more practical choice. He tells his parents about Tess, and they agree to meet her. His father, the Reverend James Clare, tells Angel about his efforts to convert the local populace, mentioning his failure to tame a young miscreant named Alec d'Urberville.

Angel returns to Talbothays Dairy and asks Tess to marry him. This puts Tess in a painful dilemma: Angel obviously thinks her a virgin, and she shrinks from confessing her past. Such is her love for him, though, that she finally agrees to the marriage, pretending that she only hesitated because she had heard he hated old families and thought he would not approve of her d'Urberville ancestry. However, he is pleased by this news because he thinks it will make their match more suitable in the eyes of his family.

As the marriage approaches, Tess grows increasingly troubled. She writes to her mother for advice; Joan tells her to keep silent about her past. Her anxiety increases when a man from Trantridge, named Groby, recognises her and crudely alludes to her history. Angel overhears and flies into an uncharacteristic rage. Tess, deciding to tell Angel the truth, writes a letter describing her dealings with d'Urberville and slips it under his door. When Angel greets her with the usual affection the next morning, she thinks he has forgiven her; later she discovers the letter under his carpet and realises that he has not seen it. She destroys it.

The wedding ceremony goes smoothly, apart from the [[omen]] of a cock crowing in the afternoon. Tess and Angel spend their wedding night at an old d'Urberville family mansion, where Angel presents his bride with diamonds that belonged to his godmother.  When he confesses that he once had a brief affair with an older woman in London, Tess finally feels able to tell Angel about Alec, thinking he will understand and forgive.

=== Phase the Fifth: The Woman Pays (35–44) ===
However, Angel is appalled by the revelation, and makes it clear that Tess is reduced in his eyes. Although he admits that Tess was "more sinned against" than she has sinned herself, he feels that her "want of firmness" confronting Alec may indicate a flaw in her character and that she is no longer the woman he thought she was. He spends the wedding night on a sofa. After a few awkward days, a devastated Tess suggests they separate, saying that she will return to her parents. Angel gives her some money and promises to try to reconcile himself to her past, but warns her not to try to join him until he sends for her. After a brief visit to his parents, Angel takes a ship to Brazil to see if he can start a new life there. Before he leaves, he encounters Tess's milkmaid friend Izz and impulsively asks her to come with him as his mistress. She accepts, but when he asks her how much she loves him, she admits "Nobody could love 'ee more than Tess did! She would have laid down her life for 'ee. I could do no more!" Hearing this, he abandons the whim, and Izz goes home weeping bitterly.

Tess returns home for a time. However, she soon runs out of money, having to help out her parents more than once. Finding her life with them unbearable, she decides to join Marian at a [[wikt:hardscrabble|starve-acre]] farm called Flintcomb-Ash; they are later joined by Izz. On the road, she is again recognised and insulted by Groby, who later turns out to be her new employer. At the farm, the three former milkmaids perform hard physical labour.

One winter day, Tess attempts to visit Angel's family at the parsonage in Emminster, hoping for practical assistance. As she nears her destination, she encounters Angel's older brothers, with Mercy Chant. They do not recognise her, but she overhears them discussing Angel's unwise marriage, and dares not approach them. On the way back home, she overhears a wandering preacher and is shocked to discover that it is Alec d'Urberville, who has been converted to [[Methodism]] under the Reverend James Clare's influence.

=== Phase the Sixth: The Convert (45–52) ===
Alec and Tess are each shaken by their encounter. Alec claims that she has put a spell on him and makes Tess swear never to tempt him again as they stand beside an ill-omened stone monument called the Cross-in-Hand. However, Alec continues to pursue her and soon comes to Flintcomb-Ash to ask Tess to marry him, although she tells him she is already married. He begins stalking her, despite repeated rebuffs, returning at [[Candlemas]] and again in early spring, when Tess is hard at work feeding a [[threshing machine]]. He tells her he is no longer a preacher and wants her to be with him. When he insults Angel, she slaps him, drawing blood. Tess then learns from her sister, Liza-Lu, that her father, John, is ill and that her mother is dying. Tess rushes home to look after them. Her mother soon recovers, but her father unexpectedly dies from a heart condition.

The impoverished family is now evicted from their home, as Durbeyfield held only a [[Life estate|life lease]] on their cottage. Alec, having followed her to her home village, tries to persuade Tess that her husband is never coming back and offers to house the Durbeyfields on his estate. Tess refuses his assistance several times. She had earlier written Angel a [[Psalms|psalm-like]] letter, full of love, self-abasement, and pleas for mercy, in which she begs him to help her fight the temptation she is facing. Now, however, she finally begins to realize that Angel has wronged her and scribbles a hasty note saying that she will do all she can to forget him, since he has treated her so unjustly.

The Durbeyfields plan to rent some rooms in the town of Kingsbere, ancestral home of the d'Urbervilles, but arrive to find that the rooms have already been rented to another family. All but destitute, they are forced to take shelter in the churchyard, under the D'Urberville window. Tess enters the church and in the d'Urberville Aisle, Alec reappears and importunes Tess again. The scene ends with her desperately looking at the entrance to the d'Urberville vault and wishing herself dead.

In the meantime, Angel has been very ill in Brazil and, his farming venture having failed, heads home to England. On the way, he confides his troubles to a stranger, who tells him that he was wrong to leave his wife; what she was in the past should matter less than what she might become. Angel begins to repent his treatment of Tess.

=== Phase the Seventh: Fulfilment (53–59) ===
Upon his return to his family home, Angel has two letters waiting for him: Tess's angry note and a few cryptic lines from "two well-wishers" (Izz and Marian), warning him to protect his wife from "an enemy in the shape of a friend". He sets out to find Tess and eventually locates Joan, now well-dressed and living in a pleasant cottage. After responding evasively to his enquiries, she tells him Tess has gone to live in [[Bournemouth#Literature references|Sandbourne]], a fashionable seaside resort. There, he finds Tess living in an expensive [[boarding house]] under the name "Mrs. d'Urberville." When he asks for her, she appears in startlingly elegant attire and stands aloof. He tenderly asks her forgiveness, but Tess, in anguish, tells him he has come too late. Thinking he would never return, she has yielded at last to Alec d'Urberville's persuasion and has become his mistress. She gently asks Angel to leave and never come back. He departs, and Tess returns to her bedroom, where she falls to her knees and begins a [[lamentation]]. She blames Alec for causing her to lose Angel's love a second time, accusing Alec of having lied when he said that Angel would never return to her.

The following events are narrated from the perspective of the landlady, Mrs. Brooks. The latter tries to listen in at the keyhole, but withdraws hastily when the argument between Tess and Alec becomes heated. She later sees Tess leave the house, then notices a spreading red spot – a bloodstain – on the ceiling. She summons help, and Alec is found stabbed to death in his bed.

Angel, totally disheartened, is leaving Sandbourne; Tess hurries after him and tells him that she has killed Alec, saying that she hopes she has won his forgiveness by murdering the man who ruined both their lives. Angel does not believe her at first, but grants her his forgiveness and tells her that he loves her. Rather than heading for the coast, they walk inland, vaguely planning to hide somewhere until the search for Tess is ended and they can escape abroad from a port. They find an empty mansion and stay there for five days in blissful happiness, until their presence is discovered one day by the cleaning woman.

They continue walking and, in the middle of the night, stumble upon [[Stonehenge]], where Tess lies down to rest on an ancient altar. Before she falls asleep, she asks Angel to look after her younger sister, Liza-Lu, saying that she hopes Angel will marry her after she is dead. At dawn, Angel sees that they are surrounded by police. He finally realises that Tess really has committed murder and asks the men in a whisper to let her awaken naturally before they arrest her. When she opens her eyes and sees the police, she tells Angel she is "almost glad" because "now I shall not live for you to despise me". Her parting words are, "I am ready."

Tess is escorted to Wintoncester ([[Winchester]]) prison. The novel closes with Angel and Liza-Lu watching from a nearby hill as the black flag signalling Tess's execution is raised over the prison. Angel and Liza-Lu then join hands and go on their way.

== Symbolism and themes ==
[[File:Stonehenge cloudy sunset.jpg|thumb|400px|Sunset at [[Stonehenge]]]]
Hardy's writing often explores what he called the "ache of modernism", and this theme is notable in ''Tess'', which, as one critic noted,<ref>[https://books.google.com/books?id=VSlD9b_o4JQC&pg=PA14&lpg=PP1&ie=ISO-8859-1&output=html Dale Kramer, ''Tess'', p. 14]</ref> portrays "the energy of traditional ways and the strength of the forces that are destroying them". In depicting this theme Hardy uses imagery associated with hell when describing modern farm machinery, as well as suggesting the effete nature of city life as the milk sent there must be watered down because townspeople cannot stomach whole milk. Angel's middle-class fastidiousness makes him reject Tess, a woman whom Hardy presents as a sort of [[Wessex]] [[Eve]], in harmony with the natural world. When he parts from her and goes to [[Brazil]], the handsome young man gets so ill that he is reduced to a "mere yellow skeleton". All these instances have been interpreted as indications of the negative consequences of man's separation from nature, both in the creation of destructive machinery and in the inability to rejoice in pure and unadulterated nature.{{citation needed|date=September 2014}}

On the other hand, Marxist critic [[Raymond Williams]] in ''The English Novel From Dickens to Lawrence'' questions the identification of Tess with a peasantry destroyed by [[Industrial Revolution|industrialisation]]. Williams sees Tess not as a peasant, but as an educated member of the rural working class, who suffers a tragedy through being thwarted, in her aspirations to socially rise and her desire for a good life (which includes love and sex), not by industrialism, but by the landed bourgeoisie (Alec), liberal idealism (Angel) and Christian moralism in her family's village (see Chapter LI).

Another important theme of the novel is the sexual [[double standard]] to which Tess falls victim; despite being, in Hardy's view, a truly good woman, she is despised by society after losing her virginity before marriage. Hardy plays the role of Tess's only true friend and advocate, pointedly subtitling the book "a pure woman faithfully presented" and prefacing it with [[Shakespeare]]'s words from ''[[The Two Gentlemen of Verona]]'': "Poor wounded name! My bosom as a bed/ Shall lodge thee." However, although Hardy clearly means to criticise [[Victorian morality|Victorian]] notions of female purity, the double standard also makes the heroine's tragedy possible, and thus serves as a mechanism of Tess's broader fate. Hardy variously hints that Tess must suffer either to [[Atonement in Christianity|atone]] for the misdeeds of her ancestors, or to provide temporary amusement for the gods, or because she possesses some [[Hamartia|small but lethal character flaw]] inherited from her ancestors.{{citation needed|date=September 2014}}

Because of the numerous [[Paganism|pagan]] and neo-[[Bible|Biblical]] references made about her, Tess has been viewed variously as an Earth goddess or as a sacrificial victim.<ref>[https://books.google.com/books?id=av57F0sS-HoC&pg=PA183 Radford, ''Thomas Hardy and the Survivals of Time'', p. 183]</ref> For example, early in the novel, she participates in a festival for [[Ceres (Roman mythology)|Ceres]], the goddess of the harvest, and when she baptises her dead child she chooses a passage from [[Book of Genesis|Genesis]], the book of creation, rather than the more traditional [[New Testament]] verses. Then at the end, when Tess and Angel come to [[Stonehenge]], which was commonly believed in Hardy's time to be a pagan temple, she willingly lies down on a stone supposedly associated with [[human sacrifice]].

Tess has also been seen as a personification of nature and her association with animals throughout the novel emphasizes this idea. Tess's misfortunes begin when she falls asleep while driving Prince to market, and causes the horse's death; at Trantridge, she becomes a poultry-keeper; she and Angel fall in love amid cows in the fertile Froom valley; and on the road to Flintcombe-Ashe, she kills some wounded [[pheasant]]s to end their suffering.{{citation needed|date=September 2014}}

However, Tess emerges as a powerful character not because of this symbolism but because "Hardy's feelings for her were strong, perhaps stronger than for any of his other invented personages".<ref>[https://books.google.com/books?id=W6aycisnuiAC&pg=PA119&lpg=PP1&ie=ISO-8859-1&output=html J.Hillis Miller, ''Fiction and Repetition'', p.119]</ref>

== Adaptations ==

=== Theatre ===
The novel was adapted for the stage for the first time in 1897. This a production by Lorimer Stoddard proved a great Broadway triumph for actress [[Mrs. Fiske|Minnie Maddern Fiske]],  was revived in 1902, and subsequently made into a motion picture by [[Adolph Zukor]] in 1913, starring ''Mrs. Fiske'', of which no copies remain.

In 1924 Hardy himself wrote the script for the first British theatrical adaptation and he chose Gertrude Bugler, a [[Dorchester, Dorset|Dorchester]] girl from the original Hardy Players, to play Tess.<ref name="woodhall">Woodhall, N., (2006), ''Norrie's Tale: An Autobiography of the Last of the 'Hardy Players''', Wareham: Lullworde Publication</ref> The Hardy Players (now re-formed in 2005 by Bugler's sister Norrie) was an amateur group from Dorchester who re-enacted Hardy’s novels. Bugler was highly acclaimed,<ref name="tomalin">Tomalin, C., (2006), ''Thomas Hardy'', London: Viking</ref> but she was prevented from taking the London stage part by Hardy's wife, [[Florence Dugdale|Florence]], who was jealous of her;{{citation needed|date=June 2013}} Hardy had said that young Gertrude was the true incarnation of the Tess he had imagined. Years before writing the novel, Hardy had been inspired by the beauty of her mother Augusta Way, then an 18-year-old milkmaid, when he visited Augusta's father's farm in [[Higher Bockhampton|Bockhampton]]. Hardy remembered her when writing the novel. When Hardy saw Bugler (he rehearsed The Hardy Players at the hotel run by her parents), he immediately recognised her as the young image of the now older Augusta.<ref name="woodhall"/>

The novel was successfully adapted for the stage several other times:

* 1946: An adaptation by playwright [[Ronald Gow]] became a triumph on the West End starring [[Wendy Hiller]].
* 1999: ''Tess of the d'Urbervilles'', a new West End musical with music by Stephen Edwards and lyrics by Justin Fleming opens in London at the Savoy Theatre.
* 2007: ''Tess, The New Musical'' (a rock opera) with lyrics, music and libretto by Annie Pasqua and Jenna Pasqua premieres in NYC.
* 2009: ''Tess of the d'Urbervilles'', a new adaptation for the stage with five actors was produced in London by Myriad Theatre & Film.
* 2010: ''Tess'', a new rock opera is an official Next Link Selection at the New York Musical Theatre Festival with music, lyrics, and libretto by Annie Pasqua and Jenna Pasqua.
* 2012: ''Tess of the d'Urbervilles'' is produced into a piece of musical theatre by the Youth Music Theatre UK as part of their summer season.

=== Opera ===

1906: An [[Italian opera]]tic version written by [[Frédéric Alfred d'Erlanger|Frederic d'Erlanger]] was first performed in [[Naples]], but the run was cut short by an eruption of [[Mount Vesuvius]]. When the opera came to London three years later, Hardy, then 69, attended the premiere.

=== Film ===

The story has also been filmed at least eight times, including three for general release through cinemas and four television productions.

* '''Cinema''':
** 1913: The [[Tess of the d'Urbervilles (1913 film)|"lost" silent version]], mentioned above (in theatre), starring [[Mrs. Fiske|Minnie Maddern Fiske]] as Tess and Scots-born [[David Torrence]] as Alec.<ref>[http://www.imdb.com/title/tt0003442/ ''Tess of the D'Urbervilles'' (1913)]. – [[IMDb]].</ref>
** 1924: [[Tess of the d'Urbervilles (1924 film)|Another lost silent version]] made with [[Blanche Sweet]] (Tess), [[Stuart Holmes]] (Alec), and [[Conrad Nagel]] (Angel).<ref>[http://www.imdb.com/title/tt0015394/ ''Tess of the D'Urbervilles'' (1924)]. – [[IMDb]].</ref>
** 1967: Hindi film "[[Dulhan Ek Raat Ki]]" starring [[Nutan]], [[Dharmendra]] and [[Rehman (actor)|Rehman]].<ref>{{cite web|url=https://memsaabstory.com/2008/03/03/dulhan-ek-raat-ki-1967/|title=Dulhan Ek Raat Ki (1967)|date=3 March 2008|publisher=}}</ref>
** 1979: [[Roman Polanski]]'s film ''[[Tess (film)|Tess]]'' with [[Nastassja Kinski]] (Tess), [[Leigh Lawson]] (Alec), and [[Peter Firth]] (Angel).<ref>[http://www.imdb.com/title/tt0080009/ ''Tess'']. – [[IMDb]].</ref>
** 1996: [[Rajiv Kapoor]]'s film ''[[Prem Granth]]'' with [[Madhuri Dixit]], [[Rishi Kapoor]] and [[Shammi Kapoor]] in lead roles. It was a commercial failure.
** 2000: Assamese filmmaker Bidyut Chakrabarty's film ''Nishiddha Nadi'' starring Trisha Saikia, Bina Baruwoti, Dhritiman Phukan and a host of others was based on the novel. The film was produced by the Assam State Film (Finance and Development) Corporation and was released on 18 February 2000. Cinematography, Editing and Music Direction were done by National Award Winners, Mrinal Kanti Das, [[A. Sreekar Prasad]] and Sher Choudhury respectively.
** 2011: [[Michael Winterbottom]] 21st century Indian set film ''[[Trishna (2011 film)|Trishna]]'' with [[Freida Pinto]] and [[Riz Ahmed]].<ref>[http://www.imdb.com/title/tt1836987/ ''Trishna'']. – [[IMDb]].</ref>
** 2013: ''The Maiden'' 21st century set film starring Brittany Ashworth,<ref>{{cite web|url=http://www.imdb.com/name/nm1312305/|title=Brittany Ashworth|publisher=}}</ref> Matt Maltby and Jonah Hauer-King, directed by Daisy Bard, written and produced by Jessica Benhamou.<ref>{{cite web |url=http://oxfordstudent.com/2013/10/02/interview-oxford-grad-adapts-hardys-tess/ |title=Interview: Oxford grad adapts Hardy’s Tess |first=Rachel |last=Brook |date=10 February 2013 |website=The Oxford Student |publisher=WordPress |accessdate=March 15, 2015}}</ref>
* '''Television''':
** 1952: [[BBC TV]], directed by Michael Henderson, and starring [[Barbara Jefford]] (Tess), [[Michael Aldridge]] (Alec), and [[Donald Eccles]] (Angel).<ref>[http://www.imdb.com/title/tt1294219/ ''Tess of the D'Urbervilles'' (1952) (TV)]. – [[IMDb]].</ref>
** 1960: [[ITV (TV network)|ITV]], ''ITV Play of the Week'', "Tess", directed by Michael Currer-Briggs, and starring [[Geraldine McEwan]] (Tess), [[Maurice Kaufmann]] (Alec), and [[Jeremy Brett]] (Angel).<ref>[http://www.imdb.com/title/tt0922206/ ''ITV Play of the Week'' – "Tess" (1960)]. – [[IMDb]].</ref>
** 1998: [[London Weekend Television]]'s three-hour mini-series ''Tess of the D'Urbervilles'', directed by Ian Sharp, and starring [[Justine Waddell]] (Tess), [[Jason Flemyng]] (Alec), and [[Oliver Milburn]] (Angel), the latter himself Dorset-born.<ref>[http://www.imdb.com/title/tt0126100/ ''Tess of the D'Urbervilles'' (1998)]. – [[IMDb]].</ref>
** 2008: A [[Tess of the D'Urbervilles (TV serial)|four-hour BBC adaptation]], written by [[David Nicholls (writer)|David Nicholls]], aired in the United Kingdom in September and October 2008 (in four parts),<ref>[http://www.bbc.co.uk/pressoffice/pressreleases/stories/2008/01_january/21/tess.shtml ''Tess of the D'Urbervilles'' – Thomas Hardy's classic novel for BBC One]. – [[BBC]]. – 21 January 2008.</ref> and in the United States on the [[Public Broadcasting Service|PBS]] series ''[[Masterpiece Classic]]'' in January 2009 (in two parts).<ref>Wiegand, David. – "Compelling performances rescue 'Tess'". – ''[[San Francisco Chronicle]]''. – 2 January 2009.</ref> The cast included [[Gemma Arterton]] (Tess), [[Hans Matheson]] (Alec), [[Eddie Redmayne]] (Angel), [[Ruth Jones]] (Joan), [[Anna Massey]] (Mrs d'Urberville), and [[Kenneth Cranham]] (Reverend James Clare).<ref>[http://www.bbc.co.uk/pressoffice/pressreleases/stories/2008/03_march/17/tess.shtml ''Tess Of The D'Urbervilles'' – vibrant young cast line-up for dramatic adaptation of Hardy classic for BBC One]. – [[BBC]]. – 17 March 2008.</ref><ref>[http://www.imdb.com/title/tt1186342/ ''Tess of the d'Urbervilles'' (2008)]. – [[IMDb]].</ref>

===Music===
American [[metalcore]] band [[Ice Nine Kills]] has a song called Tess-Timony inspired by this novel on their album ''[[Every Trick in the Book]]''.

== Notes ==
{{Reflist|30em}}

== Secondary sources ==
* Davis, William A., Jr. "Hardy and the 'Deserted Wife' Question: The Failure of the Law in ''Tess of the D'urbervilles''." Colby Quarterly 29.1 (1993): 5–19.
* Gossin, Pamela. Thomas Hardy's Novel Universe: Astronomy, Cosmology, and Gender in the Post-Darwinian World. Aldershot, England : Ashgate, 2007.
* Heffernan, James A. W. "'Cruel Persuasion': Seduction, Temptation and Agency in Hardy's ''Tess''." Thomas Hardy Yearbook 35 (2005): 5–18.
* Leavis, L. R. "Marriage, Murder, and Morality: The Secret Agent and Tess." Neophilologus 80.1 (1996): 161–69.
* Lovesey, Oliver. "Reconstructing ''Tess''." SEL: Studies in English Literature, 1500–1900 43.4 (2003): 913–38.
* Poole, Adrian. "'Men's Words' and Hardy's Women." Essays in Criticism: A Quarterly Journal of Literary Criticism 31.4 (1981): 328–45.
* Tumanov, Vladimir.  [https://owl.uwo.ca/access/content/group/d9c3b137-1d5d-4026-9794-2079b0d9f6a8/Under%20the%20Hood%20of%20Tess.pdf “Under the Hood of Tess: Conflicting Reproductive Strategies in Thomas Hardy’s Tess of the D’Urbervilles.”] [http://link.springer.com/article/10.1007%2Fs11061-012-9302-8 Neophilologus] 97.1 (2013): 245-259.

== External links ==
{{Wikisource}}

* {{Gutenberg|no=110|name=Tess of the d'Urbervilles}}
* [http://www.doc.ic.ac.uk/~rac101/concord/texts/tess/ A hypertextual, self-referential, complete edition of ''Tess of the d'Urbervilles'']
* {{librivox book | title=Tess of the d'Urbervilles | author=Thomas Hardy}}

{{Thomas Hardy}}
{{Tess of the d'Urbervilles}}

{{DEFAULTSORT:Tess Of The D'urbervilles}}
[[Category:1891 novels]]
[[Category:Novels by Thomas Hardy]]
[[Category:19th-century British novels]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in The Graphic]]
[[Category:Victorian novels]]
[[Category:British novels adapted into films]]
[[Category:British novels adapted into plays]]
[[Category:Novels adapted into television programs]]