{{Update|date=December 2016}}

{{Infobox company
|name             = Carousell
|logo            = Carousell logo.png
|logo_caption = Carousell logo
|type = [[Private company|Private]]
|industry = [[E-commerce]]
|hq_location = [[Singapore]]
|area_served = [[Southeast Asia]], [[Taiwan]], United States
|founded = May 1, 2012
|key_people = Quek Siu Rui, Lucas Ngoo, Marcus Tan
|num_employees = 23
|homepage = https://carousell.com
}}

'''Carousell''' (旋轉拍賣) is a mobile and online [[consumer to consumer]] marketplace for buying and selling new and secondhand goods. It is headquartered in [[Singapore]]. Carousell operates within countries like [[Singapore]], [[Malaysia]], [[Indonesia]], [[Taiwan]], and the [[United States]].<ref name=Carousell2015 /> Carousell is currently available as a mobile app for both [[Apple Inc.|Apple]] and [[Android (operating system)|Android]] devices.<ref name=apple2015 />

==History==

Carousell was founded in [[Singapore]] on May 1, 2012, by co-founders Quek Siu Rui, Lucas Ngoo, and Marcus Tan.<ref name=JackyYap2014 /> The first item sold on Carousell was an [[Amazon Kindle]] e-reader for S$75.<ref name=JackyYap2012 />

In November 2013, [[Rakuten]], with follow-on investments by [[Golden Gate Ventures]], 500 Startups, and a few other investors invested $1 million in Carousell.<ref name=TechinAsia2014 /><ref name=TechinAsia2013 /> Subsequently, in November 2014, Carousell announced that it received US$6 million in investment from Sequoia India.<ref name=Todayonline2014 />

As of November 2014, over 2 million items had been sold on Carousell and users had created over 8 million listings of new and used items for sale.<ref name=JackyYap2014 />

Carousell has expanded regionally to three countries, including [[Malaysia]], [[Indonesia]], and [[Taiwan]].<ref name=ChanHoiCheong2014 /> However, Carousell's expansion into [[Taiwan]] faces competition against headstart companies like [[Yahoo!]] and Ruten, which are already settled in the e-commerce market.<ref name=JoshHorwitz2014 />

==Partnerships and collaborations==

In January 2013, Singapore Press Holdings' ST Classifieds announced its collaboration with Carousell.<ref name=STClassifieds />

According to a Yahoo Finance article in August 2013, Carousell announced its partnership with Singapore Press Holdings (SPH) Magazines, a subsidiary of SPH, to create a mobile app called SheShops Marketplace, selling fashion and beauty items.<ref name=asiaonenews2014 /><ref name=YahooFinance2013 />

==Operation==
Carousell is an online mobile platform for consumers to buy or sell items. It lists and sells a variety of products. Items are prohibited for sale on Carousell if it is illegal and violates the marketplace's community guidelines.<ref name=ProhibiteditemsonCarousell />

==Criticism==
In a Yahoo [[Tumblr]] page, images of users posting photos or screenshots of their bad experiences they encounter on Carousell relating to scam, fraud and harassment have circulated on social media platforms like [[Facebook]] and [[Tumblr]].<ref name=YahooTumblr /> Users of Carousell have also complained about cases of scams by fake sellers.{{Citation needed|date=December 2016}}

==References==
{{reflist|30em|refs=

<ref name=Carousell2015>{{cite web|title=Mobile Marketplace Carousell Raises $6M Series A Led By Sequoia Capital|url=http://techcrunch.com/2014/11/26/carousell-seriesa/|website=Techcrunch|accessdate=14 April 2015}}</ref>

<ref name=apple2015>{{cite web|title=Carousell|url=https://itunes.apple.com/sg/app/carousell-snap-to-sell-chat/id548607187?mt=8|website=iTunes|accessdate=14 April 2015}}</ref>

<ref name=JackyYap2014>{{cite web |first = Jacky |last = Yap |title = Singapore Marketplace App Carousell Saw 8 Times More Listings, Raises $6 Million In Investment! |url = https://vulcanpost.com/88971/singapore-darling-app-carousell-saw-8-times-listings-raises-6-million-investment/ |website = Vulcan Post |date = 27 November 2014 |accessdate = 18 December 2016 |archiveurl = http://www.webcitation.org/6mq54Vkkw?url=https://vulcanpost.com/88971/singapore-darling-app-carousell-saw-8-times-listings-raises-6-million-investment/ |archivedate = 18 December 2016 |deadurl = no }}</ref>

<ref name=JackyYap2012>{{cite web|title=Carousell makes e-commerce simpler with mobile marketplace|url=http://e27.co/just-snap-and-sell-with-carousell-the-mobile-marketplace-that-makes-e-commerce-simpler/|website=e27|accessdate=10 April 2015}}</ref>

<ref name=ChanHoiCheong2014>{{cite web|title=All Abroad the Carousell E-Commerce Expansion in Asia |url=http://www.establishmentpost.com/mobile-e-commerce-growth-fuels-carousell-expansion-asia/ |website=establishment post |accessdate=5 April 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20150406084752/http://www.establishmentpost.com/mobile-e-commerce-growth-fuels-carousell-expansion-asia/ |archivedate=6 April 2015 |df= }}</ref>

<ref name=JoshHorwitz2014>{{cite web|title=Carousell's entry into Taiwan presents big opportunities and big challenges|url=https://www.techinasia.com/carousells-taiwan-entry-means-islands-ecommerce-industry/|website=TechinAsia|accessdate=10 April 2015}}</ref>

<ref name=ProhibiteditemsonCarousell>{{cite web|title=Prohibited items on Carousell|url=http://help.carousell.com/article/167-prohibited-items-on-carousell|website=Carousell Help|accessdate=7 April 2015}}</ref>

<ref name=TechinAsia2013>{{cite web|title=Singaporean marketplace app Carousell snags $800K funding, sets sights on Malaysia and Indonesia|url=https://www.techinasia.com/marketplace-carousell-raises-800k-funding/|website=Techinasia|accessdate=14 April 2015}}</ref>

<ref name=TechinAsia2014>{{cite web|title=Singapore's Carousell raises $6M, gets Sequoia as new investor, squashes competition|url=https://www.techinasia.com/singapores-carousell-raises-6m-sequoia-capital-existing-investors/|website=techinasia|publisher=techinasia|accessdate=14 April 2015}}</ref>

<ref name=asiaonenews2014>{{cite web|title=Carousell partners with Blu Inc Media|url=http://news.asiaone.com/news/diva/carousell-partners-blu-inc-media|website=asiaone news|publisher=Straits Times|accessdate=14 April 2015}}</ref>

<ref name=Todayonline2014>{{cite web|last1=Today|first1=Online|title=Carousell secures S$7.79 million in funding|url=http://www.todayonline.com/tech/carousell-secures-s779-million-funding|website=Today Online|publisher=Today Papers|accessdate=14 April 2015}}</ref>

<ref name=STClassifieds>{{cite web|title=STClassifieds collaborates with Carousell to strengthen foothold with the Gen Y users|url=http://articles.stclassifieds.sg/gadgets-and-home-improvement/stclassifieds--collaborates-with-carousell-to-strengthen-foothold-with-the-gen-y-users/a/101189|website=ST Classifieds|accessdate=15 April 2015}}</ref>

<ref name=YahooFinance2013>{{cite web|title=Carousell partners with SPH Magazines, marries editorial and e-commerce|url=https://sg.finance.yahoo.com/news/carousell-partners-sph-magazines-marries-000039630.html|website=Yahoo! Finance|accessdate=15 April 2015}}</ref>

<ref name=YahooTumblr>{{cite web|title=‘Bro, still avail?’ Wacky conversations on the Carousell app compiled in Tumblr blog|url=http://yahoosg.tumblr.com/post/100226696540/bro-still-avail-wacky-conversations-on-the|website=Yahoosg! Tumblr|accessdate=15 April 2015}}</ref>

}}

==External links==
* {{Official website|https://carousell.com/}}

[[Category:E-commerce]]
[[Category:Classified advertising websites]]