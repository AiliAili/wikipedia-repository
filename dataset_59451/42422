<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Bo 207
 |image= File:Bolkow207.JPG
 |caption=
}}{{Infobox Aircraft Type
 |type=Civil utility aircraft
 |manufacturer=[[Bölkow]]
 |designer=
 |first flight=10 December [[1960 in aviation|1960]]<ref name="Janes 65 p66">Taylor 1965, p.66.</ref>
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=1961-1963
 |number built=92
 |unit cost=$13,700 in 1962 <ref>{{cite journal|magazine=Flying Magazine|date=May 1962|page=3}}</ref>
 |variants with their own articles=
}}
|}

The '''Bölkow Bo 207''' was a four-seat light aircraft built in [[Germany]] in the early 1960s, a development of the [[Klemm Kl 107]] built during [[World War II]].

==Design and development==
The Bo 207 is a wood construction, single-engined, cantilever low-wing cabin monoplane with a conventional landing gear with a tail wheel. The aircraft is powered by a [[Lycoming O-360]] four-cylinder, direct-drive, horizontally opposed, air-cooled, piston engine.

Bölkow had built the two and three-seat Kl 107 and developed a four-seat variant, at first designated the Kl 107D.<ref name="part2" /> The low-wing cabin monoplane had a re-designed cockpit and canopy and a larger tail.<ref name="part2" /> The two prototype Kl 107Ds were built at Nabern and the first flew on 10 October 1960.<ref name="part2" /> With the change of name of the company to Bolkow and the move to new factory at Laupheim production of the new variant was started at the new factory.<ref name="part2" /> In May 1961 the design was re-designated the Bolkow F.207 but by July 1961 it was re-designated again as the Bolkow BO 207.<ref name="part2" />

The first of 90 production aircraft built at Lauphein was flown on 24 May 1961 and production continued until 1963.<ref name="part2" /><ref name="part3" /> A number of aircraft remained unsold and the last aircraft although built in 1963 did not fly until 1 April 1966.<ref name="part3" /> One aircraft was built with tricycle [[landing gear]], and designated BO 217B or sometimes known as the BO 214 but it was fitted with a [[conventional landing gear]] before it was sold.<ref name="part3" />

==Variants==
;Kl 107D
:Two prototypes later re-designated F.207 and then BO 207. V-1 prototype modified as the BO 207T.<ref name="part2" />
;BO 207
:Production aircraft, 90 built.<ref name="part2" /><ref name="part3" />
;BO 207B
:One 207 temporary modified with a tricycle landing gear, also known as the BO 214.<ref name="part3" />
;BO 207T
:Prototype V-1 modified as a trainer with reduced weight.<ref name="part2" />

==Specifications (Bo 207) ==
{{aerospecs
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->met
|ref=''Jane's All The World's Aircraft 1965-66''.<ref name="Janes 65 p66-7">Taylor 1965, pp. 66–67.</ref>
|crew=One pilot
|capacity=3 passengers
|length m=8.30
|length ft=27
|length in=2¾
|span m=10.81
|span ft=35
|span in=5½
|dia m=<!-- helicopters -->
|dia ft=<!-- helicopters -->
|dia in=<!-- helicopters -->
|height m=2.25
|height ft=7
|height in=4½
|wing area sqm=15.40
|wing area sqft=166
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|aspect ratio=7.6:1<!-- sailplanes -->
|empty weight kg=715
|empty weight lb=1,575
|gross weight kg=1,200
|gross weight lb=2,645
|eng1 number=1
|eng1 type=[[Lycoming O-360]] four-cylinder [[piston engine]]
|eng1 kw=<!-- prop engines -->135
|eng1 hp=<!-- prop engines -->180
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=255
|max speed mph=159
|max speed mach=<!-- for supersonic aircraft -->
|cruise speed kmh=200<!-- if max speed unknown -->
|cruise speed mph=124<!-- if max speed unknown -->
|range km=1,250
|range miles=775
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=4,300
|ceiling ft=14,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=3.6
|climb rate ftmin=700
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
*[[Klemm Kl 107]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}

==References==
{{commons category|Bölkow Bo 207}}
{{reflist|refs=
<ref name="part2">{{cite journal |last= |first= |authorlink=  |coauthors= |year=1990 |month= |title=The Bolkow Lightplanes Part Two|journal=Archive |volume=1990 |issue=2 |pages=49–50|publisher=[[Air-Britain]]|ISSN=0262-4923}}</ref>
<ref name="part3">{{cite journal |last= |first= |authorlink=  |coauthors= |year=1990 |month= |title=The Bolkow Lightplanes Part Three |journal=Archive |volume=1990 |issue=3 |pages=73–76|publisher=[[Air-Britain]]|ISSN=0262-4923}}</ref>
}}

*{{cite book|last=Taylor|first=John W. R.|authorlink=John W. R. Taylor |coauthors= |title= Jane's All The World's Aircraft 1965-66 |year=1965 |publisher=Samson Low, Marston |location= London|isbn= |page= |pages= |url= |accessdate=}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=192 }}
* {{cite book |last= Simpson |first= R. W. |title=Airlife's General Aviation |year=1995 |publisher=Airlife Publishing |location=Shrewsbury |pages=85 }}

<!-- ==External links== -->
{{MBB aircraft}}

{{DEFAULTSORT:Bolkow Bo 207}}
[[Category:German civil utility aircraft 1960–1969]]
[[Category:Low-wing aircraft]]
[[Category:Bölkow aircraft|Bo 207]]
[[Category:Single-engine aircraft]]