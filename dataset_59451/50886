{{Infobox journal
| title = Chemometrics and Intelligent Laboratory Systems
| cover = 
| editor = R. Tauler
| discipline = [[Chemometrics]]
| abbreviation = Chemometr. Intell. Lab.
| publisher = [[Elsevier]] 
| country =
| frequency = 10/year
| history = 1986-present
| openaccess =
| license = 
| impact = 2.321
| impact-year = 2014
| website = http://www.elsevier.com/locate/issn/01697439
| link1 = http://www.sciencedirect.com/science/journal/01697439
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 13352494
| LCCN = 
| CODEN = 
| ISSN = 0169-7439 
| eISSN =
}}
'''''Chemometrics and Intelligent Laboratory Systems''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] sponsored by the [[Chemometrics Society]] and published since 1986 by [[Elsevier]]. The current [[editor-in-chief]] is R. Tauler (Barcelona, Spain).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Analytical Abstracts]], [[CSA (database company)|Cambridge Scientific Abstracts]], [[Chemical Abstracts Service|Chemical Abstracts]], [[Current Contents]], [[Current Index to Statistics]], [[EMBASE]], [[Inspec]], [[Science Citation Index]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal's [[impact factor]] is 2.321, ranking it 15th out of 58 journals in the category "Automation & Control Systems", 29th out of 74 in the category "Chemistry, Analytical", 26th out of 123 in the category "Computer Science, Artificial Intelligence", 9th out of 56 in the category "Instruments & Instrumentation", 12th out of 99 in the category "Mathematics, Interdisciplinary Applications" and 8th out of 122 journals in the category "Statistics & Probability".

== Most cited articles ==
According to the [[Web of Science]], the following three articles have been cited most often (>600 times):<ref name="WoS">{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2010 |accessdate=2011-01-19}}</ref>
# {{Cite journal | last1 = Wold | first1 = S. | doi = 10.1016/0169-7439(87)80084-9 | title = Principal component analysis | journal = Chemometrics and Intelligent Laboratory Systems | volume = 2 | pages = 37 | year = 1987 | pmid =  | pmc = }}
# {{Cite journal | last1 = Wold | first1 = S. | doi = 10.1016/S0169-7439(01)00155-1 | title = PLS-regression: a basic tool of chemometrics | journal = Chemometrics and Intelligent Laboratory Systems | volume = 58 | pages = 109–622 | year = 2001 | pmid =  | pmc = }}
# {{Cite journal | last1 = Bro | first1 = R. | doi = 10.1016/S0169-7439(97)00032-4 | title = PARAFAC. Tutorial and applications | journal = Chemometrics and Intelligent Laboratory Systems | volume = 38 | pages = 149–201 | year = 1997 | pmid =  | pmc = }}

==References==
{{reflist}}

{{Statistics journals}}

[[Category:Chemistry journals]]
[[Category:Statistics journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1986]]
[[Category:English-language journals]]