{{Infobox Organization
|name = Royal Statistical Society
|abbreviation = RSS
|motto =
|image = RSS_strapline logo.jpg
|formation = 1834
|extinction =
|type = Professional body, learned society, charity
|status = Non-profit company
|purpose = A world where data are at the heart of understanding and decision making
|headquarters = 12 Errol Street, [[St Luke's, Islington|St Luke's]], EC1Y 8LX
|region_served = UK and worldwide
|membership = British and worldwide statisticians and data professionals
|language =
|leader_title = Executive Director
|leader_name = Hetan Shah
|main_organ = RSS Council (President: [[Peter Diggle|Peter J Diggle]])
|parent_organization =
|affiliations = [[American Statistical Association]]
|num_staff =
|num_volunteers =
|budget =
|website = {{URL|http://www.rss.org.uk}}
|remarks =
}}

The '''Royal Statistical Society''' ('''RSS''') is a British [[learned society]] for [[statistics]], a [[professional body]] for [[statistician]]s, and a [[Charitable organization|charity]] which promotes statistics for the public good.

== History ==
[[File:Statistical Society of London - 1837 logo.png|thumb|right|The early logo of the Statistical Society of London with the motto ''Aliis exterendum'']]
[[File:Statistical_society_of_London_logo.jpg|thumb|Later logo]]
The society was founded in 1834 as the Statistical Society of London, though a perhaps unrelated London Statistical Society was in existence at least as early as 1824.<ref>[https://books.google.com/books?id=qC9HPSFSqvMC&printsec=frontcover&source=gbs_navlinks_s#v=onepage&q=&f=false Statistical Illustrations ... of the British Empire], London Statistical Society, Third Edition, 1827</ref><ref>{{cite journal | last1 = Willcox | first1 = WF | year = 1934 | title = Note on the Chronology of Statistical Societies | url = | journal = [[Journal of the American Statistical Association]] | volume = 29 | issue = | pages = 418–420 | doi=10.2307/2278614}}</ref> At that time there were many provincial statistics societies throughout Britain, but most have not survived. The [[Manchester Statistical Society]] (which is older than the LSS) is a notable exception. The associations were formed with the object of gathering information about society.<ref>{{cite journal |last=Hilts |first=V. L. |year=1978 |title=''Aliis Exterendum'', or the Origins of the Statistical Society of London |journal=[[Isis (journal)|Isis]] |volume=69 |issue=1 |pages=21–43 |jstor=230606 |doi=10.1086/351931}}</ref> The idea of statistics referred more to political knowledge rather than a series of methods. The members called themselves "statists" and the original aim was "...procuring, arranging and publishing facts to illustrate the condition and prospects of society" and the idea of interpreting data, or having opinions, was explicitly excluded. The original logo had the motto ''Aliis Exterendum'' (for others to thresh out, i.e. interpreted) but this separation was found to be a hindrance and the motto was dropped in later logos.<ref name=hilts>{{cite journal|title=Aliis exterendum, or, the Origins of the Statistical Society of London| author=Hilts, Victor L. | journal=Isis| volume=69| issue=1| year=1978| pages=21–43| doi=10.1086/351931}}</ref> It was many decades before mathematics was regarded as part of the statistical project.<ref>Aldrich, J. (2010) [http://www.jehps.net/juin2010/Aldrich.pdf "Mathematics in the London/Royal Statistical Society 1834-1934"], [http://www.jehps.net/juin2010.html ''Electronic Journ@l for History of Probability and Statistics''], 6, (1).</ref>

=== Key figures ===
Instrumental in founding the LSS were [[Richard Jones (economist)|Richard Jones]], [[Charles Babbage]], [[Adolphe Quetelet]], [[William Whewell]], and [[Thomas Malthus]]. Among its famous members was [[Florence Nightingale]], who was the society's first female member in 1858. [[Stella Cunliffe]] was the first female president. Other notable RSS presidents have included [[William Beveridge]], [[Ronald Fisher]], [[Harold Wilson]], and [[David Cox (statistician)|David Cox]] (see also the [[list of presidents of the Royal Statistical Society]]).

=== Royal Charter ===
The LSS became the RSS (Royal Statistical Society) by [[Royal Charter]] in 1887, and merged with the [[Institute of Statisticians]] in 1993. Today the society has 7,200 members around the world, of whom some 1,500 are [[British professional qualifications|professionally qualified]], with the status of [[Chartered Statistician]] (CStat). In January 2009, the RSS received Licensed Body status from the UK Science Council to award Chartered Scientist status. Since February 2009 the society has awarded Chartered Scientist status to suitably qualified members.

Unusually among [[professional body|professional societies]], all members of the RSS are known as "[[Fellow#Learned or professional societies|Fellows]]"—fellowship is nowadays not usually used as a [[Post-nominal letters|post-nominal]] mark of distinction. However, before the 1993 merger with the [[Institute of Statisticians]], Fellows did often use the post-nominal letters FSS. The merger enabled the society to take on the role of a professional body as well as that of a learned society; use of the unearned FSS qualification was viewed as inappropriate<ref>Professional membership pages on the RSS website:
http://www.rss.org.uk/site/cms/contentChapterView.asp?Chapter=11 and http://www.rss.org.uk/site/cms/contentviewarticle.asp?article=495</ref> and strongly discouraged, and it became less common.

== Structure ==
The RSS has premises (including offices and meeting rooms) in Errol Street, EC1, in the [[London Borough of Islington]] close to the boundary with the [[City of London]], between [[Old Street station|Old Street]] and [[Barbican station]]s.

The society has various local groups in the UK, together with a wide range of topic-related sections and study groups. Each of these sections and groups organizes lectures and seminars on statistical topics.

== Functions ==
The society was particularly engaged with the passage of the [[Statistics and Registration Service Act 2007]], having long argued for legislation on statistics.

=== Events ===
The RSS organises an annual conference. Among the society's awards are the [[Guy Medal]]s in gold, silver and bronze, in honour of [[William Guy]].

The RSS team reached the finals of ''[[University Challenge]]: The Professionals 2006'', where they were beaten 230 to 125 by a team from the [[Bodleian Library]], [[Oxford]].

=== Publications ===
[[Image:SigCover.jpg|thumb|140px|right|''Significance'' magazine]]
The society publishes the ''[[Journal of the Royal Statistical Society]]'', which currently consists of three separate series of journals whose contents include papers presented at ordinary meetings of the society, namely Series A (''Statistics in Society''), Series B (''Statistical Methodology'') and Series C (''Applied Statistics''), as well as a [[Popular science|general audience]] magazine called ''[[Significance (magazine)|Significance]]'' published in conjunction with the [[American Statistical Association]]. In September 2013, the society established ''StatsLife'', an online magazine website that features news, interviews and opinion from the world of statistics and data.

== See also ==
{{Commons category|Royal Statistical Society}}
* [[Council for the Mathematical Sciences]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.rss.org.uk|name = Royal Statistical Society website}}
* {{Official website|http://www.statslife.org.uk|name = RSS StatsLife website}}
* {{Official website|https://twitter.com/RoyalStatSoc|name = RSS on Twitter}}
* [http://www-gap.dcs.st-and.ac.uk/~history/Societies/Royal_Statistical.html MacTutor: The Royal Statistical Society]
* [http://www.scholarly-societies.org/history/1834rss.html Scholarly Societies Project: RSS]

=== Video clips ===
* {{YouTube|user=RoyalStatSoc|title=RoyalStatSoc}}

[[Category:Organizations established in 1834]]
[[Category:Statistical organisations in the United Kingdom]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Professional associations based in the United Kingdom]]
[[Category:London Borough of Islington]]
[[Category:Royal Statistical Society| ]]
[[Category:1834 establishments in the United Kingdom]]
[[Category:Organisations based in London]]