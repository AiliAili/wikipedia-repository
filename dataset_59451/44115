{{Infobox airport
| name = Ankara Güvercinlik Army Air Base
| nativename = Ankara Güvercinlik Kara Hava Üssü
| image-width = 
| caption = 
| IATA = 
| ICAO = LTAB
|pushpin_map= Turkey
|pushpin_mapsize=230
|pushpin_mark=Airplane_silhouette.svg|pushpin_label=Ankara Güvercinlik AAB| pushpin_map_caption = Location of airport in Turkey
| type = Military/civil
| owner = [[Turkish Army]]
| operator = {{small|[[Turkish General Staff|General Staff]] controlled units<br>[[General Command of Mapping (Turkey)|General Command of Mapping]]<br>Army Aviation School<br>1st Army Aviation Regiment<br>Ankara Gendarmerie Aviation Group}}
| city-served = [[Ankara]], [[Turkey]]
| location = Güvercinlik, [[Etimesgut]], Ankara
| built = 1933
| used = 1933-1955 civil<br>1958-2002 military<br>2002-present military/civil
| elevation-f = 2694
| elevation-m = 821 
|coordinates = {{coord|39|56|05.82|N|032|44|26.79|E|display=inline,title}}
| website = 
| metric-elev = y
| metric-rwy = y
| r1-number = 06/24
| r1-length-f = 6,635
| r1-length-m = 2,022
| r1-surface = [[Asphalt]]
| footnotes =Source: [[DAFIF]]<ref name="wad">{{WAD|LTAB|source=[[DAFIF]]}}</ref>
}}

'''Ankara Güvercinlik Army Air Base''', ({{lang-tr|Ankara Güvercinlik Kara Hava Üssü}}) {{airport codes||LTAB}} is a military airport of the [[Turkish Army]] located in Güvercinlik of [[Etimesgut]] district, {{convert|10|km|mi|abbr=on}} west of [[Ankara]] in central [[Turkey]].

The air base hosts [[Turkish General Staff|General Staff]] controlled units, [[General Command of Mapping (Turkey)|General Command of Mapping]]'s aviation unit, Army Aviation School, 1st Army Aviation Regiment and Ankara Gendarmerie Aviation Group. The airport is open to [[general aviation]] for civil domestic flights with permission.

==History==
With the foundation of the Turkish State Airlines Enterprise ({{lang-tr|Devlet Hava Yolları İşletmesi}}) (DHY), the predecessor of the [[Turkish Airlines]] (THY), in 1933, regular domestic passenger flights started the same year between Ankara and [[Istanbul]] via a stopover in [[Eskişehir]]. The airport in Güvercinlik became the first airport of Ankara.<ref>{{cite web |url=http://www.airkule.com/default.asp?page=yazar&id=176 |publisher=Airkule.com |title= Türkiye'de İç Hat Uçuşları |author= Baykal, Osman Gazi |language=Turkish |accessdate=2010-09-25 }}</ref>

In 1935, a training and a maintenance service center were established by the [[Turkish Aeronautical Association]] ({{lang-tr|Türk Hava Kurumu}}) at the airport. The center, called [[Turkish Aeronautical Association#Türkkuşu Training Center|Türkkuşu]] (for "Türkish Bird"), carried out revision services to the DHY in two hangars until its relocation to the nearby airport in [[Etimesgut]] in 1945.<ref>{{cite web |url=http://www.thy.com/en-INT/skylife/article.aspx?mkl=800 |publisher=Yurkish Technic |title=History-Aircraft Maintenance and Repair in Turkey |accessdate=2010-09-25 }}</ref>

On February 13, 1947, the first international passenger flight of the DHY departed from the Güvercinlik Airport for [[Athens]] via Istanbul.<ref>{{cite web |url=http://wowturkey.com/forum/viewtopic.php?p=246622 |title=Dünden Bugüne Türk Hava Yolları-İki Özdeş İsim, Türk Hava Yolları ve DC-3 |author=Altıngöz, Firuz |date=2006-10-15 |language=Turkish |accessdate=2010-09-25 }}</ref><ref>{{cite web |url=http://www.slideshare.net/anergiz/trk-hava-yollar-tarihi |publisher=SlideShare Inc. |title=Türk Havayolları Tarihi 1933-2007 |language=Turkish |accessdate=2010-09-26 | archiveurl= https://web.archive.org/web/20100914225859/http://www.slideshare.net/anergiz/trk-hava-yollar-tarihi| archivedate= 14 September 2010 <!--DASHBot-->| deadurl= no}}</ref>

Güvercinlik Airport served for the city of Ankara 22 years until 1955 when the civil flights were transferred to the newly completed [[Esenboğa Airport]].<ref>{{cite web |url=http://www.ankara.bel.tr/AbbSayfalari/ABB_Nazim_Plani/rapor/8-baglanti.pdf |publisher=Ankara Büyükşehir Belediyesi |title=Ankara Büyükşehir Belediyesi Nazım Projesi-8. Bağlantı Sektörleri-8.1 Ulaşım |language=Turkish |accessdate=2010-09-26 }}</ref> Maintenance facilities were relocated to [[Atatürk International Airport|Istanbul Yeşilköy Airport]] the same year.<ref name="hf">{{cite web |url=http://www.hayatforumda.com/ic-anadolu/219772-ankara-guvercinlik-havaalani.html |publisher=HayatForumda.com |title=Ankara Güvercinlik Havaalanı |language=Turkish |accessdate=2010-09-26 }}</ref>

The Army Aviation School (''Kara Havacılık Okulu''), which was established in 1948 at the Turkish Army's Artillery School in [[Polatlı]] to train pilots and flight equipment technicians for [[reconnaissance aircraft]] and [[helicopter]]s of the army and the gendarmerie, was deployed in 1958 to Güvercinlik Airport.<ref>{{cite web |url=http://www.facebook.com/group.php?gid=23070556320 |publisher=Facebook |title=315. Kd. Kara Havacılık Komutanlığı 1. Alay Güvercinlik/Ankara |language=Turkish |accessdate=2010-09-26 }}</ref>

Following the re-establishment of the aviation branch of the [[Turkish Navy]] in 1968, the first naval helicopter pilots were trained by the Army Aviation Command's flight school at the Güvercinlik Army Air Base.<ref>{{cite web |url=http://www.dzkk.tsk.tr/turkce/BunlariBiliyormuydunuz/DzHavaUsKLigi/dzhvuskomweb.htm |publisher=DzKK |title=Deniz Hava Üs Komutanlığı-Türk Deniz Havacılık Tarihi |language=Turkish |accessdate=2010-09-26 }}</ref>

==Legal status==
Currently, the air base is owned and operated by the Turkish Army. However, the military airport is open to general aviation for non-scheduled civil domestic flights only with permission according to a "Protocol on the Use of Military Airports by Civil Aviation" signed on July 22, 2002, between the Turkish General Staff and the [[Ministry of Transport (Turkey)|Ministry of Transport]]. Civil aircraft, foreign flagged or Turkish, may make use of the maintenance facilities at the base with permission. Aircraft with any foreigner crew member, however, need to obtain a special permission to use the facilities. No staying overnight is allowed at the airport.<ref name="hf"/>

==Units and equipment stationed==
Following units and their equipment are stationed at the air base:<ref>{{cite web |url=http://www.ole-nikolajsen.com/TURKISH%20FORCES%202004/TURKISH%20ARMY%202008.htm |publisher=Ole Nikolajsen |title=Turkish Army Aviation |date=2008-11-21 |accessdate=2010-09-26 }}</ref><ref>{{cite web |url=http://www.scramble.nl/tr.htm |publisher=Scramble on the Web|title=Turkish Air Force/Türk Hava Kuvvetleri-Ankara/Güvercinlik (LTAB) |accessdate=2010-09-26 | archiveurl= https://web.archive.org/web/20101001022410/http://www.scramble.nl/tr.htm| archivedate= 1 October 2010 <!--DASHBot-->| deadurl= no}}</ref>
*General Staff controlled units
**Special Aviation Group Command (''Özel Hava Grup Komutanlığı'')<br>[[Sikorsky UH-60 Black Hawk|S-70A]] helicopters and [[CASA CN-235|CN235-100M]] transporters
**GES Aviation Group Command (''GES Hava Grup Komutanlığı'')<br>[[CASA CN-235|CN235-100M]] transporters and [[Bell UH-1 Iroquois variants#UH-1H|UH-1H]], [[Bell 206#206L LongRanger|Bell 206L]] helicopters
*General Command of Mapping (''Harita Genel Komutanlığı'') 
**Mapping Aviation Group (''Harita Hava Grubu'')<br>[[Beechcraft Super King Air|Beech B200]] aircraft
*Army Aviation School Command (''K.K. Havacılık Okulu Komutanlığı'')
**Attack Helicopter Squadron (''Taarruz Helikopter Taburu'') 
***1st Flight (''1. Filo'')<br>[[Bell AH-1 Cobra|Bell AH-1]]
***2nd Flight (''2. Filo'')<br>[[Bell AH-1 Cobra#Single-engine|Bell AH-1P]], [[Bell AH-1 Cobra#Single-engine|Bell TAH-1P Trainer]]
**Helicopter Squadron (''Helikopter Taburu'') 
***1st Flight (''1.Bölük'')<br>[[Bell UH-1 Iroquois|Bell UH-1]]
***2nd Flight (''2.Bölük'')<br>[[Sikorsky UH-60 Black Hawk|S-70A]] 
**Air Transport Group (''Hava Ulaştırma Grubu'')<br>[[Beechcraft Super King Air|Beech B200]], [[Cessna 421#Variants|421C Golden Eagle/Executive Commuter]] aircraft and [[Bell UH-1 Iroquois variants#UH-1H|UH-1H]], [[Eurocopter AS 532#Variants|Eurocopter AS 532 UL Cougar]] helicopters
**Instruction Flight Command (''Kurs Bölük Komutanlığı'')
***Basic Flying Instruction-Rotating (''Temel Uçuş Hareketli'')<br>[[Bell 206|Augusta Bell 206R]] helicopters
***Instrumental Flight Instruction (''Aletli Uçus Eğitim'')<br>[[Cessna T-41 Mescalero#Variants|Cessna T-41D]] to be replaced by [[Cessna 182#Variants|Cessna T182T Skylane]], [[Beechcraft Baron#T-42A Cochise (95-B55B)|Beechcraft T-42A]] aircraft
*** Tactical Flying, Shooting and Instruction Departments (''Taktik Uçuş, Atış ve Eğitim Bölümleri'')<br>[[Bell 204/205#Bell 204|Agusta Bell AB 204B]], [[Bell UH-1 Iroquois variants#UH-1H|Bell UH-1H]], [[Bell OH-58 Kiowa#Variants|Bell OH-58A Kiowa]] helicopters and [[Cessna 185#Military variants|Cessna U-17B]] aircraft
**5th Main Maintenance Center (''5. Ana Bakım Merkezi'')
*1st Army Aviation Regiment (''1. Kara Havacılık Alayı'')
**Air Transport Group (''Hava Ulaştırma Grubu'')<br>[[Beriev Be-200]] [[amphibious aircraft]], [[Cessna 421#Variants|421C Golden Eagle/Executive Commuter]] aircraft and [[Bell UH-1 Iroquois variants#UH-1H|Bell UH-1H]], [[Eurocopter AS 532|Aerospatiale AS 532-VIP]], [[Eurocopter AS 532#Variants|Aerospatiale AS 532UL Cougar]] helicopters
*Ankara Gendarmerie Aviation Group Command (''Ankara Jandarma Hava Grup Komutanlığı'')
**Headquarter (''Karargah Kıtaatı'')<br>[[Cessna 182#Variants|Cessna 182P Skylane]] aircraft and [[Sikorsky UH-60 Black Hawk#Variants|S-70A-17]] helicopters
**1st Helicopter Flight (''1. Helikopter Filo'')<br>[[Bell 204/205#Variants|Augusta Bell AB 205]] and [[Sikorsky UH-60 Black Hawk#Variants|S-70A-17]] 
**2nd Helicopter Flight (''2. Helikopter Filo'')<br>[[Mil Mi-17#Variants|Mil Mi-17-1V]] (VIP, gunship and transport)

==1993 airplane crash==
[[List of the Chiefs of the Turkish Gendarmerie|Chief of the Gendarmerie]], Gen. [[Eşref Bitlis]] departed on February 17, 1993 from the Güvercinlik Air Base aboard a [[Beechcraft Super King Air|Beechcraft B200]] for an official trip. The aircraft crashed shortly after take-off. Bitlis, his aide-de-camp, the pilots and a technician were killed.<ref>{{cite web |url=http://turksiyer.com/onemli-olaylar/50-turk-tarihinde-unutulmayan-olaylar/1670-esref-bitlis-olayi.html |publisher=turksiyer.com |title=Eşref Bitlis Olayı |language=Turkish |accessdate=2010-09-26 }}</ref>

The pilot, who had VIP green card certification for excellence in flying, had switched the airplanes before the flight after having realized that the cockpit was not in order. The statement of the [[Chief of the General Staff (Turkey)|Chief of the General Staff]], Gen. [[Doğan Güreş]], that the accident on that snowy day was caused by [[atmospheric icing]] was denied by the crash investigators.<ref>{{cite news |url=http://www.yenisafak.com.tr/arsiv/2001/NISAN/08/g4.html |work=[[Yeni Şafak]]|title=Eşref Bitlis'in Ölümü-Buzlanma yok! |language=Turkish |accessdate=2010-09-26 | archiveurl= https://web.archive.org/web/20101016002349/http://www.yenisafak.com.tr/arsiv/2001/NISAN/08/g4.html| archivedate= 16 October 2010 <!--DASHBot-->| deadurl= no}}</ref>

==Other airports in Ankara==
* [[Esenboğa International Airport]]
* [[Akıncı Air Base]]
* [[Etimesgut Air Base]]

==References==
{{reflist}}

{{DEFAULTSORT:Ankara Guvercinlik Army Air Base}}
[[Category:Airports in Turkey]]
[[Category:Heliports in Turkey]]
[[Category:Military in Ankara|Guvercinlik Air Base]]
[[Category:Turkish Army air bases]]
[[Category:Transport in Ankara Province]]