{{Use dmy dates|date=June 2015}}
{{Use British English|date=June 2015}}
{{Infobox boxer
|name=Frank Buglioni
|image=Buglioni_(21_September_2013).jpg
|caption=
|alt=
|nickname=The Wise Guy
|weight=[[Super-middleweight]]<br />[[Light heavyweight]]
|height={{convert|6|ft|1|in|m|2|abbr=on}}<ref>{{cite web|url=http://www.boxnation.com/boxers/frank-buglioni/ |title=Frank Buglioni |website=Box Nation |date= |accessdate=22 August 2012}}</ref>
|reach={{convert|198|cm|in|0|abbr=on}}
|nationality=[[United Kingdom|British]]
|birth_date={{Birth date and age|1989|04|18|df=y}}
|birth_place=[[London Borough of Enfield|Enfield]], [[London]], [[United Kingdom]]
|style=[[Orthodox stance|Orthodox]]
|total=22
|wins=19
|KO=15
|losses=2
|draws=1
|no contests=0
|medaltemplates = 
 {{MedalCountry | {{GBR2}} }}
 {{MedalCompetition|Commonwealth Boxing Championships}}
 {{MedalSilver| India 2010 | Super-Middleweight}}
}}
'''Frank Buglioni''' (born 18 April 1989)<ref name="BN">"[http://www.boxnation.com/boxers/frank-buglioni/ Frank Buglioni]", ''Box Nation''. Retrieved 28 March 2016</ref> is a British professional boxer from London, England, and current holder of the British light heavyweight belt.

== Amateur career==
Buglioni took up boxing at the age of 14 to increase his fitness and strength, his priority at the time being football.<ref>{{cite web|url=http://www.boxingasylum.com/showthread.php?t=46942#.Ue1ldI1QFyQ|title=Prospect Profile: Frank Buglioni|website=boxingasylum.com}}</ref> Buglioni joined the Waltham Forest Amateur Boxing Club, and after winning his first four fights it was decided that he would move on to train at the Repton Boxing Club in [[Bethnal Green]] under the tutelage of head coach Tony Burns.<ref name="Maylett">Maylett, Chris (2012) "[http://www.britishboxers.co.uk/2012/11/in-ring-q-interview-with-frank-buglioni.html In the Ring Q&A Interview with Frank Buglioni]", ''britishboxers.co.uk'', 22 November 2012. Retrieved 28 March 2016</ref>

Buglioni went on to have 70 amateur fights, winning 60 and losing 10, with over half of his wins by knockout.<ref name="BN" /><ref>{{cite web|url=http://www.hattonboxing.com/tv/features/frank-buglioni-a-fighter-heading-in-the-right-direction |title=Frank Buglioni: A fighter heading in the right direction |website=Hattonboxing.com |date=14 March 2012 |accessdate=22 August 2012}}</ref> He won two national titles and also picked up various medals, whilst representing his country in international tournaments, including a silver medal against the world number one [[Vijender Singh]] in the Commonwealth Federations Tournament in Delhi in 2010.<ref>{{cite web|url=http://news.boxrec.com/news/2011/frank-buglioni-interview-%E2%80%93-explosive-knockout-bonfire-night-and-beyond |title=Frank Buglioni interview – explosive knockout on bonfire night and beyond |website=News.boxrec.com |date=14 November 2011 |accessdate=22 August 2012}}</ref>

Buglioni was selected to train with the Great Britain Olympic team in 2010. However, the following year he decided to turn professional.

==Professional career==
Nicknamed "The Wise Guy", Buglioni made his professional debut in November 2011, stopping Sabie Montieth in the first round. He won his first 10 fights before stopping [[Stepan Horvath]] in the eighth round in November 2013 to win the [[WBO]] European super middleweight title.<ref name="BN" /> He successfully defended the title three months later against [[Gaetano Nespro]], but lost it in April 2014 when he was stopped by [[Sergey Khomitsky]].<ref name="BN" />

Three months later he stopped Sam Couzens in four rounds to win the vacant [[BBBofC]] Southern Area super middleweight title.<ref name="BN" /> In November he regained the WBO European title with a unanimous decision over the previously unbeaten Andrew Robinson.<ref name="BN" /> He made two further defences (a first round stoppage of Ivan Jukic and a draw with [[Lee Markham]]) before beating [[Fernando Castaneda]] in July 2015 to take the vacant [[World Boxing Association|WBA]] International super middleweight title.

In September 2015 he challenged for [[Fedor Chudinov]]'s WBA World super middleweight title at [[Wembley Arena]];<ref name="Davies">Davies, Gareth (2015) "[http://www.telegraph.co.uk/sport/othersports/boxing/11876682/Frank-Buglioni-uses-hypnotist-Steve-Collins-employed-to-defeat-Chris-Eubank-ahead-of-Fedor-Chudinov-world-title-challenge.html Frank Buglioni uses hypnotist Steve Collins employed to defeat Chris Eubank ahead of Fedor Chudinov world title challenge]", ''[[Daily Telegraph]]'', 19 September 2015. Retrieved 28 March 2016</ref> The fight went the full 12 rounds with Chudinov winning comfortably on points.<ref name="BN" /><ref>"[http://www.msn.com/en-gb/sport/boxing/frank-buglioni-beaten-by-fedor-chudinov-in-london/ar-AAePGwQ Frank Buglioni beaten by Fedor Chudinov in London]", ''msn.com'', 27 September 2015. Retrieved 28 March 2016<br>- Davies, Gareth A. (2015) "[http://www.telegraph.co.uk/sport/othersports/boxing/11894309/Frank-Buglioni-loses-tilt-at-World-Boxing-Association-super-middleweight-title-against-Fedor-Chudinov.html Frank Buglioni loses tilt at World Boxing Association super middleweight title against Fedor Chudinov]", ''[[Daily Telegraph]]'', 27 September 2015. Retrieved 28 March 2016</ref>

In 2016, Buglioni moved up to [[light heavyweight]], stopping journeyman Olegs Fedotovs in the first round in March.<ref>Robinson, Isaac (2016) "[http://www.skysports.com/boxing/news/12183/10198202/frank-buglioni-is-moving-up-to-light-heavyweight Frank Buglioni is moving up to light-heavyweight]", ''[[Sky Sports]]'', 9 March 2016. Retrieved 28 March 2016</ref><ref>"[http://ringtv.craveonline.com/news/416469-hughie-fury-shuts-out-dominick-guinn-over-10 Hughie Fury shuts out Dominick Guinn over 10]", ''The Ring'', 26 March 2016. Retrieved 28 March 2016</ref>

Buglioni was trained by Mark Tibbs at the TRAD TKO Boxing Gym in [[Canning Town]], before moving on to work with the Irish boxing brothers Paschal Collins and former world champion [[Steve Collins]] at the Celtic Warrior Boxing Gym in [[Dublin, Ireland]].<ref name="BN" /><ref name="Davies" /><ref>Holmes, Jason (2013) "[http://www.huffingtonpost.co.uk/jason-holmes/boxing-exclusive-frank-buglioni_b_2579562.html Exclusive: Frank Buglioni, a Rising Star in the Boxing Firmament]", ''[[Huffington Post]]'', 30 January 2013. Retrieved 28 March 2016</ref> In 2016, he moved on to new trainer Don Charles.<ref>Stafford, Dominique (2016) "[http://www.barnet-today.co.uk/article.cfm?id=109086&headline=Buglioni%20excited%20by%20his%20step%20up%20to%20light-heavweight&sectionIs=sport&searchyear=2016 Buglioni excited by his step up to light-heavweight]", ''Barnet & Whetstone Press'', 13 March 2016. Retrieved 28 March 2016</ref>

== Personal life ==
Buglioni has Italian heritage and can trace his roots to [[Naples]]. His great grandfather emigrated to London in the 1920s. He was born and raised in [[London Borough of Enfield|Enfield]], [[London]] where he still resides between training camps.

After achieving his International Baccalaureate Diploma from [[Highlands School, Grange Park|Highlands School]], Buglioni attended the [[University of Westminster]] to study building surveying. He chose to put his education on hold when he was selected for the GB Olympic team. Buglioni has said he would like to go back and complete his degree.<ref>{{cite web|author=Fella Tio says: |url=http://www.boxingnews24.com/2012/02/frank-buglioni-qa/ |title=Frank Buglioni Q&A |website=Boxingnews24.com |date=6 February 2012 |accessdate=22 August 2012}}</ref>

Buglioni's boxing heroes are [[Oscar De La Hoya]] and [[Arturo Gatti|Arturo "Thunder" Gatti]].<ref>{{cite web|url=http://www.frankwarren.com/news/interviews/frank-buglioni-q-a.html |title=Frank Buglioni Q&A &#124|website=Frankwarren.com |date=7 February 2012 |accessdate=22 August 2012}}</ref>

Before Buglioni took up boxing full-time, he worked as a building surveyor.<ref name="Davies2">Davies (2015) "[http://www.telegraph.co.uk/sport/othersports/boxing/11893006/Frank-Buglioni-faces-tough-task-against-Fedor-Chudinov-to-lift-supermiddleweight-crown.html Frank Buglioni faces tough task against Fedor Chudinov to lift supermiddleweight crown]", ''[[Daily Telegraph]]'', 26 September 2015. Retrieved 28 March 2016</ref> He supports London football team [[Chelsea F.C.]].<ref>{{cite web|author=Gary Smith/Frank Buglioni |url=http://boxtacular.com/2012/01/30/frank-buglioni-interview-by-gary-smith/ |title=Frank Buglioni Interview (by Gary Smith) |website=Boxtacular |date=30 January 2012 |accessdate=22 August 2012}}</ref>

== Professional boxing record ==
{{S-start}}
| style="text-align:center;" colspan="8"|'''19 Wins''' (15 knockouts, 4 decisions), '''2 Loss''' (1 knockout), '''1 Draw'''
|- style="text-align:center; background:#e3e3e3;"
| style="border-style:none none solid solid; "|'''Result'''
| style="border-style:none none solid solid; "|'''Record'''
| style="border-style:none none solid solid; "|'''Opponent'''
| style="border-style:none none solid solid; "|'''Type'''
| style="border-style:none none solid solid; "|'''Round, Time'''
| style="border-style:none none solid solid; "|'''Date'''
| style="border-style:none none solid solid; "|'''Location'''
| style="border-style:none none solid solid; "|'''Notes'''
|- align=center
|{{yes2}}Win
|19-2-1
|align=left|{{flagicon|UK}} [[Hosea Burton]]
|{{small|TKO}}
|{{small|12 (12), 1:56}}
|{{small|10 Dec 2016}}
|align=left|{{small|{{flagicon|UK}} [[Manchester Arena]], [[Manchester]]}}
|align=left|{{small|Won [[British Boxing Board of Control|British]] [[Light Heavyweight]] title.}}
|- align=center
|{{yes2}}Win
|18-2-1
|align=left|{{flagicon|LAT}} Olegs Fedotovs
|{{small|TKO}}
|{{small|1 (6), 1:40}}
|{{small|26 Mar 2016}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|
|- align=center
|{{no2}}Loss
|17-2-1
|align=left|{{flagicon|RUS}} [[Fedor Chudinov]]
|{{small|UD}}
|{{small|12}}
|{{small|26 Sep 2015}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|{{small|For [[World Boxing Association|WBA (Regular)]] [[Super Middleweight]] title.}}
|- align=center
|{{yes2}}Win
|17-1-1
|align=left|{{flagicon|MEX}} [[Fernando Castañeda]]
|{{small|KO}}
|{{small|5 (12), 2:26}}
|{{small|24 Jul 2015}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|{{small|Won vacant [[World Boxing Association|WBA]] Inter-Continental [[Super Middleweight]] title.}}
|- align=center
|style="background:#abcdef;"|Draw
|16-1-1
|align=left|{{flagicon|UK}} Lee Markham
|{{small|SD}}
|{{small|10}}
|{{small|9 May 2015}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|{{small|Retained WBO European super middleweight title}}
|- align=center
|{{yes2}}Win||16-1||align=left|{{flagicon|CRO|}} Ivan Jukic
|{{small|TKO}}||{{small|1 (10)}}||{{small|28 Feb 2015}}
|align=left|{{small|{{flagicon|GBR}} [[The O2 Arena|O2 Arena]], [[Greenwich]], [[London]]}}
|align=left|{{small|Retained WBO European super middleweight title}}
|- align=center
|{{yes2}}Win||15-1||align=left|{{flagicon|GBR|}} Andrew Robinson
|{{small|UD}}||{{small|10}}||{{small| 29 Nov 2014}}
|align=left|{{small| {{Flagicon|GBR}} [[ExCeL London|ExCeL Arena]], [[Newham]], [[London]]}}
|align=left|{{small|Won vacant WBO European super middleweight title}}
|- align=center
|{{yes2}}Win||14-1||align=left|{{flagicon|BUL|}} Alexey Ribchev
|{{small|TKO}}||{{small|6 (10)}}||{{small|20 Sep 2014}}
|align=left|{{small|{{flagicon|GBR}} [[York Hall]], [[Bethnal Green]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||13-1||align=left|{{flagicon|GBR|}} Sam Couzens
|{{small|TKO}}||{{small|4 (10)}}||{{small|16 Jul 2014}}
|align=left|{{small|{{flagicon|GBR}} [[York Hall]], [[Bethnal Green]], [[London]]}}
|align=left|{{small|Won vacant BBBofC Southern Area super middleweight title}}
|- align=center
|{{no2}}Loss||12-1||align=left|{{flagicon|Belarus|}} [[Sergey Khomitsky]]
|{{small|TKO}}||{{small|6 (12)}}||{{small|12 Apr 2014}}
|align=left|{{small|{{flagicon|GBR}} [[Copper Box]], [[Hackney Wick]], [[London]]}}
|align=left|{{small|Lost WBO European super middleweight title.}}
|- align=center
|{{yes2}}Win||12-0||align=left|{{flagicon|ITA}} Gaetano Nespro
|{{small|TKO}}||{{small|5 (12), 2:24}}||{{small|15 Feb 2014}}
|align=left|{{small|{{flagicon|GBR}} [[Copper Box]], [[Hackney Wick]], [[London]]}}
|align=left|{{small|Retained WBO European super middleweight title.}}
|- align=center
|{{yes2}}Win||11-0||align=left|{{flagicon|CZE}} Stepan Horvath
|{{small|KO}}||{{small|8 (12), 2:53}}||{{small|30 Nov 2013}}
|align=left|{{small|{{flagicon|GBR}} [[Copper Box]], [[Hackney Wick]], [[London]]}}
|align=left|{{small|Won vacant WBO European super middleweight title.}}
|- align=center
|{{yes2}}Win||10-0||align=left|{{flagicon|CZE}} Broislav Kubin
|{{small|TKO}}||{{small|2 (10), 1:19}}||{{small|21 Sep 2013}}
|align=left|{{small|{{flagicon|GBR}} [[Copper Box]], [[Hackney Wick]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||9-0||align=left|{{flagicon|LIT}} Kirill Psonko
|{{small|UD}}||{{small|8}}||{{small|20 Jul 2013}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||8-0||align=left|{{flagicon|GBR}} Darren McKenna
|{{small|TKO}}||{{small|3 (8), 1:36}}||{{small|20 Apr 2013}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|
|-align=center
|{{yes2}}Win||7-0||align=left|{{flagicon|IRE}} Ciaran Healy
|{{small|RTD}}||{{small|2 (6), 3:00}}||{{small|15 Dec 2012}}
|align=left|{{small| {{Flagicon|GBR}} [[ExCeL London|ExCeL Arena]], [[Newham]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||6-0||align=left|{{flagicon|UK}} Joe Rea
|{{small|TKO}}||{{small|2 (6), 1:24}}||{{small|14 Sep 2012}}
|align=left|{{small|{{flagicon|GBR}} [[York Hall]], [[Bethnal Green]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||5-0||align=left|{{flagicon|GBR}} Jody Meikle
|{{small|UD}}||{{small|6}}||{{small|1 Jun 2012}}
|align=left|{{small|{{flagicon|GBR}} [[York Hall]], [[Bethnal Green]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||4-0||align=left|{{flagicon|BUL}} Daniel Borisov
|{{small|TKO}}||{{small|1 (6), 2:48}}||{{small|28 Apr 2012}}
|align=left|{{small|{{flagicon|GBR}} [[Royal Albert Hall]], [[Kensington]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||3-0||align=left|{{flagicon|GBR}} Ryan Clark
|{{small|UD}}||{{small|4}}||{{small|10 Feb 2012}}
|align=left|{{small|{{flagicon|GBR}} [[York Hall]], [[Bethnal Green]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||2-0||align=left|{{flagicon|GBR}} Paul Morby
|{{small|TKO}}||{{small|1 (4), 2:37}}||{{small|18 Nov 2011}}
|align=left|{{small|{{flagicon|GBR}} [[York Hall]], [[Bethnal Green]], [[London]]}}
|align=left|
|- align=center
|{{yes2}}Win||1-0||align=left|{{flagicon|GBR}} Sabie Montieth
|{{small|TKO}}||{{small|1 (4), 2:04}}||{{small|5 Nov 2011}}
|align=left|{{small|{{flagicon|GBR}} [[Wembley Arena]], [[London Borough of Brent|Brent]], [[London]]}}
|align=left|{{small|Professional debut.}}
{{S-end}}

==References==
{{Reflist|30em}}

== External links==
*{{Official website|www.Frank-Buglioni.com}} {{plays audio}}
*{{Boxrec|id=587892}}

{{DEFAULTSORT:Buglioni, Frank}}
[[Category:1989 births]]
[[Category:Living people]]
[[Category:English male boxers]]
[[Category:Super-middleweight boxers]]
[[Category:Italian British sportspeople]]
[[Category:British people of Italian descent]]