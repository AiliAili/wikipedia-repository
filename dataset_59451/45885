{{Other uses|Cuatro (disambiguation){{!}}Cuatro}}
{{Infobox film
| name           = ¡Cuatro!
| image          = Cuatro film poster.jpg
| image_size     = 215px
| caption        = Official promotional poster
| director       = Tim Wheeler
| producer       = Tim Lynch
| starring       = [[Billie Joe Armstrong]]<br />[[Mike Dirnt]]<br />[[Tré Cool]]<br />[[Jason White (musician)|Jason White]]<br />[[Rob Cavallo]]
| cinematography = Greg Schneider<br />Alex Kopps<br />[[Chris Dugan]]
| editing        = Tim Wheeler
| studio         = Farm League
| distributor    = [[Reprise Records]]
| released       = September 24, 2013
| runtime        = 105 minutes
| country        = United States
| budget         = 
}}

'''''¡Cuatro!''''' is a 2013 [[rockumentary]] starring the punk rock band [[Green Day]], directed by Tim Wheeler. The film documents the creation of the band's 2012 album trilogy ''[[¡Uno!]]'', ''[[¡Dos!]]'' and ''[[¡Tré!]]''.<ref>Gallo, Phil (March 17, 2013). [http://www.billboard.com/articles/news/1552424/green-day-and-the-creative-process-story-of-two-films-broadway-idiot-and "Green Day and the Creative Process: Story of Two Films, 'Broadway Idiot' and '¡Cuatro!'"]. Billboard. Accessed January 2014.</ref><ref>{{cite web|url=http://www.nme.com/news/green-day/67327/ |title=Green Day announce plans for '¡Quatro!' release |publisher=[[NME]] |date= |accessdate=2013-12-13}}</ref> The documentary, directed by Tim Wheeler and produced by Tim Lynch (who had previously produced Green Day's ''[[Bullet in a Bible]]'' in 2005), was released through [[Reprise Records]] on the September 24, 2013.<ref name="iTunes - Movies - ¡Cuatro! - Apple">{{cite web|url=https://itunes.apple.com/us/movie/id686893778/ |title=iTunes - Movies - ¡Cuatro! - Apple |publisher=Apple |date= |accessdate=2013-12-12}}</ref><ref>{{cite web|url=http://www.imdb.com/title/tt0485773/fullcredits?ref_=tt_ov_st_sm |title=Green Day: Bullet in a Bible |publisher=[[IMDb]] |date= |accessdate=2013-12-13}}</ref> A 40-minute version of the documentary premiered on [[VH1]] in 2012. The documentary contains footage of Green Day's producer [[Rob Cavallo]] and Green Day's days composing and organizing the trilogy until their release. ''¡Cuatro!'' was nominated for a [[Grammy Award]] for Best Music Film.<ref>{{cite web|url=http://www.grammy.com/nominees/ |title=56th Annual GRAMMY Awards Nominees |publisher=[[Grammy]] |date= |accessdate=2013-12-12}}</ref>

== Content ==
''¡Cuatro!'' contains footage of Green Day in the studio composing, organizing, practicing and discussing the albums, as well as speaking on their approach to recording an album trilogy. The documentary also contains live footage of Green Day performing their new songs at small club shows across the United States in 2011 and 2012.

== Production ==
When Green Day entered the studio to record the trilogy, multiple clips were posted on the band's [[YouTube]] channel of them creating the albums. [[Mike Dirnt]], bassist of Green Day said that "Over the last year while we were recording our trilogy, we posted multiple clips every week so fans can see parts of the recording process of ''¡Uno! ¡Dos! ¡Tré!''. ''¡Quatro!'' brings our fans one step closer by giving them even more access and revealing what it was like for us to make these records."<ref name="Spin magazine">{{cite web|url=http://www.spin.com/articles/green-day-quatro-documentary/ |title=American Vidiots: Green Day Announce 'Quatro' Documentary |publisher=[[Spin (magazine)]] |date= |accessdate=2013-12-12}}</ref> Lead guitarist and vocalist of Green Day, [[Billie Joe Armstrong]] told [[Billboard (magazine)|Billboard]] that their aim for ''¡Cuatro!'' as a documentary was "not going to be the sitting down, head shot of me going, 'We started out blah blah blah'. "We wanted to get into lifestyles of rock 'n' roll and playing rock 'n' roll and letting the story kind of tell itself rather than create revisionist (history)."<ref>{{cite web|url=http://www.billboard.com/articles/news/481867/exclusive-green-day-documentaries-coming-with-album-trilogy |title=Exclusive: Green Day Documentaries Coming with Album Trilogy |publisher=[[Billboard (magazine)]] |date= |accessdate=2013-12-12}}</ref> Green Day drummer [[Tré Cool]] told [[BBC Breakfast]] that the purpose of the documentary was to show fans what it's like to make a record by giving them an inside look at the process, saying that "People ask what is it like to make a record. It's like someone asking what a sausage tastes like, you can describe it - or you can just hand someone a sausage."<ref>{{cite web|url=https://www.youtube.com/watch?v=6ahoXyQiqOw#t=2m06s/ |title=Green Day on BBC Breakfast - 08/23/2012 |publisher=[[BBC]] |date= |accessdate=2013-12-13}}</ref>

== Release and reception ==
A documentary about the making of ''¡Uno! ¡Dos! ¡Tré!'' was announced July 9, 2012, ''¡Cuatro!'', which at the time was titled ''¡Quatro!'', was later confirmed on November 21, 2012. A 40-minute version of the documentary premiered a week after on [[VH1]] on November 28, 2012, and subsequently aired on [[MTV2]] and [[Palladia]].<ref name="Spin magazine"/><ref>{{cite web|url=http://www.greenday.com/news/press-release-104236 |title=PRESS RELEASE |publisher=GreenDay.com |date= |accessdate=2013-12-12}}</ref> The full documentary made its debut showing at the [[Winter X Games XVII]] in January 2013, and was later screened again at [[SXSW]] in March 2013.<ref>{{cite web|url=http://www.rollingstone.com/music/news/green-day-documentary-to-premiere-at-x-games-20130122 |title=Green Day Documentary to Premiere at X Games |publisher=[[Rolling Stone]] |date= |accessdate=2013-12-13}}</ref><ref>{{cite web|url=http://www.rollingstone.com/music/news/sxsw-2013-green-day-launch-broadway-idiot-and-cuatro-documentaries-20130316 |title=SXSW 2013: Green Day Launch 'Broadway Idiot' and 'Cuatro!' Documentaries |publisher=[[Rolling Stone]] |date= |accessdate=2013-12-13}}</ref> ''¡Cuatro!'' was originally released exclusively to ''¡Uno!'', ''¡Dos!'', ''¡Tre!'' box set purchasers on April 8, 2013 and was later released bundled with ''[[¡Tre!]]'' on August 26, 2013.<ref>{{cite web|url=http://www.greendayauthority.com/news/4010/ |title=Green Day's trilogy documentary, ¡Cuatro!, is out today for box set purchasers |publisher=Green Day Authority |date= |accessdate=2013-12-13}}</ref><ref>{{cite web|url=http://www.fuse.tv/2013/09/green-day-cuatro-release-date |title=Green Day's 'Cuatro' Rock Doc Gets Release Date |publisher=[[Fuse TV]] |date= |accessdate=2013-12-13}}</ref> The documentary was released by itself on DVD and digitally on September 24, 2013.<ref name="iTunes - Movies - ¡Cuatro! - Apple"/>

''¡Cuatro!'' received generally positive reviews upon release. [[Crackle (company)|Crackle]] gave the film a score 4 out of 5.<ref>{{cite web|url=http://www.crackle.com/c/green-day-cuatro |title=Green Day: Cuatro |publisher=Crackle |date= |accessdate=2013-12-12}}</ref> The film has received a user score of 8.4/10 on [[IMDb]].<ref>{{cite web|url=http://www.imdb.com/title/tt2553800/?ref_=ttfc_fc_tt |title=¡Cuatro! (2012) |publisher=[[IMDb]] |date= |accessdate=2013-12-12}}</ref> ''¡Cuatro!'' was also nominated for a [[Grammy Award]] for [[Grammy Award for Best Music Film|Best Music Film]].

{| class="wikitable sortable"
|-
! Chart (2013)
! Peak<br />position
|-
|Australia ([[Australian Recording Industry Association|ARIA]] Music DVDs)<ref name="ARIA Australian Top 40 Music Chart">{{Cite web |url=http://ariacharts.com.au/chart/music-dvds |archive-url=http://www.freezepage.com/1384676835WNQYHVPZTR?url=http://ariacharts.com.au/chart/music-dvds |dead-url=yes |archive-date=November 17, 2013 |title=ARIA Music DVDs Chart 18/11/2013 |publisher=[[ARIA Charts]] |accessdate=Dec 12, 2013}}</ref>
| style="text-align:center;"| 26
|-
|}

== Personnel ==
{{col-begin}}
{{col-2}}
*[[Billie Joe Armstrong]] – [[guitar]], [[singing|lead vocals]]
*[[Mike Dirnt]] – [[bass guitar]], backing vocals
*[[Tre Cool]] – [[Drum kit|drums]], backing vocals
*[[Jason White (musician)|Jason White]] – guitar, backing vocals
*[[Rob Cavallo]] - producer of ''[[¡Uno!]] [[¡Dos!]] [[¡Tré!]]''

'''Production'''
*Tim Lynch - producer
*Pat Magnerella - executive producer
*Tieneke Pavesic - associate producer
*Michael Pizzo - co-producer
*Devin Sarno - associate producer
{{col-2}}
'''Cinematography'''
*Joseph Aguirre
*[[Chris Dugan]]
*Alex Kopps
*Greg Schneider

'''Film editing'''
*Tim Wheeler

'''Sound department'''
*[[Chris Dugan]] - mixing
*Tim Hoogenakker - sound re-recording mixer
{{col-end}}

== References ==
{{reflist|30em}}

{{Green Day}}

{{DEFAULTSORT:Cuatro!}}
[[Category:2013 films]]
[[Category:2010s documentary films]]
[[Category:American documentary films]]
[[Category:American films]]
[[Category:Rockumentaries]]
[[Category:Green Day]]