{{Infobox journal
| cover = [[Image:Journal of the history of philosophy.gif]]
| discipline = [[Philosophy]], [[classics]], [[history]]
| language = English, French, German
| abbreviation = J. Hist. Philos.
| editor = [[Jack Zupko]]
| publisher = [[Johns Hopkins University Press]]
| country = United States
| history = 1963–present
| frequency = Quarterly
| website = https://sites.ualberta.ca/~jhphil/
| link1 = http://www.press.jhu.edu/journals/journal_of_the_history_of_philosophy/
| link1-name = Journal page at publisher's website
| link2 = http://muse.jhu.edu/journals/journal_of_the_history_of_philosophy/
| link2-name = Online access at [[Project MUSE]]
| ISSN = 0022-5053
| eISSN = 1538-4586
| OCLC  = 1783132
| LCCN = 65009310
}}
The '''''Journal of the History of Philosophy''''' is a quarterly [[peer-reviewed]] [[academic journal]]. It was established in 1963 after the Eastern Division of the [[American Philosophical Association]] passed a motion to this effect in 1957. The journal is published by the [[Johns Hopkins University Press]] and covers the history of [[Western philosophy]]. Time periods covered include everything from the [[Ancient history|ancient period]] to modern developments in the study of [[philosophy]]. The [[editor-in-chief]] is [[Jack Zupko]] ([[University of Alberta]]).

== External links ==
* {{Official website|http://philosophy.wisc.edu/jhp/}}

[[Category:Western philosophy]]
[[Category:History of philosophy journals]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:Quarterly journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1963]]


{{philo-journal-stub}}
{{history-journal-stub}}