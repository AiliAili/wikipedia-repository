{|{{Infobox Aircraft Begin
  |name = XC-123A
  |image = File:Chase XC-123A.jpg{{!}}border
  |caption =
}}{{Infobox Aircraft Type
  |type = [[Military transport aircraft]]
  |manufacturer = [[Chase Aircraft]]
  |designer = [[Michael Stroukoff]]
  |first flight = 21 April 1951
  |introduced = 
  |retired =
  |status = 
  |primary user = [[United States Air Force]]
  |more users =
  |produced =
  |number built = 1
  |unit cost =
  |program cost = 
  |developed from= [[Chase XCG-20]]
  |variants with their own articles =
  |developed into= 
}}{{Infobox Aircraft Career
|sole example of type?= Y
|other names          = Jet Avitruc
|construction number  = 
|construction date    = 
|military serial      = 47-787
|radio code           = 
|flights              = 
|total hours          = 
|total distance       = 
|fate                 = Converted to [[Fairchild C-123 Provider#Experimental projects|YC-123D]] 53-8068
|preservation         = 
}}
|}

The '''Chase XC-123A''' was an experimental [[military transport aircraft|transport aircraft]] developed by [[Chase Aircraft]]. The first jet-powered transport built for the [[United States Air Force]], it was intended for use as a high-speed transport for high-priority cargo and personnel. The XC-123A was determined to have insufficient advantages over existing types in service, and did not go into production. The sole prototype was converted into the piston-powered '''Stroukoff YC-123D''' to evaulate [[boundary layer control]] systems; following the conclusion of testing, it was sold onto the civilian market, and still exists, having been converted to turboprop power.

==Design and development==
In the late 1940s, Chase Aircraft had developed the [[Chase XCG-20|XG-20]], the largest [[military glider|glider]] ever built in the United States.<ref name="BS">Sergievsky et al. 1998, p.128</ref> By the time it was ready for operations, however, U.S. military doctrine had been altered to remove the requirement for the use of transport gliders in combat.<ref name="AAHS">Mitchell 1992, p.164.</ref>

However, the XG-20's aircraft had been designed to allow for the easy installation of powerplants, and Chase modified the two prototypes into powered aircraft, one becoming the [[Fairchild C-123 Provider|XC-123]], with twin piston engines.<ref>Adcock 1992, p.4.</ref> The second XG-20, however, was taken in hand for a more radical reconfiguration, being fitted with two twin-jet engine pods, of the type used by the [[Convair B-36]] and [[Boeing B-47]] bombers, to become the XC-123A.<ref name="AiA"/> As there was no provision for housing fuel in the former glider's wings, fuel tanks were installed underneath the cabin floor.<ref name="AiA"/>

==Operational history==
Dubbed "Avitruc" by its manufacturer,<ref>Air League 1975, p. 113.</ref> the XC-123A conducted its maiden flight on April 21, 1951,<ref name="AiA"/> becoming the first jet-powered transport aircraft to successfully fly in the United States.<ref name="AiA"/> It was considered "excellent" in flight trials, with the aircraft showing few vices,<ref name="BG"/> and demonstrating reasonably good short-field capability.<ref name="AiA"/>

Despite this, even as the XC-123 proved successful, the XC-123A failed to win sufficient favor in flight testing to receive a production order. Although the aircraft's short-field performance was good, on rough, unimproved fields the low-slung jet pods would suck [[Foreign object damage|debris]] into the intakes, damaging the engines.<ref name="AiA"/> In addition, the aircraft's design was mismatched to its engines,<ref>Sweetman 1979, p.97.</ref> resulting in the XC-123A being incapable of providing sufficient cargo capacity compared to the amount of fuel its jet engines required.<ref name="AAHS"/> As a result, the XC-123A project was abandoned without additional aircraft being built.<ref name="AAHS"/>

Following the conclusion of trials, the XC-123A was converted to be powered by two [[Pratt & Whitney R-2800]] radial engines, and was used for [[boundary layer control]] trials as the [[Fairchild C-123 Provider#Experimental projects|Stroukoff YC-123D]], receiving serial number 53-8068.<ref name="AiA"/><ref>Baugher 2010a</ref><ref name="Bau2">Baugher 2010b</ref>
<!-- ==YC-123D==
In 1954, the '''YC-123D''', formerly the XC-123A prototype, flew in its modified state after being converted by '''Stroukoff Aircraft'''. While the most obvious change from the original XC-123A was the switch of engines, the YC-123D also had a [[Boundary layer control]] (BLC) system fitted. This system directs air from the engines at high speed over the top of the wing, making the wing act as if the aircraft is flying at a much higher airspeed. As a result, the YC-123D had a greatly reduced take-off and landing distance. Compared to the C-123B, the YC-123D could land in 755 feet instead of 1,200, and take off with only 850&nbsp;ft of runway instead of 1,950, with a 50,000&nbsp;lb total weight.

==C-123T==
Following its military service, the YC-123D was retired and sold on the civilian market, currently owned by [[Provider Aviation]], which is re-converting the aircraft to turboprop power.<ref name="Bau2"/>-->

==Specifications (XC-123A)==
[[File:Chase XC-123A on runway.png|thumb|right|The XC-123A]]
{{Aircraft specs
|ref=Gunston<ref name="BG">Gunston (ed.) 1980</ref> and Adcock<ref name="AiA">Adcock 1992, p.7.</ref>
|prime units?=imp
|genhide=
|crew=3
|capacity=
|length m=
|length ft=77
|length in=1
|length note=
|span m=
|span ft=110
|span in=0
|span note=
|height m=
|height ft=33
|height in=10
|height note=
|wing area sqm=
|wing area sqft=1222.78
|wing area note=
|aspect ratio=
|airfoil=NACA 23017<ref>Lednicer 2010</ref>
|empty weight kg=
|empty weight lb=25000
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=60000
|max takeoff weight note=
|more general=
|eng1 number=4
|eng1 name=[[General Electric J47]]-GE-11
|eng1 type=[[turbojet]]s
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=5200
|eng1 note=
|thrust original=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=500
|max speed kts=
|max speed note=
|max speed mach=
|cruise speed kmh=
|cruise speed mph=400
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{Portal|Aviation|United States Air Force}}
{{aircontent|
|related=
* [[Chase XCG-20]]
* [[Fairchild C-123 Provider]]
* [[Stroukoff YC-134]]
|similar aircraft=
* [[Avro Ashton]]
* [[Avro Canada C102 Jetliner]]
* [[Vickers Type 618 Nene-Viking]]
|lists=
* [[List of military aircraft of the United States]]
|see also=
}}

==References==
;Notes
{{reflist|2}}
;Bibliography
{{refbegin}}
*{{cite book |last1=Adcock |first1=Al |title=C-123 Provider in action |series=Aircraft In Action |volume=124 |year=1992 |publisher=Squadron/Signal Publications |location=Carrollton, TX |isbn=978-0-89747-276-0}}
*{{cite journal |author=Air League |authorlink=Air League |year=1975 |title=Chase XC-123A |journal=Air Pictorial |volume=37 |page=113 |publisher=Air League of the British Empire |location=London}}
*{{cite web |url=http://www.joebaugher.com/usaf_serials/1946.html |title=1946-1948 USAAF Serial Numbers |first=Joe |last=Baugher |year=2010 |work=USAAS-USAAC-USAAF-USAF Aircraft Serial Numbers--1908 to Present |accessdate=2010-11-28}}
*{{cite web |url=http://www.joebaugher.com/usaf_serials/1953.html |title=1953 USAAF Serial Numbers |first=Joe |last=Baugher |year=2010 |work=USAAS-USAAC-USAAF-USAF Aircraft Serial Numbers--1908 to Present |accessdate=2011-01-22}}
*{{cite book |editor1-last=Gunston |editor1-first=Bill |editor-link=Bill Gunston |title=The Illustrated Encyclopedia of Commercial Aircraft |year=1980 |publisher=Exeter Books |location=New York |isbn=978-0-89673-077-9}}
*{{cite web |url=http://www.ae.illinois.edu/m-selig/ads/aircraft.html |title=The Incomplete Guide to Airfoil Usage |first=David |last=Lednicer |year=2010 |publisher=University of Illinois at Urbana-Champaign |accessdate=2010-11-27}}
*{{cite journal |last=Mitchell |first=Kent A. |year=1992 |title=The C-123 Provider |journal=AAHS Journal |volume=37 |publisher=[[American Aviation Historical Society]] |location=Santa Ana, CA |url=https://books.google.com/books?ei=GqnyTK3-O4aKlweYzdzWDA&ct=result&id=TkkpAQAAIAAJ&dq=%22XC-123A%22+Avitruc&q=%22transport+the+fuel%22#search_anchor |accessdate=2010-11-28}}
*{{cite book |last1=Sweetman |first1=William |authorlink=Bill Sweetman |title=A History of Passenger Aircraft |year=1979 |publisher=W.H. Smith/Hamlyn Publishing Group |location=London |isbn=978-0-600-37248-6}}
{{refend}}

==External links==
{{commons category|Chase XC-123A}}
* [https://books.google.com/books?id=0SADAAAAMBAJ&pg=PA81&dq=popular+science+1951+how+your+train+stops&hl=en&ei=k4S-TLmzA5WlngeQ7diJDg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CC8Q6AEwAA#v=onepage&q&f=true "Jet Power Troop Transport."] ''Popular Science,'' July 1951, bottom of page 81

{{Chase aircraft}}
{{USAF transports}}

{{DEFAULTSORT:Chase XC-123A}}
[[Category:Chase aircraft|C-123A Jet Avitruc]]
[[Category:United States military transport aircraft 1940–1949]]
[[Category:Quadjets]]
[[Category:High-wing aircraft]]