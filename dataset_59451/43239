{|{{Infobox aircraft begin
 |name            = Me P.1112
 |image           = MeP1112 3.JPG
 |size            = 300px
 |alt             =
 |caption         = Model of one Me P.1112/V1 design concept
 |long caption    =
}}{{Infobox aircraft type
 |type            = Jet fighter
 |national origin = [[Nazi Germany]]
 |manufacturer    =
 |builder         = [[Messerschmitt]]
 |designer        = [[Woldemar Voigt (aerospace engineer)|Woldemar Voigt]]<ref name="Mono52">{{harvnb|Griehl|1988|pp=24, 87}}</ref><ref name="Herwig 2003 174">{{harvnb|Herwig|Rode|2003|p=174}}</ref>{{vague|Other references give Waldemar as Voigt's first name.|date=January 2012}}
 |design group    =
 |first flight    =
 |introduced      =
 |introduction     =
 |retired         =
 |status          =
 |primary user    = [[Luftwaffe]]
 |more users      =
 |produced        =
 |number built    = 0
 |program cost    =
 |unit cost       =
 |developed from  =[[Messerschmitt P.1110]] and [[Messerschmitt P.1111]]
 |variants with their own articles =
 |developed into  =
}}
|}

The '''Messerschmitt P.1112''' was a proposed [[Germany|German]] [[jet fighter]], developed by [[Messerschmitt AG]] during the closing stages of [[World War II]], and intended for use by the [[Luftwaffe]]. The progress of the war prevented the completion of a prototype before the fall of [[Nazi Germany]]. Its design, however, had a direct influence on post-war US Navy carrier fighters.<ref name=Schick167/>

==Design and development==
The work on the Me P.1112 started on 25 February 1945 after [[Willy Messerschmitt]] decided to halt the development of the [[Messerschmitt P.1111]] that would have required as standard equipment a [[Cabin pressurization|pressurized cockpit]] and [[ejection seat]].<ref name="Mono82"/><ref name=herwig176>{{harvnb|Herwig|Rode|2003|p=176}}</ref> Designed by the head of the Messerschmitt Project Office [[Woldemar Voigt (aerospace engineer)|Woldemar Voigt]] (1907–1980), between 3 and 30 March 1945 as an alternative to the [[Messerschmitt P.1111|Me P.1111]], the Me P.1112 design was less radical than P.1111's and incorporated the lessons learned from the development of the [[Messerschmitt P.1110]] design.<ref name="Herwig 2003 174"/><ref name="Mono82">{{harvnb|Griehl|1988|p=82}}</ref><ref name="SP2"/> Voigt estimated that the Me P.1112 would commence flight testing by mid-1946.<ref name=Schick167/>

Intended to be powered by a single [[Heinkel HeS 011]] [[turbojet]], three design concepts of the Me P.1112 were developed.<ref name=herwig176/> The last proposed design was the Me P.1112/V1 using a [[V-tail]] design and fuselage lateral intakes; the two first were the Me P.1112 S/1, with wingroot air intakes, and the Me P.1112 S/2, with fuselage lateral intakes, both with a larger, single fin; both designs lacked conventional [[horizontal stabilizer]]s. All three had a fuselage maximum diameter of {{convert|1.1|m|3|ft|sigfig=2}}.<ref name=herwig177>{{harvnb|Herwig|Rode|2003|p=177}}</ref>  The aircraft's wing design was similar in appearance to that of Messerschmitt's [[Messerschmitt Me 163|Me 163 ''Komet'']] rocket fighter. The pilot was seated in a semi-reclined position, and was equipped with an [[ejection seat]].<ref name="SP2">{{harvnb|LePage|2009|pp=275–276}}</ref>

A partial mockup of the Me P.1112 V/1,<ref name=herwig178>{{harvnb|Herwig|Rode|2003|p=178}}</ref> consisting of the aircraft's forwards fuselage section, was constructed in the ''"Conrad von Hötzendorf"'' Kaserne at [[Oberammergau]], but the Messerschmitt facilities there were occupied by American troops in April 1945, before construction of the prototype could begin.<ref name="SP2"/><ref name="Mono87">{{harvnb|Griehl|1988|p=87}}</ref>

{{Gallery
| title = Mock-up of the Messerschmitt Me P.1112/V1 found by the US Army in April 1945.
| width = 250
| align = center
|File:P.1112 mockup 1945 front.jpg|Fuselage
|File:P.1112 mockup 1945 cockpit.jpg|Cockpit
}}

Although the Me P.1112 was never completed, follow-on designs were already proposed even as design work on the type itself was done. These included a proposed [[night fighter]] version, which was intended to be fitted with twin engines mounted in the [[wing root]]s of the aircraft.<ref name="SP2"/>

Following the war, Voigt's experience in [[tailless aircraft]] design was put to use by the [[Vought|Chance Vought company]]  in the [[United States]], where he was involved in the design of the [[F7U Cutlass]] fighter.<ref name="SP2"/>

==Specifications==
{{Aircraft specs
|ref=Schick,<ref name=Schick167>{{harvnb|Schick|Meyer|1997||p=167}}</ref> Herwig,<ref name=herwig17677>{{harvnb|Herwig|Rode|2003|pp=176–177}}</ref> LePage<ref name="SP2"/>
|prime units?=met
|plane or copter?=plane
|jet or prop?=jet
|crew=one (pilot)
|capacity=
|length m=8.25
|length ft=
|length in=
|length note= Version P 1112 S/1 of 27 March 1945{{#tag:ref|The P 1112/V1 design of 30 March
1945 had an overall length of {{convert|9.24|m|ftin|abbr=on}} and a span of {{convert|8.16|m|ftin|abbr=on}}.<ref name=herwig178/>|name=shick2|group=notes}}
|span m=8.74
|span ft=
|span in=
|span note=Version P 1112 S/1 of 27 March 1945<ref name=shick2 group=notes/>
|height m=2.84
|height ft=
|height in=
|height note=
|wing area sqm=19
|wing area sqft=
|wing area note=
|aspect ratio=3.5 : 1
|airfoil=
|empty weight kg=2290
|empty weight lb=
|empty weight note=
|gross weight kg=4673
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|1900|l|gal|abbr=on}} with provision for increasing fuel capacity to {{convert|2400|l|gal|abbr=on}}
|eng1 number=1
|eng1 name=[[Heinkel HeS 011]]A0
|eng1 type=[[turbojet]]
|eng1 kn=
|eng1 lbf=2700
|eng1 note=<ref group="notes">to be replaced later by a HeS 011B0 rated at 1,500 kP (3,306 lb) thrust</ref>
|thrust original=1300 kPa
|max speed kmh=1100
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=estimated at more than two hours at {{convert|7000|m|ft|abbr=on}} at 100% thrust
|ceiling m=14000
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=246
|wing loading lb/sqft=
|wing loading note=maximum
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight= 0.26 with HeS 011A
|more performance=
|armament= *Standard armament (proposed): 4x {{convert|30|mm|abbr=on}} [[MK 108 cannon]] or 2x 30mm MK 108 cannon and 2x {{convert|30|mm|abbr=on}} [[MK 103 cannon]]
*Special armament for anti-bomber role (proposed): 1x {{convert|50|mm|abbr=on}} [[MK 214 cannon]] (upper forward fuselage) for anti-bomber role<ref>{{cite web|title=Mauser Mk214 50mm Cannon|url=http://www.stormbirds.com/warbirds/tech_mk214.htm|work=Stormbirds.com: the Messerschmitt Me 262 at War|accessdate=1 January 2012|archiveurl=http://www.webcitation.org/64MiClXCZ|archivedate=1 January 2012}}</ref><ref group=notes>The long-barreled Mauser MK214 50mm cannon was developed in an effort to create the ultimate bomber-killers, capable of attacking from a long distance, protected from the defensive fire of the bombers. A MK 214 mounted on a [[Messerschmitt Me 262|Me 262]] was used only time against American bombers on 16 April 1945 by the special fighter unit [[Jagdverband 44]] ({{harvnb|Forsyth|2008|p=62}}; {{harvnb|Jenkins|1996|p=48}}). The MK 214 was planned to be mounted asymmetrically on the P1112 front, necessitating an asymmetric seating ({{harvnb|Griehl|1988|p=86}}).</ref>  or 1x {{convert|55|mm|abbr=on}} [[MK 112 cannon]] (lower forward fuselage)
*Maximum weapon load: {{convert|500|kg}}
|avionics=
}}

==See also==
{{aircontent|
|related=
* [[Messerschmitt Me P.1110]]
* [[Messerschmitt Me P.1111]]
|similar aircraft=
|sequence=
|lists=
*[[List of World War II military aircraft of Germany]]
|see also=
}}

==Notes==
{{Reflist|group=notes}}

==References==
;Citations
{{Reflist|2}}
;Bibliography
{{refbegin|2}}
*{{cite book|last=Griehl|first=Manfred|title=Jet Planes of the Third Reich: The Secret Projects|volume=1|year=1988|publisher=Monogram Aviation Publications|location=Boylston, Massachusetts|isbn=978-0-914144-36-6|ref=harv}}
*{{cite book|last=Forsyth|first=Robert|title=Jagdverband 44 : Squadron of Experten|year=2008|publisher=Osprey|location=Oxford|isbn=978-1-84603-294-3|url=https://books.google.com/books?id=1Bl5OfE78e8C&lpg=RA1-PT31&dq=%22mk%20214%22%20cannon&pg=RA1-PT31#v=onepage&q&f=false|accessdate=1 January 2012|ref=harv}}
* {{cite book|last=Jenkins|first=Dennis|title=Messerschmitt Me 262 Sturmvogel|year=1996|publisher=Specialty Press|location=North Branch, MN|isbn=0-933424-69-8|ref=harv}}
* {{cite book|last1=Herwig|first1=Dieter|title=Luftwaffe Secret Projects : Ground Attack & Special Purpose Aircraft|year=2003|publisher=Midland Publishing|location=Leicester |isbn=978-1-85780-150-7|last2=Rode|first2=Heinz|ref=harv}}
*{{cite book|last=LePage|first=Jean-Denis|title=Aircraft of the Luftwaffe 1935-1945|url=https://books.google.com/books?id=hdQBTcscxyQC&pg=PA275&dq=Messerschmitt+P.1112#v=onepage&q=Messerschmitt%20P.1112&f=false|year=2009|publisher=McFarland & Company|location=Jefferson, North Carolina|isbn=978-0-7864-3937-9|ref=harv}}
*{{cite book|last1=Schick|first1=Walter|title=Luftwaffe Secret Projects : Fighters 1939-1945|year=1997|publisher=Midland Publishing|location=Hinckley, England|isbn=978-1-85780-052-4|last2=Meyer|first2=Ingolf|ref=harv}}
{{refend}}

==External links==
{{Commons category|Messerschmitt P.1112}}
*{{cite web|title=Messerschmitt Me P.1112|url=http://www.luft46.com/mess/mep1112.html P.1112|work=Luft46.com|accessdate=1 January 2012|archiveurl=http://www.webcitation.org/64MljpZRp|archivedate=1 January 2012}}
* {{cite web|title=Picture of the Me 262A-1 a/U4 (Werk-Nr. 170083) equipped with a Rheinmetall-Mauser 50mm MK 214 cannon|url=http://the.secret.birds.free.fr/messerschmitt/Me262A-1-U4_avec_MK214.jpg}}. Source Air Force Museum Collection ({{harvnb|Jenkins|1996|p=48}})

{{Messerschmitt aircraft}}
{{Use dmy dates|date=January 2012}}

[[Category:Messerschmitt aircraft|P.1112]]
[[Category:Abandoned military aircraft projects of Germany]]
[[Category:V-tail aircraft]]
[[Category:Tailless aircraft]]
[[Category:Single-engined jet aircraft]]
[[Category:World War II jet aircraft of Germany]]