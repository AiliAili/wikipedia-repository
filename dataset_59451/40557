{{Orphan|date=February 2013}}

'''James Sandler''' is an American investigative journalist who was part of the [[New York Times]] team that won the 2004 [[Pulitzer Prize for Public Service]]. His work has appeared in the New York Times, the [[San Francisco Chronicle]], [[Salon.com|Salon]], on the [[CBS Evening News]] with [[Dan Rather]] and on [[PBS Frontline]].<ref name="mail.centerforinvestigativereporting">{{cite web|title=The Reporters: James Sandler|url=http://mail.centerforinvestigativereporting.org/reporters?profile=123|publisher=Center for Investigative Reporting|accessdate=14 September 2012}}</ref><ref name="legacy">{{cite web|title=2004 Atlanta Bios|url=http://legacy.ire.org/training/atlanta04/AtlantaBios.html|publisher=Investigative Reporters and Editors|accessdate=14 September 2012}}</ref><ref name="topics.nytimes">{{cite news|title=Dangerous Business|url=http://topics.nytimes.com/topics/news/national/series/dangerousbusiness/index.html|accessdate=14 September 2012|newspaper=The New York Times|date=January 2003}}</ref>

Sandler was born and raised in [[St. Louis, Missouri]] where he attended [[Ladue Horton Watkins High School]]. Sandler dropped out of high school at the age of 16 and received his GED. He eventually graduated from the [[University of Florida]] in [[Gainesville, Florida|Gainesville]] with a degree in psychology, and subsequently obtained a master’s degree in journalism from the [[University of California]] at [[Berkeley, California|Berkeley]].<ref name="mail.centerforinvestigativereporting" />

While at Berkeley, Sandler worked closely with [[Lowell Bergman]], a former [[60 Minutes]] producer who was portrayed by [[Al Pacino]] in the movie, [[The Insider (film)|The Insider]]. Following up on a tip provided by Bergman, Sandler and another student, Robin Stein, developed a story about deaths, injuries and environmental violations at a nationwide water and sewer pipe foundry. The New York Times hired Sandler and Stein to work with its reporters and continue their reporting.<ref name="sfweekly">{{cite news|last=Smith|first=Matt|title=Fun With Pipe|url=http://www.sfweekly.com/2003-02-05/news/fun-with-pipe/|accessdate=14 September 2012|newspaper=SF Weekly|date=5 February 2003}}</ref>

The story, called “Dangerous Business,” appeared as a three-day series on the front page of the New York Times in January 2003, and as a documentary on PBS Frontline.<ref name="pbs">{{cite web|title=Dangerous Business|url=http://www.pbs.org/wgbh/pages/frontline/shows/workplace/etc/nytimes.html|publisher=PBS|accessdate=14 September 2012}}</ref>  It won the 2004 Pulitzer Prize for Public Service, the 2004 [[Alfred I. duPont–Columbia University Award|Alfred I. duPont-Columbia University Award/Silver Baton]], the 2004 [[Goldsmith Prize for Investigative Reporting|Harvard University Goldsmith Prize for Investigative Reporting]], the 2003 [[Peabody Award|George Foster Peabody Award]], and the 2003 [[Investigative Reporters and Editors|Investigative Reporters and Editors Award for Investigative Network Television]].<ref name="pulitzer">{{cite web|title=The Pulitzer Prizes: 2004 Public Service|url=http://www.pulitzer.org/archives/6835|publisher=The Pulitzer Prizes|accessdate=14 September 2012}}</ref><ref name="pulitzer_a">{{cite web|title=David Barstow|url=http://www.pulitzer.org/biography/2009-Investigative-Reporting|work=The 2009 Pulitzer Prize Winners|publisher=The Pulitzer Prizes|accessdate=14 September 2012}}</ref>

Sandler left journalism in 2008 to pursue a career in health care. He earned a master’s degree in nursing from [[Vanderbilt University]] in [[Nashville, Tennessee]] and currently works as a [[Family Nurse Practitioner]].

==References==
{{reflist}}

*
*
*
*

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Sandler, James}}
[[Category:American male journalists]]
[[Category:Living people]]
[[Category:Writers from St. Louis]]