{{Infobox artist
| name          = Nina Evans Allender
| image         = Nina E. Allender.jpg
| caption       = ''Nina E. Allender,'' Harris & Ewing, photograph about 1915
| birth_name    = Nina Evans
| birth_date    = {{Birth date|1873|12|25}}
| birth_place   = [[Auburn, Kansas]]
| death_date    = {{Death date and age|1957|04|02|1873|12|25}}
| death_place   = [[Plainfield, New Jersey]]
| nationality   = American
| spouse        = Charles H. Allender (1893–1905, divorce)
| field         = Political cartoonist
| training      = {{plain list |
* [[Corcoran Gallery of Art|Corcoran Museum of Art]]
* [[Pennsylvania Academy of the Fine Arts]]
* [[Robert Henri]]'s summer painting tour of Italy
* [[Frank Brangwyn]], London
}}
| movement      = 
| works         = 
| awards        = 
| elected       = 
| bgcolour      = LightSteelBlue
}}
[[File:Nina E Allender at desk.jpg|thumb|Nina E. Allender at desk]]
'''Nina Evans Allender''' (December 25, 1873 – April 2, 1957) was an American artist, cartoonist, and women's rights activist.{{r|Plainfield 1957}} She studied art in the United States and Europe with [[William Merritt Chase]] and [[Robert Henri]]. Allender worked as an organizer, speaker, and campaigner for women's suffrage and was the "official cartoonist" for the [[National Woman's Party]]'s publications, creating what became known as the "Allender Girl."

== Personal life ==

=== Background ===
Nina Evans Allender was born on [[Christmas Day]], December 25, 1873 in [[Auburn, Kansas]].{{r|AAA 1916}}{{r|Wash Post 6 Apr 1957}}{{efn|Her year of birth is reported to be 1872{{r|Chapman}} or 1873. A passport for Nina Evans Allender (artist, born in Auburn, Kansas and living in Washington, D.C.) states that she was born on December 25, 1873.{{r|NEA Passport}}|name=dob}} 
Her father, David Evans was from Oneida County, New York and moved to Kansas and was a teacher there before becoming superintendent of schools.{{r|Sheppard}} His mother, Eva Moore,{{r|Sheppard}} was a teacher at a prairie school.{{r|Kay Boyle}}{{r|Evans-Moore 1871}} The Evans lived in Washington, D.C. by September 1881 when Eva Evans was working at the Department of the Interior as a clerk in the Land Office. She worked there until August 1902,{{r|Sheppard}}{{r|Interior}}{{r|Kay Boyle}} and she was one of the first women to be employed by the federal government.{{r|Wash Post 6 Apr 1957}} David Evans worked at the [[United States Department of the Navy]] as a clerk{{r|Evans Obit}} and was a poet{{r|DJ Evans 1893}}{{r|DJ Evans 1902}}{{r|DJ Evans 1904}} and short story writer.{{r|DJ Evans 1905}} He died on December 13, 1906 and was buried at [[Arlington National Cemetery]].{{r|Evans Obit}}

=== Marriage ===
In 1893, at the age of 19 Nina Evans married Charles H. Allender.{{r|Sewall Women}}{{r|Wedding 1893}}  Some years later Charles Allender reportedly took a sum of money from the bank where he worked and ran off with another woman.{{r|Kay Boyle}} Abandoned by her husband,{{r|Sewall Women}} Nina sued Charles for divorce in January 1905, alleging infidelity.{{r|Wash Post 1-1905}} Their divorce was granted that year.{{r|Wash Law Reporter 1905}}{{r|Wash Post divorce}}

=== Middle and later years ===
[[File:Allender Great Statues 1915.jpg|thumb|"Great Statues of History", 1915]]
About 1906 her portrait was painted with fellow artist [[Charles Sheeler]] by [[Morton Livingston Schamberg]]. It is in the collection of the Corcoran Gallery of Art in Washington, D.C.{{r|SI Portrait}} Following her years abroad studying art, Allender worked at the Treasury Department{{r|LOC}} and the Government Land Office in Washington, D.C.{{r|Sewall Women}} She lived in Washington, D.C. in 1916{{r|AAA 1916}} and maintained an art studio in New York City by 1917.{{r|Wash Post 4 Feb 1917}}

In 1942, Allender moved to [[Chicago]], [[Illinois]] where she remained for over a decade. In 1955 she moved to [[Plainfield, New Jersey]], where a niece, Mrs. Frank Detweiler (Joan) resided.{{r|Plainfield 1957}} She died at her niece's Plainfield house on April 2, 1957.{{r|NYT 1957}}{{r|Wash Post 6 Apr 1957}}{{r|Plainfield 1957}}

== Art and suffrage ==

=== Education and style ===
Allender enrolled in classes at the [[Corcoran Gallery of Art|Corcoran Museum of Art]]{{r|Sheppard}} and then studied under [[Robert Henri]] and [[William Merritt Chase]] at the [[Pennsylvania Academy of the Fine Arts]]{{r|AAA 1916}} from the spring of 1903 through 1907. She spent the summer of 1903 on a summer painting tour directed by Chase.{{r|Pach 1903}} Allender joined [[Robert Henri]]'s summer painting tour of Italy in 1905.{{r|Kay Boyle}}{{r|Doss 2005}} Allender considered William Merritt Chase{{r|Equal Rights 1923}} and Robert Henri as her mentors.{{r|Equal Rights 1923}} During one European study trip she became good friends with modernist painters [[Charles Sheeler]] and [[Morton Livingston Schamberg|Morton Schamberg]].{{r|Kay Boyle}} In London, she was a student of [[Frank Brangwyn]].{{r|AAA 1916}}{{r|Equal Rights 1923}}{{r|Art Annual 1918}} Allender's works in a Washington Society of Artists exhibit in 1909 were described as "some excellent little snow pictures".{{r|Studios 1909}}

=== Women's suffrage ===
[[File:Allender Spirit of 76 1915.jpg|thumb|"The Spirit of '76.' On to the Senate!", 1915]][[File:Allender Hat in the Ring 1916.jpg|thumb|"Our Hat in the Ring", 1916]]
At the age of 38, Nina Allender became actively involved in the [[National American Woman Suffrage Association]] (NAWSA).{{r|Wash Post 4 Feb 1917}} In 1912, Ohio held a referendum on woman suffrage, and Allender traveled there and enjoyed canvassing door to door and demonstrating with other suffragettes.{{r|Sheppard}} Allender had volunteered to assist NAWSA's Congressional Committee in planning their March 3, 1912 suffrage pageant in Washington.{{r|Parade 1912}} Allender was appointed chair of the committee on "outdoor meetings" as well as on "posters, post cards and colors."{{r|Famed Women 1913}} Within the year she became president of the District of Columbia Woman Suffrage Association and was a featured speaker at numerous local gatherings.{{r|Wheeling 30 Mar 1914}} In spring 1913, she was president of the Stanton Suffrage Club, which held "Suffrage as Relating to Business Women". Allender shared the speaker's platform with future congresswoman [[Jeannette Rankin]],{{r|League 1913}} one of about 14 women representing multiple states to meet with President [[Woodrow Wilson]] in a suffrage deputation.{{r|Deputation 1913}}

In 1913, Eva and Nina were recruited into the Congressional Union of NAWSA{{r|LOC}}, later the [[National Woman's Party]] by [[Alice Paul]]{{r|Sewall Women}}{{r|Walton}} when she was in Washington D.C. to lead the Congressional Committee of NAWSA.{{r|Irwin, 1921b}} [[Inez Haynes Irwin]] stated that both Eva and Nina had readily agreed to make monthly financial donations and volunteer their time for the organization.{{r|Irwin, 1921}}

In April 1914, Allender relocated temporarily to [[Wilmington, Delaware]] to head the Delaware Congressional Union for Equal Suffrage and to coordinate a parade on May 2.{{r|Wilmington 6 Apr}}{{r|Wilmington 7 Apr}} A year later she was on the advisory council of the national Congressional Union for Woman's Suffrage{{r|Council 1915}} and became chairman of the newly organized local branch of the Congressional Union.{{r|Excursion 1915}} In a press release on suffrage, Allender was identified as one of six "crack street orators" of the suffrage campaign.{{r|Women speakers 1915}} On December 9, 1915, she was slated to preside over a meeting of the state chairs and officers.{{r|Presides 1915}}

In 1916, Allender was an official delegate at the Chicago convention of the newly launched [[National Woman's Party]].{{r|Delegates 1916}} That fall, she was sent by the National Woman's Party to lobby in Wyoming for the federal amendment.{{r|chronology}}{{r|Suffragist}} When the National Woman's Party began picketing the White House to pressure [[Woodrow Wilson|President Wilson]], Allender joined the picket line{{r|Wash Post 4 Feb 1917}} and served as a delegate to a large suffrage parade.{{r|White House 1917}} The National Woman's Party sent valentines, designed by Allender, on February 14, 1917 to President Wilson and legislators as a softer appeal in the campaign to attain women's right to vote.{{r|Chapman}}

=== National Woman's Party cartoonist ===
[[File:Allender President Wilson says 1917.jpg|thumb|"President Wilson Says, "Godspeed to the Cause", 1917]]
[[File:Allender American Woman 1918.jpg|thumb|"American Woman: Is it not Enough?", 1918]]
Integral to the women's rights and suffrage campaigns were its newspapers.{{efn|The ''Woman's Journal'' was founded in 1870 for the American Woman Suffrage Association.{{r|Radcliffe}} When in 1890 the organization merged with the Anthony-Stanton National Woman Suffrage Association, the ''Woman's Journal'' became the news and propaganda outlet for the [[National American Woman Suffrage Association]].}} The Congressional Union under [[Alice Paul]] founded its own periodical, ''The Suffragist,'' in 1913.{{r|Irwin, 1921c}} Allender was the key artist for the publication{{r|LOC}} which featured political cartoons. The writers were Alice Paul and [[Rheta Childe Dorr]], the founding editor,{{r|Dorr 1924}} who came to Washington at the urging of Paul and [[Lucy Burns]], another suffrage leader.{{r|Dorr 1924b}} Allender, having been coaxed by Paul, found she had a talent for drawing cartoons and became ''The Suffragist's'' "official cartoonist".{{r|Dorr 1924}}{{r|Cartooning for Suffrage p. 8}} Her first political cartoon, which portrayed the campaign and women's need for the ballot, was published in the June 6, 1914 issue on heavy 10" x 13" paper.{{r|Cartooning for Suffrage p. 9}} The entire front page was subsequently occupied by a cartoon by Nina Allender. A 1918 review of her work conceded that her early period "dealt with old suffrage texts, still trying to prove that woman's place was no longer in the home."{{r|Cartooning for Suffrage p. 9}}

Early 20th-century American cartoons had enjoyed the Gibson Girl from [[Charles Dana Gibson]] and the Brinkley Girl from [[Nell Brinkley]].{{r|Cartooning for Suffrage p. 8}} Allender was credited with producing 287 political cartoons regarding suffrage.{{r|Newark}} Her depiction of the "Allender girl," captured the image of a young, capable American woman,{{r|political cartoonists 1920}} embodying "the new spirit that came into the suffrage movement when Alice Paul and Lucy Burns came to the National Capital in 1913."{{r|Cartooning for Suffrage p. 8}}

{{quotation|She gave to the American public in cartoons that have been widely copied and commented on, a new type of suffragist--the young and zealous women of a new generation determined to wait no longer for a just right. It was Mrs. Allender's cartoons more than any other one thing that in the newspapers of this country began to change the cartoonist's idea of the suffragist.|"The Suffragist' as a Publicity Medium". ''The Suffragist''{{r|Suffragist 2}}}}

Public image of a women's rights advocates changed through Allender's representation of the stylish, attractive, and dedicated young woman,{{r|LOC}} like the educated, modern, and freer [[New Woman]].{{r|Endres}}{{r|Prieto}} Other subjects in her cartoons were Congressmen, [[Uncle Sam]], and symbols for the woman suffrage amendment were used in the publication to promote the efforts of the National Women's Party and communicate women's rights movement events.{{r|LOC}}

[[File:Nina E. Allender, Amelia Himes Walker's Jailed for Freedom Pin, 1917.png|thumb|left|130px|''Amelia Himes Walker's "Jailed for Freedom" Pin,'' 1917]]
Allender designed the "Jailed for Freedom" pin, which was bestowed on women who were jailed beginning July 1917 for their campaigning and picketing activities. It was named Amelia Himes Walker's "Jailed for Freedom" pin in acknowledge the two-month period when the woman's rights activist was imprisoned in the [[Occoquan Workhouse]] and the incarceration and abuse that had been suffered by other suffragettes.{{r|SI Pin}}

The cover of September 1, 1920 issue of ''The Suffragist'' had Allender's ''Victory'' to symbolize the attainment of the right to vote.{{r|Sewall2}} The publication was produced weekly until 1921, was then succeeded in 1923 by ''Equal Rights'',{{r|Sewall2}} for which Allender also created political cartoons.{{r|LOC}} She continued to work for equal rights after women won the right to vote, including sitting on the NWP's council. She retired due to poor health in 1946.{{r|LOC}}

=== Art organizations ===

[[File:Nina E. Allender, Victory, September 1920, The Suffragist.jpg|thumb|"Victory", ''The Suffragist'', September 1, 1920]]
Nina Allender was a member of the following art organizations.
* [[Arts Club of Washington|Arts Club of Washington, D.C.]],{{r|Plainfield 1957}}{{r|Wardle 2005}} a founding member{{r|Plainfield 1957}}{{r|Wash Post 6 Apr 1957}}
* Art Students League of Washington{{r|Wardle 2005}} and was its corresponding secretary early in the century.{{r|Directory 1903}}
* Beaux Arts Club{{r|Beaux Arts 1918}}
* Society of Washington Artists{{r|AAA 1916}}{{r|Wardle 2005}}
* Washington Watercolor Club{{r|Wardle 2005}}

=== Exhibits ===

Works by Nina Allender have been exhibited at the following:
* Arts Club of Washington{{r|Plainfield 1957}}
* [[Cosmos Club]] of Washington{{r|Cosmos Club}}{{r|Wardle 2005}}
* [[National Academy Museum and School|National Academy of Design]]{{r|Wardle 2005}}
* [[Pennsylvania Academy of the Fine Arts]]{{r|Art Notes}}
* The Society of Washington Artists{{r|Wardle 2005}}
* [[Library of Congress]].{{r|Kay Boyle}}{{r|Wardle 2005}}
* Sewall-Belmont House and Museum{{r|Sewall}}

=== Legacy ===

Following the culmination of the suffrage crusade, Nina Allender remained active in the National Woman's Party in its work for gender equality, and remained on its council for another two decades.{{r|NYT 1940}} Her original drawings were initially housed in the [[Library of Congress]],{{r|Kay Boyle}} until reclaimed by the [[Sewall-Belmont House and Museum]], which was the headquarters for the [[National Woman's Party]]. Some were reprinted in collections.{{R|Bruere & Beard}}

To commemorate the 75th anniversary of the Nineteenth Amendment in 1995, the [[National Museum of Women in the Arts]] hosted an exhibition, "Artful Advocacy: Cartoons of the Woman Suffrage Movement."  Featured artists were Allender, [[Lou Rogers]], and [[Blanche Ames Ames|Blanche Ames]].{{r|advocacy1995}}{{r|Myers 1995}}

==See also==
*[[List of suffragists and suffragettes]]
*[[Timeline of women's suffrage]]
*[[List of suffragists and suffragettes#Major suffrage organizations|Women's suffrage organizations]]

== Notes ==
{{notelist}}

== References ==

{{Reflist|2|refs=

<ref name="AAA 1916">{{cite book|title=American Art Annual|url=https://books.google.com/books?id=G4sXAQAAIAAJ&pg=PA315|year=1916|publisher=MacMillan Company|page=315|chapter=Allender, Nina E.}}</ref>

<ref name=advocacy1995>{{cite news|last=Bass|first=Holly|title='Artful Advocacy: Cartoons From the Woman Suffrage Movement'|url=http://www.washingtoncitypaper.com/articles/7278/artful-advocacy-cartoons-from-the-woman-suffrage-movement|accessdate=19 Jan 2013|newspaper=Washington City  Paper|date=September 1, 1995}}</ref>

<ref name="Art Annual 1918">{{cite book|editor=Florence Levy|title=American Art Annual |volume=14|year=1918|publisher=American Federation of Arts|location=Washington, DC|page=415}}</ref>

<ref name="Art Notes">{{cite news|title=Art Notes|newspaper=Evening Star|date=December 1, 1906}}</ref>

<ref name="Beaux Arts 1918">{{cite news|title=Society|newspaper=Evening Star|date=March 18, 1918}}</ref>

<ref name="Bruere & Beard">{{cite book|last=Bruere|first=Martha Bensley & Mary Ritter Beard|title=Laughing their Way: Women's Humor in America|year=1934|publisher=MacMillan|location=New York|pages=295}}</ref>

<ref name="Cartooning for Suffrage p. 8">{{cite journal|title=Cartooning for Suffrage|journal=The Suffragist|date=March 2, 1918|volume=6|page=8}}</ref>

<ref name="Cartooning for Suffrage p. 9">{{cite journal|title=Cartooning for Suffrage|journal=The Suffragist|date=March 2, 1918|volume=6|page=9}}</ref>

<ref name="Chapman">{{cite book|author1=Mary Chapman|author2=Angela Mills|title=Treacherous Texts: U.S. Suffrage Literature, 1846-1946|url=https://books.google.com/books?id=rfOLWGUiLrgC&pg=PA241|year=2011|publisher=Rutgers University Press|isbn=978-0-8135-4959-0|pages=241–242|chapter=The President's Valentine: Nina E. Allender (1872-1957)}}</ref>

<ref name="Cosmos Club">{{cite news|last=Moser|first=James Henry|title=Art Topics|via=Proquest|newspaper=The Washington Post|date=February 23, 1902}}</ref>

<ref name="Council 1915">{{cite news|title=Suffragists at New York Meeting|newspaper=Evening Star|date=March 30, 1915}}</ref>

<ref name="chronology">{{cite web|title=Detailed Chronology National Woman’s Party History|url=http://memory.loc.gov/ammem/collections/suffrage/nwp/detchron.pdf|work=American Memory|publisher=Library of Congress|accessdate=June 16, 2013}}</ref>

<ref name="Delegates 1916">{{cite news|title=Convention Plans of DC Delegates|newspaper=Evening Star|date=May 28, 1916}}</ref>

<ref name="Deputation 1913">{{cite news|title=Women to See Wilson|url=https://query.nytimes.com/mem/archive-free/pdf?res=FB0C15FE345F13738DDDAE0894DA415B838DF1D3|accessdate=June 12, 2013|newspaper=The New York Times|date=December 7, 1913}}</ref>

<ref name="Directory 1903">{{cite book|title=City Directories for Washington DC|year=1903|publisher=William H Boyd|page=1064|url=http://www.fold3.com/image/78920871/}}</ref>

<ref name="DJ Evans 1893">{{cite news|title=A Memorial Poet|newspaper=Evening Star|date=March 4, 1893}}</ref>

<ref name="DJ Evans 1902">{{cite news|last=Evans|first=David James|title=Washington's Greeting to Her Gallant Guests|newspaper=Evening Star|date=October 7, 1902}}</ref>

<ref name="DJ Evans 1904">{{cite news|last=Evans|first=David|title=Contentment|newspaper=Washington Post|date=August 21, 1904}}</ref>

<ref name="DJ Evans 1905">{{cite news|last=Evans|first=David James|title=How He Threw Her Overboard|newspaper=''The Washington Post''|date=July 23, 1905}}</ref>

<ref name="Dorr 1924">{{cite book|last=Dorr|first=Rheta Childe|title=A Woman of Fifty|year=1924|publisher=Funk & Wagnalls|location=New York|page=288}}</ref>

<ref name="Dorr 1924b">{{cite book|last=Dorr|first=Rheta Childe|title=A Woman of Fifty|year=1924|publisher=Funk & Wagnalls|location=New York|pages=281–2}}</ref>

<ref name="Doss 2005">{{cite book|last=Doss|first=Erika|chapter=Complicating Modernism: Issues of Liberation and Constraint among the Women Art Students of Robert Henri | editor=Marian Wardle |title=American Women Modernists: The Legacy of Robert Henri, 1910-1945|year=2005|publisher=Rutgers University Press|location=Piscataway, NJ|isbn=0813536847}}</ref>

<ref name="Endres">{{cite book|author1=Kathleen L. Endres|author2=Therese L. Lueck|title=Women's Periodicals in the United States: Social and Political Issues|url=https://books.google.com/books?id=rHNlZkqY6w4C&pg=PA369|date=January 1, 1996|publisher=Greenwood Publishing Group|isbn=978-0-313-28632-2|page=369}}</ref>

<ref name="Evans-Moore 1871">{{cite news|title=D. J. Evans, Esq|newspaper=Topeka Daily Commonwealth|date=February 26, 1871}}</ref>

<ref name="Evans Obit">{{cite news|title=In and About: David J. Evans|newspaper=Washington Post|location=Washington, D.C.|page=11, column 2|date=December 16, 1906}}</ref>

<ref name="Equal Rights 1923">{{cite journal|title=Nina E. Allender|journal=Equal Rights|year=1923|volume=1|page=43}}</ref>

<ref name="Excursion 1915">{{cite news|title=Suffragists on Excursion|newspaper=Evening Star|date=June 23, 1915}}</ref>

<ref name="Famed Women 1913">{{cite news|title=Famed Women in Line|newspaper=The Washington Post|date=January 4, 1913}}</ref>

<ref name="Kay Boyle">{{cite book|last=Bell|first=Elizabeth S. (Ed.)|title=Words That Must Somehow Be Said; Selected Essays of Kay Boyle 1927-1984|year=1985|publisher=North Point Press|location=San Francisco|page=10}}</ref>

<ref name="Interior">{{cite book|author=United States. Department of the Interior|title=Register of the Department of the Interior: Containing Appointees of the President and of the Secretary of the Interior, 1877-1909|url=https://books.google.com/books?id=HDtGAQAAMAAJ&pg=PA55|year=1909|publisher=U.S. Government Printing Office|page=55}}</ref>

<ref name="Irwin, 1921">{{cite book|last=Irwin|first=Inez Haynes|title=the Story of the Woman's Party|year=1921|publisher=Harcourt, Brace and Co.|location=New York|page=19}}</ref>

<ref name="Irwin, 1921b">{{cite book|last=Irwin|first=Inez Haynes|title=the Story of the Woman's Party|year=1921|publisher=Harcourt, Brace and Co.|location=New York|page=18}}</ref>

<ref name="Irwin, 1921c">{{cite book|last=Irwin|first=Inez Haynes|title=the Story of the Woman's Party|year=1921|publisher=Harcourt, Brace and Co.|location=New York|page=46}}</ref>

<ref name="League 1913">{{cite news|title=Suffrage League Meets|url=http://www.genealogybank.com|accessdate=June 11, 2013|newspaper=Evening Star|date=April 18, 1913}}</ref>

<ref name="LOC">{{ cite web | url=http://www.loc.gov/collections/women-of-protest/articles-and-essays/selected-leaders-of-the-national-womans-party/propagandist/ | title=Propagandist: Nina Allender (1872-1957) | work=Women of Protest: Photographs from the Records of the National Woman's Party | publisher=Library of Congress | accessdate=April 20, 2015 }}</ref>

<ref name="Myers 1995">{{cite news|last=Myers|first=Laura|title=Cartoonists' Role in Suffrage Debate Focus of Exhibit|url=https://news.google.com/newspapers?id=tDpHAAAAIBAJ&sjid=bekMAAAAIBAJ&pg=5435%2C4338815|newspaper=The Daily Gazette|date=August 20, 1995}}</ref>

<ref name="NEA Passport">{{citation|title=Nina Evans Allender, Passport date June 16, 1903, Number 74314 | work=NARA Series: Passport Applications, 1795-1905; Roll #: 628; Volume #: Roll 628 - 11 Jun 1903-16 Jun 1903 |publisher=National Archives and Records Administration (NARA) | location= Washington D.C.}}</ref>

<ref name=Newark>{{cite news|title=She Changed Suffragists from Grim Old Maids to Pretty Young Girls|newspaper=The Newark Advocate|date=February 4, 1921}}</ref>

<ref name="NYT 1940">{{cite news|title='Whirlwind Drive' Mapped By Women; 'Finish the Job' Is Motto for Party's Move to Achieve Universal Equal Rights|url=https://select.nytimes.com/gst/abstract.html?res=FA0910F6345810738DDDAE0894DA415B8088F1D3|accessdate=June 12, 2013|newspaper=The New York Times|date=December 7, 1940}}</ref>

<ref name="NYT 1957">{{cite news|title=Mrs. Nina Evans Allender|url=https://query.nytimes.com/gst/abstract.html?res=9904E0D71E3EE63ABC4B53DFB266838C649EDE |date=April 3, 1957|newspaper=[[New York Times]]}}</ref>

<ref name="Pach 1903">{{cite web|title=Walter Pach diary, 1903 June 24 through Sept. 14|url=http://www.aaa.si.edu/collections/viewer/walter-pach-diary-3345/4726|work=Research Collections|publisher=Archives of American Art|accessdate=June 18, 2013}}</ref>

<ref name="Parade 1912">{{cite news|title=Suffrage Parade Plans|newspaper=Evening Star|date=December 28, 1912}}</ref>

<ref name="Plainfield 1957">{{cite news|title=Mrs. Allender, Artist, Dies|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=113820654|quote=Mrs. Nina E. Allender, 85, of 1200 W. Seventh St., one of the fighters for women's suffrage and equal rights, died yesterday at home. ...|newspaper=[[Plainfield Courier-News]]|date=April 3, 1957}}</ref>

<ref name="political cartoonists 1920">{{cite news|title=Women Political Cartoonists|newspaper=Christian Science Monitor|date= October 15, 1920}}</ref>

<ref name="Prieto">{{cite book|author=Laura R. Prieto|title=At Home in the Studio: The Professionalization of Women Artists in America|url=https://books.google.com/books?id=0bcXHa08knsC&pg=PA145|year=2001|publisher=Harvard University Press|isbn=978-0-674-00486-3|page=145}}</ref>

<ref name=Radcliffe>{{cite web|title=Woman's Journal (Boston, Mass. : 1870)|url=http://oasis.lib.harvard.edu/oasis/deliver/~sch00577|work=Records in the Woman's Rights Collection, 1888-1948: A Finding Aid|publisher=Radcliffe Institute for Advanced Study, Harvard University|accessdate=June 14, 2013}}</ref>

<ref name="Presides 1915">{{cite news|title=Great Interest in Coming Meet|newspaper=Evening Star|date=October 19, 1915}}</ref>

<ref name=Sewall>{{cite web | url=http://sewallbelmont.pastperfect-online.com/36836cgi/mweb.exe?request=keyword;keyword=nina%20allender;dtype=d  | title=Search: Nina Allender | publisher=Sewall Belmont }}</ref>

<ref name=Sewall2>{{cite web | url=http://sewallbelmont.pastperfect-online.com/36836cgi/mweb.exe?request=record;id=8F292CEF-999E-482F-BA01-732316504886;type=101 | title="Victory" by Nina Allender, Sept. 1, 1920 - Nina Allender Political Cartoon Collection | publisher=Sewall Belmont }}</ref>

<ref name="Sewall Women">{{cite web | url=http://www.sewallbelmont.org/womenwecelebrate/nina-allender/ | title=Nina Allender (1872-1957) |work=Women We Celebrate | publisher=Sewall-Belmont | accessdate=April 20, 2015 }}</ref>

<ref name="Sheppard">{{cite book|author=Alice Sheppard|title=Cartooning for Suffrage|url=https://books.google.com/books?id=KmrqAAAAMAAJ|date=January 1994|publisher=University of New Mexico Press|location=Albuquerque|page=102}}</ref>

<ref name="SI Portrait">{{cite web | url=http://siris-artinventories.si.edu/ipac20/ipac.jsp?&profile=all&source=~!siartinventories&uri=full=3100001~!40467~!0#focus | title=Charles Sheeler and Nina Allender, (painting) | publisher=Smithsonian Institution | accessdate=April 21, 2015 }}</ref>

<ref name="SI Pin">{{cite web | url=http://collections.si.edu/search/tag/tagDoc.htm?recordID=nmah_516351&hlterm=Nina%2BAllender | title=Amelia Himes Walker’s "Jailed for Freedom" Pin | publisher=Smithsonian Institution | accessdate=April 21, 2015 }}</ref>

<ref name="Studios 1909">{{cite news|last=Mechlan|first=Leila|title=From Local Studios|newspaper=Evening Star|date=March 19, 1909}}</ref>

<ref name="Suffragist">{{cite journal|title=Campaigners to Speak Next Sunday|journal=The Suffragist|date=November 25, 1916|volume=4|page=7}}</ref>

<ref name="Suffragist 2">{{cite journal|title='The Suffragist' as a Publicity Medium|journal=The Suffragist|date=February 23, 1918|volume=6|page=9}}</ref>

<ref name="Wardle 2005">{{cite book|last=Wardle|first=Marian|title=American Women Modernists: The Legacy of Robert Henri, 1910-1945|year=2005|publisher=Rutgers University Press|location=Piscataway, NJ|isbn=0813536847}}</ref>

<ref name="Walton">{{cite book|author=Mary Walton|title=A Woman's Crusade: Alice Paul and the Battle for the Ballot|url=https://books.google.com/books?id=hAWbMy0IvkkC&pg=PA61|date=August 17, 2010|publisher=St. Martin's Press|isbn=978-0-230-11141-7|page=61}}</ref>

<ref name="Wash Law Reporter 1905">{{cite journal|title=Legal Notices|journal=The Washington Law Reporter|date=March 1905|volume=33|pages=125|bibcode=  1916SciAm.114..367.|doi=10.1038/scientificamerican04011916-367|issue=14}}</ref>

<ref name="Wash Post 4 Feb 1917">{{cite news|title=The Women Who are 'Guarding' the White House Portals: Artist Aids in Fight|newspaper=The Washington Post|location=Washington, D.C.|date=February 4, 1917 | page=Magazine Section, 1}}</ref>

<ref name="Wash Post 6 Apr 1957">{{cite news|title=Obituaries: Nina E. Allender|location=Washington D.C. |newspaper=[[The Washington Post]]|date=April 6, 1957}}</ref>

<ref name="Wash Post 1-1905">{{ cite news | title=Wife Sues for Absolute Divorce | newspaper=Washington Post | location=Washington, D.C. | date=January 22, 1905 | page=2, column 1 }}</ref>

<ref name="Wash Post divorce">{{cite news|title=The Legal Record: Record of October 12, 1905|newspaper=The Washington Post|date=October 13, 1905}}</ref>

<ref name="Wedding 1893">{{cite news|title=Social and Personal Chat|via=Proquest|newspaper=The Washington Post|date=September 10, 1893}}</ref>

<ref name="Wheeling 30 Mar 1914">{{cite news|title=Mrs. Allender is Interesting|newspaper=Wheeling Register|date=March 30, 1914}}</ref>

<ref name="White House 1917">{{cite news|title=Pickets to Circle White House Today|newspaper=Evening Star|date=March 4, 1917}}</ref>

<ref name="Wilmington 6 Apr">{{cite news|title=Planning for Suffrage Parade|newspaper=Wilmington|date=April 6, 1914}}</ref>

<ref name="Wilmington 7 Apr">{{cite news|title=Suffrage Tree will be Planted on Arbor Day|newspaper=Wilmington|date=April 16, 1914}}</ref>

<ref name="Women speakers 1915">{{cite news|title=Rough and Tumble Campaign Develops Clever Women Speakers; Hecklers Don't Tackle These Suffragists Now|url=http://fultonhistory.com/Fulton.html|accessdate=June 12, 2013|newspaper=Syracuse Journal|date=July 2, 1915}}</ref>
}}

== External links ==
{{commons category-inline}}

{{Suffrage}}
{{New Woman (late 19th century)}}

{{DEFAULTSORT:Allender, Nina E.}}
[[Category:Articles created via the Article Wizard]]
[[Category:American women illustrators]]
[[Category:American suffragists]]
[[Category:People from Shawnee County, Kansas]]
[[Category:American cartoonists]]
[[Category:Women cartoonists]]
[[Category:1873 births]]
[[Category:1957 deaths]]
[[Category:National Woman's Party activists]]
[[Category:People from Plainfield, New Jersey]]