{{Infobox journal
| title = Ibis
| cover = [[File:Cover of Ibis.jpg|200px]]
| discipline = [[Ornithology]]
| editor = Paul F. Donald
| abbreviation =
| country = United Kingdom
| publisher = [[Wiley-Blackwell]] on behalf of the [[British Ornithologists' Union]]
| frequency = Quarterly
| history = 1859–present
| impact = 2.361
| impact-year = 2012
| website =http://www.bou.org.uk/pubibis.html
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1474-919X/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1474-919X/issues
| link2-name = Online archive
| ISSN = 1474-919X
}}
'''''Ibis''''', subtitled ''the International Journal of Avian Science'', is the [[peer review|peer-reviewed]] [[scientific journal]] of the [[British Ornithologists' Union]]. Topics covered include [[ecology]], conservation, behaviour, [[palaeontology]], and [[Taxonomy (biology)|taxonomy]] of birds. The [[editor-in-chief]] is Paul F. Donald ([[Royal Society for the Protection of Birds]]).<ref>{{cite web|url=http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291474-919X/homepage/EditorialBoard.html |title=Ibis editorial board |accessdate=2010-01-21 |publisher=Wiley Interscience}}</ref> The journal is published by [[Wiley-Blackwell]] in print and online. It is available free on the internet for institutions in the developing world through the OARE scheme (Online Access to Research in the Environment).<ref>{{cite web|url=http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291474-919X/homepage/News.html |title=Ibis News |accessdate=2010-11-24 |publisher=Wiley Interscience}}</ref>

== See also ==
* [[List of ornithology journals]]

== References ==
{{reflist}}

== External links ==
{{Commons category|Ibis (journal)}}
{{wikisource|Ibis}}
* {{Official website|http://www.ibis.ac.uk/}}

{{European birding journals}}

[[Category:Journals and magazines relating to birding and ornithology]]
[[Category:Publications established in 1859]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:1859 establishments in the United Kingdom]]


{{zoo-journal-stub}}