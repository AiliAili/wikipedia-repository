{{Infobox person
| name        = Robert Hurwitz
| occupation  = President of [[Nonesuch Records]]
}}

'''Robert Hurwitz''' has been president of [[Nonesuch Records]] since 1984.<ref name="nytimes">https://www.nytimes.com/2004/10/03/magazine/03NONESUCH.html?pagewanted=3</ref><ref name="gawker">http://gawker.com/551328/robert-hurwitz</ref> He previously ran the American operations of [[ECM Records]],<ref name="newyorker">http://www.newyorker.com/arts/critics/musical/2007/01/29/070129crmu_music_frerejones</ref><ref>http://www.nonesuch.com/about#change</ref> after beginning his career at [[Columbia Records]].<ref name="nytimes" /> Hurwitz grew up in Los Angeles, where he was trained as a pianist, then went on to study at the [[University of California, Berkeley]].<ref name="nytimes_b">https://www.nytimes.com/2004/10/03/magazine/03NONESUCH.html?pagewanted=2</ref>

Founded as a classical label in 1964,<ref>Holzman, Jac and Gavan Daws (1998).‘’Follow the Music’’, p. 100. FirstMedia Books.</ref> Nonesuch expanded into the world music field in 1967 with its Explorer series.<ref>http://www.billboard.com/articles/news/74382/nonesuch-explorer-series-africa</ref> Since Hurwitz became head of the company, it has further expanded its mission to include artists from a wide range of musical genres, including jazz, musical theater, folk, bluegrass, and rock.<ref name="nonesuch">http://www.nonesuch.com/about</ref><ref name="nytimes_a">https://www.nytimes.com/2004/10/03/magazine/03NONESUCH.html</ref>

Among the artists he has signed or worked with are composers including [[John Adams (composer)|John Adams]], [[Timo Andres]], [[Louis Andriessen]], [[Henryk Górecki]], [[Philip Glass]], [[Ástor Piazzolla]], [[Steve Reich]], [[Stephen Sondheim]], and [[John Zorn]] as well as performers and songwriters including [[Björk]], [[Jeremy Denk]], [[Bill Frisell]], [[Richard Goode]], [[Lorraine Hunt Lieberson]], [[Kronos Quartet]], [[Gidon Kremer]], [[k.d. lang]], [[Audra McDonald]], [[Brad Mehldau]], [[Pat Metheny]], [[Punch Brothers]], [[Randy Newman]], [[Joshua Redman]], [[Fernando Otero]],<ref>{{cite news|url=http://www.nonesuch.com/artists/fernando-otero|title=About Fernando Otero|work=[[Nonesuch Records]]|author=Michael Hill|accessdate=2013-05-13}}</ref> [[Chris Thile]], [[Dawn Upshaw]], and [[Caetano Veloso]]. During this time, the label’s artist roster has grown to also include [[Laurie Anderson]], [[The Black Keys]], [[Buena Vista Social Club]], [[Carolina Chocolate Drops]], [[Ry Cooder]], [[Emmylou Harris]], and [[Wilco]], among many others.<ref name="gawker" /><ref name="nonesuch" /><ref name="nytimes_a" />

Hurwitz has produced recordings by [[Caetano Veloso]],<ref name="newyorker" /><ref>http://www.nonesuch.com/albums/caetano-veloso</ref> [[Stephen Sondheim]],<ref>http://www.nonesuch.com/albums/bounce</ref> [[Astor Piazzolla]],<ref>http://www.nonesuch.com/albums/concierto-para-bandoneon-tres-tangos</ref> and [[Teresa Stratas]],<ref>http://www.nonesuch.com/albums/stratas-sings-weill</ref> and was the producer of the 1993 film ''George Balanchine’s The Nutcracker''.<ref>http://www.imdb.com/title/tt0107719/fullcredits?ref_=tt_cl_sm#cast</ref> Nonesuch releases have won 42 [[Grammy Awards]] during his tenure.<ref>http://www.nonesuch.com/about#grammys</ref>

Robert Hurwitz has taught a course at [[The New School]] in New York City since 2006.<ref>http://www.newschool.edu/lang/academics.aspx?id=24558</ref>

In a July 1998 article on Hurwitz and Nonesuch Records, the ''Boston Globe''’s Ed Siegel wrote: “Under Robert Hurwitz, Nonesuch Records has been an oasis of artistic excitement. When one picks up a Nonesuch CD, there is a sense of occasion, the feeling that the artists in question have been assembled not as an exercise in star power, but as an exercise in artistic exploration.”<ref>https://secure.pqarchiver.com/boston/access/31964536.html?FMT=ABS&FMTS=ABS:FT&type=current&date=Jul+12%2C+1998&author=Ed+Siegel%2C+Globe+Staff&pub=Boston+Globe&edition=&startpage=N.1&desc=Nonesuch+lives+up+to+its+name</ref>

An October 2004 ''New York Times Magazine'' profile written by Russell Shorto states: “In a business now largely run by accountants and M.B.A.’s, Hurwitz is, in the words of [[Stephen Sondheim]], ‘one of the few left who practice the making of records as a craft.’”<ref name="nytimes_b" />

== References ==

{{reflist}}
{{Authority control}}

{{DEFAULTSORT:Hurwitz, Robert}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American chief executives]]
[[Category:University of California, Berkeley alumni]]