{{Orphan|date=May 2015}}

{{Infobox person
| name        = Ruth Maddison
| birth_name  = 
| birth_date  = 18 November 1945
| birth_place = [[Melbourne]], [[Australia]]
| nationality = Australian
| occupation  = Photographer
| known_for   = A series of hand-coloured photographs depicting domestic situations
| notable_works  = 'And so...we joined the union', 1985
                   'Photography meets feminism', 2015
}}

'''Ruth Maddison''' is an Australian photographer born in 1945.<ref>{{cite book|last1=Newton|first1=Gael|title=Shades of Light|date=1988|publisher=Australian National Gallery|location=Canberra|isbn=0642081522|page=142}}</ref> She started photography in the 1970s and continues to make contributions to the Australian visual arts community.

==Biography==
Maddison is an Australian photographer who now resides at Eden on the south coast of [[New South Wales]].<ref name=NPG>{{cite web|title=Ruth Maddison|url=http://www.portrait.gov.au/people/ruth-maddison-1945|website=National Portrait Gallery|publisher=National Portrait Gallery|accessdate=10 April 2015}}</ref> She was born on 18 November 1945. As a first-year physiotherapy student, and a mother of three children, Maddison held various miscellaneous jobs before venturing into photography in 1976.<ref name=1980s /> Her friend, Ponch Hawkes, who was a photographer, encouraged her to use an available camera and a home set-up darkroom.<ref name="3rd eye">{{cite web|last1=Pigot|first1=Estelle|title=The Camera Is My 3rd Eye|url=http://regionalartsnsw.com.au/2013/06/artist-in-profile-ruth-maddison-the-camera-is-my-3rd-eye/|website=Regional Arts NSW|accessdate=20 March 2015}}</ref> This would lead to a commercial project six months later and a solo exhibition to follow within a period of three years. Although self-taught, she has seen both commercial and critical success nationally.

Maddison has also held a position as a lecturer at an Australian learning institute.<ref name=NPG />

{{Quote box
 |quote  = 
“I see an individual’s life as being immensely rich and deep and complex, and everyone a mixture of ordinary and extraordinary. From my first roll of film, I wanted to photograph people.”<ref name=1980s>{{cite book|last1=Ennis|first1=Helen|title=Australian Photography: The 1980s|publisher=Australian National Gallery|isbn=064208159X|page=38}}</ref>
 |width  = 35%
 |align  = right
}}

==Honours==
Her list of accolades include the Hobart City Art Prize (2007) and the Josephine Ulrick National Photography Prize for Portraiture (2002).<ref name=NPG />

In 2013, Maddison was the recipient of a 3-months residency at the Artspace Visual Arts Centre in Sydney.<ref name="3rd eye" /> This is a program made possible by the collaboration between Regional Arts New South Wales and Sydney Gallery, and sponsored by the Copyright Agency Cultural Fund. It offers regional artists the space, finance, and curatorial assistance to create and exhibit their works in an urban environment, and to make contributions to the visual arts community at large.

==Photography==
Maddison's career as a freelance photographer encompasses commissioned projects for theater companies, governmental organizations, and newspaper agencies. From 1984 to 1985, she became involved in a project called artists-in-community, which was funded by the Victorian Ministry for the Arts.<ref name=1980s /> As her professional interests lie in portraiture and social documentary, there is a strong sense of intimacy in Maddison's work. Throughout the 1980s, most of her photographs were of people in social environments within the metropolitan [[Melbourne]] area.<ref name=NGV>{{cite web|title=Ruth Maddison|url=http://www.ngv.vic.gov.au/col/artist/2305?view=textview|website=National Gallery of Victoria|publisher=National Gallery of Victoria|accessdate=20 April 2015}}</ref> Since moving from the city to a more rural, sea-side location in 1996, and being a prolific photographer, her portfolio has expanded to include many recognized series from then to now focusing on the stories of people in the Eden and Pambula district in New South Wales. These include: Now A River Went Out of Eden (2002) and Girt by Sea (2008).<ref name=NPG />

She continues to explore themes of relationships, communities and families.  Maddison's projects often speak of the ties-that-bind, offering the viewers glimpses into the daily lives and rituals of ordinary citizens within their homes and neighbourhoods. Furthermore, her endeavors as a feminist photographer  have given a voice to women in the community and the feminist movement within Australia in the 1970s and 1980s at large. These include: And so...we joined the Union (1985),<ref name=NPG /> Women over 60 (1991) and Single Mothers (1995).<ref name=NPG />

==Photographic techniques==
In her first solo exhibition in 1979, Christmas Holiday with Bob's Family,<ref name="Photo Access" /> Maddison produced a series of hand-coloured images. This is a technique that was popular in the 19th century, as well as the start of the 20th century. Photographers added paints, dyes or other materials and mediums to infuse colours to black and white images, extend the longevity of existing colours in images, increase the monetary value of their works, correct mistakes that have been made, and imbue a uniqueness to their photographs as a mean of creative expression.<ref name=NGA /> However, with the advancement of technology and the introduction of the modern prints, the practice of hand-colouring photographs lost its popularity until recent times where there has been a revival. Now, it is considered to be an important facet of contemporary Australian photography. In 2015, the [[National Gallery of Victoria]] held an exhibition featuring works by various artists, including Maddison's, that celebrates the beauty of this medium.

There was a shift in artistic endeavors within the latter part of her career. In a 2015 interview by Estelle Pigot,<ref name="3rd eye" /> Maddison expresses her concerns for the news on public broadcasting channels, which she believes cause a drift in the community. Her focus now resides mainly in the unknown stories that unite people as well as themes of death, decay, and mortality. Although she started with photography, Maddison now also use the mediums of textiles, sculptures, and the moving-image. Some of her experimental works in 2009, belonging to a project called There is a time,<ref name="Photo Access" /> include: 'Dead to the world', inkjet print on cotton/silk fabric, found fabric flowers sewn with cotton thread, 'Coming to the Monaro', silent video loop, 'The day I left my garden #1 & #2', perspex, glass, wood, neon light, with pressed and dried flowers on paper and 'There is a time', 2009, archival pigment prints on 100% cotton rag.

==Critical evaluation==
In an article published by [[The Australian]], 1983, 'Abstract painters point the way to an improving economy',<ref>{{cite news|last1=McGrath|first1=Sandra|title=Abstract painters point the way to an improving economy!|accessdate=23 April 2015|agency=The Australian|date=April 1983}}</ref> Sandra McGrath commented on Maddison's work in a group exhibition titled Survey 83' at the Watters Gallery in Sydney. The photographs shown in this exhibition belonged to the series Some Men, of the same year, by the photographer. McGrath made an observation on the use of the technique of hand-coloring employed by Maddison, which is not to add glamour to the subject matter as commercial photographers often do, but to create a sense of surrealism and painting-like effect to the images. By utilizing oil-paint, Maddison changed the tonality of the background, texture, skin tones and colors. Furthermore, McGrath noted the absence of sexual overtones, except in two instances, in Maddison's work, although these are very candid shots of male models. This is attributed to the fact that Maddison spent time to establish a relationship with her subjects before photographing them..

==Collections==
Maddison’s work is represented in major public collections in museums, galleries, art spaces and institutions nationally. These include: [[National Gallery of Australia]], [[Canberra]]; [[National Gallery of Victoria]], [[Melbourne]]; [[Queensland Art Gallery]], [[Brisbane]]; [[Museum of Contemporary Art, Sydney]]; Tasmanian Museum and Gallery, [[Hobart]]; [[Jewish Museum of Australia]], Melbourne; [[National Library of Australia]] Picture Collection, Canberra; [[State Library of Victoria]] Picture Collection, Melbourne; [[State Library of New South Wales]] Picture Collection, Sydney; Tweed River Regional Art Gallery, [[Queensland]]; Griffith Artworks, Griffith University, Queensland; Charles Sturt University, [[New South Wales]]; ACT Community Health Care, Belconnen; Royal Women’s Hospital, Melbourne; Albury Regional Art Centre, Victoria; Trades Hall Council, Melbourne; Australian Council of Trade Unions, Melbourne; Performing Arts Museum, Melbourne; Artists for Peace Gallery, [[Moscow]].<ref name="Photo Access" />

==Publications==
Her work can also be found in many important publications by major national galleries. These include: ''Recent Australian Photography: From the KODAK Fund'', 1985; ''Living in the 70s: Contemporary Photographers'' by the National Gallery of Victoria, 1987; ''The Thousand Mile Stare: A Photographic Exhibition'' by the Victorian Centre for Photography, Melbourne, 1988; ''Australian Photography: the 1980s'' by the Australian National Gallery, Canberra, 1988; ''Shades of Light'' by the Australian National Gallery, Canberra, 1988; ''Indecent Exposures: Twenty Years of Australian Feminist Photography'' by the Power Institute of Fine Arts, Sydney, 1994 and ''2nd Sight: Australian Photography'' by the National Gallery of Victoria, Melbourne, 2002.

==Exhibitions==

===Selected solo exhibitions===
Maddison has held solo-exhibitions within and outside of Australia since 1979.<ref name="Photo Access" /> Some of these include: 
* 1979: ''Christmas Holiday with Bob's Family'', Ewing Gallery, University of Melbourne. This exhibition included a number of hand-coloured photographs which were made to resemble images from a family album
* 1980: ''When A Girl Marries'', Australian Centre for Photography, Sydney
* 1983: ''Some Men'', Watters Gallery, Sydney
* 1988: ''Street Photography from Australia'', Artists for Peace Gallery, Moscow
* 1991/92/94: ''Women Over 60'', Victorian Centre for Photography; Artspace, Sydney; Darwin Performing Arts Centre
* 1993/94: ''Blanche Street 1985–1993: Extracts From a Photo Diary'', Linden, St.Kilda Arts Centre, Melbourne; Tin Sheds Gallery, Sydney
* 1995: ''Single Mothers,'' Access Gallery, National Gallery of Victoria; Royal Women’s Hospital, Melbourne; National Wool Museum, Geelong, Victoria
* 1998: ''Jewish Families'', Jewish Museum of Australia, Melbourne
* 2002/03: ''Now A River Went Out Of Eden'', Bega Valley Regional Gallery, NSW and Stills Gallery, Sydney
* 2003: ''Response'', Photoaccess, Canberra
* 2008 /09: ''Girt By Sea'', Cowra Regional Art Gallery; Eden Killer Whale Museum, NSW
* 2008: ''Now You See Us'', Jindabyne National Park Visitors Centre
* 2009: ''There is a tim''e, PhotoAccess, Canberra

===Selected group exhibitions===
Many significant group exhibitions have featured Maddison's work. A few of these include:

* 1985: ''And so...we joined the Union'', RMIT Gallery, Melbourne.<ref name="Photo Access">{{cite web|last1=Gates|first1=Merryn|title=There Is A Time|url=http://www.photoaccess.org.au/files/RMaddison_cat_combined.pdf|website=Photo Access|accessdate=16 April 2015}}</ref> This is an exhibition that focuses on the involvement of women in the trade union. 
* 1988: ''Australian Photography: the 1980s'', Australian National Gallery and State Galleries; The Thousand Mile Stare, Australian Centre for Contemporary Art, Melbourne Art and Working Life: More Than a Document, Australian Council of Trade Unions, Melbourne<ref name="Photo Access" />
* 1994: ''All In The Family'', National Library of Australia, Canberra; The Full Spectrum: Colour and Photography in Australia 1860s to 1990s, National Gallery of Victoria, Melbourne and Aspects of Australian Portraiture, Australian Portrait Gallery, Canberra<ref name="Photo Access" />
* 2007: ''City of Hobart Art Prize'' (Winner), Tasmanian Museum and Gallery; Hobart Reveries: Photography and Mortality, National Portrait Gallery, Canberra; Josephine Ulrick/Win Shubert Photography Award (Finalist), Gold Coast, City Art Gallery<ref name="Photo Access" />
* 2015: ''Photography meets feminism: Australian women photographers'' 1970s–80s, Monash Gallery of Art. This is an exhibition that identifies the relationship between photography and two significant social reforms in the 1970s and 1980s: women's liberation and feminism. Also, it highlights the progression of photographic technologies and practices that was made possible by female photographers of this period in Australian history.<ref name=NAG>{{cite web|title=Exhibitions|url=http://www.nag.org.au/exhibitions/present/artist/photography_meets_feminism_australian_women_photographers_1970s80s|website=Newcastle Art Gallery|publisher=Newcastle Art Gallery|accessdate=20 March 2015}}</ref>
* 2015: ''Colour My world: Hand-coloured Australian Photography, National Gallery of Australia''. The first exhibition of its kind which showcases hand-coloured photographs by various artists, including Maddison. In essence, this collection celebrates a practice that went into decline but then witnessed a resurgence in recent times.<ref name=NGA>{{cite web|title=Colour my world: hand coloured Australian photography|url=http://nga.gov.au/ColourMyWorld/|website=National Gallery of Australia|publisher=National Gallery of Australia|accessdate=30 March 2015}}</ref>

==References==
{{reflist}}

*
*
*
*

{{DEFAULTSORT:Maddison, Ruth}}
[[Category:Photographers from Melbourne]]
[[Category:1945 births]]
[[Category:Living people]]
[[Category:Photographers from New South Wales]]