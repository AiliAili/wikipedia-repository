{{Infobox building
| name                = Al Bahr Towers
| native_name         = 
| native_name_lang    = 
| former_names        = 
| alternate_names     = 
| status              = 
| image               = 
| image_alt           = 
| caption             = 
| map_type            = 
| map_alt             = 
| map_caption         = 
| altitude            = 
| building_type       = [[Office]]s
| architectural_style = [[Islamic architecture]]
| structural_system   = 
| cost                = 
| ren_cost            = 
| client              = [[Abu Dhabi Investment Council]]
| owner               = 
| current_tenants     = 
| landlord            = 
| location            =
| address             = 
| location_town       = [[Abu Dhabi]]
| location_country    = {{flag|United Arab Emirates}}
| coordinates         = 
| groundbreaking_date =
| start_date          =
| completion_date     = 
| opened_date         = 
| inauguration_date   = 
| renovation_date     = 
| demolition_date     = 
| destruction_date    = 
| height              = 145m
| diameter            = 
| antenna_spire       = 
| roof                = 
| top_floor           = 
| other_dimensions    = 
| floor_count         = 25
| floor_area          = 70,000 sq m 
| seating_type        = 
| seating_capacity    = 
| elevator_count      = 
| main_contractor     = [[Al-Futtaim Group|Al-Futtaim]] [[Carillion]] 
| architect           = 
| architecture_firm   = AHR
| structural_engineer =
| services_engineer   = 
| civil_engineer      = 
| other_designers     = 
| quantity_surveyor   = 
| awards              = 
| designations        =
| ren_architect       = 
| ren_firm            = 
| ren_str_engineer    = 
| ren_serv_engineer   = 
| ren_civ_engineer    = 
| ren_oth_designers   = 
| ren_qty_surveyor    = 
| ren_awards          = 
| url                 = 
| embedded            =
| references          = 
}}
'''Al Bahr Towers''' is a development in the emirate of [[Abu Dhabi]] consisting of two 29-storey, 145m–high towers. It is located at the intersection of Al Saada and Al Salam Street in Abu Dhabi City, the capital of the United Arab Emirates, at the eastern entrance. One tower houses the new headquarters of the [[Abu Dhabi Investment Council]] (ADIC), an investment arm of the Government of Abu Dhabi. The other serves as the head office of [[Al Hilal Bank]],<ref>{{cite web|url=http://www.alhilalbank.ae/|title=Al Hilal Bank|publisher=Al Hilal Bank|accessdate=21 December 2013}}</ref> a progressive and innovative Islamic Bank.

==Description==
The distinguishing feature of the towers is their protective skin of 2,000 umbrella-like glass elements that automatically open and close depending on the intensity of sunlight. Inspired by the '[[mashrabiya]]', geometrically-designed wooden lattice screens that have been used to fill windows of traditional Arabic architecture since the 14th century, the 'intelligent' façade of the Al Bahr Towers is dynamically controlled by a building management system.  The adjustable shades help reduce interior heat gains caused by sunlight by around 50 percent.<ref>{{cite web|title=50 percent|url=http://www.gizmag.com/al-bahar-towers/26139/|publisher=GizMag|accessdate=7 February 2013}}</ref> The eco-friendly towers are one of the first buildings in the Gulf to receive the Leadership in Energy and Environmental Design (LEED) Silver rating.<ref>{{cite web|title=Silver rating |url=http://en.wikiarquitectura.com/index.php/Al_Bahar_Towers|publisher=wikiarquitectura|accessdate=10 August 2013}}</ref> They were placed second at the Emporis Skyscraper Award – the world's premier honorary event for high-rise architecture – for projects completed in 2012. The development has also been featured on the Chicago-based Council on Tall Buildings and Urban Habitat's "Innovative 20" list of buildings that "challenge the typology of tall buildings in the 21st century".<ref>{{cite web|title=21st century |url= http://www.thenational.ae/business/industry-insights/property/abu-dhabis-al-bahr-towers-second-to-canadas-marilyn-monroe-buildings-in-best-architecture-awards|publisher=The National|accessdate=19 September 2013}}</ref> Al Bahr Towers is designed by AHR (formerly [[Aedas]] UK), and constructed by [[Al-Futtaim Group|Al-Futtaim]] [[Carillion]], a Dubai-based provider of integrated [[solution]]s for infrastructure, building, and services.<ref>{{cite web|url=http://skyscrapercenter.com/abu-dhabi/al-bahar-tower-2/9130/|title=Al Bahr Towers|publisher=Skyscraper City|accessdate=21 December 2013}}</ref>

==References==
{{reflist}}

{{coord|24.4566|N|54.4009|E|source:wikidata|display=title}}

[[Category:Office buildings completed in 2012]]
[[Category:Skyscrapers between 100 and 149 meters]]
[[Category:Skyscraper office buildings in Abu Dhabi]]