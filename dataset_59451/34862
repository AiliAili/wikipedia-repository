{{Good article}}
{{Infobox cycling race report
| name               = 1910 Giro d'Italia
| image              = Giro Italia 1910-map.png
| image_caption      = Overview of the stages:
route clockwise from Milan, down to Naples, then up to Milan
| date               = 18 May &ndash; 5 June
| stages             = 10
| distance           = 2987.4
| unit               = km
| time               = 114h 24' 00"
| speed              = 26.08
| first              = [[Carlo Galetti]]
| first_nat          = ITA
| first_natvar       = 1861
| first_team         = Atala-Continental
| second             = [[Eberardo Pavesi]]
| second_nat         = ITA
| second_natvar      = 1861
| second_team        = Atala-Continental
| third              = [[Luigi Ganna]]
| third_nat          = ITA
| third_natvar       = 1861
| third_team         = Atala-Continental
| team               = [[Atala (cycling team)|Atala-Continental]]
| team_nat           = ITA
| team_natvar        = 1861
| previous = [[1909 Giro d'Italia|1909]]
|next = [[1911 Giro d'Italia|1911]]
}}
The '''1910 Giro d'Italia''' was the second edition of the [[Giro d'Italia]], a cycling race organized and sponsored by the [[newspaper]] ''[[La Gazzetta dello Sport]]''. The race began on 18 May in [[Milan]] with a stage that stretched {{convert|388|km|0|abbr=on}} to [[Udine]], finishing back in Milan on 5 June after a {{convert|277.5|km|0|abbr=on}} stage and a total distance covered of {{convert|2987.4|km|0|abbr=on}}. The race was won by the Italian rider [[Carlo Galetti]] of the [[Atala (cycling team)|Atala-Continental]] team, with fellow Italians [[Eberardo Pavesi]] and [[Luigi Ganna]] coming in second and third respectively.<ref name="elm stage 10, finals">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1910/06/16/MD19100616-003.pdf |title=La Vuelta De Italia|language=Spanish|date=16 June 1910|page=3|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia| archiveurl= http://www.webcitation.org/6QS45MHsb | archivedate=19 June 2014}}</ref>

Eberardo Pavesi was the first rider to lead the race after winning the first stage into [[Udine]]. After the second stage, Carlo Galetti took the lead of the race. Galetti then successfully defended the lead all the way to the race's finish in Milan. En route to his overall victory, Galetti won two stages. The Atala-Continental team finished as winners of the team classification.<ref name="BRI 1910">{{cite web |last1=McGann |first1=Bill |last2=McGann |first2=Carol |url=http://bikeraceinfo.com/giro/giro1910.html |title=1910 Giro d'Italia |work=Bike Race Info |publisher=Dog Ear Publishing |accessdate=10 July 2012| archiveurl=https://web.archive.org/web/20140223035301/http://www.bikeraceinfo.com/giro/giro1910.html | archivedate=19 June 2014}}</ref>

==Changes from the 1909 Giro d'Italia==

One major change was made to the calculation for the general classification before the start of the second Giro d'Italia.<ref name="BRI 1910"/> Originally a point was given to each rider for his placing on each stage,<ref name="BRI 1909">{{cite web |last1=McGann |first1=Bill |last2=McGann |first2=Carol |url=http://bikeraceinfo.com/giro/giro1909.html |title=1909 Giro d'Italia |work=Bike Race Info |publisher=Dog Ear Publishing |accessdate=10 July 2012| archiveurl=https://web.archive.org/web/20140304151837/http://www.bikeraceinfo.com/giro/giro1909.html | archivedate=19 June 2014}}</ref> but the organizers chose to give the riders who placed 51st or higher in a stage 51 points and keep the point distribution system the same for the riders who placed 1st through 50th in a stage.<ref name="BRI 1910"/><ref name="CR 1910">{{cite web|url=http://www.cyclingrevealed.com/timeline/Race%20Snippets/GdI/GdI_1910.htm|title=The Second Giro in 1910|publisher=Cycling Revealed|author=Barry Boyce|year=2004|accessdate=16 April 2013| archiveurl= http://www.webcitation.org/6QS4DKRkD | archivedate=19 June 2014}}</ref>

The organizers chose to increase the length of their race after the success from the [[1909 Giro d'Italia|first edition]].<ref name="BRI 1910"/><ref name="elm stages 1, 2">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1910/05/26/MD19100526-005.pdf|title=La Vuelta De Italia|language=Spanish|date=26 May 1910|page=5|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia| archiveurl= http://www.webcitation.org/6QS4FIzmF | archivedate=19 June 2014}}</ref> The original race was eight stages long and the 1910 edition was increased by two stages, to ten.<ref name="BRI 1910"/><ref name="elm stages 1, 2"/> The overall length of the race was increased to close to 3,000 kilometers,<ref name="elm stages 1, 2"/> which made the race close to 500 kilometers longer than the inaugural edition of the race.<ref name="BRI 1910"/>

==Participants==

A total of 101 riders started the second Giro d'Italia out of the 118 that signed up to participate.<ref name="elm stages 1, 2"/> Of the 101 riders that began the Giro d'Italia on 18 May, twenty of them made it to the finish in [[Milan]] on 5 June.<ref name="BRI 1910"/> Riders were allowed to ride on their own or as a member of a team.<ref name="BRI 1910"/> There were six teams that competed in the race: [[Atala (cycling team)|Atala-Continental]], Atena-Dunlop, [[Bianchi cycling team|Bianchi-Dunlop]], [[Legnano-Pirelli|Legnano-Dunlop]], Otav-Pirelli, and Stucchi-Pirelli.<ref name="BRI 1910"/>

The notable participants included the reigning champion of the [[Giro d'Italia]], [[Luigi Ganna]].<ref name="BRI 1910"/> The peloton also contained some of the most famous Italian cyclists at the time in [[Carlo Galetti]], [[Ezio Corlaita]], [[Giovanni Rossignoli]], and [[Eberardo Pavesi]].<ref name="BRI 1910"/> Two-time [[Tour de France]] champion [[Lucien Petit-Breton]] rode the Giro for the second straight year, along with his fellow countryman [[Jean-Baptiste Dortignacq]] who had been successful in the Tour.<ref name="BRI 1910"/> The Giro also saw its first German participants, neither of whom completed the race.<ref name="BRI 1910"/>

==Race overview==

[[File:Eberardo Pavesi 1904.JPG|thumb|left|[[Eberardo Pavesi]] won two stages at the 1910 Giro d'Italia.|alt=A man posing with a bike.]]
The first stage of the second Giro d'Italia began with a large send off in the start city of [[Milan]].<ref name="elm stages 1, 2"/> The finish in [[Udine]] was hotly contested as five riders came to the finish line in a pack.<ref name="elm stages 1, 2"/> [[Ernesto Azzini]] managed to out-sprint the other four riders in the leading group to win the stage.<ref name="elm stages 1, 2"/> The next stage saw the first non-Italian stage winner in the history of the Giro d'Italia, the Frenchman [[Jean-Baptiste Dortignacq]].<ref name="elm stages 1, 2"/><ref name="LR 1910 source">{{cite web|author=Eugenio Capodacqua|language=Italian|date=10 May 2007|url=http://www.repubblica.it/2007/05/speciale/altri/2007giroditalia/storia/storia.html|title=La storia del Giro d'Italia (1909-1950)|newspaper=La Repubblica|publisher=Gruppo Editoriale L’Espresso| accessdate= 27 December 2007 <!--DASHBot-->| archiveurl= https://web.archive.org/web/20071224103726/http://www.repubblica.it/2007/05/speciale/altri/2007giroditalia/storia-3/storia-3.html| archivedate= 24 December 2007| deadurl= no|trans_title=The history of the Tour of Italy (1909-1950)}}</ref><ref name="ep hist">{{cite news|url=http://www.elpais.com/deportes/ciclismo/giro-de-italia/historia/1909-1914/|title=El inicio |language=Spanish |work= El País|publisher=Ediciones El País |accessdate=27 May 2013|trans_title=The Start| archiveurl= http://www.webcitation.org/6QS4HRhXr | archivedate=19 June 2014}}</ref> Dortignacq managed to breakaway from his fellow group member [[Carlo Galetti]] and then solo to the stage victory while five other riders chased close behind.<ref name="BRI 1910"/><ref name="elm stages 1, 2"/> Galetti's second-place finish on the stage was high enough for him to take the lead of the race.<ref name="elm stages 1, 2"/> The French success enraged the Italian fans and led the competing riders to ban unite against the foreign riders.<ref name="BRI 1910"/> The third stage saw three major Italian riders, [[Luigi Ganna]], Carlo Galetti, and [[Eberardo Pavesi]], and attack early on in the stage.<ref name="BRI 1910"/><ref name="LR 1910 source"/> The three riders then rode together all the way to the finish in [[Teramo]].<ref name="LR 1910 source"/><ref name="elm stages 3, 4, 5">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1910/06/02/MD19100602-005.pdf|title=La Vuelta De Italia|language=Spanish|date=2 June 1910|page=5|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=2 June 2012|format=PDF|trans_title=The Giro d'Italia| archiveurl= http://www.webcitation.org/6QS4MZtGQ | archivedate=19 June 2014}}</ref> Galetti went on to win the stage, with Ganna and Pavesi following in quick succession.<ref name="elm stages 3, 4, 5"/>

The Giro's fourth stage saw a tough stage with poor conditions for the riders, which ultimately led to the withdrawal of many riders including the likes of [[Lucien Petit-Breton]] - who was ranked third overall at the time.<ref name="elm stages 3, 4, 5"/> The peloton's pace was slower due to the w poor conditions, which caused the riders to arrive two hours later than expected in [[Naples]].<ref name="elm stages 3, 4, 5"/> [[Pierino Albini]] beat out the Frenchman [[Maurice Brocco]] by 36 seconds for stage victory.<ref name="BRI 1910"/><ref name="elm stages 3, 4, 5"/> Prior to the fifth stage's start, Frenchman [[Jean-Baptiste Dortignacq]] withdrew from the race after citing an illness.<ref name="BRI 1910"/><ref name="elm stages 3, 4, 5"/> The police suspected foul play and looked into the situation, but their results are disputed through contradicting stories.<ref name="BRI 1910"/> Due to impassable roads, the start of the fifth stage was moved from Naples to [[Capua]] and decreased the distance of the stage from {{convert|224.1|km|0|abbr=on}} to {{convert|192.3|km|0|abbr=on}}.<ref name="elm stages 3, 4, 5"/> Eberardo Pavesi won the fifth stage after leading for most of the race.<ref name="elm stages 3, 4, 5"/>

The peloton remained intact for roughly the first half of the sixth stage.<ref name="elm stages 6,7,8,9">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1910/06/09/MD19100609-002.pdf|title=La Vuelta De Italia|language=Spanish|date=9 June 1910|page=2|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia| archiveurl= http://www.webcitation.org/6QS4OviSo | archivedate=19 June 2014}}</ref> Luigi Ganna and Carlo Galetti broke away from the pack and made it to the finish in [[Rome]] where Ganna then edged out Galetti for the stage victory.<ref name="elm stages 6,7,8,9"/> During the next stage, a group of six riders lead for the most part before Galetti and Ganna broke away again.<ref name="elm stages 6,7,8,9"/> The two riders made their way to the finish in [[Genoa]], where a large crowd of spectators came to see the finish.<ref name="elm stages 6,7,8,9"/> Ganna went on to win his second consecutive stage, while Galetti extended his lead over the rest of the field.<ref name="elm stages 6,7,8,9"/> The race's eighth stage contained some harsh climbs, most notably the Giovi.<ref name="BRI 1910"/><ref name="elm stages 6,7,8,9"/> The finish of the stage was hotly contested as Galetti, Ganna, and Eberardo Pavesi finished at the same time, with Galetti winning the stage in the end.<ref name="elm stages 6,7,8,9"/>

Eberardo Pavesi dominated the difficult ninth stage that contained the major climbs of the Nava, Tenda, and San Bartolomeo.<ref name="BRI 1910"/><ref name="elm stages 6,7,8,9"/> Pavesi went on to win the stage by close to six minutes over the second-place finisher Luigi Ganna.<ref name="elm stages 6,7,8,9"/> The race's tenth and final stage was marred by rain.<ref name="elm stage 10, finals"/> Race leader Carlo Galetti crashed into a hay wagon early on in the stage and sustained some heavy wounds,<ref name="LR 1910 source"/> he would get back on his bike and finish the stage in fifth place.<ref name="elm stage 10, finals"/><ref name="BRI 1910"/> Despite the rain in Milan, many spectators still came to watch the riders arrive.<ref name="elm stage 10, finals"/> Luigi Ganna was the first rider to cross the finish line in Milan and in doing so, he won his third stage of the 1910 Giro d'Italia.<ref name="elm stage 10, finals"/><ref name="BRI 1910"/> Carlo Galetti won the Giro d'Italia by a margin of eighteen points over Pavesi.<ref name="elm stage 10, finals"/> Galetti and his team, Atala-Continental, won the team classification.<ref name="BRI 1910"/>

==Final standings==

===Stage results===

{| class="wikitable"
|+ Stage results<ref name="BRI 1910"/><ref name="CR 1910"/>
|- style="background:#efefef;"
!Stage
!Date
!Course
!Distance
!colspan="2"|Type<ref group="Notes">In 1910, there was no distinction in the rules between plain stages and mountain stages; the icons shown here indicate that the fourth, fifth, sixth, seventh, eighth, ninth, and tenth stages included major mountains.</ref>
!Winner
!Race Leader
|-
!style="text-align:center"|1
|18 May
|align=left| [[Milan]] to [[Udine]]
|style="text-align:center"| {{convert|388|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| {{flagathlete|[[Ernesto Azzini]]|ITA|1861}}
| {{flagathlete|[[Ernesto Azzini]]|ITA|1861}}
|-
!style="text-align:center"|2
|20 May
|align=left| [[Udine]] to [[Bologna]]
|style="text-align:center"| {{convert|322.4|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| {{flagathlete|[[Jean-Baptiste Dortignacq]]|FRA}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|3
|22 May
|align=left| [[Bologna]] to [[Teramo]]
|style="text-align:center"| {{convert|345.2|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|4
|24 May
|align=left| [[Teramo]] to [[Naples]]
|style="text-align:center"| {{convert|319.5|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Pierino Albini]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|5
|26 May
|align=left| [[Naples]] to [[Rome]]
|style="text-align:center"| {{convert|192.3|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| {{flagathlete|[[Eberardo Pavesi]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|6
|28 May
|align=left| [[Rome]] to [[Florence]]
|style="text-align:center"| {{convert|327.5|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Luigi Ganna]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|7
|30 May
|align=left| [[Florence]] to [[Genoa]]
|style="text-align:center"| {{convert|263.5|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Luigi Ganna]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|8
|1 June
|align=left| [[Genoa]] to [[Mondovì]]
|style="text-align:center"| {{convert|218.1|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|9
|2 June
|align=left| [[Mondovì]] to [[Turin]]
|style="text-align:center"| {{convert|333.4|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Eberardo Pavesi]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!style="text-align:center"|10
|5 June
|align=left| [[Turin]] to [[Milan]]
|style="text-align:center"| {{convert|277.5|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Luigi Ganna]]|ITA|1861}}
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
|-
!
| colspan="2" style="text-align:center;"| Total
| colspan="5" style="text-align:center;"| {{convert|2987.4|km|0|abbr=on}}
|}

===General classification===

There were 20 cyclists who had completed all ten stages. For these cyclists, the points they received from each of their stage placing's were added up for the [[General classification in the Giro d'Italia|general classification]]. The cyclist with the least accumulated points was the winner.

{| class="wikitable" style="width:45em;margin-bottom:0;"
|+ Final general classification (1–10)<ref name="elm stage 10, finals"/><ref name="BRI 1910"/><ref name="Cycling Archives GC">{{cite web|url=http://www.cyclingarchives.com/ritfiche.php?ritid=9317&wedstrijdvoorloopid=209#first |title=Giro d'Italia 1910 |work=Cycling Archives|accessdate=16 April 2013}}</ref>
|-
!Rank
!Name
!Team
!Points
|-
!style="text-align:center"| 1
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}}
| Atala-Continental
| style="text-align:center;"| 28
|-
!style="text-align:center"| 2
| {{flagathlete|[[Eberardo Pavesi]]|ITA|1861}}
| Atala-Continental
| style="text-align:center;"| 46
|-
!style="text-align:center"| 3
| {{flagathlete|[[Luigi Ganna]]|ITA|1861}}
| Atala-Continental
| style="text-align:center;"| 51
|-
!style="text-align:center"| 4
| {{flagathlete|[[Ezio Corlaita]]|ITA|1861}}
|  — 
| style="text-align:center;"| 71
|-
!style="text-align:center"| 5
| {{flagathlete|[[Emilio Chironi]]|ITA|1861}}
| Otav-Pirelli
| style="text-align:center;"| 77
|-
!style="text-align:center"| 6 
| {{flagathlete|[[Battista Danesi]]|ITA|1861}}
| Atala-Continental
| style="text-align:center;"| 87
|-
!style="text-align:center"| 7
| {{flagathlete|[[Clemente Canepari]]|ITA|1861}}
| Otav-Pirelli
| style="text-align:center;"| 102
|-
!style="text-align:center"| 8
| {{flagathlete|[[Giovanni Marchese (cyclist)|Giovanni Marchese]]|ITA|1861}}
| Otav-Pirelli
| style="text-align:center;"| 114
|-
!style="text-align:center"| 9
| {{flagathlete|[[Ildebrando Gamberini]]|ITA|1861}}
| — 
| style="text-align:center;"| 120
|-
!style="text-align:center"| 10
| {{flagathlete|[[Giuseppe Galbai]]|ITA|1861}}
| — 
| style="text-align:center;"| 132
|}
{| class="collapsible collapsed wikitable" style="width:45em;margin-top:0;"
|-
!colspan=4|Final general classification (11–20)<ref name="elm stage 10, finals"/><ref name="BRI 1910"/><ref name="Cycling Archives GC"/>
|-
!Rank
!Name
!Team
!Points
|-
| style="text-align:center;"|  11 || {{flagathlete|[[Augusto Rho]]|ITA|1861}} || — || align=center| 137
|-
| style="text-align:center;"|  12 || {{flagathlete|[[Antonio Rontondi]]|ITA|1861}} || — || align=center| 139
|-
| style="text-align:center;"|  13 || {{flagathlete|[[Giuseppe Perna]]|ITA|1861}} || — || align=center| 141
|-
| style="text-align:center;"|  14 || {{flagathlete|[[Cesare Osnaghi]]|ITA|1861}} || — || align=center| 145
|-
| style="text-align:center;"|  15 || {{flagathlete|[[Amedeo Dusio]]|ITA|1861}} || — || align=center| 149
|-
| style="text-align:center;"|  16 || {{flagathlete|[[Alberto Sonetti]]|ITA|1861}} || — || align=center| 151
|-
| style="text-align:center;" rowspan="3"|  17 || {{flagathlete|[[Mario Secchi]]|ITA|1861}} || — || align=center rowspan="3"| 156
|-
| {{flagathlete|[[Giovanni Scarpetta]]|ITA|1861}} || — 
|-
| {{flagathlete|[[Luigi Rotta]]|ITA|1861}} || — 
|-
| style="text-align:center;"|  20 || {{flagathlete|[[Umberto Turconi]]|ITA|1861}} || — || align=center| 161
|}

==Notes==

{{Reflist|group="Notes"}}

==References==

{{reflist|30em}}

{{Giro d'Italia}}

[[Category:Giro d'Italia by year]]
[[Category:1910 in road cycling|Giro d'Italia]]
[[Category:1910 in Italian sport|Giro d'Italia]]