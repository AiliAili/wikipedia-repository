{{Use British English|date=September 2013}}
{{Use dmy dates|date=February 2014}}
'''J.R. Rix & Sons Ltd''' is a family-owned British business that is headquartered in [[Kingston upon Hull|Hull]], [[East Riding of Yorkshire]]. The roots of the company date back to 1873 when founder Robert Rix, the son of a Norfolk farmer, established a ship building business in Teesside, which he subsequently moved to Hull in 1883.<ref name="history">A History of Robert Rix & Sons & J.R. Rix & Sons Ltd by Graham Atkinson</ref>

Today J.R. Rix & Sons Ltd is involved in a large range of activities ranging from commercial, residential and marine fuel distribution, fuel cards, ship owning, stevedoring, warehousing and off shore wind farm support to holiday home manufacturing, car retail and property, through a number of subsidiaries of which the largest is Rix Petroleum Ltd.

Others include Rix Heating Services; Rix Shipping; Rix Sea Shuttle; Maritime Bunkering; Rix Shipping (Scotland) Ltd; Jordans Cars; Rix Truck Services; Fuelmate, and; Victory Leisure Homes.<ref>{{cite web|url=http://www.rix.co.uk/our-family-of-companies/ |title=Our Family Of Companies &#124; A Rix Petroleum Company |publisher=Rix.co.uk |date=1 January 1970 |accessdate=11 February 2014}}</ref>

In 2014 the company was ranked 30th in the Sunday Times’ Top Track 250. The current managing director of J.R. Rix & Sons Ltd is Tim Rix<ref>{{cite web|url=http://www.fasttrack.co.uk/fasttrack/leagues/dbtop250Details.asp?siteID=5&compID=1664&yr=2012 |title=Company profile |publisher=Fasttrack.co.uk |date= |accessdate=11 February 2014}}</ref> the great-great-grandson of founder Robert.

==Early years==

[[File:Robert Rix.jpg|thumb|left|220x220px|Robert Rix]]
Robert Rix was born in March 1841 into a farming family in [[Burnham Overy]], [[Norfolk]],<ref name="history"/> and at age 10 was sent to work on the land. However, he did not take to the work and within two years had run away to sea. He joined a small sailing coaster on the Tees and his early career was spent trading with northern Scottish ports down to South Wales and later the near Continent.

He settled in [[Stockton-on-Tees]] in the North East of England and in 1862 married Margaret Dobson. It was while living here that he first established a shipbuilding company on the south bank of the Tees while at the same time continuing to work as a captain. The couple produced seven children and moved to Hull in 1883 where Robert continued his business.<ref name="history"/>

Robert was joined in the business by his three sons, John Robert (Bob), Ernest Bertie (Bert) and Herbert Dobson (Herbie), and continued to expand his fleet with the addition of several steamers. In 1908 he came ashore to manage the business from the office.

===1914–1918===

Due to an increased demand for freight tonnage during the [[World War I|Great War]], Robert Rix & Sons increased revenue and profit throughout the period and invested in a ship building programme to modernise its fleet. The company also offloaded a number of older steamers which were fetching higher prices than in peacetime. In 1916 and 1917 Robert Rix & Sons took delivery of two pairs of [[steamships]] from Cochrane & Sons Ltd in [[Selby]], which for the first time bore the family name. These were the Magrix, the Robrix, and Jarrix and the Ebbrix and they set the precedent by which all Rix ships would be named from then on.<ref name="history"/>

===1920s – 1950s===

The early 1920s were a difficult period for ship owners due to the depressed economic conditions and the increasing popularity of [[rail freight]], but Robert Rix & Sons continued to grow its fleet with the addition of six ships, many of which were used to work the east coast coal trading routes.

In November 1925 Robert Rix died following a cerebral haemorrhage, aged 84. He had worked right up to his death, leaving the office the day before along with the rest of the staff.

In 1927 the roots of the largest company in the J.R. Rix & Sons group, Rix Petroleum Ltd, were established when Robert Rix & Sons began importing tractor vaporising oil to the UK to fuel the post-war agricultural revolution. The company also carried lamp oil from Russia to the Humber on Rix ships. This led to such an expansion that by 1939 the company owned 11 ships spread across four companies.<ref name="history"/>

===1939 – 1947===

Freight rates increased again before the [[Second World War]] which meant the value of elderly steam coasters rose and Robert Rix & Sons sold a number of vessels. The company also lost two ships during the war; one, the Malrix hit a mine off [[Southend]] in December 1940 and the Pegrix sank off the [[Norfolk]] coast after colliding with Coast Line’s [[Normandy]] Coast.<ref name="history"/>

The company helped the war effort directly in several ways. The Ebbrix was taken for special duties on the east coast of [[Scotland]] as part of the invasion of [[Europe]] and Kenrix’s master, Capt. George Simison, was awarded the Atlantic Star medal for his participation in the Normandy Landings.

During the [[war]] years the shipping companies were run by Robert Kenneth Rix (Ken) and the petroleum business by John Leslie Rix (Les), both sons of Bob Rix. After the war, however, Malcolm and Geoffrey Rix, sons of Bert and Herbie respectively, both expressed an interest in joining the firm.

Bob felt the company was not big enough to support the extra family members and it was finally decided that the partnership should be dissolved. Bert and Herbie became equal partners in Robert Rix & Sons and Bob, Ken and Les left to establish J.R. Rix & Sons.<ref name="history"/>

J.R. Rix & Sons started life on January 1947 with a working capital of £7,000 and one motor ship, the Magrix, in offices in the [[Bank of England]] Chambers, Whitefriargate, Hull, but did not become a [[limited company]] until 14 February 1957. In the meantime, several Rix companies still in existence today were established including Rix Shipping Co Ltd (Rix Shipping), formed in March 1950, and Rix Shipping (Scotland) Ltd, formerly Piggins Rix Ltd, based in Montrose, Scotland.<ref name="history"/>

Piggins Rix Ltd grew out of Rix Limes Ltd which had been established to supply [[limestone]] mined in quarries in the East Riding of Yorkshire to the Scottish agricultural market. The lime was transported by sea from Hull to Montrose on Rix ships where it was discharged by J.M. Piggins Ltd. J.R. Rix & Sons bought the company – its first owned interest in Scotland – and renamed it Piggins Rix Ltd. A short time later Rix Petroleum established a fuel depot there to serve the offshore industry and its growing domestic, [[agricultural]] and [[haulage]] customers.

Piggins Rix Ltd was renamed Rix Shipping (Scotland) Ltd in January 2013.<ref>{{cite web|url=http://wck2.companieshouse.gov.uk//compdetails |title=Companies House WebCheck |publisher=Wck2.companieshouse.gov.uk |date= |accessdate=11 February 2014}}</ref>

==Rix Shipping Ltd==

Rix Shipping was formed on 23 March 1950, making it is the oldest company in the J.R. Rix & Sons Ltd group, however it did not become a limited company until 14 February 1957. It started out with a single ship, Magrix (2) but the company soon acquired two further vessels: Roxton, from [[Middlesbrough]] owners, and a ship bought from [[Sweden|Swedish]] owners which the company renamed as the Jarrix (2).

Rix Shipping Ltd established regular voyages for [[coal]] from [[Amble]], [[Blyth, Northumberland|Blyth]] and [[Goole]] for [[Teignmouth]], [[Exmouth]], [[Hayle]], [[Penryn, Cornwall|Penryn]], [[Falmouth, Cornwall|Falmouth]] and [[Penzance]], with back cargoes of [[china clay]] and stone chippings. Another regular cargo was sulphate of [[ammonia]] from the [[River Tyne|Tyne]] or [[Tees]] bound for [[Ipswich]] and [[Avonmouth]] and in 1958 Rix Shipping Ltd began carrying agricultural limestone from [[Whitby]] to North Eastern Scottish ports as well as [[chalk]], mined from [[Little Weighton]] in the East Riding of Yorkshire, from Hull.

Over the years the company continued to expand its fleet by both buying ships and commissioning new vessels to be built until by the early sixties it had four motor ships all under five years old. This enabled the business to trade with near continental ports as well as those in the [[Baltic region|Baltic]] and [[Bay of Biscay]]. Regular return cargoes included seasonal crops, including French onion sellers who, complete with bikes, were carried in the ship’s hold with the onions.<ref name="history"/>

Today Rix Shipping Ltd is based at the head office of parent firm, J.R. Rix & Sons Ltd, in Spyvee Street, Hull, and owns and operates a fleet of estuarial tank barges, coastal tankers and high speed crew transfer workboats. Its main business activities include carriage of petroleum products, carriage of engineers to maintain wind turbines and stevedoring services through Hull's [[Port of Hull|King George Dock]] and Montrose in Scotland.

Rix Shipping Ltd also owns 25 acres of secure storage and warehousing space close to King George Dock in the east of the city and significant areas of land and warehousing in Montrose.

==Rix Petroleum Ltd==

Rix Petroleum Ltd is the largest subsidiary of J.R. Rix & Sons Ltd which turned over £425.4&nbsp;million in 2011.<ref>{{cite web|url=http://www.thebusinessdesk.com/yorkshire/news/357109-green-energy-focus-for-rix.html |title=News / Green energy focus for Rix |publisher=Thebusinessdesk.com |date=9 August 2012 |accessdate=11 February 2014}}</ref> It is headquartered in Hull but has depots across the UK, from Aberdeen down to [[Coleshill, Warwickshire|Coleshill]] in the [[West Midlands (region)|West Midlands]].

[[File:Rix Fuel Tanker.jpg|thumb|right|A Rix fuel tanker]]
[[File:Disused filling station awaiting development - geograph.org.uk - 841314.jpg|thumb|right|A now closed Rix filling station in [[Hornsea]], [[East Riding of Yorkshire]].]]

The roots of Rix Petroleum go back to 1927 when Robert Rix & Sons began importing tractor vaporising oil and lamp oil from [[Russia]] to supply local garages. The war years saw the business expand and supply commercial diesel customers in the East Riding of Yorkshire and the road transport industry. Domestic customers also provided an opportunity for the business as many households converted to cheaper, oil-fired central heating and the agricultural sector opened up as farmers turned from horses to tractors.<ref name="history"/>

The company was incorporated in June 1956 operates a fleet of more than 100 tankers in the UK serving agriculture and agribusiness, hauliers and other fleet-vehicle operators and the residential [[Fuel oil|heating oil]] market customers.

Its red, white and blue logo has remained relatively unchanged for many decades and is a common site on tankers, particularly around [[Yorkshire]], but also in other regions where the business operates.

==Maritime Bunkering Ltd==

The demand for marine fuel also spawned another Rix company, Maritime Bunkering Ltd, which supplies marine fuels to vessels on the [[Humber Estuary]].

==Jordan & Co (Hull) Ltd==

Jordan & Co (Hull) Ltd is a car dealership based in Witham, close to central Hull that is well known locally as a Rix-owned business. The business was established in the early 1900s as a [[bicycle]] retailer based on High Street in Hull City Centre. In the 1920s it progressed to selling [[motorcycles]] and in the 1950s became a car retailer.<ref name="history"/>

In 1960 the roots of the modern day business were laid down after Rix Petroleum acquired the Jubilee filling station on Holderness Road in the city, which was also the site of a BMC car dealership. The location became the site of Jordans & Co (Hull) Ltd, where it remains to this day. Over time BMC vehicles lost out to more reliable Japanese brands and therefore the company began selling Datsun, which later became Nissan.

Jordans, as it is known locally, was recently the subject of a £300,000 investment in its showroom facilities to become the UK’s first ever [[Fiat]] "super centre".<ref>{{cite news|url=http://www.thisishullandeastriding.co.uk/Hull-car-dealership-Jordans-undergoing-pound-300k/story-16478972-detail/story.html#axzz2Lu4RAQMD |title=Hull car dealership Jordans undergoing £300k refit |work= Hull Daily Mail |date=4 July 2012 |accessdate=11 February 2014}}</ref>

==Hepworth Shipyard Ltd==

In 1977 J.R. Rix & Sons Ltd bought the [[Paull]]-based shipbuilder J.R. Hepworth & Co (Hull) Ltd and a formed new company, Hepworth Shipyard Ltd, which built small craft such as [[Fishing trawler|trawlers]] and [[tugs]] at its location on the north bank of the Humber. The yard successfully continued shipbuilding until 2012 and was for many years the last remaining shipbuilder on the Humber as all other yards had turned to repairs only. But after completing the 53m long Lerrix for Rix Shipping Ltd early last year, the company was closed and the premises was let to Dunston (Shipbuilders) Ltd, a new business dedicated to making [[aluminium]] boats for the wind farm industry.<ref>{{cite news|url=http://www.thisishullandeastriding.co.uk/Launch-help-economy-power/story-16226227-detail/story.html#axzz2Lu4RAQMD |title=Launch to help our economy power up |work=Hull Daily Mail  |date=30 May 2012 |accessdate=11 February 2014}}</ref>

==Fuelmate Ltd==

Along with its core services of supplying commercial [[Diesel fuel|diesel]] and residential heating oil, Rix Petroleum also provided UK fuel cards to help its customers manage their fuel costs effectively. However, as demand for fuel cards increased, J.R. Rix & Sons Ltd decided to create a specialist fuel card supplier to complement the services of Rix Petroleum; in 2008 it launched Fuelmate Ltd.

==Victory Leisure Homes Ltd==

Victory Leisure Homes Ltd is a holiday home and lodge manufacturer established by J.R. Rix & Sons in April 2009, after the demise of Cosalt Holiday Homes in the previous year.<ref>{{cite news|url=http://www.thisishullandeastriding.co.uk/Holiday-Homes-firm-officially-launched/story-11947261-detail/story.html#axzz2Lu4RAQMD |title=Victory Leisure Homes officially launched &#124; This is Hull |work=Hull Daily Mail |date=8 April 2009 |accessdate=11 February 2014}}</ref> Cosalt had occupied land adjacent to Rix Road Industrial Estate, a 12-acre site owned by Rix on Stoneferry in the east of Hull; when it ceased trading Rix decided to buy the factory, land and plant to create its own caravan and lodge manufacturing facility and increase the size of its land holding in the area to 20 acres.

Three years after being founded the business outgrew its initial base and moved to a modern manufacturing plant in [[Gilberdyke]], East Riding of Yorkshire, around 20 miles west of Hull city centre,<ref>{{cite web|url=http://www.thebusinessdesk.com/yorkshire/news/271642-factory-deal-for-caravan-maker.html |title=Property / Commercial Property / Factory deal for caravan maker |publisher=Thebusinessdesk.com |date=19 January 2012 |accessdate=11 February 2014}}</ref> the former home of modular buildings maker Britspace, which went into [[Administration (law)|administration]] in 2011.<ref>{{cite web|url=http://www.thebusinessdesk.com/yorkshire/news/207283-administrators-seek-buyer-for-britspace.html |title=Property / Commercial Property / Administrators seek buyer for Britspace |publisher=Thebusinessdesk.com |date=16 August 2011 |accessdate=11 February 2014}}</ref>

The business is run by former Cosalt Holiday Homes managing director, Peter Nevitt.

==Rix Sea Shuttle Ltd==

Rix Sea Shuttle is the most recently established business in the J.R. Rix & Sons group. It was founded in 2012 to provide high speed crew transfer boats for wind farm personnel and equipment from the east coast of the UK to offshore wind farms in the [[North Sea]].<ref>{{cite web|url=http://www.thebusinessdesk.com/yorkshire/news/369329-rix-launches-sea-shuttle-venture.html |title=Manufacturing / Rix launches sea shuttle venture |publisher=Thebusinessdesk.com |date=12 September 2012 |accessdate=11 February 2014}}</ref>

The business was created in response to the burgeoning renewables industry, particularly offshore [[wind farms]], much of which is centred round the Humber Ports complex of Hull, [[Immingham]] and [[Grimsby]].<ref name="thisishullandeastriding">{{cite news |url=http://www.thisishullandeastriding.co.uk/Rix-ship-future-shore-bright/story-16710995-detail/story.html#axzz2Lu4RAQMD |title=Rix: We had to ship out but future shore is bright |work= Hull Daily Mail |date=15 August 2012 |accessdate=11 February 2014}}</ref>

Rix Sea Shuttle owns five specially-constructed wind farm work boats built in [[Blyth, Northumberland]], and in [[Paull]] on the north bank of the Humber Estuary. The company plans to increase the size of its fleet by 2015.<ref name="thisishullandeastriding" />

==References==
{{reflist}}

==Further reading==
{{refbegin}}
*{{cite book|last1=Atkinson|first1=Graham|last2=Rix|first2=John|title=Rix Shipping|date=2014|publisher=Ships in Focus Publications|location=Longton, Preston, Lancs|isbn=9781901703597}}
{{refend}}

{{DEFAULTSORT:Rix and Sons}}
[[Category:Companies based in Kingston upon Hull]]