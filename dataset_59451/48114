{{Orphan|date=August 2012}}

{{Infobox person
| name = George Corn Tassel
| birth_name = 
| birth_date = 
| birth_place = 
| death_date = {{death date|1830|12|24}}
| death_place = Hall County, Georgia
| death_cause = Execution
| spouse = 
| residence = 
| nationality = [[Indigenous peoples of the Americas|Native American]]
| ethnicity = [[Cherokee]]
| known_for = 
}}
'''George Corn Tassel''' (Utsi'dsata) was a [[Cherokee]] (Cherokee: Tsalagi, Aniyvwiyaʔi) man illegally tried, convicted, and executed for murder by the State of Georgia on December 24, 1830. His case became the first Cherokee legal document to support Cherokee sovereignty, and by extension Native American sovereignty in general. This incident  led to the United States Supreme Court’s ruling in [[Worcester v. Georgia]], which affirmed that the states do not have jurisdiction in Native American territories.<ref>"George Tassel," Famous Cherokee. AAA Native Arts. 2008. Website: www.aaanativearts.com/cherokee/famous-cherokee.htm</ref><ref>"Pickens County," Roadside Georgia. Golden Ink. 2006. Website: roadsidegeorgia.com/county/pickens</ref>

==Murder charges==
In 1830 George Tassel was charged with the murder of another Cherokee man at Talking Rock.  Tassel had allegedly waylaid and killed Sanders “Talking Rock” Ford while under the influence of alcohol.

Talking Rock (now [[Pickens County, Georgia]]) was within the territories of the then-Cherokee nation and 50 miles from Hall County, where Tassel would be brought for his trial. Since the Cherokee Nation was considered sovereign by U.S. law, Tassel should have been tried in a Cherokee court.

==Trial and appeal==
Tassel was tried on November 22, 1830 in Gainesville, Georgia in a log cabin courthouse set in the center of town.  A jury of 12 white men found Tassel guilty of murder. Superior Court Judge Augustin Smith Clayton sentenced Tassel to be hanged by the neck until dead.<ref>Norton, William. Historic Gainesville & Hall County: An Illustrated History. Hall County Historical Society. Hall County, Georgia/Singapore. 2001.</ref><ref>"Winning and Losing," The Cherokee in North Georgia. About North Georgia. Golden Ink. 2006. Website: ngeorgia.com</ref><ref>Garrison, Tim Alan. The Legal Ideology of Removal: The Southern Judiciary and the Sovereignty of Native American Nations. University of Georgia Press. Athens, Georgia. 2009.</ref>

Tassel's case was appealed and went before a 'convention of judges" of Georgia superior courts, at that time the de facto Georgia Supreme Court. The judges who denied Tassel's appeal included Augustin S. Clayton [who had himself adjudicated Tassel's case], William Law, William H. Crawford, William H. Holt, L.Q.C. Lamar, Charles Daugherty, C.B. Strong, G.E. Thomas, L. Warren, John W. Hooper, and H. Warner.<ref>Garrison, Tim Alan. The Legal Ideology of Removal: The Southern Judiciary and the Sovereignty of Native American Nations. University of Georgia Press. Athens, Georgia. 2009.</ref>

At the behest of Cherokee Chief [[John Ross (Cherokee chief)|John Ross]], U.S. Attorney-General [[William Wirt (Attorney General)|William Wirt]] brought up Tassel’s appeal before the Supreme Court. The Supreme Court accepted the appeal and issued a mandate forbidding Tassel’s execution. The Court also ordered the state to produce the records of the trial. They would also order Georgia Governor George Gilmer to appear before them in January 1831.<ref>Garrison, Tim Alan. The Legal Ideology of Removal: The Southern Judiciary and the Sovereignty of Native American Nations. University of Georgia Press. Athens, Georgia. 2009.</ref>

Gilmer responded by calling together an emergency session of the state's General Assembly on Dec. 22, 1830. In that session, laws were enacted nullifying any contracts between white and Cherokee people, requiring white residents of the Cherokee Nation to take a pledge of loyalty to the State of Georgia, and establishing a system whereby any white employees of the Cherokee had to obtain a permit from the State of Georgia to work for them.<ref>Abbott, Belle Kendrick. "The Cherokee Indians in Georgia," The Atlanta Constitution. Atlanta, Georgia. 1889.</ref>

Georgia, in the person of Clayton and after encouragement from Gilmer, refused the Supreme Court's demand for trial records and proceeded with Tassel's hanging on Dec. 24, 1830.<ref>"Winning and Losing," The Cherokee in North Georgia. About North Georgia. Golden Ink. 2006. Website: ngeorgia.com</ref><ref>Abbott, Belle Kendrick. "The Cherokee Indians in Georgia," The Atlanta Constitution. Atlanta, Georgia. 1889.</ref>

==Death==

On Christmas Eve morning, Sheriff Jacob Eberhart conducted Tassel from the Hall County Jail in an oxcart to gallows erected for the hanging in a field at the corner of Main and Grove streets, two blocks south of the log courthouse. Tassel rode to the hanging place atop his own coffin with tied hands and feet. Judge Clayton stood by Eberhart as he conducted the execution.<ref>Garrison, Tim Alan. The Legal Ideology of Removal: The Southern Judiciary and the Sovereignty of Native American Nations. University of Georgia Press. Athens, Georgia. 2009.</ref>

Spectators came to the hanging from miles around, doubling the number of inhabitants of the frontier town to about 500. According to eyewitness accounts, "Tassel rode up to the gallows sitting on his coffin, ascended the low scaffold without a tremor, and talked with great calmness to the crowd...." After Tassel was dead, his body was given to his Cherokee friends and he was buried several hundred yards away in the vicinity of what is now Bradford Street in Gainesville.<ref>Abbott, Belle Kendrick. "The Cherokee Indians in Georgia," The Atlanta Constitution. Atlanta, Georgia. 1889.</ref><ref>Garrison, Tim Alan. The Legal Ideology of Removal: The Southern Judiciary and the Sovereignty of Native American Nations. University of Georgia Press. Athens, Georgia. 2009.</ref>

==Aftermath and legacy==

Wirt would later argue the Cherokee Nation’s case unopposed in court on March 12 and 14, 1831.<ref>Abbott, Belle Kendrick. "The Cherokee Indians in Georgia," The Atlanta Constitution. Atlanta, Georgia. 1889.</ref>  The Supreme Court found that, as it pertained to a criminal matter within the geographic boundaries of the State of Georgia, the case for the Cherokee Nation and George Corn Tassel, three months dead and buried, lacked merit. Following the trial of George Corn Tassel, Georgia began enforcing its emergency session laws, which led to the imprisonment of Samuel "The Messenger" Worcester, bringing about the case of Worcestor v. Georgia in 1832.<ref>"Winning and Losing," The Cherokee in North Georgia. About North Georgia. Golden Ink. 2006. Website: ngeorgia.com</ref><ref>"Winning and Losing," The Cherokee in North Georgia. About North Georgia. Golden Ink. 2006. Website: ngeorgia.com</ref>

==In popular culture==

In 1966, during the height of the [[Civil Rights Movement]], Gainesville, Georgia, held its first annual crafts festival named the Corn Tassel Festival. The name continued to be in use until 1993 when, in light of the discovery of Tassel's history, the name was changed, by the Gainesville Jaycees, to Mule Camp Festival after the Cherokee trader outpost of the same name which became Gainesville.<ref>Fruhlinger, Joshua. The Comics Curmudgeon. 2011. Website: joshreads.com</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Tassel, George Corn}}
[[Category:1830 deaths]]
[[Category:Cherokee Nation (1794–1907)]]
[[Category:Year of birth unknown]]