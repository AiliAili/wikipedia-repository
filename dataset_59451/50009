{{Infobox character
| name        = Brandon Stark
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Isaac Hempstead Wright]]<br />(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Bran Stark - Isaac Hempstead-Wright.jpeg
| caption     = [[Isaac Hempstead Wright]] as Bran Stark
| first       = '''Novel''': <br />''[[A Game of Thrones]]'' (1996) <br />'''Television''': <br />"[[Winter Is Coming]]" (2011)
| last        =
| occupation  =
| alias       =
| gender      = Male
| title       = Lord of [[Winterfell]] (acting) <br> Prince of Winterfell
| family      = [[House Stark]]
| relatives   = [[Ned Stark]] {{small|(father)}}<br />[[Catelyn Stark|Catelyn Tully]] {{small|(mother)}}<br />[[Robb Stark]] {{small|(brother)}}<br />[[Sansa Stark]] {{small|(sister)}}<br />[[Arya Stark]] {{small|(sister)}}<br />[[Rickon Stark]] {{small|(brother)}}<br />[[Jon Snow (character)|Jon Snow]] {{small|(half-brother; books) <br>(paternal cousin/foster brother; TV series)}}<br />Brandon Stark {{small|(uncle)}}<br />[[Benjen Stark]] {{small|(uncle)}}<br />[[Lyanna Stark]] {{small|(aunt)}}<br />[[Lysa Arryn|Lysa Tully]] {{small|(aunt)}}<br />[[Edmure Tully]] {{small|(uncle)}}<br />[[Robert Arryn|Robert/Robin Arryn]] {{small|(maternal cousin)}}
| lbl21       = Kingdom
| data21      = [[The North (A Song of Ice and Fire)|The North]]
}}

'''Brandon Stark''', typically called '''Bran''', is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''. Martin told ''[[Rolling Stone]]'' in 2014 that Bran's chapter with [[Jaime Lannister|Jaime]] and [[Cersei Lannister]] is what "hooked" many readers early in the first novel.<ref name="RS GRRM 2014-04">{{cite web |url=http://www.rollingstone.com/tv/news/george-r-r-martin-the-rolling-stone-interview-20140423? |title=George R.R. Martin: The ''Rolling Stone'' Interview |first=Mikal |last=Gilmore |work=[[Rolling Stone]] |date=April 23, 2014 |accessdate=November 18, 2014}}</ref>

Introduced in 1996's ''[[A Game of Thrones]]'', Bran is the son of [[Ned Stark]], the honorable lord of [[Winterfell]], an ancient fortress in [[the North (A Song of Ice and Fire)|the North]] of the fictional kingdom of [[Westeros]]. He subsequently appeared in Martin's ''[[A Clash of Kings]]'' (1998) and ''[[A Storm of Swords]]'' (2000). Bran was one of a few prominent characters that were not included in 2005's ''[[A Feast for Crows]]'', but returned in the next novel ''[[A Dance with Dragons]]'' (2011).

Bran is portrayed by [[Isaac Hempstead Wright]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/bran-stark/bio/bran-stark.html |title=''Game of Thrones'' Cast and Crew: Bran Stark played by Isaac Hempstead Wright |publisher=[[HBO]] | accessdate=November 15, 2014}}</ref>

== Character description ==
Bran is seven years old at the beginning of ''A Game of Thrones'' (1996). He is the second son of Eddard "Ned" Stark and his wife [[Catelyn Tully|Catelyn]], and has five siblings: [[Robb Stark|Robb]], [[Sansa Stark|Sansa]], [[Arya Stark|Arya]], [[Rickon Stark|Rickon]], and the illegitimate [[Jon Snow (character)|Jon Snow]]. Bran is constantly accompanied by his [[direwolf]], Summer. Martin describes Bran as favoring his mother in appearance, having the thick auburn hair and deep blue eyes of the [[House Tully|Tullys]].<ref name=gotchap7>{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_7 |title=''Game of Thrones'' Chapter 7| website=Westeros.org}}</ref>

According to Martin, Bran is a sweet and thoughtful boy, well-loved by everyone at Winterfell. He enjoys climbing and exploring the walls and ramparts of the [[castle]];<ref name=gotchap6>{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_6 |title=''Game of Thrones'' Chapter 6| website=Westeros.org}}</ref><ref name=gotchap8>{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_8 |title=''Game of Thrones'' Chapter 8| website=Westeros.org}}</ref> he is also dutiful and tough-minded.

With his dreams of being a knight dashed by the crippling attempt on his life in ''A Game of Thrones'', duty forces Bran to overcome his new limitations and embrace his new abilities.<ref name="Spark Clash">{{cite web | url=http://www.sparknotes.com/lit/a-clash-of-kings/canalysis.html#Bran-Stark |title=''A Clash of Kings'': Analysis of Major Characters (Bran Stark)| publisher=[[SparkNotes]] |accessdate=November 18, 2014}}</ref> His gradual acceptance of his seemingly-prophetic dreams (called the "greensight") and his ability to inhabit Summer (which marks him as a type of skin-changer known as a [[warg]]) show his growing maturity and his worth beyond the loss of his legs.<ref name="Spark Clash"/>

== Overview ==
The very first—and youngest—[[Narration#Third-person|point of view]] character in the novels, Bran was set up by Martin as a young hero of the series. Mikal Gilmore of ''[[Rolling Stone]]'' noted in 2014 that the moment in ''A Game of Thrones'' in which Jaime Lannister pushes Bran to his likely death "grabs you by the throat".<ref name="RS GRRM 2014-04"/> Martin commented in the interview:

{{Quote|I've had a million people tell me that was the moment that hooked them, where they said, "Well, this is just not the same story I read a million times before." Bran is the first viewpoint character. In the back of their heads, people are thinking Bran is the hero of the story. He's young King Arthur. We're going to follow this young boy–and then, boom: You don't expect something like that to happen to him. So that was successful [laughs].<ref name="RS GRRM 2014-04"/>}}

In 2000, Martin called Bran the hardest character to write:

{{Quote|Number one, he is the youngest of the major viewpoint characters, and kids are difficult to write about. I think the younger they are, the more difficult. Also, he is the character most deeply involved in magic, and the handling of magic and sorcery and the whole supernatural aspect of the books is something I'm trying to be very careful with. So I have to watch that fairly sharply. All of which makes Bran's chapters tricky to write.<ref name=scifi_magical_tale>{{cite journal |last=Robinson |first=Tasha |url=http://www.scifi.com/sfw/issue190/interview.html |archiveurl=https://web.archive.org/web/20020223190420/http://www.scifi.com/sfw/issue190/interview.html |archivedate=February 23, 2002 |title=Interview: George R.R. Martin continues to sing a magical tale of ice and fire |journal=[[Science Fiction Weekly]] |publisher=[[scifi.com]] |issue=190 |volume=6, No. 50 |date=December 11, 2000 |accessdate=February 2, 2012}}</ref>}}

''[[Booklist]]'' cited Bran as a notable character in 1999,<ref name="Booklist Clash">{{cite web|url=http://vufind.uhls.org/vufind/Record/929050/Reviews |archive-url=https://archive.is/20140727035042/http://vufind.uhls.org/vufind/Record/929050/Reviews |dead-url=yes |archive-date=2014-07-27 |title=Reviews: ''A Clash of Kings'' |first=Roberta |last=Johnson |work=[[Booklist]] |date=January 1999 |accessdate=July 25, 2014 }}</ref> and the ''[[Publishers Weekly]]'' review of ''A Game of Thrones'' noted, "It is fascinating to watch Martin's characters mature and grow, particularly Stark's children, who stand at the center of the book."<ref name=pw_review_agot>{{cite web |url=http://www.publishersweekly.com/978-0-553-10354-0 |title=Fiction review: ''A Game of Thrones'' |work=[[Publishers Weekly]] |date=July 29, 1996 |accessdate=August 5, 2014}}</ref>

Noting Bran's absence in 2005's ''[[A Feast for Crows]]'', [[James Poniewozik]] of ''[[Time (magazine)|Time]]'' wrote in his review of ''[[A Dance with Dragons]]'' (2011):
{{Quote|Some favorite characters were [[missing in action|MIA]] for eleven long years. ''ADWD'' brings them back—bastard warrior Jon Snow, exiled dragon queen [[Daenerys Targaryen]], fugitive dwarf [[Tyrion Lannister]] and crippled, mystical Bran Stark, among others—and almost from the get-go that gives it a narrative edge over its companion book. Each, in his or her own way, is dealing with a question of power.<ref name=time_problem_power>{{cite web |last=Poniewozik |first=James |authorlink=James Poniewozik |url=http://entertainment.time.com/2011/07/12/the-problems-of-power-george-r-r-martins-a-dance-with-dragons/ |title=The Problems of Power: George R.R. Martin's ''A Dance With Dragons'' |work=[[Time (magazine)|Time]] |date=July 12, 2011 |accessdate=January 21, 2012}}</ref>}}

== Storylines ==
[[File:Stark Coat of Arms.png|thumb|150px|alt=A coat of arms showing a gray wolf on a white field.|Coat of arms of House Stark]]

=== ''A Game of Thrones'' ===
In ''[[A Game of Thrones]]'' (1996), Bran accidentally sees Queen [[Cersei Lannister]] and her brother Ser [[Jaime Lannister|Jaime]] having sex; whereupon he is pushed from the window by Jaime to keep the incest a secret, but he survives in a coma.<ref name=gotchap8 /> While Bran remains unconscious, an attempt is made on his life,<ref name=gotchap14>{{cite web | url=http://awoiaf.westeros.org/index.php/A_Game_of_Thrones-Chapter_14 |title=''Game of Thrones'' Chapter 14 |website=Westeros.org}}</ref> and [[Catelyn Stark|Catelyn]] delays the assassin long enough for Bran’s direwolf to kill him. Senseless, Bran dreams of his falling from the tower and of a three-eyed crow that offers to teach him to fly. With the crow's guidance, Bran wakes; but having been crippled by the fall, he is unable to walk. Thereafter he relies on the giant simpleton [[Hodor (character)|Hodor]], and a harness designed by [[Tyrion Lannister]], to move. When [[Robb Stark|Robb]] rides south to relieve [[Ned Stark|Ned]]'s arrest in [[King's Landing (A Song of Ice and Fire)|King's Landing]], Bran becomes the acting Lord of Winterfell.

=== ''A Clash of Kings'' ===
1998's ''[[A Clash of Kings]]'' finds Robb named King in the North, and Bran, as Robb's heir, rules the castle in his brother's absence.<ref name="Spark Clash"/> When [[Theon Greyjoy]] betrays the Starks and captures Winterfell, Bran and [[Rickon Stark|Rickon]] escape, aided by the [[wildling (character)|wildling]] [[Osha (character)|Osha]]. To hide his failure, Theon has two other children murdered and proclaims them to be Bran and Rickon. Theon himself is betrayed by [[Ramsay Snow]], the bastard son of [[Roose Bolton]]. Having been hiding in the crypts of Winterfell, Bran and his companions emerge to find the castle in ruins. They come upon a mortally wounded Maester Luwin, who advises their traveling party to split. Osha takes Rickon in the direction of White Harbor, while Bran, Hodor, [[Meera Reed|Meera]], and [[Jojen Reed]] set off north to seek the three-eyed crow. Meanwhile, Bran has slowly accepted the veracity of his dreams, and his ability to psychically inhabit Summer, which makes him a type of skin-changer known as a [[warg]].<ref name="Spark Clash"/>

=== ''A Storm of Swords'' ===
Bran, Hodor, Meera and Jojen travel north to [[the Wall (A Song of Ice and Fire)|the Wall]] in search of the three-eyed crow in ''[[A Storm of Swords]]'' (2000).

=== ''A Dance with Dragons'' ===
In ''[[A Dance with Dragons]]'' (2011), Bran, Hodor, Meera and Jojen are joined by the mysterious [[Coldhands (character)|Coldhands]], and a Child of the Forest named Leaf takes them to the three-eyed crow (actually a human telepath), who in turn offers to train Bran in [[clairvoyance]].

=== Family tree of House Stark ===
* See: [[Family tree of House Stark|Extended family tree of House Stark]]

{{Family tree of Rickard Stark}}

== TV adaptation ==
[[File:Isaac Hempstead Wright by Gage Skidmore.jpg|thumb|right|upright|[[Isaac Hempstead Wright]] plays the role of Bran Stark in the [[Game of Thrones|television series]].]]
Bran Stark is played by [[Isaac Hempstead Wright]] in the television adaption of the series of books.

===Storylines===
Brandon "Bran" Stark is the second son and fourth child of Eddard and Catelyn Stark. He was named after his deceased uncle, Brandon.

====Season 1====
Bran receives one of a litter of recovered direwolves given to the Stark childen and names him Summer. During the King's visit to Winterfell, Bran accidentally interrupts the Queen, Cersei, having sex with her brother, Jaime, who shoves him from the window. While he is unconscious and recovering from his injuries, Summer kills an assassin sent to murder Bran. When he awakens Bran cannot recall the events before his fall and finds that he is crippled from the waist down, forced to be carried everywhere by the stableboy Hodor. Slowly, he realizes that he has gained the ability to assume Summer's consciousness, making him a warg or a skinchanger. After his older brother, Robb, is crowned King in the North, Bran becomes Robb's heir and the Lord of Winterfell.

====Season 2====
After Theon Greyjoy captures Winterfell, Osha helps Bran and his younger brother Rickon go into hiding. To cement his claim on Winterfell, Theon kills two orphan boys and passes their bodies off as Bran and Rickon. After Theon's men betray him and Winterfell is sacked, Bran, Rickon, Hodor, Osha and their direwolves head north to find his older brother Jon Snow for safety.

====Season 3====
Bran and his group encounter Jojen and Meera Reed, two siblings who aid them in their quest. Jojen shares Bran's "greensight", and tutors him in his prophetic visions. After coming close to the wall, Osha departs with Rickon for Last Hearth while Bran insists on following his visions beyond the Wall. He also encounters Sam and Gilly, who tries to persuade him not to, but Bran claims it is his destiny and leaves through the gate with Hodor and the Reeds.

====Season 4====
During their travels beyond the Wall, Bran and his group stumble across Craster's Keep, where they are captured and held hostage by the Night's Watch mutineers led by Karl Tanner. Night's Watch rangers led by Jon eventually attack Craster's Keep to kill the mutineers, but Locke, a new recruit but secretly a spy for Roose Bolton, attempts to take Bran away and kill him elsewhere. Bran wargs into Hodor and kills Locke by snapping his neck, but Bran and his group are forced to continue on their journey without alerting Jon, whom Jojen claims would stop them. Bran's group eventually reaches the heart tree, but are set upon by wights outside the entrance. Jojen is killed in the attack, but the Children of the Forest destroy the wights and lead Bran and his company into the cave to meet the three-eyed raven. The Three-Eyed Raven declares that he will not walk again, but will fly instead.

====Season 6====
As part of his training, Bran is shown several visions of the past, including Ned Stark and Howland Reed confronting Ser Arthur Dayne and Ser Gerold Hightower at the Tower of Joy, and learns of how the Children of the Forest injected one of the First Men with dragonglass in a ritual to create the Night King, the first White Walker, as a defense against the other First Men. However, the Three-Eyed Raven is always quick to withdraw Bran from the visions, warning that he may become trapped in them if he stays too long. Growing bored with his slow progress, Bran enters a vision on his own and witnesses the Night King in the present day, who sees Bran and marks him, making the Three-Eyed Raven's cave vulnerable to the White Walkers' magic.

The Three-Eyed Raven enters Bran into another vision of Winterfell's past to impart all his knowledge, but before the transfer is completed the White Walkers attack the cave, killing the Three-Eyed Raven, Summer, and the Children of the Forest. Bran, still caught in the vision, wargs into Hodor through his younger self (named Wylis), and he and Meera flee as he carries his unconscious body out of the cave. Meera carries Bran into the forest and Hodor gives his life to hold back the cave door against the army of wights until they overwhelm him. Bran witnesses how his warging accidentally linked Hodor's past and present mind, inducing a seizure in young Wylis and causing him to repeat Meera's command to "hold the door" until he can only slur the word "Hodor".

After the wight army catches up to them again, Bran and Meera are rescued by Bran's uncle Benjen Stark, who had been killed by the White Walkers several years prior but was revived by the Children. Benjen whisks the duo to safety, telling Bran that he is now the Three-Eyed Raven and must learn to control his powers before the Night King attacks the Seven Kingdoms. Benjen leaves Bran and Meera at the weirwood in the Haunted Forest, as the Wall's magic prevents the dead from passing. Bran touches the weirwood and witnesses the rest of the vision of Ned Stark at the Tower of Joy. He discovers that Lyanna Stark died giving birth to Rhaegar Targaryen's son, [[Jon Snow (character)|Jon Snow]].

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Stark, Bran}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional characters with disability]]
[[Category:Fictional nobility]]
[[Category:Fictional orphans]]
[[Category:Child characters in television]]