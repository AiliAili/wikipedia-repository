{{italic title}}
{{taxobox
|image = Dendrophthoe falcata in Hyderabad, AP W IMG 0462.jpg
|image_caption = ''Dendrophthoe falcata'' in Hyderabad , India.
|regnum = [[Plant]]ae
|unranked_divisio = [[Angiosperms]]
|unranked_classis = [[Eudicots]]
|unranked_ordo = [[Core eudicots]]
|ordo = [[Santalales]]
|familia = [[Loranthaceae]]
|genus = ''[[Dendrophthoe]]''
|species = '''''D. falcata'''''
|binomial = ''Dendrophthoe falcata''
|binomial_authority =([[Carl Linnaeus the Younger|L.f.]]) Ettingsh
|}}

'''''Dendrophthoe falcata''''' (L.f) Ettingsh is one of the [[Hemiparasite|hemiparasitic]] plants that belong to the ''[[Loranthaceae]]'' family of [[mistletoe]]s. It is the most common of all the mistletoes that occur in [[India]]. At the moment reports say that it has around 401 plant hosts. The genus ''Dendrophthoe'' comprises about 31 species spread across tropical [[Africa]], [[Asia]], and [[Australia]] (Flora of China, 2003) among which 7 species are found in India.

''D.falcata'' bears grey barks, thick [[coriaceous]] leaves variable in shape with stout flowers (Wealth of India. 2002). The flowering [[inflorescence]]s in D.falcata was previously referred to as axillary or as one developing on the scars of fallen leaves, but Y.P.S Pundir (1996) verified it to be of strictly [[Cauliflory|cauliflorous]] nature and also notified that it shares fundamental similarity to that of ''[[Ficus glomerata]], F. pomifera'' and ''F. hispida''. Two of its varieties are widespread in India namely, var. ''falcata'' ('''honey suckled mistletoe''') and var. ''coccinea'' ('''red honey suckled mistletoe''') distinguished by occurrence of white and red flowering, respectively (Flowers of India). To date, ''D.falcata'' represents the only known mistletoe with the largest global [[host range]] (Calvin and Wilson, 2009) which is continuously and rapidly widening.

== Host-parasite interface==
Among [[angiosperm]]s, parasitic relationship through the formation of [[Haustorium|haustorial]] linkages is known to be widespread (Wilson and Calvin, 2006). In general, haustorial connections among 72 (of the 75) ariel parasitic genera may belong to either of the four types viz., epicortical roots (ERs), clasping unions, [[wood rose]]s, and bark strands (Calvin and Wilson, 1998). ERs may run along the host branches in either direction forming haustorial structures at variable intervals while “unions” occur as single points of attachment of individual [[parasite]]s hence pronounced as solitary. In ''D.falcata'' on different hosts two of the haustorial kinds have been observed viz., solitary unions as on Sugar apple (''[[Annona squamosa]]''), and epicortical roots as on Sapota (''[[Achras zapota]]''), guava (''[[Psidium guajava]]''), pomegranate (''[[Punica granatum]]'') have been known. It is unknown about what factors decide formation of different haustorial types by the leafy mistletoe on different hosts.
The host branches infected with ''D.falcata'' show a gradual reduction in growth and diameter as compared to other healthy uninfected branches (Karunaichamy et al., 1999). This mistletoe does not have an indigenous rooting system and is dependent on the host for water and minerals. [[Nutrient]] dynamics have shown that a higher titre of N, P, K, Mg and Na in the leaves of mistletoe than the leaves of uninfected and infected hosts which may be due to differential translocation of elements within the host [[phloem]] (Surya Prakash et al., 1967; Karunaichamy et al., 1999). The haustorial connections of the parasite with the plant are devoid of any efficient retranslocation system (Smith and Stewart, 1990).

== Seed dispersal and pollination==
Seed dispersal and [[pollination]] is usually mediated by the birds that thrive on fruits from the parasite and/or host. Particularly in southern India, [[Dicaeum erythrorhynchos|Tickel’s flowerpecker]] (also called the pale-billed flowerpecker) is reported to facilitate seed dispersal of ''D. falcata'' among [[Neem]] through fecal excretions or [[Regurgitation (digestion)|regurgitation]]s (Karunaichamy et al., 1999; Hambali, 1977 and references therein). Studies conducted at the higher altitudes of the [[Western Ghats]] (where both the mistletoes and the flowerpeckers occur predominantly), which parallel the western coast of India infer that the flowerpecker pollinated mistletoes have particularly developed feature specialized to attract a unique vector both to facilitate pollination and seed dispersal: the fruit and flowers have similar resemblance and more significantly, the fruiting time overlap with the next flowering season (Davidar, 1983). The [[hair-crested drongo]] (sometimes called the spangled drongo) and [[sunbird]]s are also known to feed on the nectar from the ''D.falcata'' flowers adding to the list of pollinators to this mistletoe (Kunwar et al., 2005).

== Medicinal uses ==
''Dendrophthoe falcata'' possesses remarkable potentials as a [[medicinal plant]] evident from the [[wound healing]], [[anti-microbial]], [[anti-oxidant]], [[antinociceptive]] properties of its [[ethanol]]ic extracts (Pattanayak and Sunita, 2008, Shihab et al., 2006). Medicinal properties of this hemiparasite may vary in effects respective to different hosts it establishes a relation with (Mallavadhani et al., 2006).

The whole plant is used in indigenous system of medicine as cooling, bitter, [[astringent]], [[aphrodisiac]], [[narcotic]] and [[diuretic]] (Alekutty e al., 1993) and is useful in treating [[pulmonary tuberculosis]], [[asthma]], [[menstrual disorder]]s, swelling wounds, [[ulcer]]s, [[Kidney stone|renal]] and vesical calculi and vitiated conditions of [[Ayurveda|kapha and pitta]] (Anarthe et al., 2008; Sastry, 1952; Pattanayak et al., 2008 ). Also, the decoction of plant used by women as an anti-fertility agent has been evidenced to possess [[anticancer]] activity (Nadkarni, 1993). The leaf ethanolic extract significantly and dose dependently inhibits the [[acetic acid]] induced writhing in mice (Shihab et al., 2006) and has indicated a low level [[toxicity]] in the [[brine shrimp]] lethality assays. Besides, a more recent work by Pattanayak et al. (2008) shows significant tumor reduction in induced [[Mammary cancer|mammary]] [[carcinogenesis]] in [[Wistar rat|Wistar female rats]] when fed with hydroalcoholic extracts of ''D. falcata''.

==Diseases==
''Dendrophthoe falcata'' is susceptible to diseases such as [[leaf blight]] caused by ''[[Colletotrichum]]'' stage of ''[[Glomerella cingulata]]'' (Mohamed Ali and Florence, 1987).

==Hyper-parasitism==
''D.falcata'' can be parasitised by ''[[Scurrula cordifolia]]'' (another mistletoe) (Pundir, 1979). Similarly, ''[[Viscum orientale]]'' has also been reported to grow on ''D.falcata'' (Saxena, 1971). In another instance ''[[Cuscuta reflexa]]'' has been shown to act as a rival to the leafy mistletoe (Nath and Indira, 1975).

From a [[Conservation biology|conservation biologists’]] viewpoint mistletoes are considered as a keystone resource of [[biodiversity]] (Watson, 2001) and from that of an [[Ethnobiology|ethnobiologist’s]] and/or [[Pharmacology|pharmacologist’s]] (Pattanayak et al., 2008), they possess numerous [[Ethnomedicine|ethnomedicinal]] assets with prospects extending to promises even for use as an [[anti-tumor]] agent. Besides, a farmer’s perspective entails that they are notorious and devastating parasitic plants. Being backed by easy seed dispersal mediated by [[Frugivore|frugivorous]] birds, they continue to pose serious losses to economically valuable fruit trees, flowering plants and those with medicinal properties whether growing in forests, orchards or gardens (Sridhar and Rao, 1978).

== References==
{{refbegin|2}}
* Alekutty NA, Srinivasan KK, Gundu Rao P, Udupa AC, Keshavamurthy KR. Diuretic and antilithiatic activity of Dendrophthoe falcata. Fitoterapia 1993;64:325-31.
* Anarthe SJ, Bhalke RD, Jadhav RB, Surana SJ: In vitro antioxidant activities of methanol extract of Dendrophthoe falcata Linn. Stem. Biomed 3(2) July-September 2008. pp.&nbsp;182–189.
* Calvin, C. L., And C. A. Wilson. 1998. The Haustorial System In African Loranthaceae. In R. Polhill and D. Wiens [eds.], The mistletoes of Africa, 17–36. Royal Botanic Gardens, Kew, UK.
* Calvin, C.L., Wilson, C.A. 2009. Epiparasitism in Phoradendron durangense and P. falcatum (Viscaceae). Aliso, 27:1–12.
* Davidar, P. Similarity between Flowers and Fruits in some Flowerpecker Pollinated Mistletoes. Biotropica 15:32-37 (1983).
* Flora of China Vol. 5 Page 227. June 1, 2003 Published by Science Press (Beijing) and Missouri Botanical Garden Press. Online at www.EFloras.org.
* Hambali, G. G. (1977) On mistletoe parasitism. Proceedings of the 6th Asian-Pacific Weed Science Society Conference, Indonesia, 1977. pp.&nbsp;58–66
* Karunaichamy, Kstk; Arp, K. Paliwal and P. A (1999). "Biomass and nutrient dynamics of mistletoe (Dendrophthoe falcata) and neem (Azadirachta indica) seedlings.". Current Science 76 (6): 840–843.
* Kunwar, R.M., Adhikari, N., Devkota, M.P. Indigenous use of mistletoes in tropical and temperate region of Nepal, Banko Janakari 15:38-42 (2005).
* Mallavadhani, UV, Narasimhan, K, Sudhakar, AVS, Mahapatra, A, Li, W, Breemen, RBV. “Three New Pentacyclic Triterpenes and Some Flavonoids from the Fruits of an Indian Ayurvedic Plant Dendrophthoe falcata and Their Estrogen Receptor Binding Activity”, Chem. Pharm. Bull., 54:740-744 (2006).
* Mohamed Ali, M.I., and Florence, E.J.M., 1987. Transactions of the British Mycological Society, 88:275-277.
* Nadkarni K.M. 1993. Indian Materia Medica, vol. I, Popular Prakashan, pp.&nbsp;750
* Nath, V.R., Indira, S. 1975. Cuscuta-Reflexa a Rival to Dendrophthoe falcata in Home Gardens. Journal of the Bombay Natural History Society, 72:607-608.
* Pattanayak S.P. and Sunita P.; Wound healing, anti-microbial and antioxidant potential of Dendrophthoe falcata (L.f) Ettingsh. Journal of Ethnopharmacology, 120:241-247 (2008).
* Pattanayak, SP; Mazumder PM, Sunita P., 2008. Dendrophthoe falcata (Lf): a consensus review. Pharmacognosy Reviews 8: 359-368.
* Pundir, Y. P. S: On the cauliflorous mode of flowering cauliflory in Dendrophthoe falcata Lf Ettingsh Loranthaceae. World Weeds 3(1/2): 87-106 1996.
* Pundir, Y.P.S. !979. A note on the biological control of Scurrula cordifolia (Wall.) G. Don by another mistletoe in Sivalik Hills (India). Weed Research, 21:233 - 234
* Sastry B. N., “The Wealth of India (Raw Materials),” Vol. III, CSIR, New Delhi, India, 1952, p.&nbsp;34.
* Saxena, H.O. 1971 A parasite Viscum orientale on another Dendrophthoe falcata. J. Bombay Nat. Hist. Soc. 68: 502
* Shihab, HM, Iqbal, AM, Sukla, M, Methedi, MM, Kumar, SS, Masami, I, Jamal, US. Antioxidant, antinociceptive activity and general toxicity study of Dendrophthoe falcata and isolation of quercitrin as the major component. Oriental Pharmacy and Experimental Medicine, 6:355-360 (2006).
* Smith, S. and Stewart, G. R., Plant Physiol., 1990, 94, 1472–1476.
* Sridhar, T.S., and Rao, V.R. 1978. Dendrophthoe falcata, a menace to fruit orchards. Current Science 38: 908.
* The Wealth of India. 2002. Raw materials, Vol- III, 4th edition, Council of Scientific and Industrial Research New Delhi, *Reprinted by the Publication of Information Directorate, New Delhi, p.&nbsp;588.
* Watson, D. 2001. Mistletoe - a keystone resource in forests and woodlands worldwide. Annu. Rev. Ecol. Syst. 32: 219-249.
* Wilson, CA, Calvin, CL (2006) An Origin Of Aerial Branch Parasitism In The Mistletoe Family, Loranthaceae. American Journal of Botany 93(5): 787–796.
{{refend}}
[[Category:Flora of India]]
[[Category:Medicinal plants of Asia]]
[[Category:Loranthaceae]]