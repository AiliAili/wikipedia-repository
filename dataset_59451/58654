{{for|the women's lifestyle magazine|Women's Health (magazine)}}
{{for|the other academic journal of a similar name|Women's Health Issues (journal)}}
{{Infobox journal
| title = Journal of Women's Health
| cover =
| discipline = [[Women's studies]]
| abbreviation = J. Womens Health
| editor = Susan G. Kornstein
| publisher = [[Mary Ann Liebert, Inc.]]
| country =
| frequency = Monthly
| history = 1992–present
| impact = 2.032
| impact-year = 2015
| website = http://www.liebertpub.com/overview/journal-of-womens-health/42/
| link1 = http://online.liebertpub.com/loi/JWH
| link1-name = Online archive
| ISSN = 1540-9996
| eISSN = 1931-843X
| OCLC = 50229847
| LCCN = 2002213698
| CODEN = JWHOAQ
}}
The '''''Journal of Women's Health''''' is a monthly [[peer-reviewed]] [[healthcare journal]] focusing on women's [[health care]], including advancements in diagnostic procedures, therapeutic protocols for the management of diseases, and research in gender-based biology that impacts patient care and treatment.<ref name=Overview>{{cite web |title=Journal of Women's Health, about this publication: Overview |url=http://www.liebertpub.com/overview/journal-of-womens-health/42/ |publisher=[[Mary Ann Liebert, Inc.]] |accessdate=5 June 2015 }}</ref> The journal was established in 1992 and is published by  [[Mary Ann Liebert, Inc.]]. The [[editor-in-chief]] is Susan G. Kornstein ([[Virginia Commonwealth University]]). It is the official journal of the [[Academy of Women's Health]]<ref name=Overview /> and the [[American Medical Women's Association]].<ref>{{cite press release |author=<!--Staff writer--> |date=14 April 2008 |title=Journal of Women's Health named official journal of American Medical Women's Association ''(Mary Ann Liebert, Inc./Genetic Engineering News)'' |url=http://www.eurekalert.org/pub_releases/2008-04/mali-jow041408.php |location=New Rochelle, New York |publisher=[[American Association for the Advancement of Science|EurekAlert!, American Association for the Advancement of Science (AAAS)]] |accessdate=8 June 2015}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101159262 |title=Journal of Women's Health |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-06-05}}</ref>
* [[Current Contents]]/Clinical Medicine<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-06-05}}</ref>
* Current Contents/Social & Behavioral Sciences<ref name=ISI/>
* [[Science Citation Index]]<ref name=ISI/>
* [[Social Sciences Citation Index]]<ref name=ISI/>
* [[Prous Science|Prous Science Integrity]]
* [[Embase]]/Excerpta Medica<ref name=Embase>{{cite web |url=http://www.elsevier.com/online-tools/embase/about |title=Journal titles covered in Embase |publisher=[[Elsevier]] |work=Embase |accessdate=2015-06-05}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-06-05}}</ref>
* [[CINAHL]]<ref name=CINAHL>{{cite web |url=http://www.ebscohost.com/titleLists/ccf-coverage.htm |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2015-06-05}}</ref>
* [[PsycINFO]]<ref>{{cite web |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2015-06-05}}</ref>
* [[CAB Direct (database)#CAB Abstracts|CAB Abstracts]]
* [[CAB Direct (database)#Global Health database|Global Health]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.032, ranking it 3rd out of 40 journals in the category "Women's Studies".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of women's studies journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.liebertpub.com/overview/journal-of-womens-health/42/}}
* [http://academyofwomenshealth.org/ Academy of Women's Health]

{{DEFAULTSORT:Journal of Women's Health}}
[[Category:English-language journals]]
[[Category:Mary Ann Liebert academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1992]]
[[Category:Women's health]]
[[Category:Healthcare journals]]
[[Category:Academic journals associated with learned and professional societies]]

{{Womens-health-stub}}
{{med-journal-stub}}
{{Womens-journal-stub}}