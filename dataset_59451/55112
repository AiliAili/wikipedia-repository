{{refimprove
| date = November 2016
}}
{{Infobox school
| name             = Islamia English School
| native_name      = المدرسة الإسلامية الانجليزية
| image            = 
| caption          = 
| motto            = 
| streetaddress    = Defence Road
| city             = [[Abu Dhabi]]
| country          = UAE
| coordinates      =24.4759° N, 54.3767° E 
| schooltype       = Semi Private 
| fundingtype      = Non Profit organisation 
| established      = 1978
| principal        = Ms. Safi 
| staff            = 150+
| faculty          = 50+
| teaching_staff   = 50+
| grades           = FS-II - Yr-13
| avg_class_size   = 30
| ratio            = 
| system           = Pakistani, British.
| mascot           = 
| accreditation    = [[Edexcel]] (London), 
| picture          = 
| Location         = Defence Road, P.O.Box: 2157, Abu Dhabi.
| enrollment       =March - May 
| imagesize        = 
| schoolnumber     = +97126417773
| language         = English , Urdu , Arabic , Hindi
| campus           = 
| campus size      = 2000+
| campus type      = 
| school_colours   = Pale and Dark brown 
| homepage         = {{URL|http://www.islamia.ae}}
}}

'''Islamia English School''' ({{lang-ar|مدرسة الإسلامية الإنجليزية}}), abbreviated as '''IES''', is an English Medium School located in the Al-Dhafra neighborhood of [[Abu Dhabi]]. The majority of the students are children of Pakistani expatriates living in Abu Dhabi, with significant minorities of Indian, Bangladeshi, and Arab students as well. The students of the school are called IESians.

==Education==
The school used to be affiliated with Federal Board of Intermediate and Secondary Education (Islamabad, Pakistan)<ref>[http://www.fbise.edu.pk/ Federal Board of Intermediate & Secondary Education]</ref> for providing [[Secondary School Certificate|SSC]] and [[Higher Secondary (School) Certificate|HSSC]] education. But it no longer teaches the Pakistani curriculum. Its affiliated with [[Edexcel]] for providing [[IGCSE]] and [[GCE Advanced Level|IAL]] advanced education.

===Academic subjects===
The school was founded based on the principle of providing both secular (science, social science, modern languages) and Islamic education (Quran, Arabic language, Islamic studies) to students. It offers classes in Maths, Biology, Chemistry, Physics, Pakistan studies, Urdu, (and student's native language from Yr-5 onward) Arabic, Islamic studies, Social science, English language, English literature, ICT and Physical Education. From Yr-12 onward, students decide between Commerce and Science subjects.

=== Hifz-e-Quran Project ===
A special feature of the school is the Hifz-e-Quran-e Karim project. It envisages the memorizing of the [[Sacred|Holy]] [[Quran]] over a period of   5 years  without disturbing the regular studies of the pupils. This option is open to deserving and outstanding students - both boys and girls in Yr-4 only.

==Facilities==
The school provides the following facilities catering to the all-round development of the pupils.

=== <u>Physical Development</u> ===

# Children's Park for the Pre-Primary and Playgrounds (separate for boys and girls).
# Indoor courts for Table-Tennis and Squash.
# [[Gymnasium (school)|Gymnasium]] for Indoor games.
# Fitness training facilities.(not true)
# Training in Indoor games like Basketball, Volleyball, Badminton and Squash
# Training in outdoor games like Football and Cricket
# Summer Camps
# Regular instructions on Building Evacuation and Disaster Management (Fire & Earthquake Drills)

=== <u>Academic Development</u> ===

# Well equipped Science [[Laboratory|Labs]] for Physics, Chemistry, Biology and Design & Technology
# A General Science Lab for smaller grades
# A streamlined [[Information and communications technology|ICT]] lab with the best equipment (separate for all sections)
# A full fledged School Library
# Audio - Visual teaching Aids
# A spacious [[Auditorium]] for seminars and school functions, Multipurpose Hall and Resource Rooms for events
# [[Mosque]]
# Activity rooms for all Sections. Activity rooms are equipped with Audio-Visual Aids, provision of [[Overhead projector|OHP]] in classes, Multimedia, Computer, [[Television|TV]] and Record Players.

==Achievements==
In the 16th Inter-School Athletic championship, Islamia English School secured the third position.<ref>[http://gulfnews.com/news/gulf/uae/general/a-marathon-effort-1.436662 A marathon effort, By Ajitha Renganathan, Gulf News, December 12, 2000]</ref>

Many of IGCSE and IAL high acchievers are from Islamia English School.

The students of IES have been participating in various Public Speaking Competitions.

IES has won most of the UAE Inter-School Quiz competitions

IES received an overall Band B+ rating from ADEC inspection

==The Student Council==
They are selected from Grade 12. In order of Authority

1: Head Boy/Girl

2: President Student Council

3: Sports Captain

4: House Captains (Blue, Green, Red, Yellow)

5: Secretary (Cultural Activities, Literary Activities, Science Club, Eco Club,IT, Arts)

6: Prefects (Seven)

The student council also consists of their own deputies from Grade 11.

The posts are decided on a students academic achievements, his/her willingness to work for the school, and his discipline. The posts are chosen by the senior teachers and the Heads of the School. The Principal usually acts as an honorary head of office in sectional decisions.

==References==
{{reflist}}

{{Coord missing|United Arab Emirates}}

[[Category:Educational institutions with year of establishment missing]]
[[Category:Islamic schools in the United Arab Emirates]]
[[Category:Schools in Abu Dhabi]]