{{Use dmy dates|date=July 2011}}
{{Use Australian English|date=July 2011}}

'''Gwambygine Homestead'''  is one of the earliest Colonial Buildings still remaining in [[Western Australia]]. Until the death in 1998 of the last occupant, Merton Clifton, the house had the reputation of being the oldest continually occupied house in the state.

== Early history ==

[[File:Gwambygine Homestead, Western Australia.jpg|thumb|right|The heritage listed Gwambygine Homestead, c2012. Photo courtesy Ron Bodycoat]]Indigenous people were known to have frequented Gwambygine and the Pool for many thousands of years before European settlement. The fertile [[York, Western Australia|York]] district was explored by [[Robert Dale|Ensign Robert Dale]] in August 1830. Dale found the cave with Aboriginal markings which gave its name to Cave Hill, part of the original Gwambygine land grant. A large [[corroboree]] was recorded there in 1841. Today there are no registered Aboriginal sites within the Gwambygine farm property, but the [[Avon River (Western Australia)|Avon River]] and the Pool do have mythological significance. In 1861, an epidemic of measles in the [[Swan River Colony]] reduced the Aboriginal population, some of whom had worked at Gwambygine Farm.

Gwambygine Estate is one of the earliest rural Land Grants of the Colony in Western Australia. The Reverend [[John Burdett Wittenoom]] (1788-1855),<ref>O'Brien, Jacqueline & Drew-Statham, Pamela (2009). ''On we go the Wittenoom way : the legacy of a colonial chaplain'', Fremantle, W.A. : Fremantle Press, ISBN 9780646510033</ref> was the Colonial Chaplain to the new British Colony, founded by [[James Stirling (Royal Navy officer)|Lieutenant Governor James Stirling]] in 1829. The British feared that the French would claim the western part of Australia, prompting them to establish this colony.

Reverend Wittenoom saw the Swan River Colony as an opportunity for his sons to become owners of land and for him to carve out a new career. Wittenoom's first wife, Mary Teasdale whom he married in 1815, had died in England in 1824 following the death of their 5-year-old son Edward. Their deaths prompted John Burdett Wittenoom’s decision to move to Swan River. At the age of 42, accompanied by his sister Eliza and four sons John, Henry, Frederick and Charles, Wittenoom left England on 14 August 1829 on the Wanstead, a barque bound for the Swan River and Tasmania.

In 1830 Wittenoom took up Location Z, on the bank of the Avon River south of the York townsite. In 1835 the 5,000 acres were surveyed. The name Gwambygine, bestowed by the Wittenooms, comes from the indigenous [[Nyungar|Noongar]] (Ballardong) name of the deep permanent pool in the Avon River. This pool influenced the location of the Homestead, and it survives today behind the Homestead on the eastern side. The local Aborigines named the area around the pool Gwarbanginning. This is believed to mean ‘a good place to stay’ and so it has proved for the three main families who have lived here, the Wittenooms, Hicks and Cliftons. In colonial times the pool was slightly brackish, fine for stock. Most drinking water was obtained from springs or rainwater.

In 1836 the teenage sons of Reverend John Burdett Wittenoom, together with Thomas Carter an early settler from Norfolk, started building the first sections of the house. They constructed the mud house on the same principles Carter had seen used in England. Initially, Carter worked on the property undertaking improvement duties for the Reverend Wittenoom, and farmed wheat and sheep. The boys Aunt Eliza, who had emigrated with her brother, kept house for the boys through the late 30's and early 1840's and was joined by her 68-year-old mother Elizabeth in 1839.  Elizabeth died on the property in 1845 at the age of 74 and was buried in the old York cemetery.

The Reverend John Burdett Wittenoom’s second marriage in 1839 to Mary Helms produced two daughters, Mary Eliza and Augusta Henrietta, and a son, John Burdett who died at the age of two years. Reverend Wittenoom died on 23 January 1855 at the age of 66 years, a distinguished and much renowned clergyman and prominent member of the Swan River Colonial community. Apart from being the first Colonial Chaplain to the civil establishment of the colony, and ministering to scattered congregations around the Colony, Reverend Wittenoom was also responsible for the establishment of St. George's Church, the new Church in Perth, completed in 1845.

Before his death, John Burdett Wittenoom divided the Gwambygine Estate into the following parts:
* 100 acres in the northeast corner of the Estate to his sister Eliza Wittenoom; 
* 1,250 acres in the northwest corner of the Estate Knocklemony to his son John by his first marriage; John joined the police in the 1840s then left for the Victorian Goldfields where it is thought he died as no   further record has been found
* 2,400 acres to his youngest son Charles including the Homestead, upon his marriage to Sarah Harding, daughter of Captain James Harding, Assistant Harbour Master, who had arrived in the Colony at King George's Sound in 1846 with his wife and four children; Charles and Sarah had four children Edward, Frank Rose and James. 
* 2,250 acres to his son Frederick, who did not remain at the property, leaving the farm in 1840 to become a public servant and eventually Sheriff of Perth and dying unmarried in 1863;
* life residency at the Homestead for Henry, his invalid son, to remain at Gwambygine in the care of whoever occupied the Homestead.

After the death of his 27-year-old wife Sarah and baby son James from measles in 1861, Charles Wittenoom leased Gwambygine to Joseph Hicks senior who acted as farm manager and blacksmith at Gwambygine. The Hicks family had arrived with the Australinders and come to York seeking work.  Both Joseph senior and junior operated a smithy on the property and remnants can still be seen outside the stone barn just north of the Homestead. Aunt Eliza took the motherless children back to Perth and cared for them until adulthood. Eliza then went to live with her niece Augusta  who lived at the Bowes near Geraldton where she died in 1867 at the age of 75.

On Charles’ untimely death in 1866, aged only 42, he left the property to his two sons Frank and Edward Wittenoom who continued with the Hicks lease. Henry Wittenoom lived in the homestead with the Hicks family until his death in 1884 aged 64.

== Twentieth Century history ==

[[File:Gwambygine Eastern garden.jpg|thumb|right|The Eastern Garden of Gwambygine Homestead, c2012. Photo courtesy Ron Bodycoat]]In May 1901 the Western Australian Government bought Gwambygine Locations Z and Y from Charles’ son Frank Wittenoom for £7,650 for railway purposes after the opening of the [[Great Southern Railway (Western Australia)|Great Southern Railway]]. The land was then subdivided into about 60 ‘homestead’ blocks.

In 1923, Joseph Hicks junior, who was married to Rosina, purchased a number of these blocks.<ref>Bodycoat, Ronald AM LFRAIA. ''Gwambygine Homestead - An Historic Homestead Refurbished'', Unknown publisher & date</ref> Joseph Hicks junior put the titles to 519 acres, Lots 33, 34, 35 and Homestead Lot 36 of the Gwambygine Estate, which included the land around the Homestead, in the name of his unmarried daughter Henrietta Maria Hicks.

In 1899, Joseph Hicks junior's other daughter Florence married Claude Robert Henry Clifton, a northwest pastoralist. In 1901, Claude Clifton purchased 1,000 acres at Cave Hill.
In 1925, Brian Merton Clifton, born in 1902 as the second son of Claude and Florence Clifton, was working on the Gwambygine property. In 1929, Merton, as he was known, married Gwenyth Compton. Merton rented Gwambygine Homestead from his aunt, and they resided there, improving and altering it to their needs.

When Henrietta Hicks died in 1952 the Gwambygine property passed to Brian Merton Clifton. In 1973 he converted the landholding back to 550 acres around the old Homestead. Brian Merton Clifton died in 1998, and Gwambygine Homestead was closed. The building has been unoccupied since that time.

The original, central, structure of the house is made of rammed earth, a mix of clay, sand and straw, with a mud and lime render. Later additions, including the chimneys, are of fired brick. The walls are mostly 300&nbsp;mm thick. The Clifton’s daughter, Mrs Margaret (Maggie) Venerys now owns the property.

== Restoration ==

After Merton Clifton’s death in 1998 the building progressively deteriorated, with [[termites]] and water causing extensive damage.<ref>[http://riverconservationsociety.org/what-we-do/gwambygine-homestead-historical-site/ "Gwambygine Homestead – Historical Site"]. River Conservation Society/</ref> In 2004 Western Australian historian Pamela Statham-Drew, and Jacqueline O’Brien, a descendant of the Wittenoom family, began work on a new book on the history of the Wittenooms. Pamela’s work brought her to Gwambygine and lead to an interest in having the homestead restored.

In 2009, Pamela Statham-Drew  approached the River Conservation Society of York to help obtain funding to restore the building. Pamela's husband Nick was then treasurer of the Federation of Australian Historical Societies and heard through this body that part of the Rudd Governments job stimulus package was to be quarantined for Heritage projects if they were 'shovel ready'. Thanks to a conservation study undertaken by Heritage architect Ron Bodycoat and historian Robyn Taylor in 2006 such a plan was available.<ref>http://www.ydcm.com.au/2010/March/GWAMBYGINE%20HOMESTEAD.html, 'RESTORATION OF GWAMBYGINE HOMESTEAD GETS UNDER WAY', York & Districts Community Matters website, retrieved 2013-10-02</ref> Ron joined Pamela and Nick and Tony Clack of the River Conservation Society (the Gwambygine Group) in placing an application to the Federal Government for funds to undertake the restoration,<ref>[http://www.avonadvocate.com.au/story/1200557/avons-history-preserved-with-heritage-grant-funding/ "Avon’s history preserved with heritage grant funding"]. ''Avon Valley Advocate''. 
Dec. 21, 2012</ref> and another request was sent to the Western Australian Heritage Commission.<ref>[http://inherit.stateheritage.wa.gov.au/Public/Inventory/Details/3305127c-b84a-409c-a784-835394f932c8 "Gwambygine Farm and Pool]. Government of Western Australia Heritage Council.</ref> Federal Funding of $100,000 was confirmed in October 2009. The State Heritage Council granted a further $30,000. Conservation works started before the end of year, but were slowed during the months of extreme heat.

Restoration work began with the removal of seven white ant nests, then a total re-roofing. The restorers kept the underlying shingles but replaced the rusted Corrugated iron covering those shingles. They also repaired the timberwork and carefully and painstakingly in-filled damage to the mud walls and brickwork, which they then whitewashed with a special formulae.

A party was held on the December 4, 2010 to mark completion of the renovation,<ref>http://www.ydcm.com.au/2011/January/GWAMBYGINE.html, 'FUNCTION MARKS COMPLETION OF HOMESTEAD RESTORATION', York & Districts Community Matters website, retrieved 2013-10-02</ref> but a huge storm on January 29, 2011 took off the kitchen roof and destroyed the newly completed side out-house. Much of the original shingle roof was scattered and lost. It took the remainder of that year to repair the damage. And by then some of the original repairs needed attention again…

== Reopening ==

The Gwambygine group promised the Government that they would periodically open the restored house to the public, so it is open from April to October on every second Saturday of the month, and by appointment. The many visitors on Open Days help the society to keep alive this witness to 175 years of Western Australian history.

== The Building ==

[[File:Gwambygine Pool.jpg|thumb|right|Gwambygine Pool on the Avon River. Photo courtesy Ron Bodycoat]]Historically, Gwambygine Homestead is linked inextricably to the permanent Pool, Gwambygine Pool, in the Avon River that runs past the property at the eastern side of the Homestead. The Homestead is a typical Colonial rural farmhouse ; single-depth rooms accessible from verandas along both sides. The verandas provided shelter and protection, and a means of access to the rooms that formed the amenity of the homestead, sitting room, bedrooms, kitchen, storerooms, farm office, and so on. Gwambygine, with its high-pitched roof and shady verandas, conforms to the early pattern for rural Western Australian houses. It also shows the extensions carried out to provide additional accommodation for increased family needs and improved amenity over the life of the building.

As a building dating from 1836, the fabric reflects the means by which early buildings were constructed; mud walling from clay dug on the property plus sand and fermented straw, rough hewn timber roof framing cut on the property and supporting the original [[Xanthorrhoea|grasstree]] roof covering, later to be replaced with timber shingles, and later in the 19th century, when corrugated iron became available, with corrugated iron sheeting over the shingles for weatherproofing. Bricks made by convict labourers, who also built the stone barn, were used later to build chimneys and repair walls. Fireplaces provided heating in the rooms, and most survive today. In the 20th century concrete replaced the original timber-boarded floors that were laid onto the ground. These concrete floors exacerbated the rising damp that destroys the lowest section of the pug walling.

In 2010, the Gwambygine property is identified as Lot 36, No. 5561 Southern Highway, comprising 36.77 hectares (90.86 acres) incorporating Gwambygine Homestead, in the ownership of Margaret (Maggie) Venerys, the daughter of Brian Merton Clifton.

Former elements of Gwambygine Farm that have not survived the vicissitudes of time and decay due to flooding and earthquake, are the large shed north of the Homestead, the timber-framed building and the air raid shelter between the Homestead and the Pool, the duck yards, parts of the orchard which included olives, pears, figs and pomegranates, the vegetable garden and the jetty into the Pool. The sheep pens and pigsty were removed during the restoration when found to be full of white ant. The tennis court paved in rammed anthill, hard against the western side of the Homestead, is now open space but no longer suitable for tennis. Some former building onto the north side of the barn has also disappeared.

Other significant places associated with Gwambygine Farm and the Homestead is the nearby Hicks Siding on the railway, Hicks Siding School (Gwambygine School) and District Hall, all of which are now only historic sites. The Clifton family's ''Cave Hill House'' opposite Gwambygine Homestead to the west and ''Millbrook'' to the southwest, survive as early 19th century elements in the rural and social life of the locality.

Cave Hill, Gwambygine Hill and Sugarloaf Hill on the western side of the Great Southern Highway remain as identifiable natural features that played an important role in the life of the Gwambygine community. Blackjack dam, an important water source for the Gwambygine Estate; the many waterholes, the quarry and the caves at Cave Hill, were elements that featured strongly in the farming community and remain today as relevant historic sites associated in particular with the various owners of Gwambygine Estate.

== References ==

{{Reflist|1}}

== External links ==
*[http://www.york.wa.gov.au/visitors/seeanddo|York Shire Visitor Information]

{{coord|31|58|51.34|S|116|48|21.59|E|type:landmark_region:AU|display=title}}

[[Category:Heritage places of Western Australia]]
[[Category:Buildings and structures in Western Australia]]
[[Category:Wheatbelt (Western Australia)]]