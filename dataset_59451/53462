{{Infobox
|title   = CPA Australia
|image   = <br /><br />[[Image:CPA Australia logo 400x175.PNG]]
|label2  = Industry
|data2   = Accounting and Finance
|label3  = Founded
|data3   = {{flagicon|Australia}}Melbourne, [[Victoria, Australia]] ({{Start date|1886 as IIAV}})
|label4  = Head Office
|data4   = {{flagicon|Australia}}Melbourne, Australia 
|label5  = Locations
|data5   = {{flagicon|Australia}}[[Australia]], {{flagicon|EU}}[[Europe]], {{flagicon|Hong Kong}}[[Hong Kong]], {{flagicon|China}}[[China]] (mainly [[Beijing]], [[Shanghai]], and [[Guangzhou]]), {{flagicon|Indonesia}}[[Indonesia]], {{flagicon|Malaysia}}[[Malaysia]], {{flagicon|NZ}}[[New Zealand]], {{flagicon|Singapore}}[[Singapore]], {{flagicon|Vietnam}}[[Vietnam]], and {{flagicon|UK}}[[London]], [[United Kingdom]]
|label6  = Areas served
|data6   = Global
|label7  = President
|data7   =  Graeme Wade
|label8  = CEO
|data8   = Alex Malley
|label9  = Members
|data9   = 150,000
|label10 = Member's Designations
|data10  = CPA & FCPA
|label11 = Website
|data11  = [http://www.cpaaustralia.com.au/ www.cpaaustralia.com.au]
}}
{{Distinguish2|the [[Communist Party of Australia]]}}
'''CPA Australia''' (Certified Practising Accountants) is an [[accountancy|accounting]] body in [[Australia]], with over 150,000 members.<ref>{{Cite web|url=http://www.asic.gov.au/asic/asic.nsf/byheadline/Certificates+issued+by+a+qualified+accountant|title=Certificates issued by a qualified accountant|website=[[Australian Securities and Investments Commission]]|access-date=2016-04-18}}</ref>

Founded in 1886, CPA Australia is one of the world’s largest accounting bodies, with more than 150,000 members across 121 countries. Core services to members include education, training, knowledge exchange, technical support, networking and advocacy.'''
 
CPA Australia was an early entrant into the Asian market, where their involvement began in the early 1950s and was aimed at developing and strengthening the accounting profession in the region. Today almost one-quarter of CPA Australia’s members now reside outside of Australia, with over 35,000 in Asia.

CPA Australia currently has 19 staffed offices across Australia, China, Hong Kong, Malaysia, Singapore, Indonesia, Vietnam, New Zealand and the UK.<ref>{{Cite web|url=http://www.cpaaustralia.com.au/|title=CPA Australia|website=www.cpaaustralia.com.au|access-date=2016-04-18}}</ref>

==History==
The current form of CPA Australia dates from 1952, when the Commonwealth Institute and Federal Institute merged to create the Australian Society of Accountants. In July 1990 the name changed to the Australian Society of Certified Practicing Accountants, and in April 2000, the name became CPA Australia.<ref>CPA Australia Handbook, 1995, p11021</ref>

The main predecessor bodies of the Society, with year of formation, were:
*Incorporated Institute of Accountants, 1886 
*Federal Institute of Accountants, 1894
*Association of Accountants of Australia, 1910
*Australasian Institute of Cost Accountants, 1920

==Membership==
[[File:CPAMelbOffice.jpg|upright|thumb|CPA Australia's head office in Melbourne.]]

To become a CPA, candidates must hold a degree or a postgraduate award that is recognized by CPA Australia, and demonstrate competence in the required knowledge areas and, within a six-year period, successfully complete the CPA Program.

When a candidate applies to study the CPA Program, their previous education and qualifications are assessed to determine if they have covered the required knowledge areas to begin the CPA Program.

The required knowledge can be demonstrated by completing one or more of CPA Australia’s foundation exams, or completion of an accredited and recognised degree, such as an accounting degree. Such degrees will often meet all the required knowledge areas and allow candidates to commence the CPA Program.

The CPA Program is an integrated education and professional experience program with exceptionally high standards, and is recognized with ISO 9001 certification. It consists of 6 education subjects, 4 compulsory subjects and 2 electives, as well as a fully integrated practical experience requirement.

The CPA Program focuses on four main areas that ensure a CPA is valued by any employer – leadership, strategy, ethics, and governance. Content is globally relevant, with a focus on providing flexibility of learning and delivery modes. Overall, the CPA Program builds leadership, strategy, higher level analysis, judgment, decision making and reporting skills.

The three-year practical experience requirement that is part of the CPA Program has been designed in response to the needs of different stakeholders including employers, industry and graduates. The practical experience requirement further develops candidates’ technical, business, personal effectiveness and strategic leadership skills.

Upon renewing membership each year, members are required to declare their ongoing compliance with the CPA Australia constitution, by-laws and minimum continuing professional development (CPD) requirements. Fulfillment of 120 Continuing Professional Development (CPD) hours per triennium (3-year period) with a minimum of 20 CPD hours in each year is required for continued membership. Members must monitor their own CPD hours and CPA Australia also conducts random audits of members to confirm that they are meeting the CPD requirements.

Full members of CPA Australia use the designatory letters CPA, while senior members may become Fellows and use the letters FCPA.<ref name=about>[http://www.cpaaustralia.com.au/cps/rde/xchg/cpa-site/hs.xsl/about.html About CPA Australia]</ref>

==International affiliations==
CPA Australia has affiliations with a range of leading accounting bodies all around the world, to ensure that CPAs are internationally recognized. These include:

=== Pakistan ===
* [[The Institute of Cost & Management Accountants of Pakistan]]
* [[Institute of Chartered Accountants of Pakistan]]
* [[Pakistan Institute of Public Finance Accountants]]

===Canada===
* [[Certified General Accountants Canada]]
* [[Certified Management Accountants Canada]]

===Europe===
* [[Chartered Institute of Public Finance and Accountancy]]
* [[Chartered Institute of Management Accountants]]
* [[CPA Ireland]]: Institute of Certified Public Accountants in Ireland

===Hong Kong===
* [[Hong Kong Institute of Certified Public Accountants]].

===India===
* [[Institute of Chartered Accountants of India]].

===Singapore===
* [[Institute of Singapore Chartered Accountants]].

===Sri Lanka===
* [[Institute of Chartered Accountants of Sri Lanka]].
* [[Institute of Certified Management Accountants of Sri Lanka]].

===Malaysia===
* [[Malaysian Institute of Accountants]].

===United Kingdom===
* [[Aston University]].

===Others===
CPA Australia has signed a memorandum of cooperation (MoC) with the Chinese Institute of Certified Public Accountants and the Macau Society of Registered Accountants.

CPA Australia has also signed an MoC with the Vietnam Association of Certified Public Accountants and Auditors, the National Accounting Council of Cambodia, Kampuchea Institute of Certified Public Accountants and Auditors, and the Vietnam Association of Accountants and Auditors.

Members of these bodies can benefit from an exchange of training and continuing professional development programs.<ref>{{Cite web|url=http://www.cpaaustralia.com.au/become-a-cpa/international-affiliations|title=International affiliations|website=www.cpaaustralia.com.au|access-date=2016-04-18}}</ref>

==Arms==
{{Infobox COA wide
|image = CPA Australia Arms.svg
|bannerimage =
|badgeimage =
|notes = The arms of CPA Australia consist of:<ref>{{Cite book|last=Low|first=Charles|authorlink=Charles Low|title=A Roll of Australian Arms|year=1971|publisher=Rigby Limited|location=Adelaide|page=9|isbn=0-85179-149-2}}</ref>
|adopted = 1957
|crest = On a Wreath of the colors, five books erect proper.
|torse =
|helm =
|escutcheon = Azure, a Fess enhanced and in base a Pale Argent, over all a representation of the Southern Cross counterchanged.
|supporters =
|compartment =
|motto = Integrity
|orders =
|other_elements =
|banner =
|badge =
|symbolism = The T formed by the fess and pale represents a double-sided account, and thus double entry book-keeping.
|previous_versions =
}}

==See also==
* [[International Qualification Examination]]

==References==
{{reflist}}

==External links==
*[http://www.cpaaustralia.com.au/ CPA Australia's website]
*[http://www.cpaaustralia.com.au/become-a-cpa/b/about-the-program/ The CPA Program]
*[http://www.cpaaustralia.com.au/become-a-cpa/b/ Become a CPA]
*[http://www.cpaaustralia.com.au/become-a-cpa/free-student-network/ CPA Australia's student network]
*[https://www.youtube.com/CPAaustralia/ YouTube Channel]
*[http://twitter.com/cpaaustralia/ Twitter Page]
*[https://web.archive.org/web/20160109070929/https://www.facebook.com/group.php?gid=60984520543%2F Facebook Page]
*[http://slurl.com/secondlife/Bracket/51/98/28/?title=CPA%20AUSTRALIA%20CONGRESS%20CENTRE/ CPA Australia Congress Centre – Second Life]
*[http://www.ifacnet.com/ IFACnet]
*[https://www.flickr.com/photos/cpaaustralia/ Flickr page]
{{IFAC Members}}

{{DEFAULTSORT:Cpa Australia}}
[[Category:Accounting in Australia]]
[[Category:Professional accounting bodies|Australia, CPA]]