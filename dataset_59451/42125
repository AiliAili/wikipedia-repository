The '''Aero-Club des Cheminots Aerofer''' was a [[France|French-built]] light utility aircraft of the mid-1950s.
{|{{Infobox Aircraft Begin
 | name=Aerofer
 | image=Aerofer edited-2.jpg
 | caption=The sole Aerofer at Guyancourt airfield near Paris in June 1963
}}{{Infobox Aircraft Type
 | type=light utility aircraft
 | national origin=France
 | manufacturer=Aero-Club des Cheminots
 | designer=Aero-Club des Cheminots
 | first flight=1954
 | introduced=1954
 | retired=
 | status=no longer registered
 | primary user=aero club
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

==Design and development==

The '''Aerofer''' was designed and built by members of the ''Aero-Club des Cheminots'' as a co-operative project. It was a small single-seat low-winged aircraft. The Aerofer had a two-spar wooden wing with fabric covering. The [[fuselage]] was of wooden construction with a combination of [[Aircraft fabric covering|fabric]] and [[plywood]] covering.  The [[conventional landing gear|tailwheel undercarriage]] was fixed. The aircraft was powered by a 50 h.p. [[Walter Mikron]] four-cylinder air-cooled engine built by Aster.<ref>Green, 1965, p.34</ref>

==Operational history==

The Aerofer was completed in 1954 and was operated until 1964 by the members of the Aero-Club des Cheminots, based at [[Guyancourt]] airfield (now closed) to the west of Paris.<ref>Butler, 1964, p.115</ref> Its extremely small dimensions meant that it was semi-aerobatic, despite the low-powered engine fitted. Only one example of the design was completed.<ref>Green, 1965, p.34</ref>

By 1965 the aircraft, registered as ''F-PERS'' with a ''Certificat de Navigabilite Restreint d'Aeronef'' (CNRA)<ref>Burnett, 1983, p.111</ref> was owned by the Aero-Club Etienne Boileau and based at [[Fontenay-Tresigny]] airfield.<ref>Green, 1965, p.34</ref> It was no longer on the CNRA register by March 1983.<ref>Burnett, 1983, p.112</ref>

==Specifications==
{{Aircraft specs
|ref=Green
|prime units?=imp <!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=1
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=26
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=1112
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Walter Mikron I]] 
|eng1 type=4-cyl. inverted air-cooled piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=50 <!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=90
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=78
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*{{cite book|last=Butler|first=P.H.|title=French Civil Aircraft Register 1964|year=1964|publisher=Merseyside Society of Aviation Enthusiasts|isbn=}}
*{{cite book|last=Burnett|first=I.P.|title=Civil Aircraft Registers of France 1983|year=1983|publisher=Air-Britain (Historians) Ltd|isbn=0-85130-104-5}}
*{{cite book|last=Green|first=William|title=The Aircraft of the World|year=1965|publisher=Macdonald & Co (Publishers) Ltd|isbn=}}
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:French civil utility aircraft 1950–1959]]