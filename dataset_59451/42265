{|{{Infobox aircraft begin
 |name= AVE Mizar
 |image= File:AVE-Mizar-1973-N68X-XL.jpg
 |caption= The aircraft at [[Oxnard Airport]], Oxnard, California - August 1973
}}{{Infobox aircraft type
 |type= [[Roadable aircraft]]
 |national origin= [[United States]]
 |manufacturer= Advanced Vehicle Engineers
 |designer= Henry Smolinski
 |first flight=
 |introduced= 1973
 |retired=
 |status= Destroyed in crash
 |primary user=
 |more users= <!--Limited to three in total; separate using <br /> -->
 |produced= <!--years in production-->
 |number built= 2
 |program cost= <!--Total program cost-->
 |unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 |developed from= [[Ford Pinto]], [[Cessna 337]]
 |variants with their own articles=
}}
|}

The '''AVE Mizar''' (named after the star [[Mizar (star)|Mizar]]) was a [[roadable aircraft]] built between [[1971 in aviation|1971]] and [[1973 in aviation|1973]] by Advanced Vehicle Engineers (AVE) of [[Van Nuys, Los Angeles|Van Nuys]], [[Los Angeles]], [[California]]. The company was started by Henry Smolinski, a graduate of [[Northrop University|Northrop Institute of Technology]]'s aeronautical engineering school.<ref>{{cite web |author= Brian Campbell |url= http://www.cookieboystoys.com/mizar.htm |title= The History Of... AVE Mizar Flying Pinto |publisher= Cookieboystoys.com |date= 1973-09-11 |accessdate= 2013-06-07}}</ref>

==Development==
The [[prototype]]s of the Mizar were made by mating the rear portion of a [[Cessna Skymaster]] to a [[Ford Pinto]].<ref>{{cite journal |magazine=[[Popular Mechanics]]|title=Flying Pinto Sprouts Wings|url= https://books.google.com/books?id=LdQDAAAAMBAJ&lpg=PA171&dq=Popular%20Mechanics.%20September%201973&pg=PA113#v=onepage&q=mizar&f=false |date=September 1973}}</ref> The pod-and-[[twin-boom aircraft|twin-boom]] configuration of the Skymaster was a convenient starting point for a hybrid automobile/airplane. The passenger space and front engine of the Skymaster were removed, leaving an airframe ready to attach to a small car. AVE planned to have their own airframe purpose-built by a subcontractor for production models, rather than depending on [[Cessna]] for airframes.<ref name=special>[https://books.google.com/books?id=VcsDJgKwpDQC&pg=PA185 ''Special Use Vehicles: An Illustrated History of Unconventional Cars and Trucks Worldwide'']. George W. Green. McFarland. ISBN 978-0-7864-2911-0 (2007)</ref>

According to ''Peterson's Complete Ford Book'', by mid-1973, two prototypes had been built and three more were under construction. One prototype was slated for static display at Galpin Ford, owned by AVE partner Bert Boeckmann of [[Sepulveda, California]]. The other prototype, fitted with a [[Teledyne Continental Motors]] {{convert|210|hp}} engine, was unveiled to the press on May 8, 1973. It then began a series of taxi tests at [[Van Nuys, California]]. AVE made special arrangements to do flight testing at the [[U.S. Navy]]'s test facilities at [[Point Mugu]], California. AVE stated that [[Federal Aviation Administration|FAA]] certification flights were underway in mid-1973.<ref>Peterson, ''Peterson's Complete Ford Book'' 3rd Edition (1973)</ref><ref name=intro>{{cite news
 |authorlink=
 |author=
 |coauthors=
 |title= Car That Can Fly Away Developed in Van Nuys
 |url= http://www.cookieboystoys.com/mizar/05%20mizar.htm
 |work= [[The Van Nuys News]]
 |pages=
 |page=
 |date= May 15, 1973
 |accessdate= 2012-11-20
}}</ref>

The Mizar was intended to use both the [[aircraft engine]] and the car [[engine]] for takeoff. This would considerably shorten the takeoff roll. Once in the air, the car engine would be turned off. Upon landing, the four-wheel braking would stop the craft in {{convert|525|ft}} or less. On the ground, telescoping [[strut|wing supports]] would be extended and the airframe would be tied down like any other aircraft. The Pinto could be quickly unbolted from the airframe and driven away.<ref name=intro/>

Production was scheduled to begin in 1974. AVE had stated that prices would range from [[US dollar|US$]]18,300 to US$29,000.<ref name=intro/>

On a test flight from [[Camarillo Airport]] in California on August 26, 1973, according to test pilot Charles "Red" Janisse, the right wing strut base mounting attachment failed soon after takeoff. Because turning the aircraft would put too much stress on the unsupported wing, Janisse put the aircraft down in a bean field. After the roadway was closed to traffic, Janisse drove the otherwise undamaged aircraft back to the airport.

On September 11, 1973, during a test flight at Camarillo, the right wing strut again detached from the Pinto. With Janisse not available for this test flight, Mizar creator Smolinski was at the controls. Although some reports say the Pinto separated from the airframe, an air traffic controller, watching through binoculars, said the right wing folded. According to Janisse, the wing folded because the pilot tried to turn the aircraft when the wing strut support failed. Smolinski and the Vice President of AVE, Harold Blake, were killed in the resulting fiery crash.

Even though the Pinto was a light car, the total aircraft without passengers or fuel was already slightly over the certified gross weight of a Skymaster. However, in addition to poor [[aircraft design process|design]] and loose parts, the [[National Transportation Safety Board]] reported that bad [[welding|welds]] were partly responsible for the crash, with the right wing strut attachment failing at a body panel of the Pinto.<ref>[http://www.ntsb.gov/aviationquery/brief.aspx?ev_id=84720&key=0 Accident report - NTSB Identification: LAX74FUQ18], [[National Transportation Safety Board]]</ref><ref>{{cite journal
 |author= Peter Garrison
 |authorlink= Peter Garrison
 |date=August 1993
 |title= Can We Ever Make A Car Airworthy?
 |work= [[Flying Magazine]]
 |publisher=
 |accessdate= 2012-11-20
 |url= https://books.google.com/books?id=qnVtljLAyr4C&pg=PA100
}}</ref>

==In popular culture==
In the 1974 Bond movie [[The Man with the Golden Gun (film)|''The Man with the Golden Gun'']], [[Francisco Scaramanga|Scaramanga]] and his henchman, [[list of henchmen of James Bond villains#The Man with the Golden Gun|Nick Nack]], were originally going to escape Bond in an AVE Mizar; however, due to the crash that occurred on 11 September 1973, resulting in the death of its designer, Henry Smolinski, a [[model airplane]] was used instead.{{cn|date=June 2016}}

==Specifications==
{{aircraft specifications
 |plane or copter?= <!-- options: plane/copter --> plane
 |jet or prop?= <!-- options: jet/prop/both/neither --> prop
 |ref= ''Special Use Vehicles: An Illustrated History of Unconventional Cars and Trucks Worldwide'' and ''Peterson's Complete Ford Book''
 |crew= one, pilot
 |capacity= three passengers
 |length main= 28 ft
 |length alt= 8.5 m
 |span main= 38 ft 0 in
 |span alt= 11.58 m
 |height main= 8½ ft
 |height alt= <!--m-->
 |area main= 201 ft²
 |area alt= 18.7 m²
 |airfoil=
 |empty weight main= <!--lb-->
 |empty weight alt= <!--kg-->
 |loaded weight main= <!--lb-->
 |loaded weight alt= <!--kg-->
 |useful load main= <!--lb-->
 |useful load alt= <!--kg-->
 |max takeoff weight main= <!--lb-->
 |max takeoff weight alt= <!--kg-->
 |more general=
 |engine (prop)= [[Continental IO-360]]-C
 |type of prop=
 |number of props= 1
 |power main= 210 hp
 |power alt= 157 kW
 |power original=
 |max speed main= <!--knots-->
 |max speed alt= <!--mph,km/h-->
 |cruise speed main= <!--knots-->
 |cruise speed alt= 130
 |never exceed speed main= <!--knots-->
 |never exceed speed alt= <!--mph,km/h-->
 |stall speed main= <!--knots-->
 |stall speed alt= <!--mph,km/h-->
 |range main= <!--nm-->
 |range alt= <!--mi,km-->
 |ceiling main= 12,000 ft
 |ceiling alt= <!--m-->
 |climb rate main= <!--ft/min-->
 |climb rate alt= <!--m/s-->
 |loading main= <!--lb/ft²-->
 |loading alt= <!--kg/m²-->
 |thrust/weight= <!--a unitless ratio-->
 |power/mass main= <!--hp/lb-->
 |power/mass alt= <!--W/kg-->
 |more performance=
 |armament=
 |avionics=
}}

==See also==
{{aircontent|
|related=
*[[Cessna Skymaster]]
*[[Ford Pinto]]
|similar aircraft=
*[[Aerocar]]
*[[Aerauto PL.5C]]
|sequence=
|lists=
|see also=
}}

;Other
*[[List of inventors killed by their own inventions]]

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20040812032226/http://world.std.com:80/%7Ejlr/doom/blake.htm A Pinto for Icarus]
*[http://highroad.smugmug.com/keyword/ave/1/583531558_DUJPo#583531558_DUJPo Photos of the flight test (high quality)]
*[https://www.youtube.com/watch?v=rzv4q5EEy1k Promotional Video]
*[http://www.cookieboystoys.com/mizar.htm The unflyable Pinto used for the promotional video today]

{{Flying cars}}

{{DEFAULTSORT:Ave Mizar}}
[[Category:AVE aircraft|Mizar]]
[[Category:1973 introductions]]
[[Category:Abandoned civil aircraft projects of the United States]]
[[Category:Aircraft manufactured in the United States]]
[[Category:Aviation accidents and incidents by aircraft]]
[[Category:High-wing aircraft]]
[[Category:Roadable aircraft]]
[[Category:Single-engined pusher aircraft]]
[[Category:United States civil utility aircraft 1970–1979]]