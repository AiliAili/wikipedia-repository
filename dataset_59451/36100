{{Good article}} 
{{Use mdy dates|date=December 2011}}
{{Infobox album 
| Name = Alive or Just Breathing
| Type = [[Album]]
| Artist = [[Killswitch Engage]]
| Cover = killswitch engage alive or just breathing.png
| Released = May 21, 2002
| Recorded = October 2001 – February 2002
| Studio = Zing Studios, [[Westfield, Massachusetts]]
| Genre = [[Metalcore]],<ref name="metalreview1"/> [[Heavy metal music|heavy metal]]<ref name="allmusic"/>
| Length = 44:57
| Label = [[Roadrunner Records|Roadrunner]]
| Producer = [[Adam Dutkiewicz]]
| Last album = ''[[Killswitch Engage (2000 album)|Killswitch Engage]]''<br />(2000)
| This album = '''''Alive or Just Breathing''''' <br /> (2002)
| Next album = ''[[The End of Heartache]]''<br />(2004) |
| Misc = {{Singles
  | Name = Alive or Just Breathing
  | Type = studio
  | single 1 = Self Revolution
  | single 1 date = 2002<ref>{{cite AV media notes | others=Killswitch Engage | date=2002 | title=Self Revolution | type=track listing | publisher=[[Roadrunner Records]] | id=RR PROMO 636}}</ref>
  | single 2 = My Last Serenade
  | single 2 date = 2002<ref>{{cite AV media notes | others=Killswitch Engage | date=2002 | title=My Last Serenade | type=track listing | publisher=[[Roadrunner Records]] | id=RR PROMO 637}}</ref>
  | single 3 = The Element of One
  | single 3 date = 2003<ref>{{cite AV media notes | others=Killswitch Engage | date=2003 | title=The Element of One | type=track listing | publisher=[[Roadrunner Records]] | id=RR 8457-2}}</ref>
  }}
}}

'''''Alive or Just Breathing''''' is the second studio album by American [[metalcore]] band [[Killswitch Engage]]. It was released on May 21, 2002, through [[Roadrunner Records]]. ''Alive or Just Breathing'' was Killswitch Engage's first album on Roadrunner and was recorded from October 2001 to February 2002. Produced by drummer and guitarist [[Adam Dutkiewicz]], this was the band's first album on a major label, which prompted them to write and record the album to the best of their abilities.<ref name="lambgoat">{{cite web | url=http://lambgoat.com/features/interviews/14/Killswitch-Engage-interview  | title=Killswitch Engage interview  | author=Lambgoat, John  |date=June 2, 2002  |publisher=Lambgoat.com  |accessdate=May 2, 2012}}</ref> ''Alive or Just Breathing'' has been viewed as a landmark album in the metalcore genre and was well praised upon its release by fans and critics.<ref name="metalreview1"/> The lyrics, which were all written by vocalist [[Jesse Leach]], were aimed to bring a positive message through the music.<ref name="lambgoat"/>

Shortly after the release of ''Alive or Just Breathing'', Leach left the band for personal and health issues.<ref name="departure">{{cite web |url=http://legacy.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=4557 |title=Killswitch Engage land new frontman |date=July 4, 2002 |publisher=[[Blabbermouth.net]] |accessdate=May 5, 2012}}</ref> Killswitch Engage quickly recruited [[Howard Jones (American musician)|Howard Jones]] to replace Leach.<ref name="departure"/> Jones would stay in the band for nearly ten years, until his departure in early 2012, which prompted Leach to rejoin the band on the ten-year anniversary of ''Alive or Just Breathing''.<ref name="rejoin">{{cite web|url=http://legacy.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=169328|title=It's Official: Jesse Leach Rejoins Killswitch Engage|publisher=[[Blabbermouth.net]]|date=February 6, 2012|accessdate=May 14, 2012}}</ref> This is also the last album to feature Dutkiewicz on drums, as he would switch to guitar before the touring process.<ref name="AMbio">{{cite web|url=http://www.allmusic.com/artist/killswitch-engage-p436947/biography|title=Killswitch Engage|publisher=[[AllMusic]]|first=Eleanor|last=Ditzel|accessdate=May 15, 2012}}</ref> The drumming position was filled by [[Tom Gomes]].<ref name="AMbio"/>

==Background==
In June 2000, Killswitch Engage released their debut album, titled ''[[Killswitch Engage (2000 album)|Killswitch Engage]]'' through [[Ferret Records]]. The band got its recording deal as a favor for the artwork bassist [[Mike D'Antonio]] created for Ferret. The label's CEO, Carl Severson signed the band without a contract simply to get the album out to the public because he thought the album "ruled", and stated "I had a one-off deal and when I knew they wanted to go someplace bigger, I figured it was in my best interest, as their friend, to make the best thing happen for them."<ref name="Linernotes">{{cite AV media notes | others=Killswitch Engage | title=Alive or Just Breathing Roadrunner Records 25th Anniversary Edition| year=2005 | type=booklet |publisher=[[Roadrunner Records]] | id=RR 8457-2}}</ref> After the album was released, Severson showed it to [[Roadrunner Records]] A&R man, Mike Gitter.<ref name="Linernotes"/>

Gitter played the album for other Roadrunner employees and stated "The feeling was this was something that touched upon classic metal, pulls it up through hardcore, and creates something fairly new and completely exciting. When their music started circulating around the office, there was an immediate 'Holy&nbsp;... this is great!' feeling about it. People weren't halfway about it."<ref name="Linernotes"/> Gitter offered to sign the band to the label and felt that signing Killswitch Engage was the label's last chance to have any heavy metal bands on the roster. The band was apprehensive at first, judging by the artist roster of Roadrunner at the time, but after nearly six months of deliberation, Killswitch Engage signed to Roadrunner due to the opportunities it provided for them. After signing, the band began work on their major label debut, which would become ''Alive or Just Breathing''.<ref name="Linernotes"/> Gitter has claimed that signing Killswitch Engage was the most gratifying signing of his career.<ref>{{cite web | url=http://thesilvertongueonline.com/2009/03/band-essentials-tst-talks-ar-with-roadrunner-records-mike-gitter/  | title=Band Essentials: TST talks A&R with Roadrunner Record's Mike Gitter | author=Lucas, SiN | date=March 13, 2009 | publisher=The Silver Tongue | accessdate=May 1, 2012}}</ref>

==Writing and recording==

===Writing===
In summer of 2001, Killswitch Engage recorded a demo produced by drummer/guitarist [[Adam Dutkiewicz]] which featured the songs "Transfiguration" (which would later become "Fixation on the Darkness"), "Just Barely Breathing", and "Numbered Days". After giving the demo to Roadrunner, the label agreed to let Dutkiewicz produce ''Alive or Just Breathing'' because they were impressed with his production skills.<ref name="Linernotes"/>

As Killswitch Engage started writing for ''Alive or Just Breathing'', then-guitarist [[Pete Cortese]], who co-wrote "Fixation on the Darkness", left the band to spend more with his family. The music for ''Alive or Just Breathing'' was all written by Dutkiewicz, D'Antonio, and guitarist [[Joel Stroetzel]], while all of the lyrics were written by singer [[Jesse Leach]].<ref name="lambgoat"/>  After completing the composition for the album, the band knew they would need to find a second guitarist to play with Stroetzel, because ''Alive or Just Breathing'' was written for two guitarists during live performances.<ref name="lambgoat"/>

Taking into account that this would be the band's first album on a major label, Killswitch Engage was adamant on making the best album that they could. This required Dutkiewicz to spend more time in the studio producing and writing songs as a whole, rather than just a collection of guitar riffs put together.<ref name="lambgoat"/> D'Antonio stated the writing process for the record went very fast as he wrote the "[[Moshing|moshy]], [[hardcore punk|hardcore]]" style [[Ostinato#Riff|riffs]], Stroetzel wrote the [[thrash metal]] styled riffs, and Dutkiewicz wrote the "catchy" choruses.<ref name="Linernotes"/> Leach said that his main goal when writing lyrics was to be used as an instrument to shine the light of God through his music.<ref name="Linernotes"/> He felt that music lyrics in general featured too much negativity and wanted to have positivity shown in his.<ref name ="Worldablaze">{{cite video | people= Korycki, Denise (director); Starace, Lia (editor) | date=November 22, 2005 | title=(Set This) World Ablaze | medium=DVD | time=<!-- Documentary section, respectively -->  |id={{UPC|016861093990}} | accessdate=May 02, 2012}}</ref>

===Recording===
On November 3, 2001, Killswitch Engage entered Zing Studios in [[Westfield, Massachusetts]] to begin recording ''Alive or Just Breathing''.<ref>{{cite web | url=http://www.roadrunnerrecords.com/news/killswitch-engage-studio--2 | title=Killswitch Engage Studio | publisher=Roadrunner Records | date=November 5, 2001 | accessdate=May 2, 2012}}</ref> Upon entering the studio, the album already had its title, derived from a lyric in the song "Just Barely Breathing." The band members all had jobs and some were going to school during the recording of the album, so each member came in for short periods as they had time available. Stroetzel said this was helpful, as it reduced stress, but regretted that they were not able to get more done in one session.<ref name="Linernotes"/> A problem that D'Antonio, Dutkiewicz, and Stroetzel encountered was keeping their guitars in tune during the sessions. They spent a large amount of the recording budget buying new guitars to improve the [[guitar tunings|tuning]]. Since the recording of ''Alive or Just Breathing'', they have learned how to keep in tune better as they record.<ref name="Linernotes"/>

{{Listen |filename=My Last Serenade.ogg |title="My Last Serenade" |description=Sample of "My Last Serenade" from ''Alive or Just Breathing'' (2002). Jesse Leach had troubles recording his vocals for ''Alive or Just Breathing'', but was pleased with the end result. Much of the album contains a balance of screaming and [[Singing|clean vocals]].}}

Jesse Leach had troubles gaining confidence as he was recording his vocals, but was determined to make them a "masterpiece".<ref name="Linernotes"/> Leach spoke of Dutkiewicz and recording vocals stating, "I definitely hand it to Adam D. – he's the man. We're taking our time, dissecting things. It's made me push myself to my limits – getting a lot more passion and deliver out of what I'm doing."<ref>{{cite web |url=http://www.roadrunnerrecords.com/news/in-the-studio-with-killswitch |title=In the Studio with Killswitch Engage |publisher=Roadrunner Records | date=December 4, 2001 |accessdate=May 3, 2012}}</ref> After struggling with nervousness for a while, Dutkiewicz and Leach went to Leach's home in [[Rhode Island]] to finish recording vocals. Mike Gitter was reluctant to hear of this when Killswitch Engage turned the record into Roadrunner, but was ultimately pleased with the results.<ref name="Linernotes"/> Before the completion of the record, the band brought in [[Tom Gomes]] to play drums as Dutkiewicz moved to the empty guitar position.<ref name="lambgoat"/> Though Dutkiewicz recorded most of the drums himself, Gomes provided additional percussion on the album, before he was an official member of the band. Backing vocals on ''Alive or Just Breathing'' were done by Leach, Dutkiewicz, as well as Dutkiewicz's sister Becka,<ref>{{cite web| url=http://www.allmusic.com/artist/becka-dutkiewicz-mn0001572216 |title=Becka Dutkiewicz – Credits |publisher=[[AllMusic]] |accessdate=June 16, 2012}}</ref> and [[All That Remains (band)|All That Remains]] singer [[Philip Labonte]].<ref name="Linernotes"/>

During the ''Alive or Just Breathing'' recording sessions, Killswitch Engage recorded 15 songs. Three of the songs were re-recordings of songs from the band's debut album. Stroetzel stated that the band decided to re-record "Temple From the Within", "Vide Infra", and "In the Unblind" because at the time, Killswitch Engage was unsure of how many people would ever hear ''Killswitch Engage'' because it was released through a small label.<ref name="Linernotes"/> "Temple From the Within" and "Vide Infra" made it onto ''Alive or Just Breathing''. "In the Unblind" and two newly written songs titled "When the Balance is Broken" and "Untitled and Unloved" respectively, did not make it on to the album, but would eventually be heard on the 2005 reissue of ''Alive or Just Breathing''.<ref name="411mania"/>

''Alive or Just Breathing'' was mixed in February 2002 at Backstage Productions in [[Ripley, Derbyshire|Ripley, Derbyshire, UK]] by [[Andy Sneap]]. Dutkiewicz brought the album to the UK and oversaw the mixing process. The band chose Sneap after hearing good things about his work. The band and label were both very pleased with the mix, and Dutkiewicz in particular noted his satisfaction with the drum tones.<ref name="lambgoat"/>

===Artwork and packaging===
Mike D'Antonio owns a graphic design company called Darkicon Designs and has made album covers and various other pieces of art (T-shirts, posters, logos, etc.) for bands such as [[Shadows Fall]], [[All That Remains (band)|All That Remains]], [[Crowbar (American band)|Crowbar]] among others.<ref name="Worldablaze"/> Killswitch Engage made sure to have written in their contract with Roadrunner that D'Antonio would produce all of the band's artwork.<ref name="Portland">{{cite web |url=http://www.portlandmusicians.com/crave/2004/06/kei_interview.shtml |title=Interview with Mike D'Antonio |first=Robin |last=Steeley |date=May 12, 2004 |publisher=Crave Magazine |accessdate=May 5, 2012}}</ref> D'Antonio stated about the cover of ''Alive or Just Breathing'', "I wanted something that looked 'old school cut-and-paste' for the cover. Something with texture that appeared like it was just laid down and photographed. I remember Roadrunner hating the cover, saying there was too much going on and nothing to focus on. I told them I did not agree, and that some day they would get it."<ref name="Linernotes"/><ref name="Portland"/>

==Release==
''Alive or Just Breathing'' was released on May 21, 2002. Killswitch Engage filmed a music video for the song "My Last Serenade", which gained heavy rotation on [[MTV]]'s [[Headbanger's Ball]] and propelled the album to number 37 on ''[[Billboard (magazine)|Billboard]]''{{'}}s [[Top Heatseekers]] chart.<ref name="Worldablaze"/> ''Alive or Just Breathing'' also stayed at number one on [[CMJ]]'s Loud Rock radio chart for over a month.<ref>{{cite journal |last1=Boyce |first1=Kevin |last2=Sciarretto |first2=Amy |year=2003 |title=Far Beyond Driven |journal=[[CMJ]] |issue=820 |pages=9 |publisher=CMJ Network Inc |doi_brokendate=June 30, 2003 |url=https://books.google.com/books?id=lw0U_9_pUfMC&pg=PA9&dq=%22Alive+or+just+breathing%22+Killswitch&hl=en&sa=X&ei=SPfcT-KXNYaQ8wS41OCECw&sqi=2&ved=0CEAQ6AEwAQ#v=onepage&q=%22Alive%20or%20just%20breathing%22%20Killswitch&f=false |accessdate= June 16, 2012}}</ref> By the time the band's third album, ''[[The End of Heartache]]'' was released, ''Alive or Just Breathing'' sold 114,000 copies in the United States.<ref>{{cite journal |last1=Christman |first1=Ed |year=2004 |title=Viral Marketing |journal=[[Billboard (magazine)|Billboard]] |pages=42 |publisher=[[Prometheus Global Media]] |doi_brokendate=July 3, 2004 |url=https://books.google.com/books?id=OhAEAAAAMBAJ&pg=PA41&dq=%22Alive+or+just+breathing%22 |accessdate=June 16, 2012}}</ref> Music videos were also filmed for "Life to Lifeless" and "Fixation on the Darkness". Upon the release of the album, Killswitch Engage embarked on their first nationwide tour, supporting [[Soilwork]] and [[Hypocrisy]] and would tour for the next year in support of ''Alive or Just Breathing''.<ref name="Worldablaze"/> Other legs of the tour featured the band touring with acts such as [[Kittie]], [[Poison the Well (band)|Poison the Well]], [[Shadows Fall]], and [[Hotwire (band)|Hotwire]].<ref>{{cite journal |last1=Boyce |first1=Kevin |last2= |first2= |year=2002 |title=Loud Rock |journal=[[CMJ]] |issue=770 |pages=13 |publisher=CMJ Network Inc. |doi_brokendate=July 8, 2002 |url=https://books.google.com/books?id=aBw3t2fY64UC&pg=PA10-IA3#v=onepage&q&f=false |accessdate=June 16, 2012 }}</ref><ref>{{cite web| url=http://legacy.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=3863 |title=Shadows Fall to Tour With Kittie, Poison the Well, Others in Late Summer |publisher=[[Blabbermouth.net]] |date=June 2, 2002 |accessdate=June 16, 2012}}</ref>

Missing his wife whom he had married two weeks prior to the tour, Jesse Leach left Killswitch Engage in the middle of their first national tour. With a combination of depression, missing his wife, and throat problems, Leach sent D'Antonio an email just a few days before the band was scheduled to play a show, announcing his departure. D'Antonio stated in an interview that  "after three years of hanging out with the dude, and considering him a brother, to just get an email was a little bit harsh."<ref name="departure"/> Killswitch Engage very quickly set up auditions to fill the vocalist position in the band and eventually settled on [[Howard Jones (American musician)|Howard Jones]], who was at the time the frontman of [[Blood Has Been Shed]]. Jones very quickly had to learn a set of songs to tour with the band.<ref name="Worldablaze"/> Leach and the rest of the band quickly reconciled and remained close friends.<ref name="Worldablaze"/><ref>{{cite web |url=http://www.metalsucks.net/2010/03/31/exclusive-interview-with-former-killswitch-engagecurrent-the-empire-shall-fall-vocalist-jesse-leach/ |title=Exclusive interview with former Killswitch Engage/current The Empire Shall Fall vocalist Jesse Leach |date=March 31, 2010 |publisher=[[MetalSucks]]|accessdate=May 5, 2012}}</ref>

After the touring process for the album was over, Tom Gomes left the band and was replaced by [[Justin Foley]].<ref name="AMbio"/> This line-up of Killswitch Engage would last until early 2012, when Jones left the band.<ref name="rejoin"/> Following Jones' departure, Jesse Leach rejoined the band on the ten-year anniversary of ''Alive or Just Breathing''.<ref name="rejoin"/>

In 2005, as part of Roadrunner's 25th anniversary, the label re-released ''Alive or Just Breathing'' as a special edition. The re-release included a bonus [[enhanced cd]] with all of the songs left over from the album's recording sessions, as well as a re-recorded version of "Fixation on the Darkness", featuring Jones on lead vocals, a track of studio outtakes and music videos for "My Last Serenade", "Fixation on the Darkness", and "Life to Lifeless". The special edition also included expanded artwork and a biography documenting the recording and release of the album.<ref name="Linernotes"/>

==Critical reception==
{{Album ratings
| rev1 = 411mania.com
| rev1score = 7.0/10<ref name="411mania">{{cite web|url=http://www.411mania.com/music/album_reviews/69/Killswitch-Engage--Alive-Or-Just-Breathing-Review.htm |title=Music – Killswitch Engage -Alive Or Just Breathing Review |publisher=411mania.com |date=October 19, 2002 |author=Evocator Manes |accessdate=December 22, 2011}}</ref>
| rev2 = [[AllMusic]]
| rev2Score = {{Rating|4|5}}<ref name="allmusic">{{cite web | url=http://www.allmusic.com/album/r588062 | author=Taylor, Jason D.  | title=Alive or Just Breathing | publisher=[[AllMusic]] | accessdate=May 2, 2012}}</ref>
| rev3 = [[Blabbermouth.net|Blabbermouth]]
| rev3score = 10/10<ref name="Blabbermouth">{{cite web | url=http://legacy.roadrunnerrecords.com/blabbermouth.net/showreview.aspx?reviewID=501 | title=Alive or Just Breathing (2-CD Special Edition) (Roadrunner) | author=Alisoglu, Scott  | publisher=[[Blabbermouth.net]] | accessdate=May 2, 2012}}</ref>
| rev4 = [[CMJ]]
|rev4score = positive<ref name=CMJreview/>
|rev5 =''[[Kerrang!]]''
|rev5Score = {{Rating|5|5}}<ref name="Kerrang">{{cite web | url=http://legacy.roadrunnerrecords.com/news/THE-TERMINATORS-1644.aspx | title=The Terminators | author=Lawson, Dom  |date=May 15, 2002 | publisher=Roadrunner Records |accessdate=May 3, 2012}}</ref>
|rev6 =  Lambgoat
|rev6score =  8/10<ref name="lambgoatreview">{{cite web|url=http://lambgoat.com/albums/366/Killswitch-Engage-Alive-Or-Just-Breathing|title=Killswitch Engage – Alive Or Just Breathing? Review|publisher=lambgoat.com|first=Geoff|last=Harman|date=September 30, 2002|accessdate=May 30, 2012}}</ref>
|rev7=Metal Review
|rev7score=9.2/10<ref name="metalreview1">{{cite web|last=Thomas |first=Erik |url=http://www.metalreview.com/reviews/2247/killswitch-engage-alive-or-just-breathing---reissued-(2-discs) |title=Review of Killswitch Engage – Alive Or Just Breathing – Reissued (2 Discs) |publisher=Metal Review |date=December 7, 2005 |accessdate=December 22, 2011}}</ref>
}}
Critical reception of ''Alive or Just Breathing'' was generally positive. Erik Thomas from Metalreview.com said "while this album certainly is responsible for the vast amount of saturation, it stands as a pretty trendsetting, stellar must-own example of [[metalcore]]."<ref name="metalreview1"/> Jason D. Taylor of [[AllMusic]] stated "This is a pure metal album that seemingly has ignored any fashionable trend and instead relies solely on skill and expertise to sculpt some of meatiest heavy metal since the glory days of Metallica and Slayer."<ref name="allmusic"/> Taylor also expressed the impressiveness of the band's "do-it-yourself" work ethic as Dutkiewicz handled all production and D'Antonio "took the time to give much thought on the album's packaging and art direction."<ref name="allmusic"/> Dom Lawson of ''[[Kerrang!]]'' magazine gave the album five "Ks" (or five out of five stars). He praised the album's unique new sound, music, and vocal performances from Leach.<ref name="Kerrang"/> Kevin Boyce of ''[[CMJ]]'' said, "This album is more addictive than crack cocaine that's been smothered in caffeine and nicotine and drenched with chocolate. Starting with the track 'Numbered Days,' this 12-track platter never takes its foot off of the throttle."<ref name="CMJreview">{{cite journal |last1=Boyce |first1=Kevin|last2= |first2= |year=2002 |title=Loud Rock – Killswitch Engage – Alive or Just Breathing |journal=[[CMJ]] |issue=761 |pages=16 |publisher=CMJ Network Inc. |doi_brokendate=May 6, 2002 |url=https://books.google.com/books?id=iKqgNrVzuOcC&pg=PA11-IA5#v=onepage&q&f=false |accessdate=June 16, 2012 }}</ref> Boyce also stated his favorite song from the album was "My Last Serenade," and applauded "Just Barely Breathing," comparing parts of it to [[Metallica]]'s ''[[Ride the Lightning]]''.<ref name="CMJreview"/>

Reviewing the 2005 re-release of the album, Scott Alisoglu of [[Blabbermouth.net]] said that ''Alive or Just Breathing'' is "one special album" and that its release "marked a defining moment in metal."<ref name="Blabbermouth"/> Alisoglu also viewed the album as one of the greatest albums of 2002 and of "the new century."<ref name="Blabbermouth"/> ''Alive or Just Breathing'' is one of only 21 albums to have a perfect site rating on Blabbermouth.net.<ref>{{cite web| url=http://legacy.roadrunnerrecords.com/blabbermouth.net/reviews.aspx?PageNum=1&mode=Search&order_by=Rating&searchtext2=&asc_desc=desc |title=CD Reviews (page 1) |publisher=[[Blabbermouth.net]] |accessdate=May 15, 2012}}</ref><ref>{{cite web| url=http://legacy.roadrunnerrecords.com/blabbermouth.net/reviews.aspx?PageNum=2&mode=Search&order_by=Rating&searchtext2=&asc_desc=desc |title=CD Reviews (page 2) |publisher=[[Blabbermouth.net]] |accessdate=May 15, 2012}}</ref> In 2009, [[MetalSucks]] compiled a list of the "21 Best Metal Albums of the 21st Century So Far" based on the opinions of various musicians, managers, publicists, label representatives and writers, where ''Alive or Just Breathing'' was placed at number four on the list.<ref name="21best">{{cite web|url=http://www.metalsucks.net/2009/07/01/4-killswitch-engage-alive-or-just-breathing/|title=#4: Killswitch Engage – Alive Or Just Breathing|publisher=[[MetalSucks]]|first=Axl|last=Rosenberg|date=July 1, 2009|accessdate=May 18, 2012}}</ref> [[Noisecreep]] posted a list of the top 10 best metal albums of the past decade in 2010, where the album was listed at number six.<ref>{{cite web| url=http://www.noisecreep.com/2009/12/02/best-albums-2000s/ |title=Best Albums of the 2000s |publisher=[[AOL]] |author=Ramirez, Carlos |accessdate=June 16, 2012}}</ref>

In a review for 411mania.com, Evocator Manes stated his dislike for the melodic "smooth parts" that he felt were too repetitious throughout ''Alive or Just Breathing'', however he praised the album's production and positive lyrics.<ref name="411mania"/> Jesse Leach stated there was a small portion of Killswitch Engage fans who disliked his use of clean singing on the album.<ref name="Linernotes"/>

==Track listing==
{{tracklist
| all_lyrics      = Jesse Leach
| writing_credits = yes
| total_length = 44:57
| title1 = Numbered Days
| writer1 = Killswitch Engage
| length1 = 3:35
| title2 = Self Revolution
| writer2 = Killswitch Engage
| length2 = 3:08
| title3 = Fixation on the Darkness
| writer3 = Killswitch Engage, [[Pete Cortese]]
| length3 = 3:37
| title4 = My Last Serenade
| writer4 = Killswitch Engage
| length4 = 4:13
| title5 = Life to Lifeless
| writer5 = Killswitch Engage
| length5 = 3:17
| title6 = Just Barely Breathing
| writer6 = Killswitch Engage
| length6 = 5:42
| title7 = To the Sons of Man
| writer7 = Killswitch Engage
| length7 = 1:58
| title8 = Temple from the Within
| writer8 = Killswitch Engage
| length8 = 4:04
| title9 = The Element of One
| writer9 = Killswitch Engage
| length9 = 4:08
| title10 = Vide Infra
| writer10 = Killswitch Engage
| length10 = 3:28
| title11 = Without a Name
| writer11 = Killswitch Engage
| length11 = 1:45
| note11 = Instrumental
| title12 = Rise Inside
| writer12 = Killswitch Engage
| length12 = 5:56
}}
{{Track listing
| collapsed = yes
| headline = 2005 Re-release Bonus Disc
| title1 = In the Unblind
| length1 = 2:48
| title2 = When the Balance is Broken
| length2 = 4:35
| title3 = Untitled and Unloved
| length3 = 3:20
| note3 = Instrumental
| title4 = Numbered Days
| length4 = 3:37
| note4 = Demo
| title5 = Transfiguration
| length5 = 3:38
| note5 = Demo – early version of "Fixation on the Darkness"
| title6 = Just Barely Breathing
| length6 = 5:13
| note6 = Demo
| title7 = Fixation on the Darkness
| length7 = 3:37
| note7 = Re-recorded version with [[Howard Jones (American musician)|Howard Jones]] on [[lead vocals]]
| title8 = AOJB studio outtakes
| length8 = 1:17
}}

==Personnel==
;Killswitch Engage
* [[Jesse Leach]] – [[lead vocalist|lead vocals]]
* [[Joel Stroetzel]] – [[lead guitar]], [[rhythm guitar]]
* [[Mike D'Antonio]] – [[bass guitar]]
* [[Adam Dutkiewicz]] – [[Drum kit|drums]], [[guitar]]s, [[piano]], [[backing vocalist|backing vocals]], [[Record producer|production]], [[Audio engineer|engineering]]
;Additional musicians
* Becka Dutkiewicz – backing vocals
* [[Philip Labonte]] – backing vocals
* [[Tom Gomes]] – additional drumming
* [[Howard Jones (American musician)|Howard Jones]] - lead vocals on "Fixation on the Darkness (re-recorded version)
;Technical staff and photography
* [[Andy Sneap]] – [[Audio mixing (recorded music)|mixing]] and [[audio mastering|mastering]]
* Daniel Moss – photography
* Mike D'Antonio – art direction and design

==References==
* ''[[(Set This) World Ablaze]]'' [DVD]. Directed by: Denise Korycki. [[Roadrunner Records]], 2005. {{UPC|016861093990}}
* ''Alive or Just Breathing: Roadrunner Records 25th Anniversary Edition'' [Liner notes]. Oral history/Liner notes compiled by Ryan J. Downey. Roadrunner Records, 2005. {{UPC|016861809720}}

==Notes==
{{Reflist|2}}

{{KillswitchEngage}}

{{DEFAULTSORT:Alive Or Just Breathing}}
[[Category:Killswitch Engage albums]]
[[Category:2002 albums]]
[[Category:Roadrunner Records albums]]
[[Category:Albums produced by Adam Dutkiewicz]]