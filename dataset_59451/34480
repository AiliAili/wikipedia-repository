{{Use dmy dates|date=January 2017}}
{{Use Australian English|date=January 2017}}
{{Infobox military unit
|unit_name=1st Parachute Battalion (Australia)
|image=1 Para Bn (AWM 041494).jpg
|alt=Men wearing parachutes and helmets file into a military aircraft on the ground
|caption=Soldiers from the 1st Parachute Battalion boarding a DC-2 in 1944.
|dates=1943–46
|country=Australia
|branch=[[Australian Army]]
|type=[[Airborne forces]]
|role=[[Paratrooper|Parachute infantry]]
|size=[[Battalion]]
|command_structure=
|garrison=
<!-- Commanders -->
|current_commander=
|ceremonial_chief=
|colonel_of_the_regiment=
|notable_commanders=[[Lieutenant colonel (Australia)|Lieutenant Colonel]] [[John Overall (architect)|Sir John Overall]]
<!-- Insignia -->
|identification_symbol=
|identification_symbol_2=
<!-- Culture and history -->
|nickname=
|patron=
|motto=
|colors=
|march=
|mascot=
|battles=
|anniversaries=
|decorations=
|battle_honours=None
}}
The '''1st Parachute Battalion''' was a [[Paratrooper|parachute]] [[infantry]] [[battalion]] of the [[Australian Army]]. Raised for service during the [[World War II|Second World War]], it was formed in early 1943 from volunteers for [[Airborne forces|airborne]] training. Despite achieving a high level of readiness, the battalion did not see action during the war and was disbanded in early 1946.

==History==
Like the British Army, Australia did not have a parachute operations capability at the outbreak of the Second World War; however, the demonstration of the effectiveness of such forces by the Germans in the early stages of the conflict soon provided the impetus for their development.<ref name=Dennis410>Dennis et al. 2008, p. 410.</ref> Efforts to raise an operational parachute capability in the Australian Army began in November 1942, with 40 volunteers being selected for initial training with the newly formed [[Paratroop Training Unit RAAF|Paratroop Training Unit]]. The first descents were made at [[Tocumwal]] in [[New South Wales]], with the initial parachute courses consisting of four jumps.<ref name=Dennis410/><ref name=Lord16>Lord & Tennant 2000, p.16.</ref> By March 1943 enough personnel had been trained for the Army to consider forming a full parachute [[battalion]]. As a result, the 1st Parachute Battalion was raised at this time at [[RAAF Base Richmond|RAAF Station Richmond]] near [[Sydney]], New South Wales.<ref name=Long554>Long 1963, p. 554.</ref>

Initially, raised on a reduced scale of only two rifle [[Company (military unit)|companies]], the battalion's personnel were mainly drawn from volunteers from other Army units—mostly the [[Australian commandos|independent companies]] that had been set up in 1941–42 to carry out irregular warfare—and as a result, most of the battalion's personnel had seen active service prior to being accepted.<ref name=Lord16/> These volunteers completed their parachute training with 1st Parachute Training Unit before joining the battalion,<ref>Lord & Tennant 2000, p. 20.</ref> and upon completion of their training qualified to wear the [[maroon beret]], which was adopted by the 1st Parachute Battalion as a symbol of their elite status.<ref name=Digger>{{cite web|url=http://www.diggerhistory.info/pages-uniforms/1-para-regt.htm|archiveurl=https://web.archive.org/web/20080613150030/http://www.diggerhistory.info/pages-uniforms/1-para-regt.htm |title=1st Australian Parachute Battalion|publisher=Digger History|accessdate=31 August 2009 |archivedate=10 June 2011}}</ref> In April 1943, while based at [[Scheyville National Park|Scheyville Farm]],<ref name=Digger/><ref name=McNicol364>McNicol 1982, p. 364.</ref> the battalion raised a [[troop]] of [[Royal Australian Engineers|engineers]]. Consisting of six officers and 51 other ranks, the 1st Parachute Troop, Royal Australian Engineers, was specially trained to undertake clandestine demolitions work alongside the battalion's rifle companies.<ref name=McNicol363>McNicol 1982, p. 363.</ref>

As the battalion was to be Australia's first [[airborne forces|airborne]] unit it required extensive training. Consequently, in addition to basic parachute training at Richmond, the battalion also trained in jungle warfare at [[Canungra]] in [[Queensland]].<ref name=Digger/> In September 1943, Major John Overall, formerly of the 2/13th Field Company, Royal Australian Engineers,<ref name=McNicol363/> was appointed as commanding officer.<ref name=Long554/> Throughout this time training continued in the demolitions, tactics and parachuting, and as no reserve parachutes were used several fatalities occurred.<ref name=Lord16/> A third rifle company was formed in October 1943 and by January 1944 the battalion was at full strength.<ref name=Long554/> Following company and battalion level exercises the battalion was declared ready for operations in May 1944 and moved to [[Mareeba, Queensland|Mareeba]] airstrip in [[Queensland|North Queensland]].<ref name=Lord16/> A fourth rifle company was formed in June 1944. In August 1944 the battalion gained its own organic indirect fire support when it was joined by the parachute qualified [[1st Mountain Battery (Australia)|1st Mountain Battery, Royal Australian Artillery]], equipped with [[Ordnance QF 25-pounder Short|short 25 Pounder guns]].<ref name=Dunn120>Dunn  1999, p. 120.</ref>

[[File:1 Para bn (AWM 041495).jpg|thumb|left|Members of 'A' Company, 1st Parachute Battalion during a training flight in 1944|alt=Men with helmets sit in an aircraft with weapons held across their chests, strapped into parachutes]]

In late 1944, the battalion was alerted to begin preparations for operations in Borneo as part of the [[Borneo campaign (1945)|Borneo campaign]].<ref name=Lord16/> As well as preparing for airborne operations, the battalion also conducted amphibious training in late January and early February 1945 as part of a possible role in the amphibious landing at [[Battle of Balikpapan (1945)|Balikpapan]].<ref>Dunn 1999, p. 195.</ref> The battalion was not used in this operation, however, due to a shortage of suitable aircraft.<ref name=McNicol363/><ref name="Moffit 1989, p. 229">Moffit 1989, p. 229.</ref>{{#tag:ref|The battalion required between 24 and 100 [[C-47 Skytrain|C-47]] transports to lift it (24 if dropped in waves, 100 in one drop).<ref name=McNicol363/><ref name="Moffit 1989, p. 229"/>|group=Note}} A few months later, the battalion was also warned to prepare for a mission to rescue thousands of Allied prisoners held by the Japanese at [[Sandakan camp|Sandakan]] in North Borneo. This operation, codenamed [[Operation Kingfisher (World War II)|Operation Kingfisher]], was controversially also cancelled due to a lack of aircraft, and the prisoners were subsequently killed by the Japanese in what subsequently became known as the [[Sandakan Death Marches]].<ref>Moffit 1989, p. 228.</ref> The disappointment of not being deployed to Borneo caused significant frustration within the battalion, with many soldiers requesting transfers to other infantry units such as [[Z Special Unit]].<ref name=Lord16/> Many of these requests were denied, however, as the battalion had been instructed to prepare to operate alongside British paratroopers in the planned liberation of [[Singapore]] that was to have taken place later in 1945 as part of [[Operation Zipper|Operations Zipper and]] [[Operation Mailfist|Mailfist]].<ref name=McNicol364/>

The war ended before these operations took place, however, and following the [[Surrender of Japan|Japanese surrender]] the battalion was ordered to prepare to deploy to Singapore for garrison duties. While an advance party of 120 men arrived in Singapore on 9 September, the rest of the battalion remained in Australia. The unit contributed an honour guard to the main surrender ceremony. Afterwards, a further 75 men were sent out to join them and together this force performed general garrison and policing duties before returning to Australia in January 1946.<ref name=Long554/> Orders were received to disband the battalion on 29 January 1946, and these were carried out the following day at Sydney.<ref name=Lord16/>

==Composition==
The 1st Parachute Battalion was organised with the following sub units:
* Battalion Headquarters
** Battalion Headquarters Company
** 'A' Company
** 'B' Company
** 'C' Company (from October 1943)
** 'D' Company (from June 1944)<ref name=Lord16/>
** 1st Parachute Troop, [[Royal Australian Engineers]]<ref name=McNicol363/>
** 1st Mountain Battery (from August 1944).<ref name=Dunn120/>

==See also==
* [[Airborne forces of Australia]]

==Notes==
;Footnotes
{{Reflist|group=Note}}

;Citations
{{Reflist|30em}}

==References==
{{refbegin}}
*{{cite book|last1=Dennis|first1=Peter|last2=Grey|first2=Jeffrey|authorlink2=Jeffrey Grey |last3=Morris|first3=Ewan|last4=Prior|first4=Robin|last5=Bou|first5=Jean|title=The Oxford Companion to Australian Military History|publisher=Oxford University Press|location=Melbourne|year=2008|edition=Second|isbn= 0195517849}}
* {{cite book |last=Dunn |first=J.B. |year=1999 |title= Eagles Alighting: A History of 1st Australian Parachute Battalion |publisher=1 Australian Parachute Battalion Association |location=East Malvern, Victoria |isbn=0-646-37323-4}}
* {{cite book|last=Long|first=Gavin|authorlink=Gavin Long|title=The Final Campaigns|series=Australia in the War of 1939–1945, Series 1—Army. Volume VII|year=1963|edition=1st|publisher=Australian War Memorial| location=Canberra, Australian Capital Territory|url=https://www.awm.gov.au/collection/RCDIG1070206/|oclc=1297619}}
* {{cite book |last=Lord |first=Cliff |author2=Tennant, Julian |year=2000 |title=ANZAC Elite: The Airborne & Special Forces Insignia of Australia and New Zealand |publisher=IPL Books |location=Wellington |isbn=0-908876-10-6 |oclc=}}
* {{cite book|last=McNicoll|first=Ronald|authorlink=Ronald McNicoll |title=The Royal Australian Engineers 1919 to 1945: Teeth and Tail|series=History of the Royal Australian Engineers, Volume 3|year=1982|publisher=The Corps Committee of the Royal Australian Engineers|location=Canberra, Australian Capital Territory|isbn=978-0-9596871-3-2}}
* {{cite book |last=Moffit |first=Athol |year=1989 |title=Project Kingfisher |publisher=ABC Books |location=Sydney, New South Wales |isbn=0-7333-0489-3 |oclc=}}
{{refend}}

{{Good Article}}

[[Category:Australian World War II battalions|P]]
[[Category:Military units and formations established in 1943|P]]
[[Category:Infantry units and formations of Australia|P]]
[[Category:Airborne units and formations of Australia|P]]
[[Category:Military units and formations disestablished in 1946|P]]
[[Category:Parachuting in Australia]]