{{For|the disused station in Queensland|Goodwood railway station, Queensland}}
{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox station
| name                = Goodwood
| image               = Levels of Transport (5184588840).jpg
| caption             = Goodwood Station with the [[Glenelg Tram]] overpass.
| address             = Railway Terrace North<br />Goodwood SA 5034
| coordinates         = {{coord|-34.9515825|138.5849102|format=dms|type:railwaystation_region:AU-SA|display=inline,title}}
| owned               = [[Department of Planning, Transport and Infrastructure|DPTI]]
| operator            = [[Adelaide Metro]]
| line                = [[Belair railway line|Belair Line]], [[Seaford railway line|Seaford Line]], [[Tonsley railway line|Tonsley Line]]
| distance            = 5&nbsp;km from [[Adelaide railway station|Adelaide]]
| platforms           = 3 (2 Centre)
| tracks              = 4
| structure           = Ground
| parking             = Yes
| bicycle             = No
| disabled            = No
| bus_routes          = 
| code                = 
| zone                = 
| website             = 
| opened              = 5 March 1883
| closed              = 
| rebuilt             = 1929
| services            = 
{{s-rail|title=TransAdelaide}}
{{s-line|system=TransAdelaide|line=Belair|previous=Adelaide Showground|next=Millswood|rows1=3}}
{{s-line|system=TransAdelaide|line=Seaford|previous=Adelaide Showground|next=Clarence Park|hide1=yes}}
{{s-line|system=TransAdelaide|line=Tonsley|previous=Adelaide Showground|next=Clarence Park|hide1=yes}}
}}

'''Goodwood railway station''' is the junction station for the [[Belair railway line|Belair]] and [[Seaford railway line|Seaford]]/[[Tonsley railway line|Tonsley]] lines.<ref>[http://www.adelaidemetro.com.au/var/metro/storage/original/application/6ede59c8022d206c992773d85ff753a4.pdf Belair timetable] {{webarchive|url=https://web.archive.org/web/20150123141703/http://www.adelaidemetro.com.au/var/metro/storage/original/application/6ede59c8022d206c992773d85ff753a4.pdf |date=23 January 2015 }} Adelaide Metro 12 October 2014</ref><ref>[http://www.adelaidemetro.com.au/var/metro/storage/original/application/85a61478521f6729a670aaeb5a7624d6.pdf Seaford & Tonsley timetable] Adelaide Metro 20 July 2014</ref> The Belair line diverges south-east towards [[Millswood railway station|Millswood]], while the Seaford and Tonsley lines diverge south-west towards [[Clarence Park railway station|Clarence Park]]. The [[Glenelg Tram]] line crosses over the railway lines at the south end of Goodwood station.

The station services the [[Adelaide]] inner-southern suburb of [[Goodwood, South Australia|Goodwood]], five kilometres from [[Adelaide railway station|Adelaide station]]. It opened on 5 March 1883 with the opening of the [[Adelaide railway station|Adelaide]] to [[Aldgate railway station, Adelaide|Aldgate]] section of the [[Melbourne-Adelaide railway|Adelaide-Melbourne line]].

==Platform and track usage==

===Current===
Goodwood station has three platforms serving the three [[broad gauge]] tracks of the three southern metropolitan lines. The fourth and western most track, which is part of the [[Melbourne-Adelaide railway|standard gauge line]] to [[Melbourne]], bypasses the no-longer-used fourth platform.

From the north of the stations, there are four tracks between Greenhill Road and the station. The western most track is standard gauge, and the other three are broad gauge.

The yard immediately south of the station allows trains on broad gauge lines to transfer from any of the three platforms served by broad gauge to any of the broad gauge lines. The three broad gauge tracks then split into four broad gauge tracks, with two tracks heading south-east along the Belair line alignment, and the other two tracks heading southwest along the Seaford and Tonsley alignment. This also allows a crossing loop on the Belair line.

The western most and now fifth track, (standard gauge), remains separate from all of the broad gauge tracks, and then crosses the two Seaford tracks (from west to east). Keeping west of the two Belair tracks, it heads south-east and follows the Belair alignment. The two Belair broad gauge tracks then merge into one, and follow the eastern side of the Belair alignment. The standard gauge track then follows the western side of the Belair alignment; i.e. south-east of the Seaford / Belair separation, (sometimes referred to as the Goodwood junction), the standard gauge track is where (prior to 1995) the western of the dual broad gauge tracks from Goodwood to Belair used to be.

===Pre-1995===
Prior to the [[One Nation (infrastructure)|One Nation]] gauge standardisation project in 1995, all four tracks at Goodwood were broad gauge, and the Belair line was dual broad gauge tracks. In 1995, the western track of the Belair line was converted to standard gauge, and the Belair line is now only a single track of each gauge.

The Belair trains typically used Platform 1 for outbound services, and Platform 2 for inbound services, whilst the Seaford and Tonsley lines used Platform 3 for outbound services, and Platform 4 for inbound services. The yard immediately south allowed trains to transfer from any platform to any line. The most common use of this feature was when Inter/Intrastate treight and passenger trains would use the Seaford tracks to Goodwood from Keswick Rail Yards, then change to the Belair tracks through Goodwood Yard.

==Bridges and underpass==
[[File:Tram Bridge Over Railway (16491057599).jpg|thumb|The tram bridge]]
Prior to 1929, what is now the [[Glenelg Tram]] line was a railway line that crossed the main south line on a [[level junction]]. As part of its conversion from a broad gauge railway to a standard gauge tramway, the bridge was built to carry the trams safely over the railway lines.<ref>{{cite news|url=http://nla.gov.au/nla.news-article53453606|title=TRAMWAY BRIDGE At GOODWOOD|date=16 May 1929|newspaper=[[The Register News-Pictorial]]|issue=27,379|location=South Australia|volume=XCIV,|page=31|via=National Library of Australia|accessdate=2 June 2016}}</ref>

[[File:Adelaide Metro Train 3135 (10959517806).jpg|thumb|Train on the Belair line passing the underpass]]
The Goodwood Junction Upgrade separated the existing freight and [[Belair railway line|Belair]] passenger lines from the [[Seaford railway line|Seaford]] line by a [[grade separation]]. A rail underpass near Victoria Street lowered the Seaford line below ground level, with the freight and Belair lines above. It was completed in 2014<ref>[http://www.infrastructure.sa.gov.au/RR/rail_revitalisation/goodwood_junction Goodwood Junction upgrade] Department of Planning, Transport & Infrastructure</ref><ref>[http://www.yorkcivil.com.au/project/goodwood-junction-rail-grade-separation-2/ Goodwood Junction Rail Grade Separation] York Civil</ref>

A project to construct a footbridge adjacent to the tram bridge was announced in May 2016. The footbridge will provide a new access point to the station.<ref>{{cite news|last1=Jervis-Bardy|first1=Dan|title=$10m bridge to take cyclists safely over top of Goodwood train station|url=http://www.adelaidenow.com.au/messenger/east-hills/10m-bridge-to-take-cyclists-safely-over-top-of-goodwood-train-station/news-story/4a5cda3671b119eb79fb451dc289e5cd?nk=9e8d51302428f93938ae7f23f9f89ec1-1472305882|work=Eastern Courier Messenger|date=16 May 2016}}</ref>

==Royal Adelaide Show==
Goodwood station was the main railway station servicing the [[Royal Adelaide Showgrounds]]. However between 2005 and 2013 during the [[Royal Adelaide Show]], [[TransAdelaide]] closed Platform 1 at Goodwood, and exclusively devoted the easternmost of the four tracks between Adelaide and Goodwood to a non-stop shuttle service between Platform 1 at [[Adelaide railway station|Adelaide station]] and a temporary platform at the Showgrounds named [[Showground Central railway station|Showground Central]].<ref>[https://web.archive.org/web/20130428141154/http://theshow.com.au/showground/royal-adelaide-show/plan-your-show-day/getting-there.jsp Getting There] Adelaide Show</ref> During this period, Belair line movements were merged with (the previously) Noarlunga Line movements through Platforms 2 and 3 at Goodwood. In 2014, Showground Central was replaced by the permanent [[Adelaide Showground railway station|Adelaide Showground station]] with existing services making additional stops.

==References==
{{Reflist}}

==External links==
*{{commons category-inline}}
*[http://www.johnnyspages.com/classic_pictures_menu_files/classic_present_day_pics_files/goodwood_junction_upgrade.htm Goodwood Station Upgrade] gallery
*[https://www.flickr.com/photos/baytram366/sets/72157629139329515/ Flickr gallery]

{{Belair railway line, Adelaide}}
{{Noarlunga Centre railway line, Adelaide}}
{{Tonsley railway line, Adelaide}}

[[Category:Railway stations in Adelaide]]
[[Category:Railway stations opened in 1883]]