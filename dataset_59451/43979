<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Classic
 | image=Flugplatzfest Rechlin-Lärz 2008 (8739744310).jpg
 | caption=A Fisher Classic, equipped with a [[Rotax 914]] power plant
}}{{Infobox Aircraft Type
 | type=[[Kit aircraft]]
 | national origin=[[Canada]]
 | manufacturer=[[Fisher Flying Products]]
 | designer=
 | first flight=1987
 | introduced=1987
 | retired=
 | status=
 | primary user=
 | number built=155 (2011)<ref name="KitplanesDec2011" />
 | developed from=
 | variants with their own articles=
}}
|}
The '''Fisher Classic''' is a [[Canada|Canadian]] two-seat, [[conventional landing gear]], single-engined, [[biplane]] [[kit aircraft]] designed for construction by amateur builders. The aircraft is a two-seat derivation of the [[Fisher FP-404]]. Fisher Flying Products was originally based in [[Edgeley, North Dakota]], [[United States|USA]] but the company is now located in [[Woodbridge, Ontario]], [[Canada]].<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 53. Belvoir Publications. ISSN 0891-1851</ref><ref name="KitplanesDec2004">Downey, Julia: ''Kit Aircraft Directory 2005'', Kitplanes, Volume 21, Number 12, December 2004, page 58. Belvoir Publications. ISSN 0891-1851</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook'', page 161. BAI Communications. ISBN 0-9636409-4-1</ref><ref name="KitplanesDec1998">Kitplanes Staff: ''1999 Kit Aircraft Directory'', Kitplanes, Volume 15, Number 12, December 1998, page 48. Primedia Publications. IPM 0462012</ref><ref name="FisherClassic">{{cite web|url = http://www.fisherflying.com/index.php?option=com_content&view=article&id=38&Itemid=22|title = Classic |accessdate = 2009-10-25|last = Fisher Flying Products|authorlink = |year = n.d.}}</ref><ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page B-79. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref>

==Design and development==
The Classic was designed by Fisher Aircraft in the [[United States]] in 1987 and was intended to comply with the US ''[[Homebuilt aircraft|Experimental - Amateur-built]]'' category, although it qualifies as an ultralight aircraft in some countries, such as [[Canada]]. It also qualifies as a US Experimental [[Light Sport Aircraft]]. The Classic's standard empty weight is {{convert|400|lb|kg|0|abbr=on}} when equipped with a {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine and it has a gross weight of {{convert|850|lb|kg|0|abbr=on}}.<ref name="Aerocrafter" /><ref name="FisherClassic" /><ref name="Cliche" />

The construction of the Classic is of wood, with the wings, tail and fuselage covered with doped [[aircraft fabric]]. The aircraft features [[interplane strut]]s, inverted "V" [[cabane strut]]s, four ailerons and a semi-symmetrical airfoil. Like the original FP-404 upon which it is based, the Classic has no flaps. The Classic's main landing gear is [[bungee cord|bungee]] suspended. Cockpit access is via the lower wing. The company claims an amateur builder can complete the aircraft from the kit in 500 hours.<ref name="Aerocrafter" /><ref name="FisherClassic" /><ref name="Cliche" />

The specified engine for the Classic is the {{convert|64|hp|kW|0|abbr=on}} Rotax 582 two-stroke engine.<ref name="KitplanesDec2004" /><ref name="Aerocrafter" /><ref name="FisherClassic" /><ref name="Cliche" />

The estimated time to build the aircraft from the kit is 400–500 hours, or 250 hours from the quick-build kit. By late 2011 more than 155 Classics were flying.<ref name="KitplanesDec2011" /><ref name="Cliche" />

==Specifications Classic) ==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Company website, AeroCrafter & Kitplanes<ref name="KitplanesDec2004" /><ref name="Aerocrafter" /><ref name="KitplanesDec1998" /><ref name="FisherClassic" /><!-- the source(s) for the information -->
|crew=one
|capacity=one passenger<!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 16 ft 9 in
|length alt=5.11 m
|span main=22 ft 0 in
|span alt=6.71 m
|height main=5 ft 11 in
|height alt=1.80 m
|area main= 154 sq ft
|area alt= 14.32 sq m
|airfoil=
|empty weight main= 400 lbs
|empty weight alt= 181 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 450 lb
|useful load alt= 204 kg
|max takeoff weight main= 850 lbs
|max takeoff weight alt= 385 kg
|max takeoff weight more=
|more general=
|engine (jet)=
|type of jet=
|number of jets=
|thrust main= 
|thrust alt= 
|thrust original=
|afterburning thrust main=
|afterburning thrust alt= 
|thrust more=
|engine (prop)=[[Rotax 582]]
|type of prop=[[Two stroke]], two cylinder [[aircraft engine]]<!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 64 hp
|power alt=48 kW
|power original=
|power more=
|propeller or rotor?=propeller
|propellers=
|number of propellers per engine= 1
|propeller diameter main=
|propeller diameter alt= 
|max speed main= 90 mph
|max speed alt=146 km/h
|max speed more= 
|cruise speed main= 80 mph
|cruise speed alt=130 km/h
|cruise speed more 
|stall speed main= 39 mph
|stall speed alt= 63 km/h
|stall speed more=
|never exceed speed main= 100 mph
|never exceed speed alt= 162 km/h
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 
|ceiling alt= 
|climb rate main= 600 fpm
|climb rate alt= 3.1 m/s
|loading main=5.2 lb/sq ft
|loading alt=26.9 kg/sq m
|thrust/weight=
|power/mass main=13.3 lb/hp
|power/mass alt=0.125 kW/kg
|more performance=
|armament=<!-- if you want to use the following specific parameters, do not use this line at all-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Fisher Celebrity]]
*[[Fisher FP-404]]
*[[Fisher R-80 Tiger Moth]]
*[[Murphy Renegade]]
*[[Sorrell Hiperlight]]
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{commons category|Fisher Classic}}
*[http://www.fisherflying.com/index.php?option=com_content&view=article&id=38&Itemid=22 Official website]
*[http://www.airport-data.com/images/aircrafts/small/064/064514.jpg Photo of a Classic]

{{Fisher Flying Products}}

[[Category:Canadian ultralight aircraft 1980–1989]]
[[Category:Light-sport aircraft]]