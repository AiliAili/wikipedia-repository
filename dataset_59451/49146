{{Use mdy dates|date=November 2012}}
{{Infobox artist discography
| Artist       = La Oreja de Van Gogh
| Image        = La Oreja de Van Gogh Deseos de Cosas Imposibles San Jose Costa Rica.jpg
| Caption      = 
| Studio       = 6
| Compilation  = 2
| Live         = 1
| EP           = 1
| Music videos = 26
| Singles      = 35
| Option       = 1
| Option name  = Box sets
}}

The discography of Spanish pop rock band [[La Oreja de Van Gogh]] consists of six [[studio album]]s, one [[live album]], two [[compilation album]]s, one [[extended play]], one [[box set]], thirty five [[single (music)|single]]s and twenty six [[music video]]s. The band have sold over 8 million records worldwide,<ref>{{cite news|url=http://www.europafm.com/noticias/oreja-van-gogh-estrena-nuevo-single-nina-que-llora-tus-fiestas_2011072800212.html|title=La Oreja de Van Gogh estrena nuevo single La niña que llora en tus fiestas|work=EuropaFm|accessdate=January 1, 2013}}</ref><ref>{{cite news|url=http://mx.mujer.yahoo.com/lanzar%C3%A1-oreja-gogh-m%C3%A9xico-cd-cometas-cielo-214400966.html|title=Lanzará La Oreja de Van Gogh en México su CD "Cometas por el cielo"|work=Yahoo.com|accessdate=January 6, 2013}}</ref> making them the best selling pop band in Spain and the country's most influential pop group since [[Mecano]].<ref name="USOpens" /> La Oreja de Van Gogh released their debut studio album ''[[Dile Al Sol]]'' on May 18 of 1998. It was a commercial success in Spain, eventually peaking at number 1 and being certified 7 times Platinum in the country.<ref name="CertDAS" /> The band's second studio album, ''[[El viaje de Copperpot]]'', was released on September 11 of 2000. It is the band's most successful album in Spain; selling more than 1,200,000 copies there, becoming Sony's Spain second highest selling album in history.<ref name="USOpens" /> It also catapulted the band's fame and success in [[Latin America]]. The first three singles reached number 1 in Spain, Mexico and most Latin American countries.

La Oreja de Van Gogh's third album was releasead in April 28 of 2003. Internationally, it is the most successful album of the band and it is said to be their consolidation album in the music industry. Like their previous work, its first three singles; "Puedes contar conmigo", "20 de Enero" and "Rosas" peaked #1 in Spain and most latinamerican charts.<ref name="40Bio">{{cite news|url=http://www.los40.com/musica/artista/biografia/musica-biografia/10000000141.aspx|title=La Oreja de Van Gogh Biografía|work=Los40|accessdate=January 1, 2013}}</ref> After three years, the band released their fourth album, ''[[Guapa]]'' and the extended play ''[[Guapa|Más guapa]]'', the last to feature [[Amaia Montero]]. The first single "Muñeca de Trapo" manage to reach #1 position on the Spanish and Mexican charts and while the rest of the singles reached high positions in Spain, they had modest impact in latinamerica. After the separation of former vocalist Amaia Montero, they released their first compilation album "LOVG - Grandes éxitos". Their fifth studio album [[A las cinco en el Astoria]] features new vocalist of the band [[Leire Martínez]]. Four singles were released from the album. The first, "[[El Último Vals]]", had average success in Spain and latinamerica. On September 13, 2011 the band released ''[[Cometas por el cielo]]''. The two first singles, "La Niña que Llora en tus Fiestas" and "Cometas por el Cielo", reached the top 20 in Spain and Latin America.<ref name=Mexcharts /><ref name=Spcharts>http://spanishcharts.com/</ref>

==Albums==

===Studio albums===
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
|+ List of studio albums, with selected chart positions, sales figures and certifications
! scope="col" rowspan="2" style="width:12em;"| Title
! scope="col" rowspan="2" style="width:14em;"| Album details
! scope="col" colspan="4"| Peak chart positions
! scope="col" rowspan="2" style="width:14em;"| [[List of music recording certifications|Certifications]]
! scope="col" rowspan="2" style="width:12em;"| Sales
|-
! scope="col" style="width:3em;font-size:85%;"|[[Productores de Música de España|SPA]]<br/><ref name=spa1>
*''Tell the Sun'': {{cite web|url=http://afyvecharts.blogspot.com/2007/08/1999-albumes-1-parte.html|title=LISTAS DE AFYVE:1999 Albumes (1ª parte)|publisher=[[Promusicae]]|accessdate=January 1, 2013}}
*''El viaje de Copperpot'': {{cite web|url=http://afyvecharts.blogspot.com/2007/08/2001-albumes-1-parte.html|title=LISTAS DE AFYVE: 2001 (1ª parte)|publisher=[[Promusicae]]|accessdate=January 1, 2013}}
*''Lo que te conté mientras te hacías la dormida'': {{cite web|url=http://afyvecharts.blogspot.com/2007/08/2003-albumes-2-parte.html|title=LISTAS DE AFYVE: 2003 Albumes (2ª parte)|publisher=[[Promusicae]]|accessdate=January 3, 2013}}
*{{cite web | url=http://spanishcharts.com/search.asp?cat=a&search=la+oreja+de+van+gogh | title=Spanish Albums Chart | publisher=Spanish Charts | work=PROMUSICAE | date=August 27, 2006 | accessdate=January 1, 2013}}</ref>
! scope="col" style="width:2.5em;font-size:90%;"|[[Argentine Chamber of Phonograms and Videograms Producers|ARG]]<br/><ref>{{cite web |url=http://adminlic.capif.org.ar/sis_resultados_rankings_web.aspx |title=Ranking Semanal Pop |language=Spanish |publisher=[[Argentine Chamber of Phonograms and Videograms Producers]] |date=November 11, 2012 |accessdate=January 3, 2013}} Note: Reader must define search parameters as "Ranking Semanal Pop" and "11/11/2012".</ref>
! scope="col" style="width:2.5em;font-size:90%;"|[[Top 100 Mexico|MEX]]<br/><ref name=Mexcharts>{{cite web |url=http://mexicancharts.com/search.asp?search=la+oreja+de+van+gogh&cat=a|title=La Oreja de Van Gogh Mexican Charts |work=''mexicancharts.com'' |publisher=Hung Medien |accessdate=January 3, 2013}}</ref>
! scope="col" style="width:2.5em;font-size:90%;"|[[Top Latin Albums|US<br />Latin]]<br/><ref name=bb2LO>{{cite web|title=La Oreja de Van Gogh Album & Song Chart History: Latin Albums|url={{BillboardURLbyName|artist=la oreja de van gogh|chart=Latin Albums}}|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=November 17, 2016}}</ref>
|-
! scope="row" | ''[[Dile Al Sol]]''
|
*Released: May 18, 1998
*Label: Epic
*Format: CD, cassette
| 1 || — || — || — 
|style="text-align:left;"|
*SPA: 7× Platinum<ref name="CertDAS">{{cite web|url=http://www.promusicae.es/files/listassemanales/albumes/historial/TOP%20100%20ALBUMES%2006_17.pdf|title=Top 100 álbumes|publisher=[[Productores de Música de España]]|accessdate=January 2, 2013}}</ref>
|style="text-align:left;"|
*SPA: 800,000<ref name="USOpens">{{cite news|url=http://books.google.co.cr/books?id=0A8EAAAAMBAJ&pg=PA49&lpg=PA49&dq=best+selling+artists+in+spain+mecano+la+oreja+de+van+gogh&source=bl&ots=ax36OeMLsn&sig=RKL73tNubP4SvsGv4PmN_4M4mLQ&hl=es-419&sa=X&ei=GljjUKTuDYaY8gTz5YHIBQ&ved=0CEEQ6AEwAg#v=onepage&q=best%20selling%20artists%20in%20spain%20mecano%20la%20oreja%20de%20van%20gogh&f=false|title=U.S. Open Hears to La Oreja de Van Gogh|last=Cobo|first=Leila|work=[[Billboard (magazine)|''Billboard'' (magazine)]]|publisher=Prometheus Global Media|date=March 13, 2004|accessdate=January 3, 2013}}</ref>
*USA: 40,000<ref name="USOpens" />
|-
! scope="row" | ''[[El viaje de Copperpot]]''
|
*Released: September 11, 2000
*Label: Epic
*Format: CD, cassette
| 1 || — || 1 || —  
|style="text-align:left;"|
*SPA: 11× Platinum<ref>{{cite web|url=http://www.promusicae.es/files/listassemanales/albumes/historial/TOP%20100%20ALBUMES%2005_41.pdf|title=Top 100 álbumes|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
*MEX: 2× Platinum <ref name="amprcert">{{cite web | publisher=[[Asociación Mexicana de Productores de Fonogramas y Videogramas]] (AMPROFON)| title=Certificaciones Encontradas: La Oreja de Van Gogh|url=http://www.amprofon.com.mx/certificaciones.php?artista=thalia&titulo=&disquera=&certificacion=todas&anio=todos&categoria=todas&Submitted=Buscar&item=menuCert&contenido=buscar|format=AMPROFON updates its database every so often, so some albums do not appear|accessdate=January 3, 2013}}</ref>
*US: Platinum (Latin)<ref name=riaa>{{cite web|title=Gold & Platinum: La Oreja de Van Gogh|url=http://www.riaa.com/goldandplatinumdata.php?resultpage=1&table=SEARCH_RESULTS&action=&title=El%20viaje%20de%20Copperpot&artist=LA%20OREJA%20DE%20VAN%20GOGH&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=Platinum&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2010&sort=Artist&perPage=25|publisher=[[Recording Industry Association of America]]|accessdate=January 2, 2013}}</ref>

|style="text-align:left;"|
*WW: 2,500,000<ref>{{cite news|url=http://www.musica.com/letras.asp?info=65234&biografia=8453&idf=5|title=LODVG biografía|work=Musica.com|accessdate=January 1, 2013}}</ref>
*SPA: 1,200,000<ref name="USOpens" />
*MEX: 400,000<ref name="USOpens" />
*USA: 100,000<ref name="USOpens" />
|-
! scope="row" | ''[[Lo Que te Conté Mientras te Hacías la Dormida]]''
|
*Released: April 28, 2003
*Label: Epic
*Format: CD, cassette
| 1 || 1 || 1 || 9 
|style="text-align:left;"|
*SPA: 6× Platinum<ref>{{cite web|url=http://www.promusicae.es/files/listasanuales/albumes/Top%2050%20albumes%202004.pdf|title=Top 100 álbumes 2004|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
*ARG: 4× Platinum<ref name="capi">{{cite web
 |url= http://www.capif.org.ar/Default.asp?CodOp=ESOP&CO=5
 |title=Discos de Oro y Platino
 |trans_title=Gold and Platinum discs
 |work=capif.org.ar
 |accessdate=January 3, 2013
}}</ref>
*CHL: 2× Platinum<ref name="LatinCert">{{cite web|url=http://www.nosotras.com/actualidad/la-oreja-van-gogh-conquista-america-8255|title=La Oreja de Van Gogh conquista América|publisher=Nosotras Magazine|accessdate=December 14, 2013}}</ref>
*COL: Platinum<ref name="LatinCert"/>
*ECU: Gold<ref name="LatinCert"/>
*MEX: Platinum+Gold<ref name="amprcert" />
*URU: Gold<ref name="LatinCert"/>
*VEN: Gold<ref name="LatinCert"/>
*US: 2× Platinum (Latin)<ref name=riaa /> 
|style="text-align:left;"|
*WW: 2,000,000<ref name="AM">{{cite news|url=http://www.caracol.com.co/noticias/actualidad/amaia-montero/20081117/nota/712274.aspx|title=Amaia Montero Biografía|work=[[Caracol Radio]]|accessdate=January 6, 2013}}</ref><ref name="SpainLends">{{cite news|url=http://books.google.co.cr/books?id=dA4EAAAAMBAJ&pg=PA63&lpg=PA63&dq=lo+que+te+conte+mientras+te+hacias+la+dormida+160,000+argentina&source=bl&ots=8sx2GshxBw&sig=ZKUX5GX9HY2ilJjw1yDoiRjs5fQ&hl=es-419&sa=X&ei=5cDjUNHkIY6g8QSMkoGYAQ&ved=0CC8Q6AEwADgK#v=onepage&q=lo%20que%20te%20conte%20mientras%20te%20hacias%20la%20dormida%20160%2C000%20argentina&f=false|title=Spain Lends Ear|last=Williamson|first=Nigel|work=[[Billboard (magazine)|''Billboard'' (magazine)]]|publisher=Prometheus Global Media|date=June 28, 2003|accessdate=January 1, 2013}}</ref>
*SPA: 700,000<ref>{{cite web|url=http://www.elmundo.es/metropoli/2004/04/21/musica/1082547804.html|title=Predestinados a triunfar|work=[[El Mundo (Spain)]]|publisher=Pedro J. Ramírez|accessdate=January 6, 2013}}</ref>
*CHI: 88,000<ref>{{cite web|url=http://www.radioagricultura.cl/client/18734-Conoce-los-discos-m%C3%A1s-vendidos-del-siglo-XXI-en-Chile.html|title=Conoce los discos más vendidos del siglo XXI en Chile|work=Radioagricultura.cl|accessdate=July 30, 2013}}</ref>
*USA: 260,000<ref name="HearginVan">{{cite news|url=http://books.google.co.cr/books?id=NBYEAAAAMBAJ&pg=PA34&dq=la+oreja+de+van+gogh+billboard&hl=es-419&sa=X&ei=P13kULnJC4yA9gThl4GQDg&ved=0CDQQ6AEwAQ#v=onepage&q=la%20oreja%20de%20van%20gogh%20billboard&f=false|title=Hearing Van Gogh's Ear|last=Cobo|first=Leila|work=[[Billboard (magazine)|''Billboard'' (magazine)]]|publisher=Prometheus Global Media|date=July 15, 2006|accessdate=January 1, 2013}}</ref>
|-
! scope="row" | ''[[Guapa]]/[[Guapa|Más guapa]]''
|
*Released: April 25, 2006
*Label: Epic
*Format: CD, digital
| 1 || 1 || 1 || 5 
|style="text-align:left;"|
*SPA: 7× Platinum<ref>{{cite web|url=http://www.promusicae.es/files/listasanuales/albumes/Top%2050%20albumes%202006.pdf|title=Top 100 álbumes 2006|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
*ARG: Platinum<ref name="capi" />
*CHL: Gold<ref name="GuapaCert">{{cite web|url=http://www.los40.com.mx/actualidad/noticias/la-oreja-de-van-gogh-obtiene-disco-de-platino/20060905/nota/328873.aspx|title=La Oreja de Van Gogh obtiene disco de Platino|publisher=Los 40 Mex|accessdate=December 14, 2013}}</ref>
*COL: Gold<ref name="GuapaCert"/>
*MEX: Platinum<ref name="amprcert" />
|style="text-align:left;"|
*WW: 1,000,000<ref name="Xtt">{{cite news|url=http://www.digitaltraduc.com/ARTICULOS_INTERES/Musica-Oreja_Van_Gogh.html|title=LA OREJA DE VAN GOGH Un poco de música en la vida|work=digitaltraduc|date=December 12, 2006|accessdate=January 1, 2013}}</ref>
*SPA: 600,000<ref>{{cite news|url=http://www.elmundo.es/elmundo/2006/12/03/cultura/1165163063.html|title=La Oreja de Van Gogh celebra sus 10 años con 'Más guapa', una edición especial de 'Guapa'|work=[[El Mundo (Spain)]]|date=December 12, 2006|accessdate=January 1, 2013}}</ref>
*USA: 75,000<ref name="splits">{{cite news|url=http://www.billboard.com/biz/articles/news/1316681/montero-splits-from-la-oreja-de-van-gogh|title=Montero Splits From La Oreja de Van Gogh|last=Cobo|first=Leila|work=[[Billboard (magazine)|''Billboard'' (magazine)]]|publisher=Prometheus Global Media|date= November 19, 2007|accessdate=December 14, 2013}}</ref>
|-
! scope="row" | ''[[A las cinco en el Astoria]]''
|
*Released: September 2, 2008
*Label: Epic
*Format: CD, digital
| 1 || 1 || 20 || 21 
|style="text-align:left;"|
*SPA: 2× Platinum <ref name="Top 100 álbumes 2009">{{cite web|url=http://www.promusicae.es/files/listasanuales/albumes/Top%2050%20ALBUMES%202009%20.pdf|title=Top 100 álbumes 2009|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
*MEX: Platinum<ref name="amprcert" />
|style="text-align:left;"|
*SPA: 150,000<ref>{{cite news|url=http://www2.esmas.com/ritmoson-latino/artistas/251230/?page=1|title=La Oreja de Van Gogh Biografía|work=Ritmosonlatino|accessdate=January 1, 2013}}</ref>
|-
! scope="row" | ''[[Cometas por el cielo]]''
|
*Released: September 13, 2011
*Label: Epic
*Format: CD, digital
| 1 || 2 || 21 || 2 
|style="text-align:left;"|
*SPA: Platinum<ref name="Top 100 álbumes 2009"/>
|style="text-align:left;"|
|-
! scope="row" | ''[[El planeta imaginario]]''
|
*Released: November 4, 2016
*Label: Epic
*Format: CD, digital
| 1 || — || — || 9
|style="text-align:left;"|
|style="text-align:left;"|
|}

===Live albums===
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
|+ List of live albums, with selected chart positions, sales figures and certifications
! scope="col" rowspan="2" style="width:12em;"| Title
! scope="col" rowspan="2" style="width:16em;"| Album details
! scope="col" colspan="4"| Peak chart positions
! scope="col" rowspan="2"| [[List of music recording certifications|Certifications]]
|-
|-
! scope="col" style="width:3em;font-size:85%;"|[[Productores de Música de España|SPA]]<br><ref>http://promusicae.org/listassemanales/dvds/historial/TOP%2020%20DVD%2004_02.pdf</ref>
! scope="col" style="width:2.5em;font-size:90%;"|[[Top 100 Mexico|MEX]]<br>
! scope="col" style="width:2.5em;font-size:90%;"|[[Billboard Top Latin Albums|US<br />Latin]]<br>
! scope="col" style="width:2.5em;font-size:90%;"|[[Argentine Chamber of Phonograms and Videograms Producers|ARG]]<br>
|-
! scope="row" | ''La Oreja de Van Gogh - Gira 2003''
|
*Released: April 19, 2004
*Label: Epic
*Format: CD
| 14 || — || — || — 
|style="text-align:left;"|
*SPA: Gold<ref name="promusicae">{{cite web|url=http://www.promusicae.org/listassemanales/albumes/historial/TOP%20100%20ALBUMES%2004_29.pdf|title=TOP 100 ALBUMES 04_29|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
*MEX: Gold<ref>{{cite web|url=http://www.amprofon.com.mx/certificaciones.php?artista=La+Oreja+de+Van+Gogh&titulo=lo+que+te+cont%E9&disquera=&certificacion=todas&anio=todos&categoria=MUSIC+VIDEO&Submitted=Buscar&item=menuCert&contenido=buscar|title=Amprofon certificaciones|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
|-
! scope="row" | ''Cometas por el cielo en directo desde América''
|
*Released: September 18, 2012
*Label: Epic
*Format: CD/DVD, digital 
| 9 || — || — || — 
|style="text-align:left;"|
|-
! scope="row" | ''Primera Fila''
|
*Released: 2013
*Label: Epic
*Format: CD/DVD, digital 
| 6 || — || — || — 
|style="text-align:left;"|
|}

===Box sets===
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
|+ List of box sets
! scope="col" style="width:15em;"| Title
! scope="col" style="width:16em;"| Album details
|-
! scope="row" | ''LOVG 1996-2006''
|
*Released: April 4, 2006
*Label: Epic
*Format: CD
|-
|}

===Remix albums===
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
|+ List of remix albums
! scope="col" style="width:15em;"| Title
! scope="col" style="width:16em;"| Album details
|-
! scope="row" | ''Cometas por el Cielo (Remixes)''
|
*Released: June 5, 2012
*Label: Epic
*Format: Digital
|-
|}

==Extended plays==
{| class="wikitable plainrowheaders" style="text-align:center;" border="1"
|+ List of extended plays
! scope="col" style="width:15em;"| Title
! scope="col" style="width:16em;"| Details
|-
! scope="row" | ''París (French edition)''
|
*Released: May 10, 2004
*Label: Epic
*Format: CD
|-
|}

== Singles ==

=== As main artist ===
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
! scope="col" rowspan="2" style="width:20em;" | Title
! scope="col" rowspan="2" | Year
! scope="col" colspan="3" | Peak chart positions
! scope="col" rowspan="2" | Album
|-
! scope="col" style="width:2.5em;font-size:90%;"|[[Productores de Música de España|SPA]]<br><ref name=SpN>{{cite web|title=Spanish Charts|url=http://spanishcharts.com/search.asp?search=la+oreja+de+van+gogh&cat=s|accessdate=January 1, 2013}}</ref><ref>{{cite web|url=http://afyvecharts.blogspot.com/2007/08/1999-albumes-1-parte.html|title=PROMUSICAE Charts list 1987-2008|publisher=[[Productores de Música de España]]|accessdate=January 3, 2013}}</ref>
! scope="col" style="width:2.5em;font-size:90%;"|[[Top 100 Mexico|MEX]]<br><ref name=Mexcharts/>
! scope="col" style="width:2.5em;font-size:90%;"|[[Latin Pop Airplay]]<br><ref name=bb200>{{cite web|title=Billboard - La Oreja de Van Gogh Latin Pop Songs|url={{BillboardURLbyName|artist=la oreja de van gogh|chart=Latin Pop Songs C}}|publisher=[[Prometheus Global Media]]|accessdate=January 3, 2013}}</ref>
|-
! scope="row"|"El 28"
| rowspan="3"| 1998
| 4 || — || 31
| rowspan="8"| ''Dile al Sol''
|- 
! scope="row"|"Soñaré"
| 1 || 33 || 22
|- 
! scope="row"|"Cuéntame al Oído"
| 1 || — || 22
|-
! scope="row"|"Pesadilla"
| rowspan="5"| 1999
| 16 || — || —
|-
! scope="row"|"Dile al Sol"
| 19 || — || —
|-
! scope="row"|"Qué puedo pedir"
| 9 || — || —
|-
! scope="row"|"El Libro"
| 18 || — || —
|-
! scope="row"|"La estrella y la Luna"
| 34 || — || —
|-
! scope="row"|"Cuídate"
| rowspan="2"| 2000
| 1 || 1 || 27
| rowspan="8"| ''El Viaje de Copperpot''
|-
! scope="row"|"París"
| 1 || 1 || —
|-
! scope="row"|"La Playa"
| rowspan="5"| 2001
| 1 || 1 || 18
|-
! scope="row"|"Pop"
| 8 || — || —
|-
! scope="row"|"Soledad"
| 6 || — || —
|-
! scope="row"|"Mariposa"
| 13 || — || —
|-
! scope="row"|"Tu Pelo"
| — || — || —
|-
! scope="row"|"La chica del gorro azul"
| 2002
| 16 || — || —
|-
! scope="row"|"[[Puedes Contar Conmigo]]"
| rowspan="3"| 2003
| 1 || 1 || 3
| rowspan="8"| ''Lo que te conté mientras te hacías la dormida''
|-
! scope="row"|"20 de Enero"
| 1 || 2 || —
|-
! scope="row"|"Rosas"
| 1 || 1 || 4
|-
! scope="row"|"Deseos de cosas imposibles"
| rowspan="4"| 2004
| — || 9 || 10
|-
! scope="row"|"Vestido azul"
| — || — || —
|-
! scope="row"|"Geografía"
| — || 5 || —
|-
! scope="row"|"Historia de un Sueño"
| — || — || —
|-
! scope="row"|"Bonustrack/Nana Nara"
| 2005
| — || — || —
|-
! scope="row"|"[[Muñeca de Trapo]]"
| rowspan="4"| 2006
| 1 || 1 || 3
| rowspan="4"| ''Guapa/Más Guapa''
|-
! scope="row"|"[[Dulce Locura]]"
| 1 || 16 || 22
|-
! scope="row"|"Perdida"
| 17 || 36 || —
|-
! scope="row"|"En mi lado del sofá"
| 1 || 11 || —
|-
! scope="row"|"[[El Último Vals]]"
| rowspan="3"| 2008
| 1 || 8 || 17
| rowspan="4"| ''A las cinco en el Astoria''
|-
! scope="row"|"[[Inmortal (La Oreja De Van Gogh song)|Inmortal]]"
| 10 || — || 14
|-
! scope="row"|"Jueves"
| 14 || 15 || 34
|-
! scope="row"|"Europa VII"
| rowspan="2"| 2009
| 43 || — || —
|-
! scope="row"|"Cuéntame al oído"
| — || — || —
| rowspan="2"| ''Nuestra casa a la izquierda del tiempo''
|-
! scope="row"|"Puedes contar conmigo"
| 2010
| — || — || —
|-
! scope="row"|"La niña que llora en tus fiestas"
| rowspan="3"| 2011
| 8 || 1 || 1
| rowspan="2"| ''Cometas por el Cielo''
|-
! scope="row"|"Cometas por el Cielo"
| 20 || 1 || 1
|-
! scope="row"|"La luz que nace en ti"
| 43 || — || —
| {{N/A|Non-album single}}
|-
! scope="row"|"Día cero"
| rowspan="2"| 2012
| — || — || —
| ''Cometas por el Cielo''
|-
! scope="row"|"Otra vez me has sacado a bailar"
| 22 || — || —
| {{N/A|Non-album single}}
|-
! scope="row"|"El primer día del resto de mi vida"
| rowspan="2"| 2013
| 6 || — || —
| rowspan="2"| "Primera Fila"
|-
! scope="row"|"Maria"<small>(featuring [[Natalia Lafourcade]])</small>
| 26 || — || —
|-
! scope="row"|"Verano"
| 2016
| 5 || — || —
| ''[[El planeta imaginario]]''
|-
|}

=== As featured artist ===
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
! scope="col" rowspan="2" style="width:20em;" | Title
! scope="col" rowspan="2" | Year
! scope="col" colspan="3" | Peak chart positions
! scope="col" rowspan="2" style="width:10em;" | [[List of music recording certifications|Certifications]]
! scope="col" rowspan="2" | Album
|-
! scope="col" style="width:2.5em;font-size:90%;"|[[Productores de Música de España|SPA]]<br><ref name=SpN />
! scope="col" style="width:2.5em;font-size:90%;"|[[Top 100 Mexico|MEX]]<br><ref name=Mexcharts />
! scope="col" style="width:2.5em;font-size:90%;"|[[Latin Pop Airplay]]<br><ref name=bb200 />
|-
! scope="row"|"Ay Haiti" <small>(Various Artists featuring La Oreja de Van Gogh)</small>
| 2010
| 1 || — || — 
|
*SPA: Platinum<ref name=spaincert>{{cite web|url=http://www.promusicae.es/files/listastonos/historial/TOP%2050%20CANCIONES%2010_25.pdf |title=Promusicae SEMANA 25:del 05.07.2010 al 11.07.2010(In Spanish) |format=PDF |accessdate=2010-09-06}}</ref>
| [[Charity single]]
|- 
|}

==Videography==
{{refimprove|section|date=March 2017}}
===Video albums===
{| class="wikitable plainrowheaders" width=98%
|-
!scope="col" width=3%|Year
!scope="col" width=20%| Title
!scope="col" width=12%| Production details
!scope="col" width=45%| Notes
|-
|-
|1999
!scope="row"|''Dile al Sol'' 
|
*Released: 1999
*Label: Epic
*Format: VHS
|
*Contains the "Dile al Sol Tour" shot live from Spain.
|-
|2002
!scope="row"|''La Oreja de Van Gogh (Video)'' 
|
*Released: 2002
*Label: Epic
*Format: DVD, VHS
|
*First homonym DVD of the band. Contains 12 music videos, 3 live performances and special features from the group at that point.
|-
|2003
!scope="row"|''La Oreja de Van Gogh en Directo Gira 2003'' 
|
*Released: 2003
*Label: Epic
*Format: DVD
|
*Contains the "Tour Lo que te conté mientras te hacías la dormida" shot live from several Spanish cities such as [[San Sebastián]], [[Burgos]], [[Valencia]], [[Sevilla]] and [[Málaga]]. Was certified gold in Spain and México, and Platinum in Argentina. 
|-
|2010
!scope="row"|''Un viaje al Mar Muerto'' 
|
*Released: 2010
*Label: Epic
*Format: DVD
|
*Their first full-length feature film, which was filmed in Israel in collaboration with other artists. Included as a bonus disc in the special edition of Nuestra casa a la izquierda del tiempo.
|-
|2012
!scope="row"|''Cometas por el cielo en directo desde Argentina'' 
|
*Released: 2012
*Label: Epic
*Format: DVD
|
*Contains the "Tour Cometas por el cielo" shot live from Buenos Aires, Argentina.
|-
|}

==See also==
*[[List of best-selling albums in Spain]]

==References==
{{reflist|30em}}

==External links==
*[http://www.laorejadevangogh.com/ La Oreja de Van Gogh official website]

{{La Oreja De Van Gogh}}

[[Category:2003 singles]]
[[Category:Discographies of Spanish artists]]
[[Category:Songs written by Amaia Montero]]