{{Use mdy dates|date=December 2016}}
{{Infobox person
| image       = Ota Benga 1904.jpg
| image_size  = 250px
| caption     = Benga at the [[Louisiana Purchase Exposition|St. Louis World's Fair]], 1904
| birth_name  = 
| birth_date  = {{circa|1883}}
| birth_place = {{flagicon image|Flag of Congo Free State.svg}} [[Colonization of the Congo|Colonial Congo]]
| death_date  = {{Death date and given age|1916|03|20|32}}
| death_place = {{flagicon image|US flag 48 stars.svg}} [[Lynchburg, Virginia|Lynchburg]], [[Virginia]], [[United States|U.S.]]
| death_cause = [[Suicide]]
| resting_place = White Rock Cemetery
| resting_place_coordinates = {{Coord|37|23|56.23|N|79|7|58.41|W}}
| residence =
| ethnicity = [[Democratic Republic of the Congo|Congolese]] ''[[Mbuti people|Mbuti]]'' [[Pygmy peoples|pygmy]]
| nationality =
| other_names =
| education =
| employer =
| occupation =
| title =
| salary =
| networth =
| height = {{convert|4|ft|11|in|cm}}
| weight = {{convert|103|lb|kg}}
| term =
| predecessor =
| successor =
| party =
| boards =
| religion =
| spouse = a wife (murdered)
| partner =
| parents =
| children = 2 (murdered)
| relatives =
| signature =
| website =
| footnotes =
}}
'''Ota Benga''' ({{circa|1883}}<ref>Bradford and Blume (1992), p. 54.</ref>&nbsp;– March 20, 1916) was a [[Democratic Republic of the Congo|Congolese]] man, a ''[[Mbuti people|Mbuti]]'' [[Pygmy peoples|pygmy]] known for being featured in an anthropology exhibit at the [[Louisiana Purchase Exposition]] in [[St. Louis, Missouri]] in 1904, and in a [[human zoo]] exhibit in 1906 at the [[Bronx Zoo]]. Benga had been purchased from [[African slave trade]]rs by the explorer Samuel Phillips Verner, a businessman hunting Africans for the Exposition.<ref>{{cite news|title=Caged in the human zoo: The shocking story of the young pygmy warrior put on show in a monkey house – and how he fuelled Hitler's twisted beliefs|url=http://www.dailymail.co.uk/news/article-1224189/Caged-human-zoo-The-shocking-story-young-pygmy-warrior-monkey-house--fuelled-Hitlers-twisted-beliefs.html|accessdate=May 23, 2014|newspaper=Dailymail|date=October 31, 2009}}</ref> He traveled with Verner to the United States. At the Bronx Zoo, Benga had free run of the grounds before and after he was exhibited in the zoo's Monkey House. Except for a brief visit with Verner to Africa after the close of the St. Louis Fair, Benga lived in the United States, mostly in [[Virginia]], for the rest of his life.

Displays of non-white humans as examples of "earlier stages" of [[human evolution]] were common in the early 20th century, when [[scientific racism|racial theories]] were frequently intertwined with concepts from evolutionary biology. [[African-American newspapers]] around the nation published editorials strongly opposing Benga's treatment. Dr. [[R. S. MacArthur]], the spokesperson for a delegation of black churches, petitioned the New York City mayor for his release from the Bronx Zoo. 

The mayor released Benga to the custody of Reverend James M. Gordon, who supervised the Howard Colored Orphan Asylum in Brooklyn and made him a ward. That same year Gordon arranged for Benga to be cared for in Virginia, where he paid for him to acquire American clothes and to have his teeth [[Dental restoration|capped]], so the young man could be more readily accepted in local society. Benga was tutored in English and began to work. Several years later, the outbreak of [[World War I]] stopped ship passenger travel and prevented his returning to Africa. This, as well as the inhumane treatment he was subjected to for most of his life, caused Benga to fall into a [[clinical depression|depression]]. He committed [[suicide]] in 1916 at the age of 32.<ref>{{cite book|last=Evanzz|first=Karl|title=The Messenger: The rise and fall of Elijah Muhammad|year=1999|publisher=Pantheon Books|location=New York|isbn=9780679442608}}</ref>

==Life==

===Early life===
As a member of the [[Mbuti]] people,<ref>Bradford and Blume describe Benga as Mbuti and write, "A feature article described Ota Benga as 'a dwarfy, black specimen of sad-eyed humanity.' He was sad because the other pygmies were Batwa but he was not..." (p. 116). They later mention that he "never fully assimilated into the Batwa" during his time with them. Parezo and Fowler refer to "[t]he Mbuti (Batwa) Pygmies and 'Red Africans'" and note that "McGee called them all Batwa Pygmies, 'real aboriginals of the Dark Continent'&nbsp;... [Benga] was slightly taller than the other Pygmies, a characteristic common to his society, the Badinga or Chiri-chiri. Verner considered the Chiri-chiris a Pygmy society, and McGee and the press decided not to quibble over details." (pp. 200–203). Many sources, e.g. Adams (p. 25) and [[NPR]], simply describe him as "a Batwa Pygmy from Africa".</ref> Ota Benga lived in equatorial forests near the [[Kasai River]] in what was then the [[Belgian Congo]]. His people were killed by the ''[[Force Publique]],'' established by [[Leopold II of Belgium|King Leopold II of Belgium]] as a militia to control the natives for labor in order to exploit the large supply of [[rubber]] in the Congo.  Benga lost his wife and two children, surviving only because he was on a hunting expedition when the Force Publique attacked his village. He was later captured by slave traders.<ref name="nyt2006">
{{cite news
 |url=https://www.nytimes.com/2006/08/06/nyregion/thecity/06zoo.html
 |title=The Scandal at the Zoo
 |date=August 6, 2006
 |work=[[The New York Times]]
 |last = Keller | first=Mitch
}}</ref>

The American businessman and explorer Samuel Phillips Verner travelled to Africa in 1904 under contract from the [[Louisiana Purchase Exposition]] (St. Louis World Fair) to bring back an assortment of pygmies to be part of an exhibition.<ref>Bradford and Blume (1992), pp. 97–98.</ref> To demonstrate the fledgling discipline of [[anthropology]], the noted scientist [[W. J. McGee]] intended to display "representatives of all the world's peoples, ranging from smallest pygmies to the most gigantic peoples, from the darkest blacks to the dominant whites" to show what was commonly thought then to be a sort of [[Scientific racism|cultural evolution]].<ref>Quoted in Bradford and Blume (1992), p. 5.</ref> Verner discovered Ota Benga while 'en route' to a [[Twa peoples|Batwa]] village visited previously; he negotiated Benga's release from the slave traders for a pound of salt and a bolt of cloth.<ref>Bradford and Blume (1992), pp. 102–103.</ref> Verner later claimed Benga was rescued from the cannibals by him.<ref>{{Cite web|url=https://www.rt.com/news/336335-ota-benga-caged-pygmy/|title=100 years ago today, Ota Benga ended his horrible life after caged as ‘pygmy’ at Bronx Zoo|website=RT International|language=en-EN|access-date=2016-03-22}}</ref> The two spent several weeks together before reaching the village. There the villagers had developed distrust for the ''muzungu'' (white man) due to the abuses of King Leopold's forces. Verner was unable to recruit any villagers to join him until Benga spoke of the ''muzungu'' saving his life, the bond that had grown between them, and his own curiosity about the world Verner came from. Four Batwa, all male, ultimately accompanied them. Verner recruited other Africans who were not pygmies: five men from the [[Kuba Kingdom|Bakuba]], including the son of King Ndombe, ruler of the Bakuba, and other related peoples&nbsp;– "Red Africans" as they were collectively labeled by contemporary anthropologists.<ref name="parezo204">Parezo and Fowler (2007), p. 204.</ref><ref>Bradford and Blume (1992), pp. 109–110.</ref>

===As exhibit===

====St. Louis World Fair====
[[File:Louisiana Purchase pygmies.jpg|thumb|300px|Benga (second from left) and the Batwa in St. Louis]]
The group arrived in [[St. Louis, Missouri]] in late June 1904 without Verner, who had been taken ill with [[malaria]]. The Louisiana Purchase Exposition had already begun, and the Africans immediately became the center of attention. Ota Benga was particularly popular, and his name was reported variously by the press as ''Artiba'', ''Autobank'',<ref name="Bradford Geronimo">Bradford and Blume (1992), pp. 12–16.</ref> ''Ota Bang'', and ''Otabenga''. He had an amiable personality, and visitors were eager to see his teeth, which had been [[human tooth sharpening|filed to sharp points]] in his early youth as ritual decoration. The Africans learned to charge for photographs and performances. One newspaper account, promoting Ota Benga as "the only genuine African [[cannibal]] in America", claimed "[his teeth were] worth the five cents he charges for showing them to visitors".<ref name="parezo204" />

When Verner arrived a month later, he realized the pygmies were more prisoners than performers. Their attempts to congregate peacefully in the forest on Sundays were thwarted by the crowds' fascination with them. McGee's attempts to present a "serious" scientific exhibit were also overturned. On July 28, the Africans' performing to the crowd's preconceived notion that they were "savages" resulted in the First Illinois Regiment being called in to control the mob. Benga and the other Africans eventually performed in a warlike fashion, imitating [[Native Americans in the United States|American Indians]] they saw at the Exhibition.<ref name="Verner arrive">Bradford and Blume (1992), pp. 118–121</ref> The [[Apache]] chief [[Geronimo]] (featured as "The Human Tyger"&nbsp;– with special dispensation from the [[United States Department of War|Department of War]])<ref name="Bradford Geronimo" /> grew to admire Benga, and gave him one of his [[arrowhead]]s. For his efforts, Verner was awarded the gold medal in anthropology at the close of the Exposition.<ref name="Verner arrive" />

====American Museum of Natural History====
Benga accompanied Verner when he returned the other Africans to the Congo. He briefly lived amongst the Batwa while continuing to accompany Verner on his African adventures. He married a Batwa woman who later died of snakebite, and little is known of this second marriage. Not feeling that he belonged with the Batwa, Benga chose to return with Verner to the United States.<ref>Bradford and Blume (1992), pp. 151–158.</ref>

Verner eventually arranged for Benga to stay in a spare room at the [[American Museum of Natural History]] in New York City while he was tending to other business.  Verner negotiated with the curator Henry Bumpus over the presentation of his acquisitions from Africa and potential employment. While Bumpus was put off by Verner's request of the prohibitively high salary of $175 a month and was not impressed with the man's credentials, he was interested in Benga. Wearing a Southern-style linen suit to entertain visitors, Benga initially enjoyed his time at the museum. He became homesick, however.<ref name="museum">Bradford and Blume (1992), pp. 159–168.</ref>

The writers Bradford and Blume imagined his feelings:
<blockquote>What at first held his attention now made him want to flee. It was maddening to be inside&nbsp;– to be swallowed whole&nbsp;– so long. He had an image of himself, stuffed, behind glass, but somehow still alive, crouching over a fake campfire, feeding meat to a lifeless child. Museum silence became a source of torment, a kind of noise; he needed birdsong, breezes, trees.<ref>Bradford and Blume (1992), pp. 165–166.</ref></blockquote>

The disaffected Benga attempted to find relief by exploiting his employers' presentation of him as a 'savage'. He tried to slip past the guards as a large crowd was leaving the premises; when asked on one occasion to seat a wealthy donor's wife, he pretended to misunderstand, instead hurling the chair across the room, just missing the woman's head. Meanwhile, Verner was struggling financially and had made little progress in his negotiations with the museum. He soon found another home for Benga.<ref name ="museum" />

====Bronx Zoo====
At the suggestion of Bumpus, Verner took Benga to the [[Bronx Zoo]] in 1906. There the Mbuti man was allowed to roam the grounds freely. He became fond of an [[orangutan]] named Dohong, "the presiding genius of the Monkey House", who had been taught to perform tricks and imitate human behavior.<ref>Bradford and Blume (1992), pp. 172–174.</ref> The events leading to his "exhibition" alongside Dohong were gradual:<ref name="nyt2006"/> Benga spent some of his time in the Monkey House exhibit, and the zoo encouraged him to hang his [[hammock]] there, and to shoot his bow and arrow at a target. On the first day of the exhibit, September 8, 1906, visitors found Benga in the Monkey House.<ref name="nyt2006"/> Soon, a sign on the exhibit read:

[[File:Ota Benga at Bronx Zoo.jpg|thumb|300px|Ota Benga at the Bronx Zoo in 1906. Only five promotional photos exist of Benga's time here, none of them in the "Monkey House"; cameras were not allowed.<ref>Bradford and Blume (1992), photo insert.</ref>]]

<blockquote>The African Pygmy, "Ota Benga."<br />
Age, 23 years. Height, 4 feet 11 inches.<br />
Weight, 103 pounds. Brought from the<br />
Kasai River, Congo Free State, South Cen-<br />
tral Africa, by Dr. Samuel P. Verner. Ex-<br />
hibited each afternoon during September.<ref name="nyt1906">"Man and Monkey Show Disapproved by Clergy," ''[[The New York Times]]'', September 10, 1906, pg. 1.</ref>
</blockquote>

[[William Temple Hornaday|William Hornaday]], the Bronx Zoo director, considered the exhibit a valuable spectacle for visitors; he was supported by [[Madison Grant]], Secretary of the [[New York Zoological Society]], who lobbied to put Ota Benga on display alongside apes at the Bronx Zoo.  A decade later, Grant became prominent nationally as a [[scientific racism|racial anthropologist]] and [[eugenics|eugenicist]].<ref>Bradford and Blume (1992), pp. 173–175.</ref>

[[African-American]] clergymen immediately protested to zoo officials about the exhibit. Said James H. Gordon, 
<blockquote>"Our race, we think, is depressed enough, without exhibiting one of us with the apes ... We think we are worthy of being considered human beings, with souls."<ref name="nyt2006"/>  Gordon thought the exhibit was hostile to Christianity and a promotion of [[Darwinism]]: "The Darwinian theory is absolutely opposed to Christianity, and a public demonstration in its favor should not be permitted."<ref name="nyt2006"/></blockquote>A number of clergymen backed Gordon.<ref name="Spiro 47">Spiro (2008), p. 47.</ref>

In defense of the depiction of Benga as a lesser human, an editorial in  ''[[The New York Times]]'' suggested:
<blockquote>We do not quite understand all the emotion which others are expressing in the matter ... It is absurd to make moan over the imagined humiliation and degradation Benga is suffering. The pygmies ... are very low in the human scale, and the suggestion that Benga should be in a school instead of a cage ignores the high probability that school would be a place ... from which he could draw no advantage whatever. The idea that men are all much alike except as they have had or lacked opportunities for getting an education out of books is now far out of date.<ref name="Spiro 48">Spiro (2008), p. 48.</ref></blockquote>

After the controversy, Benga was allowed to roam the grounds of the zoo. In response to the situation, as well as verbal and physical prods from the crowds, he became more mischievous and somewhat violent.<ref>Smith (1998). See chapter on Ota Benga.</ref> Around this time, an article in ''The New York Times'' stated, "It is too bad that there is not some society like the Society for the Prevention of Cruelty to Children. We send our missionaries to Africa to Christianize the people, and then we bring one here to brutalize him."<ref name="nyt1906" />

The zoo finally removed Benga from the grounds. Verner was unsuccessful in his continued search for employment, but he occasionally spoke to Benga. The two had agreed that it was in Benga's best interests to remain in the United States despite the unwelcome spotlight at the zoo.<ref>Bradford and Blume (1992), pp. 187–190.</ref> Toward the end of 1906, Benga was released into Reverend Gordon's custody.<ref name="nyt2006" />

===Later life===

[[File:Reverend James Gordon.jpeg|thumb|right|Rev. James Gordon]]
Gordon placed Benga in the Howard Colored Orphan Asylum, a church-sponsored [[orphanage]] which he supervised. As the unwelcome press attention continued, in January 1910, Gordon arranged for Benga's relocation to [[Lynchburg, Virginia|Lynchburg]], [[Virginia]], where he lived with the McCray family.<ref>Bradford and Blume (1992), pp. 191–204.</ref> So that Benga could more easily be part of local society, Gordon arranged for the African's teeth to be capped and bought him American-style clothes. Tutored by Lynchburg poet [[Anne Spencer]],<ref>Bradford and Blume (1992), pp. 212–213.</ref> Benga could improve his English, and he began to attend elementary school at the Baptist Seminary in Lynchburg.<ref name="Spiro 48" />

Once he felt his English had improved sufficiently, Benga discontinued his formal education. He began working at a Lynchburg [[tobacco]] factory. He proved a valuable employee because he could climb up the poles to get the tobacco leaves without having to use a ladder. His fellow workers called him "Bingo".  He often told his life story in exchange for sandwiches and root beer. He began to plan a return to Africa.<ref name="Spiro 49">Spiro (2008), p. 49.</ref>

In 1914 when [[World War I]] broke out, a return to the Congo became impossible as passenger ship traffic ended. Benga became depressed as his hopes for a return to his homeland faded.<ref name="Spiro 49" /> On March 20, 1916, at the age of 32, he built a ceremonial fire, chipped off the caps on his teeth, and shot himself in the heart with a stolen pistol.<ref>[http://www.encyclopediavirginia.org/Benga_Ota_ca_1883-1916 "Ota Benga"], ''Encyclopedia Virginia''</ref>

He was buried in an unmarked grave in the black section of the [[Old City Cemetery (Lynchburg, Virginia)|Old City Cemetery]], near his benefactor, Gregory Hayes. At some point, the remains of both men went missing.  Local oral history indicates that Hayes and Ota Benga were eventually moved from the Old Cemetery to White Rock Cemetery, a burial ground that later fell into disrepair.<ref>Bradford and Blume (1992), p. 231.</ref>

==Legacy==
Phillips Verner Bradford, the grandson of Samuel Phillips Verner, wrote a book on the Mbuti man, entitled ''Ota Benga: The Pygmy in the Zoo'' (1992). During his research for the book, Bradford visited the [[American Museum of Natural History]], which holds a life mask and body cast of Ota Benga. The display is still labeled "Pygmy", rather than indicating Benga's name, despite objections beginning a century ago from Verner and repeated by others.<ref>{{cite news|first=Darrel |last=Laurent |url=http://www.newsadvance.com/servlet/Satellite?pagename=LNA%2FMGArticle%2FLNA_BasicArticle&c=MGArticle&cid=1031782991730&path=!news!archive |title=Demeaned in Life, Forgotten in Death |publisher=The Lynchburg News & Advance |date=May 29, 2005 |accessdate=2006-04-03 }}{{dead link|date=April 2016|bot=medic}}{{cbignore|bot=medic}}</ref> Publication of Bradford's book in 1992 inspired widespread interest in Ota Benga's story and stimulated creation of many other works, both fictional and non-fiction, such as:

* 1994 – John Strand's play, ''Ota Benga'', was produced by the Signature Theater in [[Arlington, Virginia]].<ref>{{cite book|url=http://www.broadwayplaypubl.com/otabenga.htm |title=''Ota Benga''|publisher= Broadway Plays}}</ref>
* 1997 – The play, ''Ota Benga, Elegy for the Elephant,'' by Dr. Ben B. Halm, was staged at [[Fairfield University]] in Connecticut.<ref>{{cite web|url=http://www.fairfield.edu/pr_memdetails1.html|title=Memorial details&nbsp;— Ben Halm|publisher=Fairfield University|accessdate=2009-01-06 |archiveurl = https://web.archive.org/web/20071102145744/http://www.fairfield.edu/pr_memdetails1.html |archivedate = November 2, 2007}} {{Dead link|date=April 2009}}</ref>
* 2002 – The Mbuti man was the subject of the short documentary, ''Ota Benga: A Pygmy in America,'' directed by Brazilian [[Alfeu França]]. He incorporated original movies recorded by Verner in the early 20th century.<ref>{{cite video |title=Ota Benga:A Pygmy in America |people=Alfeu França |date=2002 |medium=film}}</ref>
* 2005 – A fictionalized account of his life portrayed in the film ''[[Man to Man (2005 film)|Man to Man]]'', starring Joseph Fiennes, Kristin Scott Thomas. 
* 2006 – The Brooklyn-based band [[Piñataland]] released a song titled "Ota Benga's Name" on their album ''Songs from the Forgotten Future Volume 1'', which tells the story of Ota Benga.<ref name="nyt2006" />
* 2006 – The fantasy film ''[[The Fall (2006 film)|The Fall]]'' features a highly fictionalized character based on Ota Benga.
* 2007 – McCray's early poems about Benga were adapted as a performance piece; the work debuted at the [[Columbia Museum of Art]] in 2007, with McCray as narrator and original music by Kevin Simmonds.
* 2008 – Benga inspired the character of Ngunda Oti in the film ''[[The Curious Case of Benjamin Button (film)|The Curious Case of Benjamin Button]]''.<ref>{{cite news |url=http://www.washingtonpost.com/wp-dyn/content/article/2009/01/02/AR2009010202444.html |title=Basest Instinct: Case of the Zoo Pygmy Exhibited a Familiar Face of Human Nature |last=Hornaday |first=Ann |date=January 3, 2009 |work=Washington Post |accessdate=2009-01-06}}</ref>
*2010 – The story of Ota Benga was the inspiration for a concept album by the [[St. Louis, Missouri|St. Louis]] musical ensemble May Day Orchestra <ref>{{cite book|title=Ota Benga|url=http://www.allmusic.com/album/ota-benga-mw0002066253|author=May Day|publisher=Allmusic.com}}</ref>
* 2011 – Italian band Mamuthones recorded the song "Ota Benga" in their album ''Mamuthones''.{{citation needed|date=September 2015}}
* 2012 – ''Ota Benga Under My Mother's Roof'', a poetry collection, was published by [[Carrie Allen McCray]], whose family had taken care of Benga
* 2012 – Ota Benga was mentioned in the song, "Behind My Painted Smile", by English rapper [[Akala (rapper)|Akala]]
* 2015 – Journalist Pamela Newkirk published the biography ''Spectacle: The Astonishing Life of Ota Benga''<ref>{{cite book|title=Spectacle: The Astonishing Life of Ota Benga|author=Newkirk, Pamela|date=June 2, 2015 |publisher=Amistad|isbn= 9780062201003}}</ref>
* 2016 – Radio Diaries, a Peabody Award-winning radio show, tells the story of Ota Benga in "The Man in the Zoo" on the Radio Diaries podcast. <ref>{{cite |title=The Man in the Zoo|url=http://www.radiodiaries.org/the-man-in-the-zoo/|author=Radio Diaries}}</ref>

==Similar case==
[[File:Ishi 1914.jpg|thumb|right|250px|Like Ota Benga, [[Ishi]] (a Native American) was displayed in the human zoo.]]
Similarities have been observed between the treatment of Ota Benga and [[Ishi]]. The latter was the sole remaining member of the [[Yana people|Yahi]] [[Native Americans in the United States|Native American]] tribe, and he was displayed in [[California]] around the same period. Ishi died on March 4th,1916, five days after Ota.<ref>Weaver (2003), p. 41.</ref><ref>{{cite book|url=https://books.google.com/books?id=YFghGGXLJ7IC&pg=PA41&lpg=PA41|page=41|title=Ishi in Three Centuries|authors=Kroeber, Karl & Kroeber, Clifton B. (editors)|date=2003|location=Lincoln|publisher= University of Nebraska Press}}</ref>

==See also==
* [[Human zoo]]
* [[Saartjie Baartman]], called the "Hottentot Venus"

==References==
{{reflist}}

==Bibliography==
* {{cite book
 | last = Adams
 | first = Rachel
 | year = 2001
 | title = Sideshow U.S.A: Freaks and the American Cultural Imagination
 | publisher = University of Chicago Press
 | location = Chicago
 | isbn = 0-226-00539-9
}}
* {{cite book
 | last1 = Bradford
 | first1 = Phillips Verner
 | last2= Blume
 | first2= Harvey 
 | year = 1992
 | title = Ota Benga: The Pygmy in the Zoo
 | publisher = St. Martins Press
 | location = New York
 | isbn = 0-312-08276-2
}}
* {{cite book
 | last = McCray
 | first = Carrie Allen
 | title = Ota Benga under My Mother's Roof
 | editor = Kevin Simmonds
 | publisher = University of South Carolina Press
 | location = Columbia
 | year = 2012
 | isbn = 978-1-61117-085-6
}}
* {{cite book
 | last = Newkirk
 | first = Pamela
 | year = 2015
 | title = Spectacle: The Astonishing Life of Ota Benga
 | publisher = Amistad
 | location = New York
 | isbn = 978-0-06-220100-3
}}
* {{cite book
 | last1 = Parezo
 | first1 = Nancy J.
 | last2= Fowler
 | first2 = Don D. 
 | year = 2007
 | title = Anthropology Goes to the Fair: The 1904 Louisiana Purchase Exposition
 | publisher = University of Nebraska Press
 | location = Lincoln, Nebraska
 | isbn = 0-8032-3759-6
}}
* {{cite book
 | last = Smith
 | first = Ken
 | year = 1998
 | title = Raw Deal: Horrible and Ironic Stories of Forgotten Americans
 | publisher = [[Blast Books]]
 | location = New York
 | isbn = 0-922233-20-9
}}
* {{cite book
 | last = Spiro
 | first = Jonathan Peter
 | title = Defending the Master Race: Conservation, Eugenics, and the Legacy of Madison Grant
 | publisher = University of Vermont Press
 | location = Burlington, VT
 | year = 2008
 | pages = 43–51
 | isbn = 978-1-58465-715-6
}}
* {{cite book
 | last = Weaver
 | first = Jace
 | title = Ishi in Three Centuries
 | chapter = When the Demons Came: (Retro)Spectacle among the Savages
 | editor = Karl Kroeber; Clifton B. Kroeber
 | publisher = University of Nebraska Press
 | location = Lincoln
 | year = 2003
 | pages = 35–47
 | isbn = 0-8032-2757-4
}}

==External links==
{{commons}}
* [http://www.npr.org/templates/story/story.php?storyId=5787947 "From the Belgian Congo to the Bronx Zoo"], September 8, 2006, [[National Public Radio]]
* [http://www.cdbaby.com/cd/maydayorchestra1 ''Ota Benga Folk Opera''], 2010 recording of a suite of songs based on the life of Ota Benga and the history of Congo
* [http://www.straightdope.com/columns/040220.html Cecil Adams answer to "Are Pygmies really human?"], ''[[The Straight Dope]],'' February 20, 2004, discusses history of Ota Benga
* [http://www.otabenga.org Ota Benga Alliance for Peace, Healing and Dignity], Official Website
* {{Find a Grave|20167712}}

{{Authority control}}

{{DEFAULTSORT:Benga, Ota}}
[[Category:1880s births]]
[[Category:1916 deaths]]
[[Category:Anthropology]]
[[Category:African Pygmies]]
[[Category:Suicides by firearm in Virginia]]
[[Category:American slaves]]
[[Category:American people of Democratic Republic of the Congo descent]]
[[Category:Male suicides]]