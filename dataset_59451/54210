{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Findon
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 5,718
| pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref name=ABS2011>{{Census 2011 AUS |id =SSC40200 |name=Findon (State Suburb) |accessdate=5 October 2016| quick=on}}</ref>
| postcode            = 5023<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/findon |title=Findon, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=5 June 2011}}</ref>
| area                = 
| dist1               = 6.5
| dir1                = NW
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = [[City of Charles Sturt]]<ref name=CharlesSturt>{{cite web|url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |title=City of Charles Sturt Wards and Council Members |author= |date= |work= |publisher=City of Charles Sturt |accessdate=5 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110805135335/http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |archivedate=5 August 2011 |df=dmy }}</ref>
| stategov            = [[Electoral district of Cheltenham|Cheltenham]] ''(2014)''</small><ref name=ECSA>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=5 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov              = [[Division of Port Adelaide|Port Adelaide]]
| near-n              = [[Woodville West, South Australia|Woodville West]]
| near-ne             = [[Woodville South, South Australia|Woodville South]]
| near-e              = [[Beverley, South Australia|Beverley]]
| near-se             = [[Flinders Park, South Australia|Flinders Park]]
| near-s              = [[Kidman Park, South Australia|Kidman Park]]
| near-sw             = [[Kidman Park, South Australia|Kidman Park]]
| near-w              = [[Seaton, South Australia|Seaton]]
| near-nw             = [[Seaton, South Australia|Seaton]]
| est                 = 1848<ref name=Manning>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/f/f2.htm#findon |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=5 June 2011}}</ref>
}}

'''Findon''' is a western [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Charles Sturt]].

==History==
In 1839, George Cortis was granted the land now constituting Findon, which he subdivided in 1848. The suburb may have been named for the town of [[Findon, West Sussex|Findon]], [[United Kingdom]], near Cortis' hometown of [[Worthing]].<ref name=Manning/>

==Geography==
Findon lies astride Crittenden and [[Findon Road, Adelaide|Findon]] roads, in Adelaide's western suburbs. [[Grange Road, Adelaide|Grange Road]] forms its southern boundary.<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The 2011 Census by the [[Australian Bureau of Statistics]] counted 5,718 persons in Findon on census night. Of these, 49% were male and 51% were female.<ref name=ABS2011/>

The majority of residents (60.9%) are of Australian birth, with other common census responses being [[Italy]] (9.6%) and [[England]] (2.7%).<ref name=ABS2006>{{Census 2006 AUS |id =SSC41506 |name=Findon (State Suburb) |accessdate=5 June 2011| quick=on}}</ref>

The age distribution of Findon residents is skewed higher than the greater Australian population. 72.1% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 27.9% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Politics==

===Local government===
Findon is part of Findon and Beverley wards in the [[City of Charles Sturt]] [[Local government in Australia|local government area]], being represented in that council by Doriana Coppola and Joe Ienco (Findon), and by Edgar Agius and Mick Harley (Beverley).<ref name=CharlesSturt/>

===State and federal===
Findon lies in the state [[Electoral districts of South Australia|electoral district]] of [[Electoral district of Cheltenham|Cheltenham]]<ref name=ECSA/> and the federal [[Divisions of the Australian House of Representatives|electoral division]] of [[Division of Port Adelaide|Port Adelaide]].<ref name=AEC>{{cite web|url=http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Port+Adelaide&filterby=Electorate&divid=189 |title=Find my electorate: Port Adelaide |author= |date= |work= |publisher=Australian Electoral Commission |accessdate=5 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110727073801/http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Port+Adelaide&filterby=Electorate&divid=189 |archivedate=27 July 2011 |df=dmy }}</ref> The suburb is represented in the [[South Australian House of Assembly]] by [[Jay Weatherill]]<ref name=ECSA/> and [[Australian House of Representatives|federally]] by [[Mark Butler]].<ref name=AEC/>

==Community==

===Community groups===
Findon Community Centre is located on Findon Road.<ref>{{cite web |url=http://www.charlessturt.sa.gov.au/site/page.cfm?u=244 |title=Findon Community Centre Inc Events & Activities |author= |date= |work= |publisher=City of Charles Sturt |accessdate=5 June 2011}}</ref>

===Schools===
Findon High School is on Drummond Avenue.<ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=5 June 2011}}</ref>  and Nazareth Catholic College Primary and Early Childhood Centre are on Crittenden Road.<ref name=UBD/>

==Facilities and attractions==

===Shopping and dining===
Shops are located on Findon Road and Grange Road.

===Parks===
'''Findon Oval''', home of the ''Woodville District Baseball Club'' in summer months, and the ''Woodville Lacrosse Club'' in winter months, is located on Drummond Avenue,<ref name="UBD" /> beside '''Findon Cycle Speedway''', home of the ''Findon Skid Kids'' cycling club.<ref>{{cite web |url=http://www.cyclespeedway.asn.au |title=Findon Cycle Speedway |author= |date= |work= |publisher= |accessdate=5 June 2011}}</ref>

The other main greenspace in the suburb is '''Matheson Reserve''', between Buccleuch Avenue and Dominion Avenue.

Other parks and reserves are located on Findon Road, Strathbogie Avenue, Pamela Street, Rondo Avenue and Dampier Avenue.

'''Basa Reserve''', mainly located in neighbouring Beverley, extends into the suburb.<ref name=UBD/>

===Basketball===
Findon is home to the 8,000 seat [[Adelaide Arena]], home of the four time [[National Basketball League (Australasia)|National Basketball League]] champions the [[Adelaide 36ers]] and home of five time [[Women's National Basketball League]] champions the [[Adelaide Lightning]].

The [[Australian dollar|A$]]16 million arena, the largest purpose built [[basketball]] stadium in Australia, opened in 1992 as the '''Clipsal Powerhouse''' before a change of sponsorship saw a name change to the '''Distinctive Homes Dome''' in 2002. This lasted until 2009 when it became known as '''The Dome''', and in 2010 the venue was renamed the Adelaide Arena. The main basketball court at the arena is known as the '''Brett Maher Court''' in honour of 36ers games record holder and three-time championship winning captain [[Brett Maher]].

==Transportation==

===Roads===
Findon is primarily serviced by [[Findon Road, Adelaide|Findon Road]] and Crittenden Road, both of which pass through the suburb. [[Grange Road, Adelaide|Grange Road]] links Findon to [[Adelaide city centre]] and the coast.<ref name=UBD/>

===Public transport===
Findon is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date= |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=5 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

====Buses====
The suburb is serviced by buses run by the [[Adelaide Metro]].<ref name=Metro/>

==See also==
{{Commons category|Findon, South Australia}}
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.charlessturt.sa.gov.au |title=City of Charles Sturt |author= |date= |work=Official website |publisher=City of Charles Sturt |accessdate=5 June 2011}}

{{Coord|-34.906|138.533|format=dms|type:city_region:AU-SA|display=title}}
{{City of Charles Sturt suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1848]]