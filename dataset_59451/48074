{{Infobox Wrestling event
|name       = Gary Albright Memorial Show
|image      = Gary Albright Memorial Show poster.jpg
|caption    = Original event poster
|alt        =
|tagline    =
|theme      =
|promotion  = [[World Xtreme Wrestling]]
|brand      =
|date       = April 19, 2000
|date_aired =
|attendance =
|venue      = Agricultural Hall
|city       = [[Allentown, Pennsylvania]]
|liveevent  = Y
|lastevent  = N/A
|nextevent  = [[Yokozuna Memorial Show]]
}}
The '''Gary Albright Benefit Memorial Show''' was a [[professional wrestling]] event produced by [[World Xtreme Wrestling]] (WXW) [[professional wrestling promotion|promotion]] which took place on April 19, 2000, at the Agricultural Hall in [[Allentown, Pennsylvania]]. It was held in memory of wrestler [[Gary Albright]], the son-in-law of promoter [[Afa_Anoa%CA%BBi|Afa Anoa'i]] and a mainstay of [[All Japan Pro Wrestling]], who suffered a fatal heart attack at a WXW show in [[Hazleton, Pennsylvania]] three months earlier. Twelve professional wrestling matches were featured on the event's card, with two including championships.<ref name="RF Video">{{cite video | people=[[World Xtreme Wrestling]] (Producer) | date=2000-04-19 | url=http://www.rfvideo.com/browseproducts/WXW-Gary-Albright-Benefit-Memorial-Show.HTML | title=WXW Gary Albright Benefit Memorial Show | medium=DVD | location=[[Allentown, Pennsylvania]] | publisher=[[RF Video]]}}</ref><ref name="Cawthon">{{cite web |url=http://www.thehistoryofwwe.com/00.htm |title=2000|author=Cawthon, Graham|work=Graham Cawthon's History of the WWE |publisher=TheHistoryofWWE.com}} {{verify credibility|failed=y|date=September 2013}}</ref><ref name="PWInsider.com">{{cite web |url=http://www.pwinsider.com/ViewArticle.php?id=23970&p=1 |title=This Day In History: Crockett Cup, Spring Stampede, Eugene Debuts And More |accessdate=20 February 2011 |author=Woodward, Buck |date=April 19, 2007 |publisher=PWInsider.com }}</ref>

The show featured wrestlers from both All-Japan Pro Wrestling and the [[World Wrestling Federation]], as well as members of the [[Anoa'i family]] including [[Afa_Anoa%CA%BBi_Jr.|Afa, Jr.]] and [[Lloyd_Anoa%CA%BBi|Lloyd Anoa'i]], [[Samula_Anoa%CA%BBi|Samula Anoa'i]] and [[Jimmy Snuka]]. [[Dwayne Johnson|Dwayne "The Rock" Johnson]], another relative, made a special afternoon appearance at the [[Wild Samoans#Post-tag team careers|Wild Samoan Training Center]] to make a pre-recorded message aired at the start of the event; this was followed by a live appearance by [[WWF Hall of Fame]]r [[Bob Backlund]] later on in the show.<ref name="AfaJr.com">{{cite web |url=http://afajr.tripod.com/ |title=The Memorable Gary Albright Memorial Show |accessdate=20 February 2011 |author=Kost, Frank J. |year=2000 |publisher=AfaJr.com }}</ref> Although a number of major stars were scheduled to be on the card, including [[Gangrel (wrestler)|Gangrel]] and [[Luna Vachon]], [[Mick Foley|Cactus Jack]], [[Sabu (wrestler)|Sabu]] and [["Dr. Death" Steve Williams]], many were unable to attend at the last minute. Others, most notably [[Rob Van Dam]], were on hand for autograph signings.<ref>{{cite news |title=Ring of death |author=Maher, John |url= |newspaper=[[Austin American-Statesman]] |date=August 6, 2000}}</ref>

The [[Card (sports)#Main event|main event]] was a [[Professional wrestling match types#Variations of singles matches|standard wrestling match]] between WWF wrestlers, [[Rikishi (wrestler)|Rikishi Phatu]], and the challenger, [[Brian Gerard James|The Road Dogg]], which was followed by an impromptu four-way match with [[2 Cold Scorpio|Too Cold Scorpio]] and [[Chris Jericho]].<ref>{{cite web |url=http://doggystylin.tripod.com/index2.html |title=04/20/2000 - Hey, its the Road Dogg's day today! 4:20, bay-bee! |accessdate=20 February 2011 |author= |date=April 20, 2000 |work=News & Rumors |publisher=DoggyStylin.com }}</ref> Another featured match was Chris Jericho versus [[WWF European Championship|WWF European Champion]] [[Eddie Guerrero]], in a non-title match which Jericho won. [[The Headbangers]] ([[Mosh (wrestler)|Mosh]] and [[Thrasher (wrestler)|Thrasher]]) beat [[Kai En Tai]] ([[Shoichi Funaki|Sho Funaki]] and [[Taka Michinoku]]), and [[Taiyō Kea|Maunakea Mossman]], who was accompanied by [[Nicole Bass]], defeated [[John Hindley|Johnny Smith]].<ref name="AfaJr.com"/> This was the first-ever match that Mossman had wrestled in the United States.<ref name="Horie">{{cite web |url=http://www.geocities.com/masanorihorie2000/masa012201.htm |title=View From The Rising Sun: Taiyo Kea |accessdate=20 February 2011 |author=Horie, Masanori |date=January 22, 2001 |work=View From The Rising Sun |publisher=Rob Moore, Texas Wrestling Announcer |archiveurl=https://web.archive.org/web/20080321090734/http://www.geocities.com/masanorihorie2000/masa012201.htm |archivedate=March 21, 2008}}</ref>

==Show results==
'''April 19, 2000 in [[Allentown, Pennsylvania]] (Agricultural Hall)'''
{{Pro Wrestling results table
|results=
|times='<ref name="RF Video"/><ref name="Cawthon"/><ref name="PWInsider.com"/><ref name="AfaJr.com"/><ref>{{cite web |url=http://www.angelfire.com/wrestling/CrimsonIdol/pasttalk.html |title=Lucifer's Past Commentary |accessdate=20 February 2011 |author=Grimm, Lucifer |date=May 16, 2000 |publisher=Official Website of "The Crimson Idol" Lucifer Grimm }}</ref>

|match1=Sugaa and [[2 Cold Scorpio|Too Cold Scorpio]] (with [[Ana Rocha|Ariel]]) defeated The American Hunk Society (Mark The Body & Tommy Suede)
|stip1=[[Tag team match]]
|time1=&nbsp;

|match2=[[Jimmy Snuka|"Superfly" Jimmy Snuka]] defeated Jak Molsonn
|stip2=Singles match
|time2=&nbsp;

|match3=[[Doink the Clown]] defeated "Showtime" Shane Black
|stip3=Singles match
|time3=&nbsp;

|match4=[[The Headshrinkers]] ([[Lloyd_Anoa%CA%BBi|Alofa]] and [[Samula_Anoa%CA%BBi|Samu]]) defeated [[Big Dick Dudley]] and [[The Hungarian Barbarian]]
|stip4=Tag team match
|time4=&nbsp;

|match5=[[Scotty 2 Hotty]] {{small|(c)}} (with [[Brian Christopher|Grand Master Sexay]] and Tommy Fierro) defeated [[Stevie Richards]]
|stip5=Singles match for the [[WWF Light Heavyweight Championship]]
|time5=&nbsp;

|match6=[[Samula_Anoa%CA%BBi.|Afa, Jr.]] defeated [[Gillberg (wrestler)|Gillberg]] and Lucifer Grimm
|stip6=[[Professional wrestling match types#Basic elimination matches|Three-Way Dance]]
|time6=&nbsp;

|match7=[[Crowbar (wrestler)|Crowbar]] (managed by Little Jeanie) defeated Judas Young
|stip7=Singles match
|time7=&nbsp;

|match8=[[The Headbangers]] ([[Mosh (wrestler)|Mosh]] and [[Thrasher (wrestler)|Thrasher]]) defeated [[Kai En Tai]] ([[Shoichi Funaki|Sho Funaki]] and [[Taka Michinoku]]) (with The Prophet)
|stip8=Tag team match
|time8=&nbsp;

|match9=[[Taiyō Kea|Maunakea Mossman]] (with [[Nicole Bass]]) defeated [[John Hindley|Johnny Smith]]
|stip9=Singles match
|time9=&nbsp;

|match10=[[Chris Jericho]] defeated [[Eddie Guerrero]]
|stip10=Non-title singles match
|time10=&nbsp;

|match11=[[Rikishi (wrestler)|Rikishi Phatu]] defeated [[Brian Gerard James|The Road Dogg]]
|stip11=Singles match
|time11=&nbsp;

}}

==See also==
*[[List of professional wrestling memorial shows]]

==References==
{{Reflist}}

==External links==
*[http://www.cagematch.net/?id=1&nr=27425 Gary Albright Memorial Show at Cagematch.net]
*[http://WrestlingData.com/index.php?befehl=shows&show=8204 Gary Albright Memorial Show at WrestlingData.com]

[[Category:2000 in professional wrestling]]
[[Category:Professional wrestling memorial shows]]