{{good article}}
{{Infobox University Boat Race
| name= 133rd Boat Race
| winner =Oxford
| margin =  4 lengths
| winning_time=  19 minutes 59 seconds
| date= {{Start date|1987|3|29|df=y}}
| umpire = [[Colin Moynihan, 4th Baron Moynihan|Colin Moynihan]]<br>(Oxford)
| prevseason= [[The Boat Race 1986|1986]]
| nextseason= [[The Boat Race 1988|1988]]
| overall =69&ndash;63
| reserve_winner=Goldie
| women_winner =Cambridge
}}
The 133rd [[The Boat Race|Boat Race]] took place on 29 March 1987.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford won by four lengths.  The race featured the tallest, heaviest, youngest and oldest crew members in the event's history.  

Oxford's crew rebelled in the prelude to the race, with several American rowers and the cox leaving the squad in February after their coach [[Daniel Topolski|Dan Topolski]] removed their compatriot Chris Clark from the crew, replacing him with Scottish rower Donald Macdonald.  The rebels were replaced in the main by the reserves.  Umpired by former Oxford [[Blue (university sport)|Blue]] [[Colin Moynihan, 4th Baron Moynihan|Colin Moynihan]], it was the first year that the race was sponsored by [[Beefeater Gin]], replacing Ladbrokes after ten years.

In the 23rd reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] defeated Oxford's Isis by one length. Cambridge won the 42nd [[Women's Boat Race]].

==Background==
===History===
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] and the [[University of Cambridge]] (sometimes referred to as the "Dark Blues"<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 17 July 2014 }}</ref> and the "Light Blues"<ref name=blues/> respectively). First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 17 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities and is followed throughout the United Kingdom; the races are broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=14 July 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 14 July 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1986|1986 race]] by seven lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company | title = Boat Race – Results| accessdate = 12 June 2014}}</ref> and led overall with 69 victories to Oxford's 62 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company | title = Classic moments – the 1877 dead heat | accessdate = 6 June 2014}}</ref>  The 1987 race was the first race to be sponsored by [[Beefeater Gin]].<ref name=illwind/>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

===Mutiny===
Following defeat in the previous year's race, Oxford's first in eleven years, American Chris Clark was determined to gain revenge: "Next year we're gonna kick ass ... Cambridge's ass. Even if I have to go home and bring the whole US squad with me."<ref name=mutineer>{{Cite web | url = http://www.telegraph.co.uk/sport/olympics/rowing/2310461/When-mutineers-hit-the-Thames.html| title = When mutineers hit the Thames | work = [[The Daily Telegraph]] | date = 6 April 2007 | first = Andrew | last = Baker | accessdate = 11 August 2014}}</ref>  He recruited another four American post-graduates: three international-class rowers ([[Daniel Lyons (rower)|Dan Lyons]], Chris Huntington and Chris Penney) and a cox (Jonathan Fish),<ref name=people>{{Cite web | url = http://www.people.com/people/archive/article/0,,20095697,00.html | work = [[People (magazine)|People]] | title = Oxford's U.S. Rowers Jump Ship, Leaving the Varsity Without All Its Oars in the Water| first = William | last = Plummer | date= 23 February 1987 | accessdate = 11 August 2014}}</ref><ref name=unnatural >{{Cite web |url = https://books.google.com/books?id=HksEAAAAMBAJ&pg=PA57&lpg=PA57&dq=Chris+Huntington+rower&source=bl&ots=0_daP0DlTn&sig=46mcrt1LReQiIeqviIc6sfmX_aY&hl=en&sa=X&ei=e73pU-ikHa-U0QXAgYHYAg&ved=0CCYQ6AEwAQ#v=onepage&q=Chris%20Huntington%20rower&f=false| work = Rowing News | date = July 2007 | pages =54&ndash;63 | first = Christopher | last = Dodd |title = Unnatural selection}}</ref> in an attempt to put together the fastest Boat Race crew in the history of the contest.<ref>{{Cite news | title = Mutiny in the boathouse | work = [[The Times]] | issue = 62728 | page = 11 | first = Glenys | last = Roberts | date = 28 March 1987}}</ref>  

{{Quote box
 |quote  =When you recruit mercenaries, you can expect some pirates.
 |source =British press<ref>{{Cite web | url = https://books.google.com/books?id=BEsEAAAAMBAJ&pg=PA40&lpg=PA40&dq=mercenaries+pirates+%22boat+race%22&source=bl&ots=nYy7SPHwXW&sig=7QuQTHc0aK7QjLhP-dRaoPGzLa8&hl=en&sa=X&ei=DBkQVJeKOMvLaKLmgMgO&ved=0CDAQ6AEwAw#v=onepage&q=mercenaries%20pirates%20%22boat%20race%22&f=false | work = Rowing News | date = May 2006 | first = Jeff | last = Moag | title = Melting Pot | page = 40}}</ref>
 |quoted = 1
 |width = 25%
}}
Disagreements over the training regime of [[Daniel Topolski|Dan Topolski]], the Oxford coach, ("He wanted us to spend more time training on land than water!" lamented Lyons<ref name=people/>) led to the crew walking out on at least one occasion, and resulted in the coach revising his approach.<ref name=higher/>  A fitness test between Clark and Scottish former Blue Donald Macdonald (in which the American triumphed) resulted in a call for the Scotsman's removal; it was accompanied with a threat that the Americans would refuse to row should Macdonald remain in the crew.<ref name=higher/>  As boat club president, Macdonald "had absolute power over selection" and after announcing that Clark would row on starboard, his weaker side, Macdonald would row on the port side and Briton Tony Ward was to be dropped from the crew entirely, the American contingent mutinied.<ref name=unnatural/>  After considerable negotiation and debate, much of it conducted in the public eye, Clark, Penny, Huntington, Lyons and Fish were dropped and replaced by members of Oxford's reserve crew, Isis.<ref name=unnatural/>

==Crews==
Oxford's crew weighed an average of nearly {{convert|9|lb|kg}} a rower more than their opponents.<ref name=illwind/>  The race featured the tallest and heaviest (Oxford's stroke Gavin Stewart), youngest (Cambridge's Matthew Brittin) and oldest (Oxford's president Donald Macdonald) crew members in the event's history.<ref name=illwind/>  The Cambridge boat saw four returning [[Blue (university sport)|Blues]] while Oxford welcomed back just one, in Macdonald.  Oxford's coach was Topolski, his counterpart was [[Alan Inns]].<ref name=illwind/>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || Hugh M. Pelham || [[Christ Church, Oxford|Christ Church]] || 13 st 9.5 lb || Ian R. Clarke || [[Fitzwilliam College, Cambridge|Fitzwilliam]] || 12 st 6.5 lb
|-
| 2 ||  Peter A. Gish || [[Oriel College, Oxford|Oriel]] || 14 st 0 lb || Richard A. B. Spink || [[Downing College, Cambridge|Downing]] || 14 st 0.5 lb
|-
| 3 || Tony D. Ward || [[Oriel College, Oxford|Oriel]] || 13 st 9 lb || Nicholas J. Grundy || [[Jesus College, Cambridge|Jesus]] || 12 st 9 lb
|-
| 4 || Paul Gleeson || [[Hertford College, Oxford|Hertford]] || 14 st 12 lb || Matthew J. Brittin || [[Robinson College, Cambridge|Robinson]] || 14 st 8.5 lb
|-
| 5 || Richard Hull || [[Oriel College, Oxford|Oriel]] || 14 st 7.5 lb || Stephen M. Peel (P) || [[Downing College, Cambridge|Downing]] || 13 st 8.5 lb
|-
| 6 || Donald H. M. Macdonald (P) || [[Mansfield College, Oxford|Mansfield]] || 13 st 13 lb || Jim S. Pew|| [[Trinity College, Cambridge|Trinity]]  || 14 st 13 lb
|-
| 7 || Tom A. D. Cadoux-Hudson|| [[New College, Oxford|New College]] || 14 st 6 lb || Jim R. Garman|| [[Lady Margaret Boat Club]] ||  14 st 2 lb
|-
| [[Stroke (rowing)|Stroke]] || Gavin B. Stewart || [[Wadham College, Oxford|Wadham]] || 16 st 7 lb || Paddy H. Broughton || [[Magdalene College, Cambridge|Magdalene]]  || 14 st 1 lb
|-
| [[Coxswain (rowing)|Cox]] || Andy D. Lobbenberg || [[Balliol College, Oxford|Balliol]] || 8 st 3 lb || Julian M. Wolfson || [[Pembroke College, Cambridge|Pembroke]]  || 8 st 12 lb
|-
!colspan="7"|Source:<ref name=illwind>{{Cite news | work = [[The Times]] | date = 28 March 1987 | page = 42 | issue = 62728 | title = Ill wind plagues Blues of 1987 | first = Jim | last = Railton}}</ref><br>(P) &ndash; boat club president
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
With a more experienced crew and less disruption in the preparation for the race, Cambridge were considered favourites.<ref name=illwind/>  Oxford won the toss and elected to start from the Middlesex station.<ref name=fairy>{{Cite news | title = Oxford's gamblers gambol to flying fairy-tale finish | first = Jim | last= Railton | date = 30 March 1987 | work = [[The Times]] | page = 34 | issue =62729}}</ref>  A malfunction to umpire [[Colin Moynihan, 4th Baron Moynihan|Colin Moynihan]]'s barge caused a delay to the start; as a consequence the crews avoided racing in a lightning storm.  Straight from the start, Oxford steered towards Middlesex to seek shelter from the inclement weather.  Cambridge eventually followed, taking on water, and receiving warnings for encroaching into Oxford's water.<ref name=fairy/>  Almost a length ahead by [[Craven Cottage]], Oxford steered across and in front of Cambridge to control the race before the Mile Post.  A seven-second advantage at [[Hammersmith Bridge]] became twelve seconds by [[Barnes Railway Bridge|Barnes Bridge]] and remained so by the finishing post, with Oxford winning by four lengths in a time of 19 minutes 59 seconds.<ref name=fairy/>

In the reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] beat Oxford's Isis by one length, their first victory in three years.<ref name=results/>  Cambridge won the 42nd Women's Boat Race, their fourth victory in six years.<ref name=results/>

==Reaction==

Oxford's Macdonald was triumphant: "It was a fairy-tale."<ref name=fairy/>  Topolski acknowledged his crew's luck in winning the toss combined with the conditions: "We had been praying for rough water."<ref>{{Cite news | title = Oxford's win is a triumph for ethics| first = David| last= Miller | date = 30 March 1987 | page = 36 | issue = 62729| work = [[The Times]]}}</ref>  He also appeared conciliatory: "I wish the Americans had been there.  It has nothing to do with vindication.  We just won the race, that's all."<ref name=unnatural/>  Cadoux-Hudson said, "I thought Cambridge would murder us but we took 20 colossal strokes and there was a primeval scream from the crew. There was a huge release."<ref name=unnatural/>

==Legacy==

In 1989 Topolski and author [[Patrick Robinson (author)|Patrick Robinson]]'s book about the events, ''[[True Blue: The Oxford Boat Race Mutiny]]'', was published.  Seven years later, a [[True Blue (1996 film)|film]] based on the book was released.  Alison Gill, the then-president of the [[Oxford University Women's Boat Club]] wrote ''The Yanks in Oxford'', in which she defended the Americans and claimed Topolski wrote ''True Blue'' in order to justify his own actions.<ref name=higher>{{Cite web | url = http://www.timeshighereducation.co.uk/news/mutiny-on-the-isis/91573.article|date = 25 November 1996 | work = [[Times Higher Education]] | title = Mutiny on the Isis | accessdate = 11 August 2014| first=Chris |last = Johnston}}</ref> The journalist Christopher Dodd described ''True Blue'' as "particularly offensive".<ref name=unnatural/>

Oxford's stroke Gavin Stewart, writing in ''The Times'' in 1996, had chosen to mutiny because "Macdonald as president had lost the respect of the squad and the selection system had lost credibility".<ref name=higher/>  In 2003, Clark had "broken his silence", stating "Mutiny is such a loaded term ... Rebellion would be a more apt description.  On the face of it, I have no regrets whatsoever. However, I now lament my own personal maturity level. In hindsight my callowness had the effect of exacerbating a complicated but manageable situation."<ref name=mutineer/>  On the twentieth anniversary of the race, Topolski insisted "It was just a selection dispute, an argument which every club in every sport has from time to time.  The media just took the story and hyped it up."<ref name=mutineer/>

==References==
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1987}}
[[Category:The Boat Race]]
[[Category:1987 in English sport]]
[[Category:March 1987 sports events]]
[[Category:1987 in rowing]]