{{Use mdy dates|date=October 2016}}
{{good article}}
{{Infobox NCAA football yearly game
| Game Name          = East–West Shrine Game
| Optional Subheader = All-Star Bowl Game
| Title Sponsor      = Asset Protect
| Image              =  
| Caption            =
| Date Game Played   = January 23
| Year Game Played   = 2010
| Football Season    = 2009
| Stadium            = [[Citrus Bowl]]
| City               = [[Orlando, Florida]]
| Visitor School     = East–West Shrine Game#Game results
| Visitor Name Short = East Team
| Visitor Nickname   =
| Visitor Coach      = [[Romeo Crennel]] ([[Kansas City Chiefs|Chiefs]])
| Visitor1           = 0
| Visitor2           = 6
| Visitor3           = 0
| Visitor4           = 7
| Home School        = East–West Shrine Game#Game results
| Home Name Short    = West Team
| Home Nickname      =
| Home Coach         = [[Marty Schottenheimer]] (None)
| Home1              = 0
| Home2              = 3
| Home3              = 0
| Home4              = 7
| MVP                = [[O'Brien Schofield]] (defensive), [[Van Eskridge]] (defensive), [[Mike Kafka]] (offensive)
| Odds               =
| Anthem             = [[Kenny G]]
| Referee            =
| Halftime           = [[Kenny G]]
| Attendance         = 8,345
| US Network         = [[ESPN2]]
| US Announcers      = [[Bob Wischusen]], [[Todd McShay]], [[Brian Griese]]
| Ratings            =
| Intl Network       =
| Intl Announcers    =
}}

The '''2010 [[East–West Shrine Game]]''' was the 85th staging of the [[All-star game|all-star]] [[college football]] exhibition game featuring [[NCAA]] [[Division I (NCAA)|Division I]] [[Football Bowl Subdivision]] players.<ref name=EWAPESG/> The game featured over 100 players from the [[2009 NCAA Division I FBS football season|2009 college football season]], and prospects for the [[2010 NFL Draft|2010 Draft]] of the professional [[National Football League]] (NFL), as well as for the [[United Football League (2009)|United Football League]]'s inaugural draft.  In the week prior to the game, [[scout (sports)|scouts]] from all 32 NFL teams attended.<ref name=EWAPESG/>  The proceeds from the East-West Shrine Game benefit [[Shriners Hospitals for Children]].<ref name=EWAPESG/>

[[Marty Schottenheimer]] and [[Romeo Crennel]] served as the two teams' coaches for the game. The East team won by a 13–10 margin on the strength of a [[touchdown]] with just six seconds remaining. According to some reliable sources, [[2009 Wisconsin Badgers football team|Wisconsin]] [[defensive end]] [[O'Brien Schofield]] and [[2009 Northwestern Wildcats football team|Northwestern]] [[quarterback]] [[Mike Kafka]] of the east team were defensive and offensive MVPs, respectively.  However, other sources point to [[2009 East Carolina Pirates football team|East Carolina]] [[cornerback]] [[Van Eskridge]] also of the east team was selected as defensive MVP.

Although no players from this game were chosen in the first round of the NFL Draft and only 7 were chosen on the second day of the draft (rounds 2 & 3), a total of 34 participants were selected during the draft's seven rounds.  This includes four selections by the [[Pittsburgh Steelers]] and three each by the [[Philadelphia Eagles]] and [[Green Bay Packers]].  Three [[Utah Utes football]] players and five [[offensive tackle]]s from this game were selected in the draft.

==Game notes==
[[File:Marty Schottenheimer-Aug-11-2006-Autograph.jpg|thumb|upright|[[Marty Schottenheimer]] served as a [[head coach]].]]
The West team was coached by Schottenheimer and the East by Crennel.<ref name=G8C>{{cite web|url=http://www.shrinegame.com/coaches.shtm |title=Game #85 Coaches |accessdate=February 5, 2010 |publisher=[[East West Shrine Game]] |archiveurl=https://web.archive.org/web/20100117060931/http://www.shrinegame.com/coaches.shtm |archivedate=January 17, 2010 |deadurl=yes |df= }} </ref>  The game was played on January 23, 2010 at 3:00&nbsp;p.m. local time at [[Citrus Bowl]] in [[Orlando, Florida|Orlando]], Florida.<ref name=ESPNbs>{{cite web|url=http://scorecenter.espn.go.com/ncf/boxscore?gameId=300233147|title=EAST 13, WEST 10 (box score)|accessdate=February 5, 2010|publisher=[[ESPN.com]]}}</ref>  It was the first time the game was played at the Citrus Bowl.<ref name=EWAPESG/> In fact, it was the first time the game was played in the state of Florida.<ref name=EbWaSg/>  The game had been hosted in California since 1925 until 2006 when it was moved to Texas.<ref name=BTwriESg/>  The game was broadcast on [[ESPN2]].<ref>{{cite web|url=http://insider.espn.go.com/nfl/draft10/insider/news/story?id=4837517&action=login&appRedirect=http%3a%2f%2finsider.espn.go.com%2fnfl%2fdraft10%2finsider%2fnews%2fstory%3fid%3d4837517|title=First practice sets storylines: Players in just shorts and helmets, but some players still stand out while others struggle|date=January 18, 2010|accessdate=February 6, 2010|publisher=[[ESPN]]|author=McShay, Todd| archiveurl= https://web.archive.org/web/20100211083555/http://insider.espn.go.com/nfl/draft10/insider/news/story?id=4837517&action=login&appRedirect=http%3A%2F%2Finsider.espn.go.com%2Fnfl%2Fdraft10%2Finsider%2Fnews%2Fstory%3Fid%3D4837517| archivedate= February 11, 2010 | deadurl= no}}</ref>  The combined score of 23 was the lowest since the 14–6 1992 East-West Shrine Game.<ref name=EWAPESG/> The total attendance of 8,345 was the lowest in the history of the self-described longest running college all-star game.<ref name=EbWaSg/>  During the week before the game was played, the players interacted with general managers and scouts between practices.<ref name=EbWaSg/>

In the first half, the East posted two [[interception]]s.  Schofield, who also had three [[tackle (football move)|tackles]], made an interception of a pass by [[2009 BYU Cougars football team|BYU]] quarterback [[Max Hall]].<ref name=EWAPESG/>  Subsequently, Eskridge also intercepted a pass by [[2009 Kansas Jayhawks football team|Kansas]] quarterback [[Todd Reesing]].<ref name=EbWaSg/> The only first half scoring came on [[Field goal (football)|field goal]]s. [[Joshua Shene]] of [[2009 Ole Miss Rebels football team|Ole Miss]] posted field goals of 44 and 40&nbsp;yards for the East. [[2009 Texas Longhorns football team|Texas]] [[placekicker]] [[Hunter Lawrence]] had a 47-yarder for the West.<ref name=EbWaSg/>  Shene's field goals both came in the final two minutes and fifteen seconds of the first half.<ref name=SE1SW1>{{cite news|url=http://content.usatoday.com/sportsdata/football/ncaaf/game/Shrine-EastAll-Stars_Shrine-WestAll-Stars/2010/01/23|title=Shrine-East 13, Shrine-West 10 (Game Story tab)|accessdate=February 6, 2010|date=January 23, 2010|work=[[USA Today]]}}</ref>

The West took a 10–6 lead with 6:59 left in the game when Hall connected with [[2009 UCLA Bruins football team|UCLA]] [[fullback (American football)|fullback]] [[Ryan Moya]] for an 8-yard touchdown pass.<ref name=EWAPESG/><ref>{{cite web|url=http://scorecenter.espn.go.com/ncf/playbyplay?gameId=300233147&period=0|title=EAST 13, WEST 10 (play-by-play)|accessdate=February 5, 2010|publisher=[[ESPN.com]]}}</ref> A key play on the drive was a 41-yard pass from Hall to [[2009 Eastern Washington Eagles football team|Eastern Washington]] tight end [[Nathan Overbay]] as he was cutting across the middle of a wide-open field.<ref name=EbWaSg/> BYU's [[Dennis Pitta]] then caught a 17&nbsp;yard reception.<ref name=SE1SW1/>

{{multiple image
 | align     = left
 | direction = horizontal
 | header    =
 | header_align = left/right/center
 | header_background =
 | footer    = [[MVP]]s [[Mike Kafka]] and [[O'Brien Schofield]]
 | footer_align = left/right/center
 | footer_background =
 | width     =

 | image1    = Mike Kafka.jpg
 | width1    = 104
 | alt1      =
 | caption1  =

 | image2    = 20091010 O'Brien Schofield leads Wisconsin onto the field.jpg
 | width2    = 125
 | alt2      =
 | caption2  =
}}
Kafka threw the game-winning touchdown to [[2009 Penn State Nittany Lions football team|Penn State]] [[tight end]] [[Andrew Quarless]] with six seconds left, resulting in the 13–10 victory over the West.  The touchdown capped an 11-play 55-yard game-winning drive.  The play before the touchdown, Kafka had [[Quarterback scramble|scrambled]] out of the grasp of a swarm of defenders for a 9-yard gain.<ref name=EbWaSg/>  During the drive [[Freddie Barnes]] of [[2009 Bowling Green Falcons football team|Bowling Green]] caught three consecutive passes of 12, 7, and 10&nbsp;yards.<ref name=SE1SW1/> The final play was set up after Kafka eluded a [[quarterback sack|sack]] during a 2nd down and 10&nbsp;yards situation on the West 11 yardline which led to a [[Time-out (sport)|timeout]] with 12&nbsp;seconds left before Kafka connected with Quarless in the back of the end zone.<ref name=SE1SW1/>

Kafka was 18 of 27 for 150&nbsp;yards and [[2009 Michigan State Spartans football team|Michigan State]]'s [[Blair White]] made 7 [[reception (American football)|receptions]] for 93&nbsp;yards for the East.<ref name=SE1SW1/> Hall was 7 of 12 for 119&nbsp;yards, a touchdown, and an interception and Pitta recorded 4 receptions for 72&nbsp;yards for the West.<ref name=SE1SW1/> The game saw no one accumulate more than 28 total rushing [[yards from scrimmage]] and no run was longer than 16&nbsp;yards.<ref name=ESPNbs/> In addition to the aforementioned players, the defensive standouts for the East on Saturday were [[2009 Virginia Tech Hokies football team|Virginia Tech]]'s [[Kam Chancellor]] (7 tackles), [[2009 South Florida Bulls football team|USF]]'s [[Kion Wilson]] (6 tackles, forced fumble) and Ole Miss' [[Greg Hardy]], Jr. (5 tackles, sack).  The West were led by seven tackles from Kansas' [[Darrell Stuckey]] and six tackles and three pass breakups from [[2009 Texas Tech Red Raiders football team|Texas Tech]]'s [[Jamar Wall]].<ref name=BTwriESg/>

According to the release from Shriners International Headquarters and several other sources, Schofield and Kafka of the east team were defensive and offensive MVPs, respectively.<ref name=EWAPESG>{{cite web|url=http://www.shrinershq.org/Shrine/Feature/|title=East Wins Asset Protect East-West Shrine Game |accessdate=February 5, 2010|date=January 23, 2010|publisher=Shriners International Headquarters}}</ref><ref name=BTwriESg>{{cite web|url=http://www.orlandosentinel.com/sports/college/os-east-west-shrine-sidebar-0124-20100123,0,2832819.story|title=Big Ten well represented in East-West Shrine game: Wisconsin's O'Brien Shofield wins East defensive MVP|accessdate=February 5, 2010|date=January 23, 2010|work=[[Orlando Sentinel]]|author=Carnahan, J.C.}}</ref><ref>{{cite news|url=https://www.nytimes.com/aponline/2010/01/23/sports/AP-FBC-East-West-Shrine.html?_r=1&scp=1&sq=East+West+Shrine+game+Schofield&st=nyt|title=Northwestern's Kafka Shines in Shrine Game|accessdate=February 5, 2010|date=January 23, 2010|work=[[The New York Times]]|author=}} {{Dead link|date=September 2010|bot=RjwilmsiBot}}</ref> However, according to the [[Associated Press]] [[press release]] that was published by [[ESPN]], ''[[Sports Illustrated]]'' and several other sources, Eskridge was selected as defensive MVP.<ref name=EbWaSg>{{cite web|url=http://sports.espn.go.com/ncf/news/story?id=4852424|title=East beats West at Shrine game|accessdate=February 5, 2010|date=January 23, 2010|publisher=[[ESPN.com]]| archiveurl= https://web.archive.org/web/20100129170428/http://sports.espn.go.com/ncf/news/story?id=4852424| archivedate= January 29, 2010 | deadurl= no}}</ref><ref>{{cite news|url=http://sportsillustrated.cnn.com/2010/football/ncaa/01/24/east.west.shrine.ap/index.html|title=East wins Shrine Game 13–10|accessdate=February 5, 2010|date=January 24, 2010|work=[[Sports Illustrated]]| archiveurl= https://web.archive.org/web/20100128112311/http://sportsillustrated.cnn.com/2010/football/ncaa/01/24/east.west.shrine.ap/index.html| archivedate= January 28, 2010 | deadurl= no}} </ref>

===Scoring summary===
{| class="wikitable"
|-
! Scoring Play
! Score
|-
| colspan="4" style="text-align:center;"| '''2nd Quarter'''
|-
| West – [[Hunter Lawrence]] 47&nbsp;yd field goal, 5:56
| West 3–0
|-
| East – [[Joshua Shene]] 44&nbsp;yd field goal, 2:15
| TIE 3–3
|-
| East – Joshua Shene 40&nbsp;yd field goal, 0:05
| East 6–3
|-
| colspan="4" style="text-align:center;"| '''4th Quarter'''
|-
| West – [[Ryan Moya]] 8&nbsp;Yd Pass From [[Max Hall]] (Hunter Lawrence Kick), 6:59
| West 10–6
|-
| East – [[Andrew Quarless]] 2&nbsp;Yd Pass From [[Mike Kafka]] (Joshua Shene Kick), 0:06
| East 13–10
|}

===Statistical leaders===
{| class="wikitable" style="text-align:left"
|-
|  style="text-align:center; background:#f0f0f0;"|
| colspan="3" |'''East'''
| colspan="3" |'''West'''
|-
| ||Player||Yards||TDs||Player||Yards||TDs
|-
| Leading Passer||[[Mike Kafka]]||150||1||[[Max Hall]]||119||1
|-
| Leading Rusher||[[Andre Dixon]]||24||0||[[Pat Paschall]]||28||0
|-
| Leading Receiver||[[Blair White]]||93||0||[[Dennis Pitta]]||72||0
|}

==Coaching staff==

===West Team===
{| class="wikitable"
|-
! Name || Position
|-
|[[Marty Schottenheimer]] || [[Head coach]]
|-
|[[Gary Brown (running back)|Gary Brown]] || [[Running back]]s/[[Special teams]]
|-
|[[Ray Brown (offensive lineman)|Ray Brown]] || [[Offensive line]]
|-
|[[Gerald Carr (American football)|Gerald Carr]] || [[Quarterback]]s
|-
|[[Jay Hayes]] || [[Defensive line]]
|-
|[[Keenan McCardell]] || [[Wide receiver]]s
|-
|[[Marlon McCree]] || [[Defensive back]]s
|-
|[[Kurt Schottenheimer]] || [[Linebacker]]/[[Defensive coordinator]]
|-
|[[Mike Stock (American football)|Mike Stock]] || [[Tight end]]s/Special teams
|}

===East Team===
{| class="wikitable"
|-
! Name || Position
|-
|[[Romeo Crennel]] || [[Head coach]]
|-
|[[Dave Atkins (NFL coach)|Dave Atkins]] || [[Running back]]s
|-
|[[Pep Hamilton]] || [[Quarterback]]s
|-
|[[Sam Mills, Jr.]] || [[Tight end]]s/[[Special teams]]
|-
|[[Robert Prince (American football)|Robert Prince]] || [[Wide receiver]]s
|-
|[[Will Shields]] || [[Offensive line]]
|-
|[[Vantz Singletary]] || [[Linebacker]]s
|-
|[[Richard Solomon (American football)|Richard Solomon]] || [[Defensive back]]s
|-
|[[Eric Washington (American football)|Eric Washington]] || [[Defensive line]]
|}

==Rosters==

===East team===
{| class="wikitable"
! Name
! Position
! School
! Hometown
|-
| [[Alejandro Villanueva (American football)|Ali Villanueva]] ||[[Tight end]] ||[[Army Black Knights football|Army]] ||[[Meridian, MS]]
|-
| [[Mike McLaughlin (American football)|Mike McLaughlin]] ||[[Inside linebacker]] ||[[Boston College Eagles football|Boston College]] ||[[Woburn, MA]]
|-
| [[Freddie Barnes]] ||[[Wide receiver]] ||[[Bowling Green Falcons football|Bowling Green State University]] ||[[Chicago, IL]]
|-
| [[Thomas Austin (American football)|Thomas Austin]] ||[[Offensive guard]]  ||[[Clemson Tigers football|Clemson University]]  ||[[Clemson, SC]]
|-
| [[Chris Chancellor]] ||[[Cornerback]] ||Clemson University  ||[[Miami, FL]]
|-
| [[Kavell Conner]] ||[[Outside linebacker]] ||Clemson University  ||[[Richmond, VA]]
|-
| [[Patrick Simonds]] ||Wide Receiver ||[[Colgate Raiders football|Colgate University]] ||[[Sidney (town), New York|Sidney, NY]]
|-
| [[Van Eskridge]]  ||[[Free safety]] ||[[East Carolina Pirates football|East Carolina University]]  ||[[Shelby, NC]]
|-
| [[John Skelton (American football)|John Skelton]] ||[[Quarterback]] ||[[Fordham Rams football|Fordham University]] ||[[El Paso, TX]]
|-
| [[Cord Howard]] ||Offensive Guard ||[[Georgia Tech Yellow Jackets football|Georgia Tech]] ||[[Phenix City, AL]]
|-
| [[Rodger Saffold III]] ||[[Offensive tackle]] ||[[Indiana Hoosiers football|Indiana University]] ||[[Bedford, OH]]
|-
| [[Rahim Alem]] ||[[Defensive end]] ||[[LSU Tigers football|Louisiana State University]]  ||[[New Orleans, LA]]
|-
| [[Richard Dickson]] ||[[Fullback (American football)|Fullback]]  ||Louisiana State University  ||[[Ocean Springs, MS]]
|-
| [[Blair White]] ||Wide Receiver ||[[Michigan State Spartans football|Michigan State University]] ||[[Saginaw, MI]]
|-
| [[Chris McCoy]] ||Outside Linebacker ||[[Middle Tennessee Blue Raiders football|Middle Tennessee State]] ||[[Villa Roca, MS]]
|-
| [[Clay Harbor]] ||Tight End ||{{cfb link|year=1910|team=Missouri State Bears|school=Missouri State University|title=Missouri State University}} ||[[Dwight, IL]]
|-
| [[Jamar Chaney]] ||Inside Linebacker ||[[Mississippi State Bulldogs football|Mississippi State University]] ||[[Ft. Pierce, FL]]
|-
| [[Ross Pospisil]] ||Inside Linebacker ||[[Navy Midshipmen football|Navy]] ||[[Temple, TX]]
|-
| [[Willie Young (defensive end)|Willie Young]]  ||Defensive End  ||[[NC State Wolfpack football|North Carolina State University]] ||[[Riviera Beach, FL]]
|-
| [[Mike Kafka]] ||Quarterback ||[[Northwestern Wildcats football|Northwestern University]] ||[[Oak Lawn, IL]]
|-
| [[Jim Cordle]] ||[[Center (American football)|Center]] ||[[Ohio State Buckeyes football|Ohio State University]] ||[[Lancaster, OH]]
|-
| [[Doug Worthington]] ||[[Defensive tackle]] ||Ohio State University ||[[Athol Springs, NY]]
|-
| [[Jeremy Boone]] ||[[Punter (football)|Punter]] ||[[Penn State Nittany Lions football|Penn State University]] ||[[Mechanicsburg, PA]]
|-
| [[Daryll Clark]] ||Quarterback ||Penn State University ||[[Youngstown, OH]]
|-
| [[Andrew Quarless]] ||Tight End ||Penn State University ||[[Long Island, NY]]
|-
| [[Mike Neal]] ||Defensive Tackle ||[[Purdue Boilermakers football|Purdue University]] ||[[Merrillville, IN]]
|-
| [[Kevin Haslam (American football player)|Kevin Haslam]] ||Offensive Tackle ||[[Rutgers Scarlet Knights football|Rutgers University]] ||[[Mahwah, NJ]]
|-
| [[Andre Anderson (American football)|Andre Anderson]]  ||[[Running back]]  ||[[Tulane Green Wave football|Tulane University]]  ||[[Stone Mountain, GA]]
|-
| [[Naaman Roosevelt]] ||Wide Receiver ||[[Buffalo Bulls football|University at Buffalo]] ||[[Buffalo, NY]]
|-
| [[Justin Woodall]] ||[[Strong safety]] ||[[Alabama Crimson Tide football|University of Alabama]] ||[[Oxford, MS]]
|-
| [[Mitch Petrus]] ||Offensive Guard ||[[Arkansas Razorbacks football|University of Arkansas]] ||[[Carlisle, AR]]
|-
| [[Torell Troup]] ||Defensive Tackle ||[[UCF Knights football|University of Central Florida]] ||[[Conyers, GA]]
|-
| [[Andre Dixon]] ||Running Back  ||[[Connecticut Huskies football|University of Connecticut]] ||[[North Brunswick, NJ]]
|-
| [[Lindsey Witten]] ||Defensive End ||University of Connecticut ||[[Cleveland, OH]]
|-
| [[Kyle Calloway]] ||Offensive Tackle ||[[Iowa Hawkeyes football|University of Iowa]]  ||[[Vail, AZ]]
|-
| [[Javarris James]] ||Running Back  ||[[Miami Hurricanes football|University of Miami]] ||[[Immokalee, FL]]
|-
| [[A.J. Trump]] ||Center ||University of Miami ||[[Palm Harbor, FL]]
|-
| [[Greg Hardy]] ||Defensive End ||[[Ole Miss Rebels football|University of Mississippi]] ||[[Millington,TN]]
|-
| [[Joshua Shene]] ||[[Placekicker|Kicker]] ||University of Mississippi ||[[Oklahoma City, OK]]
|-
| [[Aaron Berry]] ||Cornerback ||[[Pittsburgh Panthers football|University of Pittsburgh]] ||[[Harrisburg, PA]]
|-
| [[Nate Byham]] ||Tight End ||University of Pittsburgh ||[[Franklin, PA]]
|-
| [[Darian Stewart]] ||Strong Safety ||[[South Carolina Gamecocks football|University of South Carolina]] ||[[Huntsville, AL]]
|-
| [[Kion Wilson]] ||Inside Linebacker ||University of South Florida ||[[Miami, FL]]
|-
| [[Chris Scott (offensive lineman)|Chris Scott]] ||Offensive Tackle ||[[Tennessee Volunteers football|University of Tennessee]] ||[[Riverdale, GA]]
|-
| [[Barry Church]]  ||Strong Safety ||[[Toledo Rockets football|University of Toledo]] ||[[Pittsburgh, PA]]
|-
| [[Nate Collins]] ||Defensive Tackle ||[[Virginia Cavaliers football|University of Virginia]] ||[[Port Chester, NY]]
|-
| [[Matt Morencie]] ||Center ||{{cfb link|year=1910|team=Windsor Lancers|school=University of Windsor|title=University of Windsor}} ||[[Windsor, ON]]
|-
| [[O'Brien Schofield]] ||Outside Linebacker ||[[Wisconsin Badgers football|University of Wisconsin]] ||[[Chicago, IL]]
|-
| [[Kam Chancellor]] ||Free Safety ||[[Virginia Tech Hokies football|Virginia Tech]] ||[[Norfolk, VA]]
|-
| [[Sergio Render]] ||Offensive Guard ||Virginia Tech ||[[Newman, GA]]
|-
| [[Stephan Virgil]] ||Cornerback ||Virginia Tech ||[[Rocky Mount, NC]]
|-
| [[Chris DeGeare]] ||Offensive Guard ||[[Wake Forest Demon Deacons football|Wake Forest University]] ||[[Kernersville, NC]]
|-
| [[Ben Staggs]] ||Offensive Tackle ||{{cfb link|year=1910|team=West Liberty Hilltoppers|school=West Liberty University|title=West Liberty University}} ||[[Wooster, OH]]
|-
| [[Alric Arnett]] ||Wide Receiver ||[[West Virginia Mountaineers football|West Virginia University]] ||[[Belle Glade, FL]]
|-
| [[Patrick Stoudamire]] ||Cornerback ||{{cfb link|year=1910|team=Western Illinois Leathernecks|school=Western Illinois University|title=Western Illinois University}} ||[[Portland, OR]]
|}

===West team===
{| class="wikitable"
! Name
! Position
! School
! Hometown
|-
| [[Chris Thomas (Air Force)|Chris Thomas]] ||Safety  ||[[Air Force Falcons football|Air Force Academy]] ||[[Westerville, OH]]
|-
| [[Dimitri Nance]] ||Running Back ||[[Arizona State Sun Devils football|Arizona State University]] ||[[Euless, TX]]
|-
| [[Carter Brunelle]] ||Long Snapper ||[[Baylor Bears football|Baylor University]] ||[[San Antonio, TX]]
|-
| [[Joe Pawelek]] ||Inside Linebacker ||Baylor University ||[[San Antonio, TX]]
|-
| [[Max Hall]] ||Quarterback ||[[BYU Cougars football|Brigham Young University]] ||[[Mesa, AZ]]
|-
| [[Jan Jorgensen]] ||Defensive End ||Brigham Young University ||[[Helper, UT]]
|-
| [[Dennis Pitta]] ||Tight End ||Brigham Young University ||[[Moorpark, CA]]
|-
| [[Klint Kubiak]] ||Safety  ||[[Colorado State Rams football|Colorado State University]] ||[[Houston,TX]]
|-
| [[Cole Pemberton]] ||Offensive Takle ||Colorado State University ||[[Highsland Ranch, CO]]
|-
| [[Shelley Smith (American football)|Shelley Smith]] ||Offensive Guard ||Colorado State University ||[[Phoenix, AZ]]
|-
| [[Matt Nichols]] ||Quarterback ||[[Eastern Washington Eagles football|Eastern Washington University]] ||[[Cottonwood, CA]]
|-
| [[Nathan Overbay]] ||Tight End ||Eastern Washington University ||[[Chehalis, WA]]
|-
| [[Seyi Ajirotutu]] ||Wide Receiver ||[[Fresno State Bulldogs football|Fresno State University]] ||[[El Dorado Hills, CA]]
|-
| [[Robert Malone]] ||Punter ||Fresno State University ||[[Riverside, CA]]
|-
| [[Lonyae Miller]] ||Running Back ||Fresno State University ||[[Fontana, CA]]
|-
| [[Reggie Stephens (offensive lineman)|Reggie Stephens]] ||Offensive Guard ||[[Iowa State Cyclones football|Iowa State University]] ||[[Dallas, TX]]
|-
| [[Jeffery Fitzgerald]] ||Defensive End ||[[Kansas State Wildcats football|Kansas State University]] ||[[Richmond, VA]]
|-
| [[Thad Turner]] ||Cornerback ||[[Ohio Bobcats football|Ohio University]] ||[[Marietta, GA]]
|-
| [[Keith Toston]] ||Running Back ||[[Oklahoma State Cowboys football|Oklahoma State University]] ||[[Angleton, TX]]
|-
| [[Keaton Kristick]] ||Outside Linebacker ||[[Oregon State Beavers football|Oregon State University]] ||[[Fountain Hills, AZ]]
|-
| [[Emmanuel Sanders]] ||Wide Receiver ||[[SMU Mustangs football|Southern Methodist University]] ||[[Bellville, TX]]
|-
| [[Chris Marinelli]] ||Offensive Tackle ||[[Stanford Cardinal football|Stanford University]] ||[[Braintree, Massachusetts]]
|-
| [[Ekom Udofia]] ||Defensive Tackle ||Stanford University ||[[Phoenix, AZ]]
|-
| [[Mike Hicks (American football)|Mike Hicks]] ||Wide Receiver ||{{cfb link|year=1910|team=Tennessee-Martin Skyhawks|school=University of Tennessee at Martin|title=Tennessee Martin}} ||[[Jacksonville, FL]]
|-
| [[Michael Shumard]] ||Offensive Guard ||[[Texas A&M Aggies football|Texas A& M University]] ||[[Fort Hood, TX]]
|-
| [[Marshall Newhouse]] ||Offensive Tackle ||[[TCU Horned Frogs football|Texas Christian University]] ||[[Dallas, TX]]
|-
| [[Brandon Carter (American football)|Brandon Carter]] ||Offensive Guard ||[[Texas Tech Red Raiders football|Texas Tech University]] ||[[Longview, TX]]
|-
| [[Jamar Wall]] ||Cornerback ||Texas Tech University ||[[Plaview, TX]]
|-
| [[Terrence Austin]] ||Wide Receiver ||[[UCLA Bruins football|UCLA]] ||[[Longbeach, CA]]
|-
| [[Reggie Carter (American football)|Reggie Carter]] ||Inside Linebacker ||UCLA ||[[Los Angeles, CA]]
|-
| [[Ryan Moya]] ||Fullback ||UCLA ||[[El Dorado Hills, CA]]
|-
| [[Alterraun Verner]] ||Cornerback ||UCLA ||[[Carson, CA]]
|-
| [[Earl Mitchell]] ||Defensive Tackle ||[[Arizona Wildcats football|University of Arizona]] ||[[Houston, TX]]
|-
| [[Devin Ross]] ||Cornerback ||University of Arizona ||[[Los Angeles, CA]]
|-
| [[Mike Tepper]] ||Offensive Tackle ||[[California Golden Bears football|University of California]] ||[[Cypress, CA]]
|-
| [[Verran Tucker]] ||Wide Receiver ||University of California ||[[Lynwood, CA]]
|-
| [[Riar Geer]] ||Tight End ||[[Colorado Buffaloes football|University of Colorado]] ||[[Grand Junction, CO]]
|-
| [[John Estes]] ||Center ||[[Hawaii Rainbows football|University of Hawaii]] ||[[Stockton, CA]]
|-
| [[Kerry Meier]] ||Wide Receiver ||[[Kansas Jayhawks football|University of Kansas]] ||[[Pittsburg, KS]]
|-
| [[Todd Reesing]] ||Quarterback ||University of Kansas ||[[Austin, TX]]
|-
| [[Darrell Stuckey]] ||Strong Safety ||University of Kansas ||[[Kansas City, KS]]
|-
| [[Lee Campbell (American football)|Lee Campbell]] ||Outside Linebacker ||[[Minnesota Golden Gophers football|University of Minnesota]] ||[[Naples, FL]]
|-
| [[Jaron Baston]] ||Defensive Tackle ||[[Missouri Tigers football|University of Missouri]] ||[[Blue Springs, MO]]
|-
| [[James Ruffin (American football)|James Ruffin]] ||Defensive End ||[[Northern Iowa Panthers football|University of Northern Iowa]] ||[[Burnsville, MN]]
|-
| [[Brian Jackson (American football)|Brian Jackson]] ||Cornerback ||[[Oklahoma Sooners football|University of Oklahoma]] ||[[DeSoto, TX]]
|-
| [[T. J. Ward]] ||Free Safety ||[[Oregon Ducks football|University of Oregon]] ||[[Antioch, CA]]
|-
| [[Jordan Sisco]] ||Wide Receiver ||{{cfb link|year=1910|team=Regina Rams|school=University of Regina|title=University of Regina}} ||[[Regina, SK]]
|-
| [[Hunter Lawrence]] ||Kicker ||[[Texas Longhorns football|University of Texas]] ||[[Boerne, TX]]
|-
| [[Robert Johnson (safety)|Robert Johnson]] ||Safety  ||[[Utah Utes football|University of Utah]] ||[[Los Angeles, CA]]
|-
| [[David Reed (American football)|David Reed]] ||Wide Receiver ||University of Utah ||[[New Britain, CT]]
|-
| [[Stevenson Sylvester]] ||Outside Linebacker ||University of Utah ||[[Las Vegas, NV]]
|-
| [[Daniel Te'o Nesheim]] ||Defensive Tackle ||[[Washington Huskies football|University of Washington]] ||[[Waikoloa, HI]]
|-
| [[Jason Beauchamp]] ||Outside Linebacker ||[[UNLV Rebels football|UNLV]] ||[[San Diego, CA]]
|-
| [[Martin Tevaseu]] ||Defensive Tackle ||UNLV ||[[Boonville, CA]]
|-
| [[Kenny Alfred]]  ||Center ||[[Washington State Cougars football|Washington State University]]  ||[[Gig Harbor, WA]]
|}

==2010 NFL Draft==
{{main article|2010 NFL Draft}}

Below is a list of the 34 players from this game that were drafted in the [[2010 NFL Draft]].  The [[Pittsburgh Steelers]] drafted four players that they scouted at this game and both the [[Philadelphia Eagles]] and the [[Green Bay Packers]] scouted three. The [[Arizona Cardinals]], [[Buffalo Bills]], [[Houston Texans]], [[Tennessee Titans]] and [[Baltimore Ravens]] each selected two.  Five [[offensive tackle]]s, four [[defensive tackle]]s, [[defensive end]]s, [[tight end]]s and [[wide receiver]]s were drafted from this game.  Three players from the [[Utah Utes football|Utah Utes]] as well as two each from the [[UCLA Bruins football|UCLA Bruins]] and [[Kansas Jayhawks football|Kansas Jayhawks]] were selected.  Although 34 players were selected during the seven round draft, none were selected in the first round, while ten were chosen in the fifth and an additional 7 were chosen in the final seventh round.<ref>{{cite web|url=http://espn.go.com/nfl/draft10/|title=NFL Draft 2010|accessdate=April 29, 2010|publisher=[[ESPN]]| archiveurl= https://web.archive.org/web/20100426235259/http://espn.go.com/nfl/draft10/| archivedate= April 26, 2010 | deadurl= no}}</ref>

The east team's [[Matt Morencie]] had already been drafted with the fifth pick of the third round in the [[2009 CFL Draft]] by the [[BC Lions]].<ref name="2009CFLDraft" group=" ">{{cite web| url = http://www.ticats.ca/page/draft_2009| title = Ticats.ca 2009 CFL Canadian Draft Tracker |work = www.ticats.ca| publisher  = Hamilton Tiger-Cats |archiveurl = https://archive.is/20130115153036/http://www.ticats.ca/page/draft_2009| archivedate = January 15, 2013| dead-url = yes| accessdate  = April 30, 2010}}</ref>  [[Jordan Sisco]]  was selected with the first pick in the second round (8th overall) of the [[2010 CFL Draft]] by the [[Saskatchewan Roughriders]].<ref>{{cite web|url=http://www.leaderpost.com/sports/Saskatchewan+Roughriders+happy+land+Regina+Jordan+Sisco+2010+draft/2978606/story.html|title=Saskatchewan Roughriders happy to land Regina's Jordan Sisco in 2010 CFL draft|accessdate=May 3, 2010|date=May 2, 2010|work=[[Leader-Post]]|author=McCormick, Murray| archiveurl= https://web.archive.org/web/20100506071720/http://www.leaderpost.com/sports/Saskatchewan+Roughriders+happy+land+Regina+Jordan+Sisco+2010+draft/2978606/story.html| archivedate= May 6, 2010 | deadurl= no}}</ref>

{| class="wikitable sortable sortable" style="width:100%;"
|-
!  style="background:#a8bdec; width:7%;"| Round # !!  style="background:#a8bdec; width:7%;"| Pick # !!  style="width:40%; background:#a8bdec;"| NFL Team !!  style="width:20%; background:#a8bdec;"| Player !!  style="width:13%; background:#a8bdec;"| Position !!style="background:#A8BDEC;"| College
|-
! 2||33
| St. Louis Rams || [[Rodger Saffold]] || Offensive tackle || [[Indiana Hoosiers football|Indiana]]
|-
! 2||38
| Cleveland Browns || [[T. J. Ward]] || Safety || [[Oregon Ducks football|Oregon]]
|-
! 2||41
| Buffalo Bills || [[Torrell Troup]] || Defensive tackle || [[UCF Knights football|UCF]]
|-
! 2||56
| Green Bay Packers || [[Mike Neal]] || Defensive tackle || [[Purdue Boilermakers football|Purdue]]
|-
! 3||81
| Houston Texans || [[Earl Mitchell]] || Defensive tackle || [[Arizona Wildcats football|Arizona]]
|-
! 3||82
| Pittsburgh Steelers || [[Emmanuel Sanders]] || Wide receiver || [[SMU Mustangs football|SMU]]
|-
! 3||86
| Philadelphia Eagles <sup>(from Green Bay)</sup> || [[Daniel Te'o-Nesheim]] || Defensive end || [[Washington Huskies football|Washington]]
|-
! 4||104
| Tennessee Titans <sup>(from Seattle)</sup> || [[Alterraun Verner]] || Cornerback || [[UCLA Bruins football|UCLA]]
|-
! 4||110
| San Diego Chargers <sup>(from Miami)</sup> || [[Darrell Stuckey]] || Safety || [[Kansas Jayhawks football|Kansas]]
|-
! 4||114
| Baltimore Ravens <sup>(from Denver)</sup> || [[Dennis Pitta]] || Tight end || [[BYU Cougars football|BYU]]
|-
! 4||122
| Philadelphia Eagles <sup>(from Green Bay)</sup> || [[Mike Kafka]] || Quarterback || [[Northwestern Wildcats football|Northwestern]]
|-
! 4||125
| Philadelphia Eagles <sup>(from Dallas)</sup> || [[Clay Harbor]] || Tight end || [[Missouri State Bears football|Missouri State]]
|-
! 4||130
| Arizona Cardinals <sup>(from New Orleans Saints)</sup> || [[O'Brien Schofield]] || Defensive end || [[Wisconsin Badgers football|Wisconsin]]
|-
! 5||147
| New York Giants || [[Mitch Petrus]] || Guard || [[Arkansas Razorbacks football|Arkansas]]
|-
! 5||148
| Tennessee Titans || [[Robert Johnson (safety)|Robert Johnson]] || Safety || [[Utah Utes football|Utah]]
|-
! 5||151
| Pittsburgh Steelers || [[Chris Scott (offensive lineman)|Chris Scott]] || Offensive tackle || [[Tennessee Volunteers football|Tennessee]]
|-
! 5||154
| Green Bay Packers || [[Andrew Quarless]] || Tight end || [[Penn State Nittany Lions football|Penn State]]
|-
! 5||155
| Arizona Cardinals <sup>(from Philadelphia via New York Jets and Pittsburgh)</sup> || [[John Skelton (American football)|John Skelton]] || Quarterback || [[Fordham Rams football|Fordham]]
|-
! 5||156
| Baltimore Ravens || [[David Reed (American football)|David Reed]] || Wide receiver || Utah
|-
! 5||161
| Minnesota Vikings || [[Chris DeGeare]] || Offensive tackle || [[Wake Forest Demon Deacons football|Wake Forest]]
|-
! 5||165
| Atlanta Falcons || [[Kerry Meier]] || Wide receiver || Kansas
|-
! 5||166
| Pittsburgh Steelers || [[Stevenson Sylvester]] || Linebacker || Utah
|-
! 5||169
| Green Bay Packers || [[Marshall Newhouse]] || Offensive tackle || [[TCU Horned Frogs football|TCU]]
|-
! 6||175
| Carolina Panthers <sup>(from Oakland)</sup> || [[Greg Hardy]] || Defensive end || [[Ole Miss Rebels football|Ole Miss]]
|-
! 6||182
| San Francisco 49ers || [[Nate Byham]] || Tight end  || [[Pittsburgh Panthers football|Pittsburgh]]
|-
! 6||187
| Houston Texans || [[Shelley Smith (American football)|Shelley Smith]] || Guard || [[Colorado State Rams football|Colorado State]]
|-
! 6||196
| Dallas Cowboys || [[Jamar Wall]] || Cornerback || [[Texas Tech Red Raiders football|Texas Tech]]
|-
! 7||212
| Miami Dolphins <sup>(from Kansas City)</sup> || [[Chris McCoy (American football)|Chris McCoy]] || Linebacker || [[Middle Tennessee Blue Raiders football|Middle Tennessee]]
|-
! 7||213
| Detroit Lions <sup>(from Seattle)</sup> || [[Willie Young (defensive end)|Willie Young]] || Defensive end || [[NC State Wolfpack football|North Carolina State]]
|-
! 7||216
| Buffalo Bills || [[Kyle Calloway]] || Offensive tackle || [[Iowa Hawkeyes football|Iowa]]
|-
! 7||219
| Washington Redskins <sup>(from Miami)</sup> || [[Terrence Austin]] || Wide receiver || UCLA
|-
! 7||228
| Cincinnati Bengals || [[Reggie Stephens (offensive lineman)|Reggie Stephens]] || Guard || [[Iowa State Cyclones football|Iowa State]]
|-
! 7||240
| Indianapolis Colts || [[Kavell Conner]] || Linebacker || [[Clemson Tigers football|Clemson]]
|-
! 7||242
| Pittsburgh Steelers || [[Doug Worthington]] || Defensive tackle || [[Ohio State Buckeyes football|Ohio State]]
|}

==References==
{{Reflist|30em}}

{{2009 bowl game navbox}}
{{East–West Shrine Game navbox}}

{{DEFAULTSORT:2010 East-West Shrine Game}}
[[Category:2009–10 NCAA football bowl games|East-West Shrine Game]]
[[Category:East–West Shrine Game]]
[[Category:American football in Orlando, Florida]]
[[Category:2010 in sports in Florida|East-West Shrine Game]]
[[Category:2010s in Orlando, Florida]]
[[Category:January 2010 sports events]]