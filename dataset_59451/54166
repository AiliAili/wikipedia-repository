{{Use dmy dates|date=April 2015}}
{{Use Australian English|date=April 2015}}
{{Refimprove|date=November 2013}}
{{Infobox Radio station
|name               = Cruise 1323
|image              = [[File:Cruise 1323 logo 2012.png|150px|center|4KQ]]
|city               = [[Adelaide]], [[South Australia]]
|area               = Adelaide 
|branding           = 
|slogan             = ''Adelaide's Classic Hits''
|frequency          = 1323 [[kilohertz|kHz]] [[AM broadcasting|AM]] (also on DAB+)
|translator         = 
|repeater           = 
|airdate            = {{Start date|df=yes|1930|8|2}}<ref>{{cite web|url=http://www.acma.gov.au/webwr/_assets/main/lib100052/lic022_commercial_radio_broadcasting_licences.pdf |title=Archived copy |accessdate=2015-07-15 |deadurl=yes |archiveurl=https://web.archive.org/web/20160303173315/http://acma.gov.au/webwr/_assets/main/lib100052/lic022_commercial_radio_broadcasting_licences.pdf |archivedate=3 March 2016 |df=dmy }}</ref>
|share              = 
|share as of        = 
|share source       = 
|format             = [[Oldies]]
|language           = English
|power              = 2,000 [[watt]]s
|erp                = 
|haat               = 
|class              = 
|facility_id        = 
|coordinates        ={{coord|34.907178|S| 138.593063|E|format=dms|display=inline,title}}
|callsign_meaning   = 
|former_callsigns   = 
|former_frequencies = 
|affiliations       = [[Pure Gold Network]]
|owner              = [[Australian Radio Network|Australian Radio Network Pty Ltd]]
|licensee           = ARN Communications Pty Ltd
|sister_stations    = 
|webcast            = [http://player.arn.com.au/cruise1323.aspx]
|website            = [http://www.cruise1323.com.au/ Official website]
}}

'''Cruise 1323''' (call sign: '''5DN''') is one of [[Adelaide]]'s longest running radio stations. In its 80+ years it has changed considerably. It was the first commercial station to begin broadcasting in [[South Australia]].

== History ==

Radio 5DN began operating as an experimental station in 1924 on a wavelength of "about 200 metres" (1500&nbsp;kHz),<ref>{{cite news |url=http://nla.gov.au/nla.news-article63796419 |title=Queries Answered. |newspaper=[[The Mail (Adelaide)|The Mail]] |location=Adelaide |date=27 September 1924 |accessdate=28 July 2014 |page=24 |publisher=National Library of Australia}}</ref> with official services beginning on 24 February 1925. It began as the hobby of businessman and industrialist Ernest James Hume, a brother of [[Walter Reginald Hume]], but soon grew to absorb the time and energy of the Hume family.

Hume bought his transmitter, and some other equipment from radio pioneer Lance C. Jones, who ran station 5BQ in [[Westbourne Park, South Australia|Westbourne Park]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article57882504 |title=Wireless for All |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=4 October 1924 |accessdate=28 July 2014 |page=14 |publisher=National Library of Australia}}</ref> The purchase included the original call sign: "5Don N", which had been allocated to Jones in 1923. The origin of that call sign has been lost in the mists of time, but it may be the result of an early [[spelling alphabet]] (A for Arthur, C for Charlie, D for Don, etc.).

The transmitter and studios were set up in the Hume family mansion, Peltonga, on Park Terrace (now [[Greenhill Road]]), in the suburb of [[Parkside, South Australia|Parkside]]. Hume installed landlines from the [[Elder Conservatorium]] and [[Adelaide University]], allowing live broadcasts of concerts and lectures.

The original voice of 5Don N was E.J. Hume's wife [[Stella Hume|Stella]]. As well as being an announcer, she also acted as program organiser, studio director, technical operator, pianist and producer. Their eldest son, Ernest Jeremy Hume took over as technical operator from Jones.<ref>{{cite news |url=http://nla.gov.au/nla.news-article54918888 |title=Wireless for All |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=13 June 1925 |accessdate=28 July 2014 |page=7 |publisher=National Library of Australia}}</ref>

Initially, 5DN broadcast only a few hours a day, three days a week, but as the number of listeners grew, broadcasting hours were extended until it became a full-time 24 hours a day radio station. By 1928, transmissions from 5DN were accessed in almost every country.<ref>{{cite news |url=http://nla.gov.au/nla.news-article57040078 |title=Capable Radio Engineer: Career of Mr. Ernest Hume, Jun. |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=19 April 1928 |accessdate=4 May 2014 |page=12 |publisher=National Library of Australia}}</ref>

5DN was one of the original members of the Macquarie Broadcasting Network, which provided programming and advertising sales services to its stations. Among the Macquarie stars to feature on 5DN in the early years were Australia's first real radio "star", [[Jack Davey]], and [[Roy Rene]] and his character "Mo", after whom Australia's entertainment awards, the [[Mo Awards]], are named.

Big local names of 5DN in the 40s and 50s included long time Breakfast announcer,Mel Cameron, Vic Braham, Ron Sullivan, Phyllis Pullman, Alan Sanders and Barry Hall. Cameron was on the air at 5DN for more than 35 years, making him one of South Australia's best known radio voices.

One of the station's most popular locally produced programs of the late 1940s and 50s was "Under the Stars", written for 20 years by 5DN's chief scriptwriter, Merv Hill. He also single handedly wrote the scripts for "Radio Canteen". Begun as an experiment in Saturday night live entertainment, the show ran for 13 years, featuring virtually every leading entertainer in South Australia.

Other shows heard on 5DN around the same time included "Quiz Kids", the "Cashmere Bouquet Show", "Doctor Mac" and "Music While You Work", many of them originating from the Macquarie Network production studios in Sydney.

During the late 30s 5DN kept its listeners informed, with regular news broadcasts, establishing South Australia's first independent radio news service in 1938. It continued its news service during the war years, and in the 1950s continued its own local news service, with the backing of Macquarie National News, which became the largest radio news network in Australia.

Sport has also played a large part in 5DN's programming. The station became the first in South Australia to introduce "ball by ball" commentaries on cricket, in 1930. The station also covered motor racing in the 1930s, with live broadcasts of the Lobethal Grand Prix events. In later years, football commentary, horse and greyhound racing were also major elements of 5DN's sports coverage.

In 1969, 5DN introduced Adelaide's first Open Line talkback programme, beginning the move to the News/Talk format that saw the station dominate the ratings in the 1970s and 80s. The pioneer of the format on 5DN was the Reverend Neil Adcock, on his daytime talkback show.

Jeremy Cordeaux joined the station in 1976 to host the daytime show, with Neil Adcock moving to a Sunday night spot. But for a long time 5DN's ratings record was held by late night Open Line host, Roger Mac. His ratings often exceeded the figures other stations recorded for their prime time programs.

In 1983/84 5DN scratched the races and broadcasts were moved to Beautiful Music station [[FIVEaa]] which had been bought by the TAB for this purpose.

Led by Paul Linkson as General Manager other broadcasters who featured on 5DN 972 from 1980 include Leigh Hatcher, Vincent Smith, Murray Nicoll, Gary Rivett, [[Kevin Crease]], Ken Dickin, Judith Barr, [[Ken "KG" Cunningham|Ken Cunningham]], Gary Bau, [[David Hookes]], Trevor Ford, Bob Byrne, Noel Yeates, Geoff Jay, Alex Zastera, Gerard Stone, Dave Waterman, Joan Hanger, Ray Fewings, Andrew Pearce, Chris Glenn, Dom Rinaldo, Murray "Buzzard" Olds, Terry Clark, Nan Witcomb and Jeff Medwell. Australia Overnight emanated from 2GB in Sydney and was hosted by Owen Delaney (M-F) and Les Solomon at weekends.

Until the sale of the [[Macquarie Radio Network]] by Warwick Fairfax and the station's affiliation with [[2UE]], [[Macquarie National News]] – [[2GB]], [[3AW]], [[4BH]], 5DN and [[6PR]] – was provided by Colin Tyrus (News Director), Murray Olds (News Director and later breakfast personality), Mark Smith (News Director), Kevin Donnellan, Verity Webb, Simon Francis, Graham Warburton, Neil Wiese, Tim Sauer, Jim Snedden, Eric Wisgard, Craig Middleton, Shaun Fewings, Shane Sody, Graham Cairns, Anne Fulwood, Amanda Bachmann, Michelle Weidenhofer, Alan Murrell, Heidi Koch and Narelle Hill.

Sport was provided by Kim Anderson and Jonathan Rivett.

5DN's 972AM frequency is today utilised by [[ABC NewsRadio]].

=== The move to FM ===

In 1990, 5DN moved to [[Frequency modulation|FM]], and changed its on-air identity to Radio 102 FM. The high rating News/Talk/Sport format was dropped, staff were made redundant and the station changed to a [[Classic Hits]] music format managed and programmed by the same people who had overseen 5DN.

'Radio 102FM – Sounds Like Adelaide to Me' was not a success in Adelaide. Poor ratings led the station change its position to compete with [[SAFM]], the market dominator at the time. The station then changed to an adult rock format in early 1992, and adopted the on air identifier X102.

X102 failed to derail the dominant [[SAFM]] and continued to operate at a financial loss. Later 1323 5AD became available for sale and was purchased by Montclair Investments. The owners of Montclair, which included former 5DN personality and First Radio Limited investor Jeremy Cordeaux and former 5DN Station Manager Sue Fraser, subsequently made an offer to the by now defeated owners of the 102.3 FM frequency.  The transaction proceeded and 5AD was immediately simulcast on the FM frequency from July 1993. It proved to be an instant success in both ratings and advertising terms with the station continuing to simulcast on the AM frequency.

5AD dominated the ratings during the simulcast period but was eventually instructed to end the simulcast by the regulatory authorities. This left the owners with a 'spare' commercial licence on the AM band but, as the 5AD brand name had moved to FM, the AM station was without an identity. So Radio 1323 was launched with a format featuring old 5DN personalities and a mixture of talk and [[Easy Listening]] music.

Soon Radio 1323 was rebranded as 5DN but despite the name change it could and would never retain its former leadership position as South Australia's number one choice for news, talk and sport. Behind the scenes the station was literally a shell of its once proud former self with only a small number of people technically employed by 5DN with the remainder (management, news, technical, sales) employed by 5ADFM.

[[FIVEaa]] and [[891 ABC Adelaide]] became the dominant news/talk stations in Adelaide after 5DN converted to FM in 1990 and few listeners were convinced to return to 5DN on 5AD's old frequency.

=== The SEN experiment ===

The holding company for the two stations was later bought by the [[Australian Radio Network]] (ARN). However the reincarnation failed to recapture the glory days of the late 1970s and '80s and the Australian Radio Network closed it down in December 2004, leasing the licence to [[Melbourne]] based broadcaster [[SEN 1116|SEN]], the Sports and Entertainment Network.

The radio listener share of 1323 AM dropped from 6.7% in December 2004 to almost Nil in May 2005.  SEN encountered financial difficulties, and relinquished control of the licence, handing it back to ARN.  After regaining control, ARN began a [[simulcast]] of its FM station, now rebranded as Mix 102.3.

=== Cruise begins ===

In November 2005 the AM frequency was again separated from the FM program, and was rebranded as Cruise 1323, playing easy listening music. The first Adelaide ratings survey of 2006 gave Cruise 1323 8th place and a 5.2% share of the total audience. By the first survey of 2007, Cruise had moved to 6th place and 7.8% of the total audience overtaking the National youth station [[Triple J]] and commercial FM Rock station [[Triple M Adelaide]]. In March 2012 the station joined the [[Classic Hits Network]] alongside [[101.7 WSFM]] Sydney, [[Gold 104.3]] Melbourne and [[4KQ]] Brisbane.<ref>[http://jocksjournal.com Jocksjournal] (2012-03-20). [http://www.jocksjournal.com/main.html]. Accessed on 2012-03-20.</ref>

At September 2016 the Cruise 1323 Weekdays On-Air Line Up is:

6am-10am John Dean

10am-1pm Dom Rinaldo

1pm-4pm Craig Huggins

4pm-7pm Mark Elliston

==References==
{{reflist}}
* Towler, David J. ''The First Sixty Years, 1924–1984, 5DN'', 5DN Macquarie, 1984.
* Walker, R.R. ''The Magic Spark, 50 Years of Radio in Australia'', The Hawthorn Press, 1973.

==External links==
*[http://www.cruise1323.com.au/ Cruise1323 Web Site]

{{Australian Radio Network}}
{{Adelaide Radio}}



[[Category:Radio stations in Adelaide]]
[[Category:Radio stations established in 1924]]
[[Category:Classic hits radio stations in Australia]]
[[Category:Australian Radio Network]]