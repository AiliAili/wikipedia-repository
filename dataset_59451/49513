{{Infobox software
| name                   = SharpDevelop
| screenshot             = SharpDevelop.png
| caption                = <!--Something other that the obvious "Screenshot of SharpDevelop" please.-->
| developer              = IC#Code Team
| latest release version = 5.1.0
| latest release date    = {{Start date and age|2016|04|14|df=y}}
| latest preview version = 5.1 Release Candidate  
| latest preview date    = {{Start date and age|2015|07|14|df=yes}}<ref>{{cite web|url=http://community.sharpdevelop.net/forums/t/22254.aspx|title=SharpDevelop 5.1 Release Candidate|work=SharpDevelop Community|accessdate=September 23, 2015}}</ref>
| programming language   = [[C Sharp (programming language)|C#]]
| operating system       = [[Microsoft Windows]]
| genre                  = [[Integrated development environment]]
| license                = [[MIT License]]
| website                = {{URL|www.icsharpcode.net/OpenSource/SD/}}
}}

'''SharpDevelop''' (also styled as '''#develop''') is a [[free and open source]] [[integrated development environment]] (IDE)<ref name= "b7" /><ref name = "b9" /> for the [[.NET Framework]],<ref name="b11"/> [[Mono (software)|Mono]],<ref name="b6" /> [[Gtk Sharp|Gtk#]]<ref name="b6" /> and [[Glade Sharp|Glade#]] platforms.<ref name= "b1" /> It supports development in [[C Sharp (programming language)|C#]],<ref name= "b5" /> [[Visual Basic .NET]],<ref name = "b4" /><ref name="b14" /> [[Boo (programming language)|Boo]], [[F Sharp (programming language)|F#]],<ref name="b2" /> [[IronPython]] and [[IronRuby]] [[programming language]]s.<ref name="sdfeatures">[http://www.icsharpcode.net/OpenSource/SD/Features.aspx "SharpDevelop Features"]. ICSharpCode website.</ref>

It was designed as a free and lightweight alternative to [[Microsoft Visual Studio]], and contains an equivalent feature for almost every essential Visual Studio Express feature, including features for project management, code editing, application compiling and debugging.<ref name= "b12" /><ref>[http://visualbasic.about.com/od/usingvbnet/l/aa122602a.htm "A Free 'Visual Studio' for VB.NET"]. About.com, Visual Basic.</ref> To allow for easy project migration, SharpDevelop works natively with Visual Studio project and code files. It is able to compile applications for .NET Framework version 2.0, 3.0, 3.5, 4.0 and the .NET Compact Framework 2.0 and 3.5.<ref>[http://www.codeproject.com/KB/windows/WiMoSansVS.aspx "Windows Mobile Development Without Visual Studio"]. The Code Project.</ref>

Although not as widespread as the Visual Studio line of products,{{Citation needed|date=September 2011}} SharpDevelop is fairly popular and {{as of |2013 |lc=1}} has been downloaded at least 8 million times worldwide,<ref>[http://sourceforge.net/projects/sharpdevelop/files/stats/timeline?dates=2001-01-04+to+2013-12-30 "SharpDevelop Download Stats 2001-01-04 to 2013-12-30"]. SourceForge. (8.1M downloads). Retrieved December 30, 2013.</ref><ref>[http://sharpdevelop.codeplex.com/stats "SharpDevelop (Stats for the project lifetime)"]. CodePlex. (40K downloads). Retrieved December 30, 2013.</ref> and has been documented in the book ''Dissecting a C# Application: Inside SharpDevelop'' (2003) written by the core development team and published by [[Wrox Press]].<ref name="Holm, Kruger, Spudia" />

SharpDevelop is written entirely in C#<ref name="b3" /> and consists of about 20 components that integrate to form the application. The [[source code editor]] component is known as AvalonEdit<ref>[http://wiki.sharpdevelop.net/AvalonEdit.ashx "AvalonEdit"]. SharpDevelop wiki.</ref> and can be used by other applications.<ref name="sdfeatures" /><ref>[http://www.codeproject.com/KB/edit/AvalonEdit.aspx "Using AvalonEdit (WPF Text Editor)"] The Code Project.</ref> Early in its development the project was split for [[Mono (software)|Mono]] and Gtk# development into the [[MonoDevelop]] project.

== Features ==

SharpDevelop includes features very similar to those found in Visual Studio, or Delphi/Kylix, including a [[Graphical user interface|GUI]] Designer, Code/Design views, [[Syntax highlighting]], [[Autocomplete|Auto completion]] menus (similar to [[IntelliSense]]<ref name="b3" />) the ability to compile and debug form/console [[.NET Framework]] applications,<ref name="b1" /> the New Project wizard, Toolbars, Menus, Panels, and a Panel docking system.<ref name="Holm, Kruger, Spudia" />{{Rp|7}}<ref name="b10" /><ref>Ward, Matt (July 5, 2006). [http://community.sharpdevelop.net/blogs/mattward/articles/FeatureTour.aspx "Feature Tour"]. SharpDevelop.net community.</ref><ref>Ward, Matt (July 12, 2006). [http://community.sharpdevelop.net/blogs/mattward/articles/VisualStudioExpressComparison.aspx "Visual Studio Express and SharpDevelop Compared"]. SharpDevelop.net community.</ref>

SharpDevelop integrates [[Graphic User Interface]] Designers for the C#,<ref name="b1" /> VB.NET, Boo, and the IronPython and IronRuby languages, using the following GUI technologies:<ref name="sdfeatures" />
* [[Windows Forms]]<ref name="b3" /><ref>[http://community.sharpdevelop.net/blogs/mattward/articles/FeatureTourVisuallyDesigningForms.aspx "Visually Designing Forms"]. SharpDevelop Feature Tour.</ref>
* [[Windows Presentation Foundation]] (WPF)
* [[Entity Framework]]

SharpDevelop has in-built [[code refactoring]] tools,<ref>[http://community.sharpdevelop.net/blogs/mattward/articles/FeatureTourRefactoring.aspx "Refactoring"], SharpDevelop Feature Tour.</ref><ref>[http://community.sharpdevelop.net/blogs/mattward/articles/FeatureTourCodeGeneration.aspx "Code Generation"]. SharpDevelop Feature Tour.</ref> such as to create or modify functions and properties. It has an integrated [[debugger]]<ref name="b13" /> that allows for stepping, viewing values of objects in memory, and [[breakpoint]]s.<ref>[http://community.sharpdevelop.net/blogs/mattward/articles/FeatureTourDebugging.aspx "Debugging"]. SharpDevelop Feature Tour.</ref> It also includes [[ILSpy]], a decompiler for IL assemblies, so debugging third-party assemblies without source code is also possible.{{citation needed|date=July 2012}}

It also includes functionality for:
* External [[Component Object Model|COM]] and [[ActiveX]] components
* [[Code analysis]] ([[FxCop]])<ref>[http://www.codeproject.com/KB/macros/sharpdev_community_annota.aspx "SharpDevelop in the Real World: Code Annotations"]. The Code Project.</ref>
* [[Unit testing]] ([[NUnit]])<ref name="b1" /><ref name="b8" /><ref>[http://www.codeproject.com/Articles/34658/SharpDevelop-NUnit-and-Visual-Studio-Express "SharpDevelop, NUnit, and Visual Studio Express"]. The Code Project.</ref>
* [[Code coverage]] (PartCover)
* Profiler
* [[Apache Subversion|Subversion]] (TortoiseSVN)
* [[Git (software)|Git]]
* [[Mercurial]]
* [[StyleCop]] addin
* Documentation generation ([[Sandcastle (software)|Sandcastle]], SHFB)
* Plugins<ref>[http://www.codeproject.com/KB/cs/ICSharpCodeCore.aspx "Building Applications with the SharpDevelop Core"]. The Code Project.</ref><ref>[http://www.codeproject.com/KB/cs/LineCounterSDAddIn.aspx "Line Counter - Writing a SharpDevelop Add-In"]. The Code Project.</ref>

== History ==
On the 11th September 2000 Mike Kruger launched the project, while testing the first public release of .Net Framework 1.0.
At that time there was no IDE publicly available. So he decided to write a code editor to run the compiler.<ref>[http://community.sharpdevelop.net/blogs/christophwille/archive/2010/09/11/happy-anniversary-10-years-of-sharpdevelop.aspx "Happy Anniversary - 10 Years of SharpDevelop"]. 10 Years of SharpDevelop - Christoph Wille</ref>

== See also==

{{Portal|Free software}}
* [[MonoDevelop]]
* [[Microsoft Visual Studio]]
* [[Comparison of integrated development environments]]
* [[Software development kit]]
* {{Section link|.NET Framework|Standardization and licensing}}

== References ==

{{Reflist|30em|refs=
<ref name="b1">Avery, James; Holmes, Jim (2006). [https://books.google.com/books?id=5ISfkblE9AUC&lpg=PA272&dq=SharpDevelop&pg=PA270#v=onepage&q=SharpDevelop&f=false ''Windows Developer Power Tools'']. O'Reilly. p. 272.</ref>
<ref name="b2">Pickering, Robert (2009). [https://books.google.com/books?id=UObTv4SB_5UC&lpg=PA11&dq=SharpDevelop&pg=PA11#v=onepage&q=SharpDevelop&f=false ''Beginning F#''], Apress. p. 11.</ref>
<ref name= "b3">Troelsen, Andrew (2010). [https://books.google.com/books?id=rgloOPCa3zoC&lpg=PA50&dq=SharpDevelop&pg=PA50#v=onepage&q=SharpDevelop&f=false ''Pro C# 2010 and the .NET 4.0 Platform, (5th Edition)'']. Apress. p. 50.</ref>
<ref name="b4">Troelsen, Andrew; Agarwal, Vidya Vrat (2010). [https://books.google.com/books?id=xLo1o9wHMh8C&lpg=PA701&dq=SharpDevelop&pg=PA701#v=onepage&q=SharpDevelop&f=false ''Pro VB 2010 and the .NET 4 Platform'']. Apress. p. 701.</ref>
<ref name="b5">Sphar, Chuck; Davis, Stephen R. (2008). [https://books.google.com/books?id=YJ-Ry6HM7PkC&lpg=PA3&dq=SharpDevelop&pg=PA3#v=onepage&q=SharpDevelop&f=false ''C# 2008 for Dummies''], For Dummies. p. 3.</ref>
<ref name = "b6">Dumbill, Edd; Bornstein, Niel M. (2004). [https://books.google.com/books?id=HyszoedfP3MC&lpg=PA13&dq=SharpDevelop&pg=PA13#v=onepage&q=SharpDevelop&f=false ''Mono: a Developer's Notebook'']. O'Reilly. p. 13</ref>
<ref name= "b7">Hewitt, Eben (2009). [https://books.google.com/books?id=W2XrQRMIEd4C&lpg=PA624&dq=SharpDevelop&pg=PA624#v=onepage&q=SharpDevelop&f=false ''Java SOA Cookbook'']. O'Reilly. p. 624.</ref>
<ref name="b8">Sempf, Bill; Sphar, Charles; Davis, Stephen R. (2010). [https://books.google.com/books?id=zUvtxtAEQAIC&lpg=PA7&dq=SharpDevelop&pg=PA7#v=onepage&q=SharpDevelop&f=false ''C# 2010 All-in-One For Dummies'']. John Wiley and Sons. p. 7.</ref>
<ref name="b9">Jones, Bradley. (2001). [https://books.google.com/books?id=uA9I6817GM4C&lpg=PA15&dq=SharpDevelop&pg=PA15#v=onepage&q=SharpDevelop&f=false ''Sams Teach Yourself C# in 21 Days'']. Sams Publishing. p. 15.</ref>
<ref name="b10">Cabrera, Harold; Bagnall, Brian; Faircloth, Jeremy (2002). [https://books.google.com/books?id=SYa-IVsfEWwC&lpg=PA45&dq=SharpDevelop&pg=PA45#v=onepage&q=SharpDevelop&f=false ''C# for Java Programmers'']. Syngress. 2002. p. 45.</ref>
<ref name="b11">{{Citation | last1 = Gunderloy | first1 = Michael ‘Mike’ | last2 = Jorden | first2 = Joseph L | year = 2006 | url = https://books.google.com/books?id=If_eQIdgy7IC&lpg=PA580&dq=SharpDevelop&pg=PA581#v=onepage&q=SharpDevelop&f=false | title = Mastering Microsoft SQL Server 2005 | publisher = Wiley | place = India | page = 580}}.</ref>
<ref name="b12">Arking, Jon; Millett, Scott (2010). [https://books.google.com/books?id=jTY3qAAvxiUC&lpg=PT32&dq=SharpDevelop&pg=PT32#v=onepage&q=SharpDevelop&f=false ''Professional Enterprise .NET'']. John Wiley and Sons.</ref>
<ref name="b13">{{Citation | last = Kofler | first = Michael | year = 2002 | url = https://books.google.com/books?id=aeQU0Hovu4EC&lpg=PA72&dq=SharpDevelop&pg=PA72#v=onepage&q=SharpDevelop&f=false | title = Visual Basic .NET | publisher = Pearson | place = Germany | page = 72 | language = German}}.</ref>
<ref name = "b14">Kettermann, Uwe; Rohde, Andreas (2004). [https://books.google.com/books?id=dw_5MkizvgoC&lpg=PA18&dq=SharpDevelop&pg=PA18#v=onepage&q=SharpDevelop&f=false ''Spiele effektiv programmieren mit VB.net und DirectX'']. Springer. p. 18. (German).</ref>
<ref name="Holm, Kruger, Spudia">{{Citation |title= Dissecting a C# Application: Inside SharpDevelop | last1 = Holm | first1 = Christian | last2 = Kruger | first2 = Michael ‘Mike’ | last3 = Spuida | first3 = Bernhard |year= 2003|publisher= [[Wrox Press]]/[[Apress]] |isbn= 978-1-86100-817-6 | archiveurl = https://web.archive.org/web/20070120003019/http://www.apress.com/free/content/Dissecting_A_CSharp_Application.pdf | archivedate = 2007-01-20 | url = http://www.apress.com/free/content/Dissecting_A_CSharp_Application.pdf | format = [[PDF]]}}</ref>
}}

== External links ==
* {{official website|www.sharpdevelop.com/OpenSource/SD}}
* [http://sharpdevelop.codeplex.com/ SharpDevelop at CodePlex]
* {{sourceforge|sharpdevelop}}

{{Integrated development environments}}
{{.NET Framework}}

[[Category:.NET programming tools]]
[[Category:C Sharp software]]
[[Category:Free software programmed in C Sharp]]
[[Category:Free integrated development environments]]
[[Category:Windows-only free software]]
[[Category:Software using the LGPL license]]