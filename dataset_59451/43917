{|{{Infobox Aircraft Begin
 | name= Yak-200
 | image=YakovlevYak-200.jpg
 | caption=The Yak-200 prototype after modifications
}}{{Infobox Aircraft Type
 | type=Multi-engine [[trainer (aircraft)|trainer]]
 | national origin=[[Soviet Union]]
 | manufacturer=[[Yakovlev]]
 | designer=
 | first flight=10 April 1953
 | introduced=
 | retired=
 | status=Cancelled
 | primary user=
 | number built=1 + 1 Yak-210
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Yakovlev Yak-200''' was a prototype [[Soviet Union|Soviet]] multi-engine [[trainer (aircraft)|trainer]] built during the 1950s. A modified version was built as the '''Yak-210''' for navigator training, but only one example of each was built before the program was cancelled in 1956.

==Development==
The [[Yakovlev]] [[OKB]] began work in February 1951 on an aircraft that could be used as a cheap bomber trainer as the Yak-UTB (''oochebnotrenirovochnyy bombardirovshchik'' — bomber trainer), but this was quickly split into two closely related versions, the Yak-200 for pilot training and the Yak-210 for navigator/bombardier training. The primary difference between them was in equipment; the Yak-200 lacked all of the specialized navigation and bombardier gear.<ref name=g8>Gordon, p. 248</ref>

The Yak-200 was a mid-winged, twin-engined monoplane with a tricycle [[Landing gear|undercarriage]]. The metal-skinned, semi-[[monocoque]] fuselage was built in three sections with side-by-side seating for the crew. The nose was glazed with an optically flat panel and lacked a seat or any equipment. The control surfaces of the tail were fabric-covered although the tail itself was metal-skinned. The one-[[spar (aviation)|spar]] metal wing was made in three pieces with detachable trapezoidal outer panels. The [[aileron]]s were covered with fabric, as were the [[flap (aircraft)|flap]]s in the outer wing panels, but the flaps in the center section were metal-skinned. The main undercarriage legs retracted forward into the engine [[nacelle]]s, while the nose leg retracted backwards. Two {{convert|700|hp|kW|adj=on}} [[Shvetsov ASh-21]] [[radial engine]]s powered the Yak-200 and drove [[Controllable pitch propeller|variable-pitch]] VISh-11V-20A propellers.<ref>Gordon, pp. 246–47</ref>

The Yak-210 carried a full suite of navigation equipment with which to train navigators. The main distinguishing characteristic between it and the Yak-200 was an external [[radome]] under the rear fuselage for the PSBN-M (''pribor slepovo bombometahniya i navigahtsii''—blind-bombing and navigational device) search/bomb-aiming [[radar]] as was fitted on the [[Ilyushin Il-28]]. An OPB-6SR (''opticheskiy pritsel bombardirovochnyy''—optical synchronized bombsight) and an AFA-BA-40 camera were also fitted; the latter could tilt 15° aft to record bomb impacts.<ref name=g8/> All this equipment weighed {{convert|860.5|kg|abbr=on}} which forced the fuel load to be reduced by {{convert|235|kg|abbr=on}} in compensation. Seats were fitted in the nose for the trainee navigator and his instructor.<ref name=g8/>

The Yak-200 prototype made its first flight on 10 April 1953 and it underwent its State acceptance trials between 29 July and 10 September. These revealed a number of problems including poor handling, insufficient longitudinal stability and significant changes in trim as engine power was changed. Other problems included the lack of [[Deicing|anti-icing]] devices for the windshield and the propeller blades. Remedies included adding a {{convert|35|cm|abbr=on}} extension in the fuselage and a dorsal [[fillet (mechanics)|fillet]] to correct the stability issues. The wings were raised by {{convert|10|cm|abbr=on}} and their [[dihedral (aircraft)|dihedral]] was reduced. These changes moved the aircraft's [[Center of gravity of an aircraft|center of gravity]] forward and it became simple and pleasant to fly, even though its empty weight increased by {{convert|120|kg|abbr=on}} and its gross weight by {{convert|195|kg|abbr=on}} which did include some additional fuel.<ref>Gordon, pp. 247–48</ref>

The Yak-210 first flew on 1 August 1953 and it received some of the improvements given to the Yak-200 including the dorsal fin. Its radome was changed from its initial oval shape to a teardrop during testing.<ref name=g8/> It could carry {{convert|300|kg|abbr=on}} of practice bombs.<ref name=g3>Gunston, p. 483</ref>

All these modifications required time and the [[Soviet Air Forces|VVS]] ultimately decided that the Il-28U trainer met its requirements, despite the extra costs involved, but the program was cancelled.<ref name=g3/>

==Specifications (Yak-200 before modifications) ==

{{Aircraft specs
|ref=Gordon et al., ''OKB Yakovlev: A History of the Design Bureau and its Aircraft''
|prime units?=met

<!--
        General characteristics
-->
|crew=two
|capacity=
|length m=12.95
|length ft=
|length in=
|length note=
|span m=17.45
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=36
|wing area sqft=
|wing area note=
|airfoil=
|empty weight kg=3910
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=4715
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Shvetsov ASh-21]]
|eng1 type=seven-cylinder, air-cooled, single-row, [[radial engine]]
|eng1 kw=522
|eng1 hp=<!-- prop engines -->
|eng1 note=
|power original=
|thrust original=
|more power=
|prop blade number=2
|prop name=[[Controllable pitch propeller|variable-pitch]] VISh-11V-20A 
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=400
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1280
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=7160
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2
|disk loading lb/sqft=
|disk loading note=
|more performance=*'''Time to height:''' 2.3 minutes to {{convert|1000|m|0}}
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|2}}

==Bibliography==
{{commons category|Yakovlev aircraft}}
{{refbegin}}
* {{cite book|last=Gordon|first=Yefim|author2=Komissariov, Dmitry and Sergey  |title=OKB Yakovlev: A History of the Design Bureau and its Aircraft|publisher=Midland Publishing|location=Hinckley, England|year=2005|isbn=1-85780-203-9}}
* {{cite book|last=Gunston|first=Bill|title=The Osprey Encyclopedia of Russian Aircraft 1875-1995|publisher=Osprey|location=London|year=1995|isbn=1-85532-405-9}}
{{refend}}

<!-- ==External links== -->

{{Yakovlev aircraft}}

[[Category:Soviet military trainer aircraft 1950–1959]]
[[Category:Yakovlev aircraft|Yak-200]]
[[Category:Abandoned military aircraft projects of the Soviet Union]]