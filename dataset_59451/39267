{{Infobox single
|Name=Candida
|Cover= Candida single cover by the group Dawn.jpg
|Artist=[[Tony Orlando and Dawn|Dawn]]
|from Album=[[Candida (album)|Candida]]
|B-side="Look At..."
|Released= July 1970
|Recorded=   
|Format=[[vinyl record|7"]] [[Single (music)|single]]
|Genre= Pop / Rock<ref name=Viglione>{{harvnb|Viglione}}</ref>
|Length=3:02
|Label=[[Bell Records]]
|Writer=[[Toni Wine]] and [[Irwin Levine]]
|Producer=[[Hank Medress]] and [[Dave Appell]]
|Last single =
|This single = "'''Candida'''" <br> (1970)
|Next single = "[[Knock Three Times]]" <br> (1971)
}}

"'''Candida'''" was the first single released by the American [[pop music]] group [[Tony Orlando and Dawn|Dawn]], with vocals by [[Tony Orlando]], in July 1970. The song, written by [[Irwin Levine]] and [[Toni Wine]], was produced by [[Dave Appell]] and [[Hank Medress]] for [[Bell Records]]. Appell and Medress originally recorded another singer on the track, but decided that a different vocal approach would be preferable. Medress then approached Orlando to do the vocals. Orlando had been a professional singer in the early 1960s, but now worked as a music publishing manager for [[Columbia Records]]. Although initially worried about losing his job at Columbia, Orlando eventually agreed to lend his voice to the track.

"Candida" became a worldwide hit, reaching number one in five countries, and the top ten in many others. It was included on Dawn's debut LP in 1970 and later appeared on several compilation albums. [[Andy Williams]], [[Jesse Winchester]], [[Ray Conniff]], and [[Bernd Spier]] are among the artists who have covered the song.

==Background and recording==
In 1970 Hank Medress of [[the Tokens]] and Dave Appell were producing a song called "Candida" for Bell Records.<ref name=Childs>{{harvnb|Childs|2011}}</ref><ref name=Warner484>{{harvnb|Warner|2006|p=484}}</ref> The composition was written by Toni Wine and Irwin Levine.<ref name=Warner484/><ref name=NorwegianCharts>{{harvnb|Dawn – Candida (Norwegiancharts.com)}}</ref> For the first recording of the song, the lead vocal was done by blues singer Frankie Paris,<ref name=Childs/> in a style reminiscent of [[the Drifters]].<ref name=Warner484/> Paris's performance was deemed unsatisfactory, and a new singer was sought for the track.<ref name=Childs/><ref name=Warner484/> Medress believed that "an ethnic feel" would suit the song well.<ref name=Orlando91>{{harvnb|Orlando|2003|p=91}}</ref> He asked his friend Tony Orlando, whose heritage is Puerto Rican and Greek, to perform its lead vocal.<ref name=Warner484/><ref name=Bronson>{{harvnb|Bronson|2003}}</ref>

{{Listen|filename="Candida" by Dawn.ogg|title="Candida"|description=Producer Hank Medress aimed for "an ethnic feel" for "Candida",<ref name=Orlando91/> which has "a lilting, sing-along groove".<ref name=hoffman/>}}

Orlando, a former professional singer, had had two top-40 hits in the USA in 1961,<ref name=hoffman>{{harvnb|Hoffman|2004|p=789}}</ref> but later moved into the music publishing business; in 1967 Columbia Records chose him to manage their publishing division, April-Blackwood Music.<ref name=hoffman/><ref>{{harvnb|Ankeny}}</ref> When Medress approached Orlando, he was reluctant to perform on a Bell Records single, as he did not want to jeopardize his job at Columbia.<ref name=Childs/><ref name=Orlando91/> Medress reassured him by saying they would use a band name for the release, and that nobody would know who the singer was.<ref name=Childs/><ref name=Orlando91/> Orlando finally agreed, partly because he believed the song would be unsuccessful and would not attract any attention.<ref name=Orlando92>{{harvnb|Orlando|2003|p=92}}</ref> He went into a studio with Appell and Medress, and sang his lead vocal over prerecorded tracks.<ref name=Orlando92/> Background vocals were done by Wine and the Tokens' Jay Siegel;<ref name=Childs/><ref name=Warner484/><ref name=Orlando94>{{harvnb|Orlando|2003|p=94}}</ref> Orlando was not present when these were recorded.<ref name=Warner484/> By different accounts, additional background singers may have included [[Ellie Greenwich]],<ref name=Warner484/> Robin Grean,<ref name=Warner484/> Leslie Miller,<ref name=Childs/> and [[Linda November]].<ref name=Orlando94/><ref group=a>Still other sources attribute the background vocals to [[Telma Hopkins]] and [[Joyce Vincent Wilson]] (see {{harvnb|Hoffman|2004|p=789}}; {{harvnb|Bronson|2003}}; {{harvnb|Ankeny}}), who were members of Dawn from some point in the early 1970s onwards (see {{harvnb|Huey}}). However, in his autobiography, Orlando recounts recruiting Hopkins and Wilson for the group after "Candida" and its follow-up single, "[[Knock Three Times]]", were recorded and released ({{harvnb|Orlando|2003|pp=95–100}}).</ref> [[Phil Margo]] and Siegel played instruments on at least one of the versions of the song.<ref name=Childs/><ref name=Warner484/> The music of Orlando's version has been described as having "a lilting, sing-along groove".<ref name=hoffman/>

==Release==
"Candida" was released as a single in July 1970 under the moniker Dawn, named after the daughter of either Jay Siegel<ref name=Bronson/> or Bell Records executive Steve Wax.<ref name=NorwegianCharts/><ref name=Warner484/> The single reached number one in Brazil,<ref name=BrazilianCharts>{{harvnb|Hits of the World (1971-01-09)|p=48}}</ref> Malaysia,<ref name=MalaysianCharts>{{harvnb|Hits of the World (1970-12-05)|p=72}}</ref> Singapore,<ref name=SingaporeanCharts>{{harvnb|Hits of the World (1970-11-14)|p=53}}</ref> Spain,<ref name=SpanishCharts>{{harvnb|Hits of the World (1971-03-13)|p=56}}</ref> and Sweden,<ref name=SwedishCharts>{{harvnb|Hits of the World (1971-02-20)|p=52}}</ref> and the top ten in Austria,<ref>{{harvnb|Hits of the World (1971-01-30)|p=56}}</ref> Canada,<ref name=CanadianCharts>{{harvnb|Top Singles -  Volume 14, No. 10, October 24, 1970}}</ref> Denmark,<ref name=SwedishCharts/> Mexico,<ref name=SpanishCharts/> New Zealand,<ref name=SingaporeanCharts/> Norway,<ref name=NorwegianCharts/> South Africa,<ref name=SingaporeanCharts/> the UK,<ref name=UKCharts>{{harvnb|Singles: Candida}}</ref> and the USA.<ref name=BillboardCharts>{{harvnb|Hot 100|p=90}}</ref> It also reached the top twenty in Australia,<ref name=Warner484/> Belgium,<ref name=BelgianCharts>{{harvnb|Dawn – Candida (Ultratop)}}</ref> and Germany.<ref name=GermanCharts>{{harvnb|Dawn – Candida (Charts.de)}}</ref> ''Billboard'' ranked the record as the No. 18 song of 1970.<ref>[[Billboard Year-End Hot 100 singles of 1970]]</ref>

Jay Warner, author of ''American Singing Groups: A History from 1940 to Today'', notes that the group the Corporation released a different version of "Candida" around the same time as Dawn's.<ref name=Warner484/> The Corporation's recording was produced by Bill and Steve James, and released on [[Musicor Records]].<ref name=Warner484/> Warner believes that this version was based on an early, slower piano-and-vocals [[Demo recording|demo]] by Toni Wine.<ref name=Warner484/> A July 1970 capsule review in ''[[Billboard (magazine)|Billboard]]'' magazine of both Dawn's and the Corporation's versions categorized the latter's recording as possessing "a strong blues and [[Tejano music|Tex-Mex]] flavor", and stated that both singles had "equal sales and chart potential".<ref name>{{harvnb|Top 60 Pop Spotlight|p=80}}</ref> However, although for a short while it seemed there might be competition between the two, the Corporation's single did not sell well.<ref name=Warner484/>

Dawn's version was released on their debut album, ''[[Candida (album)|Candida]]'', in 1970,<ref name=Viglione/> and later on the Dawn compilations ''Greatest Hits'', ''The World of Tony Orlando & Dawn'', ''The Definitive Collection'', and ''The Big Hits''.<ref>{{harvnb|Tony Orlando: Discography}}</ref> It has also appeared on various-artists compilations including ''Today's Super Hits'', ''AM Gold: 1970'', and ''Real 70's: the Polyester Hits, Disc One''.<ref>{{harvnb|Dawn Candida: Overview}}</ref>

==Covers==
[[File:Jesse Winchester Smile JazzFest 2011.jpg|thumb|right|Jesse Winchester is one of many singers who have covered "Candida".]]
Numerous musicians have covered "Candida", among them [[Andy Williams]],<ref>{{harvnb|Haney}}</ref> [[Jesse Winchester]],<ref>{{harvnb|Ruhlmann}}</ref> [[Jimmy Velvet]],<ref>{{harvnb|Jimmy Velvet Did You Know Elvis?: Overview}}</ref> [[Ray Conniff]],<ref>{{harvnb|Campbell}}</ref> and reggae artists [[Owen Gray]]<ref>{{harvnb|Birchmeier}}</ref> and [[The Pioneers (band)|the Pioneers]].<ref>{{harvnb|Various Artists Roots, Rockers, Reggae, Vol. 3: Overview}}</ref> Foreign-language versions have included recordings in Portuguese by [[the Fevers]],<ref>{{harvnb|The Fevers Para Sempre: Overview}}</ref> in Spanish by La Tropa Loca,<ref>{{harvnb|La Tropa Loca 12 Exitos: Overview}}</ref> and in German by Bata Illic<ref>{{harvnb|Bata Illic – Candida}}</ref> and [[Bernd Spier]].<ref>{{harvnb|Bernd Spier – Candida}}</ref>

==Chart performance==
{{col-begin|width=65%}}
{{col-2}}
===Weekly charts===
{| class="wikitable sortable plainrowheaders"
|-
!|Chart (1970)
!|Peak<br>position
|-
!scope=row|Australian Charts<ref name=Warner484/>
| style="text-align:center;"|13
|-
!scope=row|Belgian Charts<ref name=BelgianCharts/>
| style="text-align:center;"|11
|-
!scope=row|Brazilian Charts (Rio de Janeiro)<ref name=BrazilianCharts/>
| style="text-align:center;"|1
|-
!scope=row|[[RPM (magazine)|Canadian ''RPM'' Top Singles]]<ref name=CanadianCharts/>
| style="text-align:center;"|2
|-
!scope=row|German Charts<ref name=GermanCharts/>
| style="text-align:center;"|18
|-
!scope=row|Malaysian Charts<ref name=MalaysianCharts/>
| style="text-align:center;"|1
|-
!scope=row|New Zealand (''[[New Zealand Listener|Listener]]'')<ref>{{cite web|url=http://www.flavourofnz.co.nz/index.php?qpageID=search%20listener&qartistid=859#n_view_location |title=flavour of new zealand - search listener |website=Flavourofnz.co.nz |date= |accessdate=2016-10-03}}</ref>
| style="text-align:center;"|2
|-
!scope=row|Norwegian Charts<ref name=NorwegianCharts/>
| style="text-align:center;"|4
|-
!scope=row|Singaporean Charts<ref name=SingaporeanCharts/>
| style="text-align:center;"|1
|-
!scope=row|Spanish Charts<ref name=SpanishCharts/>
| style="text-align:center;"|1
|-
!scope=row|Swedish Charts<ref name=SwedishCharts/>
| style="text-align:center;"|1
|-
!scope=row|[[UK Singles Chart]]<ref name=UKCharts/>
| style="text-align:center;"|9
|-
!scope=row|US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref name=BillboardCharts/>
| style="text-align:center;"|3
|-
!scope=row|US [[Cashbox (magazine)|''Cashbox'' Top 100]]<ref>{{harvnb|Downey, Albert, and Hoffmann|p=87}}</ref>
| style="text-align:center;"|1
|}
{{col-2}}

===Year-end charts===
{| class="wikitable sortable"
|-
!align="left"|Chart (1970)
! style="text-align:center;"|Rank
|-
|Canada ''RPM'' <ref>{{cite web|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.3740&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.3740.gif&Ecopy=nlc008388.3740|title=Item Display - RPM - Library and Archives Canada|work=collectionscanada.gc.ca}}</ref>
| style="text-align:center;"|25
|-
|U.S. ''Billboard'' <ref>[http://www.musicoutfitters.com/topsongs/1970.htm Musicoutfitters.com]</ref>
| style="text-align:center;"|18
|-
|U.S. ''Cash Box'' <ref>{{cite web|url=http://tropicalglen.com/Archives/70s_files/1970YESP.html |title=Cash Box YE Pop Singles - 1970 |website=Tropicalglen.com |date=1970-12-26 |accessdate=2016-10-03}}</ref>
| style="text-align:center;"|24
|}
{{col-end}}

==Notes==
{{reflist|group=a}}

==Footnotes==
{{Reflist|30em}}

==References==
{{Refbegin|30em}}
* {{cite web| first=Jason|last=Ankeny| url=http://www.allmusic.com/artist/tony-orlando-mn0000016235/biography |title=Tony Orlando: Biography|work=Allmusic|accessdate=2013-08-03| ref=harv|postscript=<!--None-->}}
* {{cite web|url=http://germancharts.com/showitem.asp?interpret=Bata+Illic&titel=Candida&cat=s |title=Bata Illic – Candida|work=Germancharts.com| accessdate=2012-09-27 |ref={{SfnRef|Bata Illic – Candida}}}}
* {{cite web|url=http://germancharts.com/showitem.asp?interpret=Bernd+Spier&titel=Candida&cat=s |title=Bernd Spier – Candida|work=Germancharts.com| accessdate=2012-09-27 |ref={{SfnRef|Bernd Spier – Candida}}}}
* {{cite web| first=Jason|last=Birchmeier| url=http://www.allmusic.com/album/shook-shimmy-shake-the-anthology-mw0000211028 |title=Owen Gray Shook, Shimmy & Shake: The Anthology: Overview|work=Allmusic|accessdate=2013-07-27| ref=harv|postscript=<!--None-->}}
* {{Cite book| first=Fred|last=Bronson| url=http://books.google.ca/books?id=PgGqNrqfrsoC&pg=PT296&dq=candida+dawn&hl=en&sa=X&ei=-1TzUaqVF-POiwK37YAQ&redir_esc=y#v=onepage&q=candida%20dawn&f=false| title=The Billboard Book of Number One Hits|publisher=Random House LLC|year=2003|isbn=9780823076772|ref=harv|postscript=<!--None-->}}
* {{cite web| first=Al|last=Campbell| url=http://www.allmusic.com/album/weve-only-just-begun-love-story-mw0000219514 |title=Ray Conniff We've Only Just Begun/Love Story: Overview|work=Allmusic|accessdate=2013-07-27| ref=harv|postscript=<!--None-->}}
* {{Cite book| first=Marti Smiley|last=Childs| url=http://books.google.ca/books?id=qwOyZukEwD4C&pg=PT112&dq=%22played+instruments+and+Frankie+Paris%22&hl=en&sa=X&ei=FNb_UaKcKrD0iwL7oICYBw&redir_esc=y#v=onepage&q=%22played%20instruments%20and%20Frankie%20Paris%22&f=false | title=Echos of the Sixties|publisher=EditPros|year=2011|isbn=9781937317027|ref=harv|postscript=<!--None-->}}
* {{cite web|url=http://www.officialcharts.de/song.asp?artist=Dawn&title=Candida&country=de|title=Dawn – Candida|work=Charts.de| accessdate=2012-09-20 |ref={{SfnRef|Dawn – Candida (Charts.de)}}}}
* {{cite web|url=http://norwegiancharts.com/showitem.asp?interpret=Dawn&titel=Candida&cat=s |title=Dawn – Candida|work=Norwegiancharts.com| accessdate=2012-09-20 |ref={{SfnRef|Dawn – Candida (Norwegiancharts.com)}}}}
* {{cite web|url=http://www.ultratop.be/nl/showitem.asp?interpret=Dawn&titel=Candida&cat=s |title=Dawn – Candida|publisher=Ultratop| accessdate=2012-09-20 |ref={{SfnRef|Dawn – Candida (Ultratop)}}}}
* {{cite web|url=http://www.allmusic.com/song/candida-mt0006280054|title=Dawn Candida: Overview|work=Allmusic|accessdate=2013-08-10|ref={{SfnRef|Dawn Candida: Overview}}}}
* {{cite book|title=Cash Box pop singles charts, 1950–1993|year=1994|publisher=Libraries Unlimited|last1=Downey|first1=Pat|last2=Albert|first2=George|last3=Hoffmann|first3=Frank W|isbn=978-1-56308-316-7|ref={{SfnRef|Downey, Albert, and Hoffmann}}}}
* {{cite web| first=Shawn M.|last=Haney| url=http://www.allmusic.com/album/love-story-mw0000196805 |title=Andy Williams Love Story: Overview|work=Allmusic|accessdate=2013-07-27| ref=harv|postscript=<!--None-->}}
* {{cite web|url=http://books.google.ca/books?id=sSkEAAAAMBAJ&pg=PA53&dq=candida+dawn+billboard+%22hits+of+the+world%22&hl=en&sa=X&ei=ZjnzUerXD4n-iQKRooCABQ&redir_esc=y#v=onepage&q=candida%20dawn%20billboard%20%22hits%20of%20the%20world%22&f=false |title=Hits of the World |work=Billboard|date=1970-11-14|accessdate=2013-07-26|ref={{SfnRef|Hits of the World (1970-11-14)}}}}
* {{cite web|url=http://books.google.ca/books?id=oCkEAAAAMBAJ&pg=PA72&dq=candida+dawn+billboard+%22hits+of+the+world%22&hl=en&sa=X&ei=ZjnzUerXD4n-iQKRooCABQ&redir_esc=y#v=onepage&q=candida%20dawn%20billboard%20%22hits%20of%20the%20world%22&f=false |title=Hits of the World|work=Billboard|date=1970-12-05|accessdate=2013-07-26|ref={{SfnRef|Hits of the World (1970-12-05)}}}}
* {{cite web|url=http://books.google.ca/books?id=9AgEAAAAMBAJ&pg=PA48&dq=candida+dawn+billboard+%22hits+of+the+world%22&hl=en&sa=X&ei=0TDzUaKWCOjPigL9y4D4Bg&redir_esc=y#v=onepage&q=candida%20dawn%20billboard%20%22hits%20of%20the%20world%22&f=false |title=Hits of the World |work=Billboard|date=1971-01-09|accessdate=2013-07-26|ref={{SfnRef|Hits of the World (1971-01-09)}}}}
* {{cite web|url=http://books.google.ca/books?id=7ggEAAAAMBAJ&pg=PA56&dq=billboard+%22hits+of+the+world%22+candida&hl=en&sa=X&ei=SOAGUoqZMeTYyQGakIHYAg&ved=0CDwQ6AEwAw#v=onepage&q=billboard%20%22hits%20of%20the%20world%22%20candida&f=false |title=Hits of the World |work=Billboard|date=1971-01-30|accessdate=2013-08-10|ref={{SfnRef|Hits of the World (1971-01-30)}}}}
* {{cite web|url=http://books.google.ca/books?id=6QgEAAAAMBAJ&pg=PA52&dq=candida+dawn+billboard+%22hits+of+the+world%22&hl=en&sa=X&ei=0TDzUaKWCOjPigL9y4D4Bg&redir_esc=y#v=onepage&q=candida%20dawn%20billboard%20%22hits%20of%20the%20world%22&f=false |title=Hits of the World|work=Billboard|date=1971-02-20|accessdate=2013-07-26|ref={{SfnRef|Hits of the World (1971-02-20)}}}}
* {{cite web|url=http://books.google.ca/books?id=_QgEAAAAMBAJ&pg=PA56&dq=billboard+%22hits+of+the+world%22+candida&hl=en&sa=X&ei=SOAGUoqZMeTYyQGakIHYAg&ved=0CDQQ6AEwAQ#v=onepage&q=billboard%20%22hits%20of%20the%20world%22%20candida&f=false |title=Hits of the World|work=Billboard|date=1971-03-13|accessdate=2013-07-26|ref={{SfnRef|Hits of the World (1971-03-13)}}}}
* {{Cite book|first=Frank|last=Hoffman| url=http://books.google.ca/books?id=xV6tghvO0oMC&pg=PA789&dq=candida+dawn&hl=en&sa=X&ei=M1bzUavUMsHNiwLgmIEw&redir_esc=y#v=onepage&q=candida%20dawn&f=false | title=Encyclopedia of Recorded Sound, Volume 1 |publisher=CRC Press| year=2004|isbn=9780203484272|ref=harv|postscript=<!--None-->}}
* {{cite web|url= http://books.google.ca/books?id=jCkEAAAAMBAJ&pg=RA1-PA90&lpg=RA1-PA90&dq=Billboard+Hot+100+17+Oct+1970+Candida+Dawn&source=bl&ots=QBTYTdCxPS&sig=Qtn8hUWYMCHVTHXUR3Wd9ONYFNQ&hl=en&sa=X&ei=F89XUP6IGsv2iQKCnoDABg&ved=0CEAQ6AEwBA#v=onepage&q=Billboard%20Hot%20100%2017%20Oct%201970%20Candida%20Dawn&f=false |title=Hot 100 |work=Billboard|date=1970-10-10 |accessdate=2012-09-22|ref={{SfnRef|Hot 100}}}}
* {{cite web| first=Steve|last=Huey| url=http://www.allmusic.com/artist/dawn-mn0000232720/biography |title=Dawn: Biography|work=Allmusic|accessdate=2013-08-03| ref=harv|postscript=<!--None-->}}
* {{cite web| url=http://www.allmusic.com/album/did-you-know-elvis-mw0000920370|title=Jimmy Velvet Did You Know Elvis?: Overview|work=Allmusic|accessdate=2013-07-27|ref={{SfnRef|Jimmy Velvet Did You Know Elvis?: Overview}}}}
* {{cite web| url=http://www.allmusic.com/album/12-exitos-mw0001444981 |title=La Tropa Loca 12 Exitos: Overview|work=Allmusic|accessdate=2013-07-27|ref={{SfnRef|La Tropa Loca 12 Exitos: Overview}}}}
* {{Cite book|first=Tony, with Patsi Bale Cox|last=Orlando| url=http://books.google.ca/books?id=5iF_qpH5E2IC&pg=PA96&dq=dawn+candida+halfway+to+paradise&hl=en&sa=X&ei=tcf5UaOmI4bliALe3YG4Cw&redir_esc=y#v=onepage&q=dawn%20candida%20halfway%20to%20paradise&f=false | title=Halfway to Paradise |publisher=Macmillan| year=2003|isbn=9780312319748|ref=harv|postscript=<!--None-->}}
* {{cite web| first=William|last=Ruhlmann| url=http://www.allmusic.com/album/a-touch-on-the-rainy-side-mw0000081678 |title=Jesse Winchester A Touch on the Rainy Side: Overview|work=Allmusic|accessdate=2013-07-27| ref=harv|postscript=<!--None-->}}
* {{cite web|url=http://www.officialcharts.com/search-results-album/_/Candida |title=Singles: Candida|work=Theofficialcharts.com|accessdate=2012-09-20|ref={{SfnRef|Singles: Candida}}}}
* {{cite web| url=http://www.allmusic.com/album/para-sempre-mw0000225384|title=The Fevers Para Sempre: Overview|work=Allmusic|accessdate=2013-07-27|ref={{SfnRef|The Fevers Para Sempre: Overview}}}}
* {{cite web|url=http://www.tonyorlando.com/?mod=career_history_discography|title=Tony Orlando: Discography|work=TonyOrlando.com|accessdate=2013-08-10|ref={{SfnRef|Tony Orlando: Discography}}}}
* {{cite web|url= http://www.collectionscanada.gc.ca/rpm/028020-119.01-e.php?brws_s=1&file_num=nlc008388.3703&type=1&interval=24&PHPSESSID=m89iq841abagb37ld9c0fdc1f3 |title =Top Singles - Volume 14, No. 10, October 24, 1970 | work=[[RPM (magazine)|RPM]]| date=1970-10-24|accessdate=2012-09-20|ref={{SfnRef|Top Singles -  Volume 14, No. 10, October 24, 1970}}}}
* {{cite web| url=http://books.google.ca/books?id=bykEAAAAMBAJ&pg=PA80&dq=Billboard+candida+dawn+easy+listening&hl=en&sa=X&ei=bwwDUsbfFYbBigKkiYDIDg&redir_esc=y#v=onepage&q=Billboard%20candida%20dawn%20easy%20listening&f=false | title=Top 60 Pop Spotlight| work=Billboard|date=1970-07-11|accessdate=2013-08-10|ref={{SfnRef|Top 60 Pop Spotlight}}}}
* {{cite web| url=http://www.allmusic.com/album/roots-rockers-reggae-vol-3-mw0001143957 |title=Various Artists Roots, Rockers, Reggae, Vol. 3: Overview|work=Allmusic|accessdate=2013-07-27|ref={{SfnRef|Various Artists Roots, Rockers, Reggae, Vol. 3: Overview}}}}
* {{cite web| first=Joe|last=Viglione| url=http://www.allmusic.com/album/candida-mw0000097790 |title=Dawn Candida: Overview|work=Allmusic|accessdate=2013-08-03| ref=harv|postscript=<!--None-->}}
* {{Cite book|first=Jay|last=Warner| url=http://books.google.ca/books?id=mTM_9JTeoMIC&pg=PA484&dq=candida+dawn&hl=en&sa=X&ei=p0TzUcnyMMLliAKAoYHwCQ&redir_esc=y#v=onepage&q=candida%20dawn&f=false | title=American Singing Groups: A History from 1940 to Today |publisher=Hal Leonard Corporation| year=2006|isbn=9780634099786|ref=harv|postscript=<!--None-->}}
{{refend}}

{{Tony Orlando and Dawn}}
{{good article}}

{{DEFAULTSORT:Candida (song)}}
[[Category:1970 singles]]
[[Category:Songs written by Irwin Levine]]
[[Category:Tony Orlando songs]]
[[Category:Andy Williams songs]]
[[Category:Bell Records singles]]
[[Category:Songs written by Toni Wine]]
[[Category:1970 songs]]