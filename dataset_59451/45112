{{about|the company|the channel owned by this company|TBS (U.S. TV channel)}}
{{refimprove|date=February 2013}}
{{Infobox company 
| name             = Turner Broadcasting System, Inc.
| logo             = Turner Broadcasting System 2015.svg
| image            = Atlanta-cnn-center-aerial.jpg
| image_caption    = The [[CNN Center]], which houses the headquarters of Turner Broadcasting System
| type             = [[Division (business)|Division]]<ref>http://revenuesandprofits.com/how-time-warner-makes-money/</ref>
| genre            =
| fate             = 
| predecessor      = Turner Communications Group
| successor        =
| foundation       = {{Start date and age|1970}}
| founder          = [[Ted Turner]]
| location_city    = [[CNN Center]], [[Atlanta]], [[Georgia (U.S. state)|Georgia]]
| location_country = [[United States|U.S.]]
| location         =
| locations        =
| area_served      =
| key_people       = John K. Martin (CEO & Chairman)<ref>{{cite web|url=https://pressroom.turner.com/us/john-martin#.VS_mcZTF9Ug|title=Pressroom|work=Turner}}</ref><br>David Levy (President)<ref>{{cite web|url=http://www.deadline.com/2013/08/david-levy-upped-to-president-of-turner-broadcasting-system/|title=David Levy Upped To President Of Turner Broadcasting System|author=Nellie Andreeva|work=Deadline|accessdate=20 June 2015}}</ref>
| industry         = [[Entertainment industry|Entertainment]]<br>[[Cable television]]<br>[[Mass media]]<br>[[Interactive media]]
| products         = [[CNN]]<br>[[CNN International]]<br>[[HLN (TV network)|HLN]]<br>[[TNT (TV channel)|TNT]]<br>[[Turner Classic Movies]]<br>[[Cartoon Network]]<br>[[Boomerang (TV channel)|Boomerang]]<br>[[Adult Swim]]<br>[[TruTV]]<br>[[TBS (U.S. TV channel)|TBS]]<br>[[CNN Airport]]
| services         =
| revenue          =
| operating_income =
| net_income       =
| aum              =
| assets           =
| equity           =
| num_employees    =
| divisions        = [[Turner Sports]]
| subsid           = [[Turner Broadcasting System Europe]]<br>[[Turner Broadcasting System Asia Pacific]]<br>[[Turner Broadcasting System Latin America]]<br>[[Cartoon Network Studios]]<br>Turner Private Networks<br>[[Hulu]] (10%)<br>[[Williams Street]]
| parent           = [[Time Warner]]
| homepage         = {{official URL}}
| footnotes        =
| intl             =
}}

'''Turner Broadcasting System, Inc.''' (known professionally as '''Turner Broadcasting System''' and also known simply as '''Turner''') is an American [[media conglomerate]] that is a [[Division (business)|division]] of [[Time Warner]] and manages the collection of [[cable television|cable]] [[television network]]s and properties initiated or acquired by [[Ted Turner]]. The company was founded in 1970, and merged with Time Warner on October 10, 1996. It now operates as a semi-autonomous unit of Time Warner. The company's assets include [[CNN]], [[HLN (TV channel)|HLN]], [[TBS (TV network)|TBS]], [[TNT (TV channel)|TNT]], [[Cartoon Network]], [[Adult Swim]], [[Boomerang (TV channel)|Boomerang]] and [[TruTV]]. The company's current chairman and CEO is John K. Martin.<ref>{{cite web|title=JOHN MARTIN BIO|url=https://pressroom.turner.com/us/john-martin#.VS_mcZTF9Ug|website=Turner}}</ref> The headquarters of Turner's properties are located in both the [[CNN Center]] in [[Downtown Atlanta|Downtown]] [[Atlanta]]<ref>"[http://www.turner.com/#/contact-us Contact Us]." Turner Broadcasting System.</ref> and the Turner Broadcasting campus off Techwood Drive in [[Midtown Atlanta]], which also houses [[Turner Studios]].<ref>{{cite web|url=http://www.turner.com/terms-of-use|title=TERMS OF USE|publisher=|accessdate=20 June 2015}}</ref> Across [[Interstate 75/85]] from the Techwood campus is the original home of Turner's WTBS [[superstation]] (now separated into its TBS cable network and [[Peachtree TV]]), which today houses the headquarters of [[Adult Swim]] and [[Williams Street|Williams Street Productions]].

==History==

===1970s===
In 1970, [[Ted Turner]], then owner of a successful Atlanta-based outdoor advertising company, purchased WJRJ-Atlanta, Channel 17, a small, struggling [[Ultra High Frequency]] station, and renamed it WTCG, for parent company [[Turner Communications Group]]. By careful programming acquisitions, Turner guided the station to success. During December 1976, WTCG originated the "superstation" concept, transmitting via [[satellite]] to [[cable]] systems.

On December 17, 1976 at 1:00 pm, WTCG Channel 17's signal was beamed via [[satellite]] to its four cable systems in [[Grand Island, Nebraska]]; [[Newport News, Virginia]]; [[Troy, Alabama]]; and [[Newton, Kansas]]. All four cable systems started receiving the 1948 Dana Andrews - Cesar Romero film ''[[Deep Waters (1948 film)|Deep Waters]]'' already in progress. The movie had started 30 minutes earlier. WTCG went from being a little television station to a major [[TV network]] that every one of the 24,000 households outside of the 675,000 in Atlanta was receiving coast-to-coast. WTCG became a so-called [[Superstation]] and created a precedent of today's basic [[cable television]].

[[HBO]] had gone to satellite transmissions to distribute its signal nationally in 1975, but that was a service cable subscribers were made to pay extra to receive. Ted Turner's innovation signaled the start of the basic cable revolution.

In 1979, the company changed its name to Turner Broadcasting System, Inc. (TBS, Inc.) and the call letters of its main entertainment channel to WTBS.

===1980s===
On June 1, 1980, [[CNN]] was launched at 17:00 [[Eastern Time|EST]] becoming the first 24-hour [[television news|news]] [[cable television|cable channel]]. The husband and wife team of [[David Walker (journalist)|Dave Walker]] and [[Lois Hart]] [[news presenter|news anchored]] the first [[newscast]], [[Burt Reinhardt]] the then executive [[vice president]] of [[CNN]], hired most of [[CNN]]'s first 200 employees & 25-member staff including  [[Bernard Shaw (journalist)|Bernard Shaw]], the network's first [[news anchor]].

In 1981, Turner Broadcasting System acquired Brut Productions from [[Faberge Inc.]]<ref>{{cite web|title=Faberge Sells Brut's Assets|url=https://www.nytimes.com/1982/01/01/business/faberge-sells-brut-s-assets.html|website=http://www.nytimes.com|publisher=New York Times|accessdate=27 November 2014}}</ref>

In 1984, Turner initiated [[Cable Music Channel]], his competition for [[Warner-Amex Satellite Entertainment Company|WASEC]]'s [[MTV]]. The channel was short-lived, but helped influence the original format of [[VH1]].

In 1986, after a failed attempt to acquire [[CBS]], Turner purchased the film studio [[Metro-Goldwyn-Mayer|MGM]]/[[United Artists|UA]] Entertainment Co. from [[Kirk Kerkorian]] for $1.5 billion. Following the acquisition, Turner had an enormous debt and sold parts of the acquisition. MGM/UA Entertainment was sold back to Kirk Kerkorian. The [[Sony Pictures Studios|MGM/UA Studio lot]] in [[Culver City]] was sold to [[Lorimar]]/[[Telepictures]]. Turner kept MGM's pre-May 1986 and pre-merger film and TV library<ref name="ymrt">''You Must Remember This: The Warner Bros. Story,'' (2008) p. 255.</ref><ref>{{cite web|url=https://archive.org/details/mediahistory&tab=collection?and%5B%5D=subject%3A%22Motion%20pictures%20--%20Catalogues%22|title=Media History Digital Library|publisher=}}</ref> [[Turner Entertainment|Turner Entertainment Co.]] was founded on August 4, 1986.

On October 3, 1988, the company launched [[TNT (TV channel)|Turner Network Television]] (TNT).

===1990s===
Turner expanded its presence in movie production and distribution, first with the 1991 purchase of the [[Hanna-Barbera]] animation studio.<ref>{{cite news|last=Lippman|first=John|title=Turner Is Buying Hanna-Barbera Film Library|url=http://articles.latimes.com/1991-10-30/business/fi-565_1_film-library|work=Los Angeles Times|accessdate=September 8, 2010|date=October 30, 1991}}</ref> On December 22, 1993, Turner acquired [[Castle Rock Entertainment]]. Turner purchased [[New Line Cinema]] a month later.<ref>{{cite news| url=http://pqasb.pqarchiver.com/latimes/access/2157288.html?dids=2157288:2157288&FMT=ABS&FMTS=ABS:FT&type=current&date=Aug+07%2C+1993&author=Harris%2C+Kathryn&pub=Los+Angeles+Times&desc=New+Line+Cinema+holding+merger+talks+with+Turner&pqatl=google | first=Kathryn | last=Harris | title=New Line Cinema holding merger talks with Turner | date=August 7, 1993}}</ref><ref>{{cite news| url=http://pqasb.pqarchiver.com/latimes/access/6359666.html?dids=6359666:6359666&FMT=ABS&FMTS=ABS:FT&type=current&date=Aug+18%2C+1993&author=Citron%2C+Alan&pub=Los+Angeles+Times&desc=Turner+gets+nod+to+buy+New+Line+and+Castle+Rock&pqatl=google | first=Alan | last=Citron | title=Turner gets nod to buy New Line and Castle Rock | date=August 18, 1993}}</ref><ref>[http://www.sec.gov/Archives/edgar/data/100240/0000950144-94-000832.txt] ''Turner Broadcasting Company Report.'' [[Securities and Exchange Commission]], [[Washington, D.C.]]</ref>

Turner launched [[Cartoon Network]] on October 1, 1992, followed by [[Turner Classic Movies]] (TCM) on April 14, 1994.

On October 10, 1996, Turner merged with [[Time Warner]], a company formed in 1989 by the merger of Time Inc. and Warner Communications.  Through this merger, Warner Bros. had regained the rights to its pre-1950 library, while Turner gained access to the company's post-1950 library and other properties.

===2000s===
In 2003, Philip I. Kent succeeded [[Jamie Kellner]] as chairman. Operational duties for [[The WB]] were transferred by [[Time Warner]] from [[Warner Bros.]] to Turner Broadcasting during 2001, while Kellner was chairman, but were returned to Warner Bros. in 2003 with the departure of Kellner.

On February 23, 2006, the company agreed to sell the regional entertainment channel [[Turner South]] to [[Fox Entertainment Group]]. Fox assumed control of the channel on May 1, and on October 13 relaunched it as [[SportSouth]] - coincidentally, the former name of [[Fox Sports South]] when Turner owned this channel in partnership with [[Liberty Media]] between 1990 and 1996.

In May 2006 Time Warner, which had owned 50% of Court TV since 1998, purchased the remaining 50% from Liberty Media and began running the channel as part of Turner Broadcasting. The channel was relaunched as [[TruTV]] on January 1, 2008.

Also in May 2006, Ted Turner attended his last meeting as a board member of Time Warner and officially parted with the company.

On October 5, 2007, Turner Broadcasting System completed the acquisition of Claxson Interactive Pay Television Networks in Latin America.<ref>{{cite web|url=http://www.reuters.com/finance/stocks/companyProfile?symbol=XSONF.PK|title=Claxson Interactive Group Inc (XSONF.PK)|publisher=|accessdate=20 June 2015}}</ref>{{reliable source|date=April 2016}}

===2010s===
On August 26, 2010 Turner Broadcasting took full control of Chilevisión, a TV channel owned by Chile's President Sebastián Piñera.<ref>{{cite web|url=http://www.timewarner.com/newsroom/press-releases/2010/08/25/turner-broadcasting-system-inc-to-acquire-chilevisi-n|title=Turner Broadcasting System, Inc. to Acquire Chilevisión|publisher=|accessdate=20 June 2015}}</ref>

On September 8, 2011 Turner Broadcasting System acquired [[Lazytown Entertainment|LazyTown Entertainment]], the producer of the TV series ''[[LazyTown]]''.<ref>{{cite news| url=http://www.hollywoodreporter.com/news/lazytown-founder-sells-turner-broadcasting-232908 | work=The Hollywood Reporter | first=Mimi | last=Turner | title='Lazytown' Founder Sells To Turner Broadcasting For $25 Million | date=8 September 2011}}</ref>

On January 1, 2014, John K. Martin succeeded Phil Kent as chairman and CEO of Turner Broadcasting.<ref>{{cite web|title=Turner|url=https://pressroom.turner.com/us/john-martin#.VS_o_5TF9Ug|website=John Martin Bio}}</ref>

On August 26, 2014, it was reported that Turner Broadcasting was preparing to fire 550 people before an [[NBA]] rights deal.<ref>{{cite news | url=http://www.cbssports.com/nba/eye-on-basketball/24680537/report-turner-to-fire-550-people-in-advance-of-rising-nba-rights-deal | title=Report: Turner to fire 550 people in advance of rising NBA rights deal | work=CBS Sports | accessdate=August 26, 2014}}</ref>  It was further reported in October 2014 that the company planned to reduce its workforce by 10% (1,475 people) through layoffs across a wide set of units including corporate positions.<ref>{{cite news | author=Friedman, Wayne | title=Turner Broadcasting To Cut 10% Of Workforce | date=October 6, 2014 | work=MediaDailyNews | publisher=MediaPost Communications | location=New York | url=http://www.mediapost.com/publications/article/235579/turner-broadcasting-to-cut-10-of-workforce.html | deadurl=no<!--added to archive.org--> }}</ref>

On August 14, 2015, it was announced that Turner Broadcasting had acquired a majority stake in iStreamPlanet, a Las Vegas-based video streaming services company, in an effort to bolster its [[over-the-top content|over-the-top]] programming and shift its core technology infrastructure to the cloud. iStreamPlanet is a direct competitor of [[Major League Baseball Advanced Media]]. The deal was reported to be in the neighborhood of $200 million.<ref>{{cite web|last1=Ramachandran|first1=Shalini|title=Time Warner's Turner Cable Unit Acquires Majority Stake In iStreamPlanet|url=http://blogs.wsj.com/cmo/2015/08/14/time-warners-turner-cable-unit-acquires-majority-stake-in-istreamplanet/|website=The Wall Street Journal|publisher=The Wall Street Journal|accessdate=14 August 2015}}</ref>

[[File:Turner logo.svg|alt=Turner Broadcasting System logo until 2015|thumb|Former Turner Broadcasting System logo used from 1987 to November 2, 2015]]

== Channels ==

===U.S. domestic===

'''News'''
* [[HLN (TV network)|HLN]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[CNN]]<br>("CNN Philippines TV9") ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[CNN Airport]]

'''Movies & Entertainment'''
* [[TBS (U.S. TV channel)|TBS]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[TNT (TV channel)|TNT]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[TruTV]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Turner Classic Movies]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])

'''Terrestrial Broadcasting'''
* [[WPCH-TV]] (also known as Peachtree TV; operated by [[Meredith Corporation]] under a Local Marketing Agreement; Full Acquisition by Meredith pending approval.)

'''Animation, Young Adults & Kids Media'''
* [[Boomerang (TV Channel)|Boomerang]]<br>("TV5, Boing, Animax Asia") ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Cartoon Network]]<br>("Cartoon Network Philippines, TV5, Boing, Animax Asia")
* [[Adult Swim]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])

'''Interactive/broadband sites'''
* AdultSwim.com
* CartoonNetwork.com
* CNN.com
* HLNtv.com
* TBS.com
* TCM.com
* TNTDrama.com
* truTV.com
* TeamCoco.com
* VeryFunnyAds.com
* BleacherReport.com
* NBA.com
* PGA.com
* NCAA.com/March Madness Live
*[[Hulu]] (10%)

'''Sports'''
* [[NBA TV]] ([[High-definition television|HD]] and [[Standard-definition television|SD]]) (Owned by the [[National Basketball Association]] while Turner Broadcasting operates the network)

===International===

'''Latin America'''<br>
The channels in Latin America are controlled by [[Turner Broadcasting System Latin America]], headquartered in Buenos Aires, Argentina. It broadcasts [[Latin American]] versions of U.S. channels, and also channels that are exclusive for the region. TBS LA also handles advertising sales for [[Warner Channel]] (owned by fellow Time Warner division [[Warner Bros.|Warner Bros. Entertainment Inc.]]) and for the Brazilian action sports channel Woohoo.

* [[Chilevisión]] (Free to air, Chile)

'''News and Information'''
* [[CNN en Español]]
** CNN en Español Pan-regional
** CNN en Español México
* [[CNN Chile]]
* [[CNN Indonesia]] (co-owned by [[Trans Corp|Trans Media]])
* [[CNN International]]
** Channel [[Standard-definition television|SD]]<br>([[Latin America]], [[Asia]], [[Europe]], [[Africa]])
** Channel [[High-definition television|HD]]<br>([[Asia]],  [[Middle East and North Africa]])
* [[CNN Philippines]] (co-owned with [[Nine Media Corporation]] and [[Radio Philippines Network]] through a brand licensing agreement)
* [[CNN Türk]] (co-owned with [[Doğan Media Group]])
* [[CNN|CNN HD (USA)]]
* [[HLN (TV channel)|HLN International]]
** Channel [[Standard-definition television|SD]]<br>([[Asia]], [[Middle East and North Africa]])
'''Indian'''
* [[CNN-News18]]
* [[IBN-7]]
* [[CNN International South Asia]]
* [[Toonami (India)|Toonami India]]
* [[IBN-Lokmat]]
* [[News 18 Assam]]
* [[TV 18|News 18 Tamil]]
* [[Boing (India)]]
* [[News Nation Assam]]
* [[Boomerang|Boomerang India]]
* [[Cartoon Network (India)|Cartoon Network India]]
* [[Pogo TV]]
* [[HBO India]]
* [[WB India]]
* [[TV Asia]]
* [[TV 18]]

'''Kids and Teens'''
* [[Boomerang (Latin America)|Boomerang]]
** Boomerang Latin America
** Boomerang México
** Boomerang Brasil
** Boomerang India
* [[Boomerang (Australia and New Zealand)|Boomerang Australia]]
* [[Boomerang Germany]]
* [[Boomerang (EMEA)|Boomerang Nederland]] (Audio option Dutch)
* [[Boomerang (EMEA)|Boomerang (Middle East & Africa)]]
* [[Boomerang (CEE)|Boomerang Central, Southeast Europe and Russia]] (Audio option in English, Romanian, Hungarian, Bulgarian and Russian)
* [[Boomerang (France)|Boomerang France]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Boomerang (Italy)|Boomerang Italy]]
* [[Boomerang (Nordic)|Boomerang Nordic]]
* [[Boomerang (UK & Ireland)|Boomerang UK & Ireland]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Boomerang (Southeast Asia)|Boomerang Southeast Asia]] (Multiaudio option)
* Boomerang Turkey
* Boomerang Japan
* [[Cartoon Network (Latin America)|Cartoon Network Latin America]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
** Cartoon Network Latin America 1
** Cartoon Network Chile
** Cartoon Classic India
** Cartoon Network Argentina
** Cartoon Network México
** Cartoon Network Brasil
* [[Cartoon Network (Poland)|Cartoon Network Poland]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Cartoon Network (Hungary)|Cartoon Network Hungary]]
* [[Cartoon Network (Sweden)|Cartoon Network Nordic]] (Sweden, Norway, Danmark and Finland)
* [[Cartoon Network (Italy)|Cartoon Network Italia]]
* [[Cartoon Network (Netherlands)|Cartoon Network Nederland]]
* [[Cartoon Network (Turkey)|Cartoon Network Türkiye]]
* [[Cartoon Network (Pakistan)|Cartoon Network Pakistan]]
* [[Cartoon Network (France)|Cartoon Network France]]([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Cartoon Network (Canada)|Cartoon Network Canada]]
* [[Cartoon Network (Australia)|Cartoon Network Australia]]
* [[Cartoon Network (India)|Cartoon Network India]]
* [[Cartoon Network (UK & Ireland)|Cartoon Network UK & Ireland]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Cartoon Network (Japan)|Cartoon Network Japan]]
* [[Cartoon Network (Taiwan)|Cartoon Network Taiwan]]
* [[Cartoon Network (Russia and Southeastern Europe)|Cartoon Network Russia and Southeast Europe]]
* [[Cartoon Network (Greece)|Cartoon Network (Middle East & Africa)]]
* [[Cartoon Network Arabic]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Cartoon Network (Germany)|Cartoon Network Deutschland]]
* [[Cartoon Network (Korea)|Cartoon Network Korea]]
* [[Cartoon Network (Southeast Asia)|Cartoon Network Southeast Asia]]
* [[Cartoon Network (Philippines)|Cartoon Network Philippines]]
* [[Tooncast]] (Latin America)
** Tooncast Pan-regional
** Tooncast Brasil
* [[Cartoonito]] (UK & Ireland)
* [[Toonami (Asia)|Toonami Channel]] (Asia)
* [[Toonami (France)|Toonami Channel]] (France)
* [[Pogo TV|Pogo]] (India)

'''Lifestyle'''
* [[Glitz* (TV channel)|Glitz* Latin America]]
** Glitz* Pan-regional 
* [[TNT Glitz]] (Germany, previously Glitz*)
* [[truTV (Latin America)|TruTV Latin America]] (Also in High Definition)
** TruTV Pan-regional
** TruTV Brasil
* [[Cartoonito|TruTV Southeast Asia]]
* TABI Channel (Japan)
* MONDO TV (Japan)

'''Music'''
* [[Hispanic Television|HTV]] (Latin America)
* [[MuchMusic Latin America|MuchMusic]] (Latin America)
* [[Imagine Showbiz]] (India)

'''Movies & Entertainment'''
* [[TNT (TV channel)#International|TNT]] (Also in High Definition)
** TNT México
** TNT Brasil
** TNT Argentina (Argentina, Uruguay and Paraguay)
** TNT Chile & South American countries
** TNT Colombia
** TNT Panama & Central American countries
* [[TNT (TV channel)|TNT Series]] (Latin America, [[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[TNT (TV channel)#International|TNT]]
* [[TNT (TV channel)#International|TNT Poland]]
* [[TNT (TV channel)#International|TNT Romania]]
* [[TNT Serie]] (Germany)
* [[TNT Film]] (Germany)
* [[TNT7|TNT Nordic]]
* [[I.Sat|I-SAT]] (Latin America)
** I-SAT Pan-regional
** I-SAT Argentina
** I-SAT Brasil
* [[Space (Latin American TV channel)|Space]] (Latin America, Also in High Definition)
** Space Pan-regional
** Space Argentina
** Space Brasil
* [[Turner Classic Movies|TCM]] (Latin America)
** TCM Pan-regional
** TCM Argentina
** TCM Brazil
* [[TCM Nordic|Turner Classic Movies Nordic]] (Sweden, Norway, Danmark and Finland)
* [[Turner Classic Movies (UK & Ireland)|Turner Classic Movies UK]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Turner Classic Movies (Middle East and Africa)|Turner Classic Movies Middle East and Africa]]
* [[Turner Classic Movies|TCM Spain]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Turner Classic Movies|TCM Cinema (France)]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[Turner Classic Movies (Asia)|TCM South East Asia]]
* [[TBS (Latin American TV channel)|TBS]] (Latin America)
** TBS Pan-regional
** TBS Argentina
** TBS Brasil
* [[HBO|HBO Latin America]] (HBO, HBO2, HBO Plus East and West, HBO Family, HBO Signature, Max East, Max West, Maxprime East, Maxprime West, Max UP HD)
* [[HBO South Asia|HBO Asia]] (HBO Asia, HBO Hits, HBO Family, Cinemax and Red Screen)
* [[HBO|HBO India]]
* [[HBO Pakistan]]
* [[WB Channel|Warner TV South East Asia]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[WB Channel|Warner TV India]] ([[High-definition television|HD]] and [[Standard-definition television|SD]])
* [[WB Channel Pakistan|Warner TV Pakistan]]
* [[Warner Channel|Warner Channel Latin America]]

'''Sports'''
* Woohoo (Brazil)
* Esporte Interativo (Brazil)
* Esporte Interativo Plus (Brazil) - Only Web
* EI Maxx (Brazil)
* EI Maxx 2 (Brazil)

'''Chinese'''
* [[Phoenix Chinese Channel]]
* [[Phoenix InfoNews Channel]]
* [[Phoenix North America Chinese Channel]]
* [[Phoenix Chinese News and Entertainment Channel]]
* [[Phoenix Hong Kong Channel]]
* [[Oh!K]]

===Other regions===

'''Regional News'''
* [[CNN Chile]], a joint-venture between Turner Latin América and VTR Globalcom that is only aired in [[Chile]].
* [[CNN IBN]], a joint-venture between Turner, [[TV18]] and Global Broadcast News that is only aired in India.
* [[CNN Türk]], owned by Doğan Medya Grubu that is only aired in Turkey.
* [[CNN+]], a joint-venture between Turner (50%) and [[Sogecable]] that is only aired in Spain, closed down in late 2010.

'''Entertainment'''
* [[China Entertainment Television|CETV]] (36%, joint venture with [[TOM Group]])
* [[QTV (South Korea)|QTV]] (50%, joint venture with IS Plus, an affiliate of [[JoongAng Ilbo]]) ([[South Korea]])
* [[Showtime Scandinavia]] through [[Turner NonStop Television]] in the Scandinavian countries.
* [[Silver (TV channel)]], independent and international movies, through [[Turner NonStop Television]] in the Scandinavian countries.
* [[Star]], showbiz news programming, through [[Turner NonStop Television]] in the Scandinavian countries.
* [[TruTV (UK and Ireland)]] Entertainment & Reality channel

'''Animation'''
* [[Boomerang (British TV channel)|Boomerang UK]], various [[Boomerang (TV channel)|Boomerang]] channels around Europe
* [[Boing (Italy)]] (Mediaset 51% and Turner 49%)
* [[Boing (France)]]
* [[Boing (Africa)]]
* [[Boing (Spain)]]

'''Movies'''
* [[Lumiere Movies]] : 92%

==Former assets==
* [[Atlanta Braves]] — An [[Major League Baseball|MLB]] franchise (Now owned by [[Liberty Media]])
* [[Atlanta Hawks]] — An [[National Basketball Association|NBA]] franchise (Now owned by [[Atlanta Spirit]])
* [[Atlanta Thrashers]] — An [[National Hockey League|NHL]] franchise (now owned by [[True North Sports & Entertainment]] as the [[Winnipeg Jets]])
* [[Cable Music Channel]] — A defunct television channel
* [[Castle Rock Entertainment]] — A film production company*
* [[CNNfn]] — A defunct television channel
* [[CNNSI]] — A defunct television channel
* [[Hanna-Barbera Productions]] — An animation studio*
* [[SportSouth]] — A regional sports network (Now owned by [[21st Century Fox]] as [[Fox Sports South]])
* [[New Line Cinema]] — A film production company*
** [[New Line Television]] — A television production arm of New Line Productions*
* [[Turner Publishing]]<ref>{{cite web|title=Turner Publishing|url=http://www.turnerpublishing.com/}}</ref> — A book publisher
* [[Turner Entertainment|Turner Entertainment Co.]] — A film holding company*
** [[Turner Entertainment#Production company|Turner Pictures]] — An original in-house programming*
** [[Turner Entertainment#Production company|Turner Pictures Worldwide Distribution]] — An international distribution sales unit*
** [[Turner Entertainment#Production company|Turner Feature Animation]] — An animation unit*
** [[Turner Entertainment#Home Video|Turner Home Entertainment]] — A home video distributor (Merged into [[Warner Home Video]])
** [[Turner Program Services]] — A former syndication arm, merged into Time Warnerʻs [[Telepictures|Telepictures Productions]]
* [[Turner South]] — A regional television channel (Now owned by [[Fox Sports Networks]] as [[Fox Sports Southeast]])
* [[Universal Wrestling Corporation]] - A professional wrestling promotion formerly known as World Championship Wrestling. Currently a non-operational company, select assets are now owned by WWE, Inc. through WCW, Inc.<ref>{{cite web|title=WCW: How It Died, and How WWE and Vince McMahon Made Sure It Never Rose Again|url=http://bleacherreport.com/articles/632761-wcw-how-it-died-and-how-wwe-and-vince-mcmahon-made-sure-it-never-rose-again|website=Bleacher Report}}</ref>
<nowiki>*</nowiki> Now owned or absorbed by sister company, [[Warner Bros.]]

==References==
{{Reflist|35em}}

==External links==
* {{official website}}

{{Turner Broadcasting System}}
{{Atlanta companies}}
{{Time Warner}}
{{Portal bar|Atlanta|Companies}}

[[Category:Turner Broadcasting System| ]]
[[Category:American cable network groups]]
[[Category:Broadcasting companies of the United States]]
[[Category:Entertainment companies of the United States]]
[[Category:Media companies of the United States]]
[[Category:Multinational companies headquartered in the United States]]
[[Category:Companies based in Atlanta]]
[[Category:Media companies established in 1970]]
[[Category:1970 establishments in Georgia (U.S. state)]]
[[Category:Time Warner]]