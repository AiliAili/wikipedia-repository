{{Infobox publisher
| image = [[File:Universal-Publishers-BrownWalkerPress-DissertationCom-Logos2015a.jpg|250px|center]]
| logo = [[File:Universal-Publishers-BrownWalkerPress-DissertationCom-Logos2015a.jpg|250px|center]]
| parent = 
| status = 
| founded = 1999
| founder = Jeffrey R. Young
| country = United States
| headquarters = [[Boca Raton, Florida]]
| keypeople = Jeff Young, Publisher; Shereen Siddiqui, Artistic Director
| publications = [[Book]]s, [[edited volume]]s, [[textbook]]s, [[monograph]]s, [[conference proceedings]], [[academic journal]]s
| topics = [[technology]], [[how-to]], [[higher education]], [[science]], [[professional development]]
| genre = 
| imprints = Universal-Publishers, Dissertation.com, BrownWalker Press
| revenue = 
| numemployees = 
| nasdaq = 
| url = {{URL|http://www.Universal-Publishers.com}}
}}
'''Universal Publishers''' is the parent [[publishing company]] of three non-fiction book [[imprint (trade name)|imprint]]s specializing in textbook and academic titles (Universal-Publishers, BrownWalker Press & Dissertation.com). It originally began in 1997 as "Dissertation.com," one of the first companies to use [[print on-demand]] and PDF ([[PDF|Portable Document Format]]) [[e-book]] technologies to publish academic [[theses]] and [[Ph.D.]] dissertations for sale online.<ref>{{Cite book |title=The dissertation journey: a practical and comprehensive guide to planning, writing, and defending your dissertation |author=Carol M. Roberts |publisher=[[Corwin Press]] |year=2004}}</ref><ref>{{Cite book |title=Completing a Professional Practice Dissertation: A Guide for Doctoral Students and Faculty |author1=Jerry W. Willis |author2=Deborah Inman |author3=Ron Valenti |publisher=[[Information Age Publishing]] |year=2010 |page=339}}</ref> The company was founded by Jeffrey R. Young, while he was in graduate school as a way to make academic dissertations widely available to other students and researchers at a reasonable price.<ref>{{cite web |last=Beamish |first=Rita |title=Rescuing Scholars From Obscurity |url=https://www.nytimes.com/library/tech/99/02/circuits/articles/18diss.html |publisher=New York Times |accessdate=26 June 2011}}</ref>

The ''BrownWalker Press'' imprint was launched in 2001 to publish academic textbooks, scholarly [[monograph]]s, and edited collections across a wide range of academic expertise. In 2010, BrownWalker Press added an academic conference proceeding publishing division (ConferencePaper Publisher), and in 2011 launched the ''ASMT International Journal of Afro-Asian Studies'', the first of a series of [[open-access journal|open access]], [[academic journal]]s.

Universal Publishers has over 1000 nonfiction and how-to titles currently available in print and electronically, and releases about 50 new titles each year. In addition to worldwide distribution through [[Ingram Content Group]], it has an independent distribution relationship for some of its titles in [[India]] with Overseas Press.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.universal-publishers.com}}

[[Category:Book publishing companies based in Florida]]
[[Category:Academic publishing companies]]
[[Category:Publishing companies of the United States]]
[[Category:Publishing companies established in 1999]]
[[Category:Companies based in Boca Raton, Florida]]
[[Category:1909 establishments in Florida]]