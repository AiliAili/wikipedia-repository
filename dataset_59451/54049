{{Use dmy dates|date=July 2011}}
{{Use Australian English|date=July 2011}}
{{Infobox Australian place| type = suburb
| name     = Athelstone
| city     = Adelaide
| state    = sa
| image    = Torrensriver athelstone.JPG
| caption  = River Torrens located at Athelstone
| lga      = [[City of Campbelltown, South Australia|City of Campbelltown]]
| postcode = 5076
| pop      = 9,258 | pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref>{{Census 2011 AUS | id = SSC40027 | name = Athelstone (State Suburb) | quick = on | accessdate=14 February 2015}}</ref>
| pop2     = 9,289 | pop2_year = {{CensusAU|2006}}
| pop2_footnotes = <ref>{{Census 2006 AUS | id = SSC41076 | name = Athelstone (State Suburb) | quick = on | accessdate=5 June 2008}}</ref>
| est      = 1852
| area     = 
| stategov = 
| fedgov   = 
| near-nw  = 
| near-n   = 
| near-ne  = 
| near-w   = 
| near-e   = 
| near-sw  = 
| near-s   = 
| near-se  = 
| dist1    = 
| location1= 
}}
[[File:Athelstone Community Hall (22854965566).jpg|thumb|right|Athelstone Community Hall]]

'''Athelstone''' {{IPAc-en|ˈ|æ|θ|əl|s|t|ən}} is a suburb of [[Adelaide]] in the [[City of Campbelltown, South Australia|City of Campbelltown]]. It was established in the 19th century but only became part of suburban Adelaide in the second half of the 20th century.

It is approximately 10&nbsp;km north-east of Adelaide's central business district. The main arterial road, Gorge Road, runs through this suburb in an east-west direction. The [[River Torrens]], one of Adelaide's major water supplies, borders the suburb. This water source runs its way from [[Mount Pleasant, South Australia|Mount Pleasant]] to the sea. Gorge Road leads up into the [[Adelaide Hills]], joining the gorge of the Kangaroo Creek Dam, and the Torrens' source. Fifth Creek (a tributary to the River Torrens) also runs through the suburb as an intermittent stream, prone to flooding in late Spring (October–December).

Athelstone is bounded by the River Torrens, [[Black Hill Conservation Park]], Montacute Road, Stradbroke Road, Hamilton Terrace, Schulze Road and River Drive.

== History ==
{{See also|History of Adelaide}}
{{See also|European settlement of South Australia}}

The first Europeans known to have visited Athelstone were the explorers [[John Hill (explorer)|John Hill]] and Dr George Imlay. They camped overnight beside the Torrens on 23 January 1838, en route to the [[Murray River]].<ref>Register newspaper, 16 June 1838, p. 3., ''Messrs Imlay & Hill's Excursion to the River Murray.''</ref>

Due to the high fertility of the alluvial river soils, the district was soon being farmed by incoming European migrants. Athelstone's name comes from Athelstone house and mill, built circa 1843 to 1845. John Coulls from [[Helston]] in [[Cornwall]] bought the mill in 1845, converting it for grape crushing. He named the buildings ''At Helstone'' after his hometown. The mill and house remain, listed as State Heritage Places.<ref>National Trust Register, Register of the National Estate</ref>

The suburb (lower Athelstone) began as a village along the River Torrens. The shopping centre or village centre began from these settlements. Athelstone Post Office opened on 19 March 1863.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref> The Addison family held possession of the land sold to the [[Jesuits]] in the 1960s to build [[Saint Ignatius' College, Adelaide|Saint Ignatius' College]] senior campus around the Upper Athelstone site. Smaller subdivisions have been created. For example, Foxfield Estate in the east, was a development of the Hickinbotham Group during the late 1970s. Other subdevelopments have also occurred.

Land has also been preserved in the process. Wadmore Park, adjoining Athelstone Oval and Black Hill Conservation Park were established as recreational and nature reserves.

Market gardening was a big part of Athelstone's development. Families such as the Tunno family migrated from Italy in the 1950s and grew a variety of bunched vegetables on a small acreage for 30 years selling it to various retailers and wholesale markets throughout Adelaide. Much of the produce grown was European in origin such as endive, spinach, chickory, and fennel. As at 1 December 2014 only one market gardener is left in Athelstone located on Maryvale Road.

==Geography==
Athelstone Recreation Reserve is a reserve in the suburb of Athelstone. It is home to the Athelstone Soccer Club and NAB Soccer Club. It is the largest reserve in the suburb of Athelstone.

== Demography ==
The latest Australian Bureau of Statistics [http://www.abs.gov.au link ABS] data for 2001 indicates Athelstone's population to be predominantly [[upper-middle class]] and totalling just over 9,150 people. There is an even spread of males and females. Over 48% of the population comprises married couples with a skew to the 35- to 64-year-old age range. Less than 15% of the entire population of Athelstone is over 65 years old.

Some 63% of the population was born in Australia. About 36% of the population speaks other languages. Approximately 40% of the people living in Athelstone have both parents born overseas. More than 13% of Athelstone speaks Italian, over 4% speaks Greek, and just over 2% speaks Chinese.

Athelstone residents work primarily in the retail, manufacturing, health, medical, law, property / business services, education and construction sectors (in descending order) with over 66% of its workers employed in those industries.

== Housing ==
The predominantly contemporary housing style is echoed throughout the suburb. There have been different stages in the suburb's development. Dwellings from the first half of the twentieth century housing still predominate in some sections of the suburb, with corrugated iron cladding or stone exteriors (mostly Californian bungalows).

The first contemporary housing was built in the area during the 1970s. Many of these styles have emerged as the area still has some market gardening holdings yet to be released as housing allotments. Some blocks have been subdivided for high density housing arrangements.

== References ==
{{Reflist}}
{{refimprove|date=January 2008}}

{{City of Campbelltown suburbs}}
{{Coord|34.870|S|138.703|E|format=dms|type:city_region:AU-SA|display=title}}

[[Category:Suburbs of Adelaide]]