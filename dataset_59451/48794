'''Isaac Stollman''' ([[Hebrew language|Hebrew]]: יצחק סטולמן) was a noted rabbi, author and religious Zionist leader.
He was born in Russia<ref>Religion: Soil & Soul. Time.  Jan. 21, 1957</ref> in 1897. He studied at some of the most prominent [[Yeshivos]] including the Yeshiva of the [[Chofetz Chaim]] in [[Radin]],<ref>American Jews, Their Lives and Achievements: a contemporary biographical record. American Jewish Literary Foundation, 1947. Page 157.</ref> the [[Yeshivas Knesses Yisrael (Slabodka)|Slobodka yeshiva]], and the [[Novardok yeshiva]]. He received rabbinical ordination from [[Shimon Shkop]], [[Moshe Mordechai Epstein]] and others. In 1924, he immigrated to the United States where he served as rabbi in Detroit, Michigan. In 1925 he became rabbi of the Mishkan Israel synagogue.<ref>Bolkosky, Sidney M. Harmony & Dissonance: Voices of Jewish Identity in Detroit, 1914-1967. page 30. Wayne State University Press; 1st ed edition (November 1991)</ref> In Detroit he was active in many areas of Jewish life. He served as leader of [[Young Israel]], the Stoliner Synagogue, the Beth Yehudah school, the Jewish Community Council, and was also on the board of the [[Jewish Federation]].<ref>Bolkosky, Sidney M. Ibid, page 389</ref> In 1957, having served as vice-president of [[Mizrachi-Hapoel Hamizrachi]] of America for years,<ref>Religion Decried as McCarthy Issue. New York Times. November 22, 1954. Page 12.</ref> he was elected president of that organization,<ref>Detroit Rabbi Will Head Religious Zionist Group. New York Times. January 14, 1957.</ref><ref>Religion: Soil & Soul. Time.  Jan. 21, 1957</ref> re-elected in 1959,<ref>Religious Zionists Hear Leader Ask Soviet To Permit Jews To Emigrate. Canadian Jewish Review. December 19, 1959.</ref> and remained in that position till 1960.<ref>Religious Zionists Pick City Rabbi as Leader. New York Times. January 18, 1960.</ref> He was the president of the [[Detroit]] Rabbinical Council and the author of the acclaimed<ref>Rabbi Stollman to Speak At Bond Dinner. The Canadian Jewish Chronicle. March 22, 1957. "The Rabbi is the author of the four volumes on the Pentateuch under the Hebrew titles "Minchas Yitzchak" which received wide acclaim in the religious world and established its author as master interpreter of the bible in light of Jewish ethics and philosophy."</ref><ref>Hamaor, Volume 8, No. 8, Page 36. Review of fourth volume of Minchas Yitzchak.</ref> Minchas Yitzchak on the Pentateuch.<ref>Rand, Asher (editor). Toldot Anshe Shem. New York, 1950.</ref>
Upon Stollman's death in 1980, [[Emanuel Rackman]] representing [[Bar-Ilan University]] referred to Rabbi Stollman as a "distinguished Rabbi and scholar and outstanding Religious Zionist leader".<ref>Obituary. New York Times, January 8, 1980.</ref> Rabbi [[Norman Lamm]] representing [[Yeshiva University]] referred to Stollman as "an outstanding scholar and Zionist."<ref>Obituary. New York Times, January 9, 1980.</ref>

==Works and articles==
* מנחת יצחק  (Minchas Yitzchak), 4 volumes. 1948
* ציון מן התורה. Zevi Tabory, editor. Isaac Stollman, contributor. Torah Education and Culture Department of the Jewish Agency, Jerusalem, 1963.
* Or HaMizrach, No. 4, Vol 1: (הליכות עולם (לבעיות השעה
* Unpublished works (still in manuscript)
* Religious Freedom in Israel. New York Times, May 10, 1958.

==References==
{{reflist}}

==Biographies==
* Rand, Asher (editor). Toldot Anshe Shem. New York, 1950.
* Kovetz Bais Aharon veYisroel, No. 47, 1993.
* [[:he:יצחק סטולמן]] (Hebrew Wikipedia article on Isaac Stollman)

{{DEFAULTSORT:Stollman, Isaac}}
[[Category:1897 births]]
[[Category:1980 deaths]]