{{Use dmy dates|date=June 2013}}
{{Use British English|date=May 2013}}
{{Infobox automobile
  |image = [[File:Campbell Railton Blue Bird Replica.JPG|250px]] |
  |name = Campbell-Railton Blue Bird
  |related = [[Campbell-Napier-Railton Blue Bird]]
  |production = one-off (1933)
  |body_style = front-engined [[land speed record]] car.
  |engine = 2,300&nbsp;hp 36.7&nbsp;litre supercharged [[Rolls-Royce R]] V12
  |designer = [[Reid Railton]]
  |wheelbase = 13ft 8in ({{convert|13.67|ft|disp=output only}}), Track front 5ft 3in ({{convert|5.25|ft|disp=output only}}), rear {{convert|5|ft}}
  |length = {{convert|27|ft}}
  |weight = 95&nbsp;cwt (4.75tons)
  |fuel_tank = _capacity={{convert|28|impgal}}
  |manufacturer= bodywork by [[J Gurney Nutting & Co Limited|Gurney Nutting]]
}}
The '''Campbell-Railton Blue Bird''' was [[Sir Malcolm Campbell]]'s final [[land speed record]] car.

His previous [[Campbell-Napier-Railton Blue Bird]] of 1931 was rebuilt significantly. The overall layout and the simple twin deep chassis rails remained, but little else. The bodywork remained similar, with the narrow body, the tombstone radiator grille and the semi-spatted wheels, but the mechanics were new. Most significantly, a larger, heavier and considerably more powerful [[Rolls-Royce R]] V12 engine replaced the old [[Napier Lion]], again with supercharger.<ref name="Blue Bird team 1933" >{{Cite web
  |url=http://www.bluebirdteamracing.net/bluebirdsupportersclub/car/1933.html
  |title=Blue Bird 1933
  |publisher=Bluebird team racing
}}</ref> This required two prominent "knuckles" atop the bodywork, to cover the V12 engine's camboxes.<ref name="Brooklands 1933" >{{Cite web|publisher=[[Brooklands photo archive]]
  |title=1933 Blue Bird
  |url=http://www.brooklandsarchives.com/Gallery_C3/target18.html
}}</ref><ref name="Brooklands 1933 rear" >{{Cite web|publisher=[[Brooklands photo archive]]
  |title=1933 Blue Bird from the rear
  |url=http://www.brooklandsarchives.com/Gallery_C3/target8.html
}}</ref><ref name="Sir 1933" >{{Cite web
  |title=Blue Bird, 1933
  |url=http://www.sirmalcolmcampbell.com/sir33.html
}} many period photos</ref>

==1933==
{{externalimage
  |align = right
  |width = 250
  |image1 = [http://www.brooklandsarchives.co.uk/Gallery_C3/target18.html 1933 Blue Bird]<ref name="Brooklands 1933" />
  |image2 = [http://www.brooklandsarchives.co.uk/Gallery_C3/target8.html 1933 Blue Bird from the rear]<ref name="Brooklands 1933 rear" />
}}
''Blue Bird's'' first run was back at [[Daytona Beach Road Course|Daytona]], setting a record of {{convert|272|mph}} on 22 February 1933.

Campbell now had a car with all the power that he could want, but no way to use all of it. Wheelspin was a problem, losing perhaps {{convert|50|mph}} from the top speed.<ref name="Blue Bird team 1933" />

==1935==
[[File:Bluebird land speed record car 1935 rc10413.jpg|thumb|right|on Daytona Beach in 1935]]
[[File:Bluebird 1935 Lledo toy.jpg|thumb|right|Modern [[Lledo]] toy of the 1935 Blue Bird]]
{{externalimage
|align = right
|width = 250
|image1 = [http://www.brooklandsarchives.co.uk/Gallery_C3/target31.html Running at Brooklands]<ref name="Brooklands 1935 track" />
|image2 = [http://www.brooklandsarchives.co.uk/Gallery_C3/target13.html On the banking at Brooklands]<ref name="Brooklands 1935 banking" />
|image3 = [http://www.brooklandsarchives.co.uk/Gallery_C5/target0.html 1935 engine and chassis in the workshops, bodywork removed.] <br>Note the airbrake actuating cylinder<ref name="Brooklands 1935 chassis" />
}}
Visually the car was quite different. The bodywork was now rectangular in cross section and spanned the full width over the wheels. Although actually higher, this increased width gave the impression of a much lower and sleeker car, accentuated by the long stabilising tailfin and the purposeful raised ridges over the engine camboxes.<ref name="Blue Bird team 1935" >{{Cite web
  |url=http://www.bluebirdteamracing.net/bluebirdsupportersclub/car/1935.html
  |title=Bluebird 1935
  |publisher=Bluebird team racing
}}</ref> This ''Blue Bird'' was clearly a design of the [[Modernist#Modernism.27s second generation .281930.E2.80.931945.29|Modernist]] '30s, not the brute heroism of the '20s.<ref name="Brooklands 1935 track" >{{Cite web|publisher=[[Brooklands photo archive]]
  |title=Running at Brooklands
  |url=http://www.brooklandsarchives.com/Gallery_C3/target31.html
}}</ref><ref name="Brooklands 1935 banking" >{{Cite web|publisher=[[Brooklands photo archive]]
  |title=On the banking at Brooklands
  |url=http://www.brooklandsarchives.com/Gallery_C3/target13.html
}}</ref>

Mechanically the changes to the car had focussed on improving the traction, rather than increasing the already generous power. Double wheels and tyres were fitted to the rear axle, to improve grip.<ref name="Brooklands 1935 double tyres" >{{Cite web|publisher=[[Brooklands photo archive]]
  |title=Cockpit, showing the double wheels and also the airbrake flaps
  |url=http://www.brooklandsarchives.com/Gallery_C3/target10.html
}}</ref> The final drive was also split into separate drives to each side. This reduced the load on each drive, allowed the driver position to be lowered, but required the wheelbase to be shortened asymmetrically on one side by 1½"(37&nbsp;mm).<ref name="Brooklands 1935 chassis" >{{Cite web|publisher=[[Brooklands photo archive]]
  |title=Engine and chassis in the workshops, bodywork removed. Note the airbrake actuating cylinder.
  |url=http://www.brooklandsarchives.com/Gallery_C5/target0.html
}}</ref> Airbrakes were fitted, actuated by a large air cylinder. For extra streamlining the radiator air intake could be closed by a movable flap, for a brief period during the record itself.<ref name="Racing Campbells 1935" >{{Cite web
  |title=Blue Bird, 1935
  |url=http://www.racingcampbells.com/content/cars.asp#1935
  |publisher=Racing Campbells
}}</ref>

''Blue Bird'' made its first record runs back on [[Daytona Beach]] in early 1935. On 7 March 1935 Campbell improved his record to {{convert|276.82|mph}}, but the unevenness of the sand caused a loss of grip and he knew the car was capable of more.<ref name="Sir 1935 Daytona" >{{Cite web
  |title=Blue Bird at Daytona, 1935
  |url=http://www.sirmalcolmcampbell.com/sir35d.html
}} many period photos</ref>

The faster car needed a bigger and smoother arena, and this led to the [[Bonneville Salt Flats]] of [[Utah]]. This time the young [[Donald Campbell]] accompanied his father. On 3 September 1935, the 300&nbsp;mph barrier fell by a bare mile-per-hour, that being {{convert|301,337|mi|disp=output only}}, crowning [[Sir Malcolm Campbell]]'s record-breaking career.<ref name="Holthusen 1986" >{{Cite book
  |title=The Land Speed Record
  |last=Holthusen
  |first=Peter J.R.
  |isbn=0-85429-499-6
  |year=1986
}}</ref><ref name="Sir 1935 Bonneville" >{{Cite web
  |title=Bluebird at Bonneville, 1935
  |url=http://www.sirmalcolmcampbell.com/sir35.html
  |archiveurl=https://web.archive.org/web/20070505163751/http://www.sirmalcolmcampbell.com/sir35.html
  |archivedate=2007-05-05
}} many period photos</ref>

==Survival today==
[[International Motorsports Hall of Fame|Alabama Motor Speedway Hall of Fame]], [[Talladega, Alabama|Talladega]], [[Alabama]], USA

There is a replica in the Campbell gallery at the Lakeland Motor Museum, England.

The original is located at the [[Motorsports Hall of Fame of America]], located at [[Daytona International Speedway]] Tour Center.

==References==
{{Reflist}}
* {{Cite web
 |title=record-breaking Pendine Sands 
 |url=http://www.sandspeedwales.co.uk/5907.html 
 |publisher=Sand Speed Wales 
 |format=photos 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20100909181752/http://www.sandspeedwales.co.uk/5907.html 
 |archivedate=9 September 2010 
 |df=dmy 
}} Many rare period photos.

==See also==
*[[Land speed record]]

[[Category:Bluebird record-breaking vehicles]]
[[Category:Wheel-driven land speed record cars|Bluebird,Campbell-Railton Bluebird]]
[[Category:Vehicles designed by Reid Railton|Bluebird,Campbell-Railton Bluebird]]
[[Category:1933 in motorsport]]
[[Category:1935 in motorsport]]
[[Category:Automobiles powered by aircraft engines]]