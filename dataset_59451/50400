{{Infobox journal
| title = African Zoology
| cover = [[File:AfricanZoologycover.jpg|200px]]
| discipline = [[Zoology]]
| abbreviation = Afr. Zool.
| formernames = Zoologica Africana, South African Journal of Zoology
| editor = C.A. Simon
| publisher = [[Zoological Society of Southern Africa]]
| country = South Africa
| history = 1965–present
| frequency = Biannually
| impact = 0.746
| impact-year = 2012
| website = http://www.zssa.co.za/
| link1 = http://www.bioone.org/toc/afzo/current
| link1-name= Online access at [[BioOne]]
| link2 = http://africanzoology.journals.ac.za/pub/issue/current
| link2-name= Latest Issue
| ISSN = 1562-7020
| LCCN = 00227123
| OCLC = 44395820
}}
'''''African Zoology''''' is a biannual [[peer review|peer-reviewed]] [[scientific journal]] that covers any aspect of [[zoology]] relevant to Africa and its surrounding oceans, seas, and islands. It is published by the [[Zoological Society of Southern Africa]]. It publishes topical reviews, full-length papers, short communications, and [[book review]]s.

== History ==
The journal was established by the Zoological Society of Southern Africa in 1965 as ''Zoologica Africana'', which was renamed to ''South African Journal of Zoology'' in 1979, when the South African Bureau of Publications became its publisher. In 2000, the journal returned to the society and obtained its current name.
 
== Abstracting and indexing==
''African Zoology'' is abstracted and indexed in [[Biological Abstracts]], [[Chemical Abstracts Service|Chemical Abstracts]], [[Current Advances in Biology]], [[GeoAbstracts]], [[Science Citation Index]],  and [[The Zoological Record]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2009 [[impact factor]] of 0.746, ranking it 91st among 129 journals in the category "Zoology".<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2011-06-14| archiveurl= https://web.archive.org/web/20110713061611/http://isiwebofknowledge.com/| archivedate= 13 July 2011 <!--DASHBot-->| deadurl= no}}</ref>

== See also ==
* [[List of zoology journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.zssa.co.za/}}
{{wikispecies|ISSN 1562-7020}}

[[Category:Zoology journals]]
[[Category:Publications established in 1965]]
[[Category:English-language journals]]
[[Category:Biannual journals]]