{{Use dmy dates|date=March 2016}}
{{good article}}
{{Infobox military conflict
| conflict    = 1 November 1944 reconnaissance sortie over Japan
| partof      = [[Air raids on Japan]], [[World War II]]
| image       = F-13 Superfortress in May 1947.jpg
|image_size = 300px
| alt         = Black and white photo of a military aircraft powered by four propeller engines parked in an open area
| caption     = An F-13 Superfortress similar to the aircraft involved in the 1 November 1944 sortie
| date        = 1 November 1944
| place       = Japan
| coordinates = 
| map_type    = 
| map_relief  = 
| latitude    = 
| longitude   = 
| map_size    = 
| map_marksize = 
| map_caption = 
| map_label   = 
| territory   = 
| result      = Successful US photo reconnaissance mission
| status      = 
| combatants_header = 
| combatant1  = {{flag|United States|1912}}
| combatant2  = {{flagcountry|Empire of Japan}}
| combatant3  = 
| commander1  = 
| commander2  = 
| commander3  = 
| units1      = 
| units2      = 
| units3      = 
| strength1   = 1 [[Boeing B-29 Superfortress variants#RB-29J (RB-29, FB-29J, F-13, F-13A)|F-13 Superfortress]]
| strength2   = Multiple fighter aircraft<br>Anti-aircraft batteries
| strength3   = 
| casualties1 = None
| casualties2 = None
| casualties3 = 
| notes       = 
| campaignbox = {{Campaignbox Japan}}
}}
On 1 November 1944 a [[United States Army Air Forces]] (USAAF) [[Boeing B-29 Superfortress variants#RB-29J (RB-29, FB-29J, F-13, F-13A)|F-13 Superfortress]] conducted the first flight by an Allied aircraft over the [[Tokyo]] region of Japan since the [[Doolittle Raid]] in April 1942. This photo reconnaissance sortie was highly successful, with the aircraft's crew taking thousands of photographs which were later used to plan many [[air raids on Japan]] during the final months of [[World War II]]. Attempts by Japanese air units and anti-aircraft gun batteries to destroy the F-13 failed, as the available fighter aircraft and guns could not reach the high altitude it operated at.

==Background==

In late 1944 the United States [[Twentieth Air Force|Twentieth Air Force's]] [[XXI Bomber Command]] prepared to conduct strategic bombing raids on the Japanese home islands from bases in the [[Mariana Islands]]. These attacks were to replace the largely unsuccessful [[Operation Matterhorn]] raids which had been conducted by [[XX Bomber Command]] aircraft based in India and staging through bases in China since June 1944.{{sfn|Wolk|2004|p=72}} While XX Bomber Command conducted photo reconnaissance sorties over Japan as part of this effort, the aircraft flying from China lacked the range to reach Japan's main industrial centers.{{sfn|Craven|Cate|1953|p=555}} Without photographic intelligence XXI Bomber Command was unable to develop detailed plans for raids against its intended targets.{{sfn|Kreis|1996|pp=335–336}}

On 10 October 1944 the Committee of Operations Analysts, which provided advice to USAAF commanders on suitable strategic bombardment targets, recommended that photo reconnaissance flights be conducted over Japan's main industrial areas as soon as possible to provide intelligence which could be used to direct raids from the Mariana Islands.{{sfn|Craven|Cate|1953|pp=26, 555}} These operations were to be conducted by the [[3d Photographic Squadron|3d Photographic Reconnaissance Squadron]] (3d PRS), which was the only photo reconnaissance unit in the XXI Bomber Command.{{sfn|Craven|Cate|1953|p=555}}

The 3d PRS had been formed on 10 June 1941. After conducting flights over the Americas, it was deployed to the [[China-Burma-India Theater]]. The unit flew mapping missions over the region from 10 December 1943 until it was disbanded and re-formed in the United States during April 1944 to be equipped with the new [[Boeing B-29 Superfortress variants#RB-29J (RB-29, FB-29J, F-13, F-13A)|F-13]] photo reconnaissance variant of the [[Boeing B-29 Superfortress]] heavy bomber.{{sfn|Cahill|2012|p=14}} Due to delays to the development of the F-13, the 3d PRS was unable to commence training on the type until 24 August, and began to receive its first operational F-13s on 4 October. 3d PRS F-13s began to depart for [[Saipan]] in the Mariana Islands on 19 October, where they would be supported by the unit's ground echelon which had arrived on 18 September.{{sfn|Cahill|2012|p=14}}

==Photo reconnaissance sortie==
The first two 3d PRS F-13s arrived at Saipan on 30 October after a 33-hour flight from [[Mather Air Force Base|Mather Field]] in California via [[Oahu]] and [[Kwajalein]].{{sfn|Craven|Cate|1953|p=555}}{{sfn|Cahill|2012|p=14}} While the commander of the XXI Bomber Command, Brigadier General [[Haywood S. Hansell]], encouraged the exhausted airmen to rest, they insisted on conducting a flight over Japan as soon as possible.{{sfn|Hansell|1986|p=179}}{{sfn|Dorr|2002|p=25}}

At 5.55 am on 1 November an F-13 whose crew was led by Captain [[Ralph D. Steakley]] took off from Saipan bound for Japan.{{sfn|Craven|Cate|1953|p=555}} Weather conditions over Tokyo were perfect for photo reconnaissance, with the skies free of clouds.{{sfn|Dorr|2012|p=118}} Flying at {{convert|32000|ft|m}}, Steakley's aircraft repeatedly passed over a complex of aircraft and engine plants to the west of Tokyo, before moving on to photograph a similar facility near the city of [[Nagoya]]. Overall, the American airmen took 7,000 photos during the mission.{{sfn|Hansell|1986|p=179}}{{sfn|Dorr|2012|pp=118–119}} Steakley was surprised to encounter strong head winds, and reported that his [[ground speed]] while passing over Tokyo was only about {{convert|70|mph|kph}}. The strong winds over this region were not known to the USAAF at the time, and greatly complicated XXI Bomber Command's later air raids.{{sfn|O'Hare|Sweeney|Wilby|2014|p=97}}{{sfn|Craven|Cate|1953|p=576}}

The [[Imperial Japanese Army Air Service|Imperial Japanese Army Air Service's]] [[47th Sentai]] had responsibility for providing fighter aircraft to guard the Tokyo area on 1 November.{{#tag:ref|[[Sentai]]s were air groups which comprised several flying squadrons.{{sfn|Takai|Sakaida|2001|p=6}}|group=Note}} The unit's [[Nakajima Ki-44]] fighters began to take off from Narimasu airfield to intercept the F-13 at 1 pm. These aircraft were not designed to be used at high altitudes, and the Japanese airmen were unable to get closer than about {{convert|3300|ft|m}} of Steakley's aircraft. Two formations of fighters fired machine guns at the F-13, but did not hit it.{{sfn|Takai|Sakaida|2001|pp=25–26}} Several batteries of Japanese anti-aircraft guns also unsuccessfully fired on the American aircraft.{{sfn|Dorr|2012|p=118}} The F-13 was the first American aircraft to fly over Tokyo since the [[Doolittle Raid]] in April 1942, and was seen by many Japanese civilians.{{sfn|Takai|Sakaida|2001|p=25}}

==Aftermath==

The F-13 returned to Saipan after a flight of 14 hours duration. Either later that day or the next day the crew named the aircraft "Tokyo Rose" in reference to [[Tokyo Rose|the propaganda broadcaster]]. Steakley received the [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] for the flight, and the other members of the crew were also later issued with awards.{{sfn|Cahill|2012|p=14}}{{sfn|Dorr|2012|p=119}}

The photographs taken during the sortie were an important source of intelligence for the Twentieth Air Force and other American units, especially as 1 November proved to be the only day of the air campaign against Japan in which weather conditions over the home islands were entirely clear of cloud.{{sfn|Dorr|2012|pp=118–119}} Hansell later stated that the sortie had been "probably the greatest&nbsp;... single contribution&nbsp;... in the air war with Japan".{{sfn|Fedman|Karacas|2012|p=6}}

The 3d PRS conducted a further 16 sorties over Japan before XXI Bomber Command's first raid against Tokyo on 24 November, but several of these missions were frustrated by bad weather.{{sfn|Hansell|1986|p=179}}{{sfn|Cahill|2012|p=15}} One F-13 was lost during a mission to Nagoya on 21 November, but the squadron had nine aircraft at Saipan by the end of the month.{{sfn|Craven|Cate|1953|pp=555–556}} The 3d PRS continued to fly reconnaissance sorties over Japan until the end of the war.{{sfn|Cahill|2012|p=19}}

==References==

===Footnotes===
{{Reflist|group=Note}}

===Citations===
{{reflist|colwidth=30em}}

===Works cited===
*{{cite journal|last=Cahill|first=William M.|title=Imaging the Empire: The 3d Photographic Reconnaissance Squadron in World War II|journal=Air Power History|date=2012|volume=50|issue=1|pages=12–19|issn=1044-016X|ref=harv}}
* {{Cite book|last1=Craven|first1=Wesley|last2=Cate|first2=James (editors)|title=The Pacific: Matterhorn to Nagasaki|publisher=The University of Chicago Press|location=Chicago|year=1953|series=The Army Air Forces in World War II. Volume V|url=http://www.ibiblio.org/hyperwar/AAF/V/index.html|OCLC=256469807|ref=harv}}
* {{cite book|last=Dorr|first=Robert F.|authorlink=Robert F. Dorr|title=B-29 Superfortress Units of World War 2|publisher=Osprey Publishing|location=Oxford|isbn=978-1-84176-285-2|year=2002|ref=harv}}
* {{cite book|last1=Dorr|first1=Robert F.|title=Mission to Tokyo: The American Airmen Who Took the War to the Heart of Japan|date=2012|publisher=MBI Publishing Company|location=Minneapolis, Minnesota|isbn=0-7603-4122-2|ref=harv}}
* {{cite journal|last1=Fedman|first1=David|last2=Karacas|first2=Cary|title=A cartographic fade to black: mapping the destruction of urban Japan during World War II|journal=Journal of Historical Geography|date=2012|volume=38|issue=3|pages=1–23|url=http://www.sciencedirect.com/science/article/pii/S0305748812000266|issn=0305-7488|ref=harv|doi=10.1016/j.jhg.2012.02.004}}
* {{cite book|last1=Hansell|first1=Haywood S.|title=The Strategic Air War Against Germany and Japan: A Memoir|date=1986|publisher=Office of Air Force History, United States Air Force|location=Washington, D.C.|isbn=0-912799-39-0|url=http://ibiblio.org/hyperwar/AAF/Hansell/index.html|ref=harv}}
* {{cite book|last1=Kreis|first1=John F.|editor1-last=Kreis|editor1-first=John F.|title=Piercing the Fog : Intelligence and Army Air Forces Operations in World War II|date=1996|publisher=Air Force History and Museums Program|location=Washington, DC|isbn=0-16-048187-2|pages=297–348|url=http://www.afhso.af.mil/shared/media/document/AFD-101203-023.pdf|chapter=Taking the Offensive: From China-Burma-India to the B-29 Campaign|ref=harv}}
* {{cite book|last1=O'Hare|first1=Greg|last2=Sweeney|first2=John|last3=Wilby|first3=Rob|title=Weather, Climate and Climate Change: Human Perspectives|date=2014|publisher=Routledge|location=London|isbn=1-317-90482-6|ref=harv}}
* {{cite book|last1=Takai|first1=Kōji|last2=Sakaida|first2=Henry|title=B-29 Hunters of the JAAF|publisher=Osprey Publishing|location=Oxford|year=2001|series=Aviation Elite Units|isbn=1-84176-161-3|ref=harv}}
* {{cite magazine|last=Wolk|first=Herman S.|title=The Twentieth Against Japan|magazine=Air Force Magazine|date=2004|url=http://www.airforcemag.com/MagazineArchive/Documents/2004/April%202004/0404japan.pdf|pages=68–73|issn=0730-6784|ref=harv}}

[[Category:World War II strategic bombing of Japan]]
[[Category:World War II aerial operations and battles of the Pacific theatre]]
[[Category:1944 in Japan]]
[[Category:November 1944 events]]