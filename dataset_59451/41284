'''Banjee''' or '''banjee boy''' is a term from the 1980s or earlier that describes a certain type of young [[Hispanic and Latino Americans|Latino]] or [[African American]] [[men who have sex with men|man who has sex with men]] and who dresses in [[stereotype|stereotypical]] masculine [[Hip hop fashion|urban fashion]] for reasons which may include expressing [[masculinity]], hiding his [[sexual orientation]] and attracting male partners. The term is mostly associated with [[New York City]] and may be [[Nuyorican]] in origin.<ref>"'''Bangee'''" and "'''Banjy'''" should probably be regarded as a misspellings rather than variant spellings based on the well-established use of "banjee" in print since at least the 1990 release of ''Paris Is Burning''.</ref><ref name="decon">[http://www.bravesoulcollective.org/perspectives/topic-of-the-month/maskulinity/deconstructing-banjee-realness/ Bravesoulcollective.org] Deconstructing Banjee Realness</ref><ref>[[Sleeveless shirt|Tank tops]], "[[wifebeater (shirt)|wifebeaters]]" and other [[muscle shirt]]s that show off the wearer's physique are a major part of banjee fashion (as evidenced by the promotional shots for the play ''Banjee'' discussed herein, as well as the costumes worn between sex scenes in the porn movies mentioned). Considering that these shirts are a ubiquitous trapping of hip hop culture and are arguably more popular among young gay men than they are in general, the banjee predilection for them makes sense.</ref> [[Attitude (psychology)|Attitude]], [[clothing]], [[ethnicity]], [[masculinity]], [[physical attractiveness|physique]] and [[youth]] are all elements of what has been called "banjee realness".

An African-American man writes:

<blockquote>Banjee. That was the identity I was given back in the summer of 1991, when I, [[closeted|half out/half in]] approached the colored museum of the [[Christopher Street pier]]s. I was new to the life, so I had no reference for what people were talking about, but I soon gathered that "banjee" meant that I wasn't a "queen." Whatever the terms of identification, all I knew was that there was one thing that brought both the banjees and the queens (and whatever lies between) to the pier: we were men who loved men. An anxious 19 year old, I wore my banjee realness designation like a badge of honor. [...] a queen schooled me on how my masculinity was something that carried great weight, not only in the gay world, but the straight world as well.<ref name="decon"/></blockquote>

The 1990 [[documentary film]] ''[[Paris Is Burning (film)|Paris Is Burning]]'' featured "banjee realness" as one of the categories in which contestants competed for [[trophy|trophies]].  According to ''[[The Village Voice]]'' "banjee boy categories have been a part of [[ball culture|vogue balls]] since at least the early 1980s".<ref>[http://www.villagevoice.com/news/0002,trebay,11690,5.html Villagevoice.com], ''[[The Village Voice]]'' 2000</ref>

The word "banjee" never entered mainstream [[popular culture|pop culture]], but it had currency as gay slang throughout the 1990s. In 1998, a report in the [[medical journal]] ''[[AIDS]] Patient Care and [[Sexually transmitted disease|STDs]]'' regarding [[safer sex]] practices among young Black and Latino men was entitled "Banjee Boys Are Down" ("down", in this [[vernacular]], meaning "supportive of it").<ref>[http://www.ncbi.nlm.nih.gov/pubmed/11361901 "Banjee Boys Are Down"], ''AIDS Patient Care and STDs'' 1998 Jan;12(1):71. {{pmid|11361901}}.</ref>

The 1999 play ''Banjee'', written by playwright A.B. Lugo, presented at the Milagro Theater/Clemente Soto Vélez Cultural and Educational Center (and in another NYC venue in 2004), is "the story of Angel (played by Indio Meléndez), a [[heterosexuality|straight]] homeboy, and Tony (played by Will Sierra), an admittedly [[bisexuality|bi]] banjee, who've known each other since childhood."<ref>[http://www.theatrereviews.com/pastreviews/banjee.html Theatrereviews.com]</ref><ref>[http://www.oobr.com/top/volTen/twentyone/0221banjee.htm OOBR.com], Midtown International Theatre Festival 2004</ref><ref>[http://www.villagevoice.com/theater/9925,hannaham,6624,11.html Villagevoice.com], Village Voice 2004</ref><ref>[http://www.nyblade.com/2004/1-30/arts/theater/theater.cfm NYblade.com] [[New York Blade]]'' 2004</ref>

The term banjee has also been used by several producers of [[gay pornography]] in presenting the type of young man described herein. For example, in 1995 a company called Pleasure Productions produced a [[DVD]] called ''Banjee Black Boys'' (and five similarly named [[sequel]]s) and c. 1999-2003 a company called Banjee Boy, Inc. produced films with taglines such as "Wanna see some of the sexiest, thugged out [[gangsta rap|gangstas]] that NY has to offer?" There are other examples from adult films, as well as several pornographic [[website]]s (such as "Banjee Boy Group Slam") that still use the term.<ref>[http://shop.freyacomm.com/rgo/index.cfm?method=Product&ID=215297 Banjee Black Boys] (adults only)</ref><ref>[http://www.kinkyandwild.com/1209/inter/2/index.html Banjee Boy Group Slam] (adults only)</ref>

While seeming to have peaked in popularity during the 1990s, the term banjee is still in use.  For example, a 2003 web page for a [[restaurant]] in [[East Harlem]] describes its clientele as an "eclectic mix of patrons that range from pretty neighborhood Banjee boys to some of the wise guys that once populated the space formerly."<ref>[http://www.orbiteastharlem.com/about.html Orbit East Harlem]</ref>  In 2008 the band [[Hercules & Love Affair]] has performed wearing matching shirts with the word printed on them.<ref>[http://stereogum.com/archives/concert/hercules-love-affair-played-the-first-show-studi_009867.html Hercules & Love Affair Played Their First Show @ Studio B, Brooklyn 5/17/08 - Concert - Stereogum<!-- Bot generated title -->]</ref>
New York clothing label Hood By Air uses the Banjee term as a point of reference for their S/S09 collection.

==Related terms==
'''Homo thug''' is a more recent and more popular term which is nearly-synonymous with "banjee". However, ''homo'' thug does imply that the man in question is primarily [[homosexuality|homosexual]]. In contrast, a banjee might be homosexual but might also be bisexual or only have opportunistic homosexual sex with men when women are unavailable. The latter situation is a theme in many of the pornographic films mentioned above.<ref>[http://www.ronntaylor.com/bulbs/000030.html Ronntaylor.com] Homie-Sexuals, Homo-Thugs & Banjee Queens Galore</ref>

'''Gayngsta''', a [[portmanteau]] derived from "[[gay]]" and "gangsta", is another recent coinage. It has mostly been used in relation to the [[Underground hip hop|underground]] [[LGBT]] [[hip hop music|hip hop]] scene as shown in the documentary ''[[Pick Up the Mic]]'' and featured in the "Homorevolution Tour 2007" with these artists.  While easily discernible in writing, pronunciation is barely discernible from "gangsta".<ref>[http://www.peaceoutfestival.com/id28.html peaceoutfestival.com], ''Pick Up the Mic'': The Movie</ref><ref>[http://www.rollingstone.com/rockdaily/index.php/2007/01/24/gayngsta-rapper-deadlee-launches-a-homorevolution/ Rollingstone.com] ''Rolling Stone''</ref><ref>To pronounce "gayngsta" differently from "gangsta", one has to either draw out the first syllable, accent it, create a stop after it, or take care to pronounce it as "gay" and the rest of the word as "ngster".  As all of these options run contrary to the flow of conversational English, it seems unlikely this term will see much use outside of writing.</ref>

'''Banjee girl''' is heard so rarely that it is difficult to define. In discussing a fashion show in [[Paris]], one reviewer wrote in 2005:<blockquote>
The low-rise skirt in denim is the first of its kind seen on the Paris [[Runway (fashion)|runways]]. What is now clear to me is that no self-respecting Banjee [[Latina]], or [[ghetto-fabulous]] “Shamecka-girl” or high rolling white chick will be ever able to resist its urban appeal.<ref>[http://www.fashionwindows.com/fashion_review/paris/2005S/xuly_bet.asp Fashionwindows.com], Xuly Bët at Paris Prêt-á-Porter Spring 2005.  While the reviewer does seem to understand that "banjee" has some relation to urban fashion and may be deliberately mixing cultural associations for humorous effect, they seem to have confused ''Shamecka'' (a female first name in [[Arabic language|Arabic]]) with ''schmatte'' (from [[Yiddish]] שמאַטע, from [[Polish language|Polish]] ''szmata'') meaning "rag".  The reviewer in question may be similarly confused about the meaning of "banjee".</ref></blockquote>
Several examples of the use of the term "Banjee girl" exist in the [[blogosphere]] but it has rarely, if ever, made it into print or [[mass media]]. An exception is the Billboard charting single "[[Back to My Roots]]" by [[RuPaul]], which states the phrase in a list of hair fashions.
In the film ''[[Paris Is Burning (film)|Paris is Burning]]'', the term itself is used in comparable frequency with its male counterpart, "banjee boy", which coupled with the film's focus on the inextricably connected transgender and drag culture of 1980s NYC, lends itself to a contextual definition of those performers impersonating females and attempting to exhibit the ultimately judged quality of holistic visual verisimilitude—"realness".

==See also==
*[[Down-low (MSM)]]

'''General:'''
* [[African-American culture and sexual orientation]]

==Notes and references==
{{reflist|2}}

==External links==
{{LGBT slang}}
{{Sexual identities}}
{{LGBT}}
{{Sexual slang}}

[[Category:Words coined in the 1980s]]
[[Category:LGBT culture in the United States]]
[[Category:Sexual slang]]
[[Category:LGBT terminology]]
[[Category:Drag ball culture]]
[[Category:Culture of New York City]]
[[Category:Sexual orientation]]
[[Category:LGBT Hispanic and Latino American culture]]
[[Category:LGBT African-American culture]]
[[Category:Intersectionality]]
[[Category:Male homosexuality]]