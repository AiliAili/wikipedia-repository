{{Infobox person
| image       = Emily Gerson Saines, May 2011.jpg
| imagesize   = 200px
| caption     = 
| name        = Emily Gerson Saines
| birthname   = 
| birth_date  = 
| birth_place = [[New York City]], New York
| alma_mater  = [[Dana Hall]] 
[[Northwestern University]]
| occupation  = Talent manager, producer
| othername   = 
| spouse      = 
| children    = 
| notable_works = 
| yearsactive = 
}}
'''Emily Gerson Saines''' is an American [[talent manager]] and producer.

==Biography==

===Early life===
Gerson Saines was born in [[New York City]], [[New York (state)|New York]] and graduated from [[Northwestern University]] with a degree in Radio/Television/Film.

===Career===

====Representation====
Gerson Saines began her career as an assistant at [[Creative Artists Agency]] and first became an agent at [[Agency for the Performing Arts]].  She went on to become a vice president at the [[William Morris Agency]], where she represented clients such as [[Angelina Jolie]], [[Scarlett Johansson]], and [[Robert Downey, Jr]].<ref name="westchestermag2010"/> In 1998, Gerson Saines left William Morris to form her own talent management company, Gerson Saines Management.<ref name="example web reference">[http://www.variety.com/article/VR1117488248 Gerson Saines hangs out N.Y. manager shingle]</ref>  The company would later become Brookside Artist Management, where her clients now include [[Ansel Elgort]], [[Sebastian Stan]], Daveed Diggs, [[Hayden Panettiere]], [[Cynthia Nixon]], [[Yasiin Bey]] (formerly known as [[Mos Def]]), [[Famke Janssen]], [[Sofia Black-D'Elia]], [[Anson Mount]], [[Eric Bogosian]], [[Condola Rashad]], [[Steven Pasquale]], [[Alex Brightman]], [[Mekhi Phifer]], [[Laura Benanti]], [[Nelsan Ellis]], [[Donna Murphy]], [[Tika Sumpter]], Chris Wood, Makenzie Leigh, Grace Van Patten, and [[Macaulay Culkin]].

====Producer====
Gerson Saines made her producing debut with the television film ''[[The Courage to Love]]'', starring [[Vanessa L. Williams|Vanessa Williams]], and executive produced ''Foster Hall'' for [[NBC]] with [[Conan O'Brien]].<ref>[http://www.imdb.com/title/tt0887908/fullcredits Foster Hall - Full Cast and Crew]</ref>

In 2010, she served as executive producer for the [[HBO]] film ''[[Temple Grandin (film)|Temple Grandin]]'' starring [[Claire Danes]], [[David Strathairn]], [[Catherine O’Hara]], and [[Julia Ormond]]. ''Temple Grandin'' aired February 2010 and received seven [[Emmy Awards]] including [[Primetime Emmy Award for Outstanding Made for Television Movie|Outstanding Made for Television Movie]]. ''Temple Grandin'' also received three [[Golden Globe Award]] nominations, a [[Peabody Award]], an [[American Film Institute]] AFI Award, the Princess Grace AMADE medal at the [[Monte Carlo Television Festival]], an [[International Press Academy]] [[Satellite Award]], the US Department of Health & Human Services Voice Award, a 2010 WIN Award, A [[Producers Guild of America Award]] Nomination, a Los Angeles Times Gold Derby TV Award nomination, a [[Critics' Choice Television Award|Critics' Choice Award]] Nomination, a [[Television Critics Association]] Award nomination, and the 2010 Humanitas Prize.<ref>[http://www.imdb.com/title/tt1278469/awards Temple Grandin - Awards]</ref> A.O. Scott of ''[[The New York Times]]'' also named ''Temple Grandin'' the best biopic and one of the best films of 2010.<ref>[http://www.rottentomatoes.com/m/temple_grandin/ Temple Grandin - Rotten Tomatoes]</ref>

===Autism Advocacy===
Gerson Saines co-founded the [[Autism Coalition for Research and Education]] (A.C.R.E), which was the founding organization of [[Autism Speaks]].  A.C.R.E raised millions of dollars for Autism biomedical research, education, advocacy, and awareness.<ref name="westchestermag2010">{{cite web|url=http://www.westchestermagazine.com/Westchester-Magazine/February-2010/HBO-Producer-Emily-Gerson-Saines/ |title=HBO Producer Emily Gerson Saines |publisher=Westchester Magazine |date=February 2010 |first=Marisa |last=Lascala |accessdate=October 30, 2014 }}</ref>

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->
*
*
*
*

==External links==
* {{IMDb name|0756593}}

{{DEFAULTSORT:Saines, Emily Gerson}}
[[Category:Talent managers]]
[[Category:Living people]]
[[Category:People from New York City]]
[[Category:Northwestern University School of Communication alumni]]
[[Category:American film producers]]
[[Category:American talent agents]]