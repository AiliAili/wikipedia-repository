{{good article}}
{{Infobox Military Conflict
|conflict=Battle of Fort Lahtzanit
|partof=the [[Yom Kippur War]]
|image=
|caption=
|date=October 6, 1973
|coordinates={{Coord|31|4|53|N|32|20|31|E|type:event_region:EG|display=inline,title}}
|place=[[Sinai Peninsula|Sinai]], [[Egypt]]
|result=[[Egypt]]ian victory
|combatant1={{Flag|Egypt|1972}}
|combatant2={{flagicon|Israel}} [[Israel]]
|commander1=Mustafa el-'Abassi
|commander2=Muli Malhov
|strength1=1 infantry battalion<br />Supporting weapons
|strength2=1 infantry company<br />1 tank platoon<br />Reinforcing forces
|casualties1=23 killed<br />7 wounded
|casualties2=60 killed<br />26 captured<br />5 tanks destroyed
}}
{{Campaignbox Yom Kippur War}}

The '''Battle of Fort Lahtzanit''' took place on October 6, 1973, between the [[Egyptian Army]] and the [[Israel Defense Forces]] (IDF).  Part of the Egyptian-initiated [[Operation Badr (1973)|Operation Badr]], the battle was one of the first of the [[Yom Kippur War]], fought in and around Fort Lahtzanit, a fortification of the [[Bar Lev Line]], located {{convert|19|km|mi}} south of [[Port Fouad]] in the [[Sinai Peninsula]].

Commencing the battle with an artillery barrage against the fort, the Egyptians surrounded and isolated the fort prior to assaulting it.  Firing ramps intended for Israeli tanks were occupied by Egyptian infantry, who defeated several Israeli attempts to reinforce the fort with armor.  The Egyptians managed to breach the defenses and swiftly capture the fort, and proceeded to clear the bunkers, utilizing flamethrower teams.  By nighttime, the fort was completely under Egyptian control.

==Background==
[[Operation Badr (1973)|Operation Badr]], an Egyptian military operation, had the objective of crossing the [[Suez Canal]] and seizing the Bar Lev line of fortifications. Three of these fortifications, codenamed Budapest, Orkal and Lahtzanit, fell within the area of operations of the Port Said Military Sector. Commanded by [[Major General]] Omar Khaled, the sector was a military command independent of the Egyptian Second Field Army to the south.<ref>Hammad (2002), p.639</ref> The military sector incorporated the towns of [[Port Said]] and [[Port Fouad]] on the [[Mediterranean Sea]] and surrounding areas. Two independent infantry brigades, the 30th and 135th, were under the military sector's command along with some coast guard units.<ref>Hammad (2002), pp.642-643, 645-646</ref>

The commander of Fort Lahtzanit was Lieutenant Muli Malhov, who had served on the canal before. During the week leading up to the [[Yom Kippur War]], Israeli patrols between Lahtzanit and Orkal discovered footprints coming from the canal and moving inwards, almost on a daily basis, indicating that the Egyptians were possibly sending men on long-range intelligence missions, or to act as [[artillery observer]]s. Two days before the outbreak of the war, Malhov expressed concern to his superior officer of the observations being made on the canal line, and that the forts would not stand a chance if they were attacked.<ref>Rabinovich (2004), p.81</ref>

===Plan of attack===
The 30th Independent Infantry Brigade was tasked with capturing Fort Lahtzanit, located at the Kilometer&nbsp;19 mark south of Port Fuoad. Fort Lahtzanit was surrounded by minefields and barbed wire to a depth of {{convert|600|m|ft}}, and incorporated seven bunkers.<ref>Hammad (2002), p.645</ref> The commander of the 30th Brigade, Colonel Mustafa el-'Abassi, committed an infantry battalion to capture the Israeli fort.  The battalion would cross at three different points, between an area {{convert|2|km|mi}} south and {{convert|1.5|km|mi}} north of the fort. Initially the battalion would encircle the fort and cut it off from north, east and south, before the battalion's assault units would move to attack the fort from several directions. 'Abassi was also tasked with overseeing the defense of the Port Said Sector, and thus deployed two battalions on the west bank to defend against an Israeli naval landing or canal-crossing. Additionally, 'Abassi was reinforced with a Sa'iqa [[Company (military unit)|company]] (lit. ''lightning''; Egyptian [[commando]]s). Per planning the company would cross the canal south of the fort, then advance eastward to seize a crossroads located eight kilometers east of the canal, while simultaneously working to intercept enemy reserves headed towards the fort.<ref>Hammad (2002), pp.646-647</ref>

==Battle==
===Isolation===
At 1:55&nbsp;p.m., just before the start of the war, a reconnaissance force swam to the east bank of the [[Suez Canal]], two kilometers south of Fort Lahtzanit, and laid two ropes across the canal to facilitate the crossing of friendly forces. At 2:05&nbsp;p.m., as [[Operation Badr (1973)|Operation Badr]] began, an artillery barrage was initiated against the fort using just the 85&nbsp;mm guns and [[B-10 recoilless rifle]]s of the brigade's anti-tank company; the use of high trajectory artillery guns was forbidden since a number of Egyptian aircraft were flying over this area.<ref>Hammad (2002), p.647</ref>

The troops tasked with isolating Fort Lahtzanit began crossing the canal in dinghies at 2:15&nbsp;p.m.. Ten minutes later they reached the east bank and proceeded to climb the sand wall. They succeeded in attracting Israeli fire, thereby facilitating the main assault force in its mission to attack the fort later on. The troops tasked with isolating the fort from the east reached and occupied a firing ramp prepared for tanks, and raised the Egyptian flag over it. This severely demoralized the Israeli soldiers inside Fort Lahtzanit. Soon after the Sa'iqa company (less one platoon) arrived at the firing ramp as well.<ref>Hammad (2002), pp.647-648</ref>

A reserve of eight [[M48 Patton]] tanks were pushed forward to reinforce the fort. They came up against the eastern isolation force, which destroyed one of the tanks at a range of {{convert|300|m|ft}}. Another tank broke through the Egyptian position and proceeded immediately north towards Fort Orkal, but was destroyed at Kilometer 14 by the northern isolation force. The remaining tanks retreated eastwards to Baluza.<ref name=Hammad648>Hammad (2002), pp.648</ref>

===Attack===
At around 2:50&nbsp;p.m., the B-10 rifles on the west bank managed to open a breach in the barbed wire surrounding the fort, and also destroyed other significant targets, such as the observation equipment. Meanwhile, [[combat engineer]]s were breaching further openings using [[Bangalore torpedo|Bangalore torpedoes]], under heavy [[small arm]]s fire and [[hand grenade]]s. The battalion's main assault force was crossing at the same time. The main assault force then funneled through the breaches and entered the Israeli trenches. The Israeli command in Northern Sinai kept receiving frantic calls for help from the fort's radio operator, and gunfire could also be heard. The Egyptians captured the southern sector of the fort by 3:05&nbsp;p.m.. Five minutes later, the assault force moved against the northern sector of the fort. Within fifteen minutes, the Egyptians were in control of the fort. The power generator inside the fort was destroyed, and all electricity and communication lines leading to the fort were severed. At 3:30&nbsp;p.m., Maj. Gen. Khaled was informed that Lahtzanit had been seized and that Israeli soldiers had been captured.<ref name=Hammad648/> The Israelis reported losing radio contact with the fort at around 4:00&nbsp;p.m.<ref name=Hammad649>Hammad (2002), p.649</ref>

The Egyptians proceeded to clear the bunkers. They utilized [[flamethrower]] teams, which had a visible psychological impact on the fort's garrison. At one point, the Egyptians, with the help of a wounded Israeli prisoner, managed to secure a bunker by convincing the panicked Israelis inside to come out and surrender. Soon after losing contact with the fort, the Israelis attempted to reach it once more. By this time, Malhov was dead. A group of tanks from Baluza advanced to Lahtzanit, but the Sa'iqa company occupying the firing ramp managed to destroy two tanks, forcing the remainder to retreat. Another group of tanks and half-tracks tried to break through the southern isolation force, but were ambushed and withdrew after losing a tank.<ref name=Hammad649/><ref>Rabinovich (2004), p.109</ref>

At 4:00&nbsp;p.m., two anti-tank teams joined the Sa'iqa company on the firing ramp. Both teams, along with the company, were ordered to advance eastward and seize the crossroads. At around 5:00&nbsp;p.m., the [[Israeli Air Force]] began attacking Egyptian forces in the fort and on the west bank of the canal. One group of four aircraft attempted to bomb the fort; the first aircraft dropped its payload of seven bombs, then descended to a low altitude to escape anti-aircraft fire. An anti-air platoon at the Kilometer 17 mark soon shot down an Israeli aircraft (either a [[Mirage III]] or an [[A-4 Skyhawk]]) with a [[Strela 2]] missile, destroying it.<ref>Rabinovich (2004), p.141</ref> Between 6:00 and 7:00&nbsp;p.m. all bunkers and troop shelters inside the fort were cleared, and the Egyptians transferred 26 Israeli prisoners to the west bank of the canal, and thereafter transferred them to Port Said for intelligence gathering. The Israelis concentrated heavy artillery fire against the fort, once they were sure it had been captured by the Egyptians.<ref>Hammad (2002), pp.649-650</ref>

At 9:30&nbsp;p.m., the Sa'iqa company reported an Israeli armored column advancing towards the fort. Two Egyptian tanks on the west bank opened fire, forcing the Israeli tanks and armored vehicles to withdraw. The company continued eastwards, eventually reaching the crossroads, facing no resistance along the way. With the reinforcing anti-tank teams, the company established defensive positions, and el-'Abassi dispatched patrols to secure the roads leading to the fort.<ref name=Hammad650>Hammad (2002), p.650</ref>

==Aftermath==
Fort Lahtzanit was the first defensive fortification of the Bar Lev Line to be captured, over one hour after the start of the assault.<ref>Hammad (2002), p. 648, 650</ref> Israeli casualties were 60 killed and 26 captured; Egyptian losses were 23 killed, including one officer, and 7 wounded, including two officers.<ref name="Hammad650"/> The most important factor leading to the capture of the fort was its isolation on all sides, and the speed with which the infantry reached and seized the firing ramp east of the fort before Israeli tanks occupied it.<ref>Hammad (2002), pp.650-651</ref> In contrast to the capture of Fort Lahtzanit, the attempt to capture Fort Orkal by the 135th Brigade went awry, and el-'Abassi was forced to commit a reserve infantry company on the west bank to help seize the fort. The company crossed at 4:00&nbsp;p.m. on October 6, and the following day Fort Orkal was captured.<ref>Hammad (2002), pp.653-657</ref>

==References==
{{Reflist|3}}

==Bibliography==
*{{Cite book| edition = First| publisher = Dār al-Shurūq| isbn = 977-09-0866-5| pages = 903| last = Hammad| first = Gamal| title = Military Battles on the Egyptian Front| year = 2002|language=Ar}}
*{{Cite book| publisher = Schocken| isbn = 0-8052-1124-1| pages = 592| last = Rabinovich| first = Abraham| title = The Yom Kippur War: The Epic Encounter That Transformed the Middle East| year = 2004}}

{{DEFAULTSORT:Battle Of Fort Lahtzanit}}
[[Category:Battles of the Yom Kippur War]]