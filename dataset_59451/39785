{{for|the squash player|Clive Caldwell (squash player)}}
{{Infobox military person
|name= Clive Robertson Caldwell
|image= AWM SUK12082 Clive Caldwell.jpg
|caption= Clive Caldwell c. 1942
|birth_date= 28 July 1910
|death_date= {{Death date and age|1994|8|5|1910|7|28|df=yes}}
|birth_place= [[Lewisham, New South Wales]]
|death_place= [[Sydney]], New South Wales
|nickname= "Killer"
|allegiance= Australia
|branch= [[Royal Australian Air Force]]
|serviceyears= 1940–46
|rank= [[Group Captain]]
|commands= [[No. 112 Squadron RAF]] (1942)<br/>[[No. 1 Wing RAAF]] (1942–43)<br/>[[No. 80 Wing RAAF]] (1944–45)
|unit=
|battles= World War II
* [[North African campaign]]
* [[Syria-Lebanon campaign]]
* [[South West Pacific theatre of World War II|South West Pacific theatre]]
* [[New Guinea campaign]]
|awards= [[Distinguished Service Order]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & [[Medal bar|Bar]]<br/>[[Krzyż Walecznych]] (Poland)
|laterwork= 
}}
[[Group Captain]] '''Clive Robertson Caldwell''', {{postnominals|country=AUS|size=100%|sep=,|DSO|DFC1}} (28 July 1910 – 5 August 1994) was the leading Australian [[flying ace|air ace]] of World War II. He is officially credited with shooting down 28.5 enemy aircraft in over 300 operational sorties. In addition to his official score, he has been ascribed six probables and 15 damaged.<ref name="Stephens">Stephens 2006, pp. 81–83.</ref><ref>Watson 2005, p. 4.</ref> Caldwell flew [[Curtiss P-40]] Tomahawks and Kittyhawks in the [[North African Campaign]] and [[Supermarine Spitfires]] in the [[South West Pacific theatre of World War II|South West Pacific Theatre]]. He was the highest-scoring P-40 pilot from any air force and the highest-scoring Allied pilot in North Africa.<ref>Alexander 2006, p. 85.</ref> Caldwell also commanded a [[Royal Air Force]] (RAF) [[Squadron (aviation)|squadron]] and two [[Royal Australian Air Force]] (RAAF) [[Wing (military aviation unit)|wings]]. His military service ended in controversy, when he resigned in protest at the misuse of [[Australian First Tactical Air Force|Australian First Tactical Air Force's]] fighter units and was later court martialed and convicted for trading liquor.

==Early life==
Caldwell was born in the Sydney suburb of [[Lewisham, New South Wales|Lewisham]], and educated at [[Albion Park High School|Albion Park School]], [[Sydney Grammar School]] and [[Trinity Grammar School (New South Wales)|Trinity Grammar School]]. He was at Sydney Grammar School from June 1924 until May 1927, but did not complete his Leaving Certificate there (he rowed in the 4th IV and was a member of the Games Committee). He learned to fly in 1938 with the Aero Club of New South Wales. When [[World War II]] broke out, he joined the [[Royal Australian Air Force]] (RAAF), with the intention of becoming a [[Fighter aircraft|fighter]] pilot. As he was over the age limit for fighter training, Caldwell persuaded a pharmacist friend to alter the details on his birth certificate.<ref>Alexander 2006, p. 8.</ref><ref>Watson 2005, pp. 21–22.</ref>  He was accepted by the RAAF and joined the [[British Commonwealth Air Training Plan|Empire Air Training Scheme]] (EATS; also known as the British Commonwealth Air Training Plan and similar names).

==Second World War==
===Middle East and North Africa===
Caldwell's first, brief combat posting was a British [[Hawker Hurricane|Hurricane]] unit, [[No. 73 Squadron RAF|No. 73 Squadron]], [[Royal Air Force]], in the early stages of the [[North African campaign]]. He had gained only a few operational hours when he was transferred to [[No. 250 Squadron RAF]] as it converted to the new [[Curtiss P-40|P-40 Tomahawk]], one of the first units in the world to operate P-40s. According to some accounts,<ref>Alexander 2006, p. 25.</ref> on 6 June 1941, Caldwell as [[Flying Officer]] Jack Hamlyn's wingman, was involved in the P-40's first ever kill, of an Italian [[CANT Z.1007]] bomber, over [[Egypt]]. However, the claim was not officially recognised. (Hamlyn and Sergeant Tom Paxton scored the first official kill two days later, another CANT.) Soon afterwards, Caldwell served with the squadron over [[Syria-Lebanon campaign|Syria and Lebanon]].

After struggling to acquire the skill of [[Deflection (military)|gunnery deflection]], Caldwell developed a training technique, known as "shadow shooting", in which he fired at the shadow of his own aircraft on the desert surface.<ref name="Stephens"/> This was later widely adopted by the [[Desert Air Force]].

The squadron returned to North Africa. On 26 June 1941, while escorting bombers attacking [[Gazala]], [[Libya]], Caldwell destroyed an aircraft in air-to-air combat for the first time, during his 30th [[sortie]].<ref>Alexander 2006, pp. 28, 32, 230.</ref> He downed a German [[Messerschmitt Bf 109]]E, piloted by ''[[Leutnant]]'' Heinz Schmidt of I gruppe, ''[[Jagdgeschwader 27]]'' (JG 27—Fighter Wing 27), over [[Fort Capuzzo|Capuzzo]], he followed this claim with a 'half share' of a Bf 110 on III./[[ZG 26]] and 2 Ju-87s of II./''[[Sturzkampfgeschwader 2]]'' (StG 2—Dive Bomber Wing 2) on 30 June.<ref>Shores and Williams 1994, pp. 162–163.</ref>

On 4 July 1941, Caldwell saw a German pilot shoot and kill a close friend, [[Pilot Officer]] Donald Munro, who was descending to the ground in a parachute.<ref>Alexander 2006, p. 28.</ref> This was a controversial practice, but was nevertheless common among some German and Allied pilots. One biographer, Kristin Alexander, suggests that it may have caused Caldwell's attitude to harden significantly. Months later, [[Press secretary|press officers]] and journalists popularised Caldwell's nickname of "Killer", which he disliked. One reason for the nickname was that he too [[Attacks on parachutists|shot enemy airmen]] after they parachuted out of aircraft.<ref name="Alexander"/> Caldwell commented many years later: "... there was no blood lust or anything about it like that. It was just a matter of not wanting them back to have another go at us. I never shot any who landed where they could be taken prisoner."<ref name="Alexander"/> (In later life, Caldwell said that his thoughts often turned to one Japanese airman or passenger, who survived Caldwell's last aerial victory but could not be rescued.)<ref name="Alexander p. 150">Alexander 2006, p. 150.</ref> A more commonly cited reason for the nickname was his habit of using up ammunition left over at the end of sorties, to shoot up enemy troop convoys and vehicles.<ref name="Stephens"/><ref name="Alexander">Alexander 2006, pp. xviii–xxii.</ref> During his war service, Caldwell wrote in a notebook: "it's your life or theirs. This is war."<ref name="Alexander"/>

While flying to his base alone, over northwest Egypt on 29 August 1941, Caldwell was attacked by two Bf 109s, in a simultaneous approach at right angles. His attackers included one of Germany's most famous aces, ''Leutnant'' [[Werner Schröer]], also of JG 27, in a Bf 109E-7. Caldwell sustained three separate wounds from ammunition fragments and or shrapnel. His Tomahawk was hit by more than 100 7.9&nbsp;mm bullets and five 20&nbsp;mm cannon shells, but he shot down Schröer's wingman, and heavily damaged Schröer's "Black 8", causing Schröer to disengage.<ref name="WWII Ace Stories - Caldwell">Dragicevic, George. [http://www.elknet.pl/acestory/caldw/caldw.htm "Clive 'Killer Caldwell: Stuka party."]  ''WWII Ace Stories'',  Retrieved: 7 March 2006.</ref> On 23 November, Caldwell shot down an ''Experte'', ''[[Hauptmann]]'' [[Wolfgang Lippert (pilot)|Wolfgang Lippert]], ''[[Gruppenkommandeur]]'' (Group Commander) of II./JG 27, who bailed out. Lippert had struck the stabiliser and following capture had his legs amputated but 10 days later, a [[gangrene]] infection set in and he died on 3 December.<ref>Weal 1994, p. 72.</ref><ref>Weal 1994, p. 90.</ref>  For this action, Caldwell was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]].<ref name=DFCs>{{LondonGazette|issue=35392|startpage=7297|endpage=7298|date=23 December 1941|accessdate=20 February 2008}}</ref> Caldwell claimed five [[Junkers Ju 87]] (''Stuka'') [[dive bomber]]s in a matter of minutes on 5 December. For this he was awarded a [[Medal bar|Bar]] to his DFC.<ref name="Stephens"/><ref name="Odgers">Odgers 1984, p. 83.</ref><ref name=DSO /> His report of that action reads:<ref name="WWII Ace Stories - Caldwell"/>

{{quote|I received radio warning that a large enemy formation was approaching from the North-West. No. 250 Squadron went into line astern behind me and as No. 112 Squadron engaged the escorting enemy fighters we attacked the JUs from the rear quarter. At 300 yards I opened fire with all my guns at the leader of one of the rear sections of three, allowing too little deflection, and hit No. 2 and No. 3, one of which burst into flames immediately, the other going down smoking and went into flames after losing about 1000 feet. I then attacked the leader of the rear section...from below and behind, opening fire with all guns at very close range. The enemy aircraft turned over and dived steeply... opened fire [at another Ju 87] again at close range, the enemy caught fire...and crashed in flames. I was able to pull up under the belly of one of the rear, holding the burst until very close range. The enemy... caught fire and dived into the ground.}}

The citations for both the original DFC and the Bar were published in the same issue of the ''[[London Gazette]]'', a supplement to that of 23 December 1944, dated 26 December 1944.<ref name=DFCs /> The first citation described Caldwell as continuing to "take his toll of enemy aircraft" and that "he personally shot down 5 of the enemy's aircraft bringing his total victories to 12." The second that he "has performed splendid work in the Middle East operations", "shown dogged determination and high devotion to duty which have proved an inspiration to his fellow pilots", and that after receiving "wounds on his face, arms and legs...he courageously returned to the attack and shot down one of the hostile aircraft."

On 24 December 1941, Caldwell was involved in an engagement which mortally wounded another Luftwaffe ace, ''Hpt.'' [[Erbo Graf von Kageneck]] (credited with 69 air victories) of III./JG 27. Caldwell only claimed a "damaged" at the time, but postwar sources have attributed him with the kill.<ref>Alexander 2006, pp. 224–228. Note: Kageneck's brother and biographer, [[August von Kageneck]], who later corresponded with Caldwell, was among those who held this theory.</ref>

In January 1942, Caldwell was promoted to [[squadron leader]] and given command of [[No. 112 Squadron RAF]], becoming the first EATS graduate to command a British squadron.<ref>Brown 2000, p. 78.</ref> 112 Sqn at that time included several Polish aviators, and this was why Caldwell was later awarded the Polish ''[[Krzyż Walecznych]]'' (KW; "Cross of Valour").<ref name=KW>{{LondonGazette|issue=35654|supp=yes|startpage=3410|date=31 July 1942|accessdate=20 February 2008}}</ref>

Caldwell scored another striking victory in February 1942, while leading a formation of 11 Kittyhawks from 112 Sqn and 3 Sqn. Over [[Gazala]], he sighted a ''schwarm'' of Bf 109Fs flying some 2,000&nbsp;ft higher. Caldwell immediately nosed into a shallow dive, applied maximum power and [[Boost gauge|boost]], then pulled his Kittyhawk up into a vertical climb. With his P-40 "hanging from its propeller," he fired a burst at a Bf 109 flown by ''Leutnant'' [[Hans-Arnold Stahlschmidt]] of I./JG 27, who was lagging behind the others. Stahlschmidt's fighter "shuddered like a carpet being whacked with a beater" before spinning out of control. Although the Kittyhawk pilots thought that the Bf 109 had crashed inside Allied lines, Stahlschmidt was able to crash-land in friendly territory.<ref>Pentland 1974, p. 9.</ref>

When Caldwell left the theatre later that year, the commander of air operations in North Africa and the Middle East, [[Air Vice Marshal]] [[Arthur Tedder, 1st Baron Tedder|Arthur Tedder]] described him as: "[a]n excellent leader and a first class shot".<ref name="Odgers"/> Caldwell claimed 22 victories while in North Africa flying P-40s, including ten Bf 109s and two [[Macchi C.202]]s. He had flown some 550 hours in over 300 operational sorties.

[[File:Herb and fire chief.jpg|thumb|Test Pilot Herbert O. Fisher shakes hands with Curtiss-Wright fire chief; note Caldwell in background, August 1942.]]

While on a tour of the United States, Caldwell visited [[Curtiss-Wright]] in [[Buffalo, New York]]. On August 6, 1942, he was invited to come on an acceptance re-flight of a [[Curtiss C-46 Commando]], the latest transport aircraft destined for overseas use. The aircraft was also loaded with Curtiss executives, and flown by Chief Production Test Pilot [[Herbert O. Fisher]]. The landing gear became stuck in a three-quarters down position, and after an extended eight-hour attempt to release the gear, Fisher calmly belly-landed the C-46. With the weight of the aircraft gently pushing the gear back into the wheel wells, a minimum of damage resulted. Caldwell had taken over as the co-pilot on the eight hours of circling over Buffalo, receiving certification that he was checked out on the C-46, under the tutelage of Fisher. Finishing his tour at Curtiss-Wright, Caldwell went on to visit the [[North American Aviation]] factory and was able to personally evaluate their new [[North American P-51 Mustang|P-51]] fighter, then in development.<ref>Alexander 2006, pp. 165–166.</ref>

===South West Pacific===
[[File:Caldwell (AWM OG1998).jpg|thumb|Caldwell with his Spitfire on Morotai in December 1944]]
During 1942, Australia came under increasing pressure from [[Empire of Japan|Japanese]] forces, and Caldwell was recalled by the RAAF, to serve as the [[wing leader]] of [[No. 1 Wing RAAF|No. 1 (Fighter) Wing]], comprising [[No. 54 Squadron RAF]], [[No. 452 Squadron RAAF]] and [[No. 457 Squadron RAAF]]. The wing was equipped with the [[Supermarine Spitfire]] and in early 1943 was posted to [[Darwin, Northern Territory|Darwin]], to defend it against [[Japanese air attacks on Australia, 1942-43|Japanese air raids]].

Caldwell claimed two kills in his first interception sortie over Darwin, a [[A6M Zero|Mitsubishi A6M Zero]] (also known by the Allied codename "Zeke") fighter and a [[Nakajima B5N]] "Kate" light bomber.<ref>RAAF Historical Section 1995, pp. 128–131.</ref><ref name="Alexander pp. 109–111">Alexander 2006, pp. 109–111.</ref> The Spitfire pilots found Japanese fighter pilots reluctant to engage Allied fighters over Australia, due to the distance from their bases in the [[Dutch East Indies]]. The wing initially suffered high losses, due to the inexperience of many of its pilots, and teething mechanical problems with their newly "[[tropics|tropicalised]]" Mark VC Spitfires. This was viewed with concern by high commanders, to such extent that the Allied air commander in the South West Pacific, Major General [[George Kenney]], considered sending the wing to the [[New Guinea campaign]], and returning U.S. [[Fifth Air Force]] fighter units to Darwin.

Caldwell scored what was to be his last aerial victory, a [[Mitsubishi Ki-46]] "Dinah" of the ''202nd Sentai'', over the [[Arafura Sea]] on 17 August 1943.<ref name="Alexander p. 150"/> He claimed a total of 6.5 Japanese aircraft shot down.<ref name="Alexander pp. 109–111"/>

Later in 1943, Caldwell was posted to [[Mildura, Victoria|Mildura]], to command [[No. 2 Operational Conversion Unit RAAF|No. 2 Operational Training Unit]] (2OTU).  He was awarded the [[Distinguished Service Order]] (DSO) in November 1943.<ref name=DSO>{{LondonGazette|issue=36215|supp=yes|startpage=4621|date=15 October 1943|accessdate=20 February 2008}}</ref>  By 1944, with the Japanese forces retreating north, Caldwell was again posted to Darwin, this time commanding [[No. 80 Wing RAAF|No. 80 (Fighter) Wing]], equipped with the Spitfire Mark VIII.

In April 1945, while serving at [[Morotai]] in the Dutch East Indies with the [[Australian First Tactical Air Force]], as [[Officer Commanding]] No. 80 Wing, Caldwell played a leading part in the "[[Morotai Mutiny]]", in which several senior flyers resigned in protest at what they saw as the relegation of RAAF fighter squadrons to dangerous and strategically worthless ground attack missions. An investigation resulted in three senior officers being relieved of their commands, with Caldwell and the other "mutineers" cleared.<ref>Stephens 2006, pp. 123–124.</ref><ref name="Cleaning the augean stables">Alexander 2004.</ref>

Prior to the "mutiny", Caldwell had been charged over his involvement in an alcohol racket on Morotai, where liquor was flown in by RAAF aircraft and then sold to the sizable U.S. forces contingent in the locality.<ref name="Cleaning the augean stables"/><ref>Shores 1999, p. 56.</ref> He was court martialled in January 1946 and reduced to the rank of [[Flight Lieutenant]]. Caldwell left the service in February.<ref>Watson 2005, pp. 228–239.</ref>

==Later years==
After the war, Caldwell was involved as a purchasing agent obtaining surplus aircraft and other military equipment from the U.S. [[Foreign Liquidation Commission]] in the [[Philippines]]. The aircraft and equipment were exported to Australia in 1946. After the successful conclusion of this venture, Caldwell joined a cloth import/export company in Sydney and shortly after became its managing director. He became a partner in 1953 and later served as chairman of the board. The firm, Clive Caldwell (Sales) Pty Ltd, achieved considerable success under Caldwell's direction and expanded through subsidiaries worldwide.<ref>Musciano 1966, p. 70.</ref>

Although in later life Caldwell "spoke modestly" about his wartime service, upon his death in Sydney on 5 August 1994,<ref>[http://www.awm.gov.au/fiftyaustralians/6.asp "Fifty Australians: "Killer" Caldwell."] ''Australian War Museum''. Retrieved: 19 June 2013.</ref> many Australians "mourned the passing of a true national hero".<ref>Musciano 1966, p. 71.</ref>

==Honours and awards==
<center>
[[File:Dso-ribbon.png|100x30px]]<!-- 218x60px -->
[[File:UK DFC w bar BAR.svg|100x30px]]

[[File:1939-45 Star.jpg|100x30px]]<!-- 218x60px -->
[[File:Air Crew Europe BAR.svg|100x30px]]
[[File:Africa Star N-AFRICA 42-43 BAR.svg|100x30px]]<!-- 100x30px, and correct colours, too -->
[[File:Pacific Star.gif|100x30px]]<!-- 100x30px, and correct colours, too -->

[[File:Defence Medal BAR.svg|100x30px]]<!-- 218x60px - find a 100x30px version -->
[[File:War Medal 1939–1945 (UK) ribbon.png|100x30px]]<!-- 100x30px -->
[[File:Australia Service Medal 1939-1945 BAR.svg|100x30px]]<!-- 218x60px - find a 100x30px version -->
[[File:Krzyz Walecznych Ribbon.png|100x30px]]<!-- 106x30px - find a 100x30px version -->
</center>

*[[Distinguished Service Order]] (DSO) – awarded 14 October 1943<ref name=DSO />
*[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] and Bar (DFC*) – both awarded 26 December 1941<ref name=DFCs />
*[[1939-1945 Star]] – campaign medal
*[[Air Crew Europe Star]] – campaign medal
*[[Africa Star]] with "North Africa 1942–43" Clasp – campaign medal
*[[Pacific Star]]
*[[Defence Medal (United Kingdom)|Defence Medal]]
*[[War Medal 1939–1945]] – campaign medal
*[[Australia Service Medal 1939-45]] – campaign medal
*[[Cross of Valor (Poland)|Polish Cross of Valour]] (''Krzyż Walecznych'')  – permission to wear granted 2 August 1942<ref name=KW />

==References==
===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Alexander, Kristen. [http://www.thefreelibrary.com/%22Cleaning+the+augean+stables.%22+The+Morotai+Mutiny%3F-a0123162109 "Cleaning the augean stables". The Morotai Mutiny?] ''Sabretache'' (Military Historical Society of Australia), 2004.
* Alexander, Kristen. ''Clive Caldwell: Air Ace''. Crows Nest, Australia: Allen & Unwin, 2006. ISBN 1-74114-705-0.
* Brown, Russell. ''Desert Warriors: Australian P-40 Pilots at War in the Middle East and North Africa, 1941–1943''. Maryborough, Queensland, Australia: Banner Books, 2000. ISBN 1-875593-22-5.
* Musciano, Walter. "Killer Caldwell: Australia's Ace of Aces." ''Air Progress''  Volume 19, No. 3, September 1966.
* [[George Odgers|Odgers, George]]. ''The Royal Australian Air Force: An Illustrated History''. Brookvale, Australia: Child & Henry, 1984. ISBN 0-86777-368-5.
* Pentland, Geoffrey. ''The P-40 Kittyhawk in Service''. Melbourne, Victoria, Australia: Kookaburra Technical Publications Pty. Ltd., 1974. ISBN 0-85880-012-8.
* RAAF Historical Section, "RAAF Base Darwin", in ''Units of the Royal Australian Air Force: A Concise History. Volume 1: Introduction, Bases, Supporting Organisations''. Canberra, Australia: Australian Government Public Service, 1995. ISBN 0-644-42792-2.
* Shores, Christopher. ''Aces High – Volume 2: A Further Tribute to the Most Notable Fighter Aces of the British and Commonwealth Air Forces in World War II''. London: Grub Street, 1999. ISBN 1-902304-03-9.
* Shores, Christopher and Clive Williams. ''Aces High''. London: Grub Street, 1994. ISBN 1-898697-00-0.
* Stephens, Alan. ''The Royal Australian Air Force: A History''. London: Oxford University Press, 2006. ISBN 0-19-555541-4.
* Watson, Jeffrey. ''Killer Caldwell''. Sydney, Australia: Hodder, 2005. ISBN 0-7336-1929-0.
* Weal, John. ''Jagdgeschwader 27 'Afrika'''. London: Osprey Publishing, 2003. ISBN 1-84176-538-4.
{{Refend}}

==External links==
{{commons category|Clive Caldwell}}
*[http://www.acesofww2.com/australia/caldwell/ Clive Caldwell at acesofww2.com]
*[http://www.awm.gov.au/people/18265.asp Australian War Memorial: Who’s Who in Australian Military History]
*[http://naa12.naa.gov.au/scripts/imagine.asp?B=5372784&I=1&SE=1 Clive Robertson Caldwell: Record of Service – Airmen] [[National Archives of Australia]]
*[http://naa12.naa.gov.au/scripts/imagine.asp?B=99760&I=1&SE=1 Record of Court Martial proceedings against Squadron Leader (acting Group Captain) Clive Robertson Caldwell], [[National Archives of Australia]]

{{Authority control}}
{{DEFAULTSORT:Caldwell, Clive}}
[[Category:1910 births]]
[[Category:1994 deaths]]
[[Category:Australian aviators]]
[[Category:Australian Companions of the Distinguished Service Order]]
[[Category:Australian World War II flying aces]]
[[Category:Businesspeople from Sydney]]
[[Category:People educated at Trinity Grammar School (New South Wales)]]
[[Category:People educated at Sydney Grammar School]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:Royal Australian Air Force officers]]
[[Category:Royal Australian Air Force personnel of World War II]]