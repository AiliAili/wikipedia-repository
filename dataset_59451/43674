{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = 7F.1 Snipe
  |image = Sopwith Snipe.jpg 
  |caption = [[William George Barker]]'s Snipe
}}{{Infobox Aircraft Type
  |type =  [[Fighter aircraft|Fighter]]
  |manufacturer = [[Sopwith Aviation Company]]
  |designer = [[Herbert Smith (engineer)|Herbert Smith]]
  |first flight = October 1917
  |introduced = 1918
  |retired = <!--date the aircraft left military or revenue service. if vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Air Force]]
  |more users = [[Royal Australian Air Force|Australian Flying Corps]]<br>[[Royal Canadian Air Force|Canadian Air Force]]  
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 497
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = [[Sopwith Salamander]]<br>[[Sopwith Dragon]]
}}
|}
The '''Sopwith 7F.1 Snipe''' was a [[United Kingdom|British]] single-seat [[biplane]] fighter of the [[Royal Air Force]] (RAF). It was designed and built by the [[Sopwith Aviation Company]] during the [[World War I|First World War]], and came into squadron service a few weeks before the end of the conflict, in late 1918.

The Snipe was not a fast aircraft by the standards of its time, but its excellent climb and manoeuvrability made it a good match for contemporary German fighters.

It was selected as the standard postwar single-seat RAF fighter and the last examples were not retired until 1926.

==Design and development==
In April 1917, [[Herbert Smith (engineer)|Herbert Smith]], the chief designer of the Sopwith Company, began to design a fighter intended to be the replacement for Sopwith's most famous aeroplane, the successful [[Sopwith Camel]].<ref name="AMOct90 p588">Lumsden ''Aeroplane Monthly'' October 1990, p. 588.</ref> The resultant design, called Snipe by Sopwith, was in its initial form a single-[[interplane strut|bay]] [[biplane]], slightly smaller than the Camel, and intended to be powered by similar engines.{{#tag:ref|Alternative engines proposed included the {{convert|150|hp|kW|abbr=on}} Bentley AR.1 (later known as the [[Bentley BR.1]], the 150&nbsp;hp [[Gnome Monosoupape]], the {{convert|130|hp|kW|abbr=on}} [[Clerget 9B]] or {{convert|110|hp|kW|abbr=on}} [[Le Rhône 9J]].<ref name="AIap74 p190">Bruce ''Air International'' April 1974, p. 190.</ref>|group=nb}} The pilot sat higher than in the Camel while the centre-section of the upper wing was uncovered, giving a better view from the cockpit. Armament was to be two [[Vickers machine gun]]s.<ref name="AIap74 p190-1">Bruce ''Air International'' April 1974, pp. 190–191.</ref><ref name="Davis p136">Davis 1999, p. 136.</ref> In the absence of an official order, Sopwith began construction of two prototypes as a private venture in September 1917. This took advantage of a licence that had been granted to allow construction of four [[Sopwith Rhino]] bomber prototypes, only two of which were built. The first prototype Snipe, powered by a [[Bentley BR.1|Bentley AR.1]] [[rotary engine]] was completed in October 1917.<ref name="AMOct90 p588"/><ref name="Davis p136"/> The second prototype was completed with the new, more powerful [[Bentley BR.2]], engine, which gave {{convert|230|hp|kW}} in November 1917. This promised better performance, and prompted an official contract for six prototypes to be placed, including the two aircraft built as private ventures.<ref name="AIap74 p191-2">Bruce ''Air International'' April 1974, pp. 191–192.</ref>

The third prototype to fly, [[United Kingdom military aircraft serials|serial number]] ''B9965'', had modified wings, with a wider centre-section and a smaller cutout for the pilot, while the fuselage had a fully circular section, rather than the slab-sided one of the first two aircraft, and the tail was smaller. It was officially tested in December 1917, reaching a speed of 119&nbsp;mph (192&nbsp;km/h), and was then rebuilt with longer-span (30&nbsp;ft (9.14 m)) two-bay wings (compared with the 25&nbsp;ft 9&nbsp;in (7.85 m) single bay wings).<ref name="AMOct90 p589">Lumsden ''Aeroplane Monthly'' October 1990, p. 589.</ref> This allowed the Snipe to compete for Air Board Specification A.1(a) for a high-altitude single-seat fighter. This specification required a speed of at least 135&nbsp;mph (225&nbsp;km/h) at 15,000&nbsp;ft (4,573 m) and a ceiling of at least 25,000&nbsp;ft (7,620 m) while carrying an armament of two fixed and one swivelling machine gun. An oxygen supply and heated clothing were to be provided for the pilot to aid operation at high altitude.<ref name="AIap74 p191">Bruce  ''Air International'' April 1974, p. 191.</ref><ref name="AMOct90 p588-9">Lumsden ''Aeroplane Monthly'' October 1990, pp. 588–589.</ref>

The Snipe was evaluated against three other fighter prototypes, all powered by the Bentley BR.2 engine: the [[Austin Osprey]] [[triplane]], the [[Boulton & Paul Bobolink]] and the [[Nieuport B.N.1]]. While there was little difference in performance between the aircraft, the Sopwith was selected for production, with orders for 1,700 Snipes placed in March 1918.<ref name="Bruce rfc p548">Bruce 1982, p. 548.</ref>

The Snipe's structure was heavier but much stronger than earlier Sopwith fighters. Although not a fast aircraft for 1918, it was very maneuverable, and much easier to handle than the Camel, with a superior view from the cockpit - especially forwards and upwards. The Snipe also had a superior rate of climb, and much better high-altitude performance compared to its predecessor, allowing it to fight Germany's newer fighters on more equal terms. Further modifications were made to the Snipe during the war and postwar. The Snipe was built around the [[Bentley BR2]] engine - the last rotary to be used by the RAF. It had a maximum speed of 121&nbsp;mph at 10,000&nbsp;ft compared with the Camel's 115&nbsp;mph (185&nbsp;km/h) at the same altitude and an endurance of three hours. Its fixed armament consisted of two [[.303 British|0.303]] in (7.7&nbsp;mm) [[Vickers machine gun]]s on the [[cowling]], and it was also able to carry up to four 25&nbsp;lb (11&nbsp;kg) bombs for [[ground attack]] work, identical to the Camel's armament. The design allowed for a single [[Lewis gun]] to be mounted on the centre section in a similar manner to those carried by the [[Sopwith Dolphin|Dolphin]] - in the event this was not fitted to production aircraft.

The Snipe began production in 1918, with more than 4,500 being ordered. Production ended in 1919, with just under 500 being built, the rest being cancelled due to the end of the war. There was only one variant, the '''Snipe I''', with production by several companies including Sopwith, [[Boulton & Paul Ltd]], [[Coventry Ordnance Works]], [[Napier & Son|D. Napier & Son]], [[Nieuport & General Aircraft|Nieuport]] and [[Ruston, Proctor and Company]].

Two aircraft were re-engined with a 320&nbsp;hp (239&nbsp;kW) [[ABC Dragonfly]] radial engine and these entered production as the [[Sopwith Dragon]]. An armoured version entered production as the [[Sopwith Salamander]].

==Operational history==

===First World War===
[[File:Sopwith 7F.1 Snipe.jpg|thumb|right|Sopwith Snipe]]
In March 1918, an example was evaluated by No.1 Aeroplane Supply Depot (No.1 ASD) at [[St-Omer]] in France. Lieutenant [[Leslie Norman Hollinghurst|L. N. Hollinghurst]] (later an [[Flying ace|ace]] in [[Sopwith Dolphin]]s, and an [[Air Chief Marshal]]) flew to 24,000&nbsp;ft in 45 minutes. He stated that the aircraft was tail heavy and had "a very poor rudder", but that otherwise manoeuvrability was good.<ref name="AIAp74 p195,06">Bruce ''Air Enthusiast International'' April 1974, pp. 195, 206.</ref>

The first squadron to equip with the new fighter was [[No. 43 Squadron RAF|No. 43 Squadron]], based at [[Fienvillers]] in France, which replaced its Camels with 15 Snipes on 30 August 1918. After spending much of September training, it flew its first operational patrols equipped with the Snipe on 24 September.<ref name="AIJn74 p290">Bruce ''Air Enthusiast International'' June 1974, p. 290.</ref> The Snipe also saw service with [[No. 4 Squadron RAAF|No. 4 Squadron]] [[Royal Australian Air Force|Australian Flying Corps]] (AFC) from October 1918. While 43 Squadron's Snipes saw relatively little combat, the Australians had more success, claiming five victories on 26 October and six on 28 October, while on 29 October, 4 Squadron claimed eight [[Fokker D.VII]]s destroyed and two more driven down out of control for the loss of one of 15 Snipes.<ref name="Davis p141-2">Davis 1999, pp. 141–142.</ref><ref name="AIJn74 p290-1">Bruce ''Air Enthusiast International'' June 1974, pp. 290–291.</ref> [[No. 208 Squadron RAF]] converted from Camels in November, too late for the Snipes to see action.<ref name="AIJn74 p291">Bruce ''Air Enthusiast International'' June 1974, p. 291.</ref>

One of the most famous incidents in which the Snipe was involved, occurred on 27 October 1918 when [[Canadians|Canadian]] Major [[William George Barker|William G. Barker]] attached to [[No. 201 Squadron RAF]] flew over the [[Forêt de Mormal]] in France. Barker's Snipe (No. ''E8102'') had been brought with him for personal evaluation purposes in connection with his UK-based training duties and was therefore operationally a "one-off". The engagement with enemy aircraft occurred at the end of a two-week posting to renew his combat experience as Barker was returning to the UK. While on his last operation over the battlefields of France, Major Barker attacked a two-seater German aircraft and swiftly shot it down. However, Barker was soon attacked by a formation of at least 15 Fokker D.VIIs, an aircraft widely considered to be the ultimate German fighter design of the First World War. The ensuing melee was observed by many Allied troops. In the engagement, Barker was wounded three times, twice losing consciousness momentarily, but managing to shoot down at least three D.VIIs before making a forced landing on the Allied front lines. Barker was awarded the [[Victoria Cross]] for this action.<ref name="Davis p142">Davis 1999, p. 142.</ref><ref>''Flight''  5 December 1918, p. 1369.</ref>  The fuselage of this Snipe is preserved at the [[Canadian War Museum]], Ottawa, Ontario.<ref name="AIJn74 p291"/>

===Postwar operations===
Following the [[Armistice with Germany]] that ended the First World War, Sopwith Snipes formed part of the [[British Army of the Rhine|British Army of Occupation]], returning to the United Kingdom in August/September 1919, while Snipes replaced Camels in four home defence squadrons based in the United Kingdom. This force was quickly run down, however, and by the end of 1919, only a single squadron, [[No. 80 Squadron RAF|No 80]] was equipped with the Snipe.<ref name="AMnov90 p666">Thetford ''Aeroplane Monthly'' November 1990, p. 666.</ref>

In 1919, the Snipe took part in the [[Allied intervention in the Russian Civil War|Allied intervention]] on the side of the [[White Movement|White Russians]] during the [[Russian Civil War]] against the [[Bolsheviks]], twelve Snipes being used by the RAF mission in north Russia.<ref name="Davis p144-5">Davis 1999, pp. 144–145.</ref> At least one of the RAF Snipes was captured by the Bolsheviks and pressed into service.<ref name="Robertson p137">Robertson 1970, p. 137.</ref>

Although the performance demonstrated by the Snipe was unimpressive (tests at [[RAF Martlesham Heath|Martlesham Heath]] in October 1918 had shown that the Snipe was inferior to the [[Martinsyde F.3]] and [[Fokker D.VII]]<ref name="AIJn74 p299">Bruce ''Air Enthusiast International'' June 1974, p. 299.</ref>), it was selected as the standard postwar single-seat fighter of the RAF almost by default, with the [[Martinsyde Buzzard]] development of the F.3 being 25 percent more expensive than the Snipe and relying on a French engine that was in short supply (the 300&nbsp;hp [[Hispano-Suiza 8]]), while the range of fighters powered by the ABC Dragonfly [[radial engine]] did not come to fruition owing to failure of that engine.<ref name="Mason fighter p138">Mason 1992, p. 138.</ref><ref name="AIJn74 p296">Bruce ''Air Enthusiast International'' June 1974, p. 296.</ref><ref name="AInov90 p665">Thetford ''Aeroplane Monthly'' November 1990, p. 665.</ref> The last Snipes were retired by that service in 1926.

The [[Canadian Air Force]] (CAF) operated the Snipe after the war, but it was phased out in 1923, a year before the [[Royal Canadian Air Force]] (RCAF) was formed.{{citation needed|date=February 2012}}

==Operators==
;{{AUS}}:
* [[Royal Australian Air Force|Australian Flying Corps]]
** [[No. 4 Squadron RAAF|No. 4 Squadron AFC]] in [[France]].
** [[No. 5 Squadron RAAF|No. 5 (Training) Squadron AFC]] in the [[United Kingdom]].
** [[No. 8 Squadron RAAF|No. 8 (Training) Squadron AFC]] in the United Kingdom.

;{{BRA}}:
* [[Brazilian Naval Aviation]]  operated 12 Snipes.<ref name="Davis p145">Davis 1999, p. 145.</ref>

;{{flag|Canada|1868}}:
* [[Canadian Air Force (1918–1920)|Canadian Air Force]]
** [[No. 1 Squadron RCAF]]

;{{USSR}}
*[[Soviet Air Force]] - Postwar.

;{{UK}}:
* [[Royal Flying Corps]] / [[Royal Air Force]]<ref name="Halleyp354">Halley 1980, p. 354.</ref>
{{col-begin}}
{{col-2}}
** [[No. 1 Squadron RAF]]
** [[No. 3 Squadron RAF]]
** [[No. 17 Squadron RAF]]
** [[No. 19 Squadron RAF]]
** [[No. 23 Squadron RAF]]
** [[No. 25 Squadron RAF]]
** [[No. 29 Squadron RAF]]
** [[No. 32 Squadron RAF]]
** [[No. 37 Squadron RAF]]
** [[No. 41 Squadron RAF]]
** [[No. 43 Squadron RAF]]
{{col-2}}
** [[No. 45 Squadron RAF]]
** [[No. 56 Squadron RAF]]
** [[No. 70 Squadron RAF]]
** [[No. 78 Squadron RAF]]
** [[No. 80 Squadron RAF]]
** [[No. 81 Squadron RAF]]
** [[No. 111 Squadron RAF]]
** [[No. 112 Squadron RAF]]<ref name="Davis p144">Davis 1999, p. 144.</ref>
** [[No. 143 Squadron RAF]]
** [[No. 201 Squadron RAF]]
** [[No. 208 Squadron RAF]]
{{col-end}}

==Survivors and reproductions==
[[File:Hendon 190913 Sopwith Snipe 05.jpg|thumb|''E6655'' at the [[Royal Air Force Museum London]]]]
Two complete, original, Sopwith Snipes survive. ''E6938'' resides in the [[Canada Aviation and Space Museum]] in Rockcliffe on the outskirts of [[Ottawa]]. Formerly owned by film star [[Reginald Denny (actor)|Reginald Denny]], it had been restored by Jack Canary, in California, in the 1960s.<ref name="AMnov90 p670">Thetford ''Aeroplane Monthly'' November 1990, p. 670.</ref> ''E8105'' is exhibited at the [[National Air And Space Museum]] in Washington, D.C. Previously, it had been one of the most cherished aircraft at the [[Old Rhinebeck Aerodrome]], in [[Rhinebeck (town), New York|Rhinebeck, New York]]. It passed to the NASM after [[Cole Palen]]'s death in December 1993.<ref>[http://www.nasm.si.edu/collections/artifact.cfm?id=A19940151000 "Sopwith 7F.1 Snipe"]. ''Smithsonian Air and Space Museum''. Retrieved 27 January 2012.</ref> The fuselage (minus landing gear, engine and cowl) of Major William G. Barker's Snipe, ''E8102'', resides at the Canadian War Museum, Ottawa.

===Reproductions===
Antique Aero in California has completed construction of an airworthy, very detailed reproduction Sopwith Snipe. It awaits a new-build 230 h.p. Bentley B.R.2 engine.

Another Snipe reproduction ''E8102'' has been built in New Zealand by The Vintage Aviator Ltd., and was subsequently purchased by Kermit Weeks for his Fantasy of Flight aviation museum in Polk City, Florida. This is a flying copy, complete with the original Bentley rotary engine.

The RAF Museum in August 2012 took delivery of a static display reproduction of the Snipe. Marked ''E6655'', it was constructed in the Wellington workshops of The Vintage Aviator Ltd. in New Zealand. The inclusion of original, non-functioning parts precludes this aircraft from flight status.

==Specifications (Snipe)==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |ref=, |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specification]]. -->
|ref=British Aeroplanes 1914–18<ref name="BruceBrit p613">Bruce 1957, p. 613.</ref>
|crew=one
|length main=19 ft 10 in
|length alt=6.05 m
|span main=31 ft 1 in
|span alt=9.48 m
|height main=9 ft 6 in
|height alt=2.90 m
|area main=271 ft²
|area alt=25.2 m²
|empty weight main=1,312 lb
|empty weight alt=596 kg
|loaded weight main=2,020 lb
|loaded weight alt=918 kg
|max takeoff weight main=
|max takeoff weight alt=
|engine (prop)=[[Bentley BR2]]
|type of prop=[[rotary engine]]
|number of props=1
|power main= 230 hp
|power alt=172 kW
|max speed main=121 mph
|max speed alt=105 knots, 195 km/h
|max speed more=at 10,000 ft (3,050 m)
|ceiling main=19,500 ft
|ceiling alt=5,945 m
|climb rate main= 
|climb rate alt= 
|more performance=
* '''Endurance:''' 3 hours
* '''Climb to 6,600 ft (1,980 m):''' 5 min 10 sec
*'''Climb to 15,000 ft (4,570 m):''' 18 min 50 sec
|guns=2× [[.303 British|0.303 in]] (7.7 mm) [[Vickers machine gun]]s
|bombs=4× 25 lb (11 kg) bombs
}}

==See also==
{{aircontent
|related=
* [[Sopwith Salamander]]
* [[Sopwith Dragon]]
|similar aircraft=
*[[Boulton Paul Bobolink]]
*[[Nieuport B.N.1]]
*[[Austin Osprey]]
*[[Armstrong Whitworth Armadillo]]
|see also=
|lists=
* [[List of aircraft of the Royal Air Force]]
}}

==References==

===Notes===
{{reflist|group=nb}}

===Citations===
{{reflist|30em}}

===Bibliography===
* Bruce, J. M. ''The Aeroplanes of the Royal Flying Corps (Military Wing)''. London: Putnam, 1982. ISBN 0-370-30084-X.
* Bruce, J. M. ''British Aeroplanes 1914–18''. London: Putnam, 1957.
* Bruce, J. M. "Sopwith Snipe...the RAF's first fighter: Part 1". ''[[Air International|Air Enthusiast International]]'', April 1974, Vol 6 No 4. Bromley, Kent, UK: Fine Scroll. pp.&nbsp;190–195, 206–207.
* Bruce, J. M. "Sopwith Snipe...the RAF's first fighter: Part 2". ''Air Enthusiast International'', June 1974, Vol 6 No 6. Bromley, Kent, UK: Fine Scroll. pp.&nbsp;289–299.
* Davis, Mick. ''Sopwith Aircraft''. Ramsbury, Malborough, UK: The Crowood Press, 1999. ISBN 1-86126-217-5.
* Franks, Norman. ''Dolphin and Snipe Aces of World War I (Aircraft of the Aces).'' London: Osprey Publishing, 2002. ISBN 1-84176-317-9.
* Halley, James J. ''The Squadrons of the Royal Air Force''. Tonbrige, Kent, UK: Air Britain (Historians), 1980. ISBN 0-85130-083-9.
* [http://www.flightglobal.com/pdfarchive/view/1918/1918%20-%201368.html "Honours: Two More VCs"]. ''[[Flight International|Flight]]'', 5 December 1918, Vol X No 49. p.&nbsp;1369.
* Lumsden, Alec. "On Silver Wings: Part 1". ''Aeroplane Monthly'', October 1990, Vol 18 No 10. London:IPC. ISSN 0143-7240. pp.&nbsp;586–592.
* Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.
* Robertson, Bruce. ''Sopwith–The Man and His Aircraft''. Letchworth, UK: Air Review, 1970. ISBN 0-900435-15-1.
* Thetford, Owen. "On Silver Wings: Part 2". ''[[Aeroplane Monthly]]'', November 1990, Vol 18 No 11. London:IPC. ISSN 0143-7240. pp.&nbsp;664–670.

==External links==
{{Commons category-inline|Sopwith Snipe}}

{{Sopwith Aviation Company aircraft}}
{{wwi-air}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Military aircraft of World War I]]
[[Category:Sopwith aircraft|Snipe]]