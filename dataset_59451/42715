<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Dyott monoplane
 | image=George Miller Dyott, c. 1913.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Sport and touring [[monoplane]]
 | national origin=United Kingdom
 | manufacturer=[[Hewlett & Blondeau]] Co. Ltd, London
 | designer=[[George Miller Dyott]]
 | first flight=early 1913
 | introduced=
 | retired=
 | status=
 | primary user=G.M Dyott
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Dyott monoplane''' was a single-engined, single-seat mid-wing [[monoplane]] designed by [[George Miller Dyott]] for his own use as a sports and touring aircraft. It proved successful, making a six-month tour of the United States soon after its first flight in 1913.

== Design ==
The Dyott monoplane was named after its designer and owner, George Dyott. He had earned [[Royal Aero Club]] [[List of pilots awarded an Aviator's Certificate by the Royal Aero Club in 1911|Aviators' Certificate]] (no. 114) in 1911 and designed his cross country machine the following year. One strength of the design was the simplicity of rigging and assembly, making it easy to transport by land or sea where necessary.  The machine was built by  Hewlett & Blondeau of [[Clapham, London]].<ref name="Lewis">{{harvnb|Lewis|1962|pages=233–5}}</ref><ref name="Flight">The Dyott Monoplane ''Flight'' 26 April 1913</ref>

The Dyott was a single-seat, mid-wing monoplane of clean appearance for its day.  The [[fuselage]] was built up around four [[longerons]].  These were of [[Fraxinus|ash]] in the stressed region from wing spars to engine, [[spruce]] at the rear and internally wire braced.  Stringers behind the cockpit formed a smooth rounded decking under the overall [[aircraft fabric covering|fabric covering]]. The forward fuselage, including the cockpit was [[aluminium]] clad, with a neat nose piece over the 7-cylinder, 50&nbsp;hp (37&nbsp;kW) [[Gnome rotary engine]], more to protect the pilot from oil than for streamlining. Steel tubing was used in several places: the empennage was steel framed, as was  the pilot's seat, and steel tubes formed the vertical [[Landing gear|undercarriage]] members.  There were four of the latter, each pair mounting a short wooden skid with steel cross bracing and a single axle on shock absorbers carrying a pair of wheels.  The undercarriage was initially completed with a long sprung tail skid,<ref name="Flight"/> later replaced by a shorter cane skid mounted further aft.<ref name="Lewis"/><ref name="Flight2">The Dyott monoplane at Hendon  ''Flight'' 1 November 1913</ref> At the same time, Dyott made some changes to the transverse bracing of the main undercarriage.<ref name="Flight2"/>

The low [[aspect ratio]] wings were parallel edged and almost square tipped, with the thin [[airfoil]] section typical of the time.  They were built around two [[Spar (aviation)|spars]], each a spruce-ash-spruce sandwich, and the profile was formed with mixed spruce and ash ribs. Both spars fitted into sockets formed by two transverse fuselage struts, but the rear socket was made oversize and the spar attached with a bolt and split pin to allow movement, as the Dyott was laterally controlled by [[wing warping]]. The main in-flight aerodynamic forces on each wing were carried by pairs of cables to the front spar from the forward skid mounting point, an arrangement that caused some concern<ref name="G&T">{{harvnb|Goodall|Tagg|2001|page=106}}</ref> over the transfer of landing loads to the wing. Each wing was braced from above with four wires, two to each spar, from an inverted V steel pylon just in front of the cockpit.  A short vertical steel post below the rear of the cockpit carried pairs of wing warping wires to the rear spar.<ref name="Flight"/>

The tailplane, attached to the top of the fuselage was triangular, without eternal bracing and carrying elevators with a V shaped gap to allow rudder movement.  The fin was very small and triangular; the rudder hinge ran from fin tip to the bottom of the fuselage.  Control wires ran externally from about halfway down the rear fuselage.<ref name="Flight"/>

The aircraft was completed early in 1913; testing was rapid and satisfactory.<ref name="Lewis"/><ref name="G&T"/>

== Operational history ==
Dyott took the monoplane on a tour of the United States immediately after flight testing was complete.  He flew over 2,000 miles (3,200&nbsp;km) between April and October 1913. The Dyott proved to have good performance ("It goes like a rocket", he wrote) and high reliability, giving demonstration flights across the US from [[New York (state)|New York]] to [[California]].<ref name="Lewis"/>

After returning from the US, he entered his monoplane into the London to Brighton handicap, which involved a round trip to [[Brighton]] from [[Hendon Aerodrome]], refuelling at [[Shoreham Airport|Shoreham]] near Brighton. A strong wind took him off course and required a landing at [[Beachy Head]].  This was done successfully, but the wind overturned the aircraft and damaged it, forcing Dyott's retirement.<ref name="Lewis"/>

Dyott planned to take the repaired machine on a tour of India.<ref name="Lewis"/>  This did not happen; instead, the monoplane was taken over by the Admiralty in 1914.<ref name="G&T"/>

The Dyott monoplane appeared in a set of cigarette cards issued by [[Lambert & Butler]] in 1915. 
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

== Specifications ==
{{Aircraft specs
|ref=<ref name="Flight"/>
<!-- for giving the reference for the data -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=23
|length in=0
|length note=
|span m=
|span ft=29
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=148
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Gnome rotary engine|Gnome]]
|eng1 type=7-cylinder rotary
|eng1 kw=<!-- prop engines -->
|eng1 hp=50<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=75
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=45<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=3 hr<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

== Notes ==
{{reflist}}

== References ==
{{refbegin}}
*{{cite book |title= British Aircraft before the Great War|last=Goodall|first=Michael H.|last2=Tagg|first2=Albert E.|year=2001|publisher=Schiffer Publishing Ltd|location=Atglen, PA, USA|isbn=0-7643-1207-3|ref=harv}}
*{{cite book |title= British Aircraft 1809-1914|last=Lewis|first=Peter| year=1962|volume= |publisher=Putnam Publishing |location=London |isbn=|ref=harv}}
*{{cite magazine |last= |first= |authorlink= |coauthors= |year= |month= |title= The Dyott monoplane.|magazine= [[Flight International|Flight]] |date= 26 April 1913 |volume=V |number=17|pages=454–7 |id= |url= http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200434.html |accessdate= |quote= }}
*{{cite magazine |last= |first= |authorlink= |coauthors= |year= |month= |title= The Dyott monoplane at Hendon.|magazine= [[Flight International|Flight]]|volume=V |number=44 |date= 26 April 1913|page=1139 |id= |url= http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201163.html |accessdate= |quote= }}
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:British sport aircraft 1910–1919]]
[[Category:Hewlett & Blondeau aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Rotary-engined aircraft]]
[[Category:High-wing aircraft]]