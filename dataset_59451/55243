{{Infobox journal
| cover = [[File:2009 cover of the journal Acta Neuropathologica.jpg|100px]]
| editor = Werner Paulus
| discipline = [[Neuropathology]], [[Clinical Neurology]], [[Pathology]], [[Neurosciences]]
| abbreviation = Acta Neuropathol. (Berl)
| publisher = [[Springer Science+Business Media]]
| frequency = Monthly
| history = 1961–present
| impact = 11.360
| impact-year = 2015
| website = http://www.springer.com/medicine/pathology/journal/401
| link2 = http://link.springer.com/journal/volumesAndIssues/401
| link2-name = Online archive
| ISSN = 0001-6322
| eISSN = 1432-0533
| OCLC = 610316690
| LCCN = sf78000750
| CODEN = ANLSBX
}}
'''''Acta Neuropathologica''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[neuropathology]] published by [[Springer Science+Business Media]]. It was established in 1961 and the [[editor-in-chief]] is Werner Paulus ([[University of Münster]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[PubMed]]/[[MEDLINE]]
* [[Scopus]]
* [[PsycINFO]]
* [[EMBASE]]
* [[Chemical Abstracts Service]]
* [[EBSCO Publishing|EBSCO databases]]
* [[CAB International]]
* [[Abstracts in Anthropology]]
* [[Academic OneFile]]
* [[Academic Search]]
* [[Biological Abstracts]]
* [[BIOSIS]]
* [[CSA Environmental Sciences]]
* [[Current Contents]]/Life Sciences
* [[Elsevier Biobase]]
* [[Global Health]]
* [[INIS Atomindex]]
* [[International Bibliography of Book Reviews]]
* [[International Bibliography of Periodical Literature]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 10.762.<ref name=WoS>{{cite book |year=2015 |chapter=Acta Neuropathologica |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/medicine/pathology/journal/401}}

{{DEFAULTSORT:Acta Neuropathologica}}
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1961]]
[[Category:Neurology journals]]