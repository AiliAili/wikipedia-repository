{{for|"The ACE ORB"|TAO (software)}}
{{multiple issues|
{{more footnotes|date=December 2009}}
{{context|date=June 2012}}
}}

{{Infobox software
| name=Tao Framework
| author=[[Randy Ridge]]
| latest release version=2.1
| latest release date={{Start date and age |2008|5|1}}
| programming language=[[C Sharp (programming language)|C#]]
| genre=[[Software library]]
| license = [[MIT License]]
| website={{URL|sourceforge.net/projects/taoframework/}}
}}
{{Portal|Free software}}
The '''Tao Framework''' is a [[C Sharp (programming language)|C#]] [[library (computer science)|library]] giving [[Microsoft .NET|.NET]] and [[Mono (software)|Mono]] developers access to popular graphics and gaming libraries like [[OpenGL]] and [[Simple DirectMedia Layer|SDL]]. It was originally developed by the C# OpenGL programmer Randy Ridge, and since its start many developers have contributed to the project. The latest version of Tao is version 2.1 released on May 1, 2008.

Tao Framework has been superseded by OpenTK.<ref>{{cite web|url=http://sourceforge.net/projects/taoframework/ |title=Tao Framework |publisher=sourceforge.net |date= |accessdate=2014-05-04}}</ref>

In 2012, in parallel with the development of OpenTK, a new project called [[TaoClassic]] has been introduced on SourceForge, as a direct continuation of Tao Framework, with the same licensing conditions and design disciplines, but with new authors and cutting-edge features, like support for OpenGL 4.3, 64-bit operating systems, etc.<ref>{{cite web|url=http://sourceforge.net/projects/taoclassic/ |title=Tao Classic |publisher=sourceforge.net |date= |accessdate=2012-12-22}}</ref>

== Bindings ==
* [[Cg (programming language)|Cg]] 2.0.0.0
* [[DevIL]] 1.6.8.3
* [[FFmpeg]] 0.4.9.0
* [[freeglut]] 2.4.0.2
* [[FreeType]] 2.3.5.0
* [[GLFW]] 2.6.0.0
* [[Lua (programming language)|Lua]] 5.1.3.0
* [[Open Dynamics Engine|ODE]] 0.9.0.0
* [[OpenAL]] 1.1.0.1
* [[OpenGL]] 2.1.0.12
* [[PhysFS]] 1.0.1.2
* [[Simple DirectMedia Layer|SDL]] 1.2.13.0

== References ==
{{Reflist}}

== External links ==
* {{sourceforge|taoframework/|Tao Framework}}
* {{sourceforge|taoclassic/|Tao Classic}}

{{Widget toolkits}}

[[Category:3D graphics software]]
[[Category:Mono (software)]]
[[Category:Software using the MIT license]]


{{software-stub}}