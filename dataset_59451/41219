{{Infobox Ethnic group
|group    = Pathans of Sri Lanka
|image    =
|caption  = 
|pop      = '''551'''<ref name="Haraprasad"/>
|poptime  = <small>1946</small>
|popplace = [[Colombo]]{{·}} [[Kandy]]{{·}} [[Batticaloa]]
|langs    = [[Pashto]]{{·}} [[Tamil language|Tamil]]{{·}} [[Sinhalese language|Sinhalese]]
|rels     = [[Islam]] ([[Sunni Islam|Sunni]])
|related  = [[Pathans of Tamil Nadu]]{{·}} [[Pashtun diaspora]]
}}
The '''Pathans of Sri Lanka''' are a [[Muslim]] community in [[Sri Lanka]] of [[Pashtun diaspora|Pashtun ancestry]].

==History==
===Early history===
[[Pashtuns|Pathan]] traders from modern [[Afghanistan]] and [[Pakistan]] arrived by boat in eastern Sri Lanka as early as the 15th century, via [[Indian subcontinent|India]].<ref name="Essed">{{cite book|title=Refugees and the Transformation of Societies: Agency, Policies, Ethics, and Politics|publisher=Berghahn Books|date=2004|isbn=9781571818669|pages=50–51|first=Philomena|last=Essed|first2=Georg|last2=Frerks|first3=Joke|last3= Schrijvers|url=https://books.google.com/books?id=Ixn_D2mhxyAC&pg=PA50}}</ref><ref name="Dennis">{{cite book|title=Crucible of Conflict: Tamil and Muslim Society on the East Coast of Sri Lanka|pages=73–77, 375|first=Dennis B.|last=McGilvray|publisher=Duke University Press|date=2008|isbn=9780822389187|url=https://books.google.com/books?id=MgHIiEtdVFAC&pg=PA75}}</ref> They landed in [[Batticaloa District|Batticaloa]], which was a key port.<ref name="Essed"/> Economic competition at the time led to frequent [[History of Eastern Tamils#Local sources|conflicts between Tamil fishing castes]], particularly over control of resources.<ref name="Dennis"/> One nearby village known as [[Eravur]], inhabited by [[Mukkuvar]]s, was the target of multiple attacks and looting during harvesting seasons by Thimilar folks from Batticaloa.<ref name="Essed"/> The Mukkuvars established an alliance with Batticaloa's Pathan warriors, enlisting their help to fend off the incursions and protect the village. The Thimilars were defeated and retreated northwards.<ref name="Essed"/>{{efn|In addition to the Pathans, it is known that there were also other Muslim trader groups who formed the alliance including Indian Moors, Turks and Arabs.<ref name="Dennis"/>}}

The Mukkuvar–Pathan alliance became a key part of local folklore and temple mythology.<ref name="Essed"/><ref name="Dennis"/> The Pathans were rewarded through marriages with local women, and settled in Eravur.<ref name="Essed"/> Their settlement may have been deliberate, so as to form a buffer against future invasions from the north.<ref name="Dennis"/> They achieved a strong social status, becoming prosperous landowners and merchants in the Batticaloa region.<ref name="Essed"/><ref name="Dennis"/> As the Pathans were small in number, they assimilated into the wider [[Islam in Sri Lanka|Muslim community]] and commonly self-identified as [[Sri Lankan Moors|Moors]].<ref name="Dennis"/><ref name="Smith">{{cite web|url=http://discovery.ucl.ac.uk/1317642/1/265119.pdf|title=Islamic Ideology and Religious Practice Among Muslims in a Southern Sri Lankan Town|first=Llyn|last=Smith|date=January 1997|accessdate=26 January 2017|work=Department of Anthropology, University College London|page=28}}</ref><ref>{{cite book|title=Politics of Tamil Nationalism in Sri Lanka|first=Ambalavanar|last=Sivarajah|publisher=South Asian Publishers|date=1996|isbn=9788170031956|page=23|url=https://books.google.com/books?id=PYVIPgAACAAJ|quote=Besides Moors, the Muslim community in Sri Lanka contains a sizeable number of Malays, Bohras, and Memons as well as recent migrants such as Coast Moors, Khojas and Afghans.}}</ref>

A similar history is recorded in [[Akkaraipattu]], where itinerant Pathan traders helped the Mukkuvars quell a group of [[Vedda]] bandits, thereafter settling there.<ref name="Dennis"/>

===Colonial period===
The arrival of Pathan settlers continued during the colonial era, mainly for purposes of trade.<ref name="Maharoof">{{cite book|title=An Ethnological Survey of the Muslims of Sri Lanka: From Earliest Times to Independence|first=M. M. M.|last=Mahroof|page=9|publisher=Sir Razik Fareed Foundation|date=1986|url=https://books.google.com/books?id=cAtDAAAAYAAJ|quote=The term "Afghan" refers to those people of the North-West Frontier region of the Indian sub-continent who have settled in the island. This includes Pathans and Baluchis who form the largest group amongst them.}}</ref> Difficult economic conditions in their [[Pashtunistan|native homelands]] may have prompted their migration to the subcontinent's southern regions in the 19th century.<ref name="Maharoof"/>

==Culture and religion==
Locally, the Pathans were also known as ''Pattani'' or simply ''[[Afghan (ethnonym)|Afghan]]''.<ref name="Dennis"/><ref name="Peebles">{{cite book|title=Historical Dictionary of Sri Lanka|first=Patrick|last=Peebles|url=https://books.google.com/books?id=50igCgAAQBAJ&pg=PA27|publisher=Rowman & Littlefield|date=2015|isbn=9781442255852|page=27}}</ref> They were adherents of [[Sunni Islam]], and mainly originated from the [[North-West Frontier Province (1901–55)|North-West Frontier Province]] and [[Baluchistan (Chief Commissioner's Province)|Baluchistan]] in [[British India]] (modern Pakistan).<ref name="Haraprasad"/><ref name="Shukri">{{cite book|title=Muslims of Sri Lanka: Avenues to Antiquity|first=M.A.M.|last=Shukri|publisher=Jamiah Naleemia Institute|year=1986|pages=54, 312|url=https://books.google.com/books?id=HgdDAAAAYAAJ|quote=At one time there was a flourishing group commonly referred to as "Afghans." These were Muslims from Baluchistan who carried out small money lending businesses... Along with their money lending business, there were others who worked as Watchers.}}</ref> Others came from Afghanistan.<ref name="Pippet">{{cite book|title=A History of the Ceylon Police, Volume 2|first=G. K.|last=Pippet|first2=A. C.|last2=Dep|publisher=Times of Ceylon Company|date=1969|page=144|url=https://books.google.com/books?id=TLr7LqbnQQoC|quote=The Afghans had originally come to Ceylon as horse- keepers from different parts of Afghanistan. On arrival some of them took to petty trading and penetrated into the remote Kandyan villages, taking for sale textiles of Indian manufacture... In 1880 on 13th and 14th November, nearly 150 Afghans gathered in Kandy for a festival and indulged in games of skill.}}</ref> The Pathans spoke [[Pashto]] and usually settled their disputes amongst themselves through a [[jirga]] system.<ref name="Maharoof"/> Because they were predominately men who had migrated for employment, leaving their families behind, most only stayed for a temporary period.<ref name="Shukri"/> Those who remained back established their households by marrying local women.<ref name="Shukri"/>

According to M.M. Maharoof, the colonial era Pathan immigrants were able to maintain their separate cultural identity.<ref name="Maharoof"/> Their relationship with other Muslims was mostly confined to "the precincts of the [[mosque]]."<ref name="Shukri"/> However, those who intermarried with local Muslims such as the [[Sri Lankan Moors|Moors]] and [[Sri Lankan Malays|Malays]] became completely integrated into their respective communities over time.<ref name="Maharoof"/><ref name="Shukri"/> Consequently, they adopted the local dialect and customs, and lost much of their ancestral links.<ref name="Maharoof"/>

==Demographics==
{{Historical populations
|type = 
|footnote = Source: Census of Sri Lanka.<ref name="1911Census"/>
|1881 | 1000
|1911 | 466
|1921 | 304
|1946 | 551
}}
In 1880, the Pathan population numbered around 1,000 in what was then [[British Ceylon]].<ref name="Maharoof"/>{{efn|The Census of Ceylon used the term "Afghan" to refer to individuals of Pathan extraction.<ref name="Peebles"/>}} They included horse keepers, textile merchants, traders, money lenders, and plantation workers in up-country estates.<ref name="Maharoof"/><ref name="Peebles"/> Around 300 of them were based in [[Kandy]], 100 in [[Trincomalee]] and Batticaloa, 150 in [[Colombo]], and the remaining 450 in [[Jaffna]], [[Kurunegala]], [[Badulla]], [[Haldummulla]] and [[Ratnapura]] districts.<ref name="Pippet"/> Patrick Peebles states that the Pathans were recognisable by their [[Pashtun clothing|distinctive clothing]] and turbans, and were the subject of discriminatory [[usury]] laws.<ref name="Peebles"/> They were notable for their skills in sport,<ref name="Pippet"/> and usually lived together.<ref>{{cite book|title=The Indian Review, Volume 36|first=G.A.|last=Natesan|authorlink=G. A. Natesan|publisher=G.A. Natesan & Co|date=1935|url=https://books.google.com/books?id=cuum1nmn4I4C|page=232|quote=There are the Muslims from Baluchistan who are called Afghans in Ceylon. They are chiefly engaged in lending money at high interest...}}</ref>

A 1911 Census report described them as follows:

{{Cquote|They are well known figures in the streets of Colombo and Kandy and in estate bazaars. They are tall and well formed... Their dress is distinctive – a loose tunic, baggy drawers, and thick boots. Their head dress is wound in rolls round the head, generally over a small skull-cap. There are some excellent wrestlers amongst them. They have their own chiefs and settle disputes amongst themselves. Their occupation they usually give as cloth sellers or horse traders, but their principal business is usury; they are the petty money lenders of the country.<ref name="1911Census"/>}}

In addition to plying trade in provincial cities like Colombo (including [[Slave Island]]) and Kandy, the community was also scattered itinerantly across hill country towns like [[Passara Electoral District|Passara]] and [[Bandarawela]].<ref name="Maharoof"/> In 1921, the population was recorded at 304 individuals, decreasing from 466 in the previous census.<ref name="1911Census">{{cite book|title=Census Publications: Ceylon, 1921|first=Lewis James Barnetson|last=Turner|publisher=H.R. Cottle|date=1923|page=228|url=https://books.google.com/books?id=Xi0PAQAAMAAJ|quote=Their numbers in 1921 show a decrease from 466 to 304, but it is clear that the persons generally known in Ceylon as " Afghans " have, in many cases, rightly or wrongly, been described as Baluchis, or some other Indian race.}}</ref> Many Pathans returned home during the 1940s.<ref name="Peebles"/>

The 1946 Census identified 551 Pathans living on the island, the majority of them concentrated in Colombo and urban centres.<ref name="Haraprasad">{{cite book|title=Ethnic Unrest in Modern Sri Lanka: An Account of Tamil-Sinhalese Race Relations|first=Haraprasad|last=Chattopadhyaya|publisher=M.D. Publications|date=1994|isbn=9788185880525|page=11|url=https://books.google.com/books?id=MRU6QKPBTFQC&pg=PA11}}</ref><ref name="Shukri"/> They were primarily money lenders and creditors, a profession not traditionally occupied by natives.<ref name="Haraprasad"/><ref name="Smith"/><ref name="Nira">{{cite book|title=Sri Lanka in the Modern Age: A History of Contested Indentities|first=Nira|last=Wickramasinghe|publisher=University of Hawaii Press|date=2006|isbn=9780824830168|pages=133, 139|url=https://books.google.com/books?id=Y-xQ8qk9mgYC&pg=PA133}}</ref> Some were recruited as guards, or worked in the [[Sri Lanka Post|postal service]].<ref name="Shukri"/> [[K. P. S. Menon (senior)|K. P. S. Menon]] notes that the Ceylonese "did not mind borrowing money" from the Pathan lenders, but kept them at a distance as they were known to charge high [[interest rate]]s. In fact, "almost the entire [[Sri Lanka Railways|railway]] staff was in debt" to them, and "shut their eyes when their creditors traveled without tickets."<ref name="Haraprasad"/> The non-payment of dues was a matter that often invoked the taking of law into their own hands, which made the Ceylonese wary of them a great deal.<ref name="Pippet"/><ref name="Nira"/>

==See also==
{{Portal|Sri Lanka|Pakistan|Afghanistan}}
* [[Islam in Sri Lanka]]
* [[Memons in Sri Lanka]]
* [[Pakistanis in Sri Lanka]]

==Notes==
{{notelist}}

==References==
{{reflist|2}}

{{Pashtun Diaspora}}
{{Immigration to Sri Lanka}}
{{Overseas Pakistani}}
{{Afghan diaspora}}

[[Category:Ethnic groups in Sri Lanka]]
[[Category:Islam in Sri Lanka]]
[[Category:Pakistanis in Sri Lanka]]
[[Category:Pashtun diaspora in Asia|Sri Lanka]]