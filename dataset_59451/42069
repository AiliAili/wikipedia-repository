<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=I-TEC Maverick
 | image=File:ITECMaverick.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Powered Parachute]], Roadable aircraft
 | national origin=[[United States]]
 | manufacturer=[[Indigenous People's Technology and Education Center]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost=$94,000 in 2012 <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''I-TEC Maverick''' is a [[powered parachute]] aircraft with a roadable [[fuselage]].
[[File:Maverick Flying Car.jpg|right|thumb|prototype flight]]

==Design and development==
Equipped with the largest powered parachute certified by the [[FAA]],{{Citation needed|date=June 2009}} the Maverick received [[experimental aircraft]] airworthiness certification on April 14, 2008, with [[Aircraft registration|N-Number]] 356MV.<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=356MV FAA N-number registry]</ref>

Capable of [[Interstate Highway System|interstate]] speeds on pavement, the Maverick's dune buggy-like frame of [[chromoly]] tubing gives it the ability to be used [[off-road]]. Additionally, the vehicle can deploy a [[parafoil]] and fly as a [[powered parachute]]. It weighs about 1100 pounds and has a useful carrying capacity equivalent to a [[Cessna 172]] (fuel and 550 pounds cargo). With a 22-foot mast, the Maverick can take off and land in weather conditions that other powered parachutes would not be able to safely operate in. Steve Saint has said he envisions the Maverick being useful to the Huaorani and other Indian groups, farmers and ranchers, [[pipeline transport|pipeline]] inspection crews and anyone else with a requirement to traverse rough, roadless ground. ''[[Popular Mechanics]]'' gave it their Breakthrough Award in 2009.<ref>{{cite web
  | last = Bolduan
  | first = Kate
  | title = Missionary builds flying car, FAA certifies it
  | publisher = CNN
  | date = 2010-10-27
  | url = http://religion.blogs.cnn.com/2010/10/27/missionary-builds-flying-car-faa-certifies-it/
  | accessdate = 2010-10-30}}</ref> In 2012, the Maverick was accepted by the United States FAA to operate as an Experimental homebuilt, S-LSA, or E-LSA.<ref>{{cite web|title=Maverick Specifications|url=http://www.mavericklsa.com/specifications.html|accessdate=10 August 2012}}</ref>

<!-- ==Operational history== -->

==Variants==
;Maverick URVATV (Ultimate Roadable, All-Terrain, Aerial Vehicle)
:Initial model
;Maverick HPAV (High Performance Aerial Vehicle)
:Transaxle removed, fan drive only
;Maverick HPRATV (High-Performance, Roadable, All-Terrain Vehicle )
:Wheel drive only, convertible to flight.
;Maverick XTRV (Xtreme Roadable Vehicle)
:{{convert|300|hp|kW|0|abbr=on}} wheel drive only.

==Specifications (Maverick) ==
{{Aircraft specs
|ref=Manufacturer<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=1
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=2.5l Subaru 
|eng1 type=Four cylinder
|eng1 kw=<!-- prop engines -->
|eng1 hp=190<!-- prop engines -->

|prop blade number=5<!-- propeller aircraft -->
|prop name=Warp Drive
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=40
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=3 hours
|ceiling m=
|ceiling ft=10,000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=600
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|I-TEC Maverick}}
{{reflist}}
<!-- insert the reference sources here -->
{{refend}}
<!-- ==External links== -->
{{Flying cars}}

[[Category:Homebuilt aircraft]]
[[Category:Powered parachutes]]
[[Category:Roadable aircraft]]