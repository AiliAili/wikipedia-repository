{{Orphan|date=March 2014}}
{{Infobox scientist
| name              = Helmut Paul
| native_name       = 
| native_name_lang  = 
| image             = Helmut Paul 2014.JPG
| image_size        = 
| alt               = 
| caption           = Helmut Paul, 2014
| birth_date        = {{birth date|1929|11|04}}
| birth_place       = [[Vienna]]
| death_date        = {{dda|2015|12|21|1929|11|4}}
| death_place       = [[Linz]]
| resting_place     = 
| resting_place_coordinates = <!-- 
                      {{Coord|LAT|LONG|type:landmark|display=inline,title}} -->
| other_names       = 
| residence         = [[Linz]]
| citizenship       = 
| nationality       = [[Austria]]n
| fields            = [[Nuclear physics]], [[Atomic physics]]
| workplaces        = [[Institute for Radium Research]], [[Vienna]], [[CERN]], Reactor Center [[Seibersdorf]], [[Brookhaven National Laboratory]], [[University of Linz]]
| patrons           =
| alma_mater        = [[Purdue University]]
| thesis_title      = On the attenuation of the angular correlation of gamma rays resulting from the decay of the 181Ta nucleus by time varying fields in liquid solutions
| thesis_url        = 
| thesis_year       = 1954
| doctoral_advisor  = Rolf M. Steffen
| academic_advisors = 
| doctoral_students = Erich Steinbauer, Robin Golser
| notable_students  = 
| known_for         = Internet table of stopping power data for positive ions
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = 
| signature         = <!--(filename only)-->
| signature_alt     = 
| website           = {{URL|www-nds.iaea.org/stopping}}<br>{{URL|www.exphys.jku.at/stopping}}<br>{{URL|andreas_paul.public1.linz.at/phpgedview/}}
| footnotes         = 
| spouse            = 
| children          = 
}}

'''Helmut Paul''' (born November 4, 1929 in [[Vienna]]; died December 21, 2015 in [[Linz]]) was an [[Austria]]n [[nuclear physics|nuclear]] and [[atomic physicist]]. He taught as a full professor of experimental physics at the [[Johannes Kepler University of Linz|University of Linz]] from 1971 to 1996. Since then he was professor emeritus. He was [[Rector (academia)|Rector]] of the University from 1974 to 1977.

== Life and work ==

Helmut Paul was born in 1929 as child of Hans and Ilona Paul (née Just) in [[Vienna]]. Both parents were of middle class origin. The father was employed in the accounting and financial sector of the [[Siemens]] company,<ref name="Happy Physicist">[http://www.sciencedirect.com/science/article/pii/0168583X96800838 Mitio Inokuti: ''Helmut Paul, a happy physicist''], Nuclear Instruments and Methods B 115 (1996) xiii</ref> the mother worked first in the household and later as an interpreter in the American Embassy in Vienna.

Helmut Paul was an excellent pupil, and it became soon apparent that he was gifted in mathematics. He received his secondary education (Gymnasium) partly in Berlin, partly in Gmunden, Upper Austria, and got his Matura (Abitur) in Vienna in 1947. Paul began the study of physics and mathematics at the [[University of Vienna]] in the fall of 1947. Among his professors in mathematics were the world-renowned mathematicians [[Johann Radon]] and [[Edmund Hlawka]]; there were friendly private relations to Radon and his family. Among his professors in physics in Vienna were [[Hans Thirring]], [[Felix Ehrenhaft]] and later the nuclear physicist [[Berta Karlik]].

Paul spent the year 1950/51 with a fellowship from the [[US State Department]] at the Graduate School of [[Purdue University]] in [[Lafayette (Indiana)|Lafayette]], USA.<ref name="Happy Physicist"/> Paul wrote a master's thesis on the efficiency of [[Geiger counter]]s and received at the conclusion of this academic year the degree of [[Master's degree|Master of Science]]. The supervisor of this thesis, Professor Rolf M. Steffen, told Paul that he would gladly be willing to supervise Paul's doctoral studies.

Already in spring 1952, Paul was back at Purdue University, this time for doctoral studies  (Ph.D.), which he concluded in December 1954 with a thesis "On the attenuation of the angular correlation of gamma rays resulting from the decay of the <sup>181</sup>Ta nucleus by time varying fields in liquid solutions".<ref name="My Life">http://www.jku.at/iep/content/e105784/e105821/index_html?team_view=3Dsection&emp=e105821/employee_groups_wiss105836/employees107702</ref> After returning to Vienna, Paul obtained a half time position at the [[Institute for Radium Research, Vienna]], of the  [[Austrian Academy of Sciences]], which was directed by Professor Karlik. This employment was of great significance for Paul privately as well: He met the secretary of Professor Karlik, Maria Elisabeth Mathis (1931 - 2008), and fell in love with her.<ref name ="Happy Physicist" /> Helmut Paul und Elisabeth Mathis got engaged in December 1956; the wedding took place in June, 1957. In time, three children came from this marriage.

In October 1957, Paul took on a [[Ford Foundation]] fellowship at [[CERN]] in [[Geneva]] which Berta Karlik hat procured for him. At CERN, the first particle accelerator, the [[synchrocyclotron]], had just started operation, and Paul got the chance to work on the first experiment that was carried out with this machine.<ref name ="My Life" /> It consisted of the search for the rare decay of the charged [[pion]] into an [[electron]] and a [[neutrino]], which was indeed found to occur with a probability of 0.012% relative to the normal decay into [[muon]] and neutrino.<ref>[http://link.aps.org/doi/10.1103/PhysRevLett.1.247 T. Fazzini, G. Fidecaro, [[Alec Merrison|A. W. Merrison]], H. Paul, and A. V. Tollestrup: ''Electron Decay of the Pion'', Physical Review Letters 1 (1958) 247]</ref><ref>J. Ashkin, T. Fazzini, G. Fidecaro, A. W. Merrison, H. Paul, A. V. Tollestrup: ''The Electron Decay Mode of the Pion'', Il Nuovo Cimento Ser. X, 13 (1959) 1240</ref>
After two very fruitful years in Geneva, Paul went a third time to Purdue, this time as a visiting professor, supplying for Professor Steffen. There he studied a beta-gamma angular correlation.<ref name="Happy Physicist" /><ref>Helmut Paul: ''Beta-Gamma Directional Correlations in the Decay Sb124 → Te124'', Physical Review 121 (1961) 1175</ref> In the meantime, a new nuclear research center was established in [[Seibersdorf]] near Vienna, where close colleagues from the [[Institute for Radium Research, Vienna|Radium Institute]] (Rupert Patzelt) and the 2nd Physics Institute of Vienna University (Peter Weinzierl) were active. Weinzierl offered him a position in Seibersdorf, und Paul accepted.<ref name="My Life" /> Paul was employed at the Seibersdorf center from October 1960 to March 1971, interrupted by a fourth stay in the USA, this time at the [[Brookhaven National Laboratory]] on [[Long Island]] (1965/66).

In Seibersdorf, Paul had a magnetic intermediate-image beta ray spectrometer at his disposal.  With it, he measured the shapes of beta ray spectra. Above all, he investigated the spectrometer itself, in order to show that the measured shapes are not due to distortions produced by the apparatus.<ref>[http://www.sciencedirect.com/science/article/pii/0029554X65903460 H. Paul: ''Instrumental distortion of beta spectra measured in an intermediate-image spectrometer'', Nucl. Instrum. Methods 37 (1965) 109]</ref>  An essential part of his work was devoted to the measurement of the electron-neutrino angular correlation in [[neutron]] decay, a difficult project that went on for years and was finally concluded when Paul was no longer at Seibersdorf.<ref>[http://link.aps.org/doi/10.1103/PhysRevD.11.510 R.Dobrozemsky, E.Kerschbaum, G.Moraw, H.Paul, C.Stratowa, P.Weinzierl, ''Electron-neutrino angular correlation coefficient a measured from free-neutron decay'', Physical Review D 11 (1975) 510]</ref><ref>[[ASTRA (reactor)|The ASTRA reactor at Seibersdorf]]</ref>
In Brookhaven, Paul attempted to find a possible [[Parity (physics)|parity]] admixture in an excited state of a radioactive [[hafnium]] isotope, which would manifest itself by a small [[circular polarization]] of the emitted [[Gamma ray|gamma radiation]]. The result was negative: there was no circular polarization.<ref>H. Paul, M. McKeown, and G. Scharff-Goldhaber, "Search for Parity Admixture in the 57.5-keV Gamma Transition of Hf180m", Phys. Rev. 158, 1112–1117 (1967)</ref> While at Brookhaven, Paul also published a summarizing article on the shapes of beta spectra which he had already begun at Seibersdorf.<ref>Helmut Paul: ''Shapes of Beta Spectra'', Nuclear Data A2 (1967) 281</ref>
In 1970, Paul received a call to the young  [[Johannes Kepler University of Linz|University of Social Sciences, Economics and Business]] in [[Linz]] (from 1975: Johannes Kepler University of Linz) for the newly established chair of Experimental Physics. Paul had the chance to cooperate in the planning of his professorship even prior to beginning his office. Since no chair for experimental physics had existed before, everything had to be built up from scratch (teaching, laboratories, electronics, mechanical workshop).

Paul started his professorship on April 1, 1971. Soon an electrostatic [[particle accelerator]] for 700 keV protons was installed (later also a [[Particle accelerator|tandem accelerator]]), und in collaboration with O. Benka, D. Semrad, A. Kropf and others, atomic physics experiments were started. To make the Linz group internationally known, Paul started to organize international workshops: at first three on theories of [[Ionization|ionisation]] of inner [[Atomic orbital|atomic shells]].<ref>Proc. Workshop on Theories of Inner Shell Ionization by Heavy Particles, Nucl. Instr. and Meth., Linz, Austria, May 17–19, 1979, 169 (1980), p. 249</ref><ref>Proc. Second Workshop on Inner Shell Ionization by Light Ions, Nucl. Instr. and Meth., Linz, Austria, March 12–14, 1982, 192 (1982), p. 1</ref><ref>Proc. Third Workshop on Inner Shell Ionization by Light Ions, Nucl. Instr. and Meth., Linz, Austria, August 4–5, 1983, 232 (1984), p. 211</ref> A table of cross sections for K shell ionisation by light ions, established together with J. Muhr and O. Bolik, resp., is available in the Internet.<ref>[http://www.exphys.jku.at/Kshells/ Cross Sections for K-Shell Ionization by Light Ions]</ref>
Later, Paul's interests turned toward the [[Stopping power (particle radiation)|stopping power]] of matter for charged particles, a subject on which D. Semrad, P. Bauer, R. Golser and other coworkers had already worked intensively for some time. He initiated again three international workshops on this theme.<ref>Proc. Workshop on Stopping Power for Low Energy Ions, Linz. Austria. September 20–21, 1984, Nucl. Instr. and Meth. B 12 (1985)</ref><ref>Proc. Second Workshop on Stopping Power for Low Energy Ions, Linz, Austria, September 18–19. 1986. Nucl. Instr. and Meth. B 27 (1987) 249</ref><ref>Proc. Gmunden Workshop on Aggregation and Chemical Effects in Stopping, Nucl. Instr. and Meth. B 93 (1994)</ref>
In Juli 1995,  Paul's group initiated the Sixteenth International Conference on Atomic Collisions in Solids in Linz, with Paul as Chairman; D. Semrad, P. Bauer and O. Benka were editors of the conference volume.<ref>''Sixteenth International Conference on Atomic Collisions in Solids'', Nucl. Instr. Methods 115 (1996) Issues 1–4, Pages 1–608</ref>
The years in Linz were a very successful time for Paul. Apart from teaching and research, Paul's managing qualities were appreciated and asked for. Paul's equilibrated and confidence-inspiring personality<ref name="Happy Physicist"/> contributed to the fact that Paul became a Senator at the University as early as 1972. In 1973 he was elected Dean of the newly established Faculty of natural and technical sciences, and in 1974 there came the election to the office of the Rector of the university for three years until 1977. In 1985, he was again elected Dean for two years.

Paul's interest (as professor emeritus since 1996) now turned also to themes of medical physics.<ref>Paul H., Jäkel O., Geithner O.: ''The influence of Stopping Powers upon Dosimetry for Radiation Therapy with Energetic Ions'', Advances in Quantum Chemistry (2007) Vol. 52, pp. 289–306</ref><ref>H. Paul: ''On the Accuracy of Stopping Power Codes and Ion Ranges Used for Hadron Therapy'', in: Dzevad Belkic (Ed.): Theory of Heavy Ion Collision Physics in Hadron Therapy, Adv. Quantum Chem., Vol. 65 (2013), pp. 39–59, Elsevier/Academic Press</ref> He was co-author of several reports of the [[International Commission on Radiation Units and Measurements]] (ICRU)<ref>''Stopping of Ions heavier than Helium'', ICRU Report 73, Journal of the ICRU (2005)</ref> and of a report of the [[International Atomic Energy Agency]].<ref>M. Berger, H. Paul: ''Stopping powers, ranges and straggling'', Chapter 7 of "Atomic and molecular data for radiotherapy and radiation research", IAEA-TECDOC-799 (1995)</ref> Even after his retirement from active university duty in 1996, Paul received invitations to specialized conferences abroad, notably to the USA (last time 2012), but also to Brazil (2011).

In 1990, Paul began to establish a collection of all published Stopping Power Data for light [[Ion]]s, with many graphical displays, and to install it in the Internet.<ref>[http://www.exphys.jku.at/stopping/ Stopping Power for Light Ions: Graphs, Data, Comments and Programs]</ref> In the meantime he has extended this collection to all positive ions, and he kept it updated till his death. It was important to him to compare these data statistically with various theories, in order to judge the quality of the data (and of the theories).<ref>[http://www.sciencedirect.com/science/article/pii/S0168583X11006707 Helmut Paul: ''Comparing experimental stopping power data for positive ions with stopping tables, using statistical analysis'', Nucl. Instr. Methods B 273 (2012) 15]</ref> His private interests included extended travels, the participation in a church choir and work on the genealogy of his family.<ref>[http://andreas_paul.public1.linz.at/phpgedview/ Helmut Paul’s Genealogy]</ref>

== Selected publications ==
* [http://www.jku.at/iep/content/e105784/e105821/index_html?team_view=section&t=1&emp=e105821/employee_groups_wiss105836/employees107702 List of publications since 2000, and autobiography]

== External links ==
* [http://www.exphys.jku.at/stopping H. Paul: "Stopping Power Graphs" on JKU sever]
* [https://www-nds.iaea.org/stopping/ H. Paul: "Stopping Power Graphs" on IAEA server]
* [http://andreas_paul.public1.linz.at/phpgedview/ Helmut Paul's Genealogy]

== Footnotes ==

{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Paul, Helmut}}
[[Category:Austrian physicists]]
[[Category:20th-century Austrian people]]
[[Category:1929 births]]
[[Category:2015 deaths]]
[[Category:People associated with CERN]]