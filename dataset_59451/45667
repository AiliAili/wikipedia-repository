{{BLP sources|date=September 2012}}

'''Hsue-Chu Tsien''', [[Colonel|COL]]<ref name="COL Tsien">{{cite web|url=http://book.ifeng.com/lianzai/detail_2009_11/30/292580_55.shtml |title=Biography of H. C. Tsien |format=shtml |publisher=[[Phoenix Television]], Hong Kong | accessdate=July 10, 2010|language=zh}}</ref> ('''H.C. Tsien''', 1914–1997; {{zh|t=錢學榘|s=钱学榘|p=Qián Xuéjù}}), was an [[Aerospace engineering|aeronautic]] and [[Mechanical engineering|mechanical]] [[engineer]] who played important roles in [[Aircraft industry|aircraft building]] in both China and afterward the United States.

==Biography==
[[File:China Motor Corporation top crew.jpg|400px|thumb|right|H.C. Tsien (sitting row right) was in front of the China Motor Corporation together with his Chinese and American colleagues. Only Tsien was in [[military uniform]]. [[Tung Hua Lin]] was in the center of the sitting row. <br />China Motor Corp (different from the current ''[[China Motor Corporation]]'' in [[Taipei]], [[Taiwan]]) was established during the [[World War II]] with American support, and it was the first Chinese factory of manufacturing [[jet engine]]s. The Chinese title of the factory (中國發動機厰) was written ([[Chinese calligraphy|calligraphy]]) by the famous Chinese scholar [[Hu Shih]].]]

Tsien was born in 1914 in [[Hangzhou]], the capital city of [[Zhejiang Province]], China. Tsien was a 33rd-generation descendant of [[Qian Liu]], King of [[Wuyue]].<ref>[[Sohu]].com Culture: [http://cul.sohu.com/20081009/n259935350.shtml The history of Tsien family]</ref> Tsien studied at Zhejiang Anding School (浙江安定學堂; now Hangzhou No.7 High School) and [[Hangzhou High School]].{{citation needed|date=April 2014}}

Tsien was admitted to the National Chekiang University (current [[Zhejiang University]]) in Hangzhou. In 1931 August, Tsien chose the [[Shanghai Jiao Tong University|National Chiao Tung University]] in Shanghai, and enrolled at the School of Mechanics. Tsien completed his [[internship]] at the National Central Aviation Academy (國立中央航空學校) at the [[Hangzhou Jianqiao Airport|Jianqiao Airfield]] in suburban Hangzhou.

Tsien graduated the top of 161 from the school of mechanics. Soon after graduation Tsien went to [[Beiping]] (now [[Beijing]]) and became a [[teaching assistant]] at [[Tsinghua University]] where he taught undergraduate courses for one year.

In August 1935, Tsien won the [[Boxer Rebellion Indemnity Scholarship Program|Boxer Rebellion Indemnity Scholarship]]. He went to study [[mechanics]] and [[aeronautics]] in the United States. In 1937, Tsien obtained his master's degree in [[aeronautical engineering]] from the [[Massachusetts Institute of Technology]]. In the same year, the [[Second Sino-Japanese War]] broke out, Tsien continued staying in the United States and was in charge of importing plants for the Central Machinery Factory (中央機器厰) of the [[National Resources Commission]] for the war-time China. Based on the [[Allies of World War II|Allies]]' agreement, those plants were used to support the battlefront of the war.

In 1938, Tsien went to work in [[Switzerland]] and returned in 1939. Tsien was pointed the deputy chief-engineer (the [[Chief Engineer]] was [[Y. T. Li]]) of the aircraft engine factory of the National Air Aviation Commission (國家航空委員會), which was the most important aircraft manufacturer of the [[Republic of China]] during [[World War II]]. Because of the invasion of [[Imperial Japanese Army|Japanese army]], the factory was relocated in Dading, [[Guizhou Province]], the rear area and one of the logistic centers of China during the war.

In 1943, Tsien was sent to [[India]], and was responsible for organizing and importing necessary supplies into west China through India, to support the war. Because the east coastline and [[Manchukuo|northeast China]] were already occupied by Japanese army, and the [[Indochina]] route was the only open way that [[Allies of World War II|Allies]] can deliver supplies to China. Tsien also participated in the design and manufacture of vehicles and aircraft for battle and transport uses. Tsien was ranked  [[Colonel]] of the [[Republic of China Air Force]].<ref name="COL Tsien"/> Tsien made contributions for Allies to the [[Surrender of Japan|victory]] of the World War II East Asian battlefield.<ref>[[Shanghai Jiao Tong University]] Archive: [http://sjtu.cuepa.cn/show_more.php?doc_id=96050 1935 Alumni: Hsue-Chu Tsien]</ref>

After the end of the Second World War, Tsien and his family settled in the United States. Due to the queasy atmosphere of the following [[Cold War]] and China's extreme turmoil, Tsien [[United States nationality law|naturalized as an American citizen]] after 1949. Tsien later served as the chief engineer of [[Boeing]].

In 1997, Tsien succumbed to cancer in California at age 83.

==Legend of Tsien's family==

===H.C. and H.S. Tsien===
Tsien's cousin, the famous aerospace scientist and engineer [[Tsien Hsue-shen|Tsien Hsue-shen ('''H.S.''')]], also played important roles both in the United States and China (however, quite opposite to Hsue-Chu Tsien's first-ROC-then-USA, H.S. first served the United States and later the PRC). H.S. was a co-founding father of the [[Jet Propulsion Laboratory]] of [[NASA]] (managed by [[Caltech]]). The two cousins had totally different destinies. After 1949, Hsue-Chu Tsien decided to stay and work in the United States; however H.S., widely regarded as a [[McCarthyism#Victims of McCarthyism|victim]] of the [[McCarthyism]] during the 1950s, was deported to the communist China in 1956. H.S. became the prominent leader of China's rocket and missile programs. In 1979, H.S. was awarded the prestigious ''Distinguished Alumni Award'' of [[Caltech]], which he received in 2001.<ref>[[Caltech]]News (Volume 36, Number 1, 2002): [http://pr.caltech.edu/periodicals/CaltechNews/articles/v36/tsien.html Tsien Revisited]</ref> H.S., a senior academician of both the [[Chinese Academy of Sciences]] and [[Chinese Academy of Engineering]], died in Beijing on October 31, 2009 at age 97.

Hsue-Chu Tsien and H.S. not only shared the same paternal grandfather, but also were schoolmates. They matriculated at the same university ([[National Jiaotong University|Jiaotong University]]) and had the same major. H.S. was one year senior.

Both Tsiens won the Boxer Rebellion Indemnity Scholarship and went to study in the United States in the 1930s, with H.S. went to USA in August 1934, and Hsue-Chu Tsien followed after one year (in August 1935). They became school mates again in the United States, both studied at MIT, but H.S. later transferred to [[Caltech]].

During World War II, H.C. was given the [[military rank]] of Air Force [[Colonel]] by the [[Republic of China]] when he served as the Deputy [[Chief Engineer]] at the Dading Airplane Factory (大定飛機製造廠) in Dading, [[Guizhou Province]]. Approximately at the same time, H.S. was also temporarily given the military rank as Air Force Colonel, but by the [[United States Army]].<ref>Obituary - Tributes.com: [http://www.tributes.com/show/Qian-Xuesen-87059818 Qian Xuesen 1911 - 2009]</ref> In 1956 during the [[Sino-Soviet relations|Sino-Soviet negotiation]], H.S. was again temporarily granted military rank but as the [[Lieutenant General]] of the [[People's Liberation Army]]; it was nominated by [[Zhou Enlai]] and approved by Chairman [[Mao Zedong]].<ref>[[Sina.com]]: [http://news.sina.com.cn/c/2009-11-01/023218949356.shtml Qian Xuesen's temporary military rank]</ref>

===The science and engineering dynasty===
Hsue-Chu Tsien married Yi-Ying Tsien (Née: [[Li (李)|Li]]; 李懿颖), also an MIT [[alumnus]], and whose brothers were also MIT alumni and [[engineering]] professors at MIT. Two of Yi-Ying Tsien's brothers [[S. Y. Lee]] and [[Y. T. Li]] are members of the [[National Academy of Engineering]]. Y. T. Li's wife, T.D. Lin (林同端), is the sister of [[Tung-Yen Lin]] who was also a member of the National Academy of Engineering and received the [[National Medal of Science]] in 1986 from President [[Ronald Reagan]]. Tung-Yen Lin's cousin [[Tung Hua Lin]] was also a member of the National Academy of Engineering and received the [[Theodore von Karman Medal]] in 1988. Tung Hua Lin's son [[Robert Lin]] is a member of [[United States National Academy of Sciences|National Academy of Sciences]].

Hsue-Chu Tsien had three sons, the oldest [[Richard W. Tsien]] is a famous [[neurobiologist]] at [[Stanford University|Stanford]] who's also member of both the [[United States National Academy of Sciences]] and [[Institute of Medicine]]; the youngest - [[Roger Y. Tsien]] is a [[Cell biology|cell biologist]] who is the 2008 [[Nobel Prize in Chemistry|Nobel Chemistry Prize]] [[List of Nobel laureates in Chemistry|Laureate]].<ref>Nobelprize.org: [http://nobelprize.org/nobel_prizes/chemistry/laureates/2007/index.html The Nobel Prize in Chemistry 2007]</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Tsien, Hsue-Chu}}
[[Category:1914 births]]
[[Category:1997 deaths]]
[[Category:American engineers]]
[[Category:Boxer Indemnity Scholarship recipients]]
[[Category:Deaths from cancer in California]]
[[Category:Chinese emigrants to the United States]]
[[Category:Massachusetts Institute of Technology alumni]]
[[Category:Tsien family]]
[[Category:Tsinghua University faculty]]
[[Category:Educators from Hangzhou]]