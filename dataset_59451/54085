{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
| name     = Blair Athol
| city     = Adelaide
| state    = sa
| image    = Grand_junction_rd_blair_athol.jpg
| caption  = Grand Junction Road
| lga      = City of Port Adelaide Enfield
| postcode = 5084
| est      = 
| pop            = 4,373
| pop_year = {{CensusAU|2011}}
| pop_footnotes  = <ref name=Census2011>{{Census 2011 AUS | id = SSC40052 | name = Blair Athol (State Suburb)|quick = on | accessdate=16 February 2015}}</ref>
| pop2           = 3,970
| pop2_year = {{CensusAU|2006}}
| pop2_footnotes = <ref name=Census2006>{{Census 2006 AUS | id = SSC41146 | name = Blair Athol (State Suburb)|quick = on | accessdate=16 February 2015}}</ref>
| area     = 
| stategov = 
| fedgov   = 
| near-nw  =
| near-n   = 
| near-ne  = 
| near-w   = 
| near-e   = 
| near-sw  = 
| near-s   = 
| near-se  = 
| dist1    = 
| location1= 
}}
'''Blair Athol''' is located about {{convert|15|km|0|abbr=on}} north of the [[Adelaide CBD]], South Australia. Blair Athol borders the suburbs of [[Gepps Cross, South Australia|Gepps Cross]], [[Enfield, South Australia|Enfield]], [[Prospect, South Australia|Prospect]] and [[Kilburn, South Australia|Kilburn]]. Blair Athol's main and longest street is Florence Avenue.

==History==
Blair Athol Post Office opened on 15 June 1955 and was renamed ''Blair Athol West'' in 1966. At that time the existing ''Enfield'' office (open since 1852) was renamed ''Blair Athol'' and it was replaced by the ''Enfield Plaza'' office in 1997.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Reserves==
Blair Athol has two reserves, Blair Athol Recreational Reserve and Barton Street Reserve.  The Blair Athol Recreational Reserve is a large reserve covering a total [[area]] of approx. 46,800 sq. metres (180 x 260 metres). It features a [[Australian rules football|football]] oval, [[cricket]] ground with batting cages, two [[netball]]/[[basketball]] courts and [[tennis]] courts which are exclusively used by members of the Kilburn tennis club. This reserve also features a large parking lot, 2 playgrounds with one to the north and the other to the south, a gazebo and the '''Kilburn Footy Club'''. Barton Street Reserve features a large grass field and a playground with several benches. This reserve covers a total area of approx. 18,400 Sq metres (230 x 80 metres).

==Government==
Blair Athol is run by the [[City of Port Adelaide Enfield]]. The council was set up in 1996 and one of its main offices is located in [[Enfield, South Australia|Enfield]]. The South Australian Education Department helped set up the school system in Blair Athol and [[Kilburn, South Australia|Kilburn]].
Blair Athol has 3 main schools. Gepps Cross Primary School and St. Paul Lutheran Primary School cater for levels 1-7. Gepps Cross Senior School is the only secondary school in Blair Athol and is partners with [[Enfield High School]].

==Shopping==
Blair Athol has one of the few family owned hardware stores left Randall's Hardware next to another family owned business Pets Everywhere.
Blair Athol had a [[Coles (Australia)|Coles]] [[supermarket]] located on the corner of Florence Avenue and Prospect Road. This supermarket has ceased operation in July 2016, with the site to be redeveloped. The Kilburn Post Office is located across the road from the old Coles (Bi-Lo).

==Transport==
Blair Athol is serviced by the G10 [[bus|bus line]] between Adelaide and [[Grand Junction Road]], travelling via Prospect Road and also bus routes 221, 222, 224, 225, 226, 228 and 229, which travel between Adelaide and various destinations in the Northern suburbs via Main North Road.   Residents of Blair Athol can also catch the [[train|train line]] to and from the [[city]]. The station is located in Kilburn, several hundred metres from Prospect Road.

==See also==
* [[List of Adelaide suburbs]]

== References ==
{{reflist}}

==External links==
*[http://www.portenf.sa.gov.au/ City of Port Adelaide Enfield]

{{Coord|-34.861|138.597|format=dms|type:city_region:AU-SA|display=title}}
{{City of Port Adelaide Enfield suburbs}}

[[Category:Suburbs of Adelaide]]