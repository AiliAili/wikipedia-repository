{|{{Infobox Aircraft Begin
 |name = EMB 110 Bandeirante <!--please avoid stating manufacturer in this field; it's stated two lines below -->
 |logo =
 |image =SKL in flight cropped (105091729).jpg
 |caption =
}}{{Infobox Aircraft Type
 |type = [[Regional airliner]]
 |national origin= [[Brazil]]
 |manufacturer = [[Embraer]]
  |designer = [[Max Holste]]
 |first flight =26 October 1968
 |introduction = 16 April 1973
 |retired = 
 |status = Active
 |primary user = [[Brazilian Air Force]]<!--please list only one, and don't use those tiny flags as they limit horizontal space-->
 |more users = [[AirNow]]<!--limited to three "more users" total. please separate with <br />-->
 |produced = 1968–1990
 |number built = 501
 |unit cost =
 |developed from =
 |variants with their own articles = 
}}
|}

The '''Embraer EMB 110 Bandeirante''' is a general purpose 15–21 passenger twin-[[turboprop]] light transport [[aircraft]] suitable for [[military]] and [[Civil aviation|civil]] duties.  It was manufactured by the [[Brazil]]ian corporation [[Embraer]].

[[Bandeirantes|Bandeirante]] ({{lang-en|pioneer}}) was the name given to the Portuguese settlers and pioneers who expanded the limits of the [[Portuguese Empire]], language and culture in [[Brazil]] by progressively moving in and then settling from the early coastal settlements towards the inner, then unknown and uncharted zones of the vast continent.<ref>[http://en.bab.la/dictionary/portuguese-english/bandeirante "pioneer"]</ref>

==Design and development==
[[File:Embraer EMB-100 Bandeirante - first prototype (FAB YC-95).jpg|thumb|YC-95 first prototype (EMB-100) in [[Museu Aeroespacial|Aerospace Museum]], [[Rio de Janeiro]]]]

The EMB 110 was designed by the [[France|French]] engineer [[Max Holste]] following the specifications of the [[Embraer|IPD-6504 program]] set by the [[Brazilian General Command for Aerospace Technology (CTA)|Brazilian Ministry of Aeronautics]] in 1965.<ref name="Pioneer p163-4">''Air International'' April 1978, pp. 163–164.</ref>
The goal was to create a general purpose aircraft, suitable for both civilian and military roles with a low operational cost and high reliability. On this measure, the EMB 110 has succeeded.

The first prototype, with the military designation '''YC-95''', was flown on 26 October 1968.<ref name="Pioneer p164">''Air International'' April 1978, p.164.</ref> and two other prototypes were built, known as EMB 100.<ref>{{cite web |url=http://www.centrohistoricoembraer.com.br/en-us/historiaaeronaves/pages/emb-100-bandeirante.aspx |title=EMB 100 Bandeirante |publisher=Embraer Historical Center |accessdate=4 January 2015}}</ref> By 1969 an order was placed for 80 production aircraft, by now known as '''EMB 110 Bandeirante''', for the Brazilian Air Force (FAB) with the newly formed aircraft company Embraer. The Bandeirante received its Brazilian [[Type certificate|airworthiness certificate]] at the end of 1972.<ref name="Pioneer p165"/> and on 9 February 1973 was made the first delivery to FAB.<ref name="Pioneer p165">''Air International'' April 1978, p.165.</ref><ref>{{cite web |url=http://www.centrohistoricoembraer.com.br/en-us/historiaaeronaves/pages/emb-110-bandeirante.aspx |title=EMB 110 Bandeirante |publisher=Embraer Historical Center |accessdate=4 January 2015}}</ref>

[[File:Embraer C-95 Bandeirante (EMB-110A), Brazil - Air Force AN0965736.jpg|thumb|<center>EMB 110A cockpit]]
[[File:Cook Islands IMG 4970 (8451963059).jpg|thumb|EMB 110 cabin, operated by [[Air Rarotonga]]]]

Further development of the EMB 110 was halted by the manufacturer in order to shift focus to the larger, faster, and pressurized 30-seat [[Embraer EMB 120 Brasilia|EMB 120 Brasilia]].

On Dec 15, 2010, the Brazilian Air Force first flew an upgraded EMB 110 equipped with modern avionics equipment. Designated as the C/P-95, the aircraft has had several new systems installed by Israeli firm Elbit Systems' Brazilian subsidiary, Aeroeletronica. The Brazilian Air Force has an active fleet of 96 EMB-110s.<ref name="Flightglobal">Hoyle, Craig. [http://www.flightglobal.com/articles/2010/12/15/350959/pictures-brazil-flies-first-upgraded-emb-110-bandeirante.html "PICTURES: Brazil flies first upgraded EMB-110 Bandeirante"]. ''Flightglobal'', 15 December 2010.</ref>

==Operational history==
[[File:G-TABS2.JPG|thumb|EMB 110 registration G-TABS, operated by Skydrift, loading through the large cargo door.<ref>{{cite web|url= https://www.planespotters.net/search.php?q=G-TABS|title= G-TABS|publisher= Planespotters|accessdate= 31 January 2017}}</ref>
]]
Deliveries started to the Brazilian Air Force in February 1973.<ref name="Pioneer p165"/>  The [[Airliner|passenger model]] first flew on 9 August 1972 and entered commercial service on 16 April 1973 with the now defunct Brazilian airline company [[Transbrasil]].

Over the next 21 years Embraer built 494 aircraft in numerous configurations for a variety of roles. Production was halted in 1990, as the EMB 110 had been superseded by the [[Embraer EMB 120 Brasilia]].

==Variants==

* '''YC-95''' or '''EMB 100''' - [[Prototype]], powered by two 550&nbsp;shp (410&nbsp;kW) [[Pratt & Whitney Canada PT6]]A-20 [[turboprop]] engines. Three built.<ref name="Pioneer p170">''Air International'' April 1978, p.170.</ref>
* '''EMB 110''' Initial production version, powered by 680&nbsp;shp (507&nbsp;kW) PT6A-27 engines - Twelve seat military transport for the [[Brazilian Air Force]], who designate it the '''C-95'''.  60 built.<ref name="Pioneer p170"/>
* '''EMB 110A''' - Radio calibration version for the Brazilian Air Force ('''EC-95'''). Three built.<ref name="Pioneer p170"/>
* '''EC-95B''' - Calibration version for the Brazilian Air Force.
* '''EMB 110B''' - [[Aerial survey]], aerial photography version. Seven built, six as '''R-95''' for the Brazilian Air Force.<ref name="Pioneer p170"/>
* '''EMB 110C''' - The first commercial model, similar to C-95, a 15-seat passenger version.<ref name="Pioneer p170"/>
* '''EMB 110C(N)''' - Three navalised EMB 110Cs sold to the [[Chilean Navy]].<ref name="Pioneer p170"/>
* '''EMB 110E''' Executive version of EMB 110C. Six to eight seats.<ref name="Pioneer p170"/>
** '''EMB 110E(J)''' Modified version of EMB 110E.<ref name="Pioneer p170"/>
* '''EMB 110K''' Stretched version with 0.85&nbsp;m (2&nbsp;ft 9½&nbsp;in) fuselage plug and 750&nbsp;shp (560&nbsp;kW) PT6A-34 engines and fitted with ventral fin.<ref name="Pioneer p170"/>
** '''EMB 110K1''' - Cargo transport version for the Brazilian Air Force, with cargo door in rear fuselage. 20 built, designated '''C-95A'''.<ref name="Pioneer p170"/>
* '''EMB 110P''' Dedicated commuter version of EMB 110C for Brazilian airlines, powered by PT6A-27 or -34 engines.<ref name="Pioneer p170"/>
* '''EMB 110P1''' - Quick change civil cargo/passenger transport version based on EMB 110K1, with same rear cargo door.<ref name="Pioneer p170"/>
* '''EMB 110P2''' - Dedicated civil passenger version of EMB 110P1, without cargo door.<ref name="Pioneer p170"/>
* '''EMB 111A''' - Maritime patrol version for the [[Brazilian Air Force]].  The aircraft also has the Brazilian Air Force designation '''P-95 ''Bandeirulha'''''.<ref>"''Bandeirulha''" is a nickname - junction of the names "Bandeirante" and "Patrulha" (Patrol).</ref> Two were leased to the [[Argentine Navy]] during the [[Falklands War]] due to the retirement of their last [[P-2 Neptune|SP-2H Neptune]] and until the introduction of modified [[L-188 Electra]]s.<ref name="trackerenmalvinas.com.ar">{{cite web|url=http://www.trackerenmalvinas.com.ar/html/download.html |title=PDF book: Historia de la Aviación Naval Argentina |publisher=trackerenmalvinas.com.ar |language=es |accessdate=2009-04-07 |deadurl=yes |archiveurl=https://web.archive.org/web/20070517220622/http://www.trackerenmalvinas.com.ar:80/html/download.html |archivedate=2007-05-17 |df= }}</ref>
* '''P-95B''' -
* '''EMB 111AN''' - Six maritime patrol aircraft sold to the Chilean Navy.
* '''C-95B''' - Quick change cargo/passenger version for the Brazilian Air Force.
* '''EMB 110P1 SAR''' - Search and rescue version.
* EMB 110P/A - 18 seat passenger version, intended for export.
* EMB 110P1/A - Mixed passenger/freight version with enlarged cargo door.
* '''EMB 110P1/41''' - Cargo/passenger transport aircraft.
* '''EMB 110P1K/110K''' - Military version.
* '''C-95C''' - The Brazilian Air Force version of the EMB 110P2.
* '''EMB 110P2'''
* '''EMB 110P2/A''' - Modifications for airline commuter role, seating up to 21 passengers.
* '''EMB 110P2/41''' - 21-seat pressurised commuter airliner.
* '''EMB 110S1''' - [[Geophysical survey]] version.
* '''SC-95''' - [[Search and rescue]] version for the Brazilian Air Force.
* '''XC-95''' - Rain research version for the Brazilian Air Force.
* '''C/P-95''' - Updated version with modernised avionics.<ref name="Flightglobal"/>

==Operators==

===Civil operators===
In August 2008 a total of 122 EMB 110 aircraft (all variants) remained in airline service worldwide with some 45 airlines.<ref>[http://www.flightglobal.com/assets/getasset.aspx?ItemID=24736 ''Flight International'' 2008 World Airliner Census (online version).] Retrieved: 10 December 2008</ref>  Major operators include:
; {{AUS}}
* [[Aeropelican]] (1)
* [[King Island Airlines]] (1)
; {{BAH}}
* [[Pineapple Air]] (1)
; {{BRA}}
* [[Abaeté Linhas Aéreas]] (6)
* [[Manaus Aerotáxi]]
* [[Táxi Aéreo Weiss]]
; {{CAN}}
* [[Aeropro]] (2)
* [[Kenn Borek Air]] (1)
; {{COK}}
* [[Air Rarotonga]] (3)
; {{CUB}}
* [[Aerocaribbean]] (4)
; {{CUR}}
* [[Insel Air]] (3) (Retired)
; {{FIJ}}
*  [[Northern Air (Fiji)]]
; {{GUA}}
*  [[Transportes Aéreos Guatemaltecos]] (6)
; {{GHA}}
* [[Aberdair Aviation]] (1)
; {{GBR}}
;* JEA
* [[SkyDrift Air Charter]] (1)
* [[Air UK]]
* [[Genair]]
; {{HON}}
* [[CM Airlines]]
; {{IRL}}
* [[Ryanair]] (1)
; {{KEN}}
* [[Aberdair Aviation]] (3)
; {{NOR}}
* [[Teddy Air]] (1)
; {{USA}}
* [[Agape Flights]] (1)
* [[AirNow]] (9)
* [[Air Sunshine]] (2)
* Arctic Circle Air (3)
* [[Royal Air Freight]] (5)
* [[Special Aviation Systems]] (4)
* [[Tropical Air Transport]] (1)
* [[Wiggins Airways]] (7)
; {{VEN}}
* [[Rutaca]] (5)
; {{IRN}}
* [[Payam Air]] (5) (4 stored)

Historically, a number of commuter airlines in the U.S. and elsewhere operated the EMB 110 in scheduled passenger airline operations.

===Military operators===
[[File:Brazilian Air Force Embraer P-95B Bandeirulha (EMB-111A).jpg|thumb|Brazilian Air Force Embraer P-95B ''Bandeirulha'' (EMB-111A)]]

* '''EMB 100'''
; {{BRA}}: [[Brazilian Air Force]] - former operator.
* '''EMB 110'''
; {{ANG}}: [[National Air Force of Angola]]
; {{BRA}}: [[Brazilian Air Force]] Operates 104 aircraft.<ref name=Census2011>[http://www.emagazine.flightinternational.com/1B4ee1d2e7f2aef012.cde/page/35 "World Airliner Census 2011".] {{webarchive |url=https://web.archive.org/web/20120825034701/http://www.emagazine.flightinternational.com/1B4ee1d2e7f2aef012.cde/page/35 |date=August 25, 2012 }} ''Flight Global," 13–19 December 2011. Retrieved: 12 January 2012</ref>
; {{CPV}}: [[Military of Cape Verde]]
; {{CHI}}: [[Chilean Navy]] Operates five aircraft.<ref name=Census2011 />
; {{COL}}: [[Colombian Air Force]] Operates two aircraft.<ref name=Census2011 />
; {{GAB}}
; {{flag|Guyana}}
* [[Guyana Defence Force]] - former operator.
; {{SEN}}
; {{URY}}: [[Uruguayan Air Force]] Operates three aircraft.<ref name=Census2011 />
* '''EMB 111'''
; {{ANG}}: [[National Air Force of Angola]]
; {{ARG}}: [[Argentine Navy]] - leased by [[Argentine Naval Aviation|naval aviation]] during the [[Falklands War]]<ref name="trackerenmalvinas.com.ar"/>
; {{BRA}}: [[Brazilian Air Force]]
; {{CHI}}: [[Chilean Navy]]

==Specifications (EMB 110P1A/41)==
[[File:TG-TAY Embraer Emb.110 TAG (7475007508).jpg|thumb|A Bandeirante with its PT6A engine uncovered]]

{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=<!-- options: plane/copter --> plane
|jet or prop?=<!-- options: jet/prop/both/neither --> prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->
|ref=Jane's All The World's Aircraft 1988–89<ref name="JAWA88 p11">Taylor 1988, p. 11.</ref>
|crew=2
|capacity=18 passengers
|length main= 15.10 m
|length alt= 49 ft 6½ in
|span main= 15.33 m
|span alt= 50 ft 3½ in
|height main= 4.92 m
|height alt= 16 ft 1¾ in
|area main= 29.10 m²
|area alt= 313.2 ft²
|airfoil=NACA 23016(mod) at root, NACA 23012 (mod) at tip
|aspect ratio=8.1:1
|empty weight main= 3,393 kg
|empty weight alt= 7,480 lb
|loaded weight main= <!--kg-->
|loaded weight alt= <!--lb-->
|useful load main= <!--kg-->
|useful load alt= <!--lb-->
|max takeoff weight main= 5,900 kg
|max takeoff weight alt= 13,010 lb
|more general=
|engine (prop)=[[Pratt & Whitney Canada PT6|Pratt & Whitney Canada PT6A-34]]
|type of prop= turboprop engines
|number of props=2
|power main= 559 kW
|power alt= 750 shp
|power original=
|max speed main= <!--km/h-->
|max speed alt= <!--knots,mph-->
|cruise speed main= 341 km/h
|cruise speed alt= 184 knots, 212 mph
|cruise speed more= - econ cruise at 3,050 m (10,000 ft)
|never exceed speed main= <!--km/h-->
|never exceed speed alt= 
|stall speed main= <!--km/h-->
|stall speed alt= <!--knots,mph-->
|range main= 1,964 km
|range alt= 1,060 nm, 1,220 mi
|ceiling main= 6,550 m
|ceiling alt= 21,500 ft
|climb rate main= 8.3 m/s
|climb rate alt= 1,640 ft/min
|loading main= <!--kg/m²-->
|loading alt= <!--lb/ft²-->
|thrust/weight=<!--a unitless ratio-->
|power/mass main= <!--W/kg-->
|power/mass alt= <!--hp/lb-->
|more performance=
|armament=
|avionics/radios=
}}

==Incidents and accidents==
*27 February 1975: a [[VASP]] Embraer EMB 110 Bandeirante registration PP-SBE operating flight 640 from [[Congonhas-São Paulo Airport|São Paulo-Congonhas]] to [[Bauru Airport|Bauru]] crashed after take-off from Congonhas. All 2 crew members and 13 passengers died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19750227-1 | title=Accident description PP-SBE | publisher=Aviation Safety Network | accessdate=20 May 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928–1996 | chapter=O primeiro Bandeirante | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=294–301 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*22 January 1976: a [[Transbrasil]] Embraer EMB 110C Bandeirante registration PT-TBD operating flight 107 from [[Chapecó Airport|Chapecó]] to [[Erechim Airport|Erechim]], crashed upon take-off from Chapecó. Seven of the nine passengers and crew on board died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19760122-1 | title=Accident description PT-TBD | publisher=Aviation Safety Network | accessdate=26 May 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Estouro de pneu na decolagem | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=302–307 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*23 April 1977: [[Brazilian Air Force]], an Embraer C-95 Bandeirante registration FAB-2169 crashed upon landing at [[Natal Air Force Base]].<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19770423-0 | title=Accident description FAB-2169 | publisher=Aviation Safety Network | accessdate=9 May 2011}}</ref>
*3 June 1977: [[Brazilian Air Force]], an Embraer C-95 Bandeirante registration FAB-2157 crashed after take-off from [[Natal Air Force Base]]. All 18 occupants died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19770603-2 | title=Accident description FAB-2157 | publisher=Aviation Safety Network | accessdate=9 May 2011}}</ref>
*20 June 1977: a [[:es:Transporte Aéreo Militar Uruguayo|Transporte Aéreo Militar Uruguayo]] Embraer EMB110C Bandeirante registration CX-BJE/T584 flying from [[Carrasco International Airport|Montevideo]] to [[Nueva Hesperides International Airport|Salto]] crashed after striking trees in an orange grove during approach to Salto. The crew of 2 and 3 of the 13 passengers died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19770620-0 | title=Accident description CX-BJE/T584 | publisher=Aviation Safety Network | accessdate=11 July 2011}}</ref>
*31 January 1978: a [[TABA – Transportes Aéreos da Bacia Amazônica]] Embraer EMB 110 Bandeirante registration PT-GKW crashed upon take-off from [[Eirunepé Airport|Eirunepé]]. The crew of 2 died but all 14 passengers survived.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19780131-0 | title=Accident description PT-GKW | publisher=Aviation Safety Network | accessdate=30 May 2011}}</ref>
*8 February 1979: a [[TAM Airlines]] Embraer EMB 110 Bandeirante registration PT-SBB operating a flight from [[Bauru Airport|Bauru]] to [[Congonhas-São Paulo Airport|São Paulo-Congonhas]], while on initial climb from Bauru, struck trees and crashed into flames. All 2 crew and 16 passengers died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19790208-0 | title=Accident description PT-SBB | publisher=Aviation Safety Network | accessdate=20 May 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Compensador automático | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=308–312 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*24 February 1981: a [[VOTEC]] Embraer EMB110P Bandeirante registration PT-GLB flying from [[Tucuruí Airport|Tucuruí]] to [[Val de Cans International Airport|Belém-Val de Cans]] collided with a ship in dry dock while approaching Belém in rain and high winds. The aircraft subsequently struck two barges and broke in two. The front part crashed onto a tug, and the tail section sank. Only 3 passengers of a total of 14 passengers and crew survived.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19810224-0 | title=Accident description PT-GLB | publisher=Aviation Safety Network | accessdate=7 June 2011}}</ref>
*7 October 1983: a [[TAM Airlines]] Embraer EMB 110C Bandeirante registration PP-SBH flying from [[Campo Grande International Airport|Campo Grande]] and [[:pt:Complexo de Urubupungá|Urubupungá]] to [[Araçatuba Airport|Araçatuba]] struck the ground just short of the runway threshold after missing the approach at Araçatuba Airport twice. Seven crew and passengers died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19831007-0 | title=Accident description PP-SBH | publisher=Aviation Safety Network | accessdate=8 May 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Três é demais | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=332–334 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*18 April 1984: two [[VOTEC]] Embraer EMB 110 Bandeirante registrations PT-GJZ and PT-GKL collided on air, while on approach to land at [[Imperatriz Airport|Imperatriz]]. PT-GJZ was flying from [[Marechal Cunha Machado International Airport|São Luís]] to Imperatriz and crashed on ground killing all its 18 passengers and crew died. PT-GKL was flying from [[Val de Cans International Airport|Belém-Val de Cans]] to Imperatriz and its pilot was able to make an emergency landing on Tocantins river. One passenger of its 17-passenger and crew died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19840418-0 | title=Accident description PT-GJZ | publisher=Aviation Safety Network | accessdate=22 July 2011}}</ref><ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19840418-1 | title=Accident description PT-GKL | publisher=Aviation Safety Network | accessdate=22 June 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Roleta russa | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=335–337 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*28 June 1984: a [[TAM Airlines]] Embraer EMB 110C Bandeirante registration PP-SBC operating a chartered flight by [[Petrobras]] from [[Rio de Janeiro–Galeão International Airport|Rio de Janeiro–Galeão]] to [[Macaé Airport|Macaé]] [[1984 Transportes Aéreos Regionais Bandeirante accident|flew into São João Hill]] while descending through rain and clouds over the Municipality of [[São Pedro da Aldeia]]. All 16 passengers and 2 crew died. The passengers were journalists of well-known Brazilian networks who were preparing a special report about the [[Campos Basin]] oil fields.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19840628-1 | title=Accident description PP-SBC | publisher=Aviation Safety Network | accessdate=4 August 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Visumento | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=338–341 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*6 December 1984: [[PBA Flight 1039]], using an Embraer EMB 110 Bandeirante (registration N96PB-) crashed shortly after taking off from [[Jacksonville International Airport]] in [[Jacksonville]], [[Florida]], United States. All 11 passengers and both pilots died.
*23 June 1985: a [[TABA – Transportes Aéreos da Bacia Amazônica]] Embraer EMB 110 Bandeirante registration PT-GJN flying from [[Juara Airport|Juara]] to [[Marechal Rondon International Airport|Cuiabá]], while on approach to land at Cuiabá, had technical problems on engine number 1. An emergency landing was attempted but the aircraft stalled and crashed 1&nbsp;km short of the runway. All 17 occupants died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19850623-1 | title=Accident description PT-GJN | publisher=Aviation Safety Network | accessdate=11 August 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Obstáculo imprevisto | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=342–344 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*9 October 1985: a [[Nordeste Linhas Aéreas Regionais|Nordeste]] Embraer EMB110C Bandeirante registration PT-GKA operating a cargo flight from [[Vitória da Conquista Airport|Vitória da Conquista]] to [[Deputado Luís Eduardo Magalhães International Airport|Salvador da Bahia]] crashed during initial climb from Vitória da Conquista after flying unusually low. The two crew members died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19851009-0 | title=Accident description PT-GKA | publisher=Aviation Safety Network | accessdate=8 June 2011}}</ref>
*1 March 1988: [[Comair (South Africa)|Comair]] [[Comair Flight 206|Flight 206]], using an Embraer 110, crashed in Johannesburg, killing all 17 occupants.<ref>"[http://www.airdisaster.com/cgi-bin/view_details.cgi?date=03011988&reg=ZS-LGP&airline=Comair+(South+Africa) Accident Synopsis » 03011988]," ''[[Airdisaster.com]]''</ref>
*14 November 1988: Oy Wasawings Ab flight to [[Seinäjoki]] crashed during landing in [[Ilmajoki]], [[Finland]]. 6 deaths, 6 injured.<ref>[http://www.onnettomuustutkinta.fi/24022.htm Onnettomuustutkintakeskus - 2/1988<!-- Bot generated title -->]</ref>
*20 September 1990: an Embraer EMB110P1 Bandeirante registration PT-FAW belonging to the Government of Pernambuco, flying from [[Fernando de Noronha Airport|Fernando de Noronha]] to [[Recife Airport|Recife]], crashed into the sea shortly after take-off. All 12 crew and passengers died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19900920-2 | title=Accident description PT-FAW | publisher=Aviation Safety Network | accessdate=20 May 2011}}</ref>
*11 November 1991: a [[Nordeste Linhas Aéreas Regionais|Nordeste]] Embraer EMB110P1 Bandeirante registration PT-SCU operating flight 115 from [[Recife Airport|Recife]] to [[Zumbi dos Palmares International Airport|Maceió]], during on initial climb had an engine failure followed by fire. The aircraft crashed on populated area. All 13 occupants of the aircraft and 2 persons on the ground died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19911111-0 | title=Accident description PT-SCU | publisher=Aviation Safety Network | accessdate=20 May 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Fogo na decolagem | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=364–369 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*3 February 1992: a [[Nordeste Linhas Aéreas Regionais|Nordeste]] Embraer EMB 110 Bandeirante registration PT-TBB en route from [[Deputado Luís Eduardo Magalhães International Airport|Salvador da Bahia]] to [[Guanambi Airport|Guanambi]] descended below minimum levels in bad weather and crashed on a hill hidden by clouds near [[Caetité]]. All 12 passengers and crew aboard died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19920203-1 | title=Accident description PT-TBB | publisher=Aviation Safety Network | accessdate=1 May 2011}}</ref><ref>{{cite book | first=Carlos Ari César | last=Germano da Silva | title=O rastro da bruxa: história da aviação comercial brasileira no século XX através dos seus acidentes 1928-1996 | chapter=Nordeste 092 | publisher=EDIPUCRS | edition=2 | place=Porto Alegre | year=2008 | pages=371–375 | isbn=978-85-7430-760-2 | language=Portuguese}}</ref>
*13 January 1993: A [[Titan Airways]] cargo flight crashed into a hill near [[Sellafield]], en route from [[London Southend Airport]] to [[Glasgow International Airport]]. The flight used ''G-ZAPE'', a 110P, and both pilots were killed in the crash.<ref>Air Safety Network, [http://aviation-safety.net/database/record.php?id=19930113-0 accident description]</ref>
*26 October 1993:  A [[Brazilian Air Force]] patrol P-95 (Embraer EMB 111 Bandeirante Patrulha) registration FAB-2290 that departed from [[Canoas Air Force Base]] crashed into the ocean near [[Angra dos Reis]] while flying in bad weather conditions. All crew of 3 died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19931026-0 | title=Accident description FAB-2290 | publisher=Aviation Safety Network | accessdate=12 May 2011}}</ref>
*19 July 1994: [[Alas Chiricanas Flight 00901]] Panamanian domestic airline ALAS, registration HP-1202AC using an Embraer 110P1, the aircraft crashed after a bomb exploded in the cabin killing 21, twelve Jewish businessmen were among the passengers.
*24 May 1995 G-OEAA, an Embraer EMB-110-P1 operated by [[United Kingdom|UK]] domestic airline Knight Air flight between [[Leeds Bradford International Airport|Leeds]] and [[Aberdeen Airport|Aberdeen]] entered a steeply descending spiral dive, broke up in flight and crashed into farmland at Dunkeswick Moor near Leeds. All 12 occupants were killed. The probable cause of the accident was the failure of one or both [[Attitude indicator|artificial horizon]] instruments. There was no standby artificial horizon installed (as there was no airworthiness requirement for one on this aircraft) and the accident report concluded that this left the crew without a single instrument available for assured attitude reference or simple means of determining which flight instruments had failed. The aircraft entered a spiral dive from which the pilot, who was likely to have become spatially disoriented, was unable to recover.<ref>{{cite web| url=http://www.aaib.gov.uk/publications/formal_reports/2_1996__g_oeaa.cfm | title= AAIB Report No: 2/1996 | work= UK AAIB  |accessdate= 2008-03-05 }}</ref><ref>{{cite web |url=http://aviation-safety.net/database/record.php?id=19950524-0 | title=EMB-110, G-OEAA | work=Aviation Safety Network | accessdate= 2007-12-13 }}</ref><ref>{{cite web|url=http://www.pmpsimple.com/ |title=PMP Simple EMB-110 |work=Aviation Safety Network |accessdate=2010-02-13 |deadurl=yes |archiveurl=https://web.archive.org/web/20100301030903/http://www.pmpsimple.com:80/ |archivedate=2010-03-01 |df= }}</ref>
*13 September 1996: a [[Helisul Linhas Aéreas|Helisul]] Embraer EMB 110 Bandeirante registration PT-WAV operating a cargo flight from [[Salgado Filho International Airport|Porto Alegre]] to [[Joinville-Lauro Carneiro de Loyola Airport|Joinville]] collided with a hill and crashed during final approach to land at Joinville. The crew of two died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19960913-1 | title=Accident description PT-WAV | publisher=Aviation Safety Network | accessdate=October 3, 2012}}</ref>
*17 November 1996: [[Brazilian Air Force]], an Embraer P-95 Bandeirante registration FAB-7102 flying from [[Deputado Luís Eduardo Magalhães International Airport|Salvador da Bahia Air Force Base]] to [[Natal Air Force Base]] had an accident in the vicinity of [[Caruaru]]. Four Brazilian Air Force Bandeirantes were flying on formation from Salvador to Natal when the tail of FAB-7102 was struck by the propeller of another aircraft. Control of the aircraft was lost and it crashed. All 9 occupants died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=19961117-0 | title=Accident description FAB-7102 | publisher=Aviation Safety Network | accessdate=9 May 2011}}</ref>
*24 July 1999: an [[Air Fiji]] Embraer EMB 110 Bandeirante registration DQ-AFN on a domestic flight from Nausori to Nadi in the Fiji Islands crashed on a slope of a ridge. The aircraft had apparently descended below the 5400 feet safety altitude until the right wing struck a tree on a ridgeline at 1300 feet altitude. The Bandeirante then broke up and impacted the slope of a ridge 1,3&nbsp;km further on. The tail section and right wing were found 150m from the main wreckage. Weather at 05:00 was good: nil wind, 40&nbsp;km visibility, scattered clouds at 2200 feet and an insignificant small shower band. Investigation revealed a.o. that the captain had insufficient rest prior to the flight and that he had consumed an above-therapeutic level of antihistamine prior to the flight, which would have degraded his ability to safely pilot the aircraft. Also Air Fiji’s published standard operating procedures were inadequate for the Bandeirante aircraft.
*26 December 2002: [[Brazilian Air Force]], an Embraer EMB 110 Bandeirante registration FAB-2292 en route from [[Campo de Marte Airport|São Paulo-Campo de Marte]] to [[Florianópolis Air Force Base]], crashed while trying to carry out an emergency landing at [[Afonso Pena International Airport|Curitiba-Afonso Pena]]. Reportedly, both engines had quit. The airplane had taken off with insufficient fuel on board to complete the flight to Florianópolis. Three passengers and crew of the 16 aboard died.<ref>{{Cite web | url=http://aviation-safety.net/database/record.php?id=20021226-0 | title=Accident description FAB-2292 | publisher=Aviation Safety Network | accessdate=6 May 2011}}</ref>
*7 February 2009 An Embraer 110, operated by [[Manaus Aerotáxi]], registration PT-SEA, flying a domestic route in Brazil from Coari to Manaus (Amazonas) struggled in bad weather conditions and [[2009 Amazonian airplane crash|crashed]] 80&nbsp;km from Manaus killing 24 passengers. 4 survivors were reported.<ref>{{cite web|url=http://www.estadao.com.br/noticias/cidades,embraer-vai-ajudar-nas-investigacoes-sobre-acidente-no-am,320705,0.htm |title=Embraer vai ajudar nas investigações sobre acidente no AM |work=Estado de S. Paulo |accessdate=2009-02-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20090212123445/http://estadao.com.br:80/noticias/cidades,embraer-vai-ajudar-nas-investigacoes-sobre-acidente-no-am,320705,0.htm |archivedate=2009-02-12 |df= }}</ref><ref>{{cite web|url=http://ultimahora.publico.clix.pt/noticia.aspx?id=1364424&idCanal=11 |title=Queda de avião no Brasil faz 24 mortos |work=Publico.pt |accessdate=2009-02-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20090213235309/http://ultimahora.publico.clix.pt:80/noticia.aspx?id=1364424&idCanal=11 |archivedate=2009-02-13 |df= }}</ref>
*3 July 2013 An Embraer 110, operated by Batair Cargo, registration ZS-NVB, en route from [[Lanseria Airport]] in [[Johannesburg]] for Lubumbashi in the [[Democratic Republic of the Congo]], crashed while attempting to land in Francistown, Botswana. The pilots  had planned to land and refuel but thick mist on the ground caused them to miss the landing strip on their first pass. They called in to the control tower to notify that they would make a second pass because they could see the landing strip, but never did. The wreckage was found two hours later about 10&nbsp;km from the airport. The plane crashed with no survivors.<ref>{{cite web |url=http://www.news24.com/SouthAfrica/News/SA-pilots-die-as-Batman-plane-crashes-20130703 | title=
SA pilots die as 'Batman' plane crashes | accessdate= 2013-07-03 }}</ref>

==See also==
{{Portal|Aviation}}
{{Aircontent|
|related=
*[[Embraer EMB 120 Brasilia]]
*[[Embraer EMB 121 Xingu]]
|similar aircraft=
|lists=
|see also=

}}

==References==
{{commons category|Embraer EMB 110}}
;Notes
{{reflist}}
;Bibliography
* [http://www.airliners.net/info/stats.main?id=195 EMB 110 information at Airliners.net]
* Endres, Gunter and Gething, Mike. (2002). ''Aircraft Recognition Guide'', (2nd Ed.). New York: Harper Collins Publishers. ISBN 0-00-713721-4.
* [[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1976-77''. London:Jane's Yearbooks, 1976. ISBN 0-354-00538-3.
* Taylor, John W. R. ''Jane's All The World's Aircraft 1982-83''. London:Jane's Yearbooks, 1982. ISBN 0-7106-0748-2.
* Taylor, John W. R. ''Jane's All The World's Aircraft 1988-89''. Couldon, UK:Jane's Defence Data, 1988. ISBN 0-7106-0867-5.
* "The Pioneers from São Paulo". ''[[Air International]]'', April 1978, Vol. 14 No. 4. pp.&nbsp;163–170, 193–194.

{{Embraer}}

{{DEFAULTSORT:Embraer Emb 110 Bandeirante}}
[[Category:Brazilian airliners 1980–1989]]
[[Category:Embraer aircraft|EMB 110]]