{{good article}}

{{Planetbox begin
| name=55 Cancri b
}}
{{Planetbox image
| image = 55 Cancri b.png
| caption = Artist's impression of 55 Cancri b.
}}
{{Planetbox star
| star=[[55 Cancri|55 Cancri A]]
| constell=[[Cancer (constellation)|Cancer]]
| RA={{RA|08|52|35.8}}
| DEC={{DEC|+28|19|51}}
| app_mag=5.95
| dist_ly=40.3 ± 0.4
| dist_pc=12.3 ± 0.1
| class=G8V
| mass=0.95 ± 0.10
| radius=1.152 ± 0.035
| temperature=5373 ± 9.7
| metallicity=0.29
| age=7.4–8.7
}}
{{Planetbox orbit
| semimajor=0.115 ± 0.0000011<ref name=fischer>{{cite journal | author =DA Fischer | title=Five Planets Orbiting 55 Cancri |journal=Astrophysical Journal |issue=675| pages=790–801 |doi=10.1086/525512 |date=March 2008 | volume =675 | bibcode=2008ApJ...675..790F|arxiv = 0712.3917 |display-authors=etal}}</ref>
| semimajor_gm=17.2
| semimajor_mas=9.18
| periastron=0.113
| periastron_gm=16.9
| apastron=0.116
| apastron_gm=17.4
| eccentricity=0.014 ± 0.008<ref name=fischer />
| period=14.65162 ± 0.0007<ref name=fischer />
| period_year=0.04011325
| inclination = ~85<ref name=transitatmo>{{cite journal | journal=Astronomy & Astrophysics | date=October 2, 2012 | author=D. Ehrenreich | doi=10.1051/0004-6361/201219981 |title=Hint of a transiting extended atmosphere on 55 Cancri b|arxiv = 1210.0531 |bibcode = 2012A&A...547A..18E |display-authors=etal}}</ref><ref name=Dragomir>D. Dragomir, 2012-08-27, referred to in Ehrenreich</ref>
| arg_peri=131.94 ± 30<ref name=fischer />
| t_peri=2,450,002.94749 ± 1.2<ref name=fischer />
| semi-amp=71.32 ± 0.41<ref name=fischer />
}}
{{Planetbox character
| mass=0.824 ± 0.007<ref name=fischer /><ref name="transitatmo" />
| stellar_flux = 48
}}
{{Planetbox discovery
| discovery_date=April 12, 1996
| discovery_site=[[California, USA]]
| discovery_method=[[Radial velocity]]
| detection_methods=Transit
| discoverers=[[R. Paul Butler|Butler]], [[Geoffrey Marcy|Marcy]]
| discovery_status=Published
}}
{{Planetbox catalog
| names= Galileo, 55 Cancri&nbsp;Ab, Rho<sup>1</sup>&nbsp;Cancri&nbsp;b, HD&nbsp;75732&nbsp;b
}}
{{Planetbox reference
| star = 55+Cnc
| planet = b
}}
{{Planetbox end}}
'''55 Cancri b''' (abbreviated '''55 Cnc b'''), occasionally designated 55 Cancri Ab (to distinguish it from the [[star]] [[55 Cancri|55 Cancri B]]), also named '''Galileo''', is an [[exoplanet]] orbiting the [[Sun]]-like star [[55 Cancri|55 Cancri A]] every 14.65 [[day]]s. It is the second planet in order of distance from its star, and is an example of a [[hot Jupiter]], or possibly rather "warm Jupiter".<ref name=newsci>{{cite web|url=http://www.newscientist.com/article/dn22376-astrophile-first-puffy-warm-jupiter-spotted.html |title=Astrophile: First puffy, 'warm Jupiter' spotted - space - 12 October 2012 |publisher=New Scientist |accessdate=2012-11-09}}</ref>

In July 2014 the [[International Astronomical Union]] launched a process for giving proper names to certain exoplanets and their host stars.<ref>[http://www.iau.org/news/pressreleases/detail/iau1404/ NameExoWorlds: An IAU Worldwide Contest to Name Exoplanets and their Host Stars]. IAU.org. 9 July 2014</ref> The process involved public nomination and voting for the new names.<ref>[http://nameexoworlds.iau.org/process NameExoWorlds The Process]</ref> In December 2015, the IAU announced the winning name was Galileo for this planet.<ref>[http://www.iau.org/news/pressreleases/detail/iau1514/ Final Results of NameExoWorlds Public Vote Released], International Astronomical Union, 15 December 2015.</ref> The winning name was submitted by the Royal Netherlands Association for Meteorology and Astronomy of the [[Netherlands]]. It honors early-17th century astronomer and physicist [[Galileo Galilei]].<ref>[http://nameexoworlds.iau.org/names NameExoWorlds The Approved Names]</ref>

==Discovery==
[[File:55 Cnc b rv.pdf|thumb|left|275px|The radial velocity trend of 55 Cancri caused by the presence of 55 Cancri b.]]

55 Cancri b was discovered in 1996 by [[Geoffrey Marcy]] and [[R. Paul Butler]]. It  was the fourth known extrasolar planet, excluding [[pulsar planet]]s. Like the majority of known extrasolar planets, it was discovered by detecting variations in its star's [[radial velocity]] caused by the planet's [[gravity]]. By making sensitive measurements of the [[Doppler shift]] of the [[spectrum]] of 55 Cancri A, a 15-day periodicity was detected. The planet was announced in 1996, together with the planet of [[Tau Boötis]] and the innermost planet of [[Upsilon Andromedae]].<ref>{{cite journal | author=Butler | title=Three New 51 Pegasi-Type Planets | journal=The [[Astrophysical Journal]] | volume=474 | issue=2 | date=1997 | pages=L115–L118 | doi=10.1086/310444 | last2=Marcy | first2=Geoffrey W. | last3=Williams | first3=Eric | last4=Hauser | first4=Heather | last5=Shirts | first5=Phil | bibcode=1997ApJ...474L.115B|display-authors=etal}}</ref>

Even when this inner planet, with a [[mass]] at least 78% times that of [[Jupiter]] was accounted for, the star still showed a drift in its radial velocity. This eventually led to the discovery of the outer planet [[55 Cancri d]] in 2002.

==Orbit and mass==
55 Cancri b is in a short-period orbit, though not so extreme as that of the previously detected hot Jupiter [[51 Pegasi b]]. The orbital period indicates that the planet is located close to a 1:3 [[mean motion]] [[orbital resonance|resonance]] with [[55 Cancri c]], however investigations of the planetary parameters in a Newtonian simulation indicate that while the orbital periods are close to this ratio, the planets are not actually in the resonance.<ref name=fischer />

In 2012, b's upper atmosphere was observed transiting the star; so its inclination is about 85 degrees, coplanar with [[55 Cancri e]]. This helped to constrain the mass of the planet but the inclination was too low to constrain its radius.<ref name="transitatmo" />

The mass is about .85 that of Jupiter.<ref name="transitatmo" />

==Characteristics==
55 Cancri b is a [[gas giant]] with no [[solid]] surface. The atmospheric transit has demonstrated hydrogen in the upper atmosphere.<ref name=transitatmo />

That transit is so tangential, that properties such as its [[radius]], density, and [[temperature]] are unknown. Assuming a composition similar to that of Jupiter and that its environment is close to [[chemical equilibrium]], 55 Cancri b's upper [[atmosphere]] is predicted to be [[cloud]]less with a [[spectrum]] dominated by [[alkali metal]] [[absorption (optics)|absorption]].<ref>{{cite journal|bibcode=2003ApJ...588.1121S|author=Sudarsky, D.|title=Theoretical Spectra and Atmospheres of Extrasolar Giant Planets|journal=[[The Astrophysical Journal]]|volume=588|issue=2|pages=1121–1148|date=2003|doi=10.1086/374331|arxiv = astro-ph/0210216 |display-authors=etal}}</ref>

The atmosphere's transit indicates that it is slowly evaporating under the sun's heat. The evaporation is slower than that for previously studied (hotter) hot Jupiters.<ref name=newsci/>

The planet is unlikely to have large [[natural satellite|moons]], since [[tide|tidal forces]] would either eject them from orbit or destroy them on short timescales relative to the age of the system.<ref>{{cite journal|bibcode=2002ApJ...575.1087B|author=Barnes, J.|author2=O'Brien, D.|title=Stability of Satellites around Close-in Extrasolar Giant Planets|journal=[[The Astrophysical Journal]]|volume=575|issue=2|pages=1087–1093|date=2002|doi=10.1086/341477|arxiv = astro-ph/0205035 }}</ref>

==See also==
{{Wikipedia books|55 Cancri}}
*[[Appearance of extrasolar planets]]
{{Clear}}

==References==
{{reflist}}

==External links==
* {{cite web |url=http://exoplanet.eu/planet.php?p1=55+Cnc&p2=b |title=Notes for Planet 55 Cnc b |author=Jean Schneider |date=2011 |publisher=[[Extrasolar Planets Encyclopaedia]] |accessdate=8 October 2011}}

{{Sky|08|52|35.8|+|28|19|51|40900}}

{{55 Cancri}}

[[Category:55 Cancri]]
[[Category:Cancer (constellation)]]
[[Category:Exoplanets discovered in 1996]]
[[Category:Exoplanets]]
[[Category:Giant planets]]
[[Category:Exoplanets detected by radial velocity]]
[[Category:Transiting exoplanets]]
[[Category:Exoplanets with proper names]]