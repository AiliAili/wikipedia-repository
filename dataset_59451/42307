<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=BD-4
 |image=Raffensparger BD-4 (N643R).jpg
 |caption=BD-4
}}{{Infobox Aircraft Type
 |type=Recreational/Utility aircraft
 |manufacturer=[[Bedecorp]] for [[homebuilt aircraft|homebuilding]]
 |designer=[[Jim Bede]]
 |first flight=1968
 |introduced=1968
 |number built=
 |unit cost= [[US$]]295 (plans, 2015)
 |developed from=[[BD-1]]
 |variants with their own articles=
}}
|}

[[File:BedeBD-4C-FORD.JPG|thumb|right|Front view of a BD-4 [[taildragger]].]]
[[File:Holger-2005-07-09-4392.jpg|thumb|Rear view of a BD-4 taildragger]]
The '''Bede BD-4''' is an [[United States|American]] [[light aircraft]], designed by [[Jim Bede]] for [[Homebuilt aircraft|homebuilding]] and available since 1968. It was the first homebuilt aircraft to be offered in kit form.<ref>[https://web.archive.org/web/20080704110556/http://www.tvap.com/bede_bd4.html The TEAM Aviation BD-4]</ref> It remains one of the world's most popular homebuilts with thousands of plans sold and hundreds of examples completed to date.

==Design and development==
[[File:Bede4CSpar.jpg|thumb|Tubular spar of a BD-4C]]
Based on previous work with innovative light aircraft, the [[BD-1]] (eventually developed into the [[Grumman American AA-1|American Aviation AA-1 Yankee]]) and BD-2, Jim Bede designed the BD-4, the first real "kitplane" in the world. The design was based on a high-wing cantilever monoplane of conventional design, able to be fitted either with tailwheel or tricycle undercarriage, as the builder chooses. The builder was also able to choose between building a two-seat or four-seat version. Bede wrote a 165-page BD-4 builder's book, "Build Your Own Airplane", that is still available in 2012, that gives the amateur builder a good perspective on construction techniques.<ref>{{cite web|url=http://www.bedecorp.com/store/books |title=Books &#124; Bedecorp, LLC |publisher=Bedecorp.com |date= |accessdate=2012-10-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20130917072908/http://www.bedecorp.com/store/books |archivedate=2013-09-17 |df= }}</ref>

The intention was to have people with little or no fabrication experience start with a set of comprehensive plans and work up to a bolt-together operation, with complex components provided from the factory.  In order to simplify construction, there were few curved surfaces and most of the fuselage was made up of flat aluminum sheeting. The only major components with compound curves were the engine cowling and landing gear spats which were made of fiberglass. The fuselage is constructed of aluminum angle braces bolted together to form a truss frame.<ref>{{cite journal|magazine=Air Progress|date=Winter 1971|title=What kind of airplane should you build?|page=41}}</ref>

An innovative feature was the wing structure, which employed a "panel-rib" constructed in sections consisting of a rib whose upper edge was extended horizontally to become one section of the wing surface. The wing was progressively built up by sliding these sections together over the tubular spar and fastening them together where they met. Although the original wing design was easy to build, the current BD-4B features a redesigned, more conventional, metal wing with a tubular [[Spar (aviation)|spar]] bonded to honeycomb [[Rib (aircraft)|ribs]].<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 97. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

One downside to the panel-rib construction was not noticed until the aircraft had been in service for some time. Because the panels were glued together, they formed a liquid-tight bond, unlike conventional systems using rivets. Instead of using a separate tank to hold fuel, builders simply drilled holes in the ribs to interconnect the sections to form a tank. In service, it was found that leaks inevitably developed due to problems like improper seals and natural flexing of the wing.<ref>{{cite newsletter |first=Roger |last=Mellema |title=Fuel Tanks |publisher=BD-4 Builders/Owners Newsletter |date= March 1986 |url=http://old.bd-4.org/newsletter13.html}}</ref> BedeCorp later redesigned the wing to use a more conventional system with separate fuel tanks in the BD-4C.<ref>{{cite web |title=BD-4C |website=BedeCorp |url=http://www.bedecorp.com/bd-4c.html}}</ref>

The aircraft remained available as plans for amateur construction in 2017, from [[Bedecorp]] of [[Medina, Ohio]], United States.<ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 102. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>
<!-- ==Operational history== -->

==Variants==
;BD-4B
:Two-seat or four-seat model with {{convert|1250|lb|kg|0|abbr=on}} empty weight and {{convert|2400|lb|kg|0|abbr=on}} gross weight. Estimated construction time is 900 hours. 700 completed and flown by 2011.<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 45. Belvoir Publications. ISSN 0891-1851</ref>
;BD-4C
:Improved four-seat model with {{convert|1200|lb|kg|0|abbr=on}} empty weight and {{convert|2400|lb|kg|0|abbr=on}} gross weight. Estimated construction time is 700 hours. Two completed and flown by 2011.<ref name="KitplanesDec2011" />

==Specifications (BD-4B)==
{{Aircraft specs
|ref=Kitplanes and The Incomplete Guide to Airfoil Usage<ref name="KitplanesDec2011" /><ref name="Incomplete">{{cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 15 October 2014|last = Lednicer |first = David |year = 2010}}</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=three passengers
|length m=
|length ft=21.4
|length in=
|length note=
|span m=
|span ft=25.6
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=110
|wing area note=
|aspect ratio=
|airfoil=[[NACA airfoil|NACA 64-415]]
|empty weight kg=
|empty weight lb=1250
|empty weight note=
|gross weight kg=
|gross weight lb=2400
|gross weight note=
|fuel capacity={{convert|52|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming IO-360]]
|eng1 type=four cylinder [[horizontally opposed]] piston [[aircraft engine]]
|eng1 kw=
|eng1 hp=200

|prop blade number=2
|prop name=[[constant speed propeller]]
|prop dia m=
|prop dia ft=
|prop dia in=
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=198
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=61
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=900
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=1700
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}
* Taylor, Michael J.H. ''Jane's Encyclopedia of Aviation''. London: Studio Editions, 1989, p.&nbsp;123. ISBN 0-517-10316-8.
* Winchester, Jim. "Bede BD-10." ''Concept Aircraft: Prototypes, X-Planes and Experimental Aircraft''. Kent, UK: Grange Books plc., 2005. ISBN 978-1-84013-809-2.
* ''World Aircraft Information Files'' (File 890 Sheet 05). London: Bright Star Publishing.

==External links==
{{commons category|Bede BD-4}}
* {{Official website|http://www.jimbede.com}}

{{Bede aircraft}}

[[Category:Bede aircraft|BD-004]]
[[Category:United States civil utility aircraft 1960–1969]]
[[Category:Single-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Homebuilt aircraft]]