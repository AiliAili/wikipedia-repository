{{refimprove|date=September 2012}}
{{Infobox journal
| cover         = 
| editor        = Warren Bromley-Vogel
| discipline    = [[Literary journal]]
| peer-reviewed = 
| language      = 
| former_names  = 
| abbreviation  = 
| publisher     = [[University of Idaho]]
| country       = [[United States]]
| frequency     = Biannually
| history       = 1990-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       =http://www.fuguejournal.com/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 1054-6014
| eISSN         = 
| boxwidth      = 
}}
'''Fugue''' is an American [[literary magazine]] based out of the [[University of Idaho]] in [[Moscow, Idaho]].<ref name=rlb/> The journal was founded in 1990<ref>{{cite web|title=Fugue|url=http://www.pw.org/content/fugue_39|work=Poets and Writers|accessdate=7 July 2016}}</ref> under the editorship of J. C. Hendee.  It publishes fiction, essays and poetry twice each year.<ref name=rlb>{{cite book|author=Robert Lee Brewer|title=2013 Poet's Market|url=https://books.google.com/books?id=Wm8AcVWq3EYC&pg=PA246|accessdate=17 February 2017|date=20 August 2012|publisher=Writer's Digest Books|isbn=1-59963-637-9|page=246}}</ref>

==Notable contributors==
*[[Kathy Acker]]
*[[Jacob M. Appel]]
*[[Samuel R. Delany]]
*[[Stephen Dobyns]]
*[[Stephen Dunn]]
*[[Raymond Federman]]
*[[Brenda Hillman]]
*[[W.S. Merwin]]
*[[Sharon Olds]]
*[[James Reiss]]
*[[Pattiann Rogers]]
*[[Virgil Suarez]]
*[[Robert Wrigley]]
*[[Charles Baxter (author)|Charles Baxter]]

==Honors and awards==
* [[Fred Bahnson]]'s essay "Climbing the Sphinx" (issue #30) was reprinted in [[The Best American Series|Best American Spiritual Writing]] 2007.
* [[Cary Holladay]]'s story "The Burning" (issue #28)  was reprinted in [[New Stories from the South]].
* [[Floyd Skloot]]'s essay "A Stable State" (issue #29) was selected as a "Notable Essay" in [[Best American Essays]] 2005.
* [[Becky Hagenston]]'s story "Vines" (issue #26) received Special Mention in [[Pushcart Prize]]: Best of the Small Presses XXIX.

==See also==
*[[List of literary magazines]]

==References==
{{Reflist}}

==External links==
* [http://www.fuguejournal.com/ ''Fugue'' Homepage]
* [http://digital.lib.uidaho.edu/cdm/search/collection/fugue?_ga=1.13932023.1910747201.1426525482 ''Fugue Literary Journal Digital Collection''] -- part of the University of Idaho Library Digital Initiatives

{{University of Idaho}}

{{DEFAULTSORT:Fugue (Magazine)}}
[[Category:1990 establishments in Idaho]]
[[Category:American literary magazines]]
[[Category:Biannual magazines]]
[[Category:Magazines established in 1990]]
[[Category:Magazines published in Idaho]]
[[Category:University of Idaho]]


{{US-lit-mag-stub}}