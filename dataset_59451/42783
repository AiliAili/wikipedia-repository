{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- {{Refimprove|date={{subst:CURRENTMONTHNAME}} {{subst:CURRENTYEAR}}}}, no longer correct...  wrong Dec 2009
--->
{|{{Infobox Aircraft Begin
  |name = Felixstowe F.2
  |image =Felixstowe F2.jpg
  |caption = Felixstowe F.2A in flight.
}}{{Infobox Aircraft Type
  |type = Military flying boat
  |manufacturer = [[S.E.Saunders|S.E.Saunders Ltd]]<br>[[Airco|Aircraft Manufacturing Co Ltd]]<br>[[May, Harden and May|May, Harden & May]]
  |designer = [[John Cyril Porte]]
  |first flight = July 1916
  |introduced = 1917
  |retired = <!--date the aircraft left military or revenue service. if vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Naval Air Service]]
  |more users = [[Royal Air Force]]<br />[[United States Navy]]
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 175
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->[[Felixstowe F.1]]<br>[[Curtiss H-12]]
  |variants with their own articles = [[Felixstowe F.3]]<br> [[Felixstowe F.5]]<br>[[Felixstowe F5L]]<!-- variants OF the topic type -->
}}
|}
The '''Felixstowe F.2''' was a 1917 British [[flying boat]] class designed and developed by [[Lieutenant Commander]] [[John Cyril Porte]] [[Royal Navy|RN]] at the [[RNAS Felixstowe|naval air station]], [[Felixstowe]] during the [[First World War]] adapting a larger  version of his superior [[Felixstowe F.1]] hull design married with the larger [[Curtiss H-12]] flying boat. The Felixstowe hull had superior water contacting attributes and became a key base technology in most seaplane designs thereafter.

==Design and development==
Before the war Porte had worked with American aircraft designer [[Glenn Curtiss]] on a flying boat, ''America'' in which they intended to cross the Atlantic in order to win the [[Daily Mail aviation prizes|£10,000 prize]] offered by the British ''[[Daily Mail]]'' newspaper for the first aircraft to cross the Atlantic. Following the outbreak of war in Europe, Porte returned to England and rejoined the [[Royal Navy]], eventually becoming commander of the [[RNAS Felixstowe|naval air station]] at [[Felixstowe]] where he recommended the purchase from [[Curtiss Aeroplane and Motor Company|Curtiss]] of an improved version of the ''America'' flying boat on which he had worked, the [[Curtiss H-4]] type,<ref name="Bruce pt 1 p843-4">Bruce ''Flight'' 2 December 1955, pp. 843–844.</ref> resulting in the RNAS receiving two prototype ''Americas'' and 62 H-4s.<ref name="Thetford Navy p78">Thetford 1978, p.78.</ref>

The Curtiss H-4 was found to have a number of problems, being both underpowered and having a hull too weak for sustained operations and having poor handling characteristics when afloat or taking off.<ref name="Bruce pt 1 p844">Bruce ''Flight'' 2 December 1955, p.844.</ref><ref name="London p16-7">London 2003, pp. 16–17.</ref>  One flying boat pilot, Major Theodore Douglas Hallam, wrote that they were "comic machines, weighing well under two tons; with two comic engines giving, when they functioned, 180 horsepower; and comic control, being nose heavy with engines on and tail heavy in a glide."<ref name="Spider p21-2">Hallam 1919, pp. 21–22.</ref>

To try to resolve the H-4's hydrodynamic issues, in 1915 Porte carried out a series of experiments on four H-4s fitted with a variety of modified hulls,<ref name="Bruce pt 1 p844"/> using the results of these tests to design a new {{convert|36|ft|m|adj=mid|-long}} hull which was fitted to the wings and tail of an H-4, [[United Kingdom military aircraft serials|serial number]] ''3580'', with a pair of 150&nbsp;hp (112&nbsp;kW) [[Hispano-Suiza 8]] engines as the [[Felixstowe F.1]].<ref name="London p18">London 2003, p.18.</ref>  Rather than the lightweight boat-type structure of the Curtiss boats, the F.1's hull was based around a sturdy wooden box-girder similar to that used in contemporary landplanes, to which were attached a single-step planing bottom and side sponsons.  Once modified by the fitting of a further two steps, the new hull proved to give much better take off and landing characteristics and was much more seaworthy.<ref name="Bruce pt 1 p845-6">Bruce ''Flight'' 2 December 1955, pp. 845–846.</ref><ref name="Thetford Navy p191">Thetford 1978, p. 191.</ref>

Porte then designed a similar hull, for the larger [[Curtiss H-12]] flying boat, which while larger and more capable than the H-4s, shared failings of a weak hull and poor water handling. The combination of the new Porte II hull,<ref>{{cite journal|last1=Rennie|first1=Major John Douglas|editor1-last=Pritchard|editor1-first=J. Laurence|title=SOME NOTES ON THE DESIGN, CONSTRUCTION AND OPERATION OF FLYING BOATS|journal=The Journal of the Royal Aeronautical Society|date=1923|volume=XXVII|pages=136–137|url=https://archive.org/stream/journalaero27roya#page/136/mode/2up/search/fury+servo|accessdate=25 September 2015|publisher=Royal Aeronautical Society|location=University of Toronto}}</ref> this time fitted with two steps, with the wings of the H-12, a new tail and powered by two [[Rolls-Royce Eagle]] engines was named the Felixstowe F.2; its first flight was in July 1916,<ref name="London p24-5">London 2003, pp. 24–25.</ref> proving greatly superior to the Curtiss on which it was based.<ref name="Bruce pt 1 p846">Bruce ''Flight'' 2 December 1955, p. 846.</ref> The F.2 entered production as the Felixstowe F.2A, being used as a patrol aircraft, with about 100 being completed by the end of World War I. Another seventy were built, and these were followed by two F.2C which were built at Felixstowe.

In February 1917, the first prototype of the [[Felixstowe F.3]] was flown.  This was larger and heavier than the F.2, giving it greater range and heavier bomb load, but poorer agility.  Approximately 100 Felixstowe F.3s were produced before the end of the war.

The [[Felixstowe F.5]] was intended to combine the good qualities of the F.2 and F.3, with the prototype first flying in May 1918.  The prototype showed superior qualities to its predecessors but the production version was modified to make extensive use of components from the F.3, in order to ease production, giving lower performance than either the F.2A or F.3.
[[File:F.2A in dazzle scheme.jpg|thumb|Felixstowe F.2A (N4545) in [[Dazzle paint|dazzle scheme]] during an anti-submarine patrol. The dazzle camouflage adopted aided identification during air combat and on the water in the event of being forced down.]]
[[File:Felixstowe F.2A Q 082243.jpg|thumb|Felixstowe F.2A (N4283), finished in black and white scheme, in flight in March 1918, being flown by Captains [[Robert Leckie (RCAF officer)|R. Leckie]] and [[Gerald Livock|G. E. Livock]].]]

==Operational history==
The Felixstowe F.2A was used as a patrol aircraft over the [[North Sea]] until the end of the war. Its excellent performance and maneuverability made it an effective and popular type, often fighting enemy patrol and fighter aircraft, as well as hunting [[U-boats]] and [[Zeppelins]].  The larger F.3, which was less popular with its crews than the more maneuverable F.2A, served in the Mediterranean and the North Sea.

The F.5 did not enter service until after the end of World War I, but replaced the earlier Felixstowe boats (together with Curtiss flying boats) to serve as the [[RAF]]'s standard flying boat until being replaced by the [[Supermarine Southampton]] in 1925.

==Variants==
*'''F.2A:''' Based on Curtiss H12 with new hull.  Powered by two 345&nbsp;hp Rolls-Royce Eagle VIII engines. 4 to 7 machine guns and 460&nbsp;lb of bombs.
*'''F.2C:'''  Modified F.2A with lighter hull.  Two built.

==Operators==
;{{CHI}}
*[[Chilean Air Force]]
;{{NLD}}
*[[Netherlands Naval Aviation Service|Royal Netherlands Naval Air Service]] - one in service (N4551), receiving serial L-1<ref>{{cite web|last1=Jonker|first1=K.W.|title=Felixstowe F2A|url=https://web.archive.org/web/20150620165503/http://kw.jonker.co/index.php?option=com_content&view=article&id=658:felixstowe-f2a-uk&catid=87&Itemid=549&showall=&limitstart=1&lang=en|website=Nederlandse Modelbouw en Luchtvaartsite Modelling and Aviation|publisher=K.W. Jonker|accessdate=31 December 2014}}</ref>
;{{UK}}
*[[Royal Naval Air Service]]
*[[Royal Air Force]]
**[[No. 228 Squadron RAF]]
**[[No. 230 Squadron RAF]]
**[[No. 231 Squadron RAF]]
**[[No. 232 Squadron RAF]]
**[[No. 234 Squadron RAF]]
**[[No. 240 Squadron RAF]]
**[[No. 247 Squadron RAF]]
**[[No. 249 Squadron RAF]]
**[[No. 257 Squadron RAF]]
**[[No. 259 Squadron RAF]]
**[[No. 261 Squadron RAF]]
**[[No. 267 Squadron RAF]]
;{{USA}}
*[[United States Navy]]

==Specifications (F.2A) ==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=British Naval Aircraft since 1912 <ref name="Thetford Navy p194,6">Thetford 1978, pp. 194, 196.</ref> 
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=4
|capacity=
|length main= 46 ft 3 in
|length alt= 14.1 m
|span main= 95 ft 7½ in
|span alt= 29.15 m
|height main= 17 ft 6 in
|height alt= 5.34 m
|area main= 1,133 ft²
|area alt= 105.3 m²
|airfoil=
|empty weight main= 7,549 lb
|empty weight alt= 3,424 kg
|loaded weight main= 10,978 lb
|loaded weight alt=4,980 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|more general= 
|engine (prop)= [[Rolls-Royce Eagle]] VIII
|type of prop= V12 piston
|number of props=2
|power main= 345 hp
|power alt= 257 kW
|power original=
|max speed main= 95½ mph at 2,000 ft
|max speed alt= 83 knots, 154 km/h
|max speed more= 
|cruise speed main= 
|cruise speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|stall speed main= 
|stall speed alt= 
|range main= <!--nm -->
|range alt= <!--mi,  km -->
|ceiling main= 9,600 ft
|ceiling alt= 2,926 m
|climb rate main=  <!--522 ft/min (3 min 50 s to 2,000 ft) -->
|climb rate alt= <!--159 m/min to 609 m -->
|loading main= 9.69 lb/ft²
|loading alt= 47.4 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.063 hp/lb
|power/mass alt= 0.10 kW/kg
|more performance=*'''Endurance:''' 6 hours
*'''Climb to 2,000 ft (610 m):''' 3 min 50 s.
*'''Climb to 10,000 ft (3,050 m):''' 39 min 30 s.
|guns=4 &times; [[.303 British|.303 in]] (7.7 mm) [[Lewis Gun]]s, 1 in nose, 3 amidships
|bombs= Up to 460 lbs of bombs beneath wings
|avionics=
}}

==See also==
{{aircontent
<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Felixstowe F.1]]
*[[Curtiss H-12]]
*[[Curtiss H-16]]
*[[Felixstowe F.3]]
*[[Felixstowe F.5]]
*[[Felixstowe F5L]]

<!-- aircraft similar in appearance or function to this design: -->
|similar aircraft=
<!-- any lists that are appropriate: -->
|lists=

<!-- other articles that could be useful to connect with: -->
|see also=
}}

==References==
; Notes
{{reflist|2}}
;Bibliography
{{refbegin}}
* Bruce, J.M. [http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201723.html "The Felixstowe Flying-Boats: Historic Military Aircraft No. 11, Part 1."] ''[[Flight International|Flight]]'', 2 December 1955, pp.&nbsp;842–846.
* Bruce, J.M. [http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201772.html "The Felixstowe Flying-Boats: Historic Military Aircraft No. 11, Part 2."] ''Flight'', 16 December 1955, pp.&nbsp;895–898.
* Bruce, J.M. [http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201806.html "The Felixstowe Flying-Boats: Historic Military Aircraft No. 11, Part 3."] ''Flight'', 23 December 1955, pp.&nbsp;929–932.
* Bruce, J.M. ''British Aeroplanes 1914-18''. London: Putnam, 1957.
* Hallam, T.D. ''The Spider Web: The Romance of a Flying Boat War Flight''. London: William Blackwood, 1919. 
* London, Peter. ''British Flying Boats''. Stroud, UK: Sutton Publishing, 2003. ISBN 0-7509-2695-3.
* Thetford, Owen. ''British Naval Aircraft since 1912''. London: Putnam, Fourth edition 1978. ISBN 0-370-30021-1.
{{refend}}

==External links==
{{commons category|Felixstowe Flying Boats}}
*[http://www.iwm.org.uk/collections/item/object/1060022770 Pigeons and Seaplanes]: Film of [[carrier pigeon]]s used to send messages from British seaplanes in the [[North Sea]] during the First World War, including the Felixstowe F.2, c.1917.
*[http://www.willhiggs.co.uk/dundee/felixstowes.htm Felixstowe Flying-Boats]

{{Seaplane Experimental Station aircraft}}
{{wwi-air}}

[[Category:British patrol aircraft 1910–1919]]
[[Category:Flying boats]]
[[Category:Felixstowe aircraft|F.2]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Biplanes]]