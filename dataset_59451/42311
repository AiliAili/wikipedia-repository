<!-- This article is a part of [[Wikipedia:Project Aircraft]]. Please see [[Wikipedia:Project Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Model 76 Duchess
  |image = Ntps-be76-N5410M-090123-02-cr8.jpg
  |caption = 1978 model Beech 76 Duchess operated by the [[National Test Pilot School]] at the [[Mojave Airport]]
}}{{Infobox Aircraft Type
  |type = Four-seat cabin monoplane
  |manufacturer = [[Beechcraft]]
  |first flight = September 1974<ref name="Green">Green, William: ''Observers Aircraft'', page 48. Frederick Warne Publishing, 1980. ISBN 0-7232-1604-5</ref>
  |introduction = 1978<ref name="Green"/>
  |retired = 
  |status = 
  |primary user = Flight schools<ref name="Green"/>
  |more users = 
  |produced = 
  |number built = 437
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}
[[File:Beech 76 N6697D Timm, Milw, WI 07.06.06R edited-2.jpg|thumb|US registered 1979 model Duchess]]
[[File:Beechcraft76DuchessC-GJFE02.jpg|thumb|1979 model Duchess]]

The '''Beechcraft Model 76 Duchess''' is an [[United States|American]] twin-engined monoplane built by [[Beechcraft]].<ref name="Green"/>

The Duchess is a cantilever low-wing monoplane with an all-metal structure, four seats, retractable tricycle undercarriage and a T-tail. It is powered by one {{convert|180|hp|kW|0|abbr=on}} [[Lycoming O-360]]-A1G6D on the left wing and one LO-360-A1G6D on the right wing, which drive counter-rotating, constant-speed two-bladed propellers.<ref>{{cite book |last= Frawley|first= Gerard|title=The International Directory of Civil Aircraft, 2003-2004 |year=2003 |publisher=Aerospace Publications Pty Ltd |location= Fyshwick, ACT, Australia |isbn=1-875671-58-7 |page=41 }}</ref>

==Design and development==

The Duchess was developed by [[Beechcraft]] from the single-engined [[Beechcraft Musketeer]].

The prototype was first flown in September 1974 with the first production version flown on 24 May 1977. Deliveries to the Beech Aero Centers commenced early in 1978.<ref name="Green"/>

The Model 76 was designed as an economical twin-engine trainer for the Beech Aero Centers and to compete with the very similar [[Piper PA-44 Seminole]] as well as the [[Cessna 310]].<ref name="Green"/>

The Model 76 incorporates engines that turn in different directions to eliminate the [[critical engine]] from single engine operation.<ref name="Plane and Pilot">Plane and Pilot: ''1978 Aircraft Directory'', page 84. Werner & Werner Corp, Santa Monica CA, 1977. ISBN 0-918312-00-0</ref>

The Duchess wing is of honeycomb construction fastened by bonding, rather than rivets, to reduce cost and produce a smoother aerodynamic surface.<ref name="Plane and Pilot"/>

The Duchess is no longer in production but large numbers remain in use in flight schools around the world.

===T-tail===
The use of a [[T-tail]] on the Model 76 met with mixed critical reception when the aircraft was introduced. Plane & Pilot pronounced: "Outstanding design characteristics of the new Duchess include an aerodynamically advantageous T-tail, which places the horizontal surfaces above the propeller slipstream for better stability and handling.",<ref name="Plane and Pilot"/> while Gerald Foster said: "[Beechcraft's] interest in T-tails was perhaps an affectation triggered by their wide use on jet airliners".<ref name="Foster">Montgomery, MR & Gerald Foster: ''A Field Guide to Airplanes, Second Edition'', page 92. Houghton Mifflin Company, 1992. ISBN 0-395-62888-1</ref>

==Specifications==
[[File:Beechcraft 76 Duchess C-FDMO instrument panel 01.JPG|thumb|1976 model Duchess instrument panel]]
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's All The World's Aircraft 1980–81<ref name="JAWA80 p268-9">Taylor 1980, pp. 268–269.</ref> and 1978 Beechcraft Duchess Pilot Operating Handbook
|crew=1
|capacity=3 passengers
|payload main=
|payload alt=
|length main=29ft 0½ in.
|length alt=8.86 m
|span main=38 ft 0 in
|span alt=11.58 m
|height main=9 ft 6 in
|height alt=2.89 m
|area main=181 sq ft
|area alt= 16.81 m<sup>2</sup>
|aspect ratio=7.973:1
|airfoil= Root: NACA 63A413.2; Tip: NACA 63A415<ref name="Lednicer">{{cite web|url = http://www.ae.uiuc.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage|accessdate = 2008-07-27|last = Lednicer|first = David|authorlink = |date=October 2007}}</ref>
|empty weight main=2,446 lb
|empty weight alt= 1,116 kg
|loaded weight main= 3500 lb
|loaded weight alt= 1590 kg
|useful load main= 1,470 lb
|useful load alt= 668 kg
|max takeoff weight main= 3,900 lb.
|max takeoff weight alt= 1,772 kg
|more general=
|engine (prop)= [[Lycoming O-360]]-A1G6D (on left wing; LO-360-A1G6D on right wing, turning in opposite direction)
|type of prop= air-cooled [[flat-four]]
|number of props=2
|power main= 180 hp
|power alt= 134 kW
|power original=
|never exceed speed main= 194 knots
|never exceed speed alt= 223 mph, 359 km/h
|cruise speed main= 154 knots
|cruise speed alt= 177 mph, 285 km/h
|cruise speed more=at 10,000 ft (3,050 m)
|stall speed main=60 knots 
|stall speed alt=69 mph, 111 km/h
|stall speed more= flaps down, ([[Indicated air speed|IAS]] 
|range main= 780 nmi
|range alt=898 mi, 1,445 km
|range more=at 12,000 ft (3,660 m) (econ cruise) - 151 knots
|ceiling main= 19,650 ft
|ceiling alt= 5,990 m
|climb rate main= 1,248 ft/min
|climb rate alt= 6.3 m/s
|loading main=
|loading alt=
|thrust/weight=
|mass/power main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent
|related=
* [[Beechcraft Musketeer]]
|similar aircraft=
* [[Gulfstream American GA-7 Cougar]]
* [[Piper Seminole]]
|see also=
}}

==References==
;Notes
{{Reflist}}
;Bibliography
* {{cite book |last= |first= |authorlink= |coauthors= |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=}}
* {{cite book |last=Simpson |first= R.W.|authorlink= |coauthors= |title= Airlife's General Aviation|year= 1991|publisher= Airlife Publishing|location=Shrewsbury, England |isbn=1 85310 194 X}}
* {{cite book|last=Taylor|first=John W. R.|title=Jane's All The World's Aircraft 1980–81|year=1980|publisher=Jane's Publishing Company|location=London|isbn=0-7106-0705-9|authorlink=John W. R. Taylor}}

==External links==
{{commons category-inline|Beechcraft Duchess}}

{{Beechcraft}}

[[Category:Beechcraft aircraft|Duchess]]
[[Category:United States civil trainer aircraft 1970–1979]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:T-tail aircraft]]