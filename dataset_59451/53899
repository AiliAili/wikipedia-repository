{{external links|date=April 2014}}
[[Image:RubensTube.png|right|thumb|290px|A Rubens' tube setup]]

A '''Rubens' tube''', also known as a '''standing wave flame tube''', or simply '''flame tube''', is an antique [[physics]] apparatus for  demonstrating acoustic [[standing wave]]s in a tube.  Invented by German physicist [[Heinrich Rubens]] in 1905, it graphically shows the relationship between [[sound waves]] and [[sound pressure]], as a primitive [[oscilloscope]].  Today, it is used only occasionally, typically as a demonstration in physics education.

==Overview==
A length of pipe is perforated along the top and sealed at both ends - one seal is attached to a small speaker or frequency generator, the other to a supply of a flammable gas (propane tank). The pipe is filled with the gas, and the gas leaking from the perforations is lit. If a suitable constant frequency is used, a standing wave can form within the tube. When the speaker is turned on, the standing wave will create points with oscillating (higher and lower) pressure and points with constant pressure (pressure nodes) along the tube. Where there is oscillating pressure due to the sound waves, less gas will escape from the perforations in the tube, and the flames will be lower at those points. At the pressure nodes, the flames are higher. At the end of the tube gas molecule velocity is zero and oscillating pressure is maximal, thus low flames are observed. It is possible to determine the wavelength from the flame minimum and maximum by simply measuring with a ruler.

==Explanation==
Since the time averaged pressure is equal at all points of the tube, it is not straightforward to explain the different flame heights. The flame height is proportional to the gas flow as shown in the figure. Based on [[Bernoulli's principle]], the gas flow is proportional to the square root of the pressure difference between the inside and outside of the tube. This is shown in the figure for a tube without standing sound wave. Based on this argument, the flame height depends non-linearly on the local, time-dependent pressure. The time average of the flow is reduced at the points with oscillating pressure and thus flames are lower.<ref>G.W. Ficken, F.C. Stephenson, Rubens flame-tube demonstration, [http://dx.doi.org/10.1119/1.2340232 The Physics Teacher, Vol. 17, pp. 306-310 (1979)]</ref>

[[File:Rubens flame.png|thumb|Flame height on a Rubens tube (without standing sound wave) for different flows of natural gas. Dashed line is linear fit.]]
[[File:Rubens pressure.png|thumb|Square root of the pressure difference between inside and outside of Rubens tube (without standing sound wave) for different flows of natural gas. Dashed line is linear fit.]]

==History==
[[Heinrich Rubens]] was a German physicist born in 1865. Though he allegedly worked with better remembered physicists such as Max Planck at the University of Berlin on some of the ground work for quantum physicists, he is best known for his flame tube, which was demonstrated in 1905.  This original Rubens' tube was a four-meter section of pipe with 200 holes spaced evenly along its length.

When the ends of the pipe are sealed and a flammable gas is pumped into the device, the escaping gas can be lit to form a row of flames of roughly equal size.  When sound is applied from one end by means of a loudspeaker, internal pressure will change along the length of the tube.  If the sound is of a frequency that produces standing waves, the wavelength will be visible in the series of flames, with the tallest flames occurring at pressure nodes, and the lowest flames occurring at pressure antinodes. The pressure antinodes correspond to the locations with the highest amount of compression and [[rarefaction]].<ref>G.W. Ficken, F.C. Stephenson, Rubens flame-tube demonstration, [http://dx.doi.org/10.1119/1.2340232 The Physics Teacher, Vol. 17, pp. 306-310 (1979)]</ref>

==Public displays==
A Rubens' tube was on display at ''The Exploratory'' in [[Bristol|Bristol, England]] until it closed in 1999. A similar exhibit using polystyrene beads instead of flames featured in the ''[[At-Bristol]]'' science centre until 2009.<ref>{{cite web|url=http://www.exploratory.org.uk/exhibits/sound.htm|title=The Exploratory - Exhibits |accessdate=November 6, 2006}}</ref> Students make models of rubens' tube at their school science exhibition.

This display is also found in physics departments at a number of universities.<ref>{{cite web | url=http://pirt.asu.edu/detail_3.asp?ID=1462&offset=175 | title=Oscillation & Waves | accessdate=November 8, 2006}}</ref>
A number of physics shows also have one, such as: Rino Foundation <ref>{{cite web|url=http://www.stichtingrino.nl |title=website Rino Foundation|accessdate=October 29, 2009}}</ref> (The Netherlands), Fysikshow Aarhus (Denmark), Fizika Ekspres (Croatia) and ÅA Physics show (Finland).<ref>{{cite web|url=http://fizika-ekspres.hfd.hr/web2/index.php|title=Fizika Ekspres website |accessdate=April 20, 2009}}</ref><ref>{{cite web|url=http://users.abo.fi/jlinden/fysikshow.html|title=ÅA website |accessdate=April 20, 2009}}</ref>

The [[MythBusters]] also included a demonstration on their "Voice Flame Extinguisher" episode in 2007.<ref>{{cite web|url=http://dsc.discovery.com/videos/mythbusters-rubens-tube.html|title=Discovery Channel Video |accessdate=August 11, 2009}}</ref>  The [[Daily Planet (TV series)|Daily Planet]]'s The Greatest Show Ever,<ref>{{cite web|url=http://www.discoverychannel.ca/greatestshowever/ |title=Daily Planet's The Greatest Show Ever |accessdate=October 10, 2010}}{{dead link|date=October 2013}}</ref> ran a competition whereby five Canadian science centres competed for the best science centre's experiment/display.  Edmonton's Science Centre (Telus World of Science) utilized a Rubens' tube, and won the competition.  The special was filmed on October 10, 2010.
Tim Shaw on the show Street Genius on National Geographic Channel also featured one in Episode 18 "Wave of fire".

The artist [[Emer O'Brien]] used Rubens tubes as the basis for the [[sound sculpture]] featured in her 2012 exhibition ''Return to Normal'' at the [[Wapping Hydraulic Power Station|Wapping Project]] in [[London]].<ref>{{cite web
|url=http://www.re-title.com/public/newsletters/14_June_12_-_Sculpture_Installation_Feature_Newsletter_0.htm
|title = Emer O'Brien - Return to Normal
|publisher=re-title
|accessdate=January 1, 2014
}}</ref>

== 2D Rubens' Tube ==

=== Overview ===
A 2D Rubens’ tube, also known as a pyro board, is a plane of [[Bunsen burner]]s that can demonstrate an acoustic [[standing wave]] in two dimensions. Similar to its predecessor, the one dimensional Rubens' tube, this standing wave is caused by a multitude of factors. Pressure variation caused by the inflow of propane gas interfering with the input of [[sound waves]] into the plane causes changes in the height and color of the flames.  The 2D Rubens’ tube was made famous by a Danish science demonstrator group in Denmark called [http://www.fysik.su.se/skolor_allmanhet/fysikshow/ Fysikshow].<ref>{{Cite web|title = FysikShow - Fysikum|url = http://www.fysik.su.se/skolor_allmanhet/fysikshow/|website = www.fysik.su.se|access-date = 2016-02-06}}</ref>

=== Explanation ===
A 2D Rubens’ tube is made up of a lot different parts. The main part itself is the rectangular steel box that outputs the propane gas. Steel is generally used for the plane on pyro boards because the compound can generally withstand immense amounts of heat and still be able to maintain its structure. Holes are drilled on the top of the steel plane to output the propane gas that is being constantly and slowly pumped into the steel box.<ref>{{Citation|title = Pyro Board: 2D Rubens' Tube!|url = https://www.youtube.com/watch?v=2awbKQ2DLRE|date = 2014-04-17|accessdate = 2016-02-06|last = Veritasium}}</ref> Instead of having a complete steel box, some pyro boards designs have wooden sides to support the steel plane on top.  In wooden-style pyro boards, the interior of the box is usually covered with some sort of heat-resistant [[membrane]] that prevents the propane inside the box from leaking.

On the sides of the steel box are speakers that input a sound into the contained medium. The rate at which the propane gas escapes through the holes on the top of the pyro board is dependent on the intensity of the inputted sound. This relationship is directly proportional, meaning as the intensity of the sound increases, the rate at which the propane gas increases.

Since the medium inside the steel box is kept at a constant volume, a standing wave has the ability to be produced. The frequency at which the standing wave can be produced is largely dependent on the physical dimensions of the box and the wavelength of the wave. Since pyro boards range in sizes, each board has its own unique frequencies at which a standing wave can be produced.

=== Public displays ===
In 2014, Danish science demonstrator [http://www.whoi.edu/website/nirvana/ Sune Nielsen], a member of [http://www.fysik.su.se/skolor_allmanhet/fysikshow/ Fysikshow], teamed up with science blogger [[Derek Muller]] in a [https://www.youtube.com/watch?v=2awbKQ2DLRE YouTube video] showing off the pyro board in action. Derek Muller, also known as [http://veritasium.com/ Veritasium] on YouTube, explains the science behind how the 1D and 2D Rubens’ Tubes work.

==References==
<references/>

==External links==
{{Commons category|Rubens' Tube}}
*[http://player.vimeo.com/video/37587421 Baroque hoedown for 6 Ruben's tubes by Mathew Kneebone and Yuri Suzuki]
*[https://www.youtube.com/watch?v=oPWucNgN8TQ Detailed Video including sound board and microphone]
*[https://web.archive.org/web/20110718054118/http://www.vuw.ac.nz/scps-demos/demos/Light_and_Waves/SoundFlames/SoundFlames.htm Experiment notes, video & detailed analysis]
*[http://www.fysikbasen.dk/English.php?page=Vis&id=6 Flame tube setup and explanation of effects]
*[http://www.physics.umd.edu/lecdem/services/demos/demosh3/h3-17.htm Brief Setup Guide]
*[http://www.physics.isu.edu/physdemos/waves/flamtube.htm Classroom setup guide]
*[https://web.archive.org/web/20040724131501/http://www.science-on-stage.de/fileadmin/Materialien/CD_POS3/Luehrs_waterwave_englisch.doc Information on Rubens' original design in .doc format]
*[http://groups.physics.umn.edu/demo/old_page/demo_gifs/3D30_50.GIF Image showing setup]
*[https://web.archive.org/web/20110929204818/http://physicslearning.colorado.edu/website_new/Common/ViewDemonstration.asp?DemoCode=3D30.50 General information]
*[http://pirt.asu.edu/detail_3.asp?ID=1462&offset=175 Experiment setup - under "Links" heading and photo illustrating this experiment]
*[https://web.archive.org/web/20110930144701/http://www.richdunajewski.com/videos/YHpovwbPGEoo Video various tones and music being played]
*[https://www.youtube.com/watch?v=cootexkMmrY Rubens' Tube performance by Alyce Santoro]
*[http://dcc.umd.edu/portfolio/gfiola/ DCC capstone proposal]
*[https://www.youtube.com/watch?v=Y4Q4tirAOmU Fysikshow YouTube demonstration]

[[Category:Fire]]
[[Category:Physics experiments]]
[[Category:Acoustics]]
[[Category:Wave mechanics]]
[[Category:History of physics]]