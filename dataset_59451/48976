{{Use dmy dates|date=June 2014}}
{{Infobox scientist
|name              = Alfredo Kanthack
|image             = Alfredo Antunes Kanthack.jpg
|image_size       =
|caption           = 
|birth_date        = {{birth date|df=yes|1863|3|4}}
|birth_place       = [[Bahia]], [[Brazil]]
|death_date        = {{death date and age|df=yes|1898|12|21|1863|3|4}}
|death_place       = [[Cambridge]], [[United Kingdom]]
|residence         = 
|citizenship       = 
|nationality       = [[Brazil]]ian
|ethnicity         = 
|field             = [[Microbiology]], [[Pathology]], [[Immunology]], [[Epidemiology]]
|work_institutions = [[Liverpool Royal Infirmary]], <br>[[St Bartholomew's Hospital]], [[University of Cambridge]]
|alma_mater        = [[Liverpool University]] <br>[[University of Cambridge]], <br>[[London University]]
|doctoral_advisor  = 
|doctoral_students = 
|known_for         = 
|author_abbrev_bot = 
|author_abbrev_zoo = 
|influences        = 
|influenced        = 
|prizes            = 
|footnotes         = 
|signature         =
}}

'''Alfredo Kanthack''' [[Bachelor of Arts|BA]] (Lond), [[BSc]], [[Bachelor of Medicine, Bachelor of Surgery|BS]], [[Master of Arts (Oxbridge and Dublin)|MA]] (Cantab), [[Bachelor of Medicine|MB]], [[Doctor of Medicine|MD]], [[LRCP]], [[Fellow of the Royal College of Physicians|FRCP]], [[FRCS]] (1863-1898) was a Brazilian-born [[microbiologist]] and [[pathologist]] who worked in England. His distinguished career was cut short by his premature death at the age of 35.<ref name ="  RCS_bio" >{{cite web|url=http://livesonline.rcseng.ac.uk/biogs/E002409b.htm|title=Kanthack, Alfredo Antunes - Biographical entry - Plarr's Lives of the Fellows Online|work=rcseng.ac.uk|accessdate=24 March 2015}}</ref><ref name ="  HRC" >{{cite web|url=http://www.histonroadcemetery.org/graves/CAMBRIDGE%20LIVES/AlfredoKanthack.html|title=Histon Road Cemetery|work=histonroadcemetery.org|accessdate=24 March 2015}}</ref>

==Early life==
Alfredo Antunes Kanthack was born on 4 March 1863 in [[Bahia]], [[Brazil]], the second son of Emilio Kanthack, and lived in Brazil until he was six years old when his father's business took the family firstly to Germany in 1869, and then to Liverpool in 1881.<ref name =  RCS_bio /> His father subsequently returned to Brazil as [[Consul (representative)|British Consul]] at [[Pará]] in 1886.<ref>{{cite web|url=https://www.thegazette.co.uk/London/issue/25598/page/2909|title=Page 2909 - Issue 25598, 18 June 1886 - London Gazette - The Gazette|work=thegazette.co.uk|accessdate=24 March 2015}}</ref>

==Education==
From 1871-1881 he went to school in Germany, first in Hamburg and then in [[Wandsbeck]], [[Lüneburg]], and [[Gutersloh]].<ref name =  RCS_bio />

After moving to England in 1881, aged 18, he went to [[Liverpool College]] for a few months and then in 1882 became a student at [[University of Liverpool#University College Liverpool|University College, Liverpool]], where his academic brilliance became apparent, passing the matriculation exams for the [[University of London]] with [[Honours degree|honours]].<ref name =  RCS_bio />

In 1887 he went to [[St Bartholomew's Hospital]], London, to study medicine and was awarded a Gold Medal in obstetrics.<ref name =  RCS_bio />

In 1889 he continued his studies in Berlin under Wilhelm Krause; cellular pathologist [[Rudolf Virchow]]; and pioneering microbiologist and founder of modern bacteriology, [[Robert Koch]], who received a [http://www.nobelprize.org/nobel_prizes/medicine/laureates/1905/ Nobel Prize in 1905] for his groundbreaking research on [[tuberculosis]].<ref name =  RCS_bio /> His studies in Germany demonstrated his accurate observation and original thought in the field of research.<ref name =  RCS_bio />

==Career==
Returning from Berlin in 1890 he served as Obstetric Resident at St Bartholomew's Hospital<ref name =  RCS_bio />
[[File:Map showing the distribution of Leprosy in India Wellcome L0039112.jpg|thumb|Map by Alfredo Kanthack showing the increase and decrease of Leprosy in India since 1881. Wellcome L0039112]]
While holding this position he was appointed by the [[Royal College of Surgeons of England|Royal College of Surgeons]], [[Royal College of Physicians]], and the Executive Committee of the National Leprosy Fund as one of the Special Commissioners to investigate the prevalence and treatment of [[leprosy]] in India.<ref name =  RCS_bio /><ref name =  HRC /> Among his conclusions were that direct contagion was at the most a very small factor in causing the spread of leprosy, and that compulsory segregation of lepers was not advisable.<ref name =  RCS_bio />

On his return from India in 1891 he matriculated at Cambridge as a Fellow [[Commoner (academia)|Commoner]] of [[St John's College, Cambridge|St John's College]]. At the same time he was appointed [http://archive.spectator.co.uk/article/27th-november-1886/14/the-john-lucas-walker-studentship John Lucas Walker Student in Pathology], a scholarship given for original pathological research, under which he studied immunity.<ref name =  RCS_bio />

In 1892 he left Cambridge and began to practise as a physician in Liverpool. He was appointed Medical Tutor and Registrar at the Liverpool Royal Infirmary where he set up a Bacteriological Laboratory,<ref name =  HRC /> Senior Demonstrator of Bacteriology in a post specially created for him, and also Medical Tutor at University College, Liverpool.<ref name =  RCS_bio />

[[File:St. Bartholomew's Hospital, 1896. Wellcome M0010860.jpg|thumb|St. Bartholomew's Hospital, 1896. Kanthack is standing in white coat. <br>Wellcome Images]]
In 1893 he returned to St Bartholomew's as Director of the Pathological Department in the Medical School and Hospital, Lecturer on Pathology and Bacteriology, and Curator of the Museum.<ref name =  RCS_bio />

In 1894 he suffered from a severe attack of [[typhoid fever]], but in spite of this won the prestigious Royal College of Surgeons' [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2312237/ Jacksonian Prize] in 1895 for his essay ''The Aetiology of Tetanus and the Value of the Serum Treatment.''<ref name =  RCS_bio />

In 1896 he acted as Deputy to Professor Charles Roy, who had been appointed to the new Chair in Pathology at Cambridge<ref name = CamPath>{{cite web|url=http://www.path.cam.ac.uk/about/history.html|title=Department of Pathology|work=cam.ac.uk|accessdate=24 March 2015}}</ref> but was unable to take up the post due to ill-health. This required Kanthack to travel to and from St Bartholomew's and the University. In the spring of 1897 he moved to Cambridge where he was given an honorary MA and elected a Fellow of [[King's College, Cambridge|King's]]. In the autumn he succeeded Roy as Professor of Pathology at the age of 34.<ref name =  RCS_bio />

During the autumn of 1898 the report of his research on the [[tsetse fly]], conducted by him in conjunction with [[Herbert Durham]] and [http://www.bate.ox.ac.uk/assets/files/WALTER%20FIELDING%20HOLLOWAY%20BLANDFORD.pdf Walter Blandford], was published. This report cleared the ground for further investigation of what was then known as tsetse-fly disease, although no method of prevention or cure was yet propounded<ref name =  RCS_bio /> His last work, in conjunction with Dr Sydney Sladen, reported on tuberculous milk.<ref name =  RCS_bio /><ref>{{cite web|url=http://www.thelancet.com/journals/lancet/article/PIIS0140-6736%2801%2979094-0/abstract|title=An Error Occurred Setting Your User Cookie|work=thelancet.com|accessdate=24 March 2015}}</ref>

Shortly before his death he recommended one of his promising students, [[Walter Myers (physician)|Walter Myers]], for a John Lucas Walker scholarship.

Kanthack is credited for the use of [[formalin]] as a [[histological]] fixative.<ref name = CamPath/>

==Death==
He died of cancer on 21 December 1898 aged 35. After his funeral in [[Kings College Chapel]], he was buried at [[Histon Road Cemetery]], Cambridge.<ref name =  HRC />

After his death his wife donated funds in his memory<ref>{{Cite journal|title=The Kanthack Memorial Fund|journal=British medical journal|volume=1|issue=1994|pages=689|pmid=20758376|pmc=2462566|year=1899|doi=10.1136/bmj.1.1994.689-a}}</ref> to establish a library in the Pathology Department which still bears his name,<ref name =  HRC /><ref name = CamPath/> and a bronze memorial plaque was placed at the entrance in Tennis Court Road.<ref name =  HRC />

A sum of money was collected after his death to establish  ''The Kanthack Memorial Library''  in the Pathological Institute of St Bartholomew's Hospital.<ref name =  RCS_bio /><ref>{{cite journal |pmc=2462566 |title=The Kanthack Memorial Fund|journal=British Medical Journal|volume=1|issue=1994|pages=689|year=1899|pmid=20758376|doi=10.1136/bmj.1.1994.689-a}}</ref>

The ''Kanthack Medal'' has been awarded at Liverpool since 1900 for experimental pathology and microbiology, reflecting the importance of his work there.<ref>{{cite web|url=http://www.liverpoolmuseums.org.uk/walker/exhibitions/doves/liverpool1900/bronze_medal_mcnair.aspx|title=University College Liverpool: Kanthack Medal, Charles J Allen and James Herbert McNair|work=liverpoolmuseums.org.uk|accessdate=24 March 2015}}</ref>

==Bibliography==
'''Books'''
*''Manual of Practical Morbid Anatomy Being a Handbook for the Post-Mortem Room. ''  A A Kanthack, Humphry, R Rolleston.  Cambridge University Press, 1894
*[https://archive.org/details/acourseelementa00drysgoog ''A Course of Elementary Practical Bacteriology, Including Bacteriological Analysis and Chemistry''.] Alfredo Antunes Kanthack, John Hannah Drysdale. Macmillan 1896
*[https://books.google.co.uk/books?id=VVk5qAAACAAJ&dq=kanthack+durham&hl=en&sa=X&ei=1Uz8VICYB4LoOOHfgaAO&ved=0CCEQ6AEwAA ''On Nagana, or Tsetse Fly Disease.'' A. A. Kanthack, H. E. Durham and W. F. H. Blandford, 1898]
*[http://www.worldcat.org/title/influence-of-the-milk-supply-on-the-spread-of-tuberculosis-based-upon-an-investigation-in-cambridge/oclc/154336269 ''Influence of the Milk Supply on the Spread of Tuberculosis.''] Alfredo Antunes Kanthack, E. Sydney St B. Sladen. Cambridge University Press, 1898

'''Publications'''
*''The Thyreo-Glossal Duct''. Kanthack AA. J Anat Physiol. 1891 Jan; 25(Pt 2):155-65
*''Apparently Successful Cultivation of the Bacillus Leprae''. Kanthack AA, Barclay A. Br Med J. 1891 Jun 6;1(1588):1222-3
*''Pure Cultivation of the Leprosy Bacillus''. Kanthack AA, Barclay A. Br Med J. 1891 Jun 20;1(1590):1330-1
*''A Case of Acromegaly''. Kanthack AA. Br Med J. 1891 Jul 25;2(1595):188-9
*''Cultivation of the Bacillus Leprae''. Kanthack AA. Br Med J. 1891 Aug 29;2(1600):476
*''Acute Leucocytosis Produced by Bacterial Products''. Kanthack AA. Br Med J. 1892 Jun 18;1(1642):1301-3
*''The Diagnostic Value of the Eosinophile Leucocytes in Leukaemia and Hodgkin's Disease''. Kanthack AA. Br Med J. 1892 Jul 16;2(1646):120-1
*''A Few Notes on Epithelial Pearls in Foetuses and Infants''. Kanthack AA. J Anat Physiol. 1892 Jul; 26(Pt 4):500.1-508
*''The Nature of Cobra Poison''.  Kanthack AA. J Physiol. 1892 May; 13(3-4):272-99
*''Myology of the Larynx''.  Kanthack AA. J Anat Physiol. 1892 Apr; 26(Pt 3):279-294.3
*''Complete Cervical Fistulae. A Note on C. F. Marshall's Paper on the Thyro-Glossal Duct or 'Canal of His'.''  Kanthack AA. J Anat Physiol. 1892 Jan; 26(Pt 2):197-8
*''The Action of the Epiglottis during Deglutition''.  Kanthack A A, Anderson HK. J Physiol. 1893 Mar;14(2-3):154-62
*''Report on Immunity Against 'Cholera.': An Experimental Inquiry into the Bearing on Immunity of Intracellular' and 'Metabolic' Bacterial Poisons''. Kanthack AA, Wesbrook FF. Br Med J. 1893 Sep 9;2(1706):572-5
*''The Morphology and Distribution of the Wandering Cells of Mammalia''. Kanthack AA, Hardy WB. J Physiol. 1894 Aug 13;17(1-2):80.1-119

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for
instructions on how to add citations. --->

{{Authority control}}

{{DEFAULTSORT:Kanthack, Alfredo}}
<!--- Categories --->
[[Category:Microbiologists]]
[[Category:1863 births]]
[[Category:Epidemiologists]]
[[Category:Pathologists]]
[[Category:1898 deaths]]
[[Category:Articles created via the Article Wizard]]