{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = The Amazing Jeckel Brothers
| Type        = studio
| Artist      = [[Insane Clown Posse]]
| Cover       = Jack Jeckel.jpg
| Caption     = Jack Jeckel edition.
| Released    = May 25, 1999
| Recorded    = 1998–1999
| Length      = 69:30
| Genre       = [[Midwest hip hop]], [[horrorcore]], [[rap metal]]
| Label       = [[Island Records]]<br />[[Psychopathic Records]]<br><small>524 661 (Jack)<br>524 659 (Jake)</small>
| Producer    = [[Mike E. Clark]]<br />ICP
| Last album  = [[The Great Milenko]] <br />(1997)
| This album  = '''The Amazing Jeckel Brothers'''<br />(1999)
| Next album  = [[Bizaar]]<br />(2000)
| Misc        =
{{Extra album cover
  | Upper caption  = ''Alternative Cover''
  | Type           = studio
  | Cover          = Jake Jeckel.gif
  | Lower caption  = Jake Jeckel edition.
  }}
}}
'''''The Amazing Jeckel Brothers''''' is the fifth [[studio album]] by American [[hip hop]] group [[Insane Clown Posse]], released on May 25, 1999, by [[Island Records]], in association with [[Psychopathic Records]]. Recording sessions for the album took place from 1998 to 1999. The album is the fifth [[Dark Carnival (Insane Clown Posse)#Joker's Cards|Joker's Card]] in the group's [[Dark Carnival (Insane Clown Posse)|Dark Carnival]] mythology. The album's lyrics focus on the 9 circles of hell, and the morality of man as he is torn between righteousness and evil. The album's titular Jeckel Brothers are spirits who juggle balls of fire, representing the sins committed during the mortal life of the dead.

''The Amazing Jeckel Brothers'' was the second studio album Insane Clown Posse released by Island, and features a more [[hip hop music|hip hop]]-based sound, as opposed to the [[rock music|rock]]-oriented sound of its predecessor, ''[[The Great Milenko]]'' (1997). ''The Amazing Jeckel Brothers'' features guest appearances by rappers [[Ol' Dirty Bastard]] and [[Snoop Dogg]], and additional contributions by [[The Jerky Boys]] and [[Twiztid]]. It debuted at number four on the ''[[Billboard (magazine)|Billboard]]'' charts and was later [[Music recording sales certification|certified platinum]] by the [[Recording Industry Association of America]] (RIAA). It is the group's 12th overall release.

==Background==
After a tumultuous contract with [[Jive Records]] sub-label Battery in 1995, Insane Clown Posse (ICP) attempted to find a new record label. Manager Alex Abbiss negotiated a contract with the [[The Walt Disney Company|Walt Disney Company]]-owned label [[Hollywood Records]], which reportedly paid US$1&nbsp;million to purchase the Insane Clown Posse contract from Battery/Jive Records.<ref>{{cite news |url=http://www.mtv.com/news/articles/1430332/19970703/i_c_p.jhtml |title=Insane Clowns Point The Finger At Disney |accessdate=2008-05-04 |date=July 3, 1997 |publisher=[[MTV]]}}</ref>

After recording and releasing ''[[The Great Milenko]]'', Insane Clown Posse was notified that Hollywood Records had deleted the album within hours of its release,<ref name="BehindthePaint306"/> despite having sold 18,000 copies and reaching #63 on the [[Billboard 200|''Billboard'' 200]].<ref name="Browne">{{cite web |url=http://www.ew.com/ew/article/0,,288784,00.html |title=Review of ''The Great Milenko'' |accessdate=2008-05-08 |last=Browne |first=David |date=July 25, 1997 |publisher=''[[Entertainment Weekly]]''}}</ref><ref>{{cite news |url=http://www.mtv.com/news/articles/1430331/19970704/i_c_p.jhtml |title=Insane Clown Posse Angry At Disney's Decision |accessdate=2008-05-04 |date=July 4, 1997 |publisher=[[MTV]]}}</ref> It was later revealed that Disney was being criticized by the [[Southern Baptist Convention|Southern Baptist Church]]. The church claimed Disney was turning its back on [[family values]].<ref name="Disney-controversy">{{cite news |url=http://www.mtv.com/news/articles/1430334/19970627/i_c_p.jhtml |title=Insane Clown Posse Album Recalled |accessdate=2008-05-04 |date=June 27, 1997 |publisher=[[MTV]]}}</ref>

In due time, labels such as [[Interscope Records]] wanted to sign the group,<ref name="Disney-controversy"/> but [[Island Records]]' [[Chris Blackwell]] came to the group's rescue and agreed to release ''The Great Milenko'' as it was originally intended.<ref name="BehindthePaint330">[[#behind|Bruce (2003)]], p. 330–335.</ref> Thanks to the controversy, and additional promotion by Island, over one million copies of ''The Great Milenko'' had been sold by 1998,<ref name="BehindthePaint414">[[#behind|Bruce (2003)]], p. 414–433.</ref> and Insane Clown Posse was ready for the fifth Joker's Card, ''The Amazing Jeckel Brothers''.

==Recording and production==
Working with [[Mike E. Clark]] and Rich "[[Legz Diamond]]" Murrell, [[Violent J|Joseph Bruce]] and [[Shaggy 2 Dope|Joseph Utsler]] developed their album with the highest of hopes. Hoping to receive the respect Bruce and Utsler felt they deserved, they planned to feature well-known, respected rappers on their album.<ref name="BehindthePaint414"/> Bruce stated outright that he wanted to involve [[Snoop Dogg]], [[Ol' Dirty Bastard]], and [[Ice-T]].<ref name="Rappers">{{Cite web | title = Insane Clown Posse Taps O.D.B., Snoop, Ice-T For New Album, Wave Goodbye to WWF
  | publisher = [[MTV]]  | date = December 10, 1998|url = http://www.mtv.com/news/articles/1429456/19981210/ghostface_killah.jhtml |accessdate=July 12, 2015}}</ref> Snoop Dogg requested that Insane Clown Posse not pay his then-current record label, [[No Limit Records]], and said that he would appear on the album if Bruce and Utsler gave him [[United States dollar|$]]40,000 in a briefcase.<ref name=AVClub>{{Cite web | title = Set List: Violent J of Insane Clown Posse |last=Rabin |first=Nathan|url=http://www.avclub.com/articles/violent-j-of-insane-clown-posse,60282/|publisher=[[The A.V. Club]]|date=August 12, 2011|accessdate=April 7, 2011}}</ref> Insane Clown Posse agreed, and Snoop Dogg appeared on the song "The Shaggy Show", which also featured the [[ska]] band Gangster Fun playing music before each of the song's faux commercial breaks.<ref name=AVClub/> Insane Clown Posse also unsuccessfully attempted to contact [[Ice Cube]] to collaborate with them.<ref>http://www.detroitnews.com/article/20120517/ENT09/205170387</ref>

Snoop Dogg also helped them contact Ol' Dirty Bastard, who was paid $30,000 for his appearance. Ol' Dirty Bastard recorded his track in a matter of two days; however, his recording consisted of nothing more than him rambling about "bitches."<ref name="BehindthePaint414"/><ref name=AVClub/> It took Bruce and Utsler a week to assemble just four rhymes out of his rambling, using [[Pro Tools]] because his raps were out of synch with Clark's beat.<ref name="BehindthePaint414"/><ref name=AVClub/> The duo eventually had to re-record their lines and re-title the song "Bitches".<ref name=AVClub/> Finally, Insane Clown Posse contacted [[Ice-T]].<ref name="Rappers"/> However, he charged them only $10,000.<ref name="BehindthePaint414"/> The group felt that Ice-T's song did not belong on the album, and was instead released on the compilation, ''[[Psychopathics from Outer Space]]'' (2000).<ref name="BehindthePaint414"/> The song "Echo Side" was originally released at an Insane Clown Posse concert in [[Garden City, Michigan]] as the first ever single from [[Dark Lotus]].<ref name="BehindthePaint560">[[#behind|Bruce (2003)]], p. 560–561.</ref>

To help increase their positive publicity, Island Records hired the Nasty Little Man publicity team.<ref name="BehindthePaint414"/><ref name=AVClub/> The team set up a photo shoot for Insane Clown Posse that was to appear on the cover of ''[[Alternative Press (music magazine)|Alternative Press]]'' magazine in [[Cleveland]]. On the set of the photo shoot, a member of the publicity team approached Bruce and explained that in the song "Fuck the World", the lyric that stated "Fuck the [[Beastie Boys]] and the [[Dalai Lama]]" needed to be changed.<ref name="BehindthePaint414"/> Insulted, Bruce exclaimed that his music would not be censored again – referring to [[Walt Disney Company|Disney's]] previous requirement of censure.<ref name="BehindthePaint414"/> Nasty Little Man told Bruce that the Beastie Boys were not only clients of the company but also personal friends, and the Beastie Boys told the company to make Bruce change the lyric.<ref name="BehindthePaint414"/> In response, Bruce fired Nasty Little Man and asked its team to leave the photo shoot.<ref name="BehindthePaint414"/>

==Musical style==
{| style="font-size:80%" align="right" border="0" color="black" width=130px
| {{Listen|filename=Jake Jeckel.ogg|title="Jake Jeckel" (sample)|description="Jake Jeckel", from the group's 1999 album ''The Amazing Jeckel Brothers''.|format=[[Ogg]]}}
|}
While the album's predecessor, ''The Great Milenko'', was written and recorded in a more [[rock music|rock]]-oriented style, featuring contributions by guitarists [[Slash (musician)|Slash]] and [[Steve Jones (musician)|Steve Jones]], ''The Amazing Jeckel Brothers'' featured a more [[hip hop music|hip hop]]-oriented sound. [[Stephen Thomas Erlewine]] of [[Allmusic]] wrote, "Where ''The Great Milenko'' [...] was targeted at white-boy, adolescent metalheads [...]  ''The Amazing Jeckel Brothers'' contains cameos from Snoop Dogg and Ol' Dirty Bastard, plus a cover of a Geto Boys song, which brings [Insane Clown Posse] to street level."<ref name="Erlewine"/>

To produce the album, Insane Clown Posse once again teamed up with renowned Detroit [[record producer]] and [[disc jockey|DJ]] [[Mike E. Clark]], who utilized standard hip hop techniques such as [[record scratching]] and [[Sampling (music)|samples]] ranging from 1970s [[funk]] to [[Calliope (music)|calliope]] music.<ref name="Maher">{{Cite web  | last = Maher   | first = Brendan  | title = Insane Clown Posse, The Amazing Jeckel Brothers  | publisher = PopMatters  | url = http://www.popmatters.com/pm/review/19728/insaneclownposse-amazing  |accessdate=10 May 2010}}</ref> "Another Love Song" was based upon [[Beck]]'s song "[[Jack-Ass (song)|Jack-Ass]]", which itself was derived from a sample of [[Bob Dylan]]'s "[[It's All Over Now, Baby Blue]]". Bruce loved the song and wanted to rewrite it in his own style. Although the group "lifted the riff from Beck", since Beck's song sampled the Dylan composition, Insane Clown Posse's sample was cleared with Dylan rather than Beck.<ref name=AVClub/> ''[[Rolling Stone]]'' writer Barry Walters wrote that Clark's production incorporates elements of "carnival organ riffs, [[power chord]]s and shotgun blasts&nbsp;... banjolike plucking and [[Van Halen]]-esque guitar squeals."<ref name="RSJeckelBros">{{cite web |first=Barry |last=Walters |title=Insane Clown Posse: The Amazing Jeckel Brothers: Music Reviews | url = http://www.rollingstone.com/music/albumreviews/the-amazing-jeckel-brothers-19990610 |accessdate=2012-03-23 |date=1999-06-10 |publisher=''[[Rolling Stone]]''}}</ref>

==Lyrical themes==
{{Quote box
 | quote  =Emerging from the Dark Carnival like phantom smoke drifting into the minds of men, they are the Amazing Jeckel Brothers. A chaotic duo of juggling masters, Jack "the sinister" and Jake "the just" juggle the sins of mortal men... There is no escape from their juggling act because there is no way to escape ourselves. Only in death will we recognize this as we twist and spin to the other side.
 | source = Liner notes<ref name="Liner"/>
 | width  =30%
 | align  =right
}}
During the two years between ''The Great Milenko'' and ''The Amazing Jeckel Brothers'', Insane Clown Posse had become nationally known, but were not taken very seriously.<ref name="BehindthePaint414"/> While the controversy over ''The Great Milenko'' allowed the duo to attract the attention of [[Island Records]], it also attracted Insane Clown Posse to public criticism for their style and lyrics.<ref name="BehindthePaint306">[[#behind|Bruce (2003)]], p. 306–314.</ref> Bruce recalls the period as an angry era for the group due to all of the negativity directed toward them.<ref name="BehindthePaint414"/> He says that they "used to keep two piles of press at [their] office. One pile was all the positive press [they've] gotten, which was under an inch tall. Then [they] had the negative press pile, which was spilling over the side of a full basket."<ref name="BehindthePaint414"/> As a result, ''The Amazing Jeckel Brothers'' was recorded as a release for their anger.<ref name="BehindthePaint414"/>

''The Amazing Jeckel Brothers'' focuses on the 9 circles of [[hell]], and the [[morality]] of man as he is torn between [[righteousness]] and [[evil]].<ref name="Erlewine">{{cite web |url={{Allmusic|class=album|id=r415252/review|pure_url=yes}} |title=allmusic ((( The Great Milenko > Overview ))) |accessdate=2008-05-08 |last=Erlewine |first=Stephen Thomas |publisher=[[Allmusic]]}}</ref> Jack "the sinister" and Jake "the just" (bad and good) emerge from the flame of a candle to determine the fate of the dead.<ref name="Liner">{{cite web | title=The Amazing Jeckel Brothers LP | url=http://www.insaneclownposse.com/music | publisher=[[Psychopathic Records]] | accessdate=2009-05-22 }}</ref> The Jeckel Brothers [[Juggling|juggle]] fire balls.<ref name="Liner"/> For every [[sin]] committed during the mortal life of the dead, another ball is added.<ref name="Liner"/> Jack attempts to throw Jake curves in an attempt to see a ball drop.<ref name="Liner"/> If a soul witnesses Jake drop one of the balls, it will be damned to hell. Souls who see Jake successfully complete the act ascend to [[heaven]].<ref name="Liner"/>

==Release and reception==
{{Album reviews
| rev1 = [[Allmusic]] 
|rev1Score = {{Rating|4|5}}<ref name="Erlewine"/>
| rev2 = [[Robert Christgau]] 
|rev2Score = C+<ref name="Christgau">{{cite book |last1=Christgau |first1=Robert |authorlink1=Robert Christgau |title=Christgau's consumer guide: albums of the 90's |year=2000 |publisher=Macmillan |isbn=0-312-24560-2 |page=144 |chapter=I }}</ref>
| rev3 = ''[[College Music Journal]]''
| rev3Score = (unfavorable)<ref name="Ashare">{{cite journal |last1=Ashare |first1=Matt |title=Review of ''The Amazing Jeckel Brothers'' |journal=[[College Music Journal]] |issue=71 |page=51 |issn=1074-6978 |date=July 1999 }}</ref>
| rev4 = ''[[NME]]''
|rev4Score = {{Rating|3|10}}<ref name="NME">{{Cite journal|title=The Amazing Jeckel Brothers |journal=[[NME]] |url=http://www.nme.com/reviews/insane-clown-posse/1015 |date=April 20, 1999 |accessdate=10 May 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20091130013723/http://www.nme.com/reviews/insane-clown-posse/1015 |archivedate=November 30, 2009 }}</ref>
| rev5 = [[PopMatters]] 
|rev5Score = {{Rating|3|10}}<ref name="Maher"/>
| rev6 = ''[[Rolling Stone]]''
|rev6Score = {{Rating|2|5}}<ref name="RSJeckelBros"/>
| rev7 = [[Martin Charles Strong]]
| rev7Score = {{rating|4|10}}<ref name="Strong">{{cite book |last1=Strong |first1=Martin Charles  |title=The Great Rock Discography |edition=7th |year=2004 |publisher=Canongate |isbn=1-84195-615-5 |page=733 |chapter=Insane Clown Posse }}</ref>
}}<!--List Automatically Moved by DASHBot-->
''The Amazing Jeckel Brothers'' debuted and peaked at #4 on the [[Billboard 200|''Billboard'' 200]].<ref>{{cite news |url=http://www.mtv.com/news/articles/1430318/19990603/i_c_p.jhtml |title=Insane Clown Posse Makes "Amazing" Top Five Debut |accessdate=2008-05-04 |date=June 3, 1999 |publisher=[[MTV]]}}</ref><ref name="Billboard">{{cite web |url={{Allmusic|class=album|id=r415252/charts-awards|pure_url=yes}} | title=Insane Clown Posse Artist Chart History: Albums |publisher=''[[Billboard (magazine)|Billboard]]''|accessdate=2008-08-19}}</ref> In order to promote the album, Island released multiple collectible versions of ''The Amazing Jeckel Brothers'', emphasizing the faces of Jake or Jack Jeckel.<ref name="RSAlbumGuide">[[#rolling|Brackett (2004)]], pp. 405–6.</ref> In 2008, it achieved [[Music recording sales certification|platinum certification]].<ref name="RIAA">{{cite web |url=http://riaa.com/goldandplatinumdata.php?resultpage=1&table=SEARCH_RESULTS&action=&title=&artist=Insane%20Clown%20Posse&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2009&sort=Artist&perPage=25 |title=Certification for Insane Clown Posse |accessdate=2008-05-05 |publisher=RIAA Gold and Platinum Certification Database}}</ref>

The album received mostly negative reviews from critics. ''[[NME]]'' wrote that "the slick, dumbed-down ''[[Dungeons & Dragons]]'' [[rap rock|rap-rock]] schtick [...] is often unbearable".<ref name="NME"/> ''[[College Music Journal]]'' writer Matt Ashare described the album as "[[Cirque du Soleil|Cirque de so-lame]]".<ref name="Ashare"/> ''[[Rolling Stone]]'' writer Barry Walters gave the album two out of five stars, writing that "no musical sleight of hand can disguise the fact that Shaggy and J remain the ultimate wack MCs."<ref name="RSJeckelBros"/> In ''The Great Rock Discography'', [[Martin Charles Strong]] gave the album four out of ten stars.<ref name="Strong"/>

[[PopMatters]] reviewer Brendan Maher accused Insane Clown Posse of misogyny and described ''The Amazing Jeckel Brothers'' as "music to strangle your ex-girlfriend to".<ref name="Maher"/> [[Robert Christgau]] gave the album a C+, writing "Though they claim clown, they rarely get funnier than 'I'd cut my head off but then I would be dead'."<ref name="Christgau"/> However, Stephen Thomas Erlewine of [[Allmusic]] gave the album a four out of five star rating, writing that "[Insane Clown Posse] actually delivered an album that comes close to fulfilling whatever promise their ridiculous, [[carnival]]esque blend of [[hardcore hip hop]] and [[Shock rock|shock-metal]] had in the first place".<ref name="Erlewine"/>

==Legacy==
On January 20, 2017 it was announced that the ''2017 Juggalo Day Show'' will now feature [[Insane Clown Posse]] performing ''The Amazing Jeckel Brothers'' in it's entirety on Day 2 (February 17, 2017).

==Track listing==
{{tracklist
| total_length    = 69:30
| writing_credits = yes
| title1          = Intro
| length1         = 1:19
| writer1         = [[Mike E. Clark]] and [[Insane Clown Posse|ICP]]
| title2          = Jake Jeckel
| length2         = 1:26
| writer2         = Mike E. Clark and ICP
| title3          = Bring It On
| length3         = 4:28
| writer3         = Mike E. Clark and ICP
| title4          = I Want My Shit
| length4         = 5:20
| writer4         = Mike E. Clark and ICP
| title5          = Bitches
| length5         = 4:33
| writer5         = ICP and Ol' Dirty Bastard
| note5           = featuring [[Ol' Dirty Bastard]] and [[The Jerky Boys]]
| title6          = Terrible
| length6         = 4:21
| writer6         = Mike E. Clark and ICP
| title7          = I Stab People
| length7         = 1:40
| writer7         = Mike E. Clark and ICP
| title8          = [[Another Love Song (Insane Clown Posse song)|Another Love Song]]
| length8         = 4:09
| writer8         = Mike E. Clark and ICP
| title9          = Everybody Rize
| length9         = 3:21
| writer9         = Mike E. Clark and ICP
| title10         = Play with Me
| length10        = 4:19
| writer10        = Mike E. Clark and ICP
| title11         = Jack Jeckel
| length11        = 1:25
| writer11        = Mike E. Clark and ICP
| title12         = [[Fuck the World (Insane Clown Posse song)|Fuck the World]]
| length12        = 3:44
| writer12        = Mike E. Clark and ICP
| title13         = The Shaggy Show
| length13        = 6:32
| writer13        = ICP, Mike E. Clark, Snoop Dogg and Gangster Fun
| note13          = featuring [[Snoop Dogg]] 
| title14         = Mad Professor
| length14        = 5:49
| writer14        = Mike E. Clark and ICP
| title15         = Assassins
| length15        = 5:15
| writer15        = Geto Boys and ICP
| note15          = featuring [[The Jerky Boys]]) ([[Geto Boys]] cover
| title16         = Echo Side
| length16        = 5:39
| writer16        = Mike E. Clark and ICP
| note16          = featuring [[Twiztid]]
| title17         = Nothing's Left
| length17        = 6:10
| writer17        = Mike E. Clark and ICP
}}

==Personnel==
{{col-begin}}
{{col-break}}
;Band members and production<ref name="Liner"/>
*[[Violent J]]&nbsp;– [[singing|vocals]], lyrics
*[[Shaggy 2 Dope]]&nbsp;– vocals, lyrics
*[[Mike E. Clark]]&nbsp;– [[Record producer|production]], [[Programming (music)|programmer]], [[Audio engineering|engineer]]
*Rich "[[Legz Diamond]]" Murrell&nbsp;– guitar, vocals
{{col-break}}
;Other personnel<ref name="Liner"/>
*[[Snoop Dogg]] — featured on "The Shaggy Show"
*[[Ol' Dirty Bastard]] — featured on "Bitches"
*[[The Jerky Boys]] — featured on "Bitches" and "Assassins"
*[[Twiztid]] — featured on "Echo Side"
*[[Gangster Fun]] — featured on "The Shaggy Show"
{{col-end}}

== Charts and certifications ==
{{col-begin}}
{{col-2}}

=== Charts ===
{| class="wikitable sortable"
|-
!Chart
!Peak<br />Position
|-
|[[Billboard 200|US ''Billboard'' 200]]<ref name="Billboard"/>
| style="text-align:center;"|4
|-
|Top Internet Albums<ref name="Billboard"/>
| style="text-align:center;"|5
|}
{{col-2}}

=== Certifications ===
{| class="wikitable sortable"
|-
!Country
![[Music recording sales certifications|Certification]]<br /><small>([[List of music recording sales certifications|sales thresholds]])</small>
|-
|[[Recording Industry Association of America|United States]]
| style="text-align:center;"|Platinum<ref name="RIAA"/>
|}
{{col-end}}

==Notes==
{{refbegin}}
*{{citation
| ref=rolling
|last=Brackett
|first=Nathan
|title=The New Rolling Stone Album Guide
|publisher=Simon and Schuster
|year=2004
|isbn=0-7432-0169-8
}}
*{{citation
| ref=behind
|last=Bruce
|first=Joseph
|title=ICP: Behind the Paint
|publisher=Psychopathic Records
|year=2003
|isbn=0-9741846-0-8
}}
{{refend}}

==References==
{{Reflist|2}}

{{Insane Clown Posse}}

{{good article}}

{{DEFAULTSORT:Amazing Jeckel Brothers, The}}
[[Category:1999 albums]]
[[Category:Albums produced by Mike E. Clark]]
[[Category:Concept albums]]
[[Category:Island Records albums]]
[[Category:Insane Clown Posse albums]]
[[Category:Psychopathic Records albums]]
[[Category:Horrorcore albums]]