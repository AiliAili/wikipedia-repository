'''The American Society for Bone and Mineral Research''' ('''ASBMR''') is a professional, scientific and medical society established in 1977 to promote excellence in bone and mineral research and to facilitate the translation of that research into clinical practice. The ASBMR has a membership of nearly 4,000 physicians, basic research scientists, and clinical investigators from around the world.

==Mission==
The mission of the ASBMR is to promote excellence in bone and mineral research, foster integration of clinical and basic science, and facilitate the translation of that science to health care and clinical practice. The Society’s broad goals include supporting the educational development of future generations of basic and clinical scientists, and disseminating new knowledge in bone and mineral metabolism.

==Founding==
In the 1970s, a growing number of US-based scientists began to focus their research on the understanding of basic bone biology and the disease [[osteoporosis]]. This led to the rise of a new field – bone and mineral research. In 1974, while attending the annual meeting of [[The Endocrine Society]] in Chicago, Illinois, USA, bone scientists Louis Avioli, Claude Arnaud, Norman Bell, William Peck, John Potts and Lawrence Raisz, along with Shirley Hohl, met at the [[Drake Hotel (Chicago)|Drake Hotel]]. The group laid the groundwork for an organization that would promote the study of bone and mineral research, support scientists involved in such research, and facilitate the discussion and exchange of new developments in the field. Three years later, in November 1977, the group’s goals were realized with the official incorporation of the ASBMR as a nonprofit organization in St. Louis, Missouri, USA. The first ASBMR Annual Meeting was held June 11–12, 1979 at the Disneyland Hotel in Anaheim, California, USA with approximately 150 people in attendance.

==Growth==
The ASBMR grew modestly in its first ten years of existence. Its primary activity in the early years was convening an annual bone and mineral research scientific meeting in conjunction with The Endocrine Society meeting. As interest in the bone and mineral field expanded, however, so too did the Society’s activities and scope.

In 1984, the ASBMR leadership recognized that a need existed for a patient-focused organization whose mission it would be to address patient and public education on the disease of osteoporosis and to promote lifelong bone health. That same year, ASBMR leaders established The Osteoporosis Foundation, which was later renamed the [http://www.nof.org/ National Osteoporosis Foundation].

[http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291523-4681 The Journal of Bone and Mineral Research (JBMR®)], the official journal of the ASBMR, was established in 1986 initially as a bi-monthly publication, but transitioned to a monthly journal in 1990. The publication of the JBMR® expanded the Society’s ability to disseminate cutting-edge research on bone and mineral metabolism year-round and helped to broaden the field’s audience to researchers and clinicians outside of North America. Lawrence G. Raisz served as the first Editor-in-Chief of the JBMR®.

In the late 1980s, the most cutting-edge scientific knowledge on bone and diseases of bone was largely contained within the field by members of the Society. A need was recognized to compile this knowledge into a text book that could be disseminated to introduce graduate and medical students, as well as fellows, to the field. In response, the ASBMR published the first edition of The Primer on the Metabolic Bone Diseases and Disorders of Mineral Metabolism in 1990. Murray J. Favus served as the Editor-in-Chief for the first six editions of the Primer.

The rise of new pharmacological treatments for osteoporosis in the 1990s, most notably the class of drugs known as bisphosphonates, gave rise to an influx of scientists and clinician-researchers into the field. This influx resulted in a dramatic increase in ASBMR membership and annual meeting attendance. During this period, the ASBMR expanded its advocacy endeavors targeted at U.S. government funding for bone research. The Society was a founding member of the National Coalition for Osteoporosis and Related Bone Diseases (“Bone Coalition”) in 1991. ASBMR also became a member of the [http://www.faseb.org/#sthash.5mAEW5A7.dpbs Federation of American Societies for Experimental Biology (FASEB)] which represents nearly 100,000 scientists and plays a key role in formulating U.S. science policy.

Though conceived as a scientific research society, in recent years, ASBMR has made increasing public and healthcare professional awareness of bone diseases a top priority. The Society launched several educational initiatives aimed at primary care physicians to improve the detection and treatment of bone diseases, and founded the [http://www.nbha.org/ National Bone Health Alliance] to serve as a resource and raise public awareness of bone diseases. It has also spearheaded task forces on numerous clinically relevant topics, including: osteonecrosis of the jaw and atypical femoral fractures. The Society has also sought to expand the study of bone to those in related fields and to those in emerging areas of the world.

==Education==

=== Annual Meetings ===

The ASBMR Annual Meeting brings together leading basic, translational and clinical researchers in bone from around the world. The event is held in September or October and attracts nearly 4,000 attendees each year. The scientific program includes poster presentations, plenary lectures, workshops, networking events, ancillary meetings, and a host of other activities. Hallmarks of the ASBMR Annual Meeting include the Gerald D. Aurbach Lecture, the Louis V. Avioli Lecture, and the ASBMR/ECTS Clinical Debate. Abstracts from the meeting’s poster presentations are published as supplements in the JBMR®. A list of past ASBMR Annual Meetings, as well as photos, program and abstracts books and webcasts, can be found on the [http://www.asbmr.org/Meetings/PastAnnualMeetings.aspx ASBMR website].

=== Topical Meetings ===

The ASBMR began holding topical meetings in 2002 to address specialized research topics within the bone field. Smaller in scale, topical meetings disseminate and discuss in-depth research on a specific area of scientific interest. A list of past ASBMR Topical Meetings, as well as photos, program and abstracts books and webcasts, can be found on the [http://www.asbmr.org/TopicalMeetings/PastTopicalMeetings.aspx ASBMR website].

==Publications==

=== ''Journal of Bone and Mineral Research''® ===

The [http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291523-4681 ''Journal of Bone and Mineral Research®''], the official journal of the ASBMR, is the highest-ranked journal in the field, with an impact factor of 6.128 in 2012. The ''JBMR®'' publishes original manuscripts, reviews, and special articles in basic and clinical science relevant to [[bone]], muscle and mineral metabolism. Manuscripts are published on the biology and physiology of bone and muscle, relevant systems biology topics (e.g. [[osteoimmunology]]), and the [[pathophysiology]] and treatment of [[osteoporosis]], [[sarcopenia]] and other disorders of bone and mineral metabolism.

JBMR® readers include basic scientists and physicians specializing in [[endocrinology]], [[physiology]], [[cell biology]], [[pathology]], [[molecular genetics]], [[epidemiology]], [[internal medicine]], [[rheumatology]], [[orthopaedics]], [[geriatrics]], [[dentistry]], [[gynecology]], [[molecular biology]], [[nephrology]] and many other disciplines. The JBMR®’s international editorial board encourages manuscript submissions from around the world.

=== ''Primer on the Metabolic Bone Diseases and Disorders of Mineral Metabolism'' ===

The [http://www.wiley.com/WileyCDA/WileyTitle/productCd-0977888215.html ''Primer on the Metabolic Bone Diseases and Disorders of Mineral Metabolism''] is a resource for scientists and students seeking an overview of the bone and mineral field and for clinicians who see patients with disorders of bone and mineral metabolism. The text provides valuable information on the symptoms, pathophysiology, diagnosis, and treatment of metabolic bone diseases and both common and rare disorders. Authors include internationally renowned experts in the field.

=== ASBMR e-News Weekly ===

The ASBMR e-News Weekly is a member newsletter featuring society initiatives and related information on upcoming events, conferences, membership benefits, and other important information. The newsletter keeps members abreast of ASBMR activities—from Council, Committee, task force, and program updates to the role of ASBMR within the bone and mineral field and the scientific and medical community at large. Each issue also includes the most recently published articles in JBMR® and highlights current noteworthy news articles pertaining to the bone field from thousands of news sources worldwide.

==Grants & Awards==
The Society has established numerous grant and award programs since its inception aimed at supporting the career development of its members, as well as recognizing their scientific accomplishments and contributions to the field. More information on these [http://www.asbmr.org/Grants/Default.aspx%20 grants] and [http://www.asbmr.org/Meetings/ASBMRAWards.aspx awards] is available on the ASBMR website.

=== Esteemed Awards ===

ASBMR Esteemed Awards recognize the contributions of accomplished researchers to the field and to the Society.

'''The Fuller Albright Award''' is given in recognition of meritorious scientific accomplishment in the bone and mineral field to an ASBMR member who has not yet reached his or her 45th birthday.

'''The Louis V. Avioli Founders Award''' honors an ASBMR member for fundamental contributions to bone and mineral basic research.

'''The ASBMR Early Career Excellence in Teaching Award''' recognizes an individual in the early stage of his or her career who displays a strong commitment to teaching and learning in the classroom, the clinic and/or research laboratory setting.

'''The Frederic C. Bartter Award''' is given to an ASBMR member in recognition of outstanding clinical investigation in disorders of bone and mineral metabolism.

'''The Shirley Hohl Service Award''' recognizes an individual who has made significant contributions to the mission of the ASBMR.

'''The William F. Neuman Award''' is the ASBMR's oldest and most prestigious award. It recognizes an ASBMR member for outstanding and major scientific contributions in the area of bone and mineral research and for contributions to associates and trainees in teaching, research, and administration.

'''The Lawrence G. Raisz Award''' is given to an individual having a record of outstanding achievements in preclinical translational research in the bone and mineral field.

'''The Gideon A. Rodan Excellence in Mentorship Award''' is given in recognition of outstanding support provided by a senior scientist who has helped promote the independent careers of young investigators in bone and mineral metabolism.

'''The Paula Stern Achievement Award''' recognizes a woman in the bone field who has made significant scientific achievements and who has promoted the professional development/advancement of women in the field.

=== Annual Meeting Abstract-Based Awards ===

Each year, ASBMR gives numerous merit-based awards for research presented at its Annual Meeting.

'''ASBMR Felix Bronner Young Investigator Award''' is awarded to the highest ranking abstract submitted by a young investigator with a focus on bone. Award recipients receive a $1,000 honorarium and a plaque.

'''The ASBMR Most Outstanding Abstract Award''' (formerly the ASBMR Award for Outstanding Research in the Pathophysiology of Osteoporosis) is given to the lead investigators of the highest scored basic abstract and the highest scored clinical abstract submitted for presentation at the ASBMR Annual Meeting. The award includes a $1,500 honorarium and a plaque.

'''ASBMR Phoebe Leboy Professional Development Award''' provides recipients resources needed to attend the Annual Meeting, mentorship from senior women in related areas of research, and access to the professional and educational resources during the event to further develop her research career.

'''The ASBMR President's Award''' is given to the highest ranking abstract submitted by a student. The award includes a $1,500 honorarium and a plaque.

'''ASBMR Young Investigator Awards''' recognize young investigators who submit top-ranking abstracts to an ASBMR Meeting. Award recipients receive a $1,000 honorarium and a plaque.

=== ASBMR Grant Programs ===

Committed to supporting the academic potential and careers of scientists in the bone and mineral field, the ASBMR offers several grant programs. Given current funding challenges for researchers globally, the Society has put forward funding programs designed to support bone and mineral science researchers across all career stages.

'''ASBMR Career Enhancement Award''' strengthens and expands the basic, clinical and/or translational expertise of the awardee, enhances the applicant’s career development and fosters sustained collaborative research endeavors.

'''ASBMR Harold M. Frost Young Investigator Award''' provides travel support for young investigators to attend the annual Sun Valley Workshop on Musculoskeletal Biology.

'''ASBMR Junior Faculty Osteoporosis Research Award''' provides support to a scientist conducting clinical or basic research related to osteoporosis developing preliminary data to be used to compete for government, private foundation and/or industry research support.

'''ASBMR John Haddad Young Investigator Award''' facilitates the professional development of young basic and clinical scientists in the field of bone and mineral metabolism by providing recipients with financial support to attend the Annual  Advances in Mineral Metabolism (AIMM) Meeting.

'''ASBMR Mentored Career Development Award''' offers funding for young investigators whose mentored career development applications scored in the outstanding range, but did not receive funding.

'''ASBMR Raisz-Drezner Journal of Bone and Mineral Research (JBMR®) First Paper Award''' honors a first author of a meritorious scientific publication published in the JBMR®. To qualify for consideration, a paper must be the first scientific publication in which the first author appears as first author.

'''ASBMR Young Investigator Awards for PhD Training''' provides travel support for ASBMR members to participate in an annual Ph.D. Training Course in Europe of the European Calcified Tissue Society.

'''ASBMR Young Investigator Travel Awards to Attend International Conference on Osteoimmunology''' provides young scientists travel support to attend the bi-annual International Conference on Osteoimmunology.

==External links==
*[http://www.asbmr.org ASBMR Website]
*[http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291523-4681 Journal Website]
*[http://ifmrs.org/Default.aspx International Federation of Musculoskeletal Research Societies]
*[[Australian and New Zealand Bone and Mineral Society|Australian and New Zealand Bone and Mineral Society (ANZBMS)]]
*[http://www.ectsoc.org/ European Calcified Tissue Society] 
*[http://www.sibomm.net/ Ibero American Society of Osteology and Mineral Metabolism (SIBOMM)]
*[[International Bone and Mineral Society|International Bone and Mineral Society (IBMS)]]
*[http://www.icmrs.net/ International Chinese Musculoskeletal Research Society (ICMRS)]
*[[Orthopaedic Research Society|Orthopaedic Research Society (ORS)]]
*[http://jsbmr.umin.jp/ Japanese Society for Bone and Mineral Research (JSBMR)]

{{DEFAULTSORT:American Society For Bone And Mineral Research}}
[[Category:Non-profit organizations based in Washington, D.C.]]
[[Category:Chemistry societies]]
[[Category:Biology organizations]]
[[Category:Osteology]]