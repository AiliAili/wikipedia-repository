{{Infobox journal
| title = Asian Journal of Women's Studies
| cover = 
| discipline = [[Women's studies]]
| abbreviation = Asian J. Women Stud.
| editor = Pilwha Chang
| publisher = [[Ewha Womans University Press]]
| country = [[South Korea]]
| frequency = Quarterly
| history = 1995–present
| impact = 0.214
| impact-year = 2015
| website = http://home.ewha.ac.kr/~acws/eng/
| link1 = http://home.ewha.ac.kr/~acws/eng/
| link1-name = Online archive
| link2 = http://www.tandfonline.com/loi/rajw20
| link2-name = Taylor and Francis (volume 21 onwards)
| ISSN = 1225-9276
| eISSN = 2377-004X
| OCLC = 60630333
| LCCN = 99110935
| CODEN = 
}}
'''''Asian Journal of Women's Studies''''' is a quarterly [[peer-reviewed]] [[academic journal]] published by [[Ewha Womans University Press]]. Its articles have a theoretical focus, and its country reports provide information on specific subjects and countries. The journal also publishes research notes and [[book review]]s containing information on recent publications on women in Asia and elsewhere.<ref name=about>{{cite web |title=About |work=Asian Journal of Women's Studies |url=http://home.ewha.ac.kr/~acws/eng/ |publisher=[[Ewha Womans University Press]] |accessdate=23 June 2015}}</ref> The [[editor-in-chief]] is Pilwha Chang ([[Emory University|The Halle Institute, Emory University]]).<ref name=about_2>{{cite web |title=About, editorial committee |work=Asian Journal of Women's Studies |url=http://home.ewha.ac.kr/~acws/eng/ |publisher=[[Ewha Womans University Press]] |accessdate= 23 June 2015}}</ref>

From January 2015 the journal began to also be published by [[Taylor and Francis]].<ref>{{Cite journal | last = Chang | first = Pilwha | title = Editor's note | journal = Asian Journal of Women's Studies | volume = 21 | issue = 1 | page = 1 | publisher = [[Ewha Womans University Press]] | date = January 2015 | url = http://www.tandfonline.com/doi/full/10.1080/12259276.2015.1030816 | ref = harv | postscript = . | doi=10.1080/12259276.2015.1030816}} doi: [http://www.tandfonline.com/doi/full/10.1080/12259276.2015.1030816 10.1080/12259276.2015.1030816].</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Social Sciences Citation Index]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[ProQuest|ProQuest databases]]

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.214, ranking it =37th out of 40 journals in the category "Women's Studies".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of women's studies journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://home.ewha.ac.kr/~acws/eng/}}

{{DEFAULTSORT:Asian Journal of Women's Studies}}
[[Category:Academic journals published by university presses]]
[[Category:Publications established in 1995]]
[[Category:Quarterly journals]]
[[Category:Women's studies journals]]
[[Category:English-language journals]]


{{gender-journal-stub}}
{{Womens-journal-stub}}