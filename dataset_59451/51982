{{Infobox journal
| title = The Journal of Neuroscience
| cover = [[File:Journal of Neuroscience.gif]]
| editor = Marina Picciotto
| discipline = [[Neuroscience]]
| abbreviation = J. Neurosci.
| publisher = [[Society for Neuroscience]]
| country = United States
| frequency = Weekly
| history = 1981–present
| openaccess =
| license =
| impact = 5.924
| impact-year = 2015
| website = http://www.jneurosci.org/
| link1 = http://www.jneurosci.org/content/current
| link1-name = Online access
| link2 = http://www.jneurosci.org/content
| link2-name = Online archive
| link3 = http://www.eigenfactor.org/
| link3-name = EigenFactor = 0.413
| JSTOR =
| OCLC = 476317794
| LCCN = 81640907
| CODEN = JNRSDS
| ISSN = 0270-6474
| eISSN = 1529-2401
}}'''''The Journal of Neuroscience''''' is a weekly [[peer-reviewed]] [[scientific journal]] published by the [[Society for Neuroscience]]. It covers [[empirical]] research on all aspects of [[neuroscience]]. Its [[editor-in-chief]] is Marina Picciotto ([[Yale University]]). According to the ''[[Journal Citation Reports]]'', the journal had a 2014 [[impact factor]] of 6.344 and an [[Eigenfactor]] of 0.413, which is almost twice as high as the next highest neuroscience journal.<ref name=WoS>{{cite book |year=2014 |chapter=The Journal of Neuroscience |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== History ==
The journal was established in 1981 and issues appeared monthly; as its popularity grew it switched to a biweekly schedule in 1996 and then to a weekly in July, 2003.

== Themes ==
=== Main themes ===
Articles appear within one of the following five sections of the journal:
* Cellular/Molecular
* Development/Plasticity/Repair 
* Systems/Circuits
* Behavioral/Cognitive
* Neurobiology of Disease
The journal has revised its sections over the years. In 2004, it added the Neurobiology of Disease section due to the growing number of papers on this subject. In January 2013, the journal split the section Behavioral/Systems/Cognitive into two sections, Systems/Circuits and Behavioral/Cognitive, in order to make the sections of the journal approximately the same in size.<ref>{{cite journal |last1=Maunsell |first1=John |year=2013 |title=New Journal Sections |journal=The Journal of Neuroscience |volume=33 |issue=1 |pages=1 |publisher= |doi= |url= |accessdate=5 January 2013}}</ref>

=== Features ===
In addition, some issues of the journal contain articles in the following sections:
* Brief Communications
* Journal Club (brief reviews of articles that appeared in the Journal; written by graduate students or postdoctoral fellows; first published in September, 2005)

== References ==
{{reflist}}

== External links ==
{{Portal|Neuroscience}}
* {{Official website|http://www.jneurosci.org/}}
*{{Official website|http://www.eigenfactor.org/}}

{{DEFAULTSORT:Journal Of Neuroscience, The}}
[[Category:Neuroscience journals]]
[[Category:Publications established in 1981]]
[[Category:Weekly journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]