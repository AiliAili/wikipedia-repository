{{redirect|Historical journal|other uses|Journal}}
{{Infobox journal
| title         = The Historical Journal
| cover         = [[File:The Historical Journal.jpg]]
| editor        = Clare Jackson, Julian Hoppit
| discipline    = [[History]]
| former_names  = The Cambridge Historical Journal
| abbreviation  = 
| publisher     = [[Cambridge University Press]]
| country       = United Kingdom
| frequency     = Quarterly
| history       = 1958–present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://journals.cambridge.org/his
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 301165981
| LCCN          = 62052664
| CODEN         = 
| ISSN          = 0018-246X
| eISSN         = 1469-5103
| boxwidth      = 
}}
'''''The Historical Journal''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Cambridge University Press]]. It publishes approximately thirty-five articles per year on all aspects of [[History of the British Isles|British]], [[History of Europe|European]], and [[World History|world history]] since the fifteenth century. In addition, each issue contains numerous review articles covering a wide range of historical literature. Contributing authors include historians of established academic reputation as well as younger scholars making a debut in the historical profession.

The journal was founded in 1923 as ''The Cambridge Historical Journal'' by [[Harold Temperley]].  It obtained its present title in 1958. Despite choosing to omit the Cambridge label from the latter date, it remained under the editorial leadership of the History Faculty at the [[University of Cambridge]], as it does to this day. Its current [[editor-in-chief|editors]] are Andrew Preston ([[Clare College, Cambridge]]) and  Phil Withington ([[University of Sheffield]]).

==See also==
* [[Historiography]]
* [[Historiography of the United Kingdom]]

== External links ==
* {{Official website|http://journals.cambridge.org/his}}

{{DEFAULTSORT:Historical Journal, The}}
[[Category:History journals]]
[[Category:Publications established in 1923]]
[[Category:English-language journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:Quarterly journals]]