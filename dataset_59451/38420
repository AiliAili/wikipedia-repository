{{Infobox Christian leader
| name = Boniface of Savoy
| archbishop_of = [[Archbishop of Canterbury]]
| image = Lambeth Palace London 240404.jpg
| imagesize = 300px
| alt =
| caption = The site of Lambeth Palace, where Boniface built as an archbishop
| appointed= 1 February 1241
| enthroned = 1249
| ended = 18 July 1270
| predecessor = [[Edmund of Abingdon]]
| successor = [[William Chillenden]]
| consecration = 15 January 1245
| consecrated_by=Pope [[Pope Innocent IV|Innocent IV]]
| death_date = 18 July 1270
| death_place = Savoy
| buried = [[Hautecombe Abbey]] in Savoy
| parents =[[Thomas I, Count of Savoy]]<br>[[Margaret of Geneva]]
| feast_day = 14 July
| venerated =
| saint_title =
| beatified_date = 1839
| beatified_place =
| beatified_by = Pope [[Pope Gregory XVI|Gregory XVI]]
| canonized_date =
| canonized_place =
| canonized_by =
| attributes =
| patronage =
| shrine =
| suppressed_date =
}}

'''Boniface of Savoy''' ([[circa|c.]] 1217&nbsp;– 18 July 1270) was a medieval [[Roman Catholic Diocese of Belley-Ars|Bishop of Belley]] in France and [[Archbishop of Canterbury]] in England. He was the son of [[Thomas, Count of Savoy]], and owed his initial ecclesiastical posts to his father. Other members of his family were also clergymen, and a brother succeeded his father as count. One niece was married to King [[Henry III of England]] and another was married to King [[Louis IX of France]]. It was Henry who secured Boniface's election as Archbishop, and throughout his tenure of that office he spent much time on the continent. He clashed with his bishops, with his nephew-by-marriage, and with the papacy, but managed to eliminate the archiepiscopal debt which he had inherited on taking office. During [[Simon de Montfort, 6th Earl of Leicester|Simon de Montfort]]'s struggle with King Henry, Boniface initially helped Montfort's cause, but later supported the king. After his death in [[Savoy]], his tomb became the object of a cult, and he was eventually beatified in 1839.

==Early life==
Boniface and his elder brother [[Amadeus IV, Count of Savoy]], were sons of [[Thomas I, Count of Savoy]], and [[Margaret of Geneva]]. He is thus not to be confused with his nephew, and fellow member of the [[House of Savoy]], Count [[Boniface, Count of Savoy|Boniface of Savoy]], the son of Amadeus IV. The elder Boniface was born about 1207 in Savoy.<ref name=DNB/> He was the eleventh child of his parents.<ref name=Gibbs189>Gibbs ''Bishops and Reform'' p. 189</ref> Some sources state that at a young age he joined the [[Carthusian|Carthusian Order]].<ref name=Walsh104>Walsh ''New Dictionary of Saints'' pp. 104–105</ref> However, there is no evidence of this, and it would have been very unusual for a nobleman to enter that order with its very strict discipline.<ref name=Moorman162>Moorman ''Church Life'' p. 162</ref> He also had a brother [[Peter II, Count of Savoy|Peter of Savoy]] who was named [[Earl of Richmond]] in 1240 and yet another brother [[William of Savoy]], who was [[Roman Catholic Diocese of Valence|Bishop of Valence]] and a candidate to be [[Bishop of Winchester]] in England.<ref name=DNB>Knowles "Savoy, Boniface of" ''Oxford Dictionary of National Biography''</ref><ref name=Edward21>Prestwich ''Edward I'' p. 21</ref>

==Ecclesiastical career==
[[File:BNMsFr2829Fol18Henry3LandsAquit.jpg|thumb|left|250px|Henry III of England landing in Aquitaine. Boniface was the uncle by marriage of the king.]]
Boniface was the [[Prior]] of [[Nantua]] in 1232 along with the bishopric of [[Belley]] in [[Burgundy (region)|Burgundy]]. When his father died, he received the castle of Ugine as his inheritance, and he surrendered any entitlement to any other inheritance in 1238. After the marriage of his niece, [[Eleanor of Provence]] to King Henry III of England, Henry attempted to have Boniface elected Bishop of Winchester, but was unable to get the cathedral chapter to elect Boniface.<ref name=DNB/> On 1 February 1241 he was nominated to the [[Archbishop of Canterbury|see of Canterbury]].<ref name=Handbook233/> Pope [[Pope Innocent IV|Innocent IV]] confirmed the appointment on 16 September 1243, as an attempt to placate Henry.  Boniface did not, however, come to England until 1244 and was present, in the following year 1245, at the [[First Council of Lyon]].<ref name=DNB/> There, he was consecrated by Innocent IV on 15 January<ref name=Handbook233>Fryde, et al. ''Handbook of British Chronology'' p. 233</ref> at Lyons, but it was only in 1249 that he returned to England and was enthroned at [[Canterbury Cathedral]] on 1 November 1249.<ref name=BHOCant>Greenway ''[http://british-history.ac.uk/report.aspx?compid=33853 Fasti Ecclesiae Anglicanae 1066-1300: Volume 2: Monastic Cathedrals (Northern and Southern Provinces): Canterbury: Archbishops]''</ref> Before he returned in 1249, he helped arrange the marriage another of his nieces, [[Beatrice of Provence]], the sister of Queen Eleanor, to [[Charles I of Naples|Charles of Anjou]], the brother of King Louis IX of France.<ref name=DNB/>

The medieval chronicler [[Matthew Paris]] said that Boniface was "noted more for his birth than for his brains."<ref name=Moorman159>Quoted in Moorman ''Church Life'' pp. 159–160</ref> He showed little concern for the spiritual duties of his office. His exactions and his overbearing behaviour, combined with the fact that he was a foreigner, offended the English. He was heavily involved in advancing the fortunes of his family on the continent, and spent fourteen of the twenty-nine years he was archbishop outside England.<ref name=Moorman166>Moorman ''Church Life'' pp. 166–167</ref> He made strenuous efforts to free his office from debt, as he had inherited a see that was in debt over 22,000 [[Mark (money)|marks]], but managed to clear the debt before his death.<ref name=Moorman172>Moorman ''Church Life'' pp. 172–173</ref> He did this by securing the right to tax his clergy, for seven years, from the papacy. When a number of bishops refused to pay, they were suspended from office.<ref name=Gibbs20>Gibbs ''Bishops and Reform'' p. 20</ref> He also worked for the canonisation of [[Edmund of Abingdon]] while he was at the papal court-in-exile at [[Lyon]] from 1244 to 1249.<ref name=DNB/>

In 1244, Boniface rejected [[Robert Passelewe]], who had been selected as [[Bishop of Chichester]], on the grounds that Passelewe was illiterate. Boniface then nominated his own candidate, [[Richard of Chichester]], and although the king objected, Pope Innocent IV confirmed Richard's election. In 1258, Boniface objected to the selection of [[Hugh de Balsham]] as [[Bishop of Ely]], and tried to elevate [[Adam Marsh]] instead, but Hugh appealed to Rome, which upheld Hugh's election.<ref name=Gibbs84>Gibbs ''Bishops and Reform'' p. 84</ref> Boniface held church councils to reform the clergy, in 1257 at London, in 1258 at [[Merton Priory|Merton]], and in 1261 at [[Lambeth Palace|Lambeth]].<ref name=Gibbs146>Gibbs ''Bishops and Reform'' p. 146</ref>

During his archiepiscopate, a provincial court was established in the archdiocese of Canterbury, with a presiding ''Officialis'' appointed by Boniface.<ref name=Smith207>Smith "Officialis of the Bishop" ''Medieval Ecclesiastical Studies'' p. 207</ref>

==Controversies==
Boniface was energetic in defending the liberties of his see, and clashed with King Henry over the election of Henry's clerk Robert Passelewe to the [[Diocese of Chichester|see of Chichester]]. [[Robert Grosseteste]], the [[Bishop of Lincoln]], had examined Passelewe, and found him unfit for episcopal office, and Boniface then quashed the election in 1244. He was also involved in disputes with the king's half-brothers, especially [[Aymer de Valence]], who was Bishop of Winchester. He also quarrelled with his [[suffragan bishop]]s, who resented his attempts to supervise their affairs closely.<ref name=DNB/> In 1250 Boniface attempted a visitation of his province, and this disturbed his suffragan bishops, who protested that Boniface was taking exorbitant amounts of money during his visits. They appealed to the pope, who reaffirmed the right of Boniface to conduct his visitation, but set a limit on the amount that could be taken from any monastery or church.<ref name=Gibbs157>Gibbs ''Bishops and Reform'' p. 157</ref> After the visitation, Boniface left England again, and only returned in 1252, after the pope had decided the bishops' appeal in Boniface's favour. After his return, he continued to assert his rights and settled a number of disputes with his bishops. He secured professions of obedience from all but three of the 37 bishops consecrated during his time as archbishop. He also set up a court at Canterbury that heard appeals from the ecclesiastical courts of his suffragan bishops.<ref name=DNB/>

Boniface clashed with Henry's half-brothers, the [[Lusignan dynasty|Lusignan]]s, who arrived in England in 1247 and competed for lands and promotions with the queens' Savoy relatives. Boniface's quarrel with Aymer de Valence over the a hospital in Southwark led to the archbishop's palace at Lambeth being plundered and one of Boniface's functionaries being kidnapped. The dispute with Aymer was only settled in early 1253. Boniface was once more absent from England from October 1254 to November 1256, and spent most of that time in Savoy where he attempted to help his brothers rescue their eldest brother Thomas who was being held captive at Turin.<ref name=DNB/>

In 1258 and 1259, Boniface was a member of the Council of Fifteen, which conducted business for Henry III under the [[Provisions of Oxford]]. This Council consisted of the earls of [[Simon de Montfort, 6th Earl of Leicester|Leicester]], [[Richard de Clare, 6th Earl of Hertford|Gloucester]], [[Roger Bigod, 4th Earl of Norfolk|Norfolk]], [[William Maudit, 8th Earl of Warwick|Warwick]], [[Humphrey de Bohun, 2nd Earl of Hereford|Hereford]], the [[William de Forz, 4th Earl of Albemarle|Count of Aumale]], Peter of Savoy, [[John Fitzgeoffrey|John fitzGeoffrey]], [[Peter de Montfort]], Richard Grey, [[Roger Mortimer, 1st Baron Mortimer|Roger Mortimer]], James Audley, [[John Maunsell]], [[Walter de Cantilupe|Walter de Cantilupe, Bishop of Worcester]] as well as Boniface.<ref name=Powell189>Powell and Wallis ''House of Lords'' pp. 189–190</ref> One of the actions of this council was to send the Lusignans into exile.<ref name=DNB/> In April 1260, Boniface worked with [[Richard, 1st Earl of Cornwall|Richard of Cornwall]] to broker a peace between King Henry and Prince Edward.<ref name=DNB/>

Boniface accompanied the queen and Prince [[Edward I of England|Edward]] to [[Burgos]] for the marriage of Edward to [[Eleanor of Castile]] and Edward's knighting.<ref name=Edward10>Prestwich ''Edward I'' p. 10</ref> But in 1261 Boniface held a church council at Lambeth, where a series of ecclesiastical laws were published which denounced any royal limitations on ecclesiastical courts. These decrees were done without royal consent and thus was tantamount to an ecclesiastical revolt against royal authority similar to the baronial opposition movement that had begun in 1258.<ref name=Gray215>Gray "Archbishop Pecham" ''Studies in Church History II'' pp. 215–216</ref>

During the [[Second Barons' War]], Boniface seems to have sided first with the English bishops against King Henry, but later he sided with Henry. In 1262, he went to France, where he excommunicated the barons opposing the king. He was not summoned to the Parliament at London in January 1265 because he was abroad.<ref name=Powell196>Powell and Wallis ''House of Lords'' p. 196</ref> On the triumph of the king's party in 1265, he returned to England, arriving there in May 1266.<ref name=DNB/>

==Death and aftermath==
The [[English Gothic architecture#Early English Gothic|Early English Gothic]] chapel of Lambeth Palace dates from work carried out while Boniface was archbishop.<ref name=Survey>Roberts and Godfrey ''[http://www.british-history.ac.uk/report.aspx?compid=47051 Survey of London: Volume 23: Lambeth: South Bank and Vauxhall]''</ref> Boniface left England in November 1268, and never returned.<ref name=DNB/> He died 18 July 1270,<ref name=Handbook233/> in Savoy. He was buried with his family in the [[Cistercians|Cistercian]] abbey of [[Hautecombe Abbey|Hautecombe]] in Savoy.<ref name=DNB/> In his will, he left legacies to all the houses of the Franciscans and Dominicans in the diocese of Canterbury.<ref name=Burton121>Burton ''Monastic and Religious Orders'' p. 121</ref> His will had differing provisions for his burial depending on whether he died in England, France, or near the Alps.<ref name=Struggle437>Carpenter ''Struggle for Mastery'' pp. 437–438</ref> Oddly enough, his official seal included a head of the pagan god [[Serapis|Jupiter Serapis]] along with the usual depiction of the archbishop in full vestments.<ref name=Seals65>Harvey and McGuinness ''Guide to British Medieval Seals'' p. 65</ref>

After his death, Boniface's tomb was the center of a cult, and when the tomb was opened in 1580, his body was found to be perfectly preserved. The tomb and effigy was destroyed in the [[French Revolution]], his remains were reburied and a new tomb built in 1839. He was [[Beatification|beatified]] by Pope [[Pope Gregory XVI|Gregory XVI]] in 1839,<ref name=DNB/> and his feast day is 14 July.<ref name=DictSaint>Delaney ''Dictionary of Saints'' p. 104</ref>

Although Matthew Paris disapproved of Boniface,<ref name=Gibbs19>Gibbs ''Bishops and Reform'' p. 19</ref> modern historians have seen him as a responsible archbishop. The historian [[David Carpenter (historian)|D. A. Carpenter]] says that Boniface "became a respected and reforming archbishop".<ref name=Struggle342>Carpenter ''Struggle for Mastery'' p. 342</ref> His episcopal registers do not survive.<ref name=DNB/>

==Citations==
{{Reflist|40em}}

==References==
{{refbegin|colwidth=60em}}
* {{cite book |author=Burton, Janet |title= Monastic and Religious Orders in Britain: 1000–1300 |year= 1994|publisher=Cambridge University Press|series=Cambridge Medieval Textbooks |location=Cambridge, UK |isbn=0-521-37797-8}}
* {{cite book |author=Carpenter, David |authorlink= David Carpenter (historian) |title=The Struggle for Mastery: The Penguin History of Britain 1066–1284 |publisher=Penguin |location= New York|year=2004 |isbn=0-14-014824-8 }}
* {{cite book |author=Delaney, John P. |title=Dictionary of Saints|edition=2nd |publisher=Doubleday |location=Garden City, NY |year=1980 |isbn=0-385-13594-7 }}
* {{cite book |author1=Fryde, E. B. |author2=Greenway, D. E. |author3=Porter, S. |author4=Roy, I. |title=Handbook of British Chronology|edition=Third revised |publisher=Cambridge University Press |location=Cambridge, UK |year=1996 |isbn=0-521-56350-X }}
* {{cite book |author=Gibbs, Marion E. |author2= Lang, Jane |title=Bishops And Reform |publisher=Oxford University Press |location=Oxford, UK |origyear=1934 |year=2006|edition=Hesperides Press reprint |isbn=1-4067-1232-9 }}
* {{cite encyclopedia |author=Gray, J. W. |title=Archbishop Pecham and the Decrees of Boniface |encyclopedia=Studies in Church History |volume=II |editor=G. J. Cuming |publisher=Nelson |year=1965 |pages=215–219 |location=London}}
* {{cite book |author = Greenway, Diana E. |title = Fasti Ecclesiae Anglicanae 1066-1300: Volume 2: Monastic Cathedrals (Northern and Southern Provinces): Canterbury: Archbishops |url=http://british-history.ac.uk/report.aspx?compid=33853|year=1971|publisher = Institute of Historical Research | accessdate=6 February 2009}}
* {{cite book |author1=Harvey, P. D. A. |author2=McGuinness, Andrew |title=A Guide to British Medieval Seals |publisher=University of Toronto Press |location=Toronto |year=1996 |isbn=0-8020-0867-4}}
* {{cite encyclopedia |author=Knowles, Clive H. |title=Savoy, Boniface of (1206/7–1270) |encyclopedia=Oxford Dictionary of National Biography |publisher= Oxford University Press |year= 2004 |url=http://www.oxforddnb.com/view/article/2844| accessdate= 8 November 2007 |doi= 10.1093/ref:odnb/2844}} {{ODNBsub}}
* {{cite book |author=Moorman, John R. H. |authorlink= John Moorman |title= Church Life in England in the Thirteenth Century  |year=1955|edition=Revised |publisher= Cambridge University Press |location= Cambridge, UK  }}
* {{cite book |author1= Powell, J. Enoch  |authorlink1= Enoch Powell |author2= Wallis, Keith |title= The House of Lords in the Middle Ages: A History of the English House of Lords to 1540 |year=1968 |publisher= Weidenfeld and Nicolson |location=London  }}
* {{cite book |author=Prestwich, Michael|authorlink=Michael Prestwich |title=Edward I |publisher=Yale University Press |location=New Haven, CT |year=1997  |isbn=0-300-07157-4 }}
* {{cite book |author1=Roberts, Howard |author2=Godfrey, Walter H. |url=http://www.british-history.ac.uk/report.aspx?compid=47051|title= Survey of London: Volume 23: Lambeth: South Bank and Vauxhall |publisher=English Heritage |year=1951|accessdate= 7 February 2009}}
* {{cite encyclopedia |author=Smith, David M. |title=The Officialis of the Bishop in Twelfth- and Thirteenth-Century England: Problems of Terminology |encyclopedia=Medieval Ecclesiastical Studies in Honour of Dorothy M. Owen |editor1=Franklin, M. J. |editor2=Harper-Bill, Christopher |publisher=Boydell Press |location=Woodbridge, UK |year=1995 |isbn=0-85115-384-4| pages=201–220}}
* {{cite book |author=Walsh, Michael J. |title= A New Dictionary of Saints: East and West |year=2007 |publisher= Burns & Oates |location=London |isbn=0-86012-438-X  }}
{{refend}}

{{s-start}}
{{s-rel|ca}}
{{s-bef| before=[[Edmund of Abingdon]] }}
{{s-ttl| title=[[Archbishop of Canterbury]] | years=1241–1270}}
{{s-aft| after=[[Robert Kilwardby]] <br>([[William Chillenden]]<br> chosen but set aside by the Pope)  }}
{{s-end}}

{{Archbishops of Canterbury}}

{{Good article}}
{{Use dmy dates|date=June 2013}}
{{Use British English|date=June 2013}}
{{Authority control}}

{{DEFAULTSORT:Boniface Of Savoy}}
[[Category:1217 births|Savoy, Boniface of]]
[[Category:1270 deaths|Savoy, Boniface of]]
[[Category:Bishops of Belley]]
[[Category:Archbishops of Canterbury]]
[[Category:13th-century Roman Catholic archbishops]]
[[Category:13th-century Christian saints]]
[[Category:House of Savoy]]
[[Category:Medieval English saints]]
[[Category:Burials at Hautecombe Abbey]]