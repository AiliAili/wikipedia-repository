{{Infobox journal
| title = Astronomy Letters
| cover =
| editor = [[Rashid A. Sunyaev]]
| discipline = [[Astronomy]], [[astrophysics]]
| formernames =
| abbreviation = Astron. Lett.
| publisher = [[MAIK Nauka/Interperiodica]]
| country = Russia
| frequency = Monthly
| history = 1994–present
| openaccess =
| license =
| impact = 0.956
| impact-year = 2015
| website = http://www.maik.ru/cgi-perl/journal.pl?lang=eng&name=letters
| link1 = http://www.springer.com/astronomy/journal/11443
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 43712597
| LCCN = 94646556 
| CODEN =
| ISSN = 1063-7737
| eISSN = 1562-6873
}}
'''''Astronomy Letters''''' (Russian: '''''Pis’ma v Astronomicheskii Zhurnal''''') is a [[Russia|Russian]] [[Peer review|peer-reviewed]] [[scientific journal]]. The journal covers research on all aspects of [[astronomy]] and [[astrophysics]], including high energy astrophysics, [[cosmology]], [[space astronomy]], [[theoretical astrophysics]], [[radio astronomy]], extra galactic astronomy, stellar astronomy, and investigation of the Solar system.

''Pis’ma v Astronomicheskii Zhurnal'' is translated in its English version by [[MAIK Nauka/Interperiodica]], which is also the official publisher. However, beginning in 2006 access and distribution outside of Russia is made through [[Springer Science+Business Media]]. Both English and Russian versions are published simultaneously.

''Astronomy Letters'' was established in 1994 and published bimonthly. From 1999, it has been published monthly. The [[editor-in-chief]] is [[Rashid A. Sunyaev]] ([[Space Research Institute]]).

==Abstracting and indexing==
''Astronomy Letters'' is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Academic Search]]
* [[Astrophysics Data System]]
* [[Chemical Abstracts Service]]
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Inspec]]
* [[EBSCO Publishing|EBSCO]]/Science & Technology Collection
* [[Science Citation Index]]
* [[Scopus]]
* [[SIMBAD|Simbad Astronomical Database]]
* [[TOC Premier]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.988.<ref name=WoS>{{cite book |year=2012 |chapter=Astronomy Letters |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-11-05 |series=[[Web of Science]] |postscript=.}}</ref>

==See also==
* ''[[Astronomy Reports]]''

==References==
{{Reflist}}

==External links==
* {{Official website|1=https://web.archive.org/web/20070824165129/http://www.maik.rssi.ru:80/cgi-perl/journal.pl?lang=eng&name=letters&page=main}}
* [http://hea.iki.rssi.ru/pazh/ ''Pis'ma v Astronomicheskii Zhurnal''] (English)

[[Category:Astronomy journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1994]]
[[Category:Science and technology in Russia]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Nauka academic journals]]