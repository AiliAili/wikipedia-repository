{{Infobox Political post
|post            = Minister for Defence
|body            = the Republic of Abkhazia
|insignia        = 
|insigniasize    = 165px
|insigniacaption = 
|image           =
|incumbent       = [[Mirab Kishmaria]]
|incumbentsince  = 26 June 2007
|residence       =
|appointer       = [[President of Abkhazia]]
|termlength      = unrestricted
|inaugural       = [[Vladimir Arshba]]
|formation       = 11 October 1992
|website         = 
}}

The office of '''Minister for Defence''' ({{lang-ab|атәыла хьчара аминистр}}, {{lang-ka|თავდაცვის მინისტრი}}, {{lang-ru|министр обороны}}) has been one of the most important in the breakaway [[Republic of Abkhazia]]<ref>{{Abkhazia-note}}</ref> due to the ongoing [[Georgian-Abkhazian conflict|conflict with Georgia]].<ref name=agovsosnaliev/> The position was created 11 October 1992, shortly after the outbreak of the [[War in Abkhazia (1992-1993)|1992-1993 war with Georgia]]. The first Minister for Defence was [[Vladimir Arshba]], but due to his injuries for most of the time his functions were carried out by Deputy Minister for Defence and Chief of the General Staff [[Sultan Sosnaliev]], who eventually also formally succeeded Arshba on 25 April 2003. Sosnaliev oversaw Abkhazia's successful expulsion of Georgian forces and remained Defence Minister until on 1 July 1996 he resigned to return to his native [[Kabardia]].

Sosnaliev's successor was [[Vladimir Mikanba]], who held the position during the [[War in Abkhazia (1998)|May 1998 war in the Gali district]] and the [[2001 Kodori crisis|October 2001 incursion of Chechen fighters down the Kodori valley]].

Mikanba was succeeded by [[Raul Khajimba]], who would go on to become Prime Minister, and who is the current Vice President of Abkhazia. 22 April 2003, when Khajimba became Prime Minister, [[Viacheslav Eshba]] became Minister for Defence. On 8 November 2004, during the height of the crisis following the [[Abkhazian presidential election, 2004|2004 Presidential election]], a source within the government claimed that Eshba had been replaced by [[Mukhamed Kilba]], the deputy chief of Khajimba's presidential campaign, but this was denied by Prime Minister [[Nodar Khashba]].<ref name=kuzel63965>{{cite web
 |url = http://www.kavkaz-uzel.ru/articles/63965
 |title = Премьер-министр Абхазии не подтвердил назначение нового министра обороны непризнанной республики
 |publisher = Кавказский узел
 |date = 8 November 2004
 |accessdate = 18 January 2009
 }}</ref>

On 25 February 2005, newly elected Predident [[Sergei Bagapsh]] called on Sultan Sosnaliev to become Defence Minister once more. Sosnaliev agreed and held the office until shortly after his 65th birthday on 8 May 2007 he resigned for personal reasons.<ref name=agov5559/>

26 June 2007 then Deputy Defence Minister [[Mirab Kishmaria]] was appointed Sosnaliev's successor. During the [[2008 South Ossetia war|August 2008 war in South Ossetia]], he oversaw [[Battle of the Kodori Valley|the Abkhazian conquest]] of so-called [[Upper Abkhazia]], the only part of Abkhazia that had remained under Georgian control since the 1992-1993 war. Kishmaria remains Minister of Defence until the present day.

== List of people to hold the office ==

{| class="wikitable" width=75% cellpadding="2"
|-style="background-color:#E9E9E9; font-weight:bold" align=left
| #
| width=240|Name
| width=150|Entered office
|
| width=150|Left office
|
| width=160|President
| width=200|Comments
|-
| 1
| [[Vladimir Arshba]] 
| 11 October 1992
|<ref name=agovsosnaliev>{{cite web
 |url = http://www.abkhaziagov.org/ru/president/press/smi_abkhazia/sosnaliev.php
 |title = Полководец Султан Сосналиев
 |publisher = Администрация Президента Республики Абхазия
 |date =
 |accessdate = 17 January 2009
 }}</ref>
| 25 April 1993
|
| rowspan=5|[[Vladislav Ardzinba]]
| 
|-
| 2
| [[Sultan Sosnaliyev]] 
| 25 April 1993
|<ref name=agovsosnaliev/>
| 1 July 1996
|<ref name=agovsosnaliev/>
| First time
|-
| 3
| [[Vladimir Mikanba]] 
| July 1996
|
| 9 December 2002
|<ref name=informacia/>
|
|-
| 4
| [[Raul Khadjimba|Raul Jumka-ipa Khajimba]] 
| 9 December 2002
|<ref name=informacia/>
| 22 April 2003
|
|
|-
| 5
| [[Viacheslav Eshba]]
| 22 April 2003
|<ref name=informacia>{{cite web
 |url = http://www.informacia.ru/facts/ardzinba-facts.htm
 |title = Регистрация средства массовой информации Эл №77 - 8262
 |publisher = Секретные материалы России
 |date =
 |accessdate = 18 January 2009
 }}</ref>
| 25 February 2005
|<ref name=agov1454>{{cite web
 |url = http://www.abkhaziagov.org/ru/president/activities/decree/detail.php?ID=1454
 |title = Указ Президента Абхазии №1 от 14.02.2005 |publisher = Администрация Президента Республики Абхазия
 |date = 14 February 2005
 |accessdate = 17 December 2008
 }}</ref>
| 
|-
| 6
| [[Sultan Sosnaliyev]] 
| 25 February 2005
|<ref name=agov1672>{{cite web
 |url = http://www.abkhaziagov.org/ru/president/activities/decree/detail.php?ID=1672
 |title = Указ Президента Абхазии №26 от 25.02.2005
 |publisher = Администрация Президента Республики Абхазия
 |date = 25 February 2005
 |accessdate = 17 January 2009
 }}</ref>
| 8 May 2007
|<ref name=agov5559>{{cite web
 |url = http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=5559
 |title = Министр обороны Абхазии ушел в отставку 
 |publisher = Администрация Президента Республики Абхазия
 |date = 8 May 2007
 |accessdate = 17 January 2009
 }}</ref>
|rowspan=2|[[Sergei Bagapsh]]
| Second time
|-
| 7
| [[Mirab Kishmaria|Mirab Boris-ipa Kishmaria]] 
| 26 June 2007
|<ref name=agov6655>{{cite web
 |url = http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=6655
 |title = Мераб Кишмария назначен на должность Министра обороны Республики Абхазия
 |publisher = Администрация Президента Республики Абхазия
 |date = 26 June 2007
 |accessdate = 17 January 2009
 }}</ref>
| Incumbent
|
|
|}

==See also==
*[[Military of Abkhazia]]
*[[President of Abkhazia]]
*[[Vice President of Abkhazia]]
*[[Prime Minister of Abkhazia]]
*[[Minister for Foreign Affairs of Abkhazia]]

==References==
{{reflist|2}}

[[Category:Politics of Abkhazia]]