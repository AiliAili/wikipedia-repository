{{Infobox person
|name    = Freeman Fulbright
|image   = Freeman Fulbright - January 4, 1966.jpg
|birth_date        = {{birth date|1925|4|26|mf=y}}
|birth_place       = [[Atlanta]], Georgia
|death_date        = {{death date and age|1978|6|6|1925|4|26|mf=y}}
|death_place       = [[New York City]], New York
|occupation        = Reporter and public relations executive
}}

'''Freeman Fulbright''' (1925-1978) was a reporter and public relations executive, notable for his coverage of [[Adlai Stevenson II|Adlai Stevenson]]'s 1952 presidential campaign for the [[International News Service]]. He was editor of ''[[Newsweek]]'' in the 1950s, executive editor of the ''[[New York Herald Tribune]]'' (1961-1964), and an executive vice president of [[Hill and Knowlton]], the largest public relations firm in the world at the time.

==Early life==

Fulbright was born on April 26, 1925 in Atlanta, Georgia, to Ernest Alexander Fulbright and Lessie Freeman Fulbright, both of North Carolina. The family moved back to North Carolina soon after their son’s birth, settling in [[Durham, North Carolina|Durham]]. Fulbright’s father was originally a traveling salesman, but with the onset of the Depression lost work and subsequently left the family, ultimately joined the US [[Merchant Marine]] service in 1937, and died of a ruptured appendix in a hospital in [[Calcutta]] on April 10, 1943 after sailing in a convoy from the US East Coast.  Fulbright was thereafter raised by his mother and maternal grandmother, Minerva “Minnie” Freeman.  Fulbright graduated from [[Durham High School (North Carolina)|Durham High School]], where he received high marks and was editor of the school newspaper. He was accepted to [[Duke University]], but was unable to attend due to financial constraints.<ref name="WhoWho">Who’s Who in America, 1977-78, page 1088</ref>

==Newspaper, wire service, and magazine career==

Fulbright started his newspaper career as a sportswriter for the ''[[Durham Morning Herald]]'' in 1941 while still in high school.  After graduating from Durham High School and receiving a 1H deferment from the draft due to a heart murmur, he worked for the ''[[Cincinnati Post]]'' from 1944 to 1945.  In 1945 he signed on with the late [[International News Service]] (INS) as legislative correspondent covering the Ohio state legislature in Columbus, Ohio.  He moved to Chicago with INS in 1947 as a news editor, and then to Washington, DC in 1951 as a political correspondent.<ref name="WhoWho" />
 
In 1952 Fulbright was assigned by INS to cover Democrat [[Adlai Stevenson II|Adlai Stevenson Jr.]] in his campaign for President of the United States running against [[Dwight Eisenhower]]. Fulbright’s fellow wire-service reporters on the Stevenson campaign were [[Merriman Smith]] of [[United Press]] (UP) and [[Jack Bell]] of [[Associated Press]] (AP).  The INS White House correspondent position was opening in 1953, and INS promised that position to the correspondent of the winning Presidential candidate.  The INS correspondent covering Eisenhower was [[Bob Clark (television reporter)]], who went on to be a political reporter with [[American Broadcasting Company|ABC-TV]] News.  With Eisenhower’s victory, Fulbright was made Night Managing Editor in the New York City office, which had been his ultimate destination. He served in that role from 1952 to 1955.  During this time [[William Randolph Hearst Jr.]] wrote about him, “Editors like Milt Kaplan, Phil Reed, Paul Allerup, Freeman Fulbright, and others took second place to no one in the business.".<ref>William Randolph Hearst, Jr., The Hearsts: Father and Son, 1981, page 301</ref>
 
Fulbright joined the staff of ''[[Newsweek]]'' in 1957, first as editor of its popular and flagship “Periscope” section, and then as a general editor. It was at ''Newsweek'' that Fulbright came to the attention of editor-in-chief [[John Denson]]. Fulbright and Denson worked closely together at Newsweek, then left together in 1961 to take over editor responsibilities at the [[New York Herald Tribune]], long considered one of the best-written papers in the country, but suffering from declining circulation.<ref>{{cite journal|url=https://select.nytimes.com/gst/abstract.html?res=F70717F83F5E147A93C4A91789D95F458685F9|title=4 Executives Named by Herald Tribune|journal=New York Times|date=December 6, 1961}}</ref> Denson served as editor-in-chief and Fulbright as executive editor of the “Trib”. Denson brought a fresh and edgier style to the paper, particularly in the graphic design of the front page, but was unable to reverse declining circulation trends, and the 1962 newspaper strike delivered a mortal blow to that paper. Denson was forced out by owner [[John Hay Whitney|John Whitney]], and Fulbright “[fell] devotedly on his sword”.<ref>Richard Kluger, The Paper: the Life and Death of the New York Herald Tribune, 1986, page 646</ref>
.<ref>{{cite journal|url=https://select.nytimes.com/gst/abstract.html?res=F20F15FA3D5E177A93C0A8178BD95F468685F9|title=Herald Tribune Gets New Editor; Denson Leaves in Dispute|journal=New York Times|date=October 12, 1962}}</ref>

==Public relations career==

After leaving INS and before joining Newsweek, Fulbright worked at New York-based public relations firm [[Walker & Crenshaw]] as a vice president and general manager from 1955 to 1957.
 
With the decline of the New York newspaper industry and the demise of the ''New York Herald Tribune'', ''[[New York Journal American]]'', and ''[[New York World Sun]]'', opportunities as a newspaper editor were limited, and so Fulbright joined public relations firm [[Selvage and Lee]] (subsequently to become [[Manning, Selvage and Lee]]) as senior vice president in 1962
,<ref>{{cite journal|url=https://select.nytimes.com/gst/abstract.html?res=F40F14FA395D147B93C2AB178AD95F468685F9|title=Advertising: FM Sales Bid Tuned to Quality; More Independent Stations Accounts People Addendum|journal=New York Times|date=November 20, 1962}}</ref> promoted to executive vice president in 1966.<ref>{{cite journal|url=https://select.nytimes.com/gst/abstract.html?res=F40915FE3A5F1A7493C5A9178AD85F428685F9|title=Advertising: Mobil Makes Turn on the Road; People Addenda|journal=New York Times|date= January 7, 1966}}</ref> He served in that role until 1969, and grew revenues and the client list over that period. 
 
In 1969 he joined [[Hill and Knowlton]] as a senior vice president, then became one of six executive vice presidents in 1973.<ref>{{cite journal|url=https://select.nytimes.com/gst/abstract.html?res=F40917FF3F5F10738DDDA10A94DE405B838BF1D3|title=Advertising: Money Is Talking; Air Force and D'Arcy To Keep Relationship A New Monsanto Entry Times Rate Rise Delayed Accounts People Addenda Swift Unit Has Agency|journal=New York Times|date=June 28, 1973}}</ref> At this time Hill and Knowlton (known as “H&K” in the industry) was the largest public relations firm in the world. Fulbright primarily handled financial relations for clients, including [[Gillette (brand)|Gillette]], [[Ernst & Ernst]], [[Lincoln National Corporation|Lincoln National Insurance]], [[Russell Athletic]], [[Mattel]], [[Cities Service]], [[Reserve Oil & Gas]], [[The Richmond Company]], and [[LouAna]].<ref name="WhoWho" />
 
Fulbright died of a massive heart attack in the Hill and Knowlton offices in New York on June 6, 1978.<ref>{{cite journal|url=https://select.nytimes.com/gst/abstract.html?res=F40917FE3B5513728DDDA10894DE405B888BF1D3|title=Freeman Fulbright Dies At 53, Vice President with Hill and Knowlton|journal=New York Times|date=June 8, 1978}}</ref>

==Personal life==
Fulbright met his future wife Jane Meese in Chicago in 1947, and the pair was married in 1948. His wife survived him by three years, dying in 1981.  They had two children, daughter Carolyn Frances Fulbright Fried, born in 1953 and deceased in 2008, and son Charles Cary Freeman Fulbright, born in 1957. Fulbright was active in his church, serving as a trustee of [[Rutgers Presbyterian Church]] on West 73rd Street in New York. He was a member of the [[National Press Club]], [[Sigma Delta Chi]], and the [[Wings Club]].  He is buried with his wife and her parents in [[East Avenue Cemetery]] in [[New Philadelphia, Ohio]], with the words "I Did It May Way" on the tombstone.<ref>{{cite journal|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=10658987|title=Freeman Fulbright|journal=Find a Grave|date=March 23, 2005}}</ref>

The Fulbright Library at [[the Bay School of San Francisco]] is named in memory of Jane and Freeman Fulbright.<ref>{{cite journal|url=http://www.bayschoolsf.org/Academics/Library|title=The Fulbright Library|journal=The Bay School of San Francisco|date=April 26, 2014}}</ref>

==References==
{{Reflist|2}}

{{DEFAULTSORT:Fulbright, Freeman}}
[[Category:1925 births]]
[[Category:1978 deaths]]
[[Category:American male journalists]]
[[Category:American public relations people]]
[[Category:Writers from Atlanta]]
[[Category:20th-century American writers]]