{{about|the first novel by Charles Dickens|other uses|The Pickwick Papers (disambiguation)}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = The Pickwick Papers 
| title_orig     = The Posthumous Papers of the Pickwick Club, Containing a Faithful Record of the Perambulations, Perils, Travels, Adventures and Sporting Transactions of the Corresponding Members
| translator     =
| image          = Pickwickclub serial.jpg
| caption  = Original cover issued in 1836
| author         = [[Charles Dickens]] ("Boz")
| illustrator    = [[Robert Seymour (illustrator)|Robert Seymour]]<br>[[Robert William Buss]]<br>[[Hablot Knight Browne]] ([[Phiz]])
| cover_artist   =
| country        = England
| language       = English
| series         = 
| subject        = Travels in the English Countryside
| genre          = [[Novel]]
| publisher      = [[Chapman & Hall]]
| published   = Serialised April 1836 – November 1837; book format 1837
| media_type     = Print 
| pages          = 
| isbn           =
| dewey=
| congress=
| oclc=
| preceded_by    =
| followed_by    =
}}

[[File:Charles Dickens - Pickwick Papers, autographed first edition title page.jpg|thumb|upright|Autographed title page of a 1st edition copy]]

'''''The Posthumous Papers of the Pickwick Club''''' (also known as '''''The Pickwick Papers''''') was [[Charles Dickens]]'s first novel. He was asked to contribute to the project as an up-and-coming writer following the success of ''[[Sketches by Boz]]'', published in 1836 (most of Dickens' novels were issued in [[shilling]] instalments before being published as complete volumes). Dickens (still writing under the pseudonym of Boz) increasingly took over the unsuccessful monthly publication after the original illustrator [[Robert Seymour (illustrator)|Robert Seymour]] had committed suicide.

With the introduction of [[Sam Weller (fictional character)|Sam Weller]] in chapter 10, the book became the first real publishing phenomenon, with bootleg copies, theatrical performances, Sam Weller joke books, and other merchandise.

After the publication, the widow of Robert Seymour claimed that the idea for the novel was originally her husband's; however, in his preface to the 1867 edition, Dickens strenuously denied any specific input, writing that "Mr Seymour never originated or suggested an incident, a phrase, or a word, to be found in the book."<ref>C. Dickens, ''The Posthumous Papers of the Pickwick Club'' (1867 reprint), p.8</ref>

==Background==

Dickens, a young writer, 24 years old, was working as a Parliamentary reporter and a roving journalist; a collection of his "colour" sketches mainly of London life had been published as ''[[Sketches by Boz]]''. A firm of London publishers, Messrs. [[Chapman & Hall]], was then projecting a series of "cockney sporting plates" by illustrator Robert Seymour. There was to be a club, the members of which were to be sent on hunting and fishing expeditions into the country. Their guns were to go off by accident; fishhooks were to get caught in their hats and trousers. All these and other misadventures were to be depicted in Seymour's comic plates.<ref name=ea>{{Americana|wstitle=Pickwick Papers|first=Wilbur L. |last= Cross |authorlink=Wilbur L. Cross |inline=1}}</ref>

At this juncture, Charles Dickens was called in to supply the letterpress – that is, the description necessary to explain the plates and connect them into a sort of [[graphic novel|picture novel]] such as was then the fashion. Though protesting that he knew nothing of sport, Dickens nevertheless accepted the commission; he consented to the machinery of a club, and in accordance with the original design sketched Mr Winkle who aims at a sparrow only to miss it.<ref name=ea/>

Only in a few instances did Dickens adjust his narrative to plates that had been prepared for him. Typically, he himself led the way with an instalment of his story, and the artist was compelled to illustrate what Dickens had already written. The story thus became the prime source of interest, and the illustrations merely of secondary importance. By this reversal of interest, Dickens transformed, at a stroke, a current type of fiction, consisting mostly of pictures, into a novel of contemporary London life. Simple as the process may appear, others who had tried the plan had all failed. [[Pierce Egan]] partially succeeded in his ''Tom and Jerry'', a novel in which the pictures and the letterpress are held in even balance. Dickens won a complete triumph.<ref name=ea/> In future years, however, Dickens was suspiciously eager to distance himself from suggestions that Pierce Egan's ''Life in London'' had been a formative influence.<ref>David Snowdon, ''Writing the Prizefight: Pierce Egan's Boxiana World'' (Bern, 2013)</ref>

Robert Seymour provided the illustrations for the first two instalments before his suicide. [[Robert William Buss|Robert Buss]] illustrated the third instalment, but his work was not liked by Dickens and the remaining instalments were illustrated by [[Hablot Knight Browne|''"Phiz"'' (Hablot Knight Browne)]] who went on to illustrate most of Dickens' novels. The instalments were first published in book form in 1837.<ref>{{cite web|last=Dickens|first=Charles|title=The posthumous papers of the Pickwick Club|url=http://openlibrary.org/books/OL23001437M/The_posthumous_papers_of_the_Pickwick_Club|publisher=[[Open Library]]}}</ref>

==Summary==

[[File:The Writings of Charles Dickens v1 p4 (engraving).jpg|thumb|left|200 px|Robert Seymour illustration depicting Pickwick addressing the club]]

Written for publication as a [[Serial (literature)|serial]], ''The Pickwick Papers'' is a sequence of loosely related adventures. The action is given as occurring 1827–8, though critics have noted some seeming anachronisms.<ref name="Mark Wormald 2003">Mark Wormald (2003) "Introduction" to ''The Pickwick Papers'' by Charles Dickens. London, Penguin.</ref> It has been stated that Dickens satirized the case of George Norton suing Lord Melbourne in ''The Pickwick Papers.''<ref>(Melbourne by Lord David Cecil. Bobbs-Merrill Company. 1939. p301</ref>
The novel's main character, [[Samuel Pickwick]], [[Esquire]], is a kind and wealthy old gentleman, the founder and perpetual president of the Pickwick Club. To extend his researches into the quaint and curious phenomena of life, he suggests that he and three other "Pickwickians" (Mr Nathaniel Winkle, Mr Augustus Snodgrass, and Mr Tracy Tupman) should make journeys to places remote from London and report on their findings to the other members of the club. Their travels throughout the English countryside by [[Coach (carriage)|coach]] provide the chief theme of the novel. A distinctive and valuable feature of the work is the generally accurate description of the old [[coaching inn]]s of England.<ref>Mark Wormald (2003) "Introduction" to ''The Pickwick Papers'' by Charles Dickens. London, Penguin</ref>  (One of the main families running the Bristol to Bath coaches at the time was started by [[Eleazer Pickwick]]).<ref>Brenda J. Buchanan, ‘Pickwick, Eleazer (bap. 1749, d. 1837)’, Oxford Dictionary of National Biography, Oxford University Press, 2004 [http://www.oxforddnb.com/view/article/47584, accessed 4 Aug 2014]</ref>

Its main literary value and appeal is formed by its numerous memorable characters. Each character in ''The Pickwick Papers'', as in many other Dickens novels, is drawn comically, often with exaggerated personality traits. [[Alfred Jingle]], who joins the cast in chapter two, provides an aura of comic villainy, with his devious tricks repeatedly landing the Pickwickians into trouble. These include a nearly successful attempted elopement with the spinster Rachael Wardle of Dingley Dell manor, misadventures with Dr Slammer, and others.

Further humour is provided when the comic cockney Sam Weller makes his advent in chapter 10 of the novel. First seen working at the [[White Hart Inn (Borough)|White Hart Inn]] in [[Borough, London|The Borough]], Weller is taken on by Mr Pickwick as a personal servant and companion on his travels and provides his own oblique ongoing narrative on the proceedings. The relationship between the idealistic and unworldly Pickwick and the astute [[cockney]] Weller has been likened to that between [[Don Quixote]] and [[Sancho Panza]].<ref>Mark Wormald (2003) "Introduction" to ''The Pickwick Papers'' by Charles Dickens</ref>

Through humor Dickens is able to capture quintessential aspects of English life in the mid-nineteenth century that a more sober approach would miss.  Perhaps the popularity of this novel was due in part to the fact that the readers of the time were able to truly see themselves, and could accept themselves because of Dickens's skillful use of humor.

Other notable adventures include Mr Pickwick's attempts to defend a lawsuit brought by his landlady, Mrs Bardell, who (through an apparent misunderstanding on her part) is suing him for [[breach of promise]]. Another is Mr Pickwick's incarceration at [[Fleet Prison]] for his stubborn refusal to pay the compensation to her — because he doesn't want to give a penny to Mrs Bardell's lawyers, the unscrupulous firm of Messrs. Dodson and Fogg. The generally humorous tone is here briefly replaced by biting social satire (including satire of the legal establishment). This foreshadows major themes in Dickens's later books.

Mr Pickwick, Sam Weller, and Weller Senior also appear in Dickens's serial, ''[[Master Humphrey's Clock]]''.

==Characters==

===Central characters===
[[File:Hablot Knight Browne - The Pickwick Papers, Sam Weller with his father, Chapter XXXII.jpg|thumb|right|Sam Weller and his father Tony Weller (The Valentine)]]
*[[Samuel Pickwick]] — the main protagonist and founder of the Pickwick Club. Following his description in the text, Pickwick is usually portrayed by illustrators as a round-faced, clean-shaven, portly gentleman wearing spectacles.
*Nathaniel Winkle — a young friend of Pickwick's and his travelling companion; he considers himself a sportsman, though he turns out to be dangerously inept when handling horses and guns.
*Augustus Snodgrass — another young friend and companion; he considers himself a poet, though there is no mention of any of his own poetry in the novel.
*Tracy Tupman — the third travelling companion, a fat and elderly man who nevertheless considers himself a romantic lover.
*[[Sam Weller (character)|Sam Weller]] — Mr Pickwick's valet, and a source of idiosyncratic proverbs and advice.
*Tony Weller — Sam's father, a loquacious coachman.
*Alfred Jingle — a strolling actor and charlatan, noted for telling bizarre anecdotes in a distinctively extravagant, disjointed style.<ref name="Mark Wormald 2003"/>

===Supporting characters===

*Joe — the "fat boy" who consumes great quantities of food and constantly falls asleep in any situation at any time of day; Joe's sleep problem is the origin of the medical term [[Pickwickian syndrome]] which ultimately led to the subsequent description of [[Obesity hypoventilation syndrome]].
*Job Trotter — Mr Jingle's wily servant, whose true slyness is only ever seen in the first few lines of a scene, before he adopts his usual pretence of meekness.
*Mr Wardle — owner of a farm in Dingley Dell. Mr Pickwick's friend, they meet at the military review in Rochester. Joe is his servant.
*Rachael Wardle — Mr. Wardle's spinster sister, who tries in vain to elope with the unscrupulous Jingle.
*Mr Perker — an attorney of Mr Wardle, and later of Mr Pickwick.
*Mary — "a well-shaped female servant" and Sam Weller's "Valentine".
*Mrs Martha Bardell — Mr Pickwick's widowed landlady who brings a case against him for breach of promise.
*Emily Wardle — one of Mr Wardle's daughters, very fond of Mr Snodgrass.
*Arabella Allen — a friend of Emily Wardle and sister of Ben Allen. She later elopes with Mr. Winkle and marries him.
*Benjamin "Ben" Allen — Arabella's brother, a dissipated medical student.
*Robert "Bob" Sawyer — Ben Allen's friend and fellow student.

==Other adaptations==
[[File:Hablot Knight Browne - The Pickwick Papers, Pickwick at the slide.jpg|thumb|right|Mr Pickwick Slides]]
The novel has been adapted to film, television, and radio:
*1913 — a [[Silent film|silent]] short starring [[John Bunny]] as [[Samuel Pickwick]] and H. P. Owen as [[Sam Weller (character)|Sam Weller]].
*1921 — ''[[The Adventures of Mr. Pickwick]]'', silent, [[Lost film|lost]], starring Frederick Volpe and Hubert Woodward
*1936 — On 13 November 1936 (less than two weeks after the BBC began regularly scheduled television broadcasts) ''The British Music Drama Opera Company'' under the direction of [[Vladimir Rosing]] presented the world's first televised opera: ''Pickwick'' by [[Albert Coates (musician)|Albert Coates]].<ref>Herbert, Stephen A., ''History of Early Television Vol 2.'', (2004), p. 86-87. Routledge.</ref>
*1938 — 'The Pickwick Papers', [[Orson Welles]]'s ''Mercury Theater on the Air'' radio adaptation (20 November 1938)<ref>[https://news.google.com/newspapers?nid=1499&dat=19381120&id=fnYxAAAAIBAJ&sjid=HCIEAAAAIBAJ&pg=4294,1395102 ‘Orson Welles Offers 'Pickwick Papers'’, The Milwaukee Journal — Nov 20, 1938]</ref>
*[[The Pickwick Papers (1952 film)|1952]] — starring [[James Hayter (actor)|James Hayter]], [[Nigel Patrick]], [[Alexander Gauge]] and [[Harry Fowler]] (the first sound film version, and to this day, the only sound version of the story released to cinemas)

In 1985 BBC released a [[The Pickwick Papers (TV series)|12-part 350-minute miniseries]] starring [[Nigel Stock (actor)|Nigel Stock]], [[Alan Parnaby (actor)|Alan Parnaby]], [[Clive Swift]] and [[Patrick Malahide]].

There was also a London stage musical version entitled ''[[Pickwick (musical)|Pickwick]]'', by [[Cyril Ornadel]], [[Wolf Mankowitz]], and [[Leslie Bricusse]]. It starred [[Harry Secombe]], later to become more famous as [[Mr Bumble]] in the film version of ''[[Oliver! (film)|Oliver!]]''. But ''[[Pickwick (musical)|Pickwick]]'' (the musical) was not a success in the United States when it opened there in 1965; in 1969 the [[BBC]] filmed the musical as the TV movie ''[[Pickwick (TV movie)|Pickwick]]''. Both versions featured the song ''[[If I Ruled the World]]'', which became a modest hit for Secombe.

Part of ''The Pickwick Papers'' were featured in ''Charles Dickens' Ghost Stories'', a 60-minute animation made by Emerald City Films (1987). These included ''The Ghost in the Wardrobe'', ''The Mail Coach Ghosts'', and ''The Goblin and the Gravedigger''.

==Publication==
[[File:Hablot Knight Browne - The Pickwick Papers, Gabriel and the goblin.jpg|thumb|right|The Goblin and the Sexton]]
[[File:Hablot Knight Browne - The Pickwick Papers, Mr. Pickwick in debtor's prison.jpg|thumb|right|Discovery of Jingle in the Fleet]]
The novel was published in 19 issues over 20 months; the last was double-length and cost two [[shilling]]s. In mourning for his sister-in-law [[Mary Hogarth]], Dickens missed a deadline and consequently there was no number issued in May 1837. Numbers were typically issued on the last day of its given month:
*I – March 1836 (chapters 1–2);
*II – April 1836 (chapters 3–5);
*III – May 1836 (chapters 6–8);
*IV – June 1836 (chapters 9-11);
*V – July 1836 (chapters 12–14);
*VI – August 1836 (chapters 15–17);
*VII – September 1836 (chapters 18–20);
*VIII – October 1836 (chapters 21–23);
*IX – November 1836 (chapters 24–26);
*X – December 1836 (chapters 27–29);
*XI – January 1837 (chapters 30–32);
*XII – February 1837 (chapters 33–34);
*XIII – March 1837 (chapters 35–37);
*XIV – April 1837 (chapters 38–40);
*XV – June 1837 (chapters 41–43);
*XVI – July 1837 (chapters 44–46);
*XVII – August 1837 (chapters 47-49);
*XVIII – September 1837 (chapters 50–52);
*XIX-XX – October 1837 (chapters 53–57);

It is interesting to keep the number divisions and dates in mind while reading the novel, especially in the early parts. ''The Pickwick Papers'', as Charles Dickens's first novel, is particularly chaotic: the first two numbers featured four illustrations by Robert Seymour and 24 pages of text. Seymour killed himself and was replaced by R W Buss for the third number; the format was changed to feature two illustrations and 32 pages of text per issue. Buss didn't work out as an illustrator and was replaced by [[Hablot Knight Browne|H K 'Phiz' Browne]] for the fourth issue; Phiz continued to work for Dickens for 23 years (he last illustrated ''[[A Tale of Two Cities]]'' in 1859).{{citation needed|date=August 2014}}

As a testament to the book's popularity, many other artists, beyond the three official illustrators, created drawings without the approval of the author or publisher, sometimes for bootleg copies or hoping that 'Extra Plates' for the original issue would be included in later issues. These artists included [[William Heath (artist)|William Heath]], [[Alfred Henry Forrester]] ("[[Alfred Crowquill]]"), [[Thomas Onwhyn]] (who sometimes signed as "[[Sam Weller (fictional character)|Sam Weller]]") and [[Thomas Sibson]]. In 1899 [[Joseph Grego]] collected 350 ''Pickwick Paper'' illustrations, including portraits based on stage adaptations, with other notes and commentary in ''Pictorial Pickwickiania''.<ref>''Pictorial Pickwickiania'' .. see External Links</ref>

==''The Pic-Nic Papers''==
In 1841 the three-volume anthology titled ''The Pic-Nic Papers''<ref>''The Pic-Nic Papers'' .. see External links</ref> was published, composed of miscellaneous pieces by various authors. It was originated by Dickens to benefit the widow and children of 28-year-old publisher [[John Macrone]], who died suddenly in 1837. Dickens had begun soliciting submissions in 1838, and he eventually contributed the "Introduction" and one short story "The Lamplighter's Story". Other contributors included [[William Harrison Ainsworth]], [[Thomas Moore]], [[Leitch Ritchie]] and [[Agnes Strickland]]. Macrone's widow eventually received 450 pounds from this charitable publication.<ref>Paul Schlicke. ''Oxford Reader's Companion to Dickens''. ISBN 0-19-866253-X – page 455-56</ref>

==Models==
Mary Weller, Charles Dickens's nurse, recalling her famous charge's occupations as a child, said: "Little Charles was a terrible boy to read".<ref>Collins, Philip, "Dickens: Interviews and Recollections", (1981), p. 2. Palgrave Macmillan.</ref>

"In the young Charles Dickens's reading we have in some ways the very core of his novels...the young Charles came upon the great [[picaresque novel]]s of the eighteenth century — ''[[Roderick Random]]'', ''[[Peregrine Pickle]]'', ''[[Humphrey Clinker]]'', ''[[The History of Tom Jones, a Foundling|Tom Jones]]'', ''[[The Vicar of Wakefield]]'', their French counterpart ''[[Gil Blas]]'', and their great predecessor ''[[Don Quixote]]''. Don Quixote's connection with Mr Pickwick, as [[Fyodor Dostoyevsky|Dostoyevsky]] saw, is basic. With Don Quixote, of course, goes [[Sancho Panza]], who with the reinforcement of the faithful, shrewd, worldly servants of the young heroes Tom Jones, Peregrine Pickle, Roderick Random and the rest, goes to make up Sam Weller."<ref>''The World of Charles Dickens'' [[Angus Wilson]] ISBN 0-14-003488-9</ref>

==Influences and Legacy==

The popularity of ''The Pickwick Papers'' spawned many imitations and sequels in print as well as actual Clubs and Societies inspired by the club in the novel. One example is the still in operation Pickwick Bicycle Club in London, which was established in 1870, the same year as Charles Dickens' death.<ref>{{cite web|title=The Pickwick Bicycle Club|url=http://www.pickwickbc.org.uk/|website=The Pickwick Bicycle Club|accessdate=9 December 2016}}</ref> Other clubs, groups, and societies operating under the name "The Pickwick Club" have existed since the original publication of the story. 

In 1837, Charles Dickens wrote to William Howison about the Edinburgh Pickwick Club. Dickens approved of the use of the name and the celebration of the characters and spirit of the novel. He wrote:
::"If a word of encouragement from me, can as you say endow you with double life, you will be the most lively club in all the Empire, from this time; for every hearty wish that I can muster for your long-continued welfare and prosperity, is freely yours. Mr Pickwick's heart is among you always." <ref name="Pilgrim Letters 2">{{cite book|editor1-last=House|editor1-first=Madeline|editor2-last=Storey|editor2-first=Graham|title=The Letters of Charles Dickens Volume 1|date=1965|publisher=The Clarendon Press|location=Oxford|pages=346-347}}</ref> 

Other known clubs include one meeting as early as December 1836 in the East of London and another meeting at the Sun Tavern in Long-acre in London. Dickens wrote to the secretary of the latter club in 1838 about attending a meeting:
::"If the dinner of the Pickwick Club had been on Monday Week, I would have joined it with all the pleasure which you will readily imagine this most gratifying recollection of my works by so many gentlemen, awakens in my mind." <ref name="Pilgrim Letters">{{cite book|editor1-last=House|editor1-first=Madeline|editor2-last=Storey|editor2-first=Graham|title=The Letters of Charles Dickens Volume 1|date=1965|publisher=The Clarendon Press|location=Oxford|page=398}}</ref> 

In many Pickwick Clubs, members even take on the names of the characters in the novel. As the website for the Pickwick Bicycle Club states, "Our rules state that 'Each Member shall adopt the sobriquet allocated by the Management Committee, being the name of some male character in the Pickwick Papers, and be addressed as such at all meetings of the Club."<ref>[http://www.pickwickbc.org.uk/Sobriquets.html /Pickwick Bicycle Club]
</ref>
Imitations/plagiarisms published at the same time as Dickens's ''Pickwick Papers'' include [[G. W. M. Reynolds]]' ''Pickwick Abroad; or, The Tour in France'' 
<ref>''Pickwick Abroad'' here: [https://archive.org/details/pickwickabroadco00reyn Pickwick Abroad]</ref>

==See also==
{{portal|Charles Dickens}}
*[[Pickwickian syndrome]]
*The [[George and Vulture]]
*[[The Spaniards Inn]]
*[[The Moosepath League]] books of Van Reid are a tribute to the ''Pickwick Papers'' with thoroughly Pickwickian characters. In chapter four of ''Cordelia Underwood'', Cordelia finds a copy of the ''Pickwick Papers'' in her uncle's chest.
*[[Wellerism]]

==References==
{{reflist|30em}}
* {{Nuttall|title=Dickens, Charles}}

==External links==
*{{Librivox book | title=The Pickwick Papers | author=Charles Dickens}}
{{Commons category|The Pickwick Club}}
{{Wikiquote|Pickwick Papers}}
{{Wikisource|The Pickwick Papers}}

'''Source editions online'''
{{Gutenberg|no=580|name=The Pickwick Papers}}.
*[http://etext.library.adelaide.edu.au/d/dickens/charles/d54pp/ ''The Pickwick Papers''], HTML version
*[https://archive.org/details/posthumouspapers021837dick ''The posthumous papers of the Pickwick Club by Charles Dickens; with forty-three illustrations, by R. Seymour and Phiz.''], 1st edition at the [[Internet Archive]]

'''Other online books'''
*[[Charles Dickens|Dickens, Charles]]. [https://archive.org/search.php?query=title%3Apic-nic%20creator%3Adickens%20AND%20mediatype%3Atexts  ''The Pic-Nic Papers'']. London, H. Colburn, 1841.
*[[Percy Hetherington Fitzgerald|Fitzgerald, Percy Hetherington]]. [https://archive.org/details/historyofpickwic00fitziala ''The history of Pickwick; an account of its characters, localities, allusions and illustrations, with a bibliography'']. London: Chapman and Hall. 1891.
*[[Percy Hetherington Fitzgerald|Fitzgerald, Percy Hetherington]]. [https://archive.org/details/pickwickianmanne00fitzuoft ''Pickwickian manners and customs'']. Westminster: Roxburghe Press. 1897.
*[[Joseph Grego|Grego, Joseph]]. [https://archive.org/details/pictorialpickwic01greguoft ''Pictorial Pickwickiana; Charles Dickens and his illustrators. With 350 drawings and engravings'' Volume 1] and [https://archive.org/details/pictorialpickwic02greguoft Volume 2]. London Chapman and Hall. 1899.
*[[Charles Montague Neale|Neale, Charles Montague]]. [https://archive.org/details/anindextopickwic00nealuoft ''An index to Pickwick''], 1897. Index of words, characters, places.
*Matz, B. W. ''[https://archive.org/details/innstavernsofpic00matz The inns & taverns of "Pickwick"]'' (C. Scribner's Sons, 1921).

'''Resources'''
*[http://dickens.ucsc.edu/universe/PP_biblio.html ''The Pickwick Papers'' Bibliography], from the Dickens Project at the U of CA
*[http://www.victorianweb.org/authors/dickens/pickwick/index.html ''The Pickwick Papers''], from The Victoria Web
*[http://personales.ya.com/quijotepickwick/English.html ''Similitudes between Pickwick Papers and Don Quixote''], from 'Dickens Quarterly'
*[https://www.chesterton.org/pickwick-papers/ An essay on ''The Pickwick Papers''] by [[G. K. Chesterton]]

{{Charles Dickens}}
{{The Pickwick Papers}}

{{Authority control}}

{{DEFAULTSORT:Pickwick Papers, The}}
[[Category:1836 novels]]
[[Category:Chapman & Hall books]]
[[Category:Debut novels]]
[[Category:English novels]]
[[Category:Novels by Charles Dickens]]
[[Category:Novels first published in serial form]]
[[Category:Novels set in the 1820s]]
[[Category:The Pickwick Papers|*]]
[[Category:British novels adapted into plays]]
[[Category:Novels adapted into television programs]]
[[Category:British novels adapted into films]]
[[Category:Novels adapted into radio programs]]