{{Use dmy dates|date=January 2016}}
{{featured article}}
{{Infobox islands
| name                              = Surtsey
| image name                        = Surtsey eruption 1963.jpg
| image caption                     = Surtsey, sixteen days after the onset of the eruption
| image size                        =
| map = Topographic map of Surtsey-fr.svg
| map_caption = Map of Surtsey
| image_flag                        = 
| flag_alt                          =
| image_shield                      = 
| native name                       = 
| native name link                  = 
| sobriquet                         =
| location                          = Atlantic Ocean
| coordinates                       = 
| archipelago                       = [[Vestmannaeyjar]]
| total islands                     = 
| major islands                     = 
| area km2                          = 1.4
| area footnotes                    = 
| rank                              =
| length km                         = 
| length footnotes                  = 
| width km                          = 
| width footnotes                   = 
| coastline km                      =
| highest mount                     = 
| elevation m                       = 155
| Country heading                   =
| country                           = Iceland
| country admin divisions title     = 
| country admin divisions           = 
| country admin divisions title 1   = 
| country admin divisions 1         = 
| population                        = 
| population as of                  = 
| density km2                       =
| ethnic groups                     =
| additional info                   = {{designation list | embed=yes
   | designation1 = WHS
   | designation1_offname = Surtsey
   | designation1_date = 2008 <small>(32nd [[World Heritage Committee|session]])</small>
   | designation1_type = Natural
   | designation1_criteria = ix
   | designation1_number = [http://whc.unesco.org/en/list/1267 1267]
   | designation1_free1name = State Party
   | designation1_free1value = [[Iceland]]
   | designation1_free2name = Region
   | designation1_free2value = [[List of World Heritage Sites in Europe|Europe and North America]]
}}
| website = 
}}
{{Location map | Iceland
| width      = 
| float      = 
| border     = 
| caption    = Location of Surtsey in Iceland.
| alt        = 
| relief     = yes
| AlternativeMap = 
| label      = Surtsey
| label_size = 
| position   = 
| background = 
| mark       = 
| marksize   = 
| link       = 
| lat_deg    = 63.303
| lon_deg    = -20.605
}}

'''Surtsey''' ("[[Surtr]]'s island" in [[Icelandic language|Icelandic]], pronounced {{IPA|/ˈsʏr̥tsei/}}) is a [[volcano|volcanic]] island located in the [[Vestmannaeyjar]] [[archipelago]] off the southern coast of [[Iceland]]. At {{Coord|63.303|N|20.605|W|region:IS_type:isle|display=inline,title}}, Surtsey is the [[Extreme points of Iceland|southernmost point of Iceland]]. It was formed in a [[volcanic eruption]] which began 130&nbsp;metres (426&nbsp;ft) below sea level, and reached the surface on 14 November 1963. The eruption lasted until 5 June 1967, when the island reached its maximum size of {{convert|2.7|km2|abbr=on}}. Since then, wave erosion has caused the island to steadily diminish in size: {{As of|2012|lc=y}}, its surface area was {{convert|1.3|km2|abbr=on}}.<ref>{{citation|publisher=Iceland Rewiew online|url=http://icelandreview.com/news/2013/08/13/surtsey-island-50-percent-original-size|title=Surtsey Island 50 Percent Original Size
|date=2013-08-13}}</ref> The most recent survey (2007) shows the island's maximum elevation at {{convert|155|m|ft|abbr=on}} above sea level.<ref name="Vésteinsson">
{{citation
 | first = Árni
 | last = Vésteinsson
 | title = Surveying and charting the Surtsey area from 1964 to 2007
 | journal = Surtsey Research Progress Report XII
 | page = 52 (Figure 11)
 | publisher = The Surtsey Research Society
 | year = 2009
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_XII.htm
 | accessdate =2014-08-15}}</ref>

The new island was named after ''[[Surtr]]'', a fire ''[[jötunn]]'' or giant from [[Norse mythology]].<ref>{{Citation|title=Folk och länder, Norden| year=1986 |publisher=Bokorama |location=Höganäs |isbn=91-7024-256-9 |pages=38 |editor=Time-Life books}}</ref> It was intensively studied by [[Volcanology|volcanologists]] during its eruption, and afterwards by [[botany|botanists]] and other [[biology|biologists]] as life forms gradually colonised the originally barren island. The undersea vents that produced Surtsey are part of the ''[[Vestmannaeyjar]]'' submarine volcanic system, part of the fissure of the sea floor called the [[Mid-Atlantic Ridge]]. Vestmannaeyjar also produced the famous eruption of ''[[Eldfell]]'' on the island of [[Heimaey]] in 1973. The eruption that created Surtsey also created a few other small islands along this volcanic chain, such as ''[[Jólnir]]'' and unnamed other peaks. Most of these eroded away fairly quickly.

==Geology==

===Formation===

{| class="wikitable"
|+'''Scheme of the [[Surtseyan eruption]]'''
|-
| rowspan="9"| [[File:Surtseyan Eruption-numbers.svg|200px]]
|1: Water vapour cloud
|-
|2: Cypressoid ash jet
|-
|3: Crater
|-
|4: Water
|-
|5: Layers of lava and ash
|-
|6: Stratum
|-
|7: Magma conduit
|-
|8: Magma chamber
|-
|9: Dike
|}

The eruption was unexpected, and almost certainly began some days before it became apparent at the surface. The sea floor at the eruption site is 130&nbsp;metres (426&nbsp;ft) below sea level, and at this depth volcanic emissions and explosions would be suppressed, quenched and dissipated by the water pressure and density. Gradually, as repeated flows built up a mound of material that approached sea level, the explosions could no longer be contained, and activity broke the surface.<ref name="Decker">
{{citation
 | last = Decker
 | first = Robert
 | last2 = Decker
 | first2 = Barbara
 | title = Volcanoes
 | publisher = Freeman
 | year = 1997
 | location = New York
 | isbn = 0-7167-3174-6 }}</ref>

The first noticeable indications of volcanic activity were recorded at the seismic station in [[Kirkjubæjarklaustur]], Iceland from 6 to 8 November, which detected weak tremors emanating from an [[epicentre]] approximately west-south-west at a distance of {{convert|140|km|mi|abbr=on}}, the location of Surtsey. Another station in [[Reykjavík]] recorded even weaker tremors for ten hours on 12 November at an undetermined location, when seismic activity ceased until 21 November.<ref name="Sigtryggson">
{{citation
 | first = Hlynur
 | last = Sigtryggsson
 | last2 = Sigurðsson
 | first2 = Eiríkur
 | title = Earth Tremors from the Surtsey Eruption 1963–1965: a preliminary survey
 | journal = Surtsey Research Progress Report II
 | pages = 131–138
 | publisher = The Surtsey Research Society
 | year = 1966
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_II.htm
 | accessdate =2008-07-08}}</ref> That same day, people in the coastal town of [[Vík]] {{convert|80|km|mi|abbr=on}} away noticed a smell of [[hydrogen sulfide|hydrogen sulphide]].<ref name="Decker"/> On 13 November, a fishing vessel in search of herring, equipped with sensitive thermometers, noted sea temperatures {{convert|3.2|km|mi|1|abbr=on}} SW of the eruption center were {{convert|2.4|C-change|F-change}} higher than surrounding waters.<ref>{{citation
 | first = Svend-Aage
 | last = Malmberg
 | title = The temperature effect of the Surtsey eruption: a report on the sea water
 | journal = Surtsey Research Progress Report I
 | pages = 6–9
 | publisher = The Surtsey Research Society
 | year = 1965
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_I.htm
 | accessdate =2008-07-08}}</ref>

====Eruption at the surface====
[[File:Surtsey eruption 2.jpg|thumb|right|250px|Surtsey's ash column rises over the newly forming island]]

At 07:15 [[Greenwich Mean Time|UTC]] on 14 November 1963, the cook of ''Ísleifur II'', a trawler sailing these same waters spotted a rising column of dark smoke southwest of the boat. The captain thought it might have been a boat on fire, and ordered the vessel to investigate. Instead, they encountered explosive eruptions giving off black columns of ash, indicating that a volcanic eruption had begun to penetrate the surface of the sea.<ref name="Decker"/> By 11:00 the same day, the [[eruption column]] had reached several kilometres in height. At first the eruptions took place at three separate vents along a northeast by southwest trending [[fissure]], but by the afternoon the separate eruption columns had merged into one along the erupting fissure. Over the next week, explosions were continuous, and after just a few days the new island, formed mainly of [[scoria]], measured over 500&nbsp;metres (1640&nbsp;ft) in length and had reached a height of 45&nbsp;metres (147&nbsp;ft).<ref name="Thorarinsson I">
{{citation
 | first = Sigurður
 | last = Þórarinsson
 |author-link=Sigurður Þórarinsson
 | title = The Surtsey eruption: Course of events and the development of the new island.
 | journal = Surtsey Research Progress Report I
 | pages = 51–55
 | publisher = The Surtsey Research Society
 | year = 1965
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_I.htm
 | accessdate =2008-07-08}}</ref>

As the eruptions continued, they became concentrated at one vent along the fissure and began to build the island into a more circular shape. By 24 November, the island measured about 900&nbsp;metres by 650&nbsp;metres (2950 by 2130&nbsp;ft). The violent explosions caused by the meeting of [[lava]] and sea water meant that the island consisted of a loose pile of volcanic rock ([[scoria]]), which was eroded rapidly by North [[Atlantic]] storms during the winter. However, eruptions more than kept pace with wave erosion, and by February 1964, the island had a maximum diameter of over 1300&nbsp;metres (4265&nbsp;ft).<ref name="Decker" />

The explosive [[Phreatic eruption|phreatomagmatic]] eruptions caused by the easy access of water to the erupting vents threw rocks up to a kilometre (0.6&nbsp;mi) away from the island, and sent [[Volcanic ash|ash]] clouds as high as 10&nbsp;km (6&nbsp;mi) up into the [[Earth's atmosphere|atmosphere]]. The loose pile of unconsolidated [[tephra]] would quickly have been washed away had the supply of fresh magma dwindled, and large clouds of dust were often seen blowing away from the island during this stage of the eruption.<ref name="Decker" />

The new island was named after the fire [[jötunn]] [[Surtur]] from [[Norse mythology]] (''Surts'' is the [[genitive case]] of ''Surtur'', plus [[:wikt:ey|-ey]], ''island''). Three French journalists representing the magazine ''[[Paris Match]]'' notably landed there on 6 December 1963, staying for about 15&nbsp;minutes before violent explosions encouraged them to leave. The journalists jokingly claimed French [[sovereignty]] over the island, but Iceland quickly asserted that the new island belonged to it.<ref>{{citation|last=Doutreleau|first=Vanessa|title=Surtsey, naissances d'une île|journal=Ethnologie française|publisher=Presses Universitaires de France|volume=XXXVII|issn=0046-2616|isbn=2-13-055455-5|pages=421–33|year=2006|url=http://www.cairn.info/article.php?ID_REVUE=ETHN&ID_NUMPUBLIE=ETHN_063&ID_ARTICLE=ETHN_063_0421|accessdate=2008-07-08|doi=10.3917/ethn.063.0421|issue=3|language=fr}}</ref>

====Permanent island====
By early 1964, though, the continuing eruptions had built the island to such a size that sea water could no longer easily reach the vents, and the volcanic activity became much less explosive. Instead, [[lava fountain]]s and [[lava flow|flows]] became the main form of activity. These resulted in a hard cap of extremely erosion-resistant rock being laid down on top of much of the loose volcanic pile, which prevented the island from being washed away rapidly. Effusive eruptions continued until 1965, by which time the island had a surface area of {{convert|2.5|km2|abbr=on}}.<ref name="Decker" />

On 28 December 1963, submarine activity 2.5&nbsp;km (1.5&nbsp;mi) to the northeast of Surtsey caused the formation of a ridge 100&nbsp;m (328&nbsp;ft) high on the sea floor. This [[seamount]] was named [[Surtla]], but never reached sea level. Eruptions at Surtla ended on 6 January 1964, and it has since been eroded from its minimum depth of 23&nbsp;m (75&nbsp;ft) to 47&nbsp;m (154&nbsp;ft) below sea level.<ref name="Norrman">
{{citation
 | first = John
 | last = Norrman
 | last2 = Erlingsson
 | first2 = Ulf
 |author-link1=John O. Norrman
 | title = The submarine morphology of Surtsey volcanic group
 | journal = Surtsey Research Progress Report X
 | pages = 45–56
 | publisher = The Surtsey Research Society
 | year = 1992
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_X.htm
 | accessdate =2008-07-08}}</ref>

====Subsequent volcanic activity====
[[File:Surtsey craters.jpg|left|thumb|250px|The eruption vents in 1999]]

In 1965, the activity on the main island diminished, but at the end of May that year an eruption began at a vent 0.6&nbsp;km (0.37&nbsp;mi) off the northern shore. By 28 May, an island had appeared, and was named [[Syrtlingur]] (Little Surtsey). The new island was washed away during early June, but reappeared on 14 June. Eruptions at Syrtlingur were much smaller in scale than those that had built Surtsey, with the average rate of emission of volcanic materials being about a tenth of the rate at the main vent. Activity was short-lived, continuing until the beginning of October 1965, by which time the islet had an area of {{convert|0.15|km2|abbr=on}}. Once the eruptions had ceased, wave erosion rapidly wore the island away, and it disappeared beneath the waves on 24 October.<ref name ="Thorarinsson II">{{citation
 | first = Sigurður
 | last = Þórarinsson
 | title = The Surtsey eruption: course of events and the development of Surtsey and other new islands
 | journal = Surtsey Research Progress Report II
 | pages = 117–23
 | publisher = The Surtsey Research Society
 | year = 1966
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_II.htm
 | accessdate =2008-07-08}}</ref>

During December 1965, more submarine activity occurred 0.9&nbsp;km (0.56&nbsp;mi) southwest of Surtsey, and another island was formed. It was named [[Jólnir]], and over the following eight months it appeared and disappeared several times, as wave erosion and volcanic activity alternated in dominance. Activity at Jólnir was much weaker than the activity at the main vent, and even weaker than that seen at Syrtlingur, but the island eventually grew to a maximum size of 70&nbsp;m (230&nbsp;ft) in height, covering an area of {{convert|0.3|km2|abbr=on}}, during July and early August 1966. Like Syrtlingur, though, after activity ceased on 8 August 1966, it was rapidly eroded, and dropped below sea level during October 1966.<ref name="Thorarinsson III">{{citation
 | first = Sigurður
 | last = Þórarinsson
 | title = The Surtsey eruption: course of events during the year 1966
 | journal = Surtsey Research Progress Report III
 | pages = 84–90
 | publisher = The Surtsey Research Society
 | year = 1967
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_III.htm
 | accessdate =2008-07-08}}</ref>

Effusive eruptions on the main island returned on 19 August 1966, with fresh lava flows giving it further resistance to erosion. The eruption rate diminished steadily, though, and on 5 June 1967, the eruption ended. The volcano has been dormant ever since. The total volume of lava emitted during the three-and-a-half-year eruption was about one cubic kilometre (0.24&nbsp;cu&nbsp;mi), and the island's highest point was 174&nbsp;metres (570&nbsp;ft) above sea level at that time.<ref name="Decker" />

Since the end of the eruption, erosion has seen the island diminish in size. A large area on the southeast side has been eroded away completely, while a sand spit called ''Norðurtangi'' (north point) has grown on the north side of the island. It is estimated that about {{convert|0.024|km3|abbr=on}} of material has been lost due to erosion—this represents about a quarter of the original above-sea-level volume of the island.<ref name="Garvin 2000">
{{citation
 | first = J.B.
 | last = Garvin
 | first2 = R.S.
 | last2 = Williams Jr
 | first3 = J.J.
 | last3 = Frawley
 | first4 = W.B.
 | last4 = Krabill
 | title = Volumetric evolution of Surtsey, Iceland, from topographic maps and scanning airborne laser altimetry
 | journal = Surtsey Research Progress Report XI
 | pages = 127–134
 | publisher = The Surtsey Research Society
 | year = 2000
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_XI.htm
 | accessdate =2008-07-08}}
</ref><ref>{{citation
| title=Surtsey Topography
| publisher= NASA
| date=1998-11-12
| url=http://denali.gsfc.nasa.gov/research/garvin/surtsey.html
| accessdate=2008-07-08}}
</ref> Its maximum elevation has diminished to {{convert|155|m|ft|abbr=on}}.<ref name="Vésteinsson"/>

===Recent development===

[[File:Surtsey from plane, 1999.jpg|thumb|250px|right|The island of Surtsey in 1999]]
[[File:North Spit of Surtsey Jan 2009.jpg|thumb|right|250px|North spit of Surtsey in January 2009]]
Following the end of the eruption, scientists established a grid of benchmarks against which they measured the change in the shape of the island. In the 20&nbsp;years following the end of the eruption, measurements revealed that the island was steadily subsiding and had lost about one metre in height. The rate of subsidence was initially about 20&nbsp;cm (8&nbsp;in) per year but slowed to 1–2&nbsp;cm (0.4–0.8&nbsp;in) a year by the 1990s. It had several causes: settling of the loose [[tephra]] forming the bulk of the volcano, compaction of sea floor [[sediment]]s underlying the island, and downward warping of the [[lithosphere]] due to the weight of the volcano.<ref>{{citation|first=J.G.|last= Moore|first2=Sveinn|last2=Jakobsson|first3=Josef|last3=Holmjarn|year=1992|title=Subsidence of Surtsey volcano, 1967–1991|journal=Bulletin of Volcanology|volume=55|pages=17–24|doi=10.1007/BF00301116|bibcode = 1992BVol...55...17M }}</ref>

The typical pattern of volcanism in the Vestmannaeyjar archipelago is for each eruption site to see just a single eruption, and so the island is unlikely to be enlarged in the future by further eruptions. The heavy seas around the island have been eroding it ever since the island appeared, and since the end of the eruption almost half its original area has been lost. The island currently loses about {{convert|1.0|ha|acre}} of its surface area each year.<ref name="jakobssen3">{{citation|last=Jakobssen|first=Sveinn P.|title=Erosion of the Island|url=http://www.surtsey.is/pp_ens/geo_2.htm|publisher=The Spurtsey Research Society|accessdate=2008-07-08|date=2007-05-06}}</ref>

===Future===

[[File:Small islands in the Vestmannaeyjar archipelago, Iceland.jpg|thumb|right|250px|Other islands in the archipelago show the effects of centuries of erosion]]

This island is unlikely to disappear entirely in the near future. The eroded area consisted mostly of loose [[tephra]], easily washed away. Most of the remaining area is capped by hard lava flows, which are much more resistant to erosion. In addition, complex chemical reactions within the loose tephra within the island have gradually formed highly erosion resistant [[tuff]] material, in a process known as [[Palagonite|palagonitization]]. On Surtsey this process has happened quite rapidly, due to high temperatures not far below the surface.<ref>{{citation|last=Jakobssen|first=Sveinn P.|title=The Formation of Palagonite Tuffs|url=http://www.surtsey.is/pp_ens/geo_4.htm|publisher=The Surtsey Research Society|accessdate=2008-07-08|date=2007-05-06}}</ref>

Estimates of how long Surtsey will survive are based on the rate of erosion seen up to the present day. Assuming that the current rate does not change, the island will be mostly at or below sea level by 2100. However, the rate of erosion is likely to slow as the tougher core of the island is exposed: an assessment assuming that the rate of erosion will slow exponentially suggests that the island will survive for many centuries.<ref name="Garvin 2000" /> An idea of what it will look like in the future is given by the other small islands in the Vestmannaeyjar archipelago, which formed in the same way as Surtsey several thousand years ago, and have eroded away substantially since they were formed.<ref name="jakobssen3" />

==Biology==

===Settlement of life===

A classic site for the study of [[Colonisation (biology)|biocolonisation]] from [[founder population]]s that arrive from outside (''[[allochthon]]ous''), Surtsey was declared a nature reserve in 1965, while the eruption was still in active progress. Today only a few scientists are permitted to land on Surtsey; the only way anyone else can see it closely is from a small plane. This allows the natural [[ecological succession]] for the island to proceed without outside interference. In 2008, [[UNESCO]] declared the island a [[World Heritage Site]], in recognition of its great scientific value.<ref>{{citation|url=http://whc.unesco.org/en/news/453/|title=Twenty-seven new sites inscribed|publisher=UNESCO|accessdate=2015-02-13}}</ref>

====Plant life====

In the spring of 1965,<ref name="surtseyresearchsociety2007">Surtsey Research Society [http://www.surtsey.is/pp_ens/biola_1.htm "Colonization of the Land"] Accessed: 2015-01-23. (Archived by WebCite® at http://www.webcitation.org/6VndTSmcM)</ref> the first [[vascular plant]] was found growing on the northern shore<ref name="csmonitor2008">{{citation
 | title = Iceland's new island is an exclusive club&nbsp;– for scientists only
 | url = http://www.csmonitor.com/The-Culture/2008/1024/iceland-s-new-island-is-an-exclusive-club-for-scientists-only
 | year = 2008
 | last = Blask
 | first = Sara
 | publication-date = 2008-10-24
 | journal = The Christian Science Monitor
}}</ref> of Surtsey, [[moss]]es became visible in 1967, and [[lichen]]s were first found on the Surtsey lava in 1970.<ref name=Burrows>{{citation| title=Processes of Vegetation Change |first=Colin |last=Burrows |year=1990| publisher=Routledge | pages=124–127 | isbn=0-04-580012-X}}</ref> Plant colonisation on Surtsey has been closely studied, the vascular plants in particular as they have been of far greater significance than mosses, lichens and [[fungi]] in the development of vegetation.<ref name=plants/>

Mosses and lichens now cover much of the island. During the island's first 20&nbsp;years, 20&nbsp;species of plants were observed at one time or another, but only 10 became established in the nutrient-poor sandy soil.
As birds began nesting on the island, soil conditions improved, and more vascular plant species were able to survive. In 1998, the first [[Woody plant|bush]] was found on the island—a tea-leaved [[willow]] (''[[Salix phylicifolia]]''), which can grow to heights of up to 4&nbsp;metres (13&nbsp;ft). By 2008, 69&nbsp;species of plant had been found on Surtsey,<ref name="csmonitor2008" /> of which about 30 had become established. This compares to the approximately 490 species found on mainland Iceland.<ref name="csmonitor2008" /> More species continue to arrive, at a typical rate of roughly 2–5&nbsp;new species per year.<ref name=plants>{{citation| url=http://www.vulkaner.no/n/surtsey/esurtplant.html|title= The volcano island: Surtsey, Iceland: Plants| publisher=Our Beautiful World| accessdate=2008-07-08}}</ref>

====Birds====

[[File:Puffin2.jpg|thumb|right|150px|The first [[puffin]] nests were found on Surtsey in 2004]]

The expansion of [[bird]] life on the island has both relied on and helped to advance the spread of plant life. Birds use the plants for nesting material, but also continue to assist in the spreading of seeds, and fertilize the soil with their [[guano]].<ref name=thornton>{{citation|first=Ian |last=Thornton |first2= Tim|last2=New | title=Island Colonization: The Origin and Development of Island Communities |publisher=Cambridge University Press|page=178|isbn=0-521-85484-9|year=2007}}</ref> Birds first began nesting on Surtsey three years after the eruptions ended, with [[fulmar]] and [[guillemot]] the first species to set up home. Twelve species are now regularly found on the island.<ref name=birds>{{citation|last=Petersen|first=Ævar|title=Bird Life on Surtsey|url=http://www.surtsey.is/pp_ens/biola_5.htm|publisher=The Surtsey Research Society|date=2007-05-06|accessdate=2008-07-14}}</ref>

A [[gull]] '''colony''' has been present since 1984, although gulls were seen briefly on the shores of the new island only weeks after it first appeared.<ref name=birds/> The gull colony has been particularly important in developing the plant life on Surtsey,<ref name=thornton/><ref name=birds/> and the gulls have had much more of an impact on plant colonisation than other breeding species due to their abundance. An expedition in 2004 found the first evidence of nesting [[Atlantic puffin]]s,<ref name=birds/> which are extremely common in the rest of the [[archipelago]].<ref>{{citation|url=http://iceland.vefur.is/Iceland_nature/wildlife/puffins.htm|title=Puffins in Iceland|publisher=Iceland on the web|accessdate=2008-07-14}}</ref>

As well as providing a home for some species of birds, Surtsey has also been used as a stopping-off point for [[bird migration|migrating]] birds, particularly those en-route between Europe and Iceland.<ref>{{citation|url=http://www.vulkaner.no/v/volcan/surtsey_e.html|title=Surtsey, Iceland|publisher=Our Beautiful World|accessdate=2008-07-08}}</ref><ref>{{citation|last=Friðriksson|first=Sturla|last2=Magnússon|first2=Borgþór|url=http://www.surtsey.is/pp_ens/biola_1.htm|title=Colonization of the Land|publisher=The Surtsey Research Society|date=2007-05-06|accessdate=2008-07-08}}</ref> Species that have been seen briefly on the island include [[whooper swan]]s, various species of [[geese]], and [[common raven]]s. Although Surtsey lies to the west of the main migration routes to Iceland, it has become a more common stopping point as its vegetation has improved.<ref>{{citation| url=http://www.vulkaner.no/n/surtsey/esurtbird.html| title= The volcano island: Surtsey, Iceland: Birdlife| publisher=Our Beautiful World|accessdate=2008-07-08}}</ref> In 2008, the 14th bird species was detected with the discovery of a common raven's nest.<ref name="csmonitor2008" />

According to a 30 May 2009 report, a [[golden plover]] was nesting on the island with four eggs.<ref>[http://www.icenews.is/index.php/2009/05/30/new-family-moves-onto-surtsey-island-no-parties-allowed/ New family moves onto Surtsey Island, no parties allowed] IceNews, 30 May 2009</ref>

====Marine life====
Soon after the island's formation, [[Pinniped|seals]] were seen around the island. They soon began basking there, particularly on the northern spit, which grew as the waves eroded the island. Seals were found to be breeding on the island in 1983, and a group of up to 70 made the island their breeding spot. [[Grey seal]]s are more common on the island than [[harbour seal]]s, but both are now well established.<ref>{{citation|last=Hauksson|first=Erlingur|year=1992|title=Observations on Seals on Surtsey in the Period 1980–1989|publisher=The Surtsey Research Society|journal=Surtsey Research Progress Report X|url=http://www.surtsey.is/SRS_publ/1992-X/1992_X_2_03.pdf|format=PDF|pages=31–32|accessdate=2008-07-14}}</ref> The presence of seals attracts [[orca]]s, which are frequently seen in the waters around the Vestmannaeyjar archipelago and now frequent the waters around Surtsey.

On the submarine portion of the island, many marine species are found. [[Starfish]] are abundant, as are [[sea urchin]]s and [[limpet]]s. The rocks are covered in [[algae]], and [[seaweed]] covers much of the submarine slopes of the volcano, with its densest cover between 10 and 20&nbsp;metres (33 to 66&nbsp;ft) below sea level.<ref>{{citation| url=http://www.vulkaner.no/n/surtsey/esurtseal.html|title= The volcano Island Surtsey, Iceland: Sealife| publisher=Our Beautiful World|accessdate=2008-07-08}}</ref>

====Other life====

Insects arrived on Surtsey soon after its formation, and were first detected in 1964. The original arrivals were flying insects, carried to the island by winds and their own power. Some were believed to have been blown across from as far away as [[mainland Europe]]. Later insect life arrived on floating [[driftwood]], and both live animals and carcasses washed up on the island. When a large, grass-covered [[tussock (grass)|tussock]] was washed ashore in 1974, scientists took half of it for analysis and discovered 663 land invertebrates, mostly [[mite]]s and [[springtail]]s, the great majority of which had survived the crossing.<ref>
{{citation
 | first = Erling
 | last = Ólafsson
 | title = The development of the land-arthropod fauna on Surtsey, Iceland, during 1971–1976 with notes on terrestrial Oligochaeta
 | journal = Surtsey Research Progress Report VIII
 | pages = 41–46
 | publisher = The Surtsey Research Society
 | year = 1978
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_VIII.htm
 | accessdate =2008-07-08}}</ref>

The establishment of insect life provided some food for birds, and birds in turn helped many species to become established on the island. The bodies of dead birds provide sustenance for [[carnivorous]] insects, while the fertilisation of the soil and resulting promotion of plant life provides a viable habitat for [[herbivorous]] insects.

Some higher forms of land life are now colonising the soil of Surtsey. The first [[earthworm]] was found in a soil sample in 1993, probably carried over from [[Heimaey]] by a bird. However, the next year earthworms were not found. [[Slug]]s were found in 1998, and appeared to be similar to varieties found in the southern Icelandic mainland. Spiders and [[beetle]]s have also become established.<ref>{{citation| url=http://www.vulkaner.no/n/surtsey/esurtinns.html| title= The volcano island: Surtsey, Iceland: Insects| publisher=Our Beautiful World| accessdate=2008-07-08}}</ref><ref>
{{citation
 | first = Hólmfríður
 | last = Sigurðardóttir
 | title = Status of collembolans (Collembola) on Surtsey, Iceland, in 1995 and first encounter of earthworms (lumbricidae) in 1993
 | journal = Surtsey Research XI
 | pages = 51–55
 | publisher = Surtsey Research Committee
 | year = 2000
 | location = Reykjavík, Iceland
 | url = http://www.surtsey.is/pp_ens/report/report_XI.htm
 | accessdate =2008-07-08}}
</ref>

==Human impact==
The only other significant human impact is a small prefabricated hut which is used by researchers while staying on the island. The hut includes a few bunk beds and a solar power source to drive an emergency radio and other key electronics. All visitors check themselves and belongings to ensure no seeds are accidentally introduced by humans to this ecosystem. It is believed that some young boys tried to introduce potatoes, which were promptly dug up once discovered.<ref name="csmonitor2008" /> An improperly handled human defecation resulted in a tomato plant taking root which was also destroyed.<ref name="csmonitor2008" />
In 2009 a weather station for weather observations and a webcam were installed on Surtsey.<ref>[http://www.surtsey.is/index_eng.htm The Surtsey Research Society]</ref>

==See also==
{{Portal|Iceland|Volcanoes}}
*[[Geography of Iceland]]
*[[Iceland plume]]
*[[List of islands of Iceland]]
*[[List of new islands]]
*[[List of volcanoes in Iceland]]
*[[Volcanology of Iceland]]
*[[Nishinoshima (Ogasawara)|Nishinoshima]], a [[Japan]]ese volcanic island that was enlarged in recent decades by a similar process to the formation of Surtsey.

==References==
{{reflist|30em}}

==External links==
{{Commons category}}
*[http://www.surtsey.is/index_eng.htm Website of The Surtsey Research Society]
*[http://en.vedur.is/weather/observations/areas/south/#station=6012 Weather observations on Surtsey]
*[http://en.vedur.is/weather/observations/webcams/surtsey/ Webcam on Surtsey]
*[https://web.archive.org/web/20021216214346/http://volcano.und.nodak.edu/vwdocs/volc_images/europe_west_asia/surtsey.html Aerial photos, maps and volcanic geology of Surtsey.]
*[http://www.visitwestmanislands.com/ Visit Westman Islands]
*[http://denali.gsfc.nasa.gov/research/garvin/surtsey.html Surtsey Topography]
*[http://www.surtsey.is/index_eng.htm The Surtsey Research Society], which administers the island
*[http://www.ust.is/LogOgReglur/Fridlysingar/Fridlond/nr/246 A notice from the Environmental & Food Agency] declaring Surtsey a protected area {{is icon}}
*[http://www.explorenorth.com/library/weekly/aa042601a.htm Explore North site illustrates postage stamps.]
*[http://www.vulkaner.no/v/volcan/surtsey_e.html Extensive information about plant and bird life on the island]
*http://bookingwestmanislands.is/blog/
{{Islands of Iceland}}
{{Volcanoes of Iceland}}

{{Authority control}}

[[Category:1963 in Iceland]]
[[Category:Natural disasters in Iceland]]
[[Category:1963 natural disasters]]
[[Category:20th-century volcanic events]]
[[Category:New islands]]
[[Category:Uninhabited islands of Iceland]]
[[Category:Volcanoes of Iceland]]
[[Category:World Heritage Sites in Iceland]]
[[Category:Active volcanoes]]
[[Category:Vestmannaeyjar]]
'''