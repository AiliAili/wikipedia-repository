{{multiple issues|
{{Orphan|date=March 2014}}
{{BLP sources|date=January 2011}}
}}

'''Bruce Dethlefsen''' is an American [[poet]] and teacher of poetry. He was Wisconsin [[Poet Laureate]] for 2011-2012, having been appointed by Governor [[Jim Doyle|James Doyle]]. He also served as secretary for the Wisconsin Fellowship of Poets for six years.

==Early life and influences==
Bruce Dethlefsen was born to Frank  and Patricia Dethlefsen in 1948.<ref>{{cite book|last1=Greasley|first1=Philip A.|title=Dictionary of Midwestern Literature, Volume 2: Dimensions of the Midwestern Literary Imagination|date=August 8, 2016|publisher=Indiana University Press|location=Bloomington, IN|page=878}}</ref>  Bruce lived in Kansas City with his family until 1966. Frank worked as a grocer.  According to Bruce, his father wanted him to attend college, but was not keen on Bruce’s desire to be a poet, as he did not think it practical.  Bruce recalls the early influence his mother had in encouraging creativity with words and rhythm.  He says,   
<blockquote>I watched my mother, a displaced Okie, in her house-dress dance and sing around the kitchen. She loved to make up songs and little rhymes. The sillier, the better. She gave me permission to mess with words, the sound of words strung and unstrung together. E. E. Cummings also told me it’s okay to play.   Most poets I know read and were read to as children. I remember fondly the words, the pictures, the touching, the warmth of hand and voice as my mother read to me.<ref>{{cite journal|year=2010 |journal=Quill and Parchment |volume=MAR |url=http://archives.quillandparchment.com/March2010/Bruce.html |deadurl=yes |archiveurl=https://web.archive.org/web/20110720033139/http://archives.quillandparchment.com/March2010/Bruce.html |archivedate=2011-07-20 |df= }}</ref></blockquote>

Dethlefsen was aware of his poetic inclinations as a teenager but has later remarked that, “I wish that when I was in 7th or in 8th grade, a man would have gotten up in front of our class and read a poem that I understood and said ‘it’s really ok to do this.’  I think it would have added 10 years to my writing career."<ref>{{cite journal |year=2010 |journal=The Wisconsin Eye | url= http://www.wiseye.org/Programming/VideoArchive/SegmentDetail.aspx?segid=4482
}}</ref>

==Career and education==
Bruce graduated from [[Paseo High School]] in 1966.  He completed his undergraduate degree in [[Secondary education|Secondary Education]] at the [[University of Wisconsin–La Crosse]], where he had initially thought he would attend on a football scholarship, but was injured before graduating from high school.  Throughout college, Bruce worked in the emergency ward of the La Crosse Lutheran Hospital.<ref>{{cite journal |journal=The Wisconsin Eye | url= http://www.wiseye.org/Programming/VideoArchive/SegmentDetail.aspx?segid=4482}}</ref>
He earned his master’s degree in Curriculum and Instruction from the [[University of Wisconsin–Oshkosh]].<ref>{{cite journal
 |date=2008 
 |journal=brucedethlefsen.org 
 |url=http://brucedethlefsen.org/about.html 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20110812215522/http://brucedethlefsen.org/about.html 
 |archivedate=2011-08-12 
 |df= 
}}</ref>

Dethlefsen credits at least two writers for his success in poetry: John Judson, an instructor at UW–La Crosse, and David Steingass, [[artist-in-residence]] in Wisconsin schools.<ref>{{cite journal
 |date=2008 
 |journal=brucedethlefsen.org 
 |url=http://brucedethlefsen.org/about.html 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20110812215522/http://brucedethlefsen.org/about.html 
 |archivedate=2011-08-12 
 |df= 
}}</ref>
Dethlefsen took a position as a school media specialist for the Nekoosa Wisconsin School District.
From there, he became a public [[librarian]], serving as director of the Montello Public Library from 1999 to 2007.  During his time there, he held regular poetry readings that included [[open mic]] sessions.  At these events, Dethlefsen encouraged everyone to share their own poetry.  He also taught poetry workshops there where he helped people to focus and refine their style. During his time as Montello Library Director, he worked closely with the Friends of the Library to offer the once a month Poet Tree event.<ref>{{cite journal |journal=Marquette County Tribune | url= http://www.marquettecountytribune.com/pages/11-18.html}}</ref>

Since retiring, Dethlefsen has increased his attention to his writing and also stays active with a variety of artistic ventures.  He sings and plays [[bass guitar]] with musician Bill Orth as the Obvious Dogs who recorded a CD with Wisconsin Poet Cathryn Cofell.<ref>{{cite journal |journal=Marquette County Tribune | url= http://www.marquettecountytribune.com/pages/11-18.html}}</ref>

==Poet Laureate==
Dethlefsen was appointed to the position of Wisconsin Poet Laureate for 2011-2012. In an interview for the [[Wisconsin Eye]], he says, “Wisconsin needs an ambassador for the arts.  My job will be to go out and promote poets and poetry in Wisconsin.”
<ref>{{cite journal |year=2010 |journal=The Wisconsin Eye | url= http://www.wiseye.org/Programming/VideoArchive/SegmentDetail.aspx?segid=4482
}}</ref>
He intends to use his position as the State Poet Laureate to promote the writing and sharing of poetry throughout Wisconsin.  He plans to encourage and support local cultural and creative talent by organizing local poetry readings in public libraries, schools and coffee houses throughout the State.
<ref>{{cite journal |journal=Wisconsin Poet Laureate Commission| url= http://www.wipoetlaureate.wi.gov}}</ref>

===Creation and abolishment of WI Poet Laureate position ===
Executive Order 404, given by [[Tommy Thompson|Governor Thompson]], created the Commission in on July 31, 2000 to recommend candidates for the Poet Laureate of Wisconsin. Seven members were appointed to 4-year terms. Each of five organizations recommended one person for membership: the Council for Wisconsin Writers, the Wisconsin Fellowship of Poets, the Wisconsin Regional Writers Association, the Wisconsin Humanities Council, and the Wisconsin Arts Board.<ref>{{cite journal | title=WI Executive Orders 1987-2001:  Executive Order 404| url= http://nxt.legis.state.wi.us/nxt/gateway.dll/Administrative%20Code%20Related/execord/3?f=templates$fn=document-frame.htm$3.0$q=$uq=$x=$up=1$nc=9869#LPHit1}}</ref>

On February 4, 2011, The Wisconsin Poet Laureate Commission received a form letter from the office of Governor [[Scott Walker (politician)|Scott Walker]] indicating that the commission would be abolished.<ref>{{cite journal |journal=Library of Congress |issue=1 |pages=415–9| url= http://www.loc.gov/rr/main/poets/wisconsin.html}}</ref>   In May, 2011, the Wisconsin Academy of Sciences, Art and Letters picked up the sponsorship of the Wisconsin Poet Laureate position.

===Artistic inspiration===
Asked how poems come to him, Dethlefsen says, “They float by in the air and I catch them. If I’m not paying attention, they go on by. That’s the most important thing for poets, to pay attention. Listen, watch, regard. Artists must pay close attention.”<ref>{{cite journal |journal=Marquette County Tribune | url= http://www.marquettecountytribune.com/pages/11-18.html
}}</ref>

In an interview for [[Quill and Parchment]], he comments on his writing habits,
<blockquote> I write every day, the fourth thing every morning for the last twelve years or so. Thank you, Julia Cameron, author of The Artist’s Way, for giving me “morning pages” to write. I buy cheap spiral notebooks and use purple ink from my Waterman fountain pen. Sometimes poems wiggle out of these pages. Otherwise, I wait for and try to snatch poem ideas that fly by. My first three or four drafts are done with pen and paper. There’s something very important about that feel, the scratchiness, of the process for me.”<ref>{{cite journal|year=2010 |journal=Quill and Parchment |volume=MAR |url=http://archives.quillandparchment.com/March2010/Bruce.html |deadurl=yes |archiveurl=https://web.archive.org/web/20110720033139/http://archives.quillandparchment.com/March2010/Bruce.html |archivedate=2011-07-20 |df= }}</ref></blockquote>

=== Awards and honors ===
* 2005-2010 Secretary of the Wisconsin Fellowship of Poets
* 2003 Posner Award, Full-Length Poetry, Honorable Mention for ''Something Near the Dance Floor''
* 2010  Outstanding Achievement in Poetry, Wisconsin Library Association for ''Breather''
* 2003, 2009, 2011  Pushcart Prize Nominee

=== Works ===
Poetry Collections
* ''A Decent Reed'', Tamafyhr Mountain Press, 1998
* ''Something Near the Dance Floor'', Marsh River Editions, 2003
* ''Breather'', Fireweed Press, 2009
* ''Unexpected Shiny Things'', Cowfeather Press, 2011
Poems in Journals, Anthologies, and on the Radio
https://web.archive.org/web/20110911080200/http://www.brucedethlefsen.com/pubs.html

==Personal life==
Dethlefsen resides in Westfield, Wisconsin with longtime love and partner, Sue Allen.{{citation needed|date=January 2011}}  Bruce also had a son, Wilson, now deceased.

==See also==
*[[List of U.S. states' Poets Laureate]]

== References ==
{{reflist|colwidth=30em}}

== External links ==
* WI Executive Order 404  http://nxt.legis.state.wi.us/nxt/gateway.dll/Administrative%20Code%20Related/execord/3?f=templates$fn=document-frame.htm$3.0$q=$uq=$x=$up=1$nc=9869#LPHit1
* Library of Congress  http://www.loc.gov/rr/main/poets/wisconsin.html
* Bloomberg Article about Abolishment of WI Poets Laureate Position  http://www.bloomberg.com/news/2011-03-08/poet-laureate-s-2-000-gas-reimbursement-a-casualty-in-walker-s-wisconsin.html
* Bruce Dethlefsen's Website https://web.archive.org/web/20110911075848/http://www.brucedethlefsen.com/
* The Wisconsin Eye (video) http://www.wiseye.org/Programming/VideoArchive/SegmentDetail.aspx?segid=4482
* Wisconsin State Journal  http://host.madison.com/wsj/news/local/article_1f536d72-ed34-11df-b819-001cc4c002e0.html
* Milwaukee JournalSentinel http://dev.www.jsonline.com/newswatch/107062673.html?newsWatchDate=1-7-2011
* Marquette County Tribune http://www.marquettecountytribune.com/pages/11-18.html
* Wisconsin Fellowship of Poets  http://www.wfop.org/
* https://web.archive.org/web/20110812215522/http://brucedethlefsen.org/about.html
* Quill and Parchment  https://web.archive.org/web/20110720033139/http://archives.quillandparchment.com/March2010/Bruce.html
* Library of Congress  http://www.loc.gov/rr/main/poets/wisconsin.html
* UW Oshkosh Today  http://www.uwosh.edu/today/7955/alumnus-named-wisconsin-poet-laureate/
* WI Blue Book  https://web.archive.org/web/20110602060428/http://legis.wisconsin.gov:80/lrb/bb/09bb/
* Bloomberg  http://www.bloomberg.com/news/2011-03-08/poet-laureate-s-2-000-gas-reimbursement-a-casualty-in-walker-s-wisconsin.html

{{Authority control}}

{{DEFAULTSORT:Dethlefsen, Bruce}}
[[Category:Living people]]
[[Category:1948 births]]
[[Category:Poets Laureate of Wisconsin]]
[[Category:American male poets]]
[[Category:University of Wisconsin–La Crosse alumni]]
[[Category:University of Wisconsin–Oshkosh alumni]]