{{Infobox journal
| title = Harvard Theological Review
| cover = [[File:Harvard Theological Review 2012 cover.jpg]]
| discipline = [[Religious Studies]]
| abbreviation = Harv. Theol. Rev.<br />(HTR)
| editor = [[Jon D. Levenson]], [[Kevin Madigan]]
| publisher = [[Cambridge University Press]] on behalf of the [[Harvard Divinity School]]
| country = United States
| history = 1908-present
| frequency = Quarterly
| website = http://journals.cambridge.org/htr
| link1 = http://journals.cambridge.org/action/displayIssue?jid=HTR&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=HTR
| link2-name = Online archive
| ISSN = 0017-8160
| eISSN = 1475-4517
| JSTOR =
| OCLC = 803348474
| LCCN = 09003793
}}
The '''''Harvard Theological Review''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] established in 1908<ref>{{cite web|url=http://www.hds.harvard.edu/faculty-research/research-publications/harvard-theological-review |title=Harvard Theological Review &#124; Harvard Divinity School |publisher=Hds.harvard.edu |date=2012-09-20 |accessdate=2012-09-27}}</ref> and published by  [[Cambridge University Press]] on behalf of the [[Harvard Divinity School]]. It covers a wide spectrum of fields in [[Theology|theological]] and [[religious studies]]; its range is not limited to any one [[Religion|religious tradition]] or set of traditions. The [[editors-in-chief]] are [[Jon D. Levenson]] and [[Kevin Madigan]] (Harvard Divinity School).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[IBZ International Bibliography of Periodical Literature]]
* Rambi (Index of Articles on Jewish Studies)
* [[FRANCIS]]
* [[Arts and Humanities Citation Index]]
* [[International Medieval Bibliography]]
* [[MLA Bibliography]]
* [[Index to Jewish Periodicals]]
}}

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.hds.harvard.edu/faculty-research/research-publications/harvard-theological-review}}

[[Category:Harvard Divinity School]]
[[Category:Harvard University academic journals]]
[[Category:Religious studies journals]]
[[Category:Quarterly journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1908]]
[[Category:1908 establishments in Massachusetts]]


{{reli-journal-stub}}