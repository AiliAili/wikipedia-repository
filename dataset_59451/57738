The '''International Society on General Relativity and Gravitation''' (ISGRG) is a learned society established in 1971<ref>{{cite web|url=http://www.isgrg.org/history.php|title=The International Society on General Relativity and Gravitation}}</ref> with the goal to promote research on [[general relativity]] (GR) and gravitation.  To that end, it encourages communication between relativity researchers, in particular by organizing the triennial international GR conferences, sponsoring the Hyperspace website, and publishing the journal ''[[General Relativity and Gravitation]]''. The society also serves as the Affiliated Commission 2 (AC.2) of the [[International Union of Pure and Applied Physics]].

The society's president is always a prominent gravitational physicist. In the past, the office has been held by [[Christian Møller]] (1971–74), [[Nathan Rosen]] (1974–77), [[Peter Bergmann]] (1977–80), [[Yvonne Choquet-Bruhat]] (1980–83), [[Dennis Sciama]] (1983–86), [[Ezra Ted Newman]] (1986–89), [[George Francis Rayner Ellis|George Ellis]] (1989–92), [[Roger Penrose]] (1992–95), [[Jürgen Ehlers]] (1995–98), [[Werner Israel]] (1998–2001), [[Robert Wald]] (2001–04), [[Clifford Will]] (2004–07), [[Abhay Ashtekar]] (2007–10), and Malcolm A. H. MacCallum (2010–13). The current president is [[Gary Horowitz]].

The society awards two thesis prizes every three years.  One is the Bergmann-Wheeler prize for an outstanding PhD thesis in the broad area of quantum gravity. The other is the  Jürgen Ehlers prize for an outstanding PhD thesis in numerical or mathematical relativity. It also awards the International Union of Pure and Applied Physics Young Scientist Prize in Gravitational Physics every year.

==References==
{{Reflist}}

==External links==
*[http://www.isgrg.org/ ISGRG website]

[[Category:International scientific organizations]]
[[Category:Physics societies]]
[[Category:Scientific organizations established in 1971]]


{{physics-org-stub}}