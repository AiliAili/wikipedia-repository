
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Alpi Pioneer 400
 | image=Alpi Aviation Pioneer 400 Quattrocento Airfield Bonn-Hangelar 20090822.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=Fixed wing single engine
 | national origin=[[Italy]]
 | manufacturer= [[Alpi Aviation]]
 | designer=
 | first flight=Early 2013
 | introduced=
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=19 by November 2014
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Alpi Pioneer 200]], [[Alpi Pioneer 300]]
 | variants with their own articles=
}}
|}

The '''Alpi Pioneer 400''' is an [[Italy|Italian]] four-seat [[ultralight aircraft|ultralight]] and [[light-sport aircraft]], designed produced by [[Alpi Aviation]], of [[Pordenone]]. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.

==Design==
The Alpi Pioneer series of light aircraft, [[Alpi Pioneer 200|200]], [[Alpi Pioneer 300|300]] and 400 are developments of the [[Vidor Champion V]] or Asso V, designed by [[Giuseppe Vidor]] and renamed Gamma Pioneer, of which, the Gamma Pioneer 400 is the largest seating four. ''Quattrocentro'' (four hundred) is often included in the name.<ref name=JAWA16/>

The Pioneer 400 is a low wing monoplane, with a trapezoidal plan wing that has sweep only on the trailing edge and winglets at its tips. The wing structure is wooden with a single box-spar and is covered with pre-preg carbon fibre. Ailerons occupy about half the span; inboard, slotted flaps fill the rest. Both ailerons and flaps are fabric covered.<ref name=JAWA16/>

The fuselage and empennage have a wooden structure and carbon fibre skin. The engine is 73.5 kW (98.6 hp) Rotax 912S flat-four, driving a variable pitch propeller. The cabin is over the wings, with two pairs of side-by-side seats. Entry is by two upward-opening doors and there is a baggage space behind the rear seats. At the rear the vertical surfaces are straight-edged and swept, with a long dorsal strake. The horizontal surfaces are approximately trapezoidal in plan, with an unswept leading edge.Both the rudder and elevators are fabric covered. The rudder is balanced and both it and the port elevator carry trim tabs.<ref name=JAWA16/>

The Pioneer has retractable tricycle landing gear though the nose-wheel remains partially exposed when retracted. An emergency ballistic parachute is an option.<ref name=JAWA16/>

==Development==

The date of the first flight of the Pioneer 400 is not known but it first appeared in public at the 2009 [[Friedrichshafen]] airshow held 2-5 April.<ref name=JAWA16/>  Though briefly registered in Italy, by 11 June 2009 the first prototype had been re-registered in the [[UK]] as ''G-CGAJ''.<ref name=GINFO/>  It received type approval in late 2010 and full approval in August 2012.<ref name=JAWA16/>

Three distinct variants, each available as a kit or a flyaway aircraft, have been marketed.  The Ultralight has a maximum take-off weight (MTOW) of {{convert|450|kg|lb|abbr=on}} to fulfil the European Ultralight limit. Another variant, designated Experimental has an empty weight increased by {{convert|90|kg|lb|abbr=on}} and an MTOW of {{convert|750|kg|lb|abbr=on}}. The Pioneer 400T has the same MTOW as the Experimental but is powered by a [[turbo-charged]] {{convert|84.6|kW|hp|abbr=on}} [[Rotax 914]] [[flat-four engine|flat-four]] which raises the cruising speed to {{convert|256|km/h|mph kn|abbr=on}}.<ref name=JAWA16/>

Nineteen Gamma Pioneer 400s had been built by November 2014.<ref name=JAWA16/>

==Variants==
; Gamma Pioneer Ultralight: MTOW {{convert|450|kg|lb|abbr=on}} to Euro UL limit.
; Gamma Pioneer Experimental: MTOW {{convert|750|kg|lb|abbr=on}} to Euro Experimental limit.
; Gamma Pioneer 400T: as Experimental but with uprated [[Rotax 914]] engine.  

==Specification (400 Ultralight)==
[[File:VH-LPI Alpi Aviation Pioneer 400 Quattrocento (7083276893).jpg|thumb]]
{{Aircraft specs
|ref=Jane's All the World's Aircraft 2016/17<ref name=JAWA16/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=Three passengers
|length m=7.00
|length note=
|span m=8.80
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=11.20
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=290
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=450
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912 ULS]]
|eng1 type= [[flat-four]] [[aircraft engine]]
|eng1 kw=73.2
|eng1 note=
|more power=

|prop blade number=2
|prop name=[[propeller (aircraft)#Variable pitch|variable pitch]]
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=250
|max speed note=
|cruise speed kmh=220
|cruise speed note=
|stall speed kmh=67
|stall speed note=
|never exceed speed kmh=296
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=900
|range note=at 75% power
|endurance=<!-- if range unknown -->
|ceiling m=6000
|ceiling note=service
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=13 best
|climb rate ms=7.62
|climb rate ftmin=
|climb rate note=maximum, at sea level
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
*'''Take-off and landing runs:''' {{convert|120|m|ft|abbr=on}}

|avionics=
*[[EFIS]]: Dyson EFIS-D100 and FlightDEK-D-180
}}

==References==
{{Reflist|refs=

<ref name=JAWA16>{{cite book |title= Jane's All the World's Aircraft  : development & production : 2016-17|last=Gunston |first=Bill |coauthors= |edition= |year=2016|publisher=IHS Global|location=|isbn= 978-0-7106-3177-0 |pages=427-8}}</ref>

<ref name=GINFO>{{cite web |url=http://publicapps.caa.co.uk/modalapplication.aspx?catid=1&pagetype=65&appid=1&mode=detailnosummary&fullregmark=CGAJ|title=CAA UK civil register|accessdate=29 April 2016}}</ref>

}}

==External links==
{{Commons category|Alpi Pioneer 400}}
{{official website|http://www.alpiaviation.com/it/models.htm||name=Alpi Aviation website}}

{{Alpi Aviation aircraft}}

[[Category:Italian ultralight aircraft 2000–2009]]
[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Alpi Aviation aircraft]]