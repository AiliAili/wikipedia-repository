{{For|his son|Edward Albert}}
{{Infobox person
| name = Eddie Albert
| image = Eddie Albert Robert Wagner Switch 1975 (cropped).JPG
| caption = Albert in 1975
| birth_name = Edward Albert Heimberger
| birth_date = {{birth date|1906|4|22}}
| birth_place = [[Rock Island, Illinois]], U.S.
| death_date = {{death date and age|2005|5|26|1906|4|22}}
| death_place = {{nowrap|[[Pacific Palisades, Los Angeles]], U.S.}}
| death_cause = [[Pneumonia]]
| resting_place = [[Westwood Village Memorial Park Cemetery]], Los Angeles, California
| home_town = [[Minneapolis]], [[Minnesota]]
| parents  = Frank Daniel-Heimberger<br>Julia Jones
| alma_mater = [[University of Minnesota]]
| years_active = 1933&ndash;1997
| occupation = Actor, singer, humanitarian, activist
| spouse = {{marriage|[[Margo (actress)|Margo]]|1945|1985|end=her death}}
| children = [[Edward Albert]]
| module = {{Infobox military person|embed=yes
| allegiance = [[United States|United States of America]]
| branch = [[United States Navy]]
| serviceyears = 1942–45
| rank = Lieutenant
| battles = [[Battle of Tarawa]]
| awards = [[Bronze Star Medal]]}}
}}

'''Edward Albert Heimberger''' (April 22, 1906 – May 26, 2005), known professionally as '''Eddie Albert''', was an American actor and [[activism|activist]]. He was nominated for the [[Academy Award for Best Supporting Actor]] in 1954 for his performance in ''[[Roman Holiday]]'', and in 1973 for ''[[The Heartbreak Kid (1972 film)|The Heartbreak Kid]]''.<ref name=nytobit/>

Other well-known screen roles of his include Bing Edwards in the ''[[Brother Rat]]'' films, traveling salesman Ali Hakim in the musical ''[[Oklahoma!]]'', and the [[Sadistic personality disorder|sadistic]] prison warden in 1974's ''[[The Longest Yard (1974 film)|The Longest Yard]]''. He starred as [[Oliver Wendell Douglas]] in the 1960s television [[sitcom]] ''[[Green Acres]]'' and as Frank MacBride in the 1970s [[Crime film#In television|crime drama]] ''[[Switch (TV series)|Switch]]''. He also had a recurring role as Carlton Travis on ''[[Falcon Crest]]'', opposite [[Jane Wyman]].<ref name=nytobit/>

==Early life==
Edward Albert Heimberger was born in [[Rock Island, Illinois|Rock Island]], [[Illinois]], on April 22, 1906, the oldest of the five children of Frank Daniel Heimberger, a realtor, and his wife, Julia Jones.<ref>[http://www.filmreference.com/film/35/Eddie-Albert.html Eddie Albert profile at FilmReference.com]</ref> His year of birth is often given as 1908, but this is incorrect. His parents were not married when Albert was born, and his mother altered his birth certificate after her marriage.<ref>{{cite news|url=http://www.usatoday.com/life/people/2005-05-27-albert-obit_x.htm|agency=[[Associated Press]]|title='Green Acres' star Eddie Albert dies at 99|date=May 28, 2005|accessdate=May 1, 2010}}</ref>

When he was one year old, his family moved to [[Minneapolis]], [[Minnesota]]. Young Edward secured his first job as a newspaper boy when he was only six. During [[World War I]], his German name led to taunts as "the enemy" by his classmates. He studied at Central High School in Minneapolis and joined the drama club. His schoolmate Harriet Lake (later known as actress [[Ann Sothern]]) graduated in the same class. Finishing high school in 1926,<ref>The 1926 Centralian yearbook for Minneapolis Central High School</ref> he entered the [[University of Minnesota]], where he majored in business.

==Career==
When he graduated, he embarked on a business career. However, the [[Wall Street Crash of 1929|stock market crash in 1929]] left him essentially unemployed. He then took odd jobs, working as a [[trapeze]] performer, an [[insurance]] salesman, and a [[nightclub singer]]. Albert stopped using his last name professionally, since it invariably was mispronounced as "Hamburger". He moved to [[New York City]] in 1933, where he co-hosted a radio show, ''The Honeymooners - Grace and Eddie Show'', which ran for three years. At the show's end, he was offered a film contract by [[Warner Bros.]]<ref name=wp>{{cite web|last=Holley|first=Joe|title=Eddie Albert, Star of 'Green Acres,' Dies at 99|date=28 May 2005|publisher=''[[The Washington Post]]''|url=http://www.washingtonpost.com/wp-dyn/content/article/2005/05/27/AR2005052701459.html|accessdate=17 June 2015}}</ref>

In the 1930s, Albert performed in [[Broadway theatre|Broadway]] stage productions, including ''Brother Rat'', which opened in 1936. He had lead roles in ''[[Room Service (play)|Room Service]]'' (1937&ndash;1938) and ''[[The Boys from Syracuse]]'' (1938&ndash;1939). In 1936, Albert had also become one of the earliest television actors, performing live in one of [[RCA]]'s first television broadcasts in association with [[NBC]], a promotion for their New York City radio stations.<ref name=wp/>

[[File:First television play NBC 1936.jpg|thumb|200px|Albert and Grace Bradt applying makeup for their first TV appearance, November 1936]]

Performing regularly on early television, Albert wrote and performed in the first [[teleplay]], ''The Love Nest'', written for television. Done live (not recorded on film), this production took place November 6, 1936, and originated in Studio 3H (now 3K) in the [[GE Building]] at [[Rockefeller Center]] (then called the [[RCA]] Building) in New York City and was broadcast over NBC's experimental television station W2XBS (now [[WNBC]]).  Hosted by Betty Goodwin, ''The Love Nest'' starred Albert, [[Hildegarde]], [[The Ink Spots]], [[Ed Wynn]], and actress Grace Brandt. Before this time, television productions were adaptations of stage plays.<ref>Gary R Edgerton, ''The Columbia History of American Television'', Columbia University Press, 2007, p. 53; ISBN 978-0-231-12164-4</ref>

In 1938, he made his feature-film debut in the Hollywood version of ''[[Brother Rat]]'' with [[Ronald Reagan]] and [[Jane Wyman]], reprising his Broadway role as cadet "Bing" Edwards. The next year, he starred in ''[[On Your Toes]],'' adapted for the screen from the Broadway smash by [[Rodgers and Hart]].<ref>{{cite news|last=Nugent|first=Frank S.|title=Mirele Efros (1938) THE SCREEN; 'On Your Toes,' From the Rodgers and Hart Library, Opens at the Strand--'Mirele Efros' at Cameo At the Cameo At the 86th St. Garden Theatre At the 48th Street Theatre At the Modern Playhouse|date=21 October 1939|publisher=''[[The New York Times]]''|url=https://www.nytimes.com/movie/review?res=9500E7DF143EE432A25752C2A9669D946894D6CF|accessdate=17 June 2015}}</ref>

===Military===
Prior to World War II, and before his film career, Albert had toured Mexico as a clown and high-wire artist with the Escalante Brothers Circus, but secretly worked for [[U.S. Army]] intelligence, photographing German [[U-boats]] in Mexican harbors.<ref>[http://www.grandtimes.com/eddie.html >"Organic Eddie", ''Grand Times'', 1996]</ref> On September 9, 1942, Albert enlisted in the [[United States Coast Guard]] and was discharged in 1943 to accept an appointment as a lieutenant in the [[U.S. Naval Reserve]]. He was awarded the [[Bronze Star Medal|Bronze Star]] with Combat "V" for his actions during the [[Battle of Tarawa|invasion of Tarawa]] in November 1943, when, as the pilot of a  Coast Guard [[landing craft]], he rescued 47 [[United States Marine Corps|Marines]] who were stranded offshore (and supervised the rescue of 30 others), while under heavy enemy machine-gun fire.<ref>[http://thunderaway.com/worldwar/pdfwar/WW2hollywood.pdf Profile of Albert's WWII exploits]</ref>

===As leading man===
During the war years, Albert returned to films, starring in ones such as ''[[The Great Mr. Nobody]]'', ''[[Lady Bodyguard]]'', and ''[[Ladies' Day]]'', as well as reuniting with Reagan and Wyman for ''[[An Angel from Texas]]'' and co-starring with [[Humphrey Bogart]] in ''[[The Wagons Roll at Night]]''. After the war, he continued to appear in leading roles, including 1947's ''[[Smash-Up, the Story of a Woman]]'', opposite [[Susan Hayward]].

===As character actor===
From 1948 on, Albert guest-starred in nearly 90 [[television series]].<ref>{{cite web|url=http://us.imdb.com/name/nm0000734|title=Eddie Albert|publisher=IMDb|accessdate=2010-07-20}}</ref> He made his guest-starring debut on an episode of ''[[The Ford Theatre Hour]]''. This part led to other roles such as ''Chevrolet Tele-Theatre'', ''[[Suspense (US TV series)|Suspense]]'', ''[[Lights Out (1946 TV series)|Lights Out]]'', ''[[Schlitz Playhouse of Stars]]'', ''[[Studio One (TV series)|Studio One]]'', ''[[Philco Television Playhouse]]'', ''[[Your Show of Shows]]'', ''[[Front Row Center]]'', ''[[The Alcoa Hour]]'', and in dramatic series ''[[The Eleventh Hour (U.S. TV series)|The Eleventh Hour]]'', ''[[The Reporter (TV series)|The Reporter]]'', and ''[[General Electric Theater]]''.

In 1959, Albert was cast as businessman Dan Simpson in the episode "The Unwilling" of the [[NBC]] [[Western (genre)|Western]] series ''[[Riverboat (TV series)|Riverboat]]''. In the story line, Dan Simpson attempts to open a general store in the [[American West]] despite a raid from pirates on the [[Mississippi River]] who stole from him $20,000 in merchandise. [[Debra Paget]] is cast in this episode as Lela Russell; [[Russell Johnson]] is Darius, and [[John Pickard (American actor)|John M. Picard]] is uncredited as a river pirate.<ref>{{cite web|url=http://www.imdb.com/title/tt0687117/|title="The Unwilling", ''Riverboat'', October 11, 1959|publisher=[[Internet Movie Data Base]]|accessdate=February 20, 2013}}</ref>

===On stage===
The 1950s also had a return to Broadway for Albert, including roles in ''[[Miss Liberty]]'' (1949–1950) and ''[[The Seven Year Itch]]'' (1952–1955). In 1960, Albert replaced [[Robert Preston (actor)|Robert Preston]] in the lead role of Professor Harold Hill, in the Broadway production of ''[[The Music Man]]''. Albert also performed in regional theater. He created the title role of [[Marc Blitzstein]]'s ''[[Reuben, Reuben (opera)|Reuben, Reuben]]'' in 1955 in Boston. He performed at [[The Muny]] Theater in [[St. Louis, Missouri|St. Louis]], reprising the Harold Hill role in ''The Music Man'' in 1966 and playing Alfred P. Doolittle in ''[[My Fair Lady]]'' in 1968.

[[File:Eddie Albert.jpg|left|thumb|Eddie Albert in ''[[I'll Cry Tomorrow]]'' (1955)]]

===1950s and 1960s film career===
In the 1950s, Albert appeared in film roles such as that of [[Lucille Ball]]'s fiancé in ''[[The Fuller Brush Girl]]'' (1950), Bill Gorton in ''[[The Sun Also Rises (1957 film)|The Sun Also Rises]]'' (1957), and a traveling salesman in ''[[Carrie (1952 film)|Carrie]]'' (1952). He was nominated for his first Oscar as Best Supporting Actor with ''[[Roman Holiday]]'' (1953). In ''[[Oklahoma! (1955 film)|Oklahoma!]]'' (1955), he played a womanizing Persian peddler, and in ''[[Who's Got the Action?]]'' (1962), he portrayed a lawyer helping his partner ([[Dean Martin]]) cope with a gambling addiction. In ''[[The Teahouse of the August Moon (film)|Teahouse of the August Moon]]'' (1956) he played a psychiatrist with an enthusiasm for farming. He appeared in several military roles, including ''[[The Longest Day (film)|The Longest Day]]'' (1962), about the Normandy invasion. The film ''[[Attack (1956 film)|Attack]]'' (1956) provided Albert with a dark role as a cowardly, psychotic [[United States Army|Army]] captain whose behavior threatens the safety of his company. In a similar vein, he played a psychotic United States Army Air Force colonel in ''[[Captain Newman, M.D.]]'' (1963), opposite [[Gregory Peck]].

===Television series===

===''Leave It to Larry''===
Albert's first television series was ''[[Leave It to Larry]]'', a [[CBS]] [[situation comedy|sitcom]] that aired in the 1952-1953 season, with Albert as Larry Tucker, a shoe salesman who lives with his young family in the home of his father-in-law and employer, played by [[Ed Begley]]. He guest-starred on various series, including [[American Broadcasting Company|ABC]]'s ''[[The Pat Boone Chevy Showroom]]'', as well as the [[Westinghouse Studio One]] series (CBS, 1953–54), playing Winston Smith in the first TV adaptation of ''[[1984 (1953 TV program)|1984]]'', by [[William Templeton (screenwriter)|William Templeton]].

===''The Eddie Albert Show''===
Albert had his own daytime variety program, ''The Eddie Albert Show'', on CBS television in 1953. Singer [[Ellen Hanley]] was a regular on the show. A review in [[Broadcasting & Cable|''Broadcasting'']] magazine panned the program, saying, "Mr. Albert with the help of Miss Hanley, conducts an interview, talks a little, sings a little and looks all-thumbs a lot."<ref>{{cite news|title=In Review: The Eddie Albert Show|url=http://www.americanradiohistory.com/hd2/Archive-BC-IDX/53-OCR/BC-1953-03-09-OCR-Page-0014.pdf|accessdate=11 July 2015|agency=Broadcasting|date=March 9, 1953|page=14}}</ref>

===''Saturday Night Revue''===
Beginning June 12, 1954, Albert was host of ''Saturday Night Revue'', which replaced ''[[Your Show of Shows]]'' on NBC. The 9:00-10:30&nbsp;pm (Eastern Time) program also featured [[Ben Blue]] and [[Alan Young]] and the [[Sauter-Finegan Orchestra]].<ref>{{cite news|title=In Review: Saturday Night Revue|url=http://www.americanradiohistory.com/hd2/Archive-BC-IDX/54-OCR/1954-06-21-BC-OCR-Page-0014.pdf|accessdate=30 July 2015|agency=Broadcasting|date=June 21, 1954|page=14}}</ref>

===Guest appearances===
In 1964, Albert guest-starred in "[[Cry of Silence]]", an episode of the science fiction television series ''[[The Outer Limits (1963 TV series)|The Outer Limits]]''.  Albert played Andy Thorne, who along with his wife Karen (played by June Havoc), had decided to leave the city and buy a farm (a recurring theme in Albert's career). They find themselves lost and in the middle of a deserted valley where they come under attack by a series of tumbleweeds, frogs, and rocks. Also in 1964, he guest-starred as a government agent in the pilot episode of ''[[Voyage to the Bottom of the Sea (TV series)|Voyage to the Bottom of the Sea]]''  entitled "Eleven Days to Zero".

Albert was cast as Charlie O'Rourke in the 1964 episode "Visions of Sugar Plums" of the NBC education drama series, ''[[Mr. Novak]]'', starring [[James Franciscus]].  [[Bobby Diamond]], formerly of the ''[[Fury (TV series)|Fury]]'' series, also appeared in this episode.

===''Green Acres''===
In 1965, Albert was approached by producer [[Paul Henning]] to star in a new sitcom for CBS called ''[[Green Acres]]''. His character, [[Oliver Wendell Douglas]], was a lawyer who left the city to enjoy a simple life as a farmer. Co-starring on the show was [[Eva Gabor]] as his urbanite, spoiled wife. The show was an immediate hit, achieving fifth place in the ratings in its first season.  The series lasted six seasons with 170 episodes.

===''Switch''===
After a four-year-absence from the small screen, and upon reaching age 69 in 1975, Albert signed a new contract with [[Universal Television]], and starred in the popular 1970s adventure/[[crime drama]] ''[[Switch (TV series)|Switch]]'' for CBS, as a retired police officer, Frank McBride, who goes to work as a private detective with a former criminal he had once jailed. In its first season, ''Switch'' was a hit. By late 1976, the show had become a more serious and traditional [[crime drama]]. At the end of its third season in 1978, ratings began to drop, and the show was canceled after 70 episodes.

===Television specials===
Eddie Albert appears in a number of [[television special]]s. His first was the 1956 made-for-television NBC [[documentary]] ''[[Our Mr. Sun]]'', a Bell Telephone-produced color special.<ref>[https://archive.org/details/our_mr_sun ''Our Mr. Sun''], a Bell Telephone TV special starring Eddie Albert</ref> Directed by [[Frank Capra]], it blends live action and animation. Albert appears with Dr. [[Frank C. Baxter|Frank Baxter]], who appears in several other Bell Telephone science specials.

In 1965, the year that ''Green Acres'' premiered, Albert served as host/narrator for the telecast of a German-American made-for-television film version of ''[[The Nutcracker]]'', which was rerun several times and is now available as a Warners Archive DVD. The host sequences and the narration, all included on the DVD, were especially filmed for English-language telecasts of this short film (it was only an hour in length, and cut much from the Tchaikovsky ballet).<ref>{{IMDb title|221445|Der Nußknacker}}</ref>

In 1968, he voiced [[Myles Standish]] in the [[Rankin/Bass]] [[Animated cartoon|Animated TV special]] ''[[The Mouse on the Mayflower]]''.

===Later work===
In 1971, Albert guest-starred in a season-one ''[[Columbo]]'' episode called  "[[List of Columbo episodes#Season 1|Dead Weight]]", which also featured guest star [[Suzanne Pleshette]], as a retired [[US Marine Corps]] major general from the [[Korean War]] who murders his adjutant to cover up a ''[[quid pro quo]]'' contracting scheme.

In 1972, Albert resumed his film career and was nominated for an [[Academy Awards|Oscar]] for [[Academy Award for Best Supporting Actor|Best Supporting Actor]] for his performance as an overprotective father in ''[[The Heartbreak Kid (1972 film)|The Heartbreak Kid]]'' (1972), and delivered a memorable performance as an evil prison warden in 1974's ''[[The Longest Yard (1974 film)|The Longest Yard]]''. In a lighter vein, Albert portrayed the gruff though soft-hearted Jason O'Day in the successful Disney film ''[[Escape to Witch Mountain]]'' in 1975.

Albert appeared in such 1980s films as ''[[How to Beat the High Co$t of Living]]'' (1980), ''[[Yesterday (1981 film)|Yesterday]]'' (1981), ''[[Take This Job and Shove It (film)|Take This Job and Shove It]]'' (1981), ''[[Rooster (television film)|Rooster]]'' (1982 television film), and ''[[Yes, Giorgio]]'' (1982), and as the US President in ''[[Dreamscape (1984 film)|Dreamscape]]'' (1984). His final feature film role was a cameo appearance in ''[[The Big Picture (1989 film)|The Big Picture]]'' (1989). He also appeared in many all-star television miniseries, including ''[[Evening in Byzantium]]'' (1978), ''[[The Word (novel)#TV miniseries|The Word]]'' (1978), ''[[Peter and Paul]]'' (1981), ''[[Goliath Awaits]]'' (1981) and ''[[War and Remembrance (miniseries)|War and Remembrance]]'' (1988).

In the mid-1980s, Albert was reunited with longtime friend and co-star of the ''Brother Rat'' and ''An Angel from Texas'' films, Jane Wyman, in a recurring role as the villainous Carlton Travis in the popular 1980s [[soap opera]] ''[[Falcon Crest]]''. He also guest-starred on an episode of the '80s television series ''[[Highway to Heaven]]'', as well as ''[[Murder, She Wrote]]'', and in 1990, he reunited with Eva Gabor for a ''Return To Green Acres''. In 1993, he guest-starred for several episodes on the ABC daytime soap opera ''[[General Hospital]]'' as Jack Boland, and also made a guest appearance on the ''[[The Golden Girls|Golden Girls]]'' spin-off ''[[The Golden Palace]]'' the same year.

==Hollywood blacklist==
Eddie Albert's wife, Mexican actress [[Margo (actress)|Margo]], was well known in Hollywood for her left-wing political leanings.<ref>Colacello, Bob. ''Ronnie and Nancy: Their Path to the White House--1911 to 1980'' Grand Central Publishing, 2004</ref> Though not herself a Communist, Margo was well-acquainted with several members of the American [[Communist Party]].<ref>Lawrence, Greg. ''Dance with Demons: The Life Jerome Robbins'' Penguin, 2001</ref> As a result, in 1950, Albert's name was published in "[[Red Channels]]", an anti-Communist pamphlet that sought to expose purported Communist influence within the entertainment industry.<ref>Walker, William T. ''McCarthyism and the Red Scare: A Reference Guide'' pp. 24-25 ABC-CLIO, 2011</ref><ref name="autogenerated973">DiMare, Philip C. ''Movies in American History: An Encyclopedia, Volume 1'' p. 973 ABC-CLIO, 2011</ref>

By 1951, those identified in "Red Channels" were [[Hollywood blacklist|blacklisted]] across much or all of the movie and broadcast industries unless they cleared their names, the customary requirement being that they testify before the [[House Un-American Activities Committee]].

{{Quote|text=Additional hearings in 1951-52 generated the bulk of the blacklist, which was then used by the industry on both coasts to control who was hired. In addition, the 1950 publication "Red Channels" listed 151 suspects, and hearings on a smaller scale continued through the decade. Friendly witnesses included actors [[Lloyd Bridges]], [[Lee J. Cobb]], [[Gary Cooper]], [[Robert Montgomery (actor)|Robert Montgomery]], [[Ronald Reagan]], and [[Robert Taylor (actor)|Robert Taylor]]; studio heads [[Walt Disney]], [[Louis B. Mayer]], and [[Jack L. Warner]]; and director [[Elia Kazan]] (whose compliance generated controversy over honoring him in the 1990s). Among the hundreds named were Eddie Albert, [[Richard Attenborough]], [[Lucille Ball]] (who testified but satisfied the committee without naming others), [[Will Geer]], [[Charlie Chaplin]], [[Howard da Silva]], [[Lee Grant]], [[Lillian Hellman]], [[Kim Hunter]], [[Norman Lloyd]], [[Arthur Miller]], [[Zero Mostel]], [[Dorothy Parker]], [[Paul Robeson]], and [[Lionel Stander]].

The results were devastating for many on the list. Some changed careers, while others left the United States, or if screenwriters, worked under pseudonyms and used "fronts" to sell their scripts.<ref name="autogenerated973"/>}}

Albert later spoke of this period:

{{Quote|text=Everyone was so full of fear. Many people couldn't support their families, or worse, their lives were ruined and they had to go out and do menial jobs. Some even killed themselves. ~ Eddie Albert, quoted in ''Vincent Price: A Daughter's Biography''<ref>Price, Victoria. ''Vincent Price: A Daughter's Biography.'' Open Road Media, 2014 [https://books.google.com/books?id=jZVxAwAAQBAJ&pg=PT164&dq=%22eddie+albert%22+huac&hl=]</ref>}}

Albert's son spoke of his parents' blacklisting in an interview published in December 1972, crediting Albert's service during World War II with ultimately saving his career.

{{Quote|text=My mom was blacklisted for appearing at an anti-Franco rally; she was branded a Communist, was spat upon in the streets, and had to have a bodyguard. And my dad found himself unemployable at several major studios, just when his career was gathering momentum. During the second World War, dad joined the Navy and saw action at Tarawa, and because he came back something of a hero, he was able to get work again. But he never got as far as he should have gotten.<ref>Brown, Gene. ''The New York Times Encyclopedia of Film: 1972-1974'' Time Books, 1984</ref>}}

While Albert's career survived the blacklist, his wife, Margo, had extreme difficulty finding work.<ref>Price, Victoria. ''Vincent Price: A Daughter's Biography'' Open Road Media, 2014 [https://books.google.com/books?id=jZVxAwAAQBAJ&pg=PT164&dq=%22eddie+albert%22+huac&hl=]</ref>

==Activism and interests==
Albert was active in social and environmental causes, especially from the 1970s onward. Beginning in the 1940s, Eddie Albert Productions produced films for various US corporations, as well as documentaries such as ''Human Beginnings'' (a for-its-time controversial sex-education film) and ''Human Growth''.<ref>''Playground Daily News'' (Fort Walton Beach, Florida), March 20, 1970.</ref> He narrated and starred in a 1970 film promoting the views of [[Weyerhaeuser]], a major international forestry products concern.<ref>Williams, Ted. [http://www.treedictionary.com/DICT2003/hardtoget/myth5/pg25-29.html ''The Insightful Sportsman''], Camden, Maine: Down East Books, 1996<!-- ISBN needed --></ref>

He was special envoy for [[Meals for Millions]] and consultant for the World Hunger Conference.<ref name="Congressional Record 2005">Congressional Record, July 18, 2005, Section 22</ref> He joined [[Albert Schweitzer]] in a documentary about African malnutrition<ref>{{cite web|last=McKee|first=Brent|url=http://childoftv.blogspot.com/2005/05/eddie-albert-1906-2005.html|title=I Am A Child Of Television: Eddie Albert 1906-2005|publisher=Childoftv.blogspot.com|date=2005-05-28|accessdate=2010-07-20}}</ref><ref>''[http://video.google.com/videoplay?docid=3382029653866127786 Excerpts of documentary about African malnutrition] at Google Video</ref> and fought agricultural and industrial pollution, particularly [[DDT]].<ref name="Congressional Record 2005"/> Albert promoted [[organic gardening]], and founded City Children's Farms for inner-city children,<ref>''Pacific Palisades Post'', June 2, 2005</ref> while supporting eco-farming and tree planting.<ref>Walters, Charles. "The Last Word", ''Acres USA'', July 2005, Vol. 35, No. 7<!-- ISBN needed --></ref>

Albert was national chairman for the [[Boy Scouts of America]]'s conservation program and founded the Eddie Albert World Trees Foundation. He was a trustee of the [[National Recreation and Park Association]] and a member of the [[United States Department of Energy|U.S. Department of Energy]]'s advisory board. ''[[TV Guide]]'' called him "an ecological [[Paul Revere]]".<ref>''Los Angeles Times'', May 27, 2005.</ref>

Albert was also a director of the U.S. Council on Refugees<ref>{{cite web|author=Staff|url=http://www.avsforum.com/avs-vb/showthread.php?t=440744&page=106|title=Hot Off The Press!|publisher=AVS Forum|accessdate=2010-07-20}}</ref><ref>{{cite web|url=http://marriage.about.com/od/entertainmen1/p/eddiealbert.htm|title=Eddie and Margo Albert Marriage Profile|publisher=Marriage.about.com|accessdate=2010-07-20}}</ref> and participated in the creation of [[Earth Day]] and spoke at its inaugural ceremony in 1970.<ref name="Congressional Record 2005"/>

==Personal life==
Albert married Mexican actress Margo (née María Margarita Guadalupe Teresa Estela Bolado Castilla y O'Donnell) in 1945. Albert and Margo had a son, [[Edward Albert|Edward Jr.]], also an actor, and adopted a daughter, Maria, who became her father's business manager. Margo Albert died from [[brain cancer]] on July 17, 1985.

The Alberts lived in [[Pacific Palisades, California]], in a Spanish-style house on an acre of land (0.4ha) with a cornfield in front. Albert grew organic vegetables in a greenhouse and recalled how his parents had a "liberty garden" at home during [[World War I]]. Eddie Albert was left-handed.

==Last years and death==
[[File:Eddie Albert grave at Westwood Village Memorial Park Cemetery in Brentwood, California.JPG|thumb|Eddie Albert's grave]]

Albert was diagnosed with [[Alzheimer's disease]] in his last years.<ref>{{cite web|last=McLellan|first=Dennis|title=Eddie Albert, 99. Versatile Stage and Screen Actor Best Known for Role in 'Green Acres'|date=28 May 2005|newspaper=[[Los Angeles Times]]|url=http://articles.latimes.com/2005/may/28/local/me-albert28|accessdate=30 August 2015}}</ref><ref>{{cite web|last=Holley|first=Joe|title=Eddie Albert, Star of 'Green Acres,' Dies at 99|date=28 May 2005|newspaper=[[The Washington Post]]|url=http://www.washingtonpost.com/wp-dyn/content/article/2005/05/27/AR2005052701459.html|accessdate=30 August 2015}}</ref><ref>{{cite web|last=McLellan|first=Dennis|title=Eddie Albert, 1906-2005: "Green Acres" star, WWII vet, activist|date=28 May 2005|newspaper=[[The Seattle Times]]|url=http://old.seattletimes.com/html/television/2002291252_albert28.html|accessdate=30 August 2015}}</ref>

His son put his acting career aside to care for his father. Despite his illness, Albert exercised regularly until shortly before his death. Eddie Albert died of [[pneumonia]] on May 26, 2005, at the age of 98 at his home in [[Pacific Palisades, Los Angeles|Pacific Palisades]].<ref name=nytobit>{{cite news |author=[[Margalit Fox]] |title=Eddie Albert, Character Actor, Dies at 99 |url=https://www.nytimes.com/2005/05/28/arts/television/eddie-albert-character-actor-dies-at-99.html |quote= |newspaper=[[New York Times]] |date=May 28, 2005 }}</ref> He was interred at [[Westwood Village Memorial Park Cemetery]], next to his late wife and close to his ''Green Acres'' co-star Eva Gabor.

Albert's son, [[Edward Albert|Edward Jr.]] (1951–2006), was an actor, musician, singer, and [[linguist]]/dialectician.<ref>[http://www.accuracyproject.org/cbe-Albert,Edward.html "Edward Albert" profile at Internet Accuracy Project]. "Edward Albert was also a photographer, sculptor, singer/songwriter, musician (guitar), and a linguist/dialectician who was fluent in French, Spanish, Portuguese, and Mandarin Chinese."</ref> Edward Jr. died at age 55, one year after his father. He had been suffering from [[lung cancer]] for 18 months.

For contributions to the television industry, Eddie Albert was honored with a star on the [[Hollywood Walk of Fame]] at 6441 Hollywood Boulevard.<ref name="HWOFDB">{{cite web|url=http://www.hwof.com/stars?recipient=Eddie_Albert |title=Hollywood Walk of Fame database |publisher=HWOF.com}}</ref>

==Filmography==

{| class="wikitable" style="font-size:90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Film
|- style="text-align:center;"
! style="background:#ccc;"| Year
! style="background:#ccc;"| Film
! style="background:#ccc;"| Role
! style="background:#ccc;"| Notes
|-
| 1938
| ''[[Brother Rat]]''
| 'Bing' Edwards
|
|-
| 1939
| ''[[On Your Toes]]''
| Phil Dolan Jr.
|
|-
| 1939
| ''[[Four Wives]]''
| Dr. Clinton Forrest, Jr.
|
|-
| 1940
| ''[[Brother Rat and a Baby]]''
| 'Bing' Edwards
|
|-
| 1940
| ''[[An Angel from Texas]]''
| Peter Coleman
|
|-
| 1940
| ''[[My Love Came Back]]''
| Dusty Rhodes
|
|-
| 1940
| ''[[A Dispatch from Reuter's]]''
| Max Wagner
|
|-
| 1941
| ''[[Four Mothers]]''
| Clinton Forrest, Jr.
|
|-
| 1941
| ''[[The Great Mr. Nobody]]''
| Robert 'Dreamy' Smith 
|
|-
| 1941
| ''[[The Wagons Roll at Night]]''
| Matt Varney
|
|-
| 1941
| ''[[Thieves Fall Out]]''
| Eddie Barnes
|
|-
| 1941
| ''[[Out of the Fog (film)|Out of the Fog]]''
| George Watkins
|
|-
| 1942
| ''[[Treat 'Em Rough]]''
| Bill Kingsford aka The Panama Kid
|
|-
| 1942
| ''[[Eagle Squadron (film)|Eagle Squadron]]''
| Leckie
|
|-
| 1943
| ''[[Lady Bodyguard]]''
| Terry Moore
|
|-
| 1943
| ''[[Ladies' Day]]''
| Wacky Waters
|
|-
| 1943
| ''[[Bombardier (film)|Bombardier]]''
| Tom Hughes
|
|-
| 1946
| ''[[Strange Voyage]]''
| Chris Thompson
|
|-
| 1946
| ''[[Rendezvous with Annie]]''
| Cpl. Jeffrey Dolan
|
|-
| 1947
| ''[[The Perfect Marriage]]''
| Gil Cummins
|
|-
| 1947
| ''[[Smash-Up: The Story of a Woman]]''
| Steve Nelson
|
|-
| 1947
| ''[[Hit Parade of 1947]]''
| Kip Walker
|
|-
| 1947
| ''[[Time Out of Mind (1947 film)|Time Out of Mind]]''
| Jake Bullard
|
|-
| 1947
| ''[[Unconquered]]''
| Barker
| (scenes deleted)
|-
| 1948
| ''[[The Dude Goes West]]''
| Daniel Bone
|
|-
| 1948
| ''[[You Gotta Stay Happy]]''
| Bullets Baker
|
|-
| 1948
| ''[[Every Girl Should Be Married]]''
| Harry Proctor/ 'Old' Joe
| cameo, Uncredited
|-
| 1950
| ''[[The Fuller Brush Girl]]''
| Humphrey Briggs
|
|-
| 1951
| ''[[You're in the Navy Now]]''
| Lt. Bill Baron
|
|-
| 1951
| ''Meet Me After the Show''
| Chris Leeds
|
|-
| 1952
| ''[[Actor's and Sin]]''
| Orlando Higgens
| (segment "Woman of Sin")
|-
| 1952
| ''[[Carrie (1952 film)|Carrie]]''
| Charles Drouet
|
|-
| 1953
| ''[[Roman Holiday]]''
| Irving Radovich
|
|-
| 1955
| ''[[The Girl Rush]]''
| Elliot Atterbury
|
|-
| 1955
| ''[[Oklahoma! (1955 film)|Oklahoma!]]''
| Ali Hakim
|
|-
| 1955
| ''[[I'll Cry Tomorrow]]''
| Burt McGuire
|
|-
| 1956
| ''[[Attack! (film)|Attack!]]''
| Capt. Erskine Cooney
|
|-
| 1956
| ''[[The Teahouse of the August Moon (film)|The Teahouse of the August Moon]]''
| Capt. McLean
|
|-
| 1957
| ''[[The Sun Also Rises (1957 film)|The Sun Also Rises]]''
| Bill Gorton
|
|-
| 1957
| ''[[The Joker Is Wild]]''
| Austin Mack
|
|-
| 1958
| ''[[Orders to Kill]]''
| Major MacMahon
|
|-
| 1958
| ''[[The Gun Runners]]''
| Hanagan
|
|-
| 1958
| ''[[The Roots of Heaven]]''
| Abe Fields
|
|-
| 1959
| ''[[Beloved Infidel]]''
| Bob Carter
|
|-
| 1961
| ''[[Madison Avenue (film)|Madison Avenue]]''
| Harvey Holt Ames
|
|-
| 1961
| ''[[The Young Doctors (film)|The Young Doctors]]''
| Dr Charles Dornberger
|
|-
| 1962
| ''[[The Two Little Bears]]''
| Harry Davis
|
|-
| 1962
| ''[[The Longest Day (film)|The Longest Day]]''
| Col. Thompson
|
|-
| 1962
| ''[[Who's Got the Action?]]''
| Clint Morgan
|
|-
| 1963
| ''[[Miracle of the White Stallions]]''
| Rider Otto
|
|-
| 1963
| ''[[General Hospital]]''
| Jack Boland #1 (1993)
|
|-
| 1963
| ''[[Captain Newman, M.D.]]''
| Col. Norval Algate Bliss
|
|-
| 1963
| ''[[Combat!|Combat]]''  
| Phil
| episode Doughboy
|-
| 1965
| ''[[The Party's Over (1965 film)|The Party's Over]]''
| Ben
|
|-
| 1966
| ''[[7 Women]]''
| Charles Pether - Mission Teacher
|
|-
| 1971
|  ''[[List of Columbo episodes#Season 1|Columbo: Dead Weight]]''
| Major Gen Martin Hollister
| opposite [[Suzanne Pleshette]]
|-
| 1972
| ''[[The Heartbreak Kid (1972 film)|The Heartbreak Kid]]''
| Mr. Corcoran
|
|-
| 1974
| ''[[McQ]]''
| Kosterman
|
|-
| 1974
| ''The Take''
| Chief Berrigan
|
|-
| 1974
| ''[[The Longest Yard (1974 film)|The Longest Yard]]''
| Warden Hazen
|
|-
| 1975
| ''[[Escape to Witch Mountain (1975 film)|Escape to Witch Mountain]]''
| Jason O'Day
|
|-
| 1975
| ''[[The Devil's Rain]]''
| Dr. Sam Richards
|
|-
| 1975
| ''[[Whiffs]]''
| Colonel Lockyer
|
|-
| 1975
| ''[[Hustle (1975 film)|Hustle]]''
| Leo Sellers
|
|-
| 1976
| ''Birch Interval''
| Pa Strawacher
|
|-
| 1976
| ''[[Moving Violation]]''
| Alex Warren
|
|-
| 1978
| ''[[Evening in Byzantium]]''
| Brian Murphy
|television film
|-
| 1978
| ''[[Crash (1978 film)|Crash]]''
| Capt. Dunn
|television film
|-
| 1978
| ''[[The Word (novel)#TV miniseries|The Word]]''
| Ogden Towery
| television miniseries
|-
| 1979
| ''[[The Concorde ... Airport '79]]''
| Eli Sands
|
|-
| 1979
| ''[[Border Cop]]''
| Moffat
|
|-
| 1980
| ''[[How to Beat the High Co$t of Living]]''
| Max
|
|-
| 1980
| ''[[Foolin' Around]]''
| Daggett
|
|-
| 1981
| ''[[Yesterday (1981 film)|Yesterday]]''
| Bart Kramer
|
|-
| 1981
| ''[[Take This Job and Shove It (film)|Take This Job and Shove It]]''
| Samuel Ellison
|
|-
| 1981
| ''[[Peter and Paul]]''
| [[Porcius Festus]]
| television miniseries
|-
| 1981
| ''[[Goliath Awaits]]''
| Adm. Wiley Sloan
| television film
|-
| 1982
| ''[[Yes, Giorgio]]''
| Henry Pollack
|
|-
| 1983
| ''[[The Demon Murder Case]]''
| Father Dietrich 
| television film
|-
| 1984
| ''[[The Act (film)|The Act]]''
| Harry Kruger
|
|-
| 1984
| ''[[Dreamscape (1984 film)|Dreamscape]]''
| The President
|
|-
| 1985
| ''[[Stitches (1985 film)|Stitches]]''
| Dean Bradley
|
|-
| 1985
| ''[[Head Office]]''
| Pete Helmes
|
|-
| 1987
| ''Turnaround''
| Theo
|
|-
| 1988
| ''[[War and Remembrance (miniseries)|War and Remembrance]]''
| [[Breckinridge Long]]
| television miniseries
|-
| 1989
| ''[[The Big Picture (1989 film)|The Big Picture]]''
| M.C.
| Cameo
|-
| 1989
| ''[[Thirtysomething (TV series)|thirtysomething]]''
| Charlie Weston
|
|-
| 1989
| ''[[Brenda Starr (film)|Brenda Starr]]''
| Police Chief Maloney
|
|-
| 1990
| ''Return to Green Acres''
| Oliver Wendell Douglas
|
|-
| 1994
| ''Headless!''
| Sheriff George
| short subject
|-
| 1994
| ''Death Valley Memories''
| narrator
| documentary
|}

==References==
{{reflist|30em}}

==Further reading==
*Wise, James. ''Stars in Blue: Movie Actors in America's Sea Services''. Annapolis, MD: Naval Institute Press, 1997.  ISBN 1557509379 {{OCLC|36824724}}

==External links==
{{Commons category}}
{{Portal|Biography|United States Navy|World War II|Illinois|Minnesota|California|Theatre|Film|Television}}
* {{IMDb name|0000734}}
* {{tcmdb name|id=1614|name=Eddie Albert}}
* {{IBDB name}}
* {{AllRovi person|669}}
* {{Find a Grave|11043389}}
* [http://www.grandtimes.com/eddie.html A 1996 Interview]
* [http://www.legacy.com/ladailynews/Obituaries.asp?Page=LifeStory&PersonID=14072567 Obituary] in the ''[[Los Angeles Daily News]]''

{{Tony hosts}}
{{National Society of Film Critics Award for Best Supporting Actor}}
{{Authority control}}

{{DEFAULTSORT:Albert, Eddie}}
[[Category:20th-century American male actors]]
[[Category:American male film actors]]
[[Category:1906 births]]
[[Category:2005 deaths]]
[[Category:People from Rock Island, Illinois]]
[[Category:American activists]]
[[Category:Male actors from Los Angeles]]
[[Category:Male actors from Minnesota]]
[[Category:American people of German descent]]
[[Category:American gardeners]]
[[Category:American male singers]]
[[Category:American military personnel of World War II]]
[[Category:American male musical theatre actors]]
[[Category:American male radio actors]]
[[Category:American male soap opera actors]]
[[Category:American male stage actors]]
[[Category:American male television actors]]
[[Category:American male voice actors]]
[[Category:American television personalities]]
[[Category:American beekeepers]]
[[Category:Bell Records artists]]
[[Category:Burials at Westwood Village Memorial Park Cemetery]]
[[Category:Deaths from Alzheimer's disease]]
[[Category:Deaths from pneumonia]]
[[Category:Infectious disease deaths in California]]
[[Category:Neurological disease deaths in the United States]]
[[Category:Male actors from Minneapolis]]
[[Category:People from Pacific Palisades, Los Angeles]]
[[Category:United States Navy officers]]
[[Category:University of Minnesota alumni]]
[[Category:Warner Bros. contract players]]
[[Category:Activists from California]]