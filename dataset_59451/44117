{{for|the former water aerodrome|Arendal Airport, Rådhuskaien}}
{{Infobox airport
| name         = Arendal Airport, Gullknapp
| nativename   = Arendal lufthavn, Gullknapp
| nativename-a =
| nativename-r =
| image        = Arendal Airport, Gullknapp logo.png
| image-width  = 200px
| caption      =
| image2       = Gullknapp3.jpg
| image2-width = 300px
| caption2     =
| IATA         =
| ICAO         = ENGK
| FAA          =
| LID          =
| type         = Civil
| owner        = [[Arendals Fossekompani]]
| operator     = Arendal Lufthavn, Gullknapp AS
| owner-oper   =
| city-served  = [[Arendal]], Norway
| location     = [[Blakstadheia]], [[Froland]], Norway
| pushpin_map            = Norway
| pushpin_mapsize        = 300px
| pushpin_relief         = yes
| pushpin_label          = ENGK
| built        = 
| elevation-f  = 394
| elevation-m  = 120
| coordinates  = {{coord|58|31|00|N|008|42|00|E|display=inline,title}}
| website      = [http://Www.gullknapp.no gullknapp.no]
| metric-elev  = 1
| metric-rwy   = 1
| r1-number    = 05/23
| r1-length-f  = 3,625
| r1-length-m  = 1,120
| r1-surface   = Asphalt
| stat-year    =
| stat1-header =
| stat1-data   =
| footnotes    =
}}

'''Arendal Airport, Gullknapp''' ({{lang-no|Arendal lufthavn, Gullknapp}}; {{airport codes||ENGK|p=n}}) is a [[general aviation]] airport situated at Blakstadheia in [[Froland]], [[Norway]]. It consists of a single 1,120-meter (3,650&nbsp;ft) asphalt runway aligned 05/23. The airport is located about {{convert|15|km|sp=us}} from the town center of [[Arendal]].

Plans for an airport in [[Aust-Agder]] were first proposed in the 1930s at [[Løddesøl]]. The airport operating company was founded in 1984 and construction on the original {{convert|500|m|sp=us|adj=on}} runway commenced in 1987 and was completed in 1992. It was paved in 1996, when an operating permission was granted. The runway was extended to 1,120 meters (3650&nbsp;ft) in 2006 and in 2008 a majority of the shares were bought by [[Arendals Fossekompani]]. This spurred work on further expansions in order to compete with [[Kristiansand Airport, Kjevik]]. Concession for scheduled services was issued in 2014.

== History ==

===Løddesøl proposal===

Proposals for an airport in the vicinity of Arendal were first launched in the late 1930s.<ref name=aksjon>{{cite news |title=Aksjon for flyplass i Arendal |agency=[[Norwegian News Agency]] |date=25 February 1988 |language=Norwegian}}</ref> [[Kristiansand Airport, Kjevik]] opened on 1 June 1939 as Norway's third land airport. It has since been closest airport to Arendal with scheduled flights.<ref>{{cite web |url=http://www.avinor.no/lufthavn/kristiansand/omoss/75_Lufthavnens+historie |title=Lufthavnens historie |publisher=[[Avinor]] |language=Norwegian}}</ref>

During the 1950s the [[Royal Norwegian Air Force]] examined the possibilities of building an emergency aerodrome in Aust-Agder. They settled for Løddesøl, about {{convert|10|km|sp=us}} north of Arendal. The ground was bought and investments in a road and a water line commenced, but none of the work was finalized. [[Braathens SAFE]] resumed the plans in April 1956, when they announced that they planned to fly to Arendal as an intermediate stop on their service from [[Oslo Airport, Fornebu]] to [[Stavanger Airport, Sola]]. [[Ludvig G. Braathen]] stated that he hoped that an airport could be built quickly, citing the fast completion of [[Notodden Airport, Tuven]]. The airline stated the need for a runway of between {{convert|900|and|1200|m|sp=us}}, estimated to cost seven to eight hundred thousand [[Norwegian krone]] (NOK).<ref>{{cite news |title=Arendal nytt stoppested på Braathens "Melkerute"? |last=Askeland |first=Gunnar |work=[[Verdens Gang]] |date=28 April 1956 |page=5 |language=Norwegian}}</ref>

===Establishment===
Work on the airport resumed in the 1970s, this time under the auspice of [[Aust-Agder County Municipality]]. This eventually led to the procurement of land for Gullknapp in 1984.<ref>{{cite web |url=http://www.ablikk.no/index.cfm?event=doLink&famID=414631&frontFamID=395285 |title=Nils P. – Romfergelanding på Gullknapp? |last=Øyraas |first=Eivind |date=15 August 2014 |language=Norwegian |work=Ablikk |accessdate=8 September 2014}}</ref> The same year a [[aksjeselskap|limited company]], Gullknapp A/S, was established to secure ownership of the airport.<ref>{{cite web |url=https://www.arendal.kommune.no/PageFiles/57537/Eierskapsmelding%202011-med%20protokoll.pdf |title=Eierskapsmelding 2011 |year=2012 |publisher=Arendal Municipality |language=Norwegian |format=PDF |page=22 |accessdate=8 September 2014}}</ref> A cooperation was carried out with [[Blakstad Upper Secondary School]]. They became the primary users of the facilities, and were the main contributors to the construction of a road to the airport and the building of a runway. They started work in 1987 and by 1990 they had invested about NOK&nbsp;2.4 million in the facility. The airport company invested a further NOK&nbsp;1 million in administration and planning.<ref name=oftedal>{{cite news |title=Avviser flyplassplan |last=Oftedal |first=Hallgeir |date=19 May 1990 |page=49 |work=[[Dagens Næringsliv]] |language=Norwegian}}</ref>

In 1988 a campaign was started to gain support amongst local businesses. At the time the plans called for investments of NOK&nbsp;40 million.<ref name=aksjon /> Gullknapp was at the time one of fourteen regional airports being considered by the state. They had just completed a round of building nine new regional airports during the mid-1980s and more communities were interested. [[Minister of Transport and Communications (Norway)|Minister of Transport and Communications]] [[William Engseth]] stated in 1988 that a cap had been reached on the number of regional airports which would receive state grants, underlining how each regional passenger cost the government NOK&nbsp;1,500 in subsidies.<ref>{{cite news |title=Ikke flere småflyplasser nå |last=Monsen |first=Trygve |date=5 September 1988 |page=39 |work=[[Aftenposten]] |language=Norwegian}}</ref>

The establishment spurred a political debate locally, with the municipalities of Arendal and [[Hisøy]] backing the project. The [[Avinor|Civil Airport Administration]] granted in 1990 permission to build a {{convert|600|to|700|m|sp=us|adj=on}} runway, along with tarmac for six small aircraft. By then a further NOK&nbsp;2.1 million in investments were needed, but the municipalities turned down funding, pointing to the proximity to Kjevik.<ref name=oftedal />

Work on a {{convert|500|m|sp=us|adj=on}} gravel runway was completed in 1992. However, there was a power line just short of the runway which needed to be moved for the airport to receive operating permission. The airport had originally been foreseen to wavier the requirement, but after an accident where an aircraft crashed into a power line near an airport the authorities became more restrictive. Arendal and Froland Municipalities, the owners of the airport at the time, were only willing to fund the NOK&nbsp;352,000 cost of moving the power lines as long as the county also partially fund it. The latter rejected any responsibility, citing that the power line was municipally owned and they were having to cut back on hospital services and could not prioritize the airport. As a result, the airport was disused for more four years.<ref>{{cite news |title=Her får ingen lande |last=Fonbæk |first=Dag |work=[[Verdens Gang]] |date=21 April 1994 |page=23 |language=Norwegian}}</ref> Not until July 1996 did the airport reopen, after upgrades which included asphalting the runway.<ref name=asphalt>{{cite news |title=For tunge passasjerer årsak til flyhavari ved Arendal |agency=[[Norwegian News Agency]] |date=15 January 1997 |language=Norwegian}}</ref>

===Expansion===
[[File:Gullknapp.jpg|thumb|upright|Location of the airport in relation to Arendal]]
Plans for a further extension of the runway to {{convert|1200|m|sp=us}} were made in 2001, at the time estimated to cost NOK&nbsp;3 million.<ref>{{cite news |title=Ny flyplass til tre millioner |work=[[Aftenposten]] |date=17 January 2001 |page=24 |language=Norwegian}}</ref> From 2003 support and some funding was issued by the municipalities of Arendal and Froland.<ref name=history>{{cite web |url=http://www.gullknapp.no/om-2/ |title=Om |publisher=Arendal Airport, Gullknapp |language=Norwegian |accessdate=8 September 2014}}</ref> This resulted in extension of the runway to {{convert|1120|by|30|m|sp=us}} in 2006, costing NOK&nbsp;10 million.<ref name=blakstadheia>{{cite news |title=– Gullknapp må bli hovedflyplass |work=[[Fædrelandsvennen]] |date=24 March 2006 |page=4 |last=Corneliussen |first=Helge}}</ref> By then about NOK&nbsp;20 million had been invested in the airport, mostly funded by the municipalities of Arendal and Froland.<ref name=owners>{{cite news |title=Gullknapp Flyplass AS |date=11 June 2007 |work=[[Agderposten]] |language=Norwegian}}</ref>

The proposals became more aggressive when the airport company in March 2006 called for the runway to be extended to {{convert|2300|m|sp=us}}, estimated by the company to cost NOK&nbsp;100 million. It argued that an extension of the runway at Kjevik would be costly and that instead Agder should be served by a combination of Gullknapp and [[Farsund Airport, Lista]]. Although the proposal was met with enthusiasm in Arendal, the Agder Council stated that they were opposed to an additional airport in Agder.<ref name=blakstadheia /> Also in Froland there was large opposition to the plans, as there were concerns that the airport would spoil the tranquil environment. Meanwhile, Arendal Municipality promised NOK&nbsp;9 million in share capital if Froland Municipality agreed to extend the runway to at least {{convert|1800|m|sp=us}} at a future date.<ref name="blakstadheia"/>

After a regional political debate the airport decided to not aim at traffic which could compete with Kjevik. Arendal Municipality bough new shares worth NOK&nbsp;7 million in 2006, increasing its control over the company.<ref>{{cite news |title=Arendal tar styringen på Gullknapp |last=Skinnemoen |first=Marit Elisabeth |date=19 June 2007 |language=Norwegian}}</ref> As of early 2007 the airport company was owned 80.8 percent by Arendal Municipality, 11.2 percent by Froland Municipality, 2 percent by ''[[Agderposten]]'', 1.2 percent by [[Tvedestrand]] Municipality and the remainder by various others, mostly local businesses.<ref name=owners /> The first business to use the airport commercially was OSM Group, which stationed two smaller aircraft at the airport.<ref>{{cite news |title=Arendal tar spakene på Gullknapp |last=Eide |first=Bjørn Atle |work=[[Agderposten]] |date=11 June 2007 |language=Norwegian}}</ref> By 2008 they had completed a hangar for their aircraft.<ref name=lofter >{{cite news |title=Løfter flyplass |date=4 February 2008 |work=[[Agderposten]] |language=Norwegian}}</ref>

[[Arendals Fossekompani]] bought newly issued shares for NOK&nbsp;27 million in the company in February 2008, becoming a majority shareholder.<ref name=lofter /> The airport company subsequently started purchasing land around the airport, paying NOK&nbsp;4 per square meter for the land itself, plus the value of natural resources.<ref>{{cite news |title=Landet millionavtale |date=11 March 2008 |work=[[Agderposten]] |last=Askeland |first=Linda |language=Norwegian}}</ref> That year a college for training air traffic controllers and commercial pilots was given governmental approval. It initially planned to have its facilities split between [[Evjemoen]] and Gullknapp.<ref>{{cite news |title=Flygerstudiet er godkjent |date=18 October 2008 |work=[[Agderposten]] |language=Norwegian}}</ref> The college later decided to relocate to Kjevik.<ref>{{cite web |url=http://lfhs.no/status/historikk |title=Historikk |publisher=Lufttrafikkhøyskolen Sør |language=Norwegian |date=10 September 2014}}</ref> A [[Cessna Citation CJ1+]] landed at the airport on 20 June 2008.<ref>{{cite news |title=Historisk Gullknapp-landing |last=Stiansen |first=Håvard |work=[[Agderposten]] |language=Norwegian}}</ref>

A four-lane freeway section of [[European route E18|European Road E18]] opened in 2009, reducing travel times from Arendal to Kjevik.<ref>{{cite news |title=Når firefeltsveien mellom |work=[[Agderposten]] |date=15 March 2008 |language=Norwegian}}</ref> The same year Gullknapp announced new plans to become a regional airport. In a new master plan it called for initially building a perimeter fence, installing fire- and rescue equipment, building a tarmac for nine aircraft, and building a control tower and a termina building. With this in place the airport could qualify to serve commercial flights. In a longer perspective the airport hoped to extend its runway to {{convert|2400|m|sp=us}}.<ref>{{cite news |title=Plan for kortbane og regionflyplass |last=Askeland |first=Linda |date=13 January 2009 |page=4 |work=[[Agderposten]] |language=Norwegian}}</ref>

In a common municipal council meeting between Arendal and Kristiansand on 17 February 2010 the two agreed that Kjevik would remain the primary airport for Agder and that Gullknapp would be used for small commercial aircraft, training and general aviation.<ref>{{cite news |url=http://www.nrk.no/sorlandet/gulknapp-striden-begravet-1.6998161 |title=Flyplass-striden begravet |last=Sundsland |first=Svein |last2=Nilsen |first2=Anne Torhild |date=17 February 2010 |publisher=[[Norwegian Broadcasting Corporation]] |language=Norwegian |archiveurl=https://web.archive.org/web/20140910092621/http://www.nrk.no/sorlandet/gulknapp-striden-begravet-1.6998161 |archivedate=10 September 2014 |accessdate=10 September 2014 |deadurl=no}}</ref> The plans for Gullknapp became more ambitious in 2012, when the airport director stated that the airport aimed competing with [[Sandefjord Airport, Torp]] and exceed its traffic of 1.8 million passengers per year within twenty years. In comparison, Kjevik had 1 million passengers.<ref>{{cite news |url=http://www.nrk.no/sorlandet/vil-ta-opp-kampen-mot-torp-1.8391813 |title=Gullknapp vil ta opp kampen mot Torp |last=Heggem |first=Sander |date=10 November 2012 |publisher=[[Norwegian Broadcasting Corporation]] |language=Norwegian |archiveurl=https://web.archive.org/web/20140910095101/http://www.nrk.no/sorlandet/vil-ta-opp-kampen-mot-torp-1.8391813 |archivedate=10 September 2014 |accessdate=10 September 2014 |deadurl=no}}</ref> Two months later the county council decided that it hoped to expand the airport to serve Mediterranean charter flights.<ref>{{cite news |url=http://www.nrk.no/sorlandet/onsker-charterfly-til-gullknapp-1.10882288 |title=Ønsker charterfly til Gullknapp |last=Sundsland |first=Svein |last2=Nilsen |first2=Anne Torhild |date=10 November 2012 |publisher=[[Norwegian Broadcasting Corporation]] |language=Norwegian |archiveurl=https://web.archive.org/web/20140910095321/http://www.nrk.no/sorlandet/onsker-charterfly-til-gullknapp-1.10882288 |archivedate=10 September 2014 |accessdate=10 September 2014 |deadurl=no}}</ref>

The expansion of Gullknapp attracted criticism from some sources. This was mostly concerned with the services at Kjevik. Professor Frode Steen stated that Kjevik has a hard time attracting international routes and retaining competition on the route to Oslo. If a significant amount of patronage is redirect to Gullknapp, this could be a pivoting cause of insufficient volume on certain routes and could see airlines pull out of Kjevik or hinder the establishment of new routes.<ref>{{cite news |url=http://www.nrk.no/sorlandet/_-gullknapp-kan-hindre-kjevik-1.11768924 |title=– Gullknapp kan hindre vekst på Kjevik |last=Rosenvinge |first=Morten |date=11 June 2014 |publisher=[[Norwegian Broadcasting Corporation]] |language=Norwegian |archiveurl=https://web.archive.org/web/20140910092249/http://www.nrk.no/sorlandet/_-gullknapp-kan-hindre-kjevik-1.11768924 |archivedate=10 September 2014 |accessdate=10 September 2014 |deadurl=no}}</ref> Also the [[Confederation of Norwegian Enterprise]] in Agder stated that they were opposed to an expansion, stating that Agder was best served with one primary airport.<ref>{{cite news |title=Avfeier Gullknapp som hovedflyplass |last=Sørlie |first=Ingjerd |work=[[Agderposten]] |date=8 May 2009 |page=12 |language=Norwegian}}</ref> Similarly, the [[Federation of Norwegian Aviation Industries]] opposed the expansion, stating that it would weaken Kjevik and Torp.<ref>{{cite news |title=Går imot ny Gullknapp-konsesjon |last=Sørlie |first=Ingjerd |work=[[Agderposten]] |date=16 January 2013 |page=8 |language=Norwegian}}</ref>

A twenty-year concession to operate the airport with aircraft with up to fifty passengers was granted by the [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]] on 10 June 2014. With this the airport started extending its runway to {{convert|1199|m|sp=us}} and commenced construction of a terminal. The investments will cost between NOK&nbsp;130 and 140 million.<ref name=legoland>{{cite news |title=Vil vurdere flyrute til Legoland |last=Bergen |first=Pål Yngve |work=[[Agderposten]] |date=11 June 2014 |page=10 |language=Norwegian}}</ref> The airport company stated that it hopes to immediately start services with aircraft with up to nine passengers. It then hopes to expand the airport to allow for regional airliners which could take fifty people.<ref name=history /> It stated that they hoped the first services would be domestic services to [[Bergen Airport, Flesland]] and [[Stavanger Airport, Sola]], as well as to [[Billund Airport]] in Denmark.<ref name=legoland />

==Facilities==
Arendal Airport, Gullknapp is situated at Blakstadheia in Froland, {{convert|15|km|sp=us}} by road northwest of the town center of Arendal.<ref name=blakstadheia /> The airport as a single, {{convert|1120|by|30|m|sp=us}} asphalted runway, aligned 05/23 (roughly southwest–northeast). The airport is situated at an elevation of {{convert|120|m|sp=us}} [[above mean sea level]].<ref name=history2>{{cite web |url=http://www.gullknapp.no/for-piloter/ |title=For piloter |publisher=Arendal Airport, Gullknapp |language=Norwegian |accessdate=10 September 2014}}</ref>

==Accidents and incidents==
On 24 July 1996 a [[Piper Cherokee]] with four people was written off in a [[runway overrun]]. All four people on board, including a press photographer from ''[[Agderposten]]'' and the airport company's chairman, survived.<ref>{{cite news |title=Gullknapp for knapp |last=Selås |first=Jon |date=25 July 1996 |page=14 |work=[[Verdens Gang]] | language=Norwegian}}</ref> The cause of the accident was that the aircraft was overloaded.<ref name=asphalt />

==References==
{{reflist|30em}}
{{commons category|Arendal Airport, Gullknapp}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}
{{Use dmy dates|date=September 2014}}

[[Category:Airports in Aust-Agder]]
[[Category:Companies based in Aust-Agder]]
[[Category:Froland]]
[[Category:Companies established in 1984]]
[[Category:Airports established in 1996]]
[[Category:1996 establishments in Norway]]