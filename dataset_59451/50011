{{Infobox character
| name        = Bronn
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Jerome Flynn]]<br />(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Bronn-Jerome Flynn.jpg
| caption     = [[Jerome Flynn]] as Bronn
| first       = '''Novel''': <br />''[[A Game of Thrones]]'' (1996) <br />'''Television''': <br />"[[Cripples, Bastards, and Broken Things]]" (2011)
| last        = 
| occupation  =
| title       = Commander of the City Watch<br>Lord Protector of Stokeworth {{small|(books)}}
| alias       = Ser Bronn of the Blackwater, The Cutthroat 
| gender      = Male
| family      = 
| spouse      = Lollys Stokeworth {{small|(books)}}
| children    = 
| relatives   = 
| nationality = [[Westeros]]i
}}

'''Bronn''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.

Introduced in 1996's ''[[A Game of Thrones]]'', Bronn is a low-born sellsword of great skill and cunning from the kingdom of [[Westeros]]. He subsequently appeared in Martin's ''[[A Clash of Kings]]'' (1998) and ''[[A Storm of Swords]]'' (2000).

Bronn is portrayed by [[Jerome Flynn]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/bronn/bio/bronn.html |title=''Game of Thrones'' Cast and Crew: Bronn played by Jerome Flynn |publisher=[[HBO]] | accessdate=December 29, 2015}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|work=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html |title=From HBO |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20160307150640/http://grrm.livejournal.com/164794.html |archivedate=2016-03-07 |df= }}</ref>

== Character description ==
Bronn is sarcastic, with a black sense of humor, and a pragmatic, amoral philosophy for life. However, he is not completely heartless, nor is he sadistic. He openly expresses sympathy to [[Tyrion Lannister]] after making the pragmatic decision not to champion him during his second trial by combat. After Tyrion asks him if he would murder an innocent baby in front of her mother without question, Bronn denies it and claims that he would ask for a price, implying that he would demand a very high price for such a despicable deed. Despite Bronn's avaricious nature, which is sneered at by more honorable knights, he is a skilled and dangerous fighter.<ref>{{cite web|url=http://viewers-guide.hbo.com/guide/houses/jbaratheon/bronn/|title=Game of Thrones Viewer's Guide|publisher=}}</ref>

== Overview ==
Bronn is not a [[Narration#Third-person|point of view]] character in the novels, so his actions are witnessed and interpreted through the eyes of other people, such as [[Catelyn Stark]] and [[Tyrion Lannister]]. Bronn is mostly a background character in the novels.<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=}}</ref>

== Storylines ==

===In the books===
Bronn is a skilled [[mercenary|sellsword]] of low birth. Debuting in ''A Game of Thrones'', he helps [[Catelyn Stark]] escort her prisoner, [[Tyrion Lannister]], to [[the Eyrie]], presumably in the hope of a reward. During the journey, he befriends Tyrion and also demonstrates his skill with a sword when they are attacked by wildlings.{{Sfn|''A Game of Thrones''|loc=Chapter 34: Catelyn VI}}{{Sfn|''A Game of Thrones''|loc=Chapter 40: Catelyn VII}}  Bronn later accepts Tyrion's offer to champion for him in a [[trial by combat]], recognizing there is more gain in helping Tyrion. He wins the duel against Lysa Arryn's champion, Ser Vardis Egen. Bronn wears minimal armor and uses his superior skill and speed to easily beat him. He becomes Tyrion's personal bodyguard and accompanies him to the camp of [[Tywin Lannister|Tywin Lannister's]] army, and later King's Landing, serving as his captain of the guard and right-hand man. When [[Stannis Baratheon]] attacks the capital, Bronn is knighted for his defense of the city, taking up the name "Ser Bronn of the Blackwater" and taking a green burning chain as his personal [[seal (emblem)|sigil]] in commemoration of his role in the battle. In ''A Storm of Swords'', Tyrion is accused of murdering King [[Joffrey Baratheon]] and asks Bronn to champion him in a second trial by combat, this time against the monstrous [[Gregor Clegane]]. Bronn recognizes that although he can conceivably win, the benefits aren't worth the risks, and declines. Instead he takes up [[Cersei Lannister|Cersei's]] offer to marry Lollys of the wealthy House Stokeworth, an unmarried woman who is pregnant from being raped during a riot. When his wife gives birth, Bronn names his stepson Tyrion in dubious honor of his former employer. Cersei tells Lollys' brother-in-law Balman Byrch to kill Bronn, fearing he is in league with Tyrion. However, Bronn wins a duel against Balman and kills him instead. After all members of House Stokeworth ahead of his wife die under mysterious circumstances, Bronn takes control of the house and styles himself as Lord Protector of Stokeworth.

===In the TV show===
[[File:Jerome Flynn 2013 (cropped).jpg|thumb|right|upright|[[Jerome Flynn]] plays the role of Bronn in the [[Game of Thrones|television series]].]]
While he is a minor character in the book series, his role is greatly expanded in the TV show.
====First season====
Bronn initially serves under Catelyn Stark and aids her in arresting Tyrion Lannister and taking him to the Vale to stand trial for the murder of Jon Arryn and attempted murder of Bran Stark. During the trial, he volunteers to fight for Tyrion when he demands a trial by combat. Bronn defeats Lysa Arryn's champion and becomes Tyrion's companion and protector, accompanying him back to King's Landing.
====Second season====
Bronn's service to Tyrion earns him a position as Commander of the City Watch after his predecessor is revealed to be accepting bribes. When Stannis Baratheon attacks the city by sailing up the Blackwater Bay, he shoots a fire arrow to a ship containing wild fire which destroys half of Stannis' fleet, and kills several of the attackers in defence of the city.
====Third season====
Bronn is stripped of his position after Tywin Lannister takes his seat as Hand of The King, but is knighted for his service during the defense of King's Landing, taking the name Ser Bronn of Blackwater. He subsequently demands more gold for protecting Lord Tyrion and remains his confidante, though discord is increasing between the two. When Tyrion is forced to marry Sansa Stark, Bronn claims he desires her sexually, which Tyrion takes as a grave insult.
====Fourth season====
Tyrion pays Bronn to train Jaime Lannister in fencing with his left hand, as well as get Shae out of King's Landing, which he assures was completed. Bronn later implores Jaime to visit and help Tyrion after he is accused of murdering Joffrey, telling Jaime that Tyrion originally named him as his defender while on trial in the Vale before Bronn volunteered. Bronn is later offered betrothal to the wealthy House Stockworth by Cersei, if he does not champion Tyrion in trial by battle again. Bronn visits Tyrion in his cell to inform him, and tells him that he most likely would not have been prepared to fight Ser Gregor Clegane, Cersei's champion, anyway. He bids Tyrion farewell and they part ways as friends.

====Fifth season====
Jaime is sent to retrieve Myrcella Baratheon from Dorne and travels to Stokeworth to enlist Bronn, who is with his betrothed Lollys Stokeworth. Bronn reluctantly agrees to help Jaime after he is promised a prettier bride and a larger castle. Arriving at Dorne's Water Gardens, Jaime and Bronn rescue Myrcella before being confronted by Oberyn Martell's bastard daughters the Sand Snakes, who had intended to kill Myrcella. A fight ensues which is eventually broken up by Doran Martell's bodyguard Areo Hotah and the other palace guards, and Bronn is placed in a cell next to the Sand Snakes. In the cells, Tyene Sand taunts Bronn by exposing her breasts before revealing that she had given him a poison that activates when his heart rate increases, only giving him the antidote when he calls her the "most beautiful woman in the world". Bronn is eventually allowed to return to King's Landing with Jaime and Myrcella, though Trystane Martell insists that Bronn first be struck by Areo as punishment for knocking him out during Myrcella's rescue.

====Sixth season====
After Brynden "Blackfish" Tully captures Riverrun from House Frey, Jaime has Bronn to accompany him to Riverrun to assist in directing the siege. When the siege is lifted, Bronn joins the Lannister army at a feast at House Frey's home The Twins. He is noticeably disgruntled when several female servants appear smitten with Jaime, unaware one is secretly a disguised [[Arya Stark]]. He returns to King's Landing with Jaime, and, like Jaime, is shocked to discover that Cersei's machinations have destroyed the Sept of Baelor in their absence.

== TV adaptation ==
Bronn is played by the English actor and singer [[Jerome Flynn]] in the television adaption of the series of books.<ref>{{cite web|url=http://www.telegraph.co.uk/culture/tvandradio/10693120/Game-of-Thrones-stars-Lena-Headey-and-Jerome-Flynn-not-on-speaking-terms.html|title='Game of Thrones: Jerome Flynn on Bronn|work=Telegraph}}</ref>

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Bronn}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional knights]]
[[Category:Fictional mercenaries]]
[[Category:Fictional swordsmen]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]