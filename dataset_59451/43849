<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Villiers 31
 | image=Villiers 31.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Eight passenger [[airliner]]
 | national origin=[[France]]
 | manufacturer=[[Villiers aircraft|Ateliers d'Aviation François Villiers]]
 | designer=
 | first flight=1930
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1 or 2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Villiers 31''' or '''Villiers 310''' was a [[France|French]] eight passenger airliner of advanced construction.  Owing to Villiers' financial failure, it was not developed.

==Design== 
Ateliers Villier's Type 31 was their last aircraft before they collapsed in 1931.<ref name=GunstonMan/> A single engine [[monoplane]] of mixed construction, it was also their only passenger aircraft.

It was a [[high wing]] aircraft; the wing had constant [[chord (aeronautics)|chord]] out to rounded tips and had a wooden structure with [[plywood]] skinning. The [[Villiers XXIV]] had been the first French aircraft maker to use [[leading edge slats|Handley Page slats]] and the [[Villiers 26]] reconnaissance [[seaplane]] used a combination of automatically opening slats on the [[leading edge]] in front of the [[ailerons]] and another set which the pilot opened as he lowered the flaps.  The Type 31 had a similar combination of automatic and commanded slats.<ref name=LaAe/>

The Villiers 31 had a flat sided [[fuselage]] of rectangular cross-section behind the wing, built around a frame of [[chrome steel]] tubes and [[aircraft fabric covering|fabric covered]].<ref name=LaAe/> It was the first aircraft to have an autogenically welded structure, that is welded without the use of a filler metal.<ref name=LaeroSal/> There was a {{convert|420|hp|kW|abbr=on|order=flip}} [[Gnome et Rhône 9Ab]] nine-cylinder [[radial engine]] in the nose, which some photographs show under a long-chord, close-fitting, circular [[aircraft fairing#Engine cowling|cowling]]. Others show it uncowled. The two crew sat in a [[cockpit]] at the wing leading edge, the wing itself raised a little above the general fuselage line on a low fairing over the cabin, which seated eight and was lit by long strips of transparencies on each side.  Access was via a rear starboard side door.<ref name=LaAe/>

Like the rest of the fuselage the [[empennage]] was steel framed and fabric covered. The [[tailplane]]s were mounted just below the top of the [[fuselage]], each braced from below with a single strut and carrying a [[elevator (aeronautics)|balanced elevator]]. A curved, deep, [[balanced rudder]], mounted on a small [[fin]] and slightly pointed on top, worked in elevator cut-outs. The airliner had a fixed tailwheel [[undercarriage (aeronautics)|undercarriage]]. The two mainwheels were mounted independently on V-struts from the lower fuselage with near-vertical [[oleo strut]]s to the wing roots. The mainwheels had brakes and the tailwheel was steerable.<ref name=LaAe/>

==Operational history==

The exact date of the first flight of the Villiers 31 is not known, though all reports on it are from 1930.<ref name=LaAe/><ref name=LaeroSal/><ref name=Cont/> On 13 March 1930 the Air Ministry concluded a contract for two Type 31s;<ref name=Cont/> one of them (F-AKCR) was certainly built.<ref name=LaeroSal/>

==Specifications==
{{Aircraft specs
|ref=L'année aéronautique 1930-31. p.61<ref name=LaAe/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=two
|capacity=eight
|length m=14.10
|length note=
|span m=19.20
|span note=
|height m=4.08
|height note=
|wing area sqm=56
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=1950
|empty weight note=
|gross weight kg=3540
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|400|kg|lb|abbr=on}} fuel and oil
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Gnome et Rhône 9Ab]]
|eng1 type=9-cylinder [[radial engine|radial]]
|eng1 hp=420
|eng1 note=<ref name=Cont/>
|power original=
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=180
|max speed note=at sea level
|cruise speed kmh=160
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=3500
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude= 1 hr to ceiling
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=

}}

==References==
{{reflist|refs=

<ref name=GunstonMan>{{cite book |last=Gunston |first=Bill |title=World Encyclopaedia of Aircraft Manufacturers: from the pioneers to the present day  |year=1993|publisher=Patrick Stephens Limited|location=Sparkford, Somerset|isbn=9 781852 602055|page=318}}</ref>

<ref name=LaAe>{{cite journal |last=Hirshauer |first=L. |author2=Ch. Dolfus |title=Villiers 310|journal=L'année aéronautique|volume=1930-31 |issue=12|year=1931|pages=61–2|publisher= Dunod |location=Paris| url=http://gallica.bnf.fr/ark:/12148/bpt6k65676432/f70}}</ref>

<ref name=LaeroSal>{{cite journal|date=1930 |title=Les Tendences actuelle de la construction aéronautique|journal=L'Aérophile-Salon |pages=92, 93, 96|url=http://gallica.bnf.fr/ark:/12148/bpt6k6553807c/f89}}</ref>

<ref name=Cont>{{cite journal|date=13 March 1930 |title= Rapport - Ministère de l'Air|journal=Senat |issue=124|pages=48|url=http://gallica.bnf.fr/ark:/12148/bpt6k6573035g/f48}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Villiers aircraft}}

[[Category:French airliners 1930–1939]]
[[Category:Villiers aircraft]]