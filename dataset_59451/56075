{{refimprove|date=October 2011}}
{{Infobox magazine
| title = Callaloo
| image_file = File:Callaloo.gif
| editor = Charles Henry Rowell
| editor_title = [[Editor-in-chief]]
| category = [[African-American studies]], [[literature]], [[African studies]]
| publisher = [[Johns Hopkins University Press]]
| country = United States
| frequency = Quarterly
| firstdate = 1976
| website = {{URL|http://callaloo.tamu.edu/}}
| oclc = 41669989
| issn = 0161-2492
| eissn = 1080-6512
}}
'''''Callaloo, A Journal of African Diaspora Arts and Letters''''', is a quarterly [[literary magazine]] that was established in 1976<ref>{{cite web|title=Top 50 Literary Magazine|url=http://www.everywritersresource.com/topliterarymagazines.html|work=EWR|accessdate=August 17, 2015}}</ref> by Charles Rowell, who remains its [[editor-in-chief]]. It contains creative writing, visual art, and critical texts about literature and culture of the [[African diaspora]], and is probably the longest continuously running African-American literary magazine.<ref>[http://www.clmp.org/about/newswire_archive_01_01.html "Eminent African American Literary journal Celebrates 25th Year"]. CLMP Newswire</ref>

In addition to receiving grants of support from national agencies such as the [[Lannan Foundation]] and the [[National Endowment for the Arts]], ''Callaloo'' has garnered a number of national honors, including the best special issue of a journal from the [[Council of Editors for Learned Journals]] for "The Haitian Issues" in 1992 (volume 15.2 & 3: Haiti: the Literature and Culture Parts I & II); an honorable mention for the "Best Special Issue of a Journal" in 2001 from the Professional/Scholarly Publishing (PSP) Division of the American Association (volume 24.1: The Confederate Flag Controversy: A Special Section); and recognition for the Winter 2002 issue from the Council of Editors for Learned Journals as one of the best special issues of that year (volume 25.1: Jazz Poetics).{{Citation needed|date=February 2011}}

==See also==
* [[List of literary magazines]]
* [[African-American literature]]
* [[African American culture]]

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://callaloo.tamu.edu/}}
*[http://www.press.jhu.edu/journals/callaloo/ ''Callaloo'' on the Johns Hopkins University Press website]
*[http://muse.jhu.edu/journals/callaloo/ ''Callaloo'' ] at [[Project MUSE]]
*[http://www.jstor.org/journal/callaloo ''Callaloo'']  at [[JSTOR]]


{{DEFAULTSORT:Callaloo}}
[[Category:American literary magazines]]
[[Category:American studies]]
[[Category:Magazines established in 1976]]
[[Category:African-American studies publications]]
[[Category:American quarterly magazines]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:English-language magazines]]
[[Category:African-American magazines]]
[[Category:Magazines published in Maryland]]


{{Africandiaspora-stub}}