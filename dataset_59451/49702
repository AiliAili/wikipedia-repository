{{infobox book | 
| name          = L'Assommoir
| title_orig    = 
| translator    = 
| image         = Lassommoir.jpg
| image_size    = 175px
| caption = Cover of 1877 Charpentier edition of ''L'Assommoir''
| author        = [[Émile Zola]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Les Rougon-Macquart]]
| genre         = [[Novel]]
| publisher     = 
| release_date  = 1877
| english_release_date =
| media_type    = Print ([[Serial (literature)|Serial]], [[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| isbn          = 
| preceded_by   = [[Son Excellence Eugène Rougon]]
| followed_by   = [[Une Page d'amour]]
}}
'''''L'Assommoir''''' {{IPA-fr|lasɔmwaʁ|}} (1877) is the seventh novel in [[Émile Zola]]'s twenty-volume series ''[[Les Rougon-Macquart]]''. Usually considered one of Zola's masterpieces, the novel—a study of [[alcoholism]] and poverty in the working-class districts of Paris—was a huge commercial success and helped establish Zola's fame and reputation throughout France and the world.

==Plot summary==

The novel is principally the story of Gervaise Macquart, who is featured briefly in the first novel in the series, ''[[La Fortune des Rougon]]'', running away to Paris with her shiftless lover Lantier to work as a washerwoman in a hot, busy laundry in one of the seedier areas of the city. 

''L'Assommoir'' begins with Gervaise and her two young sons being abandoned by Lantier, who takes off for parts unknown with another woman. Though at first she swears off men altogether, eventually she gives in to the advances of Coupeau, a [[teetotal]] roofing engineer, and they are married. The marriage sequence is one of the most famous set-pieces of Zola's work; the account of the wedding party's impromptu and chaotic trip to the [[Louvre]] is one of the novelist's most famous passages. Through a combination of happy circumstances, Gervaise is able to realise her dream and raise enough money to open her own laundry. The couple's happiness appears to be complete with the birth of a daughter, Anna, nicknamed [[Nana (novel)|Nana]] (the protagonist of Zola's later novel of the same title).

However, later in the story, we witness the downward trajectory of Gervaise's life from this happy high point. Coupeau is injured in a fall from the roof of a new hospital he is working on, and during his lengthy convalescence he takes first to idleness, then to gluttony and eventually to drink. In only a few months, Coupeau becomes a vindictive, wife-beating alcoholic, with no intention of trying to find more work. Gervaise struggles to keep her home together, but her excessive pride leads her to a number of embarrassing failures and before long everything is going downhill. Gervaise becomes infected by her husband’s newfound laziness and, in an effort to impress others, spends her money on lavish feasts; leading to uncontrolled debt.

The home is further disrupted by the return of Lantier, who is warmly welcomed by Coupeau - by this point losing interest in both Gervaise and life itself, and becoming seriously ill. The ensuing chaos and financial strain is too much for Gervaise, who loses her laundry-shop and is sucked into a spiral of debt and despair. Eventually, she too finds solace in drink and, like Coupeau, slides into heavy alcoholism. All this prompts Nana - already suffering from the chaotic life at home and getting into trouble on a daily basis - to run away from her parents' home and become a streetwalker. 

Gervaise’s story is told against a backdrop of a rich array of other well drawn characters with their own vices and idiosyncrasies. Notable amongst these being Goujet, a young metal worker, who wastes his life in unconsummated love of the hapless laundress. 

Eventually, sunk by debt, hunger and alcohol Coupeau and Gervaise both die. The latter’s corpse lying for days in her unkempt hovel before it is even noticed by her disdaining neighbours. 

[[File:Émile Zola's L'Assomoir by Augustin Daly.jpg|thumb|left|1879 poster for an American theatre production of ''L'Assommoir'' by [[Augustin Daly]]]]

==Themes and criticism==
Zola spent an immense amount of time researching Parisian street argot for his most realistic novel to that date, using a large number of obscure contemporary slang words and curses to capture an authentic atmosphere. His shocking descriptions of conditions in working-class 19th-century Paris drew widespread admiration for his realism, as it still does. L'Assommoir was taken up by [[Temperance movement|temperance]] workers across the world as a tract against the dangers of alcoholism, though Zola always insisted there was considerably more to his novel than that. The novelist also drew criticism from some quarters for the depth of his reporting, either for being too coarse and vulgar or for portraying working-class people as shiftless drunkards. Zola rejected both these criticisms out of hand; his response was simply that he had presented a true picture of real life.

==The title==
The title ''L'Assommoir'' cannot be properly translated into English. It is adapted from the French verb "assommer" meaning to stun or knock out. The noun is a colloquial term popular in late nineteenth-century Paris, referring to a shop selling cheap liquor distilled on the premises "where the working classes could drown their sorrows cheaply and drink themselves senseless".<ref>Douglas Parmée, "Introduction", in Émile Zola, ''Nana'' (Oxford University Press, 2009), p. xii note)</ref> Perhaps the closest equivalent terms in English are the slang adjectives "[[Wiktionary:hammered|hammered]]" and "[[Wiktionary:plastered|plastered]]". In the absence of a corresponding noun, English translators have rendered it as ''The Dram Shop'', ''The Gin Palace'', ''The Drunkard'', and ''The Drinking Den''. Most translators choose to retain the original French title.

==Translations==
''L'Assommoir'' has often been translated, and there are several unexpurgated editions widely available. Among them is ''The Drinking Den'', translated by Robin Buss.<ref>(London/New York: Penguin Books: 2003: ISBN 014044954X)</ref> In the 1880s, [[Ernest Alfred Vizetelly]] produced an English language translation of the novel with some edits designed not to offend the sensibilities of British audiences. Along with his [[Henry_Vizetelly|father]], Vizetelly had translated a number of Zola's books. However, the salacious nature of Zola's work (even in edited form) would see the Vizetellys vilified in parliament and later prosecuted for obscenity.<ref>Robin Buss, The Guardian: [https://www.theguardian.com/books/2002/sep/28/classics.emilezola Emile Zola - Apostle of the gutter]</ref>

==Adaptations==
The American film ''[[The Struggle (film)|The Struggle]]'' (1931), directed by [[D. W. Griffith]], is a loose adaptation of the novel.
 
The French film ''[[Gervaise (film)|Gervaise]]'' (1956), directed by [[René Clément]], is an adaptation of the novel.

==References==
{{Reflist}}

==External links==
*[https://archive.org/search.php?query=title%3AAssommoir%20creator%3Azola%20-contributor%3Agutenberg ''L'Assommoir''], available at [[Internet Archive]] (English, illustrated scanned books)
{{Gutenberg|no=8558|name=L'Assommoir}} (English, HTML and plain text)
{{Gutenberg|no=6497|name=L'assommoir}} (French, HTML and plain text)
* {{fr}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/zola-emile-lassommoir.html/ L'Assommoir, audio version ]
*[http://www.polyglotproject.com/books/French/lassommoir L'Assommoir] in French with English translation
* {{librivox book | title=L'Assommoir | author=Émile ZOLA}}

{{Les Rougon-Macquart}}

{{Authority control}}

{{DEFAULTSORT:Assommoir}}
[[Category:1877 novels]]
[[Category:Novels by Émile Zola]]
[[Category:Books of Les Rougon-Macquart]]
[[Category:19th-century French novels]]
[[Category:French novels adapted into films]]
[[Category:Novels set in Paris]]