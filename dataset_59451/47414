{{Infobox power station
| name                = De Aar Solar Power
| name_official       = 
| image               = 
| image_size          = 
| image_caption       = 
| image_alt           = 
| location_map        = South Africa
| location_map_size   = 
| location_map_caption= Location of De Aar Solar Power in South Africa
| location_map_alt    = 
| coordinates = {{coord|30|37|30|S|24|1|46|E|region:ZA-NC_type:landmark|display=inline,title}}
| country             = South Africa
| location            = [[De Aar]], [[Northern Cape Province]]
| status              = O
| construction_began  = 
| commissioned        = 2014
| decommissioned      = 
| cost                =
| owner               = 
| operator            = Globeleq
| solar_type          = 
| solar_cpv_concentration= 
| solar_csp_technology= 
| solar_csp_heliostats= 
| solar_cpvt          = 
| solar_site_area     = 
| ps_units_operational= 
| ps_units_manu_model = 
| ps_units_uc         = 
| ps_units_planned    = 
| ps_units_decommissioned= 
| ps_electrical_capacity= 48&nbsp;[[Megawatt|MW]]<sub>p</sub>
| ps_electrical_cap_fac= 
| ps_annual_generation= 89.4&nbsp;[[Gigawatt-hour|GWh]]
| th_cogeneration     = 
| th_combined_cycle   = 
| website             = 
| extra               = 
}}
'''De Aar Solar Power''' is located 6&nbsp;km outside the town of [[De Aar]] in the [[Northern Cape Province]] of [[South Africa]]. The facility is based on over 100 hectares of [[Emthanjeni Local Municipality|Emathanjeni Municipal]] land, and comprises 167 580 [[Solar panel|solar (PV) panels]]. De Aar Solar Power supplies [[Eskom]] with 85 458 MWh of renewable electrical energy per year; enough to power more than 19 000 average South African households.<ref>{{cite web|title=DE AAR SOLAR POWER INAUGURATION & FOCUS ON ENRICHING COMMUNITIES|url=http://www.sapvia.co.za/de-aar-solar-power-inauguration-focus-on-enriching-communities/|website=www.sapvia.co.za}}</ref>

De Aar Solar Power signed a 20-year Power Purchase Agreement with Eskom as part of the South African Government’s [http://www.ipprenewables.co.za/ Renewable Energy Independent Power Producer Procurement Programme]<ref>{{cite news|title=An Update On The Renewable Energy Independent Power Producer Procurement Programme|url=http://www.leadershiponline.co.za/articles/an-update-on-the-renewable-energy-independent-power-producer-procurement-programme-10254.html|work=www.leadershiponline.co.za}}</ref> (REIPPP) and was one of the first solar power projects in South Africa.<ref>{{cite news|last1=Kolver|first1=Leandi|title=De Aar Solar project starts supplying power into grid|url=http://www.engineeringnews.co.za/article/de-aar-solar-project-starts-supplying-power-into-grid-2013-12-10}}</ref> The project also has an Implementation Agreement with the [[Department of Energy (South Africa)|South African Department of Energy.]]

The project received full Environmental Authorisation from the [[Department of Environmental Affairs]] in 2011.

== Background ==

[[Mainstream Renewable Power|Mainstream Renewable Energy]]<ref>{{cite news|title=TOP 10 - Commissioned solar PV projects|url=http://www.solarfinancesolutions.com/commissioned-projects/|work=Solar Finance Solutions Africa}}</ref> was responsible for the construction management of the De Aar solar facility, which took place between December 2012 and mid-2014.

De Aar Solar Power is owned by a consortium comprising: [http://www.globeleq.co.za/ Globeleq], [http://www.thebe.co.za/ Thebe Investment Corporation], [[Mainstream Renewable Power]], The Sibona Ilanga Trust,<ref>{{cite web|title=Trustees will boost communities|url=http://www.news24.com/SouthAfrica/Local/Express-News/trustees-will-boost-communities-20160223|website=News24}}</ref> [http://www.enzani.co.za/ Enzani Technologies] and Usizo Engineering.<ref>{{cite news|last1=Reporter|first1=Creamer Media|title=Globeleq Inaugurates South African Solar Projects|url=http://www.engineeringnews.co.za/article/globeleq-inaugurates-south-african-solar-projects-2014-05-20/rep_id:4136|work=Engineering News}}</ref> Globeleq and Siemens Energy Sector jointly manage the project.<ref>{{cite news|last1=Creamer|first1=Terence|title=50 MW De Aar solar project at peak-construction stage|url=http://www.engineeringnews.co.za/article/50-mw-de-aar-solar-project-at-peak-construction-stage-2013-06-04}}</ref>

== Solar panels ==

The 167 580 Suntech STP 290/295 Watt solar panels at De Aar Solar Power each with a capacity of 290 and 295 Watt<ref>{{cite news|last1=Creamer|first1=Terence|title=50 MW De Aar solar project at peak-construction stage|url=http://www.engineeringnews.co.za/article/50-mw-de-aar-solar-project-at-peak-construction-stage-2013-06-04|work=Engineering News}}</ref> and measure 1 x 2m. The inverters used at this facility are SINVERT PVS2400.

== Local community ==

Benefits for the local community arising from the construction and operation of De Aar Solar include a number of enterprise development and socio-economic development programmes, as well as local procurement and employment opportunities.<ref>{{cite web|title=Local Enterprises Receive Support from De Aar PV Project|url=http://www.greenbusinessguide.co.za/local-enterprises-receive-support-from-de-aar-pv-project/|website=The Green Business Guide|date=21 April 2015}}</ref>

Some of the projects supported include: [http://www.farrsa.org.za/ Foundation for Alcohol Related Research], Early Childhood Development,<ref>{{cite news|title=ECD practitioners complete training|url=http://www.news24.com/SouthAfrica/Local/Express-News/ECD-practitioners-complete-training-20150615|work=News24}}</ref> Reading Coach Programme,<ref>{{cite news|title=Solar farms promote literacy|url=http://www.news24.com/southafrica/local/express-news/solar-farms-promote-literacy-20160405|work=News24}}</ref> Telematics Programme, Gentle Care Centre, and an Engineering Scholarship Fund<ref>{{cite news|title=Youth bursary fund launched|url=http://www.news24.com/SouthAfrica/Local/Express-News/Youth-bursary-fund-launched-20150728|work=News24}}</ref>

== See also ==
{{stack|{{Portal|South Africa|Sustainable development|Renewable energy}}}}
* [[List of power stations in South Africa]]
* [[Solar power]]
* [[Environmental impact of solar power]]
* [[Sustainable energy]]

==References==
{{reflist}}

{{Power in South Africa |state=expanded}}

[[Category:Photovoltaic power stations in South Africa]]
[[Category:Proposed power stations in South Africa]]