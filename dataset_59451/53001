{{Infobox journal
| title = The Modern Language Journal
| cover = 
| editor = Heidi Byrnes
| discipline = [[Linguistics]], [[education]]
| former_names = 
| abbreviation = Mod. Lang. J.
| publisher = [[Wiley-Blackwell]] on behalf of the National Federation of Modern Language Teachers Associations
| country = United States
| frequency = Quarterly
| history = 1916-present
| openaccess = 
| license = 
| impact = 1.914
| impact-year = 2009
| website = http://mlj.miis.edu/
| link1 =  http://www.blackwellpublishing.com/journal.asp?ref=0026-7902
| link1-name = Journal page at publisher's website
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291540-4781
| link2-name = Online access
| JSTOR = 00267902
| OCLC = 615546163
| LCCN = 37024312
| CODEN = 
| ISSN = 0026-7902
| eISSN = 1540-4781
}}
'''''The Modern Language Journal''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf of the National Federation of Modern Language Teachers Associations. It covers research and discussion about the learning and teaching of foreign and [[second language]]s.<ref>[http://mlj.miis.edu/about.htm About the journal]</ref>

Types of articles published include documented essays, research studies using quantitative/qualitative methodologies, response articles, and editorials that challenge paradigms of language learning and teaching.<ref>[http://www.blackwellpublishing.com/journal.asp?ref=0026-7902 Journal page at publishers website]</ref> The journal has a News & Notes of the Profession section offering a calendar of events, professional announcements, initiatives, and concerns. The journal also provides a list of relevant articles in other professional journals, and reviews of scholarly books, monographs, and computer software. An annual survey of doctoral degrees granted in foreign languages, literatures, cultures, linguistics, and foreign language education in the United States is available on the journal's website.

Since 2007, the journal issues a fifth issue in addition to the regular four issues published every year. This additional issue is a focus issue or a [[monograph]] in alternating years.

== References ==
{{reflist}}

== External links ==
* {{Official|http://mlj.miis.edu/}}

{{DEFAULTSORT:Modern Language Journal, The}}
[[Category:Linguistics journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1916]]