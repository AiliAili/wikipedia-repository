{{Infobox journal
| italic title = force
| title = Psychology of Aesthetics, Creativity, and the Arts
| cover = [[File:Psychology of Aesthetics Creativity and the Arts journal cover.gif]]
| editor = Roni Reiter-Palmon, Pablo Tinio
| discipline = 
| abbreviation = 
| publisher = [[American Psychological Association]]
| country =
| frequency = Quarterly
| history = 2007-present
| openaccess =
| impact = 3.054
| impact-year = 2014
| website = http://www.apa.org/pubs/journals/aca/index.aspx
| link1 = http://content.apa.org/journals/aca
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR =
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 1931-3896
| eISSN = 1931-390X
}}
'''''Psychology of Aesthetics, Creativity, and the Arts''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]] on behalf of Division 10. The journal covers research on the [[psychology]] of the production and appreciation of [[the arts]] and all aspects of creative endeavor.<ref>{{cite web |date=22 October 2012 | url=http://www.apa.org/pubs/journals/aca/index.aspx |title=Psychology of Aesthetics, Creativity|publisher=[[American Psychological Association]] |accessdate=2012-10-22}}</ref> The current [[editors-in-chief]] are Roni Reiter-Palmon ([[University of Nebraska at Omaha]]) and Pablo Tinio ([[Queens College, City University of New York]]). The founding co-editors of the journal were Jeffrey Smith, Lisa Smith, and [[James C. Kaufman]].

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.054, ranking it 17th out of 85 journals in the category "Psychology, Experimental".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Psychology, Experimental |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.apa.org/pubs/journals/aca/index.aspx}}

[[Category:Psychology of creativity journals]]
[[Category:American Psychological Association academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2007]]