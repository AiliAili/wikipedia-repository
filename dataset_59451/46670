{{Infobox college coach
| name               = Brian Boyer
| image              = Brian Boyer 2014.jpg
| alt                = 
| caption            = 
| sport              = [[Women's basketball]]
| current_title      = [[Head coach]]
| current_team       = [[Arkansas State]]
| current_conference = 
| current_record     = 306-254 ({{winning percentage|306|254}})
| contract           =
| birth_date         = {{birth date and age|1969|10|24|mf=yes}}
| birth_place        = [[Memphis, Missouri]]
| death_date         = 
| death_place        = 
| alma_mater         = 
| player_positions   =
| overall_record     = 
| bowl_record        = 
| tournament_record  = 
| championships      = Sun Belt Conference Regular Season Championship (2003-04; 2013-14; 2015-16)
| awards             = 4x Sun Belt Conference Coach of the Year (2003-04; 2004-05; 2013-14; 2015-16)
| coaching_records   = 
| medaltemplates     = 
| player_years1      = 
| player_team1       = 
| coach_years1       = 1989-1993
| coach_team1        = [[Missouri Western]] (asst. men's program)
| coach_years2       = 1993-1995
| coach_team2        = [[Missouri Western]] (asst. women's program)
| coach_years3       = 1995-1999
| coach_team3        = [[Arkansas State]] (asst.)
| coach_years4       = 1999–Present
| coach_team4        = [[Arkansas State]]
| coach_years5       = 
| coach_team5        = 
}}

'''Brian Boyer''' (born October 24, 1969) is an American college basketball coach and the current head coach of the women’s basketball team at [[Arkansas State University]].

Boyer has been the head coach at A-State for 18 years and has compiled an overall record of 306-254 and has won more games than any coach (men’s or women’s) in the school’s history.<ref>{{cite web|url=http://web1.ncaa.org/stats/StatsSrv/careercoach |title=NCAA® Career Statistics |website=Web1.ncaa.org |date=1999-03-20 |accessdate=2016-05-20}}</ref>

==Early life and education==
Boyer was born in [[Memphis, Missouri]], a small farming community in the Northern part of the state. He attended Missouri Western where he earned a bachelor's degree in Physical Education in 1993.<ref name="astateredwolves1">{{cite web|url=http://www.astateredwolves.com/ViewArticle.dbml?ATCLID=204188 |title=Head Coach Brian Boyer - Arkansas State Athletics Official Web Site |website=Astateredwolves.com |date= |accessdate=2016-05-20}}</ref>

==Coaching career==
===Missouri Western===
Upon arriving at [[Missouri Western]] as a student, Boyer began working as assistant with the men’s program in 1989 and moved to assistant with the men’s program in 1993.<ref>{{cite web|url=https://arkansasstate.n.rivals.com/news/who-is-brian-boyer |title=Rivals |website=Arkansasstate.n.rivals.com |date= |accessdate=2016-05-20}}</ref>

During his time at Missouri Western, the Griffons made five [[NCAA Division II]] appearances (1990, 1991, 1992, 1994 and 1995) and captured two conference championships and advanced to the Sweet 16 in 1990 while with the men’s program.

After moving into his role with the women’s program, Missouri Western and Boyer continued their joint success as the program made two NCAA Tournament appearances, including a trip to the Elite Eight in 1994 and a Final Four berth in 1995.<ref name="astateredwolves1"/>

===Arkansas State===
In 1995, Boyer arrived at Arkansas State as an assistant under [[Jeff Mittie]], who is the current head coach of the Kansas State Wildcats. Upon Mittie’s departure in August 1999 to become the head coach at TCU,<ref>{{cite web|author= |url=http://www.gofrogs.com/sports/w-baskbl/mtt/jeff_mittie_83013.html |title=Jeff Mittie Bio - TCU Horned Frogs Official Athletic Site |website=Gofrogs.Com |date= |accessdate=2016-05-20}}</ref> Boyer was promoted to the position of head coach.

In his first season as head coach in 1999-00, Boyer guided A-State to an 18-12 overall record and a second straight trip to the [[Women's National Invitation Tournament|Women’s National Invitation Tournament]]. 

His teams returned to the postseason in 2003-04 and 2004-05, both of which resulted in appearances in the WNIT. During the 2005 tournament, A-State hosted Arkansas at the [[Convocation Center (Arkansas State University)|Convocation Center]] in front of the largest crowd in arena history at 10,892. A-State won the game 98-84 and advanced to quarterfinals where the squad fell to Iowa.<ref>{{Cite web|url=http://espn.go.com/espn/wire?section=ncw&id=2019343|title=New rivalry? Arkansas still hesitant to embrace Arkansas State|website=ESPN.com|access-date=2016-05-23}}</ref> In 2013-14 Boyer’s teams began a run of three consecutive WNIT berths that also included two regular season [[Sun Belt Conference]] championships. During the run the Red Wolves played in the Sun Belt Conference championship game twice and compiled 75 wins during the span.<ref name="astateredwolves1"/>

The 2013-14 season proved to be one of major significance for Boyer as he became the Sun Belt Conference’s all-time leader in league victories, passing former La Tech coach Leon Barmore and former Florida International coach Cindy Russo who each had 136 wins on the sidelines.<ref name="astateredwolves1"/>

Arkansas State missed the NCAA Tournament in 2014-15 despite compiling a record of 23-10 during the regular season.<ref>{{cite web|author= |url=http://www.usatoday.com/story/sports/ncaaw/2015/03/17/arkansas-state-women-miss-ncaa-tournament-to-play-in-wnit/24889641/ |title=Arkansas State women miss NCAA Tournament, to play in WNIT |website=Usatoday.com |date=2015-03-17 |accessdate=2016-05-20}}</ref> A-State also missed the NCAA Tournament at the end of 2015-16 season despite an 18-game winning streak<ref>{{cite web|url=http://www.astate.edu/news/a-state-women-s-basketball-claims-outright-sun-belt-conference-regular-season-championship |title=A-State Women’s Basketball Claims Outright Sun Belt Conference Regular Season Championship |website=Astate.edu |date=2016-02-25 |accessdate=2016-05-20}}</ref> and a 26-5 record during the regular season.

During the run, Boyer won two Sun Belt Conference Coach of the Year honors and helped Aundrea Gamble become the first player in league history to win three consecutive Student-Athlete of the Year honors.<ref>{{cite web|author= |url=http://www.thv11.com/sports/ncaa/arkansas-state/asus-gamble-becomes-first-three-time-sun-belt-womens-student-athlete-of-the-year/74011703 |title=ASU's Gamble becomes first three-time Sun Belt Women's Student-Athlete of the Year |website=THV11.com |date=2016-03-08 |accessdate=2016-05-20}}</ref>

==Coaching record==
{{CBB Yearly Record Start
| type=coach
| conference=
| postseason=
| poll=no
}}
{{CBB Yearly Record Subhead
| name=[[Arkansas State]]
| startyear=1999
| conference=[[Sun Belt Conference]]
| endyear=Present
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1999-00
 | name         = Arkansas State
 | overall      = 18-12
 | conference   = 8-8
 | confstanding = 5th
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2000-01
 | name         = Arkansas State
 | overall      = 14-14
 | conference   = 8-8
 | confstanding = T-6th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2001-02
 | name         = Arkansas State
 | overall      = 12-16
 | conference   = 6-8
 | confstanding = T-7th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2002-03
 | name         = Arkansas State
 | overall      = 12-18
 | conference   = 5-9
 | confstanding = 9th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = conference
 | season       = 2003-04
 | name         = Arkansas State
 | overall      = 19-10
 | conference   = 10-4
 | confstanding = T-1st
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2004-05
 | name         = Arkansas State
 | overall      = 21-11
 | conference   = 11-3
 | confstanding = T-2nd
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2005-06
 | name         = Arkansas State
 | overall      = 15-15
 | conference   = 7-7
 | confstanding = 6th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2006-07
 | name         = Arkansas State
 | overall      = 21-13
 | conference   = 11-7
 | confstanding = 5th
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2007-08
 | name         = Arkansas State
 | overall      = 20-12
 | conference   = 13-2
 | confstanding = 3rd
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2008-09
 | name         = Arkansas State
 | overall      = 16-14
 | conference   = 10-8
 | confstanding = T-2nd
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2009-10
 | name         = Arkansas State
 | overall      = 13-18
 | conference   = 7-11
 | confstanding = 7th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2010-11
 | name         = Arkansas State
 | overall      = 18-14
 | conference   = 9-7
 | confstanding = T-5th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2011-12
 | name         = Arkansas State
 | overall      = 12-18
 | conference   = 6-10
 | confstanding = 9th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2012-13
 | name         = Arkansas State
 | overall      = 15-15
 | conference   = 12-8
 | confstanding = 4th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = conference
 | season       = 2013-14
 | name         = Arkansas State
 | overall      = 22-12
 | conference   = 14-4
 | confstanding = 1st
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 2014-15
 | name         = Arkansas State
 | overall      = 24-11
 | conference   = 16-4
 | confstanding = 2nd
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = conference
 | season       = 2015-16
 | name         = Arkansas State
 | overall      = 27-6
 | conference   = 19-1
 | confstanding = 1st
 | postseason   = WNIT
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship =
 | season       = 2016-17
 | name         = Arkansas State
 | overall      = 7-25
 | conference   = 4-14
 | confstanding = 11th
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Subtotal
 | name       = Arkansas State
 | overall    = 306-254
 | confrecord = 176-126 ({{winning percentage|176|126}})
}}
{{CBB Yearly Record End
| overall      = 306-254 ({{winning percentage|306|254}})
| poll         = no
}}

==References==
{{Reflist}}

==External links==
* [http://www.astateredwolves.com/ViewArticle.dbml?ATCLID=204188 Coach Boyer's Bio]

{{DEFAULTSORT:Boyer, Brian}}
[[Category:1969 births]]
[[Category:Living people]]
[[Category:Articles created via the Article Wizard]]
[[Category:People from Missouri]]