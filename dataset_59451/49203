'''Hadrien Laroche''' (born 13 November 1963 in [[Paris]], [[France]]) is a French writer.

== Biography ==
Born in Paris in 1963, Hadrien Laroche is a former student of the [[Ecole normale supérieure]]. He was visiting professor at [[Dartmouth College]] (1985-1986) and a fellow of the [[School of Criticism and Theory]]. There he met [[Jacques Derrida]] and [[Patricia Williams]]. He completed his doctorate in philosophy under Derrida in 1996 at the [[Ecole des Hautes Etudes en Sciences Sociales]] (EHESS); Derrida considered Laroche, his last doctoral student, as "one of the most talented and original thinkers of his generation."<ref>[http://www.arsenalpulp.com/bookinfo.php?index=324]</ref>

==Works==
He has published essays on [[Jean Genet]], [[Paul Cézanne]], [[Marcel Duchamp]] ("La machine à signatures", ''Inculte'' #18, 2009), and three French-language novels—''Les Orphelins'' (Paris: Allia/J'ai Lu, 2005),<ref>[http://www.lexpress.fr/culture/livre/les-heretiques_811638.html]</ref> ''Les Heretiques'' (Paris: Flammarion, 2006), and ''La Restitution'' (Paris: Flammarion, 2009)<ref>[http://www.lexpress.fr/culture/livre/la-restitution_832142.html]</ref>]—which have placed him at the forefront of contemporary French writing.<ref>[http://www.dailymotion.com/video/xa0ife_hadrien-laroche-restitution-mediapa_news#.UMDlJY6PtOw]</ref>

For the centenary of Jean Genet's birth, Arsenal Pulp Press has published a translation of his essay ''The Last Genet, a writer in revolt'', translated by David Homel.<ref>[http://www.theglobeandmail.com/arts/books-and-media/the-last-genet-a-writer-in-revolt-by-hadrien-laroche/article4349837/]</ref> The book was presented at Nottingham Contemporary (UK).<ref>[http://www.nottinghamcontemporary.org/event/hadrien-laroche-last-genet-1968-1986]</ref> The book has been widely acclaimed by Bernard Henry Levy.<ref>[http://www.bernard-henri-levy.com/jean-genet-23628.html]</ref>

He has been correspondent for ''Les Inrockuptibles''. He has done interviews with [[Bret Easton Ellis]],<ref>[http://blogs.lesinrocks.com/25ans/2011/10/14/entretien-avec-bret-easton-ellis-en-1989/]</ref> [[Hubert Selby, Jr.]], and other French and American writers.

For the [[Maison des écrivains et des traducteurs étranger]] meetings, he has been the editor of the magazine for the meeting ''Le Caire/Vancouver'' (2008) and the meet no. 16 ''Quito/Dublin'' (2012). He has participated in a meeting no.6 and was the director of the centenary conference ''Pour Genet'', at the Abbaye de Fontevraud in 2010.<ref>[http://www.maisonecrivainsetrangers.com/Hadrien-Laroche-572.html]</ref>

== Bibliography ==

===English===
* ''Marcel Duchamp: The signature's machine'', trans. by Molleen Shilliday, in ''Breathless Days, 1959, 1960'', editors: Serge Guilbaut, John O Brian (Duke University Press, 2017). ISBN 978-0822360414.
* ''Orphans'', trans. by Jan Steyn and Caite Dolan-Leach (Dalkey Archive Press, 2014). ISBN 978-1628970029.
* ''The Last Genet: a writer in revolt'', trans. by David Homel (Arsenal Pulp Press, 2010). ISBN 978-1-55152-365-1.

===French===
;Novels
* 2009 ''La Restitution'', [[Groupe Flammarion|Flammarion]] ISBN 2081226472
* 2007 ''Les Hérétiques'', [[Groupe Flammarion|Flammarion]] ISBN 2080690124
* 2005 ''Les Orphelins'',  Allia, J'ai Lu. ISBN 2290355062
;Essays
* 2014 ''Duchamp Déchets, Les hommes, les objets, la catastrophe'', [[Editions du Regard]] ISBN 2841053253
* 2009 ''La Machine à signatures'', [[Marcel Duchamp]], ''Incultes'', #18
* 2001 ''Face à la Pente'', in [[Valère Novarina]], Théâtres du verbe, Corti
* 1997 ''Le Dernier Genet, Histoire des hommes infâmes'', Seuil, Fiction & Cie, nommé pour le [[prix Fémina]]ISBN 2020303485
;Non-fiction
* 1999 ''Le Miroir chinois'', Le Seuil. ISBN 2020342987
;poetry
* 1990 ''au pire'', mem/Arte Facts
;Articles
* "Court traité de la décision en deux pages", in [[Jacques Derrida]] L'événement déconstruction, [[Les Temps modernes]], n° 669-670, Gallimard,  juillet/déc. 2012.
* "Dans le tramway avec W.G Sebald", in Face à Sebald, édition [[inculte]], 2011, p.p.&nbsp;289-305.
* "[[Marcel Duchamp]]. La machine à signatures" in [[inculte]], n° 18, 2009, p.p 41-65.

==References==
{{Reflist}}
* http://www.seuil.com/livre-9782020303484.htm
* http://www.nottinghamcontemporary.org/event/hadrien-laroche-last-genet-1968-1986
* http://www.maisonecrivainsetrangers.com/
* http://www.maisonecrivainsetrangers.com/Hadrien-Laroche-572.html
* http://www.arsenalpulp.com/contributorinfo.php?index=291
* [[:fr:Hadrien Laroche]]
* http://www.dailymotion.com/video/xa0ife_hadrien-laroche-restitution-mediapa_news#.UMDji46PtOw
* http://www.franceculture.fr/personne-hadrien-laroche.html
* http://www.theglobeandmail.com/arts/books-and-media/the-last-genet-a-writer-in-revolt-by-hadrien-laroche/article4349837/
* http://www.nottinghamcontemporary.org/event/hadrien-laroche-last-genet-1968-1986
* http://www.magazine-litteraire.com/mensuel/437/hadrien-laroche-interrogation-radicale-responsabilite-01-12-2004-15501
* http://pigiconi.blogspot.ie/2011/07/le-dernier-genet-de-hadrien-laroche.html

{{portal|Literature}}

{{DEFAULTSORT:Laroche, Hadrien}}
[[Category:1963 births]]
[[Category:Writers from Paris]]
[[Category:20th-century French novelists]]
[[Category:21st-century French novelists]]
[[Category:French poets]]
[[Category:LGBT writers from France]]
[[Category:Jean Genet]]
[[Category:Dartmouth College faculty]]
[[Category:École Normale Supérieure alumni]]
[[Category:Jacques Derrida]]
[[Category:Living people]]
[[Category:LGBT poets]]
[[Category:LGBT novelists]]