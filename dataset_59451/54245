{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
| name     = Glengowrie
| city     = Adelaide
| state    = sa
| image    = Dog Park Glengowrie.jpg
| caption  = Hazelmere Reserve on the south-eastern boundary of Glengowrie
| lga      = City of Marion
| postcode = 5044
| est      = 
| pop      = 4,720 | pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref>{{Census 2006 AUS|id=SSC41636|name=Glengowrie (State Suburb)|quick=on|accessdate=28 February 2008}}</ref>
| area     = 1.9
| stategov = [[Electoral district of Morphett|Morphett]]
| fedgov   = [[Division of Hindmarsh|Hindmarsh]]
| near-nw  = [[Glenelg East, South Australia|Glenelg East]]
| near-n   = [[Glenelg East, South Australia|Glenelg East]]
| near-ne  = [[Camden Park, South Australia|Camden Park]]
| near-w   = [[Glenelg East, South Australia|Glenelg East]]
| near-e   = [[Morphettville, South Australia|Morphettville]]
| near-sw  = [[Somerton Park, South Australia|Somerton Park]]
| near-s   = [[Warradale, South Australia|Warradale]]
| near-se  = [[Oaklands Park, South Australia|Oaklands Park]]
| dist1    = 
| location1= 
}}
'''Glengowrie''' {{IPAc-en|ɡ|l|ɛ|n|.|ˈ|ɡ|aʊr|i}} is a suburb of the [[Australia]]n city of [[Adelaide]], approximately 12 kilometres south west of the city centre. The name Glengowrie means "[[Glen]] of Gowrie", so called in honour of [[Alexander Hore-Ruthven, 1st Earl of Gowrie|Lord Gowrie]] (formerly, Alexander Gore Arkwright Hore-Ruthven), [[Governor-General of Australia]] from 1936-1944.

==Location==
Located in the [[City of Marion]], Glengowrie is bounded by the [[Glenelg Tram|Glenelg tramline]] to the north, Morphett Road to the east, Oaklands road to the south and parts of Diagonal Road, Panton Crescent and Buttrose Street to the west.<ref>{{cite web |url=http://www.marion.sa.gov.au/webdata/resources/files/Community_Facilities_Map.pdf |title=Community Facilities Map |accessdate=20 August 2010 |publisher=City of Marion}}</ref> The western tip of Glengowrie is approximately 2 kilometres from the beaches of [[Glenelg, South Australia|Glenelg]], one of Adelaide's best known beachside precincts.

The suburb occupies an area of 1.9&nbsp;km<sup>2</sup>.{{Citation needed|date=August 2010}}

==Demography==
The suburb's population at the 2006 Census was approximately 4,720, of which slightly more than half (52.7%) were female. Around 15.9% of the suburb's residents were born overseas, with the five largest groups originating from [[England]] (6.6%), [[Scotland]] (1.1%), [[New Zealand]] (0.7%), [[Ireland]] (0.6%), and the [[Netherlands]] (0.5%). The most common languages other than [[Australian English|English]] spoken at home were Greek (0.8%), Italian (0.6%), Serbian (0.3%), German (0.3%) and Japanese (0.3%).

There were 2,104 occupied private dwellings in the suburb, including: 1,352 detached, 468 semi-detached, 281 flats or apartments, and 3 'other'. Approximately 40.1% of occupied private dwellings were fully owned, 31.6% were being purchased and 19.2% were rented. The median weekly household income was $919, compared with $924 in Adelaide overall.

==Governance==
Glengowrie falls under the local-level governance of the [[City of Marion]]. It lies in the state electoral district of [[Electoral district of Morphett|Morphett]] and the federal electoral division of [[Division of Hindmarsh|Hindmarsh]].

==Amenities==

* The Hazelmere Reserve is a public park on the southern edge of Glengowrie. It features the popular but controversial fenced off area known as "Dog Park" which is typically filled with dogs and their owners late in the afternoon and on weekends. The reserve is located on the site of the oval of the former [[Glengowrie High School]].
* The Glengowrie Tram Depot houses the trams (launched in 1927) which service Adelaide's only remaining tramline from Adelaide to Glenelg.
* The Sturt River Linear Park is a trail which follows the [[Sturt River, Adelaide|Sturt River]] through the south-western metropolitan area from Marion to Glenelg. It crosses through the north-eastern tip of Glengowrie at site of Glenelg East Reserve off Fisk Avenue.<ref>{{cite web |url=http://www.marion.sa.gov.au/site/page.cfm?u=224 |title=Sturt River Linear Park |accessdate=20 August 2010 |publisher=City of Marion}}</ref>
* Glengowrie High School first opened in 1969.<ref>{{cite journal |url=http://www.dete.sa.gov.au/mediacentre/files/links/XTRA_7_no_18.pdf |pages=1 |journal=Xtra |publisher=Department of Education and Children's Services, Government of South Australia |volume=7 |issue=18 |year=2004 |title=celebrations |accessdate=20 August 2010 }}</ref> It was closed<ref>{{cite web |url=http://www.parliament.sa.gov.au/HouseofAssembly/BusinessoftheAssembly/RecordsandPapers/VotesandProceedings/Votes%20and%20Proceedings/18_wednesday_04_apr_1990.htm |date=4 April 1990 |publisher=Parliament of South Australia |title=Votes and Proceedings of the House of Assembly}}</ref> and demolished in 1991 when the school body was amalgamated with Mitchell Park High School to form Hamilton Secondary College.<ref>{{cite web |url=http://www.hamcoll.sa.edu.au/media/files/2633.pdf |title=Hamilton Secondary College - School Context Statement |accessdate=20 August 2010 }}</ref> The site is now occupied by the Glengowrie Retirement Estate and Hazelmere Reserve.<ref>http://www.retireaustralia.com.au/our-communities/glengowrie</ref>

==See also==
* [[List of Adelaide suburbs]]

==References==
{{Reflist}}

{{Coord|34|59|S|138|32|E|type:city_region:AU-SA|display=title|format=dms}}
{{City of Marion suburbs}}

[[Category:Suburbs of Adelaide]]