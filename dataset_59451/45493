{{about|an Irish fighter pilot from Dublin|the African American journalist from Houston, Texas|George A. McElroy}}
{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = George Edward Henry McElroy
| image         =
| caption       =
| birth_date    = {{Birth date|df=yes|1893|5|14}}
| death_date    = {{Death date and age|df=yes|1918|7|31|1893|5|14}}
| birth_place   = [[Donnybrook, Dublin]], [[Ireland]]
| death_place   = [[Laventie|Laventie, France]]
| placeofburial = Laventie Military Cemetery,    [[La Gorgue]], Nord, France
| placeofburial_label =
| placeofburial_coordinates = {{Coord|50|38|10|N|2|46|25|E|display=inline,title}}
| nickname      = "Deadeye", "Mac", "McIrish"
| allegiance    = United Kingdom
| branch        = British Army<br/>Royal Flying Corps<br/>Royal Air Force
| serviceyears  = 1914–1918
| rank          = Captain
| unit          = {{Plainlist|
* [[Royal Engineers]]
* [[Royal Irish Regiment (1684–1922)|Royal Irish Regiment]]
* [[Royal Garrison Artillery]]
* [[No. 40 Squadron RAF|No. 40 Squadron RFC/RAF]]
* [[No. 24 Squadron RAF|No. 24 Squadron RFC/RAF]]
}}
| commands      =
| battles       =
| awards        = [[Military Cross]] & [[Medal bar|Two Bars]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & Bar
}}
[[Captain (British Army and Royal Marines)|Captain]] '''George Edward Henry McElroy''' [[Military Cross|MC]] & [[Medal bar|Two Bars]], [[Distinguished Flying Cross (United Kingdom)|DFC]] & Bar (14 May 1893 – 31 July 1918) was a leading Irish-born fighter pilot of the [[Royal Flying Corps]] and [[Royal Air Force]] during [[World War I]]. He was credited with 47 aerial victories.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/ireland/mcelroy.php |title=George Edward Henry McElroy |work=The Aerodrome |year=2015 |accessdate=10 June 2015}}</ref>

==Military career==
McElroy was born at [[Donnybrook, Dublin|Donnybrook, County Dublin, Ireland]]<ref name="Franks26">Franks (2000), p.26.</ref> to Samuel and Ellen McElroy.<ref name="CWGC">{{cite web |url=http://www.cwgc.org/find-war-dead/casualty/328068/McELROY,%20G%20E%20H |title=Casualty Details: McElroy, G. E. H. |work=[[Commonwealth War Graves Commission]] |year=2015 |accessdate=10 June 2015}}</ref> He enlisted promptly at the start of World War I in August 1914, and was shipped out to [[France]] two months later.<ref name="Franks26"/> He was serving as a corporal in the Motor Cyclist Section of the [[Royal Engineers]] when he was first commissioned as a second lieutenant on 9 May 1915.<ref>{{London Gazette |date=14 May 1915 |issue=29162 |startpage=4661 |nolink=yes}}</ref> While serving in the [[Royal Irish Regiment (1684-1922)|Royal Irish Regiment]]<ref name="Franks26"/> he was severely affected by [[mustard gas]] and was sent home to recuperate. He was in Dublin in April 1916, during the [[Easter Rising]], and was ordered to help quell the insurrection. McElroy refused to fire upon his fellow Irishmen, and was transferred to a southerly garrison away from home.<ref>{{cite web |url= http://www.firstworldwar.com/bio/mcelroy.htm |title=Who's Who - George McElroy |work=First World War.com |year=2013 |accessdate=10 June 2015}}</ref>

On 1 June 1916 McElroy relinquished his commission in the Royal Irish Regiment when awarded a cadetship<ref>{{London Gazette |date=6 June 1916 |issue=29613 |startpage=5635 |nolink=yes}}</ref> at the [[Royal Military Academy, Woolwich]], from which he graduated on 28 February 1917, and was commissioned as a second lieutenant in the [[Royal Garrison Artillery]].<ref>{{London Gazette |date=27 February 1917 |issue=29963 |startpage=2023 |endpage=2024 |nolink=yes}}</ref>

===Aerial service===
McElroy was promptly seconded to the Royal Flying Corps, being trained as a pilot at the [[Central Flying School]] at [[Upavon]], and appointed a [[flying officer]] on 28 June.<ref>{{London Gazette |date=20 July 1917 |supp=yes |issue=30193 |startpage=7407 |nolink=yes}}</ref> On 27 July his commission was backdated to 9 February 1916,<ref>{{London Gazette |date=27 July 1917 |supp=yes |issue=30206 |startpage=7740 |nolink=yes}}</ref> and he was promoted to lieutenant on 9 August.<ref>{{London Gazette |date=29 January 1918 |supp=yes |issue=30503 |startpage=1509 |nolink=yes}}</ref> On 15 August he joined [[No. 40 Squadron RFC]],<ref name="FlightFAdVR">{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200445.html |title=No. 40 (Bomber) Squadron: The First "Gordon" Squadron |first=F.A. de V. |last=Robertson |issue=1220 |volume=XXIV |page=413 |journal=[[Flight International|Flight]] |date=13 May 1932 |accessdate=10 June 2015 }}</ref> where he benefited from mentoring by [[Edward Mannock]]. He originally flew a [[Nieuport 17]], but with no success in battle. By the year's end McElroy was flying [[Royal Aircraft Factory S.E.5|S.E.5s]]<ref name="Franks26"/> and claimed his first victory on 28 December.

An extremely aggressive dog-fighter who ignored often overwhelming odds, McElroy's score soon grew rapidly. He shot down two German aircraft in January 1918, and by 18 February had run his string up to 11.<ref name="theaerodrome"/> At that point, he was appointed a [[flight commander]] with the temporary rank of captain,<ref>{{London Gazette |date=22 March 1918 |supp=yes |issue=30594 |startpage=3710 |nolink=yes}}</ref> and transferred to [[No. 24 Squadron RFC]]. He continued to steadily accrue victories by ones and twos. By 26 March, when he was awarded the [[Military Cross]],<ref>{{London Gazette |date=22 March 1918 |supp=yes |issue=30597 |startpage=3745 |nolink=yes}}</ref> he was up to 18 "kills". On 1 April, the Army's Royal Flying Corps (RFC) and the Royal Naval Air Service (RNAS) were merged to form the Royal Air Force, and his squadron became [[No. 24 Squadron RAF]]. McElroy was injured in a landing accident on 7 April; he brushed a treetop while landing.<ref>Shores & Rolfe (2001), p.79.</ref> By then he had run his score to 27. While he was sidelined with his injury, on 22 April, he was awarded a bar to his Military Cross. Following his [[convalescence]], McElroy returned to No. 40 Squadron in June, scoring three times, on the 26th, 28th, and 30th. The latter two triumphs were observation balloons. That ran his tally to 30.<ref name="theaerodrome"/>

In July, he added to his score almost daily, a third balloon busting on the 1st, followed by one of the most triumphant months in the history of fighter aviation, adding 17 victims during the month. His run of success was threatened on the 20th by a vibrating engine that entailed breaking off an attack on a German two seater and a rough emergency landing that left him with scratches and bruises.{{Citation needed|date=January 2011}} There was a farewell luncheon that day for his friend [[Gwilym Hugh Lewis|"Noisy" Lewis]]; their mutual friend "Mick" Mannock pulled McElroy aside to warn him about the hazards of following a German victim down within range of ground fire.<ref>Franks (2000), p.27.</ref>

On 26 July, his mentor and friend, [[Edward Mannock|Edward "Mick" Mannock]], was killed by ground fire. Ironically, on that same day, "McIrish" McElroy received the second Bar to his Military Cross. He was one of only ten airmen to receive the second Bar.{{Citation needed|date=January 2011}}

===Death in action===
McElroy's continued apparent disregard for his own safety when flying and fighting could have only one end. On 31 July 1918, he reported destroying a [[Hannover C]] for his 47th victory. He then set out again. He failed to return from this flight and was posted missing. Later it was learned that McElroy had been killed by [[Anti-aircraft warfare|ground fire]]. He was 25 years old.

McElroy would receive the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] posthumously on 3 August, citing his shooting down 35 aeroplanes and three observation balloons. The Bar would arrive still later, on 21 September, and would laud his low-level attacks. In summary, he shot down four enemy aircraft in flames and destroyed 23 others, one of which he shared destroyed with other pilots. He drove down 16 enemy aircraft "out of control" and out of the fight; in one of those cases, it was a shared success. He also destroyed three balloons.<ref>Shores ''et. al.'' (1990)</ref>

McElroy is interred in Plot I.C.1 at the [[Laventie|Laventie Military Cemetery]] in [[La Gorgue]], northern France.<ref name="CWGC"/>

==Awards and citations==
;Military Cross
:2nd Lieutenant George Edward Henry McElroy, Royal Garrison Artillery and Royal Flying Corps.
::"For conspicuous gallantry and devotion to duty. He has shown a splendid offensive spirit in dealing with enemy aircraft. He has destroyed at least two enemy machines, and has always set a magnificent example of courage and initiative." ([[Gazetted]] 26 March 1918.)<ref>{{London Gazette |date=24 August 1918 |supp=yes |issue=30862 |startpage=9914 |nolink=yes}}</ref>

;Bar to Military Cross
:2nd Lieutenant George Edward Henry McElroy, MC, Royal Garrison Artillery and Royal Flying Corps.
::"For conspicuous gallantry and devotion to duty. When on an offensive patrol, observing a hostile scout diving on one of our aeroplanes, he opened fire, and sent down the enemy machine in an irregular spin out of control, when it finally crashed completely. Later in the same day, he sent down another enemy machine in flames. On another occasion, when on offensive patrol, he singled one out of four enemy machines, and sent it down crashing to earth. On the same day he attacked another enemy machine, and, after firing 200 rounds, it burst into flames. On a later occasion, he opened fire on an enemy scout at 400 yards range, and finally sent it down in a slow spin out of control. In addition, this officer has brought down two other enemy machines completely out of control, his skill and determination being most praiseworthy." (Gazetted 2 April 1918.)<ref>{{London Gazette |date=19 April 1918 |supp=yes |issue=30643 |startpage=4822 |nolink=yes}}</ref>

;Second bar to Military Cross
::Lieutenant (Temporary Captain) George Edward Henry McElroy, MC, Royal Garrison Artillery and Royal Flying Corps.
::"For conspicuous gallantry and devotion to duty. While flying at a height of 2,000 feet, he observed a patrol of five enemy aircraft patrolling behind the lines. After climbing into the clouds, he dived to the attack, shot down and crashed one of them. Later, observing a two-seater, he engaged and shot it down out of control. On another occasion he shot down an enemy scout which was attacking our positions with machine-gun fire. He has carried out most enterprising work in attacking enemy troops and transport and in the course of a month has shot down six enemy aircraft, which were seen to crash, and five others out of control." (Gazetted 26 July 1918.)<ref>{{London Gazette |date=26 July 1918 |issue=30813 |startpage=8753 |supp=yes |nolink=yes}}</ref>

;Distinguished Flying Cross
:Lieutenant (Temporary Captain) George Edward Henry McElroy, MC.
::"A brilliant fighting pilot who has destroyed thirty-five machines and three kite balloons to date. He has led many offensive patrols with marked success, never hesitating to engage the enemy regardless of their being, on many occasions, in superior numbers. Under his dashing and skilful leadership his flight has largely contributed to the excellent record obtained by the squadron." (Gazetted 3 August 1918.)<ref>{{London Gazette |date=3 August 1918 |issue=30827 |startpage=9201 |supp=yes |nolink=yes}}</ref>

;Bar to Distinguished Flying Cross
:Lieutenant (Temporary Captain) George Edward Henry McElroy, MC, DFC. (Royal Garrison Artillery).
::"In the recent battles on various army fronts this officer has carried out numerous patrols, and flying at low altitudes, has inflicted heavy casualties on massed enemy troops, transport, artillery teams, etc., both with machine-gun fire and bombs. He has destroyed three enemy kite balloons and forty-three machines, accounting for eight of the latter in eight consecutive days. His brilliant achievements, keenness and dash have at all times set a fine example and inspired all who came in contact with him." (Gazetted 20 September 1918.)<ref>{{London Gazette |date=20 September 1918 |supp=yes |issue=30913 |startpage=11248 |nolink=yes}}</ref>

==List of aerial victories==
{| class="wikitable" style="font-size:90%;"
|-
|+Combat record<ref name="theaerodrome"/>
|-
!No.
! width="125" |Date/Time
! width="100" |Aircraft/<br/>(Serial No.)
!Foe
!Result
!Location
!Notes
|-
! colspan="7" |No. 40 Squadron RFC
|-
| 1 || 28 December 1917<br/>@1120 || S.E.5a<br/>(B598) || [[LVG]] C || Destroyed || [[Drocourt, Pas-de-Calais]]—[[Vitry-en-Artois|Vitry]] ||
|-
| 2 || 13 January 1918<br/>@0920 || S.E.5a<br/>(B598) || [[Rumpler]] C || Out of control || [[Pont-à-Vendin]] ||
|-
| 3 || 19 January 1918<br/>@1205 || S.E.5a<br/>(B598) || [[Deutsche Flugzeug-Werke|DFW]] C || Destroyed || Vitry ||
|-
| 4 || 24 January 1918<br/>@1250 || S.E.5a<br/>(B598) || DFW C || Out of control || [[Oppy, Pas-de-Calais|Oppy]]—[[Henin-Liétard]] ||
|-
| 5 || 2 February 1918<br/>@1330 || S.E.5a<br/>(B598) || C || Out of control || South-east of Habourdin ||
|-
| 6 || 5 February 1918<br/>@1240 || S.E.5a<br/>(B22) || DFW C || Destroyed || [[Wingles]] ||
|-
| 7 || 5 February 1918<br/>@1300 || S.E.5a<br/>(B22) || DFW C || Destroyed (on fire) || North of [[La Bassée]] ||
|-
| 8 || 16 February 1918<br/>@1130 || S.E.5a<br/>(B598) || DFW C || Out of control || West of Henin-Liétard ||
|-
| 9 || 17 February 1918<br/>@1125 || S.E.5a<br/>(B22) || [[Pfalz D.III]] || Destroyed || Marquain ||
|-
|10 || 17 February 1918<br/>@1155 || S.E.5a<br/>(B22) || C || Destroyed (on fire) || 4 miles south-east of [[Lens, Pas-de-Calais|Lens]] ||
|-
|11 || 18 February 1918<br/>@1200 || S.E.5a<br/>(B598) || [[Albatros D.V]] || Out of control || [[Douvrin]] ||
|-
! colspan="7" |No. 24 Squadron RFC
|-
|12 || 21 February 1918<br/>@1000 || S.E.5a<br/>(B664) || Albatros D.V || Out of control || South of [[Honnecourt-sur-Escaut|Honnecourt]] ||
|-
|13 || 26 February 1918<br/>@1605 || S.E.5a<br/>(B891) || [[Fokker Dr.I]] || Destroyed (on fire) || 4 miles east of [[Laon]] ||
|-
|14 || 1 March 1918<br/>@1045 || S.E.5a<br/>(B891) || DFW C || Out of control || [[Beaurevoir]] ||
|-
|15 || 6 March 1918<br/>@0705 || S.E.5a<br/>(C1751) || Albatros D.V || Destroyed || North-east of [[Bullecourt]] ||
|-
|16 || 8 March 1918<br/>@0755 || S.E.5a<br/>(B891) || DFW C || Out of control || East of [[Saint-Quentin, Aisne|St. Quentin]] ||
|-
|17 || 8 March 1918<br/>@1455 || S.E.5a<br/>(B891) || Fokker Dr.I || Destroyed || South-east of [[La Fère]] ||
|-
|18 || 9 March 1918<br/>@0930 || S.E.5a<br/>(B891) || Albatros D.V || Out of control || North-east of La Fère ||
|-
|19 || 27 March 1918<br/>@1520 || S.E.5a<br/>(A8900) || Albatros D.V || Destroyed (on fire) || [[Chipilly]] ||
|-
|20 || 29 March 1918<br/>@1530 || S.E.5a<br/>(C1800) || Albatros D.V || Destroyed || [[Foucaucourt-en-Santerre|Foucaucourt]] ||
|-
|21 || 29 March 1918<br/>@1545 || S.E.5a<br/>(C1800) || DFW C || Out of control || East of [[Lamotte-Warfusée|Warfusée]] ||
|-
! colspan="7" |No. 24 Squadron RAF
|-
|22 || 1 April 1918<br/>@1215 || S.E.5a<br/>(C1084) || Albatros D.V || Destroyed || North of [[Ignaucourt]] ||
|-
|23 || 2 April 1918<br/>@1430 || S.E.5a<br/>(C1084) || Albatros D.V || Out of control || East of [[Moreuil]] ||
|-
|24 || 4 April 1918<br/>@1505 || S.E.5a<br/>(C1084) || Pfalz D.III || Destroyed || North of Warfusée ||
|-
|25 || 7 April 1918<br/>@1040 || S.E.5a<br/>(C1800) || Albatros D.V || Out of control || Warfusée ||
|-
|26 || 7 April 1918<br/>@1100 || S.E.5a<br/>(C1800) || Albatros D.V || Destroyed || 3 miles east of [[Marcelcave]] ||
|-
|27 || 7 April 1918<br/>@1115 || S.E.5a<br/>(C1800) || Fokker Dr.I || Out of control || North of Moreuil Wood ||
|-
! colspan="7" |No. 40 Squadron RAF
|-
|28 || 26 June 1918<br/>@0655 || S.E.5a<br/>(C8869) || DFW C || Destroyed || 500 yards south-east of [[Annay, Pas-de-Calais|Annay]] ||
|-
|29 || 28 June 1918<br/>@1255 || S.E.5a<br/>(C8869) || [[Observation balloon|Balloon]] || Destroyed || [[Provin]] ||
|-
|30 || 30 June 1918<br/>@1045 || S.E.5a<br/>(C8869) || Balloon || Destroyed || Annay || Shared with Lieutenants A. R. Whitten and Gilbert Strange
|-
|31 || 1 July 1918<br/>@1225 || S.E.5a<br/>(C8869) || Balloon || Destroyed || [[Harnes]] ||
|-
|32 || 2 July 1918<br/>@1022 || S.E.5a<br/>(D6122) || DFW C || Destroyed || North of La Bassée ||
|-
|33 || 2 July 1918<br/>@2100 || S.E.5a<br/>(E1318) || DFW C || Destroyed || North-west of Pacaut Wood ||
|-
|34 || 5 July 1918<br/>@1150 || S.E.5a<br/>(C8869) || DFW C || Out of control || [[Lestrem]] ||
|-
|35 || 6 July 1918<br/>@0540 || S.E.5a<br/>(C8869) || [[Hannoversche Waggonfabrik|Hannover]] C || Destroyed || North-east of [[Arras]] ||
|-
|36 || 8 July 1918<br/>@0925 || S.E.5a<br/>(C8869) || Hannover C || Out of control || East of Monchy || Shared with Lieutenants [[Indra Lal Roy]] and Gilbert Strange
|-
|37 || 8 July 1918<br/>@1045 || S.E.5a<br/>(C8869) || Hannover C || Destroyed || 3 miles east of La Bassée ||
|-
|38 || 11 July 1918<br/>@0550 || S.E.5a<br/>(C8869) || Hannover C || Destroyed || West of Vitry ||
|-
|39 || 13 July 1918<br/>@0645 || S.E.5a<br/>(C8869) || Hannover C || Destroyed || West of [[Estaires]] || Shared with Lieutenants F. H. Knobel, Indra Lal Roy and Gilbert Strange
|-
|40 || 13 July 1918<br/>@2005 || S.E.5a<br/>(C8869) || Pfalz D.III || Destroyed || Vitry ||
|-
|41 || 14 July 1918<br/>@0730 || S.E.5a<br/>(C8869) || Hannover C || Destroyed || North of Drocourt ||
|-
|42 || rowspan="2" | 15 July 1918<br/>@2015 || rowspan="2" | S.E.5a<br/>(C8869) || Fokker D.VII || Destroyed || rowspan="2" | North-east of [[Hill 70]], Lens ||
|-
|43 || Fokker D.VII || Out of control ||
|-
|44 || 19 July 1918<br/>@1245 || S.E.5a<br/>(C8869) || Albatros D.V || Destroyed || 2 miles North of Estaires ||
|-
|45 || 25 July 1918<br/>@0615 || S.E.5a<br/>(C8869) || Hannover C || Destroyed || North of [[Neuve-Chapelle]] ||
|-
|46 || 25 July 1918<br/>@0715 || S.E.5a<br/>(C8869) || Hannover C || Destroyed || West of [[Épinoy|Bois d'Épinoy]] ||
|-
|47 || 31 July 1918<br/>@0930 || S.E.5a<br/>(E1310) || Hannover C || Destroyed || [[Laventie]] ||
|-
|}

==References==
{{Reflist|30em}}
;Bibliography
* {{cite book |first=Norman |last=Franks |authorlink=Norman Franks |title=SE 5/5a Aces of World War I |location=London, UK |publisher=Osprey Publishing |year=2007 |isbn=978-1-84603-180-9}}
* {{cite book |title=British and Empire Aces of World War I |first1=Christopher |last1=Shores |first2=Mark |last2=Rolfe |publisher=Osprey Publishing |year=2001 |isbn=978-1-84176-377-4 |lastauthoramp=yes}}
* {{cite book |first1=Christopher F. |last1=Shores |first2=Norman |last2=Franks |authorlink2=Norman Franks |first3=Russell F. |last3=Guest |title=Above the Trenches: a Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920 |location=London, UK |publisher=Grub Street |year=1990 |isbn=978-0-948817-19-9 |lastauthoramp=yes}}

==External links==
* [http://membres.lycos.fr/asduciel/mcirish.htm Profile of "McIrish McElroy"] Accessed 13 September 2008. Translated from French to English via Babelfish. {{dead link|date=June 2015}}

{{wwi-air}}


{{DEFAULTSORT:McElroy, George}}
[[Category:1893 births]]
[[Category:1918 deaths]]
[[Category:People from County Dublin]]
[[Category:Royal Engineers soldiers]]
[[Category:Royal Irish Regiment (1684–1922) officers]]
[[Category:Royal Artillery officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force officers]]
[[Category:British Army personnel of World War I]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:Irish World War I flying aces]]
[[Category:British military personnel killed in World War I]]
[[Category:Aviators killed by being shot down]]
[[Category:Recipients of the Military Cross]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]