[[Image:Wines of Abkhazia.jpg|thumb|right|Wines of Abkhazia]]
'''[[Abkhazia]]''' is a disputed region in Georgia, but currently self-proclaimed an independent state. It has a long history of '''[[wine]]'''-making. Most of the produced wine is consumed locally or exported to Russia.

==Varieties==
Abkhazian wine varieties include:
===White===
*[[New Athos|Anakopia]] is a white semi-dry table wine made from the Tsolikauri grape variety grown in the [[Sukhumi]] and [[Gudauta]] districts in Abkhazia. The color range is from light to dark-straw. It has a specific aroma and a subtle fresh taste. The alcohol content in the ready wine is 9-11%, sugar content 1-2 g/100 ml, titrated acidity 5-8 g/l. The wine has been produced since 1978.
===Red===
*[[Apsny]] is a naturally semi-sweet red wine made of red grape varieties cultivated in Abkhazia. The wine of pomegranate color has a pleasant aroma, a full and harmonious taste with gentle sweetness. When ready for use, the wine contains 9-10% alcohol, 3-5% sugar and has 5-7% titrated acidity. At an international exhibition the wine received one silver medal.
*[[Lykhny]] is a naturally semi-sweet red wine made of the Izabela grape variety cultivated in Abkhazia. The wine has red color, a specific aroma and a fresh harmonious taste. The wine contains about 10% alcohol, 3-5% sugar and has 5-7% titrated acidity. At international exhibitions Lykhny was awarded one silver and one bronze medal.

==See also==
*[[Georgian wine]]

==References==
{{reflist}}

==External links==
[https://web.archive.org/web/20070529232936/http://www.abkhazia.ru:80/abkhaz_vine.htm Abkhazian wine] {{ru icon}}

{{Wines}}
{{Wine by country}}

{{abkhazia-stub}}
[[Category:Abkhazia]]