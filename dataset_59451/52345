{{Italic title}}
'''''Mycoses''': Diagnosis, Therapy and Prophylaxis of Fungal Diseases'' is a bimonthly [[peer-reviewed]] [[medical journal]] covering [[mycology]]. It is published by [[Wiley-Blackwell]]. The [[editor-in-chief]] are Oliver Cornely, Jacques Meis and Martin Schaller. It is the official publication of the [[Deutschsprachige Mykologische Gesellschaft]]. The journal covers the [[pathogenesis]], [[diagnosis]], [[therapy]], [[prophylaxis]], and [[epidemiology]] of fungal infectious diseases in humans and animals as well as on the biology [[of pathogenic fungi]].

== History ==
The journal was established in 1957 by Heinz Grimmer ([[Wiesbaden]]) and published by [[Medizinische Verlags Anstalt]] (Berlin) under the title ''Mykosen'' (German for "mycoses"). It was originally published in German, but switched 1988 to English. At that time the title was changed to ''Mycoses''.

== Abstracting and indexing ==
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.239.<ref name=WoS>{{cite book |year=2015 |chapter=Mycoses |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1439-0507}}

[[Category:Mycology journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]