{{Infobox company
 | name = Informa  plc
 | logo = [[Image:Informalogo.PNG]]
 | type = [[Public limited company]] 
 | traded_as = {{Lse|INF}}
 | genre = 
 | foundation = December 1998 
 | founder = 
 | location_city = 
 | location_country = 
 | location = [[London]]
 | locations = 150 offices
 | area_served = 43+ countries worldwide
 | key_people = [[Stephen Carter, Baron Carter of Barnes]] ([[CEO]])
 | industry = Academic [[Publishing]], Business Intelligence, Global Exhibitions, Knowledge, Events
 | products = Specialist information and business knowledge
 | services = Large scale events, Exhibitions, Academic Books & Journals, Conferences, Training, Specialist publishing and Intelligence
 | revenue = [[Pound sterling|£]]1,345.7 million (2016)<ref name=prelims>{{cite web|url=http://www.informa.com/Documents/Investor%20Relations/2017/Informa%202016%20Full%20Year%20Results%20Statement.pdf|title= Preliminary Results 2016|publisher=Informa|accessdate=7 April 2017}}</ref>
 | operating_income = [[Pound sterling|£]]416.1 million (2016)<ref name=prelims/>
 | net_income = [[Pound sterling|£]]173.5 million (2016)<ref name=prelims/>
 | assets = 
 | equity = 
 | owner = 
 | num_employees = 
 | parent = 
 | divisions = Academic Publishing, Business Intelligence, Global Exhibitions, Knowledge & Networking<ref>{{cite web|url=http://www.themediabriefing.com/article/informa-restructuring-four-divisions-andrew-mullins|title=Informa restructure aims to 'increase operational, customer and market focus'|author=TheMediaBriefing|date=10 July 2014|publisher=|accessdate=25 March 2016}}</ref> 
 | subsid = 
 | caption = 
 | homepage = {{URL|http://www.informa.com}}
 | footnotes = 
 | intl = 
}}

'''Informa plc''' is a [[Multinational corporation|multinational]] [[publishing]] and events company with its head office and registered office in [[London]].<ref>{{cite web|url=http://www.informa.com/Contact-Us/|title=Informa.com - Global Contacts|author=Informa PLC|publisher=|accessdate=25 March 2016}}</ref><ref>"[http://www.informa.com/Contact-Us/ Global Contacts]." Informa. Retrieved on 4 February 2011. "Head Office Mortimer Street, London"</ref> It has offices in 43 countries and around 6,500 employees. It owns numerous brands including [[CRC Press]], [[Datamonitor]], [[Institute for International Research]], ''[[Lloyd's List]]'' (London Press Lloyd), [[Routledge]], and [[Taylor & Francis]].

It is listed on the [[London Stock Exchange]] and is a constituent of the [[FTSE 100 Index]].

==History==
Informa's oldest business started in 1734 when ''[[Lloyd's List]]'', now one of the world's oldest continuously running journals, began covering [[London]] shipping news.<ref name="Ref_a">{{cite web|url=http://www.lloydslist.com/ll/home/about.htm |title=Lloyd's List |accessdate=2009-08-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20090223191107/http://www.lloydslist.com/ll/home/about.htm |archivedate=February 23, 2009 }}</ref>

Informa itself was created in 1998 by the merger of IBC Group plc and LLP Group plc. Since then Informa has expanded considerably, including a 2004 merger with the publishing company Taylor & Francis and a 2005 acquisition of IIR Holdings, a human capital development company, for £768 million.<ref name=ForbesRoundup>{{cite news |title=ROUNDUP Informa names new chairman, CEO after FY profits soar |url=http://www.forbes.com/markets/feeds/afx/2007/03/14/afx3514841.html |work=Forbes |date=2007-03-14 |accessdate=2007-12-31}}</ref><ref name="Ref_2007">{{cite news |title=T&F Informa buys conferences group Institute for International Research for 768 mln stg |url=http://www.forbes.com/home/feeds/afx/2005/06/01/afx2068249.html |work=Forbes |date=2007-06-01 |accessdate=2007-12-31|archiveurl=https://web.archive.org/web/20081121012535/http://www.forbes.com/home/feeds/afx/2005/06/01/afx2068249.html|archivedate=2008-11-21}}</ref> In October 2006, the company was approached by [[Springer Science and Business Media]] in a takeover bid,<ref name="Kundnani2006">{{cite news |first=Hans |last=Kundnani |title=Springer Science & Business Media launches bid for Informa |url=https://www.theguardian.com/business/2006/oct/19/privateequity.pressandpublishing |work=The Guardian |date=2006-10-19 |accessdate=2007-12-30 | location=London}}</ref> but in early November the Informa board rejected the 630p per share offer as too low.<ref name="Ref_2006">{{cite news |title=Informa rejects Springer bid |url=http://www.forbes.com/business/feeds/afx/2006/11/03/afx3144026.html |work=AFX News Limited |date=2006-11-03 |accessdate=2007-12-30}}</ref>

In early 2007, chairman Richard Hooper announced his retirement in May and, after consulting with major shareholders, the company moved chief executive Peter Rigby to chairman, and managing director David Gilbertson to Rigby's former post. Informa explained Rigby's move by the need to maintain management stability, although it generated some controversy because it did not follow the advice of the UK's Combined Code on [[Corporate Governance]].<ref name=ForbesRoundup/><ref name="Ref_2006a">{{cite news |title=Informa unveils new CEO, chairman as 2006 results top forecasts |url=http://www.forbes.com/markets/feeds/afx/2007/03/14/afx3514395.html |work=Forbes |date=2006-03-14 |accessdate=2007-12-30}}</ref>

On 8 June 2008, ''[[The Sunday Telegraph]]'' revealed that [[United Business Media]] (UBM) had proposed a merger with Informa to create a media group worth over £3 billion.<ref name="Harrington2008">{{cite news |first=Ben |last=Harrington |author2=Kleinman, Mark |title=Media giants United Business Media and Informa open talks on £3bn merger |url=http://www.telegraph.co.uk/money/main.jhtml?view=DETAILS&grid=&xml=/money/2008/06/08/cnubm108.xml |work=The Sunday Telegraph |date=2008-06-08 |accessdate=2008-06-08 | location=London}}</ref> The talks were confirmed by Informa in a press release that same day, but described as "preliminary".<ref name="Ref_2008">{{cite news |title=Statement Regarding Press Speculation |url=http://www.informa.com/about/Press_Releases/2008/statement_regarding_press_speculation/statement_regarding_press_speculation2 |publisher=Informa |date=2008-06-08 |accessdate=2008-06-08}}</ref><ref name="Ref_2008a">{{cite news |title=UBM and Informa confirm merger talks |url=https://www.theguardian.com/media/2008/jun/09/mediabusiness.unitedbusinessmedia?INTCMP=SRCH |work=The Guardian |date=2008-06-09 |accessdate=2013-07-18 | location=London}}</ref> Previously on May 13 ''[[The Times]]'' had reported that the [[Carlyle Group]] and [[Apax Partners]] were considering bidding for the company.<ref name="Andrews2008">{{cite news |first=Amanda |last=Andrews |author2=Kennedy, Siobhan |title=Informa garners attention of private equity |url=http://business.timesonline.co.uk/tol/business/industry_sectors/media/article3919555.ece |work=The Times |date=2008-05-13 |accessdate=2008-06-08 | location=London}}</ref> On June 17 talks with UBM ended because of the rapid rise in Informa's stock price after the public disclosure of the potential merger.<ref name="Arnott2008">{{cite news |first=Sarah |last=Arnott |title=Cash approach for Informa as merger talks end with UBM |url=http://www.independent.co.uk/news/business/news/cash-approach-for-informa-as-merger-talks-end-with-ubm-849295.html |work=The Independent |date=2008-06-18 |accessdate=2008-07-07 | location=London}}</ref> On June 26 a private equity consortium consisting of [[Providence Equity Partners]], the [[Carlyle Group]] and [[Hellman & Friedman]] proposed a takeover bid offering 506 pence per share.<ref name="Thiel2008">{{cite news |first=Simon |last=Thiel |author2=Sabine Pirone |title=Informa Rises on $4.29 Billion Private-Equity Offer |url=https://www.bloomberg.com/apps/news?pid=20601102&sid=aSRNcFwXy3qw&refer=uk |publisher=Bloomberg |date=2008-07-02 |accessdate=2008-07-07}}</ref>

On 1 May 2009 the company announced that it would be restructure its business so it was incorporated in [[Jersey]] but tax resident in [[Switzerland]].<ref name="Kollewe2009">{{cite news |title=Publishing firm Informa blames budget for tax switch |url=https://www.theguardian.com/business/2009/may/01/informa-budget-tax-switch|work=The Guardian |date=2009-05-01 |accessdate=2009-05-04 | location=London | first=Julia | last=Kollewe}}</ref>

On 10 July 2013 the company announced that Peter Rigby would retire at the end of 2013 to be replaced as CEO by [[Stephen Carter, Baron Carter of Barnes|Stephen A Carter]].<ref name="Informa2013">{{cite web |url=http://www.informa.com/Media-centre/Press-releases--news/Latest-News/Informa-announces-retirement-of-Peter-Rigby-and-appointment-of-Stephen-Carter-as-Chief-Executive |title=Informa announces retirement of Peter Rigby and appointment of Stephen Carter |date=10 July 2013 |accessdate=10 July 2013}}</ref> Also in 2013, Informa acquired the Canadian company Hobby Star Marketing, who runs the [[comic book convention]] brand [[Fan Expo Canada|Fan Expo]]. Informa has since acquired other conventions and placed them under Fan Expo's auspises, including [[Dallas Comic Con]] and [[MegaCon]].<ref>{{cite web|title=Dallas Comic Con Gets a New Owner and Home|url=http://www.dallasobserver.com/arts/dallas-comic-con-gets-a-new-owner-and-home-7082192|website=Dallas Observer|accessdate=24 February 2016}}</ref><ref>{{cite news|last1=Dineen|first1=Caitlin|title=Orlando's MegaCon sold to Informa|url=http://www.orlandosentinel.com/business/tourism/tourism-central-florida-blog/os-orlando-megacon-sold-to-informa-20150407-post.html|accessdate=9 April 2015|work=Orlando Sentinel|date=7 April 2015}}</ref><ref>{{cite press release|last=Mantei|first=Shelley|date=9 April 2015|title=Informa Exhibitions Expands Pop Culture Convention Portfolio With MegaCon Orlando|url=http://fanexpocanada.com/wp-content/uploads/MegaCon-Purchase_Press-Release_04072015.pdf|location=Orlando, Florida|publisher=FAN EXPO HQ|accessdate=9 April 2015}}</ref>

In December 2013, Informa acquired the assets of Elsevier Business Intelligence (EBI) from [[Reed Elsevier]]. The EBI business unit includes such publications as The Pink Sheet, The Gray Sheet, IN VIVO, Start-Up, and the Strategic Transactions database along with a series of notable conferences. The EBI assets combined with Scrip, Datamonitor and several other publications formed the newly created Pharma Intelligence division of Informa Business Information.<ref name="InformaEBI2013">{{cite web |url=http://newsbreaks.infotoday.com/NewsBreaks/Informa-Acquires-Elsevier-Business-Intelligence-93697.asp |title=Informa Acquires Elsevier Business Intelligence |date=9 December 2013 |accessdate=9 December 2013}}</ref>

In January 2014, [[Stephen Carter, Baron Carter of Barnes|Stephen Carter]] became CEO of Informa plc.<ref>{{cite news|title=Informa Confirms Stephen Carter To Be CEO, Effective Jan. 1 - Update |url=http://www.rttnews.com/2245312/informa-confirms-stephen-carter-to-be-ceo-effective-jan-1-update.aspx|publisher=RTT News|date=31 December 2013|accessdate=2 July 2014}}</ref>

==Operations==
The company operates more than 10,000 conferences annually, and publishes newsletters, [[academic journal]]s, commercial databases, and academic and business books. Topics covered include arts and humanities through social sciences to physical science and technology; and finance and the law through to telecommunications, maritime trade, energy, biotech, commodities and agriculture. Its online publishing portal '''Informaworld''' provided subscribers with more than half a million journal articles and 13,000 e-books from all its imprints.<ref name="Griffin2006">{{cite journal |last=Griffin |first=Daniel |date=December 2006 |title=Single vision of the world at Informa |journal=Information World Review |issue=230 |pages=2 }}</ref> In June 2011 the journals and e-books transferred to a new website, Taylor & Francis Online. Abstracting and indexing databases and bibliographic databases were to move from Informaworld to Taylor & Francis Online at a later date.<ref>{{cite web |url= http://resources.tandfonline.com/documents/library-faqs.pdf |title=Taylor & Francis Online – FAQs |year=2011 |publisher=Taylor & Francis Group |accessdate=23 September 2012}}</ref> Besides publishing, the group has several other interests represented by the brands it owns in many countries. These brands (companies) work in the areas of performance improvement and management consulting. It has the following divisions:

{{Col-begin|width=95%}}
{{Col-1-of-2}}

===Academic and scientific===
*Informa Healthcare<ref>{{cite web|url=http://www.informahealthcare.com|title=Welcome to Informa Healthcare|work=informahealthcare.com|accessdate=29 March 2015}}</ref>
*Informa Life Sciences
*[[Taylor & Francis]]

===Commercial===
*KNect365 Knowledge & Networking
**Euroforum
** ICBI
**[[Institute for International Research]]
**Informa Telecoms & Media
*IBC
*Informa Agra
** Agra Europe
** Agrow
** Animal Pharm
**[[The Public Ledger]]
**F.O. Licht
**Phillips McDougall
*Informa Australia
*Informa Corporate Learning<ref name="Ref_b">{{cite web |url=http://www.informa.com.au/training |title=Informa Corporate Learning |publisher=Informa Corporate Learning |accessdate=21 February 2014}}</ref>
*Informa Economics
*Informa Maritime & Transport
**[[Lloyd's List]]
**[[Lloyd’s MIU]]
*Leaders in India ''Business Forum'' 2008
*[[Monaco Yacht Show]]
{{Col-2-of-2}}

===Professional===
* Insurance Day
*Council on Education
*[[CPDcast]]
*[[Datamonitor]]
*Health Insurance Magazine
*ICBI
*[[Institute for International Research|IIR Finance]]
*iMoneyNet
*Informa Finance
*Informa Global Markets
*Informa Insurance
*Informa Investment Solutions
*Informa Law
*[[Informa Research Services]]
*InformaDATA
*International Faculty of Finance
*International Insider
*MarketLine
*Pharma Intelligence
{{col-end}}

==References==
{{Reflist|colwidth=30em}}

==External links==
{{Portal|Switzerland|Companies}}
* [http://www.informa.com Informa]

{{Informa}}
{{Media in the United Kingdom|comporg}}
{{FTSE 100 Index constituents}}


[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1998]]
[[Category:Communications in Switzerland]]
[[Category:Companies listed on the London Stock Exchange]]
[[Category:1998 establishments in England]]