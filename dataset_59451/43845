<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Vireo
 | image=Vickers 125 Vireo.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Experimental ship-borne fighter
 | national origin=United Kingdom
 | manufacturer=Vickers Ltd.
 | designer=
 | first flight=March 1928
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
[[File:Vickers 125 Vireo rear quarter view.jpg|300px|thumb]]
The '''Vickers Vireo''' was an experimental low wing all-metal monoplane built to explore both all-metal service aircraft and the use of catapult launched ship board fighters.  Only one was built.

==Development==
The Vickers-Wibault construction method was based on the patents of [[Michel Wibault]], who began working with Vickers in 1922.<ref>{{Harvnb|Andrews|Morgan|1988|pages=207–9}}</ref>  It was a way of producing an all-metal aircraft with an airframe built up from simple, non-machined metal shapes, covered by very thin {{convert|0.4|mm|in}} corrugated light alloy sheets. On the wings, the corrugations were aligned along the chord and longitudinally on he fuselage. The resulting fuselage was not monocoque but was internally braced and the skin on the wings was not stressed.  Panels were riveted to each other and to the underlying structure.  Vickers first experience of the method was with the licence built [[Vickers Wibault|Wibault Scout]].  The first Vickers design using this construction was the Vireo.<ref name="Andrews"/>

The Vireo (named after a Latin word thought to mean Greenfinch) was built to [[List of Air Ministry Specifications#1920-1929|Air Ministry specification 17/25]], intended to evaluate both all-metal aircraft and low powered, catapult launched, carrier borne fighters.  It was a low-winged single-engined monoplane of rather angular appearance with a flat-sided, deep fuselage except immediately aft of the engine.  Forward of the overwing open cockpit the nose fell away, giving it a slightly humped look.  The flying surfaces were all without external bracing; the wing was tapered, of deep section and incorporated twin machine guns.  The horizontal stabiliser had a straight leading edge but tapered at the rear.  There was a square topped, balanced rudder but no fin.<ref name="Andrews">{{Harvnb|Andrews|Morgan|1988|pages=212–5, 222}}</ref>

The Vireo was powered by an uncowled {{convert|230|hp|kW}} [[Armstrong Siddeley Lynx]] IV radial engine, driving a two-bladed propeller.  The specification called for the fitting of either wheels or floats and both were used, though the Vireo took its Ministry tests as a landplane.  These tests began at [[RAF Martlesham Heath]] a month after the initial flights in March 1928. The long gap between the tender submission in December 1925 and the first flight was partly because the novel structure had undergone structural and aerodynamic tests at the [[Royal Aircraft Establishment]].  There were a few minor incidents during the tests but more serious was a tendency to drop heavily at touchdown, which led to some rear fuselage damage.  This was later attributed to root interference of the highly cambered wing leading to nasty stall characteristics.  Nevertheless, by July the Vireo was on board {{HMS|Furious|47|6}} for deck landing trials.<ref name="Andrews"/>

The Air Ministry's interest in low-powered on-board fighters, catapult-launched to compensate for their small engines, waned when the Vireo proved no faster than the conventional ship board aircraft like the [[Fairey IIIF]].  The Vireo experience gave Vickers enough confidence in all-metal fighters to proceed with their later [[Vickers Jockey|Jockey]] and [[Vickers Venom|Venom]] designs.<ref name="Andrews"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aerospecs
|ref={{Harvnb|Andrews|Morgan|1988|pages= 222}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=1
|capacity=
|length m=8.43
|length ft=27
|length in=8
|span m=10.67
|span ft=35
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.45
|height ft=11
|height in=5
|wing area sqm=19.88
|wing area sqft=214
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=871
|empty weight lb=1,921
|gross weight kg=1,158
|gross weight lb=2,553
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Armstrong Siddeley Lynx]] IV 7-cylinder radial
|eng1 kw=170<!-- prop engines -->
|eng1 hp=230<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=193
|max speed mph=120
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=4,496
|ceiling ft=(service) 14,750
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=2× [[Vickers Auto RC]] (type E) machine guns in wings
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Vickers aircraft}}
{{reflist}}
* {{cite book |title=Vickers Aircraft since 1908 |last=Andrews |first=C. F. |last2=Morgan |first2=E. B. |edition=2nd |year=1988 |publisher=Putnam |location=London |isbn=0-85177-815-1|ref=harv}}
{{refbegin}}
{{refend}}

<!-- ==External links== -->
{{Vickers aircraft}}

[[Category:British fighter aircraft 1920–1929]]
[[Category:Vickers aircraft|Vireo]]