{{good article}}
{{Infobox NCAA football yearly game
| Game Name          = Sun Bowl
| Optional Subheader = 
| Title Sponsor      = <!--None prior to 1986 Sun Bowl -->
| Image              = <!-- Commented out: [[File:SunBowloldlogo.jpg|200px]] -->
| Caption            = Sun Bowl logo used prior to corporate sponsorship
| Date Game Played   = January 1
| Year Game Played   = 1947
| Football Season    = 1946
| Stadium            = [[Kidd Field]]
| City               = [[El Paso, Texas]]
| Visitor School     = Virginia Polytechnic Institute and State University
| Visitor Name Short = Virginia Tech
| Visitor Nickname   = Hokies
| Visitor Record     = 3&ndash;3&ndash;3  <!-- Field should contain record prior to game -->
| Visitor AP         = 
| Visitor Coaches    = 
| Visitor Coach      = [[James Kitts]]
| Visitor1           = 0
| Visitor2           = 0
| Visitor3           = 0
| Visitor4           = 6
| Home School        = University of Cincinnati
| Home Name Short    = Cincinnati
| Home Nickname      = Bearcats
| Home Record        = 8&ndash;2 <!-- Field should contain record prior to game -->
| Home AP            = 
| Home Coaches       = 
| Home Coach         = [[Ray Nolting]]
| Home1              = 0
| Home2              = 0
| Home3              = 12
| Home4              = 6
| MVP                = 
| Odds               = 
| Anthem             = 
| Referee            = 
| Halftime           = 
| Attendance         = approximately 10,000<ref name="Hokiesports recap">[http://www.hokiesports.com/football/bowls/1947Sun.html 1947 Sun Bowl] Virginia Tech Sports Information Department, Hokiesports.com. Accessed December 30, 2007.</ref>
| Payout             = 9,438 per team<ref name="Year-By-Year Game Results">[http://www.sunbowl.org/index.php?option=com_content&task=view&id=46&Itemid=37 Year-By-Year Game Results] Brut Sun Bowl, Accessed March 5, 2008.</ref>
| US Network         = 
| US Announcers      = 
| Ratings            = 
}}

The '''1947 [[Sun Bowl]]''' was a post-season American [[college football]] [[bowl game]] between the [[Virginia Tech Hokies football|Virginia Tech Hokies]] of the [[Southern Conference]] and the [[Cincinnati Bearcats football|Cincinnati Bearcats]]. It took place on January 1, 1947, at [[Kidd Field]] in [[El Paso, Texas]].<ref name="Hokiesports recap"/> Cincinnati defeated Virginia Tech 18–6 in cold and icy conditions that led to a scoreless first half and three blocked [[extra point]]s by Virginia Tech. The game was the first [[National Collegiate Athletic Association|NCAA]]-sanctioned post-season football contest for Cincinnati,<ref>[http://grfx.cstv.com/photos/schools/cinn/sports/m-footbl/auto_pdf/06FBguide-Records151-192.pdf (PDF) UC Bowl History May Include First-Ever Bowl Game] Cincinnati Sports Information Department, ''2006 Football Media Guide'', Page 36. Accessed December 30, 2007. [http://www.webcitation.org/5kNGDTEwG Archived] 2009-10-08.</ref>  and was the first bowl game in Virginia Tech history. The 1947 game was also the 13th edition of the Sun Bowl, which had been played every year since 1935.<ref name="Hokiesports recap"/> In exchange for their participation in the event, each team received $9,438.<ref name="Year-By-Year Game Results"/>

== Pregame buildup ==

The 1947 Sun Bowl game was held as the culminating event of the Sun Carnival and was held at 15,000-seat Kidd Field on the campus of Texas Western University, today known as the [[University of Texas El Paso]].<ref name="Sun Bowl Attendance"/> The matchup of Virginia Tech and Cincinnati was out of character for the Sun Bowl, which traditionally matched the champion of the [[Border Conference]] with the best possible opponent.<ref>[http://www.sunbowl.org/index.php?option=com_content&task=view&id=43&Itemid=54 Sun Bowl History] Brut Sun Bowl, Accessed March 5, 2008. [http://www.webcitation.org/5kNF2Z1AO Archived] 2009-10-08.</ref> [[Hardin-Simmons University]], champions of the Border Conference, declined a Sun Bowl bid, as did the second-place team, [[Texas Tech Red Raiders football|Texas Tech]]. With no other option, a member of the Sun Bowl Committee—who happened to be an alumnus of Virginia Tech—suggested inviting the Hokies to play against Cincinnati, which had already accepted an invitation.<ref>Colston, Chris. ''Tales from the Virginia Tech Sidelines''. Sports Publishing LLC, 2003. Page 26.</ref>

=== Virginia Tech ===

Virginia Tech came into the game having gone 3-3-3 under coach [[James Kitts]]. Kitts, in his first year replacing coach [[H. M. McEver]], had coached the team in 1941 before the outbreak of [[World War II]]  interrupted the Virginia Tech football program. During the 1946 season, Kitts' team defeated the No. 12 [[NC State Wolfpack football|N.C. State Wolfpack]] for the first win over an [[Associated Press]] Top 25 team in school history,<ref>[http://www.hokiesports.com/football/2004MG/history.pdf The Hokies and the AP Ratings] (PDF) Virginia Tech Sports Information, ''2004 Football Media Guide'' Page 42. Accessed December 30, 2007. [http://www.webcitation.org/5kNFPnHbH Archived] 2009-10-08.</ref> the [[Washington and Lee University]] Generals, and the Hokies' traditional rivals, the [[Virginia Military Institute]] Keydets.<ref>[http://www.hokiesports.com/football/2004MG/history.pdf Year-by-Year Scores and Results] (PDF) "1946", Virginia Tech Sports Information, ''2004 Football Media Guide'' Page 37. Accessed December 30, 2007. [http://www.webcitation.org/5kNFPnHbH Archived] 2009-10-08.</ref> During Virginia Tech's preparations before its departure for El Paso, heavy snow fell on [[Blacksburg, Virginia]], forcing the team to use [[snowplow]]s and construction equipment to clear a space for the team to practice.<ref>[http://www.hokiesports.com/football/2007MG/records.pdf Tech's Bowl History] (PDF) Virginia Tech Sports Information Department, 2007 Virginia Tech Football Media Guide, Page 32. Accessed December 30, 2007.</ref> The Hokies traveled to El Paso without star [[Punter (American football)|punter]] and rusher Bobby Smith, who had been injured in Virginia Tech's final regular-season game.<ref>[http://www.hokiesports.com/football/recaps/07312000aaa.html Six Names To Tech Hall Of Fame] Virginia Tech Sports Information Department, Hokiesports.com, July 31, 2000. Accessed December 30, 2007.</ref>

=== Cincinnati ===

The Cincinnati Bearcats traveled to El Paso having amassed an 8–2 record under second-year head coach Ray Nolting, who took the head coaching position in Cincinnati with the revival of the football program after the end of World War Two. The Bearcats' two losses came against Kentucky and at Tulsa, and they earned wins against tough opponents such as [[Indiana Hoosiers football|Indiana]], [[Michigan State Spartans football|Michigan State]], and [[Ohio Bobcats football|Ohio]].<ref name="First bowl game">[http://grfx.cstv.com/photos/schools/cinn/sports/m-footbl/auto_pdf/06FBguide-Records151-192.pdf Year-by-Year scores] (PDF) Cincinnati Sports Information Department, ''2006 Football Media Guide'', Page 31. Accessed December 30, 2007. [http://www.webcitation.org/5kNGDTEwG Archived] 2009-10-08.</ref>

The 1947 Sun Bowl was the Bearcats' first official bowl game, but two 1902 post-season games in [[New Orleans, Louisiana]], may be considered the first bowl games in Cincinnati football history. In 1902, Cincinnati completed a 7-1-1 season, losing only to the [[Carlisle Indians football|Carlisle Indians]]. Following the conclusion of its football schedule, the Bearcats were invited to New Orleans by the Southern Athletic Club to play a football game on New Year's Day. Cincinnati easily defeated the Athletic Club team, and at the victory party following the win, students from nearby [[Louisiana State University]] invited the Cincinnati players to come to their school to play another game. That game, which took place a few days later, resulted in a 22–0 Cincinnati win, and may be considered the first bowl game.<ref name="First bowl game"/>

== Game summary ==

<!-- Deleted image removed: [[File:1947 Sun Bowl.jpg|thumb|250px|right|Virginia Tech (dark uniforms) and Cincinnati (white uniforms) battle for possession of the ball during the 1947 Sun Bowl. Snow can be seen on the field and on the mountains in the background.]] -->
The game was played in extremely cold and icy conditions, still the worst in Sun Bowl history.<ref>http://www.sunbowl.org/game-a-events/sun-bowl-game/game-history/bowl-summary Referenced December 6, 2010.</ref> Three inches of snow fell on top of a layer of frozen rain the day before the game, and at kickoff the teams took the field under cloudy skies and in below-freezing temperatures.<ref name="Hokiesports recap"/> Despite the inclement weather, 15,000-seat Kidd Field was approximately half full, and bowl officials estimated the crowd at around 10,000 people.<ref name="Sun Bowl Attendance">[http://www.sunbowl.org/index.php?option=com_content&task=view&id=44&Itemid=76 Brut Sun Bowl - Sun Bowl Attendance Figures] Sunbowl.org. Accessed December 30, 2007.</ref>

Weather conditions allowed both teams' defenses to dominate in the first half. Virginia Tech had the best chance to score of either team in the first half when it drove to a [[Down (American football)|first down]] inside the Cincinnati two-yard line late in the first quarter.<ref name="Hokiegames.com">[http://hokiegames.com/blog/2007/12/12/hokie-bowl-20%E2%80%941947-sun-bowl/ Hokie Bowl #20—1947 Sun Bowl] The Hokie Games Blog, December 17, 2007. Accessed December 30, 2007.</ref> On four straight running plays, however, the Bearcats' defense held, and Virginia Tech was denied a scoring opportunity.<ref name="Hokiesports recap"/>

In the second half, however, Cincinnati's offense managed to begin moving the ball effectively. On Cincinnati’s first play of the second half, [[running back|halfback]] Roger Stephens broke through the Virginia Tech [[defensive line]] for 26 yards, taking the ball inside Virginia Tech territory. Cincinnati's drive would overcome two 15-yard penalties and one five-yard penalty en route to a [[touchdown]] just a few plays later.<ref name="Hokiegames.com"/> On its next possession, Cincinnati's [[All-American]] Roger Stephens again broke off another long run, this time for 19 yards, setting up another Bearcats' touchdown.<ref name="Hokiesports recap"/> Virginia Tech countered with a long drive that reached the Cincinnati 23-yard line before an errant pass was intercepted by the Bearcats in the end zone. Virginia Tech managed a defensive stop, however, and marched down the field for a touchdown to climb within six points. Cincinnati sealed its victory, however, when Bearcats' halfback Harold Johnson intercepted a Virginia Tech pass late in the fourth quarter, returning it all the way to the Virginia Tech 25-yard line. That return set up a Cincinnati touchdown and put the Bearcats up by the game's final score, 18–6.<ref name="Hokiesports recap"/><ref name="Hokiegames.com"/>

== Statistical summary ==

<div style="margin-left: 10px; margin-bottom: 10px; float: right;">
{| class="wikitable"
|+ '''Statistical Comparison'''<ref name="Cincy recap"/>
!  !! UC !! VT
|-
| '''1st Downs''' || 16 || 13
|-
| '''Total Yards''' || 463 || 119
|-
| '''Passing Yards''' || 94 || 85
|-
| '''Rushing Yards''' || 369 || 34
|-
| '''Penalties''' || 9-100 || 3-25
|-
| '''Turnovers''' || 0 || 2
|}
</div>

Virginia Tech blocked all three Cincinnati [[extra point]] attempts, while Virginia Tech's sole extra point kick missed.<ref name="Hokiesports recap"/> Cincinnati's kicking woes were also reflected in their [[Punt (gridiron football)|punt]]ing game. Cincinnati averaged just 19 yards a punt, setting the record for the lowest punting average in Sun Bowl history.<ref>[http://www.sunbowl.org/index.php?option=com_content&task=view&id=52&Itemid=82 Team Records] Brut Sun Bowl, Accessed March 5, 2008.</ref> All 24 of the game's points were scored in the second half.<ref>[http://www.hokiesports.com/football/2007MG/bowl.pdf FedEx Orange Bowl Extends Hokies' Bowl Streak] (PDF) Virginia Tech Sports Information Department, ''2008 Virginia Tech Orange Bowl Guide'', Page 4. Accessed December 30, 2007.</ref> Virginia Tech earned just 34 rushing yards against the Bearcats' defense while allowing 369 yards to Cincinnati's rushing offense. Those two totals are the least-gained and most-allowed marks in Virginia Tech bowl game history.<ref>[http://www.hokiesports.com/football/2007MG/bowl.pdf Team Bowl Marks] (PDF) Virginia Tech Sports Information Department, ''2008 Virginia Tech Orange Bowl Guide'', Page 96. Accessed December 30, 2007.</ref>

Prior to 1954, the Sun Bowl did not award [[most valuable player]] honors,<ref>[http://www.sunbowl.org/index.php?option=com_content&task=view&id=49&Itemid=37 C.M. Hendricks Most Valuable Player] Brut Sun Bowl, Accessed March 5, 2008.</ref> but Harold Johnson from Cincinnati was statistically the game's most valuable player, intercepting two Virginia Tech passes (one in the end zone) and scoring the first touchdown of the game on a 13-yard run.<ref name="Cincy recap">[http://gobearcats.cstv.com/sports/m-footbl/spec-rel/bowl-history-47-sun.html 1947 Sun Bowl] Cincinnati Athletics Department, Gobearcats.cstv.com. Accessed March 5, 2008.</ref>

==References==
{{Reflist|2}}

{{1946 bowl game navbox}}
{{Sun Bowl navbox}}
{{Cincinnati Bearcats bowl game navbox}}
{{Virginia Tech Hokies bowl game navbox}}

[[Category:1946–47 NCAA football bowl games|Sun Bowl]]
[[Category:Sun Bowl]]
[[Category:Cincinnati Bearcats football bowl games]]
[[Category:Virginia Tech Hokies football bowl games]]
[[Category:1947 in Texas|Sun Bowl]]
[[Category:January 1947 sports events]]