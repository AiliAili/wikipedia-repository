{{Infobox officeholder
|name          = Richard Edward Enright
|image         = Enright circa 1915.png
|caption       =Enright in 1918
|birth_name    = Richard Edward Enright
|birth_date    = {{birth date|1871|9|30}}
|birth_place   = [[Campbell, New York]], [[United States]]
|death_date    = {{death date and age|1953|9|4|1871|9|30}}
|death_place   = [[East Meadow, New York]]
|death_cause   = spinal injury caused by a fall
|resting_place =
|resting_place_coordinates =
|residence     =
|nationality   = [[United States|American]]
|other_names   =
|office   = [[New York City Police Commissioner]] 
| term_start =January 23, 1918 | term_end =January 1, 1925 | appointed =[[John Francis Hylan]] | predecessor =[[Frederick Hamilton Bugher]]	| successor = [[George Vincent McLaughlin]]
|education     =
|alma_mater    =
|employer      =
|occupation    = Police officer, private detective
|home_town     =
|title         =
|salary        =
|networth      =
|height        =
|weight        =
|term          =
|party         =
|boards        =
|religion      =
|spouse        = Jean Smith Enright
|partner       =
|children      =
|parents       =
|relations     = Willian Enright, brother<br />Patrick Enright, brother
|signature     =
|website       =
|footnotes     =
}}
[[File:Richard Enright 1918.jpg|thumb]]
'''Richard Edward Enright''' (August 30, 1871 &ndash; September 4, 1953) was an American law enforcement officer, detective, crime writer and served as [[New York City Police Commissioner|NYPD Police Commissioner]] from 1918 until 1925. He was the first man to rise from the rank-and-file to assume command of the NYPD and, until the appointment of [[Lewis Joseph Valentine]], he was the longest serving commissioner.

Although his eight-year tenure as commissioner received heavy criticism at the time of his resignation, mostly as the result of controversial actions of then Mayor [[John F. Hylan]],  his accomplishments and successes were eventually recognized as valued contributions during his near 30-year service on the police force.

==Biography==
===Early life and police career===
Richard Enright was born in [[Campbell, New York]] on August 30, 1871. He worked as a telegraph operator in [[Elmira, New York|Elmira]] and [[Queens]] before joining the [[New York City Police Department]] in 1896. He was described as being educated and very well-read, being able to recite poetry by heart, and was an avid student of art and history especially the life of [[Napoleon Bonaparte]]. A gifted and eloquent speaker, he became well liked by the men under his command.<ref name="Article">"R.E. Enright Dies; Headed City Police -- Commissioner Under Hylan, First to Rise From Ranks, Had Been Hurt In Fall. Organized Vice Squads; His Eight Years in Office Set Record for Length of Time -- Eased Work Conditions". <u>New York Times.</u> 5 Sep 1953</ref><ref name="Bailey">Bailey, William G., ed. ''The Encyclopedia of Police Science''. New York: Garland Publishing, 1995. (pg. 156-157) ISBN 0-8153-1331-4</ref><ref name="Roth">Roth, Mitchel P. ''Historical Dictionary of Law Enforcement''. Westport, Connecticut: Greenwood Press, 2000. (pg. 106-107) ISBN 0-313-30560-9</ref>

He slowly rose through the ranks to [[lieutenant|police lieutenant]] and, although a public unknown, he was highly popular on the force as a champion for the rank-and-file officers while president of the Police Lieutenants' Benevolent Association. His popularity and pro-union views had a negative impact on his career however, most especially his criticism of the policies of the [[John Purroy Mitchel|Mitchel administration]], resulting in his being passed over for promotion to [[police captain]] three times "for the good of the service".<ref name="Article"/><ref name="Bailey"/><ref name="Roth"/>

===Rise to Police Commissioner===
A change in administrations enabled Enright to succeed [[Frederick H. Bugher]] as Police Commissioner, becoming the first police officer to be appointed from within the ranks.<ref name="Roth"/> Bugher had incurred the wrath of Mayor [[John Hylan]] for his resisting the mayor in his attempts to interfere with the police department by refusing to take "guidance". Enright proved to be more open to Mayor Hylan and was officially appointed commissioner on January 23, 1918. As time went on however, even Enright would reach his limits as the mayor continued in his efforts to control the police force.<ref name="Article"/>

As a result of Hylan's meddling, Enright became the scapegoat for the mayor's controversial decisions. One of the most serious criticisms was the virtually forced retirement of noted detective Daniel Costigan|Daniel "Honest Dan" Costigan, then active in the Parkhurst vice investigations, who became the first of many old-time officers either transferred, demoted or forced into retirement. He was, however, able to effectively counter the manpower shortage and rising crime in the post-[[World War I]] period by creating old-style "strong-arm squads".<ref name="Article"/>

Enright specifically targeted [[illegal gambling]] establishments run by former [[U.S. Congressman]] and underworld figure [[John Kelly (U.S. politician)|"Honest" John Kelly]] and, during his first four years as police commissioner, assigned a uniformed patrolman to 24-hour watch outside Kelly's ''Vendome Clun'' on West 141st Street. Enright's continued harassment of Kelly's operations eventually forced him to sell the building to a Republican political organization.<ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 315) ISBN 1-56025-275-8</ref><ref>Asbury, Herbert. ''Sucker's Progress: An Informal History of Gambling in America from the Colonies to Canfield''. New York: Dodd, Mead & Company, 1938. (pg. 434)</ref>

Enright was also able to institute a number of reforms and was greatly able to improve working conditions for police officers while in office. He allowed a day off for officers after every six days on duty, oversaw the buildup of large police relief funds and improved the [[pension system]]. He also reduced the number of precincts for better management, set up a special police unit to handle vice and gambling on a city-wide basis, reorganized the arrest-quota based merit system and established the first NYPD police camp at [[Tannersville, New York]] where ill or wounded officers could recover until they were able to return to duty.<ref name="Article"/> Other reforms he made included successfully petitioning the federal government to exempt police officers from being drafted, established the Missing Persons Bureau as a 24-hour service, and increased the number of policewomen on the force. He would, however, encounter heavy resistance in attempting to rid the department of corrupt and inefficient officers.<ref name="Bailey"/><ref name="Roth"/>

The [[International Police Conference]] was developed by Enright in order to promote greater international cooperation among the world's police forces.<ref name="Article"/> He also advocated [[fingerprinting|universal fingerprinting registration]], not only for crime prevention but to resolve any kind of identity question, as well as convicted criminals being required to pay damages to both the victim and police with money earned while in prison.<ref name="Bailey"/>

===Prohibition, scandal and retirement===
During the early years of [[Prohibition]], the police force came under fire for widespread corruption and its ineffectiveness in the enforcement of the [[Volstead Act]] and the rising violence from the "Bootleg Wars". In 1921, a grand jury and Legislative investigation committee looked into the department's activities regarding [[Graft (politics)|graft]] and [[police corruption]]. Partially as a result of their findings, Enright's resignation was demanded by the [[Citizens Union]] and several newspapers. The next year, Enright issued [[libel|libel suits]] against an assemblyman and city magistrate based on correspondence charging police bootleg grafting. Though he lost in both cases, he did receive an apology from the two men.<ref name="Article"/>

Enright addressed the issue of Prohibition enforcement noting his embarrassment over the recent "crime waves". In 1924, Enright attempted to press charges against thirteen inspectors, a number of deputy inspectors and police captains for failing to enforce the Volstead Act, but was never able to establish his cases.<ref name="Bailey"/> In frustration, he resigned from the police force on December 30, 1925, the day before his term in office was to expire, presumably to become eligible for a police pension which he later received. At the time of his retirement, it was acknowledged that Enright had brought "a fair amount of professional knowledge and efficiency" during his time as commissioner.<ref name="Article"/>

He had a brief, but successful career as a [[crime writer]] the year following his departure from the police force. He had previously written a detective story, ''Inside the Net'', while still in office and which was adapted to a [[silent film|motion picture]] in 1924. His first book, ''Vultures of the Dark'', was published a year later and met with some commercial success. His second novel, ''The Borrowed Shield'', failed to achieve the same success and Enright retired from writing soon after. His published a police manual that same year entitled ''Syllabus and Instruction Guide of the Police Academy''.<ref name="Bailey"/><ref name="Roth"/>

===Later years===
In the years following his retirement, he served as a colonel in the [[Army Reserves]], briefly published a [[pulp magazine]] and had an interest in automatic alarm signals for stores and businesses. In 1933, Enright was employed by the federal government during the [[Great Depression]] to set up an enforcement division for the [[National Recovery Administration]]. He also became the director of the United Service Detective Bureau, a position he held until his death.<ref name="Bailey"/><ref name="Roth"/> In 1947, he petitioned the Police Pension Fund to increase his pension from $3,750 to $6,000 but was rejected.<ref name="Article"/>

On the evening of September 3, 1953, the 82-year-old Enright suffered a serious spinal injury when he fell at the Clocks Boulevard home of a female friend, Mrs. Mary D. Beal, in [[Massapequa, New York|Massapequa, Long Island]]. He was taken to [[Meadowbrook Hospital]] where he died of his injuries the following day.<ref name="Bailey"/><ref name="Roth"/> Enright was survived by his brother Patrick Enright and five nephews.<ref name="Article"/>

==Bibliography==
===Books===
*''Syllabus and Instruction Guide of the Police Academy'' (1925)

===Articles===
*''Police Commissioner Richard E. Enright Replies to His Critics: No "Crime Wave" in New York City'' <u>New York Times.</u> (January 1921)
*''"Enright Proposes Civic Center for City. He Would Use Twelve Blocks Above North End of Central Park. Project for a Great Municipal Site is Linked with Plan to Relieve Traffic Congestion in Lower Manhattan: He Would Use Twelve Blocks Above North End of Central Park"'' <u>New York Times.</u> (May 1924)
*''"Everybody Should Be Fingerprinted"''. <u>Scientific American.</u> (October 1925)

===Fiction===
*''Into the Net'' (1924)
*''Vultures of the Dark'' (1925)
*''The Borrowed Shield'' (1925)

===Speeches===
*''Tribute by Police Commissioner Richard E. Enright at Unveiling of Monument Dedicated to Reverend Francis J. Sullivan, Late Chaplain of the Police Department, at Calvary Cemetery, June 23, 1918'' (1918)
*''Address of Richard E. Enright, Police Commissioner: At the Dinner of the Mayor's Public Welfare Committee, at the Waldorf-Astoria, October 6, 1920'' (1920)
*''Address of Police Commissioner Richard E. Enright, Before the Motor Truck Association, October 27, 1920'' (1920)
*''Address by Richard E. Enright, Police Commissioner Delivered at the Dinner of the Casualty and Surety Club of New York, November 4, 1920'' (1920)
*''Address by Police Commissioner Richard E. Enright to the Kings County Grand Jurors Association: January 14, 1921'' (1921)
*''Address by Police Commissioner Richard E. Enright to the New York Methodist Preachers, May 2, 1921'' (1921)
*''Address by Police Commissioner Richard E. Enright at Dinner to His Excellency, the Peruvian Ambassador, Dr. Hernan Velarde, Hotel Plaza, January 29, 1925'' (1925)
*''Speech of Police Commissioner Richard E. Enright Before the Kings County Grand Jurors' Association, June 19, 1925'' (1925)

===Radio===
*''$10,500 Reward: The City of New York Offers $10,500 Reward for the Arrest and Furnishing of Evidence Sufficient to Warrant and Secure the Conviction of the Person Or Persons Guilty of Causing Or Perpetrating the Disastrous Explosion on September 16, 1920, on Wall Street Near Broad Street'' (1920)
*''"Traffic Problems"... Address of Police commissioner Richard E. Enright: By radio on police problems. First subject, September 5, 1923'' (1923)
*''"National Police Bureau" ... Address of Police commissioner Richard E. Enright: By radio on police problems. Second subject, September 12, 1923'' (1923)
*''"The Uniformed Force" ... Address of Police commissioner Richard E. Enright: By radio on police problems. Third subject, September 18, 1923'' (1923)
*''"Detective Bureau" ... Address of Police commissioner Richard E. Enright: By radio on police problems. Fourth subject, September 25, 1923'' (1923)
*''"Crime Prevention" ... Address of Police commissioner Richard E. Enright: By radio on police problems. Fifth subject, October 2, 1923'' (1923)
*''"Truth Versus Fiction" in Modern Detective Service ... Address of Police commissioner Richard E. Enright: By radio on police problems. Sixth subject, October 16, 1923'' (1923)
*''"Prohibition" ... Address of Police commissioner Richard E. Enright: By radio on police problems. Seventh subject, October 23, 1923'' (1923)
*''"Police Problems - Answers" ... Address of Police commissioner: By radio on police problems. Eighth subject, October 30, 1923'' (1923)

==References==
{{Reflist}}

==Further reading==
*Carey, Arthur A. and McLellan, Howard. ''Memoirs of a Murder Man''. New York: Doubleday, Doran and Company, 1930.
*Cole, Simon A. ''Suspect Identities: A History of Fingerprinting and Criminal Identification''. Cambridge: Harvard University Press, 2002. ISBN 0-674-01002-7
*Duncombe, Stephen Andrew Mattson. ''The Bobbed Haired Bandit: A True Story of Crime and Celebrity in 1920s New York''. New York: NYU Press, 2006. ISBN 0-8147-1980-5
*Gage, Beverly. ''The Day Wall Street Exploded''. New York: Oxford University Press, 2009. ISBN 978-0-19-514824-4
*Hickey, John J. ''Our Police Guardians: History of the Police Department of the City of New York, and the Policing of Same for the Past One Hundred Years''. New York: John J. Hickey, 1925.
*Lardner, James and Thomas Reppetto. ''NYPD: A City and Its Police''. New York: Macmillan, 2001. ISBN 0-8050-6737-X
*Lerner, Michael A. ''Dry Manhattan: Prohibition in New York City''. Cambridge: Harvard University Press, 2007. ISBN 0-674-02432-X
*Mackey, Thomas C. ''Pursuing Johns: Criminal Law Reform, Defending Character, And New York City's Committee of Fourteen, 1920-1930''. Columbus: Ohio State University, 2005. ISBN 0-8142-0988-2
*McCormick, Charles H. ''Hopeless Cases: The Hunt for the Red Scare Terrorist Bombers''. Lanham, Maryland: University Press of America, 2005. ISBN 0-7618-3133-9
*McDonald, Brian Vincent. ''My Father's Gun: One Family, Three Badges, One Hundred Years in the NYPD''. New York: Dutton, 1999. ISBN 0-525-94396-X
*Pietrusza, David. ''Rothstein: The Life, Times, and Murder of the Criminal Genius Who Fixed the 1919 World Series''. New York: Carroll & Graf Publishers, 2003. ISBN 0-7867-1250-3
*Washburn, Watson and Edmund DeLong. ''High and Low Financiers: Some Notorious Swindlers and Their Abuses of Our Modern Stock Selling System''. Indianapolis: Bobbs-Merrill, 1932.
*Willis, Clint, ed. ''NYPD: Stories of Survival from the World's Toughest Beat''. New York: Thunder's Mouth Press, 2002. ISBN 1-56025-412-2

==See also==
{{Portal|New York City|New York|Law enforcement/Law enforcement topics}}
*[[New York City Police Department]]

==External links==
* {{Internet Archive author |sname=Richard Enright |sopt=t}}

{{S-start}}
{{S-civ|pol}}
{{Succession box|before=[[Frederick H. Bugler]]|title=[[NYPD Commissioner]]|years=1918-1925 |after=[[George V. McLaughlin]]}}
{{S-end}}

{{DEFAULTSORT:Enright, Richard E.}}
[[Category:1871 births]]
[[Category:1953 deaths]]
[[Category:People from Steuben County, New York]]
[[Category:New York City Police Department officers]]