<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=C.12
 | image=C.12A.png
 | caption=C.12a
}}{{Infobox Aircraft Type
 | type=Single seat sport plane
 | national origin=[[Netherlands]]
 | manufacturer=Carley's Aeroplanes, [[Voorburg]]
 | designer=Joop D. Carley and Theo Slot<ref name=DNV/>
 | first flight=18 June 1923.<ref name=DNV/>
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=12 (all variants)
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The Carley '''C.12''' was a small [[Netherlands|Dutch]] single seat sporting [[monoplane]] from the 1920s.  There were several developments but only small numbers were produced.

==Design and development==
Like his earlier [[Carley S.1]], Joop Carley's C.12 was a compact single seat monoplane with a [[shoulder wing]]. It differed by being much lighter and all of the several engines fitted were much less powerful than the S.1's 37&nbsp;kW (50&nbsp;hp) [[Gnôme]].<ref name=DNV/> The wing of the C.12 was a [[cantilever]], thick [[airfoil|aerofoil section]], low [[aspect ratio (wing)|aspect ratio]] structure built around two wooden [[spar (aviation)#Wooden construction|box spar]]s, covered with [[plywood|three-ply]] ahead of the forward spar and [[aircraft fabric covering|fabric]] behind.<ref name=Flight1/>

The C.12's [[fuselage]] was constructed around three wooden [[longerons]] and 3-ply bulkheads, giving it a [[triangular]] cross section and entirely plywood covered.  The [[cockpit]], within a circular cut-out in the wing, placed the pilot high between the wing spars giving him a good forward view. There were sizeable cut-outs in the wing [[trailing edge]] to improve his rearward view downward. The [[empennage]] was conventional, the one piece [[elevator (aircraft)|elevator]] having a cut-out for [[rudder]] movement.  The fixed, [[conventional undercarriage]] was of the divided axle type, the axle enclosed within a wooden aerofoil [[aircraft fairing|fairing]] and hinged on the central lower longeron, with its wheels and extremities on rubber sprung V-form struts to the [[wing root]]s at the upper longerons.  The small, coil sprung tail skid steered with the rudder.<ref name=Flight1/>

For its first flight on 18 June 1923 at [[Waalhaven]] the C.12 had an 1,180&nbsp;L (73&nbsp;cu&nbsp;in) engine from a two-cylinder [[Indian Chief (motorcycle)|Indian Chief]] motorcycle, producing about 7.5&nbsp;kW (10&nbsp;hp); this was not satisfactory and was replaced by a slightly more powerful, four cylinder [[Sergant A]] air-cooled engine, which in turn proved unreliable.  Despite these problems Vliegtuig Industrie Holland (VIH), in English Aircraft Industry Holland, took an interest in what had become known as the "flying bicycle" and funded Carley, who had set up Carley's Aeroplanes Co. for the purpose, to fit an [[Anzani 3-cylinder fan engines|Anzani inverted Y-type]] air-cooled engine which produced about 15&nbsp;kW (20&nbsp;hp), mounted in the nose on a steel ring and driving a two blade [[propeller (aircraft)|propeller]].<ref name=Flight1/> At this point the aircraft was redesignated as the C.12a and later a headrest, faired aft into the fuselage, was added.<ref name=DNV/>

The cost of the refurbishment of the C.12 was much higher than Carley had indicated to VIH and in the Spring of 1924 he was pressed into leaving VIH and liquidating Carley's Aeroplanes.<ref name=DNVH/> VIH then acquired its assets, including the C.12a which remained on the Dutch register until February 1925. They also produced a developed version called the '''Holland&nbsp;H.2''', engineered by Theo Slot, one of the original designers, with H. Van der Kwast, Carley's Aeroplanes' old manager. The most obvious change was to the shape of the fuselage which was now rounded rather than triangular and much less deep at the nose, where the Anzani engine was retained.  The fin was also reshaped into a circular quadrant. It first flew on 11 July 1924.<ref name=DNVH/>

The final version followed the insolvency of VIH during 1924 and its acquisition by [[Pander & Son|H. Pander]], previously the owner of a furniture factory, who then set up Nederlandse Fabriek van Vliegtuigen H.&nbsp;Pander & Zonen (Dutch Aircraft Factory H.&nbsp;Pander & Son).   The first flight of the H.2 redesign, called the '''Pander&nbsp;D''', was on 16 November 1924. It had the same tail and engine as the Holland H.2 but the wing plan was altered to have more taper and rounded tips, as well as a greater span.  Its weight was slightly up and the maximum speed reduced by about 25%.<ref name=DNVP/>

[[File:C.12D.png|thumb|right|Carley C.12a]]

==Operational history==
On 18 December 1924, not long after the C.12a's first flight with the Anzani engine in October, the Belgian pilot Raparlier flew it from [[Waalhaven]], [[Rotterdam]] to [[Le Bourget]], [[Paris]] via [[Brussels]]. The whole flight took just under six hours, despite a one-hour stop in Brussels and a head wind on the second leg, slowing him down and forcing him to land near Le Bourget for extra fuel. The flight drew much attention; after a large number of demonstration flights in Paris Raparlier returned the C.12a to Waalhaven on 22 February 1924, again getting press attention.<ref name=DNV/><ref name=Flight2/><ref name=Flight3/>

After an attempt to compete with both the C.12a and the H.2, following the latter's successful first flight on 11 July, in the eight leg Tour de France des Avionettes was foiled by arguments over rules, the latter went for testing with the [[Dutch Naval Aviation Service|Marine-Luchtvaartdienst]] (MLD) but no sales resulted. It remained with Pander after his take-over but crashed fatally, from a spin, early in 1927.<ref name=DNV/>

Two Pander&nbsp;Ds were sold to the MLD, mostly used for pleasure flights, two to the [[KNIL]] who used them for stunt flying at airshows. Of six more civilian examples two were lost on test or delivery flights, two more were sold in [[France]] and [[Spain]], one went to the Dutch East Indies and the other was used for a time by a flying instructor in the Netherlands.<ref name=DNV/>

==Variants==

Data from Wesselink<ref name=DNV/>
;C.12: Original aircraft with Indian Chief then Sergant engine. First flew 18 June 1923.
;C.12a: Re-engined with 20&nbsp;hp Anzani.  First flew in October 1923. Faired headrest added.
;Holland H.2: Major fuselage and fin redesign by H. Van der Kwast and Theo Lock at VIH.  First flight 11 July 1924.
;[[Pander D]]: Further development of the H-2. First flight 16 November 1924.

==Specifications (C.12a) ==
[[File:C.12-3viewA.png|thumb|right|Carley C.12a]]
{{Aircraft specs
|ref=<ref name=Flight1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=4.80
|length note=
|span m=7.50
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=10.13
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=185
|empty weight note=
|gross weight kg=225
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Anzani 3-cylinder fan engines|Anzani]]
|eng1 type=3-cylinder inverted Y-type air cooled 
|eng1 hp=20
|eng1 note=
|power original=
|
|more power=

|prop blade number=2
|prop name=Pierre Levasseur
|prop dia m=1.7
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=140
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=35
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=3.5 h
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=19
|wing loading lb/sqft=
|wing loading note=
|power/mass=74.5 W/kg (0.045 hp/lb)
|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=DNV>{{cite book |title= De Nederlandse vliegtuigen |last=Wesselink|first=Theo|last2= Postma|first2=Thijs|year=1982|publisher=Romem  |location=Haarlem |isbn=90 228 3792 0|pages=18, 35}}</ref>

<ref name=DNVH>Wesselink (1982) p.41</ref>

<ref name=DNVP>Wesselink (1982) p.57</ref>

<ref name=Flight1>{{cite magazine|coauthors= |date=29 November 1923  |title= The Carley Light 'Plane|magazine= [[Flight International|Flight]]|volume=XV |issue=48 |pages=725–8|url= http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200725.html}}</ref>

<ref name=Flight2>{{cite magazine|coauthors= |date=27 December 1923  |title=Light 'Plane and Glider Notes|magazine= [[Flight International|Flight]]|volume=XV |issue=52 |page=779|url= http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200779.html}}</ref>

<ref name=Flight3>{{cite magazine|coauthors= |date=3 January 1924  |title=Light 'Plane and Glider Notes|magazine= [[Flight International|Flight]]|volume=XVI |issue=1 |page=7|url= http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200007.html}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->
<!-- Navboxes go here -->

[[Category:Dutch aircraft 1920–1929]]