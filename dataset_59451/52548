{{Infobox Bibliographic Database
|title = Philosophy Research Index
|image = 
|caption = 
|producer = [[Philosophy Documentation Center]]
|history = 2011-2014 (consolidated with [[PhilPapers]]) 
|languages = English, German, French, Spanish, and other western languages
|providers = [[Philosophy Documentation Center]]
|cost = Subscription with limited preview
|disciplines = Philosophy
|depth = Index 
|formats = Books. Journal, Disserations, other documents
|temporal = 1450-current
|geospatial = international
|number = 1.3 million +
|update = monthly
|ISSN = 
| eISSN = 2158-916X
|web = http://www.pdcnet.org/pri
|titles =
}}

The '''Philosophy Research Index''' is an indexing database containing bibliographic information on philosophical publications in several western languages. It contains listings for a range of philosophical publications, including books, anthologies, scholarly journals, dissertations, and other documents.<ref>{{cite web|url=http://www.pdcnet.org/pri |title=Philosophy Research Index web site|accessdate=2 August 2014|}}</ref> The first version of the database was launched by the [[Philosophy Documentation Center]] in 2011<ref>''Philosophy Research Index'' [[WorldCat]] Record, OCLC Accession No.690158987</ref> after a multi-year planning and development process, with technical support from Makrolog Content Management. It was established to build systematic coverage of philosophical literature in several languages in a manner that could be sustained for the long term. The database provides faceted searching, automatic translation and social networking functionality, and a visual time line for the display of search results, as well as OpenURL linking to fulltext resources. Philosophical topics covered include [[aesthetics]], [[epistemology]], [[ethics]], [[history of philosophy]], [[logic]], [[metaphysics]], [[philosophy of language]], [[philosophy of religion]], [[philosophy of science]], [[political philosophy]], and [[social philosophy]]. 

In July 2014 PDC partnered with the PhilPapers Foundation. All data in the Philosophy Research Index will be consolidated into the '''[[PhilPapers]]''' database. The Philosophy Research Index will no longer be available as a separate resource once commitments to its current customers have been fulfilled.<ref>{{Cite web|url=http://philpapers.org/bbs/thread.pl?tId=942 |title= 
PhilPapers to incorporate Philosophy Research Index, PhilPapers web site|accessdate=2 August 2014}}</ref>

==See also==
* [[List of academic databases and search engines]]

==Notes==
{{Reflist}}

== External links ==
* [http://www.pdcnet.org/pri Philosophy Research Index]
* [http://www.pdcnet.org/ Philosophy Documentation Center]
* [http://www.philpapers.org/ PhilPapers]

[[Category:Bibliographic databases and indexes]]
[[Category:Publications established in 2010]]
[[Category:Philosophy Documentation Center academic journals]]
[[Category:Philosophical databases]]