{{multiple issues|
{{citation style|date=April 2013}}
{{refimprove|date=April 2013}}
}}

{{Use dmy dates|date=April 2012}}
{{Infobox military person
|honorific_prefix  =
|name              =Arthur Deane Nesbitt
|honorific_suffix  =
|native_name       =
|native_name_lang  =
|image         =
|image_size    =
|alt           =
|caption       =
|birth_date    ={{birth date|1910|11|16|df=y}}
|death_date    ={{Death date and age|1978|02|22|1910|11|16|df=y}}
|birth_place   =[[Westmount, Quebec]], [[Canada]]
|death_place   =[[Montreal|Montreal, Quebec]], [[Canada]]
|placeofburial =[[Mount Royal Cemetery]]
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =
|birth_name    =
|allegiance    ={{flag|Canada|23px}}<br>{{flag|United Kingdom|23px}}
|branch        ={{air force|Canada|23px}}
|serviceyears  =1939–1945
|rank          =[[Group Captain]]
|servicenumber = <!--Do not use data from primary sources such as service records.-->
|unit          =
|commands      =No. 143 Wing RCAF
|battles       =[[World War II]]
*[[Battle of Britain]]
|battles_label =
|awards        =[[Order of the British Empire|Officer of the Order of the British Empire]]<br>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]<br>[[Croix de guerre 1939–1945|Croix de guerre]]
|spouse        = <!-- Add spouse if reliably sourced -->
|relations     =
|laterwork     =
|signature     =
|website       = <!-- {{URL|example.com}} -->
}}
'''Arthur Deane Nesbitt''' [[Order of the British Empire|OBE]], [[Distinguished Flying Cross (United Kingdom)|DFC]], [[Croix de guerre 1939–1945|CdeG]] (16 November 1910 – 22 February 1978) was a [[Canadians|Canadian]] businessman and a decorated [[aviator|pilot]] and [[Wing Commander (rank)|Wing Commander]] in [[World War II]].

== Early life ==
Deane Nesbitt was born in [[Westmount, Quebec]], the son of the very successful [[stockbroker]] and co-founder of [[Nesbitt, Thomson and Company]], [[Arthur James Nesbitt]]. Trained as an [[electrical engineer]],<ref name=":0">{{Cite web|url=http://www.bbm.org.uk/airmen/Nesbitt.htm|title=Battle of Britain London Monument - F/O A D Nesbitt|last=|first=|date=|website=www.bbm.org.uk|publisher=|archive-url=https://web.archive.org/web/20170123154332/http://www.bbm.org.uk/airmen/Nesbitt.htm|archive-date=2017-01-23|dead-url=no|access-date=2017-01-23}}</ref> after his older brother [[J. Aird Nesbitt|Aird]] decided to take over permanent management of the family-owned [[Ogilvy (Montreal)|Ogilvy department store]] in [[Montreal]], Deane Nesbitt joined the family's securities business. On the death of his father in 1954, he became head of the [[brokerage firm]] and took over the presidency of the Nesbitt Thomson [[holding company]], [[Power Corporation of Canada]]. Under his guidance, Nesbitt Thomson expanded across Canada, and into the [[United States]] and [[Europe]]. They were the first Canadian firm in three decades to obtain a seat on the [[New York Stock Exchange]].

A flying enthusiast, Nesbitt obtained his pilot's license and as a member of the Montreal Light Aeroplane Club,<ref name=":0" /> and was twice voted the James Lytell Memorial Trophy as the club's top [[aviator|pilot]].

== Wartime Service ==
He had 200 flying hours to his credit when he enlisted in the R.C.A.F. on 15 September 1939. Nesbitt was trained at [[Camp Borden]] as a [[fighter pilot]]. As a Flying Officer Nesbitt was then posted to No 1 Squadron RCAF, and flew Hurricanes with the unit during the [[Battle of Britain]] before being wounded on 15 September. On 26 August 1940 Nesbitt claimed a 'Do215' destroyed, and Bf 109s on 4 and 15 September. He also received the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]].

When recovered he later commanded No.401 Squadron, and returned to Canada in September 1941 to command No.14 Squadron and later No.111 Squadron. He was promoted to Wing Commander in June 1942 and given command of Station Annette Island.Nesbitt was made CO, No.6 SFTS, Dunneville, in December 1943, and in March 1944 returned to the UK to command No.144 Wing. he then joined No.83 Group HQ as Accidents Investigation Officer. Promoted to [[Group Captain]] on 1 January 1945, Nesbitt then commanded No.143 Wing, with Hawker Typhoons, from January to August 1945 before returning to Canada in September 1945, retiring in November 1947.

Along with his brother Aird who served the [[Canadian Army]], he was part of the liberation of the [[Netherlands]] and at [[Eindhoven]]. For his service, Deane Nesbitt received a number of military honours. In 1946 he was made an Officer of the [[Order of the British Empire]] and in 1947 a Commander of the [[Order of Orange-Nassau]] with Swords, and awarded the [[Croix de guerre 1939–1945|Croix de Guerre]] with Silver Star by the government of [[France]].

== Post war career ==
After being decommissioned at the end of the War, Group Captain Nesbitt rejoined the family's [[Saint Jacques Street|St. James Street]] securities firm. Highly successful in business, he also organised the financing for the [[TransCanada pipeline]] in the 1950s, the then-largest [[natural gas]] pipeline in the world and one of the most significant energy projects in the history of Canada. His long and successful business career earned him an induction into the [[Canadian Business Hall of Fame]].

At the age of 68, Deane Nesbitt suffered a [[ski]]ing accident that left him near totally paralyzed on 4 February.<ref>{{Cite web|url=http://airforce.ca/uploads/airforce/2009/07/ALPHA-NA.html|title=NAFTEL, F/L Leslie Roland (J5130) - Commended for Valuable Services in the Air - No|last=|first=|date=|website=airforce.ca|publisher=|archive-url=https://web.archive.org/web/20170123153606/http://rcafassociation.ca/uploads/airforce/2009/07/ALPHA-NA.html|archive-date=2017-01-23|dead-url=no|access-date=2017-01-23}}</ref> He died in Montreal in 1978 and was interred in the [[Mount Royal Cemetery]].<ref>{{Cite web|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=Nesbitt&GSiman=1&GSst=832&GRid=108226891&|title=Arthur Deane Nesbitt ( - 1978) - Find A Grave Memorial|last=|first=|date=|website=www.findagrave.com|publisher=|archive-url=https://web.archive.org/web/20170123154130/http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=Nesbitt&GSiman=1&GSst=832&GRid=108226891&|archive-date=2017-01-23|dead-url=no|access-date=2017-01-23}}</ref>

In his honor, the [[BMO Nesbitt Burns]] division of the [[Bank of Montreal]] established the A. Deane Nesbitt/Charles Burns Award recognizing exceptional performance.

== References ==
<references />
* [https://web.archive.org/web/20061010041034/http://www.airforce.ca/wwii/ALPHA-NA.html Air Force Association of Canada decorations for A. Deane Nesbitt]
* [https://web.archive.org/web/20060321124917/http://collections.ic.gc.ca/heirloom_series/volume6/332-333.htm Government of Canada, "The Nesbitts" - ''Nation Builders'' collection]

{{DEFAULTSORT:Nesbitt, Arthur Deane}}
[[Category:1910 births]]
[[Category:1978 deaths]]
[[Category:Canadian aviators]]
[[Category:Canadian World War II pilots]]
[[Category:Canadian businesspeople]]
[[Category:Canadian people of Scottish descent]]
[[Category:Canadian stockbrokers]]
[[Category:The Few]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Officers of the Order of the British Empire]]
[[Category:Power Corporation of Canada]]
[[Category:People from Montreal]]
[[Category:People from Westmount, Quebec]]
[[Category:Royal Canadian Air Force officers]]
[[Category:Stock and commodity market managers]]
[[Category:Anglophone Quebec people]]
[[Category:Recipients of the Croix de guerre 1939–1945 (France)]]
[[Category:Commanders of the Order of Orange-Nassau]]
[[Category:Royal Air Force pilots of World War II]]