
{{Infobox person
| name        = Samuel Galloway Hibben
| image       = Samuel Hibben image.jpg
| alt         = Samuel Hibben
| caption     = 
| birth_name  = 
| birth_date  = 6 June 1888
| birth_place = Hilsboro, [[Ohio]]
| death_date  = 11 June 1972
| death_place = 
| nationality = American
| other_names = 
| known_for   = Lighting the [[Statue of Liberty]] and [[Washington Monument]]
| occupation  = Lighting Designer
}}

'''Samuel Galloway Hibben''' (June 6, 1888 — June 11, 1972) had a distinguished career in the science and application of lighting. During a lifetime of employment with the [[Westinghouse Electric Company|Westinghouse Company]] he pioneered many new types of lighting and its applications in the home, the workplace and in public displays. Among his many noteworthy design achievements were the lighting of the [[Holland Tunnel]], the [[Statue of Liberty]], the [[Washington Monument]], and several underground caverns. He supplemented his design work with lectures and demonstrations to educate the public on the practical and aesthetic possibilities of lighting. He received several professional awards for his achievements, and in 2006 was designated one of the eight most distinguished pioneers in the field of lighting design.

==Birth and early life==
Samuel Hibben was born and raised in [[Hillsboro, Ohio]]. He attended Case Institute of Technology (now [[Case Western Reserve]]), graduating in 1910 with a degree in electrical engineering. At Case he worked part-time in the physics laboratory used by [[Albert A. Michelson]] to develop the optical interferometer, for which Michelson was awarded the [[Nobel Prize in physics]] in 1907. Hibben was inspired by Michelson’s work, and decided to study the field of lighting; his senior thesis was on the [[photometry (optics)|photometry]] of colored light.

==First job==
Hibben’s first job was with the [[MacBeth-Evans Glass Company]] in Pittsburgh, where he designed optical glass components. Notable among these was a system of lenses and mirrors for lighthouses, known as the [[catadioptric lens]], the first of its kind in the U.S.  His lenses were installed in a number of lighthouses, and were credited with a major improvement in the safety of coastal navigation <ref>New York Railroad Club, Official Proceedings, Jan. 15, 1942, p. 199.</ref>

==Life during the war==
Hibben left MacBeth to join the [[Westinghouse Electric Company]] in 1916.  He took time off to serve in the [[United States Army Corps of Engineers|Army Corps of Engineers]] in World War I, where he designed anti-aircraft searchlights and an acoustic artillery ranging system.

==Life post-WWI==
After the war he rejoined Westinghouse, where he remained for the rest of his career, most of it at the company‘s Lamp Division in Bloomfield, New Jersey. He was appointed Director of Applied Lighting there in 1933. The company recognized his talents, and gave him virtually a free hand in the development of light bulbs and innovative lighting designs until his retirement in 1954.

Hibben conceived the idea that the beauty of some geological formations could be enhanced by application of multicolored lighting. He first tried this out on the [[Natural Bridge (Virginia)|Natural Bridge of Virginia]] in 1927, where his team installed a controllable system of concealed floodlights that bathed the arch and adjacent rock walls at night in varying combinations of colors, which highlighted features not visible in daylight.<ref>Hibben, Samuel G.; Stephens, Phinehas V. The Illumination of the Natural Bridge of Virginia. Illuminating Engineering Society Transactions, v. 22, 1927, p. 1158-1164.</ref>  Nocturnal tours through the arch were an instant success, and inspired Hibben to apply the same techniques to the [[Endless Caverns]] in Virginia in 1928.<ref>Oglesby, W. A. Illumination of Endless Caverns. Illuminating Engineering Society Transactions, v. 23, 1928, p. 1231-1240.</ref>  The skill and artistry of this display were also warmly received by the public, and Hibben went on to design similar systems for [[Carlsbad Caverns|Carlsbad Caverns, New Mexico]] and Crystal Caves in Bermuda (1937).

Fairs and expositions provided a good venue for showcasing the latest in lighting technology, and Hibben’s Westinghouse team designed displays for a number of them, including the [[Philadelphia Sesquicentennial Exposition of 1926|Philadelphia Sesquicentennial]] (1926), Barcelona Exposition (1929), [[Century of Progress|Chicago World’s Fair]] (1933) and the [[1939 New York World's Fair|New York World’s Fair]] of 1939.<ref>Hibben, Samuel G. How New York World’s Fair Exhibitors Use Light. ibid., Sept. 1939, p. 855-880.</ref> Hibben’s lighting designs for the New York Fair were a milestone, being the first to introduce [[fluorescent lighting]] on a large scale.

One of Hibben’s most interesting challenges was the upgrading of the [[Statue of Liberty National Monument, Ellis Island and Liberty Island|Statue of Liberty torch]] lighting during World War II. The Statue lighting was turned off when the U.S. entered the war in 1941, and Westinghouse was commissioned to redesign the system. Hibben devised a combination of high power incandescent and mercury vapor lamps that created the illusion of a flaming torch, and also redesigned the floodlighting of the statue itself.<ref>http://scienceservice.si.edu/pages/025071.htm</ref><ref>http://scienceservice.si.edu/pages/018003.htm</ref>  The brilliant new array was switched on in a ceremony on [[V-E Day]], May 8, 1945.

In another wartime activity, Hibben was appointed to a committee of lighting experts, to study blackout techniques for defending cities against possible bombing attacks. Concluding that total blackouts were counterproductive, he introduced reduced lighting or ‘dimout’ techniques that would give protection while allowing nearly normal vehicular and industrial activity.<ref>Hibben, S.G.; Reid, K.M. Comments on Blackouts. Illuminating Engineering, April 1942, p. 210-216.</ref>

Hibben’s innovations extended to many other aspects of lighting. Among these were reflector designs for indirect lighting, cluster lighting for streets and highways, airport runway lighting, and underwater lights for swimming pools, as well as for deep sea research with pioneers [[William Beebe]] and [[Auguste Piccard]].<ref>New York Times Science Review, Aug. 24, 1947.</ref>  While most of his achievements were in the visible spectrum, he also pioneered studies in the ultraviolet and infrared regions. His research in the [[Bactericidal|bactericidal properties]] of the ultraviolet band—so-called [[black light]] —led to a myriad of new applications in sterile environments such as hospitals and food processing plants.

Hibben also made it his mission to raise public awareness of the proper uses and evolving possibilities of lighting. He lectured and published widely on the subject in both the professional and popular press, mainly in publications of the [[Illuminating Engineering Society]] (IES), where he was a lifelong member. He had a gift for explaining the physics of illumination in layman’s terms; his popular lectures often included dazzling demonstrations of lighting effects.

Hibben’s experiments with lighting sometimes had unexpected consequences. One disastrous event took place at a dinner meeting in a hotel, where he had replaced the standard dining room ceiling lights with a combination of red and green lights. These caused every dish on the menu to appear in such strange colors that most of the guests were revolted and unable to eat any of the meal.<ref>[http://blog.modernmechanix.com/2007/06/07/the-miracles-of-light/ Modern Mechanix blog; ''The Miracles of Light'' - 7 June 2007]</ref>

==Honours and awards==
His contributions to lighting earned Hibben a number of honors: the Westinghouse Order of Merit (1944), an honorary Doctorate in Engineering from Case Institute (1952), the IES Gold Medal (1962) and IES Distinguished Service Award (1969). A posthumous honor was added by the IES on its centennial celebration in 2006, when Samuel Hibben was cited as one of the eight most distinguished pioneers in the history of lighting design.

== References ==
{{reflist}}

== External links ==

{{DEFAULTSORT:Hibben, Samuel}}
[[Category:1888 births]]
[[Category:1972 deaths]]
[[Category:American lighting designers]]
[[Category:American scientists]]