{|{{Infobox Aircraft Begin
 |name= Nord 1500 Griffon
 |image= File:Nord 1500 Griffon II.jpg
 |caption=Griffon II
}}{{Infobox Aircraft Type
 |type=Research fighter
 |national origin=France
 |manufacturer=[[Nord Aviation]]
 |first flight= 20 September 1955 (Griffon I), 23 January 1957 (Griffon II)
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=2
 |variants with their own articles=
}}
|}

The '''Nord 1500 Griffon''' was an [[Experimental Aircraft|experimental]] [[ramjet]]-powered [[fighter aircraft]] designed and built in the mid-1950s by [[France|French]] state-owned aircraft manufacturer [[Nord Aviation]]. It was part of a series of competing programs to fill a French Air Force specification for a [[Mach number|Mach]] 2 [[Fighter aircraft|fighter]].

==Design and development==
Design of the Griffon originated in a late 1940s requirement for a high speed interceptor. Engineers at [[Arsenal de l'Aéronautique]] instigated studies into swept and delta wings using supersonic gliders, the [[Arsenal 1301]] and [[Arsenal 2301]]. Results from these flight tests favoured the delta configuration, which was incorporated into design studies using a variety of powerplants. By this time Arsenal had been privatised as [[SFECMAS]] - ''Société Française d'Etude et de Construction de Matériel Aéronautiques Spéciaux''. Powered by a large ramjet with turbojet sustainer, the Griffon was renamed from the '''SFECMAS 1500 Guépard''' (Cheetah) after SFECMAS was merged with [[SNCAN]] to form [[Nord Aviation]].<ref name="Arsenal">{{cite web |url=http://www.hydroretro.net/etudegh/arsenal_de_l_aeronautique.pdf |title=L’Arsenal de l’aéronautique |publisher=www.hydroretro.net |date=2007-08-18 |accessdate=2011-06-16}}</ref>

Two prototypes were ordered initially in a letter dated 24 August 1953, with the final contract, (No. 2003/55) in 1955. Although intended to eventually fulfil a requirement for a light interceptor capable of operation from 1,000m grass runways, the two prototypes were ordered without military equipment for research purposes only.<ref name="Griffon 1">{{cite web |url= http://jpcolliat.free.fr/griffon/griffon-1.htm |title=Griffon overview |publisher= jpcolliat.free.fr |date=2003-04-30  |accessdate=2011-06-16}}</ref>

Constructed mainly of light alloys, the Griffon comprised a large tubular fuselage which supported the middle set [[delta wing]]s, fin with rudder and the forward fuselage, which extended forwards over the turbo-ramjet air intake. The forward fuselage housed the single-seat cockpit and carried small delta [[Canard (aeronautics)|canards]] on either side of the cockpit. The tricycle undercarriage retracted into the wings and the underside of the air intake.<ref name="Griffon 1"/>

The design of the Griffon featured a dual [[turbojet]]-[[ramjet]] powerplant, with the turbojet enabling unassisted takeoffs (ramjets cannot produce thrust at zero airspeed and thus cannot move an aircraft from a standstill) and the ramjet producing extra thrust at airspeeds above 1,000&nbsp;km/h (600&nbsp;mph). To reduce risks in using the relatively new turbo-ramjet powerplant, the first Griffon (Nord 1500-01 Griffon I) was completed with only the 3,800 kgf thrust [[ATAR 101]]F turbojet component. First flown by [[Andre Turcat]] on  20 September 1955, the Griffon I proved to be underpowered but plans to install the planned ramjet component were never realised. Despite the lack of power the Griffon I still managed to reach M 1.17.<ref name="Griffon 1"/> Flying with the Griffon I ceased in April 1957 in favour of the ramjet-equipped Griffon II. Visible differences between the two aircraft were limited to the smaller intake and two-position exhaust nozzle of the Griffon I.<ref name="Griffon 2">{{cite web |url= http://jpcolliat.free.fr/griffon/griffon-2.htm |title=Griffon II |publisher= jpcolliat.free.fr |date=2003-04-30  |accessdate=2011-06-16}}</ref>

==Operational history==
After proving the aerodynamic aspects and systems of the Griffon, the 1500-01 was retired in April 1957. Flying continued with the Griffon II after its first flight on 23 January 1957. With Major [[André Edouard Turcat|André Turcat]] at the controls, the Griffon II reached a top speed of Mach 2.19 ({{convert|2330|km/h|disp=or|abbr=on}}) in 1958, thus proving the soundness of the basic design. However, the aircraft met several technical difficulties, such as kinetic heating, due to the lack of temperature-resistant materials, such as [[Inconel]] or [[titanium]], in the parts of the airframe experiencing the highest temperatures. The ramjet was found to work well at high speed, but was unstable at medium speeds.<ref name="Griffon 2"/>

Production of operational versions, dubbed '''Super Griffon''' did not take place as it was found that the requirements could be met and exceeded with less complex and cheaper aircraft such as the [[Dassault Mirage III]].<ref name="Griffon 3">{{cite web |url= http://jpcolliat.free.fr/griffon/griffon-3.htm |title=Griffon derivatives |publisher= jpcolliat.free.fr |date=2003-04-30  |accessdate=2011-06-16}}</ref>

==Variants==
;SFECMAS 1500 Guépard
:The original designation and name of the initial design studies carried out at SFECMAS.<ref name="Arsenal"/>
;Nord 1500-01 Griffon I
:The first aircraft completed with only the [[ATAR 101F]] afterburning turbojet component of the planned turbo-ramjet powerplant.<ref name="Griffon 1"/>
;Nord 1500-02 Griffon II
:The second aircraft fitted with the definitive turbo-ramjet powerplant.<ref name="Griffon 2"/>
;Nord Super Griffon
:A projected scaled up operational development of the Griffon II, with a 2m diameter ramjet and retractable canards, intended to reach M3, constructed mainly of stainless steel.<ref name="Griffon 3"/>

==Aircraft on display==
A preserved Nord 1500-02 Griffon II aircraft is on display in the [[French Air and Space Museum]], at [[Le Bourget]], near Paris.<ref name="Griffon 3"/>

==Specifications (Nord 1500-2 Griffon II)==
{{aircraft specifications
|jet or prop?=both <!-- this is a formatting trick to display two types of jet engine -->
|plane or copter?=plane
|has armament?=yes
|switch order of units?=
|ref=''Jane's''<ref>Taylor 1976, p. 167.</ref>
|crew=one
|length main=14.54 m
|length alt=47 ft 8.5 in
|span main= 8.10 m
|span alt=26 ft 7 in
|height main=5.00 m
|height alt=16 ft 5 in
|area main= 32 m²
|area alt=344.5 sq.ft
|empty weight main=
|empty weight alt=
|loaded weight main=6,745 kg
|loaded weight alt=14,839 lb
|max takeoff weight main=
|max takeoff weight alt=
|number of jets=1
|engine (jet)= [[ATAR 101]]E-3
|type of jet=[[turbojet]]  
|thrust main= 34.3 kN
|thrust alt=7,710 lbs
|afterburning thrust main=
|afterburning thrust alt=
|number of props=1<!-- this is a formatting trick -->
|engine (prop)=Nord Stato-Réacteur
|type of prop= [[ramjet]]
|power main= 68.0 kN
|power alt=15,290 lbs
|max speed main=[[Mach number|Mach]] 2.19
|max speed alt= 2,330 km/h
|cruise speed main=
|cruise speed alt= 
|range main= 
|range alt= 
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt=6,000 ?
|loading main=
|loading alt=210 kg/m²
|thrust/weight=
|armament=
}}

==See also==
{{aircontent
|related=
*[[Nord Gerfaut]]
|similar aircraft=
*[[Hawker P.1121]]
*[[Saunders-Roe SR.53]]
*[[Saunders-Roe SR.177]]
*[[SNCASO Trident]]
*[[XF-91 Thunderceptor]]
|lists=
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*Taylor, John W.R. ''Jane's Pocket Book of Research and Experimental Aircraft'', London, Macdonald and Jane's Publishers Ltd, 1976. ISBN 0-356-08409-4.
{{refend}}

== External links ==
{{Commons category|Nord 1500}}
*[http://jpcolliat.free.fr/griffon/griffon-1.htm Overview of the Griffon program] (in French)
*[http://jpcolliat.free.fr/griffon/griffon-2.htm Griffon II]
*[http://jpcolliat.free.fr/griffon/griffon-3.htm Griffon derivatives]
*[http://www.eads.net/1024/en/eads/history/airhist/1950_1959/nordaviation_nord1500_1954.html EADS - Nord 1500]
*[http://www.hydroretro.net/etudegh/arsenal_de_l_aeronautique.pdf Arsenal de lÁeronautique]
*[http://xplanes.free.fr/stato/stato-8.html turbo-ramjet genesis]

{{Nord aircraft}}

[[Category:French experimental aircraft 1950–1959]]
[[Category:Ramjet-powered aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Nord aircraft|Griffon]]
[[Category:Canard aircraft]]