{{refimprove|date=January 2015}}
'''CiteSeer''' was a public [[Search engine (computing)|search engine]] and [[digital library]] for scientific and academic papers, primarily in the fields of [[computer science|computer]] and [[information science]], that has been replaced by '''CiteSeer<sup>''X''</sup>'''. Many consider it to be the first academic paper search engine. It became public in 1998 and had many new features unavailable in academic search engines at that time. These included:

* Autonomous Citation Indexing automatically created a citation index that can be used for literature search and evaluation.
* Citation statistics and related documents were computed for all articles cited in the database, not just the indexed articles.
* Reference linking allowing browsing of the database using citation links.
* Citation context showed the context of citations to a given paper, allowing a researcher to quickly and easily see what other researchers have to say about an article of interest.
* Related documents were shown using citation and word based measures and an active and continuously updated bibliography is shown for each document.

It is often considered to be the first automated citation indexing system, has a patent on this topic, and was considered a predecessor of academic search tools such as [[Google Scholar]] and [[Microsoft Academic Search]].{{citation needed|date=January 2012}} CiteSeer-like engines and archives usually only harvest documents from publicly available websites and do not crawl publisher websites. As such authors whose documents are freely available are more likely to be represented in the index.

CiteSeer's goal is to improve the dissemination and access of academic and scientific literature. As a non-profit service that can be freely used by anyone, it has been considered as part of the [[Open access (publishing)|open access]] movement that is attempting to change [[academic publishing|academic and scientific publishing]] to allow greater access to scientific literature. CiteSeer freely provided [[Open Archives Initiative]] [[metadata]] of all indexed documents and links indexed documents when possible to other sources of metadata such as [[DBLP]] and the [[ACM Portal]]. To promote open data, '''CiteSeer<sup>''X''</sup>''' shares its data for non-commercial purposes under a Creative Commons license.<ref>{{Cite web|url = http://csxstatic.ist.psu.edu/about/data|title = CiteSeerX Data Policy|date = |accessdate = 2015-11-10|website = |publisher = |last = |first = }}</ref>

The name can be construed to have at least two explanations. As a pun, a 'sightseer' is a tourist who looks at the sights, so a 'cite seer' would be a researcher who looks at cited papers. Another is a 'seer' is a prophet and a 'cite seer' is a prophet of citations. CiteSeer changed its name to ResearchIndex at one point and then changed it back.

== History ==

=== CiteSeer and CiteSeer.IST ===

CiteSeer was created by researchers [[Lee Giles]], [[Kurt Bollacker]] and [[Steve Lawrence (computer scientist)|Steve Lawrence]] in 1997 while they were at the [[NEC Research Institute]] (now [[NEC Corporation|NEC]] Labs), [[Princeton, New Jersey]], USA.  CiteSeer's goal was to actively crawl and harvest academic and scientific documents on the web and use autonomous [[citation index]]ing to permit querying by citation or by document, ranking them by [[citation impact]]. At one point, it was called ResearchIndex.

After NEC, in 2004 it was hosted as CiteSeer.IST on the [[World Wide Web]] at the College of Information Sciences and Technology, The [[Pennsylvania State University]], and had over 700,000 documents. For enhanced access, performance and research, similar versions of CiteSeer were supported at universities such as the [[Massachusetts Institute of Technology]], [[University of Zürich]] and the [[National University of Singapore]]. However, these versions of CiteSeer proved difficult to maintain and are no longer available. Because CiteSeer only indexes freely available papers on the web and does not have access to publisher metadata, it returns fewer citation counts than sites, such as [[Google Scholar]], that have publisher metadata.  
<!-- for historical reference:
Versions of CiteSeer have been or are available at the following links:
* [http://citeseer.ittc.ku.edu Univ. of Kansas]
* [http://citeseer.csail.mit.edu MIT]
* [http://sherry.ifi.unizh.ch Univ. of Zürich]
* [http://citeseer.comp.nus.edu.sg/cs National Univ. of Singapore]

-->

CiteSeer had not been comprehensively updated since 2005 due to limitations in its architecture design.  It had a representative sampling of research documents in computer and information science but was limited in coverage because it was limited to papers that are publicly available, usually at an author's homepage, or those submitted by an author. To overcome some of these limitations, a modular and open source architecture for CiteSeer was designed - CiteSeerX.

=== CiteSeer<sup>X</sup> ===

'''CiteSeer<sup>''X''</sup>''' replaced CiteSeer and all queries to CiteSeer were redirected. CiteSeer<sup>''X''</sup><ref name="about-page">{{Cite web | title = About CiteSeerX |  accessdate = 2010-05-07 | url = http://citeseerx.ist.psu.edu/about/site}}</ref> is a public [[search engine]] and [[digital library]] and [[Disciplinary repository|repository]] for scientific and academic papers primarily with a focus on [[computer science|computer]] and [[information science]].<ref name="about-page"/> However, recently CiteSeerX has been expanding into other scholarly domains such as economics, physics and others. Released in 2008, it was loosely based on the previous CiteSeer search engine and digital library and is built with a new [[open source]] infrastructure, SeerSuite, and new algorithms and their implementations. It was developed by researchers Dr. Isaac Councill and Dr. C. [[Lee Giles]] at [[Penn State College of Information Sciences and Technology|the College of Information Sciences and Technology]], [[Pennsylvania State University]]. It continues to support the goals outlined by CiteSeer to actively crawl and harvest academic and scientific documents on the public web and to use a citation inquery by citations and ranking of documents by the impact of citations. Currently, Lee Giles, Prasenjit Mitra, Susan Gauch, Min-Yen Kan, Pradeep Teregowda, Juan Pablo Fernández Ramírez, Pucktada Treeratpituk, Jian Wu, Douglas Jordan, Steve Carman, Jack Carroll, Jim Jansen, and Shuyi Zheng are or have been actively involved in its development. Recently, a table search feature was introduced.<ref>{{Cite web | title = The CiteSeerX Team | publisher = Pennsylvania State University | accessdate = 2010-07-24 | url = http://citeseerx.ist.psu.edu/about/team }}</ref>  It has been funded by the [[National Science Foundation]], [[NASA]], and [[Microsoft Research]].

CiteSeerX continues to be rated as one of the world's top repositories and was rated number 1 in July 2010.<ref>{{Cite web | title = Ranking Web of World Repositories: Top 800 Repositories | publisher = Cybermetrics Lab | date = July 2010 | url = http://repositories.webometrics.info/top800_rep.asp | accessdate = 2010-07-24 }}</ref> It currently has over 6 million documents with nearly 6 million unique authors and 120 million citations.

CiteSeerX also shares its software, data, databases and metadata with other researchers, currently by [[Amazon S3]] and by [[rsync]].<ref>{{ Cite web | title = About CiteSeerX Data | publisher = Pennsylvania State University | accessdate = 2012-01-25 | url = http://csxstatic.ist.psu.edu/about/data }}</ref> Its new modular open source architecture and software (available previously on [[SourceForge]] but now on [[GitHub]]) is built on [[Apache Solr]] and other [[Apache Software Foundation|Apache]] and open source tools which allows it to be a testbed for new algorithms in document harvesting, ranking, indexing, and information extraction.

==Current Features==

=== Automated Information Extraction ===

CiteSeerX uses automated information extraction tools, usually built on machine learning methods such ParsCit, to extract scholarly document metadata such as title, authors, abstract, citations, etc. As such, there are sometime errors in authors and titles. Other academic search engines have similar errors.

=== Focused Crawling ===

CiteSeerX crawls publicly available scholarly documents primarily from author webpages and other open resources, and does not have access to publisher metadata. As such citation counts in CiteSeerX are usually less than those in Google Scholar and Microsoft Academic Search who have access to publisher metadata.

=== Usage ===

CiteSeerX has nearly 1 million users worldwide based on unique IP addresses and has millions of hits daily. Annual downloads of document PDFs was nearly 200 million for 2015.

=== Data ===

CiteSeerX data is regularly shared under a Creative Commons BY-NC-SA License with researchers worldwide and has been and is used in many experiments and competitions.

== Other SeerSuite-based search engines ==

The CiteSeer model had been extended to cover academic documents in business with [[SmealSearch]] and in e-business with [[eBizSearch]].  However, these were not maintained by their sponsors. An older version of both of these could be once found at [[BizSeer.IST]] but is no longer in service.

Other Seer-like search and repository systems have been built for chemistry, [[ChemXSeer|Chem<sub>X</sub>Seer]] and for archaeology, ArchSeer. Another had been built for robots.txt file search, [[BotSeer]]. All of these are built on the open source tool [[SeerSuite]], which uses the open source indexer [[Lucene]].

== See also ==

* [[arXiv]]
* [[Google Scholar]]
* [[Microsoft Academic Search]]
* [[The Collection of Computer Science Bibliographies]]
* [[DBLP]] (Digital Bibliography & Library Project)
* [[List of academic databases and search engines]]
* [[Arnetminer]]
* [[Disciplinary repository]]
* [[RePEc]]

== References ==
{{Reflist|2}}

== Further reading ==

* {{cite journal|title=CiteSeer: an automatic citation indexing system|year=1998|citeseerx=10.1.1.30.6847|doi=10.1145/276675.276685|last1=Giles|first1=C. Lee|last2=Bollacker|first2=Kurt D.|last3=Lawrence|first3=Steve|isbn=0-89791-965-3 |journal=Proceedings of the third ACM conference on Digital libraries |pages= 89–98 }}

== External links ==
* [http://citeseerx.ist.psu.edu Official website of CiteSeer<sup>''X''</sup>]

{{DEFAULTSORT:Citeseer}}
[[Category:Bibliographic databases in computer science]]
[[Category:Eprint archives]]
[[Category:Internet search engines]]
[[Category:Library 2.0]]
[[Category:Online databases]]
[[Category:Open-access archives]]
[[Category:Pennsylvania State University]]
[[Category:Scholarly search services]]