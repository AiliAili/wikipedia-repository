{{Infobox person
| name        = Bartolomeo Bulgarini
| image       = <!-- just the filename, without the File: or Image: prefix or enclosing [[brackets]] -->
| alt         = 
| caption     = 
| birth_name  = 
| birth_date  = 1300-1310<!-- {{Birth date and age|YYYY|MM|DD}} or {{Birth-date and age|Month DD, YYYY}} -->
| birth_place = 
| death_date  = 1378 <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|Month DD, YYYY|Month DD, YYYY}} (death date then birth date) -->
| death_place = 
| nationality = [[Italians|Italian]]
| other_names = 
| known_for   = 
| occupation  = [[Painting|Painter]]
}}'''Bartolomeo Bulgarini''' (1300-1310 – 1378), also known as Bulgarino or Bologhini,<ref name="BoskovitzMiklos" /> was an Italian painter of the [[Trecento period]] in [[Siena]] both before and after the [[Black Death]].<ref name="Dobrynin"/>

==Early life==
Born into a noble family with several members being elected into [[Siena#History|the Commune]], Siena’s central governing body, several times.<ref name="SteinhoffMorrisonJudith" /> He is firmly in the [[Sienese school]] of painting using a [[byzantine]]-esque figuration and traditional [[gold leaf]] aesthetic of Sienese painting. With his contemporaries, [[Simone Martini]], [[Pietro Lorenzetti]] and [[Ambrogio Lorenzetti]] and others he is part of the generations following [[Duccio]]. He was the only painter of his generation to be mentioned by [[Giorgio Vasari]].<ref name="SteinhoffMorrisonJudith" />

==History==
[[File:Ugilino Lorenzetti Ste Cathérine.gif|thumb|Ugilino Lorenzetti Ste Cathérine]]
Bulgarini’s oeuvre has seen much controversy in its reconstructions with works formerly attributed to Ugolino Lorenzetti, a composite name constructed by [[Bernard Berenson]] referencing the stylistic similarities to [[Ugolino Da Niero]] and Pietro Lorenzetti<ref name="Dobrynin"/> which he attached to a small body of nine paintings believed, to all be by the same unknown [[Sienese]] artist. The paintings were grouped on a similar visual fluency, style and formula demonstrating a distinctly “Ugolini-esque” aesthetic.<ref name=jstor /> However, of those nine paintings six were later attributed to "The master of the [[Ovile Madonna]]" by Ernst Dewald on the grounds of a perceived fundamental stylistic difference he claimed separated the paintings.<ref name="jstor" /> It wasn’t until Millard Meiss made the argument that the works attributed to both "The master of the Ovile Madonna" and "Ugolino Lorenzetti" might actually be by the same artist, Bulgarini.<ref name="jstor" /> Much of the difficulty in constructing his [[Work of art|oeuvre]] is due to the lack of documentation needed to establish the Painters authorship and timeline of his work. The only known autographed work by Bulgarini is the [[Victor of Marseilles|St. Victor]] [[altarpiece]] in [[Siena Cathedral]].<ref name="SteinhoffMorrisonJudith" /> It was not until much later when several documents were found and the St Victor Altarpiece was reconstructed that we are able to have a clearer picture of his artistic career.<ref name=SteinhoffMorrisonJudith />

==The St. Victor Altarpiece==

This altarpiece (1348–1350),<ref>Steinhoff-Morrison, Judith “Bartolomeo Bulgarini and Sienese Painting of the Mid-Fourteenth century” PhD dissertation, Princeton University, 1990.</ref> found in the [[Siena Cathedral]], was one of four altarpieces commissioned by the Commune, that depict the four patrons saints of the city. The other three altarpieces: ''[[St Ansanus]]'' by [[Simone Martini]], ''St Crescentious'' by Ambrogio Lorenzetti and ''St Savinus'' by Pietro Lorenzetti; were identified earlier because the panels were signed and dated, and because of 15th century inventories from the cathedral.<ref name="jstor_a" /> However, none of these sources were consistent in mentioning the St Victor Altarpiece. It wasn’t until the late 16th century that previously lost inventories, made by Guigurta Tommasi, named Bulgarini as the painter of the St Victor altarpiece.<ref name="jstor_a" />

==Career==

The earliest mention of Bulgarini is in 1338 for a payment made for a Biccherna book cover; books containing the financial transactions for the Commune starting in the thirteenth century until the fifteenth century.  In 1341 and 1342 he was commissioned for two more covers for consecutive seasons.<ref name="SteinhoffMorrisonJudith" />  His career, starting roughly in the 1330s, lasted into the 1370s.<ref name="Dobrynin"/>  His last documented work supposedly signed and dated 1373, which is now lost was a panel painting for the hospital of [[santa Maria della Scala|Santa Maria Della Scala]] in Siena.<ref name="BoskovitzMiklos" /> Though he was active up until his death in 1378.

==The ''Assumption of the Virgin and Doubting Thomas'' Altarpiece==

[[File:Bartolomeo bulgarini, madonna assunta in cielo da un coro d'angeli, da s.m. della scala.JPG|thumb|130px|''Assumption of Virgin'' altarpiece in Santa Maria della Scala church]]
The ''Assumption of the Virgin with Doubting Thomas'' (early 1360s)<ref name=SteinhoffMorrisonJudith />  is a large panel painted by Bulgarini. It was part of an altarpiece for the chapel which housed a group of important relics acquired by the [[Santa Maria della Scala, Siena|Santa Maria della Scala]] Hospital from [[Constantinople]], which included the [[Girdle of Thomas|Virgin’s belt or girdle]] that she cast down to [[Thomas the Apostle|Thomas]].<ref name=AFaithfulSea/> The importance of the relics is reflected in the composition of the panel painted by Bulgarini. Bulgarini’s somewhat unusual portrayal of Thomas with his back to the viewer differs from other iconography of the period, perhaps representing the position of the parishioners and Sienese officiants in worship of the virgin and the recently acquired relics in the newly constructed chapel.<ref name=AFaithfulSea/>

==Works==
Works attributed to Bulgarini are found at the [[Isabella Stewart Gardner Art Museum]] in Boston; the [[Fogg Art Museum]] in Cambridge; the [[Städel]] Art Museum in Frankfurt (''Blinding of St Victor''); and 
the [[Wallraf Richartz Art Museum]] in Cologne (''Enthroned Madonna and Child'').

{|
| [[File:Bartolomeo Bulgarini. The Berenson Polyptych. 1340s. Villa I Tatti. Florence..jpg|thumb|upright|300px|''Berenson Polyptych'' (1340s) [[Villa I Tatti]], Florence.]]
| [[File:Bartolomeo Bulgarini. The-madonna-and-child-with-saints. about 1335. Siena, Pinacoteca Nazionale.jpg|thumb|upright|250px|''Madonna and child and Saints'' (c. 1335) [[Pinacoteca Nazionale, Siena]]]]
|[[File:Bartolomeo Bulgarini - Madonna van de nederigheid.jpg|thumb|upright|100px|''Madonna van de nederigheid'']]
|}

==References==
{{Reflist|refs=

<ref name="BoskovitzMiklos">{{cite book|author1=Miklós Boskovits|author2=Serena Padovani|title=Early Italian painting: 1290-1470|url=https://books.google.com/books?id=75jqAAAAMAAJ|accessdate=17 May 2012|year=1990|publisher=Sotheby's Publications|isbn=978-0-85667-381-8}}</ref>

<ref name="jstor">{{cite journal | title = Ugolino Lorenzetti | journal = The Art Bulletin | year = 1931 | first = Millard | last = Meiss | volume = 13 | issue = 3 | pages = 376–397| id = | jstor = 3050804 | url = http://www.jstor.org/stable/3050804 | accessdate = 2012-05-17 | doi = 10.2307/3050804}}</ref>

<ref name="jstor_a">{{cite journal | title = The St. Victor Altarpiece in Siena Cathedral: A Reconstruction | journal = The Art Bulletin | year = 1986 | first = Elizabeth H. | last = Beatson |author2=Norman E. Muller|author3=Judith B. Steinhoff | volume = 68 | issue = 4 | pages = 610–631| id = | jstor = 3051044 | url = http://www.jstor.org/stable/3051044 | accessdate = 2012-05-17 | doi = 10.2307/3051044}}</ref>

<ref name="SteinhoffMorrisonJudith">{{cite book|author=Judith Steinhoff-Morrison|title=Bartolomeo Bulgarini and Sienese painting of the mid-fourteenth century|url=https://books.google.com/books?id=TmjHYgEACAAJ|accessdate=17 May 2012|year=1990|publisher=Princeton University}}</ref>

<ref name="Dobrynin">{{cite web|last=Dobrynin|first=Laura|title=Bulgarini, Saint Francis, and the Beginning of a Tradition|url=http://www.ohiolink.edu/etd/view.cgi?ohiou1149520563|work=MA Thesis|publisher=OhIo University|year=2006}}</ref>

<ref name="AFaithfulSea">{{cite book|title=A Faithful Sea: The ReligiSous Cultures of The Mediterranean, 1200-1700|year=2007|publisher=Oneworld|location=Oxford|isbn=978-1851684960|author=McClannan, Anne|authorlink=Bulgarini’s Assumption with Doubting Thomas: Art, Trade, and Faith in Post-Plague Siena|editor=Fleming, K.E. |editor2=Adnan A. Husain}}</ref> 
}}

==Further reading==
* Boskovitz, Miklos, ed. The Alana Collection: Italian Paintings from the 13th to 15th century. Firenze: Edizioni Polistampa, 2009.
* Meiss, Millard. "Ugolino Lorenzetti". The Art Bulletin, Vol. 13, No. 3 (1931), pp.&nbsp;376–397. Accessed: 06/04/2012 18:05. http://www.jstor.org/stable/3050804
* McClannan, Anne. “Bulgarini’s Assumption with Doubting Thomas: Art, Trade, and Faith in Post-Plague Siena”. Fleming, K.E., Adnan A. Husain ed. A Faithful Sea: The Religious Cultures of The Mediterranean, 1200-1700. Oxford: Oneworld, 2007. ISBN 978-1851684960
*{{cite book |author=Pope-Hennessy, John |author-link=John Pope-Hennessy |author2=Kanter, Laurence B. |last-author-amp=yes| title= The Robert Lehman Collection I, Italian Paintings |url=http://libmma.contentdm.oclc.org/cdm/compoundobject/collection/p15324coll10/id/190122/rec/1 | location=New York, Princeton | publisher=The Metropolitan Museum of Art in association with Princeton University Press | year=1987| isbn=0870994794}} (see index; plate 7)
*Steinhoff, Judith B. 2007. Sienese painting after the Black Death: artistic pluralism, politics, and the new art market. Cambridge: Cambridge University Press.

{{Authority control}}

{{DEFAULTSORT:Bulgarini, Bartolomeo}}
[[Category:1378 deaths]]
[[Category:14th-century Italian painters]]
[[Category:Italian male painters]]
[[Category:Sienese painters]]
[[Category:1300 births]]