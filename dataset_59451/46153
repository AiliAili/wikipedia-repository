{{Infobox building
| name                = Amdavad ni Gufa
| native_name         = 
| native_name_lang    = 
| former_names        = Husain-Doshi ni Gufa
| alternate_names     = 
| status              = 
| image               = Amdavad ni gufa.jpg
| image_alt           = Whole structure from outside
| image_size          = 
| caption             = 
| map_type            = 
| map_alt             = 
| map_caption         = 
| altitude            = 
| building_type       = 
| architectural_style = [[Modern architecture|Modern]], [[blobitecture]], [[Novelty architecture|novelty]]
| structural_system   = 
| cost                = 
| ren_cost            = 
| client              = [[M. F. Husain]]
| owner               = 
| current_tenants     = 
| landlord            = 
| location            = [[Ahmedabad]]
| address             = Lalbhai Dalpatbhai Campus, near [[CEPT University]], opp. [[Gujarat University]], University Road
| location_town       = [[Ahmedabad]], [[Gujarat]]
| location_country    = [[India]]
| iso_region          = 
| coordinates         = {{coord|23|2|10|N|72|32|58|E|display=inline, title}}
| groundbreaking_date = 
| start_date          = 1992
| completion_date     = 1995
| opened_date         = 
| inauguration_date   = 
| renovation_date     = 
| height              = 
| architectural       = 
| tip                 = 
| antenna_spire       = 
| roof                = 
| top_floor           = 
| observatory         = 
| other_dimensions    = 
| floor_count         = 
| floor_area          = 
| seating_type        = 
| seating_capacity    = 
| elevator_count      = 
| architect           = [[B. V. Doshi]]
| architecture_firm   = Vāstu Shilpā Consultants
| structural_engineer = 
| services_engineer   = 
| civil_engineer      = 
| other_designers     = M. F. Husain
| quantity_surveyor   = 
| main_contractor     = 
| awards              = 
| designations        = 
| ren_architect       = 
| ren_firm            = 
| ren_str_engineer    = 
| ren_serv_engineer   = 
| ren_civ_engineer    = 
| ren_oth_designers   = 
| ren_qty_surveyor    = 
| ren_awards          =
| parking             =
| url                 = 
| embedded            = 
| references          = 
}}

'''Amdavad ni Gufa''' ({{audio|Amdavad ni Gufa pronunciation.ogg|<small>pronunciation</small>|help=yes}}) is an underground [[Art museum|art gallery]] in [[Ahmedabad]], India. Designed by the architect [[Balkrishna Vithaldas Doshi]], it exhibits works of the Indian artist [[Maqbool Fida Husain]]. The gallery represents a unique juxtaposition of architecture and art.<ref name=jt>{{cite book |last=Lang |first=Jon T. |title=Concise History Of Modern Indian Architecture |publisher=[[Orient Blackswan]] |isbn=8178240173 |url=https://books.google.com/books?id=gxyGbhlKQXQC&pg=PA163&dq=amdavad+ni+gufa#v=onepage&q=amdavad%20ni%20gufa&f=false |accessdate=6 December 2012 |page=164 |year=2002}}</ref> The cave-like underground structure has a roof made of multiple interconnected domes, covered with a [[mosaic]] of tiles.<ref name=gt/> On the inside, irregular tree-like columns support the domes. It was earlier known as '''Husain-Doshi ni Gufa'''.<ref name=gt>{{cite web|title=Hussain Doshi Gufa|url=http://www.gujarattourism.com/showpage.aspx?contentid=141&webpartid=949|work=|publisher=Gujarat Tourism|accessdate=6 December 2012}}</ref>

There are facilities for special painting exhibitions and for projecting films. Gardens and a [[café]] are located above ground.<ref name="gt" />

==Etymology==
The gallery is called ''gufa'' ("cave" in [[Gujarati language|Gujarati]]) because of its resemblance to a [[cave]].<ref name="its">{{cite news | url=http://indiatoday.intoday.in/story/art-struck/1/144528.html | title=Art struck | newspaper=[[India Today]] | date=14 July 2011 | accessdate=6 December 2012 | last=Mulchandani | first=Anil }}</ref> It was known earlier as Husain-Doshi ni Gufa, after its architect, B.V. Doshi, and the artist, [[M.F. Husain]]. Later it was renamed after the city of [[Ahmedabad]], known locally as ''Amdavad''.<ref name=ang>{{cite book |first1=Maqbul Fida |last1=Husain |authorlink1=M. F. Husain |first2=Balkrishna V. |last2=Doshi |authorlink2=B. V. Doshi |title=Amdavad-ni-gufa |publisher=Vāstu Shilpā Foundation |url=https://books.google.com/books?id=ekwJywAACAAJ&dq=amdavad+ni+gufa |accessdate=6 December 2012 |year=2008}}</ref>

==Development==
[[File:Junagah - Girnar, Gujarat - India (3418492399).jpg|thumb|left|The [[mosaic]] tiles on the roofs of the [[Girnar Jain temples|Jain temples at Girnar]]]]
The structure's [[contemporary architecture]] draws on ancient and natural themes. The domes are inspired by the shells of [[tortoise]]s and by soap bubbles. The [[mosaic]] tiles on the roof are similar to those found on the roofs of the [[Girnar Jain temples|Jain temples at Girnar]], and the mosaic snake is from [[Hindu mythology]]. The Buddhist caves of [[Ajanta Caves|Ajanta]] and [[Ellora Caves|Ellora]] inspired Doshi to design the interior with circles and ellipses, while Husain's wall paintings are inspired by [[Cave painting|Paleolithic cave art]].<ref name=ds>{{cite book |last=Sharp |first=Dennis |title=20th Century Architecture: A Visual History |publisher=Images Publishing |isbn=1864700858 |url=https://books.google.com/books?id=sQUi8kRKWrMC&pg=PA440&dq=hussain+doshi+gufa#v=onepage&q&f=false |accessdate=6 December 2012 |page=440 |year=2002}}</ref> The interior is divided by tree trunks or columns similar to those found at [[Stonehenge]].<ref name=ang/><ref name=art>{{cite book |first1=Kester |last1=Rattenbury |first2=Rob |last2=Bevan |first3=Kieran |last3=Long |title=Architects Today |publisher=Laurence King Publishing |isbn=1856694925 |url=https://books.google.com/books?id=LmFooNzPaN0C&pg=RA2-PA1986&dq=hussain+doshi+gufa#v=onepage&q=hussain%20doshi%20gufa&f=false |accessdate=6 December 2012 |pages=46–47 |year=2006}}</ref><ref name=jkw>{{cite book |last=Waters |first=John Kevin |title=Blobitecture: Waveform Architecture and Digital Design |publisher=[[Quarto Group|Rockport Publishers]] |isbn=1592530001 |url=https://books.google.com/books?id=sTbb-VZuff0C&pg=PT92&dq=hussain+doshi+gufa |accessdate=6 December 2012 |pages=183 |year=2003}}</ref>

==Construction==
While visiting Ahmedabad, Husain asked his friend Doshi to design a permanent art gallery for the exhibition of his works.<ref name=ang/> Together they planned an underground structure capable of withstanding the area's severe summer heat.<ref name="dna">{{cite news | url=http://www.dnaindia.com/india/report_tracing-mf-husains-footprints-in-ahmedabad_1553439 | title=Tracing MF Husain’s footprints in Ahmedabad | newspaper=[[Daily News and Analysis|DNA]] | date=10 June 2011 | accessdate=6 December 2012 }}</ref><ref name="ia">{{cite web | url=http://www.indian-architects.com/en/projects/30261_amdavad_ni_gufa/16/indexAZ | title=Amdavad ni Gufa | publisher=Indian Architects | accessdate=6 December 2012}}</ref>

[[Computer-aided design|Computer-assisted]] planning facilities were used to resolve the structure's unorthodox design.<ref name=jkw/><ref name=ia/><ref name=uk>{{cite web |title=Hussain Doshi Gufa |url=http://www.ahmedabad.org.uk/museums/hussain-doshi-gufa.html |publisher=ahmedabad.org.uk |accessdate=6 December 2012}}</ref> A simple floor of wire [[mesh]] and [[Mortar (masonry)|mortar]] was used instead of a traditional [[foundation (engineering)|foundation]].<ref name=ds/><ref name=jkw/> All the structure's components are self-supporting, relieving stress by their ubiquitous continuity. [[Ferrocement]], only one inch thick, was used for the undulating walls and domes in order to reduce load.<ref name=jkw/><ref name=ia/> The cave was constructed by unskilled tribal labourers using only hand tools.<ref name=uk/> Broken [[ceramic]] crockery<ref name=ds/> and waste tiles were used to cover the domes' exterior, which bears a transversal mosaic of a snake.<ref name="gt"/><ref name=jkw/><ref name=ia/>

Work was carried out in two phases: the first was the construction of the main cave as an underground art gallery, while the second covered the surrounding structures including the paving, the café, and a separate art gallery for exhibitions.<ref name=ang/>

==Structure==
[[File:Amdavad ni Gufa3.jpg|thumb|Amdavad ni Gufa exterior|400px|right]]
The gallery space is below ground level. A partially hidden staircase leads to a circular door which opens into a cave-like space. Though designed to display paintings, the cave has no straight walls, instead using a continuation of the curved dome structure which extends down to the floor. The domes themselves are supported by irregularly shaped inclined columns, similar to those found in natural caves. They are also said to resemble the trunks of trees.<ref name=jt/> The entire design is made up of circles and ellipses.<ref name=ang/> Light arrives though snouts, creating spots of light on the floor which move around as the day progresses, intended to create a mystic atmosphere.<ref name=ang/><ref name=ds/><ref name=jkw/><ref name=dna/>

==Art==
Husain used the gallery's walls as a canvas, painting on them with bold strokes and bright colours. The artwork depicts human figures and motifs of animals, including his famous [[horse]] figures. He also decorated features such as doors and even [[Air conditioning|air conditioners]]. The figures were designed to resemble ancient cave paintings in a modern environment. Husain also placed a few metal sculptures of human figures between the inclining columns.<ref name=ang/> His largest work, ''Sheshnag'' (the divine serpent), stretches over a length of {{convert|100|ft}}.<ref name=dna/>

==Gallery==
<gallery mode="packed">
Image:AmdavadNiGufa.JPG| Signbord painted by Husain
Image:Amdavad ni Gufa, Ahmedabad.JPG|Exterior
Image:Caves of Ahmedabad.JPG|Secretly guarded door
Image:Ahmedabad caves.JPG|Entrance
Image:Amdavad ni gufa-3.jpg|Entrance door decorated by M.F. Husain
Image:Amdavad ni Gufa1.JPG|Snouts for entry of light
Image:Amdavad ni Gufa2.JPG|Tile mosaics
Image:Statue--zen cafe.jpg|Metal sculpture on display
Image:Zen cafe.JPG|Zen cafe
</gallery>

==References==
{{reflist}}

{{Ahmedabad topics}}
{{Modern architecture|state=autocollapse}}

[[Category:1995 establishments in India]]
[[Category:Art galleries established in 1995]]
[[Category:Buildings and structures by Indian architects]]
[[Category:Buildings and structures in Ahmedabad]]
[[Category:Modernist architecture in India]]
[[Category:Tourist attractions in Ahmedabad]]