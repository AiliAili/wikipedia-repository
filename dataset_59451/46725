[[File:Bronx Library Center.jpg|thumb|right|300px|View from Kingsbridge Road looking northwest]]
The '''Bronx Library Center''', a branch of the [[New York Public Library]], is located at 310 East Kingsbridge Road in [[the Bronx]], [[New York City]], situated just off [[Fordham Road]], and several blocks away from [[Grand Concourse (Bronx)|Grand Concourse]]. 

The library opened in January 2006, replacing the Fordham Branch Library, which had served the Bronx previously. At {{Convert|78000|ft2|m2}}, it serves as the central library for the Bronx, and is the first "[[Green building|green]]" library in [[New York City]] with [[Leadership in Energy and Environmental Design|LEED Silver]] certification; it also has three times as much space as its predecessor. This building cost an estimated $50 million and is characterized by the sloping curve of its roof and the extensive glass [[Curtain wall (architecture)|curtain wall]] on its east [[façade]]. The library serves a diverse and growing population, predominantly of Hispanic and African-American background. 

The building was designed by the New York City-based [[architecture firm]] Dattner Architects, led by architect Richard Dattner.

== History ==
For more than 75 years, the Fordham Library Center at 2556 Bainbridge Avenue, served the [[New York City borough|borough]] before the new center opened in 2006. The library was part of a public library system built in the United States, supported by donations from wealthy businessman [[Andrew Carnegie]]. As part of the [[Carnegie library]] system it followed the philosophy of being accessible and free to all people with an emphasis on access and service to working class Americans. The library opened in 1923 and saw multiple expansions and renovations over the decades to continue serving a rapidly growing community. As immigrants (primarily of Irish, Italian and Jewish heritage) moved into the Bronx from [[Manhattan]], they used the library to learn to read and write English and become acclimated to American culture.<ref name="nytimes">Glenn Collins, "[https://www.nytimes.com/2006/01/16/nyregion/16library.html New Bronx Library Meets Old Need]" (Jan. 16, 2006), ''New York Times''.</ref> Compared to main branch libraries in the other [[outer boroughs]], the Bronx building was significantly smaller taking up only three floors at a total of roughly 27,500 square feet.<ref>Jon Minners, "[http://www.bronxmall.com/norwoodnews/past/61799/news/page5.html Borough Waits (and Waits) for New Main Library]" (June 17-30, 1999), ''Norwood News'', Vol. 12, No. 12.</ref> A renovation in 1956 proved to be the last; it would take nearly 50 years before enough money was raised to build a new, bigger, much needed building. The Fordham Library Center officially closed its doors to the public in November 2005. Two months later the Bronx Library Center opened right around the corner.

[[File:Fordham Library Center.jpg|thumb|left|Former Fordham Library Center, Bainbridge Avenue]]
It was the goal of the New York Public Library and the architects who designed the new library branch in the Bronx to continue the tradition of the accessible library model established by Carnegie. It would remain a welcoming place of learning for a new generation of immigrants primarily from the Caribbean and Latin America with Spanish being the new dominant language.<ref name="nytimes" /> A bigger piece of land was needed to accommodate the new building. Fortunately, one was available around the corner from the old Carnegie library: it had held a [[Consolidated Edison|Con Edison]] building that closed its doors in 1999; the building components were mostly recycled to be reused or sold for scraps to help pay for demolition. 

After a long fund-raising push in the late 1990s, with contributions from [[New York State Legislature]] and allocation of funds by former [[Mayor of New York City|mayor]] [[Rudolph Giuliani]], the New York Public Library acquired the site for the new library in 2001.<ref>Heather Haddon, "[http://www.norwoodnews.org/id=1396&story=bronx-library-center-no-longer-a-fairy-tale/ Bronx Library Center No Longer a Fairy Tale]" (January 26, 2006), ''Norwood News'.</ref>

== Location ==
The library is in the [[Fordham, Bronx|Fordham Heights]] neighborhood of [[the Bronx]], overlooking the shopping strip along Fordham Road and facing the old library on Bainbridge Avenue to the east.  The massive new building is highly visible from East Kingsbridge Road, contrasting drastically with its neighboring buildings, which are primarily low-rise commercial or residential buildings.  The [[Edgar Allan Poe Cottage]], [[Fordham University]], the [[New York Botanical Garden]], and the [[Bronx Zoo]] are all nearby.  It is close to the [[New York City Subway]]'s {{NYCS trains|Concourse local}} at [[Kingsbridge Road (IND Concourse Line)|Kingsbridge Road]] ([[IND Concourse Line]]) and the {{NYCS trains|Jerome}} at [[Kingsbridge Road (IRT Jerome Avenue Line)|Kingsbridge Road]] ([[IRT Jerome Avenue Line]]). The {{NYC bus link|Bx12|Bx12 SBS}} [[New York City Bus]] routes are also nearby.

==Description==
===Floor layout===
Much larger than the previous Fordham Library and other Bronx libraries, the library contains over 300,000 volumes, including the New York Public Library’s central collection of [[Hispanic and Latino Americans|Hispanic/Latino]] and [[Puerto Rican culture|Puerto Rican]] heritage works. 

The building is designed for the digital technology and social/civic functions as well as for books; it contains reading areas, a 150-person [[auditorium]], computer rooms, staff offices, conference rooms, and a public gallery/gathering area, divided among the floors:
*Basement or [[concourse]] level - auditorium and writing center. 
*Ground floor - Teen center and main circulation desk.
*Second floor - Children's floor
*Third floor - Adult circulating materials
*Fourth floor - Reference materials with computer desks
*Partial fifth floor - Career services center and conference room. 

While the public areas are lit by the glazing on the front façade, [[skylight]]s above staff offices in the building's rear provide daylight for library workers. To help keep the library floors open, circulation is pushed to the back of the building as well. The main stair wraps around the elevator and is framed by a translucent glass wall to provide daylight. Patrons are expected to move through the library from back to front; from the stairs, through the stacks, and finally to the lounging and reading areas that are situated along the curtain wall.

The library also offers classes to the community including financial and career services to help people manage their money and improve their resume-writing in the pursuit of jobs.

===Architecture===
The building is steel-framed and uses diverse materials in its construction that contrast with each other. The building is intended to be inviting on the interior and bold on the exterior. The exterior face of the library includes a multi-story glazed glass curtain wall (which stretches four stories and [[cantilever]]s out from the main body of the library to dominate the front façade). Water-treated [[Minnesota]] red [[granite]] panel the rest of the façade and also extend into the interior of the library on the fourth floor towards the terrace. The signature swooping roof is clad in [[aluminum]] paneling on the exterior and [[maple]] veneer on the interior. [[Maple]] softens many of the other surface cladding. The swooping roofline also contributes to visibility and also allows a partial fifth floor for the building while also following setback regulations for the block.

Practicality and durability take precedence in other areas of the library. The main stair at the back of the library has a frosted [[channel glass]] wall, steel handrails, and [[linoleum]] flooring. This floor surface is extended to other areas of the building in contrast with the [[terrazzo]] and carpeting that weather the high foot traffic hardily.<ref name="Fortmeyer">Russell Fortmeyer, "[http://greensource.construction.com/projects/0701_sunshine.asp Let the Sun Shine In: A light-filled library becomes a dynamic meeting place for an underserved community] {{webarchive |url=https://web.archive.org/web/20120527050807/http://greensource.construction.com/projects/0701_sunshine.asp |date=May 27, 2012 }}" (January 2007). ''[[GreenSource Magazine]]''.</ref>

[[File:Bronx Library Center second floor interior.jpg|thumb|View of the second floor children’s library with curtain wall and light shelf]]
The Bronx Library Center is LEED Silver-certified and is the first "green" library in New York City; it includes many [[sustainable architecture]] features. Approximately 40 percent of materials used in the construction of the library were manufactured locally (within 500 miles of the site). The majority of materials produced by the demolition of the Con Edison building on site before the library were recycled to be used in off-site locations. The library itself is composed of roughly 20 percent [[recycled material]]. In addition to using environmentally safe materials, chemical release into the atmosphere is further mitigated by water-cooled [[chiller]]s. The mechanical [[HVAC]] systems help provide [[Efficient energy use|building energy use]] of 20 percent less than code requires.

The energy-efficient lighting systems and the extensive use of [[sunlight]] are perhaps the boldest sustainable features of the library. The glazed curtain wall is composed of glass with a low [[U-value]] for better [[Building insulation|insulating]] properties on the interior of the building. At times there may be too much daylight along the perimeter reading areas along the wall, so there are mechanized [[nylon]] mesh shades to soften the light and [[Architectural light shelf|light shelves]] that bounce some of the strongest light off of the surface and push it deeper into the spaces. [[Photodetector|Photocell]] sensors on the ceilings react to daylight levels and automatically change electric lighting intensities accordingly to save energy used. Overall, 75 percent of the interior spaces meet LEED criteria for the ratio of daylight to illuminated light.<ref name="Fortmeyer"/>

==Gallery==

<gallery>
File:Bronx Library Center Concourse stair.jpg|''Portrait of a Young Reader'', by [[Iñigo Manglano-Ovalle]], flanking the Concourse level stair
File:Bronx Library Center Elevation.jpg|Front/Kingsbridge elevation
File:Bronx Library Center main entrance.jpg|Main entrance off Kingsbridge Road
File:Bronx Library Center Puerto Rican Heritage Collection.jpg|Puerto Rican Heritage Collection room
File:Bronx Library Center interior ceiling.jpg|Fourth floor swooping ceiling view
</gallery>

==References==
===Notes===
{{Reflist}}

===Further reading===
*Richard Dattner & Partners, Architects, Dattner Architects, Mulgrave, Victoria 3170, Australia : Images Pub., 2009, 144 p.
*“Projects-Building Types Study – Libraries – Bronx Library Center” Architectural Record, (May 2006) http://archrecord.construction.com/projects/bts/archives/libraries/06_BronxLib/overview.asp
*“Libraries = Cultural Icons: 2006 Showcase of New and Renovated Facilities,” American Libraries, Vol. 37, No. 4 (Apr. 2006), pp. 28-47
*Sondra Wolfer “New Library Checks in $50M Bx. Center is first “Green” site” New York Daily News (January 18, 2006)  http://www.nydailynews.com/archives/boroughs/library-checks-50m-bx-center-green-site-article-1.641667
*Jake Mooney, “Young Readers Embrace Sanctuary of the Bronx Libraries” City Room, (August 17, 2007) http://cityroom.blogs.nytimes.com/2007/08/17/young-readers-embrace-sanctuary-of-the-bronx-libraries/?scp=9&sq=bronx%20library%20center&st=cse
*Thomas de Monchaux, “Meet Mister Streetscape” The Architects Newspaper, (February 1, 2006) http://archpaper.com/news/articles.asp?id=157

==External links==
{{commonscat|NYPL Bronx Library Center}}
*[http://www.nypl.org/locations/bronx-library-center Profile] from the [[New York City Public Library]] official website

{{coord|40|51|47.03|N|73|53|40.12|W|region:US-NY|display=title}}

[[Category:New York Public Library]]
[[Category:Buildings and structures in the Bronx]]
[[Category:Tourist attractions in the Bronx]]
[[Category:Education in the Bronx]]
[[Category:Fordham, Bronx]]