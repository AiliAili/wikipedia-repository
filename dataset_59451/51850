{{Distinguish2|the medical journal [[Evidence-Based Complementary and Alternative Medicine]] and the blog [[Science-Based Medicine]]}}
{{Infobox journal
| title = Journal of Evidence-Based Complementary & Alternative Medicine
| cover = Journal of Evidence-Based Complementary & Alternative Medicine.jpg
| editor =Bruce Buehler
| discipline = [[Alternative medicine]]
| former_names = 
| abbreviation = J. Evid. Based Complement. Altern. Med.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1995-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = https://sagepub.com/en-gb/eur/journal-of-evidence-based-complementary-alternative-medicine/journal201646#description
| link1 = http://chp.sagepub.com/content/current
| link1-name = Online access
| link2 = http://chp.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 2156-5872
| eISSN = 2156-5899
| OCLC = 649083104
| LCCN = 2010203263
}}
The '''''Journal of Evidence-Based Complementary & Alternative Medicine''''' is a quarterly [[peer-reviewed]] [[medical journal]] that covers research in the field of [[alternative medicine]]. The [[editor-in-chief]] is Bruce Buehler ([[University of Nebraska Medical Center]]). The journal was established in 1995 and is published by [[SAGE Publications]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
*[[CINAHL]]<ref name=CINAHL>{{cite web |url=https://www.ebscohost.com/nursing/products/cinahl-databases/cinahl-complete |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2016-11-12}}</ref>
*[[EBSCO Information Services|EBSCO databases]]
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101556804 |title=Journal of Evidence-Based Complementary & Alternative Medicine |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-11-12}}</ref>
*[[InfoTrac]]
*[[PsycINFO]]<ref>{{cite web |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2016-11-12}}</ref>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-11-12}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|https://sagepub.com/en-gb/eur/journal-of-evidence-based-complementary-alternative-medicine/journal201646#description}}

{{DEFAULTSORT:Journal of Evidence-Based Complementary and Alternative Medicine}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Alternative and traditional medicine journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1995]]
[[Category:Evidence-based medicine]]