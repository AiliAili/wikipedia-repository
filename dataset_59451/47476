{{Infobox person
| name        = Derrick Charles Fung
| image       =
| alt         = 
| caption     = 
| birth_name  = Derrick Charles Fung
| birth_date  = {{birth date and age|1987|10|19}}
| birth_place = Toronto, Ontario, Canada
| death_date  = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|Month DD, YYYY|Month DD, YYYY}} (death date then birth date) -->
| death_place = 
|nationality = Canadian
|residence = [[Image:Flag of Canada.svg|20px]] [[Canada]]
| alma_mater = University of Toronto - Scarborough Campus
| other_names = 
| occupation  = CEO of Drop
Former CEO of Tunezy
| prizes = Forbes 30 Under 30, 2014
}}

'''Derrick Fung''' (born October 19, 1987) is an entrepreneur, former investment banker and contributing writer for Forbes.<ref>{{cite web|title=How A Business Is Like An Airplane|url=http://www.forbes.com/sites/quora/2013/10/01/how-a-business-is-like-an-airplane/|publisher=Forbes}}</ref> He is currently the CEO of Drop. He is best known as the founder and CEO of Tunezy, a music company founded in Toronto, Ontario and now based in New York City. Tunezy was acquired in 2013 by [[SFX Entertainment]].<ref>{{cite web|title=SFX Beefing Up Digital Capabilities, Plans to Buy Tunezy, Fame House and Arc90|url=http://www.billboard.com/biz/articles/news/digital-and-mobile/5770373/sfx-beefing-up-digital-capabilities-plans-to-buy-tunezy|publisher=Billboard}}</ref> In 2014, Derrick was listed on the Forbes Top 30 under 30 list in Music.<ref>{{cite web|title=2014 30 under 30: Music|url=http://www.forbes.com/pictures/eeel45ehgfg/derrick-fung-26/|publisher=Forbes}}</ref>

== Early Life and Education ==

Derrick grew up in North York, Ontario, Canada. He attended [[York Mills Collegiate Institute|York Mills Collegiate]] for high school. During high school, he created a site called QualitySheetMusic.com, which was the largest online sheet music community at the time with over 50,000 registered users.<ref>{{cite web|title=Get to know a Toronto startup: Tunezy|url=http://www.blogto.com/tech/2012/04/get_to_know_a_toronto_startup_tunezy/|publisher=BlogTO}}</ref> After the site gained notoriety, he sold the website to U.S. investors in 2005.

After graduating from high school in 2005, Derrick attended the [[University of Toronto Scarborough]] where he was enrolled in the [[Bachelor of Business Administration]] (BBA) degree.<ref>{{cite web|title=Management student selected for prestigious New York internship|url=http://ose.utsc.utoronto.ca/ose/story.php?id=1998}}</ref> He was a teaching assistant for courses in Macroeconomics and Intermediate Finance. As part of the undergraduate program, he interned at [[Microsoft]] in Marketing, [[BNP Paribas]] in Equity Derivative Sales and on the trading floor of [[Merrill Lynch]] in 2009 as the first class after the merger with Bank of America. Fung was awarded the Gordon Cressy Award in 2010 for his leadership potential.<ref>{{cite news|title=2010 Gordon Cressy Student Leadership Awards Ceremony|url=http://ose.utsc.utoronto.ca/ose/story.php?id=2018|newspaper=University of Toronto}}</ref>

In January 2010, Fung worked at the [[Clinton Foundation]] in New York City as an Intern in the [[Clinton Global Initiative]] (CGI).<ref>{{cite web|title=From Big Man on Campus to the Big Apple|url=http://careerinsiderbusiness.ca/profiles-interviews/from-big-man-on-campus-to-the-big-apple/}}</ref> He was one of four Canadians chosen that term to assist with the foundation. He was part of the Membership Department, which was responsible for inviting and managing the delegates for the conference.

Upon graduation from the University of Toronto, he started working at [[CIBC World Markets]] on the trading floor where he traded Foreign Exchange. During his time at CIBC, Fung concurrently started building Tunezy part-time with former classmate <ref>{{cite web|title=Tunezy the Social Record Label Debuts with Beta Test|url=http://www.techvibes.com/blog/tunezy-the-social-record-label-debuts-with-beta-test-2012-05-31}}</ref> and co-founder, Brandon Chu, who was in finance at Kraft Foods.<ref>{{cite web|title=Toronto Startup: Tunezy A CrowdFunded Record Label Of Sorts INTERVIEW|url=http://seriousstartups.com/2012/10/06/toronto-startup-tunezy-a-crowdfunded-record-label-of-sorts-interview/}}</ref> On Friday, April 22, 2011, one of his former bosses encouraged him to leave to work on Tunezy full-time and provided him with the initial capital to start the business.<ref>{{cite news|title=Cash from former boss helps launch music company|url=http://www.theglobeandmail.com/report-on-business/small-business/starting-out/cash-from-former-boss-helps-launch-music-company/article12057597/|newspaper=The Globe & Mail|date=May 23, 2013}}</ref> Fung officially left CIBC to start Tunezy on December 22, 2011, with Brandon following suit a few months later.<ref>{{cite web|title=Tunezy the Social Record Label Debuts with Beta Test|url=http://www.techvibes.com/blog/tunezy-the-social-record-label-debuts-with-beta-test-2012-05-31}}</ref>

== Tunezy ==

<!-- Deleted image removed: [[File:Derrick Billboard.jpg|thumb|left|Derrick Fung pitching Tunezy at the 2012 Billboard FutureSound Showcase in San Francisco]] -->
Tunezy was officially incorporated in November 2011 and operations began in January 2012, with Derrick as CEO and Brandon as Head of Product.<ref>{{cite web|title=Tunezy's New Fan Wishlists Connect Musicians And Fans Through Tangible Experiences|url=http://www.hypebot.com/hypebot/2013/02/tunezy-connects-musicians-and-fans-through-tangible-experiences-via-new-fan-wishlists.html}}</ref> The first engineer at the company was Fung's brother, Darren Fung. The team started working in Fung's living room in Scarborough, Ontario{{citation needed|date=October 2014}}. The initial concept of the platform was that it would become an online "Social Record Label". Tunezy quickly caught the attention of institutional investors and the company raised a seed round of financing from Intertainment Media (TSXV: INT) in early 2012.<ref>{{cite web|title=Intertainment Media Announces Investment in Independent Music Platform Tunezy, Inc.|url=https://www.bloomberg.com/article/2012-04-10/aGuOY12DNk2o.html|publisher=Bloomberg}}</ref>

Tunezy came first at the Entrepreneurship pitch in March 2012 against 32 other companies.<ref>[http://nspire.org/nbtc/ National Business & Technology Conference (NBTC)]</ref> The company shifted its vision and presented its new direction at the ''[[Billboard (magazine)|Billboard]]'' FutureSound Conference in San Francisco in November 2012, and was placed first in the Billboard Innovators Showcase.<ref>{{cite news|title=Tunezy Wins Billboard's FutureSound Innovators Showcase|url=http://www.billboard.com/biz/articles/news/1083000/tunezy-wins-billboards-futuresound-innovators-showcase|newspaper=Billboard|date=November 17, 2012}}</ref> In October 2013, it was announced that Tunezy would be acquired by [[SFX Entertainment]].<ref>{{cite web|title=SFX Beefing Up Digital Capabilities, Plans to Buy Tunezy, Fame House and Arc90|url=http://www.billboard.com/biz/articles/news/digital-and-mobile/5770373/sfx-beefing-up-digital-capabilities-plans-to-buy-tunezy|publisher=Billboard}}</ref> The Tunezy platform was rolled into SFX Entertainment after the acquisition. Fung was made a senior member of the Global Business Development team at SFX Entertainment where he focused on the festival [[Tomorrowland (festival)|Tomorrowland]].<ref>{{cite web|url=https://www.linkedin.com/in/derrickfung|website=LinkedIn}}</ref> Fung was the youngest Vice President at the company at age 26. Fung was a co-chair for [[Canadian Music Week]] in 2015.<ref>{{cite web|title=Co-Chair Committee Announced for CMW 2015|url=http://cmw.net/media/press-room/co-chair-committee-announced-for-cmw-2015/}}</ref>

== Drop ==

Fung launched Drop in 2016 after raising $1 million in venture capital from [[ff Venture Capital]], White Star Capital, Rothenberg Ventures, and HIGHLINE.<ref>{{cite web|title=WITH $1 MILLION IN FUNDING, TORONTO-BASED DROP HOPES TO DOMINATE NORTH AMERICA’S LOYALTY MARKET|url=http://betakit.com/with-1-million-in-funding-toronto-based-drop-hopes-to-dominate-north-americas-loyalty-market/}}</ref> He currently serves as the company's CEO.

== Awards ==

*Inducted into the 2014 [[Forbes]] Top 30 under 30 for Music <ref>{{cite web|title=Forbes Top 30 Under 30 - Music|url=http://www.forbes.com/pictures/eeel45ehgfg/derrick-fung-26/|publisher=Forbes}}</ref>
*Named a ''Top 20 under 30 Entrepreneur'' by ''[[Profit (magazine)|Profit]]''<ref>{{cite news|title=Profile: Derrick Fung, 2012 FuEL Award Winner|url=http://www.profitguide.com/news/profile-derrick-fung-2012-fuel-award-winner-45297|newspaper=Profit Magazine}}</ref>

== References ==

{{reflist|2}}

{{DEFAULTSORT:Fung, Derrick}}
[[Category:1987 births]]
[[Category:Living people]]
[[Category:University of Toronto alumni]]