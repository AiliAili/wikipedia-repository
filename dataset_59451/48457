{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
'''Henry Stephens''', MRCS (March 1796 – 15 September 1864) was a doctor, surgeon, chemist, writer, poet, inventor and entrepreneur.  At [[medical school]] in London he was a friend of, and shared rooms with, poet [[John Keats]], later wrote treatises on [[hernia]] and [[cholera]], and conducted experiments to improve writing fluids and wood stains.  In 1832 he invented an indelible blue-black writing fluid, patented it in 1837 and later formed the Stephens' Ink company which grew into a worldwide brand with a famous inkblot image.

==Early life==

Henry Stephens was born in [[Holborn]], London, the second son of Joseph Stephens (1771–1820) and his wife Catherine (1763–1843), née Farey, but along with his elder brother John was soon moved to more rural [[Hertfordshire]].<ref name="one">The Life of Henry Stephens MRCS, Martha Walsh (daughter), published 1925</ref> The family lived briefly in [[Hatfield, Hertfordshire|Hatfield]] where sisters Frances (1798–1860) and Catherine (1800–1855) were born.  Around 1801 they moved to [[Redbourn]] near [[St Albans]] where a fifth child, Josiah (1804–1865) was born.  Joseph Stephens became the innkeeper at ''The Bull'', the principal inn and busy staging-post in Redbourn High Street on a main stagecoach route between London and the north.<ref name="two">[http://www.redbourn.org.uk/Redbourn/HenryStephens HENRY STEPHENS (1796–1864)], Redbourne Village website, sourced from ''John Keats, Henry Stephens and George Wilson Mackereth: The Unparallel Lives of Three Medical Students'' by W.S.Pierpoint (October 2010) ISBN 978-0-9567127-0-7</ref>

In 1811 Stephens was apprenticed to a local doctor in [[Markyate]] three miles north of Redbourn and in 1815 enrolled as a pupil in the united teaching school of [[Guy's Hospital|Guy's]] and [[St Thomas' Hospital]] in London.<ref name="one" /> He shared lodgings at 28 St Thomas Street, [[Southwark]], with George Wilson Mackereth, whose daughter Stephens' son, [[Henry Charles Stephens|Henry Charles]], later married, and with [[John Keats]], who was to become famous as a poet, and who died in Rome in 1821.  It is on record that in 1816 Stephens helped Keats compose the line from [[Endymion (poem)|Endymion]] book 1, "A thing of beauty is a joy forever ..."<ref name="one" /><ref name="two" />

On 14 March 2002, as part of the 'Re-weaving Rainbows' event of [[National Science Week]] 2002, [[Poet Laureate]] Sir [[Andrew Motion]] unveiled a blue plaque on the front wall of 28 St Thomas Street to commemorate the sharing of lodgings there by Keats and Stephens while they were medical students at Guy's and St Thomas' Hospital in 1815–16.

==Redbourn, 1817–1828==

After qualifying, Stephens returned to Redbourn where he developed a medical practice for ten years, dealing with all manner of medical problems including agricultural accidents and veterinary matters.  He was a friend of the distinguished surgeon [[Astley Cooper|Sir Astley Cooper]], physician to King William IV, who encouraged Stephens to return to London where his medical abilities could be put to greater use.<ref name="two" />

==Tourist and Travel Writer==

In November 1827 Stephens embarked upon what he called his "Excursion to the North", a journey begun by [[stagecoach]] through [[Northampton]] and [[Leicestershire]] to [[Derby]] and [[Staffordshire]].<ref name="three">Journal of an Excursion into the North, November 1827, Henry Stephens, 1827; ed. Jennifer Stephens; published by The Stephens Collection, 1997.</ref> He visited [[Chatsworth House]] and Haddon Hall and walked many miles in the [[Matlock, Derbyshire|Matlock]] area.  Stephens continued to Manchester and Liverpool, visited the [[Northwich]] salt mines, then continued via [[Chester]] to Eaton Hall where he toured Earl Grosvenor's estate on horseback.

Passing through [[Wrexham]], he reached [[Shrewsbury]] where he joined the famous Holyhead mail coach to Birmingham, then on via [[Worcester]], [[Tewkesbury]] and [[Gloucester]] to [[Bristol]].  Here he enjoyed the Clifton Downs but had difficulty crossing the muddy and tidal [[River Avon (Bristol)|River Avon]] below the Downs by ferry, Brunel's famous [[Clifton Suspension Bridge]] not yet having been built.  In [[Bath, Somerset|Bath]] Stephens paid a visit to the famous stone quarries and [[Grand Pump Room, Bath|Pump Room]], and finally returned to London by coach via [[Marlborough, Wiltshire|Marlborough]], the [[Savernake Forest]], [[Henley-on-Thames|Henley]] and [[Windsor, Berkshire|Windsor]].<ref name="three" />

His perambulations, which took over a month, featured in a "Journal" of 14,000 words written on his return, a revised monograph of which was edited in 1997 by his great-granddaughter Jennifer Stephens, and published by The Stephens Collection in [[Finchley]].<ref name="three" /> The doctor makes acute observations on his travels through the daily life and society of England in the reign of [[George IV of the United Kingdom|George IV]], before the advent of the railways.

==Southwark (1829–1843) and first marriage==

The doctor established a practice at 54 [[Stamford Street]], near [[Blackfriars Bridge]] and not far from Guy's and St Thomas' Hospitals.  In September 1829, at Trinity Church, Marylebone, Stephens married Hannah Woodbridge "from Christchurch in the County of [[Surrey]]"; nothing is known of her background or how the couple met.  In December 1830 a daughter, Harriot, was born, but died almost exactly a year later from consumption ([[tuberculosis]]).  Worse was to follow in April 1832 when Stephens' wife Hannah also died from consumption; both mother and child were buried in the graveyard at St Mary's, Redbourn.<ref name="two" />

Despite these setbacks Stephens' medical practice flourished, and although a series of [[cholera]] outbreaks kept the doctor very busy, he found time to become an expert on [[hernia]].  His published work on this subject – ''A Treatise on Obstructed and Inflamed Hernia: and on Mechanical Obstructions of the Bowels Internally'' – appeared in 1831, the same year that his daughter died.  An original copy of this work, presented to a Dr Hope "...with the author's respectful compliments", is held by the [[Wellcome Library]] in London.<ref name="four">[Wellcome Library, 183 Euston Road, London NW1 2BE "http://encore.wellcome.ac.uk/iii/encore/search/C__SStephens%20Hernia"]</ref>

During the 1830s Stephens was very active as a member of the [[Medical Society of London]].  He also became an expert on [[cholera]] which was always a problem in the city until [[John Snow (physician)|John Snow]] (1813–1858), Vice-President and later President of the Society, brought the outbreaks to an end by his celebrated removal of the handle from the pump of the sewage-contaminated well in [[Broadwick Street|Broad Street]], [[Soho]], in 1854.

==Inventor==

From around 1830 Stephens had been experimenting with chemicals and [[ink]]-making in the basement cellar at 54 Stamford Street, perhaps as a sort of hobby but more likely due to dissatisfaction with the poor quality of the writing materials available at that time.

Despite pressure from friends and relations to give up commerce and continue his career as a medical professional, Stephens continued experimenting with his inks and stains.  As the population slowly became more literate, writing materials were much in demand, and Stephens took over a nearby stable and yard, where he kept his doctor's horse and carriage, for his factory, employing a foreman and men to handle the bottling.

In 1832 Stephens' Ink was registered as a company name.  Stephens soon had an eye on exporting his products; he travelled to Paris and appointed an agent to handle his business in France, and also appointed an agent in New York.<ref name="one" /> Patents were granted in 1837.  The introduction of the [[Penny Post]] in 1840 helped to accelerate the demand for writing materials.

Stephens' invention is acclaimed as superior to others. A long letter published in ''Mechanics' Magazine, Register, Journal and Gazette'' dated 2 July 1836 about ink and ink stands contained the following paragraph:

''"Mr Stephens' writing fluid is remarkable for the ease with which it flows from and follows every stroke of the pen, for its bright and distinct blue colour when first written with, and for the superior blackness which it afterwards invariably acquires."''

signed  Wm. Baddeley, London 9 June 1836

Stephens' ink is renowned for its non-fading ability and to this day Stephens' indelible Registrar's Ink is one of the official inks that Registrars of Births, Marriages and Deaths throughout the United Kingdom are required to use for their register entries.<ref>[http://www.registrarsink.co.uk/about.html Registrars' Ink]</ref>

==Second marriage (1840)==

In 1840, eight years after the death of Hannah, Stephens remarried.  His new wife was Anne O'Reilly, a 26-year-old spinster from Holborn; the wedding took place on 3 May at St Andrew's church, Holborn.<ref name="one" /> After a honeymoon in Brussels, the couple settled in their new home in York Road, [[Lambeth]].  Two children were born in the years that followed, [[Henry Charles Stephens|Henry Charles]] in 1841 and Martha in 1843.  Later on there were four more additions to the family: Harold, Catherine (Kitty), Ellen and Julian.

In 1844 Dr Stephens published in London ''The Book of the Farm: Detailing the Labours of the Farmer, Farm-steward, Ploughman, Shepherd, Hedger, Cattle-man, Field-worker, and Dairy-maid'', a standard work that has provided much useful information for agricultural historians, and formed the basis of a [[BBC Two]] documentary ''[[Victorian Farm]]'' in 2009.

In 1846 the doctor and his family, tired of the noise and squalor of the city, moved six miles north to the leafy village of [[Finchley]], where they acquired a spacious home, Grove House, with outhouses and several fields adjoining Ballards Lane.  The older part of the rambling house was torn down and redeveloped and there were stables for horses and a variety of other animals.  In the years that followed first Martha, then Henry, were sent to school in Paris to learn French.<ref name="one" />

==Products==

1851 was the year of the [[Great Exhibition]] in London's [[Hyde Park, London|Hyde Park]].  Stephens' indelible "blue-black writing fluid" and other inks received favourable press reviews and the company's wood stains for oak, mahogany and walnut were used on the doors and panels of the Exhibition buildings.<ref name="one" /> The Prince Consort, visiting early one morning, was reported to be much impressed by the results.

Later that year, Stephens' Ink company advertisements featured not only inks and wood stains but also patent propelling pencils, parallel rulers, and stamp and label dampers, and claimed patronage by Prince Albert and other European royals.<ref name="five">Advertisement from 1851 held by The Stephens Collection, London</ref>

In 1852 Henry Charles, now aged eleven, returned from Paris to attend [[University College School]] in Gower Street, London, where he stayed for five years before studying chemistry at the School of Mines in Kensington.  Dr Stephens was a friend of [[Michael Faraday|Professor Michael Faraday]] (1791–1867) and often took his young son to the eminent professor's lectures at the [[Royal Institution]].<ref name="one" />

==Henry Charles Stephens==

Around this time Stephens continued to instruct his son in the preparation of inks and wood stains in his outhouses at Grove House in [[Finchley]] in a sort of apprenticeship.  The experience would prove invaluable when young Henry later went to work in his father's factory.  Henry Charles married, in 1863, Agnes Mackereth, the daughter of his father's old friend at medical school.

==Death==

Tragedy struck on 15 September 1864 when Dr Stephens, returning from his office in Aldersgate Street with Henry Charles, collapsed and died at [[Farringdon station]].  His son boarded a train believing his father was in the crowd behind him, and only learned of his missing father's death later that evening.<ref name="one" />

The doctor was deeply mourned, in London as well as in Finchley.  In addition to Michael Faraday, among his personal friends were John Glover, Queen Victoria's librarian; [[Thomas Sopwith (geologist)|Thomas Sopwith]], geologist and mining engineer; and Sir [[Benjamin Ward Richardson]], eminent doctor and physiologist.  At his funeral it was said that although Dr Henry's life was given to the service of science, he had a great yearning for literature and the arts and a great love of justice.<ref name="one" />

He was buried in [[East Finchley Cemetery|St Marylebone Cemetery]], Finchley, where a monument commemorates him and other members of his family.  The family name is commemorated as a street name, Stephens Way, in Redbourn, Hertfordshire.<ref name="two" />

The company founded by Dr Stephens continued for more than a century after his death.  Dr Stephens' eldest son [[Henry Charles Stephens]] (1841–1918), known to his friends as "Inky", ran the company until his death in 1918, was an entrepreneur and philanthropist and also MP for Hornsey and Finchley from 1887 until 1900.

==The Stephens Collection==

A small museum – '''The Stephens Collection''' – in [[Finchley]], north London, features the history of the Stephens family and the eponymous ink company, with a section on the development of writing materials.  The museum is located in a new visitor centre in the former stables block at [[Avenue House]], the home of Henry "Inky" Stephens from 1874 until his death.  In his will he bequeathed Avenue House and his ten acres of gardens to the 'people of Finchley', held in trust by the Finchley Urban District Council so long as it was kept open 'for the use and enjoyment always of the people'.

The house and [[Avenue House Grounds|grounds]], now known as Stephens House and Gardens, are run by the [http://www.stephenshouseandgardens.com Avenue House Estate Trust], a local charity, for public benefit and as a memorial to Dr Stephens and his son.

==References==
{{reflist}}

==External links==
*[http://www.bbc.co.uk/radio4/history/making_history/makhist10_prog11b.shtml Henry 'Inky' Stephens – the inventor of blue-black ink at BBC Radio 4]

{{DEFAULTSORT:Stephens, Henry}}
[[Category:1796 births]]
[[Category:1864 deaths]]
[[Category:19th-century English medical doctors]]
[[Category:English inventors]]
[[Category:English travel writers]]
[[Category:People from Finchley]]
[[Category:English male writers]]
[[Category:19th-century male writers]]
[[Category:People from Holborn]]
[[Category:People from Redbourn]]