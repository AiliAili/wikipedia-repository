{{Infobox journal
| title = Police Quarterly
| cover = [[File:Police Quarterly.tif]]
| editor = John L. Worrall
| discipline = [[Criminology]]
| former_names =
| abbreviation = Police Q.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1998-present
| openaccess = 
| license = 
| impact = 0.684
| impact-year = 2011
| website = http://pqx.sagepub.com/
| link1 = http://pqx.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pqx.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1098-6111
| eISSN = 1552-745X
| OCLC = 150091551
| LCCN = 98000390
}}
'''''Police Quarterly''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[criminology]],  in particular theoretical contributions, essays, and empirical studies on issues related to policing. The journal's [[editor-in-chief]] is John L. Worrall ([[University of Texas at Dallas]]). It was established in 1998 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''Police Quarterly'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 0.684, ranking it 29 out of 50 journals in the category "Criminology & Penology".<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Criminology & Penology |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=[[Web of Science]] |postscript=.}}</ref> 

== References ==
{{reflist}}

== External links ==
* {{Official website|http://pqx.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Policing journals]]
[[Category:Publications established in 1998]]
[[Category:Quarterly journals]]