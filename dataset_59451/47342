{{Infobox musical artist
| name                = Anthony D'Amato
| image               = 013116BrooklynBowlVivian.JPEG
| image_size          = 220px
| landscape           = yes
| caption             = Anthony D'Amato at Brooklyn Bowl, January 2016
| background          = solo_singer
| birth_date          = {{birth date and age|1987|11|27}}
| birth_place         = Blairstown, New Jersey, United States
| genre                 = indie folk, rock, americana
| occupation          = songwriter, singer, musician
| years_active        = 2010 - present
| label               = New West Records
| website             = http://www.anthonydamatomusic.com
}}

'''Anthony D'Amato''' is an American [[songwriter]] and singer signed to [[New West Records]].<ref>{{cite web|last=Brandle|first=Lars|title=Anthony D'Amato Signs With New West, Drops 'Ludlow': Exclusive Video Premiere|url=http://www.billboard.com/articles/news/5800716/anthony-damato-signs-with-new-west-drops-ludlow-exclusive-video-premiere|work=Billboard|accessdate=16 February 2014}}</ref> His latest album is 2016's 'Cold Snap,' produced by [[Mike Mogis]] ([[Bright Eyes (band)|Bright Eyes]], [[First Aid Kit (band)|First Aid Kit]]) and featuring performances by [[Conor Oberst]] and members of [[Bright Eyes (band)|Bright Eyes]], [[The Faint]], and [[Cursive (band)|Cursive]].<ref>http://consequenceofsound.net/2016/04/anthony-damato-announces-new-album-cold-snap-shares-rain-on-a-strange-roof-listen/</ref>

== Biography ==

D'Amato grew up in [[Blairstown, New Jersey]], and attended college at [[Princeton University]], where he recorded his first album 'Down Wires' in a dormitory during his senior year.<ref>https://www.nytimes.com/2011/01/23/nyregion/23artsnj.html?_r=1&)</ref> He released the record independently in 2010, catching the attention of [[NPR]], which called his song "My Father's Son" a "modern folk gem,"<ref>http://www.npr.org/2011/01/19/133049358/anthony-damato-warm-and-magnetic</ref> and the [[World Cafe]], which featured him in their emerging artist series "Next."<ref>http://www.npr.org/2011/07/18/138467269/anthony-damato-on-world-cafe-next</ref>
D'Amato followed it up with another home recording, 'Paper Back Bones,' which made [[BBC Scotland|BBC Scotland's]] Best Americana Albums of 2012,<ref>http://www.bbc.co.uk/programmes/b01p6v7p</ref> and was described by host [[Ricky Ross (musician)|Ricky Ross]] as "one of [our] favourites of all time."<ref>http://www.rickyross.com/blog/?p=2761</ref>

In 2014, D'Amato released his [[New West Records]] debut, 'The Shipwreck From The Shore,' which was inspired in part by his time studying with the Pulitzer Prize-winning Irish poet [[Paul Muldoon]]<ref>http://www.npr.org/2014/08/30/343990263/anthony-damato-a-songsmith-schooled-by-a-master-poet</ref> and earned widespread critical acclaim, with [[NPR]] noting that "he writes in the tradition of [[Bruce Springsteen]] or [[Josh Ritter]]"<ref>http://www.npr.org/event/music/357196181/anthony-damato-tiny-desk-concert</ref> and [[USA Today]] saying it "strikes every right note."<ref>http://www.usatoday.com/story/popcandy/2014/07/18/week-music/12827561/</ref> The album was recorded with producer Sam Kassirer ([[Josh Ritter]], [[Langhorne Slim]]) at The Great North Sound Society in Maine with members of [[Bon Iver]] and [[Megafaun]].<ref>https://mountainx.com/arts/5-questions-with-anthony-damato/</ref> Songs from the record collectively cracked more than three million plays on [[Spotify]]<ref>https://open.spotify.com/artist/1oplL2hHYq7CQykvSbd6gy</ref> and turned up on the ABC series [[Nashville (2012 TV series)|Nashville]],<ref>http://www.anthonydamatomusic.com/news-1/2016/3/12/rfoaw0fcpeld4b9xz9g60q239yud43</ref> while the album earned additional praise everywhere from the [[The New York Times|New York Times]]<ref>http://www.newwestrecords.com/news/ny-times-premieres-the-shipwreck-from-the-shore</ref> and [[The Wall Street Journal|WSJ]]<ref>https://www.wsj.com/articles/young-musician-studies-art-of-telling-stories-through-songs-1409364565</ref> to [[New York (magazine)|NY Mag]]<ref>http://newwestrecords.com/news/nymags-approval-matrix</ref> and [[Billboard (magazine)|Billboard]].<ref>http://www.billboard.com/articles/news/5800716/anthony-damato-signs-with-new-west-drops-ludlow-exclusive-video-premiere)</ref>

D'Amato returned in 2016 with 'Cold Snap,' recorded in Omaha, NE, with producer [[Mike Mogis]] ([[Bright Eyes (band)|Bright Eyes]], [[First Aid Kit (band)|First Aid Kit]]) and featuring performances by [[Conor Oberst]] and members of [[Bright Eyes (band)|Bright Eyes]], [[The Faint]], and [[Cursive (band)|Cursive]].<ref>http://consequenceofsound.net/2016/04/anthony-damato-announces-new-album-cold-snap-shares-rain-on-a-strange-roof-listen/</ref> [[Rolling Stone|RollingStone.com]] named him an "Artist You Need To Know",<ref>http://www.rollingstone.com/music/lists/10-new-country-artists-you-need-to-know-july-2016-20160701/anthony-damato-20160630</ref> and D'Amato made his national TV debut on [[CBS This Morning]].<ref>http://www.cbsnews.com/videos/anthony-damato-performs-rain-on-a-strange-roof/</ref>

== Discography ==

* ''Down Wires'' (2010)
* ''Paper Back Bones'' (2012)
* ''The Shipwreck From The Shore'' (Sept 2, 2014)
* ''Cold Snap'' (June 17, 2016)

== References ==

{{reflist}}

== External links ==
* [http://www.anthonydamatomusic.com Official Webpage]

{{Authority control}}

{{DEFAULTSORT:D'Amato, Anthony}}
[[Category:Living people]]
[[Category:1987 births]]
[[Category:American folk rock musicians]]
[[Category:American singer-songwriters]]
[[Category:American male singer-songwriters]]
[[Category:New West Records artists]]
[[Category:Songwriters from New Jersey]]
[[Category:Musicians from New Jersey]]
[[Category:Singers from New Jersey]]