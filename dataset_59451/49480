{{About|a programming language||Fantom (disambiguation)}}
{{Tone|date=March 2012}}
{{Infobox programming language
| name = Fantom
| logo = [[File:Fantom-logo.png]]
| paradigm = [[Multi-paradigm programming language|multi-paradigm]]
| year = 2005<ref>[http://fantom.org/sidewalk/topic/355 Blog post about history of Fantom]</ref>
| designer =
| developer = Brian Frank, Andy Frank
| latest_release_version = 1.0.68
| latest_release_date    = {{Start date and age|2016|04|07}}<ref>{{cite web | url = http://www.fantom.org/forum/topic/2387 | title = Fantom | date = 7 April 2016}}</ref>
| typing = [[static typing|static]], [[dynamic typing|dynamic]]
| implementations =
| dialects =
| influenced_by = [[C Sharp (programming language)|C#]], [[Java (programming language)|Java]], [[Scala (programming language)|Scala]], [[Ruby (programming language)|Ruby]], [[Erlang (programming language)|Erlang]]
| influenced =
| license = [[Academic Free License]] version 3.0<ref>[http://fantom.org/doc/docIntro/Faq.html FAQ of Fandoc language website]</ref>
| website = {{URL|www.fantom.org}}
}}

'''Fantom''' is a general purpose [[object-oriented programming language]] created by Brian and Andy Frank<ref>[http://fantom.org/doc/docIntro/Faq.html#contact Fantom FAQ: Contact Us]</ref> that runs on the [[Java Runtime Environment]] (JRE), [[JavaScript]], and the .NET [[Common Language Runtime]] (CLR)  (.NET support is considered "prototype"<ref>[http://fantom.org/doc/docIntro/Roadmap.html Fantom FAQ: Roadmap]</ref> status).  Its primary design goal is to provide a standard library [[application programming interface|API]]<ref>[http://fantom.org/doc/docIntro/WhyFantom.html Fantom FAQ: Why Fantom?]</ref> that abstracts away the question of whether the code will ultimately run on the JRE or CLR. Like [[C Sharp (programming language)|C#]] and [[Java (programming language)|Java]], Fantom uses a [[curly brace programming language|curly brace syntax]]. The language supports [[functional programming]] through closures and concurrency through the [[Actor model]]. Fantom takes a "middle of the road" approach to its type system, blending together aspects of both static and dynamic typing.

== Typing ==
Fantom's type system is simple by design. All variables are statically typed, as they are in [[C Sharp (programming language)|C#]] and [[Java (programming language)|Java]]. Fantom rejects [[Generic programming|generic types]] due to their complexity, but it does have a set of built-in generic types: <tt>List</tt>, <tt>Map</tt>, and <tt>Func</tt>. Fantom can also take on the feel of a dynamically typed language through dynamic calls and automatic [[downcasting]]. Fantom has an easy to use [[Reflection (computer science)|reflection]] API and [[metaprogramming]] capabilities.

Fantom is open source under the [[Academic Free License]] 3.0 and is available for Windows and Unix-like platforms (including Mac OS X).<ref>{{cite web |last=McAllister |first=Neil |url=http://www.infoworld.com/d/application-development/10-programming-languages-could-shake-it-181548?page=0,2 |title=10 programming languages that could shake up IT |publisher=InfoWorld |date=2012-01-03 |accessdate=2015-10-30}}</ref>

== Pods ==
In Fantom, the unit of deployment is called a ''pod''. Pods take on the role of [[namespace]]s, [[Java package|packages]], and [[Modular programming|modules]]. They are stored as .pod files, which are [[ZIP (file format)|zip]] files containing the FCode (the Fantom bytecode), the documentation, and [[Resource (Java)|resource files]] necessary to run the pod. A pod can define any number of types for use in other libraries and applications. A pod name fully qualifies a type name. For example, <tt>fwt::Widget</tt> is distinct from <tt>webapp::Widget</tt>. If a pod contains a type named <tt>Main</tt>, then it can be executed on the command line with: <tt>fan &lt;podName&gt;</tt>

The Fantom build system can package a set of Pods into a [[JAR (file format)|JAR archive]] through <tt>build::JarDist</tt>.

== Fantom Widget Toolkit ==
Fantom ships with a standard [[Widget toolkit|windowing toolkit]] called the Fantom Widget Toolkit, or FWT for short.<ref>{{cite web|url=http://fantom.org/doc/fwt/pod-doc.html |title=fwt – Fantom |publisher=Fantom.org |date=2015-01-08 |accessdate=2015-10-30}}</ref> Like Fantom, FWT was designed to be portable across several platforms. It is currently implemented on the JVM using the [[Standard Widget Toolkit]] as a backend. The JavaScript implementation is backed by the [[canvas element]] and [[JavaFX]], allowing FWT applications to be run in a web browser. There are plans for a CLR implementation using [[Windows Forms]].

== "Hello World" example ==
Here is the classic [[Hello world program]] written in Fantom:

<source lang="fan" highlight="4">
// Hello from Fantom!
class HelloWorld
{
  static Void main()
  {
    echo("Hello, World!")
  }
}
</source>

Notice that "Void" is capitalized. This is because Void is a class, not a primitive type in Fantom.

== Name change ==
The original name of the Fantom programming language was Fan, named after the [[Fan district|neighborhood]] where the creators live in [[Richmond, Virginia]]. After gaining some popularity members of the community raised concerns about the [[Search engine optimization|searchability]] of the name. In November 2009,<ref>[http://es.scribd.com/doc/47962182/scala-haskell-and-fantom-programming-language History of Fantom programming language]</ref> the name of the project was officially changed from Fan to Fantom.<ref name="Fantom rename">[http://fantom.org/sidewalk/topic/821 Fan is officially now Fantom]</ref>

== Other features ==
Fantom has other useful features:

* Fantom supports imports of Java Classes and modules with some limitations.<ref>[http://fantom.org/doc/docLang/JavaFFI.html Java FFI at Fantom home page]</ref>
* Integer is 64-bit. Unlike Java and C#, Fantom does not have Long or Short integer types.
* Serialization and deserialization of classes to/from strings.<ref>[http://fantom.org/doc/docLang/Serialization.html Serialization at Fantom.org]</ref>
* Fantom does ''not'' support [[tuple]]s (that is, types which are the [[Cartesian product]] of other types)<ref>[http://fantom.org/sidewalk/topic/1815 Tuples question at Fantom official page]</ref>

== See also ==
{{Portal|Free software}}
* [[Boo (programming language)|Boo]]
* [[Ceylon (programming language)|Ceylon]]
* [[C Sharp (programming language)|C#]]
* [[Erlang (programming language)|Erlang]]
* [[Gosu (programming language)|Gosu]]
* [[Groovy (programming language)|Groovy]]
* [[Kotlin (programming language)|Kotlin]]
* [[Scala (programming language)|Scala]]
* [[Ruby (programming language)|Ruby]]

== References ==
{{Reflist}}

==Further reading==
* {{cite web |last=Binstock |first=Andrew |title=Top five scripting languages on the JVM: Groovy and JRuby lead a strong field, with Scala, Fantom, and Jython following behind |url=http://www.infoworld.com/article/2627426/application-development/top-five-scripting-languages-on-the-jvm.html?page=4 |publisher=Dr. Dobb's |date=2010-07-14 |accessdate=2015-10-30}}
* {{cite web |last=Frank |first=Brian |title=Fantom |url=http://www.drdobbs.com/tools/fantom/229218754?pgno=1 |publisher=Dr. Dobb's |date=2011-02-18 |accessdate=2015-10-30}}

== External links ==
* {{Official website}}
* [http://fantom.org/doc/docIntro/WhyFantom.html Why Fantom], by Fantom's authors, explains why they have created this language.
* [http://www.ajaxonomy.com/2008/java/not-a-fan-of-scala-an-evolutionary-approach Not a Fan of Scala? An Evolutionary Approach | Ajaxonomy] - blog post discussing ''Fan''
* [http://www.sdtimes.com/link/33476 Fan of a New Language | SD Times]
* [http://article.gmane.org/gmane.comp.java.vm.languages/633 Re: Fan Programming Language (jvm-languages@googlegroups.com mailing list)] a forum post by one of Fantom's authors.
* [http://www.artima.com/lejava/articles/javaone_2010_the_next_big_jvm_lang_stephen_colebourne.html The Next Big JVM Language], a conversation with Stephen Colebourne by Bill Venners.
* [http://langref.org/fantom Language reference page at LangRef.org]

{{DEFAULTSORT:Fantom (Programming Language)}}
[[Category:Object-oriented programming languages]]
[[Category:Statically typed programming languages]]
[[Category:JVM programming languages]]
[[Category:.NET programming languages]]
[[Category:Programming languages created in 2005]]
[[Category:Software using the Academic Free License]]