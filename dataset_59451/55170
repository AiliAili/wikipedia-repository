{{Infobox journal
| title         = Able Muse
| cover         = [[File:9780986533822cvr-front-mm.jpg|frameless]]
| caption       = Front Cover - Able Muse, Inaugural Print Edition, Winter 2010
| editor        = Alexander Pepple
| discipline    = [[Literary journal]]
| language      = English
| abbreviation  =
| publisher     = Able Muse Review
| country       = [[United States]]
| frequency     = Semiannual
| history       = 1999-present
| firstdate = December 1999
| website = {{URL|http://www.ablemuse.com}}
| ISSN          = 2168-0426
| eISSN         = 1528-3798
}}

'''''Able Muse''''' is a [[literary journal]] founded in San Jose, California, in 1999 by Alexander Pepple. ''Able Muse'' started as an online journal ({{ISSN|1528-3798}}), winter 1999, publishing poems, short stories, essays, book reviews, art and photography from authors worldwide. 
''Able Muse'' includes the sister organizations of [[Eratosphere]], an online workshop forum for poetry, fiction and art; and [http://www.ablemusepress.com Able Muse Press], a small press that publishes poetry and fiction collections by established and emerging authors.

==''Able Muse'' review==
''Able Muse'' transitioned into a print journal <ref>{{ISSN|2168-0426}}</ref> with the tenth release, the Winter 2010 Inaugural Print Edition, accompanied with the publication of the ''Able Muse Anthology'',<ref>{{cite book| title=[[Able Muse Anthology]], 2005| editor=Alexander Pepple | publisher=Able Muse Press| year=2010| isbn=978-0-9865338-0-8 }}</ref> edited by Alexander Pepple with a foreword by [[Timothy Steele]].The anthology culled the best of more than the first decade of published material. In his blurb for the anthology included in it, [[Dana Gioia]], the former Chairman of the [[National Endowment for the Arts]] states that “Over the last twelve years, ''Able Muse'' and its extraordinary companion website the ''[[Eratosphere]]'' have created a huge and influential virtual literary community . . . ''Able Muse'' has now gathered the best of their works in both prose and verse in an ink-and-paper anthology. This book fills an important gap in understanding what is really happening in early twenty-first century American poetry.” ''Able Muse'' still maintains its online presence where it simultaneously releases a digital edition that mirrors the corresponding issue of the print edition.

The ''Able Muse''’s editorial board of acclaimed poets and writers includes [[X. J. Kennedy]], [[A.E. Stallings]], [[Rachel Hadas]], and [[Deborah Warren]]. ''Able Muse'' has been edited, since its inception by Alexander Pepple.

''Able Muse'' has been featured in major American reviews, such as a poem from the Winter 2010 issue, “The Cricket in the Stump” by [[Catherine Tufariello]], featured in [[Ted Kooser]]’s [http://www.americanlifeinpoetry.org/columns/349.html ''American Life in Poetry'']; and from the Winter 2013 issue, “Sawhorses” by [[Greg Williamson]], the issue’s featured poet, was featured in [http://www.versedaily.org/2013/aboutgregwilliamson.shtml ''Verse Daily''].

===Contributors===
Contributors to ''Able Muse'' are international in scope and include [[Ted Kooser]], [[Turner Cassity]], [[X. J. Kennedy]], [[Mark Jarman]], [[A.E. Stallings]], [[Annie Finch]], [[Rachel Hadas]], [[R.S. Gwynn]], [[Richard Percival Lister]], [[Willis Barnstone]] [[William Baer (writer)|William Baer]], [[Deborah Warren]], [[Greg Williamson]], [[John Whitworth]], [[Stephen Edgar]], [[Catharine Savage Brosman]], [[Kim Bridgford]], [[Ned Balbo]], [[Rory Waterman]], [[Lyn Lifshin]], [[Amit Majmudar]], [[R. S. Gwynn]], [[Michael Palma]], [[Jay Hopler]], [[Rhina P. Espaillat]], [[Mark Jarman]], [[Thomas Carper]], [[A.M. Juster]], [[J. Patrick Lewis]], [[Dolores Hayden]], [[Geoffrey Brock]], [[Lewis Buzbee]], [[Marly Youmans]], [[Tamas Dobozy]], [[Adel Souto]], and [[Thaisa Frank]].

===Awards===
''Able Muse'' hosts a series of annual awards designed to bring recognition to emerging poets and writers or enhance the prestige of established authors. Poets compete, with a handful of their best poems, in the Able Muse Write Prize for Poetry, and writers compete with a couple of their best flash fiction pieces in the Able Muse Write Prize for Fiction. Past judges for these contests include [[Rachel Hadas]], [[Alan Cheuse]], Ellen Sussman, John Drury, Thaisa Frank, [[Kelly Cherry]], [[Amit Majmudar]], Dick Allen.
The Able Muse Book Award is held annually in collaboration with the sister organization [http://www.ablemusepress.com Able Muse Press], where poets compete for the publication of a full-length collection of poetry. Past judges include [[Andrew Hudgins]], [[Mary Jo Salter]], [[X. J. Kennedy]] and [[Molly Peacock]].

===Staff===
<ref name=Staff>[http://www.ablemuse.com/v9/masthead Staff, "Able Muse".]</ref>
* Editor: Alexander Pepple
* Fiction Editor: Karen Kevorkian
* Nonfiction Editor: Gregory Dowling
* Assistant Poetry Editors: Stephen Kampa, Callie Siskel, Reagan Upshaw
* Assistant Fiction Editors: Jonathan Danielson, Janice D. Soderling, Rob Wright
* Editorial Board: [[Rachel Hadas]], [[X.J. Kennedy]], [[A.E. Stallings]], [[Timothy Steele]], [[Deborah Warren]]

===Reviews===
<blockquote>It’s nice to see that the new New Formalist critics published in Able Muse definitely do not write in a tweedy style, as evidenced by Taylor’s  . . . The fiction in Able Muse is also exceptional, especially the surprising “Relativity” by the astonishingly young Emily Cutler, currently a junior in high school. There is also an excellent “photographic exhibit” by Massimo Sbreni, a “people and travel photographer from Ravenna, Italy.” Four of the photos can be seen online at the [http://www.ablemuse.com/v10/featured-artist/massimo-sbreni ''Able Muse website'']. This reviewer also urges potential readers to check out their [http://www.ablemuse.com/archives/ ''archives''] in hopes that others will be impressed enough to subscribe to the excellent and non-tweedy print edition piece.<ref>[http://www.newpages.com/cl-grad-writing-progs/item/5390-able-muse "Literary Magazines: Literary Magazine Reviews", ''The NewPages'', June 15, 2011]</ref></blockquote>

<blockquote>Although it’s slightly twee, David J. Rothman’s ''Able Muse'' conversation with poet David Mason exemplifies the sort of experimentation that makes the magazine well worth reading . . . This issue also contains the well-deserved winner of the ''Able Muse'' fiction contest—Douglas Campbell’s “Sunflowers, Rivers”—in which a boy first recognizes his mother as a sexual being . . .  And then there’s the lovely “Street Song” by William Conelly, with the refrain “Fortune save you, Brother Crow.” It’s a poem that, like the best parts of ''Able Muse'', is serious, playful . . .<ref>[http://www.newpages.com/literary-magazine-reviews/magazine-reviews-archive/2008-01-07/item/4694-able-muse "Literary Magazines: Literary Magazine Reviews", ''The New Pages'', April 16, 2012]</ref></blockquote>

<blockquote>While poetry and short story collections provide more in-depth exposure to the vision of a single writer, they don’t offer the same opportunity to unexpectedly stumble onto your next obsession like a good journal can. ''Able Muse'', with its eclectic blend of fiction, essays, book reviews, art portfolios, artist interviews, as well as its focus on metrical poetry, provides readers with a bevy of opportunities to do just that . . . Whether it’s an essay compelling you to buy the book of an under-appreciated poet, or the innovative structure of a short story causing you to rethink the role of structure in your own work, ''Able Muse''’s latest trove will definitely give you something to treasure.<ref>[http://newpages.com/literary-magazine-reviews/magazine-reviews-archive/2014-03-17/item/4096-able-muse "Literary Magazines: Literary Magazine Reviews", ''The New Pages'', March 17, 2014]</ref></blockquote>

==Able Muse Press==
[http://www.ablemusepress.com Able Muse Press] <ref>[http://www.pw.org/content/able_muse_press_0 Able Muse Press’ ''Poets & Writers'' Listing]</ref> is a sister organization of ''Able Muse'', founded in 2010 by Alexander Pepple, who is also the press’ editor and publisher. This was at about the same time the print edition of ''Able Muse'' was launched.

===Honors Received===
Despite being a small press that publishes only a handful of titles yearly, Able Muse Press has garnered several awards for its published titles, including winning: the 2013 Outstanding Award in Poetry from the [[Wisconsin Library Association]], 2014 Gold Medal in fiction from the [[Independent Publisher Book Awards]], 2014 [[Peace Corps]] Writers Best Book Award; and placing as finalist in: the 2012 [[Governor General's Awards]] in Poetry, 2012 [[Colorado Book Awards]] for Poetry, 2013 [[Griffin Poetry Prize]], 2013 and 2013 [[Massachusetts Book Award]], and 2013 Foreword Review’s Best Book of the Year.

===Books published===
<ref>[http://www.ablemusepress.com/publication-list Able Muse Publication List]</ref>
* [[Melissa Balmain|Balmain, Melissa]], ''Walking in on People – Poems'' (June 2014). ISBN 978-1-927409-29-9. Winner, 2013 Able Muse Book Award.
* [[Ben Berman|Berman, Ben]], ''Strange Borderlands – Poems'' (January 2013). ISBN 978-1-927409-05-3. Winner, 2014 [[Peace Corps]] Writers Best Book Award; Finalist, 2014 [[Massachusetts Book Award]]; [[Publishers Weekly]] starred review.<ref>[http://www.publishersweekly.com/978-1-927409-05-3 Starred Review, ''Publishers Weekly'', February 18, 2013]</ref>
* [[Michael Cantor|Cantor, Michael]], ''Life in the Second Circle – Poems'' (March 2013). ISBN 978-0-9878705-5-1. Finalist, 2013 [[Massachusetts Book Award]].
* [[Catherine Chandler|Chandler, Catherine]], ''Line of Flight – Poems'' (April 2011). ISBN 978-0-9865338-3-9. Shortlisted for the 2013 [[Poets' Prize]].
* [[William Conelly|Conelly, William]], ''Uncontested Grounds – Poems'' (March 2015). ISBN 978-1-927409-39-8.
* [[Maryann Corbett|Corbett, Maryann]], ''Credo for the Checkout Line in Winter – Poems'' (September 2013). ISBN 978-1-927409-14-5.
* [[D.R. Goodman|Goodman, D.R.]], ''Greed: A Confession – Poems'' (December 2014). ISBN 978-1-927409-38-1.
* [[Margaret Ann Griffiths|Griffiths, Margaret Ann]], ''The Poetry of M A Griffiths'' (April 2011). ISBN 978-1-904852-28-5.
* [[Ellen Kaufman|Kaufman, Ellen]], ''House Music – Poems'' (February 2014). ISBN 978-1-927409-25-1.
* [[Carol Light|Light, Carol]], ''Heaven from Steam – Poems'' (January 2014). ISBN 978-1-927409-18-3.
* [[April Lindner|Lindner, April]], ''This Bed Our Bodies Shaped – Poems'' (November 2012). ISBN 978-0-9878705-9-9.
* [[Jeredith Merrin|Merrin, Jeredith]], ''Cup – Poems'' (December 2014). ISBN 978-1-927409-34-3.
* [[Richard Newman (poet)|Newman, Richard]], ''All the Wasted Beauty of the World – Poems'' (October 2014). ISBN 978-1-927409-31-2.
* [[Frank Osen|Osen, Frank]], ''Virtue, Big as Sin – Poems'' (August 2013). ISBN 978-1-927409-16-9. Winner, 2012 Able Muse Book Award; Shortlisted for the 2015 [[Poets' Prize]].
* [[Alexander Pepple (editor)|Pepple, Alexander]], ''Able Muse Anthology'' (July 2010). ISBN 978-0-9865338-0-8.
* [[Alexander Pepple (editor)|Pepple, Alexander]], ''Able Muse – a review of poetry, prose & art'' (Winter 2010 onward). {{ISSN|2168-0426}}.
* [[James Pollock|Pollock, James]], ''Sailing to Babylon – Poems'' (June 2012). ISBN 978-0-9865338-7-7. Winner, Outstanding Achievement Award in Poetry from the [[Wisconsin Library Association]]; Finalist, 2013 [[Griffin Poetry Prize]]; Finalist, 2012 [[Governor General’s Literary Award]] in Poetry; Honorable Mention, 2012 Posner Poetry Book Award.
* [[Aaron Poochigian|Poochigian, Aaron]], ''The Cosmic Purr – Poems'' (February 2012). ISBN 978-0-9878705-2-0. Shortlisted for the 2014 [[Poets' Prize]].
* [[Stephen Scaer|Scaer, Stephen]], ''Pumpkin Chucking – Poems'' (January 2014). ISBN 978-1-927409-12-1.
* [[Hollis Seamon|Seamon, Hollis]], ''Corporeality – Stories'' (January 2013). ISBN 978-1-927409-03-9. Gold Medal winner, 2014 [[Independent Book Publishers Association]] Outstanding Book Award; Finalist, 2013 Foreword Review’s Best Book of the Year.
* [[Matthew Buckley Smith|Smith, Matthew Buckley]], ''Dirge for an Imaginary World – Poems'' (April 2012). ISBN 978-0-9878705-0-6. Winner, 2011 Able Muse Book Award.
* [[Barbara Ellen Sorensen|Sorensen, Barbara Ellen]], ''Compositions of the Dead Playing Flutes – Poems'' (December 2013). ISBN 978-1-927409-23-7.
* [[Wendy Videlock|Videlock, Wendy]], ''The Dark Gnu and Other Poems'' (January 2013). ISBN 978-1-927409-09-1 (paperback), ISBN 978-1-927409-13-8 (deluxe edition).
* [[Wendy Videlock|Videlock, Wendy]], ''Nevertheless – Poems'' (May 2011). ISBN 978-0-9865338-4-6. Finalist, 2012 [[Colorado Book Awards]].
* [[Richard Wakefield|Wakefield, Richard]], ''A Vertical Mile – Poems'' (May 2012). ISBN 978-0-9878705-7-5. Shortlisted for the 2014 [[Poets' Prize]].
* [[Chelsea Woodard|Woodard, Chelsea]], ''Vellum – Poems – Poems'' (November 2014). ISBN 978-1-927409-35-0.

==See also==
*[[List of literary magazines]]

==References==
{{Reflist}}

==External links==
*[http://www.ablemuse.com/ ''Able Muse'']  (official website)
*[http://www.pw.org/content/able_muse_1 ''Able Muse'' listing at ''Poets & Writers'']
*[http://catalog.loc.gov/vwebv/search?searchCode=TALL&searchArg=Able+muse+%28Online%29?&searchType=1&recCount=100 Library of Congress Online Catalog for ''Able Muse'']

{{Authority control}}

[[Category:American literary magazines]]
[[Category:Biannual magazines]]
[[Category:Magazines established in 1999]]
[[Category:Magazines published in California]]
[[Category:Publications established in 1999]]
[[Category:Poetry literary magazines]]