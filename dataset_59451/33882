{{taxobox
 | image               = Crested Tern Tasmania.jpg
 | image_caption       = Breeding plumage ''T. b. cristata'' displaying
 | status              = LC
 | status_system       = IUCN3.1
 | status_ref          = <ref name=IUCN>{{IUCN|id=22694571 |title=''Sterna bergii'' |assessor=[[BirdLife International]] |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
 | regnum              = [[Animal]]ia
 | phylum              = [[Chordate|Chordata]]
 | classis             = [[Bird|Aves]]
 | ordo                = [[Charadriiformes]]
 | familia             = [[tern|Sternidae]]
 | genus               = ''[[Thalasseus]]''
 | species             = '''''T. bergii'''''
 | binomial            = ''Thalasseus bergii''
 | binomial_authority  = ([[Martin Lichtenstein|Lichtenstein]], 1823)
 | synonyms            = ''Sterna bergii'' <small>Lichtenstein, 1823</small>
 | synonyms_ref        = <ref>{{avibase|526978C4A16D3750|Greater crested tern}}</ref>
 | range_map           = SternaBergiMap2.svg | range_map_width     = 250px
 | range_map_caption   = 
{{Legend2|#007D1D|Approximate breeding range|border=1px solid #aaa}}<br />{{Legend2|#AFEEEE|Wintering range|border=1px solid #aaa}}
}}

The '''greater crested tern'''<ref name=gill>{{cite web|editor1=Gill, F |editor2=Donsker, D |title=IOC World Bird Names (v 2.11) |work= |url=http://www.worldbirdnames.org/n-shorebirds.html |publisher=International Ornithologists' Union |deadurl=yes |archiveurl=https://web.archive.org/web/20131205171302/http://www.worldbirdnames.org/n-shorebirds.html |archivedate=2013-12-05 |df= }} Retrieved 28 February 2012</ref> (''Thalasseus bergii''), also called '''crested tern''' or '''swift tern''', is a [[seabird]] in the [[tern]] family that nests in dense colonies on coastlines and islands in the tropical and subtropical [[Old World]]. Its five [[subspecies]] breed in the area from [[South Africa]] around the [[Indian Ocean]] to the central [[Pacific Ocean|Pacific]] and [[Australia]], all populations [[bird migration|dispersing]] widely from the breeding range after nesting. This large tern is closely related to the [[Royal tern|royal]] and [[lesser crested tern]]s, but can be distinguished by its size and bill colour.

The greater crested tern has grey upperparts, white underparts, a yellow bill, and a shaggy black crest that recedes in winter. Its young have a distinctive appearance, with strongly patterned grey, brown and white plumage, and rely on their parents for food for several months after they have [[fledge]]d. Like all members of the genus ''Thalasseus'', the greater crested tern feeds by plunge diving for fish, usually in marine environments; the male offers fish to the female as part of the courtship ritual.

This is an adaptable species that has learned to follow fishing boats for jettisoned  [[bycatch]], and to use unusual nest sites such as the roofs of buildings and artificial islands in [[salt evaporation pond|salt pans]] and [[sewage treatment|sewage works]]. Its eggs and young are taken by [[gull]]s and [[ibis]]es, and human activities such as fishing, shooting and egg harvesting have caused local population declines. There are no global conservation concerns for this bird, which has a stable total population of more than 500,000 individuals.

== Taxonomy ==
The terns, family Sternidae, are small to medium-sized seabirds closely related to the [[gull]]s, [[skimmer]]s and [[skua]]s. They are gull-like in appearance, but typically have a lighter build, long pointed wings (which give them a fast, buoyant flight), a deeply forked tail and short legs. Most species are grey above and white below, and have a black cap that is reduced or flecked with white in the winter.<ref name = BWP764>Snow & Perrin (1998) 764</ref>

{{Cladogram|caption=Relationships in the genus ''Thalasseus''</small><ref name = ref3>Adapted from Bridge ''et al.'' (2005). This study did not include the Chinese crested tern</ref>
|clades={{clade|style=font-size:80%;line-height:80%
|label1=&nbsp;
   |1={{clade
   |label1 =&nbsp;
      |1= {{clade
          |1=''[[Lesser crested tern|T. bengalensis]]''
          |2=''[[Royal tern|T. maxima]]''
          }}
   |label2=&nbsp;
         |2=  ''T.bergii''
      }}
  |2={{clade
      |1=''[[Sandwich tern|T. sandvicensis]]''
      |2= ''[[Elegant tern|T. elegans]]''
      }}
}}
}}

The greater crested tern was originally described as ''Sterna bergii'' by German naturalist [[Martin Lichtenstein]] in 1823, but was moved to its current genus, ''[[Thalasseus]]'',<ref name = Boie>This genus had originally been created by [[Heinrich Boie]] in 1822, but had been abandoned until the Bridge (2005) study confirmed the need for a separate genus for the crested terns.</ref> after [[mitochondria]]l [[DNA]] studies confirmed that the three main head patterns shown by terns (no black cap, black cap, black cap with a white forehead) corresponded to distinct [[clade]]s.<ref name=Bridge2005 >{{cite journal|last=Bridge |first=Eli S. |author2=Jones, Andrew W. |author3=Baker, Allan J.|year=2005 |title= A phylogenetic framework for the terns (Sternini) inferred from mtDNA sequences: implications for taxonomy and plumage evolution | url = http://www.cmnh.org/site/Files/Ornithology/MPETerns.pdf| format = PDF|  doi=10.1016/j.ympev.2004.12.010  | journal=Molecular Phylogenetics and Evolution | volume=35 |pages=459–469|pmid=15804415|issue=2}}</ref>

The greater crested tern's closest relatives within its genus appear to be the [[lesser crested tern]] (''T.&nbsp;bengalensis''), and the [[royal tern]] (''T. maximus'').<ref name=Bridge2005/> The DNA study did not include the [[Critically endangered species|critically endangered]] [[Chinese crested tern]] (''T. bernsteini'') but, as that bird was formerly considered to be [[conspecificity|conspecific]] with the greater crested tern as a [[synonym]] of the subspecies ''T. b. cristatus'', it is presumably also very closely related.<ref name= Mees>{{cite journal| last= Mees | first= G. F. | year=1975 | title= Identiteit en status van ''Sterna bernsteini'' Schlegel | journal= Ardea | volume=63 |pages= 78–86 |language=nl}}</ref>

The [[genus|generic name]] of the greater crested tern is derived from [[Greek language|Greek]] ''Thalassa'', "sea", and the species [[epithet]] ''bergii'' commemorates [[Karl Heinrich Bergius|Carl Heinrich Bergius]], a [[Prussia]]n pharmacist and botanist who collected the first specimens of this tern near [[Cape Town]].<ref name= HANZAB>Higgins & Davies (1996) 605–609</ref>

The greater crested tern has about five geographical races, differing mainly in the colour of the upperparts and bill. These are listed below in [[taxonomic sequence]]. A similar number of other potential subspecies have been proposed, but are not considered valid.<ref name =HBW/>
{{clear}}
{| width = 98% class="wikitable" style="float: center; border: 5px solid #BBB; margin: .96em 0 0 .9em;"
|-
!width=18% | Subspecies<ref name =ref1>Parentheses indicate originally described under a different name</ref>
!width=27% | Breeding range
!width=27% | Distinctive features
!width=27% | Population estimates
|-
|''T. b. bergii''<ref name= Harrison/><br />(Lichtenstein, 1823)
|Coasts of [[South Africa]] and [[Namibia]]
|Dark grey above, slightly larger than ''thalassina'', least white on head<ref name= Olsen>Olsen & Larsson (1995) 35–42</ref>
|20,000 individuals (inc 6,336 breeding pairs in South Africa and up to 1,682 pairs in Namibia)<ref name= COOPER/>
|-
|''T. b. enigma''<ref name= Harrison>Harrison (1988) 383</ref><ref name =ref2>Not considered by Olsen & Larsson (1995)</ref><br />([[Phillip Clancey|Clancey]], 1979)
|[[Zambezi]] delta, [[Mozambique]], south to [[Durban]], South Africa
|Palest subspecies<ref name =HBW/>
|8,000–10,000 individuals in [[Madagascar]] and Mozambique<ref name= COOPER/>
|-
|''T. b. cristata''<ref name= Olsen/><br />([[James Francis Stephens|Stephens]], 1826)
|Eastern [[Indian Ocean]], [[Australia]] and western [[Pacific Ocean]]
|Like ''bergii'', with tail, rump and back concolorous. paler in Australia
|500,000+ individuals in Australia<ref name=HBW>Del Hoyo ''et al.'' (1996) 648</ref>
|-
|''T. b. thalassina''<ref name= Olsen/><br />([[Erwin Stresemann|Stresemann]], 1914)
|Western Indian Ocean
|Small and pale, larger and less pale in south of range
|2,550–4,500 individuals in Eastern Africa and [[Seychelles]]<ref name= COOPER/>
|-
|''T. b. velox''<ref name= Olsen/><br />([[Philipp Jakob Cretzschmar|Cretzschmar]], 1827)
|[[Red Sea]], [[Persian Gulf]], northern Indian Ocean
| Largest, heaviest, darkest and longest-billed subspecies
| 33,000 in [[Middle East]] (inc 4,000 pairs [[Oman]] and 3,500 pairs on islands off [[Saudi Arabia]])<ref name=HBW/>
|-
|}

== Description ==
[[File:Crested Tern.jpg|thumb|Non-breeding adult ''T. b. cristata'']]
The greater crested tern is a large tern with a long ({{convert|5.4|–|6.5|cm|in|abbr=on|disp=or}}) yellow bill, black legs, and a glossy black crest that is noticeably shaggy at its rear. The breeding adult of the [[Subspecies#Nomenclature|nominate subspecies]] ''T. b. bergii'' is {{convert|46|–|49|cm|in|round=0.5|abbr=on}} long, with a {{convert|125|–|130|cm|in|round=0.5|abbr=on}} wing-span; this subspecies weighs 325–397&nbsp;[[gram|g]] (11.4–14.0&nbsp;[[ounce|oz]]).<ref name=Olsen/> The forehead and the underparts are white, the back and inner wings are dusky-grey. In winter, the upperparts plumage wears to a paler grey, and the crown of the head becomes white, merging at the rear into a peppered black crest and mask.<ref name=BWP/>

The adults of both sexes are identical in appearance, but juvenile birds are distinctive, with a head pattern like the winter adult, and upperparts strongly patterned in grey, brown, and white; the closed wings appear to have dark bars. After [[moult]]ing, the young terns resemble the adult, but still have a variegated wing pattern with a dark bar on the inner [[flight feather]]s.<ref name=Olsen/>
[[Image:Crested tern444 edit.jpg|thumb|left|First year ''T. b. cristata'']]

The northern subspecies ''T. b. velox'' and ''T. b. thalassina'' are in breeding plumage from May to September or October, whereas the relevant period for the two southern African races is from December to April. For ''T. b. cristata'', the moult timing depends on location; birds from Australia and [[Oceania]] are in breeding plumage from September to about April, but those in [[Thailand]], China and [[Sulawesi]] have this appearance from February to June or July.<ref name=Olsen/>

The royal tern is similar in size to this species, but has a heavier build, broader wings, a paler back and a blunter, more orange bill. The greater crested often associates with the lesser crested tern, but is 25%&nbsp;larger than the latter, with a proportionately longer bill, longer and heavier head, and bulkier body.<ref name=BWP/> Lesser crested tern has an orange-tinted bill, and in immature plumage it is much less variegated than greater crested.<ref name=Harrison/>

The greater crested tern is highly [[bird vocalization|vocal]], especially at its breeding grounds. The territorial advertising call is a loud, raucous, [[crow|crow-like]] ''kerrak''. Other calls include a ''korrkorrkorr'' given at the nest by anxious or excited birds, and a hard ''wep wep'' in flight.<ref name=BWP/>

== Distribution and habitat ==
[[File:Little Tern with Crested Terns-2008-30-07.jpg|thumb|Roosting with [[little tern]]s, note size difference]]
The greater crested tern occurs in tropical and warm temperate coastal parts of the Old World from South Africa around the Indian Ocean to the Pacific and Australia. The subspecies ''T. b. bergii'' and ''T. b. enigma'' breed in Southern Africa from Namibia to [[Tanzania]], and possibly on islands around Madagascar. There is then a break in the breeding distribution of this species until [[Somalia]] and the Red Sea, and another discontinuity further east in southern [[India]].<ref name =HBW/>

The greater crested tern breeds on many islands in the Indian Ocean including [[Aldabra]] and Etoile in the Seychelles, the [[Chagos Archipelago]], and [[Rodrigues (island)|Rodrigues.]]<ref>{{cite book  | title=Birds of Seychelles  | first= Adrian   | last = Skerrett |author2=Bullock, I. |author3=Disley, T.  | publisher = Christopher Helm  | location = London |year = 2001  | isbn = 0-7136-3973-3  | pages = 230  }}</ref> There are colonies on numerous Pacific islands, including [[Kiribati]], [[Fiji]], [[Tonga]], the [[Society Islands]] and the [[Tuamotus]].<ref>{{cite book  | title=The Birds of Hawaii and the Tropical Pacific  | first= H. Douglas | last = Pratt |author2=Bruner, P. |author3=Berrett, D. | publisher = Princeton University Press  |location = Princeton |year = 1987  | isbn = 0-691-08402-5  | pages = 178  }}</ref>

The nests are located on low‑lying sandy, rocky, or coral islands, sometimes amongst stunted shrubs, often without any shelter at all.<ref name = BWP/> When not breeding, the greater crested tern will roost or rest on open shores, less often on boats, pilings, harbour buildings and raised salt mounds in lagoons. It is rarely seen on tidal creeks or inland waters.<ref name= COOPER/>
[[File:Thalasseus bergii in flight - Marion Bay.jpg|thumb|left|In flight]]
All populations of greater crested tern disperse after breeding. When Southern African birds leave colonies in Namibia and [[Western Cape Province]], most adults move east to the Indian Ocean coastline of South Africa. Many young birds also travel east, sometimes more than {{convert|2,000|km|mi|abbr=on}}, but others move northwards along the western coast. ''T. b. thalassina'' winters on the east African coast north to [[Kenya]] and Somalia and may move as far south as Durban. Populations of ''T. b. velox'' breeding from the Persian Gulf eastwards appear to be sedentary or dispersive rather than truly [[bird migration|migratory]], but those breeding in the Red Sea winter south along the east African coast to Kenya.<ref name= COOPER/> ''T. b. cristata'' mostly stays within {{convert|400|km|mi|abbr=on}} of its colonies, but some birds wander up to around {{convert|1,000|km|mi|abbr=on}}.<ref name= Carrick/> This species has occurred as a vagrant to [[Hawaii]],<ref name = AOU>{{cite book|last = [[American Ornithologists' Union]]|title= Check-list of North American Birds  |url= http://www.aou.org/checklist/pdf/AOUchecklistGall-Charad.pdf |format = PDF extract |publisher= [[American Ornithologists' Union]] |location=Washington, D.C. |year=1998 |edition=7th |isbn=1-891276-00-X | pages = 198}}</ref> [[New Zealand]],<ref name = birdlife/><ref name =OSNZ >{{cite web|author1=Robertson C.J.R. |author2=Medway, D. G. | title= New Zealand recognized bird names (NZRBN) | work= | url= http://www.bird.org.nz/assets/NZBRN/NZRBN_list.pdf| format = PDF|publisher= Ornithological Society of New Zealand}}</ref> [[North Korea]],<ref name = birdlife>{{cite web| title= Great Crested Tern ''Sterna bergii''  | work= BirdLife Species Factsheet (additional data)| url= http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=3263&m=1 | publisher= [[BirdLife International]]  |accessdate=7 July 2008 }}</ref> [[Jordan]],<ref name = birdlife/> and [[Israel]].<ref name = BWP>Snow & Perrin (1998) 770–771</ref>
{{clear}}

== Behaviour ==

=== Breeding ===
[[File:Crested Tern courtship.jpg|thumb|''T. b. cristata'' displaying]]

The greater crested tern breeds in [[seabird colony|colonies]], often in association with other seabirds. It is [[monogamy|monogamous]] and the pair bond is maintained through the year and sometimes in consecutive breeding seasons.<ref>Higgins & Davies (1996) 610–611</ref> The colony size is related to the abundance of [[pelagic fish]] prey,<ref name= COOPER/> and the largest documented colony, with 13,000 to 15,000 pairs, is in the [[Gulf of Carpentaria]] in northern Australia, a region which also supports major colonies of other seabirds. Since nesting in this area follows the summer [[monsoon]]al flooding, it is presumably a response to fish stocks rising, probably due to river run-off providing extra nutrient to the Gulf.<ref name= Walker>{{cite journal| doi= 10.1071/MU9920152| last= Walker | first= T. A. | year= 1992 | title= A record Crested Tern ''Sterna bergii'' colony and concentrated breeding by seabirds in the Gulf of Carpentaria | journal= Emu | volume=92 | issue=3 | pages=152–156 }}</ref> This tern does not show [[philopatry|site fidelity]], frequently changing its nest site from year to year,<ref name=BWP/> sometimes by more than {{convert|200|km|mi|abbr=on}}.<ref name= Carrick >{{cite journal| last= Carrick| first= R. |author2=Wheeler, W. R. |author3=Murray, M. D. | year=1957 | title= Seasonal dispersal and mortality in the Silver Gull, ''Larus novae-hollandiae'' Stephens, and Crested tern, ''Sterna bergii'' Lichstein, in Australia | journal= Wildlife Research | volume=2 |  pages=116–144 |doi=10.1071/CWR9570116| issue= 2}}</ref>
[[Image:Thailandsand Laem Pak Bia.jpg|Tropical sandy islands are used for breeding and roosting, but may be disturbed by human activities.|thumb|left]]
A male greater crested tern establishes a small area of the colony in preparation for nesting, and initially pecks at any other tern entering his territory. If the intruder is another male, it retaliates in kind, and is normally vigorously repelled by the incumbent. A female entering the nest area reacts passively to the male's aggression, enabling him to recognise her sex and initiate pair formation by display, including head raising and bowing; this behaviour is frequently repeated during nesting to reinforce the bond between the pair. Terns also use fish as part of the courtship ritual. One bird flies around the colony with a fish in its beak, calling loudly; its partner may also fly, but the pair eventually settle and the gift is exchanged.<ref name= Fisher >{{cite book| last = Fisher | first = James |author2=Lockley, R. M.  | title = Sea‑Birds (Collins [[New Naturalist]] series) | year = 1989 | publisher =  Bloomsbury Books |location = London  | isbn =1-870630-88-2 |pages = 155–156}}</ref>
[[File:Crested Tern breeding plumage.jpg|thumb|Breeding plumage in [[New South Wales]]]]
The nest is a shallow scrape in the sand on open, flat or occasionally sloping ground. It is often unlined, but sometimes includes stones or [[cuttlefish]] bones. One, sometimes two, eggs are laid and [[avian incubation|incubated]] by both parents for 25 to 30&nbsp;days prior to hatching.<ref name= COOPER/> The eggs are cream with blackish streaks.<ref name = Ausmus>{{cite web| title= Crested Tern | work=Fact Sheets | url= http://www.austmus.gov.au/factsheets/crested_tern.htm | publisher= Australian Museum  |accessdate=5 July 2008 }}</ref> Egg laying is synchronised within a breeding colony<ref name= Dunlop >{{cite journal| last= Dunlop | first= J. N. | year=1987 | title= Social-behavior and colony formation in a population of crested terns, ''Sterna bergii'', in southwestern Australia | journal= Australian Wildlife Research | volume= 14 | pages= 529–540.| doi= 10.1071/WR9870529| issue= 4}}</ref> and more tightly so within sub-colonies.<ref name=emu>{{cite journal|doi=10.1071/MU9860023|last=Langham|first=N.P.E.|author2=Hulsman, K|year=1986|title=The Breeding Biology of the Crested Tern ''Sterna bergii''|journal=Emu|volume=86|pages=23–32}}</ref> Parents do not recognize their own eggs or newly hatched chicks, but are able to distinguish their chicks by the time they are two days-old, shortly before they begin to wander from the nest.<ref name=Davies >{{cite journal| last= Davies | first= S. J. J. F. |author2=Carrick, R | year=1962 | title= On the ability of crested terns, ''Sterna bergii'', to recognize their own chicks | journal= Australian Journal of Zoology | volume= 10| pages=171–177 |  doi =10.1071/ZO9620171| issue= 2}}</ref> The [[precocial]] chicks, which are very pale with black speckling, are brooded and fed by both parents, but may gather in crèches when older. The young terns fledge after 38 to 40&nbsp;days, but remain dependent on the parents after leaving the colony until they are about four months old.<ref name= COOPER>Cooper (2006) 760–764</ref>

[[File:Thalasseus bergii by Gregg Yan 01.jpg|thumb|A nesting colony in [[Tubbataha Reef]], [[Philippines]]]]
[[File:Thalasseus bergii MWNH 0382.JPG|thumb|Egg, Collection [[Museum Wiesbaden]]]]

In South Africa, this species has adapted to breeding on the roofs of building, sometimes with [[Hartlaub's gull]], which also shares the more typical nesting sites of the nominate race. In 2000, 7.5%&nbsp;of the population of this subspecies bred on roofs.<ref name= Crawford>{{cite journal| last= Crawford | first= R. J. M.  |author2=Dyer B.| year=2000 | title= Swift Terns ''Sterna bergii'' breeding on roofs and at other new localities in southern Africa | journal= Marine Ornithology | volume= 28| issue= | pages=123–124 | url =http://www.marineornithology.org/PDF/28_2/28_2_7.pdf |format = PDF}}</ref> Artificial islands in salt pans and sewage works have also recently been colonised by this adaptable seabird.<ref name=HBW/>

Adult terns have few predators, but in Namibia immature birds are often robbed of their food by [[kelp gull]]s, and that species, along with Hartlaub's gull, [[silver gull]] and [[sacred ibis]], has been observed feeding on eggs or nestlings, especially when colonies are disturbed.<ref name =HBW/><ref name = leroux>{{cite web| author = le Roux, Janine | title= Swift Tern ''Sterna bergii''| work= | url= http://web.uct.ac.za/depts/stats/adu/species/sp324_00.htm | publisher= Avian Demography Unit, University of Cape Town  |accessdate=5 July 2008 }}</ref> Smaller subcolonies with a relatively larger numbers of nests located on the perimeter are subject to more predation.<ref name=emu/> In Australia, predation by cats and dogs, and occasional deaths by shooting or collisions with cars, wires or light-towers have been documented.<ref name= HANZAB/>
[[File:Sternabergiisouthafrica.jpg|thumb|left|Nominate subspecies roosting with [[Sandwich tern]]s in South Africa]]
Commercial fisheries can have both positive and negative effects on the greater crested tern. Juvenile survival rates are improved where trawler discards provide extra food, and huge population increases in the southeastern Gulf of Carpentaria are thought to have been due to the development of a large prawn trawl fishery.<ref name = Blaber/> Conversely, [[Seine fishing#Purse seine|purse-seine]] fishing reduces the available food supply, and sizeable fluctuations in the numbers of great crested terns breeding in the Western Cape of South Africa are significantly related to changes in the abundance of pelagic fish, which are intensively exploited by purse-seine fishing.<ref name = leroux/> Terns may be killed or injured by collisions with trawl warps, trapped in trawls or discarded gear, or hooked by [[longline fishing]], but, unlike [[albatross]]es and [[petrel]]s, there is little evidence that overall numbers are significantly affected.<ref name= COOPER/>

An unusual incident was the incapacitation of 103 terns off [[Robben Island]], South Africa by marine foam, generated by a combination of wave action, [[kelp]] [[mucilage]] and [[phytoplankton]]. After treatment, 90%&nbsp;of the birds were fit to be released.<ref name= Parsons >{{cite journal| last= Parsons | first= Nola J. |author2=Tjørve, Kathy M. C. |author3=Underhill, Les G. |author4=Strauss, Venessa |date=April 2006 | title= The rehabilitation of Swift Terns ''Sterna bergii'' incapacitated by marine foam on Robben Island, South Africa | journal= Ostrich: Journal of African Ornithology| volume=77 | issue=1–2 | pages=95–98| doi= 10.2989/00306520609485514}}</ref>

=== Feeding ===
[[Image:Pacific sardine002.jpg|thumb|[[Sardine]]s are a common food item.]]
Fish are the main food of the greater crested tern, found to make up nearly 90% of all prey items with the remainder including [[cephalopod]]s, [[crustacea]]ns and [[insect]]s.<ref name= COOPER/> Unusual [[vertebrate]] prey included [[Agamids|agamid lizards]] and [[green turtle]] hatchlings.<ref name= Blaber>{{cite journal| last= Blaber | first= S. J. M. |author2=Milton, D. A. |author3=Smith, G. C. |author4=Farmer, M. J. |date=November 1995 | title= Trawl discards in the diets of tropical seabirds of the northern Great Barrier Reef, Australia | journal= Marine Ecology Progress Series | volume=127 | issue= | pages=1–13 | url = http://www.int-res.com/articles/meps/127/m127p001.pdf | format = PDF| doi = 10.3354/meps127001 }}</ref>

The great crested tern feeds mostly at sea by plunge diving to a depth of up to {{convert|1|m|ft|abbr=on}}, or by dipping from the surface, and food is usually swallowed in mid-air. Birds may forage up to {{convert|10|km|mi|abbr=on}} from land in the breeding season. Prey size ranges from {{convert|7|–|138|mm|in|abbr=on}} in length and up to {{convert|30|g|oz|abbr=on}} in weight. Shoaling [[pelagic]] fish such as [[anchovy]] and [[sardine]] are typical prey,<ref name= COOPER/> but bottom-living species are taken as discards from commercial fishing. This tern actively follows trawlers, including at night, and during the fishing season trawl discards can constitute 70% of its diet.<ref name= Blaber/> Prawn fishing is particularly productive in providing extra food, since prawns usually represent only 10–20% of the catch, the remaining being bycatch, mainly fish such as [[Apogonidae|cardinalfish]] and [[Goby|gobies]].<ref name= Blaber/>
[[File:Shrimp bycatch.jpg|thumb|left|Bycatch from [[prawn]] fishing can provide extra food]]
A study of an area of the [[Great Barrier Reef]] where the number of breeding great crested terns has grown ten-fold, probably due to extra food from trawl [[bycatch|by-catch]], suggested that lesser crested and [[sooty tern]]s have moved away and now breed on a part of the reef where fishing is banned. It is possible that the large increase in the number of greater crested terns may have affected other species through competition for food and nesting sites.<ref name= Blaber2 >{{cite journal| last= Blaber | first= S. J. M. |author2=Milton, D. A. |author3=Farmer, M. J. |author4=Smith, G. C. | year=1998 | title= Seabird breeding populations on the far northern Great Barrier Reef, Australia: trends and influences | journal= Emu | volume=98 | pages=44–57| doi= 10.1071/MU98006 }}</ref>

Terns have red oil droplets in the [[cone cell]]s of the [[retina]]s of their eyes. This improves contrast and sharpens distance [[bird vision|vision]], especially in hazy conditions.<ref name= Sinclair>{{cite book| last = Sinclair | first = Sandra |  title = How Animals See: Other Visions of Our World | year =  1985| publisher =Croom Helm |location = Beckenham, Kent  | isbn =0-7099-3336-3 |pages = 93–95}}</ref> Birds that have to see through an air/water interface, such as terns and gulls, have more strongly coloured [[carotenoid]] [[pigment]]s in the cone oil drops than other avian species.<ref name = Varela>Varela, F. J.; Palacios, A. G.; Goldsmith T. M. (1993) "Vision, Brain, and Behavior in Birds"  in Harris, Philip; Bischof, Hans-Joachim ''Vision, Brain, and Behavior in Birds: a comparative review'' Cambridge, Massachusetts: MIT Press 77–94 ISBN 0-262-24036-X</ref> The improved eyesight helps terns to locate shoals of fish, although it is uncertain whether they are sighting the [[phytoplankton]] on which the fish feed, or observing other terns diving for food.<ref name= Lythgoe>{{cite book| last = Lythgoe | first = J. N.| title =The Ecology of Vision | year =1979  | publisher =Clarendon Press |location = Oxford| isbn = 0-19-854529-0 |pages =180–183}}</ref> Tern's eyes are not particularly [[ultraviolet]] sensitive, an adaptation more suited to terrestrial feeders like the gulls.<ref name= Hastad>{{cite journal| last= Håstad | first= Olle |author2=Ernstdotter, Emma |author3=Ödeen, Anders |  title= Ultraviolet vision and foraging in dip and plunge diving birds | journal= Biology Letters |volume=1 |issue=3 |pages=306–309|date=September 2005 |pmid=  17148194|doi= 10.1098/rsbl.2005.0320| pmc= 1617148}}</ref>

== Status ==
The greater crested tern has a widespread distribution range, estimated at 1–10&nbsp;million square kilometres (0.4–3.8&nbsp;million square miles). The population has not been quantified, but it is not believed to approach the thresholds for either the size criterion (fewer than 10,000 mature individuals) or the population decline criterion (declining more than 30%&nbsp;in ten years or three generations) of the [[IUCN Red List]]. For these reasons, the species is evaluated as being of Least Concern at the global level.<ref name=IUCN/> However, there are concerns for populations in some areas such as the [[Gulf of Thailand]] where the species no longer breeds, and in [[Indonesia]] where egg harvesting has caused declines.<ref name =HBW/>

All subspecies except ''T. b. cristata'' are covered under the ''Agreement on the Conservation of African-Eurasian Migratory Waterbirds'' ([[AEWA]]).<ref name=AEWA>{{cite web|title=Annex 2: Waterbird species to which the Agreement applies |work=Agreement on the conservation of African-Eurasian migratory Waterbirds (AEWA) |url=http://www.unep-aewa.org/documents/agreement_text/eng/pdf/aewa_agreement_text_annex2.pdf |format=PDF |publisher=UNEP/ AEWA Secretariat |accessdate=4 July 2008 |deadurl=yes |archiveurl=http://www.webcitation.org/614hdkeBw?url=http://www.unep-aewa.org/documents/agreement_text/eng/pdf/aewa_agreement_text_annex2.pdf |archivedate=20 August 2011 |df= }}</ref> Parties to the Agreement are required to engage in a wide range of conservation strategies described in a detailed action plan. The plan is intended to address key issues such as species and habitat conservation, management of human activities, research, education, and implementation.<ref name=AEWAintro>{{cite web|title=Introduction |work=African-Eurasian Waterbird Agreement |url=http://www.unep-aewa.org/about/introduction.htm |publisher=UNEP/ AEWA Secretariat |accessdate=4 July 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20120211082252/http://www.unep-aewa.org/about/introduction.htm |archivedate=11 February 2012 |df= }}</ref>

== Footnotes ==
{{reflist|30em}}

== References ==<!-- Micronesica32:257. -->
* Cooper, John (2006) "[http://www.jncc.gov.uk/PDF/pub07_waterbirds_part5.5.8.pdf Potential impacts of marine fisheries on migratory waterbirds of the Afrotropical Region: a study in progress]" (PDF extract) in Boere, C.A.; Galbraith G.C.; Stroud D.A. (eds) ''Waterbirds around the world.'' Edinburgh: The Stationery Office, ISBN 0-11-497333-4
* {{cite book| editor1-last = Del Hoyo | editor1-first = Josep |editor2=Elliott, Andrew|editor3=Sargatal, Jordi | title = [[Handbook of the Birds of the World]]: Hoatzin to Auks v. 3  | year =1996  | publisher = Lynx Edicions | location = Barcelona | isbn = 84-87334-20-2}}
* {{cite book| last = Harrison | first = Peter | title = Seabirds | year = 1988 | publisher = Christopher Helm|location = London | isbn = 0-7470-1410-8}}
* {{cite book| editor1-last = Higgins | editor1-first = P. J. | editor2 = Davies, S. J. J. F.| title = [[Handbook of Australian, New Zealand and Antarctic Birds]]: Volume 3: Snipe to Pigeons | year = 1996 | publisher = [[Oxford University Press]]|location = Melbourne | isbn = 0-19-553070-5}}
* {{cite book| last = Olsen | first = Klaus Malling |author2=Larsson, Hans | title = Terns of Europe and North America | year =1995  | publisher = Christopher Helm |location = London| isbn =0-7136-4056-1}}
* {{cite book | editor1-last = Snow   | editor1-first =  David  |editor2= Perrins, Christopher M.| title = [[The Birds of the Western Palearctic]] (BWP) concise edition (2 volumes) | publisher = Oxford University Press |year = 1998| location =Oxford | isbn = 0-19-854099-X }}

== External links ==
{{commons|Sterna bergii}}
{{wikispecies|Sterna bergii}}
* (Greater crested tern = ) Swift tern - [http://sabap2.adu.org.za/docs/sabap1/324.pdf Species text in The Atlas of Southern African Birds]

{{featured article}}
{{portalbar|Birds|Animals}}
{{taxonbar}}
{{Authority control}}

{{DEFAULTSORT:tern, crested, greater}}
[[Category:Thalasseus|greater crested tern]]
[[Category:Birds of Africa]]
[[Category:Birds of Asia]]
[[Category:Birds of Oceania]]
[[Category:Birds of the Indian Ocean]]
[[Category:Birds described in 1823|greater crested tern]]