<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Superlite
 | image=Belite Trike.jpg
 | caption=A Belite Trike
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[United States of America]]
 | manufacturer=[[Belite Aircraft]]
 | designer=[[James Wiebe]]
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=10 (2011)
 | program cost= <!--Total program cost-->
 | unit cost=$11,200 in 2011 for kit minus engine<!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Denney Kitfox|Kitfox Lite]]
 | variants with their own articles=
}}
|}
[[File:Belite-Superlite.jpg|thumb|right|Belite Superlite]]
The '''Belite Superlite''' is a single-seat, [[high-wing]], single-engine [[ultralight aircraft]] developed from the [[Kitfox Lite]] aircraft especially for the United States [[FAR 103 Ultralight Vehicles]] category.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 32. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 34. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref><ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, pages 45-46. Belvoir Publications. ISSN 0891-1851</ref>

==Design and development==
Designer James Wiebe bought the assets and tooling of the [[Kitfox Lite]] from Skystar. He modified the prototype Kitfox Lite to meet FAR 103 regulations requiring an ultralight aircraft to have an empty weight of less than {{convert|254|lb|kg|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA15"/><ref name="ReferenceA">{{cite journal|magazine=EAA Sport Pilot & Light Sport Aircraft|date=September 2009}}</ref>

The fuselage is made from [[4130 steel]] tubing. [[Flaperon]]s and [[vortex generator]]s are used for roll control and low speed flight. The wings are foldable for storage.<ref name="WDLA11" /><ref name="WDLA15"/>

Items were substituted with [[carbon-fiber-reinforced polymer]] to make the aircraft lighter than a Kitfox Lite. This included the tailwheel leaf spring, wing spars, wing ribs (aluminum on later kits), [[lift strut]]s, firewall, [[Elevator (aircraft)|elevator]] and fuel tank.<ref name="ReferenceA"/> A variety of engines may be used such as the [[Hirth F33]], [[Hirth F-23]], [[Zanzottera MZ 34]], 1/2 [[Volkswagen air-cooled engine]] and the [[Zanzottera MZ 201]].<ref name="WDLA11" /><ref name="WDLA15"/>

==Variants==
;254
:The basic ultralight fuselage design, for powerplants of {{convert|28|to|45|hp|kW|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA15"/><ref>{{cite journal|magazine=Kitplanes|date=April 2011}}</ref>
;Superlite
:Maximum weight reduction fuselage for larger engines of {{convert|50|hp|kW|0|abbr=on}}, with an empty weight of {{convert|278|lb|kg|0|abbr=on}} when equipped with the [[Hirth F-23]] engine of {{convert|50|hp|kW|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA15"/><ref name="KitplanesDec2011" />
;Trike
:A tricycle gear version of the Superlite, with an empty weight of {{convert|254|lb|kg|0|abbr=on}} when equipped with the [[Hirth F-33]] engine of {{convert|30|hp|kW|0|abbr=on}}.<ref name="WDLA11" /><ref name="KitplanesDec2011" />

==Specifications (Superlite) ==
{{Aircraft specs
|ref=Sport Aviation<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=1
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=25
|span in=2
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=254
|empty weight note=
|gross weight kg=
|gross weight lb=550
|gross weight note=
|fuel capacity={{convert|5|gal}}
|more general=*'''[[Ballistic parachute]]:''' Second Chantz system
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Compact Radial MZ-201
|eng1 type=two stroke
|eng1 kw=<!-- prop engines -->
|eng1 hp=45<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=55
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=28<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=400
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=5.56
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=*Belite Electronics lightweight electronic gauges
}}

==Popular culture==<!--Renamed from operational history-->
A Belite aircraft was used in the show [[List of MythBusters episodes|Mythbusters Episode 174 – Duct Tape Plane]].  A Belite was "mauled" by an artificial bear claw with the damage being limited to the [[Aircraft fabric covering|fabric skin]] of the rear fuselage and [[vertical stabilizer]]. The [[Flight control surfaces|control surfaces]] were not damaged during the destruction. The aircraft was then repaired with [[Duct tape|Duct-Tape]] and successfully flown.<ref>{{cite web|title=Not a Myth: Duct Tape-Covered Plane Flies
|url=http://www.youngeagles.org/news/2011%20-%2010_20%20-%20Not%20a%20Myth_%20Duct%20Tape-Covered%20Plane%20Flies.asp|accessdate=28 October 2011}}</ref>

Note: This is not to be confused with [[Speed tape]].

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{Commons category|Belite Aircraft Superlite}}
*{{Official website|http://www.beliteaircraft.com/}}
{{Belite Aircraft}}

[[Category:Homebuilt aircraft]]