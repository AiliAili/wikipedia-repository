{{Infobox amusement park
| name            = Ferrari World Abu Dhabi
| image           =
| caption         = Ferrari World Abu Dhabi
| resort          = 
| location        = [[Abu Dhabi]], United Arab Emirates
| location2       = 
| location3       = 
| coordinates     = <!-- {{Coord|LAT|LON|type:landmark||display=inline,title}} -->
| theme           = [[Ferrari]]
| homepage        = [http://www.ferrariworldabudhabi.com/]
| general_manager = 
| operator        = Farah Experiences
| opening_date    = <!-- {{Start date|2010|11|04}} -->
| closing_date    = <!-- {{End date|YYYY|MM|DD}} -->
| previous_names  = 
| season          = 
| visitors        = 
| area            = 86,000 sq m
| rides           = 
| coasters        = 
| water_rides     = 
| slogan          = 
| footnotes       = 
}}

'''Ferrari World Abu Dhabi'''  is an [[amusement park]] located on [[Yas Island]] in [[Abu Dhabi]], United Arab Emirates. It is the first [[Ferrari]]-branded theme park and has the record for the largest [[Space Frame|space frame structure]] ever built.<ref name="Ferrari World Abu Dhabi">{{cite web|title=About Ferrari World Abu Dhabi|url=https://www.ferrariworldabudhabi.com/park-overview/|publisher=Ferrari World|accessdate=25 April 2016|author=Ferrari World Abu Dhabi}}</ref> [[Formula Rossa]], the world's fastest roller coaster, is also located here.<ref>{{cite web|url=https://www.ferrariworldabudhabi.com/en-gb/attractions/formula-rossa.aspx |title=Formula Rossa &#124; World's Fastest roller coaster |publisher=Ferrari World Abu Dhabi |date=2015-07-04 |accessdate=2015-11-28}}</ref>

The foundation stone for the park was laid on 3 November 2007. It took three years to develop the park and it officially opened to the public on 4 November 2010. Ferrari World Abu Dhabi covers an area of 86,000 square metres. Ferrari World Abu Dhabi was named the "Middle East's Leading Tourist Attraction" at the World Travel Awards 2015.

==Rides and attractions==

===Operating===

====Roller coasters====
{|class="wikitable"
|-
! Name !! Opened !! Manufacturer !! Type !! Notes
|-
| [[Fiorano GT Challenge]] || 2010 || [[Maurer Söhne]] || Launched, Dueling || R-Coaster prototype.
|-
| [[Formula Rossa]] || 2010 || [[Intamin]] || Launched || It is the fastest roller coaster since 2010.
|-
| Mission Ferrari || 2017 || [[Dynamic Attractions]] || SFX Coaster || SFX Coaster prototype.
|-
| Flying Aces || 2016 || [[Intamin]] || Launched, Wing Coaster || Known for having the world's tallest non-inverting loop.
|-
| Turbo Track || 2017 || [[Intamin]] || Shuttle || Will be the park's 5th coaster.
|}

====Thrill rides====
{|class="wikitable"
|-
! Name
! Type
|-
| Karting Academy
| Go-karts
|-
| Scuderia Challenge
| Interactive motion simulator
|- 
| V12 Enter the Engine
| Log Flume
|}

====Family rides====
{|class="wikitable"
|-
! Name
! Type
|-
| Driving Experience
| Drive a Ferrari
|-
| Tyre Twist
| Teacup ride
|-
| Viaggio in Italia
| Flight simulator
|-
| Speed of Magic
| 4-D ride
|-
| Made in Maranello
| Tour of the historic Ferrari factory
|-
| Galleria Ferrari
| Interactive historical gallery
|-
| Driving with the Champion
| F1™ & GT virtual simulation
|-
| Fast Lane
| Interactive game show
|-
| Bell'Italia
| Tour of miniature Italy 
|}

====Children's rides====
{|class="wikitable"
|-
! Name
! Type
|-
|Junior GT
|Junior Pilota's driving school
|-
|Junior GP
|Junior Pilota's racing school
|-
|Khalil's Car Wash
|Children’s interactive play area
|-
|Junior Training Camp
|Climbing area
|-
|Benno’s Great Race
|Interactive dark ride
|}

====Shows====
{|class="wikitable"
|-
! Name
! Type
|-
|Cinema Maranello
|Film featuring [[Enzo Ferrari]]
|-
|RED
|Live performance with dancing, BMX biking, mega wheels and acrobatics
|-
|Tyre Change Show
|Two mechanics whose lifelong dream is to be pit crew members for Scuderia Ferrari
|-
|Viva Ferrari
|3D animations, performance arts and optical illusions
|}

==Awards and nominations==

* Best Commercial, Office, Retail Future Development, Cityscape Abu Dhabi, 2010.<ref>{{cite web|url=http://www.cityscapeabudhabi.com/Awards/2010-Awards-Winners/ |title=THE HOME OF REAL ESTATE INVESTMENT - 2010 Awards Winners |publisher=Cityscapeabudhabi.com |date= |accessdate=2015-11-28}}</ref>
* Best International Leisure Development, International Property Award and Bloomberg, 2010.<ref>{{cite web|url=http://propertyawards.net/ |title=The International Property Awards – The Property Industry's most prestigious awards programme |publisher=Propertyawards.net |date=2014-06-20 |accessdate=2015-11-28}}</ref>
* CIAT Open Award for Technical Excellence in Architectural Technology, The Chartered Institute of Architectural Technologists (CIAT), 2010.<ref>{{cite web|url=http://www.ciat.org.uk/en/media_centre/news_and_events/index.cfm/techex#.VaECj_mUOf4 |title=Ferrari World races ahead to win CIAT's Award for Technical Excellence - News |publisher=Ciat.org.uk |date=2010-11-01 |accessdate=2015-11-28}}</ref>
* Overall GCC Project of the Year 2010, MEP Middle East Awards, 2010.<ref>{{cite web|author=Gerhard Hope |url=http://www.constructionweekonline.com/article-10444-mep-awards-ferrari-world-is-project-of-the-year/ |title=MEP Awards: Ferrari World is Project of the Year |publisher=ConstructionWeekOnline.com |date=2010-12-13 |accessdate=2015-11-28}}</ref>
* 2011 Brass Ring (Best new product, show production and entertainment displays and sets), IAAPA, 2011.<ref>{{cite web|url=http://www.iaapa.org/home |title=The International Association of Amusement Parks and Attractions |publisher=IAAPA |date= |accessdate=2015-11-28}}</ref>
* Excellence in Live Design Award (ShowTex Middle East for Ferrari World Grand Opening), Live Design Magazine, 2011.<ref>[http://www.showtex.com/files/library/Stage_and_Event_Report_07.pdf ]{{dead link|date=November 2015}}</ref>
* Gold Winner designation for Best Permanent Exhibition, Event Design Award by Event Design Magazine, 2011.<ref>{{cite web|url=http://www.jackrouse.com/2011/07/JRA-Wins-International-Design-Award-for-Ferrari-World.cfm |title=Jack Rouse Associates: Blog - JRA Wins International Design Award for Ferrari World |publisher=Jackrouse.com |date=2011-07-07 |accessdate=2015-11-28}}</ref>
* Hospitality & Leisure Project of the Year, Middle East Architect Awards 2011, 2011.<ref>[http://www.benoy.com/press/ferrari-world-abu-dhabi-scoops-major-industry-award ]{{dead link|date=November 2015}}</ref>
* Most Innovative Retail & Leisure Concept of the Year Award, Global Retail and Leisure International Awards, 2011.<ref>{{cite web|url=http://www.benoy.com/awards |title=Awards |publisher=Benoy |date= |accessdate=2015-11-28}}</ref>
* PALME award - Best A/V solution in a commercial development, PALME Middle East, 2011.<ref name="palme-middleeast.com">{{cite web|url=http://www.palme-middleeast.com/en/Palme/Home/ |title=Palme - Home |publisher=Palme-middleeast.com |date=2012-05-03 |accessdate=2015-11-28}}</ref>
* PALME award - Best 3D projection of the year (Speed of magic), PALME Middle East, 2011.<ref name="palme-middleeast.com"/>
* Project Excellence Award, IPMA – NL, 2011.<ref>{{cite web|url=http://www.gielissen.com/data/news/ipma-award-2011-jpg.jpg|format=JPG|title=2011 Project Excellence Awards|publisher=Gielissen.com|accessdate=2015-11-28}}</ref>
* Best Theme Park in the UAE, Hospitality India, 2013.<ref>{{cite web|url=http://indiahospitalityawards.in/ |title=India Hospitality Awards |publisher=Indiahospitalityawards.in |date= |accessdate=2015-11-28}}</ref>
* Middle East's Leading Tourist Attraction 2015, World Travel Awards, 2015.<ref>{{cite web|url=http://www.worldtravelawards.com/ |title=World Travel Awards |publisher=World Travel Awards |date=2015-10-17 |accessdate=2015-11-28}}</ref>

==References==
{{Reflist|colwidth=30em}}

==External links==
* {{official website|https://ferrariworldabudhabi.com/}}

{{Ferrari World Attractions}}

[[Category:Amusement parks in the United Arab Emirates]]
[[Category:2010 establishments in the United Arab Emirates]]
[[Category:Ferrari]]
[[Category:Amusement parks opened in 2010]]