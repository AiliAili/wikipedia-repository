{{Infobox Mayor
| name=Astamur Anton-ipa Adleiba<br/>Асҭамыр Антон-иҧа Адлеиба<br/>ასტამურ ადლეიბა
| image=
| imagesize=170px
| order=[[Mayor of Sukhumi]]
| term_start=16 February 2005
| term_end=2 May 2007
| president=
| predecessor=[[Adgur Kharazia]]
| successor=[[Anzor Kortua]] (acting)
| birth_date=1960
| birth_place=[[Sukhumi]], [[Abkhazian ASSR]], [[Georgian SSR]], [[USSR]]
| party=
| religion=
| spouse=
}}

'''Astamur Anton-ipa Adleiba''' ({{lang-ab|''Асҭамыр Антон-иҧа Адлеиба''}}, {{lang-ka|''ასტამურ ადლეიბა''}}) is a former Minister of Youth, Sports, Tourism and Resorts of [[Abkhazia]] and a former mayor of Abkhazia's capital [[Sukhumi]]. He was dismissed from this last position for corruption.

==Early life and career==

Astamur Adleiba was born in 1960 in [[Sukhumi]]. From 1977 until 1984 he was a student of the [[Georgian State University of Subtropical Agriculture]] in Sukhumi. From 1979 until 1981 Adleiba was engaged in community service for the [[Soviet army]] and in 1981-1982 he was an instructor with the Sukhumi city committee of the [[Komsomol]]. From 1982 until 1984 Adleiba served as an agronomist in the ministry of agriculture of the [[Abkhazian ASSR]] and from 1984 until 1986 he served as an agronomist in the state committee for agricultural production of the autonomous republic. From 1986 until 1989 Adleiba was the chief agronomist in the [[Kindga]] poultry farms and from 1989 until 1992 he was the vice-president of the council for collective farms in the Abkhazian ASSR.

After the [[War in Abkhazia (1992-1993)|1992-1993 war with Georgia]], from 1993 until 2002 Adleiba was the director of the tourist-hotel Aytar. In December 2002 he was appointed Minister of Youth, Sports, Tourism and Resorts by President [[Vladislav Ardzinba]].<ref>{{cite news|url=http://www.regnum.ru/news/408745.html&tbb=1 |title=Астамур Адлейба назначен и.о. мэра столицы Абхазии |publisher=REGNUM |date=February 17, 2005 |accessdate=2008-07-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20110521103323/http://www.regnum.ru/news/408745.html&tbb=1 |archivedate=2011-05-21 |df= }}</ref>

==Mayor of Sukhumi==
After coming to power, the new President [[Sergei Bagapsh]] made Astamur Adleiba mayor of Sukhumi on 16 February 2005, replacing [[Adgur Kharazia]], who had been appointed only 4 months before by outgoing president Ardzinba.<ref>{{cite news|url=http://www.abkhaziagov.org/ru/president/activities/decree/detail.php?ID=1498 |title=Указ Президента Абхазии №5 от 16.02.2005 |publisher=Администрация Президента Республики Абхазия |date=February 16, 2005 |accessdate=2008-07-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20110721061501/http://www.abkhaziagov.org/ru/president/activities/decree/detail.php?ID=1498 |archivedate=2011-07-21 |df= }}</ref>
In the 11 February 2007 local elections, Adleiba successfully defended his seat in the Sukhumi city assembly and was thereupon reappointed mayor by Bagapsh on 20 March .<ref>{{cite news|url=http://www.apsnypress.info/news2007/march/20.htm |title=Президент Сергей Багапш подписал указы о назначении глав городских и районных администраций |publisher=[[Apsnypress|Апсныпресс]] |date=March 20, 2007 |accessdate=2008-07-01 |deadurl=yes |archiveurl=https://web.archive.org/web/20071026093918/http://www.apsnypress.info/news2007/march/20.htm |archivedate=2007-10-26 |df= }}</ref>

In April 2007, while President Bagapsh was in Moscow for medical treatment, the results of an investigation into corruption within the Sukhumi city administration were made public. The investigation found that large sums had been embezzled and upon his return, on 2 May, Bagapsh fired Adleiba along with his deputy [[Boris Achba]], the head of the Sukhumi's finance department [[Konstantin Tuzhba]] and the head of the housing department [[David Jinjolia]].<ref>{{cite news|url=http://www.iwpr.net/?p=crs&s=f&o=335985&apc_state=henicrs2007|title=Abkhazia's anti-corruption drive|publisher=[[Institute for War & Peace Reporting]]|date=March 20, 2007|accessdate=2008-07-01}}</ref>

On 4 June Adleiba paid back to the municipal budget 200,000 rubels.<ref>{{cite news|url=http://www.regnum.ru/news/838958.html&tbb=1 |title=Экс-мэр Сухуми вернул в бюджет двести тысяч рублей |publisher=REGNUM |date=June 5, 2007 |accessdate=2008-07-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20110521103339/http://www.regnum.ru/news/838958.html&tbb=1 |archivedate=2011-05-21 |df= }}</ref> On 23 July, Adleiba resigned from the Sukhumi city council, citing health reasons and the need to travel abroad for medical treatment.<ref>{{cite news|url=http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=7220 |title=Экс-мэр Сухума намерен покинуть Столичное городское Собрание |publisher=Администрация Президента Республики Абхазия |date=July 23, 2007 |accessdate=2008-07-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20110721061529/http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=7220 |archivedate=2011-07-21 |df= }}</ref>

==References==
{{reflist|2}}

{{s-start}}
{{succession box | before=[[Adgur Kharazia]] | title=[[Mayor of Sukhumi]] | years=2005&ndash;2007 | after=[[Anzor Kortua]] (acting)}}
{{s-end}}

{{DEFAULTSORT:Adleiba, Astamur}}
[[Category:1960 births]]
[[Category:Living people]]
[[Category:People from Sukhumi]]
[[Category:Mayors of Sukhumi]]
[[Category:Ministers for Youth Affairs, Sport, Resorts and Tourism of Abkhazia]]
[[Category:4th convocation of the Sukhumi City Council]]
[[Category:3rd convocation of the Sukhumi City Council]]