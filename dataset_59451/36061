{{Use dmy dates|date=October 2013}}
{{Use British English|date=October 2013}}
{{good article}}
{{Infobox single
| Name           = Alfie
| Cover          = LilyA_AlfieEPJapanOnly.jpg
| Artist         = [[Lily Allen]]
| Border         = yes
| Album          = [[Alright, Still]]
| A-side         = "[[Shame for You]]"
| Released       = 5 March 2007 <small>([[United Kingdom|UK]])</small><br>11 July 2007 <small>([[Japan]])</small>
| Format         = [[CD single]], [[music download|digital download]], [[gramophone record|7"]]
| Recorded       = 
| Genre          = [[Ska]]
| Length         = 2:46
| Label          = [[Regal Recordings|Regal]]<br>[[Festival Mushroom Records]]<br>[[Warner Bros.]]
| Writer         = Lily Allen, [[Greg Kurstin]]
| Producer       = Greg Kurstin
| Certification  = 
| Last single    = "[[Littlest Things]]"<br>(2006)
| This single    = "[[Shame for You]]"/"'''Alfie'''"<br>(2007)
| Next single    = "[[Oh My God (Kaiser Chiefs song)#Mark Ronson version|Oh My God]]"<br>(2007)
}}
"'''Alfie'''" is a song by British recording artist [[Lily Allen]] from her debut studio album, ''[[Alright, Still]]'' (2006). Written by Allen and [[Greg Kurstin]], the song was released as the fourth and final [[single (music)|single]] from the album, on 5 March 2007, by [[Regal Recordings]]. In the United Kingdom, it was marketed as a [[A-side and B-side|double A-side]] single, along with "[[Shame for You]]". While the melody incorporates a sample of [[Sandie Shaw]]'s "[[Puppet on a String]]", the lyrics directly describe Allen's real life younger brother, actor [[Alfie Allen]], criticising him for his lazy behaviour.

Contemporary critics gave the song mixed reviews, some complimenting the production, while others considered it fell flat as the album's last song. The single peaked inside the top 20 of the charts in New Zealand and on the [[UK Singles Chart]], where it became her third single to do so. The accompanying [[music video]] portrayed Allen's brother as a puppet while the storyline follows the lyrical meaning of the song. The song was performed live by Allen during her 2007 concert tour, as part of the encore.

==Background==
Musically, the song is set in [[common time]] and has a tempo of 120 [[beats per minute]].<ref name="notes">{{cite web|url=http://www.musicnotes.com/sheetmusic/scorch.asp?ppn=SC0029278|title=Lily Allen Sheet Music|work=Music Notes|publisher=[[Alfred Music Publishing]]|accessdate=2009-09-03}}</ref> While it is played in the key of [[C major]] and has backing piano and guitar sounds,<ref name="notes"/> "Alfie" also uses a sample from [[Sandie Shaw]]'s [[Eurovision Song Contest 1967|1967 Eurovision Song Contest]] winning song "[[Puppet on a String]]".<ref name="twm">{{cite web|url=http://www.twowaymonologues.com/reviews/lily-allen/alright-still/|first=Ronan|last=Hunt-Murphy|title=Lily Allen - Alright, Still - Indie Music Review|publisher=Two Way Monologues|date=2006-07-21|accessdate=2009-09-05}}{{dead link|date=September 2016|bot=medic}}{{cbignore|bot=medic}}</ref> In an interview with [[Pitchfork Media]], Allen stated that the song's lyrics describe her younger brother, [[Alfie Allen]]. She further explained:<blockquote>When I wrote "Alfie", nobody really knew who I was. At first he was really upset about it, because he thought that I was just pointing out all of his bad points and attacking him. I thought it was really flattering [Laughs]. I thought he'd be really, really happy because it proved to him how much I loved him, that I care about him, and I want him to do something with his life. I suppose his paranoia — induced by smoking so much weed — made him think, "Why are you trying to be mean?"<ref>{{cite web|url=http://pitchfork.com/features/interviews/6476-lily-allen/|publisher=[[Pitchfork Media]]|first= Scott|last= Plagenoef|date=2006-11-06|accessdate=2009-09-01|title=Interview:Lily Allen| archiveurl= https://web.archive.org/web/20091008085953/http://pitchfork.com/features/interviews/6476-lily-allen/| archivedate= 8 October 2009 <!--DASHBot-->| deadurl= no}}</ref></blockquote>
In the song, she tries to persuade him to emerge from his room and stop wasting his life away, while complaining that he is spending too much time stuck in his room smoking [[Cannabis (drug)|weed]], watching television and playing computer games.<ref name="facts"/> Alfie Allen treated the song with good humour, but declared that his sister was also lazy when she was pregnant: "She's at home in bed and everyone is running around after her. Mind you, you have to look after Lily even if she isn't pregnant."<ref name="facts">{{cite web|url=http://www.songfacts.com/detail.php?id=8214|publisher=Songfacts|title=Alfie by Lily Allen Songfacts|accessdate=2009-09-05}}</ref>

The cover of the single released in the United Kingdom is different from the EP one used in Japan, as it also shows "Shame for You" written on it.<ref name="ukcover"/> "Alfie" was performed live, as part of the setlist of Allen's 2007 [[concert tour]].<ref>{{cite web|url=http://www.nme.com/reviews/lily-allen/8073|work=[[NME]]|publisher=[[IPC Media]]|title=Lily Allen/New Young Pony Club: Leadmill, Sheffield, Saturday, October 28|first=Alex|last=Miller|accessdate=2009-09-03}}</ref>

==Critical and commercial reception==
{{listen
 |pos=right
 | filename     = Lily_Allen_-_Alfie.ogg
 | title        = "Alfie"
 | description  = A 27 second sample of "Alfie", in which Allen sings the chorus.
 | format       = [[Ogg]]
}}
Rosie Swash from ''[[The Guardian]]'' described the song's sound as "fairground pomp". She then suggested that Allen has fused together a "uniquely acidic brand of pop", with the icing on the cake being "that brutally barbed tongue".<ref>{{cite web|work=[[The Guardian]]|publisher=[[Guardian Media Group]]|first=Rosie|last=Swash|url=https://www.theguardian.com/music/2006/jul/16/22|title=CD: Lily Allen -Alright, Still|date=2006-07-16|accessdate=2009-09-04 | location=London}}</ref> Rob Webb from [[Drowned in Sound]] considered that "Alfie" rescued the lack of wit and imagination of some previous tracks on the album,<ref>{{cite web|first=Rob|last=Webb|publisher=Silentway|work=[[Drowned in Sound]]|url=http://drownedinsound.com/releases/7691/reviews/999875-lily-allen-alright-still|title=Alright, Still Review|date=2006-07-18|accessdate=2009-09-04}}</ref> while ''[[Blender (magazine)|Blender]]'' reporter Jon Dolan thought that the "silly waltz-rap" of the song is an example at how Allen "casually and wittily blurs the line between fan and friend".<ref>{{cite web|url=http://www.blender.com/guide/new/54394/alright-still.html |date=2007-01-30 |title=Alright, Still - Blender |work=[[Blender (magazine)|Blender]] |publisher=Alpha Media Group |first=Jon |last=Dolan |accessdate=2009-09-04 |deadurl=yes }}</ref>
Sal Cinquemani from [[Slant Magazine]] praised the production of "Alfie", as well as "Shame for You", with "plenty of catchy melodies and clever samples", but claimed that Allen lacks charisma.<ref>{{cite web|first=Sal |last=Cinquemani |publisher=[[Slant Magazine]] |url=http://www.slantmagazine.com/music/music_review.asp?ID=1034 |title=Lily Allen: Alright, Still - Musical Review |date=2006-12-28 |accessdate=2009-09-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20090309000348/http://slantmagazine.com:80/music/music_review.asp?ID=1034 |archivedate=9 March 2009 |df=dmy }}</ref> Heather Phares of [[Allmusic]] gave a negative review for the song, claiming it "falls especially flat as the album's final song", with the album losing stream at the end,<ref>{{cite web|url={{Allmusic|class=album|id=r1020486|pure_url=yes}}|first=Heather|last=Phares|title=allmusic ((( Alright, Still > Overview )))|accessdate=2009-09-04|work=[[allmusic]]|publisher=[[Rovi Corporation|Macrovision]]}}</ref> and [[NME]]'s Priya Elan considered that if "the pop dreams get slightly tarnished by the graffiti put-downs of 'Not Big' and 'Alfie', then that's not too worrying, as with a personality this size, this isn’t the last time you’ll be hearing from [Allen]".<ref>{{cite web|first=Priya|last=Elan|title=Lily Allen: Alright, Still -Album Reviews|work=[[NME]]|publisher=[[IPC Media]]|url=http://www.nme.com/reviews/lily-allen/7978|date=2006-07-17|accessdate=2009-09-04}}</ref> It was considered by reviewer Ronan Hunt-Murphy from Two Way Monologues that the song could be used for a children's TV show, if the swearing and drug references were cut.<ref name="twm"/>

The single failed to achieve commercial success. It debuted at 44 and peaked inside the top 20 of the [[UK Singles Chart]], at number 15 for only one week, where it charted as the double A-sided single, "Alfie"/"Shame for You".<ref name="uk"/> Other moderate chart peaks include Ireland, at 31, and New Zealand at 15.<ref name ="acharts"/> In Japan, "Alfie" was only released as an [[extended play]], made exclusively for that country,<ref name="ep"/> which was ineligible to chart on the Singles Chart, so it peaked on the [[Oricon|Albums Chart]] at 226.<ref name="jp"/> In Europe, it reached position 55 on the [[Eurochart Hot 100 Singles]] compiled by ''[[Billboard (magazine)|Billboard]]''.<ref name="euro"/>

==Music video==
[[File:Lily-allen-alfie.png|thumb|right|Alfie trying to get back the water pipe from Allen in the music video for "Alfie".]]
The [[music video]] was directed by Sarah Chatfield,<ref name="video">{{cite web|url=http://www.gbh.tv/music.videos/lilyallen/alfiepage.html|title=GBH: Music Video  Page; Lily Allen - Alfie|publisher=GBH.com|accessdate=2009-09-05|archiveurl=https://web.archive.org/web/20070208172650/http://www.gbh.tv/music.videos/lilyallen/alfiepage.html|archivedate=2007-02-08}}</ref> who wanted to do a [[Tom and Jerry]]-like video, where the human character is cut off at the waist. Hence Allen was not going to be part of it originally. However, she did appear on the video. The singer declared on her official blog that, <blockquote>I did a video shoot for the next single, which is Alfie, and that was quite a lot of fun, Alfie was played by a puppet and the whole thing looks like an episode of a mixture Tom and Jerry and Roger Rabbit, you'll understand when you see it. The whole day was a pleasurable experience [...]<ref>{{cite web|url=http://www.lilyallenmusic.com/emi-site/site.php?page=blog&item=1654943|work=official blog|publisher=LilyAllenMusic.com|title=Rock and Roll|accessdate=2009-12-21}}</ref></blockquote>  The video starts off with an opening title parodying [[Looney Tunes]] and shows Allen in the kitchen preparing a cup of tea, while Alfie, impersonated by a [[puppet]], smokes in his miniature bedroom, watching television.<ref name="video"/> The singer enters the room and takes away his [[bong]], as he unsuccessfully tries to get it back. Next, Allen irons his tuxedo and marks a job announcement called "The Puppet Show" in the newspaper, as the lyric "You need to get a job because the bills need to get paid" is sung. Meanwhile, Alfie is in his bed, staring at a magazine in which there are undressed dolls, titled "Roxy & Babs get it on!" and proceeds to [[masturbation|masturbate]]. Allen walks in on him with the newspaper and suit as he does so, but immediately exits out of embarrassment.<ref name="video"/> With the start of the bridge, puppet birds sing outside the home, while Alfie sneaks out of his room and tries to get beer out of the refrigerator, but is ultimately caught by Allen. She then takes off his "stupid fitted cap", as he scrawls a picture of her, which is also the cover of "[[Littlest Things]]".<ref name="video"/> The final scene happens at night, in the kitchen, where Allen is lying on the floor, watching her brother dance. After the last lyrics, "Please don't despair, my dear, mon frere" is sung, he punches her and the video ends.<ref name="video"/> <!--Director <ref>Sarah Chatfield</ref> commented on the video, saying:<blockquote>When I listened to the track, it struck me how much it sounded like a cartoon. I could just see bright colors all over. I wanted to create a slightly musical feel to the whole production. Like the way Snow White has birds and animals around her. That old Disney touch: something good fun and cartoon like.<ref name="quote">{{cite web|url=http://www.videology-tv.com/viewclip.php?id=64|publisher=videology|accessdate=2009-09-11|title=director sarah chatfield - 'alfie' by lily allen}}</ref></blockquote>-->
There is also an alternate version, where the words "weed", "[[THC]]" and "twat" are blanked, the bong is replaced by a [[joystick]], Alfie does not smoke, and, when Allen walks in on him masturbating, there is a big "censored" sign.<ref>{{cite web|url=http://www.lilyallenmusic.com/lily/video/1752000|publisher=LilyAllenMusic|title=Lily Allen · Video|accessdate=2009-06-09| archiveurl= https://web.archive.org/web/20090528160501/http://www.lilyallenmusic.com/lily/video/1752000| archivedate= 28 May 2009 <!--DASHBot-->| deadurl= no}}</ref> The video won "Best Pop Video" and "Best New Director" at the CADs Music Vision Awards in June 2007,<ref>{{cite web|url=http://www.showstudio.com/contributors/32802|publisher=SHOWstudio|title=Contributors: Chatfield|accessdate=2009-09-11}}</ref> and nominated at the 2007 [[Q Awards]] for "Best Video", but lost to "[[Ruby (Kaiser Chiefs song)|Ruby]]" by [[Kaiser Chiefs]].<ref>{{cite news|url=http://news.bbc.co.uk/2/hi/entertainment/7033815.stm|title=Winners in full: Q Awards 2007|work=[[BBC News]]|publisher=[[BBC]]|accessdate=2010-01-19 | date=2007-10-08}}</ref>

==Track listing and formats==
{{col-begin}}
{{col-2}}
* '''CD Single'''<ref name="ukcover">{{cite web|work=[[Discogs]]|publisher=Zink Media, Inc.|title=Lily Allen - Shame for You/Alfie - CD, Single|accessdate=2009-09-03|url=http://www.discogs.com/Lily-Allen-Shame-For-You-Alfie/release/1578908}}</ref><ref name="digital"/>
#"[[Shame for You]]"
#"Alfie" (explicit)

* '''7" Vinyl'''<ref name="digital"/>
#"Shame for You"
#"Alfie" (explicit)

* '''Digital download'''<ref name="digital">{{cite web|url=http://www.lilyallenmusic.com/lily/music/1751888 |publisher=Lily Allen Music |title=Alfie Tracklisting |accessdate=2009-09-03 |archiveurl=https://web.archive.org/web/20091008081631/http://www.lilyallenmusic.com/lily/music/1751888 |archivedate=8 October 2009 |deadurl=yes |df=dmy }}</ref>
# "Shame for You"
# "Shame for You" (live at Bush Hall)
# "Alfie" (explicit)
# "Alfie" ([[CSS (band)|CSS]] remix)
#"Alfie" (live at Bush Hall)
{{col-2}}
* '''Japanese EP'''<ref name="ep">{{cite web|url=http://www.emimusic.jp/international/release/200707/tocp61131.htm|title=EMI Music Japan  商品情報|work=[[EMI Music Japan]]|publisher=[[EMI]]|accessdate=2009-09-04}}</ref>
#"Alfie"
#"[[Smile (Lily Allen song)|Smile]]"
#"[[Everybody's Changing]]"
#"Nan You're a Window Shopper"
#"Alfie" (CSS remix)
#"Smile" (Version Revisited) ([[Mark Ronson]] remix)
#"Alfie" (banned version) (CD extra music video)
#"[[LDN (song)|LDN]]" (TYO version) (CD extra music video)
#"[[Littlest Things]]" (CD extra music video)
{{col-end}}

==Credits and personnel==
*Lead vocals — Lily Allen
*Written by — Lily Allen and [[Greg Kurstin]]
*Produced by — Greg Kurstin
*[[Master recording|Mastered]] by — Tim Burrell, Tim Debney
*[[Audio mixing (recorded music)|Audio mixing]] — Greg Kurstin

==Charts==
{|class="wikitable"
!align="left"|Chart (2007)
!align="center"|Peak<br/>position
|-
|align="left"|[[Eurochart Hot 100 Singles]]<ref name="euro">{{cite web|url={{BillboardURLbyName|artist=lily allen|chart=European Hot 100}}|work=[[Billboard (magazine)|Billboard]]|publisher=Nielsen Company|title=Lily Allen Album & Song chart History|accessdate=2009-11-23}}</ref>
|align="center"|55
|-
|align="left"|[[Irish Singles Chart]]<ref name="acharts">{{cite web|url=http://acharts.us/song/12000|title=Lily Allen - Alfie - Music Charts|publisher=αCharts.us|accessdate=2009-09-04}}</ref>
|align="center"|31
|-
|align="left"|[[Oricon|Japan Albums Chart]]<ref name="jp">{{cite web|url=http://ranking.oricon.co.jp/free_contents/search/ranking_list.asp?itemcd=714288&samecd=1&chart_kbn=11A&linkcd=31375071|title= オリコンランキング情報サービス「you大樹」(アルフィーＥＰ)|work=[[Oricon]]|publisher=Oricon Entertainment Inc.|accessdate=2009-09-04}}</ref><sup>{{anchor|ref_A}}[[#endnote_A|[A]]]</sup>
|align="center"|226
|-
|align="left"|[[RIANZ|New Zealand Singles Chart]]<ref name="acharts"/>
|align="center"|15
|-
|align="left"|[[UK Singles Chart]]<ref name="uk">{{cite web|url=http://www.chartstats.com/songinfo.php?id=32978|publisher=Chart Stats|title=Chart Stats - Lily Allen - Alfie|accessdate=2009-09-04}}</ref><sup>{{anchor|ref_B}}[[#endnote_B|[B]]]</sup>
|align="center"|15
|}

'''Notes:'''<br/>
{{refbegin}}
*A{{anchor|endnote_A}}'''[[#ref_A|^]]''' Charted as the "Alfie" [[extended play]].
*B{{anchor|endnote_B}}'''[[#ref_B|^]]''' Charted as the double A-sided single "Alfie"/"[[Shame for You]]".
{{refend}}

==References==
{{reflist|2}}

==External links==
*[http://www.lilyallenmusic.com/video/alfie-music-video/ "Alfie" music video] at LilyAllenMusic.com
*[http://www.lilyallenmusic.com/lily/lyrics/1580821/ "Alfie" lyrics] at LilyAllenMusic.com
* {{MetroLyrics song|lily-allen|alfie}}<!-- Licensed lyrics provider -->

{{Lily Allen}}
{{Lily Allen songs |state=autocollapse}}

{{DEFAULTSORT:Alfie (Lily Allen Song)}}
[[Category:2006 songs]]
[[Category:2007 singles]]
[[Category:Lily Allen songs]]
[[Category:Songs written by Greg Kurstin]]
[[Category:Songs written by Lily Allen]]
[[Category:Song recordings produced by Greg Kurstin]]