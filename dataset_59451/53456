[[File:Bates Cooke (NY Comptroller).png|thumb|right|Bates Cooke]]
'''Bates Cooke''' (December 23, 1787 in [[Wallingford, Connecticut|Wallingford]], [[New Haven County, Connecticut]] &ndash; May 31, 1841 in [[Lewiston (town), New York|Lewiston]], [[Niagara County, New York]]) was an American lawyer and politician.

==Life==
He was the son of Captain [[Lemuel Cooke]] who had fought in the [[American Revolutionary War]]. Bates and his brother Lathrop participated in the [[War of 1812]].

Bates Cooke was [[Town supervisor|Supervisor of the Town]] of [[Cambria, New York]] in 1814. Then he studied law, was [[Admission to the bar in the United States|admitted to the bar]] about 1815 and commenced practice in Lewiston.

He was elected as an [[Anti-Masonic Party|Anti-Mason]] to the [[22nd United States Congress]], and served from March 4, 1831 to March 3, 1833.

Bates and Lathrop Cooke were partners of the [[Lewiston Railroad]] Company, which connected with the [[Lockport and Niagara Falls Railroad]] in 1835.

He was [[New York State Comptroller]] from 1839 to January 1841 when he resigned because of his bad health.  Subsequently, he was appointed a bank commissioner and died in office soon after.

He was buried at the Oak Wood Cemetery in Lewiston.

==Sources==
{{CongBio|C000729}}
*[https://books.google.com/books?id=E3sFAAAAQAAJ&pg=PA34 Google Book] ''The New York Civil List'' compiled by Franklin Benjamin Hough (pages 34 and 39; Weed, Parsons and Co., 1858) (giving the impression that Cooke was appointed bank commissioner in May 1840, but in fact he was appointed ["under the act of May 1840"] only after his resignation as Comptroller)
*[http://politicalgraveyard.com/bio/cooke.html Bates Cooke] on Political Graveyard
*[https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9807E4D91731E433A2575BC0A9629C94629FD7CF An episode from the Revolutionary War], in ''[[The New York Times]]'' on April 8, 1883 ([[Portable Document Format|PDF]])
*[https://web.archive.org/web/20101010190744/http://www.historiclewiston.org:80/history.html Lewiston history]

==External links==
*{{Find a Grave|6599174}}
*[https://web.archive.org/web/20080530145629/http://www.historiclewiston.org:80/pictures.html] Historic houses in Lewiston, among them Cooke's on 755 Center Street

{{s-start}}
{{s-par|us-hs}}
{{USRepSuccessionBox|state=New York|district=30|before=[[Ebenezer F. Norton]]|after=[[Philo C. Fuller]]|years=1831&ndash;1833}}
{{s-off}}
{{succession box|before=[[Azariah Cutting Flagg]]|title=[[New York State Comptroller]]|years=1839–1841|after=[[John A. Collier]]}}
{{s-end}}

{{NYSComptroller}}

{{Bioguide}}

{{Authority control}}

{{DEFAULTSORT:Cooke, Bates}}
[[Category:1787 births]]
[[Category:1841 deaths]]
[[Category:People from Wallingford, Connecticut]]
[[Category:Anti-Masonic Party members of the United States House of Representatives from New York]]
[[Category:New York State Comptrollers]]
[[Category:People from Lewiston, New York]]
[[Category:19th-century American railroad executives]]
[[Category:American people of the War of 1812]]
[[Category:New York lawyers]]
[[Category:Town supervisors in New York]]
[[Category:19th-century American politicians]]