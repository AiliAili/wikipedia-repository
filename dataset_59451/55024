{{Infobox airport
| name         = Sukhumi Babushara Airport 
| nativename   = 
| image        = Babushera_UGSS_SUI_airport.jpg
| IATA         = SUI
| ICAO         = UGSS
| type         = Public / Military
| owner        =
| operator     =
| city-served  = [[Sukhumi]]
| location     = [[Abkhazia]],<ref>{{Abkhazia-note}}</ref> [[Georgia (country)|Georgia]]
| elevation-f  = 53
| elevation-m  = 16
| coordinates  = {{coord|42|51|29|N|041|07|41|E|type:airport}}
| website      =
| metric-rwy   = y
| r1-number    = 12/30
| r1-length-m  = 3,661
| r1-length-f  = 12,012
| r1-surface   = Concrete
| footnotes    = Source: [[DAFIF]]<ref>[http://www.worldaerodata.com/wad.cgi?airport=UG29 Airport information for Sukhumi Dranda Airport (UG29)] from [[DAFIF]] (effective October 2006)</ref>
}}

'''Sukhumi Babushara Airport''' {{airport codes|SUI|UGSS}},<ref>{{ASN|SUI|Sukhumi-Babusheri Airport (SUI / UGSS)}}</ref> previously known as '''Sukhumi Dranda Airport''', is the main airport of [[Abkhazia]]. It is located in the village of [[Babushara]] next to the larger village of [[Dranda]] and  some {{convert|20|km|mi}} from [[Sukhumi]], the capital of Abkhazia.

==History==
The airport was built in the mid-1960s, when the region was part of the [[Union of Soviet Socialist Republics|Soviet Union]]. In the Soviet era, it was used only for domestic flights, primarily to transport people from across the Soviet Union to the sunny beaches of Abkhazia. The airport was heavily damaged during the [[Georgian Civil War|civil war]] in the early 1990s. Land mines and other explosive remnants of war have been cleared from the airport since by the [[HALO Trust]], the only land mine clearance agency active in Abkhazia at the present time.

The airport is currently only used for flights to the mountain village of [[Pskhu]] and for flights carried out by [[Russian Air Force]]{{Verify source|date=August 2015}}.

In 2006 the [[government of the Republic of Abkhazia]] expressed its desire to resume international air traffic in the future,<ref>{{cite web | url = http://www.abkhaziagov.org/en/news/detail.php?ID=3820 | title = Sukhum's Airport May Soon Resume Operation | work = News release | publisher = Administration of the President of the Republic of Abkhazia | date = 2006-12-20}}</ref> however the facility is not recognized as an international airport by [[ICAO]] and flights can only be allowed with the permission of the Georgian government.{{citation needed|date=September 2011}}

There is another airport in Abkhazia near [[Gudauta]], which serves [[Russian military]] troops located there.

==External links==
* [http://www.airliners.net/photo/Abkhazia-air-force/Yakovlev-Yak-52/1628189/M/ Babushara airport on Airliners.net]

==References==
{{Reflist}}

{{Airports in Georgia (country)}}

[[Category:Airports built in the Soviet Union]]
[[Category:Airports in Georgia (country)]]
[[Category:Transport in Abkhazia]]
[[Category:Sukhumi]]
{{Airports built in the Soviet Union}}