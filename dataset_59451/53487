{{about||the California politician|Bill Dodd (California politician)|the English footballer|Bill Dodd (footballer)}}
{{Cleanup|reason=the article cites very few references or sources |date=September 2015}}
{{Infobox officeholder
| image       =Bill_Dodd_of_Louisiana.jpg
| image size  =
| caption     =
| office      = [[Louisiana State Legislature|Louisiana State Representative from Allen Parish]]
| term_start  = 1940
| term_end    = 1948
| preceded    = David Cole
| succeeded   = M.V. Hargrove
| office2     = 42nd [[Lieutenant Governor of Louisiana]]
| term_start2 = May 11, 1948
| term_end2   = May 13, 1952
| governor2   = [[Earl K. Long]]
| preceded2   = [[J. Emile Verret]]
| succeeded2  = [[C. E. "Cap" Barham]] 
| office3     = Louisiana State [[Auditor]] (later Comptroller; office is no longer elected.)
| term_start3 = 1956
| term_end3   = 1960
| preceded3   = [[Allison Kolb]]
| succeeded3  = [[Roy R. Theriot]]
| office4     = Louisiana State Board of Education
| term_start4 = 1960
| term_end4   = 1964
| preceded4   = Merle Welsh
| succeeded4  = V.J. Scogin
| office5     = Louisiana Education Superintendent
| term_start5 = 1964
| term_end5   = 1972
| preceded5   = [[Shelby M. Jackson]]
| succeeded5  = [[Louis J. Michot]]
|birth_name        = William Joseph Dodd
| birth_date  = {{birth date|mf=yes|1909|11|25}}
| birth_place = [[Liberty, Texas|Liberty]], [[Liberty County, Texas|Liberty County]]<br>[[Texas]], USA
| death_date  = {{death date and age|mf=yes|1991|11|16|1909|11|25}}
| death_place = [[Baton Rouge, Louisiana|Baton Rouge]], [[East Baton Rouge Parish]]<br>Louisiana
| spouse      = Verone Ford Dodd (1918-2005, married 1939-his death) 
| children    = William Ford Dodd (born 1947)<br/>Leonard Bruce Dodd (born 1951)
| party       = [[Democratic Party (United States)|Democrat]] 
| occupation  = [[Lawyer|Attorney]]; [[Politician]] 
| religion    = [[Baptist]]
| footnotes   =
}}
'''William Joseph "Bill" Dodd''' (November 25, 1909 – November 16, 1991) held five important positions in [[Louisiana]] government in the mid-twentieth century, including the offices of [[Louisiana House of Representatives|state representative]], [[Lieutenant Governor of Louisiana|lieutenant governor]], state [[auditor]], president and member of the State Board of Education, and state education superintendent, but he never achieved his ultimate goal: the state's powerful [[Napoleon]]ic-style [[governor]]ship. Twice Dodd failed to win the pivotal [[Democratic Party (United States)|Democratic]] [[gubernatorial]] nomination: 1952 and 1959. To his critics, he was a Long "hatchet man." To his admirers, he never let his defeats sour his optimistic spirit, his [[patriotism]], or his devotion to his adopted home state.

==From Liberty, Texas, to Allen Parish, Louisiana==
Dodd was born in a logging camp in [[Liberty, Texas|Liberty]], [[Texas]], near [[Houston, Texas|Houston]], to Daniel David Dodd and the former Nancy J. Pawley. The family relocated to [[Sabine Parish]] (parish seat: [[Many, Louisiana|Many]], pronounced MAN NIE) between [[Shreveport, Louisiana|Shreveport]] and [[Lake Charles, Louisiana|Lake Charles]]. He graduated from Zwolle [[High School]] in [[Zwolle, Louisiana|Zwolle]] in Sabine Parish.

After high school, Dodd played professional [[baseball]] for teams in [[Monroe, Louisiana]], and [[Cody, Wyoming|Cody]], [[Wyoming]]. In Wyoming, one of Dodd's teammates was a future [[Republican Party (United States)|Republican]] [[governor of Wyoming|governor]] and U.S. senator, [[Milward L. Simpson]]. While he was in Cody, Dodd became a friend of the Simpson family and often babysat the two Simpson sons, [[Pete Simpson]] and [[Alan K. Simpson]], both of whom eventually served in the [[Wyoming House of Representatives]]. Alan Simpson also served from 1979 to 1997 as a Republican U.S. senator from Wyoming. For years afterwards, Dodd maintained contact with the Simpson family. Dodd, who was white, was said, in his own words, to resemble the popular black [[Boxing|boxer]] [[Joe Louis]]. But Dodd was playing baseball, not boxing.

After his baseball years, Dodd attended [[Northwestern State University]] (then Louisiana Normal School) in [[Natchitoches, Louisiana|Natchitoches]], where he was a member of [[Sigma Tau Gamma]] Fraternity. He graduated with a [[bachelor's degree]] in 1934 and taught school for a number of years at [[Oakdale High School (Oakdale, Louisiana)|Oakdale High School]] in [[Oakdale, Louisiana|Oakdale]] in [[Allen Parish]]. Dodd recalled in his memoirs that he earned $140 per month as a teacher, with many duties outside the classroom. A high school student advised Dodd in 1939 that he should run for the state legislature so that he would have a greater impact on the people of the state.

On August 27, 1939, Dodd married one of his former students, the former Verone Ford (February 18, 1918 – September 25, 2005). The couple had two sons, William Ford Dodd (born August 17, 1947), often called "Bill, Jr.," and Leonard Bruce Dodd (born 1951) of [[Clinton, Louisiana|Clinton]], the seat of [[East Feliciana Parish]].

Another of Dodd's former students was future state [[Treasurer]] [[Mary Evelyn Parker|Mary Evelyn Dickerson Parker]], whom he taught speech and later helped to secure a college scholarship and a job in the [[Earl Kemp Long]] administration. Dodd in his [[memoirs]] described Mrs. Parker as "a born ingrate" because she often tried to undercut his political aspirations.

Dodd's political career, spurred by a student's advice, hence began with his election in 1940 to the [[Louisiana House of Representatives]] from Allen Parish. He unseated four-term [[incumbent]] Democrat  [[David Cole (Louisiana politician)|David Cole]], who had been an ally of the disgraced [[Richard W. Leche]] administration. In 1932, Cole, who had a fiery temper, clashed on the House floor with a colleague and both men came away with scrapes and bruises. (Richard D. White, Jr., ''Kingfish'', Random House, 2006, pp.&nbsp;154–155)

Dodd recalled that the families of his students worked hard to get him elected to the legislature, and he never forgot their support. Dodd served in the House under the anti-Long Governors [[Sam H. Jones|Sam Houston Jones]] of Lake Charles and [[Jimmie Davis|James Houston "Jimmie" Davis]], originally of [[Jackson Parish]]. Jones defeated Earl Long in 1940, and Davis topped the Long factional choice of former [[U.S. Representative]] [[Lewis L. Morgan]] of [[Covington, Louisiana|Covington]], the seat of [[St. Tammany Parish]], in the 1944 primary runoff.

Although he was exempt from [[conscription]], Dodd volunteered for the [[U.S. Army]] during [[World War II]] and served as a [[lieutenant]] in Europe. He was reelected to his second term in the legislature while he was still in the military. When he returned home in 1945, he began his legal studies at [[Louisiana State University]] in [[Baton Rouge]]. He received his [[LL.B.]] degree less than two years later and was admitted to the Louisiana bar.

==Lieutenant Governor Dodd, 1948–1952==
In 1948, he was elected [[Lieutenant Governor of Louisiana|lieutenant governor]] under Earl Long, who won the second of his three [[gubernatorial]] terms. While he was lieutenant governor, a part-time position, which permitted him to practice law as well, Dodd directed Louisiana [[Civil Defense]], and he represented the state before the Tidelands settlement commission.

Dodd and then [[Louisiana Attorney General]] [[Bolivar Edwards Kemp, Jr.]], who served from 1948–1952, maintained that Louisiana seriously erred when it rejected a compromise tidelands offer from the [[Harry Truman|Truman]] administration. Had Governor Long agreed to the deal, the state would have gained billions of additional dollars in state revenues over the coming decades, Dodd said. Long rejected the compromise on advice from Judge [[Leander Perez]] of [[Plaquemines Parish]], who maintained that Louisiana could win a much better settlement before the [[United States Supreme Court]]. As it turned out, the Dodd-Kemp assessment was vindicated, as the Supreme Court rebuffed Louisiana's attempt to get a greater share of the offshore revenues. Dodd maintained that Louisiana government would have been the best funded in the nation had Long accepted Truman's offer.

In 1982, Edgar Poe, former president of the National Press Corps in [[Washington, D.C.]], wrote Dodd that "Louisiana would be a tax free state had the compromoise proposed been accepted. As you know, offshore Louisiana is the most intense oil and gas development area in the world. As a result, the federal government is collecting hundreds of millions of dollars every year from lease sales of its share of the oil and gas sales." Dodd called Poe "among the finest persons and most reliable reporters who ever stroked a typewriter."

As lieutenant governor, Dodd became "acting governor" whenever Earl Long went outside the state's boundaries. Dodd found that Long was reluctant to leave Louisiana for very long because Long disliked the procedure by which the lieutenant governor could perform certain gubernatorial duties when the governor was out of state.

Once Dodd was invited to Wyoming to hunt with his friends the Simpsons. Long became so jealous of Dodd's invitation to visit the first family of Wyoming that he told Dodd that he was going to [[Hot Springs, Arkansas]], at the time Dodd was to be in Wyoming. According to Dodd, Earl Long "let on as if he did not know of our already scheduled Wyoming trip. But he did know of it, and he knew that if he left the state, I would have to stay and act as governor while he was away ... Of course, I cancelled the hunt. Then on the weekend, when Earl was supposed to leave for Hot Springs, he went to his hog farm in Winnfield and didn't even tell me he had cancelled his Arkansas trip."

Earl Long had a severe [[myocardial infarction|heart attack]] late in 1951, and Dodd even wondered if he might yet succeed to the state's top office, but Long slowly recovered.

From 1952–1956, Dodd was a member of the powerful Louisiana Democratic State Central Committee.

==Dodd's first gubernatorial campaign, 1951–1952==
The Longite Dodd occasionally quarreled with the Longs. While he was lieutenant governor, and his office was across the corridor from the governor's, Earl Long often ignored Dodd. He preferred to communicate in writing, rather than face-to-face meetings. Long flatly objected to Dodd's candidacy for governor. Long occasionally derided the lieutenant governor in private correspondence in which he denounced Dodd as "Big Bad Bill Dodd" and "the Dr. Jekyll and Mr. Hyde of Louisiana politics." The Dodd-Long relationship appeared to have been eerily similar to that which surfaced years later with President [[Bill Clinton|William Jefferson Blythe "Bill" Clinton]] and his political consultant, [[Dick Morris]].

Dodd selected the future "Cajun" humorist and chef, [[Justin Wilson (chef)|Justin E. Wilson]], as his gubernatorial campaign manager and moved full speed ahead in a bid to succeed Long. Wilson was no stranger to Louisiana politics. His father, [[Harry Wilson (Louisiana politician)|Harry D. Wilson]], had been agriculture commissioner, and his brother-in-law, Bolivar Kemp, Jr., was the sitting attorney general who had worked with Dodd on the tidelands issue. The Dodd intraparty ticket included lieutenant governor candidate [[Leon Gary]], who from 1948 to 1964 was the mayor of [[Houma, Louisiana|Houma]] in [[Terrebonne Parish, Louisiana|Terrebonne Parish]], incumbent Auditor L. B. Baynard, and Henry C. Sevier for [[Attorney General of Louisiana|attorney general]]. Dodd did not field a candidate for register of states lands, temporarily vacated by [[Lucille May Grace]]. His former pupil in whom he has lost confidence, Mary Evelyn Dickerson, was defeated that year in the open race for register to [[Ellen Bryan Moore]].<ref>''Minden Press'', January 11, 1951, p. 7.</ref>

Among Dodd's primary opponents were Judge [[Robert F. Kennon]] of [[Minden, Louisiana|Minden]] in [[Webster Parish]], Congressman [[Hale Boggs|Thomas Hale Boggs, Sr.]], of New Orleans, Judge [[Carlos Spaht]] of Baton Rouge, wealthy [[cattle]]man [[James M. McLemore]] of [[Alexandria, Louisiana|Alexandria]], and Lucille May Grace, register of the state land office and the first woman ever to seek the Louisiana governorship. Dodd finished fifth in the primary, with 90,925 votes (11.9 percent). Miss Grace, who accused her rival Boggs of either "being a communist or having had communist sympathies," ran last in tenth place.

The two finalists were Judge Spaht, who had most of the Long dynastic support that year, and Judge Kennon, around whom the anti-Long forces coalesced in the runoff primary, which Kennon won handily. Dodd could take comfort that he still ran ahead of the colorful State Senator [[Dudley J. LeBlanc]] of [[Abbeville, Louisiana|Abbeville]] in [[Vermilion Parish]], who polled 62,906 votes (8.3 percent). The wealthy LeBlanc is best remembered for peddling the [[alcohol]]-laden [[patent medicine]] called [[Hadacol]]. In the runoff primary, Dodd endorsed Kennon in part to get back at Earl Long.

Dodd recalled that Earl Long did not want Dodd to run for governor in the 1951 primary because Long was reconciled to an "anti-Long" victory, whether Kennon or Boggs. Long therefore expected Spaht to lose to Kennon. He also expected [[John McKeithen]] to lose to [[C. E. "Cap" Barham|Charles E. "Cap" Barham]] of [[Ruston, Louisiana|Ruston]], the original lieutenant governor choice of Boggs, who was "adopted" by Kennon in the Democratic runoff. Still, Long worked hard for McKeithen but held back his most stringent efforts on Spaht's behalf. Dodd said that Earl Long wanted to lead the Long faction both in the governorship and out of the governorship. In other words, he did not want anyone else in the Long faction to be governor. That top job was "reserved" for Earl Long. The same was often said of Governor [[Edwin Washington Edwards]] during his stormy four-term tenure in the state's highest constitutional office.

==Did Justin Wilson help to elect Kennon?==
Dodd said that his 1951–1952 campaign manager, Justin Wilson, helped him to draw crowds. "He is a good speaker as well as the best storyteller in Louisiana. But Justin and I soon saw that old Earl had outfoxed us, and we just played out the game to eliminate and harass those who had double-crossed us.

{{cquote|Justin came up with the trick that killed both Spaht and Boggs, and it might have elected me if the field had not been so crowded, and Spaht had not had the payroll boys, and Kennon most of the independents...

Justin dropped a 'bombshell'. He exposed a conspiracy between Earl and Senator [[Russell B. Long]] to field the two candidates for governor – Carlos Spaht and Hale Boggs – as a racehorse owner fields two of his own horses in the same race... All they wanted was to get Boggs and Spaht into the second primary, then Uncle Earl would flip a nickel and choose the winner. The press ate it up... Finally Uncle Earl gave credibility to the story by asking the voters to vote for Boggs if they couldn't vote for Spaht. Earl said that on purpose to kill his two candidates. He knew I [Dodd] was whipped. That speech by Earl in [[West Monroe, Louisiana|West Monroe]] helped put Bob Kennon in the governor's mansion. So Justin, if he ever wants to claim the credit, was responsible for old Bob's [Kennon] getting a lot of votes, probably enough to get into the second primary...}}

==Dodd defeats Allison Kolb for auditor, 1956==
Earl Long wanted Dodd not to run for any office in 1952 but wait until the 1955-1956 cycle to run for another constitutional office while Earl Long ran and easily secured a third term as governor. And that ultimately happened: Earl Long returned to the governorship, and Dodd was reconciled on the Long ticket to the elected office of state auditor. He unseated the incumbent Democrat and later Republican [[Allison Kolb]] to win the auditor's position. Among those helping Dodd in his race against Kolb was the young Baton Rouge attorney [[Jack M. Dyer]], later a state representative. Later, the name "auditor" was changed to "comptroller", and the position was taken by [[Roy R. Theriot]] of [[Abbeville, Louisiana|Abbeville]], the seat of [[Vermilion Parish]].

For more on the auditor's race, see the Wikipedia article on [[Allison Kolb]].

=="William Dodd and William White," 1959==
As the outgoing auditor, Dodd launched his second bid for governor in 1959 on an intraparty ticket with William J. "Bill" White (1910–1990), a candidate for lieutenant governor. White would serve through 1985 as the mayor of [[Gretna, Louisiana|Gretna]], the seat of [[Jefferson Parish]]. Dodd recruited for Louisiana [[insurance commissioner]] the attorney Paul C. Tate, Sr. (1922–1983), of [[Mamou, Louisiana|Mamou]] in [[Evangeline Parish]], a 1951 graduate of the LSU Law School, whose classmates included future [[U.S. Representative]] [[Gillis William Long]], 9th Judicial District Judges [[Guy Humphries|Guy E. Humphries, Jr.]], and [[Lloyd George Teekell]], all of [[Alexandria, Louisiana|Alexandria]], and future state Representatives [[George B. Holstead]] of [[Ruston, Louisiana|Ruston]] and [[Risley C. Triche]] of [[Napoleonville, Louisiana|Napoleonville]].<ref>{{cite web|url=http://www.e-yearbook.com/yearbooks/lsu/1951/Page_127.html|title=Louisiana State University ''Gumbo'' yearbook, 1951|publisher=e-yearbook.com|accessdate=July 3, 2013}}</ref>  Dodd's choice for comptroller was Toby O'Rillion, a son-in-law of former State Senator [[Gilbert Franklin Hennigan]] of 
Beauregard Parish and an uncle by marriage of later [[Louisiana Department of Agriculture and Forestry|Commissioner of Agriculture]] [[Gil Dozier]]. White lost out to conservative former state House Speaker [[Clarence C. Aycock|Clarence C. "Taddy" Aycock]] of [[Franklin, Louisiana|Franklin]] in [[St. Mary Parish, Louisiana|St. Mary Parish]]. Tate was a loser to [[Rufus D. Hayes]], an Earl Long appointee and the choice of Jimmie Davis. O'Rillion lost to [[Roy R. Theriot]], the mayor of Abbeville.

Dodd's unofficial campaign manager that year, [[Sidney Bowman]], was a former LSU great and an Olympic athlete, who sought with apparently little success to recruit supporters of former Governor Kennon, who did not seek office in 1959.

Three LSU scholars described the 1959 Dodd campaign, accordingly:

"Dodd was periodically estranged but never wholly alienated from the Longs. [While lieutenant governor and state auditor], he feuded with Earl Long . . . despite apparent agreement on substantive liberal programs, and although he tried for the office of governor in 1952 after his first split with Earl Long, he ran quite weakly. His success at the polls up to this point has depended on the Long endorsements."<ref>William C. Havard, Rudolf Heberle, and [[Perry H. Howard]], ''The Louisiana Elections of 1960'', [[Baton Rouge]]: [[Louisiana State University]] Studies, 1963, p. 39</ref>

The entire Dodd-White slate failed. Dodd finished fourth with 85,436 votes (10.1 percent). Dodd even ran some five thousand votes behind his showing eight years earlier.

Dodd got much of his funding for the race from his close friend, the high-powered businessman and banker [[Louis J. Roussel, Jr.]], of New Orleans and Metairie, who began his career as a bus driver. He raised some $500,000. Roussell supported many candidates over the years, including Governor [[Edwin Washington Edwards]] and even Republicans, such as [[Ben Bagert]], when the GOP began to make a stronger showing in Louisiana elections.

[[Segregationist]] but self-styled "classical liberal" [[William M. Rainach]] of [[Claiborne Parish]] took third place, but the governorship that year went to a popular singer, former Governor Jimmie Davis, also a proclaimed segregationist. The 1959 election was the first in Louisiana in which race had been the major factor in the voting.

Dodd said that Rainach hurt him "with the scared white segregationists". Dodd said that if he had not raised so much money for the race, he would have withdrawn when Davis announced his candidacy "for I could see that he would beat me out in the first primary. But I couldn't get out without being branded a quitter..."

Dodd continued:

{{cquote|I ran the race out to its end, but never was in it because of the split vote. Rainach and Davis took most of my following, and Earl spent all of his time blasting my candidacy and that of Rainach. Jimmie Davis and [[deLesseps Story Morrison|deLesseps Story "Chep" Morrison, Sr.]], made the runoff, and Jimmie ate his lunch, just as I would have done had Davis and Rainach stayed out... My support for Chep in the second primary did not help Chep very much, but it paved the way for me to win the state superintendent of education race four years later, a race I began planning before the votes were counted in 1960...}}

While Dodd ran for governor, Earl Long sought the lieutenant governorship – he had also run for lieutenant governor unsuccessfully in 1944 – on an intraparty "ticket" with former Governor [[James A. Noe|James Albert Noe, Sr.]] It was common for men like Dodd, Noe, Perez, and LeBlanc to have a period of alliance with the Longs followed by times of alienation.

==Superintendent of Education (1964–1972)==
In 1960, Dodd was elected to the Sixth Congressional District seat on Louisiana State Board of Education. He unseated in the Democratic primary Merle M. Welsh, the owner of Welsh Funeral Home in Baton Rouge. In his memoirs, Dodd said, "I knew I could win and said nothing about Mr. Welsh. When someone asked me who was running against me, I would only say, 'an old southern planter.' I got a few laughs, and I got Mr. Welsh's goat." In November 1960, Dodd defeated Mrs. Mary Helen Stracener (born October 30, 1922) of Baton Rouge, the only Republican that he ever faced in a contested [[general election]]. He received 86.1 percent of the ballot to her 13.9 percent. After his election to the state board, Dodd was named its president by his fellow members.

His gubernatorial prospects dashed, Dodd ran in the Democratic primary in 1963 for state superintendent of education, when long-term incumbent [[Shelby M. Jackson]], a native of the Monterey community near [[Ferriday, Louisiana|Ferriday]] in [[Concordia Parish]], entered the gubernatorial primary that year under a segregationist banner. Dodd defeated three fellow Democrats in the primary, Archie Robinson, F.F. Wimberly, and Raphiel Teagle (1915–1994) of Baton Rouge.<ref>''Minden Press'', December 9, 1963, p. 1</ref> He had no Republican opposition in the [[general election]] held on March 3, 1964.

Dodd began planning for the superintendent's race in 1960. He and his close advisors quietly encouraged Shelby Jackson to run for governor to open up the superintendency. The Dodd people sent letters to Jackson with one-dollar bills enclosed to demonstrate "grass roots" support for the superintendent were he to run for governor.

Moreover, Dodd had endorsed Morrison over Davis in the 1960 runoff on the premise that Morrison would run for governor again in 1963 and might in gratitude for Dodd's past support agree at least not to oppose Dodd for superintendent. In fact, Morrison supported Dodd for superintendent in 1963-1964. Similarly, Dodd expected to pick up support from Kennon voters because he had endorsed Kennon over Spaht in 1952. He also recruited Senator Russell Long to his cause. Later, Dodd and Davis became close friends as survivors of the rough world of Louisiana politics, and Davis wrote the introduction to ''Peapatch Politics'', Dodd's memoirs.

Dodd was renominated for superintendent in 1967, when he overwhelmed [[Ned Touchstone|Ned O'Neal Touchstone]] (1926–1988), the editor of ''The Councilor'', a "Radical Right" newspaper supporting the Louisiana Citizens’ Council in its fight against the [[civil rights movement]] in the 1960s. Touchstone owned and operated a Shreveport bookstore specializing in right-wing materials. He also conducted other mail order business ventures.

Touchstone tried to revive the Rainach-Jackson segregationist tradition to win the education position. He urged whites to unite as a voting bloc to counter the tendency of [[Black people|blacks]] to vote only for liberal Democratic candidates. Touchstone denounced the "liberalism," as he saw it, in both major parties, including the institutional Louisiana Democratic Party, of which Dodd had once sat on the central committee. After he defeated Touchstone for renomination, Dodd had no further opponent because the Republicans once again did not field a candidate for superintendent in the 1968 general election.

Dodd was superintendent during the full implementation of desegregation in Louisiana public schools, a process that lasted nearly a decade before its completion in August 1970. The desegregation, however, was ordered by the federal courts; it did not originate with the superintendent's office.

==Enter Louis J. Michot==
Then came 1971, and Dodd was sidelined by [[Lafayette, Louisiana|Lafayette]] businessman [[Louis J. Michot]], who had run for governor and finished far behind in the 1963 Democratic primary. While Dodd was superintendent, Michot was simultaneously a member of the Louisiana State Board of Education. Dodd finished second to the more conservative Michot in the primary and promptly withdrew from the runoff. Dodd declared that his accomplishments had been often "misunderstood by and misrepresented to the public. I hope my successor does not have this problem."<ref>''[[Minden Press-Herald]]'', November 11, 1971, p. 1</ref> Michot alleged that Dodd had created a bloated bureaucracy that required streamlining to increase the effectiveness of the department.

Michot, unlike Dodd in 1964 and 1968, faced a determined Republican challenge when a professor of education at [[Southeastern Louisiana University]] in [[Hammond, Louisiana|Hammond]], [[Robert L. Frye]] (1927-2011), entered the race. Michot still easily prevailed, with 662,597 votes (63.5 percent) to Frye's 380,896 (36.5 percent). Frye carried [[East Baton Rouge Parish]] and five north Louisiana parishes, including the Long stronghold of [[Winn Parish]].

Dodd sought to reclaim the education superintendency in first-ever [[jungle primary]] in Louisiana in 1975. He ran a strong third but was eliminated from the general election by [[J. Kelly Nix]], the first executive assistant to Governor Edwin Edwards and formerly of Monroe, who went on to unseat Michot.<ref>''[[Minden Press-Herald]]'', November 3, 1975, p. 1</ref>

=="Peapatch Politics," Dodd-style==
Having left elective office, Dodd felt freer to speak his mind on politics, and he did so. He criticizes himself as well as friends and foes in his memoirs. He explains on numerous occasions how the Longs and the anti-Longs created the rudiments of a two-party system centered on support or opposition to the Long dynasty. Dodd says that Earl Long was content for an anti-Long man to hold the governorship when Long was ineligible himself to run. Having mostly anti-Long candidates to oppose in the next election was much easier for Earl Long than it would have been to have one of his own men running. Therefore, Earl Long sabotaged both of Dodd's gubernatorial bids, then called Dodd back into his inner circle when he needed his advice and support, including Long's last campaign for the [[U.S. House]] in the Eighth Congressional District in 1960.

Dodd mellowed in his later years: no longer a hatchetman, he was a Louisiana "statesman." In 1988, he sent a moving letter to the ''[[The Advocate (Baton Rouge)|Baton Rouge Morning Advocate]]'', which mourned the death of one of his old intraparty rivals and sometimes ally as well, former Governor Kennon, winner of the 1951-1952 gubernatorial elections. Dodd had once dismissed Kennon as "naive," but he had endorsed Kennon in the 1952 runoff at a time when he was alienated from Earl Long.

Dodd had also demonstrated ways to heal political differences, much as Senator Russell Long had tried to bridge pro-Long and anti-Long sentiment in his lengthy career. Perhaps it was his love for his state that made it easy for him to eulogize Kennon even though he had once characterized his former rival as "naive." The letter was similar to a [[eulogy]] delivered on the floor of the [[U.S. House of Representatives]] in [[Washington, D.C.]], by [[Mississippi]] Democratic Congressman [[Lucius Quintus Cincinnatus Lamar (II)|Lucius Lamar]] on the death of Radical Republican Senator [[Charles Sumner]] of [[Massachusetts]].

(Dodd's letter can be accessed at the end of Wikipedia's article on [[Robert F. Kennon]].)

His ''Peapatch Politics: The Earl Long Era in Louisiana Politics'' is a political memoir from the 1940s to the 1970s. Dodd in time seemed to reach the conclusion that Louisiana did not benefit nearly so much from Longism and the constant corrupt practices as many of the Longites had always assumed. For the rest of his life, he regretted the consequences that stemmed from the rejection of the tidelands compromise that President Truman had offered in 1948.

Dodd's son, William Ford Dodd, who resided in [[Houma, Louisiana|Houma]] in [[Terrebonne Parish, Louisiana|Terrebonne Parish]] at the time of his father's death, ran for lieutenant governor in the 1987 [[jungle primary]] but finished third with 17 percent of the vote. William Ford Dodd was born during his father's first campaign for lieutenant governor. [[Paul Hardy]], a Democrat converted to Republican, won the position when Dodd, a Democrat, sought to follow his father's path – exactly forty years later – into the office.

==Death on election day==
Dodd died of cancer in Baton Rouge General Medical Center. Death came at 6 a.m. on the day of the Edwin Edwards-[[David Duke]] gubernatorial showdown. Had he been able to comprehend fully, Dodd would have surely known the outcome of that battle, but it was ironic that this "political man" would die on the day of one of the most contentious elections in Louisiana history.

Memorial services were held at his home church, the Southside [[Baptist Church]] in Baton Rouge on November 18, 1991, with Dr. John Robson officiating. In addition to his wife Verone, William, Jr., and Bruce, Dodd was survived by three grandchildren, William Seth Dodd and Katherine Sims Dodd, then of Houma, and Elizabeth Ann Dodd, then of Baton Rouge. Bill Dodd and Verone Dodd, who died fourteen years later, are buried in Resthaven Gardens of Memory in Baton Rouge.

[[File:Dodd Hall at NSU in Natchitoches IMG 1995.JPG|200px|right|thumb|<span style="font-size:100%;">Dodd Hall at [[Northwestern State University]] in [[Natchitoches, Louisiana|Natchitoches]].</span>]]

Dodd's papers are in the [[Eugene P. Watson]] Memorial Library at Northwestern State University, where he also headed the alumni association for a number of years. Dodd Hall there, which houses continuing education, [[electronics]], and [[Internet]] services, is named for him.

Dodd was also a member of the [[Masonic lodge]] and its companion organization, the [[Shriner]]s. He was a frequent speaker at school and political events. He often addressed commencement exercises, and his homespun humor charmed the graduates, not all of whom were sure what was ahead for them, but he convinced most that things would work out in the long run. He once joked that "the "A" students make professors, the "B" students make teachers, and the "C" students make the money."

Dodd was posthumously inducted into the [[Louisiana Political Museum and Hall of Fame]] in [[Winnfield]] in 2002. He died two years before the museum opened.

{{Portalbar|Biography|Texas|Louisiana|Education|Law|Politics|United States Army|World War II|Baptist}}

==Notes==
{{Reflist}}

==References==
*William J. "Bill" Dodd, ''Peapatch Politics: The Earl Long Era in Louisiana Politics'', Baton Rouge: Claitors Publishing, 1991
*''[[Who's Who in America]]'', Vol. 34 (1966–1967)
*''[[Shreveport Journal]]'', March 6, 1970
*http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=6386451
*https://web.archive.org/web/20060110093342/http://www.nsula.edu:80/watson_library/cghrc_core/dodd_b.htm
*http://www.cityofwinnfield.com/museum.html
*http://www.ferris.edu/HTMLS/OTHERSRV/isar/Institut/cca/touchstone_cca.htm
*http://ssdi.rootsweb.com/cgi-bin/ssdi.cgi

{{S-start}}
{{S-off}}
{{Succession box
| before = [[David Cole (Louisiana politician)|David Cole]]
| title  = [[Louisiana House of Representatives|Louisiana State Representative from Allen Parish]] 
| years  = 1940&ndash;1948
| after  = [[M.V. Hargrove]]
}}
{{Succession box
| before = [[J. Emile Verret]]
| title  = [[Lieutenant Governor of Louisiana]]  
| years  = 1948&ndash;1952
| after  = [[C. E. "Cap" Barham]]
}}
{{Succession box
| before = [[Allison Kolb]]
| title  = [[Louisiana]] State [[Auditor]] (thereafter called Comptroller)
| years  = 1956&ndash;1960
| after  = [[Roy R. Theriot]]
}}
{{Succession box
| before = Merle M. Welsh
| title  = Member, [[Louisiana]] State Board of [[Education]]
| years  = 1960&ndash;1964
| after  = V.J. Scogin
}}
{{Succession box
| before = [[Shelby M. Jackson]]
| title  = [[Louisiana]] State Superintendent of [[Education]] 
| years  = 1964&ndash;1972
| after  = [[Louis J. Michot]]
}}
{{S-end}}
{{Lieutenant Governors of Louisiana}}

{{Louisiana Political Museum and Hall of Fame}}

{{Authority control}}

{{DEFAULTSORT:Dodd, Bill}}
[[Category:1909 births]]
[[Category:1991 deaths]]
[[Category:People from Allen Parish, Louisiana]]
[[Category:Educators from Louisiana]]
[[Category:American military personnel of World War II]]
[[Category:Baptists from the United States]]
[[Category:Minor league baseball players]]
[[Category:Louisiana lawyers]]
[[Category:Louisiana Democrats]]
[[Category:Lieutenant Governors of Louisiana]]
[[Category:Comptrollers in the United States]]
[[Category:Louisiana State Superintendents of Education]]
[[Category:Louisiana State University alumni]]
[[Category:Members of the Louisiana House of Representatives]]
[[Category:Louisiana gubernatorial candidates]]
[[Category:Northwestern State University alumni]]
[[Category:Politicians from Baton Rouge, Louisiana]]
[[Category:School board members in Louisiana]]
[[Category:American memoirists]]
[[Category:United States Army officers]]
[[Category:Deaths from cancer in Louisiana]]
[[Category:20th-century American lawyers]]
[[Category:20th-century American writers]]
[[Category:20th-century American politicians]]