<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=O-320
 |image= File:SymphonyAircraftSA160LycomingO-320-D2A.jpg
 |caption=A Lycoming O-320-D2A installed in a [[Symphony SA-160]]
}}{{Infobox Aircraft Engine
 |type=[[Piston]] [[aircraft engine|aero-engine]]
 |manufacturer=[[Lycoming Engines]]
 |first run=1953
 |major applications=[[Cessna 172]]<br>[[Piper Cherokee]]
}}
|}

The '''Lycoming O-320''' is a large family of 92 different normally aspirated, air-cooled, four-cylinder, direct-drive engines commonly used on light [[aircraft]] such as the [[Cessna 172]] and [[Piper Cherokee]].  Different variants are rated for 150 or 160 [[horsepower]] (112 or 119 kilowatts). As implied by the engine's name, its cylinders are arranged in [[horizontally opposed]] configuration and a displacement of 320 cubic inches (5.24&nbsp;L).<ref>Gunston 1989, p.98.</ref>

==Design and development==
The O-320 family of engines includes the [[carburetor|carbureted]] O-320, the [[fuel-injection|fuel-injected]] IO-320, the inverted mount, fuel-injected AIO-320 and the [[aerobatics|aerobatic]], fuel-injected AEIO-320 series. The LIO-320 is a "left-handed" version with the crankshaft rotating in the opposite direction for use on twin-engined aircraft to eliminate the [[critical engine]].<ref name="E274">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/B8D9638EAC0BE96786257085006EBBA3?OpenDocument&Highlight=320|title = TYPE CERTIFICATE DATA SHEET NO. E-274 Revision 20|accessdate = 2009-01-15|last = [[Federal Aviation Administration]]|authorlink = |date=September 2005}}</ref><ref name="1E12">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/BE788C9F926115C286256E4E006974C9?OpenDocument&Highlight=320|title = TYPE CERTIFICATE DATA SHEET NO. 1E12 Revision 9|accessdate = 2009-01-16|last = [[Federal Aviation Administration]]|authorlink = |date=December 2003}}</ref>

The first O-320 (with no suffix) was [[Federal Aviation Administration|FAA]] [[Type certification|certified]] on 28 July 1953 to CAR 13 effective 5 March 1952; this same engine was later re-designated, without change, as the O-320-A1A.<ref name="E274" /> The first IO-320 was certified on 10 April 1961, with the AIO-320 following on 23 June 1969 and the first aerobatic AEIO-320 on 12 April 1974. The LIO-320s were both certified on 28 August 1969.<ref name="E274" /><ref name="1E12" />

The O-320 family of engines externally resembles the [[Lycoming O-235]] and [[Lycoming O-290|O-290]] family from which they were derived. The O-320 shares the same {{convert|3.875|in|mm|0|abbr=on}} stroke as the smaller engines, but produces more power with the bore increased to {{convert|5.125|in|mm|0|abbr=on}}. The design uses hydraulic tappets and incorporates the provisions for a hydraulically controlled propeller installation as well. The controllable-pitch propeller models use a different crankshaft from those intended for fixed-pitch propellers.<ref name="Christy">Christy, Joe: ''Engines for Homebuilt Aircraft & Ultralights'', pages 77-80 TAB Books, 1983. ISBN 0-8306-2347-7</ref>

The O-320 uses a conventional [[wet sump]] system for lubrication. The main bearings, connecting rods, camshaft bearings, tappets and pushrods are all pressure lubricated, while the piston pins, cylinder walls and gears are all lubricated by spray. The oil system is pressurized by an accessory-drive mounted oil pump. A remotely mounted oil cooler is used, connected to the engine by flexible hoses.<ref name="Christy" />

The -A, -C and -E variants of the carbureted O-320, but none of the high compression or fuel-injected versions, have available [[Supplemental type certificate|STCs]] that allow the use of [[Avgas#Automotive gasoline|automotive fuel]] as a replacement for more expensive [[avgas]].<ref name="EAASTC">{{cite web|url = http://www.eaa.org/autofuel/autogas/approved.asp|title = Engine Models Approved|accessdate = 2009-06-06|last = [[Experimental Aircraft Association]]|authorlink = |year = 2009}}</ref> However, in some cases the D series engine, with a fuel-pump modification, is allowed to use minimum 91 octane automotive gas as part of an STC for the Piper aircraft to which it is attached.<ref>{{cite web|url = http://www.autofuelstc.com/piper_airplanes.phtml|title = Auto Fuel STC for Piper PA28 Aircraft|accessdate = 2014-05-22}}</ref>

The factory retail price of the O-320 varies by model. In 2010 the retail price of an O-320-B1A purchased outright was [[United States Dollar|USD$]]47,076<ref name="Pricing">{{cite web|url = http://www.lycoming.textron.com/utility/global-resources/2010-Aftermarket-Engine-Price-List.pdf|format=PDF|title = 2010 Lycoming Service Engine Price List|accessdate = 3 October 2010|last = [[Lycoming Engines]]|authorlink = |date=January 2010}}</ref>

==Variants==

===O-320 series===
;O-320 (No suffix) later redesignated O-320-A1A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 [[avgas]], compression ratio 7.00:1. Provisions for a controllable-pitch propeller and 25-degree spark advance.<ref name="E274" />
;O-320-A1B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A1A but with straight riser in oil sump and -32 carburetor.<ref name="Christy" />
;O-320-A2A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A1A but with fixed-pitch propeller.<ref name="Christy" />
;O-320-A2B 
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A2A but with straight riser in oil sump and -32 carburetor.<ref name="Christy" />
;O-320-A2C
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A2B but with retard breaker magnetos.<ref name="Christy" />
;O-320-A2D
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E3D but with conical mounts.<ref name="Christy" />
;O-320-A3A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A1A but with 7/16" prop bolts.<ref name="Christy" />
;O-320-A3B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A3A but with straight riser in oil sump and -32 carburetor.<ref name="Christy" />
;O-320-A3C
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A3B but with retard breaker magnetos.<ref name="Christy" />
;O-320-B1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as A1A but with high compression pistons.<ref name="Christy" />
;O-320-B1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B1A but with straight riser in oil sump and -32 carburetor.<ref name="Christy" />
;O-320-B2A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B1A but with fixed-pitch propeller provisions.<ref name="Christy" />
;O-320-B2B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B2A but with straight riser in oil sump and -32 carburetor.<ref name="Christy" />
[[File:Robinson R22 engine detail.jpg|thumb|right|An O-320-B2C mounted in a [[Robinson R22]] helicopter]]
;O-320-B2C
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B2B but with retard breaker magnetos.<ref name="Christy" />
;O-320-B2D
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1. Same as D1D but with conical engine mounts and no propeller governor.<ref name="E274" />
;O-320-B2E
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1. Same as B2B except the carburetor is in the same location as the O-320-D models.<ref name="E274" />
;O-320-B3A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B1A but with 7/16 inch propeller bolts.<ref name="Christy" />
;O-320-B3B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B1A but with 7/16 inch propellor bolts, a straight riser in oil sump, and -32 carburetor.<ref name="Christy" />
;O-320-B3C
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B3B but with retard breaker magnetos.<ref name="Christy" />
;O-320-C1A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B1A.<ref name="Christy" />
;O-320-C1B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B1B.<ref name="Christy" />
;O-320-C2A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B2A.<ref name="Christy" />
;O-320-C2B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B2B.<ref name="Christy" />
;O-320-C2C
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B2C.<ref name="Christy" />
;O-320-C3A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B3A.<ref name="Christy" />
;O-320-C3B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B3B.<ref name="Christy" />
;O-320-C3C
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Low compression version converted through field conversion of B3C.<ref name="Christy" />
;O-320-D1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as B3B but with Type 1 [[Dynafocal engine mount|dynafocal mounts]].<ref name="Christy" />
;O-320-D1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as D1A but with retard breaker magnetos.<ref name="Christy" />
;O-320-D1C
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as D2C but with provisions for a controllable propeller,<ref name="Christy" />
;O-320-D1D
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1. Same as the D1A but with Slick instead of Bendix magnetos and a horizontal carburetor and induction housing.<ref name="E274" /> This model was used in the [[Gulfstream American GA-7 Cougar]] twin.
;O-320-D1F
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E1F except with high compression pistons.<ref name="Christy" />
[[File:Lycoming O-320-D2A engine A.JPG|thumb|right|A brand new Lycoming O-320-D2A engine with baffles already mounted]]
;O-320-D2A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as D1A but with fixed-pitch propeller provisions and 3/8 inch attaching bolts.<ref name="Christy" /> Used in the [[Symphony SA-160]].
;O-320-D2B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as D2A but retard breaker magnetos.<ref name="Christy" />
;O-320-D2C
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100/130 or 91/96 avgas, compression ratio 8.50:1. Same as D2A except -1200 series magnetos.<ref name="Christy" />
;O-320-D2F
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2F except with high compression pistons.<ref name="Christy" />
;O-320-D2G
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1. Same as the D2A except with Slick instead of Bendix magnetos and 7/16 inch instead of 3/8 inch propeller flange bolts.<ref name="E274" />
;O-320-D2H
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1. Same as the D2G except with a O-320-B sump and intake pipes and with provisions for AC type fuel pump.<ref name="E274" />
;O-320-D2J
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1.Similar to the D2G but with two Slick impulse coupling magnetos and the propeller governor pad, fuel pump and governor pads on the accessory housing all not machined.<ref name="E274" /> Used in the [[Cessna 172|Cessna 172P]].
;O-320-D3G
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 avgas, compression ratio 8.50:1. Same as the D2G but with 3/8 inch propeller attaching bolts.<ref name="E274" />
;O-320-E1A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A3B but with Type 1 dynafocal mounts.<ref name="Christy" />
;O-320-E1B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E1A but with retard breaker magnetos.<ref name="Christy" />
;O-320-E1C
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E1B.<ref name="Christy" />
;O-320-E1F
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E1C but with propeller governor drive on the left front of the crankcase.<ref name="Christy" />
;O-320-E1J
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as the E1F but with Slick magnetos.<ref name="E274" />
;O-320-E2A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, or {{convert|140|hp|kW|0|abbr=on}} at 2450&nbsp;rpm Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E1A but with fixed-pitch propeller, 3/8 inch attaching bolts and an alternate power rating of {{convert|140|hp|kW|0|abbr=on}}.<ref name="Christy" />
;O-320-E2B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2A but with retard breaker magnetos.<ref name="Christy" />
;O-320-E2C
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, or {{convert|140|hp|kW|0|abbr=on}} at 2450&nbsp;rpm Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2A but -1200 series mags and an alternate power rating of {{convert|140|hp|kW|0|abbr=on}}.<ref name="Christy" />
;O-320-E2D
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Similar to E2A but with Slick magnetos and O-235 front.<ref name="Christy" /> Used in the [[Cessna 172]] I to M models.
;O-320-E2F
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E1F but with fixed pitch prop provisions.<ref name="Christy" />
;O-320-E2G
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2D but with O-320-A sump and intake pipes.<ref name="Christy" />
;O-320-E2H
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2D but with S4LN-20 and -21 magnetos.<ref name="Christy" />
;O-320-E3D
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2D but with 3/8 inch propeller flange bolts.<ref name="Christy" />
;O-320-E3H
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E3D but with S4LN-20 and -21 magnetos.<ref name="Christy" />
;O-320-H1AD
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100LL avgas, compression ratio 9.00:1. Integral accessory section crankcase, front-mounted fuel pump external mounted oil pump and D4RN-2O21 impulse coupling dual magneto.<ref name="E274" />
;O-320-H1BD
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100LL avgas, compression ratio 9.00:1. Same as H1AD but with a D4RN-2200 retard breaker dual magneto.<ref name="E274" />
;O-320-H2AD
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100LL avgas, compression ratio 9.00:1. Same as H1AD but with provisions for a fixed-pitch propeller.<ref name="E274" /> This was the troublesome engine that was installed on the 1977 to 1980 [[Cessna 172|Cessna 172N Skyhawk]].<ref name="C172NPOH">[[Cessna Aircraft]]: ''Pilot Operating Handbook 1977 Skyhawk Cessna Model 172N'', page 1-3. Cessna Aircraft, 1976.</ref>  It was notable from all other Lycoming models by incorporating [[hydraulic lifters]] that were barrel shaped instead of mushroom type, in an attempt to make the lifters possible to be serviced without having to disassemble the entire engine case, but the higher load on the [[cam]] lobes resulted in severe [[spalling]]. Multiple service bulletins and [[airworthiness directive]]s have been issued regarding this specific model, and multiple modifications exist to attempt to mitigate its design defects.<ref>Lycoming Service Bulletins #424 (new lifter modification), #446C, #435C, #1406B ("T" case modification), Ney Nozzles modification</ref><ref>{{cite web|url=http://cessnaowner.org/memforums/topic.html?id=8409|publisher=Cessna Owner Organization|title=O320-H2AD|date=February 23, 2004}}</ref><ref>{{cite web|url=http://generalaviationnews.com/2015/03/05/one-engine-many-questions/|title=One engine, many questions|date=March 5, 2015|first=Paul|last=McBride|publisher=General Aviation News}}</ref>
;O-320-H2BD
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100LL avgas, compression ratio 9.00:1. Same as the H2AD but with a D4RN-2200 retard breaker dual magneto.<ref name="E274" />
;O-320-H3AD
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100LL avgas, compression ratio 9.00:1. Same as the H2AD but with 3/8 inch propeller flange bolts, in place of instead of 7/16 inch.<ref name="E274" />
;O-320-H3BD
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 100LL avgas, compression ratio 9.00:1. Same as H3AD but with a D4RN-2200 retard breaker dual magneto.<ref name="E274" />

===IO-320 series===
;IO-320-A1A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Base model with a Bendix RSA -5AD1 fuel injection system.<ref name="1E12" />
;IO-320-A2A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A1A but with provisions for fixed-pitch propeller.<ref name="1E12" />
;IO-320-B1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the A1A but with the fuel injector offset toward the engine's fore and aft centerline.<ref name="1E12" />
;IO-320-B1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the A1A but with an AN fuel pump drive.<ref name="1E12" />
;IO-320-B1C
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the B1A but with an adapter for mounting the fuel injector straight to the rear.<ref name="1E12" />
;IO-320-B1D
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the B1C but with S-1200 series high altitude magnetos.<ref name="1E12" />
;IO-320-B1E
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the D1C except with a horizontal fuel injector.<ref name="1E12" />
;IO-320-B2A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the BIA but with provision for a fixed-pitch propeller.<ref name="1E12" />
;IO-320-C1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the B1B except it has features making it suitable for adding a turbo-supercharger via a [[Supplemental Type Certificate]] This engine has internal piston cooling oil nozzles.<ref name="1E12" />
;IO-320-C1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the C1A but with a horizontal rear-mounted fuel injector.<ref name="1E12" />
;IO-320-D1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the B1D but with Type 1 dynafocal mounts, S4LN-1227 and S4LN-1209 magnetos and the fuel injector mounted vertically under the oil sump.<ref name="1E12" />
;IO-320-D1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the D1A but with the propeller governor drive on the left front of crankcase instead of on the accessory housing.<ref name="1E12" />
;IO-320-D1C
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the D1B but with Slick Magnetos.<ref name="1E12" />
;IO-320-E1A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2A but with provision for a controllable-pitch propeller.<ref name="1E12" />
;IO-320-E1B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as  E1A but with Slick 4050 and 4051 magnetos.<ref name="1E12" />
;IO-320-E2A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as A2A but with Scintilla S4LN-20 and S4LN-21 magnetos,  straight conical mounts, and the fuel injector mounted under the oil sump.<ref name="1E12" />
;IO-320-E2B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as E2A but with Slick 4050 and 4051 magnetos.<ref name="1E12" />
;IO-320-F1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the C1A except with a Type 1 (30°) dynafocal mount attachment instead of Type 2 (18°) mount attachment.<ref name="1E12" />

===LIO-320 series===
;LIO-320-B1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as B1A except with counter-clockwise engine rotation and reverse rotation of accessories. It uses a modified starter ring gear, crankshaft, cam shaft, accessory housing and oil pump body. This engine is usually paired with an IO-320-B1A on a twin-engined aircraft.<ref name="1E12" />
;LIO-320-C1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as C1A except with the same changes as the LIO-320-B1A. It has provisions for adding a turbo-supercharger. This engine is usually paired with an IO-320-C1A on a twin-engined aircraft.<ref name="1E12" />

===AIO-320 series===
;AIO-320-A1A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the IO-320-B1D but this model permits installation and operation of the engine in the inverted position. The differences include a front-mounted propeller governor, two dry oil sumps, dual external oil scavenge pumps, an oil tank, three options for the position of the fuel injector and a Type 1 dynafocal mount.<ref name="1E12" />
;AIO-320-A1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the A1A but with one impulse coupling magneto.<ref name="1E12" />
;AIO-320-A2A
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the A1A but with provision for a fixed-pitch propeller.<ref name="1E12" />
;AIO-320-A2B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the A1A but has one impulse coupling magneto and a fixed-pitch propeller.<ref name="1E12" />
;AIO-320-B1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the A1B but with a front-mounted fuel injector.<ref name="1E12" />
;AIO-320-C1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the B1B but with the fuel injector vertically mounted on bottom of the oil sump in the front position.<ref name="1E12" />

===AEIO-320 series===
;AEIO-320-D1B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the IO-320-D1B but with an inverted oil system kit to allow aerobatic flight.<ref name="1E12" />
;AEIO-320-D2B
:{{convert|160|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 91/96 or 100LL avgas, compression ratio 8.50:1. Same as the AEIO-320-D1A but without provisions for a propeller governor.<ref name="1E12" />
;AEIO-320-E1A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as the IO-320-E1A but with an inverted oil system kit to allow aerobatic flight.<ref name="1E12" />
;AEIO-320-E1B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as the IO-320-E1B but with an inverted oil system kit to allow aerobatic flight.<ref name="1E12" />
;AEIO-320-E2A
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as the IO-320-E2A but with an inverted oil system kit to allow aerobatic flight.<ref name="1E12" />
;AEIO-320-E2B
:{{convert|150|hp|kW|0|abbr=on}} at 2700&nbsp;rpm, Minimum fuel grade 80/87 avgas, compression ratio 7.00:1. Same as the IO-320-E2B but with an inverted oil system kit to allow aerobatic flight.<ref name="1E12" />

==Applications==
[[File:Thorp T-18 amateur-built C-GEMP 02.JPG|thumb|right|The Lycoming O-320 is a common engine used by [[homebuilt aircraft|amateur-builders]] in the [[Thorp T-18]].]]

{{columns-list|3|
*[[Aviat Husky|Aviat Husky A-1B-160]]
*[[Avid Flyer]]
*[[Aero Commander 100]]
*[[Alpha 160A]]
*[[American Champion Citabria]]
*[[Australian Aircraft Kits Hornet STOL]]
*[[Australian Lightwing SP-4000 Speed]]
*[[Aviation Industries of Iran AVA-202]]
*[[Beagle Pup]] Series 2 and 3
*[[Beechcraft Musketeer]]
*[[Bede BD-12]]
*[[Bellanca Decathlon]]
*[[Bushcaddy L-160]]
*[[Canadian Home Rotors Safari]]
*[[Cessna 172]]
*[[Cessna 177]]
*[[Custom Flight North Star]]
*[[Christavia Mk IV]]
*[[Dakota Cub Super 18]]
*[[DRDO Rustom]]
*[[EM-11 Orka]]
*[[Eaves Cougar 1]]
*[[Explorer Ellipse]]
*[[Falconar F12A Cruiser]]
*[[Farrington Twinstar]]
*[[Fly-Fan Shark]]
*[[Glasair GlaStar]]
*[[Grumman American AA-5]]
*[[Gulfstream American GA-7 Cougar]]
*[[Hatz CB-1]]
*[[Hatz Classic]]
*[[Lambert Mission 212]]
*[[MBB Bo 209]]
*[[Mooney M20]]
*[[Murphy Elite]]
*[[Mustang Aeronautics Mustang II]]
*[[Osprey Osprey 2]]
*[[Partenavia P66B Oscar 150]]
*[[Peña Dahu]]
*[[PIK-15]]
*[[PIK-19]]
*[[Piper Aztec]]
*[[Piper Apache]]
*[[Piper PA-40 Arapaho]]
*[[Piper Twin Comanche]]
*[[Piper Cherokee]]
*[[Piper PA-18 Super Cub]]
*[[Piper PA-20 Pacer|Piper Tripacer]]
*[[Preceptor STOL King]]
*[[PZL-110 Koliber]]
*[[Quikkit Glass Goose]]
*[[Rihn DR-107 One Design]]
*[[Robin DR400]]
*[[Robinson R22]]
*[[Rutan Long-EZ]]
*[[Sands Fokker Dr.1 Triplane]]
*[[SGP M-222 Flamingo]]
*[[Socata TB9 Tampico]]
*[[Symphony SA-160]]
*[[Tapanee Levitation 2]]
*[[Thorp T-18]]
*[[Piper PA-18-150 Super Cub]]
*[[Van's Aircraft RV-3]]
*[[Van's Aircraft RV-4]]
*[[Van's Aircraft RV-6]]
*[[Van's Aircraft RV-8]]
*[[Van's Aircraft RV-9]]
*[[Varga Kachina]]
*[[Velocity V-Twin]]
*[[Vortech Shadow]]
*[[Vulcanair P-68C]]
*[[Wassmer WA 52]]
*[[Wickham B]]
*[[Wittman Tailwind]]
}}

== Specifications (O-320-A1A) ==
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully formatted line with <li> -->
|ref=TYPE CERTIFICATE DATA SHEET NO. E-274 Revision 20<ref name="E274" />
|type=Four-cylinder air-cooled [[flat engine|horizontally opposed engine]]
|bore={{convert|5.125|in|mm|2|abbr=on}}
|stroke={{convert|3.875|in|mm|2|abbr=on}}
|displacement={{convert|319.8|cuin|l|2|abbr=on}}
|length=
|diameter=
|width=
|height=
|weight={{convert|244|lb|kg|0|abbr=on}}
|valvetrain=Two [[overhead valve]]s per cylinder
|supercharger=
|turbocharger=
|fuelsystem=[[Updraft carburetor]]
|fueltype=minimum grade of 80/87 [[avgas]]
|oilsystem=Wet sump
|coolingsystem=Air-cooled
|power={{convert|150|hp|kW|0|abbr=on}}
|compression=7:1
|specpower=
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=1.63 lb/hp (0.99 kW/kg)
|reduction_gear=
}}

==See also==
*[[List of aircraft engines]]

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
{{refend}}

==External links==
{{Commons category}}
*[http://www.lycoming.com/Lycoming/PRODUCTS/Engines/Certified/320Series.aspx Lycoming - O-320 page]
{{Lycoming aeroengines}}
{{US military piston aeroengines}}

{{Use dmy dates|date=February 2011}}

[[Category:Boxer engines]]
[[Category:Lycoming aircraft engines|O-320]]
[[Category:Aircraft piston engines 1950–1959]]
[[Category:1953 introductions]]