{{more footnotes|date=November 2010}}

{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Franklin Evans, or The Inebriate: A Tale of the Times
| title_orig    = 
| translator    = 
| image         = Franklinevans.jpg
| image_size = 176px
| caption = Title page, Reprint edition
| author        = [[Walt Whitman]]
| illustrator   = 
| series        = 
| genre         = Temperance
| publisher     = 
| pub_date      = 1842
| english_pub_date =
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}

'''''Franklin Evans; or The Inebriate''''', is a [[temperance movement|temperance]] novel by [[Walt Whitman]] first published in 1842.<ref name="Luloff 1988">{{cite web|last=Lulloff|first=William G.|title=Franklin Evans; or The Inebriate |url=http://www.whitmanarchive.org/criticism/current/encyclopedia/entry_81.html|work=Criticism|publisher=The Walt Whitman Encyclopedia|accessdate=28 November 2010}}</ref>

==Plot overview==
''Franklin Evans or The Inebriate: A Tale of the Times'', the first novel written by [[Walt Whitman]], is the rag-to-riches story of Franklin Evans. Franklin Evans starts as an innocent young man, leaving [[Long Island]] to come to [[New York City]] for the opportunity to better himself. Being young and naïve, he is easily influenced by someone whom he befriended (Colby) and eventually becomes a drunkard. He tries many times to abstain from [[alcohol]] but does not succeed until after the death of his two wives. Franklin Evans takes you through a journey of a young man living and learning through his mistakes, picking up life lessons along the way.

==Contexts==

<!-- Deleted image removed: Waltwhitman.jpg|thumb|left|<center>[[Walt Whitman]]</center>]] -->

An introduction to a modern reprint of ''Franklin Evans'', written by Christopher Castiglia and Glenn Hendler, serves as a detailed preface to the novel. It incorporates a section about Walt Whitman's early life to give readers an understanding about why he took up writing in the first place.  After his childhood, he operated a hand press for both the Long Island ''Patriot'' and the ''Star'' until his family returned to rural [[Long Island]].  He soon began a journalistic career until becoming a teacher in 1836.  He earned a very small wage with which he was not satisfied, so he began his own weekly paper. From here, a concrete writing career began, and influential authors began to target Whitman's work, including Park Benjamin, who published ''Franklin Evans'' in his own paper.

The authors also touch on Whitman's [[journalism]] of the 1840s. They believe it represents the state of [[New York (state)|New York]] in a time of transition. Referencing one of Whitman's pieces of literature, they write one of his statements regarding Chatham Square: "In the middle are dray carts, coaches, and cabs: on the right, loom up small hills of [[furniture]], of every quality, with here and there an auctioneer, standing on a table or barrel top, and crying out to the crowd around him, the merits of the articles, and the bids made for them." This statement contains a great deal of nostalgia from past times, which alludes to change in social mobility from a changing [[economy]]. 
 
Overall, Castiglia and Hendler offer a special focus on labor and social reform of the times as possible reasons for the novel. It is difficult for readers to formulate a rationale for why Whitman would write a book about a man ruining his life by indulging in intemperance.  At the time, working-class individuals who read newspapers like Benjamin's were becoming interested in tales of temperance. About 12 percent of American novels published during the 1830s concerned temperance fiction. Conforming to these social demands of the time period, Benjamin published Whitman's story in his newspaper as a continuous series. This kept readers consistently wanting to read every issue, and more buyers of Benjamin’s paper. Hendler and Castiglia mention how Whitman did indeed oppose legal restraints on [[alcohol]] consumption because he believed they would not prove effectual, and how he was not "one of those who would deny people any sense of delight."  However, some of his literature up to 1840 had been used to discourage all forms of intemperance.  Therefore, it is safe to say that Whitman only believed temperance would succeed if it was reformed. This is why ''Franklin Evans'' has been referred to as a supplement to America's temperance reform. It is Whitman's direct example of why intemperance is such a dangerous act, and needed to be rectified with [[regulations]] different from those that already were not successful.

== Themes ==
'''Success after Struggle:'''
Whitman conveys this theme through an entire [[novel]] of hardship.  After battling times of intemperance since he first arrived in New York, Evans finally achieves some sort of satisfaction at the end.  As a middle aged man at the conclusion of the novel, Evans states: “So, at an age which was hardly upon the middle verge of [[life]], I found myself possessed of a comfortable property; and, as the term is ‘unincumbered’ person-which means I have no wife to love me-no [[children]] to please me, and be the recipients of my own affection, and no domestic hearth around which we might gather, as the center of joy and delight.” (Whitman 107).  Readers can see that for someone as ill-behaved as Evans, whose poor judgement lost him nearly everything in his life, amends could still be made.  He came to terms with his life and set himself on a reformed path.  It seemed as though nothing remotely good could come out of Evans's situation after lost jobs, wives, and [[money]], but that would have taken away from Whitman's lesson that immense struggle will be balanced out with sort of [[Social status|success]], or in Evans's case, content frame of mind.

'''Change is Always Possible:'''
Coinciding with Success after Struggle, Whitman conveys both themes in conjunction with one another.  From the moment Evans took his first drink to the countless losses he experienced throughout the duration of the [[novel]], it was tough to have much hope for him.  He continuously chose intemperance over important aspects of life-for instance [[marriage]].  However, with the guidance of the Marchion family and some solid effort on his part, Evans changed his life for the better.  After finally experiencing some well due good fortune, Evans displays some newfound maturity with his statement "My [[country]] relations were not forgotten by me in my good fortune.  The worthy [[uncle]], who had kindly housed and fed me when I was quite too small to make him any repayment for that service, received in his old age the means to render his life more easy and happy.  My cousins too, had no reason to be sorry for the good-will which they had ever shown toward me.  I was never the [[person]] to forget a friend, or leave unrequited a favor, when I had the payment of it in my power." (Whitman 109).  Months and even years previous, it probably was not even possible for Evans to utter words of recognition.  After realizing his faults and making the changes for a good life, Evans takes the time to acknowledge those who made a positive mark on his life.  Even for someone with as many issues as Franklin Evans had, [[Personal development|change]] is always possible.

==Characters==
'''Franklin Evans -''' The main [[character (arts)|character]] of the novel.

'''Colby -''' The first [[person]] whom Evans befriended.  He introduced Evans to [[music]] halls, theaters and taverns when he first gets to the [[city]].  In the end of the novel Evans sees Colby being made a fool of by some [[children]] for money.  This image helps Evans reflect on what he could have become had he not quit drinking.

'''Mr. Lee -''' A [[gentleman]] who took a liking toward Evans.  He helped him to get a job when first getting to the city and watched his life unfold from a distance.  While on his [[death]] bed, Lee called for Evans to come see him and told him that he had left his [[Wealth|fortune]] to him.  Like his late wife, Evans was a drunkard but Evans did not let it take him to his [[grave (burial)|grave]].  Mr. Lee felt that when Evans came to the [[city]] he should have watched over him as a [[father]] would and since he failed to do so he wanted to make up for his neglect.

'''Mary -''' Evans’ first [[wife]].  Evans and Mary are truly in love but she dies because of his neglect toward her.

'''The Marchion Family-'''  After saving their [[daughter]] from drowning, the Marchion family helps Evans when he is thrown in jail and help him, both times he tried to abstain from alcohol.

'''Bourne -'''  This is the friend whom Evans stays with when he leaves the city and reintroduces him to drink.

'''Margaret -''' She was a [[slave]] but was giving her freedom when Evans decided he wanted to marry her.  Their [[marriage]] was not very pleasant because Evans did not really [[love]] her; he only thought he did in his drunken state.  Evans’ love for another woman leads her to commit murder and then [[suicide]].

'''Mrs. Conway -''' After marrying Margaret, Evans meets Mrs. Conway and takes an instant liking to her.  He would spend more time with her than with his [[wife]] and would do anything for her.  Knowing that Evans would never say no to her, she asks for the [[brother]] of Margaret to be her servant.  Evans complies with her and causes so much anger in Margaret that she plots to kill Mrs. Conway in her quest for revenge.  When her plan backfires and her brother becomes ill and dies, Margaret decides to strangle her.  Not being able to live with the guilt, she kills herself.

==Criticism==
Two critics of Franklin Evans are [[Michael Warner]] and William Lulloff. In his paper, "Whitman Drunk", Warner addresses a variety of the problems he has with Walt Whitman and Franklin Evans. Lulloff does the same in a shorter piece of criticism in "The Walt Whitman Encyclopedia".

'''Whitman Drunk -''' Readers today often find Franklin Evans very unsatisfactory due to the fact that it was originally published in [[newspapers]], Michael Warner states. When Whitman is talking about [[alcohol]] in Franklin Evans, he often seems to be thinking about something else. Most of the time, the idea of pleasure seems to be stuck in his [[brain]]. For example, he writes "Oh, fatal ''pleasure''", not "Oh, fatal ''alcohol''". Whitman seems to make a great correlation between will and desire in his novel. Warner states that the [[temperance movement|temperance]] movement was one of a non-state society and Franklin Evans helped to mediate constitutive self-understanding. Commissioning [[fiction]] as propaganda had been a part of the public strategy of the temperance movement since 1836, and Whitman went along with this idea. Because of this, both the temperance movement and Franklin Evans are in a context of non-state political association. Warner argues that hardly any bad situations follow directly after the consumption of alcohol in the plot of the story. The disasters of [[marriage]], impulse, and a creole slave turning into a homicidal madwoman do not have to do with alcohol. Alcohol had so little to do with the story that Whitman had to change the title twice. The theme of the novel seems to be addiction, not alcoholism. It is not hard to hear some reference to the same-sex subculture of [[New York City]] in many passages. When talking about drunk men, Whitman focuses on the description of the men, not the alcohol they have consumed. This idea of sexuality is the main focus of many of Whitman's poems, too.

'''The Walt Whitman Encyclopedia -''' In this piece of writing, William Lulloff starts off by stating that, at first, Walt Whitman disguised his authorship of the [[novel]] by assigning his name to be "J.R.S.". This may have been because Whitman claims to have completed the novel in three days. Whitman even included short stories that he had previously written to shorten the amount of actual writing he had to do. Some of these possible short stories were about the Indian in chapter 2, "Little Jane" in chapter 14, and the allegorical dream in chapter 21. Lulloff states that Whitman had called his novel "damned rot--rot of the worst sort", and many people agree with this assessment. Lulloff also makes a point that this novel was meant to be in [[newspapers]], so it does not flow like a [[book]] should. This should not surprise anyone, though, as Whitman states in the introduction that this novel "is not written for the critics but for the people".

== Notes==
{{reflist}}

== References ==

* Lulloff, William G. "Franklin Evans; or The Inebriate." The Walt Whitman Archive. Web. 30 Nov. 2010. <http://www.whitmanarchive.org/criticism/current/encyclopedia/entry_81.html>.
* Warner, Michael. "Whitman Drunk." Breaking Bounds. New York: Oxford UP, 1996. 30-43. Print.
* Whitman, Walt, Christopher Castiglia, and Glenn Hendler. "Introduction." Introduction. Franklin Evans, or The Inebriate a Tale of the Times. Durham: Duke UP, 2007. Print.
* Whitman, Walt, Christopher Castiglia, and Glenn Hendler. Franklin Evans, or The Inebriate a
	Tale of the Times. Durham: Duke UP, 2007. Print.

== External links ==
*{{cite web|last=Wulff|first=Alex|title=Review of Whitman, *Franklin Evans*|url=http://rmmla.wsu.edu/ereview/62.1/reviews/wulff.asp|publisher=Rocky Mountain Modern Language Association|accessdate=}}

{{Walt Whitman}}

[[Category:1842 novels]]
[[Category:Temperance movement]]
[[Category:Works by Walt Whitman]]
[[Category:Novels set in New York]]