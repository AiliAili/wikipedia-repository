{{Infobox Archbishop
| name = Deusdedit
| title = [[Archbishop of Canterbury]]
| image =Gravesites_of_Mellitus,_Justus_and_Laurence_at_St._Augustine's_Abbey.jpg
| imagesize = 250px
| alt = 
| caption = The location of Deusdedit's unmarked grave, at [[St Augustine's Abbey]] in [[Canterbury]]. The graves marked with stones are those of [[Justus]], [[Mellitus]], and [[Laurence of Canterbury|Laurence]].
| appointed = 655
| ended = [[circa|c.]] 664
| predecessor = [[Honorius of Canterbury|Honorius]]
| successor = [[Wighard]]
| consecration = March 655
| consecrated_by = [[Ithamar (bishop)|Ithamar]]
| birth_name = perhaps Frithona
| birth_date = unknown
| birth_place = England
| death_date = c. 664
| buried = St Augustine's Abbey, Canterbury
| feast_day = 14 July
| venerated = [[Eastern Orthodox Church]]<br>[[Roman Catholic Church]]<br>[[Anglican Communion]]
| canonized_date = [[Pre-congregation|Pre-Congregation]]
}}

'''Deusdedit''' (died [[circa|c.]] 664) was a medieval [[Archbishop of Canterbury]], the first native-born holder of the [[see of Canterbury]]. By birth an [[Anglo-Saxons|Anglo-Saxon]], he became archbishop in 655 and held the office for more than nine years until his death, probably from [[plague (disease)|plague]]. Deusdedit's successor as archbishop was one of his priests at Canterbury. There is some controversy over the exact date of Deusdedit's death, owing to discrepancies in the medieval written work that records his life. Little is known about his episcopate, but he was considered to be a [[saint]] after his demise. A [[hagiography|saint's life]] was written after his [[relic]]s were moved from their original burial place in 1091.

==Life==
A post-[[Norman conquest of England|Norman Conquest]] tradition, originating with [[Goscelin]],<ref name=DNB/> gives Deusdedit's original name as ''Frithona'', possibly a corruption of Frithuwine.<ref name=Brooks67>Brooks ''Early History of the Church of Canterbury'' pp. 67–69</ref>{{efn|The modern historian [[Peter Hunter Blair]] gives the name as Frithonas.<ref>Blair ''World of Bede'' p. 118</ref>}} He was consecrated by [[Ithamar (bishop)|Ithamar]], [[Bishop of Rochester]], on 26 March<ref name=Handbook213>Fryde, et al. ''Handbook of British Chronology'' p. 213</ref> or perhaps 12&nbsp;March 655.<ref name=Abels20/> He was the sixth archbishop after the arrival of the [[List of members of the Gregorian mission|Gregorian missionaries]],<ref name=Bede176/> and the first to be a native of the island of Great Britain rather than an Italian, having been born a [[Wessex|West Saxon]].<ref name=Brooks67/><ref name=Hindley45>Hindley ''Brief History of the Anglo-Saxons'' p. 45</ref> One reason for the long period between the [[Christianization]] of the Kentish kingdom from [[Anglo-Saxon paganism]] in about 600 and the appointment of the first native archbishop may have been the need for the schools established by the [[Gregorian mission]]aries to educate the natives to a sufficiently high standard for them to take ecclesiastical office.<ref name=BASE>Lapidge "Deusdedit" ''Blackwell Encyclopaedia of Anglo-Saxon England'' p. 140</ref> Deusdedit probably owed his appointment to the [[see of Canterbury]] to a collaboration between [[Eorcenberht of Kent]] and [[Cenwalh of Wessex]].<ref name=Brooks67/> The name Deusdedit means "God has given" in Latin,<ref>Müller ''God the Teacher of Mankind'' p. 201</ref> and had been the name of a recent pope,<ref name=DNB/> [[Pope Adeodatus I|Deusdedit]], in office from 615 to 618;<ref name=Ithamar890>Sharpe "Naming of Bishop Ithamar" ''English Historical Review'' p. 890</ref> it was the practice of many of the early medieval Saxon bishops to take an adopted name, often from recent papal names.<ref name=DNB/> It is unclear when Deusdedit adopted his new name, although the historian Richard Sharpe considers it likely to have been when he was consecrated as an archbishop, rather than when he entered [[Consecrated life|religious life]].<ref name=Ithamar890/>

The see of Canterbury seems at this time to have been passing through a period of comparative obscurity.<ref name=DNB>Thacker "Deusdedit" ''Oxford Dictionary of National Biography''</ref> During Deusdedit's nine years as archbishop, all the new bishops in England were consecrated by [[Celtic Christianity|Celtic]] or foreign bishops, with one exception:<ref name=Hindley96/> Deusdedit consecrated [[Damianus]], Ithamar's successor as Bishop of Rochester.<ref name=DNB/> Deusdedit did, however, found a [[nunnery]] in the [[Isle of Thanet]] and helped with the foundation of [[Medeshamstede]] Abbey, later [[Peterborough Cathedral|Peterborough Abbey]], in 657.<ref name=Hindley96>Hindley ''Brief History of the Anglo-Saxons'' p. 96</ref> He was long overshadowed by [[Agilbert]], bishop to the West Saxons,<ref name=ASE122>Stenton ''Anglo-Saxon England'' p. 122</ref> and his authority as archbishop probably did not extend past his own diocese and that of [[Diocese of Rochester|Rochester]], which had traditionally been dependent on Canterbury.<ref name=DNB/>

The [[Synod of Whitby]], which debated whether the [[Northumbria]]n church should follow the Roman or the Celtic [[Computus|method of dating]] [[Easter]], was held in 664.<ref name=Hindley79>Hindley ''Brief History of the Anglo-Saxons'' pp. 79–81</ref> Deusdedit does not appear to have been present, perhaps because of an outbreak of the plague prevalent in England at the time.<ref name=ASE129>Stenton ''Anglo-Saxon England'' p. 129</ref>

==Death==
Deusdedit died at some time around the Synod of Whitby, although the exact date is disputed.<ref name=Abels20/> [[Bede]], in the ''[[Historia ecclesiastica gentis Anglorum]]'', states that "On the fourteenth of July in the above mentioned year, when an eclipse was quickly followed by plague and during which Bishop [[Colmán of Lindisfarne|Colman]] was refuted by the unanimous decision of the Catholics and returned to his own country, Deusdedit the sixth Archbishop of Canterbury died."<ref name=Bede203>Bede ''History of the English Church'' p. 203</ref> A solar eclipse occurred on 1 May 664, which would appear to make the date of Deusdedit's death 14&nbsp;July 664. But that conflicts with Bede's own information earlier in the ''Historia'',<ref name=Abels20>Abels "Council of Whitby" ''Journal of British Studies'' pp. 20–22</ref> where he claims that Deusdedit's predecessor, [[Honorius of Canterbury|Honorius]], "died on the 30th of September 653, and after a vacancy of 18 months, Deusdedit, a West Saxon was elected to the archiepiscopal see and became the 6th Archbishop. He was consecrated by Ithamar, Bishop of Rochester, on the 26th of May, and ruled the see until his death nine years, four months, and two days later."<ref name=Bede176>Bede ''History of the English Church'' p. 176</ref> If this information is accurate, then Deusdedit must have died on 28&nbsp;July 664.<ref name=Abels20/> Various methods of reconciling these discrepancies have been proposed. [[Frank Stenton]] argues that Bede began his years on 1 September; thus the date of Honorius' death should be considered 30&nbsp;September 652 in modern reckoning. Further, Stenton argued that medieval copyists had introduced an error into the manuscripts of the ''Historia'', and that Bede meant that the length of Deusdedit's reign was 9 years and 7 months, rather than 9 years and 4 months as stated in the manuscripts. From this, he concludes that Deusdedit's death occurred in the year September 663 to September 664. This would make the year of death correct according to the eclipse, but still leave a discrepancy on the specific day of death, for which Stenton asserted the length calculations given by Bede were more correct than the actual death date given. Thus Stenton concluded that Deusdedit died on 28 October 663.<ref name=ASE129/>

Other historians, including Richard Abels, P. Grosjean, and Alan Thacker, state that Deusdedit died on 14 July 664.<ref name=DNB/><ref name=Abels20/><ref name=Wood>Wood "Bede's Northumbrian Dates" ''English Historical Review'' p. 289</ref> The main argument was put forward by Grosjean, who claimed that Bede had the consecration date wrong, as 26 May was [[Maundy Thursday]] in 655, not a date that would normally have been chosen for a consecration. Grosjean argues that the best method for resolving the conflicts is to just take 14&nbsp;July 664 as the date of death, and figure backwards with the length of reign given by Bede, which gives a consecration date of 12&nbsp;March 655.<ref name=Abels20/> Thacker and Abels agree generally, although Thacker does not give a specific consecration date beyond March.<ref name=DNB/><ref name=Abels20/> Abels adds to Grosjean's arguments Bede's association of Deusdedit's death with that of King Eorcenberht, which Bede gives as occurring on the same day. Bede states that the plague of 664 began soon after the eclipse on 1&nbsp;May. Nothing in Bede contradicts the date of 14&nbsp;July 664 for Eorcenberht; therefore, Abels considers that date to be the best fit for the available data.<ref name=Abels20/> The historian D. P. Kirby agrees that Deusdedit died in 664, although he does not give a precise date within that year.<ref name=Kirby519>Kirby "Bede and Northumbrian Chronology" ''English Historical Review'' p. 519</ref>

Most historians state that Deusdedit died of the plague that was prevalent in England at the time.<ref name=DNB/><ref name=Abels20/><ref name=Plague15>Maddicott "Plague" ''Past and Present'' p. 15</ref> Because Bede records the death of Deusdedit shortly after he mentions the outbreak of the plague, the historian [[John Maddicott|J. R. Maddicott]] asserts that both Deusdedit and Eorcenberht were struck suddenly with the disease and died quickly.<ref name=Plague15/> Bede is not specific on the type of plague, but Maddicott argues that because of the time of its eruption and the way it arrived in England it was probably [[bubonic plague]]. Although Bede does not describe either Eorcenberht or Deusdedit's symptoms he does discuss another victim of the 664 disease, who suffered from a tumour on his thigh, resembling the characteristic groin swellings of bubonic plague.<ref name=Plague20>Maddicott "Plague" ''Past and Present'' pp. 20–22</ref>

==Legacy==
Except for the bare facts of his life, little is known about Deusdedit.<ref name=DNB/><ref name=BASE/> Deusdedit's successor as Archbishop of Canterbury, [[Wighard]], had been one of his clergy.<ref name=Abels14>Abels "Council of Whitby" ''Journal of British Studies'' p. 14</ref> Deusdedit was regarded as a saint after his death, with a feast day of 14&nbsp;July,<ref name=DictSaint>Delaney ''Dictionary of Saints'' p. 177</ref> although the [[Bosworth Psalter]], a late 10th or early 11th-century [[psalter]] produced at {{nowrap|[[St Augustine's Abbey]]}}, gives a date of 15 July.<ref name=DNB/> His feast day is designated as a major feast day, and is included along with those of a number of other early Canterbury archbishops in the Bosworth Psalter.<ref name=Bosworth175>Kornhammer "Origin of the Bosworth Psalter" ''Anglo-Saxon England 2'' p. 175</ref> Deusdedit was buried in the church of St Augustine's in Canterbury, but was [[translation (relic)|translated]] to the new abbey church in 1091. A [[hagiography]], or saint's biography, on Deusdedit was written by Goscelin after the translation of his relics, but the work was based mainly on Bede's account;<ref name=DNB/> the manuscript of the ''De Sancto Deusdedit Archiepiscopo'' survives as part of [[British Library]] manuscript (ms) [[Cotton library|Cotton]] Vespasian B.xx.<ref name=BL>Staff "[http://www.bl.uk/catalogues/manuscripts/HITS0001.ASP?VPath=html/73767.htm&Search=Cott.+Vesp.+B.+xx.&Highlight=F Full Description: [[Cotton Vespasian]] B.xx]" ''Manuscripts Catalogue''</ref> Because of the late date of the ''Sancto'', Bede's ''Historia'' is the main source for what little is known about Deusdedit.<ref name=BASE/> Other than the hagiography, there is scant evidence of a cult surrounding him.<ref name=DNB/> His shrine survived until the [[dissolution of the monasteries]] in the 1530s.<ref name=ODS144>Farmer ''Oxford Dictionary of Saints'' p. 144</ref>

==Note==
{{notelist}}

==Citations==
{{reflist|40em}}

==References==
{{refbegin|colwidth=60em}}
* {{cite journal |author=Abels, Richard  |date=Autumn 1983  |title=The Council of Whitby: A Study in Early Anglo-Saxon Politics |journal=[[Journal of British Studies]] |volume=23 |issue= 1|pages=1–25  |doi=10.1086/385808 |jstor= 175617}}
* {{cite book |author1=Bede |authorlink1=Bede |others= Sherley-Price. Leo (translator) |title=A History of the English Church and People |publisher=Penguin Classics |location=London |year=1988 |isbn=0-14-044042-9 }}
* {{cite book |author=Blair, Peter Hunter |authorlink= Peter Hunter Blair |title=The World of Bede |publisher=Cambridge University Press |location=Cambridge, UK |year=1990 |edition=Reprint |origyear= 1970 |isbn=0-521-39819-3 }}
* {{cite book |author=Brooks, Nicholas |authorlink=Nicholas Brooks |title=The Early History of the Church of Canterbury: Christ Church from 597 to 1066 |publisher=Leicester University Press |location=London |year=1984 |isbn=0-7185-0041-5}}
* {{cite book |author=Delaney, John P. |title=Dictionary of Saints|edition=Second |publisher=Doubleday |location=Garden City, NY |year=1980 |isbn=0-385-13594-7 }}
* {{cite book| author=Farmer, David Hugh |title=Oxford Dictionary of Saints |publisher=Oxford University Press |year=2004 |edition=Fifth |location=Oxford, UK |isbn= 978-0-19-860949-0}}
* {{cite book |author1=Fryde, E. B. |author2=Greenway, D. E. |author3=Porter, S. |author4=Roy, I. |title=Handbook of British Chronology|edition=Third revised |publisher=Cambridge University Press |location=Cambridge, UK |year=1996 |isbn=0-521-56350-X }}
* {{cite book |author= Hindley, Geoffrey|title=A Brief History of the Anglo-Saxons: The Beginnings of the English Nation  |year= 2006|publisher= Carroll & Graf Publishers |location=New York |isbn=978-0-7867-1738-5 }}
* {{cite journal |author=Kirby, D. P.  |date=July 1963  |title=Bede and Northumbrian Chronology |journal=[[The English Historical Review]] |volume=78 |issue= 308 |pages=514–527 |doi= 10.1093/ehr/LXXVIII.CCCVIII.514 |jstor= 562147}}
* {{cite encyclopedia |author=Kornhammer, P. M. |title=The Origin the Bosworth Psalter|encyclopedia=Anglo-Saxon England 2 |year=1973 |pages=173–187|doi= 10.1017/S0263675100000417 |publisher=Cambridge University Press |location=Cambridge, UK }}
* {{cite encyclopedia |author=Lapidge, Michael |authorlink=Michael Lapidge |title=Deusdedit |page=140|encyclopedia=The Blackwell Encyclopaedia of Anglo-Saxon England |editors=Lapidge, Michael; Blair, John; [[Simon Keynes|Keynes, Simon]]; Scragg, Donald |year=2001 |publisher=Blackwell Publishing |location=Malden, MA |isbn=978-0-631-22492-1 }}
* {{cite journal |author=Maddicott, J. R. |authorlink= John Maddicott |title=Plague in Seventh-Century England|journal =Past and Present |number= 156 |date=August 1997 |pages=7–54 |jstor= 651177 |doi= 10.1093/past/156.1.7}}
* {{cite book |author=Müller, Michael |title=God the Teacher of Mankind: or, Popular Catholic Theology, Apologetical, Dogmatical, Moral, Liturgical, Pastoral, and Ascetical|volume= 3|location= New York |publisher= Benziger Brothers|year= 1881}}
* {{cite journal |author=Sharpe, R. |authorlink= Richard Sharpe (historian) |doi=10.1093/ehr/117.473.889  |title=The Naming of Bishop Ithamar |journal=[[The English Historical Review]] |volume=117 |issue=473 |date=September 2002 |pages=889–894 }}
* {{cite web |author=Staff |url=http://www.bl.uk/catalogues/manuscripts/HITS0001.ASP?VPath=html/73767.htm&Search=Cott.+Vesp.+B.+xx.&Highlight=F |title=Full Description: Cotton Vespasian B.xx|work= Manuscripts Catalogue |publisher=British Library |accessdate=3 July 2011}}
* {{cite book |author=Stenton, F. M. |authorlink= Frank Stenton  |title= Anglo-Saxon England |year= 1971|publisher= Oxford University Press |location=Oxford, UK |edition=Third |isbn=978-0-19-280139-5 }}
* {{cite encyclopedia |author=Thacker, Alan |title=Deusdedit (d. 664)| encyclopedia=Oxford Dictionary of National Biography|publisher= Oxford University Press |year=2004|url=http://www.oxforddnb.com/view/article/7560 |doi= 10.1093/ref:odnb/7560 |accessdate=21 August 2010}} {{ODNBsub}}
* {{cite journal |author=Wood, Susan |title=Bede's Northumbrian Dates Again|journal=[[The English Historical Review]] |volume=98 |issue=387 |date=April 1983|pages=280–296 |jstor= 569438 |doi= 10.1093/ehr/XCVIII.CCCLXXXVII.280}}
{{refend}}

==Further reading==
{{refbegin}}
* {{cite encyclopedia |author= Orchard, N. |title= The Bosworth Psalter and the St Augustine's Missal |encyclopedia=Canterbury and the Norman Conquest |editor1=Eales, R. |editor2=[[Richard Sharpe (historian)|Sharpe, R.]] |year=1995|pages= 87–94 |isbn= 1-85285-068-X |publisher = Hambledon Press |location=London}}
* {{cite encyclopedia |author= Sharpe, R. |authorlink= Richard Sharpe (historian) |title=The Setting to St Augustine's Translation, 1091 |encyclopedia=Canterbury and the Norman Conquest |editor1=Eales, R. |editor2=Sharpe, R. |year=1995|pages= 1–13 |isbn= 1-85285-068-X |publisher = Hambledon Press |location=London}}
{{refend}}

==External links==
* {{PASE|7792|Deusdedit 1}}

{{s-start}}
{{s-rel| [[Christianity|Christian]] titles }}
{{s-bef | before=[[Honorius of Canterbury|Honorius]] }}
{{s-ttl| title=[[Archbishop of Canterbury]] | years=655–[[circa|c.]] 664}}
{{s-aft| after=[[Wighard]] }}
{{s-end}}

{{Archbishops of Canterbury}}
{{Anglo-Saxon saints}}

{{featured article}}
{{Authority control}}

{{DEFAULTSORT:Deusdedit of Canterbury}}
[[Category:660s deaths]]
[[Category:7th-century archbishops]]
[[Category:7th-century Christian saints]]
[[Category:Archbishops of Canterbury]]
[[Category:Kentish saints]]
[[Category:West Saxon saints]]
[[Category:Year of birth unknown]]