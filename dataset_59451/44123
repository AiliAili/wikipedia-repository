{{Infobox airport
| name         = Barentsburg Heliport, Heerodden
| nativename   = 
| image        = Barentsburg Heliport overview.jpg
| image-width  = 300px
| caption      = 
| IATA         =
| ICAO         = ENBA
| type         = Private
| owner        = 
| operator     = [[Arktikugol]]
| city-served  = [[Barentsburg]], [[Svalbard]], [[Norway]]
| location     = [[Heerodden]]
| metric-elev  = yes
| elevation-f  = 25
| elevation-m  = 92
| website      = 
| coordinates  = {{coord|78|06|02|N|14|11|46|E|region:NO|display=inline,title}}
| pushpin_map            = Svalbard
| pushpin_label          = '''ENBA'''
| pushpin_label_position = bottom
| pushpin_map_caption    = Location within [[Svalbard]]
| metric-rwy   = yes
| h1-number    = 09–27
| h1-length-m  = 90
| h1-length-f  = 295
| h1-surface   = Concrete
}}

'''Barentsburg Heliport, Heerodden''' ({{lang-no|Barentsburg helikopterhavn, Heerodden}}; {{airport codes||ENBA|p=n}}) is a private [[heliport]] located at [[Heerodden]] (also known as Kapp Heer), serving the mining town of [[Barentsburg]] in [[Svalbard]], [[Norway]]. The [[airport]] is owned and operated by [[Arktikugol]], which also owns the [[company town]]. The airport features a {{convert|91|by|21|m|sp=us|adj=on}} [[runway]], two hangars and an administration building with a control tower. There are two [[Mil Mi-8]] helicopters based at Heerodden, which are operated by [[Spark+]]. Flights are provided to [[Svalbard Airport, Longyear]] and [[Pyramiden Heliport]].

The heliport was built by Arktikugol in 1961 and the company originally flew two [[Mil Mi-4]] helicopters. The airport received a major upgrade between 1975 and 1978, following the opening of [[Svalbard Airport, Longyear]]. This saw the number of operative aircraft increase to five and the arrival of the Mi-8, operated by [[Aeroflot]]. Operations were cut in the early 1990s, with only two aircraft remaining by 1993. There was a fatal crash at the airport in 2008, killing three of nine passengers.

==History==
The heliport was built by the mining company Arktikugol in 1961. In addition to flights around Barentsburg, it was used to fly to the heliport at [[Pyramiden]]. Arktikugol originally operated [[Mil Mi-4]] helicopters, with place for eleven passengers.<ref>Risanger: 319</ref> The airport was gradually expanded with new infrastructure,<ref name=r305>Risanger: 305</ref> with the first hangar having a capacity of {{convert|700|m2|sp=us}}.<ref name=r310>Risanger: 310</ref> The ''Aviation Act'' applies to Svalbard and from 1961 to 1974 Arktikugol followed this by applying for and receiving a helicopter operating concession from the [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]]. After 1974 the Soviet Union stated that the regulations were in violation of the [[Svalbard Treaty]] allowing free shipping.<ref>Risanger: 317</ref>

[[File:Heerodden IMG 4445 from north.JPG|thumb|left|[[Heerodden]] and the heliport]]
The Soviet Union agreed in 1971 to allow the construction of Svalbard Airport, Longyear. The condition was that the airport be built with capacity to allow [[Aeroflot]] to operate flights to [[Moscow]]. This would again increase the need for the heliport in Barentsburg, as it would be used for fly passengers from Barentsburg to the Longyearbyen.<ref name=r260>Risanger: 260</ref> A major upgrade commenced in 1975, consisting of a new terminal building, hangars and a radar. The upgrades were completed in 1978 and by then there were 20 pilots, 25 mechanics and 20 other employees working and living at Heerodden.<ref name=r310 /> 

[[File:Spark Mil Mi-8MTV-1 at Barentsburg.jpg|thumb|[[Mil Mi-8]] helicopter operated by [[Spark+]] in the hangar at Barentsburg]]
Arktikugol increased its fleet to five new Mil Mi-8 helicopters, all with Aeroflot markings.<ref>Risanger: 316</ref> Each helicopter has a capacity for 28 passengers and a range of {{convert|375|km|sp=us}}.<ref>Risanger: 319</ref> Following a [[Jon Michelet]] article in ''[[Klassekampen]]'' in 1976 there was a major interest by the Norwegian press concerning the heliport.<ref>Arlov: 289</ref> Rumor spread concerning that the airport was illegal and that it was a military installation which in a short time could be transformed into a [[air base]]. The lack of initial Norwegian inspections fueled the speculation. The press which underlined that the heliport had a significant higher capacity than the five helicopters actually in use, and that with the heliport's strategic location it could easy be militarized in a conflict.<ref>Risanger: 313</ref> Contributing to the speculation was that Arktikugol used military, rather than the civilian, version of the Mi-8.<ref>Risanger: 316</ref> However, there was never any evidence to support the claims.<ref>Risanger: 314</ref> The surveillance section of the Governor was however not in doubt that there agents from the [[Main Intelligence Directorate (Russia)|Main Intelligence Directorate]].<ref name=a290>Arlov: 290</ref> There were also speculations that the heliport was planned expanded to a full-length runway which could support fixed-winged aircraft.<ref>Arlov: 291</ref>

The issue became more tense on 31 August when the [[Hopen Accident]], when a [[Soviet Air Forces]] [[Tupolev Tu-16]] crashed at [[Hopen, Svalbard|Hopen]]. The press started asking if Norwegian authorities had control over the Soviet activities on the island. The [[Avinor|Civil Aviation Administration]] carried out inspections at the heliport in 1979.<ref name=a290 /> The [[Avinor|Civil Aviation Administration]] gave an operating permit on 1 August 1980.<ref name=a13>Accident Investigation Board Norway: 13</ref> Following the [[dissolution of the Soviet Union]] in 1991, subsidies and resources allocated to Svalbard and Arktikugol were diminished. By 1993 there were only two remaining helicopters, and all crew and airport employees were relocated to live in Barentsburg.<ref>Holm: 18</ref> Operation were reduced from 1998 when Arktikugol closed Pyramiden.<ref>Holm: 108</ref>

==Facilities==
[[File:Isfjorden IMG 1354 Heerodden.jpg|thumb|The airport seen from [[Grønfjorden]]]]
The heliport is situated at Heerodden, a peninsula {{convert|3.5|km|sp=us}} north of the town of Barentsburg,<ref name=a13 /> at an elevation of {{convert|25|m|sp=us}} [[above mean sea level]]. The facility consists of an administration building, a control tower, two hangars and a radar. The airport has a single concrete runway, which is {{convert|90|by|21|m|sp=us|adj=on}} and aligned east–west. It connects to the main apron via a {{convert|8|m|sp=us|adj=on}} wide concrete [[taxiway]]. The largest apron area measures {{convert|96|by|32|m|sp=us}}.<ref name=a13 />

The administration building has meeting rooms and a canteen in the first floor, teaching and technical rooms in the second, apartments in the third and the [[control tower]] in the fourth. The building has {{convert|800|m2|sp=us}} of residential and office space and {{convert|360|m2|sp=us}} of technical rooms. There are two hangars, the smaller being {{convert|700|m2|sp=us}},<ref name=r310 /> while the larger is {{convert|96|by|32|m|sp=us}} and {{convert|8|m|sp=us}} tall.<ref name=a13 /> This allows for the indoor parking of five Mi-8 helicopters. There is hangar capacity for maintenance of one such helicopter at a time.<ref name=r310 />

[[File:Isfjorden IMG 1839 Heerodden Heliport.jpg|thumb|left|A view of the heliport from the fjord]]
The airport is located in [[uncontrolled airspace]]. The tower is manned on days and times with flights. Due to the language barrier, there is no radio communication between the helicopters and heliport on the one side, and Svalbard Airport, Longyear on the other. Yet both airport and helicopters have radios to communicate at Longyearbyen's frequency. Information concerning flights is relayed by fax from the meteorological station at Barentsburg to Longyearbyen.<ref name=a14>Accident Investigation Board Norway: 14</ref>

==Airlines and destinations==
Arktikugol has two Mil Mi-8 aircraft which are operated by [[Spark+]].<ref name=a14 /> They are based at the heliport and provide transport services for Arktikugol and the [[Consulate-General of Russia in Barentsburg]] for flights to Pyramiden and to Longyearbyen.<ref name=a4>Accident Investigation Board Norway: 4</ref> The airport is also used by the [[Governor of Svalbard]]'s helicopter when it visits Barentsburg.<ref>Accident Investigation Board Norway: 26</ref>

==Accidents and incidents==
On 30 March 2008 a Mi-8 with registration RA-06152 crashed at the airport whilst attempting to land. There had been recent heavy snowfalls at the airport and as the helicopter came into land, its rotor wash disturbed the loose snow, causing the pilots to lose their visual references in what was effectively a highly localised blizzard. The helicopter's course deviated {{convert|100|m|sp=us}} beyond the runway, at an angle of 50 degrees, and crashed into the smaller hangar. Three of the nine passengers were killed in the impact, and three more were seriously injured.<ref>Accident Investigation Board Norway: 3</ref> The investigation concluded that the airport needed to improve is procedures for landing in snow, and that the airline needed to improve its crew management and could take advantage of wearing seat belts.<ref name=a4 />

==References==
{{reflist|30em}}

==Bibliography==
{{commons category|Barentsburg Heliport, Heerodden}}
* {{cite web |author=[[Accident Investigation Board Norway]] |title=Report concerning aviation accident on the Cape Heer Heliport, Svalbard, Norway, 30 March 2008 with Mil Mi-8MT, RA-06152, operated by Spark+ Airline Ltd.|url=http://www.aibn.no/Luftfart/Rapporter/2013-06-eng?pid=SHT-Report-ReportFile&attach=1 |year=2013 |accessdate=7 April 2014}}
* {{cite book |last=Arlov |first=Thor B. |title=Den rette mann |year=2011 |location=Trondheim |publisher=Tapir Akademisk Forlag |language=Norwegian |isbn=978-82-519-2827-4}}
* {{cite book |last=Holm |first=Kari |title=Longyearbyen – Svalbard: historisk veiviser |year=1999 |url=http://urn.nb.no/URN:NBN:no-nb_digibok_2008090100031 |language=Norwegian |isbn=82-992142-4-6}}
* {{cite book |last=Risanger |first=Otto |title=Russerne på Svalbard |url=http://www.nb.no/nbsok/nb/b733fa7e240161a56203b2d75c3246a0 |location=Longyearbyen |publisher=Sampress |year=1978 |isbn=82-90210-03-5 |language=Norwegian}}

{{Airports in Norway}}
{{Svalbard}}
{{Portal bar|Aviation|Arctic|Norway|Russia|Soviet Union}}

[[Category:Airports in Svalbard]]
[[Category:Heliports in Norway]]
[[Category:Barentsburg]]
[[Category:Airports established in 1961]]
[[Category:1961 establishments in Norway]]