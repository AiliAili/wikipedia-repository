{{Infobox comic book title|  <!--Wikipedia:WikiProject Comics-->
| image = All-Negro Comics 1.jpg
| caption = ''All-Negro Comics'' #1 (June 1947). Cover artist unknown. Clockwise from top left: Lion Man, Snake Oil, Sugarfoot, Bubba, Ace Harlem. Center: The Little Dew Dillies
| schedule = 
| format = [[Anthology]]
|ongoing=
|Superhero   = 
| publisher = All-Negro Comics, Inc.
| date = [[1947 in comics|1947]]
| issues = 1
| main_char_team = Ace Harlem<br>Lion Man
| writers = 
| artists = John Terrell<br>George J. Evans Jr.
| editors = 
| pencillers =
| inkers =
| colorists =
| creative_team_month =
| creative_team_year =
| creators = 
|subcat=
|sort=All Negro Comics
}}
'''''All-Negro Comics''''', published in 1947, was a single-issue, small-press [[United States|American]] [[comic book]] that represents the first known comics magazine written and drawn solely by [[African-American]] writers and artists.

==Publication history==
[[African-American]] journalist [[Orrin Cromwell Evans]] was "the first black writer to cover general assignments for a mainstream white newspaper in the United States" when he joined the staff of the ''[[Philadelphia Record]]''.<ref name=tom /> Evans was a member of the [[NAACP]] and a strong proponent of [[racial equality]]. After the ''Record'' closed in 1947, Evans thought he could use the comic-book medium to further highlight "the splendid history of Negro journalism".<ref name=Evans>{{cite web| last=Evans| first=Orrin C.|title= 'All-Negro Comics': Presenting Another FIRST in Negro History| publisher= Foreword to ''All-Negro Comics'' #1|date=June 1947}} Reprinted at {{cite web| title=Orrin C. Evans and the Story of All-Negro Comics | url=
http://www.tomchristopher.com/orrin-c-evans-and-the-story-of-all-negro-comics/ | first=Tom|last=Christopher| publisher=TomChristopher.com|accessdate= March 1, 2016 |archivedate= June 15, 2016| archiveurl= https://web.archive.org/web/20160615235622/http://www.tomchristopher.com/orrin-c-evans-and-the-story-of-all-negro-comics/ | deadurl=no}}</ref><ref name=first>{{cite web| url=http://www.firstcomicsnews.com/?p=98138 |title=Orrin C. Evans: The First Black Comic Book Publisher | publisher=FirstComicsNews.com|date= February 11, 2016|accessdate= June 15, 2016| archivedate= March 6, 2016| archiveurl=https://web.archive.org/web/20160306045620/http://www.firstcomicsnews.com/?p=98138 | deadurl=no}}</ref> Evans partnered with former ''Record'' editor Harry T. Saylor, ''Record'' sports editor Bill Driscoll, and two others<ref name=time>{{cite journal|url=http://www.time.com/time/magazine/article/0,9171,779181,00.html |title=The Press: Ace Harlem to the Rescue| work=[[Time (magazine)|Time]] | date = July 14, 1947 | archiveurl= https://web.archive.org/web/20100424121454/http://www.time.com/time/magazine/article/0,9171,779181,00.html|archivedate=April 24, 2010 | deadurl = no | accessdate=July 1, 2011}}</ref> to found the [[Philadelphia]] publishing company All-Negro Comics, Inc., with himself as president.<ref name=tom>{{cite web|last=Christopher|first=Tom|url=http://www.tomchristopher.com/?op=home/Comic%20History/Orrin%20C.%20Evans%20and%20The%20Story%20of%20All%20Negro%20Comics |title= Orrin C. Evans and the story of All-Negro Comics| publisher= TomChristopher.com|year= 2002|archiveurl=https://web.archive.org/web/20090307212906/http://www.tomchristopher.com/?op=home/Comic+History/Orrin+C.+Evans+and+The+Story+of+All+Negro+Comics |archivedate=March 7, 2009|deadurl=no|accessdate=July 1, 2011}} Reprinted from ''[[Comics Buyer's Guide]]'' February 28, 1997, pp. 32, 34, 37-38. Article includes reprinted editorial page "All-Negro Comics: Presenting Another First in Negro History" from ''All-Negro Comics'' #1</ref> In mid-1947, the company published one issue of ''All-Negro Comics'', a 48-page,<ref name=time /> standard-sized comic book with a typical glossy color cover and newsprint interior.<ref name=gcd /> It was [[copyright]]ed July 15, 1947, with a June 1947 issue date,<ref>[https://books.google.com/books?id=QD4hAQAAIAAJ&pg=RA2-PA10&dq=%22All-Negro+Comics%22&hl=en&ei=MTENTpnKNqrZ0QG8zMy8Dg&sa=X&oi=book_result&ct=result&resnum=5&ved=0CD4Q6AEwBA#v=onepage&q=%22All-Negro%20Comics%22&f=false ''Catalog of Copyright Entries. Third Series: 1947''], [[Library of Congress]], [[United States Copyright Office|Copyright Office]], p. 10</ref> and its press run and distribution are unknown.<ref name=tom /> Unlike other comic books of the time, it sold for 15 cents rather than 10 cents.<ref name=gcd>[http://www.comics.org/issue/241535/ ''All-Negro Comics'' #1] at the [[Grand Comics Database]]</ref>

As writer Tom Christopher described, Evans
{{quote|text=...co-created the features in the comic along with the artists, who included his brother, George J. Evans Jr.; two other Philadelphia cartoonists, one of whom was John Terrell,<ref>Christopher spells the artist's surname "Terrell" throughout, except for one instance in which he spells it "Terrill"</ref> the other named Cooper; and a Baltimore artist who signed his work Cravat. The cartoonists probably wrote their own scripts, and there was further editorial input by Bill Driscoll.<ref name=tom />}}

As one cultural historian notes of the era, "[W]hile there were a few heroic images of blacks created by blacks, such as the ''Jive Gray'' [[comic strip]] and ''All-Negro Comics'', these images did not circulate outside of pre-[[civil rights]] segregated black communities."<ref>Carpenter, Stanford W. "Imagining Just Them, Just Us, or a Just Society: Creating Black Characters for the Justice Society of America Comic Book", Chapter 14 in  Agorsah, E. Kofi, and G. Tucker Childs, ''Africa and the African Diaspora: Cultural Adaptation and Resistance'' (AuthorHouse, 2005), ISBN 978-1-4208-2760-6</ref>

Evans attempted to publish a second issue but was unable to purchase the newsprint required. Many believe he was blocked from doing so by prejudiced distributors, as well as from competing, white-owned publishers (such as [[Parents (magazine)|Parents Magazine Press]] and [[Fawcett Comics]]) which began producing their own black-themed titles.<ref name=first />

''The Official Overstreet Comic Book Price Guide'', a standard reference, considers the single issue "rare" and notes, "Seldom found in fine or mint condition; many copies have brown pages."<ref>{{cite news|title=''All-Negro Comics'' |last=Overstreet|first= Robert M.|authorlink=Bob Overstreet|work= The Official Overstreet Comic Book Price Guide|volume=37 |publisher= Gemstone Publishing / House of Collectibles|year= 2007| page= 411|isbn= 978-0-375-72108-3}}</ref>

==Contents==
''[[Time (magazine)|Time]]'' magazine in 1947 called ''All-Negro Comics'' "the first to be drawn by Negro artists and peopled entirely by Negro characters." In describing lead feature "Ace Harlem", it said, "The villains were a couple of [[Zoot suit|zoot-suited]], jive-talking Negro muggers, whose presence in anyone else's comics might have brought up complaints of racial 'distortion.' Since it was all in the family, Evans thought no Negro readers would mind."<ref name=time /> The protagonist of "Ace Harlem," was an African-American [[police detective]]; the characters in the "Lion Man and Bubba" feature were meant to inspire black people's pride in their African heritage.<ref name=tom />

=== Stories<ref name=tom /><ref name=rare>{{cite web|url=http://www.blackradionetwork.com/rare_1st_africanamerican_published_comic_book__all_negro_1_1947_comes_to_auction |title=1st African-American Published Comic - All Negro #1- (1947) Comes to Auction|publisher= Metropolis Collectibles Inc. / ComicConnect Corp. [[press release]] via BlackRadioNetwork.com| date=February 2009 |archiveurl=https://web.archive.org/web/20110702143035/http://www.blackradionetwork.com/rare_1st_africanamerican_published_comic_book__all_negro_1_1947_comes_to_auction | archivedate=July 2, 2011| deadurl=no|accessdate=July 1, 2011}}</ref> ===
[[File:Lion Man page 300px.jpg|thumb|"Lion Man" page from ''All-Negro Comics'' #1. Art by George J. Evans Jr.|left]]
* One-page introductory editorial, "All-Negro Comics: Presenting Another First in Negro History"
* "Ace Harlem", a [[private detective]] feature drawn by John Terrell
* "The Little Dew Dillies", a children's feature starring cherub-like creatures only babies can see and talk to, drawn by Cooper
* "Ezekiel's Manhunt", a two-page boy's-adventure text story
* "Lion Man and Bubba", starring a college-educated African American sent by the [[United Nations]] on a mission to a [[uranium]] deposit on Africa's Gold Coast, where he adopted the mischievous orphan Bubba. Drawn by George J. Evans, Jr. (no relation to [[White people|Caucasian]] comic-book and comic-strip artist [[George Evans (cartoonist)|George Evans]]). One modern-day writer said Lion Man "wore the obligatory leotard costume of the comic hero",<ref>Cripps, Thomas. ''Making Movies Black: The Hollywood Message Movie from World War II to the Civil Rights Era '', ([[Oxford University Press]], 1993), p. 154. ISBN 978-0-19-507669-1</ref> though the comic's cover and interior pages depict him in loin cloth.
* "Hep Chicks on Parade", spot-illustration gags with highly stylized women wearing exaggerated fashions, signed "Len"
* "Lil' Eggie", by Terrell, about henpecked husband Egbert and his wife
* "Sugarfoot", a humor feature, drawn by Cravat, starring traveling musicians Sugarfoot and Snake Oil, who try to woo a farmer's daughter. Evans' editorial said the feature's creators hoped "to recapture the almost lost humor of the loveable wandering Negro minstrel of the past."
* "Remember — Crime  Doesn't Pay, Kids!", a one-page public service announcement and next-issue promo, with Ace Harlem
{{-}}

==See also==
{{portal|Comics|African American}}
* [[Portrayal of black people in comics]]
* [[African characters in comics]]
* ''[[Martin Luther King and the Montgomery Story]]''
*[[Black Panther (comics)]]
*[[Lobo (Dell Comics)]]
* ''[[Real Deal (comics)|Real Deal]]''
* [[Race film]]
* [[Blaxploitation]]

==References==
{{reflist|30em}}

==External links==
*{{cite web|url=http://today.msnbc.msn.com/id/29613342/ns/today-books/t/first-african-american-comic-book-auction/ |title=First African-American Comic Book on Auction |publisher=[[Associated Press]] via [[MSNBC.com]] |date=March 10, 2009 |archiveurl=http://www.webcitation.org/5zsmpnH2Y?url=http%3A%2F%2Ftoday.msnbc.msn.com%2Fid%2F29613342%2Fns%2Ftoday-books%2Ft%2Ffirst-african-american-comic-book-auction%2F |archivedate=July 2, 2011 |deadurl=no |accessdate=July 1, 2011 |df= }}
*{{cite web|last=Carter|first= Jason R. |url=http://www.blacksuperhero.com/exhibithtml/detail.cfm?id=375 |title=Lion Man|publisher= BlackSuperhero.com| date= n.d.|accessdate=July 1, 2011}}
* {{cite web|authorlink=Scott Shaw|last=Shaw|first= Scott|url=http://www.oddball-comics.com/article.php?story=2007-02-26 |title=''All-Negro Comics'', No. 1|publisher= Oddball Comics (column) #1148|date= February 25, 2007| archiveurl=https://web.archive.org/web/20070520173646/www.oddballcomics.com/article.php?story=2007-02-26 |archivedate=May 20, 2007|deadurl=no|accessdate=July 1, 2011}}
* [https://marswillsendnomore.wordpress.com/2013/01/04/all-negro-comics-1947-first-issue/ ''All-Negro Comics'' scans]

[[Category:American comics magazines]]
[[Category:Magazines established in 1947]]
[[Category:Magazines disestablished in 1947]]
[[Category:Fictional African-American people]]
[[Category:African-American magazines]]
[[Category:1947 comics debuts]]
[[Category:One-shot comic titles]]
[[Category:Defunct American comics]]
[[Category:Black people in comics]]