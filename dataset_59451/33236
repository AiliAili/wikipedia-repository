{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:SMS Rheinland NH 46835.jpg|300px]]
| Ship caption = ''Rheinland'' shortly after entering service in 1910
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship name = 
| Ship namesake = [[Rhineland]]
| Ship owner = 
| Ship operator = 
| Ship registry = 
| Ship route = 
| Ship ordered = 
| Ship awarded = 
| Ship builder = [[Stettiner Vulcan AG|Vulcan AG]], [[Stettin]]
| Ship original cost = 
| Ship yard number = 
| Ship way number = 
| Ship laid down = 1 June 1907
| Ship launched = 26 September 1908
| Ship sponsor = 
| Ship christened = 
| Ship completed = 
| Ship acquired = 
| Ship commissioned = 30 April 1910
| Ship recommissioned = 
| Ship decommissioned = 
| Ship maiden voyage = 
| Ship in service = 
| Ship out of service = 
| Ship renamed = 
| Ship reclassified = 
| Ship refit = 
| Ship struck = 
| Ship fate = Scrapped 1921
| Ship status = 
| Ship notes = 
| Ship badge = 
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Nassau|battleship}}
| Ship tonnage = 
| Ship displacement =* Designed: {{convert|18,873|t|LT|abbr=on}}
* Full load: {{convert|20,535|t|LT|abbr=on}}
  
| Ship tons burthen = 
| Ship length = {{convert|146.1|m|ftin|abbr=on}}
| Ship beam = {{convert|26.9|m|ftin|abbr=on}}
| Ship height = 
| Ship draft = {{convert|8.9|m|ftin|abbr=on}}
| Ship propulsion =* 3-shaft vertical triple expansion
* {{convert|16,181|kW|ihp|lk=in|abbr=on|order=flip}}
  
| Ship speed =* Designed: {{convert|19|kn|lk=in}}
* Maximum: {{convert|20|kn}}
  
| Ship range = At {{convert|12|kn}}: {{convert|8,300|nmi|lk=in|abbr=on}}
| Ship boats = 10
| Ship complement =* Standard: 40&nbsp;officers, 968&nbsp;men
* Squadron flagship: 53&nbsp;officers, 1,034&nbsp;men
* 2nd command flagship: 42&nbsp;officers, 991&nbsp;men
  
| Ship crew = 
| Ship armament =* 12 × {{convert|28|cm|in|abbr=on}} SK L/45 guns
* 12 × {{convert|15|cm|in|abbr=on}} SK L/45 guns
* 16 × {{convert|8.8|cm|in|abbr=on}} SK L/45 guns
* 5 × {{convert|45|cm|in|abbr=on}} torpedo tubes
  
| Ship armor =* Belt: {{convert|300|mm|in|abbr=on}}
* Turrets: {{convert|280|mm|in|abbr=on}}
* Battery: {{convert|160|mm|in|abbr=on}}
* Conning Tower: {{convert|300|mm|in|abbr=on}}
* Torpedo bulkhead: {{convert|30|mm|in|abbr=on}}
  
| Ship notes =* Double bottom: 88%
* Watertight compartments: 16
  
}}
|}

'''SMS ''Rheinland'''''{{efn|name=SMS}} was one of four {{sclass-|Nassau|battleship}}s, the first [[dreadnought]]s built for the [[German Imperial Navy]] (''Kaiserliche Marine''). ''Rheinland'' mounted twelve {{convert|28|cm|in|abbr=on}} main guns in six twin turrets in an unusual hexagonal arrangement. The navy built ''Rheinland'' and her [[sister ship]]s in response to the revolutionary British {{HMS|Dreadnought|1906|6}}, which had been launched in 1906. ''Rheinland'' was laid down in June 1907, launched the following year in October, and commissioned in April 1910.

''Rheinland''{{'}}s extensive service with the [[High Seas Fleet]] during [[World War I]] included several fleet advances into the [[North Sea]], some in support of raids against the English coast conducted by the German battlecruisers of the [[I Scouting Group]]. These sorties culminated in the [[Battle of Jutland]] on 31 May&nbsp;– 1 June 1916, in which ''Rheinland'' was heavily engaged by British [[destroyer]]s in close-range night fighting.

The ship also saw duty in the [[Baltic Sea]], as part of the support force for the [[Battle of the Gulf of Riga]] in 1915. She returned to the Baltic as the core of an expeditionary force to aid the [[White Guard (Finland)|White Finns]] in the [[Finnish Civil War]] in 1918, but ran aground shortly after arriving in the area. Significant portions of her armor and all her main guns had to be removed before she could be refloated. The damage done by the grounding was deemed too severe to justify repairs and ''Rheinland'' was decommissioned to be used as a barracks ship for the remainder of the war. In 1919, following the [[scuttling of the German fleet in Scapa Flow]], ''Rheinland'' was ceded to the Allies who, in turn, sold the vessel to [[Ship breaking|ship-breakers]] in the Netherlands. The ship was eventually broken up for scrap metal starting in 1920. Her bell is on display at the [[Military History Museum of the Bundeswehr]] in [[Dresden]].

== Description ==
{{main|Nassau-class battleship}}

[[File:Nassau class main weapon.svg|thumb|left|Line drawing of a ''Nassau''-class battleship showing the disposition of the main battery]]

The ship was {{convert|146.1|m|ftin|abbr=on}} long, {{convert|26.9|m|ftin|abbr=on}} wide, and had a draft of {{convert|8.9|m|ftin|abbr=on}}. She displaced {{convert|18,873|t|LT|abbr=on}} with a normal load, and {{convert|20,535|t|LT|abbr=on}} fully laden. She retained coal-fired 3-shaft [[Steam engine#Multiple expansion engines|triple expansion engines]] instead of more advanced [[turbine]] engines.{{sfn|Gröner|p=23}} This type of machinery was chosen at the request of both Admiral [[Alfred von Tirpitz]] and the Navy's construction department; the latter stated in 1905 that the "use of turbines in heavy warships does not recommend itself."{{sfn|Herwig|pp=59–60}}

''Rheinland'' carried twelve [[28 cm SK L/45 gun|{{convert|28|cm|in|abbr=on}} SK L/45]]{{efn|name=gun nomenclature}} guns in an unusual hexagonal configuration. Her secondary armament consisted of twelve [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} SK L/45]] guns and sixteen [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} SK L/45]] guns, all of which were mounted in [[casemate]]s.{{sfn|Gröner|p=23}} The ship was also armed with six {{convert|45|cm|in|abbr=on}} submerged [[torpedo tube]]s. One tube was mounted in the bow, another in the stern, and two on each broadside, on both ends of the [[torpedo bulkhead]]s.{{sfn|Gardiner & Gray|p=140}}

== Commanding officers ==
''Rheinland'' was initially commanded by ''[[Kapitän zur See]]'' (''KzS'') [[Albert Hopman]], from her commissioning until August 1910. He was temporarily replaced by ''[[Korvettenkapitän]]'' [[Wilhelm Bunnemann]] when the ship's crew was reduced to commission the battlecruiser {{SMS|Von der Tann||2}} in September 1910. Hopman returned to the ship later that month, and held command through September 1911. ''KzS'' [[Richard Engel]] replaced Hopman in 1911 and commanded the ship until August 1915. That month, he left the ship and ''KzS'' [[Heinrich Rohardt]] was given command of ''Rheinland''. He served for over a year, until December 1916, when he was replaced by ''Korvettenkapitän'' [[Theodor von Gorrissen]]. Gorrissen's command lasted until September 1918; he was replaced by ''KzS'' [[Ernst Toussaint]], who held command of the ship for less than a month. ''[[Fregattenkapitän]]'' [[Friedrich Berger]] was the ship's last commander, serving from September 1918 until the ship's decommissioning on 4 October.{{sfn|Hildebrand, Röhr & Steinmetz|p=71}}

== Service history ==
[[File:Bundesarchiv DVM 10 Bild-23-61-23, Linienschiff "SMS Rheinland".jpg|thumb|alt=A large battleship lined with guns and equipped with two tall masts sits in harbor.|SMS ''Rheinland'', c. 1910]]

''Rheinland'' was ordered under the provisional name ''Ersatz Württemberg'', as a replacement for the old {{sclass-|Sachsen|ironclad|2}} {{SMS|Württemberg|1878|2}}.{{sfn|Gröner|p=23}} She was laid down on 1 June 1907 at the [[AG Vulcan]] shipyard in [[Stettin]].{{sfn|Staff|p=27}} Like her sister {{SMS|Nassau||2}}, construction proceeded under absolute secrecy; detachments of soldiers guarded the shipyard itself, as well as contractors such as [[Krupp]] that supplied building materials.{{sfn|Hough|p=26}} The ship was launched on 26 September 1908;{{sfn|Staff|p=27}} at the launching ceremony the ship was christened by [[Elisabeth of Wied]] and [[Clemens Freiherr von Schorlemer-Lieser]] gave a speech.{{sfn|Hildebrand, Röhr & Steinmetz|p=72}} [[Fitting-out]] work was completed by the end of February 1910. A dockyard crew was used for limited [[sea trial]]s, which lasted from 23 February to 4 March 1910 off [[Swinemünde]]. She was then taken to [[Kiel]], where she was commissioned into the [[High Seas Fleet]] on 30 April 1910. More sea trials followed in the Baltic Sea.{{sfn|Staff|p=30}}

At the conclusion of [[sea trial|trials]] on 30 August 1910, ''Rheinland'' was taken to [[Wilhelmshaven]], where a significant portion of the crew was transferred to the new [[battlecruiser]] {{SMS|Von der Tann||2}}. Following the autumn fleet maneuvers in September, the crew was replenished with crewmembers from the old [[pre-dreadnought]] {{SMS|Zähringen||2}}, which was decommissioned at the same time. ''Rheinland'' was then assigned to the I Battle Squadron of the High Seas Fleet. In October, the fleet went on the annual winter cruise, followed by fleet exercises in November. The ship took part in the summer cruises to Norway each August in 1911, 1913, and 1914.{{sfn|Staff|p=30}}

=== World War I ===
''Rheinland'' participated in nearly all of the fleet advances throughout the war.{{sfn|Staff|p=30}} The first such operation was conducted primarily by the battlecruisers; the ships [[Raid on Scarborough, Hartlepool and Whitby|bombarded Scarborough, Hartlepool, and Whitby]] on 15–16 December 1914.{{sfn|Tarrant|p=31}} During the operation, the German battle fleet of some 12 dreadnoughts and 8 pre-dreadnoughts, which was serving as distant support for the battlecruisers, came to within {{convert|10|nmi|abbr=on}} of an isolated squadron of six British battleships. However, skirmishes between the rival [[destroyer]] screens convinced the German commander, Admiral [[Friedrich von Ingenohl]], that he was confronted with the entire [[Grand Fleet]]. He broke off the engagement and turned for home.{{sfn|Tarrant|pp=31–33}} A fleet sortie to the [[Dogger Bank]] took place on 24 April 1915. During the operation, the high-pressure cylinder of ''Rheinland''{{'}}s starboard engine failed. Repair work lasted until 23 May.{{sfn|Staff|pp=30–31}}

==== Battle of the Gulf of Riga ====
{{main|Battle of the Gulf of Riga}}
In August 1915, the German fleet attempted to clear the Russian-held [[Gulf of Riga]] in order to facilitate the capture of [[Riga]] by the German army. To do so, the German planners intended to drive off or destroy the Russian naval forces in the Gulf, which included the [[pre-dreadnought]] battleship {{ship|Russian battleship|Slava||2}} and a number of gunboats and destroyers. The German naval force would also lay a series of minefields in the northern entrance to the Gulf to prevent Russian naval reinforcements from reentering the area. The assembled German fleet included ''Rheinland'' and her three sister ships, the four {{sclass-|Helgoland|battleship|2}}s, and the battlecruisers {{SMS|Von der Tann||2}}, {{SMS|Moltke||2}}, and {{SMS|Seydlitz||2}}. The force operated under the command of Vice Admiral [[Franz von Hipper]]. The eight battleships were to provide cover for the forces engaging the Russian flotilla. The first attempt on 8 August was unsuccessful, as it had taken too long to clear the Russian minefields to allow the minelayer {{SMS|Deutschland|1914|2}} to lay a minefield of her own.{{sfn|Halpern|pp=196–197}}

On 16 August 1915, a second attempt was made to enter the Gulf: {{SMS|Nassau||2}} and {{SMS|Posen||2}}, four light cruisers, and 31&nbsp;torpedo boats managed to breach the Russian defenses.{{sfn|Halpern|p=197}} On the first day of the assault, the German minesweeper {{SMS|T46||2}} was sunk, as was the destroyer {{SMS|V99||2}}. The following day, ''Nassau'' and ''Posen'' engaged in an artillery duel with ''Slava'', resulting in three hits on the Russian ship that forced her to retreat. By 19 August, the Russian minefields had been cleared and the flotilla entered the Gulf. However, reports of Allied submarines in the area prompted the Germans to call off the operation the following day.{{sfn|Halpern|pp=197–198}} Admiral Hipper later remarked that "To keep valuable ships for a considerable time in a limited area in which enemy submarines were increasingly active, with the corresponding risk of damage and loss, was to indulge in a gamble out of all proportion to the advantage to be derived from the occupation of the Gulf ''before'' the capture of Riga from the land side."{{sfn|Halpern|p=198}}

==== Return to the North Sea ====
By the end of August, ''Rheinland'' and the rest of the High Seas Fleet units were back in their bases on the North Sea. The next operation conducted was a sweep into the North Sea on 11–12 September, though it ended without any action. Another sortie followed on 23–24 October during which the German fleet did not encounter any British forces. On 12 February 1916, ''Rheinland'' was sent to the dockyard for an extensive overhaul, which lasted until 19 April. ''Rheinland'' was back with the fleet in time to participate in another advance into the North Sea on 21–22 April. Another bombardment mission followed two days later; ''Rheinland'' was part of the battleship support for the I Scouting Group battlecruisers that [[Bombardment of Yarmouth and Lowestoft|attacked Yarmouth and Lowestoft]] on 24–25 April.{{sfn|Staff|p=31}} During this operation, the battlecruiser {{SMS|Seydlitz||2}} was damaged by a British mine and had to return to port prematurely. Visibility was poor, so the operation was quickly called off before the British fleet could intervene.{{sfn|Tarrant|pp=52–54}}

==== Battle of Jutland ====

{{main|Battle of Jutland}}
Admiral [[Reinhard Scheer]] immediately planned another attack on the British coast, but the damage to ''Seydlitz'' and condenser trouble on several of the III&nbsp;Battle Squadron dreadnoughts delayed the plan until the end of May.{{sfn|Tarrant|pp=56–58}} The German battlefleet departed the [[Jadebusen|Jade]] at 03:30 on 31 May.{{sfn|Tarrant|p=62}}{{efn|name=CET times}} ''Rheinland'' was assigned to the II&nbsp;Division of the I Battle Squadron, under the command of Rear Admiral W. Engelhardt. ''Rheinland'' was the second ship in the division, astern of ''Posen'' and ahead of ''Nassau'' and {{SMS|Westfalen||2}}. The II&nbsp;Division was the last unit of dreadnoughts in the fleet; they were followed by the elderly pre-dreadnoughts of the II&nbsp;Battle Squadron.{{sfn|Tarrant|p=286}}

Between 17:48 and 17:52, 11 German dreadnoughts, including ''Rheinland'', engaged and opened fire on the British 2nd Light Cruiser Squadron, though the range and poor visibility prevented effective fire, which was soon checked.{{sfn|Campbell|p=54}} Some ten minutes later ''Rheinland'' again opened fire on the British cruisers, targeting what was most likely {{HMS|Southampton|1912|6}}, though without success.{{sfn|Campbell|p=99}} By 20:15, the German fleet had faced the deployed [[Grand Fleet]] for a second time and was forced to turn away; in doing so, the order of the German line was reversed, with ''Rheinland'' third from the front, behind ''Westfalen'' and ''Nassau''.{{sfn|Tarrant|p=172}} At 21:22, crewmen aboard ''Rheinland'' and ''Westfalen'', the two leading ships in the German line, spotted two torpedo tracks that turned out to be imaginary. The ships were then forced to slow down in order to allow the battlecruisers of the I Scouting Group to pass ahead.{{sfn|Campbell|p=254}} Around 22:00, ''Rheinland'' and ''Westfalen'' observed unidentified light forces in the gathering darkness. After flashing a challenge via searchlight that was ignored, the two ships turned away to starboard in order to evade any torpedoes that might have been fired. The rest of I Battle Squadron followed them.{{sfn|Campbell|p=257}}

[[File:HMS Black Prince.jpg|thumb|left|HMS ''Black Prince''|alt=A large dark gray warship sits motionless in the water; it has four tall, thin smoke stacks closely arranged in the middle of the ship with two tall masts on either end.]]
At about 00:30, the leading units of the German line encountered British destroyers and cruisers. A violent firefight at close range ensued; ''Rheinland'' pounded the [[armored cruiser]] {{HMS|Black Prince|1904|6}} with her secondary guns at a range of {{convert|2200|to|2600|m|yd|abbr=on}}. After a few minutes, ''Rheinland'' and the rest of the German battleships turned away to avoid torpedoes. At 00:36, ''Rheinland'' was hit by a pair of {{convert|6|in|cm|adj=on}} shells from ''Black Prince''.{{sfn|Campbell|pp=286–287}} One of the shells cut the cables to the four forward searchlights and damaged the forward funnel. The second struck the side of the ship and exploded on the forward armored transverse bulkhead. Although the bulkhead was bent inward from the explosion, it was not penetrated.{{sfn|Campbell|p=303}} About 45 minutes later, ''Rheinland'' opened fire on another destroyer, possibly {{HMS|Ardent|1913|2}}, but she had to cease when a German cruiser came too close to the line of fire.{{sfn|Campbell|p=289}} At the same time, ''Black Prince'' was obliterated by accurate fire from the battleship {{SMS|Ostfriesland||2}}.{{sfn|Campbell|p=290}}

Despite the ferocity of the night fighting, the High Seas Fleet punched through the British destroyer forces and reached [[Horns Reef]] by 04:00 on 1&nbsp;June.{{sfn|Tarrant|pp=246–247}} The German fleet reached Wilhelmshaven a few hours later, where ''Rheinland'' refueled and re-armed. Meanwhile, her three sisters stood out in the [[roadstead]] in defensive positions.{{sfn|Tarrant|p=263}} Over the course of the battle, the ship had fired thirty-five 28&nbsp;cm (11&nbsp;in) shells and twenty-six 15&nbsp;cm (5.9&nbsp;in) rounds.{{sfn|Tarrant|p=292}} The two hits from ''Black Prince'' had killed 10 men and wounded 20.{{sfn|Tarrant|p=298}} Repair work followed immediately in Wilhelmshaven and was completed by 10 June.{{sfn|Campbell|p=336}}

==== Later actions ====
Another fleet advance followed on 18–22&nbsp;August; the I&nbsp;Scouting Group battlecruisers were to bombard the coastal town of [[Sunderland, Tyne and Wear|Sunderland]] in an attempt to draw out and destroy Beatty's battlecruisers. As only two of the four German battlecruisers were still in fighting condition, three dreadnoughts were assigned to the Scouting Group for the operation: {{SMS|Markgraf||2}}, {{SMS|Grosser Kurfürst|1913|2}} (or ''Großer''{{efn|name=sharp S}} ''Kurfürst''), and the newly commissioned {{SMS|Bayern||2}}. ''Rheinland'' and the rest of the High Seas Fleet were to trail behind and provide cover.{{sfn|Massie|p=682}} The British were aware of the German plans and sortied the Grand Fleet to meet them. By 14:35, Admiral Scheer had been warned of the Grand Fleet's approach and, unwilling to engage the whole of the Grand Fleet just 11&nbsp;weeks after the decidedly close call at Jutland, turned his forces around and retreated to German ports.{{sfn|Massie|p=683}}

''Rheinland'' covered a sweep by torpedo boats into the North Sea on 25–26 September. She then participated in a fleet advance on 18–20 October. In early 1917, the ship was stationed on sentry duty in the [[German Bight]]. The crew became unruly due to poor quality food in July and August of that year. The ship did not take part directly in [[Operation Albion]] against the Russians, but remained in the western Baltic to prevent a possible incursion by the British to support their Russian ally.{{sfn|Staff|p=31}}

==== Expedition to Finland ====
[[File:SMS Westfalen LOC 25466u.jpg|thumb|SMS ''Westfalen''|alt=A large warship steams at low speed; gray smoke drifts from the two smoke stacks]]

On 22 February 1918, ''Rheinland'' and her sister {{SMS|Westfalen||2}} were tasked with a mission to Finland to support German army units to be deployed there. The ship arrived in the [[Åland Islands]] on 6 March, where her commander became the Senior Naval Commander, a position he held until 10 April. On 11 April, the ship departed the Ålands for [[Helsinki]], with the intention of proceeding to [[Danzig]] to refuel. However, she encountered heavy fog while ''en route'' and ran aground on [[Lagskär Island]] at 07:30. Two men were killed in the incident and the ship was badly damaged. Three boiler rooms were flooded and the inner hull was pierced. Refloating efforts on 18–20 April proved unsuccessful. The crew was removed temporarily, to bring the pre-dreadnought {{SMS|Schlesien||2}} back into service. On 8 May, a floating crane was brought in from Danzig; the main guns, some of the turret armor, and the bow and citadel armor were all removed. The ship was lightened by {{convert|6400|MT|sp=us}}—more than a third of her normal displacement—and with the aid of pontoons, eventually refloated by 9 July.{{sfn|Staff|p=31}} The ship was towed to [[Mariehamn]] where some limited repairs were effected.{{sfn|Staff|pp=31–32}} On 24 July the ship departed for Kiel with the assistance of two [[tug boat]]s; she arrived there three days later. It was determined that repair work was impractical and instead the ship was decommissioned on 4 October and placed into service as a barracks ship in Kiel.{{sfn|Staff|p=32}}

=== Fate ===
Following the German collapse in November 1918, a significant portion of the High Seas Fleet was interned in [[Scapa Flow]] according to the terms of the [[Armistice with Germany|Armistice]]. ''Rheinland'' and her three sisters were not among the ships listed for internment, so they remained in German ports.{{sfn|Hore|p=67}} However, a copy of ''[[The Times]]'' informed von Reuter that the Armistice was to expire at noon on 21 June 1919, the deadline by which Germany was to have signed the peace treaty. Rear Admiral von Reuter came to the conclusion that the British intended to seize the German ships after the Armistice expired.{{efn|name=armistice expiry}} To prevent this, he decided to [[scuttling|scuttle]] his ships at the first opportunity. On the morning of 21 June, the British fleet left Scapa Flow to conduct training maneuvers; at 11:20 Reuter transmitted the order to his ships.{{sfn|Herwig|p=256}}

As a result of the scuttling at Scapa Flow, the Allies demanded replacements for the ships that had been sunk. This included ''Rheinland'', which was struck from the German naval list on 5 November 1919 and subsequently handed over to the Allies.{{sfn|Gröner|p=24}} The ship was sold on 28 June 1920 to ship-breakers in [[Dordrecht]] in the Netherlands, under the contract name "F".{{sfn|Staff|p=32}}{{sfn|Gröner|p=24}} She was towed there a month later on 29 July and broken up by the end of the following year. ''Rheinland''{{'}}s bell is preserved at the [[Military History Museum of the Bundeswehr]] in [[Dresden]].{{sfn|Gröner|p=24}}

== Notes ==

{{portal|Battleships}}

'''Footnotes'''

{{notes
| colwith = 40em
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''", or "His Majesty's Ship".
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick firing, while "L/45" provides the length of the gun in terms of the diameter of the barrel. In this case, the L/45 gun is 45 [[Caliber (artillery)|caliber]], which means that the gun is 45&nbsp;times as long as its diameter. See {{harvnb|Grießmer|p=177}}.
}}

{{efn
| name = CET times
| The times mentioned in this section are in [[Central European Time|CET]], which is congruent with the German perspective. This is one hour ahead of [[GMT]], the time zone commonly used in British works.
}}

{{efn
| name = sharp S
| This is the German "sharp S"; see [[ß]].
}}

{{efn
| name = armistice expiry
| By this time, the Armistice had been extended to 23 June, though there is some contention as to whether von Reuter was aware of this. Admiral [[Sydney Fremantle]] stated that he informed von Reuter on the evening of the 20th, though von Reuter claims he was unaware of the development. For Fremantle's claim, see {{harvnb|Bennett|p=307}}. For von Reuter's statement, see {{harvnb|Herwig|p=256}}.
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==

* {{cite book
  | last = Bennett
  | first = Geoffrey
  | authorlink = Geoffrey Bennett
  | year = 2005
  | title = Naval Battles of the First World War
  | publisher = Pen & Sword Military Classics
  | location = London
  | isbn = 978-1-84415-300-8
  | oclc = 57750267
  | ref = {{sfnRef|Bennett}}
  }}
* {{cite book
  | last = Campbell
  | first = John
  | year = 1998
  | title = Jutland: An Analysis of the Fighting
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-1-55821-759-1
  | ref = {{sfnRef|Campbell}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last = Grießmer
  | first = Axel
  | year = 1999
  | language = German
  | title = Die Linienschiffe der Kaiserlichen Marine
  | publisher = Bernard & Graefe Verlag
  | location = Bonn
  | isbn = 978-3-7637-5985-9
  | oclc = 
  | ref = {{sfnRef|Grießmer}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last1 = Hildebrand
  | first1 = Hans H.
  | last2 = Röhr
  | first2 = Albert
  | last3 = Steinmetz
  | first3 = Hans-Otto
  | year = 1993
  | title = Die Deutschen Kriegsschiffe
  | volume = 7
  | publisher = Mundus Verlag
  | location = [[Ratingen]]
  | isbn = 978-3-8364-9743-5
  | oclc = 
  | ref = {{sfnRef|Hildebrand, Röhr & Steinmetz}}
  }}
* {{cite book
  | last = Hore
  | first = Peter
  | year = 2006
  | title = Battleships of World War I
  | publisher = Southwater Books
  | location = London
  | isbn = 978-1-84476-377-1
  | oclc = 77797289
  | ref = {{sfnRef|Hore}}
  }}
* {{cite book
  | last = Hough
  | first = Richard
  | authorlink = Richard Hough
  | year = 2003
  | title = Dreadnought: A History of the Modern Battleship
  | publisher = Periscope Publishing
  | location = Penzance, Cornwall, UK
  | isbn = 978-1-904381-11-2
  | ref = {{sfnRef|Hough}}
  }}
* {{cite book
  | last = Massie
  | first = Robert K.
  | authorlink = Robert K. Massie
  | year = 2003
  | title = [[Castles of Steel]]
  | publisher = Ballantine Books
  | location = New York City
  | isbn = 978-0-345-40878-5
  | oclc = 57134223
  | ref = {{sfnRef|Massie}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918
  | volume = 1
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-467-1
  | oclc = 705750106
  | ref = {{sfnRef|Staff}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}

{{Nassau class battleship}}
{{April 1918 shipwrecks}}
{{coord|59|51|N|19|55|E|type:landmark_source:kolossus-dewiki|display=title}}
{{Featured article}}

{{DEFAULTSORT:Rheinland}}
[[Category:Nassau-class battleships]]
[[Category:Ships built in Stettin]]
[[Category:World War I battleships of Germany]]
[[Category:1908 ships]]
[[Category:Maritime incidents in 1918]]