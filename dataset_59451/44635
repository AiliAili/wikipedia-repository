The '''Embassy of Canada to the United Arab Emirates''' (French: Ambassade du Canada Aux Emirats arabes unis) is [[Canada]]'s diplomatic mission to the [[United Arab Emirates]]. The Embassy is located on the 9th and 10th floors of the West Tower of the Abu Dhabi Trade Center, next to [[Abu Dhabi Mall]].

==History==
Diplomatic relations between Canada and the United Arab Emirates were established in 1974. As Canadian presence in the United Arab Emirates increased, a Trade Office was opened in [[Dubai]] in 1991 in cooperation with the private-sector Canada Arab Business Council. Two years later, in 1993, the Trade Office was upgraded to Consulate, with Robert Farrell becoming the first Consul-General and Senior Trade Commissioner.  The Embassy was officially opened in 1996 with Stuart McDowall becoming the first resident ambassador. In 1999, the United Arab Emirates opened its Embassy in [[Ottawa]].

The Embassy was originally located on the Corniche by the Hilton hotel and then in 1998 moved to a Villa in Al Manasir area. In 2006 having outgrown the villa, the Embassy moved to its present location in Abu Dhabi Mall, West Tower 9th & 10th floors.

There are 40,000 Canadian citizens currently living in the United Arab Emirates, with a large number of them involved in the health and education sectors. 15,000 reside in the emirate of Abu Dhabi and the remaining 25,000 in Dubai and the northern emirates.<ref>Information provided by the Embassy of Canada to the United Arab Emirates</ref> The United Arab Emirates is the only [[Arab States of the Persian Gulf|Persian Gulf country]] to host two Canadian diplomatic missions (one in Abu Dhabi and one in Dubai). 

The United Arab Emirates is also Canada's largest export market in the [[MENA|Middle East and North Africa region]]. A bilateral agreement to further enhance economic cooperation, trade and investment was signed in 2009 by Canada's Minister of International Trade, [[Stockwell Day]], and the United Arab Emirates' Minister of Economy, [[Sultan Bin Saeed Al Mansoori|Sultan bin Saeed Al Mansoori]].<ref>{{cite web|url=http://www.uae-embassy.ae/Embassies/ca/Content/1141 |title=UAE Embassy in Ottawa » UAE–Canada Relations-Bilateral relationship |publisher=Uae-embassy.ae |date=2012-09-18 |accessdate=2013-08-18}}</ref>

==Embassy sections==
The Visa Section processes applications for temporary resident and permanent resident visas from citizens and residents of the [[United Arab Emirates]], [[Bahrain]], [[Kuwait]], [[Oman]], [[Qatar]], [[Saudi Arabia]] and [[Yemen]].<ref>{{cite web|url=http://www.canadainternational.gc.ca/uae-eau/visas/index.aspx?lang=eng&menu_id=4 |title=Visas and Immigration |publisher=Canadainternational.gc.ca |date= |accessdate=2013-08-18}}</ref>

The Trade Section promotes Canada's trade and economic interests in the emirate of Abu Dhabi, and supports Canadian companies and their products, services or technologies in the Emirati and regional market. This section also offers UAE clients assistance regarding investment and trade opportunities in Canada.<ref>{{cite web|url=http://www.tradecommissioner.gc.ca/eng/offices-united-arab-emirates.jsp |title=Export, Innovate and Invest in United Arab Emirates |publisher=Tradecommissioner.gc.ca |date=2007-02-20 |accessdate=2013-08-18}}</ref>

The Consular Section provides assistance to Canadians travelling, studying or residing in the emirate of Abu Dhabi. 

The Political, Economic and Public Affairs Section has a broad scope of responsibilities including promoting bilateral political relations and managing public affairs. 

The [[Department of National Defence (Canada)|Department of National Defence]] has also a Canadian Defence Attaché Office.

==List of ambassadors==
* 1996-1999: Stuart McDowall
* 1999-2002: Christopher Thomson
* 2002-2006: David Hutton
* 2006-2009: Sara S. Hradecky
* 2009-2012: Ken W. Lewis
* 2012-2016: Arif Lalani<ref>{{cite web|url=http://www.international.gc.ca/media/aff/news-communiques/2012/08/28a.aspx?lang=eng |title=Diplomatic Appointment |publisher=International.gc.ca |date=2012-08-28 |accessdate=2013-08-18}}</ref>
* Since 2016: Masud Husain<ref>{{cite web|url=http://news.gc.ca/web/article-en.do?nid=1100789 |title=Diplomatic Appointment |publisher=International.gc.ca |date=2016-07-19 |accessdate=2017-01-13}}</ref>

==References==
{{Reflist}}

{{Diplomatic missions of Canada}}
{{Diplomatic missions in the United Arab Emirates}}

{{coord|24.4964|N|54.3828|E|source:wikidata|display=title}}

[[Category:Canada–United Arab Emirates relations]]
[[Category:Diplomatic missions in Abu Dhabi|Canada]]
[[Category:Diplomatic missions of Canada|United Arab Emirates]]