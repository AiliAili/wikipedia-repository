{{Infobox dot-com company
| name     = Bark & Co. (BarkBox)
| logo     = [[File:BarkBox Logo.jpg|250px]]
| company_type     = [[E-commerce]] [[Subscription business model|subscription service]]
| traded_as        = 
| foundation       = 
| dissolved        =
| location         = [[New York City]], [[New York (state)|New York]], [[United States|U.S.]]
| locations        = 
| incorporated     =
| area_served      = USA and Canada
| founder          = Matt Meeker, Henrik Werdelin, and Carly Strife 
| chairman         = 
| chairperson      = 
| president        = 
| CEO              = Matt Meeker
| MD               = 
| GM               = 
| key_people       = Cathie Black
| industry         = 
| products         = 
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 54
| parent           = 
| divisions        = 
| subsid           = 
| company_slogan   = 
| url              = {{URL|www.barkbox.com}}
| website_type     =
| advertising      = 
| registration     = 
| num_users        =
| language         =
| launch_date      = {{Start date and age|2011|12}}
| current_status   = 
| screenshot       = 
| caption          = 
| footnotes        = 
| intl             =
}}

'''Bark & Co. (BarkBox)''' is a New York-based provider of pet-themed products and technology known for its monthly subscription service BarkBox, which has approximately 200,000 monthly subscribers.<ref name =Needleman2013>{{cite web | url = http://blogs.wsj.com/accelerators/2013/03/22/are-you-ready-to-go-from-founder-to-top-dog/ | title = Are You Ready to Go From Founder to Top Dog | publisher = [[Wall Street Journal]]| author = Sarah E. Needleman | date = 22 March 2013| accessdate = 28 July 2014}}</ref><ref name =Whitelocks2013>{{cite web | url = http://www.dailymail.co.uk/femail/article-2425412/BarkBox-subscription-service-dogs-delivers-bones-squeaky-toys.html | title = Mail dogs CAN get their teet into!| publisher = Daily Mail UK | author = Sadie Whitelocks | date = 19 September 2013| accessdate = 28 July 2014}}</ref><ref name = Perez2014>{{cite web | url = http://techcrunch.com/2014/07/11/barkbox-series-b/| title = Doggie-Focused Bark & Co (BarkBox) Raises $15 Million Series B | publisher = [[TechCrunch]] | author = Sarah Perez | date =11 July 2014| accessdate = 28 July 2014}}</ref><ref name =Primack2014>{{cite web | url = http://fortune.com/2014/07/14/deals-of-the-day-mylan-finds-its-tax-inversion-partner/ | title = Deals of the day: Mylan finds its tax inversion partner | publisher = [[Fortune (magazine)|Fortune]] | author = Dan Primack | date = 14 July 2014| accessdate = 28 July 2014}}</ref><ref name =Marikar2014>{{cite web | url = https://www.nytimes.com/2014/07/13/fashion/social-media-stars-use-instagram-twitter-and-tumblr-to-build-their-career.html?_r=0| title = Turning 'Likes' Into a Career| publisher = [[The New York Times]]| author = Sheila Marikar | date = 11 July 2014| accessdate = 28 July 2014}}</ref><ref name =McCarthy2013>{{cite web | url = http://www.huffingtonpost.com/fueled/barkbox-founder-matt-meek_b_4223439.html | title = BarkBox Founder Matt Meeker is Building A Community for Man’s Best Friend| publisher = The Huffington Post| date =28 July 2014| accessdate = 28 July 2014}}</ref><ref name = Shontell2013>{{cite web | url = http://www.businessinsider.com/barkbox-revenue-and-barkcare-2013-6 | title = BarkBox Generates $1 Million Per Month, Wants To Become A $5 Billion Company in 5 Years| publisher =[[Business Insider]]| author = Alyson Shontell | date = 10 July 2013| accessdate = 28 July 2014}}</ref> The company also operates the dog-themed content site BarkPost, the photo-sharing app BarkCam, and BarkBuddy, a "Tinder for Dogs."<ref name =Stampler2014>{{cite web | url = http://time.com/132095/tinder-for-dogs/ | title = Sick of the People on Dating Apps? Now There's a Tinder for Dogs | publisher = Time | author = Laura Stampler | date = 20 May 2014| accessdate = 29 July 2014}}</ref><ref name =Drell2013>{{cite web | url = http://mashable.com/2013/08/20/scaling-smarter-barkbox/ | title = Top Dog: BarkBox's Mission to Reinvent the Pet Industry | publisher = [[Mashable]]| author = Lauren Drell| date = 20 August 2013| accessdate = 28 July 2014}}</ref><ref name =BloombergTV2/><ref name =Bell2014>{{cite web | url = http://mashable.com/2014/07/16/barkcam-app/ | title = BarkCam Helps the Pet-Obsessed Take Better Dog Portraits | publisher = [[Mashable]] | author = Karissa Bell | date = Jul 16, 2014| accessdate = 28 July 2014}}</ref> Matt Meeker, Henrik Werdelin, and Carly Strife are the founders.<ref name = Needleman2013/><ref name =McCarthy2013/>

==History==

===BarkBox===

CEO Matt Meeker, Henrik Werdelin, and Carly Strife founded BarkBox in December 2011.<ref name = Needleman2013/><ref name =Whitelocks2013/><ref name = Perez2014/><ref name =McCarthy2013/><ref name =Drell2013/><ref name =BloombergTV2>{{cite web | url = https://www.bloomberg.com/video/making-millions-off-of-man-s-best-friend-ly8wJKbdS9qKRFrcT~xDGA.html| title = Making Millions of Man’s Best Friend| publisher = [[Bloomberg Television]] | date =| accessdate = Jul 28, 2014}}</ref><ref name =BloombergTV1>{{cite web | url = https://www.bloomberg.com/video/like-to-spoil-your-dog-barkbox-may-be-for-you-AyS_LhZFQ7~iIL_e~cTi7w.html | title = Like to Spoil Your Dog? BarkBox May Be for You | publisher = [[Bloomberg Television]]| date = 2013| accessdate = Jul 28, 2014}}</ref> The inspiration for BarkBox arose when co-founder Matt Meeker failed to find retailers in New York City who sold products tailored to dogs of all sizes, specifically his 150 lb. [[Great Dane]] Hugo.<ref name =Whitelocks2013/><ref name =BloombergTV1/><ref name =Martin2013>{{cite web | url = http://www.bbc.com/news/business-24109140| title = Pet projects: How US retailers are going to the dogs | publisher = [[BBC]] | author = Jill Martin| date =28 September 2013| accessdate = 28 July 2014}}</ref><ref name = Swartz2013/><ref name = Grant2013>{{cite web | url = http://venturebeat.com/2013/11/18/obsessed-with-your-dog-barkbox-releases-photo-sharing-forum-for-pet-owners-exclusive/ | title = Obsessed with your dog? BarkBoxreleases photo-sharing forum for pet owners (exclusive)| publisher = | date =| accessdate = Jul 28, 2014}}</ref> Prior to co-founding BarkBox, Matt Meeker co-founded the social networking site [[Meetup (website)|Meetup.com]].<ref name =Needleman2013/><ref name = Shontell2013/><ref name = Drell2013/><ref name = Grant2013/>

===Bark & Co===

The company rebranded to Bark & Co. in April 2013 after raising $6.7M in venture funding.<ref name =McCarthy2013/><ref name = Drell2013/><ref name =Griffith2013>{{cite web | url = http://pando.com/2013/10/25/mock-all-you-want-barkbox-is-laughing-all-the-way-to-the-bank/ | title = Mock all you want: Barbox is laughing all the way to the bank| publisher = [[PandoDaily]]| author = Erin Griffith | date = Oct 25, 2013| accessdate = Jul 28, 2014}}</ref> The company used the funds to diversify its products, adding the dog-themed content sites BarkPost and PuppyFeed to their repertoire.<ref name = Drell2013/><ref name =BloombergTV2/> The company surpassed the $25 million revenue run rate the same year.<ref name = Griffith2013/> Bark & Co. received an additional $15 Million in [[Venture round|Series B]] funding led by Resolute.vc, along with RRE, BoxGroup, Lerer Ventures, Bertelsmann Digital Media Investments, Slow Ventures, Daher Capital, CAA, Vast Ventures, and City National Bank in 2014.<ref name = Perez2014/><ref name =Primack2014/>

''Forbes'' Magazine named BarkBox as the one of the top 20 companies to follow on social media.<ref>{{cite web | url = http://www.forbes.com/sites/ilyapozin/2014/03/06/20-companies-you-should-be-following-on-social-media/ | title = 20 Companies You Should Be Following on Social Media | author = Ilya Pozin| publisher = Forbes | date = 6 March 2014| accessdate = 6 August 2014 }}</ref>

Bark & Co. launched BarkCam in early 2014, a photo sharing app which operates as an "[[Instagram]] for dogs" and their owners.<ref name =Bell2014/>

==Products and services==

===BarkBox===

Bark & Co.’s flagship product, BarkBox, is a monthly subscription service which caters to dogs and “dog parents.”<ref name = Drell2013/><ref name =BloombergTV2/><ref name =FoxNews2013/> Each BarkBox typically contains 4-6 items including bones, toys, treats, accessories, and designed-for-pet dental and health products which have been tested by the company’s on-staff vet.<ref name =BloombergTV1/> Subscribers can sign up for 1, 3, 6, or 12 month subscriptions.<ref name =Whitelocks2013/><ref name = Perez2014/><ref name =BloombergTV1/><ref name =Martin2013/><ref name = Grant2013>{{cite web | url = http://venturebeat.com/2013/11/18/obsessed-with-your-dog-barkbox-releases-photo-sharing-forum-for-pet-owners-exclusive/ | title = Obsessed with your dog? BarkBoxreleases photo-sharing forum for pet owners (exclusive)| publisher = | date =| accessdate = Jul 28, 2014}}</ref><ref name =FoxNews2013>{{cite web | url = https://archive.org/details/FOXNEWSW_20130323_100000_FOX_and_Friends_Saturday#start/12840/end/12900| title = Fox and Friends Saturday | publisher = [[Fox News Channel|Fox News]] | date = March 23, 2013| accessdate = Jul 28, 2014}}</ref><ref name = Waas2013>{{cite web | url = http://www.parade.com/5061/amandawaas/are-you-obsessed-with-your-dog/ | title = Are You Obsessed With Your Dog| publisher = [[Parade (magazine)|Parade]]| author = Amanda Waas | date = Apr 15, 2013| accessdate = Jul 28, 2014}}</ref><ref name = Waas2013/><ref name = Harper2013>{{cite web | url = http://techland.time.com/2013/12/06/10-great-gift-box-subscriptions/| title = 10 Great Gift Box Subscriptions | publisher = [[Time (magazine)|Time]]| author = Elizabeth Harper| date = Dec 6, 2013| accessdate = Jul 28, 2014}}</ref><ref name =Jones2013>{{cite web | url = http://www.fastcompany.com/3021392/whos-next/are-you-a-dog-owner-or-a-dog-parent-this-ceo-wants-to-know | title = Are You A Dog Owner Or A Dog Parent? This CEO Wants to Know| publisher = [[Fast Company (magazine)|Fast Company]] | author = Stacy Jones| date = 11 November 2013| accessdate = 28 July 2014}}</ref><ref name =McClear2013>{{cite web | url = http://www.nydailynews.com/life-style/find-boxing-match-subscription-goods-fit-tastes-article-1.1416422?print | title = Subscription boxes bring the whole package: Regular service, no shopping and goods of all kinds| publisher = New York Daily News | author = Sheila McClear | date = Aug 5, 2013| accessdate = Jul 28, 2014}}</ref><ref name =Bufalini2013>{{cite web | url = http://www.dailycandy.com/everywhere/flipbook/146564/10/Pet-Essentials/BarkBox-Dog-Goodies-Subscription| title = ShopTalk 23 Pet Products to Purr About| publisher = Daily Candy|author1=Danielle Bufalini |author2=Allison Hatfield |author3=DeAnna Janes | date =Apr 16, 2013| accessdate = Jul 28, 2014}}</ref><ref name = Rains2013/><ref name =Mulpeter2014>{{cite web | url = http://dailysavings.allyou.com/2014/02/14/subscription-boxes/| title = 14 Fun Subscription Boxes to Match Your Personality | publisher = All You| author = Kathleen Mulpeter | date =Feb 14, 2014| accessdate = Jul 28, 2014}}</ref>

10% of BarkBox proceeds go to local shelters, rescues, and animal welfare organizations.<ref name =Whitelocks2013/><ref name = Swartz2013>{{cite web | url = http://allthingsd.com/20130703/barkboxs-matt-meeker-has-big-plans-for-pet-projects-qa/ | title = BarkBox’s Matt Meeker Has Big Plans for Pet Projects | publisher = All Things Digital| author = Angela Swartz| date = Jul 3, 2013| accessdate = Jul 28, 2014}}</ref><ref name =FoxNews2013/><ref name = Harper2013/><ref name =Bufalini2013>{{cite web | url = http://www.dailycandy.com/everywhere/flipbook/146564/10/Pet-Essentials/BarkBox-Dog-Goodies-Subscription| title = ShopTalk 23 Pet Products to Purr About| publisher = Daily Candy|author1=Danielle Bufalini |author2=Allison Hatfield |author3=DeAnna Janes | date =Apr 16, 2013| accessdate = Jul 28, 2014}}</ref><ref name =Rains2013>{{cite web | url = http://www.realsimple.com/holidays-entertaining/gifts/gift-of-the-month-00100000081736/page9.html | title =8 Surprising Subscription Box Ideas | publisher = Real Simple| author = Valerie Rains| date = 2013| accessdate = Jul 28, 2014}}</ref><ref name =Mulpeter2014/>

===BarkPost===

BarkPost is a dog-themed content site similar to [[BuzzFeed]]'s “Cute” section.<ref name = Shontell2013/><ref name =BloombergTV2/><ref name =BloombergTV1/><ref name =BloombergTV1/><ref name = Grant2013/><ref name = Donfro2013>{{cite web | url = http://www.businessinsider.com/barkbox-adds-media-maven-and-dog-parent-cathie-black-to-its-board-2013-10 | title = BarkBox Adds Media-Maven and ‘Dog Parent’ Cathie Black TO Its Board| publisher = [[Business Insider]] | author = Jillian D’Onfro | date = Oct 2013| accessdate = Jul 28, 2014}}</ref><ref name = Donfro2013/> The site has over 3M unique visitors each month.<ref name = Griffith2013/> The site works in tandem with PuppyFeed, a dedicated webspace where dog owners can share dog photos and videos.<ref name = Grant2013/>

===BarkCam===

BarkCam is a photo-sharing app, similar to a dog version of [[Instagram]], available for [[iOS]].<ref name =Bell2014/> Users can choose from a variety of built-in sounds, designed to grab the pets' attention, which users activate by tapping the camera shutter.<ref name = Perez2014/> Users can then add text, filters, stickers and chat bubbles to the photo to share on social media platforms.<ref name = Bell2014/>

===BarkBuddy===

Bark & Co. has also launched BarkBuddy, which is similar to a "[[Tinder]] for dogs," as users can swipe left or right depending on their level of interest in the dog.<ref name = Perez2014/><ref name =Stampler2014/><ref name = SPerez2014>{{cite web | url = http://techcrunch.com/2014/05/27/barkbuddy-is-a-tinder-for-dogs/ | title = BarkBuddy is A Tinder for Dogs | publisher = [[TechCrunch]] | author = Sarah Perez | date = May 27, 2014 | accessdate = Aug 6, 2014}}</ref><ref name = Faircloth2014>{{cite web | url = http://jezebel.com/theres-finally-a-tinder-for-adoptable-dogs-1582201953 | title = There’s Finally a Tinder for Adoptable Dogs| publisher = [[Jezebel (website)|Jezebel]]| author = Kelly Faircloth| date = May 27, 2014 | accessdate = Aug 6, 2014}}</ref> The free app, available for [[iPhone]] and [[Android (operating system)|Android]],<ref name = Eley2014/> is designed to match humans with dogs up for adoption at nearby shelters, and users can filter choices by gender, location, activity level, age, and size.<ref name = Eley2014>{{cite web | url = http://www.today.com/pets/tinder-dogs-barkbuddy-app-matches-pet-lovers-local-rescues-2D79730864 | title = Tinder for dogs? BarkBuddy app matches pet lovers with local rescues | publisher = Today | author = Amy Eley | date = May 29, 2014 | accessdate = Aug 6, 2014}}</ref> The app is connected to rescue centers and shelters across the United States and Canada, and there are approximately 300,000 dogs in the database.<ref name = Perez2014/><ref name =Stampler2014/><ref name = Faircloth2014/><ref name = Eley2014/> The app sources many of its adoptable dogs from the pet adoption website [[Petfinder]] and from Bark & Co’s personal network of shelters and rescue organizations.<ref name = SPerez2014/><ref name = Faircloth2014/>

==Co-founders==

Matt Meeker is the co-founder of [[Meetup (website)|Meetup]] and former runner of Dogpatch Labs NY.<ref name = Shontell2013/> He has founded four startups in total, but BarkBox is his first position as CEO.<ref name =Needleman2013/><ref name = Drell2013/><ref name = Grant2013/>

Henrik Werdelin founded [[Prehype]], a New York City-based [[Product innovation|venture development firm]] which worked closely with Matt Meeker and Carly Strife to develop BarkBox.<ref name = Weissman2012>{{cite web | url = http://digiday.com/brands/building-the-brand-barkbox/ | title = Building the Brand: BarkBox | publisher = Digiday | author = Saya Weissman | date = Aug 13, 2012 | accessdate = Jul 28, 2014}}</ref>

==References==
{{reflist|2}}

[[Category:2011 establishments in New York]]
[[Category:E-commerce]]
[[Category:Subscription businesses]]