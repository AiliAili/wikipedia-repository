{{Infobox aviator
| name             = Gottlob Espenlaub
| image            = Bundesarchiv Bild 102-09705, Segelflieger Gottlob Espenlaub.jpg
| image_size       = 
| alt              = 
| caption          = Gottlob Espenlaub preparing to fly an early hang glider design
| full_name        = 
| native_name      = 
| native_name_lang = 
| birth_name       =
| birth_date       = 1900<!-- {{Birth date|YYYY|MM|DD}} -->
| birth_place      = 
| death_date       = 1972, age 72<!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place      = 
| death_cause      = 
| resting_place    = 
| resting_place_coordinates = <!-- {{Coord|LATITUDE|LONGITUDE|type:landmark}} -->
| monuments        = 
| nationality      = [[Germany|German]]
| education        = 
| spouse           = 
| relatives        = 
| known_for        = early glider design and construction in [[Germany]]
| first_flight_date     = 
| first_flight_aircraft = 
| famous_flights   = 
| license_date     = 
| license_place    = 
| air_force        = 
| battles          = 
| rank             = 
| awards           = 
| memorials        = 
| first_race       = <!-- For racing aviators -->
| racing_best      = <!-- For racing aviators -->
| racing_aircraft  = <!-- For racing aviators -->
| website          = <!-- {{URL|example.com}} -->
| child            = 
| module           = 
}}
'''Gottlob Espenlaub''' (25 October 1900 &ndash; 9 January 1972<ref>{{cite book |title=Neue deutsche Biographie: Bd. Laverrenz-Locher-Freuler |last=Bayerische Akademie der Wissenschaften |first= |authorlink= |author2=Historische Kommission |year=1985 |publisher=Duncker & Humblot |location= |isbn= |page=662 |pages= |url=https://books.google.com/books?id=RbjpAAAAMAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=qrYaTe_wHInQsAP0x9jFAg&sa=X&oi=book_result&ct=result&resnum=8&ved=0CD0Q6AEwBzge |accessdate=December 28, 2010}}</ref>), nicknamed '''Espe''',<ref>{{cite book |title=Kronfeld on gliding and soaring: the story of motorless human flight |last=Kronfeld |first=Robert |authorlink= |author2=Manchot, J. |year=1932 |publisher=J. Hamilton, ltd |location= |isbn= |page=69 |pages= |url=https://books.google.com/books?id=AytFAAAAMAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=_KsaTdS0NoHQsAPoisHLAg&sa=X&oi=book_result&ct=result&resnum=9&ved=0CEIQ6AEwCA |accessdate=December 28, 2010}}</ref> was an [[inventor]] who specialized in early types of [[aircraft]], specifically [[Glider aircraft|gliders]] and [[Rocket propellant|rocket propulsion systems]] designed for them. He invented a number of different aircraft, focusing  on tailless designs. Espenlaub co-founded the practice of [[aerotowing]].

==Aeronautic career==
[[File:Espenlaub 1921 hangglider.jpg|thumb|Gottlob Espenlaub]]
Espenlaub was born in Balzholz,{{CN|date=June 2016}} since 1938 part of [[Beuren, Esslingen|Beuren]]. As a young man Espenlaub served as a [[joiner]] to aircraft builders. He helped [[Alexander Lippisch]] to build a style of glider in 1921 according to Lippisch's designs. The glider was subsequently dubbed the [[Lippisch-Espenlaub E-2 glider]], due to Espenlaub's participation in its creation.<ref>{{cite book |title=The Delta wing: history and development |last=Lippisch |first=Alexander |authorlink= |year=1981 |publisher=Iowa State University Press |location= |isbn= |page=2 |pages= |url=https://books.google.com/books?id=P6lTAAAAMAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=_KsaTdS0NoHQsAPoisHLAg&sa=X&oi=book_result&ct=result&resnum=8&ved=0CD4Q6AEwBw |accessdate=December 28, 2010}}</ref> Espenlaub began building his own [[Rocket glider|rocket propelled gliders]] in 1928, conducting his first rocket test on October 22, 1929.<ref>{{cite book |title=The dream machines: an illustrated history of the spaceship in art, science, and literature |last=Miller |first=Ron |authorlink= |year=1993 |publisher=Krieger Pub. Co |location= |isbn= |page= |pages=185–194 |url=https://books.google.com/books?id=aj_L950Fcn0C&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=_KsaTdS0NoHQsAPoisHLAg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCMQ6AEwAA |accessdate=December 28, 2010}}</ref> His rocket glider for this test was dubbed the RAK-3 and it featured missiles attached to the wings. During the flight, these missiles caught the tail on fire, forcing an early landing. Due to this, many of Espenlaub's later designs would feature tailless gliders.<ref>{{cite book |title=Flugmotoren und Strahltriebwerke: Entwicklungsgeschichte der deutschen Luftfahrtantriebe von den Anfängen bis zu den internationalen Gemeinschaftsentwicklungen |last=von Gersdorff |first=Kyrill |authorlink= |author2=Schubert, Helmut |author3=Grasmann, Kurt |author4=Ebert, Stefan |author5= Faltermair, Richard  |year=2007 |publisher=Bernard & Graefe |location= |isbn= |page=307 |pages= |url=https://books.google.com/books?id=GBw-AQAAIAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=kLcaTbuuJIHGsAPfwuWvCg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCIQ6AEwADgo |accessdate=December 28, 2010}}</ref><ref>{{cite news |title=Noticias Mundiales De Ultima Hora |author= |url=http://pqasb.pqarchiver.com/latimes/access/378543921.html?dids=378543921:378543921&FMT=ABS&FMTS=ABS:AI&type=historic&date=May+02,+1930&author=&pub=Los+Angeles+Times&desc=NOTICIAS+MUNDIALES+DE+ULTIMA+HORA&pqatl=google |newspaper=[[Los Angeles Times]] |date=May 2, 1930 |accessdate=December 28, 2010}} ([https://translate.google.com/translate?hl=en&sl=es&tl=en&u=http://pqasb.pqarchiver.com/latimes/access/378543921.html%3Fdids%3D378543921:378543921%26FMT%3DABS%26FMTS%3DABS:AI%26type%3Dhistoric%26date%3DMay%2B02,%2B1930%26author%3D%26pub%3DLos%2BAngeles%2BTimes%26desc%3DNOTICIAS%2BMUNDIALES%2BDE%2BULTIMA%2BHORA%26pqatl%3Dgoogle English])</ref>

After conducting further tests, he improved his design and then did another test with his glider in [[Düsseldorf]], [[Germany]] in 1930, achieving a speed of 90 kilometers per hour.<ref>{{cite book |title=Spaceplanes: From Airport to Spaceport |last=A. Bentley |first=Matthew |authorlink= |year=2008 |publisher=シュプリンガー・ジャパン株式会社 |location= |isbn= |page=221 |pages= |url=https://books.google.com/books?id=rk1Sivcwyc4C&pg=PA221&dq=%22Gottlob+Espenlaub%22&hl=en&ei=_KsaTdS0NoHQsAPoisHLAg&sa=X&oi=book_result&ct=result&resnum=5&ved=0CDIQ6AEwBA#v=onepage&q=%22Gottlob%20Espenlaub%22&f=false |accessdate=December 28, 2010}}</ref> He had already been known as a "renowned German sailplane pilot" before, because of his various designs and his piloting abilities. Therefore, he was asked to test fly numerous new kinds of aircraft, such as the A2 airplane in 1929 built by Alexander Soldenhoff.<ref>{{cite book |title=Winged wonders: the story of the flying wings |last=Wooldridge |first=E. T. |author2=National Air and Space Museum |year=1983 |publisher=Smithsonian Institution Press |location= |isbn= |page=28 |pages= |url=https://books.google.com/books?id=mopTAAAAMAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=j7QaTfykNZSusAOfn4XGCg&sa=X&oi=book_result&ct=result&resnum=6&ved=0CDcQ6AEwBTgU |accessdate=December 28, 2010}}</ref>

Espenlaub also served as a theorist and implementor in terms of aircraft design and the future abilities of aircraft. He and [[Gerhard Fieseler]] conducted a number of demonstrations in 1927 at [[Kassel]] on the idea of [[aerotowing]] and its feasibility in the air.<ref>{{cite journal |author= |year=1981 |title=Gottlob Espenlaub |journal=[[Soaring (magazine)|Soaring]] |volume=45 |issue= |pages=33 |publisher=[[Soaring Society of America]] |doi= |pmid= |pmc= |url=https://books.google.com/books?id=FzZWAAAAMAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=_KsaTdS0NoHQsAPoisHLAg&sa=X&oi=book_result&ct=result&resnum=10&ved=0CEUQ6AEwCQ |accessdate=December 28, 2010}}</ref> It was the first time this idea was shown, and it became widely used after Espenlaub's and Fieseler's demonstrations.<ref>{{cite book |title=Gliding and advanced soaring |last=Douglas |first=Ann C. |first2=Ann Courtenay Edmonds |last2=Welch |first3=Alan E. |last3=Slater |year=1947 |publisher=J. Murray |location= |isbn= |page=28 |pages= |url=https://books.google.com/books?id=_Jc7AAAAMAAJ&q=%22Gottlob+Espenlaub%22&dq=%22Gottlob+Espenlaub%22&hl=en&ei=FrMaTdOQCYv6sAPLjuSvAg&sa=X&oi=book_result&ct=result&resnum=5&ved=0CDQQ6AEwBDgK |accessdate=December 28, 2010}}</ref>

==Automotive designs==
{{Main|Espenlaub (automobile)}}
Both before and after the war, Espenlaub produced innovative [[streamliner]] automotive designs, but none were commercially successful.<ref>[http://strangevehicles.greyfalcon.us/Espenlaub%20Experimental.htm "ESPENLAUB EXPERIMENTAL AERIAL and MOTOR CRAFT"], Rob Arndt</ref>

==References==
{{reflist}}

{{Authority control}}
{{DEFAULTSORT:Espenlaub, Gottlob}}
[[Category:1900 births]]
[[Category:1972 deaths]]
[[Category:German people of World War II]]
[[Category:German aerospace engineers]]