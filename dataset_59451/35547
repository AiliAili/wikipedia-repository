{{good article}}
{{Use dmy dates|date=April 2014}}
{{Infobox Grand Prix race report
| Type = F1
| Country = Spain
| Grand Prix = Spanish
| Details ref = <ref name="formal">{{cite web|url=http://www.formula1.com/content/fom-website/en/championship/races/2015/Spain.html |title=2015 Formula 1 Gran Premio de España|work=Formula1.com |publisher=[[Formula One Group|Formula One Administration]] |accessdate=22 April 2015 |archiveurl=https://web.archive.org/web/20150531183319/http://www.formula1.com/content/fom-website/en/championship/races/2015/Spain.html |archivedate= 31 May 2015 |deadurl=no}}</ref><ref name="fiapreview">{{cite web|title= Race Preview: 2015 Spanish Grand Prix |url=http://www.fia.com/file/27441/download?token=Cn6xCf8B |accessdate= 31 May 2015 |work=FIA.com |publisher=Fédération Internationale de l'Automobile |archiveurl=https://web.archive.org/web/20150531183556/http://www.fia.com/file/27441/download?token=Cn6xCf8B |archivedate=31 May 2015 |deadurl=no}}</ref>
| Image = Catalunya.svg
| image-size = 250px
| image-alt = A track map of the Circuit de Barcelona-Catalunya. The track has 16 corners, which range in sharpness from hairpins to gentle, sweeping turns. There are two long straights that link the corners together. The pit lane splits off from the track on the inside of Turn 16, and rejoins the track after the start-finish straight.
| Caption = Circuit de Barcelona-Catalunya
| date = 10 May
| year = 2015
| Official name = Formula 1 [[Spanish Grand Prix|Gran Premio de España]] Pirelli 2015<ref name="formal" />
| Race_No = 5
| Season_No = 19
| location = [[Circuit de Barcelona-Catalunya]]<br />[[Montmeló]], Spain
| Course = Permanent racing facility
| Course_length = {{convert|4.655|km|mi|abbr=on}}
| Distance_laps = 66
| Distance_length = {{convert|307.104|km|mi|abbr=on}}
| Weather = Sunny<br/>{{convert|25|C|F}} air temperature<br/>{{convert|44|C|F}} track temperature<br/>{{convert|1.8|m/s|ft/s|abbr=on}} wind from the northeast<ref>{{cite web|title=GP Spanien in Barcelona / Rennen|url=http://www.motorsport-total.com/f1/ergeb/2015/05/71.shtml|website=motorsport-total.com|accessdate=20 March 2016|language=German|trans_title=GP Spain in Barcelona / Race}}</ref>
| Attendance = 86,700
| Pole_Driver = [[Nico Rosberg]]
| Pole_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| Pole_Time = 1:24.681
| Pole_Country = Germany
| Fast_Driver = [[Lewis Hamilton]]
| Fast_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| Fast_Time = 1:28.270
| Fast_Lap = 54
| Fast_Country = United Kingdom
| First_Driver = [[Nico Rosberg]]
| First_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| First_Country = Germany
| Second_Driver = [[Lewis Hamilton]]
| Second_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| Second_Country = United Kingdom
| Third_Driver = [[Sebastian Vettel]]
| Third_Team = [[Scuderia Ferrari|Ferrari]]
| Third_Country = Germany
| Lapchart = {{F1Laps2015|ESP}}
}}

The '''2015 Spanish Grand Prix''', formally titled the '''Formula 1 Gran Premio de España Pirelli 2015''', was a [[Formula One]] [[auto racing|motor race]] held on 10 May 2015 at the [[Circuit de Barcelona-Catalunya]] in [[Montmeló]], Spain. The race was the fifth round of the {{F1|2015}} season and marked the forty-fifth running of the [[Spanish Grand Prix]] as a round of the Formula One World Championship and the twenty-fifth running at Catalunya. [[Mercedes-Benz in Formula One|Mercedes]] driver [[Nico Rosberg]] took his first win of the season, his first in Spain and the ninth of his career. His team-mate [[Lewis Hamilton]] finished second after a bad start, followed by [[Sebastian Vettel]] in third.

==Report==

===Background===
[[Mercedes-Benz in Formula One|Mercedes]] were expected to be dominant in Barcelona, since it was at the circuit that their advantage per lap had been biggest in [[2014 Spanish Grand Prix|2014]]<ref>{{cite web|last1=Collantine|first1=Keith|title=Mercedes' dominance outstripped Red Bull’s of 2010–3|url=http://www.f1fanatic.co.uk/2014/12/12/mercedes-2014-dominance-outstripped-red-bulls-2010-3/|publisher=F1Fanatic|accessdate=8 May 2015|date=12 December 2014}}</ref> and they had shown strong form at the two pre-season tests at the circuit.<ref>{{cite web|last1=Johnson|first1=Daniel|title=Formula One 2015 pre-season review: Mercedes could be untouchable this year|url=http://www.telegraph.co.uk/sport/motorsport/formulaone/11444185/Formula-One-2015-pre-season-review-Mercedes-could-be-untouchable-this-year.html|newspaper=The Daily Telegraph|accessdate=8 May 2015|date=2 March 2015}}</ref>

After problems with their engine in the first races, which saw [[Daniel Ricciardo]] down to his last power unit after just four outings, [[Renault in Formula One|Renault]] introduced a new, modified engine for the Spanish Grand Prix, aiming to improve both reliability and drivability.<ref>{{cite web|title=Renault introduce modified engine|url=http://www.planetf1.com/driver/55667788/47796/Renault-introduce-modified-engine|website=planetf1.com|date=7 May 2015|deadurl=yes|archiveurl=https://web.archive.org/web/20150708145549/http://www.planetf1.com/driver/55667788/47796/Renault-introduce-modified-engine|archivedate=8 July 2015|accessdate=21 January 2016}}</ref>

[[Scuderia Ferrari|Ferrari]] brought new parts to Barcelona, but [[Kimi Räikkönen]] was unsatisfied with the handling of his car, switching back to the earlier setup for the third practice session.<ref>{{cite web|title=Spanish Grand Prix – Comparing old and new|url=http://formula1.ferrari.com/en/spanish-grand-prix-comparing-old-and-new/|website=formula1.ferrari.com|publisher=Scuderia Ferrari|accessdate=11 May 2015|date=8 May 2015}}</ref> He later conceded that "maybe it would have been better with the new bits."<ref>{{cite web|title=Kimi was 'prepared' to take the hit|url=http://planetf1.com/driver/55667788/48548/Kimi-was-prepared-to-take-the-hit|website=planetf1.com|date=11 May 2015|deadurl=yes|archiveurl=https://web.archive.org/web/20150706182117/http://www.planetf1.com/driver/55667788/48548/Kimi-was-prepared-to-take-the-hit|archivedate=6 July 2015|accessdate=21 January 2016}}</ref> [[McLaren]] arrived in Barcelona with a new dark grey livery in order to "improv[e] its visual impact [...] for the floodlights increasingly used in twilight and night races," a team representative said.<ref>{{cite web|title=New-look for McLaren in Spain|url=http://www.planetf1.com/driver/3373/47526/New-look-for-McLaren-in-Spain|website=planetf1.com|date=6 May 2015|deadurl=yes|archiveurl=https://web.archive.org/web/20150709090743/http://www.planetf1.com/driver/3373/47526/New-look-for-McLaren-in-Spain|archivedate=9 July 2015|accessdate=21 January 2016}}</ref>

====Tyres====
On 22 April, [[Pirelli]] announced that the teams would be using the hard and medium tyres for this race, the same choice as the year before.<ref>{{Cite web|url=http://www.formula1.com/content/fom-website/en/latest/headlines/2015/4/pirelli-reveal-tyre-choices-for-spain--monaco--canada-and-austri.html|title=Pirelli reveal tyre choices for Spain, Monaco, Canada and Austria|publisher=Formula One World Championship Limited|date=22 April 2015|website=Formula1.com|accessdate=24 April 2015}}</ref>

===Free practice===
[[File:Alonso Spain 2015.jpg|thumb|Local favourite [[Fernando Alonso]]'s [[McLaren]] sporting the team's new, dark grey livery]]
Per the regulations for the 2015 season, three practice sessions were held, two 1.5-hour sessions on Friday and another one-hour session before qualifying on Saturday.<ref name=pracreg>{{cite web|title=Practice and qualifying|url=http://www.formula1.com/content/fom-website/en/championship/inside-f1/rules-regs/Practice_qualifying_and_race_start_procedure.html|website=formula1.com|publisher=FOM|accessdate=25 October 2015|archiveurl=https://web.archive.org/web/20150905082403/http://www.formula1.com/content/fom-website/en/championship/inside-f1/rules-regs/Practice_qualifying_and_race_start_procedure.html|archivedate=5 September 2015}}</ref> [[Nico Rosberg]] was fastest on Friday morning, outpacing his team mate [[Lewis Hamilton]] by 0.07 seconds. Rosberg raised the interest of the race stewards when he failed to stay to the left of the bollard at the pit lane entrance returning to pit lane after one of his runs. Mercedes lived up to the expectations by lapping almost a second quicker than the next fastest cars, the two Ferrari drivers. Three test drivers took part in the session: [[Jolyon Palmer]] took the place of [[Romain Grosjean]] at the wheel of one of the [[Lotus F1|Lotus]] cars as he had done in the two prior races; [[Raffaele Marciello]] returned for [[Sauber]] after he had already filled the role at the {{F1 GP|2015|Malaysian}}; and making her debut for 2015 was [[Susie Wolff]] for [[Williams Grand Prix Engineering|Williams]], finishing eight-tenths of a second behind regular driver [[Felipe Massa]].<ref>{{cite web|last1=Collantine|first1=Keith|title=Rosberg quickest despite pit entry slip-up|url=http://www.f1fanatic.co.uk/2015/05/08/rosberg-quickest-despite-pit-entry-slip-up/|publisher=F1Fanatic|accessdate=8 May 2015|date=8 May 2015}}</ref>

Lewis Hamilton was quickest in the second session on Friday afternoon. Track temperatures rose by more than {{convert|20|C-change}} to almost {{convert|50|C}} at the end of the session. The extreme heat was especially hard on the tyres, which led to many drivers complaining about a lack of grip on track. [[Sebastian Vettel]] split the two Mercedes cars and finished second fastest, four-tenths of a second slower than Hamilton, with Rosberg a further three tenths down. Daniel Ricciardo struggled with his engine yet again, which needed to be changed mid-session, leaving him with only ten minutes of running. [[McLaren]] were yet to score in 2015, but with [[Jenson Button]] finishing seventh and [[Fernando Alonso]] in eleventh place, they proved to be closer to the point ranks than in previous races. [[Romain Grosjean]] suffered from technical problems on his Lotus, which ended in the engine cover of his car shattering on the start-finish straight, bringing out a red flag.<ref>{{cite web|last1=Collantine|first1=Keith|title=Hamilton ahead as heat rises in Spain|url=http://www.f1fanatic.co.uk/2015/05/08/hamilton-ahead-as-heat-rises-in-spain/|publisher=F1Fanatic|accessdate=8 May 2015|date=8 May 2015}}</ref>

The third practice session, on Saturday morning, showed that Mercedes' advantage on the softer, medium tyres was less significant than on the hard compound, meaning Vettel was able to lap within two-tenths of a second of Rosberg, who was fastest. Hamilton spun in turn three, compromising his session. He finished third fastest, two-tenths down on his team mate. Kimi Räikkönen, who was fifth fastest, was once more unsatisfied with the setup of his car, as he could be heard on the radio telling his team "Excellent. How can we be in wrong settings?"<ref>{{cite web|last1=Collantine|first1=Keith|title=Rosberg leads Vettel as Hamilton spins|url=http://www.f1fanatic.co.uk/2015/05/09/rosberg-leads-vettel-as-hamilton-spins/|publisher=F1Fanatic|accessdate=9 May 2015|date=9 May 2015}}</ref>

===Qualifying===
[[File:Hamilton Spain 2015.jpg|thumb|For the first time in 2015, [[Lewis Hamilton]] did not take pole position.]]
Qualifying consisted of three parts, 18, 15 and 12 minutes in length respectively, with five drivers eliminated from competing after each of the first two sessions.<ref name=pracreg /> During the first session (Q1), [[Mercedes-Benz in Formula One|Mercedes]] was able to proceed into the next part without having to use the softer and faster tyre compound. The same applied for Sebastian Vettel, while his Ferrari team mate Kimi Räikkönen was once again unhappy with his setup and recorded his time on the softer medium tyres. [[McLaren]]'s upward trend continued, as both cars progressed into Q2, the first time the team achieved this in 2015. The [[Manor Marussia F1|Manor Marussia]] drivers took their familiar place at the back of the grid, [[Will Stevens]] in 19th almost three seconds off the time of the next-slowest car. Both [[Force India]] cars as well as the [[Sauber]] of [[Marcus Ericsson]] did not make it into Q2 either.<ref name=quali>{{cite web|last1=Collantine|first1=Keith|title=Rosberg halts Hamilton's pole run in Spain|url=http://www.f1fanatic.co.uk/2015/05/09/rosberg-halts-hamiltons-pole-run-in-spain/|publisher=F1Fanatic|accessdate=12 May 2015|date=9 May 2015}}</ref>

The second part of qualifying saw both Mercedes drivers and [[Valtteri Bottas]] make only one timed run, which proved sufficient to get into Q3 comfortably. The second Sauber of [[Felipe Nasr]] was eliminated as were both McLarens and both Lotus cars, meaning that the last five rows on the grid would each be taken by pairs of team mates.<ref name=quali />

For the last session of qualifying, the top ten drivers had twelve minutes to set their times. Nico Rosberg recorded provisional pole on his first outing, a quarter of a second in front of team mate [[Lewis Hamilton]]. Unlike their team mates, both [[Felipe Massa]] in the [[Williams Grand Prix Engineering|Williams]] and Kimi Räikkönen did not have an extra set of fresh tyres, meaning they were caught out by the fast-running [[Scuderia Toro Rosso|Toro Rosso]] drivers, who took the third row on the grid. Neither Rosberg nor Hamilton were able to improve on their times in their second running. For the first time in 2015, it was not Hamilton who would start the race first on the grid, as Rosberg recorded the 16th pole position of his career.<ref name=quali />

====Post-qualifying====
Toro Rosso were delighted with their result, even though [[Carlos Sainz Jr.]] admitted he did not expect to fare as well in the race.<ref>{{cite web|last1=Collantine|first1=Keith|title="Very happy" Sainz doesn't expect fifth in race|url=http://www.f1fanatic.co.uk/2015/05/09/very-happy-sainz-doesnt-expect-fifth-in-race/|publisher=F1Fanatic|accessdate=12 May 2015|date=9 May 2015}}</ref> Rosberg was satisfied with his first pole position of the season, commenting "of course I needed it sooner rather than later," referring to the 27-point gap in the championship to Hamilton.<ref>{{cite web|last1=Johnson|first1=Daniel|title=Spanish Grand Prix 2015: Nico Rosberg takes pole position to turn table on Lewis Hamilton in Barcelona|url=http://www.telegraph.co.uk/sport/motorsport/formulaone/11594699/Spanish-Grand-Prix-2015-Nico-Rosberg-takes-pole-position-to-turn-table-on-Lewis-Hamilton-in-Barcelona.html|newspaper=The Daily Telegraph|accessdate=12 May 2015|date=9 May 2015}}</ref> As pole position is statistically of crucial importance to winning the race, even more so than in [[Monaco Grand Prix|Monaco]], being first on the grid was considered to be a big advantage for Rosberg.<ref>{{cite web|title=The Spanish Grand Prix – did you know?|url=https://www.formula1.com/content/fom-website/en/latest/features/2015/5/the-spanish-grand-prix---did-you-know-.html|website=formula1.com|publisher=FIA|accessdate=12 May 2015}}</ref>

===Race===
[[File:Start 2015 Spanish Grand Prix.jpg|thumb|The start of the race]]
At the start of the race, Lewis Hamilton did not get away well, having to yield second place to Sebastian Vettel while also coming under pressure from Valtteri Bottas. Nico Rosberg remained in front while the rest of the field got through the first corners without incidents. Kimi Räikkönen had a good first lap and moved up to fifth. The Toro Rosso drivers were unable to capitalise on their good qualifying performances and steadily headed down the order. As Rosberg was developing a lead, Hamilton told his team that overtaking Vettel on track was "impossible." Mercedes switched him to a three-stop strategy, starting on lap 14, but his first stop was marred by a problem with the wheel nut, not being able to get ahead of Rosberg who made a pit stop two laps later without problems. During the following laps, Hamilton tried to overtake Vettel on track, but to no avail. Unlike the top runners, Kimi Räikkönen did not take the medium tyres during his middle stint, but chose instead to run on the hard compound, meaning he would be a force to watch towards the end of the race.<ref name=McVeigh10May/>

Battles for position in midfield during the opening laps saw the two Lotus cars touch, damaging [[Pastor Maldonado]]'s rear wing. When he came into the pit for his first stop, the entire right side plate of the wing was taken off, later forcing him to retire on lap 47. Fernando Alonso had previously become the first retirement of the race when his brake failed due to a torn-off visor lodging in his rear brake.<ref>{{cite web|last1=Parkes|first1=Ian|title=Spanish GP: Tear-off caused Fernando Alonso's McLaren brake issue|url=http://www.autosport.com/news/report.php/id/118916|website=autosport.com|accessdate=12 May 2015|date=11 May 2015}}</ref> When he came into the pit lane on lap 28, he overshot his pit box and hit a mechanic, and was unable to continue. [[Red Bull Racing|Red Bull]], who had a troublesome weekend, qualifying behind their sister team Toro Rosso, made up for lost ground and moved ahead to eventually finish seventh and tenth.<ref name=McVeigh10May/>

Up in front, Hamilton put his three-stop strategy to good use, consistently setting the fastest laps on track. On lap 46, Rosberg made a pit stop for the second and final time, emerging just a few seconds ahead of Hamilton, who quickly moved past him into the lead. Hamilton came in for his last stop on lap 51, easily retaining second place in front of Vettel's Ferrari. He subsequently tried to close the gap to Rosberg in front, but a 19-second lead proved impossible to overcome in the remaining 14 laps. Räikkönen had closed in on Valtteri Bottas and tried to overtake him during the last laps of the race, but was eventually unable to do so, leaving him in fifth position, lamenting his poor qualifying performance. As the race drew to a close, Rosberg crossed the line for his first victory of the season and the ninth of his career.<ref>{{cite web|last1=McVeigh|first1=Niall|title=Spanish Grand Prix: F1 – as it happened|url=https://www.theguardian.com/sport/live/2015/may/10/spanish-grand-prix-f1-live|newspaper=The Guardian|accessdate=12 May 2015|date=10 May 2015}}</ref> Rosberg's victory made him the ninth different winner of the [[Spanish Grand Prix]] in as many years.<ref>{{cite web|last1=Baldwin|first1=Alan|title=Nico Rosberg turns the tide with Spanish win|url=http://in.reuters.com/article/2015/05/10/motor-racing-spanish-grand-prix-idINKBN0NV0JN20150510|publisher=Reuters|accessdate=15 May 2015|date=11 May 2015}}</ref>

===Post-race===
Race winner Nico Rosberg said he had a "perfect weekend" during the podium interview conducted by Spanish TV presenter Maria Serrat. At the official post-race press conference, [[Lewis Hamilton]] conceded that it had "been a long time since I've had such a poor start" and went on to explain how difficult it is to overtake on the circuit. Sebastian Vettel admitted that had Hamilton's first stop not gone wrong, it would have been unlikely to stay ahead of him at that point.<ref>{{cite web|title=FIA post-race press conference – Spain|url=http://www.formula1.com/content/fom-website/en/latest/headlines/2015/5/fia-post-race-press-conference---spain.html|publisher=FIA|accessdate=12 May 2015|date=10 May 2015}}</ref>

Kimi Räikkönen, who had decided to drive with the old setup of the car, later stated that he had made a "sacrifice" for the team, saying "I think we learned a lot from going for two separate cars following yesterday and today so I'm more confident we can see things more balanced." He went on to point out that the characteristics of the circuit suited Mercedes more than Ferrari, as they had more downforce and raw speed, stating he was not too concerned with the "bigger than normal" gap, after Vettel had finished 45 seconds behind Rosberg.<ref>{{cite web|last1=Medland|first1=Chris|title=Raikkonen made 'sacrifice' for Ferrari|url=http://en.f1i.com/news/11441-raikkonen-made-sacrifice-for-ferrari.html|website=f1i.com|accessdate=12 May 2015|date=10 May 2015}}</ref>

Following another difficult weekend and constant problems with their underpowered Renault power units, Red Bull repeated their threats voiced earlier in the season to exit the sport. Their motorsport advisor [[Helmut Marko]] was quoted saying: "If we don't have a competitive engine in the near future, then either [[Audi]] is coming or we are out."<ref>{{cite web|last1=Benson|first1=Andrew|title=Red Bull say they will quit F1 unless Audi join forces with them|url=http://www.bbc.com/sport/0/formula1/32688667|website=bbc.com|publisher=BBC|accessdate=12 May 2015|date=11 May 2015}}</ref> Audi had just two weeks earlier stated that it had "no plans to enter Formula 1."<ref>{{cite web|last1=Benson|first1=Andrew|title=Audi: Motoring giant 'has no plans to enter Formula 1'|url=http://www.bbc.com/sport/0/formula1/32506081|website=bbc.com|publisher=BBC|accessdate=12 May 2015|date=28 April 2015}}</ref> Team principal [[Christian Horner]] had earlier declared the {{F1|2015}} season a "write-off" based on their engine reliability issues.<ref>{{cite web|last1=Edmondson|first1=Laurence|title=Renault reliability has made 2015 a write-off for Red Bull – Christian Horner|url=http://www.espn.co.uk/f1/story/_/id/12860087/renault-reliability-made-2015-write-red-bull-christian-horner|website=espn.co.uk|accessdate=12 May 2015|date=10 May 2015}}</ref>

==Classification==

===Qualifying===
{| class="wikitable" style="font-size: 85%;"
|-
!rowspan="2"|{{Tooltip|Pos.|Qualifying position}}
!rowspan="2"|{{Tooltip|Car<br>no.|Number}}
!rowspan="2"|Driver
!rowspan="2"|Constructor
!colspan="3"|{{nowrap|Qualifying times}}
!rowspan="2"|{{Tooltip|Final<br>grid|Final grid position}}
|-
!Q1
!Q2
!Q3
|-
! 1
| style="text-align:center"|6
| {{flagicon|GER}} [[Nico Rosberg]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 1:26.490
| '''1:25.166'''
| '''1:24.681'''
| 1
|-
! 2
| style="text-align:center"|44
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| '''1:26.382'''
| 1:25.740
| 1:24.948
| 2
|-
! 3
| style="text-align:center"|5
| {{flagicon|GER}} [[Sebastian Vettel]]
| [[Scuderia Ferrari|Ferrari]]
| 1:27.534
| 1:26.167
| 1:25.458
| 3
|-
! 4
| style="text-align:center"|77
| {{flagicon|FIN}} [[Valtteri Bottas]]
| [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:27.262
| 1:26.197
| 1:25.694
| 4
|-
! 5
| style="text-align:center"|55
| {{flagicon|ESP}} [[Carlos Sainz Jr.]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| 1:26.773
| 1:26.475
| 1:26.136
| 5
|-
! 6
| style="text-align:center"|33
| {{flagicon|NED}} [[Max Verstappen]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| 1:27.393
| 1:26.441
| 1:26.249
| 6
|-
! 7
| style="text-align:center"|7
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| [[Scuderia Ferrari|Ferrari]]
| 1:26.637
| 1:26.016
| 1:26.414
| 7
|-
! 8
| style="text-align:center"|26
| {{flagicon|RUS}} [[Daniil Kvyat]]
| [[Red Bull Racing]]-[[Renault in Formula One|Renault]]
| 1:27.833
| 1:26.889
| 1:26.629
| 8
|-
! 9
| style="text-align:center"|19
| {{flagicon|BRA}} [[Felipe Massa]]
| [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:27.165
| 1:26.147
| 1:26.757
| 9
|-
! 10
| style="text-align:center"|3
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Red Bull Racing]]-[[Renault in Formula One|Renault]]
| 1:26.611
| 1:26.692
| 1:26.770
| 10
|-
! 11
| style="text-align:center"|8
| {{flagicon|FRA}} [[Romain Grosjean]]
| [[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:27.383
| 1:27.375
|
| 11
|-
! 12
| style="text-align:center"|13
| {{nowrap|{{flagicon|VEN}} [[Pastor Maldonado]]}}
| [[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:27.281
| 1:27.450
|
| 12
|-
! 13
| style="text-align:center"|14
| {{flagicon|ESP}} [[Fernando Alonso]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| 1:27.941
| 1:27.760
|
| 13
|-
!14
| align="center" | 22
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| 1:27.813
| 1:27.854
|
| 14
|-
! 15
| style="text-align:center"|12
| {{flagicon|BRA}} [[Felipe Nasr]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:27.625
| 1:28.005
|
| 15
|-
! 16
| style="text-align:center"|9
| {{flagicon|SWE}} [[Marcus Ericsson]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:28.112
|
|
| 16
|-
! 17
| style="text-align:center"|27
| {{flagicon|GER}} [[Nico Hülkenberg]]
| {{nowrap|[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]}}
| 1:28.365
|
|
| 17
|-
! 18
| style="text-align:center"|11
| {{flagicon|MEX}} [[Sergio Pérez]]
| {{nowrap|[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]}}
| 1:28.442
|
|
| 18
|-
! 19
| style="text-align:center"| 28
| {{flagicon|GBR}} [[Will Stevens]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| 1:31.200
|
|
| 19
|-
! 20
| style="text-align:center"| 98
| {{flagicon|ESP}} [[Roberto Merhi]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| 1:32.038
|
|
| 20
|-
! colspan=8 | 107% time: 1:32.428
|-
! colspan=8 | Source:<ref>{{cite web|title=2015 Spain Qualifying|url=http://www.formula1.com/content/fom-website/en/championship/results/2015-race-results/2015-spain-results/qualifying.html|publisher=FIA|accessdate=10 May 2015|date=9 May 2015}}</ref>
|-
|}

===Race===
{|class="wikitable" style="font-size: 85%;"
!{{Tooltip|Pos.|Position}}
!{{Tooltip|No.|Car number}}
!Driver
!Constructor
!Laps
!Time/Retired
!Grid
!Points
|-
! 1
| align="center" | 6
| {{flagicon|GER}} '''[[Nico Rosberg]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| align="center" | 66
| 1:41:12.555
| align="center" | 1
| align="center" |  '''25'''
|-
! 2
| align="center" | 44
| {{flagicon|GBR}} '''[[Lewis Hamilton]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| align="center" | 66
| +17.551
| align="center" | 2
| align="center" |  '''18'''
|-
! 3
| align="center" | 5
| {{flagicon|GER}} '''[[Sebastian Vettel]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| align="center" | 66
| +45.342
| align="center" | 3
| align="center" | '''15'''
|-
! 4
| align="center" | 77
| {{flagicon|FIN}} '''[[Valtteri Bottas]]'''
| '''[[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 66
| +59.217
| align="center" | 4
| align="center" | '''12'''
|-
! 5
| align="center" | 7
| {{flagicon|FIN}} '''[[Kimi Räikkönen]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| align="center" | 66
| +1:00.002
| align="center" | 7
| align="center" |  '''10'''
|-
! 6
| align="center" | 19
| {{flagicon|BRA}} '''[[Felipe Massa]]'''
| '''[[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 66
| +1:21.314
| align="center" | 9
| align="center" | '''8'''
|-
! 7
| align="center" | 3
| {{flagicon|AUS}} '''[[Daniel Ricciardo]]'''
| '''[[Red Bull Racing]]-[[Renault in Formula One|Renault]]'''
| align="center" | 65
| +1 Lap
| align="center" | 10
| align="center" | '''6'''
|-
! 8
| align="center" | 8
| {{flagicon|FRA}} '''[[Romain Grosjean]]'''
| '''[[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 65
| +1 Lap
| align="center" | 11
| align="center" | '''4'''
|-
! 9
| align="center" | 55
| {{flagicon|ESP}} '''[[Carlos Sainz Jr.]]'''
| '''[[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]'''
| align="center" | 65
| +1 Lap
| align="center" | 5
| align="center" | '''2'''
|-
! 10
| align="center" | 26
| {{flagicon|RUS}} '''[[Daniil Kvyat]]'''
| '''[[Red Bull Racing]]-[[Renault in Formula One|Renault]]'''
| align="center" | 65
| +1 Lap
| align="center" | 8
| align="center" | '''1'''
|-
! 11
| align="center" | 33
| {{flagicon|NED}} [[Max Verstappen]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| align="center" | 65
| +1 Lap
| align="center" | 6
|
|-
! 12
| align="center" | 12
| {{flagicon|BRA}} [[Felipe Nasr]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 65
| +1 Lap
| align="center" | 15
|
|-
! 13
| align="center" | 11
| {{flagicon|MEX}} [[Sergio Pérez]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="center" | 65
| +1 Lap
| align="center" | 18
|
|-
! 14
| align="center" | 9
| {{flagicon|SWE}} [[Marcus Ericsson]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 65
| +1 Lap
| align="center" | 16
|
|-
! 15
| align="center" | 27
| {{flagicon|GER}} [[Nico Hülkenberg]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="center" | 65
| +1 Lap
| align="center" | 17
|
|-
! 16
| align="center" | 22
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| align="center" | 65
| +1 Lap
| align="center" | 14
|
|-
! 17
| align="center" | 28
| {{flagicon|GBR}} [[Will Stevens]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 63
| +3 Laps
| align="center" | 19
|
|-
! 18
| align="center" | 98
| {{flagicon|ESP}} [[Roberto Merhi]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 62
| +4 Laps
| align="center" | 20
|
|-
! Ret
| align="center" | 13
| {{flagicon|VEN}} [[Pastor Maldonado]]
| [[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="center" | 45
| Collision damage
| align="center" | 12
|
|-
! Ret
| align="center" | 14
| {{flagicon|ESP}} [[Fernando Alonso]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| align="center" | 26
| Brakes
| align="center" | 13
|
|-
! colspan=8 | Source:<ref name=McVeigh10May>{{cite web|last1=McVeigh|first1=Niall|title=Spanish Grand Prix: F1&nbsp;– as it happened|url=https://www.theguardian.com/sport/live/2015/may/10/spanish-grand-prix-f1-live|newspaper=The Guardian|accessdate=10 May 2015|date=10 May 2015}}</ref><ref>{{cite web|title=2015 Spain Results|url=http://www.formula1.com/content/fom-website/en/championship/results/2015-race-results/2015-spain-results/race.html|publisher=FIA|accessdate=10 May 2015|date=10 May 2015}}</ref>
|-
|}

===Championship standings after the race===
{{col-start}}
{{col-2}}
;Drivers' Championship standings
{|class="wikitable" style="font-size: 85%;"
|-
!
!{{Tooltip|Pos.|Position}}
!Driver
!Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 1
| {{flagicon|GBR}} [[Lewis Hamilton]]
| align="left" | 111
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 2
| {{flagicon|GER}} [[Nico Rosberg]]
| align="left" | 91
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 3
| {{nowrap|{{flagicon|GER}} [[Sebastian Vettel]]}}
| align="left" | 80
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 4
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| align="left" | 52
|-
|align="left"| [[File:1uparrow green.svg|10px]] 1
| align="center" | 5
| {{flagicon|FIN}} [[Valtteri Bottas]]
| align="left" | 42
|-
|}
{{col-2}}
;Constructors' Championship standings
{|class="wikitable" style="font-size: 85%;"
|-
!
!{{Tooltip|Pos.|Position}}
!Constructor
!Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 1
| {{flagicon|DEU}} [[Mercedes-Benz in Formula One|Mercedes]]
| align="left" | 202
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 2
| {{flagicon|ITA}} [[Scuderia Ferrari|Ferrari]]
| align="left" | 132
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 3
| {{flagicon|GBR}} [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="left" | 81
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 4
| {{nowrap|{{flagicon|AUT}} [[Red Bull Racing]]-[[Renault in Formula One|Renault]]}}
| align="left" | 30
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 5
| {{flagicon|CHE}}&nbsp;&nbsp;&nbsp;[[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| align="left" | 19
|-
|}
{{col-end}}
* <small>'''Note''': Only the top five positions are included for both sets of standings.</small>

==References==
{{reflist|30em}}

==External links==
* [https://www.formula1.com/content/fom-website/en/championship/races/2015/Spain.html Event site at Formula1.com]

{{Commons category|2015 Spanish Grand Prix}}

{{F1 race report
| Name_of_race = [[Spanish Grand Prix]]
| Year_of_race = 2015
| Previous_race_in_season = [[2015 Bahrain Grand Prix]]
| Next_race_in_season = [[2015 Monaco Grand Prix]]
| Previous_year's_race = [[2014 Spanish Grand Prix]]
| Next_year's_race = [[2016 Spanish Grand Prix]]
}}
{{F1GP 10-19}}

[[Category:2015 Formula One races|Spanish]]
[[Category:Spanish Grand Prix]]
[[Category:2015 in Spanish motorsport|Grand Prix]]