{{Infobox lacrosse player
| name = Nolan Godfrey
| image = Godfrey Nanaimo lacrosse.jpg
| image_size = 200px
| birth_date = {{birth date and age|1981|3|31|mf=y}}
| birth_place = [[Haverhill, Massachusetts]]
| position = Midfield/FO (field)<br> Transition/FO (box)
| height_ft = 5
| height_in = 10
| weight_lbs = 193
| league = [[NCAA]]
| team = [[Merrimack College]]
| former_teams =
| nll_team =
| former_nll_team = [[Buffalo Bandits]]
| mll_team = [[Denver Outlaws]]
| former_mll_teams = [[Philadelphia Barrage]]<br> [[Boston Cannons]]<br> [[Ohio Machine]]
| nationality = United States
| nll_draft = 50th overall
| nll_draft_year = 2006
| nll_draft_team = [[Buffalo Bandits]]
| career_start =
| career_end =
| shoots = Right
| nickname =
| website = http://faceoffclub.com
| career_highlights =
;College highlights
* [[USILA All-American Team|NCAA All-American]], Second Team, (2005)
* [[Northeast-10 Conference|Northeast-10]] All-Conference, First Team, (2005)
;Draft history
* [[MLL_Supplemental_Draft#2007_MLL_Supplemental_Draft|2007 MLL Supplemental Draft]] <br>[[Philadelphia Barrage]]: 1st round, 10th pick
| uslaxhof =
| uslaxhof_year =
| canlaxhof =
| nllhof =
}}
'''Nolan Patrick Godfrey''' (born March 31, 1981) is an American professional lacrosse player, most recently for the [[Denver Outlaws]] of [[Major League Lacrosse]].<ref>http://www.denveroutlaws.com/player/1147/majorleaguelacrosse.com</ref>  A former [[USILA All-American Team|All-American]] at [[Merrimack College]], he has experience in [[Major League Lacrosse]], the [[National Lacrosse League]], Senior A level box lacrosse in the [[Western Lacrosse Association]], and for USA Indoor.  Godfrey began playing the sport one month short of his 21st birthday.  Four years later he was an NCAA All-American and the year following was drafted to the MLL in the 1st Round.<ref>http://www.hgazette.com/sports/x1723394619/Godfrey-s-pro-lacrosse-career-grows</ref>

Godfrey is also the president of Elite Player Development, LLC.  His company provides lacrosse development and college recruiting programs throughout the United States, and Europe.  

Godfrey has a background in the entertainment industry with Hollywood film acting experience along with acting, stunt performing, and production work in television.  He was recently involved with [[MTV]]'s Teen Wolf series as a sports action coordinator and technical advisor, stunt double, actor, and camera operator for several episodes during the show's initial season that debuted in the summer of 2011.<ref>http://blog.zap2it.com/frominsidethebox/2011/02/teen-wolf-mtv-sets-premiere-date.html</ref>

==Background==
Godfrey was born in [[Haverhill, Massachusetts]], attending and graduating from [[Haverhill High School]].

== College career ==
===2005 season===
Godfrey collected All-American (second team), All-Conference (first team), and All-New England honors for the 2005 year.  He finished second nationally with a .682 success rate on face-offs.<ref name="northeast10">http://www.northeast10.org/sports/mlax/2005-06/news/2006091406mergodfreynll</ref>

===2006 season===
Prior to the 2006 season, Godfrey was named to the [[Inside Lacrosse]] preseason All-America team.<ref>http://www.lemoynedolphins.com/sports/mlax/2006/news/20051219_mlaxFACEOFFAA</ref>  Godfrey and teammates traveled to LeMoyne for the NE10 Conference Championship game and closed out as league runners-up as LeMoyne won 10-3.<ref>http://www.laxpower.com/laxnews/news.php?story=4009</ref>  Godfrey capped off a workhorse campaign leading the nation in face-off attempts (345) and ground balls (131) and ranking second in face-off wins (211).  He finished his Merrimack career with a 22-10 record and several school records in all face-off and ground ball categories.

== Professional career ==
===National Lacrosse League===
Nolan Godfrey was initially drafted by the [[Buffalo Bandits]] in the 4th round (50th overall selection) of the 2006 [[National Lacrosse League Entry Draft|NLL Entry Draft]]<ref>http://www.nll.com/article.php?id=322</ref> at [[Madison Square Garden]] in New York City.

===Major League Lacrosse===
In December 2006, Godfrey was selected with the tenth overall pick in the 1st round of the [[MLL_Supplemental_Draft#2007_MLL_Supplemental_Draft|2007 MLL Supplemental Draft]] by the defending MLL Champion [[Philadelphia Barrage]].<ref>http://www.oursportscentral.com/services/releases/?id=3403699</ref>   He was acquired prior to the 2008 season by his hometown [[Boston Cannons]].<ref>http://www.oursportscentral.com/services/releases/?id=3567150</ref>  Godfrey began each of his two MLL seasons with his respective teams, though went to Canada both summers to play Senior level indoor box lacrosse.  He rejoined the MLL coming into the [[Ohio Machine]] 2012 season<ref>http://www.theohiomachine.com/news/4913/machine-roster-constructed-for-season/</ref> and prior to appearing in a game for the Machine was acquired mid-season by the [[Denver Outlaws]]<ref>http://inlacrossewetrust.com/?p=610</ref> who he saw brief action with.

===Team USA Indoor===
Godfrey prepared for the 2008 indoor season by playing transition for Team USA Indoor at the Warrior Cup in [[Iroquois Confederacy|Six Nations]] ([[Ontario, Canada]]) and on their Developmental Team's arena exhibition tour.<ref>http://www.laxpower.com/laxnews/news.php?story=9200</ref>

===Canadian Lacrosse Association===
Godfrey was one of the first Americans to embark on an extended career in Canadian [[box lacrosse]] and his first season up North was chronicled in a journal series published in [[Inside Lacrosse]] magazine in late 2006.<ref name="northeast10"/>  His initial experience in the Canadian ranks was with Brooklin of the [[Ontario Lacrosse Association]] in 2006.  In 2007, Godfrey played between the Valleyfield and Longueuil franchises in the province of [[Quebec]].  His career evolved to the highest level of play in Canada in the summer of 2008 when he signed and suited up with the [[Nanaimo Timbermen]] of the Senior-A [[Western Lacrosse Association]] in [[British Columbia]].  A top draw man throughout his Canadian box career, he also led Quebec in penalty minutes served (PIM) with 108 in 2011.<ref>http://hometeamsonline.com/teams/default.asp?u=SRBLAX&t=c&s=lacrosse&p=stats&viewseas=2011&gametype=ALL&psort=penaltyminutes#dt_p</ref>

== Coaching and business career ==
Godfrey's commitment to the indoor game was evident in 2009 when he partnered with Essex, VT businessman Jeff Culkin in starting the first American box lacrosse franchise to compete in an otherwise exclusively Canadian league.<ref>http://www.ilindoor.com/2009/05/01/champlain-challenge-cup-set-for-vermont/</ref>  The two co-owners debuted the Vermont Voyageurs in their inaugural season competing in the Quebec Senior Lacrosse League (QSLL) in 2009.

Godfrey is an experienced coach with time in the NCAA ranks at Division II [[Saint Anselm College]], founder of two Massachusetts boys high school programs ([[Haverhill High School]] and [[Lawrence Central High School|Central Catholic High School]]), Massachusetts Division I state finalist with [[Billerica Memorial High School]],<ref>http://miaa.statebrackets.com/tmenu.cfm?tid=848</ref> and growing the Texas landscape as former head coach of [[St. Andrew's Episcopal School (Texas)|St. Andrew's Episcopal]] in Austin<ref>http://www.sasaustin.org/podium/default.aspx?t=204&nid=489345&sdm=2</ref> and [[Plano Senior High School]].<ref>http://www.westsidelax.com/Nolan-Godfrey-Named-New-Plano-Head-Coach</ref>  He has toured Europe on several occasions to grow the game on a grassroots level by running camps in Spain, Italy, Holland, Switzerland, Scotland, and Finland.  Godfrey was a guest clinician at the 2008 European Lacrosse Championships in Lahti, Finland and blogged about the trip in Inside Lacrosse.<ref>http://insidelacrosse.com/blog/2008/08/25/european-championships-atmosphere-lahti</ref>  He has also coached for the Spanish National Team in 2007.<ref>http://forums.insidelacrosse.com/showthread.php?t=95348</ref>

An innovative entrepreneur of the sport, Godfrey is known as a preeminent clinician of the face-off position<ref>http://www.wikihow.com/Face-off-in-Lacrosse</ref> through his company, [http://faceoffclub.com FaceOff Club].  US Lacrosse has invited him to be a featured speaker at their 2011 National Convention in Baltimore and he returned again to speak in 2012.<ref>http://laxmagazine.com/blogs/dasilva/111110_us_lacrosse_convention_speakers_boast_broad_backgrounds</ref>  Godfrey's business pursuits extend also to Claddagh Lacrosse and Southern Combat Lacrosse.

His business experience is complemented by graduate work at [[Boston University]] in their Master's program in Coaching and Sport Psychology.  BU boasts one of the only such programs in the nation and their research is focused through their Institute for Athletic Coach Education.  Godfrey has taken his higher education and advanced his business to contracting speaking engagements with major corporations and organizations to train their executives and upper management in the topics of team leadership, group coaching, positive psychology, and motivation.

== Personal ==
Godfrey is also involved in the entertainment industry and has appeared in two Hollywood productions.  He has produced several documentaries of the lacrosse and sports cultures in several European nations.   His latest work has been with [[MTV]] in 2011 "Teen Wolf" series in the area of sports choreography and technical advisory, stunt doubling, acting, and camera operation. <ref>http://www.imdb.com/title/tt1567432/fullcredits#cast</ref>

== Honors and achievements ==
'''Honors'''
* NCAA All-American (2005) - Second Team, Specialist
* Northeast 10 All-Conference (2005) - First Team, Specialist
* NEILA All-New England (2005, 2006) - Midfielder
* NEILA Senior East-West All-Star Game (2006)
* Northeast-10 All-Academic (2006)<ref>http://www.northeast10.org/sports/mlax/2005-06/news/2060522ne10mlaxallacademic</ref>
'''Achievements''' 
* NCAA Statistical Leader in Ground Balls (2006) - 131
* NCAA Statistical Runner-up in Face-off Wins (2006) - 211
* NCAA Statistical Runner-up in Face-off Wins (2006) - 197
* NCAA Statistical Runner-up in Face-off Percentage (2006) - .682
''Godfrey was the Merrimack College record holder in all of these single-season categories until 2011''<ref>http://merrimackathletics.com/sports/mlax/2010-11/Record_Book.pdf</ref>

== Statistics ==
=== NCAA ===
{| BORDER="0" CELLPADDING="3" CELLSPACING="1"
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="2" bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
|- ALIGN="center" bgcolor="#e0e0e0"
! Season !! School !! GP !!FO win !! FO att !! FO% !! GB
|- ALIGN="center" bgcolor="#ffffff"
| 2005 || Merrimack || 16 || 197 || 289 || .682 || 89 ||
|- ALIGN="center"
| 2006 || Merrimack || 16 || 211 || 345 || .612 || 131 ||
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="2" |NCAA Totals !!32!!408!!634!!.643!!220
|}

==References==
{{reflist}}

{{DEFAULTSORT:Godfrey, Nolan}}
[[Category:1981 births]]
[[Category:Living people]]
[[Category:Articles created via the Article Wizard]]
[[Category:Boston University alumni]]
[[Category:Merrimack College alumni]]
[[Category:Sportspeople from Haverhill, Massachusetts]]
[[Category:Sportspeople from Massachusetts]]