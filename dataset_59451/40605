{{Infobox scientist
| honorific_prefix =
| name        = Richard Paul Korf
| honorific_suffix =
| native_name = 
| native_name_lang = 
| image       =         <!--(filename only, i.e. without "File:" prefix)-->
| image_size  = 
| alt         = 
| caption     = 
| birth_date  = {{birth date |1925|05|28}}
| birth_place = 
| death_date  = {{death date and age |2016|08|20|1925|05|28}}
| death_place = [[Ithaca, New York|Ithaca]], New York
| death_cause = 
| resting_place = 
| resting_place_coordinates =  <!--{{coord|LAT|LONG|type:landmark|display=inline,title}}-->
| other_names = 
| residence   = 
| citizenship = United States
| nationality = American
| fields      = [[Mycology]]
| workplaces  = 
| patrons     = [[Herbert Hice Whetzel]]<br />[[Harry Morton Fitzpatrick]]
| education   = 
| alma_mater  =  [[Riverdale Country School]]<br />[[Cornell University]]
| thesis_title =        <!--(or  | thesis1_title =  and  | thesis2_title = )-->
| thesis_url  =         <!--(or  | thesis1_url  =   and  | thesis2_url  =  )-->
| thesis_year =         <!--(or  | thesis1_year =   and  | thesis2_year =  )-->
| doctoral_advisor =    <!--(or  | doctoral_advisors = )-->
| academic_advisors = 
| doctoral_students = 
| notable_students = 
| known_for   = Founding co-editor of the journal ''[[Mycotaxon]]''
| influences  = 
| influenced  = 
| awards      = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| spouse      =         <!--(or | spouses = )-->
| partner     =         <!--(or | partners = )-->
| children    = 
| signature   =         <!--(filename only)-->
| signature_alt = 
| website     =         <!--{{URL|www.example.com}}-->
| footnotes   = 
}}

'''Richard Paul "Dick" Korf''' (May 28, 1925 – August 20, 2016) was an American [[Mycology|mycologist]] and founding co-editor of the journal ''[[Mycotaxon]]''. He was a preeminent figure in the study of [[discomycetes]] and made significant contributions to the field of fungal nomenclature and taxonomy. Korf was Professor Emeritus of Mycology at [[Cornell University]] and Director Emeritus of Cornell University’s Plant Pathology Herbarium.

== Early life and education ==

Korf was born on May 28, 1925, to an upper-middle-class family with homes in [[Westchester County, New York]], and [[New Fairfield, Connecticut]]. While attending the prestigious [[Riverdale Country School]] in New York City, Korf was placed in charge of his biology class after their teacher joined the military. "In retrospect," Korf wrote, "I am convinced that this experience had an enormous impact on my future and on my decision to enter the teaching profession."<ref name=":0">{{Cite journal|last=Korf|first=Richard P.|year=1991|title=An Historical Perspective: Mycology In The Departments of Botany and of Plant Pathology at Cornell University and the Geneva Agricultural Experiment Station|url=https://ecommons.cornell.edu/bitstream/handle/1813/28593/Korf%20Mycology%20Plant%20Path%20History%2025Feb12.pdf|journal=Mycotaxon|volume=40}}</ref>

In 1942, shortly after his 17th birthday, Korf enrolled at [[Cornell University]] "with the vague notion that [he] might like to become a gentleman farmer."<ref name=":0" /> At the suggestion of botany professor Loren C. Petry, Korf began studying plant pathology under [[Herbert Hice Whetzel]].<ref name=":5">{{Cite web|url=http://hdl.handle.net/1813/28593|title=A Conversation with Richard P. Korf|last=|first=|date=2012|website=eCommons: Cornell's digital repository|publisher=The Internet-First University Press|access-date=2016-09-30}}</ref> Korf continued his training under mycologist [[Harry Morton Fitzpatrick]], whom he nicknamed "Prof Fitz." Korf completed his B.S. in botany in 1946 and doctorate in plant pathology and mycology with minors in genetics and general botany in 1950.<ref name=":0" />

== Contributions to mycology ==

=== Professorship ===

Following the completion of his doctorate, Korf spent a year teaching mycology at the [[University of Glasgow]] in Scotland.<ref name=":1">{{Cite journal|last=Lizoň|first=Pavel|year=2015|title=Richard ("Dick") P. Korf’s 90th birthday|url=http://www.imafungus.org/Issue/61/04.pdf|journal=IMA Fungus|volume=6|issue=1|page=20}}</ref> He returned to Cornell in 1951 as a faculty member in the Department of Plant Pathology. He was named Associate Professor in 1955 and Professor of Mycology in 1961.<ref name=":2">{{Cite web|url=https://pppmb.cals.cornell.edu/people/richard-p-korf|title=Richard P. Korf {{!}} Plant Pathology and Plant-Microbe Biology Section|last=|first=|date=2016|website=|publisher=Cornell University|access-date=2016-09-30}}</ref> He retired in 1992 but continued to teach until 1998, when he was replaced by Kathie Hodge.<ref name=":5" /> Korf trained and advised 27 PhD students,<ref name=":3">{{Cite web|url=http://www.news.cornell.edu/stories/2016/08/renowned-mycologist-richard-korf-dies-91|title=Renowned mycologist Richard Korf dies at 91|last=Ramanujan|first=Krishna|date=August 25, 2016|website=Cornell Chronicle|publisher=|access-date=2016-09-30}}</ref> including [[Lekh Raj Batra]], William C. Denison,  K. P. Dumont, James Kimbrough, Donald H. Pfister, Martin A. Rosinski, Robert L. Shaffer, Robert A. Shoemaker, and [[Wenying Zhuang]].<ref name=":0" />

=== Herbarium ===
Korf was Director Emeritus of the Cornell University Plant Pathology Herbarium (CUP), one of the largest fungal herbariums in the United States.<ref name=":2" /> Korf traveled globally over the course of his career, collecting fungi in Belgium, Denmark, Dominican Republic, Finland, France, Hungary, Indonesia, Jamaica, Japan, [[Macaronesia]], New Zealand, Norway, Philippines, Portugal, Puerto Rico, Scotland, Singapore, Sweden, and Taiwan. The Richard P. Korf Herbarium within CUP boasts nearly 5000 specimens, including 257 type specimens. This collection is dominated by taxa of particular interest to Korf: ''[[Arachnopeziza]]'', ''Calycella'' (now ''[[Bisporella]]''), ''[[Cheilymenia]]'', ''[[Chlorosplenium]]'', ''Dasyscyphus'', ''[[Hyaloscypha]]'', ''[[Hymenoscyphus]]'', ''[[Mollisia]]'', [[Orbiliaceae]], ''[[Peziza]]'', ''[[Rutstroemia]]'', ''[[Scutellinia]]'', and ''[[Trichophaea]]''.<ref name=":4">{{Cite journal|last=LaGreca|first=Scott|year=2015|title=The Richard P. Korf Herbarium and CUP’s geographical collections: the enduring legacy of a globe-trotting mycologist|url=http://bermudalichens.myspecies.info/sites/bermudalichens.myspecies.info/files/AscomyceteOrg%2007-06%20245-254_2.pdf|journal=Ascomycete.org|volume=7|issue=6}}</ref>

=== ''Mycotaxon'' ===

Korf co-founded ''[[Mycotaxon]]'', an international peer-reviewed journal of taxonomy and nomenclature of fungi and lichens, with co-worker Grégoire L. Hennebert in 1974.<ref>{{Cite web|url=http://www.mycotaxon.com/about.html|title=About Mycotaxon|last=|first=|date=|website=Mycotaxon|publisher=|access-date=2016-09-30}}</ref> Korf and Hennebert were frustrated that papers submitted to journals such as ''[[Mycologia]]'' took a year or longer from submission to publication. Korf and Hennebert introduced a number of innovations to make their journal more efficient and accessible than its contemporaries. ''Mycotaxon'' reduced the wait time between submission and publication by requiring authors to submit camera-ready copy. [[Linotype machine|Linotype]] was the industry standard at the time; ''Mycotaxon'' used photo-offset [[lithography]] to expedite publication.<ref name=":5" /> A quarterly journal, ''Mycotaxon'' aimed to publish papers within four months of submission.<ref name=":6" /> ''Mycotaxon'' took an unusual non-blind approach to refereeing: authors were required to enlist a reviewer outside of their institution to peer-review their manuscript prior to its submission.<ref name=":5" /> Initially ''Mycotaxon'' did not demand page charges from authors, rather relying on subscription fees to finance publication. Papers of all lengths were accepted.<ref name=":6">{{Cite journal|last=Korf|first=Richard P.|last2=Hennebert|first2=G. L.|year=1974|title=Mycotaxon, a new international journal on taxonomy and nomenclature of fungi and lichens|url=http://www.cybertruffle.org.uk/cyberliber/59575/0001/001/0001.htm|journal=Mycotaxon|volume=1|issue=1}}</ref>

Korf oversaw journal preparation, final editing, and subscription management as Managing Editor & English Language Editor (1974-1991) and Business Manager (1991-2006). He served as Assistant to the Editor-in-Chief from 1998 to 2003 and as Treasurer of the Corporation from 1999 to his passing in 2016.<ref>{{Cite web|url=http://mycotaxon.com/about/editors.html|title=Editors and Administrators|last=|first=|date=|website=Mycotaxon|publisher=|access-date=2016-09-30}}</ref>

''Mycotaxon'' reached 131 published volumes in 2016.<ref>{{Cite web|url=http://www.mycotaxon.com/vol-list.html|title=Publication History of the journal MYCOTAXON|last=|first=|date=|website=Mycotaxon|publisher=|access-date=2016-09-30}}</ref>

=== Taxa described ===
Korf described or reclassified hundreds of species of fungi. The standard [[List of botanists by author abbreviation|author abbreviation]] '''Korf''' is applied to taxa he described. The list below is only a small selection of the taxa described by Korf. 
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=393372&Fields=All ''Arachnopeziza aranea f. monilipila'' Korf 1981]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=265412&Fields=All ''Arachnopeziza fitzpatrickii'' Korf 1952]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=276493&Fields=All ''Arachnopeziza japonica'' Korf 1959]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=276494&Fields=All ''Arachnopeziza nuda'' Korf 1959]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=99639&Fields=All ''Callorina'' Korf 1971]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=256807&Fields=All ''Chlorosplenium rodwayi'' Korf 1959]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=94716&Fields=All ''Dencoeliopsis'' Korf 1971]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=8664&Fields=All ''Dipodascus albidus f. minor'' Korf 1957]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=179942&Fields=All ''Eriopezia samuelsii'' Korf 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=95335&Fields=All ''Geocoryne'' Korf 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=180010&Fields=All ''Geocoryne'' ''variispora'' Korf 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=100713&Fields=All ''Gremmenia'' Korf 1962]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=92865&Fields=All ''Hemiphacidiaceae'' Korf 1962]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=56688&Fields=All ''Hemiphacidium'' Korf 1962]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=56754&Fields=All ''Iodophanus'' Korf 1967]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=95800&Fields=All ''Jafnea'' Korf 1960]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=183808&Fields=All ''Jafnea'' ''imaii'' Korf 1960]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=273198&Fields=All ''Lasiobelonium'' ''dumontii'' Korf 1976]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=173811&Fields=All ''Marasmiellus'' ''corticis'' Korf 1986]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=92919&Fields=All ''Medeolariaceae'' Korf 1982]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=92478&Fields=All ''Medeolariales'' Korf 1982]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=279176&Fields=All ''Octospora'' ''metachromatica'' Korf 1969]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=188734&Fields=All ''Orbilia'' ''lunata'' Korf 1992]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=96931&Fields=All ''Parachnopeziza'' Korf 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=58755&Fields=All ''Pezicula'' ''linda'' Korf 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=122466&Fields=All ''Peziza'' ''badioconfusa'' Korf 1954]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=20192&Fields=All ''Peziza'' ''ostracoderma'' Korf 1960]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=155467&Fields=All ''Peziza'' ''pudicella'' Korf 1982]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=211085&Fields=All ''Pezoloma'' ''kathiae'' Korf 1999]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=443067&Fields=All ''Pezoloma'' subgen. ''Phaeopezoloma'' Korf 1999]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=279317&Fields=All ''Phaeohelotium asiaticum'' Korf 1959]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=414911&Fields=All ''Phialea eustrobilina'' Korf 1958]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=21930&Fields=All ''Plectania nannfeldtii'' Korf 1957]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=22361&Fields=All ''Polydesmia fructicola'' Korf 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=57251&Fields=All ''Pseudopezicula'' Korf 1986]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=285039&Fields=All ''Rutstroemia macrospora f. gigaspora'' Korf 1959]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=93143&Fields=All ''Vibrisseaceae'' Korf 1990]

== Personal life ==

While on sabbatical in Japan in 1957, Korf met his wife Kumiko “Kumi" Tachibana.<ref name=":1" /> Her younger sisters was a student in his English conversation group at [[Yokohama National University]].<ref name=":4" /> They married and had four children together: Noni, Mia, Ian, and Mario. Kumi is a fine artist specializing in printmaking.<ref>{{Cite web|url=http://www.ithaca.com/entertainment/kumi-korf-subject-of-film-hidden-books/article_9c9e5ef2-784f-11e1-80ff-0019bb2963f4.html|title=Kumi Korf Subject of Film: Hidden Books|last=Olds|first=Majorie|date=March 28, 2012|website=Ithaca Times|publisher=|access-date=2016-09-30}}</ref>

In the 1960s, Korf chaired the [[Liberal Party of New York|Liberal Party]] of [[Tompkins County, New York]].<ref>{{Cite news|url=http://cdsun.library.cornell.edu/cgi-bin/cornell?a=d&d=CDS19660926-01.2.5&e=--------20--1-----all----|title=Ewing, O’Connor Address Ithaca Residents|last=Guldin|first=Robert E.|date=September 26, 1966|work=The Cornell Daily Sun|issue=7|volume=83|access-date=2016-09-30|via=}}</ref>

In 1972, Korf purchased Exe Island on [[Big Rideau Lake]] in Portland, Ontario, Canada. He later transferred the three acre island with cottage to Mycotaxon, Ltd. as Exe Island Biological Station.<ref name=":1" />

Korf enjoyed the card game [[Crazy Eights]], and is remembered for organizing tournaments at mycological conferences and during holidays on Exe Island.<ref name=":1" />

Korf passed away on August 20, 2016 at the age of 91 at home in Ithaca, New York.<ref name=":3" />

== Thespian activity ==

Korf was an active thespian with a lifelong enthusiasm for theatre arts. Performing in plays since childhood, he acted on Cornell and Ithaca stages and in the recording studio. Korf performed under the pseudonym "Jonah Webster" as a student to hide his "favorite avocation" from professor Harry Morton Fitzpatrick, who disapproved of non-mycological pursuits. Korf served part-time as chair of Cornell’s Department of Theatre Arts from 1985 to 1986.<ref name=":0" />

== Honors, awards, and memberships ==
In November 2015, the journal [[Ascomycete.org]] published a Jubilee tribute issue in honor of Korf's 90th birthday.<ref>{{Cite web|url=http://www.ascomycete.org/Home/tabid/58/ID/37/Special-issue-dedicated-to-RP-Korf-90th-birthday.aspx|title=Special issue dedicated to R.P. Korf (90th birthday)|last=Van Vooren|first=Nicolas|date=November 30, 2015|website=Ascomycete.org|publisher=|access-date=16 October 2016}}</ref>

=== Epomymous taxa ===

* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=270609&Fields=All ''Anthracobia korfii'' S. Ahmad 1978]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=510704&Fields=All ''Chlorencoelia ripakorfii'' Iturr & Mardones 2013] 
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=546232&Fields=All ''Cookeina korfii'' Iturr., Xu, F & Pfister 2015] 
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=272257&Fields=All ''Discina korfii'' Raitv. 1970]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=456340&Fields=All ''Geopyxis korfii'' W.Y. Zhuang 2006]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=272258&Fields=All ''Gyromitra korfii'' (Raitv.) Harmaja 1973]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=95898&Fields=All ''Korfia'' J. Reid & Cain 1963]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=95899&Fields=All ''Korfiella'' D.C. Pant & V.P. Tewari 1970]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=426251&Fields=All ''Korfiomyces'' Iturr. & D. Hawksw. 2004]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=136470&Fields=All ''Lambertella korfii'' W.Y. Zhuang 1990]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=180674&Fields=All ''Naemacyclus korfii'' V.G. Rao, Ullasa & A.S. Patil 1972]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=52231&Fields=All ''Phoma korfii'' Boerema & Gruyter 1999]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=546016&Fields=All ''Phylacia korfii'' J. Fourn. & Lechat 2015]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=546024&Fields=All ''Protocreopsis korfii'' Lechat C. & Fournier J. 2016]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=211002&Fields=All ''Sarcoscypha korfiana'' F.A. Harr. 1997] 
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=443640&Fields=All ''Scutellinia korfiana'' W.Y. Zhuang 2005]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=280463&Fields=All ''Scutellinia korfii'' Le Gal 1969]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=275881&Fields=All ''Stictus korfii'' Sherwood 1977]
* [http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=136881&Fields=All ''Strossmayeria dickorfii'' Iturr.1990]

=== Honors and awards ===

* 2010, Ainsworth Medal for extraordinary service to international mycology, International Mycological Congress
* 1996, [[British Mycological Society]] Centenary Fellow
* 1993, Gamma Sigma Delta Cornell Chapter 1992 Distinguished Teaching Award
* 1992, State University of New York Chancellor's Award for Excellence in Teaching
* 1991, Distinguished Mycologist Award, [[Mycological Society of America]]
* 1972-73, Fulbright Senior Research Scholar, [[Université catholique de Louvain|Université Catholique de Louvain]]
* 1957-58, John Simon Guggenheim Memorial Foundation Fellow (declined)
* 1957-58, National Science Foundation Senior Postdoctoral Fellow, [[Yokohama National University]]<ref name=":2" />

=== Memberships ===

* [[Mycological Society of America]]
* [[British Mycological Society]]
* [[Société mycologique de France|Société Mycologique de France]]
* [http://www.mycology-jp.org/~msj7/WL_English_site/index_e.html Mycological Society of Japan]
* [[International Association for Plant Taxonomy]]<ref name=":2" />

== Publications ==

Korf was a prolific author, having published over 400 papers,<ref name=":3" /> many in his journal ''Mycotaxon''. The following list comprises his publications made in his last ten years of writing.

* Korf, R.P. 2011. Henry Dissing: a reminiscence. Asocomycete.org 2(4): 7-8.
* Korf, R. P. & L. L. Norvell. 2011. Cautionary advice to authors who alter their reprints in any way from the original publication. Mycotaxon 114: 485. [2010]
* Korf, R.P., J. W. Lorbeer & W. A. Sinclair. 2010. George Clarence Kent. Memorial Statements, Cornell University Faculty 2008-2009. Office of the Dean of Faculty, Ithaca, New York: 35-37
* Korf, R.P. 2009d. Can we really afford an International Code of Mycological Nomenclature? Mycotaxon 110: 505-507.
* Korf, R. P. 2009c. Naming Nature: the Clash Between Instinct and Science, by Carol Kaesuk Yoon. Inoculum [suppl. to Mycologia] 60(6): 9-10.
* Korf, R. P. 2009b. [Letter, 1 August 2008], pp.&nbsp;117–118 in S. Knapp & Q. Wheeler [eds.], Letters to Linnaeus. London: The Linnean Society, 324 pp.
* Wong, G. J. & R. P. Korf. 2009. Currently known and reported Discomycetes (Ascomycota) of Hawai'i. Pacific Sci. 61: 449-456.
* Korf, R. P. 2009a. Indian sarcoscyphaceous fungi, by D. C. Pant and Vindeshwari Prasad. Mycotaxon 107: 511-512.
* Korf, R. P. & R. Dirig. 2009. Discomycetes Exsiccati - Fascicles 5 and 6. Mycotaxon 107: 25-34.
* Korf, R. P. 2008. Nomenclatural notes. 12. Untangling Hedwig's Octospora villosa: Helvella fibrosa comb. nov. Mycotaxon 103: 307-312.
* Abawi, G. S., G. W. Hudler, & R. P. Korf. 2007. In Memoriam William F. Mai 1916-2007. J.Nematol. 39:211-212.
* Korf, R. P. 2007d. Fungi of the protected landscape area of Vihorlat, by Sona Ripova, Slavomir Adamcik, Viktor Kucera, and Ladislav Palko. Mycotaxon 102: 437-438.
* Korf, R. P. 2007c. MycoKey 2.1, by Thomas Laessoe and Jens H. Petersen. Mycotaxon 102: 434-435.
* Korf, R. P. 2007b. On the genus Solenopezia (Fungi: Lachnaceae) and ICBN Art. 58-a sleeping dog bites back. Bol. Soc. Argent. Bot. 43: 29-32.
* Korf, R. P. 2007a. A tribute to Grégoire Laurent Hennebert and Mycotaxon's 100th volume. Mycotaxon 100: 1-4.
* Korf, R. P. 2005. Reinventing taxonomy: a curmudgeon's view of 250 years of fungal taxonomy, the crisis in biodiversity, and the pitfalls of the phylogenetic age. Mycotaxon 93: 407-415.
* Hodge, K. T., W. Gams, R. A. Samson, R. P. Korf, & K. A. Seifert. 2005. Lectotypification and status of Isaria Pers. : Fr. Taxon 54: 485-489.
* Gams, W., K. T. Hodge, R. A. Samson, R. P. Korf, & K. A. Seifert. 2005, (1684) Proposal to conserve the name Isaria (anamorphic fungi) with a conserved type. Taxon 54: 537.
* Gams, W, R. P. Korf, J. I. Pitt, D. L. Hawksworth, M. L. Berbee, & P. M. Kirk, edited by K. A. Seifert. 2003. Has dual nomenclature for fungi run its course? The Article 59 debate. Mycotaxon 88: 493-508.
* Korf, R. P. 2003. Mycotaxon gets a face-lift. Inoculum [suppl. to Mycologia] 56(6): 7.
* Lizon, P. & R. P. Korf. 2003. Editorial changes coming for Mycotaxon. Mycotaxon 86: 529-530
* Korf, R. P. 2002b. S. C. Teng, a Fitzpatrick student, a model taxonomist. Mycosystema 21: 473-474
* Korf, R. P. 2002a. Ainsworth & Bisby's Dictionary of the Fungi, by Paul M. Kirk, Paul F. Cannon, John C. David & Joost A. Stalpers, ed. 9. Mycotaxon 82: 475.
* Korf, R. P. 2001c. A cautionary tale: on publishing, pirating, copyright infringement, and slander. Inoculum [suppl. to Mycologia] 52(4): 26.
* Korf, R. P. 2001b. Molecules, Morphology and Classification: Towards monophyletic genera in the ascomycetes, edited by Keith A. Seifert, Walter Gams, Pedro W. Crous, and Gary J. Samuels. Mycotaxon 77: 499-500.
* Korf, R. P. 2001a. Amphithallic; Aneuploid; Apothecium; Ascocarp; Ascoconidium; Ascomycete; Ascospore;Ascus; Binucleate; Bulbil; Clamp Connection; Cleistothecium; Coenocytic; Dikaryotic; Diploid; Fungus; Haploid; Haustorium; Heterokaryosis;Heterothallic; Homokaryosis; Homothallic; Hypha; Hyphopodium; Hysterothecium; Mating Incompatibility; Mating Type; Microsclerotium; Monokaryotic; Multinucleate; Mycelial Fan; Mycelial Mat; Mycelium; Mycology; Nuclear Condition; Parasexual Cycle; Perithecium; Polyploid; Pseudohomothallic; Pseudothecium; Rhizomorph; Runner Hyphae; Sclerotium; Septate, in O. C. Maloy & T. D. Murray [eds.], Encyclopedia of Plant Pathology. 2 vols. New York: John Wiley & Sons.
* Korf, R. P. & P. Lizon. 2001. The status of the ordinal name Leotiales. Czech Mycol. 52: 255-257.(2000.)

== References ==
{{Reflist|30em}}

== External links ==
* [http://www.mycotaxon.com/grfx/elias.JPEG Photo of Korf in character as Elias Magnus Fries]
* [http://www.mycotaxon.com/pubsRPK.html Complete bibliography of Richard P. Korf]

{{authority control}}

{{DEFAULTSORT:Korf, Richard}}
[[Category:Articles created via the Article Wizard]]
[[Category:1925 births]]
[[Category:2016 deaths]]
[[Category:American mycologists]]
[[Category:Cornell University alumni]]
[[Category:Cornell University faculty]]
[[Category:People from Ithaca, New York]]
[[Category:People from Westchester County, New York]]
[[Category:People from New Fairfield, Connecticut]]