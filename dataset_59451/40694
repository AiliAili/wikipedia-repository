{{Infobox person
| honorific_prefix          = 
| name                      = Theodore Luqueer Mead
| honorific_suffix          = 
| native_name               = 
| native_name_lang          = 
| image                     = Mead Aged 22 Taken 1874.jpg
| image_size                = 
| alt                       = 
| caption                   = Mead as a young man, aged 22, taken in 1874
| birth_name                = 
| birth_date                =  February 23, 1852
| birth_place               = 
| death_date                = {{Death date and age|1936|05|04|1852|02|23}} <!--  (death date then birth date) -->
| death_place               = 
| death_cause               = 
| body_discovered           = 
| resting_place             = Greenwood cemetery, Orlando
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments                 = Mead Botanical Garden
| residence                 = 
| nationality               = American
| other_names               = 
| ethnicity                 = <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               = United States
| education                 = 
| alma_mater                = Cornell University
| occupation                = naturalist, entomologist and horticulturist
| years_active              = 
| employer                  = 
| organization              = 
| agent                     = 
| known_for                 = # ''Butterflies'' - Mead’s Sulphur, Mead’s Silverspot, Mead’s&nbsp;Wood-Nymph.
# ''Entomology'' - Discoverer of Florissant Fossil Beds.
# ''Horticulture'' - Orchids: Cattleya Meadii & Oviedo; Bromeliads: Billbergia Theodore L. Mead, xCryptbergia Mead; Caladium: Arrow and Lance Hybrids; Crinum: Kirkcape & Peachblow; Amaryllis: Mead-strain Hybrids; Daylily: Chrome Orange.
| notable_works             = 
| style                     = 
| influences                = 
| influenced                = 
| home_town                 = 
| spouse                    = Edith Katharine Antill Edwards
| children                  = Dorothy Luqueer Mead
| parents                   = Samuel H. and Mary C. Mead
| relatives                 = 
| callsign                  = 
| awards                    = 
| signature                 = 
| signature_alt             = 
| signature_size            = 
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 = 
| box_width                 = 
}}
'''Theodore Luqueer Mead''' (February 23, 1852 &ndash; May 4, 1936) was an important American naturalist, entomologist and horticulturist. As an entomologist he discovered more than 20 new species of North American butterflies and introduced the Florissant Fossil Beds in Colorado to the wider scientific world. As a horticulturist, he is best known for his pioneering work on the growing and cross-breeding of orchids, and the creation of new forms of caladium, bromeliad, crinum, amaryllis and hemerocallis (daylily). In addition he introduced many new semi-tropical plants, particularly palm varieties, into North America. Recently a comprehensive historical biography of his life and times has been published. <ref name="WikiMarkup">{{cite book|last=Butler|first=Paul|title=Orchids & Butterflies|year=2017|publisher=Little Red Hen Press|location=Winter Park, Florida|isbn=9780997966688}}</ref>

==Early life and schooling==
The Mead family was originally from England; his mother (née Luqueer), a descendant of Dutch Huguenot stock.<ref name=Mead>{{cite journal|last=Mead|first=Theodore L.|title=Theodore L. Mead - Naturalist, Entomologist and Plantsman. An Autobiography|journal=Yearbook, American Amaryllis Society|year=1935|volume=2|pages=11–22}}</ref> Mead was born at Fishkill, New York to Samuel H. Mead and Mary C. Mead and was the grandson of Ralph Mead, a well-known New York wholesale grocer,<ref>{{cite book|last=Barrett|first=Walter|title=The Old Merchants of New York City|year=1885|publisher=Thomas R. Knox and Co,|location=New York|pages=377|url=https://books.google.com/books/reader?id=AxIwAAAAIAAJ&printsec=frontcover&output=reader&pg=GBS.PP1}}</ref> who in 1838 had built and lived in the Second Avenue Manhattan house, now referred to as the [[Isaac T. Hopper House]]. Schooled in America and in Europe, where he learned French and German and studied the classics, he developed a deep interest in natural sciences from an early age that was strongly encouraged by his parents.

==Butterfly years==
His first interest was in butterflies, and as a youth he apprenticed under the guiding influence of [[William Henry Edwards]], author of the monumental five volume standard text at that time, “The Butterflies of North America”.<ref name=Edwards>{{cite book|last=Edwards|first=William H.|title=The Butterflies of North America, Series 1-5|year=1868–1897|publisher=Houghton, Mifflin & Company|location=Boston & New York}}</ref> In 1871, Edwards suggested that Mead, aged 19, take a trip to Colorado with the task of exploring and discovering new butterfly species in the Colorado Rockies.<ref>Calhoun, John V. 2010. The Discovery of Theodore L. Mead's Journal of 1871. News of the Lepidopterists' Society. 52:85-86. http://images.peabody.yale.edu/lepsoc/nls/2010s/2010/2010_v52_n3.pdf.</ref><ref>Calhoun, John V. 2013. The correct publication date of the ''Report Upon the Collections of Diurnal Lepidoptera Made in Portions of Colorado, Utah, New Mexico, and Arizona'' by Theodore L. Mead. News of the Lepidopterists' Society. 55:96-99. http://images.peabody.yale.edu/lepsoc/nls/2010s/2013/2013_v55_n3.pdf.</ref><ref>Calhoun, John V. 2015. An Updated Itinerary of Theodore L. Mead in Colorado in 1871, with Type Locality Clarifications and a Lectotype Designation for ''Melitaea eurytion'' Mead (Nymphalidae). Journal of the Lepidopterists' Society. 69:1-38. http://images.peabody.yale.edu/lepsoc/jls/2010s/2015/2015-69-1-001.pdf</ref><ref>Calhoun, John V. 2015. Assessing Specimen Provenance through the Writings of Theodore L. Mead, with Notes on his Specimens of Hesperia colorado(Hesperiidae). News of the Lepidopterists' Society. 57:176-181, 173. http://images.peabody.yale.edu/lepsoc/nls/2010s/2015/2015_v57_n4.pdf.</ref>

Over 20 species new to science were collected by Mead on this trip,<ref>{{cite journal|last=Brown|first=F. Martin|title=Colorado Butterflies|journal=Proceedings of Denver Museum of Natural History|year=1957|pages=4|url=http://www.dmns.org/media/371070/pseries2,v3.pdf|accessdate=10 October 2012}}</ref> and three named by Edwards in his honor still carry his name: ''Colias meadii'' (Mead’s Sulphur), ''Speyeria callippe meadii'' (Mead’s Silverspot) and ''Cercyonis meadii'' (Mead’s Wood-Nymph).<ref name=Pyle>{{cite book|last=Pyle|first=Robert Michael|title=National Audubon Society Field Guide to North American Butterflies|year=1981|publisher=Alfred A. Knopf|location=New York|isbn=0394519140|edition=A Chanticleer Press|author2=Opper, Jane }}</ref>

Mead’s butterfly knowledge and expertise grew to such an extent that he was given the job of collating the butterfly discoveries from all the [[Wheeler Survey]] expeditions in Colorado, Utah, New Mexico and Arizona from 1871 to 1874 and submitting the results as part of the final report to the United States Government.<ref>{{cite journal|last=Mead|first=Theodore L.|title=Report upon the collections of diurnal Lepidoptera made in portions of Colorado, Utah, New Mexico, and Arizona during the years 1871, 1872, 1873, and 1874|journal=Reports of the Wheeler Survey, United States Geographical Surveys west of the 100th meridian|year=1875|volume=5 - Zoology|pages=739–794|publisher=Government Printing Office|location=Washington}}</ref>

==Discovery of Florissant fossil beds==
At the end of his collecting time in Colorado, in September 1871, he heard tell of a strange petrified forest and rock formation at Florissant and went there on horseback to investigate.<ref name=Brown>{{cite book|last=Brown|first=edited by Grace H. Brown ; annotated by F. Martin|title=Chasing butterflies in the Colorado Rockies with Theodore Mead in 1871 : as told through his letters|year=1996|publisher=Pikes Peak Research Station, Colorado Outdoor Education Center|location=Florissant, CO|isbn=0910715106|pages=62}}</ref> Realizing the scientific importance of the site, he collected 25&nbsp;lbs. of compressive shale fossil rocks containing insects and leaves<ref name=Brown />  and sent these via Edwards to [[Samuel Hubbard Scudder]], a Harvard University paleontologist, whose publications and lectures after analyzing the fossils alerted the rest of the scientific community to this important site,<ref>{{cite book |last1=Veatch |first1=Steven |last2=Meyer |first2=Herbert |editor1-last=Meyer |editor1-first=Herbert |editor2-last=Smith|editor2-first=Dena|title=Paleontology of the Upper Eocene Florissant Formation, Colorado |publisher=The Geological Society of America |year=2008 |page=2 |chapter=Chapter 1: History of paleontology at the Florissant fossil beds, Colorado |isbn=9780813724355 }}</ref> now the [[Florissant Fossil Beds National Monument]].

==University years==
Mead attended Cornell University as a freshman in 1874 and graduated in Civil Engineering in 1877. In this year he sold his extensive butterfly collection to the [[Carnegie Museum of Natural History]] in Pittsburgh, and turned his attention to horticulture. He continued to work at Cornell after graduation doing research in natural history, which quickly became his passion. Mead was active in the [[Alpha Delta Phi]] fraternity at Cornell and a major driver in the construction of the first dedicated chapter house for the fraternity.<ref>{{cite web|last=Zawel|first=Marc B.|title=A comprehensive history of Alpha Delta Phi|url=http://www.adphicornell.org/public6.asp|accessdate=10 October 2012}}</ref> He counted fellow student and fraternity brother [[Louis Agassiz Fuertes]], the famous bird artist, among his closest friends.<ref>Mead, Theodore L. (1935). "Theodore L. Mead - Naturalist, Entomologist and Plantsman. An Autobiography". Yearbook, American Amaryllis Society 2: 11–22</ref>

Still uncertain as to his future vocation, in 1878 Mead and his parents embarked on a six-month long entomological and nature trip to California and the Western States, travelling by steamer from New York via Panama and up the coast to San Francisco, returning via Salt Lake City and Chicago. [[Epiphytes]], cacti and several new species of butterflies were collected including one he named, ''Gaeides editha'' (Edith’s Copper),<ref name=Pyle /> after Edward’s eldest daughter Edith, whom he later married in 1882.

==Horticulture in Florida==
Mead had first visited Florida in 1869 on a butterfly collecting trip, where he had successfully captured his rarest specimen near Enterprise - a female of ''Papilio calverleyi''; only one other specimen of that type had ever been found before.<ref name=Mead /> He considered the climate there ideal for the growing of semi-tropical plants, so after marriage he moved to [[Eustis, Florida]] where his father had bought him an orange grove and land to develop and grow other plants. Income would come from citrus and the growing of other cash crops such as pineapples, leaving time for his experiments in horticulture.
In 1886 he purchased eighty-five acres in [[Oviedo, Florida]], close to Lake Charm where orange grove land was more fertile, choosing a location next to Edith’s aunt Mary. She had previously married Dr. Henry Foster owner of the [[Clifton Springs Sanitarium]] in New York State, and the couple were winter visitors to the Lake Charm area.

At Lake Charm Mead grew many palms from seed and hybridized [[orchids]], [[bromeliads]], [[crinum]] and later, [[caladium]], [[amaryllis]] and [[daylilies]]. He established a strong friendship with [[Henry Nehrling]] of [[Gotha, Florida]] with whom he collaborated on many plant experiments.

Citrus in central Florida did well until the [[Great Freeze]] of 1894-5. Many growers abandoned their groves entirely, but Mead recognized that below ground artesian water at a constant 70 degrees might provide relief from frost and allow citrus trees to survive freezing temperatures. His engineering background led him to postulate that overhead water irrigation of the trees hanging with fruit could keep the fruit protected from damage at 32° within an ice cocoon regardless of how low the mercury fell. He successfully covered an acre of oranges, installed a pump and irrigation system, and proved the concept, the first known description<ref>{{cite journal|last=Mead|first=Theodore L.|title=A Frost-Proof Orange Orchard|journal=Country Life in America|date=February 1905|volume=VII|pages=367–369 and 385–386}}</ref> of a technique still used today.

==Cross-breeding experiments and achievements==
Mead’s approach to hybridization was to strive to create new types of plants of sufficient novelty or improved characteristics to make them worthy of commercial introduction, whether or not the hybridization was difficult, as for orchids, or easy, as for daylilies. In this work, he applied a rigorous scientific approach coupled with meticulous record-keeping. He did not seek publicity for his efforts, and according to [[Henry Nehrling]],<ref>{{cite book|title=Nehrling's Early Florida Gardens|year=2001|publisher=University Press of Florida|location=Gainesville|isbn=0813024250|pages=72|author=Read, edited by Robert W.}}</ref> was a more accomplished hybridizer of plants than [[Luther Burbank]].

===Orchids===
At that time orchid seed germination was traditionally a hit or miss affair, and the single biggest factor preventing orchid growing from moving from being a rich-man’s hobby to a business where money could be made. Irregular and irreproducible germination seriously hampered the speed and success rate of Mead’s hybridization efforts, and it was not uncommon for him to have to wait 10 or more years before a new orchid bloomed for the first time, in one instance taking 17 years.<ref>{{cite journal|last=Grover|first=Edwin Osgood|title=Theodore Luqueer Mead|journal=The Orchid Journal|date=April 1953|pages=159–163}}</ref> But Mead was a patient man, employing scientific principles and keeping detailed and careful notes. His horticultural notebook in the ''Michael A. Spencer Collection on Theodore Mead, Special Collections and University Archives'', [[University of Central Florida]], lists several thousand crosses as well as information on all his other plants. In addition, he photographed many of his new creations.

[[File:MeadSingapore1991Stamp.jpg|thumb|Cattleya Meadii was chosen to honor Mead on the 1991 30c Singapore stamp]]He was a frequent contributor to ''[[The Orchid Review]]'', a journal of the [[Royal Horticultural Society]] who also maintain ''The International Orchid Register'', and wrote about a number of crosses to the journal. In 1904 he reported<ref>{{cite journal|last=Mead|first=Theodore L.|title=Orchid Hybridising in Florida|journal=The Orchid Review|date=March 1904|volume=12|pages=72}}</ref> ''Cattleya bowringiana x Cattleya forbesii'' and ''Cattleya maxima x Cattleya schilleriana'' as germinating in January 1894 and blooming in January 1904. These two hybrids were added to the register in 1904 as officially originated by Mead, and named in recognition ''Cattleya Meadii'' and ''Cattleya Oviedo'', respectively. The Singapore 30c stamp from 1991 carries an illustration of ''Cattleya Meadii''.

Mead’s biggest contribution to orchidology, after over 25 years of orchid growing, came when Lewis Knudson of Cornell University approached him for advice and assistance in experiments he was performing related to the possible non-symbiotic germination of orchid seed. By then Mead had realized that germination rates could be made more favorable in a sterile and temperature-controlled environment<ref>{{cite journal|last=Rolfe|first=Robert Allen|title=Orchid Hydridising in Florida|journal=The Orchid Review|date=June 1905|volume=13|pages=162}}</ref> akin to clean room conditions in a laboratory. Knudson’s breakthrough involved the use of sterilized agar containing nutrients to achieve reproducible orchid seed germination.<ref>{{cite journal|last=Knudson|first=Lewis|title=Nonsymbiotic Germination of Orchid Seeds|journal=Botanical Gazette|year=1922|volume=73|pages=1–25|jstor=2470045|doi=10.1086/332956}}</ref>
It has been stated<ref>{{cite book |last1=Yam |first1=TW |last2=Nair |first2=H |last3=Hew |first3=CS |last4=Arditti |first4=J |editor1-last=Kull |editor1-first=T |editor2-last=Arditti |editor2-first=J |title=Orchid Biology: Reviews and Perspectives |publisher=Kluwer |location=Dordrecht |year=2002 |page=483 |chapter=Orchid seeds and their germination: An historical account |isbn=1402005806}}</ref> as being doubtful whether Knudson would have been successful without Mead’s inputs and donation of viable ripe orchid seeds, so arguably he should have been credited as a co-discoverer. Improved versions of Knudson’s solution are used today for orchid seed germination,<ref>{{cite book |last1=Griesbach |first1=RJ |editor1-last=Janick |editor1-first=J |editor2-last=Whipkey |editor2-first=A |title=Trends in New Crops and New Uses |publisher=ASHS Press |location=Alexandria, VA |year=2002 |page=459 |chapter=Development of ''Phalaenopsis'' orchids for the mass-market}}</ref> and the accomplishment was the catalyst for the emergence of the orchid industry as we know it today.

===Bromeliads===
In the 1920s he was the first American to hybridize bromeliads<ref>{{cite journal|last=Padilla|first=Victoria|title=Bromeliads in American Horticulture|journal=BSI Journal|date=January–February 1985|volume=35|issue=1|pages=5|url=http://journal.bsi.org/V35/1/|accessdate=10 October 2012}}</ref> and introduced many new representatives of several bromeliad genera, such as Aechmea, Ananas, Billbergia, Crypthansus, Guzmania, Hohenbergia, Nidalarium, and Tillandsia.<ref name=Mead /> He hybridized the billbergia genera extensively and sent his hybrids to the [[Brooklyn Botanic Garden]], to Nehrling and to many other growers. For many years Mead exchanged seeds and plants with E.O. Orpet and he introduced into California ''Billbergia xmeadii'' (a cross between ''Billbergia nutans'' and ''Billbergia porteana'', sometimes also referred to as [[Billbergia 'Theodore L. Mead']]), which became one of the favorite billbergias of western growers.<ref>{{cite journal|last=Padilla|first=Victoria|title=Pineapples in the Pit|journal=BSI Journal|date=May–June 1956|volume=6|issue=3|url=http://journal.bsi.org/V06/3/|accessdate=10 October 2012}}</ref>
Mead became the first person ever to create a bromeliad intergeneric cross,<ref>{{cite journal|last=Foster|first=Mulford B.|title=Earth Stars among the Bromeliads|journal=BSI Journal|date=November–December 1952|volume=2|issue=6|url=http://journal.bsi.org/V02/6/|accessdate=10 October 2012}}</ref> selecting ''cryptanthus beuckeri'' and crossing it with pollen from one of his favourites, ''billbergia nutans'', to produce the first cryptbergia, [[xCryptbergia Mead]].

===Crinum===
Mead was always fascinated by bulbous plants and the magnificence of floral displays from their cut flowers. Sometime around 1890, he obtained a large and extensive collection of around 80 [[crinum]] species from an English collector in India<ref name=Mead /> and set about hybridizing them, producing the hybrids ''Crinum Kircape''<ref>{{cite journal|last=Orpet|first=Edward O.|title=Three Good Plants|journal=Garden and Forest|date=July 1895|volume=8|issue=386|pages=288|url=https://play.google.com/books/reader?id=QfbmAAAAMAAJ&printsec=frontcover&output=reader&authuser=0&hl=en&pg=GBS.PA288|accessdate=11 October 2012}}</ref>  around 1894, a cross of ''C. Kirkii'' a species from Zanzibar, and ''C. Capense'' from South Africa, and ''Peachblow''<ref name=Ogden>{{cite book|last=Ogden|first=Scott|title=Garden bulbs for the South|year=1994|publisher=Taylor Pub.|location=Dallas, Tex.|isbn=9780878338610|pages=146 and 206}}</ref>  around 1900, a plant with tall stems and huge, pink-white, scented blooms.

===Caladium===
Both Mead and Nehrling hybridized [[caladiums]] and created dozens of new and highly colored fancy-leaved cultivars; Nehrling growing them commercially in their tens of thousands.<ref>{{cite book|title=Nehrling's plants, people, and places in early Florida|year=2001|publisher=University Press of Florida|location=Gainesville|isbn=0813024285|pages=6|author=Read, edited by Robert W.}}</ref> Mead favored hybridizing the unusual and in 1920 he crossed the narrow-leaved species, ''C. albanense'', ''C. speciosum'' and ''C. venosum'', with the standard caladium varieties to create the “arrow and lance” type caladium,<ref name=Mead /><ref name=Ogden /> bringing intriguing narrow strap-leaves and dwarf growth habits to a race of caladium that still possessed the high coloration and patterns of the larger fancy-leaved forms.

===Amaryllis===
With orchid crossing coming to an end, Mead devoted more and more attention of the breeding of [[amaryllis]] (hippeastrum), being greatly helped by Nehrling,<ref>{{cite journal|last=Hayward|first=Wyndham|title=Bulbous Plants Adopted to Florida|journal=Proceedings of Florida State Horticultural Society|year=1947|volume=60|url=http://www.fshs.org/Proceedings/Password%20Protected/1947%20Vol.%2060/205-211(HAYWARD).pdf|accessdate=11 October 2012}}</ref> who had a fine collection and allowed Mead to take what he wanted in the way of pollen for his experiments. Specimens with just a hair line color around each petal were intercrossed to produce nearly white varieties with a narrow border around each petal.<ref>{{cite journal|last=Eastman|first=Cecil|title=Southern Personalities - Theodore L. Mead, Grower of Amaryllis and Orchids|journal=Holland's: The Magazine of the South|date=July 1931|pages=7 and 42–43}}</ref> His hybrid red and white striped ‘Mead-strain’ amaryllis also became the bulb of choice for many American southern gardens in the 1940s and 1950s.<ref>{{cite web|last=Pertuit|first=A. J.|title=Understanding and Producing Amaryllis|url=http://www.clemson.edu/psapublishing/pages/HORT/HORTLF63.PDF|publisher=Clemson University Extension Service|accessdate=11 October 2012}}</ref>

The American Amaryllis Society’s yearbook for 1935 was dedicated to Mead in recognition of his pioneer work with hybrid amaryllis   plants, and in 1937 the [[International Bulb Society]] awarded Mead [[The Herbert Medal]] for his contributions in advancing the knowledge of bulbous plants.

===Hemerocallis (Daylily)===
Mead was an early hybridizer of the [[daylily]] and many crosses were made during the 20s and 30s. Only one however, “Chrome Orange” registered in 1933, did he consider sufficiently novel and of outstanding color for commercial introduction.<ref>{{cite journal|last=Hayward|first=Wyndham|title=The Daylily in Florida|journal=Proceedings of Florida State Horticultural Society|year=1950|volume=63|pages=195|url=http://www.fshs.org|accessdate=11 October 2012}}</ref>
[[File:Mead Family Grave Orlando 2012.jpg|thumb|The Mead family grave in Greenwood cemetery, Orlando, lies beneath a tall pine tree where eagles nest each year]]

==Personal life==

Mead married Edith Katharine Antill Edwards at Coalburg, West Virginia in 1882. Their only child, Dorothy Luqueer Mead, was lost to scarlet fever, aged 4, in 1892.

Mead’s eldest brother, Samuel H. Mead, Jr, died of an accidental self-inflicted gunshot wound to the head in New York City in 1875, aged 27.<ref>{{cite news |work=New York Daily Tribune |title=Fatal Accident to an Inventor |date=21 May 1875 |url=http://fultonhistory.com/Newspapers%206/New%20York%20NY%20Tribune/New%20York%20NY%20Tribune%201875%20Apr%20-%20Jun%20Grayscale/New%20York%20NY%20Tribune%201875%20Apr%20-%20Jun%20Grayscale%20-%200525.pdf}}</ref>

Theodore Mead died on May 4, 1936 and is buried in Greenwood cemetery, Orlando.

[[Mead Botanical Garden]], opened in [[Winter Park, Florida]] in 1940, is dedicated to his memory.

==References==
{{Reflist|2}}

==External links==
*[http://asp3.rollins.edu/olin/oldsite/archives/mead.htm ''Archives and Special Collections'', Rollins College, Winter Park, Florida; A Guide to the Theodore L. Mead Collection]
*[http://www.wppl.org/wphistory/HelenConnery/index.html Winter Park History and Archives Collection, Winter Park Library, Winter Park, Florida; Helen S. Connery Collection]
*[http://library.ucf.edu/SpecialCollections/FindingAids/Mead_SpencerCollection.xml Special Collections and University Archives, University of Central Florida Libraries, Orlando, Florida; Guide to the Michael A. Spencer Collection on Theodore Mead, 1887-1939]
*[http://www.meadgarden.org Mead Botanical Garden]

{{DEFAULTSORT:Mead, Theodore Luqueer}}
[[Category:American horticulturists]]
[[Category:Citrus farmers]]
[[Category:Orchid cultivation]]
[[Category:American entomologists]]
[[Category:Lepidoptera of North America]]
[[Category:Cornell University College of Engineering alumni]]
[[Category:1852 births]]
[[Category:1936 deaths]]