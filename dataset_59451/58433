{{Orphan|date=June 2015}}

{{Italic title}}

The '''''Journal of Prisoners on Prisons''''' is a [[peer review|peer-reviewed]] [[academic journal]], which gives a voice to [[prison]]ers. Using collections of [[essay]]s, each issue brings to light new ideas, emotions, and descriptions of life inside minimum-to-maximum security institutions. The journal seeks to promote thought on why much of what is deemed criminal (e.g. illicit drug use) is deemed criminal, impacts on community members, including and up to the offender.

The journal's stated aim is "to acknowledge the accounts, experiences, and criticisms of the criminalized by providing an educational forum that allows women and men to participate in the development of research that concerns them directly."<ref>{{cite web |url=http://www.jpp.org/index.html |title=General information |work=Journal of Prisoners on Prisons |accessdate=2010-12-20}}</ref> Furthermore, it attempts to affect policy shifts, and expose stereotypes, laws and policies it deems reactionary (e.g. [[Sex offender registries in the United States]]), and the business of crime.

The journal is published by the [[University of Ottawa Press]] and was established in 1988. It was born out of the 3rd [[ICOPA]] Conference in Montreal, 1987. Related to the [[Prison abolition movement]], the journal supports the rights of prisoners inside, by allowing those outside to understand what really happens in a prison.

== See also ==
* [[Prison abolition movement]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.jpp.org}}

[[Category:Imprisonment and detention]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Sociology journals]]
[[Category:Publications established in 1988]]
[[Category:Prison abolition movement]]