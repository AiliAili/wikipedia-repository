<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= Mü22
 | image=Akaflieg Mue 22b.jpg
 | caption= The Mü22b
}}{{Infobox Aircraft Type
 | type= High Performance Glider
 | national origin=[[Germany]]
 | manufacturer= [[Akaflieg München]]
 | designer=   [[Dipl. Ing. J Kraus]]<ref name = "Mü22 Simons">Simons, Martin. ''Sailplanes 1945-1965”. 2nd revised edition. EQIP Werbung und Verlag G.m.b.H.. Königswinter. 2006. ISBN 3-9807977-4-0</ref>
 | first flight= Mü22 – 1954,<ref name="Mü22a">[http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=140&Itemid=155 Mü22a]</ref>  Mü22b – 13 March 1964<ref name = "Mü22b">[http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=90&Itemid=121 Mü22b]</ref>
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=  Mü22 – 1,   Mü22b - 1
 | developed from=
 | variants with their own articles=  
}}
|}
The '''Akaflieg München Mü22''' is a single-seat research glider designed and built in [[Germany]] from 1953<ref name="Mü22a"/>

== Development ==

=== Mü22(a) ===
The first aircraft built by Akaflieg München after [[World War II]] was the Mü22, with typical 'Munich School' construction of steel tube fuselage with wooden wings and tail-unit. Design of this high performance glider with 30% [[laminar flow]] forward swept wings and vee tail started in 1953 with construction complete in late 1954 at the Akaflieg München workshops at [[Prien am Chiemsee|Prien]].

Use of the laminar flow [[NACA airfoil|NACA 63 (3)-618]] aerofoil section imbued the Mü22 with a good glide ratio at a high speed which was ideal for competition cross-country flight, in contrast to the low speed [[Mü Scheibe]] section used for previous sailplanes at Akaflieg München. To maintain strength with reduced weight and to eliminate turbulence from gaps and protuberances the Mü22 was built without airbrakes, relying on the use of the 70 degree trailing edge split flaps and the skill of the pilot in side-slipping for approach control. The high performance of the design was demonstrated during flight testing after the first flight on 29 November 1954 at [[Riem]], as well as by placing second in the German National Gliding Championships in 1959. The Mü22 was also flown extensively to investigate mountain flying during which the Mü22 suffered a serious accident late in 1959 and was damaged beyond repair.<ref name="Mü22a"/>

[[File:Akaflieg Mue22a Flaeche.jpg|thumb|right|The Mü22 wings under construction illustrating the closely spaced ribs]]
Note: Originally the Mü22 was not given the 'a' suffix. This suffix was applied after the  Mü22b was built to differentiate between the two aircraft.

=== Mü22b ===
The second Mü22, given the suffix 'b', followed the design principles of the original crashed version but introduced an all-flying vee-tail and a smoother fibreglass covered forward fuselage to reduce drag. Construction of the laminar flow wings, using plywood skin over closely spaced ribs, gives an accurate profile which has remained true since manufacture. Flight testing commenced in 1964 but was marred, on 5 June 1964, by the failure of a bolt attaching a bearing in the tail unit, the tail fluttered dramatically breaking the tail unit off the fuselage. The pilot, Dipl.-Ing. [[Frodo Hadwich]], bailed out safely and the glider impacted relatively softly enabling repairs to be carried out.<ref name = "Mü22 Simons"/>

The relatively high performance of the Mü22b makes the aircraft popular for cross-country tasks but the landing characteristics, without airbrakes, require practice for smooth landings. The Mü22b continued to fly at the  Akaflieg München gliding club, until at least 2002, and has also competed in recent competitions, flown by [[Heiko Hertrich]] in 1986, completing a 586&nbsp;km triangle for the [[Barron Hilton Cup]], as well as completing a 650&nbsp;km triangle flown by [[Jan Skedelj]].<ref name = "Mü22b"/>
<!-- ==Operational history== -->

== Variants ==
* ''' Akaflieg München Mü22(a)'''  – The original aircraft with fixed vee-tail and fabric covered steel tube fuselage.
* ''' Akaflieg München Mü22b''' – The second aircraft, built to replace the crashed Mü22 with fibreglass covered fuselage and all-flying vee-tail.
<!-- ==Operators (choose)== -->

==Specifications (Mü22b)==
{{aerospecs
|ref=Mü22b<ref name="Mü22b"/>
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew=1
|capacity=
|length m=
|length ft=
|length in=
|span m= 17
|span ft= 55
|span in= 9
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m= 
|height ft= 
|height in= 
|wing area sqm=  13.7
|wing area sqft=  147.5
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes--> 21.1
|wing profile=<!-- sailplane --> [[NACA airfoil|NACA 63 (3)-618]]
|empty weight kg= 280
|empty weight lb= 617
|gross weight kg= 360
|gross weight lb= 794
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number= 
|eng1 type=  
|eng1 kw=<!-- prop engines --> 
|eng1 hp=<!-- prop engines --> 
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
 
|max speed kmh= 
|max speed mph= 
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown --> 
|cruise speed mph=<!-- if max speed unknown --> 
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km= 
|range miles= 
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m= 
|ceiling ft= 
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes --> 36 @ 80 km / h (50mph)
|climb rate ms= 
|climb rate ftmin= 
|sink rate ms=<!-- sailplanes --> @ 69 km / h (43mph) 0.56
|sink rate ftmin=<!-- sailplanes --> 110
|armament1= 
|armament2= 
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
* [[SZD-22 Mucha Standard]]
* [[Standard Austria]]
* [[Schemp-Hirth SHK]]
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=
*Simons, Martin. ''Sailplanes 1945-1965”. 2nd revised edition. EQIP Werbung und Verlag G.m.b.H.. Königswinter. 2006. ISBN 3-9807977-4-0
*[http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=140&Itemid=155]
*[http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=90&Itemid=121]

}}
{{refbegin}}
{{refend}}

==External links==
* [http://www.sailplanedirectory.com/munchen.htm]
{{Akaflieg München aircraft}}

{{DEFAULTSORT:Akaflieg Munchen Mu22}}
[[Category:German sailplanes 1950–1959]]
[[Category:Akaflieg München aircraft|Mu22]]