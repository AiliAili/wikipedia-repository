'''Lee + Mundwiler Architects''' (a.k.a. leeMundwiler) is an architectural design studio based in [[Los Angeles]], California and [[Basel]], Switzerland.<ref>http://lm-arch.com/</ref> Made up of founding architects [[Cara Lee]] and Stephan Mundwiler, the studio is known for its clean, modern, and sustainable design<ref>http://www.dwell.com/green/article/l-longevity</ref> along with futuristic concepts and theoretical industrial design.

== Background ==

Before establishing offices in Los Angeles and Basel, the two principals met while pursuing [[Master's Degree]]s at the architectural institute [[Southern California Institute of Architecture|SCI-Arc]] at the international branch in [[Vico Morcote]], [[Switzerland]]. Lee is originally from Korea, Mundwiler is originally from Switzerland. They have been married since 1996.

== Career ==

The studio has been the recipients of a number of prestigious regional and national [[American Institute of Architects]] (AIA) Awards for both residential and commercial projects and for "Emerging Practice of the Year".<ref>http://www.aialosangeles.org/home-page-latest-news/aia-la-counts-down-to-the-2011-design-awards-ceremony-and-party-presented-by-italian-living-umbria-3#.UajrZciWXAE</ref>

Lee + Mundwiler award-winning "Coconut House"<ref>http://www.aialaarchive.org/about/archives/member_news/nationalawardwinners.html</ref> that "tweaks iconic forms to deliver views without losing privacy." <ref>http://www.treehugger.com/culture/this-month-in-architectural-record-july.html</ref> was photographed by legendary architectural photographer [[Julius Shulman]] with the assistance of photographer [[Juergen Nogai]] in 2006.<ref>http://www.sciarcalumni.org/coconut-house-by-cara-lee-m-arch-96/</ref> Lee and Mundwiler remained close friends with Shulman up until his death in 2009.

== Notable Projects and Awards ==
'''Dapeng Geology Museum and Research Center, Shenzhen, China'''
* 2014 WAN Awards Winner for Civic Buildings<ref>http://backstage.worldarchitecturenews.com/wanawards/project/dapeng-geology-museum-and-research-center/?source=categorywinners&mode=gallery&selection=winner</ref>

'''2011 AIA Emerging Practice Award'''<ref>http://www.sciarc.edu/news_archive.php?id=1970</ref>

'''[[Bundesplatz (Bern)|Bundesplatz]] - Government plaza in Bern, Switzerland'''

* 1992 AIA Competition Winner
* 2006 AIA National Honor Award for Urban Design
* 2006 AIA California Chapter Merit Award for Urban Design
* 2006 Nomination for 10 Best Projects in Switzerland<ref>http://www.sciarcalumni.org/bundesplatz-by-cara-lee-m-arch-%C2%B496/</ref>

'''The Coconut House<ref>http://www.sciarcalumni.org/coconut-house-by-cara-lee-m-arch-96/</ref>'''

* 2006 AIA National Housing Award<ref>http://www.sciarcalumni.org/coconut-house-by-cara-lee-m-arch-96/</ref>

'''The Central Park of the New Radiant City, Guangming New Town, China'''

* 2009 Institute Honor Awards for Regional and Urban Design<ref>http://info.aia.org/aiarchitect/thisweek09/0109/0109n_hareg.cfm</ref>

'''Swiss Pavilion [[2010 Expo]]'''

* 3rd Place in international open competition<ref>http://www.sciarc.edu/news_archive.php?id=1063</ref>

== Unique Design Concepts ==

=== Breathing Buildings ===

The core feature of Lee + Mundwiler's Swiss Pavilion project is its light and temperature-responsive exterior. Consisting of hundreds of red doors, the doors open and shut to maintain a consistent interior temperature and shade as sunlight moves. The purpose of the design is to simulate the way a living organism's skin, or a living cell would respond to environmental stimuli, while creating an energy-efficient and visually stunning solution to climate-control.<ref>https://www.youtube.com/watch?v=J1MsIXFqvWA</ref> Lee + Mundwiler's "Breathing Building" concept has been applied to several other theoretical projects.<ref>http://lm-arch.com/projects.php?id=148</ref>

=== "Ray of Hope" Iraqi Memorial ===

Consisting of a single ray of light projected from an orbiting satellite on a pre-determined path, the conceptually abstract "Ray of Hope" Iraqi memorial was designed to be seen all over the world, while having a practical use of supporting communications infrastructures in Iraq and other countries in need.<ref>http://www.iraqimemorial.org/arabic_proposals_list.php?last=Mundwiler&first=Stephan</ref>

== References ==

{{reflist}}
http://lm-arch.com/
http://lm-arch.com/projects.php?id=106
http://www.dwell.com/green/article/l-longevity
http://www.aialosangeles.org/home-page-latest-news/aia-la-counts-down-to-the-2011-design-awards-ceremony-and-party-presented-by-italian-living-umbria-3#.UajrZciWXAE
http://www.aialaarchive.org/about/archives/member_news/nationalawardwinners.html

[[Category:Architecture firms based in California]]
[[Category:Companies based in Los Angeles]]