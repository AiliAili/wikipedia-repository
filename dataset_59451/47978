{{Other uses|Frank Smith (disambiguation){{!}}Frank Smith}}
{{Infobox writer
|name = Frank Kingston Smith, Sr.
<!-- Commented out because image was deleted: |image =FrankKingstonSmithSenior.jpg -->
|image_size =
|caption = Frank Kingston Smith, Sr.
|pseudonym =
|birth_name = 
|birth_date = {{Birth date|1919|9|15}}
|birth_place = 
|death_date = {{Death date and age|mf=yes|2003|9|03|1919|9|15}}
|death_place =
|medium = author
|active = 1957–2003
|spouse =Marianne Hiller
|children ={{plainlist|
* [[Frank Kingston Smith]]
* Doug
* Greg
}} 
|notable_work = ''Week-end Pilot''
}}
'''Frank Kingston Smith, Sr.''' was a criminal attorney in Philadelphia, Pennsylvania, but is best known as the author of several books and articles on aviation. His first book, ''Week-end Pilot'', helped to bring the joys of flying to the attention of the general public.<ref>Collins, Michael. [http://flighttraining.aopa.org/magazine/2005/January/200501_Commentary_Preflight.html "Meet Bill Kershner"], ''[[AOPA Preflight]]'', Web, January 2005. Retrieved on 25 July 2014.</ref>  His eldest son, [[Frank Kingston Smith]] is a well-known radio personality and is an aviation enthusiast himself.

==Career==

===Early career===
After serving in World War II in naval intelligence, Smith graduated from law school and became a criminal trial attorney and, subsequently, a partner in a Philadelphia law firm.  Smith used the joys of flying as a way to relax from the stresses of his job.  Although Smith gave up law in 1965 to become the executive director of the National Aviation Trades Association (NATA), he returned to law ten years later specializing in aviation matters for a Washington, D.C. law firm.<ref>Aero News Network. [http://www.aero-news.net/index.cfm?do=main.textpost&id=b327e243-442a-43ad-bcb2-83bd9b950853 "Frank Kingston Smith's Ashes Spread at Wings Field in Private Ceremony"], ''[[Aero News Network]]'', Web, 9 June 2005. Retrieved on 25 July 2014.</ref>

===Pilot===
His first airplane lesson was on June 2, 1955<ref>Aero News Network. [http://www.aero-news.net/subscribe.cfm?do=main.textpost&id=b327e243-442a-43ad-bcb2-83bd9b950853 "Frank Kingston Smith's Ashes Spread at Wings Field in Private Ceremony"], ''[[Aero News Network]]'', Web, 9 June 2005. Retrieved on 25 July 2014.</ref> at Wings Field located in suburban Northeast Philadelphia. Smith was an instrument as well as multi-engine rated pilot with over 10,000 hours of flight time.  He finally gave up flying in 1995.

===Author===
Smith was the author of several books and articles on general aviation.  His first book, ''Week-end Pilot'', was published in 1957.  Throughout the 1960s and 1970s, Smith wrote a column for ''Flying Magazine'' as well as articles for ''AOPA Pilot'' and ''Sports Aviation''.

As a writer, his output was prolific, writing a total of 16 books and well over 1,000 articles.<ref>AOPA News. [http://www.aopa.org/News-and-Video/All-News/2003/March/9/Longtime-aviation-author-Frank-Kingston-Smith-dies "Longtime aviation author Frank Kingston Smith dies"], ''[[AOPA News]]'', Web, March 2005. Retrieved on 13 November 2014.</ref>

==Personal life and Death==
Smith is survived by three sons, Frank, Doug and Greg, all of whom are pilots.  He was married to Marianne (née Hiller) until his death in 2003 of [[Alzheimer's disease]]. At his request, Smith's ashes were spread over Wings Field exactly 50 years to the day after he had his first airplane lesson.

===Lawyer-Pilots Bar Association===
In 1959, Smith was one of nine lawyers to sign the articles of incorporation to create the [http://www.lpba.org/ Aviation Lawyer-Pilots Bar Association], a New Jersey non-profit corporation dedicated to bringing together lawyers who also happen to be pilots. This association still survives today.

==Selected Works==

===Novels (selection)===
* ''Week-end Pilot''. Random House, 1957
* ''Flights of Fancy''. Random House, 1960
* ''I'd Rather Be Flying!''. Random House, 1962
* ''Computer Guide (Modern Aircraft Series)''. Sports Car Press, 1962
* ''Private pilot's survival manual''. TAB Books, 1979
* ''How to Take Great Photos from Airplanes''. McGraw-Hill, 1979
* ''Aviation and Pennsylvania'' (with James P Harrington). Franklin Institute Press, 1981
* ''Weekend Wings''. Random House, 1982
* ''Legacy of Wings: The Story of Harold F. Pitcairn''. Jason Aronson/T.D. Associates, 1982
* ''Flying the Bahamas: The weekend pilot's guide''. TAB Books, 1983

==Awards==
* Max Karant Lifetime Achievement Award, AOPA (1997)
* Elder Statesman of Aviation Award, National Aeronautic Association (1999)

==References==
{{Reflist}}

==External links==
* [http://www.lpba.org/ Aviation Lawyer-Pilots Bar Association]
* [http://www.aopa.org/ Aircraft Owners and Pilots Association]

{{DEFAULTSORT:Smith, Frank Kingston, Sr.}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:1919 births]]
[[Category:2003 deaths]]
[[Category:American naval personnel of World War II]]
[[Category:20th-century American writers]]
[[Category:Deaths from Alzheimer's disease]]