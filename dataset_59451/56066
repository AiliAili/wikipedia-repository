{{italic title}}

The '''''Cahiers Élisabéthains''''' is an international, peer-reviewed [[academic journal]] founded in 1972, which specializes in publishing research on the [[England|English]] [[literature|literary]] [[Renaissance]]. The journal is published by the Institut de Recherche sur la Renaissance, l'Âge Classique et les Lumières (UMR 5186, a joint research unit of the [[French National Centre for Scientific Research]] and [http://www.univ-montp3.fr Paul Valéry University, Montpellier 3]).<ref>{{cite web|title=Home page|url=http://www.ircl.cnrs.fr|accessdate=1 February 2016}}</ref> {{As of|2015}}, the editors-in-chief are Florence March, Jean-Christophe Mayer, Nathalie Vienne-Guerrin and Peter J. Smith.<ref>{{cite web|title=Cahiers homepage|url=https://uk.sagepub.com/en-gb/eur/cahiers-%C3%A9lisab%C3%A9thains/journal202512|website=SAGE Publications|accessdate=1 February 2016}}</ref>

In defining its mission, the journal notes that the term English Renaissance "is given its broadest connotation: subjects have ranged from Chaucer to Restoration drama and beyond. The literature and drama of the Elizabethan period is, however, the focal point of our interests."<ref>{{cite web|title=Cahiers homepage|url=https://uk.sagepub.com/en-gb/eur/cahiers-%C3%A9lisab%C3%A9thains/journal202512#description|website=SAGE Publications|accessdate=1 February 2016}}</ref> The journal also publishes articles on Performance in Context and a wide-range of international play reviews, as well as book reviews. The journal is published three times a year (April, July, November).

The journal is a member of the [http://publicationethics.org/ Committee on Publication Ethics (COPE)].<ref>{{cite web|title=Cahiers homepage|url=https://uk.sagepub.com/en-gb/eur/cahiers-%C3%A9lisab%C3%A9thains/journal202512#description|website=SAGE Publications|accessdate=1 February 2016}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official website|http://cae.sagepub.com/}}

{{DEFAULTSORT:Cahiers Elisabethains}}
[[Category:Shakespearean scholarship]]
[[Category:English-language journals]]
[[Category:Triannual journals]]
[[Category:Publications established in 1972]]
[[Category:French literary magazines]]