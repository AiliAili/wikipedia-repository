{{Userspace draft|source=ArticleWizard|date=October 2016}}

'''John Matthew Ennis''' (5 August 1864 – 31 May 1921), invariably referred to as '''Matthew Ennis''' or '''J. Matthew Ennis''', was an English pianist and organist who had a substantial academic career in [[Adelaide, South Australia]].

==History==
[[File:J M Ennis.jpg|thumb|Drawing of Ennis by "Cosmopolitan"]]
Ennis was born a son of Matthew Ennis in [[Dover]], but grew up in London where he was educated at the University College School. He sang as a choirboy and gained sufficient expertise in organ playing to take his first church appointment as organist at the age of 14, serving at the Church of St Barnabas, [[King Square, London]], from 1878, then the Church of St Philip, [[Clerkenwell]], from 1883 to 1887 (both [[List of Commissioners' churches in London|Commissioners' churches since demolished]]).<ref name=Humphries>Maggie Humphries, Robert Evans ''Dictionary of Composers for the Church in Great Britain and Ireland'' A.& C. Black 1997</ref>

After leaving school, Ennis entered the Post Office,<ref>{{cite web|url=https://www.thegazette.co.uk/London/issue/25134/page/3584/data.pdf|title=The London Gazette|date=1 August 1882|accessdate=19 October 2016}}</ref> meanwhile studying pianoforte under [[Edward Dannreuther]]. It was around this time he decided life as a music teacher would be more interesting.

Ennis graduated B.Mus. (1892) and D.Mus. (1894) at the [[University of London]], passing the four examinations in four years — the first person to do so.<ref name=interesting>{{cite news |url=http://nla.gov.au/nla.news-article58182651 |title=Interesting People |newspaper=[[The Mail (Adelaide)|The Mail]] |issue=18 |location=Adelaide |date=31 August 1912 |accessdate=18 October 2016 |page=2 |via=National Library of Australia}}</ref> He served as choirmaster and organist at Holy Trinity Church (since demolished) in [[Knightsbridge]] from 1887 to 1893, and at [[List of churches in the Diocese of London|St Mary's, Brookfield]], from 1893 to 1899.

In 1898, Ennis supported Sir [[John Stainer]] in founding an association of musical graduates, the [[Union of Graduates in Music]], the object of which was to prevent trafficking in degrees. Around this time he became an advocate of Virgil's method of teaching piano, and became a lecturer and examiner at the Virgil Piano School in London.<ref>Almon Kincaid Virgil (c. 1840–1921), an American inventor, developed a practice clavier (i.e. clicking keyboard) and teaching method which enjoyed considerable popularity around the turn of the century, but is now forgotten.</ref>

Early in 1900, Ennis arrived in Sydney, Australia, to take up an appointment at [[Christ Church St Laurence]] near [[Central Station, Sydney|Central Station]], followed a year later by Mrs. Ennis.<ref>{{cite news |url=http://nla.gov.au/nla.news-article71465653 |title=Social News. |newspaper=[[Australian Town and Country Journal]] |volume=LXII, |issue=1626 |location=New South Wales, Australia |date=6 April 1901 |accessdate=21 October 2016 |page=44 |via=National Library of Australia}}</ref> He did a considerable amount of teaching in Sydney, served for a time as acting city organist, and participated in a series of recitals with renowned Australian pianist [[Elsie Stanley Hall]].

In 1901 Ennis was offered the Chair of Music at the Adelaide University, made vacant by the involuntary retirement of [[Professor Ives]], and in February 1902 arrived in Adelaide to take the position.

In 1910 he assumed conductorship of the Adelaide Choral Society, which led to a number of triumphant concerts.<ref>{{cite news |url=http://nla.gov.au/nla.news-article105225587 |title=Adelaide Choral Society |newspaper=[[The Daily Herald (Adelaide)|The Daily Herald]] |volume=3, |issue=652 |location=Adelaide |date=6 April 1912 |accessdate=18 October 2016 |page=10 |via=National Library of Australia}}</ref>

It was during Ennis's tenure that the State Conservatorium of Music of New South Wales joined the other states in adopting a uniform code of public music examinations, finally making the AUMEB examinations universally accepted qualifications throughout Australia.<ref>{{cite news |url=http://nla.gov.au/nla.news-article64539079 |title=University Music Examinations |newspaper=[[The North Western Advocate and the Emu Bay Times]] |location=Tasmania, Australia |date=4 December 1917 |accessdate=19 October 2016 |page=3 |via=National Library of Australia}}</ref>

Ennis retired from the Elder Conservatorium in 1919, when his health began to fail, and was succeeded at the university and Elder Conservatorium by [[E. Harold Davies|Harold Davies]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article15943898 |title=Music and Drama |newspaper=[[The Sydney Morning Herald]] |issue=26,026 |location=New South Wales, Australia |date=4 June 1921 |accessdate=18 October 2016 |page=8 |via=National Library of Australia}}</ref> His wife, who was a violinist, predeceased him by a few years. She was remembered for her work with the [[Society for the Prevention of Cruelty to Animals]],<ref>{{cite news |url=http://nla.gov.au/nla.news-article212481281 |title=Personal. |newspaper=[[The Critic (Adelaide)|The Critic]] |volume=XIX, |issue=1115 |location=Adelaide |date=25 June 1919 |accessdate=19 October 2016 |page=7 |via=National Library of Australia}}</ref>

==Compositions==
Ennis composed a [[magnificat]] for soloists, chorus, strings and organ; also a song, "Beautiful Maiden",<ref name=Humphries/> but pressure of other duties in Australia did not allow him the freedom for further composition.<ref name=interesting/>
<!-- succession box
Joshua Ives, 1884-1901,
John Matthew Ennis, 1902-1918.
Edward Harold Davies, 1919-1947 -->

==Recognition==
Friends and supporters of Ennis funded the erection of an ornamented [[headstone]]<ref>The ornament, whatever it may have been, is now gone and the grave is largely ruined.</ref> on the couple's grave in the [[West Terrace Cemetery]], and a brass plaque in his memory was placed on the northern wall just inside the western entrance to the [[Elder Conservatorium]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article64250001 |title=The Ennis Memorial |newspaper=[[The Register (Adelaide)|The Register]] |volume=XC, |issue=26,466 |location=Adelaide |date=23 October 1925 |accessdate=18 October 2016 |page=8 |via=National Library of Australia}}</ref>

==Family==
Ennis married Jane Isabel Hutchinson (? – 21 June 1919) on 17 April 1895. She was a daughter of John Hutchinson CE of London. 
No mention of any offspring has been found.

== References ==
{{Reflist}}

{{DEFAULTSORT:Ennis, Matthew}}
[[Category:1864 births]]
[[Category:1921 deaths]]
[[Category:English classical pianists]]
[[Category:English classical organists]]
[[Category:Australian classical pianists]]
[[Category:Australian classical organists]]
[[Category:Australian music educators]]
[[Category:University of Adelaide faculty]]
[[Category:Articles created via the Article Wizard]]