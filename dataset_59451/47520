{{Orphan|date=June 2016}}
 
'''Diffuse field acoustic testing''' is the testing of the mechanical resistance of a spacecraft to the acoustic pressures during launch.

In the [[aerospace industry]], acoustic chambers are the main facilities for such tests. A chamber is a reverberant room that creates a diffuse sound field and is composed of an empty volume (from 1 m<sup>3</sup> to 2900 m<sup>3</sup>) and a multifrequency sound generation system.

== Diffuse field principle==
Theoretically, diffuse field is defined as a [[Sound pressure]] field where there is no privileged direction of the energy. In other words, when sound pressure is the same everywhere in the room. This is obtained with large rooms with no absorbent materials on walls, ceiling or floor. Diffusionis enhanced in asymmetric rooms. To obtain such conditions, the room must be reverberant. The source's direct field must be negligible compared to the reverberant field, avoiding privileged propagation.<ref name="potel">{{cite book|first1=Catherine |last1=Potel|first2=Michel|last2= Bruneau|title=Acoustique générale: équations différentielles et intégrales, solutions en milieux fluides et solides, applications|url={{google books |plainurl=y |id=t8HUGAAACAAJ}}|year=2006|publisher=Ellipses|isbn=978-2-7298-2805-9}}</ref>

=== Reverberation time ===
{{see also|Reverberation}}
Reverberation is due to multiple reflections on walls with some delays that come back to the receptor. Summing up these contributions, a reverberant pressure field is created. The more reverberation, the more the field is diffused.

Two oft-used measures of reverberation time quantify this parameter,  : <math>RT_{30}</math> and <math>RT_{60}</math>. These values are the interval for the sound pressure level to the lower of 30 or 60 [[dBSPL]]. It can be obtained by measuring the sound pressure decrease after a sound impulse or by using approximate formulas such as Sabine's or Eyring's. In the case of a diffuse field (low absorption on the walls, and big volumes) Sabine's formula is used.

<math>RT_{60} = 0.16 \frac{V}{A}</math>

Where <math>A=\sum S\times \alpha</math> is the equivalent absorption area involving the surface <math>S</math> of the walls and their absorption coefficient <math>\alpha</math>.

=== Geometrical approach ===
{{see also|Geometrical acoustics}}
[[File:Reflexion spec diffuse wiki.jpg|thumb|Schematic representation of specular (in blue) and diffused (in green) reflections of an incident wave ray (in red)]]
Many theoretical ways to model sound propagation are used. One of these is the geometrical approach. This represents sound waves as a ray of energy propagating. When it meets an obstacle, this ray has two possible behaviors: It can be reflected following the normal of the plan, [[specular reflection]]. Alternatively it can be separated into many rays following a mathematical law (for example [[Lambert's law]]), [[diffused reflection]].

=== Quantities involving diffuse pressure field ===

Frequency is a main factor of a good diffused field. Some phenomena linked to frequency of the sound pressure field lead to poor homogeneity of a pressure field. The frequency response of a room is the amplification or reduction of some frequencies. It represents the repartition of pressure with respect to frequency. In low frequencies this can lead to mode apparition. These modes are due to standing waves that lead to maximum and minimum pressure according to the geometry of the room. To determine the frequency for which the pressure field can be considered diffused, [[Schroeder's frequency]] is commonly used. It is obtained considering the frequency from which the modal overlap exceeds <math>M=3</math>. Below this frequency, the field is not diffuse and standing waves create pressure modes

<math>F_s = 2000\sqrt{\frac{RT_{60}}{V}} </math>

Where <math>RT_{60}</math> is the reverberation time of the room and <math>V</math> its volume.

[[File:Modes duct.jpg|thumb|Schematic transverse cut view of pressure modes in a duct (closed-closed)]]

For example, in the case of a rectangular room, low frequency modes are determined relative to the room dimensions as

<math>f_{lmn} = \frac{c_0}{2}\sqrt{\left(\frac{l}{L_x}\right)^2 + \left(\frac{m}{L_y}\right)^2 + \left(\frac{n}{L_z}\right)^2}</math>

 
Where <math>l</math>, <math>m</math> and <math>n</math> are respectively the mode of the length <math>L_x</math>, <math>L_y</math> and <math>L_z</math> of the room and <math>c_0</math> the [[Speed|celerity]] of sound in the working fluid.

== Aerospace application ==

Acoustic tests are mainly use for environmental tests on aircraft structures.<ref name="tustin">{{cite book|first1=Wayne |last1=Tustin|first2=Robert|last2= Mercado|title=Random vibration in perspective|url={{google books |plainurl=y |id=w4EoAQAAMAAJ}}|date=1 August 1984|publisher=Tustin Institute of Technology|isbn=978-0-918247-00-1}}</ref> Satellites are expensive products with high-engineering built-in components.<ref name="MaralBousquet2009">{{cite book|first1=Gerard |last1=Maral|first2=Michel|last2= Bousquet|title=Satellite Communications Systems: Systems, Techniques and Technology|url={{google books |plainurl=y |id=pGA8ngEACAAJ}}|year=2009|publisher=Wiley|isbn=978-0-470-71458-4|edition=5}}</ref> To improve the resistance of a spacecraft during launch and during its orbital life, analysis is focused on tests in three categories : [[Spacecraft thermal control|Thermal]], [[Radio frequency|Radio-frequencies]] and [[Vibration]]s. This last test area is focused on the mechanical stresses that the specimen will meet during its life, especially during launch.

{| class="wikitable"
|+Mechanical forces applied during spacecraft launch
! Launch steps !! Resulting mechanical aggressions
|-
| First Stage ignition and boosters ignition || Acoustic vibration and quasi-static loads
|-
| Boosters extinction || Sinusoidal vibrations
|-
| First stage Extinction || Sinusoidal vibrations
|-
| Second Stage ignition || Quasi-static loads
|-
| Fairing release || Pyrotechnic shock
|-
| Second stage extinction || Sinusoidal vibrations
|-
| Third stage ignition || Quasi-static loads
|-
| Third stage extinction || Sinusoidal vibrations
|-
| Spacecraft release || Clamp band release shock
|}

Acoustics creates mechanical stresses during the first five seconds. Sound pressure levels can go up to 150 dBSPL. Acoustic tests are used to verify the mechanical resistance of the satellite and its elements to acoustic pressures generated.

=== Accelerometer measurement ===
{{see also|Vibration#Vibration testing}}
Once the sound generation system is working, acceleration measurement is performed by accelerometers placed on the specimen.

== Acoustic chamber ==

=== Sound generation ===
To test a satellite, a sound generation system generates a broadband spectrum ([25&nbsp;Hz-10000]Hz) simulating the maximum envelope of all launchers that the satellite may fly in. To qualify, three tests are realized with changing global gain compared to launcher spectrum:
* Low-Level : - 8&nbsp;dB SPL
* Intermediate : - 4&nbsp;dB SPL
* PFM (Proto Flight Model) : 0&nbsp;dB SPL

Before testing the satellite, an empty room test is performed to check the chamber's signature.

This pressure field is generated by multifrequency sirens powered by [[nitrogen]] or [[compressed air]] modulators. This system can generate sound pressure levels up to 160 dBSPL. Each acoustic chamber has its own configurations, but each siren is centered on a frequency where sound pressure levels are the highest. In some cases these sirens can be completed with electroacoustic systems to generate and control midrange and high frequencies. Sirens generate low frequencies, but with high sound pressure levels [[distortion]] appears that leads to higher harmonics. Loudspeakers are used in some chambers to control these frequencies.

To produce exact levels, piloting microphones check sound pressure levels and apply a realtime gain correction to adjust the level.

=== Advantages ===
* Homogeneity : Spatial homogeneity guaranteed for ± 1.5 dBSPL
* Low frequency generation : Very efficient low frequency generation (below 50&nbsp;Hz)
* Security : Control with piloting microphones that adjust the level or abort if needed
* Representativeness : Faithful to real stresses during launch
* Well known process : Used by many aerospace industries 

=== Disadvantages ===
* Gas generation : May require large amounts of nitrogen
* High frequency control : If no high frequency sirens or electroacoustic devices are included, only harmonics generated by distortion produce mid and high frequencies.

== Examples ==
* Thales Alenia Space (Cannes) : 1000 m<sup>3</sup>
* IABG (Ottobrunn) : 1378 m<sup>3</sup>
* NASA : 2860 m<sup>3</sup>
== References ==

{{Reflist|30em}}

== External links ==
* [https://www.thalesgroup.com/en/worldwide/space/space Thales Alenia Space Official Website]
* [http://www.iabg.de/en/business-fields/space/ IABG Space Official Website]
* [[Space Power Facility#Reverberant Acoustic Test Facility|NASA's Space Power Facility (SPF) Wikipedia page]]
* [http://www.intespace.net/en/tests/mechanical-tests/acoustics-tests.html Intespace's acoustic test facility]

[[Category:Physics]]
[[Category:Acoustics]]