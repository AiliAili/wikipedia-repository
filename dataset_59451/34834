{{good article}}
{{Infobox hurricane season
| Basin= Atl
| Name = 1902 Atlantic hurricane season
| Year=1902
| Track=1902 Atlantic hurricane season map.png
| First storm formed=June 12, 1902
| Last storm dissipated=November 6, 1902
| Strongest storm name=Four
| Strongest storm pressure=970
| Strongest storm winds=90
| Total depressions=
| Total storms=5
| Total hurricanes=
| Total intense=
| Damages=
| Fatalities=5
| Inflation=
| five seasons=[[1900 Atlantic hurricane season|1900]], [[1901 Atlantic hurricane season|1901]], '''1902''', [[1903 Atlantic hurricane season|1903]], [[1904 Atlantic hurricane season|1904]]
}}

The '''1902 Atlantic hurricane season''' featured five known [[tropical cyclone]]s, three of which made [[Landfall (meteorology)|landfall]] in the [[United States]]. The first system was initially observed in the northwestern [[Caribbean Sea]] on June&nbsp;12. The last system dissipated on November&nbsp;6 while located well southeast of [[Newfoundland]]. These dates fall within the period with the most tropical cyclone activity in the Atlantic. None of the systems existed simultaneously.

Of the season's five tropical cyclones, three reached hurricane status. However, none of them strengthened into [[Saffir-Simpson Hurricane Scale#Categories|major hurricane]]s, which are Category&nbsp;3 or higher on the modern-day [[Saffir–Simpson hurricane wind scale]]. Along with [[1901 Atlantic hurricane season|1901]], this was the first time that two consecutive seasons lacked a major hurricane since [[1864 Atlantic hurricane season|1864]] and [[1865 Atlantic hurricane season|1865]].<ref name="ACE"/> Only one storm left significant impact, which was the second hurricane. It brought flooding and strong winds to [[Texas]], resulting in severe damage in some areas. A tornado spawned by the storm also caused five fatalities.

The season's activity was reflected with an [[accumulated cyclone energy]] (ACE) rating of 28, the lowest value since [[1890 Atlantic hurricane season|1890]]. ACE is, broadly speaking, a measure of the power of the hurricane multiplied by the length of time it existed, so storms that last a long time, as well as particularly strong hurricanes, have high ACEs. It is only calculated for full advisories on tropical systems at or exceeding 39&nbsp;mph (63&nbsp;km/h), which is tropical storm strength.<ref name="ACE">{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/comparison_table.html|title=Atlantic Basin Comparison of Original and Revised HURDAT|date=February 2014|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|accessdate=April 28, 2014|location=Miami, Florida}}</ref>
__TOC__
{{Clear}}

==Systems==
{{Clear}}

===Tropical Storm One===
{{Infobox Hurricane Small
| Basin=Atl
| Image=1902 Atlantic tropical storm 1 track.png
| Formed=June 12
| Dissipated=June 16
| 1-min winds=50
| Pressure=<997
}}
In June, a [[Low-pressure area|low pressure area]] was observed over the western [[Caribbean Sea]].<ref name="partagas">{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1901-1904/1902.pdf|title=Year 1902|author=Jose Fernandez Partagas and Henry F. Diaz|date=1997|publisher=[[National Oceanic and Atmospheric Administration]]|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|accessdate=May 5, 2014|location=Miami, Florida|format=[[PDF]]}}</ref> At 12:00&nbsp;[[Coordinated Universal Time|UTC]] on June&nbsp;12, a tropical depression developed about {{convert|20|mi|km}} north of [[Swan Islands, Honduras|Swan Island, Honduras]]. The depression moved north-northeastward and intensified into a tropical storm early the next day. Later on June&nbsp;13, the storm made landfall in modern-day [[Artemisa Province]] of [[Cuba]] with winds of 45&nbsp;mph (75&nbsp;km/h). After reaching the [[Gulf of Mexico]] late on June&nbsp;13, the system continued to strengthen and peaked with [[maximum sustained wind]]s of 60&nbsp;mph (95&nbsp;km/h). Around 23:00&nbsp;UTC, it made landfall near [[Steinhatchee, Florida]] at the same intensity.{{Atlantic hurricane best track}}

After moving inland, the storm moved northeastward and slowly weakened. Around midday on June&nbsp;16, the system became extratropical over southeastern [[Virginia]] and then accelerated northeastward, before dissipating near [[Anticosti Island]], [[Quebec]] late on the following day.{{Atlantic hurricane best track}} Some locations in Florida observed tropical storm force winds.<ref name="partagas"/> In [[Virginia]], the steamer ''Falcon'' sank {{convert|2|mi|km}} southeast of [[False Cape]]. On June&nbsp;16, the cities of [[Fredericksburg, Virginia|Fredericksburg]] and [[New Canton, Virginia|New Canton]] both set 24-hour rainfall records for the month of June, with {{convert|3.45|in|mm}} and {{convert|3.7|in|mm}} observed, respectively. A drought in the area was ended, which particularly benefited tobacco crops.<ref>{{cite report|url=http://www.wpc.ncep.noaa.gov/research/roth/vaerly20hur.htm|title=Early Twentieth Century|author=David M. Roth and Hugh Cobb|date=July 16, 2001|work=[[Weather Prediction Center]]|publisher=National Oceanic and Atmospheric Administration|accessdate=May 1, 2014|location=Camp Springs, Maryland}}</ref> 
{{clear}}

===Hurricane Two===
{{Infobox Hurricane Small
| Basin=Atl
| Image=1902 Atlantic hurricane 2 track.png
| Formed=June 21
| Dissipated=June 28
| 1-min winds=70
| Pressure=<995
}}
The second observed tropical cyclone of the season developed over [[Chiapas]] at 00:00&nbsp;UTC on June&nbsp;21. Initially a tropical depression, the system moved slowly northwestward and reached the Gulf of Mexico early the next day. Around 12:00&nbsp;UTC on June&nbsp;23, it strengthened into a tropical storm. While curving north-northwestward, the storm intensified into a hurricane at 00:00&nbsp;UTC on June&nbsp;26. Six hours later, the hurricane attained its maximum sustained wind speed of 80&nbsp;mph (130&nbsp;km/h). However, it then began to weaken falling to tropical storm status late on June&nbsp;26. At 21:00&nbsp;UTC that day, the storm made landfall near [[Corpus Christi, Texas]] with winds of 70&nbsp;mph (110&nbsp;km/h). After moving inland, the system quickly weakened and became extratropical over [[Oklahoma]] by midday on June&nbsp;28. The remnants moved rapidly northeastward before dissipating over [[Pennsylvania]] the next day.{{Atlantic hurricane best track}}

A drought existed in Texas for the previous six weeks. Thus, rainfall produce by this storm was beneficial to cotton and rice. However, withered corn stalks were easily blown down by the wind. Further, heavy precipitation in some areas resulted in flooding. The highest 24-hour rainfall total was {{convert|14.22|in|mm|abbr=on}} at [[Nacogdoches, Texas|Nacogdoches]], setting a daily rainfall record for June. This swelled Lanana and Bonita creeks. All bridges were swept away and communication was cut off, while the south side of town was underwater. At [[Galveston, Texas|Galveston]], {{convert|5.54|in|mm|abbr=on}} of rain fell on June&nbsp;27, setting a daily rainfall record. Over {{convert|6|in|mm|abbr=on}} fell in [[La Porte, Texas|La Porte]], ruining more than {{convert|200000|lbs|kg|abbr=on}} of hay west of town. [[Morgan's Point, Texas|Morgan’s Point]] was {{convert|3|ft|m|abbr=on}} under water. In [[Gregg County, Texas|Gregg]] and [[Harrison County, Texas|Harrison]] counties, heavy rains washed out the Texas and Pacific railroads. Flooding occurred across the upper Sabine basin as well.<ref name="wpctx">{{cite report|url=http://www.wpc.ncep.noaa.gov/research/txhur.pdf|title=Texas Hurricane History |author=David M. Roth|date=January 17, 2010|work=[[Weather Prediction Center]]|publisher=National Oceanic and Atmospheric Administration|pages=30-31|accessdate=May 7, 2014|location=Camp Springs, Maryland}}</ref>

Strong winds also buffeted portions of the state, with gusts up to {{convert|65|mph|km/h|abbr=on}} in Galveston. A freight train was blown off the track at [[East Bernard, Texas|East Bernard]] in [[Wharton County, Texas|Wharton County]]. A tornado moved northeast through Krasna, near 
[[Wallis, Texas|Wallis]], killing five people. In [[El Campo, Texas|El Campo]], fruit fell off trees. Windmills and chimneys were blown down in [[Edna, Texas|Edna]], while in [[Ganado, Texas|Ganado]] and [[Louise, Texas|Louise]], outhouses and barns were destroyed. In [[Houston, Texas|Houston]], trees were uprooted and outhouses destroyed by the gale. Electric wires were downed. Trees were uprooted and damage to sorghum crops were reported in [[Lavaca County, Texas|Lavaca County]]. The remnants of the storm produced heavy rainfall in [[Missouri]] and tornadoes in [[Indiana]]. The storm in [[Great Plains]] set up a cold and moist easterly wind which caused colder than normal temperatures in [[Nebraska]] and a rare late June snowstorm in [[Denver, Colorado]]; up to {{convert|8|in|mm|abbr=on}} was observed.<ref name="wpctx"/>
{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
| Basin=Atl
| Image=1902 Atlantic hurricane 3 track.png
| Formed=September 16
| Dissipated=September 22
| 1-min winds=85
| Pressure=981
}}
A tropical storm was first observed at 06:00&nbsp;UTC on September&nbsp;16, while located about {{convert|640|mi|km}} southwest of [[Cape Verde]]. The storm headed west-northwestward for a few days and slowly strengthened. Late on September&nbsp;19, it accelerated and began curving northwestward. At 00:00&nbsp;UTC the next day, the storm strengthened into a Category&nbsp;1 hurricane on the modern-day [[Saffir–Simpson hurricane wind scale]]. Later on September&nbsp;20, the hurricane curved northward and then to the northeast. Further deepening occurred, with the storm becoming a Category&nbsp;2 hurricane early the following day.{{Atlantic hurricane best track}}

On September&nbsp;21, the system peaked with sustained winds of 100&nbsp;mph (155&nbsp;km/h). A ship that encountered the storm and observed a barometric pressure of {{convert|981|mbar|inHg|abbr=on|lk=on}}. The storm began weakening on September&nbsp;22 and fell to Category&nbsp;1 intensity. Several hours later, it became extratropical while located about {{convert|810|mi|km}} west-southwest of [[Flores Island (Azores)|Flores Island]] in the [[Azores]]. The remnants of this storm continued northeastward and weakened, until dissipating about {{convert|800|mi|km}} southeast of [[Cape Farewell, Greenland]] on September&nbsp;25.{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Four===
{{Infobox Hurricane Small
| Basin=Atl
| Image=1902 Atlantic hurricane 4 track.png
| Formed=October 3
| Dissipated=October 11
| 1-min winds=90
| Pressure=970
}}
A tropical depression developed in the [[Pacific Ocean]] while located about {{convert|105|mi|km}} west-southwest of [[Tapachula]], [[Chiapas]] on October&nbsp;3. The depression moved slowly north-northwestward and made landfall in a rural area of southeastern [[Oaxaca]] early the next day. On October&nbsp;5, the system reached the Gulf of Mexico and soon intensified into a tropical storm. Thereafter, it curved east-northeastward and strengthened into a Category&nbsp;1 hurricane by October&nbsp;6. Early the next day, the storm became a Category&nbsp;2 hurricane and peaked with maximum sustained winds of 105&nbsp;mph (165&nbsp;km/h) and a minimum barometric pressure of {{convert|970|mbar|inHg|abbr=on}}.{{Atlantic hurricane best track}}

However, on October&nbsp;9, the hurricane weakened to a Category&nbsp;1. Around 06:00&nbsp;UTC the following day, the system weakened further to a tropical storm. Later on October&nbsp;10 at 21:00&nbsp;UTC, it made another landfall near [[Pensacola, Florida]] with winds of 60&nbsp;mph (95&nbsp;km/h). The storm rapidly weakened inland and became extratropical early on October&nbsp;11. The remnants accelerated while moving across the [[Southeastern United States]] and eventually emerged into the Atlantic Ocean, before dissipating south of [[Atlantic Canada]] on October&nbsp;13.{{Atlantic hurricane best track}} Some damage was reported in [[Tabasco]] due to gales. Along the eastern [[Gulf Coast of the United States]], strong winds were observed.<ref name="partagas"/>  
{{clear}}

===Tropical Storm Five===
{{Infobox Hurricane Small
| Basin=Atl
| Image=1902 Atlantic tropical storm 5 track.png
| Formed=November 1
| Dissipated=November 6
| 1-min winds=60
| Pressure=<993
}}
The final storm developed north of [[Puerto Rico]] at 00:00&nbsp;UTC on November&nbsp;1 as a tropical depression. Initially, it headed northwestward, but re-curved to the northeast about six hours later. By midday on November&nbsp;1, the depression strengthened into a tropical storm. The storm continued to intensify and peaked with sustained winds of 70&nbsp;mph (110&nbsp;km/h) early on November&nbsp;3.{{Atlantic hurricane best track}} Around that time, a ship observed a barometric pressure of {{convert|993|mbar|inHg|abbr=on}}, possibly suggesting hurricane intensity.<ref name="partagas"/> Later on November&nbsp;5, the storm began heading in a more eastward direction and weakened. At 06:00&nbsp;UTC the next day, it weakened to a tropical depression and dissipated several hours later, while located about {{convert|760|mi|km}} southeast of [[Cape Race]], [[Newfoundland]].{{Atlantic hurricane best track}} 
{{clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of tropical cyclones]]
*[[List of Atlantic hurricane seasons]]

==References==
<references />

==External links==
{{Commons category|1902 Atlantic hurricane season}}
*[http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1902.pdf Monthly Weather Review]

{{TC Decades|Year=1900|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1902 Atlantic Hurricane Season}}
[[Category:1900–1909 Atlantic hurricane seasons| ]]