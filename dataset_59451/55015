'''Aslan-Bey Shervashidze''' was a prince of the [[Principality of Abkhazia]] in 1808–10. He was the second son of [[Kelesh Ahmed-Bey Shervashidze]]. Aslan-Bey was associated with pro-Turkish elements of the region, and was responsible for rebelling against and later killing his father<ref>George Hewitt, The Abkhazians, 1998, page 71, calls this a Russian fabrication. He presents Aslan-Bey as a popular ruler and Sefer-Bey as a foreign-backed usurper</ref> in order to ascend the thrown of the Principality. Shervashidze turned the town of [[Sukhumi]] into his royal residence, which at the time, was guarded by a [[Ottoman Empire|Turkish]] military regiment.  Aslan-Bey actively fought together with King Solomon II of [[Imereti]] against [[Tsarist]] Russian forces.

[[File:Sharvashidze.BMP.jpg|thumb|Coat of arms of the princely family of Shervashidze]]

In 1810, after several decisive Russian military victories, Shervashidze was driven out of Sukhumi together with the Turkish regiment that was protecting him and fled to [[Turkey]]. After Aslan-Bey’s expulsion from Abkhazia, the Tsarist Russian leadership established Aslan-Bey’s brother, [[Sefer Ali-Bey Shervashidze]], as the new ruler of Abkhazia.

==Descendants in Turkey==
{{unsourced section|date=October 2016}}
During his exile, Aslan-Bey had a son named [[Ahmed-Bey]]. When Ahmed-Bey was in his fifties, Aslan-Bey died of old age. Ahmed-Bey, who also led his life in the outer skirts of Georgia had a son named [[Uzun-Burhan]]. When Uzun-Burhan was old enough, Ahmed-Bey sold all their property and fled to Turkey along with his son, buying villages in Sinop. Uzun-Burhan had two sons, Aslan, named after his great grandfather Aslan-Bey, and Ahmet-Bey, named after his father.

Once the surname law was established in the year 1934, Aslan-bey honored his great-grandfather, by getting the surname Torun, which means grandson as in the grandsons of Aslan-Bey. He was thereby known as [[Aslan Torun]]. Aslan Torun had three children, Şükran Torun-Serdaroğlu, Rasim Torun, Cahit Torun. Aslan Torun had 7 grandchildren: Müge, Ayşegül, İsmet, Nilgün, Selma, Jale and Gizem.  Ahmet-Bey had a daughter called Meliha, and 2 grandchildren called Gülüm and Süleyman.

==References==

* ''Georgian State (Soviet) Encyclopedia.'' 1983. Book 10. p.&nbsp;688.
* ''Sinop Etnografya Museum/Aslan Torun Mansion''
{{reflist}}
{{s-start}}
{{s-hou|House of [[Shervashidze]]/Chachba}}
{{s-reg|}}
{{succession box | title=[[Principality of Abkhazia|Prince of Abkhazia]] | before=[[Kelesh Ahmed-Bey Shervashidze|Kelesh Ahmed-Bey]] | after=[[Sefer Ali-Bey Shervashidze|Sefer Ali-Bey]] | years=1808–1810}}
{{s-end}}

{{DEFAULTSORT:Shervashidze, Aslan-Bey}}
[[Category:Princes of Abkhazia]]


{{abkhazia-bio-stub}}
{{Georgia-noble-stub}}