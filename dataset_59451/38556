{{Infobox video game
| title = BoxBoy!
| image = BoxBoy art.jpg
| developer = [[HAL Laboratory]]
| publisher = [[Nintendo]]
| director = Yasuhiro Mukae
| producer = {{ubl|Satoshi Mitsuhara|Keisuke Terasaki}}
| designer = Haruka Itou
| programmer = Takashi Nozue
| artist = Tatsuya Kamiyama
| composer = {{ubl|Jun Ishikawa|Hirokazu Ando}}
| platforms = [[Nintendo 3DS]]
| released = {{Video game release|JP|January 14, 2015|WW|April 2, 2015}}
| genre = [[Puzzle video game|Puzzle]] [[platformer]]
| modes = [[Single-player video game|Single-player]]
}}
{{nihongo|'''''BoxBoy!'''''|ハコボーイ|Hako Bōi!|lead=yes}} is a [[puzzle video game|puzzle]] [[platformer]] [[video game]] developed by [[HAL Laboratory]] and published by [[Nintendo]] for the [[Nintendo 3DS]] [[handheld console]]. Players control Qbby, a square-shaped character who can produce a string of connected boxes. The boxes are used to overcome obstacles in stages that Qbby must be guided through. The project plan for ''BoxBoy!'' was devised in 2011 by director Yasuhiro Mukae, although the game did not enter development until 2013. The core gameplay concept remained the same throughout the title's development, however changes were made to length of levels and the structure of the game. Each set of stages was designed with a specific theme in mind with the intention of expanding the game's depth by introducing new elements continuously. The game was released via the [[Nintendo eShop]] distribution service in Japan in January 2015, and worldwide in April 2015.

''BoxBoy!'' received a positive reception from critics. Reviewers praised the game's creative puzzles and inventive use of the box-manipulation mechanics. Some critics were pleasantly surprised by the amount of content that was offered and agreed that the package was good value for money. However, the game's simple art style was divisive among reviewers. A sequel, ''[[BoxBoxBoy!]]'', was released for the Nintendo 3DS in 2016, with a third game, ''[[Bye-Bye BoxBoy!]]'', in 2017.

==Gameplay==
[[File:Boxboy screenshot.png|thumb|Qbby uses boxes to shield himself from the lasers. He can then detach and drop the boxes on the switch to open the door ahead.]]
''BoxBoy!'' is a [[Puzzle video game|puzzle]] [[Platform game|platform]] video game in which players control a square-shaped character named Qbby.<ref name="ign_review" /> The goal of the game is to guide Qbby through a series of obstacle-filled stages that are divided into sets called worlds.<ref name="ign_review" /><ref name="gamespot_review" /> Each world is focussed on a particular gameplay theme; for example one world introduces stages that contain deadly lasers, and another introduces cranes for transporting Qbby around.<ref name="ninlife_int" /> The central gameplay mechanic is Qbby's ability to create boxes and make use of them to clear the obstacles. Boxes are created one at a time as a connected string and the maximum number of boxes in a string varies between each stage.<ref name="ign_review" /><ref name="gamespot_review" /> Qbby can detach the string of boxes then push it to different positions.<ref name="gamespot_review" /> The boxes can be used to overcome obstacles, such as creating a bridge to pass bottomless pits or building staircases to reach higher platforms.<ref name="ign_review" /> Additionally, Qbby can keep the string of boxes attached to his body.<ref name="gamespot_review" /> He may then use the boxes as a shield to block hazards such as deadly lasers.<ref name="ign_review" /> He is also able to grapple onto ledges and pull himself up by retracting through the boxes.<ref name="ign_review" /><ref name="gamespot_review" /> Creating a new string of boxes causes the previous string to disappear.<ref name="gamespot_review" /> If Qbby dies, he will [[respawn]] at a nearby [[Checkpoint (video gaming)|checkpoint]].<ref name="ign_review" />

Completing stages rewards the player with medals that can be spent on challenge stages, music, and costumes for Qbby. Additional medals can be earned by collecting crowns found on each stage, which must be collected before a certain number of boxes are used.<ref name="gamespot_review" /><ref name="gi_review" /> The game makes use of the [[Nintendo 3DS#Activity log|Nintendo 3DS' Play Coin]] feature; Play Coins can be used to request hints to the puzzle solutions.<ref name="eurogamer_review" /> ''BoxBoy!'' features a black and white [[monochrome]] graphical style.<ref name="ign_review" /><ref name="gamespot_review" /> The game features a minimal story, in which Qbby meets two additional characters after progressing through certain worlds.<ref name="shacknews_review" /><ref name="venturebeat_review" />

==Development and release==
<!-- production/concept -->
''BoxBoy!'' was developed by Japanese video game company [[HAL Laboratory]].<ref name="ninlife_int" /> It was a small experimental project in development while the studio was working on ''[[Kirby: Triple Deluxe]]'' and ''[[Kirby and the Rainbow Curse]]''.<ref name="ninlife_int" /><ref name="kotaku_int" /> The project plan for ''BoxBoy!'' was conceived in July 2011 by employee Yasuhiro Mukae, who would later serve as the game's director. Prior to ''BoyBoy!'', Mukae had never designed or directed a game, but his interest in undertaking these roles prompted him to start planning a project. His lack of experience and the difficulty of designing a game with a large scope led him to plan a game that was simple and compact. The concept of ''BoxBoy!'' arose when he was brainstorming ideas for a game that featured [[Retrogaming|retro gameplay]], in the style of titles released on the [[Nintendo Entertainment System]] and the [[Game Boy]].<ref name="ninlife_int" /> The idea was simply creating and using boxes to traverse puzzle levels.<ref name="engadget_int" /> Several members of staff saw potential in the concept and created a prototype for demonstration.<ref name="kotaku_int" /> Mukae's product proposal wasn't formally submitted to HAL until 2013,<ref name="ninlife_int" /> when the company announced that it would field game design concepts featuring new characters.<ref name="kotaku_int" /> The development team spent about six months experimenting with the game's design.<ref name="engadget_int" /> Once development of ''Kirby: Triple Deluxe'' concluded, some members of its development team joined the ''BoxBoy!'' development team and the project entered full production.<ref name="kotaku_int" /> The game was complete after a year in full production.<ref name="engadget_int" />

<!-- design -->
To give each world a theme, they were each designed with a particular gameplay element in mind. This approach created a structure where players would be introduced to new gameplay mechanics frequently. The intention was to keep the game engaging and continually expand the depth of gamplay as players progressed through the game. The first stage of each world is designed to be a simple puzzle that teaches players the new mechanic.<ref name="ninlife_int" /> Initially, the development team were creating large stages that would take a fairly long time to complete. However, they later decided that players should be able to complete them quicker, and thus the level structure was changed and the amount of content per stage was reduced. Story elements were also added to encourage players to keep playing. Creating an intuitive and easy control scheme was an important factor as the development team wanted to ensure that the action creating boxes was comfortable and fun.<ref name="engadget_int" /> The development team had considered implementing a [[multiplayer]] mode for ''BoxBoy!''. Utimately, the feature was excluded as they believed that making a fun single-player experience should be the priority.<ref name="ninlife_int" />

<!-- art/character design -->
The use of black and white [[line art]] for the game's graphics was an attempt to differentiate it from other games and catch the interest of gamers.<ref name="engadget_int" /> Implementing it presented some challenges for the design staff.<ref name="ninlife_int" /> The design of Qbby, the [[player character]], came after the idea of creating boxes was established; he was designed from a functional perspective. At one point in development, while brainstorming ''BoxBoy!''{{'}}s visual style, the team had considered making [[Kirby (character)|Kirby]] the main character. It become difficult trying to make Kirby, a round character, look natural in the box-producing action, so they settled with a character that had a square-shaped body, the same as the boxes. Feet were added to Qbby so he could move and jump, and eyes were added so the player could recognise which direction he was facing.<ref name="kotaku_int" /> Since Qbby had a simple design, the team focussed on making his animations expressive to ensure the character was engaging and had a personality.<ref name="engadget_int" />

<!-- release -->
''BoxBoy!'' was published by [[Nintendo]].<ref name="engadget_int" /> The game was released for the [[Nintendo 3DS]] [[handheld console]] via the [[Nintendo eShop|eShop]] distribution service in Japan on January 14, 2015,<ref name="kotaku_jp_release" /> and worldwide on April 2, 2015.<ref name="ninlife_ww_release" />

==Reception==
{{Video game reviews
| MC = 80/100<ref name="metacritic" />
| EuroG = Recommended<ref name="eurogamer_review" />
| GI = 8.25/10<ref name="gi_review" />
| GSpot = 8/10<ref name="gamespot_review" />
| IGN = 8.2/10<ref name="ign_review" />
| NLife = 6/10<ref name="ninlife_review" />
| rev1 = [[Shacknews]]
| rev1Score = 8/10<ref name="shacknews_review" />
| rev2 = [[USgamer]]
| rev2Score = {{Rating|4.5|5}}<ref name="usgamer_review" />
| rev3 = [[VentureBeat]]
| rev3Score = 80/100<ref name="venturebeat_review" />
}}
''BoxBoy!'' received generally favourable reviews from critics according to [[review aggregator]] website [[Metacritic]].<ref name="metacritic" />

Some reviewers highlighted that the early stages in ''BoxBoy!'' felt too basic, and consequently the game did not make a great first impression.<ref name="gamespot_review" /><ref name="usgamer_review" /> [[GameSpot]] writer Justin Haywald described the simplicity of the game's initial puzzles as "almost off-putting". However, he was satisfied with the more elaborate stages presented after progressing through the game, calling the puzzles "ingenious" and praising the intelligent use of the box-manipulation mechanic.<ref name="gamespot_review" /> [[IGN]]'s Jose Otero complimented the game's level design and challenge posed by later stages in the game. He also liked the game's generous checkpoint system noting that it was helpful during the more difficult puzzles.<ref name="ign_review" /> Bob Mackey of [[USgamer]] mentioned that the friendly approach, short levels, and frequent checkpoints did not detract from the game's challenge; he said that by allowing players to quickly test different solutions without having to restart a stage, HAL had managed to remove the frustration typically found in similar block-based puzzle games.<ref name="usgamer_review" />

Christian Donlan of [[Eurogamer]] was surprised by the long length of the game and the amount of variety it offered given its simple premise. He enjoyed the game's pacing and felt that separating puzzles into short levels kept the game fun.<ref name="eurogamer_review" /> ''[[Game Informer]]'' reviewer Kyle Hilliard also approved of the game's pacing. He remarked that ''BoxBoy!''{{'}}s low price and well-designed puzzles made it entirely worth playing.<ref name="gi_review" /> [[VentureBeat]] writer Gavin Greene agreed that the game offered a lot of content at an inexpensive price.<ref name="venturebeat_review" /> The game's structure did draw some criticism from Jon Wahlgren, writing for [[Nintendo Life]]. He thought the game spent too much time teaching new mechanics and did not give players enough freedom to explore or experiment; as a result he believed the game felt restricted.<ref name="ninlife_review" />

According to some writers, ''BoxBoy!''{{'}}s graphics and art resembled the style found in Game Boy titles;<ref name="gamespot_review" /><ref name="shacknews_review" /><ref name="venturebeat_review" /><ref name="ninlife_review" /> the reaction towards it was mixed among reviewers. While some critics described the graphics as "charming" and "stylish",<ref name="gi_review" /><ref name="eurogamer_review" /> others called it "sterile".<ref name="ign_review" /> The expressive animations and cute character designs were well received.<ref name="gamespot_review" /><ref name="shacknews_review" /><ref name="venturebeat_review" />

==Sequels==
In 2016, a sequel titled ''[[BoxBoxBoy!]]'' was released for the [[Nintendo 3DS]], allowing Qbby to create two sets of boxes at a time.<ref name="eurogamer_sequel" /> The game was announced and released in Japan on January 6, 2016,<ref name="eurogamer_sequel_jp" /> and in other terrorities on June 30, 2016.<ref name="eurogamer_sequel" /> A third game, titled ''[[Bye-Bye BoxBoy!]]'', was released 2017. The game adds new kinds of boxes, such as explosive bombs and rocket propelled boxes, as well as box children that need to be escorted safely through levels. A physical compilation of all three games, titled {{nihongo|''HakoBoy! Hakozume Box''|ハコボーイ！ ハコづめBOX|BoxBoy! Stuffed Box}}, was also released in Japan the same day. The compilation also included a soundtrack CD featuring the music from all three games, as well as a Qbby [[Amiibo]] figure, the latter of which can be used with ''Goodbye! BoxBoy!''.<ref name="kotaku_boxboy3" />

==References==
{{Reflist|refs=

<ref name="engadget_int">{{cite web | url=https://www.engadget.com/2015/05/05/hal-laboratory-boxboy-interview/ | title=The Nintendo studio behind 'Kirby' talks its new game 'BOXBOY!' | work=[[Engadget]] | publisher=[[AOL]] | first=Anthony John | last=Agnello | date=5 May 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160517193550/http://www.engadget.com/2015/05/05/hal-laboratory-boxboy-interview/ | archivedate=May 17, 2016 | deadurl=no}}</ref>

<ref name="eurogamer_review">{{cite web | url=http://www.eurogamer.net/articles/2015-04-09-boxboy-review | title=Boxboy! review | work=[[Eurogamer]] | publisher=[[Gamer Network]] | first=Christian | last=Donlan | date=April 9, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20161009221808/http://www.eurogamer.net/articles/2015-04-09-boxboy-review | archivedate=October 9, 2016 | deadurl=no}}</ref>

<ref name="eurogamer_sequel">{{cite web | url=http://www.eurogamer.net/articles/2016-06-15-boxboxboy-release-date-set-this-month | title=BoxBoxBoy! release date set this month | work=[[Eurogamer]] | publisher=[[Gamer Network]] | first=Jeffrey | last=Matulef | date=June 15, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20161012170426/http://www.eurogamer.net/articles/2016-06-15-boxboxboy-release-date-set-this-month | archivedate=October 12, 2016 | deadurl=no}}</ref>

<ref name="eurogamer_sequel_jp">{{cite web | url=http://www.eurogamer.net/articles/2016-01-06-boxboy-sequel-gets-a-surprise-release-in-japan | title=Boxboy! sequel gets a surprise release in Japan | work=[[Eurogamer]] | publisher=[[Gamer Network]] | first=Jeffrey | last=Matulef | date=January 6, 2016 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160312182329/http://www.eurogamer.net/articles/2016-01-06-boxboy-sequel-gets-a-surprise-release-in-japan | archivedate=March 12, 2016 | deadurl=no}}</ref>

<ref name="gamespot_review">{{cite web | url=http://www.gamespot.com/reviews/boxboy-review/1900-6416125/ | title=BoxBoy! Review | work=[[GameSpot]] | publisher=[[CBS Interactive]] | first=Justin | last=Haywald | date=May 7, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160401155343/http://www.gamespot.com/reviews/boxboy-review/1900-6416125 | archivedate=April 1, 2016 | deadurl=no}}</ref>

<ref name="gi_review">{{cite web | url=http://www.gameinformer.com/games/boxboy/b/3ds/archive/2015/04/15/boxboy-3ds-game-informer-review.aspx | title=Thinking With Boxes - BoxBoy | work=[[Game Informer]] | publisher=[[GameStop]] | first=Kyle | last=Hilliard | date=April 15, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160202003229/http://www.gameinformer.com/games/boxboy/b/3ds/archive/2015/04/15/boxboy-3ds-game-informer-review.aspx | archivedate=February 2, 2016 | deadurl=no}}</ref>

<ref name="ign_review">{{cite web | url=http://uk.ign.com/articles/2015/05/05/boxboy-review | title=BoxBoy Review | work=[[IGN]] | publisher=[[Ziff Davis]] | first=Jose | last=Otero | date=May 5, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20161027225050/http://www.ign.com/articles/2015/05/05/boxboy-review | archivedate=October 27, 2016 | deadurl=no}}</ref>

<ref name="kotaku_boxboy3">{{cite web | url=http://kotaku.com/boxboy-3-is-coming-in-2017-also-amiibo-1789710479 | title=BoxBoy 3 is Coming In 2017 (Also AMIIBO) | work=[[Kotaku]] | publisher=[[Gizmodo Media Group]] | first=Luke | last=Plunkett | date=December 5, 2016 | accessdate=December 6, 2016 | archiveurl=https://web.archive.org/web/20161206074220/http://kotaku.com/boxboy-3-is-coming-in-2017-also-amiibo-1789710479 | archivedate=December 6, 2016 | deadurl=no}}</ref>

<ref name="kotaku_int">{{cite web | url=http://kotaku.com/the-guy-behind-one-of-the-year-s-best-3ds-games-1702235041 | title=The Guy Behind One Of The Year’s Best 3DS Games | work=[[Kotaku]] | publisher=[[Gizmodo Media Group]] | first=Stephen | last=Totilo | date=May 5, 2016 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160824143056/http://kotaku.com/the-guy-behind-one-of-the-year-s-best-3ds-games-1702235041 | archivedate=August 24, 2016 | deadurl=no}}</ref>

<ref name="kotaku_jp_release">{{cite web | url=http://kotaku.com/box-boy-is-full-of-puzzles-platforming-and-cuteness-1681298991 | title=Box Boy is Full of Puzzles, Platforming, and Cuteness | work=[[Kotaku]] | publisher=[[Gizmodo Media Group]] | first=Richard | last=Eisenbeis| date=January 23, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160824133941/http://kotaku.com/box-boy-is-full-of-puzzles-platforming-and-cuteness-1681298991 | archivedate=August 24, 2016 | deadurl=no}}</ref>

<ref name="metacritic">{{cite web | url=http://www.metacritic.com/game/3ds/boxboy! | title=BOXBOY! for 3DS Reviews | work=[[Metacritic]] | publisher=[[CBS Interactive]] | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160308100850/http://www.metacritic.com/game/3ds/boxboy! | archivedate=March 8, 2016 | deadurl=no}}</ref>

<ref name="ninlife_int">{{cite web | url=http://www.nintendolife.com/news/2015/04/interview_bringing_together_the_3ds_and_game_boy_in_boxboy | title=Interview: Bringing Together the 3DS and Game Boy in BOXBOY! | work=[[Nintendo Life]] | publisher=[[Gamer Network]] | first=Jon | last=Wahlgren | date=April 24, 2015 | accessdate=October 28, 2016 | archiveurl=http://www.nintendolife.com/news/2015/04/interview_bringing_together_the_3ds_and_game_boy_in_boxboy | archivedate=August 3, 2016 | deadurl=no}}</ref>

<ref name="ninlife_review">{{cite web | url=http://www.nintendolife.com/reviews/3ds-eshop/boxboy | title=Review: BOXBOY! | work=[[Nintendo Life]] | publisher=[[Gamer Network]] | first=Jon | last=Wahlgren | date=April 1, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20161026234553/http://www.nintendolife.com/reviews/3ds-eshop/boxboy | archivedate=October 26, 2016 | deadurl=no}}</ref>

<ref name="ninlife_ww_release">{{cite web | url=http://www.nintendolife.com/news/2015/04/hals_3ds_puzzle_platformer_boxboy_will_be_available_to_download_today | title=HAL's 3DS Puzzle Platformer BOXBOY! Will Be Available To Download Today | work=[[Nintendo Life]] | publisher=[[Gamer Network]] | first=Damien | last=McFerran | date=April 2, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160304235119/http://www.nintendolife.com/news/2015/04/hals_3ds_puzzle_platformer_boxboy_will_be_available_to_download_today | archivedate=March 4, 2016 | deadurl=no}}</ref>

<ref name="shacknews_review">{{cite web | url=http://www.shacknews.com/article/88864/boxboy-review-game-cubed | title=BoxBoy! Review: Game Cubed | work=[[Shacknews]] | publisher=Gamerhub | first=Steve | last=Watts | date=April 1, 2016 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160721200843/http://www.shacknews.com/article/88864/boxboy-review-game-cubed | archivedate=July 21, 2016 | deadurl=no}}</ref>

<ref name="usgamer_review">{{cite web | url=http://www.usgamer.net/articles/box-boy-3ds-review-its-hip-to-be-square | title=Box Boy 3DS Review: It's Hip to be Square | work=[[USgamer]] | publisher=[[Gamer Network]] | first=Bob | last=Mackey | date=April 7, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20161010113437/http://www.usgamer.net/articles/box-boy-3ds-review-its-hip-to-be-square | archivedate=October 10, 2016 | deadurl=no}}</ref>

<ref name="venturebeat_review">{{cite web | url=http://venturebeat.com/2015/04/10/boxboys-chill-minimalism-makes-it-fun-to-think-inside-outside-and-around-the-box/ | title=BoxBoy’s chill minimalism makes it fun to think inside, outside, and around the box | work=[[VentureBeat]] | first=Gavin | last=Greene | date=April 10, 2015 | accessdate=October 28, 2016 | archiveurl=https://web.archive.org/web/20160602235119/http://venturebeat.com/2015/04/10/boxboys-chill-minimalism-makes-it-fun-to-think-inside-outside-and-around-the-box/ | archivedate=June 2, 2016 | deadurl=no}}</ref>

|30em}}

==External links==
* {{Official website|http://boxboy.nintendo.com/}}

{{good article}}

[[Category:2015 video games]]
[[Category:HAL Laboratory games]]
[[Category:Nintendo 3DS games]]
[[Category:Nintendo 3DS-only games]]
[[Category:Nintendo 3DS eShop games]]
[[Category:Nintendo games]]
[[Category:Platform games]]
[[Category:Puzzle video games]]
[[Category:Video games developed in Japan]]