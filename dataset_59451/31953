{{Use dmy dates|date=May 2011}}
{{Use Australian English|date=May 2011}}
{{Infobox military person
|name= Bruce Kingsbury
|image= BruceSKingsbury.jpg
|image_size= 
|alt= 
|caption= Private Bruce Kingsbury, {{circa|lk=no|1940}}
|nickname= 
|birth_date= {{Birth date|1918|01|08|df=yes}}
|birth_place= [[Melbourne]], Australia
|death_date= {{Death date and age|1942|08|29|1918|01|08|df=yes}}
|death_place= [[Isurava, Papua New Guinea|Isurava]], Papua New Guinea
|placeofburial= 
|allegiance= Australia
|branch= [[Second Australian Imperial Force]]
|serviceyears= 1940–42
|rank= [[Private (rank)|Private]]
|unit= [[2/14th Battalion (Australia)|2/14th Infantry Battalion]]
|battles= Second World War
* [[North African Campaign]]
* [[Syria–Lebanon Campaign]]
* [[Battle of Jezzine (1941)|Battle of Jezzine]]
* [[South West Pacific theatre of World War II|South West Pacific theatre]]
* [[Kokoda Track Campaign]]
|awards= [[Victoria Cross]]
|relations= 
|laterwork= 
}}
'''Bruce Steel Kingsbury''', [[Victoria Cross|VC]] (8 January 1918 – 29 August 1942) was an Australian soldier of the [[Second World War]]. Serving initially in the Middle East, he later gained renown for his actions during the [[Battle of Isurava]], one of many battles forming the [[Kokoda Track campaign|Kokoda Track Campaign]] in [[New Guinea]]. His bravery during the battle was recognised with the [[Victoria Cross]], the highest decoration for gallantry "in the face of the enemy" that can be awarded to members of the British and [[Commonwealth of Nations|Commonwealth]] armed forces. The first serviceman to [[List of Australian Victoria Cross recipients|receive]] the VC for actions on Australian territory, Kingsbury was a member of the [[2/14th Battalion (Australia)|2/14th Infantry Battalion]].

On 29 August 1942, during the Battle of Isurava, Kingsbury was one of the few survivors of a platoon that had been overrun by the Japanese. He immediately volunteered to join a different platoon, which had been ordered to counter-attack. Rushing forward and firing his [[Bren light machine gun|Bren gun]] from the hip, he cleared a path through the enemy and inflicted several casualties. Kingsbury was then seen to fall, shot by a Japanese sniper and killed instantly. His actions, which delayed the Japanese long enough for the Australians to fortify their positions, were instrumental in saving his battalion's headquarters and he was posthumously awarded the Victoria Cross as a result.

==Early life==
Born in the [[Melbourne]] suburb of [[Preston, Victoria|Preston]] on 8 January 1918, Kingsbury was the second son of Philip Blencowe Kingsbury, an estate agent, and his wife Florence Annie, née Steel.<ref name=adb /> Growing up in [[Prahran]], Kingsbury became friends with Allen Avery when he was five years old. The two often raced billycarts down the hilly streets, and would remain lifelong friends.<ref name=Dornan1>{{Harvnb|Dornan|1999|pp=11–18}}</ref> Kingsbury attended [[Windsor, Victoria|Windsor]] State School as a child, and his results were good enough to earn a scholarship at [[RMIT University|Melbourne Technical College]]. Avery began an agricultural course in Longerenong. Although qualified as a printer, Kingsbury began working at his father's real estate business,<ref name=adb>{{Australian Dictionary of Biography|last=McAllester|first=James|id=A150032b|title=Kingsbury, Bruce Steel (1918–1942) |publisher=Australian Dictionary of Biography|year=2000|accessdate=7 March 2009}}</ref> a job he disliked.<ref name=Dornan1 />

Unhappy in the estate agency, Kingsbury took up the position of caretaker on a farm at [[Boundary Bend, Victoria|Boundary Bend]], not far from where Avery was working. After three months, the pair decided that they would go on an adventure – walking through western Victoria and [[New South Wales]].<ref name = Dornan1 /> In February 1936, Kingsbury and Avery left their jobs and began travelling north, working on various farms and estates. The pair eventually arrived in Sydney several months later, and returned to Melbourne on the first train back. Kingsbury resumed working as a real estate agent, while Avery worked as a nurseryman.<ref name=Dornan1 /> They spent their free time at dances and parties. During this time, Kingsbury met and became close to Leila Bradbury.<ref name = Dornan1 /> As the war in Europe escalated, Kingsbury and Avery made up their minds to enlist. Despite his parents' disapproval, Kingsbury signed up to the [[Second Australian Imperial Force|Australian Imperial Force]] on 29 May 1940.<ref name=WW2Roll>{{citation|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=A&veteranId=431320|title=Kingsbury, Bruce Steel|work=World War II Nominal Roll|publisher=Commonwealth of Australia|date=|accessdate=7 March 2009}}</ref>

==Second World War==
===Middle East===
Kingsbury was originally assigned to the [[2/2nd Pioneer Battalion (Australia)|2/2nd Pioneer Battalion]], but requested a transfer to the [[2/14th Battalion (Australia)|2/14th Infantry Battalion]] to join Avery who had, coincidentally, enlisted on the same day at a different recruitment centre.<ref name=adb /><ref name=Dornan2>{{Harvnb|Dornan|1999|pp=19–26}}</ref> The pair undertook basic training at [[Puckapunyal]], where they were assigned to the same [[Section (military unit)|section]] (7 Section of 9 Platoon) and were given drill instruction, rifle drill and mock battle training.<ref name=Dornan2/> After 7 Section learned they would be sent overseas, Kingsbury informed Avery that he planned to propose to Leila. The pair went to Melbourne to try to organise the wedding. Although Kingsbury gave Leila a wristwatch as an engagement present, they could not arrange a marriage licence before he left, and the marriage never took place.<ref name=Dornan2/>

Kingsbury, along with the rest of the [[7th Division (Australia)|7th Division]], was shipped to the Middle East in late 1940. Spending time in [[Tel Aviv]] and the surrounding areas, the 7th Division continued training and awaited further orders.<ref name=adb /><ref name=Dornan3>{{Harvnb|Dornan|1999|pp=29–41}}</ref> On 9 April, the division was sent forward to [[Mersa Matruh]] in Egypt to support the Commonwealth force's defences.<ref name = Dornan3 /> It replaced a Scottish unit and took up positions in the garrison.<ref name=Dornan3 /> On 23 May, Kingsbury's brigade was sent back to Palestine, en route to battle in [[Syria-Lebanon Campaign|Syria and Lebanon]].<ref name=Dornan4>{{Harvnb|Dornan|1999|pp=49–78}}</ref> The 2/14th fought against the [[Vichy French]] on the Lebanese mountain ranges,<ref name = Dornan4 /> as part of a three-pronged attack on [[Beirut]]. During this time, the division fought in many towns, including a major [[Battle of Jezzine (1941)|battle in Jezzine]], where Avery was wounded by a grenade—which drove metal splinters into his spine<ref name=Dornan4 />—and awarded the [[Military Medal]] for his "cool courage and devotion".<ref>{{cite web|url=http://www.awm.gov.au/cms_images/awm192/00309/003090045.pdf|title=Recommendation for Alan Richard Avery to be awarded a Military Medal|accessdate=26 March 2009|format=PDF|work=Index to Recommendations: Second World War|publisher=Australian War Memorial}}</ref> As the war with the Vichy French was winding down, on 11 July Kingsbury and Avery were selected for a contingent to collect and bury the dead.<ref name=Dornan4 /> The battalion stayed in Beirut for a few months, until setting up a semi-permanent camp at Hill 69, outside [[Jerusalem]].<ref name = Dornan4 />

On 30 January 1942, the 7th Division left Egypt for Australia, sailing via [[Bombay]], as the division was needed to fight against the Japanese.<ref name=Dornan5>{{Harvnb|Dornan|1999|pp=81–87}}</ref> Kingsbury's battalion made landfall at [[Adelaide]] and continued to Melbourne by rail. The battalion arrived on 16 March, and was given a week's leave. After this, the battalion underwent training in [[Glen Innes, New South Wales|Glen Innes]], before camping in [[Yandina, Queensland|Yandina]], Queensland. On 5 August, the battalion moved north to [[Brisbane]], boarding a ship to [[Port Moresby]] to join the fighting in New Guinea, where a force of mostly [[Australian Army Reserve|Militia]] personnel were engaged in a desperate defensive action.<ref name=Dornan5 />

===Kokoda Track Campaign===
[[File:9 platoon 2-14Bn 16Aug42.jpg|thumb|Kingsbury with the other members of his platoon on 16 August 1942]]
Following the [[Battle of the Coral Sea]], the Japanese abandoned the attempt to capture Port Moresby from the sea and, on 21 July, landed ground forces at [[Buna, Papua New Guinea|Buna]] in north-east Papua.<ref>{{Harvnb|McCarthy|1959|pp=108–122}}</ref><ref>{{Harvnb|Keogh|1965|pp=146–149; 169}}</ref> After capturing the town of [[Kokoda]] for the second time on 9 August, the Japanese began advancing along the [[Kokoda Track]] towards Port Moresby. The 2,500-strong Japanese force met the [[39th Battalion (Australia)|39th]] and [[53rd Battalion (Australia)|53rd Infantry Battalions]], at the town of [[Isurava, Papua New Guinea|Isurava]].{{sfn|Brune|2004|p=134}} As the battle was beginning to develop, on 26 August, members of the 2/14th, including Kingsbury, arrived at Isurava to reinforce the exhausted 39th Battalion.<ref name=Dornan5 /><ref name=Keogh205-206>{{Harvnb |Keogh|1965|pp=205–206}}</ref>

====Battle of Isurava====
The two combined battalions began digging in around Isurava. A headquarters had been set up at the top of the hill, which was vital to the defence of the position. While the Australians dug themselves in, the Japanese, led by Japanese Major General [[Tomitarō Horii]], prepared to attack. On 28 August, the Japanese launched their offensive. The Australians, who had initially been outnumbered but were now roughly equal in strength, resisted in the face of heavy machine-gun fire and hand-to-hand combat. On 29 August, the Japanese broke through the right flank, pushing the Australians back with heavy fire, threatening to cut off their headquarters.<ref name=Lindsay>{{Harvnb|Lindsay|2002|pp=77–81}}</ref> The Australians began to prepare a counter-offensive, and men volunteered to join an attack party. Kingsbury, one of the few survivors of his platoon, ran down the Track with the group.<ref>{{Harvnb|Brune|2004|p=149}}</ref> {{quote|You could see his Bren gun held out and his big bottom swaying as he went with the momentum he was getting up, followed by Alan Avery. They were cheerful. They were going out on a picnic almost.|Lieutenant Colonel [[Philip Rhoden|Phil Rhoden]]|''The Spirit of Kokoda''}}Using a [[Bren light machine gun|Bren gun]] he had taken from wounded Corporal Lindsay Bear, Kingsbury, alongside Avery and the rest of the group, engaged the nearby Japanese. The fire was so heavy that the undergrowth was completely destroyed within five minutes.<ref name=Lindsay /> It was then that Kingsbury, firing from his hip, charged straight at the Japanese.

{{quote|He came forward with this Bren and he just mowed them down. He was an inspiration to everybody else around him. There were clumps of Japs here and there and he just mowed them down.|Private Allen Avery|''The Spirit of Kokoda''}}

His actions demoralised the Japanese, killing several and forcing others to find cover.<ref name=Lindsay /> The rest of the Australian group, inspired by Kingsbury's actions, forced the Japanese further back into the jungle.<ref name = Lindsay /> Kingsbury was then seen to fall to the ground, shot by a Japanese sniper. The sniper fired one shot before disappearing. Avery, who had been about {{convert|6|ft|m}} from Kingsbury, briefly chased after the sniper but returned to carry Kingsbury to the regimental aid post;<ref name=Lindsay /> Kingsbury was dead by the time he arrived there.<ref name=Ham>{{Harvnb|Ham|2004|pp=176–177}}</ref>

==Legacy==
[[File:Brucekingsburygrave.jpg|thumb|right|Signalman R. Williams tending to Kingsbury's grave in 1944]]
Kingsbury's actions were a turning point in the battle.<ref name=Lindsay /><ref name=Ham /> The Japanese had begun to gather momentum in their attack, and were threatening to overrun the 2/14th's headquarters.{{sfn|Brune|1992|p=112}} His attack inflicted damage to the Japanese force, temporarily halting their advance. This allowed the Australian troops to stabilise their positions, eventually regaining control and defending the battalion's headquarters.<ref name=LonGaz>{{LondonGazette|issue=35893|supp=yes|startpage=695|date=5 February 1943|accessdate=7 March 2009}}</ref> His act of bravery served as an inspiration to the troops.<ref name=Lindsay /> However, the battle ended in defeat for the Australians, with elements of the 2/14th breaking during the afternoon of 29 August. The remainder of the battalion managed to withdraw during the night, but suffered heavy casualties and another defeat during fighting the next day at positions around the Isurava Guest House.<ref>{{cite web|title=Battle of Isurava|url=https://www.awm.gov.au/military-event/E342/|publisher=Australian War Memorial|accessdate=21 December 2015}}</ref>

Authors and military analysts have speculated that had Kingsbury not attacked, the Japanese might have destroyed the battalion.<ref name=Ham /><ref name=730Report>{{citation|last=Bannerman|first=Mark|url=http://www.abc.net.au/7.30/content/2002/s637883.htm|title=7.30 Report – 01/08/2002: Cosgrove calls for recognition for soldiers on Kokoda Trail|publisher=[[Australian Broadcasting Corporation]]|date=1 August 2002|accessdate=25 March 2009}}</ref> The Japanese had been attacking in waves, and had started to climb a steep hill to outflank the Australians, in an effort to win the battle.<ref name=Ham /> The Australians were low on supplies and the Japanese were on the verge of breaking through the Australian line. Had they broken through, they would have been able to isolate the battalion's headquarters from the soldiers on the flanks. This would have prevented the Australians from retreating to [[Alola, Papua New Guinea|Alola]], allowing the Japanese to overrun them.<ref name=Ham />

For his actions, Kingsbury was awarded the Victoria Cross,<ref>{{citation|last=|first=|url=http://www.awm.gov.au/honours/honours/person.asp?p=VX19139|title=Honours and awards (gazetted)|publisher=Australian War Memorial|date=|accessdate=7 March 2009}}</ref><ref name=JohnstonChagas>{{Harvnb|Johnston|Chagas|2007|p=23}}</ref> which was [[London Gazette|gazetted]] on 9 February 1943. His citation read:

{{quote|''War Office, 9th February, 1943.''

The KING has been graciously pleased to approve the posthumous award of the VICTORIA CROSS to: —

No. VX 19139 Private. Bruce Steel Kingsbury, Australian Military Forces.

In New Guinea, the Battalion to which Private Kingsbury belonged had been holding a position in the Isurava area for two days against continuous and fierce enemy attacks. On the 29th August, 1942, the enemy attacked in such force that they succeeded in breaking through the Battalion's right flank, creating a serious threat both to the rest of the Battalion and to its Headquarters. To avoid the situation becoming more desperate, it was essential to regain immediately the lost ground on the right flank.

Private Kingsbury, who was one of the few survivors of a Platoon which had been overrun and severely cut about by the enemy, immediately volunteered to join a different platoon which had been ordered to counter-attack.

He rushed forward firing his Bren Gun from the hip through terrific machine-gun fire and succeeded in clearing a path through the enemy. Continuing to sweep the enemy positions with his fire and inflicting an extremely high number of casualties on them, Private Kingsbury was then seen to fall to the ground shot dead, by the bullet from a sniper hiding in the wood.

Private Kingsbury displayed a complete disregard for his own safety. His initiative and superb courage made possible the recapture of the position which undoubtedly saved Battalion Headquarters, as well as causing heavy casualties amongst the enemy. His coolness, determination and devotion to duty in the face of great odds was an inspiration to his comrades.<ref name=LonGaz />}}

[[File:KingsburyfamilyVC.jpg|thumb|Kingsbury's family accepting the Victoria Cross on his behalf]]

Kingsbury was the first Australian soldier to be awarded the Victoria Cross for actions in the South Pacific and also the first on [[Territory of Papua|Australian territory]].{{sfn|Brune|2004|p=150}} Kingsbury's section remains the most highly decorated section in the British Empire, its members having received a Victoria Cross, one [[Distinguished Conduct Medal]] and four Military Medals by war's end;<ref name=Dornan5 /><ref name=Austin>{{Harvnb|Austin|1988|p=158}}</ref> the platoon also suffered the highest proportional losses in the Kokoda Campaign.{{sfn|Brune|McDonald|2005|p=59}} Kingsbury's platoon sergeant, Sergeant Robert Thompson, later commented that when he was recommending Kingsbury for the Victoria Cross, he was asked several times would he "please write it up a bit more with a bit more action and such"<ref name="film" /> and

{{quote|On the same day or the next day there was another chap named Charlie McCarthy [sic, McCallum], who really did something, probably far more deserving but they were only going to award one VC, so Bruce got it. I'm not decrying it. He was worded up and Charlie unfortunately was worded down …<ref name="film">{{citation|url=http://www.australiansatwarfilmarchive.gov.au/aawfa/interviews/1804.aspx|title=The Australians at War Film Archive|accessdate=18 March 2009}}</ref>}}

Kingsbury's Rock, the rock next to which Kingsbury died, stands within sight of where the 2/14th Battalion's headquarters had been established, and has been incorporated as part of the Isurava Memorial. His body now rests in the Bomana Cemetery, Port Moresby,<ref>{{citation|url=http://www.cwgc.org/search/casualty_details.aspx?casualty=2165531|title=Casualty details—Kingsbury, Bruce Steel|accessdate=7 March 2009|publisher=[[Commonwealth War Graves Commission]]}}</ref> and his Victoria Cross is on display at the [[Australian War Memorial]], [[Canberra]].<ref>{{citation|url=http://www.awm.gov.au/virtualtour/valour.asp|title=Hall of Valour: Victoria Crosses at the Memorial|accessdate=7 March 2009|publisher=Australian War Memorial}}</ref> The Melbourne suburb of [[Kingsbury, Victoria|Kingsbury]] was named in his honour,<ref name=adb /> as was a rest area on the [[Remembrance Driveway (Australia)|Remembrance Driveway]]<ref>{{citation|url=http://www.remembrancedriveway.org.au/vc-citations/default.asp?ID=Kingsbury|title=Victoria Cross Citations – The Remembrance Driveway – A Living Memorial|accessdate=26 April 2009|publisher=Remembrance Driveway Committee}}</ref> and a street in the Canberra suburb of [[Gowrie, Australian Capital Territory|Gowrie]].<ref>{{cite web|title=Kingsbury Street|url=http://www.actpla.act.gov.au/tools_resources/maps_land_survey/place_names/place_search?sq_content_src=%2BdXJsPWh0dHAlM0ElMkYlMkYyMDMuOS4yNDkuMyUyRlBsYWNlTmFtZXMlMkZQbGFjZURldGFpbHMuYXNweCUzRm9iamVjdElEJTNEODI5MiZhbGw9MQ%3D%3D|archiveurl=https://web.archive.org/web/20120407041558/http://www.actpla.act.gov.au/tools_resources/maps_land_survey/place_names/place_search?sq_content_src=%2BdXJsPWh0dHAlM0ElMkYlMkYyMDMuOS4yNDkuMyUyRlBsYWNlTmFtZXMlMkZQbGFjZURldGFpbHMuYXNweCUzRm9iamVjdElEJTNEODI5MiZhbGw9MQ%3D%3D|work=Place name search|publisher=ACT Planning and Land Authority|accessdate=18 June 2012|archivedate=7 April 2012}}</ref> Kingsbury's story was featured in the [[History (U.S. TV channel)|History Channel]] production ''For Valour''.<ref>{{cite AV media|chapter=Part Four|title=For Valour|chapterurl=https://www.youtube.com/watch?v=QfWzzTzgMjU|url=http://www.historychannel.com.au/tv-shows/showDetails.aspx?show=301|archiveurl=https://web.archive.org/web/20090418005219/http://www.historychannel.com.au/tv-shows/showDetails.aspx?show=301|publisher=[[History (U.S. TV channel)|History Channel]]|airdate=25 April 2009|archivedate=18 April 2009}}</ref>

==Notes==
{{Reflist|30em}}

== References ==
* {{cite book|last1=Austin|first1=Victor|title=To Kokoda and Beyond|publisher=Melbourne University Press|year=1988|isbn=0-522-84374-3 |ref=harv}}
* {{cite book|last1=Brune|first1=Peter|title=Those Ragged Bloody Heroes|publisher=Allen & Unwin|year=1992|isbn=1-86373-264-0 |ref=harv}}
* {{cite book|last1=Brune|first1=Peter|title=A Bastard of a Place|publisher=Allen & Unwin|year=2004|isbn=1-74114-403-5 |ref=harv}}
* {{cite book| last1=Brune|first1=Peter|last2=McDonald|first2=Neil|title=200 Shots|publisher=Allen & Unwin|year=2005|isbn=1-74114-631-3 |ref=harv}}
* {{cite book|last1=Dornan|first1=Peter|title=The Silent Men|publisher=Allen & Unwin|year=1999|isbn=1-86448-991-X |ref=harv}}
* {{cite book |last1=Ham|first1=Paul|authorlink=Paul Ham|title=Kokoda|publisher=Harper Collins Publishers|year=2004|isbn=0-7322-8232-2 |ref=harv}}
* {{cite book|last1=Johnston|first1=Mark|authorlink1=Mark Johnston (historian)|last2=Chagas|first2=Carlos|title=The Australian Army in World War II|publisher=Osprey Publishing|year=2007|isbn=1-84603-123-0 |ref=harv}}
* {{cite book|last=Keogh|first=Eustace|title=South West Pacific 1941–45|year=1965|publisher=Grayflower Publications|oclc=7185705|ref=harv}}
* {{cite book|last1=Lindsay|first1=Patrick|title=The Spirit of Kokoda: Then and Now|publisher=Hardie Grant Books|year=2002|isbn=1-74066-075-7 |ref=harv}}
* {{cite book|last=McCarthy|first=Dudley|title=South-West Pacific Area – First Year|series=Australia in the War of 1939–1945. Series 1 – Army|volume=Volume 5|publisher=Australian War Memorial|location=Canberra|year=1959|url=https://www.awm.gov.au/collection/RCDIG1070204/|oclc=3134247|ref=harv}}

==External links==
{{Commons category|Bruce Kingsbury}}
* [http://www.nationalarchives.gov.uk/theartofwar/valgal/valour/INF3_0460.htm Private B.S. Kingsbury] in ''The Art of War'' exhibition at [[The National Archives (United Kingdom)|The National Archives]] (United Kingdom)

{{Featured article}}

{{DEFAULTSORT:Kingsbury, Bruce Steel}}
[[Category:1918 births]]
[[Category:1942 deaths]]
[[Category:Australian Army soldiers]]
[[Category:Australian military personnel killed in World War II]]
[[Category:Australian World War II recipients of the Victoria Cross]]
[[Category:People from Melbourne]]