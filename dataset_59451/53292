{{Infobox football biography
| name                = Jason Kreis
| image               = Jason-Kreis Boz.jpg
| image_size          = 
| caption             = Kreis as Head Coach of [[Real Salt Lake]]
| fullname            = Jason Clarence Kreis
| birth_date          = {{birth date and age|1972|12|29}}
| birth_place         = [[Omaha, Nebraska]], United States
| height              = {{height|ft=5|in=8}}
| position            = [[Midfielder]], [[Forward (association football)|forward]]
| currentclub         = 
| collegeyears1         = 1991–1994
| college1         = [[Duke Blue Devils men's soccer|Duke Blue Devils]]
| years1              = 1993
| clubs1              = [[Raleigh Flyers (soccer)|Raleigh Flyers]]
| caps1               =
| goals1              =
| years2              = 1994
| clubs2              = [[New Orleans Storm]]
| caps2               =
| goals2              =
| years3              = 1995
| clubs3              = [[Raleigh Flyers (soccer)|Raleigh Flyers]]
| caps3               =
| goals3              = 10
| years4              = 1996–2004
| clubs4              = [[Dallas Burn]]
| caps4               = 247
| goals4              = 91
| years5              = 2005–2007
| clubs5              = [[Real Salt Lake]]
| caps5               = 58
| goals5              = 17
| totalcaps           = 305
| totalgoals          = 108
| nationalyears1      = 1996–2000
| nationalteam1       = [[United States men's national soccer team|United States]]
| nationalcaps1       = 14
| nationalgoals1      = 1
| medaltemplates      =
| manageryears1       = 2007–2013
| managerclubs1       = [[Real Salt Lake]]
| manageryears2       = 2015
| managerclubs2       = [[New York City FC]]
| manageryears3       = 2016
| managerclubs3       = [[United States men's national soccer team|United States]] (assistant)
| managerclubs4       = [[Orlando City SC]]
| manageryears4       = 2016–
}}

'''Jason Clarence Kreis''' (born December 29, 1972) is an American [[association football|soccer]] manager and former player. He coaches [[Orlando City SC]] in [[Major League Soccer]] and was previously an assistant coach under [[Jürgen Klinsmann]] for the [[United States men's national soccer team]].<ref name="Orlando">{{cite news |last=DelGallo |first=Alicia |date=July 19, 2016 |title=Orlando City hires former NYCFC coach Jason Kreis |url=http://www.orlandosentinel.com/sports/orlando-city-lions/os-orlando-city-hires-jason-kreis-20160719-story.html |work=[[Orlando Sentinel]] |accessdate=July 19, 2016}}</ref> Prior to that he was the head coach of [[New York City FC]] and [[Real Salt Lake]].

Kreis spent the majority of his playing career in MLS in the United States, initially with the Dallas Burn, and later with Real Salt Lake. In total he made over 300 professional appearances, was [[Major League Soccer MVP]] in 1999, and is currently tied for the fifth highest scorer in MLS regular season history, with 108 goals. He also earned fourteen international caps for the [[United States men's national soccer team|United States national team]].

==Early life==
Kreis played for the Gladiator Soccer Club in his hometown of [[Omaha, Nebraska]], where his parents were among the pioneers who established this first premier/select club within the state. As a freshman at [[Omaha Burke High School|Burke High School]] in Omaha, Kreis was selected First Team All-State.<ref>''NEBRASKA'' USA TODAY – Wednesday, June 8, 1988</ref>  He remains the only freshman selected to the first team. Following his freshman year, Kreis and his family moved to [[Mandeville, Louisiana]], where he led [[Mandeville High School]] to several Class 5A state soccer tournaments, but never won a state title, losing to [[Lafayette High School (Louisiana)|Lafayette High]] in the quarterfinals his senior year. While at Mandeville, he was also a standout performer for the Baton Rouge United Jags, a U-19 Select team that finished as national runner-up in 1991 and won the prestigious Capital Cup tournament in Washington, D.C. He played collegiately at [[Duke University]] in [[Durham, North Carolina]].

==Club career==

===Minor leagues===
In 1993, Kreis played during the summer for the [[Raleigh Flyers (soccer)|Raleigh Flyers]] of the [[USISL]].<ref>''Flyers top Dynamo 2–1 in a shootout'' The News & Observer – Wednesday, June 9, 1993</ref>  In the summer of 1994, he returned to Louisiana to play for the [[New Orleans Riverboat Gamblers]], also of the USISL.<ref>"KREIS LEADS GAMBLERS' ROUT, 5–1" ''Times-Picayune'' (New Orleans) – Saturday, July 9, 1994</ref>  After finishing his collegiate eligibility in 1994, Kreis signed his first professional contract with the Raleigh Flyers in the spring of 1995.

===Dallas Burn===
On August 1, 1995, Kreis signed a contract with [[Major League Soccer]] as the league prepared for its inaugural season.<ref>"Flyers' Kreis signs deal with Major League Soccer" ''The News & Observer'' Wednesday, August 2, 1995</ref> Kreis was drafted forty-third overall by the [[Dallas Burn]] in the fifth round of the [[MLS Inaugural Draft]]. He scored the first goal in Burn history. In 1999, Kreis was the first American-born player to be named [[MLS MVP|league MVP]] after he [[MLS Scoring Champion Award|led the league in points]] and [[MLS Golden Boot|goals]], and also registered the first 15-goal, 15-assist season in league history. The five-time all-star led his team in goals five times and in points four times.

On June 26, 2004, Kreis scored his eighty-ninth league goal against [[D.C. United]], moving past [[Roy Lassiter]] to the top of the league's all-time goal-scoring chart.{{citation needed|date=June 2015}} Kreis remained the MLS all time goalscoring leader for more than three years until [[Jaime Moreno]] surpassed his record on August 22, 2007. He ended his ninth MLS season with career totals of 91 goals and 65 assists for 247 points, plus added four goals and two assists in the playoffs. He remains Dallas' all-time leader in games played (247), goals (91), assists (65), and points (247).

===Real Salt Lake===

On November 17, 2004, Kreis was traded to expansion team [[Real Salt Lake]], becoming the first player in the club's history. Kreis scored the first goal in Salt Lake's history in its second match, a 3–1 loss to the [[Los Angeles Galaxy]].<ref>{{cite news |last=Edward |first=James |date=April 10, 2005 |title=Donovan shoots L.A. Galaxy over Real S.L. |url=http://www.deseretnews.com/article/600125221/Donovan-shoots-LA-Galaxy-over-Real-SL.html?pg=all |work=[[Deseret News]] |accessdate=July 12, 2016}}</ref> In doing so, he became the first player in league history to do so for two different teams, as he scored the inaugural goal for Dallas in 1996 as well.{{citation needed|date=July 2016}}

On August 13, 2005, he became the first player in MLS history to score 100 career league goals, during a 4–2 loss to the [[Kansas City Wizards]].<ref>{{cite news |last=Black |first=Michael |date=August 14, 2005 |title=Real notes: Hitting century mark a Real big deal for Kreis |url=http://www.deseretnews.com/article/600155819/Hitting-century-mark-a-Real-big-deal-for-Kreis.html?pg=all |work=Deseret News |accessdate=July 12, 2016}}</ref> The achievement was unexpected from observers, especially out of a 1996 class that included other candidates like [[Eric Wynalda]], [[Brian McBride]], or [[Joe-Max Moore]]; Kreis had never been considered a particularly dangerous goalscorer and had only lead the league in scoring once, in 1999.<ref>{{cite news |last=Connolly |first=Marc |date=August 14, 2005 |title=Kreis, the unexpected record holder |url=http://www.mlsnet.com/news/mls_news.jsp?ymd=20050814&content_id=39889&vkey=news_mls&fext=.jsp |deadurl=yes |archiveurl=https://web.archive.org/web/20070520081320/http://www.mlsnet.com/news/mls_news.jsp?ymd=20050814&content_id=39889&vkey=news_mls&fext=.jsp |archivedate=May 20, 2007 |work=MLSnet.com |accessdate=July 12, 2016}}</ref>

Kreis was left unprotected in the [[2006 MLS Expansion Draft]] by Real Salt Lake and subsequently chosen by [[Toronto FC]]. He was reacquired by Salt Lake on November 17, 2006, with the club sending partial [[allocation money]] to Toronto in exchange.<ref>{{cite press release |date=November 17, 2006 |title=Real Salt Lake reacquires rights to captain Jason Kreis |url=http://real.saltlake.mlsnet.com/news/team_news.jsp?ymd=20061117&content_id=79039&vkey=pr_rsl&fext=.jsp&team=t121 |deadurl=yes |archiveurl=https://web.archive.org/web/20070506175916/http://real.saltlake.mlsnet.com/news/team_news.jsp?ymd=20061117&content_id=79039&vkey=pr_rsl&fext=.jsp&team=t121 |archivedate=May 6, 2007 |publisher=Real Salt Lake |accessdate=July 11, 2016}}</ref><ref>{{cite news |date=November 17, 2006 |title=MLS all-time scoring leader Kreis chosen by Toronto |url=http://espn.go.com/espn/wire?section=soccer&id=2666395 |agency=Associated Press |publisher=ESPN |accessdate=July 12, 2016}}</ref>

He retired as a player on May 3, 2007, to take over as head coach, finishing with 108 career goals.

==International career==
With only 14 international appearances,<ref>{{cite book |last=Lodes |first=Kirk J. |year=2008 |title=The American Soccer Guide |page=70 |publisher=Infosential Press |location=Mobile, Alabama |oclc=213482683 |url=https://books.google.com/books?id=IEtLjtqaCTgC |via=Google Books |accessdate=July 12, 2016}}</ref> Kreis was never able to translate his club success into one with the [[United States men's national soccer team|U.S. national team]]. He received his first [[Cap (sport)|cap]] on August 30, 1996, against [[El Salvador national football team|El Salvador]] at the [[Los Angeles Memorial Coliseum]].<ref>{{cite news |date=August 31, 1996 |title=International Friendly: USA 3, El Salvador 1 (Aug. 30) |url=http://www.socceramerica.com/article/16235/international-friendly-usa-3-el-salvador-1-aug.html |work=[[Soccer America]] |accessdate=July 12, 2016}}</ref> Kreis scored his only international goal on September 8, 1999 in a 2–2 draw against Jamaica in [[Kingston, Jamaica|Kingston]].<ref name="Jamaica">{{cite news |date=September 9, 1999 |title=Late goal gives U.S. tie with Jamaica |url=http://sportsillustrated.cnn.com/soccer/news/1999/09/08/us_jamaica_ap/ |work=[[Sports Illustrated]] |deadurl=yes |archiveurl=https://web.archive.org/web/20121102232204/http://sportsillustrated.cnn.com/soccer/news/1999/09/08/us_jamaica_ap/ |archivedate=November 2, 2012 |accessdate=July 12, 2016}}</ref>

===International goals===
{{International goals header}}
|-
|1.
|September 8, 1999
|[[Independence Park (Jamaica)|National Stadium]], [[Kingston, Jamaica]]
|{{fb|Jamaica}}
|align="center"|'''1''' – 0
|align="center"|2–2
|align="center"|[[Exhibition game|Friendly]]
|align="center"|<ref name="Jamaica"/>
|}

==Coaching career==

===Real Salt Lake===
On May 3, 2007 Kreis retired from professional competition and was announced as the new head coach of [[Real Salt Lake]], retiring as a player and taking over the coaching reins from [[John Ellinger]].<ref>[http://real.saltlake.mlsnet.com/news/team_news.jsp?ymd=20070503&content_id=91741&vkey=pr_rsl&fext=.jsp&team=t121 Real Salt Lake captain Jason Kreis retires, named head coach]</ref> At the time of his hire, Kreis became the youngest active head coach in MLS at 34 years and 127 days. He led Real Salt Lake to its first-post season playoff appearance in 2008 and in 2009 he coached the team to its first [[2009 MLS Cup|MLS Cup]] championship. Kreis is the youngest coach in MLS history to win the MLS Cup. Following the 2009 [[2009 MLS Cup|MLS Cup]] win, Kreis would lead Real Salt Lake to playoff appearances in every subsequent season through 2013, including an appearance in the 2013 [[2013 MLS Cup|MLS Cup]], which RSL lost to [[Sporting Kansas City]] in dramatic fashion on penalty kicks. Kreis also guided Real Salt Lake to a very good CONCACAF Champions League campaign in 2011. Real Salt Lake won Group A and made it all the way to the final, only to lose to 3-2 on aggregate to [[C.F. Monterrey]].

Real Salt Lake signed Kreis to a contract extension on March 24, 2011 that kept him at the RSL helm through the 2013 season, after which he left RSL to become head coach of [[New York City FC]]. Real Salt Lake has retired Kreis' number 9 jersey from his playing days.<ref>{{cite web|url=http://www.mlssoccer.com/news/article/real-salt-lake-extend-kreis-contract-through-2013 |title=Real Salt Lake extend Kreis' contract through '13 |first=Randy |last=Davis |work=MLSSoccer.com |date=March 25, 2011 |accessdate=April 24, 2011}}</ref>

===New York City FC===
On December 11, 2013, Kreis was announced as the first head coach of [[New York City FC]], having reached the end of his contract at [[Real Salt Lake]] and declining an extension offer from RSL so he could take the New York City FC position.<ref name="Kreis">{{cite news |url=http://www.nycfc.com/News/Latest-News/2013/December/Head-Coach? |title=Jason Kreis named as Head Coach |date=December 11, 2013 |accessdate=December 11, 2013 |work=nycfc.com }}</ref> The move came just four days after he missed out on lifting his second [[2013 MLS Cup|MLS Cup]] with the [[Utah]] team, losing on penalty kicks to [[Sporting Kansas City]]. It was revealed in the announcement that his contract, starting on January 1, 2014, would see him begin by travelling to [[Manchester]] in England to familiarise himself with the set-up of franchise-owners [[Manchester City F.C.|Manchester City]].<ref name="Kreis"/>

Kreis' [[2015 New York City FC season|inaugural season]] with NYCFC ended in disappointment. The club finished with a 10–17–7 record in the [[2015 Major League Soccer season|regular season]] placing 8th in the Eastern Conference, failing to make the [[2015 MLS Cup Playoffs]]. He and the club parted ways on November 2, 2015.<ref>{{cite news |url=http://www.mlssoccer.com/post/2015/11/02/new-york-city-fc-part-ways-head-coach-jason-kreis-after-missing-playoffs-inaugural|title=New York City part ways with head coach Jason Kreis|date=November 2, 2015 |accessdate=November 2, 2015 |work=mlssoccer.com }}</ref>

===Orlando City SC===

On July 19, 2016, Kreis was hired by [[Orlando City SC]] to replace outgoing inaugural coach [[Adrian Heath]]. He will reunite with two of his assistant coaches, [[Miles Joseph]] and [[C. J. Brown]], at Orlando.,<ref name="Orlando"/> he also led them to Orlando City's first win in two months.

==Career statistics==

===Club career statistics===
{| class="wikitable" style="text-align:center"
|-
! colspan=3 | Club performance
! colspan=2 | League
! colspan=2 | Cup
! colspan=2 | League Cup
! colspan=2 | Continental
! colspan=2 | Total
|-
! Season !! Club !! League
! Apps !! Goals
! Apps !! Goals
! Apps !! Goals
! Apps !! Goals
! Apps !! Goals
|-
|-
! colspan=3 | USA
! colspan=2 | League
! colspan=2 | [[Lamar Hunt U.S. Open Cup|Open Cup]]
! colspan=2 | League Cup
! colspan=2 | [[CONCACAF|North America]]
! colspan=2 | Total
|-
|[[1996 Major League Soccer season|1996]]||rowspan="9"|[[FC Dallas]]||rowspan="12"|[[Major League Soccer|MLS]]||31||13||0||0||2||1||0||0||33||14
|-
|[[1997 Major League Soccer season|1997]]||32||8||2||0||4||1||0||0||38||9
|-
|[[1998 Major League Soccer season|1998]]||30||9||2||0||2||1||0||0||34||10
|-
|[[1999 Major League Soccer season|1999]]||32||18||1||1||6||1||0||0||39||20
|-
|[[2000 Major League Soccer season|2000]]||27||11||3||1||2||0||0||0||32||12
|-
|[[2001 Major League Soccer season|2001]]||25||7||1||0||3||0||0||0||29||7
|-
|[[2002 Major League Soccer season|2002]]||27||13||3||0||3||1||0||0||33||14
|-
|[[2003 Major League Soccer season|2003]]||18||7||1||0||0||0||0||0||19||7
|-
|[[2004 Major League Soccer season|2004]]||25||5||1||0||0||0||0||0||26||5
|-
|[[2005 Major League Soccer season|2005]]||rowspan="3"|[[Real Salt Lake]]||24||9||0||0||0||0||0||0||24||9
|-
|[[2006 Major League Soccer season|2006]]||30||8||0||0||0||0||0||0||30||8
|-
|[[2007 Major League Soccer season|2007]]||4||0||0||0||0||0||0||0||4||0
|-
! rowspan=1 | Total
! colspan=2 | USA
!305||108||14||2||22||5||0||0||341||115
|-
! colspan=3 | Career total
!305||108||14||2||22||5||0||0||341||115
|}

:''Source: MLS''<ref>{{cite web |title=Jason Kreis |url=http://www.mlssoccer.com/players/jason-kreis |deadurl=yes |archiveurl=https://web.archive.org/web/20150930111415/http://www.mlssoccer.com/players/jason-kreis |archivedate=September 30, 2015 |publisher=Major League Soccer |accessdate=July 20, 2016}}</ref>

===Coaching record===
{{updated|September 11, 2016}}
{| class="wikitable" style="text-align: center"
|-
!rowspan=2|Team
!rowspan=2|From{{ref|a|1}}
!rowspan=2|To{{ref|a|2}}
!colspan=6|Record{{ref|a|3}}
|-
!G!!W!!L!!D!!Win %
|-
|[[Real Salt Lake]]
|align=left|May 3, 2007
|align=left|Dec 10, 2013
{{WDL|261|112|85|64}}
|-
|[[New York City FC]]
|align=left|January 1, 2014
|align=left|November 2, 2015
{{WDL|35|10|18|7}}
|-
|[[Orlando City SC]]
|align=left|July 19, 2016
|align=center|—
{{WDL|8|3|3|2}}
|-
!colspan=3|Total
{{WDLtot|304|125|106|73}}
|}

*1.{{note|a}}Date hired.
*2.{{note|a}}Date fired or indication he is the current head coach.
*3.{{note|a}}Includes [[Major League Soccer|league]], [[Lamar Hunt U.S. Open Cup|U.S. Open Cup]], [[MLS Cup Playoffs]] and [[CONCACAF Champions League]] games.

==See also==
* [[List of Major League Soccer coaches]]

==References==
{{reflist}}

==External links==
* [http://www.mlssoccer.com/players/jason-kreis MLS player profile]

{{Navboxes colour
|title= Awards
|bg=gold
|fg=navy
|list1=
{{Major League Soccer MVP Award}}
{{MLS top scorers}}
}}
{{Navboxes
|title=Jason Kreis managerial positions
|list1=
{{Real Salt Lake managers}}
{{New York City FC managers}}
{{Orlando City SC managers}}
}}
{{Orlando City SC squad}}
{{Major League Soccer head coaches}}

{{DEFAULTSORT:Kreis, Jason}}
[[Category:1972 births]]
[[Category:Living people]]
[[Category:American soccer coaches]]
[[Category:American soccer players]]
[[Category:FC Dallas players]]
[[Category:Duke Blue Devils men's soccer players]]
[[Category:New Orleans Riverboat Gamblers players]]
[[Category:New York City FC coaches]]
[[Category:Sportspeople from Omaha, Nebraska]]
[[Category:Raleigh Flyers players]]
[[Category:Real Salt Lake players]]
[[Category:Real Salt Lake coaches]]
[[Category:Soccer players from Nebraska]]
[[Category:United States men's international soccer players]]
[[Category:Major League Soccer players]]
[[Category:Major League Soccer All-Stars]]
[[Category:Major League Soccer MVPs]]
[[Category:Orlando City SC coaches]]
[[Category:Association footballers not categorized by position]]
[[Category:People from Mandeville, Louisiana]]