{{redirect|Gawler}}
{{refimprove|date=April 2016}}
{{Use Australian English|date=March 2013}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type          = town
| name          = Gawler
| state         = sa
| image         = 
| caption       = 
| lga           = Town of Gawler
| postcode      = 5118
| latd          = 34
| latm          = 35
| lats          = 53
| longd         = 138
| longm         = 44
| longs         = 42
| alternative_location_map = Australia Greater Adelaide
| pop           = 23957
| pop_year      = {{CensusAU|2011}}
| pop_footnotes = <ref name=2011pop>{{Census 2011 AUS|id=UCL412001|name=Gawler (Urban Centre/Locality)|accessdate=6 March 2016|quick=on}}</ref>
| est           = 1836
| elevation     = 75
| maxtemp       = 22.4
| mintemp       = 10.3
| rainfall      = 440.3
| stategov      = [[Electoral district of Light|Light]]
| fedgov        = [[Division of Wakefield|Wakefield]]
| dist1         = 40
| dir1          = N
| location1     = Adelaide city centre
| near-n             = Willaston
| near-ne            = Willaston
| near-e             = [[Gawler East, South Australia| Gawler East]]
| near-se            = Gawler East	
| near-s             = [[Gawler South, South Australia| Gawler South]]
| near-sw            = Willaston
| near-w             = [[Willaston, South Australia| Willaston]]
| near-nw            = Willaston
|footnotes=Adjoining localities<ref name=PLB>{{cite web|title=Search result for " Gawler (Suburb)" (Record no SA0025581) with the following layers selected - “Suburbs and Localities” and “Local Government Areas” |url=http://maps.sa.gov.au/plb/#|publisher=[[Department of Planning, Transport and Infrastructure]] |date= |accessdate=16 April 2016}}</ref>
}}
'''Gawler''' is the first country town in the state of [[South Australia]], and is named after the second [[Governor of South Australia|Governor]] (British Vice-Regal representative) of the colony of South Australia, [[George Gawler]].<ref>[http://shawfactor.com/gazetteer/south-australia/gawler/ History of Gawler] 'Shawfactors'</ref> It is about  {{convert|40|-|44|km|mi|abbr=on}} north of the [[Adelaide city centre|centre]] of the state capital, [[Adelaide]], and is close to the major wine producing district of the [[Barossa Valley (wine)|Barossa Valley]]. Topographically, Gawler lies at the confluence of two tributaries of the [[Gawler River (South Australia)|Gawler River]], the [[North Para River|North]] and [[South Para River|South]] Para rivers, where they emerge from a range of low hills. It is now a suburb of the Greater Adelaide metropolitan area.

==History==
[[File:Gawler around 1869.jpg|thumb|left|Gawler in around 1869]]
<!-- Deleted image removed: [[File:Satellite image of Adelaide South Australia.jpg|thumb|right|Location of Gawler in relation to Adelaide city centre]] -->
A [[United Kingdom|British]] colony, South Australia was established as a commercial venture by the [[South Australia Company]] through the sale of land to free settlers at £1 per acre (£2/9/5d per hectare). Gawler was established through a {{convert|4,000|acre|ha|adj=on}} "special survey" applied for by Henry Dundas Murray and John Reid and a syndicate of ten other colonists.

The town plan was devised by the colonial surveyor [[William Light]], and was the only town planned by him other than Adelaide. William Jacob used Light's plans and laid out the town.

Adelaide became a model of foresight with wide streets and ample parklands. After Light's death, it also became a model for numerous other planned towns in South Australia (many of which were never built). As the only other town planned by Light, Gawler is dissimilar to Adelaide's one square mile (2.6&nbsp;km²) grid; the heart of Gawler is triangular rather than square, a form dictated by the topographical features. The parkland along the riverbanks and a [[Victorian era|Victorian]] preference for public squares are present, but Light was aware that he was planning a village, not a metropolis.

Gawler prospered early with the discovery of copper nearby at [[Kapunda, South Australia|Kapunda]] and [[Burra, South Australia|Burra]], which resulted in Gawler becoming a resting stop to and from Adelaide. Later, it developed industries including flour milling by Hilfers & Co, and the engineering works of [[James Martin & Co]] manufactured agricultural machinery, mining and ore-processing machinery and smelters for the mines of Broken Hill and the Western Australian goldfields, and steam locomotives and rolling stock. [[May Brothers and Company|May Brothers & Co.]] also manufactured mining and agricultural machinery. <ref>[http://trove.nla.gov.au/ndp/del/article/92302648 Australasian Institute of Mining Engineers visit to Gawler] ''South Australian Chronicle'', 15 April 1893. Pp 7-8. Accessed 17 April 2015.</ref>

With prosperity came a modest cultural flowering, ("The colonial Athens" was its nickname in the late 19th and early 20th centuries<ref>[http://trove.nla.gov.au/ndp/del/article/42985131 New Council Chambers, Gawler] ''South Australian Register'' 18 April 1878 p.6 accessed 10 March 2011</ref>), the high point of which was the holding of a competition to compose an anthem for Australia in 1859, four decades before nationhood. The result was the [[Song Of Australia]], written by [[Caroline Carleton]] to music by [[Carl Linger]]. This became, in the next century, a candidate in a national referendum to choose a new National Anthem for [[Australia]] to replace ''God Save the Queen''.

Gawler had a horse street tram service from 1879 to 1931.<ref>[[Australian Railway History|Australian Railway Historical Society Bulletin]], August/September 1950 pp55-56/75-76</ref>

==Culture==
Gawler is a commercial centre for the Mid-North districts of South Australia and, increasingly, a dormitory town for Adelaide.

==Transport==
Gawler is just over forty kilometres north of [[Adelaide city centre]] along [[Main North Road]]. Main North Road was the historic road to the [[Mid North]] region of South Australia. North of Gawler, the road is now known as the [[Horrocks Highway]]. The [[Sturt Highway]] runs northeast from the north side of Gawler, leading to [[Nuriootpa, South Australia|Nuriootpa]], the [[Riverland]], [[Mildura, Victoria|Mildura]] and [[Sydney]]. The [[Barossa Valley Way]] runs east from the centre of Gawler into the [[Barossa Valley]], and was the original route of the Sturt Highway. The [[Thiele Highway]] leads north between the Horrocks and Sturt Highways to [[Freeling, South Australia|Freeling]], [[Kapunda, South Australia|Kapunda]] and [[Morgan, South Australia|Morgan]]. The [[Max Fatchen Expressway]] is a new highway to the southwest providing a bypass of Gawler as part of the [[North–South Corridor, Adelaide]] which will eventually provide a non-stop road from south of Adelaide to Nuriootpa.

[[Gawler railway station]] was the terminus of the [[Gawler railway line|railway from Adelaide]] from 1857. The railway was [[Morgan railway line|extended to Kapunda]] in 1860. Gawler became a junction station when a branch was constructed [[Barossa Valley railway line|into the Barossa Valley]] in 1911. This is the line that provides the [[Gawler Oval railway station|Gawler Oval]] and [[Gawler Central railway station|Gawler Central]] (originally named North Gawler) railway stations in Gawler. Gawler Central is now the terminus of the metropolitan rail services from Adelaide.

[[File:Tram Bridge, Gawler B-19378.jpeg|thumb|Horse Tram crossing the bridge in Gawler]]
Gawler's horse-drawn tram service opened in 1879. It operated for both goods and passengers from the railway station along what is now Nineteenth Street (then known as part of Murray Street) and Murray Street (the town's main street) to a terminus near where the Gawler Central station is now. It passed the [[James Martin & Co]] engineering factory, providing a convenient way to deliver heavy equipment such as locomotives manufactured there. Broad gauge locomotives were taken directly on the tramway, narrow gauge were transported on specially-built flat-bed trucks.<ref>{{cite web |url=https://www.flickr.com/photos/gawler_history/9252034333/in/album-72157634578761298/ |page=3 |title=Gawler Tramway |work=Trolley Wire Magazine |date=December 1982 |via=Gawler History Flickr |accessdate=18 April 2016}}</ref> There were also sidings at [[May Brothers and Company]], Roedigers, and Dowson's Mill.<ref>{{cite web |url=https://www.flickr.com/photos/gawler_history/9252034179/in/album-72157634578761298/ |title=Gawler Tramway |work=Trolley Wire Magazine |date=December 1982 |via=Gawler History Flickr |accessdate=18 April 2016}}</ref> The tram closed in 1931 replaced by a bus, and the tracks lifted soon after.<ref>{{cite news |url=http://nla.gov.au/nla.news-article58843931 |title=GAWLER LANDMARK |newspaper=[[The Mail (Adelaide)]] |volume=20, |issue=989 |location=South Australia |date=9 May 1931 |accessdate=18 April 2016 |via=National Library of Australia}}</ref><ref>[[Australian Railway History|Australian Railway Historical Society Bulletin]], August/September 1950 pp55-56/75-76</ref>

The tram route is now part of [[Adelaide Metro]] bus route number 491.<ref>{{cite web |url=https://www.adelaidemetro.com.au/routes/491 |title=Route 491 - Hewett Circuit |accessdate=18 April 2016}}</ref>

==Notable people==
[[File:2010 0119 Tour Down Under Murray Street Gawler (9) (19332572883).jpg|right|thumb|Current local federal MP for [[Division of Wakefield|Wakefield]] [[Nick Champion]], [[Premier of South Australia|Premier]] [[Mike Rann]], [[Prime Minister of Australia|Prime Minister]] [[Kevin Rudd]] and current local state MP for [[Electoral district of Light|Light]] [[Tony Piccolo]] in Gawler for the [[Tour Down Under]] in 2010.]]
*[[Jack Bobridge]], Australian Olympic cycling medallist
*[[Cecil Hincks]], politician
*[[Leslie Duncan]], politician
*[[Darren Lehmann]], Former Australian cricketer, was born in Gawler in 1970
*[[Lyn Lillecrapp]], Paralympic swimmer
*[[Riley McGree]], Soccer player for [[Adelaide United]]
*[[Lisa Ondieki]], Long distance runner and Olympic silver medallist
*[[Justin Kurzel]], film director

==See also==
* [[Town of Gawler]] (local government)
* [[List of locomotive builders]]

==References==
{{reflist}}

==External links==
{{commonscat|Gawler, South Australia}}
* [http://www.gawler.sa.gov.au/page.aspx Town of Gawler website]
*[http://gawler.nowandthen.net.au/index.php?title=Main_Page Gawler Now and Then]

{{Town of Gawler suburbs}}

{{Authority control}}
[[Category:Suburbs of Adelaide]]
[[Category:Towns in South Australia]]
[[Category:1836 establishments in Australia]]