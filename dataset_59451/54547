{{Infobox book
| name          = Le Colonel Chabert
| title_orig    = Le Colonel Chabert
| translator    = 
| image         = BalzacChabert01.jpg
| caption = 
| author        = [[Honoré de Balzac]]
| illustrator   = 
| cover_artist  = [[Édouard Toudouze]]
| country       = France
| language      = French
| series        = ''[[La Comédie humaine]]''
| subject       = 
| genre         = ''Scènes de la vie privée''
| publisher     = Mame-Delaunay
| pub_date      = 1829
| english_pub_date = 
| media_type    = 
| pages         = 
| isbn          = 
| oclc          = 
| preceded_by   = [[Le Père Goriot]]
| followed_by   = [[La Messe de l'athée]]
}}
'''''Le Colonel Chabert''''' (English: ''Colonel Chabert'') is an 1832 [[novella]] by French [[novelist]] and [[playwright]] [[Honoré de Balzac]] (1799–1850). It is included in his [[Novel sequence|series of novels]] (or ''Roman-fleuve'') known as ''[[La Comédie humaine]]'' (''The Human Comedy''), which depicts and parodies French [[society]] in the period of the [[Bourbon Dynasty, Restored|Restoration]] (1815–1830) and the [[July Monarchy]] (1830–1848). This novella, originally published in ''[[Le Constitutionnel]]'', was adapted for six different motion pictures, including two silent films.

==Plot summary==

Colonel Chabert marries Rose Chapotel, a prostitute. Colonel Chabert then becomes a French [[cavalry]] [[Military officer|officer]] who is held in high esteem by [[Napoleon Bonaparte]]. After being severely wounded in the [[Battle of Eylau]] (1807), Chabert is recorded as dead and buried with other French casualties. However, he survives and after extricating himself from his own grave is nursed back to health by local peasants. It takes several years for him to recover. Returning to Paris, he discovers his widow has married the social climber, Count Ferraud, and has liquidated all of Chabert's belongings. Seeking to regain his name and monies that were wrongly given away as inheritance, he hires Derville, an advocate, to win back his money and his honour. Derville, who also represents the Countess Ferraud, warns Chabert against accepting a settlement bribe from the Countess. In the end, Chabert walks away empty-handed and spends the rest of his days in an asylum.

==Themes==
In ''Le Colonel Chabert'' Balzac juxtaposes two world-views:  the Napoleonic value-system, founded on honour and military valour;  and that of the [[Bourbon Restoration|Restoration]].   Chabert was not killed at the Battle of Eylau, though it was thought that he was.   He struggles back to life, but cannot reclaim his identity.   His “widow”, who is actually his wife, and who fittingly was a prostitute in her early adult years, is now the Comtesse Ferraud, married (or so it would seem) to an important Restoration nobleman and politician.   She repudiates her “former” husband (just as Ferraud, in changed political circumstances, would now be happy to repudiate her). All that matters in the modern era is social rank based upon the possession of money, especially inherited wealth.

This theme of the trenchant purity of the military way of life is something to which Balzac returns in ''[[La Rabouilleuse]]'', but there the subject is treated quite differently.

==Characters==
* Hyacinthe Chabert, Colonel
* Countess Ferraud (formerly Chabert)
* Count Ferraud
* Derville
* Bouchard
* Godeschal
* Desroches
* Simonin
* Boutin
* Chamberlain
* Delbecq
* A Notary

==Film adaptations==
* 1911: ''Le Colonel Chabert''. France. Directed by [[André Calmettes]] and [[Henri Pouctal]].
* 1920: ''Il Colonnello Chabert''. Italy. Directed by [[Carmine Gallone]]. Starring [[Charles Le Bargy]] and Rita Pergament.
* 1932: ''[[Man Without a Name (1932 film)|Man Without a Name]]'' (''Mensch ohne Namen''). Germany. Directed by [[Gustav Ucicky]]. Starring [[Werner Krauss]], Mathias Wieman, Hans Brausewetter and Helene Thimig.
* 1943: ''[[Colonel Chabert (1943 film)|Colonel Chabert]]'' (''Le Colonel Chabert''). France. Directed by [[René Le Hénaff]]. Starring [[Raimu]].
* 1978: ''[[:ru:Полковник Шабер (фильм, 1978)|Colonel Chabert]]'' (''Полковник Шабер''). Russia. Starring [[Vladislav Strzhelchik]], [[Oleg Basilashvili]], [[Mikhail Boyarsky]].
* 1994: ''[[Colonel Chabert (1994 film)|Colonel Chabert]]'' (''Le Colonel Chabert''). France. Directed by [[Yves Angelo]].

==See also==
{{portal|Novels}}
* [[Honoré de Balzac]]
* [[La Comédie humaine]]
* [[List of characters of La Comédie humaine]]

==Footnotes==
{{Reflist}}

==External links==
* [http://www.cs.cmu.edu/~spok/metabook/humancomedy.html The Human Comedy by Honoré de Balzac] hosted by [[Carnegie Mellon University]].
* [http://onlinebooks.library.upenn.edu/webbin/gutbook/lookup?num=1954 Colonel Chabert by Honoré de Balzac] from Project Gutenberg (English translation by Ellen Marriage and Clara Bell), hosted by the [[University of Pennsylvania]]
* {{fr icon}} [http://www.litteratureaudio.com/index.php?s=Le+Colonel+Chabert&sbutt=Ok/ Le Colonel Chabert, audio version] [[Image:Speaker Icon.svg|20px]]

{{Honoré de Balzac}}

{{Authority control}}
{{DEFAULTSORT:Colonel Chabert, Le}}
[[Category:Books of La Comédie humaine]]
[[Category:1832 novels]]
[[Category:French novels adapted into films]]
[[Category:Napoleonic War novels]]
[[Category:Works originally published in Le Constitutionnel]]