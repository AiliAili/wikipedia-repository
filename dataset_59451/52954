{{Italic title}}
{{Infobox journal
| title = Texas Review of Entertainment & Sports Law
| cover =
| editor = Arthur Reyna
| discipline = [[Law]]
| formernames =
| abbreviation = Tex. Rev. Entertain. Sports Law
| publisher = [[University of Texas School of Law]]
| country = 
| frequency = Biannual
| history = 2000-
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.utexas.edu/law/journals/tresl/index.html
| link1 = 
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 45176548
| LCCN = 2001214515
| CODEN =
| ISSN = 1533-1903
| eISSN =
}}
The '''''Texas Review of Entertainment & Sports Law''''' is a student-edited biannual [[law review]] at the [[University of Texas School of Law]]. It covers issues related to law that affects the entertainment and sports industries.<ref>[http://www.utexas.edu/law/journals/tresl/index.html Official website]</ref> Its [[Bluebook]] abbreviation is ''Tex. Rev. Ent. & Sports L.''.

== History ==

The journal began in  1997, consisting only of student notes included as a supplement to the ''The State Bar of Texas Entertainment and Sports Law Journal'', published by the  Entertainment and Sports Law Section of the [[State Bar of Texas]]. It became an independent journal in 2000.<ref>{{Citation
  | last = Nebgen
  | first = Stephen Wade
  | title = Ten years later
  | journal = Tex. Rev. Ent. & Sports L.
  | volume = 11
  | pages = 175
  | date = Spring 2010
   }}</ref>

== Symposia ==
On March 25, 2010, the journal held its first annual [[academic conference|symposium]]. The subject was the expiring [[collective bargaining agreement]]s in the [[National Basketball Association]] and the [[National Football League]].<ref>{{cite web
| url         = http://utdirect.utexas.edu/localn/evnt.WBX?calendar_id=201003252164
| title       = UT Law Events Calendar
| date        = 15 March 2010
| publisher   = The University of Texas School of Law
| accessdate  = 9 November 2010
}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.utexas.edu/law/journals/tresl/index.html}}
* [http://www.teslaw.org/ The State Bar of Texas Entertainment and Sports Law Section website]

{{DEFAULTSORT:Texas Review of Entertainment and Sports Law}}
[[Category:American law journals]]
[[Category:University of Texas at Austin]]
[[Category:Publications established in 2000]]
[[Category:Biannual journals]]
[[Category:English-language journals]]
[[Category:Entertainment law]]