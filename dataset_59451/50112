{{Infobox language family
|name=Abazgi
|altname=Abkhaz–Abaza
|region=[[Caucasus]]
|familycolor=Caucasian
|family=[[Northwest Caucasian languages|Northwest Caucasian]]
|protoname=[[Proto-Abazgi language|Proto-Abazgi]]
|child1=[[Abaza language|Abaza]]
|child2=[[Abkhaz language|Abkhaz]]
|glotto=abkh1243
|glottorefname=Abkhaz–Abaza
|map=Caucasic languages.svg
|mapcaption={{legend|#FF6C60|Abazgi}}
}}

'''Abazgi''' is the branch of the [[Northwest Caucasian languages]] that contains the [[Abaza language|Abaza]] and [[Abkhaz language|Abkhaz]] languages. "Abazgi" was once the preferred designation, but has now been replaced by "Abkhaz–Abaza".

The literary dialects of Abkhaz and Abaza are two ends of a [[dialect continuum]]. Grammatically, the two are very similar; however, the differences in phonology are substantial, and are the main reason many linguists prefer to classify them as distinct languages. Most linguists (see for instance [[Viacheslav Chirikba]] 2003) believe that [[Ubykh language|Ubykh]] is the closest relative of the Abazgi dialect continuum.

==References==
{{reflist}}

==Bibliography==
*Wixman, Ronald.  ''The Peoples of the USSR''. p. 2
*[[Viacheslav Chirikba]] (2003) 'Abkhaz'. – Languages of the World/Materials 119. Muenchen: Lincom Europa.
{{Northwest Caucasian languages}}
{{Languages of the Caucasus}}

[[Category:Ethnic groups in Russia]]
[[Category:Ethnic groups in Abkhazia]]
[[Category:Northwest Caucasian languages]]