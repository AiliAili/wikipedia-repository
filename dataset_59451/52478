{{Infobox journal
| title = Organization & Environment
| cover = [[File:Organization & Environment.tif]]
| editor = J. Alberto Aragon-Correa, Mark Starik
| discipline = [[Sustainability|Sustainability management and policy]]
| former_names = 
| abbreviation = Organ. Environ.
| publisher = [[Sage Publications]]
| country = 
| frequency = Quarterly
| history = 1987-present
| openaccess = 
| license = 
| website = http://oae.sagepub.com/
| link1 = http://oae.sagepub.com/content/current
| link1-name = Online access
| link2 = http://oae.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1086-0266
| eISSN = 1552-7417
| OCLC = 300182389
| LCCN = 97652883
| CODEN = ORENFX
}}
'''''Organization & Environment''''' (''O&E'') is a [[Peer review|peer-reviewed]] [[academic journal]] that covers the fields of "sustainability management, policy and related social science".<ref>[http://oae.sagepub.com/site/includefiles/Organization_and_Environment_Call_for_Papers_rev.pdf "Call for Papers," ''Organization & Environment'']. Accessed: December 2, 2012.</ref> The [[editors-in-chief]] are J. Alberto Aragon-Correa ([[University of Granada]]) and Mark Starik ([[San Francisco State University]]). The journal was established in 1987 and is published by [[Sage Publications]]; it is sponsored by the Group of Research on Organizations and the Natural Environment (GRONEN).<ref>[http://oae.sagepub.com/ Official webpage.] Accessed: December 2, 2012.</ref>

== Abstracting and indexing ==
''Organization & Environment'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. In 2014 the journal had its impact factor suppressed due to anomalous self-citation patterns <ref>{{cite web|url=http://ipscience-help.thomsonreuters.com/incitesLiveJCR/JCRGroup/titleSuppressions.html|accessdate=23 June 2015}}</ref>

== History ==

Under the editorship of founding co-editor John M. Jermier and a number of editors, the journal took an interdisciplinary direction, playing host to a variety of perspectives including critical organization theory and radical ecology. Collaboration between members of the Organization and Natural Environment section of the [[Academy of Management]] and the Section on Environment and Technology section of the [[American Sociological Association]] was at the heart of the journal's framework. During this period ''O&E'' became a prominent outlet for [[environmental sociology]], publishing articles both from established and world-renowned environmental thinkers and younger scholars who would later become leaders in environmental sociology. The journal played an important role in increasing the influence of environmental sociology within the larger discipline and by 2011 its impact factor had grown to rival that of distinguished general interest journals such as ''[[Social Problems (journal)|Social Problems]]'' and ''[[Social Forces (journal)|Social Forces]]''.<ref name=JermierYears>{{cite journal |last=Foster |first=John Bellamy |title=Organization & Environment: The Jermier Years, 1997-2012 |journal=American Sociological Association Environment and Technology Newsletter |date=Fall 2012 |pages=1–3 |url=http://www.envirosoc.org/newsletters.php}}</ref>

=== Editors ===
The following persons have been (co-)editors of the journal:
* [[Paul Shrivastava]] (founding co-editor, [[Concordia University]])<ref>[http://paulshrivastava.com/ "Biography", Paul Shrivastava.] Accessed: December 2, 2012.</ref>
* John M. Jermier (founding co-editor, [[University of South Florida]]), 1997-2012<ref>[http://www.patelcenter.usf.edu/oe/journal.htm "Organization & Environment", Patel Center, University of South Florida]. Accessed: December 2, 2012.</ref><ref>[http://www.sagepub.com/editorDetails.nav?contribId=514039 "John M. Jermier", Sagepub.com.] Accessed: December 2, 2012.</ref>
* [[John Bellamy Foster]] ([[University of Oregon]]), 1996-2001<ref>[http://sociology.uoregon.edu/cv/foster.pdf "Curriculum Vitae," John Bellamy Foster, November 2012.] Accessed: December 2, 2012.</ref>
* Richard York (University of Oregon), 2006-2012<ref>[http://sociology.uoregon.edu/cv/york.pdf "Curriculum Vitae", Richard York, October 2012.] Accessed: December 2, 2012.</ref>
* J. Alberto Aragon-Correa (University of Granada), 2012–present
* Mark Starik (San Francisco State University), 2012–present

== Editorial transition ==
As of December 1, 2012, ''Organization & Environment'' is edited by Aragon-Correa and Starik. From this point, the journal aims to conduct "rigorous explorations and analyses of the multiple connections between the management of organizations and any of the relevant dimensions of [[sustainability]]"; targeted contributors are "sustainability management, policy, and related social science researchers".<ref name=transition>[http://oae.sagepub.com/site/includefiles/Editorial_Transition_Web_OE_081912.pdf "Editorial Transition in Organization & Environment", Sagepub.com.] Accessed: August 29, 2012.</ref>

=== Criticism ===

The December 2012 editorial transition was strongly criticized by (now former) [[editorial board]] members of ''Organization & Environment'' as an "editorial coup" on the part of Sage and GRONEN.<ref name=JermierYears /><ref name=withdrawal>{{cite web |title=Statement of Collective Withdrawal from Editorial Review Board of ''Organization & Environment''|url=http://climateandcapitalism.com/2012/10/18/editors-resign-from-leading-environment-journal/|publisher=Climate & Capitalism|accessdate=18 October 2012}}</ref> According to one account, the editorial transition was decided upon and carried out by Sage independently of its editors/editorial board, without their prior knowledge or acceptance.<ref>{{cite journal |journal=Monthly Review |date=October 2012 |volume=64 |issue=7  |title=Notes from the Editors |author=The Editors |url=http://monthlyreview.org/2012/10/01/mr-064-05-2012-09 |accessdate=October 12, 2012 }}</ref> Critics have pointed out that this is not the first time accusations of this sort have been leveled at Sage. In 2009, political scientists charged that the editor of [[Political Theory (journal)|''Political Theory'']] was replaced unilaterally and unfairly. Sage was forced to apologize, admitting that "whatever" they had done "was done without consulting the scholars on the editorial board of the journal".<ref>{{cite web |author=Jaschik, Scott |title=Who Controls Journals? |date=July 7, 2009 |url=http://www.insidehighered.com/news/2009/07/07/sage |work=Inside Higher Ed |accessdate=13 October 2012}}</ref>

Critics also argue that the pro-corporate/management stance of GRONEN stands in stark contrast to the work of previous ''O&E'' editors and writers, many of whom tended strongly to emphasize the role of corporate power in perpetuating environmental degradation.{{citation needed|date=December 2012}} In a statement published in October 2012, 25 members of the editorial board of ''O&E'' resigned in protest of the transition.<ref name=withdrawal />

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://oae.sagepub.com/}}
* [http://www.gronenonline.org/ Group of Research on Organizations and the Natural Environment]
* [http://www.patelcenter.org/oe/journal.htm Former editors' website,] [[University of South Florida]] (through December 2012; does not reflect current editors or journal contact information)

{{DEFAULTSORT:Organization and Environment}}
[[Category:Environmental social science journals]]
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Environmental sociology]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1987]]
[[Category:Sustainability-related journals]]
[[Category:Business and management journals]]
[[Category:Environmental studies journals]]