{{Italic title}}
{{Infobox journal
| title         = Journal of the Physical Society of Japan 
| cover         = 
| editor        = A. Kawabata  
| discipline    = All fields of physics 
| peer-reviewed = 
| formernames   = Proceedings of the Physico-Mathematical Society of Japan (pre-1946) 
| abbreviation  = J. Phys. Soc. Jpn., and J. Phys. Soc. 
| publisher     = The Physical Society of Japan (JPS)  
| country       = Japan
| frequency     = monthly
| history       = 1946 to present 
| openaccess    = some 
| license       = 
| impact        = 2.572  
| impact-year   = 2009 
| website       = http://jpsj.ipap.jp/index.html
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           = http://www.ipap.jp/jpsj/rss/jpsj.xml 
| atom          = 
| JSTOR         = 
| OCLC          = 1762332  
| LCCN          = 50013353  
| CODEN         = JUPSAU 
| ISSN          = 0031-9015 
| eISSN         = 1347-4073 
| boxwidth      = 
}}

'''''Journal of the Physical Society of Japan''''' ('''''JPSJ''''') is a monthly,  [[peer review]]ed, [[scientific journal]] published by the [[Physical Society of Japan]] (JPS). It was first published in July 1946 (volume 1). The editor-in-chief is A. Kawabata, until August 2010. The [[impact factor]] for ''JPSJ'' in 2009 is 2.572, according to  [[Journal Citation Reports]].<ref name=about/>

Volume 1 consists of a single issue designated, on the cover, from July to December 1946. And between 1967-1980 this journal published at a rate of two volumes per year. The other (Japanese) title for this journal is ''Nihon Butsuri Gakkai ōji hōkoku''. Volumes for 1967 to the present day are accompanied by an annual supplement.<ref name=LCC>{{Cite web
  | title =Nature materials. 
  | work =Bibliographic information for this journal. 
  | publisher =Library of Congress 
  | date =January 2009 
  | url =http://lccn.loc.gov/50013353 
  | format = 
  | accessdate =2010-07-31}}</ref>

Research paper formats include full papers, letters, short notes, comments, addenda, errata, invited papers and special topics.<ref name=about/>

==Organizational structure==
The organizational structure of the journal is described as follows:

The ''Full Papers'', ''Letters'' and ''Short Notes'' sections of the journal comprise the published original research results. Furthermore, the ''Full Papers'' section is intended to be self-contained, original research papers. The ''Short Notes'' are brief reporting on recent breakthroughs. Finally, invited reviews from a notable researcher in the field, and a collection of relevant subjects under Special Topics  are occasionally included for publication.<ref name=about/>

All articles are published online in advance, before they are printed on paper. The online version of JPSJ is updated twice a month (on the 10th and 25th). The paper version of JPSJ is printed once per month (on the 15th). This version comprises the two groups of articles that are published online on two different dates.<ref name=frequency>{{Cite web
  | last =JPSJ 
  | title =Information for Submission 
  | work =See: 9. Online Publication, and 10. Paper Publication for frequency 
  | publisher =Physical Society of Japan  
  | date =June 16, 2010  
  | url =http://jpsj.ipap.jp/authors/index.html#10 
  | accessdate =2010-07-07}}</ref>

==Overview==
The journal was established in 1946, succeeding its predecessor publication, ''Proceedings of the Physico-Mathematical Society of Japan''. In its present state, ''JPSJ'' is an international journal, with submissions from authors worldwide. Additionally, financial support is available to those authors in need from developing countries.<ref name=about/>

The online version has allowed for notable extra information to be available. Certain significant works are selected and emphasized on the journals' homepage. In the News and Comments column the background and impact of selected researchers are discussed by those who are described as experts, which indirectly provides the added dimension of relevant information about recent developments in physics.<ref name=about>{{Cite web
  | title =About Journal of the Physical Society of Japan
  | publisher =The Physical Society of Japan (JPS)  
  | date =July 2010 
  | url =http://jpsj.ipap.jp/e_policy.html 
  | accessdate =2010-07-05}}</ref>

==Scope==
The main focus of ''JPSJ'' is all topics related to physics. This includes pure and applied [[physics]] research topics which encompass core physics disciplines, and  broad topical coverage that is related to these core disciplines.<ref name=about/> Hence, subject areas cover an exploration and investigation of nature and substances that exist in the world and the universe, from [[atom]]ic to [[cosmological]] scales. This encompasses defining and describing observations, interactions, and forces which occur in nature and, hence, in substances. Such descriptions may include their effect on, or within, a given natural system. More broadly, it is the general analysis of nature, conducted in order to understand how the universe behaves. Therefore, subject areas encompass [[energy]], [[forces]], [[mechanics]], [[radiation]], [[heat]], [[matter]], [[electromagnetism]], [[quantum mechanics]] and [[general theory of relativity]].<ref>
{{cite book
 |author=J.C. Maxwell
 |year=1878
 |title=Matter and Motion
 |url=https://books.google.com/books?id=noRgWP0_UZ8C&printsec=titlepage&dq=matter+and+motion
 |page=9
 |publisher=(publisher is D. Van Nostrand)
 |quote=Physical science is that department of knowledge which relates to the order of nature, or, in other words, to the regular succession of events.
 |isbn=0-486-66895-9
}}</ref><ref>
{{cite book
 |author=H.D. Young, R.A. Freedman
 |year=2004 |edition=11th
 |title=University Physics with Modern Physics
 |page=2
 |publisher=[[Addison Wesley]]
 |isbn=
 |quote=Physics is an ''experimental'' science. Physicists observe the phenomena of nature and try to find patterns and principles that relate these phenomena. These patterns are called physical theories or, when they are very well established and of broad use, physical laws or principles.
}}</ref><ref>
{{cite book
 |author=S. Holzner
 |year=2006
 |title=Physics for Dummies
 |url=http://www.amazon.com/gp/reader/0764554336
 |page=7
 |publisher=[[John Wiley & Sons|Wiley]]
 |quote=Physics is the study of your world and the world  and universe around you.
 |isbn=0-470-61841-8
}}</ref><ref name=feynmanLectures>[[Richard Feynman]] begins [[The Feynman Lectures on Physics|his ''Lectures'']] with the [[atomic theory|atomic hypothesis]], as his most compact statement of all scientific knowledge: "If, in some cataclysm, all of scientific knowledge were to be destroyed, and only one sentence passed on to the next generations ..., what statement would contain the most information in the fewest words? I believe it is ... that ''all things are made up of atoms – little particles that move around in perpetual motion, attracting each other when they are a little distance apart, but repelling upon being squeezed into one another. ...''" {{Cite book
  | last =Feynman 
  | first = Richard P. 
  | format = Library of Congress bibliography. 
  |author2=Leighton, Robert B. |author3= Sands, Matthew L. 
   | title =The Feynman lectures on physics 
  | publisher =Addison-Wesley Pub. Co 
  | date = 1963–1965 
  | location =Reading, Mass. 
  | work = source is Library of Congress |LCCN=63020717
  | url =http://lccn.loc.gov/63020717 
  | isbn = 
  | authorlink = Richard Feynman }} LCCN = 63020717(click on title)</ref>

==Previous and current IF==
Thomson Reuters, Journal Citation Reports rated ''JPSJ'' with an impact factor of 2.212 in 2008, and an impact factor of 2.572 in 2009.<ref name=if-2008>{{Cite web
  | title =JPSJ facts 
  | publisher =The Physical Society of Japan 
  | date =July 2010 
  | url =http://wwwsoc.nii.ac.jp/jps/english/editorschoice.html 
  | accessdate =2010-07-05}}</ref>

==Abstracting and indexing==
''Journal of the Physical Society of Japan'' is indexed in the following bibliographic databases:<ref name=LCC/><ref name=TR-index>{{Cite web
  | title =Journal Search 
  | work =Master Journal List  
  | publisher =Thomson Reuters 
  | date =July 2010 
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0031-9015}}</ref><ref name=cassi>{{Cite web
  | title =''Journal of the Physical Society of Japan'' 
  | work =Chemical Abstracts Service Source Index (CASSI) (Displaying Record for Publication) 
  | publisher =[[American Chemical Society]]
  | date =July 2010 
  | url =   http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfGSvxz8hlgseUwvRl-5hXyTLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfYmf3z791AoeQ2XmtIWVE7w
  | format = 
  | accessdate =2010-07-31}}</ref>  
*[[Chemical Abstracts]] 0009-2258
*[[Chemical Abstracts Service]] -[[CASSI]]
*[[Current Contents]] - Physical, Chemical & Earth Sciences
*[[Science Citation Index]]
*[[Science Citation Index Expanded]]

==See also==
*''[[American Journal of Physics]]''
*''[[Annales Henri Poincaré]]''
*''[[Applied Physics Express]]''
*''[[CRC Handbook of Chemistry and Physics]]''
*''[[European Physical Journal E|European Physical Journal E: Soft Matter and Biological Physics]]''
*''[[Journal of Physics A: Mathematical and Theoretical]]''
*''[[Journal of Physical and Chemical Reference Data]]''
*''[[Physical Review E|Physical Review E: Statistical, Nonlinear, and Soft Matter Physics]]''
*''[[Physics Today]]''

==References==
{{Reflist}}

[[Category:English-language journals]]
[[Category:Physics journals]]
[[Category:Publications established in 1946]]
[[Category:Monthly journals]]