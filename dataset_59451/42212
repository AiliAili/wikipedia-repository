{{Use dmy dates|date=February 2013}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = AMX
 |image= File:Italian Air Force AMX fighter.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type = [[Ground-attack aircraft]]
 |national origin= [[Italy]] and [[Brazil]]
 |manufacturer = [[AMX International]]
 |designer=
 |first flight = 15 May 1984
 |introduced = 1989
 |retired=
 |status= Active service
 |primary user = [[Italian Air Force]]
 |more users = [[Brazilian Air Force]]
 |produced = 1986–1999
 |number built = ~200
 |unit cost=$16.5 million (1999)<ref>[https://www.forecastinternational.com/archive/disp_pdf.cfm?DACH_RECNO=247 "Alenia/Aermacchi/Embraer AMX - Archived 02/2008."] ''Forecast International'', February 2007.</ref>}}
|}

The '''AMX International AMX''' is a [[ground-attack aircraft]] for battlefield interdiction, [[close air support]] and [[reconnaissance]] missions. As a result of a memorandum between [[Italy]] and [[Brazil]] for the type's joint development in 1981, [[AMX International]], an Italian-Brazilian joint venture, was formed to develop, manufacture, and market the aircraft. The AMX is designated '''A-11 Ghibli''' by the [[Italian Air Force]] and '''A-1''' by the [[Brazilian Air Force]].

The AMX is capable of operating at high [[Subsonic flight|subsonic]] speed and low altitude, by day or night, and if necessary, from bases with poorly equipped or damaged [[runway]]s. It possesses a low [[infrared]] (IR) signature and a reduced [[radar]]-equivalent cross-section to help prevent detection, while low vulnerability of structure and systems aid survivability and a compliment of integrated [[electronic countermeasures]] (ECM), [[air-to-air missile]]s and nose-mounted guns provide self-defence capabilities.

The Italian name, "Ghibli", is taken  from the [[Ghibli (wind)|hot dry wind]] of the [[Libyan desert]], and is traditional in the Italian Air Force. 

==Development==
[[File:AMX (5167938804).jpg|thumb|A pair of AMXs in flight, 2010]]
In early 1977, the [[Italian Air Force]] issued a requirement for 187 new-build [[strike fighter]]s, which were to replace its existing [[Aeritalia G.91]] in the [[close air support]] and [[reconnaissance]] missions, as well as the [[Lockheed F-104 Starfighter|Lockheed RF-104G Starfighter]] also being used in the reconnaissance role.<ref name = "bomber 289">Gunston and Gilchrist 1993, p. 289.</ref><ref name="warwick p1544"/> Rather than competing for the contract, [[Aeritalia]] (now [[Alenia Aeronautica]]) and [[Aermacchi]] agreed to produce a joint proposal for the requirement, as both firms had been considering the development of a similar class of aircraft for some years. During the early 1970s, Aermacchi had been conducted work on a design study for such a light ground attack aircraft under the designation of ''MB-340''. In April 1978, development work on the joint venture formally commenced.<ref name="Braybrook p267">Braybrook 1989, p. 267.</ref> 

During 1980, the Brazilian government announced that they intended to participate in the program as a replacement for the [[Aermacchi MB-326]]. In July 1981, the Italian and Brazilian governments agreed on joint requirements for the aircraft, and [[Embraer]] was invited to join the industrial partnership.<ref name="Braybrook p267"/> An agreement was also struck to divide AMX manufacturing between the partners; for each production aircraft, Aeritalia manufactured 46.5% of the components (central fuselage, stabilisers and rudders), Aermacchi produced 22.8% (front fuselage and tail cone), and Embraer performed 29.7% of the work (wing, air intakes, pylons and drop tanks).<ref name = "bomber 289"/> There was no duplication of work, each component of the aircraft was built at one source only. The planned requirements were 187 aircraft for Italy and 100 for Brazil.<ref name="warwick p1544">Warwick 1981, p. 1544.</ref>

[[File:FAB AMX International A-1A - Lofting.jpg|thumb|left|Brazilian Air Force A-1A at [[Mendoza, Argentina|Mendoza]], [[Argentina]], 2005]]
In the early stages of development, various different powerplants and engine configurations were studied to power the type; both twin-engine and single-engine approaches were considered. The use of US-sourced engines had been promptly discounted to avoid any potential restrictions on export sales of the overall aircraft.<ref name="warwick p1544"/> Amongst the engines examined were the [[Turbo-Union RB199]] (as used by the larger [[Panavia Tornado]]), the [[Rolls-Royce Turbomeca Adour]], [[Armstrong Siddeley Viper|Rolls-Royce Viper]], and the [[Rolls-Royce Spey]] engine.<ref name="warwick p1544"/> In 1978, the Spey 807 model, which featured several additional improvements, was selected to power the new aircraft.<ref name="warwick p1544"/>

Key features that were reportedly emphasised in the design process of the new aircraft were accessibility and survivability; the AMX had to be able to sustain a single failure to any onboard system without any performance degradation.<ref name="warwick p1544"/> To achieve this, key systems are duplicated and vital systems are protected; the use of [[Vehicle_armour#Aircraft|cockpit armor]] was considered but ultimately discounted due to the expense involved.<ref name="warwick p1544"/> The detailed definition phase of the project was completed in March 1980.<ref name = "bomber 289"/>

A total of seven flight-capable prototypes were produced for the test program, three by Aeritalia, two by Aermacchi, and two by Embraer, as well as two static airframes. The first prototype, assembled in Italy, made its maiden flight on 15 May 1984. This first aircraft was lost on its fifth flight in an accident, resulting in the death of its pilot.<ref name="Braybrook p275">Braybrook 1989, p. 275.</ref> Aside from this early loss, testing progressed smoothly and without further incident.<ref name="Mietus p223-4">Meitus 1992, pp. 223–224.</ref> The first Brazilian-assembled prototype made its first flight on 16 October 1985. On 11 May 1988, the first production aircraft performed its first flight.<ref name = "bomber 291"/> Deliveries of production aircraft to Italy began in 1988,<ref name="Braybrook p275"/> the first examples were delivered to the [[Brazilian Air Force]] during the following year.<ref name="jawa93 p126">Lambert 1993, p. 126.</ref> On 14 March 1990, the prototype two-seat AMX made its first flight.<ref name = "bomber 291">Gunston and Gilchrist 1993, p. 291.</ref>

==Design==
[[File:AMX - RIAT 2008 (2674769345).jpg|thumb|An AMX performing a display at the 2008 [[Royal International Air Tattoo]]]]
The AMX has a conventional shoulder-winged [[monoplane]]. It is composed primarily of aluminium and manufactured using traditional construction methods, however elements such as the tail fin and [[Elevator (aircraft)|elevators]] use [[carbon fibre]] composite materials.<ref name = "bomber 289"/> For its size, a large proportion of the AMX's internal space is allocated to avionics and onboard computer systems, both the navigation and attack systems are computerized.<ref name="warwick p1545">Warwick 1981, p. 1545.</ref> For accessibly and ease of maintenance, all avionics are installed directly in bays beneath the cockpit in a manner in which they can be worked upon at ground level without the use of support platforms.<ref name="warwick p1544"/>

Drawing on experience from the [[Panavia Tornado]], the AMX is equipped with a hybrid [[flight control system]]; a [[fly-by-wire]] control system is employed to operate flight control surfaces such as the [[Spoiler (aeronautics)|spoiler]]s, [[rudder]] and variable incidence [[tailplane]], while the [[aileron]]s and [[Elevator (aeronautics)|elevator]]s are actuated via a dual-redundant [[hydraulics|hydraulic]] system. Manual reversion is provided for the ailerons, elevator and rudder to allow the aircraft to be flown even in the event of complete hydraulic failure; either control system can act independent of one another.<ref name="jawa93 p129-1"/><ref name="warwick p1544-5">Warwick 1981, pp. 1544–1545.</ref> The wing is fitted with both [[leading edge slats]] and trailing edge [[Flap (aircraft)|flaps]] and overwing spoilers ahead of the flaps. The spoilers can function as airbrakes and to negate lift; improving take-off and landing performance as well as manoeuvrability during flight.<ref name = "bomber 290"/>

A single [[Rolls-Royce Spey]] [[turbofan]] engine serves as the AMX's powerplant. During the aircraft's development, the Spey was heavier and less modern than some of the available alternatives, but it was considered to be reliable, relatively cheap and was free of export restrictions that would be imposed by using American engines.<ref name="warwick p1544"/><ref name="Braybrook p270">Braybrook 1989, p. 270.</ref> The Spey engine also enabled for the use of a simplified round-lipped inlet design. The rear fuselage is detachable in order to gain access to the engine. Separate consortia in both Brazil and Italy manufactured the Spey for the AMX.<ref name = "bomber 290"/> Unusual for a strike aircraft of the era, no attempt was made to develop the aircraft to fly at [[supersonic]] speeds.<ref name = "bomber 289"/>

[[File:Italian-Brazilian AMX.JPEG|thumb|left|An AMX and several munitions on display at the [[Paris Air Show]], 1989]]
Brazilian and Italian aircraft differ considerably in their avionics, Italian aircraft being equipped with various [[NATO]] systems which were considered redundant in the South American theatre.<ref name="warwick p1545"/> AMXs in Brazilian service are often fitted with one of three [[pallet]]-mounted sensor packages, which contain various vertical, oblique, and forward-facing cameras, for performing the aircraft's secondary [[aerial reconnaissance]] role.<ref name = "bomber 291"/><ref name="warwick p1545"/> A simple ranging radar is equipped for targeting purposes, however the specific radar also differs between both operators.<ref name = "bomber 290">Gunston and Gilchrist 1993, p. 290.</ref> The flight system employs a [[General Electric Company plc|GE Avionics]] flight control computer.<ref>Gunston and Gilchrist 1993, pp. 289-290.</ref> Extensive [[electronic countermeasure]] (ECM) are fitted to protect the aircraft, include passive receiver antenna on the tailfin and an active jammer pod that is typically mounted on one of the aircraft's [[hardpoints]].<ref name = "bomber 290"/>

Various munitions could be mounted on the one centerline and four under-wing hardpoints, including bombs, missiles and rockets; payloads of up to 2,000 lb may be carried upon the centerline and the two innermost under-wing pylons, while the outer pylons can carry up to 1,000 lb.<ref name="warwick p1545"/> All four of the under-wing hardpoints are plumbed for [[drop tank]]s to extend the aircraft's range, Italian aircraft are also fitted with a fixed [[aerial refueling]] probe.<ref name = "bomber 290"/> Optical reconnaissance pods can be carried externally on the centerline hardpoint.<ref name = "bomber 291"/> Wingtip rails are provided for infrared guided [[air-to-air missile]]s such as the [[AIM-9 Sidewinder]] and [[MAA-1 Piranha]]. Italian aircraft are fitted with a [[M61 Vulcan]] 20&nbsp;mm [[rotary cannon]] on the port side of the lower fuselage.<ref name="warwick p1545"/> The United States denied the sale of the M61 to Brazil, thus its AMXs are instead fitted with two 30&nbsp;mm [[DEFA cannon|DEFA 554]] [[revolver cannon]]s.<ref name="jawa93 p129-1"/><ref name="wapj5 p135">Jackson 1991, p. 135.</ref>

==Operational history==
[[File:AMX International AMX-T, Italy - Air Force AN0326384.jpg|thumb|Italian AMX at [[RAF Fairford]], [[England]], 1998]]
The first operational squadron of the Italian air force, the 103° ''Gruppo'' of 51° ''Stormo'' formed in November 1989,<ref name = "Itp351">Nicolli 1997, p. 351.</ref> with the first Italian unit also forming in 1989.<ref name="iapr5 p138">Jackson 1991, p. 138.</ref> Both the Italian and Brazilian AMX fleets were grounded in February 1992, following the crash of an Italian AMX due to engine failure. Operations were allowed to restart in May that year, following modification of the engines.<ref name="It p351-2">Nicolli 1997, pp. 351–352.</ref><ref>[http://www.flightglobal.com/pdfarchive/view/1992/1992%20-%201158.html?search=AMX  "Brazil and Italy lift AMX Ban"]. ''Flight International'', 6–12 May 1992, p. 14.</ref>

Italy assigned six AMXs from 103° ''Gruppo'' to operations over [[Bosnia]] in 1995 as part of [[Operation Deny Flight]], which was followed by a similar deployment in support of the [[Implementation Force (IFOR)|IFOR]] peacekeepers in Bosnia. This deployment was interrupted by another grounding, again due to engine failure, between January and March 1996.<ref name= "It p352">Nicolli 1997, p. 352.</ref> Italian AMX aircraft were used in 1999 in the [[Kosovo war]]. Instead of using unguided or more traditional [[laser-guided bomb]]s, the Italian Air Force used dozens of [[Mark 82 bomb|Mk 82]] bombs fitted with [[Opher]] Israeli guidance kits, effectively converting the "dumb" bombs into an [[infrared]]-guided bomb.<ref>{{cite web | url = http://www.flightglobal.com/Articles/1999/06/17/52884/Opher+bomb+deployed+in+Kosovo.html | title = Opher bomb deployed in Kosovo | publisher = Flight Global}}</ref>

In the late 1990s, AMX International was considering a major engine refit; a non-[[afterburners|afterburning]] variant of the [[Eurojet EJ200]] was proposed, with considerably more thrust than the existing powerplant.<ref>Norris, Guy. [http://www.flightglobal.com/pdfarchive/view/1999/1999%20-%200943.html "EJ200 engine proposed for AMX."] ''Flight International'', 20 April 1999.</ref> In 2005, the Italian Air Force launched an upgrade programme (ACOL ''Aggionamento Capacità Operative e Logistiche'' – Operational and Logistical Capability Upgrade) for 55 of its AMXs,<ref>{{cite web |url= http://www.flightglobal.com/articles/2005/02/08/193586/alenia-lands-390m-amx-upgrade-contract.html |title= Alenia lands $390m AMX upgrade contract |author= |date= 8 February 2005 |work= Flight International|publisher= Flight Global|accessdate=7 November 2009 }}</ref> adding a new [[Inertial navigation system#Laser gyros|laser INS]], new cockpit displays and allowing the aircraft to drop [[Joint Direct Attack Munition]] guided bombs.<ref name="Niccoli p44">Niccoli 2009, p.44.</ref> In August 2007, Embraer began a major midlife upgrade programme and modernisation of 53 Brazilian Air Force AMX A-1s, focusing on avionics systems and new armament additions; the programme is estimated to have extended the lifespan of the fleet beyond 2027.<ref>Gething, Michael J. [http://www.janes.com/products/janes/defence-security-report.aspx?ID=1065926600 "Embraer commences AMX modifications for the Brazilian Air Force."] ''[[Jane's]]'', 9 June 2007.</ref>

[[File:AMI AMX.jpg|thumb|left|Italian Air Force AMX, 2010]]

Starting in November 2009, four Italian AMXs were deployed overseas for operations in Afghanistan, replacing the same number of Italian Tornado IDS in the reconnaissance role.<ref>Peruzzi, Luca.  [http://www.janes.com/products/janes/defence-security-report.aspx?id=1065971247 "Italy's JATF in Afghanistan boosts air-to-ground and C-IED capabilities."] ''[[Jane's]]'', 9 November 2012.</ref> Of particular note is the aircraft's ability to share digital electro-optical and infrared sensor information with ground troops in real time, providing valuable reconnaissance information and helping to minimise threat exposure.  By the end of 2010 over 700 combat missions had been flown in the Afghan theatre. On 28 May 2014, the AMX performed its last combat sortie in the Afghan theatre, and on 20 June 2014, all remaining AMXs were withdrawn from Afghanistan.<ref>[http://www.avionews.com/index.php?corpo=see_news_home.php&news_id=1123496&pagina_chiamante=index.php  "Italian Air Force: a year of missions for the AMX airplanes in Afghanistan."] ''AvioNews'', 3 December 2010.</ref>

In 2011, Italian AMX aircraft were employed during the [[2011 military intervention in Libya]]. Italian military aircraft deployed 710 guided bombs and missiles during sorties: Italian Air Force Tornados and AMX fighter bombers deployed 550 bombs and missiles, while Navy AV-8Bs deployed 160 guided bombs. The conflict saw the first use by AMX aircraft of [[LITENING#Litening III|Litening]] targeting pods paired with [[Paveway]] and JDAM guided bombs.<ref>{{cite web |url = http://www.defensenews.com/story.php?i=8567271 |title = Italy Gives Bombing Stats for Libya Campaign |first = Tom |last = Kingston |publisher = Defense News | date = 14 December 2011}}</ref> In early 2016, due to the declining stability of Libya, Italy opted to station additional aircraft, including four AMX fighters, to [[Vincenzo Florio Airport Trapani–Birgi|Bassi Airbase]], [[Trapani]], [[Sicily]].<ref>[http://sputniknews.com/military/20160116/1033228494/italy-sends-fighter-birgi.html "Italy Sends Fighter Aircraft to Birgi Base Amid Concerns Over Libya."] ''Sputnik News'', 16 January 2016.</ref>

In March 2012, the Philippines was reportedly holding negotiations with Italy for the possible procurement of used AMX aircraft;<ref>{{cite web | url= http://www.philstar.com/Article.aspx?articleId=775432&publicationSubCategoryId=63 | title = DND signs five-year agreement with Italy | work = The Philippine Star | date = 8 February 2012 | accessdate = 20 February 2012}}</ref> however, on 28 March 2014, the Philippines' Department of National Defense signed a contract for 12 [[KAI T-50 Golden Eagle|FA-50]] light attack aircraft worth P18.9 billion (US$421.12 million).<ref>{{cite web|url=https://www.koreaaero.com/english/pr_center/cpr_view.asp?pg=1&gubun=v&seq=25777&bbs=10 |title=KAI won a contract to export 12 FA-50s to the Phil |publisher=Korea Aerospace Industries, LTD. (KAI) |date=2014-03-28 |accessdate=29 November 2015}}</ref><ref>{{cite web|url=http://www.arirang.co.kr/News/News_View.asp?nseq=159955|title=Korean government to sell 12 FA-50 fighter jets to Philippines|publisher=Arirang News|date=28 March 2014|accessdate=29 November 2015}}</ref>

==Variants==
{{externalimage |topic= |width= |align=right
|image1=[http://cdn-www.airliners.net/aviation-photos/photos/3/9/0/1798093.jpg Head-on view of an AMX]
|image2=[http://cache.desktopnexus.com/thumbnails/1230173-bigthumbnail.jpg AMX in wrap-around camouflage]
|image3=[http://media.defenceindustrydaily.com/images/AIR_AMX_Brazil_Armed_lg.jpg Laiden AMX taking off]
|image4=[http://cdn-www.airliners.net/aviation-photos/photos/0/7/6/1201670.jpg Inside view of cockpit]
|image5=[http://cdn-www.airliners.net/aviation-photos/photos/8/6/4/1607468.jpg AMX in special colour scheme]}}
;AMX-T
In 1986, development of a two-seat advanced trainer variant was undertaken. This was intended to provide trainee pilots with experience on fast jets, while still retaining the single-seater's attack capabilities.  First flying in 1990, the AMX-T equipped both the Italian and Brazilian air forces.

;AMX-ATA
The AMX Advanced Trainer Attack (AMX-ATA) is a new AMX two-seater multi-mission attack fighter developed for combat roles and advanced training. The AMX-ATA incorporates new sensors, a forward-looking infrared helmet-mounted display, a new multi-mode radar for air-to-air and air-to-surface capability, and new weapons systems including anti-ship missiles and medium-range missiles. The [[Venezuelan Air Force]] ordered eight AMX-ATA in 1999 for the advanced trainer and attack aircraft role, but the US Congress vetoed the sale because the aircraft systems include US technology.<ref>Spinelli, Andrea. [https://www.flightglobal.com/news/articles/venezuela-selects-mb339fd-and-amx-ata-combination-39344/ "Venezuela selects MB339FD and AMX ATA combination."] ''Flight International'', 15 July 1998.</ref><ref>[https://www.flightglobal.com/news/articles/military-aircraft-directory-amx-40212/ "Military Aircraft Directory: AMX."] ''Flight International'', 29 July 1998.</ref>

;AMX-R (RA-1)
An AMX variant designed for reconnaissance missions.  Various reconnaissance pallets can be fitted; used by the Brazilian Air Force.

;A-1M
The product of a Brazilian upgrade program of their A-1s; significant features include a Mectron SCP-01 Scipio radar,<ref>{{Citation | trans_title = Embraer profits from aircraft retrofit | url = http://www.estadao.com.br/noticias/impresso,embraer-lucra-com-retrofit-de-avioes,773299,0.htm | title = Embraer lucra com ''retrofit'' de aviões | language = pt | newspaper = Estadão | date = 16 September 2011 | accessdate = 29 January 2012}}.</ref> Embraer BR2 data link, FLIR Systems navigation equipment,<ref>{{Citation | url = http://www.militaryaerospace.com/articles/2009/06/flir-systems-wins-7-million-contract-from-embraer.html | title = FLIR Systems wins $7 million contract from Embraer | newspaper = Military Aero Space | date = 9 June 2009 | accessdate = 29 January 2012}}.</ref> Elbit INS/GPS/databus, the adoption of a glass cockpit,<ref>{{Citation | trans_title = Elbit gets US$ 187 mi contract with Embraer | url = http://br.reuters.com/article/domesticNews/idBRSPE4AA01G20081111 | title = Elbit consegue contrato de US$187 mi da Embraer | language = pt | newspaper = Reuters | date = 11 November 2009 | accessdate = 29 January 2012}}.</ref> a new OBOGS system and [[Helmet mounted display|HMD DASH IV]].

;A-11A
Italian military designation for the AMX from 2012.<ref name = "dgaa">{{cite web |url = http://www.dgaa.it/newsletter/newsletter60/AER-0-0-12.pdf | trans_title = Usage of the ‘Mission design series’ (MDS) designation in the technical publications (PTT) under Daa’s competence | title = Utilizzo della Nomenclatura 'Mission Design Series' (MDS) neele Pubblicazioni Tecniche (PPTT) di Competenza della Daa | publisher = Ministero Della Defesa | language = it | format = [[Portable document format|PDF]] | date = June 2011}}</ref>

;TA-11A
Italian military designation for the AMX-T from 2012.<ref name = "dgaa" />

;A-11B
Italian military designation for the AMX ACOL from 2012.<ref name = "dgaa" />

;TA-11B
Italian military designation for the AMX-T ACOL from 2012.<ref name = "dgaa" />

==Operators==
[[File:AMX A-1 FAB.JPG|thumb|Brazilian Air Force AMX in [[Pirassununga]], [[São Paulo]], Brazil]]

;{{BRA}}
*[[Brazilian Air Force]] operates 60 AMX-A/T (including eight AMX-T for training).<ref name=Census2011>[http://www.emagazine.flightinternational.com/1B4ee1d2e7f2aef012.cde/page/35 "World Airliner Census 2011".] ''Flight Global," 13–19 December 2011. Retrieved: 10 January 2012</ref> 43 aircraft to be modernized to A-1M, delivered between 2013 and 2017, and to be retired in 2032.<ref>[http://www.estadao.com.br/noticias/impresso,fab-ganha-poder-de-fogo-com-novo-caca-,823166,0.htm "FAB ganha poder de fogo com 'novo' caça" (in Portuguese).] {{webarchive |url=https://web.archive.org/web/20120129033836/http://www.estadao.com.br/noticias/impresso,fab-ganha-poder-de-fogo-com-novo-caca-,823166,0.htm |date=29 January 2012 }} ''Estadao, '' 16 January 2012. Retrieved: 16 January 2012.</ref>
**1 Esquadrão/16 Grupo de Aviação Esquadrão Adelphi
**1 Esquadrão/10 Grupo de Aviação Esquadrão Poker
**3 Esquadrão/10 Grupo de aviação Esquadrão Centauro

;{{ITA}}
*[[Italian Air Force]] operates 35× A-11B (originally AMX ACOL) and 11× TA-11B (originally AMX-T ACOL) (out of four prototypes, 110 one-seaters and 26 two-seaters delivered)
**101 Gruppo, 51 Stormo (Training Squadron operating 11× AMX-T ACOL) - were disbanded
**103 Gruppo, 51 Stormo (Ground Attack Squadron 18× AMX ACOL) - were disbanded
**132 Gruppo, 51 Stormo (Ground Attack Reconnaissance Squadron 17× AMX ACOL with the [[RecceLite]] Pod from [[Rafael Advanced Defense Systems|Rafael]])

==Aircraft on display==
* A former Italian Air Force AMX is displayed on a pedestal in front of the Thales Alenia Space plant in [[Turin]]<ref>https://www.google.it/maps/place/Strada+Antica+di+Collegno,+253,+10146+Torino/@45.0820987,7.6117471,136m/data=!3m1!1e3!4m2!3m1!1s0x47886b7fc3ad006f:0x667334df0dc3663e</ref>

* The first prototype built in Brazil is displayed on a open museum called [[Memorial Aeroespecial Brasileiro]] in [[São José dos Campos]]
<ref>http://www.aeroin.net/historia-da-industria-aeronautica-brasileira-memorial-aeroespacial-brasileiro/</ref>

==Specifications (AMX)==
[[File:AMX (AERITALIA, AERMACCHI, EMBRAER).png|right|350px|Orthographically projected diagram of the AMX]]
[[File:AMX Air force Jet (4934654146) (2).jpg|thumb|A close-up of the tailfin and rear fuselage of an AMX]]
[[File:M61 Vulcan italian AMX.jpg|thumb|M61 Vulcan cannon on the underside of an Italian AMX]]
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|ref=Jane's All The World's Aircraft 1993–94,<ref name="jawa93 p129-1">Lambert 1993, pp. 129–131.</ref> Jet Bombers<ref name = "bomber 292">Gunston and Gilchrist 1993, p. 292.</ref>
|plane or copter?=plane
|jet or prop?=jet
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully-formatted line beginning with an asterisk "*" -->
|crew=1
|length main=13.23 m
|length alt=43 ft 5 in
|span main=8.87 m
|span alt=29 ft 1{{fraction|1|2}} in
|height main=4.55 m
|height alt=14 ft 11{{fraction|1|4}} in
|area main=21.0 m²
|area alt=226 ft²
|aspect ratio=3.75:1
|empty weight main=6,700 kg
|empty weight alt=14,771 lb
|loaded weight main=10,750 kg
|loaded weight alt=23,700 lb
|max takeoff weight main=13,000 kg
|max takeoff weight alt=28,700 lb
|more general= '''Internal fuel capacity:''' 3,555l (2,700kg) internal.<BR>
* '''External fuel capacity:''' 2x 1,000l (760kg) in inner wing and 2x 500l (380kg) in outer wing.<ref>[http://www.fighter-planes.com/info/amx.htm "Aeritalia-Embraer-Aermacchi AMX".] ''fighter-planes.com. '' Retrieved: 9 February 2012,</ref>
|engine (jet)=[[Rolls-Royce Spey]] 807
|type of jet=[[turbofan]]
|number of jets=1
|thrust main=49.1 kN
|thrust alt=11,030 lbf
|max speed main=1053 km/h
|max speed alt=568 knots, 654 mph
|max speed more= at 10,975 m (36,000 ft)<ref name="worldmil p31">Donald and Lake 1996, p. 31.</ref>
|combat radius main=889 km
|combat radius alt=480 nmi, 553 mi
|combat radius more=(hi-lo-hi profile, 900 kg (2,000 lb) of external stores)
|ferry range main=3,336 km
|ferry range alt=1,800 nmi, 2,073 mi
|ceiling main=13,000 m
|ceiling alt=42,650 ft
|climb rate main=52.1 m/s
|climb rate alt=10,250 ft/min
|loading main=512 kg/m²
|loading alt=105 lb/ft²
|thrust/weight=0.47
|endurance= 4h 15min<ref name=Global04>[http://www.flightglobal.com/pdfarchive/view/2004/2004-09%20-%200805.html "Directory: Military Aircraft".] ''Flight Global, '' 21–25 May 2004. Retrieved: 2 February 2012.</ref>
|guns=<br/>
** 1× [[20 mm caliber|20 mm (0.787 in)]] [[M61A1 Vulcan]] [[Gatling gun#M61 Vulcan, Minigun, and other designs|6-barreled Gatling cannon]] (Italian aircraft)
** 2× [[30 mm caliber|30 mm (1.181 in)]] [[DEFA cannon|Bernardini Mk-164]] [[autocannon|cannon]] (Brazilian aircraft)
|missiles=2× [[AIM-9 Sidewinder]]s or [[Mectron MAA-1 Piranha|MAA-1 Piranha]]s or [[MAA-1B|MAA-1B Piranha II]] (under development) or [[A-Darter]] (under development),<ref>[http://www.defenseindustrydaily.com/south-africa-brazil-to-develop-adarter-sraam-03286/ "South Africa, Brazil to Develop A-Darter SRAAM".] ''Defense Industry Daily, '' 26 April 2010. Retrieved: 29 December 2012.</ref> carried on wingtip rails
|bombs=3,800 kg (8,380 lb) on 5 external hardpoints, including [[MAR-1]]<ref>[http://www.estadao.com.br/noticias/impresso,defesa-ganha-mais-espaco-na-odebrecht,703389,0.htm "Defesa ganha mais espaço na Odebrecht" (in Portuguese).] {{webarchive |url=https://web.archive.org/web/20130606123610/http://www.estadao.com.br/noticias/impresso,defesa-ganha-mais-espaco-na-odebrecht,703389,0.htm |date=6 June 2013 }} ''Estadão, '' 8 April 2011. Retrieved: 29 January 2012.</ref> missiles, general-purpose and laser-guided bombs, air-to-ground missiles, and rockets
}}

==See also==
{{Portal|Aviation}}
{{Aircontent
|related=
|similar aircraft=
* [[BAE Hawk]]
* [[Soko J-22 Orao]]
|lists=
* [[List of attack aircraft]]
* [[List of active Brazilian military aircraft]]
}}

==References==
===Citations===
{{Reflist|30em}}
===Bibliography===
{{Refbegin}}
* Air International. "Italy's Black Cats." February 2015, Key Publishing. pp. 64–69.
* Braybrook, Roy. "Assessing the AMX". ''[[Air International]]'', June 1989, Vol 36 No 6. Bromley, UK:Fine Scroll. ISSN 0306-5634. pp.&nbsp;267–278.
* Donald, David and Jon Lake. ''Encyclopedia of World Military Aircraft''. London: Aerospace Publishing, Single Volume Edition, 1996. ISBN 1-874023-95-6.
* Gunston, Bill and Peter Gilchrist. ''Jet Bombers: From the Messerschmitt Me 262 to the Stealth B-2''. Osprey, 1993. ISBN 1-85532-258-7.
* Jackson, Paul. "AMX: The 'Pocket Tornado'". ''World Air Power Journal''. Volume 5, Spring 1991. ISSN 0959-7050. pp.&nbsp;132–139.
* Lambert, Mark. ''Jane's All The World's Aircraft 1993–94''. Coulsdon, Surry, UK: Jane's Data Division, 1993. ISBN 0-7106-1066-1.
* Metius, Wojtek. "AMX IOC: Service use for Italy's new agile attacker". ''Air International'', October 1992, Vol 43 No 4. Stamford, UK:Key Publishing. ISSN 0306-5634. pp.&nbsp;222–225.
* Niccoli, Ricardo. "AMX In Italian Service". ''Air International'', June 1997, Vol 52 No 6. Stamford, UK:Key Publishing. ISSN 0306-5634. pp.&nbsp;349–353.
* Niccoli, Ricardo. "AMX: Upgraded and Ready for Combat". ''Air International'', November 2009, Vol 77 No 5. Stamford, UK:Key Publishing. ISSN 0306-5634. pp.&nbsp;42–45.
* Warwick, Graham. [http://www.flightglobal.com/pdfarchive/view/1981/1981%20-%203652.html "AMX design reflects Tornado experience"]. ''[[Flight International]]'', 21 November 1981. pp.&nbsp;1544–1545.
{{Refend}}

==External links==
{{Commons|AMX International AMX}}
* [https://web.archive.org/web/20120719100043/http://www.aleniaaermacchi.it:80/Eng/Military/Proprietari/Pages/AMX.aspx AMX page on Alenia Aermacchi]
* [https://web.archive.org/web/20101227171318/http://www.vectorsite.net/avamx.html The Alenia-Aermacchi-EMBRAER AMX]
* [http://www.militaryfactory.com/aircraft/detail.asp?aircraft_id=464 AMX International AMX (Ghibli)]

{{Alenia aircraft}}
{{Aermacchi aircraft}}
{{Embraer}}
{{Italian military aircraft}}

[[Category:Aeritalia aircraft]]
[[Category:Alenia aircraft]]
[[Category:Embraer aircraft]]
[[Category:International attack aircraft 1980–1989]]
[[Category:Macchi aircraft]]
[[Category:Brazil–Italy relations]]