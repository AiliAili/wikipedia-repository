{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name          =Ernest Arthur Deighton
|image         =
|image_size    =
|alt           =
|caption       = 
|birth_date    ={{Birth date|1889|5|28|df=y}}
|death_date    ={{Death date and age|1957|12|5|1889|5|28|df=y}}
|birth_place   =[[Masham|Masham, Yorkshire]], England
|death_place   =[[Bournemouth|Bournemouth, Hampshire]], England
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =
|allegiance    = United Kingdom
|branch        = Royal Air Force<br/>British Territorial Army Reserve
|serviceyears  =1917–1918<br/>1942–1945
|rank          =Sergeant
|servicenumber = <!--Do not use data from primary sources such as service records.-->
|unit          =[[No. 20 Squadron RAF]]<br/>[[Army Cadet Force]]
|commands      =
|battles       =[[World War I]]<br/>[[World War II]]
|battles_label =
|awards        =[[Distinguished Conduct Medal]]
|spouse        = <!-- Add spouse if reliably sourced -->
|relations     =
|laterwork     =
|signature     =
}}
Sergeant '''Ernest Arthur Deighton''' {{post-nominals|country=GBR|DCM}} (28 May 1889 – 5 December 1957) was an English World War I observer/gunner [[flying ace]] credited with 15 confirmed aerial victories; all but one of them was against enemy fighters.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/deighton.php |title=Ernest Arthur Deighton |work=The Aerodrome |year=2014 |accessdate=21 November 2014 }}</ref>

==Military career==
===World War I===
Deighton enlisted in the [[Royal Flying Corps]] on 15 March 1917 as a transport driver. As a [[corporal]] mechanic, he volunteered to fly as an observer/gunner in the rear seat of No. 20 Squadron's [[Bristol F.2 Fighter]]s.<ref name="AWF13">Franks ''et.al.'' (1997), p.13.</ref> 

Deighton flew as an observer/gunner for four pilot aces: Captain [[Wilfred Beaver]], Lieutenants [[David John Weston|David Weston]], Leslie Capel, and [[Ernest Lindup]]. Deighton scored his first victory on 11 April 1918 and closed out his string on 23 June 1918. In total, he was credited with destroying ten enemy fighters and an observation plane, and with four other German fighters claimed 'out of control'.<ref name="AWF13"/>

Deighton was awarded the [[Distinguished Conduct Medal]] on 7 June 1918. He and Beaver were forced down by a German [[Albatros D.V]] on 13 June 1918, but were uninjured. However, Deighton was subsequently injured on 15 July 1918 and returned to Britain.<ref name="AWF13"/>

Deighton's Distinguished Conduct Medal was gazetted to him on 1 October 1918. The award citation read:

:67051 Corporal (Acting-Serjeant) E. A. Deighton, Royal Air Force. (Cheltenham).
:For conspicuous gallantry and devotion to duty. In little more than a fortnight he has shot down five enemy aircraft. He has shown remarkable marksmanship and coolness in action, and is a valuable asset in his squadron.<ref>{{London Gazette |date=1 October 1918 |supp=yes |issue=30932 |startpage=11668 |nolink=yes}}</ref>

Unusually, the original award recommendations still exist. One of them was written on 27 May 1918; Captain Beaver followed up with a second one two days later. Both give more detailed accounts of Deighton's exploits than the award citation, mainly enumerating his aerial victories.<ref>{{cite web |url= http://rcafassociation.ca/uploads/airforce/2009/07/gong-1a-b.html |title=First World War Honours and Awards to Canadians in British Flying Services |first=H.A. |last=Halliday |work=rcafassociation.ca |year=2009 |accessdate=21 November 2014}}</ref>

===World War II===
Deighton apparently returned to service during World War II, being commissioned as a second lieutenant in the Warwickshire [[Army Cadet Force]]<ref>{{London Gazette |date=12 March 1943 |supp=yes |issue=35939 |startpage=1242 |endpage=1243 |nolink=yes}}</ref> on 25 November 1942.<ref>{{London Gazette |date=17 September 1943 |supp=yes |issue=36177 |startpage=4176 |nolink=yes}}</ref> He finally resigned his commission on 12 May 1945.<ref>{{London Gazette |date=12 June 1945 |supp=yes |issue=37130 |startpage=3124 |nolink=yes}}</ref>

Nothing more is known of him until his death in [[Bournemouth]], England on 5 December 1957.<ref name="AWF13"/>

== References ==
{{reflist|30em}}

== Bibliography ==
* {{cite book |first1=Norman |last1=Franks |authorlink1=Norman Franks |first2=Russell F. |last2=Guest |first3=Gregory |last3=Alegi |title=Above the War Fronts: The British Two-seater Bomber Pilot and Observer Aces, the British Two-seater Fighter Observer Aces, and the Belgian, Italian, Austro-Hungarian and Russian Fighter Aces, 1914–1918 |location=London, UK |publisher=Grub Street |year=1997 |isbn=978-1-898697-56-5 |lastauthoramp=yes}}
* {{cite book |title=Bristol F2 Fighter Aces of World War I |first1=Jon |last1=Guttman |first2=Harry |last2=Dempsey |publisher=Osprey Publishing |year=2007 |isbn=978-1-84603-201-1 |lastauthoramp=yes}}

{{DEFAULTSORT:Deighton, Ernest}}
[[Category:1889 births]]
[[Category:1957 deaths]]
[[Category:People from Harrogate (district)]]
[[Category:Royal Flying Corps soldiers]]
[[Category:Recipients of the Distinguished Conduct Medal]]
[[Category:British World War I flying aces]]