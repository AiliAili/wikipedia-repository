{{Infobox journal
| title = Aliso
| discipline = [[Botany]]
| cover =
| website = http://scholarship.claremont.edu/aliso/
| publisher = [[Rancho Santa Ana Botanic Garden]]
| country = United States
| abbreviation = Aliso
| history = 1948–present
| openaccess=[[Delayed open access journal|Delayed]] one year<ref name="Journal Home"/>
| ISSN          =0065-6275
| eISSN         =2327-2929
}}
'''''Aliso: A Journal of Systematic and Evolutionary Botany''''' is a [[peer review|peer-reviewed]] [[scientific journal]] that publishes original research on plant [[Taxonomy (biology)|taxonomy]] and evolutionary botany with a worldwide scope, but with a particular focus on the [[floristics]] of the [[Western United States]]. ''Aliso'', first published in 1948, is the scientific journal of the [[Rancho Santa Ana Botanic Garden]]. The journal is named for the western sycamore, ''[[Platanus racemosa]]'', which was commonly called by its Spanish name ''aliso''.<ref name="Journal Home">{{cite web | title = About this journal | url = http://scholarship.claremont.edu/aliso/about.html | publisher = Rancho Santa Ana Botanic Garden,  | accessdate = 10 November 2013}}</ref>

It is noted as the journal where [[Robert Folger Thorne|Robert F. Thorne]] first published the [[Thorne system (1992)|Thorne system]] of flowering plant classification in 1968.<ref name="textbook">Sharma, O. P. 2009. ''[https://books.google.com/books?id=Roi0lwSXFnUC&pg=PA13 Plant Taxonomy]'', 2nd edition. New Delhi: Tata McGraw-Hill. p. 13. ISBN 978-0-07-014159-9</ref>

==References==
{{reflist}}

==External links==
* {{official website|http://www.rsabg.org/scientific-publications}}

[[Category:Botany journals]]
[[Category:Publications established in 1948]]
[[Category:English-language journals]]


{{botany-journal-stub}}