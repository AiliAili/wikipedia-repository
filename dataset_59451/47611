{{Orphan|date=February 2012}}

'''John Dugmore''' of Swaffham (1793-1871) was a British draughtsman and grand-tourist. He realized at least 130 drawings and watercolours, most of them in Germany, France and Italy.

==Youth==
Born in a Norfolk noble family,<ref>The Dugmore of Swaffham family archives are filed by the [http://www.nationalarchives.gov.uk/A2A/records.aspx?cat=153-mc386&cid=17#17 UK National Archives]</ref> he had a deep classical education. Renowned for his taste in arts, he moved to London to seek his fame and fortune at the Royal Court, where he met his ‘patron’, [[William Keppel, 4th Earl of Albemarle|William Charles Keppel (1772-1849), 4th Earl of Albermarle]]. Dugmore was probably responsible for Keppel children’s education.

==The Grand Tour==
In 1820, Dugmore accompanied in the [[Grand Tour]] a son of Charles Keppel, perhaps George Thomas (1799-1891), later 6th Earl of [[Earl of Albemarle|Albermarle]], Viscount of [[Bury]] and Baron of [[Ashford, Kent|Ashford]], who made a brilliant military career (started at [[Battle of Waterloo|Waterloo]]) as well as was a memorialist, a distinguished collector and a member of the English Society of Antiquaires.
They moved from Scotland to Western Bohemia, France, Switzerland and Italy. In around 20 months, Dugmore fixed on paper everything was surprising him, mostly mountains, lakes, rivers and other natural beauties. Sometimes he sketches a strange architecture or an historical place, for instance the cell in the [[Great St Bernard Hospice|Great St. Bernard Hospice]] where [[Napoleon]] slept during the cross of the Alps (1800) or the Clermont Castle, former residence of [[Blaise Pascal]].

==Works==
Lady Hutton, John Dugmore’s daughter-in-law, mentioned the existence of many albums filled by 130 c. drawings. 65 drawings are recorded in detail in Lady Hutton’s last will. Among them, 47 were recently discovered by Guy Peppiat and offered on sale at an English auction house.<ref>Jeremy Highsmith, ''Tre importanti nuclei di disegni'', in ''Quadri e Sculture'', VIII, 2001, 37, p. 92</ref> Another one is filed in Sabin Galleries archives, London. These works are usually made on a slightly cream paper, 260x200 mm., by pencil, pen and black ink.<ref>The 47 mentioned works are listed in Giacomo Pintus, Entries 117-118, in ''Idea y Sentimiento. Itinerarios por el dibujo de Rafael a Cézanne'', Barcelona, 2007, pp. 299-301.</ref>

==Value==
Dugmore's urban landscapes are visual witnesses of the shape of many main and minor European cities, among them Paris, [[Vichy]], [[Clermont-Ferrand]], Lyon, [[Schandau]], [[Tivoli, Lazio|Tivoli]], [[Susa, Piedmont|Susa]], Florence, [[Siena]].
Dugmore is relevant for his ability in feeling and amplifying the esthetic news which he was meeting place by place. For instance, his ‘German’ drawings are very linear and pure; on the opposite side his ‘French’ drawings are ‘touched’ by pencil and brush in a way that seems largely to anticipate the impressionist idea of the light.

==Museums==
Three works by Dugmore are included in the ''Progetto Linea'', a large private project for increasing the paper cabinets in Italian public museums of art.

==References==
{{reflist}}

{{DEFAULTSORT:Dugmore, John}}
[[Category:1793 births]]
[[Category:1871 deaths]]
[[Category:British draughtsmen]]
[[Category:British watercolourists]]