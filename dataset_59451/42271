<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Husky
 |image= File:AviatA-1BHuskyC-GTHY01.jpg
 |caption=Aviat A-1B Husky
}}{{Infobox Aircraft Type
 |type=Light [[utility aircraft]]
 |manufacturer=[[Aviat]]
 |design group=[[Christen Industries]]
 |first flight=1986
 |introduced=1987
 |retired=
 |status=Active service
 |primary user=
 |more users=
 |produced=<!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
 |number built=650+<ref name="AviatAbout">[http://www.aviataircraft.com/about.html "Aviat Aircraft: About Us."] {{webarchive |url=https://web.archive.org/web/20080803021732/http://www.aviataircraft.com/about.html |date=August 3, 2008 }} ''[[Aviat Aircraft]]'', 2008. Retrieved: May 4, 2012.</ref>
 |unit cost=
 |variants with their own articles=
}}
|}
[[File:HuskyA1CFloat.jpg|thumb|Husky A-1C on floats]]

The '''Aviat Husky''' is a tandem two-seat, high-wing, utility [[light aircraft]] built by [[Aviat|Aviat Aircraft]] of [[Afton, Wyoming]].<ref name="airliners.net">[http://www.airliners.net/aircraft-data/stats.main?id=44 "The Aviat A-1 Husky."] ''Demand Media,'' 2008. Retrieved: May 4, 2012.</ref>

It is the only all-new light aircraft that was designed and entered series production in the [[United States]] in the mid-to-late 1980s.<ref name="airliners.net"/>

==Development==
Design work by [[Christen Industries]] began in 1985. The aircraft is one of the few in its class designed with the benefit of [[Computer Aided Design|CAD software]]. The [[prototype]] first flew in [[1986 in aviation|1986]], and [[Type certificate|certification]] was awarded the following year.<ref name="airliners.net"/>

The Husky has been one of the best-selling light aircraft designs of the last 20 years, with more than 650 sold since production began.<ref name="AviatAbout"/>

==Design==
The Husky features a braced high wing, tandem seating and dual controls. The structure is steel tube frames and [[Dacron]] covering over all but the rear of the [[fuselage]], plus metal leading edges on the wings. The high wing was selected for good all-around visibility, making the Husky ideal for observation and patrol roles.  Power is supplied by a relatively powerful (for the Husky's weight) {{convert|180|hp|kW|0|abbr=on}} [[Lycoming O-360|Textron Lycoming O-360]] flat-four [[piston engine]] turning a [[constant speed propeller]]. In 2015 a reversible MT Propeller was approved under a [[Supplementary Type Certificate]] for better control during [[floatplane]] water operations.<ref>{{cite web|title=Reversible prop approved for Husky|accessdate=24 April 2015|url=http://www.aopa.org/News-and-Video/All-News/2015/April/24/Reversible-prop-approved-for-Husky}}</ref>  The Husky's high [[power-to-weight ratio]] and low [[wing loading]] result in good short-field performance.<ref name="airliners.net"/>

Options include floats, skis and banner and [[Glider (sailplane)|glider]] tow hooks.<ref name="airliners.net"/>

==Operational history==
The Husky has been used for observation duties, fisheries patrol, pipeline inspection, glider towing, border patrol and other utility missions. Notable users include the [[United States Department of the Interior|US Department of the Interior]] and [[United States Department of Agriculture|Agriculture]] and the [[Kenya Wildlife Service]], which flies seven on aerial patrols of [[elephant]] herds as part of the fight against illegal ivory [[poaching]].<ref name="airliners.net"/>

==Variants==
[[File:G-HSKI-Huskie-095.jpg|thumb|right|A 2005-built A-1B Husky at Biggin Hill, modified with a {{convert|200|hp|kW|0|abbr=on}} [[Lycoming O-360|Lycoming IO-360-A1D6]] engine]]
The Husky comes in six versions:<ref name="AviatSpecs">[http://www.aviataircraft.com/hspecs.html "Aviat Husky Specs."] ''Aviat Aircraft,'' 2009. Retrieved: May 4, 2012.</ref>
;Husky A-1
:Certified on 1 May 1987. Maximum gross weight is {{convert|1800|lb|kg|0|abbr=on}}. Powered by a [[Lycoming O-360|Lycoming 0-360-A1P]] or a [[Lycoming O-360|Lycoming O-360-C1G]] of {{convert|180|hp|kW|0|abbr=on}}<ref name="A22NM">[http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/937a0e85d3e3d53586257561004b2d93/$FILE/A22NM.pdf "Type certificate Data Sheet No. A22NM."] ''[[Federal Aviation Administration]],'' February 2009. Retrieved: December 18, 2009.</ref>
;Husky A-1A
:Certified on 28 January 1998. Maximum gross weight is {{convert|1890|lb|kg|0|abbr=on}}. Powered by a [[Lycoming O-360|Lycoming 0-360-A1P]] of {{convert|180|hp|kW|0|abbr=on}}<ref name="A22NM" />
[[File:Husky at KAPA.jpg|thumb|Aviat A-1B Husky]]
;{{visible anchor|Husky A-1B}}
:Certified on 28 January 1998. Powered by a [[Lycoming O-360|Lycoming 0-360-A1P]] of {{convert|180|hp|kW|0|abbr=on}}<ref name="A22NM" /> The A-1B can be modified to accept a [[Lycoming O-360|Lycoming IO-360-A1D6]] engine of {{convert|200|hp|kW|0|abbr=on}} and an MT MTV-15-B/205-58 propeller under an [[Supplemental type certificate|STC]].<ref name="SA10463SC">[http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgSTC.nsf/0/8c1897437cc9e115862571f50064d919/$FILE/sa10463sc.pdf "Supplemental type certificate SA10463SC."] ''[[Federal Aviation Administration]],'' December 2005. Retrieved: December 18, 2009.</ref>
;Husky A-1B-160 Pup
:Certified on 18 August 2003 without [[Flap (aircraft)|flaps]] and 21 October 2005 with flaps. Powered by a [[Lycoming O-320|Lycoming 0-320-D2A]], {{convert|160|hp|kW|0|abbr=on}}. The Pup has a smaller engine, a gross weight of {{convert|2000|lb|kg|0|abbr=on}} and a useful load of {{convert|775|lb|kg|0|abbr=on}}<ref name="AviatSpecs"/><ref name="A22NM" />
;Husky A-1C-180
[[File:Aviat Husky A1C Cockpit.jpg|thumb|right|A Garmin equipped A-1C cockpit]]
:Certified on 24 September 2007. Powered by a [[Lycoming O-360|Lycoming 0-360-A1P]] of {{convert|180|hp|kW|0|abbr=on}}. The 180 has a gross weight of {{convert|2200|lb|kg|0|abbr=on}} and a useful load of {{convert|925|lb|kg|0|abbr=on}}<ref name="AviatSpecs"/><ref name="A22NM" />
;Husky A-1C-200
:Certified on 24 September 2007. Powered by a [[Lycoming O-360|Lycoming IO-360-A1D6]] of {{convert|200|hp|kW|0|abbr=on}}. The 200 has a gross weight of {{convert|2200|lb|kg|0|abbr=on}} and a useful load of {{convert|880|lb|kg|0|abbr=on}}<ref name="AviatSpecs"/><ref name="A22NM" />

==Specifications (A-1C Husky)==
{{Aircraft specs
|ref=Aviat website<ref>{{cite web|title=Husky A-1C |url=http://aviataircraft.com/husky-specifications/ |website=aviataircraft |publisher=Aviat Aviation |accessdate=29 March 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20160401041115/http://aviataircraft.com/husky-specifications/ |archivedate=1 April 2016 |df= }}</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=one passenger
|length m=
|length ft=22
|length in=7
|length note=
|span m=
|span ft=35
|span in=6
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=183
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=1,275
|empty weight note=on wheels
|gross weight kg=
|gross weight lb=2,200
|gross weight note=on wheels and floats
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=50 US gallons (190 litres)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming O-360|Lycoming O-360-A1P]]
|eng1 type=four cylinder, [[four stroke]] piston [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=180<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 note=
|power original=
|prop blade number=2<!-- propeller aircraft -->
|prop name=[[Hartzell Propeller]]
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=6<!-- propeller aircraft -->
|prop dia in=4<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=145
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=140
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=53<!-- aerobatic -->
|stall speed kts=
|stall speed note=flaps down, power off
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=800
|range nmi=
|range note=at 55% power
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=20000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=1500
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
*[[Airband|VHF comm radio]]
*[[Transponder (aviation)|Transponder]]
*[[GPS]] optional
}}

==See also==
{{aircontent
|related=
*[[Piper Super Cub]]
|similar aircraft=
*[[8GCBC Scout|American Champion/Bellanca Scout]]
|lists=
|see also=
}}

==References==
;Notes
{{reflist}}

==External links==
{{Commonscat|Aviat A-1 Husky}}
* {{Official website|http://www.aviataircraft.com/ }}

[[Category:Aviat aircraft|Husky]]
[[Category:High-wing aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:United States civil utility aircraft 1980–1989]]
[[Category:Glider tugs]]