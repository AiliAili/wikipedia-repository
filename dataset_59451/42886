{{Infobox company
| name             = Grob Aircraft
| logo             = [[File:Logo Grob Aircraft H3.jpg|250px|Logo Grob Aircraft H3]]| type             = 
| genre            = 
| predecessor      = 
| foundation       = [[Germany]] ({{Start date|1971}})
| founder          = Burkhart Grob
| location_city    = [[Tussenhausen]]
| location_country = [[Germany]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = André Hiebeler <small>([[Chief executive officer|CEO]])</small>
Andreas Berninger <small>([[Chief Financial Officer|CFO]])</small>
| industry         = [[Aerospace]]
| products         = [[Aircraft]]
| services         = 
| market cap       = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 250 (2015){{Citation needed|date=December 2015}}
| parent           = H3 Aerospace GmbH & Co KG
| divisions        = 
| subsid           =
| homepage         = [http://www.grob-aircraft.com/ www.grob-aircraft.com]
| footnotes        = 
| intl             = 
}}
[[File:Grob 103 C-GIAK 09.JPG|thumb|right|[[Grob G 103 Twin Astir]] sailplane]]
[[File:grob.g109b.glider.arp.jpg|thumb|right|[[Grob G 109]] B motor glider, built in 1986]]
[[File:Grob G 115E EA-3.JPG|thumbnail|right|Grob 115E 'Tutor T1' operated by the [[Royal Air Force]]]]
[[File:Grob Spn3.jpg|thumb|[[Grob G180 SPn|Grob SP<sup>n</sup>]]]]

'''Grob Aircraft''' is a [[Germany|German]] [[aircraft]] manufacturer, previously known as ''Grob Aerospace''. It has been manufacturing aircraft using [[carbon fiber reinforced polymer]] since the 1970s.<ref>{{cite web | url = http://www.dw-world.de/dw/article/0,,3363309,00.html?maca=en-podcast_made-in-germany_en-2108-xml-mrss | title = One of the Last German Airplane Manufacturers&nbsp;— Grob Aerospace | accessdate=2009-02-25}}</ref>

==History==
The company was founded in 1971 by Dr Burkhart Grob, son of Ernst Grob, who had started producing internal combustion engines in 1926 and later made other parts for the automobile industry. Grob Aerospace became involved in [[Glider (sailplane)|glider]] manufacture in 1971 when it was sub-contracted by [[Schempp-Hirth]] to build the [[Schempp-Hirth Standard Cirrus|Standard Cirrus]] under licence. Burkhart Grob was a power pilot and a glider pilot already.

Two hundred Standard Cirruses were built by Grob at its own Tussenhausen-Mattsies airfield between 1971 and 1975. In 1974 Grob decided to continue with glider production independently using its experience with fibre-composite construction. Instead of building competitive gliders, they decided to build for the club market at competitive prices. The result was the G-102 Astir. The two-seat G 103 Twin Astir followed shortly after.

In the late 1970s Grob also decided to build the G 109, the world's first production all-composite [[motor glider]] which was certified in 1981. This was following by the G 115 which was certified by the FAA in 1987. In the late 1980s the G 520 [[Egrett]]/STRATO 1 high-altitude aircraft was produced which established five world records. Glider production ceased in 1996 and production concentrated on powered aircraft.

===SPn program===
The [[Grob G180 SPn]], six-passenger jet, first flew in 2005. The second prototype crashed shortly after takeoff on November 29, 2006 near the production plant in Germany. Chief test pilot [[Gérard Guillaumaud]], the aircraft's sole occupant, was killed.<ref name="FlighGlobal05Dec06">{{cite web|url = http://www.flightglobal.com/articles/2006/12/05/210909/grob-resolute-after-crash-destroys-spn-prototype.html|title = Chief test pilot killed as aircraft comes down near factory during demonstration flight|accessdate = 2009-09-07|last = Sarsfield|first = Kate|authorlink = |date=December 2006}}</ref>

Grob Aerospace's largest creditor did not approve the sale of the SPn to Guizhou Aircraft Industry Corporation and instead retained it. Niall Olver, Grob Aerospace's former CEO, was appointed by the creditor to find new investors to buy the SPn assets with the aim of restarting the program by June 2009. Olver indicated in March 2009 that the plan was to complete certification and commence production near the middle of 2012.<ref name="FlightGlobal25Mar09">{{cite web|url = http://www.flightglobal.com/articles/2009/03/25/324272/spn-light-business-jet-nearer-to-resurrection.html|title = SPn light business jet nearer to resurrection |accessdate = 2009-03-13|last = Sarsfield|first = Kate|authorlink = |date=March 2009}}</ref>

===2008 insolvency===
On 18 August 2008, Grob Aerospace filed for insolvency, suspending their light-jet production and calling into question timely delivery of [[Bombardier Aerospace]]'s [[Learjet 85]] prototypes which Grob was contracted to build. The company was unable to find any investors for its SPn jet project. Most employees were released on 3 November 2008.<ref name="AWST19Aug08">{{cite web|url = http://www.aviationweek.com/aw/generic/story_generic.jsp?channel=busav&id=news/GROB08198.xml&headline=Grob%20Aerospace%20Files%20For%20Insolvency|title = Grob Aerospace Files For Insolvency|accessdate = 2008-10-31|last = Collogan|first = David|authorlink = |date=August 2008}}</ref><ref name="AOPA30Oct08">{{cite web|url = http://www.aopa.org/aircraft/articles/2008/081030industry.html|title = Credit crunch felt by GA manufacturers|accessdate = 2008-10-31|last = Marsh|first = Alton K.|authorlink = |date=October 2008}}</ref>

Niall Olver, Grob's chief executive officer said:

{{cquote|This unfortunate situation has arisen fairly rapidly off the back of recent delays in the SPn program, resulting in commensurately increased cash requirement to see the aircraft through to certification. Our current loan provider has elected to discontinue support, with immediate effect.<ref name="AWST19Aug08"/> }}

On 16 December 2008 Grob insolvency administrator Dr. Michael Jaffé announced that two parties had expressed interest in taking over the insolvent company, [[Munich]] based [[H3 Aerospace]] and the [[People's Republic of China|Chinese]] [[Guizhou Aircraft Industry Corporation]]. Both companies offered about $4.5 million for Grob, with Guizhou also offering an additional $3.5 million for the SPn jet program.<ref name="AInOnline.com">{{cite web|url = http://www.ainonline.com/news/single-news-page/article/two-buyers-interested-in-acquiring-grob/|title = Two Buyers Interested in Acquiring Grob|accessdate = 2008-12-19|last = Stocker|first = Thomas|authorlink = |date=December 2008}}</ref>

==Grob Aircraft AG==
[[File:Grob Aircraft Fleet.JPG|thumb|[[Grob G 120|G 120TP]], G 120A, [[Grob G 115|G 115E]]]]
[[File:Grob Aircraft Formation.jpg|thumb|Formation flight of G 120TP (D-ETPX) and G 120A (D-ESAI)]]
In January 2009 H3 Aerospace bought the training aircraft and support business of Grob Aerospace and renamed it '''Grob Aircraft AG'''. The training aircraft production, which had been halted in November 2008, was restarted in February 2009.<ref name="FlightGlobal25Mar09"/><ref>{{cite web | url = http://www.aviationweek.com/aw/generic/story_channel.jsp?channel=busav&id=news/GROB012809.xml | title = Grob Aerospace Buyer Found | accessdate=2009-02-25}}</ref>

In April 2012 it was announced that Argentina's government-controlled Aircraft Factory [[Fabrica Argentina de Aviones|FAdeA]] plans to produce 100 [[FMA IA 63 Pampa|IA-63 Pampa II]] training and combat aircraft at its plant in Cordoba in association with Grob Aircraft AG. The Pampa II aircraft will have several parts for its updated version supplied by Grob.<ref>[http://en.mercopress.com/2012/04/10/argentine-factory-begins-production-of-pampa-training-aircraft-with-german-help Argentine factory begins production of Pampa training aircraft with German help], (April 10th 2012).</ref>

===Grob Training Systems (GTS)===
Grob Training Systems provides a ground-based training system (GBTS) for the G 120TP, which includes aircrew and maintenance technician training as well as training system logistics support. The system typically consists of computer-based classroom training  and G 120TP Flight Training Devices.<ref>{{cite web|url=https://grob-aircraft.com/en/training-systems.html|title=Training Systems|author= Grob Aircraft AG|work=grob-aircraft.com|accessdate=17 October 2016}}</ref>

The G 120TP Flight Training Device (FTD) features a G 120TP cockpit that is used to train basic and emergency procedures. A dome display screen is mounted to allow advanced flight maneuvers and formation flight training, in addition to basic flight training.{{Citation needed|date=October 2016}} 

==Aircraft Types==
[[File:Grob120-D-ETPG.jpg|thumb|Grob Aircraft G 120TP]]
;Produced by Grob Aircraft AG
* [[Grob G 115E]] - Trainer
* [[Grob G-120|Grob G 120]] - Trainer
* [[Grob G 120TP]] - Turboprop trainer
* [[Grob G 520 Egrett]] - High altitude reconnaissance and surveillance aircraft

;Produced by Grob Aerospace GmbH
* [[Grob G 102 Astir]] - single-seat composite construction [[Glider Competition Classes#Standard Class|Standard Class]] sailplane
* Grob G 103 - two-seat sailplane
**[[Grob G103 Twin Astir|G 103 Twin Astir]]
** [[Grob G103a Twin II|G 103 Twin II]] (also available in Acro version [[G 103a Twin II]])
** [[G 103C Twin III]] (also available in Acro versions)
* [[Grob G 104 Speed Astir]] - single-seat [[Glider Competition Classes##15 metre Class|15 metre Class]] sailplane
* [[Grob G 109]] - self-launching two-seat motorglider
* [[Grob G 115]] - Low wing all-composite two-seat aerobatic monoplane
* [[Grob G 116]]
* [[Grob G 120]] - Trainer
* [[Grob G 140]] four seat turboprop
* [[Grob GF 200]] - Business aircraft
* [[Grob G 520 Egrett|Grob G 520 Egrett/STRATO 1]]
* [[Grob G180 SPn|Grob SP<sup>n</sup>]] business jet and variants
* [[HALE G 600]] High altitude long endurance variant of the SPn
* [[Grob Strato|Grob G 850 Strato 2c]]

==References==
{{reflist}}

==External links==
{{commons category|Grob Aircraft}}
* [http://www.grob-aircraft.com/ Homepage of Grob Aircraft]
* [http://www.avbuyer.com/Articles/Article.asp?Id=769 Grob SPn Update from World Aircraft Sales]
* [http://video.images-de-prestige.com/aircrafts.php?com=Grob Grob SPn video]

{{Grob aircraft}}

{{coord|48|06|43|N|10|31|25|E|type:landmark_scale:5000_region:DE|display=title}}

[[Category:Aircraft manufacturers of Germany]]