<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=FP-202 Koala & Super Koala
 | image=Fisher FP-202 Koala D-MKOA.jpg
 | caption=FP-202 Koala
}}{{Infobox Aircraft Type
 | type=[[Kit aircraft]]
 | national origin=[[Canada]]
 | manufacturer=[[Fisher Flying Products]]
 | designer=
 | first flight=FP-202: 1981<br />Super Koala: 1982
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=FP-202: 330 (2011)<ref name="KitplanesDec2011" /><br />Super Koala: 105 (2011)<ref name="KitplanesDec2011" />
 | developed from= [[Piper J-3]]
 | variants with their own articles=
}}
|}
[[File:Fisher FP-202 Koala D-MKOA fuselage.jpg|thumb|right|Detail of FP-202 internal structure]]
The '''Fisher FP-202 Koala''' and '''Super Koala''' are a family of [[Canada|Canadian]] single and two seat high wing, [[conventional landing gear]], single engined light [[kit aircraft]] designed for construction by amateur builders. Both aircraft were inspired by the design of the [[Piper J-3 Cub]] and strongly resemble that design.<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2011 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 53-54. Belvoir Publications. ISSN 0891-1851</ref><ref name="KitplanesDec2004">Downey, Julia: ''Kit Aircraft Directory 2005'', Kitplanes, Volume 21, Number 12, December 2004, page 57. Belvoir Publications. ISSN 0891-1851</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook'', page 160. BAI Communications. ISBN 0-9636409-4-1</ref><ref name="KitplanesDec1998">Kitplanes Staff: ''1999 Kit Aircraft Directory'', Kitplanes, Volume 15, Number 12, December 1998, page 47. Primedia Publications. IPM 0462012</ref><ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page B-21 Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref><ref name="FisherFP202">{{cite web|url = http://www.fisherflying.com/index.php?option=com_content&view=article&id=31&Itemid=22|title = FP-202 |accessdate = 2009-10-15|last = Fisher Flying Products|authorlink = |year = n.d.}}</ref><ref name="FisherSuperKoala">{{cite web|url = http://www.fisherflying.com/index.php?option=com_content&view=article&id=43&Itemid=22|title = Super Koala |accessdate = 2009-10-15|last = Fisher Flying Products|authorlink = |year = n.d.}}</ref>

Fisher Flying Products was originally based in [[Edgeley, North Dakota]], [[United States|USA]] but the company is now located in [[Woodbridge, Ontario]], [[Canada]].<ref name="KitplanesDec2004" /><ref name="Aerocrafter" /><ref name="KitplanesDec1998" /><ref name="Cliche" /><ref name="FisherFP202" /><ref name="FisherSuperKoala" />

==Development==
The FP-202 was designed by Fisher Aircraft in the [[United States]] in 1981 and was intended to meet the requirements of the US [[Ultralight aircraft (United States)|FAR 103 ''Ultralight Vehicles'']] category, including that category's maximum {{convert|254|lb|kg|0|abbr=on}} empty weight. The design goal was to provide ultralight pilots with an aircraft that looked like and flew like the classic Piper Cub, without the regulation that goes with owning a [[type certificate|type certified]] aircraft. The FP-202 can achieve an empty weight of {{convert|250|lb|kg|0|abbr=on}} when equipped with a lightweight, two-stroke engine.<ref name="Aerocrafter" /><ref name="Cliche" /><ref name="FisherFP202" />

The Super Koala was first flown in 1983 and has two side by side seats arrangement. With its {{convert|400|lb|kg|0|abbr=on}} empty weight and {{convert|830|lb|kg|0|abbr=on}} maximum gross weight, the Super Koala was intended for the US [[Homebuilt aircraft]] category.<ref name="Aerocrafter" /><ref name="Cliche" /><ref name="FisherSuperKoala" />

The construction of the FP-202 and Super Koala are unusual for aircraft in their class. The aircraft's structure is entirely made from wood, with the wooden [[fuselage]] built from wood strips arranged in a [[geodesic]] form, resulting in a very strong and light aircraft with redundant load paths. Like the Cub, both the wings and fuselage on the Koalas are covered with doped [[aircraft fabric]]. The wings are [[strut]]-braced and utilize [[jury strut]]s. The landing gear is [[bungee cord|bungee]] suspended and the tail wheel is steerable. The Super Koala has flaps and brakes are optional on both designs. The company claims it would take the amateur constructor between 250–500 hours to build the FP-202 and 500 hours for the Super Koala.<ref name="Aerocrafter" /><ref name="Cliche" /><ref name="FisherFP202" /><ref name="FisherSuperKoala" />
<!-- ==Operational history== -->

==Variants==
[[File:Fisher FP-202 Koala D-MKOA front.jpg|thumb|right|FP-202 quarter front view]]
;FP-202 Koala
:Single seat, high wing ultralight aircraft, standard empty weight {{convert|250|lb|kg|0|abbr=on}} with {{convert|28|hp|kW|0|abbr=on}} [[Rotax 277]] engine<ref name="Aerocrafter" /><ref name="Cliche" /><ref name="FisherFP202" />
;Super Koala
:Two seat side by side, high wing homebuilt aircraft, standard empty weight {{convert|400|lb|kg|0|abbr=on}}. Engine options are the {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]] and the  {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine. With the Rotax 503 the gross weight is {{convert|740|lb|kg|0|abbr=on}} and with the Rotax 582 is {{convert|830|lb|kg|0|abbr=on}}.<ref name="Aerocrafter" /><ref name="Cliche" /><ref name="FisherSuperKoala" />
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Super Koala) ==
[[File:Fisher FP-202 Koala D-MKOA cockpit.jpg|thumb|right|FP-202 instrument panel]]
{{aircraft specifications

|plane or copter?=plane
|jet or prop?=prop

|ref=Company website, Kitplanes and Cliche<ref name="KitplanesDec2004" /><ref name="Aerocrafter" /><ref name="KitplanesDec1998" /><ref name="Cliche" /><ref name="FisherSuperKoala" /><!-- the source(s) for the information -->

|crew=one
|capacity=one passenger<!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 18 ft 1 in
|length alt=5.52 m
|span main=31 ft 0 in
|span alt=9.46 m
|height main=5 ft 7 in
|height alt=1.70 m
|area main= 140 sq ft
|area alt= 13.02 sq m
|airfoil=
|empty weight main= 400 lbs
|empty weight alt= 181 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 430 lb
|useful load alt= 195 kg
|max takeoff weight main= 830 lbs
|max takeoff weight alt= 376 kg
|max takeoff weight more=
|more general=

|engine (jet)=
|type of jet=
|number of jets=
|thrust main= 
|thrust alt= 
|thrust original=
|afterburning thrust main=
|afterburning thrust alt= 
|thrust more=

|engine (prop)=[[Rotax 582]]
|type of prop=two cylinder, two-stroke piston engine <!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 64 hp
|power alt=48 kW
|power original=
|power more=

|propeller or rotor?=propeller
|propellers=
|number of propellers per engine= 
|propeller diameter main=
|propeller diameter alt=

|max speed main= 
|max speed alt=
|max speed more= 
|cruise speed main= 75 mph
|cruise speed alt=122 km/h
|cruise speed more 
|stall speed main= 32 mph
|stall speed alt= 52 km/h
|stall speed more=
|never exceed speed main= 95 mph
|never exceed speed alt= 154 km/h
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 
|ceiling alt= 
|climb rate main= 1100 fpm
|climb rate alt= 5.6 m/s
|loading main=5.92 lb/sq ft
|loading alt=28.88 kg/sq m
|thrust/weight=
|power/mass main=12.96 lb/hp
|power/mass alt=0.13 kW/kg
|more performance=

|armament=<!-- if you want to use the following specific parameters, do not use this line at all-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Aero-Works Aerolite 103]]
*[[Beaujon Mach .07]]
*[[Beaujon Enduro]]
*[[Birdman Chinook|Birdman WT-11 Chinook]]
*[[ISON Airbike]]
*[[Spectrum Beaver|Spectrum RX-28 Beaver]]

|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{Commons category|Fisher FP-202}}
*[http://www.fisherflying.com/index.php?option=com_content&view=article&id=31&Itemid=22 Official FP-202 Koala page]
*[http://www.fisherflying.com/index.php?option=com_content&view=article&id=43&Itemid=22 Official Super Koala page]
*[http://www.airliners.net/photo/Untitled/Fisher-FP-202-Koala/1078766/L/&tbl=&photo_nr=0&sok=&sort=&prev_id=&next_id=1078765 Photo of 202 Koala]
*[http://www.airliners.net/photo/Fisher-Super-Koala/0885947/&sid=ffa5489a194cf3d9cc221dfc8a94616c Photo of Super Koala]

{{Fisher Flying Products}}

[[Category:Canadian ultralight aircraft 1980–1989]]