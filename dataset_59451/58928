{{Infobox journal
| title = Mind, Brain, and Education
| cover = 
| editor = [[Kurt W. Fischer]]
| discipline = [[Psychology]]
| former_names = 
| abbreviation = Mind Brain Educ. 
| publisher = [[Wiley-Blackwell]] on behalf of the [[International Mind, Brain, and Education Society]]
| country = 
| frequency = Quarterly
| history = 2007-present
| openaccess = 
| license = 
| impact =0.980
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1751-228X
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1751-228X/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1751-228X/issues
| link2-name = Online archive
| JSTOR = 
| OCLC = 123539820
| LCCN = 2007235349
| CODEN = 
| ISSN = 1751-2271
| eISSN = 1751-228X
}}
'''''Mind, Brain, and Education''''' is a quarterly [[peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]]. It was established in 2007 as the official journal of the [[International Mind, Brain, and Education Society]].  Its [[editor-in-chief]] is [[Kurt W. Fischer]] ([[Harvard Graduate School of Education]]). The journal covers brain and behavioral issues relevant to the broad field of education.

In 2007, the journal received a [[PROSE Awards|PROSE Award]] in the category "Best New Journal in the Social Sciences & Humanities" from the [[Association of American Publishers]]' Professional & Scholarly Publishing Division.<ref>{{cite web|url=http://www.proseawards.com/index.html |title=Welcome |publisher=PROSE Awards |date= |accessdate=2014-01-15}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.980.<ref name=WoS>{{cite book |year=2013 |chapter=Mind, Brain, and Education |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
<references/>

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1751-228X}}
* [http://www.imbes.org/ International Mind, Brain and Education Society]

[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2007]]
[[Category:Educational psychology journals]]
[[Category:Quarterly journals]]