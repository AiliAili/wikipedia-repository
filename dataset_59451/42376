<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=EZ Harvard
 | image=Blue Yonder EZ Harvard C-IKDM 06.JPG
 | caption=The prototype EZ Harvard
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft|Kit plane]]
 | national origin=[[Canada]]
 | manufacturer=[[Blue Yonder Aviation]]
 | designer=Wayne Winters
 | first flight=2002
 | introduced=2002
 | retired=
 | status=
 | primary user=Private owners
 | number built= 1
 | developed from= [[Blue Yonder EZ King Cobra|EZ King Cobra]]
 | variants with their own articles=
}}
|}

[[File:Blue Yonder EZ Harvard C-IKDM 01.JPG|thumb|right|The EZ Harvard prototype]]

The '''Blue Yonder EZ Harvard''' is a [[Canada|Canadian]] designed and built, single-engined, single-seat aircraft provided as a completed aircraft or in kit form by [[Blue Yonder Aviation]]. The aircraft is a 75% scale replica of the [[T-6 Texan|North American Harvard]] trainer of the [[Second World War]].<ref name="BY1">{{cite web|url = http://www.ezflyer.com/page10BYA.html|title = EZ HarvardFlyer|accessdate = 2009-03-02|last = Winters |first = Wayne|authorlink = |year = n.d.}}</ref>

The aircraft can be constructed in Canada as a [[Ultralight aircraft (Canada)#Basic ultra-light aeroplane|basic ultra-light]], or [[homebuilt aircraft|amateur-built aircraft]], but is not currently available as an [[Ultralight aircraft (Canada)#Advanced ultra-light aeroplane|advanced ultra-light]].<ref name="TCCAR">{{cite web|url=http://wwwapps.tc.gc.ca/Saf-Sec-Sur/2/CCARCS-RIACC/ADet.aspx?id=500808&rfr=RchSimp.aspx |title=Canadian Civil Aircraft Register |accessdate=7 November 2016 |last=[[Transport Canada]] |date=7 November 2016 }}</ref><ref name="TCAULA">{{cite web|url = http://www.tc.gc.ca/civilaviation/general/CCARCS/advancedullist.htm|title = Listing of Models Eligible to be Registered as Advanced Ultra-Light Aeroplanes (AULA) |accessdate = 2009-03-02|last = [[Transport Canada]]|authorlink = |date=November 2008}}</ref>

==Development==
The EZ Harvard was designed by Wayne Winters of [[Indus, Alberta]] and based on the earlier [[Blue Yonder EZ King Cobra|EZ King Cobra]]. The project was started as a customer request for a scale Harvard replica and was later offered as a commercially available kit aircraft.

Winters created the EZ Harvard by using the [[cantilever]] wing design from the EZ King Cobra and added {{convert|4|ft|m}} additional span, to increase the wingspan to {{convert|31|ft|m}} and the wing area to {{convert|176|sqft|m2|abbr=on}}. The fuselage was redesigned to give the round cross section, glazed canopy and distinctive fin shape of the original Harvard. The aircraft retained the Junkers ailerons of the original [[Blue Yonder Merlin|Merlin]] wing along with the Clark "Y" [[airfoil]] and construction featuring a leading edge "D" cell and foam ribs. The fuselage is constructed of welded [[41xx steel|4130 steel tube]]. Even though the Harvard was originally a two-seat aircraft the EZ Harvard is a single seater with the prototype powered by a [[Rotax 582]] two stroke engine of {{convert|64|hp|kW|0|abbr=on}}.<ref name="BY1" /><ref name="IncompleteGuide">{{cite web|url = http://www.ae.uiuc.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage|accessdate = 2009-02-28|last = Lednicer|first = David |authorlink = |date=October 2007}}</ref>

The prototype of the new design flew in 2002. In the basic ultralight version gross weight is limited to the category maximum of {{convert|1200|lb|kg|0|abbr=on}}.<ref name="BY1" />

The EZ Harvard has a large round cowling that can accommodate a variety of powerplants:<ref name="BY2">{{cite web|url = http://www.ezflyer.com/page34BYA.html|title = EZ Harvard Price List|accessdate = 2009-03-02|last = Winters |first = Wayne|authorlink = |year = n.d.}}</ref>

*[[Rotax 503]] {{convert|50|hp|kW|0|abbr=on}}
*[[Rotax 582]] {{convert|64|hp|kW|0|abbr=on}}
*[[Rotax 912]] {{convert|80|hp|kW|0|abbr=on}}

==Operational history==
Despite being widely demonstrated no further orders have been received for the type and the prototype remains the sole flying example.<ref name="TCCAR" />
<!-- ==Variants== 
==Operators== -->

==Specifications (Rotax 582) ==

{{aircraft specifications

|plane or copter?=<!-- options: plane/copter -->plane
|jet or prop?=<!-- options: jet/prop/both/neither -->prop

|ref=Blue Yonder website<ref name="BY1" />

|crew=one
|capacity=<!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 21 ft
|length alt=6.4 m
|span main=31 ft
|span alt=9.5 m
|height main=7 ft
|height alt=2.1 m
|area main= 176 sq ft
|area alt= 16.4 sq m
|airfoil=Clark Y<ref name="IncompleteGuide" />
|empty weight main= 495 lb
|empty weight alt= 224 kg
|loaded weight main= 1200 lb
|loaded weight alt= 544 kg
|useful load main= 705 lb
|useful load alt= 320 kg
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=

|engine (jet)=
|type of jet=
|number of jets=
|thrust main= 
|thrust alt= 
|thrust original=
|afterburning thrust main=
|afterburning thrust alt= 
|thrust more=

|engine (prop)=[[Rotax 582]]
|type of prop= <!-- meaning the type of propeller driving engines -->fixed pitch
|number of props=<!--  ditto number of engines-->1
|power main= 64 hp
|power alt=48 kW
|power original=
|power more=

|propeller or rotor?=<!-- options: propeller/rotor -->propeller
|propellers=1
|number of propellers per engine= 1
|propeller diameter main=
|propeller diameter alt=

|max speed main= 100 mph
|max speed alt=162 km/h
|max speed more= 
|cruise speed main= 90 mph
|cruise speed alt=146 km/h
|cruise speed more 
|stall speed main= 40 mph
|stall speed alt= 65 km/h
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 380 sm
|range alt=615 km
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 12,000 ft
|ceiling alt= 3660 m
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=18.75 lb/hp
|power/mass alt=0.09 kW/kg
|more performance=

|armament=<!-- if you want to use the following specific parameters, do not use this line at all-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=

*[[Blue Yonder EZ King Cobra]]

|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
{{Commons category|Blue Yonder EZ Harvard}}
* [http://www.ezflyer.com Blue Yonder Aviation]

{{Blue Yonder Aviation}}

[[Category:Low-wing aircraft]]
[[Category:Blue Yonder aircraft|EZ Harvard]]
[[Category:Canadian ultralight aircraft 2000–2009]]
[[Category:Homebuilt aircraft]]
[[Category:Replica aircraft]]