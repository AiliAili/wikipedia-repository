{{lead too short|date=March 2016}}
{{Infobox person
| name                      = Andrew Mackenzie Hull
| image                     = <!--just the name, without the File: or Image: prefix or enclosing [[brackets]] -->
| image_size                = 
| alt                       = 
| caption                   = 
| birth_date                = {{Birth date|1963|8|15}} 
| birth_place               = [[Oshawa]], [[Ontario]], Canada
| death_date                = {{Death date and age|2010|5|8|1963|8|15}}
| death_place               = London, England
| death_cause               = 
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} --> 
| monuments                 = 
| residence                 = 
| nationality               = 
| other_names               = 
| ethnicity                 = <!-- Ethnicity should be supported with a reliable citation --> 
| citizenship               = 
| education                 = [[Lakefield College School]], Ontario
| alma_mater                = [[Carleton University]], Ottawa
| occupation                = Architect and film director
| years_active              = 1989 – 2010
| notable_works             = 
| style                     = 
| partner                   = Shaan Syed
| children                  = 
| parents                   = 
| relatives                 = 
| awards                    = 
}}
'''Andrew Mackenzie Hull''' (August 15, 1963 – May 8, 2010) was a Canadian born [[film maker]], [[film director]] and [[architect]]. He was born in [[Oshawa, Ontario|Oshawa, Ontario, Canada]], and died in [[London]], England.<ref name=cfccreates>{{cite web|url=http://www.cfccreates.com/about_us/announcements/item.php?id=item120 |title=The Canadian Film Centre :: About Us |publisher=Cfccreates.com |date=May 14, 2010 |accessdate=March 6, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110708133112/http://www.cfccreates.com/about_us/announcements/item.php?id=item120 |archivedate=July 8, 2011 |df= }}</ref>

==Early life==
Andrew Hull was the youngest of three children born to Ralph Hull, [[psychiatrist]], and Margot Finley, artist. He spent his childhood in [[Ann Arbor, Michigan|Ann Arbor]], [[Michigan]] and [[Peterborough, Ontario]], attended [[Lakefield College School]] as a boy, and studied architecture at [[Carleton University]], [[Ottawa]].  During his studies in 1989 he won the AIA/ACSA Research Council-Otis Elevator International Student Competition for his mixed-use commercial, retail and residential design for a development in historic London.  Despite this accolade his interests shifted to film and video.  Being one of the first students to work with video at the School of Architecture, he graduated with an experimental video work about a cryptic symbol of a traced hand that appears in the urban environment.  This first completed video marked his move from architecture into [[narrative film]].

==Film career==
While working as an architect in [[Paris]] in 1991, Andrew Hull was invited to [[Germany]] by [[Carleton University]] colleagues Stephen Kovats and Ian Johnston to work on video and animation projects and to mentor students of the [[Bauhaus Dessau]]'s international and [[multi-disciplinary]] 'Experimental Studio Dessau North'.  In 1992 he was co-commissioned by the 'Werkstatt Industrielles Gartenreich' of the [[Bauhaus Dessau Foundation]] to make a documentary video about the 'Kulturpalast Bitterfeld', a Socialist Utopian model project of the [[German Democratic Republic]], built in 1954.  His collaborator in this video project was Stephen Kovats, media researcher, architect and artistic director of [[Transmediale]].<ref>{{cite web|url=http://www.v2.nl/archive/people/stephen-kovats |title=Stephen Kovats — V2_ Institute for the Unstable Media |publisher=V2.nl |date= |accessdate=March 6, 2011}}</ref> The film was entitled “Pay No Attention to the Man Behind the Curtain”. As a result of this collaboration, Hull moved to [[Germany]], living first in [[Dessau]], later in [[Berlin]]. He made films during this period and taught at the Bauhaus Dessau in the newly created Electronic Media Interpretation Studio. Hull's films from this time explored the genres of [[Horror film|horror]] and [[comedy]], the real and the [[fictional]]; they also documented and questioned the strained political and social transformations taking place in Germany after the fall of the [[Berlin Wall]]. In Dessau he made ''Earworm'', a forty-three-minute film funded by both German and [[Canada Council|Canadian Arts Councils]] that told the story of a group of [[anarchist]] [[karaoke]] enthusiasts gripped by a mysterious virus that causes an addiction to [[techno music]] and a taste for sucking the inner ear out of unsuspecting victims.  The film could be read as a [[metaphor]] and biting [[satire]] on the [[paranoia]] of [[communist]] era [[East Germany]], intertwined with the politics around [[HIV/AIDS]], and the growing [[gay]] underground club and [[rave]] scene in Germany in the late eighties and early nineties.  Hull also presented the [[video installation]] ''[[Berlin Alexanderplatz]]'' at the [[Ostranenie]] International Video Festival at the Bauhaus Dessau.<ref>{{cite web|url=http://www.cfmdc.org/catalogue/filmmaker/1620 |title=Hull, Andrew |publisher=cfmdc.org |date= |accessdate=March 6, 2011}}</ref>

In 1996 Andrew Hull returned to [[Canada]] and lived in [[Toronto]] until 2008. While working as an [[Art Director]] in the Canadian/US Film and Television industry,<ref>{{cite web|url=http://www.imdb.com/name/nm0401402/|title=Andrew Hull|work=IMDb|accessdate=13 April 2015}}</ref> he wrote and directed several of his own short films. ''Dizzy'' (Winner of Best Short at the Long Island Gay and Lesbian Film Festival in 2003) and ''That Thing We Do'' (Winner of Best Short at the [[Tampa International Gay and Lesbian Film Festival]] in 2004)<ref>{{cite web|url=http://www.andrewhull.ca/directing_cv.html |title=Directing |publisher=Andrewhull.ca |date= |accessdate=March 6, 2011}}</ref> were made with assistance from the [[Canada Council for the Arts]] and the [[Ontario Arts Council]] and were coming-of-age stories directly based on his own experiences and diaristic writings. In 2003, Hull was one of eight students invited to enter the  Director's Program at the [[Norman Jewison]] created [[Canadian Film Centre]] (CFC), Canada's foremost school for advanced training in film, television and [[new media]].  He graduated in 2004 with the short film ''Squeezebox'', a [[tragicomedy]] starring Canadian cult rock heroine, actress and singer-songwriter [[Mary Margaret O'Hara]]. The film is the story of a teen accordion prodigy who struggles to reunite the family band after his father's suicide. ''Rewind'', a five-minute film made shortly after Hull's graduation from the [[Canadian Film Centre]], paid homage to [[film noir]] and was loosely based on [[Martin Amis]]' book ''[[Time's Arrow (novel)|Time's Arrow]]''. All of Hull's films from this period went on to tour at film festivals around the globe.

In 2008 Andrew re-located to [[London]], England, to be with his [[life partner]], [[contemporary artist]] and painter, Shaan Syed, and to begin production on his first [[feature film]], ''Siren'', co-written with Canadian-born, US-based [[screenwriter]] Geoffrey Gunn.<ref>{{cite web|url=http://www.imdb.com/name/nm1622439/maindetails|title=Geoffrey Gunn|work=IMDb|accessdate=April 13, 2015}}</ref> ''Siren'', a [[horror film|horror]] / [[Thriller (genre)|thriller]] [[genre film]] is an [[allegory]] on the [[Greek myth]] of the [[Siren (mythology)|siren]], and tells the tale of three friends whose lives are forever changed when they stop to rescue a beautiful young woman on a remote island.  The film was produced by UK-based Poisson Rouge Pictures and was shot in [[Tunisia]] during the summer of 2009. The film's soundtrack features and is based on the Los Angeles all-girl group [[Warpaint (band)|Warpaint]]'s song, "Elephants". It premiered on November 11, 2010 at [[Abertoir]] Film Festival in Wales, UK<ref name="Johnny Davies">{{cite web|author=Johnny Davies |url=http://www.abertoir.co.uk/index.php?option=com_content&view=article&id=80%3Afilm-list&catid=40%3Areviews&Itemid=56&lang=en |title=Films Showing |publisher=Abertoir.co.uk |date=October 20, 2010 |accessdate=March 6, 2011}}</ref> to critical acclaim <ref name="Johnny Davies"/><ref name="imdb.com">{{cite web|url=http://www.imdb.com/title/tt1548635/news?year=2010|title=Siren (2010/III) - News|work=IMDb|accessdate=April 13, 2015}}</ref><ref>{{cite web|url=http://www.maxim.co.uk/entertainment/movies/20518/siren.html |title=Siren - one to watch! &#124; Entertainment |publisher=Maxim |date= |accessdate=March 6, 2011}}</ref> and was subsequently bought by [[Lionsgate]] to be released to DVD in March 2011.<ref name="imdb.com"/>

===Death===
Andrew Hull died at the [[Royal London Hospital]] on May 8, 2010 of a head injury as a result of a fall from his bicycle. Hull was at the time close to finishing a final edit on his newest short film ''Breaking and Entering'', shot in Toronto in 2008. The film is a story of a young man coming to terms with the death of his father, and is based on the short story of the same name by Canadian author [[Andrew Pyper]].  The film was subsequently completed by the Estate of Andrew Hull.

===Representation===
Andrew Hull was a member of the [[Director's Guild of Canada]].<ref>{{cite web|url=http://www.dgc.ca/news.php?id=400&archives=true&main=true&news=1178 |title=Directors Guild of Canada - DGC |publisher=Dgc.ca |date= |accessdate=March 6, 2011}}</ref> His short films are distributed by the [[Canadian Film Maker's Distribution Centre (CFMDC)]] <ref>{{cite web|url=http://www.cfmdc.org |title=cfmdc.org |publisher=cfmdc.org |date= |accessdate=March 6, 2011| archiveurl= https://web.archive.org/web/20110208163112/http://www.cfmdc.org/| archivedate=8 February 2011 <!--DASHBot-->| deadurl= no}}</ref> and [[V tape]],<ref>{{cite web|url=http://www.vtape.org |title=V tape |publisher=V tape |date= |accessdate=March 6, 2011}}</ref> both in [[Toronto]]. ''Siren'' is distributed by [[Lionsgate]].<ref name="imdb.com"/>

==Posthumous Artist tributes and works==

London-based [[choreographer]] and artistic director of Dance Art Foundation, Joe Moran dedicated his piece ''Score for 30 Dancers'' to Andrew Hull. It was presented on May 27, 2010 as part of ''From Morning'', a day of dance and performance curated by London-based dancer and choreographer, Florence Peak. The piece was performed by thirty of the UK's most prominent dance artists and was made in response to an invitation to create a new site-specific work that spoke to the distinct architecture of [[Nicholas Hawksmoor]]'s [[Christ Church, Spitalfields]] in the [[East End of London]],<ref>{{cite web|url=http://www.danceartfoundation.com/scorefor30.html |title=Artistic Director Joe Moran |publisher=Dance Art Foundation |date= |accessdate=March 6, 2011}}</ref> the neighbourhood where Hull lived and died from 2008 to 2010.

London-based Dutch contemporary artist, Magali Reus, in the final credit of her 2010 video work ''Finish'', dedicated the piece to Andrew MacKenzie Hull. The short work was premiered at [http://www.fonswelters.nl Galerie Fons Welters] in [[Amsterdam]] in July 2010 as part of a solo exhibition of sculpture and video titled ''Weekend'' and featured five athletes/actors racing against the backdrop of an ocean beach. Reus had previously featured Hull in her video ''Conflicting Shadows'' (2009) as the main actor playing an ambiguous street patroller at night.<ref>{{cite web|url=http://www.fonswelters.nl/artist/magali_reus/#images/finish |title=Magali Reus - Artist - Galerie Fons Welters |publisher=Fonswelters.nl |date= |accessdate=March 6, 2011}}</ref>

In November 2010, Andrew Hull's life partner, artist Shaan Syed, began ''The Andrew Project'',<ref>{{cite web|last=Topping |first=David |url=http://torontoist.com/2010/12/andrew_mackenzie_hull_posters.php |title=Remembering Andrew |publisher=Torontoist |date=December 23, 2010 |accessdate=March 6, 2011}}</ref> a street-poster campaign featuring one thousand identical line drawings of Andrew's portrait posted throughout Toronto,<ref>{{cite web|url=http://www.cbc.ca/canada/toronto/story/2010/12/20/artist-andrew-project.html?ref=rss#socialcomments |title='Andrew' drawings pop up around Toronto - Toronto - CBC News |publisher=Cbc.ca |date=2010-12-20 |accessdate=March 6, 2011}}</ref> the city in which the couple first met in 2001.

==Filmography==
{| class="wikitable"
|-
! Year !! Title !! Role !! Notes
|-
|1989 || ''The City Rediscovered'' || Writer/Director|| 21min, [[betacam]]
|-
|1993 || ''Pay No Attention to the Man Behind the Curtain'' || Writer/Director || 56min [[Hi8]].<br/>Collaboration with Stephen Kovats
|-
|1996 || ''[[Earworm]]'' || Writer/Director ||43min, [[video]] [[Hi8]]/[[Super 8 mm film|Super 8]]
|-
|1996 || ''Calypso ! || Writer/Director ||4:26min, [[video]]
|-
|1999 || ''Dizzy'' || Writer/Director ||19min, [[16mm]]
|-
|2003 || ''That thing we do'' || Writer/Director ||16min, [[35mm]]
|-
|2003 || ''Wilden Jungen'' || Co-Writer/Co-Director ||5min, [[Super 8 mm film|Super 8]], [[video]].<br/>Collaboration with Shaan Syed.  An invitational by IMAGES, Toronto's GLTB Film Festival.
|-
|2005 || ''[[Squeezebox]]'' || Writer/Director || 14min, [[35mm]]
|-
|2006 || ''Rewind'' || Writer/Director ||5min, [[high-definition video|HD]]
|-
|2011 || ''[[Siren (Film)|Siren]]'' || Director/Co-Writer || [[feature film]], redcam.  Co-written with Geoffrey Gunn, distributed by [[Lionsgate]].
|-
|2011 || ''Breaking and Entering'' || Writer/Director ||7min, redcam.  Completed posthumously by the Estate of Andrew Hull.
|}

==References==
{{reflist|30em}}

{{DEFAULTSORT:Hull, Andrew}}
[[Category:1963 births]]
[[Category:2010 deaths]]
[[Category:Canadian film directors]]
[[Category:People from Oshawa]]
[[Category:LGBT artists from Canada]]
[[Category:Canadian screenwriters]]
[[Category:LGBT writers from Canada]]
[[Category:LGBT directors]]
[[Category:LGBT screenwriters]]
[[Category:Gay writers]]
[[Category:Cycling road incident deaths]]
[[Category:Road incident deaths in London]]