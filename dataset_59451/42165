{{redirect|A319|the British road|A319 road}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= A319
 |image= File:Lufthansa.a319-100.d-aili.arp.jpg
 |caption=A [[Lufthansa]] Airbus A319
}}{{Infobox aircraft type
 |type= [[Narrow-body]] [[jet airliner]]
 |national origin= Multi-national{{efn|The Airbus A319 is built in Hamburg, Germany and Tianjin, China}}
 |manufacturer= [[Airbus]]
 |designer=
 |first flight= 25 August 1995
 |introduced= 1996 with [[Swissair]]
 |produced= 1994–present
 |retired=
 |status= In service
 |primary user= [[EasyJet]]
 |more users= [[American Airlines]] <br />[[United Airlines]] <br />[[Delta Air Lines]]<!--Limit of three (3) more users here. -->
 |number built= 1,457 {{as of|2017|03|31|df=y|lc=y}}<ref name="Airbus O&D">{{cite web|url=http://www.airbus.com/company/market/orders-deliveries|publisher=Airbus|title=Airbus Orders & Deliveries|date=31 March 2017|accessdate=9 April 2017}}</ref>
|unit cost= US$89.6&nbsp;million<ref name="2016 price">{{cite press release|title=New Airbus aircraft list prices for 2015|date=13 January 2015|url=http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/new-airbus-aircraft-list-prices-for-2016/|website=Airbus|accessdate=25 July 2016}}</ref>
 |developed from= [[Airbus A320 family|Airbus A320]]
 |variants with their own articles= [[Airbus A318]]
 |developed into= [[Airbus A320neo family]]
}}
|}

The '''Airbus A319''' is a member of the [[Airbus A320 family]] of short- to medium-range, [[Narrow-body aircraft|narrow-body]], commercial passenger [[Twinjet|twin-engine]] [[jet airliner]]s manufactured by [[Airbus]].{{efn|Airbus was originally a [[consortium]] of European aerospace companies named, Airbus Industrie, and is now fully owned by [[Airbus Group]], originally named EADS. Airbus' name has been ''Airbus SAS'' since 2001.}} The A319 carries up to 160 passengers and has a maximum range of {{convert|3700|nmi|abbr=on}}.<ref name="A319 specifications"/> Final assembly of the aircraft takes place in [[Hamburg]], [[Germany]] and [[Tianjin]], [[China]].

The A319 is a shortened-fuselage variant of the Airbus A320 and entered service in April 1996 with [[Swissair]], around two years after the stretched Airbus A321 and eight years after the original A320. The aircraft shares a common [[type rating]] with all other Airbus A320 family variants, allowing existing A320 family pilots to fly the aircraft without the need for further training.

As of 31 March 2017, a total of 1,457 Airbus A319 aircraft have been delivered, of which 1,438 are in service. In addition, another 24 airliners are on firm order. As of 31 March 2017, [[EasyJet]] was the largest operator of the Airbus A319, operating 144 aircraft.<ref name="Airbus O&D"/>

In December 2010, Airbus announced a new generation of the A320 family, the [[Airbus A320neo family|A320neo]] (new engine option).<ref>{{cite web|url=http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/airbus-offers-new-fuel-saving-engine-options-for-a320-family/ |title=Airbus offers new fuel saving engine options for A320 Family |publisher=Airbus |date=1 December 2010 |accessdate=31 December 2011}}</ref>  The similarly shortened fuselage A319neo variant offers new, more efficient engines, combined with airframe improvements and the addition of [[winglet]]s, named ''Sharklets'' by Airbus.  The aircraft will deliver fuel savings of up to 15%. The A319neo is the least popular variant of the Airbus A320neo family, with total orders for only 55 aircraft placed as of 31 March 2017, compared with 3,616 for the A320neo and 1,385 for the A321neo.<ref name="Airbus O&D"/>

== Development ==<!-- Linked from [[Airbus A320 family]] -->

===Background===
The first member of the A320 family was the A320 which was launched in March 1984 and [[Maiden flight|first flew]] on 22 February 1987.<ref>{{harvnb|Norris|Wagner|1999|p=50}}</ref> The family was extended to include the stretched A321 (first delivered 1994), the shortened A319 (1996), and the further shortened A318 (2003). The A320 family pioneered the use of digital [[fly-by-wire]] [[Aircraft flight control system|flight control systems]], as well as [[side stick]] controls, in commercial aircraft. The A319 was developed at the request of [[Steven F. Udvar-Házy|Steven Udvar-Hazy]], the former president and CEO of [[ILFC]] according to ''[[The New York Times]]''.<ref>{{Cite news |url=https://www.nytimes.com/2007/05/10/business/10flyboy.html?pagewanted=2&_r=1 |title=The Real Owner of All Those Planes |accessdate=4 March 2011 |author =Wayne, Leslie |date=10 May 2007 |work=The New York Times |page=2}}</ref>

[[File:US Airways A319-132 LAS N838AW.jpg|thumb|A [[US Airways]] A319 in [[America West]] heritage livery. ]]

===Origins and design===
The A319 design is a shortened fuselage, minimum change derivative of the A320 with its origins in the 130- to 140-seat SA1, part of the Single-Aisle studies.<ref name="N&W p.53">{{harvnb|Norris|Wagner|1999|p=53}}</ref> The SA1 was shelved as the consortium concentrated on its bigger siblings. After healthy sales of the A320/A321, Airbus re-focused on what was then known as the ''A320M-7'', meaning A320 ''minus seven fuselage frames''.<ref>{{harvnb|Eden|2008|p=26}}</ref> It would provide direct competition for the [[Boeing 737-300|737–300]]/[[Boeing 737 Next Generation|-700]].<ref name="N&W p.53"/> The shrink was achieved through the removal of four fuselage frames fore and three aft the wing, cutting the overall length by {{Convert|3.73|m|ftin}}.<ref name="A319 specifications"/><ref name="A320 specifications">{{cite web |url=http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/a320/specifications/ |title=Specifications Airbus A320 |accessdate=13 February 2012 |work=Airbus}}</ref><ref name="Meeting demand">{{cite web |author =Moxon, Henley |url=http://www.flightglobal.com/pdfarchive/view/1995/1995%20-%202575.html |title=Meeting demands |accessdate=25 February 2011 |date=30 August 1995 |work=Flight International }}</ref>  Consequently, the number of overwing exits was reduced from four to two. The bulk-cargo door was replaced by an aft container door, which can take in reduced height [[Unit Load Device|LD3-45 containers]].<ref name="Meeting demand"/> Minor [[Computer programming|software]] changes were made to accommodate the different handling characteristics; otherwise the aircraft is largely unchanged. Power is provided by the CFM56-5A or V2500-A5, derated to {{Convert|98|kN|lbf|abbr=on|lk=in}}, with option for {{Convert|105|kN|lbf|abbr=on}} thrust.<ref>{{cite web |url=http://www.flightglobal.com/pdfarchive/view/1997/1997%20-%202909.html |title=A319 flight test |accessdate=26 February 2011 |author =Henley, Peter |work=Flight International }}</ref>

With virtually the same fuel capacity as the A320-200, and fewer passengers, the range with 124 passengers in a two-class configuration extends to {{Convert|6,650|km|nmi|abbr=on}}, or {{Convert|6,850|km|nmi|abbr=on}} with the "Sharklets".<ref name="A319 specifications"/> The A319's wingspan is longer than the aircraft's overall length.

===Production and testing===
Airbus began offering the new model from 22 May 1992, and the A319's first customer was ILFC, who signed for six aircraft.<ref name="N&W p.54" /> Anticipating further orders by Swissair and Alitalia, Airbus launched the $275&nbsp;million (€250 million) programme on 10 June 1993.<ref name="Meeting demand"/><ref name="N&W p.54">{{harvnb|Norris|Wagner|1999|p=54}}</ref><ref name="Gunston 2009 216">{{harvnb|Gunston|2009|p=216}}</ref> On 23 March 1995, the first A319 underwent final assembly at Airbus' German plant in Hamburg, where the A321s are also assembled. It was rolled out on 24 August 1995, with the maiden flight the following day.<ref>{{harvnb|Eden|2008|p=27}}</ref> The certification programme would take 350 airborne hours involving two aircraft; certification for the CFM56-5B6/2-equipped variant was granted in April 1996, and the qualification for the V2524-A5 started the following month.<ref name="N&W p.55"/>

[[File:G-EZBW Airbus A319-111 A319 EZY (neue Bemalung) (16000999414).jpg|thumb|An EasyJet A319]]

Delivery of the first A319, to [[Swissair]], took place on 25 April 1996, entering service by month's end.<ref name="N&W p.55">{{harvnb|Norris|Wagner|1999|p=55}}</ref> In January 1997, an A319 broke a record during a delivery flight by flying {{Convert|3,588|nmi|km|sigfig=4}} on the [[great circle]] route to [[Winnipeg]], Manitoba from Hamburg, in 9 hours 5 minutes.<ref name="N&W p.55"/> The A319 has proved popular with low-cost airlines such as [[EasyJet]], with 172 delivered.<ref name="Airbus O&D"/>

A total of 1,457 of the A319ceo model have been delivered, with 24 remaining on order as of 31 March 2017.<ref name="Airbus O&D"/> The direct [[Boeing]] competitor is the [[Boeing 737-700]].

=== Variants ===

==== A319CJ ====
The A319CJ (rebranded ACJ319) is the corporate jet version of the A319. It incorporates removable extra fuel tanks (up to 6 Additional Center Tanks) which are installed in the cargo compartment, and an increased service ceiling of {{convert|12500|m|abbr=on}}.<ref name="a319cjceiling">{{cite web|url=http://stagev4.airbus.com/en/aircraftfamilies/executive_aviation/acj_family/ |title=Aircraft Families – Airbus Executive and Private Aviation – ACJ Family |publisher=Stagev4.airbus.com |date= |accessdate=9 July 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20081203105457/http://stagev4.airbus.com/en/aircraftfamilies/executive_aviation/acj_family |archivedate=3 December 2008 }}</ref> Range with eight passengers' payload and auxiliary fuel tanks (ACTs) is up to 6,000 nautical miles (11,100&nbsp;km).<ref>[http://www.airbus.com/fileadmin/media_gallery/files/tech_data/jetFamily/media_object_file_ACJ_Specifications.pdf ACJ Specifications], airbus.com</ref><ref>"ACJ Analysis" Business & Commercial Aviation Magazine&nbsp;– July 2002, Page 44</ref> Upon resale, the aircraft can be reconfigured as a standard A319 by removing its extra tanks and corporate cabin outfit, thus maximising its resale value. It was formerly also known as the ACJ, or [[Airbus Corporate Jets|Airbus Corporate Jet]], while starting with 2014 it has the marketing designation ACJ319.

[[File:MJF-AC1 E01.JPG|thumb|ACJ319 operated by the Austrian operator MJET]]
The aircraft seats up to 39 passengers, but may be outfitted by the customers into any configuration. Tyrolean Jet Service Mfg. GmbH & CO KG, [[MJET]] and [[Reliance Industries]] are among its users. The A319CJ competes with other ultralarge-cabin corporate jets such as the Boeing [[Boeing 737#737-700|737–700]]-based [[Boeing Business Jet]] (BBJ) and [[Embraer]] [[Lineage 1000]], as well as with large-cabin and ultralong-range [[Gulfstream G650]], [[Gulfstream G550]] and [[Bombardier Aerospace|Bombardier]]'s [[Global 6000]]. It is powered by the same engine types as the A320. The A319CJ was used by the ''[[Escadron de transport, d'entrainement et de calibration|Escadron de Transport, d'Entraînement et de Calibration]]'' which is in charge of transportation for France's officials and also by the Flugbereitschaft of the [[German Air Force]] for transportation of Germany's officials. An ACJ serves as a presidential or official aircraft of [[Armenia]],<ref>{{cite web|url=http://www.airliners.net/photo/Armenia-Government/Airbus-A319-132-CJ/1702856/&sid=10ab84d6e153604fd2aae14b30e94974 |title=– Government of Armenia A319CJ |publisher=Airliners.net |date=11 April 2010 |accessdate=9 July 2012}}</ref> [[Azerbaijan]], [[Brazil]], [[Bulgaria]], [[Czech Republic]], [[Germany]], [[Italy]],<ref>{{cite web|url=http://www.aeronautica.difesa.it/Mezzi/velivoliDotazione/Pagine/A-319CJ.aspx |title=Il portale dell'Aeronautica Militare – Airbus A319CJ |publisher= |accessdate=26 December 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20150110235024/http://www.aeronautica.difesa.it:80/Mezzi/velivoliDotazione/Pagine/A-319CJ.aspx |archivedate=10 January 2015 |df=dmy }}</ref> [[Malaysia]], [[Thailand]], [[Turkey]], [[Ukraine]], and [[Venezuela]].

Starting from 2014, a modularized cabin version of the ACJ319, known as "Elegance", is also available. It is said to be able to lower cost and ease reconfiguration.<ref>{{cite web|url=https://www.flightglobal.com/news/articles/abace-airbus-unveils-modular-option-for-acj319-398184/|title=ABACE: Airbus unveils modular option for ACJ319|date=14 April 2014|publisher=}}</ref>

==== A319neo ====
{{main article|Airbus A320neo family}}
The A319neo will be part of the Airbus A320neo family of [[airliner]]s developed since December 2010 by [[Airbus]],<ref name="A320ceo + A320neo family information">{{cite web|author=|url=http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/ |title=Airbus A320 (A320ceo and A320neo) Aircraft family |publisher=Airbus.com |date=3 March 2013 |accessdate=21 April 2013| archiveurl= https://web.archive.org/web/20130303214628/http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/| archivedate=3 March 2013| deadurl= no}}</ref> with the suffix "neo" meaning "new engine option". It is the last step of the [[A320 Enhanced]] (A320E) modernisation programme, which was started in 2006. The A319neo replaces the original A319, which is now referred to as A319ceo, for "current engine option".

In addition to the new engines, the modernisation programme also included such improvements as: aerodynamic refinements, large curved [[winglet]]s (sharklets), weight savings, a new [[aircraft cabin]] with larger [[hand luggage]] spaces, and an improved air purification system.<ref name="enh1">{{cite web|url=http://www.flightglobal.com/news/articles/pictures-airbus-aims-to-thwart-boeings-narrowbody-plans-with-upgraded-a320-207273/ |title=Pictures: Airbus aims to thwart Boeing’s narrowbody plans with upgraded 'A320 Enhanced' |work=Flight International |date=20 June 2006 |accessdate=8 July 2013}}</ref><ref name="enh2">{{cite web|author= |url=http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/avianca-takes-delivery-of-sharklet-equipped-a320 |title=Avianca takes delivery of Sharklet equipped A320 |publisher=Airbus.com |date= |accessdate=27 March 2013}}</ref> Customers will have a choice of either the [[CFM International LEAP|CFM International LEAP-1A]] or the [[Pratt & Whitney PW1000G|Pratt & Whitney PW1100G]] engines.

These improvements in combination are predicted to result in 15% lower fuel consumption per aircraft, 8% lower operating costs, reduced noise production, and a reduction of [[NOx|nitrogen oxide (NO<sub>x</sub>)]] emissions by at least 10% compared to the A320 series, as well as an increase in range of approximately {{convert|500|nmi|km|sigfig=1}}.<ref name="A320neo information">{{cite web|url=http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/ |title= A320neo family information, Maximum benefit and minimum change|publisher=Airbus.com |date=1 July 2011 |accessdate=30 December 2011}}</ref>

The A319neo is the least popular variant of the Airbus A320neo family, with total orders for only 55 aircraft placed as of 31 March 2017, compared with 3,616 for the A320neo and 1,385 for the A321neo.<ref name="Airbus O&D"/>
====A319 MPA====
The '''[[Airbus A319 MPA]]''' (Maritime Patrol Aircraft) is derived from a military version of the Airbus A319. Currently, it is in development by [[Airbus Defense and Space]] to compete against the [[Boeing P-8 Poseidon]], which is a derivative aircraft of the Boeing 737 manufactured in the United States. Airbus Defense & Space had proposed the aircraft to the [[Indian Navy]] and to the [[Royal Australian Navy]], but lost both contracts to Boeing's P-8. Airbus is now proposing the aircraft to the [[Royal Canadian Air Force]] as a [[CP-140 Aurora]] replacement, and as a [[Breguet Atlantique]] replacement to the [[French Navy|French]] and [[German Navy|German]] navies. <ref>{{cite web|url=http://c295.ca/aircraft-family/large-aircraft/|title=Large aircraft|work=c295.ca|accessdate=11 March 2016}}</ref><ref>{{cite web|url=http://www.naval-technology.com/projects/a319mpa/|title=A319 MPA Maritime Patrol Aircraft|publisher=}}</ref>

====A319LR====
[[File:Airbus A319-133LR Qatar Airways A7-CJA.jpg|thumb|A [[Qatar Airways]] A319LR]]
The A319LR is the longer range version of the A319. The typical range of the A319LR is increased up to 4,500 nautical miles (8,300 km) compared to the standard A319. [[Qatar Airways]] was the launch customer, getting two A319-100LRs,<ref>{{Cite web |url=https://www.flightglobal.com/news/articles/privatair-and-qatar-opt-for-long-range-a319s-162955/ |title=PrivatAir and Qatar opt for long-range A319s |date=18 March 2003 |editor-last=Wastnage |editor-first=Justin |publisher=Flight International}}</ref> [[PrivatAir]] got two A319LRs in 2003,<ref>{{cite web|url=http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/privatair-receives-its-first-airbus-a319/|title=PrivatAir receives its first Airbus A319|publisher=}}</ref> and [[Eurofly]] got two in 2005.<ref>{{cite web|url=http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/airbus-press-centre-press-release-24/|title=Eurofly Orders Airbus A319 Long Range Aircraft|publisher=}}</ref>

==Design==
The Airbus A319 is a narrow-body (single-aisle) aircraft with a retractable tricycle [[landing gear]] and is powered by two wing pylon-mounted turbofan engines. It is a low-wing [[cantilever]] [[monoplane]] with a conventional [[Empennage|tail unit]] having a single [[vertical stabilizer]] and [[rudder]]. Two suppliers provide turbofan engines for the A319: CFM with its CFM56 and International Aero Engines with the V2500 engine.

==Operators==
{{main article|List of Airbus A320 family operators}}
As of 31 March 2017, 1,438 Airbus A319 aircraft were in service with 105 operators,<ref name="Airbus O&D"/> with [[EasyJet]] and [[American Airlines]] operating the largest A319 fleets of 144 and 125 aircraft respectively.<ref name="Airbus O&D"/> The A319 is the most popular variant of the Airbus A320 family to operated by Governments and as executive and private jets, with 73 aircraft in operation in these capacities as of March 2017.<ref name="Airbus O&D"/>

===Orders and deliveries===
{{See also|List of Airbus A320 orders}}

{| class="wikitable" border="1" style="text-align:right; font-size:96%"
|-
! !!colspan="2"|Orders!!colspan="23"|Deliveries
|-
!Type!!Total!!Backlog!!Total!!2017!!2016!!2015!!2014!!2013!!2012!!2011!!2010!!2009!!2008!!2007!!2006!!2005!!2004!!2003!!2002!!2001!!2000!!1999!!1998!!1997!!1996
|-
!A319
|1,481||24||1,457||&mdash;||4||24||34||38||38||47||51||88||98||105||137||142||87||72||85||89||112||88||53||47||18
|-
|}
<small>''Data through end of March 2017. Updated 9 April 2017.''</small><ref name="Airbus O&D"/>

== Accidents and incidents ==
{{Main article|Accidents and incidents involving the Airbus A320 family}}

For the Airbus A319, 11 [[aviation accidents and incidents]] have occurred,<ref>[http://aviation-safety.net/database/types/Airbus-A319/database Airbus A319 occurrences]. Aviation Safety, 23 July 2016.</ref> including two [[hull loss|hull-loss]] accidents,<ref>[http://aviation-safety.net/database/types/Airbus-A319/losses Airbus A319 hull-loss occurrences]. Aviation Safety, 23 July 2016.</ref> though there have been no fatal accidents recorded involving the aircraft type as of June 2016.<ref>[http://aviation-safety.net/database/types/Airbus-A319/statistics Airbus A319 statistics]. Aviation Safety, 23 July 2016.</ref>

==Specifications==
{| class="wikitable" style="text-align:center; font-size:96%;"
|+ '''Airbus A319'''<ref name="A319 specifications">{{cite web |url= http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/a319/specifications/ |title= A319 Dimensions & key data |publisher= Airbus}}</ref>
! Cockpit crew
| Two
|-
! Exit limit
| 160<ref name=cert>{{cite web |url= https://www.easa.europa.eu/system/files/dfu/TCDS_EASA%20A%20064_%20Airbus_%20A318_A319_A320_A321_Iss_22.pdf |title= Type Certificate Data Sheet |publisher= [[EASA]] |date= 28 June 2016}}</ref> / 150<ref name="FAA-Data">{{cite web |url= http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/57529f07d8f6e3b58625800d0057c1d4/$FILE/A28NM_Rev24.pdf |title= Type Certificate Data Sheet |date= August 12, 2016 |publisher= FAA}}</ref>
|-
! 1-class max. seating
| 156 at {{convert|28|-|30|in|cm|abbr=on}} pitch<ref name=AllAbout/>
|-
! 1-class, typical
| 134 at {{convert|32|in|cm|abbr=on}} pitch<ref name=AllAbout>{{cite web |url= https://fr.scribd.com/doc/30183955/All-About-Airbus-A-320-Family |title= All About the Airbus A320 Family |date= 2009 |publisher= Airbus}}</ref>
|-
! 2-class, typical
| 124 (8F @ 38&nbsp;in, 116Y @ 32&nbsp;in)<ref name=AllAbout/>
|-
! Cargo capacity
| {{convert|27.7|m3|cuft|abbr=on}}
|-
! [[Unit load device]]s
| 4× LD3-45
|-
! Length
| {{convert|33.84|m|ftin|abbr=on}}
|-
! Wheelbase
| {{convert|11.04|m|ftin|abbr=on}}
|-
! Track
| {{convert|7.59|m|ftin|abbr=on}}
|-
! Wingspan
| {{convert|35.8|m|ftin|abbr=on}} {{efn|name=sharklets|with [[Sharklet (wingtip device)|sharklets]]}}
|-
! Wing area
| {{convert|122.4|m2|ft2|abbr=on}}<ref name=elsevier/>
|-
! Wing sweepback
| 25 degrees<ref name=elsevier>{{cite web |url= http://booksite.elsevier.com/9780340741528/appendices/data-a/table-1/table.htm |title= Airbus Aircraft Data File |publisher= Elsevier |work= Civil Jet Aircraft Design |date= July 1999}}</ref>
|-
! Tail height
| {{convert|11.76|m|ftin|abbr=on}}
|-
! Cabin width
| {{convert|3.70|m|ftin|abbr=on}}
|-
! Fuselage width
| {{convert|3.95|m|ftin|abbr=on}}
|-
! Fuselage height
| {{convert|4.14|m|ftin|abbr=on}}
|-
! [[Operating empty weight]] (OEW)
| {{convert|40.8|t|lb|abbr=on}}
|-
! [[Maximum zero-fuel weight]] (MZFW)
| {{convert|58.5|t|lb|abbr=on}}
|-
! [[Maximum landing weight]] (MLW)
| {{convert|62.5|t|lb|abbr=on}}
|-
! [[Maximum takeoff weight]] (MTOW)
| {{convert|75.5|t|lb|abbr=on}}
|-
! Cruising speed
| {{convert|0.78|Mach|39000|0}}<ref>{{cite web |url= http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/technology-and-innovation/ |title= A320 Family Technology |publisher= Airbus}}</ref>
|-
! Maximum speed
| {{convert|0.82|Mach|39000|0}}
|-
! Range, typical payload{{efn|Passengers and bags}}
| 3,750 nmi, 6,950&nbsp;km{{efn|name=sharklets}}
|-
! [[Airbus Corporate Jets|ACJ]] range
| 6,000 nmi, 11,100&nbsp;km<ref name=ACJ319>{{cite web |url= http://www.airbus.com/aircraftfamilies/corporate/acj-family/acj319/ |title= ACJ319 |publisher= Airbus}}</ref>
|-
! Takeoff (MTOW, SL, [[International standard atmosphere|ISA]])
| {{convert|1850|m|ft|abbr=on}}<ref name=ACJ319/>
|-
! Landing (MLW, SL, [[International standard atmosphere|ISA]])
| {{convert|1360|m|ft|abbr=on}}<ref name=ACJ319/>
|-
! [[Jet fuel|Fuel]] capacity
| {{convert|24210|-|30190|L|USgal|abbr=on}}
|-
! Service ceiling
| {{convert|39,100|-|41,000|ft|m|abbr=on}}<ref name=cert/>
|-
! Engines (×2)
| [[CFM56]]-5B, {{convert|68.3|in|m|abbr=on}} fan<br/>[[IAE V2500]]A5, {{convert|63.5|in|m|abbr=on}} fan
|-
! Thrust (×2)
| {{convert|98|-|120|kN|abbr=on}}
|}

=== Engines ===
{| class="wikitable sortable" style="text-align: center;"
|-
! Aircraft Model !! Certification Date !! Engines<ref name=cert/>
|-
| A319-111 || 10 April 1996 || [[CFM International CFM56|CFM56-5B5 or 5B5/P]]
|-
| A319-112 || 10 April 1996 || [[CFM International CFM56|CFM56-5B6 or 5B6/P or 5B6/2P]]
|-
| A319-113 || 31 May 1996 || [[CFM International CFM56|CFM56-5A4 or 5A4/F]]
|-
| A319-114 || 31 May 1996 || [[CFM International CFM56|CFM56-5A5 or 5A5/F]]
|-
| A319-115 || 30 July 1999 || [[CFM International CFM56|CFM56-5B7 or 5B7/P]]
|-
| A319-131 || 18 December 1996 || [[International Aero Engines V2500|IAE Model V2522-A5]]
|-
| A319-132 || 18 December 1996 || [[International Aero Engines V2500|IAE Model V2524-A5]]
|-
| A319-133 || 30 July 1999 || [[International Aero Engines V2500|IAE Model V2527M-A5]]
|}

==See also==
{{Portal|Aviation}}
{{aircontent
|related=
* [[Airbus A318]]
* [[Airbus A320 family]]
|similar aircraft=
* [[Boeing 717]]
* [[Boeing 737 Classic|Boeing 737-300]]
* [[Boeing 737 Next Generation|Boeing 737-700]]
* [[Bombardier CSeries]]
* [[Comac C919]]
* [[Irkut MC-21]]
* [[Tupolev Tu-204]]
|lists=
* [[List of Airbus A320 operators]]
* [[List of jet airliners]]
* [[Lists of airlines]]
|see also=
* [[Airbus Corporate Jets]]
}}

==References==
'''Footnotes'''
{{notelist}}

'''References'''
{{Reflist|30em}}

'''Bibliography'''
* {{cite book |last=Eden |first=Paul E. (general editor) |title=Civil Aircraft Today |publisher=Amber Books |year=2008 |location=London |isbn=978-1-905704-86-6 |ref={{harvid|Eden|2008}}}}
* {{Cite book |last=Gunston |first=Bill |authorlink=Bill Gunston |title=Airbus: The Complete Story |year=2009 |location=Sparkford, Yeovil, Somerset, UK |publisher=Haynes Publishing |isbn=978-1-84425-585-6 |ref={{harvid|Gunston|2009}}}}
* {{Cite book |last1=Norris |first1=Guy and Mark Wagner |title=Airbus |location=St. Paul, Minnesota |publisher=MBI Publishing |year=1999 |isbn=0-7603-0677-X |ref={{harvid|Norris|Wagner|1999}}}}

== External links ==
{{Commons category|Airbus A320 family}}
* [http://www.airbus.com/aircraftfamilies/passengeraircraft/a320family/a319/ Official Airbus website of the A319]

{{Airbus aircraft}}
{{Airbus A3xx timeline}}
{{Use dmy dates|date=July 2016}}

[[Category:Airbus aircraft|A319]]
[[Category:International airliners 1990–1999]]
[[Category:Twinjets]]
[[Category:1996 introductions]]
[[Category:Airbus A320 family| ]]