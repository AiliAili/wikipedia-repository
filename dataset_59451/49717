{{italic title}}
[[File:BalzacCousinPons01.jpg|300px|thumb|right|Title page illustration of ''Cousin Pons'' (Philadelphia: George Barrie & Son, 1897).]]

'''''Le Cousin Pons''''' ({{IPA-fr|lə kuzɛ̃ pɔ̃s}}) is one of the last of the 94 works of [[Honoré de Balzac]]’s ''[[La Comédie humaine|Comédie humaine]]'', which are in both [[novel]] and [[short story]] form. Begun in 1846 as a [[novella]], or long-short story, it was envisaged as one part of a diptych, ''[[Les Parents pauvres]]'' (''The Poor Relations''), the other part of which was ''[[La Cousine Bette]]'' (''Cousin Bette''). The book was originally published as a serial in ''[[Le Constitutionnel]]''.

The novella grew in 1847 into a full-length novel with a male poor relation, Pons, as its subject, whereas ''La Cousine Bette'' describes the female aspect of that subordinate relationship. The two novels were thus similar yet diametrically different. They were complementary, forming two parts of a whole.

''Le Cousin Pons'' has been classified by Balzac as the second Episode of ''Les Parents pauvres'', the first Episode being ''La Cousine Bette''. Especially admired by [[Paul Bourget]], it is one of the very greatest of his novels.

==Plot summary==
The novella was based on a short story by an acquaintance of Balzac, [[Albéric Second]],<ref>A.  Second,“Histoire des deux bassons de l’Opéra”, ''[[Le Siècle]]'', 17–18 August 1841</ref> as Tim Farrant has demonstrated. Its original title was to have been “Le Parasite”. Sylvain Pons, a musician in a Parisian boulevard orchestra, has a close friend in another musician from that same orchestra, the German pianist Wilhelm Schmucke. They lodge with Mme Cibot, but Pons – unlike Schmucke – has two failings: his passion (which is almost a mania) for collecting works of art, and his passion for good food. Schmucke, on the other hand, has only one passion, and that is his affection for Pons. Pons, being a gourmet, much enjoys dining regularly with his wealthy lawyer cousins M. and Mme Camusot de Marville, for their food is more interesting than Mme Cibot’s and full of gastronomic surprises. In an endeavour to remain on good terms with the Camusots, and to repay their favour, he tries to find a bridegroom for their unappealing only child Cécile. However, when this ill-considered marriage project falls through, Pons is banished from the house.

The novella becomes a novel as Mme Camusot learns of the value of Pons’s art collection and strives to obtain possession of it as the basis of a dowry for her daughter. In this new development of the plot line a bitter struggle ensues between various vulture-like figures all of whom are keen to lay their hands on the collection: Rémonencq, Élie Magus, Mme Camusot – and Mme Cibot herself. Betraying his client Mme Cibot’s interests, the unsavoury barrister Fraisier acts for the Camusots. Mme Cibot sells Rémonencq eight of Pons’s choicest paintings, untruthfully stating in the receipt that they are works of lesser value. She also steals one for herself.

Horrified to discover his betrayal by Mme Cibot, and the plots that are raging around him, Pons dies, bequeathing all his worldly possessions to Schmucke. The latter is browbeaten out of them by Fraisier. He in turn dies a broken-hearted man, for in Pons he has lost all that he valued in the world. The art collection comes to the Camusot de Marville family, and the vultures profit from their ill-gotten gains.

==Fundamental themes of the work==
# ''Le Cousin Pons'' is set entirely in Paris, where, as Balzac informs us in his ''Avant-propos'' (''Foreword'') to the ''Comédie humaine'', “the extremes of good and evil are to be found”. However, ''Le Cousin Pons'' is not exclusively about the clash of extremes. Some characters, even the eponymous hero himself, are presented in a nuanced way.
# Balzac’s hatred of the [[bourgeoisie]] is epitomized by the greedy, money-obsessed M. and Mme Camusot de Marville who put up with the weekly visits of their poor relation Sylvain Pons until they realize he is a very wealthy art collector, whereupon their sole concern is to exploit him. Balzac also presents the lawyer Fraisier and the doctor Poulain in an ambivalent light.
# The morals of the working-class characters, e.g., La Cibot and Rémonencq, are scarcely any better than those of the bourgeoisie. As in Balzac’s novel of the countryside, ''Les Paysans'', the proletarian world is displayed in a fiercely aggressive, acquisitive light – almost to the extent of engaging in bitter class conflict.
# The values of art are contrasted with those of money. As Balzac says in ''[[Splendeurs et misères des courtisanes]]'', “la Charte ( [[Charter of 1814]] ) a proclamé le règne de l’argent, le succès devient alors la raison suprême d’une époque athée”. Artistic values aside, Balzac displays the reification or materialization of the world.
# The law is seen by Balzac as a way of depriving people of their rightful property. Harassed by Fraisier, Schmucke renounces his property rights. Pons’s second will is more vulnerable than the first.
# Balzac subverts conventional social values as social norms are revealed to be a fiction. The values of the Camusot de Marville family are materialistic. It is not the personality of Cécile Camusot herself but Pons’s art collection which is “the heroine of this story”; it is that, not her value as a person, which secures her marriage. The union of the Topinards, who are not strictly married, is the kindest, most affectionate relationship of man and woman in the novel. The friendship of Pons and Schmucke is true love but not love within marriage. The two men are poor and physically ugly but their relationship is golden and pure. Their Platonic friendship runs parallel to the idealizing function of art.
# Though not a lover in the human physical sense, Pons is a man with an overriding passion, the passion for artistic beauty.   In its etymological sense ''passion'' equates to suffering.   Pons is a Christ-like figure, like some other characters in Balzac's novels (e.g., Joseph Bridau in ''[[La Rabouilleuse]]'', and Goriot).   He is a man with a mania or ''idee fixe'', and this passion is the cause of his suffering and death.

==Narrative strategies==
# As has been shown by [[Donald Adamson]], ''Le Cousin Pons'' began its existence as a novella, or nouvelle, and was suddenly transformed into a full-length novel. This process of transformation necessitated certain inconsistencies and an uneasy transition from long-short story to fiction of sizable proportions and complexity. Though this longer fiction is often referred to as “Part II” of the novel, Balzac himself does not embark upon his “Part II” of ''Le Cousin Pons'' until all the new characters – the corrupt Mme Cibot, Rémonencq, Élie Magus, Poulain and Fraisier – have been introduced. It is in dispute whether these two narrative elements have been fused into a perfect whole. [[V.S. Pritchett]] considers that Balzac has been totally successful in combining the two storylines.<ref>V.S. Pritchett, ''''The Living Novel'', 1946, pp. 187, 195</ref>
# ''Le Cousin Pons'' thus became one of Balzac’s four inheritance novels (the others being ''[[Eugénie Grandet]]'', ''[[Ursule Mirouët]]'' and ''[[La Rabouilleuse]]''). From being the vignette of a downtrodden elderly man it mutated into a story of conflict, though with a plot far less complex than that of ''La Cousine Bette'' or ''Splendeurs et misères des courtisanes''. The struggle for an inheritance was one of the narrative situations most congenial to Balzac.
# In the tradition of [[melodrama]] Schmucke represents “extreme good”, Mme Camusot “extreme evil”, whereas Pons is an amalgam of the two whilst, Janus-like, Mme Cibot embodies aspects of both. The lurid tones of Pons’s deathbed scene are the height of melodrama. In this drama of light and darkness, or [[chiaroscuro]], the art collection is the heroine of the story.
# ''Roman-feuilleton'' ([[serial (literature)|serial]]). The serialization of novels was a feature of the rapid growth of the newspaper industry in France after 1814.   Leading ''feuilletonistes'' were [[Eugène Sue]], [[Alexandre Dumas, père]], [[Paul Féval, père]], Frédéric Soulié and [[Eugène Scribe]]. Balzac became increasingly preoccupied by their popularity in the 1840s and tried to emulate them.   This involved incorporating many features of [[melodrama]]; it also encouraged the ending of each serialized extract on a note of high suspense.
# The serialization of fiction also necessitated the increasing use of dialogue. This is particularly so in the later stages of the novel. In Donald Adamson’s words, “the second half of ''Le Cousin Pons'' is surely unsurpassed in the extent to which it uses dialogue and in the variety of purposes to which dialogue is applied. It contains few narrative interludes or other digressions”.<ref>D. Adamson, ''The Genesis of "Le Cousin Pons"'', 1966, p. 79</ref>  This gave the novel its markedly dramatic flavour.

==Bibliography==
*Honoré de Balzac, ''Les Parents pauvres'', 12 vols, Paris: Chlendowski et Pétion, 1847-1848 (''Le Cousin Pons'', vols 7-12, 1848)
*Honoré de Balzac, ''Les Parents pauvres'', vol. XVII and 1st supplementary vol. of ''La Comédie humaine'', Paris: Furne, 1848
*''Cousin Pons'' (translated by Herbert J. Hunt), Harmondsworth (Penguin Classics), 1968
*[[Donald Adamson]], ''The Genesis of “Le Cousin Pons”'', Oxford University Press, 1966
*Donald Adamson, “''Le Cousin Pons'': the ''paragraphe composé''”, ''Modern Language Review'', 1964, pp.&nbsp;209–213
*Donald Adamson and André Lorant, “L’''Histoire de deux bassons de l’Opéra'' et ''le Cousin Pons''”, ''Année balzacienne'', 1963, pp. 185-194
*[[Paul Bourget]], ''Nouvelles Pages de critique et de doctrine'', vol. I, Paris: Plon-Nourrit, 1922
*Tim Farrant, “Les Premières notes des deux bassons:  une source balzacienne retrouvée”, ''Année balzacienne'', 1995, pp.&nbsp;421–6
*René Guise, “Balzac et le roman-feuilleton”, ''Année balzacienne'', 1964, pp.&nbsp;283–338
*André Lorant,''Les Parents pauvres d’H. de Balzac,La Cousine Bette, Le Cousin Pons. Étude historique et critique'', 2 vols, Geneva: Droz, 1968
*[[V.S. Pritchett]], ''The Living Novel'', London (Chatto & Windus) 1946
* Scott Sprenger, "Le Cousin Pons ou l'anthropologie balzacienne du goût,” Année Balzacienne, Paris, PUF, 2009, 157-180.

==References==
{{Reflist}}

==External links==
* {{gutenberg|no=1856|name=Cousin Pons}}
* {{librivox book | title=Cousin Pons | author=Honoré de Balzac}}
* [http://mchip00.nyu.edu/lit-med/lit-med-db/webdocs/webdescrips/balzac282-des-.html Summary of the plot]

{{Honoré de Balzac}}

{{Authority control}}
{{DEFAULTSORT:Cousin Pons, Le}}
[[Category:1847 novels]]
[[Category:Books of La Comédie humaine]]
[[Category:Works originally published in Le Constitutionnel]]
[[Category:Novels first published in serial form]]