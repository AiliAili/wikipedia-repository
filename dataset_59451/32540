[[File:Old-40-Watt.jpg|thumb|right|The [[40 Watt Club]], a key venue in the city's punk rock scene, at its third location in Broad Street]]
The '''music of Athens, Georgia''', includes a wide variety of popular music and was an important part of the early evolution of [[alternative rock]] and [[New wave music|new wave]]. The city is well known as the home of chart-topping bands like [[R.E.M.]] and [[The B-52s]], C Fresh, and several long-time [[indie rock|indie /rock]] hip-hop groups. [[Athens, Georgia|Athens]] hosts the [[Athens Symphony Orchestra]] and other music institutions, as well as prominent local music media, such as the college radio station [[WUOG]].<ref>{{cite web|url=http://www.athenswelcomecenter.com/images/Athens_Music_History_Walking_Tour.pdf|format=PDF|title=Athens Music History Walking Tour|accessdate=February 11, 2008|publisher=Athens Welcome Center and ''Flagpole Magazine''|quote=Attributed to ''Rolling Stone'', February 20, 2003|year=1998}}</ref> Much of the modern Athens music scene relies on students from the large [[University of Georgia]] campus in the city. The University sponsors [[Western classical music|Western classical]] performances and groups specializing in other styles.

Athens became a center for music in the region during the [[American Civil War|Civil War]] and gained further fame in the early twentieth century with the founding of the [[Morton Theatre]], which was a major touring destination for African American performers. The city's local [[rock music]] scene can be traced to the 1950s, with live music at [[Allen's]] Hamburgers in [[Normaltown]]. International attention came in the 1970s when the B-52's began releasing the first of several best-selling recordings. Athens-based rock bands have performed in a wide array of styles, and the city has never had a characteristic style of rock; most of the bands have been united only in their quirky and iconoclastic image.<ref>Unterberger, pg. 133 Unterberger further notes that the only ''characteristic they shared was their willingness to do something different.''</ref>

Music author Richie Unterberger describes the town as an unlikely center for musical development, as a "sleepy [place where] it's difficult to imagine anyone working up a sweat, let alone playing rock music."<ref>Unterberger, pg. 133 reports that this claim was common in the 80s alternative rock press: ''It's a shock to drive into town for the first time through neighborhoods dominated by stately antebellum homes and modest, attractive suburban dwellings. Apart from a few blocks near the campus, things are so sleepy that it's difficult to imagine anyone working up a sweat, let alone playing rock music.''</ref> The contributions of Athens to [[rock music|rock]], [[country music]], and [[bluegrass music|bluegrass]] have earned it the nickname "the [[Music of Liverpool|Liverpool]] of the South", and the city is known as one of the American birthplaces for both modern alternative rock and new wave music.<ref>{{cite web|url=http://www.nationaltrust.org/magazine/archives/arc_news/072704.htm|title=Piece of Music History Needs a Tune-Up|date=July 27, 2004|first=Margaret|last=Foster|publisher=Preservation Online|quote=Preservation Online refers to Athens as the "birthplace of New Wave"}}</ref><ref>{{cite web|title=Athens has served as wellspring of musical talent|date=December 4, 2001|accessdate=February 11, 2008|first=Jeff|last=Montgomery|url=http://www.onlineathens.com/cgi-bin/rockathens/stories.cgi?/stories/120401/ath_bimusic.shtml|publisher=OnlineAthens|quote=OnlineAthens cites the "mother of modern music" to ''[[Esquire magazine|Esquire]]''}}</ref> Athens was home to the first and most famous college music scene in the country, beginning in the 1970s.<ref name="Subculture">{{cite journal|title=Subcultural Identity in Alternative Music Culture|first=Holly|last=Kruse|journal=Popular Music|volume=12|issue=1|date=January 1993|pages=33–41|doi=10.1017/S026114300000533X|jstor=931257}}</ref>

== Music venues and institutions ==
[[File:Pylon at AthFest 2005.jpg|thumb|left|200px|Pylon performing at the 2005 AthFest]]

Athens' local music is based primarily in the small downtown area of the northern part of the town. The [[nightclub]] [[40 Watt Club]] is among the most famous indie rock venues on the East Coast; the club opened on Halloween in 1978, with a band called Strictly American featuring Curtis Crowe, founder of the club and future member of the band [[Pylon (band)|Pylon]].<ref>{{cite web|url=http://www.40watt.com/history.php|publisher=40 Watt Club|title=40 Watt Club History|accessdate=February 11, 2008}}</ref> Other major music venues in the city include the Georgia Theater, (a converted [[Movie theater|cinema]] that hosted both local and touring performers. The Theater burned down in June 2009, but has been fully remodelled into a state of the art music venue reopening in early August 2011), the Caledonia Lounge, the historic Foundry Music Venue (which was originally built in 1850 as an iron foundry), and the UGA Performing Arts Center, home to the Ramsey Concert Hall and the Hugh Hodgson Concert Hall. The [[Morton Theater]] is a historic venue, a major part of the city's African American community in the early 20th century; it claims to be the only theater from that era remaining in operation.<ref name="Morton">{{cite web|title=About the Morton Building|work=History of the Morton Theatre|publisher=Morton Theatre|accessdate=February 11, 2008 |url=http://www.mortontheatre.com/history/aboutbuilding.htm |archiveurl = https://web.archive.org/web/20070928042214/http://www.mortontheatre.com/history/aboutbuilding.htm <!-- Bot retrieved archive --> |archivedate = September 28, 2007}}</ref>

Athens is home to the summer [[music festival]] [[Athfest]], [http://www.astoryworthhearing.org AlanFest], the [[Athens Popfest]] and the late spring Athens Human Rights Festival and [[North Georgia Folk Festival]]. The [[college radio]] station [[WUOG]] (90.5 FM), the low-power (100.7) FM [[WPPP-LP]] and the free weekly ''[[Flagpole Magazine|Flagpole]]'' are the city's most prominent modern music media. Athens has never produced a major local label like many similar indie rock towns; the most important label of the 1970s and 1980s was [[DB Records]], based out of Atlanta, though [[jangle pop]] pioneers [[Kindercore Records]] and [[Wuxtry Records]] were also Athens-based.<ref>Unterberger, pgs. 139–140 notes the lack of a major indie industry and mentions DB Records</ref>

Local music institutions include the [[Athens Symphony Orchestra]], [[Athens Choral Society]] (founded in 1971), [[Athens Youth Symphony]] and the [[Athens Folk Music and Dance Society]]. The Athens Symphony Orchestra was founded in 1978 as a firmly non-profit, strictly volunteer organization, conducted by Albert Ligotti of the University of Georgia. The first performance came in 1979; the Orchestra now has two regular performances, one in the summer and one in the winter, and has also done shows for young people, pops concerts and Christmas concerts. In 1996, the Athens Symphony moved into its modern home, the Classic Center Theatre in downtown Athens.<ref>{{cite web|url=http://www.athenssymphony.org/history.html|archive-url=https://archive.is/20080209144833/http://www.athenssymphony.org/history.html|dead-url=yes|archive-date=February 9, 2008|work=The Story of the Athens Symphony|last=Rapp|first=Carl|title=Portrait of the Athens Symphony|publisher=Athens Symphony Orchestra|accessdate=February 11, 2008}}</ref>

The University of Georgia's Athens campus has long been an important part of local music. Country Music Hall of Fame song writwer and performer "Whispering" Bill Anderson attended UGA and used to play guitar around campus. The faculty of the Hugh Hodgson School of Music operate the Georgia Brass Quintet and Georgia Woodwind Quintet. Student institutions include the ARCO Chamber Orchestra, Men's and Women's [[Glee club|Glee Clubs]], several concert choirs, jazz bands, and brass and woodwind ensembles, the Redcoat [[marching band|Marching Band]], the University Philharmonia and a Symphony Orchestra. The University of Georgia also has multiple a cappella groups, including the Ecotones, Noteworthy, and the [[UGA Accidentals|Accidentals]], who are regionally known.<ref>{{cite web|url=http://www.music.uga.edu/ensembles/#faculty|work=Hugh Hodgson School of Music|accessdate=February 11, 2008|title=Faculty Ensembles|publisher=Performing Ensembles at the University of Georgia}}</ref>

[[File:Uptown-Lounge.jpg|thumb|right|240px|The first location of the Uptown Lounge (40 Watt's rival). Uptown would become the Georgia Theater the next year and provide a very large local venue.]]

== History ==
The earliest music in [[North Georgia]], including what is now Athens, was that of the Native Americans of the area, principally the [[Creek people|Creek]] and the [[Cherokee]]. Athens was officially chartered in 1806, and began growing rapidly near the middle of the 19th century. By the time of the [[American Civil War]] in the 1860s, the city was an important part of musical life in Georgia. The war accelerated the development of the city's musical importance, as Athens was largely spared widespread destruction while the larger city of Atlanta took a long time to recover. Major touring acts like the [[Dixie Family]] and [[The Slomans]] visited Athens during the war; the Dixie Family, a prominent touring group, performed disastrously, according to local newspapers, who said that the highlight of the performance came from four local African American musicians, and the Dixie Family had absconded with the concert's proceeds, which had been promised to the local Ladies Aid Society.<ref>Abel, pgs. 246–247</ref> In the 1870s, the city was almost half African American, and local black-owned industry flourished; among the residents was [[Bob Cole (composer)|Bob Cole]], born in 1868 to a musically active family. Cole would later become a pioneer in African American theater, known for works like the 1898 musical ''[[A Trip to Coontown]]''.<ref>{{cite journal|title="Bob" Cole: His Life and His Legacy to Black Musical Theater|first=Thomas L.|last=Riis|journal=The Black Perspective in Music|volume=13|issue=2|date=Autumn 1985|pages=135–150|doi=10.2307/1214581|publisher=The Black Perspective in Music, Vol. 13, No. 2|jstor=1214581}}</ref>

African American industry, churches and other institutions grew rapidly in prominence through the end of the 19th century. The city's [[African American]] community was well established by the beginning of the 20th century, when the corner of Lumpkin and Washington Streets became a major center for the city's black culture. This area was known as the ''Hot Corner'', and was owned by a number of black professional businesses, as well as many performance spaces and a renowned [[opera]] house in the Morton Building that hosted such national figures as [[Louis Armstrong]] and [[Duke Ellington]].<ref>{{cite web|title=City of Athens History|url=http://www.athensclarkecounty.com/history/athens.htm|accessdate=February 11, 2008|work=ACConline|archiveurl=https://web.archive.org/web/20080129034119/http://www.athensclarkecounty.com/history/athens.htm <!--Added by H3llBot-->|archivedate=January 29, 2008}}</ref> The [[Morton Theater]] was one of the preeminent venues in the city in the early 20th century, and is the only such theater to survive to the present, though it was not in operation for many years, until re-opening in 1993.<ref name="Morton"/>

=== Origins of the modern scene ===

In the 1950s, the city's musical life consisted primarily of dances at local venues like the [[American Legion|American Legion Hall]] and the [[YMCA]], where popular bandleaders included most famously [[Jimmy Dorsey]]. The Canteen was a spot in Memorial Park in Athens, which became an important performance space after local musician Terry "Mad Dog" Melton and his group began playing there in 1958. The Canteen later hosted local [[Motown]]/[[beach music|beach]] legends [[The Jesters (band)|The Jesters]], who have continued to perform from 1964 to the present.<ref name="Online Athens"/>

Later in the 1960s and into the 1970s, locally prominent bands gradually changed from primarily cover bands to more well rounded groups, while the city's musical opportunities grew with the foundation of venues and institutions. This period has been called the ''Normaltown River of Music'', and included long-time local performers like Mad Dog Melton as well as Brian Burke, Davis Causey and [[Randall Bramblett]], many of whom later worked with [[Gregg Allman]] and the [[Nitty Gritty Dirt Band]]. The most influential local bands to emerge from this period included the Normaltown Flyers, and Dixie Grease. The oldest bar featuring live music in Athens is [[Allen's]] Hamburgers where the Normaltown Flyers were the house band for many years. [[Allen's]] opened in 1955 on Prince Avenue, closed for a brief time, and has since reopened on Hawthorne Ave. as part of a restoration project. Bars like The Last Resort (currently the Last Resort Grille restaurant) opened in the 1960s, beginning the local club scene just as some bands were beginning to gain some regional fame for Athens.<ref name="Online Athens">{{cite web|title=Athens has served as wellspring of musical talent|date=December 4, 2001|accessdate=February 11, 2008|first=Jeff|last=Montgomery|url=http://www.onlineathens.com/cgi-bin/rockathens/stories.cgi?/stories/120401/ath_bimusic.shtml|publisher=OnlineAthens}}</ref>

=== Rock ===
[[File:New-40-Watt.jpg|thumb|left|240px|The 2nd location of the 40 Watt Club]]
In the late 1970s, the 40 Watt Club became a well-known regional attraction for music fans, and was followed by the Uptown Lounge; with the local industry's growth in the 1980s, both the 40 Watt Club and the Uptown Lounge moved to larger spaces, the latter taking over the landmark [[Georgia Theatre]]. The early 1980s saw a host of new bands and venues appear, while the city's musical subculture became more diversified. [[LSD]], a [[hallucinogen]]ic drug, was widely used in the college music scene in this era.<ref name="Subculture"/> Many members of Athens's most prominent later bands became locally renowned starting in the 1970s, including The B-52's.<ref>Unterberger, pgs. 133–134 Unterberger states that the scene ''was brewing... even before the [[Sex Pistols]] made one of their few 1970s American appearances in nearby Atlanta in early 1978.''</ref>

''Ort's Oldies'', a used record store on Jackson Street, and its proprietor, William Orten Carlton, commonly known as ''Ort'', were among the institutional figures that made the Athens music scene possible. Ort has an excellent memory for rock trivia, which served him well in running the store. Perhaps more importantly, his off-the-wall sense of humor and warmly iconoclastic personality (and his thrift-sale wardrobe) were regularly on display at parties, gigs and musical venues around town.<ref name="JuneAthens"/>

A final element in creating and sustaining the Athens musical culture was the University of Georgia [[Lamar Dodd School of Art]]. The great majority of Athens' musicians and their fan base were associated with the University's liberal arts curriculum, and the School of Art, rather than the music department, was the area where the creative and musical alliances that later defined the scene began forming in the 1970s. [[Michael Stipe]] of R.E.M. was an art major (although he did not graduate), and the Art School incubated other major figures such as Curtis Crowe, founding member and drummer for [[Pylon (band)|Pylon]]. The cinematographer for the documentary film ''Athens GA: Inside/Out'' was Jim Herbert, an art school professor. Herbert went on to direct music videos for a number of Athens bands, including 14 for R.E.M.<ref name="JuneAthens">{{cite web|url=http://www.athensmagazine.com/jun06_insideout.html|publisher=Athens Magazine|date=June 2006|accessdate=February 11, 2008|last=Phillips|first=Julie|title=Athens, GA – Inside/Out|archiveurl = https://web.archive.org/web/20080118042058/http://www.athensmagazine.com/jun06_insideout.html |archivedate = January 18, 2008|deadurl=yes}}</ref>

{{listen|
filename=R.E.M. - Radio Free Europe.ogg|
title="Radio Free Europe"|
description=Breakthrough hit by R.E.M.|
format=[[Ogg]]}}
{{listen|
filename=The B-52's - Love Shack.ogg|
title="Love Shack"|
description=Major hit by The B-52's|
format=[[Ogg]]}}
{{listen|
filename=Bubba Sparxxx - Ugly.ogg|
title="Ugly"|
description=First major hip hop single from Athens, by Bubba Sparxxx|
format=[[Ogg]]}}
{{listen|
filename=Matthew Sweet - Evangeline.ogg|
title="Evangeline"|
description=Recording by the underground, Athens-based performer Matthew Sweet|
format=[[Ogg]]}}
{{listen|
filename=Neutral Milk Hotel - 2 Head.ogg|
title="Two-Headed Boy"|
description=Recording by Neutral Milk Hotel indie rock band and member of the [[Elephant 6]] collective|
format=[[Ogg]]}}
{{listen|
filename=Drive-By Truckers - My Swee.ogg|
title="My Sweet Annette"|
description=Recording by modern Southern rock band Drive-By Truckers|
format=[[Ogg]]}}
{{listen|
filename=Widespread Panic - Ain't Li.ogg|
title="Ain't Life Grand"|
description=Song by the popular [[jam band]] Widespread Panic|
format=[[Ogg]]}}
{{listen|
filename=Pylon_-_Feast_On_My_Heart.ogg|
title="Feast on My Heart"|
description=Song by Pylon, one of the long-time underground legends of the Athens scene|
format=[[Ogg]]}}
[[File:Magnapop trio 2007.jpg|alt=Magnapop performs onstage|thumb|[[Oh-OK]] member [[Linda Hopper]] would later form the Atlanta-based [[Magnapop]] with [[Ruthie Morris]]. The band is pictured here performing in 2007. From left to right: bassist Scott Rowe, drummer Chad Williams (obscured except for his hands), singer Linda Hopper, and guitarist and backing vocalist Ruthie Morris.]]
The B-52's and R.E.M. became by far the most famous musical products of Athens in the 1980s, when both bands launched a string of hits. Their roots in the city's local scene go back to the 1970s and early 1980s. The B-52's formed after a St. Valentine's Day party in 1977. The members had little musical knowledge, but performed [[new wave music]] with a cheeky and humorous image and sound. They were known for their [[Camp (style)|campy]] thrift store fashion, and their unusual and eye-catching [[music video]]s for hits like "[[Rock Lobster]]" and "[[Love Shack]]".<ref>Unterberger, pgs. 133–135</ref> Though the B-52's were the first Athens band to achieve national prominence, their popularity was soon eclipsed by [[R.E.M. (band)|R.E.M.]]. The future members of the band [[R.E.M. (band)|R.E.M.]] moved to Athens to work and/or attend the University of Georgia, including bassist [[Mike Mills]] and former drummer [[Bill Berry]]. The group began performing as R.E.M. in 1980. They became locally prominent, and released a single, "[[Radio Free Europe (song)|Radio Free Europe]]", that was a major [[college rock]] hit. Their popularity grew with a series of singles, EPs and albums that made R.E.M. the top underground band in the country, finally breaking into the mainstream with 1987's "[[The One I Love (R.E.M. song)|The One I Love]]" and "[[It's the End of the World as We Know It (And I Feel Fine)]]". By 1991's ''[[Out of Time (album)|Out of Time]]'' album&nbsp;— which featured vocals by [[Kate Pierson]] of The B-52s&nbsp;— and its acclaimed follow-up ''[[Automatic for the People]]'' (named after the motto of Weaver D's, a local Athens soul-food eatery), R.E.M. had become one of the world's biggest rock bands. The band's style went through many evolutions but originally had a [[jangle pop]] sound and harmonies often compared to [[folk-rock]] band [[The Byrds]]; singer and songwriter [[Michael Stipe]] is known for obscure, allusive lyrics delivered in a monotonous drone.<ref>Unterberger, pgs. 135–136 "Radio Free Europe" was chosen as the "best independent single of the year" by the major New York magazine ''[[The Village Voice]]''. "The One I Love" was R.E.M.s first Top Ten hit in the United States.''</ref> The success of R.E.M. and the B-52's brought attention from major labels and music media to Athens, and many local bands received a career boost.

The band Pylon was a long-standing and influential part of the Athens scene, and became critical darlings in the 1980s, but never achieved significant mainstream success. This was partially because they eschewed several record contracts from the major labels due to a lack of trust in their corporate goodwill. Pylon's dance rock style was not very accessible or commercial, and was accompanied grating, chanting-style vocals, funky guitars and bass-heavy beats. Other 1980s local bands with nationwide alternative followings included [[Love Tractor]], [[Oh-OK]], with Michael Stipe's sister [[Lynda Stipe]], vocalist Linda Hopper (later of Magnapop) and future solo performer [[Matthew Sweet]], [[Dreams So Real]]. The members of R.E.M. have remained fixtures in Athens as they have also become international stars, helping out local performers like [[Vic Chesnutt]], the [[Chickasaw Mudd Puppies]] and [[Jack Logan]]&nbsp;.<ref>Unterberger, pgs. 134-137</ref> The [[Elephant 6 Collective]], a group of like-minded indie bands, gained limited nationwide exposure starting in the mid-1990s with the rise of [[Neutral Milk Hotel]], [[Elf Power]] and [[Olivia Tremor Control]]. The same period saw the [[Kindercore Records]] roster find critical acclaim, including the bands [[Sunshine Fix]], [[Masters of the Hemisphere]], [[Japancakes]], Love Tractor, Gresham Disco and [[Of Montreal]]. Candy, a DJ store owned by Michael Lachowski of Pylon, opened in 1998; the store became an important part of the burgeoning dance music scene that produced [[Danger Mouse (musician)|Danger Mouse]], [[Phungus]] and [[DJ 43]].<ref name="Online Athens"/>

=== Other styles ===
Athens is near the [[Blue Ridge Mountains]] area of North Georgia; this is an important region in the development of several varieties of folk music, including the [[Appalachian folk music|Appalachian]] [[bluegrass music|bluegrass]] style and the [[Piedmont blues]]. North Georgia's bluegrass heritage can be traced back to the 19th century, when bluegrass was a nascent style throughout Appalachia and North Georgia was home to major fiddling contests, beginning in the 1880s. A 1983 recording expedition by Art and Margo Rosenbaum documented the continued existence of many forms of folk music, including work songs, string bands, African American hymns and [[spiritual (music)|spirituals]], banjo tunes and unaccompanied ballads; the collection includes a chapter devoted to Doc and Lucy Barnes of Athens.<ref>{{cite journal|title=Review of ''Folk Visions and Voices: Traditional Music and Song in North Georgia''|author2=Rosenbaum, Margo|author3=Rosenbaum, Art|author4=Foltin, Béla, Jr.|first=Lee|last=Haring|journal=Ethnomusicology|volume=28|issue=3|date=September 1984|pages=564–565|doi=10.2307/851249|jstor=851249}}</ref><ref>{{cite journal|title=Review of ''Folk Visions and Voices: Traditional Music and Song in North Georgia''|author2=Rosenbaum, Art|first=William E.|last=Lightfoot|journal=Journal of American Folklore|volume=98|issue=389|date=July–September 1985|pages=351–353|jstor=539949}}</ref><ref>{{cite journal|title=Review of ''Folk Visions & Voices. Traditional Music and Song in North Georgia''|author2=Rosenbaum, Art|author3=Rosenbaum, Margo Newmark|author4=Foltin, Béla, Jr.|first=James|last=Porter|journal=Jahrbuch für Volksliedforschung|issue=32|year=1987|pages=201–202|doi=10.2307/849481|volume=32|jstor=849481}}</ref> Athens' modern contributions to the field of bluegrass include the [[Packway Handle Band]] and [[BlueBilly Grit]].<ref>{{cite web|url=http://ngeorgia.com/mountains/blueridgemountains.html|accessdate=February 11, 2008|title=The Blue Ridge Mountains|publisher=About North Georgia|author=Golden Ink}}</ref>

Athens' local country scene has never been as significant as the profusion of indie rock bands; however, modern Athens rock takes many elements from the folk, bluegrass and country traditions, including such bands as the Normaltown Flyers. The band [[Drive-By Truckers]], and the [http://www.holmanautryband.com Holman Autry Band], have done much in recent years to make [[country rock]] a major part of Athens' musical identity. The rapper [[Bubba Sparxxx]], originally from South Georgia, has also helped diversify Athens' country heritage, by adding a rural image and elements of country music to his [[Dirty South (music)|Dirty South]] style of [[hip hop music]].<ref>{{cite web|url=http://www.popmatters.com/columns/poole/040428.shtml|title=Catfish Row: Redneck Chic and Hip-hop Get Down and Dirty |publisher=PopMatters|accessdate=February 11, 2008|last=Poole|first=W. Scott|date=April 28, 2004}}</ref>

Folk artists and singer-songwriters have always flourished in the Athens atmosphere, albeit, as mentioned, not as significantly as Pop and Rock. Some of Athens' most notable solo singer-songwriter performers are [[Vic Chesnutt]], Nathan Sheppard, [[Corey Smith (musician)|Corey Smith]], [[John Berry (singer)|John Berry]], [[Patterson Hood]], Ricky Fitzpatrick, along with younger, emerging musicians like [[Thayer Sarrano]] and T. Hardy Morris.

Athens also has an Irish band community representing several Irish folk bands (e.g. The Green Flag Band, Repent at Leisure).

Recently, a [[Latin music (genre)|Latin music]] scene has emerged with a diverse array of different musical styles that include [[bossa nova]], [[samba]], [[salsa music]], and [[tango]]. Notable groups include Grogus,<ref>{{cite web|url=http://onlineathens.com/stories/062311/mar_847791200.shtml|title=Band Bios |publisher=Online Athens|accessdate=September 15, 2014|date=June 23, 2011}}</ref><ref>{{cite web|url=http://flagpole.com/music/music-features/2012/06/27/flagpole-athens-music-awards|title=Flagpole Athens Music Awards |publisher=Flagpole|accessdate=September 15, 2014|last=Davis|first=Michelle|date=June 27, 2012}}</ref> Incatepec,<ref>{{cite web|url=http://onlineathens.com/stories/122208/mar_369996406.shtml|title=Incatepec members sing Venezuelan song about a Donkey |publisher=Online Athens|accessdate=September 15, 2014|last=Jones|first=Beth|date=December 22, 2008}}</ref> [[Athens Tango Project]],<ref>{{cite web|url=http://flagpole.com/music/music-features/2014/05/14/athens-tango-project-s-worlds-of-sound|title=Athens Tango Project's Worlds of Sound |publisher=Flagpole|accessdate=September 15, 2014|last=Bailey|first=Rachel|date=May 14, 2014}}</ref><ref>{{cite web|url=http://musicawards.flagpole.com|title=The 2014 Flagpole Athens Music Awards|publisher=Flagpole|accessdate=September 15, 2014|date=May 14, 2014}}</ref> and Quiabo De Chapeu.<ref>{{cite web|url=http://flagpole.com/news/pub-notes/2013/08/28/moving-on|title=Moving On |publisher=Flagpole|accessdate=September 15, 2014|last=McCommons|first=Pete|date=August 28, 2013}}</ref>

=== Video game music ===

Athens was the home of [[Robert Prince (video game composer)|Robert Prince]] (Bobby Prince&nbsp;– a long time Athens musician) when he wrote the music and created the sound effects for early computer and video games, including the ''[[Commander Keen]]'' series, ''[[Wolfenstein 3D]]'' and ''[[Doom (1993 video game)|Doom]]''.

=== Post-2000 ===

Recently, Athens has produced some notable rock bands, including  [[Dead Confederate]], [[Futurebirds]], [[Reptar]], [[The Whigs (band)|The Whigs]], New Madrid,  [[of Montreal]], [[Perpetual Groove]], [[Phosphorescent]] and [[Lera Lynn]].

== See also ==

* [[Howard Finster]]

{{portal|Georgia (U.S. state)}}

== Notes ==
{{Reflist|30em}}

== References ==

* &nbsp;{{cite web
|url=http://www.40watt.com/history.php
|publisher=40 Watt Club
|title=40 Watt Club History
|accessdate=February 11, 2008
}}
* &nbsp;{{cite book
|last=Abel
|first=E. Lawrence
|title=Singing the New Nation: How Music Shaped the Confederacy, 1861–1865
|edition=First
|location=Mechanicsburg, Pennsylvania
|publisher=Stackpole Books
|isbn=0-8117-0228-6|year=2000
}}
* &nbsp;{{cite web
|title=City of Athens History
|url=http://www.athensclarkecounty.com/history/athens.htm
|accessdate=February 11, 2008
|publisher=ACConline
|archiveurl=https://web.archive.org/web/20080129034119/http://www.athensclarkecounty.com/history/athens.htm <!--Added by H3llBot-->
|archivedate=January 29, 2008
}}
* &nbsp;{{cite web
|url=http://www.athenswelcomecenter.com/images/Athens_Music_History_Walking_Tour.pdf
|format=PDF
|title=Athens Music History Walking Tour
|accessdate=February 11, 2008
|publisher=Athens Welcome Center and ''Flagpole Magazine''
|quote=Attributed to ''Rolling Stone'', February 20, 2003
|year=1998
}}
* &nbsp;{{cite web
|url=http://www.nationaltrust.org/magazine/archives/arc_news/072704.htm
|title=Piece of Music History Needs a Tune-Up
|date=July 27, 2004
|last=Foster
|first=Margaret
|publisher=Preservation Online
|quote=Preservation Online refers to Athens as the "birthplace of New Wave"
}}
* &nbsp;{{cite web
|url=http://ngeorgia.com/mountains/blueridgemountains.html
|accessdate=February 11, 2008
|title=The Blue Ridge Mountains
|publisher=About North Georgia
|author=Golden Ink
}}
* &nbsp;{{cite web
|title=Touring Athens, Ga., with the B-52's: Southern college city is alternative rock's hometown
|publisher=The Washington Post
|work=The Anti-Orange Page
|url=http://antiorange.dawgtoons.com/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=74&page=1
|last=Hendrix
|first=Steve
|accessdate=February 11, 2008
| archiveurl = https://web.archive.org/web/20070927021656/http://antiorange.dawgtoons.com/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=74&page=1| archivedate = September 27, 2007}}
* &nbsp;{{cite web
|url=http://www.music.uga.edu/ensembles/#faculty
|publisher=Hugh Hodgson School of Music
|accessdate=February 11, 2008
|title=Faculty Ensembles
|work=Performing Ensembles at the University of Georgia
}}
* &nbsp;{{cite web
|title=Athens has served as wellspring of musical talent
|date=December 4, 2001
|accessdate=February 11, 2008
|first=Jeff
|last=Montgomery
|url=http://www.onlineathens.com/cgi-bin/rockathens/stories.cgi?/stories/120401/ath_bimusic.shtml
|publisher=OnlineAthens
}}
* &nbsp;{{cite web
|title=About the Morton Building
|work=History of the Morton Theatre
|publisher=Morton Theatre
|accessdate=February 11, 2008
|url=http://www.mortontheatre.com/history/aboutbuilding.htm
|archiveurl = https://web.archive.org/web/20070928042214/http://www.mortontheatre.com/history/aboutbuilding.htm <!-- Bot retrieved archive --> |archivedate = September 28, 2007}}
* &nbsp;{{cite web
|url=http://www.popmatters.com/columns/poole/040428.shtml
|title=Catfish Row: Redneck Chic and Hip-hop Get Down and Dirty
|publisher=PopMatters
|accessdate=February 11, 2008
|last=Poole
|first=W. Scott
|date=April 28, 2004
}}
* &nbsp;{{cite web
|url=http://www.athenssymphony.org/history.html
|archive-url=https://archive.is/20080209144833/http://www.athenssymphony.org/history.html
|dead-url=yes
|archive-date=February 9, 2008
|work=The Story of the Athens Symphony
|last=Rapp
|first=Carl
|title=Portrait of the Athens Symphony
|publisher=Athens Symphony Orchestra
|accessdate=February 11, 2008
}}
* &nbsp;{{cite web
|first=S.
|last=Thomas
|accessdate=February 11, 2008
|title=''Music to Eat''   by the Hampton Grease Band
|year=2004
|url=http://hamptongreaseband.com/
}}
* &nbsp;{{cite book
|last=Unterberger
|first=Richie
|title=Music USA: The Rough Guide
|publisher=The Rough Guides
|year=1999
|pages=133–140
|isbn=1-85828-421-X
}}

== Further reading ==
* {{cite book|publisher=Everthemore Books|isbn=0-9743877-0-3|year=2003|title=Party out of Bounds|author=Brown, Rodger Lyle}}
* {{cite book|title=Folk Visions and Voices: Traditional Music and Song in North Georgia|author2=Margo Rosenbaum|first=Art|last=Rosenbaum|others=Musical transcriptions by Béla Foltin, Jr., foreword by [[Pete Seeger]]|publisher=University of Georgia Press|location=Athens, Georgia|year=1983}}

== External links ==
* [http://40watt.com/ 40 Watt Club]
* [http://newearthmusichall.com/ New Earth]
* [http://alanfest.org/ AlanFest]
* [http://athfest.com/ AthFest]
* [http://www.music.uga.edu/ Hugh Hodgson School of Music] at the University of Georgia
* [http://athensmusic.net/newsdesk_info.php?newsPath=3&newsdesk_id=84 Music history tour of Athens]
* [http://wuog.org/ WUOG 90.5fm, Athens' College Radio]

{{featured article}}

[[Category:American music by city|Athens]]
[[Category:Athens, Georgia]]
[[Category:Music of Georgia (U.S. state)|Athens]]
[[Category:Music scenes]]
[[Category:R.E.M.]]
[[Category:The B-52's]]