{{Infobox single
| Name      = Boys
| Cover     = Boys COED rmx.png
| Border    = yes
| Artist    = [[Britney Spears]] featuring [[Pharrell Williams]] of [[N.E.R.D]]
| Album     = [[Britney (album)|Britney]] and [[Austin Powers in Goldmember#Soundtrack|Austin Powers in Goldmember soundtrack]]
| Released  = {{start date|2002|07|29}}
| Format    = {{flat list|
*[[CD single]]
*[[12-inch single|12"]]
}}
| Recorded  = 2001
| Genre     = {{flat list|
*[[Hip hop music|Hip hop]]
*[[Contemporary R&B|R&B]]
*[[funk]]
}}
| Length    = {{duration|m=3|s=28}} <small>(album version)</small> <br /> {{duration|m=3|s=46}} <small>(remix)</small>
| Label     = [[Jive Records|Jive]]
| Writer    = {{flat list|
*[[Chad Hugo]]
*[[Pharrell Williams]]
}}
| Producer  = [[The Neptunes]]
| Chronology     = [[Britney Spears]] singles
| Last single    = "[[Anticipating]]"<br /> (2002)
| This single    = "'''Boys'''"<br />(2002)
| Next single    = "[[Me Against the Music]]"<br />(2003)
| Misc           = {{Extra chronology
| Artist         = [[Pharrell Williams]] singles
| Type           = singles
| Last single    = "[[Formal Invite]]"<br />(2002)
| This single    = "'''Boys'''"<br />(2002)
| Next single    = "[[When the Last Time]]"<br />(2002)
{{External music video|{{YouTube|s25OMP4Ww6Y|"Boys"}}}}
}}}}

"'''Boys'''" is a song recorded by American singer [[Britney Spears]] for her third studio album, ''[[Britney (album)|Britney]]'' (2001). It was written and produced by [[Chad Hugo]] and [[Pharrell Williams]] (known collectively as [[The Neptunes]]). A version of the song, entitled the "The Co-Ed Remix", was released as the sixth and final single from ''Britney'' on July 29, 2002. The new version also served as the second single from the [[Austin Powers in Goldmember#Soundtrack|soundtrack]] of ''[[Austin Powers in Goldmember]]''. "Boys" is a [[Contemporary R&B|R&B]] and [[hip hop music|hip hop]] song, including [[funk music|funk]] influences. The remix carries a slower tempo than the album version, and both versions are noted to be reminiscent of [[Janet Jackson]]. Some critics praised Williams and Spears' chemistry, as well as the production on the track, while others did not think the song worked well.

While the song did not perform well on the ''[[Billboard (magazine)|Billboard]]'' charts in the United States, it reached the top ten on the Belgian charts and in Ireland and the United Kingdom, and charted in the top 20 in Australia, Germany, Austria, Switzerland, Sweden, Finland, and Denmark. The song would later be certified Gold in Australia. The song's accompanying music video, directed by [[Dave Meyers (director)|Dave Meyers]], was nominated at the [[2003 MTV Video Music Awards]] for "Best Video from a Film." The clip features Spears and Williams at a party. The singer has performed "Boys" a number of times including at the [[2002 NBA All-Star Game]], on ''[[Saturday Night Live]]'', and on several of her concert tours.

==Background==
"Boys" was originally recorded by [[Janet Jackson]] before it was given to Spears.<ref name=neptunes>{{cite web|url=http://startrakmusic.com/web/neptunes_all.aspx |title=All Projects |publisher=Startrak |year=2002 |accessdate=May 14, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20020806184614/http://startrakmusic.com/web/neptunes_all.aspx |archivedate=August 6, 2002 }}</ref> The song was originally included on Spears' third, [[Britney (album)|self-titled studio album]].<ref name="AOL"/> It was written and produced by [[The Neptunes]] ([[Chad Hugo]] and [[Pharrell Williams]])&nbsp;— members of American group [[N.E.R.D.]].<ref name="AOL"/> The song was re-recorded as "The Co-Ed Remix", with a slightly different sound, and added featured vocals from Pharrell Williams.<ref name="AOL"/> Following its inclusion on the ''[[Austin Powers in Goldmember#Soundtrack|Austin Powers in Goldmember OST]]'', it was released as the second single from the soundtrack, following "[[Work It Out (Beyoncé Knowles song)|Work It Out]]", performed by one of the film's stars', [[Beyoncé]].<ref name="AOL"/> The release also made it the sixth single release to come from ''Britney''. A press release for [[Maverick Records]] called the track a standout from the film's soundtrack, and said that Spears' cameo in the film "brings superstar status to the Fembot brigade."<ref name="About"/> The release also stated that the song and video "stays true to the fractured funk [at] the heart of the film."<ref name="About"/> [[AOL Music]] premiered the new track on June 13,&nbsp;2002, and it was streamed more than 1.35 million times, setting a new record an AOL Music "First Listen."<ref name="AOL">{{cite web|url=http://goliath.ecnext.com/coms2/gi_0199-1855380/Britney-Spears-World-Debut-of.html|title=Britney Spears'' World Debut of `Boys -Co-Ed Remix-'' From Austin Powers in Goldmember Streamed 1.35 Million Times in 24 Hours, Smashing AOL Music''s Previous `First Listen'' Records.|work=Business Wire|publisher=Goliath Business/[[The Gale Group]]|accessdate=February 11, 2011|date=June 13, 2002}}</ref>

==Composition==
{{Listen
|filename=BritneySpears_Boys.ogg
|description = A sample of the Co-Ed remix of "Boys", with Spears trading lines with Pharrell Williams
|title = "Boys" (The Co-Ed Remix)
|pos = left
}}

"Boys" is a song that combines [[Contemporary R&B|R&B]] with [[hip hop music|hip hop]].<ref>{{cite web|url=http://www.digitalspy.co.uk/music/albumreviews/a187837/britney-spears-the-singles-collection.html#|title=Britney Spears: 'The Singles Collection'|publisher=[[Digital Spy]]. [[Hachette Filipacchi Médias]]|first=Mayer|last=Nissim|date=November 20, 2009|accessdate=December 1, 2012}}</ref> According to the sheet music published at Musicnotes.com by [[EMI|EMI Music Publishing]], it is set in the [[time signature]] of [[common time]] and has a [[tempo (music)|tempo]] of 108 beats per minute.<ref>{{cite journal|author=Chad Hugo, Pharrell Williams|title=Boys: Britney Spears Digital Sheet Music|year=2001|work=Musicnotes.com |publisher=[[EMI|EMI Music Publishing]]|at=SC0012017 (Product Number)}}</ref> While the original ''Britney'' version features Spears solely, "The Co-ed Remix" which was released as a single, sees the singer and the song's co-producer, rapper Pharrell Williams trading lines.<ref name="Needham"/> The remix version of the track contains a slower tempo then that of the original.<ref name="Needham"/> According to the ''[[Milwaukee Journal Sentinel]]'', Williams and Spears put on a "[[rap music|rap]]-lite [[teen-pop]] tease."<ref>{{cite web|url=https://news.google.com/archivesearch?q=Britney+Spears+Boys+Co-Ed+remix&scoring=a&sa=N&sugg=d&as_ldate=2002&as_hdate=2003&lnav=hist6|title=Milwaukee Journal Sentinel : CD SPOTLIGHT|publisher=NewsBank|work=[[Milwaukee Journal Sentinel]]|accessdate=February 12, 2011|date=July 26, 2002}}</ref> The song also includes influences of [[funk music]].<ref name="About">{{cite web|url=http://movies.about.com/library/weekly/aa062902a.htm|title=Get Funky With "Austin Powers" Sizzling Soundtrack|work=''[[About.com]]''|publisher=[[The New York Times Company]]|date=July 2, 2002|accessdate=February 12, 2011}}</ref> During the time of the album's release, her official site stated that the song had aspects of 70's [[soul music]] and influences of [[Prince (entertainer)|Prince]]'s music.<ref>{{cite web|url=http://www.britneyspears.com/album/review.php|title=Britney The Album|work=''Britneyspears.com''|accessdate=February 12, 2011|publisher=NVU Productions|archiveurl=https://web.archive.org/web/20020803075143/http://www.britneyspears.com/album/review.php|archivedate=August 3, 2002}}</ref> According to Alex Needham of ''[[New Musical Express]]'', the track takes influence from [[Janet Jackson]].<ref name="Needham"/> David Browne of ''[[Entertainment Weekly]]'' said the song was "cut-rate '80s Janet Jackson."<ref name="Browne"/> Lyrical content sees Spears eying a guy with an intent to "get nasty."<ref>{{cite web|url=http://www.pluggedin.com/music/albums/2001/britneyspears-britney.aspx|title=Britney&nbsp;– Plugged In Online Album Reviews|work=Plugged In|publisher=[[Focus on the Family]]|accessdate=February 12, 2011}}</ref>

==Reception==
[[File:Pharrell performing September 2008.jpg|thumb|150px|right|Critics were divided concerning the production of Pharrell Williams (pictured) and Chad Hugo on "Boys."]]

===Critical reception===
Calling the song "a decidedly lubricious duet", Alex Needham of ''[[New Musical Express]]'' said that "She sings about boys, he sings about girls. A simple concept, but an effective one, resulting in Britney ’s best single [in] ages."<ref name="Needham">{{cite web|url=http://www.nme.com/reviews/britney-spears/6574|title=Spears, Britney :  Boys|work=[[New Musical Express]]|first=Alex|last=Needham|accessdate=February 11, 2011|date=July 22, 2002}}</ref> David Browne of ''[[Entertainment Weekly]]'' said that [[The Neptunes]]' productions on the album, "[[I'm a Slave 4 U]]" and "Boys" "swaddle her in writhing, kick-the-can beats, but never have a groove and a verse been so betrayed by a limp chorus."<ref name="Browne">{{cite web|url=http://www.ew.com/ew/article/0,,183907~4~~britney,00.html|title=Britney (2001)|work=[[Entertainment Weekly]]|first=David|last=Browne|accessdate=February 11, 2011|date=November 12, 2001}}</ref> Nicki Tranter of [[PopMatters]] said that the remainder of the album was "tried and tested" dance-pop, noting "Boys" and "Cinderella" revisiting "old Britney territory exploring predictable issues including her love being irreplaceable, her use of the dance floor as an appropriate courting place, and her much-loved girl-ness."<ref name="Tranter">{{cite web|url=http://www.popmatters.com/music/reviews/s/spearsbritney-britney.shtml|title=Britney Spears: Britney|work=[[PopMatters]]|first=Nicki|last=Tranter|accessdate=February 11, 2011|date=November 6, 2001}}</ref> Leah Greenblatt of ''[[Entertainment Weekly]]'' called it a "popping, [[Janet Jackson|Janet]]-esque groove."<ref>{{citation|title=Britney Spears - Britney: We Rank 17 of Her Greatest Hits! - Photo 1 of 18 - EW.com|url=http://www.ew.com/ew/gallery/0,,20322205_20706624,00.html|publisher=Entertainment Weekly|work=Greenblatt, Leah|date=2009-11-26|accessdate=2012-03-09}}</ref> ''[[Yale Daily News]]'' writer Catherine Halaby considered the song "an envelope-pushing (when you consider her claims of wholesomeness) smutfest."<ref name=yalereview>{{cite news| url=http://www.yaledailynews.com/scene/music/2001/11/09/for-spears-maturity-is-like-cool/|title=For Spears, maturity is, like, cool|last=Halaby|first=Catherine|date=November 9, 2001|work=[[Yale Daily News]]|publisher=The Yale Daily News Publishing Company|accessdate=January 6, 2010}}</ref>

===Chart performance===
The song failed to make much of an impact in the United States, only appearing on the ''[[Billboard (magazine)|Billboard]]'' [[Bubbling Under Hot 100]] chart, where it appeared at number 22. "Boys" is so far Britney Spears least successful single in the U.S.<ref name="US">{{Cite book|last = Whitburn|first = Joel|authorlink = Joel Whitburn|title = Bubbling Under the Billboard Hot 100: 1959–2004: Joel Whitburn Presents|publisher = Record Research|year = 2005|isbn = 978-0-89820-162-8|ref = harv}}</ref> It appeared on the ''Billboard'' [[Pop Songs]] chart at number 32.<ref name="sc_Billboardpopsongs_Britney Spears"/> In Canada, on the [[Canadian Singles Chart]], the song performed better, reaching number 21.<ref name="sc_Billboardcanadianhot100_Britney Spears"/> "Boys" fared better internationally, appearing in the top 20 in several markets in which it charted. On the [[UK Singles Chart]], the song peaked at number seven.<ref name="UK"/> It also peaked in the top ten of the [[Irish Singles Chart]] and both the Belgian Flanders and Wallonia [[Ultratop]] charts.<ref name="sc_Wallonia_Britney Spears feat. Pharrell Williams"/> On the [[ARIA Singles Chart]] in Australia, the song spent 12 weeks on the chart, peaking at number 14.<ref name="sc_Wallonia_Britney Spears feat. Pharrell Williams"/> "Boys" would go on to be certified Gold in Australia by the [[Australian Recording Industry Association]].<ref name="ARIA">{{cite web|url=http://aria.com.au/pages/aria-charts-accreditations-singles-2002.htm|title=ARIA Charts&nbsp;– Accreditations&nbsp;– 2002 Singles|publisher=[[Australian Recording Industry Association]]|accessdate=February 12, 2011}}</ref> Furthermore, it peaked in the top 20 of the charts in Germany, Austria, Switzerland, Sweden, Finland, and Denmark.<ref name="sc_Austria_Britney Spears feat. Pharrell Williams"/>

==Music video==
The [[music video]] for "Boys" was directed by [[Dave Meyers (director)|Dave Meyers]].<ref>{{cite web|url=http://www.mtv.com/videos/britney-spears/53857/boys-co-ed-remix.jhtml#artist=501686|title=Boys Co-Ed Remix&nbsp;– Britney Spears&nbsp;– Music Video|publisher=[[MTV]] ([[Viacom]])|accessdate=February 12, 2011}}</ref> In the video, a man [[DJ Qualls]], tries to get into a castle which is hosting a party being attended by Spears. When the security guards refuse to grant him access, he begins to scream for Spears. As the music starts, Spears, readying herself for the party in one of the castle's towers, is dancing with a group of women. The scene shifts to a castle courtyard, where Spears and a man are sit at opposite ends of a long table. Following this, Spears walks around a pool, in which she sees a man ([[Justin Bruening]]) swimming. As they begin to interact with each other, [[Pharrell Williams|Pharrell]] is at the bar with a woman. Spears walks over to him and they begin to talk. Following this, Spears dances with a group of people, including [[Mike Myers (actor)|Mike Myers]] as [[Austin Powers (character)|Austin Powers]]. Actors [[Jason Priestley]], [[Justin Bruening]] and [[Taye Diggs]] also make cameo appearances in the video. Its choreography was favorably likened to [[Janet Jackson]].<ref>{{citation|title=Ranking all of Britney Spears' music videos - PopWrap |url=http://www.nypost.com/p/blogs/popwrap/britney_spears_music_video_report_4rrLUSeEJQRZusLm464f1K |publisher=[[New York Post]] |author=Wieselman, Jarett |date=2011-02-15 |accessdate=2012-03-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20130706220031/http://www.nypost.com/p/blogs/popwrap/britney_spears_music_video_report_4rrLUSeEJQRZusLm464f1K |archivedate=July 6, 2013 }}</ref> The video was nominated for at the [[2003 MTV Video Music Awards]] for Best Video from a Film, but lost to [[Eminem]]'s "[[Lose Yourself]]."<ref>{{cite web|url=http://www.mtv.com/bands/v/news_feature_vma_noms03/vma03frame.html |title=2003 MTV Video Music Awards Winners List |publisher=MTV (Viacom) |accessdate=February 11, 2011 |deadurl=yes |archiveurl=http://www.webcitation.org/66Pb8Xdtq?url=http%3A%2F%2Fwww.mtv.com%2Fbands%2Fv%2Fnews_feature_vma_noms03%2Fvma03frame.html |archivedate=March 24, 2012 |df= }}</ref>

==Live performances==
[[File:BritneyPOM9.jpg|thumb|left|Spears performing "Boys" on [[Britney: Piece of Me]] show in 2014.]]
Spears performed "Boys" on multiple occasions. She performed the song on February 10,&nbsp;2002 at the [[2002 NBA All-Star Game]].<ref>{{cite web|url=http://www.nba.com/allstar2002/|title=All-Star 2002|work=''[[NBA|NBA.com]]''|accessdate=February 11, 2011}}</ref> She also performed it on the [[Saturday Night Live (season 27)|twenty-seventh]] season of ''[[Saturday Night Live]]'' on February 2,&nbsp;2002.<ref>{{cite web|url=http://movies.msn.com/movies/movie-synopsis/saturday-night-live-britney-spears-2/|title=Saturday Night Live: Britney Spears [2]: Synopsis|work=''[[MSN|MSN Movies]]''|publisher=MSN|accessdate=January 12, 2011}}</ref> She performed the song as a mash-up with "[[I'm a Slave 4 U]]" on her [[American Broadcasting Company|ABC]] special, ''In the Zone'', to promote her fourth album  ''[[In the Zone (album)|In the Zone]]''.<ref>{{cite web|url=http://www.amazon.com/dp/B0001BXYUG|title=In the Zone (+ Bonus CD) (2003)|work=''[[Amazon.com]]''|publisher=Amazon, Inc|accessdate=February 11, 2011}}</ref> In the United Kingdom, Spears performed the song on ''[[CD:UK]]''. On the 2001 [[Dream Within a Dream Tour]], Spears performed it wearing a tank-top and suspenders.<ref name=mtvreview>{{cite news|url=http://www.mtv.com/news/articles/1450487/20011102/spears_britney.jhtml |title=Britney Goes Bald, Plays Tiny Dancer, Gets Caught In The Rain At Tour Kickoff |last=Reid |first=Shaheem |last2=Moss |first2=Corey |date=November 2, 2001 |work=[[MTV]] |publisher=[[MTV Networks]] |accessdate=February 11, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20080418101925/http://www.mtv.com/news/articles/1450487/20011102/spears_britney.jhtml |archivedate=April 18, 2008 |df= }}</ref> In the tour's extension in 2002, she replaced the original version with the remix on the tour.<ref name="mtvreview"/>

On [[The Onyx Hotel Tour]] in 2004, it featured male dancers pushing her while she was standing in [[baggage cart|luggage carts]].<ref name="seattlereview">{{cite news|url=http://www.seattlepi.com/pop/164745_britneyq.html|title=Britney ready for Vegas in a show rated 'R' for racy|date=March 13, 2004|accessdate=February 12, 2011|last=Stout|first=Gene|work=[[Seattle Post-Intelligencer]]|publisher=[[Hearst Corporation]]}}</ref> During her performance of the song on [[The Circus Starring Britney Spears]], the singer performed wearing a military costume, while surrounded by her dancers, some of them riding bicycles.<ref name=usat>{{cite news|url=http://www.usatoday.com/life/music/news/2009-03-04-britney-concert_N.htm|title=Britney in concert: The Circus is finally back in town|date=March 4, 2009|accessdate=February 12, 2011|last=Shriver|first=Jerry|first2=|last2=|work=[[USA Today]]|publisher=[[Gannett Company]], Inc.}}</ref> The rendition was described as a "[[Rhythm Nation]]-like military stomp."<ref>{{citation|title=Britney Does Boston: The Review|url=http://www.muumuse.com/2009/03/britney-does-boston-review.html/|work=[[MuuMuse]]|author=Stern, Bradley|date=2009-03-18|accessdate=2012-03-09}}</ref> On 2011's [[Femme Fatale Tour]], Spears wears a golden cape for a [[snake charming]] number of the song.<ref name=sacbee>{{cite web|url=http://blogs.sacbee.com/ticket/archives/2011/06/spears-finds-gr.html|title=Spears finds groove, Minaj mesmerizes in Sacramento concert|work=[[The Sacramento Bee]]|publisher=[[The McClatchy Company]]|last=Meyer|first=Carla|date=June 17, 2011|accessdate=June 17, 2011}}</ref> Spears included "Boys" on the setlist of her 2013-15 Las Vegas residency, [[Britney: Piece of Me]]. During this performance, she and her dancers wear neon outfits. Sophie Schillaci from [[MTV]] considered the performance "crowd-pleasing".<ref>{{cite web|url=http://www.mtv.com/news/1719668/britney-spears-las-vegas-piece-of-me-opening-night/|title=Britney Spears Pours On ‘Cheese’ (And Glitter) For ‘Piece Of Me’ Las Vegas Kickoff |publisher=MTV. MTV Networks|last=Schillaci|first=Sophie|date=December 28, 2013|accessdate=May 10, 2014}}</ref>

==Usage in media==
The song has been performed in a mash up with [[Justin Bieber]]'s "[[Boyfriend (Justin Bieber song)|Boyfriend]]" by [[Kevin McHale (actor)|Kevin McHale]] and [[Darren Criss]] (respectively as their characters [[Artie Abrams]] and [[Blaine Anderson]]) in the musical TV series ''[[Glee (TV series)|Glee]]'' in the second episode of [[Glee (season 4)|season four]], "[[Britney 2.0]]" (aired on September 20, 2012), which is also the show's second tribute episode to [[Britney Spears]].<ref name="HR905">{{cite web|url=http://www.hollywoodreporter.com/live-feed/glee-season-4-music-justin-bieber-lady-gaga-britney-spears-368244 |title={{-'}}Glee' Season 4 to Feature Justin Bieber, Lady Gaga, Britney Spears Songs |first=Lesley|last=Goldberg|work=[[The Hollywood Reporter]]|date=September 5, 2012|accessdate=September 5, 2012}}</ref>

==Track listings==
{{col-begin}}
{{col-2}}
*;12" vinyl
#"Boys" (Co-Ed Remix)&nbsp;– 3:45
#"Boys" (Co-Ed Remix) [Instrumental]&nbsp;– 3:45
#"[[I'm a Slave 4 U]]"&nbsp;– 3:23

*;CD single
# "Boys" (Co-Ed Remix)&nbsp;— 3:45
# "Boys" (Album Version)&nbsp;— 3:26
{{col-2}}
*;CD maxi single
# "Boys" (Co-Ed Remix)&nbsp;— 3:45
# "Boys" (Instrumental) [Co-Ed Remix]&nbsp;— 3:45
# "Boys" (Album Version)&nbsp;— 3:26
# "Boys" (Instrumental) [Album Version]&nbsp;— 3:26
{{col-end}}

==Charts and certifications==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable sortable"
!Chart (2002)
!Peak<br />position
|-
{{singlechart|Australia|14|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Austria|18|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Flanders|7|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Wallonia|10|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Billboardcanadianhot100|21|artist=Britney Spears|artistid={{BillboardID|Britney Spears}}|accessdate=February 11, 2011}}
|-
{{singlechart|Denmark|14|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|France|55|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Germany|19|artist=Spears, Britney|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Ireland|10|year=2002|week=31|accessdate=February 11, 2011}}
|-
| Japan ([[Oricon]])<ref name="japanoricon">{{cite web|url=http://www.oricon.co.jp/prof/artist/145326/ranking/cd_single/|title=ブリトニー・スピアーズのCDシングルランキング、ブリトニー・ORICON STYLE|publisher=[[Oricon]]|language=Japanese|accessdate=October 4, 2011}}</ref>
| align="center"|63
|-
{{singlechart|Dutch40|14|year=2002|week=38}}
|-
{{singlechart|New Zealand|39|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Scotland|7|artist=Britney Spears|song=Boys|date=August 4, 2002|accessdate=June 22, 2015}}
|-
{{singlechart|Spain|16|artist=Britney Spears feat. Pharrell Williams|song=boys|accessdate=April 2, 2011}}
|-
{{singlechart|Sweden|11|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|Switzerland|20|artist=Britney Spears feat. Pharrell Williams|song=Boys|accessdate=February 11, 2011}}
|-
{{singlechart|UKsinglesbyname|7|artist=Britney Spears|song=Boys|accessdate=April 24, 2015|refname="UK"}}
|-
|align="left"|US ''Billboard'' [[Bubbling Under Hot 100 Singles]]<ref name="US"/>
|align="center"|22
|-
{{singlechart|Billboardpopsongs|32|artist=Britney Spears|accessdate=February 11, 2011}}
|}
{{col-2}}

===Year-end charts===
{|class="wikitable sortable"
|-
! style="text-align:center;"|Chart (2002)
! style="text-align:center;"|Peak<br />position
|-
|Belgian Singles Chart (Flanders)<ref>{{cite web|url=http://www.ultratop.be/nl/annual.asp?year=2002|title=Jaaroverzichten 2002|publisher=[[Ultratop 50]]. Hung Medien|accessdate=May 4, 2011|language=Dutch}}</ref>
|align="center"|73
|-
|Belgian Singles Chart (Wallonia)<ref>{{cite web|url=http://www.ultratop.be/fr/annual.asp?year=2002|title=Rapports annuels 2002|publisher=[[Ultratop 50]]. Hung Medien|accessdate=May 4, 2011|language=French}}</ref>
|align="center"|76
|-
|UK Singles Chart<ref>{{cite web|url=http://www.ukchartsplus.co.uk/ChartsPlusYE2002.pdf|title=UK Year-End Chart 2002|publisher=The Official Charts Company|accessdate=April 11, 2011}}</ref>
|align="center"|141
|}

===Certifications===
{{Certification Table Top}}
{{Certification Table Entry|type=single|region=Australia|artist=Britney Spears|title=Boys|award=Gold|certyear=2002|relyear=2002|recent=false|certref=<ref name="ARIA"/>}}
{{Certification Table Bottom|format = 3col|nosales=true|nounspecified=true}}
{{col-end}}

==Release history==
{| class="wikitable plainrowheaders"
|-
! scope="col"| Country
! scope="col"| Date
! scope="col"| Format
! scope="col"| Label
|-
! scope="row"| United Kingdom<ref>{{cite web|title=Britney Spears: Boys: Single, Maxi|url=http://www.amazon.co.uk/Boys-Britney-Spears/dp/B000069JAY|publisher=Amazon.co.uk|accessdate=April 24, 2015|location=United Kingdom}}</ref>
| July 29, 2002
| rowspan="3"| [[CD single]]
| [[RCA Records|RCA]]
|-
! scope="row"| Japan<ref>{{cite web|title=Britney Spears: Boys: Single, Import|url=http://www.amazon.co.jp/Boys-Britney-Spears/dp/B0000DESM9|accessdate=April 24, 2015|location=Japan|publisher=Amazon.co.jp}}</ref>
| September 3, 2002
| rowspan="2"| [[Sony Music Entertainment|Sony]]
|-
! scope="row"| Germany<ref>{{cite web|title=Britney Spears: Boys: Single, Maxi|url=http://www.amazon.de/Boys-Britney-Spears/dp/B00006H1EE|publisher=Amazon.de|accessdate=April 24, 2015|location=Germany}}</ref>
| September 30, 2002
|-
|}

==References==
{{reflist|30em}}

==External links==
* {{MetroLyrics song|britney-spears|boys}}<!-- Licensed lyrics provider -->

{{Britney Spears singles}}
{{Pharrell Williams singles}}

{{Good article}}

[[Category:2001 songs]]
[[Category:2002 singles]]
[[Category:Britney Spears songs]]
[[Category:Jive Records singles]]
[[Category:Music videos directed by Dave Meyers (director)]]
[[Category:Pharrell Williams songs]]
[[Category:Song recordings produced by The Neptunes]]
[[Category:Songs written for films]]
[[Category:Songs written by Chad Hugo]]
[[Category:Songs written by Pharrell Williams]]
[[Category:Songs about sexuality]]