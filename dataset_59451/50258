{{Infobox noble house
|surname          = House of Al Nahyan
|native_name      = 
|native_name_lang = Arabic
|other_name       = 
|coat of arms     = [[File:Emblem_of_Abu_Dhabi.svg|200px]]
|image_size       = 
|alt              = 
|caption          = Emblem of Abu Dhabi
|type             = <!-- Royal house, noble house, etc. -->
|country          = United Arab Emirates
|estates          = 
|parent house     = 
|titles           = [[Emir of Abu Dhabi]] <br>
[[Crown Prince of Abu Dhabi]]                  
|styles           = ''His/Her Highness''
|founded          = 1761
|founder          = [[Al Nahyan]]
|final ruler      = 
|current head     = 
|dissolution      = <!-- {{End date|YYYY}} -->
|deposition       = 
|ethnicity        = Emirati
|cadet branches   = 
|notes            = 
}}
{{redirect|Al Nahyan|other uses}}
{{refimprove|date=October 2011}}
'''Al Nahyan''' ({{lang-ar|آل نهيان}} ''Āl Nohayan''/ {{lang-en|The house of Nahyan}}) are one of  [[Royal families of the United Arab Emirates|the six ruling families of the United Arab Emirates]], and are based in the capital [[Abu Dhabi]], [[United Arab Emirates]]. Al Nahyan is a branch of the House of Al-Falahi (Āl Bū Falāḥ), a branch of the [[Bani Yas]] tribe, and are related to the [[House of Al-Falasi]] and also related to [[House of Al-Yassi]], these two powerful houses/tribes are from which the ruling the family of [[Dubai]], [[Al Maktoum]], descends. Al Nahyans came to Abu Dhabi in the 18th century and hail from [[Liwa Oasis]].<ref name=ono2011mar>{{cite journal|last=Motohiro|first=Ono|title=Reconsideration of the Meanings of the Tribal Ties in the United Arab Emirates: Abu Dhabi Emirate in Early ʼ90s|journal=Kyoto Bulletin of Islamic Area Studies|date=March 2011|volume=4-1|issue=2|pages=25–34|url=http://www.asafas.kyoto-u.ac.jp/kias/kyodo/pdf/kb4_1and2/05ono.pdf|accessdate=17 April 2013}}</ref> They have been the rulers of Abu Dhabi since 1793, but have been ruling their tribe in Liwa Oasis hundreds of years back. Five of the rulers killed one another who ruled from late 1800s to early 1900s; they were brothers.<ref name=onley2006sep>{{cite journal|last=Onley|first=James|author2=Sulayman Khalaf|title=Shaikhly Authority in the Pre-oil Gulf: An Historical–Anthropological Study|journal=History and Anthropology|date=September 2006|volume=17|issue=3|pages=189–208|url=http://socialsciences.exeter.ac.uk/iais/downloads/Onley_Khalaf_Shaikhly_Authority-2006.pdf|accessdate=17 April 2013|doi=10.1080/02757200600813965}}</ref>

==Members==
It is still very common not to mention the female members of the family in most of the [[Arab States of the Persian Gulf|monarchies in the Persian Gulf]]. A family tree of the Al Nahyan, designed by French historian Béatrice Lachaume in 1996 (to commemorate his Silver Jubilee), does for instance not show a single female member of the family. It is still common to withhold the first names of brides in official wedding ceremonies when they involve royalty. Current male members of the family are under 200 while the number of females is unknown.
Notable members of the Al Nahyan family include:
* Sheikh [[Dhiyab bin Isa Al Nahyan]], ruler of Abu Dhabi from 1761 to 1793
** Sheikh [[Shakhbut bin Dhiyab Al Nahyan]], ruler of Abu Dhabi from 1793 to 1816
*** Sheikh [[Muhammad bin Shakhbut Al Nahyan]], ruler of Abu Dhabi from 1816 to 1818
*** Sheikh [[Tahnoun bin Shakhbut Al Nahyan]], ruler of Abu Dhabi from 1818 to 1833
**** Sheikh [[Saeed bin Tahnoun Al Nahayan]], ruler of Abu Dhabi
*** Sheikh [[Sultan bin Shakhbut Al Nahyan]]
*** Sheikh [[Khalifa bin Shakhbut Al Nahayan]], ruler of Abu Dhabi from 1833 to 1845
**** Sheikh [[Zayed bin Khalifa Al Nahyan]], Zayed grandfather, ruler of Abu Dhabi from 1855-1909
***** Sheikh [[Khalifa bin Zayed bin Khalifa Al Nahyan|Khalifa bin Zayed Al Nahyan]]
****** Sheikh Mohammad bin Khalifa bin Zayed, only son. 
***** Sheikh [[Tahnoun bin Zayed bin Khalifa Al Nahyan|Tahnun bin Zayed Al Nahyan]], ruler of Abu Dhabi from 1909 to 1912
***** Sheikh [[Said bin Zayed bin Khalifa Al Nahayan|Said bin Zayed Al Nahayan]]
***** Sheikh [[Saqr bin Zayed Al Nahyan]], ruler of Abu Dhabi from 1926-1928
***** Sheikh Mohammad bin Zayed bin saqr Al Nahyan
***** Sheikh Khalid bin Zayed bin Saqr Al Nahyan (bin Zayed Group)
***** Sheikh Ahmed bin mohammad bin Zayed bin Saqr Al Nahyan (Reef properties , Reef group)
***** Sheikh Saqr bin mohammad bin Zayed bin Saqr Al Nahyan
***** Sheikha Fatima bin Zayed bin Saqr Al Nahyan (wife of Sheikh Humaid bin Rashid Al Nuaimi, UAE Supreme Council member and ruler of Ajman)
***** Sheikh [[Hamdan bin Zayed bin Khalifa Al Nahyan|Hamdan bin Zayed Al Nahyan]], ruler of Abu Dhabi 1912-1922
***** Sheikh [[Sultan bin Zayed bin Khalifa Al Nahyan|Sultan bin Zayed Al Nahyan]], Ruler of Abu Dhabi 1922-1926.
****** Sheikh [[Shakhbut bin Sultan Al Nahyan]] (1905-1989), Ruler of Abu Dhabi 1928-1966
******* Sheikh Saeed bin Shakhbut bin Sultan Al Nahyan
******* Sheikh Sultan bin Shakhbut bin Sultan Al Nahyan 
******* Sheikha Ousha bint Shakhbut bin Sultan Al Nahyan
******* Sheikha Moza bint Shakhbut bin Sultan Al Nahyan
******* Sheikha Qout bint Shakhbut bin Sultan Al Nahyan
******* Sheikha Roudha bint Shakhbut bin Sultan Al Nahyan
****** Sheikh Khalid bin Sultan Al Nahyan
****** Sheikha Maryam bint Sultan Al Nahyan
****** Sheikh [[Zayed bin Sultan Al Nahyan]] (1918-2004), Ruler of Abu Dhabi 1966-2004, founder of the United Arab Emirates
******* Sheikh [[Khalifa bin Zayed Al Nahyan]] (*1948), present President of the United Arab Emirates.
******** Sheikh [[Sultan bin Khalifa Al Nahyan]] (*1965), longtime chief of the Crown Prince's Court, and owner of the SBK Holding. Married to Sheikha Shaikha bint Saif bin Mohammed Al Nahyan.
********* Sheikh Zayed bin Sultan bin Khalifa Al Nahyan (age 28) married daughter of Sheikh Sultan bin Hamdan bin Mohammed Al Nahyan ********* Sheikh Mohammed bin Sultan bin Khalifa Al Nahyan
******** Sheikh [[Mohammed bin Khalifa bin Zayed Al Nahyan]] (*1972), member of the Abu Dhabi Executive Council 
******** Sheikha Osha bint Khalifa bin Zayed 
******* Sheikh [[Sultan bin Zayed bin Sultan Al Nahyan|Sultan bin Zayed Al Nahyan]] (*1953), longtime Deputy Prime Minister
******** Sheikh [[Khalid bin Sultan bin Zayed Al Nahyan]]
******* Sheikh [[Mohammed bin Zayed Al Nahyan]] (*1961), Crown Prince of Abu Dhabi and Deputy Supreme Commander of the UAE Armed Forces, married to Sheikha Salama bint Hamdan Al Nahyan
******** Sheikh [[Khalid bin Mohammed bin Zayed Al Nahyan]], married in 2008 the daughter of Sheikh Suroor bin Mohammed Al Nahyan at a lavish wedding at Emirates Palace
******** Sheikha [[Shamma bint Mohammed bin Zayed bin Sultan Al Nahyan]] (*1992) 
******** Sheikh [[Zayed bin Mohammed bin Zayed Al Nahyan]] 
******** Sheikh [[Dhiab bin Mohammed bin Zayed Al Nahyan]]
******** Sheikha [[Hessa bint Mohammed bin Zayed Al Nahyan]] (*2001)
******** Sheikh [[Hamdan bin Mohammed bin Zayed Al Nahyan]] 
******** Sheikha [[Maryam bint Mohammed bin Zayed Al Nahyan]]
******* Sheikh [[Hamdan bin Zayed bin Sultan Al Nahyan]] (*1963), Deputy Prime Minister, married to Sheikha Shamsa bint Hamdan Al Nahyan
******** Sheikh [[Sultan bin Hamdan bin Zayed]], who married in 2005 a daughter of Sheikh Khalifa bin Zayed, and Advisor to the President
******** One daughter, who married in 2013 Sheikh Dhiab bin Mohammed bin Zayed.
******* Sheikha Moizza bint Sultan bin Zayed Al Nahyan
******* Sheikh Waseem bin Sultan bin Zayed Al Nahyan 
******* Sheikh Mateen bin Moizza bin Sultan bin Zayed Al Nahyan (*1994) 
******* Sheikh [[Hazza bin Zayed bin Sultan Al Nahyan]] (*1965), is a businessman and holds a PhD in Economics.
******* Sheikh [[Tahnoon bin Zayed bin Sultan Al Nahyan]], prominent businessman, married to Sheikha Khawla Al Suwaidi
******* Sheikh [[Mansour bin Zayed Al Nahyan]] (*1970), Minister of Presidential Affairs, married to Sheikha Manal bint Mohammed bin Rashid Al Maktoum
******** Sheikh Zayed bin Mansour bin Zayed Al Nahyan
******** Sheikha Fatima bint Mansour bin Zayed Al Nahyan (*2006)
******** Sheikh Mohammed bin Mansour bin Zayed Al Nahyan (*2007)
******* Sheikh [[Abdullah bin Zayed Al Nahyan]] (*1972), Minister of Foreign Affairs
******* Sheikh [[Saif bin Zayed Al Nahyan]] (*1968), Minister of Interior
******* Sheikh [[Ahmed bin Zayed Al Nahyan]] (1968-2010), who died in ultralight aircraft crash<ref>{{cite web|url=http://news.bbc.co.uk/1/hi/world/middle_east/8595249.stm|title=Abu Dhabi sheikh's body in lake|date=30 March 2010|publisher=|via=bbc.co.uk}}</ref>
******* Sheikh [[Hamed bin Zayed Al Nahyan]] (*1971) 
******* Sheikh [[Omar bin Zayed Al Nahyan]] 
******* Sheikh [[Khalid bin Zayed Al Nahyan]] 
******* Sheikh [[Saeed bin Zayed Al Nahyan]] (*1966)
******* Sheikh [[Nahyan bin Zayed Al Nahyan]]
******* Sheikh [[Falah bin Zayed Al Nahyan]] (*1970)
******* Sheikh [[Diab bin Zayed Al Nahyan]] (*1966)
******* Sheikh [[Issa bin Zayed Al Nahyan]], real estate developer 
******* Sheikh [[Nasser bin Zayed Al Nahyan]] (1967-2008), died in a helicopter crash
******* Sheikha [[Latifa bint Zayed bin Sultan Al Nahyan]] 
******* Sheikha [[Maitha bint Zayed bin Sultan Al Nahyan]]
******* Sheikha [[Shaikha bint Zayed bin Sultan Al Nahyan]] 
******* Sheikha [[Shamma bint Zayed bin Sultan Al Nahyan]]
******* Sheikha [[Elyazieh bint Zayed bin Sultan Al Nahyan]]
******* Sheikha [[Afra'a bint Zayed bin Sultan Al Nahyan]]

; Offshoot
* Sheikh [[Nahyan bin Mubarak Al Nahyan]] — Former Minister of Higher Education
* Sheikh [[Hamad bin Hamdan Al Nahyan]] — Member of the Ruling Family
* Sheikh Dr [[Ahmed bin Saif Al Nahyan]] — Member of the Ruling Family
* Sheikh Dr [[Mansoor bin Tahnoon Al Nahayan]] — A medical doctor who holds a medical degree and a PhD in international relation and security

==See also==
{{Portal|United Arab Emirates}}
* [[List of Sunni Muslim dynasties]]
* [[List of rulers of separate Emirates of the United Arab Emirates]]

== References ==
{{reflist}}

{{Abu Dhabi}}

[[Category:Muslim families]]
[[Category:Emirati families]]
[[Category:House of Al Nahyan| ]]
[[Category:Middle Eastern royal families]]
[[Category:History of Abu Dhabi (emirate)]]
[[Category:Tribes of the United Arab Emirates]]