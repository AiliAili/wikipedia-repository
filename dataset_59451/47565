{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox person
| name = David Domoney
| image = David_Domoney_Profile.jpg
| caption = David Domoney
| birth_name = David Martin Domoney
| birth_date = {{Birth date and age|df=yes|1963|3|26}}
| birth_place = [[Devizes]], Wiltshire, England
| nationality = British
| occupation = Horticulturalist, Broadcaster, [[writer]], [[gardener]]
| salary =
| television = ''[[This_Morning_(TV_programme)]]'' <small>(2011–2012)</small><br>''[[Love Your Garden]]'' <small>(2012—)</small>
| partner = Adele Holdsworth
| children =Alice & Abigail
| website ={{url|daviddomoney.com}}
}}
'''David Martin Domoney''', C Hort. FI Hort (born 26 March 1963)<sup>[[David Domoney#cite note-1|[1]]]</sup> is an English [[celebrity gardener]] and broadcaster. He demystifies gardening.[[David Domoney#cite note-2|<sup>[2]</sup>]] He is a Chartered Fellow of The Chartered Institute of Horticulture

David has co-presented [[Love Your Garden|''Love Your Garden'']] on ITV since 2012. He is the resident gardening expert on ITV This Morning.[[David Domoney#cite note-3|<sup>[3]</sup>]] Domoney founded Cultivation Street, the national campaign to promote Community Gardening & School Gardens now in its 5<sup>th</sup> year. Domoney founded Young Gardeners of the Year competition (in association with the Princes Foundation) now in its 7<sup>th</sup> year showcasing new British talent in garden design & construction. David also writes weekly for the national Sunday British newspaper, the Sunday Mirror and David writes every issue in the Grow your own magazine[[David Domoney#cite note-5|<sup>[5]</sup>]][[David Domoney#cite note-6|<sup>[6]</sup>]] 

==Early career==
David Domoney was born in [[Devizes]], Wiltshire, the son of Raymond, a British Telecoms senior executive. Jean Domoney, David's mother, born in [[Rothesay, Bute|Rothesay]] on the [[Isle of Bute]], [[Scotland]], had three children and a career in sales. After leaving [[Moseley School]], Birmingham, in 1979 aged 16, he studied at [[Warwickshire]] College of Agriculture, gaining a Pre Cert. Horticulture qualification.[[David Domoney#cite note-7|<sup>[7]</sup>]]

In 1980, he enrolled in one of the last of the industry's old style comprehensive three-year apprenticeships with Notcutts Nursery & Garden Centre Group (which at the time were the Harrods of Horticulture) during which he studied at [[Hadlow]] College of Horticulture. In 1983, David left Notcutts to study full-time at [[Pershore]] College of Horticulture, receiving an Advanced Certificate in Horticulture (ANCH).[[David Domoney#cite note-8|<sup>[8]</sup>]] He had a successful business career in horticulture with Notcutts predominantly as a plant advisor/manager on the front line answering visitors gardening questions. David joined the biggest UK Growing group Anglia Nursries as Sales & Marketing Manager, before joining Seasons Garden centre Group PLC as Horticulture Director.

Domoney then spent a decade heading up the garden buying teams for super store giants [[Texas Homecare]] & [[Do It All]].[[David Domoney#cite note-9|<sup>[9]</sup>]] David began his broadcasting career at [[Texas Homecare]]—presenting gardening features on the satellite TV station the company ran at its 20,000 staff.

In 1999, Domoney formed his own garden design company, he raised finance for his own garden designs at the [[Royal Horticultural Society]] (RHS) shows including the [[Chelsea Flower Show]], [[Hampton Court Palace]], BBC [[Gardener's World]] Live and the [[Tatton Park|Tatton Flower Show]]. To date, he has won 28 [[Royal Horticultural Society]] medals.[[David Domoney#cite note-10|<sup>[10]</sup>]] 

David has designed and built spectacular show gardens and events to promote some of the nation’s favourite brands. His clients have been John Lewis, Harley Davidson, the Ritz Piccadilly, Lexus, Croft Sherry, Laura Ashley, Manchester Children’s Hospital, British trust of Ornithology and Levive Diamonds, to name but a few.

Amongst Davids celebrity clients he has landscaped gardens for [[Phillip Schofield]], at his home in 2010, and England striker [[Michael Owen]] for his 'Hello' magazine wedding shoot.[[David Domoney#cite note-24|<sup>[24]</sup>]]

==Career==
===Television===
Domoney's passionate ''and enthusiastic'' approach shows that gardening should be about getting stuck in and learning as you go.[[David Domoney#cite note-22|<sup>[22]</sup>]]

He is an expert who can demystify gardening in a warm and inspiring way.
*''[[Love Your Garden]]'' (2012–present)
*''[[The Alan Titchmarsh Show]]'' (2011–2013)<ref>The Alan Titchmarsh Show</ref>
*''Garden ER'' (2011–2013)<ref>Garden ER</ref>
*''[[60 Minute Makeover]]'' (2009–2010)<ref>60 Minute Makeover</ref>
*''[[This Morning (TV programme)|This Morning]]'' (2006–2010)<ref>This Morning</ref>
*''[[Daybreak (2010 TV programme)|Daybreak]]'' (2006)<ref>Daybreak</ref>
*''Solution Street'' (2005)<ref>Solution Street</ref>
*''House of Horrors'' (2003–2004)<ref>House of Horrors</ref>
*''Better Homes'' (2002–2003)<ref>Better Homes</ref>
*''[[House Auction]]'' (2003)<ref>House Auction</ref>
*''Gardeners from Hell''<ref>Gardeners form Hell</ref>
*''Plant Doctor'' (2002)<ref>Plant Doctor</ref>

===Garden designs===
His approach is to attract people not normally interested in gardening by enticing them with innovative design and inspiring ideas. He created a Piranha-infested underwater garden with five tanks containing plant biotopes at the RHS [[Chelsea Flower Show]] in 2009.[[David Domoney#cite note-27|<sup>[27]</sup>]] His show shocked the nation and stopped the Queen in her tracks. It also won him a Gold Medal.[[David Domoney#cite note-28|<sup>[28]</sup>]] The following year he created the Ace of Diamonds garden – the most expensive show garden ever seen at the [[Chelsea Flower Show]] with £20m worth of diamonds.[[David Domoney#cite note-29|<sup>[29]</sup>]]  

For Harley Davidson he built a garden for a Hells Angel which even had a sculpture Ace of Spades made from Spades. He built the 2010 World cup football garden at BBC Gardeners World Live and he created a garden in 2015 with the biggest tree house ever seen at a Garden event for Quiet mark, John Lewis & Lexus at RHS Hampton Court Palace.

For 2017 at RHS Chelsea Flower Show he is designing a garden for The Commonwealth War Graves Commission as [part of their celebration on the Commissions centenary.

== Achievements ==
* Winner of 25 RHS Medals for Garden Design 1999 - 2015
* Winner of 2 RHS Medals for Floral Exhibits 2005- 2006
* Winner of RHS Medal for Science & Education Exhibit 2015
* Winner of 4 & 5 Star RHS trade stands 2015
* Winner Best Chelsea Flower Show City Garden RHS Trophy 2006
* Winner Most Creative Show Garden RHS Trophy 2008
* David has won medals in every RHS category
* Winner of the Frank Howell Trophy Most outstanding old student Warwickshire College Group
* Awarded HTA (Horticultural Trades Association) Honouree Membership for services to the Hort Industry 2015
* Awarded BALI (British Association of Landscape Industries Honouree Membership for services 2010
* Winner of a GIMA Trophy (garden industry manufacturers association) Best designed product 2009

=== Industry Achievements: ===
* Giving over 600 Horticultural students the opportunity to have the invaluable first hand experience to create a show garden at a national show before they leave college
* Motivating thousands of communities and schools into gardening through his Cultivation Street Campaign and promoting the benefits of gardening in social environments and as part of a healthy life
* Stretching the appeal of gardening to a wider audience with inspiring and exciting show exhibits & gardens

===Other projects===
David sits on the Royal Horticultural Society (RHS) Commercial Board. David is a Governor at the London Colleges of Horticulture at Capel Manor. David is also the face of community gardening with his national campaign ‘Cultivation Street’, now in its 5<sup>th</sup> year supporting hundreds of community gardening projects and school gardens up and down the country. David pioneered the ‘Young Gardeners of the Year’ annual competition at Olympia London, which is now in its 7<sup>th</sup> year. It is run through his association with HRH The Prince of Wales and The Prince’s Foundation for Building Community. 

== Charities ==
He is the Gardening Ambassador for THRIVE, one of the largest garden charities in Britain, which aims to enable positive change in the lives of disabled and disadvantaged people through the use of gardening and social horticultural therapy.

For over 15 years David has hosted the Charity auctions/lotto for Euro plants open days. 

Since childhood Domoney has collected for the Methodist charity, JMA, and continues to do so.

==Personal life==
David has 2 daughters with Adele. Alice-Rose Domoney born in August 2014 & Abigail Violet Domoney Born in March 2016 is a keen biker and has a V800 Vulcan motorbike. He is a master scuba diver with over 200 logged dives. His Specialist PADI Diving Qualifications include: Wreck Diving, Diver Propulsion Vehicles, Maldivian Shark Diver, Deep Diving and Dry Suit Diver.[[David Domoney#cite note-36|<sup>[36]</sup>]]

Domoney lives near [[Stratford-upon-Avon]] 

==References==
{{Reflist|2}}

==External links==
* {{official website|http://www.daviddomoney.com/}}
* [http://www.facebook.com/DavidDomoneyTV/ Facebook Page]
* [http://www.twitter.com/daviddomoney Twitter]
* [http://www.pinterest.com/daviddomoney Pinterest]
* [http://www.instagram.com/daviddomoney/ Instagram]
* [http://www.youtube.com/channel/UCGyJSi6OdIqoiE8qlDAwm_g YouTube]
* [http://www.facebook.com/CultivationStreet/ Cultivation Street Facebook]
* [http://www.twitter.com/CultivationSt Cultivation Street Twitter]
* [http://www.daviddomoney.com/cultivation-street/ Cultivation Street]

{{DEFAULTSORT:Domoney, David}}
[[Category:Articles created via the Article Wizard]]
[[Category:1963 births]]
[[Category:Living people]]
[[Category:English television presenters]]
[[Category:English gardeners]]
[[Category:People from Devizes]]