{{primary sources|date=April 2012}}
{{Infobox non-profit
| name = Boston Patent Law Association
| image = Boston Patent Law Association Logo.png
| type = Professional [[organization]]
| founded_date = 1924
| location = One Batterymarch Park, Suite 101 [[Quincy, Massachusetts|Quincy]], MA 02169-7454 
| coordinates = {{Coord|42.2301|-71.0257|type:landmark_region:US-MA|display=inline,title|format=dms}}
| key_people = President Donna M. Meuth <br>President-Elect Gregory J. Sieczkiewicz
| area_served = [[Boston]] and surrounding areas
| focus = [[Patent]], [[Trademarks]], [[Copyright]], [[Intellectual Property]] and [[Licensing]]
| num_members = c. 1,000
| homepage = {{URL|bpla.org}}
}}
Founded in [[Boston]] in 1924, the '''Boston Patent Law Association''' (BPLA) is an organization of intellectual property professionals that has a membership of over 1000 individuals.  Primarily located in the [[jurisdiction]] of the [[United States Court of Appeals for the First Circuit]],<ref>http://www.ca1.uscourts.gov/</ref> the BPLA’s members are a diverse population of [[Lawyer|attorneys]] and other professionals in the field of  [[intellectual property]] [[law]], encompassing [[trademark]], [[patent]], and [[copyright]] laws.

== Mission ==
The BPLA, through its volunteer Board of Governors and various committees, provides the IP community educational and social opportunities to exchange information relevant to current patent, trademark and copyright laws.  Additionally, the BPLA  offers commentary and analysis on legislation that affects the profession.<ref>http://www.bpla.org/displaycommon.cfm?an=1&subarticlenbr=4</ref>

== Officers and Board of Governors ==
The [[Board of Governors]] is a volunteer group of men and women consisting of well educated attorney and non-attorney intellectual property professionals who provide leadership to the BPLA. For 2015, the Board of Governors consists of the following.<ref>http://www.bpla.org/?page=5</ref>
{| class="wikitable"
|-
| '''Position''' || '''Name'''  || '''Company Currently Working For'''
|-
| President || Gregory J. Sieczkiewicz<ref>{{cite web|url=http://www.flagshipventures.com/team/greg-sieczkiewicz/ |title=Archived copy |accessdate=2012-04-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20120412171121/http://www.flagshipventures.com:80/team/greg-sieczkiewicz |archivedate=2012-04-12 |df= }}</ref> || Flagship Ventures <ref>http://www.flagshipventures.com/</ref>
|-
| President-Elect || Erik Paul Belt<ref>http://www.mccarter.com/Erik-Paul-Belt/</ref> || McCarter & English <ref>http://www.mccarter.com/</ref>
|-
| Vice President || Monica Grewal<ref>http://www.wilmerhale.com/monica_grewal/</ref> ||[[Wilmer Cutler Pickering Hale and Dorr LLP]]<ref>http://www.wilmerhale.com/</ref>
|-
| Treasurer || Rory P. Pheiffer<ref>http://www.nutter.com/Rory-P-Pheiffer/</ref> || [[Nutter McClennen & Fish LLP]]<ref>http://www.nutter.com/</ref>
|-
| Secretary || Deirdre E. Sanders<ref>{{cite web|url=http://www.hbsr.com/directory/deirdre-sanders |title=Archived copy |accessdate=2012-06-12 |deadurl=yes |archiveurl=https://web.archive.org/web/20120819041139/http://www.hbsr.com:80/directory/deirdre-sanders |archivedate=2012-08-19 |df= }}</ref> || Hamilton, Brook, Smith & Reynolds, P.C.<ref>http://www.hbsr.com/</ref>
|-
| Board Member and Past President || Donna M. Meuth || [[Eisai (company)|Eisai, Inc.]]<ref>http://www.eisai.com/</ref>
|-
| Board Member || Michael Bergman<ref>http://www.bergmansongllp.com/mbergman</ref> || Bergman and Song LLP <ref>http://www.bergmansongllp.com/</ref>
|-
| Board Member || Daniel W. Young<ref>http://www.wolfgreenfield.com/directory/daniel-young/</ref> || Wolf, Greenfield & Sacks, P.C.<ref>http://www.wolfgreenfield.com/</ref>
|-
| Board Member || Nikhil Patel <ref>http://www.burnslev.com/our-attorneys/nikhil-patel{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> || Burns & Levinson LLP<ref>{{cite web|url=http://www.burnslev.com/index.asp |title=Archived copy |accessdate=2015-01-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20141221060701/http://www.burnslev.com/index.asp |archivedate=2014-12-21 |df= }}</ref>
|}

== Past Presidents==
In the history of the BPLA, there have been 60 acting Presidents. Since 1950, Presidents have been chosen for a single 1 one-year term. The [[President-Elect]] automatically assumes the position of President of the BPLA at the end of the current President’s Term.<ref>http://www.bpla.org/?page=3</ref>
{| class="wikitable"
|-
|[[Frederick Perry Fish]] || 1924–1929 || | Frederick I. Emery || 1930–1932
|-
| Odin Roberts || 1933–1934  || | Nathan Heard || 1935–1941
|-
| George P. Dike || 1942–1946  ||  | Herbert W. Kenway || 1947–1949
|-
| Hector M. Holmes || 1950–1951  ||   | Melvin R. Jenney || 1952–1953
|-
| Merwin F. Ashley || 1954–1955   ||   | Cedric W. Porter || 1956–1957
|-
| Robert I. Thompson || 1958–1959   ||   | W. Bigelow Hall || 1960–1961
|-
| Herbert P. Kenway || 1962–1963   ||   | William R. Hulbert || 1964–1965
|-
| Elmer J. Gorn || 1966–1967   ||   | John D. Woodbury || 1967–1968
|-
| Oliver W. Hayes || 1968–1969   ||   | Joseph Weingarten || 1969–1970
|-
| Clarence S. Lyon || 1970–1971   ||   | Robert A. Cesari || 1971–1972
|-
| Robert A. Wise || 1972–1973   ||   | Charles Hieken || 1973–1974
|-
| Robert F. O'Connell || 1974–1975   ||   | David Wolf || 1975–1976
|-
| Willis M. Ertman || 1976–1977   ||   | Leo R. Reynolds || 1977–1978
|-
| W. Hugo Liepmann || 1978–1979   ||   | Martin J. O'Donnell || 1979–1980
|-
| William D. Roberson || 1980–1981   ||   | David G. Conlin || 1981–1982
|-
| Jerry Cohen || 1982–1983   ||   | Jacob N. Erlich || 1983–1984
|-
| Faith F. Driscoll || 1984–1985   ||  | Steven J. Henry || 1985–1986
|-
| Andrew T. Karnakis || 1986–1987   ||   | John H. Skenyon || 1987–1988
|-
| George W. Neuner  || 1988–1989   ||   | Edward J. Coleman || 1989–1990
|-
| Frank P. Porcelli || 1990–1991   ||   | Ronald J. Paglierani || 1991–1992
|-
| A. Jason Mirabito || 1992–1993   ||   | John L. DuPré || 1993–1994
|-
| Ronald L. Kransdorf || 1994–1995   ||   | Gregory D. Williams || 1995–1996
|-
| Susan G.L. Glovsky || 1996–1997   ||   | Timothy A. French || 1997–1998
|-
| Walter F. Dawson || 1998–1999   ||   | David J. Thibodeau, Jr. || 1999–2000
|-
| Thomas J. Engellenner || 2000–2001   ||   | William G. Gosz || 2001–2002
|-
| Peter C. Lando || 2002–2003   ||   | Peter F. Corless || 2003–2004
|-
| Doreen M. Hogle || 2004–2005   ||   | Ingrid A. Beattie || 2005–2006
|-
| Lee Carl Bromberg || 2006–2007   ||   | Leslie Meyer-Leon || 2007–2008
|-
| Mark B. Solomon || 2008–2009   ||   | Lisa Adams || 2009–2010
|-
| Grant Houston || 2010–2011   ||   | Neil Ferraro || 2011–2012
|- 
| Joseph M. Maraia|| 2012–2013   ||   | Donna Meuth || 2013-2014  
|}

== Members ==
There are four different types of members in the BPLA.<ref>http://www.bpla.org/?page=8</ref>
#  '''Attorney Members''': To qualify for Attorney Member status an individual must be an attorney in good standing whose practice focuses on intellectual property law, especially in the areas of patent, trademark and copyright law.
# '''Non-Attorney Members''': In order to obtain Non-Attorney Member status an individual must meet one of the following criteria:  (1) the individual must be registered with the [[United States Patent and Trademark Office]] (USPTO) and able to act for an applicant before the Patent Office, (2) the individual must be a current [[law school]] student enrolled at an accredited law school, (3) paralegals who are employed by firms or companies who are members of the BPLA and who provide intellectual property based services or (4) individuals who are interested in intellectual property law and who work for organizations that participate in patent, trademark, copyright or other areas of intellectual property law.
# '''Student Members''': (a) Persons who are not lawyers, but are regularly enrolled as candidates for a professional law degree in a law school approved by the Association of American Law Schools and would be otherwise qualified for membership, if a member of the Bar, (b) persons who are not lawyers, but are regularly enrolled as candidates for an associate, bachelor, or masters level degree at an accredited 2 or 4 year college or university, or (c) persons, who are not lawyers, but have graduated with a professional law degree from a law school approved by the Association of American Law Schools within two years of graduation and would be otherwise qualified for membership, if a member of the Bar, shall be eligible for Student membership. Student Members shall have all the privileges of the BPLA except those of voting and holding office. 
# '''Life Members''': Life Members can be either Attorney or Non-Attorney members but who have reached the age of 70 or above.  Life Members enjoy the same membership privileges they had before becoming Life Members.

== Annual events==
* '''BPLA Annual Judges Dinner''': Held Annually in honor of the Federal Judiciary by the BPLA, this event has a distinguished speaker and the annual BPLA Distinguished Public Service Award is given out to that year’s winner.
*'''BPLA Annual Writing Competition''': Currently enrolled law school students are invited to participate in the BPLA's Annual Writing Competition.  Each year students are asked to submit papers relating to relevant topics impacting intellectual property law.

== Committees==
The standing committees of the BPLA include the following:<ref>http://www.bpla.org/?page=A5</ref>
* Activities and Public Relations
* AIPLA Moot Court
* Amicus
* Antitrust
* Biotechnology
* Business & Marketplace
* Case Law
* Chemical Patent Practice
* Computer Law
* Contested Matters
* Copyright Law
* Corporate (In-House) Practice
* Ethics and Grievances
* International and Foreign Practice
* Invented Here!
* Licensing
* Litigation
* Medical Devices
* New Lawyers and Law Students
* Patent Agents and Technical Specialists
* Patent Law
* Patent Office Practice
* Pro Bono
* Trade Secrets Law
* Trademarks and Unfair Competition

==Amicus briefs submitted by BPLA==

Amicus briefs submitted by BPLA:<ref>http://www.bpla.org/?page=7</ref>

* Highmark Inc., v. AllCare Health Management Systems, Inc. - January 24, 2014 <ref>http://www.bpla.org/associations/10845/files/12_1163%20bsac%20Boston%20Patent%20Law-c.pdf</ref>
* Association for Molecular Pathology v. Myriad Genetics - March 14, 2013 <ref>http://www.bpla.org/associations/10845/files/12-398_resp_amcu_bpla.authcheckdam.pdf{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* [[Jenzabar|Jenzabar, Inc.]] v. Long Bow Group, Inc. - December 22, 2011<ref>{{cite web|url=http://bpla.org/associations/10845/files/Jenzabar%20v.%20Long%20Bow%20Group%20--%20BPLA%20Amicus%20Brief.pdf |title=Archived copy |accessdate=2012-03-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20130723040053/http://bpla.org/associations/10845/files/Jenzabar%20v.%20Long%20Bow%20Group%20--%20BPLA%20Amicus%20Brief.pdf |archivedate=2013-07-23 |df= }}</ref>
* [[Association for Molecular Pathology v. Myriad Genetics|Association of Molecular Pathology v. United States Patent and Trademark Office]] - October 29, 2010<ref>http://www.pubpat.org/assets/files/brca/CAFC/Boston%20Patent%20Law%20Assoc%20Amicus%20Brief.pdf</ref>
* Therasense v. Becton Dickinson - August 2, 2010<ref>http://www.wilmerhale.com/files/upload/BD_BPLA_amicus.pdf</ref>
* ACLU v. Myriad - January 13, 2010<ref>http://bpla.affiniscape.com/associations/10845/files/BPLA%20amicus%20brief%20in%20ACLU%20v.%20Myriad.pdf{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* [[In re Bilski]] - August 5, 2009<ref>http://www.bpla.org/associations/10845/files/in_re_bilski_20090805.pdf</ref>
* In re Engage - June 12, 2008<ref>http://www.bpla.org/associations/10845/files/in_re_engage_20080612.pdf</ref>
* [[In re Bilski]] - April 7, 2008<ref>http://www.bpla.org/associations/10845/files/in_re_bilski_20080407.pdf</ref>
* [[MedImmune, Inc. v. Genentech, Inc.|MedImmune v. Genentech]] - July 25, 2006<ref>http://www.bpla.org/associations/10845/files/medimmune_v._genentech_20060725.pdf</ref>
* [[2005 term per curiam opinions of the Supreme Court of the United States#Laboratory Corp. of America Holdings v. Metabolite Laboratories.2C Inc.|Laboratory Corp. of America v. Metabolite Labs., Inc.]] - February 6, 2006<ref>http://www.bpla.org/associations/10845/files/laboratory_corp._of_america_v._metabolite_labs.,_inc._20060206.pdf</ref>
* Merck & Co. v. Teva Pharma. USA, Inc. - September 22, 2005<ref>http://www.bpla.org/associations/10845/files/merck_&_co._v._teva_pharma._usa,_inc._20050922.pdf</ref>
* [[Phillips v. AWH Corp.]] - September 17, 2004<ref>http://www.bpla.org/associations/10845/files/phillips_v._awh_corp_20040917.pdf</ref>

== USPTO comments submitted by BPLA ==

United States Patent and Trademark Office comments submitted by BPLA:<ref>http://www.bpla.org/?page=42</ref>

=== April 24, 2014 ===

Comments on Changes to Require Identification of Attributable Owner, in response to requests for comments at 79 Fed. Reg. 4105<ref>http://www.bpla.org/resource/resmgr/USPTO_Comments/BPLA_Comments_Submission_424.pdf</ref>

=== January 30, 2012 ===

Comments in support of Boston as a location for a USPTO satellite office,<ref>http://bpla.org/associations/10845/files/PTO-C-2011-0066%20-%20Nationwide%20Workforce%20Program%20-%20Package%20to%20USPTO%20from%20Boston%20Patent%20Law%20Association.PDF{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> in response to request for comments at Federal Register Doc. 2011-30717<ref>http://www.gpo.gov/fdsys/pkg/FR-2011-11-29/html/2011-30717.htm</ref> (November 28, 2011)

=== March 8, 2010 ===

Comments on Enhancement in the Quality of Patents,<ref>http://bpla.affiniscape.com/associations/10845/files/CommentsonPatentQuality.pdf{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> in response to request for comment at 74 Federal Regulation 65093<ref>http://www.uspto.gov/web/offices/com/sol/notices/74fr65093.pdf</ref> (December 9, 2009), the period for comments having been extended to March 8, 2010 at 75 Fed. Reg. 5040<ref>http://www.uspto.gov/patents/law/notices/75fr5040.pdf</ref> (February 1, 2010)

=== February 26, 2010 ===

Comments on BPAI Rules of Practice in Ex Parte Appeals,<ref>http://bpla.affiniscape.com/associations/10845/files/CommentsOnExParteAppeals.pdf{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> in Response to Requests for Comment at 74 Fed. Reg. 67987<ref>http://www.gpo.gov/fdsys/pkg/FR-2009-12-22/pdf/E9-30402.pdf</ref> (Dec. 22, 2009)

=== November 25, 2009 ===

Comments on Patent Ombudsman Pilot Program,<ref>http://bpla.affiniscape.com/associations/10845/files/CommentsOnOmbudsman.PDF{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> in Response to Request for Comments at 74 Fed. Reg. 55212<ref>http://www.uspto.gov/patents/law/notices/74fr55212.pdf</ref> (Oct. 27, 2009)

== Recipients of the Boston Patent Law Association Distinguished Public Services Award ==
{| class="wikitable"
|-
| 2013 || [[Mark L. Wolf]]<ref>http://www.mad.uscourts.gov/boston/wolf.htm</ref>
District Judge

Chief Judge, 2006 - 2012

United States District Court for the District of Massachusetts

|-
| 2012 || [[Marianne B. Bowler]]<ref>http://www.mad.uscourts.gov/boston/bowler.htm</ref>
[[United States magistrate judge|Magistrate Judge]]

[[United States District Court of Massachusetts]]<ref>http://www.mad.uscourts.gov/</ref>
|-
| 2011 || [[Alan David Lourie]]<ref>http://www.cafc.uscourts.gov/judges/alan-d-lourie-circuit-judge.html</ref><ref>{{cite web|url=http://www.cafc.uscourts.gov/2011/judge-lourie-to-receive-the-boston-patent-law-association-distinguished-public-service-award-on-friday-june-10-2011.html |title=Archived copy |accessdate=2012-03-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20120915082459/http://www.cafc.uscourts.gov/2011/judge-lourie-to-receive-the-boston-patent-law-association-distinguished-public-service-award-on-friday-june-10-2011.html |archivedate=2012-09-15 |df= }}</ref>
[[United States federal judge|Circuit Judge]]

[[United States Court of Appeals for the Federal Circuit]]<ref>http://www.cafc.uscourts.gov/</ref>
|-
| 2010 || [[Rya W. Zobel]]<ref>http://www.mad.uscourts.gov/boston/zobel.htm</ref>
District Judge

United States District Court of Massachusetts
|-
| 2009 || [[Richard Gaylore Stearns]]<ref>http://www.mad.uscourts.gov/boston/stearns.htm</ref>
District Judge

United States District Court for the District of Massachusetts
|-
| 2008 || [[Patti B. Saris]]<ref>http://www.mad.uscourts.gov/boston/saris.htm</ref>
District Judge

United States District Court for the District of Massachusetts
|-
| 2007 || [[William G. Young]]<ref>http://www.mad.uscourts.gov/boston/young.htm</ref>
District Judge

United States District Court for the District of Massachusetts
|}

==See also==
* [[American Intellectual Property Law Association]]
* [[Bar Association]]
* [[Intellectual property organizations]]

==References==
{{citation style|section|date=April 2012}}
{{reflist}}

[[Category:Organizations based in Boston]]
[[Category:Intellectual property organizations]]