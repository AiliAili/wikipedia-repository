{{Infobox journal
| title = Fluid Dynamics Research
| cover = 
| editor = Yasuhide Fukumoto
| discipline = [[Fluid Mechanics]]
| abbreviation = Fluid Dyn. Res.
| publisher = [[IOP Publishing]] on behalf of the [[Japan Society of Fluid Mechanics]]
| country = Japan
| frequency = Bimonthly
| history = 1986–present
| openaccess = no<ref>{{Cite web|url=http://iopscience.iop.org/journal/1873-7005/page/Copyright-and-permissions|title=Copyright & permissions - Fluid Dynamics Research - IOPscience|website=iopscience.iop.org|access-date=2016-08-29}}</ref>
| license =
| impact = 0.990
| impact-year = 2014
| website = http://iopscience.iop.org/journal/1873-7005
| JSTOR = 
| OCLC = 905462743
| LCCN = sn87012756
| CODEN = FDRSEH
| ISSN = 0169-5983
| eISSN = 1873-7005
}}
'''''Fluid Dynamics Research''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering all fields of [[fluid dynamics]]. It is published by [[IOP Publishing]] on behalf of the [[Japan Society of Fluid Mechanics]]. The [[editor-in-chief]] is [[Yasuhide Fukumoto]] ([[Kyushu University]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.990.<ref name=WoS>{{cite book |year=2015 |chapter=Fluid Dynamics Research |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

Every year, one article published in the previous year is awarded the FDR Prize, to recognize its outstanding quality.<ref>{{cite web|url=http://iopscience.iop.org/journal/1873-7005/page/The%20FDR%20prize |title=The FDR prize |website=Fluid Dynamics Research |publisher=IOP Publishing}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official website|http://iopscience.iop.org/journal/1873-7005}}

[[Category:Publications established in 1986]]
[[Category:English-language journals]]
[[Category:Fluid dynamics journals]]
[[Category:Bimonthly journals]]
[[Category:IOP Publishing academic journals]]


{{Physics-journal-stub}}