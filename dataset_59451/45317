[[File:Philippe Pétain, Victor Denain, 1934.jpg|thumb|right|Denain (right) in 1934]]
'''Victor-Léon-Ernest Denain''' (6 November 1880, in [[Dax, Landes|Dax]] – 31 December 1952, in [[Nice]]) was a French general, aviator and politician.<ref>[http://atf40.forumculture.net/t4272-general-denain-1939-1940 "Général DENAIN 1939 1940"], ''ATF40 - Armée de Terre Française 1940''. {{fr icon}} Retrieved 13 July 2012.</ref> He was behind the creation of the [[Salon-de-Provence]] Air School and the general development of military aviation. 

==Biography==
Denain graduated from [[Ecole Spéciale Militaire de Saint-Cyr|Saint-Cyr]] in 1901 and joined the French Army's cavalry. In 1903, he was assigned as Second Lieutenant to the 6e régiment de chasseurs à cheval (6th Cavalary Regiment)and in October 1905, as a First Lieutenant, he campaigned in the southern territories with the 5e régiment de chasseurs d'Afrique (5th Regiment of Chasseurs d'Afrique).<ref name="L'Air et l'espace">{{cite book|title=L'Air et l'espace|url=https://books.google.com/books?id=qDwPAAAAIAAJ|accessdate=17 July 2012|year=1953|publisher=G. Roche d'Estrez|language=French|page=109}}</ref> In 1915, he transferred to the French Air Force where he commanded the aircraft of the [[Allies of World War I|Allied armies]] on the [[Eastern Front (World War I)|Eastern Front]] (1916–1918). With the French Air Force, he served in the [[Levant]] from 1918 to 1923, mainly in Syria.<ref name="ChurchillGilbert1977">{{cite book|last1=Churchill|first1=Randolph Spencer|last2=Gilbert|first2=Martin|title=Winston S. Churchill|url=https://books.google.com/books?id=30gmAQAAMAAJ|accessdate=17 July 2012|date=March 1977|publisher=Houghton Mifflin|isbn=978-0-395-25104-1|page=569}}</ref> As such, he became a protege of General [[Maxime Weygand|Weygand]] who arranged a foreign career for him. Denain was Head of the [[French Military Mission to Poland]] 1924 through 1931.  

From 10 March 1933 to 6 February 1934, General Denain served as the Chief of the Air Force General Staff under Air Minister [[Pierre Cot]], replacing General [[Joseph Barès]].  Denain and Cot dealt with the Armee de l'Air's technological issues.  They built a series of new aircraft built to make the air services competitive; they worked with French aircraft manufacturers on improvements to aircraft design and production; and they made threats to nationalize the French aviation industry.<ref name="HighamHarris2006">{{cite book|last1=Higham|first1=Robin D. S.|last2=Harris|first2=Stephen John|title=Why Air Forces Fail: The Anatomy of Defeat|url=https://books.google.com/books?id=T0gt_RjeCrgC&pg=PA54|accessdate=17 July 2012|date=17 February 2006|publisher=University Press of Kentucky|isbn=978-0-8131-2374-5|pages=54–}}</ref> Denain developed a strategic role for the Air Force with plans in 1933 for equipping it with 1,000 new planes.<ref>Kaufmann, J. E.; Kaufmann, H. W.: ''Hitler's Blitzkrieg Campaigns: The Invasion And Defense Of Western Europe'', Da Capo Press, 2002, p. 52. ISBN 9780306812163. {{fr icon}} </ref>  From 9 February 1934 to 24 January 1936, he was Aviation Minister in the [[Gaston Doumergue]] government.<ref>"Le général Denain face au défi allemand : continuité, ambitions et déceptions (février 1934 – janvier 1936)" in Thierry Vivier: ''La Politique Aéronautique Militaire de la France: janvier 1933–septembre 1939'', Editions L'Harmattan, 1997, p. 136. ISBN 9782738450333. {{fr icon}} </ref> During this time, an on behalf of France, Denain announced this his country would  organize a Paris to [[Hanoi]] air race in 1935, modeled after the London-Melbourne race.<ref name="Aero1934">{{cite book|title=Aero digest|url=https://books.google.com/books?id=o23mAAAAMAAJ|accessdate=17 July 2012|year=1934|publisher=Aeronautical Digest Pub. Corp.|page=22}}</ref>

By August 1936, under the [[Léon Blum#Government|Blum government]],<ref name="GannesMarion1936">{{cite book|last1=Gannes|first1=Harry|authorlink1=Harry Gannes|last2=Marion|first2=George|title=Spain defends democracy|url=https://books.google.com/books?id=Dt0pAQAAMAAJ|accessdate=17 July 2012|year=1936|publisher=Workers Library Publishers|page=10}}</ref> General Denain, had become High Commissioner of [[French protectorate of Morocco|French Morocco]].<ref name="GannesRepard1937">{{cite book|last1=Gannes|first1=Harry|authorlink1=Harry Gannes|last2=Repard|first2=Theodore|authorlink2=Theodore Draper|title=Spain in revolt|url=https://books.google.com/books?id=Gy5HAAAAIAAJ|accessdate=17 July 2012|year=1937|publisher=Knopf|page=142}}</ref> The next year, as Inspector General of the Air Force overseas, he transferred into the reserves. A very skilled pilot, he performed reconnaissance trips on numerous occasions. For example, as Air Minister he piloted his personal [[Breguet 27]] to Belgrade, accompanied by two squadrons of Breguet 27s and a [[Dewoitine]], to attend the funeral of King [[Alexander I of Yugoslavia]] on 17 October 1934.

==References==
{{Reflist}}

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Joseph Barès]]<br><small>As Chief of Staff of the ''Forces Aériennes''</small>}}
{{s-ttl|title=[[Chief of Staff of the French Air Force]]|years=1 April 1933 – 8 February 1934}}
{{s-aft|after=[[Joseph Barès]]}}
{{s-end}}

{{commons category}}

{{DEFAULTSORT:Denain, Victor}}
[[Category:French aviators]]
[[Category:French politicians]]
[[Category:French generals]]
[[Category:1880 births]]
[[Category:1952 deaths]]
[[Category:People from Landes (department)]]
[[Category:Chiefs of the Staff of the French Air Force]]