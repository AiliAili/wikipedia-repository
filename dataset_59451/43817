{{for|the V-bomber|Vickers Valiant}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name           = Valiant
|image          = Vickers 131 001.jpg<!--in the ''Image:filename'' format with no image tags-->
|caption        = Vickers 131 Valiant at Martlesham Heath, 1927
<!--Image caption; if it isn't descriptive, please skip-->
}}{{Infobox Aircraft Type
|type           = General purpose biplane
|national origin = United Kingdom
|manufacturer   = [[Vickers Limited]]
|designer       = <!--Only appropriate for single designers, not project leaders-->
|first flight   = 1927<!--If this hasn't happened, skip this field!-->
|introduction   = 1928<!--Date the aircraft entered or will enter military or revenue service-->
|retired        = 1929<!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|status         = <!--In most cases, redundant; use sparingly-->
|primary user   = Chilean Air Force
|more users     = <!-- Limited to THREE (3) 'more users' here (4 total users). Separate users with <br/>. -->
|produced       = <!--Years in production (e.g. 1970-1999) if still in active use but no longer built -->
|number built   = 1
|program cost   = <!--Total program cost-->
|unit cost      = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|developed from = <!--The aircraft which formed the basis for this aircraft-->
|variants with their own articles = <!--Variants OF this aircraft-->
}}
|}
[[File:Vickers 131 Valiant front quarter view.jpg|300px|right|thumb]]
The '''Vickers Type 131 Valiant''' was a [[United Kingdom|British]] general-purpose [[biplane]] produced by [[Vickers Limited|Vickers]] in 1927,<ref name="Jackson355" /> with the intention of replacing the [[Royal Air Force]]'s [[Airco DH.9A]]s, but was unsuccessful, with only a single example built, which was sold to Chile.

==Development and design==

In 1926, based on experience with the wooden-winged [[Vickers Vixen]] biplane, where the wings proved vulnerable to extremes of temperature and humidity, designed a set of metal wings for the Vixen, with which it became the [[Vickers Vivid]], and in parallel, designed an all-metal general purpose biplane, the Vickers Type 131, hoping to replace the DH.9A in that role.<ref name="Andrews Vickers p187-8">Andrews and Morgan, pp. 187-188.</ref> In 1927, the British [[Air Ministry]] issued [[Air Ministry Specification|Specification 26/27]] for a DH.9A replacement which, to save money, had to use as many components of the DH.9A as possible because the RAF held large stocks of DH.9A spares. Vickers submitted the Type 131 design to the Ministry but, as it did not make use of the required DH9A components, did not receive a contract for a prototype. However Vickers decided to build a single prototype as a private venture for evaluation against the specification.<ref name="Mason bomber p191">Mason 1994, p. 191.</ref>

The Vickers 131 Valiant was a single-bay biplane of all-metal construction, sharing much of the structure with the Vivid. It was powered by a 492&nbsp;hp (367&nbsp;kW) [[Bristol Jupiter]] engine, and the crew of two sat in separate but adjacent cockpits, giving good communication between the pilot and observer. It could carry up to 500&nbsp;lb (230&nbsp;kg) of bombs under the wing, with a fixed [[Vickers machine gun]] for the pilot and a [[Lewis gun]] on a [[Scarff ring]] for the observer.<ref name="Mason bomber p192">Mason 1994, p. 192.</ref><ref name="Jarrett p31-3">Jarrett 1997, pp. 31-33.</ref>

The prototype had made its first flight by 5 March 1927,<ref name="Jarrett p33">Jarrett 1997, p. 33.</ref> and underwent official evaluation against the designs from [[Bristol Aeroplane Company|Bristol]] (the [[Bristol Beaver|Beaver]]), [[Fairey Aviation Company|Fairey]] (the [[Fairey Ferret]] and [[Fairey III|IIIF]]), [[Gloster Aircraft Company|Gloster]] (the [[Gloster Goral|Goral]]) and [[Westland Aircraft|Westland]] (the [[Westland Wapiti|Wapiti]]). Its initial tests showed it to possess good handling,<ref name="Andrews p189">Andrews and Morgan 1988, p. 189.</ref> and was taken forwards for squadron trials, along with the Ferret, IIIF and Wapiti.<ref name="Jarrett p35">Jarrett 1997, p. 35.</ref> Following these trials, the Wapiti was chosen as the winner, with the Valiant, which was 30% more expensive than the Wapiti,<ref name="Mason bomber p189">Mason 1994, p. 189.</ref> rejected because it was a poor bombing platform, not being sufficiently stable.<ref name="Jarrett p35"/>

==Operational history==
The Valiant was shipped to Chile in 1928 for demonstration to the [[Chilean Air Force]] that wanted a replacement for their Vixens. While no production followed, Chile purchased the prototype,<ref name="Jackson355">Jackson 1974, p. 355</ref> which entered service with the School of Aviation, being destroyed in a crash on 29 March 1929.<ref name="Jarrett p35"/>

==Operators==
;{{flag|Chile}}
*[[Chilean Air Force]]

==Specifications (Type 131 Valiant)==
{{aircraft specifications|
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=Limited Editions Part 7:Vickers Valiant biplane <ref name="Jarrett p35" /> 
|crew=Two (pilot and observer/gunner)
|length main=33 ft 5½ in
|length alt=10.20 m
|span main=45 ft 5 in
|span alt=13.85 m
|height main=11 ft 7½ in<ref>Tail down.</ref>
|height alt=3.54 m
|area main=597 ft²
|area alt=55.5 m²
|empty weight main=3,048 lb
|empty weight alt=1,385 kg
|loaded weight main=4,519 lb
|loaded weight alt= 2,054 kg
|max takeoff weight main=
|max takeoff weight alt= 
|engine (prop)=[[Bristol Jupiter]] VI 
|type of prop=radial engine
|number of props=1
|power main=492 hp 
|power alt=367 kW
|max speed main=130 mph
|max speed alt=209 km/h 
|max speed more=at sea level
|range main=
|range alt= 
|ceiling main=19,650 ft
|ceiling alt=5,990 m
|climb rate main=940 ft/min
|climb rate alt= 4.8 m/s
|loading main=7.56 lb/fm²
|loading alt= 37.0 kg/m²
|power/mass main=0.11 hp/lb
|power/mass alt= 0.18 kW/kg
|guns=2× 0.303 (7.7 mm) machine guns
|bombs= 500 lb (227 kg) of bombs
}}

==See also==
{{aircontent|
|related=
*[[Vickers Vixen]]
*[[Vickers Vivid]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
* [[List of bomber aircraft]]
|see also=<!-- other relevant information -->
}}

==References==
{{commons category|Vickers 131 Valiant}}

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book|last=Andrews |first=C.F.|author2=E.B. Morgan  |title=Vickers Aircraft since 1908  |authorlink= |year=1988 |publisher= Putnam|location=London |isbn=0-85177-815-1 }}
*{{cite book |last= Jackson|first= A.J.|authorlink= |title= British Civil Aircraft since 1919 Volume 3|year= 1974|publisher= Putnam|location= London|isbn=0-370-10014-X }}
*{{cite magazine |last=Jarrett |first=Philip |authorlink= | title = Limited Editions Part 7: Vickers Valiant biplane |date= March 1997|magazine= Aeroplane Monthly|volume= 25| issue =  3 |pages=30–36|publisher=IPC |location=London |issn=0143-7240|url= |accessdate= |quote= }}
*{{cite book|last=Mason |first=Francis K. |authorlink= |title=The British Bomber since 1914  |year=1994 |publisher=Putnam |location= London|isbn= 0-85177-861-5|url= |accessdate=}}
{{refend}}

{{Vickers aircraft}}

[[Category:British bomber aircraft 1920–1929]]
[[Category:British military reconnaissance aircraft 1920–1929]]
[[Category:Vickers aircraft|131 Valiant]]