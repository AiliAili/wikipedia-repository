{{redirect|The Israelite|an Israelite person|Israelites}}
{{Infobox Newspaper
| name                = The American Israelite
| logo                = 
| image               = [[File:The American Israelite 2010.PNG|175px]]
| caption             = August 5, 2010, front page
| type                = Weekly [[newspaper]]
| format              = [[Broadsheet]]
| foundation          = July 15, 1854
| ceased publication  = 
| price               = 
| owners              = 
| political position  = 
| publisher           = Netanel Deutsch
| editor              = Netanel Deutsch
| staff               = 
| politics            = 
| circulation         = 6,500<ref>{{cite web|url=http://www.mondotimes.com/1/world/us/35/1922/25399|title=American Israelite|accessdate=2016-12-25}}</ref>
| headquarters        = 18 West Ninth Street<br/>Cincinnati, Ohio
| ISSN                = 
| website             = [http://www.americanisraelite.com www.americanisraelite.com]
}}
'''''The American Israelite''''' is a [[Jews|Jewish]] weekly newspaper published in [[Cincinnati, Ohio]].  Founded in 1854 as '''''The Israelite''''' and assuming its present name in 1874, it is the longest-running English-language Jewish newspaper still published in the United States.<ref name="cinc-mag">{{cite news | url=https://books.google.com/books?id=2-0CAAAAMBAJ&pg=PA30 | title=Wise Man | author=Irwin, Julie | magazine=[[Cincinnati Magazine]] | date=March 2000}}</ref>

The paper's founder, Rabbi [[Isaac Mayer Wise]], and publisher, Edward Bloch and his [[Bloch Publishing Company]], were both very influential figures in American Jewish life.
During the 19th century, ''The American Israelite'' became the leading organ for [[Reform Judaism]] in America.  During the early 20th century, it helped geographically dispersed American Jews, especially in the West and the South of the country, keep in touch with Jewish affairs and their religious identity.

== Founding and early history ==
The first Jewish newspaper published in Cincinnati was the English-language ''The  Israelite'', established on July 15, 1854.<ref name="ai-web">{{cite web | url=http://www.americanisraelite.com/ | title=American Israelite: Current Issue | publisher=The American Israelite | accessdate=August 5, 2010}}</ref>  It was also among the first Jewish publications in the nation.<ref>{{cite news | url=https://select.nytimes.com/mem/archive/pdf?res=FB0713F7385A11728DDDAA0894D1405B8088F1D3 | title=Charles E. Bloch, Long a Publisher | newspaper=[[The New York Times]] | date=September 3, 1940 | page=17}}</ref>  It was founded by Rabbi [[Isaac Mayer Wise]], who became known as the father of [[Reform Judaism]] in the United States.<ref name=ISJL2006TI/>  Its initial issues were published by Charles F. Schmidt.<ref name="Bloch"/> The paper lost $600 in its first year, and although Wise repaid the publisher out of his own funds, Schmidt terminated the relationship.<ref name="Bloch"/> Edward Bloch and his [[Bloch Publishing Company]] began to publish the paper with the issue of July 27, 1855.<ref name="Bloch">{{cite book | first=Robert | last=Singerman | chapterurl=http://www.blochpub.com/early_history | chapter=Bloch & Company: Pioneer Jewish Publishing House in the West | title= Jewish Book Annual, Volume 52, 1994–1995 | pages= 110–130 | year=1994 | editor-first=Jacob | editor-last=Kabakoff | publisher=[[Jewish Book Council]] | isbn=1-885838-02-6 | location=New York}}</ref> Bloch, who was Wise's brother-in-law, subsequently became known as the dean of American Jewish publishers.

From the start, the newspaper's motto was ''' יהי אור ''' "Let There Be Light," and still is.<ref name="ai-web"/><ref name="isaac-profile">{{cite news | url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9904EFDF1139E433A25750C0A9629C94699ED7CF | title=Isaac M. Wise | author=May, Max B. | newspaper=[[The New York Times Magazine]] | date=April 3, 1898 | page=SM11}}</ref>  Its two goals were to propagate the principles of Reform Judaism and to keep American Jews, who often lived in small towns singly or in communities of two or three families, in touch with Jewish affairs and their religious identity.<ref name="jencyc-ai">{{cite web | url=http://www.jewishencyclopedia.com/view.jsp?letter=A&artid=1388 | title=American Israelite, The | author=Wise, Leo | work=[[Jewish Encyclopedia]] | publisher=[[Funk and Wagnalls]] | date=1901–1906}}</ref>

The publication, along with ''Die Deborah'', a German-language supplement that Wise started the following year, soon attracted a large circulation and was influential in helping the nascent Reform movement spread throughout North America.<ref name="isaac-obit">{{cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9807E3DD1E3CE433A25754C2A9659C946197D6CF | title=Career of Rabbi Wise. | newspaper=[[The New York Times]] | date=March 27, 1900 | page=1}}</ref>
Both Wise and the paper had a reach beyond Cincinnati, and especially to the growing Jewish communities in the American Midwest and South.<ref name="cinc-mag"/>
In 1858, for instance, the members of [[Temple Israel (Memphis, Tennessee)|Congregation B'nai Israel]] in [[Memphis, Tennessee]] advertised for their first rabbi in ''The Israelite'', at the same time they advertised for a kosher butcher.<ref name=ISJL2006TI>{{cite web | url=http://www.isjl.org/history/archive/tn/HistoryofTempleIsrael.htm | title=History of Temple Israel | publisher=[[Goldring / Woldenberg Institute of Southern Jewish Life]] | work=Encyclopedia of Southern Jewish Communities | location=Jackson, Mississippi | year=2006 | accessdate= July 22, 2010 }}</ref>

[[File:The Israelite 1859-09-163.png|thumb|right|upright|175px|September 16, 1859, front page of ''The Israelite'']]

Despite its spread, the early years of ''The Israelite'' were a financial struggle.  Most subscribers did not pay their bills, the [[Panic of 1857]] adversely affected it, and the paper lost half its subscribers in the South during the [[American Civil War|Civil War]].<ref name="Bloch"/>  Bloch travelled east several times in the late 1850s in order to solicit subscriptions and advertising.<ref name="Bloch"/>  Wise's admitted sloppiness in monetary matters did not help either.<ref name="Bloch"/>  Nevertheless, the newspaper and Bloch stayed out of bankruptcy and relocated to larger offices twice during this period.<ref name="Bloch"/>

Wise, a prolific writer, published in the editorial columns of ''The Israelite'' numerous studies on various subjects of Jewish interest.
Besides being the leading organ for American Reform Judaism, it also forcefully defended the civil and religious rights of all Jews.<ref name="centcinc"/>
Wise tirelessly expounded his call to the "ministers and other Israelites" of the United States, urging them to form a union which might put an end to the prevalent religious anarchy.<ref name="jencyc-w"/>  In 1873, twenty-five years after he had first broached the idea, the [[Union of American Hebrew Congregations]] was organized at Cincinnati.<ref name="jencyc-w">{{cite web | url=http://www.jewishencyclopedia.com/view.jsp?artid=214&letter=W | title=Wise, Isaac Mayer | author=Adler, Cyrus | author2=Philipson, David | work=[[Jewish Encyclopedia]] | publisher=[[Funk and Wagnalls]] | date=1901–1906}}</ref>
Another campaign he presented in the columns of ''The Israelite'' was the desire for an educational institution, and this eventually led to success in 1875 when the [[Hebrew Union College]] opened its doors for the reception of students.<ref name="jencyc-w"/>
Wise also wrote a number of novels, which appeared first as serials in the ''Israelite''.

== New name and continued influence ==
''The Israelite'' was renamed ''The American Israelite'' beginning with the issue of July 3, 1874.<ref name="Bloch"/>  The goal was to make the name more in consonance with the ideas it represented.<ref name="jencyc-ai"/>  Despite the change, the paper continued to cover and advocate for not only American Jews but also Jews around the world.<ref name="isaac-profile"/>  By 1879, a typical issue had eight pages 28-by-42-inches in size, and a subscription cost $4, or $5 if the ''Die Deborah'' four-page supplement was included.<ref name="and-1879">{{cite book | title=Geo. P. Rowell & Co.'s American Newspaper Directory | year=1879 | publisher=Geo. P. Rowell & Co. | location=New York | page=[https://books.google.com/books?id=oIrQAAAAMAAJ&pg=PA266 266]}}</ref>

Rabbi Wise's son Leo Wise, who had become business manager for the paper in 1875,<ref name="jencyc-ai"/> took over as its publisher from 1883 to 1884, and then he did so again, permanently, in 1888 (due apparently to some kind of rupture between Leo Wise and Bloch).<ref name="Bloch"/> A sister publication, ''The Chicago Israelite'', was started in 1885.<ref name="Bloch"/>  The papers stressed their reputation in trade publications, stating "None but clean advertisements of reputable houses accepted."<ref name="and-1900"/>

[[File:The American Israelite TIS-1900-03-29-001.png|thumb|right|upright|175px|March 29, 1900, page of ''The American Israelite'' that announced the death of Isaac Mayer Wise]]

Leo Wise gradually took over the principle editorial functions from his father,<ref name="jencyc-ai"/> but Rabbi Wise remained active on the paper until his death on March 26, 1900,<ref name="isaac-profile"/><ref name="isaac-obit"/> writing an editorial for it just a few days before.<ref name="centcinc">{{cite book | title=Centennial History of Cincinnati and Representative Citizens, Volume 1 |first=Charles Theodore  | last=Greve | publisher=Biographical Publishing Company | location=Chicago | year=1904 | page=[https://books.google.com/books?id=eJxABLtxX60C&pg=PA946 946]}}</ref>  Ownership then passed to Leo Wise.<ref name="leo-obit">{{cite news | url=https://select.nytimes.com/mem/archive/pdf?res=F10A1EF6355F1A7A93CAAB178AD85F478385F9 | title=Leo Wise Dies at 84; Cincinnati Editor | newspaper=[[The New York Times]] | date=January 28, 1933 | page=13}}</ref>

By 1900, ''The American Israelite'', in combination with ''The Chicago Israelite'', claimed a circulation of other 35,000, about 12,000 in Ohio and Illinois and the balance spread across almost every other state as well as Canada and Mexico.<ref name="and-1900">{{cite book | title=American Newspaper Directory | year=1900 | publisher=Geo. P. Rowell & Co. | location=New York | page=[https://books.google.com/books?id=EE0CAAAAYAAJ&pg=PA164 164]}}</ref>  The publication ''Printer's Ink'' said they had the largest guaranteed circulation of any Jewish newspaper in the U.S.,<ref name="and-1900"/> and it continued to be especially strong in the West and the South.<ref name="jencyc-ai"/>   One 1902 book characterized ''The American Israelite'' as "the leading Jewish newspaper in the United States and the National Journal of the Jews."<ref>{{cite book | title=The Cincinnati Southern Railway: A History | first=Charles G. | last=Hall | publisher=[[Cincinnati, New Orleans and Texas Pacific Railway]] | location=Cincinnati | year=1902 | page=[https://books.google.com/books?id=vmVS_SnU_1MC&pg=PA167 167] }}</ref>

In the early 20th century, the paper's short articles were sometimes picked up and run by ''[[The New York Times]]'' with a credit "From The American Israelite".<ref name="anti-zion">{{cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9E00E6D8173BE733A2575AC1A9679C946397D6CF | title=The Evil of Zionism | agency=From The American Israelite | newspaper=[[The New York Times]] | date=January 19, 1902 | page=28}}</ref><ref>{{cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9F00EEDD163EE733A25756C2A9649C946497D6CF | title=How Moscow Was 'Purified' | agency=From The American Israelite | newspaper=[[The New York Times]] | date=February 25, 1905 | page=8}}</ref><ref>{{cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9E00E4DF143FE633A25754C1A9639C946296D6CF | title=Carnegie Pension for a Rabbi | agency=From The American Israelite | newspaper=[[The New York Times]] | date=May 17, 1913 | page=10}}</ref>  In those years, ''The American Israelite'' became known for its very strong stance against the new [[Zionism]] movement, calling it in 1902 a "pernicious agitation" that would undermine the acceptance of Jews in the countries where they currently resided.<ref name="anti-zion"/>  Rabbi [[David Philipson]] was among the editorial contributors to the paper<ref name="jencyc-ai"/> who used it to oppose Zionism, arguing that Judaism was a religion exclusively, and thus stateless.  Other noted contributors to the paper in this era included Rabbi [[Moses Mielziner]] and Jewish history scholar [[Gotthard Deutsch]], as well as other prominent rabbis and Jewish thinkers within the country.  The paper gave extensive coverage to the goings-on of the Union of American Hebrew Congregations and the Hebrew Union College (and was sometimes viewed as a publication of them), as well as notices of various rabbinical conferences.<ref name="jencyc-ai"/>

''Die Deborah'' was discontinued after Isaac Wise's death, then resumed for a while.<ref name="jencyc-ai"/>
''The Chicago Israelite'' ceased publication in 1920.<ref>{{cite web | url=https://www.jewishvirtuallibrary.org/jsource/judaica/ejud_0002_0004_0_04235.html | title=Chicago | work=[[Jewish Virtual Library]] | publisher=American-Israeli Cooperative Enterprise | year=2008}}</ref>
Leo Wise edited ''The American Israelite'' until his retirement at age 78 in 1928<ref name="nyt-jonah">{{cite news | url=https://select.nytimes.com/mem/archive/pdf?res=FB0616F93C5C177A93CAA9178AD85F4C8285F9 | title=American Israelite Has a New Editor | newspaper=[[The New York Times]] | date=January 8, 1928 | page=29}}</ref> (he died in 1933).<ref name="leo-obit"/>  Another son of Isaac, Isidor Wise, worked as a writer and associate editor for the paper until his death in 1929.<ref>{{cite news | url=https://select.nytimes.com/mem/archive/pdf?res=F60C11F93D5F177A93C4A8178AD95F4D8285F9 | title=Isidor Wise Dead | newspaper=[[The New York Times]] | date=November 16, 1929 | page=13}}</ref>

== Subsequent history ==
Leo Wise was succeeded as editor and publisher of ''The American Israelite'' in 1928 by his half-brother, Rabbi [[Jonah Wise]] of New York, who remained in that city<ref name="nyt-jonah"/> and who himself became a long-time leader of American Reform Judaism.

The Jonah Wise arrangement did not last long, and in 1930, journalist Henry C. Segal bought the paper and became its editor and publisher for more than five decades, until his death in 1985.<ref>{{cite news | url=https://www.nytimes.com/1985/07/20/us/henry-c-segal.html | title=Henry C. Segal | newspaper=[[The New York Times]] | date=July 20, 1985 | page= }}</ref>  Along with Isaac Wise, Segal is still named on the paper's [[Masthead (publishing)|masthead]].<ref name="ai-web"/>

Contributors to the newspaper in the late 1980s and early 1990s included writer Don Canaan.<ref name="aja-canaan"/> His four-part series published in 1988, "Jews in Ohio's Prisons: Does Anyone Care?", won the award for best weekly journalism from the [[Ohio State Bar Association]].<ref name="aja-canaan">{{cite web | url=http://collections.americanjewisharchives.org/ms/ms0708/ms0708.html | title=A Finding Aid to the Don Canaan Papers. 1986-2013. | publisher=[[The Jacob Rader Marcus Center of the American Jewish Archives]] | accessdate=February 13, 2016}}</ref>

By the 1990s, the paper was focusing on local Jewish news.<ref name="cinc-mag-96">{{cite news | url=https://books.google.com/books?id=Ax8DAAAAMBAJ&pg=PA43 | title=Lies, Damned Lies | author=Tate, Skip | magazine=[[Cincinnati Magazine]] | date=October 1996}}</ref>
In 1995, ''The American Israelite'' was sued for $2 million by an Ohio lawyer for calling him and his son anti-Semitic.<ref name="cinc-mag-96"/><ref>{{cite news | url=http://www.highbeam.com/doc/1G1-45363531.html | title=American Israelite Sued for $2 Million | author=Canaan, Don | agency=[[United Press International]] | date=February 28, 1995}}</ref>

By 2010, Ted Deutsch was the editor and publisher.  A typical issue ran 24 pages, with color front and back pages and black-and-white inside.<ref name="ai-arch">{{cite web | url=http://www.americanisraelite.com/ | title=American Israelite: Archive | publisher=The American Israelite | accessdate=August 7, 2010}}</ref>  Some stories were locally written, while many others were run from the [[Jewish Telegraphic Agency]].<ref name="ai-arch"/>  It published full facsimile copies of its issues on its website.<ref name="ai-arch"/>

== See also ==
* [[List of Jewish newspapers in the United States]]

== References ==
{{reflist|2}}

== External links ==
* [http://www.americanisraelite.com Official website]
* [http://www.israeliteonline.com/ The Israelite 1859–1867 full issues]
* [http://americanjewisharchives.org/wise/home.php Isaac Mayer Wise Digital Archive, including some reproduced pages from the paper through 1900]
{{good article}}

{{DEFAULTSORT:American Israelite}}
[[Category:Publications established in 1854]]
[[Category:Jewish newspapers published in the United States]]
[[Category:Jews and Judaism in Cincinnati]]
[[Category:Newspapers published in Cincinnati]]
[[Category:Weekly newspapers published in the United States]]