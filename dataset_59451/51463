{{Infobox journal
| title = High Performance Polymers
| cover = [[File:High Performance Polymers front cover image.jpg]]
| editor = John Connell 
| discipline = [[Chemistry]]
| former_names = 
| abbreviation = High Perform. Polym.
| publisher = [[Sage Publications]]
| country = 
| frequency = 8/year
| history = 1989-present
| openaccess = 
| license = 
| impact = 1.090
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201635/title
| link1 = http://hip.sagepub.com/content/current
| link1-name = Online access
| link2 = http://hip.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 300291348
| LCCN = 93648956 
| CODEN = HPPOEX
| ISSN = 0954-0083 
| eISSN = 1361-6412
}}
'''''High Performance Polymers''''' is a [[peer-reviewed]] [[scientific journal]] that covers the field of [[polymer chemistry]], in particular [[molecular structure]]/[[processability]]/property relationships of high performance polymers such as [[Electroactive polymers#Liquid Crystalline Polymers|liquid crystalline polymers]]. It is published eight times a year by [[Sage Publications]]. The [[editor-in-chief]] is John Connell ([[NASA Langley Research Center]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Engineered Materials Abstracts]], the [[Materials Science Citation Index]], [[Scopus]], and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.090, ranking it 53rd out of 82 journals in the category "Polymer Science".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Polymer Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Sciences |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201635/title}}


[[Category:Chemistry journals]]
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1989]]