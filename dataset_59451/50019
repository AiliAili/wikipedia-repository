'''Clairmont L. "Claire" Egtvedt''' (1892–1975) was an airplane designer and president and chairman of the [[Boeing Company]]. Along with [[Ed Wells]], he is considered to be the father of the [[Boeing B-17]] bomber.<ref name="Redding1997" />

Egtvedt was chief engineer on airplanes such as the B-1 mailplane, [[Boeing Model 15]] and [[Boeing Model 21]] pursuit airplanes, and the [[Boeing Model 40]] airliner-mailplane. Though promoted to the executive ranks, he also participated heavily the design of the [[Boeing Model 80]], [[XB-15]], and [[B-17]] models.<ref name="Mansfield1966" /> As president, and later chairman of Boeing he oversaw and approved the development of the [[B-47]], [[B-52]], 707, 727,737, and 747. Flight Global ranked Egtvedt 2nd behind [[William McPherson Allen]] as most impactful Boeing boss.<ref>"Boeing Bosses: All 10, in Order of Impact", ''Flight Global'', https://www.flightglobal.com/news/articles/boeing-bosses-all-10-in-order-of-impact-414295/</ref>

==Biography==

Born just outside Stoughton, WI in 1892, Egtvedt was the son of Sver Egtvedt, a Norwegian immigrant, and Mary (Ruble) Egtvedt, a first generation Norwegian-American.  Egtvedt was raise in a tight-knit ethnic community.  Egtvedt attended high school in Stoughton, where he played basketball and was a track champion.  The family moved to Seattle, WA in 1911, settling in the Scandinavian neighborhood of Ballard.    

Egtvedt (along with his fellow future Boeing chairman, [[Philip G. Johnson]]) was hired by [[William E. Boeing]] as a draftsman straight out of the University of Washington in 1917.<ref>[[Robert J. Serling|Serling, Robert J.]], ''Legend and Legacy'', St. Martins Press, 1991 ISBN 0-312-05890-X</ref>{{page needed|date=June 2016}} Egtvedt's ability became immediately apparently to Bill Boeing, who would recollect that Egtvedt "took to the aircraft engineering very readily".<ref>Boeing Historical Archives, subject file "William E. Boeing Interviews". Harold Mansfield and Ren Phillips interview with William E. Boeing, Feb. 15, 1955</ref> Egtvedt rose quickly, becoming chief engineer by the early 1920s.

In 1922, the aircraft industry was suffering a debilitating downturn due to a market flooded with World War I surplus aircraft.<ref>"Shutting Down the Aeronautical Industry," ''Aviation'', Vol. XVI, No. 11, 17 March 1924, pp. 282-283</ref> Bill Boeing was digging deep into his own pockets to make payroll, the craftsmen on the shop floor were building furniture, and any aircraft manufacturing that was occurring was of other people's designs (the [[GAX]] airplanes and [[Thomas-Morse]] fighters). Egtvedt knew the importance of developing new products, not just from a business standpoint, but from a technical proficiency standpoint. Egtvedt confronted Boeing, declaring, "We are building airplanes, not cement sidewalks!"<ref name="Mansfield1966">Mansfield, Harold, ''Vision: The Story of Boeing,'' New York: Popular Library, 1966 {{OCLC|1478117}}</ref>{{page needed|date=June 2016}} Egtvedt convinced Boeing that he had to allow his engineering staff to start designing airplanes again. A year later, the highly successful Boeing PW-9 fighter first flew.<ref>Rickard, J., "Boeing PW-9," http://www.historyofwar.org/articles/weapons_boeing_PW-9.html, [cited 12 May 2016]</ref> A whole family of successful fighters followed, along with Boeing's first commercial airliners the Model 40 mailplane and subsequent Model 80 airliner.<ref>[[Peter M. Bowers|Bowers, Peter M.]] ''Boeing Aircraft Since 1916''. London: Putnam Aeronautical Books, 1989 ISBN 0-87021-037-8</ref>{{page needed|date=June 2016}}

After becoming the company's vice president in 1926, Egtvedt continued to push the company's airplane designs forward, helping define the configuration of the seminal Model 247. 
Under Egtvedt's watch, Boeing shifted from producing small pursuit aircraft to large bombers and commercial aircraft. At Egtvedt's direction, the company invested its limited resources into projects such as the B-17, [[Boeing 307 Stratoliner]], [[Boeing 314 Clipper]], paving the way for Boeing to become the premier manufacturer of large airplanes.<ref>"Executive Biography of Clairmont L. Egtvedt", The Boeing Company, http://www.boeing.com/history/pioneers/clairmont-l-egtvedt.page</ref> For his role in creating the XB-15 and B-17 bombers, Egtvedt is often referred to as "Father of the Four Engine Bomber".<ref name="Redding1997">Redding, Robert and Yenne, Bill, ''Boeing: Planemaker to the World (Revised)'', Thunder Bay Press, 1997 ISBN 0-517-42270-0</ref>{{page needed|date=June 2016}}

Egtvedt was named chairman of the Boeing Airplane Company in 1935, following the dissolution of [[United Aircraft and Transport Corporation]]. He remained in this role until his retirement in 1966.<ref>"Claire Egtvedt", Boeing Historical Archives, People Files</ref> Under his stewardship as chairman, Boeing embarked on the sequence of airplane development that has come to define the company: the B-29, B-47, B-52, and finally the first of the 7-series family of jets, the 707, 727, and 737.

Egtvedt was named an [[Alumnus Summa Laude Dignatus]] of the [[University of Washington]] in 1957.<ref>http://www.washington.edu/alumni/about-uwaa/awards/asld/ University of Washington Alumni Awards: ASLD</ref>

==References==
{{reflist}}

==Further reading==
* {{cite news |title=Former Wisconsin Man Responsible for Boeing's Fighting, Peace Giants |url=https://news.google.com/newspapers?nid=1499&dat=19441201&id=EakWAAAAIBAJ&sjid=HSMEAAAAIBAJ&pg=3736,345906&hl=en |newspaper=The Milwaukee Journal |date=1 December 1944}}
* {{cite news |last=Thomas, Jr. |first=Robert McG. |authorlink=Robert McG. Thomas Jr. |date=21 October 1975 |title=Claire Egtvedt of Boeing, Developer of B-17, Is Dead |newspaper=The New York Times}}
* {{cite book |last=van den Linden |first=F. Robert |date=1991 |title=The Boeing 247: The First Modern Airliner |publisher=University of Washington Press |isbn=978-0-295-80381-4}}
* {{cite book |last=Shearer |first=Benjamin F. |date=2007 |title=Home Front Heroes: A Biographical Dictionary of Americans During Wartime |url=https://books.google.com/books?id=UJhx8H8XLnQC&pg=PA265 |publisher=Greenwood Publishing Group |pages=265– |isbn=978-0-313-33421-4}}
* {{cite news |last=Johnson |first=Tracy |date=18 February 2004 |title=A $20 million tug of war over a widow's will |url=http://www.seattlepi.com/local/article/A-20-million-tug-of-war-over-a-widow-s-will-1137330.php |newspaper=Seattle Post-Intelligencer}}
* {{cite web |url=http://www.thekenney.org/pdf/kq2010-1.pdf |title=History of the Egtvedt's, A Model of Planned Giving |date=Spring 2010 |website=The Kenney Quarterly}}

==External links==
* {{cite web |url=http://yourshot.nationalgeographic.com/photos/3038155/ |title=The office of early Boeing chief engineer Claire Egtvedt, in the Red Barn exhibit, Museum of Flight, Seattle. |last=Morgan |first=Sue |website=National Geographic Your Shot}}

{{DEFAULTSORT:Egtvedt, Claire L.}}
[[Category:20th-century American businesspeople]]
[[Category:American aviation businesspeople]]
[[Category:American chairmen of corporations]]
[[Category:Boeing people]]
[[Category:Businesspeople from Seattle]]
[[Category:Businesspeople in aviation]]
[[Category:Chairmen of Boeing]]
[[Category:People from Wisconsin]]
[[Category:University of Washington alumni]]
[[Category:1892 births]]
[[Category:1975 deaths]]