<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=H.P.20
 | image=HP 20.png
 | caption=Showing open and closed slots and depressed aileron
}}{{Infobox Aircraft Type
 | type=experimental monoplane
 | national origin=[[United Kingdom]]
 | manufacturer=[[Handley Page]]
 | designer=
 | first flight=24 February 1921
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Handley Page H.P.20''' was an experimental monoplane modification of a [[de Havilland DH.9A]], built to study controllable [[Leading edge slot|slots]] and slotted ailerons as high lift devices.  It was the first aircraft to fly with controllable slots.

==Development==
[[Frederick Handley Page]] obtained his master patent for controllable [[leading edge slot|slots]] on the edge of an aircraft wing on 24 October 1919.<ref name="Barnes1">{{Harvnb|Barnes|James|1987|pages=211}}</ref>  He knew that the [[lift coefficient]] of all wings increased linearly until the stall was approached, then fell away; he argued that if the stall could be delayed, higher lift coefficients could be reached.  In Germany [[Gustav Lachmann]] had the same idea, though through a wish to avoid the dangers of stalling.<ref name="Barnes2">{{Harvnb|Barnes|James|1987|pages=215–6}}</ref>  Rather than going to litigation an agreement was reached in which Lachmann, working with [[Prandtl]] at the advanced Goettingen air tunnel acted as consultant to Handley Page.  Lachmann heard about the Handley Page work when they modified a standard [[de Havilland DH.9A|DH.9A]] with fixed slots and demonstrated it dramatically at the Handley Page airfield at [[Cricklewood]] on 21 October 1920.  That machine was retrospectively designated the H.P.17.  The first aircraft with pilot controllable slots, designed by Handley Page and wind tunnel tested by Lachmann was called the X.4B in the company's contemporary notation but became, retrospectively, the H.P.20.<ref name="Barnes3">{{Harvnb|Barnes|James|1987|pages=212–5}}</ref>  The [[Air Ministry]] met the cost.

Like the H.P.17 it used a D.H.9A engine, fuselage and empennage, but fitted with an entirely new wing.<ref name="Barnes3"/><ref>[http://www.flightglobal.com/pdfarchive/view/1921/1921%20-%200732.html ''Flight'' 10 November 1921 p.732-3]</ref>  The H.P.20 was a high-wing monoplane, using a thick wing with a straight leading edge but taper on the trailing edge.  It was a semi-cantilever structure bolted to a small cabane on the fuselage and braced to the lower fuselage longerons with a pair of steeply rising struts on each side. The heaviness of early cantilever wing structures is shown by a comparison of the loaded weight of the H.P.20 (6,500&nbsp;lb) with that of the loaded standard biplane DH.9A (4,645&nbsp;lb including fuel for over 5 hours of flight and a 460&nbsp;lb bomb load).<ref name="Flight">[http://www.flightglobal.com/pdfarchive/view/1956/1956%20-%200679.html ''Flight'' 1 June 1956 p.679]</ref> The undersurface was flat and the front edge cropped to allow the full span slats, when closed, to form the true leading edge.  The slats were hinged ahead of the wing and at their leading edges; their rotation formed the slots. In addition, slots opened in front of the ailerons when they were lowered.  This was done via a groove in the wing just in front of the aileron hinge, narrowing towards the top surface.<ref name="Barnes3"/>

The spans and wing areas of the DH.9A and H.P.20 were about the same,<ref name="Barnes4">{{Harvnb|Barnes|James|1987|pages=229}}</ref><ref name="Jackson1">{{Harvnb|Jackson|1978|pages=118}}</ref> so the wing chord of the monoplane was about double that of the biplane. As a result the tailing edge extended aft beyond the DH.9A's pilot's cockpit and so the H.P.20 was flown from what had been the gunner's position.<ref name="Flight"/> There was a small cut out in the trailing edge to enhance the pilot's view. As there was no upper wing to house the fuel, the H.P.20 had a slightly round-ended cylindrical tank mounted high over the centre section to provide gravity feed.  Because high angles of attack were used in landing and take off with the slots open, the undercarriage was lengthened,<ref name="Flight"/> as it had been on the H.P.17.<ref name="Barnes5">{{Harvnb|Barnes|James|1987|pages=211–3}}</ref> There was an airflow direction indicator mounted on a little boom which projected forward of the starboard wing.

The H.P.20 was first flown, with slots closed on 24 February 1921 at Cricklewood.<ref name="Barnes3"/>  About a month later it was flying with controllable slots.  In one test it landed at 43 mphh (69&nbsp;km/h) at a [[wing loading]] of 11&nbsp;lb/ft<sup>2</sup> (54&nbsp;kg/m<sup>2</sup>),<ref name="Jackson2">{{Harvnb|Jackson|1978|pages=115}}</ref> about the same as a [[Cessna 152]].  This corresponds to a lift coefficient of 1.17.  Aerodynamic loads made the slats hard to operate reliably.  After manufacturer's tests, the Air Ministry agreed to take on the aircraft, but the Ministry's pilot made a heavy landing at Cricklewood during acceptance flights and the H.P.20 remained there for repairs until final delivery in February 1922.<ref name="Barnes3"/>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->
==Specifications==

{{aerospecs
|ref={{Harvnb|Barnes|James|1987|pages=229}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=1
|capacity=
|length m=9.15
|length ft=30
|length in=0
|span m=14.5
|span ft=47
|span in=6
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=46.4
|wing area sqft=500
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=2,950
|gross weight lb=6,500
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Liberty L-12|Liberty 12-N]]  water cooled V-12
|eng1 kw=300<!-- prop engines -->
|eng1 hp=400<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|perfhide=Y <!-- REMOVE THIS LINE IF PERFORMANCE FIGURES COME TO LIGHT -->
|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Bibliography===
{{reflist}}

===Note===
{{commons category}}
{{refbegin}}
*{{cite book |title= Handley Page Aircraft since 1907|last= Barnes|first=C.H. |last2 =James|first2= D. N.|year=1987 |publisher=Putnam Publishing  |location=London |isbn= 0-85177-803-8|ref=harv}}
*{{cite book |title= de Havilland Aircraft since 1909|last= Jackson|first=A.J.| year=1978 |publisher=Putnam Publishing |location=London |isbn=0-370-30022-X|ref=harv }} 
{{refend}}

<!-- ==External links== -->
{{Handley Page aircraft}}

[[Category:British experimental aircraft 1920–1929]]
[[Category:Handley Page aircraft|H.P.20]]