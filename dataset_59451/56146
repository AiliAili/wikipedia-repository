{{italic title}}
[[File:Cassinia (1916) (20352169000).jpg|thumb|381x381px|The original cover design, pictured here, was designed by Alfred Morton Githens in 1901.]]
'''''Cassinia''''' is the [[Academic journal|journal]] of the [[Delaware Valley Ornithological Club]] (DVOC). The journal is composed of papers relating to the ornithology of eastern [[Pennsylvania]], [[New Jersey]], and [[Delaware]]. Seventy three issues have been published to date. Five initial volumes (1890–1900) were published under the name ''Abstract of the Proceedings of the Delaware Valley Ornithological Club'', and then in 1901 the journal was renamed in honor of ornithologist [[John Cassin]].<ref>{{Cite journal|last=Stone|first=Witmer|year=1901|title=John Cassin|journal=Cassinia|volume=5|pages=1–7}}</ref>

== Editorial History ==
There have been 21 different editors of ''Cassinia'' since its inception:<ref>{{Cite web|url=http://www.dvoc.org/Publications/Cassina/Cassinia.htm|title=Table of Cassinia Issues & Editors|last=|first=|date=|website=|publisher=|access-date=31 October 2016}}</ref>
# [[Witmer Stone]] — Vols. 1–14, 1890–1910
# Robert T. Moore — Vols. 15–19, 1911–1915
# Spencer Trotter — Vols. 20–24, 1916–1921
# Wharton Huber — Vol. 25: 1922, 1924
# Philip A. Livingston — Vols. 26–27, 1925–1928; and Vol. 31, 1938–1941
# J. Fletcher Street — Vols. 28–29, 1929–1932
# C. Brooke Worth / [[Witmer Stone]] — Vol. 30, 1933–1937
# Ernest A. Choate — Vols. 32–37, 1942–1948
# C. Chandler Ross — Vols. 38–42, 1949–1954
# Lester S. Thomas — Vols. 43–45, 1958–1960
# James K. Meritt — Vols. 46–49, 1961–1966; and Vols. 51–55, 1968–1975
# Albert Conway — Vol. 50, 1966–1967
# Keith C. Richards — Vol. 56, 1976
# Richard C. Bell — Vols. 57–59, 1977–1981
# Edward D. Fingerhood — Vols. 60–62, 1982–1987
# Franklin C. Haas — Vols. 63–66, 1988–1995
# Sandra L. Sherman — Vols. 67–68, 1996–1999
# Colin Campbell — Vol. 69, 2000–2001
# F. Arthur McMorris — Vols. 70–71, 2002–2005
# Dave B. Long — Vols. 72/73–74/75, 2006–2015
# Matthew R. Halley — present

==External links==
*[http://www.dvoc.org/Publications/Cassina/Cassinia.htm ''Cassinia'' Web site]
*[http://www.dvoc.org DVOC Web site]

== References ==
<references />

[[Category:Journals and magazines relating to birding and ornithology]]
[[Category:Academic journals published in the United States]]


{{zoo-journal-stub}}