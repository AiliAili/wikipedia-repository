{{good article}}
{{Use dmy dates|date=July 2014}}
{{Infobox officeholder
| birth_name = 
| image = Joseph Berchtold.jpg
| imagesize = 215px
| smallimage = 
| caption = Berchtold in his SA uniform
| order = ''[[Reichsführer-SS]]''
| term_start = 15 April 1926
| term_end = 1 March 1927
| leader = [[Adolf Hitler]]
| predecessor = [[Julius Schreck]]
| successor = [[Erhard Heiden]]
| birth_date = 16 March 1897
| birth_place = [[Ingolstadt]], [[German Empire]]
| death_date = {{death date and age|1962|8|23|1897|3|16|df=y}}
| death_place = [[Herrsching]], [[West Germany]]
| party = [[National Socialist German Workers' Party]] (Nazi Party; NSDAP)
| religion = [[Catholic Church|Catholic]]
| signature = 
| footnotes =
| allegiance = {{flag|German Empire}}
| branch = [[German Army (German Empire)|Army]]
| unit = 
| serviceyears =
| rank = 
| battles = [[World War I]]
}}

'''Joseph Berchtold''' (6 March 1897 – 23 August 1962) was an early senior [[Nazi Party]] member and a co-founder of both the ''[[Sturmabteilung]]'' (SA) and ''[[Schutzstaffel]]'' (SS).

Berchtold served in [[World War I]] and upon Germany's defeat joined the [[German Workers' Party]] (DAP), a small extremist organization at the time. He remained in the party after it became known as the National Socialist German Workers' Party (Nazi Party; NSDAP) and went on to become the second commander of the ''Schutzstaffel'' (SS) from April 1926 to March 1927.

After resigning as the SS leader, Berchtold spent much of his time writing for Nazi magazines and journals. He survived the war, but was arrested by the Allies. Berchtold was later released and died in 1962. He was the last surviving person to hold the rank of ''[[Reichsführer-SS]]'' and the only one to survive under it during the [[Second World War]].

==Early life==
Born on 6 March 1897 in [[Ingolstadt]], Berchtold attended school in [[Munich]] from 1903 to 1915. He went on to serve in the Royal Bavarian Army during [[World War I]] (1914-18) and held the rank of [[second lieutenant]] at the end of the war. After the war, he studied economics at the University of Munich and gained employment as a journalist.{{sfn|Miller|2006|p=92}} In early 1920, he joined the small right-wing extremist group the [[German Workers' Party]] (DAP). He remained in the party after it became known as the [[National Socialist German Workers' Party]] (Nazi Party; NSDAP).{{sfn|McNab|2009|pp=8, 9, 11}} Berchtold became the treasurer of the Nazi Party, until he resigned at the end of July, 1921.{{sfn|Miller|2006|p=93}}

==SA Career==
[[File:Bundesarchiv Bild 119-1486, Hitler-Putsch, München, Marienplatz.jpg|thumb|left|280px|SA men taking part in the attempted [[coup d'état]] in Munich, 1923]]
Upon re-joining the party in 1922, Berchtold became a member of the ''[[Sturmabteilung]]'' ("Storm Detachment"; SA), a paramilitary wing formed to protect its speakers at rallies, and to police Nazi meetings.{{sfn|Miller|2006|p=93}} [[Adolf Hitler]], leader of the party since 1921, ordered the formation of a small separate bodyguard dedicated to his protection only instead of a suspected mass of the party in 1923.{{sfn|McNab|2009|pp=14, 16}} Originally the unit was composed of only eight men, commanded by [[Julius Schreck]] and Berchtold.{{sfn|Weale|2010|p=16}} It was designated the ''Stabswache'' ("Staff Guard"). Later that year, the unit was renamed ''[[Stoßtrupp-Hitler]]'' ("Shock Troop-Hitler").{{sfn|McNab|2009|pp=14, 16}}

On 9 November 1923 the ''Stoßtrupp'', along with the SA and several other paramilitary units, took part in what would become known as the [[Beer Hall Putsch]]. The plan was to take control of [[Munich]] and then seize total power in [[Berlin]]. The [[coup d'état]] failed and resulted in the death of 16 Nazi supporters and 4 police officers. In the aftermath of the putsch both Hitler and other Nazi leaders were incarcerated at [[Landsberg Prison]].{{sfn|Hamilton|1984|p=172}} The Nazi Party and all associated formations, including the ''Stoßtrupp'', were officially disbanded.{{sfn|Wegner|1990|p=62}} Berchtold then left Germany and fled to Tirol, [[Austria]]. Berchtold was tried in absentia before the special [[People's Court (Bavaria)|People's Court]] in Munich in 1924 for his role in the Beer Hall Putsch and sentenced to a prison term. During his time in Austria, Berchtold continued to be involved with Nazi Party activities, even though it was illegal.{{sfn|Miller|2006|p=93}}

When Hitler was released from prison on 20 December 1924, Berchtold was District Director of the Nazi Party in [[Carinthia]], Austria and was leader of the SA there.{{sfn|Miller|2006|p=93}} After the re-formation of the Nazi Party on 20 February 1925, he again joined the party, documented as member #964. In March 1926, Berchtold returned to Munich from Austria.{{sfn|Weale|2010|pp=29, 30}} He became chief of the SA in Munich.{{sfn|Miller|2006|p=93}}

==SS Career==
On 15 April 1926, Berchtold became the successor to Schreck as chief of the ''[[Schutzstaffel]]'' ("Protection Squadron"; SS), a special elite branch of the party under the control of the SA. Berchtold changed the title of the office position which became known as the ''Reichsführer-SS''.{{sfn|Weale|2010|p=30}} He issued new rules to establish the position of the SS. The rules stated the unit was "...neither a military organisation nor a group of hangers-on, but a small squad of men that our movement and our Führer can rely on."{{sfn|Weale|2010|p=30}} He further stressed that the men must follow "only party discipline".{{sfn|Weale|2010|p=30}} He was considered to be more dynamic than his predecessor, but was still unable to keep the party organizers at bay. He was frustrated in his efforts to have a more independent unit and became disillusioned by the SA's authority over the SS.{{sfn|Weale|2010|p=32}} On 1 March 1927, he handed over leadership of the SS to his deputy [[Erhard Heiden]].{{sfn|Cook|Russell|2000|pp=21-22}}

==After the SS==
In 1927, he became a lead writer for ''[[Völkischer Beobachter]]'', the Nazi Party newspaper. From 1928 to 1945, Berchtold was an SA leader on the staff of the Supreme SA leadership (OSAF). In 1934, he became the permanent deputy editor-in-chief of the ''Völkischer Beobachter'' newspaper. In the following years, he operated primarily as a journalist and propagandist. In 1928, Berchtold founded the newspaper ''SA-Mann'' ("SA Man"). Until January 1938, he was the main writer of the paper, which was published by the OSAF. Berchtold was also the author of various Nazi publications and staff of additional magazines.{{sfn|Miller|2006|p=94}}

Additional posts in [[Nazi Germany]] were of secondary importance to Berchtold. From March 1934 to the end of the war, Berchtold was city councilman of the town council in Munich. On 15 November 1935, Berchtold was appointed to the Reich Culture Senator. Furthermore, he belonged to the "Cultural Circle of the SA" since 6 March 1936. He belonged to the [[Reichstag (Nazi Germany)|Reichstag]] from 29 March 1936, forward. From 29 April 1940, Berchtold served as a captain of the reserve on a temporary basis in the [[Wehrmacht]].{{sfn|Miller|2006|pp=92, 94}}

==Post-war==
After [[World War II]] in [[Europe]] ended in early May 1945, Berchtold was temporarily in [[Allies of World War II|Allied]] detention. He was released and later died on 23 August 1962.{{sfn|Miller|2006|pp=92, 94}}

==SA promotions==
{| class="wikitable float-right"
|- bgcolor="silver"
!colspan = "2"|Berchtold's SA Ranks{{sfn|Miller|2006|p=92}}
|- bgcolor="silver"
!Date
!Rank
|-
|18 December 1931
|SA-''Standartenführer''
|-
|1 January 1933
|SA-''Oberführer''
|-
|9 November 1934
|SA-''Brigadeführer''
|-
|1 May 1937
|SA-''Gruppenführer''
|-
|30 January 1942
|SA-''Obergruppenführer''
|}

== Awards and decorations ==
*[[Iron Cross]] (1914) 2nd Class{{sfn|Miller|2006|p=94}}
*[[The Honour Cross of the World War 1914/1918]]{{sfn|Miller|2006|p=94}}
*[[Blood Order]]{{sfn|Miller|2006|p=94}}
*[[Honour Chevron for the Old Guard]]{{sfn|Miller|2006|p=94}}
*[[Golden Party Badge]]{{sfn|Miller|2006|p=94}}
*[[Nazi Party Long Service Award]] (bronze, silver, gold){{sfn|Miller|2006|p=94}}

==References==

===Citations===
{{reflist|30em}}

===Bibliography===
* {{cite book | author1-last = Cook | author1-first = Stephen | author2-last = Russell | author2-first = Stuart | title = Heinrich Himmler's Camelot: the Wewelsburg Ideological Center of the SS, 1934-1945 | year = 2000 | publisher = Kressmann-Backmeyer | isbn = 978-0967044309 | ref = harv }}
* {{cite book | last = Hamilton | first = Charles | title = Leaders & Personalities of the Third Reich, Vol. 1 | year = 1984 | publisher = R. James Bender Publishing | isbn = 0-912138-27-0 | ref = harv }}
* {{cite book | last = McNab | first = Chris | title = The SS: 1923–1945 | publisher = Amber Books Ltd | year = 2009 | isbn = 978-1-906626-49-5 | ref=harv }}
* {{cite book | last = Miller | first = Michael | year = 2006 | title = Leaders of the SS and German Police, Vol. 1 | publisher = R. James Bender | location = San Jose, CA | isbn = 978-93-297-0037-2 | ref = harv }}
* {{cite book | last = Weale | first = Adrian | title = The SS: A New History | year = 2010 | publisher = Little, Brown | location = London | isbn = 978-1408703045 | ref = harv }}
* {{cite book | last = Wegner | first = Bernd | authorlink = Bernd Wegner | title = The Waffen-SS: Organization, Ideology and Function | publisher = Blackwell | year = 1990 | isbn = 0-631-14073-5 | ref = harv }}

{{S-start}}
{{S-gov}}
{{Succession box | before = [[Julius Schreck]]| title = [[Reichsführer SS|Reich Leader of the SS]]|years=1926&ndash;1927| after = [[Erhard Heiden]]}}
{{S-end}}

{{Authority control}}

{{DEFAULTSORT:Berchtold, Joseph}}
[[Category:1897 births]]
[[Category:1962 deaths]]
[[Category:German military personnel of World War I]]
[[Category:Nazis who served in World War I]]
[[Category:Nazi leaders]]
[[Category:Nazis who participated in the Beer Hall Putsch]]
[[Category:People from Ingolstadt]]
[[Category:Recipients of the Blood Order]]
[[Category:Reichsführer-SS]]
[[Category:Sturmabteilung officers]]
[[Category:Members of the Reichstag of Nazi Germany]]
[[Category:People from the Kingdom of Bavaria]]
[[Category:Military personnel of Bavaria]]