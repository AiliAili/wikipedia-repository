{{Use dmy dates|date=December 2015}}
{{Use Indian English|date=December 2015}}
{{Infobox journal
| title         = Aligarh Institute Gazette
| cover         =
| editor        = [[Syed Ahmed Khan]]
| discipline    =
| peer-reviewed =
| language      = [[Urdu]], English
| former_names  =
| abbreviation  =
| publisher     = Syed Ahmed Khan
| country       = India
| frequency     = Biweekly
| history       =
| openaccess    =
| license       =
| impact        =
| impact-year   =
| website       =
| link1         =
| link1-name    =
| link2         =
| link2-name    =
| JSTOR         =
| OCLC          =
| LCCN          =
| CODEN         =
| boxwidth      =
}}

The '''''Aligarh Institute Gazette''''' ([[Urdu]]: {{Nastaliq|اخبار سائنٹیفک سوسائٹی}})<ref>{{cite book|title=Cementing Ethics with Modernism: An Appraisal of Sir Sayyed Ahmed Khan's Writings|url=https://books.google.com/books?id=kmYjnGDHPFwC&pg=PA99|year=2010|publisher=Gyan Publishing House|isbn=978-81-212-1047-8|pages=99–}}</ref> was the first multilingual journal of India, introduced, edited, and published in 1866 by [[Sir Syed Ahmed Khan]] <ref name="Muhammad1969">{{cite book|author=Shan Muhammad|title=Sir Syed Ahmad Khan: A Political Biography|url=https://books.google.com/books?id=wYMeAAAAMAAJ|year=1969|publisher=Meenaksi Parkashan|pages=172–|quote=In the copies of The Aligarh Institute Gazette from its start to October 1897 the name of Sir Syed Ahmad Khan Bahadur is clearly mentioned as the Editor.101 Copies of the journal from November 1897 to 1899 are not available even at Aligarh.}}</ref><ref>{{cite book|author1=Asghar Abbas|title=PRINT CULTURE- Sir Syed's Aligarh Institute Gazette, 1866–97|publisher=Primus Books|isbn=978-93-84082-29-1|edition=1st|}}</ref> which was read widely across the country.<ref>{{cite web|title=Write back!|url=http://www.thehindu.com/features/friday-review/write-back/article7561258.ece|website=The Hindu|publisher=The Hindu|accessdate=4 October 2015}}</ref> [[Theodore Beck]] later became its editor.<ref name="Sen2010">{{cite book|author=Sailendra Nath Sen|title=An Advanced History of Modern India|url=https://books.google.com/books?id=bXWiACEwPR8C&pg=RA1-PA1876|year=2010|publisher=Macmillan India|isbn=978-0-230-32885-3|pages=1–}}</ref>

== History ==

In 1866, a building named the Aligarh Institute was erected for the [[Scientific Society of Aligarh]], which launched its journal ''Aligarh Institute Gazette'' in the same year.<ref name="Moj2015">{{cite book|author=Muhammad Moj|title=The Deoband Madrassah Movement: Countercultural Trends and Tendencies|url=https://books.google.com/books?id=TLi2BgAAQBAJ&pg=PA47|date=1 March 2015|publisher=Anthem Press|isbn=978-1-78308-388-6|pages=47–}}</ref>  A joint mouthpiece of the Scientific Society and the Institute the journal came into weekly circulation from 30 March 1866 with the slogan "To permit the liberty of the Press is the part of a wise Government; to preserve it is the part of a free people."<ref name="Zakaria1986">{{cite book|author=Rafiq Zakaria|title=Rise of Muslims in Indian politics: an analysis of developments from 1885 to 1906|url=https://books.google.com/books?id=JNq1AAAAIAAJ|year=1986|publisher=Somaiya Publications|quote=On his transfer to Aligarh, it was considerably expanded and renamed the Scientific Society; its organ the famous Aligarh Institute Gazette was started on March 30, 1866. The same year he also formed the British Indian Association with the ...}}</ref><ref>{{cite book|title=Fundamenta Scientiae|url=https://books.google.com/books?id=0eMoAQAAIAAJ|volume=6|year=1985|publisher=Pergamon Press|pages=306–|quote=... policy of the cultivation of Western arts and sciences through the medium of the local languages the Society started a weekly newspaper the Aligarh Institute Gazette on 30 March 1866 with a slogan "to permit the liberty of the press is the part ...}}</ref> Part of the  ''Gazette's'' contents were printed in Urdu alone, part in English, and a portion also in both languages.<ref name="Atkinson1875">{{cite book|author=Edward Atkinson|title=DESCRIPTIVE AND HISTORICAL  ACCOUNT OF THE ALIGARH DISTRICT|url=https://books.google.com/books?id=O54IAAAAQAAJ|year=1875|pages=403–|quote=In connection with the society, a newspaper called the Aligarh Institute Gazette is published. Part of its contents are printed in Urdu alone, part in English, and a portion also in both languages. It consists largely of extracts from the English ...}}</ref> The editorial team was composed of Sir Syed Ahmad Khan as Honorary Editor, Munshi Mohammad Yaar Khan as Editor, Munshi Chaukhan Lal as translator, Babu Durga Prashad as translator and Shaikh Fida Ali as Librarian and distribution incharge.<ref name="Muhammad2002">{{cite book|author=Shan Muhammad|title=Education and Politics: From Sir Syed to the Present Day : the Aligarh School|url=https://books.google.com/books?id=Q7BCcgC0kvsC&pg=PA10|year=2002|publisher=APH Publishing|isbn=978-81-7648-275-2|pages=10–|quote= the Aligarh School Shan Muhammad ... his 'School' Syed started in 1866 the Aligarh Institute Gazette, a bi-weekly in two columns. ... Head Master English School, Aligarh and Honorary Secretary of the Society while the Vernacular columns were edited by Munshi Mohammad Yar Khan.}}</ref> The ''Aligarh Institute Gazette'' had a circulation of four hundred but played a key role in mobilizing the people and convey the message of the Society.<ref name="HoltLambton1977">{{cite book|author1=P. M. Holt|author2=Ann K. S. Lambton|author3=Bernard Lewis|title=The Cambridge History of Islam:|url=https://books.google.com/books?id=y99jTbxNbSAC&pg=PA85|date=21 April 1977|publisher=Cambridge University Press|isbn=978-0-521-29137-8|pages=85–|quote=... to found a separate Mohammedan Anglo-Oriental Defence Association of Upper India to mobilize Muslim public opinion ... His 'Aligarh Institute Gazette had a circulation of no more than four hundred. ... In collaboration with the district officers, they played a leading role in the establishment of the Board of Trustees and in ...}}</ref><ref name="Pirzada2007">{{cite book|author=Syed Sharifuddin Pirzada|title=Foundations of Pakistan: 1906-1924|url=https://books.google.com/books?id=KiVuAAAAMAAJ|year=2007|publisher=National Institute of Historical and Cultural Research Center of Excellence, Quaid-i-Azam University|isbn=978-969-415-078-9|page=xxxii|quote= And in 1903, the Aligarh Institute Gazette took up the task of mobilizing support for the proposal. The Gazette wrote that the Musalmans of India, on account of their religious unity, were fit to become a nation,2 and ...}}</ref>

In 1871 [[William Wilson Hunter]], a British civil servant in Bengal, published his famous book ''The Indian Mussalmans'', in which he raised questions about the loyalty of the Muslims to the British government and referred to the earlier military campaigns of Sayyid Ahmad Barelvi to establish Muslim rule. Sir Syed's reaction to this book is described as follows:

"Sir Syed Ahmad Khan took Hunter's book very seriously, and vehemently criticized its contents by publishing a review on it. In the review he tried to argue that the jihad movement of Sayyid Ahmad [Barelvi] and his followers was directed solely against the Sikh rule in the Punjab and that it had nothing to do with the British government in India. He adopted an apologetic tone to convince the British authorities that the Indian Muslims, including the followers of Sayyid Ahmad [Barelvi], were not opposed to the British rule.
'The articles of Sir Syed Ahmad Khan, which were published in The Pioneer and in the ''Aligarh Institute Gazette'', refuted William Hunter's ideas alleging that there was a widespread conspiracy among the Indian Muslims. He tried to convince the English readers and the British authorities that the accounts of William Hunter about the followers of Sayyid Ahmad and their jihad movement were not based on facts.'<ref>http://www.ahmadiyya.org/allegs/hamdard.htm</ref>

By 1876 the paper was jubilantly reporting the changing spirit of times.<ref>{{cite book|author1=Ayesha Jalal|title=Self and Sovereignty: Individual and Community in South Asian Islam Since 1850|publisher=Routledge|location=Great Britain}}</ref>

Old archives (till 1944) of the ''Gazette'' can be found at [[Nehru Memorial Museum & Library]].<ref>http://nehrumemorial.nic.in/images/pdf/library/List%20of%20%20holdings%20on%20Microfilm.pdf</ref>

== See also ==
* [[Tehzeeb-ul-Akhlaq]]
* [[Aligarh Muslim University]]

== References ==
{{reflist|colwidth=30em}}

=== Further reading ===
* {{cite book
|author1=Asghar Abbas
|author2=Syed Asim Ali
|title=Print Culture: Sir Syed's Aligarh Institute Gazette 1866-1897
|url=https://books.google.com/books?id=xHT-rQEACAAJ
|date=10 July 2015
|publisher=Primus Books
|isbn=978-93-84082-29-1}} {{Small|This book is based on a critical study of The Aligarh Institute Gazette covering the period 1866 97, a phase when India was slowly transiting to the modern age, with the spread of new political, social, educational and religious ideas. Numerous social movements too, were gathering steam during this period to reform the Indian society. Sir Syed Ahmed Khan, the founder of The Aligarh Institute Gazette, fought against obscurantist ideas and persuaded the Indian people to accept the impending changes.}}

== External links ==
* http://www.outlookindia.com/article/a-musafir-to-london/278673
* http://www.bzu.edu.pk/PJSS/Vol30No12010/Final_PJSS-30-1-07.pdf
* http://www.newindianexpress.com/columns/Sir-Syeds-message-of-reform-lost/2013/10/23/article1849811.ece
* http://www.asiamap.ac.uk/newspapers/display.php?ID=486&Query=..&Order=0&pp=10&View=0&RecView=1&HoldingsID=198
* http://www.frontline.in/static/html/fl1707/17070780.htm
* http://www.termpaperwarehouse.com/essay-on/History-Of-Press-In-Sub-Continent/154060
* http://www.ampltd.co.uk/digital_guides/indian_newspaper_reports_parts_1_to_4/Detailed-Listing-Part-3.aspx

{{Aligarh Muslim University}}

[[Category:Works by Syed Ahmed Khan]]
[[Category:Aligarh Muslim University]]
[[Category:Urdu journals]]