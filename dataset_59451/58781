{{Infobox journal
| title        = Literature and Medicine
| cover        = [[Image:Literature and medicine.gif]]
| editor       = Catherine Belling
| discipline   = [[Literature]], [[Medicine]]
| language     = English
| abbreviation = Lit. Med.
| publisher    = [[Johns Hopkins University Press]]
| country      = United States
| frequency    = Biannually
| history      = 1982-present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = http://www.press.jhu.edu/journals/literature_and_medicine/
| link1        = http://muse.jhu.edu/journals/literature_and_medicine/
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 31871349
| LCCN         = 
| CODEN        = 
| ISSN         = 0278-9671
| eISSN        = 1080-6571
}}
'''''Literature and Medicine''''' is an [[academic journal]] founded in 1982. It is devoted to researching and understanding the interfaces between literary and medical knowledge. Literary and cultural texts are used to examine concerns related to illness, trauma, the body, and other medical issues. Articles are provided by experts in a variety of fields in both medicine and the [[humanities]] and [[social sciences]]. There are two issues each year, one general, one thematic. 

The journal is published biannually in May and November by the [[Johns Hopkins University Press]]. Circulation is 497 and the average length of an issue is 164 pages.

==See also==
*[[Medical humanities]]

==External links==
*[http://www.press.jhu.edu/journals/literature_and_medicine/ Official website]
*[http://muse.jhu.edu/journals/literature_and_medicine/  ''Literature and Medicine'' ] at [[Project MUSE]]

[[Category:American literary magazines]]
[[Category:Publications established in 1982]]
[[Category:General medical journals]]
[[Category:Biannual journals]]
[[Category:English-language journals]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:1982 establishments in Maryland]]