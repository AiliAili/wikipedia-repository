{{Infobox musical artist
| name            = Parker Ighile
| birth_name      = Peter Ibrahim Ighile
| image           =<!-- Deleted image removed: [[File:Photo for Billboard.jpg|thumb|right]] -->
| caption         = 
| background      = solo_singer
| birth_place     = [[Pinner]], [[London]], [[England]]
| origin          = 
| genre           = [[Contemporary R&B|R&B]], [[Pop music|pop]], [[Hip hop music|hip hop]], [[Grime (music)|grime]], [[alternative hip hop]]
| occupation      = Producer, composer, singer, songwriter, rapper
| years_active    = 2009–present
| label           =  Pink Friday Records
| associated_acts = [[Chipmunk (rapper)|Chipmunk]], [[Nicki Minaj]]
| website         = 
}}

'''Parker Ibrahim Ighile''' (born March 1, 1990), better known as '''Parker Ighile''', is a [[United Kingdom|British]] producer, rapper, singer and songwriter. He was born in [[London]], England. He is currently signed to Trinidadian musician [[Nicki Minaj]]'s record label.<ref>{{cite web
| url        = http://www.mtvbase.com/news/nicki-minaj-signs-naija-artist/
| title      = Parker is Nicki's sole signing 
| date       = {{date|14 nov 2012}}
| accessdate = {{date|15 mar 2013}}
}}</ref> As a producer, artist and songwriter he has worked with [[Quincy Jones]], [[G-Eazy]], [[Nicki Minaj]], [[Rihanna]], [[Jessie J]], [[Ariana Grande]], [[Grace (Australian singer)|Grace]], [[Delta Goodrem]], [[Rita Ora]], [[N-Dubz]], [[Chipmunk (rapper)|Chipmunk]], and [[Livvi Franc]]. At the age of 18, he started his professional music career, producing [[Chipmunk (rapper)|Chipmunk]]'s song "[[Oopsy Daisy]]", which reached number one in the [[UK]] Charts.<ref name="digitalspy">{{cite web
| url        = http://www.digitalspy.com/music/news/a181559/chipmunk-beats-the-saturdays-to-no1.html
| title      = Chipmunk beats The Saturdays to No.1
| date       = {{date|11 oct 2009}}
| accessdate = {{date|15 mar 2013}}
}}</ref>

==Life and career==

===Early life and career beginnings===
Parker Ighile was born in London where he lived with his mother and sister Asabe Ighile. His father and mother are Nigerian, from [[Benin City|Benin]] and [[Kaduna]], respectively. He began composing and producing at the age of 11 when he and numerous church friends formed a [[rap]] group. In his teens made [[Grime (music)|grime music]] under the name Piztol.<ref>http://bntl.co.uk/a-twist-of-fate/</ref> The church his family attended was instrumental in Parker's early musical ventures and he studied music in college and university where he began learning [[music theory]]. Parker's music is influenced by a dynamic range of genres but more noticeably R&B, rap, garage and [[Bollywood]] music – due to frequent exposure to his grandmother’s diverse tastes in music and cinema.

At 18, Ighile produced & co-wrote his first of many major releases; [[Chipmunk (rapper)|Chipmunk]]'s first & number one single,<ref name="digitalspy" /> "[[Oopsy Daisy]]".<ref>{{cite web|url=http://www.bmgchrysalis.com/category/music/?artist=parker-james |title=Music &#124; BMG Chrysalis US |publisher=Bmgchrysalis.com |date=2013-06-16 |accessdate=2013-08-02}}</ref> Parker produced and co-wrote seven songs in total on the ''[[I Am Chipmunk]]'' album which peaked at number two in the UK album chart and was [[Music recording sales certification|Certified Gold]]. He went on to write with [[The Invisible Men]] and [[Jessie J]] and co-wrote her [[debut single]], "[[Do It like a Dude]]" as well as "[[Who's Laughing Now (song)|Who's Laughing Now]]". More credits include a song on [[N-Dubz]]'s ''[[Love.Live.Life]]'' album, which he cowrote with [[Ina Wroldsen]].
He used trans-genre influences to create a unique and dynamic sound and also produced [[Angel (British musician)|Angel]]'s "[[Wonderful (Angel song)|Wonderful]]", debuting at number 20 for the week ending July 21—reaching a peak on [[List of UK top 10 singles in 2012|top 10]] at number nine several weeks later;<ref>{{cite web|url=http://www.officialcharts.com/archive-chart/_/1/2012-09-01/ |title=2012-09-01 Top 40 Official UK Singles Archive |publisher=Official Charts |date=2012-09-01 |accessdate=2013-08-02}}</ref> and [[Conor Maynard]]'s "Vegas Girl".<ref>[[Vegas Girl]]</ref><ref>{{BillboardURLbyName|artist=conor maynard|chart=Heatseekers Songs}}</ref>

===2012-present: Record deal and ''Young, Dumb & 21''===

Ighile teamed up with music strategist Efe Ogbeni, who was heavily involved in the success of producer [[RedOne]], to help propel him further into the music business.<ref>{{cite web|url=http://www.hitquarters.com/index.php3?page=intrview/opar/intrview_RedOne_Interview.html |title=A&R, Record Label / Company, Music Publishing, Artist Manager and Music Industry Directory |publisher=Hitquarters.com |date= |accessdate=2013-08-02}}</ref> On 15 November 2012, Trinidadian rapper [[Nicki Minaj]] announced that Ighile was signed as the first official artist to her as of yet, unnamed label.<ref>[http://www.dailystar.co.uk/gossip/view/282868/Nicki-Minaj-launching-record-label/Nicki-Minaj-launching-record-label ]{{dead link|date=August 2013}}</ref> She has described his artistry as "lethal and dope".<ref>{{cite web|url=http://www.power1051fm.com/player/?mid=22651580 |title=Nicki Minaj Interview Pt VI |publisher=Power 105.1 FM |date= |accessdate=2013-08-02}}</ref> He has since produced Minaj's "Hell Yeah" and [[Rihanna]]'s "What Now". In late 2012, a song titled "So Beautiful" leaked on the internet, and Ighile announced some weeks later on Twitter that he was working on a mixtape, ''Young, Dumb & 21'', which is set to be released in early 2013 and that "So Beautiful" would act as the first single. Ighile's artistry is usually compared to [[Sting (musician)|Sting's]] and [[Fela|Fela's]]; and his production to that of [[Kanye West]].
On February 22, 2013, Parker released a music video for his mixtape's leading track, "So Beautiful", onto his official YouTube account. The video is set in black and white, which follows the story of a young couple. He would later release a remix to the song that features Nigerian musician MI Abaga.<ref>{{cite web 
| url        = http://www.rap-up.com/2013/02/22/video-parker-ighile-so-beautiful/
| work       = Rap-up
| title      = So Beautiful
| date       = {{date|22 feb 2013}}
| accessdate = {{date|15 mar 2013}}
}}</ref> 
Vibe Magazine<ref>{{cite web|url=http://www.vibe.com/article/v-playlist-juelz-santana-camron-2-pistols-x-french-montana-x-talib-kweli-and-more |title=V Playlist: Juelz Santana, Cam'Ron, 2 Pistols x French Montana x Talib Kweli, And More |publisher=Vibe |date=2013-01-10 |accessdate=2013-08-02}}</ref> describes Ighile's sound as Electro-Soul.<ref>{{cite web|url=http://singersroom.com/content/2013-01-10/Parker-So-Beautiful |title=Music &#124; Parker - So Beautiful |publisher=Singersroom |date=2013-01-10 |accessdate=2013-08-02}}</ref> Ighile defined his sound as Progressive Urban Pop, PUP, sometimes referred to as Progressive Afro Pop, P.A.P during an interview with [Billboard.com]. Speaking about "So Beautiful" Ighile tells Billboard.com, “This song is an introduction to my sound, Progressive Urban Pop (P.U.P) or Progressive Afro Pop (P.A.P).”<ref>{{cite web
| url        = http://www.billboard.com/articles/columns/the-juice/1550554/parker-ighile-first-signing-to-nicki-minajs-label-talks-so
| title      = Parker Ighile, First Signing to Nicki Minaj's Label, Talks 'So Beautiful’
| date       = {{date| 4 mar 2013}}
| accessdate = {{date|15 mar 2013}}
| author     = Lars Brandle
}}</ref>

===Partnership with Samsung Galaxy===

Parker Ighile, in collaboration with [[Quincy Jones]] and [[Samsung]], produced his own version of "Over the Horizon", the official song of the [[Samsung Galaxy]] products. The official song and video performance have been released on August 2013 on the [[Samsung Galaxy]] Music website.<ref>http://www.playgalaxymusic.com/spotlight/quincy.html#quincyLink</ref>

===Other Projects===

Among his projects, Parker Ighile is executive producing Australian 19-year-old sensation [[Grace (singer-songwriter)|Grace Sewell]].

During Summer 2013, Parker Ighile took part of the [[Quincy Jones]] 80th Birthday Celebration in Asia with concerts in Korea and Japan.

==Discography==

===Mixtapes===

* 20?? – ''Young, Dumb and 21''

===Singles===
====As lead artist====
{| class="wikitable"
|-
! Song !! Release date !! Music video
|-
| "So Beautiful" || late-2012 || [https://www.youtube.com/watch?v=5tSy7z-tj5U/ Yes]
|-
| "Heavens Playground" || late-2012/early-2013 || No
|-
| "Beneath The Silence" || mid-2013 || [https://www.youtube.com/watch?v=jotiwI1fsSI/ Yes] 
|-
| "The Opposition" || late-2013 || No
|-
| "This Is America" || mid-2014 || No
|}

===Other songs===
====As featured artist====
{| class="wikitable" 
|-
! Song !! Artist !! Album
|-
| "Hell Yeah" || [[Nicki Minaj]] <small>featuring Parker Ighile</small> || ''[[Pink Friday: Roman Reloaded – The Re-Up]]''
|}

===Writing and production credits===
{| class="wikitable sortable"
|-
! Title !! Year !! Artist(s) !! Album
|-
| "[[Oopsy Daisy]]" || rowspan="7"|<center>2009</center> || rowspan="2"|[[Chip (rapper)|Chipmunk]] || rowspan="5"|<center>''[[I Am Chipmunk]]''</center>
|-
| "Saviour"
|-
| "Lose My Life" || Chipmunk <small>featuring [[N-Dubz]]</small>
|-
| "Beast" || Chipmunk <small>featuring [[Loick Essien]]</small>
|-
| "Business" || Chipmunk <small>featuring Young Spray</small>
|-
| "Uh Ay" || Chipmunk || rowspan="2"|<center>''I Am Chipmunk''<br /><small>(Platinum Edition)</small></center>
|-
| "History" || Chipmunk <small>featuring [[Wretch 32]]</small>
|-
| "What If You Knew" || rowspan="4"|<center>2010</center> || rowspan="2"|[[Gabriella Cilmi]] || rowspan="2"|<center>''[[Ten (Gabriella Cilmi album)|Ten]]''</center>
|-
| "Invisible Girl"
|-
| "Love Sick" || [[N-Dubz]] <small>featuring Ny</small> || <center>''[[Love.Live.Life]]''</center>
|-
| "[[Do It like a Dude]]" || rowspan="2"|[[Jessie J]] || rowspan="2"|<center>''[[Who You Are (Jessie J album)|Who You Are]]''</center>
|-
| "[[Who's Laughing Now (song)|Who's Laughing Now]]" || rowspan="2"|<center>2011</center>
|-
| "Foul" || Chipmunk || <center>''[[Transition (Chipmunk album)|Transition]]''</center>
|-
| "[[Wonderful (Angel song)|Wonderful]]" || rowspan="6"|<center>2012</center> || [[Angel (British musician)|Angel]] || <center>''[[About Time (Angel album)|About Time]]''</center>
|-
| "[[Vegas Girl]]" || [[Conor Maynard]] || <center>''[[Contrast (Conor Maynard album)|Contrast]]''</center>
|-
| "So Beautiful" || Parker Ighile || <center>''Young, Dumb & 21''</center>
|-
| "[[What Now (song)|What Now]]" || [[Rihanna]] || <center>''[[Unapologetic]]''</center>
|-
| "Hell Yeah" || [[Nicki Minaj]] <small>featuring Parker Ighile</small> || <center>''[[Pink Friday: Roman Reloaded – The Re-Up]]''</center>
|-
| "Heaven's Playground" || Parker Ighile || <center>''Young, Dumb & 21''</center>
|-
| "[[The World (Angel song)|The World]]" || rowspan="5"|<center>2013</center> || [[Angel (British musician)|Angel]] || <center>''About Time''</center>
|-
| "Piano" || [[Ariana Grande]] || <center>[[Yours Truly (Ariana Grande album)|Yours Truly]]</center>
|-
| "Heart Hypnotic" || [[Delta Goodrem]] || <center>TBA</center>
|-
| "Beneath The Silence" || Parker Ighile || <center>TBA</center>
|-
| "The Opposition" || Parker Ighile ||<center>''Young, Dumb & 21''</center>
|-
| "Us" || rowspan="3"|<center>2014</center> || [[Angel (British musician)|Angel]] || <center>TBA</center>
|- 
| "This Is America || Parker Ighile <small>featuring [[G-Eazy]]</small> || <center>''Young, Dumb & 21''</center>
|-
| "Four Door Aventador" || [[Nicki Minaj]] ||  <center> [[The Pinkprint]] </center>
|-
| "You Don't Own Me" || rowspan="1"|<center>2015</center> || [[Grace (Australian singer)|Grace]] <small>featuring [[G-Eazy]]</small> || <center> Memo  </center>
|}

===Remixes===
{| class="wikitable sortable"
|-
! Title !! Year !! Artist(s) !! Album
|-
| "[[This Girl (Stafford Brothers and Eva Simons song)|This Girl]]" <small>(Parker Ighile House Of Hausa Mix)</small> || <center>2014</center> || [[Stafford Brothers]] <small>featuring [[Eva Simons]] and [[T.I.]]</small> || <center>Non-album single</center>
|}

==References==

{{reflist}}

{{Nicki Minaj}}

{{DEFAULTSORT:Ighile, Parker}}
[[Category:1990 births]]
[[Category:Living people]]
[[Category:Nicki Minaj]]
[[Category:People from Pinner]]
[[Category:British record producers]]