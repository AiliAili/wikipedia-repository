{{Use dmy dates|date=August 2015}}
{{Use British English|date=August 2015}}
{{Infobox military person
| name          = James Thomas Duane Ashworth
| image         = 
| image_size    = 
| alt           = 
| caption       = Lance Corporal James Ashworth VC
| birth_date    = 26 May 1989
| death_date    = {{Death date and age|2012|06|13|1989|05|26|df=yes}}
| birth_place   = 
| death_place   = [[Nahri Saraj District|Nahr-e Saraj District]], [[Helmand Province]], Afghanistan
| placeofburial = [[Corby]], Northamptonshire
| nickname      = 
| allegiance    = United Kingdom
| branch        = [[British Army]]
| serviceyears  = 2006–12{{KIA}}
| rank          = [[Lance Corporal]]
| servicenumber = <!--Do not use data from primary sources such as service records.-->
| unit          = [[Grenadier Guards]]
| battles       = [[War in Afghanistan (2001–14)|War in Afghanistan]]
*[[Operation Herrick]]
| awards        = [[Victoria Cross]]
| relations     =
}}
'''James Thomas Duane Ashworth''', [[Victoria Cross|VC]]  (26 May 1989 – 13 June 2012) was a British soldier and posthumous recipient of the [[Victoria Cross]], the highest award for gallantry in the face of the enemy that can be awarded to British and Commonwealth forces. He was killed in [[War in Afghanistan (2001–14)|Afghanistan]] on 13 June 2012 as he led his [[fire team]] in an attack on an enemy-held compound. The award was [[London Gazette|gazetted]] on 22 March 2013,<ref name="LG">{{London Gazette| issue=60455| supp=yes| date=22 March 2013| startpage=5735| endpage=5736| accessdate=25 October 2014}}</ref> having been confirmed by the [[British Army]] earlier in the week.<ref>{{cite web|title=VC for heroic Lance Corporal|url=http://www.army.mod.uk/news/24844.aspx|publisher=British Army|date=18 March 2013|accessdate=18 March 2013}}</ref> Ashworth is the 14th recipient of the award since the end of the [[Second World War]].

==Early life==
Ashworth lived and grew up in [[Corby]], Northamptonshire, where he attended [[Lodge Park Technology College]]. A keen sportsman, he represented his school at both football and basketball.

In 2006, aged 17, Ashworth joined the [[British Army]] following his father who had previously served in the [[Grenadier Guards]].<ref name=telegraph>{{cite news|last=Silverman|first=Rosa|url=http://www.telegraph.co.uk/news/uknews/defence/9934510/British-soldier-to-receive-posthumous-VC-for-bravery-in-Afghanistan.html |title=British soldier to receive posthumous VC for bravery in Afghanistan |newspaper=The Daily Telegraph|date=16 March 2013 |accessdate=16 March 2013}}</ref><ref name=dailymail2012>{{cite news|url=http://www.dailymail.co.uk/news/article-2159921/Lance-Corporal-James-Ashworth-named-Ministry-Defence-dead-British-soldier-killed-Afghanistan.html |title=Lance Corporal James Ashworth named by Ministry of Defence as dead British soldier as another is killed in Afghanistan |newspaper=Daily Mail |date=15 June 2012 |accessdate=16 March 2013}}</ref> Ashworth trained at the [[Infantry Training Centre (British Army)|Infantry Training Centre]] in [[Catterick Garrison|Catterick]] before being posted to Nijmegen Company Grenadier Guards, which is focused on public duties and state ceremonial events in London.<ref name=mod>{{cite web|title=Lance Corporal James Ashworth killed in Afghanistan|url=https://www.gov.uk/government/news/lance-corporal-james-ashworth-killed-in-afghanistan|publisher=Ministry of Defence|date=15 June 2012|accessdate=17 March 2013}}</ref>

He was identified as being capable of becoming a [[paratrooper]] and was assigned to the Guards' Parachute Platoon, which is part of [[3rd Battalion, Parachute Regiment]]. In his three years in the platoon, he took part in [[Operation Herrick]] 8 and was deployed to exercises overseas on three occasions. He was deployed to [[Canada]] before joining the Reconnaissance Platoon for Operation Herrick 16.<ref name=mod/>

==Death==
[[File:Victoria Cross Medal Ribbon & Bar.png|thumbnail|left|Victoria Cross|185px]]
{{Quote box
 |quote  = Despite the ferocity of the insurgent's resistance, Ashworth refused to be beaten. His total disregard for his own safety in ensuring that the last grenade was posted accurately was the gallant last action of a soldier who had willingly placed himself in the line of fire on numerous occasions earlier in the attack. This supremely courageous and inspiring action deserves the highest recognition.
 |source = Victoria Cross citation for James Ashworth VC<ref>{{cite news|title=Parents of Lance Corporal James Ashworth pay tribute to Victoria Cross hero|url=http://www.telegraph.co.uk/news/newsvideo/9937973/Parents-of-Lance-Corporal-James-Ashworth-pay-tribute-to-Victoria-Cross-hero.html|accessdate=18 March 2013|newspaper=The Daily Telegraph|date=18 March 2013}}</ref>
|width  = 33%
|align  = right
}}
On 13 June 2012, Ashworth was serving as part of the Reconnaissance Platoon, 1st Battalion Grenadier Guards. He was on a patrol in the [[Nahri Saraj District]] of [[Helmand Province]], [[Afghanistan]]. He was leading a [[fire-team]], clearing out compounds,<ref name=bbcnews>{{cite news|title=Victoria Cross for Afghan hero L/Cpl James Ashworth|url=http://www.bbc.co.uk/news/uk-21810581|accessdate=17 March 2013|newspaper=BBC News|date=16 March 2013}}</ref> when his team came under fire from [[Taliban]] armed with rifles and [[rocket-propelled grenades]] from several mud huts. Ashworth charged the huts, providing cover for his team who followed in single file behind him.<ref name=mailonsunday>{{cite news|title=The Victoria Cross hero who charged at insurgents in Helmand battle to protect his comrades|url=http://www.dailymail.co.uk/news/article-2294638/James-Ashworth-The-Victoria-Cross-hero-charged-insurgents-Helmand-battle-protect-comrades.html|accessdate=17 March 2013|newspaper=Mail on Sunday|date=17 March 2013}}</ref> After his fire-team took out most of the insurgents, Ashworth pursued the final remaining member. He crawled forward under cover of a low wall while his team provided covering fire and acted as a diversion. When he got within {{convert|5|m|ft}} of the enemy, he was killed as he attempted to throw a grenade.<ref name=bbc18th>{{cite news|title=VC hero L/Cpl James Ashworth 'supremely courageous'|url=http://www.bbc.co.uk/news/uk-21833210|accessdate=18 March 2013|newspaper=BBC News|date=18 March 2013}}</ref> Captain Michael Dobbin, commander of the platoon, who was awarded the [[Military Cross]] for repeated courage throughout the operational tour, said about Ashworth, "His professionalism under pressure and ability to remain calm in what was a chaotic situation is testament to his character. L/Cpl Ashworth was a pleasure to command and I will sorely miss his calming influence on the battlefield. Softly spoken, he stepped up to every task thrown in his direction."<ref name=dailymail2012/><ref name=bbcnews/> After his death, his body was taken to [[Camp Bastion]] and was then repatriated to the United Kingdom.<ref name=mailonsunday/><ref>{{cite news|title=Victoria Cross Award For L/Cpl James Ashworth|url=http://news.sky.com/story/1065461/victoria-cross-award-for-l-cpl-james-ashworth|accessdate=17 March 2013|newspaper=Sky News|date=16 March 2013}}</ref>

On 16 March 2013, British media reported that Ashworth was to be posthumously awarded the [[Victoria Cross]] for bravery and this was confirmed by the [[Ministry of Defence (United Kingdom)|Ministry of Defence]] on 18 March 2013.<ref>{{cite web|url=http://www.blogs.mod.uk/defence_news/2013/03/defence-diary-18-march-2013.html|title=Defence Diary: 18 March 2013|work=MoD blog|publisher=MoD|date=18 March 2013|accessdate=18 March 2013}}</ref> His citation was read out at the Grenadier Guard barracks in Aldershot.<ref name=bbc18th/> He was only the second person to be awarded the medal during the [[Taliban insurgency]], after [[Bryan Budd]] for his actions in 2006. Ashworth is the 14th person to be awarded the Victoria Cross since the end of the [[Second World War]].<ref name=bbcnews/> The Victoria Cross was first awarded for actions in the [[Crimean War]] of 1854–56, and is the highest British military award for bravery.<ref name=bbcnews/>

==Victoria Cross citation==
The announcement and accompanying citation for the decoration was published in supplement to the London Gazette on 22 March 2013, reading<ref name="LG"/>

{{Quotation|St James’s Palace, London SW1
22 March 2013
The Queen has been graciously pleased to approve the award of the Victoria Cross to the under-mentioned:

ARMY

Lance Corporal James Thomas Duane Ashworth, Grenadier Guards, 25228593 (killed in action).

On the 13th June 2012 the conspicuous gallantry under fire of Lance Corporal Ashworth, a section second-incommand in 1st Battalion Grenadier Guards Reconnaissance Platoon, galvanised his platoon at a pivotal moment and led to the rout of a determined enemy grouping in the Nahr-e-Saraj District of Helmand Province.

The two aircraft inserting the Reconnaissance Platoon on an operation to neutralise a dangerous insurgent sniper team, were hit by enemy fire as they came into land. Unflustered, Ashworth – a young and inexperienced noncommissioned officer – raced 300 metres with his fire-team into the heart of the insurgent dominated village. Whilst two insurgents were killed and two sniper rifles recovered in the initial assault, an Afghan Local Police follow-up attack stalled when a patrolman was shot and killed by a fleeing enemy. Called forward to press-on with the attack, Ashworth insisted on moving to the front of his fire team to lead the pursuit. Approaching the entrance to a compound from which enemy machine gun fire raged, he stepped over the body of the dead patrolman, threw a grenade and surged forward. Breaking into the compound Ashworth quickly drove the insurgent back and into an out-building from where he now launched his tenacious last stand.

The village was now being pressed on a number of fronts by insurgents desperate to relieve their prized sniper team. The platoon needed to detain or kill the final sniper, who had been pinned down by the lead fire team, and extract as quickly as possible. Ashworth realised that the stalemate needed to be broken, and broken quickly. He identified a low wall that ran parallel to the front of the outbuilding from which the insurgent was firing. Although only knee high, he judged that it would provide him with just enough cover to get sufficiently close to the insurgent to accurately post his final grenade. As he started to crawl behind the wall and towards the enemy, a fierce fire fight broke out just above his prostrate body. Undaunted by the extraordinary danger – a significant portion of his route was covered from view but not from fire – Ashworth grimly continued his painstaking advance. After three minutes of slow crawling under exceptionally fierce automatic fire he had edged forward fifteen metres and was now within five metres of the insurgent’s position. Desperate to ensure that he succeeded in accurately landing the grenade, he then deliberately crawled out from cover into the full view of the enemy to get a better angle for the throw. By now enemy rounds were tearing up the ground mere centimetres from his body, and yet he did not shrink back. Then, as he was about to throw the grenade he was hit by enemy fire and died at the scene. Ashworth’s conspicuous gallantry galvanised his platoon to complete the clearance of the compound.

Despite the ferocity of the insurgent’s resistance, Ashworth refused to be beaten. His total disregard for his own safety in ensuring that the last grenade was posted accurately was the gallant last action of a soldier who had willingly placed himself in the line of fire on numerous occasions earlier in the attack. This supremely courageous and inspiring action deserves the highest recognition.}}

==Personal life==
Ashworth played [[Association football|football]] both for his regiment, and for a local team near his home. He was a supporter of [[Tottenham Hotspur]].<ref name=dailymail2012/> He has two sisters and two brothers, one of whom is also a soldier.<ref name=indy>{{cite news|last=Harris|first=Dominic|title=Lance Corporal James Ashworth to receive posthumous Victoria Cross for protecting comrades in Afghanistan|url=http://www.independent.co.uk/news/uk/home-news/lance-corporal-james-ashworth-to-receive-posthumous-victoria-cross-for-protecting-comrades-in-afghanistan-8537202.html|accessdate=17 March 2013|newspaper=The Independent|date=16 March 2013}}</ref>

==References==
{{reflist|30em}}

{{DEFAULTSORT:Ashworth, James}}
[[Category:1989 births]]
[[Category:2012 deaths]]
[[Category:British Army recipients of the Victoria Cross]]
[[Category:British military personnel killed in the War in Afghanistan (2001–2014)]]
[[Category:Grenadier Guards soldiers]]
[[Category:People from Kettering]]
[[Category:People from Corby]]
[[Category:War in Afghanistan (2001–2014) recipients of the Victoria Cross]]