{{unreferenced|date=June 2015}}
{{Infobox organization
|image        = The Newcomen Society logo.svg
|alt          = Logo of the Newcomen Society is a griffin regardant.
|caption      = Logo of the Newcomen Society
|motto        = Actorum memores simul affectamus agenda
|formation    = 1920
|type         = Learned society
|purpose      = Promotion and study of the history of engineering and technology
|headquarters = The [[Science Museum (London)|Science Museum]]
|language     = English
|leader_title = President
|leader_name  =
|name         = The Newcomen Society
|location     = [[London]]
|website      = http://newcomen.com/
|named_after=[[Thomas Newcomen]]}}

The '''Newcomen Society''' is an [[International]] [[learned society]] that promotes and celebrates the history of [[engineering]] and [[technology]]. It was founded in [[London]] in 1920 and takes its name from [[Thomas Newcomen]], one of the inventors associated with the early development of the [[steam engine]] and who is widely considered the "father of the Industrial Revolution".

The motto of the Society is the [[Latin]] ''actorum memores simul affectamus agenda'', meaning "mindful of things that have taken place, at the same time we strive after things yet to be done". The choice of a [[griffin]] [[regardant]] for the logo was to symbolise vigilance and looking backward while going forward.

The Newcomen Society is based at the [[Science Museum (London)|Science Museum]] in London. There are regional branches in England: Midlands ([[Birmingham]]), North West ([[Manchester]]), North East ([[Newcastle upon Tyne|Newcastle]]), Western ([[Bristol]]) and Southern ([[Portsmouth]]), and one in Scotland ([[Glasgow]] and [[Edinburgh]]).

The Society is concerned with all branches of engineering: [[civil engineering|civil]], [[mechanical engineering|mechanical]], [[electrical engineering|electrical]], [[Electronic engineering|electronic]], [[structural engineering|structural]], [[aeronautical engineering|aeronautical]], [[marine engineering|marine]], [[chemical engineering|chemical]] and [[manufacturing]] as well as [[biography]] and [[invention]].

It publishes the '''International Journal for the History of Engineering and Technology''' (formerly the Transactions of the Newcomen Society) and '''Newcomen Links''', a quarterly newsletter. An online archive of previous Transactions is also available to members of the Society.

An American branch was established in 1923, but the [[Newcomen Society of the United States]] was entirely separate from its UK counterpart in 2007, when the chairman and trustees announced its closure.

==Notable members==
* [[Henry Winram Dickinson]]
* [[L. T. C. Rolt]]
* [[Alec Skempton]]
* [[Hugh Pembroke Vowles]]
* [[Rex Wailes]]
* [[Paul Wilson, Baron Wilson of High Wray]] (president, 1973–1975)

==External links==
* [http://newcomen.com/ The Newcomen Society]

{{coord|51|29|51|N|0|10|29|W|type:landmark_region:GB|display=title}}

[[Category:Organizations established in 1920]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Engineering societies]]
[[Category:Science and technology in the United Kingdom]]
[[Category:History of science organizations]]
[[Category:History of technology]]
[[Category:1920 establishments in the United Kingdom]]
[[Category:Thomas Newcomen]]