{{POV|date=December 2014}}
{{Use British English|date=March 2013}}
{{Use dmy dates|date=March 2013}}
{{Infobox person
|name          =  Gurbaksh Singh Khalsa
|image         = 
|image_size    = 
|caption       =  
|birth_name    = Gurbaksh Singh
|birth_date    = {{birth date|df=yes|1965|12|12}}
|birth_place   = Thaska Ali, [[Kurukshetra]], Haryana [[India]]
|citizenship   = [[India]]
|religion      = [[Sikhism]]
|spouse        = Jasbir Kaur (since 1987)
|children      = 1 son
|parents       = Ajit Singh and Mohinder Kaur 
}}

'''Gurbaksh Singh Khalsa''' is a Sikh activist, demanding the release of Sikh prisoners from various jails in India.

== Early life ==

Khalsa was born on December 12, 1965 at Thaska Ali. [[Kurukshetra]], [[Haryana]]. His father’s name is Ajit Singh and his mother is Mohinder Kaur. He has two brothers and one sister. He took primary and Sikh religious education from the Gurdwara Lakhnaur Sahib and was also baptized there. In 1987, he married Jasbir Kaur and had a son.

==Campaign==
In November 2013, he went on a 44-day hunger strike.  The campaign was ended after Giani Gurbachan Singh (Akal Thakat Jathedar), promised to take up the issue with the State Government. After none of the detained Sikhs were released, Bhai Gurbaksh Singh Khalsa restarted his hunger strike, one year after his 2013 hunger strike.<ref>http://www.sikh24.com/2013/12/a-brief-biography-of-bhai-gurbakhsh-singh-khalsa-haryana/#.UtqhFORFCP8</ref> Over the course of the campaign, he got support from various other Sikh bodies. A number of Punjabi singers, actors and other artist visited him and pledged their support for his campaign.

His second campaign started on 14 November 2014 at Lakhnaur Gurdwara, Harayana. On the 8th January Bhai Gurbaksh Singh Khalsa, who had entered day 56, had proposed to visit Akal Takht - the highest temporal seat of Sikhs however a police blockade just a short distance from the Gurdwara prevented him fulfilling his wishes.<ref>http://timesofindia.indiatimes.com/india/Haryana-police-stopped-Khalsa-from-entering-Punjab/articleshow/45816703.cms</ref>

Details of the list of prisoners and discussion on the various options available to the Punjab government in case it wants to extend some relief to prisoners are detailed in the Tribune newspaper of India on the 11th Jan.<ref>http://www.tribuneindia.com/news/kaleidoscope/sikh-prisoners-ins--outs-of-jail-terms/28539.html</ref>

On the 10th January (Day 58) he developed breathing difficulties and was taken by authorities to Hospital, Khalsa to continue hunger strike whilst in hospital, Gurpiar Singh sits on hunger strike in the absence of Khalsa at Lakhnaur Gurdwara.<ref>http://sikhsiyasat.net/2015/01/10/gurbaksh-singh-khalsa-hospitalized-in-ambala-by-haryana-police-and-civil-administration/</ref>

Bhai Gurbaksh Singh Khalsa ended his 2nd hunger strike after 64 days on the 15th January.  Bhai Gurbaksh Singh Khalsa had been confined and kept isolated from his supporters at the Civil Hospital in Ambala under tight security since he was brought here for medical treatment.<ref>http://www.sikh24.com/2015/01/15/bhai-gurbaksh-singh-khalsas-second-hunger-strike-comes-to-an-end/#.VLlBZSusVRo</ref>

==See also==
*[[Darshan Singh Pheruman]], an eminent Akali leader who in 1969 had observed the hunger strike but died on the 74th day of his fast as he was observing to press for amalgamation of Punjabi-speaking areas of neighbouring states with Punjab.<ref>http://timesofindia.com/city/chandigarh/khalsas-fast-is-second-longest-post-1947-on-punjab-issue/articleshow/45802831.cms</ref>
*Bapu [[Surat Singh Khalsa]], is sitting on fast unto death for over 38 days seeking the release of those Sikh detainees who have completed their legal jail term.<ref>{{cite news|title=bapu-surat-singhs-hunger-strike-crosses-38th-day; no-significant-progress-on-sikh-political-prisoners-case|url=http://sikhsiyasat.net/2015/02/23/bapu-surat-singhs-hunger-strike-crosses-38th-day-no-significant-progress-on-sikh-political-prisoners-case/}}</ref><ref>{{cite news|title=Radical sikh organisations throw weight around Bapu Surat Singh Khalsa|url=http://timesofindia.indiatimes.com/city/ludhiana/Radical-sikh-organisations-throw-weight-around-Bapu-Surat-Singh-Khalsa/articleshow/46186987.cms}}</ref> 
*[[Vikram Singh Dhanaula]], youth activist who had hurled a shoe at Punjab chief minister Badal on Independence Day, had campaigned for the 1984 Sikh genocide victims and sought punishment for the culprits. His hunger strike ended after the newly elected Delhi Government gave a written assurance to form SIT for 1984 Sikh Genocide cases.<ref>{{cite news|title=The hunger strike at Delhi's Jantar Mantar by former Aam Admi Party activist Vikram Singh, who had hurled a shoe at Punjab chief minister Parkash Singh Badal on Independence Day last year, is not drawing any political party's attention even though 1984 riots are a big poll issue in Delhi.|url=http://timesofindia.indiatimes.com/city/chandigarh/Badal-shoe-hurlers-fast-enters-day-54/articleshow/46102038.cms}}</ref><ref>{{cite news|title=Vikram Singh Dhanaula’s Treatment Starts After 65 Day Hunger Strike|url=http://www.sikh24.com/2015/02/14/vikram-singh-dhanaulas-treatment-starts-after-65-day-hunger-strike/#.VOtCxvmUfIo}}</ref><ref>{{cite news|title=breaking-vikram-singh-dhanaula-ends-hunger-strike-following-written-assurance-kejriwal|url=http://singhstation.net/2015/02/breaking-vikram-singh-dhanaula-ends-hunger-strike-following-written-assurance-kejriwal/}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.sikh24.com/2013/12/a-brief-biography-of-bhai-gurbakhsh-singh-khalsa-haryana/#.UsAeteRFCP8]
* [http://sikhsiyasat.net/tag/gurbaksh-singh-khalsa/]

{{DEFAULTSORT:Khalsa, Gurbaksh Singh}}
[[Category:Articles created via the Article Wizard]]
[[Category:Indian Sikhs]]
[[Category:1965 births]]
[[Category:Living people]]