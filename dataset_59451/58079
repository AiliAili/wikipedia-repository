{{distinguish|Journal of Statistics Education}}

{{Infobox journal
| title = Journal of Educational and Behavioral Statistics
| cover = [[File:Journal of Educational and Behavioral Statistics.tif]]
| editors = Li Cai, Daniel McCaffrey
| discipline = [[Education]]
| former_names = Journal of Educational Statistics
| abbreviation = J. Educ. Behav. Stat.
| publisher = [[Sage Publications]] on behalf of the [[American Educational Research Association]]
| country =
| frequency = Bimonthly
| history = 1976-present
| openaccess = 
| license = 
| impact = 1.255
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201853/title
| link1 = http://jeb.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jeb.sagepub.com/content/by/year
| link2-name = Online archive
| link3 = http://www.aera.net/tabid/12610/Default.aspx
| link3-name = Journal page at association's website
| JSTOR = 
| OCLC = 30674665
| LCCN = 94664267
| CODEN = 
| ISSN = 1076-9986
| eISSN = 1935-1054
| ISSN2 = 0362-9791
}}
The '''''Journal of Educational and Behavioral Statistics''''' is a [[peer-reviewed]] [[academic journal]] published by [[Sage Publications]] on behalf of the [[American Educational Research Association]] and [[American Statistical Association]]. It covers [[statistical method]]s and [[applied statistics]] in the [[Education studies|educational]] and [[behavioral sciences]]. The journal was established in 1976 as the ''Journal of Educational Statistics'' and obtained its current name in 1994. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.255.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Educational and Behavioral Statistics |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.sagepub.com/journals/Journal201853/title}}

{{Statistics journals|state=collapsed}}

[[Category:Education journals]]
[[Category:Statistics journals]]
[[Category:American Statistical Association academic journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1976]]