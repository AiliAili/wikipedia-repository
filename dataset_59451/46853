{{Multiple issues|
{{notability|date=January 2017}}
{{BLP sources|date=July 2016}}
{{BLP primary sources|section on career|date=January 2016}}}}
{{Infobox person
| name			= Grant Cardone
| image			=
| birth_date	= {{Birth date and age|1958|03|21|mf=y}}
| birth_place	= [[Lake Charles, Louisiana]]
| occupation    = Motivational speaker, author
| religion      = [[Scientology]]
| spouse        = Elena Lyons
| children       = 2
| website	= {{official website|http://grantcardone.com}}
}}
'''Grant Cardone''' (born March 21, 1958) is an American author,  [[motivational speaking|motivational speaker]], [[real estate]] investor, and [[sales training|sales trainer]].<ref>{{cite news |url=http://www.huffingtonpost.com/grant-cardone/buying-a-car-made-quick-a_b_773614.html |title=Automotive Sales Trainer Shows Tricks to Buying a Car Quick and Easy
 |author=Grant Cardone |date=8 November 2010 |work=[[Huffington Post]] |publisher=TheHuffingtonPost.com, Inc. |accessdate=8 July 2013}}</ref><ref>{{cite web|last=Theiss |first=Eliza |url=http://www.cpexecutive.com/cities/nashville/grant-cardone-drops-48m-on-four-apartment-communities/1004114822.html |title=Grant Cardone Drops $48M on Four Apartment Communities |website=Cpexecutive.com |date=2015-03-03 |accessdate=2016-07-28}}</ref><ref>{{cite news|last1=Cassell|first1=Warren|title=How Grant Cardone Built a $350M Real Estate Empire|url=http://www.investopedia.com/articles/personal-finance/072315/how-grant-cardone-built-350m-real-estate-empire.asp|accessdate=24 October 2016|work=[[Investopedia]]|date=23 July 2015}}</ref>

==Early life and education==
Cardone was born on March 21, 1958, in [[Lake Charles, Louisiana]]. His father was Curtis Louis Cardone (died February 1968) and his mother was Concetta Neil Cardone (died May 2009). He has a twin brother, Gary Cardone, and is the fourth of five children.<ref>{{cite news| url=http://www.huffingtonpost.com/grant-cardone/say-yes-to-life_b_209903.html | work=Huffington Post | title=Say Yes to Life | date=June 4, 2009}}</ref>

Cardone graduated from [[LaGrange High School (Louisiana)|LaGrange High School]] in Lake Charles in 1976. He then went to [[McNeese State University]] from 1976-1981 where he graduated with a Bachelor of Science Degree in Accounting. Cardone was awarded the McNeese State University Distinguished Alumnus Award 2010.<ref>{{cite web|url=http://mcneesealumni.com/s/1148/index.aspx?sid=1148&gid=1&pgid=349 |title=McNeese State University Alumni Association - Alumni Awards |website=Mcneesealumni.com |date= |accessdate=2016-07-28}}</ref>

==Career==
In 1987, Cardone moved to Chicago to work for a sales-training company. Although he lived in Chicago, he traveled all over the United States, living in different cities. Cardone briefly returned to Lake Charles before moving to Houston, Texas, where he lived for the next five years. He then spent the next 12 years in La Jolla, California,{{citation needed|date=September 2015}} before moving to Los Angeles.<ref>{{cite web|url=http://www.mercurynews.com/lalife/ci_14765228|title=Beautiful view may be priceless, but homeowners pay plenty|work=San Jose Mercury News|access-date=2016-03-03}}</ref>  Cardone moved his offices from California to Miami Beach in late 2012.<ref>{{cite web|url=http://cardoneacquisitions.com/#about|title=Grant Cardone Acquisitions|website=cardoneacquisitions.com|access-date=2016-03-02}}</ref>

After college, Cardone chose not to pursue the field of accounting and moved into [[car dealership|automobile sales]].<ref>{{cite book|last=Cardone|first=Grant|title=Maximizing Every Opportunity|year=2005|publisher=Wiley|location=Los Angeles|page=1}}</ref>

Cardone also spent some time as CEO of Freedom Motorsports Group Inc.<ref>{{cite web|url=http://www.scientology-buffalo.org/articles/605241844471.vm |title=Dianetics Racing Team to Join NASCAR Circuit |website=Scientology-buffalo.org |accessdate=2016-07-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20120422101359/http://www.scientology-buffalo.org/articles/605241844471.vm |archivedate=2012-04-22 |df= }}</ref> In late 2010, Cardone worked with [[Atlas Media Corp.]] to develop a reality television series for the [[National Geographic Channel]] named ''Turnaround King''. The series followed Cardone as he attempted to help a struggling business restore their profitability.<ref>{{cite web|url=http://www.prlog.org/11506097-atlas-media-brings-turnaround-king-to-national-geographic.html |title=Atlas Media brings ‘Turnaround King’ to National Geographic - Kelly Li |website=PRLog.org |date= |accessdate=2016-07-28}}</ref>

Cardone holds a real estate portfolio in the United States estimated to be valued at around $350 million.<ref>{{cite web|author=Vaughn Reynolds |url=http://cardoneacquisitions.com/press/cardone-real-estate-acquisitions-adds-36m-audubon-multi-family-deal/ |title=Cardone Real Estate Acquisitions Adds $36m Audubon Multi-Family Deal - Grant Cardone Acquisitions |website=Cardoneacquisitions.com |date=2015-03-22 |accessdate=2016-07-28}}</ref><ref name="Warren Cassell, Jr.">{{cite web | url=http://www.investopedia.com/articles/personal-finance/072315/how-grant-cardone-built-350m-real-estate-empire.asp | title=How Grant Cardone Built a $350M Real Estate Empire | publisher=Investopedia | date=23 June 2015 | accessdate=20 May 2016 | author=Warren Cassell, Jr.}}</ref><ref>{{cite web|url=http://www.elitemiamiproperty.com/blog/author-wants-invest-1-billion-worth-florida-real-estate-daily-business-review/ |title=Author Wants To Invest $1 Billion Worth Of Florida Real Estate Daily Business Review |website=Elitemiamiproperty.com |date=2014-05-12 |accessdate=2016-07-28}}</ref><ref>{{cite web|author=Vaughn Reynolds |url=http://cardoneacquisitions.com/press/cardone-real-estate-acquisitions-llc-announces-purchase-of-126-million-in-multi-family-apartments/ |title=Cardone Real Estate Acquisitions, LLC Announces Purchase of $126 Million in Multi-family Apartments - Grant Cardone Acquisitions |website=Cardoneacquisitions.com |date=2015-03-12 |accessdate=2016-07-28}}</ref>

Cardone is the author of seven books: ''Sell To Survive'' (2008), ''The Closer's Survival Guide'' (2009), ''If You're Not First, You're Last'' (2010), ''The 10X Rule'' (2011), and ''Sell or be Sold'' (2012), ''The Millionare Booklet (2016)'', ''Be Obsessed or Be Average (2016).''

On November 29, 2016, ''Forbes'' named Cardone #1 of the "25 Marketing Influencers to Watch in 2017".<ref>{{cite web|last1=Schroeder|first1=Jules|title=How 3 Social Superstars Built Huge Followings Fast|url=http://www.forbes.com/sites/julesschroeder/2016/11/29/25-marketing-influencers-to-watch-in-2017/#c7e92bf375b8|publisher=[[Forbes]]|accessdate=January 31, 2017|date=November 29, 2016}}</ref>

==Personal life==
Cardone is a member of the [[Church of Scientology]].<ref>{{cite web |url=http://www.thehollywoodsentinel.com/issue20article7.html |title=Grant Cardone - World Financial Leader |publisher=Hollywood Sentinel |author=Bruce Edwin |accessdate=17 July 2013}}</ref> In 2006 he helped promote [[Dianetics]] and [[Scientology]] by sponsoring NASCAR drivers through his company, Freedom Motorsports.<ref>{{cite web|last=Walls|first=Jeannette|title=Scientology revs up to join NASCAR circuit|url=http://www.today.com/id/13105675/ns/today-entertainment/t/scientology-revs-join-nascar-circuit/#.UfRLEMDEWCc|work=TODAY Entertainment|publisher=[[NBC News]]|accessdate=27 July 2013}}</ref><ref>{{cite web|last=Gray|first=Kenton|title=Risky Business: Scientology Makes Play Into NASCAR|url=http://m.sportsbusinessdaily.com/Daily/Issues/2006/06/Issue-175/Sponsorships-Advertising-Marketing/Risky-Business-Scientology-Makes-Play-Into-NASCAR.aspx|work=Sports Business Daily|publisher=AEG|accessdate=27 July 2013}}</ref>

Cardone has two daughters and is married to Spanish-born actress Elena Lyons.<ref>{{cite web|last=Cardone|first=Grant|title=Grant Cardone, New York Times best selling author & sales expert|url=http://cardoneuniversity.com/about.php|work=Grant Cardone Sales University|accessdate=27 July 2013|date=Mar 10, 2013}}</ref><ref>{{cite web|last=Lyons|first=E.|title=Bio: Elene Lyons|url=http://www.elenacardone.com/bio/|website=Elenacardone.com|accessdate=29 July 2013}}</ref>

==References==
{{Reflist|30em}}

==External links==
* {{Official|http://www.grantcardone.com/}}

{{Authority control}}

{{DEFAULTSORT:Cardone, Grant}}
[[Category:1958 births]]
[[Category:American male writers]]
[[Category:American motivational speakers]]
[[Category:American Scientologists]]
[[Category:American self-help writers]]
[[Category:American television personalities]]
[[Category:Living people]]
[[Category:McNeese State University alumni]]