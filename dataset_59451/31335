{{EngvarB|date=October 2013}}
{{Use dmy dates|date=October 2013}}
{{Infobox person
| name          = Augusta, Lady Gregory
| image         = Lady gregory.jpg
| alt           = Head and shoulders profile of a dignified older woman with hair swept back and a slightly prominent nose. Underneath is the signature "Augusta Gregory".
| caption       = Lady Gregory pictured on the frontispiece to "Our Irish Theatre: A Chapter of Autobiography" (1913)
| birth_name    = Isabella Augusta
| birth_date    = {{Birth date|1852|03|15}}
| birth_place   = Roxborough, [[County Galway]]
| death_date    = {{Death date and age|1932|05|22|1852|03|5}}
| death_place   =
| death_cause   = [[Breast cancer]]
| resting_place = New Cemetery in [[Bohermore]], [[County Galway]]
| resting_place_coordinates = <!-- {{coord|LAT|LONG|type:landmark|display=inline}} -->
| nationality   = [[Republic of Ireland|Irish]]
| other_names   = 
| occupation    = [[Dramatist]], [[Folklore|folklorist]], Theatre manager
| spouse        = [[William Henry Gregory|Sir William Henry Gregory]] <small>(m. 1880)</small>
| children      = [[Robert Gregory (cricketer)|William Robert Gregory]] <small>(born 1881)</small>
| years_active  = 
| known_for     = 
| notable_works = [[Irish Literary Revival]]
}}

'''Isabella Augusta, Lady Gregory''' (''née'' '''Persse'''; 15 March 1852 – 22 May 1932) was an Irish [[dramatist]], [[Folklore|folklorist]] and theatre manager. With [[William Butler Yeats]] and [[Edward Martyn]], she co-founded the [[Irish Literary Theatre]] and the [[Abbey Theatre]], and wrote numerous short works for both companies. Lady Gregory produced a number of books of retellings of stories taken from [[Irish mythology]]. Born into a class that identified closely with British rule, her conversion to cultural nationalism, as evidenced by her writings, was emblematic of many of the political struggles to occur in Ireland during her lifetime.

Lady Gregory is mainly remembered for her work behind the [[Irish Literary Revival]]. Her home at [[Coole Park]], [[County Galway]], served as an important meeting place for leading Revival figures, and her early work as a member of the board of the Abbey was at least as important for the theatre's development as her creative writings. Lady Gregory's motto was taken from [[Aristotle]]: "To think like a wise man, but to express oneself like the common people."<ref>Yeats 2002, p. 391.</ref>

==Early life and marriage==
Gregory was born at Roxborough, [[County Galway]], the youngest daughter of the [[Anglo-Irish]] gentry family Persse. Her mother, Frances Barry, was related to [[Standish O'Grady, 1st Viscount Guillamore|Viscount Guillamore]], and her family home, Roxborough, was a 6,000-acre (24&nbsp;km²) estate located between [[Gort]] and [[Loughrea]], the main house of which was later burnt down during the [[Irish Civil War]].<ref>Foster (2003), p. 484.</ref> She was educated at home, and her future career was strongly influenced by the family nurse (i.e. [[nanny]]), Mary Sheridan, a Catholic and a [[Irish language|native Irish speaker]], who introduced the young Augusta to the history and legends of the local area.<ref>Shrank and Demastes 1997, p. 108.</ref>

She married [[William Henry Gregory|Sir William Henry Gregory]], a widower with an estate at [[Coole Park]], near Gort, on 4 March 1880 in St Matthias' Church, Dublin.<ref>Coxhead, Elizabeth. ''Lady Gregory: a literary portrait'', Harcourt, Brace & World, 1961, p. 22.</ref> Sir William, who was 35 years her elder, had just retired from his position as Governor of [[Ceylon]] (now Sri Lanka), having previously served several terms as Member of Parliament for County Galway. He was a well-educated man with many literary and artistic interests, and the house at Coole Park housed a large library and extensive art collection, both of which Lady Gregory was eager to explore. He also had a house in London, where the couple spent a considerable amount of time, holding weekly [[Salon (gathering)|salons]] frequented by many leading literary and artistic figures of the day, including [[Robert Browning]], [[Alfred Tennyson, 1st Baron Tennyson|Lord Tennyson]], [[John Everett Millais]] and [[Henry James]]. Their only child, [[Robert Gregory (cricketer)|Robert Gregory]], was born in 1881. He was killed during the [[First World War]], while serving as a pilot, an event which inspired Yeats's poems "An Irish Airman Foresees His Death," "In Memory of Major Robert Gregory," and "Shepherd and Goatherd."<ref>[http://www.wwnorton.com/college/english/nael/20century/topic_1_05/wbyeats_irish.htm "Representing the Great War: Texts and Contexts"], The Norton Anthology of English Literature, 8th edition, accessed 5 October 2007.</ref><ref>Kermode 1957, p. 31.</ref>

==Early writings==
The Gregorys travelled in Ceylon, India, Spain, Italy and Egypt. While in Egypt, Lady Gregory had an affair with the English poet [[Wilfrid Scawen Blunt]], during which she wrote a series of love poems, ''A Woman's Sonnets''.<ref>Hennessy 2007; Holmes 2005, p. 103.</ref>

Her earliest work to appear under her own name was ''Arabi and His Household'' (1882), a pamphlet—originally a letter to ''[[The Times]]''—in support of [[Ahmed 'Urabi|Ahmed Orabi Pasha]], leader of what has come to be known as the [[Urabi Revolt]], an 1879 Egyptian nationalist revolt against the oppressive regime of the [[Khedive]] and European domination of Egypt. She later said of this booklet, "whatever political indignation or energy was born with me may have run its course in that Egyptian year and worn itself out".<ref>Gregory 1976, p. 54.</ref> Despite this, in 1893 she published ''A Phantom's Pilgrimage, or Home Ruin'', an anti-Nationalist pamphlet against [[William Ewart Gladstone]]'s proposed second [[Home Rule]] Act.<ref>Kirkpatrick 2000, p. 109.</ref>

She continued to write prose during the period of her marriage. During the winter of 1883, while her husband was in Ceylon, she worked on a series of memoirs of her childhood home with a view to publishing them under the title ''An Emigrant's Notebook'',<ref>Garrigan Mattar 2004, p. 187.</ref> but this plan was abandoned. She wrote a series of pamphlets in 1887 called ''Over the River'', in which she appealed for funds for the parish of St. Stephens in [[Southwark]], south London.<ref>Yeats, Kelly and Schuchard 2005, p. 165, fn 2.</ref> She also wrote a number of short stories in the years 1890 and 1891, although these also never appeared in print. A number of unpublished poems from this period have also survived. When Sir William Gregory died in March 1892, Lady Gregory went into mourning and returned to Coole Park where she edited her husband's autobiography, which she published in 1894.<ref name="Gonzalez98">Gonzalez 1997, p. 98.</ref> She was to write later, "If I had not married I should not have learned the quick enrichment of sentences that one gets in conversation; had I not been widowed I should not have found the detachment of mind, the leisure for observation necessary to give insight into character, to express and interpret it. Loneliness made me rich—'full', as Bacon says."<ref>Owens and Radner 1990, p. 12.</ref>

==Cultural nationalism==
A trip to [[Inisheer]] in the [[Aran Islands]] in 1893 re-awoke an interest in the Irish language<ref name="IWO">[https://web.archive.org/web/20041119094612/http://www.irishwriters-online.com:80/ladygregory.html Lady Gregory]". Irish Writers Online, accessed 23 September 2007.</ref> and in the folklore of the area in which she lived. She organised Irish lessons at the school at Coole and began collecting tales from the area around her home, especially from the residents of Gort [[workhouse]]. One of the tutors she employed, was [[Norma Borthwick]], who would visit Coole numerous times.<ref name=Rouse>{{cite book|last1=Rouse|first1=Paul|editor1-last=McGuire|editor1-first=James|editor2-last=Quinn|editor2-first=James|title=Dictionary of Irish Biography|date=2009|publisher=Cambridge University Press|location=Cambridge|chapter=Borthwick, Mariella Norma}}</ref> This activity led to the publication of a number of volumes of folk material, including ''A Book of Saints and Wonders'' (1906), ''The Kiltartan History Book'' (1909), and ''The Kiltartan Wonder Book'' (1910). She also produced a number of collections of "Kiltartanese" versions of Irish myths, including ''[[Cuchulain of Muirthemne]]'' (1902) and ''Gods and Fighting Men'' (1904).  ("Kiltartanese" is Lady Gregory's term for English with Gaelic syntax, based on the dialect spoken in [[Kiltartan]].) In his introduction to the former, Yeats wrote "I think this book is the best that has come out of Ireland in my time."<ref>Love, Damian. "Sailing to Ithaca: Remaking Yeats in Ulysses". The Cambridge Quarterly – Volume 36, Number 1, 2007, pp. 1–10.</ref> [[James Joyce]] was to parody this claim in the [[Scylla and Charybdis]] chapter of his novel ''[[Ulysses (novel)|Ulysses]]''.<ref>Emerson Rogers 1948, pp. 306–327.</ref><!--Can't find a ref for this: [[Flann O'Brien]] would also parody the book in his ''[[At Swim-Two-Birds]]'' with his overly literal versions of the myths of the [[Irish mythology#Fenian cycle|Fenian cycle]].-->

Towards the end of 1894, encouraged by the positive reception of the editing of her husband's autobiography, Lady Gregory turned her attention to another editorial project. She decided to prepare selections from Sir William Gregory's grandfather's correspondence for publication as ''Mr Gregory's Letter-Box 1813–30'' (1898). This entailed researching Irish history of the period, and one outcome of this work was a shift in her own position from the "soft" [[Unionism (Ireland)|Unionism]] of her earlier writing on Home Rule to a definite support of [[Irish nationalism]] and [[Irish Republicanism|Republicanism]] and what she was later to describe as "a dislike and distrust of England".<ref>Komesu and Sekine 1990, p. 102.</ref>

==Founding of the Abbey==
[[Image:Abbey1.jpg|right|140px|thumb|alt=THE NATIONAL THEATRE SOCIETY / SPREADING THE NEWS / ON BAILE'S STRAND / KATHLEEN NI HOULIHAN / ON THE SHADOW OF THE GLEN / ABBEY THEATRE / TUESDAY, 27 Dec, '04 / TUESDAY, 3 Jan, '05|A poster for the opening run at the Abbey Theatre from 27 December 1904 to 3 January 1905.]]

[[Edward Martyn]] was a neighbour of Lady Gregory's, and it was during a visit to his home, Tullira Castle, in 1896 that she first met W. B. Yeats.<ref>Graham, Rigby. "Letter from Dublin". American Notes & Queries, Vol. 10, 1972.</ref> Discussions between the three of them over the following year or so led to the founding of the Irish Literary Theatre in 1899.<ref>Foster (2003). pp. 486, 662.</ref> Lady Gregory undertook fundraising, and the first programme consisted of Martyn's ''The Heather Field'' and Yeats's ''The Countess Cathleen''.

The Irish Literary Theatre project lasted until 1901,<ref>Kavanagh, Peter. "The Story of the Abbey Theatre: From Its Origins in 1899 to the Present". (New York): Devin-Adair, 1950.</ref> when it collapsed due to lack of funding. In 1904, Lady Gregory, Martyn, Yeats, [[John Millington Synge]], [[George William Russell|Æ]], [[Annie Horniman]] and [[William Fay|William]] and [[Frank Fay (Irish actor)|Frank Fay]] came together to form the [[Irish National Theatre Society]]. The first performances staged by the society took place in a building called the Molesworth Hall. When the [[Mechanics' Theatre|Hibernian Theatre of Varieties]] in Lower Abbey Street and an adjacent building in Marlborough Street became available, Horniman and William Fay agreed to their purchase and refitting to meet the needs of the society.<ref>McCormack 1999, pp. 5–6.</ref>

On 11 May 1904, the society formally accepted Horniman's offer of the use of the building. As Horniman was not normally resident in Ireland, the Royal Letters Patent required were paid for by her but granted in the name of Lady Gregory.<ref>Yeats, Kelly and Schuchard 2005, p. 902.</ref> One of her own plays, ''[[Spreading the News]]'' was performed on the opening night, 27 December 1904.<ref>Murray, Christopher.{{cite web|url=http://www.abbeytheatre.ie/pdfs/introduction.pdf |title=Introduction to the abbeyonehundred Special Lecture Series |accessdate=6 October 2007 |deadurl=yes |archiveurl=https://web.archive.org/web/20080307102415/http://www.abbeytheatre.ie/pdfs/introduction.pdf |archivedate=7 March 2008 }}  (PDF). abbeytheatre.ie, accessed 6 August 2009.</ref> At the opening of Synge's ''[[The Playboy of the Western World]]'' in January 1907, a significant portion of the crowd rioted, causing the remainder of the performances to be acted out in [[dumbshow]].<ref>Ellis 2003</ref> Lady Gregory did not think as highly of the play as Yeats did, but she defended Synge as a matter of principle. Her view of the affair is summed up in a letter to Yeats where she wrote of the riots: "It is the old battle, between those who use a toothbrush and those who don't."<ref>Frazier 2002</ref>

==Later career==
[[Image:Lady gregory1.jpg|left|thumbnail|150px|alt=Print of a woman and a dog on a leash. Underneath is "The WHITE COCKADE. A Comedy in Three Acts, by Lady Gregory, being Volume VIII. of the Abbey Theatre Series."|The cover of Lady Gregory's 1905 play]]
Lady Gregory remained an active director of the theatre until ill health led to her retirement in 1928. During this time she wrote more than 19 plays, mainly for production at the Abbey.<ref name="IWO" /> Many of these were written in an attempted transliteration of the [[Hiberno-English]] dialect spoken around Coole Park that became widely known as Kiltartanese, from the nearby village of [[Kiltartan]]. Her plays had been among the most successful at the Abbey in the earlier years,<ref>Pethica 2004</ref> but their popularity declined. Indeed, the Irish writer [[Oliver St John Gogarty]] once wrote "the perpetual presentation of her plays nearly ruined the Abbey".<ref>[http://www.ricorso.net/rx/az-data/index.htm Augusta Gregory]. Ricorso</ref> In addition to her plays, she wrote a two-volume study of the folklore of her native area called ''Visions and Beliefs in the West of Ireland'' in 1920. She also played the lead role in three performances of ''Cathleen Ni Houlihan'' in 1919.

During her time on the board of the Abbey, Coole Park remained her home and she spent her time in Dublin staying in a number of hotels. At the time of the 1911 national census for example, she was staying in a hotel at 16 South Frederick Street.<ref>[http://www.census.nationalarchives.ie/reels/nai000209724/ 1911 Census Form]</ref> In these, she ate frugally, often on food she brought with her from home. She frequently used her hotel rooms to interview would-be Abbey dramatists and to entertain the company after opening nights of new plays. She spent many of her days working on her translations in the [[National Library of Ireland]]. She gained a reputation as being a somewhat conservative figure.<ref>DiBattista and McDiarmid 1996, p. 216.</ref> For instance, when [[Denis Johnston]] submitted his first play ''Shadowdance'' to the Abbey, it was rejected by Lady Gregory and returned to the author with "The Old Lady says No" written on the title page.<ref>Dick, Ellmann and Kiberd 1992. p. 183.</ref> Johnson decided to rename the play, and ''The Old Lady Says 'No' '' was eventually staged by the Gate Theatre in 1928.

==Retirement and death==
[[Image:Augusta, Lady Gregory - Project Gutenberg eText 19028.jpg|150px|thumb|alt=Half length portrait of a serious elderly woman in a black Victorian dress, standing with hands clasped at her waist|Lady Gregory in later life]]
When she retired from the Abbey board, Lady Gregory returned to live in Galway, although she continued to visit Dublin regularly. The house and [[demesne]] at Coole Park had been sold to the [[Coillte Teoranta|Irish Forestry Commission]] in 1927, with Lady Gregory retaining life tenancy.<ref>Genet, Jacqueline. "The Big House in Ireland: Reality and Representation". Barnes & Noble, 1991. p. 271.</ref> Her Galway home had long been a focal point for the writers associated with the Irish Literary Revival and this continued after her retirement. On a tree in what were the grounds of the now demolished house, one can still see the carved initials of Synge, Æ, Yeats and his artist brother [[Jack Yeats|Jack]], [[George A. Moore|George Moore]], [[Seán O'Casey]], [[George Bernard Shaw]], [[Katharine Tynan]] and [[Violet Martin]]. Yeats wrote five poems about or set in the house and grounds: "The Wild Swans at Coole", "I walked among the seven woods of Coole", "In the Seven Woods", "Coole Park, 1929" and "Coole Park and Ballylee, 1931".

The woman Shaw once described as "the greatest living Irishwoman"<ref>Goldsmith 1854, p. 178.</ref> died at home aged 80 from [[breast cancer]],<ref name="Gonzalez98" /> and is buried in the New Cemetery in [[Bohermore]], [[County Galway]]. The entire contents of Coole Park were auctioned three months after her death and the house demolished in 1941.<ref>[http://coolepark.ie/briefhistory/index.html "Brief History of Coole Park"], The Department of Arts, Heritage and the GAeltacht, accessed 6 April 2013.</ref>

Her plays fell out of favour after her death and are now rarely performed.<ref>Gordon 1970, p. 28.</ref> Many of the diaries and journals she kept for most of her adult life have been published, providing a rich source of information on Irish literary history during the first three decades of the 20th century.<ref>Pethica 1995.</ref>

==See also==
*''[[Cathleen Ní Houlihan]]''

==Notes==
{{Reflist|30em}}

==References==
{{Refbegin|30em}}
*Coxhead, Elizabeth. ''Lady Gregory: a literary portrait'', Harcourt, Brace & World, 1961.
*DiBattista, Maria; McDiarmid, Lucy. ''High and Low Moderns: Literature and Culture, 1889–1939''. New York: Oxford University Press, 1996.
*Dick, Susan; Ellmann, Richard; Kiberd, Declan. "Essays for Richard Ellmann: Omnium Gatherum". The Yearbook of English Studies, Vol. 22, Medieval Narrative Special Number, McGill-Queen's Press, 1992.
*Ellis, Samantha. "[http://arts.guardian.co.uk/curtainup/story/0,12830,937744,00.html The Playboy of the Western World, Dublin, 1907]". ''The Guardian'', 16 April 2003, accessed 1 September 2009.
*Emerson Rogers, Howard. "Irish Myth and the Plot of Ulysses," ELH, Vol. 15, No. 4, December 1948. pp.&nbsp;306–327.
*Foster, R. F. ''W. B. Yeats: A Life, Vol. II: The Arch-Poet 1915–1939''. New York: Oxford University Press, 1993. ISBN 0-19-818465-4.
*Garrigan Mattar, Sinéad. ''Primitivism, Science, and the Irish Revival''. Oxford University Press, 2004. ISBN 0-19-926895-9.
*Genet, Jacqueline. ''The Big House in Ireland: Reality and Representation''. Barnes & Noble, 1991.
*Goldsmith, Oliver. ''The Works of Oliver Goldsmith''. London: John Murray, 1854. OCLC: 2180329.
*Gonzalez, Alexander G. ''Modern Irish Writers: A Bio-Critical Sourcebook''. Greenwood Press, 1997
*Gordon, Donald James. ''W. B. Yeats: images of a poet: my permanent or impermanent images'', Manchester University Press ND, 1970
*Graham, Rigby. "Letter from Dublin". ''American Notes & Queries'', Vol. 10, 1972.
*Gregory, Augusta. ''Seventy years: being the autobiography of Lady Gregory''. Macmillan, 1976
*Hennessy, Caroline. "[http://www.rte.ie/arts/2005/1230/ladygregory.html Lady Gregory: An Irish Life by Judith Hill]", Raidió Teilifís Éireann, 2007, accessed 1 September 2009.
*Holmes, John. ''Dante Gabriel Rossetti and the Late Victorian Sonnet Sequence''. Aldershot: Ashgate, 2005.
*Igoe, Vivien. ''A Literary Guide to Dublin''. Methuen, 1994. ISBN 0-413-69120-9
*Kavanagh, Peter. ''The Story of the Abbey Theatre: From Its Origins in 1899 to the Present''. New York: Devin-Adair, 1950.
*Kermode, Frank. ''Romantic Image''. New York: Vintage Books, 1957.
*Kirkpatrick, Kathryn. ''Border Crossings: Irish Women Writers and National Identities''. Tuscaloosa: University Of Alabama Press, 2000.
*Komesu, Okifumi and Sekine, Masuru. ''Irish Writers and Politics''. Rowman & Littlefield, 1990. ISBN 0-389-20926-0
*Love, Damian. "Sailing to Ithaca: Remaking Yeats in Ulysses," ''The Cambridge Quarterly'', Volume 36, Number 1, 2007, pp.&nbsp;1–10.
*McCormack, William. ''The Blackwell Companion to Modern Irish Culture''. Oxford: Blackwell, 1999.
*Murray, Christopher.{{cite web|url=http://www.abbeytheatre.ie/pdfs/introduction.pdf |title=Introduction to the abbeyonehundred Special Lecture Series |accessdate=2007-10-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20080307102415/http://www.abbeytheatre.ie/pdfs/introduction.pdf |archivedate=7 March 2008 |df=dmy }} . abbeytheatre.i.e., accessed 6 August 2009.
*Owens, Cóilín and Radner, Joan Newlon. ''Irish Drama, 1900–1980'', CUA Press, 1990
*Pethica, James. ''Lady Gregory's Diaries 1892–1902'' Colin Smythe, 1995. ISBN 0-86140-306-1
*Pethica, James L. [http://www.oxforddnb.com/view/article/33554 "Gregory, (Isabella) Augusta, Lady Gregory (1852–1932)"], ''Oxford Dictionary of National Biography'', Oxford University Press, 2004.
*Ryan, Philip B. ''The Lost Theatres of Dublin''. The Badger Press, 1998. ISBN 0-9526076-1-1
*Shrank, Bernice and Demastes, William. ''Irish playwrights, 1880–1995''. Westport: Greenwood Press, 1997.
*Tuohy, Frank. ''Yeats''. London: Herbert, 1991.
*Yeats, William Butler. ''Writings on Irish Folklore, Legend and Myth''. Penguin Classics, republished 26 February 2002. ISBN 0-14-018001-X.
*Yeats, William Butler; Kelly, John; Schuchard, Richard. ''The collected letters of W. B. Yeats'', Oxford University Press, 2005.
*[https://web.archive.org/web/20130415065320/http://www.coolepark.ie:80/briefhistory/index.html "Brief History of Coole Park"], Department of Arts, Heritage and the Gaeltacht, accessed 6 April 2013.
*[http://www.wwnorton.com/college/english/nael/20century/topic_1_05/wbyeats_irish.htm "Representing the Great War: Texts and Contexts"], ''The Norton Anthology of English Literature'', 8th edition, accessed 1 September 2009.
{{refend}}

==Further reading==
{{wikisource author}}
{{Refbegin|30em}}
* Kohfeldt, Mary Lou. "Lady Gregory: The Woman Behind the Irish Renaissance". André Deutsch, 1984; ISBN 0-689-11486-9
* McDiarmid, Lucy; Waters, Maureen (edit). "Lady Gregory: Selected Writings". Penguin Twentieth Century Classics, 1996; ISBN 0-14-018955-6
* Napier, Taura. "Seeking a Country: Literary Autobiographies of Irish Women". [[University Press of America]], 2001; ISBN 0-7618-1934-7
* [https://web.archive.org/web/20041119094612/http://www.irishwriters-online.com:80/ladygregory.html Lady Gregory at Irish Writers Online], accessed 4 November 2004.
*''Irish Writers on Writing'' featuring Augusta, Lady Gregory. Edited by [[Eavan Boland]] ([[Trinity University (Texas)#Trinity University Press|Trinity University Press]], 2007).
* [http://digital.library.upenn.edu/women/gregory/theatre/appendix-I.html '' Plays Produced by the Abbey Theatre Co. and its Predecessors, with dates of First Performances.'' by Lady Augusta Persse Gregory (1852–1932)], accessed 4 November 2004.
{{refend}}

==External links==
{{Library resources box|by=yes|onlinebooks=yes|viaf=73885426}}
* {{Gutenberg author |id=Gregory,+Lady | name=Lady Gregory}}
* {{Internet Archive author|name=Lady Gregory}}
* {{Librivox author |id=8167}}
* {{UK National Archives ID}}
* {{NPG name|name=Isabella Augusta, Lady Gregory}}
* [http://www.sacred-texts.com/neu/celt/vbwi/ ''Visions and Beliefs in the West of Ireland'']
* [http://digital.library.upenn.edu/women/gregory/theatre/theatre.html ''Our Irish Theatre'']
* {{LCAuth|n80056886|Lady Gregory|108|ue}}
* Archival Material at {{wikidata|qualifier|property|P485|Q24568958|P856|format=\[%q %p\]}} 

{{featured article}}

{{Authority control}}

{{DEFAULTSORT:Gregory, Lady Augusta}}
[[Category:1852 births]]
[[Category:1932 deaths]]
[[Category:Abbey Theatre]]
[[Category:Anglo-Irish artists]]
[[Category:Anglo-Irish people]]
[[Category:Deaths from cancer in Ireland]]
[[Category:Deaths from breast cancer]]
[[Category:Irish diarists]]
[[Category:20th-century Irish dramatists and playwrights]]
[[Category:Irish philanthropists]]
[[Category:Irish women poets]]
[[Category:Irish women writers]]
[[Category:Irish translators]]
[[Category:Patrons of literature]]
[[Category:Patrons of music]]
[[Category:People from County Galway]]
[[Category:Translators from Irish]]
[[Category:Irish Anglicans]]
[[Category:Irish women dramatists and playwrights]]
[[Category:19th-century Irish dramatists and playwrights]]
[[Category:19th-century women writers]]
[[Category:20th-century Irish writers]]
[[Category:20th-century women writers]]
[[Category:Women memoirists]]
[[Category:19th-century translators]]
[[Category:20th-century translators]]