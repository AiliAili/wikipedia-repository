{{Orphan|date=March 2016}}

{{Infobox person
| name        = Lexi duPont
| birth_name  = Alexis duPont
| birth_date  = February 16, 1989
| birth_place = Sun Valley, Idaho
| education = Community School, Sun Valley, Idaho; University of Colorado Boulder 
| occupation  = Professional Freeskier
}}

'''Alexis "Lexi" duPont''' (born February 16, 1989) is an American professional big mountain freeskier. She is a Freeskiing World Tour contender<ref>{{cite web|url=http://freerideworldtour.com/riders/alexis-dupont|website=Freeride World Tour}}</ref> and former Junior Olympics competitor,<ref name="discreteclothing.com">{{cite web|url=http://www.discreteclothing.com/team-member/lexi-dupont/|title=Lexi Dupont|work=Discrete}}</ref> who has been featured in several films such as Warren Miller’s ''Wintervention''<ref>{{cite web|url=http://www.skinet.com/warrenmiller/athletes/five-question-interview-lexi-dupont%7cwebsite=SkiNet.com|title=Chasing Shadows Athletes|work=Warren Miller Entertainment}}</ref> and ''Tracing Skylines''.<ref>{{cite web|url=http://www.powder.com/stories/first-chair-blog/retracing-skylines/|title=Retracing Skylines|website=Powder.com}}</ref> Her mother, freestyle skier Holley duPont, was one of the first women to land a backflip on skis.<ref>{{cite web|url=http://xgames.espn.go.com/skiing/article/6581846/qa-freeskiing-world-tour-lexi-dupont%7cwebsite=ESPN|title=Q&A with the Freeskiing World Tour's Lexi DuPont|work=X Games}}</ref>

== Personal life ==
One of three daughters of Holley and Chris duPont, duPont was born in [[Sun Valley, Idaho]],<ref name="discreteclothing.com"/> After graduating from the [[Community School (Sun Valley, Idaho)|Community School]] in 2007,<ref name="auto">{{cite web|url=http://www.sunvalleyguide.com/f07/girlsofsummer.htm|website=Sun Valley Guide}}</ref> she moved to [[Beverly, Massachusetts]], to attend [[Endicott College]] on a sailing scholarship,<ref>{{cite web|url=http://espn.go.com/action/photos/gallery/_/id/6581806/image/1/lexi-dupont%7cwebsite=ESPN|title=null - Lexi DuPont - ESPN|work=go.com}}</ref> but after a year decided to enroll in the [[University of Colorado Boulder]] where she majored in fine art.<ref name="auto1">{{cite web|url=http://www.powder.com/stories/poor-boyz-productions-new-girl-lexi-dupont/|website=Powder Magazine}}</ref> In 2010, duPont circumnavigated the globe with [[Archbishop Desmond Tutu]] on the MV Explorer through the study abroad program [[Semester at Sea]].<ref>{{cite web|url=http://espn.go.com/action/photos/gallery/_/id/6581806/image/10/lexi-dupont%7cwebsite=ESPN|title=null - Lexi DuPont - ESPN|work=go.com}}</ref> Since graduating from the University of Colorado Boulder in 2014, her artwork has earned a permanent display at the [[American Indian College Fund]] in Denver.{{citation needed|date=June 2015}}

== Career ==
Lexi’s parents put her in her first set of skis at the age of two.<ref name="auto1"/> Growing up, she pursued ski racing, competing on the Sun Valley ski team and even qualifying for the [[AAU Junior Olympic Games|Junior Olympics]] two years in a row.<ref name="discreteclothing.com"/>  During college, she was a member of the [[University of Colorado]] Free Style Team, and entered the Freeskiing World Tour, placing in the top 10 overall in 2009 and 2010.<ref>{{cite web|url=http://www.eddiebauer.com/first_ascent/first_ascent_team/team.jsp|website=Eddie Bauer}}</ref> After that, she went on to qualify first at [[Revelstoke, British Columbia|Revelstoke]],<ref name="auto2">{{cite web|url=http://subarufreerideseries.com/2012/01/benjamin-ogilvie-lexi-dupont-win-revelstoke-qualifier/|website=Subaru Freeride Series}}</ref> and finished fourth place at the "Red Bull Cold Rush",<ref name="auto3">{{cite web|url=http://www.redbull.com/cs/Satellite/en_INT/Article/Red-Bull-Cold-Rush-2012-–-Photostory-021243174190229|website=RedBull.com}}</ref> in 2012.

duPont was signed to Eddie Bauer’s First Ascent team in 2010.<ref>{{cite web|url=http://blog.eddiebauer.com/2011/04/12/eddie-bauer-announces-new-first-ascent-team-members/|website=Eddie Bauer blog}}</ref><ref>http://www.eddiebauer.com/first_ascent/first_ascent_team/team.jsp</ref> In 2013, she was featured in the ski film “Tracing Skylines,” for which she was nominated for “Best Female Performance” at the Powder Awards,<ref>{{cite web|url=http://www.powder.com/powder-awards/nominees-announced-14th-annual-powder-awards/|website=Powder Magazine}}</ref> and was also featured in [[National Geographic Society|National Geographic]]’s “Powder Highway Webisodes.”<ref>{{cite web|url=http://adventureblog.nationalgeographic.com/2013/02/24/skiing-powder-in-revelstoke-stop-3-on-the-powder-highway-road-trip/|website=National Geographic Adventure Blog}}</ref>

== Philanthropy ==
In 2007, she participated in a six-week volunteer program at M’Lop Tapang Center for Street Children in [[Cambodia]].<ref name="auto"/> Two years later, she climbed [[Mount Kilimanjaro]], to raise funds for children in [[Tanzania|Tanzania, Africa]].<ref>{{cite web|title=Make a Difference: Kilimanjaro climb to benefit orphaned children|url=http://archives.mtexpress.com/index2.php?ID=2005120927&var_Year=%202008&var_Month=05&var_Day=30|website=Idaho Mountain Express}}</ref> She has also volunteered for [[Beadforlife]],<ref>{{cite web|url=https://www.yumpu.com/en/document/view/34013660/annual-report-2009-2010-beadforlife/25|website=BeadForLife}}</ref> and participates in Play Hard Give Back for Higher Ground, helping people with disabilities experience competition and the outdoors without limitations.<ref>{{cite web|url=http://www.playhardgiveback.com/lexidupont|website=PlayHardGiveBack.com}}</ref>

== Films ==
* 2010 | Warren Miller’s “Wintervention”<ref>{{cite web|title=First Ascent – Lexi duPont in Warren Millers WINTERVENTION|url=http://mogulskier.com/2015/01/05/first-ascent-lexi-dupont-in-warren-millers-wintervention/|website=Mogul Skier}}</ref>
* 2012 | Two Plank Productions’ “Because”<ref>{{cite web|title=Because|url=http://www.twoplank.com/because/|website=Two Plank Productions}}</ref>
* 2013 | Doubsatch Collectives’ “AK Our Way”<ref>{{cite web|title="Dubsatch Collective recaps radical Alaskan adventure in short film, "AK Our Way""|url=http://freeskier.com/videos/dubsatch-collective-recaps-radical-alaskan-adventure-in-short-film-ak-our-way|website=Freeskier}}</ref>
* 2013 | Poor Boyz Productions’ “Tracing Skylines”<ref>{{cite web|title=PBP TRACING SKYLINES Segment|url=https://vimeo.com/113749887|website=Vimeo}}</ref>
* 2013 | National Geographic’s “Powder Highway Road Trip” Webisodes<ref>{{cite web|title=Powder Highway Road Trip – Stop #3 – Revelstoke, BC (Photos +Video)|url=http://adventureblog.nationalgeographic.com/2013/02/24/skiing-powder-in-revelstoke-stop-3-on-the-powder-highway-road-trip/|website=National Geographic Adventure Blog}}</ref>
* 2014 | Poor Boyz Productions’ “Twenty”<ref>{{cite web|title=Lexi duPont Charges in new Poor Boyz Twenty Segment|url=http://blog.eddiebauer.com/2014/12/13/lexi-du-pont-charges-new-poor-boyz-twenty-segment/|website=Eddie Bauer Blog}}</ref>
* 2014 | Unicorn Picknick’s “Pretty Faces”<ref>{{cite web|title=Pretty Faces - OFFICIAL TEASER - all female SKI film|url=https://www.youtube.com/watch?v=f38jyLPIkok|website=YouTube}}</ref>

== Awards & Nominations ==
* 2010 | 3rd Place Freeskiing World Tour, Telluride, Colo.<ref>{{cite web|url=http://espn.go.com/action/freeskiing/news/story?id=4893044|website=ESPN}}</ref>
* 2012 | 1st Place Freeskiing World Tour Revelstoke Qualifier<ref name="auto2"/>
* 2012 | 4th Place Red Bull Cold Rush<ref name="auto3"/>
* 2013 | Nomination for “Best Female Performance” at The Powder Awards<ref>{{cite web|url=http://www.powder.com/powder-awards/2014-best-female-performance/|website=Powder Magazine}}</ref>

== References ==
{{Reflist}}

{{DEFAULTSORT:duPont, Lexi}}
[[Category:American female freestyle skiers]]
[[Category:Living people]]
[[Category:1989 births]]
[[Category:Endicott College alumni]]
[[Category:University of Colorado Boulder alumni]]