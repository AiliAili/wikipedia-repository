<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
  |name = Be-200 Altair
  |image = MChS Beriev Be-200 waterbomber.jpg
  |caption = Be-200 
}}{{Infobox aircraft type
  |type = Multirole amphibian
  |national origin = [[Russia]]
  |manufacturer = [[Irkut (aircraft manufacturer)|Irkut]]
  |designer = [[Beriev Aircraft Company|Beriev]]
  |first flight = 24 September 1998
  |introduced = 2003
  |retired =
  |number built = 10<ref name="bmpd.livejournal.com">http://bmpd.livejournal.com/2371802.html</ref> <!--8 production + 2 prototypes-->
  |status = Operational / In production<ref>
 {{cite news
 |url=http://armstrade.org/includes/periodics/news/2017/0112/145539049/detail.shtml
 |title=ТАНТК передал МЧС России первый серийный самолет Бе-200ЧС
 |work=[[armstrade.org]]
 |date=2017-01-12
 }}</ref>
  |primary user = [[EMERCOM]]
  |more users = 
  |unit cost =
  |developed from = [[Beriev A-40]]
  |variants with their own articles = 
}}
|}
[[File:Beriev Be-200 Israel 5-12-2010.jpg|250px|thumb|right|Beriev Be-200 filling water tanks in the [[Mediterranean Sea]] while in operation in [[Mount Carmel forest fire (2010)|Mount Carmel forest fire]] in Israel]]

The '''Beriev Be-200 Altair''' ({{lang-ru|Бериев Бе-200}}) is a multipurpose [[amphibious aircraft]] designed by the [[Beriev|Beriev Aircraft Company]] and manufactured by [[Irkut Corporation|Irkut]].  Marketed as being designed for [[aerial firefighting|fire fighting]], [[search and rescue]], [[maritime patrol]], cargo, and passenger transportation, it has a capacity of 12 [[tonne]]s (12,000 litres, 3,170 US gallons) of water, or up to 72 passengers.<ref name='irkut'>[http://www.irkut.com/en/services/production/BE200/ Irkut—Be 200] {{webarchive |url=https://web.archive.org/web/20080925172707/http://www.irkut.com/en/services/production/BE200/ |date=September 25, 2008 }}</ref>

The name ''Altair'' was chosen after a competition amongst Beriev and Irkut staff in 2002/2003.  The name Altair was chosen as it is not only the name of the [[Altair|alpha star]] in the [[Aquila (constellation)|Eagle constellation]], but also because "Al" is the first part of the name of the [[Beriev A-40|Beriev A-40 ''Albatross'']] amphibious aircraft, whose layout was the development basis for the creation of the Be-200, "ta" stands for [[Taganrog]], and "ir" stands for [[Irkutsk]].<ref name='berpr58'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_58e.html|title=PRESS RELEASE No.58 dt. 12th February 2003. Results of the competition for the best personal names for the Be-103 and the Be-200 amphibious aircraft.|publisher=}}</ref>

==Development==
The  Be-200 was designed by the [[Beriev]] Aviation Company, with the Russian [[Irkutsk]] Aircraft Production Association (now part of the Irkut Corporation). Beriev are responsible for development, design and documentation; systems-, static-, flight- and fatigue-testing of prototypes; certification and support of the production models. Irkut's duties comprise production preparation; manufacture of tooling; production of four prototypes and production aircraft; and spare parts manufacture.<ref name='janes'>{{cite web|url=http://www.janes.com/extracts/extract/jawa/jawa0756.html|title=Defense & Security Intelligence & Analysis: IHS Jane's - IHS|publisher=}}</ref> Both companies now fall under the umbrella of the state-owned [[United Aircraft Corporation]].

Initiated in 1989 under the design leadership of [[Alexander Yavkin]], Russian government approval for a purpose-designed water bomber was granted on 8 December 1990. Details of the project were announced, and a model displayed at the 1991 Paris Air Show.<ref name='janes' />

Beriev developed unique fire-fighting equipment for the Be-200, allowing it to scoop water while skimming the water surface at 90-95% of takeoff speed. This system was developed using a specially modified [[Be-12]]P, coded '12 Yellow'. After installation of the fire-fighting system, the aircraft was registered RA-00046 and given the designation Be-12P-200. This modified Be-12 was used to develop both the fire-fighting system and methods of operation.<ref name='p79-80'>Gordon, Sal'nikov and Zablotskiy 2006, pp. 79-80.</ref>

The Be-200's first flight from land was scheduled for 1997, but was eventually achieved by the first prototype aircraft on 24 September 1998.<ref name='berpr41'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_41e.html|title=PRESS RELEASE No.58 dt. 12th February 2003. Results of the competition for the best personal names for the Be-103 and the Be-200 amphibious aircraft.|publisher=}}</ref> The aircraft was then transferred from Irkutsk to [[Taganrog]] after 26½ flying hours, and the first take off from water was conducted on 10 September 1999 in Taganrog.<ref name='berpr35'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_35e.html|title=PRESS RELEASE No.58 dt. 12th February 2003. Results of the competition for the best personal names for the Be-103 and the Be-200 amphibious aircraft.|publisher=}}</ref> The second Be-200 flew on August 27, 2002. This aircraft was built as a '''Be-200ES''', being fitted to the specifications of the launch customer, [[EMERCOM]], the Russian Ministry of Emergency Situations.<ref name='janes' /><ref name='berpr54'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_54e.html|title=PRESS RELEASE|publisher=}}</ref>

In 2001, as part of a marketing program, the Be-200 was displayed at two large exhibitions in the [[Pacific Ocean]] region; the International Maritime and Aerospace Exhibition LIMA'01 in [[Malaysia]] and the Korean Aerospace and Defence Exhibition KADE'01 in [[South Korea]].<ref name='berpr47'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_47e.html|title=PRESS RELEASE No.58 dt. 12th February 2003. Results of the competition for the best personal names for the Be-103 and the Be-200 amphibious aircraft.|publisher=}}</ref>

In 2002, the Be-200 participated in international aviation exhibitions, successfully demonstrating its capabilities to potential customers in [[France]] and [[Greece]] with 15 demonstration flights made from land, eight from water. A total of over 7,600&nbsp;km  was flown across Europe.<ref name='berpr51'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_51e.html|title=PRESS RELEASE|publisher=}}</ref>

Irkut and [[EADS]] signed a memorandum of understanding in May 2002 to jointly carry out a market study and to define the conditions and costs of international certification and the logistics of setting up a worldwide after-sales service. The study was completed in July 2003, revealing a potential market for up to 320 aircraft over 20 years. The two companies, with [[Rolls-Royce Deutschland]], plan to obtain Western certification during 2008/9 and offer a [[Rolls-Royce BR700|Rolls-Royce BR715]] powered aircraft for Western markets.<ref name='irkut' /><ref name='janes' /> This version is to be given the designation '''Be-200RR'''. The original Be-200 prototype (RF-21511) has been earmarked to be converted into the Be-200RR prototype.<ref name='p87-88'>Gordon, Sal'nikov and Zablotskiy 2006, pp. 87-88.</ref>

The first production aircraft, a Be-200ES flew on June 17, 2003. It was delivered to EMERCOM on July 31, 2003. Seven aircraft have been ordered by EMERCOM, five have been delivered. The fifth airframe was used for the European certification process which was expected to be completed in 2008. EASA eventually certified the Be200ES-E on September 7, 2010. The remaining two were scheduled for delivery by the end of 2008.<ref name='irkut' /> EMERCOM has an option to buy a further 8 Be-200s, with a decision expected during the latter part of 2008.<ref name='FI'>[http://www.flightglobal.com/articles/2008/09/11/315754/russias-oak-in-bid-to-boost-sales-of-be-200.html Flightglobal—Russia's OAK in bid to boost sales of Be-200]</ref>

In 2010, production of the Be-200 was decided to switch to the ''Centre of competence for amphibian aircraft and flying boats'' in Taganrog. An assembly line was set up using tooling and equipment received from Irkut's main manufacturing site in Irkutsk, Siberia. This will allow the Irkut Corporation to concentrate on other more lucrative projects. Production of the Be-200 will remain under the umbrella of the United Aircraft Corporation.<ref>[http://www.flightglobal.com/articles/2008/09/11/315755/russia-sets-up-central-production-site-for-amphibious.html Flightglobal—Russia sets up central production site for amphibious aircraft]</ref> First Taganrog-built aircraft was delivered in early 2017.<ref>http://armstrade.org/includes/periodics/news/2017/0112/145539049/detail.shtml</ref>

==Design==
[[File:Beriew Be-200 CH-S Detail Bug 2.jpg|thumb|right|The Be-200's engines are located high and to the rear in order to keep them clear of spray.]]

The multirole Be-200 can be configured as an amphibious water drop fire-fighting aircraft, a freighter, or as a passenger aircraft—the pressurised and air conditioned cabin allowing transportation of up to 72 passengers. The Be-200 can also be equipped for special missions. When configured as an [[air ambulance]], the aircraft can carry up to 30 stretcher patients and seven seated patients or medical crew. In the [[search and rescue]] role, the aircraft can be equipped with searchlights and sensors, an inflatable boat, thermal and optical surveillance systems, and medical equipment. The search and rescue variant can accommodate up to 45 persons. The aircraft is also capable of being configured for [[anti-submarine warfare]] duties.<ref name='irkut'/><ref name='EADS'>[http://www.irkutseaplane.com/be200.html EADS-Irkut Seaplane—Be-200]</ref>

The Be-200 fire-fighter suppresses fires by dropping water and/or chemical retardants. Eight ferric aluminium alloy water tanks are located under the cabin floor in the centre fuselage section. Four retractable water scoops, two forward and two aft of the fuselage step can be used to scoop a total of 12 tonnes of water in 14 seconds. Alternatively, the tanks can be filled from a hydrant or a water cistern on the ground. The water tanks can be removed quickly for carrying cargo. Water can be dropped in a single salvo, or in up to eight consecutive drops. The aircraft also carries six auxiliary tanks for fire-retarding chemical agents, with a total capacity of 1.2&nbsp;m³. The aircraft can empty its water tanks over the site of a fire in 0.8 to 1.0 seconds when flying above the minimum drop speed of 220&nbsp;km/h (135&nbsp;mph, 120&nbsp;kn)<ref name='irkut' />

The aircraft is powered by two, over fuselage, pylon-mounted [[Progress D-436|D-436TP]] engines. The D-436TP is a specific "maritime" corrosion-resistant version of the [[Progress D-436|D-436]] three shaft turbofan engine, designed especially for the Be-200 amphibian, by [[Ivchenko Progress]] ZMKB and manufactured by "[[Motor-Sich|Motor Sich]]" in [[Ukraine]]. These are mounted above the wingroot pods on the landing gear fairings to prevent water spraying into the engines during take-off and landing.<ref name='irkut' /><ref name='EADS' />

The Digital Flight Control ([[Fly-by-wire]]) cockpit is fitted with modern navigation systems such as satellite navigation ([[GPS]]), [[Flight management system|FMS]], [[autopilot]] and [[weather radar]]. The [[ARIA 200-M]] all-weather integrated avionics system developed by Honeywell with the Moscow Research Institute of Aircraft Equipment, uses six 152 x 203&nbsp;mm (6 x 8&nbsp;in) [[LCD]]s to [[glass cockpit|display information]] to the two-man crew.<ref name='EADS' />

The Be-200 is a high-wing [[T-tail]] [[monoplane]]. the hull is of single step design with a high length-to-[[Beam (nautical)|beam]] ratio, which contributes to stability and controllability in water. The Be-200 airframe is constructed of aluminium alloys with corrosion-protection treatments. Selective use is made of titanium, composites and other corrosion-free materials. The wings are fitted with underwing stabiliser floats. The hydraulically operated retractable landing gear units all retract rearward, and each unit is twin-wheeled. A water rudder provides steering in the water.<ref name='EADS' />

The Be-200 can operate from either a 1,800&nbsp;m long runway or an area of open water not less than 2,300&nbsp;m long and 2.5&nbsp;m deep, with waves of up to 1.3&nbsp;m high.<ref name='irkut' />

==Operational history==
[[File:Beriev Be-200 operating in Greece, Aug2007.jpg|thumb|right|The Be-200 was operated in Greece during [[2007 Greek forest fires|fires in the summer of 2007]], but has yet to secure any firm orders from Europe.]]

The first documented operational use of the Be-200 was from 20 August till 30 September 2004. For this period a Be-200ES was operated from Sardinia ([[Italy]]) by SOREM, the official operator of fire-fighting equipment of Italian Civil Defense Department ([[Protezione Civile]]). The aircraft, flown by joint Russian-Italian flight crew performed more than 100 flights with about 90 hours flown. During seven hours of operational flights, the aircraft participated in the extinguishing of four forest fires and dropped 324 tons of water.<ref name='berpr78'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_78e.html|title=Press release|publisher=}}</ref>

The partnership was renewed in 2005, with Be-200ES (Reg. RF-21512) based at Sardinia between July and September. The aircraft flew 150 hours covering 63 missions, including ferry flights, and 435 scoops and drops of water, the total mass of which exceeded 3,175&nbsp;tonnes (3,500&nbsp;tons).<ref name='berpr93'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_93e.html|title=Press release|publisher=}}</ref>

In 2006 RF-21512 was leased by the Portuguese Fire Fighting Services (SNBPC&nbsp;– Serviço Nacional de Bombeiros e Protecção Civil) for evaluation during the forest fire season. On 6 July 2006, the Russian crew of the Be-200 leased by [[Portugal]] were carrying out a water pick-up from the [[Aguieira Dam|Aguieira dam]] near [[Santa Comba Dão]], when on climbing away they clipped treetops and at least one of the Progress D-436TP engines suffered ingestion damage and had to be shut down. The aircraft recovered safely to land at [[Monte Real]] air force base, from which it had been operating. After repairs, the aircraft completed the season in Portugal. In total, 42 flights were performed during operations in Portugal, with a total flying time of 119 hours. The aircraft performed 301 water scooping operations and dropped 2,167&nbsp;tonnes (2,389&nbsp;tons) of water on fires.<ref name='berpr106'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_106e.html|title=Press release|publisher=}}</ref><ref>[http://www.flightglobal.com/articles/2006/07/18/207849/russian-firefighting-beriev-be-200-under-evaluation-by-portugal-stokes-forest-fire-by-dumping-fuel.html Flightglobal—Russian firefighting Be-200 under evaluation by Portugal stokes forest fire by dumping fuel]</ref>

In October 2006, two Be-200ES (RF-32765 and RF-32768) were leased to [[Indonesia]] by EMERCOM, fighting fires<ref>''See [[2006 Southeast Asian haze]] for more details on the 2006 fires in Indonesia''</ref> for 45 days.<ref name='irkutnews'>[http://www.irkutseaplane.com/news.html Irkut—News]</ref> This reportedly cost Indonesia around US$5.2 million.<ref>[http://www.indonesia-ottawa.org/information/details.php?type=news_copy&id=3108 Antara News Agency (November 2, 2006)—Two Russian water bombing aircraft to be operated for 10 days in Sumatra]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> These operations prompted press reports that the Indonesian government had agreed to purchase two Be-200s, each with a projected price of US$40 million.<ref>[http://www.angkasa-online.com/public/print/17/6/138.htm Angkasa No.06—March 2007] {{webarchive |url=https://web.archive.org/web/20080929000000/http://www.angkasa-online.com/public/print/17/6/138.htm |date=September 29, 2008 }}</ref> Beriev, however has not confirmed these reports.

Such was the success of the first campaign that two Be-200ES were again leased by Portugal from July 10 to September 30, 2007. During this period 58 fire-fighting flights were conducted with a total flying time of over 167 hours. 2,322&nbsp;tonnes (2,560&nbsp;tons) of water was dropped. Beriev claims that representatives of the newly formed Portuguese government enterprise EMA (Empresa de Meios Aéreos) have expressed their interest in a long-term cooperation with Beriev and the Be-200 in Portugal.<ref name='berpr118'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_118e.html|title=Press release|publisher=}}</ref>

Two Be-200ES also operated in Greece, RF-32768 fighting the [[2007 Greek forest fires|2007 forest fires]] for the whole season and RF-21512 from August 30 to September 13.<ref name='berpr116'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_116e.html|title=Press release|publisher=}}</ref>

In April 2008, [[Azerbaijan]] became the first foreign customer for the Be-200, purchasing one Be-200ES from Russia. The aircraft (reg. FHN-10201 ex RF-32769) is operated by the Azerbaijan [[Ministry of Emergency Situations (Azerbaijan)|Ministry of Emergency Situations]] and can be operated as a fire-fighting, cargo and 43-seater passenger aircraft.<ref name='berpr125'>{{cite web|url=http://www.beriev.com/eng/Pr_rel_e/pr_125e.html|title=Press release|publisher=}}</ref>

In July/August 2010 it was used in Russia during the wildfires that spread across the country.<ref>[http://hvylya.org/index.php?option=com_content&view=article&id=6650:2010-07-30-09-05-35&catid=4:2009-04-12-12-01-18&Itemid=10 Лесные пожары в России (Выкса, Воронеж, Рязань и др.): свидетельства очевидцев, фото, видео] hvylya.org 30.07.2010 10:21	</ref>

In early December 2010, two Be-200ES aircraft were used to fight the [[2010 Mount Carmel forest fire]] near Haifa, Israel.<ref name='berpl27'>[http://www.itar-tass.com/eng/level2.html?NewsID=15746515&PageNum=0 EMERCOM planes start putting out fires in Israel]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref name='berpl28'>{{cite web|url=http://sputniknews.com/voiceofrussia/2010/12/05/36249701.html|title=Russia sends another Be-200 to fight Haifa blaze|date=5 December 2010|publisher=}}</ref>

In [[Serbia]] one Be-200 belonging to Russian [[Ministry of Emergency Situations (Russia)|Ministry of Emergency Situations]] is stationed in summer on [[Niš Constantine the Great Airport]]. Aircraft has already operational history in 2012 and 2013.<ref>{{cite web|url=http://www.blic.rs/Vesti/Reportaza/398374/Ruski-piloti-iz-Nisa-vode-bitku-sa-pozarima-u-Srbiji|title=Ruski piloti iz Niša vode bitku sa požarima u Srbiji|date=27 November 2015|publisher=}}</ref>

A Be-200 was dispatched by Russia in January 2015 to assist in search and recovery operations following the loss of [[Indonesia AirAsia Flight 8501]] in the [[Java Sea]].<ref>{{cite news |url=http://sputniknews.com/asia/20150104/1016499557.html |title=Russian Divers Begin Search for Black Boxes From AirAsia QZ8501 |newspaper=Sputnik |date=4 January 2015 |accessdate=10 January 2015}}</ref> On October 20, 2015, two Be-200s were used by the [[Government of Indonesia|Indonesian government]] to fight a forest fire in [[Sumatra]].

In August 2016 two Be-200 aircraft were sent to Portugal after being asked for help in extinguishing forest fires.<ref>{{cite web|title=Russia Provides Two Be-200 Aircraft on Portuguese Fire-Fighting Mission|url=http://sputniknews.com/latam/20160812/1044211238/portugal-fire-help-russia.html|website=sputniknews.com|publisher=sputniknews|accessdate=18 August 2016}}</ref> Four days later it was reported that thanks to the work of the Russian Emergencies Ministry Be-200 pilots, the fire was prevented from spreading in the direction of two settlements — [[Castro Laboreiro]] with a population of 1,000 people and Viaden de Baixo, where 15 farms were saved from fire, and [[Peneda-Gerês National Park]].<ref>{{cite web|title=Russian Emergencies Ministry’s Be-200 Aircraft Put Out 2 Fires in Portugal|url=http://sputniknews.com/europe/20160816/1044311749/be-200-portugal-fire.html|website=sputniknews.com|publisher=sputniknews|accessdate=18 August 2016}}</ref> China bought 4 Be-200 during Zhuhai Airshow 2016.

==Variants==
[[File:Beriew Be-200 at MAKS-2009.jpg|right|thumb|Be-200ChS at the 10th edition of the [[MAKS Airshow]]]]

*'''Be-200''': Basic multirole model
*'''Be-200ChS''' (as it is more often known, a transcription of the Russian '''Бе-200ЧС''') or '''Be-200ES''' (Emergency Services): Multirole model fitted to the requirements of the Russian Ministry of Emergency Situations
*'''Be-200E''': English cockpit version of the Be-200ES<ref name='EADS2'>[http://www.irkutseaplane.com/certification.html EADS-Irkut Seaplane—Be-200E]</ref>
*'''Be-200RR''': Designation of projected [[Rolls-Royce plc|Rolls-Royce]] engined variant.<ref name='p87-88' /> However, little progress toward a Rolls-Royce version has been made since a joint study concluded in 2004 that because the intended [[Rolls-Royce BR715|BR715]] engine would require extensive redevelopment due to weight and salt-water corrosion issues, break-even would require sales of too many aircraft. <ref>{{cite web|url=https://www.flightglobal.com/news/articles/r-r-powered-be-faces-uncertain-future-191693/ |title=R-R-powered Be-200 faces uncertain future|publisher=Flight International |date=2004-12-21 |accessdate=2017-01-26}}</ref>
*'''Be-210''': Projected passenger-only model<ref name='EADS3'>[http://www.irkutseaplane.com/be210.html EADS-Irkut Seaplane—Be-210]</ref>
*'''Be-220''': Projected [[Maritime patrol aircraft|maritime patrol]] variant.<ref name='p92'>Gordon, Sal'nikov and Zablotskiy 2006, p. 92.</ref>

==Operators==
; {{RUS}}
* [[Ministry of Emergency Situations (Russia)|Ministry of Emergency Situations]]: 7 Be-200ES delivered,<ref name="bmpd.livejournal.com">http://bmpd.livejournal.com/2371802.html</ref> 10 on order ''(8 of 10 purchased on 9th Sep 2010)''<ref name='FI' /><ref>[http://en.rian.ru/russia/20100909/160529918.html Russian Emergencies Ministry buys eight Be-200 aircraft to tackle wildfires] Wildfires in Russia in 2010</ref>
* [[Ministry of Defence (Russia)|Ministry of Defence]]: 6 Be-200 ordered. The first two will be the Be-200ChS models and the last four Be-200PS models.<ref>{{cite web |url=http://en.rian.ru/military_news/20130524/181331305/Russian-Military-Orders-6-Be-200-Amphibious-Planes.html |title=Russian Military Orders 6 Be-200 Amphibious Planes|publisher=RIANOVSTI|date=2013-05-24|accessdate=2013-05-25}}</ref>
; {{AZE}}
* [[Ministry of Emergency Situations (Azerbaijan)|Ministry of Emergency Situations]]: 1 Be-200ES<ref name='berpr125' /><ref>{{cite web|url=http://economics.apa.az/en/news.php?id=34339 |title=Azeri-Press Agency—Azerbaijan buys Be-200 from Russia |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20110706130737/http://economics.apa.az/en/news.php?id=34339 |archivedate=2011-07-06 |df= }}</ref>

==Specifications (Be-200)==
[[File:Beriev Be-200 svg.svg|right|400px]]
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
|ref=''Irkut Website''<ref name='irkut' /> and ''EADS-Irkut Seaplane Website''<ref name='EADS' />
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. -->
|crew=2
|length main=32.0 m
|length alt=105 ft 0 in
|span main=32.8 m
|span alt=107 ft 7 in
|height main=8.9 m
|height alt=29 ft 2 in
|area main=117.4 m²
|area alt=1,264 ft²
|empty weight main=27,600 kg
|empty weight alt=60,850 lb)
* '''Max Take Off Weight (Land):''' 41,000 kg (90,390 lb)
* '''Max Take Off Weight (Water):''' 37,900 kg (83,550 lb)
* '''Max Capacity (Water or Retardant):''' 12,000 kg (26,450 lb)
* '''Max Capacity (Cargo):''' 7,500 kg (16,530 lb)
* '''Max Capacity (Passengers):''' 44 (Be-200ES) 72 (Be-210
|engine (jet)= [[Progress D-436|Progress D-436TP]]
|type of jet= [[turbofan]]s
|number of jets= 2
|thrust main=7,500 kgf
|thrust alt=16,534 lbf
|max speed main=700 km/h
|max speed alt=435 mph
|cruise speed main=560 km/h
|cruise speed alt=348 mph)
* '''Economy speed:''' 550 km/h (342 mph)
* '''Landing speed:''' 200 km/h (124 mph)
* '''Takeoff speed:''' 220 km/h (137 mph)
* '''Minimum speed (Flaps 38°):''' 157 km/h (98 mph
|ceiling main= 8,000 m
|ceiling alt= 26,246 ft
|range main=2,100 km
|range alt=1,305 mi)
* '''Ferry range (One Hour Reserve):''' 3,300 km (2,051 mi
|climb rate main= 13 m/s
|climb rate alt= 2,600 ft/min) (At Sea Level and MTOW—Flaps 20°)
* '''Rate of climb:''' 17 m/s (3,350 ft/min) (At Sea Level and MTOW—Flaps 0°
|loading main=
|loading alt=
|thrust/weight=
|avionics=* ARIA 200-M integrated avionics system.
|armament=
}}

==See also==
{{Aircontent
|related=
* [[Beriev A-40]]
|similar aircraft=
* [[Bombardier 415]]
* [[ShinMaywa US-2]]
* [[AVIC TA-600]]
|see also=
|lists=
* [[List of seaplanes and flying boats]]
}}

==References==

===Notes===
{{Reflist|2}}

===Bibliography===
{{Refbegin}}
* Yefim Gordon, Andrey Sal'nikov and Aleksandr Zablotskiy (2006). ''Beriev's Jet Flying Boats''. Hinckley, UK: Midland Publishing. ISBN 1-85780-236-5.
{{Refend}}

==External links==
{{Commons category|Beriev Be-200}}
* [http://www.beriev.com/eng/Be-200_e/Be-200_e.html Beriev (Aircraft Designer) Official Site]
* [https://web.archive.org/web/20080925172707/http://www.irkut.com:80/en/services/production/BE200/ Irkut (Aircraft Manufacturer) Official Site]
* [http://www.irkutseaplane.com/be200.html EADS-Irkut Seaplane (Aircraft Sales & Marketing Partners) Official Site]
* [http://www.russiatoday.ru/spotlight/release/1475/video RussiaToday: Spotlight - Be-200—Russia’s aviation breakthrough]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}—An interview with Oleg Demchenko, President of the Irkut Aviation Corporation, about the Be-200, its lack of European sales, and the state of the Russian aviation industry.
* [http://www.flightglobal.com/pdfarchive/view/1998/1998%20-%200030.html "Mixing Fire and Water"]—a 1998 ''Flight'' article on the Beriev BE-200
* [http://www.redstar.gr/Foto_red/Eng/Aircraft/Be_200.html Beriev, Be-200 Multipurpose amphibian aircraft]

{{Beriev aircraft}}

[[Category:Aerial firefighting]]
[[Category:Amphibious aircraft]]
[[Category:Beriev aircraft|Be-0200]]
[[Category:Flying boats]]
[[Category:High-wing aircraft]]
[[Category:T-tail aircraft]]
[[Category:Jet seaplanes and flying boats]]
[[Category:Russian civil utility aircraft 2000–2009]]
[[Category:Twinjets]]