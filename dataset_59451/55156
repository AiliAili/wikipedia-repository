{{DISPLAYTITLE:Zayed Port}}
{{Infobox Port
| name               = Zayed Port  
| image              = Mina Zayed.jpg
| caption            = Quayside at Abu Dhabi's Zayed Port
| blankdetailstitle1 = Native name
| blankdetails1      = ميناء زايد (Arabic)
<!-- Location -->
| country            = [[Emirate of Abu Dhabi]]
| location           = [[Abu Dhabi]]
| coordinates        = {{Coord|24|51|00|N|54|36|00|E|region:AE_type:landmark|display=title,inline}}
<!-- Details -->
| opened             = 1972
| operated           = [[Abu Dhabi Ports]]
| owner              = [[Abu Dhabi Ports]]
| type               = 
| sizewater          = 
| sizeland           = 
| size               = 535 hectares of which 41 were previously occupied by the [[container terminal]]
| berths             = 
| wharfs             = 
| piers              = 
| employees          = 
| leadershiptitle    = 
| leader             = 
| blankdetailstitle2 = 
| blankdetails2      = 
| blankdetailstitle3 = 
| blankdetails3      = 
<!-- Statistics -->
| arrivals           = 
| cargotonnage       = 900 000 ton
| containervolume    = 
| cargovalue         = 
| passengertraffic   = 
| revenue            = 
| profit             = 
| blankstatstitle1   = 
| blankstats1        = 
| blankstatstitle2   = 
| blankstats2        = 
| blankstatstitle3   = 
| blankstats3        = 
| website            = [http://www.adpc.ae/ Abu Dhabi Ports]
}}

'''Zayed Port''' ({{Lang-ar|ميناء زايد}}) also referred to as '''Mina Zayed''', is a commercial deep-water port owned by the [[Abu Dhabi Ports]] that serves [[Abu Dhabi]], [[United Arab Emirates]]. Established in 1968, Zayed Port is located in the northeast section of Abu Dhabi city. It was officially inaugurated and became fully operational in 1972,<ref>[http://www.admedia.ae/celebrate40/en/events/event-39 "Sheikh Zayed Inaugurates Zayed Port"], Abu Dhabi Media. Retrieved 2013-06-10</ref> and is named after Sheikh [[Zayed bin Sultan Al Nahyan]], the former [[President of the United Arab Emirates]]. The transfer of Zayed Port's container traffic to the newly developed US$7.2 billion [[Khalifa Port]] container terminal was completed in December 2012.<ref>[http://www.emirates247.com/business/economy-finance/khalifa-port-now-fully-operational-2012-12-09-1.486555 "Khalifa Port now fully operational"], Emirates 24/7. 2012-12-09. Retrieved 2013-04-09</ref>

==Geography==
Zayed Port covers an area of 535&nbsp;hectares and contains 21&nbsp;berths with depths ranging from 6 to 15&nbsp;metres and a total berth length of 4,375&nbsp;metres. It's one of four major ports in the emirate: the city's Zayed Port remains the gateway for general cargo vessels, [[RORO]] (roll-on/roll-off) and is emerging as a destination for international luxury cruise ship tourism;<ref>{{cite web|title=Major local cruise industry expansion plans detailed at Arabian Travel Market|url=http://www.cruisearabiaonline.com/Industry-Focus/2014/05/08/Major-local-cruise-industry-expansion-plans-detailed-at-Arabian-Travel-Market|website=Cruise Arabia & Africa|accessdate=12 January 2015}}</ref> the nearby Free Port caters to smaller vessels, tugs, [[barge]]s and service crafts; [[Musaffah Port]] is located in the heart of the industrial township of [[Musaffah]]; while the new state-of-the-art [[Khalifa Port]] in [[Taweelah]] handles all the emirate's container shipping.

==History==
In light of the increasing importance of container shipping, a well-equipped container terminal was established at Zayed Port back in 1982. Covering an area of 41 hectares, the terminal had a storage capacity of 15,000 TEUs at any given time. Four deep water berths were provided with a total length of 931 metres and 15 metres depth. The berths were equipped with five 40-tonne cranes. As a result, Zayed Port's throughput in 1998 increased 34 per cent in container volume and 25 per cent in general cargo over 1997. The port's capacity to hold chilled, cool and frozen products was significantly increased when a 15,000 tone cold store became operational in 1999.<ref>[http://www.arabiansupplychain.com/article-76-top-10-middle-east-ports/1/print/ "Top 10 Middle East Ports"], Arabian Supply Chain. 2006-31-10. Retrieved 2013-06-10</ref>

Benefiting from its strategic location in the heart of the capital, Zayed Port was instrumental in bolstering Abu Dhabi’s international trade, being the main gateway for trade in the emirate and playing a pivotal role in supporting Abu Dhabi’s Economic Vision 2030, the government’s drive to achieve economic diversification.<ref>[http://www.uaeinteract.com/docs/Mina_Zayed_celebrates_40_years_of_success_as_Abu_Dhabi_Main_Trade_Gateway_/50271.htm "Mina Zayed celebrates 40 years of success as Abu Dhabi Main Trade Gateway"], UAE Interact. 2012-07-10. Retrieved 2013-06-10</ref>

As part of this vision, all container shipping in Zayed Port was eventually moved to the Abu Dhabi Ports' newly developed AED 26.5 billion AED (USD 7.2 billion) megaproject [[Khalifa Port]] in Taweelah between [[Abu Dhabi]] and [[Dubai]]. Following the 100% TEU traffic transition from Zayed Port in September 2012, commercial operations at this new flagship state-of-the-art gateway were officially inaugurated by the [[President of the United Arab Emirates]], Sheikh [[Khalifa Bin Zayed Al Nahyan]] on 12 December 2012.<ref>[http://www.thenational.ae/thenationalconversation/industry-insights/shipping/opening-of-khalifa-port-marks-uae-milestone Opening of Khalifa Port marks UAE milestone], The National, 13 December 2012</ref>

Zayed Port is now the focus of the [[Abu Dhabi Ports]]'s plans to redevelop the port as a major luxury cruise ship terminal.<ref>[http://m.gulfnews.com/business/abu-dhabi-ports-company-looking-forward-to-new-direct-destinations-1.1185097 "Abu Dhabi Ports Company looking forward to new direct destinations"], Gulf News. 2013-18-05. Retrieved 2013-06-10</ref> Abu Dhabi is currently a seasonal home port for [[AIDA Cruises]], while [[Celebrity Cruises]] announced in December 2014 that they would be home porting their cruise ship [[Celebrity Constellation]] in Abu Dhabi from November, 2016 to January 2017.<ref>{{cite web|title=Celebrity Cruises confirm deployment of ship to Middle East cruise market out of Abu Dhabi|url=http://www.cruisearabiaonline.com/News/2015/01/04/Celebrity-Cruises-confirm-deployment-of-ship-to-Middle-East-cruise-market-out-of-Abu-Dhabi|website=Cruise Arabia & Africa|accessdate=12 January 2015}}</ref>

In 2014 the [[Red Bull Air Race World Championship]] season opened in Zayed Port.<ref>[http://www.redbullairrace.com/en_AE/event/abu-dhabi "Red Bull Air Race"] Retrieved 2014-04-16</ref> This was the seventh occasion the competition was hosted in Abu Dhabi and marked the first time the city saw the event since 2010, when it had hosted the opener for six consecutive years starting from 2005.<ref>[http://www.khaleejtimes.com/DisplayArticle08.asp?xfile=data/citytimes/2010/March/citytimes_March119.xml&section=citytimes "Abu Dhabi opens skies for Air Race"], Khaleej Times. 2010-13-03. Retrieved 2013-06-10</ref>

==See also==
{{portal|Abu Dhabi|Asia|Middle East|United Arab Emirates}}
* [[Al Ain]]
* [[Abu Dhabi]], the capital of the [[UAE]].
* [[Khalifa Port]], the state-of-the art gateway to Abu Dhabi which handles all of the emirate’s container traffic; the only semi-automated [[container terminal]] in the [[Middle East]].
* [[Kizad]], one of the world's largest free and non-free industrial zones.
* [[Musaffah Port]], the dedicated port for Abu Dhabi's [[Musaffah]] Industrial Zone.

== References ==
{{Reflist|30em}}

==External links==
* [http://www.adterminals.ae/ Abu Dhabi Terminals]
* [http://www.adpc.ae/ Abu Dhabi Ports]
* [http://www.redbullairrace.com/cs/Satellite/en_air/Event/Abu-Dhabi,-UAE-021242804119520/ Abu Dhabi Red Bull Air Race World Championship]
* [http://www.abudhabidesertsafari.ae/mina-port/ Zayed Port]

{{Developments in Abu Dhabi}}
{{Abu Dhabi}}

[[Category:Populated places in Abu Dhabi (emirate)]]
[[Category:Ports and harbours of the United Arab Emirates]]
[[Category:Transport in Abu Dhabi]]