{{Use dmy dates|date=October 2013}}
{{Use South African English|date=October 2013}}
{{Infobox school
| name              = Hoërskool Voortrekker Boksburg
| native_name       = 
| image             = 
| image_size        = 
| alt               = 
| caption           = 
| logo              = VoriesWapen.jpg
| motto             = Vorentoe
| established       = 1920
| closed            = 
| type              = Public High School
| status            = 
| category_label    = 
| category          = 
| gender_label      = 
| gender            = 
| affiliation       = 
| affiliations      = 
| administrator     = 
| assst_admin       = 
| president         = 
| chairman_label    = 
| chairman          = 
| rector            = 
| principal         = 
| asst principal    = 
| campus_director   = 
| headmaster        = MP van Heerden
| head_name         = Second Master
| head              = 
| head_name2        = Assistant Headmaster
| head2             = 
| dean              = 
| founder           = 
| chaplain          = 
| officer_in_charge = 
| faculty           = 
| teaching_staff    = 
| enrollment        = 700+
| grades_label      = 
| grades            = 8 - 12
| streetaddress     = 
| city              = [[Boksburg]]
| state             = 
| province          = 
| country           = [[South Africa]]
| coordinates       = 
| latitude          = 
| longitude         = 
| district          = 
| oversight         = 
| accreditation     = 
| campus            = 
| colors            = 
| colours           = Blue, Green and yellow
| athletics         = 
| houses            = 
| student_union     = 
| nickname          = 
| mascot            = 
| free_label        = [[Emblem]]
| free_text         = 
| rival             = 
| yearbook          = 
| newspaper          = 
| free_label_1      = Houses
| free_1            = Retief, Potgieter, Pretorius
| free_label_2      = 
| free_2            = 
| free_label_3      = 
| free_3            = 
| test_name         = 
| test_average      = 
| national_ranking  = 
| website           = [http://www.voortrekkerhs.co.za ]
| footnotes         = 
| picture           = 
| picture_caption   = 
| picture2          = 
| picture_caption2  = 
}}
'''Hoërskool Voortrekker ''' is a [[public high school]] in  [[Boksburg]], [[Gauteng]], [[South Africa]].

==Founder==
Ds. James Murray Louw<ref>{{cite book|last=Kuit|first=Albert|title=Kommandoprediker|year=1948|location=Pretoria}}</ref>  studied for the ministry at Victoria College, Stellenbosch.  The first Nederduitse Gereformeerde church in Boksburg was a wood and iron building on the corner of Market and Trichardts streets, opposite Market Square, where the Old Town Hall is situated.  Ds. Louw was the second minister for the Boksburg congregation.<ref>{{cite book|last=Olivier|first=PJ|title=Ons gemeentelike feesalbum.|year=1952|publisher=N.G. Kerk-uitgewers|location=Kaapstad en Pretoria}}</ref>   He assumed duties in December 1898. When the Anglo-Boer War broke out, Ds. Louw left for the Natal front with the Boksburg Commando on 12 October 1899. For the next two and half years Ds. Louw stayed with the Republic forces. On 5 June 1902  Ds. Louw and the remnants of the Boksburg Commando laid down their weapons finally at Kraal station, just south of Heidelberg. Under his leadership, the "Klipkerk" was built in 1912. Ds. Louw was especially concerned about education and welfare of orphans. Immediately after returning from the war, he arranged housing for 20 Boksburg war orphans.

The scoured earth policy of the British government had the consequence that there was an influx of Afrikaners looking for work on the coal and gold mines on the Witswatersrand.

After the war Ds Louw started campaigning for the establishment of a secondary school for Afrikaans speaking children on the Witswatersrand.

By 1938 Louw<ref>{{cite web|title=Boksburg link to the Voortrekker Monument|url=http://boksburghistorical.com/index_files/BHAfeb2008.pdf|publisher=Boksburg Historical Assos.}}</ref>   was frail and in poor health. As one of the last surviving preachers who had stayed with the Boer forces throughout the war, he was asked to deliver the sermon at the foundation stone laying for the Voortrekker Monument on 16 June 1938

==History==
Voortrekker High School<ref>{{cite web|title=Official website|url=http://www.voortrekkerhs.co.za/history.html}}</ref><ref>{{cite journal|last=University of South Africa|title=University of South Africa|journal=Communications: research work by professors, lecturers and students|year=1963|issue=40 –49}}</ref>   was the first [[Afrikaans]] school on the Witswatersrand. The school was started by a group of [[Afrikaners]], under the leadership of Louw, who wanted their children to have an alternative to English schools.

In April 1918 the East Rand School Council undertook a feasibility study to see if there are sufficient Afrikaans speaking pupils past standard five to justify an Afrikaans medium school.

The survey confirmed the need for an Afrikaans secondary school. Through the hard work of Louw, the OOSRANDSE HOLLANDSE MEDIUM HOËRSKOOL was established on 29 January 1920. The school started in Nobby's Bar <ref>{{cite web|title=Nobby Henrey. a Boksburg pioneer|url=http://boksburghistorical.com/index_files/BHAjan2005.pdf}}</ref>  next to the Boksburg Lake with 67 learners and 6 educators. Mr. an Wyk was the acting principal. At the start of the second quarter Mr. AM Muller became the first permanent principal. At the start of the third quarter the school was renamed Hoërskool Voortrekker. In 1922 the school relocated to a shop in the Morris arcade.
[[File:Morris Arcade.jpg|thumb|Image of Morris arcade]]

In 1923  du Toit became the new principal and shortly afterwards the erection of a permanent school building began.

On 30 July 1924 the new school buildings were officially inaugurated by Professor [[J. H. Hofmeyer]], Administrator of Transvaal.
[[File:VoortrekkerArk.jpg|thumb|Die Ark]]

In 1935 a fund raising was started to build an assembly hall and domestic science centre. Bernard Stanley Cooke was appointed as architect. Cooke<ref>{{cite web|title=Bernard Stanley Cooke|url=http://www.artefacts.co.za/main/Buildings/bldg_mid_bottom.php?bldgid=3487&archid=288}}</ref>  died in January 2011 at the age of 100. Mr Cooke was responsible for many Art Deco buildings in Johannesburg including Escom House and buildings of the Rand Easter Show grounds. He was also responsible for the drawings of the Tower of Light at the show grounds. [[Anton van Wouw]], South Africa most renowned sculptor was commissioned to create a bas-relief depicting the "Groot Trek".

[[File:VoortrekkerRelief.jpg|thumb|Die Groot Trek]]
 
Anton van Wouw’s work includes the statue of Paul Kruger on Church Square Pretoria, the Woman Memorial in Bloemfontein and statues at the [[Voortrekker Monument]]. Building of the hall started in July 1936 and the building and bas relief was inaugurated on 9 December 1938.

In 1974 a statue of a Voortrekker Boy and Girl by the sculptor P Potgieter was  unveiled.
[[File:VoriesSeunDogter.jpg|thumb|Voortrekker Boy & Girl]]

In 1976 a new hall was inaugurated. The hall was named after the principal T. S. Welman.

In 1999 Voortrekker became a dual medium school.

==Nobby's Bar==
"Nobby Henrey was the personification of the Rand Pioneer. He was open-hearted, generous to a fault, his good deeds can hardly be counted, and many a Boksburg resident down on his or her luck have reason to bless the day they met him." <ref>{{cite web|title=Boksburg Historical|url=http://french.about.com/library/begin/bl-numbers31.htm#List}}</ref> Born in Cradock in 1861, Edward Barrett Henrey decided to try his luck at the gold diggings in Barberton at the age of eighteen. Unsuccessful, he returned home but the lure of gold got the better of him and he returned to the old Transvaal and eventually arrived in Elsburg in 1885. In 1886 when  were auctioned off in Boksburg he selected a stand opposite the Old Post Office and there established what was to become the renowned "Nobby’s Bar." At the time he built the bar, he didn’t realise that his hostelry would eventually be situated in a prime position on the shores of "Montagu’s Folly," the Boksburg Lake, which became the "Beautiful Pleasure Resort of the Rand."

"From the very beginning, Nobby’s Bar was the centre of social life to the Boksburg pioneers. Here most of the meetings took place. It was from here that the first alumni from Voortrekker High School were sent out and it was in Nobby’s Bar that the various churches held their services right up until the time first billiard table on the East Rand was imported and placed in position. In one of the outhouses he kept a pet bear sent to him by a friend. Nobby's Bar was the first premises for two high schools namely Voortrekker and Boksburg High schools. Unfortunately this establishment did not stood the test of time and there is no sign of it left or the building - Morris arcade

==Principals==
* Mr. A.M. Muller     1920 —  1922
* Mr. A.E. du Toit   1923 —  1951
* Mr. G.H.B. Dykman  1951 -  1953
* Mr. J.P. van der Vyfver 1954 -1957
* Mr. T. S. Welman     1957 — 1978
* Mr. C.J. Joubert      1978 — 1989
* Mr. C.P Viljoen  1990 - 1992
* Mr. M.P. van Heerden   1993 —  current

==School badge==
The badge was designed by Mr. Saville Davis (former manager of ERPM). In 1932 the badge in its present form was accepted and the first were imported from Switzerland. In the same year the new colours (blue, green and yellow) were accepted and the motto “Voorwaart” was changed to “Vorentoe”.

At the top of the badge is the ox–wagon a symbol of the Afrikaner nation and its rise. Beneath the ox-wagon is the Bible as the only light and guidance. The mealies are symbolic of the Highveld of Transvaal. The gold mine is the source of income and represents Boksburg.

== School Anthem==

[[File:Skoollied1.jpg|thumb|left|Hilda de Groof, composer of the school anthem]]
[[File:Bladmusiek2.jpg|thumb|Music]]
[[File:Bladmusiek1.jpg|thumb|Music]]

==Afrikaans==

Hier benoorde die Vaal, waar die duine verrys

In die hartjie van Voortrekkerland

Pryk die vrug van ons arbeid, die bron van ons krag

Bloei ons Voortrekkerskool op die Rand.

Hoera, vir ons Voortrekkerskool op die Rand

Ons hart en ons hand bied ons jou.

Beur vorentoe, Trekkers van Voortrekkerland

Wees sterk om te handhaaf en te bou.

Beur vorentoe, Trekkers van Voortrekkerland!

Wees sterk om te handhaaf en bou!

==English==
To the North of the Vaal, where the dunes reach up high

In the midst of our Voortrekkerland

Vaunts the fruit of our labour, the well of our strength

Prospers Voortrekker School on the Rand.

Hurrah, for our Voortrekker School on the Rand

We bid you our heart and our hand.

To the fore, you Trekkers of Voortrekkerland

Be strong to up - hold and to grow.

To the fore, you Trekkers of Voortrekkerland!

Be strong to up - hold and grow!

==Sport achievements==
* 1923 Finalist Administrators Cup (Pretoria Boys High 17 Voortrekker 0)
* 1934 Finalist Administrators Cup (Zeerust 11 Voortrekker 8)
*1958 Joint Winner of Administrators Cup (Voortrekker 6 Lichtenburg 6)- Rugby<ref>{{cite web|title=Beeld Trophy Historic Overview|url=http://www.rugby15.co.za/2011/06/beeld-trophy-historic-overview}}</ref>
*2013 Rugby (under 15) Beeld Trophy Winner
*1958 Winner of Regional Inter-high Athletics Meeting

==Notable alumni==

===Arts and entertainment<ref>{{cite web|title=may1994 news letter |url=http://boksburghistorical.com/index_files/BHAmay1994.pdf |deadurl=yes |archiveurl=https://web.archive.org/web/20151002042802/http://boksburghistorical.com/index_files/BHAmay1994.pdf |archivedate=2 October 2015 |df=dmy-all }}</ref>===
*[[Jamie Uys]] (30 May 1921 – 29 January 1996) Afrikaans and International Film producer/director
*[[Jans Rautenbach]]  Screenwriter and ( 1936 - 2016)Afrikaans Film producer/director
*Tommie Meyer Afrikaans Film producer/director
*Jan Scholtz  Film Producer and screenwriter
*[[Dawid Engela]] (30 October 1931 – 25 November 1967) was a South African broadcaster, composer, and musicologist
*De Wet van Rooyen Opera Singer
*Steve de Villiers Sports commentator and Director General of the SABC

===Politicians===
*[[Barend du Plessis]]  Minister of Finance<ref>{{cite book|last=Gastrow|first=S|title=Who’s Who in South African Politics|year=1987|publisher=RAVEN|location=Johannesburg}}</ref> 
*Sakkie Blanche<ref>{{cite web|title=Veteraan Politikus tree uit |url=http://152.111.1.87/argief/berigte/rapport/2009/03/06/RG/3/SVN%20Sakkie%20Blanche.html |deadurl=yes |archiveurl=https://web.archive.org/web/20151002012448/http://152.111.1.87/argief/berigte/rapport/2009/03/06/RG/3/SVN%20Sakkie%20Blanche.html |archivedate=2 October 2015 |df=dmy-all }}</ref>       Minister of Welfare –MEC- Gauteng

===Academics===
* Professor Walter Marasas (1941–2012) University of Pretoria<ref>{{cite web|title=Curriculum Vitae|url=http://repository.up.ac.za/bitstream/handle/2263/3432/cv2003.pdf}}</ref> 
* Professor Desiree (Ferreira) Vorster Vice Rector University of Johannesburg
* Professor Jan van Heerden  University of Pretoria
* Professor Estelle (Blanche) Swart University of Stellenbosch
* Professor Leon Snyman University of Pretoria
* Professor Nielen van der Merwe Wits School of Mining Engineering.<ref name=Sanire>{{cite web|last=van der Merwe|title=Nielen|url=http://www.sanire.co.za/news23/personalities/244-nielen-achieves-the-incredible|accessdate=1 October 2012}}</ref>
* Professor William Reyneke University of Pretoria
* Professor Chris Boshoff is Professor of Cancer Medicine at University Collega Londen, and Director of the UCL Cancer Institute
* Professor Wayne Pearce University of Penn State

===Business and community leaders===
* Attie du Plessis  Ex Chairman of Gencor and Sentrachem
* Chantyl Mulder SAICA:2011 Most Influential Women in Business and Government and The University of Johannesburg's Ellen Kuzwayo Council Award<ref>{{cite web|title=News|url=https://www.saica.co.za/News/NewsArticlesandPressmediareleases/tabid/695/itemid/3061/language/en-ZA/Default.aspx}}</ref> 
* Marius Marais - developer of the MNet decoder

===Sport achievers – National colours===
Johan Steenekamp (*1935 +2007), Springbok Rugby player. Johan played 1 Test against the French as lock at the age of 22. The final score was SA 3 France 3.<ref name="SARU Player Profile Johan Steenekamp">{{SA Rugby Player Profile | id=46752 | name=Johan Steenekamp | date=30 May 2016}}</ref>

Jan Adriaan Smit (*1924 +) Springbok Shooting (1962-74)<ref name=UPretoria>{{cite web|title=JanSmit|url=http://web.up.ac.za/sitefiles/file/ACADADMIN.INTRANET/graduation%20ceremonies/1970s/1977.pdf}}</ref>

==References==
{{reflist}}

==Further reading==
* Pakenham, Thomas (1979). The Boer War. New York: Random House. ISBN 0-394-42742-4
* Kuit, Albert (1948). Kommandoprediker. Pretoria.
* Olivier, PJ (1952). Ons gemeentelike feesalbum.. Kaapstad en Pretoria: N.G. Kerk-uitgewers.
* Pieter Johannes Meyer (1984) Nog nie ver genoeg nie. Perskor
* Marius J. Swart. (1987) Ons voortbestaan: die kultuurstrewe van die Afrikaner.Oranjewerkers Promosies, ISBN 0-620-11257-3, ISBN 978-0-620-11257-4

==External links==
* {{Official website|http://www.voortrekkerhs.co.za}}
* http://www.Boksburghistorical.com

{{Coord missing|South Africa}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Hoerskool Voortrekker}}
[[Category:Educational institutions established in 1920]]
[[Category:High schools in South Africa]]
[[Category:Schools in Gauteng]]
[[Category:Dual-medium schools in South Africa]]