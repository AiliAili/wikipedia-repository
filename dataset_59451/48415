'''Health and wellbeing boards''' are statutory bodies introduced in England under the [[Health and Social Care Act 2012]]. According to the Act, each upper-tier local authority in England is required to form a health and wellbeing board as a committee of that authority. More than 130 "shadow" boards were created before April 2013,<ref name="HWB map">[http://www.kingsfund.org.uk/current_projects/health_and_wellbeing_boards_making_them_work/hwb_map.html], The King's Fund - Health and Wellbeing Board directory map.</ref> when they all became fully operational.<ref name="DH_short_guide">[http://healthandcare.dh.gov.uk/hwb-guide], Department of Health - A short guide to health and wellbeing boards.</ref>

== Duties ==
The aim of the health and wellbeing boards is to improve integration between practitioners in local health care, social care, public health and related public services so that patients and other service-users experience more "joined up" care, particularly in transitions between health care and social care. The boards are also responsible for leading locally on reducing [[Health equity|health inequalities]].

=== Joint strategic needs assessment ===
Each board produces a joint strategic needs assessment (JSNA) for its local authority area, replacing the JSNA formerly prepared by local authorities and [[Primary care trusts|Primary Care Trusts (PCTs)]], under [[Local Government and Public Involvement in Health Act 2007|Section 116 of the Local Government and Public Involvement in Health Act (2007)]].<ref name="JSNA_guidance">[http://www.dh.gov.uk/prod_consum_dh/groups/dh_digitalassets/@dh/@en/documents/digitalasset/dh_081267.pdf], Department of Health - Guidance on joint strategic needs assessment.</ref> A JSNA provides local policy-makers and commissioners with a profile of the health and wellbeing needs of the local population. The aim of the JSNA is to improve commissioning and reduce health inequalities by identifying current and future health trends within a local population. It is expected that the JSNA should be based on analysis of:

* [[Demographic data|demographic data about the local population]] (such as age, gender and ethnicity)
* [[Social determinants of health|social, economic and environmental determinants of health]] (such as housing, crime and  employment)
* behavioural determinants of health (such as smoking, drinking and dietary habits)
* [[epidemiology]] (such as life expectancy, quality of life, and prevalence of diseases and conditions)
* access to services and use of services (such as admissions, discharge and usage data)
* evidence of effectiveness (such as guidelines of the [[National Institute for Health and Clinical Excellence|NICE]], quality standards, good practice examples)
*  service user and patient views (views and experiences of service users and patients)<ref name="nhsconfed_JSNA_briefing">[http://www.nhsconfed.org/Publications/Documents/Briefing_221_JSNAs.PDF], NHS Confederation - The joint strategic needs assessment: a vital tool to guide commissioning.</ref>

Department of Health guidance recommends that JSNAs are refreshed every three years.<ref name="JSNA_guidance" />

=== Joint health and wellbeing strategy ===
The boards are also responsible for producing a joint health and wellbeing strategy. The first of these were published by the "shadow" boards at the end of 2012. Priority areas identified from JSNAs are key for the development of joint strategies, which in turn feed into commissioning plans.<ref name="HWB_JSNA_guidance">[http://www.dh.gov.uk/prod_consum_dh/groups/dh_digitalassets/documents/digitalasset/dh_131733.pdf], Department of Health - joint strategic needs assessment and joint health and wellbeing strategies explained: commissioning for populations.</ref> The boards also look at which areas may need deprioritising and decommissioning.<ref name="NLGNreport">[http://www.nlgn.org.uk/public/wp-content/uploads/Healthy-Places_FINAL.pdf] Healthy places: councils leading on public health, Daria Kuznetsova, New Local Government Network.</ref> The strategic direction of implementation and service delivery for health and wellbeing boards is outlined in the joint strategy documents.

=== Involvement in commissioning ===
Health and wellbeing boards have no statutory obligation to become directly involved in the commissioning process, but they do have powers to influence commissioning decisions made by CCGs. However, CCGs and local authorities may delegate commissioning powers to health and wellbeing boards so that they can lead on joint commissioning.<ref name="BMA_guidance">[http://www.bma.org.uk/images/nhsreformgpcguidancehealthwellsep11_tcm41-209261.pdf], British Medical Association - Health and wellbeing boards: GPC guidance.</ref> JSNAs and joint health and wellbeing strategies, produced by the boards, are key tools that CCGs use in deciding what public health services need to be purchased. In this sense the boards have a role in shaping the local public health landscape, and helping CCGs to commission services in an effective and targeted manner. 

An early survey of 50 local authorities found that the majority of the respondents thought that joint health and wellbeing strategies would be influential in relation to the decisions of CCGs.<ref name="TKFreport">[http://www.kingsfund.org.uk/publications/hwbs.html],  Health and wellbeing boards: system leaders or talking shops?, Richard Humphries, The King's Fund.</ref>

There is also a statutory requirement that CCGs consult health and wellbeing boards throughout the commissioning process in order to align with the local joint health and wellbeing strategy.<ref name="BMA_guidance" /> The boards can also report any concerns regarding commissioning decisions to the national body responsible for the governance of CCGs, the [http://www.commissioningboard.nhs.uk/ NHS Commissioning Board].

== Structure and composition ==
=== Structure of the health and social care system ===
[[File:NHS structure from April 2013.jpg|thumb|Structure of the NHS England from April 2013<ref name="DH_reform_briefing">[http://www.dh.gov.uk/health/files/2012/02/A3-Overview-of-health-and-care-structures1.pdf], Department of Health - Overview of health and care structures - The Health and Social Care Bill.</ref>]]
From April 2013 the changes enacted by the [[Health and Social Care Act 2012]] saw the creation of new local and national bodies within the system, such as health and wellbeing boards, [[clinical commissioning groups]] and the [[NHS Commissioning Board]]. Within the local health and social care system, health and wellbeing boards sit below [[local authorities]] and  clinical commissioning groups. However, the boards sit above local [[HealthWatch England|Healthwatch]] groups and health and social care providers.

=== Structure of the boards ===
Health and wellbeing boards sit within unitary and top-tier local authorities as committees of those authorities. Although they hold responsibility for public health at the local level, the administration and governance of the boards is not part of the [[National Health Service|NHS]].

=== Composition of the boards ===
There is a minimum membership required for a health and wellbeing board, as follows:

* a local elected representative
* a representative from the local [[HealthWatch England| Healthwatch]]
* a representative from each local [[Clinical commissioning group]] 
* the local director of adult social services
* the local director of children's social services
* the local director of public health<ref name="DH_short_guide" />
* a representative nominated by the NHS Commissioning Board<ref name="HSCA">[http://www.legislation.gov.uk/ukpga/2012/7/part/5/chapter/2/crossheading/health-and-wellbeing-boards-supplementary/enacted], Health and Social Care Act 2012.</ref>

Beyond this minimum membership other interested local stakeholders may also be invited to hold membership of a health and wellbeing board. These may include representatives of third-sector or voluntary organisations, other public services, or the NHS.

==Future Possibilities==

The boards could be put in charge of commissioning combined health and social care services if they beefed up their contingent of clinicians according to Kate Barker, who chaired the [[King’s Fund]] commission on the future of health and social care.<ref>{{cite news|title=Beefed up HWBs 'could commission health and social care'|url=http://www.hsj.co.uk/news/beefed-up-hwbs-could-commission-health-and-social-care/5074353.article#.VCnEZRLGBJ0|accessdate=29 September 2014|publisher=Health Service Journal|date=4 September 2014}}</ref>  [[Andy Burnham]] suggested that the boards could be in charge of the process of integration of health and social care which he wants to see.<ref>{{cite news|title=Burnham: I will ‘not mandate’ structural change|url=http://www.lgcplus.com/briefings/joint-working/health/burnham-i-will-not-mandate-structural-change/5075203.article?blocktitle=Latest-Local-Government-News&contentID=2249|accessdate=29 September 2014|publisher=Local Government Chronicle|date=26 September 2014}}</ref>  [[Clinical commissioning group]] leaders were not enthusiastic about Labour suggestions of making health and wellbeing boards “system leaders” for services for people with multiple long term conditions, disability or frailty.<ref>{{cite news|title=CCGs oppose bigger health role for councils|url=http://www.lgcplus.com/news/ccgs-oppose-bigger-health-role-for-councils/5074536.article|accessdate=29 September 2014|publisher=Local Government Chronicle|date=5 September 2014}}</ref>

In March 2015 [[London Councils]] called for Health and wellbeing boards to be handed responsibility for managing pressures in the health and social care system next winter.<ref>{{cite news|title=Call to boost local government role in managing NHS winter pressures|url=http://www.lgcplus.com/news/call-to-boost-local-government-role-in-managing-nhs-winter-pressures/5083034.article|accessdate=10 March 2015|publisher=Local Government Chronicle|date=6 March 2015}}</ref> 

== See also ==
[[Health and Social Care Act 2012]]

== References ==
{{Reflist}}

== External links ==
* [http://healthandcare.dh.gov.uk/early-implementers-of-health-and-wellbeing-boards-announced/ Department of Health - Early implementers of health and wellbeing boards announced]
* [http://www.dh.gov.uk/prod_consum_dh/groups/dh_digitalassets/documents/digitalasset/dh_131904.pdf Department of Health - Public health for local government factsheets]
* [http://www.nhsconfed.org/Publications/reports/Pages/Operating-principles.aspx NHS Confederation - Operating principles for health and wellbeing boards]
* [http://www.kingsfund.org.uk/current_projects/health_and_wellbeing_boards_making_them_work/health_and_wellbeing.html The King's Fund - Health and wellbeing boards project]

{{NHS England}}

[[Category:Articles created via the Article Wizard]]
[[Category:National Health Service (England)]]