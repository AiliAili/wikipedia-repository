{{Infobox journal
| title        = Journal of Structural Engineering
}}
The '''''Journal of Structural Engineering''''' is the principal professional [[peer-review]]ed journal of the  [[American Society of Civil Engineers]], the oldest professional civil engineering society in the United States.  Besides the journal, the Society publishes [technical journals, conference proceedings, and books (including technical reports, standards, manuals of practice and ASCE Press imprint titles) The Journal, and its other publications, are available through the ASCE Library] and the ASCE Bookstore.

The ''Journal of Structural Engineering'' is one of the flagship journals of the Society. It is sponsored by its division, the [ASCE Structural Engineering Institute]. {{ISSN|0733-9445}} e{{ISSN|1943-541X}} CODEN: JSENDH

== Aims & Scope ==

According to its Aims and Scope, "The ''Journal of Structural Engineering'' reports on fundamental knowledge that contributes to the state-of-the-art and state-of-practice in structural engineering. Authors discuss the art and science of structural design; investigate the physical properties of engineering materials as related to structural behavior; develop methods of analysis; and study the merits of various types of structures and methods of construction."<ref>{{cite web|title=Journal of Structural Engineering|url=http://ascelibrary.org/page/jsendh/editorialboard|work=ASCE Library|publisher=American Society of Civil Engineers|accessdate=25 July 2013}}</ref>
The journal covers both theoretical and applied research papers in the field of structural engineering, and on fields related to both structural engineering and other disciplines<ref>[http://ascelibrary.org/page/jsendh/editorialboard Journal Home page]</ref>

== History ==

Originally, the journals was part of the ''proceedings of the American Society of Civil Engineers,'' first published in 1873<ref>[http://catalog.princeton.edu/cgi-bin/Pwebrecon.cgi?v4=19&ti=1,19&SEQ=20140212024538&SAB1=Proceedings%20%28American%20Society%20of%20Civil%20Engineers&BOOL1=as%20a%20phrase&FLD1=Title%20%28TITP%29&GRP1=AND%20with%20next%20set&SAB2=&BOOL2=all%20of%20these&FLD2=Keyword%20Anywhere%20%28GKEY%29&GRP2=AND%20with%20next%20set&SAB3=&BOOL3=all%20of%20these&FLD3=Keyword%20Anywhere%20%28GKEY%29&CNT=25&PID=D3hcrsAqlrVBqS-kEl5wNX6p4PvOe&SID=4 Princeton University Library record]</ref>
The Journal started publishing separately in 1956 as part of the ASCE ''Journal of the Structural Division'' ({{ISSN|0044-8001}}). In 1983, the title was changed to the ''Journal of Structural Engineering'' ({{ISSN|0733-9445}}).

Past Editors of the Journal include:
*John E. Bower: Vol. 107 No. 6 (1 Year 3 Months)
*Donald Mcdonald: Vol. 108 No. 10 (1 Year 11 Months)
*Thomas G. Williamson: Vol. 110 No. 10 (2 Years 1 Month)
*Alfredo H-S. Ang: Vol. 112 No. 11 (4 Years 1 Month)
*James T.P. Yao: Vol. 116 No. 12 (1 Year 10 Months)
*Vernon B. Watwood: Vol. 118 No. 11 (1 Year 11 Months)
*David Darwin: Vol. 120 No. 11 (5 Years 10 Months)
*C. Dale Buckner: Vol. 126 No. 10 (2 Years 11 Months)
*Sashi Kunnath: Vol. 129 No. 10 (7 Years)
*Sherif El-Tawil: Vol. 136 No. 10 (Present)

== Metrics ==

{| class="wikitable"
|-
! Metric !! 2011
|-
| [[Impact Factor]]  (2 year)|| 1.206<ref name="admin-apps.webofknowledge">{{cite web|title=2012 Journal Citation Reports|url=http://admin-apps.webofknowledge.com/JCR/JCR?SID=1F8oLakNGcFGIO6ENOm|work=Science Edition|publisher=Thomson Reuters|accessdate=26 July 2013}}</ref>
|-
| 5-year Impact Factor || 1.532<ref name="admin-apps.webofknowledge" /> 
|-
| Immediacy Index || 0.261<ref name="admin-apps.webofknowledge" /> 
|-
| Cited Half-Life || >10.0<ref name="admin-apps.webofknowledge" /> 
|-
| [[Eigenfactor]] Score || 0.01245<ref name="admin-apps.webofknowledge" /> 
|-
| Article Influence Score|| 0.861<ref name="admin-apps.webofknowledge" /> 
|-
| ''h''-index|| 65
|-
| [[SCImago Journal Rank]]<ref>{{cite web|last=SCImago JR|title=SCImago Journal & Country Rank|url=http://www.scimagojr.com|work=SCImago Journal & Country Rank|publisher=SCImago|accessdate=25 July 2013}}</ref>  || 1.491

|}

== Indexes ==

The ''Journal of Structural Engineering'' is indexed in all the major services, including Scopus, Web of Science, and  Engineering Index. It is available on Proquest and EbscoNet. As a member of [[CrossRef]], all journal articles include links to other publisher content in the CrossRef system.

== References ==

{{reflist}}

== External links ==
* [http://ascelibrary.org/journals ASCE Library]
*[http://ascelibrary.org/page/jsendh/editorialboard Journal home page and editorial board]

[[Category:Civil engineering journals|Structural engineering]]
[[Category:English-language journals]]
[[Category:Structural engineering]]
[[Category:American Society of Civil Engineers academic journals]]