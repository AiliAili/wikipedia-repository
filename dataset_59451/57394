{{Infobox journal
| title = Imaging in Medicine
| cover = 
| discipline = [[Medical imaging]]
| abbreviation = Imaging Med.
| editor =
| publisher = [[Pulsus Group]]
| country =
| frequency = Quarterly
| history = 2009-present
| openaccess = Yes
| license =
| website = http://www.openaccessjournals.com/journals/imaging-in-medicine.html
| link1 = http://www.openaccessjournals.com/journals/imaging-in-medicine-current-issue.html
| link1-name = Online access
| link2 = http://www.openaccessjournals.com/journals/imaging-in-medicine-archive.html
| link2-name = Online archive
| ISSN = 1755-5191
| eISSN = 1755-5205
| CODEN = IMMECA
| OCLC = 651888195
}}
'''''Imaging in Medicine''''' is a quarterly [[peer-reviewed]] [[open access]] [[medical journal]]. It covers [[medical imaging]], [[radiation therapy]], [[radiology]], and basic imaging and [[nuclear medicine]]. The journal was established in 2009 and is published by [[Pulsus Group]], which is on [[Jeffrey Beall]]'s list of "Potential, possible, or probable" [[Predatory open access publishing|predatory open-access publishers]].<ref>{{cite web|url=http://scholarlyoa.com/publishers/ |title=LIST OF PUBLISHERS |work=Scholarly Open Access |last1=Beall |first1=Jeffrey |authorlink=Jeffrey Beall |accessdate=2016-09-21 |deadurl=yes |archiveurl=https://web.archive.org/web/20160917143148/https://scholarlyoa.com/publishers/ |archivedate=2016-09-17 |df= }}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in [[Chemical Abstracts Service]],<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-09-21 }}{{dead link|date=April 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> [[Embase]],<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2016-09-21}}</ref> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-09-21}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.openaccessjournals.com/journals/imaging-in-medicine.html}}

[[Category:Open access journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2009]]
[[Category:Pulsus Group academic journals]]
[[Category:Radiology and medical imaging journals]]