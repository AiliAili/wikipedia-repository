{{Multiple issues|
{{Underlinked|date=June 2016}}
{{Orphan|date=June 2016}}
}}

'''Cecily Boulstred''' (bap. 1584, d. 1609) was an English [[courtier]] and writer.  She was born to Edward Boulstred (d. 1595) and Cecily ([[floruit|fl.]] 1575-1608), the daughter of Sir John Croke of Chilton, in [[Beaconsfield]].<ref name="Considine">Considine, John. “Bulstrode, Cecily (bap. 1584, d.1609).” ''Oxford Dictionary of National Biography''. Online ed. Ed. Lawrence Goldman. Oxford: OUP, 2004. 16 Feb. 2016</ref> Her parents Edward and Cecily produced nine other children, amongst them Edward (1588-1659), who served as a judge in the courts of chancery, king’s bench, the Oxford assize circuits, and the Warwickshire quarter sessions throughout his lifetime.<ref>Prest, Wilfrid. "Bulstrode, Edward (c.1588–1659)," ''Oxford Dictionary of National Biography''. Oxford University Press, 2004; online edn, May 2007.</ref> During her time at [[James VI and I|King James]]’s court, Boulstred became the frequent subject of scandalous works by poets such as [[Ben Jonson]] who threatened her reputation with rumors of promiscuity.<ref name="Foster">Foster, Donald W., and Tobian Banton, eds. ''Women’s Works, Volume 3:1603-1625''. New York: Wicked Good Books, 2013. 36-57. Print.</ref> Many writers, including [[John Donne]], used the event of her death as an opportunity to gain favor with her friend and matron of the literary arts, the countess of Bedford.<ref name="Considine"/> The only known written work of Boulstred’s is “News of My Morning Work,” probably written in 1609.<ref name="Foster"/> Spelling variations on her first and last name include ''Cecilia'', ''Celia'', and ''Bulstrode''.

==Life==

===Early life===
Boulstred may have been baptized on February 12, 1584 at Beaconsfield.<ref name="Considine"/> Another account has her baptism taking place on February 12, 1582/3.<ref name="Foster"/> Her parents Edward and Cecily were landowners in [[Buckinghamshire]]. She was either one of 10 children,<ref name="Considine"/> or the sixth of 11 children, and nothing else is known about her childhood.<ref name="Foster"/>

===Life at Court===
Boulstred followed in the footsteps of her ancestors as a courtier. In 1605, she became part of the “entourage” of her mother’s first cousin [[Lucy Russell, Countess of Bedford|Lucy Russell, the countess of Bedford]].<ref name="Considine"/> When King James I ascended the throne, the countess of Bedford became [[Queen Anna]]’s [[Lady of the Bedchamber]]. Boulstred and her sister Dorothy moved up with Lucy Russell, becoming Maidens of the Queen’s Bedchamber. There, Boulstred “became a noted wit in the court of James I.”<ref name="Foster"/> As a good friend of the countess of Bedford and [[Handmaiden]] of the Queen, Boulstred was a lady of consequence at court.<ref name="Price">Price, Victoria E. “Troping prostitution: Jonson and ‘The Court Pucell’.” ''Nebula'' 4.2 (2007): 208+. Academic OneFile. Web. 15 Feb. 2016</ref>

===Scandals and Rumors===
While at court, Boulstred became the topic of several scandalous rumors.<ref name="Considine"/> Boulstred had a brief courtship, and possible engagement, with John Roe in 1602.<ref name="Foster"/> The definite reason for the breakup is unknown, but in “To Mistress Boulsted, 1602,” an elegy ghost-written for Roe by best friend [[Ben Jonson]], Boulstred is accused of sluttish behavior:

::::Shall I go force an elegy? abuse
::::My wit, and break the hymen of my Muse
::::For one poor hour’s love?...
::::I’ll have a Succuba as good as you!
::::::-“An Elegy to Mistress Boulsted, 1602” lines 1-3, 24<ref name="Foster"/>

This poem circulated at court as a letter to Boulstred from a J.R., presumably John Roe.<ref name="Foster"/> The poem takes the viewpoint of a man who rejects the advances of his female friend because he doesn’t want to ruin their friendship “For one poor hour’s love.”<ref name="Foster"/> Although the speaker claims to be a sincere friend who will keep the poem secret to protect her reputation, the poem was not kept secret and was most likely an attempt to ruin Boulstred’s reputation and allege, according to [[Donald Wayne Foster|Donald Foster]], “that Boulstred solicited Roe for sex, which caused him to reject her as unfit for marriage.” In 1628, Ben Jonson revealed that he actually ghostwrote this poem for Roe.<ref name="Foster"/>

===Later life and death===
Boulstred eventually started a relationship with Sir Thomas Roe, Sir John Roe’s cousin. This relationship most likely would have led to marriage were it not that Boulstred became very sick in 1609. In 1609, in the span of a couple of months, Boulstred both fell ill and died. What likely now would have been diagnosed as a brain tumor or gastric cancer was identified by doctors of the College of Medicine at the time as “the mother,” a.k.a. ''wandering womb'' – an imprecise medical diagnosis employed…to cover ailments that were thought to attend upon feminine frailty.”<ref name="Foster"/> Her symptoms included stomach pain, sleeplessness, fever, and vomiting.<ref name="Considine"/> A cure could not be found, and she wasted away at the countess of Bedford’s house in [[Twickenham]], unable to hold down food or liquids.<ref name="Foster"/>

In a letter to Sir Henry Goodyere, John Donne reports on her condition:

“I fear earnestly that Mistress Boulstred will not escape that sickness in which she labors at this time. I sent this morning to ask of her passage of this night; and the return is, that she is as I left her yesternight...[I] fear that she will scare last so long as that you, when you receive this letter, may do her any good office in praying for her.”<ref name="Foster"/>

According to the account of Dr. Francis Anthony, who was called upon by Boulstred’s mother after treatment under the College of Medicine physicians was unsuccessful, Boulstred showed improvement in symptoms in her final days:

“For in all the other administering of this medicine…her spirits were relieved! She daily recovered strength. All passions, symptoms, and accidents of disease ceased. Her sickness fully left her, and she recovered perfect health!”<ref name="Foster"/>

But in regard to her “perfect health,” Anthony exaggerated.  Although Dr. Francis Anthony gave her doses of Potabile Gold in an attempt to cure her, Boulstred died within days.<ref name="Foster"/>  As Jongsook Lee puts it, Anthony was a bit of “a quack.”<ref name="Lee">Lee, Jongsook. “Who Is Cecilia, What Was She? Cecilia Bulstrode and Jonson's Epideictics.” ''The Journal of English and Germanic Philology'' 85.1 (1986): 20–34. Web. 16 Feb. 2016</ref> Boulstred’s brother in-law, James Whitlocke, reports: “Cecil Boulstred, my wife’s sister, gentlewoman to Queen Anna, ordinary of her bedchamber, died at Twick’n’am in Middlesex, the earl of Bedford’s house, 4 August 1609,” and was buried there two days later.<ref name="Foster"/>
 
Although Sir Thomas Roe never had the chance to marry Boulstred, his love for her remained, and he carried a miniature watercolor portrait of her around with him for the rest of his life, even after he married.<ref name="Foster"/>

In death, Boulstred’s body became a theme of court poets who competed for the literary matronage of her close friend Lucy Russell, countess of Bedford.<ref name="Foster"/> Lucy Russell’s matronage was highly valued, as she had a rather large amount of power at court as a countess and as the Lady of the Queen’s Bedchamber.<ref name="Price"/>

==Literary References==

===Sir John Roe===
Sir John Roe has been attributed with writing the poems “True Love Finds Wit” and the previously mentioned “An Elegy to Mistress Boulstred.”<ref name="Considine"/> The former describes Boulstred as a “Wench at Court.”<ref name="Lee"/> Jonson later claimed authorship to the latter in his play ''The New Inn'' in 1628/9.<ref name="Foster"/>

===Ben Jonson===
Jonson continued to write about Boulstred for the rest of her life, and after her death, most of it of a slanderous nature. In 1603/4, Jonson and Roe were kicked out of a masque at Hampton Court, an instance which Jonson blamed Boulstred for and later wrote about in his play ''The New Inn'' (1628/9).<ref name="Foster"/>
 
The most well-known of his literary jabs at Boulstred is his “Epigram on the Court Pucell,” which many critics call a “disturbingly ‘personal’ attack on a woman.”<ref name="Lee"/> Jonson supposedly wrote this poem as a response to a criticism Boulstred made of his play draft Epicoene: the Silent Woman.<ref name="Foster"/> He claimed that his epigram was stolen out of his pocket when he was drunk and given to Boulstred, which he had not wanted to happen.<ref name="Evans">Evans, Robert C. "Ben Jonson (11 June 1572?-August 1637)." ''Seventeenth-Century British Nondramatic Poets: First Series''. Ed. M. Thomas Hester. Vol. 121. Detroit: Gale, 1992. 186-212. ''Dictionary of Literary Biography'' Vol. 121. Dictionary of Literary Biography Complete Online. Web. 16 Feb. 2016.</ref> Regarding the title of the poem, “pucell” was the early modern term for prostitute, so Jonson seems to be calling Boulstred a prostitute in this poem.<ref name="Price"/> He also accuses Boulstred of being promiscuous, bisexual, pretentious, hypocritical, and much more. His accusation of bisexuality stems from her close relationship with Lucy Russell, the countess of Bedford.<ref name="Foster"/> However, another reading of the poem is that rather than a direct criticism of Boulstred, the work was meant to be one of a “generic court pucelle, and by that means, an image of the false world.”<ref name="Lee"/> Yet another reading is that the poem is a representation of Jonson’s frustration with having to rely on a woman, like the countess of Bedford, for literary success.<ref name="Price"/> Lucy Russell was the matron of many writers, such as John Donne, and Jonson attempted to gain her matronage for many years.<ref name="Foster"/> Jonson may have been frustrated by this apparent “inversion of traditional gender codes,” so he used the “trope of prostitution” in this poem to reassert his position of power as a male.<ref name="Price"/>
 
The other major work that Jonson wrote about Boulstred was his “Epitaph on Cecilia Bulstrode,” written in response to her death.<ref name="Foster"/> This poem paints a very different picture of Boulstred than that of his epigram. In fact, it seems to retract each of the charges made in the first point by point.<ref name="Lee"/> Jonson calls her a virgin, teacher to Pallas and Cynthia, conscientious, and good, vastly different from his epigram.<ref name="Foster"/> The reason behind Jonson’s supposed change of heart is unknown. The likely reason is that he wanted to gain the favor of Lucy Russell, which he had been attempting to do for years. However, another explanation could be that he experienced true regret for his slanderous words after hearing of her painful death.<ref name="Lee"/>

===John Donne===
John Donne was under the matronage of Lucy Russell during the time that Boulstred fell ill and died. As previously mentioned, he visited her when she was sick and concluded that she was suffering from hysteria.<ref name="Foster"/> After she died, he wrote two poems in her honor, “Elegy on Mistress Boulstred” and “Elegy Upon the Death of Mistress Boulstred.”<ref name="Lee"/> The former poem characterizes Death as an “all-consuming glutton who swallows the good” and argues that Boulstred was one of the good ones. The latter poem says that Death is no longer needed because Boulstred was the epitome of virtue, and now that she is dead, it is like the entire world is dead. Donne most likely wrote these elegies in the hopes of getting a reward from his literary matron Lucy Russell.<ref name="Foster"/>

===Lucy Russell===
The countess of Bedford wrote “Elegy on Mistress Boulstred” in response to Donne’s first elegy that characterized Death as a gluttonous monster. In the poem, she refers to Donne’s Holy Sonnet 10, which starts with the famous line, “Death be not proud.” Russell argues that Donne’s interpretation of Death was more accurate in this Holy Sonnet instead of in his elegy. She says that Death is something that should not be proud, and that Donne’s elegy gave Death too much credit. As a [[Christian]], Boulstred went to Heaven when she died, and Donne did not give Boulstred enough credit when he characterized her as a helpless victim of Death in his elegy. After this criticism, Donne wrote his second elegy for Boulstred, focusing on Boulstred’s virtues instead of on Death.<ref name="Foster"/>

===Sir Edward Herbert===
Sir Edward Herbert was a friend of Jonson’s as well as Sir Thomas and Sir John Roe. He commemorated her death with “Epitaphium.” Though the Latin subtitle of the poem implies that Boulstred died from an “unquiet conscience,” the poem itself characterizes Boulstred as a highly religious virgin who resisted all sin up until her “noble soul” entered Heaven. Like others, Herbert in his commemoration was most likely trying to win Russell’s support.<ref name="Foster"/>

==Works==
“News of My Morning Work” is the only existing known work credited to Boulstred, though scholar John Considine says “more…by her may lie undiscovered.” In this “witty piece,” Boulstred utilizes the satirical news form common of the era that “flourished” at Russell’s court from 1605-1610. As per the genre, “News of My Morning Work” consists of a list of moral and satirical aphorisms. Several of them are humorous criticisms of court and of people who claim to be religious:<ref name="Foster"/>

:::THAT to be good, the way is to be most alone—or the best accompanied.
:::That the way to Heaven is mistaken for the most melancholy walk.
:::That the most fear of the world’s opinion more than God’s displeasure.
:::That a Court-friend seldom goes further than the first degree of charity.
:::That the Devil is the perfect courtier.
:::::: -“News of My Morning Work,” lines 1-5
 
Toward the end of her piece, she writes: “That a man with a female wit is the worst hermaphrodite.” This is perhaps the most memorable line, as it has been interpreted by many as a jab at Ben Jonson, who apparently “had a career-long interest in [[hermaphroditism]]” according to Foster and Banton as evident by his works ''Cynthia’s Revels'', ''Volpone'', and ''Epicoene''.<ref name="Foster"/> It is notable, however, that not all agree. Robert W. Halli, Jr. points out that this is only one line of many that is being unfairly singled out and misinterpreted,<ref name="Halli">Halli, Robert W., Jr. "Cecilia Bulstrode, 'The Court Pucell'." ''Subjects on the World's Stage: Essays on British Literature of the Middle Ages and the Renaissance''. 295-312. Newark: U of Delaware P, 1995. ''MLA International Bibliography''. Web. 20 Apr. 2016.</ref> and scholar Victoria E. White claims “how this [line] is” immediately “relevant to Jonson is not apparent.”<ref name="Price"/> 
 
“News of My Morning Work” was likely written around 1609, but did not rise to prominence until it was published amongst other short pieces as a supplement to Sir Thomas Overbury’s ''A Wife'' in 1615, which became “an instant bestseller.” Overbury’s poem was published along with News articles such as Boulstred’s after it was discovered that Overbury had been murdered by the woman he wrote the poem about, making the publication a huge hit. A line in Jonson’s “Epigram on the Court Pucell” reads, “And in an Epicoene fury can write News / Equal with that which for the best News goes,” which seems to imply that the News articles published with Overbury’s poem were written by Boulstred’s friends and that “News of My Morning Work” was written by Boulstred.<ref name="Foster"/> The publication itself only identifies the author as a ‘Mris B.’<ref name="Considine"/>
 
According to Price, Boulstred wrote the piece in the style conforming to what Halli calls “a precise formula, which includes a declarative beginning, a series of noun clauses, conceited similes and metaphors, and an effective concluding praise.”<ref name="Price"/>

==References==
{{Reflist}}

{{DEFAULTSORT:Boulstred, Cecily}}
[[Category:Year of birth missing]]
[[Category:1609 deaths]]