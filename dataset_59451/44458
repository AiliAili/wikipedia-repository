{{Infobox software
| name = Moonlight
| logo = [[File:MoonlightLogo.png|200px]]
| screenshot =
| caption =
| developer = [[Novell]]
| released = {{Start date and age|2009|03|04}}
| frequently_updated = yes <!-- Release version update? Don't edit this page, just click on the version number! -->
| status = Discontinued<ref name="infoq_abandon"/><ref name="zdnet_abandon"/>
| programming language = [[C (programming language)|C]], [[C++]], [[C Sharp (programming language)|C#]]
| operating system = [[Linux]]
| genre = [[Web application|Web Application framework]]
| license = [[GNU Lesser General Public License|LGPL]] 2 with proprietary codecs
| website = 
{{plainlist|
*{{URL|mono-project.com/moonlight}}
*{{URL|go-mono.com/moonlight}}
}}
}}

'''Moonlight''' is a [[free and open source software|free and open source]] implementation of the now deprecated [[Microsoft Silverlight]] application framework for [[Linux]] and other [[Unix]]-based operating systems, developed and then abandoned by the [[Mono (software)|Mono]] Project.<ref name="infoq_abandon">{{cite web
|url=http://www.infoq.com/news/2012/05/Miguel-Moonlight
|title=Miguel de Icaza on ASP.NET MVC, Moonlight, and the Android Lawsuit
|author=[[Miguel de Icaza]]
|publisher=InfoQ.com
|date=May 29, 2012
|accessdate=2014-02-06}}
</ref><ref name="zdnet_abandon">
{{cite web
|last=Foley
|first=Mary-Jo
|title=Xamarin abandons its Silverlight for Linux technology
|url=http://www.zdnet.com/blog/microsoft/xamarin-abandons-its-silverlight-for-linux-technology/12797
|work=All About Microsoft
|publisher=ZDNet
|accessdate=1 June 2012}}</ref> Like Silverlight, Moonlight was a web application framework which provided capabilities similar to those of [[Adobe Flash]], integrating multimedia, graphics, animations and interactivity into a single runtime environment.

==History and overview==
{| class="wikitable" style="float:right;margin-left:1em"
|+ Release History
! Date !! Version
|-
| 2009-02-11 || Moonlight 1.0<ref>http://tirania.org/blog/archive/2009/Feb-11.html</ref>
|-
| 2009-12-17 || Moonlight 2.0<ref>http://tirania.org/blog/archive/2009/Dec-17.html</ref>
|-
| 2010-02-03 || Moonlight 3.0 Preview 1<ref>http://tirania.org/blog/archive/2010/Feb-03.html</ref>
|- 
| 2011-02-15 || Moonlight 4 Preview 1<ref>http://tirania.org/blog/archive/2011/Feb-16.html</ref>
|}
In an interview in the beginning of June 2007, [[Miguel de Icaza]] said the Mono team expected to offer a "feasibility 'alpha' demo" in mid-June 2007, with support for [[Mozilla Firefox]] on [[Linux]] by the end of the year.<ref>{{cite web | url = http://fastforwardblog.com/2007/06/01/expect-a-june-demo-of-silverlight-on-linux-sans-browser/ | title = the FASTforward blog: Expect a June demo of Silverlight on Linux, sans browser | author = Dana Gardner | accessdate = 2007-06-06}}</ref>

After a 21-day hacking spree by the Mono team (including [[Chris Toshok]], [[Larry Ewing]] and [[Jeffrey Stedfast]] among others), a public demo was shown at Microsoft ReMIX conference in [[Paris]], [[France]] on June 21, 2007.<ref>{{cite web
| url = http://tirania.org/blog/archive/2007/Jun-21.html
| publisher = Miguel de Icaza
| title = Implementing Silverlight in 21 Days
| accessdate = 2007-06-22}}</ref><ref>{{cite web
| url = http://jeffreystedfast.blogspot.com/2007/06/implementing-silverlight-in-21-days.html
| publisher = Jeffrey Stedfast
| title = Implementing Silverlight in 21 Days
| accessdate = 2007-06-21}}</ref><ref>{{cite web
| url = http://squeedlyspooch.com/blog/2007/06/21/moonlight/
| publisher = Chris Toshok
| title = Moonlight
| accessdate = 2007-06-21}}</ref><ref>{{cite web
| url = http://arstechnica.com/news.ars/post/20070622-mono-silverlight-implementation-emerges-after-epic-hackathon.html
| publisher = Ars Technica
| title = Mono Silverlight implementation emerges after epic hackathon
| accessdate = 2007-06-22}}</ref>

However, in September 2007, developers still needed to install and compile a lot of Mono and [[Mono Olive|Olive]] (the experimental Mono subproject for [[.NET Framework#.NET Framework 3.0|.NET 3.0]] support) modules from the Mono [[Subversion (software)|SVN]] repository to be able to test Moonlight.<ref name="status">{{cite web
| url = http://www.mono-project.com/Moonlight#Getting_Started
| title = Moonlight: Getting started
| publisher = Mono Team
| accessdate = 2007-09-02}}</ref> A Moonlight IDE, named Lunar Eclipse, exists in SVN for [[XAML]] designs. Moonlight uses [[Cairo (graphics)|Cairo]] for rendering.<ref name=mainpage>[http://www.mono-project.com/MoonlightNotes#Rendering Moonlight Notes]</ref>

Moonlight was provided as a plugin for Firefox and Chrome on popular Linux distributions.<ref>{{cite web
| url = http://mono-project.com/MoonlightSupportedPlatforms
| title = Moonlight Supported Platforms
| publisher = mono-project.com
| accessdate = 2009-05-17}}</ref> The plugin itself does not include a media codec pack, but when the Moonlight plugin detects playable media it refers users to download a free Media codec pack from Microsoft.

Moonlight 2.0 tracked the Silverlight 2.0 implementation. The first completed version, Moonlight 1.0, supporting Silverlight 1.0, was released in January 2009. Moonlight 2.0 was released in December 2009.<ref>{{Cite web|url=http://team.silverlight.net/announcement/moonlight-2-is-now-available/|title=Moonlight 2 is now available|date=2009-12-17|publisher=The Silverlight Team blog}}</ref> The Moonlight 2.0 release also contained some features of Silverlight 3 including a pluggable media framework which allowed Moonlight to work with pluggable open codecs, such as [[Theora]] and [[Dirac (codec)|Dirac]].<ref>{{cite web
| url = http://tirania.org/blog/archive/2009/May-04.html
| title = First Moonlight 2.0 Preview is Out
| publisher = Miguel de Icaza
| accessdate = 2009-05-04
| quote = We have developed a handful of open source codecs for Dirac, Vorbis and ADPCM that can be used with Silverlight 3/Moonlight Preview based on existing C# and Java implementations. Hopefully someone will help us fill in the blanks with more codecs (like Theora).}}</ref>

Preview releases of Moonlight 4.0, targeting Silverlight 4 compatibility, were released in early 2011.<ref>{{Cite web|url=http://team.silverlight.net/announcement/moonlight-4-preview-now-available/|title=Moonlight 4 Preview Now Available|date=2011-02-15|publisher=The Silverlight Team Blog}}</ref>

In April 2011, the Moonlight team demonstrated Moonlight running on Android tablets and phones at the MIX11 Web Developers conference in Las Vegas.<ref>{{Cite web|url=http://jeffreystedfast.blogspot.com/2011/04/moonlight-on-android.html|title=Moonlight on Android|date=2011-04-15|publisher=A Moment of Zen}}</ref>

Shortly after the April 2011 release, [[Attachmate]], parent to developer Mono, laid off an undisclosed number of Mono employees,<ref>{{Cite web|url=http://www.internetnews.com/skerner/2011/05/attachmate-lays-off-mono-emplo.html|title=Attachmate lays off Mono employees|date=2011-05-03}}</ref> and announced a deal with startup [[Xamarin]] for Mono development and support.<ref>{{Cite web|url=http://www.datamation.com/open-source/suse-joins-xamarin-for-mono.html|title=SUSE Joins Xamarin for Mono|date=2011-08-10}}</ref>  At that time, Xamarin CEO [[Nat Friedman]] affirmed their commitment to the Moonlight project, although there were no outward signs of any further development afterward.

In December 2011, de Icaza announced that work on Moonlight had stopped with no future plans. He explained that Microsoft had "cut the air supply" to it by omitting cross-platform components, making it a web-only plugin, and including Windows-only features. He advised developers to [[Separation of concerns|separate]] user interface code from the rest of their application development to ensure "a great UI experience on every platform (Mac, Linux, Android, iOS, Windows and Web)" without being dependent on third party [[Application programming interface|APIs]].<ref>{{Cite web|url=http://lists.ximian.com/pipermail/moonlight-list/2011-December/001392.html|title=Moonlight Mailing List|date=2011-15-15|publisher=Xamarin}}</ref>

==DRM==
Silverlight supports [[Digital Rights Management]] in its multimedia stack, but Microsoft will not license their [[PlayReady]] DRM software for the Moonlight project to use and so Moonlight is unable to play encrypted content.<ref>{{Cite web|url=https://social.msdn.microsoft.com/Forums/silverlight/en-US/e680463e-ba1c-403b-9762-f8980df4680f/a-plea-for-drm-support-in-linuxmoonlight|title=Discussion on official Silverlight forum}}</ref>

==Desktop support==
Moonlight was also usable outside of the browser as a [[Gtk+]] widget (known as Moonlight.Gtk). A number of Desklets were written using this new technology during the Novell Hack Week in 2007.<ref>
{{cite web
| url = https://www.youtube.com/watch?v=IbMyPG4IKo8
| title = Moonlight Desklets demo on YouTube
| publisher = Mono Team
| accessdate = 2007-06-25}}</ref>

MoonBase is an experimental set of helper classes built on top of Moonlight.Gtk that can be used to create full blown C# desktop applications using the Moonlight (Silverlight 4.0) widgets and XAML files.<ref>[https://github.com/inorton/MoonBase MoonBase]</ref> MoonBase also has a related XAML editor/previewer.<ref>[https://github.com/inorton/XamlPreviewer XamlPreviewer]</ref>

==Microsoft support==
Shortly after the first demo at MIX 07 in Paris, Microsoft began cooperating with Novell to help with the building of Moonlight.<ref>{{cite web
| url = http://blogs.zdnet.com/microsoft/?p=695
| title = Microsoft officially ‘extends support’ for Novell's Silverlight Linux port
| publisher = zdnet.com
| last = Foley | first = Mary Jo
| date = 2007-09-25
| accessdate = 2007-10-13}}</ref> Support included giving exclusive access to Novell for the following Silverlight artifacts:<ref name="icaza0905">{{cite web
| url = http://tirania.org/blog/archive/2007/Sep-05.html
| title = Microsoft/Novell Collaboration on Silverlight.
| last = de Icaza | first = Miguel
| date = 2007-09-05
| accessdate = 2007-10-13}}</ref>
* Microsoft's Test suites for Silverlight,
* Silverlight specification details, beyond those available on the web,
* Proprietary [[codec]]s made available free-of-charge for [[Windows Media Video]] and [[Windows Media Audio|Audio]], for [[VC-1]] and [[MP3]], and in the future [[H.264]] and [[Advanced Audio Coding|AAC]], only licensed for use with Moonlight when running in a web browser. Other potential decoders include [[GStreamer]] and [[FFmpeg]] (used during the development stage) but Novell will not provide prepackaged versions of Moonlight with those libraries, because those decoders have not been granted licensing for the use of patented codec technologies.

Microsoft released two public covenants not to sue for the infringement of its patents when using Moonlight. The first one covered Moonlight 1 and 2, is quite restrictive and covered only the use of Moonlight as a plugin in a browser, only implementations that are not [[GNU General Public License#Version 3|GPLv3]] licensed, and only if the Moonlight implementation has been obtained from Novell. It also notes that Microsoft may rescind these usage rights.<ref>{{cite web
| url = http://www.microsoft.com/interop/msnovellcollab/moonlight.mspx
| title = Covenant to Downstream Recipients of Moonlight - Microsoft & Novell Interoperability Collaboration
| publisher = [[Microsoft]]
| quote = ''"Downstream Recipient" means an entity or individual that uses for its intended purpose a Moonlight Implementation obtained directly from Novell or through an Intermediate Recipient... Microsoft reserves the right to update (including discontinue) the foregoing covenant... "Moonlight Implementation" means only those specific portions of Moonlight 1.0 or Moonlight 1.1 that run only as a plug-in to a browser on a Personal Computer and are not licensed under GPLv3 or a Similar License.''
| date = 2007-09-28
| accessdate = 2008-03-08}}</ref>

The second covenant was an updated and broader covenant that no longer limits the covenant to users that obtain Moonlight from Novell, it covers any uses of Moonlight regardless of where it was obtained. The updated covenant covers the implementations as shipped by Novell for versions 3 and 4, it no longer distinguishes Novell from other distributions of Moonlight and expands the covenant to desktop applications created with Moonlight. The covenant does not extend to forks licensed under the GNU GPL (Moonlight itself uses the Lesser GPLv2).<ref>{{cite web
| url = http://www.microsoft.com/interop/msnovellcollab/newmoonlight.mspx
| title = Covenant to End Users of Moonlight 3 and 4
| publisher = [[Microsoft]]
| accessdate = 2010-05-11}}</ref>

==Codecs integration==
Although Moonlight is free software, the final version was going to use binary-only audio and video [[codecs]] provided by Microsoft which will be licensed for use with Moonlight only when used as a browser plugin (see above). The Windows media pack is not distributed together with the Moonlight plugin but the first time when media content in Silverlight is detected the user will be prompted to download the pack containing the codecs used in Silverlight directly from Microsoft.

Self built versions could still use the FFmpeg library and there was discussion about adding GStreamer support as an alternative to using Microsoft's binary codecs for those who wish to use GStreamer instead and also for use when used outside of a browser.

[[Mono (software)|Mono]] architect [[Miguel de Icaza]] blogged that the Mono team prototyped Moonlight multimedia support using the [[GNU Lesser General Public License|LGPL]]-licensed [[FFmpeg]] engine but that they were unable to redistribute packaged versions that used that library due to FFmpeg codec licensing issues inside of the United States.<ref name="icaza0905"/><ref>{{cite web
| url = http://port25.technet.com/archive/2007/09/05/silverlight-on-linux.aspx
| title = The Novell-Microsoft Wheeler Dealers Speak
| quote = ''After a great deal of work between the Moonlight and .NET teams, we’re ready to formally announce that we (Microsoft and Novell) will be bringing Silverlight to Linux'' (Sam Ramji is Director of Microsoft's Open Source Software Lab)
| last = Ramji | first = Sam
| date = 2007-09-05
| accessdate = 2007-10-13}}</ref>

==Moonlight in other distributions==
After the release of Moonlight 2, a covenant provided by Microsoft was updated to ensure that other third party distributors can distribute Moonlight without their users having to worry about getting sued over patent infringement by Microsoft.<ref name="deIcaza-2009-12-17">{{cite web|url=http://tirania.org/blog/archive/2009/Dec-17.html|title=Releasing Moonlight 2, Roadmap to Moonlight 3 and 4|author=Miguel de Icaza|date=2009-12-17|accessdate=2009-12-18}}</ref> This covenant can be found on the [http://www.microsoft.com/interop/msnovellcollab/newmoonlight.mspx Microsoft website].

Kevin Kofler and Tom Callaway, of [[Fedora (operating system)|Fedora]], have stated publicly that the last covenant was "not acceptable" for that distribution and that "it is still not permissible in Fedora".<ref>{{cite web |url=http://thread.gmane.org/gmane.linux.redhat.fedora.devel/126037/focus=126216 |title=Re: New covenant published |author=Tom Callaway |publisher=gmane.linux.redhat.fedora.devel |accessdate=2010-01-11 |date=December 23, 2009 }}</ref>

The version of Moonlight that was going to be available direct from Novell would have access to licensed closed source media codecs provided free of charge by Microsoft. Third-party distributions of Moonlight would only be able to play non-patent encumbered media like [[Vorbis]], [[Theora]] and [[Ogg]]. To support other formats, the distributors would have had to choose from a few licensing options:
* Negotiate licences directly with individual media codec owners (e.g. [[MPEG-LA]], [[Fraunhofer Society]])
* Negotiate access to Microsoft's Media Pack as Novell have done
* Use [[GStreamer]] or a commercial codec license
* Use a hardware-specific software like [[VDPAU]]<ref name="deIcaza-2009-12-17"/>

At the PDC conference on October 13, 2008, Microsoft placed the 'Silverlight XAML Vocabulary' under the [[Microsoft Open Specification Promise]],<ref>{{cite web
| url = http://www.microsoft.com/presspass/press/2008/oct08/10-13Silverlight2PR.mspx
| title = Microsoft Releases Silverlight 2, Already Reaching One in Four Consumers Worldwide
| publisher = Microsoft
| date = 2008-10-13
| accessdate = 2008-10-13}}</ref> stating in a press release, "The Silverlight XAML vocabulary specification, released under the Microsoft Open Specification Promise, will better enable third-party ISVs to create products that can read and write XAML for Silverlight." Since Moonlight is essentially a XAML reader, [[Debian]]'s position is that Moonlight is safe for them to redistribute (leaving each user to agree to their own licensing for Microsoft's and others' binary codecs).<ref>{{cite web
| url = http://wiki.debian.org/Teams/DebianMonoGroup/Moonlight
| title = Moonlight for Debian
| publisher = Debian Wiki
| quote = Moonlight 1.0 is essentially a XAML renderer with codec support. All plugin logic is handled by the browser's Javascript engine. XAML is covered by an irrevocable patent grant from Microsoft, as shown [http://www.microsoft.com/interop/osp/default.mspx here].
| accessdate = 2010-03-09 (Page marked 'this page is out of date')
}}</ref>

==See also==
{{Portal|Free software}}
* [[MonoDevelop]] – an open source [[Integrated development environment|IDE]] targeting both Mono and Microsoft [[.NET framework]] platforms

==References==
{{Reflist|30em}}

==External links==
* {{Official website|http://www.mono-project.com/Moonlight}}
* [http://www.webmonkey.com/2007/06/microsoft_silverlight_coming_to_linux/ Wired - Microsoft Silverlight Coming to Linux]
* {{webarchive |date=2012-12-06 |url=http://archive.is/20121206004849/http://news.com.com/8301-10784_3-9731410-7.html |title=CNET - 'Moonlight' makes progress on Silverlight for Linux}}
* [http://tirania.org/blog/archive/2008/Dec-02.html Moonlight 1.0 Media Stack article] by [[Miguel de Icaza]]
* [http://www.h-online.com/open/features/Health-Check-Moonlight-904464.html The H Open Source - Health Check: Moonlight]

{{Rich Internet applications}}

{{DEFAULTSORT:Moonlight (Runtime)}}
[[Category:Free multimedia software]]
[[Category:Free software programmed in C]]
[[Category:Free software programmed in C++]]
[[Category:Free software programmed in C Sharp]]
[[Category:Mono (software)]]
[[Category:Silverlight]]