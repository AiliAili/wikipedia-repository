<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox Aircraft Begin
 |name = R.T.1 Kangaroo
 |image =Blackburn Kangaroo Q 063799.jpg
 |caption =
}}{{Infobox Aircraft Type
 |type = Reconnaissance Torpedo Bomber
 |manufacturer = [[Blackburn Aircraft|Blackburn Aeroplane and Motor Co Ltd]]
 |designer = 
 |first flight = [[1918 in aviation|1918]]
 |introduced = [[1918 in aviation|1918]]
 |retired = [[1929 in aviation|1929]]
 |produced =
 |number built = 20
 |status =
 |unit cost =
 |primary user = [[Royal Air Force]]
 |more users = [[Peruvian Air Force|Peruvian Army Flying Service]]
 |developed from = [[Blackburn G.P.]]
 |variants with their own articles =
}}
|}

The '''Blackburn R.T.1 Kangaroo''' was a [[United Kingdom|British]] twin-engine [[reconnaissance]] [[torpedo bomber|torpedo]] [[biplane]] of the [[World War I|First World War]], built by [[Blackburn Aircraft]].

==World War I==
In [[1916 in aviation|1916]], the Blackburn Aircraft Company designed and built two prototypes of an [[anti-submarine warfare|anti-submarine]] [[floatplane]] designated the [[Blackburn G.P.]] or Blackburn General Purpose. It was not ordered but Blackburn developed a landplane version as the '''Blackburn R.T.1 Kangaroo''' (Reconnaissance Torpedo Type 1). The first aircraft was delivered to [[RAF Martlesham Heath|Martlesham Heath]] in January [[1918 in aviation|1918]]. Test results were disappointing, with the rear fuselage being prone to twisting and the aircraft suffering control problems, which led to the order for fifty aircraft being cut to twenty, most of which were already partly built.{{sfn|Jackson|1979|p=397}}

From the sixth aircraft, they were powered by the more powerful [[Rolls-Royce Falcon]] III engine replacing the {{convert|250|hp|kW}} Rolls-Royce Falcon II. The Kangaroo entered service later that year with [[No. 246 Squadron RAF]] based at [[Seaton Carew]], [[County Durham]] which had six months of wartime operations, in which they sank one [[U-boat]] and damaged four others. [[SM UC-70|''UC-70'']], was spotted lying submerged on the sea bottom near [[Runswick Bay]] on 28 August 1918, by a Kangaroo flown by Lt E. F. Waring. The U-boat was badly damaged by a near miss by a {{convert|520|lb|kg}} bomb and finished off by the destroyer [[HMS Ouse (1905)|HMS ''Ouse'']].{{sfn|Jackson|1979|p=397}}

==Post-World War I==
In 1919, three surviving RAF Kangaroos were sold to the [[Grahame-White]] Aviation Co Ltd, based at [[Hendon Aerodrome]].{{sfn|Jackson|1979|p=398}} Eight others were sold back to Blackburn Aircraft, three being converted with a glazed cabin for its subsidiary, North Sea Aerial Navigation Co Ltd, also based at [[Brough Aerodrome]].{{sfn|Jackson|1968|pp=124, 165}} Several different configurations were embodied for the civil market, for cargo, pilot training and/or the accommodation of up to eight passengers. In the first few months of 1919, most of these converted aircraft continued to fly (and sometimes crash) in military markings, then the survivors were repainted with civilian registrations and commercial titles. In May 1919, joy-riding, cargo and passenger charters took place at locations including Brough, Leeds, West Hartlepool, Gosport and Hounslow Heath. During August 1919, three Kangaroos flew to [[Amsterdam]] for the ELTA air traffic exhibition and spent several weeks giving flights to an estimated 1,400 passengers. On 30 September 1919, North Sea Aerial Navigation Co Ltd started a regular passenger service between Roundhay Park (Leeds) and [[Hounslow Heath]]. In 1920, the company was renamed North Sea Aerial & General Transport Co Ltd and services were extended to Amsterdam.{{sfn|Jackson|1979|pp=398–400}}

On 21 November 1919, one Kangaroo (G-EAOW) took off from Hounslow Heath in an attempt to win the [[Australia]]n government prize of £A10,000 for the first Australian airman to fly a British aircraft from the UK to Australia within thirty consecutive days. The Kangaroo was forced to make an emergency landing at [[Suda Bay]], [[Crete]] with a suspected sabotaged engine and the aircraft was abandoned there.{{sfn|Lewis|1970|pp=106–109}}{{sfn|Jackson|1968|pp=118–119}} On 8 September 1922, two Kangaroos took part in the King's Cup Air Race from [[Croydon Aerodrome]] but both retired. In 1924, under contract with the North Sea Aerial & General Transport Co Ltd, the RAF used three Kangaroos (named [[Pip, Squeak and Wilfred]] after popular cartoon characters) as dual-control trainers for refresher training but by 1929 the last Kangaroo had been withdrawn from service and scrapped.{{sfn|Jackson|1974|p=402}}

==Operators==

===Military===
;{{PER}}
* [[Peruvian Air Force|Peruvian Army Flying Service]]
;{{UK}}
* [[Royal Air Force]]
** [[No. 246 Squadron RAF]]

===Civilian===
;{{UK}}
* The Grahame-White Aviation Company
* North Sea Aerial Navigation Co Ltd, renamed in 1920 as North Sea Aerial & General Transport Co Ltd

==Specifications (first prototype)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=Blackburn Aircraft since 1909{{sfn|Jackson|1968|p=122}}
|crew=3
|length main= 44 ft 2 in
|length alt= 13.46 m
|span main= 74 ft 10 in
|span alt= 22.82 m
|height main= 16 ft 10 in
|height alt= 5.13 m
|area main= 868 ft²
|area alt= 80.64 m²
|empty weight main= 5,284 lb
|empty weight alt= 2,397 kg
|loaded weight main=
|loaded weight alt=
|max takeoff weight main= 8,017 lb
|max takeoff weight alt= 3,636 kg
|engine (prop)=[[Rolls-Royce Falcon]] II
|type of prop=liquid-cooled [[V12 engine]]
|number of props=2
|power main=250 hp
|power alt=186 kW
|max speed main=98 mph at 6,500 ft (1980 m)
|max speed alt=158 km/h
|range main=
|range alt=
|ceiling main=  13,000 ft
|ceiling alt=  3,960 m
|climb rate main= (initial) 480 ft/min
|climb rate alt= 2.44 m/s
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|performance more=*'''Endurance:''' 8 hours
|armament=
* 2× [[0.303 British|0.303 in (7.7 mm)]] [[Lewis Gun|Lewis machine gun]]
* Up to 920 lb (417 kg) of bombs
}}

==See also==
{{aircontent
|related=
|similar aircraft=
|lists=*[[List of aircraft of the RAF]]
|see also=
}}

==Footnotes==
{{reflist}}

==References==
{{refbegin}}
* {{cite book |ref={{harvid|Jackson|1968}}
|last=Jackson |first=A. J. |authorlink=|title= Blackburn Aircraft since 1909 |year=1968 |publisher=Putnam |location=London |isbn=0-370-00053-6}}
* {{cite book |ref={{harvid|Jackson|1974}}
|last=Jackson |first=A. J. |authorlink= |title=British Civil Aircraft since 1919 |year= 1974 |publisher= Putnam |location=London |isbn=0-370-10014-X}}
* {{cite journal |ref={{harvid|Jackson|1979}}
|last=Jackson |first=A. J. |date=August 1979 |title=Blackburn's Marsupial |journal=Aeroplane Monthly |publisher=IPC |location=London |volume=7 |issue=8 |pages=396–402 |issn=0143-7240}}
* {{cite book |ref={{harvid|Lewis|1970}}
|last=Lewis |first=Peter |title=British Racing and Record-Breaking Aircraft |year=1970 |publisher=Putnam |location=London |isbn=0-370-00067-6}}
{{refend}}

==Further reading==
{{refbegin}}
* {{cite book |author=<!--Staff writer(s); no by-line.--> |title=The Illustrated Encyclopedia of Aircraft |year=1981–1985 |others=in 216 parts |publisher=Orbis |location=London |oclc=669683964}}
{{refend}}

==External links==
{{commons category|Blackburn Kangaroo}}
* [http://www.flightglobal.com/FlightPDFArchive/1919/1919%20-%201468.PDF The Flight to Australia, 13 November 1919]

{{Blackburn aircraft}}

[[Category:Blackburn aircraft|Kangaroo]]
[[Category:British bomber aircraft 1910–1919]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Biplanes]]