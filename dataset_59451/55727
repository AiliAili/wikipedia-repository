{{Infobox journal
| title = Arts Education Policy Review
| cover =
| formernames = Design, Design For Arts in Education
| editor = Colleen Conway
| discipline = [[Visual arts education|Arts education]]
| abbreviation = Arts Educ. Policy Rev.
| publisher = [[Routledge]]
| country = United States
| frequency = Quarterly
| history = 1899-present
| openaccess = 
| impact =
| impact-year =
| website = http://www.tandfonline.com/action/journalInformation?journalCode=vaep20#
| link1 = http://www.tandfonline.com/toc/vaep20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/vaep20
| link2-name = Online archive
| OCLC = 807239055
| LCCN = 93644308
| CODEN =
| ISSN = 1063-2913
| eISSN = 1940-4395
}}
The '''''Arts Education Policy Review''''' is a quarterly [[peer-reviewed]] [[academic journal]] of [[Visual arts education|arts education]]. It covers research on [[K–12 (education)|K–12]] [[Visual arts education|arts education]] policy. It is published by [[Routledge]] and the [[editor-in-chief]] is Colleen Conway ([[University of Michigan]]). It was established in 1899 as ''Design'' and renamed ''Design For Arts in Education'' in 1977, before obtaining its current name in 1992.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[EBSCO Information Services|EBSCO databases]]
* [[MLA International Bibliography]]
* [[Répertoire International de Littérature Musicale]]
* [[TOC Premier]]
* [[Education Resources Information Center]]
* [[Expanded Academic ASAP]]
* [[InfoTrac]]
* [[ProQuest|ProQuest databases]]
}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=vaep20#}}

{{DEFAULTSORT:Arts Education Policy Review}}
[[Category:English-language journals]]
[[Category:Education journals]]
[[Category:Arts journals]]
[[Category:Routledge academic journals]]
[[Category:Publications established in 1899]]
[[Category:Quarterly journals]]
{{Education-journal-stub}}