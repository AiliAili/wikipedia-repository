{{Infobox mineral
| name        = Hubeite
| image       = Inesite-Hubeite-Apophyllite-158570.jpg
| caption     = Hubeite cluster in [[apophyllite]]
| category    = Sorosilicate
| formula     = {{chem|Ca|2|Mn|2+|Fe|3+|[Si|4|O|12|(OH)]·(H|2|O)|2}}
| system      = [[Triclinic]]
| class       = Pinacoidal ({{overline|1}}) <br/><small>(same [[H-M symbol]])</small>
| symmetry    = ''P''{{overline|1}}
| unit cell   = a = 9.96&nbsp;[[angstrom|Å]], b = 13.875&nbsp;Å <br/>c = 6.562&nbsp;Å; α = 133.19° <br/>β = 101.5°, γ = 66.27°, Z&nbsp;=&nbsp;2 
| color       = Dark to pale brown
| habit       = Aggregates of inter-grown crystals
| cleavage    = One good cleavage parallel to c-axis
| fracture    = Conchoidal fracture
| tenacity    = Brittle
| mohs        = 5.5
| luster      = Vitreous
| streak      = Orange-brown
| gravity     = 3.02
| opticalprop = Biaxial (-) bire=0.0230
| refractive  =  n<sub>α</sub>=1.667, n<sub>β</sub>=1.679, n<sub>γ</sub>=1.69
| birefringence = 0.0230 (γ-α)
| pleochroism = Yes
| 2V          = 87(5)° and 89(2)°
| fluorescence = None
| references  =<ref>Hawthorne F C, Cooper M A, Grice J D, Roberts A C, Cook W R, Lauf R J (2002) Hubeite, a new mineral from the Daye mine near Huangshi, Hubei Province, China, The Mineralogical Record, 33, 465-471.</ref><ref>[http://webmineral.com/data/Hubeite.shtml#.Va223KS6fRY Hubeite data on Webmineral]</ref>
}}

The [[mineral]] '''hubeite''', {{chem|Ca|2|Mn|2+|Fe|3+|[Si|4|O|12|(OH)]·(H|2|O)|2}}, is a [[sorosilicate]] of the {{chem|Si|4|O|13}} group. Structurally it also belongs to the [[Akatoreite|Akatoreite group]]. It was found and named after the province of [[Hubei]], [[China]]. It is common to [[iron ores]] in a mine of that region. It occurs mainly as aggregates of fan like crystals. It is dark to pale brown, has orange-brown [[Streak (mineralogy)|streak]] and is vitreous. Hubeite has a hardness of 5.5 in the [[Mohs scale]], one good [[Cleavage (mineralogy)|cleavage]] and [[conchoidal fracture]]. It is [[triclinic]] with a space group of P1*. The structure of hubeite is very uncommon, and in fact there is only one other mineral that fits the {{chem|Si|4|O|13}} group, which is [[ruizite]].

== Background ==

Hubeite was discovered by Hawthorne et al. (2002) at the [[Daye, Hubei|Daye mines]] in the [[Hubei]] province of China. It is classified as a [[sorosilicate]], based on its formula (Hawthorn et al., 2004). Other related minerals would be inesite, (Hawthorne et al., 2004), [[ruizite]] (Hawthorne et al., 2002) and [[Akatoreite]] (Burns et al., 1993).

== Composition ==

To analyze the composition, an [[electron microprobe]] was used in the wavelength-dispersion mode (Hawthorn et al., 2002).  The quantity of [[Hydroxide group|(OH)]] and ([[H2O]]) was acquired by [[solid solution]] and refinement, based on previous work by Hawthorne et al., 1990. To assure the presence of (OH) and (H<sub>2</sub>O) groups, an [[infrared spectrum]] was also recorded (Hawthorn et al., 2002).

== Physical and optical properties ==

Hubeite is most common as aggregates of intergrown crystals (Fig.1) that are usually less than 5&nbsp;mm across and that have individual crystals with well-developed faces that are as long as 1&nbsp;mm (Hawthorne et al., 2002). The color ranges from pale to dark brown, depending on the crystal size (Fig.2). Other properties consist of a pale orange-brown [[Streak (mineralogy)|streak]], vitreous [[lustre (mineralogy)|luster]], non-[[fluorescence]], and one good [[Cleavage (crystal)|cleavage]] parallel to the c-axis. It is also [[brittle]] with [[conchoidal fracture]], has a hardness of 5.5 in the [[Mohs scale]] and a specific gravity of 3.02 (Hawthorn et al., 2002). As for optical features, it is important to note that hubeite is strongly [[pleochroic]], [[biaxial]] with an indeterminate optic sign and has a [[birefringence]] of 0.023 (γ-α) (Hawthorne et al., 2002).[[File:Inesite-Apophyllite-Hubeite-23645.jpg|thumbnail|left|Fig 1- Bow-tie aggregate of hubeite crystals]] [[File:Inesite-Hubeite-Apophyllite-158569.jpg|thumbnail|center|Fig 2 –Color representation of hubeite]]

== Structure ==

The crystals used for structure study were acquired at the [[Daye, Hubei|Daye Mine]] (Hawthorne et al., 2004). To get a first general idea of the mineral structure it went through X-ray intensity data analysis and then, for a more detailed study, an [[electron microprobe]] was used (Hawthorne et al., 2004). Hubeite is [[triclinic]] (P1*). Basically, there are two Ca sites in the structure of hubeite, with site one being and [[octahedron]] and the second site is coordinated by 6 oxygen atoms at the same distance and one extra oxygen atom further out and arranged in an augmented octahedron (Hawthorne et al., 2004). There are also 4 sites for Si in [[tetrahedral]] arrangement, and the fourth site bonds to an OH group forming an acid-silicate group (SiO<sub>3</sub>(OH)) (Hawthorne et al., 2004). There are 2 oxygen sites that connect 2 Si atoms, thus creating a sorosilicate (Hawthorne et al., 2002). [Si<sub>4</sub>O<sub>13</sub>] corresponds to a four membered chain fragment of tetrahedra according to Hawthorne et al. (2004). The only other sorosilicate mineral that has that same four membered configuration is [[ruizite]] (Moore et al., 1985). The main difference of the two minerals is the valence of Mn and the existence of Fe<sup>3+</sup> for Hubeite (Hawthorne et al., 2002). Ruizite is of the [Si<sub>4</sub>O<sub>13</sub>] sorosilicate group (Hawthorne, 1984) and when it was discovered, it did not much any other Ca-Mn silicate already known (Willams et al., 1977), and now with the discovery of hubeite it is easier to understand the [Si<sub>4</sub>O<sub>13</sub>] [[sorosilicate]] group. The other two sites left in the hubeite structure are filled with Fe with CN=6 and Mn with CN=6, being one of the bonds to OH in the Mn case. The structure of hubeite is heteropolyhedra, with alternating layers of tetrahedra and different [[polyhedra]] parallel to (001) (Hawthorne et al., 2004). The tetrahedral layers are formed by [Si<sub>4</sub>O<sub>13</sub>] sharing corners, and the other alternating layer is formed by the [6], [7] and [8] Ca, Mn<sup>2+</sup> and Fe<sup>3+</sup> polyhedral sharing edges (Hawthorne et al., 2004). This last feature is what relates hubeite to the akatoreite group. [[Akatoreite]], like hubeite, is triclinic with space group P1*(Burns et al., 1993). [[Akatoreite]]’ structure is layerd as well with alternating sheets of octahedra and tetrahedra, parallel to (101) (Burns et al., 1993). The octahedra groups, as well as one Mn tetrahedra group, are sharing edges and linked by the corner sharing tetrahedral. The same happens in [[ruizite]], except that they are linked by the [Si<sub>4</sub>O<sub>13</sub>] group. The inesite structure also relates very well to the hubeite structure. It is also based on layers of edge sharing polyhedra alternating with corner sharing tetrahedra (Hawthrone et al., 2004). The main difference is that inesite is a [[cyclosilicate]], and in fact, by omitting 2 of the 6 tetrahedra that form the tetraheda ring, and if the other 8 membered ring is broken and hydoxylated, the new arrangement becomes a hubeite (Hawthorne et al., 2004). This just confirms the association of hubeite and inesite in the Daye mines (Hawthorn et al., 2004).

== Geological occurrence ==

Hubeite is mainly associated to a [[skarn]] assemblage with pink inesite, colorless [[apophyllite]], [[quartz]], [[pyrite]] and colorless-white [[calcite]] (Hawthorne et al., 2004). They all occur together at the Daye Mine. Usually hubeite appears in two different situations. It may occur as isolated aggregates of crystals perched on white quartz, or it may occur covering both sides of thick specimens, that are usually pink inesite and [[apophyllite]] (Hawthorne et al., 2002). Figures 3 and 4 illustrate both situations. [[File:Hubeite-Inesite-Quartz-20438.jpg|thumbnail|left|Fig 3 -Hubite occurrence on quartz]] [[File:Inesite-Hubeite-20506.jpg|thumbnail|center|Fig 4 – Hubite occurrence on pink inesite]]

The localities where ruizite is found, associated with apophyllite, inesite and pyrite as well, and there is no hubeite, which leads the conclusion that hubeite needs oxidized environments and sufficient concentration of iron to occur.
The Daye mine is an iron ore deposit (Dingyu et al., 1982). This specific area is characterized by deposits of late [[Paleozoic]] carbonate rocks in contact with [[plutons]] aging between the middle [[Jurassic]] to middle [[Cretaceous]] (Dingyu et al., 1982). According to Dingyu et al. (1982), the iron rich magma injections are the main cause for the formation of the ore deposits of the region. These [[polymetallic|polymetallic deposits]] form a belt that crosses China in the west-east direction (Ottens, 2007). Curiously, the mine where hubeite was first found is in fact a [[wollastonite]] source for minerals collectors.

== Locations ==

Hawthorne et al. (2002) discovered hubeite in the [[Daye, Hubei|Daye]] mine, in the [[Hubei]] province of China. This mine became famous after that discovery and despite this specific breakthrough, the mine is most popular for its crystals of inesite and wollastonite (Ottens, 2007). It opened up in 1966 for copper exploration, but after the lack of profits, it became a major source for wollastonite (Ottens, 2007). Luckily, along the area of the Daye mine, there are other skarn-type iron and copper deposits that are big contributors to the overall reserves of copper and iron in China (Ottens, 2007). Daye County is also rich in non-metallic mineral deposits, but it is the metallic ores that make it special and it is an important city for the manufacture of bronze (Ottens, 2007).
The Hubei province has gold and silver production as the main revenue source (Ottens, 2007). This province is also one of the birthplaces of the [[Chinese Bronze Age|Chinese bronze-age culture]], represented in the artwork of the [[Yangtze River|Yangtze River culture]] (Ottens, 2007). The copper extraction began in this area is related to the [[Yin Dynasty]] and the iron extraction began in the [[Qing Dynasty]], making these mines a “symbol” in the Chinese culture (Ottens, 2007).

== References ==

{{reflist}}

* Burns P C, Hawthorne F C (1993) Edge-Sharing Mn2+O4 tetrahedra in the structure of Akatoreite, Mn9Al2Si8O24(OH)8, The Canadian Mineralogist, 31, 321-329.
* Hawthorne F C, Cooper M A, Grice J D, Roberts A C, Cook W R, Lauf R J (2002) Hubeite, a new mineral from the Daye mine near Huangshi, Hubei Province, China, The Mineralogical Record, 33, 465-471.
* Hawthorne F C, Cooper M A (2004) The crystal structure of hubeite, a novel sorosilicate mineral, The Canadian Mineralogist, 42, 825-834.
* Hawthorne F C, Grice J D (1990) Crystal-structure analysis as a chemical analytical method:  application to light elements, The Canadian Mineralogist, 28, 693-702.
* Moore P B, Shen J, Araki T (1985) Crystal chemistry of the [M*φ2(TO4)2] sheet: structural principals and crystal structures of ruizite, macfallite and orientite, The American Mineralogist, 70, 171-181.
* Ottens B (2007) The Fengjiashan Mine; Daye District, Ezhou Prefecture, Hubei Province, China.  Mineralogical Record, 38(1), 33-42.

== External links ==
* [http://rruff.info/Hubeite Hubeite]

[[Category:Sorosilicates]]
[[Category:Calcium minerals]]
[[Category:Iron minerals]]
[[Category:Manganese minerals]]
[[Category:Triclinic minerals]]
[[Category:Hubei]]