{{Infobox company
| name             = Farrow Ltd.
| logo             = [[File:Farrow Logo.png|200px]]
| type             = Limited
| genre            = 
| fate             = 
|former_names = Russel A. Farrow
| predecessor      = Farrow Group<br />Russell A. Farrow
| successor        = 
| foundation       = 1911
| founder          = Russell Alexander Farrow
| defunct          = 
| location         = 2001 Huron Church Road<br />[[Windsor, Ontario]]<br />N9C 2L6
| locations        = 
| area_served      = Worldwide
| key_people       = 
| industry         = Customs Brokerage<br />Logistics<br />International Trade
| products         = 
| services         = [[Customs brokerage]]<br />Customs and Trade Consulting<br />Freight Forwarding<br />Trade Compliance Software<br />Logistics Outsourcing 
| market cap       = 
| revenue          = 
| operating_income = 
| net_income       = 
| aum              = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 500-1000
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{url|www.farrow.com}}
| footnotes        = 
| intl             = 
}}

'''Farrow Ltd.''', previously known as "Russell A. Farrow Ltd.",<ref name="Farrow Rebrand">{{cite news
 |last=Hall 
 |first=Dave 
 |date=2013-06-10 
 |title=Windsor customs firm changes name to Farrow (with video) 
 |url=http://blogs.windsorstar.com/business/windsor-customs-broker-changes-name-to-farrow 
 |newspaper=The Windsor Star 
 |location=Windsor, Ontario 
 |publisher=Windsor Star 
 |accessdate=2014-08-08 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20141030190337/http://blogs.windsorstar.com/business/windsor-customs-broker-changes-name-to-farrow 
 |archivedate=2014-10-30 
 |df= 
}}</ref> is the largest independently owned customs brokerage in [[Canada]].<ref name="3 Windsor Companies">{{cite news|last=Hall |first=Dave |date=2013-02-07 |title=Three local companies among Canada’s best managed |url=http://blogs.windsorstar.com/business/three-local-companies-among-canadas-best-managed |newspaper=The Windsor Star |location=Windsor, Ontario |publisher=Windsor Star |accessdate=2014-08-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20141030190330/http://blogs.windsorstar.com/business/three-local-companies-among-canadas-best-managed |archivedate=2014-10-30 |df= }}</ref> Established in 1911, Farrow now has over 34 offices and warehousing locations throughout Canada and the US.<ref name="Staples Deal">{{cite news|last=Wire |first=Live |date=2014-06-10 |title=Farrow Logistics inks deal with Staples |url=http://blogs.windsorstar.com/business/farrow-logistics-inks-deal-with-staples |newspaper=The Windsor Star |location=Windsor, Ontario |publisher=Windsor Star |accessdate=2014-08-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20141030180308/http://blogs.windsorstar.com/business/farrow-logistics-inks-deal-with-staples |archivedate=2014-10-30 |df= }}</ref> The firm is a fully integrated customs brokerage and logistics provider specializing in Canadian and US customs clearance, international freight forwarding, small parcel handling, express services and international trade consulting.<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Company Profile of Russell A. Farrow Limited |url=http://www.ic.gc.ca/app/ccc/srch/nvgt.do?lang=eng&prtl=1&sbPrtl=&estblmntNo=123456024167&profile=cmpltPrfl&profileId=1921&app=sold |newspaper=Industry Canada |publisher=Government of Canada |agency=Company Profile |date=2013-06-14 |accessdate=2014-08-08}}</ref>

==Foundation, Growth and Acquisitions==
R.A. Farrow Custom Brokers, founded in 1911 by Russell Alexander Farrow, transported goods on ferry boats between Canada and the USA. Russell A. Farrow’s father, Robinson R. Farrow, was the Commissioner of Canadian Customs.<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Officials and Work of the Department of Customs |url=https://www.newspapers.com/image/44311270/ |newspaper=The Evening Journal |publisher= The Ottawa Journal |date= 1907-07-06 |accessdate=2014-08-08}}</ref>

Russell A. Farrow participated in the founding of the Dominion Chartered Custom House Brokers Association (DCCHBA),<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Customs Brokers Meet Here |url=https://www.newspapers.com/image/39339111/ |newspaper=The Winnipeg Tribune |publisher= The Winnipeg Tribune |date= 1946-05-07 |accessdate=2014-08-08}}</ref> also known as the predecessor organization to the Canadian Society of Customs Brokers.<ref>{{cite news|author= |title=In Memoriam |url=http://216.254.168.99/node/4655 |newspaper=Canadian Society of Customs Brokers |publisher=CSCB |date=2002-11-01 |accessdate=2014-08-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20141012101553/http://216.254.168.99/node/4655 |archivedate=2014-10-12 |df= }}</ref>

After Russell A. Farrow's death in 1949, the company was passed down to his wife, Alice M. Farrow, and then his two sons, Robinson(Bob) R. Farrow and Huntley J. Farrow. Today the company is owned and operated by Richard J. Farrow, who is the son of Huntley J. Farrow. Richard's brother, John Farrow, is also actively involved in the company, serving as Vice Chairman.<ref name="Farrow Rebrand"/>

Since Farrow’s establishment in 1911, it has made 18 business acquisitions,<ref name="aquisitions">{{cite news |last=Hall |first=Dave |date=2013-02-13 |title= Farrow Group buys Kitchener company |url=http://www2.canada.com/windsorstar/columnists/story.html?id=a5714038-3323-4780-a316-bf9d8e1fc5c0 |newspaper=The Windsor Star |location= Windsor, Ontario |publisher=Windsor Star |accessdate=2014-08-08 }}</ref> including the recent acquisitions of D&H International,<ref name="Farrow Expands">{{cite news|last=Van Wageningen |first=Ellen |date=2012-05-12 |title=Windsor's Farrow Group Expands in U.S. |url=http://blogs.windsorstar.com/business/windsors-farrow-group-expands-in-u-s |newspaper=The Windsor Star |location=Windsor, Ontario |publisher=Windsor Star |accessdate=2014-08-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20141030170121/http://blogs.windsorstar.com/business/windsors-farrow-group-expands-in-u-s |archivedate=2014-10-30 |df= }}</ref> All Freight International,<ref name="Farrow Expands"/> W.Pickett and Sons,<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=W.Pickett and Sons Company Overview |url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=228324262 |newspaper=Bloomberg Businessweek |publisher= Bloomberg |date= 2013-04-02 |accessdate=2014-08-08}}</ref> Cavalier <ref>{{cite news|author= |title=Farrow Logistics acquires Cavalier |url=http://blogs.windsorstar.com/business/farrow-logistics-acquires-cavalier |newspaper=The Windsor Star |publisher=LiveWire |date=2014-08-20 |accessdate=2014-09-08 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> and CK Logistics.<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Customs broker purchases U.S.-based CK Logistics |url=http://www.todaystrucking.com/customs-broker-purchases-us-based-ck-logistics |publisher=Today's Trucking |date= 2015-01-06 |accessdate=2015-01-12}}</ref>

==Services==
Farrow provides customs brokerage, logistics, and international trade services for importers and exporters in the United States and Canada.<ref name="Company Overview">{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Company Overview of Russell A. Farrow Limited |url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=4400565 |newspaper=Bloomberg Businessweek |publisher=Bloomberg |agency=Company Overview |accessdate=2014-08-08}}</ref> It offers services such as: customs clearance, entry processing, account management, cross-country coverage, warehousing and distribution, customs consulting, duty recovery programs, compliance verification, and dispute resolution.

The company also provides information management [[solution]]s, such as their TradeMaster self-clearance software, which enables the user to link up electronically with the central database to keep track of import/export activities; and TradeSmart plus, an information management solution that provides account management, database access, reporting, and auditing capabilities.<ref name="Company Overview"/>

In addition, it offers door-to-door shipping/[[Less than truckload shipping|LTL]], airfreight re-forwarding and delivery, and cross-border courier and warehousing services; and business-to-consumer and business-to-business merchandising services. Further, the company provides forwarding and logistics services, such as inbound/outbound transportation, warehousing and distribution, and project management services, as well as air, surface, and ocean transportation services. It serves its solutions for catalogers and direct marketers.<ref name="Company Overview"/>

==References==
{{reflist|30em}}

==See also==
{{Col-begin}}
{{Col-2}}
* [[Customs broking|Customs brokerage]]
* [[Freight forwarder|International freight forwarding]]
* [[Trade facilitation]]
* [[Logistics]]
{{Col-2}}
* [[Third-party logistics]]
* [[Incoterms]]
* [[International Trade|international trade]]
{{Col-end}}

[[Category:Logistics companies of Canada]]
[[Category:Logistics companies of the United States]]
[[Category:Multinational companies headquartered in Canada]]
[[Category:Multinational companies headquartered in the United States]]
[[Category:Privately held companies of Canada]]
[[Category:International trade]]
[[Category:Companies based in Windsor, Ontario]]
[[Category:Companies established in 1911]]