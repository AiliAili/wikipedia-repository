{{Infobox NRHP
  | name = Canterbury Castle
  | nrhp_type =
  | image = Canterbury Castle exterior 2.jpg
  | caption = The house's exterior in 2009, prior to demolition
  | location= 2910 Southwest Canterbury Lane<br />[[Portland, Oregon]]
  | coordinates = {{coord|45|31|18|N|122|42|32|W|display=inline,title}}
| locmapin = Portland downtown
  | built = 1929–1931
  | demolished = 2009
  | architect = Jeter O. Frye
  | architecture = Castellated style
  | added = September&nbsp;8, 1987
  | delisted = October&nbsp;13, 2010
  | area = less than one acre
  | governing_body = Private
  | refnum = 87001509<ref name="nris">{{NRISref|version=2009a}}</ref>
}}
'''Canterbury Castle''', also known as '''Arlington Castle''',<ref name=NRHP>{{cite web|url={{NRHP url|id=87001509}}|title=National Register of Historic Places Inventory – Nomination Form|accessdate=February 21, 2012|publisher=United States Department of the Interior|year=1987}}</ref> was a private house located in southwest [[Portland, Oregon]] and listed on the [[National Register of Historic Places]]. Constructed during 1929–1931, the house was designed by Jeter O. Frye to resemble England's [[Canterbury Castle]] on the exterior and to evoke the [[Art Deco]] styling of [[Hollywood]] of the 1920s on the interior. The house included castle features such as a [[moat]], [[drawbridge]] and [[turret]] and attracted paying tourists immediately following its completion.

Canterbury Castle, Portland's only castle structure built in the 1930s, was added to the National Register of Historic Places in 1987. The property was also designated as a Portland Historic Landmark. The house underwent major renovation efforts in the 2000s, but those efforts were not completed, and the house was demolished in 2009 after failing to meet municipal safety codes. The razing of Canterbury made [[Charles Piggott House|Piggott's Castle]] the city's only remaining castle. Canterbury Castle was removed from the National Register of Historic Places in October 2010. 

==Description==
Canterbury Castle was a private {{convert|6000|ft2|m2|adj=on}}, three-story house located in [[Arlington Heights, Portland, Oregon|Arlington Heights]] near [[Washington Park (Portland, Oregon)|Washington Park]], offering views of [[downtown Portland]]. Constructed from [[basalt]] stone quarried from [[Rocky Butte]],<ref name=Lednicer>{{cite news|title=Portland's Canterbury (Lane) Castle is coming down|first=Lisa Grace|last=Lednicer|url=http://www.oregonlive.com/news/index.ssf/2009/05/portlands_canterbury_lane_cast.html|work=[[The Oregonian]]|location=Portland, Oregon|date=May 28, 2009|accessdate=October 22, 2010|issn=8750-1317|publisher=[[Advance Publications]]}}</ref><ref name=NPS>{{cite web|url={{NRHP url|id=87001509|photos=y}}|format=PDF|accessdate=February 21, 2012|publisher=[[National Park Service]]|title=Images: Canterbury Castle}} Note: Photographs taken by Dale Bernards in 1987.</ref> the house featured characteristics of a [[castle]] such as a [[moat]], a [[drawbridge]] and a copper-topped [[turret]].<ref name=Lednicer/><ref name=Bartels>{{cite news|url=http://www.portlandtribune.com/news/story.php?story_id=123680929349526500|title=City is wary of the Castle on the Hill|date=March 12, 2009|accessdate=February 21, 2012|pages=1–2|location=Portland, Oregon|work=[[Portland Tribune]]|publisher=[[Pamplin Media Group]]|first=Eric|last=Bartels}}</ref> The wooden drawbridge covered a moat seven feet wide by more than three feet deep. A single-car garage, built into the hillside, was constructed during the same time as the house. The concrete foundation supported a rectangular ground plan. Canterbury featured "buttressing wing walls, cylindrical corner bays and a crenellated parapet of uncut stone".<ref name=NRHP/> Round, arched and straight-topped frameless windows were often fitted with steel sashes. Doorways were also frameless, narrow and round or parabolic in shape. Doors had [[wrought iron]] hinges, handles and lock plates. The leaded glass slit window at the stair tower's top level featured a spiderweb pattern, a signature element of castle designer Jeter O. Frye's work.<ref name=NRHP/> [[Trachycarpus fortunei|Windmill palms]] surrounding the property added to its exotic appearance.<ref name=Bartels/>

The [[Art Deco]]-style interior, meant to evoke a 1920s Hollywood style, featured mahogany woodwork, tile floors and chandeliers. The inside also contained Spanish-style white [[stucco]], spiral [[Stairway|staircases]] and wrought iron features including stair rails, built-in cabinetry door and window hardware and lighting fixtures.<ref name=NRHP/><ref name=Bartels/> The house included a basement pool (which eventually closed due to water leaks), a stone fireplace {{convert|10|ft|m}} in width, and small square windows.<ref name=Lednicer/> Also in the basement were the furnace, a storage room, and spaces for laundry and billiards. The main floor housed the garage, a kitchen and breakfast bay, the living and dining rooms and a music room. The master bedroom was adjacent to a deck above the dining and music rooms at the southwest corner of the property. Fireplaces and hallway floors featured brown ceramic tile; black glazed ceramic tile was used in the master bathroom, while black, red and yellow tiles were used in the breakfast bay.<ref name=NRHP/> One of the castle's bathrooms was said to be modeled on [[Charlie Chaplin]]'s.<ref name=Lednicer/> The interior was arranged around a central stair tower capped with a conical roof clad in copper. Chimneys vented the centrally located furnace and rose from fireplaces in the house's west hall.<ref name=NRHP/>

==History==
[[File:Canterbury Castle exterior 1.jpg|thumb|left|upright|The house's exterior, 2009]]

Canterbury Castle was designed by Jeter O. Frye and constructed between 1929 and 1931.<ref name=Lednicer/><ref name=Bartels/> Immediately following construction, Frye was unable to sell the house and left Portland in bankruptcy to resume his career in California.<ref name=NRHP/><ref name=Lednicer/> Visitors immediately showed interest in touring the house. The castle attracted paying tourists and school groups, became a stop on the Washington Park bus tour and hosted Halloween parties for a local radio station.<ref name=Lednicer/> The house became known as Canterbury Castle over time due to the plat and name of the street on which it was situated.<ref name=NRHP/> The terraced garden was eliminated in the 1950s when a portion of the land was sold.<ref name=NRHP/> The property was added to the [[National Register of Historic Places]] in 1987 under the ownership of Dale and Karen Bernards, who had submitted an application for listing one year prior.<ref name=NRHP/><ref name=Lednicer/>

[[Image:Canterbury Castle demolition 3.jpg|thumb|right|upright|Canterbury Castle during demolition in 2009]]
Sidney Lynne and John Hefferin purchased the house for $469,900 in 2004 after it had been unoccupied for more than a year. By then, the structure had developed leaks and displayed cracks along the foundation, walls and ceiling. In 2006, a falling tree caused stones to break away, damaging the house's main gas line.<ref name=Lednicer/><ref name=Bartels/> The couple spent $200,000 over four years to reinforce the walls. Lynne and Hefferin also constructed exterior stairs and an underground retaining wall. However, the renovation process was not completed, as they were unable to pay for all repairs required by the city, which had issued $20,000 in fines on the property. The couple also struggled to pay for costly landslide prevention efforts, rising property taxes and high heating bills.<ref name=Bartels/> Canterbury Castle was listed for sale in the summer of 2008 for $2&nbsp;million. Lynne invited local interior design students and contractors to upgrade the house's interior, without success.<ref name=Lednicer/> The couple left the house in January 2009, and ownership was turned over to [[JPMorgan Chase]].<ref name=Lednicer/><ref name=Bartels/> Lynne began a "one-woman crusade" to save the castle, seeking assistance from Portland City Commissioner [[Randy Leonard]] and from [[home improvement]] expert and ''[[This Old House]]'' host [[Bob Vila]].<ref name=Bartels/> In February 2009, the city's bureau of development services found that unstable soil and the risk of stones separating from sheeting underneath posed a landslide risk. The property had missing stones and thick cracks several feet long, which presented hazards to residents and the street below. Rumors of demolition began to surface in March 2009.<ref name=Bartels/> 

[[File:Canterbury Castle demolition 2.jpg|thumb|left|The house being demolished, 2009]]

In April 2009, the house was purchased by Robert Stansel, who had lived next door to the house for several years, at a foreclosure auction for $280,000. Demolition began in May 2009; the house was considered "structurally unsound" by the local government, a determination that permitted demolition despite the house's listing on the National Register of Historic Places.<ref name=Lednicer/> Lynne said of the house and its demolition: "It's totally like a death and completely unnecessary. It's a magical property that invokes hope and imagination in people. We had a really great time with it."<ref name=Lednicer/> Residents shared mixed feelings about the demolition.<ref name=Lednicer/><ref name=Bartels/> Stansel was able to salvage 21 tiles with neo-Aztec designs. In 2009, ''[[The Oregonian]]'' reported that Stansel would wait until the economy and housing market improved before deciding whether to sell the land or build a new structure.<ref name=Lednicer/> 

Canterbury Castle was removed from the National Register of Historic Places on October&nbsp;13, 2010.<ref name="WkList20101022">{{cite web|publisher=[[National Park Service]]|title=Weekly List of Actions Taken on Properties: 10/12/10 through 10/15/10|date=October 22, 2010|url=http://www.nps.gov/history/nr/listings/20101022.htm|accessdate=February 21, 2012}}</ref> The demolition of Canterbury Castle made [[Charles Piggott House|Piggott's Castle]] in southwest Portland the last remaining castle in the city.<ref name=Lednicer/> The property had also been listed as a Portland Historic Landmark and was the city's only castle structure constructed in the 1930s.<ref name=NRHP/><ref>{{Cite web|url=http://www.portlandonline.com/bps/index.cfm?a=146276&c=44013|title=Historic Landmarks – Portland, Oregon: July 2010|date=July 2010|accessdate=February 21, 2012|format=[[Microsoft Excel |XLS]]|publisher=City of Portland}}</ref><ref>{{cite web|url=http://www.portlandonline.com/portlandplan/index.cfm?a=270877&c=51427|title=Portland Plan: Historic Resources, Report 2: Data and Maps, Portland Plan Background Report|date=Fall 2009|accessdate=February 22, 2012|publisher=City of Portland Bureau of Planning and Sustainability|format=PDF|page=37}}</ref> Two stones from Canterbury Castle serve as steps in the [[Japanese rock garden|zen garden]] of Stansel's home in [[East Hampton, New York]].<ref>{{cite news|url=http://www.oregonlive.com/hg/index.ssf/2011/07/interior_design_news_notes_por_2.html|title=Interior design news & notes: Portland's Canterbury Castle lives on; energize your home for free|first=Bridget A.|last=Otto|work=The Oregonian|date=July 21, 2011|accessdate=February 21, 2012|issn=8750-1317|publisher=Advance Publications|location=Portland, Oregon}}</ref><ref>{{cite web|url=https://www.wsj.com/articles/SB10001424052702304186404576387570180704488|title=Luxury Real Estate: Tour the Arc House in East Hampton, N.Y.|first=Ellen|last=Gamerman|date=June 17, 2011|work=[[The Wall Street Journal]]|accessdate=November 23, 2015}}</ref>

==See also==
{{Portal|National Register of Historic Places|Oregon}}
* [[Architecture of Portland, Oregon]]
* [[List of castles in the United States]]
* [[National Register of Historic Places listings in Southwest Portland, Oregon]]
{{Clear}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category}}
* [https://archive.is/20130415202450/http://oregondigital.org/cdm4/results.php?CISOOP1=exact&CISOFIELD1=CISOSEARCHALL&CISOROOT=/archpnw&CISOBOX1=Canterbury+Castle+(Portland,+Oregon) Images] from the [[University of Oregon]] Digital Collections

{{National Register of Historic Places Oregon}}
{{good article}}

[[Category:1930s architecture in the United States]]
[[Category:1931 establishments in Oregon]]
[[Category:2009 disestablishments in Oregon]]
[[Category:Art Deco architecture in Oregon]]
[[Category:Buildings and structures demolished in 2009]]
[[Category:Castles in the United States]]
[[Category:Demolished buildings and structures in Oregon]]
[[Category:Former National Register of Historic Places in Oregon]]
[[Category:Houses completed in 1931]]
[[Category:Houses in Portland, Oregon]]
[[Category:Portland Historic Landmarks]]
[[Category:Southwest Portland, Oregon]]