{{Use dmy dates|date=June 2013}}
{{Use British English|date=June 2013}}
{{Infobox Military Structure
|partof=[[Cheshire]]
|location=[[Beeston, Cheshire|Beeston]], England
|map_type=Cheshire
|coordinates = {{coord|53.1277|-2.6913|type:landmark|display=inline}}
|image=[[File:Beeston Castle 2016 017.jpg|350px]]
|caption=A modern bridge provides access to the gateway of the [[inner ward]].
{{Collapsed infobox section begin|Location map and quick summary}}
|name=Beeston Castle
|type=Enclosure castle
|code=
|built=c. 1220
|builder=[[Ranulf de Blondeville, 6th Earl of Chester|Ranulf de Blondeville]]
|materials=[[Sandstone]]
|height={{convert|9|m}}
|used=Open to public
|demolished=1646
|condition=Ruin
|ownership=[[English Heritage]]
|controlledby=
|garrison= 
|commanders= 
|occupants= 
|battles= 
|events= [[Welsh Wars]]<br />[[English Civil War]]
{{Collapsed infobox section end}}
}}
'''Beeston Castle''' is a former [[Castle|Royal castle]] in [[Beeston, Cheshire|Beeston]], [[Cheshire]], England ({{gbmapping|SJ537593}}), perched on a rocky [[sandstone]] crag {{nowrap|{{convert|350|ft|m|0|sigfig=2}}}}{{sfnp|Fry|1980|p=186}} above the [[Cheshire Plain]]. It was built in the 1220s by [[Ranulf de Blondeville, 6th Earl of Chester]], (1170–1232), on his return from the [[Crusades]]. In 1237, [[Henry III of England|Henry III]] took over the ownership of Beeston, and it was kept in good repair until the 16th century, when it was considered to be of no further military use, although it was pressed into service again in 1643, during the [[English Civil War]]. The castle was slighted (partly demolished) in 1646, in accordance with Cromwell's destruction order, to prevent its further use as a stronghold.  

The castle is now in ruins.  The walls of the [[outer bailey]], along with the walls and gatehouse of the [[inner bailey]], are separately recorded in the [[National Heritage List for England]] as designated Grade&nbsp;I [[Listed building#England and Wales|listed buildings]].<ref>{{National Heritage List for England |num=1130513 |desc=Walls of the outer bailey at Beeston Castle |accessdate=1 December 2012 |mode=cs2}}</ref><ref name="EnglishHeritage">{{National Heritage List for England |num=1330329 |desc=Walls, towers and gatehouse of the inner bailey at Beeston Castle |accessdate=1 December 2012 |mode=cs2 |fewer-links=x}}</ref>  The castle is also a [[Scheduled Monument|Scheduled Ancient Monument]],<ref>{{National Heritage List for England |num=1007900 |desc=Beeston Castle; medieval enclosure castle and site of late prehistoric hillfort |accessdate=1 December 2012 |mode=cs2 |fewer-links=x}}</ref> owned by [[English Heritage]].<ref name=eh/> It is rumoured that treasure belonging to [[Richard II of England|Richard II]] lies undiscovered in the castle grounds, but the many searches that have been carried out have failed to find any trace of it. During the 18th century the site was used as a quarry.

== Prehistory ==
[[File:Beeston Castle 2016 080.jpg|thumb|right|A model showing Beeston's Iron Age settlement]]
Beeston crag is one of a chain of rocky hills stretching across the [[Cheshire Plain]]. Pits dating from the 4th&nbsp;millennium BC indicate the site of Beeston Castle may have been inhabited or used as a communal gathering place during the [[Neolithic]] period.{{sfnp|Brennand|Hodgson|2006|p=32|ps=}} Archaeologists have discovered Neolithic flint arrow heads on the crag, as well as the remains of a [[Bronze Age]] community, and of an [[Iron Age]] [[hill fort]].<ref name=eh>{{National Heritage List for England |num=13495 |desc=Beeston Castle |accessdate=19 February 2008 |mode=cs2 |fewer-links=x}}</ref> The rampart associated with the Bronze Age activity on the crag has been dated to around 1270–830 BC; seven circular buildings were identified as being either late Bronze Age or early Iron Age in origin. It may have been a specialist metalworking site.{{sfnp|Brennand|Hodgson|2006|p=37|ps=}}

== Design ==
Beeston was built by [[Ranulf de Blondeville, 6th Earl of Chester]], as an impregnable stronghold and a symbol of power. The siting of the castle's outer bailey walls was chosen to take advantage of the fortifications remaining from the earlier Iron Age rampart.<ref name="Hickey">{{citation |title=Beeston: Castle of the Rock |last=Hickey |first=Julia |year=2005 |publisher=TimeTravel-Britain.com |url=http://www.timetravel-britain.com/05/April/beeston.shtml |accessdate=17 March 2008}}</ref> In medieval documents the castle is described as ''Castellum de Rupe'', the Castle on the Rock. It is one of three major castles built by Ranulph in the 1220s, shortly after his return from the [[Fifth Crusade]]. The others are [[Bolingbroke Castle|Bolingbroke]] in Lincolnshire, and [[Chartley Castle|Chartley]], Staffordshire, both of which share similar architectural features with Beeston; in particular the design of the towers.{{sfnp|Fry|1980|p=191|ps=}}{{sfnp|Hough|1978|p=2|ps=}}<ref name=Thompson>{{cite journal |last=Thompson |first1=M. W. |year=1966 |title=The origins of Bolingbroke Castle Lincolnshire |journal=[[Medieval Archaeology (journal)|Medieval Archaeology]] |volume=10 |pages=152–8 |mode=cs2 }}</ref>

[[File:Beeston Castle by Buck Bros.jpg|thumb|left|Engraving of 1727 by the [[Samuel and Nathaniel Buck|Buck Brothers]], showing Beeston Castle from the south{{sfnp|Ormerod|1882|ps=}}]]
Unlike many other castles of the period, Beeston does not have a [[keep]] as its last line of defence. Instead the natural features of the land together with massive walls, strong gatehouses, and carefully positioned towers made the baileys themselves the stronghold. The defences consisted of two parts. Firstly, a [[inner ward|rectangular castle]] on the summit of the hill, with a sheer drop on three sides and a defensive ditch up to {{convert|30|ft|m|0}} deep in places cut into the rock on the fourth side. Secondly, an [[outer ward|outer bailey]] was built on the lower slopes, with a massive gatehouse protected by a {{convert|16|ft|m|0}} wide and {{convert|10|ft|m|0}} deep ditch.<ref name="BeestonTeachersInfo">{{citation |title=Beeston Castle: Information for Teachers |publisher=English Heritage |url= |format= |accessdate=}}</ref>

The outer bailey was roughly rectangular, with {{convert|6|ft|m|0}} thick walls faced in sandstone and infilled with rubble. The walls, parts of which still remain, contain a number of D-shaped towers, an innovation in English castles at that time. The towers allowed defenders to shoot across the walls as well as forwards, and their open-backed design meant that they would not offer cover to any attackers who gained access to the outer bailey. The inner bailey was situated on the rocky summit at the western end of the crag.<ref name="BeestonTeachersInfo" />

To provide the castle's inhabitants with a supply of fresh water two wells were dug into the rock, one of them, at {{convert|370|ft|m|0}} deep,{{sfnp|Fry|1980|p=186}} one of the deepest castle wells in England.<ref name="Hickey" />

== Royal castle ==
[[File:Road to Beeston Castle.jpg|thumb|right|Beeston Castle is built on a rocky summit 350&nbsp;feet (110&nbsp;m) above the Cheshire Plain.]]
[[File:Beeston2.jpg|thumb|right|The castle viewed from the east, showing (from top to bottom), the inner bailey, the [[Curtain wall (fortification)|curtain wall]], and the 19th-century gatehouse]]
Although most of the defences were in place by the time of Ranulph's death in 1232, there were no living quarters, and neither were there on the death of Ranulph's successor [[John of Scotland, Earl of Huntingdon|John]] in 1237. John died without a male heir, allowing [[Henry III of England|King Henry III]] to take over the Earldom of Cheshire. Henry enlarged Beeston Castle during his wars with [[Wales]], and used it as a prison for his Welsh captives.{{sfnp|Fry|1980|p=186}} No attempt was made to equip the castle as a permanent residence with halls and chambers; garrisons were probably housed in wooden structures within the outer bailey.<ref name="BeestonTeachersInfo" />

In 1254 Henry gave Beeston, together with other lands in Cheshire, to his son [[Edward I of England|Prince Edward]]. He also gave the title [[Earl of Chester]] to the prince, a title that has been conferred on the heir to the throne of England ever since. Edward was crowned king of England in 1272, and completed the conquest of Wales.

In the middle of the 14th century there are references to men of Cheshire who were made constables of the royal castle.{{efn|Entries for 14th-century constables include:<br />1338–9, Richard son of Robert de Praers, late sheriff of Chester, and constable of the castle of Beeston.<ref>Welsh Records: Recognizance Rolls of Chester.</ref><br /> 28 November 1359. Edward earl of Chester (Edward, the Black Prince) grants to Robert de Halghton the office of constable of Beeston, and receiver of the issues of the lands and tenements of John de St. Pierre, durante bene placito. Salary £4. a year.<br /> 10 November 1361. Edward earl of Chester (Edward, the Black Prince) grants similar to John de Brundelegh (Brindley) – Offices: Constable of Beeston castle and receiver of St Pierre lands. Wages: £4 a year and turf from Peckforton moss.
Orders: To reside in the castle.<ref>Ches. Recog. Rolls, 67 and George Ormerod's History of Cheshire Vol, II.</ref>}} The constable would probably have lived in or near the gatehouse. The habitation was described in an account of the castle in 1593 by Samson Erdeswick, which describes, "a goodly strong gatehouse, and strong wall with other buildings, which when they flourished were a convenient habitation for any great personage."

Beeston was kept in good repair and improved during Edward's reign, and throughout the 14th century. However, by the 16th century, the castle was considered to be of no further use to the English Crown, and in 1602 it was sold to Sir [[Hugh Beeston]] (c. 1547–1626) of [[Beeston Hall]].<ref name="BeestonTeachersInfo" />

There have been persistent rumours of a treasure hidden by [[Richard II of England|Richard II]] somewhere in the castle grounds. Richard is supposed to have hidden part of his personal wealth at Beeston on his journey to Chester in 1399, before boarding a ship to [[Ireland]] to suppress a rebellion there. On his return, Richard was deposed by Henry, Duke of Lancaster, the future [[Henry IV of England|Henry IV]], and his treasure is said to have remained undiscovered. Many searches have been carried out, most of them focusing on the deep well in the inner bailey, but nothing has ever been found. The rumour of hidden treasure may not be well-founded, as Henry IV is recorded as having recovered Richard's gold and jewellery from its various hiding places.<ref name="Hickey" />

== Civil war ==
During the [[English Civil War]] many neglected castles were pressed into service. Beeston was seized on 20&nbsp;February 1643 by [[Roundheads|Parliamentary]] forces commanded by [[Sir William Brereton, 1st Baronet|Sir 
William Brereton]]. The walls were repaired and the motte was cleaned out. During 1643 part of the royal army of Ireland landed at [[Chester]]. On 13 December 1643 Captain Thomas Sandford and eight soldiers from that army crept into Beeston at night (possibly aided by treachery) and surprised the castle governor, Captain Thomas Steele, who was so shaken by the event that he surrendered on the promise that he would be allowed to march out of the castle with honours. Steele was tried and shot for his failure to hold the castle.{{sfnp|Dore|1966|p=33|ps=}}

The [[Cavalier|Royalists]] survived a [[siege]] by parliamentary forces from November 1644 until November 1645, when their lack of food forced them to surrender. The castle was partially demolished in 1646, to prevent its further use as a stronghold.<ref name="EnglishHeritage" />

== Later history ==
[[File:Beeston Castle (5292).jpg|left|thumb|The 18th-century gatehouse]]
Quarrying was carried out in the castle grounds during the 18th century, and the gatehouse leading into the outer bailey was demolished to build a track for the stones to be removed from the site.<ref name="Hickey" /> In 1840 the castle was purchased by [[John Tollemache, 1st Baron Tollemache]], at that time the largest landowner in Cheshire, as part of a larger estate.<ref>{{cite web |url=http://www.peckfortonhills.co.uk/public/control.php?_path=/137/165/171 |title=Peckforton Castle |accessdate=18 March 2008 |publisher=Peckforton Hills Local Heritage |archiveurl=https://web.archive.org/web/20060718082640/http://www.peckfortonhills.co.uk/public/control.php?_path=/137/165/171 |archivedate=18 July 2006 |deadurl=yes |mode=cs2}}</ref> In the mid-19th century the castle was the site of an annual two-day fete, raising money for local widows and orphans and attracting more than 3000 visitors a day.<ref name="BeestonTeachersInfo" />

== Present day ==
The castle is owned by English Heritage, and although in ruins, enough of the walls and towers are still in place to provide a clear picture of how it would have looked in its prime. It is open to visitors and has a small museum and visitor's centre. A lodge house was built by Tollemache in the 19th&nbsp;century, and was expanded in the 20th&nbsp;century. The lodge is two storeys high, with two circular towers either side of a central archway.  It is designated as a Grade&nbsp;II listed building.<ref name="Lodge">{{National Heritage List for England |num=1130512 |desc=The Lodge at Beeston Castle |accessdate=1 December 2012 |mode=cs2 |fewer-links=x}}</ref>

Beeston offers one of the most spectacular views of any castle in England, stretching across eight counties from the [[Pennines]] in the east to the Welsh mountains in the west.<ref name=eh/>

== See also ==
{{portal|Cheshire}}
* [[Peckforton Castle]]
* [[Grade I listed buildings in Cheshire]]
* [[Listed buildings in Beeston, Cheshire]]
* [[List of castles in Cheshire]]
* [[Scheduled Monuments in Cheshire (1066–1539)]]

== References ==
'''Notes'''
{{notelist|notes=}}

'''Citations'''
{{reflist|30em}}

'''Bibliography'''
{{refbegin}}
*{{citation |last=Brennand |first=Mark |last2=Hodgson |first2=John |year=2006 |title=The Prehistoric Period Resource Assessment|journal=Archaeology North West|volume=8 |pages=23–58 |issn=0962-4201}}
*{{citation |last=Dore |first=R. N.|year=1966|title=The Civil Wars in Cheshire: (Volume 8 of ''A History of Cheshire'' edited by J. J. Bagley) |publisher=Cheshire Community Council}}
*{{citation |last=Fry |first=Plantagenet Somerset |authorlink=Plantagenet Somerset Fry |title=The David & Charles Book of Castles |publisher=[[David & Charles]] |year=1980 |isbn=0-7153-7976-3}}
*{{citation |first=P. R. |last=Hough |year=1978 |title=Excavations at Beeston Castle, 1975–1977 |journal=Journal of the Chester Archaeological Society |volume=61 |pages=1–24}}
*{{citation |last=Ormerod |first=G. |editor-last=Helsby |editor-first=T. |title=History of the County Palatine and City of Chester |edition=2nd |year=1882}}
{{refend}}

== Further reading ==
*{{citation |last=Ellis |first=P |title=Beeston Castle, Cheshire: Excavations by Laurence Keen & Peter Hough, 1968–85 |year=1993 |publisher=English Heritage |url=http://archaeologydataservice.ac.uk/archives/view/eh_monographs_2014/contents.cfm?mono=1089017 |format=PDF |isbn=978-1-84802-135-8 |ref=none}}
*{{citation |last=Osborne |first=K. |title=Beeston Castle |publisher=English Heritage |year=1995 |isbn=978-1-85074-541-9 |ref=none}}

== External links ==
{{commons category|Beeston Castle}}
*[http://beestoncastle.com Beeston Castle]
*[http://www.gatehouse-gazetteer.info/English%20sites/182.html Sources on Beeston Castle]
*[http://www.cheshirenow.co.uk/beeston_castle.htm Photographs and history of Beeston Castle]
*[http://www.english-heritage.org.uk/daysout/properties/beeston-castle-and-woodland-park/#Left Visitor information from English Heritage]

{{Cheshire}}
{{Good article}}

[[Category:Buildings and structures completed in 1228]]
[[Category:Castles in Cheshire]]
[[Category:English Heritage sites in Cheshire]]
[[Category:Former populated places in Cheshire]]
[[Category:Grade I listed buildings in Cheshire]]
[[Category:Grade I listed castles]]
[[Category:History of Cheshire]]
[[Category:Ruins in Cheshire]]
[[Category:Scheduled Ancient Monuments in Cheshire]]
[[Category:Tourist attractions in Cheshire]]
[[Category:Hill castles]]