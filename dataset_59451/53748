{{refimprove|date=May 2016}}
A '''Statement of changes in equity''' and similarly the '''statement of changes in owner's equity''' for a [[sole trader]], '''statement of changes in partners' equity''' for a [[partnership]], '''statement of changes in Shareholders' equity''' for a [[Company]] or '''statement of changes in Taxpayers' equity'''<ref name=UKWOGA>{{cite web|url=https://www.gov.uk/government/publications/whole-of-government-accounts-2013-to-2014 |format=web|title=Whole of Government Accounts 2013 to 2014 |publisher=[[UK Government]]}}</ref> for [[Government financial statements]] is one of the four basic [[financial statement]]s.

The statement explains the changes in a company's [[Share capital|Share Capital]], [[Reserve (accounting)|accumulated reserves]] and [[retained earnings]] over the reporting period. It breaks down changes in the owners' interest in the organization, and in the application of retained profit or surplus from one [[accounting period]] to the next. Line items typically include profits or losses from operations, [[dividends]] paid, issue or redemption of shares, [[Revaluation of fixed assets|revaluation reserve]] and any other items charged or credited to [[accumulated other comprehensive income]]. In also includes the [[Non-Controlling Interest]] attribuable to other individuals and organisations.

The statement is expected under the [[generally accepted accounting principles]] and explains the [[Equity (finance)|owners' equity]] shown on the [[balance sheet]], where:
<blockquote>owners' equity = assets − liabilities</blockquote>

==Requirements of the U.S. GAAP==
In the United States this is called a ''statement of retained earnings'' and it is required under the U.S. [[Generally Accepted Accounting Principles (United States)|Generally Accepted Accounting Principles]] (U.S. GAAP) whenever comparative balance sheets and income statements are presented. It may appear in the [[balance sheet]], in a combined [[income statement]] and changes in retained earnings statement, or as a separate schedule.

Therefore, the statement of retained earnings uses information from the [[income statement]] and provides information to the [[balance sheet]].

Retained earnings are part of the [[balance sheet]] (another basic financial statement) under "stockholders equity (shareholders' equity)" and is mostly affected by net income earned during a period of time by the company less any dividends paid to the company's owners / stockholders.  The retained earnings account on the balance sheet is said to represent an "accumulation of earnings" since net profits and losses are added/subtracted from the account from period to period.

Retained Earnings are part of the "Statement of Changes in Equity".
The general equation can be expressed as following:

: ''Ending'' Retained Earnings =  ''Beginning'' Retained Earnings − Dividends Paid +  Net Income

This equation is necessary to use to find the Profit Before Tax to use in the Cash Flow Statement under Operating Activities when using the indirect method. This is used whenever a comprehensive income statement is not given but only the balance sheet is given.

==Requirements of IFRS==
IAS 1 requires a business entity to present a ''separate statement of changes in equity'' (SOCE) as one of the components of financial statements.

The statement shall show: (IAS1.106)
# [[total comprehensive income]] for the period, showing separately amounts attributable to owners of the [[parent company|parent]] and to [[non-controlling interest]]s
# the effects of retrospective application, when applicable, for each component
# '''reconciliations''' between the carrying amounts at the ''beginning'' and the ''end'' of the period for each component of [[equity (finance)|equity]], separately disclosing:
::* [[net income|profit or loss]]
::* each item of [[Accumulated other comprehensive income#Other comprehensive income|other comprehensive income]]
::* transactions with owners, showing separately contributions by and distributions to [[owners]] and changes in ownership interests in subsidiaries that ''do not result in a loss of control''

However, the amount of dividends recognised as distributions, and the related amount per share, ''may'' be presented in the [[notes to the financial statements|notes]] instead of presenting in the statement of changes in equity. (IAS1.107)

For [[small and medium enterprises]] (SMEs), the statement of changes in equity should show all changes in equity including:
* total comprehensive income
* owners' investments
* dividends
* owners' withdrawals of capital
* treasury share transactions

They can omit the statement of changes in equity if the entity has no owner investments or withdrawals other than dividends, and elects to present a combined statement of comprehensive income and retained earnings.

==Example statement==
The following statement of changes in equity is a very brief example prepared in accordance with IFRS. It does not show all possible kinds of items, but it shows the most usual ones for a company. Because it shows [[Minority interest|Non-Controlling Interest]], it's a [[Consolidated financial statement|consolidated]] statement.

{| class="wikitable" style="text-align: center;"
! width="100%" colspan="11"| '''Statement of Changes in Equity of XYZ, Ltd.''' <br /> ''' As of 31 December 2015'''
|-
! width="23%" rowspan="2"| 
! width="7%" rowspan="2"| ''[[Share capital|Share Capital]]''
! width="7%" rowspan="2"| ''[[Share Premium]]''
! width="7%" rowspan="2"| ''[[Treasury Share]]s''
! width="7%" rowspan="2"| ''[[Retained Earnings]]''
! width="7%" colspan="3"| ''[[Accumulated Other Comprehensive Income]]''
! width="7%" rowspan="2"| ''Total shareholders funds''
! width="7%" rowspan="2"| ''[[Minority interest|Non-Controlling Interest]]''
! width="7%" rowspan="2"| ''Total''
|-
! width="7%"| ''[[Foreign-exchange reserves]]''
! width="7%"| ''Pensions Reserve''
! width="7%"| ''[[Revaluation of fixed assets|Revaluation Reserve]]''
|-
!At 1 January 2014
! 1,000
! 100
! 0
! 2,500
! 750
! 800
! 56
! 5,206
! 600
! 5,806
|-
| Profit/(Loss) for the year
|
|
|
|300
|
|
|
!300
|30
!330
|-
| Other Comprehensive Income
|
|
|
|
|10
|35
|45
!90
|9
!99
|-
|Dividends to Shareholders @50%
|
|
|
|(150)
|
|
|
!(150)
|(15)
!(165)
|-
|Purchase of own shares
|
|
|(250)
|
|
|
|
!(250)
|
!(250)
|-
!At 1 January 2015
!1,000
!100
!(250)
!2,650
!760
!835
!101
!5,196
!624
!5,820
|-
|Profit/(loss) for the year
|
|
|
|400
|
|
|
!400
|40
!440
|-
|Other Comprehensive Income
|
|
|
|
|40
|30
|(10)
!60
|6
!66
|-
|Dividends to Shareholders @50%
|
|
|
|(200)
|
|
|
!(200)
|(20)
!(220)
|-
|Issue of Shares
|150
|87
|220
|(50)
|
|
|
!407
|
!407
|-
!At 30 December 2015
!1,150
!187
!(30)
!2,800
!800
!865
!91
!5,863
!650
!6,513
|}

==See also==

* [[International Financial Reporting Standards]] (and its [[requirements of IFRS|requirements]])
* [[Income statement]]
* [[Cash flow statement]]
* [[Comprehensive income]]
* [[Accumulated other comprehensive income]]

==References==
<references/>
* {{cite web|url=http://www.ifrs.org/IFRSs/Pages/IFRS.aspx |format=web|title=Access the unaccompanied Standards |publisher=[[IFRS Foundation]])}}

[[Category:Financial statements|Retained earnings]]
[[Category:Accounting terminology]]
[[Category:Financial capital]]