{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Off on a Comet
| title_orig    = Hector Servadac
| translator    = unknown (1877), [[Ellen Elizabeth Frewer|Ellen Frewer]] (1877), [[Edward Roth]] (1877-78), [[Idrisyn Oliver Evans|I. O. Evans]] (1965)
| image         = 'Off on a Comet' by Paul Philippoteaux 001.jpg
| author        = [[Jules Verne]]
| illustrator   = [[Paul Philippoteaux]]
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #15
| genre         = [[Science fiction]], [[adventure novel]] 
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 1877
| english_pub_date = 1877
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| preceded_by   = [[Michael Strogoff]]
| followed_by   = [[The Child of the Cavern]]
}}
'''''Off on a Comet''''' ({{lang-fr|Hector Servadac}}) is an 1877 [[science fiction]] novel by [[Jules Verne]].

==Plot==
[[File:'Off on a Comet' by Paul Philippoteaux 065.jpg|thumb|right|John Herschel observes Comet Halley from his observatory in Cape Town in 1835 (illustration from the book).]]
The story starts with a [[comet]] called Gallia, that touches the [[Earth]] in its flight and collects a few small chunks of it. The disaster occurred on January 1 of the year 188x in the area around [[Gibraltar]]. On the territory that was carried away by the comet there remained a total of thirty-six people of French, English, Spanish and Russian nationality. These people did not realize at first what had happened, and considered the collision an earthquake.

They first noticed weight loss: Captain Servadac's [[Adjutant#France|adjutant]] Ben Zoof to his amazement, jumped twelve meters high. Zoof with Servadac also soon noticed that the alternation of day and night is shortened to six hours, that east and west changed sides, and that water begins to boil at 66 degrees Celsius, from which they rightly deduced that atmosphere became thinner and pressure dropped. At the beginning of their stay in Gallia they noticed the Earth with the Moon, but thought it was an unknown planet. Other important information was obtained through their research expedition with a ship, which the comet also took.

During the voyage they discovered a mountain chain blocking the sea, which they initially considered to be the Mediterranean Sea and then they found the island of [[Formentera]] (before the catastrophe a part of the [[Balearic Islands]]), where they found a French astronomer Palmyrin Rosette, who helped them to solve all the mysterious phenomena. They were all on the comet which was discovered by Rosette a year ago and predicted a collision course with Earth, but no one believed the astronomer, because a layer of thick fog at the time prevented astronomical observations in other places.

As found by a new research expedition, the circumference of Gallia was 2320&nbsp;km. The mass of the comet was calculated by Rosette. He determined it at 209,346 billion tonnes. For the calculation he used spring scales and forty 5-franc silver coins, the weight of which on earth equaled exactly to one kilogram. However, the owner of the scales, Isaac Hakkabut, had rigged the instrument, so the results had to be cut by a quarter.

Involuntary travelers through the Solar system did not have any hope for long-term colonization of their new world, because they were lacking arable land. They ate mainly the animals that were left on the land carried away by Gallia. One strange phenomenon they met was that the sea on the comet did not freeze, even though the temperature dropped below the freezing point (theory that the stationary water level resists freezing level for longer than when a rippled by wind). Once a stone was thrown into the sea, the sea froze in a few moments. The ice was completely smooth and allowed skating and sleigh sailing.

Despite the dire situation in which the castaways found themselves, old power disputes from Earth continued on Gallia, because the French and English officers considered themselves the representatives of their respective governments. The object of their interest was for example previously Spanish [[Ceuta]], which became an island on the comet and which both parties started to consider an unclaimed territory. Captain Servadac therefore attempted to occupy Ceuta, but was not successful. It turned out that the island had been occupied by Englishmen, who maintained a connection to their base at Gibraltar through optical telegraph.

Gallia got to an extreme point of its orbit and then began its return to Earth. In early November Rossete's refined calculations showed that there will be a new collision with the Earth, exactly two years after the first, again on January 1. Therefore, the idea appeared to leave the comet collision in a balloon. The proposal was approved and the castaways made a balloon out of the sails of their ship. In mid-December there was an earthquake, in which Gallia partially fell apart and lost a fragment, which probably killed all Englishmen in Ceuta and Gibraltar. When on January 1 there was again a contact between the atmospheres of Gallia and Earth, the space castaways left in the balloon and landed safely two kilometers from [[Mostaganem]] in Algeria.

==Main characters==
The 36 inhabitants of [[Gallia (disambiguation)|Gallia]] include a German Jew, an Italian, three [[France|Frenchmen]], eight Russians, 10 [[Spain|Spaniards]], and 13 British soldiers.  The main characters are:

*'''[[Captain (land)|Captain]] Hector Servadac''' of the French Algerian army
*'''Laurent Ben Zoof''', Servadac's aide
*'''[[Count]] Wassili Timascheff''' of Russia
*'''[[Lieutenant|Lt.]] Procope''', the commander of Timascheff's yacht, Dobrina
*'''Isaac Hakkabut''', a stereotyped Jewish trader
*'''Nina''', a cheerful young Italian goatherd (girl).
*'''Pablo''', a Spanish boy
*'''[[Colonel]] Heneage Finch Murphy and [[Major]] [[Sir]] John Temple Oliphant''' of Britain's [[Gibraltar]] garrison. In the French original Murphy is actually a Brigadier (General), a rank too high to be the butt of Verne's joking description as playing an interminable game of chess without a pawn being taken. The translator accordingly demoted him to the rank of Colonel, a rank less likely to cause offense.
*'''Palmyrin Rosette''', a French astronomer and discoverer of the comet and previously Servadac's teacher.

==Publication history==
The book was first published in France (Hetzel Edition, 1877).

The English translation by Ellen E. Frewer, was published in England by Sampson Low (November 1877), and the U.S. by Scribner Armstrong<ref>{{cite web|url=http://lccn.loc.gov/62056587|title=Library of Congress catalog record|accessdate=2008-08-31}}</ref> with the title ''Hector Servadac; Or the Career of a Comet''. The Frewer translation alters the text considerably with additions and emendations, paraphrases dialogue, and rearranges material, although the general thread of the story is followed. The translation was made from the ''Magazin'' pre-publication version of the novel described below in ''Antisemitism''.

At the same time George Munro<ref>{{cite web|url=http://lccn.loc.gov/01009819|title=Library of Congress catalog record|accessdate=2008-08-31}}</ref> in New York published an anonymous translation in a newspaper format as #43 of his ''Seaside Library'' books. This is the only literal translation containing all the dialogue and scientific discussions. Unfortunately the translation stops after Part II Chapter 10, and continues with the Frewer translation.

The same year a still different translation by Edward Roth was published in Philadelphia by Claxton, Remsen, and Heffelfinger<ref>{{cite web|url=http://lccn.loc.gov/01009821|title=Library of Congress catalog record|accessdate=2008-08-31}}</ref> in two parts. Part I (October, 1877) was entitled ''To the Sun'' and Part II (May, 1878) ''Off on a Comet''. This was reprinted in 1895 by David McKay.

Occasional reprints of these books were published around 1900 by Norman L. Munro, F.M. Lupton, Street&Smith, Hurst and Co., and Federal Book Co.

In 1911, Vincent Parke and Company<ref>{{cite web|url=https://archive.org/details/worksofjulesvern09vernuoft|title=Works of Jules Verne '''9'''|editor=Charles F. Horne Ph.D.|date=1911|volume=9|publisher=Vincent Parke and Company|location=New York|accessdate=2008-10-04}}</ref> published a shortened version of the Frewer translation, omitting Part II, Chapter 3. Parke used the title ''Off on a Comet'', and since that time the book has usually been referred to with that title instead of the correct one, ''Hector Servadac''.

In 1926 the first two issues of ''Amazing Stories'' carried ''Off on a Comet'' in two parts.

In 1959, [[Classics Illustrated]] released ''Off on a Comet'' as a [[graphic novel]] (issue #149).

In 1960 Dover (New York) re-published the Roth translations, unabridged, as ''Space Novels'' by Jules Verne, including reproductions of the original engravings from the first French editions. In 1965 the I. O. Evans condensation of the Frewer translation was published in two volumes as ''Anomalous Phenomena'' and ''Homeward Bound'' by ARCO, UK and Associated Booksellers, US. University Press of the Pacific, Honolulu, re-published the Frewer translation in 2000. In September, 2007, Solaris Books (U.K.) published ''Off on a Comet'' as an appendix to ''[[Splinter (novel)|Splinter]]'' by [[Adam Roberts (British writer)|Adam Roberts]]; a slightly edited version of the Parke edition.

In a September 11, 2007 blog post on ''[[The Guardian]]'', Adam Roberts reviewed the 1877 translation. Roberts felt that the translation was inaccurate and incomplete.<ref>Roberts, Adam. "[https://www.theguardian.com/books/booksblog/2007/sep/11/julesvernedeservesabetter Jules Verne deserves a better translation service]." ''[[The Guardian]]''. September 11, 2007.</ref> However Roberts' criticism is somewhat vitiated by the fact that the version of ''Hector Servadac'' he was criticizing was the corrupt version of the original Frewer translation found on Project Gutenberg (based on the Parke edition, above) made from a different French original than the one he was using.

In October, 2007, Choptank Press published an on-line version of Munro's 1877 ''Hector Servadac, Travels and Adventures through the Solar System''
<ref>
{{cite web|url=http://www.ibiblio.org/pub/docs/books/sherwood/Servadac02.htm|title=Hector Servadac, Travels and Adventures through the Solar System|date=October 2007|publisher=Choptank Press|accessdate=2008-12-01|editor=Norman Wolcott}}</ref> edited by Norman Wolcott, followed (December, 2007) by ''Hector Servadac: The Missing Ten Chapters from the Munro Translation''<ref>
{{cite web|url=http://www.ibiblio.org/pub/docs/books/sherwood/Comet2-4.htm|others=Translated by Norman Wolcott and Christian Sanchez|publisher=Choptank Press|title=Hector Servadac: The Missing Ten Chapters from the Munro Translation|date=December 2007|accessdate=2008-12-01}}</ref> newly translated by Norman Wolcott and Christian Sánchez. In 2008 the Choptank Press published a combined book version ''Hector Servadac: Travels and Adventures Through the Solar System'' containing (I) An enlarged replica of Seaside Library edition #43 as published by George Munro, New York, 1877; (II)A typeset version of the same in large readable type; (III) A new translation of the last 10 chapters from the original French by Norman Wolcott and Christian Sanchez in the literal style of the remainder of the book; and (IV) 100 illustrations from the original publications enlarged to 8½" × 11" format.<ref>
{{cite book|title=Hector Servadac: Travels and Adventures through the Solar System|others=Norman Wolcott and Christian Sanchez, editors/translators|publisher=Choptank Press|location=St. Michaels, MD|date=2008|page=554}} [http://www.ibiblio.org/pub/docs/books/sherwood/Verne-COLLECTOR-1.htm]</ref>

==Antisemitism controversy==
{{Unreferenced section|date=December 2008}}<!-- this is being worked on: 12/2008 varnesavant-->
From the beginning Verne had problems with this novel.<ref>
Material in this section is described in the book by {{cite book|title=Jules Verne: an Exploratory Biography|author=Herbert R. Lottman|publisher=St. Martin's Press|location=New York|date=1997}}</ref> Originally he intended that Gallia would crash into the earth killing all on board. This may have been the motivation for naming the hero "Servadac" with the mirror of the French word ''cadavres'' ("corpses"), predicting all would die on the "return". His publisher  [[Pierre-Jules Hetzel|Hetzel]] would not accept this however, given the large juvenile readership in his monthly magazine, and Verne was forced to graft a rather unsatisfying ending onto the story, allowing the inhabitants of Gallia to escape the crash in a balloon.

The first appearance in French was in the serial magazine ''[[Magazine d'Éducation et de Récréation]]'', commencing on 1 January 1877 and ending on 15 December 1877. It was in June 1877 when chapter 18 appeared with the introduction and description of Isac Hakhabut:
{{quote|He was a man of fifty years, who looked sixty. Small, weakly, with eyes bright and false, a busked nose, a yellowish beard and unkempt hair, large feet, hands long and hooked, he offered the well-known type of the German Jew, recognizable among all. This was the usurer with supple back-bone, flat-hearted, a clipper of coins and a skin-flint. Silver should attract such a being as the magnet attracts iron, and if this Shylock was allowed to pay himself from his debtor, he would certainly sell the flesh at retail. Besides, although a Jew originally, he made himself a Mahometan in Mahometan provinces, when his profit demanded it, and he would have been a pagan to gain more.}}

This prompted the chief rabbi of Paris, [[Zadoc Kahn]], to write a letter to Hetzel, objecting that this material had no place in a magazine for young people. Hetzel and Verne co-signed a reply indicating they had no intention of offending anyone, and promising to make corrections in the next edition. However Verne left the salvage work to Hetzel, asking Hetzel at the end of the summer, "Have you arranged the affair of the Jews in Servadac?" The principal change was to replace "Jew" with "Isac" throughout, and to add "Christian countries" to those where Hakhabut plied his trade. The anti-semitic tone remained however, sales were lower than for other Verne books, and the American reprint houses saw little profit with only a single printing by George Munro in a newspaper format. Even the Hetzel revised version has never been translated into English, as both Victorian translations were made from the magazine version. This has caused some modern reviewers to unfairly criticize the early translators, assuming that they had inserted the anti-semitic material which Verne actually wrote.

==Film Adaptations==
* ''[[Valley of the Dragons (1961 film)|Valley of the Dragons]]'' a Columbia Picture directed by [[Edward Bernds]]
: {{IMDb title|0055583|Valley of the Dragons}}
* ''[[Na kometě]]'' ("On the Comet"), directed by [[Karel Zeman]], [[Czechoslovakia]], 1970.
: {{IMDb title|066121|Na Komete}}
* ''Off on a Comet'', an animated adaptation, directed by [[Richard Slapczynski]], Australia, 1979.
: {{IMDb title|1806984|Off on a Comet}}

A 1962 version of the novel was to have been filmed by [[American International Pictures]] but the film was never made. Various 1962 issues of [[Charlton Comics]] offered a contest where a winner would visit the set of the film.<ref>Charlton, ''Reptisaurus The Terrible'', Vol. 2, No. 3, Charlton Comics Jan 1962</ref>

==Influences==
''Off on a Comet'' was the inspiration for a 2007 novel by [[Adam Roberts (British writer)|Adam Roberts]], ''[[Splinter (novel)|Splinter]]''.

==References==
{{reflist}}

==External links==
{{commons category}}
*[http://www.ibiblio.org/pub/docs/books/sherwood/Servadac02.htm ''Hector Servadac, Travels and Adventures through the  Solar System''] edited by Norman Wolcott (George Munro 1877 version). Choptank Press, 2007
*[http://www.ibiblio.org/pub/docs/books/sherwood/Comet2-4.htm ''Hector Servadac: The Missing Ten Chapters of the Munro Translation''] by Norman Wolcott and Christian Sánchez. Choptank Press, 2007.
*[http://international.loc.gov/intldl/fiahtml/fia_collections/fia_lists/frlcrbssTitles1.html Hector Servadac], page images of the Munro Translation from the Library of Congress..
*[https://archive.org/details/hectorservadac00vern Hector Servadac], scanned copy of the Frewer translation (Charles Scribner's sons, 1906 printing).
*[http://www.gutenberg.org/etext/1353 Off on a Comet], text of Parke edition from [[Project Gutenberg]].
*[http://www.solarisbooks.com/pdf/off-on-a-comet.pdf ''Off on a Comet''], edited by Adam Roberts, Solaris Books.
*[http://www.gutenberg.org/etext/8984 Off on a Comet], English audio version from [[Project Gutenberg]]. (artificial voice read)
* {{librivox book | title=Off on a Comet | author=Jules Verne}}
*[http://jv.gilead.org.il/zydorczak/ser00.htm ''Hector Servadac''], French text.
*[http://www.film.org.pl/prace/karel_zeman.html Karel Zeman], a Polish site with detailed filmography of Karel Zeman.
*[http://www.lesia.obspm.fr/perso/jacques-crovisier/JV/verne_HS.html ''Hector Servadac and the Comets of Jules Verne''] has the most complete background on the science of the book, by astro-physicist Jacques Crovisier. (in French)

{{Verne}}

{{Authority control}}

{{DEFAULTSORT:Off On A Comet}}
[[Category:1877 novels]]
[[Category:Novels by Jules Verne]]
[[Category:1870s science fiction novels]]
[[Category:French science fiction novels]]
[[Category:Impact event novels]]
[[Category:Space exploration novels]]
[[Category:Fictional comets]]
[[Category:Gibraltar in fiction]]
[[Category:Ceuta]]
[[Category:French novels adapted into films]]