{{Good article}}
{{Use British English|date=November 2014}}
{{Use dmy dates|date=November 2014}}
{{Infobox University Boat Race
| name= 75th Boat Race
| winner = Oxford
| margin = 3/4 length
| winning_time= 20 minutes 54 seconds
| overall = 34–40
| umpire =  [[Frederick I. Pitman]]<br>(Cambridge)
| date= {{Start date|1923|3|24|df=y}}
| prevseason= [[The Boat Race 1922|1922]]
| nextseason= [[The Boat Race 1924|1924]]
}}

The '''75th Boat Race''' took place on 24 March 1923.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Cambridge's crew was marginally heavier than Oxford's, the latter included an Olympic silver medallist.  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1922|previous year's race]].   In this year's race, umpired by former rower [[Frederick I. Pitman]], Oxford won by three-quarters of a length (the narrowest margin of victory since 1913) in a time of 20 minutes 54 seconds, securing their first win in five years.  The victory took the overall record in the event to 40&ndash;34 in their favour.

==Background==
[[File:Harcourt Gilbey Gold, Vanity Fair, 1899-03-23.jpg|right|upright|thumb|[[Harcourt Gilbey Gold]] coached the Oxford crew.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1922|1922 race]] by one length, while Oxford led overall with 39 victories to Cambridge's 34 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 25 August 2014}}</ref><ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>

Oxford were coached by G. C. Bourne who had rowed for the university in the [[The Boat Race 1882|1882]] and [[The Boat Race 1883|1883 races]], [[Harcourt Gilbey Gold]] (Dark Blue president for the [[The Boat Race 1900|1900 race]] and four-time Blue) and E. D. Horsfall (who had rowed in the three races prior to the [[First World War]]).  Cambridge's coaches were Harald Peake (who had participated in the Peace Regattas of 1919), G. L. Thomson and David Alexander Wauchope (who had rowed in the [[The Boat Race 1895|1895 race]]).<ref>Burnell, pp. 110&ndash;111</ref>  For the fifteenth year the umpire was old [[Eton College|Etonian]] [[Frederick I. Pitman]] who rowed for Cambridge in the [[The Boat Race 1884|1884]], [[The Boat Race 1885|1885]] and [[The Boat Race 1886|1886 races]].<ref>Burnell, pp. 49, 108</ref>

According to author and former Oxford rower G. C. Drinkwater, the Oxford [[The Boat Race#Build-up|trial eights]] were "of a better average than those of the preceding years" and after they arrived at [[Putney]], the Dark Blue crew "improved rapidly up to the day of the race".<ref name=drink141>Drinkwater, p. 141</ref>  Conversely he reported that Cambridge suffered "a dearth of good heavy-wrights" and that the crew "were not of very high class".<ref name=drink141/>

==Crews==
The Cambridge crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 8.875&nbsp;[[Pound (mass)|lb]] (80.0&nbsp;kg), {{convert|0.375|lb|kg|1}} per rower more than their opponents.<ref name=burn71>Burnell, p. 71</ref>  Oxford's crew included four rowers with Boat Race experience, including P. C. Mallam and [[Guy Oliver Nickalls]] who were both participating in their third consecutive event.  Nickalls was a silver medallist in the [[Rowing at the 1920 Summer Olympics – Men's eight|men's eight]] at the [[1920 Summer Olympics]].<ref>{{Cite web | url = http://www.sports-reference.com/olympics/countries/GBR/summer/1920/ROW/ | title = Great Britain Rowing at the 1920 Antwerpen Summer Games| publisher = [[Sports Reference]] | accessdate = 29 January 2015}}</ref> Cambridge's crew included three rowers who had represented the university in the previous year's race: K. N. Craig, B. G. Ivory and [[Theodore Collet]].<ref name=burn71/>  Two of the participants in the race were registered as non-British: Cambridge's Kane and Mellen were from the United States.<ref>Burnell, p. 39</ref>
[[File:Irvine.jpg|right|thumb|upright|[[Andrew Irvine (mountaineer)|Andrew Irvine]] rowed for Oxford at number three.]]
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || P. C. Mallam ||  [[The Queen's College, Oxford|Queen's]] || 11 st 12 lb || W. F. Smith || [[Trinity College, Cambridge|1st Trinity]] || 11 st 7.5 lb
|-
| 2 || P. R. Wace ||  [[Brasenose College, Oxford|Brasenose]]  || 12 st 6.5 lb || F. W. Law || [[Lady Margaret Boat Club]] || 12 st 12 lb
|-
| 3 || [[Andrew Irvine (mountaineer)|A. C. Irvine]] ||  [[Merton College, Oxford|Merton]] || 12 st 10.5 lb || K. N. Craig || [[Pembroke College, Cambridge|Pembroke]]  || 13 st 0 lb
|-
| 4 || R. K. Kane  || [[Balliol College, Oxford|Balliol]] || 13 st 9.5 lb || S. H. Heap || [[Jesus College, Cambridge|Jesus]]|| 13 st 7.5 lb
|-
| 5 || G. J. Mower-White||  [[Brasenose College, Oxford|Brasenose]] || 13 st 11.5 lb || B. G. Ivory (P)|| [[Pembroke College, Cambridge|Pembroke]] || 13 st 10 lb
|-
| 6 || J. E. Pedder || [[Worcester College, Oxford|Worcester]] || 13 st 3.5 lb || [[Theodore Collet|T. D. A. Collet]] || [[Pembroke College, Cambridge|Pembroke]]|| 12 st 7 lb
|-
| 7 ||  [[Guy Oliver Nickalls|G. O. Nickalls]] (P) ||  [[Magdalen College, Oxford|Magdalen]] || 12 st 12 lb || [[Robert Morrison (rower)|R. E. Morrison]] || [[Trinity College, Cambridge|3rd Trinity]] || 12 st 1 lb
|-
| [[Stroke (rowing)|Stroke]] || W. P. Mellen  || [[Brasenose College, Oxford|Brasenose]]  || 10 st 12 lb || [[Terence Sanders|T. R. B. Sanders]] || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 12 lb
|-
| [[Coxswain (rowing)|Cox]] || G. D. Clapperton || [[Magdalen College, Oxford|Magdalen]] || 7 st 11 lb || R. A. L. Balfour || [[Trinity College, Cambridge|3rd Trinity]] || 8 st 8 lb
|-
!colspan="7"|Source:<ref name=dodd323>Dodd, p. 323</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}
==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.  Umpire Pitman started the race in calm conditions at 5:10&nbsp;p.m.<ref>Drinkwater, pp. 141&ndash;142</ref>  Apparently disrupted by the wake of a nearby moored [[Steamship|steamer]], Oxford's start was poor, allowing Cambridge to lead by a [[Canvas (rowing)|canvas']] length after a minute.  Despite this, Oxford had drawn level a minute later, to hold a small lead by the time the crews commenced the long bend.  By the Mile Post, the Dark Blues held a quarter-length lead and by [[Hammersmith Bridge]] had extended this to three-quarters of a length.  Spurting at The Doves pub, Oxford began to draw clear of Cambridge.<ref name=drink141/>  

By [[Chiswick Eyot]], Oxford accelerated away from the Light Blues and were two lengths clear before a spurt from Cambridge ahead of [[Barnes Railway Bridge|Barnes Bridge]] reduced the lead to a length and a quarter by the time the crews passed below the bridge.  With the bend in the river in their favour, and pushing hard, Cambridge slowly gained on the Dark Blues but could not level terms.<ref name=drink141/>  Oxford passed the finishing post with a lead of three-quarters of a length in a time of 20 minutes 54 seconds.  It was their first victory in five years, the narrowest winning margin since the [[The Boat Race 1913|1913 race]] and the slowest winning time since the [[The Boat Race 1920|1920 race]].  The win took the overall record in the event to 40&ndash;34 in their favour.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-95-006387-4 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]
* [http://www.gettyimages.co.uk/detail/news-photo/photo-montage-of-the-oxford-university-rowing-crew-p-c-news-photo/3404933 Montage of the Oxford crew from Getty Images]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1923}}
[[Category:1923 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1923 sports events]]