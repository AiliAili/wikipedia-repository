<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=HD.15
 | image=Hanriot_HD.15.tif
 | caption=
}}{{Infobox Aircraft Type
 | type=Two-seat fighter-reconnaissance aircraft
 | national origin=[[France]]
 | manufacturer=Aeroplanes [[Hanriot]] et Cie
 | designer=Emile Dupont
 | first flight=April 1922
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=4
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Hanriot HD.15''' was a [[France|French]] two seat [[fighter aircraft]] fitted with a supercharger for good high altitude performance, built in the 1920s.  Three were ordered by [[Japan]] but lost at sea during delivery.

==Design and development==
The Hanriot HD.15 was designed in response to a government call for a [[turbo-supercharger|turbo-supercharged]] high altitude fighter-reconnaissance aircraft. It was powered by a [[Hispano-Suiza 8F]]b 8-cylinder upright water-cooled [[V-8 engine]] fitted with a {{ill|Auguste Rateau|fr|lt=Rateau}} turbo-supercharger intended to maintain sea level powers to altitudes up to {{convert|5000|m|ft|abbr=on|0}}.<ref name=G&S/>

Structurally the HD.15 was an all-metal aircraft, though the flying surfaces and rear [[fuselage]] were [[aircraft fabric covering|fabric covered]].  The wings had rectangular section Duralumin [[spar (aviation)|box spars]], assisted by tubular auxiliary spars forward and aft of them.<ref name=Flight/> In plan they were straight edged, unswept and of constant [[chord (aircraft)|chord]] and thickness. The lower wing had a slightly greater span.  The [[wing tip]]s were essentially square, except that the [[balanced rudder|horn balances]] of the short span [[ailerons]] on both upper and lower wings projected beyond. There was no [[stagger (aviation)|stagger]]. The HD.15 had unusual [[interplane strut]]s: instead of the familiar division of the wing into [[biplane#Bays|bays]] by struts braced with crossed [[flying wires|flying]] and [[landing wires]], it had a rigid, spanwise, X-shaped strut on each side, linking the upper and lower spars.  Vertical wires maintained the interplane gap and the location of the crossing point, which was below mid-gap. The inboard end of each upper X-strut met the wing at the top of the aft member of a pair of [[cabane strut]]s.  The lower ends of the X-strut  met the wing further outboard, at the bottom of a strut that ran to the upper fuselage [[longeron]].<ref name=G&S/><ref name=Flight/>  The [[empennage]] of the HD.15 was like those used on earlier Emile Dupont designs, with a braced, rectangular [[tailplane]] mounted on top of the fuselage and a small, curved edged [[fin]].  Both carried balanced control surfaces, the [[elevator (aircraft)|elevator]]'s balances projecting beyond the tailplane tips, and the low but broad chord, curved edge, deep [[rudder]] reaching down to the keel and moving within an elevator cut-out.<ref name=G&S/>

The rather tubby fuselage of the HD.15 had tubular cross-section [[longerons]] with similar, triangularly arranged, cross bracing.<ref name=Flight/>  The pilot's open [[cockpit]] was just behind the main wing spar, under a deep [[trailing edge]] cut-out to improve his upwards and forward vision.  Close behind was the observer's cockpit, fitted with a mounted pair of swivelling [[machine guns]].  The fuselage was fabric covered from the pilot's cockpit aft. The Hispano engine, enclosed under a metal [[aircraft fairing#Engine cowling|cowling]], was cooled with a pair of circular cross-section [[radiator (engine cooling)|radiator]]s mounted ventrally between the undercarriage legs.  The HD.15 had a fixed [[conventional undercarriage]], with mainwheels on a single axle mounted on the lower fuselage longerons by two pairs of V-struts.<ref name=G&S/>

==Operational history==

The HD.15 first flew in April 1922 and should have been in competition with the [[Gourdou-Leseurre GL.50]], but the two seat reconnaissance fighter programme had been abandoned before this date.  The whole high altitude fighter project, which also included single seaters, was dropped with the inability of Rateau to deliver reliable superchargers in quantity, essentially because of high temperature material problems.<ref name=G&S/>

Nonetheless, the [[Japan|Japanese Army]] became interested in supercharger-engined fighters and in 1926 the prototype HD.15 was sold and delivered to them. An order for three more followed, but the ship taking them to Japan was sunk by a tidal wave en voyage.{{citation needed|reason=Not likely a tidal wave, but could have been a tsunami|date=December 2016}}

==Specifications==
{{Aircraft specs
|ref=Green & Swanborough p.278<ref name=G&S/>
|prime units?=met
<!--General characteristics
-->
|genhide=

|crew=Two
|capacity=
|length m=7.60
|length note=
|span m=11.40
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=2.57
|height note=
|wing area sqm=32.48
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=1050
|empty weight note=
|gross weight kg=1750
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hispano-Suiza 8F]]b
|eng1 type=8-cylinder upright water-cooled [[supercharged]] [[V8 engine|V-8]] 
|eng1 hp=300
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=180
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=800
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=10250
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|guns=2 × fixed, forward-firing {{convert|7.7|mm|in|abbr=on|3}} [[Darne machine gun]]s; 2 × similar guns on mount in rear cockpit
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
}}

<!-- ==Notes== -->

==References==
{{commons category|Hanriot HD.15}}
{{reflist|refs=

<ref name=G&S>{{cite book |last=Green |first=William |first2= Gordon|last2= Swanborough |title=The Complete Book of Fighters |year=1994|publisher=Salamander Books|location=Godalming, UK|isbn=1-85833-777-1|page=278}}</ref>

<ref name=Flight>{{cite magazine|date=24 August 1922 |title=The Hanriot Two-seater Fighter, Type H.15|magazine=[[Flight International|Flight]]|volume=XIV|issue=34|pages=484–5|url=http://www.flightglobal.com/pdfarchive/view/1922/1922%20-%200484.html}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->
{{Hanriot aircraft}}

[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:French fighter aircraft 1920–1929]]
[[Category:Hanriot aircraft|Hanriot HD.15]]