{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox airport
| name                = RAF Ansty
| ensign              = [[File:Ensign of the Royal Air Force.svg|90px]]
| nativename          = 
| nativename-a        = 
| nativename-r        = 
| image               = 
| caption             = 
| IATA                = 
| ICAO                = 
| type                = Military
| owner               = [[Air Ministry]]
| operator            = [[Royal Air Force]]
| city-served         = 
| location            = [[Ansty, Warwickshire]]
| built               = 1935
| used                = 1936-1953
| elevation-m         = 116
| elevation-f         = {{Convert|116|m|disp=output number only|0}}
| coordinates         = {{Coord|52|25|45|N|001|24|31|W|region:GB_type:airport|display=inline,title}}
| website             = 
| pushpin_map         = Warwickshire
| pushpin_label       = RAF Ansty
| pushpin_map_caption = Location in Warwickshire
| r1-number           = 03/21
| r1-length-m         = 1,128
| r1-length-f         = 3,703
| r1-surface          = [[Asphalt]]
| r2-number           = 08/26
| r2-length-m         = 1,063
| r2-length-f         = 3,488
| r2-surface          = Asphalt
| stat-year           = 
| stat1-header        = 
| stat1-data          = 
| stat2-header        = 
| stat2-data          = 
}}
'''Royal Air Force Station Ansty''' or '''RAF Ansty''' is a former [[Royal Air Force]] [[Royal Air Force station|station]] located {{Convert|5.0|mi}} east of [[Coventry]] City centre, [[Warwickshire]], [[England]], {{Convert|7.0|mi}} north-west of [[Rugby, Warwickshire]]. The airfield was opened in 1936 and after training a large number of pupils closed in 1953.<ref name="ABCT">{{cite web|url=http://www.abct.org.uk/airfields/ansty-coventry|title=Ansty (Coventry) |publisher=[[Airfields of Britain Conservation Trust]]|accessdate=11 April 2012}}</ref>

==Station history==
[[File:DH 82A Tiger Moth - N81DH.jpg|thumb|right|A DH-82A Tiger Moth similar to the one's that flew from the airfield.]]
The airfield was mainly used for schools with taught navigation and flying to new recruits using a varied range of aircraft such as [[Tiger Moth]]s and [[Avro Anson]]s. The first school teaching navigation to arrive was [[No. 4 Civilian Air Navigation School]] with the Anson between September 1938 and October 1939 before being renamed [[No. 4 Air Observer Navigation School]] (AONS) using [[Blackburn Botha]]s as an additional aircraft type between September 1939 and July 1940 before moving to another airfield.<ref name="AA">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/flying_units.htm|title=Military flying units in the south west Midlands  |publisher=Aviation Archaeology|accessdate=11 April 2012}}</ref>

The other schools were used for flying training with the first school arriving on 6 January 1936 which was the [[No. 9 Elementary and Reserve Flying Training School]] which flew Ansons, [[Hawker Hart]]s, [[Hawker Hind]]s, [[Tiger Moth]]s and Clouds until 3 September 1939. The school was operated by Air Services Training at [[RAF Hamble]], under contract from the [[Air Ministry]]. The school was renamed the [[No. 9 Elementary Flying Training School]] on 3 September 1939 days after [[World War II]] broke out. The school used Moths until 31 March 1944 which provided initial assessment before pupil pilots were sent abroad in the [[British Commonwealth Air Training Plan]] which was operated by [[Air Service Training]].<ref name="AA"/>

A number of maintenance units used the site for a small amount of time like when a sub site of [[No. 27 Maintenance Unit RAF]] joined in October 1940 and [[No. 48 Maintenance Unit RAF|No. 48 MU]] which used the airfield for temporary dispersal between 1940 and February 1941. After World War II the airfield hosted [[No. 2 Basic Flying Training School RAF]] from 21 March 1951 until 31 March 1953.<ref name="AA"/>

==The Coventry Blitz==
The first bombs of the war dropped in the vicinity of [[Coventry]] were when five dropped on RAF Ansty on 25 June 1940. There were no casualties. This was two days before any civilians were killed near Coventry, when the Hillfield's area was bombed and 16 people lost their lives.<ref name="HC">{{cite web|url=http://www.historiccoventry.co.uk/blitz/stats.php|title=Coventry's Blitz |publisher=Historic Coventry|accessdate=11 April 2012}}</ref>

==Accidents and incidents==
During life as a RAF training base accidents were not far away with a number of airmen killed during training.

{| class="wikitable sortable"
|-
! Date !! Incident !! Reference
|-
| 5 March 1941 || A Tiger Moth of 9 EFTS was landing when it collided with another Tiger Moth on the ground. Both were burnt out, but there were no casualties. || <ref name="AACrashes1941">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/crashes1941.htm |title=Military aircraft crashes in the south west Midlands - 1941 |publisher=Aviation Archaeology|accessdate=11 April 2012}}</ref>
|-
| 14 May 1941 || Tiger Moths N5456 and N5472 of 9 EFTS are recorded as crashing. || <ref name="AACrashes1941"/>
|-
| 8 July 1941 || Tiger Moth N6649 of 9 EFTS hit a bus on approach. || <ref name="AACrashes1941"/>
|-
| 1 August 1941 || A pair of Tiger Moths from 9 EFTS collided in the air at [[Whitley, Coventry|Whitley]]. || <ref name="AACrashes1941"/>
|-
| 16 February 1942 || A pair of Tiger Moths from 9 EFTS collided near the airfield || <ref name="AACrashes1942">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/crashes1942.htm |title=Military aircraft crashes in the south west Midlands - 1942 |publisher=Aviation Archaeology|accessdate=11 April 2012}}</ref>
|-
| 29 May 1943 || Tiger Moth R4921 of 9 EFTS crashed near the airfield after engine failure. || <ref name="AACrashes1943">{{cite web|url=http://www.aviationarchaeology.org.uk/marg/crashes1943.htm |title=Military aircraft crashes in the south west Midlands - 1943 |publisher=Aviation Archaeology|accessdate=11 April 2012}}</ref>
|}

==Current use==

[[Rolls-Royce plc|Rolls-Royce]] currently occupies the majority of the site as an engine overhaul and repair facility. The company recently{{When|date=August 2013}} won a contract overhauling the EJ2000 engine, which is used in the [[Eurofighter Typhoon]] with some of the work being performed at Ansty, which will also help to keep 3,000 jobs for the company throughout the country.<ref>{{cite web|url=http://evaint.com/industry-news/-865m-engine-contract-for-raf-s-fastest-jets|title=£865m Engine contract for RAF's fastest jets |publisher=Evaint|accessdate=12 April 2012}}</ref><ref>{{cite web|url=http://www.4ni.co.uk/northern_ireland_news.asp?id=105280|title= RAF £865m Jet Investment |publisher=4 Northern Ireland|accessdate=12 April 2012}}</ref><ref>{{cite web|url=http://www.key.aero/view_news.asp?ID=1413&thisSection=military|title=New engine support contract for RAF Typhoons |publisher=Key Aero Publishing|accessdate=12 April 2012}}</ref>

The northern side is currently being turned into a small business park called Ansty Park.<ref name="ABCT"/>

==References==
{{reflist|2}}

== External links==
* [http://www.historiccoventry.co.uk/blitz/stats.php Historic Coventry - Blitz]
* [http://www.aircrew-saltire.org/lib084.htm Scottish Saltire Aircrew Association - Combatting Stress - Then and Now]
* [http://www.bbc.co.uk/history/ww2peopleswar/stories/99/a7838599.shtml BBC History WW2 People's War - Henry Kaye, Flying Instructor by Ron Goldstein]
* [http://213squadronassociation.homestead.com/Secondworldwar.html 213 Squadron Association - Second World War 1937 - 1945]
* [http://www.planningportal.rugby.gov.uk/fastweb_upload/Planning%20Scanned%20Applications/R09-0035/R09-0035%20140K%20Application%2013.11.08.pdf Rugby Counical - Planning Portal - Ansty Park]
* [http://www.bbcattic.org/ww2peopleswar/stories/36/a4333736.shtml BBC History - Flt. Lieut. John Anderson - Who knows what the future holds?]
*[http://www.thehennesseys.co.uk/BuddFamilyTree/The_Second_World_War.html William Budd’s time in the RAF]

{{Defunct airports in the United Kingdom}}
{{Royal Air Force}}

{{DEFAULTSORT:Ansty}}
[[Category:Defunct airports in England]]
[[Category:Royal Air Force stations in Warwickshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]