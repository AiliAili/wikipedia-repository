{{Infobox individual darts tournament
|tournament_name = [[World Darts Trophy]]
|image = 
|dates = 6 December 2003 – 14 December 2003
|venue = [[:nl:Vechtsebanen|De Vechtsebanen]]
|location = [[Utrecht (city)|Utrecht]], [[Utrecht (province)|Utrecht]]
|country = the [[Netherlands]]
|organisation = [[British Darts Organisation|BDO]] / [[World Darts Federation|WDF]]
|format = Men<br>[[Set (darts)|Sets]]<br>Final – Best of 11 [[Set (darts)|Sets]]<br>Women<br>[[Set (darts)|Sets]]<br>Final – Best of 5 [[Set (darts)|Sets]]
|prize_fund =
|winners_share =
|nine_dart =
|high_checkout = '''164''' {{flagicon|ENG}} [[Darryl Fitton]]
|winner = {{flagicon|NED}} [[Raymond van Barneveld]] (men)<br>{{flagicon|ENG}} [[Trina Gulliver]] (women)
|prev = [[2002 World Darts Trophy|2002]]
|next = [[2004 World Darts Trophy|2004]]}}

The '''2003 World Darts Trophy''' was the second edition of the [[World Darts Trophy]], a professional darts tournament held at the [[:nl:Vechtsebanen|De Vechtsebanen]] in [[Utrecht]], the [[Netherlands]], run by the [[British Darts Organisation]] and the [[World Darts Federation]].

The 2002 winner, [[Tony David]] was beaten in the semi-finals by, the [[2003 BDO World Darts Championship#Men|BDO World Champion]] and the eventual winner, [[Raymond van Barneveld]] in the men's event. Van Barneveld then beat [[Mervyn King (darts player)|Mervyn King]] in the final, 6–2 in sets. In the women's event, [[Mieke de Boer]], the 2002 winner, was defeated in the semi-finals by [[Francis Hoenselaar]]. Hoenselaar was in turn beaten by, the [[2003 BDO World Darts Championship#Women|BDO World Champion]], [[Trina Gulliver]], 3–1 in sets in the final.

==Seeds==

'''Men'''
# {{flagicon|NED}} [[Raymond van Barneveld]]
# {{flagicon|ENG}} [[Mervyn King (darts player)|Mervyn King]]
# {{flagicon|ENG}} [[Ted Hankey]]
# {{flagicon|AUS}} [[Tony David]]
# {{flagicon|ENG}} [[Tony O'Shea]]
# {{flagicon|ENG}} [[Andy Fordham]]
# {{flagicon|ENG}} [[Martin Adams]]
# {{flagicon|ENG}} [[Brian Derbyshire]]

== Prize Money ==
=== Men ===
{| class="wikitable" style="text-align: center;"
! style="background:#ffffff; color:#000000;"| Pos<ref>{{cite web | url=http://www.dartsdatabase.co.uk/EventPrizeFund.aspx?EventKey=270 | title=2003 World Darts Trophy Prize Money | publisher=Darts Database | accessdate=6 June 2015}}</ref>
! style="background:#ffffff; color:#000000;"| Money (Euros)
|-
| Winner
| 45,000
|-
| Runner-up
| 22,500
|-
| Semi-Finals
| 11,250
|-
| Quarter-Finals
| 6,000
|-
| Last 16
| 3,000
|-
| Last 32
| 2,000
|}

== Men's Tournament ==
{{32TeamBracket
| RD1=First Round<br />Best of 5 sets<ref name=2003WDT>{{cite web | url=http://www.dartsdatabase.co.uk/EventResults.aspx?EventKey=270 | title=2003 World Darts Trophy Results | publisher=Darts Database | accessdate=6 June 2015}}</ref><ref name=2003WDT2>{{cite web | url=https://www.mastercaller.nl/en/tournaments/bdo/world-darts-trophy-men/2003 | title=2003 BDO World Darts Trophy Men | publisher=Master Caller | accessdate=6 June 2015}}</ref>
| RD2=Second Round<br />Best of 5 sets<ref name=2003WDT/><ref name=2003WDT2/>
| RD3=Quarter-Finals<br />Best of 9 sets<ref name=2003WDT/><ref name=2003WDT2/>
| RD4=Semi-Finals<br />Best of 9 sets<ref name=2003WDT/><ref name=2003WDT2/>
| RD5=Final<br />Best of 11 sets<ref name=2003WDT/><ref name=2003WDT2/>
| score-width=15
| RD1-seed01=
| RD1-team01= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD1-score01= '''3'''
| RD1-seed02=
| RD1-team02= {{flagicon|BEL}} Johnn Snijers
| RD1-score02= 0
| RD1-seed03=
| RD1-team03= {{flagicon|NZL}} [[Peter Hunt (darts player)|Peter Hunt]]
| RD1-score03= 2
| RD1-seed04=
| RD1-team04= {{flagicon|SCO}} '''[[Bob Taylor (darts player)|Bob Taylor]]'''
| RD1-score04= '''3'''
| RD1-seed05=
| RD1-team05= {{flagicon|ENG}} '''[[Darryl Fitton]]'''
| RD1-score05= '''3'''
| RD1-seed06=
| RD1-team06= {{flagicon|ENG}} Brian Derbyshire
| RD1-score06= 2
| RD1-seed07=
| RD1-team07= {{flagicon|ENG}} [[Stephen Bunting]]
| RD1-score07= 1
| RD1-seed08=
| RD1-team08= {{flagicon|ENG}} '''[[Gary Robson (darts player)|Gary Robson]]'''
| RD1-score08= '''3'''
| RD1-seed09=
| RD1-team09= {{flagicon|ENG}} '''[[Colin Monk]]'''
| RD1-score09= '''3'''
| RD1-seed10=
| RD1-team10= {{flagicon|ENG}} [[Tony Eccles]]
| RD1-score10= 0
| RD1-seed11=
| RD1-team11= {{flagicon|ENG}} [[Tony O'Shea]]
| RD1-score11= 1
| RD1-seed12=
| RD1-team12= {{flagicon|AUS}} '''Steve Duke'''
| RD1-score12= '''3'''
| RD1-seed13=
| RD1-team13= {{flagicon|ENG}} '''[[John Walton (darts player)|John Walton]]'''
| RD1-score13= '''3'''
| RD1-seed14=
| RD1-team14= {{flagicon|NED}} [[Co Stompe]]
| RD1-score14= 1
| RD1-seed15=
| RD1-team15= {{flagicon|NOR}} [[Robert Wagner]]
| RD1-score15= 0
| RD1-seed16=
| RD1-team16= {{flagicon|AUS}} '''[[Tony David]]'''
| RD1-score16= '''3'''
| RD1-seed17=
| RD1-team17= {{flagicon|FIN}} '''[[Jarkko Komula]]'''
| RD1-score17= '''3'''
| RD1-seed18=
| RD1-team18= {{flagicon|ENG}} [[Ted Hankey]]
| RD1-score18= 0
| RD1-seed19=
| RD1-team19= {{flagicon|DEN}} [[Brian Sorensen|Brian Buur]]
| RD1-score19= 1
| RD1-seed20=
| RD1-team20= {{flagicon|ENG}} '''[[James Wade]]'''
| RD1-score20= '''3'''
| RD1-seed21=
| RD1-team21= {{flagicon|SCO}} '''[[Gary Anderson (darts player)|Gary Anderson]]'''
| RD1-score21= '''3'''
| RD1-seed22=
| RD1-team22= {{flagicon|ENG}} [[Andy Fordham]]
| RD1-score22= 0
| RD1-seed23=
| RD1-team23= {{flagicon|NED}} [[Rick Hofstra]]
| RD1-score23= 1
| RD1-seed24=
| RD1-team24= {{flagicon|ENG}} '''[[Tony West (darts player)|Tony West]]'''
| RD1-score24= '''3'''
| RD1-seed25=
| RD1-team25= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD1-score25= '''3'''
| RD1-seed26=
| RD1-team26= {{flagicon|GER}} [[Tomas Seyler]]
| RD1-score26= 2
| RD1-seed27=
| RD1-team27= {{flagicon|NED}} [[Albertino Essers]]
| RD1-score27= 2
| RD1-seed28=
| RD1-team28= {{flagicon|ENG}} '''[[Dave Routledge]]'''
| RD1-score28= '''3'''
| RD1-seed29=
| RD1-team29= {{flagicon|NED}} '''[[Vincent van der Voort]]'''
| RD1-score29= '''3'''
| RD1-seed30=
| RD1-team30= {{flagicon|ENG}} [[Steve Coote]]
| RD1-score30= 1
| RD1-seed31=
| RD1-team31= {{flagicon|ENG}} [[Shaun Greatbatch]]
| RD1-score31= 2
| RD1-seed32=
| RD1-team32= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD1-score32= '''3'''
| RD2-seed01=
| RD2-team01= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD2-score01= '''3'''
| RD2-seed02=
| RD2-team02= {{flagicon|SCO}} [[Bob Taylor (darts player)|Bob Taylor]]
| RD2-score02= 0
| RD2-seed03=
| RD2-team03= {{flagicon|ENG}} [[Darryl Fitton]]
| RD2-score03= 2
| RD2-seed04=
| RD2-team04= {{flagicon|ENG}} '''[[Gary Robson (darts player)|Gary Robson]]'''
| RD2-score04= '''3'''
| RD2-seed05=
| RD2-team05= {{flagicon|ENG}} '''[[Colin Monk]]'''
| RD2-score05= '''3'''
| RD2-seed06=
| RD2-team06= {{flagicon|AUS}} Steve Duke
| RD2-score06= 1
| RD2-seed07=
| RD2-team07= {{flagicon|ENG}} [[John Walton (darts player)|John Walton]]
| RD2-score07= 1
| RD2-seed08=
| RD2-team08= {{flagicon|AUS}} '''[[Tony David]]'''
| RD2-score08= '''3'''
| RD2-seed09=
| RD2-team09= {{flagicon|FIN}} '''[[Jarkko Komula]]'''
| RD2-score09= '''3'''
| RD2-seed10=
| RD2-team10= {{flagicon|ENG}} [[James Wade]]
| RD2-score10= 1
| RD2-seed11=
| RD2-team11= {{flagicon|SCO}} [[Gary Anderson (darts player)|Gary Anderson]]
| RD2-score11= 1
| RD2-seed12=
| RD2-team12= {{flagicon|ENG}} '''[[Tony West (darts player)|Tony West]]'''
| RD2-score12= '''3'''
| RD2-seed13=
| RD2-team13= {{flagicon|ENG}} '''[[Martin Adams]]'''
| RD2-score13= '''3'''
| RD2-seed14=
| RD2-team14= {{flagicon|ENG}} [[Dave Routledge]]
| RD2-score14= 1
| RD2-seed15=
| RD2-team15= {{flagicon|NED}} [[Vincent van der Voort]]
| RD2-score15= 0
| RD2-seed16=
| RD2-team16= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD2-score16= '''3'''
| RD3-seed01=
| RD3-team01= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD3-score01= '''5'''
| RD3-seed02=
| RD3-team02= {{flagicon|ENG}} [[Gary Robson (darts player)|Gary Robson]]
| RD3-score02= 0
| RD3-seed03=
| RD3-team03= {{flagicon|ENG}} [[Colin Monk]]
| RD3-score03= 2
| RD3-seed04=
| RD3-team04= {{flagicon|AUS}} '''[[Tony David]]'''
| RD3-score04= '''5'''
| RD3-seed05=
| RD3-team05= {{flagicon|FIN}} '''[[Jarkko Komula]]'''
| RD3-score05= '''5'''
| RD3-seed06=
| RD3-team06= {{flagicon|ENG}} [[Tony West (darts player)|Tony West]]
| RD3-score06= 4
| RD3-seed07=
| RD3-team07= {{flagicon|ENG}} [[Martin Adams]]
| RD3-score07= 4
| RD3-seed08=
| RD3-team08= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD3-score08= '''5'''
| RD4-seed01=
| RD4-team01= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD4-score01= '''5'''
| RD4-seed02=
| RD4-team02= {{flagicon|AUS}} [[Tony David]]
| RD4-score02= 2
| RD4-seed03=
| RD4-team03= {{flagicon|FIN}} [[Jarkko Komula]]
| RD4-score03= 2
| RD4-seed04=
| RD4-team04= {{flagicon|ENG}} '''[[Mervyn King (darts player)|Mervyn King]]'''
| RD4-score04= '''5'''
| RD5-seed01=
| RD5-team01= {{flagicon|NED}} '''[[Raymond van Barneveld|R van Barneveld]]'''
| RD5-score01= '''6'''
| RD5-seed02=
| RD5-team02= {{flagicon|ENG}} [[Mervyn King (darts player)|Mervyn King]]
| RD5-score02= 2
}}

== Women's Tournament ==
{{8TeamBracket-Compact-NoSeeds|RD2=Quarter-Finals<br>Best of 3 sets<ref name=2003WWDT>{{cite web | url=http://www.dartsdatabase.co.uk/EventResults.aspx?EventKey=822 |title=2003 Womens World Darts Trophy Results | publisher=Darts Database | accessdate=6 June 2015}}</ref><ref name=2003WWDT2>{{cite web | url=https://www.mastercaller.nl/en/tournaments/bdo/world-darts-trophy-ladies/2003 |title=2003 BDO World Darts Trophy Ladies | publisher=Master Caller | accessdate=6 June 2015}}</ref>
| RD3=Semi-Finals<br>Best of 3 sets<ref name=2003WWDT/><ref name=2003WWDT2/>
| RD4=Final<br>Best of 5 sets<ref name=2003WWDT/><ref name=2003WWDT2/>
| team-width=200px
| RD1-seed01=
| RD1-team01= {{flagicon|BEL}} Sandra Pollet
| RD1-score01= 1
| RD1-seed02=
| RD1-team02= {{flagicon|SCO}} '''[[Anne Kirk]]'''
| RD1-score02= '''2'''
| RD1-seed03=
| RD1-team03= {{flagicon|ENG}} '''[[Trina Gulliver]]'''
| RD1-score03= '''2'''
| RD1-seed04=
| RD1-team04= {{flagicon|ENG}} Crissy Howat
| RD1-score04= 0
| RD1-seed05=
| RD1-team05= {{flagicon|SWE}} Carina Ekberg
| RD1-score05= 0 
| RD1-seed06=
| RD1-team06= {{flagicon|NED}} '''[[Mieke de Boer]]'''
| RD1-score06= '''2'''
| RD1-seed07=
| RD1-team07= {{flagicon|NED}} '''[[Francis Hoenselaar]]'''
| RD1-score07= '''2'''
| RD1-seed08=
| RD1-team08= {{flagicon|NED}} [[Karin Krappen]]
| RD1-score08= 0
| RD2-seed01=
| RD2-team01= {{flagicon|SCO}} [[Anne Kirk]]
| RD2-score01= 0
| RD2-seed02= 
| RD2-team02= {{flagicon|ENG}} '''[[Trina Gulliver]]'''
| RD2-score02= '''2'''
| RD2-seed03=
| RD2-team03= {{flagicon|NED}} [[Mieke de Boer]]
| RD2-score03= 0
| RD2-seed04=
| RD2-team04= {{flagicon|NED}} '''[[Francis Hoenselaar]]'''
| RD2-score04= '''2'''
| RD3-seed01= 
| RD3-team01= {{flagicon|ENG}} '''[[Trina Gulliver]]'''
| RD3-score01= '''3'''
| RD3-seed02=
| RD3-team02= {{flagicon|NED}} [[Francis Hoenselaar]]
| RD3-score02= 1
}}

== References ==
{{Reflist|3}}

== External links ==

[[Category:Articles created via the Article Wizard]]
[[Category:Darts in the United Kingdom]]