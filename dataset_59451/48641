{{Infobox person
| name        = Rummana Habibullah Hussain
| image       = 
| alt         = 
| caption     = 
| other_names = 
| occupation  = Conceptual artist
| birth_date  = 1952
| death_date  = 1999
| birth_place = 
| death_place = 
}}

'''Rummana Hussain''' (1952–1999) was an artist and one of the pioneers of conceptual art, installation, and politically-engaged art in [[India]].<ref>{{cite news|first=Vandana |last=Kalra|title=Musings from the Past|newspaper=[[The Indian Express]]|date= October 12, 2010}}</ref>

==Biography==
Hussain was born in Bangalore, [[India]] to a prominent Muslim family. She was the sister of Wajahat Habibullah and wife of [[Ishaat Hussain]]. For much of her career, Hussain worked in oil and watercolor. She created largely allegorical figurative paintings.<ref>{{cite news|first=Holland|last= Cotter|title=Rummana Hussain, 47, Indian Conceptual Artist|newspaper=[[The New York Times]] |date=July 18, 1999}}</ref><ref>{{cite news|first=Anupa |last=Mehta|title=An Inward Journey|newspaper=[[The Independent]] |date=March 30, 1994}}</ref> Her art underwent a significant transformation, however, after the events of 1992 in [[Ayodhya]], India – a conflict between Hindu and Muslim communities which led to the [[Babri Mosque#demolition|destruction]] of the [[Babri Mosque|Babri Masjid]].<ref>{{cite journal|title=Ten memorable exhibitions from last year|journal=[[ArtAsiaPacific]]|date=January 2013}}</ref>  In response to the communal violence of the events, as well as to her sudden exposure to ideological assault as a Muslim, Hussain’s art not only became more explicitly political as well as personal, but it moved away from traditional media towards installation, video, photography, and mixed-media work.<ref>{{cite news|first=Ranjit |last=Hoskote|title=The Metaphor Survives|newspaper=The Times of India|date= April 17, 1994}}</ref> Throughout the 1990s, Hussain participated in exhibitions and events organized by [[Safdar Hashmi#Legacy|SAHMAT]], the Safdar Hashmi Memorial Trust, alongside other politically-conscious artists and performers.<ref>{{cite journal|first=Kamala |last=Kapoor|title= Home Nation|journal=Art Asia Pacific|year= 1997}}</ref> She was invited to be an artist-in-residence at [[Art in General]] in [[New York City]], in 1998, just a year before she died, at age 47, after a battle with cancer.<ref>{{cite news|first=Holland |last=Cotter|title=Rummana Hussain: In Order to Join|newspaper=The New York Times|date= October 16, 1998}}</ref> Hussain’s work has been on view in exhibitions and art fairs worldwide, including at [[Tate Modern]], in [[London]], [[National Gallery of Modern Art, Mumbai|National Gallery of Modern Art (NGMA)]], in [[Mumbai]], [[Smart Museum of Art|Smart Museum]], in [[Chicago]], the 3rd Asia Pacific Triennial, in Brisbane, Australia, and at [[Talwar Gallery]], which represents the estate of the artist.<ref>{{cite web|url=http://talwargallery.com/rummana-bio/|publisher=Talwar Gallery|title=Rummana Hussain|accessdate=July 31, 2014}}</ref> Her work is included in the permanent collection of the [[Queensland Art Gallery]], in Queensland, Australia.

== Work ==

Hussain is cited as one of the foremost leaders in the development of [[conceptual art]] in India, and is credited with bringing the possibilities and merits of diverse media to critical and popular attention.<ref>{{cite news|first=Anupa |last=Mehta|title=What’s a bicycle doing in the art gallery?|newspaper=The Independent|date= March 30, 1994}}</ref> Despite her association with conceptual art, however, Hussain’s work remains grounded in the physical using, rather than ignoring, the “sensuousness” of the various materials that make up her installations.<ref>{{cite book|first=Roshan |last=Shahani|title=Ways of Seeing in ’94|year= 1994}}</ref> Critics often reference this emphasis on materiality in the discussion of the social, specifically feminist, concerns of much of Hussain’s oeuvre which acknowledges female corporeality as its starting point.<ref>{{cite news|first=Vishwapriya L|last= Iyengar|title=Looking for meaning in myriad|newspaper=[[The Asian Age]]|date=December 2009}}</ref> Several of her video and [http://talwargallery.com/diarypages-works7/ performance-based pieces], for example, center on Hussain’s own body – a tactic that positions her work at a unique juncture between the political and personal, the public and private. According to art historian [[Geeta Kapur]], Hussain “makes [female and religious identity] matter in a conscious and dialectical way…she not only pitches her identity for display, she [also] constructs a public space for debate.”<ref>{{cite journal|first=Geeta |last=Kapur|title=The Courage of being Rummana|journal=Art India|date=January–April 1999}}</ref> Hussain's work both establishes an effective relationship with the viewer, and challenges him or her to act.

== Notable exhibitions ==

=== Solo exhibitions ===

*2016, [http://talwargallery.com/rummana-nybreakingskin-pr/ Talwar Gallery,] ''Breaking Skin,'' New York, NY, US
*2015, [http://www.talwargallery.com Talwar Gallery], ''Breaking Skin'', New Delhi, India<ref name="talwargallery">{{cite web|url=http://talwargallery.com/rummana-breakingskin-pr/|title=Rummana Hussain - Breaking Skin press release|publisher=Talwar Gallery|accessdate=January 20, 2014}}</ref>
*2012, [http://www.talwargallery.com Talwar Gallery], New York, NY, US<ref>{{cite web|url=http://talwargallery.com/rummana-ny-pr/ |title=Rummana Hussain press release|accessdate=July 31, 2014|publisher=Talwar Gallery}}</ref>
*2010, [http://www.talwargallery.com Talwar Gallery], ''Fortitude from Fragments'', New Delhi, India<ref name="talwargallery">{{cite web|url=http://talwargallery.com/rummanafragments-pr/|title=Rummana Hussain - Fortitude from Fragments press release|publisher=Talwar Gallery|accessdate=January 20, 2014}}</ref>
*1998, Art in General, ''In Order to Join'', New York, NY, US<ref name="artingeneral">{{cite web|url=http://www.artingeneral.org/exhibitions/181|title=Rummana Hussain|publisher=artingeneral.org|accessdate=January 20, 2014}}</ref>
*1994, Gallery Chemould, ''Fragments/Multiples'', Mumbai, India, and traveled to L.T.G. Gallery, New Delhi, India
*1991, Centre for Contemporary Art, New Delhi, India
*1991, [[Jehangir Art Gallery]], Bombay, India
*1986, [[Triveni Kala Sangam|Shridharani Gallery]], New Delhi, India
*1984, [[Triveni Kala Sangam|Triveni Gallery]], New Delhi, India
*1983, [[Academy of Fine Arts, Kolkata|Academy of Fine Arts]], Calcutta, India

=== Group exhibitions ===

*2014, Kiran Nadar Museum of Art, ''[http://talwargallery.com/rummana-knma-pr/ Is it what you think?],'' New Delhi, India
*2013, Smart Museum, [http://talwargallery.com/rummana-smart-pr/ ''The Sahmat Collective: Art and Activism in India Since 1989''], Chicago, IL, US<ref name="uchicago">{{cite web|url=http://smartmuseum.uchicago.edu/exhibitions/the-sahmat-collective-art-and-activism-in-india-since-1989/|title=The Sahmat Collective: Art and Activism in India since 1989 |publisher=[[University of Chicago]]|accessdate=January 1, 2014}}</ref>
*2009, [http://www.talwargallery.com Talwar Gallery], ''Excerpts from Diary Pages'', New York, NY, US<ref>{{cite web|url=http://talwargallery.com/diarypages-pr/ |title=Excerpts from Diary Pages press release|accessdate=July 31, 2014|publisher=Talwar Gallery}}</ref>
*2009, Safdar Hashmi Memorial Trust (SAHMAT), ''IMAGE MUSIC TEXT, SAHMAT (20 Years)''], New Delhi, India<ref name="sahmat">{{cite web|url=http://www.sahmat.org/imagemtext2009.html|title=IMAGE MUSIC TEXT 20 years of SAHMAT Exhibition|publisher=sahmat.org|accessdate=January 1, 2014}}</ref>
*2007, [[Rose Art Museum]], ''Tiger by the Tail!'', Waltham, MA, US and travel to

:[[Lowe Art Museum]], Miami, FL, US
:[[Katzen Arts Center]], Washington, D.C., US
:[[Zimmerli Art Museum at Rutgers University|The Jane Voorhees Zimmerli Art Museum]], New Brunswick, NJ, US
:[http://www.upenn.edu/ARG/ Arthur Ross Gallery, University of Pennsylvania], Philadelphia, PA, US

*2004, [http://www.artgallery.wa.gov.au/exhibitions/x_edgeofdesire.asp Art Gallery of Western Australia, ''Edge of Desire: Recent Art in India''], Perth, Australia, and travel to

:[[National Gallery of Modern Art, Mumbai|National Gallery of Modern Art (NGMA)]], Mumbai, India
:[[Museo de Arte Contemporáneo de Monterrey|Museum of Contemporary Art (MARCO)]], Monterrey, Mexico
:[[Museo Rufino Tamayo|Tamayo Museum]], Mexico City, Mexico
:[[Asia Society]], New York, NY, US

*2002, [http://presentationhousegallery.org/exhibition/from-goddess-to-pin-up-icons-of-femininity-in-indian-calendar-art/ Vancouver Art Gallery, ''Moving Ideas: A Contemporary Cultural Dialogue with India''], Vancouver, Canada
*2001, [http://www.tate.org.uk/whats-on/tate-modern/exhibition/century-city Tate Modern, ''Century City''], London, UK
*1999, [http://www.qagoma.qld.gov.au/exhibitions/apt/apt_3_(1999) Queensland Art Gallery, The Third Asia Pacific Triennial], Queensland, Australia
*1997, [[Victoria Art Gallery]], ''Telling Tales'', British Council, Bath, UK
*1997, [http://www.norwayemb.org.in/Embassy/ Royal Norwegian Embassy], ''Crosscurrents: Museums of Ethnography'', New Delhi, India
*1997, [http://mcam.mills.edu/ Mills College Art Museum], ''Women Artists of India: A Celebration of Independence'', Oakland, CA, US
*1995, [http://www.sahmat.org/ Safdar Hashmi Memorial Trust (SAHMAT), ''Postcards for Gandhi''], New Delhi, India
*1995, [[Middlesbrough Institute of Modern Art]], ''Inside Out: Contemporary Women Artists of India'', Middlesbrough, UK
*1993, Husain Ki Sarai, Exhibition in Aid of Earthquake Victims, Faridabad, India
*1992, [http://www.sahmat.org/7-931992.html Safdar Hashmi Memorial Trust (SAHMAT), ''Images and Words''], New Delhi, India
*1992, [[Tata Centre]], Calcutta, India

== Performance and video ==

*1998

[http://artingeneral.org/exhibitions/181 Art in General, Residency], New York, NY, US

*1997

Artspace Studios, Residency, Bristol, UK

*1996

[[Ministry of Human Resource Development]] Senior Fellowship (Visual Arts), New Delhi, India

== References ==

{{Reflist}}

== External links ==
*[http://talwargallery.com/wp-content/themes/emptiness/rummana-press/nov2015artforum.pdf Artforum, Critics' Picks, November 2015.]
*[http://talwargallery.com/wp-content/themes/emptiness/rummana-press/sept14aap.pdf Art Asia Pacific, Rummana Hussain, September/October 2014.]
*[http://talwargallery.com/wp-content/themes/emptiness/rummana-press/jan12artfair.pdf Art Fair, Rummana Hussain: A dialogue through diffracted traces..., January 2012.]
*[http://collection.qagoma.qld.gov.au/qag/imu.php?request=display&port=45001&id=28db&flag=ecatalogue&offset=0&count=default&view=details Rummana Hussain in the permanent collection of the Queensland Art Gallery].
*[http://www.aaa.org.hk/Collection/CollectionOnline/SpecialCollectionItem/3941 Art India, ''The Courage of Being Rummana''], January–April 1999.
*[http://www.indianexpress.com/news/musings-from-the-past/695918/ The Indian Express, ''Musings from the Past''], October 12, 2010.
*[https://www.nytimes.com/1999/07/18/nyregion/rummana-hussain-47-indian-conceptual-artist.html The New York Times, ''Rummana Hussain, 47, Indian Conceptual Artist''], July 18, 1999.
*[https://www.nytimes.com/1998/10/23/arts/art-guide.html?pagewanted=all&src=pm The New York Times, ''Rummana Hussain''], October 23, 1998.
*[https://www.nytimes.com/1998/10/16/arts/art-in-review-370037.html?gwh=B6252EC70AF7D9444909A0AB577CF4BE The New York Times, ''Rummana Hussain: In Order to Join''], October 16, 1998.

{{DEFAULTSORT:Hussain, Rummana}}
[[Category:Articles created via the Article Wizard]]
[[Category:1952 births]]
[[Category:1999 deaths]]
[[Category:Indian Muslims]]
[[Category:Women artists from Karnataka]]
[[Category:Political artists]]
[[Category:Artists from Bangalore]]
[[Category:20th-century Indian women artists]]
[[Category:20th-century Indian painters]]
[[Category:Women installation artists]]
[[Category:Women video artists]]
[[Category:Painters from Karnataka]]