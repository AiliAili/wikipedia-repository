{{Infobox AFL club season
| club                 = [[Adelaide Football Club]]
| season               = 2008
| image                =
| imagesize            = 120px
| caption              = Adelaide's guernsey for the 2008 season
| president            = Bill Sanders<ref>{{Cite news|last=Australian Associated Press |title=Rob Chapman appointed Crows chairman |newspaper=WAtoday |date=2008-12-12 |url=http://www.watoday.com.au/afl/afl-news/rob-chapman-appointed-crows-chairman-20091124-j37f.html |accessdate=2010-04-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20110706113503/http://www.watoday.com.au/afl/afl-news/rob-chapman-appointed-crows-chairman-20091124-j37f.html |archivedate=2011-07-06 |df= }}</ref>
| coach                = [[Neil Craig]]
| captain              = [[Simon Goodwin]]
| home ground          = [[AAMI Stadium]]
| preseason comp       = [[2008 NAB Cup|Pre-season competition]]
| preseason result     = Runners up
| regularseason comp   = [[2008 AFL season|AFL season]]
| regularseason result = 5th
| finals series        = [[2008 AFL finals series|Finals series]]
| finals result        = Elimination final
| club b&f             = [[Malcolm Blight Medal|Best and Fairest]]
| best and fairest     = [[Nathan Bock]]
| leading goalscorer   = [[Brett Burton]] (34)
| highest attendance   = 45,524 vs. {{AFL PA}} (April 6, 2008)
| lowest attendance    = 35,649 vs. {{AFL Mel}} (May 18, 2008)
| average attendance   = 40,678
| prevseason           = [[2007 Adelaide Football Club season|2007]]
| nextseason           = [[2009 Adelaide Football Club season|2009]]
}}
After the disaster of 2007 team rebuilding was required. Notable absentees from the 2007 list included Jason Torney, Martin Mattner, Scott Welsh, and Ben Hudson. To compensate for their losses the acquisition of Brad Symes supported Neil Craig's plan to rejuvenate the ageing midfield, while Brad Moran added depth in the Ruck and key position divisions. Former Adelaide players Ben Hart and Matthew Clarke were appointed as new assistant coaches.

== Playing list changes ==

'''In:''' [[Tony Armstrong]] (draft pick 58), [[Myke Cook]] (draft pick 38), [[Patrick Dangerfield]] (draft pick 10), [[Jarrhan Jacky]] (draft pick 30), [[Aaron Kite]] (draft pick 71), [[Brad Moran (footballer)|Brad Moran]] (traded from {{AFL Kan}} for draft pick 37), [[Andy Otten]] (draft pick 27), [[Brad Symes]] (traded from {{AFL PA}} for draft pick 28), [[Taylor Walker (footballer)|Taylor Walker]] (draft pick 75)

'''Rookie: '''[[Ed Curnow]] (rookie draft pick 40), [[Brodie Martin]] (rookie draft pick 52), [[James Moss (footballer)|James Moss]] (rookie draft pick 9), [[Jared Petrenko]] (rookie draft pick 25)

'''Out:''' [[Rhys Archard]], [[Matthew Bode]], [[John Hinge]], [[Ben Hudson]] (traded to {{AFL WB}}), [[Andrew McIntyre]], [[Martin Mattner]] (traded to {{AFL Syd}} for draft pick 28), [[John Meesen]] traded to {{AFL Mel}} for draft pick 37), [[Ian Perrie]], [[Darren Pfeiffer]], [[Mark Ricciuto]] (retired), [[Jason Torney]], [[James Turner (footballer)|James Turner]], [[Scott Welsh]]

See [[List of Adelaide Football Club players#2006 - 2009|Adelaide Crows 2008 Playing List]] for the playing full list.

== NAB Cup ==

The [[2008 NAB Cup]] provided much hope for Adelaide fans stemming from their team's progression to the Grand Final. Adelaide was initially able to beat the more fancied Collingwood in Dubai, before accounting for Fremantle and Hawthorn (both at home). Adelaide had kicked more Supergoals than their to-be opposition St. Kilda and so was given the privilege of hosting the NAB cup Grand Final. It was the 2nd time in 3 years, and 5th time overall, that the Crows had reached the Preseason Cup final. However they were unable to defeat St Kilda in the Adelaide heatwave at AAMI Stadium: 37 degrees C plus, the game was a twilight game, starting during late afternoon daylight and ending under dark skies.

The match was heavily criticised by some in the Victorian media for having the lowest crowd attendance for a pre-season grand final in history. In particular the members of the Adelaide Football Club were criticised for not attending their team's home final; though most media outlets spreading this criticism neglected to take into account the extreme heat (with very low humidity) which at night wouldn't have been too much of a problem because of the sun.<ref>{{cite web|title=Saints win NAB Cup |url=http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=56004 |work= |archiveurl=http://www.webcitation.org/5gwsUvQ5D?url=http%3A%2F%2Fwww.afl.com.au%2FNews%2FNewsArticle%2Ftabid%2F208%2FDefault.aspx%3FnewsId%3D56004 |archivedate=2009-05-22 |deadurl=no |accessdate=2009-04-17 |df= }}</ref>

Despite their narrow loss, fans saw glimpses of the future for the club. The rejuvenated midfield, replacing ageing champions such as Tyson Edwards and Simon Goodwin and freeing them to move into the forward line. A new forward line thus emerged, featuring young key position forwards James Sellar and [[Kurt Tippett]] – both tipped to lead Adelaide's attack in the new era – along with the addition of veterans Andrew McLeod, Simon Goodwin and Tyson Edwards to provide experience and quality in what had long been Adelaide's Achilles Heel.

== Home and Away season ==

{| class="wikitable" width="90%" align="center"
! colspan=8 style="background:#9EBD69" | 2008 Season Results
|-
! width="8%" |Round
! width="8%" |Date
! width="20%" |Opponent
! width="20%" |Venue (H/A)
! width="18%" |Score
! width="10%" |Ladder Position
! width="8%" |Percentage
! width="8%" |W/L
|- bgcolor="#ffbbbb"
| align="center"|1 
|               |23 March
|               |{{AFL WB}}
|               |Docklands (A)
|               |18.15 (123) – 19.12 (126)
| align="center"|9
| align="center"|97.62
| align="center"|0–1
|- bgcolor="#bbffbb"
| align="center"|2 
|               |29 March
|               |{{AFL WC}}
|               |AAMI Stadium (H)
|               |21.7 (133) – 8.9 (57)
| align="center"|6
| align="center"|139.89
| align="center"|1–1
|- bgcolor="#bbffbb"
| align="center"|3 
|               |6 April
|               |{{AFL PA}}
|               |AAMI Stadium (H)
|               |12.13 (85) – 11.13 (79)
| align="center"|5
| align="center"|130.15
| align="center"|2–1
|- bgcolor="#ffbbbb"
| align="center"|4 
|               |13 April
|               |{{AFL Haw}}
|               |Aurora Stadium (A)
|               |10.10 (70) – 17.12 (114)
| align="center"|6
| align="center"|109.31
| align="center"|2–2
|- bgcolor="#bbffbb"
| align="center"|5 
|               |19 April
|               |{{AFL Fre}}
|               |AAMI Stadium (H)
|               |12.16 (88) – 10.11 (71)
| align="center"|5
| align="center"|111.63
| align="center"|3–2
|- bgcolor="#bbffbb"
| align="center"|6 
|               |26 April
|               |{{AFL Car}}
|               |MCG (A)
|               |16.15 (111) – 11.15 (81)
| align="center"|4
| align="center"|115.53
| align="center"|4–2
|- bgcolor="#bbffbb"
| align="center"|7 
|               |3 May
|               |{{AFL Nor}}
|               |AAMI Stadium (H)
|               |15.17 (107) – 11.8 (74)
| align="center"|4
| align="center"|119.10
| align="center"|5–2
|- bgcolor="#bbffbb"
| align="center"|8 
|               |18 May
|               |{{AFL Mel}}
|               |AAMI Stadium (H)
|               |22.18 (150) – 11.8 (74)
| align="center"|4
| align="center"|128.25
| align="center"|6–2
|- bgcolor="#ffbbbb"
| align="center"|9 
|               |24 May
|               |{{AFL WC}}
|               |Subiaco (A)
|               |5.17 (47) – 14.13 (97)
| align="center"|4
| align="center"|118.24
| align="center"|6–3
|- bgcolor="#bbffbb"
| align="center"|10 
|               |30 May
|               |{{AFL Ess}}
|               |AAMI Stadium (H)
|               |9.20 (74) – 10.9 (69)
| align="center"|4
| align="center"|117.34
| align="center"|7–3
|- bgcolor="#bbffbb"
| align="center"|11 
|               |7 June
|               |{{AFL Ric}}
|               |MCG (A)
|               |22.14 (146) – 14.12 (96)
| align="center"|4
| align="center"|120.90
| align="center"|8–3
|- bgcolor="#ffbbbb"
| align="center"|12 
|               |14 June
|               |{{AFL Haw}}
|               |AAMI Stadium (H)
|               |10.12 (72) – 11.10 (76)
| align="center"|5
| align="center"|118.93
| align="center"|8–4
|- bgcolor="#ffbbbb"
| align="center"|13 
|               |21 June
|               |{{AFL BL}}
|               |Brisbane Cricket Ground (A)
|               |10.10 (70) – 11.17 (83)
| align="center"|5
| align="center"|116.32
| align="center"|8–5
|- bgcolor="#ffbbbb"
| align="center"|14 
|               |4 July
|               |{{AFL Gee}}
|               |AAMI Stadium (H)
|               |8.8 (56) – 18.16 (124)
| align="center"|6
| align="center"|109.09
| align="center"|8–6
|- bgcolor="#ffbbbb"
| align="center"|15 
|               |12 July
|               |{{AFL Col}}
|               |MCG (A)
|               |11.8 (74) – 15.16 (106)
| align="center"|6
| align="center"|105.95
| align="center"|8–7
|- bgcolor="#ffbbbb"
| align="center"|16 
|               |20 July
|               |{{AFL PA}}
|               |AAMI Stadium (A)
|               |11.14 (80) – 13.14 (92)
| align="center"|9
| align="center"|104.72
| align="center"|8–8
|- bgcolor="#bbffbb"
| align="center"|17 
|               |26 July
|               |{{AFL Syd}}
|               |Sydney Cricket Ground (A)
|               |11.11 (77) – 6.17 (53)
| align="center"|7
| align="center"|106.18
| align="center"|9–8
|- bgcolor="#bbffbb"
| align="center"|18 
|               |3 August
|               |{{AFL Car}}
|               |AAMI Stadium (H)
|               |13.16 (94) – 12.14 (86)
| align="center"|6
| align="center"|106.35
| align="center"|10–8
|- bgcolor="#bbffbb"
| align="center"|19 
|               |10 August
|               |{{AFL Ric}}
|               |AAMI Stadium (H)
|               |16.12 (109) – 6.9 (45)
| align="center"|6
| align="center"|110.11
| align="center"|11–8
|- bgcolor="#bbffbb"
| align="center"|20 
|               |16 August
|               |{{AFL Ess}}
|               |Docklands (A)
|               |19.15 (129) – 10.13 (73)
| align="center"|6
| align="center"|113.01
| align="center"|12–8
|- bgcolor="#ffbbbb"
| align="center"|21 
|               |24 August
|               |{{AFL StK}}
|               |Docklands (A)
|               |6.11 (47) – 13.17 (95)
| align="center"|6
| align="center"|109.60
| align="center"|12–9
|- bgcolor="#bbffbb"
| align="center"|22 
|               |30 August
|               |{{AFL WB}}
|               |AAMI Stadium (H)
|               |10.16 (76) – 9.13 (67)
| align="center"|5
| align="center"|109.74
| align="center"|13–9
|}

(Note: in the table above, green rows are wins, red rows are losses.  In the Score column Adelaide scores are always shown first.)

{{main|2008 AFL season}}

The season got off to an anticlimactic start in a high scoring shootout at the Telstra Dome as the Crows went down to the Western Bulldogs (126–123) when Nathan Bock missed a set shot on goal just as the siren sounded, which would have won the match.<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=56717 Johnson heroics send Dogs to all-time high]</ref> The club surprised all those who doubted them by thrashing finals hopefuls West Coast in a stunning round 2 display at home, winning by 76 points (133–57).<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsid=56840 Crows smash hoodoo]</ref>

Round 3 saw the return of the Rivalry Round, and possibly the most anticipated Showdown in history, against a Port Adelaide side which had lost the first 2 rounds of the season: firstly against the reigning champs Geelong at AAMI Stadium, and then in a match that they were predominantly tipped to win against the Swans at the SCG. With the Crows having won 6 of their last 7 Showdowns, a win in Showdown XXIV would square the ledger with 12 Showdown victories each. The clash was probably one of the most M rated Showdowns in history, even more so than the one near the end of season 2006 which saw Trent Hentschell go down with a serious knee injury and out for the entire 2007 season.The Crows managed to attain a 16-point lead late in the match, before Port made one last scoring surge. However, the Crows held firm, and managed to hold the victory by 6 points (85–79) as the home crowd of over 45,000 went into a frenzy after the final siren had sounded.<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=57452 Winged Crows fly on]</ref>

The following week, round 4, Adelaide had a bad day and struggled in a 44-point loss to Hawthorn in Launceston (114–70). They bounced back to win four straight games, with the help of three of those games being played at home.<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=57822 Super Hawks too good for Crows]</ref>

Round 5 saw a 17-point win over Fremantle at home (88–71),<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=58113 Crows hold off Freo]</ref> round 6 a 30-point win over Carlton at the MCG (111–81),<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=58500 Crows too classy]</ref> round 7 a 33-point win over North Melbourne at home (107–74),<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=58903 Crows outclass North]</ref> and round 8 a 76-point thashing of Melbourne at home (150–74).<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=59676 Crows destroy Demons]</ref>

In the Round 8 victory against Melbourne at AAMI stadium, Brett Burton took one of the greatest marks in Crows history at the Northern end of the ground in the final quarter where he climbed the shoulders of an opponent and grabbed the ball into his chest before falling hard to the ground. After the win against Melbourne, Adelaide stood 5–0 at home for the first time ever in its history.

The Crows looked to continue their winning streak against a struggling, but hard to beat West Coast Eagles in a tough trip to Subiaco Oval in Round 9. However, inaccurate kicking cost the Crows dearly and they never managed to get back into the game after a slow start. With a tendency that continues to bewilder fans, the Crows again managed to find a way to lose the 'unlosable' game, in their red uniforms, eventually going down by 50 points to the team they destroyed by 76 points just 7 rounds previously (97–47).<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=60082 Eagles crush Crows at Subi]</ref>

In round ten of the 2008 season the Adelaide Crows overcame the Essendon Bombers in a close and hard fought match, finally coming out winners by five points: 74 – 69. Poor goal kicking accuracy was the main reason for the game being so close, with the main culprits being Brett Burton, Simon Goodwin and Jason Porplyzia. Scott Thompson was one of Adelaide's key players getting 33 possessions in the midfield and providing a lot of drive going forward. Burton was reported for a head-high bump to Essendon's Henry Slattery and was subsequently offered, and accepted, a two-match ban by the AFL's match-review committee.<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsid=60497 Crows in cliff-hanger]</ref>

Round eleven saw the Crows return to the MCG to face the under-performing Richmond in their red clash jumper yet again. Continuing a worrying trend in matches away from home, Adelaide started slowly and were fortunate to only trail by 14 points at half-time. After the long-break, however, the Crows woke from their slumbering style and proceeded to kick 15 goals to 5 in the second half, to run out easy winners by 50 points (146–96). Bernie Vince had 30 disposals, Scott Thompson kicked 6 goals, and Ivan Maric stepped up to dominate the ruck against the more experienced Simmonds.<ref>[http://www.afl.com.au/tabid/208/Default.aspx?newsid=60864 Crows crush fading Tigers]</ref>

At the halfway point in the season, Adelaide had an 8–3 record with a percentage of 120.90, and sat two points clear of Sydney in fourth spot on the premiership ladder. Home record: played 6, won 6; away record: played 5, won 2.

Round 12 saw Adelaide lose to Hawthorn by 4 points and it could be hard to argue that it could be their most agonizing defeat of the season (76–72). The team started well, scoring two goals with 23 disposals before Hawthorn had a confirmed stat. But the Hawks came back and the game was tight throughout, with Adelaide leading by 1 point at quarter-time, and increasing that lead by one for each of the next two quarters. The last quarter was again a close affair until the tough and calm Luke Hodge of Hawthorn snapped a goal with only a few minutes left on the clock to put the Hawks in front. From there Hawthorn closed down the game and held on by four points. It was Hawthorn's third away win against Adelaide, but their first since that infamous 97-point thrashing in Round 9, 1994. Adelaide dropped to fifth on the premiership ladder after the loss, and it may be seen in the weeks ahead as a devastating blow to their top 4 hopes, despite their 8–4 record.<ref>[http://www.afl.com.au/News/NewsArticle/tabid/208/Default.aspx?newsId=61303 Hawks win, but Buddy is in the book]</ref>

Adelaide slipped to 8–5 after losing to Brisbane at the Gabba by 13 points (83–70) in round 13. Although leading by 12 points at half-time, and 2 at three-quarter time, Adelaide were outrun by Brisbane in the last term. The Crows couldn't get their forward structure working properly even with the return of Burton from suspension.<ref>[http://www.afl.com.au/tabid/208/Default.aspx?newsid=61764 Lions outlast Crows]</ref>

In the second weekend of the split round 14, Adelaide were visited by Geelong and promptly defeated by 68 points (124–56). Adelaide were never in the contest, scoring only one point in the first quarter. The Crows slipped to 6th on the premiership ladder after the loss with a percentage of only 109.09.<ref>[http://www.afl.com.au/tabid/208/Default.aspx?newsid=62623 Top Cats crush Crows]</ref>

In round 15, Adelaide travelled to Melbourne to face Collingwood at the MCG. The Crows led by as much as 22 points midway through the second term but faded after half-time to lose by 32 (106–74). This was the first time the team had lost four games in a row under Neil Craig. Added to the loss were injuries to Burton (knee) and Porplyzia (shoulder), the former of which would end his season.<ref>[http://www.afl.com.au/tabid/208/Default.aspx?newsid=63029 Top four in reach for Pies]</ref>

In front of the smallest crowd in Showdown history, Adelaide lost the second game of 2008 against Port Adelaide by 12 points in round 16, (92–80). A lack of a settled forward line and Port's commitment led to a stifled Crows outfit. Adelaide slipped out of the eight after this loss, their fifth in a row.<ref>[http://www.afl.com.au/tabid/208/Default.aspx?newsId=63519 Crows 'Powered' out of top eight]</ref>

Adelaide faced Sydney at the SCG in round 17 as underdogs, even though they had a dominant record over the Swans since 2002. Sydney couldn't get anything moving and Adelaide were inspired by the sudden return of Porplyzia, who kicked five goals, with a 23-point win (77–53). Adelaide have now won 9 of the past 10 games against Sydney.<ref>[http://www.afl.com.au/tabid/208/Default.aspx?newsId=64016 Crows strangle Swans at the SCG]</ref>

Dominant second and third quarters, during which they kicked 11 goals to 5, helped Adelaide to a tight 8-point win (94–86) over Carlton at AAMI Stadium in their round 18 clash. Brad Moran popped up with four goals at full-forward, a move that was forced by a heavy knock to Jason Porplyzia in the second term, which would redislocate his shoulder as well as causing potentially season-ending brain bruising. At the end of the round, the Crows sat in sixth place with a 10–8 win/loss ratio.<ref>[http://www.afl.com.au/News/NEWSARTICLE/tabid/208/Default.aspx?newsId=64493 Crows hold out Blues]</ref>

In an easy game against Richmond in Round 19 (a win of 63 points, 108–45), Andrew McLeod celebrated his 300th game playing AFL football with a superb display off the half-back line. The AAMI stadium surface was wet and slippery after heavy rain during the morning of the game which made for difficult conditions. Adelaide handled the ball far better than the Tigers, kicking 7.2 to 0.1 in the second term to lead by an even 9 goals at half-time. After that, the Crows were never threatened and held on to sixth spot on the AFL ladder, four points clear of Collingwood.<ref>[http://www.afl.com.au/News/NEWSARTICLE/tabid/208/Default.aspx?newsId=65000 Crows stay in hunt for top four]</ref>

Adelaide then travelled to the Telstra Dome for a potentially tricky game against in-form Essendon in Round 20. After the game was a low-scoring slog for the first quarter and a half, the Crows broke away with ten goals to one either side of half time to lead by 56 points late in the third quarter and eventually won by the same margin (129–73). Key forward Nick Gill finally delivered on much promise, booting five goals including three in the last quarter. Controversial draftee Patrick Dangerfield played his debut and showed potential despite being unspectacular, kicking a goal on the half-time siren. The win guaranteed the Crows a place in the finals, and also put them in the top four pending the result of North Melbourne's clash with Carlton. After a win by North, Adelaide sat in fifth position with 48 points and a percentage of 113.01. Of interest is the fact that this was the first time Adelaide had defeated Essendon in Melbourne, after 11 attempts spread over the 17 years since they entered the AFL competition.<ref>[http://www.afl.com.au/News/NEWSARTICLE/tabid/208/Default.aspx?newsId=65640 Crows break Bomber hoodoo]</ref>

A return to the Telstra Dome in Round 21 saw the Crows tackle St Kilda and play probably their worst game of the year so far. St Kilda had the incentive of the game being Robert Harvey's final home game, as well as the added bonus of cementing their place in the final eight with a win. But Harvey started on the bench and Adelaide led 3.3 to 0.1 before St Kilda got into their stride late in the first quarter. After quarter-time Adelaide only kicked a further 3 goals to St Kilda's 12 to lose comprehensively (95–47). The loss meant Adelaide slipped to sixth on the ladder with a percentage of 109.60. To add to Adelaide's woes, Chris Knights was reported for making forceful front-on contact which he got off.<ref>[http://www.afl.com.au/News/NEWSARTICLE/tabid/208/Default.aspx?newsId=65962 Victorious Saints go seventh]</ref>

Adelaide knocked off the Western Bulldogs by 9 points (76–67) in wet conditions at AAMI Stadium. It was their 5th win in 6 games. The win moved Adelaide into 4th spot but St Kilda's big win over Essendon the next day dropped Adelaide down to 5th spot and into a very difficult home final vs Collingwood.<ref>[http://www.afl.com.au/News/NEWSARTICLE/tabid/208/Default.aspx?newsId=66473 Crows into fourth]</ref>

==2008 finals==
{{main|2008 AFL finals series}}

The 5th spot finish proved costly as the Crows were unable to take advantage of their poor home record vs Collingwood and were defeated in the first week finals by 31 points in front of their smallest home crowd of the season of just over 37,000 (with plenty of Collingwood in the crowd), despite perfect mid 20's weather on a Saturday Afternoon. Scott Stevens was brilliant and kicked 6 goals as Adelaide trailed for most of the game but threatened Collingwood with a final quarter comeback. But Collingwood pulled away with unselfish team play as Adelaide collapsed. Late in the game Scott Thompson kicked a goal out on the full drawing huge cheers from the Collingwood faithful. Nathan Bassett played his last game in the AFL and came out onto the ground after the game to have a kick with the fans.<ref>[http://www.afl.com.au/News/NEWSARTICLE/tabid/208/Default.aspx?newsId=67029 Pies raid Crows' nest]</ref>

==Statistics==
===Team===
{| class="wikitable" width="60%"
|-
| width="25%"| '''Highest score'''
|            |22.18 (150) vs {{AFL Mel}} in round 8
|-
| width="25%"| '''Lowest score'''
|            |5.17 (47) vs {{AFL WC}} in round 9, and 6.11 (47) vs {{AFL StK}} in round 21
|-
|}
{| class="wikitable" width="60%"
|-
| width="25%"| '''Highest ladder position'''
|            |4 after rounds 6–11
|-
| width="25%"| '''Lowest ladder position'''
|            |9 after rounds 1 and 16
|-
| width="25%"| '''Highest percentage'''
|            |139.89 after round 2
|-
| width="25%"| '''Lowest percentage'''
|            |97.62 after round 1
|-
|}
{| class="wikitable" width="60%"
|-
| width="25%"| '''Home W/L record'''
|            |9–2
|-
| width="25%"| '''Away W/L record'''
|            |4–7
|-
|}
{| class="wikitable" width="60%"
|-
| width="25%"| '''Final Score for'''
|            |287.295 (2017) – rank 13
|-
| width="25%"| '''Final Score against'''
|            |260.278 (1838) – rank 2
|-
|}

===Individual===

{| class="wikitable" width="60%"
|-
| width="25%"| '''Most games'''<ref name=aflstats>{{cite web|title=AFLStats |url=http://afltables.com/afl/stats/2008.html |work= |archiveurl=http://www.webcitation.org/5gwsVJcSB?url=http%3A%2F%2Fstats.rleague.com%2Fafl%2Fstats%2F2008.html |archivedate=2009-05-22 |deadurl=no |accessdate=2009-04-17 |df= }}</ref>
|            |23 – [[Nathan Bassett]], [[Nathan Bock]], [[Michael Doughty (Australian footballer)|Michael Doughty]], [[Tyson Edwards (footballer)|Tyson Edwards]], [[Simon Goodwin]], [[Scott Stevens]], [[Scott Thompson (footballer born 1983)|Scott Thompson]] and [[Nathan van Berlo]]
|-
| width="25%"| '''Most kicks'''<ref name=aflstats/>
|            | 335 – [[Nathan Bock]]; 306 – [[Tyson Edwards (footballer)|Tyson Edwards]]
|-
| width="25%"| '''Most handballs'''<ref name=aflstats/>
|            |264 – [[Michael Doughty (Australian footballer)|Michael Doughty]]; 254 – [[Scott Thompson (footballer born 1983)|Scott Thompson]]
|-
| width="25%"| '''Most disposals'''<ref name=aflstats/>
|            |556 – [[Scott Thompson (footballer born 1983)|Scott Thompson]]; 503 – [[Michael Doughty (Australian footballer)|Michael Doughty]]
|-
| width="25%"| '''Most hit-outs'''<ref name=aflstats/>
|            |274 – [[Ivan Maric]]; 202 – [[Jonathon Griffin]]
|-
| width="25%"| '''Most tackles'''<ref name=aflstats/>
|            |109 – [[Scott Thompson (footballer born 1983)|Scott Thompson]]; 84 – [[Robert Shirley]]
|-
| width="25%"| '''Most marks'''<ref name=aflstats/>
|            | 162 – [[Nathan Bock]]; 159 – [[Scott Stevens]]
|-
| width="25%"| '''Most goals'''<ref name=aflstats/>
|            |34 – [[Brett Burton]]; 33 – [[Simon Goodwin]]
|-
|}

==See also==
*[[List of Adelaide Football Club players#2008|Adelaide Crows 2008 Playing List]]

==References==
{{reflist}}

{{2008 AFL season}}
{{Adelaide Football Club}}

[[Category:Adelaide Football Club seasons]]
[[Category:2008 Australian Football League season|Adelaide Football Club]]