<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Nemere
 | image=Rotter Lajos-Nemere.jpg
 | caption=The Nemere after the [[Berlin]]-[[Kiel]] flight, on launch dolly
}}{{Infobox Aircraft Type
 | type=High performance [[sailplane]]
 | national origin=[[Hungary]]
 | manufacturer=Royal Hungarian Repair Works
 | designer=Lajos Rotter
 | first flight=25 July 1936
 | number built=1
 | developed from=[[Rotter Karakán]]
 | variants with their own articles=
}}
|}

The '''Rotter Nemere''' or just '''Nemere''' was a [[Hungary|Hungarian]] high performance, single seat [[sailplane]] designed and built for the 1936 [[Organisation Scientifique et Technique du Vol à Voile|ISTUS]] gliding demonstration held in 1936 alongside the [[Berlin]] [[Olympic Games]].

==Design and development==

The ISTUS international soaring demonstration was held at the same time and in the same place as the 1936 Olympics to make the case for gliding's inclusion as an Olympic discipline at later Games.  The proposition was accepted and there would have been gliding events at the 1940 Olympics had not [[World War II]] intervened.<ref name=SimonsIA/>  Rotter, using his experience of designing the successful [[Rotter Karakán|Karakán]], was responsible for both designing and flying the Nemere, Hungary's representative.<ref name=SimonsIB/>

The Nemere's progression from the Karakán was most evident in the wing and its mounting. The modified pedestal mounting to the fuselage had gone and instead the Nemere had a [[shoulder wing]] mounted on a {{convert|1150|mm|in|abbr=on|1}} span centre section built as part of the [[fuselage]]. The wing was a [[cantilever]] structure, without the earlier [[lift strut]]s, continuously tapered in plan from [[wing root|root]] to [[wing tip|tip]] with no externally distinct centre section<ref name=SimonsIB/> and with 2° of [[dihedral (aeronautics)|dihedral]].<ref name=Gabor/> There was continuous taper in wing section also; Rotter returned to Göttingen [[airfoil]]s using Gö 646 with a thickness to [[chord (aeronautics)|chord]] of 19% at the root, varying through Gö 535 to a thinner, less [[camber (aerodynamics)|cambered]], tip.<ref name=SimonsIB/>  Like the Karakán, the Nemere had a [[plywood]] covered D-box ahead of the main [[spar (aeronautics)|spar]] but, with the external struts absent, plywood covered more of the inner wing back to a diagonal internal drag strut. The wings were [[aircraft fabric covering|fabric covered]] aft. Broad chord [[aileron]]s occupied the outer 60% of the wings, which ended in [[elliptical]] tips.<ref name=SimonsIB/> 
 
The fuselage was a ply-covered [[semi-monocoque]], teardrop shaped in cross-section, which tapered markedly behind the wing.  The [[canopy (aircraft)|canopy]] was a wood framed multi-transparency unit, similar to that on the Karakán, but with more panels, which preserved the contours of the upper forward fuselage back almost to the wing [[leading edge]]. The tail was conventional, with an [[all-moving tailplane]], mostly fabric covered.  The [[rudder]], mounted on a short, narrow [[fin]], was [[balanced rudder|balanced]], rounded and full. The Nemere took off from a small, two wheeled dolly and landed on a long skid under the forward fuselage, assisted by s steel tailskid at the rear.<ref name=SimonsIB/>

The Nemere flew for the first time on 25 July 1936<ref name=Gabor/> only a few days before the demonstrations held at [[Berlin-Staaken]] airfield on 4 August.<ref name=Olymp/> Sailplanes from Austria, Bulgaria, Germany, Italy, Switzerland and Yugoslavia flew alongside the Nemere. A week later, starting from [[Rangsdorf]] some {{convert|15|mi|km|abbr=on|disp=flip|0}} south of Berlin, Rotter made a flight to [[Kiel]] where the sailing events of the Games were based.  He had nominated his objective the day before and covered the {{convert|326.5|km|mi|abbr=on|1)}} in 3 hrs 53 min. It was the longest glider flight in Europe in 1936 and won Rotter an ISTUS Gold medal.<ref name=SimonsIB/><ref name=Gabor/>

On 13 June 1937 the Nemere was damaged in a [[Gliding#Bungee launch|bungee-cord launch]] and was rebuilt with [[air brake (aeronautics)|Göppingen airbrakes]], the aileron joint adjusting lever, previously behind the pilot's head, moved under the instrument panel<ref name=Gabor/> and the rear canopy oval side-opening replaced with a rectangular aperture.<ref name=SimonsIB/>

The Nemere continued in use until at least 1943, mostly [[ridge lift|slope soaring]] near its base at [[Toros (village)|Toros]], west of [[Budapest]] A few long distance cross-country flights were made, including one of {{convert|230|km|mi|abbr=on|0}} from [[Hármashatárhegy]] in Budapest to [[Ciucea]] in west Romania, though no more records were set. Later in [[World War II]] it was slightly damaged but was restored to flying condition. In 1948 it was broken up.<ref name=Gabor/>

[[File:Nemere.svg|thumb]]

==Specifications==
{{Aircraft specs
|ref=Simons(2006) pp.194-5<ref name=SimonsIB/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=8.00
|length note=
|span m=20.00
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=23
|wing area note=
|aspect ratio=17.39
|airfoil=Göttingen 646 at root, Göttingen 535 immediately inboard of the ailerons and thinner and less cambered at the tip
|empty weight kg=340
|empty weight note=
|gross weight kg=440
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=26:1 at {{convert|75|km/h|mph kn|abbr=on|0}}<ref name=Gabor/>
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=0.63
|sink rate note=minimum at {{convert|55|km/h|mph kn|abbr=on|0}}<ref name=Gabor/>
|lift to drag=
|wing loading kg/m2=19.15
|wing loading note=

|more performance=
}}

==References==
{{reflist|refs=

<ref name=SimonsIA>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9806773 4 6|pages=128}}</ref>

<ref name=SimonsIB>{{cite book |last=Simons|title=Sailplanes 1920-1945|pages=194–5}}</ref>

<ref name=Gabor>{{cite book |title= Magyar vitorlázó repülögépek|last= Gabor|first=Jareb|year=1988|pages=59–60, 252, 254 |publisher=Müszaki Könuvkiadó|location=Budapest }}</ref>

<ref name=Olymp>{{cite web |url=http://library.la84.org/6oic/OfficialReports/1936/1936spart2.pdf|page=479|date= |title=Official Report of the 1936 Olympic Games |publisher= |accessdate=12 June 2015}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->
{{Lajos Rotter aircraft}}

[[Category:Hungarian sailplanes 1930–1939]]