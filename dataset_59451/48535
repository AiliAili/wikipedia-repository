{{multiple issues|
{{COI|date=November 2013}}
{{more footnotes|date=November 2013}}
}}

'''Mabel Ruth Hokin''' was a [[biochemist]] who spent most of her professional career conducting fundamental research in the [[University of Wisconsin Medical School]]. She is most well known for the work she did early in her career, along with then-husband Lowell Hokin, in the study of stimulated [[phosphoinositide]] turnover in secretory tissues, a key component of transmembrane signaling and many other cell regulatory processes which became known as the 'PI Effect'.<ref name="Putney">
{{Cite journal
 | author = JW Putney Jr
 | year = 1981
 | title = Recent hypotheses regarding the phosphatidylinositol effect
 | journal = Life Sciences
 | volume = 29
 | pages = 1183–1194
 | url = http://www.sciencedirect.com/science/article/pii/0024320581902216
 | pmid = 7029181
 | doi=10.1016/0024-3205(81)90221-6
 }}
</ref>

{{Infobox scientist
| name              = Mabel R. Hokin
| native_name       = 
| native_name_lang  = 
| image             = Mabel_R._Hokin_-_portrait.jpg
| image_size        = 
| alt               = 
| caption           = 
| birth_date        = {{Birth date|1924|02|09}}
| birth_place       = Sheffield, England, UK
| death_date        = {{Death date and age|2003|08|17|1924|02|09}}
| death_place       = Madison, Wisconsin, USA
| resting_place     = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline,title}} -->
| other_names       = Mabel Ruth Neaverson, Mabel Davison, Mabel Hokin-Neaverson (post-1971 publications)
| residence         = Madison, Wisconsin, USA
| citizenship       = British
| nationality       = British
| fields            = Biochemistry, Neurochemistry
| workplaces        = McGill University, University of Wisconsin
| alma_mater        = University of Sheffield
| thesis_title      = 
| thesis_url        = 
| thesis_year       = 
| doctoral_advisor  = [[Quentin Gibson]]
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = Discovery of the phosphoinositide effect
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = 
| signature         = <!--(filename only)-->
| signature_alt     = 
| website           = <!-- {{URL|www.example.com}} -->
| footnotes         = 
| spouse            = Dennis Davison (m. 1946; d. 1952), Lowell Hokin (m. 1953; d. 1971; three children), Bernard Biales (m. 2003){{citation needed|date=November 2013}}
| children          = 
}}

== Early life ==
Mabel Ruth Neaverson was born to working-class parents in the Heeley district of [[Sheffield, England]] in early 1924. She had two younger sisters, Mary and Dorothy. Mabel took to academic pursuits at an early age, excelling in elementary and grammar school as well as at bible scholarship. She spent 1942 to 1943 working in the [[Women's Land Army]], where, among other things, she helped administer a refugee camp for Czech Jews. (Mabel converted to Judaism ten years later.){{citation needed|date=November 2013}}

She also became interested in costume design and theater, where she met her first husband, actor and playwright [http://www.austlit.edu.au/austlit/page/A4039 Dennis Davison].

Mabel worked as a technician in the [[Medical Research Council (United Kingdom)|Medical Research Council]] Unit for Research in Cell Metabolism under [[Hans Adolf Krebs|Hans Krebs]] at the [[University of Sheffield]] from 1943 to 1946, and enrolled as a student in 1946, continuing to work for Krebs. Krebs recognized Mabel's scientific mind and talent in the laboratory, and urged her to pursue doctoral work, which she began in 1949 after receiving her Bachelor of Science Honors degree in Physiology. She did her graduate research under [[Quentin Gibson]] in the Physiology department. In a short time, Mabel met Krebs' graduate student Lowell Hokin, and the two began a romantic as well as professional relationship. Mabel [https://commons.wikimedia.org/wiki/File:Mabel-hokin-sheffield-record-card-1.jpg received her Ph.D.] in 1952.

== Scientific career ==
Mabel and Lowell Hokin conducted research in fundamental biochemistry together from their doctoral research days in Sheffield, to their work at [[McGill University]] in the 1950s, up to the mid-1960s at the [[University of Wisconsin]]. From the mid-1960s, Mabel worked in her own areas of research with a particular focus on neurochemistry after she received a primary appointment in the Department of Psychiatry along with a joint appointment in the Department of Physiological Chemistry in the [[University of Wisconsin Medical School]].

Mabel and Lowell's most significant<ref name="Biochemist obit">
{{Cite journal
 | author = Bob Michell
 | year = 2003
 | title = Obituary: Mabel R. Hokin
 | journal = The Biochemist
 | volume = 25
 | pages = 62–63
 | url = http://www.biochemist.org/bio/02506/0062/025060062.pdf
 }}
</ref>
work came very early in their careers, and was first published in their seminal 1953 paper<ref>
{{Cite journal
 |author1=Mabel R. Hokin  |author2=Lowell E. Hokin
  |lastauthoramp=yes | year = 1953
 | title = Enzyme secretion and the incorporation of P<sup>32</sup> into phospholipides of pancreas slices
 | journal = Journal of Biological Chemistry
 | volume = 203
 | pages = 967–977
 | pmid = 13084667
 | issue = 2
 }}
</ref>
in the [[Journal of Biological Chemistry]]. In it, Mabel and Lowell described experiments in which they stimulated enzyme secretion in slices of pigeon pancreas in the presence of media containing the radioisotope P<sup>32</sup>. They found that the [[phospholipids|phospholipid]] fraction from the stimulated slices, formerly thought to contain fairly inert structural components of cell membranes, contained up to 9 times as much P<sup>32</sup> as it did in the non-stimulated control samples. In a 1955 paper<ref>
{{Cite journal
|author1=L.E. Hokin  |author2=M.R. Hokin
 |lastauthoramp=yes | title=Effects of acetylcholine on the turnover of phosphoryl units in individual phospholipids of pancreas slices and brain cortex slices
| journal=Biochimica et Biophysica Acta
| year=1955
| volume=18
| pages=102–110
| doi=10.1016/0006-3002(55)90013-5
| pmid=13260248
| issue=1
}}
</ref> 
Lowell and Mabel showed that the bulk of the P<sup>32</sup> went into [[Phosphatidic acid|phosphatidate]] and 'a phosphoinositide' that was later identified as [[phosphatidylinositol]] (PtdIns). They then showed that this metabolic response, which became known as the 'PI effect', occurred in a variety of stimulated tissues, such as pigeon pancreas (1958),<ref>
{{Cite journal
 |author1=Lowell E. Hokin  |author2=Mabel R. Hokin
  |lastauthoramp=yes | year = 1958
 | title = Phosphoinositides and Protein Secretion in Pancreas Slices
 | journal = Journal of Biological Chemistry
 | volume = 233
 | pages = 805–810
 | pmid = 13587496
 | issue = 4
 }}
</ref>
suggesting that it plays a widespread role in cell regulation. Mabel and Lowell summarized their cell membrane biochemistry research in a 1965 article<ref>
{{Cite journal
 |author1=Lowell E. Hokin  |author2=Mabel R. Hokin
  |lastauthoramp=yes | year = 1965
 | title = The Chemistry of Cell Membranes
 | journal = Scientific American
 | volume = 213
 | pages = 78–86
 | doi = 10.1038/scientificamerican1065-78
 | pmid = 5825732
 | issue = 4
 }}
</ref>
in [[Scientific American]]. In 1974, Mabel provided some of the first experimental evidence that launched the modern phase of work in this area<ref>
{{Cite journal
 | author = M Hokin-Neaverson
 | year = 1974
 | title = Acetylcholine causes a net decrease in phosphatidylinositol and a net increase in phosphatidic acid in mouse pancreas
 | journal = Biochemical and Biophysical Research Communications
 | volume = 58
 | pages = 763–768
 | pmid = 4365414
 | doi = 10.1016/S0006-291X(74)80483-3
 | issue = 3
 }}
</ref>
(she published as M. Hokin-Neaverson after her 1971 divorce from Lowell).

In the 1970s, Mabel became interested in the biochemistry of the brain, or neurochemistry, and, as a full professor, led the Laboratory for Neurochemistry Research in the Department of Psychiatry in the [[University of Wisconsin Medical School]], which was conveniently located in the same building as the physiological chemistry, pharmacology, and anatomy departments, where her colleagues worked and from which she drew graduate students. She delved into undergraduate teaching at one point, teaching a course on drugs and the mind in the early 1970s, but her primary teaching activity was guiding graduate students and teaching graduate biochemistry courses. Unlike her earlier research, which exclusively employed animal tissues, Mabel also studied blood samples from humans, including mentally ill patients, and contributed to the understanding of the biochemical basis of mental illness, showing the first biochemical marker for one of the major psychoses.<ref>
{{Cite journal
 |vauthors=Hokin-Neaverson M, Spiegel DA, Lewis WC |date=Nov 1974
 | title = Deficiency of erythrocyte sodium pump activity in bipolar manic-depressive psychosis
 | journal = Life Sciences
 | volume = 15
 | pages = 1739–1748
 | pmid = 4620986
 | doi = 10.1016/0024-3205(74)90175-1
 | issue = 10
 }}
</ref> Mabel's assistant, Ken Sadeghian, worked with her from the 1960s until her retirement.

Interest in the PI effect waned in the 1960s and 1970s, but experienced a resurgence in the 1980s<ref name="Putney" /> due to advances in technology and understanding of cell membrane biochemistry. Mabel and Lowell became regarded<ref name="Biochemist obit" /> as founders of an important and still growing field of biochemistry and cell biology — the many roles of [[inositol]] [[phospholipids]] in cell function — for which they were honored at a 1996 symposium at the University of Wisconsin that gathered their colleagues from around the world. Their early work was highlighted in a 20th anniversary review of the discovery of inositol-1,4,5-trisphosphate as a second messenger,<ref>
{{Cite journal
 | author = Robin F. Irvine
 |date=July 2003
 | title = 20 years of Ins(1,4,5)P<sub>3</sub>, and 40 years before
 | journal = Nature Reviews Molecular Cell Biology
 | volume = 4
 | pages = 586–590
 | pmid = 12838341
 | issn = 1471-0072
 | issue = 7
 | doi = 10.1038/nrm1152
 }}
</ref>
and their 1953 and 1958 
[[Journal of Biological Chemistry|JBC]] articles were celebrated as "JBC Classics" in 2005.<ref>
{{Cite journal
 | author = Nicole Kresge, Robert D. Simoni and Robert L. Hill
 | year = 2005
 | title = A Role for Phosphoinositides in Signaling: the Work of Mabel R. Hokin and Lowell E. Hokin
 | journal = Journal of Biological Chemistry
 | volume = 280
 | pages = e27
 }}
</ref>

== Personal life ==

Most of Mabel's life was spent under a shadow cast by autoimmune illness. At the age of 16 she was diagnosed with [[lupus]] and given a dire prognosis for longevity. It turned out, however, to be a nonfatal [[autoimmunity|autoimmune]] [[connective tissue disease]] with which Mabel struggled for the rest of her life. In the mid-1960s, treatment with [[steroids]] became established, and Mabel's steroid treatment led to degradation of her hip joints. She was an early recipient of artificial hips at [[Mayo Clinic]] in 1972.{{citation needed|date=November 2013}}

Mabel and Lowell had hoped to move to the United States after finishing their doctoral work in Sheffield, but Mabel's earlier activities as [https://commons.wikimedia.org/wiki/File:Mabel-hokin-sheffield-record-card-1.jpg chair] of the Sheffield University Socialist Society and member, along with then-husband [http://www.austlit.edu.au/austlit/page/A4039 Dennis Davison], of the [[Communist Party of Great Britain]], led to her being blacklisted from entry into the United States during the [[McCarthyism|McCarthy Era]].{{citation needed|date=November 2013}} Instead, Mabel and Lowell obtained positions at the Montreal General Hospital Research Institute at [[McGill University]] in 1953. While conducting research at McGill they petitioned the United States government to allow Mabel entry into the U.S., but it was not until 1957 that they were able to move to their new jobs in Wisconsin. From their arrival in Montreal in 1953 to their departure in 1957, Mabel was unable to attend any research conferences in the United States.{{citation needed|date=November 2013}}

[[University of Wisconsin|Wisconsin]] was chosen for several reasons: Lowell received a junior faculty appointment in the Department of Physiological Chemistry, Mabel obtained a position in the department that allowed her to continue her research with Lowell, and Lowell's parents lived close by in Peoria, Illinois.

Mabel had three children. In 1966 the family moved to a home on [[Lake Mendota]], where Mabel lived for the rest of her life.{{citation needed|date=November 2013}}

Mabel and Lowell were divorced in 1971. In the mid-1980s, Mabel and Bernard Biales, who had worked in her lab at one time, began a relationship that lasted until her death.

Mabel Hokin died in Madison on August 17, 2003.<ref name="Biochemist obit" /><ref>
{{Citation 
 | year = 2003
 | title = Mabel R. Hokin dies
 | url = http://www.news.wisc.edu/8831
 }}
</ref>

== References ==

{{reflist}}

{{DEFAULTSORT:Hokin, Mabel}}
[[Category:1924 births]]
[[Category:2003 deaths]]
[[Category:People from Sheffield]]
[[Category:English biochemists]]
[[Category:Neurochemists]]
[[Category:Women biochemists]]
[[Category:American biochemists]]
[[Category:20th-century women scientists]]
[[Category:20th-century American scientists]]