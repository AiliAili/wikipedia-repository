{{Use dmy dates|date=June 2013}}
{{Infobox person
| name   = Elly Beinhorn
| image  = Bundesarchiv Bild 183-2007-1205-500, Bernd Rosemeyer, Elly Beinhorn, Ferdinand Porsche.jpg
| caption  = Elly Beinhorn (center) with [[Bernd Rosemeyer]] and [[Ferdinand Porsche]]
| birth_date  = {{Birth date|1907|5|30|df=yes}}
| birth_place =  [[Hanover]], Germany
| death_date  = {{Death date and age|2007|11|28|1907|5|30|df=yes}}
| death_place =  [[Ottobrunn]], Germany
| occupation  = Pilot
| spouse         = [[Bernd Rosemeyer]] (1909&ndash;1938)<br>(m. 1936&ndash;1938)<br> (his death)

}}

'''Elly Beinhorn-Rosemeyer''' (30 May 1907 &ndash; 28 November 2007) was a [[Germans|German]] [[aviator|pilot]].

==Life==

===Early life===
She was born in [[Hannover]], Germany on 30 May 1907.<ref>[http://www.fembio.org/english/biography.php/woman/print_bio/elly-beinhorn birthdate, location]</ref>

In 1928, she attended a lecture by famed aviator [[Hermann Köhl]], who had recently completed a historic East-West [[Atlantic]] crossing. This lecture is described as the spark that ignited her interest in aviation.<ref>[http://www.aviation-history.com/airmen/junkers.htm Köhl's transatlantic crossing]</ref><ref>[http://www.fembio.org/english/biography.php/woman/print_bio/elly-beinhorn lecture inspiration]</ref>

At just 21 years old, with funds from a small inheritance (against the wishes of her parents) she  moved to [[Spandau]] in Berlin where she took flying lessons, at [[Staaken|Berlin-Staaken]] airport, under the tutelage of instructor Otto Thomsen. She  soon  made her solo flight in a small [[Klemm|Klemm KL-20]]. With her money  running out,  it was suggested that she give aerobatic displays on the  weekends. She found this financially rewarding, but personally unsatisfying.<ref>[http://www.ddavid.com/formula1/people.htm learns to fly in Berlin]</ref><ref>[http://soloflights.org/elly_text_e.html taught by Thomsen]</ref><ref>[http://soloflights.org/elly_text_e.html Klemm KL-20]</ref><ref>[http://wairarapa.co.nz/times-age/weekly/2001/beinhorn.html weekend aerobatic displays]</ref>

===Long-distance flights===
[[File:Bundesarchiv Bild 102-01227, Elly Beinhorn.jpg|thumb|right|Elly Beinhorn 1933]]
Long distance flying was her real passion and in 1931 she seized the opportunity to fly to Portuguese Guinea (now [[Guinea-Bissau]]) [[West Africa]] on a scientific expedition. On the return journey, engine failure resulted in a crash-landing in the [[Sahara]]. With the help of nomadic [[Tuareg people|Tuareg]] tribesmen, Elly joined a camel caravan to [[Timbuktu]]. She subsequently returned to the crash site to recover parts of the plane.<ref>According to Halliburton, she met Moye in Timbuktu and he helped her to fix the plane and they flew together to meet Richard Halliburton. ''The Flying Carpet'', Garden City: Garden City Pub. Co., 1932, pp. 255-56. He had high praise for her: "there were very few men pilots who could compare with her. That flight of hers all alone from Berlin to Timbuctoo, over a route nearly twice as long as our own, was one that demanded, for a solitary woman, extraordinary courage." p. 256.</ref> Word of her plight reached the French authorities and they sent a military two-seater plane to collect her.<ref>[http://soloflights.org/elly_text_e.html flight to West Africa]</ref><ref>[http://wairarapa.co.nz/times-age/weekly/2001/beinhorn.html engine failure over Sahara]</ref>

In April 1931, fully recovered, she was able to fly herself back to Berlin to a warm reception from the crowds.<ref>[http://wairarapa.co.nz/times-age/weekly/2001/beinhorn.html return to Berlin]</ref>

Soon after this, she embarked on another flight, her Klemm monoplane developing mechanical problems near [[Bushire]], [[Iran|Persia]]. She found Moye Stephens, another pilot, in Bushire, who helped her fix the problem on her Klemm. Stephens and travel-adventure writer Richard Halliburton were flying around the world in a [[Stearman C3|Stearman C-3B]] biplane, they called the [[Flying Carpet]]. She accompanied them on part of their flight, including the trip to [[Mount Everest]].  She flew on to [[Bali]] - and eventually Australia. In the process, she became only the second woman to fly solo from Europe to Australia, after [[Amy Johnson]].<ref>[http://wairarapa.co.nz/times-age/weekly/2001/beinhorn.html Bali and Oz]</ref> The foreword of her book,  ''Flying Girl'' (1935), was written by [[Richard Halliburton]] (whose English publisher, as hers, was Geoffrey Bles); it includes a photo of [[Moye Stephens]] repairing her plane.   Barbara H. Schultz' ''Flying Carpets, Flying Wings - The Biography of Moye Stephens'' (2011) contains Stephens' own account of their meeting which was first introduced in Halliburton's bestselling ''The Flying Carpet'' (1932).

Having landed in [[Darwin, Northern Territory|Darwin]], [[North Australia]], she headed down to Sydney, arriving in March 1932. Her plane was dismantled and shipped to New Zealand, then Panama where it was reassembled. There Elly resumed her flight, following the western coast of [[South America]]. She was presented with a medal in Peru. An ill-advised trip across the [[Andes]] followed. The plane was dismantled once more in Brazil and shipped to Germany. Elly arrived in Berlin in June 1932.<ref>[http://soloflights.org/elly_text_e.html Oz to Berlin]</ref>

Now famous but in debt to the tune of 15,000 marks or more, she was pleasantly surprised to be awarded the [[Hindenburg Cup]], 10,000 marks and several other monetary awards from the German aeronautical industry which enabled her to continue her career. She also continued to write articles and sell photographs of her travels to raise funds.<ref>[http://soloflights.org/elly_text_e.html famous but in debt, awards]</ref><ref>[http://wairarapa.co.nz/times-age/weekly/2001/beinhorn.html articles & photos sold to raise money]</ref>

Free of debt, she took off for Africa using a [[Heinkel He 71]], flying down the east coast, then back up the west coast.<ref>[http://soloflights.org/elly_text_e.html Africa E & W]</ref>

The following year, Elly shipped the plane to Panama, then flew through Mexico and California before crossing the United States to Washington DC and Miami. Elly and the plane returned to Germany by ship, arriving in January 1935. She was now a true German heroine.<ref>[http://soloflights.org/elly_text_e.html Panama to Germany]</ref>

===Bernd Rosemeyer===
[[File:Bundesarchiv Bild 146-2007-0212, Heirat Elly Beinhorn, Bernd Rosemeyer.jpg|thumb|right|Elly Beinhorn and [[Bernd Rosemeyer]] at their wedding]]
On 29 September 1935 Elly attended the [[Czechoslovakian Grand Prix]], held in the town of [[Brno]] in [[Czechoslovakia]], at the invitation of [[Auto Union]] (she happened to be in the country on a lecture tour, by now a regular source of income). She congratulated the winner, [[Bernd Rosemeyer]], who seemed smitten with her. They danced together that night and were married on 13 July 1936. A true celebrity couple – an adventurous aviator and the fearless racing driver – they were the toast of [[Nazi Germany]]. [[Heinrich Himmler]] ordered a reluctant Bernd to become a member of the [[Schutzstaffel|SS]].<ref>[http://www.ddavid.com/formula1/people.htm lectures as source of income]</ref><ref>[http://www.kolumbus.fi/leif.snellman/elly100.htm Grand Prix, Bernd, marriage, SS, reluctant Nazis]</ref>

They had a son, Bernd Jr., in November 1937, but just ten weeks after his birth his father was killed while attempting a speed record in his [[Auto Union]] Streamliner. As a national hero he was mourned by much of Germany. Elly received condolences from prominent Nazis, including [[Adolf Hitler]], but requested a simple, non-political funeral ceremony. These wishes were ignored and several Nazis gave speeches at the graveside. Some accounts suggest that Elly walked off in protest at the Nazis claiming Bernd as their own and taking over what was a personal occasion.<ref>[http://www.kolumbus.fi/leif.snellman/elly100.htm death of Bernd]</ref>

===Second marriage and post-war life===
In 1941 Elly married Dr. Karl Wittman and they had a daughter, Stephanie.<ref>[http://soloflights.org/elly_text_e.html second marriage, birth of Stephanie]</ref>

After World War II she briefly took up gliding due to the ban on powered flight in Germany. But she soon moved to Switzerland to continue flying planes.<ref>[http://soloflights.org/elly_text_e.html gliders and Switzerland]</ref>

In 1979, at the age of 72, she surrendered her pilot's licence.

===Later years and death===
In her later years, Rosemeyer lived in [[Ottobrunn]], [[Bavaria]], near [[Munich]]. Her son, Dr. Bernd Rosemeyer, lives in the same area and has enjoyed a successful career as an orthopaedist.<ref>[http://www.stuttgarter-zeitung.de/stz/page/detail.php/1434867?_skip=1 lives in Ottobrunn]</ref> He married Countess Michaela von Castell-Ruedenhausen, who died 8 August 2011, and they have two children.
{{Wikinews|German pilot Elly Beinhorn dies, aged 100}}
Elly Beinhorn died on 28 November 2007, at the age of 100.<ref>[http://www.motor-klassik.de/aktuell/panorama/hxcms_article_508750_14702.hbs Elly Beinhorn gestorben]</ref>

== Publications ==
*Alt, John H. ''Don't Die in Bed: The Brief, Intense Life of Richard Halliburton''.  Atlanta: Quincunx Press, 2013. Chapters on Elly Beinorn.
* Chris Nixon & Elly Beinhorn-Rosemeyer: ''"Rosemeyer!"'', Transport Bookman Publications 1989, ISBN 0-85184-046-9
*Beinhorn, Elly, ''Flying Girl'' (Geoffrey Bles, London, 1935).
*Halliburton, Richard, ''The Flying Carpet'' (Bobbs-Merrill, Indianapolis and New York, 1932).
*Max, Gerry, ''Horizon Chasers - The Adventures of Richard Halliburton and Paul Mooney'' (McFarland Publishers, Inc., Jefferson, North Carolina, 2007).
*Schultz, Barbara H., ''Flying Carpets, Flying Wings - The Biography of Moye Stephens'' (PlaneMercantile, 2011).
* Frilling, Christoph: ''Die Pilotin und der Rennfahrer – Elly Beinhorn und Bernd Rosemeyer auf Gratwanderung im Nationalsozialismus.'' Verlag W. Dietrich, Reinhardtsgrimma 2009, ISBN 978-3-933500-10-6.
* [[Laurence Arthur Rickels]]: ''Into Africa.'' In: ''Nazi Psychoanalysis - Vol. 2.'' University of Minnesota Press, Minneapolis 2002, ISBN 978-0-8166-3698-3, p. 82–87.
* Commire, Anne: ''Beinhorn, Elly (1907–).'' In: ''Women in World History: A Biographical Encyclopedia.'' Gale, 2000, ISBN 978-0-7876-4061-3.
* Dimitrova-Moeck, Svoboda: ''Women travel abroad 1925-1932 : Maria Leitner, Erika Mann, Marieluise Fleisser, and Elly Beinhorn : women's travel writing from the Weimar Republic.'' Weidler, Berlin 2009, ISBN 978-3-89693-534-2, p. 209–243.

==References==
{{Reflist}}

==External links==
{{Commons category}}
*{{YouTube|aRB1bAyIatY|Elly Beinhorn video-biography excerpt}}
*[http://www.ctie.monash.edu.au/hargrave/beinhorn.html Monash University biography page]
*[http://blogs.slq.qld.gov.au/jol/2008/08/29/elly-beinhorn-pioneer-german-aviatrix/ Elly Beinhorn - pioneer German aviatrix]- John Oxley Library Blog, State Library of Queensland

{{Authority control}}

{{DEFAULTSORT:Beinhorn, Elly}}
[[Category:1907 births]]
[[Category:2007 deaths]]
[[Category:People from Hanover]]
[[Category:People from the Province of Hanover]]
[[Category:German centenarians]]
[[Category:Officers Crosses of the Order of Merit of the Federal Republic of Germany]]
[[Category:German female aviators]]