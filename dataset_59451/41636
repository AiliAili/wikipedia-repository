{{EngvarB|date=May 2016}}
{{Infobox military unit
| unit_name = Combeforce
| native_name =
| image = [[File:AfricaMap1.jpg|300px]]
| caption = British pursuit 9 December 1940 – 7 February 1941
| dates = February 1941
| country = Britain
| countries =
| allegiance =
| branch = Army
| type =
| role = Flying column
| size =
<!-- Commanders -->
| notable_commanders =Lieutenant-Colonel [[John Frederick Boyce Combe|J. F. B. Combe]]
}}
{{Campaignbox Western Desert}}

'''Combeforce''' or '''Combe Force''' (Lieutenant-Colonel [[John Frederick Boyce Combe|J. F. B. Combe]]), was an ''[[ad hoc]]'' [[flying column]] formed from parts of the [[7th Armoured Division (United Kingdom)|7th Armoured Division]] (Major-General Sir [[Michael O'Moore Creagh]]) of the [[Western Desert Force]]. The rapid British advance during [[Operation Compass]] (9 December 1940 – 9 February 1941) forced the Italian [[Tenth Army (Italy)|10th Army]] to evacuate [[Cyrenaica]], the eastern province of [[Libya]]. In late January, the British learned that the Italians were retreating along the ''[[Litoranea Balbo]]'' (''Via Balbia'') from Benghazi. The 7th Armoured Division was dispatched to intercept the remnants of the 10th Army, by moving through the desert, south of the [[Jebel Akhdar, Libya|Jebel Akhdar]] (Green Mountain) via [[Msus]] and [[Antelat]], as the [[Australian 6th Division|6th Australian Division]] pursued the Italians along the coast road, north of the jebel. The terrain was hard going for the British tanks and Combeforce, with the wheeled vehicles of the 7th Armoured Division, was sent ahead across the chord of the jebel.

Late on 5 February, Combeforce arrived at the ''Via Balbia'' south of [[Benghazi]] and set up road blocks near Sidi Saleh, about {{convert|30|mi|km}} south-west of Antelat and {{convert|20|mi|km}} north of [[Ajdabia|Agedabia]]. The leading elements of the 10th Army arrived thirty minutes later and were ambushed. Next day the Italians attacked to break through the road block and continued to attack into 7 February. With British reinforcements arriving and the Australians pressing down the road from Benghazi, the 10th Army surrendered later that day. From Benghazi to Agedabia, the British took {{nowrap|25,000 prisoners,}} captured {{nowrap|107 tanks}} and {{nowrap|93 guns.}} Having succeeded in its objectives, Combeforce was disbanded.

==Background==

===Operation Compass===
{{main|Operation Compass}}
In early December 1940, the British [[Western Desert Force]] began Operation Compass, a raid against the [[Kingdom of Italy (1861-1946)|Italian]] [[Tenth Army (Italy)|10th Army]] (General [[Giuseppe Tellera]]), which had conducted the [[Italian invasion of Egypt]] in September 1940. The Italians had advanced to [[Sidi Barrani]] and established defensive positions in a line of fortified camps, which were overrun during the raid. Sidi Barrani was captured and the scope of Compass was extended and the Italian garrisons in [[Bardia]] and then [[Tobruk]] were isolated and captured. The 10th Army attempted to establish a defensive line at Derna east of the [[Jebel Akhdar (Libya)|Jebel Akhdar]] mountains, with XX Corps (Lieutenant-General Annibale Bergonzoli), comprising the [[60th Infantry Division Sabratha|60th Infantry Division ''Sabratha'']] and the [[Babini Group]], an improvised armoured brigade, which had lost some of its tanks in Tobruk. The Babini Group, had all of the [[Fiat M13/40]] medium tanks in Libya and held the south-west end of the defensive front near [[Mechili]], the junction of several caravan routes, to block another British outflanking move.{{sfn|Walker|2003|p=64}}

===Derna–Mechili===
On 22 January, the British advanced towards Derna, with the 19th Australian Brigade of the 6th Australian Division and sent another Australian brigade to reinforce the [[4th Mechanised Brigade (United Kingdom)|4th Armoured Brigade]] of the 7th Armoured Division south of the Jebel Akhdar, for an advance on Mechili.{{sfn|Macksey|1972|pp=121–123}}{{sfn|Playfair|Stitt|Molony|Toomer|1954|pp=353}} On 23 January, Tellera ordered a counter-attack against the British as they approached Mechili, to avoid an envelopment of XX Corps from the south but communication within the Babini Group was slow because only the tanks of senior commanders had wireless (the other crews were reliant on flag signals limited to ''halt'', ''forward'', ''backwards'', ''right'', ''left'', ''slow down'' and ''speed up''.){{sfn|Parri|nd|nopp=y}} Next day, the Babini Group with {{nowrap|10–15 M13/40s}} attacked the [[7th Queen's Own Hussars|7th Hussars]] of the 4th Armoured Brigade which was heading west to cut the Derna–Mechili track north of Mechili. The Italians lost seven M13s by {{nowrap|11:30 a.m.,}} for the loss of one cruiser and six light tanks.{{sfn|Long|1952|p=242}}{{sfn|Macksey|1972|p=123}}

The 4th Armoured Brigade was ordered to encircle Mechili and cut the western and north-western exits, while the 7th Armoured Brigade cut the road from Mechili to Slonta but the Babini Group had already retired into the hills. On 26 January, Graziani ordered Tellera to continue the defence of Derna and to use the Babini Group to stop an advance west from Mechili–Derna.{{sfn|Macksey|1972|pp=124–127}} During 27 January, a column of [[Universal Carrier|Bren Gun Carriers]] of the 6th Australian Cavalry Regiment was sent south from the Derna area to reconnoitre the area where the Italian tanks had been reported and was ambushed by part of the Babini Group with concealed anti-tank guns and machine guns; four Australians were killed and three taken prisoner. The 11th Hussars found a gap at Chaulan south of Wadi Derna and the Italians disengaged on the night of {{nowrap|28/29 January}} and rearguards of the Babini Group cratered roads, planted mines and booby-traps and managed to conduct several skilful ambushes, which slowed the British pursuit.{{sfn|Long|1952|pp=250–253, 255–256}}

==Prelude==

===Pursuit from Mechili===
[[File:Tobruk Agedabia road2.jpg|thumb|{{centre|Tobruk–Agedabia, 1940–1941}}]]
At dawn on 4 February, the 11th Hussars left Mechili, along a route towards Beda Fomm, which had not been reconnoitred by ground forces to avoid alerting the Italians. The vehicles were loaded to capacity with supplies fuel and ammunition, the ration of drinking water cut to about a glass a day and halts for food and rest were cut by half. Low-flying air reconnaissance had reported that the going was difficult and for the first {{convert|50|mi|km}} the route was the worst yet encountered. The journey began in high winds and bitter cold and by the time the tail end moved off the winds had risen to gale force. The head of the column drove into windblown sand which cut visibility to nil, while at the tail, drivers and vehicle commanders standing up reading compasses, were hit by frozen rain.{{sfn|Moorehead|1944|p=111}} By {{nowrap|3:00 p.m.}} armoured cars had reached Msus, {{convert|94|mi|km}} away, where the garrison left hurriedly and some cars followed up another {{convert|30|mi|km}} to Antelat.{{sfn|Playfair|Stitt|Molony|Toomer|1954|pp=357–358}}

===Combeforce===

[[File:O'Connor Captured.jpg|thumb|{{centre|Photograph of Brigadier John Coombe (left) with Lieutenant-General [[Philip Neame]] (centre), Lieutenant-General [[Richard O'Connor]] (centre, middle distance), Major-General [[Richard Gambier-Parry]] (right) following their capture by the Germans, 6 April 1941}}]]
By dawn on 5 February, the tracked vehicles and the rest of the 7th Armoured Division had reached Msus. Creagh had been informed by air reconnaissance that the 10th Army was retreating and [[Richard O'Connor]] ordered that the advance be pressed to cut off the Italian retreat. Creagh decided to send the wheeled vehicles on ahead, to block the ''Via Balbia'' between Benghazi and Agedabia as quickly as possible and to follow on to the south-west with the tracked vehicles, rather than westwards to Soluch.{{sfn|Playfair|Stitt|Molony|Toomer|1954|pp=357–358}} Combeforce (Lieutenant-Colonel [[John Frederick Boyce Combe|J. F. B. Combe]]) consisted of an armoured car squadron from each of the 11th Hussars and [[King's Dragoon Guards]], the 2nd Battalion [[The Rifle Brigade]], an RAF [[Armored car (military)|armoured car]] squadron, six [[Ordnance QF 25-pounder|25-pounder]] field guns of [[C Battery, Royal Horse Artillery|C Battery]] [[4th Regiment, Royal Horse Artillery|4th Royal Horse Artillery]] (4th RHA) and the 106th ([[Lancashire Hussars]]) Battery RHA, with nine [[Bofors 37 mm]] anti-tank guns [[portee|portée]] (carried on the back of a lorry, capable of being fired), a total of about {{nowrap|2,000 men.}}{{sfn|Playfair|Stitt|Molony|Toomer|1954|p=358}}{{sfn|Macksey|1972|p=135}}

===Drive for Beda Fomm===
Combeforce reached Antelat during the morning and by {{nowrap|12:30 p.m.}} had observers overlooking the ''Via Balbia'' west of Beda Fomm and Sidi Saleh, about {{convert|48|km|mi}} south-west of Antelat and {{convert|32|km|mi}} north of Ajedabia, with the rest of Combeforce following on. An Italian convoy drove up about thirty minutes later, ran onto mines and was then engaged by the artillery, anti-tank guns and armoured cars, which threw the column into confusion. Some members of the 10th ''Bersaglieri'' tried to advance down the road and others looked for gaps in the ambush on either side of the road.{{sfn|Macksey|1972|pp=137, 139}}

==Battle==
{{main|Battle of Beda Fomm}}
The ''Bersaglieri'' had little effect, being unsupported by artillery, most of which was with the rearguard to the north. The attempts by the Italians to break through became stronger and in the afternoon, the 2nd Rifle Brigade crossed the ''Via Balbia'' into the dunes, to block the route south between the road and the sea. Combe also brought up a company behind the roadblock, placed some 25-pounders behind the infantry and kept some armoured cars manoeuvring in the desert to the east, to deter an Italian outflanking move. Several hundred prisoners were taken but only a platoon of infantry could be spared to guard them. The vanguard of the Italian retreat had no tanks, contained few front-line infantry and had been trapped by the ambush which forced them to fight where they stood.{{sfn|Playfair|Stitt|Molony|Toomer|1954|p=358}}{{sfn|Macksey|1972|pp=137, 139}}

While waiting for the 4th Armoured Brigade, Combe reconnoitred to the north and near a small white mosque found several long, low, north-south ridges with folds between, in which tanks could hide from the road as they moved back and forth to fire at close range. The brigade set off from Msus at {{nowrap|7:30 a.m.}} but the journey was delayed by moving in single-file through a field of Thermos bombs and the brigade took until {{nowrap|4:00 p.m.}} to cover the {{convert|40|mi|km}} to Antelat, where they came into the range of Combforce wireless transmissions. Combe briefed Caunter to head for the mosque north of the roadblock and then attack all along the Italian column, to reduce the pressure on Combeforce. Caunter ordered the 7th Hussars and the artillery at full speed to the ''Via Balbia'' followed by the 2nd RTR in their slower tanks and the 3rd Hussars were sent north-east, to cut the routes from Soluch and Sceleidima. The brigade moved westwards on hard, flat sand, raising clouds of dust and soon reached the ''Via Balbia''.{{sfn|Macksey|1972|p=139}}

On 6 February, Combeforce had faced some well-organised attacks with artillery and tank support, which had been repulsed by C Battery RHA and nine Bofors anti-tank guns of the 106th RHA. Italian infantry had used wrecked tanks as cover for their advance, while many more lost hope and surrendered. During the night, some tanks from the Pimple arrived and four were knocked out by mines and gunfire, four got through with some lorries and the rest gave up.{{sfn|Playfair|Stitt|Molony|Toomer|1954|pp=359–361}} The Italians had only about thirty tanks left and planned to force their way through Combeforce at dawn, before the British could attack the flanks and rear of the column.{{sfn|Macksey|1972|pp=150–151}}

The attack had artillery support, as soon as it was light enough to see movement by the anti-tank guns portée of the 106th RHA. The infantry of the 2nd Rifle Brigade stayed under cover as they were overrun by the Italian tanks, which concentrated on the RHA anti-tank guns. C Battery 4th RHA fired on the Rifle Brigade positions as the tanks passed and the Rifle Brigade resumed fire on Italian infantry following the tanks, to pin them down. The M13s knocked out all but one anti-tank gun and kept going into the reserve company area but the last gun was driven to a flank by the battery commander, his batman and the cook. The improvised crew commenced firing as the last M13s drove towards the Officers' mess tent put up the day before and knocked out the last tank {{convert|20|yd|m}} from the tent. On the road, the Italians could hear British tank engines on the flanks and from the rear and further north, the 4th Armoured Brigade surrounded another group, at which point the Italians surrendered.{{sfn|Playfair|Stitt|Molony|Toomer|1954|p=361}}

==Aftermath==

===Subsequent operations===
At the end of February, the 106th (Lancashire Hussars) Anti-tank Regiment, RHA was chosen to become a Light Anti Aircraft (LAA) regiment of three batteries with 36 x 20mm Breda guns captured from the Italians. The regiment was renamed the 106th Light Anti-Aircraft Regiment, RA (Lancashire Hussars). In March 1941, the regiment was sent to Greece in [[Operation Lustre]] as part of W Force and the regiment was sent to defend the airstrip at [[Larissa]].{{sfn|Arthur|2003|pp=16, 20}} The German advance forced the British to retreat to the town of [[Nauplion]], where the 106th were the only anti-aircraft defence. After destroying their Breda guns, the regiment was evacuated to Crete on board [[HMS Calcutta]].{{sfn|Arthur|2003|pp=49, 54}} Most of the regiment ended the campaign in the defence of Suda Bay in the [[Battle of Crete]] and were taken prisoner.{{sfn|Playfair|Flynn|Molony|Toomer|2004|pp=132–133}} The regiment was placed in suspended animation in July and many of the survivors reinforced the 102nd ([[Northumberland Hussars]]) Anti-tank Regiment RA, which was refitting after being evacuated from Greece and Crete.{{sfn|Playfair|Flynn|Molony|Toomer|2004|p=88}} The 2nd Battalion The Rifle Brigade fought through the rest of the [[Western Desert Campaign]] and took part in the defence of [[Outpost Snipe]] from {{nowrap|26–27 October 1942}} during the [[Second Battle of El Alamein]]. Along with the 239th Battery, 76th Anti-Tank Regiment RA and other units, the battalion advanced to a depression near Kidney Ridge, defended it against Axis armoured attacks and spoilt the biggest counter-attack against the ground captured by the Eighth Army during Operation Lightfoot.{{sfn|Pitt|1982|pp=153–165}}

==See also==
* [[Tenth Army (Italy)|10th Army]]
* [[7th Armoured Division (United Kingdom)|7th Armoured Division]]
* [[Battle of Beda Fomm]]

==Footnotes==
{{reflist|20em}}

==References==
{{refbegin}}
* {{cite book |ref={{harvid|Arthur|2003}}
|last=Arthur |first=Douglas |year=2003 |title=Forty Men – Eight Horses |publisher=Vanguard Press |location=Cambridge |isbn=978-1-84386-070-9}}
* {{cite book |ref={{harvid|Long|1952}}
|last=Long |first=Gavin |authorlink=Gavin Long |title=To Benghazi |year=1952 |format=PDF |series=[[Australia in the War of 1939–1945]] |publisher=[[Australian War Memorial]] |location=Canberra |url=https://www.awm.gov.au/collection/RCDIG1070200/ |accessdate=13 July 2015 |oclc=314648263}}
* {{cite book |ref={{harvid|Macksey|1972}}
|first=Major Kenneth |last=Macksey |authorlink=Kenneth Macksey |series=Ballantine's Illustrated History of the Violent Century, Battle Books |volume=22 |title=Beda Fomm: The Classic Victory |publisher=Ballantine Books |location=New York |year=1972 |isbn=0-345-02434-6}}
* {{cite book |ref={{harvid|Moorehead|1944}}
|title=The Desert War: The Classic Trilogy on the North African Campaign 1940–43 |last=Moorehead |first=A. |authorlink=Alan Moorehead |year=2009 |orig-year=1944 |publisher=Hamish Hamilton |location=London |edition=Aurum Press |isbn=978-1-84513-391-7}}
* {{cite web |ref={{harvid|Parri|nd}}
|last=Parri |first=M. |url=http://spazioinwind.libero.it/cico85/carristi.html |title=Storia del 32° Rgt. Carri dalla Costituzione del Reggimento fino al termine del Secondo Conflitto Mondiale |trans-title=History of the 32nd Armoured Regiment from its Establishment until the end of the Second World War |others=no date, no ISBN |language=Italian|website=www.assocarri.it |accessdate=27 July 2015}}
* {{cite book |ref={{harvid|Pitt|1982}}
|last=Pitt |first=B. |title=The Crucible of War: Montgomery's Command |volume=III |year=2001 |orig-year=1982 |publisher=Jonathan Cape |location=London |edition=Cassell |isbn=0-304-35952-1}}
* {{cite book |ref={{harvid|Playfair|Stitt|Molony|Toomer|1954}}
|first1=Major-General I. S. O. |last1=Playfair |author1-link=Ian Stanley Ord Playfair |first2=Commander G. M. S. |last2=with Stitt [[Royal Navy|RN]] |first3=Brigadier C. J. C. |last3=Molony |first4=Air Vice-Marshal S. E. |last4=Toomer |editor-last=Butler |editor-first=J. R. M. |editor-link=James Ramsay Montagu Butler |series=History of the Second World War, United Kingdom Military Series |title=The Mediterranean and Middle East: The Early Successes Against Italy (to May 1941) |volume=I |year=1954 |publisher=[[HMSO]] |location=London |oclc=888934805 |lastauthoramp=y}}
* {{cite book |ref={{harvid|Playfair|Flynn|Molony|Toomer|2004}}
|last1=Playfair |first1=Major-General I. S. O.  |first2=Captain F. C. |last2=Flynn RN |first3=Brigadier C. J. C. |last3=Molony |first4=Air Vice-Marshal S. E. |last4=Toomer |editor-last=Butler |editor-first=J. R. M. |series=History of the Second World War, United Kingdom Military Series |title=The Mediterranean and Middle East: The Germans come to the help of their Ally (1941) |volume=II |publisher=Naval & Military Press |year=2004 |orig-year=1st. pub. [[HMSO]] 1956 |isbn=1-84574-066-1 |lastauthoramp=y}}
* {{cite book |ref={{harvid|Walker|2003}}
|first=Ian W. |last=Walker |title=Iron Hulls, Iron Hearts: Mussolini's Elite Armoured Divisions in North Africa |publisher=Crowood |year=2003 |location=Marlborough |isbn=1-86126-646-4}}
{{refend}}

==Further reading==
{{refbegin}}
* {{cite book |last=Arthur |first=D. |year=2000 |title=Desert Watch: A Story of the Battle of Beda Fomm |publisher=Blaisdon |location=Bedale |isbn=978-1-902838-01-4}}
* {{cite thesis |last=Bierwirth |first=J. G. |series=School of Advanced Military Studies Monograph |title=Beda Fomm: An Operational Analysis |type=MAAS |url=http://cgsc.contentdm.oclc.org/cdm/ref/collection/p4013coll2/id/1031 |year=1994 |publisher=US Army Command and General Staff College |location=Fort Leavenworth, KS |accessdate=28 March 2015 |docket=ADA 284686 |oclc=41972642}}
* {{cite thesis |last=Dando |first=N. |title=The Impact of Terrain on British Operations and Doctrine in North Africa 1940–1943 |type=PhD |url=https://pearl.plymouth.ac.uk//handle/10026.1/3035 |year=2014 |publisher=Plymouth University |accessdate=25 March 2015 |oclc=885436735}}
* {{cite book |last=Latimer |first=Jon |authorlink=Jon Latimer |title=Operation Compass 1940: Wavell's Whirlwind Offensive |location=Oxford |publisher=Osprey |orig-year=2000 |year=2013 |isbn=1-85532-967-0}}
{{refend}}

==External links==
* [http://www.warlinks.com/armour/11_hussars/index.php War Diaries of the 11th Hussars]

{{World War II}}

{{Coord|32.597734|N|21.472778|E|format=dms|display=title}}

[[Category:Military units and formations established in 1940]]
[[Category:Military units and formations of the British Army in World War II]]
[[Category:Ad hoc units and formations of the British Army]]
[[Category:Military units and formations disestablished in 1941]]
[[Category:Rifle Brigade (Prince Consort's Own)]]