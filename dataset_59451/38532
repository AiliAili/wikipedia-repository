{{other ships|French ship Bouvet}}
{{Use dmy dates|date=May 2011}}
{|{{Infobox ship begin |infobox caption=''Bouvet''}}
{{Infobox ship image
|Ship image=[[File:French battleship Bouvet.jpg|300px|French battleship Bouvet]]
|Ship caption=
}}
{{Infobox ship career
|Hide header=
|Ship country=France
|Ship flag={{shipboxflag|France|naval}}
|Ship name=''Bouvet''
|Ship namesake=[[François Joseph Bouvet]]
|Ship builder=[[Lorient]], France, Charles Ernest Huin
|Ship laid down=16 January 1893
|Ship launched=27 April 1896
|Ship commissioned=June 1898
|Ship homeport=[[Toulon]]
|Ship fate=Sunk during [[Naval operations in the Dardanelles Campaign|operations off the Dardanelles]] on 18 March 1915
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=
|Ship type=[[Pre-dreadnought]] [[battleship]]
|Ship tonnage=
|Ship displacement={{convert|12007|MT|abbr=on}}
|Ship length={{convert|117.81|m|abbr=on}}
|Ship beam={{convert|21.39|m|abbr=on}}
|Ship draft={{convert|8.38|m|abbr=on}}
|Ship propulsion=*3 triple-expansion [[steam engine]]s
*32 [[Belleville boiler]]s
*{{convert|15000|ihp|abbr=on|lk=in}}
|Ship speed={{convert|18|kn|abbr=on|lk=in}}
|Ship range=
|Ship complement=*Peacetime: 666
*Wartime: 710
|Ship armament=*2 × [[305mm/40 Modèle 1893 gun|305 mm/45 Modèle 1893 gun]]s
*2 × [[274mm/45 Modèle 1893 gun]]s
*8 × [[138mm/45 Modèle 1888 gun]]s
*8 × {{convert|100|mm|abbr=on}} guns
*12 × 1.5&nbsp;kg guns
*2 × {{convert|450|mm|abbr=on}} [[torpedo tube]]s
|Ship armor=*[[Belt armor|Belt]]: {{convert|460|mm|abbr=on}}
*[[Gun turret|Turrets]]: {{convert|380|mm|abbr=on}}
*[[Conning tower]]: {{convert|305|mm|abbr=on}}
|Ship notes=
}}
|}

'''''Bouvet''''' was a [[pre-dreadnought]] [[battleship]] of the [[French Navy]]. She was laid down in January 1891, launched in April 1896, and completed in June 1898. She was a member of a group of five broadly similar battleships, along with {{ship|French battleship|Charles Martel||2}}, {{ship|French battleship|Jauréguiberry||2}}, {{ship|French battleship|Carnot||2}}, and {{ship|French battleship|Masséna||2}}, which were ordered in response to the British {{sclass-|Royal Sovereign|battleship|4}}. Like her half-sisters, she was armed with a main battery of two {{convert|305|mm|abbr=on}} guns and two {{convert|274|mm|abbr=on}} guns in individual turrets. She had a top speed of {{convert|17.8|kn|abbr=on}}.

''Bouvet'' spent the majority of her career alternating between the Northern and Mediterranean Squadrons. At the outbreak of [[World War I]], she escorted troop convoys from [[North Africa]] to [[France]]. She then joined the [[Naval operations in the Dardanelles Campaign|naval operations off the Dardanelles]], where she participated in a major attack on the Turkish fortresses in the straits on 18 March 1915. During the attack, she was hit approximately eight times by shellfire, though did not suffer fatal damage. She struck a mine at around 3:15, and sank within two minutes; only some 50 men were rescued from a complement of 710. Two British battleships were also sunk by mines that day, and the disaster convinced the Allies to abandon the naval campaign in favor of an [[Gallipoli Campaign|amphibious assault on Gallipoli]].

==Design==

''Bouvet'' was the last member of a group of five battleships built to a broadly similar design, but different enough to be considered unique vessels. The first ship was {{ship|French battleship|Charles Martel||2}}, which formed the basis for ''Bouvet'' and three other ships.<ref name=G294>Gardiner, p. 294</ref> Design specifications were identical for each of the ships, but different engineers designed each vessel. The ships were based on the previous battleship {{ship|French battleship|Brennus||2}}, but instead of mounting the main battery all on the centerline, the ships used the lozenge arrangement of the earlier vessel {{ship|French battleship|Magenta||2}}, which moved two of the main battery guns to single turrets on the [[wing turret|wings]]. The five ships were built in response to the British {{sclass-|Royal Sovereign|battleship|2}}s.<ref>Ropp, p. 223</ref>

===General characteristics and machinery===

''Bouvet'' was {{convert|117.81|m|ftin|sp=us}} [[length between perpendiculars|long between perpendiculars]], and had a [[beam (nautical)|beam]] of {{convert|21.39|m|ftin|abbr=on}} and a [[Draft (hull)|draft]] of {{convert|8.38|m|ftin|abbr=on}}. She had a displacement of {{convert|12007|t|LT|0}}. Unlike her half-sisters, her deck was not cut down to the main deck level, and her [[superstructure]] was reduced in size. She was equipped with two small fighting masts. ''Bouvet'' had a standard crew of 666 officers and enlisted men, though her wartime complement increased to 710.<ref name=G294/>

''Bouvet'' had three vertical [[triple expansion engine]]s each driving a single screw, with steam supplied by twenty-four [[Belleville boiler|Belleville]] [[water-tube boiler]]s. Her propulsion system was rated at {{convert|15000|ihp|lk=in}}, which allowed the ship to steam at a speed of {{convert|18|kn|lk=in}}. As built, she could carry {{convert|610|MT|abbr=on}} of coal, though additional space allowed for up to {{convert|980|MT|abbr=on}} in total.<ref name=G294/>

===Armament and armor===

''Bouvet''{{'}}s main armament consisted of two [[Canon de 305 mm Modèle 1893 gun]]s in two single-gun turrets, one each fore and aft. She also mounted two [[Canon de 274 mm Modèle 1893 gun]]s in two single-gun turrets, one amidships on each side, [[sponson]]ed out over the tumblehome of the ship's sides. Her secondary armament consisted of eight [[Canon de 138.6 mm Modèle 1893 gun]]s, which were mounted in single turrets at the corners of the superstructure. She also carried eight {{convert|100|mm|abbr=on}} quick-firing guns, twelve 3-pounders, and eight 1-pounder guns. Her armament suite was rounded out by four {{convert|450|mm|abbr=on}} [[torpedo tube]]s, two of which were submerged in the ship's hull. The other two tubes were mounted above water, though these were later removed.<ref name=G294/>

The ship's armor was constructed with nickel steel. The main belt was {{convert|460|mm|abbr=on}} thick amidships, and tapered down to {{convert|250|mm|abbr=on}} at the lower edge. Forward of the central citadel, the belt was reduced to {{convert|305|mm|abbr=on}} and further to {{convert|200|mm|abbr=on}} at the [[Stem (ship)|stem]]; the belt extended for the entire length of the hull. Above the belt was {{convert|101|mm|abbr=on}} thick side armor. The main battery guns were protected with {{convert|380|mm|abbr=on}} of armor, and the secondary turrets had {{convert|120|mm|abbr=on}} thick sides. The [[conning tower]] had 305&nbsp;mm thick sides.<ref name=G294/>

==Service history==
[[File:Bouvet in the Dardanelles.png|thumb|''Bouvet'' in the Dardanelles]]
''Bouvet'' was laid down in [[Lorient]] on 16 January 1893, and launched on 27 April 1896. After completing [[fitting-out]] work, she was commissioned into the French Navy in June 1898.<ref name=G294/> In 1903, ''Bouvet'' was replaced in the Mediterranean Squadron by the new battleship {{ship|French battleship|Suffren||2}}; she in turn replaced the old [[ironclad warship|ironclad battleship]] {{ship|French ironclad|Dévastation||2}} in the Northern Squadron. The Squadron remained in commission for only six months of the year.<ref>Brassey (1903), pp. 57, 60</ref> During the annual fleet maneuvers in July&ndash;August 1903, ''Bouvet'' served as the [[flagship]] of Admiral Gervais, the neutral observer for the simulated battles.<ref>Brassey (1903), p. 148</ref> During the maneuvers off [[Golfe-Juan]], the battleship {{ship|French battleship|Gaulois||2}} accidentally rammed ''Bouvet'' on 31 January 1903, though both vessels emerged largely undamaged.<ref name=C1228>Caresse (2012), pp. 122–28</ref>

By 1906, ''Bouvet'' had returned to the Mediterranean Squadron, which was under the command of Vice Admiral Touchard.<ref name=B103>Brassey (1907), p. 103</ref> Following the eruption of [[Mount Vesuvius]] in Naples in April 1906, ''Bouvet'' and the battleships {{ship|French battleship|Iéna||2}} and ''Gaulois'' aided survivors of the disaster.<ref name=C1228/> The annual summer fleet exercises were conducted in July and August; during the maneuvers, ''Bouvet'' nearly collided with the battleship ''Gaulois'' again.<ref name=B103/> She was assigned to the Second Squadron of the Mediterranean Squadron by 1908; she was retained on active service for the year, but with a reduced crew.<ref>Palmer, p. 171</ref>

===Loss off the Dardanelles===
[[File:Bouvet sinking March 18 1915.jpg|thumb|right|''Bouvet''{{'}}s last moments after striking a [[naval mine|mine]] in the [[Dardanelles]]]]

Together with the older French pre-dreadnoughts, ''Bouvet'' escorted Allied troop convoys through the Mediterranean until November when she was ordered to the [[Dardanelles]] to guard against a sortie by the German [[battlecruiser]] {{SMS|Goeben}}.<ref name=Corbett>Corbett, pp. 160, 214, 218</ref> She bombarded the Turkish fort of Kum Kale, on the Asian side of the strait on 19 February. During the bombardment, ''Bouvet'' assisted the battleship {{ship|French battleship|Suffren||2}} by sending firing corrections via [[Wireless telegraphy|radio]] while {{ship|French battleship|Gaulois||2}} provided [[counter-battery fire]] to suppress the Ottoman [[coastal artillery]].<ref>Caresse (2010), pp. 21–22</ref>

On 18 March, ''Bouvet'', along with {{ship|French battleship|Charlemagne||2}}, ''Suffren'', and ''Gaulois'', was to attack the Dardanelles fortresses. The plan called for six British pre-dreadnoughts to suppress the Turkish fortifications, after which the French battleships would attack those same fortifications at close range.<ref name=Corbett/> The French fleet was commanded by Admiral [[Émile Paul Amable Guépratte|Émile Guépratte]];<ref name=T524>Tucker, p. 524</ref> the acting Allied commander was Rear Admiral [[John de Robeck]], who stood in for Admiral [[Sackville Carden]].<ref name=G84>Griffiths, p. 84</ref> The Allied battleships were arranged in line abreast, in three rows; ''Bouvet'' was stationed in the center of the second row.<ref>Tucker, p. 463</ref> The force entered the straits at 11:30 and bombarded the town of [[Çanakkale]], before turning to the Fortress Hamidieh and other nearby fortifications at 13:30.<ref name=TEW>''The European War'', p. 219</ref>

For the first half-hour, the French and British battleships shelled the forts indiscriminately, before turning to attacking individual gun batteries.<ref name=TEW/> In the course of the attack on the fortresses, ''Bouvet'' sustained eight hits from Turkish artillery fire. Her forward turret was disabled after the propellant gas extractor broke down.<ref name=G294/> One of the shells destroyed one of her masts.<ref name=TEW/> At around 15:15, ''Bouvet'' struck a [[naval mine|mine]] with a {{convert|176|lb|adj=on}} explosive charge, which detonated below the starboard 274&nbsp;mm gun turret.<ref name=G294/><ref name=TEW/> These mines had been freshly laid a week before the attack, and were unknown to the Allies.<ref name=G84/>

[[File:Bouvet capsizing March 18 1915.jpg|thumb|''Bouvet'' capsizes in the [[Dardanelles]], 18 March 1915.]]

''Bouvet'' capsized and sank in about two minutes. The ship was in poor condition at the time due to her age, which likely contributed to her rapid sinking, though there was some speculation that her ammunition magazine exploded.<ref name=G294/> The destruction of the ship caught the Allies by surprise; her loss came during the height of the bombardment. [[Torpedo boat]]s and other smaller vessels rushed to pick up survivors, but they rescued only a handful of men.<ref name=TEW/> From her complement of 710 men, some 660 were killed in the sinking.<ref name=G294/>

Despite the sinking of ''Bouvet'', the first such loss of the day, the British remained unaware of the minefield, thinking the explosion had been caused by a shell or [[torpedo]]. Subsequently two British pre-dreadnoughts, {{HMS|Ocean|1898|2}} and {{HMS|Irresistible|1898|2}}, were sunk and the battlecruiser {{HMS|Inflexible|1907|2}} were damaged by the same minefield. ''Suffren'' and ''Gaulois'' were both badly damaged by coastal artillery during the engagement.<ref name=G84/><ref>Gardiner, p. 295</ref> The loss of ''Bouvet'' and two other British battleships during the 18 March attack was a major factor in the decision to abandon a naval strategy to take [[Constantinople]], and instead opt for the [[Gallipoli Campaign|Gallipoli land campaign]].<ref name=T524/>

==See also==
{{Portal|Battleships}}
{{commons|French battleship Bouvet}}
* [[List of battleships of France]]

==Footnotes==
{{reflist|colwidth=20em}}

==References==
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | authorlink = Thomas Brassey, 1st Earl Brassey | year = 1903 | journal = [[Brassey's Naval Annual]] | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | year = 1907 | journal = Brassey's Naval Annual | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite book|editor=Jordan, John|last=Caresse|first=Philippe|chapter=The Drama of the Battleship Suffren|publisher=Conway|location=London|year=2010|title=Warship 2010|pages=9–26|isbn=978-1-84486-110-1}}
* {{cite book|editor=Jordan, John|publisher=Conway|location=London, UK|year=2012|chapter=The Battleship Gaulois|last=Caresse|first= Phillippe|title=Warship 2012|isbn=978-1-84486-156-9}}
* {{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations|edition=reprint of the 1929 second|series=History of the Great War: Based on Official Documents|volume=II|year=1997|publisher=Imperial War Museum in association with the Battery Press|location=London, UK|isbn=1-870423-74-7}}
* {{cite book | editor-last = Gardiner | editor-first = Robert | title = Conway's All the World's Fighting Ships 1860–1905 | publisher = Conway Maritime Press | location = Greenwich, UK | year = 1979 | isbn = 978-0-8317-0302-8 }}
* {{cite book|last=Griffiths|first=William R.|title=The Great War|year=2003|publisher=Square One Publishers|location=Garden City Park, NY|isbn=0757001580}}
* {{cite journal | editor-last = Palmer | editor-first = W. | year = 1908 | journal = Hazell's Annual | volume = | publisher = Hazell, Watson & Viney, Ltd. | location = London, UK }}
* {{cite book | last = Ropp | first = Theodore | editor-last = Roberts | editor-first = Stephen S. | title = The Development of a Modern Navy: French Naval Policy, 1871–1904 | year = 1987 | location = Annapolis, MD | publisher = Naval Institute Press | isbn = 978-0-87021-141-6 }}
* {{cite journal | year = 1917| journal = The European War: April&ndash;June 1915| volume =III | publisher = The New York Times Company | location = New York, NY }}
* {{cite book | editor-last = Tucker| editor-first = Spencer| title = World War I: A - D., Volume 1 | publisher = ABC-CLIO | location = Santa Barbara, CA | year = 2005 | isbn = 1851094202 }}

{{coord|40|01|15|N|26|16|30|E|region:TR_source:frwiki|display=title}}

{{French battleship Bouvet}}
{{WWIFrenchShips}}
{{March 1915 shipwrecks}}
{{good article}}

{{DEFAULTSORT:Bouvet}}
[[Category:Gallipoli Campaign]]
[[Category:Ships sunk by mines]]
[[Category:Maritime incidents in 1915]]
[[Category:World War I battleships of France]]
[[Category:Ships built in France]]
[[Category:World War I shipwrecks in the Dardanelles]]
[[Category:1896 ships]]
[[Category:Battleships of the French Navy]]
[[Category:Ships with Belleville boilers]]