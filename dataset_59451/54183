
'''John Millard Dunn''' (5 January 1865 – 3 March 1936) was organist and choirmaster for [[St. Peter's Cathedral, Adelaide]] for 44 years.

==History==
John Dunn was born in North Adelaide a twin son of John Charles Dunn and his wife Lydia Charlotte Dunn née Smithson, of Barnard Street, North Adelaide. He was educated at John Whinham's [[North Adelaide Grammar School]], and studied piano under Miss Francis of [[Glenelg, South Australia|Glenelg]], then E. Smith-Hall and Herr Boehm.<ref name=art>{{cite news |url=http://nla.gov.au/nla.news-article105273733 |title=Art in Adelaide |newspaper=[[Daily Herald (Adelaide)|Daily Herald]] |location=Adelaide |date=28 December 1912 |accessdate=18 January 2015 |page=1 Section: Magazine Section |publisher=National Library of Australia}}</ref> He was a choirboy at St. Peter's Cathedral under Arthur Boult, and was frequently a featured soloist. He also studied organ under Mr. Boult and displayed such proficiency that in 1882 he became his assistant. He had secured full-time positions with Francis Clark & Sons and the Bank of Australasia, but in 1888 he sailed for London to study with [[W. de Manby Sergison]], organist at St. Peter's, Eaton square, London,<ref name=art/> then in 1889 he furthered his studies under Sir [[Frederick Bridge]], the great [[Westminster Abbey]] organist. On his return to Adelaide he took up teaching at the Adelaide College of Music (later Elder Conservatorium) under [[Cecil Sharp]] and [[I. G. Reimann]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article129112789 |title=Who's Who in the Church |newspaper=[[The News (Adelaide)|The News]] |location=Adelaide |date=30 April 1929 |accessdate=18 January 2015 |page=8 Edition: Home |publisher=National Library of Australia}}</ref> He was appointed organist to the Cathedral on 1 November 1891, and officiated at the inauguration of the new organ in 1930;<ref>{{cite news |url=http://nla.gov.au/nla.news-article35413420 |title=Death of Mr. J. M. Dunn |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=4 March 1936 |accessdate=18 January 2015 |page=18 |publisher=National Library of Australia}}</ref> the last service at which he presided was just a week before his death at the age of 71 years.

==Other activities==
He served as conductor for the Adelaide Orpheus Society and president of the Adelaide Society of Organists.

He was the composer for a stage musical ''The Mandarin'' with libretti by [[Harry Congreve Evans]], performed at the [[Theatre Royal, Adelaide]] in 1896.

Dunn was a successful teacher of the organ: two students of whom he was particularly proud were [[Arthur H. Otto]], who on occasion filled in as assistant; and [[Horace Weber]], Cathedral organist at [[Napier, New Zealand]].<ref name=art/>

He taught music theory at [[Tormore House]], a school for girls at North Adelaide.

==Recognition==
A newspaper once nominated him one of the 15 notable SA musicians of the late 19th and early 20th century: [[Frederick Bevan]], [[Charles Cawthorne]],  [[E. Harold Davies]], J. M. Dunn, [[Thomas Grigg (musician)|Thomas Grigg]], [[Hermann Heinicke]], [[John Horner (organist)|John Horner]], [[E. H. Wallace Packer]], [[Harold S. Parsons]], [[W. R. Pybus]], [[I. G. Reimann]], [[William Silver (musician)|William Silver]], [[C. J. Stevens (musician)|C. J. Stevens]], [[Oscar Taeuber]], [[Arthur Williamson]]<ref>{{cite news |url=http://nla.gov.au/nla.news-article90958031 |title=Notable Adelaide Musicians: Past and Present |newspaper=[[The Chronicle (Adelaide)]] |volume=LXXVI, |issue=4,008 |location=South Australia |date=7 September 1933 |accessdate=8 February 2017 |page=31 |via=National Library of Australia}}</ref>

==Family==
John M. Dunn had four brothers: Frank C. Dunn (his identical twin), banker of Sydney who retired to Mount Lofty; Walter C. Dunn of [[Launceston, Tasmania]]; Dr. Spencer S. Dunn, of [[Bournemouth]], England; and George V. S. Dunn, mining engineer, of [[Isleworth]], Middlesex, England.

John M. Dunn married Gertrude Josephine Ann Henning ( – 15 May 1939) of North Adelaide on 29 August 1906. They had two children:
*Seymour Dunn, moved to London where he married Hazel Griffith in 1937
*Evelyn Young Dunn (10 March 1910 – ) married Donnell Downey, lived at [[Thorngate, South Australia|Thorngate]]

They were not closely related to the [[Dunn family of South Australia|early settler Dunns of SA]].

== References ==
{{Reflist}}

{{DEFAULTSORT:Dunn, John Millard}}
[[Category:1865 births]]
[[Category:1936 deaths]]
[[Category:Australian choral conductors]]
[[Category:Australian classical organists]]
[[Category:Australian music educators]]