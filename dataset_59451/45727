{{Infobox person
| name                      = Friedrich Karl Richard Paul August Freiherr Koenig von und zu Warthausen
| image                     =
| image_size                = 
| alt                       =
| caption                   =
| birth_date                = {{Birth date|1906|4|2|df=y}}
| birth_place               = [[Warthausen]], [[Baden-Württemberg]], Germany
| death_date                = {{Death date and age|1986|12|15|1906|4|2|df=y}}
| death_place               = [[Munich]], Germany
| death_cause               =
| resting_place             = Niederaudorf, nr. [[Oberaudorf]], [[Upper Bavaria]]
| resting_place_coordinates = 
| monuments                 =
| residence                 =
| nationality               = {{flagicon|Germany}} [[Germany|German]]
| occupation                = Aviator
| years_active              =
| known_for                 = First solo flight around the world, August 1928–November 1929
| spouse                    = {{nowrap|Anna Marie Reps (1948-1956)}}<br/>Sigrid Roesner (1957-1983)
| children                  = Hans-Christoph (b. 1958)
| parents                   = 
| awards                    = [[Hindenburg Cup]] (1929)
| signature                 =
| signature_alt             = 
| signature_size            =
}}
'''Friedrich Karl Richard Paul August Freiherr<ref>{{German title Freiherr}}</ref> Koenig von und zu Warthausen'''<ref name="baronll">{{cite web |url= http://www.wennedach.de/Leute/Baron/BaronLL/baronll.html |title=Life of Friedrich Karl Freiherr Koenig von und zu Warthausen |work=wennedach.de |year=2006 |accessdate=5 June 2012|language=de}}</ref> (2 April 1906–15 December 1986) was a [[Germans|German]] [[aviator]] who made the [[List of circumnavigations#Aircraft|first solo flight around the world]] in 1928-29.<ref name="soloflights">{{cite web |url=http://www.soloflights.org/baron_text_e.html |title=Baron von Koenig-Warthausen|first=Claude |last=Meunier |work=Solo Flights around the World |year=2011 |accessdate=5 June 2012}}</ref> His flight took him eastwards from [[Berlin]] to [[Moscow]], then to the [[Persian Gulf]], across northern [[India]] and to [[Siam]]. He travelled mostly by ship to [[China]] and [[Japan]], then across the [[Pacific Ocean]]. After flying across the [[United States]], he again took a ship back to [[Europe]], ending his flight in [[Hanover]] after 15 months. For this feat he was awarded the [[Hindenburg Cup]].

==Biography==

===Early life and education===
[[File:Adel im Wandel19.jpg|thumb|View of ''[[Warthausen Castle|Schloss Warthausen]]'' by [[Johann Heinrich Tischbein]] (1781)]]
The eldest son of Friedrich Karl Wilhelm Freiherr Koenig von und zu Warthausen (1863-1948), and his wife Elisabeth Hedwig Marie Anna von Wiedebach und Nostiz-Jänkendorf (1878-1961),<ref>{{cite web |url= http://www.wennedach.de/Leute/Baron/Christoph/Vorfahren/vorfahren.html |title=Antecedents of the Koenig-Warthausens |work=wennedach.de |year=2006 |accessdate=5 June 2012|language=de}}</ref> he was born at the family home of [[Schloss Warthausen]],<ref name="Zepp">{{cite news |title=Der Koenig und sein Kamerad |first=Achim |last=Zepp |url=http://www.angele-verlag.de/catalog/pdf/sz060401-koenig100geb.pdf |format=PDF |newspaper=Schwäbische Zeitung |publisher= |location= |date=1 April 2006 |accessdate=5 June 2012 |language=German |trans_title=The King and his Comrade}}</ref> close to the town of [[Biberach an der Riss]] in [[Baden-Württemberg]]. He attended the ''[[Gymnasium (Germany)|Humanistisches Gymnasium]]'' [[boarding school]] in [[Munich]], and in February 1924 graduated from the high school in [[Kempten (Allgäu)|Kempten, Allgäu]], before studying [[Law]] and [[Economics]] at Universities in Munich, [[Königsberg]], [[Berlin]], and [[England]], while also pursuing his enthusiasm for sailing and flying,<ref name="baronll"/> obtaining his [[Private Pilot Licence|pilot's licence]] after only 12 hours instruction.<ref name="soloflights"/>

===Round the world flight===
[[File:Klemm-L20.jpg|thumb|left|300px|Koenig-Warthausen's Klemm L.20B aircraft on display at the [[Mercedes-Benz Museum]], Stuttgart]]
In 1928 [[Paul von Hindenburg|President Hindenburg]] announced the creation of the [[Hindenburg Cup]], awarded annually for the best sporting flight by an amateur pilot<ref name="soloflights"/> with a prize of 10,000 [[Reichsmark]]s.<ref name="Zepp"/> Koenig-Warthausen was determined to win the prize and persuaded his parents to buy him a [[Klemm]] L.20B.<ref name="soloflights"/> This aircraft, [[Aircraft registration|registration number]] D-1433, which he named ''[[Comrade#German usage|Kamerad]]'', was a two-seater [[low-wing]] [[monoplane]] with an [[Manufacturer's Weight Empty|empty weight]] of only {{Convert|265|kg}}, a top speed of {{Convert|105|kph}}, and powered by an air-cooled, two-cylinder [[Daimler-Benz]] F-7502 [[boxer engine]]<ref>{{cite web |url=http://www.gm.fh-koeln.de/~landsber/Seiten/FKKW.pdf |title=Friedrich Karl Koenig-Warthausen |first=Georg |last=von Landsberg |work=gm.fh-koeln.de |format=PDF |year=2010 |accessdate=5 June 2012|language=de}}</ref> delivering only {{Convert|20|hp|kW|abbr=on}} This aircraft, with its high [[Aspect ratio (wing)|aspect ratio]] and very low [[wing loading]], would today be classified as an [[Ultralight aviation|ultralight]].<ref name="soloflights"/>

Koenig-Warthausen took off from [[Berlin Tempelhof Airport]]<ref name="Zepp"/> at midnight on 9 August 1928, having less than 20 hours solo flying experience, only five of those in the Klemm. After crossing the [[Berezina River]], heavy rains set in,<ref name="soloflights"/> and 16 hours into the flight, he was forced to land about {{Convert|50|km}} short of his destination.<ref name="Zepp"/> The next morning he flew to [[Khodynka Aerodrome]], Moscow. After a few days he continued his flight, heading south-east across the [[Caucasus]] to [[Iran|Persia]], landing at [[Baku]], [[Bandar-e Anzali|Pahlevi]] and [[Tehran]]. He then flew on to [[Isfahan]], [[Shiraz]] and finally [[Bushire]] on the [[Persian Gulf]], where he received a [[Cablegram|cable]] informing him that he had been awarded the Hindenberg Cup for 1928. There he also met the aviator [[Ehrenfried Günther Freiherr von Hünefeld|Freiherr von Hünefeld]],<ref name="soloflights"/> who had completed the first Atlantic crossing from East to West only six months before, and was attempting his own circumnavigation with Swedish pilot Karl Gunnar Lindner in his [[Junkers W 33]] named ''Europa''.<ref>{{cite web |url= http://www.wingnet.org/rtw/RTW001S.HTM |title=Baron Hunefeld and Lindner Round-The-World Flight Attempt |work=Round the World Flights |year=2009 |accessdate=6 June 2012}}</ref>

Koenig-Warthausen decided to continue flying, heading eastwards across India, stopping at [[Bandar-Abbas]] and [[Karachi]]. Five weeks later, he flew on, stopping at [[Nasirabad, Ajmer|Nasirabad]] and [[Agra]], in order to visit the [[Taj Mahal]], then [[Allahabad]] and [[Gaya, India|Gaya]], finally reaching [[Calcutta]] on 24 December 1928.<ref name="soloflights"/> He stayed there for two months, sight-seeing, taking part in hunting trips, and travelling as far as [[Tibet]]. He also met [[Mohandas Karamchand Gandhi|Gandhi]], and the poet [[Rabindranath Tagore]].<ref name="Zepp"/> He left Calcutta on 5 February 1929, heading south and stopping at [[Sittwe|Akyab]] and [[Yangon|Rangoon]] in [[Burma]], before flying through a tropical thunderstorm to [[Bangkok]], [[Siam]]. He was delayed for 10 days by [[monsoon]] rains, during which time the wife of [[Prajadhipok]], the King of Siam, presented him with a rare [[Siamese cat]]. Koenig-Warthausen named it "Tanim", and the cat accompanied him for the rest of the journey. He finally left Bangkok on 25 March, heading south to [[Prachuap Khiri Khan]] and [[Songkhla]], and experiencing another tropical storm while flying over [[Penang]] before arriving at [[Singapore]]. He stayed there for three weeks, making the short flight south to cross the [[Equator]]. Having booked, and then cancelled, passage home for himself and his aircraft several times, it was only then that he decided to continue the flight around the world. Having been advised not to fly over [[Indochina]], he travelled by ship to [[Shanghai]], China, and then flew to [[Nanking]].<ref name="soloflights"/>

From there Koenig-Warthausen sailed east to [[Kobe]], Japan, where he spent three weeks, before sailing to the [[United States]] from [[Yokohama]], arriving in [[San Francisco]] on 8 June. His aircraft was reassembled and repaired at [[Alameda, California|Alameda]], where, on learning of his fellow German flyers death, it was renamed ''Hünefeld'' on his honour. After ten days he flew to [[Los Angeles]],<ref name="soloflights"/> where he met fellow [[Swabia]]n, film producer [[Carl Laemmle]].,<ref name="Zepp"/> before flying to [[San Diego]], then [[Tucson, Arizona]], and [[El Paso, Texas]].<ref name="soloflights"/> At El Paso, early on the morning of 12 July, a car crashed into the taxicab taking him to the airport. He was thrown from the car, suffering a [[concussion]], and seriously injuring his leg.<ref name="Zepp"/> He spent the next two months recuperating, and was also informed that if he could reach New York before 31 October he would win the Hindenburg Cup for 1930. He left El Paso on 15 September, but when landing at [[Sweetwater, Texas]], his aircraft [[Ground loop (aviation)|ground looped]] and nosed over. He was forced to wait while a complete wing and other spare parts arrived from the [[Aeromarine|Aeromarine-Klemm Corp.]] in New York, and his aircraft was transported to [[Dallas]], where repairs were made.<ref name="soloflights"/>

Koenig-Warthausen's next flew to [[St. Louis, Missouri|St. Louis]] and [[Chicago]], but after landing in [[Detroit]] on 17 October, a broken engine valve delayed him for four days. After crossing into [[Canada]], water in his fuel forced him to land in [[London, Ontario]], and he then flew through a [[snow storm]] on the way to [[Hamilton, Ontario|Hamilton]]. He flew over the [[Niagara Falls]] to [[Buffalo, New York]], then to [[Syracuse, New York|Syracuse]] and [[Albany, New York|Albany]], eventually arriving at [[Roosevelt Field, New York|Roosevelt Field]], [[Long Island]], on 2 November, and just failing to win the Hindenburg Cup for a second time.<ref name="soloflights"/>

After several receptions, newspaper interviews and speeches,<ref name="soloflights"/> a meeting with his hero [[Charles Lindbergh]] at the "[[Quiet Birdmen]] Club"<ref name="Zepp"/> and a visit to [[Washington, D.C.]], Koenig-Warthausen boarded the [[ocean liner]] {{SS|Bremen|1929|6}} to return to Europe. After arriving in [[Bremerhaven]] he flew on, but thick fog forced him to end his flight in [[Hanover]] on 23 November 1929, 15 months after it began, having covered {{Convert|20,000|mi}} in 450 hours flying time. In Berlin he was enthusiastically received, and President von Hindenburg personally presented him with the Cup he had won the previous year.<ref name="soloflights"/>

===Later life===
Following his flight, Koenig-Warthausen travelled widely throughout [[North America]], promoting the German aircraft industry,<ref name="baronll"/> before attending the [[University of Tübingen]] from 1931, studying economic and transport geography, and gaining his [[Doctor of Philosophy|D.Phil]] in 1934 with his [[dissertation]] on German aviation to South America. He also wrote accounts of his round the world flight, which were published in English and German.<ref name="Zepp"/>

From 1935 he worked for the airline ''[[Deutsche Luft Hansa]]'', and also the aircraft manufacturers ''[[Weser Flugzeugbau]]'' and ''[[Focke-Achgelis]]''. He was also a representative of the ''Reichsverband Deutsche Luftfahrtindustrie'' ("National Association of the German Aviation Industry"). During [[World War II]] he worked for the German [[holding company]] controlling the Dutch [[Philips]] company assets in Europe. In 1945 Koenig-Warthausen returned to a family estate at Sommershausen, near Biberach, where he farmed and was a member of several organisations promoting local agriculture. He sold the estate in 1973 and retired to a house overlooking [[Lake Maggiore]], in the village of [[Brezzo di Bedero]], Italy. Koenig-Warthausen died in Munich on 15 December 1986, and is buried at the cemetery in Niederaudorf, nr. [[Oberaudorf]], [[Upper Bavaria]].<ref name="baronll"/>

==Personal life==
Koenig-Warthausen was married twice, firstly on 22 December 1948 to Anna Marie Reps, a sculptor and writer from [[Bremen]]. They had no children and were divorced in December 1956. He then married Sigrid Roesner on 19 March 1957, with whom he had a son, Hans-Christoph Hubertus Alexander Franz-Ferdinand Friedrich Freiherr Koenig von und zu Warthausen, born on 12 April 1958.<ref name="baronll"/>

==Publications==
* {{cite book |title=Wings Around the World |year=1930 |publisher=[[G. P. Putnam's Sons]] |location=London & New York |isbn= }}
* {{cite book |title=Mit 20 PS und Leuchtpistole |trans_title=With 20 HP and a Flare Pistol |year=1932 |publisher=Deutsche Verlags-Anstalt |location=Stuttgart |language=German |isbn= }} (reprinted by Thienemanns, Stuttgart, 1951)
* {{cite book |title=Weiter mit 20 PS! |trans_title=Onwards with 20 HP! |year=1933 |publisher=Deutsche Verlags-Anstalt |location=Stuttgart |language=German |isbn= }} (reprinted by Thienemanns, as ''Wunderland und Wolkenkratzer [Wonderland and skyscrapers]'' Stuttgart, 1952)
* {{cite book |title= Der regelmäßige deutsche Luftverkehr nach Südamerika in seiner wirtschafts- und politisch-geographischen Bedeutung |trans_title=The regular German air traffic to South America in its economic, political and geographical importance |year=1937 |publisher=Verlag der Hohenloheschen Buchhandlung |location=Öhringen |language=German |isbn= |oclc= |doi= |id= }}<ref>{{cite web |url=http://www.gaestebuecher-schloss-neubeuern.de/biografien/Warthausen_Friedrich_von.pdf |title=Die Biografien |work=Schloss Neubeuern & Familie Miller |format=PDF |year=2011 |accessdate=5 June 2012|language=de}}</ref>

==See also==
* [[List of circumnavigations]]

==References==
{{Reflist}}

==Further reading==
* {{cite book |first=Hans |last=Angele |title=Koenig der Lüfte : Der Weltflug 1928 des F.K. Freiherr von Koenig von und zu Warthausen|trans_title=King of the Skies : The 1928 circumnavigation of F.K. Freiherr von Koenig von und zu Warthausen |year=2000 |publisher=Angele |location=[[Ochsenhausen]] |language=German |isbn=3-9807403-0-7 }}

==External links==
* [http://www.dmairfield.org/people/koenig_fk/index.htm  F.K. Baron von Koenig-Warthausen]: [[Davis-Monthan]] Airfield Register

{{Authority control}}
{{DEFAULTSORT:Koenig-Warthausen, Friedrich Karl}}
[[Category:1906 births]]
[[Category:1986 deaths]]
[[Category:German aviators]]
[[Category:Aviation pioneers]]
[[Category:20th-century aviation]]
[[Category:Swabian nobility]]