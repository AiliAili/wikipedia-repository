<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= SA-900 V-Star
 | image=File:Stolp_SA-900_V-Star_at_Paine_Field.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Stolp Starduster Corporation]]<br />[[Aircraft Spruce & Specialty Co]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=Plans available (2014)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=65 (1998)
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]195.00 (Plans only, 2014)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Stolp SA-900 V-Star''' is an [[United States|American]] [[aerobatic]] [[homebuilt aircraft|homebuilt]] [[biplane]], currently produced by [[Aircraft Spruce & Specialty Co]] in the form of plans for amateur construction. In the 1990s it was also available as a kit from [[Stolp Starduster Corporation]] of [[Riverside, California]].<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 264. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

==Design and development==
The V-Star was designed as a low-cost, economical and easy to fly design, with a light wing loading and short runway requirements. It features a [[strut-braced]] biplane layout, with [[cabane strut]]s, [[interplane strut]]s and [[flying wires]], a single-seat open cockpit, fixed [[conventional landing gear]] with [[wheel pants]] and a single engine in [[tractor configuration]].<ref name="Aerocrafter" />

The aircraft [[fuselage]] is made from welded [[4130 steel]] tubing. Its {{convert|23.00|ft|m|1|abbr=on}} span wings are made from [[spruce]] and [[plywood]], with the whole aircraft covered with [[Aircraft dope|doped]] [[aircraft fabric]]. The wings employ a [[Clark YH]] [[airfoil]] and have a total area of {{convert|141.0|sqft|m2|abbr=on}}. The engine used is the {{convert|65|hp|kW|0|abbr=on}} [[Continental A65]] or other similar powerplants.<ref name="Aerocrafter" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 13 March 2014|last = Lednicer |first = David |year = 2010}}</ref>

The V-Star has a typical empty weight of {{convert|700|lb|kg|abbr=on}} and a gross weight of {{convert|1000|lb|kg|abbr=on}}, giving a useful load of {{convert|300|lb|kg|abbr=on}}. With full fuel of {{convert|15|u.s.gal}} the payload for the pilot and baggage is {{convert|210|lb|kg|abbr=on}}.<ref name="Aerocrafter" />

The standard day, sea level, no wind, take off with a {{convert|65|hp|kW|0|abbr=on}} engine is {{convert|400|ft|m|0|abbr=on}} and the landing roll is {{convert|600|ft|m|0|abbr=on}}.<ref name="Aerocrafter" />

The designer estimates the construction time from the kit that was available in the 1990s as 1800&nbsp;hours.<ref name="Aerocrafter" />

==Operational history==
By 1998 the company reported that 65 aircraft were completed and flying.<ref name="Aerocrafter" />

In March 2014, 13 examples were [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]], although a total of 24 had been registered at one time. In Canada in March 2014 there were two registered with [[Transport Canada]] and in the [[United Kingdom]] a further two were registered with the [[Civil Aviation Authority (United Kingdom)|CAA]].<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=V-STAR&PageNo=1|title = Make / Model Inquiry Results|accessdate = 13 March 2014|last = [[Federal Aviation Administration]]|date = 13 March 2014}}</ref><ref name="TCCAR">{{cite web|url = http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/quicksearch.asp|title = Canadian Civil Aircraft Register|accessdate = 13 March 2014|last = [[Transport Canada]]|date = 13 March 2014}}</ref><ref name="GINFO">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=V-Star|title = GINFO Search Results Summary|accessdate = 13 March 2014|last = [[Civil Aviation Authority (United Kingdom)]]|date = 13 March 2014}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (V-Star) ==
{{Aircraft specs
|ref=AeroCrafter and The Incomplete Guide to Airfoil Usage<ref name="Aerocrafter" /><ref name="Incomplete" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=17
|length in=0
|length note=
|span m=
|span ft=23
|span in=0
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=141.0
|wing area note=
|aspect ratio=
|airfoil=[[Clark YH]]
|empty weight kg=
|empty weight lb=700
|empty weight note=
|gross weight kg=
|gross weight lb=1000
|gross weight note=
|fuel capacity={{convert|15|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental A65]]
|eng1 type=four cylinder, air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=
|eng1 hp=65

|prop blade number=2
|prop name=wooden, fixed pitch
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=90
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=75
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=35
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=280
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+/-6g (operational), +/-9g (ultimate)
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=600
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=7.1
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
*[[List of aerobatic aircraft]]

==References==
{{reflist}}

==External links==
{{Commons category|Stolp SA-900 V-Star}}
*[http://www.aircraftspruce.com/catalog/kitspages/vstarsa900.php?clickkey=11509 Official website]
{{Stolp aircraft}}
{{Aerobatics}}

[[Category:Stolp aircraft|SA900 V-Star]]
[[Category:United States sport aircraft 1990–1999]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Homebuilt aircraft]]
[[Category:Aerobatic aircraft]]