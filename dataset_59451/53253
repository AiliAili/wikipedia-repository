{{Infobox NCAA team season
|Mode = football
|Year = 2007
|Team = North Carolina Tar Heels
|Image = University of North Carolina Tarheels Interlocking NC logo.svg
|ImageSize = 100
|Conference = Atlantic Coast Conference
|Division = Coastal Division
|ShortConference = ACC
|BCSRank =
|CoachRank =
|APRank =
|Record = 4–8
|ConfRecord = 3–5
|HeadCoach = [[Butch Davis]]
|OffCoach = [[John Shoop]]
|DefCoach = [[Chuck Pagano]]
|OScheme = [[Pro-style offense|Pro-style]]
|DScheme =  [[4–3 defense|4–3]]
|StadiumArena = [[Kenan Memorial Stadium]]<br>(Capacity: 60,000)
|Champion =
|BowlTourney =
|BowlTourneyResult =
|uniform=
}}
{{2007 ACC football standings}}
The '''2007 North Carolina Tar Heels football team''' represented the [[University of North Carolina at Chapel Hill|University of North Carolina]] in the [[2007 NCAA Division I FBS football season]]. The Tar Heels played their home games at [[Kenan Memorial Stadium]] in [[Chapel Hill, North Carolina]] competed in the [[Atlantic Coast Conference]]. They were led by first-year head coach [[Butch Davis]].

==Previous season==
{{see also|2006 North Carolina Tar Heels football team}}
The 2006 North Carolina football team began with the Tar Heels unranked in the AP and coaches' preseason polls. Despite the fact that they were receiving little national attention, there was much optimism in the state of [[North Carolina]].  The team got off to a shaky start losing six out of their first seven games.  Just days after playing their seventh game of the season, a 23-0 loss to [[Virginia Cavaliers football|Virginia]] in [[Charlottesville]], the University of North Carolina announced that head coach [[John Bunting (coach)|John Bunting]] would be relieved of his duties at the conclusion of the season."<ref>"{{cite news |title=UNC's Bunting Out at End of Season|first=Robbi|last=Pickeral|url=http://www.newsobserver.com/758/story/501963.html|newspaper=[[The News & Observer]]|location=Raleigh|date=October 22, 2006|accessdate=March 4, 2007|archiveurl=https://web.archive.org/web/20061118004708/http://www.newsobserver.com/758/story/501763.html|archivedate=November 18, 2006|deadurl=yes}}</ref>  After the announcement of Bunting's firing the team arguably became much more competitive and won their last two games of the season, 23&ndash;9 against [[North Carolina State Wolfpack football|North Carolina State]] and 45-44 against [[Duke Blue Devils football|Duke]].

==Preseason==
Immediately after Bunting's firing on October 22, 2006 there was much speculation over who UNC would name their new football coach. Rumors immediately started to circulate that former head coach of the [[Cleveland Browns]] and [[University of Miami|Miami Hurricanes]], [[Butch Davis]], was highly interested in the position."<ref>"[http://sports.espn.go.com/ncf/news/story?id=2648903 Butch Davis jumps to top of list for North Carolina job]." ''ESPN.com.'' Retrieved on March 4, 2007.</ref> Athletic director [[Dick Baddour]] confirmed the accuracy of the rumors when he announced on November 13, 2006 that Butch Davis had agreed to become the new head football coach."<ref>"[http://www.sportingnews.com/yourturn/viewtopic.php?p=1350274 Butch Davis hired to coach at UNC]." ''sportingnews.com.'' Retrieved on March 4, 2007.</ref>
Soon after his hiring, Butch Davis began to assemble his coaching staff. He was able to lure Offensive Coordinator [[John Shoop]] and Defensive Coordinator Chuck Pagano from the [[Oakland Raiders]] of the [[NFL]], and Defensive Line Coach [[John Blake (American football)|John Blake]] from [[Nebraska Cornhuskers football|Nebraska]]. Blake is regarded by many to be one of the best recruiters in all of college football."<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/121506aab.html John Blake Hired As North Carolina Assistant Football Coach]." ''tarheelblue.com.'' Retrieved on March 4, 2007.</ref>

As of March 2007 many publications began releasing preseason rankings for the upcoming football season. Although UNC still ranked in the lower tier of the [[Atlantic Coast Conference]], many publications were impressed with the Tar Heels' hiring of Butch Davis and their top ranked recruiting class."<ref>"[http://collegefootball.rivals.com/content.asp?CID=645402 Inside Spring Drills: ACC Preview]." ''rivals.com.'' Retrieved on March 4, 2007.</ref>

==Recruiting==
The Tar Heels received 20 [[Letter of intent|letters of intent]] on [[National Signing Day]], February 7, 2007. Three student athletes had already enrolled before National Signing Day making this class relatively large with 23 commitments.
{{College Athlete Recruit Start|40=yes|collapse=yes}}
{{College Athlete Recruit Entry
| recruit    = [[Marvin Austin]]
| position   = DT
| hometown   = Washington, DC
| highschool = Ballou HS
| feet   = 6
| inches = 2
| weight = 295
| 40     = 4.68
| commitdate   = February 7, 2007
| scout stars  = 5
| rivals stars = 5
| espn grade   = 85
}}
{{College Athlete Recruit Entry
| recruit    = Charles Brown
| position   = WR
| hometown   = Maple Heights, OH
| highschool = Maple Heights Senior HS
| feet   = 5
| inches = 9
| weight = 190
| 40     = 4.47
| commitdate   = January 29, 2007
| scout stars  = 2
| rivals stars = 2
| espn grade   = 73
}}
{{College Athlete Recruit Entry
| recruit    = Kevin Bryant
| position   = OT
| hometown   = Lauderdale Lakes, FL
| highschool = Boyd H. Anderson HS
| feet   = 6
| inches = 6
| weight = 375
| 40     = 5.87
| commitdate   = February 7, 2007
| scout stars  = 4
| rivals stars = 3
| espn grade   = 82
}}
{{College Athlete Recruit Entry
| recruit    = Albert Craddock
| position   = LB
| hometown   = High Point, NC
| highschool = Southwest Guilford HS
| feet   = 6
| inches = 2
| weight = 205
| 40     = 4.45
| commitdate   = March 4, 2006
| scout stars  = 3
| rivals stars = 3
| espn grade   = 75
}}
{{College Athlete Recruit Entry
| recruit    = Mike Dykes
| position   = OG
| hometown   = Pittsburgh, PA
| highschool = Woodland Hills HS
| feet   = 6
| inches = 3
| weight = 275
| 40     = 5.20
| commitdate   = January 24, 2007
| scout stars  = 3
| rivals stars = 3
| espn grade   = 77
}}
{{College Athlete Recruit Entry
| recruit    = Linwan Euwell
| position   = DE
| hometown   = Tarboro, NC
| highschool = Southwest Edgecombe HS
| feet   = 6
| inches = 2
| weight = 210
| 40     = 4.65
| commitdate   = February 2, 2007
| scout stars  = 3
| rivals stars = 3
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = LaCount Fantroy
| position   = CB
| hometown   = New Berlin, NY
| highschool = Milford Academy
| feet   = 5
| inches = 11
| weight = 170
| 40     = 4.50
| commitdate   = June 17, 2005
| scout stars  = 2
| rivals stars = 3
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Carl Gaskins
| position   = OT
| hometown   = Melbourne, FL
| highschool = Palm Bay Senior HS
| feet   = 6
| inches = 5
| weight = 255
| 40     = NA
| commitdate   = February 4, 2007
| scout stars  = 2
| rivals stars = 2
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Brian Gupton
| position   = WR
| hometown   = Nashville, TN
| highschool = Pearl-Cohn HS
| feet   = 6
| inches = 3
| weight = 185
| 40     = 4.50
| commitdate   = February 4, 2007
| scout stars  = 2
| rivals stars = 3
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Cameron Holland
| position   = OG
| hometown   = Pittsburgh, PA
| highschool = Perry Traditional Academy
| feet   = 6
| inches = 2
| weight = 280
| 40     = 4.95
| commitdate   = January 24, 2007
| scout stars  = 3
| rivals stars = 3
| espn grade   = 72
}}
{{College Athlete Recruit Entry
| recruit    = Ryan Houston
| position   = RB
| hometown   = Matthews, NC
| highschool = David W Butler HS
| feet   = 6
| inches = 2
| weight = 245
| 40     = 4.58
| commitdate   = January 21, 2007
| scout stars  = 4
| rivals stars = 4
| espn grade   = 79
}}
{{College Athlete Recruit Entry
| recruit    = Dwight Jones
| position   = WR
| hometown   = Burlington, NC
| highschool = Hugh M Cummings HS
| feet   = 6
| inches = 5
| weight = 210
| 40     = 4.61
| commitdate   = February 7, 2007
| scout stars  = 4
| rivals stars = 5
| espn grade   = 81
}}
{{College Athlete Recruit Entry
| recruit    = Greg Little
| position   = WR
| hometown   = Durham, NC
| highschool = Hillside HS
| feet   = 6
| inches = 3
| weight = 215
| 40     = 4.55
| commitdate   = February 7, 2007
| scout stars  = 4
| rivals stars = 4
| espn grade   = 84
}}
{{College Athlete Recruit Entry
| recruit    = Rashad Mason
| position   = WR
| hometown   = Nashville, TN
| highschool = Pearl-Cohn HS
| feet   = 6
| inches = 6
| weight = 215
| 40     = 4.50
| commitdate   = February 4, 2007
| scout stars  = 4
| rivals stars = 4
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Matt Merletti
| position   = CB
| hometown   = Cleveland, OH
| highschool = St. Ignatius HS
| feet   = 5
| inches = 11
| weight = 185
| 40     = 4.48
| commitdate   = December 12, 2006
| scout stars  = 2
| rivals stars = 2
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Mike Paulus
| position   = QB
| hometown   = Syracuse, NY
| highschool = Christian Brothers Academy
| feet   = 6
| inches = 5
| weight = 220
| 40     = 4.90
| commitdate   = April 28, 2006
| scout stars  = 4
| rivals stars = 5
| espn grade   = 79
}}
{{College Athlete Recruit Entry
| recruit    = Zach Pianalto
| position   = TE
| hometown   = Springdale, AR
| highschool = Springdale HS
| feet   = 6
| inches = 4
| weight = 220
| 40     = 4.63
| commitdate   = January 2, 2007
| scout stars  = 4
| rivals stars = 3
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = Tydreke Powell
| position   = DT
| hometown   = Ahoskie, NC
| highschool = Hertford County HS
| feet   = 6
| inches = 3
| weight = 295
| 40     = 4.90
| commitdate   = January 21, 2007
| scout stars  = 4
| rivals stars = 4
| espn grade   = 81
}}
{{College Athlete Recruit Entry
| recruit    = Devon Ramsay
| position   = RB
| hometown   = Red Bank, NJ
| highschool = Lawrenceville Prep
| feet   = 6
| inches = 2
| weight = 242
| 40     = 4.60
| commitdate   = January 7, 2007
| scout stars  = 3
| rivals stars = 3
| espn grade   = 68
}}
{{College Athlete Recruit Entry
| recruit    = Da'Norris Searcy
| position   = LB
| hometown   = Decatur, GA
| highschool = Towers HS
| feet   = 6
| inches = 0
| weight = 195
| 40     = 4.50
| commitdate   = December 10, 2006
| scout stars  = 4
| rivals stars = 3
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Jonathan Smith
| position   = LB
| hometown   = Durham, NC
| highschool = Hillside HS
| feet   = 6
| inches = 2
| weight = 190
| 40     = 4.6
| commitdate   = April 11, 2006
| scout stars  = 3
| rivals stars = 3
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = Quantavius Sturdivant
| position   = LB
| hometown   = Oakboro, NC
| highschool = West Stanly HS
| feet   = 6
| inches = 3
| weight = 227
| 40     = 4.55
| commitdate   = November 16, 2006
| scout stars  = 4
| rivals stars = 3
| espn grade   = 79
}}
{{College Athlete Recruit Entry
| recruit    = Jay Wooten
| position   = K
| hometown   = Laurinburg, NC
| highschool = Scotland HS
| feet   = 6
| inches = 3
| weight = 180
| 40     = 4.8
| commitdate   = May 19, 2006
| scout stars  = 2
| rivals stars = 2
| espn grade   = 73
}}
{{College Athlete Recruit End
| 40               = yes
| year             = 2007
| rivals ref title = North Carolina Commit List for 2007
| scout ref title  = Scout.com Football Recruiting: North Carolina
| espn ref title   = RecruitTracker 2007: North Carolina
| rivals school    =
| scout s          =
| espn schoolid    =
| scout overall    = 15
| rivals overall   = 16
| accessdate       = March 4, 2007
| collapse         = yes
}}

==Coaching staff==
{| class="wikitable"
! '''Name'''
! '''Position'''<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/fbccoaches.html North Carolina Coaching Staff]." ''tarheelblue.com.'' Retrieved on March 4, 2007.</ref>
! '''Seasons in Position'''
|- align="center"
| [[Butch Davis]]
| Head Coach
| 1st
|- align="center"
| [[John Blake (American football)|John Blake]]
| Associate Head Coach / Recruiting Coordinator / Defensive Line
| 1st
|- align="center"
| Ken Browning
| Running Backs
| 14th
|- align="center"
| Jeff Connors
| Strength and Conditioning Coordinator
| 7th
|- align="center"
| Steve Hagen
| Tight Ends
| 1st
|- align="center"
| John Lovett
| Special Teams Coordinator / Defensive Assistant
| 1st
|- align="center"
| [[Chuck Pagano]]
| Defensive Coordinator / Defensive Backs
| 1st
|- align="center"
| Sam Pittman
| Offensive Line
| 1st
|- align="center"
| [[John Shoop]]
| Offensive Coordinator / Quarterbacks
| 1st
|- align="center"
| Tommy Thigpen
| Linebackers
| 3rd
|- align="center"
| Charlie Williams
| Wide Receivers
| 1st
|-
|}

==Schedule==
{{CFB Schedule Start|time=|rank=no|ranklink=|rankyear=2007|tv=|attend=yes}}
{{CFB Schedule Entry
| date         = September 1
| time         = 6:00 PM
| w/l          = w
| nonconf      = yes
| homecoming   =
| away         =
| neutral      =
| rank         = no
| opponent     = [[2007 James Madison Dukes football team|James Madison]]
| opprank      =
| site_stadium = [[Kenan Memorial Stadium]]
| site_cityst  = [[Chapel Hill, North Carolina|Chapel Hill, NC]]
| gamename     =
| tv           = [[ESPN3|ESPN360]]
| score        = 37&ndash;14
| overtime     =
| attend       = 58,500
}}
{{CFB Schedule Entry
| date         = September 8
| time         = 6:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         = no
| opponent     = [[2007 East Carolina Pirates football team|East Carolina]]
| opprank      =
| site_stadium = [[Dowdy–Ficklen Stadium]]
| site_cityst  = [[Greenville, North Carolina|Greenville, NC]]
| gamename     =
| tv           = [[CBS Sports Network|CSTV]]
| score        = 31&ndash;34
| overtime     =
| attend       = 43,387
}}
{{CFB Schedule Entry
| date         = September 15
| time         = 12:00 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         = no
| opponent     = [[2007 Virginia Cavaliers football team|Virginia]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = [[South's Oldest Rivalry]]
| tv           = [[Raycom Sports|Raycom]]/[[Lincoln Financial Media|LFS]]
| score        = 20&ndash;22
| overtime     =
| attend       = 58,000
}}
{{CFB Schedule Entry
| date         = September 22
| time         = 12:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         = no
| opponent     = [[2007 South Florida Bulls football team|South Florida]]
| opprank      = 24
| site_stadium = [[Raymond James Stadium]]
| site_cityst  = [[Tampa, Florida|Tampa, FL]]
| gamename     =
| tv           = [[ESPN]]
| score        = 10&ndash;37
| overtime     =
| attend       = 37,693
}}
{{CFB Schedule Entry
| date         = September 29
| time         = 12:00 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         = no
| opponent     = [[2007 Virginia Tech Hokies football team|Virginia Tech]]
| opprank      = 14
| site_stadium = [[Lane Stadium]]
| site_cityst  = [[Blacksburg, Virginia|Blacksburg, VA]]
| gamename     =
| tv           = Raycom/LFS
| score        = 10&ndash;17
| overtime     =
| attend       = 66,233
}}
{{CFB Schedule Entry
| date         = October 6
| time         = 12:00 PM
| w/l          = w
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         = no
| opponent     = [[2007 Miami Hurricanes football team|Miami]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     =
| tv           = [[ESPN2]]
| score        = 33&ndash;27
| overtime     =
| attend       = 59,000
}}
{{CFB Schedule Entry
| date         = October 13
| time         = 3:30 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         =
| neutral      =
| rank         = no
| opponent     = [[2007 South Carolina Gamecocks football team|South Carolina]]
| opprank      = 12
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     =
| tv           = [[ESPN on ABC|ABC]]
| score        = 15&ndash;21
| overtime     =
| attend       = 61,000
}}
{{CFB Schedule Entry
| date         = October 27
| time         = 12:00 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         = no
| opponent     = [[2007 Wake Forest Demon Deacons football team|Wake Forest]]
| opprank      =
| site_stadium = [[BB&T Field]]
| site_cityst  = [[Winston-Salem, North Carolina|Winston-Salem, NC]]
| gamename     =
| tv           = Raycom/LFS
| score        = 10&ndash;37
| overtime     =
| attend       = 33,023
}}
{{CFB Schedule Entry
| date         = November 3
| time         = 3:45 PM
| w/l          = w
| nonconf      =
| homecoming   = yes
| away         =
| neutral      =
| rank         = no
| opponent     = [[2007 Maryland Terrapins football team|Maryland]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     =
| tv           = [[ESPNU]]
| score        = 16&ndash;13
| overtime     =
| attend       = 56,000
}}
{{CFB Schedule Entry
| date         = November 10
| time         = 12:00 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         = no
| opponent     = [[2007 NC State Wolfpack football team|North Carolina State]]
| opprank      =
| site_stadium = [[Carter–Finley Stadium]]
| site_cityst  = [[Raleigh, North Carolina|Raleigh, NC]]
| gamename     = [[North Carolina–NC State football rivalry|Carolina–State Game]]
| tv           = Raycom/LFS
| score        = 27&ndash;31
| overtime     =
| attend       = 57,583
}}
{{CFB Schedule Entry
| date         = November 17
| time         = 12:00 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         = no
| opponent     = [[2007 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| opprank      =
| site_stadium = [[Bobby Dodd Stadium]]
| site_cityst  = [[Atlanta|Atlanta, GA]]
| gamename     =
| tv           = Raycom/LFS
| score        = 25&ndash;27
| overtime     =
| attend       = 45,490
}}
{{CFB Schedule Entry
| date         = November 24
| time         = 3:30 PM
| w/l          = w
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         = no
| opponent     = [[2007 Duke Blue Devils football team|Duke]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = [[Victory Bell (Carolina-Duke)|Victory Bell Game]]
| tv           = ESPNU
| score        = 20&ndash;14
| overtime     = [[Overtime (sports)|OT]]
| attend       = 52,000
}}
{{CFB Schedule End|rank=|poll=|timezone=[[Eastern Time Zone|Eastern Time]]}}

'''Did not play:'''  [[Boston College]], [[Clemson University|Clemson]], and [[Florida State University|Florida State]].

==Team Statistics==
{| class="wikitable"
! &nbsp; &nbsp; Passing Leader  &nbsp; &nbsp;
! &nbsp; Cmp &nbsp;
! &nbsp; Att &nbsp;
! &nbsp; Yds &nbsp;
! &nbsp; TD &nbsp;
! &nbsp; Int &nbsp;
|-
| [[T.J. Yates]]
| align="center" | 218
| align="center" | 365
| align="center" | 2655
| align="center" | 14
| align="center" | 18
|-
|}

{| class="wikitable"
! &nbsp; &nbsp; Rushing Leaders &nbsp; &nbsp;
! &nbsp; Car &nbsp;
! &nbsp; Yds &nbsp;
! &nbsp; Long &nbsp;
! &nbsp; TD &nbsp;
|-
| Johnny White
| align="center" | 95
| align="center" | 399
| align="center" | 39
| align="center" | 0
|-
| Anthony Elzy
| align="center" | 92
| align="center" | 321
| align="center" | 39
| align="center" | 5
|-
| Greg Little
| align="center" | 59
| align="center" | 300
| align="center" | 38
| align="center" | 2
|-
| Ryan Houston
| align="center" | 44
| align="center" | 145
| align="center" | 10
| align="center" | 1
|-
| Brandon Tate
| align="center" | 12
| align="center" | 131
| align="center" | 54
| align="center" | 1
|-
|}

{| class="wikitable"
! &nbsp; &nbsp; Receiving Leaders &nbsp; &nbsp;
! &nbsp; Rec &nbsp;
! &nbsp; Yds &nbsp;
! &nbsp; Long &nbsp;
! &nbsp; TD &nbsp;
|-
| Hakeem Nicks
| align="center" | 74
| align="center" | 958
| align="center" | 53
| align="center" | 5
|-
| Brandon Tate
| align="center" | 25
| align="center" | 479
| align="center" | 51
| align="center" | 5
|-
| Brooks Foster
| align="center" | 29
| align="center" | 417
| align="center" | 65
| align="center" | 2
|-
|}

{| class="wikitable"
! &nbsp; &nbsp; Kicking &nbsp; &nbsp;
! &nbsp; XPM &nbsp;
! &nbsp; XPA &nbsp;
! &nbsp; FGM &nbsp;
! &nbsp; FGM &nbsp;
! &nbsp; Long &nbsp;
! &nbsp; Pts &nbsp;
|-
| Connor Barth
| align="center" | 21
| align="center" | 23
| align="center" | 19
| align="center" | 22
| align="center" | 51
| align="center" | 78
|-
|}

==References==
{{Reflist}}

{{North Carolina Tar Heels football navbox}}

{{DEFAULTSORT:2007 North Carolina Tar Heels Football Team}}
[[Category:North Carolina Tar Heels football seasons]]
[[Category:2007 Atlantic Coast Conference football season|North Carolina Tar Heels]]
[[Category:2007 in North Carolina|Tar Heels]]