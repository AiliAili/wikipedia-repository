{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Anne of Geierstein
| orig title   =
| translator   =
| image        = Anne of G 1829.jpg
| caption = First edition title page.
| author       = Sir [[Walter Scott]]
| cover_artist = 
| country      = Scotland, Great Britain
| language     = English
| series       = [[Waverley Novels]]
| genre        = [[Historical novel]]
| publisher    = 
| release_date = 1829
| media_type   = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages        = 
| preceded_by  = 
| followed_by  = 
}}

'''''Anne of Geierstein, or The Maiden of the Mist''''' (1829) is a novel by Sir [[Walter Scott]]. It is set in [[Central Europe]], mainly in Switzerland, shortly after the [[House of York|Yorkist]] victory at the [[Battle of Tewkesbury]] (1471). It covers the period of Swiss involvement in the [[Burgundian Wars]].

==Plot introduction==
Two exiled [[House of Lancaster|Lancastrian]]s are on a secret mission to the court of [[Charles I, Duke of Burgundy|Charles the Bold]], Duke of [[Duchy of Burgundy|Burgundy]], hoping to gain his help in regaining the English crown from the Yorkist [[Edward IV of England|Edward IV]]. The two Englishmen get into difficulties in the Swiss mountains. They meet Countess Anne and her family, who are involved in the politics of the newly independent [[Growth of the Old Swiss Confederacy|Swiss Confederation]] and plan to confront Charles with complaints about his conduct towards the Swiss nation. The two groups decide to travel together. Anne may have inherited magical skills from her grandmother, enabling her to perform feats which defy explanation. The travellers also encounter a shadowy organization known as the [[League of the Holy Court|Vehmgericht]] or Secret Tribunal.

==Plot summary==
[[Image:Basel 073.jpg|thumb|right|Basel]]
As the merchant John Philipson and his son Arthur were travelling towards [[Basel]] they were overtaken by a storm, and found themselves at the edge of a precipice caused by a recent earthquake. Arthur was making his way towards a tower indicated by their guide Antonio, when he was rescued from imminent danger by Anne, who conducted him to her uncle Bierderman's mountain home. His father had already been brought there to safety by Biederman and his sons. During their evening games Rudolph, who had joined in them, became jealous of the young Englishman's skill with the bow, and challenged him; but they were overheard by Anne, and the duel was interrupted. The travellers were invited to continue their journey in company with a deputation of Switzers, commissioned to remonstrate with [[Charles the Bold]] respecting the exactions of Hagenbach; and the magistrates of Basel having declined to let them enter the city, they took shelter in the ruins of a castle. During his share in the night watches, Arthur fancied that he saw an apparition of Anne, and was encouraged in his belief by Rudolph, who narrated her family history, which implied that her ancestors had dealings with supernatural beings. Hoping to prevent a conflict on his account between the Swiss and the duke's steward, the merchant arranged that he and his son should precede them; but on reaching the Burgundian citadel they were imprisoned by the governor in separate dungeons. Arthur, however, was released by Anne with the assistance of a priest, and his father by Biederman, a body of Swiss youths having entered the town and incited the citizens to execute Hagenbach, just as he was intending to slaughter the deputation, whom he had treacherously admitted. A valuable necklace which had been taken from the merchant was restored to him by Sigismund, and the deputies having decided to persist in seeking an interview with the duke, the Englishman undertook to represent their cause favourably to him.

On their way to Charles's headquarters father and son were overtaken by Anne disguised as a lady of rank, and, acting on her whispered advice to Arthur, they continued their journey by different roads. The elder fell in with a mysterious priest who provided him with a guide to the "Golden Fleece," where he was lowered from his bedroom to appear before a meeting of the [[Vehmic court]] or holy tribunal, and warned against speaking of their secret powers. The younger was met and conducted by Annette to a castle, where he spent the evening with his lady-love, and travelled with her the next day to rejoin his father at [[Strassburg]]. In the cathedral there they met [[Margaret of Anjou]], who recognised Philipson as [[John de Vere, 13th Earl of Oxford]], a faithful adherent of the [[house of Lancaster]], and planned with him an appeal to the duke for aid against the [[Yorkist]]s. On reaching Charles's camp the earl was welcomed as an old companion in arms, and obtained a promise of the help he sought, on condition that Provence be ceded to Burgundy. Arthur was despatched to [[Aix-en-Provence]] to urge Margaret to persuade her father accordingly, while the earl accompanied his host to an interview with his burghers and the Swiss deputies.

King [[René of Anjou]]'s preference for the society of troubadours and frivolous amusements had driven his daughter to take refuge in a convent. On hearing from Arthur, however, the result of the earl's mission to the duke, she returned to the palace, and had induced her father to sign away his kingdom, when his grandson Ferrand arrived with the news of the rout of the Burgundian army at [[Neuchâtel]], and Arthur learned from his squire, Sigismund, that he had not seen Anne's spectre but herself during his night-watch, and that the priest he had met more than once was her father, the Count Albert of Geierstein. The same evening Queen Margaret died in her chair of state; and all the earl's prospects for England being thwarted, he occupied himself in arranging a treaty between her father and the King of France. He was still in Provence when he was summoned to rouse the duke from a fit of melancholy, caused by the Swiss having again defeated him. After raising fresh troops, Charles decided to wrest [[Nancy, France|Nancy]] from the young Duke of Lorraine, and during the siege Arthur received another challenge from Rudolph. The rivals met, and, having killed the Bernese, the young Englishman obtained Count Albert's consent to his marriage with Anne, with strict injunctions to warn the duke that the Secret Tribunal had decreed his death. On the same night, the Swiss won their decisive victory at Nancy, establishing their independence. Charles was slain in the battle, his naked and disfigured body only discovered some days afterward frozen into the nearby river. His face had been so badly mutilated by wild animals that his physician was only able to identify him by his long fingernails and the old battle scars on his body. Being still an exile, the earl accepted the patriot Biederman's invitation to reside with his countess at Geierstein, until the [[battle of Bosworth]] placed [[Henry VII of England|Henry VII]] on the throne, when Arthur and his wife attracted as much admiration at the English Court as they had gained among their Swiss neighbours.

==Characters==
* John Philipson, an English merchant, afterwards [[John de Vere, 13th Earl of Oxford]]
* Arthur de Vere, his son
* Antonio, their young Swiss guide
* Arnold Biederman, a magistrate of [[Unterwalden]]
* His sons: Rudiger, Ernest and Sigismund
* Anne of Geierstein, his niece
* Annette Veilchen, her attendant
* Rudolph of Donnershugel, a Bernese
* Count Albert of Geierstein, Anne's father
* Ital Schrechwald, his steward
* [[Charles the Bold]], Duke of Burgundy
* Count Archibald von Hagenbach, his steward
* ''Swiss deputies to the duke''
** Nicholas Bonsteteen
** Melchior Sturmthal
** Adam Zimmerman
* Dannischemend, a [[Persia]]n sorcerer
* Hermione, his daughter
* Jan Mengs, landlord of the "Golden Fleece" in [[Alsace]]
* Knights and burghers of the [[Vehmic court]]
* [[Margaret of Anjou]], widow of King [[Henry VI of England|Henry VI]]
* King [[René of Anjou|René of Provence]], her father
* [[René II, Duke of Lorraine|Ferrand de Vaudemont]], Duke of Lorraine, his grandson
* Count Campo Basso, commander of Italian mercenaries

==''Anne of Geierstein'' and opals==
In 1913, an American writer commented:
:There can be little doubt that much of the modern superstition regarding the supposed unlucky quality of the opal owes its origin to a careless reading of Sir Walter Scott's novel, ''Anne of Geierstein.'' The wonderful tale therein related of the Lady Hermione, a sort of enchanted princess, who came no one knew whence and always wore a dazzling opal in her hair, contains nothing to indicate that Scott really meant to represent the opal as unlucky. [...] when a few drops of holy water were sprinkled over it, they quenched its radiance. Hermione fell into a swoon, was carried to her chamber, and the next day nothing but a small heap of ashes remained on the bed whereon she had been laid. The spell was broken and the enchantment dissolved. All that can have determined the selection of the opal rather than any other precious stone is the fact of its wonderful play of color and its sensitiveness to moisture.<ref>[[George Frederick Kunz|George F. Kunz]]. ''[https://books.google.com/books?id=-NWBAAAAMAAJ&printsec=frontcover#v=onepage&q&f=false The curious lore of precious stones]''. J. B. Lippincott, Philadelphia, 1913. pp. 143-4.</ref>
There is in fact little evidence that the superstition was common before the 1850s. A popular [[gift book]] of the 1840s was entitled ''[[The Opal (annual)|The Opal]]'', which would seem an unlikely title if the notion of the opal's unluckiness were well established. In 1875, less than fifty years after the publication of Scott's novel, Sir [[Henry Ponsonby]] felt compelled to write to ''[[Notes and Queries]]'' to ask for the foundation of the superstition, and received several different answers, none of which mention ''Anne of Geierstein''.<ref>''Notes and Queries''. Sixth series, no. 6. 8 July 1882. p. 32. The comments there refer to discussions in earlier editions.</ref> A brief assertion of such a connection is made by Sir John Piggot in an earlier issue, but it is hedged with a quotation from the gemmologist Charles Barbot (who ascribes it to the influence of ''[[Robert le Diable]]'') and the scholars responding to Queen Victoria's secretary do not refer to it.<ref>''Notes and Queries''. Fourth series, no. 3. 13 February 1869. p. 154.</ref><ref>Charles Barbot. ''Traité complet des pierres précieuses.'' Morris et Co., Paris, 1858. p. 454.</ref>

==Notes==
{{reflist}}

==External links==
* [http://www.walterscott.lib.ed.ac.uk/works/novels/geierstein.html Page on ''Anne of Geierstein'' at the Walter Scott Digital Archive]
* [http://arthurwendover.com/arthurs/scott/annger10.html E-text at Arthur Wendover]
{{Waverley Grey}}

{{Walter Scott}}

{{DEFAULTSORT:Anne Of Geierstein}}
[[Category:1829 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by Walter Scott]]
[[Category:Novels set in the Middle Ages]]
[[Category:Historical novels]]
[[Category:Novels set in Switzerland]]