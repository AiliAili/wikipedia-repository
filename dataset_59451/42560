<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Curlew
 | image=
 | caption=''Flight Global archive''<ref name="FlightA"/>
}}{{Infobox Aircraft Type
 | type=Demonstrator and [[Trainer (aircraft)|trainer]]
 | national origin=[[United Kingdom]]
 | manufacturer=CLW Aviation Co.Ltd.
 | designer=Arthur Levell and Francis Welman
 | first flight=3 September 1936
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''CLW Curlew''' was a two-seat, single-engined [[Trainer (aircraft)|training aircraft]] built partly to demonstrate a new wing structure.  It flew successfully in the [[UK]] in 1936, but the company went bankrupt and only one Curlew flew.

==Design and development==

In the early 1930s Francis Welham and Arthur Levell conceived a new single spar duralumin wing structure that promised weight savings without loss of strength. It was quite different from the already established [[Monospar]] design.<ref name="OH2587-9">{{harvnb|Ord-Hume|2000|pp=299–300}}</ref>  The wing was built around a cross braced, girder-like box, with the front and rear members attached to it with cantilever ribs.<ref name="FlightA">[http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%202539.html ''Flight'' 17 September 1936 p.296]</ref><ref name="OH2587-9"/>  With financial support from S.W. Cole of [[EKCO]] radio, they set up a company known by their initials as CLW Aviation, based at [[Gravesend, Kent]] and built a wing for testing.<ref name="OH2587-9"/>  This performed to the calculations even under destructive testing, and the company decided to produce a small aeroplane using it.<ref name="OH2587-9"/> The CLW Curlew was intended both as a demonstrator and as a trainer for those pilots going on to modern, fast monoplanes.<ref name="OH2587-9"/> It was also seen as a contender in the open two-seater market, particularly for the richer buyer after a machine with "snappier" performance.<ref name="FlightB">[http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%201000.html ''Flight'' 24 April 1936 p.417]</ref>  It made its first flight on 3 September 1936 at Gravesend, flown by the ex-[[William Beardmore and Company#Aviation|Beardmore]] pilot A.N. Kingwill.<ref name="OH2587-9"/>

The Curlew was an all-metal aeroplane, apart from the fabric covering of its elliptical, cantilever wing, and so an unusual light plane for its time.  The wing carried short span [[Aileron#Frise ailerons|Frise ailerons]] outboard.  The rest of the trailing edge carried manually operated split flaps. The fuselage was a monocoque structure, built on duralumin ovals and stringers, covered with stress bearing [[Alclad]] sheet.<ref name="FlightA"/>  The engine installation was particularly neat for a radial.  The 7-cylinder 26.5 in (673&nbsp;mm)<ref>{{harvnb|Lumsden|2003|p=179}}</ref> diameter 90&nbsp;hp (67&nbsp;kW) Pobjoy Niagara came as a "[[power-egg]]" complete with all accessories and its own long chord cowling, plus Pobjoy's characteristic "smiley" front baffle.  The similarity to inline installations was enhanced by the upward off-set of the drive shaft of the two-bladed propeller, caused by the gearbox.<ref name="OH2587-9"/>  The two generous open cockpits were in tandem, the front one at mid wing and the other at the trailing edge. The empennage was conventional: a slender fin carried a rounded, unbalanced rudder which extended down the bottom of the fuselage, and the tapered, mid-fuselage tailplane carried separate elevators so the rudder could move between them.<ref name="OH2587-9"/>  The undercarriage had vertical (in flying position) legs from the wings with a large (3 in or 76&nbsp;mm) movement, each sloping outwards slightly to increase the track and braced inwards by struts from halfway down the legs to the wing roots.<ref name="FlightA"/>

Sqn Ldr F.W.H.Lervill (another CLW director) had clear ideas on training aircraft and the Curlew was intended to place the pupil at the front to familiarise him or her with the sensation of flying alone. He also chose not to fit wheel brakes, for he thought they were likely to confuse the novice.<ref name="FlightA"/>

After its first flight, the Curlew successfully completed its initial trials, which include a terminal velocity dive at 305&nbsp;mph (491&nbsp;km/h) and a maximum loaded weight producing a wing loading of 14&nbsp;lb/sq ft (68&nbsp;kg/m<sup>2</sup>).  Landing speed with flaps down was 38&nbsp;mph (61&nbsp;km/h).  The machine was advertised as suitable for other engines up to powers of 130&nbsp;hp (97&nbsp;kW) and the [[de Havilland Gipsy Major]] was specifically mentioned.  Though Cole had by this time withdrawn his financial support, things for a short while looked bright for CLW, with talk of an Australian order for 50 Curlews, but they were over extended and went bankrupt.  The sole Curlew, registered ''G-ADYU'' and built at a cost of £10,000 went to Essex Aero Ltd, also of Gravesend, in the asset sale.<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=ADYU  Registration Certificate for ''G-ADYU'']</ref>  It then went to [[Martlesham Heath]] and gained its Certificate of Airworthiness on 19 November 1936. It seems to have done little flying after that.  Stored during the war, it was broken up in 1948.<ref name="OH2587-9"/>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
[[File:Curlew ga.jpg|thumb|right|General arrangement drawing of the Curlew. ''Flight Global archive''<ref>[http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%200700.html ''Flight'' 13 March 1939 p.305]</ref>]]
{{Aircraft specs
|ref={{harvnb|Ord-Hume|2000|pp=299–300}}<!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=2
|capacity=
|length m=
|length ft=21
|length in=6
|length note=
|span m=
|span ft=27
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=9
|height in=0
|height note=
|wing area sqm=
|wing area sqft=116
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=970
|empty weight note=
|gross weight kg=
|gross weight lb=1500
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Pobjoy Niagara|Pobjoy Niagara III]]
|eng1 type=7-cylinder radial engine, geared down by 0.47:1
|eng1 kw=<!-- prop engines -->
|eng1 hp=90<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=120
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=110
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=380
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|armament=<!-- add bulleted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Citations===
{{reflist}}

===Cited sources===
{{refbegin}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6|ref=harv}}
*{{cite book |title=British Piston Engines and their Aircraft|last=Lumsden|first=Alec|year=2003|location=Marlborough, Wiltshire|publisher=Airlife Publishing| isbn=978-1-85310-294-3|ref=harv}}

{{refend}}

<!-- ==External links== -->

[[Category:British aircraft 1930–1939]]