{{Infobox journal
| title         = Fire Safety Journal
| cover = [[File:Fire Safety Journal.gif]]
| editors        = Ann Jeffers, Luke Bisby and Bart Merci
| discipline    = [[Engineering]]
| abbreviation  = Fire Saf. J.
| publisher     = [[Elsevier]]
| country       = Netherlands
| frequency     = 8/year
| history       = 1977-present
| openaccess    =
| license       =
| impact        = 1.259
| impact-year   = 2009
| website       = http://www.elsevier.com/wps/find/journaldescription.cws_home/405896/description#description
| link1         = http://www.sciencedirect.com/science/journal/03797112
| link1-name    = Online access
| link2         =
| link2-name    =
| RSS           = http://rss.sciencedirect.com/publication/science/5723
| atom          =
| JSTOR         =
| OCLC          = 38873366
| LCCN          =
| CODEN         =
| ISSN          = 0379-7112
| eISSN         = 
}}
'''''Fire Safety Journal''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] dealing with original and multidisciplinary research on all aspects of the science and engineering of [[fire]], [[fire safety]] and [[fire protection engineering|fire protection]]. Topics include but are not limited to chemistry and physics of fire, fire dynamics, explosions, fire protection systems, detection, suppression, structural response, structural protection, fire investigations, design (including consumer products, industrial plant, transportation, buildings), people/fire interactions (physical, physiological, and psychological), risk, management, legislation, and education.<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/405896/description#description ''Fire Safety Journal'' website]</ref> It is the official journal of the [[International Association for Fire Safety Science]] and is published by [[Elsevier]].<ref name="home">{{cite web |url=http://www.elsevier.com/wps/find/journaldescription.cws_home/405896/description#description |title=Fire Safety Journal - About |format= |publisher=Elsevier |accessdate=2010-08-25}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[CSA (database company)|Cambridge Scientific Abstracts]], [[Chemical Abstracts Service|Chemical Abstracts]], [[Compendex]], [[Current Contents]]/Engineering, [[CSA (database company)#Engineered Materials Abstracts|Engineered Materials Abstracts]], [[Compendex|Engineering Index]], [[Fire Technology Abstracts]], [[Materials Science Citation Index]], [[PASCAL (database)|PASCAL]], [[Science Citation Index]], and [[Scopus]].<ref name=abstracting>{{cite web |url=http://www.elsevier.com/wps/find/journalabstracting.cws_home/405896/abstracting#abstracting |title=Fire Safety Journal - Abstracting |format= |publisher=Elsevier |accessdate=2010-08-25}}</ref> According to the ''[[Journal Citation Reports]]'', its 2009 [[impact factor]] is 1.259.

==References==
{{reflist}}

== External links ==
* {{official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/405896/description#description}}
* [http://www.iafss.org/ The International Association for Fire Safety Science]

{{Fire protection}}

[[Category:English-language journals]]
[[Category:Fire protection]]
[[Category:Fire prevention]]
[[Category:Wildland fire suppression]]
[[Category:Occupational safety and health journals]]
[[Category:Engineering journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1977]]