{{About|the West African religion||Voodoo (disambiguation)}}
{{See also|Dahomey mythology}}
[[File:Voodo-altar.jpg|thumb|Vodun altar with several fetishes in [[Abomey]], [[Benin]].]]
[[File:Booth at Akodessawa Fetish Market 2016.jpg|thumb|Booth at Akodessawa Fetish Market 2016]]
[[File:Akodessawa Fetish Market 2008.jpg|thumb|Booth at Akodessawa Fetish Market 2008]]
[[File:Booth at Akodessawa Fetish Market 2008.jpg|thumb|Booth at Akodessawa Fetish Market 2008]]
[[File:Akodessawa Fetish Market 2005.jpg|thumb|Booth at Akodessawa Fetish Market 2005]]
[[File:Skulls at Akodessawa Fetish Market 2008.jpg|thumb|Skulls for Voodoo rituals]]
[[File:Akodessawa Fetish Market 2016.jpg|thumb| paraphernalia and dolls for Voodoo]]
[[File:Skulls at Akodessawa Fetish Market 2016.jpg|thumb|Skulls at Akodessawa Fetish Market]]
[[File:Preparation of a bat at Akodessawa Fetish Market for Voodoo rituals.jpg|thumb|Preparation of a bat at Akodessawa Fetish Market for Voodoo rituals]]

'''Vodun''' (meaning ''spirit'' in the [[Fon language|Fon]] and [[Ewe language]]s, {{IPA-ee|vodṹ|pron}} with a [[nasal vowel|nasal]] [[tone (linguistics)|high-tone]] ''u''; also spelled '''Vodon''', '''Vodoun''', '''Vodou''', '''Voudou''', '''Voodoo''', etc.) is practiced by the [[Ewe people|Ewe]] people of eastern and southern [[Ghana]], and southern and central [[Togo]]; and the [[Kabye people|Kabye]] people, [[Gen language|Mina]] people, and [[Fon people|Fon]] people of southern and central [[Togo]], and southern and central [[Benin]]. It is also practiced by some [[Ogu people|Gun]] people of [[Lagos]] and [[Ogun]] in southwest [[Nigeria]]. All the aforementioned peoples belong to [[Gbe languages|Gbe speaking]] ethnic groups of West Africa, except the Kabye. 

It is distinct from the various [[African traditional religion]]s in the interiors of these countries and is the main source of religions with similar names found among the [[African Diaspora]] in the [[New World]] such as [[Haitian Vodou|Haitian ''Vodou'']]; [[Puerto Rican Vodú|Puerto Rican ''Vodú'']]; [[Cuban Vodú|Cuban ''Vodú'']]; [[Dominican Vudú|Dominican ''Vudú'']]; [[Candomblé Jejé|Brazilian ''Vodum'']]; and [[Louisiana Voodoo|Louisiana ''Voodoo'']].
==Theology and practice (False Religion)==
{{Refimprove |date=February 2011}}
[[File:Voodo-fetischmarkt-Lomé.jpg|thumb|A vodoun [[Fetishism|fetish]] market in [[Lomé]], [[Togo]], 2008.]]

Vodun [[Religious cosmology|cosmology]] centers around the ''vodun'' spirits and other elements of divine essence that govern the Earth, a hierarchy that range in power from major deities governing the forces of nature and human society to the spirits of individual streams, trees, and rocks, as well as dozens of ethnic vodun, defenders of a certain clan, tribe, or nation. The ''vodun'' are the center of religious life, similar in many ways to doctrines such as the [[intercession of saints]] and [[angel]]s that made Vodun appear compatible with Christianity, especially [[Catholicism]], and produced syncretic religions such as Haitian Vodou. Adherents also emphasize ancestor worship and hold that the spirits of the dead live side by side with the world of the living, each family of spirits having its own female priesthood, sometimes hereditary when it's from mother to blood daughter.

Patterns of worship follow various dialects, spirits, practices, songs, and rituals.  A [[Creator deity|divine Creator]], called variously [[Mawu|''Mawu'' or ''Mahu'']], is a female being who in one tradition bore seven children and gave each rule over a realm of nature - animals, earth, and sea - or else these children are inter-ethnic and related to natural phenomena or to historical or mythical individuals. The Creator embodies a dual [[Cosmogony|cosmogonic]] principle of which ''Mawu'' the moon and ''Lisa'' the sun are respectively the female and male aspects, often portrayed as the twin children of the Creator.{{citation needed|date=October 2015}}

In other traditions, [[Legba]] is represented as Mawu's masculine counterpart, thus being represented as a [[phallus]] or as a man with a prominent phallus. Dan, who is Mawu's androgynous son, is represented as a rainbow serpent, and was to remain with her and act as a go-between with her other creations. As the mediator between the spirits and the living, Dan maintains balance, order, peace and communication.

All creation is considered divine and therefore contains the power of the divine. This is how medicines such as herbal remedies are understood, and explains the ubiquitous use of mundane objects in religious ritual. Vodun talismans, called "fetishes", are objects such as statues or dried animal or human parts that are sold for their healing and spiritually rejuvenating properties.

===Priestess===

Often described as queen mother is the first daughter of a matriarchal lineage of a family collective. She holds the right to lead the ceremonies incumbent to the clan: marriages, baptisms and funerals. She is considered one of the most important members of community. She will lead the women of a village when her family collective is the ruling one. Her dominant role has often been confused with or associated to that of a high priestess which she is not. They take part in the organisation and the running of markets and are also responsible for their upkeep, which is vitally important because marketplaces are the focal points for gatherings and social centres in their communities. In the past when the men of the villages would go to war, the Queen Mothers would lead prayer ceremonies in which all the women attended every morning to ensure the safe return of their menfolk.

The High priestess is, on the other hand, the woman chosen by the oracle to care for the convent.

Priestesses, like priests, receive a calling from an oracle, which may come at any moment during their lives. They will then join their clan's convent to pursue spiritual instruction. It is also an oracle that will designate the future high priest and high priestess among the new recruits, establishing an order of succession within the convent. Only blood relatives were allowed in the family convent; strangers are forbidden.
In modern days, however, some of the rules have been changed, enabling non family members to enter what is described as the first circle of worship. Strangers are allowed to worship only the spirits of the standard pantheon.

==Relationship to Bò==
{{Refimprove section|date=April 2015}}
Confusion and an amalgam are often made between Vodun and Bò also called O bò or [[Juju]] in Yoruba. Bò is an occult science whose priests are called Bòkônon or Bòkôtônon in opposition to Vodunsi (Vodun female priestess) and Vodunon (Vodun male priest).
Contrary to popular beliefs, in West African Vodun, spells are not cast upon someone. Vodun is a religion in which an important part is devoted to the cult of the ancestors.  Even if the origin of humanity and the world are explained in Vodun mythology, it is not a centered question of the faith. The followers believe that the answer to such question is beyond human reach. Priority is given to the ancestors with them interceding on behalf of their families and descendant towards the Almighty.  While an Almighty creator is recognized in Vodun pantheon, the believers do not address themselves to that particular deity.  Only the Loas, the messengers with the help of the dead have that access. In order to communicate and pray every clan and sometimes each family root have their own Vodun sometimes called Assanyì as Vodun can also be translated as "The spirit of those who have passed before us".  The family Vodun is often associated with a known higher spirit of the standard pantheon, but is distinctive to each family (clan). This distinctiveness is the Clan Vodun is also an assertion of identity and origin with cult and worshiping process specific to a family collective.

The occult science of Bô is not Vodun, although it often summons spirits issued from the Vodun pantheon in its process. The amalgam probably occurred through foreign observation and explanation of the rituals of Vodun. It is due to the fact that Vodun elements can be seen in the rituals of Bò.

The general perception of west African Vodun today is based on a perception of Bò (Juju in Yoruba), European witchcraft and misunderstanding of the practice.

==Demographics==
About 17% of the population of Benin, some 1.6 million people, follow Vodun. (This does not count other traditional religions in Benin.) In addition, many of the 41.5% of the population that refer to themselves as ''Christian'' practice a syncretized religion, not dissimilar from Haitian Vodou or Brazilian [[Candomblé]]; indeed, many of them are descended from freed Brazilian slaves who settled on the coast near Ouidah.<ref name="autogenerated1">{{cite web|url=https://www.cia.gov/library/publications/the-world-factbook/geos/bn.html |title=CIA Fact Book: Benin |publisher=Cia.gov |date= |accessdate=2014-08-10}}</ref>

In [[Togo]], about half the population practices indigenous religions, of which Vodun is by far the largest, with some 2.5 million followers; there may be another million Vodunists among the Ewe of Ghana, as a  13% of the total Ghana population of 20 million are Ewe and 38% of Ghanaians practise traditional religion. According to census data, about 14 million people practise traditional religion in Nigeria, most of whom are Yoruba practising [[Ifá]], but no specific breakdown is available.<ref name="autogenerated1"/>

European [[colonialism]], followed by some of the totalitarian regimes in West Africa, have tried to suppress Vodun as well as other [[African traditional religion|African indigenous religions]].{{citation needed|date=September 2015}} However, because the vodun deities are born to each clan, tribe, and nation, and their clergy are central to maintaining the moral, social and political order and ancestral foundation of its village, these efforts have not been successful. Recently there have been moves to restore the place of Vodun in national society, such as an annual International Vodun Conference held in the city of [[Ouidah]] in Benin that has been held since 1991.<ref>{{cite web
  | last = Forte
  | first = Jung Ran
  | title = Vodun Ancestry, Diaspora Homecoming, and the Ambiguities of Transnational Belongings in the Republic of Benin
  | editors = Percy C. Hintzen, Jean Muteba Rahier, and Felipe Smith (eds.)
  | work = Global Circuits of Blackness. Race, Space, Citizenship and Modern Subjectivities
  | publisher = University of Illinois Press
  | year = 2010
  | url = https://books.google.com/books?id=3r6RdOdjZ3AC&lpg=PA181&dq=ouidah%2092&pg=PA183#v=onepage&q&f=false
  | doi =
}}</ref>

==See also==
{{Portal|Traditional African religion}}
* [[Traditional African religion]]
* [[Akan religion]]
* [[Dahomey mythology]]
* [[Yoruba religion]]
* [[Vodun art]]

==References==
{{Reflist|2}}

==Further reading==
{{refbegin}}
* Ajayi, J.F. and Espie, I. "Thousand Years of West African History" (Ibadan: Ibadan University Press, 1967).
* Akyea, O.E.  "Ewe."  New York: (The Rosen Group, 1988).
* Ayivi Gam l . ''Togo Destination. High Commissioner for Tourism. Republic of Togo'', 1982.
* Bastide. R. ''African Civilizations in the New World''. New York: Harper Torchbooks, 1971.
* Decalo, Samuel. "Historical Dictionary of Dahomey" (Metuchen, N.J: The Scarecrow Press, 1976).
* Deren, Maya.  "''Divine Horsemen: The Living Gods of Haiti''."  (London: Thames and Hudson, 1953).
* "Demoniacal Possession in Angola, Africa". ''Journal of American Folk-lore''. Vol VI., 1893. No. XXIII.
* [[Alfred Burdon Ellis|Ellis, A.B.]] "Ewe-Speaking Peoples of the Slave Coast of West Africa" (Chicago: Benin Press, 1965).
* Fontenot, Wonda. L.  "''Secret Doctors: Enthnomedicine of African Americans''" (Westport: Bergin & Garvey, 1994).
* Hazoum ‚ P. "Doguicimi. ''The First Dahomean Novel''" (Washington, DC: Three Continents Press, 1990).
* Herskovits, M.J. and Hersovits, F.S. ''Dahomey: An Ancient West African Kingdom''. Evanston, IL: Northwestern University,
* Hindrew, Vivian M.Ed., ''Mami Wata: African's Ancient God/dess Unveiled. Reclaiming the Ancient Vodoun heritage of the Diaspora''. Martinez, GA: MWHS.
* Hindrew, Vivian M.Ed., ''Vodoun: Why African-Americans Fear Their Cosmogentic Paths to God''. Martinez, GA. MWHS:
* Herskovits, M.J. and Hersovits, F.S.  "''An Outline of Dahomean Religious Belief''" (Wisconsin: The American Anthropological Association, 1933).
* Hurston, Zora Neale.  "''Tell My Horse: Voodoo And Life In Haiti And Jamaica''." Harper Perennial reprint edition, 1990.
* Hyatt M. H.  "''Hoodoo-Conjuration-Witchcraft-Rootwork''" (Illinois: Alama Egan Hyatt Foundation, 1973), Vols. I-V.
* Journal of African History. 36. (1995) pp.&nbsp;391–417.''Concerning Negro Sorcery in the United States;
* ''Language Guide'' (Ewe version). Accra: Bureau of Ghana Languages,
* Maupoil, Bernard.  "''La Geomancie L'ancienne des Esclaves''" (Paris: L'université de Paris, 1943).
* Metraux, Alfred. "''Voodoo In Haiti''." (Pantheon reprint edition, 1989)
* Newbell, Pucket. N. "''Folk Beliefs of the Southern Negro''". S.C.: Chapel Hill, 1922.
* Newell, William, W. "''Reports of Voodoo Worship in Hayti and Louisiana''," Journal of American Folk-lore, 41-47, 1888. p.&nbsp;41-47.
* Barreiro, Daniel, Garcia, Diego. "''Nuit: una vision de la continuidad ancestral (spanish edition)''". Montevideo, Uruguay, 2014
* Pliya, J. "''Histoire Dahomey Afrique Occidental''" (Moulineaux: France, 1970).
* ''Slave Society on the Southern Plantation''. The Journal of Negro History. Vol. VII-January, 1922-No.1.
{{refend}}

==External links==

{{Commons category|West African Vodun}}
{{wikinews|Voodoo sex ritual leaves woman dead}}
* [http://www.afrikaworld.net/afrel/zinzindohoue.htm Traditional Religion in Africa:The Vodun Phenomenon in Benin]
* [http://rara.wesleyan.edu/ Vodou-related Rara festivals in Haiti and New York]
* [http://www.npr.org/templates/story/story.php?storyId=1666721 Voodoo and West Africa's Spiritual Life]
* [http://www.africanholocaust.net/news_ah/vodoo.htm Vodun in Africa]
{{Afro-American Religions}}

[[Category:Vodou]]
[[Category:Dahomey]]
[[Category:African traditional religions|Vodun]]