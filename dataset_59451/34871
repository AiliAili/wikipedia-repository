{{good article}}
{{Infobox hurricane season
| Basin=Atl
| Year=1917
| Track=1917 Atlantic hurricane season summary map.png
| First storm formed=July 6, 1917
| Last storm dissipated=October 20, 1917
| Strongest storm name="[[1917 Nueva Gerona hurricane|Nueva Gerona]]" 
| Strongest storm winds=130
| Strongest storm pressure=928
| Average wind speed=
| Total depressions=9
| Total storms=4
| Total hurricanes=2
| Total intense=2
| Fatalities=47 total
| Damages=.17
| Inflated=0
| five seasons=[[1915 Atlantic hurricane season|1915]], [[1916 Atlantic hurricane season|1916]], '''1917''', [[1918 Atlantic hurricane season|1918]], [[1919 Atlantic hurricane season|1919]]
}}
<!-- Created with subst: of [[Template:Hurricane season single]]. -->The '''1917 Atlantic hurricane season''' featured nine known [[tropical cyclone]]s, four of which made [[Landfall (meteorology)|landfall]].<ref name="documentation"/> The first system appeared on July&nbsp;6 east of the [[Windward Islands]]. After crossing the islands and traversing the [[Caribbean Sea]], the storm struck [[Honduras]], [[Belize]], and [[Mexico]], before dissipating on July&nbsp;14. After more than three weeks without [[tropical cyclogenesis]], another tropical storm developed west of [[Bermuda]]. As the storm brushed eastern [[New England]], four ships sank near [[Nantucket]], causing 41&nbsp;fatalities. The same cyclone brought damaging winds to [[Nova Scotia]] before transitioning into an extratropical cyclone on August&nbsp;10.

A hurricane developed over the central [[Atlantic Ocean]] on August&nbsp;30 and ultimately affected [[Bermuda]] with heightened tides as it passed to the east. Elsewhere, the hurricane had little impact, becoming extratropical on September&nbsp;5. After the third system, a series of four tropical depressions formed, but failed to become severe. The [[1917 Nueva Gerona hurricane|fourth hurricane]] brought devastation to [[Jamaica]], [[Cuba]], and portions of the [[Gulf Coast of the United States]], especially western parts of the [[Florida Panhandle]]. Overall, the storm left six deaths and inflicted at least $170,000 (1917&nbsp;[[United States dollar|USD]]) in damage.

The season's activity can be quantified in an [[accumulated cyclone energy]] (ACE) rating of 61.<ref name="ACE">{{cite report|work=[[Hurricane Research Division]]; [[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|date=February 2014|title=Atlantic basin Comparison of Original and Revised HURDAT|accessdate=July 13, 2014|url=http://www.aoml.noaa.gov/hrd/hurdat/comparison_table.html|location=Miami, Florida}}</ref> ACE is, broadly speaking, a measure of the power of the hurricane multiplied by the length of time it existed; therefore, storms that last a long time, as well as particularly strong hurricanes, have high ACE ratings.<ref>{{cite report|author=David Levinson|date=August 20, 2008|title=2005 Atlantic Ocean Tropical Cyclones|publisher=National Oceanic and Atmospheric Administration|work=[[National Climatic Data Center]]|accessdate=July 13, 2014|url=http://www.ncdc.noaa.gov/oa/climate/research/2005/2005-atlantic-trop-cyclones.html|location=Asheville, North Carolina}}</ref>

==Systems==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/06/1917 till:01/11/1917
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/06/1917

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:06/07/1917 till:14/07/1917 color:TS text:"Tropical Storm #1" (TS)
  from:06/08/1917 till:10/08/1917 color:TS text:"Tropical Storm #2" (TS)
  from:30/08/1917 till:05/09/1917 color:C3 text:"Hurricane #3" (C3)
  from:12/09/1917 till:14/09/1917 color:TD text:"TD"
  from:12/09/1917 till:16/09/1917 color:TD text:"TD"
  from:14/09/1917 till:14/09/1917 color:TD text:"TD"
  from:14/09/1917 till:15/09/1917 color:TD text:"TD"
  from:20/09/1917 till:30/09/1917 color:C4 text:"[[1917 Neuva Gerona hurricane|Hurricane #4]]" (C4)
  from:19/10/1917 till:20/10/1917 color:TD text:"TD"
  
  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/06/1917 till:01/07/1917 text:June
  from:01/07/1917 till:01/08/1917 text:July
  from:01/08/1917 till:01/09/1917 text:August
  from:01/09/1917 till:01/10/1917 text:September
  from:01/10/1917 till:01/11/1917 text:October
TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

===Tropical Storm One===
{{Infobox Hurricane Small
| Basin=Atl
| Image=Tropical Storm One surface analysis 12 Jul 1917.jpg
| Track=1917 Atlantic tropical storm 1 track.png
| Formed=July 6
| Dissipated=July 14
| 1-min winds=45
| Pressure=<1006
}}
Historical weather maps indicated a [[Trough (meteorology)|trough]] east of the [[Windward Islands]] on July&nbsp;6.<ref name="documentation">{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_dec12.html|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|author=Christopher W. Landsea|date=December 2012|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|accessdate=July 11, 2014|location=Miami, Florida|display-authors=etal}}</ref> Around 06:00&nbsp;[[Coordinated Universal Time|UTC]], a tropical depression formed about {{convert|80|mi|km|abbr=on}} southeast of [[Barbados]]. Moving west-northwestward, the depression crossed the Windward Islands and entered the [[Caribbean Sea]] early on July&nbsp;7. Later that day, the system intensified into a tropical storm. After peaking with winds of 50&nbsp;mph (85&nbsp;km/h) on July&nbsp;8, it began to weaken and fell to tropical depression intensity early on July&nbsp;10. Early the next day, the depression struck [[Honduras]], before briefly emerging into the Caribbean Sea. The system struck [[Belize]] around 06:00&nbsp;UTC on July&nbsp;12 and entered the [[Bay of Campeche]] about 24&nbsp;hours later. At midday on July&nbsp;14, the storm struck south of [[Tampico]], [[Tamaulipas]].{{Atlantic hurricane best track}} In the city of [[Veracruz, Veracruz|Veracruz]], {{convert|4|in|mm|abbr=on}} of rain fell in a 24-hour period.<ref name="documentation"/>
{{clear}}

===Tropical Storm Two===
{{Infobox Hurricane Small
| Basin=Atl
| Image=Tropical Storm Two surface analysis 9 Aug 1917.jpg
| Track=1917 Atlantic tropical storm 2 track.png
| Formed=August 6
| Dissipated=August 10
| 1-min winds=60
| Pressure=<994
}}
An [[Low-pressure area|area of low pressure]] developed into a tropical storm to the west of Bermuda at 00:00&nbsp;UTC on August&nbsp;6.<ref name="documentation"/>{{Atlantic hurricane best track}} Initially the storm drifted westward and strengthened minimally. By August&nbsp;9, it curved northward and began to accelerate. The storm strengthened further and attained its peak intensity with maximum sustained winds of 70&nbsp;mph (110&nbsp;km/h) and a minimum barometric pressure of {{convert|994|mbar|inHg|abbr=on|lk=on}} early on August&nbsp;10;{{Atlantic hurricane best track}} both were observations from the Nantucket Shoals Lightship. Offshore the island of [[Nantucket]] in [[Massachusetts]], four ships were lost, with an estimated 41&nbsp;people perishing.<ref name="documentation"/> The storm continued northeastward and made landfall in [[Saint John County, New Brunswick]] while becoming extratropical at 00:00&nbsp;UTC on August&nbsp;11. The remnants dissipated over the [[Labrador Sea]] later that day.{{Atlantic hurricane best track}} In [[Nova Scotia]], dozens of boats were beached. Wind speeds up to {{convert|64|mph|km/h|abbr=on}} in [[Yarmouth, Nova Scotia|Yarmouth]] damaged trees, power lines, windows, and roofs. Crops were impacted throughout the [[Annapolis Valley]]. Fifteen barns were damaged or demolished.<ref>{{cite report|url=http://www.ec.gc.ca/Hurricane/default.asp?lang=en&n=5752F8A2-1|title=1917-1|date=November 20, 2009|publisher=[[Environment Canada]]|accessdate=July 12, 2014|location=Moncton, New Brunswick}}</ref>
{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
| Basin=Atl
| Image=Hurricane Three surface analysis 4 Sept 1917.jpg
| Track=1917 Atlantic hurricane 3 track.png
| Formed=August 30
| Dissipated=September 5
| 1-min winds=105
| Pressure=<980
}}
Weather maps indicated that an [[Low-pressure area|area of low pressure]] existed between the [[Lesser Antilles]] and [[Cape Verde]] on August&nbsp;30. At 12:00&nbsp;UTC that day, a tropical storm developed while located about {{convert|900|mi|km|abbr=on}} northeast of [[Cayenne]], [[French Guiana]].<ref name="documentation"/>{{Atlantic hurricane best track}} Moving west-northwestward, the tropical storm reached Category&nbsp;1 hurricane intensity early on August&nbsp;31. By late on September&nbsp;1, the storm had intensified into a Category&nbsp;2 hurricane. Further strengthening occurred brought the hurricane to a Category&nbsp;3 intensity late on September&nbsp;3, while briefly heading northward. Early the next day, the cyclone attained its peak force with winds of 120&nbsp;mph (195&nbsp;km/h); the lowest barometric pressure reading in association with the storm was {{convert|980|mbar|inHg|abbr=on}}.{{Atlantic hurricane best track}}

While passing east of [[Bermuda]] late on September&nbsp;4, the storm's "unprecedented high tides" lashed the island. Portions of Market Square on [[St. David's Island, Bermuda|St. David's Island]] were submerged. Additionally, a large section of [[Higgs' Island, Bermuda|Higgs' Island]] was completed washed away.<ref name="documentation"/> The storm accelerated to the northeast and weakened to a Category&nbsp;2 hurricane around 12:00&nbsp;UTC on September&nbsp;5. Losing tropical characteristics, the hurricane transitioned into an extratropical cyclone at 00:00&nbsp;UTC the following day, while located about {{convert|290|mi|km|abbr=on}} southeast of [[Cape Race]], [[Newfoundland (island)|Newfoundland]]. Its remnants tracked northeastward until dissipating near [[Iceland]] on September&nbsp;7.{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Four===
{{Infobox Hurricane Small
| Basin=Atl
| Image=Hurricane Four surface analysis 25 Sept 1917.jpg
| Track=1917 Atlantic hurricane 4 track.png
| Formed=September 20
| Dissipated=September 30
| 1-min winds=130
| Pressure=928}}
{{Main article|1917 Nueva Gerona hurricane}}
A tropical storm formed about {{convert|160|mi|km|abbr=on}} east-northeast of Barbados on September&nbsp;20. Later that day, the storm crossed the Lesser Antilles between the islands of [[Saint Lucia]] and [[Martinique]]. Upon entering the Caribbean Sea on September&nbsp;21, the system intensified quicker, becoming a Category&nbsp;1 hurricane several hours later. While passing south of the [[Tiburon Peninsula, Haiti|Tiburon Peninsula]] on the following day, the storm strengthened into a Category&nbsp;2 hurricane. Later on September&nbsp;23, the hurricane struck the northern coast of Jamaica, before re-emerging into the Caribbean Sea. Around 06:00&nbsp;UTC on September&nbsp;24, it intensified into a Category&nbsp;3 hurricane. Approximately 24&nbsp;hours later, the hurricane deepened into a Category&nbsp;4 hurricane.{{Atlantic hurricane best track}}

Ramón Perez of the Instituto de Meteorología de la República de Cuba estimated that this hurricane attained a minimum barometric pressure of {{convert|928|mbar|inHg|abbr=on}} on September&nbsp;25. Using the pressure-wind relationship, HURDAT increased the maximum sustained wind speed to 150&nbsp;mph (240&nbsp;km/h).<ref name="documentation"/> Within the next six hours, the hurricane made landfall in the eastern [[Pinar del Río Province]] of [[Cuba]]. The system entered the Gulf of Mexico later on September&nbsp;25. While moving northwestward, the storm fell to Category&nbsp;3 hurricane intensity early on September&nbsp;27. It briefly tracked generally northward and approached southeastern Louisiana before re-curving northeastward late the next day. At 02:00&nbsp;UTC on September&nbsp;29, the hurricane made landfall near [[Fort Walton Beach, Florida]] with winds of 115&nbsp;mph (185&nbsp;km/h). Shortly thereafter, the system rapidly weakened to a Category&nbsp;1 hurricane and fell to a tropical storm several hours later. Early on September&nbsp;30, it became extratropical over [[Georgia (U.S. state)|Georgia]]. About six hours later, the remnants of the hurricane dissipated.{{Atlantic hurricane best track}}

Some islands in the Lesser Antilles experienced strong winds and heavy rainfall, including [[Dominica]], [[Guadeloupe]], and Saint Lucia.<ref name="documentation"/> In Jamaica, the hurricane caused significant damages to [[Musa (genus)|banana]] and [[coconut]] plantations. Communications from Holland Bay were disrupted when the station was demolished.<ref name="DailyGleaner">{{cite news|newspaper=[[The Daily Gleaner]]|year=1917|title=Storm News Received From the Various Districts}}</ref> The greatest damages were reported from the northern half of the island. In [[Nueva Gerona]], strong winds destroyed well-constructed buildings, devastating the town. Orchards and crops were destroyed on the Pinar del Río Province.<ref name="mwr">{{cite report|url=http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1917.pdf|title=Section III. &ndash; Forecasts|author=Henry C. Frankenfield|date=October 1, 1917|work=[[National Weather Service|Weather Bureau]]|publisher=National Oceanic and Atmospheric Administration|accessdate=July 12, 2014|location=Washington, D.C.|format=PDF}}</ref> In [[Louisiana]] and [[Mississippi]], impact was generally limited to damaged crops and timber stands.<ref name="documentation"/> One death from drowning was reported in Louisiana.<ref name="mwr"/> Farther east in [[Mobile, Alabama]], portions of roofs, trees, and other debris littered streets.<ref name="Galveston">{{cite news|newspaper=[[The Daily News (Texas)|The Daily News]]|year=1917|title=Reports Indicate Slight Life Loss}}</ref> Communications were severed in [[Pensacola, Florida]]. Several small watercrafts washed ashore, and numerous wharves, docks, and boat storages suffered impact. Total damages were estimated near $170,000 in Pensacola area. Five deaths were reported in Florida, all of them in the city of [[Crestview, Florida|Crestview]].<ref name="mwr"/>

===Tropical depressions===
In addition to the four systems reaching tropical storm intensity, there were five other tropical depressions. The first probably developed from a [[tropical wave]] offshore western Africa on September&nbsp;12, according to historical weather maps and the [[International Comprehensive Ocean-Atmosphere Data Set|Comprehensive Ocean-Atmosphere Data Set]] (COADS), both of which indicated a closed [[low pressure system|circulation]]. This depression was last observed on September&nbsp;14. Another depression also developed on September&nbsp;12, well to the east of the Lesser Antilles. While approaching the islands, the storm may have briefly strengthened into a tropical storm on September&nbsp;15, as suggested by a ship report of winds blowing 40&nbsp;mph (65&nbsp;km/h). The cyclone likely dissipated on September 15.<ref name="documentation"/>

The next tropical depression briefly existed off the east coast of Florida on September&nbsp;14. It soon became extratropical, though its remnants lasted several days, traveling along the [[East Coast of the United States]]. At Nantucket, a wind gust up to {{convert|69|mph|km/h|abbr=on}} was observed. The extratropical remnants probably dissipated near Newfoundland on September&nbsp;20. Meanwhile, a trough in the Gulf of Mexico had developed into a tropical depression on September&nbsp;14. The following day, the system made landfall in Louisiana and disintegrated into a disorganized area of thunderstorms over Mississippi and [[Arkansas]] on September&nbsp;16. The last tropical depression and final cyclone of the season developed on October&nbsp;19 from a trough over the central Atlantic Ocean. Two days later, the depression was absorbed by a [[cold front]].<ref name="documentation"/> 
{{clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of tropical cyclones]]
*[[List of Atlantic hurricane seasons]]

==References==
{{Reflist}}

==External links==
{{Commons category|1917 Atlantic hurricane season}}
*[http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1917.pdf Monthly Weather Review]

{{TC Decades|Year=1910|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1917 Atlantic Hurricane Season}}
[[Category:1910–1919 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]