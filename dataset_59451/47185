{{distinguish|Le Concert des Nations}}
{{Multiple issues|
{{orphan|date=June 2012}}
{{original research|date=May 2012}}
{{essay|date=May 2012}}
}}

The '''Concert of Nations''' is a set of political beliefs that emerged in the nineteenth century at the Congress of Vienna but continue to be influential for international relations even through the present day.  The ideas behind the Concert of Nations are rooted in seventeenth century political philosophy of harmonism, which included music, politics, religion, science, and the entire universe (physical and metaphysical).  The [[Congress of Vienna]] enacted the principles of the Concert in both musical entertainment and political relations.  The stability of Europe during the decades following the Congress of Vienna has led to widespread popularity of the idea of a Concert of Nations, though its (largely theoretical) application vary widely in their interpretations of what a Concert of Nations means for present-day international relations.

==Philosophical foundations==
The Concert of Nations emerged in the nineteenth century as the ideal for international relations in Europe.  In order to understand its practical and theoretical uses, it is important to review the philosophical history behind its formation.  By its historical definitions, ''[[concert]]'' can mean a musical performance event, or it can designate the act of cooperation between multiple parties pursuing a single enterprise.  Which meaning is primary and which is secondary has varied through time, and to this day there is no consensus on the true origin of the word.  The [[Great Chain of Being]] also provides much-needed context for understanding what the Concert of Nations meant to nineteenth century politicians.  This philosophy declared that every being has its place under God’s rule, and that harmony between all creatures is only achieved when each accepts the duties and joys to which it has been designated.  Music served as an example for pieces working together, from the overtones of the planets’ motion to the ordered motion of musical voices in an ensemble.  Not everyone is a soloist, but everyone’s part, in its own way, is essential to all the rest.

==Concert of Europe==
{{main article|Concert of Europe}}
Since the Concert of Nations was conceptualized in such strongly musical terms, these competing definitions of the concert did not seem so far apart for citizens of the nineteenth century as they do for us today.  The [[Congress of Vienna]] in 1815 is the seminal example of putting Concert ideologies to work.   Not only did diplomats establish a system to maintain European political stability, but they did so in a way that emphasized harmony over power.  Much of the significant negotiations happened around music or dancing rather than just formal meetings.<ref>Biba, Otto. "The Congress of Vienna and Music." In ''Denmark and the Dancing Congress of Vienna: Playing for Denmark's Future'', (Copenhagen: Den Kongelige Udstillingsfond, 2002), 200-214.</ref><ref>Kraehe, Enno. "The Congress of Vienna." In ''Schubert's Vienna'', ed. Raymond Erickson, (New Haven: Yale University Press, 1997), 55-76.</ref> While every nation had its own agenda to pursue and many less-powerful voices were lost in the concert, diplomats worked to blend interests within the common goal of maintaining an imperialist structure.

==Historical uses==
Given the great success of the Congress of Vienna, many politicians have resurrected the idea of a Concert of Nations in order to support their contemporary agendas.  Two excellent examples of this come from U.S. Senator [[Joseph I. France|France]] in 1921 and again from Senator [[J. William Fulbright|Fulbright]] in 1961.  Both of these cases face the difficulty of adapting concert politics to the twentieth century while disassociating themselves from its imperialist roots.

===Post-World War I===

In 1921, Senator France proposed a “Concert of Nations” to replace the League of Nations.<ref>France, Joseph Irwin. “The Concert of Nations.” ''Annals of the American Academy of Political and Social Science'' 96, no. 4 (1921): 141-146, [http://www.jstor.org/stable/1014886]</ref> In his mind, this Concert would be more effective than both the Concert of Europe or the League of Nations because it would be rooted in democratic (and very American) values.  His article demonstrates the way that politicians already looked up to the stability achieved by the Concert of Europe, but for the most part it bends concert ideology to match Senator France’s existing political agenda.

===Cold War===

The same problem resurfaces in the Cold War, when Senator Fulbright proposes an “Concert of Free Nations".<ref>Fulbright, J.W.  “For a Concert of Free Nations.”  ''Foreign Affairs'' 40, no. 1 (1961): 1-18, [http://www.jstor.org/stable/20029528]</ref> While Senator Fulbright does better research into the details of the Concert of Europe, he runs into similar trouble with how to make use of concert politics within a democratic sphere.  He sees a contemporary concert including an inner circle of powerful democracies and an outer circle of less significant countries.  In some ways, this strongly imitates the original Concert ideologies by allowing for differences in degree of influence in the concert.  But it also leaves out very significant world powers who ascribe to communist values.

==Contemporary politics==
In contemporary politics, both first-world powers and developing nations use Concert of Nations as a tool to promote their political agenda on the international stage.

===Concert of Democracies===

{{main article|Concert of Democracies}}
The [[Concert of Democracies]] seems to follow closely on the heels of both Senator France and Senator Fulbright by proposing a concert limited to democratic countries that pursues democratic ideals like human rights and self-government.  But in a similar vein, it also fails to address the significant ways in which it diverges from the Concert of Nations as understood by the Congress of Vienna: namely, its members are dependent on democratic support and it leaves out very powerful undemocratic nations.  In practice, this means that the members of a Concert of Democracies would be subject to popular opinion for their continued participation, rather than on their imperialist good judgment.  It also divides the world based on democratic or nondemocratic values rather than bringing differences together into balance.<ref>''Economist.''  “Concert of Democracies: A seductive sound.”  July 7, 2007. [http://www.economist.com/node/9304295/]</ref>

===Harmonious Society===

{{main article|Socialist Harmonious Society}}
From an entirely different tradition, China’s [[Socialist Harmonious Society]] proposes similar values as the nineteenth century's Concert of Nations.  A discourse balanced between international cooperation and pursuit of power in international relations make this policy seem the most similar to the nineteenth century conception of working “in concert”.<ref>Zheng, yongnian, and Sow Keat Tok.  “‘Harmonious society’ and ‘Harmonious World’: China’s Policy Discourse under Hu Jintao,” in Briefing Series 26, (Nottingham: China Policy Institute, October 2007). [http://nottingham.ac.uk/cpi/documents/briefings/briefing-26-harmonious-society-and-harmonious-world.pdf]</ref>

===Developing countries===

Amidst developing countries, the Concert of Nations appears as a marker of identity, influence, and power in the international sphere.  [[ASEAN]], the Association of Southeast Asian Nations, defines itself as a Concert of southeast Asian nations as a means to set the vision for their internal workings and to brand themselves for global relations.<ref>[http://www.aseansec.org/1814.htm Association of Southeast Asian Nations.  "ASEAN Vision 2020"]</ref> Many African nations have also brought the Concert of Nations into their political rhetoric, most often in reference to taking their place or having their voice heard in the concert.  Both of these contexts seem very aware of the power dynamics that surround use of the Concert as a political structure.  As a rhetorical device, the  concert provides a way to assert one’s identity as a nation without claiming to do so at anyone’s expense.<ref>Kendemeh, Emmanuel. “Cameroon’s Voice Prominent in Concert of Nations.” ''Cameroon Tribune'', August 25, 2010, [http://allafrica.com/stories/201008250701.html].</ref><ref>Embassy of the Republic of Angola Japan. “Speech Delivered By His Excellency Jose Eduardo Dos Santos, President Of The Republic Of Angola, At The Opening Ceremony Of The National Assembly.” October 27, 2010, [http://www.angola.or.jp/english/index.php/events/p1/28].</ref>

==Musical groups==
Finally, musical groups also continue to draw from the nineteenth-century concert ideas by bringing together musicians from different countries to pursue literal and figurative harmony.  Two significant examples of this are the [[West-Eastern Divan]] and [[Le Concert des Nations]].  The West-Eastern Divan explicitly explores using music to cross boundaries of political conflict by bringing together young musicians from Israel, Palestine, and Spain on an orchestral tour every summer.  Le Concert des Nations works with period instruments, but it also intentionally gathers a highly international group of performers.  These groups demonstrate the sustained symbolic relationship between musical and political discourses, though they have somewhat diverged from their nineteenth century unity.

==References==
{{reflist}}

[[Category:International relations]]