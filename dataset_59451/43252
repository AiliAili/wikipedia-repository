<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = Messenger
 |image = Mess4a.jpg
 |caption = Miles M.38 Messenger 2A
}}{{Infobox Aircraft Type
 |type = Liaison and private owner aircraft
 |manufacturer = [[Miles Aircraft]]
 |designer =
 |first flight = 12 September 1942
 |introduced =
 |retired =
 |produced = 1942-1948
 |number built = 93
 |status = examples still flying
 |unit cost =
 |primary user = [[Royal Air Force]]
 |more users = Private owner pilots
 |developed from = [[Miles Mercury|Miles M.28 Mercury]]
 |variants with their own articles =
}}
|}
The '''Miles M.38 Messenger''' is a [[United Kingdom|British]] four-seat liaison and private owner aircraft built by [[Miles Aircraft]].

==Design and development==
[[File:Miles Messenger (VH-ZZM) at an airshow in Australia in 1998.jpg|thumb|left|A surviving Messenger in 1998, painted to represent Field Marshal Montgomery's aircraft]]
The Messenger was designed to meet a [[British Army]] requirement for a robust, slow speed, low maintenance air observation post and liaison aircraft.

The aircraft designed was a cantilever low-wing monoplane with a fixed tailwheel, powered by the [[de Havilland Gipsy Major|de Havilland Gipsy Major 1D]] inline engine.

Fitted with retractable auxiliary [[Flap (aircraft)|wing flap]]s enabling a wing loading of around 12.5&nbsp;lb per square foot, the Messenger featured triple fins and rudders in order to maintain sufficient controllability down to the exceptionally [[Stall (flight)|low stalling speed]] of 25&nbsp;mph.<ref>http://www.flightglobal.com/pdfarchive/view/1945/1945%20-%200710.html</ref>

The prototype was converted from a [[Miles Mercury|Miles M.28 Mercury]] and first flew at [[Woodley, Berkshire|Woodley]] on 12 September 1942, some three months after an approach by army officers.<ref name=":0">{{Cite book|title = The Hamlyn Concise Guide to British Aircraft of World War II|last = Mondey|first = David|publisher = Chancellor Press|year = 1982|isbn = 1-85152-668-4|location =|pages = 174}}</ref> When informally test flown by an Aerial Observation Post Squadron it was declared a success, meeting all the army's requirements, however the [[Ministry of Aircraft Production]], having not been consulted, reprimanded George Miles for failing to seek the ministry's permission before rebuilding the aircraft and no orders for the Miles M.38 were placed for the aerial observation post role, AOP units using light, fixed-wing aircraft, notably various marks of [[Auster]].

During the war George Miles continued to experiment with the prototype and suggested the aircraft (known as the M.38A) could be operated in the anti-submarine role using a small 60 foot (18.29 metre) deck aboard small merchant ships, landing using a simple arrester wire system. This was tried at Woodley using a simulated deck (with passengers simulating the weight of depth charges). Perhaps predictably, no official interest was expressed in this scheme.<ref name=":0" />

However a year later a small order against Specification 17/43 was placed on behalf of the British [[Royal Air Force]] for the '''Messenger I''' to be employed in the VIP transport passenger transport role.<ref>Mondey</ref>

Wartime users of the type included [[Marshal of the Royal Air Force]] [[Arthur Tedder, 1st Baron Tedder|1st Baron Tedder]] and [[Field marshal|Field Marsha]]<nowiki/>l [[Bernard Montgomery, 1st Viscount Montgomery of Alamein|Sir Bernard Montgomery]]. (The Messenger shown at the beginning of this article - [[c:File:Aircraft_of_the_Royal_Air_Force_1939-1945-_Miles_M.38_Messenger_B7065.jpg|in the guise of this aircraft]] - attended the 60th [[D-day|D-Day]] commemorations at the [[Imperial War Museum Duxford]] in 2004.)

==Production and operations==
[[File:Miles M.38 Messenger 2A PH-NDR WW 10.09.54 edited-2.jpg|thumb|right|Messenger 2A registered in the [[Netherlands]] at [[White Waltham Airfield]] in 1954]]
During the war years of the 21 Messengers produced seventeen Messenger 1s survived, and when retired from RAF duties most were converted for civilian use as the ''Messenger 4A''.<ref name="Jackson">Jackson</ref> They were flown by private pilots and business owners.

In 1944 the prototype was rebuilt as the '''M.48 Messenger 3''' with fully retractable electrically-operated split trailing-edge flaps and a 155 h.p.&nbsp;Cirrus Major III engine. This model was not further developed as it did not provide any advantage over the other variants.<ref>Simpson</ref>

Post-war production centred on the '''Messenger 2A''' for the civilian market, aircraft being built at [[Newtownards]] in Northern Ireland and flown to Woodley for final fitting out. After 71 aircraft were built, production ceased in 1948; a single example was assembled, from existing parts, in 1950.

Several examples of the type were sold to Australia and others were exported to Argentina, Belgium, Chile, Egypt, Iran, New Zealand, South Africa and Switzerland.<ref name="Jackson"/>

The aircraft was a popular touring and racing aircraft during the late 1940s and early 1950s. In 1954 Harold Wood in ''G-AKBO'' won the [[King's Cup Race]] air race at 133&nbsp;mph.<ref name="Jackson"/>

Several examples were still flying in the United Kingdom and New Zealand in early 2011 with private owners and flying groups.

==Variants==
;Messenger 1
:Military production aircraft for the Royal Air Force powered by a Gipsy Major ID, 23 built.
;Messenger 2A
:Civil production aircraft powered by the Blackburn Cirrus Major 3, 65 built.
;Messenger 2B
:Three-seat variant of the 2A powered by the Blackburn Cirrus Major 3, one built.
;Messenger 2C
:Same as the 2A but powered by the de Havilland Gipsy Major 1D, one built.
;Messenger 3
:Dual-control variant of the 2A powered by a Blackburn Cirrus Major 3, one built, later re-designated the M.48.
;Messenger 4
:Same as the 2A but powered by the de Havilland Gipsy Major 10, three built.
;Messenger 4A
:Civil version powered by the de Havilland Gipsy Major 1D, one built and 19 converted from Mk I.
;Messenger 4B
:One 4A modified with a de Havilland Gipsy Major 10 engine.
;Messenger 5
:One I modified with a Blackburn Bombardier 702 engine.
;M.38A Mariner
:The prototype Messenger was fitted with an arrester hook for trials as a carrier-based anti-submarine aircraft.
;Handley Page HP.93
:One Messenger was used by Handley Page (Reading) for test flights using Dufaylite wings at Woodley.

==Operators==
;{{UK}}
* [[Royal Air Force]]
* Boston Air Transport
* Patrick-Duval Aviation
* Tyne Taxis Ltd
* Ulster Aviation

==Specification (Messenger 2A)==
{{Aircraft specs
|ref=For Business And Pleasure<ref name="am1186 p603">Jerram ''Aeroplane Monthly'' November 1986, p. 603.</ref>
|prime units?=imp 
<!--
        General characteristics
-->
|genhide= 

|crew=1
|capacity=3 passengers
|length m=
|length ft=24
|length in=0
|span m=
|span ft=36
|span in=2
|height m=
|height ft=9
|height in=6
|wing area sqm=
|wing area sqft=191
|aspect ratio=6.8:1
|empty weight kg=
|empty weight lb=1438
|gross weight kg=
|max takeoff weight lb=2400
|fuel capacity=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Cirrus Major]] III
|eng1 type=air-cooled 4-cylinder [[inline engine (aeronautics)|inline engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=155<!-- prop engines -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=135
|max speed note=at sea level
|cruise speed kmh=
|cruise speed mph=124
|stall speed mph=25
|stall speed note= (approx.)
|range km=
|range miles=460
|range nmi=
|endurance=5 hr 12 minutes
|ceiling m=
|ceiling ft=16000
|climb rate ms=
|climb rate ftmin=750
|more performance=*'''Take-off run to 50 ft (15 m):''' {{convert|750|ft|m|abbr=on}}
*'''Landing run from 50 ft (25 m):''' {{convert|650|ft|m|abbr=on}}

|avionics=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
|sequence=
|lists=
* [[List of aircraft of World War II]]
* [[List of aircraft of the RAF]]
|see also=
}}

==References==
{{Commons category}}

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Amos, Peter. and Brown, Don Lambert. ''Miles Aircraft Since 1925, Volume 1''. London: Putnam Aeronautical, 2000. ISBN 0-85177-787-2.
* Brown, Don Lambert. ''Miles Aircraft Since 1925''. London: Putnam & Company Ltd., 1970. ISBN 0-370-00127-3.
* Jackson, A.J. ''British Civil Aircraft since 1919''. London: Putnam & Company Ltd., 1974. ISBN 0-370-10014-X.
* Jerram, Mike. "For Business and Pleasure–No. 4: Part 1". ''[[Aeroplane Monthly]]'', Vol. 14, No. 10, October 1986. pp. 558–561. {{ISSN|0143-7240}}
* Jerram, Mike. "For Business and Pleasure–No. 4: Part Two". ''Aeroplane Monthly'', Vol. 14, No. 11, November 1986. pp. 600–603. {{ISSN|0143-7240}}
* Mondey, David. ''The Hamlyn Concise guide to British Aircraft of World War II''. London: Chancellor press, 2002. ISBN 1-85152-668-4.
* Simpson, Rod, ''General Aviation Handbook''. Midland Publishing. 2005. ISBN 1-85780-222-5
{{refend}}

{{Miles aircraft}}

[[Category:Miles aircraft]]
[[Category:British civil utility aircraft 1940–1949]]
[[Category:British military utility aircraft 1940–1949]]