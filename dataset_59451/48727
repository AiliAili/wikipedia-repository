{{Infobox Government agency
|agency_name     = Inland Empire Utilities Agency
|logo            = Inland Empire Utilities Agency logo.png
|logo_width      = 220 px
|logo_caption    = Logo of IEUA
|picture         =
|picture_width   =
|picture_caption =
|formed          = 1950 Chino Basin Municipal Water District (CBMWD)<br> 1998 Name changed to: Inland Empire Utilities Agency (IEUA)
|preceding1      = 
|dissolved       =
|superseding     =
|jurisdiction    = [[Special-purpose district|Special District]]
|headquarters    = 6075 Kimball Ave., Chino, CA 91708
|latd            =
|latm            =
|lats            =
|latNS           =
|longd           =
|longm           =
|longs           =
|longEW          =
|region_code     =
|employees       =
|budget          =
|chief1_name     = Joe Grindstaff
|chief1_position = General Manager
|parent_agency   =
|child1_agency   = 
|website         = [http://www.ieua.org/ ieua.org]
|footnotes       =
}}

The '''Inland Empire Utilities Agency ''' (IEUA) is a regional wastewater facility and wholesale supplemental water supplier in southwestern [[San Bernardino County]], in the [[Inland Empire]] region of [[Southern California]]. 

==Services==
IEUA specifically provides water services to seven cities in the [[Pomona Valley]]: [[Chino, California|Chino]], [[Chino Hills, California|Chino Hills]], [[Fontana, California|Fontana]], [[Montclair, California|Montclair]], [[Ontario, California|Ontario]], [[Rancho Cucamonga, California|Rancho Cucamonga]], and [[Upland, California|Upland]]. The Agency's service area covers 242 square miles and approximately 700,000 people.<ref>{{Cite web|url=http://www.mwdh2o.com/mwdh2o/pages/memberag/agencies/inlandempire.htm|title= Inland Empire utilities Agency|work=Metropolitan Water District of Southern California|accessdate=September 15, 2011}}</ref>

IEUA's supplemental water comes from both imported water and recycled water. 

The wastewater treatment facility consists of domestic and industrial disposal systems and energy recovery and production facilities. The agency is also a biosolids and fertilizer treatment provider and remains a leader in protecting the quality of the area's groundwater.<ref>{{Cite web|url=http://www.metroinvestmentreport.com/mir/?module=displaystory&story_id=405&edition_id=69&format=html|title= Inland Empire Utilities Agency Pioneers Sustainable Resource Management Best Practices|work=Metro Investment Report, February 2007|accessdate=September 15, 2011}}</ref>

<!-- Deleted image removed: [[File:IEUA LEED Building USGBC.jpg|thumb|center|upright=2.5|alt=IEUA's LEED Platinum building|IEUA's LEED platinum building, photo by: Magnus Stark.]] -->
==History==
IEUA was formed in 1950 as the Chino Basin Municipal Water District (CBMWD) and joined [[Metropolitan Water District of Southern California|MWD]] in the same year.<ref>{{Cite web|url=http://www.mwdh2o.com/mwdh2o/pages/memberag/agencies/inlandempire.htm|title= Inland Empire utilities Agency|work=Metropolitan Water District of Southern California|accessdate=September 15, 2011}}</ref> In 1998 CBMWD changed its name to the Inland Empire Utilities Agency (IEUA).<ref>{{Cite web|url=http://www.cakex.org/directory/organizations/inland-empire-utilities-agency|title= Inland Empire utilities Agency|work=Climate Adaptation Knowledge Exchange|accessdate=September 15, 2011}}</ref> The name change was meant to reflect changes in the District’s mission. 

In 2002 IEUA made history by becoming the first public agency to obtain a Platinum LEED rating by the [[USGBC]]<ref>{{Cite web|url=http://leedcasestudies.usgbc.org/overview.cfm?ProjectID=278|title= Certified Project List|work=United States Green Building Council|accessdate=September 15, 2011}}</ref>

== Chino Creek Wetlands and Educational Park ==
In 2008 the Chino Creek Wetlands and Educational Park opened to the public. The park is located at the IEUA headquarters adjacent to the LEED Platinum buildings. Among many other things, the park features:  <ref>{{Cite web|url=http://www.usewaterwisely.com/totm0707.cfm|title= Topic of the Month - Chino Creek Wetlands and Educational Park|work=Water Education Water Awareness Committee, July 2007|accessdate=September 15, 2011}}</ref>
* 1.7 miles of trails
* 22 acres of habitat
* 6 connecting wetland ponds used for tertiary treatment of grey water before it is sent out to the [[Chino Creek]]
* One million gallons of recycled water flowing through its wetlands each day
* Wildlife monitoring stations

== References ==
{{reflist|30em}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

==External links==
*[http://www.ieua.org/home/about.html Official '''Inland Empire Utilities Agency''' website]
*[http://www.environmental-expert.com/Files%5C5306%5Carticles%5C13484%5C438.pdf Environmental-expert.com: "How a LEED Platinum Building is Cleaning up Inland Empire Runoff"] —  ''by Whitman et al (Water Environment Foundation).''
*[http://www.coastalconference.org/h20_2007/pdf_07/2007-10-24-Wednesday/Session_4B-Stream_Restoration/Slater-IEUA's_Chino_Creek_Wetlands_Educational_Park.pdf  CoastalConference.org: "IEUA Chino Creek Wetlands Educations Park"] — ''by Cole Slater, Stephen Lyon and Judi Miller.''


[[Category:Water management authorities in California]]
[[Category:Water companies of the United States]]
[[Category:Government of San Bernardino County, California]]
[[Category:Pomona Valley]]
[[Category:Sewage treatment plants in California]]
[[Category:Special districts of California]]
[[Category:Chino, California]]
[[Category:Chino Hills, California]]
[[Category:Fontana, California]]
[[Category:Montclair, California]]
[[Category:Ontario, California]]
[[Category:Rancho Cucamonga, California]]
[[Category:Upland, California]]
[[Category:1950 establishments in California]]
[[Category:Government agencies established in 1950]]