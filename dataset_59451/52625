{{Infobox journal
| title         = Proceedings of the Entomological Society of Washington
| cover         = [[File:Proceedings of the Entomological Society of Washington cover.jpg|150px]]
| caption       = Cover of the first issue.
| former_name   = <!-- or |former_names= -->
| abbreviation  = Proc. Entomol. Soc. Wash.
| discipline    = [[Entomology]]
| peer-reviewed = 
| language      = 
| editors       = [[Matthew L. Buffington]], [[Thomas J. Henry]]
| publisher     = [[Entomological Society of Washington]]
| country       = 
| history       = 1890&ndash;present
| frequency     = Quarterly
| openaccess    = 
| license       = 
| impact        = 0.532
| impact-year   = 2014
| ISSNlabel     =
| ISSN          = 0013-8797
| eISSN         =
| CODEN         =
| JSTOR         = 
| LCCN          = 
| OCLC          = 630167895
| website       = http://entsocwash.org/default.asp?Action=Show_Proceedings
| link1         = http://www.biodiversitylibrary.org/bibliography/2510#/summary
| link1-name    = Online access (Vol. 1&ndash;108, 2008&ndash;present)
| link2         = http://www.bioone.org/loi/went<!-- up to |link5= -->
| link2-name    = Online access (Vol. 109&ndash;present, 2008&ndash;present)
| boxwidth      = 
}}

'''''Proceedings of the Entomological Society of Washington''''' is a [[peer-review]]ed [[scientific journal]] of [[entomology]] published by the [[Entomological Society of Washington]]. The journal was established in 1890 and is currently published four times per year. The journal is edited by [[Matthew L. Buffington]] and [[Thomas J. Henry]].

==Abstracting and indexing==
According to ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 0.532, ranking 66th out of 92 in the category 'Entomology'.<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Entomology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> The journal is indexed in the following databases.
{{columns-list|colwidth=20em|
*[[AGRICOLA]]
*[[Biosis]]
*[[LOCKSS]]
*[[Portico (service)|Portico]]
*[[Science Citation Index]]
*[[Scopus]]
}}

==References==
{{Reflist}}

==External links==
*{{official website|1=http://entsocwash.org/default.asp?Action=Show_Proceedings}}
*[http://www.entsocwash.org/ Entomological Society of Washington website]

[[Category:Entomology journals and magazines]]
[[Category:Publications established in 1890]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies of the United States]]