{{Other ships|HMS Severn}}
{|{{Infobox ship begin}}
{{Infobox ship image
| Ship image=
| Ship caption=
}}
{{Infobox ship career
| Hide header=
| Ship country=[[United Kingdom of Great Britain and Ireland|UK]]
| Ship flag=[[File:Naval Ensign of the United Kingdom.svg|60px|Royal Navy Ensign]]
| Ship name=HMS ''Severn''
| Ship namesake=
| Ship ordered=4 May 1812
| Ship awarded=
| Ship builder=Wigram, Wells & Green, [[Blackwall Yard]]
| Ship original cost=
| Ship yard number=
| Ship way number=
| Ship laid down=January 1813 	 	
| Ship launched=14 June 1813
| Ship sponsor=
| Ship christened=
| Ship completed=11 September 1813
| Ship acquired=
| Ship commissioned=
| Ship decommissioned=
| Ship in service=
| Ship out of service=
| Ship renamed=
| Ship reclassified=
| Ship refit=
| Ship struck=
| Ship reinstated=
| Ship homeport=
| Ship identification=
| Ship motto= 
| Ship nickname=
| Ship honours=[[Naval General Service Medal (1847)|Naval General Service Medal]] with clasp "Algiers"<ref>{{London Gazette|issue=20939|startpage=245|date=26 January 1849}}</ref>
| Ship fate=Sold for breaking up, 20 July 1825
| Ship notes=
| Ship badge=
}}
{{Infobox ship characteristics
| Hide header=
| Header caption=<ref name=Winfield/>
| Ship class={{Sclass|Endymion|frigate}} (revived)
| Ship type=
| Ship tonnage= 
| Ship displacement=
| Ship tons burthen= 1,254 {{small|{{frac|87|94}}}} [[Builder's Old Measurement|bm]] (as designed)
| Ship length= {{Convert|159|ft|2+5/8|in|m|abbr=on}} (gundeck); {{Convert|132|ft|2|in|m|abbr=on}} (keel)
| Ship beam= {{Convert|41|ft|3|in|m|abbr=on}}
| Ship height=
| Ship draught= {{Convert|9|ft|9|in|m|abbr=on}} unladen; {{Convert|12|ft|8|in|m|abbr=on}} (laden)
| Ship depth=
| Ship hold depth= {{Convert|12|ft|4|in|m|abbr=on}}
| Ship decks=
| Ship deck clearance=
| Ship propulsion=Sail
| Ship sail plan=
| Ship speed={{Convert|14.4|kn}}
| Ship range=
| Ship endurance=
| Ship complement=300 (later 340)
| Ship armament=*[[Deck (ship)#Upper deck|UD]]: 28 × 24-pounder guns
*[[Quarterdeck|QD]]: 16 × 32-pounder [[carronade]]s
*[[Forecastle|Fc]]: 2 × 9-pounder guns and 4 × 32-pounder carronades
| Ship notes=
}}
|}
'''HMS ''Severn''''' was an {{sclass|Endymion|frigate}} of the British [[Royal Navy]], launched in 1813 as one of five heavy frigates built to match the powerful [[Original six frigates of the United States Navy|American frigates]]. The shortage of oak meant that she was built of "fir" (actually [[pine]]), which meant a considerably shortened lifespan. Nonetheless, the ship saw useful service, especially at the bombardment of Algiers in 1816, before being broken up in 1825.

==Background==
''Severn'' was ordered as a {{Sclass|Leda|frigate}} of 38 guns, and was to have borne the name ''Tagus''. Relative to her prototype, she received two more guns forward. ''Tagus'' was renamed ''Severn'' on 7 January 1813, i.e., well before her launching.<ref name=Winfield/>

==War of 1812==
Initially commissioned under the command of Captain Joseph Nourse, ''Severn'' served in the North Atlantic. On 18 January 1814 she was escorting a convoy from England to Bermuda when she encountered the French 40-gun frigates ''Sultane'' and [[French frigate Étoile (1813)|''Étoile'']].<ref name=Winfield/> ''Severn'' drew them away from the convoy, saving it. After a long chase, the French frigates gave up and sailed away.{{#tag:ref|{{HMS|Hannibal|1810|2}} captured ''Sultane'' on 27 March 1814.<ref>{{London Gazette|issue=16875|startpage=678|date=29 March 1814}}</ref> {{HMS|Hebrus|1813|2}} captured ''Étoile'' that same day in a notable [[single-ship action]].<ref>{{London Gazette|issue=16876|startpage=698|endpage=699|date=2 April 1814}}</ref>|group=Note}}

Later the same year, on 1 May, she captured the American privateer schooner ''Yankee Lass'', armed with nine guns and carrying a crew of 80 men. She was 20 days out of Rhode Island and had not made any captures.<ref>{{London Gazette|issue=16924|startpage=1610|date=9 August 1814}}</ref><ref name="NMM-WH-375638">{{cite web|url=http://www.nmm.ac.uk/upload/pdf/Warship_Histories_Vessels_iii.pdf|title=NMM, vessel ID 375638|work=Warship Histories, vol iii|publisher=[[National Maritime Museum]]|accessdate=8 February 2012}}</ref> At the time, ''Severn'' was in company with {{HMS|Surprise|1812|2}}.<ref>{{London Gazette|issue=17041|startpage=1463|date=18 July 1815}}</ref>{{#tag:ref|In September 1815 prize money was paid for ''Yankee Lass''. A first-class share was worth £40 6s 3d; a sixth-class share was worth 4s 6¾d.<ref>{{London Gazette|issue=17064|startpage=1956|date=23 September 1815}}</ref>|group=Note}}

''Severn'' was among the several British warships that shared in the proceeds of the capture on 10 July of the American schooners ''William'', ''Eliza'', ''Union'', and ''Emmeline'', and the capture on 2 July of the schooner ''Little Tom''.<ref>{{London Gazette|issue=17206|startpage=11|date=4 January 1817}}</ref>{{#tag:ref|For the first four schooners a first-class share was worth £20 6s 0½d; a sixth-class share was worth 2s 7d. For the cargo of the ''Little Tom'', a first-class share was worth £6 17s 3½d; a sixth-class share was worth 11½d.<ref>{{London Gazette|issue=17209|startpage=89|date=14 January 1817}}</ref>|group=Note}}

In the late summer and autumn of 1814, ''Severn'' was an important participant in the [[War of 1812]], as she was stationed in [[Chesapeake Bay]] to blockade the [[Patuxent River]]. It was from this point that the British launched their invasion of Maryland, which led to the [[Battle of Bladensburg]] and then the subsequent [[Burning of Washington|burning of Washington D.C.]] On 2 July ''Severn'' and [[French frigate Loire (1796)|''Loire'']] captured two schooners, two gun-boats, and a sloop.<ref>{{London Gazette|issue=16941|startpage=1964|date=1 October 1814}}</ref> They also destroyed a large store of tobacco.<ref>{{London Gazette|issue=16941|startpage=1966|date=1 October 1814}}</ref>

On 20 August ''Severn'', the [[sailing frigate|frigate]] {{HMS|Hebrus|1813|2}}, and the [[gun-brig]] {{HMS|Manly|1812|2}} sailed up the Patuxent to follow the boats as far as possible.<ref>{{London Gazette|issue=16939|startpage=1939|endpage=1941|date=27 September 1814}}</ref> Admiral [[Alexander Cochrane]] and his force of marines and seamen entered [[Washington, D.C.|Washington]] on the night of 24 August. The British then burnt the [[White House]], the Treasury and the War Office. They left at 9 o'clock on the evening of the next day and returned to [[Nottingham, Maryland]] on the Patuxent where Cochrane boarded ''Manly''. The campaign cost the Navy one man killed and six wounded, including one man of the [[Corps of Colonial Marines]] killed and three wounded.<ref>{{London Gazette|issue=16939|startpage=1942|endpage=1943|date=27 September 1814}}</ref>{{#tag:ref|Prize-money arising from the booty captured by the expedition in the River Patuxent, at Fort Washington, and Alexandria, between 22 and 29 August 1814, was paid in November 1817. Nourse, as captain, received a first-class share, which was worth £183 9[[shilling|s]] 1¾[[pence|d]]; a sixth-class share, which was what an ordinary seaman would receive, was worth £1 9s 3½d.<ref name=LG17482>{{London Gazette|issue=17305|startpage=2316|date=15 November 1817}}</ref> A second and final payment came in May 1819. A first-class share was worth £42 13s 10¾d; a sixth-class share was worth 9s 1¾d.<ref>{{London Gazette|issue=17482|startpage=955|date=1 June 1819}}</ref>|group=Note}}{{#tag:ref|In 1817 there was a further payment of prize money for the expedition in the River Patuxent, at Fort Washington, and Alexandria, between 22 and 29 August 1814. A first-class share was worth £183 9s 1¾d; a sixth-class share was worth £1 19s 3½d.<ref name=LG17482/>|group=Note}}

The draught of this class of frigate was too deep to permit ''Severn'' and her sister ships from sailing into the harbour at Baltimore. Her sailors had to kedge rafts holding small cannon and rocket launchers seven miles up the river to [[Fort McHenry]].<ref>Whitehorse (1997), p.262.</ref> During the attack on Baltimore [[Admiral]] [[Sir]] [[George Cockburn]] raised his flag on ''Severn''. Although the navy contributed seamen and marines to the land attack, and took casualties, ''Severn'' did not suffer any losses.<ref>{{London Gazette|16947|startpage=2073|endpage=2080|date=17 October 1814}}</ref>

Between 1 October 1814 and 25 March 1815, ''Severn'' captured thirteen mostly small American merchant vessels, but with several armed vessels among them. These were:
* schooner ''Speedwell'', of five men and 34 tons;
* brig ''May Flower'', of 8 men 60 tons;
* ship ''Anna Marie'', of six men and 120 tons;
* ship ''Betsy'';
* ship ''Virginia'';
* schooner ''Nonsuch'', of five men and 65 tons;
* ship ''Buonaparte'';
* ship ''Anna'';
* schooner ''Virginia'';
* schooner ''Brant'';
* ship ''Necessity'', of four guns, 12 men, and 309 tons;
* schooner ''Amelia'', of 40 tons;
* schooner ''Resolution''; and
* privateer brig ''Ind'', of nine guns, 130 men, and 250 tons.<ref name="NMM-WH-375638"/><ref>{{London Gazette|issue=17012|startpage=923|date=16 May 1815}}</ref>

On 20 December ''Severn'' also captured the American [[letter of marque]] schooner ''Banyer''. She was armed with four guns and carried a crew of 31 men.<ref>{{London Gazette|issue=17010|startpage=871|date=9 May 1815}}</ref>{{#tag:ref|Head money for the ''Banyer'' was paid in 1817. A first-class share was worth £24 5s 6½d; a sixth-class share was worth 2s 6¾d.<ref>{{London Gazette|issue=17313|startpage=2534|date=13 December 1817}}</ref>|group=Note}}

On 10 January 1815, Cockburn landed on [[Cumberland Island]] in an effort to tie up American forces and keep them from joining other American forces to help defend [[New Orleans]], [[Louisiana]], and the [[Gulf Coast of the United States|Gulf Coast]]. The naval squadron consisted of [[HMS Dragon (1798)|''Dragon'']] (74-guns), ''Regulus'' (44 guns; [[en flute]]), ''Brune'' (56 guns; en flute), ''Severn'', ''Hebrus'' (36 guns), ''Rota'' (38 guns), ''Primrose'' (18 guns), [[HMS Terror (1813)|''Terror'']] and {{HMS|Devastation|1804|2}} (both [[bomb vessels]] of 8 guns), and the schooners ''Canso'' (10 guns) and [[HMS Whiting (1812)|''Whiting'']] (12 guns).

{{main article|Battle of Fort Peter}}

Five days later a British force first bombarded and then landed near Fort Peter on Point Peter by the town of St. Marys. The British attacked and took the fort without suffering any casualties. They then headed for St. Marys along the St. Mary's River and captured it after skirmishing with a small American  force. The British captured two American gunboats and 12 merchantmen, including the [[East Indiaman]] ''Countess of Harcourt'', which an American privateer had captured on her way from India to London. The British ended their occupation of St. Marys after about a week and withdrew to Cumberland Island.<ref>{{London Gazette|issue=17010|startpage=870|endpage=871|date=9 May 1815}}</ref>

On 26 February 1815, ''Severn'' recaptured the cargo ship ''Adventure'', which she sent in to Bermuda. This earned ''Severn'' salvage money for the vessel and her cargo.<ref>{{London Gazette|issue=17041|startpage=1462|date=18 July 1815}}</ref>{{#tag:ref|A first-class share was worth £85 8s 10d; a sixth-class share was worth 13s 9d.<ref>{{London Gazette|issue=17045|startpage=1547|date=29 July 1815}}</ref>|group=Note}}

Lastly, on 3 March, ''Severn'' destroyed the American privateer ''Ino''.{{#tag:ref|A first-class share of the head money was worth £130 10s 6½d; a sixth-class share was worth 15s 0¼d.<ref>{{London Gazette|issue=18518|startpage=1956|date=28 October 1828}}</ref>|group=Note}} American accounts report that ''Ino'' grounded outside of Charleston on 7 March. As hr crew was attempting to free ''Ino'', ''Severn'' came on the scene and launched her boats to board ''Ino''. ''Ino''{{'}}s crew, unaware that the war had ended on 15 February 1815, fired grapeshot and small arms at the British boats, causing them to shear off. ''Ino''{{'}} crew then set fire to her and took to their boats and some improvised rafts. A schooner that came out from Charleston rescued almost all. The ''Ino''{{'}}s crew believed that Captain Nourse of ''Severn'' had known for some days that the war had ended.<ref>Coggeshall (1856), pp.349-350.</ref> The delay of payment of the head money may have been due to the need to adjudicate the case.

==Post-war==
''Severn'' was fitted at Chatham for foreign service between February and July 1816.<ref name=Winfield/> In February the [[Frederick Whitworth Aylmer, 6th Baron Aylmer|Hon. Frederick W. Aylmer]] assumed command of ''Severn''. He then sailed her to Gibraltar and then took part in the [[Bombardment of Algiers (1816)|bombardment of Algiers]] on 27 August.<ref name="NMM-WH-375638"/> British casualties were heavy, though those of the Algerines were much heavier. ''Severn'' herself had three men killed and 34 wounded. As a result of the attack, the Dey agreed to abolish the enslavement of Christians in perpetuity, and to free all slaves whatsoever then in Algiers. The British also destroyed four large frigates, five large corvettes, numerous gunboats, and numerous merchant vessels.<ref>{{London Gazette|issue=17133|startpage=1789|endpage=1794|date=15 September 1816}}</ref> King [[Ferdinand I of the Two Sicilies|Ferdinand of the Two Sicilies]] bestowed on Aylmer the cross of a Commander of the Royal Sicilian Order of St. Ferdinand and of Merit.<ref>{{London Gazette|issue=17229|startpage=609|date=11 March 1817}}</ref> Other captains and officers received similar awards. In May 1818 the participants in the battle were granted an award of £100,000.{{#tag:ref|A first-class share was worth £1068 11s 6½d; a sixth-class share was worth £4 10s 2½d.<ref>{{London Gazette|issue=17355|startpage=791|date=2 May 18188}}</ref>|group=Note}} In 1847 the Admiralty issued the Naval General Service Medal with clasp "Algiers" to the 1328 surviving claimants from the battle.

''Severn'' initially remained in the Mediterranean, first under Captain James Gordon and then under Captain Robert Spencer.<ref  name=Winfield/> From May 1817 ''Severn'' saw service off the Kent and Sussex coasts in the Royal Naval Coast Blockade for the Prevention of Smuggling. under the command of Captain William ("Flogging Joey") McCulloch, scourge of the smugglers.<ref>Douch (1985).</ref>

On 6 August 1817 she seized a boat with foreign spirits and five empty boats.{{refn|A first-class share was worth £28 6s 10d; a sixth-class share was worth 2s 1½d.<ref name=LG17447>{{London Gazette|date=6 February 1819|issue=17447|startpage=247|endpage=248}}</ref>|group=Note}}
Three weeks later she seized ''Mary'', with four smugglers and a quantity of tea, and also seized two empty boats.{{refn|A first-class share was worth £25 13s 4d; a sixth-class share was worth 1s 10½d.<ref name=LG17447/>|group=Note}}
On 15 December ''Severn'' seized ''Po'', which was carrying a cargo of foreign spirits.{{refn|A first-class share was worth £24 9s 11d; a sixth-class share was worth 1s 8½d.<ref name=LG17447/>|group=Note}} 
On 29 March 1818 ''Severn'' seized ''Linot'', which was carrying foreign spirits, and two smugglers.{{refn|A first-class share was worth £73 5s 8d; a sixth-class share was worth 5s 2½d.<ref name=LG17447/>|group=Note}}

==Fate==
''Severn'' was in [[Reserve fleet|ordinary]] at Portsmouth in 1822, but by 1824 was at Deptford.<ref name="NMM-WH-375638"/> She was put up for sale in June 1825 at Deptford,<ref>{{London Gazette|issue=18150|date=28 June 1825|startpage=1135}}</ref> and sold to John Small Sedger, Rotherhithe, for £3,610 on 20 July.<ref name=Winfield>Winfield (2008), p. 134.</ref>

==Notes, citations, and references==

===Notes===
{{reflist|group=Note|2}}

===Citations===
{{Reflist|30em}}

===References===
*Coggeshall, George (1856) ''History of the American Privateers, and Letters-Of-Marque''. (New York).
*{{cite book |title=Flogging Joey's Warriors  |last=Douch |first=John |year=1985 |publisher= |location= |isbn=978-0-906124-08-6}}
* {{cite book|last=Whitehorne|first=Joseph|title=The Battle for Baltimore: 1814 |year=1997 |publisher=Nautical & Aviation Publishing Company |location=Mount Pleasant S.C.|isbn=1-877853-23-2}} 
*{{cite book |first=Rif|last=Winfield|title=British Warships in the Age of Sail 1793&ndash;1817: Design, Construction, Careers and Fates|publisher=Seaforth|year=2008|isbn=1-86176-246-1}}

{{WarshipHist}}

{{DEFAULTSORT:Severn (1813)}}
[[Category:1813 ships]]
[[Category:Blackwall Yard-built ships]]
[[Category:Age of Sail frigates of the United Kingdom]]
[[Category:War of 1812 ships of the United Kingdom]]