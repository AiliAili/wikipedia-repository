<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=D-Plane 1
 | image=F-PDHV Verhees Delta (9705932877).jpg
 | caption=Verhees D-Plane 1 prototype
}}{{Infobox Aircraft Type
 | type=[[Amateur-built aircraft]]
 | national origin=[[Belgium]]
 | manufacturer=[[Verhees Engineering]]
 | designer=
 | first flight=October 2004<ref name="Jane's">[http://articles.janes.com/articles/Janes-All-the-Worlds-Aircraft/D-Plane--Verhees-Engineering-Belgium.html Verhees D-Plane]</ref>
 | introduced=
 | retired=
 | status=Plans available (2012)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=One prototype
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]400 (plans only, 2011)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Verhees D-Plane 1''' is a [[Belgium|Belgian]] [[Homebuilt aircraft|homebuilt]] [[flying wing]], designed by [[Verhees Engineering]] and supplied as plans for amateur construction.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 125. WDLA UK, Lancaster UK, 2011. {{ISSN|1368-485X}}</ref>

==Design and development==
The D-Plane 1 features a cantilever [[mid-wing]], a single-seat enclosed cockpit, semi-retractable [[tricycle landing gear]] and a single engine in [[tractor configuration]].<ref name="WDLA11" />

The aircraft is made from sheet [[aluminum]] tubing. Its very low [[aspect ratio]] {{convert|4.5|m|ft|1|abbr=on}} span delta wing has an area of {{convert|10|m2|sqft|abbr=on}}. The nose wheel retracts, while the single main wheel and wing tip wheels are fixed. The recommended engine is the 1.6 litre displacement {{convert|50|hp|kW|0|abbr=on}} [[Subaru EA71]] [[four-stroke]] automotive conversion powerplant.<ref name="WDLA11" />

By 2011 only the prototype had flown, but development work had begun on the design of the two-seat D-Plane 2, which will be powered by a {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]] and cruise at about {{convert|250|km/h|mph|0|abbr=on}}.<ref name="WDLA11" />
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (D-Plane 1) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=4.5
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=10
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=210
|empty weight lb=
|empty weight note=
|gross weight kg=340
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|60|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Subaru EA71]]
|eng1 type=four cylinder, liquid-cooled, [[four stroke]] automotive conversion
|eng1 kw=<!-- prop engines -->
|eng1 hp=50<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=270
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=220
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=85
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=34.0
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{commons category|Verhees D-Plane}}
*{{Official website|http://www.d-plane.eu/}}

[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Flying wings]]