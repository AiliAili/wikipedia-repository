{{Infobox military person
|name= Alfred Austell Cunningham
|birth_date= {{birth date|1881|3|8}}
|death_date= {{Death date and age|1939|5|27|1881|3|8}}
|birth_place= [[Atlanta, Georgia]]
|death_place= [[Sarasota, Florida]]
|placeofburial= [[Arlington National Cemetery]]
|placeofburial_label= Place of burial
|image= USMC-19400.jpg
|caption= 1st Marine Corps aviator<br />1st Director of Marine Corps Aviation
|nickname=
|allegiance= {{flagicon|United States|1896}} [[United States|United States of America]]
|branch= {{flag|United States Marine Corps}}
|serviceyears= 1898, 1909–1935
|rank= [[File:US-O5 insignia.svg|25px]] [[Lieutenant colonel (United States)|Lieutenant Colonel]]
|commands= Marine Corps Aviation
|unit=
|battles= [[Spanish–American War]]<br />*[[Cuban Campaign]]<br />[[World War I]]<br />*[[Western Front (World War I)|Western Front]]<br />[[The Banana Wars]]<br />*[[1916 United States occupation of the Dominican Republic|Occupation of the Dom. Rep.]]<br />*[[Occupation of Nicaragua]]
|awards= [[Navy Cross (United States)|Navy Cross]]
|laterwork=
}}
Lieutenant Colonel '''Alfred Austell Cunningham''' (March 8, 1881 – May 27, 1939) was a [[United States Marine Corps]] officer who became the first Marine Corps [[Naval aviation|aviator]]<ref>{{cite web|url=http://www.acepilots.com/usmc/hist1.html|accessdate=2006-12-22|title=The Early Years|work=History of Marine Corps Aviation|publisher=AcePilots.com|archiveurl=https://web.archive.org/web/20061106141233/http://www.acepilots.com/usmc/hist1.html|archivedate=6 November 2006 <!--DASHBot-->|deadurl=no}}</ref> and the first Director of Marine Corps Aviation.<ref>{{cite web|url=http://www.acepilots.com/usmc/main.html |accessdate=2006-12-22|title=Introduction |work=History of Marine Corps Aviation|publisher=AcePilots.com|archiveurl=https://web.archive.org/web/20070205053912/http://www.acepilots.com/usmc/main.html|archivedate=5 February 2007 <!--DASHBot-->|deadurl=no}}</ref> His military career included service in the [[Spanish–American War]], [[World War I]], and U.S. operations in the [[Caribbean]] during the 1920s.

==Early life and career==
Cunningham was born in [[Atlanta, Georgia]]. His interest in aviation began in 1903 when he watched a balloon ascend one afternoon. The next time the balloon went up he was in it and from then on he was considered himself a "confirmed aeronautical enthusiast".<ref name="De Chant">{{cite book
  |last = De Chant
  |first = John A.
  |title = Devilbirds
  |publisher = Harper and Brothers Publishers
  |year = 1947
  |location = New York}}</ref> He enlisted in the [[3rd Georgia Volunteer Infantry]] regiment during the [[Spanish–American War]] and served a tour of occupation duty in [[Cuba]]. He spent the next decade selling real estate in Atlanta. During this time evinced an interest in [[aeronautics]], making a balloon ascent in 1903.

At the age of twenty-seven, he returned to the military life, mostly because he thought that he would be given the opportunity to fly.  He was commissioned as a [[second lieutenant#United States|2nd lieutenant]] in the [[United States Marine Corps]] on 25 January 1909.

==Supporter of Marine Corps aviation==
As a Lieutenant, Alfred Cunningham retained an interest in aeronautics, he found at Philadelphia a likewise avid group of civilians and off-duty military men who harbored an interest in the same thing. He rented an airplane and gained permission from the Commandant of the Navy Yard to use an open field at the [[Philadelphia Naval Shipyard|Philadelphia Navy Yard]] for test flights. He also joined the Aero Club of Philadelphia, and commenced "selling" Marine Corps aviation to members of the Aero Club, who, through their Washington connections, began to pressure a number of officials, including [[Major general (United States)|Major General]] [[Commandant of the Marine Corps|Commandant]] [[William P. Biddle]], himself a member of a prominent Philadelphia family.

Cunningham was an avid supporter in the new conceptual Advanced Base Force and though he saw a role for aircraft, requesting assignment to the Navy's flying school at [[Annapolis, Maryland|Annapolis]].<ref name="Millett, Semper Fidelis">{{cite book
|first=Allan R. |last=Millett
|title=Semper Fidelis: The History of the United States Marine Corps
|location=New York, NY |publisher=The New Press |year=1991}}</ref> Cunningham served in the Marine guards of [[USS New Jersey (BB-16)|''New Jersey'' (BB-16)]] and [[USS North Dakota (BB-29)|''North Dakota'' (BB-29)]], and the receiving ship {{USS|Lancaster|1858|6}}, over the next two years.

In 1911, while he was stationed at the Marine Barracks, [[Philadelphia Naval Shipyard|Philadelphia Navy Yard]], he developed the inspiration to fly. Leasing a plane from a civilian aviator only $25 a month, he experimented in the airplane, nicknamed the "Noisy Nan". He was promoted to the rank and grade of 1st Lieutenant in September 1911. Although the plane never left the ground, his profound faith and love of flying was rewarded. On 16 May 1912, Cunningham received orders and stood detached from duty at the Navy Yard in Philadelphia, and was ordered to the aviation camp the Navy had set up at [[United States Naval Academy]] in [[Annapolis]], to learn to fly. He reported six days later, on 22 May 1912, which is recognized as the birthday of Marine Corps aviation. Actual flight training was given at the Burgess Plant at Marblehead, Massachusetts, because only the builders of planes could fly in those days and after two hours and forty minutes of instruction, Cunningham soloed on 20 August 1912. He flew the [[Curtiss Model N|Curtiss seaplane]] and became Naval Aviator No. 5, and Smith became Naval Aviator No. 6.<ref>{{cite book |first=Thomas T. |last=Craven
|title=History of Aviation in the United States Navy
|year=1920}} File ZGU, Subject File, 1911—1927, RG 45.</ref>

[[File:AACunningham 1stMarineAviator.jpg|250px|thumb|left|1stLt Alfred A. Cunningham, first Marine Corps aviator]]
Between October 1912 and July 1913, Cunningham made some 400 flights in the [[Curtiss Robin|Curtiss B-1]], conducting training and testing tactics and aircraft capabilities. In August 1913, Cunningham sought detachment from aviation duty, on the grounds that his fiancée would not marry him unless he gave up flying. Although assigned duty as assistant quartermaster at the Marine Barracks at the [[Washington Navy Yard]], the first Marine aviator continued to advocate Marine Corps aviation and contribute significantly to its growth.

By November 1913, the [[Navy Department]] had assigned Cunningham (and Smith) to return to the [[Advanced Base Force|Advanced Base School]] with the understanding that they would create an aviation section for the force.<ref name="Millett, Semper Fidelis"/> Cunningham performed important reconnaissance roles for the force, which was fully functionable by 1914.<ref>{{cite book
|first=Lieutenant Colonel E. C. |last=Johnson |author2=Graham A. Cosmas
|title=A Short History of Marine Aviation
|location=Washington, D.C
|publisher=HQMC: History and Museums Division
|year=1976 |pages=1–10}}</ref> Later, he served on a board, headed by Captain [[Washington I. Chambers]], USN, tasked with drawing up a comprehensive plan for the organization of a naval aeronautical service. It was upon the recommendation of that board that the Naval Aeronautical Station at [[Pensacola, Florida]], was established in 1914.

The following February, Cunningham was assigned duty at the [[Washington Navy Yard]], assisting Naval Constructor [[Holden C. Richardson]] in working on the D-2 flying boat. Ordered to Pensacola for instruction in April 1915 (his wife apparently having relented in allowing her husband to fly), Cunningham was designated Naval Aviator No. 5 on 17 September 1915.

==World War I service==
After heading the motor erecting shop at Pensacola, he underwent instruction at the Army Signal Corps Aviation School at San Diego, whence he was assigned to the Commission on Navy Yards and Naval Stations. Cunningham received orders on 26 February 1917, to organize the Aviation Company for the Advanced Base Force, at the Philadelphia Navy Yard. Designated as the commander of this unit, Cunningham soon emerged as de facto director of Marine Corps aviation. He sought, and got, enthusiastic volunteers to become pilots, and soon embarked on a determined campaign to define a mission for land-based marine air. In addition, he served on a joint Army-Navy board that selected sites for naval air stations in seven naval districts and on the east and gulf coasts.

Detailed to Europe to obtain information on British and French aviation practices, he participated in a variety of missions over German lines. Returning to the United States in January 1918, he presented a plan to use Marine aircraft to operate against submarines off the Belgian coast and against submarine bases at Zeebrugge, Ostend, and Bruges.

The Northern Bombing Group emerged from these plans—four landplane squadrons equipped and trained in five months' time. On 12 July 1918, 72 planes, 176 officers and 1,030 enlisted men sailed for France on board the transport ''DeKalb'', arriving at Brest on 30 July 1918. The Marines were sent to the fields at Oye, Le Fresne, and St. Pol, France; and at Hoondschoote, Ghietelles, Varsennaire and Knesselaere, Belgium. Despite shortages of planes, spare parts, and tools, the Marines participated in 43 raids with British and French units, as well as 14 independent raids, and shot down eight enemy aircraft. Planes of the group also dropped 52,000 pounds of bombs, and supplied 2,650 pounds of food in five food-dropping missions to encircled French troops. For his service in organizing and training the first Marine aviation force, Cunningham was awarded the [[Navy Cross (United States)|Navy Cross]].

==Post-war activities==
After [[World War I]], Cunningham returned to the United States to become officer-in-charge of Marine Corps aviation, a billet in which he remained until 26 December 1920, when he was detailed to command the First Air Squadron in [[Santo Domingo]], [[Dominican Republic]]. Ordered thence to general duty at Marine Corps Schools, Quantico, Major Cunningham then served as assistant adjutant and inspector, and then division marine officer and aide on the staff of Commander, Battleship Division 3. On temporary detached duty in Nicaragua from June 1928, he served with the 2nd Brigade of Marines as executive officer of the Western Area at Leon, Nicaragua.

==Retirement and last years==
Subsequently, becoming executive officer and registrar of the [[Marine Corps Institute]] from 1929 to 1931, Cunningham finished up his career as assistant [[quartermaster]] at the Marine Barracks, Philadelphia. His health failing, Cunningham retired on 1 August 1935. Promoted to [[Lieutenant colonel (United States)|lieutenant colonel]] while on the retired list, he died at [[Sarasota, Florida]], on 27 May 1939.  He is buried at [[Arlington National Cemetery]].<ref>{{Find a Grave|13720630|Alfred Austell Cunningham}}</ref>

==Honors==
The destroyer [[USS Alfred A. Cunningham (DD-752)|USS ''Alfred A. Cunningham'' (DD-752)]] is named in his honor.

In 1965, Cunningham was enshrined in the [[National Aviation Hall of Fame]].<ref>{{cite web
|url=http://nationalaviation.blade6.donet.com/components/content_manager_v02/view_nahf/htdocs/menu_ps.asp?NodeID=-1805744604&group_ID=1134656385&Parent_ID=-1
|title=Alfred Cunningham
|accessdate=2006-12-22
|publisher=National Aviation Hall of Fame}}</ref>

The Alfred Cunningham Drawbridge, across the [[Neuse River]] at [[New Bern, North Carolina|New Bern NC]], is named in his honor.<ref>{{cite web |title=Openings scheduled at Alfred Cunningham Bridge
|accessdate=2017-02-23
|url=http://www.newbernsj.com/20130523/openings-scheduled-at-alfred-cunningham-bridge/305239832
|newspaper=Sun Journal
|date=May 23, 2013 
|first=Eddie |last=Fitzgerald
|location=[[New Bern, NC]]
}}</ref>

==See also==
{{Portal|Biography|World War I|United States Marine Corps}}
* [[Early Birds of Aviation]]
* [[United States Marine Corps Aviation]]
* [[List of Historically Important U.S. Marines]]
{{clear}}

==Notes==
{{Reflist}}

==References==
:{{Marine Corps}}
:{{DANFS}}
* [http://hqinet001.hqmc.usmc.mil/HD/Historical/Whos_Who/Cunningham_AA.htm Lieutenant Colonel Alfred Austell Cunningham, USMC], ''Who's Who in Marine Corps History'', History Division, United States Marine Corps.
* {{cite web|url=http://www.acepilots.com/usmc/main.html
|title=''History of Marine Corps Aviation''
|accessdate=2006-12-22|archiveurl=https://web.archive.org/web/20070205053912/http://www.acepilots.com/usmc/main.html|archivedate=5 February 2007 <!--DASHBot-->|deadurl=no}}
* {{cite web|url=http://www.history.navy.mil/danfs/a6/alfred_a_cunningham.htm 
|title=USS ''Alfred A. Cunningham''
|work=Dictionary of American Naval Fighting Ships
|publisher=[[Naval History & Heritage Command]], Department of the Navy}}
* {{cite web|url=http://www.arlingtoncemetery.net/aacunningham.htm |accessdate=2006-12-22
|title=Alfred Austell Cunningham, Lieutenant Colonel, United States Marine Corps
|publisher=Arlington National Cemetery}}

==Further reading==
* http://www.flymcaa.org/getattachment/MCAA-Publications/YellowSheetWinter2012Cunningham.pdf.aspx
* {{cite book
 |title=Marine Flyer in France: The Diary of Captain Alfred A. Cunningham
 |publisher=History and Museums Division, United States Marine Corps
 |location=Washington, D.C.
 |year=1974
|url=http://www.usmc.mil/directiv.nsf/296c3de7ecae538985256d1100686a36/50fae00acd880cbe85256dcd00503bb7?OpenDocument
 |accessdate=2007-01-12| archiveurl=https://web.archive.org/web/20070111123155/http://www.usmc.mil/directiv.nsf/296c3de7ecae538985256d1100686a36/50fae00acd880cbe85256dcd00503bb7?OpenDocument| archivedate=11 January 2007 <!--DASHBot-->| deadurl=no}}
==External links==
* {{Internet Archive author |sname=Alfred A. Cunningham |sopt=t}}

{{US Marine Corps navbox}}

{{Authority control}}
{{DEFAULTSORT:Cunningham, Alfred Austell}}
[[Category:1881 births]]
[[Category:1939 deaths]]
[[Category:American military personnel of World War I]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:United States Naval Aviators]]
[[Category:Recipients of the Navy Cross (United States)]]
[[Category:American military personnel of the Spanish–American War]]
[[Category:United States Marine Corps officers]]
[[Category:People from Atlanta]]
[[Category:Members of the Early Birds of Aviation]]