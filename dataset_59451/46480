'''Jason Bell''' (born 19 April 1969 in [[Camden Town]], [[London]]) is an [[England|English]] portrait and fashion [[photographer]]. He studied politics, philosophy and economics at [[Oxford University]] before returning to London to work for the [[Sunday Times]]. He lives in both New York and London. He describes photography as 'always part of who I was' and he sees himself as a portrait photographer rather than a fashion photographer, saying: 'for me the most important thing is the person'.<ref>Interview in: Cooke, D,'Jason Bell HonFRPS' in Reynolds, R (editor), ''[http://www.amazon.co.uk/Portfolio-Three-Royal-Photographic-Society/dp/0904495051/ref=sr_1_1?ie=UTF8&qid=1382774003&sr=8-1&keywords=Portfolio+Three+Royal+Photographic+Society Portfolio Three]'', (Bath: The Royal Photographic Society, 2013)</ref>

Bell works for ''[[Vanity Fair (magazine)|Vanity Fair]]'', British and American ''[[Vogue (magazine)|Vogue]]'', ''[[Time (magazine)|Time]]'' and ''[[Newsweek]]'' amongst others. He has photographed many celebrities such as [[David Beckham]], [[Scarlett Johansson]], [[Johnny Depp]], [[Nicole Kidman]], [[Mickey Rourke]], [[John Malkovich]], [[Daniel Craig]], [[Halle Berry]], [[Susan Sarandon]], [[Ian McKellen]], [[Kate Winslet]] and [[Benedict Cumberbatch]]. He has also worked on many film posters such as ''[[About a Boy (film)|About a Boy]]'', ''[[Bridget Jones's Diary (film)|Bridget Jones's Diary]]'', ''[[Billy Elliot]]'', ''[[Love Actually]]'', ''[[The Golden Compass (film)|The Golden Compass]]'', and ''[[Inkheart (film)|Inkheart]]''. Many of his photographs are in the [[National Portrait Gallery, London|National Portrait Gallery]] permanent collection.

In February 2003, the British edition of ''[[Gentlemen's Quarterly]]'' magazine published photographs by Jason Bell on its cover and inside the magazine of Kate Winslet which the magazine retouched, dramatically slimming her waist and legs; Winslet issued a statement saying that the alterations were made without her consent; Bell also denied having made the alterations. ''GQ'' issued an apology in the subsequent issue.

On 23 October 2013, Bell took the official christening photographs of Prince George.

==Awards==
{{BLP unsourced section|date=November 2010}}
*2011 - Honorary Fellowship of the [[Royal Photographic Society]]<ref>http://www.rps.org/annual-awards/Honorary-Fellowships Accessed 20 October 2013</ref>
*2010 - [[National Magazine Awards]] Finalist in Photo Portfolio Category<ref name =AboutBell>{{cite web|url=http://www.jasonbellphoto.com/#/about/|title=About Jason Bell|publisher=Jason Bell|accessdate=7 March 2013}}</ref>
*2008 - New York Photo Awards Best Single Advertising Image for Royal Opera House campaign<ref name =AboutBell/>
*2006 - [[Royal Photographic Society]] Terence Donovan Award for An Outstanding Contribution to Photography<ref name =AboutBell/>
*2004 - US [[National Press Photographers Association]] Award for Best of Journalism in the Magazine Portrait section<ref name =AboutBell/>
*2002 - [[BBC Music]] Photo Competition overall winner
*1998 - Best British Black & White Photographer at the British Picture Editors' Awards<ref name =AboutBell/>

==Books Published==
*2010 ''An Englishman in New York'' (ISBN 978-1-904587-97-2)
*2004 ''GiveGet'' (ISBN 0-954684-31-1)
*2002 ''Hats Off'' (ISBN 1-899235-49-3)
*2000 ''Gold Rush'' (ISBN 1-899235-33-7)

==References==
{{Reflist}}

==External links==
*[http://www.jasonbellphoto.com Official Website]
*[http://www.npg.org.uk/collections/search/person.php?sText=jason+bell&submitSearchTerm%5Fx=0&submitSearchTerm%5Fy=0&search=ss&OConly=true&firstRun=true&LinkID=mp58603 Jason Bell at the National Portrait Gallery]
*[http://www.camerapress.com/photographer/photographer%20pages/jasonBimages.htm Camera Press]
*[http://www.sohomanagement.co.uk/#/photographers/ Soho Management]

== Sources ==
*[http://www.bbc.co.uk/news/entertainment-arts-24954370 Royal photographer Jason Bell's top 10 tips]
*[http://www.bbc.co.uk/news/uk-24662966 Prince George Christening: Official Pictures released], BBC 24 October 2013
*[https://www.theguardian.com/artanddesign/2010/aug/15/jason-bell-portraits-english-new-york English people in New York: portraits by Jason Bell], [[The Observer]], 15 August 2010
*[http://www.bbc.co.uk/news/world-11029799 BBC Audio Slideshow: An Englishman in New York]
*[http://www.npg.org.uk/collections/search/person.php?sText=jason+bell&submitSearchTerm%5Fx=0&submitSearchTerm%5Fy=0&search=ss&OConly=true&firstRun=true&LinkID=mp58603 National Portrait Gallery, London]
*[https://web.archive.org/web/20081214033546/http://www.time.com/time/europe/photoessays/olympics_bell/ ''Time'' magazine article about Jason Bell]
*[http://www.bjp-online.com/public/showPage.html?page=797816 ''British Journal of Photography'']{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}
*[http://www.bjp-online.com/public/showPage.html?SEARCH_DATED=Y&page=bjp_search_results&SEARCH_PRODUCTS%5B%5D=British+Journal+of+Photography&SEARCH_QUERY=jason+bell&imageField.x=0&imageField.y=0 List of articles about Jason Bell in ''British Journal of Photography'']
*''[[Photography Monthly]]'' Sept 2007 interview p.&nbsp;76
*''[[Royal Photographic Society Journal]]'' May 2007 cover article
*[[Digital Photographer magazine|''Digital Photographer'' magazine]] - issue 19 p.&nbsp;26
*''Photography Monthly'' Oct 2004 interview p.&nbsp;115
*''Royal Photographic Society Journal'' March 2002 cover article
*''[[Amateur Photographer]]'' magazine 16 Sept 2000 p.&nbsp;43

{{Authority control}}

{{DEFAULTSORT:Bell, Jason}}
[[Category:Living people]]
[[Category:1969 births]]
[[Category:English photographers]]