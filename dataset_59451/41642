<!-- Image with inadequate rationale removed: [[Image:Fallofmogadishu.jpg|thumb|right|A technical in Somalia after the [[Fall of Mogadishu]], December, 2006]] -->
{{further|Diplomatic and humanitarian efforts in the Somali Civil War}}

After two decades of violence and [[Somali Civil War|civil war]] (which began in 1986) and after the [[Transitional Federal Government]] (TFG) of [[Somalia]] captured [[Fall of Mogadishu|Mogadishu]] and [[Fall of Kismayo|Kismayo]], the TFG attempted to [[Arms control|disarm]] the [[militia]]s of the country in late 2006. According to the UN/World Bank's Joint Needs Assessment (JNA) coordination secretariat, "the total estimated number of militias [militia members] to be demobilized is 53,000."<ref name=DEMOBILIZATION-OF-THE-MILITIA>[http://www.somali-jna.org/downloads/Demobilization%20of%20the%20Militia%203.doc Demobilization of the Militia] Somali Joint Needs Assessment (MS Word Doc)</ref> In 2005, they estimated that "there are 11-15,000 militia people controlling Mogadishu (out of national estimates ranging from 50,000 to 200,000)."<ref name=DISARMAMENT-DEMOBILIZATION-REINTEGRATION-SOMALIA-MILITIA>[http://www.somali-jna.org/downloads/ddr.doc Disarmament, Demobilization & Reintegration: Somalia Militia] Nairobi Kenya,
May 2005 (MS Word Doc)</ref>

==Past efforts==

{{further|Attempts at Reconciliation in Somalia (1991-2004)}}

Since the 1991 war, there have been over a dozen attempts to bring an end to the [[Somali Civil War]]. They were often shoaled by warlords and inter-clan rivalries.

==Recent efforts==

===2006 Islamic Courts Union===

In 2006, the [[Islamic Courts Union]] (ICU) attempted to disarm the secular warlords—a contentious issue which led to the [[Second Battle of Mogadishu]]. The Islamist victory resulted in the routing or forced surrender of many warlords and their caches of arms, and the rapid [[Rise of the Islamic Courts Union (2006)|rise of the Islamic Courts Union]].<ref name=SOMALI-RESISTANCE-OUTGUNNED>[http://www.taipeitimes.com/News/world/archives/2006/07/13/2003318585 Somalian resistance out gunned] AP, United Nations, Taipei Times</ref> By late 2006, their attempts to disarm the secular militias and their consolidation of power pitted them also against the [[Transitional Federal Government]].<ref name=ISLAMIC-LEADER-TAlKS-OF-PEACE-IN-SOMALIA>[http://www.wtop.com/?nid=387&sid=968050 Islamic Leader Talks of Peace in Somalia] Associated Press</ref>

The ICU attempted to curb the private possession of weapons, closing down the infamous Mogadishu arms market, and impounding or appropriating technicals for use solely by the Islamic Courts forces:
<blockquote>We were skeptical, but everyone we have spoken to since– doctors, teachers, journalists, shopkeepers– has talked of a city transformed. Gone are the ubiquitous checkpoints where the warlords’ militias killed, extorted and stole. Gone are their technicals, Jeeps with heavy machine guns mounted on the back. The infamous Bakaro arms markets has been closed. The only guns and technicals now are those of the Sharia courts enforcers, and the reports of violence in the papers were of the [[2006 Ipswich murder investigation|Ipswich murders]].<ref name=BATTLE-SCARRED-NATION-IS-AT-PEACE-WITH-ITSELF>[http://www.timesonline.co.uk/article/0,,3-2507129_1,00.html "Battle-scarred nation is at peace with itself... but still facing war"], Martin Fletcher, ''[[The Times]]'', December 16, 2006</ref></blockquote>

Many Somali warlords, along with the nation of [[Ethiopia]], sided with the [[Transitional Federal Parliament|TFG]] against the ICU. Once the [[War in Somalia (2006-present)|momentous battles]] were concluded in December 2006, the TFG proceeded with its plans to bring a general disarmament of the nation and closure to the [[Somali Civil War]].

===December 2006===

On December 29, 2006, [[Mohamed Qanyare]], a Mogadishu warlord, returned to the city and made a plea for the federal government to not disarm the militias.<ref name=SOMALIA-NEWS-SUMMARY-DEC-29-2006>[http://somalinet.com/news/world/Somalia/6301 Somalia: News summary for December 29, 2006] {{webarchive |url=https://web.archive.org/web/20070117030825/http://somalinet.com/news/world/Somalia/6301 |date=January 17, 2007 }} SomaliNet</ref> Qanyare was former TFG Security Minister before losing his position as a result of the [[Second Battle of Mogadishu]]. On December 31, surrounded in headquarters compound by a dozen [[Technical (fighting vehicle)|technicals]], he claimed to have 1,500 men under his command, and asserted government control over Mogadishu was an illusion, owed to the military might of Ethiopia.<ref name=AP-INTERVIEW-FORMER-WARLORD-CALLS-GOVERNMENT-CONTROL-ILLUSION>[http://www.iht.com/articles/ap/2006/12/31/africa/AF_GEN_Somalia_Warlord.php AP Interview: Former warlord calls government control of Somali capital an illusion] Associated Press</ref>

Ghedi's decree for disarmament also applied to non-government troops in the autonomous state of [[Puntland]], where it was seen as questionably enforceable.<ref name=SOMALIA-GOVERNMENT-ORDERS-PUNTLAND-TO-SURRENDER-WEAPONS>[http://www.garoweonline.com/stories/publish/article_6763.shtml Somalia govt orders Puntland to surrender weapons] {{webarchive |url=https://web.archive.org/web/20070106000000/http://www.garoweonline.com/stories/publish/article_6763.shtml |date=January 6, 2007 }} Garowe Online</ref>

===January 2007===

====Announcement of weapons collections and amnesty====

On January 1, 2007, Somali [[Prime Minister]] [[Ali Mohammed Ghedi]] announced "The warlord era in Mogadishu is now over."<ref name=SOMALI-PRIME-MINISTER-ORDERS-COMPLETE-DISARMAMENT>[http://www.theage.com.au/news/world/somali-prime-minister-orders-complete-disarmament/2007/01/02/1167500070792.html Somali prime minister orders complete disarmament] [[Associated Press]]</ref>

He said all civilian groups and businesses would have three days to disarm and turn their weapons in to the government. [[Technical (fighting vehicle)|Technicals]] were to be brought to the old port in Mogadishu. All collected arms would be registered at [[Villa Somalia]]. Villa Baidoa was also mentioned as an arms collection point.<ref name=ISLAMIC-MILITANTS-IN-SOMALIA-FLEE-AFTER-STRONGHOLD-FALLS>[http://www.iht.com/articles/ap/2007/01/02/africa/AF_GEN_Somalia.php Islamic militants in Somalia flee after stronghold falls] Associated Press</ref> An amnesty to Islamists was also extended.<ref name=SOMALI-GOVERNMENT-TO-DISARM-THE-CIVILIAN-POPULATIONS-IN-THREE-DAYS>[http://www.shabelle.net/news/ne1976.htm Somali government to disarm the civilian population in three days] [[Shabelle Media Network]]</ref>

Ghedi also made an appeal for [[Diplomatic and humanitarian efforts in the Somali Civil War|international aid efforts]] to continue, and for the establishment of a peacekeeping force (see [[IGASOM]]).<ref name="SOMALI-PRIME-MINISTER-ORDERS-COMPLETE-DISARMAMENT"/>

On January 2, Prime Minister Ghedi met with leaders of the Ayr subclan of the Habar-Gidir clan, a branch of the [[Hawiye]] tribe, to reassure the subclan regarding disarmament and to establish how they would work with the TFG. They had been supporters of the ICU.<ref name=SOMALIA-PREMIER-GEDI-MEETS-WITH-AYR-SUBCLAN-OVER-THE-DISARMAMENT>[http://somalinet.com/news/world/Somalia/6381 Somalia: Premier Gedi meets with Ayr sub-clan over the disarmament] SomaliNet</ref>

[[Abdi Qeybdid]] called for restoring peace and stability in the country. He asked that no reprisals be taken against the Islamists, and said he is not interested in getting back the battle wagons he had lost in the conflict, but hoped they were turned over to the government.<ref name=SOMALIA-FORMER-WARLORD-SAYS-NO-REVENGE-AGAINST-ISLAMISTS>[http://www.shabelle.net/news/ne1978.htm Somalia: Former warlord says no revenge against Islamists] Shabelle Media Network</ref>

On January 3, Police Commander [[Ali Mohamed Hassan Loyan]], who has only 1,000 officers under his command, admitted he was vastly outgunned: "I cannot say there is a viable police operation in Mogadishu." Meanwhile, the infamous [[Bakaara Market]] had re-opened and was doing brisk business.<ref name=EVERYONE-IN-SOMALIAS-CAPITAL-HAS-A-GUN>[http://www.iht.com/articles/ap/2007/01/03/africa/AF_GEN_Somalia_Weapons.php Everyone in Somalia's capital has a gun– everyone, that is, but the police] Associated Press</ref>

Two other warlords, including MP [[Mohamed Qanyare Afrah]] and his ally [[Abdi "Waal" Nur Siad]], questioned the government plan for a weapons turn-over without a plan for the protection of politicians.<ref name=SOMALIA-EX-WARLORDS-QUESTION-CALLS-FOR-PUBLIC-DISARMANENT>[http://www.garoweonline.com/stories/publish/article_6863.shtml Somalia: Ex-warlords question calls for public disarmament] Garowe Online</ref>

A group of 20 militia turned in their weapons, along with a machine gun mounted technical, in hopes of joining the newly forming army.<ref name=SOMALIA-ARMS-SURRENDERED-TO-GOVT-FOR-FIRST-TIME>[http://www.shabelle.net/news/ne1992.htm Somalia: Arms surrendered to the government for the first time] Shabelle Media Network</ref>

On January 4, government and Ethiopian forces began disarming residents of Jilib following an attack which killed two soldiers and wounded two others.<ref name=SOMALI-GOVT-FORCES-START-DISARMING-RESIDENTS-IN-JILIB>[http://www.shabelle.net/news/ne1997.htm Somali Government forces start disarming residents in Jilib] Shabelle Media Network</ref>

In Mogadishu, TFG militias set up checkpoints in the city. At one checkpoint, a group of militia apparently attempted to extort money from the driver of an oil tanker truck. In the ensuing argument, a rocket was fired at the vehicle, injuring at least 2 or 3 people. The vehicle had been carrying dozens of passengers who disembarked before the rocket attack.<ref name=SOMALIA-MILITIAS-BURN-VEHICLE-WOUND-3-AT-ILLEGAL-ROADBLOCK>[http://www.garoweonline.com/stories/publish/article_6870.shtml Somalia: Militias burn vehicle, wound 3 at illegal roadblock] Garowe Online</ref><ref name=TRUCK-ATTACKED-IN-SOMALIA-MANY-WOUNDED>[http://www.thewest.com.au/aapstory.aspx?StoryName=345497 Truck attacked in Somalia, many wounded] Reuters</ref><ref name=SOMALIA-TWO-PEOPLE-ARE-INJURED-AFTER-ARMED-MILITIAS-OPEN-FIRE-AT-A-TRUCK>[http://www.shabelle.net/news/ne1996.htm Somalia: Two people are injured after armed militias open fire at a truck] {{webarchive |url=https://web.archive.org/web/20070930000000/http://www.shabelle.net/news/ne1996.htm |date=September 30, 2007 }} Shabelle Media Network</ref>

On January 5, 1,000 soldiers from [[Puntland]] traveled to Mogadishu to help in the disarmament of the capital.<ref name=SOMALIA-PUNTLAND-FORCES-TO-ASSIST-IN-MOGADISHU-DISARMAMENT>{{cite news
  |title=Somalia: Puntland forces to assist in Mogadishu disarmament
  |publisher=[[Garowe Online]]
  |date=2006-01-05
  |url=http://www.garoweonline.com/stories/publish/article_6902.shtml
  |accessdate=2006-01-06 }} {{Dead link|date=October 2010|bot=H3llBot}}</ref> In [[Merca|Marka]], [[Lower Shabelle]], at least four people were killed in inter-militia fighting; two other militia and two civilians were wounded.<ref name=SOMALIA-4-DEAD-IN-CLAN-MILITIA-FIGHTING>{{cite news
  |title=Somalia: 4 dead in clan militia fighting
  |publisher=[[Garowe Online]]
  |date=2006-01-05
  |url=http://www.garoweonline.com/stories/publish/article_6901.shtml
  |accessdate=2006-01-06 }} {{Dead link|date=October 2010|bot=H3llBot}}</ref>

====Riots in Mogadishu====

On January 6, a crowd of more than 100 rioters gathered near Tarabunka square in Mogadishu. They protested the presence of Ethiopian troops as well as the plans to disarm the populace. Prime Minister Ghedi issued a decision to postpone the disarmament for an indefinite amount of time.<ref name=SOMALIA-RIOTS-IN-MOGADISHU-DISARMAMENT-POSTPONED>{{cite news|title=Somalia: Riots in Mogadishu, disarmament postponed |publisher=[[Garowe Online]] |date=2006-01-06 |url=http://www.garoweonline.com/stories/publish/article_6909.shtml |accessdate=2006-01-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20070108073607/http://www.garoweonline.com/stories/publish/article_6909.shtml |archivedate=January 8, 2007 }}</ref> At least seven were injured as police fired shots to disburse demonstrations around the city.<ref name=SOMALIA-SEVEN-DEMONSTRATORS-INJURED-IN-MOGADISHU>{{cite news|title=Somalia: Seven demonstrators injured in Mogadishu |publisher=[[Shabelle Media Network]] |date=2006-01-06 |url=http://www.shabelle.net/news/ne2014.htm |accessdate=2006-01-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20070930163824/http://www.shabelle.net/news/ne2014.htm |archivedate=September 30, 2007 }}</ref>

====Agreement to Disarm====

On January 12, the same day as the [[Battle of Ras Kamboni]] ended, Somali warlords tentatively agreed with President [[Abdullahi Yusuf Ahmed|Abdullahi Yusuf]] to disarm their militias and to direct their members to apply to join the national army or police forces. An estimated 20,000 militia were said to exist throughout Somalia. [[Mohamed Qanyare Afrah]] said the clans were "fed up" with militias and agreed to disarm his own men. [[Muse Sudi Yalahow]] was less conciliatory.<ref name=SOMALI-WARLORDS-AGREE-TO-DISARM-AS-GOVT-TROOPS-CAPTURE-LAST-ISLAMIC-HOLDOUT-IN-SOUTH>{{cite news
|title=Somali warlords agree to disarm as government troops capture last Islamic holdout in south
|url=http://www.signonsandiego.com/news/world/20070112-1327-somalia-optional.html
|publisher=[[Associated Press]]
|date=2007-01-12
|accessdate=2007-01-12 }}</ref>

Meanwhile, even as the meeting was taking place, fighting outside [[Villa Somalia]] killed seven people. The members of government and warlords present for the meeting<ref name=SOMALIA-LEADERS-EX-WARLORDS-REACH-DEAL-AS-FIGHTING-KILLS-7>{{cite news
|title=Somalia leaders, ex-warlords reach deal as fighting kills 7
|url=http://www.garoweonline.com/stories/publish/article_7042.shtml
|publisher=[[Garowe Online]]
|date=2007-01-12
|accessdate=2007-01-12 }} {{Dead link|date=October 2010|bot=H3llBot}}</ref> included the following individuals:

* President [[Abdullahi Yusuf Ahmed]]
* Prime Minister [[Ali Mohammed Ghedi]]
* Defense Minister Col. [[Barre Hirale]] - former leader of the [[Juba Valley Alliance|JVA]]
* [[Mohamed Qanyare Afrah]] - MP
* [[Muse Sudi Yalahow]] - MP, former leader of [[ARPCT]]
* [[Omar Filish]] - MP
* [[Botan Ise Alin]] - MP
* Col. [[Abdi Qeybdiid]] - leader of the self-declared state of [[Galmudug]]
* [[Abdi Waal]]

====Martial law declared====

On January 13 the [[Transitional Federal Parliament]] (TFP), by a count of 154-to-2, voted to give the President the power to declare [[martial law]] to restore order. Yet on the same day, 9 people were reported killed in fighting in Biyo-Adde in central Somalia.<ref name=MARTIAL-LAW-DECLARED-IN-SOMALIA>{{cite news
|title=Martial law declared in Somalia
|url=http://news.bbc.co.uk/2/hi/africa/6258415.stm
|date=2007-01-13
|publisher=[[BBC]]
|accessdate=2007-01-16 }}</ref> Prime Minister Gedi first stated that martial law would be declared on December 29, 2006; parliament's vote vested the government with the proper legal authority.<ref name=MARTIAL-LAW-FOR-SOMALIA-AFTER-ISLAMISTS-FLEE>{{cite news
|title=Martial law for Somalia after Islamists flee capital
|url=http://www.reliefweb.int/rw/rwb.nsf/db900SID/TKAE-6WX3WV?OpenDocument
|date=2006-12-29
|publisher=[[Reuters]]
|accessdate=2007-01-16 }}</ref>

Given their new powers, on January 15, 2007, the TFG [[Propaganda in the War in Somalia#Media manipulation and repression|closed the radio stations]] for Shabelle Radio, Horn Afrik, IQK, and the television station Al-Jazeera in Mogadishu. The ban was lifted a day later.

====Warlords turn in arms====

On January 17, 2007, [[Mohamed Qanyare]] and [[Muse Sudi Yalahow]] were the first warlords of Mogadishu to disarm, turning over their weapons and committing their militiamen to the government, though some of Sudi's arms remained in other locations controlled by Qanyare and Mohammed Dhere. Approximately 60 [[technical (fighting vehicle)|technical]]s and 600 soldier's weapons were turned in to the government, including two technicals turned in by Interior Minister [[Hussein Mohammed Farah Aidid]]. The arms were accepted by the chief commander of the government army, General Naji.<ref name=SOMALIA-WARLORDS-LAY-DOWN-WEAPONS>{{cite web
|title=Somalia: Warlords lay down weapons
|url=http://somalinet.com/news/world/Somalia/6756
|publisher=[[SomaliNet]]
|date=2007-01-17
|accessdate=2007-01-17 }}</ref><ref name=WARLORDS-FINALLY-SURRENDER-THEIR-ARMS-TO-THE-TFG>{{cite news|title=Warlords finally surrender their arms to the Somali TFG |url=http://www.shabelle.net/news/ne2095.htm |publisher=[[Shabelle Media Networks]] |date=2007-01-17 |accessdate=2007-01-17 |deadurl=yes |archiveurl=https://web.archive.org/web/20070930154916/http://www.shabelle.net/news/ne2095.htm |archivedate=September 30, 2007 }}</ref>

====Call for National Reconciliation Conference====

On January 30, 2007, President Yusuf called for a new national reconciliation conference to be held within three weeks. In related news, a commitment for 4,000 peacekeepers had been made by the AU, and the search continued for another 4,000 to constitute the full planned contingent.<ref name=SOMALI-PRESIDENT-CALLS-FOR-NATIONAL-RECONCILIATION-CONFERENCE>{{cite news
|title=Somali president calls for national reconciliation conference: Official
|url=http://www.garoweonline.com/stories/publish/article_7355.shtml
|date=2007-01-30
|publisher=[[Garowe Online]]
|accessdate=2007-02-01 }} {{Dead link|date=October 2010|bot=H3llBot}}</ref>

Eventually, the [[2007 Somali National Reconciliation Conference]] was called on 2007-03-01 to begin on 16 April 2007.

==References==
{{Reflist|2}}

==External links==
* [http://www.somali-jna.org Somali Joint Needs Assessment] United Nations & World Bank Coordination Secretariat
* [http://www.womenwarpeace.org/somalia/somalia.htm Country Profiles, Reports and Fact Sheets on Somalia] WomenWarPeace.org

== See also ==
{{War in Somalia (2006–2009)}}
{{Africa topic|Gun politics in}}

[[Category:2007 in Somalia]]
[[Category:Military disbanding and disarmament]]