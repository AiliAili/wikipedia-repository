{{unreferenced|date=December 2007}}
The '''''Archiv für Sozialwissenschaft und Sozialpolitik''''' ({{ISSN|0174-819X}}, English: '''''Archives for Social Science and Social Welfare''''') was an [[academic journal]] for the [[social sciences]] in Germany between 1888 and 1933. Its first [[Editor-in-chief|editors]] were [[Edgar Jaffé]], [[Werner Sombart]], and [[Max Weber]]. The latter published his seminal essay ''The Protestant Ethic and the Spirit of Capitalism'' in the journal in two parts in 1904 and 1905.

Jaffé bought the journal in 1903 for 60000 Mark from the Social Democrat [[Heinrich Braun (writer)|Heinrich Braun]], who had founded and edited the journal under the title ''Archiv für soziale Gesetzgebung und Statistik (Archive for Social Legislation and Statistics)'' since 1888 and changed its title to ''Archiv für Sozialwissenschaft und Sozialpolitik'' in 1904.

In 1933, when the [[Nazism|Nazis]] gained power in Germany, the last editor of the Archive, then-editor Emil Lederer and half of the editorial staff of the journal were forced to emigrate and the journal ceased to exist.

==Essays published in the ''Archiv''==
*[[Max Weber]], "[[The Protestant Ethic and the Spirit of Capitalism]]" ("Die protestantische Ethik und der Geist des Kapitalismus"), ''Archiv für Sozialwissenschaften'' 20, no. 1 (1904), pp. 1–54; 21, no. 1 (1905), pp. 1–110.
*[[Ludwig von Mises]], "[[Economic Calculation in the Socialist Commonwealth]]" ("Die Wirtschaftsrechnung im sozialistischen Gemeinwesen"), ''Archiv für Sozialwissenschaften'' 47, no. 1 (1920), pp. 86–121.

{{DEFAULTSORT:Archiv fur Sozialwissenschaft und Sozialpolitik}}
[[Category:Defunct journals]]
[[Category:1888 establishments in Germany]]
[[Category:Publications established in 1888]]
[[Category:Publications disestablished in 1933]]
[[Category:German-language journals]]
[[Category:Sociology journals]]
[[Category:1933 disestablishments in Germany]]

{{italic title}}
{{socialscience-journal-stub}}