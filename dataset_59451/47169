The '''Commonwealth Paraplegic Games''' were an international, [[multi-sport event]] involving athletes with a disability from the [[Commonwealth of Nations|Commonwealth countries]]. The event was first held in 1962 and disestablished in 1974.<ref>{{cite book|last1=DePauw|first1=Karen P|last2=Gavron|first2=Susan J|title=Disability sport|url=https://books.google.com/books?id=BPsqAoFtG-sC&pg=PA102|accessdate=25 February 2012|year=2005|publisher=Human Kinetics|isbn=978-0-7360-4638-1|pages=102–}}</ref>  The event was sometimes referred to as the Paraplegic Empire Games and British Commonwealth Paraplegic Games. The Games were held in the country hosting the [[Commonwealth Games]] for abled bodied athletes. Athletes were generally those with spinal injuries or [[poliomyelitis|polio]]. The Games were an important milestone in the Paralympic sports movement as they began the decline of the [[Stoke Mandeville Games]]' dominating influence.<ref name=bailey>{{cite book|last=Bailey|first=Steve|title=Athlete first : a history of the Paralympic movement|year=2008|publisher=John Wiley|location=Chichester|isbn=978-0-470-05824-4|pages=34}}</ref>

==Founding and establishment==
The Games were the initiative of [[George Bedbrook]], Director of the Spinal Unit of [[Royal Perth Hospital]].<ref>{{cite book|last=Bailey|first=Steve|title=Athlete first : a history of the Paralympic movement|year=2008|publisher=John Wiley|location=Chichester|isbn=978-0-470-05824-4|pages=26}}</ref> In Australia, paraplegic sports activities were first held in 1954 with the First Royal Perth Hospital Games in 1954 at the Shenton Park Annex.<ref name=prog>{{cite book|title=Commonwealth Paraplegic Games : official programme|year=1962|publisher=Paraplegic Association of Western Australia|location=Perth|pages=4}}</ref>   In 1956, Bedbrook was encouraged during a visit by [[Ludwig Guttmann]], the founder of  the Stoke Mandeville Games, to help organise disabled sport in Australia. In 1959, the Paraplegic Association of Western Australia, acting through Royal Perth Hospital, began to publicise the Paraplegic Empire Games just prior to the British Empire Games to be held in Perth in 1962.<ref>{{cite journal|last=Bedbrook|first=George|title=Paraplegic Empire Games : Letters to the Editor|journal=Canadian Medical Association Journal|date=15 November 1959|volume=81|pages=855|pmc=1831411|issue=10|pmid=20326048}}</ref>

==Games Summary==

{| class="wikitable" style="text-align:center; font-size:90%;"
|-
! Games
! Year
! Host
! Dates
! Nations
! Competitors
|-
|I
|[[1962 Commonwealth Paraplegic Games|1962]]
|align=left| [[Perth, Western Australia|Perth]], [[Australia]]
|align=left| 10–17 November
|9 || 93
|-
|II
|1966
|align=left|[[Kingston, Jamaica]]
|align=left|14–20 August 
|10 || 133
|-
|III
|[[1970 Commonwealth Paraplegic Games|1970]]
|align=left|[[Edinburgh]], Scotland
|align=left|26 July&nbsp;– 1 August
|14 || 192
|-
|IV
|1974
|align=left|[[Dunedin]], [[New Zealand]]
|align=left|13–19 January
|14 ||232 *
|}
* missing Canadian competitor numbers

===1st Perth, Western Australia===
{{Main|1962 Commonwealth Paraplegic Games}}

An Organising Committee was established with [[Hugh Leslie]], Executive Chairman, [[George Bedbrook]], General Secretary and Mrs M.R. Fathers, Secretary.<ref name="report">{{cite book|title=Report of the first Commonwealth Paralympic Games|year=1962|publisher=Paraplegic Association of Western Australia{{!}}|location=Perth|pages=4}}</ref> The Games were opened by the [[Governor of Western Australia]], [[Charles Gairdner|Sir Charles Gairdner]] on 10 November 1962.<ref name="report"/> Two Perth facilities were used: the [[Claremont Showgrounds|Royal Agricultural Showgrounds in Claremont]] for accommodation and most sporting events and the City of Perth Aquatic Centre, [[Beatty Park]] for swimming. Medals were awarded in the following sports: archery, dartchery, athletics, swimming, weightlifting, fencing, snooker, table tennis and basketball. Nine countries participated -[[England]], [[India]], [[New Zealand]], [[Northern Ireland]], [[Rhodesia]], [[Scotland]], [[Singapore]], [[Wales]] and [[Australia]], and there were 93 athletes.<ref name="report"/> A film of the Games was made.<ref>{{cite web|url=http://trove.nla.gov.au/work/158764438|title=First Commonwealth Paralympic Games|publisher=[[National Library of Australia]]|accessdate=25 February 2012|year=1962}}</ref>

'''Medal Table'''
{| class="wikitable"
|-
! Country
! Gold
! Silver
! Bronze
|-
| Australia
| 38
| 21
| 23
|-
| England
| 30
| 41
| 19
|-
| Rhodesia
| 15
| 3
| 5
|-
| Scotland
| 2
| 10
| 4
|-
| New Zealand
| 2
| 0
| 1
|-
| Wales
| 1
| 0
|3
|-
| India
| 1
| 0
| 2
|-
| Singapore
| 0
| 0
| 0
|-
| Northern Ireland
| 0
| 0
| 0
|}

===2nd Kingston, Jamaica===
There were 133 athletes from 10 countries.<ref name=gh/>

===3rd Edinburgh, Scotland===
{{Main|1970 Commonwealth Paraplegic Games}}

There were 192 athletes from 14 countries.<ref name="GH 6Nov2016">{{cite news |url=https://news.google.com/newspapers?id=hJFAAAAAIBAJ&sjid=-6QMAAAAIBAJ&pg=4319%2C356003 |title=Wheelchair athletes close Games on a proud note |work=Glasgow Herald |date=3 August 1970 |page=16 |accessdate=6 November 2016}}</ref>  The Games were opened by Prime Minister [[Edward Heath]] immediately after the Commonwealth Games.<ref name="GH 6Nov2016" /> The chairman of the Organising Committee was Lieutenant-Colonel John Fraser.<ref name=gh>{{cite news|title=Premier to open games for invalids|url=https://news.google.com/newspapers?nid=2507&dat=19700411&id=hpJAAAAAIBAJ&sjid=DaUMAAAAIBAJ&pg=2954,2343152|accessdate=20 February 2012|newspaper=Glasgow Herald|date=11 April 1970}}</ref> Sporting events were held at [[Meadowbank Stadium|Meadowbank Sports Centre]] and the [[Royal Commonwealth Pool]],<ref name=sis>{{cite journal|last=Sandeman|first=Sylvia|title=50th Anniversary of SIS|journal=Newsline – Spinal Injuries Scotland|year=2010|issue=Winter|pages=|url=http://www.sisonline.org/common/downloads/newsline-magazine/winter-2010.pdf|accessdate=20 February 2012|format=PDF}}</ref> and the Games Village was based at RAF Turnhouse located at [[Edinburgh Airport]].<ref name=gh/>

===4th Dunedin, New Zealand===
The competing countries and competitors were: New Zealand (32 ), Scotland ( 22 – 17 male, 5 female), Jamaica ( 15 – 9 male, 6 female), Malaysia (3 male), Australia (53), Mauritius (1 observer only), Kenya (12 – 11 male, 1 female), England (53 – 37 male, 16 female), Northern Ireland (7), Wales (12), Hong Kong (17 – 14 male, 3 female), Fiji (3 male), Singapore (2), India (1), Canada? (not known).<ref>{{cite book|title=Dr N.R. Jefferson  – Papers relating to the New Zealand Paraplegic & Physically Disabled Federation Inc.’, 1968–2002 , MS-1479|location=Hocken / Uare Taoka o Hākena, University of Otago Dunedin , New Zealand}}</ref>  Medals were awarded in the following sports: archery, dartchery, bowl and field events (javelin throw, precision javelin, shot putt, discus), track events (60m, 100m, 200m), pentathlon, swimming, weightlifting, fencing, snooker, table tennis and basketball.<ref>{{cite book|title=Official Sports Programme – Fourth British Commonwealth Paraplegic Games|year=1974|location=Dunedin}}</ref> Venues used were [[Caledonian Ground]], [[Logan Park, Dunedin|Logan Park]], University of Otago, Physical Education Gymnasium, R.S.A. Hall, Moana Pool and St Kilda Smallbore Rifle Range. The Games were opened by [[Denis Blundell|Sir Denis Blundell]], [[Governor General of New Zealand]].

==Disestablishment and heritage==
The Dunedin Games were the final Commonwealth Paraplegic Games mainly due to travel logistics and costs.<ref name=bailey/>  The Commonwealth Paraplegic Games Committee recommended to the International Stoke Mandeville Games Committee that the 'World Zone Games' be established. These Games did not come into fruition.<ref name=bailey/> However, Sir George Bedbrook helped to establish a Pacific Rim competition called the [[FESPIC Games|Far East and South Pacific Games for the Disabled]].<ref>{{cite web|url=http://www.pbf.asn.au/index.php?id=173|title=Sir George Bedbrook – Paralympic Hall of Fame inductee|publisher=Paraplegic Benefit Fund|accessdate=25 February 2012}}</ref>

In the [[Commonwealth Games]], athletes with a disability were first included in exhibition events at the [[1994 Commonwealth Games|1994 Victoria, Canada Games]].<ref>{{Cite web
 |last1=Van Ooyen and Justin Anjema 
 |first1=Mark 
 |last2=Anjema 
 |first2=Justin 
 |title=A Review and Interpretation of the Events of the 1994 Commonwealth Games 
 |publisher=Redeemer University College 
 |date=25 March 2004 
 |url=http://sporthamilton.com/content/histroy/1994commonwealthgames.pdf 
 |accessdate=25 February 2012 
 |format=PDF 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20130731114220/http://sporthamilton.com/content/histroy/1994commonwealthgames.pdf 
 |archivedate=31 July 2013 
 |df= 
}}</ref> At the [[2002 Commonwealth Games|2002 Manchester Games]] they were included as full members of their national teams, making them the first fully inclusive international multi-sport games. This meant that results were included in the medal count.<ref>{{cite web|title=Para-sports for elite athletes with a disability|url=http://www.thecgf.com/sports/ead.asp|work=Commonwealth Games Federation website|accessdate=20 February 2012}}</ref>

==See also==
{{portal|Commonwealth Games}}

==References==
{{reflist}}

{{Commonwealth Games years}}

[[Category:Commonwealth Games|*Commonwealth Paraplegic Games]]
[[Category:Commonwealth Paraplegic Games| ]]
[[Category:Recurring sporting events established in 1962]]
[[Category:Recurring events disestablished in 1974]]