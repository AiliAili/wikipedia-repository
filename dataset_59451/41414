{{other ships|HMS Black Joke}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Black Joke (1827).jpg|300px]]
|Ship caption=''HMS ''Black Joke'' firing on El Almirante'' by [[Nicholas Matthews Condy]]
}}
{{Infobox ship career
|Hide header=
|Ship country=Brazil
|Ship flag={{shipboxflag|Empire of Brazil}}
|Ship name=''Henriquetta''
|Ship ordered=
|Ship builder=[[Baltimore]]
|Ship laid down=
|Ship launched=1824?
|Ship acquired=1825
|Ship commissioned=
|Ship decommissioned=
|Ship in service=
|Ship out of service=
|Ship renamed=
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship captured=
|Ship fate=Captured by the Royal Navy in 1827
|Ship status=
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[United Kingdom|United&nbsp;Kingdom]]
|Ship flag={{shipboxflag|United Kingdom|naval}}
|Ship name=HMS ''Black Joke''
|Ship namesake=[[Black Joke]] (bawdy song)
|Ship acquired=Captured 1827
|Ship commissioned=1827
|Ship decommissioned=May 1832
|Ship fate=Burnt on orders from London
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=[[Baltimore clipper]]
|Ship tons burthen=Approx. 260 ton ([[Builder's Old Measurement|bm]])
|Ship length={{convert|90|ft|10|in|m|abbr=on}}<ref name=TT/>
|Ship beam={{convert|26|ft|7|in|m|abbr=on}}<ref name=TT/>
|Ship draught=
|Ship draft=
|Ship hold depth=
|Ship sail plan=[[Brig]]
|Ship complement=34 sailors & 5 marines
|Ship armament=One 18-pounder [[cannon]] pivot-mounted
|Ship notes=
}}{{Infobox service record|is_multi=yes
|is_ship=yes
|partof=[[West Africa Squadron]]
|codes=
|commanders=*1827–1828 Lt. W. Turner
*1828–1829 Lt. Henry Downes
*1829–1832 Lt. William Ramsay
|operations=
|victories=
}}
|}
The third '''HMS ''Black Joke''''' was probably built in [[Baltimore]] in 1824, becoming the Brazilian [[slave ship]] ''Henriquetta''.<ref name=RNM>{{cite web|url=http://www.royalnavalmuseum.org/visi_cfimage_blackjoke.htm|title=Slave ships and Squadron Vessels|publisher=Royal Naval Museum|accessdate=2011-01-09}}</ref> The [[Royal Navy]] captured her in September 1827 and purchased her into the service. The Navy re-named her ''Black Joke'', after an [[Black Joke|English song of the same name]], and assigned her to the [[West Africa Squadron]] (or ''Preventative Squadron''). Her role was to chase down slave ships, and over her five-year career she freed many hundreds of slaves. The Navy deliberately burnt her in May 1832 because her timbers had rotted to the point that she was no longer fit for active service.

==''Henriquetta'' – slaver==
Built as a [[Baltimore clipper]] (possibly as the vessel ''Griffen''<ref name=TT>Footner (1998), p.155.</ref>), ''Henriquetta'' (also ''Henri Quatre'') was a [[brig]] designed to be fast. [[Empire of Brazil|Brazilian]] owners purchased her in 1825,<ref name=TT/> and she worked for a slave dealer at [[Bahia]], making 80,000 pounds
(approximately £{{formatnum:{{inflation|UK|80000|1825|r=-4}}}} in {{Inflation-year|UK}}, when adjusted for inflation), by running 3,040 slaves across to Brazil in six voyages over a period of three years.<ref>Villiers, Allan  (October 1955) "The Drive for Speed at Sea", ''American Heritage'' vol. 6 no. 6.</ref><ref name=Tinnie>Tinnie (2008).</ref>

[[French frigate Sibylle (1792)|HMS ''Sybille'']] captured her on 6 September 1827.<ref>Log of [[French frigate Sibylle (1792)|HMS ''Sybille'']] held by [[The National Archives (United Kingdom)|The National Archives]], archival reference ADM 51/3466</ref> Commodore [[Francis Augustus Collier|Francis Collier]] of ''Sybille'' wrote to the [[Admiralty]] noting that at the time of her capture ''Henriquetta'' was 257 tons, mounted three guns and had a crew of 38 men. She had 569 enslaved Africans on board "and had landed at Bahia 3,360 slaves in the last two years".<ref>Correspondence from Collier to the Admiralty, 12 December 1827, held at The National Archives, archival reference ADM 1/1682.</ref>

She was sold at auction in [[Sierra Leone]] on 5 January 1828, for £330 (approximately £{{formatnum:{{inflation|UK|330|1828|r=-3}}}} in {{Inflation-year|UK}}).<ref name=Tinnie/>{{#tag:ref|A first-class share of the prize money, i.e., the amount accruing to Collier, was [[£sd|£]]1946 13[[shilling|s]] 10½[[pence|d]]; a sixth-class share, that of an ordinary seaman, was worth £5 9s 11¼d.<ref name=LG18689>{{London Gazette|issue=18689|startpage=1166|endpage=1167|date=11 June 1830}}</ref>|group=Note}}

==''Black Joke'' – slave catcher==
{{Suppression of the Slave Trade}}
The Navy took her into service as a tender to ''Sybille'', under the command of Lieutenant William Turner of ''Sybille''. During her service with the Navy, ''Black Joke''{{'}}s crew included an assistant surgeon, three midshipmen, thirty seamen and five [[Royal Marines|marines]] as well as a number of [[Liberia]]n [[Seedies and Kroomen|Kroomen]] for use on detached boat service. Her armament consisted of one long 18-pounder on a pivot mount.<ref group=Note>Some accounts give her two guns, but do not specify their type.</ref>

On 5 January 1828 she sailed with ''Sybille'' and the 20-gun [[post ship]] [[HMS Esk (1813)|''Esk'']]. On 12 January she captured the Spanish schooner ''Gertrudes'' (or ''Gertrudis''), which carried 155 slaves.<ref name="Black Joke 1832"/>{{#tag:ref|A first-class share of the prize money was worth £393 4s 8½d; a sixth-class share was worth £1 9s 9¾d.<ref name=LG18689/>|group=Note}} ''Gertrudes'' had outrun the other two British warships, but not ''Black Joke''.

On 2 April a Spanish 14-gun brig fired on ''Black Joke'' as she approached the brig. After two hours of exchanging shots, and after suffering several casualties, the brig hoisted a flag of truce. She turned out to be the ''Providentia'', of 14 guns and a crew of 80 men. She had fired on the ''Black Joke'' as ''Providentia''{{'}}s captain had been warned that a Colombian privateer answering to the same description as ''Black Joke'' was in the area. Turner therefore released her. ''Black Joke'' suffered no casualties; ''Providentia'' had numerous of her crew killed and wounded.<ref name="Black Joke 1832">"Life and Adventures of the Black Joke, recently deceased at Sierra Leone" (1832) ''United service magazine'' vol.10.</ref>

On 1 May 1828 ''Black Joke'' fought the large and well-armed pirate ''Presidenté''.  After two hours of action, and following the death of their captain and two others, as well as the wounding of a number more, the crew of the ''Presidenté'' sought a truce.  (''Black Joke'' sustained one killed and a number wounded.)  The crew of ''Presidenté'' underwent an examination before being committed for trial on charges of piracy.  Many of her crew appeared to be British or have anglicized names, and they were sent back to England for trial. The next day ''Black Joke'' retook the Portuguese vessel ''Hosse'', which ''Presidenté'' had taken as a prize. ''Presidente'' was lost at sea on her way to Sierra Leone but ''Black Joke'' earned salvage money for ''Hosse''.<ref name="Black Joke 1832"/>{{#tag:ref|A first-class share of the prize money was worth £337 17s 10½d; a sixth-class share was worth £1 7s 5¼d.<ref name=LG18689/>|group=Note}}

On 16 May, ''Black Joke'' captured ''Vengador''.<ref name=PB>{{cite web|url=http://www.pbenyon.plus.com/18-1900/B/00572a.html|title=HMS ''Black Joke'' at the Naval Database website|accessdate=2011-01-09}}</ref> She had a crew of 45 men and eight guns but offered no resistance. She carried 645 slaves, the most ever captured on a single ship.<ref name="Black Joke 1832"/>{{#tag:ref|A first-class share of the prize money was worth £1496 12s 5d; a sixth-class share was worth £5 16s 1½d.<ref name=LG18689/>|group=Note}}

On 14 September ''Black Joke'' was in company with [[HMS Primrose (1810)|''Primrose'']] when ''Primrose'' captured the ''Zephirina'' or ''Zephorina''.<ref name=LG18788>{{London Gazette|issue=18788|date=29 March 1831|startpage=597}}</ref> ''Zephorina'' was carrying 218 slaves.{{#tag:ref|A first-class share of the bounty money was worth £647 9s 3d; a sixth-class share was worth £3 12s 6d.<ref>{{London Gazette|issue=18791|startpage=672|date=8 April 1831}}</ref>|group=Note}}

On 14 November Turner received promotion to commander. He turned over command of ''Black Joke'' to Lieutenant Henry Downes of ''Sybille''. In November of the same year ''Black Joke'' was forced to leave the coast of [[Bioko]] (Fernando Po), due to fever on board.

[[Image:HMS Black Joke (1827) and prizes.jpg|thumb|left|HMS ''Black Joke'' and prizes (clockwise from top left) ''Providentia, Vengador, Presidenta, Marianna, El Almirante,'' and '' El Hassey'']]
In January 1829 ''Black Joke'' saw a Spanish brig as the Spaniard loaded slaves and set sail for [[Havana]]. ''Black Joke'' chased the Spaniard for 31 hours and on 1 February, when the wind dropped, resorted to [[oar|sweeps]] to bring herself within gunshot of her prey. ''El Almirante'' mounted a total of 14 guns (ten Gover's 18-pounder [[cannon]] and four long 9-pounders) and had a crew of 80 men.<ref group=Note>John Gover designed a new type of gun-carriage in the late 1790s or early 1800s that enabled the guns to be handled by smaller gun crews and the guns to be stored alongside the rails rather than perpendicular to them (de Toussard 1809: pp.357-362).</ref> ''Black Joke'' was almost half the size of ''El Almirante'' and mounted two guns.  Good ship-handling, the discipline of the Royal Navy gun crew, and light winds gave Lieutenant Downes the advantage. In 80 minutes he defeated and captured the slaver, which  suffered 15 dead, including the captain and the first and second mates, and a further 13 wounded, while ''Black Joke'' suffered six wounded, two of whom died later.<ref>{{London Gazette|issue=18568|date=17 April 1829|startpage=710}}</ref> ''El Almirante'' held 466 slaves, who were later landed.<ref>{{cite web|url = http://publishing.yudu.com/An8w/navynewsjune2007/resources/36.htm|title = Navy News, June 2007|accessdate =2008-02-09}}</ref>{{#tag:ref|A first-class share of the prize money was worth £1014 5d; a sixth-class share was worth £3 15s 5¾d.<ref name=LG18689/>|group=Note}}

On 6 March ''Black Joke'' captured the 2-gun brigantine ''Carolina'', which carried 420 slaves.<ref name=PB/>{{#tag:ref|A first-class share of the prize money was worth £893 3s 6d; a sixth-class share was worth £3 9s 2¼d.<ref name=LG18689/>|group=Note}} After this capture Downes was invalided home because of illness, and received a promotion to Commander on his return in recognition of the capture of ''El Almirante''. He had freed a total of 875 slaves.<ref name="Black Joke 1832"/>

''Black Joke'' then came under the command of Lieutenant E.J. Parrey.  On 11 October he captured the ''Christina'' (or ''Cristina''), a Spanish schooner of three guns and 24 crew members. She was carrying 354 slaves.<ref name="Black Joke 1832"/>{{#tag:ref|A first class share of the prize money was worth £696 7d; a sixth-class share was worth £2 12s 2d.<ref name=LG18830>{{London Gazette|issue=18830|startpage=1539|endpage=1540|date=29 July 1831}}</ref>|group=Note}} Lieutenant William Coyde replaced Parrey, and on 1 April 1830 captured the Spanish brigantine ''Manzanares'' of three guns and 34 crew. She was carrying 354 slaves.<ref name="Black Joke 1832"/>{{#tag:ref|A first class share of the prize money was worth £815 5s 11d; a sixth-class share was worth £2 19s 11d.<ref name=LG18830/>|group=Note}}

Later that month ''Black Joke'' was in refit in [[Sierra Leone]]. Coyde's replacement in ''Black Joke'' was Lieutenant William Ramsay.<ref name="Black Joke 1832"/>

On 9 November she captured ''Dos Amigos'', a Baltimore schooner with a crew of 34 and armed with a single carronade; ''Dos Amigos'' had 567 African captives aboard, but may have relanded them before her capture.<ref name="Black Joke 1832"/> The Admiralty put ''Dos Amigos'' up for auction where the commodore of the British Anti-Slavery Squadron, Jonathan Hayes, bought her and named her {{HMS|Fair Rosamond| 1830|2}}. In December ''Black Joke'' was cruising in the [[Bight of Benin]] with {{HMS|Medina|1813|2}}.

On 21 or 22 February 1831 ''Black Joke'' captured a slaver with 300 slaves on board. This was probably the Spanish schooner ''Primeira''. At the time ''Black Joke'' was acting as a tender to {{HMS|Dryad|1795|2}},<ref>{{London Gazette|issue=18922|startpage=715|date=30 March 18321}}</ref>{{#tag:ref| A first-class share, which would have accrued to the captain of ''Dryad'', was worth £369 2s 6d. A second first-class share, which would probably have accrued to Ramsey, was worth £184 11s 3d; a sixth-class share was worth £1 3s 6¾d.<ref>{{London Gazette|issue=18932|startpage=966|date=1 May 1832}}</ref>|group=Note}} and was under the temporary command of W L Castle.<ref name="Black Joke 1832"/>

In a famous action on 25 or 26 April 1831, ''Black Joke'' was again under Ramsey's command when she captured the ''Marinerito''.<ref>{{London Gazette|issue=18933|startpage=992|date=4 May 1832}}</ref> ''Black Joke'' captured the much larger and more heavily armed Spanish slaver off the island of [[Fernando Po (island)|Bioko]]. At one point, 14-year-old Midshipman Hinde from ''Black Joke'' took command and rescued the boarding party, including Ramsey, which had become stranded on the Spanish slaver's deck. ''Marinerito'' had 15 of her crew killed; ''Black Joke'' lost one man killed and four wounded, one of whom was Ramsey. Of the 496 slaves on ''Marinerito'', 26 were found to have died and 107 were in so weakened a state that they were landed on Bioko, where more than half subsequently died. The remainder were taken to freedom in Sierra Leone.{{#tag:ref|A first-class share was worth £533 8s 9d; a sixth-class share was worth ₤1 18s 6¾d.<ref>{{London Gazette|issue=18935|startpage=1053|date=11 May 1832}}</ref>|group=Note}}

In September, in company with ''Fair Rosamond'', ''Black Joke'' chased two Spanish slavers into the [[Bonny River]].  Lieutenant Ramsey, reported that "during the chase they were seen to throw their slaves overboard, by twos shackled together by the ankles, and left in this manner to sink or swim." ''Fair Rosamond'' captured the Spanish vessels, ''Regulo'' and ''Rapido'', on 10 September and took them to [[Sierra Leone]], where the [[Admiralty Court]] condemned them.  ''Black Joke'' freed 39 slaves, for which a half bounty was paid to the captain and crew.  A further bounty was paid for the 29 slaves who died between the capture and the condemnation of the ''Regulo''.<ref>{{London Gazette|issue=19119|date=14 January 1834|startpage=82}}</ref>{{#tag:ref|A first-class share was worth £383 8s 1d; a sixth-class share was worth £1 8s 6d.<ref>{{London Gazette|issue=18975|startpage=2061|endpage=2062|date=11 September 1832}}</ref> Later it was discovered that nine men had been left off the prize list for the capture of the ''Potosi'' (by ''Fair Rosamond''), and ''Rapido'' and ''Regulo'', and the holders of sixth-class shares were obliged to return 1s 0¾d for distribution to these men.<ref name=LG18933/> However, a prize court reinstated a half-bounty for 29 slaves who had died prior to the ''Regulo{{'}}''s condemnation. A captain's share was £13 10s, a commander's share was £6 5s, and a sixth-class share was 11½d.<ref>{{London Gazette|issue=19119|startpage=82|endpage=83|date=14 January 1834}}</ref>|group=Note}}

Ramsey received promotion to the rank of commander for the capture of ''Marinerito'' and handed over command to Lieutenant H V Huntley.<ref name="Black Joke 1832"/> On 15 February 1832, ''Black Joke'' captured Spanish schooner ''Frasquita'', alias ''Centilla'', which was armed with two guns and had a crew of 31 men. ''Frasquitta'' yielded bounty money for the 290 slaves on board her.<ref name="Black Joke 1832"/>{{#tag:ref|A first-class share was worth £303 3s 6d; a sixth-class share was worth £1 1s 9¾d.<ref name=LG18933>{{London Gazette|issue=18993|startpage=2477|endpage=2478|date=9 November 1832}}</ref>|group=Note}}

In all, between November 1830 and March 1832, ''Black Joke'' and ''Fair Rosamond'' accounted for 11 out of the squadron's take of 13 slavers.

==Fate==
A survey held on the ''Black Joke'' in 1832 stated that her timbers were rotten, and that "she is not, in our opinion, a vessel calculated fit for H.M. Service."  There were discussions about further use of ''Black Joke'', including use as a government vessel for Sierra Leone. She was due to be transferred to the governor when the rear admiral changed his mind and ordered that ''Black Joke'' be destroyed. She was burnt on 3 May 1832<ref>Correspondence between the governor, the Colonial Secretary and the Board of Admiralty held by The National Archives, reference ADM 1/4249</ref> and her stores sold.  The surveyors attached examples of her timber; all that now remains of the famous slave-chaser is an envelope filled with brown dust in [[The National Archives (United Kingdom)|The National Archives]]. In 1958 "a small quantity of the 'testings' of the timber of ''Black Joke'' were sent to Lagos for exhibition in the museum there".<ref>The National Archives reference ADM 1/74</ref>

When the Royal Navy ordered that ''Black Joke'' be burned, Peter Leonard, surgeon of [[HMS Dryad (1795)|HMS ''Dryad'']], wrote that she was the ship "which has done more towards putting an end to the vile traffic in slaves than all the ships of the station put together."<ref>Lloyd, Christopher (1968) ''The Navy and the Slave Trade: The Suppression of the African Slave Trade in the Nineteenth Century''.(Routledge). ISBN 0-7146-1894-2</ref><ref>Leonard (1833), p.171-2.</ref>

==Notes, citations, and references==

===Notes===
{{Reflist|group=Note}}

===Citations===
{{Reflist|30em}}

===References===
*[[Louis de Tousard|de Tousard, Louis]] (1809) ''American artillerist's companion; or, Elements of artillery, treating of all kinds of firearms in detail, and of the formation, object and service of the flying or horse artillery, preceded by an introductory dissertation on cannon.'' (Philadelphia: C. and A. Conrad).
*{{cite book |title=Tidewater Triumph: The Development and Worldwide Success of the Chesapeake Bay Pilot Schooner |last=Footner |first=Geoffrey |year=1998 |publisher=Mystic Seaport Museum |location=Mystic, CT |isbn=0-913372-80-3 |page=|url=https://books.google.com/books?id=M7a5mqVBo1YC&printsec=frontcover&source=gbs_navlinks_s#v=onepage&q&f=false |accessdate=2010-06-11}}
*Tinnie, Dinizulu Gene (2008) "The Slaving Brig ''Henriqueta'' and her Evil Sisters: A Case Study in the 19th-Century Slave Trade to Brazil". ''Journal of African American History'', vol. 93, no. 4, pp.&nbsp;509–531.
*{{Citation
  | last = Leonard
  | first = Peter, surgeon of the British Navy
  | title = The Western coast of Africa: Journal of an officer under Captain Owen: Records of a voyage in the ship Dryad, in 1830, 1831, and 1832
  | place = Philadelphia
  | publisher = Edward C. Mielke
  | year = 1833
  | url = https://books.google.com/books?id=5dURAAAAYAAJ&dq=%22Journal%20of%20an%20officer%20under%20Captain%20Owen%20%22&pg=PA171#v=snippet&q=fvile%20traffic%20in%20slaves&f=false
  | pages =
  | oclc = 191250092}}

{{DEFAULTSORT:Black Joke (1827)}}
[[Category:Brigs of the Royal Navy]]
[[Category:African slave trade]]
[[Category:Ships of the West Africa Squadron]]
[[Category:Ships built in Baltimore]]
[[Category:Merchant ships of Brazil]]
[[Category:Slave ships]]
[[Category:Captured ships]]
[[Category:Brigs]]
[[Category:1827 ships]]
[[Category:Liberian-American history]]
[[Category:Sierra Leonean-American history]]