{{Use dmy dates|date=July 2013}}
{{good article}}
{{Taxobox
| name = Black-tailed jackrabbit<ref name=msw3>{{MSW3 Hoffmann|page = 196}}</ref>
| status = LC 
| status_system = IUCN3.1
| status_ref = <ref name=iucn>{{IUCN2009.2|assessor=Mexican Association for Conservation and Study of Lagomorphs (AMCELA)|assessor2=Romero Malpica, F.J.|last-assessor-amp=yes|assessor3=Rangel Cordero, H.||year=2008|id= 41276|title=Lepus californicus|downloaded=1 February 2010}}</ref>
| image = Jackrabbit2 crop.JPG
| image_width = 240px
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Mammal]]ia
| ordo = [[Lagomorpha]]
| familia = [[Leporidae]]
| genus = ''[[Hare|Lepus]]''
| species = '''''L. californicus'''''
| binomial = ''Lepus californicus''
| binomial_authority = [[John Edward Gray|Gray]], 1837
| subdivision_ranks = [[Subspecies]]
| subdivision = 
*''Lepus californicus californicus''
*''Lepus californicus deserticola''
*''Lepus californicus insularis''
*''Lepus californicus madalenae''
*''Lepus californicus melanotis''
*''Lepus californicus texianus''
| range_map = Black-tailed Jackrabbit area.png
| range_map_caption = Black-tailed jackrabbit range}}

[[File:Big Ears Sitting.jpg|thumb|right|Black-tailed jackrabbit sitting.]]
[[File:Juvenile Black-tailed Jackrabbit Eating.jpg|thumb|right|Juvenile Black-tailed jackrabbit eating a carrot in the California [[Mojave desert]].]]
[[File:Scruffy Blactail Eating.jpg|thumb|right|Weathered adult Black-tailed jackrabbit eating]]
The '''black-tailed jackrabbit''' (''Lepus californicus''), also known as the '''American desert hare''', is a common [[hare]] of the [[western United States]] and [[Mexico]], where it is found at elevations from sea level up to {{convert|10000|ft|m|abbr=on}}. Reaching a length around {{convert|2|ft|cm|abbr=on}}, and a weight from {{convert|3|to|6|lb|kg|abbr=on}}, the black-tailed jackrabbit is the third-largest North American hare. Black-tailed jackrabbits occupy mixed shrub-grassland terrains. Their breeding depends on the location; it typically peaks in spring, but may continue all year round in warm climates. Young are born fully furred with eyes open; they are well camouflaged and are mobile within minutes of birth, thus females do not protect or even stay with the young except during nursing. The average [[Litter (animal)|litter]] size is around four, but may be as low as two and as high as seven in warm regions.

The black-tailed jackrabbit does not migrate or hibernate during winter and uses the same habitat of 0.4 to 1.2&nbsp;mi<sup>2</sup> (1–3&nbsp;km<sup>2</sup>) year-round. Its diet is composed of various shrubs, small trees, grasses, and [[forb]]s. Shrubs generally comprise the bulk of fall and winter diets, while grasses and forbs are used in spring and early summer, but the pattern and plant species vary with climate. The black-tailed jackrabbit is an important prey species for [[Bird of prey|raptor]]s and [[Carnivore|carnivorous mammals]], such as eagles, [[hawk]]s, [[owl]]s, [[coyote]]s, foxes, and wild cats. The rabbits host many [[ectoparasite]]s including [[fleas]], [[ticks]], [[louse|lice]], and [[mites]]; for this reason, hunters often avoid collecting them.
[[File:Black-tailed_jackrabbit.jpg|thumb|left|Typical pose when alerted]]

==Description==
Like other [[Hare|jackrabbits]], the black-tailed jackrabbit has distinctive long ears, and the long powerful rear legs characteristic of hares. Reaching a length about {{convert|2|ft|cm|abbr=on}}, and a weight from {{convert|3|to|6|lb|kg|abbr=on}}, the black-tailed jackrabbit is the third-largest North American hare, after the [[antelope jackrabbit]] and the [[white-tailed jackrabbit]]. The black-tailed jackrabbit's [[Dorsum (biology)|dorsal]] fur is [[Agouti gene|agouti]] (dark buff peppered with black), and its undersides and the insides of its legs are creamy white. The ears are black-tipped on the outer surfaces, and unpigmented inside. The [[ventral]] surface of the tail is grey to white, and the black dorsal surface of the tail continues up the spine for a few inches to form a short, black stripe.<ref name=wtr/> The females are larger than males, with no other significant differences.<ref>[http://www.nps.gov/bibe/naturescience/jackrabbit.htm Big Bend National Park Black-tailed Jackrabbit], US National Park Service</ref>

==Taxonomy and distribution==
Although 17 subspecies are recognized, this number may be excessive.<ref name=r31/> Using a cluster analysis of anatomical characters, Dixon and others found that black-tailed jackrabbit subspecies separated into two distinct groups that are geographically separated west and east of the Colorado Rocky Mountains and the Colorado River. They suggested only two infrataxa are warranted: the western subspecies ''L. c. californicus'' and the eastern subspecies ''L. c. texianus''.<ref name=r23/>

The black-tailed jackrabbit is the most widely distributed jackrabbit (''Lepus'' species) in North America. Native black-tailed jackrabbit populations occur from central [[Washington (U.S. state)|Washington]] east to [[Missouri]] and south to [[Baja California Sur]] and [[Zacatecas]]. Black-tailed jackrabbit distribution is currently expanding eastward in the [[Great Plains]] at the expense of [[white-tailed jackrabbit]].<ref name=r31/> The black-tailed jackrabbit has been successfully introduced in southern [[Florida]] and along the coastline in [[Maryland]], [[New Jersey]], and [[Virginia]].<ref name=r13/><ref name=r24/>

Distribution of subspecies occurring entirely or partially in the United
States is:<ref name=r24>Dunn, John P.; Chapman, Joseph A.; Marsh, Rex E. (1982). "Jackrabbits: Lepus californicus and allies" in Chapman, J. A.; Feldhamer, G. A. (eds.) ''Wild mammals of North America: biology, management and economics''. Baltimore, MD: The Johns Hopkins University Press. ISBN 0-8018-2353-6</ref><ref name=r42/>
<!-- Deleted image removed: [[File:Mated Pair Blacktails-2.jpg|thumb|right|Mated pair of Black-tailed Jackrabbits, California [[Mojave Desert]]]] -->

*''Lepus californicus altamirae'' ([[Edward William Nelson|Nelson]])
*''L. c. asellus'' (G. S. Miller)
*''L. c. bennettii'' ([[John Edward Gray|Gray]]) – coastal southern California to Baja California Norte
*''L. c. californicus'' ([[John Edward Gray|Gray]]) – coastal Oregon to coastal and Central Valley California
*''L. c. curti'' (E. R. Hall)
*''L. c. deserticola'' ([[Edgar Alexander Mearns|Mearns]]) – southern Idaho to Sonora
*''L. c. ememicus'' ([[Joel Asaph Allen|J. A. Allen]]) – central Arizona to Sonora
*''L. c. festinus'' ([[Edward William Nelson|Nelson]])
*''L. c. magdalenae'' ([[Edward William Nelson|Nelson]])
*''L. c. martirensis'' (J. M. Stowell)
*''L. c. melanotis'' ([[Edgar Alexander Mearns|Mearns]]) – South Dakota to Iowa, Missouri, and central Texas
*''L. c. merriamai'' ([[Edgar Alexander Mearns|Mearns]]) – south-central and southeastern Texas to Tamaulipas
*''L. c. richardsonii'' ([[John Bachman|Bachman]]) – central California
*''L. c. sheldoni'' (W. H. Burt)
*''L. c. texianus'' ([[Frederick George Waterhouse|Waterhouse]]) – southeastern Utah and southwestern Colorado to Zacatecas
*''L. c. wallawalla'' ([[Clinton Hart Merriam|Merriam]]) – eastern Washington to northeastern California and northwestern Nevada
*''L. c. xanti'' ([[Oldfield Thomas|Thomas]])

==Plant communities==
The black-tailed jackrabbit occupies plant communities with a mixture of shrubs, grasses, and [[forb]]s. Shrubland-herb mosaics are preferred over pure stands of shrubs or herbs. Black-tailed jackrabbit populations are common in [[sagebrush]] (''Artemisia'' spp.),<ref name=r60>Nydegger, Nicholas C.; Smith, Graham W. (1986). "Prey populations in relation to Artemisia vegetation types in southwestern Idaho", pp. 152–156 in McArthur, E. Durant; Welch, Bruce L. (eds). Proceedings—symposium on the biology of Artemisia and Chrysothamnus; 1984 July 9–13; Provo, UT. Gen. Tech. Rep. INT-200. Ogden, UT: U.S. Department of Agriculture, Forest Service, Intermountain Research Station</ref> [[creosotebush]] (''Larrea tridentata''),<ref name=r57/> and other [[desert]] [[shrublands]]; [[palouse]], shortgrass, and mixed-grass [[prairies]]; [[desert]] [[grassland]]; open-canopy [[chaparral]]; [[oak]] (''Quercus'' spp.),<ref name=r43/> and [[pinyon pine|pinyon]]-[[juniper]] (''Pinus-Juniperus'' spp.)<ref name=r24/> [[woodland]]s; and early [[seral]] (succeeding each other), low- to mid-elevation [[Temperate coniferous forest|coniferous forests]].<ref name=r39/> It is also common in and near [[croplands]], especially [[alfalfa]] (''Medicago sativa'') fields.<ref name=r24/>

==Major life events==
Male black-tailed jackrabbits reach sexual maturity around 7 months of age.<ref name=r54/> Females usually breed in the spring of their second year, although females born in spring or early summer may breed in their first year. Ovulation is induced by copulation.<ref name=r24/> The breeding season is variable depending upon latitude and environmental factors. In the northern part of their range in Idaho, black-tailed jackrabbits breed from February through May. In Utah, they breed from January through July,<ref name=r40/> with over 75% of females pregnant by April. The Kansas breeding season extends from January to August.<ref name=r68/> Breeding in warm climates continues nearly year-round. Two peak breeding seasons corresponding to rainfall patterns and growth of young vegetation occur in California,<ref name=r54/> Arizona,<ref name=r81/> and [[New Mexico]]. In Arizona, for example, breeding peaks during winter (January–March) rains and again during June monsoons.<ref name=r81/>

The gestation period ranges from 41 to 47 days.<ref name=r40/> More litters are born in warm climates: the number of litters born each year ranges from two per year in Idaho to seven in Arizona.<ref name=r81/> Litter sizes are largest in the northern portions of black-tailed jackrabbit's range and decrease toward the south. Average litter size has been reported at 4.9 in Idaho, 3.8 in Utah,<ref name=r40/> and 2.2 in Arizona.<ref name=r81/>

Female black-tailed jackrabbits do not prepare an elaborate nest. They give birth in shallow excavations called forms that are no more than a few centimeters deep. Females may line forms with hair prior to giving birth, but some drop litters in existing depressions on the ground with no further preparation.<ref name=r68/> Young are born fully furred with eyes open, and are mobile within minutes of birth.<ref name=r24/> Females do not protect or even stay with the young except during nursing.<ref name=r65/> Ages of weaning and dispersal are unclear since the young are well camouflaged and rarely observed in the field. Captive black-tailed jackrabbits are fully weaned by 8 weeks.<ref name=r81/> The young stay together for at least a week after leaving the form.<ref name=r24/><ref name=r65/>

==Preferred habitat==
The black-tailed jackrabbit can occupy a wide range of habitats as long as diversity in plant species exists. It requires mixed grasses, forbs, and shrubs for food, and shrubs or small trees for cover.<ref name=r48/> It prefers moderately open areas without dense [[understory]] growth and is seldom found in closed-[[canopy (biology)|canopy]] habitats. For example, in California, black-tailed jackrabbits are plentiful in open [[chamise]] (''Ademostoma fasciculatum'') and ''Ceanothus'' spp. [[chaparral]] interspersed with grasses, but does not occupy closed-canopy chaparral.<ref name=r4>Bell, M. M.; Studinski, G. H. (1972). "Habitat manipulation and its relationship to avian and small rodent populations on the Decanso District of the Cleveland National Forest". Unpublished paper on file at: U.S. Department of Agriculture, Forest Service, Intermountain Research Station, Fire Sciences Laboratory, Missoula, MT</ref> Similarly, the black-tailed jackrabbit occupies [[clearcutting|clearcuts]] and early [[seral]] coniferous forest, but not closed-canopy coniferous forest.<ref name=r39>Giusti, Gregory A.; Schmidt, Robert H.; Timm, Robert M.; [and others]. 1992. The lagomorphs: rabbits, hares, and pika. In: Silvicultural approaches to animal damage management in Pacific Northwest forests. Gen. Tech. Rep. PNW-GTR-287. Portland, OR: U.S. Department of Agriculture, Forest Service, Pacific Northwest Research Station: 289–307</ref>

Black-tailed jackrabbits do not migrate or hibernate during winter;<ref name=r24/><ref name=r39/> the same habitat is used year-round.  [[diurnality|Diurnal]] movement of 2 to 10 miles (3–16&nbsp;km) occurs from shrub cover in day to open foraging areas at night.<ref name=r24/> Home range area varies with habitat and habitat quality.<ref name=r39/> Home ranges of 0.4 to 1.2&nbsp;mi<sup>2</sup> (1–3&nbsp;km<sup>2</sup>) have been reported in big [[sagebrush]] (''Artemisia tridentata'') and black [[Sarcobatus|greasewood]] (''Sarcobatus vermiculatus'') communities of northern Utah.<ref name=r65/>

[[File:Btjackrabbit.jpg|thumb|Black-tailed jackrabbit camouflage]]

Black-tailed jackrabbits require shrubs or small [[conifer]]s for hiding, nesting, and thermal cover, and grassy areas for night feeding.<ref name=r24/><ref name=r48/> A shrub-grassland mosaic or widely spaced shrubs interspersed with herbs provides hiding cover while providing feeding opportunities. Small shrubs do not provide adequate cover.<ref name=r48/><ref name=r18/> In the [[Snake River Birds of Prey National Conservation Area|Snake River Birds of Prey Study Area]] in southwestern [[Idaho]], black-tailed jackrabbits were more frequent on sites dominated by [[big sagebrush]] or [[black greasewood]] than on sites dominated by the smaller shrubs [[winterfat]] (''Krascheninnikovia lanata'') or [[shadscale]] (''Atriplex confertifolia''). Black-tailed jackrabbits do not habitually use a [[burrow]],<ref name=r60/> although they have occasionally been observed using abandoned burrows for escape and thermal cover.<ref name=r24/><ref name=r81/><ref name=r65/>

==Food habits==
The black-tailed jackrabbit diet is composed of shrubs, small trees, grasses, and [[forb]]s. Throughout the course of a year, black-tailed jackrabbits feed on most if not all of the important plant species in a community.<ref name=r2/> Growth stage and moisture content of plants may influence selection more than species. Shrubs generally comprise the bulk of fall and winter diets, while grasses and forbs are used in spring and early summer. This pattern varies with climate: [[herbaceous plant]]s are grazed during greenup periods while the plants are in prereproductive to early reproductive stages, and shrubs are used more in dry seasons.<ref name=r2/><ref name=r82/> Shrubs are browsed throughout the year, however. Most of a jackrabbit's body water is replaced by foraging water-rich vegetation.<ref name=r81>Vorhies, Charles T.; Taylor, Walter P. (1933). "The life histories and ecology of jack rabbits, ''Lepus alleni'' and ''Lepus californicus'' ssp., in relation to grazing in Arizona". ''Technical Bulletin'' No. 49. Tucson, AZ: University of Arizona, Agricultural Experiment Station</ref><ref name=r85/> Jackrabbits require a plant's water weight to be at least five times its dry weight to meet daily water intake requirements. Therefore, black-tailed jackrabbits switch to [[phreatophyte]] (deep-rooted) shrubs when herbaceous vegetation is recovering from their foraging.<ref name=r85/>

Plant species used by black-tailed jackrabbits are well documented for desert regions. Forage use in other regions is less well known. However, black-tailed jackrabbits browse Douglas fir (''[[Pseudotsuga menziesii]]''), ponderosa pine (''[[Pinus ponderosa]]''), [[lodgepole pine]] (''P. contorta''), and western hemlock (''[[Tsuga heterophylla]]'') [[seedling]]s, and oak (''Quercus'' spp.) seedlings and sprouts.<ref name=r43/><ref name=r39/>

===Great Basin===
In [[Great Basin]], big sagebrush is a primary forage species and is used throughout the year; in southern Idaho it forms 16&ndash;21% of the
black-tailed jackrabbit summer diet. Rabbitbrush (''Chrysothamnus'' spp.), spiny hopsage (''gray spinosa''), and black greasewood are also browsed.<ref name=r2/><ref name=r28/> Four-wing saltbush (''Atriplex canescens'') is heavily used in western Nevada. In Butte County, Idaho, winterfat comprises 41% of black-tailed jackrabbits' annual diet. Grasses comprise 14% of the diet, with most grass consumption in March and April. Russian thistle (''[[Salsoda kali]]'') is an important forb diet item. Needle-and-thread grass (''Stipa comata'') and Indian ricegrass (''[[Oryzopsis hymenoides]]'') are preferred grasses. Other preferred native grasses include Sandberg bluegrass (''[[Poa secunda]]'') and bluebunch wheatgrass (''[[Pseudoroegneria spicata]]''). Where available, crested wheatgrass (''[[Agropyron desertorum]]'' and ''[[Agropyron cristatum]]'') and [[barley]] (''Hordeum vulgare'') are highly preferred. [[Drooping brome|Cheatgrass]] (''Bromus tectorum'') use is variable: it comprises 45% of the April diet on two southern Idaho sites,<ref name=r28/> but black-tailed jackrabbit on an eastern Washington site do not use it.<ref name=r11/>

===Warm desert===
In warm desert, [[mesquite]] (''Prosopis'' spp.) and creosotebush (''[[Larrea tridentata]]'') are principal browse species.<ref name=r57>Mares, M. A.; Hulse, A. C. (1977). "Patterns of some vertebrate communities in creosote bush deserts", pp. 209–226 in: Mabry, T. J.; Hunziker, J. H.; DiFeo, D. R., Jr. (eds.) ''Creosote bush: Biology and chemistry of Larrea in New World deserts''. U.S./IBP Synthesis Series 6. Stroudsburg, PA: Dowden, Hutchinson & Ross, Inc. ISBN 0879332824</ref><ref name=r81/> Broom snakeweed (''[[Gutierrezia sarothrae]]'') and ''[[Yucca]]'' spp. are also used. In honey mesquite (''[[Prosopis glandulosa]] var. glandulosa'') communities in New Mexico, the overall black-tailed jackrabbit diet was 47% shrubs, 22% grasses, and 31% forbs.<ref name=r18/> Black grama (''[[Bouteloua]]'' spp.), dropseed (''[[Sporobolus]]'' spp.), fluffgrass (''[[Dasyochloa|Erioneuron pulchellum]]''), and threeawns (''[[Aristida]]'' spp.) are the most commonly grazed grasses.<ref name=r18/><ref name=r82/> Leather croton (''[[Croton (genus)|Croton pottsii]]''), silverleaf nightshade (''[[Solanum elaeagnifolium]]''), desert marigold (''[[Baileya multiradiata]]''), wooly paperflower (''[[Psilostrophe]] tagetina''), and [[globemallow]] (''Sphaeralcea'' spp.) are important forbs, although many forb species are grazed.<ref name=r82/> ''[[Opuntia]]'' spp., [[saguaro]] (''Carnegiea gigantea''), and other cacti are used throughout the year, but are especially important in dry seasons as a source of moisture.<ref name=r70/>

[[File:Thirsty Black-tailed jackrabbit.jpg|thumb|right|300px|In the [[Mojave Desert]], a thirsty black-tailed jackrabbit senses water nearby on a human's property, and risks venturing onto the property to steal a drink of water from a dog's water bowl under a [[swamp cooler]].]]

==Predators==
The black-tailed jackrabbit is an important prey species for many raptors and carnivorous mammals. The black-tailed jackrabbit and [[Townsend's ground squirrel]] (''Spermophilus townsendii'') are the two most important prey species on the [[Snake River Birds of Prey National Conservation Area|Snake River Birds of Prey Study Area]].<ref name=r60/> Hawks preying on black-tailed jackrabbits include the [[ferruginous hawk]] (''Buteo regalis''), [[Buteo albicaudatus|white-tailed hawk]] (''Buteo albicaudatus''), [[Swainson's hawk]] (''B. swainsoni''), and [[red-tailed hawk]] (''B. jamaicensis'').<ref name=r47>Janes, Stewart W. (1985). [https://books.google.com/books?id=Uo_0h7bxkKcC&pg=PA185 "Habitat selection in raptorial birds"], pp. 159–188 in Cody, Martin L. (ed.) ''Habitat selection in birds. Academic Press Inc. ISBN 0323140130</ref> The black-tailed jackrabbit is the primary prey of Swainson's, red-tailed, and ferruginous hawks on Idaho and Utah sites. Other raptors consuming black-tailed jackrabbits include the [[great horned owl]] (''Bubo virginianus''), [[burrowing owl]] (''Athene cunicularia''), [[golden eagle]] (''Aquila chrysaetos''), and [[bald eagle]] (''Haliaeetus leucocephalus''). A significant correlation exists between golden eagle and black-tailed jackrabbit reproduction patterns.<ref name="r60"/> In Colorado and southeastern [[Wyoming]], black-tailed jackrabbits constitute 9% of nesting bald eagles' diet. Jackrabbits and [[cottontail]]s (''Sylvilagus'' spp.) combined form 9% of the diet of bald eagles wintering on national forests in Arizona and New Mexico.<ref>Grubb, Teryl G.; Kennedy, Charles E. (1982). "Bald eagle winter habitat on southwestern National Forests". Res. Pap. RM-237. Fort Collins, CO: U.S. Department of Agriculture, Forest Service, Rocky Mountain Forest and Range Experiment Station</ref>

Mammalian predators include [[coyote]] (''Canis latrans''), [[bobcat]] (''Lynx rufus''), [[lynx]] (''Lynx canadensis''), [[Dog|domestic dog]] (''Canis lupus familiaris''), [[Cat|domestic cat]] (''Felis silvestris catus''), [[red fox]] (''Vulpes vulpes''), [[Gray fox|common gray fox]] (''Urocyon cinereoargenteus''), [[American badger]] (''Taxidea taxus''), [[wolf]] (''Canis lupus''), and [[Cougar|mountain lion]] (''Puma concolor'').<ref name=r24/><ref name=r39/><ref name=r68/> In many areas, black-tailed jackrabbit is the primary item in coyote diets. It is locally and regionally important to other mammalian predators. One study found that jackrabbits made up 45% of the bobcat diet in [[Utah]] and [[Nevada]].<ref name=j1/> Another Utah&ndash;Nevada study found that jackrabbits were the fourth-most commonly consumed prey of mountain lions.<ref name=j2/>

[[Rattlesnakes]] (''Crotalus'' spp.) and [[garter snakes]] (''Thamnophis sirtalis'') prey on black-tailed jackrabbit young.<ref name=r54/><ref name=r81/> [[Raccoon]]s (''Procyon lotor'') and [[striped skunk]]s (''Mephitis mephitis'') may also capture young.<ref name=r81/>

==Parasites and disease==
The black-tailed jackrabbit plays host to many [[ectoparasite]]s including [[fleas]], [[ticks]], [[louse|lice]], and [[mites]], and many [[endoparasite]]s including [[trematodes]], [[cestodes]], [[nematodes]], and [[botfly]] (''Cuterebra'') larvae. Diseases affecting the black-tailed jackrabbit in the West are [[tularemia]], [[Eastern equine encephalitis virus|equine encephalitis]], [[brucellosis]], [[Q fever]], and [[Rocky Mountain spotted fever]]. Ticks are [[vector (molecular biology)|vector]]s for tularemia, and infected ticks have been found on jackrabbits in the West. Jackrabbits infected with tularemia die very quickly.<ref name=wtr>Whitaker, John O., Jr.; Hamilton, William J., Jr.. 1998. Mammals of the Eastern United States. Cornell University Press. 189-92. ISBN 0-8014-3475-0</ref>

The high prevalence of disease and parasites in wild jackrabbits affects human predation. Many hunters will not collect the jackrabbits they shoot, and those who do are well advised to wear gloves while handling carcasses and to cook the meat thoroughly to avoid contracting tularemia. Most hunting of jackrabbits is done for pest control or sport.<ref name=r24/>

==References==
{{USDA|article=Lepus californicus|url=http://www.fs.fed.us/database/feis/animals/mammal/leca/all.html}}
{{reflist|35em|refs=
<ref name=j1>{{cite journal|author1=Gashwiler, Jay S. |author2=Robinette, W. Leslie |author3=Morris, Owen W. | year=1960|jstor=3796754|title= Foods of bobcats in Utah and eastern Nevada|journal= Journal of Wildlife Management|volume= 24|issue=2|pages= 226–228|doi=10.2307/3796754}}</ref>

<ref name=j2>{{cite journal|author1=Robinette, W. Leslie |author2=Gashwiler, Jay S. |author3=Morris, Owen W. | year=1959|jstor=3796884 |title=Food habits of the cougar in Utah and Nevada|journal= Journal of Wildlife Management|volume= 23|issue=3|pages= 261–273|doi=10.2307/3796884}}</ref>

<ref name=r2>{{cite journal|author1=Anderson, Jay E. |author2=Shumar, Mark L. | year=1986|jstor=3899289 |title=Impacts of black-tailed jackrabbits at peak population densities on sagebrush vegetation|journal= Journal of Range Management|volume= 39|issue=2|pages= 152–155|doi=10.2307/3899289}}</ref>

<ref name=r11>{{cite journal|last1=Brandt|first1=C|last2=Rickard|first2=W|title=Alien taxa in the North American shrub-steppe four decades after cessation of livestock grazing and cultivation agriculture|journal=Biological Conservation|volume=68|pages=95–105|year=1994|doi=10.1016/0006-3207(94)90339-5}}</ref>

<ref name=r13>{{cite journal|author1=Chapman, J. A. |author2=Dixon, K. R. |author3=Lopez-Forment, W. |author4=Wilson, D. E. |year=1983|title= The New World jackrabbits and hares (genus ''Lepus'').--1. Taxonomic history and population status|journal=Acta Zoologica Fennica|volume= 174|pages= 49–51}}</ref>

<ref name=r18>{{cite journal|author=Alipayou, Daniel| year=1993|jstor=4002461 |title=Range condition influences on Chihuahuan Desert cattle and jackrabbit diets|journal= Journal of Range Management|volume= 46|issue=4|pages= 296–301|doi=10.2307/4002461|url=https://journals.uair.arizona.edu/index.php/jrm/article/view/8841}}</ref>

<ref name=r23>{{cite journal|author=Dixon, K. R. |year=1983|title= The New World jackrabbits and hares (genus ''Lepus'').--2. Numerical taxonomic analysis|journal=Acta Zoologica Fennica|volume= 174|pages= 53–56|display-authors=etal}}</ref>

<ref name=r28>{{cite journal|author1=Fagerstone, Kathleen A. |author2=Lavoie, G. Keith |last3=Griffith |first3=Richard E. Jr. | year=1980|jstor=3898292 |title=Black-tailed jackrabbit diet and density on rangeland and near agricultural crops|journal= Journal of Range Management|volume= 33|issue=3|pages= 229–233|doi=10.2307/3898292}}</ref>

<ref name=r31>{{cite journal|author=Flux, J. E. C. |year=1983|title= Introduction to taxonomic problems in hares|journal=Acta Zoologica Fennica|volume= 174|pages= 7–10}}</ref>

<ref name=r40>{{cite journal|author1=Gross, Jack E. |author2=Stoddart, L. Charles |author3=Wagner, Frederic H. | year=1974|jstor=4002912|title= Demographic analysis of a northern Utah jackrabbit population|journal= Wildlife Monographs |volume=45|pages=503–506}}</ref>

<ref name=r42>{{cite journal|author=Hall, E. Raymond |year=1951|url=http://www.gutenberg.org/etext/32426|title= A synopsis of the North American Lagomorpha|journal= University of Kansas Publications, Museum of Natural History|volume= 5|issue=10|pages= 119–202}}</ref>

<ref name=r43>{{cite journal|author1=Hall, Lillian M. |author2=George, Melvin R. |author3=McCreary, Douglas D. |author4=Adams, Theodore E. | year=1992|jstor=4002912 |title=Effects of cattle grazing on blue oak seedling damage and survival|journal= Journal of Range Management|volume= 45|issue=5|pages= 503–506|doi=10.2307/4002912}}</ref>

<ref name=r48>{{cite journal|author1=Johnson, Randal D. |author2=Anderson, Jay E. | year=1984|jstor=3898830 |title=Diets of black-tailed jack rabbits in relation to population density and vegetation|journal= Journal of Range Management|volume= 37|issue=1|pages= 79–83|doi=10.2307/3898830}}</ref>

<ref name=r54>{{cite journal|author=Lechleitner, R. R. | year=1959|jstor=1376117 |title=Sex ratio, age classes and reproduction of the black-tailed jack rabbit|journal= Journal of Mammalogy|volume= 40|issue=1|pages= 63–81|doi=10.2307/1376117}}</ref>

<ref name=r65>{{cite journal|author=Smith, Graham W. |year=1990 |title=Home range and activity patterns of black-tailed jackrabbits |journal=Great Basin Naturalist |volume=50 |issue=3 |pages=249–256 |url=https://ojs.lib.byu.edu/ojs/index.php/wnan/article/viewArticle/201 |jstor=41712598 }} [http://www.aphis.usda.gov/wildlife_damage/nwrc/publications/90pubs/90-18.pdf PDF copy] {{webarchive |url=https://web.archive.org/web/20111106160647/http://www.aphis.usda.gov/wildlife_damage/nwrc/publications/90pubs/90-18.pdf |date=6 November 2011 }}</ref>

<ref name=r68>{{cite journal|author1=Tiemeier, Otto W. |author2=Plenert, Marvin L. | year=1964|jstor=1377413 |title=A comparison of three methods for determining the age of black-tailed jackrabbits|journal= Journal of Mammalogy|volume= 45|issue=3|pages= 409–416|doi=10.2307/1377413}}</ref>

<ref name=r70>{{cite journal|author1=Turner, Raymond M|author2=Alcorn, Stanley M|author3=Olin, George|title=Mortality of Transplanted Saguaro Seedlings|jstor=1933697|journal=Ecology|volume=50|issue=5|pages=835–844|year=1969|doi=10.2307/1933697}}</ref>

<ref name=r82>{{cite journal|author1=Wansi, Tchouassi |author2=Pieper, Rex D. |author3=Beck, Reldon F. |author4=Murray, Leigh W. | year=1992|title= Botanical content of black-tailed jackrabbit diets on semidesert rangeland|journal= Great Basin Naturalist|volume= 52|issue=4|pages= 300–308|url=https://ojs.lib.byu.edu/ojs/index.php/wnan/article/viewFile/281/1375}}</ref>

<ref name=r85>{{cite journal|author1=Woffinden, Neil D. |author2=Murphy, Joseph R. | year=1989|jstor=3809619 |title=Decline of a ferruginous hawk population: a 20-year summary|journal= Journal of Wildlife Management|volume= 53|issue=4|pages= 1127–1132|doi=10.2307/3809619}}</ref>

}}

==External links==
{{wikispecies|Lepus californicus}}
*{{ITIS |id=180115 |taxon=Lepus californicus |accessdate=23 March 2006}}
{{commons category|Lepus californicus|position=left}}

{{Lagomorpha|L.}}

[[Category:Lepus]]
[[Category:Mammals of Mexico|Jackrabbit, Black-tailed]]
[[Category:Mammals of the United States|Jackrabbit, Black-tailed]]
[[Category:Fauna of the California chaparral and woodlands|Jackrabbit, Black-tailed]]
[[Category:Fauna of the Mojave Desert|Jackrabbit, Black-tailed]]
[[Category:Fauna of the Sonoran Desert|Jackrabbit, Black-tailed]]
[[Category:Fauna of the Baja California Peninsula|Jackrabbit, Black-tailed]]
[[Category:Fauna of the Colorado Desert|Jackrabbit, Black-tailed]]
[[Category:Fauna of Northern Mexico|Jackrabbit, Black-tailed]]
[[Category:Fauna of the Western United States|Jackrabbit, Black-tailed]]
[[Category:Animals described in 1837]]