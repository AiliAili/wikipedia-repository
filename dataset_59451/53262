{{Infobox NCAA Basketball Conference Tournament
| Year           = 2011
| Conference     = ACC
| Division       = I
| Gender         = Men's
| Image          = 2011acctournament.png
| ImageSize      = 150px
| Caption        = 2011 ACC Tournament logo
| Teams          = 12
| Arena          = [[Greensboro Coliseum]]
| City           = [[Greensboro, North Carolina]]
| Champions      = [[2010–11 Duke Blue Devils men's basketball team|Duke Blue Devils]]
| TitleCount     = 19th
| Coach          = [[Mike Krzyzewski]]
| CoachCount     = 13th
| MVP            = [[Nolan Smith]]
| MVPTeam        = Duke
| Attendance     =
| OneTopScorer   =
| TwoTopScorers  =
| TopScorer      =
| TopScorerTeam  =
| TopScorer2     =
| TopScorer2Team =
| Points         =
| Television     = [[ESPN]]/[[Raycom Sports]] 
}}

The '''2011 ACC Men's Basketball Tournament''', a part of the [[2010–11 NCAA Division I men's basketball season]], took place from March 10 to March 13 at the [[Greensboro Coliseum]] in [[Greensboro, North Carolina]]. 

In contrast to the upset-heavy tournament in 2010 where the 11 and 12 seeds reached the semifinals, there were only two games in the first two rounds in which the lower seed prevailed.  Some notable games included the first round game between Miami and Virginia. The Cavaliers held a 10-point lead with 42 seconds left, but the Hurricanes scored 10 straight points at the end of regulation to send the game to overtime, where Miami eventually won. In the quarterfinal game between Florida State and Virginia Tech, the Hokies' [[Erick Green]] hit a shot to put Virginia Tech up 1 with 4.7 seconds left. Derwin Kitchen then hit what appeared to be the game-winning shot for the Seminoles, but the shot was waved off after review, and Virginia Tech advanced.

For the first time in 10 years, Duke and North Carolina played each other in the championship game. The top-seeded Tar Heels had come back from double-digit deficits in their victories against Miami and Clemson. Duke started strong as well, scoring the first 8 points and taking a 14-point lead at halftime. However, Duke did not relinquish their lead in the second half, as North Carolina never got closer than 9. Duke won, 75-58, to give the Blue Devils their 19th ACC championship, the most in ACC history. This title also tied Duke coach Mike Krzyzewski with former UNC coach Dean Smith for the most ACC championships, each having won 13.

==Ticket policy==
The ACC implemented a new ticket policy in hopes to sell out more of the Greensboro Coliseum's approximately 23,000 seats.<ref name="new ticket policy">{{cite web | title=New Ticket Policy| work=newsobserver.com | url=http://www.newsobserver.com/2011/02/22/1005633/ticket-policy-changing.html# | date=February 22, 2011 | accessdate = February 24, 2011}}</ref>  In previous years, each school was allotted an equal number of ticket books for distribution. This left large numbers of tickets unsold as some schools, such as Boston College, Miami, and Florida State, do not have large numbers of fans who make the trip to Greensboro for the tournament. In response, the ACC issued more ticket books to schools who traditionally sold the majority of their ticket books.

==Seeding==
{{2010–11 ACC men's basketball standings}}
Teams are seeded based on the final regular season standings, with ties broken under an ACC policy.<ref name="seeding">{{cite web | title=ACC Basketball Tournament Seeding Procedures | work=TheACC.com | url=http://www.theacc.com/sports/m-baskbl/spec-rel/022607aaa.html | date=February 26, 2007 | accessdate = March 13, 2009}}</ref>
{| class="wikitable" style="white-space:nowrap; font-size:90%;"
|-
| colspan="7" style="text-align:center; background:#DDDDDD; font:#000000" | '''2011 ACC Men's Basketball Tournament seeds'''
|- bgcolor="#efefef"
!Seed
!School
!Conf.
!Over.
!Tiebreaker
|-
|1†‡
| [[2010–11 North Carolina Tar Heels men's basketball team|North Carolina]]
| 14–2
| 24–6
| 
|-
|2†
| [[2010–11 Duke Blue Devils men's basketball team|Duke]]
| 13–3
| 27–4
| 
|-
|3†
| [[2010–11 Florida State Seminoles men's basketball team|Florida State]]
| 11–5 
| 21–9
| 
|-
|4†
| [[2010–11 Clemson Tigers men's basketball team|Clemson]]
| 9–7 
| 20–10
| '''1–0''' vs. BC, '''1–0''' vs. VT
|-
|5
| [[2010–11 Boston College Eagles men's basketball team|Boston College]]
| 9–7
| 19–12
| '''2–0''' vs. VT, '''0–1''' vs. CLEM
|-
|6
| [[2010–11 Virginia Tech Hokies men's basketball team|Virginia Tech]]
| 9–7
| 19–10
| '''0–1''' vs. CLEM, '''0–2''' vs. BC
|-
|7
| [[2010–11 Maryland Terrapins men's basketball team|Maryland]]
| 7–9
| 18–13
| '''1–0''' vs. FSU
|-
|8
| [[Virginia Cavaliers men's basketball|Virginia]]
| 7–9
| 16–14
| '''0–1''' vs. FSU
|-
|9
| | [[Miami Hurricanes men's basketball|Miami]]
| 6–10
| 18–13
| 
|-
|10
| [[2010–11 NC State Wolfpack men's basketball team|North Carolina State]]
| 5–11
| 15–15
| '''1–0''' vs. GT
|-
|11
| [[Georgia Tech Yellow Jackets men's basketball|Georgia Tech]]
| 5–11
| 13–17
| '''0–1''' vs. NCSU
|-
|12
| [[2010-11 Wake Forest Demon Deacons men's basketball team|Wake Forest]] 
| 1–15
| 8–23
| 
|-
| colspan="6" style="text-align:left;|<small>‡ – ACC tournament No. 1 seed.<br>† – Received a bye in the conference tournament.<br>Overall records are as of the end of the regular season.</small>
|-
|}
{{clear}}

==Schedule==
{| class="wikitable" style="font-size: 95%"
|- style="text-align:center;"
!Session
!Game
!Time*
!Matchup<sup>#</sup>
!Television
!Attendance
|-
!colspan=6| First Round - Thursday, March 10
|-
|rowspan=2|<center>1
|<center>1
| noon
|#8 Virginia vs. #9 Miami
|Raycom
|23,381
|-
|<center>2
| 2:00pm
|#5 Boston College vs. #12 Wake Forest
|Raycom
|23,381
|-
|rowspan=2|<center>2
|<center>3
| 7:00pm
|#7 Maryland vs. #10 North Carolina State
|ESPN2
|23,381
|-
|<center>4
| 9:00pm
|#6 Virginia Tech vs. #11 Georgia Tech
|Raycom
|23,381
|-
!colspan=6| Quarterfinals - Friday, March 11
|-
|rowspan=2|<center>3
|<center>5
| noon
|#1 North Carolina vs. #9 Miami
|ESPN2/Raycom
|23,381
|-
|<center>6
| 2:00pm
|#4 Clemson vs. #5 Boston College
|ESPN2/Raycom
|23,381
|-
|rowspan=2|<center>4
|<center>7
| 7:00pm
|#2 Duke vs. #7 Maryland 
|ESPN2/Raycom
|23,381
|-
|<center>8
| 9:00pm
|#3 Florida State vs. #6 Virginia Tech
|ESPN2/Raycom
|23,381
|-
!colspan=6| Semifinals - Saturday, March 12
|-
|rowspan=2|<center>5
|<center>9
| 1:00pm
|#1 North Carolina vs. #4 Clemson
|ESPN/Raycom
|23,381
|-
|<center>10
| 3:00pm
|#2 Duke vs. #6 Virginia Tech
|ESPN/Raycom
|23,381
|-
!colspan=6| Championship Game - Sunday, March 13
|-
|<center>6
|<center>11
| 1:00pm
|#1 North Carolina vs.#2 Duke
|ESPN/Raycom
|
|-
|colspan=6| <small>*Game Times in [[Eastern Time Zone|ET]]. #-Rankings denote tournament seeding.<small>
|}

==Bracket==
{{4RoundBracket-Byes 
 |RD1='''First Round'''<br>Thursday, March 10, 2011
 |RD2='''Quarterfinals'''<br>Friday, March 11, 2011
 |RD3='''Semifinals'''<br>Saturday, March 12, 2011
 |RD4='''Championship Game'''<br>Sunday, March 13, 2011
 | RD1-seed03=8
 | RD1-team03=Virginia
 | RD1-score03=62
 | RD1-seed04=9
 | RD1-team04='''Miami'''
 | RD1-score04='''69*'''

 | RD1-seed07=5
 | RD1-team07='''Boston College'''
 | RD1-score07='''81'''
 | RD1-seed08=12
 | RD1-team08=Wake Forest
 | RD1-score08=67

 | RD1-seed11=7
 | RD1-team11='''Maryland'''
 | RD1-score11='''75'''
 | RD1-seed12=10
 | RD1-team12=North Carolina State
 | RD1-score12=67

 | RD1-seed15=6
 | RD1-team15='''Virginia Tech'''
 | RD1-score15='''59'''
 | RD1-seed16=11
 | RD1-team16=Georgia Tech
 | RD1-score16=43

 | RD2-seed01=1
 | RD2-team01='''#6 North Carolina'''
 | RD2-score01='''61'''
 | RD2-seed02=9
 | RD2-team02=Miami
 | RD2-score02=59

 | RD2-seed03=4
 | RD2-team03='''Clemson'''
 | RD2-score03='''70'''
 | RD2-seed04=5
 | RD2-team04=Boston College
 | RD2-score04=47

 | RD2-seed05=2
 | RD2-team05='''#5 Duke'''
 | RD2-score05='''87'''
 | RD2-seed06=7
 | RD2-team06=Maryland
 | RD2-score06=71
 | RD2-seed07=3
 | RD2-team07= Florida State
 | RD2-score07=51
 | RD2-seed08=6
 | RD2-team08='''Virginia Tech'''
 | RD2-score08='''52'''

 | RD3-seed01=1
 | RD3-team01='''#6 North Carolina'''
 | RD3-score01='''92*''' 
 | RD3-seed02=4
 | RD3-team02=Clemson
 | RD3-score02=87

 | RD3-seed03=2
 | RD3-team03='''#5 Duke'''
 | RD3-score03='''77'''
 | RD3-seed04=6
 | RD3-team04=Virginia Tech
 | RD3-score04=63

 | RD4-seed01=1
 | RD4-team01=#6 North Carolina
 | RD4-score01=58
 | RD4-seed02=2
 | RD4-team02='''#5 Duke'''
 | RD4-score02='''75'''
}}
<ref>http://www.theacc.com/championships/11-acc-mens-basketball-tournament.html</ref>

<nowiki>* Denotes Overtime Game</nowiki>

==Awards and honors==
'''Tournament [[Most Valuable Player|MVP]]'''<br>
[[Nolan Smith]], Duke

'''All-Tournament Team'''
{{Col-begin}}
{{Col-1-of-2}}
'''First Team'''<br>
Nolan Smith, Duke<br>
[[Kyle Singler]], Duke<br>
[[Harrison Barnes]], North Carolina<br>
[[Tyler Zeller]], North Carolina<br>
Demontez Stitt, Clemson
{{Col-1-of-2}}
'''Second Team'''<br>
[[Miles Plumlee]], Duke<br>
[[Seth Curry]], Duke<br>
[[John Henson (basketball)|John Henson]], North Carolina<br>
[[Kendall Marshall]], North Carolina<br>
[[Malcolm Delaney]], Virginia Tech
{{Col-end}}

== See also ==
{{Portal|ACC}}
*[[2010–11 Atlantic Coast Conference men's basketball season|2010–11 ACC Men's Basketball]]

==References==
{{reflist}}

==External links==
*[http://www.theacc.com/championships/11-acc-mens-basketball-tournament.html Official tournament website]

{{ACC Men's Basketball Tournament navbox}}
{{2011 NCAA Division I men's basketball tournament navbox}}

{{DEFAULTSORT:2011 Acc Men's Basketball Tournament}}
[[Category:ACC Men's Basketball Tournament]]