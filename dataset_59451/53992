{{Use dmy dates|date=December 2013}}
{{Use Australian English|date=December 2013}}
{{Infobox building
| name                = Adelaide Convention Centre
| native_name         = 
| native_name_lang    = 
| former_names        = 
| alternate_names     = 
| status              = Complete
| image               = Adelaide Convention Centre.jpg
| image_alt           = 
| caption             = Adelaide Convention Centre (right), on the banks of the [[River Torrens]]. This is before the pedestrian bridge was built.
| map_type            = 
| map_alt             = 
| map_caption         = 
| altitude            = 
| building_type       = 
| architectural_style = 
| structural_system   = 
| cost                = 
| ren_cost            = 
| client              = 
| owner               = 
| current_tenants     = 
| landlord            = 
| location            = 
| address             = [[North Terrace, Adelaide]]
| location_town       = 
| location_country    = Australia
| coordinates         = 
| groundbreaking_date = 
| start_date          = 
| completion_date     = 
| completed_date      = 
| opened_date         = 15 June 1987
| inauguration_date   = 
| renovation_date     = 
| demolition_date     = 
| destruction_date    = 
| height              = 
| diameter            = 
| antenna_spire       = 
| roof                = 
| top_floor           = 
| other_dimensions    = 
| floor_count         = 3
| floor_area          = 
| seating_type        = 
| seating_capacity    = 
| elevator_count      = 
| main_contractor     = 
| architect           = 
| architecture_firm   = 
| structural_engineer = 
| services_engineer   = 
| civil_engineer      = 
| other_designers     = 
| quantity_surveyor   = 
| awards              = 
| designations        =
| ren_architect       = [[Larry Oltmanns]]
| ren_firm            = '''2001 Extension:''' [[Skidmore, Owings and Merrill]]<br>'''New Extensions:''' [[Woods Bagot]] and Vx3 Architects.Strategists.Urban Designers<ref>{{cite web|title=River banking on a new Torrens icon|url=http://www.news.com.au/top-stories/river-banking-on-a-new-torrens-icon/story-e6frfkp9-1225992584190|accessdate=28 November 2012}}</ref> 
| ren_str_engineer    = 
| ren_serv_engineer   = 
| ren_civ_engineer    = 
| ren_oth_designers   = 
| ren_qty_surveyor    = 
| ren_awards          =
| url                 = {{url|http://www.adelaidecc.com.au/}}
| embedded            =
| references          = 
}}
[[File:Adelaideconventioncentre.JPG|thumb|right|Signage.]]
<!-- Deleted image removed: [[File:Adelaide railway station 1986.jpg|thumb|right|The convention centre under construction in 1986. The newest section has since been built above these train lines.]] -->
The '''Adelaide Convention Centre''' is a large [[convention centre]] on [[North Terrace, Adelaide|North Terrace]], [[Adelaide]], [[Australia]]. It was the first purpose-built convention centre to be built in Australia.<ref name="purpose">{{cite web |url=http://www.adelaidecc.com.au/about-us/our-purpose |title=Our Purpose |publisher=Adelaide Convention Centre |accessdate=9 August 2011 }}</ref>

The convention centre was constructed over part of the [[Adelaide railway station]], together with the [[Hyatt]] Regency Hotel (now the [[InterContinental Hotel]]), Exhibition Hall and an office block in the 1980s as part of the Adelaide Station and Environs Redevelopment (ASER) project.<ref name="htf">{{Cite news |url=http://www.adelaidenow.com.au/news/hyatt-to-fetch-100-million/story-e6freo8c-1111116189292 |title=Hyatt to fetch $100 million |author=Meredith Booth |accessdate=9 August 2011 |date=28 April 2008 |newspaper=The Advertiser |publisher=News Limited }}</ref> It has been rebuilt and extended upon a few times since its original construction in 1987.<ref>''Adelaide Convention Centre, 1987-1997 : decade of distinction : programme.'' Programme for dinner, held on 13 June 1997 to celebrate the 10th anniversary of the opening of the Adelaide Convention Centre. http://trove.nla.gov.au/work/17022809</ref> In 1999 an extension was planned <ref>South Australia. Parliament. Public Works Committee (1999). In Adelaide Convention Centre extension : final report; the 105th report of the Public Works Committee, October 1999. Government Printer, [Adelaide, S. Aust.]</ref> and in late 2001 it was unveiled.<ref>14 October 2003 (Property Australia - ABIX via COMTEX) The extension to the Adelaide Convention Centre has won the 2003 South Australia Property Council of Australia Rider Hunt Award. The centr ... http://trove.nla.gov.au/work/117642718</ref>  It was designed by [[Larry Oltmanns]] who was a design partner with [[Skidmore, Owings and Merrill|SOM]] at the time.<ref name="acce">[http://www.som.com/content.cfm/adelaide_convention_centre_expansion Adelaide Convention Centre Expansion] {{webarchive |url=https://web.archive.org/web/20120211172027/http://www.som.com/content.cfm/adelaide_convention_centre_expansion |date=11 February 2012 }}. Retrieved 9 August 2011.</ref>  The project won the [[Royal Australian Institute of Architects]] 2002 Awards of Merit: BHP Colourbond Steel Award, Interior Architecture and New Building.

SOM’s expansion and renovation of Adelaide’s Convention Centre reconnected historic parts of the city to the waterfront. Built on space assembled from [[air rights]] over a rail yard, the new facility shares a site with the Old and New [[Parliament House, Adelaide|South Australia State Parliament Houses]], the Adelaide Exhibition Hall, the [[Adelaide Festival Centre|Festival Centre]], and the Adelaide railway station - [[Adelaide Casino|Casino]]. </blockquote> The SOM project, was completed with Adelaide architects [[Woods Bagot]].,and conformed to the Adelaide Riverbank Master Plan.<ref name="acce"/> Its "rational cooking system", the largest of any convention centre in the world, is equipped to serve 4,000 dinners in 20 minutes. The centre's main Plenary Hall can house up to 3,500 people in full convention mode.

Looking over [[River Torrens#Torrens Lake|Torrens Lake]], the centre is home to most of Adelaide's major conventions.<ref>{{cite news | url=http://www.theage.com.au/articles/2003/05/26/1053801325821.html|title=Stand-in GG begins official duties|newspaper=The Age|date=26 May 2003|accessdate=8 August 2011}}</ref> It has also been the location of some significant commemorations of Australian icons.<ref>''The grand Bradman dinner, in honour of Sir [[Donald Bradman]]'s 90th birthday, 27th August 1998, Adelaide Convention Centre[menu].''http://trove.nla.gov.au/work/28204815</ref> [[AVCon]], an annual anime and video games convention has been held at the Adelaide Convention Centre since 2009.<ref>{{cite web |url=http://www.avcon.org.au/2011/about |title=About AVCon |accessdate=9 August 2011 }}</ref>

The most recent expansion was announced in 2011. Making the announcement, the then premier [[Mike Rann|Premier Mike Rann]] said that work would begin that year on the first stage of the $350 million expansion abutting the Morphett Street Bridge and be completed in 2014. Stage 1 would include a 4300 square metre multi-purpose concert space, meeting spaces and a 1000-seat ballroom over the railway tracks. Mr Rann said Stage 2, scheduled to be completed by mid 2017, would feature a distinctive high-tech glass "arrow" structure capable of seating 3,500 people. Woods Bagot and [[Larry Oltmanns]] of Vx3 Architects.Strategists.Urban Designers were appointed as the design team for the $350 million expansion in February 2011.<ref>{{cite web|title=Adelaide Convention Centre Announces Expansion Project Team|url=http://www.newsmaker.com.au/news/7295|accessdate=27 November 2012}}</ref><ref>{{cite news|url=http://www.abc.net.au/news/2011-06-29/convention-centre-upgrade-to-start-in-september/2775814|title=Convention Centre upgrade to start in September|publisher=ABC News|date=29 June 2011|accessdate=8 August 2011}}</ref><ref>http://www.infrastructure.sa.gov.au/major_projects/adelaide_riverbank_precinct/adelaide_convention_centre</ref><ref>{{cite web|title=Adelaide Convention Centre-Vx3 Website|url=http://www.vx3arch.com/crnt2201.html|accessdate=27 November 2012}}</ref>

==Events==
*Adelaide Vintage Expo 2013<ref>{{cite web|title=Adelaide Vintage Expo|url=http://www.adelaidevintageexpo.com.au/}}</ref>
*Tsanzsrs Annual Scientific Meeting<ref>{{cite web|title=TSANZSRS 2014 Adelaide|url=http://eventegg.com/events/tsanzsrs-2014/|publisher=Eventegg}}</ref>

==See also==
{{Portal|South Australia}}
*[[List of convention and exhibition centers]]

==References==
{{Reflist|2}}

==External links==
{{Commons category|Adelaide Convention Centre}}
* [http://www.adelaidecc.com.au/ Adelaide Convention Centre]

{{Adelaide landmarks}}
{{coord|34|55|15|S|138|35|41|E|display=title}}

[[Category:Buildings and structures in Adelaide]]
[[Category:Tourist attractions in Adelaide]]
[[Category:Convention centres in Australia]]
[[Category:Buildings and structures completed in 1987]]
[[Category:1987 establishments in Australia]]
[[Category:Event venues established in 1987]]