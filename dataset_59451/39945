{{Infobox person
|name=Étienne-Gaspard Robert
|image=Mémoires récréatifs, scientifiques et anecdotiques du physicien-aéronaute E.G. Robertson (vol. 1 frontispiece).jpg
|caption=Robertson's phantasmagoria, Paris, 1797
|other_names=Robertson
|nationality=[[Belgium|Belgian]]
|occupation=[[magic (illusion)|Stage magician]], [[physicist]] and [[balloon (aircraft)|balloonist]]
|birth_date=15 June 1763
|birth_place=[[Liège (city)|Liège]], [[Prince-Bishopric of Liège]]
|death_date={{d-da|2 July 1837|15 June 1763}}
|death_place=Paris, France
}}

'''Étienne-Gaspard Robert''' (15 June 1763 – 2 July 1837), often known by the stage name of "'''Robertson'''", was a prominent [[Belgium|Belgian]] physicist, [[magic (illusion)|stage magician]] and influential developer of [[phantasmagoria]].  He was described by [[Charles Dickens]] as "an honourable and well-educated showman".<ref name="Zeitler">Zeitler, William.  "[http://www.glassarmonica.com/armonica/history/creative/robertson.php E.G. Robertson]".  Accessed 29 July 2007.</ref>  Alongside his pioneering work on projection techniques for his shows Robert was also a physics lecturer and a keen [[balloon (aircraft)|balloonist]] at a time of great development in aviation.

==Early work==
Born in [[Liège (city)|Liège]] Robert studied at [[Leuven]] and became a professor of [[physics]] specialising in [[optics]].  He was an avid painter and intended to move to France to pursue a career in art.  He moved to Paris in the 1791 and maintained a living as a painter and draughtsman.  While there he attended lectures in [[natural science]] at the [[Collège de France]] as well as those by [[Jacques Charles]], a fellow scientist and important figure in ballooning history.<ref name="Zeitler" />  Charles would go on to become a mentor for Robert.

In 1796, during the [[French Revolution]] and three years after the declaration of war between France and Great Britain, Robert met with the French government and proposed a method of burning the invading ships of the British [[Royal Navy]].  Based on the myth of the [[Burning-glass|mirrors of Archimedes]] he wanted to employ enormous [[mirror]]s to direct intense amounts of sunlight onto the approaching vessels.  The government turned down his suggestion.<ref name="Zeitler" /><ref name="PreCinema">Burns, Paul.  "[http://www.precinemahistory.net/1750.htm The History of The Discovery of Cinematography: Chapter Six 1750–1799]".  Accessed 29 July 2007.</ref><ref name="AiC">''Adventures in Cybersound''.  "[http://pandora.nla.gov.au/pan/13071/20040303-0000/www.acmi.net.au/AIC/PHANTASMAGORIE.html Robertson's Phantasmagoria]".  Accessed 29 July 2007.</ref>

Robert experimented with various areas of physics, giving public demonstrations about his research into [[galvanism]] and optics in the 1790s and early 19th century.<ref name="AiC" />

==Phantasmagoria==
Robert attended a new form of [[illusion]] performance in 1793 in the form of a magic lantern show by [[Paul Philidor]] (then under the name of Paul de Philipsthal).  Philidor was one of the earliest known performers of such shows having adapted what he himself had seen by [[Johann Georg Schröpfer]].  With his understanding of optics, Robert realised the potential of what would become "[[phantasmagoria]]".  His further technological developments were combined with his skills in painting and showmanship, developing a [[precursors of film|pre-cinema]] horror show.<ref name="Zeitler" />

===Fantoscope===
Robert read the works of 17th-century scholar [[Athanasius Kircher]] and was particularly interested in the [[magic lantern]], an early form of [[slide projector]].  He created his own version of the device with several improvements, adding adjustable lenses and a moveable carriage system that would allow the operator to change the size of the projected image.  He also made it possible to project several different images at once using more than one painted glass slider.  The resultant display had a very ghostly effect especially when in a smoky atmosphere.  Through this the operator had the ability to manipulate images projected from an unseen location.<ref name="PrintsGeorge">''Prints George''.  "[http://www.printsgeorge.com/ArtEccles_Phantasmagoria.htm Phantasmagoria]".  Accessed 29 July 2007.</ref>  In 1799, after further refining the system, he received a patent for his "magic lantern on wheels", naming it the Fantoscope.

===Shows===
[[File:Étienne-Gaspard Robert's ghost illusion.png|thumb|Robert's ghost illusion]]
{{rquote|right|''I am only satisfied if my spectators, shivering and shuddering, raise their hands or cover their eyes out of fear of ghosts and devils dashing towards them''|Étienne-Gaspard Robert<ref name="AiC" />}}

Robert developed a phantasmagoria show based around his projection system and the use of other effects and techniques.  Robert scripted scenes that involved actors and [[ventriloquism]] alongside his projections, creating a convincing impression of the appearance of ghosts.<ref name="Heard">Heard, Mervyn.  "[http://www.grand-illusions.com/articles/lantern_of_fear/ The Lantern of Fear]".  Accessed 11 September 2013.</ref>  Robert used several projection devices in a variety of ways, including [[rear projection effect|rear projection]] and projection onto large pieces of wax-coated [[gauze]] (giving the image a more translucent appearance).<ref name="PreCinema" />  He also used smoke and mirrors to further disguise the mechanisms behind his show.  His painting skills allowed him to create accurate depictions of famous French heroes such as [[Jean-Paul Marat]], [[Voltaire]], and [[Jean-Jacques Rousseau]].<ref name="Zeitler" /><ref name="AiC" />

Robert appeared at the Pavillon de l'Echiquier on 23 January 1798 and performed his first show.<ref name="Zeitler" />  His charisma and the never-before-seen visual effects left the audience convinced that they had seen real ghosts, with many left terrified by the performance.

After being investigated by the authorities, Robert's show was shut down in Paris.  He moved to [[Bordeaux]] and continued to perform, before returning to Paris a few weeks later.  It was during this trip to Bordeaux that Robert first experience balloon flight as a passenger – an experience that would have a massive influence on his life.  On his return to Paris Robert discovered that two of his former assistants had continued the performances without him.  He refined his show, making it more elaborate and inventive and started performing in a more permanent location from 3 January 1799.<ref name="Zeitler" />  The Gothic surroundings of the crumbling Convent des Capucines near the [[Place Vendôme]] gave Robert the ideal eerie home for his show.<ref name="Heard" />

The shows began with the audience being shown [[optical illusion]]s and [[trompe-l'œil]] effects on their way to the showroom.  Inside the candlelit room the audience would be seated as audio effects emulate the sound of wind and thunder and an unseen [[glass harmonica]] plays unsettling music.  Robert would then enter the room and start a monologue about death and the afterlife.  He then began the show in earnest, creating smoky mix of [[sulfuric acid|sulphuric acid]] and [[aqua fortis]] before projecting his [[apparitional experience|ghostly apparitions]].<ref name="AiC" /><ref name="PrintsGeorge" />

The shows were performed at the Convent des Capucines for four years,<ref name="Zeitler" /> and Robert went on to take the show around the world, visiting Russia, Spain, and the United States among others.<ref name="Heard" />  During his travels he dedicated a lot of his time to ballooning.<ref name="Zeitler" />

==Balloon flights==

[[File:Assiette robertson 1789.jpg|thumb|right|Commemorative 18th century plate]]

Robert was a keen balloonist who designed and flew balloons in different countries around the world.  On 18 July 1803 in Hamburg he set an altitude record in a [[Montgolfier brothers|montgolfière]].<ref name="Zeitler" />  He spent many flights investigating [[meteorology|meteorological activity]].<ref name="NationalMuseet">National Museum of Denmark.  "[http://www.natmus.dk/saer/aandeninaturen/engelsk/1802e/1802e.htm The Soul in Nature: 1802]".  Accessed 30 July 2007.</ref>

Robert's two hydrogen-balloon flights in Hamburg and a third in St. Petersburg were claimed to be "scientific" by himself. In fact, he did numerous observations: Observations of barometer and thermometer, on shapes and altitudes of cloud formations, the behaviour of parachutes at different altitudes, the evaporation of [[Ether]], the electrical properties of different materials and the air, behavior of a magnetic needle, the boiling point of Water at great altitudes, sound propagation, influence of the high altitudes on animals ([[Pigeons]] and [[Butterflies]]), strength of solar radiation, the solar spectrum, gravity properties, chemical composition of the air and pressure of the air.

Nevertheless, close examination of the results shows, that many of them contradict with laws of physics, which were already known at the time of the flights. Prof. L.W. Gilbert discussed the results published by Robert in his [[Annalen der Physik]].<ref name=Gilbert>Gilbert, L.W. "[http://www.weltderphysik.de/intern/upload/annalen_der_physik/1804/Band_16_257.pdf" Annalen der Physik Volume 16, 1804, p.257-290]"</ref> and showed why Robert was wrong. For example, Robert claimed, that a [[spring scale]] with attached weights showed a lower weight at altitude as compared to the ground. Such an effect is existing, but only becomes apparent at altitudes in excess of 70,000 feet (20,000 metres).

In 1806 an audience of 50,000, including the royal family, gathered at [[Rosenborg Castle]] in Copenhagen to see Robert and his balloon.  Robert flew all the way to [[Roskilde]] – a remarkable feat for the time.  The event made a lasting impression on [[Hans Christian Ørsted]], an influential Danish physicist who went on to write a series of poems about the flight.<ref name="NationalMuseet" />

==Other details==
Robert officially opened the third [[Jardin de Tivoli, Paris]] on 14 May 1826. He died in Paris in 1837 and is buried at [[Père Lachaise Cemetery]].<ref name="FindAGrave">{{Find a Grave|20963|Robertson|work=Illusionist|date=25 Mar 2001|accessdate=17 Aug 2011}}</ref>

<gallery>
Image:robertson1.jpg|Robertson Gravesite, Paris, France
Image:robertson2.jpg|Detail of Robertson gravesite
Image:robertson3.jpg|Detail of Robertson gravesite
Image:robertson4.jpg|Detail of Robertson gravesite
Image:Étienne-Gaspard Robert's ghost illusion explanation.png|Robertson's ghost illusion explanation
</gallery>

==References==
{{reflist|30em}}

==Further reading==
*Sauvage, Emmanuelle.  "[http://www.cceae.umontreal.ca/IMG/pdf/CEL_0102.pdf Les fantasmagories de Robertson: entre «spectacle instructif» et mystification]" (French).
*Levie, Françoise : ''Étienne-Gaspard Robertson. La vie d'un fantasmagore'', 355 p., Le Préambule (Collection Contrechamp), 1990. ISBN 2-89133-117-6
*[http://hdl.loc.gov/loc.rbc/houdini.06148.1 Mémoires] From the [http://hdl.loc.gov/loc.rbc/houdini.20219.1  Harry Houdini Collection] in the Rare Book and Special Collection Division at the [[Library of Congress]]
* David J. Jones, (2011). 'Gothic Machine: Textualities, Pre-Cinematic Media and Film in Popular Visual Culture', 1670-1910, Cardiff: University of Wales Press ISBN 978-0708324073
{{Use dmy dates|date=April 2012}}

{{Authority control}}

{{DEFAULTSORT:Robert, Etienne-Gaspard}}
[[Category:1763 births]]
[[Category:1837 deaths]]
[[Category:Belgian physicists]]
[[Category:Belgian balloonists]]
[[Category:Professional magicians]]
[[Category:Burials at Père Lachaise Cemetery]]
[[Category:Phantasmagoria]]
[[Category:People from Liège]]
[[Category:Flight altitude record holders]]