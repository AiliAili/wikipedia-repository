[[File:William B. Curtis portrait.jpg|thumb|Bill Curtis (circa 1870)]]
[[File:HE-Buermeyer-With-Curtis.jpg|thumb|[[Harry Buermeyer]] (left) and Bill Curtis (right), c.&nbsp;1870]]
[[File:NYAC Track Team.jpg|thumb|N.Y.A.C. Track Team, Bill Curtis (middle)]]

'''William Buckingham "Father Bill" Curtis''' (January 17, 1837 – June 30, 1900) was one of the most important proponents of organized athletics in the late 1800s in America.<ref name=obit/>  Curtis had a remarkable career as a competitor, official, sports editor, organizer, and administrator.  He was known as "Father Bill" in the athletic world.  The death of Curtis at the age of 63 while climbing [[Mount Washington (New Hampshire)|Mount Washington]] brought forth an outpouring of testimonials from the sports world and recognition as a "father of American amateur athletics".<ref>Wettan, Richard G. and Willis, Joe D. "William Buckingham Curtis: The Founding Father of American Amateur Athletics, 1837-1900," Quest, 27 (Winter 1977), 28-37.</ref>

==Early life==
Curtis was born in Vermont and was a sickly child, having contracted tuberculosis at about 10 years old.<ref name="amnatbio">
{{cite book| title=  American National Biography: Supplement 2 |year=2005|author= Paul DeLoca
|pages=118–120 |publisher=Oxford University Press
|url=https://books.google.com/books?id=wZczV8ZxgL4C&pg=PA118
|isbn= 9780195222029 
|ol= 7392254M }}</ref> In 1850, his family moved to Chicago, and he soon enrolled in [[Wabash College]] where he quickly became a leader in many sports. After a couple of years, he changed enrollments to Bell's Commercial College but continued to focus on athletics. When the Civil War started, Curtis joined the Illinois Volunteers and served until the war was over.

==Career==

Curtis actively competed from the age of 17 to 43 and included championships in gymnastics, rowing, weightlifting, and sprinting. In 1853, in his first public competition, he won nine events in the games of the Chicago Caledonian Club at the age of 17.<ref name=whitney>{{cite journal|author=[[Caspar Whitney]]|title= The Sportsman's View-Point: The Death of Father Bill Curtis|journal= Outing|date=Aug 1900|volume=36|issue=5|page=557|url=https://archive.org/stream/outing36newy#page/557/mode/1up}}</ref> In 1860, Curtis and his friend [[John C. Babcock]] managed Hubert Ottignon's Metropolitan Gymnasium in Chicago.<ref>http://www.starkcenter.org/research/web/questforvictory/timeline</ref> From 1853 to 1872, Curtis did not lose a race in the 100-yard dash until he eventually lost to [[Harry Buermeyer]].<ref>New York Herald, February 18th, 1890</ref> Curtis was also a three-time national champion in the hammer throw. Curtis and Buermeyer were considered the strongest men of their time.<ref>Super Strength (Circa 1924) by Alan Calvert, Chapter 23</ref> Curtis successfully lifted over 3600 pounds in a "back" lift.

Curtis helped create several amateur clubs around the country. He founded the [[New York Athletic Club]] (N.Y.A.C.) with Buermeyer and Babcock in 1868, and he was the first President of the New York Athletic Club. In 1872, he opened the Chicago Athletic Club. Around 1880, Curtis founded the Fresh Air Club to encourage members in New York City to have outdoor exercise in rural areas.  Curtis helped found the [[Amateur Athletic Union]] in 1888, which eventually became the U.S. Olympic Committee.<ref>William Buckingham "Father Bill" Curtis: Founder of the U.S. Olympic Committee, by Lowell M. Seida (1998)</ref> After retiring from athletics, Curtis became the managing editor of New York City's sports newspaper, ''[[The Spirit of the Times]]''.

He died on June 30, 1900, during an ice-storm on [[Mount Washington (New Hampshire)|Mount Washington]] in [[New Hampshire]].<ref name=obit>{{cite news |author= |coauthors= |title=William B. Curtis, the Father of American Amateur Athletics. The Tragic End of an Existence Filled with Much That Was Good and Healthful  |url=http://query.nytimes.com/mem/archive-free/pdf?res=F50915FF3B5D12738DDDA10894DF405B808CF1D3 |quote=By the tragic death of William B. Curtis in a blinding storm on Mount Washington about a week ago, the world of amateur sport has lost one of its most commanding figures ... |newspaper=[[New York Times]] |date=July 8, 1900 |accessdate=2014-01-06 }}</ref><ref>“In Icy Fetters: Frightful Storm on Mount Washington – Raging Wind and Driving Sleet,” Among the Clouds, newspaper of Henry M. Burt Publishing,  July 7, 1900, p1.
From the primary source Among the Clouds, we learn Curtis fought a failed struggle against slipping on the icy rocks. His body was found near the Lake of the Clouds, his left temple resting on a projecting part of a large rock. “At the point of contact, there was an indentation in the temple.”</ref>  He is buried in [[Woodlawn Cemetery (Bronx, New York)|Woodlawn Cemetery]] in The Bronx, New York City.

==Legacy==
His referee services were in high demand by the Intercollegiate Athletic Association.   Throughout his life Curtis strove to purify sports of fraud and corruption.

W.&nbsp;B. Curtis was a devotee of [[speed skating]]. In the ''Spirit of The Times'', he covered developments in local and international speed-skating and compiled lists of skating records. He was selected as first president of the National Amateur Skating Association in 1884.<ref>{{cite book|title=Skating
|chapter=Chapter VII: Modern Racing
|year=1892 
|last=Tebbutt|first=C.G |authorlink=Charles Goodman Tebbutt
|publisher=Longmans, Green and Co.
|url=https://archive.org/stream/skatings00heatrich#page/270/
|pages=270–272
|ol=7132924M
|accessdate=Feb 10, 2013}}</ref>
Curtis was also an enthusiastic recreational skater and led skating tours on lakes and rivers with the Fresh Air Club. He would scout out the route by skating it himself beforehand and write reports in ''The Spirit of the Times'', under the name "The Pathfinder".<ref name="amnatbio"/>

Curtis was also a regular contributor to the outdoor sports magazine, [[Outing (magazine)|Outing]].
<ref name=whitney/>

He was inducted into the USA Track & Field Hall of Fame in 1979.<ref>http://www.usatf.org/HallOfFame/TF/showBio.asp?HOFIDs=38 William Curtis. USA Track and Field Hall of Fame.</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.usatf.org/HallOfFame/TF/showBio.asp?HOFIDs=38 USA Track and Field Hall of Fame]
* [http://www.nyac.org/ The New York Athletic Club (official website)]
* [http://aausports.org/ Amateur Athletic Union (official website)]

{{DEFAULTSORT:Curtis, William Buckingham}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:1837 births]]
[[Category:1900 deaths]]
[[Category:Deaths from hypothermia]]
[[Category:Mountaineering deaths]]
[[Category:American male sprinters]]
[[Category:American newspaper editors]]
[[Category:New York Athletic Club]]
[[Category:19th-century American journalists]]
[[Category:American male journalists]]
[[Category:Accidental deaths in New Hampshire]]
[[Category:Presidents of the Amateur Athletic Union]]
[[Category:19th-century male writers]]