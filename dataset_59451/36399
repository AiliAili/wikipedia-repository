{{good article}}
{{Infobox musical artist
| name = Los Ángeles Negros
| image = Los Ángeles Negros.jpg
| caption = Los Ángeles Negros' classic line-up. From left to right: [[Germaín de la Fuente]], Luis Ortíz, Jorge González, Mario Gutiérrez, and Miguel Ángel "Nano" Concha.
| alt = 
| background = group_or_band
| origin = [[San Carlos, Chile|San Carlos]], [[Chile]]
| genre = Balada rockmántica{{sfn|Tenorio|2000|p=365}}
| years_active = {{Start date|df=yes|1968}}–present
| label = [[EMI]] [[Odeon Records|Odeon]]
| website = {{URL|losangelesnegros.com.mx}}
| current_members = '''Principal'''{{plainlist |
* [[Germaín de la Fuente]]
* Mario Gutiérrez
* Jorge González
* Miguel Ángel "Nano" Concha
* Luis Ortiz
   }}
}}
'''Los Ángeles Negros''' ({{lang-en|The Black Angels}}) are a [[Chilean people|Chilean]] [[pop ballad]] band formed in [[San Carlos, Chile|San Carlos de Chile]] in 1968. The band's best-known line-up consisted of singer [[Germaín de la Fuente]], guitarist Mario Gutiérrez, keyboardist Jorge González, bassist Miguel Ángel "Nano" Concha, and drummer Luis Ortiz. Their music is a blend of [[bolero]]s, [[psychedelic rock|psychedelic]] [[funk]] and [[rock music]], known as ''Balada rockmántica''.

The original members of the band included three teenagers and a worker from a local school. After winning a local competition in June 1968, they recorded and released their first single, "Porque Te Quiero", which attracted the attention of Chile's [[Odeon Records]] representatives, urging two of the four members of the band to record an album with three other studio musicians. As a result, they recorded ''Porque Te Quiero'' in 1969, and several chart-topping singles across Latin America in the following years.

Following De la Fuente's departure from the group, most of the classic band members also left and established bands with similar names, including [[Germaín y sus Ángeles Negros]] and Los Ángeles de Chile. Guitarist Mario Gutiérrez continued to work with other musicians under the name of Los Ángeles Negros. Songs by Los Ángeles Negros have been covered by singers including [[Celia Cruz]], [[Raphael (singer)|Raphael]], and [[José Luis Rodríguez (singer)|José Luis Rodríguez]], while others have been [[sampling (music)|sampled]] by [[hip-hop music|hip-hop]] and [[rap music|rap]] musicians such as [[Funkdoobiest]], [[Damian Marley]], the [[Beastie Boys]], and [[Jay-Z]].

==History==

===Formation, radio competition, and the first LP (1968–1969)===
Cristián Blasser and Mario Gutiérrez, students from the Escuela Consolidada de San Carlos (now Liceo Politécnico de San Carlos), and Sergio Rojas, who worked at the school, formed a band in 1968. Upon hearing an announcement of a bands tournament organized by La Discusión radio station from [[Chillán]], they recruited [[Germaín de la Fuente]], who was well known in the local scene for his vocal abilities. Blasser and Gutiérrez played the guitar, Rojas the bass, and De la Fuente became the keyboardist and lead vocalist. Most of the band members were musically inspired by acts such as [[The Beatles]] except De la Fuente, who did not like them at all and wanted to do [[bolero]] music. As a result, they created a mixed style{{sfn|Leiva|2012}} known as the ''Balada rockmántica'' or ''Bolero-beat''.{{sfn|Tenorio|2000|p=365}}

Sergio Rojas suggested the band to be named Los Ángeles Negros (The Black Angels), in reference to another Chilean band called [[Pat Henry y Los Diablos Azules]] (Pat Henry and the Blue Devils). Although the band was initially reluctant to use a name in Spanish, they adopted Los Ángeles Negros after De la Fuente's mother said she liked it.{{sfn|Gumucio|2004}}

In June 1968 Los Ángeles Negros won La Discusión radio station's competition, in which they competed against a band named Los Cangrejo, and received the opportunity to record a single in Sello Indis,{{sfn|Chiappe|2012}} an independent label owned by pianist Raúl Lara. They recorded "Porque Te Quiero"/"Día Sin Sol", which was released by Indis as a single.{{sfn|Leiva|2012}}

After recording the songs in [[Santiago]], the band returned to San Carlos and became a quintet when drummer Federico Blasser joined them. In the meantime, their single received high airplay in some Chilean provinces, and because of De la Fuente's voice, attracted the attention of Jorge Oñate, the director of Chile's [[Odeon Records]], who asked the band to go to Santiago to record an LP for his record company. However, only De la Fuente and guitarist Mario Gutiérrez eventually went. Oñate quickly contacted three studio musicians &mdash;drummer Luis Ortiz, keyboardist Jorge González, and bassist Miguel Ángel "Nano" Concha&mdash; with which De la Fuente and Gutiérrez recorded Los Ángeles Negros' debut LP, ''[[Porque Te Quiero]]'', released in 1969.{{sfn|Leiva|2012}}

===Latin American success, and departure of Ortíz and De la Fuente (1969&ndash;1974)===
Following the release of ''Porque Te Quiero'', Gutiérrez and De la Fuente suggested the studio musicians to stay permanently in the group, a proposal which they accepted. The new band members used to be part of another Chilean group called Los Minimás, whose music was influenced by the [[psychedelic rock|psychedelic]] [[funk]], which they incorporated in their next recordings with Los Ángeles Negros. Later, in October 1969, they recorded and released their second album, ''[[Y Volveré]]''. The record contained songs which helped the band become popular throughout Chile and Latin America, including "Y Volveré" (cover version of "Emporte-moi" by [[Alain Barrière]] with new lyrics by De la Fuente), "Como Quisiera Decirte", and "Murió la Flor".{{sfn|Leiva|2012}}

Los Ángeles Negros then began a tour in Latin America, visiting countries including [[Ecuador]], [[Peru]], [[Venezuela]], and [[Argentina]]. According to Jorge Leiva from the Chilean [[National Council of Culture and the Arts]]' website ''Música Popular'', they performed at "stadiums which were full of people, with an unusual media coverage". In 1971, they moved to [[Mexico]], where their LP ''Y Volveré'' sold 700,000 copies. The band released seven new LPs between 1970 and 1974: ''Te Dejo la Ciudad Sin Mí'' (1970), ''Esta Noche La Paso Contigo'' (1971), ''La Cita'' (1971), ''El Tren Hacia El Olvido'' (1972), ''Déjenme Si Estoy Llorando'' (1973), ''Quédate en Mis Sueños'' (1973), and ''Aplaude Mi Final'' (1973).{{sfn|Leiva|2012}}

Their popularity in Chile decreased, however. Besides the fact that the country was taken control by a [[Government Junta of Chile (1973)|Government Junta]] after the [[1973 Chilean coup d'état|1973 coup d'état]], the musical scene of the time was mostly "folkloric", with [[Víctor Jara]] and [[Violeta Parra]] as some prominent figures. The band was also despectively qualified as "cebolleros" (drama queens). The internal relationship of the band was deteriorated, which concluded with the departure of Ortiz from Los Ángeles Negros, following an argument with De la Fuente in early 1973. De la Fuente lost interest in the band, and subsequently quit the group in March 1974.{{sfn|Leiva|2012}}

===''Mi Vida Como Un Carrusel'', continuing success, and return to Chile (1974&ndash;1993)===
After Ortíz and De la Fuente left the band, the remaining members continued to play under the direction of Miguel Ángel "Nano" Concha as Los Ángeles Negros. Ismael Montes replaced De la Fuente as the singer, and Luis Astudillo replaced Ortiz as the drummer. Concha's Ángeles Negros recorded and released the album ''Mi Vida Como Un Carrusel'' in 1974. Meanwhile, De la Fuente along with other Mexican musicians formed the band Germaín y sus Ángeles Negros, and settled in [[Mexico City]].{{sfn|Leiva|2012}}

Concha's Ángeles Negros played during the following years with Mexican singers Oscar Seín and Enrique Castillo, and Micky Alarcón and Guillermo Lynch from Chile, releasing the albums ''Despacito'', ''Bolerísimo'', an instrumental one (all three from 1976), ''Serenata Sin Luna'' (1977), ''Pasión y Vida'' (1978), ''Será Varón, Será Mujer'' (1979), and ''Tu Enamorado'' (1980). Drummer Luis Ortíz returned to the band in 1981, following Astudillo's departure in 1980. They achieved success with songs such as "El Enviado del Amor", "Volverás", [[José Alfredo Jiménez]]'s "Despacito", and [[Los Red Juniors]]' "Al Pasar Esa Edad". After the successful period in the early 1980s when the band performed live at least 150 times in Mexico in a year, they decided to move to this country in 1983.{{sfn|Leiva|2012}}

However, in 1982, keyboardist Jorge Gutiérrez left for De la Fuente's band, although he would occasionally play for Los Ángeles Negros as a "hired musician". Singer Eddie Martínez joined Los Ángeles Negros, while drummer Luis Astudillo returned to the band for the second time in 1986, after Ortiz moved to Canadá the previous year. From Ortíz comeback in 1981 until his new departure, the band released ''Volverás'' (1981), ''Siempre Románticos'' (1982), ''Maldito Piano''/''Locamente Mía'' (1983), ''Con Alas Nuevas'' (1984), and ''Prohibido'' (1985). The band did not release new albums until the 1990 comeback album ''El Esperado Regreso''. The next year they released ''De Aquí En Adelante'', an album "inspired by tropical music, which showed the band's difficulties finding a new musical path", according to Jorge Leiva.{{sfn|Leiva|2012}}

"Nano" Concha decided to return to Chile in 1992, and established an oldies music store in [[Santiago]]. Former members González and De la Fuente also returned to Chile, in 1993.{{sfn|Leiva|2012}}

===Gutiérrez's leadership and the reunion (1993&ndash;present)===
Guitarist Gutiérrez continued to be part of Los Ángeles Negros as the only original member, and later became its leader. Gutiérrez's Ángeles Negros recruited another singer in addition to Martínez, the Chilean Antonio Saavedra. The band plays yearly at least 50 times in Mexico, keeping the Ángeles Negros' most well-known songs in their live repertoire, and they have released two albums since 1993: ''Toda Una Vida'' (1996), and ''Metamorfosis'' (2003).{{sfn|Leiva|2012}}

Gutiérrez has been involved in several legal battles against the former members of the band and other unrelated individuals who perform as "Los Ángeles Negros" or similar names, who are for him "músicos piratas" (illegal musicians).{{sfn|La Nación|2012a}} Those individuals include singer Germaín de la Fuente, who performs since 1974 as Germaín y sus Ángeles Negros, keyboardist Jorge González with singer Micky Alarcón, who created a band called El Sonido de los Ángeles in the late 1990s, and drummer Luis Ortíz and singer Guillermo Lynch, who created the band Los Ángeles de Chile.{{sfn|Leiva|2012}}

De la Fuente, González, Concha, and Ortíz announced their reunion in February 2009 to commemorate the fortieth anniversary of ''Y Volveré'', originally released in 1969.{{sfn|El Universal|2009}} Their label [[EMI]] qualified the event as "unprecedented". Gutiérrez did not want to participate in the Ángeles Negros reunion, fearing eventual criticism.{{sfn|Maira|2009}} They played together for the first time since 1974 at the [[Teatro Caupolicán]] on 14 February 2010. The group's reunion, however, lasted until March of that year, because of "economic issues" just before a performance at Sala SCD, a concert hall in Santiago.{{sfn|Leiva|2012}}

==Legacy and recognition==
The style of Los Ángeles Negros, ''Balada rockmántica'' or ''Bolero-beat'', was immediately imitated by Chilean bands such as Los Golpes, Capablanca, [[Los Galos]], and the Peruvian band Los Pasteles Verdes. They also influenced bands such as [[Los Bukis]] from Mexico,{{sfn|Leiva|2012}} and [[Los Bunkers]] from Chile.{{sfn|Vergara|2012}} Los Ángeles Negros are regarded as a "classic example of romantic Latin music."{{sfn|Lavin|2004}}

Several songs by Los Ángeles Negros have been covered by bands and artists such as [[Los Tr3s]], [[Los Bunkers]], [[Sexual Democracia]], [[José José]], [[Raphael (singer)|Raphael]], and [[Celia Cruz]], while others have been [[sampling (music)|sampled]] by [[hip hop]] and [[rap (music)|rap]] acts such as the [[Beatnuts]] the [[Beastie Boys]], [[Jay-Z]], [[Damian Marley]], and [[Funkdoobiest]].{{sfn|Leiva|2012}}

A documentary titled ''Ángeles Negros'' directed by Chilean filmmakers Pachi Bustos and Jorge Leiva was premiered at the Hoyts and Arte Alameda cinemas in Santiago on 18 October 2007. The documentary, which was recorded in Chile, Mexico, and the [[United States]], is a "tribute" to the band, and "reviews their history".{{sfn|El Mercurio Online|2007}} The band was awarded "El Micrófono de Oro" (The Golden Microphone) by the Asociación Nacional de Locutores de México in 2012.{{sfn|La Nación|2012b}} The band's 1969 song "El Rey y Yo" was featured in 2013 videogame [[Grand Theft Auto V]], specifically in the game's radio ''East Los FM''. "['El Rey y Yo'] is the gamers' preferred song", states a ''[[La Cuarta]]'' article describing the song's inclusion in the videogame.{{sfn|La Cuarta|2013}}

==Members==
{{col-begin}}
{{col-2}}
;Current members{{sfn|Leiva|2012}}
* Mario Gutiérrez&nbsp;– guitar (1968–present)
* Luis Astudillo&nbsp;– drums, percussion (1974–1980; 1986–present)
* Eddy Martínez&nbsp;– vocals (1983–present)
* Horacio Medina&nbsp;– bass (1993–present)
* Alejandro Muñoz&nbsp;– keyboards (1993–present)
* Antonio Saavedra&nbsp;– vocals (2001–present)

; Principal line-up{{sfn|Radio Uno 91.7|2012}}
* [[Germaín de la Fuente]]&nbsp;– lead vocals (1968–1974)
* Mario Gutiérrez&nbsp;– guitar (1968–present)
* Jorge González&nbsp;– keyboards (1969–1985; 1989–1992)
* Miguel Ángel "Nano" Concha&nbsp;– bass (1969–1992)
* Luis Ortiz&nbsp;– drums, percussion (1969–1973; 1981–1986)

{{col-2}}
;Other past members{{sfn|Leiva|2012}}
* Cristián Blasser&nbsp;– keyboards (1968–1969)
* Federico Blasser&nbsp;– drums, percussion (1968–1969)
* Sergio Rojas&nbsp;– bass (1968–1969)
* Ismael Montes&nbsp;– vocals (1974–1976)
* Oscar Seín&nbsp;– vocals (1977)
* Micky Alarcón&nbsp;– vocals (1977–1981)
* Guillermo Lynch&nbsp;– vocals (1981–1983).
* Enrique Castillo&nbsp;– vocals (1981–1983).
* Gastón Galdames&nbsp;– vocals (1994–2001).
{{col-end}}

==Discography==
;Studio albums{{sfn|Leiva|2012}}
{{col-begin}}
{{col-2}}
* ''[[Porque Te Quiero]]'' (1969)
* ''[[Y Volveré]]'' (1969)
* ''Te Dejo La Ciudad Sin Mí'' (1970)
* ''Esta Noche La Paso Contigo'' (1971)
* ''La Cita'' (1971)
* ''El Tren Hacia El Olvido'' (1972)
* ''Déjenme Si Estoy Llorando'' (1973)
* ''Quédate En Mis Sueños'' (1973)
* ''Aplaude Mi Final'' (1973)
* ''Mi Vida Como Un Carrusel'' (1974)
* ''Despacito'' (1976)
* ''Bolerísimo'' (1976)
* ''Instrumental'' (1976)
{{col-2}}
* ''Serenata Sin Luna'' (1977)
* ''Pasión y Vida'' (1978)
* ''Será Varón, Será Mujer'' (1979)
* ''Tu Enamorado'' (1980)
* ''Volverás'' (1981)
* ''Siempre Románticos'' (1982)
* ''Maldito Piano''/''Locamente Mía'' (1983)
* ''Con Alas Nuevas'' (1984)
* ''Prohibido'' (1985)
* ''El Esperado Regreso'' (1990)
* ''De Aquí En Adelante'' (1991)
* ''Toda Una Vida'' (1996)
* ''Metamorfosis'' (2003)
{{col-end}}

==Citations==
{{reflist|20em}}

==Sources==
{{refbegin|colwidth=30em}}
* {{cite book|last=Avant-Mier|first=Roberto|title=Rock the Nation: Latin/o Identities and the Latin Rock Diaspora|url=https://books.google.com/books?id=fwPTlGwKw3cC|accessdate=22 August 2012|date=6 May 2010|publisher=Continuum International Publishing Group|isbn=9781441164483|ref=harv}}
* {{cite book|last=Tenorio|first=Myriam Olguín|title=Memoria para un Nuevo Siglo: Chile, Miradas a la Segunda Mitad Del Siglo XX|url=https://books.google.com/books?id=WPD5QrtjRWcC|accessdate=22 August 2012|year=2000|publisher=[[LOM Ediciones]]|isbn=9789562822220|ref=harv}}
* {{cite web|url=http://www.musicapopular.cl/3.0/index2.php?op=Artista&id=881|title=Los Ángeles Negros|last=Leiva|first=Jorge|year=2012|work=musicapopular.cl|publisher=[[National Council of Culture and the Arts]]|language=Spanish|accessdate=22 August 2012|location=Santiago, Chile|archiveurl=http://www.webcitation.org/6EjjsdrGj |archivedate=27 February 2013|deadurl=no|ref=harv}}
* {{cite news|url=http://fibra.portabledesign.cl/gruesa/nro19/pdf/Germain.pdf|title=Germain de la Fuente: Clásico, romántico, psicodélico|last=Gumucio|first=Rafael|date=April 2004|work=Revista Fibra|language=Spanish|accessdate=22 August 2012|archiveurl=http://www.webcitation.org/6EjjsdrGM|archivedate=27 February 2013|deadurl=no|ref=harv}}
* {{cite news|url=http://www.fmrecuerdos.com.ar/index.php?option=com_content&view=article&id=262:homenaje-qlos-angeles-negrosq&catid=35:reportajes&Itemid=67|title=Mario Gutierrez "Los Angeles Negros"|last=Chiappe|first=Jorge|year=2012|work=Recuerdos FM|language=Spanish|accessdate=23 August 2012|location=Fray Luis Beltrán, Santa Fe, Argentina|archiveurl=http://www.webcitation.org/6EjjsdrGb|archivedate=27 February 2013|deadurl=no|ref=harv}}
* {{cite news|url=http://www.radiounochile.cl/noticia/sin-mario-gutierrez-sera-reunion-de-los-angeles-negros/20090812/nota/860296.aspx|title=Sin Mario Gutiérrez será reunión de Los Ángeles Negros|date=12 August 2012|work=Radio Uno 91.7|publisher=[[PRISA]]|language=Spanish|accessdate=22 August 2012|archiveurl=http://www.webcitation.org/6EjjsdrG8|archivedate=27 February 2013|deadurl=no|ref={{sfnRef|Radio Uno 91.7|2012}}}}
* {{cite news|url=http://www.diarioladiscusion.cl/index.php/vidaycultura/110-espectaculos992990821/16409-los-bunkers-queremos-mostrar-el-crecimiento-que-hemos-tenido-en-estos-anos|title=Los Bunkers: "Queremos mostrar el crecimiento que hemos tenido en estos años"|date=26 August 2012|work=Diario La Discusión|language=Spanish|accessdate=29 August 2012|first=Felipe|last=Vergara|archiveurl=http://www.webcitation.org/6EjjsdrH5|archivedate=27 February 2013|deadurl=no|ref=harv}}
* {{cite news|url=http://www.lanacion.cl/los-angeles-negros-denuncian-a-impostores-que-ofrecen-conciertos-en-angol/noticias/2012-01-26/123858.html|title=Los Ángeles Negros denuncian a "impostores que ofrecen conciertos en Angol"|date=27 January 2012|work=[[La Nación (Chile)|La Nación]]|publisher=|language=Spanish|accessdate=29 August 2012|archiveurl=http://www.webcitation.org/6EjjsdrGx|deadurl=no|archivedate=27 February 2013|ref={{sfnRef|La Nación|2012a}}}}
* {{cite news|url=http://www.latercera.com/contenido/724_167383_9.shtml|title=Fundador de Los Angeles Negros: "No quiero que engañen a la gente"|date=11 August 2009|work=[[La Tercera]]|publisher=[[COPESA]]|language=Spanish|accessdate=29 August 2012|first=Manuel|last=Maira|archiveurl=http://www.webcitation.org/6EjjsdrHL|archivedate=27 February 2013|deadurl=no|ref=harv}}
* {{cite news|url=http://www.eluniversal.com.mx/notas/574928.html|title=Los Ángeles Negros planean show único y homenaje masivo |date=6 February 2009|work=[[El Universal (Mexico)|El Universal]]|publisher=El Universal Compañía Periodística Nacional|language=Spanish|accessdate=29 August 2012|archiveurl= http://www.webcitation.org/6EjjsdrGU|archivedate=27 February 2013|deadurl=no|ref={{sfnRef|El Universal|2009}}}}
* {{cite news|url=http://www.emol.com/noticias/magazine/2007/10/18/278985/los-angeles-negros-reviven-en-documental.html|title=Los Ángeles Negros reviven en documental|date=18 October 2007|work=[[El Mercurio|EMOL]]|publisher=Empresa El Mercurio S.A.P.|language=Spanish|accessdate=29 August 2012|archiveurl=http://www.webcitation.org/6EjjsdrHD|archivedate=27 February 2013|deadurl=no|ref={{sfnRef|El Mercurio Online|2007}}}}
* {{cite news|url=http://www.lanacion.cl/los-angeles-negros-premiados-en-mexico/noticias/2012-03-26/140456.html|title=Los Ángeles Negros premiados en México|date=26 March 2012|work=[[La Nación (Chile)|La Nación]]|publisher=Empresa Periodística La Nación S.A.|language=Spanish|accessdate=29 August 2012|archiveurl= http://www.webcitation.org/6EjjsdrGq|archivedate=27 February 2013|deadurl=no|ref={{sfnRef|La Nación|2012b}}}}
* {{cite news|url=http://www.highbeam.com/doc/1P2-6975579.html|title=Mi Rancho the place for Valentine's Day dance tickets|last=Lavin|first=Thea|date=12 February 2004|work=[[The Oakland Tribune]]|publisher=Alameda Newspaper Group|accessdate=30 August 2012|ref=harv}} {{subscription needed}}
* {{cite news|url=http://www.lacuarta.com/noticias/espectacular/2013/09/65-159432-9-los-angeles-negros-la-rompe-con-un-temazo-en-el-gta-v.shtml|title=Los Ángeles Negros la rompe con un temazo en el GTA V|date=24 September 2013|work=[[La Cuarta]]|publisher=[[COPESA]]|location=Santiago, Chile|language=Spanish|accessdate=4 October 2013|archiveurl= http://www.webcitation.org/6K878TvV2|archivedate=4 October 2013|deadurl=no|ref={{sfnRef|La Cuarta|2013}}}}
{{refend}}

==External links==
* [http://www.losangelesnegros.com.mx/ Gutiérrez's Ángeles Negros official website] {{es icon}}

[[Category:American boy bands]]
[[Category:Boy bands]]
[[Category:Chilean musical groups]]
[[Category:Chilean rock music groups]]
[[Category:Rock en Español music groups]]
[[Category:Musical groups established in 1968]]

{{Authority control}}
{{DEFAULTSORT:Angeles Negros}}