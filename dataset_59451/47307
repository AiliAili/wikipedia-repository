{{Infobox company
| name = Crystal Fountains
| logo = 
| logo_size =
| logo_alt =
| logo_caption = 
| logo_padding =
| image = Crown Fountain Chicago.jpg
| image_size = thumb
| image_alt =
| image_caption = The Crown Fountain at Millennium Park in Chicago
| native_name = Crystal
| native_name_lang =    <!-- Use ISO 639-1 code, e.g. "fr" for French. For multiple names in different languages, use {{lang|[code]|[name]}}. -->
| former_name = 
| type = Incorporated company
| industry = Water feature design and product manufacturing
| founded = {{start date|1967}} in [[Toronto]], [[Canada]]
| founder = Roger L’Heureux
| hq_location = Toronto
| hq_location_city = Ontario
| hq_location_country = Canada
| area_served = Global
| key_people = Paul and David L’Heureux 
| products = 
| brands = 
| services = 
| owner =               <!-- or: | owners = -->
| website =             <!-- or: | homepage = --><!-- {{URL|example.com}} -->
}}

'''Crystal Fountains Inc.''', known as Crystal,<ref name="Crystal Rebrand">{{cite web|title=Crystal Fountains changes its name to Crystal following a rebrand|url=http://www.ameinfo.com/268319.html|work=AMEinfo.com|accessdate=June 16, 2011}}</ref> is a [[water feature]] design and product manufacturing firm based in [[Toronto]], [[Ontario]], [[Canada]]. Founded in 1967, Crystal has completed thousands of projects worldwide, spread over 6 continents in over 30 countries.<ref name="REurope Article">{{cite news|title=One Thousand and One Fountains|newspaper=REurope Magazine|pages=84–85|date=August 2006}}</ref>  The company is best known for its work on the iconic [[Crown Fountain]]<ref name="Landscape Online Crown">{{cite news|last=Pringle|first=Jennifer|title=Chicago's Crown Fountain Is King Of The Park|url=http://www.landscapeonline.com/research/article/5410|newspaper=Landscape Architect and Specifier News|pages=86–94|year=2004}}</ref>  in [[Millennium Park]], [[Chicago]], [[Illinois]] and the newly renovated water feature at [[Washington Harbour]]<ref name="Georgetown Patch">{{cite web|last=Courtney|first=Shaun|title=New Washington Harbour Fountain Flowing Through October|url=http://georgetown.patch.com/groups/business-news/p/new-washington-harbour-fountain-flowing-through-october|work=Georgetown Patch|date=September 10, 2012}}</ref>  in [[Georgetown (Washington, D.C.)|Georgetown]], just outside [[Washington D.C.]]

== History ==
[[File:Eaton Centre in Toronto (2915527244).jpg|thumb|right|Main water feature in Toronto's Eaton Centre]]Crystal was founded in 1967 by Roger L’Heureux, an engineer at a Toronto-based drain company, when he collaborated on fountains for the [[Expo 67]] pavilions in [[Montreal]] with international sculptor [[Gerald Gladstone]].<ref name="Toronto Star Article">{{cite news|last=Warson|first=Albert|title=Making A Big Splash|url=http://www.thestar.com/business/small_business/2009/06/29/making_a_big_splash.html|newspaper=The Toronto Star|date=June 29, 2009|location=Small Business Section}}</ref> Through the late 1960s and 1970s, L'Heureux continued to design and install water features throughout the Toronto area, often in conjunction with Gladstone. One of Crystal's best known projects of this era was the design and implementation of the principal water feature in [[Eaton Centre|Toronto’s Eaton Centre]].<ref name="Globe and Mail Article">{{cite news|last=Pitts|first=Gordon|title=Waters For The World|newspaper=The Globe and Mail|date=September 19, 1994|location=Business Section}}</ref> Opened in 1977, and still operating today, the fountain shoots a column of water {{convert|21|m|ft|abbr=off|sp=us}} into the air every 10 minutes before catching it in a comparatively small basin 6 meters in diameter.<ref name="Toronto Star Article" />

In 1977, Crystal acquired Decorative Fountain Company (or DEFO),<ref name="Toronto Star Article" />  a producer of fountain components such as nozzles and drains. With its new manufacturing capabilities, Crystal continued to expand through the late 1970s and 1980s. During this time, Crystal specialized in [[fountains]] for malls and other indoor spaces, focusing on selling water feature components and design services to predominately Canadian customers.

L'Heureux retired in 1987 and passed the business down to his two sons, Paul and David L’Heureux,<ref name="Toronto Star Article" /> as Crystal continued to move away from fountain installation and focus more heavily on design services and product supply. This design focus also ushered in a decade of international expansion,<ref name="Toronto Star 97">{{cite news|last=Burg|first=Robert|title=Making A Big Splash Overseas|newspaper=The Toronto Star|date=March 24, 1997|location=Business Section}}</ref>  as Crystal began exporting its products and undertaking projects in foreign markets in the early 1990s. One of Crystal’s earliest overseas projects was also one of its most ambitious: The Lake Symphony Fountain in [[Kuala Lumper]]<ref name=Azure>{{cite news|last=Pulfer|first=Rachel|title=Waterworks|newspaper=Azure Magazine|pages=62–65|date=October 2006}}</ref>  contains over 380 sequenced jet effects stretched over a length of {{convert|80|m|ft|abbr=off}}. Some of these effects reach a height of more than {{convert|42|m|ft|abbr=off}}<ref name="South East Asia Building">{{cite news|title=Going With The Flow|newspaper=South East Asia Building Magazine|pages=46–49|date=April 2006}}</ref> [[File:Kuala Lumpur City Center Fountain.jpg|thumb|left|The Lake Symphony Fountain in Kuala Lumpur]]

The Lake Symphony Fountain was also the first Crystal project to feature its patented [[Sequencing fountain|Choreoswitch]], a device that allows for fast, smooth, and variable height sequencing effects up to 10 times per second. A solenoid in the device produces these effects by rapidly moving water from one side to the other.<ref name=Azure /> The Choreoswitch is now used worldwide in everything from large show features to simple [[Splash pad|play decks]].

In the 2000s, Crystal continued to expand internationally by opening a [[Dubai]] office in 2007,<ref name="RetailME Dubai Office">{{cite news|title=Crystal Fountains in Dubai|newspaper=RetailME Magazine|page=55|date=November 2007}}</ref> and completing notable projects such as Crown Fountain, the main water feature at [[Al Kout Mall]]<ref name="RetailME Magazine">{{cite news|title=Malls Splash Out|newspaper=RetailME Magazine|pages=56–57|date=January 2007}}</ref>  in Fahaheel, Kuwait, and the lotus-inspired fountain in [[Moscow|Moscow’s]] [[White Square]], Crystal’s first foray into [[Russia]].

[[File:The Washington Harbour fountain in Georgetown.jpg|thumb|right|The Washington Harbour fountain in Georgetown]]As of 2011, Crystal Fountains Inc. officially rebranded itself as Crystal to better reflect the comprehensive nature of its water feature work.<ref name="Crystal Rebrand" /> The company continues to operate as a fountain product manufacturer and supplier while designing a number of significant projects each year. Projects from this period include the World Voices fountain in the residential lobby of the world's tallest building, [[Burj Khalifa]]<ref name="World Voices AMEinfo" /> and the Aquatheatre aboard Royal Caribbean's [[Oasis of the Seas]] cruise ship.<ref name="Watershapes Oasis" /> For the World Voices feature, Crystal collaborated with international artist [[Jaume Plensa]]<ref name="World Voices design blog">{{cite web|title=One True Voice|url=http://www.designcurial.com/blog/one-true-voice/|publisher=Design Curial|author=Anna Lewis|date=March 4, 2011}}</ref>  (who had previously worked with Crystal on Crown Fountain<ref name="Crown LEDs Magazine">{{cite web|title=Chicago's stunning Crown Fountain uses LED lights and displays|url=http://ledsmagazine.com/features/2/5/3|publisher=LEDs Magazine|date=May 2005}}</ref> ) to create a water feature that doubled as a work of art. The Aquatheatre, meanwhile, had to be incorporated into all the other systems aboard the Oasis Of the Seas, resulting in the first-ever theatrical water feature installed on a cruise ship.<ref name="Watershapes Oasis" />

Crystal's most recent project was the central water feature for the redevelopment of [[Washington Harbour]]<ref name="WAN Washington Harbour">{{cite web|title=State shifters...|url=http://static.worldarchitecturenews.com/index.php?fuseaction=wanappln.projectview&upload_id=23247|publisher=World Architecture News|date=August 28, 2013}}</ref>  in [[Georgetown (Washington, D.C.)|Georgetown]]. While the historical tower in the middle of the plaza remains in its original form, the rest of the fountain has been renovated with new lights, nozzles, and show capabilities. The water feature also converts into a 12,000 sq ft. ice rink<ref name="WAN Washington Harbour" />  in the winter months, making the Harbour a popular destination year round.

== Noteworthy projects ==
Crystal has been a part of the following significant projects in a design or product supply capacity:
* [[Crown Fountain]],<ref name="Landscape Online Crown" />  Chicago, Illinois
* [[Washington Harbour]]<ref name="Georgetown Patch" />  Fountain, Georgetown, Washington D.C.
* [[Grand Park]] Fountain, Los Angeles, California
* [[Sugar Beach]]<ref name="LAM Sugar Beach">{{cite news|title=2012 ASLA Professional Awards|url=http://ca.zinio.com/reader.jsp?issue=416234417&WT.mc_id=ACQ_COM_EXPLORE_USA_082412_LandscapeArchitectureSep-12&o=ext|newspaper=Landscape Architecture Magazine|date=September 2012}}</ref>  Fountain, Toronto, Ontario
* [[White Square]] Fountain, Moscow, Russia
* World Voices Fountain,<ref name="World Voices AMEinfo">{{cite web|title=Crystal supports international artist, Jaume Plensa in creating spectacular artwork for Burj Khalifa Tower lobby|url=http://www.ameinfo.com/248056.html|work=AMEinfo.com|date=November 6, 2010}}</ref>  Dubai, United Arab Emirates
* Woodlands Waterway Square, [[The Woodlands, Texas]]
* [[Royal Palace of Venaria]] Fountain, Turin, Italy
* [[Zlote Tarasy]]<ref name="Specified Lighting Design Zlote">{{cite news|title=Paint A Water Rainbow|newspaper=Specified Lighting Design|pages=16–18|date=December 2007}}</ref>  Fountain, Warsaw, Poland
* [[Yas Island]]<ref name="Lighting Today Yas Island">{{cite news|title=The Dazzling Water And Light Fountain At The Yas Island|newspaper=Lighting Today|pages=64–67|date=March 2012}}</ref>  Welcome Pavilion Fountain, Abu Dhabi, U.A.E.
* [[MS Oasis of the Seas|Oasis Of The Seas]] Aquatheatre,<ref name="Watershapes Oasis">{{cite news|title=A Hit On The High Seas|url=http://watershapes.com/fountains/a-hit-on-the-high-seas.html|newspaper=Watershapes Magazine|author=James Garland|author2=Tom Yankelitis|author3=Ritesh Khetia|pages=61–69|date=November 2010}}</ref>  Royal Caribbean International
* [[Canada's Wonderland]] Royal Fountain,<ref name="Enlighter Wonderland">{{cite web|title=Royal Fountain By Crystal|url=http://www.enlightermagazine.com/projects/royal-fountain-crystal|work=Enlighter Magazine|date=February 16, 2012}}</ref>  Toronto, Ontario
* [[Washington Park (Cincinnati, Ohio)|Washington Park]]<ref name="Landscape ME Washington Park">{{cite web|title=Crystal Helps Create World's First "Smart-Controlled" Fountain|url=http://www.landscape-me.com/magazine-issue-articles-2013/crystal.html|work=Landscape Middle East|year=2013}}</ref>  Fountain, Cincinnati, Ohio
* [[Quartier des Spectacles|Place des Spectacles]]<ref name="Landscape Online Quartier">{{cite news|last=Mellisa|first=Riche|title=Launch of the Place des Festivals|url=http://landscapeonline.com/research/article/12622|newspaper=Landscape Architect and Specifier News|date=October 2009}}</ref>  Water Feature, Montreal, Quebec
* Dragon Water Feature<ref name="Taipei 101">{{cite book|title=Taipei 101|year=2008|publisher=Images Publishing|isbn=1864702486|page=130|editor=Georges Binder}}</ref>  at [[Taipei 101]], Taipei, Taiwan

== References ==
{{Reflist|2}}

== External links ==
* {{Official website|http://www.crystalfountains.com}}

[[Category:Articles created via the Article Wizard]]
[[Category:Companies based in Toronto]]
[[Category:Architecture firms of Canada]]
[[Category:1967 establishments in Ontario]]