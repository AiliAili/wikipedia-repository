{{for|the 1953 Ellery Queen novel|The Scarlet Letters}}
{{Other uses|Scarlet Letter (disambiguation)}}
{{lead too short|date=April 2015}}
{{Infobox book| 
| name          = The Scarlet Letter
| image         = Title page for The Scarlet Letter.jpg
| caption = Title page, first edition, 1850
| author        = [[Nathaniel Hawthorne]]
| country = United States
| language = English
 | illustrator   =
| series        =
| genre         = [[Romanticism|Romantic]], [[Historical Novel|Historical]]
| publisher     = [[Ticknor and Fields|Ticknor, Reed & Fields]]
| pub_date      = 1850
| english_pub_date =
| pages         = 
| isbn          =
| dewey        = 813.3
| preceded_by   =
| followed_by   =
}}
'''''The Scarlet Letter: A Romance''''' is an 1850 work of fiction in a historical setting, written by American author [[Nathaniel Hawthorne]]. The book is considered to be his "masterwork".<ref>{{cite news |publisher=National Public Radio (NPR) |date= March 2, 2008| <!-- Sunday. SHOW: Weekend All Things Considered --> |title=Sinner, Victim, Object, Winner {{pipe}} ANCHORS: JACKI LYDEN |url=http://www.npr.org/templates/story/story.php?storyId=87805369 }} (quote in article refers to it as his "masterwork", listen to the audio to hear it the original reference to it being his "magnum opus")</ref> Set in 17th-century [[Puritan]] [[Massachusetts Bay Colony]], during the years 1642 to 1649, it tells the story of [[Hester Prynne]], who conceives a daughter through an affair and struggles to create a new life of [[repentance]] and [[dignity]]. Throughout the book, Hawthorne explores themes of [[Legalism (theology)|legalism]], [[sin]], and [[guilt (emotion)|guilt]].

==Plot==
[[File:Hugues Merle - The Scarlet Letter - Walters 37172.jpg|thumb|In this painting, ''The Scarlet Letter'' by [[Hugues Merle]], Hester Prynne and Pearl are in the foreground and Arthur Dimmesdale and Roger Chillingworth are in the background (painting by [[Hugues Merle]], 1861).]]

In June 1642, in the Puritan town of Boston, a crowd gathers to witness the punishment of Hester Prynne, a young woman found guilty of adultery. She is required to wear a scarlet "A" ("A" standing for adulteress) on her dress to [[shame]] her. She must stand on the scaffold for three hours, to be exposed to public humiliation. As Hester approaches the scaffold, many of the women in the crowd are angered by her beauty and quiet dignity. When demanded and cajoled to name the father of her child, Hester refuses.

As Hester looks out over the crowd, she notices a small, misshapen man and recognizes him as her long-lost husband, who has been presumed lost at sea. When the husband sees Hester's shame, he asks a man in the crowd about her and is told the story of his wife's adultery. He angrily exclaims that the child's father, the partner in the adulterous act, should also be punished and vows to find the man. He chooses a new name &ndash; Roger Chillingworth &ndash; to aid him in his plan.

The Reverend John Wilson and the minister of Hester's church, Arthur Dimmesdale, question the woman, but she refuses to name her lover. After she returns to her prison cell, the jailer brings in Roger Chillingworth, a physician, to calm Hester and her child with his roots and herbs. He and Hester have an open conversation regarding their marriage and the fact that they were both in the wrong. Her lover, however, is another matter and he demands to know who it is; Hester refuses to divulge such information. He accepts this, stating that he will find out anyway, and forces her to hide that he is her husband. If she ever reveals him, he warns her, he will destroy the child's father. Hester agrees to Chillingworth's terms although she suspects she will regret it.

Following her release from prison, Hester settles in a cottage at the edge of town and earns a meager living with her needlework. She lives a quiet, sombre life with her daughter, Pearl. She is troubled by her daughter's unusual fascination by Hester's scarlet "A". As she grows older, Pearl becomes capricious and unruly. Her conduct starts rumours, and, not surprisingly, the church members suggest Pearl be taken away from Hester.

Hester, hearing rumors that she may lose Pearl, goes to speak to Governor Bellingham. With him are ministers Wilson and Dimmesdale. Hester appeals to Dimmesdale in desperation, and the minister persuades the governor to let Pearl remain in Hester's care.

Because Dimmesdale's health has begun to fail, the townspeople are happy to have Chillingworth, a newly arrived physician, take up lodgings with their beloved minister. Being in such close contact with Dimmesdale, Chillingworth begins to suspect that the minister's illness is the result of some unconfessed guilt. He applies psychological pressure to the minister because he suspects Dimmesdale to be Pearl's father. One evening, pulling the sleeping Dimmesdale's vestment aside, Chillingworth sees a symbol that represents his shame on the minister's pale chest.

Tormented by his guilty conscience, Dimmesdale goes to the square where Hester was punished years earlier. Climbing the scaffold, he admits his guilt to them but cannot find the courage to do so publicly. Hester, shocked by Dimmesdale's deterioration, decides to obtain a release from her vow of silence to her husband.

Several days later, Hester meets Dimmesdale in the forest and tells him of her husband and his desire for revenge. She convinces Dimmesdale to leave Boston in secret on a ship to Europe where they can start life anew. Renewed by this plan, the minister seems to gain new energy.
On Election Day, Dimmesdale gives what is declared to be one of his most inspired sermons. But as the procession leaves the church, Dimmesdale climbs upon the scaffold and confesses his sin, dying in Hester's arms. Later, most witnesses swear that they saw a stigma in the form of a scarlet "A" upon his chest, although some deny this statement. Chillingworth, losing his will for revenge, dies shortly thereafter and leaves Pearl a substantial inheritance.

After several years, Hester returns to her cottage and resumes wearing the scarlet letter. When she dies, she is buried near the grave of Dimmesdale, and they share a simple slate tombstone engraved with an escutcheon described as: "On a field, [[sable (heraldry)|sable]], the letter A, [[gules]]" ("On a field, black, the letter A, red").

==Major theme==
Elmer Kennedy-Andrews remarks that Hawthorne in "The Custom-house" sets the context for his story and "tells us about 'romance', which is his preferred generic term to describe ''The Scarlet Letter'', as his subtitle for the book&nbsp;– 'A Romance'&nbsp;– would indicate." In this introduction, Hawthorne describes a space between materialism and "dreaminess" that he calls "a neutral territory, somewhere between the real world and fairy-land, where the Actual and the Imaginary may meet, and each imbues itself with nature of the other".  This combination of "dreaminess" and realism gave the author space to explore major themes.{{sfnb|Kennedy-Andrews|1999|p = [https://books.google.com/books?id=dz6nB-LiqyoC&printsec=frontcover#v=onepage&q=romance&f=false 8–9]}}

===Other themes===
The experience of Hester and Dimmesdale recalls the story of [[Adam and Eve]] because, in both cases, sin results in expulsion and suffering. But it also results in knowledge &ndash; specifically, in knowledge of what it means to be immoral. For Hester, the Scarlet Letter is a physical manifestation of her sin and reminder of her painful solitude.  She contemplates casting it off to obtain her freedom from an oppressive society and a checkered past as well as the absence of God.  Because the society excludes her, she considers the possibility that many of the traditions held up by the Puritan culture are untrue and are not designed to bring her happiness.

As for Dimmesdale, the "cheating minister", his sin gives him "sympathies so intimate with the sinful brotherhood of mankind, so that his chest vibrate[s] in unison with theirs." His eloquent and powerful sermons derive from this sense of empathy.<ref name="sparknotes">{{cite web|url=http://www.sparknotes.com/lit/scarlet/ |title=The Scarlet Letter |publisher=Sparknotes |date= |accessdate=2012-08-07}}</ref> The narrative of the Reverend Arthur Dimmesdale is quite in keeping with the oldest and most fully authorized principles in [[Christian]] thought. His "Fall" is a descent from apparent grace to his own damnation; he appears to begin in purity but he ends in corruption. The subtlety is that the minister's belief is his own cheating, convincing himself at every stage of his spiritual [[pilgrimage]] that he is saved.<ref>Davidson, E.H. 1963. Dimmesdale's Fall. ''The New England Quarterly'' '''36''': 358–370</ref>

The rose bush's beauty forms a striking contrast to all that surrounds it &ndash; as later the beautifully embroidered scarlet "A" will be held out in part as an invitation to find "some sweet moral blossom" in the ensuing, tragic tale and in part as an image that "the deep heart of nature" (perhaps God) may look more kind on the errant Hester and her child than her Puritan neighbors do. Throughout the work, the nature images contrast with the stark darkness of the Puritans and their systems.<ref name="yahoo">[https://web.archive.org/web/20070308211539/http://education.yahoo.com/homework_help/cliffsnotes/the_scarlet_letter/ ''The Scarlet Letter'' by Nathaniel Hawthorne], CliffNotes from Yahoo! Education</ref>

Chillingworth's misshapen body reflects (or symbolizes) the anger in his soul, which builds as the novel progresses, similar to the way Dimmesdale's illness reveals his inner turmoil. The outward man reflects the condition of the heart; an observation thought to be inspired by the deterioration of [[Edgar Allan Poe]], whom Hawthorne "much admired".<ref name="yahoo"/>

Another theme is the extreme legalism of the Puritans and how Hester chooses not to conform to their rules and beliefs. Hester was rejected by the villagers even though she spent her life doing what she could to help the sick and the poor. Because of the social [[shunning]], she spent her life mostly in solitude, and wouldn't go to church.

As a result, she retreats into her own mind and her own thinking. Her thoughts begin to stretch and go beyond what would be considered by the Puritans as safe or even Christian. She still sees her sin, but begins to look on it differently than the villagers ever have. She begins to believe that a person's earthly sins don't necessarily condemn them. She even goes so far as to tell Dimmesdale that their sin has been paid for by their daily penance and that their sin won't keep them from getting to heaven, however, the Puritans believed that such a sin surely condemns.

But Hester had been alienated from the Puritan society, both in her physical life and spiritual life. When Dimmesdale dies, she knows she has to move on because she can no longer conform to the Puritans' strictness. Her thinking is free from religious bounds and she has established her own different moral standards and beliefs.<ref name="sparknotes"/>

==Publication history==
[[File:Hester Prynne.jpg|thumb|Hester Prynne at the [[stocks]], an engraved illustration from an 1878 edition]]

It was long thought that Hawthorne originally planned ''The Scarlet Letter'' to be a shorter [[Novella|novelette]] which was part of a collection to be named ''Old Time Legends'' and that his publisher, [[James Thomas Fields]], convinced him to expand the work to a full-length novel.<ref>Charvat, William. ''Literary Publishing in America: 1790–1850''.  Amherst, MA: The [[University of Massachusetts Press]], 1993 (first published 1959): 56. ISBN 0-87023-801-9</ref> This is not true: Fields persuaded Hawthorne to publish ''The Scarlet Letter'' alone (along with the earlier-completed "Custom House" essay) but he had nothing to do with the length of the story.<ref>Parker, Hershel. "The Germ Theory of ''The Scarlet Letter''," ''Hawthorne Society Newsletter'' 11 (Spring 1985) 11-13.</ref> Hawthorne's wife [[Sophia Hawthorne|Sophia]] later challenged Fields' claims a little inexactly: "he has made the absurd boast that ''he'' was the sole cause of the Scarlet Letter being published!" She noted that her husband's friend [[Edwin Percy Whipple]], a critic, approached Fields to consider its publication.<ref>[[Brenda Wineapple|Wineapple, Brenda]]. ''Hawthorne: A Life''. Random House: New York, 2003: 209–210. ISBN 0-8129-7291-0.</ref> The manuscript was written at the Peter Edgerley House in [[Salem, Massachusetts]], still standing as a private residence at 14 Mall Street. It was the last Salem home where the Hawthorne family lived.<ref>Wright, John Hardy. ''Hawthorne's Haunts in New England''. Charleston, SC: The History Press, 2008: 47. ISBN 978-1-59629-425-7.</ref>

''The Scarlet Letter'' was first published in the spring of 1850 by Ticknor & Fields, beginning Hawthorne's most lucrative period.<ref name=McFarland136>McFarland, Philip. ''Hawthorne in Concord''. New York: Grove Press, 2004: 136. ISBN 0-8021-1776-7</ref> When he delivered the final pages to Fields in February 1850, Hawthorne said that "some portions of the book are powerfully written" but doubted it would be popular.<ref>Miller, Edwin Haviland. ''Salem is my Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 299. ISBN 0-87745-332-2</ref> In fact, the book was an instant best-seller, though, over fourteen years, it brought its author only $1,500.<ref name=McFarland136/> Its initial publication brought wide protest from natives of Salem, who did not approve of how Hawthorne had depicted them in his introduction "The Custom-House". A 2,500-copy second edition included a preface by Hawthorne dated March 30, 1850, that stated he had decided to reprint his Introduction "without the change of a word... The only remarkable features of the sketch are its frank and genuine good-humor&nbsp;... As to enmity, or ill-feeling of any kind, personal or political, he utterly disclaims such motives".<ref>Miller, Edwin Haviland. ''Salem is my Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 301. ISBN 0-87745-332-2</ref>

''The Scarlet Letter'' was also one of the first mass-produced books in America. In the mid-nineteenth century, bookbinders of home-grown literature typically hand-made their books and sold them in small quantities.  The first mechanized printing of ''The Scarlet Letter'', 2,500 volumes, sold out within ten days,<ref name=McFarland136/> and was widely read and discussed to an extent not much experienced in the young country up until that time. Copies of the first edition are often sought by collectors as rare books, and may fetch up to around $18,000 [[United States dollar|USD]].

==Critical response==
On its publication, critic [[Evert Augustus Duyckinck]], a friend of Hawthorne's, said he preferred the author's [[Washington Irving]]-like tales. Another friend, critic [[Edwin Percy Whipple]], objected to the novel's "morbid intensity" with dense psychological details, writing that the book "is therefore apt to become, like Hawthorne, too painfully anatomical in his exhibition of them".<ref>Miller, Edwin Haviland. ''Salem is my Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 301–302. ISBN 0-87745-332-2</ref> English writer [[George Eliot]] called ''The Scarlet Letter'', along with [[Henry Wadsworth Longfellow]]'s 1855 book ''[[The Song of Hiawatha]]'', the "two most indigenous and masterly productions in American literature".<ref>Davidson, Mashall B. ''The American Heritage History of the Writers' America''. New York: American Heritage Publishing Company, Inc., 1973: 162. ISBN 0-07-015435-X</ref> Most literary critics praised the book but religious leaders took issue with the novel's subject matter.<ref>Schreiner, Samuel A., Jr. ''The Concord Quartet: Alcott, Emerson, Hawthorne, Thoreau, and the Friendship That Freed the American Mind''. Hoboken, NJ: John Wiley & Sons, Inc., 2006: 158. ISBN 978-0-471-64663-1</ref> [[Orestes Brownson]] complained that Hawthorne did not understand Christianity, confession, and remorse.<ref>Crowley, J. Donald, and Orestes Brownson. Chapter 50: [Orestes Brownson], From A Review In Brownson's Quarterly Review." Nathaniel Hawthorne (0-415-15930-X) (1997): 175&ndash;179. Literary Reference Center Plus. Web. 11 Oct. 2013.</ref> A review in ''[[The Church Review and Ecclesiastical Register]]'' concluded the author "perpetrates bad morals."<ref>Wineapple, Brenda. ''Hawthorne: A Life''. Random House: New York, 2003: 217. ISBN 0-8129-7291-0.</ref>

On the other hand, 20th-century writer [[D. H. Lawrence]] said that there could not be a more perfect work of the American imagination than ''The Scarlet Letter''.<ref name="ReferenceA">Miller, Edwin Haviland. ''Salem is my Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 284. ISBN 0-87745-332-2</ref> [[Henry James]] once said of the novel, "It is beautiful, admirable, extraordinary; it has in the highest degree that merit which I have spoken of as the mark of Hawthorne's best things—an indefinable purity and lightness of conception...One can often return to it; it supports familiarity and has the inexhaustible [https://www.youtube.com/watch?v=Kas6akz1jWU charm] and mystery of great works of art."<ref name="ReferenceA"/><ref>{{cite book |title= Hawthorne |last= James |first= Henry |year= 1901 |publisher= Harper |pages=108, 116 |url= https://books.google.com/books?id=Nj_bTCepzHQC&pg=PA108&dq=%22it+has+in+the+highest+degree+that+merit%22 }}</ref>

==Allusions==
The following are historical and Biblical references that appear in ''The Scarlet Letter.''
* [[Anne Hutchinson]], mentioned in Chapter 1, The Prison Door, was a religious dissenter (1591–1643). In the 1630s she was excommunicated by the [[Puritans]] and exiled from [[Boston]] and moved to [[Rhode Island]].<ref name="yahoo"/>
* [[Ann Hibbins]], who historically was executed for witchcraft in Boston in 1656, is depicted in ''The Scarlet Letter'' as a witch who tries to tempt Prynne to the practice of witchcraft.<ref name= Schwab>Schwab, Gabriele. ''The mirror and the killer-queen: otherness in literary language.'' Indiana University Press. 1996. Pg. 120.</ref><ref name= Hunter>Hunter, Dianne, ''Seduction and theory: readings of gender, representation, and rhetoric.'' University of Illinois Press. 1989.  Pgs. 186-187</ref>
* [[Richard Bellingham]], who historically was the governor of Massachusetts and deputy governor at the time of Hibbins's execution, was depicted in ''The Scarlet Letter'' as the brother of Ann Hibbins.
* [[Martin Luther]] (1483–1545) was a leader of the [[Protestant Reformation]] in Germany.
* Sir [[Thomas Overbury]] and Dr. Forman were the subjects of an adultery scandal in 1615 in England. Dr. Forman was charged with trying to poison his adulterous wife and her lover. Overbury was a friend of the lover and was perhaps poisoned.
* [[John Winthrop]] (1588–1649), second governor of the [[Massachusetts Bay Colony]].
* [[King's Chapel Burying Ground]], mentioned in the final paragraph, exists; the [[Elizabeth Pain]] gravestone is traditionally considered an inspiration for the protagonists' grave.
* The story of King [[David]] and [[Bathsheba]] is depicted in the tapestry in Mr. Dimmesdale's room (chapter 9). (See [http://www.biblegateway.com/passage/?search=2%20samuel%2011-12&version=ESV II Samuel 11-12] for the Biblical story.)
* [[John Eliot (missionary)|John Eliot]] (c. 1604–1690) was a Puritan missionary to the American Indians whom some called “the apostle to the Indians." He is referred to as "the Apostle Eliot" at the beginning of Chapter 16, A Forest Walk, whom Dimmesdale has gone to visit.

==Adaptations and influence==
{{Main article|The Scarlet Letter in popular culture}}
''The Scarlet Letter'' has inspired numerous film, television, and stage adaptations, and plot elements have influenced several novels, musical works, and screen productions.

==Audiobook==
Definitive unabridged edition narrated by Magda Allani (Slow Burn Publications, 2016) ISBN 9781908671110.<ref>[http://www.audible.com/pd/Classics/The-Scarlet-Letter-Audiobook/B01LW7N758/ref=a_search_c4_1_16_srTtl?qid=1474366050&sr=1-16]</ref>

==See also==
{{portal|Novels}}
*[[Boston in fiction]]
*[[Colonial history of the United States]]
*[[Illegitimacy in fiction]]
*[[Whore of Babylon]]
*[[Angel and Apostle]], a possible sequel or companion novel

==References==

===Notes===
{{reflist|30em}}

===Bibliography===
{{refbegin}}
* Boonyaprasop, Marina. ''Hawthorne’s Wilderness: Nature and Puritanism in Hawthorne’s The Scarlet Letter and “Young Goodman Brown'' (Anchor Academic Publishing, 2013).
* Brodhead, Richard H. ''Hawthorne, Melville, and the Novel''. Chicago and London: The University of Chicago Press, 1973.
* Brown, Gillian. "'Hawthorne, Inheritance, and Women's Property", ''Studies in the Novel'' 23.1 (Spring 1991): 107-18.
* Cañadas, Ivan. "A New Source for the Title and Some Themes in ''The Scarlet Letter''". ''Nathaniel Hawthorne Review'' 32.1 (Spring 2006): 43–51.
* {{cite book |last = Kennedy-Andrews  |first =Elmer  |year = 1999 |title = Nathaniel Hawthorne, the Scarlet Letter |publisher = Columbia University Press| series = Columbia Critical Guides|location = |url=https://books.google.com/books?id=dz6nB-LiqyoC |isbn = 9780231121903|ref = harv}}
* Korobkin, Laura Haft. "The Scarlet Letter of the Law: Hawthorne and Criminal Justice". ''Novel: a Forum on Fiction'' 30.2 (Winter 1997): 193–217.
* Garrtner, Matthew. "''The Scarlet Letter'' and the Book of Esther: Scriptural Letter and Narrative Life". ''Studies in American Fiction'' 23.2 (Fall 1995): 131-51.
* Newberry, Frederick. ''Tradition and Disinheritance in ''The Scarlet Letter''". ''ESQ: A journal of the American Renaissance'' 23 (1977), 1–26; repr. in: ''The Scarlet Letter''. W. W. Norton, 1988: pp. 231-48.
* Reid, Alfred S. ''Sir Thomas Overbury's Vision (1616) and Other English Sources of Nathaniel Hawthorne's 'The Scarlet Letter''. Gainesville, FL: Scholar's Facsimiles and Reprints, 1957.
* Reid, Bethany. "Narrative of the Captivity and Redemption of Roger Prynne: Rereading ''The Scarlet Letter''". ''Studies in the Novel'' 33.3 (Fall 2001): 247-67.
* Ryskamp, Charles. "The New England Sources of ''The Scarlet Letter''". ''American Literature'' 31 (1959): 257–72; repr. in: "The Scarlet Letter", 3rd edn. Norton, 1988: 191–204.
* Savoy, Eric. "'Filial Duty': Reading the Patriarchal Body in 'The Custom House'". ''Studies in the Novel'' 25.4 (Winter 1993): 397–427.
* Sohn, Jeonghee. ''Rereading Hawthorne's Romance: The Problematics of Happy Endings''. American Studies Monograph Series, 26. Seoul: American Studies Institute, Seoul National University, 2001; 2002.
* Stewart, Randall (Ed.) ''The American Notebooks of Nathaniel Hawthorne: Based upon the original Manuscripts in the Piermont Morgan Library''. New Haven: Yale University Press, 1932.
* Waggoner, Hyatt H. ''Hawthorne: A critical study'', 3rd edn. Cambridge, MA: Belknap Press of Harvard University Press, 1971.
{{refend}}

==External links==
{{wikisource|The Scarlet Letter|''The Scarlet Letter''}}
{{Commons category|The Scarlet Letter}}
*[http://www.hawthorneinsalem.org/Literature/Hawthorne&Women/ScarletLetter/Introduction.html Hawthorne in Salem Website Page on Hester and Pearl in ''The Scarlet Letter'']
{{Gutenberg|no=33|name=The Scarlet Letter}}
*[[s:Studies in Classic American Literature/Chapter 7|D. H. Lawrence - Studies in Classic American Literature - Nathaniel Hawthorne and ''The Scarlet Letter'']]
* {{librivox book | title=The Scarlet Letter | author=Nathaniel Hawthorne}}
*[http://www.bookscritics.com/audiobooks/the-scarlet-letter.html ''The Scarlet Letter'' Review]
* [https://www.youtube.com/playlist?list=PLTg5HNsMegM2PBJsmdaxLdNePWjS-GE4i Excerpts from Opera "The Scarlet Letter" by [[Fredric Kroll]]]

{{Nathaniel Hawthorne}}
{{The Scarlet Letter}}

{{Authority control}}

{{DEFAULTSORT:Scarlet Letter, The}}
[[Category:The Scarlet Letter| ]]
[[Category:American novels adapted into films]]
[[Category:1850 novels]]
[[Category:Adultery in novels]]
[[Category:Novels adapted into television programs]]
[[Category:Novels by Nathaniel Hawthorne]]
[[Category:Novels set in Boston]]
[[Category:Novels set in the American colonial era]]
[[Category:19th-century American novels]]
[[Category:1640s in fiction]]
[[Category:Novels set in the 17th century]]
[[Category:American novels adapted into plays]]
[[Category:Novels adapted into operas]]