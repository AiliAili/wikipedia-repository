{{Infobox journal
| title = Annals of Surgical Oncology
| cover = [[File:Annals of Surgical Oncology cover.jpg]]
| caption = 
| editor = [[Charles M. Balch]]
| discipline = [[Surgery]], [[oncology]]
| former_names = 
| abbreviation = Ann. Surg. Oncol.
| publisher = [[Springer Science+Business Media]] on behalf of the [[Society of Surgical Oncology]]
| country = 
| frequency = 13/year
| history = 1994-present
| openaccess = 
| license = 
| impact = 3.930
| impact-year = 2014
| website = http://www.annsurgoncol.org/index.html 
| link1 = http://www.annsurgoncol.org/Journals/Current_Issue.html
| link1-name = Online access
| link2 = http://www.annsurgoncol.org/Journals/Past_issues.html
| link2-name = Online archive
| link3 = http://www.springer.com/medicine/oncology/journal/10434
| link3-name = Journal page at publisher's website
| JSTOR = 
| OCLC = 397372574
| LCCN = 
| CODEN = 
| ISSN = 1068-9265
| eISSN = 1534-4681
}}
'''''Annals of Surgical Oncology''''' is a [[peer-reviewed]] [[medical journal]] published by [[Springer Science+Business Media]] on behalf of the [[Society of Surgical Oncology]]. It is an official journal of the [[American Society of Breast Surgeons]] and affiliated with the [[Japanese Society of Gastroenterological Surgery]], [[Federacion Latinoamericana de Cirugia]], and [[Sociedad Mexicana de Oncologia]].<ref>{{cite web |url=http://www.springer.com/medicine/oncology/journal/10434 |title=Annals of Surgical Oncology |publisher=Springer Science+Business Media |accessdate=2014-07-18}}</ref> The [[editor-in-chief]] is Charles M. Balch ([[MD Anderson Cancer Center]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Index Medicus]]/[[MEDLINE]]/[[PubMed]],<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9420840 |title=Annals of Surgical Oncology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-07-18}}</ref> [[Current Contents]]/Clinical Medicine,<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-07-18}}</ref> [[Excerpta Medica]], and the [[Science Citation Index]].<ref name=ISI /> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.930.<ref name=WoS>{{cite book |year=2015 |chapter=Annals of Surgical Oncology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.annsurgoncol.org/index.html}}
* [http://www.surgonc.org/ Society of Surgical Oncology]

[[Category:Oncology journals]]
[[Category:Publications established in 1994]]
[[Category:Surgical oncology]]
[[Category:Surgery journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]