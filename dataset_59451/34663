{{Good article}}
{{DISPLAYTITLE:33 (''Battlestar Galactica'')}}{{Infobox television episode
| title        = 33
| series       = [[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]
| season       = 1
| episode      = 1
| writer       = [[Ronald D. Moore]]
| director     = [[Michael Rymer]]
| teleplay     =
| story        =
| producer     =
| music        =
| photographer = [[Stephen McNutt]]
| editor       =
| production   =
| airdate      = [[United Kingdom|UK]]: {{start date|2004|10|18}}<br/>[[United States|US]]: {{start date|2005|01|14}}
| length       = 45 minutes
| guests       =
* [[Michael Hogan (Canadian actor)|Michael Hogan]] as [[Saul Tigh]]
* [[Aaron Douglas (actor)|Aaron Douglas]] as [[Galen Tyrol]]
* [[Tahmoh Penikett]] as [[Karl Agathon]]
* [[Kandyse McClure]] as [[Anastasia Dualla]]
* [[Paul Campbell (Canadian actor)|Paul Campbell]] as [[Billy Keikeya]]
* [[Alessandro Juliani]] as [[Felix Gaeta]]
* [[Samuel Witwer|Sam Witwer]] as [[Alex Quartararo|Lt. Crashdown]]
* [[Alonso Oyarzun]] as Brad Socinus
* [[Nicki Clyne]] as [[Cally Henderson Tyrol|Cally Henderson]]
| RPrev         = ''[[Battlestar Galactica (TV miniseries)|Battlestar Galactica]]''
| next         = [[Water (Battlestar Galactica)|Water]]
| episode_list = [[Battlestar Galactica (season 1)|List of season 1 episodes]]<br/>[[List of Battlestar Galactica (2004 TV series) episodes|List of ''Battlestar Galactica'' episodes]]
}}

"'''33'''" is the first episode of [[Battlestar Galactica (season 1)|first season]] and the [[television pilot|pilot]]<ref name="commentary0034">{{cite video
 |people     = [[Michael Rymer]]
 |date      = 2005-07-26
 |title      = ''"33"''
 |medium     = [[Audio commentary|DVD commentary]]
 |publisher  = [[NBC Universal]]
 |location   = [[New York City]], USA
 |time       = 00:34
 |isbn       = 1-4170-5406-9
}}</ref> episode of the reimagined [[military science fiction]] [[television program]] ''[[Battlestar Galactica (2004 TV series)|Battlestar Galactica]]'', immediately following the events of [[Battlestar Galactica (miniseries)|the 2003 miniseries]].  "33" follows ''Galactica'' and her civilian fleet as they are forced to contend with constant Cylon pursuit for days without sleep; they are forced to ultimately destroy one of their own ships to foil the Cylons and earn their first respite of the series.

The episode was written by series creator [[Ronald D. Moore]], and the television directoral debut of [[Michael Rymer]].  Moore and executive producer [[David Eick]] made the decision to slot this episode as the first of the season because of its potential impact on the audience.  "33" distinguished the themes of the new ''Battlestar Galactica'' series by following characters on the spaceships, on the planets that were fled, and in the minds of other characters.  Attention to detail was prevalent in this first episode; the production team, the editing team, and even the actors themselves strove for authenticity of specific portrayals and moments.

Though there were compromises made due to concerns of the episode being too dark for audiences, the episode was lauded by both cast and crew in addition to winning the 2005 [[Hugo Award for Best Dramatic Presentation#Short Form|Hugo Award for Best Dramatic Presentation, Short Form]].  "33" originally aired on [[Sky One]] in the United Kingdom on October 18, 2004, and subsequently aired on the [[Sci Fi Channel (United States)|Sci Fi Channel]] in the United States on January 14, 2005, along with the following episode "[[Water (Battlestar Galactica)|Water]]".<ref name="tvgwater">{{cite web
| title       = ''Battlestar Galactica'' Episode: "Water"
| url         = http://www.tvguide.com/detail/tv-show.aspx?tvobjectid=191395&more=ucepisodelist&episodeid=4371644
| work        = TVGuide.com
| publisher   = [[TV Guide]]
| location    =
| accessdate  = 2009-06-06
}}</ref>

==Plot==
Having [[Battlestar Galactica (TV miniseries)|fled]] the besieged Ragnar Anchorage, the convoy of refugee [[starship|spaceship]]s is relentlessly pursued and attacked by [[Cylon Basestar]]s.  The colonial fleet must execute a [[faster-than-light]] (FTL) jump every 33 minutes to escape the Cylons, who consistently arrive at the new jump coordinates approximately 33 minutes later.  After over 130 hours and 237 jumps, the fleet's crew and passengers, particularly those aboard [[Battlestar Galactica (ship)#Battlestar Galactica (2003)|''Galactica'']], have been [[sleep deprivation|operating without sleep]] while facing the strain of nearly constant military action.

Upon the 238th consecutive jump, the ''Olympic Carrier'' (a commercial passenger vessel with 1,345 souls aboard) is accidentally left behind and the attacks unexpectedly cease, allowing the fleet some respite.  When it arrives three hours later, President [[Laura Roslin]] and Commander Adama order [[Lee Adama|Capt. Lee "Apollo" Adama]] and [[Kara Thrace|Lt. Kara "Starbuck" Thrace]] to destroy it, believing that it has been infiltrated by Cylons and now poses a threat to the fleet's safety.  The colonial officers destroy the ship while the rest of the colonial fleet jumps away.  Baltar's [[Number Six (Battlestar Galactica)#Head/Inner/Messenger Six|internal Number Six]] explains to him that God is looking after his interests, implying that a scientist on the ''Olympic Carrier'' was preparing to reveal Baltar's unwitting collusion with the Cylon attack on [[Twelve Colonies|the colonies]].
After the fleet's last jump, the Cylons do not return, and the President's survivor whiteboard aboard ''[[Colonial One]]'', the result of a fleetwide census, is updated with one additional soul (to 47,973) with the birth of the fleet's first child aboard the ''Rising Star''—a boy.

Meanwhile, on Caprica, [[Karl Agathon|Lt. Karl Agathon]] ([[aviator call sign|call sign]] "Helo") is captured by a Cylon patrol and then "rescued" from his Cylon captors by a [[Number Eight (Battlestar Galactica)|Number Eight]] in the guise of his crewmate Sharon "Boomer" Valerii, who shoots a [[Number Six (Battlestar Galactica)|Number Six]] to free him.

==Writing==
[[File:Ronald D Moore - Comic Con 2013.jpg|thumb|Ronald D. Moore, episode writer and series creator, in 2013]]
Preparing for production of ''Battlestar Galactica''{{'}}s first season, writer and series creator [[Ronald D. Moore]] wrote a short list of potential storylines, one of which was "the fleet jumps every 33 minutes; because the Cylons are relentlessly pursuing them, the crew gets no sleep."<ref name="commentary0052">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 00:52
|isbn       = 1-4170-5406-9
}}</ref>  Conferring with fellow [[executive producer]] [[David Eick]], the two decided that this story would be "the best way to kick off the season".<ref name="commentary0115">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 01:15
|isbn       = 1-4170-5406-9
}}</ref>  Moore described writing "33" as a great experience; he wrote the whole script without a [[outline (summary)|story outline]] or much structure, excited to begin the first episode of the first season and start the first year already "at the end of the road".<ref name="RDM20050113">{{cite web
| first       = Ronald D.
| last        = Moore
| authorlink  = Ronald D. Moore
| title       = Why 33 minutes?
| url         = http://blogs.scifi.com/battlestar/archives/2005/01/why-33-minutes.php
| work        = Ron Moore Blog
| publisher   = [[Sci Fi Channel (United States)|SCI FI]]
| location    = [[New York City]], USA
| date        = 2005-01-13
| accessdate  = 2009-03-29
| archivedate = 2008-05-13
| archiveurl  = https://web.archive.org/web/20080513232549/http://blogs.scifi.com/battlestar/2005/01/why-33-minutes.php
| quote       = [T]he mystery of 33 will be permanent on this show.  No explanation, not even the attempt.  Let it just be a number that seemed like an eternity for five long days on the battlestar ''Galactica''.
}}</ref>  Moore wrote the episode over his [[Christmas break]] before the series was officially picked up; he later claimed that this aspect was what made the episode "one of the more fun projects that [he] wrote all of the first season."<ref name="commentary0126">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 01:26
|isbn       = 1-4170-5406-9
}}</ref>

David Eick found the episode to be a "standalone concept" that did not require having seen the miniseries to understand it.  Because the miniseries ended "at a very happy place", starting the series in the middle of a crisis without explanation, and showing the audience that "actually, while you&mdash;the audience&mdash;were away, really bad things have been happening" made for a much more intriguing and interesting story.<ref name="commentary0212">{{cite video
|people     = [[David Eick]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 02:12
|isbn       = 1-4170-5406-9
}}</ref>  "33{{"'}}s complex storyline was a harbinger for episodes to come, and laid the groundwork with the network and audiences alike.<ref name="commentary0439">{{cite video
|people     = [[Michael Rymer]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 04:39
|isbn       = 1-4170-5406-9
}}</ref>

Moore explained on his [[blog]] that the number 33 had no hidden meaning or significance, only that he felt it sufficiently long to allow minor functions like snacking, showering, or [[cat nap]]ping, but was too short to allow anybody to gain any meaningful [[sleep]] and recharge their batteries.  Further, Moore intentionally gave the number no meaning to avoid creating and inserting unnecessary [[technobabble]] into a drama-driven episode.<ref name="RDM20050113" />

==Production==
[[File:David Eick.jpg|thumb|left|David Eick, co-executive producer, in 2007]]
"33" was [[television director|director]] [[Michael Rymer]]'s first television episode.  He accepted the job without reading the script, saying that based on his writing experience, "33" went well beyond his expectations and excited him.<ref name="commentary0307">{{cite video
|people     = [[Michael Rymer]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 03:07
|isbn       = 1-4170-5406-9
}}</ref>  [[Bear McCreary]] originally composed the musical theme "Boomer Theme" for this episode; it was later expanded for use with the [[Number Eight (Battlestar Galactica)#Sharon "Athena" Agathon|Athena character]], before becoming the {{lang|la|de facto}} "Hera Theme" for the character [[Hera Agathon]] in the [[Battlestar Galactica (season 4)|fourth season]] episode, "[[Islanded in a Stream of Stars]]".<ref name="McCreary20090312">{{cite web
 |url=http://www.bearmccreary.com/blog/?p=1671 
 |title=BG4: "Islanded in a Stream of Stars" 
 |accessdate=2010-11-08 
 |last=McCreary 
 |first=Bear 
 |authorlink=Bear McCreary 
 |date=2009-03-07 
 |work=Bear's ''Battlestar'' Blog 
 |publisher=Bear McCreary 
 |location= 
 |archivedate=2010-11-08 
 |archiveurl=http://www.webcitation.org/5u5wJJHQh?url=http%3A%2F%2Fwww.bearmccreary.com%2Fblog%2F%3Fp%3D1671 
 |deadurl=no 
 |df= 
}}</ref>  [[Joel Ransom]] was the director of photography for [[Battlestar Galactica (TV miniseries)|the miniseries]], but when Eick learned he was unavailable for the series, he turned to [[Stephen McNutt]], with whom he had worked on [[American Gothic (1995 TV series)|''American Gothic'']].  In the interim, McNutt had moved on to shooting in [[high-definition video]]; this was fortuitous for the production team because, while Ransom had filmed the miniseries on [[35 mm film]], the production team was switching to high-definition video for the series.<ref name="commentary1109">{{cite video
|people     = [[David Eick]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 11:09
|isbn       = 1-4170-5406-9
}}</ref>

[[Executive producer]] [[David Eick]] opined that "33" was the "[[silver bullet]]" that ultimately tipped the scales in their favor and convinced the [[Sci Fi Channel (United States)|Sci Fi Channel]] to pick up the series.  The network's biggest concern in picking up the series was that ''Battlestar Galactica'' would fall victim to the same trappings of [[space opera]] as other television properties (''[[Star Trek]]'', ''[[Andromeda (TV series)|Andromeda]]'', ''[[Stargate]]'').  Two aspects that assuaged these concerns were specifically discussed in the episode's [[audio commentary|DVD commentary]].  First, "33" went into [[Gaius Baltar]]'s ([[James Callis]]) mind and visited his house on [[List of Battlestar Galactica (reimagining) locations#Caprica|Caprica]] (shot in [[Lions Bay, British Columbia]]);<ref name="commentary3229">{{cite video
|people     = [[Michael Rymer]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 32:29
|isbn       = 1-4170-5406-9
}}</ref> being swept away by the blue skies and beaches in his fantasy was not the sort of imagery expected of space opera-type shows.  Second was going back to the devastated Caprica and following-up with Helo's ([[Tahmoh Penikett]]) story.<ref name="commentary0329">{{cite video
|people     = [[David Eick]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 03:29
|isbn       = 1-4170-5406-9
}}</ref>

With [[sleep deprivation]] one of the major plot points of the episode, actor [[Edward James Olmos]] (William Adama) liaised with an expert on the subject and the crew to best depict the actual effects realistically.  Following up, director [[Michael Rymer]] gave each main cast member a specific symptom to play up, so as to avoid repetition on screen.  Olmos and several other cast members took their study a step further, to immerse themselves by restricting their sleep patterns to about three hours a night to emphasize what their expert was imparting.<ref name="bsgtoc">{{cite book | last = Bassom | first = David | editor = Adam "Adama" Newell | title = Battlestar Galactica: The Official Companion | publisher = [[Titan Books]] | isbn = 978-1-84576-097-7 | page = 46 | date = 2005-06-01 }}</ref><ref name="commentary1707" />

In the episode's DVD commentary, Moore and Rymer related how there were endless discussion about the clocks to feature in this episode.  Concerns over [[digital clock|digital]] versus [[analog clock|analog]], size and shape, the ratio of digital to analog clocks, whether they should run forwards or backwards, and whether any labels should be [[stencil]]ed or hand-drawn were all brought up. David Eick also noted that as of the commentary's recording, the clock at [[Felix Gaeta]]'s (Juliani) station still had its "33" label affixed.<ref name="commentary3439">{{cite video
|people     = [[Ronald D. Moore]] & [[Michael Rymer]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 34:39
|isbn       = 1-4170-5406-9
}}</ref>

===Editing===
The first cut of "33" was ten minutes too long.  Despite this, the production crew took extra care not to eliminate "human moments" in their efforts to trim the episode.  These included a shot of [[Galen Tyrol]] (Douglas) and [[Cally Henderson Tyrol|Cally Henderson]] (Clyne) walking across ''Galactica''{{'}}s hangar bay, a shot of Crewman Socinus (Oyarzun) giving a bedraggled look over the shoulder of another crewmember, and a shot of [[Anastasia Dualla|PO2 Dualla]] (McClure) walking through ''Galactica''{{'}}s remembrance corridor.<ref name="commentary1231">{{cite video
|people     = [[Ronald D. Moore]] & [[David Eick]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 12:31
|isbn       = 1-4170-5406-9
}}</ref>

{{multiple image
| footer    = ''Lest We Forget'' (left) and ''[[Raising the Flag at Ground Zero]]'' (right)
| image1    = Lest we forget (313).jpg
| width1    = 176
| image2    = Ground Zero Spirit.jpg
| width2    = 180
}}
In a question-and-answer session, Moore revealed a scene written for, but cut from, the episode.  In the [[deleted scene|cut scene]], the recurring prop in the characters' briefing room was to have been explicitly introduced and explained; the prop remained in the series, but its [[backstory]] was cut.<ref name="RDM20050130">{{cite web
| first       = Ronald D.
| last        = Moore
| authorlink  = Ronald D. Moore
| title       = Q&A
| url         = http://blogs.scifi.com/battlestar/2005/01/q-a-1.php
| work        = Ron Moore Blog
| publisher   = [[Sci Fi Channel (United States)|SCI FI]]
| location    = [[New York City]], USA
| date        = 2005-01-30
| accessdate  = 2009-05-30
| archiveurl  = https://web.archive.org/web/20080513232539/http://blogs.scifi.com/battlestar/2005/01/q-a-1.php
| archivedate = 2008-05-13
}}</ref>

<blockquote>There was a scene cut from "33" where we saw Laura being given her copy of the photo along with a card that said it was taken on the roof of the capitol building on Aerilon during the attack. The photo was inspired by the famous shot of the fire-fighters raising the flag at Ground Zero that became iconic. I thought the Colonies would have their own version of this -- a snapshot taken in the moment that becomes a symbol of the day they can never forget and of all they had lost. The photo itself is of a soldier falling to his knees (possibly shot or simply overcome by emotion) as he stands on the rooftop over looking the devastation of his city, while the Colonial flag waves at the edge of frame. The inscription below the photo on Laura's plaque reads, "Lest We Forget" in itself a reference to the inscription on the watch presented to John Wayne's character in ''[[She Wore a Yellow Ribbon]]''.</blockquote>

Other cut scenes included one shot in the pilots' [[head (watercraft)|head]], showing the pilots "wrecked and exhausted [...] with an exchange between Starbuck and Apollo",<ref name="commentary1416">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 14:16
|isbn       = 1-4170-5406-9
}}</ref> as well as several shots of Commander Adama (Olmos) [[pharyngeal reflex|gagging]] and [[vomiting]] because of [[gastroesophageal reflux disease|acid reflux]] brought on by sleep deprivation.<ref name="commentary1707">{{cite video
|people     = [[Michael Rymer]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 17:07
|isbn       = 1-4170-5406-9
}}</ref>  Another line of Olmos'&mdash;an [[Ad libitum|''ad-lib'']] about suicides in the fleet&mdash;was cut so as not to alienate audiences by being "too dark".<ref name="commentary1925">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 19:25
|isbn       = 1-4170-5406-9
}}</ref>

As originally written and shot, when Apollo ([[Jamie Bamber]]) fires on the ''Olympic Carrier'', it was made clear that he sees people inside.  Moore wrote the scene to be strong and clear that the characters were making the decision to fire on the passenger liner in full awareness of the consequences to illustrate and emphasize "the uncompromising nature of the show."  This was an "enormous fight" between Moore and the network, with the latter feeling this was another scene that was "too dark" and had the potential to turn away audiences; the network further implied that if the scene were left intact, they may have been compelled to air the episodes out of order.  To placate the network, Moore and Eick changed the ending of the episode and "cheated"; instead, when Apollo flies by the ''Olympic Carrier'', it is unclear whether or not there is anybody inside.<ref name="commentary2838">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 28:38
|isbn       = 1-4170-5406-9
}}</ref>  In a "small act of defiance", [[visual effects supervisor]] [[Gary Hutzel]] snuck in small, indeterminate movement behind one or two of the ''Olympic Carrier''{{'}}s windows on behalf of the production and writing teams.<ref name="commentary3050">{{cite video
|people     = [[David Eick]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 30:50
|isbn       = 1-4170-5406-9
}}</ref>  The episode also originally ended with Helo's escape from the Cylons on Caprica; again tasked by the network to keep the episode from being "too dark", Moore wrote in an additional scene&mdash;President Roslin ([[Mary McDonnell]]) learning of the newborn&mdash;upon which to end the episode on a hopeful note.<ref name="commentary4222">{{cite video
|people     = [[Ronald D. Moore]]
|date      = 2005-07-26
|title      = ''"33"''
|medium     = [[Audio commentary|DVD commentary]]
|publisher  = [[NBC Universal]]
|location   = [[New York City]], USA
|time       = 42:22
|isbn       = 1-4170-5406-9
}}</ref>

==Release and reception==
"33" first aired in the [[Television in the United Kingdom|United Kingdom]] on {{start date|2004|10|18}},<ref name="SkyOne20041017">{{cite news
| title = Sky One finds the outlandish once more in its schedules
| url = http://www.cult.tv/index.php?cm_id=606&cm_type=article
| work =
| publisher = Cult tv
| location =
| date = 2004-10-17
| accessdate = 2007-12-21
}}</ref> and in the [[United States]] on {{start date|2005|01|14}},<ref name="tvg33">{{cite web
| title       = ''Battlestar Galactica'' Episode: "33"
| url         = http://www.tvguide.com/detail/tv-show.aspx?tvobjectid=191395&more=ucepisodelist&episodeid=4371612
| work        = TVGuide.com
| publisher   = [[TV Guide]]
| location    =
| accessdate  = 2009-06-06
}}</ref> almost three months later.  UK viewers obliged US ''Battlestar Galactica'' fans by [[copyright infringement|illegally copying]] the episode&mdash;uploading [[BitTorrent (protocol)|Torrents]] to the [[Internet]]&mdash;within hours of its [[Sky1|Sky One]] airing.<ref name="eWeek20090420">{{cite journal
| last        = Wilcox
| first       = Joe
| date        = 2009-04-20
| title       = Will Copyright Holders Lose the Pirate Booty?
| journal     = [[eWeek]]
| publisher   = [[Ziff Davis]]
| issn        = 1530-6283
| accessdate  = 2009-05-30
| url = http://www.eweek.com/c/a/Data-Storage/Will-Copyright-Holders-Lose-the-Pirate-Booty-556524/
}}</ref>  "33" has been released thrice on [[home video]] as part of the [[Battlestar Galactica (season 1)|first season]] collected sets; on {{start date|2005|07|26}} as a [[Best Buy]] exclusive, again on {{start date|2005|9|20}}, and finally as an [[HD DVD]] set on {{start date|2007|12|04}}.  The episode was also released on {{start date|2009|07|28}} as part of the entire series' home video set on both [[DVD]] and [[Blu-ray Disc]].<ref name="tvshowsondvd">{{cite web
| title       = TVShowsOnDVD.com - Release Information for Battlestar Galactica
| url         = http://www.tvshowsondvd.com/releaselist.cfm?ShowID=8735
| publisher   = [[TVShowsOnDVD.com]]
| accessdate  = 2009-05-30
| archiveurl  = https://web.archive.org/web/20090525034109/http://www.tvshowsondvd.com/releaselist.cfm?ShowID=8735
| archivedate = 2009-05-25}}</ref>

Both series creator [[Ronald D. Moore]] and star [[Jamie Bamber]] (Lee Adama) claim "33" as their favourite episode.  Bamber described it as "...the perfect episode of ''Battlestar Galactica''."  Emphasizing the dark, gritty, and nightmarish aspects of the episode, the actor felt it was a microcosm of the series as a whole.<ref name="ACEDmag">{{cite journal
| last        = Bensoussan
| first       = Jenna
| date        = 2007-11-24
| title       = Battlestar Galactica: Cast Interviews
| journal     = [[ACED Magazine]]
| publisher   = ACED Magazine, Inc.
| location    = [[Coral Springs, Florida]], USA
| url         = http://acedmagazine.com/content/view/677/1/
| archiveurl  = https://web.archive.org/web/20080103041056/http://acedmagazine.com/content/view/677/1/
| archivedate = 2008-01-03
| accessdate  = 2009-03-29
}}</ref>  In interviews with ''[[Wired UK]]'' and the ''[[Los Angeles Times]]'', Moore opined that the episode subverted viewers' expectations and was a "fantastic way to open that first year."<ref name="WUK20101026">{{cite journal
 |last=Grossman 
 |first=Lisa 
 |date=2010-10-26 
 |title=Galactica creator on why he steered clear of ‘technobabble’ 
 |journal=[[Wired UK]] 
 |publisher=[[Condé Nast Publications]] 
 |location=[[United Kingdom]] 
 |issn= 
 |url=http://www.wired.co.uk/news/archive/2010-10/26/galactica-technobabble 
 |archiveurl=http://www.webcitation.org/5u0Epxukt?url=http%3A%2F%2Fwww.wired.co.uk%2Fnews%2Farchive%2F2010-10%2F26%2Fgalactica-technobabble 
 |archivedate=2010-11-05
 |accessdate=2010-11-04 
 |deadurl=no 
 |df= 
}}</ref><ref name="LAT20090319">{{cite news
 |first=Jevon 
 |last=Phillips 
 |title='Battlestar Galactica' finale: interview with Ron Moore 
 |url=http://latimesblogs.latimes.com/showtracker/2009/03/battlestar-ga-4.html 
 |work=[[Los Angeles Times]] 
 |publisher=[[Tribune Company]] 
 |location=[[Los Angeles]], [[California]], USA 
 |issn=0458-3035 
 |date=2009-03-19 
 |accessdate=2010-11-04 
 |archivedate=2010-11-05
 |archiveurl=http://www.webcitation.org/5u0EzYNve?url=http%3A%2F%2Flatimesblogs.latimes.com%2Fshowtracker%2F2009%2F03%2Fbattlestar-ga-4.html 
 |quote=Creator Ron Moore answers questions about his favorite episode and more. 
 |deadurl=no 
 |df= 
}}</ref>

"33" won the 2005 [[Hugo Award for Best Dramatic Presentation, Short Form]],<ref name="hugo">{{cite web
 |title=The Hugo Awards - 2005 Hugo Awards 
 |url=http://www.thehugoawards.org/?page_id=12 
 |work=The Hugo Awards 
 |publisher=[[World Science Fiction Society]] 
 |location=[[Glasgow]], [[Scotland]], [[United Kingdom]] 
 |accessdate=2009-03-28 
 |archiveurl=http://www.webcitation.org/5u0F4BfQy?url=http%3A%2F%2Fwww.thehugoawards.org%2Fhugo-history%2Fcredits%2F 
 |archivedate=2010-11-05
 |deadurl=no 
 |df= 
}}</ref> and drew a 2.6 household [[Nielsen ratings|Nielsen rating]], attracting 3.1 million viewers and making it the #2 program on cable (8pm-11pm).<ref name="tfc">{{cite web
 |title=Viewers Embrace SCI FI's 'Galactica' 
 |url=http://www.thefutoncritic.com/news.aspx?id=20050119scifi01 
 |publisher=The Futon Critic 
 |date=2005-01-19 
 |accessdate=2009-03-28 
 |archiveurl=http://www.webcitation.org/5u0F9asb5?url=http%3A%2F%2Fwww.thefutoncritic.com%2Fnews.aspx%3Fid%3D20050119scifi01 
 |archivedate=2010-11-05
 |deadurl=no 
 |df= 
}}</ref>  At the website [[Television Without Pity]], the staff review rated the episode an "A+", while ({{as of|2010|11|lc=yes}}) 546 of their readers awarded it an average grade of "B".<ref name="TWOP">{{cite web
 |author=Strega 
 |title=Stress Test 
 |url=http://www.televisionwithoutpity.com/show/battlestar_galactica/33.php 
 |work=[[Television Without Pity]] 
 |publisher=[[NBC Universal]] 
 |date= 
 |accessdate=2010-11-04 
 |archiveurl=http://www.webcitation.org/6OQEv2dES?url=http%3A%2F%2Fwww.televisionwithoutpity.com%2Fshow%2Fbattlestar-galactica%2Fthirty-three%2F 
 |archivedate=2014-03-28 
 |deadurl=no 
 |df= 
}}</ref>  The ''[[New York Post]]''{{'}}s "10 Most Dramatic Moments of the '00s" included "33" in its #10 spot, describing it as the premiere episode of "a sci-fi show with high stakes and serious guts."<ref name="NYP20091218">{{cite web
 |url=http://www.nypost.com/p/blogs/tvblog/drama_mama_the_most_dramatic_moments_GegdJvSRQiQ3blNILZP7qI 
 |title=Drama Mama: The 10 Most Dramatic Moments of the ’00s 
 |first=Tiffany Wendeln 
 |last=Connors 
 |date=2009-12-18 
 |work=LIVE: The TV Blog 
 |publisher=''[[New York Post]]'' 
 |location=[[New York City]], USA 
 |accessdate=2009-12-20 
 |archiveurl=http://www.webcitation.org/5u0FLpWoo?url=http%3A%2F%2Fwww.nypost.com%2Fp%2Fblogs%2Ftvblog%2Fdrama_mama_the_most_dramatic_moments_GegdJvSRQiQ3blNILZP7qI 
 |archivedate=2010-11-05
 |deadurl=no 
 |df= 
}}</ref>

==References==
{{reflist|30em}}

==External links==
{{wikiquote|Battlestar Galactica (2003)#33_.5B1.01.5D|{{noitalic|"33"}}}}
* [http://www.syfy.com/battlestar/episodes/season/1/episode/101/33 "33"] at [[Syfy]]
* [[BattlestarWiki:33|"33"]] at the Battlestar Wiki
* {{tv.com episode|battlestar-galactica/33-307360|33}}
* {{imdb episode|0519761|33}}

{{Battlestar Galactica (reimagined) episodes}}
{{Hugo Award for Best Dramatic Presentation, Short Form}}

[[Category:2004 American television episodes]]
[[Category:Battlestar Galactica (2004) episodes]]
[[Category:Hugo Award for Best Dramatic Presentation, Short Form-winning works]]
[[Category:American television pilots]]