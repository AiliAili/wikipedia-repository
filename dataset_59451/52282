{{Infobox Organization
|name                = Microbiology Society
|image               = Microbiology_Society.png
|size                = 275px
|caption             = Microbiology Society 
|abbreviation        = MicroSoc
|motto               = A world in which the science of microbiology provides maximum benefit to society
|formation           = 1945
|extinction          = 
|status              = Not-for-profit organisation
|purpose             = Microbiology
|location            = Charles Darwin House, 12 Roger Street, London, WC1N 2JL
|region_served       = Worldwide
|membership          = 4000 microbiologists
|leader_title        = Chief Executive
|leader_name         = Peter Cotgreave
|main_organ          = Microbiology Society
|parent_organization = 
|budget              = 
|website             = [http://www.microbiologysociety.org/ Microbiology Society]
|remarks             = 
}}

The '''Microbiology Society''' (previously the Society for General Microbiology) is a [[learned society]] based in the United Kingdom with a worldwide membership based in universities, industry, hospitals, research institutes and schools. It is the largest learned microbiological society in Europe.<ref name="MicroSoc_homepage">{{cite web |url=http://www.microbiologysociety.org/index.cfm |title=Microbiology Society Home |publisher=Microbiology Society |work=Homepage |accessdate=2015-09-22}}</ref> Interests of its members include basic and applied aspects of [[virus]]es, [[prion]]s, [[bacteria]], [[rickettsia]]e, [[mycoplasma]], [[fungus|fungi]], [[algae]] and [[protozoa]], and all other aspects of [[microbiology]]. Its headquarters in Charles Darwin House, London.<ref name="charlesdarwinhouse.co.uk">{{cite web|url=http://www.charlesdarwinhouse.co.uk/|title=Charles Darwin House > Home|work=charlesdarwinhouse.co.uk}}</ref> The society's current president is Prof. [[Neil A. R. Gow|Neil Gow]].<ref>{{Cite web|url=http://www.microbiologysociety.org/about-us/whos-who/council-members.cfm|title=Council {{!}} Microbiology Society|last=Society|first=Microbiology|website=www.microbiologysociety.org|access-date=2016-06-07}}</ref> The Society is a member of the Science Council.<ref>{{cite web|url=http://www.sciencecouncil.org|title=The Science Council|work=sciencecouncil.org}}</ref>

==History==
The society was founded on 16 February 1945 as the Society for General Microbiology. Its first president was [[Alexander Fleming]].<ref name="SGM_history">{{cite web |url=http://www.microbiologysociety.org/about-us/our-history/index.cfm |title=Our History|publisher=Microbiology Society |work=Homepage |accessdate=2013-09-27}}</ref><ref name="Postgate_TIM">{{cite journal |author=Postgate J |title=Fifty years of the SGM |journal=[[Trends in Microbiology]] |volume=3 |issue=7 |pages=249–50 |date=July 1995 |pmid=7551634 |doi=10.1016/S0966-842X(00)88935-0 |url=http://linkinghub.elsevier.com/retrieve/pii/S0966-842X(00)88935-0 |accessdate=2013-09-24}}</ref> The Society's first academic meeting was in July 1945<ref>{{cite web|title=Our History|url=http://www.microbiologysociety.org/about-us/our-history/timeline.cfm|publisher=Microbiology Society|accessdate=2015-09-22}}</ref> and its first [[scientific journal|journal]], the ''Journal of General Microbiology'' (later renamed [[Microbiology (journal)|''Microbiology'']]), was published in 1947.<ref name="SGM_history" /><ref name="Postgate_TIM" /> A symposium series followed in 1949, and a sister journal, the ''[[Journal of General Virology]]'', in 1967.<ref name="Postgate_TIM" /> The society purchased its own headquarters in Reading in 1971, after initially sharing accommodation with the [[Biochemical Society]] in [[London]].<ref name="SGM_history" /> In 2014 the Society moved to Charles Darwin House, London,<ref name="charlesdarwinhouse.co.uk"/> sharing the premises with several other learned societies. In 2015, the Society changed its name to the Microbiology Society, after its members voted in favour of the change.

==Activities==
The Society currently organises a large Annual Conference and a number of smaller Focused Meetings, which cover a specific microbiology discipline. It publishes a magazine, ''Microbiology Today''<ref>{{cite web|url=https://www.microbiologysociety.org/publications/microbiology-today/current-issue.html|title=Microbiology Today - Society for General Microbiology|author=Society for General Microbiology|work=sgm.ac.uk}}</ref> (formerly SGM Quarterly), and academic journals in virology and microbiology:
*[[Microbiology_(journal)|<i>Microbiology</i>]] 
*[[International_Journal_of_Systematic_and_Evolutionary_Microbiology|International Journal of Systematic and Evolutionary Microbiology]]
*[[Journal_of_General_Virology|Journal of General Virology]]
*[[Journal_of_Medical_Microbiology|Journal of Medical Microbiology]]
*''JMM Case Reports''<ref>{{cite web|url=http://jmmcr.microbiologyresearch.org/content/journal/jmmcr/|title=JMM Case Reports|work=sgmjournals.org}}</ref>
*[[Microbial_Genomics_(journal)|''Microbial Genomics'']]

==Society Prizes==
The Microbiology Society awards a range of prizes<ref>{{cite web|url=http://www.sgm.ac.uk/en/grants-prizes/prize-lectures.cfm|title=Prize Lectures|author=Society for General Microbiology|work=sgm.ac.uk}}</ref> in recognition of significant contributions to microbiology. 

In 2009, the Society announced the Society for General Microbiology Prize Medal, awarded annually to a microbiologist of international standing whose work has had a far-reaching impact beyond microbiology. The first medal was awarded to [[Stanley Prusiner]]. The recipient of the Prize Medal gives a lecture based on the work for which the award has been made, which is usually published in a Society journal. In 2015, the prize was renamed the Microbiology Society Prize.

The [[Marjory Stephenson Prize]] is awarded annually for an outstanding contribution of current importance in microbiology. The winner receives £1000 and gives a lecture on his/her work at a Society meeting. The lecture is usually published in a society journal. [[Marjory Stephenson]] was the second president of the Society (1947–1949) and a distinguished pioneer of chemical microbiology.<ref>{{cite web |url=http://www.microbiologysociety.org/grants-prizes/all-grants.cfm/marjory-stephenson-prize-lecture |title=SGM :  Grants & Prizes : Prize Lectures : Marjory Stephenson Prize Lecture |publisher=Society for General Microbiology |work=Homepage |accessdate=2014-05-28}}</ref>

The [[Fleming Prize]] Lecture is awarded annually to recognise outstanding research in any branch of microbiology by a microbiologist in the early stages of his/her career. [[Sir Alexander Fleming]] was the first President of the Society (1945-1947) and received a [[Nobel Prize]] for his discovery of penicillin.

The [[Sir Howard Dalton Young Microbiologist of the Year Competition]] is presented annually to a Society member who is a PhD student or early-career postdoctoral researcher. The competition is judged on the participants’ oral or poster presentations at Society conferences. The prize was renamed in 2009 in honour of the late Howard Dalton. 

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.microbiologysociety.org/}}
* [http://www.microbiologyonline.org.uk/ Microbiology Society - education website]
{{Use dmy dates|date=July 2011}}

{{DEFAULTSORT:Microbiology Society}}
[[Category:1945 establishments in the United Kingdom]]
[[Category:Biology societies]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Microbiology organisations]]
[[Category:Organisations based in the London Borough of Camden]]
[[Category:Science and technology in London]]
[[Category:Scientific organizations established in 1945]]