{{Infobox journal
| title = Coléoptères
| cover = 
| formernames =
| abbreviation =
| discipline = [[Entomology]]
| language = French
| editor =
| publisher = Roger-Paul Dehambre
| country = France
| history = 1995-present
| frequency =
| openaccess =
| license =
| impact =
| impact-year =
| ISSN = 1265-3357
| eISSN =
| LCCN =
| CODEN =
| OCLC = 46630757
| website =
| link1 =
| link1-name =
| JSTOR =
}}
'''''Coléoptères''''' is a [[French language|French-language]] [[scientific journal]] of [[entomology]].

== History ==
Following the cessation of publication of the ''[[Bulletin de la Société Sciences Nat]]'' in 1995, the journals ''Coléoptères'' and ''[[Besoiro]]'' were founded to accommodate some of the manuscripts that would have gone to the older journal.

== Authors ==
At the beginning, several authors published in the journal, but now only some specialists do, mainly Philippe Antoine ([[Flower chafer|Cetoniinae]]), Roger-Paul Dechambre ([[Dynastinae]]), Thierry Deuve ([[Ground beetle|Carabidae]]), Gérard Tavakilian ([[Longhorn beetle|Cerambycidae]]), and Marc Lacroix ([[Melolonthinae]]).<ref>[http://www.insects.demon.co.uk/revuecoleopteres.html ''Coléoptères'': complete list of the parts already published]</ref>

== Composition and production ==
Each number follows some rules about presentation and contains only one work. Black-and-white figures, but also many colour photographs, illustrate the publications. The size of the journal is [[ISO 216|A5]]; it is produced by [[photocopier|photocopying]]. There is no regular periodicity, a new issue is published when a new work is accepted by the [[editorial board]]. Each issue is produced at about hundred copies, of which many are sent to beetle specialists, and about 50 copies are made available for sale. When an issue is exhausted, a reissue is done.

== References ==
{{Reflist}}

{{wikispecies|ISSN 1265-3357}}

{{DEFAULTSORT:Coleopteres}}
[[Category:Publications established in 1995]]
[[Category:Entomology journals and magazines]]
[[Category:Beetle literature]]
[[Category:French-language journals]]