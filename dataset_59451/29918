{{Infobox single
| Name           = Bam  Thwok
| Artist         = the [[Pixies]]
| Cover          = Pixies - Bam Thwok.png
| Released       = June 15, 2004
| Format         = [[Music download]]
| Recorded       = March 2004 at [[Stagg Street Studios]], Los Angeles, California, United States; [[Audio mixing (recorded music)|mixed]] at [[Sound City Studios]]
| Genre          = [[Alternative rock]]
| Length         = 2:35
| Label          =
| Writer         = [[Kim Deal]]
| Producer       = [[Ben Mumphrey]]
| Chronology     = [[Pixies]]
| Last single    = "[[Debaser]]"<br />(1997)
| This single    = "'''Bam Thwok'''"<br />(2004)
| Next single    = "Bagboy"<br />(2013)
| Misc           = {{Audiosample
  | Upper caption = Audio sample
  | Name = Pixies&nbsp;— "Bam Thwok"
  | Audio file = BamThwok.ogg
  }}
}}
"'''Bam Thwok'''" is a 2004 [[Music download|download-only]] [[single (music)|single]] by the American [[alternative rock]] band the [[Pixies]]. Written and sung by bassist [[Kim Deal]], the song was released exclusively through the [[iTunes Store]] on June 15, 2004. Upon its release, "Bam Thwok" was a commercial success, debuting at number one on the very first release of the [[UK Download Chart]]. The song was the band's first recording since ''[[Trompe le Monde]]'' (1991).

Originally composed for the film ''[[Shrek 2]]'', "Bam Thwok" was not selected for the final soundtrack. The song's lyrics display a [[surrealism|surrealistic]] and nonsensical nature typical of the band; Deal's inspiration was a discarded child's art book she found on a [[New York City]] street. "Bam Thwok"'s major theme is "showing goodwill to everyone."

== Background ==
Following their 2004 reunion, the Pixies felt that recording a new song would "break the ice" between band members after their acrimonious split in 1993.<ref name="nme">{{cite web | url=http://www.nme.com/news/pixies/17179 | title=New Pixies Song on iTunes | accessdate=2007-04-23 | work=NME | date=2004-06-14}}</ref> As the band were announcing dates for their reunion world tour, [[DreamWorks Pictures|DreamWorks]] contacted manager Ken Goes to enquire whether they would be interested in recording a song for the title sequence of ''Shrek 2''. The band agreed, and frontman [[Black Francis]] and Deal began composing riffs for a song aimed at children.<ref name="fgpg208">Frank, Ganz, 2005. p. 208</ref> Deal had been experimenting with a [[chord progression]] for a while, and since her band [[The Breeders]] were then inactive, she decided to donate her new composition to the Pixies.

The Pixies chose Deal's riff, as it was a "poppier, more kid-friendly thing,"<ref name="fgpg208" /> and Francis agreed to let her sing lead vocals and write the new song; significantly, Francis is relegated to [[Backing vocalist|backing vocals]] on the track. Francis' move was meant to warm the previously cold relations between the two—in the previous two Pixies studio albums, ''[[Bossanova]]'' (1990) and ''Trompe le Monde'' (1991), Francis, the band's principal songwriter, had not let Deal contribute any songs or sing lead vocals. This became one of the reasons for the Pixies' 1993 split. However, the two appeared at the time to have resolved their differences: Francis praised "Bam Thwok," as "a really good song."<ref name="aversion" /> despite joking in interviews about how he planned to remove as many of her compositions as possible from a hypothetical new album.<ref name="newdvd">{{cite web | url=http://www.mtvasia.com/News/200510/07012615.html | title=Pixies One Fight Away From Finishing New LP, Release Live DVD | accessdate=2007-04-03 | publisher=[[MTV Asia]]. [[Viacom Media Networks]]. | date=2005-10-07|archiveurl = https://web.archive.org/web/20070929083357/http://www.mtvasia.com/News/200510/07012615.html |archivedate = September 29, 2007|deadurl=yes}}</ref> However, Deal eventually left the band in June 2013,<ref>{{cite web|last1=Cubarrubia|first1=RJ|title=Kim Deal Leaves the Pixies|url=http://www.rollingstone.com/music/news/kim-deal-leaves-the-pixies-20130614|work=[[Rolling Stone]]|accessdate=15 June 2014}}</ref> before the release of the band's next album, ''[[Indie Cindy]]''.

== Recording and release ==
<!-- Deleted image removed: [[Image:BamThwokiTunes.png|thumb|right|325px|"Bam Thwok" was released on the iTunes Music Store on June 15, 2004. The store, however, records a June 9 release.]] -->
The band arranged and rehearsed the song in lead guitarist [[Joey Santiago]]'s [[Pro Tools]] home studio, which he had built for his husband-and-wife band [[The Martinis]]. After in the words of Deal, "working it up a bit in Joe's [Santiago] Pro Tools thing,"<ref name="fgpg208" /> "Bam Thwok" was recorded in a DreamWorks-funded demo session. The band travelled to [[Stagg Street Studios]], a studio in Los Angeles, California, to record the song with engineer [[Ben Mumphrey]]. Francis later said the recording session "was very relaxed, a nice way to break the ice," and admitted that "it didn't feel like twelve years had passed." The song was mixed by Mumphrey at Sound City Studios in late March.<ref name="nme" />

"Bam Thwok" was released on the iTunes Store at midnight on June 15, 2004.<ref name="nme" /> The Pixies chose to release in that fashion as they were not signed to a major record label; their earlier albums had been released on [[4AD]] and [[Elektra Records]]. At the time of "Bam Thwok"'s release, the Pixies' management gave no indication as to whether future releases would be limited to the iTunes Music Store.<ref name="aversion" /> Additionally, DreamWorks rejected the recording and the song never appeared on the ''Shrek 2'' soundtrack.<ref name="newdvd" />

== Lyrics and melody ==
[[File:KimDeal~ATP.jpg|alt=|thumb|upright|"Bam Thwok" is one of the few Pixies songs written by bassist [[Kim Deal]].]]
The theme of "Bam Thwok" is, according to Deal, "about loving everyone, showing goodwill to everyone".<ref name="nme" /> The lyrics are typically surreal and offbeat; Deal's main inspiration for the song and its title was a discarded art book she found on a New York City street while on tour in the late 1990s.<ref name="fgpg208" /> She later described the book:<ref name="aversion">{{cite web | url=http://www.aversion.com/news/news_article.cfm?news_id=2552 | title=Pixies Record New Song | accessdate=2007-04-02 | publisher=Aversion.com | date=2004-06-15}}</ref> "From the handwriting, you could tell that this book must have belonged to a little kid. This kid had written a short story, a paragraph really, about a party that took place in another universe, about people and monsters that were partying together. That's what provided the inspiration for the lyrics."

The song is structured around a four-beat guitar melody that incorporates major chords throughout. It begins with full instrumentation, over which Santiago layers a short [[guitar solo]]. During the first verse, the guitars and bass "drop down", and do not re-emerge until the chorus, which is repeated several times. A fifteen-second "carousel-esque" organ solo  appears approximately midway through the song. The actual sound clip was performed and recorded by Santiago's father-in-law "many years ago" while he was a missionary in the Philippines.<ref name="nme" />

== Reception ==
"Bam Thwok" debuted at number one on an early version of the [[UK Download Chart]],<ref>The first official UK Download Chart was released on September 1, 2004: {{cite news|url=https://www.theguardian.com/technology/2004/sep/02/arts.media|date=2004-09-02|work=The Guardian|last=Wells|first=Matt|accessdate=2014-05-27|title=Record industry produces first chart of legal downloads}}</ref><ref>{{cite web | url=http://www.pocket-lint.co.uk/news/news.phtml/377/1401/view.phtml | title=Official downloads chart to be launched in UK | accessdate=2007-04-05 | author=Miles, Stuart | publisher=Pocket-lint | date=2004-06-28}}</ref> although one report said that the fact that the Pixies were in first place despite releasing the single halfway through the period surveyed and only through iTunes "suggests there could be problems to iron out&nbsp;... [unless] all iTunes owners are big fans of the Pixies."<ref>{{cite web | url=http://www.theregister.co.uk/2004/06/28/download_chart_lives/ | title=Pixies top UK download chart | accessdate=2006-09-10 | publisher=The Register | date=2004-06-28| archiveurl= https://web.archive.org/web/20060813230134/http://www.theregister.co.uk/2004/06/28/download_chart_lives/| archivedate= 13 August 2006 <!--DASHBot-->| deadurl= no}}</ref> In a press release from Apple, Pixies manager Ken Goes said: "By distributing our first song in 13 years exclusively on iTunes, we were able to quickly and inexpensively make it available to millions of fans in the US and Europe. One week after its release, we are thrilled at the response from iTunes users that have helped to make "Bam Thwok" a top seller across four countries."<ref name="bbcnews">{{cite news | url=http://news.bbc.co.uk/2/hi/3832201.stm | title=Strong Sales for iTunes in Europe | accessdate=2006-09-03 | publisher=BBC News | date=2006-06-23}}</ref><ref>{{cite web | url=http://www.apple.com/pr/library/2004/jun/23itunes.html | title=iTunes Music Store in Europe Sells 800,000 Songs in First Week | accessdate=2006-09-03 | publisher=Apple | date=2004-06-23| archiveurl= https://web.archive.org/web/20060828040326/http://www.apple.com/pr/library/2004/jun/23itunes.html| archivedate= 28 August 2006 <!--DASHBot-->| deadurl= no}}</ref>

Hannah Harper of BBC Music remarked that the song was "not the greatest thing they've ever done."<ref>{{cite web | last = Harper | first = Hannah | url=http://www.bbc.co.uk/derby/music/reviews/singles/pixies_bam_thwok_review.shtml | title=Pixies Bam Thwok Review | accessdate=2006-09-03 | publisher=BBC Music | archiveurl=https://web.archive.org/web/20060313225651/http://www.bbc.co.uk/derby/music/reviews/singles/pixies_bam_thwok_review.shtml | archivedate=2006-03-13}}</ref> Band members noticed the change in output; [[David Lovering]], the Pixies' drummer, commented that "it's very unlike us. It's a Pixies song but it's still unlike a Pixies song".<ref>Frank, Ganz, 2005. p. 209</ref>

==References==
{{Reflist|30em}}

===Cited text===
* Frank, Josh; Ganz, Caryn. ''[[Fool the World: The Oral History of a Band Called Pixies]]''. Virgin Books, 2005. ISBN 0-312-34007-9.

{{Pixies}}
{{Featured article}}

[[Category:2004 singles]]
[[Category:Pixies (band) songs]]
[[Category:Songs written by Kim Deal]]
[[Category:2004 songs]]