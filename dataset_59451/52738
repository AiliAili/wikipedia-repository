{{Use dmy dates|date=February 2013}}
{{Infobox journal
| title = Respiratory Research
| cover =[[File:RRlogo.gif]]
| editor = Jan Lötvall, Reynold A. Panettieri Jr.
| discipline = [[Pulmonology]]
| former_names =
| abbreviation = Respir. Res.
| publisher = [[BioMed Central]]
| country =
| frequency = Upon acceptance
| history = 2000-present
| openaccess = Yes
| license =
| impact = 3.642
| impact-year = 2012
| website = http://respiratory-research.com/
| link1 = http://respiratory-research.com/content
| link1-name = Online access
| link2 = http://respiratory-research.com/archive
| link2-name = Online archive
| JSTOR =
| OCLC = 47218396
| LCCN = 2001243154
| CODEN = RREEBZ
| ISSN = 1465-9921
| eISSN = 1465-993X
| boxwidth =
}}
'''''Respiratory Research''''' is an [[open access]] [[peer-reviewed]] [[medical journal]] published by [[BioMed Central]]. It covers all aspects of [[respiratory disease]], including clinical and basic research. The journal publishes research articles, commentaries, [[Letter to the editor|letters to the editor]], and [[review journal|reviews]].<ref>{{cite web |url=http://respiratory-research.com/authors/instructions |title=Instructions for Authors |publisher=BioMed Central |work=Respiratory Research |accessdate=2011-01-03}}</ref>

==Abstracting and indexing ==
The journal is abstracted and indexed in [[PubMed]]/[[MEDLINE]],<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101090633 |title=Respiratory Research |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-04-16}}</ref> [[Chemical Abstracts Service]], [[CAB International]], [[Current Contents]],<ref>{{cite web |url=http://www.biomedcentral.com/info/libraries/indexing |title=BioMed Central &#124; For libraries &#124; Journal indexing |format= |work= |accessdate=2011-01-03}}</ref> and the [[Science Citation Index Expanded]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-04-16}}</ref> According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 3.642, ranking it 8th out of 50 journals in the category "Respiratory System".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Respiratory System |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://respiratory-research.com/}}

[[Category:Pulmonology journals]]
[[Category:Publications established in 2000]]
[[Category:BioMed Central academic journals]]
[[Category:English-language journals]]
[[Category:Creative Commons Attribution-licensed journals]]