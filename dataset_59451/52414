{{Infobox journal
| title         = New Orleans Review
| cover         = 
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = 
| discipline    = <!-- or |subject= -->
| peer-reviewed = 
| language      = 
| editor        = <!-- or |editors= -->
| publisher     = The Walker Percy Center for Writing & Publishing and the Department of English at [[Loyola University New Orleans|Loyola University]]
| country       = United States
| history       = 
| frequency     = Biannually
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| ISSNlabel     =
| ISSN          = 0028-6400
| eISSN         =
| CODEN         =
| JSTOR         = 
| LCCN          = 
| OCLC          = 435982137
| website       =
| link1         =
| link1-name    =
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}

'''''New Orleans Review''''', founded in 1968,<ref name="Larson2013">{{cite book|author=Susan Larson|title=The Booklover's Guide to New Orleans|url=https://books.google.com/books?id=Lihu1O7B_0kC&pg=PA1992|date=5 September 2013|publisher=LSU Press|isbn=978-0-8071-5309-3|pages=1992–}}</ref> is a journal of contemporary literature and culture that publishes "poetry, fiction, nonfiction, art, photography, film and book reviews"<ref name="neworleansreview">http://neworleansreview.org/about/</ref> by established<ref name="FloraMacKethan2002">{{cite book|author1=Joseph M. Flora|author2=Lucinda Hardwick MacKethan|author3=Todd W. Taylor|title=The Companion to Southern Literature: Themes, Genres, Places, People, Movements, and Motifs|url=https://books.google.com/books?id=rl5_5u3tiRkC&pg=PA638|date=January 2002|publisher=LSU Press|isbn=978-0-8071-2692-9|pages=638–}}</ref> and emerging writers and artists. ''New Orleans Review'' is a publication of The Walker Percy Center for Writing & Publishing and the Department of English at [[Loyola University New Orleans|Loyola University]] New Orleans.

''New Orleans Review'' is published biannually and is distributed nationally and internationally by [[Ingram Content Group|Ingram Periodicals]]. Work published in ''New Orleans Review'' has been reprinted in anthologies such as the [[Pushcart Prize|Pushcart Prize Anthology]], [[Best American Nonrequired Reading]], [[New Stories From the South]], [[Utne Reader]], Poetry Daily, Verse Daily, and [[O. Henry Prize|O. Henry Prize Stories]]. In 1978 the journal published an excerpt from ''[[Confederacy of Dunces]]'' by [[John Kennedy Toole]] with a foreword by [[Walker Percy]], who was a contributing editor to the magazine. The novel was subsequently published in 1980 by [[LSU Press]] and was awarded the Pulitzer Prize in 1981. ''New Orleans Review'' published a critically acclaimed special issue on New Orleans by New Orleans writers and photographers in 2006 in the wake of [[Hurricane Katrina]], which [[Tony D'Souza]] wrote in [[Salon magazine|''Salon'']] is "a post-Katrina issue that avoids easy responses to the disaster, withholds simple prognoses for the future, and inhabits its moment of most-relevance so surely that its collective voice rises high above the din."<ref>http://www.salon.com/writer/tony_dsouza</ref>

==History==
''New Orleans Review'' was founded in 1968 by [[John William Corrington]] and [[Miller Williams]]<ref name="Larson2013" /> at Loyola University.<ref>Flora, Joseph M., Lucinda Hardwick. MacKethan, and Todd W. Taylor. "Louisiana, Literature Of." ''The Companion to Southern Literature: Themes, Genres, Places, People, Movements, and Motifs''. Baton Rouge: Louisiana State UP, 2002. 461. Print. The journal was published quarterly until 2000 and has since been published biannually.</ref>

Editors:
* [[Miller Williams]] (1968–1970)
* Joseph A. Tetlow (1970–1972)
* Forrest L. Ingram (1972–1973)
* John F. Christman (1974)
* Marcus Smith (1974–1978)
* Dawson Gaillard (1978–1979)
* [[Bruce Henricksen]] (1980–1986)
* [[John Biguenet]] (1980–1992)
* [[John Mosier]] (1980–1992)
* Ralph Adamo (1994–1999)
* Sophia Stone (1999–2000)
* Christopher Chambers (2000–2012)<ref name="neworleansreview" />

==Notable contributors==
{|
|-
|
* [[Walker Percy]]
* [[Pablo Neruda]]
* [[Ellen Gilchrist]]
* [[Nelson Algren]]
* [[Hunter S. Thompson]]
* [[John Kennedy Toole]]
* [[Richard Brautigan]]
* [[Barry Spacks]]
* [[Jimmy Carter]]
* [[James Sallis]]
* [[Jack Gilbert]]
* [[Paul Hoover]]
* [[Rodney Jones (poet)|Rodney Jones]]
* [[Annie Dillard]]
* Everette Maddox
* [[Julio Cortázar|Julio Cortazar]]
* [[Gordon Lish]]
* [[Robert Walser (writer)|Robert Walser]]
||
* [[Mark Halliday]]
* [[Jack Butler (author)|Jack Butler]]
* [[Michael S. Harper|Michael Harper]]
* [[Joyce Carol Oates]]
* [[Diane Wakoski]]
* [[Dermot Bolger]]
* [[Roddy Doyle]]
* [[William Kotzwinkle]]
* [[Alain Robbe-Grillet]]
* [[Arnošt Lustig|Arnost Lustig]]
* [[Raymond Queneau]]
* [[Yusef Komunyakaa]]
* [[Michael A. Martone|Michael Martone]]
* [[Tess Gallagher]]
* [[Matthea Harvey]]
* [[D. A. Powell]]
* [[Rikki Ducornet]]
* [[Ed Skoog]].
|}<ref name="neworleansreview" />

==The Walker Percy Fiction Contest==
The ''New Orleans Review'' publishes the winner of the annual Walker Percy Fiction Contest, established in 2011.

* 2011 winner: “Prisoners of the Multiverse” by Jacob Appel; runner-up: “War Story” by Austin Wilson. judge: Nancy Lemann.

==References==
{{reflist}}
*http://www.newpages.com/literary-magazines/new_orleans_review.htm
*Flora, Joseph M., Lucinda Hardwick. MacKethan, and Todd W. Taylor. "Louisiana, Literature Of." ''The Companion to Southern Literature: Themes, Genres, Places, People, Movements, and Motifs''. Baton Rouge: Louisiana State UP, 2002. 461. Print.
*http://www.salon.com/writer/tony_dsouza/

==External links==
*{{official website|http://neworleansreview.org/}}
*
[[Category:American literary magazines]]
[[Category:Biannual magazines]]
[[Category:Magazines established in 1968]]
[[Category:Magazines published in Louisiana]]
[[Category:Media in New Orleans]]