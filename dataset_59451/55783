{{Infobox journal
| title = Australian Archaeology
| cover =
| editor = Heather Burke, Lynley Wallis
| discipline = [[Archaeology]]
| former_names =
| abbreviation = Aust. Archaeol.
| publisher = [[Australian Archaeological Association]]
| country = Australia
| frequency = Biannually
| history = 1974-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.australianarchaeology.com/our-journal/
| link1 = http://www.library.uq.edu.au/ojs/index.php/aa/issue/current
| link1-name = Online access
| link2 = http://www.australianarchaeology.com/our-journal/journalcontents/
| link2-name = Online archive
| link3 = http://www.library.uq.edu.au/ojs/index.php/aa/index
| link3-name = University of Queensland, online archive (last 5 years)
| ISSN = 0312-2417
| eISSN =
| LCCN = sn87015931
| OCLC = 762014461
}}
'''''Australian Archaeology''''' is a biannual [[Peer review|peer reviewed]] [[academic journal]] published by the [[Australian Archaeological Association]]. It was established in 1974 and covers all fields of [[archaeology]] as well as other subjects that are relevant to archaeological research and practice in Australia and nearby areas. The journal uses a broad definition of archaeology to include prehistoric, historic, and contemporary periods, and includes [[social anthropology|social]], [[biological anthropology|biological]], and [[cultural anthropology]], history, [[Aboriginal studies]], [[environmental science]], and other related areas. The [[editors-in-chief]] are Heather Burke ([[Flinders University]]) and Lynley Wallis (Wallis Heritage Consulting).

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref>[http://www.nla.gov.au.rp.nla.gov.au/apais/journals.html Australian Public Affairs Information Service] {{subscription}}</ref><ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-10-28}}</ref><ref name=Scopus>{{cite web|url=http://cdn.elsevier.com/assets/excel_doc/0003/148548/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work= |accessdate=2013-10-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20131202041814/http://cdn.elsevier.com:80/assets/excel_doc/0003/148548/title_list.xlsx |archivedate=2013-12-02 |df= }}</ref>
{{columns-list|colwidth=30em|
* Australian Public Affairs Information Service
* [[Arts and Humanities Citation Index]]
* [[Scopus]]
* [[Anthropological Literature]]
* [[Anthropological Index Online]]
* [[Social Sciences Citation Index]]
* [[Current Contents]]/Social & Behavioral Sciences
* Current Contents/Arts & Humanities
* [[The Zoological Record]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.australianarchaeology.com/our-journal/}}

{{DEFAULTSORT:Australian Archaeology}}
[[Category:Archaeology journals]]
[[Category:Biannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1974]]
[[Category:Historiography of Australia]]
[[Category:Academic journals published by learned and professional societies]]


{{archaeology-journal-stub}}