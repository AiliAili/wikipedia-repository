{{Infobox award
| name           = AGS Awards
| current_awards = 
| image          = 
| imagesize      = 
| caption        = 
| description    = Outstanding [[Adventure Game Studio|AGS]] [[video game|games]] and [[video game developer|game developers]]
| presenter      = [[Adventure Game Studio]]
| country        = 
| location       = [[United Kingdom]]
| year           = 2001
| year2          = 
| website        = [http://www.adventuregamestudio.co.uk Adventure Game Studio]
}}

The '''AGS Awards''' are a set of annual awards for [[video games]] made with the [[Adventure Game Studio]] (acronym '''AGS''') engine to recognize the best [[graphic adventure game]]s of the year as assessed by AGS video game developers and artists. The awards show celebrates the best developers and achievements in the AGS video-games community, and features top gaming choices by creators and artists. Categories for the awards may vary year from year but overall include Best Game, Best Writing, Best Animation, Best Voice Acting, Best Puzzles, Best Gameplay and Best Music.

== History ==
{{see also|Adventure Game Studio}}
The AGS Awards were founded in 2001 and are awarded annually to the best indie [[point-and-click]] [[adventure]] games of the year. Categories include ''Best Game'', ''Best Writing'', ''Best Animation'', ''Best Voice Acting'', ''Best Puzzles'', ''Best Background Art'', ''Best Gameplay'' and ''Best Music''. The AGS Awards are an important barometer for indie adventure games, receiving regular international coverage by sites like [[Destructoid]].,<ref>{{cite web|url=http://www.destructoid.com/aaamaazzing-it-hurts-199927.phtml |title=Destructoid revies AGS Winners |publisher=[[Destructoid]] |date=2011-04-29 |accessdate=2011-04-29}}</ref> [[Rock, Paper, Shotgun]],<ref>{{cite web|url=http://www.rockpapershotgun.com/2011/02/07/ags-awards-2010-down-to-finalists/amp/  |title=AGS Awards 2010 down to finalists |publisher=[[Rock, Paper, Shotgun]] |date=2011-02-07 |accessdate=2017-02-09}}</ref><ref>{{cite web|url=http://www.rockpapershotgun.com/2013/02/25/point-click-vote-the-ags-awards/amp/ |title=Point-click-Vote for the AGS Awards |publisher=[[Rock, Paper, Shotgun]] |date=2013-02-25 |accessdate=2013-02-25}}</ref> IndiesGames<ref>{{cite web|url=http://www.indiegames.com/2010/01/indepth_the_ags_awards_2009.html |title=In-Depth: The AGS Awards 2009 |publisher=www.indiegames.com |date=2010-01-26 |accessdate=}}</ref><ref>{{cite web|url=http://indiegames.com/2013/03/the_2012_ags_awards.html |title=The 2012 AGS Awards |publisher=www.indiegames.com |date=2013-03-28 |accessdate=}}</ref> and [http://www.gamesetwatch.com/ GameSetWatch].<ref>{{cite web|url=http://www.gamesetwatch.com/2006/03/adventurers_get_ags_awards_int.php |title=Adventurers get AGS Awards |publisher=gamesetwatch.com |date=2006-03-03 |accessdate=}}</ref><ref>{{cite web|url=http://www.gamesetwatch.com/2007/02/ags_awards_zeebys_get_winners.php  |title=AGS Awards 2007 Winners |publisher=gamesetwatch.com |date=2007-02-02 |accessdate=}}</ref> Some winners have become immensely popular and commercially successful, such as ''[[Primordia (video game)|Primordia]]'' by Wormwood Studios,<ref>{{cite web|url=http://www.adventuregamers.com/articles/view/23235/page4 |title=Primordia - Wormwood Studios interview|publisher=adventuregamers.com |date= |accessdate=2012-11-13}}</ref><ref>{{cite web|url=http://www.indiegamebundles.com/primordia/ |title=Primordia review|publisher=www.indiegamebundles.com |date= |accessdate=2013-01-13}}</ref><ref>{{cite web|url=http://indiegames.com/2013/03/the_2012_ags_awards.html |title=The 2012 AGS Awards |publisher=www.indiegames.com |date=2013-03-18 |accessdate=}}</ref> ''[[Resonance (game)|Resonance]]'' by XII Games<ref>[http://kotaku.com/5919787/brand+new-indie-game-resonance-is-the-best-1990s-adventure-game-ive-played-in-years "Brand-New Indie Game ''Resonance'' is the Best 1990s Adventure Game I’ve Played in Years"] kotaku.com</ref> and ''[[Gemini Rue]]'' by Joshua Nuernberger.<ref>{{cite web|url=http://www.pcgamer.com/review/gemini-rue-review/ |title=Gemini Rue review |publisher=[[PC Gamer]] |date=2013-10-26 |accessdate=2013-11-04}}</ref> Serving as a springboard for talent.

In 2006 the founders of the AGS Awards announced the nominees and winners on the website [http://www.americangirlscouts.org www.americangirlscouts.org]. Playing into the communities running joke of claiming to be the American Girl Scouts.

In 2014, after considerable discussion and voting, the AGS Awards revamped the categories to reflect the community's increasing interest in variation.<ref>[http://www.indiegamenews.com/2014/05/ags-awards-category-reshuffle.html AGS Awards 2014 Category Reshuffle] Indie Game News</ref> Introducing a new category structure:

'''Overall awards''': ''Best Game Created with AGS'', ''Best Gameplay'', ''Best Writing'', ''Best Puzzles'', ''Best Character''. '''Category awards''': ''Best Short Game'', ''Best Demo'', ''Best Non-Adventure Game Created with AGS''. '''Sound awards''': ''Best Music & Sound'', ''Best Voice Acting''. '''Technical awards''': ''Best Background Art'', ''Best Character Art'', ''Best Animation'', ''Best Programming''. '''Committee decided''': ''Lifetime Achievement'', ''Best Innovation''.

== Competition Award History ==

The game developer with the most AGS Awards won in total is [[Benjamin Croshaw]], with 19 awards won and totaling 44 nominations. The most awards won by any one game was 12 for [[Resonance (game)|Resonance]].<ref>[http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2012 AGS Awards 2012 Results] adventuregamestudio.co.uk</ref>

===2016 Awards===
The 2016 nominees were announced on February 6, 2017 via the official website. The videogame with the most nominations was [[Shardlight]] by [[Wadjet Eye Games]] with a total of 12 nominations, including ''Best Game Created with AGS''. Wormwood Studio's [[Until I Have You]] was nominated in 9 categories and [[Tales (video game)|Tales]], which won an AGS Award in 2014 for ''Best Tech Demo'',<ref>[http://www.indiegamenews.com/2015_03_01_archive.html Best Demo crown for Tales] www.indiegames.com</ref><ref>[https://www.adventuregamestudio.co.uk/site/games/game/1793/ AGS Award - Best Tech Demo for Tales, 2014] adventuregamestudio.co.uk</ref> was nominated in 7 categories. Double nominees for ''Best Voice Acting'' include [[actor]] [[Vincent van Ommen]],<ref>[http://www.imdb.com/name/nm2894092/ Vincent van Ommen double nomination AGS Awards] imdb.com</ref> known for [[The Dark Knight Rises]] with ''[[Until I Have You]]'' and ''[[Tales (video game)|Tales]]'', and [[actress]] Shelly Shenoy,<ref>[http://www.imdb.com/name/nm4544708/ Shelly Shenoy videogame credits] imdb.com</ref> known for [[The Walking Dead: A New Frontier]] with ''[[Shardlight]]'' and ''Kathy Rain''.

{|  class="wikitable hlist"
|-
!style="width:25%"|Award
!style="width:80%"|Nominees<ref>{{cite web | url=http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2016 |title=AGS Awards 2016: The Full List of Video Game Award Nominees | work=adventuregamestudio.co.uk | last= | first=}}</ref>
|-
|Best Game Created with AGS
| 
*''Kathy Rain'' by Clifftop Games
*''[[Shardlight]]'' by [[Wadjet Eye Games]]
*''[[Tales (video game)|Tales]]'' by Screen7
*''Toffee Trouble in Creamville'' by Miez and Cat
*''[[Until I Have You]]'' by Wormwood Studios<ref>{{cite web | url=https://steamcommunity.com/games/untilihaveyou/announcements  | title=9 AGS Awards Nominations for Unit I Have You | work= steamcommunity.com | last= | first=}}</ref>
|-
|Best Freeware Game Created with AGS
|
*Among Thorns, Sniper and spotter being patriotic, [[Stair Quest]], The Man From Fugue State, Toffee Trouble in Creamville
|-
|Best Gameplay
| 
* Kathy Rain, [[Shardlight]], [[Stair Quest]], [[Tales (video game)|Tales]], [[Until I Have You]]
|-
|Best Animation
| 
*Kathy Rain, [[Shardlight]], Tales, The Journey of Iesir Demo, [[Until I Have You]]
|-
|Best Music and Sound
| 
*Kathy Rain, [[Shardlight]], [[Stair Quest]], The Journey of Iesir Demo, [[Until I Have You]]
|-
|Best Voice Acting
| 
*Kathy Rain ''(Arielle Siegel, Shelly Shenoy)''
*Order of the Thorne : The King's Challenge ''(Jesse Lowther, Matthew Curtis)''
*[[Shardlight]] ''([[Shelly Shenoy]], [[Abe Goldfarb]])''
*Tales ''(Alasdair Beckett-King, [[Vincent van Ommen]]<ref>{{cite web | url=http://denhaagfm.nl/2017/02/07/haagse-acteur-vincent-van-ommen-twee-keer-genomineerd-voor-internationale-prijs/ | title=Haagse acteur Vincent van Ommen twee keer genomineerd voor internationale prijs | work=denhaagfm | last= | first=}}</ref>)''
*[[Until I Have You]] ''([[Vincent van Ommen]],<ref>{{cite web | url=http://www.noordhollandsdagblad.nl/algemeen/cultuur/entertainment/article29000147.ece/Nederlandse-acteur-maakt-kans-op-stemprijzen | title=Nederlandse acteur Vincent van Ommen maakt kans op stemprijzen | work=noordhollands dagblad | last= | first=}}</ref> Marissa Lenti)''
|-
|Best Programming
| 
*Kathy Rain, Monkeys to the Moon!, Shardlight, [[Stair Quest]], [[Until I Have You]]
|-
|Best Character
| 
* Kathy Rain ''(Kathy Rain)'', Shardlight ''(Amy Wellard)'', Tales ''(The Cheese Seller)'', The Journey of Iesir Demo ''(Bjorn)'', Toffee Trouble in Creamville ''(Toffee)''
|-
|Best Puzzles
| 
* Kathy Rain, Shardlight, Tales, The Journey of Iesir Demo, The Man From Fugue State
|-
|Best Non Adventure Game Created with AGS
| 
*Columbus Lander, Monkeys to the Moon!, Predators Prey For Plants, Space Tunneler, [[Until I Have You]]
|-
|Best Short Game
| 
* Among Thorns, GNRBLEX, Graveyard, [[Stair Quest]], Toffee Trouble in Creamville
|-
|Best Writing
| 
* Fallen soldier, Kathy Rain, Shardlight, Tales, The Journey of Iesir Demo
|-
|Best Character Art
| 
*Kathy Rain, Shardlight, The Journey of Iesir Demo, Toffee Trouble in Creamville, [[Until I Have You]]
|-
|Best Background Art
| 
*Kathy Rain, Shardlight, The Journey of Iesir Demo, Toffee Trouble in Creamville, [[Until I Have You]]
|-
|}

===2015 Awards===
In 2015, [[Technobabylon]] won the most awards of all the nominees, taking home 9. ''Troll Song: Verse One - Completely Stoned'' won the Best Demo Awards at the AGS Awards 2015. This was the second time Troll Song has won an AGS Award for ''Best Tech Demo'', with the original tech demo claiming the prize at the 2013 AGS Awards.<ref>[http://www.troll-song.com/2016/06/troll-song-wins-best-demo-at-ags-awards.html Troll Song wins AGS Award 2015]</ref> The '''Lifetime Achievement Award''' was awarded to indie developer Peder Johnsen ''"for his services with AGS archives, hosting and many other contributions over the years"''.

{|  class="wikitable hlist"
|-
!style="width:25%"|Award
!style="width:40%"|Winners<ref>{{cite web | url=http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2015 |title=AGS Awards 2015: The Full List of Video Game Award Nominees | work=adventuregamestudio.co.uk | last= | first=}}</ref>
|-
|Best Game Created with AGS
| 
*''[[Technobabylon]]'' by [[Wadjet Eye Games]]
|-
|Best Gameplay
| 
*[[Technobabylon]]
|-
|Best Animation
| 
*[[Technobabylon]]
|-
|Best Music and Sound
| 
*[[Technobabylon]]
|-
|Best Voice Acting
| 
*[[Technobabylon]]
|-
|Best Programming
| 
*Rogue State
|-
|Best Character
| 
*The Jimi Hendrix Case
|-
|Best Puzzles
| 
*[[Technobabylon]]
|-
|Best Freeware Game Created with AGS
|
*Camp 1
|-
|Best Non Adventure Game Created with AGS
| 
*Rogue State
|-
|Best Writing
| 
*[[Technobabylon]]
|-
|Best Character Art
| 
*[[Technobabylon]]
|-
|Best Background Art
| 
*[[Technobabylon]]
|-
|Best Demo
|
* Troll Song: Verse One - Completely Stoned<ref>[http://gamejolt.com/games/troll-song-verse-one/16925/devlog/troll-song-wins-best-demo-at-the-ags-awards-2015-ttrkpm3z Troll Song wins AGS Award] www.gamejolt.com</ref>
|-
|}

===2014 Awards===
In 2014, the [[Role-playing video game|RPG]] game ''Quest for Infamy'' won in 4 categories including ''Best Gameplay'' and ''Best Puzzles''. After the award ceremony the developers of Quest for Infamy announced they'd making a prequel and a sequel to the game.<ref>[https://www.gamingonlinux.com/articles/developer-infamous-%20quests-has-announced-two-new-pointandclick-adventures-to-be-%20released-this-year.5169 Developer Infamous Quests Has Announced Two New Point-And-Click Adventures] gamingonlinux.com</ref>

{|  class="wikitable hlist"
|-
!style="width:25%"|Award
!style="width:40%"|Winners<ref>{{cite web | url=http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2014 |title=AGS Awards 2014: The Full List of Video Game Award Nominees | work=adventuregamestudio.co.uk | last= | first=}}</ref>
|-
|Best Game Created with AGS
| 
*''[[Blackwell Epiphany]]'' by [[Wadjet Eye Games]]
|-
|Best Gameplay
| 
*Quest for Infamy
|-
|Best Animation
| 
*[[Blackwell Epiphany]]
|-
|Best Music and Sound
| 
*Quest for Infamy
|-
|Best Voice Acting
| 
*[[Blackwell Epiphany]]
|-
|Best Programming
| 
*Red Hot Overdrive
|-
|Best Character
| 
*Quest for Infamy
|-
|Best Puzzles
| 
*Quest for Infamy
|-
|Best Short Game
|
*A Date in the Park
|-
|Best Non Adventure Game Created with AGS
| 
*Red Hot Overdrive
|-
|Best Writing
| 
*[[Blackwell Epiphany]]
|-
|Best Character Art
| 
*[[Blackwell Epiphany]]
|-
|Best Background Art
| 
*[[Blackwell Epiphany]]
|-
|Best Demo
|
*[[Tales (video game)|Tales]] - ''Tech Demo''
|-
|Best Freeware Game Created with AGS
|
*Mudlarks
|- 
|}

===2013 Awards===
In 2013, ''[[Heroine's Quest: The Herald of Ragnarok]]'' developed Crystal Shard by won a total of 11 AGS Awards.<ref>[http://www.indiedb.com/games/heroines-quest/news/hq-wins-eleven-ags-awards Heroine's Quest wins 11 AGS Awards] www.indiedb.com</ref> A month later the game was published on [[Valve Steam]].<ref>[http://www.indiegamenews.com/2014/03/heroines-quest-now-available-on-steam.html Heroines Quest now on Steam] indiegamenews.com</ref>

{|  class="wikitable hlist"
|-
!style="width:25%"|Award
!style="width:40%"|Winners<ref>{{cite web | url=http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2013 |title=AGS Awards 2013: The Full List of Video Game Award Nominees | work=adventuregamestudio.co.uk | last= | first=}}</ref>
|-
|Best Game Created with AGS
| 
*''[[Heroine's Quest: The Herald of Ragnarok]]'' by Crystal Shard
|-
|Best Gameplay
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Animation
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Music and Sound Effects
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Voice Acting
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Programming
| 
*The Art of Dying
|-
|Best Player Character
| 
*The Bum
|-
|Best Non-player Character
|
*Educating Adventures of Girl and Rabbit
|-
|Best Puzzles
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Short Game
|
*Time Stone
|-
|Best Non Adventure Game Created with AGS
| 
*The Art of Dying
|-
|Best Dialogue Writing
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Character Art
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Background Art
| 
*Heroine's Quest: The Herald of Ragnarok
|-
|Best Demo
|
*Troll Song - Verse One - ''Tech Demo''
|-
|}

===2012 Awards===
In 2012, ''[[Resonance (game)|Resonance]]'' developed by [[Wadjet Eye Games]] won 12 AGS Awards. Setting a record for most AGS Awards ever won by a single game.

{|  class="wikitable hlist"
|-
!style="width:25%"|Award
!style="width:40%"|Winners<ref>{{cite web | url=http://www.adventuregamestudio.co.uk/wiki/AGS_Awards_2012 |title=AGS Awards 2012: The Full List of Video Game Award Nominees | work=adventuregamestudio.co.uk | last= | first=}}</ref><ref>[http://indiegames.com/2013/03/the_2012_ags_awards.html The 2012 AGS Awards] indiegames.com</ref>
|-
|Best Game Created with AGS
| 
*''[[Resonance (video game)|Resonance]]'' by [[Wadjet Eye Games]]
|-
|Best Gameplay
| 
*Resonance
|-
|Best Animation
| 
*Resonance
|-
|Best Music and Sound Effects
| 
*Resonance
|-
|Best Voice Acting
| 
*Resonance
|-
|Best Original Story
|
*Resonance
|-
|Best Programming
| 
*Resonance
|-
|Best Player Character
| 
*[[Ben Jordan: Paranormal Investigator|Ben Jordan Paranormal Investigator Case 8: Relics of the Past]]
|-
|Best Non-player Character
|
*Ben Jordan Paranormal Investigator Case 8: Relics of the Past
|-
|Best Puzzles
| 
*Resonance
|-
|Best Short Game
|
*^_^
|-
|Best Non Adventure Game Created with AGS
| 
*RAM Ghost [BAKE SALE 2012]
|-
|Best Dialogue Writing
| 
*Resonance
|-
|Best Character Art
| 
*Resonance
|-
|Best Background Art
| 
*Resonance
|-
|Best Demo
|
*Captain Disaster in Death Has A Million Stomping Boots - ''Demo''
|-
|}

==References==
{{Reflist|refs=|30em}}

== External links ==
*{{Official website|http://adventuregamestudios.co.uk/}}
*[https://www.adventuregamestudio.co.uk/site/games/awards/ List of all AGS Award winners and nominees]

[[Category:Indie video games]]
[[Category:Video game awards]]
[[Category:Awards established in 2001]]
[[Category:2001 establishments in the United Kingdom]]