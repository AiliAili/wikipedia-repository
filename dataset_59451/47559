{{Infobox roller derby league
| name = Dolly Rockit Rollers
| founded = 10 January 2010
| teams = All-Stars (A team)<br> Raggy Dollz (B team)<br> Foxton Blocks (Intraleague team)<br> Angry Belvoirs (Intraleague team)
| metro area = [[Leicester]]
| country = [[United Kingdom]]
| logo = DRR_red_logo_transparent_bg_2012.png
| tracks = Flat
| affiliations = [[UKRDA]], [[Women's Flat Track Derby Association|WFTDA]]
| url = http://www.dollyrockitrollers.co.uk
}}

{{Use dmy dates|date=August 2013}}
{{Use British English|date=April 2012}}
The '''Dolly Rockit Rollers''' (“DRR”) are an [[England|English]] [[roller derby]] league based in [[Leicestershire]]. Formed in January 2010,<ref name=UKASF>{{cite web|url=http://www.ukamericansportsfans.com/introducing-the-dolly-rockit-rollers |title=Introducing The Dolly Rockit Rollers |publisher=UK American Sports Fans.com |date=2012-03-27 |accessdate=2012-04-04}}</ref> they skate flat track roller derby to [[WFTDA]] rules, and have been a member of [[UKRDA]]<ref>{{cite web|url=http://ukrda.org.uk/?p=167 |title=Welcome Dolly Rockit Rollers |publisher=UKRDA |date=2011-09-13 |accessdate=2012-04-03}}</ref> since September 2011 and a [[WFTDA]]<ref>{{cite web|url=http://www.wftda.com/news/wftda-welcomes-16-new-member-leaguesl }}</ref> league since July 2014. The league is entirely owned and run by its members, and was the first roller derby league to operate in Leicestershire.<ref name=UKASF/>

==History==
DRR were formed by skaters Slamabama and Nitro Noush, who were both skating with Birmingham’s [[Central City Rollergirls]]<ref>{{cite web|url=http://roundupbout.blogspot.co.uk/2009/11/leicester-girls-become-dolly-rockit.html |title=Leicester Girls Become Dolly Rockit Rollers |publisher=The Round-Up Bout |date=2009-11-09 |accessdate=2012-04-04}}</ref> but looking to set up a league in their home city of Leicester at the time. In conjunction with Central City they organised a practice in the Beaumont Leys area of the city and were encouraged by the turnout it received. Leicester has a long affinity with roller skating, with the Granby Halls roller rink widely popular until its closure in 1999,<ref>{{cite web|url=http://www.leicesterchronicler.com/granby.htm |title=Granby Halls |publisher=The Leicester Chronicler |year=2004 |accessdate=2012-04-04}}</ref> and this provided a solid basis for member numbers to increase over the following months. The fledgling league was named the Dolly Rockit Rollers, both as a pun on the term “rock it” and as an homage to Leicester’s [[National Space Centre]].

The first bout for DRR was a mixed exhibition game in August 2010, with their first interleague bout (a 79-68 loss to the [[Newcastle Roller Girls]]) taking place a month later. This bout also marked the league’s debut at their new home, with the Dollies moving from Beaumont Leys to the larger Parklands Leisure Centre at this time. The team would play two more interleague bouts in 2010, winning both of them to finish the year on a 2-1 record.<ref name=Euroderby>{{cite web|url=http://euroderby.org/scoring/ranking.php?year=2011 |title=Euroderby.org 2011 Rankings |publisher=Euroderby.org|accessdate=2012-04-04}}</ref>

Skater numbers continued to increase and by May 2011 DRR had enough advanced skaters to form a ‘B’ team, the Raggy Dollz. The DRR ‘B’ team made an unofficial debut in May 2011, skating under the name “Free Birds”, before playing their first official bout against the [[Birmingham Blitz Dames]]’ ‘B‘ team in September of that year. The Dollies were admitted into UKRDA, the governing body for UK roller derby, shortly after this bout.

DRR’s Allstars ‘A’ team played a total of twelve public bouts in 2011, with nine wins in total.<ref name=Euroderby /> This record saw them voted amongst the top eight members of the UKRDA and qualified them to play at the Tattoo Freeze 2012 tournament in January of the following year.<ref>{{cite web|url=http://ukrda.org.uk/?p=218 |title=2012 UKRDA Expo Tournament |publisher=UKRDA |date=2011-11-10 |accessdate=2012-04-04}}</ref> They would eventually be eliminated in the quarterfinals after a tense game against the Auld Reekie Rollergirls.

The Dollies also had two skaters selected to play at the inaugural [[Roller Derby World Cup]] in December 2011, as [[Team England (roller derby)|England]]’s Rogue Runner and [[All-Ireland Roller Derby|Ireland]]’s Holly Sheet were both drafted into their respective national teams.<ref>{{cite web|url=http://www.derbynewsnetwork.com/2011/08/team_england_2011_world_cup_roster |title=Team England 2011 World Cup Roster |publisher=Derby News Network |date=2011-08-10 |accessdate=2012-04-04}}</ref><ref>{{cite web|url=http://www.derbynewsnetwork.com/2011/09/world_cup_team_ireland_roster |title=World Cup Team Ireland Roster |publisher=Derby News Network |date=2011-09-13 |accessdate=2012-04-04}}</ref>

In October 2012, the Dollies were accepted as a member of the [[Women's Flat Track Derby Association Apprentice]] Programme.<ref>"[http://wftda.com/news/22-leagues-join-apprentice-program 22 Leagues Join WFTDA Apprentice Program] {{webarchive |url=https://web.archive.org/web/20130324201059/http://wftda.com/news/22-leagues-join-apprentice-program |date=24 March 2013 }}", WFTDA, 18 October 2012</ref>

In July 2014, DRR were accepted as a full member of the [[Women's Flat Track Derby Association Apprentice]] Programme.<ref>"[http://www.wftda.com/news/wftda-welcomes-16-new-member-leagues]", WFTDA, 3 July 2014</ref>

As the first [http://www.britishchamps.com/ British Championships]" came to the UK in 2015, the Dolly Rockit Rollers were placed into the 'Women T3 West' tier.

==Teams==
DRR currently field five teams, two interleague teams (who play against the interleague teams of other leagues) and three intraleague teams (who play each other exclusively).

*'''DRR All-Stars''' - DRR’s ‘A’ travel team. A 20 skater roster is picked for the Allstars every three months, which provides a pool of skaters for bouting teams to be picked from.
*'''DRR Raggy Dollz''' - DRR’s ‘B’ travel team. Again, a 20 skater roster is picked every three months, with limits on the number of "crossover" skaters (i.e. skaters who appear on both rosters in the same selection period) in accordance with UKRDA rules.
*'''Foxton Blocks''' - Intraleague team. Their name is a reference to [[Foxton Locks]], a popular Leicestershire tourist attraction.
*'''Angry Belvoirs''' - Intraleague team. Their name is a pun on the Leicestershire town of [[Belvoir, Leicestershire|Belvoir]] (pronounced “beaver”).
*'''Undercover Meerkats''' - Intraleague team. Their name is a pun on the Leicester's famous market.
==Bout History==
The following is a list of open-door interleague bouts played by the Dolly Rockit Rollers since their inception in 2010.

{| class="wikitable"
|----- bgcolor=#D46A6A
|'''Date'''
|'''Home/Away'''
|'''DRR Team'''
|'''Opponent'''
|'''Result <br><small>(DRR score shown first)</small>'''
|'''Bout name'''
|-
|------bgcolor=#F9F9F9
| 25 Sep 2010
| Home
| DRR All-stars
| [[Newcastle Roller Girls]]
| 68 - 79
| Block the Casbah
|-
|------bgcolor=#F9F9F9
| 23 Oct 2010
| Away
| DRR All-stars
| [[Granite City Roller Girls]]
| 82 - 56
| Jammerblocky
|-
|------bgcolor=#F9F9F9
| 20 Nov 2010
| Home
| DRR All-stars
| Wakey Wheeled Cats
| 109 - 76
| Helter Belter
|-
|------bgcolor=#F9F9F9
| 5 Feb 2011
| Home
| DRR All-stars
| Romsey Town Rollerbillies
| 129 - 126 (Overtime)
| Birthday Bash
|-
|------bgcolor=#F9F9F9
| 26 Feb 2011
| Away
| DRR All-stars
| Liverpool Roller Birds
| 111 - 68
| A Hard Days Fight
|-
|------bgcolor=#F9F9F9
| 2 April 2011
| Away
| DRR All-stars
| [[Hot Wheel Roller Derby]]
| 102 - 43
| Great Yorkshire Showdown
|-
|------bgcolor=#F9F9F9
| 2 April 2011
| Away
| DRR All-stars
| Manchester Roller Derby
| 52 - 83
| Great Yorkshire Showdown
|-
|------bgcolor=#F8D0D0
| 7 May 2011
| Away
| DRR Free Birds (B-team)
| Newcastle Roller Girls
| 64 - 44
| Sweet Home Alajammer
|-
|------bgcolor=#F9F9F9
| 7 May 2011
| Away
| DRR All-stars
| Newcastle Roller Girls
| 83 - 107
| Sweet Home Alajammer
|-
|------bgcolor=#F9F9F9
| 6 Aug 2011
| Home
| DRR All-stars
| [[Tiger Bay Brawlers]]
| 86 - 95
| School of Block
|-
|------bgcolor=#F9F9F9
| 10 Sep 2011
| Home
| DRR All-stars
| Hot Wheel Roller Derby
| 128 - 119
| Club TropiCarnage
|-
|------bgcolor=#F9F9F9
| 24 Sep 2011
| Away
| DRR All-stars
| [[Royal Windsor Rollergirls]]
| 161 - 63
| Unthemed
|-
|------bgcolor=#F8D0D0
| 1 Oct 2011
| Away
| DRR Raggy Dollz
| Dawn of the Dames
| 123 - 84
| Rolling Dead
|-
|------bgcolor=#F9F9F9
| 1 Oct 2011
| Away
| DRR All-stars
| [[Birmingham Blitz Dames]]
| 120 - 86
| Rolling Dead
|-
|------bgcolor=#F9F9F9
| 5 Nov 2011
| Home
| DRR All-stars
| Granite City Roller Girls
| 163 - 57
| Beats International
|-
|------bgcolor=#F9F9F9
| 18 Nov 2011
| Home
| DRR All-stars
| [[Lincolnshire Bombers Roller Girls]]
| 131 - 130
| Rollerburn
|-
|------bgcolor=#F9F9F9
| 17 Dec 2011
| Away
| DRR All-stars
| [[Rainy City Roller Girls]] Tender Hooligans
| 160 - 94
| Christmas Bonanza
|-
|------bgcolor=#F9F9F9
| 15 Jan 2012
| Home
| DRR All-stars
| [[Auld Reekie Roller Girls]]
| 60 - 100
| Tattoo Freeze
|-
|------bgcolor=#F9F9F9
| 11 Feb 2012
| Away
| DRR All-stars
| [[London Rockin Rollers]] Rising Stars
| 165 - 123
| Jammer Throw
|-
|------bgcolor=#F8D0D0
| 11 Feb 2012
| Home
| DRR Raggy Dollz
| SSRG Crucibelles
| 165 - 101
| Hit Her With Glitter
|-
|------bgcolor=#F9F9F9
| 11 Feb 2012
| Home
| DRR All-stars
| [[Sheffield Steel Rollergirls]]
| 131 - 196
| Hit Her With Glitter
|-
|------bgcolor=#F8D0D0
| 31 Mar 2012
| Away
| DRR Raggy Dollz
| Wakey Wheeled Cats
| 211 - 126
| Hoedown Showdown
|-
|------bgcolor=#F9F9F9
| 21 Apr 2012
| Home
| DRR All-stars
| Liverpool Roller Birds
| 308 - 124
| The Blocky Horror Picture Show
|-
|------bgcolor=#F9F9F9
| 5 May 2012
| Away
| DRR All-stars
| Big Bucks High Rollers
| 158 - 169 (Overtime)
| Cinco De Mayhem
|-
|------bgcolor=#F8D0D0
| 19 May 2012
| Home
| DRR Raggy Dollz
| [[The Norfolk Brawds Roller Derby|Norfolk Brawds Roller Derby]]
| 271 - 58
| Unthemed
|-
|------bgcolor=#F9F9F9
| 26 May 2012
| Away
| DRR All-stars
| Brighton Rockers Roller Derby
| 69 - 224
| A Right Royal Rumble
|-
|------bgcolor=#F9F9F9
| 9 Jun 2012
| Home
| DRR All-stars
| [[Helsinki Roller Derby]]
| 71 - 99
| Fight To The Finnish
|-
|------bgcolor=#F9F9F9
| 14 Jul 2012
| Home
| DRR All-stars
| Romsey Town Rollerbillies
| 151 - 120
| Unthemed
|-
|------bgcolor=#F9F9F9
| 4 Aug 2012
| Home
| DRR All-stars
| Blitz Dames
| 93 - 121
| Super Jamio
|-
|------bgcolor=#F9F9F9
| 8 Sept 2012
| Home
| DRR All-stars
| Royal Windsor
| 77 - 184
| SuperJammersGoBallistic…!
|-
|------bgcolor=#F8D0D0
| 29 Sept 2012
| Away
| Raggy Dollz
| Kent RGs
| 82 - 184
| Block n Roll High School
|-
|------bgcolor=#F9F9F9
| 13 Oct 2012
| Away
| DRR All-stars
| Tiger Bay Brawlers
| 30 - 496
| Un-named
|-
|------bgcolor=#F8D0D0
| 17 Nov 2012
| Home
| Raggy Dollz
| Portsmouth Roller Wenches
| 112 - 134
| My Big Fat Derby Wedding
|-
|------bgcolor=#F9F9F9
| 8 Dec 2012
| Away
| DRR All-stars
| Hot Wheel Roller Derby
| 75 - 296
| Un-named
|-
|------bgcolor=#F9F9F9
| 17 Feb 2013
| Home
| DRR All-stars
| Bristol Harbour Harlots
| 116 - 273
| Smack Down
|-
|------bgcolor=#F9F9F9
| 16 Mar 2013
| Away
| DRR All-stars
| One Love Roller Dolls
| 120 - 165
| Glam Block
|-
|------bgcolor=#F9F9F9
| 17 Mar 2013
| Away
| DRR All-stars
| GoGo Gent Roller Derby
| 74 - 300
| Closed door
|-
|------bgcolor=#F9F9F9
| 20 Apr 2013
| Home
| DRR All-stars
| Croydon
| 106 - 229
| Tank Grrls
|-
|------bgcolor=#F9F9F9
| 21 Apr 2013
| Away
| DRR All-stars
| Seaside Sirens
| 162 - 178
| Merby Opener
|-
|------bgcolor=#F9F9F9
| 11 May 2013
| Away
| DRR All-stars
| [[Middlesbrough Milk Rollers]]
| 112 - 326
| Un-named bout
|-
|------bgcolor=#F9F9F9
| 01 Jun 2013
| Home
| DRR All-stars
| Batter C Power 
| 103 - 308
| Tank Grrl
|-
|------bgcolor=#F9F9F9
| 13 Jul 2013
| Home
| DRR All-stars
| Brighton B
| 110 - 88
| Darbie Grrl
|-
|------bgcolor=#F9F9F9
| 07 Sept 2013
| Away
| DRR All-stars
| Sheffield Steel Roller Girls
| 90 - 314
| Un-named
|-
|------bgcolor=#F9F9F9
| 02 Nov 2013
| Home
| DRR All-stars
| Big Bucks High Rollers
| 264 - 71
| Blockamole
|-
|------bgcolor=#F9F9F9
| 23 Nov 2013
| Away
| DRR All-stars
| Portsmouth Roller Wenches
| 165 - 262
| Tash Bash!
|-
|------bgcolor=#F9F9F9
| 12 Jan 2014
| Away
| DRR All-stars
| Central City Roller Girls
| 28 - 372
| Tattoo Freeze 2014
|-
|------bgcolor=#F9F9F9
| 25 Jan 2014
| Home
| DRR All-stars
| Vienna Roller Girls
| 157 - 286
| Ultrablox
|-
|------bgcolor=#F9F9F9
| 22 Feb 2014
| Away
| DRR All-stars
| Tiger Bay Brawlers B
| 86 - 202
| Tiger Bay double header
|-
|------bgcolor=#F9F9F9
| 29 Mar 2014
| Home
| DRR All-stars
| Kent Roller Girls
| 119 - 177
| U Kent Touch This
|-
|------bgcolor=#F9F9F9
| 26 Apr 2014
| Away
| DRR All-stars
| Rainy City Roller Girls
| 138 - 103
| Un-named
|-
|------bgcolor=#F9F9F9
| 27 Apr 2014
| Away
| DRR All-stars
| Milton Keynes Concrete Cows
| 79 - 211
| Super Slam Down
|-
|------bgcolor=#F9F9F9
| 17 May 2014
| Home
| DRR All-stars
| London Rockin Rollers B
| 167 - 145
| Space Dinosaurs on Skates
|-
|------bgcolor=#F9F9F9
| 12 July 2014
| Away
| DRR All-stars
| Rebellion Roller Derby
| 251 - 135
| Unknown
|-
|------bgcolor=#F9F9F9
| 25 Oct 2014
| Away
| DRR All-stars
| Central City RG
| 85 - 202
| CCR Tournament
|-
|------bgcolor=#F9F9F9
| 31 Jan 2015
| Home
| DRR All-stars
| Rebellion Roller Derby
| 213 - 110
| Hunger Games
|-
|------bgcolor=#F9F9F9
| 1 Mar 2015
| Away
| DRR All-stars
| Hereford Roller Girls
| 123 - 133
| British Championships
|-
|------bgcolor=#F9F9F9
| 14 Mar 2015
| Home
| DRR All-stars
| Hell's Bells
| 188 - 109
| Pi Day Games
|-
|------bgcolor=#F9F9F9
| 29 Mar 2015
| Away
| DRR All-stars
| Belfast Roller Derby
| 147 - 297
| British Championships
|-
|------bgcolor=#F9F9F9
| 18 Apr 2015
| Away
| DRR All-stars
| Roller Derby Porto
| 140 - 189
| Porto Game
|-
|------bgcolor=#F9F9F9
| 16 May 2015
| Away
| DRR All-stars
| Swansea City Roller Derby
| 50 - 401
| British Championships
|-
|------bgcolor=#F9F9F9
| 27 Jun 2015
| Home
| DRR All-stars
| Birmingham Blitz Dames
| 42 - 400
| British Championships
|-
|------bgcolor=#F9F9F9
| 11 Jul 2015
| Away
| DRR All-stars
| RCRG Tender Hooligans
| 191 - 178
| Away game
|-
|}

<small>Last updated: 25 Jul 2012<br>Source: [https://web.archive.org/web/20120415140631/http://euroderby.org:80/scoring/ranking.php? Euroderby.org]</small>

==Sources==
{{reflist}}

==External links==
* [http://www.dollyrockitrollers.co.uk Official website]

{{UKRDA}}
{{WFTDA Apprentice leagues}}

[[Category:Roller derby leagues established in 2010]]
[[Category:Roller derby in England]]
[[Category:Roller derby leagues in the United Kingdom]]
[[Category:Women's sports teams in England]]
[[Category:Women's Flat Track Derby Association Apprentice]]