{{For|the [[Grandes écoles|Grande école]] formerly known as the Breguet School|École Supérieure d'Ingénieurs en Électronique et Électrotechnique{{!}}ESIEE}}
[[File:Louis Charles Breguet.jpg|250ăxpx|thumb|right|Louis Breguet, in 1909]]
[[File:Breguet Gyroplane 1907.jpg|thumb|[[Breguet-Richet Gyroplane]] (1907).[[Musée des Arts et Métiers]], [[Paris]].]]
[[File:Avion biplan Breguet 1911.jpg|thumb|1911 Breguet biplane aeroplane Type R.U1 No.40. [[Musée des Arts et Métiers]], [[Paris]].]]

'''Louis Charles Breguet''' (2 January 1880 in Paris – 4 May 1955 in Saint-Germain-en-Laye, France) was a [[France|French]] [[aircraft]] designer and builder, one of the early [[List of aviation pioneers|aviation pioneers]].

== Biography ==
Louis Charles Breguet was the grandson of [[Louis-Francois-Clement Breguet]], and great-great-grandson of the famous horologist [[Abraham-Louis Breguet]].

In 1902, Louis married Nelly Girardet, the daughter of painter [[Eugène Girardet]]. They had five children together.

In 1905, with his brother Jacques, and under the guidance of [[Charles Richet]], he began work on [[Breguet-Richet Gyroplane|a gyroplane]] (the forerunner of the [[helicopter]]) with flexible wings.  On 29 September 1907, it achieved the first ascent of a vertical-flight aircraft with a pilot, albeit only to a height of {{convert|0.6|m|ft}}. It was also not a free flight, as four men were used to steady the structure.

He built his first fixed-wing  aircraft, the [[Breguet Type I]], in 1909, flying it successfully before crashing it at the [[Grande Semaine d'Aviation]] held at Reims. In 1911, he founded the [[Breguet Aviation|Société anonyme des ateliers d’aviation Louis Breguet]]. In 1912, Breguet constructed his first [[seaplane|hydroplane]].

He is especially known for his development of reconnaissance aircraft used by the French in [[World War I]] and through the 1920s. One of the pioneers in the construction of metal aircraft, the [[Breguet 14]] single-engined day bomber, perhaps one of the most widely used French warplanes of its time, had an airframe constructed almost entirely of [[aluminium]] structural members. As well as the French, sixteen squadrons of the American Expeditionary Force also used it. A plane of this type has a major role in the plot of the 1927 thriller [[So Disdained]] by [[Nevil Shute]].

In 1919, he founded the [[Compagnie des Messageries Aériennes|Compagnie des messageries aériennes]], which evolved into [[Air France]].

Over the years, his aircraft set several records. A Breguet plane made the first nonstop crossing of the [[South Atlantic]] in 1927. Another made a {{convert|4,500|mi|km|adj=on}} flight across the Atlantic Ocean in 1933, the longest nonstop Atlantic flight up to that time.

He returned to his work on the gyroplane in 1935. Created with co-designer René Dorand, the craft, called the [[Gyroplane Laboratoire]], flew by a combination of blade flapping and feathering. On December 22, 1935, it established a speed record of 67&nbsp;mph (108&nbsp;km/h). It was the first to demonstrate speed as well as good control characteristics. The next year, it set an altitude record of 517&nbsp;feet (158&nbsp;m).

Breguet remained an important manufacturer of aircraft during [[World War II]] and afterwards developed commercial transports. [[Range (aeronautics)#Cruise/climb|Breguet’s range equation]], for determining aircraft [[range (aircraft)|range]], is also named after him.
He died of a heart attack{{Citation needed|date=August 2016}} in 1955 at [[Saint-Germain-en-Laye]].

== Olympic sailing ==
Breguet, as helmsmen of his 8&nbsp;metre yacht ''Namousa'', won a bronze medal in sailing during the [[Sailing at the 1924 Summer Olympics (8 metre class)|1924 Summer Olympics]].

==See also==
{{portal|Aviation}}
* [[Early Birds of Aviation]]

{{Clear}}

==References==
* {{cite book |last=Dick |first=Ron |authorlink= |author2=Dan Patterson |editor= |others= |title=Aviation Century: The Early Years |date=2003 |publisher=Boston Mills |location=Erin, Ontario |isbn=978-1-55046-407-8 |chapter=Great Names |page=208}}
* {{cite book |last=Marcel Miocque |first= |authorlink= |author2=Huguette Vernochet |author3=Alain Bertaud |author4=Lise Dassonville-Agron |editor= |others= |title=Houlgate entre mer et campagne |date=2001 |publisher=Éditions Charles Corlet |location= |isbn=978-2-85480-976-3 |chapter= |page=16}}

{{Authority control}}

{{DEFAULTSORT:Breguet, Louis Charles}}
[[Category:1880 births]]
[[Category:1955 deaths]]
[[Category:Aviation pioneers]]
[[Category:French aviators]]
[[Category:French aerospace engineers]]
[[Category:Olympic sailors of France]]
[[Category:French male sailors (sport)]]
[[Category:Sailors at the 1924 Summer Olympics – 8 Metre]]
[[Category:Olympic medalists in sailing]]