{{taxobox
 | image               = Myotis escalerai Cabrera.png
 | image_caption       = A drawing by [[Ángel Cabrera (naturalist)|Ángel Cabrera]], showing a ventral view of the entire animal, with one wing folded and the other spread; a lateral view of the head; and a view of the inner side of the ear
 | image_alt           = Drawing of a bat, seen from below, with its right wing folded. The head and ear are shown separately.
 | status              = NE
 | status_system       = IUCN3.1
 | status_ref          = 
 | regnum              = [[Animal]]ia
 | phylum              = [[Chordate|Chordata]]
 | classis             = [[Mammal]]ia
 | ordo                = [[Chiroptera]]
 | familia             = [[Vespertilionidae]]
 | genus               = ''[[Myotis]]''
 | species_complex     = [[Myotis nattereri complex|''Myotis nattereri'' complex]]
 | species             = '''''M. escalerai'''''
 | binomial            = ''Myotis escalerai''
 | binomial_authority  = [[Ángel Cabrera (naturalist)|Ángel Cabrera]], 1904
 | synonyms            = 
*''Myotis escalerae'' (misspelling)<ref name=S513/>
}}

'''''Myotis escalerai''''' is a European [[bat]] in the genus ''[[Myotis]]'', found in Spain (including the [[Balearic Islands]]), Portugal, and far [[southern France]].

Although the species was first named in 1904, it was included in [[Natterer's bat]] (''Myotis nattereri'') until molecular studies, first published in 2006, demonstrated that the two are distinct species. ''M.&nbsp;escalerai'' is most closely related to an unnamed species from Morocco. Unlike ''M.&nbsp;nattereri'', which lives in small groups in tree holes, ''M.&nbsp;escalerai'' forms large colonies in caves. Females start to aggregate in late spring in maternity colonies, and their young are born in summer. The species spends each winter in hibernation colonies, usually in caves or basements.

''M.&nbsp;escalerai'' is a medium-sized, mostly gray bat, with lighter underparts. It has a pointed muzzle, a pink face, and long ears. The wings are broad and the species is an agile flyer. Wingspan is {{convert|245|to|300|mm|in|abbr=on}} and body mass is {{convert|5|to|9.5|g|oz|abbr=on}}. Though very similar to ''M.&nbsp;nattereri'', it differs from that species in some features of the tail membrane. The [[conservation status]] of ''M.&nbsp;escalerai'' is assessed as "[[Vulnerable species|Vulnerable]]" or "[[Data Deficient]]" in various parts of its range.

==Taxonomy==
''Myotis escalerai'' was named by [[Ángel Cabrera (naturalist)|Ángel Cabrera]] in 1904, on the basis of four specimens from two localities in eastern Spain. He named the new species after the Spanish entomologist Manuel Martínez de la Escalera, who collected two specimens of the species in [[Bellver de Cerdanya|Bellver]], [[Catalonia]].<ref>Cabrera, 1904, pp.&nbsp;280–281</ref> Cabrera did not designate either of the two localities (Bellver and [[Foyos]], [[Valencia (autonomous community)|Valencia]]) as the [[type locality (biology)|type locality]], and later authors have listed both. Currently, Foyos, which was listed first by Cabrera, is accepted as the type locality.<ref>Benda et al., 2006, p.&nbsp;118, footnote; Ibáñez et al., 2006, p.&nbsp;286</ref> Cabrera commented that ''M.&nbsp;escalerai'' was close to [[Natterer's bat]] (''Myotis nattereri''),<ref>Cabrera, 1904, p.&nbsp;280</ref> and in 1912, [[Gerrit S. Miller]] listed ''escalerai'' as a [[synonym (taxonomy)|synonym]] of that species. He argued that one of the features Cabrera had listed as distinguishing the two was an artefact of the preservation of the specimens of ''M.&nbsp;escalerai'' in alcohol.<ref>Miller, 1912, p.&nbsp;174</ref> Miller's classification was followed for almost a century,<ref name=S513>Simmons, 2005, p.&nbsp;513</ref> and indeed, Cabrera himself accepted in 1914 that ''M.&nbsp;escalerai'' was not a valid species.<ref name="Sea8">Salicini et al., in press, p.&nbsp;8</ref>
{{cladogram|align=right
|style=font-size:85%;line-height:75%
|caption=Relationships in the ''Myotis nattereri'' complex based on [[nuclear DNA|nuclear]] and [[mitochondrial DNA]] sequences<ref>García-Mudarra et al., 2009, fig.&nbsp;2; Galimberti, 2011, fig.&nbsp;4.5; Salicini et al., in press, fig.&nbsp;2</ref>
|clades=
{{clade
|1={{clade
   |1={{clade
      |1=''[[Myotis nattereri]]''
      |2=''Myotis'' sp.&nbsp;A and C (see text)
      }}
   |2=''[[Myotis schaubi]]''
   |3={{clade
      |1='''''Myotis escalerai'''''
      |2=''Myotis'' sp.&nbsp;B
      }}
   }}
}}
}}
However, a 2006 study by Carlos Ibáñez and colleagues found that ''M.&nbsp;nattereri'' in fact included several [[cryptic species]] with highly distinguished [[DNA sequence]]s characteristics, even though morphological differences were small or nonexistent. One, which they recorded in the southern [[Iberian Peninsula]], was identified as ''M.&nbsp;escalerai''.<ref name=Iea286/> Populations in the mountains of northern Spain represent another species ("''Myotis'' sp.&nbsp;A"), which is now also known from the [[Alps]].<ref name=GMea441>García-Mudarra et al., 2009, p.&nbsp;441</ref> A 2009 study using data from the [[mitochondrial DNA|mitochondrial]] genes [[cytochrome b|cytochrome ''b'']] and [[ND1]] found that ''M.&nbsp;escalerai'' is most closely related to an [[undescribed taxon|unnamed species]] from Morocco previously included in ''M.&nbsp;nattereri'' ("''Myotis'' sp.&nbsp;B"), and more distantly to other members of the [[Myotis nattereri complex|''Myotis nattereri'' group]].<ref name=GMeaf2>García-Mudarra et al., 2009, fig. 2</ref> ''M.&nbsp;escalerai'' and the Moroccan species are estimated to have diverged about 2 million years ago.<ref name=GMea442>García-Mudarra et al., 2009, p.&nbsp;442</ref> Later in 2009, ''M.&nbsp;escalerai'' was also recorded for the first time from France.<ref name=Eea142/> One 2011 study found a fifth putative species in the complex ("''Myotis'' sp.&nbsp;C"), occurring in the Italian peninsula and most closely related to ''M.''&nbsp;sp.&nbsp;A,<ref>Galimberti, 2011, pp.&nbsp;99–100</ref> but another study published in the same year included these populations in ''M.''&nbsp;sp.&nbsp;A.<ref name="Sea8"/> The latter study, by I. Salicini and colleagues, used sequences from six [[nuclear DNA|nuclear]] genes to confirm the distinctiveness of ''M.&nbsp;escalerai'' and its close relationship with ''M.''&nbsp;sp.&nbsp;B.<ref>Salicini et al., in press, p.&nbsp;5</ref> The [[common name]] "Escalera's bat" has been used for ''M.&nbsp;escalerai''.<ref name=SCea166/>

==Description==
A medium-sized gray bat, ''Myotis escalerai'' is similar to ''Myotis nattereri''. The fur is long and soft; with a brown tone on the back, and the brighter underparts approaching white. The feet are dark gray. Much of the face is pink, and the muzzle is pointed, with long hairs on the upper lip resembling a moustache. The long ears are brown to gray. The [[tragus (anatomy)|tragus]], a projection on the inner side of the outer ear, is long and reaches to the middle of the ear and colored gray to yellow, becoming darker from the base towards the tip.<ref name=SCea166/> According to several authors, it differs from ''M.&nbsp;nattereri'' in showing a distinct fringe of hairs on the tail membrane,<ref>Ibáñez et al., 2006, p.&nbsp;286; Serra-Cobo et al., 2008a, p.&nbsp;167; Salicini et al., in press, p.&nbsp;8</ref> but bat specialist A.M. Hutson writes that this feature does not distinguish the two species.<ref>Hutson, 2010, p.&nbsp;5</ref> In addition, the presence of an S-shaped spur on the [[uropatagium]] (membrane between the hind legs), which approaches the middle of the membrane, is a distinctive feature of this species. With its broad wings, low flight, and rapid wingbeats, the species is capable of precise, agile flight.<ref name=SCea167>Serra-Cobo et al., 2008a, p.&nbsp;167</ref>

The head body length is {{convert|42|to|50|mm|in|abbr=on}}, tail length is {{convert|38|to|47|mm|in|abbr=on}}, forearm length is {{convert|35|to|43|mm|in|abbr=on}}, ear length is {{convert|14|to|18|mm|in|abbr=on}}, [[wingspan]] is {{convert|245|to|300|mm|in|abbr=on}}, and body mass is {{convert|5|to|9.5|g|oz|abbr=on}}.<ref name=SCea166>Serra-Cobo et al., 2008a, p.&nbsp;166</ref>

==Distribution and ecology==
The range of ''Myotis escalerai'' remains poorly constrained and may turn out to be larger than currently known.<ref name=Eea143>Evin et al., 2009, p.&nbsp;143</ref> ''M.&nbsp;escalerai'' is widespread in Spain and Portugal.<ref>García-Mudarra et al., 2009, fig. 2, pp.&nbsp;441–442; Ministério do Ambiente e do Ordenamento do Território, 2010; Serra-Cobo et al., 2008a, p.&nbsp;168</ref> For example, it occurs widely, though localized, in [[Aragón]], where ''Myotis'' sp.&nbsp;A (the only other species in the ''M.&nbsp;nattereri'' complex to occur there) is known from a single locality only.<ref>Alcalde et al., 2008, pp.&nbsp;8, 15</ref> Similarly, in Catalonia, ''M.&nbsp;escalerai'' is widespread and occurs from sea level up to an altitude of {{convert|1500|m|ft|abbr=on}}.<ref name=Fea42>Flaquer et al., 2010, p.&nbsp;42</ref> The species also occurs on the [[Balearic Islands]] of [[Mallorca]], [[Menorca]], and [[Ibiza]].<ref>Amengual et al., 2007, pp.&nbsp;274–275; Serra-Cobo et al., 2008a, p.&nbsp;168</ref> The sole French record is from a cave in [[Valmanya]], [[Pyrénées-Orientales]].<ref name=Eea142>Evin et al., 2009, p.&nbsp;142</ref>

Relatively little is known of the biology of ''M.&nbsp;escalerai''. Females begin to form reproductive colonies in April and May, either small ones or larger aggregations that may also contain males. However, most males remain solitary in this period, although some also form colonies. The single young is born in June or July and becomes independent after some six weeks. Mating usually takes place in fall, but sometimes in winter.<ref name=SCea167/> The formation of large reproductive colonies in caves, which may consist of several hundreds of individuals, distinguishes ''M.&nbsp;escalerai'' from ''M.&nbsp;nattereri'' as well as ''M.''&nbsp;sp.&nbsp;A, which roost in smaller groups in tree holes.<ref name=Iea286>Ibáñez et al., 2006, p.&nbsp;286</ref> In Aragón, colonies contain 50 to 880 individuals,<ref>Alcalde et al., 2008, p.&nbsp;8</ref> and Catalan colonies are known to contain over a hundred bats.<ref name=Fea42/> Reproductive colonies may be formed in a variety of structures, including caves, mines, tree holes, and human-made structures such as bridges and houses. However, [[hibernation]] colonies need constant temperatures between {{convert|0|and|5|C|F}}, and are usually located in caves or basements. ''M.&nbsp;escalerai'' is considered a sedentary species, and does not usually migrate over long distances, although it does move between reproduction and hibernation colonies.<ref name=SCea167/> [[Rabies]] has been identified in a Spanish specimen of ''M.&nbsp;escalerai''.<ref>Serra-Cobo et al., 2008b, p.&nbsp;171</ref>

==Conservation status==
The [[IUCN Red List]] does not separate ''Myotis escalerai'' from ''Myotis nattereri'', which is listed as "[[least concern]]",<ref>Hutson et al., 2008</ref> but the two species are listed separately on the Annex to the Agreement on the Conservation of Populations of European Bats.<ref>Anonymous, 2010</ref> Portugal lists ''M.&nbsp;escalerai'' as "[[Vulnerable species|vulnerable]]", though noting that populations may be increasing.<ref name=Portugalt1>Ministério do Ambiente e do Ordenamento do Território, 2010, table 1</ref> Because of its restriction to caves, it is considered vulnerable in Aragón.<ref>Alcalde et al., 2008, p.&nbsp;15</ref> In Catalonia, the species appears tolerant of different habitats and of human disturbance, but it is listed as "[[data deficient]]".<ref name=Fea42/> In France, where the species was only discovered in 2009, it is also listed as "data deficient".<ref>Gauthier, 2009, p.&nbsp;2</ref>

==References==
{{reflist|20em}}

==Literature cited==
*Alcalde, J.T., Trujillo, D., Artázcoz, A. and Agirre-Mendi, P.T. 2008. [http://dx.doi.org/10.3989/graellsia.2008.v64.i1.51 Distribución y estado de conservación de los quirópteros en Aragón]. Graellsia 64(1):3–16 (in Spanish).
*Amengual, B., López-Roig, M., Mas, O., González, J. and Serra-Cobo, J. 2007. [http://www.raco.cat/index.php/BolletiSHNBalears/article/view/179007/245660 Anàlisi d'ADN mitocondrial de cinc espècies de quiròpters de les Illes Balears]. Bolletí de la Societat d'Història Natural de les Balears 50:269–277 (in Catalan).
*Anonymous. 2010. [http://www.eurobats.org/documents/pdf/MoP6/record_MoP6/MoP6.Record.Annex5-Res.6.2-AmendmentAnnex.pdf 6th Session of the Meeting of Parties. Prague, Czech Republic, 20–22 September 2010. Resolution 6.2. Amendment of the Annex to the Agreement]. EUROBATS.MoP6.Record.Annex5.
*Benda, P., Andreas, M., Kock, D., Lučan, R.K., Munclinger, P., Nová, P., Obuch, J., Ochman, K., Reiter, A., Uhrin, M. and Weinfurtová, D. 2006. [http://www.nm.cz/download/pm/zoo/benda_lit/Benda2006aszb.pdf Bats (Mammalia: Chiroptera) of the eastern Mediterranean. Part 4. Bat fauna of Syria: distribution, systematics, ecology]. Acta Societatis Zoologicae Bohemicae 70:1–329.
*Cabrera, D.A. 1904. [https://books.google.com/books?id=gFYrAAAAYAAJ Ensayo monográfico sobre los quirópteros de España]. Memorias de la Sociedad Española de Historia Natural 2(5):249–286 (in Spanish).
*Evin, A., Lecoq, V., Durand, M., Tillon, L. and Pons, J. 2009. [http://dx.doi.org/10.1515/MAMM.2009.030 A new species for the French bat list: ''Myotis escalerai'' (Chiroptera: Vespertilionidae)] (subscription required). Mammalia 73(2):142–144.
*Flaquer, C., Puig, X., Fàbregas, E., Guixé, D., Torre, I., Ràfols, R.G., Páramo, F., Camprodon, J., Cumplido, J.M., Ruiz-Jarillo, R., Baucells, A.L., Freixas, L. and Arrizabalaga, A. 2010. [http://www.museugranollersciencies.org/latela_media/museu/pdf/quiropters/Flaquer_etal_Galemys22_29_61.pdf Revisión y aportación de datos sobre quirópteros de Catalunya: Propuesta de lista roja]. Galemys 22(1):29–61 (in Spanish).
*García-Mudarra, J.L., Ibáñez, C. and Juste, J. 2009. [http://dx.doi.org/10.1111/j.1095-8312.2008.01128.x The Straits of Gibraltar: barrier or bridge to Ibero-Moroccan bat diversity?] (subscription required). Biological Journal of the Linnean Society 96(2):434–450.
*Galimberti, A. 2011. [http://hdl.handle.net/10281/18920 DNA barcoding: a link between basic and applied science]. PhD thesis, Università degli Studi di Milano-Bicocca.
*Gauthier, O. 2009. [http://www.eurobats.org/documents/pdf/National_Reports/nat_rep_Fr_2010.pdf Implementation of the Agreement on the Conservation of Populations of European Bats. National Report of France. 2006–2009]. Inf.EUROBATS.MoP6.19.
*Hutson, A.M. 2010. [http://www.eurobats.org/documents/pdf/AC15_StC4/Doc_StC4_AC15_12_Rev1_DraftResolution_6_2_AmendmentAnnex.pdf Draft Resolution 6.2: Amendment of the Annex of the Agreement. Review of Species to be listed on the Annex to the Agreement]. Doc.EUROBATS.StC4-AC15.12.Rev.2.
*Hutson, A.M., Aulagnier, S. and Spitzenberger, F. 2008. {{IUCNlink|14135|Myotis nattereri}}. In IUCN. IUCN Red List of Threatened Species. Version 2010.2. <[http://www.iucnredlist.org/ www.iucnredlist.org]>. Downloaded on February 12, 2011.
*Ibáñez, C., García-Mudarra, J.L., Ruedi, M., Stadelmann, B. and Juste, J. 2006. [http://dx.doi.org/10.3161/{{urlencode:1733-5329(2006)8&#91;277:TICTCD&#93;2.0.CO;2}} The Iberian contribution to cryptic diversity in European bats]. Acta Chiropterologica 8(2):277–297.
*Miller, G.S. 1912. [http://biodiversitylibrary.org/bibliography/8830 Catalogue of the mammals of western Europe (Europe exclusive of Russia) in the collections of the British Museum]. London: British Museum (Natural History), 1019&nbsp;pp.
*Ministério do Ambiente e do Ordenamento do Território. 2010. [http://www.eurobats.org/documents/pdf/National_Reports/nat_rep_Por_2010.pdf Agreement on the Conservation of Populations of European Bats. Report on implementation of the Agreement in Portugal – 2010]. Inf.EUROBATS.MoP6.35.
*Salicini, I., Ibáñez, C. and Juste, J. In press. [http://dx.doi.org/10.1016/j.ympev.2011.08.010 Multilocus phylogeny and species delimitation within the Natterer's bat species complex in the Western Palearctic] (subscription required). Molecular Phylogenetics and Evolution, in press.<!--Page numbers for this are for the proof, available online-->
*Serra-Cobo, J., Roig, L., Bayer, X., Amengual, B. and Guasch, C. 2008a. [https://books.google.com/books?id=6xKK0HBxG3kC Ratpenats. Ciència i mite]. Barcelona: Edicions Universitat Barcelona, 267 pp. (in Catalan). ISBN 978-84-475-3344-2
*Serra-Cobo, J., Bourhy, H., López-Roig, M., Sánchez, L.P., Abellán, C., Borràs, M. and Amengual, B. 2008b. [http://www.isciii.es/htdocs/centros/epidemiologia/boletin_semanal/bes0834.pdf Rabia en quirópteros: Circulación de EBLV-1 (Lyssavirus de murciélagos europeos tipo 1) en murciélagos de España]. Boletín Epidemiológico Semanal 16(15):169–180 (in Spanish).
*Simmons, N.B. 2005. Order Chiroptera. Pp.&nbsp;312–529 in Wilson, D.E. and Reeder, D.M. (eds.). [http://www.bucknell.edu/msw3 Mammal Species of the World: A Taxonomic and Geographic Reference]. 3rd ed. Baltimore: The Johns Hopkins University Press, 2 vols., 2142&nbsp;pp. ISBN 978-0-8018-8221-0

{{Myotis nav}}
{{featured article}}

[[Category:Mammals described in 1904]]
[[Category:Mammals of Europe]]
[[Category:Mouse-eared bats]]