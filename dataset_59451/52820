{{Use dmy dates|date=August 2013}}
{{other uses}}
{{infobox bibliographic database
| title = Scopus
| image = [[File:Scopus_type_logo.jpg]]
| caption = 
| producer = [[Elsevier]]
| country = 
| history = 
| languages = English
| providers = 
| cost = Subscription
| disciplines= 
| depth = 
| formats = 
| temporal = 1995-present
| geospatial = Worldwide
| number = 55 million
| updates = 
| p_title = 
| p_dates = 
| ISSN = 
| web = http://www.scopus.com
| titles = 
}}
'''Scopus''' is a [[bibliographic database]] containing [[Abstract (summary)|abstracts]] and [[citation]]s for [[academic journal]] [[Article (publishing)|articles]]. It covers nearly 22,000 titles from over 5,000 publishers, of which 20,000 are [[peer review|peer-reviewed]] journals in the scientific, technical, medical, and social sciences (including arts and humanities).<ref>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Scopus Content Overview |work=Scopus Info |publisher=Elsevier |accessdate=2013-09-04}}</ref> It is owned by [[Elsevier]] and is available online by [[subscription business model|subscription]]. Searches in Scopus also incorporate searches of patent databases.<ref>{{cite journal |doi=10.1001/jama.2009.1307 |title=Comparisons of Citations in Web of Science, Scopus, and Google Scholar for Articles Published in General Medical Journals |year=2009 |last1=Kulkarni |first1=A. V. |last2=Aziz |first2=B. |last3=Shams |first3=I. |last4=Busse |first4=J. W. |journal=[[JAMA (journal)|JAMA]] |volume=302 |issue=10 |pages=1092–6 |pmid=19738094}}</ref>

==Overview==

Since Elsevier is the owner of Scopus and is also one of the main international publishers of scientific journals, an independent and international Scopus Content Selection and Advisory Board was established to prevent a potential conflict of interest in the choice of journals to be included in the database and to maintain an open and transparent content coverage policy, regardless of publisher.<ref>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview#content-policy-and-selection |title=Scopus Content Overview: Content Policy and Selection |work=Scopus Info |publisher=Elsevier |accessdate=2013-09-04}}</ref> The board consists of scientists and subject librarians.

Evaluating ease of use and coverage of Scopus and the [[Web of Science]] (WOS), a 2006 study concluded that "Scopus is easy to navigate, even for the novice user. ... The ability to search both forward and backward from a particular citation would be very helpful to the researcher. The multidisciplinary aspect allows the researcher to easily search outside of his discipline" and "One advantage of WOS over Scopus is the depth of coverage, with the full WOS database going back to 1945 and Scopus going back to 1966. However, Scopus and WOS complement each other as neither resource is all inclusive."<ref>{{Cite journal |pmid=16522216 |year=2006 |last1=Burnham |first1=JF |title=Scopus database: A review |volume=3 |pages=1 |doi=10.1186/1742-5581-3-1 |pmc=1420322 |journal=Biomedical Digital Libraries}}</ref>

Scopus also offers author profiles which cover affiliations, number of publications and their [[bibliographic]] data, [[references]], and details on the number of citations each published document has received. It has [[alerts|alerting]] features that allows registered users to track changes to a profile and a facility to calculate authors' [[h-index]].

Scopus IDs for individual authors can be integrated with the nonproprietary digital identifier [[ORCID]].<ref name="Scopus">{{cite web |url=http://orcid.scopusfeedback.com |title=Scopus2Orcid |publisher=Scopus |accessdate=7 May 2014}}</ref>

==See also==
*[[Source Normalized Impact per Paper]]
*[[Web of Science]]
*[[Open Citations Corpus]]

== References ==
{{reflist|30em}}

== External links ==
{{Wikidata property|P1153}}
{{Wikidata property|P1154}}
{{Wikidata property|P1155}}
{{Wikidata property|P1156}}
* {{Official website|http://www.scopus.com/}}

{{Reed Elsevier}}

[[Category:Bibliographic databases and indexes]]
[[Category:Elsevier]]
[[Category:Citation indices]]
[[Category:Library cataloging and classification]]