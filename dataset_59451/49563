{{Infobox short story <!--See [[Wikipedia:WikiProject Novels]]-->
| name                = Berenice
| image               = [[File:Berenice(SLM).gif|frameless]]
| caption             = "Berenice" as it appeared in its original published form.
| author              = [[Edgar Allan Poe]]
| title_orig          = Berenice – A Tale
| translator          = 
| country             = United States
| language            = English
| series              = 
| genre               = [[Horror fiction|Horror]] [[short story]]
| published_in        = ''[[Southern Literary Messenger]]''
| publication_type    = Print ([[Periodical]])
| publisher           = 
| media_type          = 
| pub_date            = March 1835
| english_pub_date    = 
| preceded_by         = 
| followed_by         = 
| preceded_by_italics = 
| followed_by_italics = 
}}
"'''Berenice'''" is a [[short story|short]] [[horror fiction|horror story]] by [[Edgar Allan Poe]], first published in the ''[[Southern Literary Messenger]]'' in [[1835 in literature|1835]]. The story follows a man named Egaeus who is preparing to marry his cousin Berenice. He has a tendency to fall into periods of intense focus during which he seems to separate himself from the outside world. Berenice begins to deteriorate from an unnamed disease until the only part of her remaining healthy is her teeth, which become the object of Egaeus' obsession. Berenice is buried, and Egaeus continues to contemplate her teeth. One day Egaeus wakes up from a period of focus with an uneasy feeling, and the sound of screams in his ears. A servant startles him by telling him Berenice's grave has been disturbed, and she is still alive; but beside Egaeus is a shovel, a poem about "visiting the grave of my beloved" and a box containing 32 blood-stained teeth.

Contemporary readers were horrified by the story's violence and complained to the editor of the ''Messenger''. Though Poe later published a self-censored version of the work he believed he should be judged solely by how many copies were sold.

==Plot summary==
The narrator, Egaeus, is a studious young man who grows up in a large gloomy mansion with his cousin Berenice. He suffers from a type of [[Obsessive-compulsive disorder|obsessive]] disorder, a [[monomania]] that makes him [[Fixation (psychology)|fixate]] on objects. She, originally beautiful, suffers from some unspecified degenerative illness, with periods of [[catalepsy]] a particular symptom, which he refers to as a trance. Nevertheless, they are due to be married.

One afternoon, Egaeus sees Berenice as he sits in the library. When she smiles, he focuses on her teeth. His obsession grips him, and for days he drifts in and out of awareness, constantly thinking about the teeth. He imagines himself holding the teeth and turning them over to examine them from all angles. At one point a servant tells him that Berenice has died and shall be buried. When he next becomes aware, with an inexplicable terror, he finds a lamp and a small box in front of him. Another servant enters, reporting that a grave has been violated, and a shrouded disfigured body found, still alive. Egaeus finds his clothes are covered in mud and blood, and opens the box to find it contains dental instruments and "thirty-two small, white and ivory-looking substances" – Berenice's teeth.

The [[Latin]] [[epigraph (literature)|epigraph]], "''Dicebant mihi sodales si sepulchrum amicae visitarem, curas meas aliquantulum fore levatas''," at the head of the text may be translated as:  "My companions said to me, if I would visit the grave of my friend, I might somewhat alleviate my worries." This quote is also seen by Egaeus in an open book towards the end of the story.

==Analysis==
In "Berenice", Poe was following the popular traditions of [[Gothic fiction]], a genre well-followed by American and British readers for several decades.<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. Harper Perennial, 1991. p. 111. ISBN 0-06-092331-8</ref> Poe, however, made his Gothic stories more sophisticated, dramatizing terror by using more realistic images.<ref name=Meyers77>Meyers, Jeffrey. ''Edgar Allan Poe: His Life and Legacy''. Cooper Square Press, 1992. p. 77. ISBN 0-8154-1038-7</ref> This story is one of Poe's most violent. As the narrator looks at the box which he may subconsciously know contains his cousin's teeth, he asks himself, "Why... did the hairs of my head erect themselves on end, and the blood of my body become congealed within my veins?" Poe does not actually include the scene where the teeth are pulled out. The reader also knows that Egaeus was in a trance-like state at the time, incapable of responding to evidence that his cousin was still alive as he committed the gruesome act. Additionally, the story emphasizes that all 32 of her teeth were removed.

The main theme lies in the question that Egaeus asks himself: "How is it that from beauty I have derived a type of unloveliness?"<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. Harper Perennial, 1991. p. 114. ISBN 0-06-092331-8</ref> Poe also uses a character afflicted with monomania for the first time, a device he uses many times again.<ref name=Meyers77/>

Teeth are used symbolically in many of Poe's stories to symbolize mortality. Other uses include the "sepulchral and disgusting" horse's teeth in "[[Metzengerstein]]", lips writhing about the teeth of the mesmerized man in "[[The Facts in the Case of M. Valdemar]]", and the sound of grating teeth in "[[Hop-Frog]]".<ref>Kennedy, J. Gerald. ''Poe, Death, and the Life of Writing''. New Haven, CT: Yale University Press, 1987. ISBN 0-300-03773-2 p. 79</ref>

Egaeus and Berenice are both representative characters. Egaeus, literally born in the library, represents intellectualism. He is a quiet, lonely man whose obsession only emphasizes his interest on thought and study. Berenice is a more physical character, described as "roaming carelessly through life" and "agile, graceful, and overflowing with energy." She is, however, an oppressed woman, having "spoke no word" throughout the story. Her only purpose, as with many of Poe's female characters, is to be beautiful and to die.<ref>Weekes, Karen. "Poe's Feminine Ideal," as collected in ''The Cambridge Companion to Edgar Allan Poe''. New York City: Cambridge University Press, 2002. ISBN 0-521-79727-6 p. 150</ref> Egaeus loses his interest in the full person of Berenice as she gets sick; she becomes an object to analyze, not to admire. He dehumanizes her by describing "the" forehead of Berenice, rather than "her" forehead.<ref>Weekes, Karen. "Poe's feminine ideal," collected in ''The Cambridge Companion to Edgar Allan Poe'', edited by Kevin J. Hayes. Cambridge University Press, 2002. p. 155. ISBN 0-521-79727-6</ref>

Poe may have used the names of the two characters to call to mind the conventions of [[Ancient Greece|ancient Greek]] [[tragedy]]. Berenice's name (which means "bringer of victory") comes from a poem by [[Callimachus]]. In the poem, Berenice promises her hair to [[Aphrodite]] if her husband returns from war safely. Egaeus may come from [[Aegeus]], a legendary king of Athens who had committed suicide when he thought his son [[Theseus]] had died attempting to kill the [[Minotaur]].<ref name=Meyers77/>

The final lines of the story are purposely protracted using a series of conjunctions connecting multiple clauses. The rhythm as well as the heavy accented consonant and long vowels sounds help unify the effect.<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. Harper Perennial, 1991. p. 113. ISBN 0-06-092331-8</ref>

Incidentally, this is one of the few Poe stories whose narrator is named.

===Major themes===
Several often-repeated themes in Poe's works are found in this story:
*The death of a beautiful woman (see also "[[Ligeia]]," "[[Morella (short story)|Morella]]," "[[The Oval Portrait]]," "[[The Philosophy of Composition]]")
*Being buried alive (see also "[[The Cask of Amontillado]]," "[[The Fall of the House of Usher]]," "[[The Premature Burial]]" – being buried alive is also very briefly mentioned in "How To Write A Blackwood Article" as a source of possible inspiration for The Signora Psyche Zenobia)
*Mental illness (see also "[[The Fall of the House of Usher]]," "[[The Tell-Tale Heart]]", "[[The System of Doctor Tarr and Professor Fether]]")
*Catalepsy (see also "[[The Premature Burial]]," "[[The Fall of the House of Usher]]")

==Publication history and critical response==
First published in the relatively genteel ''Southern Literary Messenger''<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. Harper Perennial, 1991. p. 110. ISBN 0-06-092331-8</ref> in March 1835. Many readers were shocked by the violence in "Berenice" and complained to publisher Thomas W. White,<ref name=Whalen69>Whalen, Terence. "Poe and the American Publishing Industry" as collected in ''A Historical Guide to Edgar Allan Poe'', edited by J. Gerald Kennedy. Oxford University Press, 2001. p. 69. ISBN 0-19-512150-3</ref> leading to an edited version eventually being published in 1840. The four removed paragraphs describe a scene where Egaeus visits Berenice before her burial and clearly sees that she is still alive as she moves her finger and smiles.

Poe disagreed with the complaints. A month after "Berenice" was published, he wrote to White saying that many magazines achieved fame because of similar stories. Whether in bad taste or not, he said it was his goal to be appreciated, and "to be appreciated you must be ''read''."<ref name=Whalen69/> He admitted, "I allow that it approaches the very verge of bad taste – but I will not sin quite so egregiously again." Even so, Poe also emphasized that its final judgment should come not from the taste of the reading public but on the circulation of the magazine.<ref name=Whalen69/>

==Adaptations==

Director [[Eric Rohmer]] directed and took the lead in his 1954 short film adaptation of "Berenice" entitled "Bérénice", filmed in 16&nbsp;mm black and white with cinematography by [[Jacques Rivette]].

''[[CBS Radio Mystery Theater]]'' presented an adapted version of the story for its radio play "Berenice" as its January 9, 1975 episode. It was directed by [[Himan Brown]], Adapted by [[George Lowther (writer)|George Lowther]], starring [[Michael Tolan]], [[Norman Rose]], [[Joan Lovejoy]], and [[Roberta Maxwell]].

[[Vincent Price]] performed "Berenice" on his 1975 album ''The Imp of the Perverse and Other Tales'' (Caedmon Records TC—1450). On the same LP Price also read "Morella" and "The Imp of the Perverse". All three Poe stories were re-issued in 2000 on the Harper Collins 5-CD set, ''The Edgar Allan Poe Audio Collection''.

The 1995 [[Video game|computer game]] ''[[The Dark Eye (video game)|The Dark Eye]]'' contained reenactments of selected stories by Poe. One of them was based on "Berenice" and allowed the player to experience the story from the alternating points of view of both Egaeus and Berenice.

There was a low-budget film adaptation released to video in 2004.<ref>[http://www.imdb.com/title/tt0428259/ ''Berenice'' at Internet Movie Database IMDb]</ref>

==References==
{{reflist|2}}

==External links==
{{wikisourcepar|The Works of the Late Edgar Allan Poe/Volume 1/Berenice|Berenice}}
* [http://www.eapoe.org/works/tales/bernicea.htm "Berenice"] – Full Text of the first printing, from the ''Southern Literary Messenger'', 1835
* {{librivox book | title=Berenice | author=Edgar Allan Poe}}

{{Edgar Allan Poe}}

{{DEFAULTSORT:Berenice (Short Story)}}
[[Category:1835 short stories]]
[[Category:Short stories adapted into films]]
[[Category:Short stories by Edgar Allan Poe]]
[[Category:Works originally published in the Southern Literary Messenger]]