{{Use dmy dates|date=November 2013}}
{{Infobox airport
| name = Salon-de-Provence Air Base
| ensign = [[File:French-roundel.svg|50px]]
| nativename = Base aérienne 701 Salon-de-Provence<br>{{smaller|Advanced Landing Ground (ALG) Y-16}}
| image = Ecole air flag guard Bastille Day 2008.jpg
| image-width = 300
| caption =
| IATA =
| ICAO = LFMY
| type = Military
| owner = [[Cabinet of France|Government of France]]
| operator = [[French Air Force|Armée de l'air]]
| city-served =
| location = [[Salon-de-Provence]]
| elevation-f = 194
| elevation-m = 59
| coordinates = {{Coord|43|36|23|N|005|06|33|E|type:airport|name=Salon-de-Provence Air Base|display=inline,title}}
|pushpin_map= France
|pushpin_label=Salon-de-Provence Air Base
|latNS=N|latd=43|latm=36|lats=23
|longEW=E|longd=005|longm=06|longs=33
|pushpin_map_caption=Location of Salon-de-Provence Air Base, France
| website =
| metric-rwy = y
| r1-number = 16/34
| r1-length-f = 6,565
| r1-length-m = 2,001
| r1-surface = [[Concrete]]
| footnotes = Source: Aerodrome chart at Direction de la Circulation Aerienne Militaire (DIRCAM)<ref name="dircam">[http://www.dircam.air.defense.gouv.fr/dia/PDF/MIAC4%20ENG/MIAC4_LFMY_ENG.pdf LFMY - SALON]</ref>
}}
'''Salon-de-Provence Air Base''' ({{lang-fr|Base aérienne 701 Salon-de-Provence|links=no}} or BA 701) {{airport codes||LFMY}} is a base of the [[French Air Force]] located {{convert|4|km|abbr=on}} south<ref name="dircam"/> [[Salon-de-Provence]] in southern [[France]].

==Overview==
It hosts the training facilities for the officers of the air force:
* ''[[École de l'Air]]'': for young students
** pilot commissioned officers
** mechanics commissioned officers
** air base commissioned officers
* ''École militaire de l'Air'': gives access to the same careers as the ''École de l'Air'', for [[non-commissioned officer]]s who want to become commissioned officers
* air [[commissary]] school
* special course of the ''École de l'Air'': for foreign officers
* special course of formation of officers

It hosts the demonstration formations of the French Air Force, including the ''[[Patrouille de France]]''.

In addition, it hosts a school of the [[French Navy]]: the school of Naval Aviation (EAN).

==World War II==
Salon-de-Provence Air Base is a pre-[[World War II]] airfield, which was used by the Armée de l'Air during the early part of the war. It was briefly a base for [[RAF Bomber Command]] [[Vickers Wellington|Wellingtons]], which were sent to Salon from England, for raids on the Italian port of [[Genoa]], as a part of [[Operation Haddock]].<ref>''The Royal Air Force 1939-1945'', Denis Richards, Her Majesty's Stationery Office 1974, ISBN 0-11-771592-1 (pp.146-147)</ref> After the 1940 [[Battle of France]] and the June Armistice with Nazi Germany, it became part of the limited ({{lang-fr|Armée de l'Air de Vichy|links=no}}) air force of the Vichy Government.  Known Vichy units at Salon-de-Provence were:<ref>[http://france1940.free.fr/vichy/ada_sep.html ''Armée de l'Air de Vichy Order of Battle''], 1 September 1940</ref>
* G.C. I/6 (1)  [[Morane-Saulnier M.S.406]] fighters
* G.C. III/9   [[Bloch MB.150|Bloch MB.152]] fighters

On 11 November 1942, Salon-de-Provence Air Base was seized by Nazi forces as part of [[Case Anton]], the occupation of Vichy and the [[Luftwaffe]] took control of the base.  Under German control, the base became a bomber airfield for anti-shipping operations over the [[Mediterranean]] against American Convoys, and later, attacking Allied forces on [[Corsica]] and [[Sardinia]] after their capture from Italian forces ([[Royal Italian Army (1940–1946)|Regio Esercito]]) during 1943.<ref name="vit1">[http://www.ww2.dk  The Luftwaffe, 1933-45]</ref>  Known units assigned were:
* [[Kampfgeschwader 100]] (KG 100), flying [[Heinkel He 111]]Hs, February–April 1943
* [[Kampfgeschwader 26]] (KG 26), flying [[Heinkel He 111]]Hs, May 1943-March 1944
* [[Zerstörergeschwader 1]] (ZG 1), flying [[Messerschmitt Bf 110]]s, May 1944
: Primarily air defense against [[Twelfth Air Force]] [[B-26 Marauder]] medium bomber attacks on Southern France
* [[Kampfgeschwader 77]] (KG 77), flying [[Junkers Ju 88]]s, June–July 1944.

It was attacked on several missions by Allied bombers based in [[England]] while under German control.  The airfield was sized by Allied Forces in August 1944 during [[Operation Dragoon]], the Invasion of Southern France in August 1944 and was repaired and placed into operational use by the [[United States Army Air Forces]] XII Engineer Command, being turned over to [[Twelfth Air Force]] on 28 August 1944.  It was designated as [[Advanced Landing Ground]] "Y-16 Salon".<ref name="eng">[http://www.ixengineercommand.com/airfields/general.php IX Engineering Command ETO Airfields General Construction Information]</ref>

Twelfth Air Force stationed the [[27th Fighter Squadron]] at the repaired field from 30 August, flying [[A-36 Apache]]s until moving north into eastern France in October.    Also the [[47th Bombardment Group]] flew [[A-20 Havoc]] light bombers from the field during September.<ref>Maurer, Maurer. ''Air Force Combat Units of World War II''. Maxwell AFB, Alabama: Office of Air Force History, 1983. ISBN 0-89201-092-4.</ref>

The use by American forces of the airfield was brief, and on 20 November 1944 it was returned to French control.<ref>Johnson, David C. (1988), U.S. Army Air Forces Continental Airfields (ETO), D-Day to V-E Day; Research Division, USAF Historical Research Center, Maxwell AFB, Alabama.
</ref>

==See also==
{{Portal|World War II}}
{{Commons|Base aérienne 701 Salon-de-Provence}}
* [[Advanced Landing Ground#Y-12 to Y-39|Advanced Landing Ground]]

==References==
{{Air Force Historical Research Agency}}
<references/>

==External links==
*[http://www.defense.gouv.fr/sites/air/decouverte/lorganisation/bases_aeriennes/metropole/region_aerienne_sud/ba_701_salon_de_provence French Air Force link]
* [http://www.ba701.air.defense.gouv.fr/ Official site]
* {{NWS-current|LFMY}}

{{Air force academies}}
{{Navboxes
|list =
{{USAAF 12th Air Force World War II}}
}}

{{DEFAULTSORT:Salon-De-Provence Air Base}}
[[Category:French Air Force bases]]
[[Category:French Naval Aviation bases]]
[[Category:Airfields of the United States Army Air Forces in France]]
[[Category:Airports established in 1935]]