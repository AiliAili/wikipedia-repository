A '''mega journal''' (also '''mega-journal''' and '''megajournal''') is a [[peer-reviewed]] [[academic journal|academic]] [[open access journal]] designed to be much larger than a traditional journal by exerting low selectivity among accepted articles. It was pioneered by ''[[PLOS ONE]]''.<ref name=Beall2013/><ref name=Bjork/> This highly lucrative publishing model<ref name=Bjork/> was soon emulated by other publishers.

==Definition==
A mega journal has the following defining characteristics:
* broad coverage of different subject areas;<ref name=Beall2013/><ref name=Bjork/><ref name=Wiley/><ref name=Lboro/><ref name=Binfield/>
* accepting articles for publication based on whether they are technically sound rather than selecting for perceived importance;<ref name=Beall2013/><ref name=Bjork/><ref name=Wiley/><ref name=Lboro/><ref name=Binfield/><ref name=Norman/> and
* [[author-pays model]] of [[open access]] where costs are covered by an [[article processing charge]].<ref name=Beall2013/><ref name=Wiley/><ref name=Binfield/>
Other less universal characteristics are
* "an accelerated review and publication process",<ref name=Bjork/> "fast turnaround time";<ref name=Norman/> and
* "academic editors",<ref name=Norman/> even "a large editorial board of academic editors",<ref name=Binfield/> (instead of professional editors)
* value-added services such as reusable graphics and data through [[Creative Commons license|Creative Commons]] licenses<ref name=":0">{{Cite journal|last=Björk|first=Bo-Christer|title=Have the "mega-journals" reached the limits to growth?|url=https://peerj.com/articles/981|journal=PeerJ|volume=3|doi=10.7717/peerj.981|pmc=4451030|pmid=26038735|page=e981}}</ref>
Mega journals are also online-only, with no printed version, and are fully open access, in contrast to [[hybrid open access]] journals.<ref name=":0" /> Some [[Predatory open access publishing|"predatory" open access publishers]] use the mega journal model.<ref name=Beall2013/>

== Influence ==
It has been suggested that the academic journal landscape might become dominated by a few mega journals in the future, at least in terms of total number of articles published.<ref>Hayahiko Ozono, Okayama University, Participants' Report on The 5th SPARC Japan Seminar 2011. “Burgeoning Open Access MegaJournals”. National Institute of Informatics. [http://www.nii.ac.jp/sparc/en/publications/newsletter/13/topics2.html]</ref>
Megajournals are also disrupting{{clarify|date=October 2014}} the market of [[article processing charges]].<ref name=Solomon2014>{{cite journal|last1=Solomon|first1=David J.|title=A survey of authors publishing in four megajournals|journal=PeerJ|date=2014|volume=2|pages=e365|doi=10.7717/peerj.365|url=https://peerj.com/articles/365/|pmid=24795855|pmc=4006221}}</ref>
Their business model may not motivate reviewers, who donate their time to "influence their field, gain exposure to the most current cutting edge research or list their service to a prestigious journal on their CVs."<ref>{{Cite journal | doi = 10.1177/2158244013507271
| title = Open Access, Megajournals, and MOOCs: On the Political Economy of Academic Unbundling
| journal = SAGE Open
| volume = 3
| issue = 4
| year = 2013
| last1 = Wellen
| first1 = R.
}}</ref>
Finally, they may no longer serve as "fora for the exchange ... among colleagues in a particular field or sub-field", as traditionally happened in scholarly journals.<ref>{{cite journal|last=Beall|first=Jeffrey |year=2013|title= The Open-Access Movement is Not Really about Open Access|journal= tripleC|volume=11|number=2|pages=589–597|url=http://www.triple-c.at/index.php/tripleC/article/view/525}}</ref> To counter that indiscrimination, [[PLOS ONE]], the prototypical megajournal, has started to "package relevant articles into subject-specific collections."<ref>{{Cite journal | doi = 10.1371/journal.pbio.1001235| title = Why ONE is More Than 5| journal = PLoS Biology| volume = 9| issue = 12| pages = e1001235| year = 2011| last1 = MacCallum | first1 = C. J. }}</ref>

== List of mega journals ==
{{expand list|date=September 2014}}
<!-- Please keep it ordered by the number of citations -->
{{columns-list|colwidth=30em|
* ''[[PLOS ONE]]<ref name=Beall2013/><ref name=Bjork/><ref name=Wiley/><ref name=Lboro/><ref name=Binfield/><ref name=Norman/><ref name=Chile/><ref name=PTPLC/><ref name=Sitek/><ref name=MacGregor/><ref name=Jackson/>
* ''[[ACS Omega]]''<ref name=Bernstein/>
* ''[[Scientific Reports]]''<ref name=Bjork/><ref name=Wiley/><ref name=Binfield/><ref name=Norman/><ref name=Sitek/><ref name=Jackson/><ref name=BinfieldJapan/>
* ''[[SAGE Open]]''<ref name=Wiley/><ref name=Lboro/><ref name=Binfield/><ref name=Sitek/><ref name=Jackson/><ref name=BinfieldJapan/>
* ''[[Royal_Society_Open_Science|Royal Society Open Science]]''{{efn|Self-declared:<ref>{{cite web|url=http://rsos.royalsocietypublishing.org/about|title=About|work=Royal Society Open Science}}</ref>}}
* ''[[SpringerPlus]]''<ref name=Wiley/><ref name=Lboro/><ref name=Binfield/><ref name=Sitek/><ref name=Jackson/><ref name=BinfieldJapan/>
* ''[[BMJ Open]]''<ref name=Bjork/><ref name=Wiley/><ref name=Binfield/><ref name=Sitek/><ref name=Jackson/>
* ''[[PeerJ]]''<ref name=Bjork/><ref name=Lboro/><ref name=Binfield/><ref name=Chile/><ref name=PTPLC/>
* ''[[Biology Open]]''<ref name=Binfield/><ref name=Norman/><ref name=Jackson/>
* ''[[IEEE Access]]''<ref name=Binfield/><ref name=Beall/>{{efn|Self-declared:<ref>New IEEE Open-Access "Mega Journal" Aims to Boost Technology Innovation [http://www.multivu.com/mnr/61674-ieee-online-open-access-mega-journal-for-faster-peer-reviewed-publishing]</ref>}}
* ''[[FEBS Open Bio]]''<ref name=Binfield/><ref name=Norman/>
* ''[[AIP Advances]]''<ref name=Binfield/><ref name=Jackson/>
* ''[[G3: Genes, Genomes, Genetics]]''<ref name=Binfield/><ref name=Jackson/>
* ''[[Zootaxa]]''{{efn|Self-declared:<ref>{{cite journal|author=Zhang, Zhi-Qiang|year=2006|title=The making of a mega-journal in taxonomy|journal=Zootaxa|issue= 1385|pages= 67–68|url=http://www.mapress.com/zootaxa/2006f/zt01385p068.pdf}}</ref>}}
* ''[[Open Library of Humanities]]''{{efn|Self-declared:<ref>{{cite web|url=https://www.openlibhums.org/about/media/press-release/|title=Press Release|work=Open Library of Humanities}}</ref>}}
* [[De Gruyter Open]] [[imprint (trade name)|imprint]]{{efn|Self-declared:<ref>{{cite web|url=http://degruyteropen.com/de-gruyter-open-converts-eight-subscription-journals-open-access-megajournals/|title=De Gruyter Open converts eight subscription journals to Open Access megajournals|work=De Gruyter Open}}</ref>}}
* ''[[Elsevier]] Heliyon''{{efn|Self-declared:<ref>{{cite web|url=http://www.elsevier.com/reviewers-update/story/innovation-in-publishing/introducing-heliyon-elseviers-new-broad-scope,-open-access-journal|title=Introducing Heliyon - Elsevier's new broad scope, open access journal|work=Elsevier Heliyon}}</ref>}}
* '' [[Sage Open Medicine]] {{efn|Self-declared:<ref>{{cite web|url=http://journals.sagepub.com/home/smo}}</ref>}}}}
<!--
Please only move an item from the comments below into the body of the article above after it obtains at least two citations from different authors, or it's self-declared mega journal.
* [[eLife]]<ref name=PTPLC/>
* [[Nature Communications]]<ref name=Sitek/>
* [[Q Science Connect]]<ref name=Binfield/>
* [[Elementa]]<ref name=Binfield/>
* [[Optics Express]]<ref name=Binfield/>
* [[Cureus]]<ref name=Binfield/>
* [[F1000 Research]]<ref name=Binfield/>
* [[BMC Research Notes]]<ref name=Jackson/>
* [[RSC Advances]]<ref name=Jackson/>
* [[Physical Review X]]<ref name=Jackson/>
* [[RSC Advances]]<ref name=Jackson/>
* [[The Scientific World Journal]]<ref name=Binfield/><ref name=BinfieldJapan/>
-->

== Notes ==
{{notelist}}

== References ==
{{Reflist|colwidth=30em|refs=
<ref name=Lboro>{{cite web|author=Claire Creaser|title= The rise of the mega-journal|date= 5 May 2014|website= School of Business and Economics Research Blog|publisher= Loughborough University|url=http://blog.lboro.ac.uk/sbe/centre-for-information-management/the-rise-of-the-mega-journal/}}</ref>
<ref name=Beall>{{cite web|author=[[Jeffrey Beall]]|title= New Term: MOAMJ = Multidisciplinary Open Access Mega Journal|website= Scholarly Open Access|date=3 March 2013|url= http://scholarlyoa.com/2013/03/05/new-term-moamj-multidisciplinary-open-access-mega-journal}}</ref>
<ref name=Chile>{{cite web|author=Francisco Osorio|title= Open Library of Humanities: mega journals seeing from the south|date= 5 April 2013|website= Facultad de Ciencias Sociales de la Universidad de Chile|url=http://www.facso.uchile.cl/noticias/90036/open-library-of-humanities-mega-journals-seeing-from-the-south}}</ref>
<ref name=Wiley>{{cite web|url=http://exchanges.wiley.com/blog/2012/02/14/wiley-open-access-update-january-2012/|title=Wiley|publisher=}}</ref>
<ref name=PTPLC>"Beyond open access for academic publishers", 15 May 2014, Publishing Technology PLC [http://www.publishingtechnology.com/2014/05/beyond-open-access-for-academic-publishers/]</ref>
<ref name=Bjork>{{cite report|authors=Bo-Christer Björk and David Solomon|title= Developing an Effective Market for Open Access Article Processing Charges|date= March 2014|pages= 69 pages. |publisher=[[Wellcome Trust]]|url= http://www.wellcome.ac.uk/stellent/groups/corporatesite/@policy_communications/documents/web_document/wtp055910.pdf}}</ref>
<ref name=Sitek>Dagmar Sitek & Roland Bertelmann, "Open Access: A State of the Art", 2 March 2014, Springer, doi:10.1007/978-3-319-00026-8_9 [http://book.openingscience.org/tools/open_access_state_of_the_art.html]</ref>
<ref name=MacGregor>James MacGregor, Kevin Stranack & John Willinsky, "The Public Knowledge Project: Open Source Tools for Open Access to Scholarly Communication", 2 March 2014, doi:10.1007/978-3-319-00026-8_11 [http://book.openingscience.org/tools/the_public_knowledge_project.html]</ref>
<ref name=Binfield>{{cite web|author=Peter Binfield|title= Novel Scholarly Journal Concepts|date= 19 January 2014| doi=10.1007/978-3-319-00026-8_10|work=Opening Science|editor=Sönke Bartling & Sascha Friesike|url= http://book.openingscience.org/tools/novel_scholarly_journal_concepts.html}}</ref>
<ref name=Norman>{{cite web|url=http://occamstypewriter.org/trading-knowledge/2012/07/09/megajournals/|author= Frank Norman|title= Megajournals|date= 9 July 2012|website= Trading Knowledge|publisher=Frank Norman}}</ref>
<ref name=BinfieldJapan>Peter Binfield, "PLoS ONE and the Rise of the Open Access MegaJournal", The 5th SPARC Japan Seminar 2011, National Institute of Informatics, The 5th SPARC Japan Seminar 2011 February 29, 2012 [http://www.nii.ac.jp/sparc/en/event/2011/pdf/20120229_doc3_binfield.pdf] [http://www.slideshare.net/PBinfield/ssp-presentation4]</ref>
<ref name=Beall2013>{{cite journal|last1=Beall|first1=Jeffrey|title=Five Predatory Mega-Journals: A Review|journal=The Charleston Advisor|date=2013|volume=14|issue=4|pages=20–25|doi=10.5260/chara.14.4.20|url=http://eprints.rclis.org/19815/2/TCA%20review%20april%202013.pdf}}</ref>
<ref name=Jackson>Rhodri Jackson and Martin Richardson, "Gold open access: the future of the academic journal?", Chapter 9 in Cope and Phillip (2014), p.223-248.</ref>
<ref name=Bernstein>{{cite web|url=http://www.acs.org/content/acs/en/pressroom/newsreleases/2015/december/american-chemical-society-announces-acs-omega-a-new-open-access-journal-serving-global-and-multidisciplinary-chemistry.html|author= Michael Bernstein and Katie Cottingham, Ph.D.|title= American Chemical Society announces ACS Omega, a new open access journal serving global and multidisciplinary chemistry|date= 14 December 2015|website= www.acs.org|publisher=ACS}}</ref>
<!-- additional uncited references:
<ref name=Jingfeng2014>{{cite journal|last1=Xia|first1=Jingfeng|title=An examination of two Indian megajournals|journal=Learned Publishing|date=2014|volume=27|issue=3|pages=195–200|doi=10.1087/20130305}}</ref>
<ref name=>David J. Solomon, "A survey of authors publishing in four megajournals", April 22, 2014, https://peerj.com/articles/365/]</ref>
<ref>{{cite journal|last1=Van Noorden|first1=Richard|title=PLOS profits prompt revamp|journal=Nature|date=19 November 2013|volume=503|issue=7476|pages=320–321|doi=10.1038/503320a|url=http://www.nature.com/news/plos-profits-prompt-revamp-1.14205|pmid=24256784}}</ref>
-->
}}

== Further reading ==
* Bill Cope and Angus Phillips, ''The Future of the Academic Journal'', 2nd ed., Chandos Publishing, Jul 1, 2014, 478 pages.
* [[Peter Binfield]], "Open Access MegaJournals -- Have They Changed Everything?", Creative Commons New Zealand Blog, [http://creativecommons.org.nz/2013/10/open-access-megajournals-have-they-changed-everything/]
* Sönke Bartling & Sascha Friesike (Editors), [http://book.openingscience.org/ ''Opening Science: The Evolving Guide on How the Web is Changing Research, Collaboration and Scholarly Publishing''], Springer, 2014, ISBN 978-3-319-00025-1, 339 pp.

[[Category:Academic publishing]]
[[Category:Open access (publishing)]]