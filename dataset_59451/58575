{{Infobox journal
| title = Journal of the European Ceramic Society
| cover = 
| editor = Richard Todd
| discipline = [[Materials science]], [[ceramic]]s
| formernames = International Journal of High Technology Ceramics
| abbreviation = J. Eur. Ceram. Soc.
| publisher = [[Elsevier]] on behalf of the [[European Ceramic Society]]
| country =
| frequency = Monthly
| history = 1985-present
| openaccess =
| license =
| impact = 2.933
| impact-year = 2015
| website = http://www.elsevier.com/locate/jeurceramsoc
| link1 = http://www.sciencedirect.com/science/journal/09552219
| link1-name = Online access
| link2 = http://ecers.org/en/journal/journal.html
| link2-name = Journal page at European Ceramic Society
| JSTOR = 
| OCLC = 780547428
| LCCN = 97657497
| CODEN =
| ISSN = 0955-2219
| eISSN =
}}
The '''''Journal of the European Ceramic Society''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by [[Elsevier]] on behalf of the [[European Ceramic Society]]. It covers research related to conventional categories of [[ceramic]]: structural, functional, traditional or composite. It was established in 1985 as the ''International Journal of High Technology Ceramics'', obtaining its current name in 1989.

==Abstracting and indexing==
This journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* Ceramics Abstracts
* [[Current Contents]]/Engineering, Computing & Technology<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-04-11}}</ref>
* Current Contents/Physical, Chemical & Earth Sciences<ref name=ISI/>
* [[FLUIDEX]]
* [[Materials Science Citation Index]]
* [[Inspec]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=2017-04-11}}</ref>
* [[Science Citation Index]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=https://www.scopus.com/sourceid/21548 |title=Source details: Journal of the European Ceramic Society |publisher=[[Elsevier]] |work=Scopus preview |accessdate=2017-04-11}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.933.<ref name=WoS>{{cite book |year=2016 |chapter=Journal of the European Ceramic Society |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

==External links==
*{{Official website|http://www.sciencedirect.com/science/journal/09552219}}

{{DEFAULTSORT:Journal of The European Ceramic Society}}
[[Category:Materials science journals]]
[[Category:English-language journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1985]]
[[Category:Monthly journals]]