{{Infobox hospital
| name        = The Nazareth Hospital
| org/group   = The Nazareth Trust
| image       = <!-- Preferably photo of the main building or entrance-->
| alt         = 
| image_size  = 225 
| caption     = <!-- optional -->
| map_type    = Israel northeast
| relief      = <!-- any non-blank value (yes, 1, etc.) will cause the template to display a relief map image, where available -->
| map_size    = <!-- width of the map in pixels (do not include "px"); default is 225 -->
| map_alt     = <!-- alternative text for map image, see WP:ALT for details -->
| map_caption = <!-- Optional. Gives a small caption under the map such as "Shown in region, country" -->
| coordinates = {{coord|32.7017|35.2917|display=inline,title}}
| logo        = <!-- Please conform to copyright -->
| logo_size   = <!-- Use to limit the logo size -->
| location    = [[Nazareth]]
| region      = [[Northern District (Israel)|Northern District]]
| country     = [[Israel]]
| address     = <!-- Address of main building -->
| healthcare  = <!-- UK:NHS. AU/CA: Medicare. ELSE freetext, e.g. Private -->
| funding     = <!-- Non-profit, For-profit, Government, Public - will generate linka -->
| type        = <!-- Community, District, General, District General, Teaching, Specialist -->
| speciality  = <!-- if devoted to a speciality (i.e. not a broad spectrum of specialities) and Type=Specialist/Teaching -->
| standards   = <!-- optional if no national standards -->
| emergency   = Yes
| helipad     = <!-- Yes, No, or use "Template:Airport codes" with p=n. Only list if verifiable, leave blank if unknown. -->
| affiliation = <!-- 'None' or Medical School and university affiliations (medical or paramedical) -->
| patron      = None
| network     = <!-- Hospital network, non-owner -->
| beds        = <!-- Cite in article as well -->
| founded     = <!-- Cite in article as well -->
| closed      = <!-- Use if defunct, please also add to Category:Defunct hospitals -->
| demolished  = <!-- Use if demolished at a different time from closure -->
| website     = {{URL|http://www.nazarethtrust.org/nazareth-hospital}}
| other_links = <!-- Creates "See also" field -->
}}
The '''EMMS Nazareth Hospital''', also known as '''Scottish Hospital''' and '''English Hospital''', is the general hospital of the city of [[Nazareth]], [[Israel]]. It was founded as a [[Christian mission]] by Dr. Karloost Vartan and the [[EMMS International|Edinburgh Medical Missionary Society]] in 1861. The hospital now houses 147 beds, employs over 500 staff, and receives over 50,000 visits annually.<ref name="Nazareth Trust">{{cite web|title=History of The Nazareth Trust|url=http://www.nazarethtrust.org/history-emms-nazareth|work=The Nazareth Trust|accessdate=16 December 2012}}</ref>

==Background==
The Nazareth Hospital project was originally led by Dr. Pacradooni [[Kaloost Vartan]]. Born in [[Constantinople]] to an Armenian family in 1835, he attended an American [[Protestant]] School for Armenian Boys. During the [[Crimean War]], he served as an interpreter for British forces. There, he was moved and inspired by the poor conditions of war, and by the care provided at the hospitals run by [[Florence Nightingale]]. Vartan moved to [[Edinburgh]], [[Scotland]] to study medicine.<ref name="The Missionary">{{cite news|last=van der Klugt|first=Melissa|title=The Missionary Who Healed the Sick of Nazareth|newspaper=The Times|date=29 September 2012|location=London|page=105}}</ref> His studies were funded by the newly founded [[Edinburgh Medical Missionary Society]] (EMMS).<ref>{{cite book|last=Yaron|first=Perry|title=Modern Medicine in the Holy Land: Pioneering British Medical Services in Late Ottoman Palestine|year=2007|publisher=Tauris Academic Studies|location=London|author2=Efraim Lev |page=129}}</ref> Established in 1841 by a group of doctors, the EMMS aimed to provide medical services to foreign countries, to "circulate information on medical mission [sic], help other institutions engaged in the same work and assist as many missionary stations as their funds would permit."<ref>{{cite web|title=Origins and Background|url=http://www.emms.org/about-us/origins-and-background|work=EMMS International|accessdate=17 December 2012}}</ref>
Dr. Vartan was first funded by the Syrian Asylum Fund in 1861 to aid massacre victims in [[Beirut]].<ref>{{cite book|last=Ziadat|first=Adel A.|title=Western Medicine in Palestine, 1860-1940|year=2003|publisher=Canadian Bulletin of Medical History|location=Montreal|pages=269–279}}</ref> After the Syrian Asylum Committee was dissolved, the EMMS recruited his services for a new mission in Nazareth starting in 1866.<ref>{{cite book|last=Wilkinson|first=John|title=The Coogate Doctors|year=1991|publisher=Edinburgh Medical Missionary Society|location=Edinburgh|page=43}}</ref>
[[File:Kaloost Vartan.png|thumb|Dr. Kaloost Vartan<ref name="Nazareth Trust"/>]]

==Palestine and health==
During this time, [[Palestine (region)|Palestine]] was governed by the [[Ottoman Empire]]. Lack of adequate social services and environmental factors contributed to poor public health. Citizens suffered from cholera, dysentery, malaria, and tuberculosis, and infant mortality was high. Palestine was en route for those participating in the [[Hajj]], which made locals susceptible to a steady influx of foreign diseases. Traditional medicine included herbal treatments, [[cauterization]], [[bloodletting]], [[bonesetting]], cupping, and [[leeching (medical)|leeching]], among various tribal rituals.<ref>{{cite journal|last=Davidovitch|first=Nadav|author2=Zalman Greenberg |title=Public Health, Culture, and Colonial Medicine: Smallpox and Variolation in Palestine During the British Mandate|journal=NIH: US National Library of Medicine|date=May–June 2007|volume=122|pages=398–406|pmc=1847484|issue=3|pmid=17518312}}</ref> In 1861 the average life expectancy was 22 for males and 24 for females.<ref name="Nazareth Trust"/> Although Ottoman law required the provision of medical services, in reality, it did little more than employ a few municipal part-time workers.<ref name="Ziadat">Ziadat, 270-271</ref> In the 1860's, Nazareth was home to nearly 5,000 citizens, but the nearest doctor or hospital was as far away as Damascus or Beirut.<ref name="The Missionary"/> Ultimately, the only care was provided by charity.

==History==

===Early history===
In 1861, Dr. Vartan arrived in Nazareth to begin work. He joined Johannes or [[John Zeller]], a missionary of the [[Church Mission Society]].<ref name="Yaron 129">Yaron, 129</ref> Zeller was an Anglican priest, and helped Dr. Vartan carry out surgeries with chloroform anesthesia.<ref name="The Missionary"/> Initially, Dr. Vartan was confronted with a campaign against him by local healers, who saw his plan of establishment a threat to their practices. There was also a question of funding, as the committee that sent Dr. Vartan to Nazareth disbanded in 1864. Through these tribulations, Dr. Vartan continued to practice medicine. In 1865, he treated the son of the Mayor of Nazareth, and won the title, "The Great Doctor."<ref name="Yaron 129"/>

Dr. Vartan first received patients in his own home. Dr. Burns Thompson, superintendent of the Coogate dispensary, supported him for two years after the original committee dissolved. In 1866, the EMMS officially adopted the Nazareth Hospital project.<ref name="Yaron 129"/> The society provided £100 for a few beds for surgical cases, which were installed in the upper room of Dr. Vartan’s house. As the only hospital near Nazareth, these accommodations were obviously insufficient, and Dr. Vartan soon received approval from the EMMS for the purchase for two adjacent houses. One would serve as a hospital, and the other his residence and dispensary. Only a year after the expansion, 36 inpatients were admitted, and 70&ndash;100 patients were treated daily.<ref>Ziadat, 272&ndash;273</ref>
This small dispensary was located on the east side of Nazareth. A small flow of clean water was available there, but rainwater was used more frequently. The hospital staff ran a boys’ Sunday school and soup kitchen,<ref name="notes">"Editorial Notes." Edinburgh Medical Missionary Society Quarterly Paper XX (Nov. 1941): 50. Print.</ref> as well as an artists’ club, in addition to normal hospital operations.<ref>"Editorial Notes." Edinburgh Medical Missionary Society Quarterly Paper XX (Nov. 1942): 97. Print.</ref> As a Christian mission, prayers were held every morning at 7am.<ref>Hatton. "Impressions of a Patient in Nazareth Hospital." Edinburgh Medical Missionary Society Quarterly Paper XX (Feb. 1941): 3. Print.</ref> Most nurses and staff were native to Ottoman Palestine, and had minimal English skills, which proved challenging to Dr. Vartan and his wife. Both patients and staff came from various religious denominations. Most were Christian, but Jews and Muslims worked for and were treated by the hospital, as well. An early survey of patients shows that in 1899, 5,747 Christians and 2,680 Muslims sought care in the hospital.<ref name="notes"/><ref>Ziadat, 271</ref>

In 1879, a plot of land overlooking Nazareth was purchased for the establishment of a bigger hospital. Before anything could be done, Dr. Vartan was tasked with getting permission, a ''[[firman]]'', from the Ottoman authorities to establish a dispensary.<ref name="The Missionary"/> The Turkish government put a stop to his efforts to build after a misleading promise to provide a permit. No more progress was made until 1895, when the EMMS directors approved a new site west of Nazareth. Dr. Vartan died in 1908, before any construction could begin.<ref>Wilkinson, 44</ref>

Dr. Frederick John Scrimgeour succeeded Dr. Vartan as the head doctor of the mission, along with two European nurses. The new hospital was almost complete, when World War I broke out through Europe and the Middle East, and British physicians were ordered to leave Palestine.<ref>Yaron, 130</ref> The new hospital was requisitioned by the military, stripped, and used as stables.<ref name="The Missionary"/> While Dr. Scrimgeour was stuck in Egypt and unable to return; two nurses from the hospital operated in his rented residence downtown.<ref>Ziadat, 273&ndash;274</ref><ref>Wilkinson, 46</ref>

The hospital was restored in 1919, and normal operations slowly resumed. In that year, the hospital was recognized by the British government’s mandate as a hospital for training nurses. In 1924, the hospital was officially inaugurated and, by 1935, it had electricity and a sanitary annex for [[tuberculosis]] patients. The mission began extending services beyond elementary care. For example, by 1924, plans were adopted for infant welfare clinics in a nearby village.<ref name="Ziadat 274-275">Ziadat, 274&ndash;275</ref> In 1933, an east wing was added to the hospital, which contained the "Abercrombie Ward".<ref name="Wilkinson 48-49">Wilkinson, 48&ndash;49</ref> It soon took on the name, the "Hospital on the Hill". By this time, Dr. Scrimgeour had retired, and Dr. William D. Bathgate and Sister Mary Parkinson had taken charge of the hospital.<ref name="Wilkinson 48-49"/>

===Late history===
In May 1948, the state of Israel was established. Dr. Runa MacKay, who joined the mission a few years later, reported that hospital operations were generally uninhibited by the political upheaval, and the majority Arab population of Nazareth remained. However, an influx of refugees created more demand on the workers. At this time, the hospital still only had 100 beds.<ref>{{cite book|last=MacKay|first=Runa|title=Exile In Israel: A Personal Journey with the Palestinians|year=1995|publisher=Wild Goose|location=Glasgow|pages=25–26}}</ref>
This pressure resulted in the recruitment of more doctors. In addition to Dr. MacKay, Drs. Hester and Bernath joined the mission in May 1956.<ref name="Nazareth Trust"/> Their work inspired major changes in the hospital. Dr. Tester joined the hospital staff in 1952. Under his supervision, the hospital installed a modern laundry system, a new outpatient facility, nurses' home and chapel, and new accommodations for staff from 1957 to 1964.<ref>Wilkinson, 49</ref>

Dr. Bernath arrived at the mission in 1956, and was appointed as administrator of the hospital in 1969. He worked to continue updating the hospital, and oversaw the development of a new maternity clinic, kitchen, and most importantly, the recruitment of specialists to provide dialysis and physiotherapy.<ref name="Nazareth Trust"/> Other significant improvements include the addition of the "Tower Block,” an intensive care unit, in 1976. A unit for [[renal dialysis]] was opened in October 1981, and the first computer was installed the following year. This period saw another increase in hospital staff. Many of the newly hired doctors came from the Arab community, in particular, Dr. Nakhle Bishara, who became the medical director of the hospital in 1981. In that same year, Israeli healthcare reform named the Nazareth Hospital as the official district hospital for the Nazareth area. This, in addition to a strike by Israeli doctors in 1983, caused a massive influx in patients, leaving the hospital once again inadequate.<ref name="Wilkinson, 51-53">Wilkinson, 51-53</ref>

In 1984, a delegation from Edinburgh was sent to review the hospital's operations and establish a plan to accommodate the renewed demand for services. Mr. Fred Aitken was appointed as head the implementation of the plan, which aimed to increase the standards of the facilities, rather than the capacity of the hospital, and had an operating budget of 3.5 million pounds.<ref name="Wilkinson, 51-53"/>
In 1988 Dr. Robert W. Martin succeeded Dr. Bernath as superintendent. In 1995, further healthcare reforms were instituted which guaranteed basic healthcare to all residents of Israel. The hospital recruited help, namely from Mr. G. Anthony Holt, who became interim general administrator, and Mr. Derek Thompson, a hospital administrator from the United Kingdom, to make further transitions to a large-scale medical facility. In 2001, The EMMS split into two separate organizations, EMMS International and EMMS Nazareth. Thompson served as the first CEO of EMMS Nazareth, followed in 2007 Mr. Joseph Maine. In that same year, Mr. Elia Abdo became the General Director of the Nazareth Hospital. In 2010, Dr. Bishara Bisharat became the head of the hospital. In 2009, the title "EMMS Nazareth" was replaced with "The Nazareth Trust."<ref name="Nazareth Trust"/> After almost 150 years, the Nazareth Hospital and nursing school are still operational.

==Today==
Today, the hospital continues to expand its practices, led by the head of the hospital, Dr. Bishara Bisharat (MD, MPh), Dr Kemal Kem (MD MBBS DCH DRCOG DFFP MRCGP) and a faithful management team. For example, in October 2012, the hospital opened a pregnancy and childbirth complex in the nearby village of Umm Al-Fahm.<ref>ابوعطا, ابراهيم."مستشفى الناصرة يفتتح دفيئة الحمل والولادة في مجمع أبو دغش في أم الفحم. " الشر  N.p., 27 Oct. 2012. Web. 15 Dec. 2012.Translation by Google</ref> Nazareth is now home to 250,000 people, and the hospital is equipped with 147 beds and over 500 staff.<ref name="interview">{{cite news|last=Billings|first=Malcom|title=150 Years On, Israeli Hospital Founded by Edinburgh Doctor Still Nurtured by Scots Staff|newspaper=The Scotsman|date=26 November 2012}}</ref> The hospital is now also home to biochemistry and hematology labs, as well as a blood bank. The hospital has various departments, including childbirth and gynecology, orthopedic surgery, general surgery, urology, cosmetic surgery, emergency, radiology, cardiology, preterm birth, anesthesia, and research.<ref>{{cite web|title=تحديد موعد و مركز معلومات|url=http://www.nazhosp.com/newver/|work=مستشفى الناصرة|publisher=AHLANNET|accessdate=17 December 2012}} Translation by Google</ref>  Although it remains Christian in principle, the hospital neither hires nor treats patients preferentially.

== References ==
{{Reflist|33em}}

== External links ==
* [http://www.emms.org/ EMMS International]
* [http://www.nazarethtrust.org/ The Nazareth Trust]

{{WAP assignment|course=Wikipedia:USEP/Courses/Medical_Missionaries_to_Community_Partners:_Great_Ideas_in_the_name_of_Public_Health_(Kent_Bream) |university=University of Pennsylvania |term=Fall 2012}}

{{DEFAULTSORT:Nazareth Hospital}}
[[Category:1861 establishments in the Ottoman Empire]]
[[Category:Articles created via the Article Wizard]]
[[Category:Buildings and structures in Northern District (Israel)]]
[[Category:Hospitals established in 1866]]
[[Category:Hospital buildings completed in 1924]]
[[Category:Hospitals in Israel]]
[[Category:Christian medical missionaries]]
[[Category:Mission hospitals]]
[[Category:Nazareth|EMMS Nazareth Hospital]]