{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:Italian battleship Ammiraglio di Saint Bon.jpg|300px]]
|Ship caption=''Ammiraglio di Saint Bon''
}}
{{Infobox ship class overview
|Name=''Ammiraglio di Saint Bon'' class
|Builders=
|Operators={{Navy|Kingdom of Italy}}
|Class before={{sclass-|Re Umberto|battleship|4}}
|Class after={{sclass-|Regina Margherita|battleship|4}}
|Subclasses=
|Cost=
|Built range=1893–1902
|In service range=
|In commission range=1901–1920
|Total ships building=
|Total ships planned=
|Total ships completed=2
|Total ships retired=2
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship type=[[Pre-dreadnought battleship]]
|Ship displacement=*{{convert|10082|LT|t|0}} normal
*{{convert|10531|LT|t|0}} full load
|Ship length={{convert|366|ft|9.5|in|m|abbr=on|0}}
|Ship beam={{convert|69|ft|3.5|in|m|abbr=on|0}}
|Ship draft={{convert|25|ft|2.5|in|m|abbr=on|0}}
|Ship power={{convert|14296|ihp|abbr=on|lk=in}}
|Ship propulsion=2 shafts, [[Marine steam engine#Triple or multiple expansion|triple expansion steam engines]], 12 cylindrical boilers
|Ship speed={{convert|18.3|kn|lk=in}}
|Ship range={{convert|5500|nmi|abbr=on}} at {{convert|10|knots}}
|Ship complement=557
|Ship armament=*4 × [[10 in/40 Type 41 naval gun|{{convert|10|in|mm|0|abbr=on}}]]/40 guns
*8 × {{convert|6|in|mm|0|abbr=on}}/40 guns
*8 × {{convert|4.7|in|mm|0|abbr=on}}/40 guns
*8 × {{convert|57|mm|abbr=on}}/43 six-pounder guns
*2 × 1 - {{convert|37|mm|abbr=on}}/20 guns
*4 × 1 - {{convert|17.7|in|mm|0|abbr=on}} [[torpedo tube]]s
|Ship armor=*[[Harvey armor]]
*[[Belt armor|Belt]] and side: {{convert|9.8|in|mm|0|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|2.75|in|mm|0|abbr=on}}
*[[Turret]]s: {{convert|9.8|in|mm|0|abbr=on}}
*[[Conning tower]]: {{convert|9.8|in|mm|0|abbr=on}}
*[[Casemate]]s: {{convert|5.9|in|mm|0|abbr=on}}
|Ship notes=
}}
|}

The '''''Ammiraglio di Saint Bon'' class''' was a pair of [[pre-dreadnought battleship]]s built for the [[Italian Navy]] ({{lang-it|Regia Marina}}) during the 1890s. The class comprised two ships: {{ship|Italian battleship|Ammiraglio di Saint Bon||2}}, the [[lead ship]], and {{ship|Italian battleship|Emanuele Filiberto||2}}. They were armed with a main battery of four {{convert|10|in|adj=on|0|abbr=off}} guns and were capable of a top speed of {{convert|18|kn|lk=in}}. Smaller and less powerfully-armed than most contemporary battleships, they marked a brief departure from Italian battleship design, which had previous emphasized large ships equipped with large guns.

Both ships served in the active duty squadron early in their careers, and participated in the [[Italo-Turkish War]] of 1911–12. They took part in the Italian offensives in [[North Africa]] and the island of [[Rhodes]], but did not see combat with the Ottoman fleet. They were reduced to harbor defense ships by the outbreak of [[World War I]], and they spent the war in [[Venice]]. The ships were discarded shortly after the end of the war, both having been stricken in 1920.

==Design==
The previous Italian [[capital ship]]s, the {{sclass-|Re Umberto|battleship|5}} and the {{sclass-|Ruggiero di Lauria|ironclad|4}}es of [[ironclad battleship]]s, marked a period of experimentation on the part of [[Benedetto Brin]], Admiral [[Simone di Pacoret Saint Bon]], and the strategists of the Italian navy. Since the planners had not determined what type of battleship would best suit their strategic needs, the government stepped in and mandated a {{convert|10000|LT|adj=on}} design smaller than the earlier classes. Following the death of di Saint Bon, Brin took over the design process and proposed a small battleship armed with {{convert|10|in|abbr=on|0}} guns, a weaker main battery than those of contemporary foreign designs.<ref>Hore, pp. 78–79</ref>

The ships, much smaller than their contemporaries, and slower than [[cruiser]]s, were not particularly useful warships. The mistake of building a battleship of only 10,000&nbsp;tons was not repeated in the subsequent, and much more successful, {{sclass-|Regina Margherita|battleship|4}}.<ref name=G343/>

===General characteristics and machinery===
[[File:Ammiraglio di Saint Bon line-drawing.png|thumb|left|Line-drawing of the ''Ammiraglio di Saint Bon'' class]]
The ships of the ''Ammiraglio di Saint Bon'' class were {{convert|105|m|sp=us}} [[length at the waterline|long at the waterline]] and {{convert|111.8|m|abbr=on}} [[length overall|long overall]]. They had a [[beam (nautical)|beam]] of {{convert|21.12|m|abbr=on}} and a maximum [[draft (hull)|draft]] of {{convert|7.69|m|abbr=on}}. ''Ammiraglio di Saint Bon'' displaced {{convert|10082|MT|sp=us}} at standard loading and {{convert|10531|MT|abbr=on}} at full combat load, while ''Emanuele Filiberto'' displaced {{convert|9645|MT|abbr=on}} and {{convert|9940|MT|abbr=on}}, respectively. The ships had a low [[Freeboard (nautical)|freeboard]] of only {{convert|3|m|abbr=on}}. ''Ammiraglio di Saint Bon'' had a crew of 557&nbsp;officers and enlisted men, while ''Emanuele Filiberto'' had a slightly larger complement of 565.<ref name=G343/>

The ships' propulsion system consisted of two [[triple expansion engine]]s; ''Ammiraglio di Saint Bon''{{'}}s were rated at {{convert|14296|ihp|lk=in}}, while ''Emanuele Filiberto''{{'}}s engines only reached {{convert|13552|ihp|abbr=on}}. Steam for the engines was provided by twelve coal-fired cylindrical [[water-tube boiler]]s. The ships' propulsion system provided a top speed of {{convert|18|kn}} and a range of approximately {{convert|5500|nmi|lk=in}} at {{convert|10|kn}}.<ref name=G343>Gardiner, p. 343</ref>

===Armament and armor===
The ships were armed with four [[10 in/40 Type 41 naval gun|{{convert|10|in|abbr=on|0}}]] 40-[[Caliber (artillery)|caliber]] guns manufactured by [[Armstrong Whitworth]]. The guns were placed in two twin [[gun turret]]s, one forward and one aft. The ships were also equipped with eight {{convert|6|in|abbr=on|0}} 40-cal. guns in individual [[casemate]]s [[amidships]]. These guns were export derivatives of the British [[QF 6 inch /40 naval gun|QF 6-inch /40 gun]]. ''Ammiraglio di Saint Bon'' was also equipped with eight {{convert|4.7|in|abbr=on|0}} 40-cal. guns in shielded pivot mounts directly above the casemate battery and eight {{convert|57|mm|abbr=on}} guns and two {{convert|37|mm|abbr=on}} guns. ''Emanuele Filiberto'' carried six {{convert|3|in|abbr=on|0}} guns and eight {{convert|47|mm|abbr=on}} guns instead. Both ships also carried four {{convert|17.7|in|abbr=on|0}} [[torpedo tube]]s in deck-mounted launchers.<ref name=G343/>

The ships were protected with [[Harvey armor|Harvey steel]]. The [[belt armor|main belt]] was {{convert|9.8|in|abbr=on|0}} thick, and the deck was {{convert|2.75|in|abbr=on|0}} thick. The [[conning tower]] was protected by 9.8&nbsp;in of armor plating. The main battery guns had 9.8&nbsp;in thick plating, and the casemates were {{convert|5.9|in|abbr=on|0}} thick.<ref name=G343/>

==Ships of the class==
{| class="wikitable"
!Name!!Builder<ref name=G343>Gardiner, p. 343</ref>!!Laid down<ref name=G343/>!!Launched<ref name=G343/>!!Completed<ref name=G343/>
|-
|{{ship|Italian battleship|Ammiraglio di Saint Bon||2}}
|[[Venice]]
|18 July 1893
|29 April 1897
|24 May 1901
|-
|{{ship|Italian battleship|Emanuele Filiberto||2}}
|[[Castellammare di Stabia]]
|5 October 1893
|29 September 1897
|16 April 1902
|-
|}

==Service==
[[File:Italian battleship Emanuele Filiberto.jpg|upright|thumb|''Emanuele Filiberto'' at [[Fiume]] in late 1918 after the end of [[World War I]]]]

{{ship|Italian battleship|Ammiraglio di Saint Bon||2}} was built by the Venice Naval Shipyard. She was laid down on 18 July 1893, launched on 29 April 1897, and completed on 24 May 1901, although she had been commissioned on 1 February 1901. {{ship|Italian battleship|Emanuele Filiberto||2}} was named after [[Prince Emanuele Filiberto, Duke of Aosta]]. She was built by the Castellammare Naval Shipyard in [[Castellammare di Stabia]], [[Province of Naples|Naples]]. She was laid down on 5 October 1893, launched on 29 September 1897, and completed on 16 April 1902, although she had been commissioned on 6 September 1901.<ref name=G343/> The ships spend the first several years in the active duty squadron until they were replaced by the new {{sclass-|Regina Elena|battleship|1}}s, which entered service by 1908.<ref>Gardiner, p. 344</ref><ref>Brassey, p. 78</ref>

Both ships took part in the [[Italo-Turkish War]] in 1911–1912 in the 3rd Division with the two {{sclass-|Regina Margherita|battleship|1}}s.<ref>Beehler, p. 9</ref> ''Emanuele Filiberto'' took part in the attack on [[Tripoli]] in October 1911,<ref>Willmott, p. 167</ref> though ''Ammiraglio di Saint Bon'' did not see action in the first months of the war. Both ships participated in the seizure of the island of [[Rhodes]], where ''Ammiraglio di Saint Bon'' provided gunfire support to the soldiers ashore.<ref>Beehler, pp. 74–75</ref>

The two ships were slated to be scrapped in 1914–15 due to their age, but the outbreak of [[World War I]] in August 1914 prevented their disposal.<ref name=G343/> Italy initially remained neutral during the war, but by 1915, had been convinced by the [[Triple Entente]] to enter the war against Germany and Austria-Hungary.<ref>Halpern, p. 140</ref> Both ''Ammiraglio di Saint Bon'' and ''Emanuele Filiberto'' were used as harbor defense ships in [[Venice]] for the duration of the war, and did not see action there.<ref>Sondhaus, p. 274</ref> Neither ship remained in service long after the end of the war. ''Emanuele Filiberto'' was stricken from the [[naval register]] on 29 March 1920 and ''Ammiraglio di Saint Bon'' was stricken on 18 June. Both ships were subsequently discarded.<ref name=G343/>

==Footnotes==
{{Commons category|Ammiraglio di Saint Bon class battleship}}
{{Portal|Battleships}}
{{Reflist|colwidth=20em}}

==References==
* {{Cite book |last=Beehler|first=William Henry|title=The History of the Italian-Turkish War: September 29, 1911, to October 18, 1912|year=1913|location=Annapolis, MD|publisher=United States Naval Institute|url=https://books.google.com/books?id=OWcoAAAAYAAJ&printsec=frontcover#v=onepage&q&f=false}}
* {{cite book| editor-last = Brassey | editor-first = Thomas A. | authorlink = Thomas Brassey, 1st Earl Brassey | year = 1908 | title = [[Brassey's Naval Annual]] | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{Cite book |editor-last=Gardiner|editor-first=Robert|title=Conway's All the World's Fighting Ships: 1860–1905|year=1979|location=Annapolis, MD|publisher=Conway Maritime Press|isbn=978-0-85177-133-5}}
* {{Cite book |last=Halpern|first=Paul G.|title=A Naval History of World War I|year=1995|location=Annapolis, MD|publisher=Naval Institute Press|isbn=978-1-55750-352-7}}
* {{Cite book| last=Hore| first=Peter| title=The Ironclads| year=2006| location=London, UK| publisher=Southwater Publishing| isbn=978-1-84476-299-6}}
* {{Cite book| last=Sondhaus| first=Lawrence| title=The Naval Policy of Austria-Hungary, 1867–1918| year=1994| location=West Lafayette, IN| publisher=Purdue University Press| isbn=978-1-557-53034-9}}
* {{Cite book|last=Willmott|first=H. P.|title=The Last Century of Sea Power (Volume 1, From Port Arthur to Chanak, 1894–1922)|year=2009|location=Bloomington, IN|publisher=Indiana University Press|isbn=978-0-253-35214-9}}

==Further reading==
* {{Cite book|last=Crawford|first=Steve|title=Battleships and Carriers|year=2000|location=Rochester, NY|publisher=Grange|isbn=978-1-84013-337-0}}
* {{cite book|last=Fraccaroli|first=Aldo |title=Italian Warships of World War I|location=London, UK|publisher=Ian Allan|year=1970|isbn=978-0-7110-0105-3}}

{{Ammiraglio di Saint Bon class battleship}}
{{WWIItalianShips}}

{{DEFAULTSORT:Ammiraglio Di Saint Bon}}
[[Category:World War I battleships of Italy]]
[[Category:Ammiraglio di Saint Bon-class battleships| ]]