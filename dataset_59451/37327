{{good article}}
{{Infobox nobility
| name          =Christopher Báthory
| native name   =
| title         =
| image         =Báthory Kristóf erdélyi fejedelem.jpg
| caption       =Christopher depicted in Franz Christoph Khevenhüller's ''Conterfet Kupfferstich''
| succession    = [[Voivode of Transylvania]]
| reign         = 14 January 1576 – 27 May 1581
| coronation    =
| predecessor   = [[Stephen Báthory]]
| successor     = [[Sigismund Báthory]]
| regent         =
| spouse         ={{hlist|Catherina Danicska|Elisabeth Bocskai}}
| issue          ={{hlist|Balthasar|Nicholas|[[Griselda Báthory|Griselda]]|[[Sigismund Báthory|Sigismund]]}}
| noble family   = [[Báthory family|House of Báthory]]
| father         =[[Stephen VIII Báthory]]
| mother         =[[Catherine Telegdi]]
| birth_date  = 1530
| birth_place = [[Szilágysomlyó]], [[Kingdom of Hungary]] (now [[Șimleu Silvaniei]], Romania)
| death_date  = {{death date and age|df=yes|1581|5|27|1530}}
| death_place = [[Gyulafehérvár]], [[Principality of Transylvania (1570–1711)|Principality of Transylvania]]<br /><small>(today [[Alba Iulia]], Romania)</small>
| place of burial=Jesuit Church, Gyulafehérvár
|signature =
}}
'''Christopher Báthory''' ({{lang-hu|Báthory Kristóf}}; 1530 – 27 May 1581) was [[voivode of Transylvania]] from 1576 to 1581. He was a younger son of [[Stephen VIII Báthory|Stephen Báthory of Somlyó]]. Christopher's career began during the reign of Queen [[Isabella Jagiellon]], who administered the [[Eastern Hungarian Kingdom|eastern territories of the Kingdom of Hungary]] on behalf of her son, [[John Sigismund Zápolya]], from 1556 to 1559. He was one of the commanders of John Sigismund's army in the early 1560s.

Christopher's brother, [[Stephen Báthory]], who succeeded John Sigismund in 1571, made Christopher captain of Várad (now [[Oradea]] in Romania). After being elected [[King of Poland]], Stephen Báthory adopted the title of [[Prince of Transylvania]] and made Christopher voivode in 1576. Christopher cooperated with [[Márton Berzeviczy]], whom his brother appointed to supervise the administration of the [[Principality of Transylvania (1570–1711)|Principality of Transylvania]] as the head of the Transylvanian chancellery at [[Kraków]]. Christopher ordered the imprisonment of [[Ferenc Dávid]], a leading theologian of the [[Unitarian Church of Transylvania]], who started to condemn the adoration of Jesus. He supported his brother's efforts to settle the [[Jesuits]] in Transylvania.

== Early life ==
Christopher was the third of the four sons of Stephen Báthory of Somlyó and [[Catherine Telegdi]].{{sfn|Horn|2002|p=245}}{{sfn|Markó|2006|p=101}} His father was a supporter of [[John Zápolya]], [[King of Hungary]], who made him voivode of Transylvania in February 1530.{{sfn|Markó|2006|p=101}} Christopher was born in [[Șimleu Silvaniei Fort|Báthorys' castle]] at Szilágysomlyó (now [[Șimleu Silvaniei]] in Romania) in the same year.{{sfn|Szabó|2012|p=182}} His father died in 1534.{{sfn|Markó|2006|p=282}}

His brother, Andrew, and their kinsman, [[Tamás Nádasdy]], took charge of Christopher's education.{{sfn|Szabó|2012|p=182}} Christopher visited England, France, Italy, Spain, and the [[Holy Roman Empire]] in his youth.{{sfn|Markó|2006|p=101}}{{sfn|Szabó|2012|p=182}} He also served as a page in [[Charles V, Holy Roman Emperor|Emperor Charles V]]'s court.{{sfn|Szabó|2012|p=182}}

== Career ==
Christopher entered the service of John Zápolya's widow, Isabella Jagiellon, in the late 1550s.{{sfn|Szabó|2012|p=182}} At the time, Isabella administered the eastern territories of the Kingdom of Hungary on behalf of her son, John Sigismund Zápolya.{{sfn|Barta|1994|pp=258–259}} She wanted to persuade [[Henry II of France]] to withdraw his troops from three fortresses that the Ottomans had captured in [[Banat]], so she sent Christopher to France to start negotiations in 1557.{{sfn|Markó|2006|p=101}}{{sfn|Szabó|2012|p=182}}

John Sigismund took charge of the administration of his realm after his mother died on 15 November 1559.{{sfn|Barta|1994|p=259}}{{sfn|Keul|2009|p=99}} He retained his mother's advisors, including Christopher who became one of his most influential officials.{{sfn|Keul|2009|p=99}} After the rebellion of [[Melchior Balassa]],{{sfn|Barta|1994|p=259}} Christopher persuaded John Sigismund to fight for his realm instead of fleeing to Poland in 1562.{{sfn|Szabó|2012|p=182}} Christopher was one of the commanders of John Sigismund's troops during the ensuing war against the Habsburg rulers of the western territories of the Kingdom of Hungary, [[Ferdinand I, Holy Roman Emperor|Ferdinand]] and [[Maximilian II, Holy Roman Emperor|Maximilian]], who tried to reunite the kingdom under their rule.{{sfn|Markó|2006|p=101}}{{sfn|Barta|1994|p=259}} Christopher defeated Maximilian's commander, [[Lazarus von Schwendi]], forcing him to lift the siege of Huszt (now [[Khust]] in Ukraine) in 1565.{{sfn|Markó|2006|p=101}}

After the death of John Sigismund, the [[Diet of Transylvania]] elected Christopher's younger brother, Stephen Báthory, voivode (or ruler) on 25 May 1571.{{sfn|Barta|1994|p=260}} Stephen made Christopher captain of Várad (now Oradea in Romania).{{sfn|Markó|2006|p=101}} The following year, the [[Ottoman Sultan]], [[Selim II]] (who was the overlord of Transylvania), acknowledged the hereditary right of the [[Báthory family]] to rule the province.{{sfn|Barta|1994|p=260}}

== Reign ==
[[Image:Partium1570.PNG|thumb|right|alt=Transylvania and the neighboring regions|[[Principality of Transylvania (1570–1711)|Principality of Transylvania]] in 1570]]

Stephen Báthory was elected King of Poland on 15 December 1575.{{sfn|Barta|1994|p=260}} He adopted the title of Prince of Transylvania and made Christopher voivode on 14 January 1576.{{sfn|Markó|2006|p=101}}{{sfn|Szegedi|2009|p=101}} An Ottoman delegation confirmed Christopher's appointment at the Diet in Gyulafehérvár (now [[Alba Iulia]] in Romania) in July.{{sfn|Szabó|2012|p=182}}{{sfn|Granasztói|1981|p=404}} The sultan's charter (or ''[[ahidnâme]]'') sent to Christopher emphasized that he should keep the peace along the frontiers.{{sfn|Felezeu|2009|p=54}} Stephen set up a separate chancellery in Kraków to keep an eye on the administration of Transylvania.{{sfn|Barta|1994|p=263}}{{sfn|Felezeu|2009|p=27}} The head of the new chancellery, Márton Berzeviczy, and Christopher cooperated closely.{{sfn|Szabó|2012|p=182}}

[[Nontrinitarianism|Anti-Trinitarian]] preachers began to condemn the worshiping of Jesus in [[Partium]] and [[Székely Land]] in 1576, although the Diet had already forbade all doctrinal innovations.{{sfn|Keul|2009|pp=119, 121}} Ferenc Dávid, the most influential leader of the Unitarian Church of Transylvania, openly joined the dissenters in the autumn of 1578.{{sfn|Keul|2009|p=121}} Christopher invited [[Fausto Sozzini]], a leading Anti-Trinitarian theologian, to Transylvania to convince Dávid that the new teaching was erroneous.{{sfn|Barta|1994|p=260}} Since Dávid refused to obey, Christopher held a Diet and the "[[Unio Trium Nationum|Three Nations]]" (including the Unitarian delegates) ordered Dávid's imprisonment.{{sfn|Barta|1994|p=260}}{{sfn|Szegedi|2009|pp=106–107}} Christopher also supported his brother's attempts to strengthen the position of the Roman Catholic Church in Transylvania.{{sfn|Szabó|2012|p=182}}{{sfn|Barta|1994|p=260}} He granted estates to the Jesuits to promote the establishment of a college in Kolozsvár (now [[Cluj-Napoca]] in Romania) on 5 May 1579.{{sfn|Granasztói|1981|p=405}}

Christopher fell seriously ill after his second wife, Elisabeth Bocskai, died in early 1581.{{sfn|Szabó|2012|p=182}} At his request, the Diet elected his only surviving son [[Sigismund Báthory|Sigismund]] as voivode, although Sigismund was still a minor.{{sfn|Markó|2006|p=101}}{{sfn|Szabó|2012|p=182}} Christopher died in Gyulafehérvár on 27 May 1581.{{sfn|Markó|2006|p=101}} He was buried in the Jesuits' church in Gyulafehérvár, almost two years later, on 14 March 1583.{{sfn|Szabó|2012|p=182}}

== Family ==
{{ahnentafel top|width=100%|Ancestors of Christopher Báthory{{sfn|Horn|2002|p=245}}}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Christopher Báthory'''

|2= 2. [[Stephen VIII Báthory]]
|3= 3. [[Catherine Telegdi]]

|4= 4. Miklós Báthory
|5=
|6=.
|7=

|8= 8. Stephen Báthory
|9= 9. Dorothea Várdai
|10=
|11=
|12=
|13=
|14=
|15=

|16= 16. Stanislaus Báthory
|17= 17. Anna Szikszói
|18=
|19=
|20=
|21=
|22=
|23=
|24=
|25=
|26=
|27=
|28=
|29=
|30=
|31=
}}</center>
{{ahnentafel bottom}}

Christopher's first wife, Catherina Danicska, was a Polish noblewoman, but only the Hungarian form of her name is known.{{sfn|Szabó|2012|p=182}}{{sfn|Horn|2002|p=23}} Their eldest son, Balthasar Báthory, moved to Kraków shortly after Stephen Báthory was crowned King of Poland;{{sfn|Horn|2002|p=23}} he drowned in the [[Vistula River]] in May 1577 at the age of 22.{{sfn|Horn|2002|p=23}} Christopher's and Catherina's second son, Nicholas, was born in 1567 and died in 1576.{{sfn|Horn|2002|p=245}}

Christopher's second wife, Elisabeth Bocskai, was a [[Reformed Church in Hungary|Calvinist]] noblewoman.{{sfn|Szabó|2012|p=183}} Their first child, [[Griselda Báthory|Cristina (or Griselda)]], was born in 1569.{{sfn|Szabó|2012|p=183}} She was given in marriage to [[Jan Zamoyski]], [[Chancellor (Poland)|Chancellor of Poland]], in 1583.{{sfn|Horn|2002|pp=52, 55}} Christopher's youngest son, Sigismund, was born in 1573.{{sfn|Horn|2002|p=245}}

== References ==
{{Reflist|30em}}

== Sources ==
{{Refbegin}}
* {{cite book |last=Barta |first=Gábor |editor1-last=Köpeczi |editor1-first=Béla |editor2-last=Barta |editor2-first=Gábor |editor3-last=Bóna |editor3-first=István |editor4-last=Makkai |editor4-first=László |editor5-last=Szász |editor5-first=Zoltán |editor6-last=Borus |editor6-first=Judit | title=History of Transylvania |publisher=Akadémiai Kiadó |year=1994 |pages=247–300 |chapter=The Emergence of the Principality and its First Crises (1526–1606) |isbn=963-05-6703-2 |ref=harv}}
* {{cite book |last=Felezeu |first=Călin |editor1-last=Pop |editor1-first=Ioan-Aurel |editor2-last=Nägler |editor2-first=Thomas |editor3-last=Magyari |editor3-first=András | title=The History of Transylvania, Vo. II (From 1541 to 1711) |publisher=Romanian Academy, Center for Transylvanian Studies |year=2009 |pages=15–73 |chapter=The International Political Background (1541–1699); The Legal Status of the Principality of Transylvania in Its Relations with the Ottoman Porte |isbn=973-7784-04-9 |ref=harv}}
* {{cite book |last=Granasztói |first=György |editor1-last=Benda |editor1-first=Kálmán |editor2-last=Péter |editor2-first=Katalin |title=Magyarország történeti kronológiája, II: 1526–1848 ''[Historical Chronology of Hungary, Volume I: 1526–1848]'' |publisher=Akadémiai Kiadó |year=1981 |pages=390–430 |chapter=A három részre szakadt ország és a török kiűzése (1557–1605) |isbn=963-05-2662-X |language=hu |ref=harv}}
* {{cite book |last=Horn |first=Ildikó |year=2002  |title=Báthory András ''[Andrew Báthory]'' |publisher=Új Mandátum |isbn=963-9336-51-3 |language=hu |ref=harv}}
* {{cite book |last=Keul |first=István |year=2009 |title=Early Modern Religious Communities in East-Central Europe: Ethnic Diversity, Denominational Plurality, and Corporative Politics in the Principality of Transylvania (1526–1691) |publisher=Brill |isbn=978-90-04-17652-2 |ref=harv}}
* {{cite book |last=Markó |first=László |year=2006  |title=A magyar állam főméltóságai Szent Istvántól napjainkig: Életrajzi Lexikon ''[Great Officers of State in Hungary from King Saint Stephen to Our Days: A Biographical Encyclopedia]'' |publisher=Helikon Kiadó |isbn=963-547-085-1 |language=hu |ref=harv}}
* {{cite book |last=Szabó |first=Péter Károly |editor1-last=Gujdár |editor1-first=Noémi |editor2-last=Szatmáry |editor2-first=Nóra |title=Magyar királyok nagykönyve: Uralkodóink, kormányzóink és az erdélyi fejedelmek életének és tetteinek képes története ''[Encyclopedia of the Kings of Hungary: An Illustrated History of the Life and Deeds of Our Monarchs, Regents and the Princes of Transylvania]'' |publisher=Reader's Digest |year=2012 |pages=182–183 |chapter=Báthory Kristóf |isbn=978-963-289-214-6 |language=hu |ref=harv}}
* {{cite book |last=Szegedi |first=Edit |editor1-last=Pop |editor1-first=Ioan-Aurel |editor2-last=Nägler |editor2-first=Thomas |editor3-last=Magyari |editor3-first=András | title=The History of Transylvania, Vo. II (From 1541 to 1711) |publisher=Romanian Academy, Center for Transylvanian Studies |year=2009 |pages=99–111 |chapter=The Birth and Evolution of the Principality of Transylvania (1541–1690) |isbn=973-7784-04-9 |ref=harv}}
{{Refend}}

{{s-start}}
{{s-hou|[[Báthory|House of Báthory]]||1530|27 May|1581}}
{{s-off|}}
{{s-bef|before=[[Stephen Báthory]]}}
{{s-ttl|title=[[Voivode of Transylvania]]|years=1576–1581}}
{{s-aft|after=[[Sigismund Báthory]]}}

{{DEFAULTSORT:Bathory, Christopher}}
[[Category:1530 births]]
[[Category:1581 deaths]]
[[Category:Voivodes of Transylvania]]
[[Category:Báthory family|Christopher]]
[[Category:People from Șimleu Silvaniei]]
[[Category:16th-century Hungarian people]]