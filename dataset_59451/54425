{{Use Australian English|date=September 2014}}
{{Infobox Australian place
| type                   = protected
| name                   = Morialta Conservation Park
| state                  = sa
| iucn_category          = III
| image                  = Panorama, Morialta Conservation Park..jpg
| caption                = Morialta Conservation Park showing First Falls and Morialta Gorge, note [[scree]] slope at far right.
| image_alt              = 
| latd                   = 34
| latm                   = 54
| lats                   = 02
| longd                  = 138
| longm                  = 42
| longs                  = 38
| relief                 = yes
| map_alt                = 
| nearest_town_or_city   = Adelaide
| area                   = 5.33
| area_footnotes         = 
| established            = {{start date|1990|8|2|df=y}}
| established_footnotes  = 
| visitation_num         = 
| visitation_year        = 
| visitation_footnotes   = 
| managing_authorities   = [[Department of Environment, Water and Natural Resources (South Australia)|Department of Environment, Water and Natural Resources]]
| url                    = environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide_Hills/Morialta_Conservation_Park
}}
'''Morialta Conservation Park''' is a  [[protected area]] 10&nbsp;km north-east of [[Adelaide city centre]], in the state of [[South Australia]], [[Australia]]. The park is in a rugged bush environment, with a narrow gorge set with three [[waterfall]]s, bounded by steep ridges and cliffs. The park caters to many activities, including bushwalking, bird watching and [[Climbing|rock climbing]].

==History==
[[File:Giants cave - morialta falls park.jpg|thumb|right|Giant's Cave]]
The land which now constitutes the Park was originally the land of the [[Kaurna]] people. Morialta is said by many to derive its name from a [[Kaurna language|Kaurna]] word, ''moriatta'', meaning "ever flowing" or "running water", however some linguists suggest it comes from the [[Kaurna language|Kaurna]] words ''mari yertalla'', meaning "eastern cascade".<ref>http://press.anu.edu.au/wp-content/uploads/2011/03/ch182.pdf</ref> It is said that the area was used as a hunting ground, and to collect firewood, during the winter months when the Kaurna would retreat from the coast to the hills. It is also said that they practised [[fire-stick farming]] here.<ref>{{Citation | publication-date = June 2001
 | year = 2001 | title = Park Guide - Morialta Conservation Park | publication-place = Adelaide
 | publisher = Department for Environment and Heritage}}</ref><ref>http://www.anps.org.au/documents/June_2005.pdf</ref> The area's religious significance appears to be lost to time. For in 1839, only three years after the proclamation of the British colony of South Australia in 1836, the area was granted by the new South Australian Government to pastoralists.<ref>http://www.environment.sa.gov.au/parks/sanpr/morialta/european.html</ref> In 1847, [[John Baker (Australian politician)|John Baker]] bought the land and built the grand [[Morialta House]] and [[Morialta Barns]] on [[River Torrens#Tributaries|Fourth Creek]], near the head of the gorge.<ref>http://www.adb.online.anu.edu.au/biogs/A030076b.htm</ref> [[John Smith Reid]] was also a major landholder in the area, and in 1911, he offered to donate part of his land as a national reserve.

Reid donated {{convert|218|ha}} in 1913, and in 1915 the area was declared a National Pleasure Resort. Much of the construction work in the park was begun in the 1920s and 1930s, although floods and bushfires have destroyed much of this original work. In 1966 additional property to the east was added, and the park was declared a National park. In 1972 the park was re-proclaimed as Morialta Conservation Park.

Major rebuilding was required after flooding in 1980, and flooding in November 2005 again caused damage to paths and walkways.

==Geography and climate==
[[File:First Falls, Morialta Conservation Park, Adelaide. Peter Neaum. - panoramio.jpg|thumb|right|First Falls]]
[[File:Second Falls.jpg|thumb|right|Second Falls]]
Morialta Conservation Park covers 5.33&nbsp;km² within the [[Mount Lofty Ranges]], which run north-south to the east of Adelaide's coastal plain. It is bounded by [[Black Hill Conservation Park]] on the north, Norton Summit road on the south, the suburb of [[Rostrevor, South Australia|Rostrevor]] on the west, and by agricultural land on the east.

The park lies mostly on either side of Morialta Gorge, along the bottom of which runs Fourth Creek. There are three waterfalls on the creek, named respectively First, Second and Third Falls.

The main access to the park is via the vehicle entrance off Morialta Road, and there is walking access at various points along Norton Summit Road.

Morialta shares Adelaide's [[Mediterranean climate]], with average temperatures of 17 degrees Celsius in winter, to 28 degrees during summer. The park receives average annual rainfall of 800&nbsp;mm, mostly between May and September. During summer (December to February) temperatures can rise above 40 degrees.

==Activities==
The park caters for a variety of activities, including bushwalking, picnics, rock climbing and bird watching. There are many walking paths within the park.

==Rock Climbing==
[[File:Morialta Toproping.JPG|thumb|right|Toproping ''Balthazar'' (12) near Boulder Bridge.]]
Being only 10&nbsp;km from the centre of [[Adelaide]] and having a large number of climbs, Morialta is one of the most popular rock climbing areas in [[South Australia]].<ref>Pritchard 2002 p 175</ref> The rock is quartzite, and there are heavy duty rings at the top of most routes to allow for easy top-roping.<ref>Neagle 1997, p41</ref>

There are several crags along both sides of the gorge. On the southern side, from the western end, there is ''Milestone Buttress, The Outcrops, The Buttress, Billiard Table, Boulder Bridge, Throne Room'' and ''Far Crag''. On the northern side there is ''The Lost Walls'' and ''Thorn Buttress''.<ref>Neagle 1997, p47</ref> Far Crag is one of the more popular crags, with over 67 routes graded from 4 ([[John Ewbank (climber)|Ewbanks]]) to 27.

===History===
The first climbing at Morialta was in 1962 by a group from the Adelaide University Mountain Club (AUMC). Four members of the club were given an introduction by the National Fitness Council and began regular climbing at the site. Access at this time was from the western end of the park (the Adelaide side) and so the cliffs to the western end were climbed first. No documentation of the climbing occurred until 1968, when ''Far Crag'' was discovered. It was named such because it is the furthest from the western end and so had the longest walk to reach it.<ref>Neagle 1997, pp41-43</ref>

A large number of routes were put up at the park up until the 1970s. A visit by international climbers in 1971 saw the standards of free climbing increase at the park, with many new bold routes being led. The late-1970s saw grades at the park reach 25.<ref>Neagle 1997, p44</ref>

Remarkably, ''The Lost Walls'' were not discovered until 1986, hence their name and is the most recent addition of a significant number of climbs. 1994 saw the highest graded new route put up in the park, at 27.<ref>Neagle 1997, p45</ref> Access is now from Norton Summit Road, on the southern side of the park.

===Style of climbing===
The highest volume of climbing in the gorge is done on [[Top roping|top rope]], which is made especially easy by the provision of regularly tested steel rings at the tops of many climbs.  These rings greatly simplify the task of building an anchor, making outdoor climbing accessible to a broader range of people.

Although most routes can be top-roped, there are routes which are bolted for [[sport climbing]], or suitable for [[traditional climbing]] with natural gear, or a mixture of both.  Often a route which is suitable for sport or traditional climbing can also be top-roped.

==Notes and references==
;Notes
{{reflist}}
;References
*{{Citation | author1=Pritchard, Greg | title=Climbing Australia : the essential guide | publication-date=2002 | publisher=New Holland | isbn=978-1-86436-694-5 }}
*{{Citation | author1=Neagle, Nick | title=The Adelaide Hills : a rock climber's guide | publication-date=1997 | publisher=N. Neagle | isbn=978-0-646-32427-2 }}
*{{Citation | author1=Department for Environment and Heritage (DEH)  | title=Morialta and Black Hill Conservation Parks Management Plan | publication-date=2001 | publisher=Dept. for Environment and Heritage|url=http://www.environment.sa.gov.au/files/sharedassets/public/park_management/parks_pdfs_morialta_blackhill.pdf | isbn=0 7590 1000 5 }}

==External links==
*[http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide_Hills/Morialta_Conservation_Park/ Morialta Conservation Park official webpage]
*[http://www.protectedplanet.net/morialta-conservation-park Morialta Conservation Park webpage on protected planet]
*[http://www.fobhm.org Friends of Black Hill and Morialta Conservation Parks], retrieved 3-12-2005
{{Protected areas of South Australia}}
{{Use dmy dates|date=November 2010}}

{{DEFAULTSORT:Morialta Conservation Park}}
[[Category:Conservation parks of South Australia]]
[[Category:Protected areas established in 1990]]
[[Category:1990 establishments in Australia]]
[[Category:Climbing areas of Australia]]