'''Eileen Vollick''' (2 August 1908 &ndash; 27 September 1968) became Canada's first licensed female pilot on 13 March 1928. She was also the first Canadian woman to parachute into water.<ref name="Allen">Allen, Shirley. [http://www.canadian99s.org/articles/p_vollick.htm “Pioneer Women Pilots: Eileen Vollick.”] ''Canadian99s''. Retrieved: 26 June 2010.</ref>

==Early life==
Mary Eileen Vene Riley was born at home on August 2 1908 to James and Mary Riley in [[Wiarton, Ontario]].<ref name="Vlerebome">Vlerebome, Peggy. “Canada Honors Pioneer Aviatrix Posthumously.” ''Largo-Seminole Times'', 1976.</ref> James Riley was killed in a mining accident in 1911. Mary Riley remarried to George Vollick and the newly formed family moved to [[Hamilton, Ontario]]. Eileen was three years old when she received her stepfather's last name.<ref name="Kastner">Kastner, Paul. [http://www.trentonian.ca/ArticleDisplay.aspx?archive=true&e=1114870 “Wiarton Event to Honour Pioneering Woman Aviator”.] ''Wiarton Echo''. Retrieved: 26 June 2010. </ref>  She graduated from St. Patrick’s High School in Hamilton, Ontario.<ref name=" Vlerebome " />

==Career==
Eileen Vollick worked at the Hamilton Cotton Co. as a textile analyst and assistant designer. Both from her bedroom window and on her way to work each morning, she watched takeoffs and landings at Jack V. Elliot’s Air Service and longed for the opportunity to learn to fly. Vollick applied for government permission to learn to fly commercially and was granted permission to take flying lessons when she turned 19. While waiting for her 19th birthday, Eileen became the first Canadian woman to parachute into water. She walked the wings of a Curtiss JN-4 (often called a “Jenny”) and parachuted 2,800 feet into Hamilton Bay, which has since been renamed [[Burlington Bay]].

When her 19th birthday arrived, Vollick officially became a student at Jack V. Elliot's Flying School at Ghents Crossing overlooking Hamilton Bay. Despite doubts, she was determined to earn her license. She took 6 a.m. lessons before going to work at 8:30 a.m.<ref name=" Vlerebome " /> Pilots Earl Jellison, Lennard Tripp and Richard Turner served as her instructors and also taught her aviation mechanics.<ref name=" How I Became ">Vollick, Eileen. [http://www.kingstonthisweek.com/ArticleDisplay.aspx?archive=true&e=1145293  “How I Became Canada’s First Licensed Woman Pilot”.] ''Owen Sound Sun Times'', 2009. Retrieved: 26 June 2010.</ref>

Since Vollick weighed a mere 89 pounds and was only 5 feet 1 inch tall, she used pillows to prop herself up to see out of the cockpit of the [[Curtiss JN-4]]. On 13 March 1928, Vollick received time off from her job at the Hamilton Cotton Co. in order to take her federal aviation test.<ref name=" Vlerebome " /> She demonstrated her knowledge of take-offs and landings on the frozen bay. In order to pass the test, the applicant had to make four landings from 1,500 feet and land within 150 feet of a designated point on the ground. An additional landing had to be executed with the motor off and the pilot had to land within 5,000 feet of a designated point. Other requirements of the test included performing five figure-eight turns between two designated points and completing a 175-mile cross-country trip.

Vollick successfully passed the test on 13 March 1928 along with 10 other male cadets of the Elliot Flying School.<ref name=" How I Became " />  Eileen Vollick was issued Private Pilot Certificate No. 77 on 22 March 1928.

==Marriage==
Shortly after obtaining her license, Vollick met James Hopkin. They married on 28 September 1929 in St. Patrick’s Church Rectory, [[Hamilton, Ontario]]. The couple moved to [[Elmhurst, Queens]] in New York City and raised two daughters, Audrey Joyce Miles and Eileen Barnes. Eileen Vollick lived in New York until her death in 1968. She is buried in [[Woodlawn Cemetery (Bronx)|Woodlawn Cemetery]], Bronx, New York.<ref name=" Vlerebome " />

==Legacy and honors==
In 1975, the First Canadian Chapter posthumously awarded Vollick with an Amelia Earhart Medallion. In 1976, the [[Ninety-Nines]], an international organization of female pilots, and the Ontario Heritage Foundation held a ceremony to reveal a plaque at the [[John C. Munro Hamilton International Airport]] commemorating Vollick's accomplishments. One of her instructors, Lennard Tripp was present at this ceremony. <ref name=" Allen " /> On August 2, 2008 (what would have been her 100th birthday) the Eileen Vollick Terminal was named in her honor at the Wiarton-Keppel International Airport. According to researcher and writer, Marilyn Dickson, this is the first Canadian airport to name a terminal after a woman.<ref name=" Kastner" />

==References==
===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Ellis, Frank H. ''In Canadian Skies.'' Toronto, Ontario, Canada: Ryerson Press, 1959. 
* Forster, Merna. ''100 Canadian Heroines: Famous and Forgotten Faces.'' Toronto, Ontario, Canada: Dundurn Press, 2004. ISBN 978-1-55002-514-9.
* Render, Shirley. ''No Place for a Lady: The Story of Canadian Women Pilots, 1928-1992.'' Winnipeg, Manitoba, Canada: Portage & Main Press, 1992. ISBN 978-0-96942-642-4.
{{Refend}}

==External links==
* [http://www.ontarioplaques.com/Plaques_GHI/Plaque_Hamilton10.html "Eileen Vollick Plaque in Hamilton"]

{{DEFAULTSORT:Vollick, Eileen}}
[[Category:1908 births]]
[[Category:1968 deaths]]
[[Category:Burials at Woodlawn Cemetery (Bronx)]]
[[Category:Canadian aviators]]
[[Category:Canadian emigrants to the United States]]
[[Category:People from Bruce County]]
[[Category:People from Hamilton, Ontario]]
[[Category:People from Long Island]]
[[Category:Canadian female aviators]]
[[Category:Aviation pioneers]]