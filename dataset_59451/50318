{{Use dmy dates|date=May 2015}}
{{Infobox university
|name=Zayed University
|image_name=Zayed University (logo).png
|image_size=120px
|established={{start date and age|1998}}
|president=[[Sheikha Lubna Al Qasimi]]
|vice-president=Reyadh Almehaideb
|city=[[Abu Dhabi]] and [[Dubai]]
|country=[[United Arab Emirates]]
|website= [http://www.zu.ac.ae www.zu.ac.ae]}}

'''Zayed University''', "جامعة زايد" is one of three government-sponsored higher education institutions in the [[United Arab Emirates]]. Achieving accreditation by the Middle States Commission on Higher Education in the USA, it became the first federal university in the UAE to be internationally accredited.<ref>{{cite web |url=http://www.uaeinteract.com/docs/Zayed_University_achieves_international_accreditation/30895.htm|title=Zayed University achieves international accreditation}}</ref> It is named in honor of [[Zayed bin Sultan Al Nahyan]], the country's first president. The University has six colleges: College of Arts and Sciences, College of Business Sciences, College of Communication and Media Sciences, College of Education, College of Information Technology, and University College.<ref>{{cite web|url=http://www.zu.ac.ae/main/en/colleges/index.aspx|title=Colleges at Zayed University}}</ref>

==History==
Zayed University was established in 1998 by the Emirati federal government. Until 2008 the university was accepting only UAE national women, but after the opening of Sweihan campus, a collaboration between Zayed University and the UAE Armed Forces, approximately 200 male students were admitted.<ref>{{cite web|url=http://www.zu.ac.ae/main/en/_careers/working/vision.aspx#zuhistory|title=History of Zayed University}}</ref>

The university is currently engaged in cooperative relationships with a number of institutions throughout the world such as: Al-Maktoum Institute for Arabic and Islamic Studies in [[Scotland]], [[Australian National University]], School of Business Management and Organization of the Foundation Antonio Genovesi Salerno in [[Italy]], Universidad Complutense de Madrid in [[Spain]] and [[Waseda University]] in [[Japan]].<ref>{{cite web |url=http://www.zu.ac.ae/main/en/explore_zu/cooperation.aspx |title=Cooperation with Other Institutions}}</ref>

In November 2014, Zayed University was ranked 23rd out of 25 in the QS World University Arab Rankings.<ref>[http://www.khaleejtimes.com/kt-article-display-1.asp?xfile=data/educationnation/2014/November/educationnation_November15.xml&section=educationnation "UAE universities top regional charts"], ''[[Khaleej Times]]'', 18 November 2014.</ref> It does not feature in any world rankings.<ref>{{cite web |url=http://www.timeshighereducation.co.uk/world-university-rankings/2012-13/world-ranking |title=World University Rankings, 2012-2013|work=Times Higher Education|accessdate=19 March 2013 }}</ref><ref>{{cite web |url=http://www.usnews.com/education/worlds-best-universities-rankings/top-400-universities-in-the-world |title=Top 400 Universities in the World|work=U.S. News|accessdate=19 March 2013 }}</ref><ref>{{cite web |url=http://www.shanghairanking.com/ |title=QS World Rankings of Universities|work=Shanghai Rankings|accessdate=19 March 2013 }}</ref>

==Accreditation==
In 2008, Zayed University announced that it received accreditation from the [[Commission on Higher Education of the Middle States Association of Colleges and Schools]].<ref>{{cite web |url=http://www.insidehighered.com/news/2013/04/03/us-accreditors-expand-their-activities-overseas |title=U.S. accreditors expand their activities overseas|work=Inside Higher Education|accessdate=3 April 2013}}</ref> Zayed University announced it had received re-accreditation in 2013.<ref>{{cite web |url=http://www.ameinfo.com/162048.html|title=Zayed University achieves international accreditation|accessdate=30 June 2013}}</ref> The University is one of only 16 overseas institutions accredited by the MSCHE, and one of only 4 in the Middle East.<ref>{{cite web|url=https://www.msche.org/Institutions_Directory.asp|title=Middle States Commission on Higher Education|work=Msche|accessdate=15 May 2015}}</ref>

Programs within its College of Technological Innovation obtained  accreditation through the [http://main.abet.org/ Accreditation Board for Engineering and Technology (ABET)] in the summer of 2012.<ref>{{cite web|url=http://main.abet.org/aps/AccreditedProgramsDetails.aspx?OrganizationID=7922|title=Search - ABET|work=abet.org|accessdate=15 May 2015}}</ref>

Programs within its College of Business obtained accreditation through the [http://www.aacsb.edu/ Association for the Advancement of Collegiate Schools of Business International (AACSB) accreditation] in June 2013.<ref>{{cite web|url=https://www.aacsb.net/eweb/DynamicPage.aspx?Site=AACSB&WebKey=CADEF77A-6573-49BB-9BA2-CE21EC93841E|title=AACSB List of Member Schools by Country|work=AACSB|accessdate=15 May 2015}}</ref>

Through the Council for Accreditation of Educator Preparation Continuous Improvement Commission (CAEP),<ref>http://caepnet.org/news/caep-update-may-21-2014/</ref> in 2013 the College of Education received accreditation by the National Council for Accreditation of Teacher Education.  Zayed University College of Education is the first university, outside of the USA, to be internationally accredited.

Programs within its College of Communication and Media Sciences obtained accreditation through [http://www2.ku.edu/~acejmc/ the Accrediting Council on Education in Journalism and Mass Communications (ACEJMC)] in May 2015.<ref>{{cite web|url=http://www2.ku.edu/~acejmc/|title=ACEJMC Homepage|work=ku.edu|accessdate=15 May 2015}}</ref>

Programs within its College of Arts and Creative Enterprises were recognized as substantially equivalent through [http://nasad.arts-accredit.org/index.jsp NASAD] in July 2015. <ref>http://nasad.arts-accredit.org/index.jsp?page=List-Substantial_Equivalency_Programs</ref>

==Outcomes-based academic program model==
Zayed University has adopted an outcomes-based academic program model. The programs are outcomes-based and designed with reference to the Zayed University Learning Outcomes. These were designed by hired U.S. consultants as a means of developing the necessary outcomes to prepare students for the world.<ref>{{cite web |url=http://www.zu.ac.ae/main/en/colleges/colleges/site_university_college/Colloquy/ZU_Learning_Outcomes.aspx |title=Zayed University Learning Outcomes in Colloquy Program|work=Zayed University|accessdate=27 March 2011 }}</ref>

The major outcomes-based programs are housed the six academic outcomes-based colleges. Outcomes-based majors are based on Zayed University's learning outcomes; they are discipline specific, yet outcomes based!<ref>{{cite web |url=http://www.zu.ac.ae/catalog/documents/pdf/2008Catalog.academicProgramModel.pdf|title=Zayed University Learning Outcomes in Zayed University Catalog |work=Zayed University|accessdate=27 March 2011 }}</ref>

The university incorporates meditation.<ref>{{Cite web|url=http://www.edarabia.com/122669/meditation-and-breathing-help-zayed-university-students-relieve-stress/|title=Meditation and breathing help Zayed University students relieve stress|website=www.edarabia.com|language=en-US|access-date=2016-05-02}}</ref>

==Campus== 

The university's campus in [[Abu Dhabi]] moved to a new campus in [[Khalifa City]] in 2011.<ref>{{cite web |url=http://www.thenational.ae/news/uae-news/teething-problems-at-new-dh3-7bn-zayed-university-campus |title=Teething problems at new Dh3.7bn Zayed University campus |work=The National |accessdate=12 September 2011 }}</ref> Its Dubai campus moved to its current Al Ruwayyah location, near [[Academic City]], in 2006.

The original campus was near the northern end of the Abu Dhabi peninsula, on Delma Street.<ref>{{cite web | url=http://zu.ac.ae/main/en/research/about/office_of_research/location.aspx |title=Locations |accessdate=25 August 2016}}</ref>  Plans for future campuses in other Emirates have been in development since the early 2000s.  Land was allotted in the Emirate of [[Ras al Khaimah]] for the building of a new campus in 2001, but building was delayed due to a drop in interest in the venture.<ref>{{cite web |url=http://gulfnews.com/news/uae/general/rak-allots-land-to-set-up-branch-of-zayed-university-1.433176| title=RAK allots land to set up branch of Zayed University |accessdate=25 August 2016}}</ref>

[[File:ZU_Abu_Dhabi_Campus.jpeg|thumb|upright=2.0|ZU Abu Dhabi Campus]]

==Controversies==
===Management concerns===
In December 2010, the [[Federal National Council]] queried the competency of the university's senior management. According to ''[[The National (Abu Dhabi)|The National]]'', Zayed University was reported to owe over Dh33 million in unpaid water and electricity bills.<ref>{{Cite news|author=Kareem Shaheen|title=Debt worry over federal universities|url=http://www.thenational.ae/news/uae-news/education/debt-worry-over-federal-universities|newspaper=The National|date=21 December 2010}}</ref>

According to ''[[The National (Abu Dhabi)|The National]]'', three people held the position of provost between April and June 2011, with a total of seven provosts between 1998 and 2011. This was confusing. <ref>{{cite web |url=http://www.thenational.ae/news/uae-news/education/zayed-university-provost-back-at-helm-amid-confusion|title=Zayed University provost back at helm amid confusion|work=The National|accessdate=20 June 2011 }}</ref>

In 2012, the effectiveness of its teacher education program was questioned. According to ''[[The National (Abu Dhabi)|The National]]'', none of the 110 teachers it produced between 2010 and 2012 were employed by the [[Abu Dhabi Education Council]] (ADEC); ADEC allegedly claimed the university produced lazy and poorly skilled graduates.<ref>{{Cite news|author=Melanie Swan |title=Zayed University seeks dean to lead it into the future|url=http://www.thenational.ae/news/uae-news/education/zayed-university-seeks-dean-to-lead-it-into-the-future|newspaper=The National|date=18 April 2012}}</ref>

In early 2013, the founding president of Zayed University, [[Nahyan bin Mubarak Al Nahyan]], was dismissed as the U.A.E.'s Minister of Higher Education and Scientific Research. He was surprisingly moved to the Ministry of Culture, Youth and Community Development. This was announced on Twitter by Prime Minister Sheikh [[Mohammed bin Rashid Al Maktoum]]. Sheikh Nahyan was subsequently displaced as president of Zayed University. Most members of the university's senior administration were also dismissed in 2013, including vice president Sulaiman Al Jassim and provost Larry Wilson. Management was also shaken up within colleges and departments, such as the University Library.<ref>{{cite web|url=https://www.academia.edu/28794488/Librarians_Need_Global_Credentials |author=[[Woody Evans]] |title=Librarians Need Global Credentials |accessdate=24 August 2016}}</ref> Maitha Al Shamsi was then instated as president in 2013.<ref>{{cite web|url=http://www.al-fanar.org/2013/03/longtime-education-minister-in-the-emirates-moves-aside/|title=Longtime Emirati Education Minister Moves Aside|work=Al Fanar|accessdate=19 March 2013}}</ref> Al Shamsi was soon followed by  [[Lubna Khalid Al Qasimi]], who was appointed president in 2014.<ref>[http://wam.org.ae/servlet/Satellite?c=WamLocEnews&cid=1290009746584&pagename=WAM%2FWAM_E_Layout&parent=Collection&parentid=1135099399983 WAM 4 March 2014]</ref> 

As well as existing concerns over academic integrity, transparency, and plagiarism at Zayed University, concerns have been raised about the weak academic credentials and lack of international experience among the new management.<ref>{{cite web|url=http://www.al-fanar.org/2013/04/uae-higher-education-power-shifts-phase-2/ |title=UAE higher education power shifts|work=Al-Fanar|accessdate=16 April 2013 }}</ref>

In 2013, then-president Maitha Al Shamsi announced Zayed University would be completely restructured. Though Al Shamsi did not explain what kind of restructuring she would implement, she said Zayed University would now be based on the U.A.E. government's Charter for National Values and Ethics and would undertake a total revision of all "academic programmes and management polices."<ref>{{Cite news|author=Shafaat Shahbandari|title=Major restructuring of ZU on cards |url=http://gulfnews.com/news/gulf/uae/major-restructuring-of-zu-on-cards-1.1232645|newspaper=Gulf News|date=17 September 2013}}</ref>

===Salary controversies===
In August 2008, [[Emirati]] staff received a 28% pay award, whereas foreign faculty received a 5% pay award. The university would not comment on the case.<ref>{{Cite news|author=Daniel Bardsley |title=Meagre pay rise angers academics|url=http://www.thenational.ae/news/uae-news/meagre-pay-rise-angers-academics |newspaper=The National|date=18 August 2008}}</ref>

In May 2010, a salary freeze was imposed at the university.<ref>[http://www.thenational.ae/apps/pbcs.dll/article?AID=/20100513/NATIONAL/705129837/1019 Pay Frozen and Job Losses Loom], ''The National'', 13 May 2010</ref>

In January 2011, ''[[The National (Abu Dhabi)|The National]]'' reported that Zayed University staff would receive a 2% pay raise retroactive to August 2010, which reportedly would be their first pay raise in three and a half years. The provost later acknowledged that "salaries have not kept pace with inflation."<ref>{{cite web |url=http://www.thenational.ae/news/uae-news/education/zayed-university-staff-to-get-2-per-cent-pay-increase|title=Zayed University staff to get 2 per cent pay increase|work=The National|accessdate=26 March 2011 }}</ref>

===Student concerns===
On 16 September 2013, ''[[The National (Abu Dhabi)|The National]]'' reported a torrent of complaints by students at Zayed University against policy changes introduced by the new president, Maitha Al Shamsi. These changes include sending SMS messages to parents when students arrive or leave campus, increasing faculty teaching loads, and preventing students from changing their class schedules.<ref>{{cite web|url=http://www.thenational.ae/rule-changes-spark-mixed-reactions-at-zayed-university|title=Rule changes spark mixed reactions at Zayed University|work=The National|accessdate=17 September 2013}}</ref> Then-provost Abdalla Al Amiri angrily rebuffed the article the following day, explaining that the SMS system is optional, no faculty teach 15 credit hours per semester, and that student schedules can be changed in University College.<ref>{{cite web|url=http://www.thenational.ae/zayed-universitys-vision-will-remain-firm-and-untouched|title=Zayed University’s vision will remain ‘firm and untouched’|work=The National|accessdate=18 September 2013 }}</ref>

On 18 September 2013, the ''[[Gulf News]]'' reported that Zayed University would be completely restructured.<ref>{{cite web |url=http://gulfnews.com/news/gulf/uae/major-restructuring-of-zu-on-cards-1.1232645#.UjqRnQGsyTo.twitter|title=Major restructuring of ZU on cards|work=Gulf News|accessdate=18 September 2013 }}</ref>

===Lack of academic freedom===
In 2012, an American journalism professor working in the College of Communication and Media Sciences was dismissed from the Abu Dhabi campus.<ref>{{cite web |url=http://www.arabmediasociety.com/index.php?article=831&p=0|title=Two Years in Abu Dhabi: Adventures teaching journalism in the UAE during the Arab Spring |work=Arab Media & Society'|accessdate=1 August 2013 }}</ref> The professor, Matt J. Duffy, expressed concern that his activities-—which included writing for ''[[Gulf News]]'', launching a student chapter of the [[Society of Professional Journalists]], and teaching objectively about the U.A.E.'s media laws—-may have led to his dismissal.<ref>{{cite web|url=http://www.insidehighered.com/news/2012/08/31/american-professor-suddenly-fired-zayed-university|title=American Professor Suddenly Fired from Zayed University|work=Insider Higher Ed|accessdate=31 August 2012}}</ref> Duffy's dismissal breached policies on [[academic freedom]] laid out by the [[Commission on Higher Education of the Middle States Association of Colleges and Schools]], which state, "To impose political considerations upon faculty selection and retention harms an institution intellectually and educationally, not only by reducing its options in the recruitment of talent, but also by creating pressures against dissent on important policy issues."<ref>{{cite web|url=http://www.msche.org/?Nav1=POLICIES&Nav2=INDEX|title=Political Intervention in Education |work=MSCHE|accessdate=27 September 2012}}</ref> Despite claiming to follow Article 19 of the United Nations [[Universal Declaration of Human Rights]] in its [[Middle States Association of Colleges and Schools|MSCHE]] Self-Study ("Everyone has the right to freedom of opinion and expression; this right includes freedom to hold opinions without interference and to seek, receive, and impart information and ideas through any media regardless of frontiers"), the university would not comment on the case in public.<ref>{{cite web|url=http://chronicle.com/article/Why-Was-I-Fired-From-Zayed-U-/134114|title=Why was I fired from Zayed U.? |work=The Chronicle for Higher Education|accessdate=3 September 2012 }}</ref>

Materials deemed to be offensive are removed from the University Library, or are deposited in a locked holding space called Special Collections. Students must obtain faculty approval in order to access these materials, which include books on nursing, art magazines, human sexuality, and books containing views critical of religion.<ref>{{cite web |url=http://www.zu.ac.ae/infoasis/modules/mod8/health/4_books.htm |title=ZU Library Infoasis |accessdate=25 August 2016}}</ref>

==References==
{{Reflist|colwidth=30em}}

==External links==
* [http://www.zu.ac.ae University website]
* [http://www.glassdoor.com/Overview/Working-at-Zayed-University-EI_IE423746.11,27.htm Reviews]
* [http://zayed.wikia.com/wiki/Zayed_University_Wiki Zayed University Wiki: The ZU Encyclopedia]

{{Coalition of Urban and Metropolitan Universities}}

{{coord|25.102019|55.385599|display=t|type:edu}}

[[Category:Public universities in the United Arab Emirates]]
[[Category:Universities and colleges in Abu Dhabi (emirate)]]
[[Category:Educational institutions established in 1998]]
[[Category:Women's universities and colleges]]
[[Category:1998 establishments in the United Arab Emirates]]
[[Category:Zayed University| ]]