{{Redirect|Winnipeg Airport|other airports in Winnipeg|List of airports in the Winnipeg area}}
{{Use mdy dates|date=October 2012}}
{{Infobox airport
| nativename = <sub>''Aéroport international James Armstrong Richardson de Winnipeg''</sub>
| image2-width = 250
| hub =
*[[Calm Air]]
*[[Cargojet Airways|Cargojet]]
*[[Perimeter Aviation]]
*[[NewLeaf]]
*[[Westjet]]
| summer = CDT
| coordinates = {{coord|49|54|36|N|097|14|24|W|region:CA-MB|display=inline,title}}
| pushpin_label = CYWG
| r1-length-f = 8,701
| r1-surface = Asphalt
| name = Winnipeg James Armstrong Richardson International Airport
| image =Winnipeg James Armstrong Richardson International Airport (logo).svg
| image-width = 250
| image2 =Winnipeg_International_Airport_arrivals_hall.jpg
| IATA = YWG
| ICAO = CYWG
| WMO = 71852
| type = Public
| owner = [[Transport Canada]]<ref name="NAS">[http://www.tc.gc.ca/eng/programs/airports-status-menu-441.htm "Airport Divestiture Status Report."] ''Transport Canada.'' Retrieved: April 1, 2012.</ref>
| operator = Winnipeg Airports Authority ([[Department of National Defence (Canada)|DND]])
| city-served = [[Winnipeg]], [[Manitoba]]
| timezone = [[Central Time Zone|CST]]
| utc = [[UTC−06:00]]
| utcs = [[UTC−05:00]]
| elevation-f = 783
| pushpin_map = Canada Manitoba
| pushpin_map_caption = Location in Manitoba
| website = [http://www.waa.ca/ www.waa.ca]
| r1-number = 13/31
| r2-number = 18/36
| r2-length-f = 11,000
| r2-surface = Asphalt
| stat-year = 2014/2016
| stat1-header = Aircraft movements (2014)
| stat1-data = 123,778
| stat2-header = Number of Passengers (2016)
| stat2-data = 4,015,200
| footnotes = Sources: [[Canada Flight Supplement]]<ref name="CFS">{{CFS}}</ref><br>[[Environment Canada]]<ref>[http://climate.weatheroffice.gc.ca/prods_servs/metstat1_e.html "Synoptic/Metstat Station Information."] ''weatheroffice.gc.ca.'' Retrieved: April 1, 2012.</ref><br>Movements from [[Statistics Canada]]<ref name="move">[http://www.statcan.gc.ca/pub/51-209-x/2012001/t002-eng.htm "Total aircraft movements by class of operation – NAV CANADA towers."] ''Stats Canada.'' Retrieved July 19, 2012.</ref><br>Passenger statistics from Winnipeg Airports Authority<ref name="pax">[http://www.waa.ca/uploads/ck/files/April2016_Passenger_Statistics.pdf "Winnipeg James Armstrong Richardson Passenger Statistics."] ''Winnipeg Airports Authority.'' Retrieved: May 23, 2016.</ref>
}}

'''Winnipeg James Armstrong Richardson International Airport''' (commonly known as '''Winnipeg International Airport''' or simply '''Winnipeg Airport''') {{Airport codes|YWG|CYWG}} is an international airport located in [[Winnipeg]], [[Manitoba]], [[Canada]]. It is the seventh [[List of the busiest airports in Canada|busiest airport in Canada]] by passenger traffic, serving 4,015,200 passengers in 2016,<ref name="pax"/> and the 11th busiest airport by aircraft movements.<ref name="move"/> It is a hub for passenger airlines [[Calm Air]] and [[Perimeter Aviation|Perimeter Airlines]], and cargo airline [[Cargojet]]. It is also a focus city for [[WestJet]]. The airport is co-located with [[Canadian Forces Base Winnipeg]].

An important [[Transport hub|transportation hub]] for the [[Provinces and territories of Canada|province of Manitoba]], Winnipeg International Airport is the only commercial [[international airport]] within the province as the other [[airport of entry|airports of entry]] serve [[domestic flight]]s and [[general aviation]] only.<ref name="CFS"/> The airport is operated by the Winnipeg Airport Authority as part of [[Transport Canada]]'s [[National Airports System]]<ref>[http://www.tc.gc.ca/eng/programs/airports-policy-nationallist-303.htm "National Airports Policy."] ''Transport Canada.'' Retrieved: April 1, 2012.</ref> and is one of eight Canadian airports that has [[United States border preclearance|US Border Pre-clearance facilities]].

Winnipeg's relatively isolated geographical location in relation to other major population centres<ref>[http://www.akcanada.com/lic_winnipeg.cfm]</ref> makes Winnipeg International Airport the primary airport for a very large area. As such, it is used as a gateway to the entire Province of Manitoba and large parts of neighbouring [[Provinces of Canada|Provinces and Territories]].<ref>Schlesinger, Joel. [http://www.winnipegfreepress.com/local/port-on-the-prairies-45248947.html "Port on the Prairies: Supply-chain economics key to becoming international trade hub."] ''[[Winnipeg Free Press]]'', May 17, 2009. Retrieved December 30, 2011.</ref> Daily non-stop flights are operated from Winnipeg International Airport to destinations across Canada, the [[United States of America|United States]], [[Mexico]], and the [[Caribbean]], along with summer seasonal flights to the [[United Kingdom]]. In addition, regularly scheduled flights to numerous small [[Remote and isolated community|remote communities]] in the northern regions of Canada, specifically [[Northern Region, Manitoba|Northern Manitoba]], [[Northwestern Ontario]], and [[Nunavut]], are also served from the airport.<ref>[http://www.waa.ca/flights/airlines/ "Airlines>"] ''Winnipeg Airports Authorities.'' Retrieved April 1, 2012.</ref>

==History==
The airport opened in 1928 as Stevenson Aerodrome in honour of the noted Manitoba aviator and pioneer [[Bush flying|bush pilot]], Captain Fred J. Stevenson. Stevenson Aerodrome, also known as Stevenson Field, was Canada's first international airport. {{Citation needed|date=April 2016}} In 1958, at the request of the Canadian Department of Transport, Stevenson Field was officially renamed Winnipeg International Airport.

The original main terminal building was built in 1964, and was designed by the architectural firm of Green Blankstein Russell and Associates (subsequently GBR Associates and Stantec Limited). It was expanded and renovated in 1984 by the architectural firm of IKOY, and a hotel was built across from the terminal in 1998. The original main terminal building was closed on Sunday October 30, 2011 and has since been demolished.

On December 10, 2006, the [[Minister of Transport (Canada)|Minister of Transport]], [[Lawrence Cannon]], announced Winnipeg International Airport was to be renamed Winnipeg [[James Armstrong Richardson, Sr.|James Armstrong Richardson]] International Airport in honour of the influential businessman and pioneer of Canadian commercial aviation from Winnipeg.<ref>[http://www.waa.ca/media/news/read,article/508/canada-amp-rsquo-s-new-government-renames-winnipeg-international-airport-in-honour-of-james-armstrong-richardson "Canada's New Government Renames Winnipeg International Airport in Honour of James Armstrong Richardson."] ''Winnipeg Airport Authorities,'' Press release. Retrieved April 1, 2012.</ref>

==Facilities==

===Main Terminal===
Winnipeg's main airport terminal was designed by Pelli Clarke Pelli Architects and Stantec.<ref>{{cite web|url=http://www.dcnonl.com/article/id47493/--canadarsquos-first-leed-certified-airport-terminal-opens-in-winnipeg|title=Daily Commercial News - Canada’s first LEED certified airport terminal opens in Winnipeg|publisher=|accessdate=August 10, 2015}}</ref> The terminal's design was inspired by the City of Winnipeg's distinctive landscape and the province of Manitoba's vast [[prairie]]s and [[sky]].<ref>{{cite web|url=http://www.newswire.ca/en/story/867989/pelli-clarke-pelli-architects-airport-terminal-opens-in-winnipeg|title=Pelli Clarke Pelli Architects' Airport Terminal Opens in Winnipeg|publisher=|accessdate=August 10, 2015}}</ref> It was the first airport terminal in Canada to be [[Leadership in Energy and Environmental Design|LEED]]-certified for its environmentally friendly concept, design, construction and operation.<ref>{{cite web|url=http://www.cbc.ca/news/canada/manitoba/story/2012/04/03/mb-airport-terminal-iconic-winnipeg.html|title=Winnipeg airport terminal listed among world's iconic|date=April 3, 2012|publisher=|accessdate=August 10, 2015}}</ref> The terminal was constructed in two phases, with construction beginning in 2007 and ending on October 30, 2011 when it was officially opened to the public.<ref>[http://www.cbc.ca/news/canada/manitoba/story/2011/10/30/winnipeg-airport-terminal-opening.html "Winnipeg's new airport terminal opens."] ''CBC News,'' October 30, 2011. Retrieved October 31, 2011.</ref><ref>Carl, Julie. [http://www.winnipegfreepress.com/local/airport-sneak-peek-delights-131411843.html "Airport sneak peek delights."] ''Winnipeg Free Press,'' October 9, 2011. Retrieved October 31, 2011.</ref> Prior to the opening of the current main terminal building, a multi-level access road and four-level, 1,559 stall [[Multi-storey car park|parkade]] were both opened in November 2006. All airlines serving Winnipeg International Airport operate at the main terminal building, with the exception of Perimeter Aviation.

[[Air Canada]] operates a [[Maple Leaf Lounge]] located in the domestic/international departures area,<ref>{{cite web|url=http://www.aircanada.com/en/travelinfo/airport/maplelounges/locations.html|title=Lounge Locations - Maple Leaf Lounges - Air Canada|accessdate=May 17, 2016|publisher=Air Canada}}</ref> and a "pay-in" lounge, operated by Plaza Premium Lounge, is also located in the domestic/international departures area.<ref>{{cite web|url=https://www.plaza-network.com/location_detail?city=Winnipeg|title=Discover a Plaza Premium Lounge - Global Airport Service Locations - Plaza Premium Lounge|accessdate=May 17, 2016|publisher=Plaza Premium Lounge Management Ltd}}</ref> Free [[WiFi]] is provided by the Winnipeg Airports Authority throughout the entire main terminal building.<ref>{{cite web|url=http://www.waa.ca/services/other/display,service/56/wi-fi|title=Wi-Fi Services Winnipeg James Armstrong Richardson International Airport|accessdate=May 17, 2016|publisher=Winnipeg Airports Authority}}</ref>

===Perimeter Terminal===
[[Perimeter Aviation]] is a regional airline that operates its own small, exclusive terminal building at Winnipeg International Airport to facilitate its passenger, cargo and charter services. Perimeter Aviation does not use the main terminal building due to its varied operations to small remote communities throughout [[Northern Manitoba]] and [[Northwestern Ontario]] using small propeller aircraft, with which regular airport terminal services ([[jet bridge]], [[airline meal|catering]], etc.) are unnecessary and can actually be a hindrance to day-to-day operations.

The Perimeter Aviation terminal building is located 2.6&nbsp;km south of the main terminal building.

===Other facilities===
A large [[Canada Post]] mail processing facility was opened at the airport site on June 4, 2010.<ref>[http://www.waa.ca/?pid=25&newsid=0096 "Canada Post announces new state-of-the-art plant to be built at the airport."] ''Winnipeg Airports Authority.'' Retrieved: April 1, 2012.</ref> The 23,225 square meter facility is located east of the main terminal building, just north of Wellington Avenue.

Three hotels are located on site, adjacent to the main airport terminal.

Richardson International Airport is included in a new {{convert|20000|acre|km2|adj=on}} [[dry port]] created by provincial legislation – CentrePort Canada Act, C.C.S.M. c. C44 – that will offer investment opportunities for distribution centres, warehousing and manufacturing.<ref>{{cite web|url=http://www.centreportcanada.ca/|title=Centreport Canada, Winnipeg Inland Port, Manitoba, Trade  - Centreport Canada|publisher=|accessdate=August 10, 2015}}</ref> [[CentrePort Canada]] will allow companies to take advantage of the cargo capabilities of Richardson International Airport, as well as serviced land, a mid-continent location and highway and rail transport.

On April 14, 2009, Prime Minister [[Stephen Harper]] with Premier [[Gary Doer]] announced at James Richardson that both the Federal and Provincial governments will contribute $212.5 million towards a divided four lane [[Limited-access road|expressway]] called CentrePort Canada Way. It is now complete, and links Inkster Boulevard to the [[Perimeter Highway (Winnipeg)|Perimeter Highway]] on the north side of the [[Canadian Pacific Railway|CP Rail]] Glenboro subdivision parallel to Saskatchewan Avenue to attract new transportation logistics associated development to the city area west and [[Rural Municipality of Rosser|Rosser Municipality]] northwest of the airport.

==Airlines and destinations==

===Passenger===
[[File:Checkin Winnipeg Airport.jpg|thumb|Airline check-in counters at Winnipeg International Airport]]
[[File:Winnipeg airport domestic terminal.jpg|thumb|Domestic/International departure gate area in the Main Terminal]]
[[File:Winnipeg Airport USA Departures.jpg|thumb|USA departure gates in the Main Terminal]]
[[File:YWG Air Canada E190.JPG|thumb|right|[[Air Canada]] [[Embraer E-Jet family#E-190 and 195|Embraer E190]] at the gate]]
{{Airport-dest-list
| 3rdcoltitle = Terminal
<!-- -->
|[[Air Canada]] | [[Montréal–Pierre Elliott Trudeau International Airport|Montréal–Trudeau]], [[Toronto Pearson International Airport|Toronto–Pearson]], [[Vancouver International Airport|Vancouver]]<br>'''Seasonal:''' [[Cancún International Airport|Cancún]], [[Sangster International Airport|Montego Bay]] | Main
<!-- -->
|{{nowrap|[[Air Canada Express]]}} | [[Calgary International Airport|Calgary]], [[Edmonton International Airport|Edmonton]], [[Montréal–Pierre Elliott Trudeau International Airport|Montréal–Trudeau]], [[Ottawa Macdonald–Cartier International Airport|Ottawa]], [[Regina International Airport|Regina]], [[Saskatoon John G. Diefenbaker International Airport|Saskatoon]], [[Thunder Bay International Airport|Thunder Bay]], [[Vancouver International Airport|Vancouver]] | Main	
<!-- -->
|[[Air Transat]] | '''Seasonal:''' [[Cancún International Airport|Cancún]], [[Lic. Gustavo Díaz Ordaz International Airport|Puerto Vallarta]], [[Punta Cana International Airport|Punta Cana]], [[Abel Santamaría Airport|Santa Clara]], [[Juan Gualberto Gómez Airport|Varadero]] | Main
<!-- -->
|[[Bearskin Airlines]] | [[Red Lake Airport|Red Lake]] | Main
<!-- -->
|[[Calm Air]] | [[Churchill Airport|Churchill]], [[Flin Flon Airport|Flin Flon]], [[Gillam Airport|Gillam]], [[Rankin Inlet Airport|Rankin Inlet]], [[Sanikiluaq Airport|Sanikiluaq]], [[The Pas Airport|The Pas]], [[Thompson Airport|Thompson]] | Main
<!-- -->
|[[Delta Air Lines]] | [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]] | Main
<!-- -->
|[[Delta Connection]] | [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]] | Main
<!-- -->
|[[First Air]] | [[Churchill Airport|Churchill]], [[Rankin Inlet Airport|Rankin Inlet]] | Main
<!--  -->
|[[NewLeaf]]<br>{{nowrap|operated by [[Flair Airlines]]}} | [[Abbotsford International Airport|Abbotsford]], [[Edmonton International Airport|Edmonton]], [[John C. Munro Hamilton International Airport|Hamilton]] | Main
<!--  -->
|[[Perimeter Aviation]] | [[Berens River Airport|Berens River]], [[Cross Lake (Charlie Sinclair Memorial) Airport|Cross Lake]], [[Island Lake Airport|Garden Hill]], [[Gods Lake Narrows Airport|Gods Lake Narrows]], [[Gods River Airport|Gods River]], [[Norway House Airport|Norway House]], [[Oxford House Airport|Oxford House]], [[Pikangikum Airport|Pikangikum]], [[Red Sucker Lake Airport|Red Sucker Lake]], [[St. Theresa Point Airport|St. Theresa Point]], [[Shamattawa Airport|Shamattawa]], [[Sandy Lake Airport|Sandy Lake]]| Perimeter Terminal
<!-- -->
|[[Sunwing Airlines]] | '''Seasonal:''' [[Cancún International Airport|Cancún]], [[Jardines del Rey Airport|Cayo Coco/Cayo Guillermo]], [[Frank País Airport|Holguín]], [[Bahías de Huatulco International Airport|Huatulco]], [[Ixtapa-Zihuatanejo International Airport|Ixtapa–Zihuatanejo]], [[General Rafael Buelna International Airport|Mazatlán]], [[Sangster International Airport|Montego Bay]], [[Lynden Pindling International Airport|Nassau]], [[Orlando International Airport|Orlando]], [[Gregorio Luperón International Airport|Puerto Plata]], [[Lic. Gustavo Díaz Ordaz International Airport|Puerto Vallarta]], [[Punta Cana International Airport|Punta Cana]], [[Los Cabos International Airport|San Jose del Cabo]], [[Abel Santamaría Airport|Santa Clara]], [[Juan Gualberto Gómez Airport|Varadero]] | Main
<!-- -->
|[[United Express]] | [[O'Hare International Airport|Chicago–O'Hare]], [[Denver International Airport|Denver]], [[Newark Liberty International Airport| Newark]] (begins May 2, 2017), | Main
<!-- -->
|[[WestJet]]|[[Abbotsford International Airport|Abbotsford]] (begins April 30, 2017),<ref>{{cite web|url=http://www.abbynews.com/news/414267223.html|title=WestJet increasing presence in Abbotsford|publisher=The News|accessdate=February 20, 2017}}</ref> [[Calgary International Airport|Calgary]], [[Edmonton International Airport|Edmonton]], [[John C. Munro Hamilton International Airport|Hamilton (ON)]], [[Kelowna International Airport|Kelowna]], [[McCarran International Airport|Las Vegas]], [[Montréal–Pierre Elliott Trudeau International Airport|Montréal–Trudeau]], [[Ottawa Macdonald–Cartier International Airport|Ottawa]], [[Toronto Pearson International Airport|Toronto–Pearson]], [[Vancouver International Airport|Vancouver]]<br>'''Seasonal:''' [[Cancún International Airport|Cancún]], [[Fort Lauderdale–Hollywood International Airport|Fort Lauderdale]], [[Halifax International Airport|Halifax]], [[Gatwick Airport|London–Gatwick]], [[London International Airport|London (ON)]], [[Sangster International Airport|Montego Bay]], [[Orlando International Airport|Orlando]], [[Palm Springs International Airport|Palm Springs]], [[Phoenix Sky Harbor International Airport|Phoenix–Sky Harbor]], [[Lic. Gustavo Díaz Ordaz International Airport|Puerto Vallarta]] | Main
<!-- -->
|[[WestJet Encore]]|[[Regina International Airport|Regina]], [[Saskatoon International Airport|Saskatoon]], [[Thunder Bay International Airport|Thunder Bay]] | Main
}}

===Cargo===
{{Airport-dest-list
| [[Cargojet Airways]] | [[Calgary International Airport|Calgary]], [[Edmonton International Airport|Edmonton]], [[John C. Munro Hamilton International Airport|Hamilton]], [[Iqaluit Airport|Iqaluit]], [[Montréal–Mirabel International Airport|Montréal–Mirabel]], [[Regina International Airport|Regina]], [[Saskatoon International Airport|Saskatoon]], [[Thunder Bay International Airport|Thunder Bay]], [[Vancouver International Airport|Vancouver]]
| [[FedEx Express]] | [[Rickenbacker International Airport|Columbus–Rickenbacker]], [[Memphis International Airport|Memphis]]
| [[FedEx Express]]<br>{{nowrap|operated by [[Morningstar Air Express]]}} | [[Calgary International Airport|Calgary]], [[Edmonton International Airport|Edmonton]], [[Thunder Bay International Airport|Thunder Bay]], [[Toronto Pearson International Airport|Toronto–Pearson]]
| [[KF Cargo]] | [[Calgary International Airport|Calgary]], [[Vancouver International Airport|Vancouver]]
| [[SkyLink Express]] | [[Regina International Airport|Regina]], [[Saskatoon International Airport|Saskatoon]]
| Suburban Air Freight | [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]]
| [[UPS Airlines]] | [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]], [[Eppley Airfield|Omaha]]
}}

==Ground transportation==

===Car===
Winnipeg International Airport is located at 2000 Wellington Avenue in the City of Winnipeg. Several short and long term [[parkade]]s are located on site, as well as a curb-side valet parking service.

===Bus===
[[Winnipeg Transit]] operates two bus routes that service the airport. The [[Winnipeg Bus Terminal]] is an intercity bus and cargo terminal for [[Greyhound Canada|Greyhound]] and other intercity bus lines. It is located beside the main terminal building. The Brandon Air Shuttle provides shuttle transportation between Winnipeg International Airport and Manitoba's second largest city, [[Brandon, Manitoba|Brandon]].

==Accidents and incidents==
* On July 23, 1983, Air Canada Flight 143 (now known as the [[Gimli Glider]]), a Boeing 767 trying to reach Winnipeg, as an alternate was forced to make an emergency landing in [[Rural Municipality of Gimli|Gimli, Manitoba]] after running out of fuel. No one was injured. This incident was the subject of the book, ''Freefall'', by William Hoffer and the subsequent TV movie, ''[[Falling from the Sky: Flight 174]]'', starring [[William Devane]].
* On March 3, 2007, [[British Airways]] Flight BA289, a [[Boeing 747]] flying from [[London Heathrow Airport]] to [[Phoenix Sky Harbor International Airport]], made an unscheduled landing at Winnipeg James Armstrong Richardson International Airport after a passenger became unruly when he was refused alcohol. The passenger was charged with mischief, causing a disturbance and failing to comply with instructions from the flight crew. The aircraft sat on the tarmac for two hours before resuming its trip to Phoenix.<ref>[http://winnipegsun.com/News/Winnipeg/2007/03/05/3698168-sun.html "Aircraft diverted."] ''Winnipeg Sun,'' March 5, 2007.</ref>
* On June 19, 2007, a [[Northwest Airlines]] Boeing 747 cargo plane en route from [[Wilmington, Ohio]] to [[Anchorage, Alaska]] made an emergency landing at Winnipeg James Armstrong Richardson International Airport after reporting a fire inside the airplane. No one was injured. After cleanup, an unrelated engine problem forced the 747 to remain in Winnipeg, leaving a week later on three engines.<ref>[http://www.winnipegfreepress.com/breakingnews/local/story/3991363p-4606808c.html "Warning signal forces cargo plane to land."] ''Winnipeg Free Press'' Online Edition, June 19, 2007.</ref>
* On October 9, 2009, [[United Airlines]] Flight 6648 from [[Denver]] landed and was taxiing when it skidded off into the grass due to blowing snow at the airport which caused poor visibility. All 35 passengers and crew were safe but the airport had to close one of two runways due to the incident. It took 18 hours to remove the airliner from the snow. In a separate incident the same evening, an Air Canada Jazz aircraft reported hitting a number of birds shortly after takeoff. The crew turned back to Winnipeg and landed safely. An initial investigation found minor damage on the Air Canada aircraft.<ref>[http://www.cbc.ca/canada/manitoba/story/2009/10/10/mb-airplane-runway-delays.html "Airplane runway delays."] ''cbc.ca''. Retrieved November 23, 2009.</ref>
* On October 25, 2010, a United Airlines [[Boeing 777]] made a successful emergency landing in the afternoon after the pilots reported smoke in the cockpit. The flight was from Chicago en route to Shanghai. Everyone on board was fine.<ref>[http://winnipeg.ctv.ca/servlet/an/local/CTVNews/20101025/wpg_plane_101025/20101025/?hub=WinnipegHome "Plane makes emergency landing in Winnipeg after pilot reports smoke in cockpit."] ''CTV.'' Retrieved: April 1, 2012.</ref>
* On May 9, 2013, a Boeing 777-300ER, [[Cathay Pacific]] Flight 806 from [[Hong Kong International Airport|Hong Kong]] to [[O'Hare International Airport|Chicago]], made an emergency landing at Winnipeg due to a possible cargo hold fire. The plane landed safely, and ultimately no fire was found on board.<ref>[http://www.winnipegfreepress.com/local/Diverted-flight-lands-safely-in-Winnipeg-206799601.html "Passengers from diverted flight to leave Winnipeg Thursday night"] ''Winnipeg Free Press.'' Retrieved: May 10, 2013.</ref>
* On June 4, 2016, a Boeing 777-300ER, Air China Flight 818 from [[Washington Dulles International Airport]] to [[Beijing Capital International Airport]]<nowiki>, made an emergency landing in Winnipeg due to problem with the right engine. The plane landed safely with all 291 passengers on board. A replacement aircraft was sent in to Winnipeg to take the stranded passengers to Beijing. {{</nowiki>[http://www.cbc.ca/news/canada/manitoba/air-china-emergency-landing-winnipeg-airport-1.3617092]

==References==

===Citations===
{{reflist|30em}}

===Bibliography===
{{Refbegin}}
* ''Canada's Airports: Reinvention & Success''. [[Ottawa Macdonald-Cartier International Airport|Ottawa-Macdonald-Cartier]]: Insight Media commissioned by the Canadian Airports Council (CAC), 2005.
{{Refend}}

==External links==
{{Commons category inline}}
*[http://www.waa.ca Winnipeg International Airport]
{{Can-arpt-wx|CYWG|Winnipeg James Armstrong Richardson International Airport}}

{{List of airports in Canada}}

[[Category:Airports established in 1928]]
[[Category:Buildings and structures in Winnipeg]]
[[Category:Canadian airports with United States border preclearance]]
[[Category:Certified airports in Manitoba]]
[[Category:Transport in Winnipeg]]
[[Category:WAAS reference stations]]
[[Category:1928 establishments in Manitoba]]
[[Category:National Airports System]]