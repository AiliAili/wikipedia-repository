{{Infobox television episode
 |title = The Wars to Come
 |image = Game-of-Thrones-S05-E01 Mance Rayder at the stake.jpg
 |image_size = 280
 |caption = [[List of A Song of Ice and Fire characters#Mance Rayder|Mance Rayder]] is burned at the stake.
 |series = [[Game of Thrones]]
 |season = 5
 |episode = 1
 |director = [[Michael Slovis]]
 |writer = [[David Benioff]]<br>[[D. B. Weiss]]
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = David Franco
 |editor = Katie Weiland
 |production = 
 |airdate = {{Start date|2015|4|12}}
 |length = 52 minutes
 |guests =
* [[Ciarán Hinds]] as Mance Rayder
* [[Owen Teale]] as Alliser Thorne
* [[Ian McElhinney]] as Barristan Selmy
* [[Julian Glover]] as Grand Maester Pycelle
* [[Tara Fitzgerald]] as Selyse Baratheon
* [[Roger Ashton-Griffiths]] as Mace Tyrell
* [[Jacob Anderson]] as Grey Worm
* [[Finn Jones]] as Loras Tyrell
* [[Daniel Portman]] as Podrick Payne
* [[Jodhi May]] as Maggy
* [[Dominic Carter (actor)|Dominic Carter]] as Janos Slynt
* [[Joel Fry (actor)|Joel Fry]] as Hizdahr zo Loraq
* [[Ben Crompton]] as Edd Tollett
* [[Eugene Simon]] as Lancel Lannister
* [[Will Tudor]] as Olyvar
* [[Rupert Vansittart]] as Yohn Royce
* [[Ian Beattie]] as Meryn Trant
* [[Ian Gelder]] as Kevan Lannister
* [[Brenock O'Connor]] as Olly
* [[Kerry Ingram]] as Shireen Baratheon
* Paul Bentley as the High Septon
* [[Reece Noi]] as Mossador
* [[Michael Condron]] as Bowen Marsh
* [[Lino Facioli]] as Robin Arryn
* [[Nell Williams]] as young Cersei Lannister
* Isabella Steinbarth as Melara Hetherspoon
 |season_list =
 |prev = [[The Children (Game of Thrones)|The Children]]
 |next = [[The House of Black and White]]
 |episode_list = [[Game of Thrones (season 5)|''Game of Thrones'' (season 5)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}
"'''The Wars to Come'''" is the first episode of the [[Game of Thrones (season 5)|fifth season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 41st overall. The episode was written by series co-creators [[David Benioff]] and [[D. B. Weiss]], and directed by [[Michael Slovis]], his directorial debut for the series.<ref name="s5 directors">{{cite web |url=http://insidetv.ew.com/2014/07/15/game-of-thrones-season-5-directors/ |title='Game of Thrones' season 5 directors chosen |work=Entertainment Weekly |first=James |last=Hibberd |date=July 15, 2014 |accessdate=July 15, 2014}}</ref> It aired on April 12, 2015. Prior to airing, this episode along with the remaining first four episodes of the season were leaked online.<ref>{{cite web|url=http://money.cnn.com/2015/04/12/technology/game-of-thrones-leaked-episodes/|title='Game of Thrones': First four episodes leaked before premiere|last=Goldman|first=David|publisher=CNN|date=April 12, 2015|accessdate=April 13, 2015}}</ref>

==Plot==

===Prologue, in the Westerlands===
In a flashback, a young Cersei Lannister ([[Nell Williams]]) and her friend (Isabella Steinbarth) meet Maggy the Frog ([[Jodhi May]]), whom they know to be a witch and fortune teller. Cersei demands to know her future, and whether or not she will marry the prince, Rhaegar Targaryen, and have his children. Maggy informs her that she will instead be wed to "the king" (Robert Baratheon) and while she will have three children, the king will have twenty. She also says that all of Cersei's children will wear golden crowns, but also golden shrouds (meaning they will all die), and that another queen, younger and more beautiful than Cersei, will cast her down.

===In King's Landing===
In the present, Cersei ([[Lena Headey]]) arrives at the Sept of Baelor to pay respects to her deceased father Tywin ([[Charles Dance]]). Once inside the Sept, she chastises her brother Jaime ([[Nikolaj Coster-Waldau]]) for freeing their brother Tyrion and thus indirectly causing their father's death. At Tywin's wake, Cersei is approached by her uncle Kevan Lannister ([[Ian Gelder]]) and her cousin Lancel Lannister ([[Eugene Simon]]), who has become a devout member of the Sparrows, a religious group devoted to the Faith of the Seven. Lancel asks Cersei's forgiveness for their adulterous relationship and for his role in giving King Robert Baratheon wine before the hunt in which Robert was mortally wounded. Cersei denies any knowledge of her husband's murder, and leaves.

Margaery Tyrell ([[Natalie Dormer]]) walks in on her brother, Loras ([[Finn Jones]]), and his lover, Olyvar ([[Will Tudor]]), naked in bed. After dismissing Olyvar, she tells Loras that he should be more discreet but he dismisses this idea by saying that they already know about him and that he is unlikely to be married to Cersei as Tywin's death means no one can force her to do anything. He calls this bad news for Margaery as it means that Cersei will remain in King's Landing instead of going to Highgarden with him. Margaery hints that she may have a solution.

===In Pentos===
Tyrion ([[Peter Dinklage]]), weary and drunk, is broken out of the crate in which he was smuggled out of Westeros by Varys ([[Conleth Hill]]). Arriving in Pentos, Varys tells Tyrion that he and Illyrio Mopatis had worked together in secret to install Daenerys Targaryen as the new ruler of Westeros, and convinces Tyrion to travel with him to Meereen to meet her.

===In Meereen===
Several Unsullied remove the large gold harpy from the top of the Great Pyramid in Meereen. Later, one of the Unsullied is murdered in a brothel by a member of the Sons of the Harpy, a resistance group operating in Meereen. Daenerys ([[Emilia Clarke]]) orders Grey Worm ([[Jacob Anderson]]) to find those responsible, and for the murdered soldier to be buried in the Temple of the Graces as a statement of defiance to the Sons.

Daario Naharis ([[Michiel Huisman]]) and Hizdahr zo Loraq ([[Joel Fry (actor)|Joel Fry]]) return to Meereen and Hizdhar declares that the mission to Yunkai has been successful, and that the Wise Masters will turn over power to a council of former slaves and former slave owners. In exchange, the Wise Masters have asked that Daenerys consent to the reopening of the fighting pits, an arena where slaves used to fight to the death. Daenerys denies the request. Later, Daario urges her to reconsider, as his youth spent fighting in the pits gave him the combat skills necessary for him to join the Second Sons, where he met Daenerys.

After a comment made by Daario, Daenerys visits her two captive dragons, Viserion and Rhaegal, which she locked underground to prevent them from killing people. When she approaches them, they show aggression toward her, forcing her to flee the room.

===In the Vale===
Lord Petyr Baelish ([[Aidan Gillen]]) and Sansa Stark ([[Sophie Turner (actress)|Sophie Turner]]) watch Lord Robin Arryn ([[Lino Facioli]]) struggle while sparring with a young boy. Sansa witnesses Baelish receiving a message, which he quickly hides. Lord Yohn Royce ([[Rupert Vansittart]]) has agreed to take Robin as his ward and train him to fight, but is not optimistic of his abilities. Though Baelish told Lord Royce that they would be travelling to the Fingers, Baelish and Sansa instead head west, with Baelish explaining that he is taking her to a place where the Lannisters can never find her.

Podrick Payne ([[Daniel Portman]]) attempts to plan the next move for Brienne of Tarth ([[Gwendoline Christie]]) and himself, but Brienne tells him that she doesn't want anyone following her. He reminds her of the oath she swore to Jaime to find the Stark girls, but she states that Arya did not want her protection. As the two talk, a caravan passes nearby, carrying, unbeknownst to them, Baelish and Sansa.

===At the Wall===
Jon Snow ([[Kit Harington]]) helps train new recruits to the Night's Watch, while Samwell Tarly ([[John Bradley-West|John Bradley]]) talks to Gilly ([[Hannah Murray]]) about the frontrunners to be the next Lord Commander. Sam fears that Ser Alliser Thorne ([[Owen Teale]]) will be selected, as he hates the wildlings. Jon is summoned by Melisandre ([[Carice van Houten]]) to speak with [[King Stannis Baratheon]] ([[Stephen Dillane]]). Stannis wants Jon to persuade Mance Rayder ([[Ciarán Hinds]]) to bend the knee and have the wildlings fight for Stannis, so that he can take back the North from Roose Bolton. Stannis tells Jon that he has until sundown to convince Mance, or he will have him burnt at the stake. Jon tries to get Mance to agree, but he will not bend the knee as he fears that the wildlings will not follow him if he does. At night, Mance is brought before Stannis and told that if he agrees, he will be allowed to live. Mance declines the offer and is placed on the pyre. Jon, unwilling to watch Mance die in agony, shoots him with an arrow to give him a quick death.

==Production==

===Writing===
[[File:D. B. Weiss and David Benioff.jpg|right|thumb|The episode was written by series co-creators David Benioff and D. B. Weiss.]]
This episode was written by executive producers David Benioff and D. B. Weiss and contains content from three of George Martin's novels, ''[[A Storm of Swords]]'', partial Samwell IV and partial Jon IX, ''[[A Feast for Crows]]'', chapters Cersei II, Cersei III, Jaime I, and Cersei VIII and ''[[A Dance with Dragons]]'', chapters Tyrion I, Daenerys I, and partial Jon III. The episode marked the first usage of flashbacks, a trope previously avoided to prevent the story from becoming more convoluted than it already was. Flashbacks are also avoided because the writers do not wish to take away from the current plot, as it allows viewers to see the reaction of characters in the present, something that would not be as easy with flashbacks.<ref name=Westeros.org>{{cite web|url=http://www.westeros.org/GoT/Episodes/Entry/The_Wars_to_Come/Book_Spoilers/#Book_to_Screen|title=EP501: The Wars to Come|last=Garcia|first=Elio|last2=Antonsson|first2=Linda|work=Westeros.org|date=April 13, 2015|accessdate=April 14, 2015}}</ref>

===Casting===
With this episode, [[Michiel Huisman]] (Daario Naharis), [[Nathalie Emmanuel]] (Missandei) and [[Dean-Charles Chapman]] (Tommen Baratheon) are promoted to series regulars. [[Ian Gelder]] (Kevan Lannister) and [[Eugene Simon]] (Lancel Lannister) make return appearances after an absence of several years (since the second season).

==Reception==

===Ratings===
"The Wars to Come" was watched by 8 million viewers during its initial airing, making it the show's most watched episode to date until it was surpassed by season finale, "[[Mother's Mercy]]." (8.1 million).<ref>{{cite web |url=http://tvbythenumbers.zap2it.com/2015/04/14/sunday-cable-ratings-game-of-thrones-wins-night-silicon-valley-mtv-movie-awards-mad-men-veep-the-royals-more/388550/ |title=Sunday Cable Ratings: 'Game of Thrones' Wins Night, 'Silicon Valley', 'MTV Movie Awards', 'Mad Men', 'Veep', 'The Royals' & More |work=TV by the Numbers |last=Bibel|first=Sara|date=April 14, 2015|accessdate=April 14, 2015}}</ref> 10.1 million viewers watched the episode on DVR or streaming platforms during its first week, bringing net viewership to 18.1 million.<ref>http://www.technewstoday.com/23283-hbo-game-of-thrones-bags-181-million-viewers-for-season-5-premier-episode/</ref> In the UK, on [[Sky Atlantic]], the episode broke the record for the highest viewer rating on the channel, with 2.63 million tuning in, It also received 0.145 million timeshift viewers.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (13-19 April 2015)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=April 7, 2016}}</ref> "The Wars to Come" was the highest-rated episode of ''Game of Thrones'' until the season finale, "[[Mother's Mercy]]." According to HBO, total viewership rose to 18 million over the next three weeks as more people watched the episode through other venues, such as streaming.<ref name=NYTimes>{{cite web|url=https://www.nytimes.com/2015/06/17/business/media/game-of-thrones-finale-sets-a-record.html?_r=0|title=‘Game of Thrones’ Finale Sets a Record|last=Koblin|first=John|work=New York Times|date=June 16, 2015|accessdate=June 16, 2015}}</ref>

===Critical reviews===
Reception to the episode has been very positive. Based on 27 critic reviews, the episode received a 100% score from [[Rotten Tomatoes]], the site's consensus calling it a "solid season premiere" and saying it "ratchets up the anticipation for inevitable bloodshed while deepening focus on characters and locales."<ref>{{cite web |url=http://www.rottentomatoes.com/tv/game-of-thrones/s05/e01/ |title=The Wars to Come - Game of Thrones: Season 5, Episode 1 |publisher=Rotten Tomatoes |accessdate=April 15, 2015}}</ref> [[BBC]]'s Keith Uhlich praised the episode and the episode's "ostensible lead", [[Peter Dinklage]], proclaiming that in the [[Game of Thrones (season 1)|first season]] the character "provided comic relief" but has since "become more central to the narrative". He concluded stating "it's a show as capable of portraying one character's specific longings as it is widening its gaze to take in a soul-stirring view. And it always leaves the viewer wondering what other lands and adventures might lie over the horizon."<ref>{{cite web |last=Uhlich |first=Keith |url=http://www.bbc.com/culture/story/20150413-game-of-thrones-tv-review |title=Game of Thrones season 5: TV review |publisher=[[BBC]] |date=April 13, 2015 |accessdate=April 13, 2015}}</ref> Matt Fowler from [[IGN]] gave it a mostly positive review stating "[the series] is off to a good start with this solid, though not stunning, premiere". He criticized the episode's character development saying  "it's part of the show's harrowing design to constantly thin the herd [but] we do reach a point where we run low on character to really care about/connect with/invest in." He gave the episode a rating of 8.2 out of 10.<ref>{{cite web|url=http://www.ign.com/articles/2015/04/13/game-of-thrones-the-wars-to-come-review|title=Game of Thrones: "The Wars to Come" review|last=Fowler|first=Matt|publisher=[[IGN]]|date=April 12, 2015|accessdate=April 13, 2015}}</ref>

''[[The A.V. Club]]'' published two reviews, as they have done for each prior episode. Erik Adams wrote the review for people who have not read the novels, and gave the episode a "B" rating.<ref>{{cite web|url=http://www.avclub.com/tvclub/game-thrones-newbies-wars-come-217900|title=Game Of Thrones (newbies): "The Wars To Come"|last=Adams|first=Erik|work=[[The A.V. Club]]|date=April 12, 2015|accessdate=April 13, 2015}}</ref> Myles McNutt, who reviewed the episode for people who have read the novels, positively reviewed the episode although he criticized the episode's overuse of filming locations stating "you wish you could spend more than three scenes in a given location".<ref name=AVClub></ref> However, he expressed his excitement towards the series "offering new perspectives on those events offered by a less rigid approach to characterization, and creating new material out of whole cloth to push the story and its characters forward."<ref name=AVClub>{{cite web|url=http://www.avclub.com/tvclub/game-thrones-experts-wars-come-217875|title=Game of Thrones (experts): "The Wars To Come"|last=McNutt|first=Myles|work=[[The A.V. Club]]|date=April 12, 2015|accessdate=April 13, 2015}}</ref> Charlotte Runcie of ''[[The Daily Telegraph]]'' gave it a four out of five star review and stated "the beginning of season five was a promise of more greatness to come" concluding that "this wasn't the most dramatic episode so far, but the acting and writing were sharper than ever."<ref>{{cite web |last=Runcie |first=Charlotte |url=http://www.telegraph.co.uk/culture/tvandradio/game-of-thrones/11528820/Game-of-Thrones-The-Wars-to-Come-season-5-episode-1-review.html |title=Game of Thrones, season 5, episode 1, The Wars to Come, review: 'sharper than ever |work=[[The Daily Telegraph|The Telegraph]] |date=April 13, 2015 |accessdate=April 13, 2015}}</ref>

==References==
{{reflist|30em}}

==External links==
{{wikiquotepar|Game of Thrones (TV series)#The_Wars_to_Come_.5B5.01.5D|The Wars to Come}}
* {{URL|1=http://www.hbo.com/game-of-thrones/episodes/5/41-the-wars-to-come/index.html|2="The Wars to Come"}} at [[HBO.com]]
* {{IMDb episode|3658012|The Wars to Come}}
* {{tv.com episode|game-of-thrones/the-wars-to-come-3072402/|The Wars to Come}}

{{Game of Thrones Episodes}}

{{DEFAULTSORT:Wars to Come, The}}
[[Category:2015 American television episodes]]
[[Category:Game of Thrones episodes]]