{{italictitle}}
{{Infobox Journal
| title	=	Journal of Computer and System Sciences
| cover	=	[[File:Journal of Computer and System Sciences.gif]]
| discipline	=	[[Computer Science]]
| abbreviation	=	J. Comput. Syst. Sci.
| publisher	=	[[Elsevier]]
| editor = [[Michael Segal]]
| country	=	
| frequency	=	6 issues per year
| history	=	1967 to present
| openaccess	=	no
| website	=	http://www.elsevier.com/locate/jcss
| ISSN		=	0022-0000
}}
The '''''Journal of Computer and System Sciences''''' (JCSS) is a [[peer review|peer-reviewed]] [[scientific journal]] in the field of [[computer science]]. ''JCSS'' is published by [[Elsevier]], and it was started in 1967. Many influential scientific articles have been published in ''JCSS''; these include five papers that have won the [[Gödel Prize]].<ref>[http://sigact.acm.org/prizes/godel/1993.html 1993 Gödel Prize], [http://sigact.acm.org/prizes/godel/2003.html 2003 Gödel Prize], [http://sigact.acm.org/prizes/godel/2005.html 2005 Gödel Prize], [http://sigact.acm.org/prizes/godel/2007.html 2007 Gödel Prize], and [http://www.acm.org/press-room/news-releases/2014/goedel-prize-14/view 2014 Gödel Prize].</ref> Its managing editor is [[Michael Segal]].

==Notes==
{{reflist}}

==References==
* {{cite web
| title=Top journals in computer science
| date=14 May 2009
| work=[[Times Higher Education]]
| accessdate=22 August 2009
| url=http://www.timeshighereducation.co.uk/story.asp?sectioncode=26&storycode=406557
}}
* {{cite web
| title=Journal Rankings
| date=July 2008
| work=CORE: The Computing Research and Education Association of Australasia
| accessdate=28 January 2014
| url=http://core.edu.au/index.php/categories/journals
}}

== External links ==
* {{ISSN|0022-0000}}
* [http://www.elsevier.com/locate/jcss Journal homepage]
* [http://www.sciencedirect.com/science/journal/00220000 ScienceDirect access]
* [http://www.informatik.uni-trier.de/~ley/db/journals/jcss/ DBLP information]

[[Category:Computer science journals]]
[[Category:Elsevier academic journals]]


{{compu-journal-stub}}