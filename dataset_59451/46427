{{Use dmy dates|date=November 2012}}
{{Use South African English|date=November 2012}}
'''Hymie Barsel''' (11 September 1920 – 13 March 1987) was a [[South Africa]]n activist.<ref name="sahistory.org.za">http://www.sahistory.org.za/pages/people/bios/barsel-h.htm</ref>

==Early life==
Hymie Barsel was born on 11 September 1920 in [[Fordsburg]], [[Johannesburg]], [[South Africa]] to Faiga and Moishe Barsel, both of [[Lithuanian Jews|Litvak]] heritage. He was raised in a [[Zionist]] oriented home. He suffered from [[epilepsy]] which was ill understood at that time, eventually receiving treatment from Dr. Max Joffe; also a Zionist. Dr. Joffe taught him that antisemitism could never be destroyed unless all racial prejudice was similarly destroyed, this concept of equality of all humanity was at that time the basis of the understanding of the term Communist – a philosophy of human upliftment followed by many in the liberation movement. This was very different from the version of Communism that would later follow in the Soviet Union.

==Political involvement==
Hymie became progressively more involved in the Youth Liberation Movement and began working as an organizer and then Secretary of the [[Friends of the Soviet Union]] (FSU). He was sent to Durban where he worked with the [[African National Congress]] (ANC) and the [[Natal Indian Congress]] (NIC), this is where he was confronted by the violence offered by the Grey Shirts (a Fascist organization).

Hymie organized FSU branches throughout South Africa, and organized medical assistance for the Soviet Union during the war. He was appointed Secretary of the Johannesburg Medical Aid for Russia. During the war, South Africa and the [[Soviet Union]] were allies, and Hymie sought diplomatic ties between South Africa and [[Russia]]. Russia opened a [[diplomatic mission]] in South Africa, but this was not reciprocated by South Africa in Russia.

==Family and politics==
After the war, Hymie married [[Esther Barsel|Esther Levin]] on 4 December 1945, another Litvak who had been born in [[Raguva]], [[Lithuania]].<ref>{{cite web|url=http://www.nelsonmandela.org/index.php/news/article/madiba_mourns_the_death_of_esther_barsel/ |title=Nelson Mandela Foundation – News - Madiba mourns the death of Esther Barsel |publisher=Nelsonmandela.org |date=2008-10-08 |accessdate=2010-12-24}}</ref> Together Hymie and Esther worked to organize the Congress of the People (COP) in June 1955. Hymie was famous for selling and distributing COP literature.

Hymie and Esther lent their energies to the organization of the Women’s March in [[Pretoria]] on 9 August 1956 where 20,000 women marched and submitted petitions protesting the "Pass Laws" a fundamental building block of Apartheid. This organization that was partly led and created by the Barsels is now accorded a National Holiday in the new South Africa.

Hymie was charged with treason and arrested on 13 December 1956. His Co-accused included Nelson Mandela and other political luminaries. Esther was left behind to care for the three children – Sonya then 8 years old, Linda then 5, and the baby – Merle, aged 8 months. Eventually the South African Government withdrew charges against Hymie on 20 April 1959 after having subjected him to torture, solitary confinement and other pernicious forms of severe punishments.

Hymie was then subjected to a banning order in March 1964. Both he and his wife Esther were then arrested on 3 July 1964 and charged in the [[Bram Fischer Trial]].<ref>{{cite web|url=http://www.nelsonmandela.org/omalley/index.php/site/q/03lv02167/04lv02264/05lv02335/06lv02357/07lv02380/08lv02386.htm |title=Chapter 6: Regional Profile Transvaal - The O'Malley Archives |publisher=Nelsonmandela.org |date= |accessdate=2010-12-24}}</ref> Esther was sentenced to three years hard labor, with a banning order upon her release from Women’s Prison, while Hymie was acquitted. He was placed under house arrest with his daughters from 1965 to 1968.

On 7 April 1968, Esther was released from hard labor, but subject to a banning order. Both were subjected to ongoing harassment. When their eldest daughter Sonya was married, Esther and Hymie were required by the [[South African Security Police]] to provide the guest list, or not be allowed to attend their daughter’s wedding. They refused to comply, notwithstanding that all their friends were either in prison, banned or in hiding. Only days before the wedding did the police relent and allow them to attend Sonya’s wedding. Esther and Hymie we not allowed to attend religious services or meet with friends. When Hymie’s mother – Feiga died, Esther was not allowed to attend her funeral.

The three daughters have spoken about how difficult it was growing up with parents who were avowed Communists. South Africa was a police state of draconian laws with a free rein given to the Department of Justice, the Police and the [[South African Bureau of State Security]] (B.O.S.S.), whose closest equivalent was the [[KGB]]. The tactics used by BOSS, especially during frequent States of Emergency, were closer to the fascistic brutality reported by survivors of death camps and Gulags than accepted strategies employed by the civilized world.

Almost all of their relatives stayed away from the girls, not because they were cruel or unkind but because the power of the Government was absolute and guilt-by-association was common. The notable exception was one unit of Hymie's family: his mother, Faiga, his sister, Chana [Anne] and her husband, Yudel (Jules) Price. More than once the little girls were dropped off in the middle of the night when it became necessary for Hymie and Esther to go into hiding. Without this family's love, it's hard to imagine what would have happened to Sonya, Linda and Merle. It was in the warm cocoon of the Price household that they learned the [[Yiddishkeit]] that has sustained them in adulthood.

Sonya (Sunny) tells stories of how she was shunned by her friends. Most of their parents were too afraid to let their children near this tainted family and life was lonely for much of her young life. Her boyfriend, Clive Lubner came from a prominent family and he was told he needed to break off contact with Sunny. He defiantly told his beloved father that not only would he not comply, but that he intended to marry her. Which he did and 43 years later they are still married.

Both Hymie and Esther were required to report to the Police weekly. On one such report, Hymie was made to sign a "Parole Book" instead of the "Banning Order Book", both located in the same place. He was then re-charged by the police for signing the wrong book, an action he was forced to take.

==Death==
Hymie died on 13 March 1987 of heart and kidney failure. His heart had been weakened by the torture he had been subjected to whilst fighting for the equality of his African brethren. He was buried in Johannesburg in a traditional Jewish funeral.

He is featured on a [[postage stamp]] issued jointly [[Liberia]], [[Gambia]] and [[Sierra Leone]] that notes him as one of the "Legendary Heroes of Africa".

==References==
<references/>
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

{{DEFAULTSORT:Barsel, Hymie}}
[[Category:1920 births]]
[[Category:1987 deaths]]
[[Category:South African Jews]]
[[Category:South African activists]]
[[Category:South African people of Lithuanian-Jewish descent]]