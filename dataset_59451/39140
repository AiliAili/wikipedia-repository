{{Infobox person
|name        = Lynton Keith Caldwell
|image       = LKC-Portrait-1-.jpg
|caption     = 
|birth_date  = {{Birth date|1913|11|21}}
|birth_place = [[Montezuma, Iowa]]
|death_date  = {{Death date and age|2006|08|15|1913|11|21}}
|death_place = [[Bloomington, Indiana]]
|other_names = Keith
|known_for   = Invented idea of environmental impact statement in NEPA
|occupation  = Professor
}}

'''Lynton Keith Caldwell''' (November 21, 1913 – August 15, 2006) was an American [[Political Science|political scientist]] and a principal architect of the 1969 [[National Environmental Policy Act]], the first act of its kind in the world. He was educated at the [[University of Chicago]] and spent most of his career at [[Indiana University Bloomington]], where he received tenure in 1956 and retired as Arthur F. Bentley Professor Emeritus of Political Science in 1984. Caldwell was the internationally acclaimed author or coauthor of fifteen books and more than 250 scholarly articles, which may be found in at least 19 different languages.<ref>Lynton K. Caldwell. Environmental Science Faculty, School of Public and Environmental Affairs, Indiana University. [http://www.indiana.edu/~speaweb/faculty/caldwell.php] Accessed 2-16-09.</ref> He served on many boards and advisory committees, as a consultant on environmental policy issues worldwide, and received numerous honors and awards.<ref name="Bloom1">"Lynton Keith Caldwell," ''Bloomington Herald-Times'', August 17, 2006.</ref><ref>Netta, Danielle. ''Who's Who in America 2002'', 56th ed. New Providence, NJ: Who's Who, 2001, p. 750.</ref>

==Early life and education==
Caldwell was born in [[Montezuma, Iowa]] to Lee Lynton and Alberta (Mace) Caldwell, and died in [[Bloomington, Indiana]], at age 92. He earned his undergraduate degree in English at the [[University of Chicago]] in 1934, his Master's degree at [[Harvard University|Harvard]] in History and Government in 1938, and his doctoral degree in political science at the University of Chicago in 1943. He married Helen A. Walcher on December 21, 1940 and they raised two children.<ref name="Bloom1"/>

==Academic career and public service==
From 1944-1947 he was director of research and publications for The [[Council of State Governments]] in Chicago. In 1947 he was appointed professor of political science at the [[Maxwell School]] of Citizenship and Public Affairs at [[Syracuse University]]. In 1952 and 1953 he was part of U.N. sponsored missions in public administration in [[Colombia]], the [[Philippines]], and [[Japan]]. His next one-year U.N. appointment was as co-Director of the Public Administration Institute for Turkey and the Middle East in [[Ankara]], [[Turkey]]. Indiana University then appointed him director of the Institute of Training for Public Service and Coordinator of Indonesian and Thailand Public Administration Programs. By the time of his retirement, further appointments, research and lecture tours and vacations had enabled him to visit nearly one hundred countries around the world as well as every state in the union. In 1956, after a year as visiting professor of government at the [[University of California, Berkeley]], he returned to Bloomington as Professor of Government at Indiana University, where he remained until his retirement.<ref name="Bloom2">"Lynton Caldwell leaves quite a legacy," ''Bloomington Herald-Times'', August 22, 2006.</ref> In the course of his career he secured 21 National Science Foundation grants to support his research.<ref name="Wertz">Wertz, Wendy Read. ''[http://www.nature.org/wherewework/northamerica/states/indiana/misc/art24490.html The Nature Conservancy’s Journey with Nature: Lynton Keith Caldwell, Indiana’s Conservation Giant]''. Accessed 2-10-09.</ref>

During his career, Caldwell served on the faculties of the [[University of Chicago]], [[Northwestern University]], the [[University of Oklahoma]], Syracuse University, and the [[University of California at Berkeley]], and had shorter appointments at some 80 other collegiate institutions both within the U.S. and overseas. At various times, Caldwell served as advisor or consultant to the [[U.S. Senate]], [[UNESCO]], the [[United Nations]], the Departments of Commerce, Energy, Defense, and Interior, and the [[National Institutes of Health]]. Although not a natural scientist, as part of his work towards establishing interdisciplinary study in universities and achieving a greater merging of the two worlds of science and public policy, he became deeply involved in national and international environmental affairs and worked closely with several important scientific bodies serving, among many appointments, on the Sea Grant Program of the [[National Oceanic and Atmospheric Administration]] (NOAA), the first Environmental Advisory Board of the [[United States Army Corps of Engineers]], the Pacific Science Congress, the President’s National Commission on Materials Policy, the Science Advisory Board for the Great Lakes of the [[International Joint Commission]], as chair of the first Commission on Environmental Policy, Law and Administration for the [[International Union for the Conservation of Nature and Natural Resources]] (IUCN), and as advisor to the [[UNESCO]] Man and the Biosphere Program (MAB), and the UNESCO working program for the environmental education and training of engineers.<ref name="Wertz"/>

He also served on the editorial boards of numerous prestigious scientific and professional journals. A lover of nature, bird watching, and botany from an early age, he was a founding member of the South Bend, Indiana chapter of the [[Audubon Society]], and of both the first local chapter of [[The Nature Conservancy]] (TNC) in New York, and the Conservancy's Indiana Chapter. He served on the Board of Governors of TNC from 1959 to 1965.<ref name="Bloom2"/>

==Notable accomplishments==
During the 1960s Caldwell was virtually a lone voice in attempting to establish policies for the environment because such a holistic and interdisciplinary approach to solving environmental problems did not then exist.<ref>Wertz, Wendy Read. 2006. “Lynton Keith Caldwell (1913-2006): His pathbreaking work in environmental policy and continuing impact on environmental professionals.” ''Environmental Practice'' 8 (December): 208-211.</ref> In 1962 his groundbreaking article “Environment: A New Focus for Public Policy?” appeared  in ''Public Administration Review,''<ref>Caldwell, Lynton K. "Environment: A New Focus for Public Policy?" ''Public Administration Review'', 23(September 1963): 1329.</ref> launching development of a new subfield of ''environmental policy studies''.<ref>Bartlett, Robert, and James Gladden. 1995. “Lynton Keith Caldwell and environmental policy: What have we learned?” In ''Environment as a focus for public policy'', edited by Lynton K. Caldwell. College Station, TX: Texas A&M University Press, pp. 3-4.</ref> After 1962, he changed the main focus of his career towards examining policies for protecting the quality of the human environment. In 1972 he was the catalyst for founding the [[School of Public and Environmental Affairs]] (SPEA) at Indiana University, Bloomington. His 1976 article "Novus Ordo Seclorum: The Heritage of American Public Administration" in ''Public Administration Review'' was a defining paper in the modern history of [[public administration]].<ref>Hadley, Donita. "Founding father of ecological policy dies at 92," ''Bloomington Herald-Times'', August 17, 2006.</ref>

Caldwell is perhaps best known as one of the principal architects of the [[National Environmental Policy Act]] (NEPA), the first act of its kind in the world, signed into law on January 1, 1970. In drafting ''A National Policy on the Environment'' in 1968<ref>U.S. Congress, Senate, Committee on Interior and Insular Affairs. ''A National Policy on the Environment''. 90th Cong., 2d sess. 1968 (Committee Print).</ref> as consultant to Senator Henry Jackson, the head of the powerful Senate Interior and Insular Affairs Committee, Caldwell realized that more was needed than a mere a policy statement: an “action-forcing mechanism” would be necessary to secure federal agency compliance with the Act’s requirements. The origin of the requirement for preparation of an [[environmental impact statement]] (EIS) has been attributed to Caldwell,<ref>Anderson, Frederick R. ''NEPA in the Courts: A Legal Analysis of the National Environmental Policy Act''. Baltimore: Johns Hopkins University Press, 1973, p. 6.</ref><ref>Wandesford-Smith, Geoffrey. “Conclusions: Congress and the Environment of the Future.” In Richard A. Cooley and Geoffrey Wandsford-Smith, eds. ''Congress and the Environment''. Seattle: University of Washington Press. ISBN 0-295-95056-0.</ref> whose testimony at the Senate hearing in April 1969 laid the groundwork for inclusion of provisions requiring an evaluation of the effects of all major federal projects significantly affecting the quality of the human environment.<ref>Weiland, Paul, Lynton K. Caldwell, and Rosemary O’Leary. “The Evolution, Operation, and Future of Environmental Policy in the United States. In: ''Environmental Law and Policy in the European Union and the United States'', Randall Baker, ed. Westport, CT: Praeger, 1997, pp. 100-101.</ref><ref>U.S. Congress. Senate. Committee on Interior and Insular Affairs. ''National Environmental Policy''. Hearings on S. 1075, S. 237 and S. 1752, 91st Cong., 1st sess., April 16, 1969, p. 116.</ref> In these “detailed statements,” as they were termed in the Act, all reasonably foreseeable social, economic, and environmental effects of a proposed action and any possible alternatives to it must be identified and assessed before any federal action takes place.<ref>Wertz, Wendy Read. The Nature Conservancy’s Journey with Nature: Lynton Keith Caldwell, Indiana’s Conservation Giant.
[http://www.nature.org/wherewework/northamerica/states/indiana/misc/art24490.html] Accessed 2-10-08.</ref><ref>Caldwell, Lynton K. ''Science and the National Environmental Policy Act: Redirecting policy through procedural reform''. University, AL: University of Alabama Press, 1982.</ref>

NEPA has been emulated, in one form or another, by more than one hundred other countries,<ref>Bronstein, D. A., D. Bear, H. Bryan, J.F. DiMento, and S. Narayan. 2005. “The National Environmental Policy Act at 35.” ''Environmental Practice'' 7 (January): 3-5.</ref> and many states have also established “mini NEPAs.”<ref name="Bloom2"/> When national government agencies first started to prepare EISs, there were no professional associations dedicated to the planning and problem solving that NEPA demanded. Subsequently, Caldwell's efforts in formulating NEPA, and later promoting it, led to formation of the [[National Association of Environmental Professionals]] (NAEP), a national professional association of persons who prepare EISs.<ref>Perkins, John H. and Debora R. Holmes. 2006. “Lynton K. Caldwell’s Legacy is NEPA and More.” ''Environmental Practice''. 8: 205-215.</ref>

==Honors and awards==
The many awards Caldwell received included the William E. Mosher Award (1964) and the Marshall E. Dimock Award of the [[American Society for Public Administration]] (1981); the John M. Gaus Award from the [[American Political Science Association]]; and the National Environmental Quality Award from the Natural Resources Council of America (1997). In 1991, he was named one of the [[United Nations Environmental Program]]'s (UNEP) Global 500 for distinguished environmental services, and in 1997, he was awarded an honorary LLD from [[Western Michigan University]]. In 2001 he was the recipient of Indiana University's Distinguished Service Award. He was a Fellow of the Royal Society of Arts, and an honorary member of the International Association for Impact Assessment.<ref name="Bloom1"/> Annually since 1995 the [[American Political Science Association]] has awarded the Lynton Keith Caldwell Prize for the best book in environmental politics and policy published during the previous three years.<ref>Lynton Keith Caldwell Prize Awarded. [http://mitpress.typepad.com/mitpresslog/2008/07/award-news---am.html] Accessed 2-18-09.</ref> Shortly after his death in 2006, the Caldwell Center for Culture and Ecology was established in Bloomington, IN to provide environmental education for youth and adults.<ref>Caldwell Center for Culture and Ecology. [http://thecaldwellcenter.org/pages/about_the_center.html] Accessed 2-1-09.</ref>

==Scholarly publications==
*''The administrative theories of Hamilton & Jefferson: Their contribution to thought on public administration''. New York: Russell & Russell, 1944.
*''The government and administration of New York''. New York: Crowell, 1954.
*''Environment: A challenge for modern society''. Garden City, NY: Natural History Press, 1970. Published for the American Museum of Natural History.
*''In defense of earth: International protection of the biosphere''. Bloomington: Indiana University Press, 1972.
*''Man and his environment: Policy and administration''. New York: Harper & Row, 1975. ISBN 978-0-06-041147-3
*''Citizens and the environment: Case studies in popular action''. With Lynton R. Hayes and Isabel M. MacWhirter. Bloomington: Indiana University Press, 1976. ISBN 978-0-253-31355-3
*''Science and the National Environmental Policy Act: Redirecting policy through procedural reform''. University, AL: University of Alabama Press, 1982. ISBN 978-0-8173-0112-5
*''US interests and the global environment''. Muscatine, IA: Stanley Foundation, 1985.
*''Biocracy: Public policy and the life sciences''. Boulder: Westview Press, 1987. ISBN 978-0-8133-7363-8
*''Perspectives on ecosystem management for the Great Lakes: A reader''. Albany: State University of New York Press, 1988. ISBN 978-0-88706-765-5
*''Between two worlds:  Science, the environmental movement, and policy choice''. New York : Cambridge University Press, 1990. ISBN 978-0-521-33152-4.
*''International environmental policy: Emergence and dimensions'', 2d ed. Durham: Duke University Press, 1990. ISBN 978-0-8223-1058-7
*"Globalizing environmentalism: Threshold of a new phase in international relations. In: ''American Environmentalism: The U.S. environmental movement, 1970-1990'', Riley E. Dunlap and Angela G. Mertig, eds. New York: Taylor & Francis, 1992. ISBN 0-8448-1730-9
*''Policy for land: Law and ethics''. With Karen S. Shrader-Frechette. Lanham, MD: Rowman & Littlefield Publishers, 1993. ISBN 978-0-8476-7778-8
*''Environment as a focus for public policy''. With Robert V. Bartlett and  James N. Gladden. College Station: Texas A & M University Press, 1995.
*''International environmental policy: From the twentieth to the twenty-first century'', 3d ed. With Paul S. Weiland. Durham: Duke University Press, 1996. ISBN 978-0-8223-1861-3
*''Environmental policy: Transnational issues and national trends''. With Robert V. Bartlett. Westport, CT: Quorum Books, 1997. ISBN 978-1-56720-079-9
*''The National Environmental Policy Act: An agenda for the future''. Bloomington: Indiana University Press, 1998. ISBN 978-0-253-33444-2

==References==
{{reflist|30em}}

==Further reading==
* Wendy Read Wertz.  ''Lynton Keith Caldwell: An Environmental Visionary and the National Environmental Policy Act'' (Indiana University Press, 2014) [https://www.h-net.org/reviews/showrev.php?id=42196 online review]

==External links==
*[http://www.global500.org/ViewLaureate.asp?ID=330 Global 500 Roll of Honour]
*[http://thecaldwellcenter.org/index.html The Caldwell Center for Culture and Ecology]

{{Good article}}

{{Authority control}}

{{DEFAULTSORT:Caldwell, Lynton K.}}
[[Category:2006 deaths]]
[[Category:American political scientists]]
[[Category:American environmentalists]]
[[Category:American conservationists]]
[[Category:Indiana University Bloomington faculty]]
[[Category:1913 births]]
[[Category:University of California, Berkeley faculty]]
[[Category:Syracuse University faculty]]
[[Category:University of Chicago faculty]]
[[Category:Northwestern University faculty]]
[[Category:University of Oklahoma faculty]]
[[Category:University of Chicago alumni]]
[[Category:Harvard University alumni]]
[[Category:Public administration scholars]]
[[Category:Activists from California]]