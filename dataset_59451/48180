{{Orphan|date=April 2014}}

'''The Global Monitoring Plan (GMP)''' under the [[Stockholm Convention on Persistent Organic Pollutants]] is a programme that enables collection of comparable monitoring data from all regions of the world to assess the effectiveness of the Stockholm Convention in minimizing human and environmental exposure to persistent organic pollutants (POPs). To know whether the levels of POPs are increasing or decreasing over time, information on environmental and human exposure levels of these chemicals should enable detection of trends. GMP looks at background levels of POPs at locations not influenced by local sources, such as ‘hot spots’. For human sampling, the focus is on the general population rather than on individuals who may have suffered high exposure to POPs.<ref>{{citation|title=Global Monitoring of POPs (GMP)- January 2007 Second Meeting of the Provisional Ad Hoc Technical Working Group (TWG) on POPs Monitoring|url=http://www.ipen.org/ipenweb/news/nl_old/3_4_nl_1mar.html}}</ref>

== Scope and implementation ==
The programme covers 22 [[Persistent organic pollutant|POPs]] listed under the Stockholm Convention. It provides a harmonized framework for the collection of comparable measurement data in air, human milk and blood and other media. The monitoring plan provides guidance on how information is to be collected, analyzed, statistically treated, and reported.<ref>{{citation|title=Guidance on the Global Monitoring Plan for Persistent Organic Pollutants |date=February 2007. Amended in May 2007|publisher=Secretariat of the Stockholm Convention on Persistent Organic Pollutants|url=http://chm.pops.int/Convention/Media/Publications/tabid/506/language/en-US/Default.aspx}}</ref>

Ambient air, and human milk and blood are used as core media for the sampling and analysis of POPs. Open ocean and coastal waters and large lakes serve as core media for sampling perfluorooctane sulfonic acid (PFOS), its salts and perfluorooctane sulfonyl fluoride (PFOS-F).

Monitoring data under the GMP are derived from existing international and regional programmes and activities, existing national programmes and activities, and national or regional arrangements and through capacity-building activities.  National data are mostly available in developed countries, while capacity building projects and partnerships support data collection in developing countries.

== Air monitoring ==
Air monitoring data are indicators of environmental exposure to POPs. Ambient air is an important matrix because it has a very short response time to changes in atmospheric eissions. It is also an entry point into food chains and a medium of global transport of POPs loadings to the environment.<ref>{{citation|title=UNEP Workshop to Develop a Global POPs Monitoring Programme to Support the Effectiveness Evaluation of the Stockholm Convention|date=24–27 March 2003|url=http://www.chem.unep.ch/gmn/Files/popsmonprg_proc.pdf}}</ref>

== Biomonitoring ==
The objective of human monitoring within the GMP is to identify temporal and  spatial trends in levels of POPs in humans. [[Biomonitoring]] under the GMP uses human milk and maternal blood as core media. Among human monitoring activities under the GMP, the [[United Nations Environment Programme]] (UNEP) / [[World Health Organization]] (WHO) monitor human exposure over time in order to evaluate whether the Stockholm agreement is effective in reducing the release of these chemicals into the environment and ultimately human exposure. The surveys include human breast milk samples from a wide range of countries with large differences in food consumption patterns and environmental levels of POPs.  In addition, maternal blood plasma is regularly monitored with standardized protocols such as the Arctic Monitoring and Assessment Programme (AMAP).<ref>{{citation|title=Global monitoring report under the global monitoring plan for effectiveness evaluation, Conference of the Parties of the Stockholm Convention on Persistent Organic Pollutants Fourth meeting|date=30 January 2009|url=http://chm.pops.int/Portals/0/Repository/COP4/UNEP-POPS-COP.4-33.English.PDF}}</ref>

== Awareness-raising ==
Human biomonitoring under the Global Monitoring Programme is a principal subject of the [[Safe Planet]] Campaign Body Burden Forum.

== See also ==
* [[Biomonitoring]]
* [[Persistent organic pollutant]]
* [[Safe Planet]]
* [[Stockholm Convention on Persistent Organic Pollutants]]
* [[United Nations Environment Programme]]
* [[World Health Organization]]

== References ==
{{reflist}}

== External links ==

=== Official websites ===
* [http://www.amap.no/ Arctic Monitoring and Assessment Programme]
* [http://www.pops.int Stockholm Convention on Persistent Organic Pollutants]
* [http://www.SafePla.net Safe Planet]
* [http://www.facebook.com/safeplanet Safe Planet on Facebook]

[[Category:United Nations Environment Programme]]