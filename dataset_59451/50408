{{italic title}}
{{Infobox journal
| title         = AIDS Research and Human Retroviruses
| cover         =
| editor        = [[Thomas Hope (biologist)|Thomas Hope]]
| discipline    = [[AIDS]], [[HIV]], human [[retrovirus]]
| peer-reviewed =
| language      = [[English language|English]]
| formernames   = AIDS Research
| abbreviation  = AIDS Res. Hum. Retroviruses
| publisher     = [[Mary Ann Liebert]]
| country       = United States
| frequency     =
| history       = 1983&ndash;1986: AIDS Research<br>1987&ndash;present:AIDS Research and Human Retroviruses
| openaccess    =
| license       =
| impact        = 2.705
| impact-year   = 2012
| website       = http://www.liebertpub.com/aid
| link1         = http://online.liebertpub.com/loi/aid
| link1-name    = Online Access
| link2         =
| link2-name    =
| RSS           = http://online.liebertpub.com/action/showFeed?ui=0&mi=3dc7lw&ai=sv&jc=aid&type=etoc&feed=rss
| atom          =
| JSTOR         =
| OCLC          = 13812822
| LCCN          =
| CODEN         =
| ISSN          = 0889-2229
| eISSN         = 1931-8405
| boxwidth      =
}}

'''''AIDS Research and Human Retroviruses'''''  is a [[peer-review]]ed [[scientific journal]] focusing on [[HIV/AIDS research]], as well as on [[human]] [[retrovirus]]es and their related diseases. The journal was founded in 1983 as ''AIDS Research'', and acquired its current name in 1987. It is published by [[Mary Ann Liebert]], and edited by [[Thomas Hope (biologist)|Thomas Hope]].

It is the official journal of the [[International Retrovirology Association]].

==Indexing and abstracting==
''AIDS Research and Human Retroviruses'' is indexed and abstracted in the following databases:
{{columns-list|3|
*[[Biological Abstracts]]
*[[BIOSIS Previews]]
*[[CAB Direct (database)|CAB Abstracts]]
*[[Current Awareness in Biological Sciences]] (CABS)
*[[Current Contents]]/Life Sciences
*[[Derwent Drug File]]
*[[Elsevier BIOBASE|BIOBASE]]
*[[EMBASE|EMBASE/Excerpta Medica]]
*[[ISI Custom Information Services]]
*[[Journal Citation Reports]]/Science Edition
*[[MEDLINE]]
*[[Prous Science Integrity]]
*[[Science Citation Index]]
*[[Scopus]]
*[[SIIC Databases]]
}}

==External links==
*[http://www.liebertpub.com/aid ''AIDS Research and Human Retroviruses'' website]
*[http://www.htlv.net/ International Retrovirology Association website]



[[Category:Publications established in 1983]]
[[Category:Immunology journals]]
[[Category:Mary Ann Liebert academic journals]]
[[Category:English-language journals]]
[[Category:HIV/AIDS journals]]
[[Category:Academic journals associated with learned and professional societies]]