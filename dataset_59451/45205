{{hatnote|For information on Larry Bell, the microbrewer, see [[Bell's Brewery, Inc.]]}}

{{Infobox person
|name          = Lawrence Dale Bell
|image         = Larrybell.gif
|image_size    = 150px
|caption       = Lawrence Dale Bell
|birth_name    = Lawrence Dale Bell
|birth_date    = {{birth date|1894|4|5|mf=y}}
|birth_place   = [[Mentone, Indiana]], [[United States|USA]]
|death_date    = {{death date and age|1956|10|20|1894|4|5|mf=y}}
|death_place   = [[Buffalo, New York]], USA
|death_cause   = Stroke
|resting_place = [[Forest Lawn Cemetery, Buffalo]], NY, USA
|resting_place_coordinates = 
|nationality   = [[United States|American]]
|other_names   = 
|known_for     = [[Bell Aircraft Corporation]]
|education     = 
|employer      = 
|occupation    = [[industrialist]]
|awards        = [[Daniel Guggenheim Medal]] <small>(1944)</small>
|home_town     = [[Mentone, Indiana]]
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = 
|spouse        = 
|partner       = 
|children      = 
|parents       = 
|relatives     = 
|signature     = 
|website       = [http://www.bellaircraftmuseum.org www.bellaircraftmuseum.org]
|footnotes     = 
}}
'''Lawrence Dale "Larry" Bell''' (April 5, 1894 – October 20, 1956) was an [[United States|American]] [[industrialist]] and founder of [[Bell Aircraft Corporation]].

Bell was born in [[Mentone, Indiana]] and lived there until 1907, when his family moved to [[Santa Monica, California]]. He joined his older brother Grover and stunt pilot [[Lincoln Beachey]] as a mechanic in 1912. Grover Bell was killed in a plane crash the following year, and Lawrence vowed to quit aviation for good; however, he went to work for the [[Glenn L. Martin Company]] after friends convinced him to return to the industry. He became Martin's shop foreman at age 20, and later the company's general manager, wanting to become partner.<ref name=nell/>

[[File:Airacobra P39 Assembly LOC 02902u.jpg|thumb|Bell plant assembly line near Niagara Falls, New York]]
He left Martin in 1928 to join [[Consolidated Aircraft]] in [[Buffalo, New York]], eventually becoming vice president and general manager. When Consolidated relocated to [[San Diego]], Bell stayed in Buffalo and founded his own company with 56 employees,<ref name=nell>Pate, J'Nell & L. Granger, Kay. "Arsenal of Defense: Fort Worth's Military Legacy" page 137-138. Texas State Historical Association Press, 2011. ISBN 9780876112588</ref> Bell Aircraft Corporation, on July 10, 1935. On a government-sponsored "spy tour" to Germany with 44 other industrialists in 1938, he saw the [[Focke-Wulf Fw 61]] helicopter and used the layout of a German aircraft factory for his [[Niagara Falls Air Reserve Station#Bell Modification Center|Niagara Falls plant]].<ref name=nell/> Bell Aircraft built the [[P-39]] "Airacobra" and [[P-63]] "Kingcobra" [[fighter aircraft]] during [[World War II]]. Their [[P-59]] "Airacomet" fighter was America's first jet-powered aircraft. Postwar, the company produced the [[Bell X-1]], the first aircraft to break the [[sound barrier]] in level flight. The company began developing [[helicopter]]s in 1941, with the [[Bell 30]] taking its maiden flight in 1943. This early model evolved into the [[Bell 47]], one of the most recognizable aircraft in history.

For his role in the X-1's first supersonic flight, he shared the 1947 [[Collier Trophy]] with pilot [[Chuck Yeager]] and [[John Stack (rower)|John Stack]], a research scientist with the [[National Advisory Committee for Aeronautics]] (now [[National Aeronautics and Space Administration|NASA]]). He was awarded the [[Society of Automotive Engineers]]' Daniel Guggenheim Medal in 1944, and was posthumously inducted into the [[National Aviation Hall of Fame]] (1977), the [[Army Aviation Hall of Fame]] (1986), and the [[International Aerospace Hall of Fame]] (2004).

== Namesakes ==
* The [http://www.bell.lib.in.us/ Bell Memorial Public Library] building in Bell's hometown of Mentone, Indiana, was constructed largely through a $20,000 grant willed to the town; it is so named because Bell requested that the money be used for a memorial for his parents.
* Mentone is also the site of the [http://www.bellaircraftmuseum.org Lawrence D. Bell Aircraft Museum], which showcases personal and historical items related to his life and the history of aviation.
* [http://www.buffalo.edu/buildings/building?id=BELL Lawrence D. Bell Hall] is a major engineering hub at the [[University at Buffalo, The State University of New York|University at Buffalo]]. In addition to the building, Mr. Bell is honored through a general-purpose fund in the School of Engineering & Applied Sciences.
* [https://www.google.com/maps/place/Lawrence+Bell+Dr,+Buffalo,+NY+14221/@42.9526658,-78.7177367,17z Lawrence Bell Drive] in [[Amherst, New York]].
* In [[Hurst, Texas]], [[L. D. Bell High School (Hurst, Texas)|L.D. Bell High School]] sits on land Bell donated to the [[Hurst-Euless-Bedford Independent School District]].
* Since 1971, the [[Helicopter Association International]] has given a Lawrence D. Bell Memorial Award for excellence in management leadership in the civil helicopter industry.

== See also ==
* [[Bell Helicopter Textron]], the current incarnation of Bell Aircraft Corporation
* [[Ira G. Ross Aerospace Museum]] in [[Buffalo, NY]], housing many examples of early-to-mid-20th century piston, turbo-jet, turbo-shaft, and jet engines, including early Bell helicopters, an example of the World War II Bell [[P-39 Airacobra]], and the [[Bell X-22]] tilt-ducted-fan VSTOL aircraft
* [[Forest Lawn Cemetery, Buffalo]], the memorial and final resting place of Mr. Bell

==References==
{{Reflist}}

== External links ==
* [http://www.bellaircraftmuseum.org Biographical sketch and photo], from the [http://www.bellaircraftmuseum.org Lawrence D. Bell Aircraft Museum]
* [http://nationalaviation.blade6.donet.com/components/content_manager_v02/view_nahf/htdocs/menu_ps.asp?NodeId=-94478373&Group_ID=1134656385&Parent_ID=-1 National Aviation Hall of Fame enshrinee profile]
* [http://www.quad-a.org/Hall_of_Fame/personnel/bell.htm Army Aviation Hall of Fame inductee profile]
* [http://www.earlyaviators.com/ebellgro.htm Grover E. Bell] brother of Lawrence Bell, EarlyAviators.com

{{Authority control}}

{{DEFAULTSORT:Bell, Lawrence Dale}}
[[Category:1894 births]]
[[Category:1956 deaths]]
[[Category:People from Mentone, Indiana]]
[[Category:American aviation businesspeople]]
[[Category:Aviation pioneers]]
[[Category:People from Santa Monica, California]]
[[Category:Burials at Forest Lawn Cemetery, Buffalo]]
[[Category:Indiana in World War II]]
[[Category:National Aviation Hall of Fame inductees]]