'''Cosentini Associates''' provides consulting engineering services for the building industry.

== Company history ==
[[File:WilliamRCosentini.jpg|thumb|Right|240px|William R. Cosentini, founder of Cosentini Associates]]
Cosentini Associates was founded in 1951 by [[William R. Cosentini|William Randolph Cosentini]] as W.R. Cosentini and Associates. William Cosentini was the second born child of Italian immigrant parents Eugenio and Vincenza Cosentini. He earned his MA in mechanical engineering from [[New York University]]. Three years after founding the company, William Cosentini died in 1954 at 41 years of age. 

The company was established to provide consulting services in the mechanical and electrical engineering disciplines. What started out as a six-person firm has grown to employ more than 300 workers. The company is headquartered at Two [[Penn Plaza]] in [[New York City]]. The firm also has offices in other US cities including [[Cambridge, Massachusetts]], [[Chicago]], [[Houston]] and [[Los Angeles]]. Outside of the United States the company has offices in [[Calgary]], [[Paris]], [[Shanghai]], [[Seoul]] and [[Moscow]].

Project types include corporate headquarters, high-rise commercial office buildings, tenant interiors, libraries, academic facilities, museums and performing arts centers, government office buildings, command and control facilities, hotels, residential towers, large-scale mixed- use developments, healthcare and R&D facilities, courthouses, and mission-critical facilities.

In 1999, Cosentini greatly expanded its engineering and design resources by joining Tetra Tech, Inc., a nationwide alliance offering consulting, engineering, and technical services. With nearly 13,000 associates in 330 offices around the world, the company supports commercial and government clients in engineering design, resource management and infrastructure, telecommunications support services, applied science, management consulting, and construction management.

== Notable projects ==

=== 1950s ===

* [[State_University_of_New_York_at_Albany#Uptown_Campus|SUNY Albany's Uptown Campus]], [[Edward Durell Stone]]<ref>{{cite web|title=Campus Heritage Preservation Plan|url=https://oneness.scup.org/asset/55490/SUNY_Albany_Getty_Plan.pdf|publisher=Mesick Cohen Wilson Baker Architects|accessdate=14 December 2012|pages=55|format=PDF|date=April 2009}}</ref> 
* [[Time-Life Building (Chicago)]], [[Wallace Harrison]] of [[Harrison, Abramovitz, and Harris]]<ref>{{cite web|title=Time-Life Building|url=http://www.skyscrapercenter.com/chicago/time-life-building/10311/|work=The Skyscraper Center|publisher=CTBUH|accessdate=14 December 2012}}</ref>

=== 1960s ===

* [[2 Columbus Circle|Huntington Hartford Museum]], [[Edward Durell Stone]]
* [[Ford Foundation Building]], [[Kevin Roche]] and [[John Dinkeloo]] of [[Roche-Dinkeloo|KRJDA]]<ref>{{cite news|title=Deep roots in design. (Profile of the Week).|url=http://www.thefreelibrary.com/Deep+roots+in+design.+%28Profile+of+the+Week%29.-a098167056|accessdate=14 December 2012|newspaper=Real Estate Weekly|date=12 February 2003}}</ref> 
* [[Habitat 67]], [[Moshe Safdie]]
* [[IBM Pavilion]], [[1964-1965 New York World's Fair]], [[Eero Saarinen]]
* [[Israel Museum]], [[Alfred Mansfeld]], [[Armand Bartos]], and [[Frederick Kiesler]]<ref>{{cite web|url=http://www.jewishpost.com/archives/people/eye-on-people-08-04-1.html |title=Eye on People |publisher=JewishPost.com |date= |accessdate=2013-05-08}}</ref>
* [[New England Aquarium]], [[Peter Chermayeff]] of [[Cambridge Seven Associates]]

=== 1970s ===

* [[Field Museum of Natural History]] restoration, [[Harry Weese]]
* [[Grand 1894 Opera House]] renovation, [[Hardy Holzman Pfeiffer]]
* [[IDS Center]], [[Philip Johnson]]<ref>{{cite book|last=Žaknić|first=Ivan|title=100 of the World's Tallest Buildings|year=1998|publisher=Images Publishing|isbn=187549832X|pages=87|url=https://books.google.com/books?id=PZqmsCgJdxgC&pg=PA71&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CC0Q6AEwADgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[John F. Kennedy Presidential Library and Museum]], [[I. M. Pei]]
* [[John Hancock Tower]], [[Henry N. Cobb]] of [[I. M. Pei & Partners]]<ref>{{cite news|title=Deep roots in design. (Profile of the Week).|url=http://www.thefreelibrary.com/Deep+roots+in+design.+%28Profile+of+the+Week%29.-a098167056|accessdate=14 December 2012|newspaper=Real Estate Weekly|date=12 February 2003}}</ref> 
* [[The Solar Project#Solar One|Solar One]], [[Mária Telkes]]
* [[Solow Building]], [[Gordon Bunshaft]] of [[Skidmore, Owings & Merrill]] 
* [[Washington Metro]], [[Harry Weese]]

=== 1980s ===

* [[499 Park Avenue]], [[James Ingo Freed]] of [[I. M. Pei & Partners]]<ref>{{cite web|title=499 Park Avenue / Park Tower|url=http://www.pcf-p.com/a/p/7717/s.html|work=Projects|publisher=Pei Cobb Freed & Partners|accessdate=14 December 2012}}</ref> 
* [[Carnegie Hall Tower]], [[César Pelli]]<ref>{{cite book|last=Žaknić|first=Ivan|title=100 of the World's Tallest Buildings|year=1998|publisher=Images Publishing|isbn=187549832X|pages=71|url=https://books.google.com/books?id=PZqmsCgJdxgC&pg=PA71&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CC0Q6AEwADgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[Crystal Cathedral]], [[Philip Johnson]]<ref>{{cite book|last=Bachman|first=Leonard|title=Integrated Buildings: The Systems Basis of Architecture|year=2004|publisher=John Wiley & Sons|isbn=047146774X|pages=67|url=https://books.google.com/books?id=9lLtElb6hlsC&pg=PA219&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CDkQ6AEwBDgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[Lipstick Building]], [[Philip Johnson]]
* [[Sony Building (New York)|AT&T Building]], [[Philip Johnson]]
* [[United Airlines]] Terminal at [[O'Hare International Airport]], [[Helmut Jahn]] of [[Murphy/Jahn Architects]]<ref>{{cite book|last=Bachman|first=Leonard|title=Integrated Buildings: The Systems Basis of Architecture|year=2004|publisher=John Wiley & Sons|isbn=047146774X|pages=219|url=https://books.google.com/books?id=9lLtElb6hlsC&pg=PA219&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CDkQ6AEwBDgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref>

=== 1990s ===

* [[Condé Nast Building|4 Times Square]], [[Bruce Fowle]] of [[Fox & Fowle]]<ref>{{cite book|last=Wells|first=Matthew|title=Skyscrapers: Structure And Design|year=2005|publisher=Yale University Press|isbn=0300106793|pages=187|url=https://books.google.com/books?id=CUV-MxiIvAUC&pg=PA187&dq=%22cosentini+associates%22&hl=en&sa=X&ei=yBbKUJO3DKf02wWK64D4BA&ved=0CDgQ6AEwAg#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref><ref>{{cite news|last=Holusha|first=John|title=Commercial Property/Office Buildings; New Technology Enhances Marketing and Design|url=https://www.nytimes.com/1998/06/07/realestate/commercial-property-office-buildings-new-technology-enhances-marketing-design.html|accessdate=13 December 2012|newspaper=New York Times|date=June 7, 1998}}</ref><ref>{{cite book|last=Gissen|first=David|title=Big and Green: Toward Sustainable Architecture in the 21st Century|year=2003|publisher=Princeton Architectural Press|isbn=1568983611|pages=23|url=https://books.google.com/books?id=lrnFW_Wv9FsC&pg=PA134&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CD8Q6AEwBjgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref><ref>{{cite book|last=Platt|first=Rutherford|title=The Humane Metropolis: People and Nature in the 21st-Century City|year=2006|publisher=Univ of Massachusetts Press|isbn=1558495541|pages=287, 295|url=https://books.google.com/books?id=-ntl2YemuxwC&pg=PA295&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CEIQ6AEwBzgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[Capella Tower]], [[James Ingo Freed]] of [[Pei Cobb Freed & Partners]]<ref>{{cite book|last=Žaknić|first=Ivan|title=100 of the World's Tallest Buildings|year=1998|publisher=Images Publishing|isbn=187549832X|pages=85|url=https://books.google.com/books?id=PZqmsCgJdxgC&pg=PA71&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CC0Q6AEwADgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[Daniel Patrick Moynihan United States Courthouse]], [[Kohn Pedersen Fox|KPF]]<ref>{{cite web|author=CTBUH |url=http://www.skyscrapercenter.com/new-york-city/foley-square-federal-courthouse/11900/ |title=Foley Square Federal Courthouse Facts &#124; CTBUH Skyscraper Database |location=0.00000 0.00000 |publisher=Skyscrapercenter.com |date= |accessdate=2013-05-08}}</ref>
* [[Walt_Disney_Studios_(Burbank)#The_Roy_E._Disney_Animation_Building|Disney Animation Building]], [[Robert A. M. Stern]]
* [[Guggenheim Museum Bilbao]], [[Frank Gehry]]<ref>{{cite book|last=Buzas|first=Stefan|title=Four Museums: Carlo Scarpa, Museo Canoviano, Possagno Frank O, Gehey, Guggenheim Bilbao Museoa Rafael Moneo, The Audrey Jones Beek Building, MFAH Heinz Tesat, Samml|year=2004|publisher=Edition Axel Menges|isbn=3930698684|pages=110|url=https://books.google.com/books?id=rPcTPMaS11UC&pg=PA110&dq=%22marvin+mass%22&hl=en&sa=X&ei=aRbKUP2uPIGy2QWw84GICQ&ved=0CC0Q6AEwADgK#v=onepage&q=%22marvin%20mass%22&f=false}}</ref><ref>{{cite book|last=Lyall|first=Sutherland|title=Remarkable Structures: Engineering Today's Innovative Buildings|year=2002|publisher=Princeton Architectural Press|isbn=1568983301|pages=218|url=https://books.google.com/books?id=Aljp7X5RqZ8C&pg=PA218&dq=%22cosentini+associates%22&hl=en&sa=X&ei=0SnKUIaPIOTc2AXzlYHACA&ved=0CDMQ6AEwAjgU#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[Rodin Pavilion]], [[Kohn Pedersen Fox|KPF]]<ref>{{cite book|last=Gandelsonas|first=Mario|title=The Rodin Museum, Seoul|year=2001|publisher=Princeton Architectural Press|isbn=1568982356|url=https://books.google.com/books?id=ClyghCLtsaQC&pg=PT10&dq=%22doug+mass%22&hl=en&sa=X&ei=pBbKUKujEuGJ2AWoz4GICg&ved=0CDkQ6AEwBA}}</ref><ref>{{cite book|title=KPF: Selected Works America Erurope Asia|year=2005|publisher=Images Publishing|isbn=1864700505|pages=1971|url=https://books.google.com/books?id=ZPJ1DQzx9gYC&pg=PA1971&dq=%22cosentini+associates%22&hl=en&sa=X&ei=USzKUJyqGMXk2QXthoD4BA&ved=0CEUQ6AEwCDge#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref> 
* [[United States Holocaust Memorial Museum]], [[James Ingo Freed]] of [[Pei Cobb Freed & Partners]], with [[Finegold Alexander + Associates Inc]]<ref>{{cite web|title=United States Holocaust Memorial Museum|url=http://www.pcf-p.com/a/p/8627/s.html|work=Projects|publisher=Pei Cobb Freed & Partners|accessdate=14 December 2012}}</ref><ref>{{cite book|last=Freed|first=James Ingo|title=United States Holocaust Memorial Museum: architectural drawings|year=1996|publisher=GSO Graphics|url=https://books.google.com/books?id=NwxaygAACAAJ}}</ref>

=== 2000s ===

* [[First World Towers]], [[Kohn Pedersen Fox|KPF]]<ref>{{cite web|author=CTBUH |url=http://www.skyscrapercenter.com/incheon/the-first-world-tower-1/1048/ |title=The First World Tower 1 Facts &#124; CTBUH Skyscraper Database |location=37.39202 126.64823 |publisher=Skyscrapercenter.com |date= |accessdate=2013-05-08}}</ref>
* [[IAC Building]], [[Frank Gehry]]<ref>http://www.iachq.com/interactive/_download/_pdf/IAC_Building_Facts.pdf</ref><ref>{{cite web|url=http://www.milrose.com/gallery.asp?gallery=21 |title=Milrose Consultants - Our Clients |publisher=Milrose.com |date=2005-09-01 |accessdate=2013-05-08}}</ref>
* [[Linked Hybrid]], [[Steven Holl]]<ref>{{cite web|author=CTBUH |url=http://www.skyscrapercenter.com/beijing/linked-hybrid-tower-1/9172/ |title=Linked Hybrid Tower 1 Facts &#124; CTBUH Skyscraper Database |location=0.00000 0.00000 |publisher=Skyscrapercenter.com |date= |accessdate=2013-05-08}}</ref>
* [[National Museum of the American Indian]], [[Douglas Cardinal]], [[Johnpaul Jones]], and [[GBQC Architects]]<ref>{{cite web|url=http://www.pai.us/institute.html |title=Pierce Associates, Inc |publisher=Pai.us |date= |accessdate=2013-05-08}}</ref><ref>{{cite web|author=Washington, D.C. |url=http://www.clarkconstruction.com/releases/entry/2275/ |title=Clark Construction Group, LLC – entry |publisher=Clarkconstruction.com |date=2004-10-06 |accessdate=2013-05-08}}</ref>
* [[Baruch_College#Newman_Vertical_Campus|Newman Vertical Campus]] at [[Baruch College]], [[Kohn Pedersen Fox|KPF]]<ref>{{cite book|last=Davis|first=J. William|title=KPF: Selected Works : America, Europe, Asia|year=2005|publisher=Images Publishing|isbn=1864701390|pages=14|url=https://books.google.com/books?id=ZTv1lm3bLfIC&pg=PT15&dq=%22cosentini+associates%22&hl=en&sa=X&ei=USzKUJyqGMXk2QXthoD4BA&ved=0CDMQ6AEwAjge}}</ref> 
* [[Princeton_University_Library#Lewis_Science_Library|Lewis Science Library]] at [[Princeton University]], [[Frank Gehry]]
* [[Richard B. Fisher Center for the Performing Arts]] at [[Bard College]], [[Frank Gehry]]<ref>{{cite web|title=The Richard B. Fisher Center for the Performing Arts at Bard College|url=http://archrecord.construction.com/projects/portfolio/archives/0307bard.asp|work=Projects|publisher=Architectural Record|accessdate=14 December 2012}}</ref> 
* [[Time Warner Center]], [[David Childs]] of [[Skidmore, Owings & Merrill]]<ref>{{cite book|last=Wells|first=Matthew|title=Skyscrapers: Structure And Design|year=2005|publisher=Yale University Press|isbn=0300106793|pages=187|url=https://books.google.com/books?id=CUV-MxiIvAUC&pg=PA187&dq=%22cosentini+associates%22&hl=en&sa=X&ei=yBbKUJO3DKf02wWK64D4BA&ved=0CDgQ6AEwAg#v=onepage&q=%22cosentini%20associates%22&f=false}}</ref><ref>{{cite web|author=CTBUH |url=http://www.skyscrapercenter.com/new-york-city/time-warner-center-south-tower/1125/ |title=Time Warner Center South Tower Facts &#124; CTBUH Skyscraper Database |location=40.76832 -73.98314 |publisher=Skyscrapercenter.com |date= |accessdate=2013-05-08}}</ref>
* [[Walt Disney Concert Hall]], [[Frank Gehry]]

=== 2010s ===

* [[11 Times Square]], [[Bruce Fowle]] of [[FXFOWLE]]<ref>{{cite web|url=http://www.energystar.gov/index.cfm?c=new_bldg_design.project_11_times_square |title=ENERGY STAR Building Design Profile - 11 Times Square : ENERGY STAR |publisher=Energystar.gov |date= |accessdate=2013-05-08}}</ref><ref>{{cite web|author=CTBUH |url=http://skyscrapercenter.com/new-york-city/eleven-times-square/ |title=Eleven Times Square Facts &#124; CTBUH Skyscraper Database |location=40.75677 -73.98963 |publisher=Skyscrapercenter.com |date= |accessdate=2013-05-08}}</ref>
* [[Millennium Place]], [[Handel Architects]]<ref>{{cite book|last=Dixon|first=John|title=Urban Spaces: The Design of Public Places, Issue 3|year=2004|publisher=Visual Reference Publications|isbn=1584710276|pages=319|url=https://books.google.com/books?id=r-N9Q7j2XrYC&dq=%22cosentini+associates%22&source=gbs_navlinks_s}}</ref> 
* [[New World Center]], [[Frank Gehry]]<ref>{{cite web|url=http://www.archdaily.com/107112/new-world-center-frank-gehry/ |title=New World Center / Frank Gehry |publisher=ArchDaily |date= |accessdate=2013-05-08}}</ref>
* [[Shanghai Tower]], [[Gensler]]<ref>{{cite news|last=Jacobson|first=Clare|title=A New Twist on Supertall: An American firm approaches the design of its 121-story, mixed used tower now rising in Shanghai as a vertical collection of neighborhoods.|url=http://archrecord.construction.com/projects/portfolio/2012/05/shanghai-tower.asp|accessdate=13 December 2012|newspaper=Architectural Record|date=May 2012}}</ref><ref>{{cite news|last=Xia|first=Jun|title=Case Study: Shanghai Tower|url=http://www.ctbuh.org/LinkClick.aspx?fileticket=a1ppDY9UeqM%3D&tabid=1090&|accessdate=13 December 2012|newspaper=CTBUH Journal|date=2010 Issue II}}</ref><ref>{{cite web|title=Pudong and Lujiazui: Shanghai Tower|url=http://skyscraper.org/EXHIBITIONS/CHINA_PROPHECY/shanghai_tower.php|work=China Prophesy|publisher=The Skyscraper Museum|accessdate=13 December 2012}}</ref>

=== Un-built ===

* [[Atlanta Symphony Center]], [[Santiago Calatrava]]
* [[Chicago Spire]], [[Santiago Calatrava]]

==External Links==

* http://www.cosentini.com

== References ==

{{Reflist|26em}}

== Further Information ==

[http://www.skyscrapercenter.com/list.php?do=company&company_id=1852&company_name=Cosentini List of projects at The Skyscraper Center], [[Council on Tall Buildings and Urban Habitat]]

[[Category:Engineering companies of the United States]]
[[Category:Engineering consulting firms]]
[[Category:Companies based in Manhattan]]
[[Category:Design companies established in 1951]]
[[Category:1951 establishments in New York]]