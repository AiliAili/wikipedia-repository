{{Use mdy dates|date=February 2012}}
{{Infobox Prime Minister
| name=Zhiuli Shartava<br>ჟიული შარტავა
| image=Jiuli shartava.jpg
| order= Head of the Ministers of [[Autonomous Republic of Abkhazia]]
| term_start=January 11, 1990 
| term_end= September 27, 1993
| president=[[Zviad Gamsakhurdia]]
| predecessor=
| successor=
| order2=Council of Self-Defence of [[Autonomous Republic of Abkhazia]]
| term_start2=1992 
| term_end2=1993
| president2=[[Eduard Shevardnadze]]
| predecessor2=
| successor2=
| birth_date={{birth date|1944|3|7|mf=y}}
| birth_place=[[Sukhumi]], [[Abkhazian ASSR]], [[Georgian SSR]], [[Soviet Union]]
| death_date={{death date and age|1993|09|27|1944|3|7}}
| death_place= [[Sukhumi]], Abkhazia, [[Georgia (country)|Georgia]]
| party=
| religion=
| spouse=
}}

'''Zhiuli Shartava''' ({{Lang-ka|ჟიული შარტავა}}) (March 7, 1944 – September 27, 1993) was a [[Georgia (country)|Georgian]] politician and the Head of the Council of Ministers of the Autonomous Republic of Abkhazia who was killed by [[Abkhaz people|Abkhaz]] militants during the [[ethnic cleansing of Georgians in Abkhazia]] in 1993.

Shartava was born on March 7, 1944 in [[Sukhumi]], [[Abkhaz ASSR]]. An engineer by education, he was elected to the [[Parliament of Georgia]] in 1992. Shartava chaired the legal Council of Ministers and the Council of Self-Defence of [[Autonomous Republic of Abkhazia]] during the [[Georgian-Abkhazian War]] in 1993. When the city of [[Sukhumi]] fell to the [[Russia]]n-supported separatist forces on September 27, 1993, Shartava with other members of the Abkhaz Government ([[Guram Gabiskiria]], [[Raul Eshba]], [[Mamia Alasania]], [[Sumbat Saakian]], [[Misha Kokaia]] and others) refused to flee and were captured by the [[Abkhaz people|Abkhaz]] militants. Initially they were promised safety,<ref>{{cite web|url=http://abkhazeti.ru/pages/1/102.html |title=Zhiuli Shartava memorial page |accessdate=July 2, 2007 |deadurl=unfit |archiveurl=https://web.archive.org/web/20070702074602/http://abkhazeti.ru/pages/1/102.html |archivedate=July 2, 2007 }}</ref> however Shartava and others from the Council of Ministers were killed by the militants and according to UN report Shartava was excessively tortured.<ref>Report of the UN Secretary General on the situation in Abkhazia, Georgia, October 12, 1993</ref>
In 2005, American journalist Malcolm Linton displayed his photo materials taken during the war in Abkhazia at the art gallery in Tbilisi, where Shartavas body was identified among the pile of corpses, clearly visible on one of the photographs. On video materials taken during the capture of Sukhumi by the militants, Shartava is carried out from the Government building and physically assaulted, after which he was forced into the van and taken to the outskirts of Sukhumi where he was killed with other Georgian and Abkhaz members of the government and their staff. Shartava's body was handed over to the Georgian side and was buried in the western Georgian city of [[Senaki]].<ref>Vakhtang Kholbaia, Labyrinth of Abkhazia, 1999</ref><ref>Conflict in the Caucasus: Georgia, Abkhazia, and the Russian Shadow (App Labour History Series; No. 3) by Svetlana Mikhailovna Chervonnaia</ref> In 1994, Shartava was officially honored as the [[Order of the National Hero of Georgia|National Hero of Georgia]] [[Posthumous recognition|posthumously]] in 2004.

[[File:Abkhazia genocide anniversary 2005.jpg|250px|thumb|Malcolm Lintons Photo gallery in Tbilisi where Shartavas body was identified among the piles of corpses on the photograph.]]

== See also ==
*[[Ethnic cleansing of Georgians in Abkhazia]]
*[[Sukhumi Massacre]]

==References==
{{reflist|colwidth=30em}}

==External links==
*[https://web.archive.org/web/20070705173138/http://abkhazeti.ru/video/sukhumi93.avi Government of Abkhazia (-in-exile)]
* (right-click to open file)
{{Prime Ministers of Abkhazia}}

{{DEFAULTSORT:Shartava, Zhiuli}}
[[Category:1944 births]]
[[Category:1993 deaths]]
[[Category:People from Sukhumi]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:Members of the Parliament of Georgia]]
[[Category:Prime Ministers of Abkhazia]]
[[Category:Ethnic cleansing of Georgians in Abkhazia]]
[[Category:Abkhazian murder victims]]
[[Category:National Heroes of Georgia]]