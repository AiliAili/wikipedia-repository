{{Infobox military person
| name          = Jerrold B. Daniels
| image         = <!-- example.jpg -->
| caption       = 
| nickname      = Hog
| birth_date    = {{birth date|1941|06|11|mf=yes}}
| death_date    = {{death date and age|1982|04|29|1941|06|11|mf=yes}}
| birth_place   = [[Palo Alto, California]]
| death_place   = [[Bangkok, Thailand]]
| placeofburial =
| placeofburial_label=Missoula, Montana 
| allegiance    = [[United States]]
| branch        = [[Central Intelligence Agency]] (U.S.)
| serviceyears  = 1960&ndash;1982
| relations     = Bob (father), Louise (mother), Ronald, Jack, and Kent (brothers)
}}

'''Jerrold B. Daniels''' or '''Jerry Daniels''' (June 11, 1941 - April 29, 1982) was a [[CIA]] officer who worked in [[Laos]] and [[Thailand]] from the early 1960s to the early 1980s. He was known by his self-chosen CIA call-sign of "Hog."<ref name=Mule>{{cite book|last=Parker Jr.|first=James E.|title=Codename Mule: Fighting the Secret War in Laos for the CIA|year=1995|publisher=Naval Institute Press}}</ref>  In the early 1960s, he was recruited by the CIA as a liaison officer between Hmong General [[Vang Pao]] and the [[CIA]].<ref name=Ravens>{{cite book|last=Robbins|first=Christopher|title=The Ravens: The Men Who Flew In America's Secret War In Laos|year=1987|publisher=Crown}}</ref><ref name="Back Fire">{{cite book|last=Warner|first=Roger|title=Back Fire: The CIA's Secret War in Laos and Its Link to the War in Vietnam|year=1995|publisher=Simon & Schuster}}</ref>  He worked with the [[Hmong people]] for the CIA's operation in [[Laos]] commonly called the "Secret War" as it was little known at the time.  In 1975, as the communist [[Pathet Lao]] and [[North Vietnamese Army]] advanced on the Hmong base at [[Long Tieng]], Daniels organized the air evacuation of Vang Pao and more than two thousand of his officers, soldiers, and their families to Thailand.<ref name = "morrison1998">{{cite book|last=Morrison|first=Gayle L.|title=Sky Is Falling:  An Oral History of the CIA's Evacuation of the Hmong from Laos|date=1998|publisher=McFarland & Co.|location=Jefferson, NC}}</ref> Immediately after the departure of Daniels and Vang Pao, thousands more Hmong fled across the [[Mekong river]] to [[Thailand]], where they lived in refugee camps.<ref name = "morrison2013">{{cite book|last=Morrison|first=Gayle L.|title=Hog's Exit|date=2013|publisher=Texas Tech University Press| isbn = 9780896727915}}</ref>  From 1975 to 1982 Daniels worked among Hmong refugees in Thailand facilitating the resettlement of more than 50,000 of them in the United States and other countries.<ref>Thompson, Larry Clinton (2010), ''Refugee Workers in the Indochina Exodus'', Jefferson, NC: McFarland Publishing Company, p.244</ref>

==Early life==
Daniels was born on June 11, 1941 in [[Palo Alto]], [[California]]. His parents were Bob and Louise Daniels. He had three brothers: Ronald, Jack, and Kent. The family moved to [[Helmville]], [[Montana]] in 1951, where he graduated from [[Missoula County High School]] in 1959.<ref name=Missoula>{{cite web|last=City of Missoula Government, Montana|title=Jerrold "Hog" Daniels|url=http://mt-missoula.civicplus.com/DocumentCenter/Home/View/751|accessdate=15 June 2012}}</ref>
When he was 17 years old, Daniels became one of the youngest [[smokejumper]]s in [[Missoula]]'s history. He parachuted to fires in Montana, New Mexico, and California.<ref name=Smokejumper>{{cite journal|last=Donner|first=Fred|title=Jerry Daniels (Missoula 58-60) Remembered|journal=Smokejumper Magazing|date=July 2003|url=http://smokejumpers.com/main/smokejumper_magazine_item.php?articles_id=299&magazine_editions_id=19|accessdate=15 June 2012}}</ref>

==CIA and Laotian Civil War==
In 1960, while Daniels was a [[smokejumper]], the CIA recruited him as a loadmaster or "kicker" for air operations based in Thailand. Kickers were often smokejumpers as they had familiarity with parachutes and jumping and surviving in rough terrain. Airplanes were loaded with cargo, flown into areas accessible only by air, and cargo was then "kicked" out the door and dropped or parachuted to locations on the ground.<ref name=Missoula /> The CIA's assistance to the Hmong who lived in the mountains of Laos, was largely delivered by air. The Hmong forces supported the Royal Lao government against the communist Pathet Lao rebels and the [[North Vietnamese Army]] which supplied its troops in [[South Vietnam]] via the [[Ho Chi Minh Trail]].

In 1960, Daniels enrolled as a student at the [[University of Montana]]. He divided time between classes and working as a kicker for CIA affiliates in Laos and other countries until 1965 when he was assigned duties as a CIA Junior Case Officer in Laos among the Hmong. He graduated from college in 1969, and then was promoted to a full Case Officer in Laos.<ref name = "morrison2013" />
[[File:Long Tiengd.jpg|thumb|left|Long Tieng in 1973]]
In 1970, Jerry "Hog" Daniels became a Personal Case Officer for General Vang Pao and worked closely with him on front-line military operations, advising and coordinating U.S. material and financial support for the army, largely made up of Hmong, that Vang Pao commanded. Daniels was based at Vang Pao's headquarters at Long Tieng, located in a secluded mountain valley.  The code name for the CIA in Long Tieng and the compound in which CIA personnel worked was "Sky,' named after Daniels' home state of Montana, "Big Sky Country."  The Long Tieng valley consisted mostly of a {{convert|4400|ft|m}} runway surrounded by a Hmong settlement of several thousand people. At its peak about 1970, 40 to 50 Laotian and U.S. aircraft were stationed at Long Tieng.  Frequent flights from Thailand brought in ammunition and supplies to Vang Pao's 30,000 soldiers.<ref>McBeth, John (March 17, 20130 "The CIA's Secret City", ''The Straits Times'', http://www.nationmultimedia.com/life/The-CIAs-secret-city-30202107.htm</ref> The only access to this area was via this airstrip. The airstrip handled [[C-130 Hercules|C130]], [[Douglas C-47 Skytrain|C47]], and [[C-46 Commando|C46]] cargo planes. This airstrip was a top secret joint operation between Laos and the United States.<ref name=Ravens /><ref name="Back Fire" /><ref name = "morrison1998" />

The [[Paris Peace Accords]] in 1973 ended U.S. direct involvement in the Vietnam War and restrictions on U.S. military aid imposed by the Lao government doomed the Hmong. The American presence in Long Tieng declined.  In April 1975, the United States rapidly began airlifting Americans and Vietnamese employees out of [[South Vietnam]] prior to the fall of [[Saigon]] to Communist forces. In Laos at the same time, communist forces were poised to capture Long Tieng.  Daniels was the only American still working full-time at Long Tieng and he organized the evacuation of Hmong from Laos to Thailand May 12–14, 1975.  Daniels and several American civilian pilots evacuated Vang Pao and 2,500 Hmong leaders and their families from Laos to northeastern Thailand where they were placed in hastily created refugee camps.<ref name = "morrison1998" /><ref>Thompson, pp. 53-58</ref>

==Refugee advocate==
{{further information|Indochina refugee crisis}}
Daniels accompanied Vang Pao to the [[Bitterroot Valley]] near [[Missoula, Montana]] (Daniel's home town) where he was resettled on a ranch purchased for him by the CIA.  Daniels returned to Thailand to assist Hmong refugees crossing the [[Mekong River]] from Laos in large numbers. The CIA also provided funds to create a refugee camp for Hmong at [[Ban Vinai Refugee Camp|Ban Vinai]]. The U.S. government was initially resistant to the resettlement of any Hmong refugees in the United States, although 130,000 Vietnamese had been evacuated from South Vietnam and resettled.  U.S. officials doubted that the Hmomg would be able to adapt to U.S. society and their role in the Vietnam War was little known. To Daniels and a few others, the U.S. government had an obligation to the Hmong, allies of the United States throughout the Vietnam War. Resistance to Hmong resettlement was overcome as a result of advocacy by Daniels, [[Lionel Rosenblatt]], MacAlan Thompson, John Tucker, and [[Pop Buell]].<ref>Thompson, pp. 60-61, 105-106</ref>

Daniels was given the title of Ethnic Affairs Officer to deal with the Hmong and other highland people fleeing Laos.  He was in charge of a large and complex process of screening Hmong refugees to determine their eligibility to be resettled in the United States. Eligibility for resettlement was based on several criteria.  Priority was given to those who had close relatives in the U.S., or had been employees or close associates of the U.S. government and had credible fears of persecution if they returned to Laos. The Hmong did not have a written language so screening was done visually and verbally to determine a person's refugee status and eligibility for resettlement. Daniels' position in the Hmong community gave him great insight and personal acquaintance with many of the Hmong.  Among the cultural and legal problems Daniels faced was the fact that most Hmong couples were not formally married.  A marriage ceremony had to be conducted for them to be eligible to be resettled in the U.S. as a family. Moreover, many Hmong men had more than one wife. To comply with U.S. laws against [[bigamy]], a single wife had to be designated by the husband and the other wives move out of the household and live separately.<ref>Thompson. p. 76, 112-113</ref>

The results of Daniels' work were that 53,700 Hmong and other highland peoples of Laos were resettled in the United States between 1975 and 1982. Several thousand were also settled in other countries.  Also by 1982, another 104,000 Lao refugees, including Hmong, had fled Laos and were living in refugee camps, mostly in [[Ban Vinai Refugee Camp|Ban Vinai]], in Thailand. Many of them would also be resettled over the next 25 years.<ref>Thompson, pp. 234, 244</ref>

==Death and rumors==
On April 29, 1982, age 40, Daniels died mysteriously in [[Bangkok, Thailand]]. The official report stated that he died of asphyxiation from a leaking propane water heater in his apartment. A disfigured body was found in his apartment, said to have been dead for a few days. The body was declared by the U. S. Embassy to be that of Jerry Daniels. The casket was sealed with explicit instructions and security to guarantee it not be opened. The family was told Daniels' body was in the casket but no verifiable proof was submitted. Upon the casket arriving in Missoula, the Hmong were allowed to honor him with a full formal three-day traditional Hmong funeral celebration. Never has any non-Hmong been paid such tribute.<ref name = "morrison2013" /> Hmong around the world claimed to have seen Jerry in Laos, the United States, and Europe after the time of his proclaimed death. Many of the Hmong believed Daniels had been placed into protective custody and continued his work.<ref>http://missoulian.com/news/local/new-book-examines-mysterious-life-death-of-missoula-s-jerry/article_da20dfae-f704-11e2-8794-001a4bcf887a.html, accessed 29 Dec 2015</ref>

==References==
{{Reflist|30em}}

{{refbegin|30em}}

==Bibliography==
* Timothy N. Castle (1999), ''One Da y Too Long: Top Secret Site 85 and the Bombing of North Vietnam'', Columbia University Press. 
* Kenneth Conboy (1995), ''The CIA's Secret War in Laos - Shadow War'', Paladin Press.
* Jane Hamilton-Merritt (1999), ''Tragic Mountains: The Hmong, the Americans, and the Secret Wars for Laos, 1942–1992'', Indiana University Press.
* Gayle L. Morrison (2013), ''Hog's Exit: Jerry Daniels, the Hmong, and the CIA'', Lubbock: Texas Tech University Press.  ISBN 9780896727915
* Gayle L. Morrison (1998), ''Sky Is Falling: An Oral History of the CIA's Evacuation of the Hmong from Laos,'' Jefferson, NC: McFarland & Co.
* James E. Parker, Jr. (1995), ''Codename Mule: Fighting the Secret War in Laos for the CIA'', Naval Institute Press.
* James E. Parker, Jr. (1997), ''Covert Ops: The CIA's Secret War in Laos'', St. Martin's Paperbacks.
* Keith Quincy (2000), ''Harvesting Pa Chay's Wheat: The Hmong and America's Secret War in Laos'', University of Washington Press.
* Christopher Robbins (1987), ''The Ravens: The Men Who Flew In America's Secret War In Laos'', Crown.
* Larry Clinton Thompson (2010), ''Refugee Workers in the Indochina Exodus, 1975–1982'', Jefferson, NC: McFarland Publishing Company.
* Roger Warner (1995), ''Back Fire: The CIA's Secret War in Laos and Its Link to the War in Vietnam'', Simon & Schuster.
* Roger Warner (1998), ''Shooting at the Moon: The Story of America's Clandestine War in Laos'', Steerforth.

{{refend}}

{{DEFAULTSORT:Daniels, Jerry}}
[[Category:1941 births]]
[[Category:1982 deaths]]
[[Category:People from Palo Alto, California]]
[[Category:People of the Central Intelligence Agency]]
[[Category:American humanitarians]]
[[Category:People from Missoula, Montana]]
[[Category:University of Montana alumni]]
[[Category:American people of the Vietnam War]]
[[Category:Activists from California]]