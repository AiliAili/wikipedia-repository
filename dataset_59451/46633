{{Infobox book 
| name          = The Borrowers Avenged
| image         =  File:TheBorrowersAvenged.jpg
| caption       = First UK edition
| author        = [[Mary Norton (author)|Mary Norton]]
| illustrator   = {{plainlist|
* [[Pauline Baynes]] (UK)
* [[Beth and Joe Krush]] (US)
}}
| country       = United Kingdom and US
| language      = English
| series        = [[The Borrowers#Series|The Borrowers]]
| genre         = [[Fantasy]] [[children's novel]]
| publisher     = {{plainlist|
* [[Viking Kestrel]] (UK)<ref name=amazon>[http://www.amazon.co.uk/The-Borrowers-Avenged-Mary-Norton/dp/0722658044/ www.amazon.co.uk] Retrieved 8 Oct 2013</ref>
* [[Harcourt Brace Jovanovich]]
}}
| pub_date      = November 1982<ref>
Silvey, Anita. ''Children's books and their creators''. 1995. p. 492. ISBN 0395653800.</ref>
| pages         = 284pp (UK); 298pp (US)
| isbn          = 0722658044
| isbn_note= <!--reported by LCCus for 1983 Kestrel printing--><br>ISBN 0152105301
| congress      = {{plainlist|
* PZ7.N8248 Bm 1983<ref name=LCCuk>
[http://lccn.loc.gov/85110751 "The Borrowers avenged"] (U.K. edition). Library of Congress Catalog Record. Retrieved 2012-11-28.</ref>
* PZ7.N8248 Bm 1982<ref name=LCCus>
[http://lccn.loc.gov/82047937 "The Borrowers avenged"] (U.S. edition). LCC record. Retrieved 2012-11-28.</ref>
}}
| preceded_by   = [[The Borrowers Aloft]]
| followed_by   = 
}}
'''''The Borrowers Avenged''''' is a children's [[fantasy novel]] by [[Mary Norton (author)|Mary Norton]], published in 1982 by [[Viking Kestrel]] in the UK<ref name=amazon/> and Harcourt in the US. It was the last of five books in a series that is usually called ''The Borrowers'', inaugurated by ''[[The Borrowers]]'' in 1952.<ref name=isfdb/><ref name=bookseller/>

''The Borrowers Avenged'' was written more than 20 years after its predecessor ''The Borrowers Aloft'' (1961).<ref name=isfdb/><ref>
D'Ammassa, Don. ''Encyclopedia of fantasy and horror fiction''. 2006. p. 29. ISBN 0816061920.</ref> 
It is about twice as long as the others at nearly 300 pages;<ref name=isfdb/><ref>
Hettinga, Donald R.; Gary D. Schmidt. ''British children's writers, 1914–1960''. 1996. p. 205.</ref> 
the 1966 British omnibus edition of four novels was only 699 pages.<ref name=LCC4>
[http://lccn.loc.gov/67094657 "The Borrowers omnibus"] (1966). LCC record. Retrieved 2012-11-28.</ref> [[Pauline Baynes]] succeeded Diana L. Stanley as illustrator in the U.K. while [[Beth and Joe Krush]] continued as U.S. illustrators.<ref name=LCCuk/><ref name=LCCus/><ref name=isfdb/><ref name=LCC4/>

The book received a positive reception by critics. ''[[New York (magazine)|New York]]'' magazine called it a "well-drawn portrait&nbsp;...wittily told"<ref>
Maynard, Joyce. [https://books.google.com/books?id=hucCAAAAMBAJ&lpg=PA87&dq=%22the%20borrowers%20avenged%22&pg=PA87#v=onepage&q=%22the%20borrowers%20avenged%22&f=false "Best Books"]. ''New York Magazine''. 4 October 1982. pp. 82–87.</ref> 
while ''[[Country Life (magazine)|Country Life]]'' called it "a modern classic in the making".<ref>
''[[Country Life (magazine)|Country Life]].'' Volume 173 (1983).{{page needed |date=November 2012}}</ref>

==Plot==

''The Borrowers Avenged'' details the events following the escape of the tiny Clock family from the attic of the scheming humans Mr and Mrs Platter and their search for a new home. After returning to the Little Fordham model village, the Clocks set out in Spiller's boat for their new home, the rectory of the local church. They make a night journey down the river, barely missing the Platters who are looking for them. When they arrive at the rectory they discover that their relatives Lupy, Hendreary and Timmus are living in the church next door. Arrietty also meets another borrower named Peagreen Overmantel who shows them a place to live under a window seat.

The Clocks settle in comfortably and Arrietty is allowed to go outside and do all of the borrowing for the two borrower families. She discovers that her human friend Miss Menzies goes to the church to arrange flowers, but she is forbidden to speak to her. The Platters, having severely damaged the model village in their hunt for the borrowers, decide to use one of Homily's old aprons to help the local "finder" Lady Mullings locate the borrowers. Miss Menzies recognizes the apron and becomes suspicious. Meanwhile, the Platters spot Timmus in the church and break in after hours to catch him, but they accidentally ring the church bells and are caught by the humans in suspicious circumstances.

==Characters==
;Borrowers ("little people")
* Arrietty Clock – An independent and spirited fourteen-year-old girl who loves the outdoors and has a penchant for talking to humans. 
* Pod Clock – Her father, a shoemaker and a highly skilled borrower. 
* Homily Clock – His wife, less adventuresome than her husband and daughter.
* Hendreary – Homily's older brother and Arrietty's maternal uncle, who suffers from gout.
* Lupy – Hendreary's wife, who has become religious from living in the church.
* Timmus – An adventuresome little boy who likes to climb around in the church.
* Peagreen Overmantel – A poetic, artistic borrower who is lame since he fell off the [[fireplace mantel]]. Peagreen has lived in the [[rectory]] all of his life.
* Spiller – An outdoors borrower, taciturn and a [[loner]]. He is exceptionally skilled with a [[Short bow|short-bow]] and arrow from a short distance.

;Big People, or human beans
* Mr Platter – A greedy builder and undertaker who is determined to make a fortune exhibiting the Clocks.
* Mrs Platter – His wife, who worries about being caught by the police. 
* Miss Menzies – Arrietty's kind and caring human friend and [[acquaintance]].
* Lady Mullings – The village "finder", who claims that she can find lost people and objects using an unusual [[psionic]] ability known as [[clairvoyance|sixth sense]].

==See also==
{{Portal bar |Children's literature |Fantasy}} <!-- delete the word "bar" if there are enough ordinary See also -->

== References ==
{{reflist |25em |refs=
<ref name=isfdb>
{{isfdb series |6665 |The Borrowers}} ('''ISFDB'''). Retrieved 2012-11-28. Select a title to see its linked publication history and general information. Select a particular edition (title) for more data at that level, such as a front cover image or linked contents.</ref>
<ref name=bookseller><!-- copied from her biography -->
[http://www.booksellerworld.com/mary-norton.htm "Mary Norton Bibliography: A Collectors Reference Guide: UK First Edition Books"]. Bookseller World. Retrieved 2012-07-10.</ref>
}}

==External links==
* [http://www.buriedinprint.com/?p=3536 Mary Norton's Poor Stainless (1966)] at Buried in Print <!-- fan review? that covers this short story in context of the novels --and says that it was eventually published as an appendix to the fourth novel -->

{{Borrowers}}

{{DEFAULTSORT:Borrowers Avenged, The}}
[[Category:The Borrowers]]
[[Category:Children's fantasy novels]]
[[Category:English children's novels]]
[[Category:English fantasy novels]]
[[Category:J. M. Dent books]]
[[Category:Low fantasy novels]]
[[Category:Novels set in England]]
[[Category:1980s fantasy novels]]
[[Category:1982 British novels]]