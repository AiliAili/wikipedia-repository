{{Use British English|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox University Boat Race
| name= 30th Boat Race
| winner = Cambridge
| margin = 3 lengths
| winning_time= 19 minutes 35 seconds
| overall = 14–16
| umpire = [[Joseph William Chitty]]<br>(Oxford)
| date= {{Start date|1873|3|29|df=y}}
| prevseason= [[The Boat Race 1872|1872]]
| nextseason= [[The Boat Race 1874|1874]]
| reserve_winner = 
| women_winner = 
}}

The '''30th Boat Race''' took place on the 29 March 1873. [[The Boat Race]] is an annual [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].   In a race umpired by former Oxford rower [[Joseph William Chitty]], Cambridge won by three lengths in a time of 19 minutes and 35 seconds, the fastest time in the history of the event. It was the first time that rowers raced on sliding seats.

==Background==
[[File:SirJosephWilliamChitty.jpg|right|upright|thumb|[[Joseph William Chitty]] was the umpire for the 1873 Boat Race.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 4 December 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 4 December 2014 | publisher = The Boat Race Company Limited}}</ref>  Cambridge went into the race as reigning champions, having defeated Oxford by two lengths in the [[The Boat Race 1872|previous year's race]], while Oxford led overall with sixteen wins to Cambridge's thirteen.<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher= The Boat Race Company Limited | title = Boat Race – Results| accessdate = 4 December 2014}}</ref>

Although the use of sliding seats had been considered the previous year, the then-Cambridge boat club president [[John Goldie (barrister)|John Goldie]] disallowed the Light Blue boat manufacturer [[Harry Clasper]] from fitting them.<ref name=drink66/>  However, for the 1873 race, both boats were, for the first time, fitted with the innovation.<ref name=burn60>Burnell, p. 60</ref>

Cambridge were coached by John Graham Chambers (who rowed for Cambridge in the [[The Boat Race 1862|1862]] and [[The Boat Race 1863|1863 races]], and was non-rowing boat club president for the [[The Boat Race 1865|1865 race]])<ref>Burnell, p. 104</ref> Oxford's coaches was Robert Lesley, the non-rowing president of [[Oxford University Boat Club]] (who had rowed in the [[The Boat Race 1871|1871]] and 1872 races).<ref>Burnell, pp. 110&ndash;111</ref>

[[Joseph William Chitty]] (who had rowed for Oxford twice in 1849 (in the [[The Boat Race 1849 (March)|March]] and [[The Boat Race 1849 (December)|December races]]) and the [[The Boat Race 1852|1852 race]]) returned as umpire for the race (with [[Robert Lewis-Lloyd]] having officiated the previous year)<ref name=drink66>Drinkwater, p. 66</ref> while the starter was Edward Searle.<ref>Burnell, pp. 49, 97</ref>

==Crews==
The Cambridge crew weighed an average of 11&nbsp;[[Stone (unit)|st]] 11&nbsp;[[Pound (mass)|lb]] (74.7&nbsp;kg), {{convert|5.25|lb|kg|1}} more than their opponents.<ref name=drink67>Drinkwater, p. 67</ref>  Oxford saw four former [[Blue (university sport)|Blues]] return from the 1872 crew, while Cambridge's crew included three rowers who had participated the previous year, in James Brooks Close, Charles Stokes Read and Constantine William Benson.<ref name=burn60/>
{| class=wikitable
|-
! rowspan="2" |Seat
! colspan="3" |Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
! colspan="3" |Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || J. B. Close || [[Trinity College, Cambridge|1st Trinity]] || 11 st 3 lb || [[Clement Courtenay Knollys|C. C. Knollys]] || [[Magdalen College, Oxford|Magdalen]] || 10 st 12.5 lb
|-
| 2 || E. Hoskyns || [[Jesus College, Cambridge|Jesus]] || 11 st 2 lb || J. B. Little || [[Christ Church, Oxford|Christ Church]] || 10 st 13 lb
|-  
| 3 || J. E. Peabody || [[Trinity College, Cambridge|1st Trinity]] || 11 st 7 lb || M. G. Farrer || [[Brasenose College, Oxford|Brasenose]] || 12 st 4 lb
|-
| 4 || W. C. Lecky-Browne || [[Jesus College, Cambridge|Jesus]]|| 12 st 1.5 lb || A. W. Nicholson || [[Magdalen College, Oxford|Magdalen]] || 12 st 9 lb
|-
| 5 || T. S. Turnbull || [[Trinity Hall, Cambridge|Trinity Hall]] || 12 st 12.5 lb || R. S. Mitchison || [[Pembroke College, Oxford|Pembroke]] || 12 st 7.5 lb
|-
| 6 || C. S. Read (P) || [[Trinity College, Cambridge|1st Trinity]] || 12 st 13 lb || W. E. Sherwood || [[Christ Church, Oxford|Christ Church]] || 11 st 1 lb
|-
| 7 || C. W. Benson || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 5 lb || J. A. Ormsby || [[Lincoln College, Oxford|Lincoln]] || 11 st 1.5 lb
|-
| [[Stroke (rowing)|Stroke]] || [[Herbert Rhodes|H. E. Rhodes]] || [[Jesus College, Cambridge|Jesus]]|| 11 st 1.5 lb || F. T. Dowding || [[St John's College, Oxford|St John's]] || 11 st 0 lb
|-
| [[Coxswain (rowing)|Cox]] || C. H. Candy || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 7 st 5 lb || G. E. Frewer || [[St John's College, Oxford|St John's]] || 7 st 6 lb
|-
! colspan="7"|Source:<ref name=burn60/><br>(P) &ndash; boat club president (Robert Lesley was a non-rowing president for Oxford)<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]], along which the race is conducted]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=burn60/> The race commenced at 2.32pm. Oxford's higher [[stroke rate]] enabled them to make "a good race of it", and remained in contention for the first mile-and-a-half to [[Hammersmith Bridge]], but in doing so exhausted themselves.  Cambridge eased off and won the race by three lengths.<ref>Drinkwater, pp. 66&ndash;67</ref>  The winning time was 19 minutes and 35 seconds, the fastest time in the history of The Boat Race and 29 seconds faster than the previous record set in the [[The Boat Race 1869|1869]].<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}
{{good article}}
{{DEFAULTSORT:Boat Race 1873}}
[[Category:1873 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1873 sports events]]