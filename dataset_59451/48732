{{Infobox University
|name = Institut Sup'Biotech de Paris
|image_name = 
|motto = ''The school for experts in biotechnology''
|president = [[Vanessa Proux]]
|type = Private
|established = 2003
|city = [[Villejuif]]
|country = [[France]]
|students = 700
|affiliations=[[IONIS Education Group]],<ref>{{en icon}} [http://www.ionis-group.com/en/#ecoles-supbiotech Sup'Biotech-Innovative education for key industries of the future]</ref> [[Cancer Campus]],<ref>{{fr icon}} [http://www.capgeris.com/formations-365/sup-biotech-pole-cancer-campus-a13983.htm Sup'Biotech : pôle Cancer Campus]</ref> [[Medicen]],<ref>{{fr icon}} [http://www.categorynet.com/communiques-de-presse/sciences/sup7biotech-devient-membre-associe-du-pole-de-competitivite-medicen-paris-region-2008061970285/ Sup'Biotech devient membre associé du pôle de compétitivité Medicen Paris Région]</ref> [[Biocitech]]<ref>{{fr icon}} [http://www.biocitech.com/biocitech-partenaires.htm Partenaires]</ref>
|website = {{URL|www.supbiotech.fr}}
}}
{{Coord|48|47|53|N|2|21|28|E|type:edu_region:FR|display=title}}

The '''Institut Sup'Biotech de Paris''' ('''Sup'Biotech''' or '''ISBP'''; {{lang-en|Paris Higher Biotechnology Institute}}<ref>{{fr icon}} [http://www.ecole-ingenieur.com/ecole/sup-biotech-1344/ Sup’Biotech]</ref>) is a [[France|French]] private engineering school created in 2003<ref>{{fr icon}} [http://www.reussirmavie.net/Philippe-20-ans-a-Sup-Biotech-j-aime-l-innovation-et-le-vivant_a547.html Philippe, 20 ans, à Sup Biotech : ''j'aime l'innovation et le vivant'']</ref> located in [[Villejuif]], near Paris.

Sup'Biotech is specialized in the field of [[biotechnology]].<ref>{{fr icon}} [http://www.buzz4bio.com/communiques-partenaires/5001-supbiotech Sup'Biotech]</ref> The school delivers a 5-year program split in two parts: the first three years correspond to a [[bachelor's degree]] in biotechnology<ref>{{fr icon}} [http://www.dimension-ingenieur.com/sup-biotech-ecole-ingenieur-en-biotechnologies/1/5/0/277/ Sup'Biotech]</ref> and the last two years to [[master's degree]] in biotechnology.<ref name="Legifrance RNCP">{{fr icon}} [http://www.legifrance.gouv.fr/affichTexte.do;jsessionid=?cidTexte=JORFTEXT000025171125&dateTexte=&oldAction=rechJO&categorieLien=id Arrêté du 11 janvier 2012 portant enregistrement au répertoire national des certifications professionnelles]</ref>

== History ==
Sup'Biotech was created in 2003 by [[IONIS Education Group]]. It provides [[middle management]] for [[biotechnology]] industry. Biotechnology industries belong mainly to health, innovation food or environment. On 11 January 2012, the degree delivered by the university was recognized [[National Classification of Levels of Training|level 1]] by [[Commission Nationale de la Certification Professionnelle|CNCP]].<ref name="Legifrance RNCP"/> The same year, on 21 February, the university was labeled by the ''Industries & Agro-Ressources'' (IAR) [[Business cluster|cluster]].<ref>{{fr icon}} [http://www.directetudiant.com/magazine/formation_initiale/926/la-formation-sup-biotech-labellisee-par-le-pole-industries-agro-ressources-iar La formation Sup'Biotech labellisée par le pôle « Industries & Agro-Ressources » (IAR)]</ref> Since January 8, 2015, the university is recognized by the French [[Ministry of National Education (France)|Ministry of National Education]].<ref>{{fr}}arrêté du 15/12/2014 et Bulletin Officiel du 08/01/2015</ref> The 6th of December 2016, Sup'Biotech is recognized by the ''[[Commission des Titres d'Ingénieur]]''.<ref>{{fr}}[http://www.letudiant.fr/educpros/actualite/sup-biotech-ecole-ingenieurs-integre-apb.html Sup'Biotech devient une école d'ingénieurs et intègre APB]</ref>

== Administration ==

=== Governance ===
Sup'Biotech is owned by [[IONIS Education Group]], making [[Marc Sellam]] is the president of the university. He is assisted by a director.<ref>{{fr icon}} [http://www.supbiotech.fr/professeurs-biotechnologie.aspx EQUIPE]</ref>

=== Director ===
Since the creation of the university, the director has been [[Vanessa Proux]], who has a [[doctorate]] in biochemistry from the [[University of Technology of Compiègne]].<ref>{{fr icon}} [http://www.educpros.fr/recherche-de-personnalites/fiche-personnalite/h/6c37794b4e/personalite/-deafe313e6.html Vanessa PROUX]</ref>

{| class="wikitable centre" style="margin: 1em auto 1em auto;"
|+ '''List of Sup'Biotech directors'''
! scope="col" style="width:200px;" | Name
! scope="col" style="width:100px;" | Years
|-----
| [[Vanessa Proux]] 
| Since 2003
|-----
|}

== Teaching and research ==

=== Curriculum ===

====  Bachelor in biotechnology ====
First cycle of three years including a ''[[classe préparatoire aux grandes écoles]]'' of two years and one year of professionalization with a 3-month [[internship]]. During the third year, a trip abroad in a partner university education is compulsory.<ref>{{fr icon}} [http://www.supbiotech.fr/cursus-formation-biologie.aspx Cursus]</ref>

====  Master in biotechnology ====
cond two-year cycle which includes the fourth and fifth year with the choice of a specialization : research / development / production processes or marketing / sales engineer. Students also makes the choice of an option: [[health]] / [[medicine]], [[environmental science|environment]], [[cosmetics]], [[food industry]] or [[bioinformatics]]. During this cycle, two internship (4 and 6 months) are compulsory. Sup'Biotech has signed in January 2011 a partnership with the Cancer [[Institut Gustave Roussy]].<ref>{{fr icon}} [http://www.capcampus.com/sante-461/sup-biotech-signe-un-partenariat-avec-l-institut-de-cancerologie-gustave-roussy-a15704.htm Sup'Biotech signe un partenariat avec l'Institut de cancérologie Gustave Roussy]</ref> The majority<ref>{{fr icon}} [http://www.supbiotech.fr/etudes-international.aspx Accords internationaux]</ref> of the courses in this cycle are given in English.

In the final year, students who wish can prepare in parallel an [[Master of Business Administration|MBA]] at the ''[[Institut supérieur de gestion]]'', a Master at [[IONIS School of Technology and Management|IONIS STM]],<ref name="5ème année">{{fr icon}} [http://www.supbiotech.fr/ingenieur-biologie5.aspx 5ème année]</ref> a Master in [[basic research]] at the [[University of Évry Val d'Essonne]] or at the ''[[École pratique des hautes études]]'',<ref>{{fr icon}} [http://www.studyrama.com/formations/specialites/biologie-chimie-physique/actualite/sup-biotech-devient-partenaire-de-l-ecole-pratique-des-hautes-etudes-ephe.html?id_article=74954 Sup’Biotech devient partenaire de l’Ecole Pratique des Hautes Etudes (EPHE)]</ref> or also a double degree with a partnership university abroad. In addition, the school has a partnership with the ''[[Ecole Centrale Paris]]'' allowing two fifth-year students to follow in parallel a ''[[Mastère Spécialisé]]'' in biomedical engineering data.<ref name="5ème année"/>

=== International partners ===
Sup'Biotech has partnership with universities abroad,<ref>{{fr icon}} [http://www.supbiotech.fr/etudes-international-partenaires.aspx Accords internationaux]</ref> including : [[Tunisia Private University]], [[University of California]] at [[San Diego, California|San Diego]], ''[[Université de Montréal]]'', [[Monterrey Institute of Technology and Higher Education]],<ref name="L'école de biotechnologie à Paris">{{fr icon}} [http://www.supbiotech.fr/etudes-international-partenaires.aspx Partenaires]</ref> [[University of Essex]], [[University of Sussex]] in [[United Kingdom]],<ref name="L'école de biotechnologie à Paris"/> ...

=== Research activities ===
Sup'Biotech has opened a [[Bioinformatics]] laboratory in September 2010, called « Bio Information Research Laboratory »,<ref>{{fr icon}} [http://birl.supbiotech.fr/ BIRL: Bio Information Research Laboratory]</ref> which purpose is the research at the interface between computing and biotechnology, and also a [[Cell (biology)|cellular models]] laboratory.<ref>{{fr icon}} [http://www.supbiotech.fr/laboratoire-celltechs.aspx Laboratoire Celltechs]</ref><ref>{{fr icon}} [http://www.capcampus.com/sante-461/sup-biotech-adhere-au-pole-de-competitivite-industries-et-agro-ressources-a16429.htm Sup'Biotech adhère au pôle de compétitivité « Industries & Agro-Ressources »]</ref>
In May 2012, the university has inaugurated a new bio-production laboratory.<ref>{{fr icon}} [http://blogs.supbiotech.fr/2012/05/un-laboratoire-de-bio-production-pour-lecole.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+supbiotech+%28Sup+Biotech%29 UN LABORATOIRE DE BIO-PRODUCTION POUR L'ÉCOLE]</ref><ref>{{fr icon}} [http://www.ville-villejuif.fr/index.php?tg=oml&file=articles.html&idcat=94 Un nouveau laboratoire de pointe à Sup’Biotech]</ref> It has also a partnership with [[Pasteur Institute]].<ref>{{fr icon}} [http://www.supbiotech.fr/partenariat-institut-pasteur.aspx Ils font avancer la recherche]</ref><ref>{{fr icon}} [http://www.capgeris.com/formations-365/l-institut-pasteur-et-sup-biotech-presentent-le-mini-site-ils-font-avancer-la-recherche-a14584.htm L'Institut Pasteur et Sup'Biotech présentent le mini-site « Ils font avancer la recherche ».]</ref><ref>{{fr icon}} [http://www.capcampus.com/sante-461/l-institut-pasteur-et-sup-biotech-presentent-le-mini-site-ils-font-avancer-la-recherche-a14584.htm L'Institut Pasteur et Sup'Biotech présentent le mini-site « Ils font avancer la recherche »]</ref>

== Notable Alumni ==
* [[Cyprien Verseux]] (2013), crew of the [[HI-SEAS#HI-SEAS IV|Hawaii Space Exploration Analog and Simulation IV]].<ref>{{fr}}[http://www.francetvinfo.fr/sciences/un-francais-va-tester-la-vie-sur-mars-pendant-huit-mois_1059807.html Un Français va vivre comme s'il était sur Mars pendant un an]</ref>

== References ==
{{Reflist|2}}

== External links ==
* {{Official website|www.supbiotech.fr/en/presentation-supbiotech.aspx}} {{en icon}}

{{Villejuif}}
{{IONIS Education Group}}

{{DEFAULTSORT:Institut Sup'Biotech de Paris}}
[[Category:Engineering universities and colleges in France|Biotech de Paris]]
[[Category:Grandes écoles|Biotech Paris]]
[[Category:Schools in Paris|Biotech de Paris]]
[[Category:Universities in Île-de-France|Biotech de Paris]]
[[Category:Educational institutions established in 2003]]