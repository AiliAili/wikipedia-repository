{{good article}}
{{Infobox Weapon
|name= 30.5&nbsp;cm SK L/50
|image=[[File:SMS Kaiser turrets aft2.jpg|300px|alt=Close-up of SMS ''Kaiser''{{'}}s aft turrets]]
|caption=Close-up of {{SMS|Kaiser|1911|6}}'s aft turrets
|origin= German Empire
|type= [[Naval gun]]<br>[[Coastal artillery|Coast-defence gun]]
<!-- Type selection -->
|is_ranged=YES
|is_artillery=YES
<!-- Service history -->
|service=1911–45
|used_by=[[Kaiserliche Marine]]<br>[[Kriegsmarine]]
|wars= [[World War I]]<br>[[World War II]]
<!-- Production history -->
|designer=
|design_date=1908
|manufacturer=[[Krupp]]
|production_date=
|number= 
|variants= 
<!-- General specifications -->
|weight={{convert|51.85|t|ton}}
|length={{convert|15.25|m|ft|sigfig=2}} 
|part_length={{convert|14.481|m|ftin|sigfig=2}}<br>(50 [[Caliber#Caliber as measurement of length|calibers]])
|width=
|height=
|crew=
<!-- Ranged weapon specifications --> 
|cartridge={{convert|250|-|405|kg|lb|abbr=on}}
|caliber={{convert|30.5|cm|in|sigfig=2}}
|action= 
|rate= 2&ndash;3 rounds per minute
|velocity={{convert|855|-|1120|m/s|ft/s|abbr=on}}
|range= {{convert|16200|m|yd|abbr=on}}<br>at 13.5° elevation<br>{{convert|41300|m|yd|abbr=on}}<br>at 49.2° elevation 
|max_range= 
|feed= 
|sights= 
<!-- Artillery specifications -->
|breech=horizontal sliding-block
|recoil=hydro-pneumatic
|carriage=
|elevation=−8° to +13.5° (naval turrets) 
|traverse=depends on the mount
}}

The '''30.5&nbsp;cm SK L/50 gun'''<ref group=A>In Imperial German Navy gun nomenclature, "SK" ({{lang-de|Schnelladekanone}}) denotes that the gun is quick firing, while the L/50 denotes the length of the gun. In this case, the L/50 gun is 50 [[Caliber#Caliber as measurement of length|calibers]], meaning that the gun is 50 times as long as it is in diameter.</ref> was a heavy German gun mounted on 16 of the  26 German [[capital ships]] built shortly before [[World War I]].<ref>Gröner, pp. 23&ndash;30.</ref><ref>Gröner, pp. 54&ndash;9.</ref> Designed in 1908, it fired a shell slightly greater than 12 inches in diameter and entered service in 1911 when the four {{sclass-|Helgoland|battleship|2}}s carrying it were commissioned into the [[High Seas Fleet]]. 

It was also fitted on the four subsequent {{sclass-|Helgoland|battleship|4}}, five {{sclass-|Kaiser|battleship|5}}s, four {{sclass-|König|battleship|2}}s, and three {{sclass-|Derfflinger|battlecruiser|2}}s. The guns were used to great effect at the [[Battle of Jutland]] on 31 May &ndash; 1 June 1916, when the two ''Derfflinger''-class ships, {{SMS|Derfflinger||2}} and {{SMS|Lützow||2}}, used them to destroy the British battlecruisers {{HMS|Queen Mary||2}} and {{HMS|Invincible|1907|2}}.<ref>{{cite book|last=Brown|first=David K.|title=The Grand Fleet: Warship Design and Development 1906-1922|publisher=Naval Institute Press|location=Annapolis, MD|year=1999|page=167|isbn=1-55750-315-X}}</ref> The gun was eventually superseded in German naval use by the much larger and more powerful [[38 cm SK L/45 "Max"|38&nbsp;cm SK L/45]].

Before World War I, 30.5&nbsp;cm SK L/50 guns were emplaced on the islands of [[Helgoland]] and [[Wangerooge]] to defend Germany's [[North Sea]] coast. One battery was emplaced during the war to defend the port of [[Zeebrugge]] in Occupied [[Flanders]]. The guns on Helgoland were destroyed by the victorious Allies at the end of the war, but the battery at Wangerooge survived intact. Three of its guns were transferred to Helgoland after the island was remilitarized in 1935.  During the [[Second World War]], the other three guns were transferred to France and employed in [[Cross-Channel guns in the Second World War|coastal defense positions along the English Channel]].

==Specifications==

===Naval Turrets===
The 30.5 cm SK L/50 guns were mounted in twin [[gun turrets]]. The ''Helgoland''-class ships used six Drh LC/1908 mountings; these turrets had {{convert|100|mm|in|abbr=on}}-thick roofs and {{convert|300|mm|in|abbr=on|1}}-thick sides.<ref>Gröner, p. 24</ref> Later ship classes used improved designs. The  ''Kaiser'' class carried five Drh LC/1909 gun houses, while the subsequent ''König'' class carried five turrets of the Drh LC/1911 type. The primary improvement for the LC/1909 turret was an increase in armor thickness of the roof, from 100&nbsp;mm to 300&nbsp;mm; side armor remained the same.<ref>Gröner, p. 26</ref> The turret roofs on the LC/1911 mounts were reduced to {{convert|110|mm|in|abbr=on}}; again, the sides remained at a thickness of 300&nbsp;mm.<ref>Gröner, p. 27</ref> ''Derfflinger'' and ''Lützow'' used four Drh LC/1912 mountings, while their half [[sister ship|sister]] {{SMS|Hindenburg||2}} carried an improved Drh LC/1913 type. The LC/1912 mounts had 110&nbsp;mm-thick roofs and 270&nbsp;mm-thick sides. The turrets on the newer ''Hindenburg'' had the thickness of their roofs increased to {{convert|150|mm|in|abbr=on}}, though the sides were the same as in the preceding LC/1912 type.<ref>Gröner, p. 56</ref> Weights for the gun houses ranged from 534&ndash;549&nbsp;tons (543&ndash;558&nbsp;[[metric tons]]); the variations depended primarily on the thickness of armor.<ref name="Navweaps">{{cite web| last = DiGiulian| first = Tony| title = Germany 30.5 cm/50 (12") SK L/50 | url = http://www.navweaps.com/Weapons/WNGER_12-50_skc12.htm| publisher = Navweaps.com| date = 28 December 2008| accessdate = 14 August 2009}}</ref>

In the ''König''-class ships, each gun turret had a working chamber beneath it that was connected to a revolving ammunition hoist leading down to the magazine below. The turrets were electrically controlled, though the guns were elevated hydraulically. In an effort to reduce the possibility of a fire, everything in the turret was constructed of steel.<ref>Herwig p. 70</ref> This layout was also used in the preceding battleships.<ref>Gardiner and Gray, pp. 146&ndash;147</ref> On the ''Derfflinger''-class battlecruisers, the two forward and the [[superfire|superfiring]] rear turret used this configuration, although the rearmost gun turret had its magazine and shell room inverted.<ref name="Navweaps"/>

All of the German gun turrets initially allowed for elevation to 13.5&nbsp;degrees, although after the battle of Jutland, they were modified to allow elevation to 16&nbsp;degrees. The centerline turrets on the warships could train 150&nbsp;degrees in either direction, though the wing turrets on the ''Helgoland'' and ''Kaiser'' classes were limited to an arc of 80&nbsp;degrees in either direction.<ref name="Navweaps"/>

===Coast defense mounts===
[[File:305cmBSG.JPG|left|thumb|A gun of Batterie Friedrich August on a BSG mount on Wangerooge Island]]
The island of Helgoland received four twin gun turrets between 1909–12, although their exact type is unknown.<ref>Rolf (2004), p. 186</ref> Battery Kaiser Wilhelm II was built to protect the port of [[Zeebrugge]] in Occupied [[Flanders]] during World War I. It consisted of four guns in concrete barbettes mounted on ''Bettungsschiessgerüst (BSG)'' (firing platforms). These manually powered mounts rotated on a pivot at the front of the mount and the rear was supported by rollers resting on a semi-circular rail. They were equipped with a [[gun shield]] and capable of all-around fire.<ref>{{cite journal|last=Norton|first=Augustus|author2=Armstrong, Donald|year=1919|title=Coast Defenses Constructed by the Germans on the Belgian Coast|journal=Journal of the United States Artillery|publisher=Coast Artillery School Press|location=Ft. Monroe, VA|volume=51|issue=1-2|pages=26, 160–5}}</ref> They were manned by sailors from Naval Artillery Regiment (''Matrosen Artillerie Regiment'') 1.<ref>{{cite book|last=François|first=Guy|title=Eisenbahnartillerie: Histoire de l'artillerie lourd sur voie ferrée allemande des origines à 1945|publisher=Editions Histoire et Fortifications|location=Paris|year=2006|page=8|language=French}}</ref>

By the end of World War I, six guns in BSG mounts equipped Battery Friedrich August on the island of [[Wangerooge]]. Three of these were transferred to Helgoland after 1935 when [[Adolf Hitler|Hitler]] renounced the [[Treaty of Versailles]] which had demilitarized the island. By 1938, they equipped Battery von Schröder and were manned by troops of the Second Naval Artillery Battalion (''II. Marine-Artillerie-Abteilung''), later 122nd Naval Artillery Battalion (''122. Marine-Artillerie-Abteilung'').<ref>Rolf (1998), pp. 38, 300</ref> After the [[Battle of France|French were defeated]] in 1940 all three guns were transferred to Le Trésorerie, near [[Boulogne-sur-Mer]], France where they assumed their former name of Battery Friedrich August.<ref>Rolf (1998), p. 335</ref> These guns were initially in open barbettes with 360° traverse, but these were later rebuilt into concrete [[casemate]]s with overhead cover.<ref>{{cite web|url=http://www.atlanticwall.polimi.it/museum/project/project_rudi1.php|title=The Atlantic Rampart|publisher=The Atlantic Wall Linear Museum|accessdate=2009-08-16}}</ref> that could elevate to 50&nbsp;degrees and train 220&nbsp;degrees in either direction.<ref name="Navweaps"/>

===Ammunition===
[[File:Derfflinger's fore turrets.jpg|thumb|right|upright|''Derfflinger''{{'}}s forward gun turrets]]
These guns fired two types of shells during World War I: [[armor-piercing shot and shell|armor-piercing]] (AP) L/3.1 and [[Explosive material#High explosives|high explosive]] (HE) L/4 types. During World War II, the guns fired a wider variety of shells, including armor-piercing L/3,4 and L/4,9 types, and high explosive L/3.8, L/5, and L/4.8 shells, as well as specially designed coast defense artillery projectiles. The AP and HE rounds weighed between 405 and 415&nbsp;kg (915&nbsp;lb), while the coastal artillery projectiles weighed only 250&nbsp;kg (551&nbsp;lb).<ref name="Navweaps"/>

Shells used during World War I used an RP C/12 main [[propellant]] charge that weighed 91&nbsp;kg (201&nbsp;lb) and a smaller RP C/12 fore charge that weighed 34.5&nbsp;kg (76&nbsp;lb); this gave the guns a [[muzzle velocity]] of  855&nbsp;meters per second (2,805&nbsp;feet per second). Coast defense guns used RP C/32 charges that weighed 85.4&nbsp;kg (188.3&nbsp;lb) for the main charge and 41.6&nbsp;kg (91.7&nbsp;lb) for the fore charge. After 1942, these were increased to 121.5&nbsp;kg (268&nbsp;lb) RP C/38 for AP shells and 143&nbsp;kg (315&nbsp;lb) RP C/38 for HE rounds. These shells were fired with a muzzle velocity of between 850&ndash;855&nbsp;m/s (2,789&ndash;2,805&nbsp;ft/s), but the lightweight coast defense shell had a muzzle velocity of 1,120&nbsp;m/s (3,700&nbsp;ft/s).<ref name="Navweaps"/>

{| class="wikitable" border="1"
|-
! Shell name
! Weight
! Filling Weight
! Muzzle velocity
! Range
|-
! colspan=5| World War I
|-
| ''Armor-piercing shell (Pzgr L/3.1)''
| {{convert|405.5|kg|lb|abbr=on}}
| {{convert|13.6|kg|lb|abbr=on}} (HE)
| {{convert|855|m/s|ft/s||abbr=on}}
| {{convert|20.4|km|yd|abbr=on}} 
|-
| ''high-explosive shell (Sprenggranate L/4)''
| {{convert|405.9|kg|lb|abbr=on}}
| unknown
| {{convert|855|m/s|ft/s|abbr=on}} 
| unknown
|-
! colspan=5| World War II
|-
| ''Armor-piercing shell (Pzgr L/3.4 (mit Haube))''
| {{convert|405|kg|lb|abbr=on}}
| {{convert|11.5|kg|lb|abbr=on}} (HE)
| {{convert|855|m/s|ft/s||abbr=on}}
| {{convert|32|km|yd|abbr=on}} 
|-
| ''Armor-piercing shell with ballistic cap (Pzgr L/4.9 (mit Haube))''
| {{convert|415|kg|lb|abbr=on}}
| unknown
| {{convert|855|m/s|ft/s||abbr=on}}
| {{convert|41.3|km|yd|abbr=on}} 
|-
| ''base-fused high-explosive shell (Sprenggranate L/3.8 m. Bdz.)''
| {{convert|415|kg|lb|abbr=on}}
| unknown
| {{convert|850|m/s|ft/s|abbr=on}} 
| unknown
|-
| ''base-fused HE shell (Sprenggranate L/5 m. Bdz.)''
| {{convert|415|kg|lb|abbr=on}}
| unknown
| {{convert|850|m/s|ft/s||abbr=on}}
| unknown 
|-
| ''nose-fuzed HE (Sprgr L/4.8 m. Kz.)''
| {{convert|405|kg|lb|abbr=on}}
| {{convert|26.5|kg|lb|abbr=on}} (HE)
| {{convert|855|m/s|ft/s|abbr=on}}
| unknown 
|-
| ''base- and nose-fused HE shell with [[Shell (projectile)|ballistic cap]] (Sprgr L/3.6 m. Bdz. u. Kz. (mit Haube))''
| {{convert|250|kg|lb|abbr=on}}
| {{convert|14.5|kg|lb|abbr=on}} (HE)
| {{convert|1120|m/s|ft/s||abbr=on}}
| {{convert|51.4|km|yd|abbr=on}} 
|}

===Performance===
At 13.5&nbsp;degrees of elevation, the 30.5&nbsp;cm gun could hit targets out to 16,200&nbsp;m (17,717&nbsp;yards) with armor-piercing shells. After the turrets were improved to allow elevation to 16&nbsp;degrees, the range correspondingly increased to 20,400&nbsp;m (22,310&nbsp;yd). At a range of 12,800&nbsp;m (14,000&nbsp;yd), the L3 armor-piercing shells fired by the gun were expected to penetrate 254&nbsp;mm (10&nbsp;in) of armor plate. At 15,000&nbsp;m (16,000&nbsp;yd) the effectiveness of the shell decreased; it was able to pierce 229&nbsp;mm (9&nbsp;in)-thick plate. The range of the World War II guns was significantly greater than the guns used in World War I. With the 405&nbsp;kg shell at 45&nbsp;degrees, the guns had a maximum range of 32,000&nbsp;m (35,000&nbsp;yd). With the 415&nbsp;kg shell at 49.2&nbsp;degrees, the range was 41,300&nbsp;m (45,166&nbsp;yd), and with the lighter 250&nbsp;kg round at 49.1&nbsp;degrees, the maximum range was 51,400&nbsp;m (56,200&nbsp;yd).<ref name="Navweaps"/>

==See also==

===Weapons of comparable role, performance and era===
*[[BL 12 inch Mk XI - XII naval gun]] British contemporary guns
*[[Obukhovskii 12"/52 Pattern 1907 gun]] Russian contemporary gun

==Notes==
{{reflist|group=A}}

==Footnotes==
{{reflist|2}}

==References==
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1922|year=1984|location=Annapolis|publisher=Naval Institute Press|isbn=0-87021-907-3}}
* {{cite book|last=Gander|first=Terry|author2=Chamberlain, Peter|title=Weapons of the Third Reich: An Encyclopedic Survey of All Small Arms, Artillery and Special Weapons of the German Land Forces 1939–1945|publisher=Doubleday|location=New York|year=1979|isbn=0-385-15090-3}}
* {{cite book |last=Gröner|first=Erich|title=German Warships: 1815&ndash;1945|year=1990|location=Annapolis|publisher=Naval Institute Press|isbn=0-87021-790-9|oclc=22101769}}
* {{cite book |last=Herwig|first=Holger|title="Luxury" Fleet: The Imperial German Navy 1888–1918 |year=1980|location=[[Amherst, New York]]|publisher=Humanity Books|isbn=978-1-57392-286-9|oclc=}}
* {{cite book|last=Hogg|first=Ian V.|title=German Artillery of World War Two|publisher=Stackpole Books|location=Mechanicsville, PA|year=1997|edition=2nd corrected|isbn=1-85367-480-X}}
* {{cite book| last = Rolf| first = Rudi| title = Der Atlantikwall: Bauten der deutschen Küstenbefestigungen 1940-1945|language=German| publisher = Biblio| year = 1998| location = Osnabrück| isbn = 3-7648-2469-7}}
* {{cite book| last = Rolf| first = Rudi | title = A Dictionary on Modern Fortification: An Illustrated Lexicon on European Fortification in the Period 1800-1945| publisher = PRAK| year = 2004| location = Middleburg, Netherlands| isbn = }}

==External links==
{{Commons category|30.5 cm SK L/50}}

{{DEFAULTSORT:30.5 cm SK L 50 gun}}
[[Category:305 mm artillery]]
[[Category:Naval guns of Germany]]
[[Category:Coastal artillery]]