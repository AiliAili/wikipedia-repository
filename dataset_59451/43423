<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Ponnier L.1
 | image=Ponnier Scout.side.tif
 | caption=
}}{{Infobox Aircraft Type
 | type=Single seat scout
 | national origin=[[France]]
 | manufacturer=[[Avions Ponnier]]
 | designer=Alfred Pagny
 | first flight=c. July 1914
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Ponnier L.1''' was an early [[France|French]] [[biplane]] single seat [[scout (aircraft)|scout]], built just before [[World War I]].  It did not reach production.

==Design==
Just before [[World War I]], most French aircraft designers had concentrated, with success, on fast [[monoplane]]s.  When a [[United Kingdom|British]] [[biplane]], the [[Sopwith Tabloid]], won the [[Schneider Trophy]] in 1913 they were encouraged by the French government to think again about the possibilities of that wing configuration for military or "Cavalrie" types. The Ponnier L.1 of July 1913 was one response, a biplane revision of the Cavalrie version of the [[Ponnier D.III]] monoplane.  The L.1 and D.III shared the same [[fuselage]].<ref name=Opdycke/><ref name=Flight1/>
[[File:Ponnier Scout.front.png|thumb|]]
[[File:Ponnier L.1 French First World War biplane.jpg|thumb]]
The L.1 was powered by a {{convert|50|hp|kW|abbr=on|0|}} [[Gnome]] [[rotary engine]], much less powerful than the {{convert|160|hp|kW|abbr=on|0|}} Gnome in the D.III,<ref name=Opdycke/> but both engines were mounted on tubular steel extensions of the main wooden fuselage.  This was rectangular in cross section, with four [[Fraxinus|ash]] [[longeron]]s interconnected by [[spruce]] [[strut]]s,<ref name=Flight2/> internally braced by wires and aluminium covered ahead of the cockpit, extending to a partial, oil deflecting [[aircraft fairing#Engine cowling|cowling]] around the upper half of the rotary engine.  Behind the single seat open [[cockpit]] the fuselage was [[aircraft fabric covering|fabric covered]].<ref name=Flight1/> As on the Ponnier monoplanes<ref name=Opdycke/> there was no fixed [[fin]] but just a rounded, flat topped [[rudder]].  The [[tailplane]] was mounted on top of the fuselage and like the monoplane Cavalrie carried separate [[Elevator (aeronautics)|elevator]]s;<ref name=Opdycke/> together they formed a horizontal rectangular tail. All the tail surfaces were steel tube structures.<ref name=Flight1/>

The L.1 was a [[biplane#Bays|single bay biplane]] with a pair of tall, parallel [[interplane strut]]s with [[flying wires|flying and landing wires]] on each side. There was mild [[stagger (aviation)|stagger]] and [[dihedral (aircraft)|dihedral]].  The wings used a thick [[airfoil]] and were straight edged, slightly tapered and square tipped; the lower wing had a slightly smaller span. The upper wing had a deep cut-out to provide some upward vision for the pilot, who sat under the wing just aft of mid-[[chord (aircraft)|chord]]. The It had a fixed, [[conventional undercarriage]] with its mainwheels on a single axle mounted on a pair of V-struts to the lower fuselage [[longeron]]s, assisted by a long tailskid, mounted well forward.<ref name=Flight1/>

Ponnier had hoped for military orders but none came.<ref name=Munson/> When the L.1 first appeared its suitability for more powerful engines was noted; rotaries with powers of up to {{convert|100|hp|kW|abbr=on|0|}} were suggested.<ref name=Flight1/>  The [[Ponnier M.1]], flown in 1915 and the only Ponnier fighter to reach production, benefited from the L.1;<ref name =Flight3/> it was smaller and better streamlined, but shared some features like the thick wings, large gap, moderate  stagger and slight span difference between upper and lower planes, the finless vertical tail and simple undercarriage. Its engine was a {{convert|80|hp|kW|abbr=on|0|}} [[Le Rhone]].<ref name=G&S/>


<!-- ==Operational history== -->
<!-- ==Variants== -->

==Specifications==
[[File:Ponnier Scout.3 view.png|thumb|right]]
{{Aircraft specs
|ref=Flight 14 August 1914 p.<ref name=Flight1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length ft=17
|length in=5
|length note=
|span ft=26
|span in=3
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqft=220
|wing area note=
|aspect ratio=
|airfoil=
|empty weight lb=570
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Gnome]]
|eng1 type=7-cylinder [[rotary engine|rotary]]
|eng1 hp=50
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia ft=8
|prop dia in=4
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed mph=65
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed mph=33
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=8 min to 1,000 m (3,050 ft), with 160 kg (350 lb) load
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{commons category|Ponnier L.1}}
{{reflist|refs=

<ref name=G&S>{{cite book |last=Green |first=William |first2= Gordon|last2= Swanborough |title= The Complete Book of Fighters |date=1994 |publisher= Salamander Books|location=Godalming, UK|isbn=1-85833-777-1 |page=209; 479}}</ref>

<ref name=Munson>{{cite book |title=The Pocket Encyclopedia of World Aircraft in Colour - Pioneer Aircraft 1903-14 |last=Munson |first=Kenneth|year=1969|page=145|publisher=Blandford Press|location=London |isbn=}}</ref>

<ref name=Opdycke>{{cite book |title=French aeroplanes before the Great War |last=Opdycke |first=Leonard E.|year=1999|page=209|publisher=Shiffer Publishing Ltd|location=Atglen, PA, USA |isbn=0-7643-0752-5}}</ref>

<ref name=Flight1>{{cite magazine|date=14 August 1914|title=The Ponnier Scouting Biplane|magazine=[[Flight International|Flight]]|volume=VI|issue=38|pages=863–5|url=http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200863.html}}</ref>

<ref name=Flight2>{{cite magazine|date=17 January 1914|title=Ponnier|magazine=[[Flight International|Flight]]|volume=VI|issue=3|pages=60–1|url=http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200060.html}}</ref>

<ref name=Flight3>{{cite magazine|date=16 March 1916|title=Eddies|magazine=[[Flight International|Flight]]|volume=VIII|issue=11|page=219|url=http://www.flightglobal.com/pdfarchive/view/1916/1916%20-%200219.html}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->

[[Category:French fighter aircraft 1910–1919]]