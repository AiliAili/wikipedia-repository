{{italic title}}
{{Infobox locomotive
| name             = ''John Bull''
| image            = John Bull.jpg
| alt              = 
| caption          = ''John Bull'', {{circa|1895}}
| powertype        = Steam
| designer         = 
| builder          = [[Robert Stephenson and Company]]
| ordernumber      = 
| serialnumber     = 
| buildmodel       = 
| builddate        = {{Start date|1831}}
| totalproduction  = 
| rebuilder        = 
| rebuilddate      = 
| numberrebuilt    = 
| whytetype        = [[4-2-0]] (built by Stephenson as an [[0-4-0]])
| uicclass         = 1'1A (built as B)
| gauge            = {{Track gauge|ussg}}
| leadingdiameter  = 
| driverdiameter   = {{convert|4|ft|6|in|m|abbr=on}}<ref name=RailwayAge672>{{cite journal| journal=Railway Age Gazette|page=672| volume=51| issue=14| date=October 6, 1911| title=Shop Safeguards| author=Burlingame, Luther D.}}</ref>
| trailingdiameter = 
| minimumcurve     = 
| wheelbase        = {{convert|4|ft|11|in|m|abbr=on}}<br />between driving axles
| length           = {{convert|14|ft|9|in|m|abbr=on}}&nbsp;— frame
| width            = {{convert|6|ft|3|in|m|abbr=on}}&nbsp;— frame
| height           = 
| axleload         = 
| weightondrivers  = 
| locoweight       = 10 ton<ref name=Johnson />
| tenderweight     = 
| locotenderweight = 
| tendertype       = 
| fueltype         = 
| fuelcap          = 
| watercap         = 
| tendercap        = 
| sandcap          = 
| boiler           = {{convert|2|ft|6|in|m|abbr=on}} diameter ×<br />{{convert|6|ft|9|in|m|abbr=on}} length
| boilerpressure   = 
| feedwaterheater  = 
| firearea         = {{convert|10.07|ft2|m2}}
| tubearea         = 
| fluearea         = 
| tubesandflues    = 
| fireboxarea      = 
| totalsurface     = {{convert|213|sqft|abbr=on}}<ref name=RailwayAge672 />
| superheatertype  = 
| superheaterarea  = 
| cylindercount    = 
| cylindersize     = {{convert|9|in|cm|abbr=on}} diameter ×<br />{{convert|20|in|cm|abbr=on}} stroke
| frontcylindersize= 
| rearcylindersize = 
| hpcylindersize   = 
| lpcylindersize   = 
| valvegear        = 
| valvetype        = 
| valvetravel      = 
| valvelap         = 
| valvelead        = 
| transmission     = 
| maxspeed         = 
| poweroutput      = 
| tractiveeffort   = 
| factorofadhesion = 
| trainheating     = 
| locobrakes       = 
| locobrakeforce   = 
| trainbrakes      = 
| safety           = 
| operator         = [[United New Jersey Railroad and Canal Company|Camden and Amboy Railroad]],<br />[[Pennsylvania Railroad]] (initial preservation)
| operatorclass    = 
| powerclass       = 
| numinclass       = 
| fleetnumbers     = 1
| officialname     = ''Stevens'' (after C&A president [[Robert L. Stevens]])
| nicknames        = 
| axleloadclass    = 
| locale           = 
| deliverydate     = {{Date|1831-09-04}}
| firstrundate     = {{Date|1831-09-15}}
| lastrundate      = 
| retiredate       = 1866
| withdrawndate    = 
| preservedunits   = 
| restoredate      = {{Date|1981-09-15}}
| scrapdate        = 
| currentowner     = [[Smithsonian Institution]]
| disposition      = Static display at the [[National Museum of American History]]
| notes            =
}}
'''''John Bull''''' is a British-built [[Rail transport|railroad]] [[steam locomotive]] that operated in the [[United States]]. It was operated for the first time on September 15, 1831, and it became the oldest operable steam locomotive in the world when the [[Smithsonian Institution]] operated it in 1981.<ref name=HistoryWired>{{cite web| url=http://historywired.si.edu/detail.cfm?ID=225| title=John Bull Locomotive| work=History Wired| publisher=Smithsonian Institution| accessdate=August 4, 2008}}</ref><ref name=Klein280>Klein and Bell, pp 280–1.</ref>  Built by [[Robert Stephenson and Company]], the ''John Bull'' was initially purchased by and operated for the [[United New Jersey Railroad and Canal Company|Camden and Amboy Railroad]], the first railroad in [[New Jersey]], which gave ''John Bull'' the number 1 and its first name, "''Stevens''". ([[Robert L. Stevens]] was president of the Camden and Amboy Railroad at the time.). The C&A used the locomotive heavily from 1833 until 1866, when it was removed from active service and placed in storage.

After the C&A's assets were acquired by the [[Pennsylvania Railroad]] (PRR) in 1871, the PRR refurbished and operated the locomotive a few times for public displays: it was fired up for the [[Centennial Exposition]] in 1876 and again for the National Railway Appliance Exhibition in 1883. In 1884 the locomotive was purchased by the [[Smithsonian Institution]] as the museum's first major industrial exhibit.

In 1939 the employees at the PRR's [[Altoona, Pennsylvania|Altoona]], [[Pennsylvania]], workshops built an operable replica of the locomotive for further exhibition duties, as the Smithsonian desired to keep the original locomotive in a more controlled environment. After being on static display for the next 42 years, the Smithsonian commemorated the locomotive's 150th birthday in 1981 by firing it up, making it the world's oldest surviving operable steam locomotive. Today, the original ''John Bull'' is on static display once more in the Smithsonian's [[National Museum of American History]] in [[Washington, D.C.]] The replica ''John Bull'' is preserved at the [[Railroad Museum of Pennsylvania]].

==Construction and initial use==
[[File:John Bull and train, as drawn by Isaac Dripps in 1887.jpg|thumb|left|The ''John Bull'' and train as it looked in 1831; drawn by Isaac Dripps in 1887. <br />''(Image from the collection of the National Museum of American History, Smithsonian Institution, [http://americanhistory.si.edu/onthemove/ America On The Move] exhibit, used with permission)''.]]

The  ''John Bull'' was built in [[Newcastle upon Tyne|Newcastle]], England, by [[Robert Stephenson and Company]] for the [[United New Jersey Railroad and Canal Company|Camden and Amboy Railroad]] (C&A), the first railroad built in New Jersey.<ref name=Johnson>{{cite book| url=https://books.google.com/?id=ThOh8bBIN9QC&dq=John+Bull+locomotive| title=American Railway Transportation| author=Johnson, Emory Richard| authorlink=Emory Richard Johnson| year=1908| pages=41–42| publisher=D. Appleton| accessdate=August 4, 2008}}</ref><ref name=PAmuseumreplica>{{cite web| url=http://www.rrmuseumpa.org/about/roster/johnbull.shtml| title=Camden & Amboy John Bull Replica| publisher=Railroad Museum of Pennsylvania| year=2008| accessdate=August 4, 2008}}</ref>  It was dismantled and then shipped across the [[Atlantic Ocean]] in crates aboard the ''Allegheny''.<ref name=HistoryWired /> C&A engineer [[Isaac Dripps]] reconstructed the locomotive to the best of his ability (the shipment did not include any drawings or instructions to assemble the locomotive) and ran it for the first time in September 1831.<ref name=Carter140>Carter, p 140.</ref><ref>Whittemore, p 32.</ref>  On November 12, 1831, Robert Stevens (then president of the C&A) repaid some political debts by inviting several members of the [[New Jersey]] legislature and some local dignitaries, including [[Napoleon]]'s nephew [[Prince Achille Murat|Prince Murat]], for rides behind the newly delivered locomotive over a short test track. The prince's wife, [[Catherine Willis Gray]], made a point of hurrying onto the train so she could be declared the first woman to ride a steam-powered train in America, though unknown to her, men and women had already ridden behind the steam powered maiden runs on the [[Baltimore and Ohio]], [[South Carolina Canal and Railroad Company|South Carolina Railroad]], and the [[Albany and Schenectady Railroad|Mohawk and Hudson]] by this point in 1831.<ref name=HistoryWired /><ref name=Carter140 /><ref name=PRRchron1831>{{cite web| url=http://www.prrths.com/Hagley/PRR1831%20June%2004.wd.pdf| title=PRR Chronology: 1831| publisher=Pennsylvania Railroad Technical and Historical Society|date=June 2004| accessdate=August 4, 2008| format=PDF}}</ref><ref name=Wilson225>Wilson, p 225.</ref>

Until the railroad was completed, the locomotive was placed in storage; horse-drawn cars served the construction efforts until 1833.<ref name=HistoryWired />  The C&A applied both numbers and names to their first locomotives, giving this engine the number 1 and officially naming it ''Stevens'' (after the C&A's first president, [[Robert L. Stevens]]).<ref name=Whittemore30>Whittemore, p 30.</ref>  However, through regular use of the engine, crews began calling it ''the old John Bull'',<ref name=PAmuseumreplica /> a reference to the cartoon personification of England, [[John Bull]]. Eventually the informal name was shortened to ''John Bull'' and this name was so much more widely used that ''Stevens'' fell out of use.<ref name=PAmuseumreplica />

In September 1836 the ''John Bull''  and two [[coach (rail)|coach]]es were shipped by canal to [[Harrisburg, Pennsylvania|Harrisburg]], and became the first locomotive to operate there.<ref name=Wilson54>Wilson, p 54.</ref>

==Mechanical modifications and early exhibitions==
Stephenson built the locomotive originally as an [[0-4-0]] (an 0-4-0 is the [[Whyte notation]] for a steam locomotive with two [[driving wheel|powered axles]] and no unpowered [[leading wheel|leading]] or [[trailing wheel|trailing axles]]). The locomotive's power was transmitted to the driving axles through pistons that were mounted under the boiler between the two front wheels and in front of the front axle. These inside cylinders' main rods were connected to a rear crank axle with a [[connecting rod]] between the two axles to power the front axle.

[[File:John Bull as it appeared in 1877.jpg|thumb|left|The ''John Bull'' as it appeared in 1877. Note the [[Cab (locomotive)|cab]] and wider exhaust stack.<br />''(Image from the collection of the National Museum of American History, Smithsonian Institution, [http://americanhistory.si.edu/onthemove/ America On The Move] exhibit, used with permission)''.]]

Due to poorer quality track than was the norm in its native England, the locomotive had much trouble with derailment; the C&A's engineers added a [[leading truck]] (an assembly consisting of an unpowered axle with smaller diameter wheels that was connected to the frame and pushed in front of the locomotive) to help guide the engine into curves.<ref name=HistoryWired />  The leading truck's mechanism necessitated the removal of the coupling rod between the two main axles, leaving only the rear axle powered. Effectively, the ''John Bull'' became a [[4-2-0]] (a locomotive with two unpowered axles, one powered main axle, and no trailing axles). Later, the C&A also added a [[pilot (locomotive)|pilot]] ("cowcatcher") to the lead truck.<ref name=Klein283>Klein and Bell, pp 283–287.</ref>  The cowcatcher is an angled assembly designed to deflect animals and debris off of the railroad track in front of the locomotive. To protect the locomotive's crew from the weather, the C&A also added walls and a roof (a [[Cab (locomotive)|cab]]) to the rear of the locomotive where the controls were located. C&A workshop crews also added safety features such as a [[Locomotive bell|bell]] and [[Locomotive headlamp|headlight]].

After several years serving as a [[switcher|switching engine]] (a locomotive used for moving railroad cars around within a [[rail yard|railroad yard]]; also known as a shunter) and stationary [[boiler (steam generator)|boiler]],<ref name=PRRchron1866>{{cite web| url=http://www.prrths.com/Hagley/PRR1866%20June%2004.wd.pdf| title=PRR Chronology: 1866| publisher=Pennsylvania Railroad Technical and Historical Society|date=June 2004| format=PDF| accessdate=August 4, 2008}}</ref> the ''John Bull'' was retired in 1866 and stored in [[Bordentown, New Jersey]]. Toward the end of its life in revenue service, the locomotive worked as a [[pump]] engine and as the power for a sawmill.<ref name=White32-4>White, pp 32–34.</ref>

The C&A was soon absorbed into the [[United New Jersey Railroad and Canal Company]] (1869) which itself was merged into the [[Pennsylvania Railroad]] (PRR) in 1871.<ref>{{cite web| url=http://rnetzlof.pennsyrr.com/corphist/urrnj.html| title=Corporate Genealogy: United New Jersey Railroad| author=Netzlof, Robert T.|date=February 7, 2001| accessdate=August 4, 2008}}</ref>  The PRR saw the potential publicity to be gained by exhibiting such an old engine, showing it at the 1876 [[Centennial Exposition]] in [[Philadelphia]];<ref name=Forney177>{{cite magazine| title=American Locomotives and Cars| author=Forney, M. N.| page=177| volume=IV|magazine=Scribner's Magazine| issue=2|date=August 1888| authorlink=Matthias N. Forney}}</ref> PRR workshop staff then "back-dated" the engine (by replacing some original parts with parts that "looked" old or by removing them entirely). The [[Chimney|exhaust stack]] was replaced with a straight tube of metal and the cab walls and roof were removed. The PRR then exhibited the engine in 1883 at the ''National Railway Appliance Exhibition'' in [[Chicago|Chicago, Illinois]].<ref name=White39 />  In 1885, the [[Smithsonian Institution]] accepted the donation of the ''John Bull'' from the PRR as the Institution's first large engineering artifact.<ref name=HistoryWired />

==Smithsonian Institution and locomotive restoration==
At the exhibition in 1883, the Pennsylvania Railroad ended up resolving two problems at once. In the Smithsonian Institution, the railroad was able to find a home for the historic locomotive, as well as a suitable new employer for a young civil engineer named [[J. Elfreth Watkins]]. Watkins had been involved in an accident on the railroad in [[New Jersey]] a few years before the exhibition.  He had lost a leg in the accident, so he was no longer suited to the physical demands of railroad work, although the railroad did employ him as a clerk for a while after his accident. The PRR employed his engineering experience as an expert curator for the Smithsonian's new Arts and Industries Building, which was opened in 1880.<ref>{{cite web| author=Massa, William R., Jr.| publisher=Smithsonian Institution| year=2004| url=http://siarchives.si.edu/findingaids/FARU7268.htm| title=Finding Aids to Personal Papers and Special Collections in the Smithsonian Institution Archives: Record Unit 7268; J. Elfreth Watkins Collection, 1869, 1881–1903, 1953, 1966 and undated| accessdate=August 5, 2008}}</ref><ref>{{cite web| url=http://americanhistory.si.edu/archives/d8523.htm| title=John H. White, Jr. Reference Collection, 1880s-1990| work=Archives Center, National Museum of American History| publisher=Smithsonian Institution|date=December 27, 2002| accessdate=August 4, 2008}}</ref>  The locomotive's first public exhibition at the Smithsonian occurred on December 22, 1884, where it was displayed in the East Hall of the Arts and Industries building.<ref name=White39>White, p 39.</ref>

[[File:John Bull at the Columbian Exposition-2.jpg|thumb|''John Bull'' at the [[World's Columbian Exposition]] in 1893]]
The locomotive remained on display in this location for nearly 80 years, but it was transported for display outside the museum on certain rare occasions. The most significant display in this time occurred in 1893 when the locomotive traveled to [[Chicago, Illinois|Chicago]] for the [[World's Columbian Exposition]].<ref>{{cite book| title=The World's Columbian Exposition, Chicago, 1893|author1=White, Trumbull |author2=Igleheart, William |author3=Palmer, Bertha Honoré | year=1893| publisher=P.W. Ziegler and Company| page=286 }}</ref>  The Pennsylvania Railroad, like many other railroads of the time, put on grand displays of their progress; the PRR arranged for the locomotive and a couple of coaches to be delivered to the railroad's [[Jersey City, New Jersey|Jersey City]], [[New Jersey]], workshops where it would undergo a partial restoration to operating condition. The PRR was planning an event worthy of the locomotive's significance to American railroad history&nbsp;— the railroad actually planned to operate the locomotive for the entire distance between New Jersey and Chicago.<ref name=White39 />

The restoration was supervised by the PRR's chief mechanical officer, [[Theodore N. Ely]]. Ely was confident enough in its 50-mile (80.5&nbsp;km) test run to [[Perth Amboy, New Jersey|Perth Amboy]], [[New Jersey]] (which took two hours and fifteen minutes), that the [[Governor (United States)|governors]] of all the states that the locomotive was to pass through and the then [[President of the United States]], [[Grover Cleveland]], were invited to ride behind the engine on its first leg toward Chicago. The ''John Bull'' was to pull a few [[Passenger car (rail)|passenger car]]s in a train that would carry dignitaries and representatives of the press. The train traveled to [[Philadelphia, Pennsylvania|Philadelphia]], [[Pennsylvania]], in the charge of one locomotive crew. From Philadelphia, local engineers (train drivers) were employed to ride on the locomotive's footplate as [[maritime pilot|pilots]] to advise the operators for the trip over the local engineers' territories for the rest of the journey to Chicago. Traveling at {{convert|25|to|30|mph|km/h}}, the train departed from the Pennsylvania Railroad's Jersey City station at 10:16&nbsp;a.m. on April 17<ref>{{cite news| url=https://query.nytimes.com/mem/archive-free/pdf?res=9F06E3DC1031E033A2575BC1A9629C94629ED7CF| format=PDF| work=New York Times| date=April 18, 1893| title=John Bull on the Way West| accessdate=August 4, 2008}}</ref> and reached Chicago on April 22.<ref name=Carter140 />  The locomotive operated during the exhibition giving rides to the exhibition's attendees, and then the train left Chicago on December 6 for the return trip to Washington. The locomotive arrived back in Washington on December 13.<ref>White, pp 39–45.</ref>

In 1927 the ''John Bull'' again traveled outside the museum. The [[Baltimore and Ohio Railroad]] was celebrating its centenary that year in its [[Fair of the Iron Horse]] in [[Baltimore, Maryland|Baltimore]], [[Maryland]].<ref name=HistoryWired />  Since the locomotive's original [[tender (locomotive)|tender]] (fuel and water car) had deteriorated beyond repair and was dismantled in 1910, the PRR built a replica of the tender at its [[Altoona, Pennsylvania|Altoona]], [[Pennsylvania]], workshops.<ref name=PAmuseumroster>{{cite web| url=http://www.rrmuseumpa.org/about/roster/locomotiveroster.pdf| title=Motive Power Roster| publisher=Railroad Museum of Pennsylvania| format=PDF|date=August 2007| accessdate=August 4, 2008}}</ref>  The locomotive was also refurbished in Altoona for operation during the fair.<ref name=White45>White, p 45.</ref>  This fair was the last steam up for the locomotive until 1980.

==(Mostly) static display==
<!-- Deleted image removed: [[File:John Bull at the Smithsonian, 1920.jpg|thumb|The ''John Bull'' on display in the East Hall, 1920]] -->
After the locomotive returned to the Smithsonian, it remained on static display.  In 1930 the museum commissioned the [[Altoona Works]] to build a second replica of the locomotive's [[tender (railroad car)|tender]] for display with the locomotive in the museum. This time, however, the replica tender re-used some of the fittings that the museum had retained when the original tender was dismantled twenty years earlier.<ref name=White45 />

The Smithsonian recognized the locomotive's age in 1931, but, since the museum didn't have the funds to refurbish the locomotive for full operation again, it was decided to run the locomotive in place (with the driving wheels lifted off the rails using [[Jack (device)|jacks]]) with compressed air. The museum borrowed an 1836 coach from the Pennsylvania Railroad to display on the track behind the newly rebuilt tender, and the locomotive's 100th birthday was officially celebrated on November 12, 1931. The locomotive's semi-operation was broadcast over the [[CBS|CBS radio network]] with [[Stanley Bell]] narrating the ceremonies for the radio audience.<ref>White, pp 45–46.</ref>

The PRR again borrowed the locomotive from 1933 to 1934 for the [[Century of Progress]] exhibition in Chicago. Unlike its earlier jaunt to Chicago, for this trip, the railroad hauled and displayed it as a static exhibit. While this exhibit was progressing, the Altoona Works were busy again building a replica; this time the replica was an operable copy of the locomotive. The replica was then operated in 1940 at the [[1939 New York World's Fair|New York World's Fair]], while the original locomotive and rebuilt tender returned to the Smithsonian.<ref name=White46>White, p 46.</ref>

The original locomotive was displayed outside the museum one more time in 1939 at the New York World's Fair, but the museum's curators decided that the locomotive was becoming too fragile for repeated outside exhibits. It was then placed in somewhat permanent display back in the East Hall where it remained for the next 25 years.  In 1964 the locomotive was moved to its current home, the [[National Museum of American History]], then called the Museum of History and Technology.<ref name=White46 />

[[File:John Bull operating in 1981.jpg|thumb|200px|The original locomotive under steam in 1981]]
The ''John Bull'' had remained on static display for another 15 years, but the locomotive's significance as one of the oldest locomotives in existence, or its use on the first railroad in New Jersey, was not very plainly noted in the display's literature. As 1981 and the locomotive's 150th birthday approached, the Smithsonian started discussions on how best to commemorate the locomotive's age and significance. There was very little question that special publications and exhibits would be prepared, but museum officials were left with the thought that the exhibit could still be so much more than that.<ref name=White46-48>White, pp 46–48</ref>

Many superficial inspections were performed on the locomotive in 1980 and it was found to be in relatively sound mechanical condition. There wasn't a significant amount of deterioration noted in these early inspections, and when the wheels were jacked off the rails, as they had been 50 years earlier, the axles were found to be freely operable. One morning in January 1980, before the museum opened to the public, museum officials used compressed air to power the cylinders and move the wheels through the connecting rods for the first time since its last semi-operation. After the compressed air blew some dirt and debris out of the locomotive's exhaust stack, it was soon running smoothly.<ref name=White46-48 />

The running gear seemed to be in good order, but it was still unknown if the boiler could still handle the pressure of steam and a live fire again. The museum asked the [[Hartford Steam Boiler Inspection and Insurance Company]] to inspect the locomotive's boiler for operation. The inspections were conducted after hours at the museum (from 6:30&nbsp;p.m. to 4:00&nbsp;a.m.) over three days and included [[electromagnet]]ic, [[ultrasound|ultrasonic]], and [[radiography|radiographic]] tests. The tests did reveal a few flaws, but it was projected that the engine could operate at a reduced boiler pressure of 50 [[pound-force per square inch|psi]] (340 [[kilopascal|kPa]] or 3.5 [[kilogram-force|kgf]]/cm²); as delivered to the Camden & Amboy, the boiler was rated for {{convert|70|psi|kPa|abbr=on}} or 4.9 kgf/cm². The Smithsonian's staff, after a few further hydrostatic tests, were confident that the locomotive could again operate under its own power.<ref name=White46-48 />  The items that needed repair were repaired, and on October 14, 1980, the locomotive was successfully field-tested on the Warrenton Branch Line in Fauquier County between Calverton and Casanova, Virginia. The site was selected, because at the time, only one freight train per week used the branch line. On September 15, 1981, the locomotive operated under steam on a few miles of branch line near the [[Potomac River]] within [[Washington, D.C.]]  With this exhibition, the locomotive became the oldest operable steam locomotive (and oldest self-propelled [[vehicle]]) in the world.<ref name=HistoryWired />

The original ''John Bull'' is housed on static display at the National Museum of American History in Washington, D.C. The replica of the ''John Bull'', built in 1939, is owned and preserved by the [[Railroad Museum of Pennsylvania]] and is, as of 2009, their only operational piece of equipment.

==Timeline==
[[File:John Bull NMAH side.jpg|thumb|220px|''John Bull'' locomotive at the National Museum of American History]]
[[File:John Bull Repilca front.jpg|thumb|220px|''John Bull'' replica in Pennsylvania]]
* June 18, 1831: The ''John Bull'' is constructed by Stephenson and Company in England.<ref name=White22>White, p 22.</ref>
* July 14, 1831: The ''John Bull'' departs [[Liverpool]] aboard the ship ''Allegheny'' bound for [[Philadelphia, Pennsylvania]].<ref name=White22 />
* September 4, 1831: The ''John Bull'' arrives in Philadelphia.
* September 15, 1831: The ''John Bull'' makes its first runs in New Jersey under its own power.
* November 12, 1831: Robert Stevens hosts a group of New Jersey politicians on a series of trial runs pulled by the ''John Bull''.
* 1833: The ''John Bull'' is one of a few locomotives operating on the newly completed [[Camden and Amboy Railroad]].
* 1866: The ''John Bull'' is retired from regular service.
* 1876: The ''John Bull'' is displayed at the United States [[Centennial Exposition]] in Philadelphia.
* 1883: The Pennsylvania Railroad displays ''John Bull'' at the ''National Railway Appliance Exhibition'' in [[Chicago|Chicago, Illinois]].<ref name=White39 />
* 1884: The Smithsonian Institution acquires the ''John Bull'' from the Pennsylvania Railroad
* 1893: The ''John Bull'' operates at the [[World's Columbian Exposition]] in Chicago.
* 1910: The original tender, now deteriorated beyond repair, is dismantled by Smithsonian staff. Usable fittings from the tender are placed in storage.
* 1927: The [[Baltimore and Ohio Railroad]] borrows the ''John Bull'' to operate at the ''Fair of the Iron Horse'' in [[Baltimore, Maryland]].
* 1930: A replica tender is commissioned by the Smithsonian and built by the Pennsylvania Railroad using the fittings previously salvaged from the original tender; the new tender is displayed with the locomotive at the museum.<ref name=White45 />
* November 12, 1931: The Smithsonian celebrates the locomotive's 100th "birthday," using compressed air to operate the stationary engine (stabilized on jacks) within the museum's exhibit hall.
* 1933–1934: The Pennsylvania Railroad borrows the ''John Bull'' to display it at the [[Century of Progress]] Exhibition in Chicago.<ref name=White46 />
* 1939: The original ''John Bull'' is displayed in the opening of the [[1939 New York World's Fair|New York World's Fair]]
* 1940: A replica of the ''John Bull'', built by engineers at the Pennsylvania Railroad's Juniata Shops in [[Altoona, Pennsylvania]], is displayed at the New York World's Fair, and the original is returned to the Smithsonian.
*  October 14, 1980: The ''John Bull'' is restored to operating condition, and tested on the Warrenton Branch Line in Fauquier County, Virginia.
* September 15, 1981: The ''John Bull'' operates in Washington, D.C., on the 150th anniversary of its first use, becoming the oldest operable steam locomotive (and oldest self-propelled vehicle) in the world.
* 1985: The ''John Bull'' is carried aboard an airplane for an exhibition in  [[Dallas, Texas]], making it the oldest locomotive in the world to travel by air.<ref name=Legacies>{{cite web| url=http://www.smithsonianlegacies.si.edu/objectdescription.cfm?ID=25| title=John Bull locomotive, 1831| work=Legacies| publisher=Smithsonian Institution Press| year=2001| accessdate=August 4, 2008}}</ref>

==See also==
{{Portal|Trains|Washington, D.C.}}

* [[LMR 57 Lion|LMR 57 ''Lion'']]
* ''[[Stourbridge Lion]]''

==Notes==
{{Reflist|30em}}

==References==
{{Commons category|John Bull locomotive}}
* {{cite book| url=https://books.google.com/?id=fj4dAAAAIAAJ&dq=John+Bull+locomotive| title=When Railroads Were New| author=Carter, Charles Frederick| publisher=H. H. Holt| year=1909 }}
* {{cite book|author1=Klein, Randolph Shipley  |author2=Bell, Whitfield Jenks| title=Science and Society in Early America| publisher=Diane Publishing| year=1986| isbn=0-87169-166-3 }}
* {{cite book|author=White, John H., Jr.|year=1981|title=The John Bull: 150 Years a Locomotive|publisher=Smithsonian Institution Press, Washington, DC|isbn=0-87474-961-1}}
* {{cite book| title=Fulfillment of Three Remarkable Prophecies in the History of the Great Empire State Relating to the Development of Steamboat Navigation and Railroad Transportation| year=1909| author=Whittemore, Henry| url= https://books.google.com/books?id=_ZRDAAAAIAAJ&printsec=frontcover&dq=Fulfillment+of+Three+Remarkable+Prophecies+in+the+History#v=onepage&q&f=false}}
* {{cite book| author=Wilson, William Bender| title=History of the Pennsylvania Railroad with Plan of Organization, Portraits of Officials and Biographical Sketches| volume=Vol 1| publisher=Henry T. Coates and Company| location=Philadelphia| year=1895 | url=https://books.google.com/books?id=GzBktVdJK9wC&dq=History%20of%20the%20Pennsylvania%20Railroad%20with%20Plan%20of%20Organization%201&pg=PP1#v=onepage&q&f=false}}
* {{cite book| author=Wilson, William Bender| title=History of the Pennsylvania Railroad with Plan of Organization, Portraits of Officials and Biographical Sketches| volume=Vol 2| publisher=Henry T. Coates and Company| location=Philadelphia| year=1895 | url=https://books.google.com/books?id=UyLtx_KPDAkC&printsec=frontcover&dq=History+of+the+Pennsylvania+Railroad+with+Plan+of+Organization,#v=onepage&q&f=false}}
* [[Railfan and Railroad|Railfan & Railroad]] January (1982)

{{Featured article}}

[[Category:Individual locomotives of the United States]]
[[Category:0-4-0 locomotives]]
[[Category:4-2-0 locomotives]]
[[Category:Early steam locomotives]]
[[Category:Preserved steam locomotives of the United States]]
[[Category:Artifacts in the collection of the Smithsonian Institution]]
[[Category:1831 works]]
[[Category:Pennsylvania Railroad locomotives]]
[[Category:Preserved steam locomotives of Great Britain]]
[[Category:Unique locomotives]]
[[Category:Standard gauge locomotives of the United States]]