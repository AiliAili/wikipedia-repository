{{Use dmy dates|date=February 2013}}
{{Infobox company
| name     = Angel Bakeries
| logo     = [[File:Angel Bakeries Logo.jpg|180px]]
| slogan   =
| type     = Public ({{TLV|ANGL}})
| genre            =
| foundation       = 1927
| founder          = Salomon Angel
| location         = [[Israel]]
| origins          = [[Jerusalem]], Israel
| key_people       = Yaron Angel, [[Chief Operating Officer|CEO]], Director <br>Yigal Hayet, Vice President, Director <br> Gadi Angel, Co-CEO, Production
| area_served      = [[Israel]], [[North America]], [[Europe]]
| industry         = [[Bakery]]
| products         = [[Bread]]s, [[hot dog bun|rolls]], [[challah]]s, [[cake]]s, [[cookie]]s, [[pastry|pastries]], [[muffin]]s
| revenue          = 
| operating_income = 
| net_income       = 
| num_employees    = 1,800
| parent           = 
| subsid           = 
| owner            =
| homepage         = [http://www.angel.co.il  www.angel.co.il]
| footnotes        = 
}}
'''Angel Bakeries''' ({{lang-he|מאפיות אנג'ל}} ''Ma'afiyot Anjel''), also known as '''Angel's Bakery''', is the largest commercial [[bakery]] in [[Israel]], producing 275,000 loaves of [[bread]] and 275,000 [[Hot dog bun|rolls]] daily<ref name="finance">{{cite web |url=https://www.google.com/finance?q=TLV:ANGL |title=Salomon A. Angel Ltd. |year=2010 |accessdate=2 August 2010 |publisher=Google finance.com}}</ref> and controlling 30 percent of the country's bread market.<ref name="haaretz">{{cite web |url=http://www.haaretz.com/print-edition/features/jerusalem-s-town-baker-1.267962 |title=Jerusalem's Town Baker |last=Hasson |first=Nir |date=2009-12-01 |accessdate=2010-08-02 |work=[[Haaretz]]}}</ref> With a product line of 100 different types of bread products and 250 different types of cakes and cookies,<ref name="jweekly">{{cite web |url= http://www.jweekly.com/article/full/32575/family-creates-a-baking-dynasty-in-israel/ |title= Family Creates a Baking Dynasty in Israel |last=Elliman |first=Wendy |date=2007-05-25 |accessdate=2010-08-02 |work=jweekly.com}}</ref> Angel sells its goods in 32 company-owned outlets nationwide<ref name="Hebrew">{{cite web |url=http://www.angel.co.il/category/CompanyProfile |title=Company Profile '''(Hebrew)''' |year=2008 |accessdate=2010-08-02 |publisher=Angel Bakeries website}}</ref> and distributes to 6,000 stores and hundreds of hotels and army bases.<ref name="jweekly" /> It also exports to the United States, United Kingdom, France, Belgium and Denmark.<ref name="angel">{{cite web |url=http://www.angel.co.il/category/profileenglish |title=Angel Bakeries |year=2008 |accessdate=2010-08-02 |publisher=Angel Bakeries}}</ref>

Founded in 1927 in [[Jerusalem]] by Salomon Angel, Angel Bakeries introduced to the Israeli market the first [[sliced bread]], plant-based [[emulsion|emulsifiers]], and new baking technologies. It has always been [[family business|family-run]], at first by Salomon with his brothers and sons,<ref name="solly">{{cite book |url=https://books.google.com/books?id=NZv3Y1mWjyYC&pg=PA25&lpg=PA25&dq=tale+of+scale+angel+bakery&source=bl&ots=3rYeHO_6My&sig=2aOjWwWlBWfHHKSGSzUWajowK8A&hl=en&ei=4Y1WTNy6DdWmsQbGsOjiAQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CBIQ6AEwAA#v=onepage&q&f=false |title=The Tale of the Scale: An odyssey of innovation |last=Angel |first=Solly |date=2004-02-05 |publisher=Oxford University Press |accessdate=2010-08-03 |ISBN=978-0-19-515868-7}}</ref> then by Salomon's grandsons, and today by Salomon's great-grandsons.<ref name="haaretz" /> The company, Salomon A. Angel Ltd., is publicly traded on the [[Tel Aviv Stock Exchange]], with a turnover of $180 million in 2008.<ref name="presentation">{{cite web |url=http://www.angel.co.il/files/english/pitot.pdf |title=Angel Bakeries Presentation Topics |accessdate=2010-08-02 |publisher=Angel Bakeries}}</ref>

==History==

The original Angel's Bakery was established in [[Bayit VeGan]] by Salomon (Shlomo) Angel, a seventh-generation Jerusalemite and scion of a [[Sephardi Jews|Sephardi]] family that traced its lineage back to Jews who were [[Alhambra Decree|expelled from Spain]] in 1492.<ref name="haaretz" /> Born in the [[Mishkenot Sha'ananim]] neighborhood of Jerusalem,<ref name="bread"/> Angel was originally a teacher in an [[Alliance Israélite Universelle|Alliance]] school in Jerusalem, where he fell in love with the principal’s daughter. But principal Chaim Farhi would only allow him to marry his daughter, Esther, if he left teaching and "earned a livelihood". Salomon agreed and became a dry-goods merchant.<ref name="marker">{{cite web |url= http://www.themarker.com/career/1.482238 |script-title=he:כך התחלנו: סיפורי המשפחות שהקימו את המותגים הישראליים הוותיקים|language=Hebrew |trans_title=This Is How We Started: Stories of the families who established the long-standing Israeli brands |first=Shuki |last=Sadeh |date=9 May 2008 |accessdate=13 May 2012 |work=[[TheMarker]]}}</ref> The couple married in 1916 and had three sons and three daughters.<ref name="bread">De Vries, Ammanjah (2000-07-21). "Jerusalem Born & Bread". ''In Jerusalem'', pp. 4–5.</ref>

[[File:Angel's factory store.jpg|225px|left|thumb|The landmark Angel's Bakery factory store in [[Givat Shaul]]. The light board with the number "62" indicates 62 years since the establishment of the [[State of Israel]].]]
Angel became a successful dry-goods merchant, traveling regularly to [[Alexandria]], [[Damascus]], and [[Beirut]] to buy flour and other basic foodstuffs to sell in Jerusalem.<ref name="haaretz" /><ref name="jweekly" /> In 1927 one of Angel's customers, the Trachtenberg Bakery in Bayit VeGan, went [[Bankruptcy|bankrupt]]. Angel decided to pay off the bakery's debts and assume ownership, bringing in his father Avraham and three brothers as partners. A few years later, two of his brothers, Leon and Refael, left to open their own bakery in [[Haifa]].<ref name="bread" /><ref name="marker"/>

Angel improved the bread-making operation by introducing Israel's first automated weighing machines and investing in new production lines.<ref name="history">{{cite web|url=http://www.angel.co.il/category/history |script-title=he:אנשי הלחם - סיפורה של משפחת אנג'ל |trans_title=The Bread People: The story of the Angel family |language=Hebrew |year=2008 |accessdate=2 August 2010 |publisher=Angel Bakeries |deadurl=yes |archiveurl=https://web.archive.org/web/20100518221319/http://www.angel.co.il:80/category/history |archivedate=18 May 2010 |df=dmy }}</ref> He often slept overnight at the bakery during the week and returned to his home in [[Talpiot]] when the bakery was closed on [[Shabbat]]. Later he built an apartment over the bakery to house his family.<ref name="bread" /> In the 1930s, the bakery employed 25 workers and had one production line that turned out 8,500 loaves per day.<ref name="marker"/> It delivered bread to stores in two trucks and a [[Horse-drawn vehicle|horse and wagon]].<ref name="jweekly" /> During World War II, the bakery contracted to supply bread to the British army, necessitating the purchase of large American-made ovens that could turn out 540 loaves per hour.<ref name="marker"/>

Angel's Bakery played a key role in feeding Jerusalem residents during the [[1948 Arab-Israeli War|1948 War of Independence]]. When Arabs [[Siege of Jerusalem (1948)|lay siege]] to the city and attacked [[convoy]]s that attempted to bring food and supplies through the narrow mountain pass from [[Tel Aviv]], water and flour were in short supply. Fire trucks were employed to shuttle water to the bakery and "flour was swept up off the bakery floor".<ref name="jweekly" /> Angel's son Danny, who was a [[Haganah]] fighter defending the besieged [[Old City (Jerusalem)|Old City]], recalled that the bakery would send bread in convoys to the fighters in the Old City and [[Mount Scopus]], and hide guns and ammunition in the sacks of bread.<ref name="jweekly" /> One of the first horses used for the Jewish assault on the Arabs' Old City positions came from Angel's Bakery; its feet were wrapped in old flour sacks so the British wouldn't detect it.<ref name="bread" />

===New baking technologies===
[[File:Angel's flour pipeline 1.jpg|225px|right|thumb|View of the overhead flour pipeline spanning Beit Hadfus Street between the mill and silos (left) and the bakery (right).]]
In the 1950s Salomon's three sons, Avraham, Ovadia and Danny, assumed managerial positions; they became co-[[Chief executive officer|CEOs]] after Salomon's death in 1966.<ref name="marker"/> In 1958 Angel's Bakery moved to its present location in the [[Givat Shaul]] industrial zone at the corner of [[Beit Hadfus Street|Beit Hadfus]] and Farbstein Streets.<ref name="history" /> The site was chosen because it stood across the street from a [[gristmill|flour mill]].<ref name="bread" /> The Angel brothers built Israel's first flour [[silo]] and commissioned a [[Texas]] company to construct a 750-foot pipeline to convey flour directly from the mill to the silo to the bakery. Today this pipeline brings 120 tons of flour to the bakery daily. The invention, initially opposed by the Jerusalem municipality for being above-ground, won the Kaplan Prize for distinction in productivity and efficiency.<ref name="jweekly" />

[[File:Angel's flour pipeline 2.jpg|175px|left|thumb|Overhead view of flour pipeline, with silos at left and Angel Bakeries trucks parked below.]]
Also in 1965, the brothers introduced new long ovens and kneading machines. That same year, they produced the country's first [[sliced bread]] and bread containing [[Soybean#Flour|soy flour]].<ref name="history" />

To enter the [[whole wheat bread]] market, the bakery imported its own wheat-cleaning machine from [[Hungary]]; the machine actually had to be [[Smuggling|smuggled]] out since Hungary did not have diplomatic relations with Israel at the time. However, health-food consumers were wary of the first whole-wheat loaves because they were white, not brown. Working with the Health Food Association, Angel's came up with the solution of adding all-natural [[molasses]] to the bread to give it a brown color.<ref name="bread" />

Angel's developed the only production line in Israel with the capacity to produce 3,300 loaves of bread per hour. Thirteen other bread production lines in its various plants each yield over 2,000 loaves per hour.<ref name="bread" /> One production line in Jerusalem has the capacity to turn out 10,000 [[pita]]s in three hours.<ref>{{cite web |url=http://www.mfa.gov.il/MFA/Israel%20beyond%20the%20conflict/The%20Wonder%20Bread%20of%20the%20Middle%20East |title=The Wonder Bread of the Middle East |date=2001-05-01 |accessdate=2010-08-04 |publisher=Israel Ministry of Foreign Affairs}}</ref> On each day of the eight-day [[Hanukkah]] festival, the Jerusalem plant also fries up 250,000 [[sufganiyah|sufaniyot]], the jelly-filled doughnut favored by Israelis at this season.<ref>{{cite web |url=http://www.food-lists.com/lists/archives/clipping-cooking/2003/12/1071940462.php |title=Sufganiyot |last=Mietkiewicz |first=Mark |date=2003-12-20 |accessdate=2010-04-06 |publisher=Food-Lists.com}}</ref>

The bakery's digitally controlled ovens continually adjust temperatures to accommodate the fermenting and baking processes; these ovens can also bake different types of bread at different temperatures on the top and bottom of each oven.<ref name="bread" /> Despite the emphasis on innovation and automation, Angel's [[challah]]s are still braided by hand.<ref name="haaretz" />

===Expansion and diversification===
[[File:Angel's factory store 2.jpg|225px|right|thumb|Interior of factory store in Givat Shaul.]]
Angel's Bakery opened its landmark factory store adjacent to the Givat Shaul plant in 1984.<ref name="haaretz" /> The store offers a full selection of packaged breads, rolls, and [[muffin]]s, [[Börek#Israel|bourekas]], [[Danish pastry|danishes]], fancy cakes, handmade pastries, coffee and soft drinks. For many years, visitors could watch bakers braid challahs through a large side window. At the end of the [[Passover]] holiday, a long line of customers traditionally forms outside the store after midnight, waiting to buy the first bread off the production line.<ref>{{cite web |url= http://jerusalemdiaries.blogspot.com/2007/04/festivities-continue.html  |title=The Festivities Continue&nbsp;... |date=2007-04-10 |accessdate=2010-08-11 |publisher=Jerusalem Diaries}}</ref><ref>{{cite web |url= http://israelity.com/2009/04/16/saying-goodbye-to-the-bread-of-affliction/ |title= Saying goodbye to the bread of affliction |date=2009-04-16 |accessdate=2010-08-11 |publisher=israelity.com}}</ref>

Also in 1984, the company began producing pastries and cakes in a factory in Jerusalem, and beginning in the 1990s it began building and acquiring other bakeries in a move to diversify its products and marketing base.<ref name="history" /> It built a new bakery in [[Lod]] in 1995 and purchased the Tuv Tam bakery in [[Netivot]] in 1999.<ref name="bread" /> Today the company owns and operates five plants:

*Jerusalem – serving Jerusalem and its environs
*[[Kfar HaHoresh]] (Oranim Bakery) – serving Northern Israel
*Lod – serving Central Israel and the [[Gush Dan]] region
*Netivot – serving Southern Israel
*[[Beit Shemesh]] – dedicated factory for cakes and pastries distributed nationwide

The company has branched into the baking of breads with special grains and added [[dietary fiber]]s, reduced-calorie and [[Food fortification|vitamin-fortified]] breads, and certified [[organic food|organic]] products.<ref name="angel" />

In 2002, it introduced [[Parbaking|parbaked]] pita, challah, rolls, [[ciabatta]], "artisan" breads, and [[pizza]] bases, which are [[Flash freezing|flash frozen]] and sold to customers or stores that complete the baking process themselves for a fresher product.<ref name="Hebrew" />

[[File:Angel lechem achid.jpg|180px|left|thumb|Bags of sliced, "dark white" Angel ''lechem achid'' (price-controlled bread) for sale in a Jerusalem grocery.]]
In keeping with government regulations, Angel Bakeries also produces several varieties of price-controlled bread ({{lang-he|לחם אחיד}}, ''lechem achid'') for the low-income sector.<ref>{{cite web |url=http://www.haaretz.com/news/shops-report-price-controlled-bread-shortage-due-to-flour-price-hikes-1.224891 |title=Shops Report Price-Controlled Bread Shortage Due To Flour Price Hikes |last=Bassok |first=Moti |date=2007-07-05 |work=Haaretz}}</ref>

In the late 1990s, the company opened a chain of cafe/bakeshops in four Jerusalem shopping districts. Besides selling the company's bakery-fresh goods, the Angel Cafe serves salads, sandwiches, pasta dishes, desserts, coffees, teas, and soft drinks.<ref>{{cite web |url= http://www.gojerusalem.com/discover/item_668/Angel-Cafe-Nayot |title=Angel Café (Nayot) |accessdate=2010-08-04 |publisher=gojerusalem.com}}</ref>

===Distribution===
Angel Bakeries owns a fleet of 200 trucks that transport its products to 32 company-owned outlets, 6,000 stores, and hundreds of hotels and army bases throughout Israel.<ref name="jweekly" /> The distribution schedule is fully computerized and designed to operate on a "just in time" basis: All the bread that needs to be delivered to a specific location is programmed to come off the production line right before it is loaded onto a truck and sent off, guaranteeing maximum freshness.<ref name="bread" />

Angel Bakeries is an approved supplier to the [[Domino's Pizza]] chain in Israel, and the sole supplier of [[hamburger bun]]s to [[McDonald's]] restaurants in Israel.<ref name="presentation" /> It also exports to the United States, United Kingdom, France, Belgium and Denmark.<ref name="angel" /> In terms of input, the company uses over 5,000 tons of flour per month.<ref name="jweekly" />

==Compliance with Jewish law==
In the interests of keeping the bread [[Kashrut|kosher]] (Angel Bakeries carries the [[hechsher]] of both the [[Edah HaChareidis]] and the [[Orthodox Union]]), Angel's son Ovadia, a trained [[chemist]], collaborated with others at the Angel's-owned Adumim Chemicals plant to develop a new formula for the [[emulsion|emulsifiers]] that bind water and oil in the bread-making process.<ref name="jweekly"/> In those early days, emulsifiers were made with animal-based fats which were not kosher. Ovadia Angel and his team produced emulsifiers from [[Hydrogenation#In the food industry|hydrogenated fats]], a vegetable-based source,<ref>{{cite journal |url=https://books.google.com/books?id=sKoeAQAAMAAJ&q=angel+bakeries+emulsifier&dq=angel+bakeries+emulsifier&hl=en&sa=X&ei=JRywT_TnJpCk8QTGpo2TCQ&redir_esc=y |page=72 |title=Our Bread |journal=The Israel Economist |volume=40 |issue=40 |year=1984}}</ref> and later sold this new knowledge to other companies.<ref name="jweekly"/><ref name="bread"/>

Another [[kashrut]] challenge which the bakery overcame in the early 1950s concerned the ability to produce fresh bread for sale on Sunday mornings after the plant had been closed for [[Shabbat]] in accordance with Jewish law. Normally dough takes four to six hours to rise, but the bakery acquired a mixer made by Tweedy of Burnley, England, which could produce dough in only two hours, thus allowing the bakery to bake bread on Saturday night in time for Sunday-morning deliveries. Today Angel Bakeries owns nine such mixers, five of which are used in its Jerusalem plant.<ref name="bread"/>

Angel Bakeries operates year-round except for [[Shabbat]], [[Jewish holiday|Yom Tov]], and [[Passover]] week. In accordance with [[halakha|Jewish law]], it annually sells its stock to a non-Jew before Passover so that it does not own [[chametz]] over the holiday.<ref>{{cite web |url=http://www.jerusalemkoshernews.com/2009/04/chief-rabbinate-update-015/ |title=Chief Rabbinate Update 015-2009 |date=2009-04-23 |accessdate=2010-08-02 |work=Jerusalem Kosher News}}</ref>

==Family business==
Angel's was the first Israeli bakery to go public on the [[Tel Aviv Stock Exchange]], in 1984.<ref name="jweekly"/> However, it remains a [[family business]]. In the 1980s Salomon’s grandchildren moved into managerial positions.<ref name="marker"/> Avraham, the eldest brother, who headed the financial division, was succeeded by his daughter Ruthie. Ovadia passed down the directorship of production and technology to his son Gadi. Danny handed over the company directorship to his son Yaron.<ref name="jweekly" /><ref name="marker"/>

Angel Bakeries employs 1,800 workers, including [[Israelis]], [[Palestinian people|Palestinians]], and [[Aliyah|new immigrants]]. Some of its workers are third-generation employees. In its over 80-year history, the bakery has never had a [[Strike action|strike]].<ref name="jweekly" />

==Retail brands==
*Angel Bakeries ({{lang-he|מאפיית אנג'ל}} ''Ma'afiyat Angel'') – white, "dark white" and whole-wheat sliced bread, whole-wheat and multi-grain breads, challahs, pita bread, light/sugar-free breads, rolls, buns
*''Lechem Eynan'' ({{lang-he|לחם עינן}}) – pre-germinated whole-wheat bread
*French-style bread ({{lang-he|לחם אנג'ל בסגנון צרפתי}}) – handmade bread using traditional French baking methods
*''Lachmaniyot'' ({{lang-he|לחמניות}}) – bestselling children's [[hot dog bun|buns]] with free plastic sandwich bags in each package<ref>{{cite web |url= http://www.angel.co.il/category/Halahmania |title=Halachmania '''(Hebrew)''' |year=2008 |accessdate=2010-08-04 |publisher=Angel Bakeries}}</ref>
*Wheat crisps ({{lang-he|קליליות}}) – 19-calorie crackers

==Honors and awards==
The intersection of [[Kanfei Nesharim Street|Kanfei Nesharim]] and Farbstein Streets, on the northeastern corner of the bakery, was named Shlomo Angel Square by the Jerusalem municipality in honor of the company's founder.<ref>{{cite web |url= https://maps.google.com/maps?hl=en&q=shlomo+angel+square+jerusalem&um=1&ie=UTF-8&hq=&hnear=Shlomo+Angel+Square,+%D7%9B%D7%A0%D7%A4%D7%99+%D7%A0%D7%A9%D7%A8%D7%99%D7%9D+7,+Jerusalem,+Israel&ei=wcViTMieNIiZOLe_uZUK&sa=X&oi=geocode_result&ct=image&resnum=1&ved=0CBQQ8gEwAA |title=Shlomo Angel Square, 7 Kanfei Nesharim |accessdate=2010-08-11 |publisher=Google Maps}}</ref>

Danny Angel (1920–2009), who worked in the family business from the age of 7<ref name="jweekly" /> until his death at age 89<ref name="death">{{cite web |url= http://www.backwelt.de/newsview/items/news_arch_10519.html |title=Danny Angel died '''(German)''' |date=2009-01-13 |accessdate=2010-08-02 |publisher=backwelt.de}}</ref> was the public face of the company.<ref name="haaretz" /> He was the recipient of many awards, including the Jerusalem Prize, Worthy Citizen of Jerusalem, and Notable Industrialist. He was president of the [[Manufacturers Association of Israel]], the Rotary Club of Israel, and the [[Variety, the Children's Charity|Variety Club of Israel]], which he helped found. He had many friends in the political echelon as well.<ref name="jweekly" /> In recognition of their longtime friendship, Jerusalem [[mayor]] [[Teddy Kollek]] always gave Danny Angel an honorary spot at the bottom of his [[electoral list]]. In 2008, mayoral candidate [[Nir Barkat]] continued the tradition and placed Angel's name at the bottom of his electoral list.<ref name="haaretz" /> Angel's funeral was attended by [[Prime Minister of Israel|Prime Minister]] [[Ehud Olmert]], [[Likud]] party leader [[Benjamin Netanyahu]], and other government officials.<ref name="death" />

==Angel's Bakery, Brooklyn==
Joe Angel, a great-great-grandson of Salomon Angel who, like his cousins, helped out at the family bakery in Jerusalem, moved to the United States in 1969 and earned a degree in bakery management from [[Kansas State University]]. In 1981 he opened his own Angel's Bakery in [[Brooklyn]], New York, specializing in muffins, cookies and cakes.<ref>{{cite web |url=http://www.angelsbakery.com/AB2/aboutus.htm |title=About Us |accessdate=2010-08-02 |publisher=angelsbakery.com}}</ref>

==See also==
{{portal|Companies|Food}}
* [[List of bakeries]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category}}
*[https://web.archive.org/web/20110728105523/http://tiferesyisroel.org/Lachmaniot.pdf  Angel's classic ''Lachmaniyah'' (hot dog bun) recipe, adapted from the Hebrew website]

{{coord|31|47|14|N|35|11|30|E|region:IL_type:landmark_source:kolossus-hewiki|display=title}}

{{Good article}}

[[Category:Bakeries of Israel]]
[[Category:Kosher bakeries]]
[[Category:Buildings and structures in Jerusalem]]
[[Category:Jewish businesses established in Mandatory Palestine]]
[[Category:Food and drink companies established in 1927]]
[[Category:1927 establishments in Mandatory Palestine]]
[[Category:Companies listed on TASE]]
[[Category:Israeli brands]]