{{Infobox Journal
|cover =
|discipline = [[International Relations]]<br>[[Peace and conflict studies]]
|abbreviation = JCCM
|publisher = [[Wiley-Blackwell]]
|frequency = quarterly
|history = 1993–present
|website = [http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-5973]
|ISSN = 0966-0879
}}
The '''''Journal of Contingencies and Crisis Management''''' ('''''JCCM''''') is a [[multi-disciplinary]], [[Peer review|peer-reviewed]] [[academic journal]] that covers all theoretical and practical aspects relating to [[crisis management]].

''JCCM'' is the leading journal on the subject of crisis management. It was founded in 1993 by Prof. Uriel Rosenthal<ref>[http://www.parlement.com/9291000/bio/02273 biography of Prof.Dr. Uri Rosenthal]</ref> and Prof. Alexander Kouzmin ([[University of Plymouth]]). The current editor is Prof. Ira Helsloot ([[Vrije Universiteit Amsterdam]]).

Subject areas include: [[emergency management]], [[risk management]], [[contingency plan]]s, [[foreign policy|foreign policies]], [[ecological crisis]], [[financial crisis]], [[international relations]], [[security]] policies, and [[conflict resolution]].

''JCCM'' is published by [[Wiley-Blackwell]]. Reviews from older issues are regularly re-published in the [[Political ReviewNet]] database.

==References==
{{reflist}}

==External links==
* [http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-5973 ''Journal of Contingencies and Crisis Management'' homepage]

[[Category:International relations journals]]
[[Category:Political science journals]]
[[Category:Strategic management]]

[[Category:Security]]
[[Category:Crisis]]
[[Category:Wiley-Blackwell academic journals]]