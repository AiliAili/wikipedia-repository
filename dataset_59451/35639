{{good article}}{{automatic taxobox
| name               = Abelisaurids
| fossil_range       = [[Middle Jurassic]]-[[Late Cretaceous]], {{fossilrange|170|66}}
| taxon = Abelisauridae
| image              = Aucasaurus.jpg
| image_width        = 250px
| image_caption      = Reconstructed skeleton of ''[[Aucasaurus]]''
| authority  = [[Jose Bonaparte|Bonaparte]] & [[Fernando Novas|Novas]], 1985
| type_species = {{extinct}}''[[Abelisaurus comahuensis]]''
| type_species_authority = Bonaparte & Novas, 1985
| subdivision_ranks  = Subgroups
| subdivision        = 
*{{extinct}}''[[Chenanisaurus]]''  
*{{extinct}}''[[Eoabelisaurus]]'' 
*{{extinct}}''[[Kryptops]]''
*{{extinct}}''[[Rugops]]''
*{{extinct}}''[[Tarascosaurus]]''?
*{{extinct}}''[[Vitakridrinda]]''?
*{{extinct}}''[[Xenotarsosaurus]]'' 
*{{extinct}}'''[[Carnotaurinae]]'''
*{{extinct}}'''[[Majungasaurinae]]'''<ref name="tortosa2014">{{cite journal |doi=10.1016/j.annpal.2013.10.003 |title=A new abelisaurid dinosaur from the Late Cretaceous of southern France: Palaeobiogeographical implications |journal=Annales de Paléontologie |volume=100 |pages=63 |year=2014 |last1=Tortosa |first1=Thierry |last2=Buffetaut |first2=Eric |last3=Vialle |first3=Nicolas |last4=Dutour |first4=Yves |last5=Turini |first5=Eric |last6=Cheylan |first6=Gilles }}</ref>
}}
'''Abelisauridae''' (meaning "Abel's lizards") is a [[family (biology)|family]] (or [[clade]]) of [[ceratosauria]]n [[theropod]] [[dinosaur]]s. Abelisaurids thrived during the [[Cretaceous]] [[Period (geology)|Period]], on the ancient southern [[supercontinent]] of [[Gondwana]], and today their [[fossil]] remains are found on the modern [[continent]]s of [[Africa]] and [[South America]], as well as on the [[India]]n [[subcontinent]] and the island of [[Madagascar]]. Reports based on isolated teeth show the occurrence in the Late Jurassic of Portugal,<ref>{{cite journal |doi=10.11646/zootaxa.3759.1.1 |pmid=24869965 |title=<strong>Abelisauridae (Dinosauria: Theropoda) from the Late Jurassic of Portugal and dentition-based phylogeny as a contribution for the identification of isolated theropod teeth</strong> |journal=Zootaxa |volume=3759 |pages=1–74 |year=2014 |last1=Hendrickx |first1=Christophe |last2=Mateus |first2=Octávio }}</ref> and the confirmed existence of European Abelisaurids comes from the Late Cretaceous of [[France]] with ''[[Arcovenator]]''.<ref name="tortosa2014"/> Abelisaurids first appear in the fossil record of the early middle [[Jurassic]] period, and at least two species, the African ''[[Chenanisaurus]]'' and the Madagascan ''[[Majungasaurus]]'') survived until the end of the [[Mesozoic]] era 66 million years ago.<ref>{{cite journal |doi=10.1016/j.cretres.2017.03.021 |title=An abelisaurid from the latest Cretaceous (late Maastrichtian) of Morocco, North Africa |journal=Cretaceous Research |year=2017 |last1=Longrich |first1=Nicholas R. |last2=Pereda-Suberbiola |first2=Xabier |last3=Jalil |first3=Nour-Eddine |last4=Khaldoune |first4=Fatima |last5=Jourani |first5=Essaid }}</ref> 

Like most theropods, abelisaurids were [[carnivore|carnivorous]] [[biped]]s. They were characterized by stocky hindlimbs and extensive ornamentation of the [[skull]] bones, with grooves and pits. In many abelisaurids, like ''[[Carnotaurus]]'', the forelimbs are [[vestigial]], the skull is shorter and bony crests grows above the eyes. Most of the known abelisaurids would have been between 5 and 9&nbsp;meters (17 to 30&nbsp;ft) in length, from snout to tip of tail, with a new and as yet unnamed specimen from northwestern Turkana in Kenya, Africa reaching a possible length of 11–12 meters (36 to 39 feet).<ref name=bonapartetal1990>{{cite web|url=http://vertpaleo.org/PDFS/0d/0d20d609-f7e6-4bb3-a0c4-765fcffde49b.pdf|title=October/November 2013, Abstracts Of Papers, 73rd Annual Meeting|publisher=[[Society of Vertebrate Paleontology]]|year=2013|accessdate=2013-10-27}}</ref> Before becoming well known, fragmentary abelisaurid remains were occasionally misidentified as possible South American [[tyrannosaurid]]s.<ref name="ageofdinosaursabelisaurus">"Abelisaurus." In: Dodson, Peter & Britt, Brooks & Carpenter, Kenneth & Forster, Catherine A. & Gillette, David D. & Norell, Mark A. & Olshevsky, George & Parrish, J. Michael & Weishampel, David B. ''The Age of Dinosaurs''. Publications International, LTD. p. 105. ISBN 0-7853-0443-6.</ref>

==Description==
[[File:Carnotaurus reconstruction Headden.jpg|thumb|left|Skeletal diagram of the known material of ''[[Carnotaurus]]'']]
Abelisaurid hindlimbs were more typical of ceratosaurs, with the [[talus bone|astragalus]] and [[calcaneum]] (upper ankle bones) fused to each other and to the [[tibia]], forming a tibiotarsus. The tibia was shorter than the [[femur]], giving the hindlimb stocky proportions. There were three functional digits on the foot (the second, third, and fourth), while the first digit, or [[hallux]], did not contact the ground.<ref name="tykoskirowe2004"/>

===Skull===
[[File:Abelisaurus comahuensis.JPG|thumb|Skull of ''[[Abelisaurus]]''.]]
Although skull proportions varied, abelisaurid skulls were generally very tall and very short in length. In ''Carnotaurus'', for example, the skull was nearly as tall as it was long. The [[premaxilla]] in abelisaurids was very tall, so the front of the snout was blunt, not tapered as seen in many other theropods.<ref name=bonapartetal1990/>

Two skull bones, the [[lacrimal bone|lacrimal]] and [[postorbital]] bones, projected into the [[Orbit (anatomy)|eye socket]] from the front and back, nearly dividing it into two compartments. The eye would have been located in the upper compartment, which was tilted slightly outwards in ''Carnotaurus'', perhaps providing some degree of [[binocular vision]]. The lacrimal and postorbital also met above the eye socket, to form a ridge or brow above the eye.<ref name=bonapartetal1990/>

Sculpturing is seen on many of the skull bones, in the form of long grooves, pits and protrusions. Like other [[Ceratosauridae|ceratosaurs]], the [[frontal bone]]s of the skull roof were fused together. Carnotaurines commonly had bony projections from the skull. ''Carnotaurus'' had two pronounced horns, projecting outward above the eyes, while its close relative ''Aucasaurus'' had smaller projections in the same area. ''[[Majungasaurus]]'' and ''[[Rajasaurus]]'' had a single bony horn or dome, projecting upwards from the skull. These projections, like the horns of many modern animals, might have been [[display (zoology)|display]]ed for species recognition or intimidation.<ref name="tykoskirowe2004"/><ref name="bonaparteetal1990">{{cite journal | last1 = Bonaparte | first1 = J.F. | last2 = Novas | first2 = F.E. | last3 = Coria | first3 = R.A. | year = 1990 | title = ''Carnotaurus sastrei'' Bonaparte, the horned, lightly built carnosaur from the middle Cretaceous of Patagonia | url = | journal = Contributions to Science of the Natural History Museum of Los Angeles County | volume = 416 | issue = | pages = 1–42 }}</ref><ref name="wilsonetal2003">{{cite journal | last1 = Wilson | first1 = J.A. | last2 = Sereno | first2 = P.C. | last3 = Srivastava | first3 = S. | last4 = Bhatt | first4 = D.K. | last5 = Khosla | first5 = A. | last6 = Sahni | first6 = A. | year = 2003 | title = A new abelisaurid (Dinosauria, Theropoda) from the Lameta Formation (Cretaceous, Maastrichtian) of India | url = | journal = Contributions of the Museum of Palaeontology of the University of Michigan | volume = 31 | issue = | pages = 1–42 }}</ref> In ''[[Arcovenator]]'', the dorsal margin of the postorbital (and probably also the lacrimal) is thickened dorsolaterally, forming a strong and rugose bony brow ridge rising above the level of the skull roof.<ref name="tortosa2014"/> Maybe this rugose brow ridge supported a keratinous or scaly structure for displays.

===Forelimbs and hands===
Data for the abelisaurid forelimbs are known from ''[[Eoabelisaurus]]'' and the carnotaurines ''Aucasaurus'', ''Carnotaurus'' and ''Majungasaurus''. All had small forelimbs which seem to have been vestigial.<ref name=senter2010>{{cite journal | last1 = Senter | first1 = P. | year = 2010 | title = Vestigial skeletal structures in dinosaurs | url = | journal = Journal of Zoology | volume = 280 | issue = 1| pages = 60–71 | doi = 10.1111/j.1469-7998.2009.00640.x }}</ref> The bones of the forearm ([[Radius (bone)|radius]] and [[ulna]]) were extremely short, only 25% of the length of the upper arm ([[humerus]]) in ''Carnotaurus'' and 33% in ''Aucasaurus''. The entire arm was held straight, and the elbow joint was immobile.<ref name=senter2010/>

As is typical for ceratosaurs, the abelisaurid [[Manus (zoology)|hand]] had four basic digits. However, it is there that any similarity ends. No [[carpal|wrist]] bones existed, with the four palm bones ([[metacarpal]]s) attaching directly to the forearm. There were no [[phalanx bone|finger bones]] on the first or fourth digits, only one on the second digit and two on the third digit. These two external fingers were extremely short and immobile. Manual claws were very small in ''[[Eoabelisaurus]]'', and totally absent in carnotaurines.<ref name=senter2010/>

More primitive relatives such as ''[[Noasaurus]]'' and ''[[Ceratosaurus]]'' had longer, mobile arms with fingers and claws.<ref name=agnolin&chiarelli2009>{{cite journal | last1 = Agnolin | first1 = Federico L. | last2 = Chiarelli | first2 = Pablo | title = The position of the claws in Noasauridae (Dinosauria: Abelisauroidea) and its implications for abelisauroid manus evolution | journal = Paläontologische Zeitschrift | volume = 84 | issue = 2 | pages = 293 | year = 2009 | doi = 10.1007/s12542-009-0044-2 }}</ref> Paleobiologist Alexander O. Vargas have suggested a major reason for the evolution towards vestigial forelimbs in the group was because of a genetic defect; the loss of function in HOXA11 and HOXD11, two genes which regulate the forelimbs' development.<ref>[http://www.smithsonianmag.com/science-nature/need-a-hand-dont-ask-an-abelisaurid-88125248/ Need a Hand? Don't Ask an Abelisaurid]</ref>

==Distribution==
[[File:Abelisaurus comahuensis jmallon.jpg|thumb|Illustration of ''[[Abelisaurus]]'']]
Abelisauroids are typically regarded as a Cretaceous group, though the earliest abelisaurid remains are known from the Middle [[Jurassic]] of Argentina (classified as the species ''[[Eoabelisaurus mefi]]'') and possibly Madagascar (fragmentary remains of an unnamed species);<ref name=eoabelisaurus>{{cite journal |doi=10.1098/rspb.2012.0660 |title=A Middle Jurassic abelisaurid from Patagonia and the early diversification of theropod dinosaurs |journal=Proceedings of the Royal Society B: Biological Sciences |volume=279 |issue=1741 |pages=3170 |year=2012 |last1=Pol |first1=D. |last2=Rauhut |first2=O. W. M. }}</ref><ref name="maganucoetal2005">{{cite journal | last1 = Maganuco | first1 = S. | last2 = Cau | first2 = A. | last3 = Pasini | first3 = G. | year = 2005 | title = First description of theropod remains from the Middle Jurassic ([[Bathonian]]) of Madagascar | url = | journal = Atti della Società Italiana di Scienze Naturali e del Museo Civico di Storia Naturale in Milano | volume = 146 | issue = 2| pages = 165–202 }}</ref> possible abelisaurid remains (an isolated left tibia, right femur and right tibia) were also discovered in Late Jurassic Tendaguru Beds in [[Tanzania]].<ref>{{cite journal |last=Rauhut |first=Oliver W. M. |year=2011 |title=Theropod dinosaurs from the Late Jurassic of Tendaguru (Tanzania) |journal=Special Papers in Palaeontology |volume=86 |issue= |pages=195–239 |doi=10.1111/j.1475-4983.2011.01084.x |doi-broken-date=2017-04-01 }}</ref> Abelisaurid remains are mainly known in the southern continents, which once made up the [[supercontinent]] of [[Gondwana]]. When first described in 1985, only ''Carnotaurus'' and ''Abelisaurus'' were known, both from the [[Late Cretaceous]] of [[South America]]. Abelisaurids were then located in Late [[Cretaceous]] [[India]] (''Indosuchus'' and ''Rajasaurus'') and [[Madagascar]] (''Majungasaurus''), which were closely connected for much of the Cretaceous. It was thought that the absence of abelisaurids from continental [[Africa]] indicated that the group [[evolution|evolved]] after the separation of Africa from Gondwana, around 100 [[million years ago]].<ref name="sampsonetal1998">{{cite journal | doi = 10.1126/science.280.5366.1048 | last1 = Sampson | first1 = S.D. | last2 = Witmer | first2 = L.M. | last3 = Forster | first3 = C.A. | last4 = Krause | first4 = D.A. | last5 = O'Connor | first5 = P.M. | last6 = Dodson | first6 = P. | last7 = Ravoavy | first7 = F. | year = 1998 | title = Predatory dinosaur remains from Madagascar: implications for the Cretaceous biogeography of Gondwana | url = | journal = Science | volume = 280 | issue = 5366| pages = 1048–1051 | pmid = 9582112 | bibcode = 1998Sci...280.1048S }}</ref> However, the discovery of ''Rugops'' and other abelisaurid material from the middle of the Cretaceous in northern Africa disproved this hypothesis.<ref name="serenoetal2004">{{cite journal | doi = 10.1098/rspb.2004.2692 | last1 = Sereno | first1 = P.C. | last2 = Wilson | first2 = J.A. | last3 = Conrad | first3 = J.L. | year = 2004 | title = New dinosaurs link southern landmasses in the mid-Cretaceous | url = | journal = [[Proceedings of the Royal Society B]] | volume = 271 | issue = 1546| pages = 1325–1330 | pmid=15306329 | pmc=1691741}}</ref><ref name="mahler2005">{{cite journal | doi = 10.1671/0272-4634(2005)025[0236:ROADTF]2.0.CO;2 | last1 = Mahler | first1 = L. | year = 2005 | title = Record of Abelisauridae (Dinosauria: Theropoda) from the [[Cenomanian]] of [[Morocco]]". | url = | journal = Journal of Vertebrate Paleontology | volume = 25 | issue = 1| pages = 236–239 }}</ref> Mid-Cretaceous abelisaurids are now known from South America as well, showing that the group existed prior to the breakup of Gondwana.<ref name="coriasalgado1998"/><ref name="calvoetal2004"/><ref name="lamannaetal2002">{{cite journal | doi = 10.1671/0272-4634(2002)022[0058:ADATDF]2.0.CO;2 | last1 = Lamanna | first1 = M.C. | last2 = Martinez | first2 = R.D. | last3 = Smith | first3 = J.B. | year = 2002 | title = A definitive abelisaurid theropod dinosaur from the early Late Cretaceous of [[Patagonia]]". | url = | journal = Journal of Vertebrate Paleontology | volume = 22 | issue = 1| pages = 58–69 }}</ref> In 2014, the description of ''[[Arcovenator escotae]]'' from southern [[France]] provided the first indisputable evidence of the presence of Abelisaurids in [[Europe]]. ''Arcovenator'' presents strong similarities with the Madagascan ''Majungasaurus'' and Indian abelisaurids, but not with the South American forms. ''Arcovenator'', ''Majungasaurus'' and Indian forms are united in the new clade [[Majungasaurinae]].<ref name="tortosa2014"/>

==Classification==
[[File:Carnotaurus DB 2.jpg|thumb|left|Illustration of ''[[Carnotaurus]]'']]
[[Paleontologist]]s [[Jose Bonaparte]] and [[Fernando Novas]] coined the name Abelisauridae in 1985 when they described the eponymous ''Abelisaurus''. The name is formed from the family name of Roberto Abel, who discovered ''Abelisaurus'', as well as from the [[Ancient Greek|Greek]] word ''σαυρος''/''sauros'' meaning 'lizard'. The very common suffix ''-idae'' is usually applied to [[zoology|zoological]] [[family (biology)|family]] names and is derived from the Greek suffix ''-ιδαι''/''-idai'', which indicates a plural noun.<ref name="bonapartenovas1985">Bonaparte, J.F. & Novas, F.E. (1985). ["Abelisaurus comahuensis, n.g., n.sp., Carnosauria of the Late Cretaceous of Patagonia".] ''Ameghiniana''. 21: 259–265. [In Spanish]</ref>

Abelisauridae is a family in rank-based [[Linnaean taxonomy]], within the [[infraorder]] '''Ceratosauria''' and the superfamily '''[[Abelisauroidea]]''', which also contains the family '''[[Noasauridae]]'''. It has had several definitions in [[phylogenetic taxonomy]]. It was originally defined as a node-based [[taxon]] including ''Abelisaurus'', ''Carnotaurus'', their common ancestor and all of its descendants.<ref name="novas1997">Novas, F.E. (1997). "Abelisauridae". In: Currie, P.J. & Padian, K.P. ''Encyclopedia of Dinosaurs''. San Diego: Academic Press. Pp. 1–2 ISBN 0-12-226810-5.</ref><ref name="sereno1998">{{cite journal | last1 = Sereno | first1 = P.C. | year = 1998 | title = A rationale for phylogenetic definitions, with applications to the higher-level taxonomy of Dinosauria | url = | journal = Neues Jahrbuch fur Geologie und Palaontologie: Abhandlungen | volume = 210 | issue = | pages = 41–83 }}</ref>

Later it was redefined as a stem-based taxon, including all animals more closely related to ''Abelisaurus'' (or the more complete ''Carnotaurus'') than to ''Noasaurus''.<ref name="wilsonetal2003"/> The node-based definition would not include animals like ''[[Rugops]]'' or ''[[Ilokelesia]]'', which are thought to be more [[Basal (phylogenetics)|basal]] than ''Abelisaurus'' and would be included by a stem-based definition.<ref name="taxonsearch">Sereno, P.C. (2005). [http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=6 Abelisauridae]. TaxonSearch. 7 November 2005. Retrieved 19 September 2006.</ref> Within Abelisauridae is the subgroup '''Carnotaurinae''', and among carnotaurines, ''Aucasaurus'' and ''Carnotaurus'' are united in '''Carnotaurini'''.<ref name="serenoetal2004"/>

===Shared characteristics===
Complete skeletons have been described only for the most advanced abelisaurids (such as ''Carnotaurus'' and ''[[Aucasaurus]]''), making it difficult to establish [[synapomorphy|defining features]] of the skeleton for the family as a whole. However, most are known from at least some skull bones, so known shared features come mainly from the skull.<ref name="tykoskirowe2004">Tykoski, R.S. & Rowe, T. (2004). "Ceratosauria". In: Weishampel, D.B., Dodson, P., & Osmolska, H. (Eds.) ''The Dinosauria'' (2nd edition). Berkeley: University of California Press. Pp. 47–70 ISBN 0-520-24209-2</ref>

Many abelisaurid skull features are shared with [[carcharodontosaurid]]s. These shared features, along with the fact that abelisaurids seem to have replaced carcharodontosaurids in South America, have led to suggestions that the two groups were related.<ref name="novas1997"/> However, no [[cladistic]] analysis has ever found such a relationship and, aside from the skull, abelisaurids and carcharodontosaurids are very different, more similar to ceratosaurs and [[Allosauroidea|allosauroids]], respectively.<ref name="tykoskirowe2004"/>

===Phylogeny===
[[File:Majungasaurus crenatissimus skeleton.jpg|thumb|Known material of ''[[Majungasaurus]]'']]
Below is a cladogram generated by Tortosa ''et al.'' (2013) in the description of ''[[Arcovenator]]'' and creation of a new subfamily [[Majungasaurinae]].<ref name="tortosa2013">{{cite journal |doi=10.1016/j.annpal.2013.10.003 |title=A new abelisaurid dinosaur from the Late Cretaceous of southern France: Palaeobiogeographical implications |journal=Annales de Paléontologie |volume=100 |pages=63 |year=2014 |last1=Tortosa |first1=Thierry |last2=Buffetaut |first2=Eric |last3=Vialle |first3=Nicolas |last4=Dutour |first4=Yves |last5=Turini |first5=Eric |last6=Cheylan |first6=Gilles }}</ref>

{{clade| style=font-size:85%; line-height:85%
|label1=Abelisauridae
|1={{clade
        |1=''[[Kryptops]]''
        |2=''[[Rugops]]''
        |3=''[[Genusaurus ]]''
        |4=MCF-PVPH-237 abelisaurid
        |5=''[[Xenotarsosaurus]]''
        |6=''[[Tarascosaurus]]''
        |7=La Boucharde abelisaurid
        |label8=&nbsp;
        |8={{clade
             |label1=[[Majungasaurinae]]
             |1={{clade
                  |1=Pourcieux abelisaurid
                  |2=''[[Arcovenator]]''
                  |3=''[[Majungasaurus]]''
                  |4=''[[Indosaurus]]''
                  |5=''[[Rahiolisaurus]]''
                  |6=''[[Rajasaurus]]}}''
             |label2=[[Brachyrostra]]
             |2={{clade
                  |1=''[[Ilokelesia]]''
                  |label2=&nbsp;
                  |2={{clade
                       |1=''[[Ekrixinatosaurus]]''
                       |label2=&nbsp;
                       |2={{clade
                            |1=''[[Skorpiovenator]]''
                            |label2=&nbsp;
                            |2={{clade
                                 |label1=[[Carnotaurini]]
                                 |1={{clade
                                      |1=''[[Abelisaurus]]''
                                      |2=''[[Aucasaurus]]''
                                      |3=''[[Pycnonemosaurus]]''
                                      |4=''[[Quilmesaurus]]''
                                      |5=''[[Carnotaurus]]''}}}}}}}}}}}}}}}}

''Ilokelesia'' was originally described as a sister group to Abelisauroidea.<ref name="coriasalgado1998">Coria, R.A. & Salgado, L. "A basal Abelisauria Novas 1992 (Theropoda-
Ceratosauria) from the Cretaceous Period of Patagonia, Argentina". In: Perez-Moreno, B, Holtz, T.R., Sanz, J.L., & Moratalla, J. (Eds.). ''Aspects of Theropod Paleobiology''. ''Gaia'' 15:89–102. [not printed until 2000]</ref> However, Sereno tentatively places it closer to ''Abelisaurus'' than to noasaurids, a result which agrees with several other recent analyses.<ref name="tykoskirowe2004"/><ref name="calvoetal2004">{{cite journal | last1 = Calvo | first1 = J.O. | last2 = Rubilar-Rogers | first2 = D. | last3 = Moreno | first3 = K. | year = 2004 | title = A new Abelisauridae (Dinosauria: Theropoda) from northwest Patagonia | url = | journal = Ameghiniana | volume = 41 | issue = 4| pages = 555–563 }}</ref><ref name="coriaetal2002">{{cite journal | doi = 10.1671/0272-4634(2002)022[0460:ANCROC]2.0.CO;2 | last1 = Coria | first1 = R.A. | last2 = Chiappe | first2 = L.M. | last3 = Dingus | first3 = L. | year = 2002 | title = A close relative of ''Carnotaurus sastrei'' Bonaparte 1985 (Theropoda: Abelisauridae) from the Late Cretaceous of Patagonia | url = | journal = Journal of Vertebrate Palaeontology | volume = 22 | issue = 2| pages = 460–465 }}</ref> If a stem-based definition is used, ''Ilokelesia'' and ''Rugops'' are therefore basal abelisaurids. However, as they are more basal than ''Abelisaurus'', they are outside of Abelisauridae if the node-based definition is adopted. ''Ekrixinatosaurus'' was also published in 2004, so it was not included in Sereno's analysis. However, an independent analysis, performed by Jorge Calvo and colleagues, shows it to be an abelisaurid.<ref name="calvoetal2004"/>

Some scientists include ''[[Xenotarsosaurus]]'' from Argentina and ''[[Compsosuchus]]'' from India as basal abelisaurids,<ref name="novasetal2004">{{cite journal | last1 = Novas | first1 = F.E. | last2 = Agnolin | first2 = F.L. | last3 = Bandyopadhyay | first3 = S. | year = 2004 | title = Cretaceous theropods from India: a review of specimens described by Huene and Matley (1933)". | url = | journal = Revista del Museo Argentino del Ciencias Naturales | volume = 6 | issue = 1| pages = 67–103 }}</ref><ref name="Rauhut">{{cite journal | last1 = Rauhut | first1 = O.W.M. | year = 2003 | title = The interrelationships and evolution of basal theropod dinosaurs | url = | journal = Special Papers in Palaeontology | volume = 69 | issue = | pages = 1–213 }}</ref> while others consider them to be outside Abelisauroidea.<ref name="MartinezNovas">Martínez, R.D. and Novas, F.E. (2006). "''Aniksosaurus darwini gen. et sp. nov''., a new coelurosaurian theropod from the early Late Cretaceous of central Patagonia, Argentina". ''Revista del Museo Argentino de Ciencias Naturales'', nuevo serie 8(2):243-259</ref> The [[France|French]] ''[[Genusaurus]]'' and ''[[Tarascosaurus]]'' have also been called abelisaurids but both are fragmentary and may be more basal ceratosaurians.<ref name="tykoskirowe2004"/>
[[File:MEF Eoabelisaurus.jpg|thumb|Skeleton of ''[[Eoabelisaurus]]'', a close relative of Abelisauridae]]
With the description of ''[[Skorpiovenator]]'' in 2008, Canale ''et al.'' published another phylogenetic analysis focusing on the South American abelisaurids. In their results, they found that all South American forms, including ''[[Ilokelesia]]'' (except ''[[Abelisaurus]]''), grouped together as a sub-clade of carnotaurines, which they named [[Brachyrostra]].<ref name="canaleetal2008">{{cite journal | last1 = Canale | first1 = Juan I. | last2 = Scanferla | first2 = Carlos A. | last3 = Agnolin | first3 = Federico L. | last4 = Novas | first4 = Fernando E. | title = New carnivorous dinosaur from the Late Cretaceous of NW Patagonia and the evolution of abelisaurid theropods | journal = Naturwissenschaften | volume = 96 | issue = 3 | pages = 409–14 | year = 2008 | pmid = 19057888 | doi = 10.1007/s00114-008-0487-4 | bibcode = 2009NW.....96..409C }}</ref> In the same year Matthew T. Carrano and Scott D. Sampson published new large phylogenetic analysis of ceratosaurian.<ref name=C&S08>{{Cite journal | volume = 6 | year = 2007 | doi = 10.1017/S1477201907002246 | journal = Journal of Systematic Palaeontology | title = The Phylogeny of Ceratosauria (Dinosauria: Theropoda) | first1 = M. T. | last1 = Carrano | last2 = Sampson | first2 = S. D. | issue = 2| pages = 183 }}</ref> With the description of ''[[Eoabelisaurus]]'', Diego Pol and Oliver W. M. Rauhut (2012) combined these analyses and added ten new characters. The following [[cladogram]] follows their analysis.<ref name=Eoabelisaurus>{{cite journal | author = Diego Pol & Oliver W. M. Rauhut | year = 2012 | title = A Middle Jurassic abelisaurid from Patagonia and the early diversification of theropod dinosaurs | journal = Proceedings of the Royal Society B: Biological Sciences | volume = 279 | issue = 1804 | pages = 3170–5 | doi = 10.1098/rspb.2012.0660 | pmid=22628475 | pmc=3385738}}</ref>

{{clade| style=font-size:85%;line-height:85%
|label1=[[Ceratosauria]]&nbsp;
|1={{clade
   |1=''[[Berberosaurus]]''
   |2=''[[Deltadromeus]]''
   |3={{clade
      |1={{clade
         |1=''[[Spinostropheus]]''
         |2={{clade
            |1=''[[Limusaurus]]''
            |2=''[[Elaphrosaurus]]'' }} }}
      |label2=&nbsp;[[Neoceratosauria]]&nbsp;
      |2={{clade
         |label1=&nbsp;[[Ceratosauridae]]&nbsp;
         |1={{clade
            |1=''[[Ceratosaurus]]''
            |2=''[[Genyodectes]]'' }}
         |label2=&nbsp;[[Abelisauroidea]]&nbsp;
         |2={{clade
            |label1=&nbsp;[[Noasauridae]]&nbsp;
            |1={{clade
               |1=''[[Laevisuchus]]''
               |2=''[[Masiakasaurus]]''
               |3=''[[Noasaurus]]''
               |4=''[[Velocisaurus]]'' }}
            |label2=&nbsp;'''Abelisauridae'''&nbsp;
            |2={{clade
               |1=''[[Eoabelisaurus]]''
               |2={{clade
                  |1=''[[Rugops]]''
                  |2={{clade
                     |1=''[[Abelisaurus]]''
                     |label2=&nbsp;[[Carnotaurinae]]&nbsp;
                     |2={{clade
                        |1={{clade
                           |1=''[[Majungasaurus]]''
                           |2=''[[Indosaurus]]''
                           |3=''[[Rajasaurus]]'' }}
                        |label2=&nbsp;[[Brachyrostra]]&nbsp;
                        |2={{clade
                           |1={{clade 
                              |1=''[[Ilokelesia]]''
                              |2=''[[Ekrixinatosaurus]]''
                              |3=''[[Skorpiovenator]]'' }}
                           |label2=&nbsp;[[Carnotaurini]]&nbsp; 
                           |2={{clade
                              |1=''[[Carnotaurus]]''
                              |2=''[[Aucasaurus]]'' }} }} }} }} }} }} }} }} }} }} }}

{| class="wikitable collapsible collapsed"
! Timeline of descriptions of genera
|-
|
<timeline>
ImageSize  = width:1000px height:auto barincrement:15px
PlotArea   = left:10px bottom:50px top:10px right:10px

Period      = from:1930 till:2100
TimeAxis    = orientation:horizontal
ScaleMajor  = unit:year increment:50 start:1930
ScaleMinor  = unit:year increment:10 start:1930
TimeAxis    = orientation:hor
AlignBars   = justify

Colors =
 #legends
  id:CAR	  value:claret
  id:ANK 	 value:rgb(0.4,0.3,0.196)
  id:HER	  value:teal
  id:HAD	  value:green
  id:OMN	  value:blue
  id:black        value:black
  id:white        value:white
  id:1900s   value:rgb(0.94,0.25,0.24)
  id:2000s     value:rgb(0.2,0.7,0.79)
  id:2000syears     value:rgb(0.52,0.81,0.91) 
  id:1900syears   value:rgb(0.95,0.56,0.45)
  id:1700s   value:rgb(0.5,0.78,0.31)
  id:1700syears   value:rgb(0.63,0.78,0.65)
  id:latecretaceous   value:rgb(0.74,0.82,0.37)
   id:1800syears     value:rgb(0.95,0.98,0.11)
  id:paleogene     value:rgb(0.99,0.6,0.32) 
  id:paleocene     value:rgb(0.99,0.65,0.37) 
  id:eocene     value:rgb(0.99,0.71,0.42) 
  id:oligocene     value:rgb(0.99,0.75,0.48) 
  id:1800s     value:rgb(0.999999,0.9,0.1) 
  id:miocene     value:rgb(0.999999,0.999999,0) 
  id:pliocene     value:rgb(0.97,0.98,0.68)  
  id:quaternary   value:rgb(0.98,0.98,0.5)
  id:pleistocene   value:rgb(0.999999,0.95,0.68)
  id:holocene   value:rgb(0.999,0.95,0.88)

BarData=
 bar:eratop
 bar:space
 bar:periodtop
 bar:space
 bar:NAM1
 bar:NAM2
 bar:NAM3
 bar:NAM4
 bar:NAM5
 bar:NAM6
 bar:NAM7
 bar:NAM8
 bar:NAM9
 bar:NAM10
 bar:space
 bar:period
 bar:space
 bar:era

PlotData=
 align:center textcolor:black fontsize:M mark:(line,black) width:25 
 shift:(7,-4)
  
 bar:periodtop
 from: 1930    till: 1940    color:1900syears    text:[[1930s in paleontology|30s]]
 from: 1940    till: 1950    color:1900syears    text:[[1940s in paleontology|40s]]
 from: 1950    till: 1960    color:1900syears    text:[[1950s in paleontology|50s]]
 from: 1960    till: 1970    color:1900syears    text:[[1960s in paleontology|60s]]
 from: 1970    till: 1980    color:1900syears    text:[[1970s in paleontology|70s]]
 from: 1980    till: 1990    color:1900syears    text:[[1980s in paleontology|80s]]
 from: 1990    till: 2000    color:1900syears    text:[[1990s in paleontology|90s]]
 from: 2000    till: 2010    color:2000syears    text:[[2000s in paleontology|00s]]
 from: 2010    till: 2020    color:2000syears    text:[[2010s in paleontology|10s]]
 from: 2020    till: 2030    color:2000syears    text:[[2020s in paleontology|20s]]
 from: 2030    till: 2040    color:2000syears    text:[[2030s in paleontology|30s]]
 from: 2040    till: 2050    color:2000syears    text:[[2040s in paleontology|40s]]
 from: 2050    till: 2060    color:2000syears    text:[[2050s in paleontology|50s]]
 from: 2060    till: 2070    color:2000syears    text:[[2060s in paleontology|60s]]
 from: 2070    till: 2080    color:2000syears    text:[[2070s in paleontology|70s]]
 from: 2080    till: 2090    color:2000syears    text:[[2080s in paleontology|80s]]
 from: 2090    till: 2100    color:2000syears    text:[[2090s in paleontology|90s]]

 bar:eratop         
 from: 1930    till: 2000    color:1900s    text:[[20th century in paleontology|20th]]
 from: 2000    till: 2100    color:2000s    text:[[21st century in paleontology|21st]]

PlotData=
 align:left fontsize:M mark:(line,white) width:5 anchor:till align:left

 color:1900s bar:NAM1 at:1933 mark:(line,black) text:[[Indosaurus ]]
 color:1900s bar:NAM2 at:1933 mark:(line,black) text:[[Indosuchus ]]
 color:1900s bar:NAM1 at:1955 mark:(line,black) text:[[Majungasaurus ]]
 color:1900s bar:NAM1 at:1985 mark:(line,black) text:[[Abelisaurus ]]
 color:1800s bar:NAM2 at:1985 mark:(line,black) text:[[Carnotaurus ]]
 color:1900s bar:NAM3 at:1986 mark:(line,black) text:[[Xenotarsosaurus ]]
 color:1900s bar:NAM4 at:1991 mark:(line,black) text:[[Tarascosaurus ]]
 color:1900s bar:NAM5 at:1998 mark:(line,black) text:[[Ilokelesia ]]
 color:1900s bar:NAM1 at:2001 mark:(line,black) text:[[Quilmesaurus ]]
 color:1900s bar:NAM2 at:2002 mark:(line,black) text:[[Pycnonemosaurus ]]
 color:1900s bar:NAM6 at:2002 mark:(line,black) text:[[Aucasaurus ]]
 color:1800s bar:NAM7 at:2003 mark:(line,black) text:[[Rajasaurus ]]
 color:1900s bar:NAM8 at:2004 mark:(line,black) text:[[Rugops ]]
 color:1900s bar:NAM9 at:2004 mark:(line,black) text:[[Ekrixinatosaurus ]]
 color:1900s bar:NAM3 at:2008 mark:(line,black) text:[[Kryptops ]]
 color:1800s bar:NAM10 at:2009 mark:(line,black) text:[[Skorpiovenator ]]
 color:1900s bar:NAM4 at:2010 mark:(line,black) text:[[Rahiolisaurus ]]
 color:1900s bar:NAM5 at:2013 mark:(line,black) text:[[Arcovenator]]

PlotData=
 align:center textcolor:black fontsize:M mark:(line,black) width:25

 bar:period
 from: 1930    till: 1940    color:1900syears    text:[[1930s in paleontology|30s]]
 from: 1940    till: 1950    color:1900syears    text:[[1940s in paleontology|40s]]
 from: 1950    till: 1960    color:1900syears    text:[[1950s in paleontology|50s]]
 from: 1960    till: 1970    color:1900syears    text:[[1960s in paleontology|60s]]
 from: 1970    till: 1980    color:1900syears    text:[[1970s in paleontology|70s]]
 from: 1980    till: 1990    color:1900syears    text:[[1980s in paleontology|80s]]
 from: 1990    till: 2000    color:1900syears    text:[[1990s in paleontology|90s]]
 from: 2000    till: 2010    color:2000syears    text:[[2000s in paleontology|00s]]
 from: 2010    till: 2020    color:2000syears    text:[[2010s in paleontology|10s]]
 from: 2020    till: 2030    color:2000syears    text:[[2020s in paleontology|20s]]
 from: 2030    till: 2040    color:2000syears    text:[[2030s in paleontology|30s]]
 from: 2040    till: 2050    color:2000syears    text:[[2040s in paleontology|40s]]
 from: 2050    till: 2060    color:2000syears    text:[[2050s in paleontology|50s]]
 from: 2060    till: 2070    color:2000syears    text:[[2060s in paleontology|60s]]
 from: 2070    till: 2080    color:2000syears    text:[[2070s in paleontology|70s]]
 from: 2080    till: 2090    color:2000syears    text:[[2080s in paleontology|80s]]
 from: 2090    till: 2100    color:2000syears    text:[[2090s in paleontology|90s]]

 bar:era         
 from: 1930    till: 2000    color:1900s    text:[[20th century in paleontology|20th]]
 from: 2000    till: 2100    color:2000s    text:[[21st century in paleontology|21st]]
</timeline>
|-
|}

==Paleobiology==

===Ontogeny and growth===
Studies of the abelisaurid ''Majungasaurus'' indicate that it was a much slower growing dinosaur than other theropods, taking nearly twenty years to reach adult size. Similar studies on other abelisaurid genera indicate that this slow maturation may have been a trait that was common to the whole of Abelisauridae.<ref>http://www.livescience.com/56897-slow-growing-theropod-dinosaur.html</ref>

==See also==
{{Portal|Dinosaurs}}
* [[Timeline of ceratosaur research]]

==References==
{{Reflist|3}}

==External links==
*[http://www.thescelosaurus.com/abelisauridae.htm Thescelosaurus! on Abelisauridae]
*[http://archosaur.us/theropoddatabase/Ceratosauria.htm#Abelisauridae Abelisauridae at The Theropod Database]

{{Ceratosauria}}

[[Category:Abelisaurids| *]]