{{other uses}}
{{Jmyth infobox}}
[[File:Prince Hanzoku terrorised by a nine- tailed fox.jpg|right|thumb|Prince Hanzoku terrorized by a nine-tailed fox. Print by [[Utagawa Kuniyoshi]], 19th century.]]

{{nihongo|'''Kitsune'''|[[wiktionary:狐#Japanese|狐]], キツネ|extra={{IPA-ja|kitsu͍ne|IPA|Ja-Kitsune.oga}}}} is the [[Japanese language|Japanese word]] for [[Red fox|fox]]. Foxes are a common subject of [[Japanese folklore]]; in English, ''kitsune'' refers to them in this context. Stories depict them as intelligent beings and as possessing [[magic (paranormal)|magical]] abilities that increase with their age and wisdom. According to [[Yōkai]] folklore, all foxes have the ability to [[human disguise|shape shift into human form]].<ref name=casal>{{cite book|last1=Casal|first1=U.A.|title=The Goblin Fox and Badger and Other Witch Animals of Japan|publisher=Nanzan University Press|pages=1–93}}</ref> While some folktales speak of kitsune employing this ability to trick others—as foxes in folklore often do—other stories portray them as faithful guardians, friends, lovers, and wives.

Foxes and human beings lived close together in [[History of Japan#Ancient and Classical Japan|ancient Japan]]; this companionship gave rise to legends about the creatures. Kitsune have become closely associated with [[Inari Ōkami|Inari]], a [[Shinto]] ''[[kami]]'' or spirit, and serve as its messengers. This role has reinforced the fox's [[supernatural]] significance. The more tails a kitsune has—they may have as many as nine—the older, wiser, and more powerful it is. Because of their potential power and influence, some people make [[sacrifice|offerings]] to them as to a deity.

Conversely foxes were often seen as "witch animals", especially during the superstitious Edo period (1603–1867), and were goblins who could not be trusted (similar to some badgers and cats).<ref name=casal/>

==Origins==
[[File:NineTailsFox.JPG|right|thumb|A nine-tailed fox, from the Qing edition of the ancient text ''[[Classic of Mountains and Seas]]'']]
[[File:Vulpes vulpes laying in snow.jpg|thumb|left|Japan is home to two [[red fox]] [[subspecies]]: the Hokkaido fox (''Vulpes vulpes schrencki'', pictured), and the Japanese red fox (''Vulpes vulpes japonica''), neither of which has nine tails.]]
Japanese fox myths had its origins in [[Chinese mythology]].<ref>{{cite journal|author=Gubler, Greg|title=Kitsune: The Remarkable Japanese Fox|journal=Southern Folklore Quarterly|volume=38|issue=2|pages=121–134}}</ref><ref>{{cite book|last1=Bargen|first1=Doris G.|title=A woman's weapon: spirit possession in the Tale of Genji|date=1997|publisher=University of Hawaii Press|location=Honolulu|isbn=9780824818586|page=292}}</ref> Chinese folk tales tell of fox spirits called ''[[Huli jing|húli jīng]]'' ({{zh|c=狐狸精}}) that may have up to nine tails (''[[Fox spirit|Kyūbi no Kitsune]]'' in Japanese). Many of the earliest surviving stories are recorded in the ''[[Konjaku Monogatarishū]]'', an 11th-century collection of [[Chinese literature|Chinese]], [[Indian literature|Indian]], and Japanese narratives.<ref>{{cite journal|url=http://faculty.humanities.uci.edu/sbklein/Ghosts/articles/goff-foxes.pdf|author=Goff, Janet|title=Foxes in Japanese culture: beautiful or beastly?|journal=Japan Quarterly|volume= 44|issue=2|date=April 1997}}</ref> The nine-tailed foxes came to be adapted as a motif from Chinese mythology to Japanese mythology.<ref>{{cite book|last1=Wallen|first1=Martin|title=Fox|date=2006|publisher=[[Reaktion Books]]|location=London|isbn=9781861892973|pages=69–70}}</ref>

Smyers (1999) notes that the idea of the fox as seductress and the connection of the fox myths to [[Buddhism]] were introduced into Japanese folklore through similar Chinese stories, but she maintains that some fox stories contain elements unique to Japan.<ref name="Smyers 1999, pp.127–128">[[#Smyers|Smyers]]. 127–128</ref>

===Etymology===
The full etymology is unknown. The oldest known usage of the word is in the 794 text ''[[Shin'yaku Kegonkyō Ongi Shiki]]''. Other old sources include [[Nihon Ryōiki]] (810–824) and [[Wamyō Ruijushō]] (c. 934). These oldest sources are written in [[Man'yōgana]] which clearly identifies the [[Jōdai Tokushu Kanazukai|historical spelling]] as ''ki<sub>1</sub>tune''. Following several diachronic phonological changes, this becomes ''kitsune''.

Many etymological suggestions have been made, though there is no general agreement:
*Myōgoki (1268) suggests that it is so called because it is "always (''tsune'') yellow (''ki'')".
*Early [[Kamakura period]] [[Mizukagami]] indicates that it means "came (''ki'') [ [[perfective aspect]] particle ''tsu''] to bedroom (''ne'')" due to a legend that a kitsune would change into one's wife and bear children.
*[[Arai Hakuseki]] in Tōga (1717) suggests that ''ki'' means "stench", ''tsu'' is a possessive particle, and ''ne'' is related to ''inu'', the word for "dog".
*Tanikawa Kotosuga in Wakun no Shiori (1777–1887) suggests that ''ki'' means "yellow", ''tsu'' is a possessive particle, and ''ne'' is related to ''neko'', the word for cat.
*[[Ōtsuki Fumihiko]] in Daigenkai (1932–1935) proposes that the word comes from ''kitsu'', which is [[onomatopoeia]] for the bark of a fox, and ''ne'', which may be an affix or an honorific word meaning a servant of an [[Inari shrine]].
* Nozaki also suggests that the word ''kitsune'' was originally [[onomatopoeia|onomatopoetic]]. ''Kitsu'' represented a fox's yelp and came to be the general word for ''fox''. ''-Ne'' signifies an affectionate mood.<ref name="Nozaki3">[[#Nozaki|Nozaki]]. 3</ref>

''Kitsu'' is now archaic; in modern Japanese, a fox's cry is transcribed as ''kon kon'' or ''gon gon''.

One of the oldest surviving kitsune tales provides a widely known [[folk etymology]] of the word ''kitsune''.<ref name="Hamel 2003, p.89">[[#Hamel|Hamel]]. 89</ref> Unlike most tales of kitsune who become human and marry human males, this one does not end tragically:<ref>Goff.  "Foxes".  ''Japan Quarterly'' 44:2</ref><ref name="Smyers 1999, p.72">[[#Smyers|Smyers]]. 72</ref>

::Ono, an inhabitant of Mino (says an ancient Japanese legend of A.D. 545), spent the seasons longing for his ideal of female beauty. He met her one evening on a vast moor and married her.  Simultaneously with the birth of their son, Ono's dog was delivered of a pup which as it grew up became more and more hostile to the lady of the moors. She begged her husband to kill it, but he refused.  At last one day the dog attacked her so furiously that she lost courage, resumed vulpine shape, leaped over a fence and fled.

::"You may be a fox," Ono called after her, "but you are the mother of my son and I love you.  Come back when you please; you will always be welcome."

::So every evening she stole back and slept in his arms.<ref name="Hamel 2003, p.89"/>

Because the fox returns to her husband each night as a woman but leaves each morning as a fox, she is called Kitsune. In classical Japanese, ''kitsu-ne'' means ''come and sleep'', and ''ki-tsune'' means ''always comes''.<ref name="Smyers 1999, p.72"/>

==Characteristics==
[[File:Fox0290.jpg|thumb|right|Statue of a kitsune at the [[Inari shrine]] adjacent to the [[Tōdai-ji]] in [[Nara, Nara|Nara]]]]
Kitsune are believed to possess superior intelligence, long life, and [[magic (paranormal)|magical]] powers. They are a type of ''[[yōkai]]'', or spiritual entity, and the word ''kitsune'' is often translated as ''[[fox spirit]]''. However, this does not mean that kitsune are [[ghost]]s, nor that they are fundamentally different from regular foxes. Because the word ''spirit'' is used to reflect a state of knowledge or enlightenment, all long-lived foxes gain supernatural abilities.<ref name="Smyers 1999, pp.127–128"/>

There are two common classifications of kitsune:

* The {{nihongo|''zenko''|善狐|extra=literally ''good foxes''}} are benevolent, celestial foxes associated with [[Inari Ōkami|Inari]]; they are sometimes simply called ''Inari foxes''
* On the other hand, the {{nihongo|''[[yako (fox)|yako]]''|野狐|extra=literally ''field foxes'', also called ''nogitsune''}} tend to be mischievous or even malicious.<ref>''Yōkai no hon'' written by Prof. Abe Masaji & Prof. Ishikawa Junichiro</ref>

Local traditions add further types.<ref name="Hearn154">[[#Hearn|Hearn]]. 154</ref> For example, a ''ninko'' is an invisible fox spirit that human beings can only perceive when it [[spiritual possession|possesses]] them.

[[File:Obake Karuta 3-01.jpg|left|thumb|This ''[[karuta|obake karuta]]'' (monster card) from the early 19th century depicts a kitsune. The associated game involves matching clues from folklore to pictures of specific creatures.]]
Physically, kitsune are noted for having as many as nine tails.<ref name="Smyers 1999, p.129">[[#Smyers|Smyers]]. 129</ref> Generally, a greater number of tails indicates an older and more powerful fox; in fact, some folktales say that a fox will only grow additional tails after it has lived 100 years.<ref name="Hamel 2003, p.91"/> One, five, seven, and nine tails are the most common numbers in folk stories.<ref>{{cite web |url=http://academia.issendai.com/fox-misconceptions.shtml#tails |title=Kitsune, Kumiho, Huli Jing, Fox |accessdate=2006-12-14 |date=2003-04-28}}</ref> When a kitsune gains its ninth tail, its fur becomes white or gold.<ref name="Smyers 1999, p.129"/> These {{Nihongo|''[[Fox spirit|kyūbi no kitsune]]''|九尾の狐||nine-tailed foxes}} gain the abilities to see and hear anything happening anywhere in the world. Other tales credit them with infinite wisdom ([[omniscience]]).<ref>[[#Hearn|Hearn]]. 159</ref> After reaching 1,000 years of age and gaining its ninth tail, a kitsune turns a golden color, becoming a {{nihongo|'[[Tenko (fox)|Tenko]]'|[[Wiktionary:天狐|天狐]]|"heavenly fox"/"celestial fox"}}, the most powerful form of the kitsune, and then ascends to the heavens.

===Shapeshifting===
[[File:Bertha Boynton Lum, Fox women, 1908.jpg|thumb|"Fox women" by [[Bertha Lum]]: kitsune as women]]
A kitsune may [[shapeshifting|take on human form]], an ability learned when it reaches a certain age—usually 100 years, although some tales say 50.<ref name="Hamel 2003, p.91">[[#Hamel|Hamel]]. 91</ref> As a common prerequisite for the transformation, the fox must place reeds, a broad leaf, or a skull over its head.<ref name="Nozaki2526">[[#Nozaki|Nozaki]]. 25–26</ref> Common forms assumed by kitsune include beautiful women, young girls, or elderly men. These shapes are not limited by the fox's age or gender,<ref name="Smyers 1999, pp.127–128"/> and a kitsune can [[human disguise|duplicate the appearance of a specific person]].<ref>[[#Hall|Hall]]. 145</ref> Foxes are particularly renowned for impersonating beautiful women. Common belief in [[History of Japan#Feudal Japan (1185-1603)|medieval Japan]] was that any woman encountered alone, especially at dusk or night, could be a fox.<ref name="Tyler xlix">[[#Tyler|Tyler]]. xlix.</ref> ''Kitsune-gao'' or ''fox-faced'' refers to human females who have a narrow face with close-set eyes, thin eyebrows, and high cheekbones. Traditionally, this facial structure is considered attractive, and some tales ascribe it to foxes in human form.<ref>[[#Nozaki|Nozaki]]. 95, 206</ref> Variants on the theme have the kitsune retain other foxlike traits, such as a coating of fine hair, a fox-shaped shadow, or a reflection that shows its true form.<ref name="Hearn155">[[#Hearn|Hearn]]. 155</ref>

In some stories, kitsune have difficulty hiding their tails when they take human form; looking for the tail, perhaps when the fox gets drunk or careless, is a common method of discerning the creature's true nature.<ref name="Ashkenazy148">[[#Ashkenazy|Ashkenazy]]. 148</ref> A particularly devout individual may in some cases even be able to see through a fox's disguise merely by perceiving them.<ref>Heine, Steven. ''Shifting Shape, Shaping Text:  Philosophy and Folklore in the Fox Koan''. Honolulu: University of Hawai'i Press, 1999. ISBN 0-8248-2150-5.
153</ref> Kitsune may also be exposed while in human form by their fear and hatred of dogs, and some become so rattled by their presence that they revert to the form of a fox and flee.

One folk story illustrating these imperfections in the kitsune's human shape concerns [[Koan (disambiguation)|Koan]], a historical person credited with wisdom and magical powers of [[divination]]. According to the story, he was staying at the home of one of his devotees when he scalded his foot entering a bath because the water had been drawn too hot. Then, "in his pain, he ran out of the bathroom naked. When the people of the household saw him, they were astonished to see that Koan had fur covering much of his body, along with a fox's tail. Then Koan transformed in front of them, becoming an elderly fox and running away."<ref>[[#Hall|Hall]]. 144</ref>

Other supernatural abilities commonly attributed to the kitsune include possession, mouths or tails that generate fire or lightning (known as [[kitsunebi]]), willful manifestation in the dreams of others, flight, invisibility, and the creation of [[illusion]]s so elaborate as to be almost indistinguishable from reality.<ref name="Nozaki2526"/><ref name="Hearn155"/> Some tales speak of kitsune with even greater powers, able to bend time and space, drive people mad, or take fantastic shapes such as a tree of incredible height or a second moon in the sky.<ref>[[#Hearn|Hearn]]. 156–157</ref><ref>[[#Nozaki|Nozaki]]. 36–37</ref> Other kitsune have characteristics reminiscent of [[vampire]]s or [[succubus|succubi]] and feed on the life or spirit of human beings, generally through sexual contact.<ref>[[#Nozaki|Nozaki]]. 26, 221</ref>

[[File:Blacksmith Munechika, helped by a fox spirit, forging the blade Ko-Gitsune Maru, by Ogata Gekkō.jpg|left|thumb|[[Inari Ōkami]] and its fox spirits help the blacksmith [[Munechika]] forge the blade ''kogitsune-maru'' (Little Fox) at the end of the 10th century. The legend is the subject of the [[noh]] drama ''[[Sanjō Kokaji]]''.]]

===''Kitsunetsuki''===
[[File:Gyokuzan Kitsunetsuki.jpg|right|thumb|240px|A depiction of a kitsunetsuki in the ''Gyokuzan Gafu'' by [[Okada Gyokuzan]]]]
{{nihongo|''Kitsunetsuki''|狐憑き, 狐付き|lead=yes}}, also written ''kitsune-tsuki'', literally means "the state of being possessed by a fox". The victim is usually a young woman, whom the fox enters beneath her fingernails or through her breasts.<ref>[[#Nozaki|Nozaki]]. 59</ref> In some cases, the victims' facial expressions are said to change in such a way that they resemble those of a fox. Japanese tradition holds that fox possession can cause illiterate victims to temporarily gain the ability to read.<ref>[[#Nozaki|Nozaki]]. 216</ref> Though foxes in folklore can possess a person of their own will, kitsunetsuki is often attributed to the malign intents of hereditary [[Witchcraft#Japan|fox employers]], or ''tsukimono-suji''.<ref>Blacker, Carmen (1999) [http://web.archive.org/web/20090304194621/https://eee.uci.edu/clients/sbklein/GHOSTS/articles/CatalpaBow-WitchAnimals.pdf ''The Catalpa Bow: A Study of Shamanistic Practices in Japan'']. Routledge. ISBN 1873410859</ref>

Folklorist [[Lafcadio Hearn]] describes the condition:
{{quote|Strange is the madness of those into whom demon foxes enter.  Sometimes they run naked shouting through the streets. Sometimes they lie down and froth at the mouth, and yelp as a fox yelps.  And on some part of the body of the possessed a moving lump appears under the skin, which seems to have a life of its own.  Prick it with a needle, and it glides instantly to another place.  By no grasp can it be so tightly compressed by a strong hand that it will not slip from under the fingers.  Possessed folk are also said to speak and write languages of which they were totally ignorant prior to possession.  They eat only what foxes are believed to like&nbsp;— [[tofu]], ''[[aburaage|aburagé]]'', ''[[adzuki bean|azuki]]meshi'', etc.&nbsp;— and they eat a great deal, alleging that not they, but the possessing foxes, are hungry.|''Glimpses of Unfamiliar Japan'', vol. 1<ref name="Hearn158">[[#Hearn|Hearn]]. 158</ref>}}

He goes on to note that, once freed from the possession, the victim will never again be able to eat tofu, ''azukimeshi'', or other foods favored by foxes:
{{quote|[[Exorcism]], often performed at an [[Inari shrine]], may induce a fox to leave its host.<ref name="Smyers 1999, p.90">[[#Smyers|Smyers]]. 90</ref> In the past, when such gentle measures failed or a priest was not available, victims of ''kitsunetsuki'' were beaten or badly burned in hopes of forcing the fox to leave. Entire families were ostracized by their communities after a member of the family was thought to be possessed.<ref name="Hearn158"/>}}

In Japan, kitsunetsuki was noted as a disease as early as the [[Heian period]] and remained a common diagnosis for [[mental illness]] until the early 20th century.<ref>[[#Nozaki|Nozaki]]. 211</ref><ref>[[#Hearn|Hearn]]. 165</ref> Possession was the explanation for the abnormal behavior displayed by the afflicted individuals. In the late 19th century, Dr. Shunichi Shimamura noted that physical diseases that caused fever were often considered kitsunetsuki.<ref>[[#Nozaki|Nozaki]]. 214–215</ref> The belief has lost favor, but stories of fox possession still occur, such as allegations that members of the [[Aum Shinrikyo]] cult had been possessed.<ref>{{cite journal|author=Miyake-Downey, Jean |title=Ten Thousand Things |url=http://www.kyotojournal.org/10,000things/039.html |journal=Kyoto Journal |deadurl=yes |archiveurl=https://web.archive.org/web/20080406161331/http://www.kyotojournal.org/10,000things/039.html |archivedate=April 6, 2008 }}</ref>

In medicine, kitsunetsuki is a [[culture-bound syndrome]] unique to [[Culture of Japan|Japanese culture]]. Those who suffer from the condition believe they are possessed by a fox.<ref>Haviland, William A. (2002) ''Cultural Anthropology'', 10th ed. New York: Wadsworth Publishing Co. ISBN 0155085506. 144–145</ref> Symptoms include cravings for rice or sweet adzuki beans, listlessness, restlessness, and aversion to eye contact. Kitsunetsuki is similar to but distinct from [[clinical lycanthropy]].<ref>{{cite journal|author=Yonebayashi, T. |title=Kitsunetsuki (Possession by Foxes)|journal=Transcultural Psychiatry|volume= 1|issue=2|year=1964|pages=95–97|doi=10.1177/136346156400100206}}</ref>

====Folk beliefs====
In [[folk religion]], stories of fox possession can be found in all lands of Japan. Those possessed by a fox are thought to suffer from a [[mental illness]]-like condition.<ref name="民間信仰">{{Cite book|author=[[宮本袈裟雄]]他|editor=[[桜井徳太郎]]編|title=民間信仰辞典|year=1980|publisher=[[東京堂出版]]|isbn=978-4-490-10137-9|pages=97-98}}</ref>

There are not merely individuals, but also families that tell of protective spirits of foxes, and in certain regions, possession by a [[kuda-gitsune]],<ref name="民俗学辞典">{{Cite book |editor=民俗学研究所編 |title=民俗学辞典 |year=1951 |publisher=[[東京堂]] |id= ncid BN01703544 |pages=137-138}}</ref> [[osaki]],<ref name="民俗学辞典" /><ref name="昔話事典">{{Cite book|author=佐藤米司他 |editor=[[稲田浩二]]他編 |title= 日本昔話事典 |year=1977 |publisher= [[弘文堂]]|isbn=978-4-335-95002-5|pages=250-251}}</ref> [[yako (fox)|yako]],<ref name="民間信仰" /> and [[hitogitsune]] are also called "kitsunetsuki."<ref name="民間信仰" /><ref name="昔話事典" /> These families are said to have been able to use their fox to gain fortune, but marriage into such a family was considered forbidden as it would enlarge the family.<ref name="民間信仰" /> They are also said to be able to bring about illness and curse the possessions, crops, and livestock of ones that they hate, and as a result of being considered taboo by the other families, it has led to societal problems.<ref name="昔話事典" />

Other than this, there is another kind of kitsunetsuki called "inari-sage" where [[shugendō]] practitioners and miko, seeing foxes to be a kind of servant of the gods, would employ them when they engage in [[Adhiṣṭhāna#Honzon Kaji|practices]] and [[oracle]]s.<ref name="民間信仰" /><ref name="民俗学辞典" />

The great amount of faith given to foxes can be seen in how, as a result of the Inari belief where foxes were believed to be [[Inari Ōkami|Inari no Kami]] or its servant, they were employed in practices of [[Dakini#In_Japanese_Buddhism|dakini-ten]] by [[mikkyō]] practitioners and shugendō practitioners and in the oracles of miko and other practitioners, and the customs related to kitsunetsuki can be seen as having developed in such a religious background.<ref name="民間信仰" />

===''Hoshi no tama''===
[[File:Hiroshige-100-views-of-edo-fox-fires.jpg|right|thumb|"[[Kitsunebi]] on New Year's Night under the Enoki Tree near Ōji" in the [[One Hundred Famous Views of Edo]] by Hiroshige. Each fox has a kitsunebi floating close to their face.]]
Depictions of kitsune or their possessed victims may feature round or onion-shaped white balls known as {{Nihongo|''hoshi no tama''|ほしのたま||star balls}}. Tales describe these as glowing with [[kitsunebi]].<ref>[[#Nozaki|Nozaki]]. 183</ref> Some stories identify them as magical jewels or pearls.<ref>[[#Nozaki|Nozaki]]. 169–170</ref> When not in human form or possessing a human, a kitsune keeps the ball in its mouth or carries it on its tail.<ref name="Hamel 2003, p.91"/> Jewels are a common symbol of Inari, and representations of sacred Inari foxes without them are rare.<ref name="Smyers 1999, pp.112–114">[[#Smyers|Smyers]]. 112–114</ref>

One belief is that when a ''kitsune'' changes shape, its ''hoshi no tama'' holds a portion of its magical power. Another tradition is that the pearl represents the kitsune's soul; the kitsune will die if separated from it for long. Those who obtain the ball may be able to extract a promise from the ''kitsune'' to help them in exchange for its return.<ref>[[#Hall|Hall]]. 149</ref> For example, a 12th-century tale describes a man using a fox's ''hoshi no tama'' to secure a favor:

<blockquote>"Confound you!" snapped the fox. "Give me back my ball!" The man ignored its pleas till finally it said tearfully, "All right, you've got the ball, but you don't know how to keep it. It won't be any good to you. For me, it's a terrible loss. I tell you, if you don't give it back, I'll be your enemy forever. If you ''do'' give it back though, I'll stick to you like a protector god."</blockquote>

The fox later saves his life by leading him past a band of armed robbers.<ref>[[#Tyler|Tyler]]. 299–300.</ref>

==Portrayal==
{{see also|Kitsune in popular culture}}
[[File:A man confronted with an apparition of the Fox goddess.jpg|left|thumb|Inari Ōkami appears to a warrior accompanied by a kitsune. This portrayal shows the influence of [[Dakini]]ten concepts from Buddhism. Print by [[Utagawa Kuniyoshi]].]]
Embedded in Japanese folklore as they are, kitsune appear in numerous Japanese works. [[Noh]], [[kyogen]], [[bunraku]], and [[kabuki]] plays derived from folk tales feature them,<ref>[[#Hearn|Hearn]]. 162–163</ref><ref>[[#Nozaki|Nozaki]]. 109–124</ref> as do contemporary works such as [[anime]], [[manga]] and [[video game]]s. Japanese metal idol band [[Babymetal]] refer to the kitsune myth in their lyrics and include the use of fox masks, hand signs, and animation interludes during live shows.<ref>{{cite web|url=http://metalhammer.teamrock.com/features/2015-07-21/the-future-is-now-it-s-metal-hammer-273|title=Metal Hammer UK issue 273|publisher = Metal Hammer|accessdate=2015-08-14|date=2015-07-21}}</ref> Western authors of fiction have also made use of the kitsune legends.<ref>{{cite book|author=Johnson, Kij |title=The Fox Woman|url=https://books.google.com/books?id=Z0S6-KNcd6wC|date= 2001|publisher=Tom Doherty Associates|isbn=978-0-312-87559-6}}</ref><ref>{{cite book|author1= Lackey, Mercedes|author2=Edghill, Rosemary |title=Spirits White as Lightning|url=https://books.google.com/books?id=ispqCirVSvQC&pg=PT91|date= 2001|publisher=Baen Books|isbn=978-0-671-31853-6|pages=91–}}</ref><ref>{{cite book|last1=Highbridge|first1=Dianne|title=In the Empire of Dreams|date=1999|publisher=Soho Press|location=New York|isbn=1-56947-146-0}}</ref>

===Servants of Inari===
[[File:Yoshitoshi - 100 Aspects of the Moon - 91.jpg|thumb|The moon on Musashi Plain (fox) by [[Yoshitoshi]]<ref>from the series ''One hundred aspects of the moon'' by Tsukioka Yoshitori. National Gallery of Victoria, Australia [http://www.ngv.vic.gov.au/ngvschools/FloatingWorld/supernatural/The-moon-on-Musashi-Plain/]</ref>]]
Kitsune are associated with Inari, the [[Shinto]] deity of rice.<ref name="Smyers 1999, p.76">[[#Smyers|Smyers]]. 76</ref> This association has reinforced the fox's supernatural significance.<ref>[[#Hearn|Hearn]]. 153</ref> Originally, kitsune were Inari's messengers, but the line between the two is now blurred so that Inari Ōkami may be depicted as a fox. Likewise, entire shrines are dedicated to kitsune, where devotees can leave [[Offering (Buddhism)|offering]]s.<ref name="Hearn154"/> Fox spirits are said to be particularly fond of a fried sliced tofu called ''[[aburage]]'', which is accordingly found in the noodle-based dishes kitsune [[udon]] and kitsune [[soba]]. Similarly, ''Inari-zushi'' is a type of [[sushi]] named for Inari Ōkami that consists of rice-filled pouches of fried tofu.<ref name="Smyers 1999, p.96">[[#Smyers|Smyers]]. 96</ref> There is speculation among folklorists as to whether another Shinto fox deity existed in the past. Foxes have long been worshipped as ''kami''.<ref name="Smyers 1999, p.77, 81">[[#Smyers|Smyers]]. 77, 81</ref>

Inari's kitsune are white, a color of good [[omen]].<ref name="Hearn154"/> They possess the power to ward off evil, and they sometimes serve as guardian spirits. In addition to protecting Inari shrines, they are petitioned to intervene on behalf of the locals and particularly to aid against troublesome ''nogitsune'', those spirit foxes who do not serve Inari. Black foxes and nine-tailed foxes are likewise considered good omens.<ref name="Ashkenazy148"/>

According to beliefs derived from ''fusui'' (''[[feng shui]]''), the fox's power over evil is such that a mere statue of a fox can dispel the evil ''[[qi|kimon]]'', or energy, that flows from the northeast. Many Inari shrines, such as the famous [[Fushimi Inari]] shrine in [[Kyoto]], feature such statues, sometimes large numbers of them.

Kitsune are connected to the Buddhist religion through the [[Dakini]]ten, goddesses conflated with Inari's female aspect. Dakiniten is depicted as a female [[boddhisattva]] wielding a sword and riding a flying white fox.<ref name="Smyers 1999, pp.82–85">[[#Smyers|Smyers]]. 82–85</ref>

===Tricksters===
[[File:Fushimi Inari mini torii.jpg|right|thumb|The [[Fushimi Inari]] shrine in [[Kyoto, Japan|Kyoto]] features numerous kitsune statues.]]
Kitsune are often presented as [[trickster]]s, with motives that vary from mischief to malevolence. Stories tell of kitsune playing tricks on overly proud [[samurai]], greedy merchants, and boastful commoners, while the crueler ones abuse poor tradesmen and farmers or devout Buddhist monks. Their victims are usually men; women are possessed instead.<ref name="Tyler xlix"/> For example, kitsune are thought to employ their [[kitsunebi]] to lead travelers astray in the manner of a [[will o' the wisp]].<ref>[[#Addiss|Addiss]]. 137</ref><ref>[[#Hall|Hall]]. 142</ref> Another tactic is for the kitsune to confuse its target with illusions or visions.<ref name="Tyler xlix"/> Other common goals of trickster kitsune include seduction, theft of food, humiliation of the prideful, or vengeance for a perceived slight.

A traditional game called ''kitsune-ken'' (''fox-fist'') references the kitsune's powers over human beings. The game is similar to [[rock-paper-scissors|rock, paper, scissors]], but the three hand positions signify a fox, a hunter, and a village headman. The headman beats the hunter, whom he outranks; the hunter beats the fox, whom he shoots; the fox beats the headman, whom he bewitches.<ref>[[#Nozaki|Nozaki]]. 230</ref><ref name="Smyers 1999, p.98">[[#Smyers|Smyers]].  98</ref>

This ambiguous portrayal, coupled with their reputation for vengefulness, leads people to try to discover a troublesome fox's motives. In one case, the 16th-century leader [[Toyotomi Hideyoshi]] wrote a letter to the ''kami'' Inari:

{{quote|To Inari Daimyojin,

My lord, I have the honor to inform you that one of the foxes under your jurisdiction has bewitched one of my servants, causing her and others a great deal of trouble.  I have to request that you make minute inquiries into the matter, and endeavor to find out the reason of your subject misbehaving in this way, and let me know the result.

If it turns out that the fox has no adequate reason to give for his behavior, you are to arrest and punish him at once.  If you hesitate to take action in this matter I shall issue orders for the destruction of every fox in the land.  Any other particulars that you may wish to be informed of in reference to what has occurred, you can learn from the high priest of [[Yoshida Shintō|Yoshida]].<ref>[[#Hall|Hall]]. 137; the Yoshida priest in question was Yoshida Kanemi (1535–1610), then head priest at the [[Yoshida Shrine]] in Kyoto.</ref>}}

[[File:Tamamo-no-mae-woodblock.jpg|right|thumb|[[Tamamo-no-Mae]], a legendary kitsune featured in [[noh]] and [[kyogen]] plays. Print by [[Yoshitoshi]].]]

Kitsune keep their promises and strive to repay any favor. Occasionally a kitsune attaches itself to a person or household, where they can cause all sorts of mischief. In one story from the 12th century, only the homeowner's threat to exterminate the foxes convinces them to behave. The kitsune patriarch appears in the man's dreams:
{{quote|My father lived here before me, sir, and by now I have many children and grandchildren. They get into a lot of mischief, I'm afraid, and I'm always after them to stop, but they never listen. And now, sir, you're understandably fed up with us. I gather that you're going to kill us all. But I just want you to know, sir, how sorry I am that this is our last night of life. Won't you pardon us, one more time? If we ever make trouble again, then of course you must act as you think best. But the young ones, sir&nbsp;— I'm ''sure'' they'll understand when I explain to them why you're so upset. We'll do everything we can to protect you from now on, if only you'll forgive us, and we'll be sure to let you know when anything good is going to happen!"<ref>[[#Tyler|Tyler]]. 114–5.</ref>}}

Other kitsune use their magic for the benefit of their companion or hosts as long as the human beings treat them with respect. As ''[[yōkai]]'', however, kitsune do not share human morality, and a kitsune who has adopted a house in this manner may, for example, bring its host money or items that it has stolen from the neighbors. Accordingly, common households thought to harbor kitsune are treated with suspicion.<ref>[[#Hearn|Hearn]]. 159–161</ref> Oddly, samurai families were often reputed to share similar arrangements with kitsune, but these foxes were considered ''zenko'' and the use of their magic a sign of prestige.<ref>[[#Hall|Hall]]. 148</ref> Abandoned homes were common haunts for kitsune.<ref name="Tyler xlix"/> One 12th-century story tells of a minister moving into an old mansion only to discover a family of foxes living there. They first try to scare him away, then claim that the house "has been ours for many years, and ... we wish to register a vigorous protest." The man refuses, and the foxes resign themselves to moving to an abandoned lot nearby.<ref>[[#Tyler|Tyler]]. 122–4.</ref>

Tales distinguish kitsune gifts from kitsune payments. If a kitsune offers a payment or reward that includes money or material wealth, part or all of the sum will consist of old paper, leaves, twigs, stones, or similar valueless items under a magical illusion.<ref>[[#Nozaki|Nozaki]]. 195</ref> True kitsune gifts are usually intangibles, such as protection, knowledge, or long life.<ref name="Smyers 1999, pp.103–105">[[#Smyers|Smyers]]. 103–105</ref>

[[File:Kuniyoshi Kuzunoha.jpg|left|thumb|The kitsune [[Kuzunoha]] casts a fox's shadow even in human form. Kuzunoha is a popular figure in folklore and the subject of [[kabuki]] plays. Print by [[Utagawa Kuniyoshi]].]]

===Wives and lovers===
Kitsune are commonly portrayed as lovers, usually in stories involving a young human male and a kitsune who takes the form of a human woman.<ref name="Hamel 2003, p.90">[[#Hamel|Hamel]]. 90</ref> The kitsune may be a seductress, but these stories are more often romantic in nature.<ref>[[#Hearn|Hearn]]. 157</ref> Typically, the young man unknowingly marries the fox, who proves a devoted wife. The man eventually discovers the fox's true nature, and the fox-wife is forced to leave him. In some cases, the husband wakes as if from a dream, filthy, disoriented, and far from home. He must then return to confront his abandoned family in shame.

Many stories tell of fox-wives bearing children. When such progeny are human, they possess special physical or [[supernatural]] qualities that often pass to their own children.<ref name="Ashkenazy148"/> The [[astrology|astrologer]]-magician [[Abe no Seimei]] was reputed to have inherited such extraordinary powers.<ref name="Ashkenazy">[[#Ashkenazy|Ashkenazy]]. 150</ref>

Other stories tell of kitsune marrying one another. Rain falling from a clear sky&nbsp;— a [[sunshower]]&nbsp;— is called ''[[kitsune no yomeiri]]'' or ''the kitsune's wedding'', in reference to a folktale describing a wedding ceremony between the creatures being held during such conditions.<ref>[[#Addiss|Addiss]]. 132</ref> The event is considered a good omen, but the kitsune will seek revenge on any uninvited guests,<ref>{{cite journal|author=Vaux, Bert|url=http://www.linguistlist.org/issues/9/9-1795.html |title=Sunshower summary|journal=Linguist |volume=9|issue=1795 |date=December 1998}}  A compilation of terms for sun showers from various cultures and languages.</ref> as is depicted in the 1990 [[Akira Kurosawa]] film ''[[Dreams (1990 film)|Dreams]]''.<ref>{{Cite journal|title=The Fox's Wedding|first=Robert|last=Blust|journal=Anthropos|volume=94|issue=4/6|year=1999|pages=487–499|jstor=40465016}}</ref>

Stephen Turnbull, in "Nagashino 1575", relates the tale of the Takeda clan's involvement with a fox-woman. The warlord [[Takeda Shingen]], in 1544, defeated in battle a lesser local warlord named [[Suwa Yorishige]] and drove him to suicide after a "humiliating and spurious" peace conference, after which Shingen forced marriage on Suwa Yorishige's beautiful 14-year-old daughter Lady Koi—Shingen's own niece. Shingen, Turnbull writes, "was so obsessed with the girl that his superstitious followers became alarmed and believed her to be an incarnation of the white fox-spirit of the [[Suwa Taisha|Suwa Shrine]], who had bewitched him in order to gain revenge."  When their son [[Takeda Katsuyori]] proved to be a disastrous leader and led the clan to their devastating defeat at the [[battle of Nagashino]], Turnbull writes, "wise old heads nodded, remembering the unhappy circumstances of his birth and his magical mother".<ref>[[Stephen Turnbull (historian)|Turnbull, Stephen]]. (2000). ''Nagashino 1575''. Osprey Publishing. ISBN 1-84176-250-4.</ref>

==See also==
{{Portal|Japan|Mythology|Animals}}
* [[Fox spirit]], a general overview about this being in [[East Asia]]n myth 
** [[Huli jing]] — Chinese fox spirit
** [[Kumiho]] — Korean fox spirit
* [[Red fox]] — species related to kitsune
* ''[[The Sandman: The Dream Hunters]]''
* [[Sessho-seki]]
* [[Tamamo-no-Mae]]
* [[Hakuzōsu]]
{{div col end}}

==Notes==
{{reflist|25em}}

==References==
{{refbegin}}
*{{cite book|ref=Addiss|author= Addiss, Stephen|title=Japanese Ghosts & Demons: Art of the Supernatural|place= New York|publisher= G. Braziller|year= 1985|isbn=0-8076-1126-3}}
* {{cite book|ref=Ashkenazy|author=Ashkenazy, Michael|title=Handbook of Japanese Mythology|place=Santa Barbara, California|publisher=  ABC-Clio|year= 2003|isbn=1-57607-467-6}}
*{{cite book|url=https://books.google.com/?id=f46OerF-91EC&printsec=frontcover|title=Human Animals |first= Frank|last= Hamel |publisher= Kessinger Publishing|year= 2003 |isbn= 0766167003|ref=Hamel}}
*{{cite book|ref=Hearn|author=Hearn, Lafcadio |year=2005|title=Glimpses of Unfamiliar Japan|url=http://www.gutenberg.org/etext/8130 |publisher=Project Gutenberg|isbn=1604247487 }}
* {{cite book|ref=Nozaki|author=Nozaki, Kiyoshi|url=http://www.delathehooda.com/kitsune/kitsunepdf.zip|title= Kitsuné&nbsp;— Japan's Fox of Mystery, Romance, and Humor|place=Tokyo|publisher= The Hokuseidô Press|year=1961}}
* {{cite book|url=https://books.google.com/?id=uaC-7pnqdtEC&printsec=frontcover|title=The fox and the jewel: shared and private meanings in contemporary Japanese inari worship |first= Karen Ann|last= Smyers |publisher= University of Hawaii Press|year= 1999 |isbn= 0824821025|ref=Smyers}}
*{{cite book|ref=Tyler|authorlink =Royall Tyler|author=Tyler, Royall|year=1987|title=Japanese Tales|place=New York|publisher= Pantheon Books|isbn=0-394-75656-8}}
{{refend}}

==Further reading==
{{refbegin}}
* Bathgate, Michael. ''The Fox's Craft in Japanese Religion and Folklore: [[shapeshifting|Shapeshifters]], Transformations, and Duplicities''. New York: Routledge, 2004. ISBN 0-415-96821-6

==External links==
{{commons category}}
* [http://www.coyotes.org/kitsune/kitsune.html The Kitsune Page]
* [http://www.cyberus.ca/~foxtrot/kitsune/index.htm Foxtrot's Guide to Kitsune Lore]
* [http://www.kitsune.org/ Kitsune.org folklore]
* [http://academia.issendai.com/fox-index.shtml Kitsune, Kumiho, Huli Jing, Fox&nbsp;– Fox spirits in Asia, and Asian fox spirits in the West] An extensive bibliography of fox-spirit books.
* [http://www.onmarkproductions.com/html/oinari.shtml Gods of Japan page on the fox spirit]
* [http://ranea.org/watts/writing/kitsune.html Kitsune: Coyote of the Orient]

{{Japanese folklore long}}

{{featured article}}

[[Category:Fairies]]
[[Category:Female legendary creatures]]
[[Category:Japanese folklore]]
[[Category:Japanese legendary creatures]]
[[Category:Mythological foxes]]
[[Category:Mythological tricksters]]
[[Category:Shapeshifting]]
[[Category:Shinto kami]]
[[Category:Spirit possession in fiction]]
[[Category:Yōkai]]
[[Category:Monsters]]
[[Category:Mythological canines]]