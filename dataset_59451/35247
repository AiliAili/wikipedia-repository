{{good article}}
{{Infobox hurricane season
| Basin=NIO
| Year=2002
| Track=2002 North Indian Ocean cyclone season summary.jpg
| First storm formed=May 6, 2002
| Last storm dissipated=December 25, 2002
| Strongest storm name=BOB 04
| Strongest storm pressure=984
| Strongest storm winds=55
| Average wind speed=1
| Total depressions=7
| Total storms=4
| Total hurricanes=1
| Total intense=0
| Fatalities=At least 182
| Damages=25
| Inflated=
| five seasons=[[2000 North Indian Ocean cyclone season|2000]], [[2001 North Indian Ocean cyclone season|2001]], '''2002''', [[2003 North Indian Ocean cyclone season|2003]], [[2004 North Indian Ocean cyclone season|2004]]
|Atlantic season=2002 Atlantic hurricane season
|East Pacific season=2002 Pacific hurricane season
|West Pacific season=2002 Pacific typhoon season
}}
The '''2002 North Indian Ocean cyclone season''' was a below active season in terms of [[tropical cyclone]] formation. The season had no official bounds, but most storms formed in either May or after October. No depressions or storms formed during the [[monsoon]] season from July to September, the first such instance on record. There are two main seas in the North [[Indian Ocean]] &ndash; the [[Bay of Bengal]] to the east of the [[Indian subcontinent]] &ndash; and the [[Arabian Sea]] to the west of India. The official [[Regional Specialized Meteorological Centre]] in this basin is the [[India Meteorological Department]] (IMD), while the [[Joint Typhoon Warning Center]] (JTWC) releases unofficial advisories. An average of four to six storms form in the North Indian Ocean every season with peaks in May and November.<ref name="IMD">{{cite report|author=Staff Writer |url=http://www.imd.ernet.in/services/cyclone/tropical-cyclone.htm |title=IMD Cyclone Warning Services: Tropical Cyclones |publisher=[[India Meteorological Department]] |accessdate=October 21, 2008 |archiveurl=https://web.archive.org/web/20081104064430/http://www.imd.ernet.in/services/cyclone/tropical-cyclone.htm |archivedate=4 November 2008 |deadurl=yes |df= }}</ref> Cyclones occurring between the meridians [[45th meridian east|45°E]] and [[100th meridian east|100°E]] are included in the season by the IMD.<ref>{{cite report|author=Staff Writer|publisher=India Meteorological Department|date=January 2009|accessdate=May 17, 2009|title=Report on Cyclonic Disturbances Over the North Indian During 2008|url=http://www.imd.ernet.in/section/nhac/dynamic/rsmc.pdf|format=PDF|archiveurl=https://web.archive.org/web/20090529004113/http://www.imd.ernet.in/services/cyclone/tropical-cyclone.htm|archivedate=May 29, 2009}}</ref>

Overall, there was a total of seven depressions and four cyclonic storms. The most intense and deadly tropical cyclone of the season, the [[2002 West Bengal cyclone|West Bengal cyclone]], lashed [[West Bengal|that province]] of [[India]] and [[Bangladesh]] in the month of November. Rough seas offshore caused at least 173&nbsp;drownings offshore Bangladesh and India, while over 100&nbsp;people were left missing. In West Bengal alone, 124&nbsp;fatalities were reported, with over one hundred people still missing. Flooding occurred there and some areas of Bangladesh, particularly the capital city of [[Dhaka]]. Another notable storm was the [[2002 Oman cyclone|Oman cyclone]] in May. It made a rare [[Landfall (meteorology)|landfall]] in the [[Oman]]i region of [[Dhofar Governorate|Dhofar]]. The storm brought historic rainfall to [[Oman]], which in turn brought flooding to the region. Nine people drowned and damage to property, crops, and transportation reached $25&nbsp;million (2002&nbsp;[[United States dollar|USD]]).

==Season summary==
<center><timeline>
ImageSize = width:800 height:220
PlotArea = top:10 bottom:80 right:20 left:20
Legend = columns:3 left:30 top:58 columnwidth:270
AlignBars = early
DateFormat = dd/mm/yyyy
Period = from:01/05/2002 till:31/12/2002
TimeAxis = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/05/2002

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)    legend:Depression/Deep_Depression
  id:TS     value:rgb(0,0.98,0.96)    legend:Cyclonic_Storm
  id:ST     value:rgb(0.80,1,1)       legend:Severe_Cyclonic_Storm
  id:VS     value:rgb(1,1,0.8)        legend:Very_Severe_Cyclonic_Storm
  id:ES     value:rgb(1,0.76,0.25)    legend:Extremely_Severe_Cyclonic_Storm
  id:SU     value:rgb(1,0.38,0.38)    legend:Super_Cyclonic_Storm

Backgroundcolors = canvas:canvas
BarData =
 barset:Hurricane
 bar:Month

PlotData=
 barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
 from:06/05/2002 till:10/05/2002 color:TS text:"[[2002 Oman cyclone|ARB 01]] (CS)"
 from:10/05/2002 till:12/05/2002 color:TD text:"BOB 02 (DD)"
 from:17/05/2002 till:19/05/2002 color:TD text:"Unnumbered (D)"
 from:22/10/2002 till:25/10/2002 color:TD text:"BOB 03"
 from:10/11/2002 till:12/11/2002 color:ST text:"[[2002 Bangladesh cyclone|BOB 04]] (SCS)"
 from:23/11/2002 till:28/11/2002 color:TS text:"BOB 05 (CS)"
 from:22/12/2002 till:25/12/2002 color:TS text:"BOB 06 (CS)"

 bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
 from:01/05/2002 till:01/06/2002 text:May
 from:01/06/2002 till:01/07/2002 text:June
 from:01/07/2002 till:01/08/2002 text:July
 from:01/08/2002 till:01/09/2002 text:August
 from:01/09/2002 till:01/10/2002 text:September
 from:01/10/2002 till:01/11/2002 text:October
 from:01/11/2002 till:01/12/2002 text:November
 from:01/12/2002 till:31/12/2002 text:December

</timeline></center>
Overall, the season was inactive in terms of tropical cyclone formation. The IMD tracked six tropical cyclones, which was below the average of 13 to 14 per season.<ref name="pw"/> No storms were active from June to September during the [[monsoon]] season,<ref name="pw"/> the first such instance of no depressions in the 115&nbsp;year record of the IMD.<ref>{{cite journal|journal=Current Science|date=May 2004|page=1404|volume=86|number=10|title=On the decreasing frequency of monsoon depressions over the Indian region|author=S.K. Dash|author2=Jenamani Rajendra Kumar|author3=M. S. Shekhar|accessdate=2014-01-07|url=http://www.iisc.ernet.in/currsci/may252004/1404.pdf|format=PDF}}</ref> Collectively, the storms of this season resulted in at least 182 deaths and $25&nbsp;million (2002&nbsp;USD) in damage, all of which can be attributed to ARB 01 and BOB 04.<ref name="rcuwm"/><ref name="dartmouth"/><ref name="em-dat">{{cite report|url=http://www.emdat.be/search-details-disaster-list|title=Disaster List|work=Université Catholique de Louvain|publisher=EM-DAT: The OFDA/CRED International Disaster Database|accessdate=23 December 2013}}</ref>

The first storm of the season, [[2002 Oman cyclone|ARB 01]], developed on May&nbsp;6 out of an [[area of low pressure]] over the Arabian Sea. It peaked winds of 65&nbsp;km/h (40&nbsp;mph) before making landfall near [[Salalah]], Oman on May&nbsp;10. The storm dissipated shortly thereafter.<ref name="pw"/> A deep depression, classified as BOB 02, developed in the [[Andaman Sea]] on May&nbsp;10. The deep depression remained disorganized and made landfall near [[Yangon]], [[Burma]] before dissipating on May&nbsp;12.<ref name="padgett5"/> Later that month, a tropical depression, recognized only by the Thailand Meteorological Department, developed in the Bay of Bengal and also made landfall in Burma.<ref name="cyclonetracks"/> Activity in the North Indian Ocean then went dormant for over five months, a direct result of the monsoon season in the region.<ref name="pw"/> [[Tropical cyclogenesis]] resumed with the development of Tropical Depression BOB 03 forming near [[Andhra Pradesh]] on October&nbsp;22.<ref name="oct02"/>

On November&nbsp;11, a severe cyclonic storm &ndash; numbered [[2002 West Bengal cyclone|BOB 04]] &ndash; developed in the Bay of Bengal. It soon became the strongest tropical cyclone with maximum sustained winds of 100&nbsp;km/h (65&nbsp;mph) and a minimum [[barometric pressure]] of {{convert|984|mbar|inHg|abbr=on|lk=on}}. BOB 04 made landfall in Bangladesh on November&nbsp;12, hours before dissipating. Later in November, another cyclonic storm &ndash; assigned to BOB 05 &ndash; formed in the Bay of Bengal on November&nbsp;23. It moved northward before eventually curving westward and dissipating on November&nbsp;28.<ref name="padgett11"/> The final tropical cyclone developed southwest of [[Sri Lanka]] on December&nbsp;21. The system headed generally east-northeastward and strengthened into cyclonic storm on December&nbsp;24, before demising well east of Sri Lanka on the following day.<ref name="padgett12"/>

==Systems==

===Cyclonic Storm ARB 01===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 01A 09 may 2002 0928Z.jpg
|Track=Cyclone 01A 2002 track.png
|Formed=May 6
|Dissipated=May 10
|3-min winds=35
|1-min winds=45
|Pressure=996
}}
{{Main article|2002 Oman cyclone}}
A low pressure area in the Arabian Sea developed into a depression while located a few hundred miles west-northwest of [[Maldives]] at 0300&nbsp;UTC on May&nbsp;6.<ref name="pw"/><ref name="imdbt">{{cite report|url=http://www.imd.gov.in/section/nhac/dynamic/bestpara.xls |title=Best Tracks Data: 2002 |date=2003 |publisher=India Meteorological Department |accessdate=23 December 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20091116093857/http://www.imd.gov.in/section/nhac/dynamic/bestpara.xls |archivedate=16 November 2009 |df= }}</ref> By the following day, it had intensified into a deep depression. However, dry air diminished convection,<ref name="pw"/> causing the cyclone to weaken to a depression on May&nbsp;8 at 0300&nbsp;UTC.<ref name="imdbt"/> Nine hours later, it was upgraded back to a deep depression. On May&nbsp;8, the cyclone turned west-northwestward. Further intensification occurred, with the deep depression becoming a cyclonic storm at 0600&nbsp;UTC on May&nbsp;9. The storm maintained its intensity until weakening slightly early on May&nbsp;10, while briefly tracking northwestward. Shortly thereafter, it made landfall near [[Salalah]], Oman. The cyclone rapidly weakened and dissipated inland later on May&nbsp;10.<ref name="pw"/>

Waves up to 4&nbsp;m (13&nbsp;ft) lashed the coast of Oman, though no coastal flooding occurred. Wind gusts reaching 106&nbsp;km/h (66&nbsp;mph) affected some areas of Oman, while light winds were reported in [[Al Ghaydah]], [[Yemen]]. The storm brought heavy rainfall to the [[Dhofar Governorate|Dhofar]] region of Oman, peaking at 251&nbsp;mm (9.88&nbsp;in) in the city of [[Qairoon]]. Areas in the vicinity of the landfall location of the storm experienced the highest precipitation totals in 30&nbsp;years.<ref name="padgett5"/> As a result, [[wadis]] quickly became rivers, sweeping away cars and drowning nine people.<ref name="rcuwm">{{cite report|author=Ahmed Majid Al-Hakmani|year=2006|title=Flood Control Project in Salalah, Oman|publisher=Regional Centre on Urban Water Management|accessdate=July 23, 2008 |url=http://www.rcuwm.org.ir/events/workshop/08/files/Oman%20-%20Al-Hakmani-full.pdf|format=PDF}}{{Dead link|date=February 2015}}</ref><ref name="padgett5"/> Additionally, property, crops, and transportation suffered impacts from flooding.<ref name="rcuwm"/> Damage from the storm totaled to $25&nbsp;million, all of which was in Oman.<ref name="dartmouth">{{cite report|author=Dartmouth Flood Observatory|date=January 8, 2003|title=2002 Global Register of Extreme Flood Events|accessdate=March 27, 2013|url=http://www.dartmouth.edu/~floods/Archives/2002sum.htm| archiveurl= https://web.archive.org/web/20080725134558/http://www.dartmouth.edu/%7Efloods/Archives/2002sum.htm| archivedate= July 25, 2008 <!--DASHBot-->| deadurl= no}}</ref>
{{clear}}

===Deep Depression BOB 02===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 02B 11 may 2002 0608Z.jpg
|Track=Cyclone 02B 2002 track.png
|Formed=May 10
|Dissipated=May 12
|3-min winds=30
|1-min winds=45
|Pressure=991
}}
A tropical disturbance near [[Sumatra]] was tracked starting on May&nbsp;7. Although the system was disorganized and convection was sporadic, it managed to develop a low-level center of circulation on May&nbsp;9. After significant strengthening on May&nbsp;10, a [[Tropical Cyclone Formation Alert]] (TCFA) was issued later that day. Shortly thereafter, the disturbance became Tropical Cyclone 02B at 1200&nbsp;UTC, while located about 230&nbsp;km (145&nbsp;mi) southeast of [[Port Blair]], [[Andaman and Nicobar Islands]]. Deep convection continued to be sporadic until becoming persistent early on May&nbsp;11. Around that time, the deep depression reached 3-minute sustained winds of 55&nbsp;km/h (35&nbsp;mph).<ref name="padgett5">{{cite report|url=http://australiasevereweather.com/cyclones/2002/summ0205.htm|title=Monthly Global Tropical Cyclone Summary &ndash; May 2002|author=Gary Padgett|accessdate=March 26, 2013}}</ref>

Later on May&nbsp;11, Cyclone 02B unexpectedly accelerated to the north-northeast while crossing the northern Andaman Sea.<ref name="padgett5"/> At 2300&nbsp;UTC on May&nbsp;11, the cyclone made landfall just east of [[Yangon]], [[Burma]].<ref name="imdbt"/> By early on the following day, it weakened to a depression. The final warning on Cyclone 02B was issued at 0600&nbsp;UTC on May&nbsp;12 and indicated that the storm dissipated about 175&nbsp;km (110&nbsp;mi). The city of Yangon experienced wind gusts of about 47&nbsp;km/h (29&nbsp;mph), according to the JTWC. Cyclone 02B co-existed in a pair, with the southern counterpart being [[2001–02 Australian region cyclone season#Tropical Cyclone Errol|Tropical Cyclone Errol]], which was in the South Indian Ocean within Australian Bureau of Meteorology's responsibility.<ref name="padgett5"/>  
{{clear}}

===Tropical Depression===
{{Infobox Hurricane Small
|Basin=WPac
|WarningCenter=TMD
|Image=Tropical Depression 18 may 2002 0749Z.jpg
|Track=TMD_Depression_2002_track.png
|Formed=May 17
|Dissipated=May 19
|10-min winds=30
|Pressure=995
}}
The Thailand Meteorological Department began issuing advisories on a tropical depression in the Bay of Bengal on May&nbsp;17. Several hours later, the JTWC issued a TCFA for the system. Minimal strengthening occurred as the depression tracked rather swiftly toward the coast of [[Myanmar]]. At 0900&nbsp;UTC on May&nbsp;18, the depression made landfall near [[Taungup]], [[Rakhine State]], with winds of 55&nbsp;km/h (35&nbsp;mph). The JTWC cancelled the TCFA seven hours later, having never classified the system as a tropical depression. It weakened inland and dissipated over eastern Myanmar at 0300&nbsp;UTC on May&nbsp;19. Impact from this system is unknown.<ref name="cyclonetracks">{{cite report|url=http://www.typhoon2000.ph/garyp_mgtcs/may02tks.txt|title=Global Tropical Cyclone Tracks &ndash; May 2002|author=Gary Padgett|date=August 1, 2002|accessdate=September 30, 2012}}</ref>
{{clear}}

===Depression BOB 03===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Depression 25 oct 2002 0838Z.jpg
|Track=October_IO_Depression_2002_track.png
|Formed=October 22
|Dissipated=October 25
|3-min winds=25
|Pressure=1003
}}
The JTWC issued a TCFA late on October&nbsp;22 for a depression located about 235&nbsp;km (145&nbsp;mi) east-southeast of [[Chennai]], [[Tamil Nadu]]. By 0300&nbsp;UTC on the following day, the India Meteorological Department issued a bulletin on the depression. Due to multiple low-level center of circulations and an ill-defined structure, the depression was difficult to track. It moved in a quasi-stationary motion offshore [[Andhra Pradesh]]. Minimal intensification occurred, and by 1930&nbsp;UTC on October&nbsp;25, the depression dissipated about 235&nbsp;km (145&nbsp;mi) north of Chennai.<ref name="oct02">{{cite report|url=http://australiasevereweather.com/cyclones/2003/summ0210.htm|title=Monthly Global Tropical Cyclone Summary &ndash; October 2002|author=Gary Padgett|accessdate=October 13, 2012}}</ref>
{{clear}}

===Severe Cyclonic Storm BOB 04===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 03B 12 nov 2002 0409Z.jpg
|Track=Cyclone 03B 2002 track.png
|Formed=November 10
|Dissipated=November 12
|3-min winds=55
|1-min winds=55
|Pressure=984<!-- JTWC -->
}}
{{main article|2002 West Bengal cyclone}}
Tropical Cyclone 04B developed as a depression near Chennai, India on November&nbsp;10. Later that day, it intensified into a deep depression while tracking northward. As the storm was moving to the northeast, it was upgraded to a cyclonic storm, due to gale force winds. The cyclone came under the influence of mid-latitude trough, which caused the storm to accelerate to the north-northeast. Early on November&nbsp;12, it was upgraded to a severe cyclonic storm,<ref name="padgett11"/> as maximum sustained winds reached 100&nbsp;km/h (65&nbsp;mph). Later that day at 0900&nbsp;UTC, the storm made landfall near [[Sagar Island]], [[West Bengal]].<ref name="imdbt"/> The cyclone quickly weakened inland and by 1200&nbsp;UTC on November&nbsp;12, the IMD issued its final advisory, while the system situated about 200&nbsp;km (125&nbsp;mi) northeast of [[Kolkata]].<ref name="padgett11"/>

Rough seas offshore [[Orissa, India|Orissa]] caused two fishing trawlers to collide, resulting in 18&nbsp;fatalities, while two additional trawlers were reported missing.<ref name="pw"/> In West Bengal, the storm uprooted trees and dropped heavy rainfall, as well as causing two deaths. Strong winds and heavy rainfall in Bangladesh impacted many cities and villages, including the capital city of [[Dhaka]], forcing thousands to evacuate.<ref name="padgett11"/> Ten wooden trawlers carrying 150&nbsp;men sank offshore Bangladesh. Eight additional boats carrying 60&nbsp;occupants were reported missing.<ref name="dn">{{cite news|url=http://www.deseretnews.com/article/948344/200-feared-dead-as-storm-sinks-boats-in-Bangladesh.html|title=200 feared dead as storm sinks boats in Bangladesh|agency=[[Associated Press]]|date=November 13, 2002|newspaper=[[Deseret News]]|accessdate=August 26, 2012}}</ref> Along coastal areas of the country, winds destroyed bamboo huts, uprooted trees, and disrupted road transport between various towns and villages. The storm was attributed to at least 51&nbsp;deaths, while between 111 and 560&nbsp;people were classified as missing.<ref name="padgett11"/>
{{clear}}

===Cyclonic Storm BOB 05===
{{Infobox Hurricane Small
|Basin=NIO
|Type1=cyclstorm
|Image=Tropical Cyclone 04B 24 nov 2002 0806Z.jpg
|Track=Cyclone 04B 2002 track.png
|Formed=November 23
|Dissipated=November 28
|1-min winds=45
|Pressure=991
}}
A low pressure area developed within an equatorial trough centered over the southeastern Bay of Bengal on November&nbsp;22. After tracking northwestward for about twenty-four hours, the system developed into Tropical Cyclone 05B, while located about 815&nbsp;km (505&nbsp;mi) east-southeast of Chennai, Tamil Nadu. While moving northward, it intensified into a deep depression at 1800&nbsp;UTC on November&nbsp;23. Strengthening continued and early on November&nbsp;24, the deep depression was upgraded to a cyclonic storm. Later that day, the storm turned northwestward and later curved westward.<ref name="pw">{{cite report|url=http://www.preventionweb.net/files/1527_7895.pdf|title=Panel On Tropical Cyclones Annual Review 2002|author=R. R. KelKar|date=2004|publisher=[[World Meteorological Organization]]|pages=58, 60|accessdate=March 26, 2013|format=PDF}}</ref>

As it was moving westward, the system became disorganized and the center was difficult to track. Despite significant convection, the JTWC discontinued advisories on the storm at 1200&nbsp;UTC on November&nbsp;25, possibly in anticipation that it would soon dissipate. However, it remained a tropical cyclone for almost three more days.<ref name="pw"/><ref name="padgett11">{{cite report|url=http://australiasevereweather.com/cyclones/2003/summ0211.htm|title=Monthly Global Tropical Cyclone Summary &ndash; November 2002|author=Gary Padgett|accessdate=March 26, 2013}}</ref> By 1200&nbsp;UTC on November&nbsp;27, the storm was downgraded to a deep depression. The system moved northwestward and weakened further to a depression six hours later. It degenerated into an area of low pressure area while located over the central Bay of Bengal on November&nbsp;28.<ref name="imdbt"/> 
{{clear}}

===Cyclonic Storm BOB 06===
{{Infobox Hurricane Small
|Basin=NIO
|Type1=cyclstorm
|Image=Tropical Cyclone 05B 24 dec 2002 0505Z.jpg
|Track=Cyclone 05B 2002 track.png
|Formed=December 21
|Dissipated=December 25
|1-min winds=35
|Pressure=997
}}
A low pressure area developed in the [[Intertropical Convergence Zone]] near Sri Lanka on December&nbsp;20.<ref name="padgett12"/> Early on December&nbsp;21, the system developed into a depression.<ref name="imdbt"/> The JTWC issued a TCFA at 1251&nbsp;UTC on December&nbsp;22, while it was centered {{convert|340|km|mi}} south-southeast of Sri Lanka. By 1800&nbsp;UTC on December&nbsp;23, the JTWC initiated advisories on Tropical Cyclone 06B, which was located about 160&nbsp;km (100&nbsp;mi) southeast of [[Dondra Head]], Sri Lanka.<ref name="padgett12">{{cite report|url=http://australiasevereweather.com/cyclones/2003/summ0212.htm|title=Monthly Global Tropical Cyclone Summary &ndash; December 2002|author=Gary Padgett|accessdate=March 26, 2013}}</ref> At that time, the storm had intensified into a deep depression. Further strengthening occurred, and it was upgraded to a cyclonic storm early on December&nbsp;24.<ref name="imdbt"/>

After peaking with maximum sustained winds of 65&nbsp;km/h (40&nbsp;mph) and a minimum barometric pressure of {{convert|997|mbar|inHg|abbr=on}} on December&nbsp;24,<ref name="padgett12"/><ref name="imdbt"/> the storm soon weakened and convection diminished, possibly due to interaction with a nearby tropical disturbance.<ref name="padgett12"/> By early on the following day, it was downgraded to a deep depression while moving toward the northeast. Later that day, Cyclonic Storm BOB 06 weakened to a depression.<ref name="pw"/> At 1800&nbsp;UTC on December&nbsp;25, the JTWC issued a final advisory on the cyclone, citing that it degenerated into a remnant low pressure area while located about 685&nbsp;km (425&nbsp;mi) east-southeast of Dondra Head, Sri Lanka.<ref name="padgett12"/>
{{clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of notable tropical cyclones]]
*[[2002 Atlantic hurricane season]]
*[[2002 Pacific hurricane season]]
*[[2002 Pacific typhoon season]]
*South-West Indian Ocean cyclone seasons: [[2001–02 South-West Indian Ocean cyclone season|2001–02]], [[2002–03 South-West Indian Ocean cyclone season|2002–03]]
*Australian region cyclone seasons: [[2001–02 Australian region cyclone season|2001–02]], [[2002–03 Australian region cyclone season|2002–03]]
*South Pacific cyclone seasons: [[2001–02 South Pacific cyclone season|2001–02]], [[2002–03 South Pacific cyclone season|2002–03]]

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20080116195620/http://www.imd.gov.in:80/services/cyclone/impact.htm Impact of Cyclonic Storms and Suggested Mitigation Actions] (by India Meteorological Department)
*[http://www.preventionweb.net/files/1527_7895.pdf WMO / ESCAP Panel on Tropical Cyclones Annual Review 2002]

{{TC Decades|Year=2000|basin=North Indian Ocean|type=cyclone}}
{{2002 North Indian Ocean cyclone season buttons}}

{{DEFAULTSORT:2002 North Indian Ocean Cyclone Season}}
[[Category:2002 North Indian Ocean cyclone season|*]]
[[Category:Articles which contain graphical timelines]]