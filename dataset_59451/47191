In [[mathematics]], the '''connective constant''' is a numerical quantity associated with [[self-avoiding walks]] on a [[lattice (group)|lattice]]. It is studied in connection with the notion of [[Self-avoiding walk#Universality|universality]] in two-dimensional [[statistical physics]] models.<ref>{{cite book
 | last = Madras | first = N.
 |author2=Slade, G.
 | year = 1996
 | title = The Self-Avoiding Walk
 | publisher = Birkhäuser
 | isbn = 978-0-8176-3891-7
 }}</ref> While the connective constant depends on the choice of lattice so itself is not [[Universality (dynamical systems)|universal]] (similarly to other lattice-dependent quantities such as the [[Percolation threshold|critical probability threshold for percolation]]), it is nonetheless an important quantity that appears in conjectures for universal laws. Furthermore, the mathematical techniques used to understand the connective constant, for example in the recent rigorous proof by Duminil-Copin and [[Stanislav Smirnov|Smirnov]] that the connective constant of the hexagonal lattice has the precise value <math>\sqrt{2+\sqrt{2}}</math>, may provide clues<ref name="sdc">
{{cite journal 
|author1=H. Duminil-Copin |author2=S. Smirnov |year= 2010
|title= The connective constant of the honeycomb lattice equals <math> \sqrt{2 + \sqrt{2}}</math>
|journal= 
|volume= 
|issue= 
|pages= 
|publisher= 
|url= http://arxiv.org/abs/1007.0575
 }}</ref> to a possible approach for attacking other important open problems in the study of self-avoiding walks, notably the conjecture that self-avoiding walks converge in the scaling limit to the [[Schramm–Loewner evolution]].

==Definition==
The connective constant is defined as follows.  Let <math>c_n</math> denote the number of ''n''-step self-avoiding walks starting from a fixed origin point in the lattice. Since every ''n''&nbsp;+&nbsp;''m'' step self avoiding walk can be decomposed into an ''n''-step self-avoiding walk and an m-step self-avoiding walk, it follows that <math> c_{n+m} \leq c_n c_m </math>. Then by applying [[Fekete's lemma]] to the logarithm of the above relation, the limit <math>\mu = \lim_{n \rightarrow \infty} c_n^{1/n}</math> can be shown to exist. This number <math>\mu</math> is called the connective constant, and clearly depends on the particular lattice chosen for the walk since <math>c_n</math> does. The value of <math>\mu</math> is precisely known only for two lattices, see below. For other lattices, <math>\mu</math> has only been approximated numerically. It is conjectured that <math>c_n \approx \mu^n n^{\gamma-1}</math> as n goes to infinity, where <math>\mu</math> depends on the lattice, but the critical exponent <math>\gamma</math> is universal (it depends on dimension, but not the specific lattice). In 2-dimensions it is conjectured that <math>\gamma = 43/32</math> <ref name="Nienhuis1982">
{{cite journal 
|author= B. Nienhuis
|year= 1982
|title= Exact critical point and critical exponents of O(''n'') models in two dimensions
|journal= Phys. Rev. Lett.
|volume= 49
|issue= 15
|pages= 1062–1065
|publisher= 
|url= 
|doi= 10.1103/PhysRevLett.49.1062 
 |bibcode= 1982PhRvL..49.1062N
}}</ref><ref>
{{cite journal 
|author= B. Nienhuis
|year= 1984
|title= Critical behavior of two-dimensional spin models and charge asymmetry in the Coulomb gas
|journal= J. Stat. Phys.
|volume= 34
|issue= 5–6
|pages= 731–761
|publisher= 
|url= 
|doi= 10.1007/BF01009437 
 |bibcode= 1984JSP....34..731N
}}</ref>

==Known values<ref>
{{cite journal
|author1=I. Jensen |author2=A. J. Guttmann |year= 1998
|title= Self-avoiding walks, neighbor-avoiding walks and trails on semi-regular lattices
|journal= J. Phys. A
|volume= 31
|issue= 40
|pages= 8137–45
|publisher= 
|url= http://www.ms.unimelb.edu.au/~tonyg/articles/polygons.pdf
|doi= 10.1088/0305-4470/31/40/008
 |bibcode=1998JPhA...31.8137J }}</ref>==
{|border="1" cellpadding="5" cellspacing="0" align="center"
|-
! scope="col" style="background:#efefef;" | Lattice
! scope="col" style="background:#efefef;" | Connective constant
|-
|[[Hexagonal lattice|Hexagonal]]
|<math>\sqrt{2 + \sqrt{2}}\simeq 1.85</math>
|-
|Triangular
|<math>4.15079(4)</math>
|-
|[[Square lattice|Square]]
|<math>2.63815853(15)</math>
|-
|[[Trihexagonal tiling|Kagomé]]
|<math>2.56062</math>
|-
|Manhattan
|<math>1.733535(3)</math>
|-
|L-lattice
|<math>1.5657(15)</math>
|-
|<math>(3.12^2)</math> lattice
|<math>1.7110412...</math>
|-
|<math>(4.8^2)</math> lattice
|<math>1.80883001(6)</math>
|}
These values are taken from the 1998 Jensen–Guttmann paper. The connective constant of the <math>(3.12^2)</math> lattice, since each step on the hexagonal lattice corresponds to either two or three steps in it, can be expressed exactly as the largest real root of the polynomial

: <math>x^{12} - 4x^8 - 8x^7 - 4x^6 + 2x^4 + 8x^3 + 12x^2 + 8x + 2</math>

given the exact expression for the hexagonal lattice connective constant. More information about these lattices can be found in the [[percolation threshold]] article.

==Duminil-Copin–Smirnov proof==<!-- The heading has a hyphen in Duminil-Copin and an en-dash between that and Smirnov.  Duminil-Copin is a hyphenated name of one person. -->
In 2010, Hugo Duminil-Copin and [[Stanislav Smirnov]] published the first rigorous proof of the fact that <math>\mu=\sqrt{2 + \sqrt{2}}</math> for the hexagonal lattice.<ref name="sdc" />
This had been conjectured by Nienhuis in 1982 as part of a larger study of O(''n'') models using renormalization techniques.<ref name="Nienhuis1982" />
The rigorous proof of this fact came from a program of applying tools from complex analysis to discrete probabilistic models that has also produced impressive results about the [[Ising model]] among others.<ref>
{{cite journal 
|author= S. Smirnov
|year= 2010
|title= Discrete Complex Analysis and Probability
|journal= Proc. Int. Congress of Mathematicians (Hyderabad, India) 2010
|volume= 1009
|issue= 
|pages= 565–621
|publisher= 
|arxiv= 1009.6077
 |bibcode= 2010arXiv1009.6077S
}}</ref>
The argument relies on the existence of a parafermionic observable that satisfies half of the discrete Cauchy–Riemann equations for the hexagonal lattice. We modify slightly the definition of a self-avoiding walk by having it start and end on mid-edges between vertices. Let H be the set of all mid-edges of the hexagonal lattice. For a self-avoiding walk <math>\gamma</math> between two mid-edges <math>a</math> and <math>b</math>, we define <math>\ell(\gamma)</math> to be the number of vertices visited and its winding <math>W_{\gamma}(a,b)</math> as the total rotation of the direction in radians when <math>\gamma</math> is traversed from <math>a</math> to <math>b</math>.  The aim of the proof is to show that the partition function

: <math>Z(x)=\sum_{\gamma: a\to H}x^{\ell(\gamma)} = \sum_{n=0}^{\infty}c_n x^n</math>

converges for <math>x<x_c</math> and diverges for <math>x>x_c</math> where the critical parameter is given by <math>x_c=1/ \sqrt{2+\sqrt{2}}</math>. This immediately implies that <math>\mu= \sqrt{2+\sqrt{2}}</math>.

Given a domain <math>\Omega</math> in the hexagonal lattice, a starting mid-edge <math>a</math>, and two parameters <math>x</math> and <math>\sigma</math>, we define the parafermionic observable

<math>F(z)=\sum_{\gamma\subset\Omega:a\to z} e^{-i\sigma W_{\gamma}(a,z)}x^{\ell(\gamma)}.</math>

If <math>x = x_c= 1/\sqrt{2 + \sqrt{2}} </math> and <math>\sigma=5/8</math>, then for any vertex <math>v</math> in <math>\Omega</math>, we have

: <math>(p-v)F(p) + (q-v)F(q) + (r-v)F(r) = 0,</math>

where <math>p,q,r</math> are the mid-edges emanating from <math>v</math>. This lemma establishes that the parafermionic observable is divergence-free. It has not been shown to be curl-free, but this would solve several open problems (see conjectures). The proof of this lemma is a clever computation that relies heavily on the geometry of the hexagonal lattice.

Next, we focus on a finite trapezoidal domain <math>S_{T,L}</math> with 2L cells forming the left hand side, T cells across, and upper and lower sides at an angle of <math>\pm \pi/3</math>. (Picture needed.) We embed the hexagonal lattice in the complex plane so that the edge lengths are 1 and the mid-edge in the center of the left hand side is positioned at &minus;1/2. Then the vertices in <math>S_{T,L}</math> are given by

: <math>V(S_{T,L})=\{ z\in V(\mathbb{H}) : 0 \leq Re(z)\leq \frac{3T+1}{2}, \; |\sqrt{3}Im(z)-Re(z)| \leq 3L\}. </math>

We now define partition functions for self-avoiding walks starting at <math>a</math> and ending on different parts of the boundary. Let <math>\alpha</math> denote the left hand boundary, <math>\beta</math> the right hand boundary, <math>\epsilon</math> the upper boundary, and <math>\bar{\epsilon}</math> the lower boundary. Let

: <math>
A_{T,L}^x:=\sum_{\gamma \in S_{T,L}:a\to \alpha\setminus\{a\}} x^{\ell(\gamma)},\quad
 B_{T,L}^x:=\sum_{\gamma \in S_{T,L}:a\to \beta} x^{\ell(\gamma)}, \quad
E_{T,L}^x:=\sum_{\gamma \in S_{T,L}:a\to \epsilon \cup \bar{\epsilon}} x^{\ell(\gamma)}.
</math>

By summing the identity

: <math>(p-v)F(p) + (q-v)F(q) + (r-v)F(r) = 0</math>

over all vertices in <math>V(S_{T,L})</math> and noting that the winding is fixed depending on which part of the boundary the path terminates at, we can arrive at the relation

: <math>1= \cos(3\pi/8) A_{T,L}^{x_c} + B_{T,L}^{x_c} + \cos(\pi/4) E_{T,L}^{x_c}</math>

after another clever computation. Letting <math>L\to\infty</math>, we get a strip domain <math>S_T</math> and partition functions

: <math>
A_{T}^x:=\sum_{\gamma \in S_{T}:a\to \alpha\setminus\{a\}} x^{\ell(\gamma)},\quad
 B_{T}^x:=\sum_{\gamma \in S_{T}:a\to \beta} x^{\ell(\gamma)}, \quad
E_{T}^x:=\sum_{\gamma \in S_{T}:a\to \epsilon \cup \bar{\epsilon}} x^{\ell(\gamma)}.
</math>

It was later shown that <math>E_{T,L}^{x_c}=0</math>, but we do not need this for the proof.<ref>
{{Cite journal|author1=N. Beaton |author2=J. de Gier |author3=A. J. Guttmann |year= 2011
|title= The critical fugacity for surface adsorption of SAW on the honeycomb lattice is <math>1+\sqrt{2}</math>
|journal=Communications in Mathematical Physics |volume=326 |issue=3 |pages=727 |arxiv= 1109.0358|doi=10.1007/s00220-014-1896-1 }}</ref>
We are left with the relation

: <math>1= \cos(3\pi/8) A_{T,L}^{x_c} + B_{T,L}^{x_c}</math>.

From here, we can derive the inequality

: <math>A_{T+1}^{x_c} - A_{T}^{x_c} \leq x_c (B_{T+1}^{x_c})^2</math>

And arrive by induction at a strictly positive lower bound for <math>B_{T}^{x_c} </math>. Since <math>Z(x_c)\geq\sum_{T>0}B_T^{x_c} =\infty</math>, we have established that <math>\mu\geq 1/\sqrt{2+\sqrt{2}}</math>.

For the reverse inequality, for an arbitrary self avoiding walk on the honeycomb lattice, we perform a canonical decomposition due to Hammersley and Welsh of the walk into bridges of widths <math>T_{-I}<\cdots < T_{-1}</math> and <math>T_0>\cdots > T_j</math>. Note that we can bound

: <math>B_T^x\leq (x/x_c)^T B_T^{x_c}\leq (x/x_c)^T</math>

which implies <math> \prod_{T>0}(1+B_T^x)<\infty</math>. 
Finally, it is possible to bound the partition function by the bridge partition functions

: <math>Z(x)\leq  \sum_{T_{-I} <\cdots < T_{-1},\; T_0>\cdots > T_j} 2 \left(\prod_{k=-I}^j B_{T_k}^x\right) = 2\left(\prod_{T>0}(1+B_T^x)\right)^2<\infty.</math>

And so, we have that <math>\mu = \sqrt{2+\sqrt{2}}</math> as desired.

==Conjectures==
Nienhuis argued in favor of Flory's prediction that the [[mean squared displacement]] of the self-avoiding random walk <math>\langle |\gamma(n)|^2 \rangle</math> satisfies the scaling relation
<math>\langle |\gamma(n)|^2 \rangle = \frac{1}{c_n} \sum_{n\;\mathrm{step\; SAW}}|\gamma(n)|^2 = n^{2\nu +o(1)}</math>,
with <math>\nu = 3/4</math>.<ref name="sdc" />
The scaling exponent <math>\nu</math> and the universal constant <math>11/32</math> could be computed if the self-avoiding walk possesses a conformally invariant scaling limit, conjectured to be a [[Schramm–Loewner evolution]] with <math>\kappa=8/3</math>.<ref>
{{cite journal 
|author1=G. Lawler |author2=O. Schramm |author3=W. Werner |year= 2004
|title= On the scaling limit of planar self-avoiding walk
|journal= Proc. Sympos. Pure. Math.
|volume= 72
|issue= 
|pages= 339–364
|publisher= 
|arxiv= math/0204277
 |bibcode=2002math......4277L }}</ref>

==See also==
* [[Percolation threshold]]

==References==
{{reflist}}

== External links ==
*{{MathWorld|urlname=Self-AvoidingWalkConnectiveConstant|title=Self-Avoiding Walk Connective Constant}}

[[Category:Discrete geometry]]