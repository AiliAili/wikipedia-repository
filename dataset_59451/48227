{{about|American pop musician||Tom Goss (disambiguation)}}
{{Infobox musical artist <!-- See Wikipedia:WikiProject_Musicians -->
| name                = Tom Goss
| image               = Tom Goss 2014.jpg
| caption             = Goss performing in 2014
| background          = solo_singer
| birth_name          = Thomas Patrick Goss
| alias               = Justin Jaymes
| birth_date          = {{Birth date and age|mf=yes|1981|4|30}}
| birth_place         =[[Quincy, Illinois|Quincy]], [[Illinois]], [[United States|U.S.]]
| death_date          = 
| instrument          = [[Singing|Vocals]], [[guitar]], [[keyboards]]
| genre               = [[Power pop]], [[blue-eyed soul]], [[Pop rock]], [[folk rock]]
| occupation          = Musician, Actor
| years_active        = 2005–present
| label               = [[Independent record label|independent]]
| associated_acts     = [[Matt Alber]], [[Richard Morel]], Liz DeRoche, Mike Ofca, Randy LeRoy
| website             = {{URL|tomgossmusic.com}}
| notable_instruments = [[Seagull Guitars]]
}}

'''Thomas Patrick Goss''' (born April 30, 1981) is an American singer-songwriter and actor. Goss has self-released five studio albums, one live album, two EPs, and multiple non-album singles.  He has seen commercial success with his music featured on [[MTV]]'s [[Logo TV]]. He won Best Gay Musician in DC from ''[[The Washington Blade]]'', both in 2011 and 2012. Many of Goss' songs and music videos ("Lover", "Bears" and "Make Believe") speak to [[LGBT]] issues such as [[marriage equality]], [[DADT]], and gay subcultures such as [[Bear (gay culture)|bears]].  Goss tours internationally, often playing at LGBT establishments.<ref>{{cite web|title=Live in London @ Bar Titania|url=http://tomgoss.fanbridge.com/tourdates/details.php?id=255712|website=fanbridge.com|accessdate=April 28, 2014}}</ref>  In 2014, Goss landed his first starring role in a feature film—the gay-themed ''[[Out to Kill]]''.

== Personal life ==
Goss was born in [[Quincy, Illinois]], and raised in [[Kenosha, Wisconsin]].<ref>{{cite web|last1=Davis|first1=Andrew|title=Tom Goss|url=http://dc.gopride.com/news/interview.cfm/articleid/238996|website=Pridedc.com|ref=}}</ref> He graduated from the [[University of Central Missouri]]<ref>{{cite news|last=Goss|first=Tom|title=Tom Goss: Back in my college town, out of the closet|url=http://www.metroweekly.com/elsewhere/2012/05/im-sitting-in-a-holiday.html|accessdate=27 March 2014|newspaper=Metro Weekly|date=May 12, 2012}}</ref> before coming to Washington, DC to attend a Roman Catholic seminary.<ref>{{cite news|last=Rule|first=Doug|title=Preacher of Love|url=http://www.metroweekly.com/arts_entertainment/music.php?ak=6153|accessdate=27 March 2014|newspaper=Metro Weekly|date=April 14, 2011}}</ref> Shortly after entry, he decided to take a different path and pursued work in the nonprofit industry, working at Charlie's Place as a Program Manager in Washington DC.<ref>{{cite web|last=bdweb8618k|first1=bdweb8618k|title=Meet Tom Goss, Charlie's Place|url=http://www.smallfriendlyplanet.org/meet-tom-goss-charlies-place/|website=Smallfriendlyplanet.org|accessdate=27 March 2014}}</ref>  He also pursued his music, performing in local coffeehouses. During this time, he [[came out of the closet]] and soon after met his husband, Mike Briggs, whom he married in October 2010.<ref>{{cite web|last1=Stump|first1=Howard|title=Celebrating Love: Tom and Mike|url=http://soundtracktomyday.blogspot.com/2010/10/celebrating-love-tom-mike.html|website=Soundtracktomyday.blogspot.com}}</ref> In 2012, Goss produced a music video for his song "You Know That I Love You" off his 2011 album, which documented his marriage ceremony.

== Music career ==

===2006-2008 Early career, "Rise"===
Goss started his career as a singer-songwriter in [[Washington, DC]] coffeehouses in the early 2000s. In 2006, Goss released his first album, ''Naked Without''.

In 2008, Goss released his first EP, ''Rise''. The title song from the EP hit #2 on [[Logo TV]]'s Click List that summer.<ref>{{cite news|last=Hillis|first=James|title=Interview Hop into bed with out singer/songwriter Tom Goss and his new video for "Til the End"|url=http://www.thebacklot.com/interview-hop-into-bed-with-out-singersongwriter-tom-goss-and-his-new-video-for-til-the-end/04/2009/|accessdate=2 November 2014|newspaper=Thebacklot.com|date=April 10, 2009}}</ref>

===2009-2010 "Till The End"===
In 2009, Goss released his second full album, ''Back to Love'', complete with 11 original songs.  On this album, Goss explored romantic love, spirituality and morality. The first single/video, "Till the End", went all the way to #1 on [[Logo TV]]'s Click List.  The album also features one of his most popular singles, "Lover".  The accompanying video tells the story of an American soldier killed in [[Afghanistan]] while his husband waits at home.  It stars servicemen who were discharged specifically under the [[Don't Ask, Don't Tell]] policy.<ref>{{cite web|title=Watch: Soldiers Discharged Under 'DADT' Star in Tom Goss' New Video 'Lover'|url=http://www.towleroad.com/2010/11/goss.html|publisher=towleroad.com|accessdate=27 March 2014}}</ref> "Lover" was put into heavy rotation on [[MTV]]'s [[Logo TV]], and garnered over 100,000 hits on [[YouTube]] and over 1.5 million hits on Youku. ''Back to Love'' received critical acclaim, with ''OnTap'' Magazine hailing it a "heartfelt winner." His newfound success propelled him into his first nationwide tour.

In 2010, Goss released a double EP called ''Politics of Love'' which combined the newly produced songs as well as songs from his 2008 EP, ''Rise''. The newly produced songs carried a political opinion about [[LGBT]] rights and complimented Goss' political project of the same name, which provided assistance for those looking to start conversations about marriage equality and equal rights in their communities.<ref>{{cite web|last1=Drew|first1=Polo|title=The Politics of Love - a New Project by Tom Goss|url=http://feastoffun.com/topics/politics-activism/2010/04/10/the-politics-of-love-a-new-project-by-tom-goss/|website=feastoffun.com|accessdate=April 10, 2010}}</ref>

Goss then released his first live album, ''Live at Terry's''.  He finished out the year with his first Christmas-themed single "Christmas, Chicago Time".

===2011-2012 "Make Believe", first music awards===
In 2011, Goss released his third full album, ''Turn It Around'', which was produced by Mike Ofca at Innovation Studios complete with 11 original songs.  The album launched another national tour.  Goss produced a music video for a song of this album called "Make Believe". The video received over 100,000 hits, and garnered attention from the media such as [[Huffington Post]]<ref>{{cite web|title=Tom Goss, Gay-Singer, Gets Naked For New "Make Believe" Video|url=http://www.huffingtonpost.com/2012/11/03/tom-goss-gay-make-believe-naked-video_n_2059277.html|website=huffingtonpost.com|accessdate=November 3, 2013}}</ref> for its use of [[time-lapse photography]].  The video and album helped him receive Best Gay Musician of the Year by ''[[The Washington Blade]]'' in 2011.<ref name="Gay DC 2011">{{cite news|last=Badmin|first=W|title=Best of Gay DC 2011: People|url=http://www.washingtonblade.com/2011/10/27/best-of-gay-d-c-2011-people/|accessdate=November 2, 2014|newspaper=Washington Blade|date=October 27, 2011}}</ref>

In 2012, Goss released his fourth full album, ''Love Songs and Underdogs'', complete with 10 original songs.  The songs on the album were all written and recorded in a one-room cabin in rural Virginia cut off from everything but his computer, keyboard, guitars and microphones. In a 2013 interview, Goss reflected on this experience: "...I think I had to think a lot about why am I creating music and what is it about being an artist and why we create art. And I think I came back and had to do something completely on my own to re-establish and have the confidence to believe in my artistic worth. And that was "Love Songs and Underdogs."<ref name="The Script">{{cite web|last1=Doyle|first1=JD|title=August 2013:The Script, Part 1|url=http://queermusicheritage.com/outaug13s.html|accessdate=November 1, 2014}}</ref> For this album, Goss toured 50 cities in America. His success led him to repeat his award by ''The Washington Blade'' for Best Gay Musician of the Year.<ref name="Gay DC 2012">{{cite news|last=Badmin|first=W|title=Best of Gay DC 2012: People|url=http://www.washingtonblade.com/2012/10/25/best-of-gay-d-c-people/|accessdate=27 March 2014|newspaper=Washington Blade|date=October 25, 2012}}</ref>

Goss rounded out the year with his second Christmas-themed song "Santa, Imma Make You Dance".

===2013 "Bears", broader international success===
Early in 2013, Goss produced a video of himself performing a cover version of [[Macklemore]] and [[Ryan Lewis]]' #1 song "[[Can't Hold Us]]".  It is his second most popular video on YouTube to date, with over 200,000 hits. Goss then released his single, "Bears", along with an accompanying music video.  He teamed up with producer [[Richard Morel]] of Blowoff, Deathfix, and Hot Sauce to craft a pop/rock anthem. He worked with Aram Vartian and Michael Patrick Key to direct the music video.  Together, the video (his most popular to date) and song received over half million YouTube hits and downloads worldwide, as well as press from media outlets like [[Queerty.com]],<ref>{{cite web|title=Tom Goss Hosts A "Bearbeque" For Wet and Wild Bears|url=http://www.queerty.com/watch-tom-goss-hosts-a-bearbeque-for-wet-and-wild-bears-20130709|website=Queerty.com}}</ref> [[Washington Post Express|The Washington Post Express]],<ref>{{cite web|last1=SHIGEOKA|first1=Scott|title=On The Spot: Tom Goss|url=http://www.washingtonpost.com/express/wp/2013/08/29/on-the-spot-tom-goss/|website=www.washingtonpost.com|publisher=The Washington Post}}</ref> and [[Sirius Radio]].<ref>{{cite news|last=Shigeoka|first=Scott|title=On The Spot: Tom Goss|url=http://www.washingtonpost.com/express/wp/2013/08/29/on-the-spot-tom-goss/|accessdate=27 March 2014|newspaper=The Washington Post|date=August 29, 2013}}</ref>  The single addressed Goss' affinity toward [[Bear (gay culture)|Bears]], a subset of the gay male culture that describes large, burly, often hairy men. Subsequently, Goss has been invited to Bear festivals around the world, most notably Bear Week in [[Provincetown, Massachusetts]].<ref>{{cite web|title=Bear Week Provincetown|url=http://www.ptownbears.org/event/event.asp|website=ptownbears.org}}</ref> and the Great British Bear Bash<ref>{{cite web|title=Great British Bear Bash|url=http://www.manbears-manchester.co.uk}}</ref> in Manchester, UK.  In September 2014, the video played in the DC Shorts Film Festival.<ref>{{cite news|title=On The Spot: Tom Goss|url=http://festival.dcshorts.com/bears/|accessdate=September 1, 2014}}</ref>  In support of "Bears", Goss toured internationally in the US and Europe.<ref>{{cite web|title=YouTube sensation Tom Goss hits the road for national tour|url=http://www.ragemonthly.com/2013/08/19/youtube-sensation-tom-goss-hits-the-road-for-national-tour/|publisher=Rage Monthly|accessdate=27 March 2014}}</ref>

Goss' further released a Liz DeRoche remix of his song "I Do".<ref name="The Script"/> This was followed by "Waiting For Your Call", a duet with long-time friend Joey Salinas. In an interview with queermusicheritage.com, Goss said "You know, it's interesting, I think that 2013, or the end of 2012-2013 has been about doing things that I wanted to do, and not really thinking as much about how people are going to perceive it. And I think that's been really freeing. So Joey and I had talked for years about doing a song together, and finally I said, well, I don't know why we're still talking, let's just set up a studio time."<ref name="The Script"/>

===2014 Contributions to the soundtrack ''Out to Kill'', "Illuminate the Dark"===
In 2014, Goss contributed three songs to soundtrack of the gay-themed murder mystery ''Out to Kill''.<ref>https://itunes.apple.com/us/album/out-to-kill-original-motion/id935511244</ref>  Most notable was "You Don't Know How Hard", performed by Goss' character in the movie "Justin Jaymes".  The dance/electro song was a big departure from Goss' previous acoustic work. The song and video are an ironic contrast to Goss' "Bears", which focuses on size inclusivity and bears the line "bears don't discriminate".<ref>{{cite web|title=Bears lyrics Tom Goss|url=https://www.sing365.com/music/HotLyrics.nsf/Bears-lyrics-Tom-Goss/1A2504E32AFF4A9048257BA6004E6576|accessdate=November 2, 2014}}</ref>  The video stands out with its multi-racial cast.

Goss then released his fifth album, ''Wait''.  It was produced by Mike Ofca (Neverwake, Righteous B, Joe Zelek) and mastered by Randy LeRoy ([[Tim McGraw]], [[Jack Johnson (musician)|Jack Johnson]], John Prine, Relient K, Chris Thile).  The album was his first production to collaborate with his band, including drummer Liz DeRoche<ref name="I’m in two other bands, other than Tom Goss’.">{{cite web|title=Women You Should Meet: Liz DeRoche|url=http://taggmagazine.com/wysm-liz-deroche/|website=Tagg Magazine|accessdate=May 15, 2014}}</ref> (The Pushovers/Cane and the Sticks), who had remixed "I Do" the year before, vocalist Sarah Gilberg (Luray), Don Harvey and Gary Prince.  He also collaborated with singer-songwriter, and long-time friend, Matt Alber. The first single off of ''Wait'' was "Illuminate The Dark".  The accompanying video features wounded war vet turned model, Alex Minsky.  The video was deemed 'incredible' by the ''Huffington Post''.<ref>{{cite web|last1=Nichols|first1=James|title="Illuminate The Dark" New Video Released By Tom Goss|url=http://www.huffingtonpost.com/2014/05/14/illuminate-the-dark-tom-goss_n_5324269.html|website=Huffingtonpost.com|accessdate= May 4, 2014}}</ref>

Edgemedianetwork.com spoke highly of the album: "Goss has always had a strong lyric talent, describing feelings and situations in his songs that stand up as short stories in their own right. That's true here, as well, with the singer ruminating on love, life, God, and his own human limitations. The songs stretch across the emotional spectrum, from cheeky to rueful to tender; this is probably Goss' best album so far, and to hear him tell it, he's having the time of his life."<ref>{{cite web|last1=Malloy|first1=Kilian|title=Learning to ’Wait’ :: Tom Goss on His New CD|url=http://www.edgemedianetwork.com/entertainment/music/Features/157826/learning_to_’wait’_::_tom_goss_on_his_new_cd|website=Edge Media Network |accessdate= November 1, 2014}}</ref>

== Acting career ==
In 2014, Goss starred in his first feature film ''[[Out to Kill]]''.  The movie has a "whodunit" murder plot, where Goss' character "Justin Jaymes" is killed and a private investigator is hired to dig up who did it and why.<ref name="BENT-CON">{{cite web|title=BENT-CON Guest Rob Williams|url=http://bent-con.org/panel-guests/2014-guest/bent-con-guest-rob-williams/|accessdate=November 3, 2014}}</ref> ''Out to Kill'' was directed and written by independent filmmaker Rob Williams (''Long Term Relationship'', ''The Men from Next Door''), who has made a name for himself in the gay genre.<ref name="BENT-CON"/> The movie is distributed by Guest House Films.

''Out to Kill'' has been shown at the Tampa International Gay & Lesbian Film Festival, qFLIXphiladelphia, the Kansas City LGBT Film Festival, and the North Carolina Gay & Lesbian Film Festival.  It won First Prize, Alternative Spirit Award (LGBTQ) Feature, at the Rhode Island International Film Festival.  It has also toured internationally at the Korea Queer Film Festival and the KASHISH Mumbai International Queer Film Festival.

== Discography ==

===Albums===
*''Naked Without'' (2006)
*''Back to Love'' (2009)
*''Live at Terry's'' (2010) 
*''Turn It Around'' (2011)
*''Love Songs and Underdogs'' (2012)
*''Wait'' (2014)
*''What Doesn't Break'' (2016)

===EPs===
*''Rise'' (2008) 
*''Politics of Love'' (2010) (Double EP)
*''Wait (The Remixes)'' (2015)

===Singles/videography===
*"Who We Are" <small>(with Matt Alber)</small> (2010) 
*"Lover" (2010)
*"Christmas, Chicago Time"e (2010) 
*"You Know That I Love You"
*"Santa, Imma Make You Dance" (2012) 
* "Make Believe" (2012)
*"Bears" (2013) 
*"I Do (remix)" (2013) 
*"Waiting For Your Call" <small>(with Joey Salinas)</small> (2013) 
*"Festivus" <small>(with Amber Ojeda)</small> (2014)
*"I Don't Mind" <small>(feat. Max Emerson)</small> (2015) 
*"Breath and Sound" <small>(feat. Matt Alber)</small> (2015)
*"Headlights" (2015)  
*"[[Son of a Preacher Man]]" (2016)
*"Enoch (From 'Confessions')" (2016)
*"All My Life" (2016)
*"More Than Temporary" (2017)

==Filmography==
*''[[Out to Kill]]'' as Justin Jaymes
*''[[Confessions (2016 film)|Confessions]]'' as The Singer

;Soundtracks
*"You Don't Know How Hard" on film ''Out to Kill'' (2014) (plus 2 other tracks)

== Awards ==
{| class="wikitable"
|-
! Organization !! Award!! Result
|-
| ''[[The Washington Blade]]'' || Best Gay Musician 2011 || Winner<ref name="Gay DC 2011"/>
|-
| ''[[The Washington Blade]]'' || Best Gay Musician 2012 || Winner<ref name="Gay DC 2012"/>
|-
| Pride in the Arts || Favorite Male Musician || Winner
|-
| Radio Crystal Blue || Album of the Year || Winner
|}

== References ==
{{reflist|2}}

== External links ==
* {{Official website}}
* {{IMDb name|5941057}}

{{DEFAULTSORT:Goss, Tom}}
[[Category:American male singers]]
[[Category:American male songwriters]]
[[Category:1981 births]]
[[Category:Living people]]
[[Category:American singer-songwriters]]
[[Category:People from Quincy, Illinois]]
[[Category:21st-century American singers]]
[[Category:American keyboardists]]
[[Category:21st-century American male actors]]
[[Category:American pop rock singers]]
[[Category:American pop rock musicians]]
[[Category:American folk rock musicians]]
[[Category:LGBT musicians from the United States]]
[[Category:Gay musicians]]
[[Category:21st-century American guitarists]]
[[Category:Songwriters from Illinois]]
[[Category:Guitarists from Illinois]]