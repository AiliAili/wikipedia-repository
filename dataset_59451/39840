<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          =Jean Alfred Fraissinet
| image         =
| caption       =
| birth_date          = 22 June 1894
| death_date          = {{Death date and age|df=yes|1981|5|23|1894|6|22}}
| placeofburial_label =
| placeofburial =
| birth_place  =Marseilles, France
| death_place  = Cogolin, France
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    =France
| branch        =Cavalry; aviation
| serviceyears  =1914&ndash;1919; also World War II
| rank          =''[[Captain (OF-2)|Capitaine]]''
| unit          =[[Escadrille 57]]
| commands      =
| battles       =
| awards        =''[[Légion d'honneur]]'', ''[[Croix de Guerre]]'' with five ''Palmes'' and an ''Etoile de bronze''
| relations     = Family was prominent in shipping
| laterwork     = Ran a shipping line, founded and owned newspapers, became a vintner, served in French National Assembly
}}
Heir to the [[Compagnie Fraissinet]], Lieutenant (later Captain) '''Jean Alfred Fraissinet''' also served in the [[French Air Force]] during World War I, becoming a [[flying ace]] credited with eight aerial victories. Postwar, he took the helm of his family's shipping line in 1927. He founded two newspapers and bought another. He also bought the ''Château Saint-Maur'' and transformed it into a winery. The capstone of his career came in 1958, when he was elected to his nation's National Assembly as a member of the [[National Centre of Independents and Peasants]].

==Early life and ground service==
Jean Alfred Fraissinet was born in the port city of Marseilles, France on 22 June 1894. When World War I began, he joined the ranks of the French military, being assigned to the 6th Hussar Regiment on 7 September 1914. On 6 November 1914, he was promoted to the enlisted rank of [[Brigadier#France|Brigadier]]. After promotion to [[Maréchal-des-logis]] on 27 September 1915, he attended school at [[Saumur]], beginning on 26 April 1916. On 1 August 1916, he was appointed an [[Aspirant]]. He then reported to [[Dijon]] for pilot training.<ref name="OTF">''Over the Front'', p. 162.</ref>

==Aerial service in World War I==
After a further transfer to [[Chartres]] on 3 September 1916, he received his military pilot's license on 4 November 1916. He underwent advanced training before posting to Escadrille N57 on 14 March 1917.<ref name="OTF"/>{{#tag:ref|Some sources give 7 March 1917 as the date of his arrival with N57.<ref>[[Society of World War I Aero Historians]]: ''[https://books.google.com/books?id=FpfvAAAAMAAJ&q=%22Jean+Fraissinet%22+Spad&dq=%22Jean+Fraissinet%22+Spad&hl=en&ei=w3C8Tv2MGoXctgeoodS3Bg&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDIQ6AEwAQ Cross & Cockade Journal]''. Volume 26, Issue 6, page 8.</ref>|group=N}} His commissioning as a temporary ''[[sous lieutenant]]'' followed on 17 August 1917. The following month, he began his victory string.<ref name="OTF"/>{{#tag:ref|The prefix "N" denotes that the ''escadrille'' was then equipped with [[Nieuport]]s. It would subsequently re-arm with Spads and be redesignated as "Spa57".<ref name="OTF"/>|group=N}}

He won the ''[[Croix de Guerre]]'' with five ''Palmes'' and an ''Etoile de bronze'' for his gallantry. On 24 September 1918, he was promoted to lieutenant. On 1 October 1918, Fraissinet and his wingman attacked a formation of 20 German two-seaters engaged in [[close air support|trench strafing]]; Fraissinet set two of them aflame. This courageous feat was rewarded with the [[Légion d'honneur]] on 9 November 1918.<ref name="OTF"/>

==List of aerial victories==
See also [[Aerial victory standards of World War I]]
{| class="wikitable" border="1" style="margin: 1em auto 1em auto"
|-
!No.
!Date/time
!Aircraft
!Foe
!Result
!Location
!Notes<ref name="OTF"/>
|-
| align="center"| 1
| align="center"| 24 September 1917 @ 1435 hours
| [[Société Pour L'Aviation et ses Dérivés|Spad]]
| [[Albatros Flugzeugwerke|Albatros]] reconnaissance plane
| Destroyed
| [[Fort de la Pompelle]]
| Victory shared with [[Charles Nuville]] and another French pilot
|-
| align="center"| 2
| align="center"| 12 April 1918
| Spad
| [[Aviatik]] reconnaissance plane
| Destroyed
| [[Hangard]]
|
|-
| align="center"| 3
| align="center"| 2 May 1918
| Spad
| Aviatik reconnaissance plane
| Destroyed
| [[Domart-sur-la-Luce]]
|
|-
| align="center"| 4
| align="center"| 31 May 1918
| Spad
| Albatros reconnaissance plane
| Destroyed
| Étrépilly
| Victory shared with [[Marcel Nogues]] and another French pilot
|-
| align="center"| 5
| align="center"| 23 June 1918
| Spad
| [[Halberstädter Flugzeugwerke|Halberstadt]] reconnaissance plane
| Destroyed
| South of [[Reims]]
|
|-
| align="center"| 6
| align="center"| 15 July 1918 @ 0455 hours
| Spad
| Albatros reconnaissance plane
| Destroyed
| Fleury
| Victory shared with Charles Nuville and another French pilot
|-
| align="center"| 7
| align="center"| 1 October 1918 @ 1800 hours
| Spad
| Halberstadt reconnaissance plane
| Destroyed; set afire
| [[Aure, Ardennes|Aure]]
|
|-
| align="center"| 8
| align="center"| 1 October 1918 @ 1805 hours
| Spad
| Halberstadt reconnaissance plane
| Destroyed; set afire
| [[Somme-Py]]
| Victory shared with another French pilot
|}

==After World War I==
Fraissinet would not be discharged from military service until 7 October 1919.<ref name="OTF"/>

In 1927, Jean Fraissinet replaced his father Alfred as head of the [[Compagnie Fraissinet]], the family's shipping firm. Fraissinet married Mathilde Cyprien-Fabre, who was the daughter of a prominent shipping family, Compagnie Française de Navigation à Vapeur Cyprien Fabre ([[Fabre Line]]). In 1930, the two shipping firms, as well as a third line, integrated operations to mutually increase their commercial competitiveness. The following year, Fraissinet established two newspapers&ndash;''Marseille-Soir'' and ''Marseille-Matin''.<ref name="FOTW">''Les Marseillais dans l'histoire: Volume 3 of Les Hommes dans l'histoire''. Pierre Guiral, Félix Reynaud. Privat, 1988. ISBN 2-7089-9404-2, ISBN 978-2-7089-9404-1. Via [http://www.crwflags.com/fotw/flags/fr~hffra.html Fraissinet (Shipping company, France)] ''Flags of the World''. Retrieved 2011-11-01.</ref>

The threeway alliance of shipping companies lasted until 1935; then Fraissinet consolidated the Fabre Line and the Compagnie Fraissinet and abandoned the third company to its fate.<ref name="FOTW"/>

On 23 May 1937, he bought the historic ''Château Saint-Maur'' and its grounds from Sir Henry Laurens, the wealthy tobacconist. Fraissinet turned the property from its former agricultural uses of growing wheat, beets, and cotton<ref>[http://www.chateausaintmaur.com/us/domaine/proprietaire.html Château Saint-Maur: The Landowners]. Château Saint-Maur. Retrieved 2011-11-01.</ref> into vineyards.<ref>[http://www.chateausaintmaur.com/us/domaine/culture.html Château Saint-Maur: The Farm]. Château Saint-Maur. Retrieved 2011-11-01.</ref>

Fraissinet returned to his country's service in World War II, rising to the rank of captain.<ref name="OTF"/> Following the war, in 1947 he bought another newspaper, ''[[La Provence|Le Méridional]]''. In 1952, he merged it with ''Sud-Est'' and ''La France de Marseille''.<ref name="FOTW"/>

As a culmination of the right wing nationalist views he espoused in his newspapers, he was elected to the [[National Assembly of France]],<ref name="FOTW"/> as a member of the [[National Centre of Independents and Peasants]] party, on 30 November 1958.<ref name="assemblee">[http://www.assemblee-nationale.fr/histoire/trombinoscope/Vrepublique/Legis01/fraissinet-jean-22061894.asp Jean Fraissinet]. Assemblee Nationale. Retrieved 2011-11-01. Translation via Babelfish.</ref> Jean Fraissinet passed direction of the family concern to his son Roland.<ref name="FOTW"/> During Jean Fraissinet's four years in political office, he was a staunch advocate of retaining a [[French Algeria]], thus being opposed to [[Charles de Gaulle]]'s policies.<ref name="FOTW"/> Fraissinet's term ended on 9 October 1962;<ref name="assemblee"/> he was not re-elected.<ref name="FOTW"/>

The Fraissinet family shipping company docked its last ship in 1968, and withdrew entirely from the shipping business in 1974, ending 138 years of maritime service.<ref name="FOTW"/>

Jean Fraissinet died in 1981 leaving behind him three children (Roland Fraissinet, Regis Fraissinet, Nadine Patricot-Fraissinet) and eight grandchildren (Hadrien Fraissinet, Eric Fraissinet, Marc Fraissinet, Dora Fraissinet, Ines Fraissinet, Philippe Patricot, Chantal Patricot, Noel Patricot).<ref name="FOTW"/>

==References==
;Notes
{{Reflist|group=N}}
;Citations
{{Reflist}}
;Bibliography
{{refbegin}}
* ''Over the Front: A Complete Record of the Fighter Aces and Units of the United States and French Air Services, 1914-1918'' Norman L. R. Franks, Frank W. Bailey. Grub Street, 1992. ISBN 0-948817-54-2, ISBN 978-0-948817-54-0.
{{refend}}

{{wwi-air}}

{{DEFAULTSORT:Fraissinet, Jean Alfred}}
[[Category:French World War I flying aces]]
[[Category:1894 births]]
[[Category:1981 deaths]]
[[Category:People from Marseille]]
[[Category:French military personnel of World War II]]
[[Category:French politicians]]