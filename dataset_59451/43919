<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R-MAX
 | image=YamahaRMax.jpg
 | caption= An R-MAX in flight in 2014.
}}{{Infobox Aircraft Type
 | type=[[Unmanned aerial vehicle]]
 | national origin=[[Japan]]
 | manufacturer=[[Yamaha Motor Company]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= Yamaha R-50
 | variants with their own articles=
}}
|}

The '''Yamaha R-MAX''' is a Japanese [[unmanned aerial vehicle|unmanned]] [[helicopter]] developed by the [[Yamaha Motor Company]] in the 1990s. The two-bladed, [[gasoline]]-powered aircraft is remote-controlled by a line-of-sight user; it was designed primarily for agricultural use, and is capable of precise [[cropdusting|aerial spraying]] of crops. The R-MAX has been used in Japan and abroad for agriculture and a variety of other roles, including [[reconnaissance]], disaster response and technology development.

==Development==
The Yamaha R-MAX and its predecessor, the Yamaha R-50, were developed in the 1990s in response to demand in the Japanese market for aerial agricultural spraying. Fixed-wing manned [[crop duster]]s had been in use in Japan for many years, but the small size of most Japanese farms meant that this method was inefficient.  Manned helicopters were sometimes used for spraying, but were very expensive. The R-MAX allowed much more precise small-scale spraying, at a lower cost and lower risk than manned aircraft.<ref name="sato">Sato, Akira. [http://www.dtic.mil/dtic/tr/fulltext/u2/a427393.pdf "The R-MAX Helicopter UAV" (PDF)]. DTIC.mil. 2 September 2003. Retrieved 30 October 2014.</ref> The R-MAX was approved for operation in the [[United States]] by the [[Federal Aviation Administration]] in 2015.<ref name=vert2015-05>{{cite news |first=Elan |last=Head |url=http://www.verticalmag.com/news/article/FAAgrantsexemptiontounmannedYamahaRMAXhelicopter |title=FAA grants exemption to unmanned Yamaha RMAX helicopter |work=Vertical Magazine |date=6 May 2015 |accessdate=6 May 2015 |deadurl=no}}</ref><ref>[http://www.faa.gov/uas/legislative_programs/section_333/333_authorizations/media/Yamaha_11448.pdf "R-MAX authorisation" (PDF)]. [[FAA]]. 2015. Retrieved 7 August 2015.</ref>

==Operational history==
{{asof|2015}}, the R-MAX fleet has conducted over two million [[flight hour]]s in agricultural roles and several other capacities, including aerial sensing, photography, academic research, and military applications.<ref name=vert2015-05/>

===Volcano observation===
In the spring of 2000, the Japanese government requested the use of an R-MAX to observe the eruption of [[Mount Usu]], which had been dormant for 22 years, as close observation of the volcano was deemed too dangerous for manned helicopters. The R-MAX allowed scientific observers to spot and measure build-ups of [[volcanic ash]] which would have otherwise been missed, and improved the observers' ability to predict  hazardous volcanic [[mudslide]]s.<ref name="sato" />

===Fukushima nuclear disaster===
Yamaha R-MAXs were used in the wake of the [[2011 Tōhoku earthquake and tsunami]] to monitor radiation levels around the site of the [[Fukushima nuclear disaster]] from inside the "no-entry" zone.<ref>[http://global.yamaha-motor.com/ymgn/news_letter/2013/pdf/2013_11_monthly-newsletter.pdf "Yamaha Motors Monthly Newsletter" (PDF)]. [[Yamaha Motor Company]]. 18 November 2013. Retrieved 31 October 2014.</ref>

===Research===
The R-MAX has been used by several universities worldwide for guidance and automatic control research. In 2002, [[Georgia Tech]]'s [http://uavrf.gatech.edu/ UAV Research Facility] began using a Yamaha R-MAX for research into autonomous aerial guidance, navigation, and control systems.<ref>[https://smartech.gatech.edu/handle/1853/35877 "System Integration and Operation of a Research Unmanned Aerial Vehicle"]. Eric N. Johnson, Daniel P. Schrage. ''Journal of Aerospace Computing, Information, and Communication'' 1(1):5-18. January 2004. Retrieved 19 August 2015.</ref> [[Carnegie Mellon University]], the [[University of California Berkeley]], [[UC Davis]] and [[Virginia Tech]] have also used R-MAX units for research.<ref>[http://robotics.eecs.berkeley.edu/bear/testbeds.html Berkeley Aerobot Team website]. Retrieved 19 August 2015.</ref><ref>[https://www.youtube.com/watch?v=Skdh4Nwm6r4 RMax Helicopter Obstacle Avoidance] via [[YouTube]]. Retrieved 19 August 2015.</ref><ref>Evan Ackerman. [http://spectrum.ieee.org/automaton/robotics/aerial-robots/yamaha-demos-agricultural-robocopter "Yamaha Demos Agricultural RoboCopter, But Humans Can't Unleash It Yet"]. ''IEEE Spectrum.'' 16 October 2014. Retrieved 19 August 2015.</ref><ref>{{cite web|url=https://www.eng.vt.edu/news/virginia-tech-engineering-team-developing-helicopter-would-investigate-nuclear-disasters|title=Virginia Tech engineering team developing helicopter that would investigate nuclear disasters|publisher=[[Virginia Tech]] College of Engineering|date=23 March 2010|accessdate=22 August 2015}}</ref>

==Variants==
In May 2014, Yamaha agreed to partner with the American defense firm [[Northrop Grumman]] to produce a variant of the R-MAX, known as the [http://www.northropgrumman.com/Capabilities/RBat/Pages/default.aspx R-Bat], combining the R-MAX airframe with a suite of autonomy-enabling hardware and software. This product is intended for both military and civilian applications.<ref>[http://www.globenewswire.com/newsarchive/noc/press/pages/news_releases.html?d=10080761 "Photo Release -- Northrop Grumman, Yamaha Motor, U.S.A., Collaborate on Unmanned Helicopter System"]. [[Northrop Grumman]] via GlobeNewsWire.com. 8 May 2014. Retrieved 12 August 2015.</ref>

== Specifications (R-MAX) ==
{{Aircraft specs
|ref=<!-- reference -->
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=3.63
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=0.72
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=1.08
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=64
|empty weight lb=141<ref name=vert2015-05/>
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=94
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
* '''Maximum payload:''' {{convert|28|-|31|kg|lb|abbr=on}}
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=water-cooled 
|eng1 type=2-cylinder 2-stroke, {{convert|0.246|l|cuin|abbr=on|2}} 
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=1
|rot dia m=3.115
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=2-bladed
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=1 hour
|ceiling m=
|ceiling ft=
|ceing note=
|g lits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb te ms=
|climb rate ftmin=
|climb rate e=
|time to altitude=
|sink rate ms=<!-- slplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate n
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=

|avionics=
* '''Control system:''' Yamaha Attitude Control System (YACS)
}}

==References==
{{reflist}}
{{Q-UAVs}}

[[Category:Unmanned helicopters]]
[[Category:Unmanned aerial vehicles of Japan]]
[[Category:Agricultural robotics]]
[[Category:Airborne military robots]]