{{Infobox journal
| title = The International Journal of Aviation Psychology
| cover = [[File:Cover of v23(3) of Int J Aviat Psychol.jpg]]
| editor = Dennis B. Beringer 
| discipline = [[Aviation]]
| formernames =,,
| abbreviation = Int. J. Aviat. Psychol.
| publisher = [[Taylor & Francis]] on behalf of the [[Association of Aviation Psychology]]
| country =
| frequency = Quarterly
| history = 1991–present
| openaccess =
| license =
| impact = 0.167
| impact-year = 2012
| website = http://www.tandfonline.com/action/aboutThisJournal?journalCode=hiap20
| link1 = http://www.tandfonline.com/toc/hiap20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/hiap20
| link2-name = Online archive
| JSTOR =
| OCLC = 45007196
| LCCN = 91640751
| CODEN =
| ISSN = 1050-8414
| eISSN = 1532-7108
}}
'''''The International Journal of Aviation Psychology''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] covering research on the "development and management of safe, effective aviation systems from the standpoint of the human operators."<ref name = aims>{{cite web |url=http://www.tandfonline.com/action/aboutThisJournal?show=aimsScope&journalCode=hiap20 |title=International Journal of Aviation Psychology – Aims and Scope |accessdate=July 10, 2013 |publisher=[[Taylor & Francis]]}}</ref> It draws on aspects of the academic disciplines of [[engineering]] and [[computer science]], [[psychology]], [[education]], and [[physiology]]. It was established in 1991 and is published by [[Taylor and Francis]] on behalf of the [[Association of Aviation Psychology]]. The [[editor-in-chief]] is Dennis B. Beringer.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Current Contents]]/Social & Behavioral Sciences
* [[PsycINFO]]
* [[PubMed]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[CSA (database company)|CSA databases]]
* [[Academic Search Premier]]
* [[ProQuest]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.167, ranking it 71st out of 72 journals in the category "Applied Psychology".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Applied Psychology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Science |series=[[Web of Science]] |postscript=.}}</ref>

== Notable articles ==
According to the [[Web of Science]], the journal's most cited paper is:
* {{Cite journal|title=The Evolution of Crew Resource Management Training in Commercial Aviation|author1 = Helmreich, R. L.|author2 = Merritt, A. C.|author3 = Wilhelm, J. A.|journal = Int. J. Aviat. Psychol.|year = 1999|volume = 9|issue = 1|pages = 19–32|doi = 10.1207/s15327108ijap0901_2|pmid = 11541445|url = http://homepage.psy.utexas.edu/homepage/group/helmreichlab/publications/pubfiles/Pub235.pdf}} ({{As of|July 2013}} cited over 200 times)

==References==
<references />

==External links==
* {{Official website|1=http://www.tandfonline.com/action/aboutThisJournal?journalCode=hiap20}}
* [http://www.avpsych.org Association of Aviation Psychology]

{{DEFAULTSORT:International Journal of Aviation Psychology, The}}
[[Category:Quarterly journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]
[[Category:Applied psychology journals]]
[[Category:Taylor & Francis academic journals]]