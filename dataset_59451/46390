{{Infobox scientist
| name              = Robert Joseph Bandoni
| image             = Robert_Joseph_Bandoni.jpg
| caption           = 
| birth_date        = {{Birth date|1926|11|09}}
| birth_place       = [[Weeks, Nevada]]
| death_date        = {{Death date and age|2009|05|18|1926|11|09}}
| death_place       = Vancouver, British Columbia, Canada
| residence         = 
| citizenship       = 
| nationality       = 
| fields            = Mycology
| workplaces        = 
| alma_mater        = [[University of Nevada, Reno|University of Nevada]]
| thesis_title      = Taxonomic studies of the genus ''Tremella'' (Tremellaceae)
| thesis_url        = 
| thesis_year       = 1957
| doctoral_advisor  = [[George Willard Martin]]
| notable_students  = 
| known_for         = Taxonomy of the [[Heterobasidiomycetes]]
| author_abbrev_bot = Bandoni
}}

'''Robert Joseph Bandoni''' (11 November 1926&nbsp;– 5 May 2009) was a [[mycologist]] who specialized on the taxonomy and morphology of the [[heterobasidiomycetes]] (“jelly fungi”).<ref name="Ginns 2011"/>

Bandoni died on May 18, 2009, in Vancouver, British Columbia, after suffering a stroke.<ref name="Gazette">{{cite news|title=Robert Joseph Bandoni|newspaper=Reno Gazette-Journal|date=June 9, 2009}}</ref> During his 50 years as professor at the [[University of British Columbia]],<ref name="BEN">{{cite journal|last=Ginns|first=Jim|title=Mycologist Extraordinaire Robert J. Bandoni (November 9, 1926-May 18, 2009)|journal=Botanical Electronic News|date=209|volume=409|url=http://www.ou.edu/cas/botany-micro/ben/ben409.html}}</ref> he wrote over 80 scientific publications as well as several books. He was awarded the title of Professor Emeritus in 1989.<ref name="BEN" /> In 1990 the Canadian Botanical Association awarded him with the prestigious George Lawson Medal.<ref name="CBA" />

== Biography ==
Robert "Bob" Joseph Bandoni was born November 9, 1926 in Weeks, Nevada to Giuseppe and Albina Bandoni.<ref name="Ginns 2011">{{cite journal |last=Ginns|first=Jim|title=Robert Joseph Bandoni, 1926–2009 |journal=Mycologia |year=2011|volume=103 |issue=5 |pages=1156–1158 |doi=10.3852/11-050}}</ref>  He went to high school in [[Hawthorne, Nevada]] and received a Bachelor of Science from the [[University of Nevada, Reno|University of Nevada]] in 1953.<ref name="Ginns 2011"/> He then studied under [[George Willard Martin]] at the [[University of Iowa]] and received his Ph.D. in 1957.<ref name="Ginns 2011"/> The title of his thesis was: "Taxonomic studies of the genus ''Tremella'' (Tremellaceae).<ref name="Ginns 2011"/> That same year, he received the Gertrude S. Burlingham Fellowship at the [[New York Botanical Garden]] and became assistant professor of Botany at the [[University of Wichita]].<ref name="Ginns 2011"/> In 1958 Bandoni joined the Botany faculty at the [[University of British Columbia]] (UBC). He helped found the [http://www.vanmyco.com/ Vancouver Mycological Society] in the 1970s. He was awarded the status of professor emeritus in 1989.<ref name="Ginns 2011"/> Bandoni died on May 18, 2009 from a stroke.<ref name="Ginns 2011"/>

== Mycological and botanical contributions ==
Bandoni mainly studied the [[heterobasidiomycetes]], commonly known as the "Jelly Fungi". His early publications were of a classical taxonomical nature, writing descriptions of the macro- and micro-morphology via the light microscope.<ref name=BEN /> In the early 1970s Bandoni started studying aquatic fungi that occur in terrestrial environments.<ref name=BEN /> He published a paper in the journal ''[[Science (journal)|Science]]'' that explained how spores can travel upwards on a monolayer of water on a leaf surface.<ref>{{cite journal|last=Bandoni|first=Robert J |author2=Koske, Richard E  |title=Monolayers and Microbial Dispersal |journal=Science |year=1974|volume=183 |issue=4129 |pages=1079–1081|doi=10.1126/science.183.4129.1079}}</ref> In the 1980s Bandoni collaborated with Franz Oberwinkler and colleagues at the [[Universität Tübingen]] to produce over 20 papers on the Jelly Fungi and allies. They described new species, genera, families and orders. They used TEM and SEM micrographs to investigate the significance of septal pore structures, began revising the classical phylogenetic classification and to demonstrate parasitism between Jelly Fungi and other fungi.<ref name=BEN /> Bandoni collaborated with mycologists in Japan and other Asian countries. He was research fellow in 1983 at the [[University of Tsukuba]] and spent a sabbatical year at the [[University of Osaka]].<ref name="Ginns 2011" /> He wrote two field guides to Thailand mushrooms.<ref name="Ginns 2011" /> Bandoni contributed to multiple publications on taxonomy in the Jelly Fungi up until his death.

In addition to scientific publications, Bandoni also contributed to several influential books. He contributed to ''Plant diversity: An evolutionary approach''; a book written by the UBC Botany Faculty that "put the Botany Department onto the North American textbook bestseller list for almost a decade".<ref name="Ginns 2011" /> He also published the 570 page textbook ''Nonvascular plants: An evolutionary survey''. In 1964 he co-authored with [[Adam Szczawinski]] a field guide titled ''Guide to common mushrooms of British Columbia''.

== Taxa described ==
{{div col|colwidth=33em}}
* ''[[Achroomyces abditus]]''	(Bandoni) Hauerslev	1993
* ''[[Agaricostilbum hyphaenes]]''	(Har. & Pat.) Oberw. & Bandoni	1982
* ''[[Aleurodiscus gigasporus]]''	Ginns & Bandoni	1991
* ''[[Aleurodiscus subglobosporus]]''	Ginns & Bandoni	1991
* ''[[Atractiella columbiana]]''	Bandoni & Inderb.	2002
* ''[[Atractiella delectans]]''	(Möller) Oberw. & Bandoni	1982
* ''[[Atractiella solani]]''	(Cohn & J. Schröt.) Oberw. & Bandoni	1982
* ''[[Atractiellales]]''	Oberw. & Bandoni	1982
* ''[[Atractogloea]]''	Oberw. & Bandoni	1982
* ''[[Atractogloea stillata]]''	Oberw. & Bandoni	1982
* ''[[Bullera aurantiaca]]''	B.N. Johri & Bandoni	1984
* ''[[Bullera globispora]]''	B.N. Johri & Bandoni	1984
* ''[[Bullera miyagiana]]''	Nakase, Itoh, Takem. & Bandoni	1990
* ''[[Bullera salicina]]''	B.N. Johri & Bandoni	1984
* ''[[Calacogloea]]''	Oberw. & Bandoni	1991
* ''[[Calacogloea peniophorae]]''	Oberw. & Bandoni	1991
* ''[[Carcinomyces]]''	Oberw. & Bandoni	1982
* ''[[Carcinomyces effibulatus]]''	(Ginns & Sunhede) Oberw. & Bandoni	1982
* ''[[Carcinomyces mycetophilus]]''	(Peck) Oberw. & Bandoni	1982
* ''[[Carcinomycetaceae]]''	Oberw. & Bandoni	1982
* ''[[Chionosphaera phylacicola]]''	(Seifert & Bandoni) R. Kirschner & Oberw.	2001
* ''[[Chionosphaera phylaciicola]]''	(Seifert & Bandoni) R. Kirschner & Oberw.	2001
* ''[[Chionosphaeraceae]]''	Oberw. & Bandoni	1982
* ''[[Christiansenia subgen. Carcinomyces]]''	(Oberw. & Bandoni) F. Rath	1991
* ''[[Cladoconidium]]''	Bandoni & Tubaki	1985
* ''[[Cladoconidium articulatum]]''	Bandoni & Tubaki	1985
* ''[[Colacogloea]]''	Oberw. & Bandoni	1991
* ''[[Colacogloea allantospora]]''	Ginns & Bandoni	2002
* ''[[Colacogloea peniophorae]]''	(Bourdot & Galzin) Oberw. & Bandoni	1991
* ''[[Cystofilobasidiaceae]]''	K. Wells & Bandoni	2001
* ''[[Cystofilobasidium]]''	Oberw. & Bandoni	1983
* ''[[Cystofilobasidium bisporidii]]''	(Fell, I.L. Hunter & Tallman) Oberw. & Bandoni	1983
* ''[[Cystofilobasidium bisporidiis]]''	(Fell, I.L. Hunter & Tallman) Oberw. & Bandoni	1983
* ''[[Cystofilobasidium capitatum]]''	(Fell, I.L. Hunter & Tallman) Oberw. & Bandoni	1983
* ''[[Dacrymyces aquaticus]]''	Bandoni & G.C. Hughes	1984
* ''[[Dioszegia aurantiaca]]''	(B.N. Johri & Bandoni) M. Takash., T. Deák & Nakase	2001
* ''[[Entomocorticium]]''	H.S. Whitney, Bandoni & Oberw.	1987
* ''[[Entomocorticium dendroctoni]]''	H.S. Whitney, Bandoni & Oberw.	1987
* ''[[Exidiopsis paniculata]]''	K. Wells & Bandoni	1987
* ''[[Exidiopsis punicea]]''	K. Wells & Bandoni	1987
* ''[[Fibulobasidium]]''	Bandoni	1979
* ''[[Fibulobasidium inconspicuum]]''	Bandoni	1979
* ''[[Fibulobasidium sirobasidioides]]''	Bandoni	1998
* ''[[Fibulostilbum phylacicola]]''	Seifert & Bandoni	1992
* ''[[Fibulostilbum phylaciicola]]''	Seifert & Bandoni	1992
* ''[[Filobasidium elegans]]''	Bandoni & Oberw.	1991
* ''[[Filobasidium globisporum]]''	Bandoni & Oberw.	1991
* ''[[Galzinia culmigena]]''	(R.K. Webster & D.A. Reid) B.N. Johri & Bandoni	1975
* ''[[Helicobasidium corticioides]]''	Bandoni	1955
* ''[[Herpobasidium australe]]''	Oberw. & Bandoni	1984
* ''[[Ingoldiella nutans]]''	Bandoni & Marvanová	1989
* ''[[Insolibasidium]]''	Oberw. & Bandoni	1984
* ''[[Insolibasidium deformans]]''	(C.J. Gould) Oberw. & Bandoni	1984
* ''[[Mycogloea amethystina]]''	Bandoni	1998
* ''[[Mycogloea bullatospora]]''	Bandoni	1998
* ''[[Mycogloea nipponica]]''	Bandoni	1998
* ''[[Myxarium allantosporum]]''	K. Wells & Bandoni	2004
* ''[[Naiadella]]''	Marvanová & Bandoni	1987
* ''[[Naiadella fluitans]]''	Marvanová & Bandoni	1987
* ''[[Platygloea abdita]]''	Bandoni	1959
* ''[[Platygloea jacksonii]]''	Bandoni & J.C. Krug	2000
* ''[[Pleotrachelus itersoniliae]]''	(D.J.S. Barr & Bandoni) M.W. Dick	2001
* ''[[Pseudozyma]]''	Bandoni emend. Boekhout	1985
* ''[[Pseudozyma prolifica]]''	Bandoni	1985
* ''[[Ptechetelium]]''	Oberw. & Bandoni	1984
* ''[[Ptechetelium cyatheae]]''	(Syd.) Oberw. & Bandoni	1984
* ''[[Rozella itersoniliae]]''	D.J.S. Barr & Bandoni	1980
* ''[[Sigmogloea]]''	Bandoni & J.C. Krug	2000
* ''[[Sigmogloea tremelloidea]]''	Bandoni & J.C. Krug	2000
* ''[[Sirotrema]]''	Bandoni	1986
* ''[[Sirotrema parvula]]''	Bandoni	1986
* ''[[Sirotrema pusilla]]''	Bandoni	1986
* ''[[Sirotrema translucens]]''	(H.D. Gordon) Bandoni	1986
* ''[[Sporobolomyces lactophilus]]''	Nakase, Itoh, M. Suzuki & Bandoni	1990
* ''[[Sporobolomyces salicinus]]''	(B.N. Johri & Bandoni) Nakase & Itoh	1988
* ''[[Stilbotulasnella]]''	Oberw. & Bandoni	1982
* ''[[Stilbotulasnella conidiophora]]''	Bandoni & Oberw.	1982
* ''[[Tetragoniomyces]]''	Oberw. & Bandoni	1981
* ''[[Tetragoniomyces uliginosus]]''	(P. Karst.) Oberw. & Bandoni	1981
* ''[[Tetragoniomycetaceae]]''	Oberw. & Bandoni	1981
* ''[[Tilletiaria]]''	Bandoni & B.N. Johri	1972
* ''[[Tilletiaria anomala]]''	Bandoni & B.N. Johri	1972
* ''[[Tremella armeniaca]]''	Bandoni & J. Carranza	1997
* ''[[Tremella aurantialba]]''	Bandoni & M. Zang	1990
* ''[[Tremella guttaeformis]]''	(Berk. & Broome) Bandoni	1961
* ''[[Tremella guttiformis]]''	(Berk. & Broome) Bandoni	1961
* ''[[Tremella lilacea]]''	Bandoni & J. Carranza	1997
* ''[[Tremella mesenterella]]''	Bandoni & Ginns	1999
* ''[[Tremella nigrifacta]]''	Bandoni & J. Carranza	1997
* ''[[Tremella phaeographidis]]''	Diederich, Coppins & Bandoni	1996
* ''[[Tremella roseolutescens]]''	Bandoni & J. Carranza	1997
* ''[[Tremella subencephala]]''	Bandoni & Ginns	1993
* ''[[Tremellina]]''	Bandoni	1986
* ''[[Tremellina pyrenophila]]''	Bandoni	1986
* ''[[Trimorphomyces]]''	Bandoni & Oberw.	1983
* ''[[Trimorphomyces papilionaceus]]''	Oberw. & Bandoni	1983
{{div col end}}

== Honors and memberships ==
* Gertrude S. Burlingham Fellowship<ref name="Ginns 2011"/>
* George Lawson Medal<ref name=CBA>{{cite web|last=Canadian Botanical Association|title=Past Recipients of the Lawson Medal|url=http://www.cba-abc.ca/AwardRes.htm|accessdate=6 February 2014}}</ref>
* Mycological Society of Japan
* Mycological Society of America

== Eponymous taxa ==
* Bandoniozyma<ref>{{cite journal|last=Valente|first=Patricia |author2=Patricia Valente |author3=Teun Boekhout |author4=Melissa Fontes Landell |author5=Juliana Crestani |author6=Fernando Carlos Pagnocca |author7=Lara Durães Sette |author8=Michel Rodrigo Zambrano Passarini |author9=Carlos Augusto Rosa |author10=Luciana R. Brandão |author11=Raphael S. Pimenta |author12=José Roberto Ribeiro |author13=Karina Marques Garcia |author14=Ching-Fu Lee |author15=Sung-Oui Suh |author16=Gábor Péter |author17=Dénes Dlauchy |author18=Jack W. Fell |author19=Gloria Scorzetti |author20=Bart Theelen |author21=Marilene H. Vainstein |title=Bandoniozyma gen. nov., a genus of fermentative and non-fermentative tremellaceous yeast species|journal=PLOS ONE|year=2012|volume=7|issue=10|pages=e46060|doi=10.1371/journal.pone.0046060}}</ref>
{{botanist|Bandoni}}

==See also==
*[[List of mycologists]]

== References ==
{{Reflist}}

{{Authority control}}

{{DEFAULTSORT:Bandoni, Robert Joseph}}
[[Category:Canadian mycologists]]
[[Category:1926 births]]
[[Category:2009 deaths]]
[[Category:University of Nevada alumni]]
[[Category:University of Iowa alumni]]
[[Category:University of British Columbia faculty]]