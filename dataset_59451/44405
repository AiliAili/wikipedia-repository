{{about|the airfield in Antarctica|the former United States Air Force base|Williams Air Force Base}}
{{Infobox airport
| name         = Williams Field
| image        = C-130 South Pole landing.jpg
| IATA         = none
| ICAO         = NZWD
| pushpin_map            = Antarctica
| pushpin_map_caption    = Location of airfield in Antarctica</small>
| pushpin_label          = NZWD
| pushpin_label_position = right
| type         = Public
| owner        = 
| operator     = 
| city-served  = 
| location     = [[McMurdo Station]], [[Antarctica]]
| elevation-f  = 68
| elevation-m  = 21
| coordinates = {{coord|77|52|03|S|167|03|24|E|type:airport|display=title, inline}}
| website      = 
| r1-number    = 07/25
| r1-length-f  = 10,000
| r1-length-m  = 3,048
| r1-surface   = [[Snow]]
| r2-number    = 15/33
| r2-length-f  = 10,000
| r2-length-m  = 3,048
| r2-surface   = [[Snow]]
| footnotes    = Source: [[DAFIF]]<ref name=WAD>{{WAD|NZWD|source=[[DAFIF]]}}</ref><ref name=GCM>{{GCM|NZWD|source=[[DAFIF]]}}</ref>
}}

'''Williams Field''' or '''Willy Field''' {{Airport codes||NZWD}} is a [[United States Antarctic Program]] airfield in [[Antarctica]]. Williams Field consists of two snow runways located on approximately 8 meters (25&nbsp;ft) of compacted snow, lying on top of 8–10&nbsp;ft of ice,<ref>{{cite web|last1=Minneci|first1=Beth|url=https://antarcticsun.usap.gov/pastIssues/2000-2001/2000_12_17.pdf|website=AntarcticSun.USAP.gov|accessdate=7 November 2016}}</ref> floating over 550 meters (1,800&nbsp;ft) of water.<ref>[http://photolibrary.usap.gov/index2.htm Antarctic Photo Library], U.S. Antarctic Program; National Science Foundation.</ref> The airport, which is approximately seven miles from [[Ross Island]], serves [[McMurdo Station]] and [[New Zealand]]’s [[Scott Base]]. Until the 2009-10 summer season, Williams was the major airfield for on-continent aircraft operations in Antarctica.

Williams Field is named in honor of [[Richard T. Williams]], a [[United States Navy]] equipment operator who drowned when his [[Caterpillar D8|D-8]] tractor broke through the ice on January 6, 1956. Williams and other personnel were participants in the first [[Operation Deep Freeze]], a [[U.S. military]] mission to build a permanent science research station at McMurdo Station in anticipation of the [[International Geophysical Year]] 1957–58.

== Operation ==
[[Image:Williamsfieldpatch2.jpg|thumb|left|100px]]
[[Image:Cat Challengers at Williams Field Antarctica.jpg|thumb|right|300px|Caterpillar Challenger machines perform constant runway grooming]]

The skiway was typically in operation from December through to the end of February. Other McMurdo Station airfields include the [[Ice Runway]] (October to December) and [[Pegasus Field]] used in August and December through to February of each season.<ref>[http://www.usap.gov/USAPgov/travelAndDeployment/documents/ParticipantGuide-Chapter7.pdf "Chapter 7: Stations and Ships"], ''2010-2012 USAP Participant Guide'']. United States Antarctica Program, May 2010.</ref>

The Williams Field snow runway is known locally as "Willy's Field." The airfield is a groomed snow surface that can support ski-equipped aircraft landings only. A cluster of facilities for flight operations, referred to as "Willy Town," includes several rows of containers for workers and a galley. Some of the buildings are relocated to support flight operations at the nearby Ice Runway and distant Pegasus Field. Willy Field Tavern, a bar at the airfield, closed in 1994.{{citation needed|date=June 2009}}

Aviation fuel at Williams Field is pumped in a 16&nbsp;km (10&nbsp;mi) flexible pipe from McMurdo Station. Fuel is stored in up to 12 tanks. The fuel tanks, like other structures at the airfield, are mounted on skis or runners for portability.<ref>[http://www.oil-spill-info.com/Publications/Antarctic%20CPlan%202001%20IOSC.pdf Planning and Hazards of Spill Response in Antarctica.] Erich R. Gundlach, E-Tech International, Inc.; John J. Gallagher
Gallagher, Marine Systems, Inc.; John Hatcher and Tom Vinson, Raytheon Polar Services Company. 2001 International Oil Spill Conference.</ref>  Generator and heating fuel is delivered to the station by fuel trucks from [[McMurdo Station]], with fuels stored at the individual structures.

The extraordinary conditions encountered at Williams Field include the fact that the airfield is in a continuous slow slide towards the sea. Seaward movement of the floating [[McMurdo Ice Shelf]] upon which the airfield is constructed has forced Williams Field to be relocated three times since its original construction. Workers last moved the airfield during the 1984-85 season.<ref>[http://www.nsf.gov/pubs/stis1993/opp94005/opp94005.txt ''Berthing at McMurdo for Williams Field''], Office of Polar Programs; [[National Science Foundation]]. August 19, 1993.</ref> Subsequently, personnel housed at Williams lived in buildings constructed on sleds to facilitate relocation. In the past, up to 450 people were housed at the airfield, according to the National Science Foundation.  In 1994 the National Science Foundation constructed two dorm buildings at McMurdo Station and began transporting personnel to Williams Field using various vehicles including Foremost Delta II and Ford E-350 vans.{{citation needed|date=June 2009}}

==Current Aircraft in Use==
*[[Lockheed LC-130]] - [[New York Air National Guard]]
*[[Basler BT-67]] - [[Kenn Borek Air]]
*[[de Havilland Canada DHC-6 Twin Otter]] - [[Kenn Borek Air]]

==Historical Notes==
[[Image:Williams_Field_Antarctica.jpg|thumb|500px|right|Williams Field support structures as seen from the cargo line]] 
*1957: Pan American [[Boeing 377 Stratocruiser]] makes round trip from Christchurch to McMurdo Sound. First civilian flight to Antarctica.
*1960: U.S. Navy WV-2 BuNo 126513 crashes after landing short of the ice runway.
*1960: First ski-equipped [[C-130 Hercules]] cargo aircraft lands in Antarctica.
*1960: Sunspots knock out radio communications for eight days, forcing cancellation of all flights between New Zealand and McMurdo.
*1966: First all-jet aircraft (USAF-C-141) lands at Williams.
*1967: Earliest scheduled winter fly-in.
*1970: U.S. Navy "Pegasus" C-121J crash lands. Aircraft is destroyed but no fatalities among the 80 persons aboard. [[Pegasus Field]] is named after this aircraft.
*1979: [[Air New Zealand Flight 901]] crashes on nearby [[Mt Erebus]]. 257 people die

==See also==
*[[Blue ice runway]]
*[[Marble Point]]
*[[McMurdo Sound]]
*[[McMurdo Station]]
*[[Pegasus Field]]
*[[Ice Runway]]

==Notes==
<references/>

==References==
*Change of Command pamphlet. U.S. Naval Support Force Antarctica; June 10, 1991.
*Clarke, Peter; ''On the Ice''. Rand McNally & Company, 1966.
*[http://www.thenewstribune.com/news/military/story/6242281p-5444941c.html''Ice can give airmen that sinking feeling,''] The NewsTribune.com. Tacoma, Wash.; November 20, 2006.
*[http://www.conniesurvivors.com/1-1960_mcmurdo_article.htm McMurdo 1960 Crash]
*United States Antarctic Research Program Calendars: 1983, 1985.
*[http://www.thenewstribune.com/news/military/story/6242281p-5450215c.html'' Where danger and wonder collide''], The NewsTribune.com. Tacoma, Wash.; November 20, 2006.

== External links ==
*[http://www.webcitation.org/query?url=http://www.geocities.com/coolrunnernz/Transport/AIRCRAFT/Airplanes.htm&date=2009-10-25+17:51:36 Aircraft of Antarctica]
*[http://www.irmahale.com/1999j.html ''Moving the Airport,''] December 21, 1999.
*[http://stratocat.com.ar/bases/41.htm List of stratospheric balloon launches ] under NASA's Long Duration Balloon program
*{{NWS-current|NZWD}}

[[Category:Airports in the Ross Dependency]]
[[Category:McMurdo Station]]
[[Category:Coastal construction]]