{{Use British English|date=February 2013}}
{{Use dmy dates|date=February 2013}}
{{Infobox cricketer
|          name = Abhimanyu Rajp
|               image =
|             country = United States
|            fullname = Abhimanyu Rajp
|            nickname = Abhi
|          birth_date = {{Birth date and age|1986|3|08|df=yes}}
| birth_place = [[Ludhiana]], [[Punjab, India]]
|         nationality = American
|           height_ft = 6
|           height_in = 0
|            heightft = 
|          heightinch = 
|             heightm = 
|             batting = Right-handed
|             bowling = Right-arm [[Off Spin|Off-spin]]
|                role = [[Bowler (cricket)|Bowler]]
|       international = true
|               club1 = 
|               year1 = 
|          deliveries = balls
|             columns = 3
|             column1 = [[List-A cricket|LA]]
|            matches1 = 
|               runs1 = 
|            bat avg1 = 
|           100s/50s1 = –/–
|          top score1 = 
|         deliveries1 = 
|            wickets1 = 
|           bowl avg1 = 
|            fivefor1 = –
|             tenfor1 = –
|       best bowling1 = 
|  catches/stumpings1 = –/–
|             column2 = [[Twenty20|T20]]
|            matches2 = 8
|               runs2 = 59
|            bat avg2 = 11.80
|           100s/50s2 = –/–
|          top score2 = 20
|    bat strike rate2 = 120.40
|         deliveries2 = 174
|            wickets2 = 10
|           bowl avg2 = 21.90
|bowling strike rate2 = 17.4
|            fivefor2 = –
|             tenfor2 = –
|       best bowling2 = 2/23
|  catches/stumpings2 = 6/–
|             column3 = [[2006 ICC Under-19 Cricket World Cup|U-19 WC]]
|            matches3 = 5
|               runs3 = 9
|            bat avg3 = 2.25
|           100s/50s3 = –/–
|          top score3 = 3
|    bat strike rate3 = 39.13
|         deliveries3 = 229
|            wickets3 = 8
|           bowl avg3 = 24.87
|bowling strike rate3 = 28.62
|            fivefor3 = 1
|             tenfor3 = –
|       best bowling3 = 5/61
|  catches/stumpings3 = 2/–
|                date = 5 August
|                year = 2012
|              source = http://www.espncricinfo.com/ci/content/player/233237.html Cricinfo
}}

'''Abhimanyu Rajp''' (born 8 March 1986) is an [[India]]n born American [[cricket]]er. Rajp is a right-arm [[off-spin]] bowler and a right handed [[Batsman (cricket)|batsman]]. Rajp currently represents the [[United States national cricket team]].<ref>{{cite web|url=http://www.espncricinfo.com/usa/content/story/142453.html|title=Abhimanyu Rajp on ESPNcricinfo|publisher=ESPNcricinfo  |accessdate=2012-08-06}}</ref>

==Career==

===Early years===
Rajp was born in [[Ludhiana]], [[Punjab, India]]. Along with being primarily a right-handed [[off-spin]]ner he is also a handy [[batsman]]. He started playing [[cricket]] professionally at an early age and represented his city [[Ludhiana]] at the U-14 and U-16 district levels. Later, Rajp, who was 14 at the time, along with his family moved to the United States of America in the year 2000. Rajp, quickly took over the Under-19 cricket scene in the USA and became one of the [[All-star]]s of the country by 2004.<ref>{{cite web|url=http://www.espncricinfo.com/usa/content/story/142453.html|title=Here is our future – but arguments rage|publisher=ESPN Cricinfo  |author = Deb K Das |accessdate=2012-08-06}}</ref> Soon after, he was labelled as one to watch for the future by various critics in the USA [[cricket]] circuit.

===Under-19 career===
Rajp [[Captain (cricket)|captain]]ed the Southern California [[Cricket]] Association's Under-19 team from its creation in 2002 till 2006. He was also the captain of USA's South West Region Under-19 team from 2004 to 2006, which participates in the annual USA National Under-19 Tournaments. He was part of the first generation of USA cricketers in the Under-19 structure of USA that started with the first Under-19 National Tournament in 2004. Rajp went on to make the national under-19 team in 2005.

Rajp first represented the [[United States national cricket team]] at the [[ICC Americas Under-19 Championship]] in 2005 against Canada, Bermuda, Cayman Islands and Argentina. Rajp was particularly successful against Bermuda, taking 5 for 7,<ref>{{cite web|url=http://static.espncricinfo.com/db/ARCHIVE/2005/OTHERS/ICC-AM-U19/SCORECARDS/BMDA-U19_USA-U19_ICC-AM-U19_09AUG2005.html|title=USA U-19 vs. Bermuda U-19 ICC Americas U-19 World Cup Qualifier 2005|publisher=ESPN Cricinfo | date=9 August 2005 | accessdate=2012-08-06}}</ref> due to which Bermuda was all out for a rather small target of  87 and USA coasted to victory by eight wickets. The very next day, Rajp took another five-for (5 for 45),<ref>{{cite web|url=http://static.espncricinfo.com/db/ARCHIVE/2005/OTHERS/ICC-AM-U19/SCORECARDS/ARG-U19_USA-U19_ICC-AM-U19_10AUG2005.html|title=USA U-19 vs. Argentina U-19 ICC Americas U-19 World Cup Qualifier 2005|publisher=ESPN Cricinfo | date=10 August 2005 |accessdate=2012-08-06}}</ref> against Argentina in a seven-wicket victory before USA registered its final victory of the tour against Cayman Islands to seal a sweep of the tournament and made history by clinching a spot in the [[2006 ICC Under-19 Cricket World Cup]] for the first time, in [[Sri Lanka]].<ref>{{cite web|url=http://www.thesightscreen.com/post-match-opinions/the-sad-and-sorry-saga-of-usa-cricket-in-the-21st-century-part-2/ |title=The Sad and Sorry Saga of USA Cricket in the 21st Century: Part 2 |publisher=The Sight Screen |date=14 March 2012 |accessdate=2012-08-06}}</ref> At that tournament, Rajp became the first American [[cricket]]er to become the [[MVP]] of any U-19 tournament involving USA. He grabbed the best bowler of the tournament and two [[man of the match]] awards.<ref>{{cite web|url=http://cricketarchive.com/Archive/Players/97/97732/match_awards.html |title=Awards at the 2005 U-19 World Cup Qualifiers |publisher=Cricketarchive.com |date= |accessdate=2012-08-06}}</ref> Rajp ended the tournament with a tournament-high 11 wickets in four games and marked his arrival.

Rajp's first major test came in the form of the [[2006 ICC Under-19 Cricket World Cup]] in Sri Lanka, in which he was promoted to be the vice-captain of the [[United States Under-19 cricket team]] due to his performances at the [[ICC Americas Under-19 Championship]] and National Under-19 Tournaments.<ref>{{cite web|url=http://www.espncricinfo.com/u19-worldcup/content/story/234667.html |title=United States of America Under-19s Face Baptism of fire|publisher=ESPN Cricinfo | author= Brian Murhatroyd |date=27 January 2006 |accessdate=2012-08-06}}</ref> He had a successful World Cup campaign and finished the tournament as one of the leading wicket takers for the US and also landed himself in the top 10 wicket takers for the entire tournament, in number of wickets taken.<ref>{{cite web|url=http://stats.espncricinfo.com/u19-worldcup/engine/records/bowling/most_wickets_career.html?id=2616;type=tournament|title=2006 U-19 World Cup Highest Wicket Takers|publisher=ESPN Cricinfo |accessdate=2012-08-06}}</ref> His most notable performance came against an ICC full-member nation, [[New Zealand]], when he took the first (and the only till date) five-wicket haul for the USA against a full member nation.<ref>{{cite web|url=http://www.espncricinfo.com/u19-worldcup/engine/match/309697.html |title=Plate Semi-Final: New Zealand Under-19s v United States of America Under-19s at Colombo (PSS), Feb 15, 2006 &#124; Cricket Scorecard |publisher=ESPN Cricinfo |date= |accessdate=2012-08-06}}</ref> [[United States of America Cricket Association]]'s administration problems soon after the 2006 ICC Under-19 Cricket World Cup resulted in a two-year ban imposed by cricket's governing body, [[International Cricket Council]]. Rajp's name slowly faded away on the national circuit and was off the radar for the next few years before making his resurgence in 2010.<ref>{{cite web|url=http://www.espncricinfo.com/usa/content/story/282227.html |title= ICC Board suspends USA Cricket Association |publisher=ESPN Cricinfo |date= 7 March 2007 |accessdate=2012-11-15}}</ref>

===USA Domestic Cricket===
Rajp has been one of the top players in the USA domestic [[cricket]] circuit for a number of years. He plays in the Southern California Cricket Association, which is based in Los Angeles, California and represents the South West Region in USA National Cricket Tournaments. He has had a good showing in domestic tournaments with the most recent being the best bowler at the 2010 Western Conference, where he also created a national level record, which stands till date, for taking 7 wickets in one innings, at a national tournament game against the Central East region.<ref>{{cite web|url=http://www.dreamcricket.com/dreamcricket/news.hspl?nid=14989&ntid=4 |title=USA Cricket 2010 Eastern & Western Conference – Top Performers and All-Tournament XI – USA Cricketer |publisher=Dreamcricket.com |author=Peter Della Penna |date= |accessdate=2012-08-06}}</ref>  Later he went on to become the best bowler for the second time in a calendar year and this time it was at the 2010 United States Senior National tournament.<ref>{{cite web|url=http://www.dreamcricket.com/community/blogs/usa_cricketer/archive/2010/11/23/usa-cricket-2010-senior-nationals-top-performers-amp-all-tournament-xi.aspx |title=USA Cricket 2010 Senior Nationals – Top Performers & All-Tournament XI – USA Cricketer |publisher=Dreamcricket.com |author=Peter Della Penna |date= |accessdate=2012-08-06}}</ref> Following solid domestic performances, and a good Under-19 record at the highest level, Rajp, was called for national trials in 2012 to earn a spot in the [[United States national cricket team]]. He would later go one to make the USA National Squad in 2012.<ref>{{cite web|url=http://www.espncricinfo.com/world-twenty20-qualifier-2012/content/story/552647.html|title=Four new faces in USA's World T20 qualifiers team
|publisher=ESPNcricinfo |author=Peter Della Penna |date=8 February 2012 |accessdate=2012-08-06}}</ref>

===International career===
Rajp made his debut in 2012 when he was selected as to be a part of the [[United States national cricket team]] at the [[2012 ICC World Twenty20 Qualifier]] in the [[UAE]] in March 2012.<ref>{{cite web|url=http://www.espncricinfo.com/ci/content/squad/552747.html|title=United States of America Squad, ICC World Twenty20 Qualifier, 2011/12|publisher=ESPNcricinfo |date=16 March 2012 |accessdate=2012-08-06}}</ref> He debuted in style against [[Italy]] when he took a [[wicket]] with his very first delivery and scored an unbeaten 14 runs off 9 balls in a match that USA lost by 8 runs.<ref>{{cite web|url=http://www.espncricinfo.com/ci/engine/match/546417.html |title=USA vs Italy at the 2012 ICC World Twenty20 Qualifier |publisher=Espncricinfo.com |date=14 March 2012|accessdate=2012-08-06}}</ref>
Rajp had a very successful debut tournament, finishing at the top in the wickets department for his team and in the top ten on the list of highest wicket takers for the entire tournament (in number of wickets taken).<ref>{{cite web|url=http://stats.espncricinfo.com/ci/engine/records/bowling/most_wickets_career.html?class=6;id=6868;type=tournament |title=Most Wickets at the 2012 ICC World Twenty20 Qualifier |publisher=Stats.espncricinfo.com |date= |accessdate=2012-08-06}}</ref> Following his performances with the ball he was also the one of the best fielder in his team, with team high 6 catches, and finished the tournament in the Top Three (in amount of fielding dismissals).<ref>{{cite web|url=http://stats.espncricinfo.com/ci/engine/records/fielding/most_catches_career.html?class=6;id=6868;type=tournament |title=Fielding dismissal at the 2012 ICC World Twenty20 Qualifier |publisher=Stats.espncricinfo.com |date= |accessdate=2012-08-06}}</ref>

Awarding his success at the [[2012 ICC World Twenty20 Qualifier]], his debut tour for the Senior national team, the selectors picked Rajp as one of the members of the [[United States national cricket team]] for the [[2012 ICC World Cricket League Division Four]] which took place from 3 to 10 September 2012 in [[Malaysia]].<ref>{{cite web|url=http://www.espncricinfo.com/ci/content/series/571057.html |title=Tournament Home Division 4 2012 Malaysia |publisher=Espncricinfo.com |date=1 September 2012|accessdate=2012-09-10}}</ref> The [[United States national cricket team]] was successful in gaining promotion to [[2013 ICC World Cricket League Division Three]] to be held in [[Bermuda]] from 28 April to 5 May 2013. Rajp, who once again finished as one of the top wicket takers for USA in the Twenty20 however,<ref>{{cite web|url=http://stats.espncricinfo.com/ci/engine/records/bowling/most_wickets_career.html?id=8046;type=tournament|title=Most Wickets at the 2013 ICC Americas Twenty20 Division One Qualifier |publisher=Stats.espncricinfo.com |date= |accessdate=2013-04-10}}</ref>  was not chosen to be a part of that team traveling to [[Bermuda]] for Division Three, 50 over format, Following a spree of changes in the USA Squad  after the team's successful triumph in the [[2013 ICC World Cricket League Americas Region Twenty20 Division One]] where USA swept the whole tournament to gain qualification for the [[2013 ICC World Twenty20 Qualifier]] in the [[UAE]].<ref>{{cite web|url=http://www.espncricinfo.com/usa/content/story/627665.html |title=USA drop key veterans for ICC WCL Division Three |publisher=Espncricinfo.com |date=10 April 2013|accessdate=2013-04-10}}</ref> Rajp again finished as one of the top wicket takes for USA in that tournament as well.<ref>{{cite web|url=http://stats.espncricinfo.com/ci/engine/records/averages/batting_bowling_by_team.html?id=8046;team=11;type=tournament |title=USA Statistics for T20 Americas Cup 2013 |publisher=Espncricinfo.com |date=10 April 2013|accessdate=2013-04-10}}</ref>

Later in 2013, Rajp traveled with the USA Squad to [[Toronto]], Canada for the 2013 edition of the Auty Cup (annual fixture vs Canada) and was hailed by critics as one of the stand out performers for the USA.<ref>{{cite web|url=http://www.dreamcricket.com/dreamcricket/news.hspl?nid=17051&ntid=4 |title=USA Cricket: Team and player reviews for The 2013 Auty Cup |publisher=Dreamcricket.com |author=Peter Della Penna| date=7 Aug 2013|accessdate=2013-10-10}}</ref> Although, his aggressive bowling as not enough for the USA to win the Auty Cup title, it surely helped his case to become a regular fixture in the USA squad.<ref>{{cite web|url=http://stats.espncricinfo.com/ci/engine/records/bowling/most_wickets_career.html?id=8371;type=tournament |title=Records / Auty Cup, 2013 |publisher=Espncricinfo.com | date=28 Sep 2013|accessdate=2013-10-10}}</ref>

== References ==
{{Reflist}}

==External links==
*[http://www.espncricinfo.com/ci/content/player/233237.html Abhimanyu Rajp] at [[Cricinfo]]
*[http://cricketarchive.com/Archive/Players/97/97732/97732.html Abhimanyu Rajp] at [[CricketArchive]]

== See also ==
*[[ICC Americas Under-19 Championship]]
*[[2006 ICC Under-19 Cricket World Cup]]
*[[2012 ICC World Twenty20 Qualifier]]
*[[2012 ICC World Cricket League Division Four]]
*[[2013 ICC World Cricket League Division Three]]

{{DEFAULTSORT:Rajp, Abhimanyu}}
[[Category:1986 births]]
[[Category:Living people]]
[[Category:People from Punjab, India]]
[[Category:American cricketers]]
[[Category:American sportsmen of Indian descent]]
[[Category:Articles created via the Article Wizard]]