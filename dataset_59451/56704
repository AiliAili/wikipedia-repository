'''Electronic articles''' are [[Article (publishing)|article]]s in [[academic journal|scholarly journal]]s or [[magazine]]s  that can be accessed via electronic transmission. They are a specialized form of [[electronic document]], with a specialized content, purpose, format, [[metadata]], and availability&ndash;they consist of individual articles from scholarly journals or  magazines (and now sometimes popular magazines), they have the purpose of providing material for academic [[research]] and study, they are formatted approximately like printed journal articles, the metadata is entered into specialized databases, such as the [[Directory of Open Access Journals]] as well as the databases for the discipline, and they are predominantly available through [[academic library|academic libraries]] and special [[library|libraries]], generally at a fixed charge. 

Electronic articles can be found in [[online and offline|online]]-only journals (par excellence), but in the 21st century they have also become common as online versions of articles that also appear in printed journals. The practice of [[Electronic publishing|publishing of an electronic version]] of an article before it later appears in print is sometimes called '''epub ahead of print''', particularly in [[PubMed]].<ref>{{cite web |url=http://www.nlm.nih.gov/services/ldepubahead.html |title=FAQ: Loansome Doc Article Ordering Service - Epub Ahead of Print |work= |accessdate=2010-10-23}}</ref><ref>{{cite web |url=http://www.gwumc.edu/library/blog/client/index.cfm/2007/11/26/Epub-ahead-of-print-What-does-this-mean |title=Himmelfarb Library Blog: Epub ahead of print… What does this mean?? |format= |work= |archiveurl=https://web.archive.org/web/20100119081653/http://www.gwumc.edu/library/blog/client/index.cfm/2007/11/26/Epub-ahead-of-print-What-does-this-mean |archivedate=2010-01-19 |deadurl=yes }}</ref>

The term can also be used for the electronic versions of less formal publications, such as online archives, working paper archives from universities, government agencies, private and public think tanks and institutes and private websites. In many academic areas, specialized [[bibliographic database]]s are available to find their online content.

Most commercial sites are [[subscription business model|subscription]]-based, or allow pay-per-view access. Many universities subscribe to electronic journals to provide access to their students and faculty, and it is generally also possible for individuals to subscribe. An increasing number of journals are now available with open access, requiring no subscription. Most working paper archives and articles on personal homepages are free, as are collections in [[institutional repository|institutional repositories]] and [[disciplinary repository|subject repositories]].

The most common formats of transmission are [[HTML]], [[Portable Document Format|PDF]] and, in specialized fields like mathematics and physics, [[TeX]] and [[PostScript]].

==See also==
* [[Academic publishing]]
* [[Eprint]]
* [[Electronic journal]]
* [[Scholarly article]]

== References ==
{{reflist}}

[[Category:Academic publishing]]
[[Category:Electronic publishing]]
[[Category:Electronic documents]]