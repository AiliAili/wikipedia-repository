{{Refimprove|date=April 2011}}
{{Infobox book
| name             = The Newcomes
| title_orig       =
| translator       =
| image            = The Newcomes.jpg
| caption          = First edition title page
| author           = [[William Makepeace Thackeray]]
| illustrator      =
| cover_artist     =
| country          = England
| language         = English
| series           =
| subject          =
| genre            = [[Fiction]]
| publisher        = [[Bradbury and Evans]]
| pub_date         = 1855
| english_pub_date =
| media_type       =
| pages            =
| isbn             =
| oclc             =
| dewey            =
| congress         =
| preceded_by      =
| followed_by      =
}}

'''''The Newcomes''''' is a [[novel]] by [[William Makepeace Thackeray]], first published in 1855.

==Publication==
''The Newcomes'' was published serially over about two years, as Thackeray himself says in one of the novel's final chapters. The novel shows its serial origin: it is very long (an undated but clearly very old edition with tiny type fills 551 pages) and its events occur over many years and in several countries before the reader reaches the predictable conclusion. The main part of ''The Newcomes'' is set a decade or two after the action of ''[[Vanity Fair (novel)|Vanity Fair]]'', and some of the characters in ''Vanity Fair'' are mentioned peripherally in ''The Newcomes''. The narrator is Arthur Pendennis, the protagonist of ''[[Pendennis]]''.

==Plot==
The novel tells the story of Colonel Thomas Newcome, a virtuous and upstanding character.  It is equally the story of Colonel Newcome's son, Clive, who studies and travels for the purpose of becoming a painter, although the profession is frowned on by some of his relatives and acquaintances &mdash; notably Clive's snobbish, backstabbing cousin Barnes Newcome.

Colonel Newcome goes out to India for decades, then returns to England where Clive meets his cousin Ethel. After years in England, the colonel returns to India for another several years and while he is there, Clive travels Europe and his love for Ethel waxes and wanes. Dozens of background characters appear, fade, and reappear.

The colonel and Clive are only the central figures in ''The Newcomes'', the action of which begins before the colonel's birth. Over several generations the Newcome family rises into wealth and respectability as bankers and begin to marry into the minor [[aristocracy (class)|aristocracy]]. A theme that runs throughout the novel is the practice of marrying for money. Herein we find first use of the coined word "[[capitalism]]", as reference an [[economic system]]. Religion is another theme, particularly [[Methodism]].

==Critical commentary==
{{blockquote|... Truth is never sacrificed to piquancy. The characters in the 'Newcomes' are not more witty, wise, or farcical than their prototypes; the dull, the insipid, and the foolish, speak according to their own fashion and not with the tongue of the author; the events which befall them are nowhere made exciting at the expense of probability. Just as the stream of life runs on through these volumes, so may it be seen to flow in the world itself by whoever takes up the same position on the bank.<ref>{{cite journal|title=Review of ''Newcomes. Memoirs of a most respectable Family.''|journal=The Quarterly Review|date=September 1855|volume=97|pages=350–387|url=https://babel.hathitrust.org/cgi/pt?id=uc1.$b661391;view=1up;seq=357|postscript=; p. 351 quotation}}</ref>}}
Perhaps one of the novel's greatest strengths is that it contains hundreds of references to the popular and educated culture of the time and thus gives a better idea than most contemporary novels of what it was like to live in England then &mdash;  almost a miniature education in the [[Victorian era]]. Thackeray mentions poets, painters, novelists (some of the characters are reading ''[[Oliver Twist]])'', politics, and other people, events and things both familiar and obscure to the 21st-century reader &mdash; and does so in a natural way that enhances the story. There are also plenty of [[Latin]], [[French language]], and even [[Italian language]] and [[ancient Greek|ancient-Greek]] phrases &mdash; all untranslated.

Colonel Newcome came to be an emblem of virtue for a period, often referred to at the turn of the 20th century. For example, in his autobiography, [[Theodore Roosevelt]] described his uncle, [[James Dunwoody Bulloch]], as "a veritable Colonel Newcome".<ref>[[Theodore Roosevelt|Roosevelt, Theodore]] [http://www.fullbooks.com/Theodore-Roosevelt-An-Autobiography-by1.html ''Theodore Roosevelt &mdash; An Autobiography''] - Full Text Free Book (Part 1/11).</ref>

[[Ethel Barrymore]] is named after the character in the play.

==See also==
{{portal|Novels}}
* [[1855 in literature]]

==Notes==
{{reflist}}

==External links==
{{Gutenberg|no=7467|name=The Newcomes}}

{{William Makepeace Thackeray}}

{{Authority control}}

{{DEFAULTSORT:Newcomes, The}}
[[Category:1855 novels]]
[[Category:Novels set in England]]
[[Category:English novels]]
[[Category:Novels set in Europe]]
[[Category:Novels set in India]]
[[Category:Novels by William Makepeace Thackeray]]
[[Category:Novels first published in serial form]]
[[Category:Victorian novels]]
[[Category:British novels adapted into plays]]