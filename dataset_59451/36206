{{Good article}}
{{taxobox
 | image = Amanita_abrupta_54083.jpg
 | image_width = 234px
 | image_caption = A young specimen
 | regnum = [[Fungi]]
 | divisio = [[Basidiomycota]]
 | classis = [[Agaricomycetes]]
 | ordo = [[Agaricales]]
 | familia  = [[Amanitaceae]]
 | genus = ''[[Amanita]]''
 | species = '''''A. abrupta'''''
 | binomial = ''Amanita abrupta''
 | binomial_authority = [[Charles Horton Peck|Peck]] (1897)
 | synonyms_ref = <ref name="urlMycoBank: Amanita abrupta"/>
 | synonyms = 
*''Lepidella abrupta'' <small>(Peck) [[J.-E.Gilbert]] (1928)<ref name="Gilbert 1928"/></small>
*''Aspidella abrupta'' <small>(Peck) J.-E.Gilbert (1940)<ref name="Gilbert 1940"/></small>
}}
{{mycomorphbox
| name = ''Amanita abrupta''
| whichGills = free
| capShape = convex
| capShape2 = flat
| hymeniumType = gills
| stipeCharacter = ring and volva
| ecologicalType = mycorrhizal
| sporePrintColor = white
| howEdible = inedible
}}
{{stack end}}

'''''Amanita abrupta''''', commonly known as the '''American abrupt-bulbed Lepidella''', is a species of [[fungus]] in the family [[Amanitaceae]]. Named for the characteristic shape of its [[basidiocarp|fruit bodies]], this white ''Amanita'' has a slender [[stipe (mycology)|stem]], a [[pileus (mycology)|cap]] covered with conical white warts, and an "abruptly enlarged" swollen base. This terrestrial species grows in [[mixed forest|mixed woods]] in eastern North America and eastern Asia, where it is thought to exist in a [[mycorrhiza]]l relationship with a variety of both [[conifer]]ous and [[deciduous]] tree species.

==Taxonomy==
''Amanita abrupta'' was first [[species description|described]] by American mycologist [[Charles Horton Peck]] in 1897, based on a specimen he found in [[Auburn, Alabama]]. Because the remains of the [[volva (mycology)|volva]] are not present on the bulb in dried, mature, specimens, Peck thought that the species should be grouped with ''[[Amanita rubescens]]'' and ''[[Amanita spissa|A.&nbsp;spissa]]''.<ref name="Peck 1897"/> [[Synonym (biology)|Synonyms]] include binomials resulting from generic transfers by [[Jean-Edouard Gilbert]] to ''Lepidella'' in 1928, and to ''Aspidella'' in 1940.<ref name="urlMycoBank: Amanita abrupta"/> Both of these genera have since been subsumed into ''[[Amanita]]''.<ref name="Kirk 2008"/>

''A.&nbsp;abrupta'' is the [[type species]] of the [[section (botany)|section]] ''Lepidella'' of the genus ''Amanita'', in the [[subgenus]] ''Lepidella'', a grouping of related ''Amanita'' mushrooms characterized by their [[amyloid]] [[basidiospore|spores]].<ref name="Tuloss"/> Other North American species in this subgenus include ''[[Amanita atkinsoniana|A.&nbsp;atkinsoniana]]'', ''[[Amanita chlorinosma|A.&nbsp;chlorinosma]]'', ''[[Amanita cokeri|A.&nbsp;cokeri]]'', ''[[Amanita daucipes|A.&nbsp;daucipes]]'', ''[[Amanita mutabilis|A.&nbsp;mutabilis]]'', ''[[Amanita onusta|A.&nbsp;onusta]]'', ''[[Amanita pelioma|A.&nbsp;pelioma]]'', ''[[Amanita polypyramis|A.&nbsp;polypyramis]]'', ''[[Amanita ravenelii|A.&nbsp;ravenelii]]'', and ''[[Amanita rhopalopus|A.&nbsp;rhopalopus]]''.<ref name="Bhatt 2004"/> European and Asian species (also in section ''Lepidella'') that are [[phylogenetics|phylogenetically]] related—close to it in the evolutionary family tree—include ''[[Amanita solitaria|A.&nbsp;solitaria]]'', ''[[Amanita virgineoides|A.&nbsp;virgineoides]]'', and ''[[Amanita japonica|A.&nbsp;japonica]]''.<ref name="Zhang 2004"/>

The [[botanical name|specific epithet]] ''abrupta'' refers to the shape of the swollen base, which is abruptly enlarged rather than gradually tapering.<ref name="Metzler p64">Metzler and Metzler (1992), [https://books.google.com/books?id=HRtfvVigMmsC&pg=PA64 p. 64].</ref> The species' [[common name]] is the "American abrupt-bulbed Lepidella".<ref name="urlAmanita abrupta Peck"/>

==Description==
[[File:Amanita abrupta 49115.jpg|thumb|left|The [[annulus (mycology)|ring]] on the stem is prominent in mature specimens.]]
In ''Amanita abrupta'', as with most [[mushroom]]s, the bulk of the organism lies unseen beneath the ground as an aggregation of fungal cells called [[hypha]]e; under appropriate environmental conditions, the visible reproductive structure ([[basidiocarp|fruit body]]) is formed. The [[Pileus (mycology)|cap]] has a diameter of {{convert|4|to|10|cm|in|1|sp=us}}, and has a broadly convex shape when young, but eventually flattens.<ref name="urlAmanita abrupta Peck"/> The central portion of the cap becomes depressed in mature specimens.<ref name="urlAmanita abrupta Peck"/> The cap surface is ''verrucose''—covered with small angular or pyramidal erect warts (1–2&nbsp;mm tall by 1–2&nbsp;mm wide at the base);<ref name="urlAmanita abrupta Peck"/> the warts are smaller and more numerous near the margin of the cap,<ref name="urlMushroomExpert: A. abrupta"/> and small fragments of tissue may be hanging from the margin of the cap.<ref name="Phillips 2005"/> The cap surface, the warts, and the [[trama (mycology)|flesh]] are white. The warts can be easily separated from the cap, and in mature specimens they have often completely or partly disappeared.<ref name="Peck 1897"/> The white [[lamella (mycology)|gills]] are placed moderately close together, reaching the stem but not directly attached to it.

The [[stipe (mycology)|stem]] is {{convert|6.5|to|12.5|cm|in|1|abbr=on}} tall, and slender, with a diameter of {{convert|0.5|to|1.5|cm|in|1|abbr=on}}.<ref name="Metzler p64"/> It is white, smooth ([[glabrous]]), solid (that is, not hollow internally), and has an abruptly bulbous base with the shape of a flattened sphere; it may develop longitudinal splits on the sides. The base is often attached to a copious white [[mycelium]]—a visual reminder that the bulk of the organism lies unseen below the surface. The [[annulus (mycology)|ring]] is membranous, and persistent—not weathering away with time;<ref name="Peck 1897"/> the ring may be attached to the stem with white fibers.<ref name="urlMushroomExpert: A. abrupta"/> The mushroom has no distinct odor.<ref name="Miller 2006"/> The [[edible mushroom|edibility]] of the mushroom is unknown; however, it is generally not recommended to consume ''Amanita'' mushrooms of questionable edibility.<ref name="Phillips 2005"/>

===Microscopic characteristics===
[[File:Amanita abrupta spores 59705.jpg|thumb|right|The spores of ''A.&nbsp;abrupta'' are broadly elliptical or roughly spherical.]]
When collected in deposit, such as with a [[spore print]], the spores appear white. Viewed with a microscope, the [[basidiospore|spores]] are broadly elliptical or roughly spherical, smooth, thin-walled, and have dimensions of 6.5–9.5 by 5.5 by 8.5&nbsp;[[micrometre|µm]]. Spores are [[amyloid]] (meaning they take up [[iodine]] when stained with [[Melzer's reagent]])<ref name="Metzler 1992"/> The [[basidia]] (spore-bearing cells on the edges of gills) are four-spored and measure 30–50 by 4–11&nbsp;µm. The bases of the basidia have [[clamp connection]]s—short branches connecting one cell to the previous cell to allow passage of the products of [[nucleus (cell)|nuclear]] division. The [[cap cuticle]] comprises a layer of densely interwoven, sightly gelatinized, filamentous hyphae that are 3–8&nbsp;µm in diameter. The stem tissue is made of sparse, thin, longitudinally oriented hyphae measuring 294 by 39&nbsp;µm.<ref name="Jenkins p. 77"/>

===Similar species===
The fruit bodies of ''[[Amanita kotohiraensis]]'', a species known only from Japan, bears a superficial resemblance to ''A.&nbsp;abrupta'', but ''A.&nbsp;kotohiraensis'' differs in having scattered floccose patches (tufts of soft woolly hairs that are the remains of the [[volva (mycology)|volva]]) on the cap surface, and pale yellow gills.<ref name="Nagasawa 2000"/> ''A.&nbsp;polypyramis'' fruit bodies have also been noted to be similar to ''A.&nbsp;abrupta'';<ref name="Miller 2006"/> however, it tends to have larger caps, up to {{convert|21|cm|in|abbr=on}} in diameter, a fragile ring that soon withers away, and somewhat larger spores that typically measure 9–14 by 5–10&nbsp;µm.<ref name="urlAmanita polypyramis (MushroomExpert.Com)"/>  The [[amyloid]]ity and size of the spores are reliable characteristics to help distinguish ''A.&nbsp;abrupta'' specimens with less prominently bulbous bases from other lookalike species.<ref name="urlMushroomExpert: A. abrupta"/>

Mycologists [[Tsuguo Hongo]] and Rokuya Imazeki suggested in the 1980s that the Japanese mushroom ''[[Amanita sphaerobulbosa|A.&nbsp;sphaerobulbosa]]'' was synonymous with the North American ''A.&nbsp;abrupta''.<ref name="Hongo 1982"/><ref name="Imazeki 1997"/> However, a 1999 study of ''Amanita'' specimens in Japanese [[herbarium|herbaria]] concluded that they were closely related but distinct species, due to differences in spore shape and in the microstructure of the volval remnants.<ref name="Yang 1999"/> Another similar species, ''[[Amanita magniverrucata|A.&nbsp;magniverrucata]]'', is differentiated from ''A.&nbsp;abrupta'' by a number of characteristics: the universal veil is clearly separated from the flesh of the cap; the volval warts disappear more quickly because the surface of the cap [[Pileipellis|cuticle]] gelatinizes; the partial veil is more persistent; the spores are smaller and roughly spherical; on the underside of the partial veil, the stem has surface fibrils that are drawn upward so as to somewhat resemble a cortina (a cobweb-like protective covering over the immature spore bearing surfaces); ''A.&nbsp;magniverrucata'' has a known distribution limited to the south western coast of North America.<ref name="Tulloss 2009"/>

==Habitat, distribution, and ecology==
The fruit bodies of ''A.&nbsp;abrupta'' grow on the ground, typically solitary, in [[mixed forest|mixed]] conifer and [[Deciduous|deciduous forests]],<ref name="Metzler p64"/> usually during autumn.<ref name="Jenkins p. 77">Jenkins (1986), p. 77.</ref> The frequency with which fruit bodies appear depends on several factors, such as season, location, temperature, and rainfall. The mushroom has been described as common in the Southeastern United States;<ref name="Bessette 2007"/> in [[Texas]], it has been called both infrequent,<ref name="Metzler p64"/> and common in the [[Big Thicket National Preserve]].<ref name="Lewis 1981"/> Like most other ''Amanita'' species, ''A.&nbsp;abrupta'' is thought to form [[mycorrhizal]] relationships with trees. This is a mutually beneficial relationship where the hyphae of the fungus grow around the roots of trees, enabling the fungus to receive moisture, protection and nutritive byproducts of the tree, and affording the tree greater access to soil nutrients.<ref>Jenkins (1986), p. 5.</ref> ''Amanita abrupta'' is widely distributed throughout eastern North America,<ref name="Metzler p64"/> where it has been found as far north as [[Quebec, Canada]],<ref name="urlwww.mao-qc.ca"/> and as far south as [[Mexico]].<ref name="Guía 2001"/> [[Orson K. Miller]] claims to have found it in the [[Dominican Republic]] where it appeared to be growing in a mycorrizhal association with [[pine]] trees.<ref name="Miller 2006"/> Kuo also mentions a mycorrhizal relationship with both [[hardwood]]s and [[conifer]]s,<ref name="urlMushroomExpert: A. abrupta"/> while Tulloss lists additional preferred tree hosts such as [[beech]], [[birch]], [[fir]], [[tsuga]], [[oak]], and [[Populus|poplar]].<ref name="urlAmanita abrupta Peck"/> However, ''A.&nbsp;abrupta'' has been shown experimentally to not form mycorrhizae with [[Virginia Pine]].<ref name="Vozzo 1961"/>

==See also==
{{Portal|Fungi}}
*[[List of Amanita species|List of ''Amanita'' species]]

==References==
{{Reflist|colwidth=30em|refs=

<ref name="Bessette 2007">{{cite book |title=Mushrooms of the Southeastern United States |author1=Bessette, A.E. |author2=Roody, W.C. |author3=Bessette, A.R. |year=2007 |publisher=Syracuse University Press |location=Syracuse, New York |isbn=978-0-8156-3112-5 |page=104 |url=https://books.google.com/books?id=IB1Gv3jZMmAC&pg=PA104}}</ref>

<ref name="Bhatt 2004">{{cite book |author1=Bhatt, R.P. |author2=Miller, O.K. Jr] |editor-last=Cripps, C.L. |title=Fungi in Forest Ecosystems: Systematics, Diversity, and Ecology |publisher=New York Botanical Garden Press |year=2004 |pages=33–59 |chapter=''Amanita'' subgenus ''Lepidella'' and related taxa in the southeastern United States |isbn=978-0-89327-459-7}}</ref>

<ref name="Gilbert 1928">{{cite journal |author1=Gilbert, J.-E. |author2=Kühner, R. |title=Recherches sur les spores des amanites |journal=Bulletin de la Société Mycologique de France |year=1928 |volume=44 |pages=149–154 |language=French}}</ref>

<ref name="Gilbert 1940">{{cite journal |author=Gilbert, J-E. |title=Iconographia mycologica, Amanitaceae |journal=Iconographia Mycologica |year=1940 |volume=27 |pages=1–198 (see p.&nbsp;79)}}</ref>

<ref name="Guía 2001">{{cite book |author=Ortigoza, C.J.A. |title=Guía micológica del género amanita del Parque Estatal Sierra de Nanchititla. Volume 4 of Cuadernos de investigación |year=2001 |publisher=UAEMEX |isbn=978-968-835-546-6 |pages=20–1 |url=https://books.google.com/books?id=2wIXsJRJCbAC&pg=PA20 |language=Spanish}}</ref>

<ref name="Hongo 1982">{{cite journal |author=Hongo, T. |year=1982 |title=The Amanitas of Japan |journal=Acta Phytotaxonomica et Geobotanica |volume=33 |pages=116–126 |language=Japanese}}</ref>

<ref name="Imazeki 1997">{{cite book |author1=Imazeki, R. |author2=Hongo, T. |title=Colored Illustrations of Mushrooms of Japan |volume=1 |year=1987 |publisher=Hoikusha Publishing |location=Osaka, Japan |pages=}}</ref>

<ref name="Kirk 2008">{{cite book |author1=Kirk, P.M. |author2=Cannon, P.F. |author3=Minter, D.W. |author4=Stalpers, J.A. |title=Dictionary of the Fungi |edition=10th |publisher=CAB International |location=Wallingford, UK |year=2008 |pages=63, 369 |isbn=978-0-85199-826-8}}</ref>

<ref name="Lewis 1981">{{cite journal |author1=Lewis, D.P. |author2=McGraw, J.L. Jr |title=Agaricales, family Amanitaceae, of the Big Thicket |jstor=3671322 |journal=The Southwestern Naturalist |year=1981  |volume=26 |issue=1 |pages=1–4 |doi=10.2307/3671322}}</ref>

<ref name="Miller 2006">{{cite book |author1=Miller, H.R. |author2=Miller, O.K. |title=North American Mushrooms: A Field Guide to Edible and Inedible Fungi |year=2006 |publisher=Falcon Guide |location=Guilford, Connecticut |page=43 |isbn=0-7627-3109-5 |url=https://books.google.com/books?id=zjvXkLpqsEgC&pg=PA43}}</ref>

<ref name="Metzler 1992">Metzler and Metzler (1992), [https://books.google.com/books?id=HRtfvVigMmsC&pg=PA331 p. 331.]</ref>

<ref name="Nagasawa 2000">{{cite journal |author1=Nagasawa, E. |author2=Mitani, S. |title=A new species of ''Amanita'' section ''Lepidella'' from Japan |journal=Memoirs of the National Science Museum (Tokyo) |year=2000 |volume=32 |pages=93–97}}</ref>

<ref name="Peck 1897">{{cite journal |author=Peck, C.H. |title=New species of fungi |journal=Bulletin of the Torrey Botanical Club |year=1897 |volume=24 |issue=3 |pages=137–147 |jstor=2477879}}</ref>

<ref name="Phillips 2005">{{cite book |author=Phillips, R. |title=Mushrooms and Other Fungi of North America |publisher=Firefly Books |location=Buffalo, New York |year=2005 |page=27 |isbn=1-55407-115-1}}</ref>

<ref name="Tuloss">{{cite web |url=http://www.amanitaceae.org/?Sections+of+Amanita |title=Sections of ''Amanita'' |author=Tulloss, R.E. |publisher=Studies in the Amanitaceae |accessdate=2011-02-11}}</ref>

<ref name="Tulloss 2009">{{cite journal |author=Tulloss, R. |title=''Amanita magniverrucata''—revision of an interesting species of ''Amanita'' section ''Lepidella'' |year=2009 |journal=Mycotaxon |volume=108 |pages=93–104 |doi=10.5248/108.93}}</ref>

<ref name="urlMushroomExpert: A. abrupta">{{cite web |url=http://www.mushroomexpert.com/amanita_abrupta.html |title=''Amanita abrupta''|author=Kuo, M. |work=MushroomExpert.Com |date=August 2003 |accessdate=2009-08-18}}</ref>

<ref name="urlMycoBank: Amanita abrupta">{{cite web |title=''Amanita abrupta'' Peck 1897 |url=http://www.mycobank.org/BioloMICS.aspx?Table=Mycobank&Rec=345059&Fields=All |publisher=[[MycoBank]]. International Mycological Association |accessdate=2012-11-08}}</ref>

<ref name="urlAmanita abrupta Peck">{{cite web |url=http://www.amanitaceae.org/?Amanita+abrupta |title=''Amanita abrupta'' |author=Tulloss, R.E. |publisher=Studies in the Amanitaceae |accessdate=2011-02-11}}</ref>

<ref name="urlAmanita polypyramis (MushroomExpert.Com)">{{cite web |url=http://www.mushroomexpert.com/amanita_polypyramis.html |title=''Amanita polypyramis''  |author=Kuo, M. |work=MushroomExpert.Com |date=March 2008 |accessdate=2009-08-19}}</ref>

<ref name="urlwww.mao-qc.ca">{{cite web |title=Liste des Macromycètes – Outaouais Québec 1984—2006 |publisher=Les mycologues amateurs de l'Outaouais |url=http://www.mao-qc.ca/myco/images/Macromycetes%201984-2006.pdf |format=PDF |accessdate=2009-08-18 |language=French}}</ref>

<ref name="Vozzo 1961">{{cite journal |author1=Vozzo, J.A. |author2=Hackskaylo, E. |year=1961 |title=Mycorrhizal fungi on ''Pinus virginiana'' |jstor=3756310 |journal=[[Mycologia]] |volume=53 |issue=5 |pages=538–539 |doi=10.2307/3756310 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0053/005/0538.htm}}</ref>

<ref name="Yang 1999">{{cite journal |author1=Yang, Z.-L. |author2=Doi, Y. |year=1999 |title=A contribution to the knowledge of ''Amanita'' (Amanitaceae, Agaricales) in Japan |journal=Bulletin of the National Science Museum of Tokyo Series B |volume=25 |issue=3 |pages=107–130}}</ref>

<ref name="Zhang 2004">{{cite journal |author1=Zhang, L. |author2=Yang, J. |author3=Zhuliang, Y. |year=2004 |title=Molecular phylogeny of eastern Asian species of ''Amanita'' (Agaricales, Basidiomycota): taxonomic and biogeographic implications |journal=Fungal Diversity |volume=17 |pages=219–238 |url=http://eticomm.net/~ret/amanita/pdfs/Zhang%20Amanita%20phylogeny.pdf |format=PDF}}</ref>

}}

===Cited books===
* {{cite book |author=Jenkins, D.B. |title=''Amanita'' of North America |publisher=Mad River Press |location=Eureka, California |year=1986 |isbn=0-916422-55-0}}
* {{cite book |author1=Metzler, V. |author2=Metzler, S. |title=Texas Mushrooms: A Field Guide |publisher=[[University of Texas Press]] |location=Austin, Texas |year=1992 |isbn=0-292-75125-7}}

==External links==
*{{IndexFungorum|213645}}

[[Category:Amanita|abrupta]]
[[Category:Fungi described in 1897]]
[[Category:Fungi of North America]]
[[Category:Taxa named by Charles Horton Peck]]