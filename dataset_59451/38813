{{good article}}
{{Infobox single
| Name = Broken
| Cover = Broken.JPG
| Artist = [[Lifehouse (band)|Lifehouse]]
| from Album = [[Who We Are (Lifehouse album)|Who We Are]]
| Released = July 8, 2008
| Format = [[Music download|Digital download]]
| Recorded = 
| Genre = [[Pop music|Pop]], [[rock music|rock]], [[Adult album alternative|adult alternative]]
| Length = 4:15 <small>(New/Radio Version)</small><br> 4:46 <small>(Album Version)</small>
| Label = [[Geffen Records|Geffen]]
| Writer = Jason Wade
| Producer = [[John Fields (producer)|John Fields]], Scott Faircloff
| Reviews =
| Last single = "[[Whatever It Takes (Lifehouse song)|Whatever It Takes]]"<br />(2007)
| This single = "'''Broken'''" <br />(2008)
| Next single = "[[Halfway Gone]]"<br/>(2009)
}}

"'''Broken'''" is a song by American [[Alternative rock|alternative]] band [[Lifehouse (band)|Lifehouse]]. It is the third single released from their fourth studio album, [[Who We Are (Lifehouse album)|''Who We Are'']] (2007). Lead singer [[Jason Wade]] was inspired to write the song after he visited a friend in [[Nashville, Tennessee|Nashville]] who needed a kidney transplant. Originally released on June 18, 2007 as the fifth track on ''Who We Are'', the song was then edited for radio to give it more of a "rock" feel. The new radio version of the song was released via digital download on July 8, 2008.

The song received positive reviews from critics, as they applauded the use of instruments in the song. The song charted at number 83 and 84 in the United States and Canada respectively, and later charted in New Zealand at number 21. The song's music video premiered on [[VH1|VH1.com]] on September 16, 2008. It is set in the scene of a tunnel where Wade is walking away in the opposite direction of a car crash. Lifehouse has played "Broken" live on several occasions, including on [[Soundstage (TV series)|''Soundstage'']] and at the [[Pinkpop Festival#Pinkpop 2011|2011 Pinkpop Festival]] in the Netherlands.  A seven-minute extended version is often the final song in their concert setlist.

==Background, release and composition==
{{listen|filename=Broken - Lifehouse.ogg|title="Broken"|description=Lifehouse lead singer Jason Wade sings the main chorus of the song.|pos= left|format=[[Ogg]]}}
The song was written by Jason Wade, who originally finished the song in a hotel room after he visited a friend in Nashville who needed a kidney transplant.<ref name="Writing Credits">{{cite web|url=http://www.allmusic.com/song/broken-t10621629|title=Broken|accessdate=2011-07-16|publisher=[[Rovi Corporation]]}}</ref><ref name="Contactmusic.com">{{cite web|url=http://www.contactmusic.com/info/lifehouse|title=Lifehouse &ndash; Biography, Photos, News, Videos, Reviews and Tour Dates and Tickets|accessdate=2011-07-12|publisher=[[Contactmusic.com]]}}</ref> It was produced by John Fields and Scott Faircloff.<ref name="Producers">{{cite web|url=http://www.discogs.com/Lifehouse-Who-We-Are/release/998516|title=Lifehouse &ndash; Who We Are (CD, Album) at Discogs|accessdate=2011-08-06|publisher=[[Discogs]]}}</ref> In an interview with [[Alternative Addiction]], Wade said, "Broken is the song that moves me the most. I wrote it in Nashville, about 6 months ago. I was in my hotel room at 3 AM, I was out there visiting a friend... he’s really sick, he needs a kidney transplant."<ref name="Inspiration">{{cite web|url=http://www.lifehousemusic.com/news/default.aspx?nid=15400|title=Lifehouse : News: Lifehouse Announces A Third Single|accessdate=2011-07-16|publisher=[[Geffen Records]]}}</ref> Furthermore, he said, "That [the] song just kinda fell in my lap, and I really haven’t written a song like that since '[[Hanging by a Moment]]' where it just happened within a 15 minute time period."<ref name="Inspiration"/> Broken was released as the third single from the album on June 18, 2007.<ref name="Who We Are - Release">{{cite web|url=http://www.lifehousemusic.com/news/default.aspx?nid=12047|title=Lifehouse Releases "Who We Are"|accessdate=2011-06-18|publisher=Geffen Records}}</ref> The song was then edited for radio to give it more of a "rock" feel and was released via digital download on July 8, 2008.<ref name="New Radio Version">{{cite web|url=http://www.lifehousemusic.com/news/default.aspx?&in=124|title=Lifehouse : News: "Broken" Available on iTunes|accessdate=2011-07-16|publisher=Geffen Records}}</ref><ref name="Digital Release">{{cite web|url=http://itunes.apple.com/us/album/broken-new-radio-version-single/id284299014|title=Broken (New / Radio Version &ndash; Single by Lifehouse &ndash;|accessdate=2011-07-16|publisher=[[Apple Inc.]]}}</ref>

"Broken" was described as a [[Pop music|pop]]-[[Rock and roll|rock]] song with influences of [[Adult album alternative|adult alternative]].<ref name="Genre">{{cite web|url=http://itunes.apple.com/us/album/broken-new-radio-version-single/id284299014|title=Broken (New / Radio Version &ndash; Single by Lifehouse|accessdate=2011-07-16|publisher=Apple Inc.}}</ref><ref name="Genre Influences">{{cite web|url=http://new.music.yahoo.com/lifehouse/albums/broken--160068310|title=Broken by Lifehouse|accessdate=2011-07-16|publisher=[[Yahoo! Inc.]]}}</ref> According to the sheet music published at Musicnotes.com by [[Sony/ATV Music Publishing]], the song is set in [[common time]] with a slow tempo of 69 [[beats per minute]].<ref name="Composition">{{cite web|url=http://www.musicnotes.com/sheetmusic/mtdFPE.asp?ppn=MN0074538&|title=Lifehouse &ndash; Broken Sheet Music (Digital Download)|accessdate=2011-07-16|publisher=[[Sony/ATV Music Publishing]]}}</ref> It is composed in the key of [[C major]] with Wade's vocal range spanning from the low-note of G<sub>3</sub> to the high-note of G<sub>5</sub>.<ref name="Composition"/> The song has a basic [[chord progression]] of F<sub>2</sub>–Am<sub>7</sub>–F<sub>2</sub>–Am<sub>7</sub>–F<sub>2</sub>–Am<sub>7</sub>–C/G–G.<ref name="Composition"/>  Alex Lai of [[Contactmusic.com]] described as a song "[that] features great string use and takes a tender tone".<ref name="Contactmusic.com Review">{{cite web|url=http://www.contactmusic.com/new/home.nsf/webpages/lifehousex31x07x07|title=Who We Are Album Review|accessdate=2011-07-16|publisher=Contactmusic.com}}</ref> A review from [[Alternative Addiction]] said that the song was one of the best Wade has ever written and noted "[that] the more you listen to it, the better it gets."<ref name="Alternative Addiction Review">{{cite web|url=http://www.alternativeaddiction.com/newmusic/exec/albumreviews.asp?id=395|title=Review of "Who We Are" by Lifehouse|accessdate=2011-07-16|publisher=Alternative Addiction LLC}}</ref>

==Chart performance==
In the United States, "Broken" debuted at number 95 on the [[Billboard Hot 100|''Billboard'' Hot 100]] for the week of November 8, 2008.<ref name="Broken - Debut">{{cite web|url={{BillboardURLbyName|artist=lifehouse|chart=all}}|title=Broken &ndash; Lifehouse|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=2011-07-16}}</ref> It fluctuated on the chart for several weeks, before reaching its peak of 83.<ref name="Broken - Billboard Hot 100 Peak">{{cite web|url={{BillboardURLbyName|artist=lifehouse|chart=Hot 100}}|title=Lifehouse Album & Song Chart History (Billboard Hot 100)|work=Billboard|publisher=Prometheus Global Media|accessdate=2011-06-23}}</ref> The song debuted at number 70 on the ''Billboard'' [[Hot Digital Songs|Digital Songs]] chart and peaked at number 38 after being on the chart for three non-consecutive weeks.<ref name="Broken - Digital Songs Peak">{{cite web|url={{BillboardURLbyName|artist=lifehouse|chart=Digital Songs}}|title=Lifehouse Album & Song Chart History (Digital Songs)|work=Billboard|publisher=Prometheus Global Media|accessdate=2011-07-16}}</ref> On the ''Billboard'' [[Adult Top 40|Adult Pop Songs]] chart, the song debuted at number 38 for the week of August 2, 2008 and after moving around the chart for 32 consecutive weeks, the song eventually peaked at number seven.<ref name="Broken - Adult Pop Songs Peak">{{cite web|url={{BillboardURLbyName|artist=lifehouse|chart=Adult Pop Songs}}|title=Lifehouse Album & Song Chart History (Adult Pop Songs)|work=Billboard|publisher=Prometheus Global Media|accessdate=2011-07-16}}</ref> The song was certified [[List of music recording certifications#Singles|Platinum]] by the [[RIAA]] in January 2013.<ref name="US cert"/>

The song also charted internationally. "Broken" debuted at number 92 on the [[Canadian Hot 100]] for the week of January 26, 2008.<ref name="Broken - Debut"/> On July 26, 2008, the song reappeared in the chart and peaked at number 84.<ref name="Broken - Canadian Hot 100 Peak">{{cite web|url={{BillboardURLbyName|artist=lifehouse|chart=Billboard Canadian Hot 100}}|title=Lifehouse Album & Song Chart History (Canadian Hot 100)|work=Billboard|publisher=Prometheus Global Media|accessdate=2011-07-16}}</ref> It debuted at number 40 on the [[New Zealand Singles Chart]] for the week of May 26, 2008.<ref name="Broken - New Zealand">{{cite web|url=http://www.charts.org.nz/showitem.asp?interpret=Lifehouse&titel=Broken&cat=s|title=charts.org.nz &ndash; Lifehouse &ndash; Broken|publisher=Hung Medien|accessdate=2011-07-16}}</ref> After being on the chart for 13 consecutive weeks, the song eventually peaked at number 21.<ref name="Broken - New Zealand"/>

==Promotion==

===Music video===
[[File:Broken Music Video.png|thumb|250px|right|Lifehouse lead singer Jason Wade looks on the scene of a deadly car crash.]]

The music video for "Broken" was filmed on August 24, 2008 in [[Los Angeles, California|Los Angeles]] and was directed by [[Kiefer Sutherland]] and Frank Borin.<ref name="Music Video Filming">{{cite web|url=http://www.lifehousemusic.com/news/default.aspx?&in=108|title=Lifehouse : News: The Making of "Broken"|publisher=Geffen Records|accessdate=2011-07-17}}</ref> It first premiered on [[VH1|VH1.com]] on September 16, 2008.<ref name="Music Video Premiere">{{cite web|url=http://www.lifehousemusic.com/news/default.aspx?&in=104|title=Lifehouse : News: Music Video For "Broken" directed by Kiefer Sutherland and Frank Borin Premieres on VH1.com|publisher=Geffen Records|accessdate=2011-07-17}}</ref> Wade and Sutherland then appeared together on the ''[[VH1 Top 20 Video Countdown]]'' on September 20, 2008 to debut the music video on [[VH1]].<ref name="Music Video Premiere"/> In an interview with [[Ultimate Guitar]], Wade described his experience with working with Sutherland by saying, "Whilst listening to the song a little while back, Kiefer came up with the video's concept. Working with him was a great experience; with all of the expressions and very moments, Kiefer was extremely hands on. With him handling direction, acting in the music video was extremely easy."<ref name="Music Video Interview'">{{cite web|url=http://www.lifehousemusic.com/news/default.aspx?&in=89|title=Lifehouse : News: Lifehouse: 'We Want to Move Forward and Push Ourselves|publisher=Geffen Records|accessdate=2011-07-17}}</ref>

The video starts with Wade in the drivers seat of a car, with the other members of Lifehouse in the car with him. The traffic is at a stand still in a tunnel, as a swarm of people have abandoned their vehicles and are walking through. Wade and the band members get out of the car, and begin walking in the opposite direction of the crowd to the other side of the tunnel. Wade pushes through the people, who are in a rush to get to the entrance of the tunnel and is constantly focused on his destination to get to the end. When he reaches the tunnel's end, he sees the wreckage of a car crash with firefighters and paramedics cleaning up the mess. Wade then sees a girl observing what appears to be herself, dead inside one of the crashed car. He then looks to the left, and sees an old man observing himself, dead on the ground. As Wade looks into the other car, he sees himself, also seemingly dead. Upon this, he frantically runs back into the tunnel towards his car. He sees himself in the car with the band members, however everyone seems to be frozen as he bangs on the windows. Suddenly he's inside the car again, and a police officer comes to the window to motion him to continue driving, since he is stopped in the middle of the tunnel for no apparent reason.

===Live performances===
Lifehouse performed "Broken" along with other songs on ''Nissan Live Sets on Yahoo! Music'' in 2007.<ref name="Nissan Live Sets">{{cite web|url=http://new.music.yahoo.com/lifehouse/albums/nissan-live-sets-lifehouse--44650928;_ylt=AmCtlHtPFaoGqxkQ9m5b1iwOxCUv|title=Nissan Live Sets: Lifehouse|publisher=Yahoo! Music|accessdate=2011-08-06}}</ref> On October 15, 2007, Lifehouse performed an acoustic version of the song on [[VH1]].<ref name="VH1">{{cite web|url=http://www.vh1.com/video/misc/181496/broken- unplugged.jhtml|title="Broken (Unplugged)" by Lifehouse|publisher=[[MTV Networks]]|accessdate=2011-08-06}}</ref> In January 2008, the band performed the song as part of a [[set list]] on the [[Public Broadcasting Service|PBS]] show [[Soundstage (TV series)|''Soundstage'']].<ref name="Soundstage">{{cite web|url=http://www.pbs.org/wttw/soundstage/lifehouse/setlist.htm|title=Soundstage . Lifehouse . Setlist|publisher=[[Public Broadcasting Service|PBS]]|accessdate=2011-08-06}}</ref> The song was also performed live at the [[Pinkpop Festival#Pinkpop 2011|2011 Pinkpop Festival]] on June 11, 2011, as part of a set list of songs at the festival.<ref name="Pinkpop Festival">{{cite web|url=http://www.efestivals.co.uk/festivals/pinkpop/2011/|title=PinkPop 2011|publisher=[[EFestivals|eFestivals.co.uk]]|accessdate=2011-08-06}}</ref>

===Media appearances===
"Broken" was featured on ''[[General Hospital]]'', ''[[Criminal Minds]]'', ''[[Grey's Anatomy]]''<ref>{{cite web|title=Grey's Anatomy: Lay Your Hands on Me|url=http://www.tv.com/shows/greys-anatomy/lay-your-hands-on-me-1151527/|work=TV.com|accessdate=September 18, 2011|date=January 2008}}</ref> and [[One Tree Hill (TV series)|''One Tree Hill'']].<ref>{{cite web|title=Music of episode 5.09 - "For Tonight You're Only Here To Know"|url=http://www.oth-music.com/episode509.html|publisher=One Tree Hill Music|accessdate=September 18, 2011|date=February 2008}}</ref>

The song was used in the trailer for ''[[The Time Traveler's Wife (film)|The Time Traveler's Wife]]''.

===Cover versions===
[[Daniel Evans (singer)|Daniel Evans]], a finalist on series 5 of [[The X Factor (UK)]] recorded a mellow contemporary version and was a standout track on his 2013 iTunes EP "Reflections"

In 2016, [[Trisha Yearwood]] covered the song for the soundtrack to [[The Passion (U.S.)|the U.S. version of ''The Passion'']] and performed the song during the live broadcast for the special. Her version peaked at number 48 on the ''Billboard'' [[Hot Christian Songs]] chart, and number 17 on the Billboard [[Adult Contemporary (chart)|Adult Contemporary]] chart<ref>{{cite web|url={{BillboardURLbyName|artist=trisha yearwood|chart=Christian Songs}}|title=Trisha Yearwood Album & Song Chart History: "Christian Songs"|work=[[Billboard (magazine)|Billboard]]|publisher=[[Nielsen Business Media, Inc]]|accessdate=March 10, 2016}}</ref>

==Credits and personnel==
*[[Songwriting]] &ndash; Jason Wade
*[[Record producer|Production]] &ndash; Jason Wade, Jude Cole
*[[Audio mixing (recorded music)|Mixing]] &ndash; [[Thom Panunzio]], [[Jack Joseph Puig]], Florian Ammon, Keith Armstrong, Dean Nelson, Jeff Rothschild
*[[Audio engineering|Engineering]] &ndash; Florian Ammon, Scott Faircloff, Ross Hogarth, Will Sandalls

Credits and personnel adapted from [[Allmusic]].<ref name="Credits and personnel">{{cite web|url=http://www.allmusic.com/album/who-we-are-r1075403/credits/name-desc|title=Who We Are &ndash; Lifehouse|accessdate=2011-07-17|publisher=Rovi Corporation}}</ref>

==Charts==
{| class="wikitable sortable"
|-
! Chart (2008–2009)
! Peak<br>position
|-
{{singlechart|Billboardcanadianhot100|84|artist=Lifehouse|artistid=32520|accessdate=2011-07-16}}
|-
{{singlechart|New Zealand|21|artist=Lifehouse|song=Broken|accessdate=2011-07-16}}
|-
{{singlechart|Billboardhot100|83|artist=Lifehouse|artistid=32520|accessdate=2011-07-16}}
|-
{{singlechart|Billboardadultpopsongs|7|artist=Lifehouse|artistid=32520|accessdate=2011-07-16}}
|-
|}

==Certifications and sales==
{{Certification Table Top}}
{{Certification Table Entry|region=United States|award=Platinum|title=Broken|type=single|artist=Lifehouse|salesamount=1,000,000|refname="US cert"}}
{{Certification Table Bottom}}

==References==
{{reflist|2}}

==External links==
* [https://www.youtube.com/watch?v=I6cdPeYJh0s "Broken" Music Video] on [[YouTube]]
* {{MetroLyrics song|lifehouse|broken}}<!-- Licensed lyrics provider -->

{{Lifehouse}}

{{DEFAULTSORT:Broken (Lifehouse Song)}}
[[Category:2008 singles]]
[[Category:Lifehouse (band) songs]]
[[Category:Songs written by Jason Wade]]
[[Category:Rock ballads]]
[[Category:2007 songs]]
[[Category:Geffen Records singles]]
[[Category:Trisha Yearwood songs]]