<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = S.E.1
  |image = RAESE1.jpg
  |caption = 
}}{{Infobox Aircraft Type
  |type = Experimental research aircraft
  |manufacturer = [[Royal Aircraft Establishment|Army Balloon Factory]]
  |designer = [[Geoffrey de Havilland]], [[F.M. Green]]
  |first flight = 11 June 1911
  |introduced = 
  |retired =
  |status =
  |primary user = 
  |more users = 
  |produced = 
  |number built = 1
  |unit cost = 
  |variants with their own articles = 
}}
|}
The S.E.1 ('''''S'''antos '''E'''xperimental'') was an experimental aircraft built at the Army Balloon Factory at Farnborough (later the [[Royal Aircraft Establishment|Royal Aircraft Factory]]) in 1911. Its place in aviation history is mainly that it was the first in the series of Royal Aircraft Factory designs - several of which played an important role in [[World War I]].

==Design and Fate==

In 1910 the Army Balloon Factory was not actually authorised to design or build aircraft, but only to repair them. When the remains of a crashed [[Blériot XII]] monoplane (nicknamed "The Man-Killer" owing to its poor handling) belonging to the army were sent from Larkhill to Farnborough for repair, authorisation for a complete reconstruction was sought, and granted.<ref name="Harep269">Hare 1990, p. 269.</ref><ref name="JarrettAEp1">Jarrett ''Air Enthusiast'' Forty-two, p. 1.</ref>

The result was a completely new design. A [[tractor configuration|tractor]] [[monoplane]] became a [[pusher configuration|pusher]] [[biplane]] with large balanced fore-elevators, similar in basic layout to the [[Wright Flyer]], but with a fully covered fuselage. [[Ailerons]] were fitted to the top wing, and twin balanced rudders were mounted behind the propeller, but out of its immediate slipstream. The only obvious component of the Bleriot that found its way into the new design was its 60&nbsp;hp (45&nbsp;kW) [[E.N.V. Type F]] engine.

The S.E.1 made its first flight, a straight mile in the hands of its designer [[Geoffrey de Havilland]] on 11 June 1911.<ref name="Jackson">{{Harvnb|Jackson|1978|pages=38–9}}</ref>  Further fight testing revealed control problems and the area of the front wing/elevator was adjusted to try to bring together the centre of pressure and the hinge line and make the S.E.1 stable in pitch. By the beginning of August the front surface was fixed and carried a conventional trailing edge elevator.<ref name="Jackson"/>  An attempt to improve the turning characteristics was made by stripping the side covering of the nacelle to reduce side area.  de Havilland continued to fly the S.E.1 until 16 August. On 18 August the aircraft was flown by the rather inexperienced pilot Lt. Theodore J. Ridge, Assistant Superintendent at the factory (whose previous experience was chiefly with dirigibles, and had only been awarded his Pilot's certificate the day before, and was described as "an absolutely indifferent flyer").<ref name="Jarrett p213-4">Jarrett 2002, pp. 213–214.</ref> Both "the designer" and a factory engineer warned him against flying it. The combination of the inexperienced pilot and the marginally controllable aircraft proved fatal - while landing he made a sharp turn with the engine off; the S.E.1 stalled and spun in, killing Ridge.<ref name="Jackson"/><ref>''Flight'' p741</ref>

No attempt to rebuild the S.E.1 was made, and the design was apparently abandoned, no attempt being made to develop it. The [[Royal Aircraft Factory S.E.2|S.E.2]] of 1913 was a completely different kind of aeroplane - a development of the B.S.1.

==Specifications==

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=de Havilland Aircraft since 1909 <ref name="Jackson"/>
|length main=29 ft  
|length alt=8.84 m
|span main=37 ft 6 in 
|span alt=11.43 m
|height main=11 ft 6 in 
|height alt=3.51 m
|area main= 400 ft²
|area alt= 37.16 m²
|airfoil=
|loaded weight main=1,200 lb. 
|loaded weight alt=544.31 Kg
|engine (prop)=[[E.N.V.|E.N.V type F]]
|type of prop=
|number of props=1
|power main= 60 hp
|power alt=45 kW
|power original=
 }}
*  No performance figures available

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* {{cite |journal=Flight |date=26 August 1911 |url=https://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200739.html |page=741 |title=The Fatal Accident at Aldershot }}
*{{cite book|last= Hare|first=Paul R   |title= The Royal Aircraft Factory|year=1990 |publisher= Putnam|location=London |isbn=0-85177-843-7}}
*{{cite book |title= de Havilland Aircraft since 1909|last= Jackson|first=A.J.| year=1978 |publisher=Putnam Publishing |location=London |isbn=0-370-30022-X }}
*{{cite magazine |last1=Jarrett |first1=Philip |year=1991 |title=Farborough's First: The story of de Havilland's S.E.1 |magazine=[[Air Enthusiast]]  |issue=Forty-two |pages=1–10| issn=0143-5450 }}
*{{cite book |last1= Jarrett|first1= Philip |editor1-first=Philip |editor1-last=Jarrett |editor1-link= |others= |title= Pioneer Aircraft:Early Aviation before 1914 |year=2002  |publisher=Putnam |location= London|isbn= 0-85177-869-0 |chapter= Making Flying Safer|pages=202–215}} 
*Lewis, Peter ''British Aircraft 1809-1914'' London, Putnam, 1962
{{refend}}

==External links==
{{commons category|Royal Aircraft Factory S.E.1}}

{{Royal Aircraft Factory aircraft}}

[[Category:British experimental aircraft 1910–1919]]
[[Category:Pusher aircraft]]
[[Category:Canard aircraft]]
[[Category:Royal Aircraft Factory aircraft|SE01]]