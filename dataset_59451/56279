{{Infobox journal
| title = Clinical Lymphoma, Myeloma & Leukemia
| cover =
| formernames = Clinical Lymphoma; Clinical Lymphoma & Myeloma
| discipline = [[Oncology]]
| abbreviation = Clin. Lymphoma Myeloma Leuk.
| editor = Bruce D. Cheson, Jorge E. Cortés, Sundar Jagannath
| publisher = [[Elsevier]]
| country = 
| impact = 2.02
| impact-year = 2014
| frequency = Bimonthly
| history = 2000-present
| website = http://www.clinical-lymphoma-myeloma-leukemia.com/
| link1 = http://www.sciencedirect.com/science/journal/21522650
| link1-name = Online access
| link2 = http://www.journals.elsevier.com/clinical-lymphoma-myeloma-and-leukemia
| link2-name = Journal page at publisher's website
| ISSN = 2152-2650
| eISSN = 2152-2669
| LCCN = 2009207906
| OCLC = 475047678
}}
'''''Clinical Lymphoma, Myeloma & Leukemia''''' is a [[peer-reviewed]] [[medical journal]] published by [[Elsevier]] (previously by [[CIG Media Group]]). It was established as ''Clinical Lymphoma'' in 2000, renamed to ''Clinical Lymphoma & Myeloma'' in 2005 and obtained its current name in 2010. The journal covers research on detection, diagnosis, prevention, and treatment of [[lymphoma]], [[myeloma]], [[leukemia]], and related disorders, including [[macroglobulinemia]], [[amyloidosis]], and plasma-cell [[dyscrasia]]s.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Index Medicus]]/[[MEDLINE]]/[[Pubmed]], [[EMBASE]], [[Excerpta Medica]], [[Current Contents]]/Clinical Medicine, [[CINAHL]], [[Chemical Abstracts]], [[Scopus]], and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.02.<ref name=WoS>{{cite book |year=2015 |chapter=Clinical Lymphoma, Myeloma & Leukemia |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== Article types ==
The journal publishes editorials, original research papers, reviews, current treatment reports, case reports, brief communications, current trials, translational medicine pieces, and a "Meeting Highlights" section.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.clinical-lymphoma-myeloma-leukemia.com/}}

{{DEFAULTSORT:Clinical Lymphoma, Myeloma and Leukemia}}
[[Category:Oncology journals]]
[[Category:Publications established in 2000]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Hematology journals]]
[[Category:Elsevier academic journals]]