{{Infobox alpine ski racer
| name             = Michel Daigle
| gender           = M
| image            = Michel Daigle Portrair.jpg
| caption          = Michel Daigle in 1974
| disciplines      = Moguls, aerials, ballet
| birth_date       = {{birth date and age|1950|9|20|df=y}}
| birth_place      = Montreal, Quebec, Canada
| death_date       = <!-- {{death date and age|YYYY|MM|DD|df=y}} -->
| death_place      = 
| height           = <!-- X ft Y in, X cm OR X m; the template will automatically convert -->
| weight           = <!-- X st Y lb, X lb OR X kg; the template will automatically convert -->
| wcdebut          = 
| retired          = 
| website          = <!-- {{URL|example.com}} -->
| olympicteams     = 
| olympicmedals    = 
| olympicgolds     = 
| paralympicteams  = 
| paralympicmedals = 
| paralympicgolds  = 
| worldsteams      = 
| worldsmedals     = 
| worldsgolds      = 
| wcseasons        = 
| wcwins           = 
| wcpodiums        = 
| wcoveralls       = 
| wctitles         = 
| medaltemplates   = 
| show-medals      = 
| updated          = 
}}'''Michel Daigle''' (born September 20, 1950) is a pioneer of [[freestyle skiing]].<ref>{{Cite book|title=Cent Ans de Ski au Québec|last=Soucy|first=Danielle|publisher=Éditions La Presse|year=2009|isbn=2923681231|location=Montreal, Quebec, Canada|pages=}}</ref><ref>{{Cite news|url=http://www.ledevoir.com/culture/livres/279110/l-epopee-du-ski-au-quebec|title=L'épopée du ski au Québec, Le Devoir|last=|first=|date=12 December 2009|work=|access-date=|via=}}</ref><ref name="rds.ca">{{Cite web|url=http://www.rds.ca/pantheon/serie-olympique-le-ski-acrobatique-1.309797|title=Série olympique, le ski acrobatique|website=RDS.ca|access-date=2016-03-21}}</ref><ref>{{Cite book|title=Sports: The Complete Visual Reference|last=|first=|publisher=QA International|year=2000|isbn=9782764408971|location=Montreal, Quebec|pages=195}}</ref><ref>{{Cite news|url=http://blogues.lapresse.ca/ski/2012/01/19/sarah-burke-et-les-grands-champions-canadiens-du-ski-acrobatique/|title=Sarah Burke et les grands champions Canadiens du ski acrobatique, La Presse|last=Guy|first=Thibaudeau|date=19 January 2012|work=|access-date=27 March 2016|via=}}</ref><ref>{{Cite news|url=|title=Le Journal de Montréal|last=Dubuc|first=Hélène|date=26 February 2010|work=Vancouver 2010, ski acrobatique|access-date=|via=}}</ref> During his career, Daigle reached the winners podium 34 times. He is considered one of the founders of freestyle skiing, alongside Darryl Bowie and John Johnston<ref>{{Cite web|url=http://ashfm.ca/absportslibrary/snow-sports/freestyle-skiing/introduction|title=Freestyle Skiing Introduction, Alberta Sports History Library|last=|first=|date=|website=|publisher=|access-date=}}</ref><ref name=":0">{{Cite news|url=|title=Hot-Dogging|last=Sagi|first=Douglas|date=|work=The Vancouver Sun|access-date=|via=}}</ref><ref name=":1">{{Cite news|url=|title=Michel Daigle a choisi le ski acrobatique pour gagner sa vie|last=Roy|first=François|date=1974|work=Le Soleil|access-date=|via=}}</ref> and has been credited with the growth of freestyle skiing in the mid 1970s. Daigle competed in [[ski ballet]], [[Mogul skiing|moguls]] and [[Aerial skiing|aerials]].

Daigle also launched the first freestyle ski camps in Canada and built the first water ski ramps in the country. His camps and water ramps were instrumental in helping train a number of moguls, ballet and aerials skiers who went on to become world champions, in turn helping position Canada as a country that has dominated [[freestyle skiing]] since the early 1970's.

He is the inventor of the Daigle Banger and starred in [[Warren Miller (director)|Warren Miller]]'s 1972 film, Winter People. Daigle co-authored ''Free Style Skiing'' in 1974<ref name=":0" /><ref name=":2">{{Cite news|url=|title=Michel Daigle au Mont Echo|last=Vanasse|first=Jacques|date=1975|work=Journal de Montréal|access-date=|via=}}</ref><ref>{{Cite journal|last=|first=|date=November 1975|title=Ski Magazine|url=https://books.google.ca/books?id=qoU3qkNUHbAC&pg=PA31&lpg=PA31&dq=michel+daigle+ski&source=bl&ots=XJH28rPAOM&sig=LQAuvTXno6aTh9Tfydi80rYz37Y&hl=en&sa=X&redir_esc=y#v=onepage&q=michel%20daigle%20ski&f=false|journal=Ski Magazine|doi=|pmid=|access-date=27 March 2016}}</ref><ref>{{Cite news|url=|title=Champion du ski "style libre" - Michel Daigle... entre ciel et terre|last=Desrosiers|first=Réal|date=13 October 1974|work=Journal de Montréal|access-date=|via=}}</ref><ref name=":11">{{Cite news|url=|title=En été au sports d'hiver|last=Dumesnil|first=Thérèse|date=2 October 1976|work=La Presse - Perspectives|access-date=|via=}}</ref> and in 1999 was inducted into the Laurentian Ski Hall of Fame.<ref>{{Cite web|url=http://www.museeduskideslaurentides.com/index.php?q=en/node/236|title=Laurentian Ski Museum Hall of Fame|last=|first=|date=|website=|publisher=|access-date=}}</ref>

== Early ski career ==
Daigle started skiing at the age of 13 and began racing at 19 with the Division Laurentienne de Ski (DLS).<ref name=":12">{{Cite journal|last=Johnson|first=Larry|date=October 1975|title=Michel Daigle|url=|journal=Ski Canada Journal|doi=|pmid=|access-date=}}</ref> He raced for 3 years in Québec, Ontario and the Eastern United States<ref>{{Cite news|url=|title=Les risques du ski acrobatique|last=Arteau|first=Jacques|date=1976|work=Le Soleil|access-date=|via=}}</ref> and finished 2nd overall in the DLS championship in 1971.

That same year, at the end of the ski season, Daigle moved to [[Whistler, British Columbia|Whistler]] in British Columbia. There, he worked with the ski patrol and avalanche rescue team for the [[Garibaldi Lift Company]], the company that operated the ski lifts on Whistler Mountain. In April 1971, in preparation for the Whistler Mountain Gelandesprung, an aerials skiing competition, Daigle worked on the construction of the ski jumps for the event. Having tested the jumps, he was urged by colleagues to enter the competition.<ref name=":2" /><ref name=":11" /><ref name=":12" /> Daigle entered and went on to win his first professional title, taking first place and a cash prize of $500<ref name=":2" /><ref name=":12" /><ref name=":3">{{Cite web|url=|title=Streakers v skiers: Montrealer Michel Daigle wins professional hot-dog title|last=Golla|first=James|date=11 March 1974|website=|publisher=The Globe And Mail (ProQuest Historical Newspapers)|access-date=}}</ref><ref name=":4">{{Cite news|url=|title=Les voyous des pentes ne portent plus le jean|last=Ladouceur|first=Albert|date=5 January 1975|work=Montréal-Matin|access-date=|via=}}</ref><ref name=":5">{{Cite news|url=|title="Hotdog" skiing's appeal now sweeping the country|last=Swayne|first=Don|date=7 March 1975|work=The Recorder and Times|access-date=|via=}}</ref>

== Freestyle skiing career ==
In the following years, Michel Daigle went on to compete in a number of Canadian, American and European events organized by the emerging freestyle organisations, including the Canadian Freestyle Skiers Association (CFSA)<ref>{{Cite news|url=|title=Après le ski sur terre, le ski dans les... airs|last=Moisan|first=Dorik|date=1973|work=Journal de Québec|access-date=|via=}}</ref> and its American counterpart, the Professional Freestyle Association (PFA).<ref name=":5" /> Daigle competed in the three main freestyle events: moguls, aerials and ballet. Daigle won the Canadian National Freestyle Championship in 1974 and 1975.

In 1972, Daigle was featured in [[Warren Miller (director)|Warren Miller]]'s film Winter People in which he appeared during a segment on Mike Wiegele Helicopter Skiing.

Daigle was head of Team Olin and was sponsored by [[Olin Corporation|Olin Skis]] (acquired by [[K2 Sports]] in 1999) from 1974 to 1976.<ref name=":6">{{Cite journal|last=Staff writer(s); no by-line.|first=|date=March 1975|title=We're no.1|url=|journal=Ski Industry Association (now Snowsports Industries America)|doi=|pmid=|access-date=}}</ref><ref name=":6" />

== The Michel Daigle Ski Camp ==
[[File:Michel Daigle - Summer Freestyle Camp - Water Ramp 1976.jpg|thumb|x250px|Michel Daigle - 1976 Summer Freestyle Camp - Aerials training on one of the water ramps]]
Daigle hosted his first freestyle skiing camp on [[Whistler Blackcomb|Whistler Mountain]] in April 1974.<ref name=":7">{{Cite news|url=|title=Daring Daigle gains stature|last=Staff writer(s); no by-line.|first=|date=21 March 1974|work=The Province|access-date=|via=}}</ref><ref name=":8">{{Cite news|url=|title=Get hot at Whistler- freestyle camps and spring clinics|last=Staff writer(s); no by-line.|first=|date=Spring 1974|work=Garibaldi's Whistler News|access-date=|via=}}</ref> In July of the following year, he hosted the first "Michel Daigle Summer Camp" with John Eaves on Lac des Sables in [[Sainte-Agathe-des-Monts|Sainte-Agathe des Monts]], Quebec, Canada.<ref>{{Cite web|url=http://www.rds.ca/pantheon/serie-olympique-le-ski-acrobatique-1.309797|title=Série olympique, le ski acrobatique|last=|first=|date=|website=|publisher=|access-date=}}</ref><ref>{{Cite web|url=http://fqsa.com/historique/|title=Fédération Québécoise du Ski Acrobatique|last=|first=|date=|website=|publisher=|access-date=}}</ref><ref name=":13">{{Cite web|url=http://www.sportetsociete.com/actualites/159/|title=Le ski acrobatique|last=Foisy|first=Michel|date=|website=Sports et Société|publisher=|access-date=}}</ref><ref>{{Cite journal|last=Staff writer(s); no by-line.|first=|date=1976|title=Ecole de Style libre OLIN-LOOK|url=|journal=Ski Québec|doi=|pmid=|access-date=}}</ref>

The camp was the first summer skiing training facility in North America to offer aerials training on [[Aerials water ramps|water ramps]]. Over 6 day sessions, campers trained for aerials on the ramps - a 25 foot and a 40 foot high ramp - and on trampolines, and practiced ski ballet on a mechanical moving carpet.<ref name=":11" /> Close to 50 campers took part in the first session of the camp in July 1975.<ref name="rds.ca"/> In July 1976, Michel Daigle hosted a second freestyle summer camp in Quebec, during which were held the first World Summer Aerials Championships.<ref name=":11" />

A number of coaches who worked at the Michel Daigle Summer Camp were or went on to become freestyle champions. Coaches included John Eaves, Michael Abson, André Derome, Pierre Poulin, Greg Athans, Larry Thouin, Tony De Joseph, and renowned dancer and choreographer [[Margie Gillis]], who trained ballet skiers.<ref name=":11" />

Notable campers include multiple freestyle world champion [[Marie-Claude Asselin]],<ref>{{Cite news|url=|title=Daigle's summer freestyle ski camp serves as a great winter warm-up|last=Bruineman|first=Margie|date=27 July 1978|work=News & Chronicle|access-date=|via=}}</ref> Canadian, British and world champion Mike Nemesvary, Yves Laroche, Dominique Laroche and [[Philippe LaRoche]],<ref name=":13" /> who went on to win 18 World Cup titles, an Olympic gold medal at the [[1992 Winter Olympics]] in [[Albertville]] and a silver medal at the [[1994 Winter Olympics]] in [[Lillehammer]]. Laroche was a founding member of the "Quebec Air Force".

Bob Salerno, a fellow Team Olin freestyler, went on to build the first water ramp training facility in the US at Nordic Valley in 1978.

== The Daigle Banger ==
Daigle invented the Daigle Banger,<ref name=":4" /> a ski ballet manoeuvre that involves a front flip and rotation during which the skier's uphill hand or arm comes into contact with the snow, and pushes off once the front flip has been landed. The Daigle Banger became a widely used [[ski ballet]] manoeuvre during competition in the 1980s and 1990s.<ref>{{Cite web|url=http://www.sportquick.com/articles/ski-freestyle-000062/tout-savoir-sur-le-ski-freestyle-003999.html#.Vv8VRRIrJE4|title=Esquí Artístico - Reglas Generales para la Puntuación - Manuel de Jueces|last=|first=|date=|website=|publisher=|access-date=}}</ref><ref>{{Cite web|url=http://www.sportquick.com/articles/ski-freestyle-000062/tout-savoir-sur-le-ski-freestyle-003999.html#.Vv8VRRIrJE4|title=Ski Freestyle - Tout savoir sur le ski freestyle|last=|first=|date=|website=|publisher=|access-date=}}</ref>
<gallery>
File:Michel Daigle - Daigle Banger - 1.jpg|1
File:Michel Daigle - Daigle Banger - 2.jpg|2
File:Michel Daigle - Daigle Banger - 3.jpg|3
File:Michel Daigle - Daigle Banger - 4.jpg|4
File:Michel Daigle - Daigle Banger - 5.jpg|5
</gallery>

== Selected freestyle skiing results ==
[[File:Michel Daigle - Whistler - 1971.png|thumb|x400px|Michel Daigle completing a double front flip at the 1971 Whistler Gelandesprung. Daigle finished in 1st place.]]
* 1971
** 1st place, Gelandesprung Aerial Championship, Whistler, British Columbia, Canada<ref name=":8" /><ref>{{Cite news|url=|title=Edmonton Journal|last=Vann|first=Mike|date=22 November 1973|work=|access-date=|via=}}</ref><ref name=":9">{{Cite journal|last=Staff writer(s); no by-line.|first=|date=1974|title=Canadian Freestylers Championship|url=|journal=Ski Canada Journal|doi=|pmid=|access-date=}}</ref>
* 1972
** 1st place, Eastern Aerial National Exhibition Skiing Championship, Waterville Valley, NH USA<ref name=":1" /><ref name=":12" /><ref name=":8" /><ref>{{Cite journal|last=Staff writer(s); no by-line.|first=|date=November 1972|title=National Exhibition Skiing Championships|url=|journal=Skiing Magazine|doi=|pmid=|access-date=}}</ref><ref>{{Cite news|url=|title=The Albertan|last=Staff writer(s); no by-line.|first=|date=18 November 1972|work=|publisher=|newspaper=The Albertan|access-date=|via=}}</ref><ref name=":10">{{Cite news|url=|title=Hotdogging practice paid off  - Winner worked on weakness|last=Coates|first=Len|date=1974|work=|publisher=Star|access-date=|via=}}</ref>
* 1973
** 2nd place, Labatt's National Aerial Championship, Whistler, British Columbia, Canada<ref name=":8" /><ref name=":9" />
** 1st place, Canadian Eastern National Freestyle Skiing Championship Mont-Cascades, Quebec, Canada<ref name=":9" />
** 1st place, Canadian Gelande Championships, Silver Star Mountain, Vernon, British Columbia<ref name=":1" /><ref name=":8" /><ref name=":9" />
** 1st place, Quebec Freestyle Championship, Le Relais, Quebec, Canada
* 1974
** 1st place, Canadian Eastern National Freestyle Skiing Championship, Mont-Cascades, Quebec, Canada<ref name=":1" /><ref name=":7" /><ref>{{Cite news|url=|title=Hot Dog Skiing - It's showbiz|last=Ferguson|first=Bob|date=1974|work=Ottawa Citizen|access-date=|via=}}</ref>
** 1st place, Canadian National Freestyle Championship Blue Mountain, Ontario, Canada<ref name=":3" /><ref name=":7" /><ref name=":9" /><ref name=":10" /><ref>{{Cite news|url=|title=Hot Dogging at Blue Mountain|last=|first=|date=11 March 1974|work=|publisher=The Toronto Star|access-date=|via=}}</ref>
** 2nd place, Kimberley Professional Freestyle Championship, Kimberley British Columbia, Canada<ref name=":8" />
** 5th place, Olympia Northwest U.S. Freestyle Classic, Alpental, Washington, USA
** 6th place, National Aerial Acrobatic Gelandy and Freestyle Championship, Whistler, British Columbia
** 2nd place, Professional Freestyle Association, Midas U.S. Freestyle Open, Aspen Highlands, Colorado, USA<ref name=":1" /><ref>{{Cite news|url=|title=Olin Freestylers Take Off!|last=|first=|date=January 1975|work=Ski Industry Association (now Snowsports Industries America)|access-date=|via=}}</ref>
* 1975
** 1st place, Canadian National Freestyle Championship
** 1st place, Northwest Open Freestyle Skiing Championships, Alpental, Washington, USA<ref>{{Cite news|url=|title=Canadian freestyle ski champ|last=Staff writer(s); no by-line.|first=|date=30 March 1975|work=The Seattle Times|access-date=|via=}}</ref>

== Post-skiing career ==
Following his professional skiing career, Michel Daigle held a number of positions in the sports industry, including ski and windsurfing equipment marketing and sales.<ref name=":12" /> Today, Daigle works as a workforce productivity consultant and leadership coach.<ref>{{Cite web|url=http://www.mobilisperforma.com/about-us/our-team/michel-daigle/|title=Mobilis Performa - Team bio|last=|first=|date=|website=|publisher=|access-date=}}</ref> He has coached executives from a number of Canadian and international organizations. Among other types of workshops he organises, he leads corporate groups on outdoor activities, including skiing workshops.

== References ==
{{reflist|30em}}

== Photos and external sources ==
[[c:Michel Daigle#Gallery|Michel Daigle]] on Wikimedia Commons

{{DEFAULTSORT:Daigle, Michel}}
[[Category:1950 births]]
[[Category:Canadian male freestyle skiers]]
[[Category:Sportspeople from Vancouver]]
[[Category:Living people]]