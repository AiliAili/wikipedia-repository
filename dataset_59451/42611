<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Curtiss T Wanamaker Triplane
 | image=Curtiss T Wanamaker Triplane2.jpg
 | caption=[[RNAS Felixstowe]], 1916
}}{{Infobox Aircraft Type
 | type= Patrol Bomber flying-boat<ref name="Bowers Curtiss p136-7"/>
 | national origin=[[United States of America]]
 | manufacturer=[[Curtiss Aeroplane and Motor Company]]
 | designer=
 | first flight=1916
 | introduced=
 | retired=1916
 | status=
 | primary user=Royal Naval Air Service
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Wanamaker Triplane''' or '''Curtiss Model T''', retroactively renamed '''Curtiss Model 3''' was a large [[Experimental aircraft|experimental]] four-engined [[triplane]] patrol [[flying boat]] of the [[First World War]]. It was the first four-engined aircraft built in the United States however, only a single example was completed (No.3073),<ref>{{cite web|title=Curtiss-Wanamaker Triplane|url=http://all-aero.com/index.php?option=com_content&view=article&id=2683:curtiss-wanamaker-triplane&catid=59|website=All-Aero|publisher=all-aero|accessdate=19 October 2015}}</ref> orders for a further 19 from the British [[Royal Naval Air Service]] (RNAS) being cancelled.  At the time the Triplane was the largest seaplane in the world.<ref name="Bowers Curtiss p136-7"/>

==Design and development==
In 1915, the American businessman [[Rodman Wanamaker]] who, prior to the outbreak of the First World War commissioned the [[Curtiss Aeroplane and Motor Company]] to build a large flying boat, ''[[Curtiss Model H|America]]'' to win the [[Daily Mail aviation prizes|£10,000 prize]] put forward by the British newspaper ''[[Daily Mail]]'' for the first aircraft to cross the Atlantic, commissioned [[Glen Curtiss|Curtiss]] to build a new, even larger [[flying boat]] for transatlantic flight that became known as the '''Wanamaker Triplane''', or '''Curtiss Model T''', (retroactively re-designated Model 3 when Curtiss changed its designation system).<ref name="Bowers Curtiss p136-7"/>
[[File:Curtiss Model T partially constructed.jpg|thumb|right|Partially constructed, 5 July 1916.]]
Early press reports showed a large [[triplane]], 68&nbsp;ft (17.9&nbsp;metres) and with equal-span [[interplane strut|six-bay]] wings of 133&nbsp;foot (40.5&nbsp;metre) span. The aircraft, to be capable of carrying heavy armament, was estimated to have an all-up weight of 21,450&nbsp;pounds (9,750&nbsp;kilogrammes) and was to be powered by six 140&nbsp;hp 104&nbsp;kW) engines driving three propellers, two of which were to be of [[tractor configuration]] and the third a [[pusher configuration|pusher]].<ref name="Flight 1916">''Flight'' 13 January 1916, pp. 43–44.</ref>

The British [[Royal Naval Air Service]] (RNAS) placed an order for 20 of the new triplanes, the first one being completed at the Curtiss factory, [[Buffalo, New York]] in July 1916.<ref>{{cite book|last1=Trimble|first1=William|title=Hero of the Air: Glenn Curtiss and the Birth of Naval Aviation|date=2013|publisher=Naval Institute Press|isbn=1612514111|url=https://books.google.co.uk/books?id=HUX9hjHe6pkC&pg=RA1-PT21&lpg=RA1-PT21&dq=wanamaker+triplane&source=bl&ots=bvAHQBMQIV&sig=CbSOeP58X0e-DR4L6WLhb9QEfd4&hl=en&sa=X&ved=0CEoQ6AEwCWoVChMIurerja7ExwIVZY7bCh0JaQh3#v=onepage&q=wanamaker%20triplane&f=false|accessdate=24 August 2015|chapter=9 Challenges Old and New}}</ref> This was the first four-engined aircraft to be built in the United States and one of the largest aircraft in the world.<ref name="Aerofiles"/> 
[[File:Curtiss Model T partially constructed_2.jpg|thumb|left|Fitted with a Rolls-Royce engine at RNAS Felixstowe.<ref>{{cite web|title=Felixstowe and Lowestoft Seaplanes 1914–1918|url=https://www.youtube.com/watch?v=ul_bwwvEib8|website=YouTube|accessdate=14 November 2016|date=11 April 2009}}</ref>]]
While of similar size and weight to the aircraft discussed in the press, the Model T had unequal span wings, with the upper wing having a span of 134&nbsp;feet, while it was planned to be powered by four tractor 250&nbsp;hp (187&nbsp;kW) [[Curtiss V-4]] engines installed, unusually for the time, individually on the middle wing. The two pilots and flight engineer were provided with an enclosed cabin, similar to the ''America'', while to reduce control loads, small windmills could be connected to the [[aileron]] cables by electrically operated clutches to act as a form of power assisted controls.<ref name="Bowers Curtiss p136-7"/>

The planned Curtiss V-4 engines were not available when the prototype was completed, so it was not flown in the United States, but taken to England by ship and reassembled at the [[Seaplane Experimental Station|naval air station]], [[Felixstowe]] being fitted with four French 240&nbsp;hp (180&nbsp;kW) Renault engines. Although later refitted with four 250&nbsp;hp [[Rolls-Royce Eagle]]s, it was unsuccessful and damaged beyond repair on its maiden flight;<ref>{{cite book|last1=Johnson|first1=E. R.|title=American Flying Boats and Amphibious Aircraft: An Illustrated History|date=2009|publisher=McFarland|isbn=0786457082|page=302|url=https://books.google.co.uk/books?id=AtqOSxG9N1YC&pg=PA302&dq=T+Triplane&hl=en&sa=X&ved=0CDAQ6AEwA2oVChMImrfp7pzExwIV8RbbCh3z-g8L#v=onepage&q=T%20Triplane&f=false|accessdate=24 August 2015}}</ref> the order for the remaining nineteen was cancelled. It did, however, provide the inspiration for [[John Cyril Porte|John Porte]] of the Seaplane Experimental Station to build a massive five-engined flying boat of similar layout, the [[Felixstowe Fury]].<ref name="Bowers Curtiss p136-7"/><ref name="Bruce pt3 p929-0">Bruce 1955, pp. 929–930.</ref><ref name="Thet navy p386">Thetford 1978, p. 386.</ref> 
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Operators==
;{{UK}}
* [[Royal Naval Air Service]]
**[[RNAS Felixstowe]]

==Specifications (Renault engines)==
[[File:Curtiss T Wanamaker Triplane3.jpg|thumb|right|Front elevation of the Model T with four [[Rolls-Royce Limited|Rolls-Royce]] engines, RNAS Felixstowe, 1916]]
{{Aircraft specs
|ref=Curtiss Aircraft 1907–1947<ref name="Bowers Curtiss p136-7">Bowers 1979, pp. 136–137.</ref>
<ref name="Aerofiles">[http://aerofiles.com/_curtx.html Curtiss: K through Z"]. ''Aerofiles''. Retrieved 15 August 2010.</ref>  
<ref>Flight{{ cite web |url=https://www.flightglobal.com/pdfarchive/view/1916/1916%20-%200043.html |title=A Fighting Flying Boat | publisher=Flight | accessdate=21 December 2016}}</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=6 
|capacity=
|length m=
|length ft=58
|length in=10
|length note=
|upper span m=
|upper span ft=134
|upper span in=
|upper span note=
|mid span m=
|mid span ft=100
|mid span in=
|mid span note=
|lower span m=
|lower span ft=78
|lower span in=3
|lower span note=
|height m=
|height ft=31
|height in=4
|height note=
|wing area sqm=
|wing area sqft=2815
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=15645
|empty weight note=
|gross weight kg=
|gross weight lb=22000
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=4
|eng1 name=[[Renault 12F]]
|eng1 type=V-12 water-cooled piston engines
|eng1 kw=<!-- prop engines -->
|eng1 hp=240<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=100
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|range km=
|range miles= 675
|range nmi=
|range note= at cruise speed of 75 mph
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=7 hr<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=10 minutes to 4,000 ft (1,220 m)
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

==See also==
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
* [[American Trans-Oceanic Company]]
* [[Curtiss NC]]
|related=<!-- related developments -->
* [[Felixstowe Fury]]
* [[Gosport Aircraft Company|Gosport]] G9
|similar aircraft=<!-- similar or comparable aircraft -->
* [[Sikorsky Ilya Muromets]]<ref name="Bowers Curtiss p136-7"/>
|lists=<!-- related lists -->
}}

==References==
{{commons category|Curtiss Wanamaker Triplane}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* [http://www.flightglobal.com/pdfarchive/view/1916/1916%20-%200043.html?tracked=1 "A Fighting Flying Boat"]. ''Flight'', 13 January 1916, pp.&nbsp;43–44.
* [[Peter M. Bowers|Bowers, Peter M.]] ''Curtiss Aircraft 1907–1947''. London:Putnam, 1979. ISBN 0-370-10029-8.
* Bruce, J.M. [http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201806.html?tracked=1 "Historic Military Aircraft No. 11 Part 3:The Felixstowe Flying Boats"]. ''[[Flight International|Flight]]'', 23 December 1955, pp.&nbsp;929–932.
* Thetford, Owen. ''British Naval Aircraft since 1912''. London:Putnam, Fourth edition, 1978. ISBN 0-370-30021-1.
<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->
<!-- Navboxes go here -->
{{Curtiss aircraft}}

[[Category:Flying boats]]
[[Category:Curtiss aircraft|Wanamaker Triplane]]
[[Category:United States patrol aircraft 1910–1919]]
[[Category:Triplanes]]
[[Category:Four-engined tractor aircraft]]
[[Category:Individual aircraft]]