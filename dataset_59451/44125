{{about|the "Beehive" building at [[City Place Gatwick]], [[London Gatwick Airport]]||Beehive}}
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{{Infobox building
| name       = The Beehive
| image               = The Beehive (Original Terminal Building at Gatwick Airport).JPG
| caption             = The Beehive from the east
| map_type            = United Kingdom Crawley
| altitude            = 
| building_type       = Former [[airport terminal]] 
| architectural_style = [[Art Deco]]
| cost                = 
| location            = [[City Place Gatwick]], [[Gatwick Airport]], [[Crawley]], [[England]]
| address             = Beehive Ring Road, Gatwick Airport, West Sussex RH6 0PA
| owner               = [[Bland Group]]
| current_tenants     = Various
| coordinates         = {{coord|51|08|39|N|0|09|48|W|region:GB|display=inline}}
| start_date          = July 1935
| completion_date     = 1936
| inauguration_date   = 6 June 1936
| height              = 
| diameter            = 
| other_dimensions    = 
| floor_count         = 3
| floor_area          = 
| main_contractor     = 
| architect           = Alan Marlow
| architecture_firm   = [[Frank Hoar|Hoar]], Marlow and Lovett
| structural_engineer = 
| services_engineer   = 
| civil_engineer      = 
| other_designers     = 
| quantity_surveyor   = 
| awards              = 
| references          = 
}}

'''The Beehive''' is the original [[Airport terminal|terminal building]] at [[London Gatwick Airport|Gatwick Airport]], England.  Opened in 1936, it became obsolete in the 1950s as the airport expanded.<ref name="Indy1">{{cite news|url=http://www.independent.co.uk/travel/news-and-advice/terminals-the-last-word-799232.html |title=Terminals: the last word |last=Calder |first=Simon |authorlink=Simon Calder |date=22 March 2008 |work=[[The Independent]] |publisher=[[Independent News & Media]] |accessdate=19 January 2015 |archiveurl=http://www.webcitation.org/6VhlVMX2j?url=http%3A%2F%2Fwww.independent.co.uk%2Ftravel%2Fnews-and-advice%2Fterminals-the-last-word-799232.html |archivedate=19 January 2015 |deadurl=no |df= }}</ref>  In 2008, it was converted into [[serviced offices]], operated by [[OREGA]], having served as the headquarters of franchised airline [[GB Airways]] for some years before that.  It was the world's first fully integrated airport building, and is considered a nationally and internationally important example of airport terminal design.<ref name="NHLE-1268327">{{NHLE|num=1268327|desc=The Beehive (Former Combined Terminal and Control Tower), Beehive Ring Road, Crawley|grade=II*|accessdate=19 January 2015}}</ref> The Beehive is a part of the [[City Place Gatwick]] office complex.<ref name="OREGA">{{cite web|url=http://www.orega.com/serviced-offices/gatwick/ |title=Gatwick: Serviced Office Space |publisher=OREGA |year=2015 |accessdate=19 January 2015 |archiveurl=http://www.webcitation.org/6VhmWJeIu?url=http%3A%2F%2Fwww.orega.com%2Fserviced-offices%2Fgatwick%2F |archivedate=19 January 2015 |deadurl=no |df= }}</ref> The  {{convert|20000|sqft|sqm|adj=on}} former terminal building is on a {{convert|2|acre|ha|adj=on}} site.<ref>{{cite web|url=http://www.cityplacegatwick.com/images/cpg_masterplan.jpg |title=City Place Gatwick Masterplan |publisher=Arora Management Services |year=2011 |accessdate=19 January 2015 |archiveurl=http://www.webcitation.org/6VhmlqfxP?url=http%3A%2F%2Fwww.cityplacegatwick.com%2Fimages%2Fcpg_masterplan.jpg |archivedate=19 January 2015 |deadurl=no |df= }}</ref>

==History==
In September 1933 A. M. (Morris) Jackaman, who owned several light aircraft, bought [[Gatwick Airport#1920–1945|Gatwick Aerodrome]] for £13,500. He planned a purpose-built terminal building; the previous aerodrome building was a converted farmhouse.  He put great importance on the design process: he, and contemporaries, considered terminals at other aerodromes to be impractical and unsuitable for expansion.<ref name="Blow">{{Harvnb|Blow|2005|pp=3–5.}}</ref>

Jackaman developed the idea of a circular terminal building—reputedly in response to a throwaway comment by his father—and submitted a [[patent application]] for the concept on 8 October 1934.  Advantages claimed for the design included efficient use of space and greater safety of aircraft movements.  [[Telescoping (mechanics)|Telescopic]] "piers" or gangways would provide covered access from the building to the aircraft.  A [[Subway (underpass)|subway]] was recommended as the best method of bringing passengers into the building from outside.<ref name="Blow"/>

Jackaman raised money by [[Float (finance)|floating]] his company, Airports Ltd, on the [[stock exchange]].  The Air Ministry also paid for the right to use Gatwick as a diversionary destination at times when Croydon Aerodrome was inaccessible; and in 1935, Hillman's Airways—months before its merger to form the company now known as [[British Airways]]—made Gatwick its operational base, increasing its commercial viability and providing more finance.  The aerodrome closed on 6 July 1935 to allow the terminal to be built.  The contracted opening date of October 1935 was not met, partly because of ongoing drainage problems, but a new railway station was provided on time in September of that year.  This was linked to the terminal when it did open.<ref name="GG">{{Harvnb|King|Tait|1980|loc=Chapter 2.}}</ref>

The terminal was completed in early 1936.  Although the airport was officially reopened on 6 June 1936, flights to various destinations began in May.  Jackaman's proposed service to Paris was included: three flights were operated each day, connecting with fast trains from [[London Victoria station]].  Combined rail and air tickets were offered for [[Pound sterling|£]]4.5[[Shilling|s]], and there was a very short transfer time at the terminal (on some flights, as little as 20 minutes was needed).<ref name="GG"/><ref name="Blow"/>

Gatwick Airport, as it had become, was [[requisition]]ed for military use during the [[Second World War]].  Afterwards, it was eventually agreed upon as the site of London's official second airport, behind [[London Heathrow Airport|Heathrow]], after other contenders were ruled out.<ref name="Gwynne151">{{Harvnb|Gwynne|1990|p=151.}}</ref>  The Government announced its decision in July 1952.<ref name="Gwynne160">{{Harvnb|Gwynne|1990|p=160.}}</ref>  Substantial redevelopment started in that year with the acquisition of more land (including the parts of the racecourse site which had not yet been developed) and the diversion of the [[A23 road|A23]] around the new boundary of the airport.  A large new terminal was built between 1956 and 1958, the 1935 railway station closed and a new station was built within the terminal complex, on the site of the old racecourse station.  The Beehive was thus cut off from transport connections and the airport at large, although it was still within its boundaries and was used for [[helicopter]] traffic for several more years.<ref name="Gwynne165">{{Harvnb|Gwynne|1990|p=165.}}</ref>

The Borough of Crawley was extended northwards in 1974 to include Gatwick Airport and its surrounding land, at which point it moved from Surrey into West Sussex.<ref name="Gwynne1">{{Harvnb|Gwynne|1990|p=1.}}</ref>  The Beehive has been in Crawley since then.  The county of Surrey had not always been accommodating towards the airport: for example, in 1935, the local council in which the terminal would be built (Dorking and Horley [[Rural District|Rural District Council]]) was concerned about possible compensation claims from local residents and the threat of facing liability for accidents; and it "could see no benefit" to allowing further development.<ref name="GG"/><ref name="Gwynne152">{{Harvnb|Gwynne|1990|p=152.}}</ref>

==Architecture==
[[File:Beehive Entrance - geograph.org.uk - 179507.jpg|thumb|upright|The building's entrance]]
The terminal was designed by architects [[Frank Hoar|Hoar]], Marlow and Lovett (job architect Alan Marlow) in accordance with the design concept provided by Morris Jackaman.  It was built from steel [[reinforced concrete]] frames with internal brickwork walls,<ref name="NHLE-1268327"/> and has been described as a good example of the 1930s trend whereby concrete was used instead of steelwork as the main material for buildings intended to project a "modern" impression.<ref name="ConcBP44">{{Harvnb|Macdonald|2002|p=44.}}</ref> A [[Truss#Vierendeel truss|Vierendeel girder]] with six supports runs around the first floor roof.<ref name="ConcBP46">{{Harvnb|Macdonald|2002|p=46.}}</ref>

As originally built, the interior consisted of [[concentric]] rings of rooms and offices with corridors between them,<ref name="NHLE-1268327"/> designed to keep arriving and departing passengers separate.<ref name="RIBA">{{cite web|url=http://www.architecture.com/WhatsOn/Exhibitions/AtTheVictoriaAndAlbertMuseum/ArchitectureGallery/TheFunctionOfBuildings/GatwickAirportTerminal.aspx|title=The Function of Buildings: Gatwick Airport terminal|year=2008|work=Royal Institute of British Architects website|publisher=[[Royal Institute of British Architects|RIBA]]|accessdate=2008-11-17| archiveurl= https://web.archive.org/web/20081210201713/http://www.architecture.com/WhatsOn/Exhibitions/AtTheVictoriaAndAlbertMuseum/ArchitectureGallery/TheFunctionOfBuildings/GatwickAirportTerminal.aspx| archivedate= 10 December 2008 <!--DASHBot-->| deadurl= no}}</ref>  Six telescopic covered corridors led from the main concourse, allowing six aircraft to be in use at one time.  A subway led from the terminal to the new station, {{convert|130|yd|m|0}} away, ensuring that passengers arriving by train from London stayed undercover from the time they arrived at [[London Victoria station|Victoria station]] until the time their aircraft reached its destination.<ref name="GG"/><ref name="Blow"/>

The building rises from one storey in the exterior ring to three in the centre.  This central section originally contained a [[control tower]], weather station and some passenger facilities; the main passenger circulating area surrounded it on the storey below.  Baggage handling also took place on this floor.  A restaurant and offices were on the ground floor in the outermost ring.  The ground and first floors have windows of various sizes at regular intervals, while the former control tower was glazed all around.<ref name="NHLE-1268327"/>  Changes have been made to the internal layout since the conversion of the building to offices.

The design is frequently described as innovative and revolutionary,<ref name="Indy1"/> and The Beehive is recognised as having been the UK's first integrated airport building, combining all necessary functions in a single structure.<ref name="NHLE-1268327"/><ref name="Indy2">{{cite news|url=http://www.independent.co.uk/travel/news-and-advice/forget-the-poll--these-are-the-real-seven-wonders-456328.html|title=Forget the poll – these are the real seven wonders|last=Calder|first=Simon|authorlink=Simon Calder|date=22 March 2008|work=[[The Independent]]|publisher=[[Independent News & Media]]|accessdate=19 January 2015|archiveurl=https://web.archive.org/web/20081224215528/http://www.independent.co.uk/travel/news-and-advice/forget-the-poll--these-are-the-real-seven-wonders-456328.html|archivedate=24 December 2008|deadurl=yes}}</ref>  It was the first airport to give direct, undercover access to the aircraft,<ref name="RIBA"/> and the first to be integrated with a railway station.

==Current use==
[[GB Airways]], established in 1931 as Gibraltar Airways<ref name="EJ1">{{cite web|url=http://www.easyjet.com/common/img/easyjet_gb_airways_acquisition.pdf|title=easyJet.com Acquisition of GB Airways|format=PDF|date=25 October 2007|work=easyJet.com Presentation to Shareholders|publisher=[[EasyJet|easyJet.com]]|page=9|accessdate=19 January 2015|archiveurl=https://web.archive.org/web/20081114040650/http://www.easyjet.com/common/img/easyjet_gb_airways_acquisition.pdf|archivedate=14 November 2008|deadurl=yes}}</ref> by [[Gibraltar]] shipping group [[Bland Group|MH Bland]], moved its headquarters and operational base to The Beehive in 1989.<ref name="EJ1"/><ref name="Nyras">{{cite web|url=http://www.nyras.co.uk/en/news-and-comment/gb-airways-sale-news.html|title=£103.5m sale of GB Airways to easyJet completed|date=31 January 2008|publisher=Nyras Capital LLP|accessdate=19 January 2015|archiveurl=https://web.archive.org/web/20090619075827/http://www.nyras.co.uk/en/news-and-comment/gb-airways-sale-news.html|archivedate=19 June 2009|deadurl=yes}}</ref>

[[EasyJet]] agreed to purchase GB Airways in 2007, but The Beehive was not included. The employment base at The Beehive closed, with 284 job losses.<ref>"[http://goliath.ecnext.com/coms2/gi_0199-7160788/GB-s-slim-profits-hastened.html GB's slim profits hastened sale.]" ''[[Travel Trade Gazette UK & Ireland]]''. 2 November 2007. 4 News. Retrieved on 2 March 2011.</ref> After the purchase was completed in January 2008, the building was retained by GB's former parent company, [[Bland Group]],<ref name="Nyras"/> and was partially converted to become a "service centre" for the group's operations in the United Kingdom.<ref name="Yacout">{{cite web|url=http://www.yacout.info/Joe-Gaggero-President-of-the-Bland-Group-Vice-President-of-the-Moroccan-British-Business-Council-to-Yacout-Info_a158.html|title=Joe Gaggero, President of the Bland Group, Vice President of the Moroccan British Business Council, to Yacout.Info|date=29 October 2008|work=Yacout.info Moroccan e-magazine|publisher=Yacout.info|accessdate=19 January 2015|archiveurl=https://web.archive.org/web/20111005172830/http://www.yacout.info/Joe-Gaggero-President-of-the-Bland-Group-Vice-President-of-the-Moroccan-British-Business-Council-to-Yacout-Info_a158.html|archivedate=5 October 2011|deadurl=yes}}</ref> The Bland Group markets the office space to tenants requiring [[Serviced office|serviced office accommodation]].<ref name="OfficialSpace">{{cite web|url=http://www.officialspace.co.uk/offices/Gatwick/14968_The-Beehive-percent-2CBeehive-Ring-Road-percent-2C-.html|title=Gatwick – Office Space to rent|year=2005–2009|publisher=Official Space|accessdate=19 January 2015|archiveurl=https://web.archive.org/web/20090226190448/http://www.officialspace.co.uk/offices/Gatwick/14968_The-Beehive-percent-2CBeehive-Ring-Road-percent-2C-.html|archivedate=26 February 2009|deadurl=yes}}</ref>  In December 2014, local firm of solicitors Mayo Wynne Baxter set up an office in the building.<ref name="CObs-6520271">{{cite news|url=http://www.crawleyobserver.co.uk/news/local/solicitors-set-up-home-in-beehive-1-6520271 |title=Solicitors set up home in Beehive |date=18 January 2015 |work=Crawley Observer |publisher=[[Johnston Press|Johnston Press Digital Publishing]] |accessdate=19 January 2015 |archiveurl=http://www.webcitation.org/6VhnO1JEn?url=http%3A%2F%2Fwww.crawleyobserver.co.uk%2Fnews%2Flocal%2Fsolicitors-set-up-home-in-beehive-1-6520271 |archivedate=19 January 2015 |deadurl=no |df= }}</ref>

The Beehive was [[Listed building|listed]] at Grade II* on 19 August 1996.<ref name="NHLE-1268327"/>  It is one of the 12 Grade II* buildings, and [[Listed buildings in Crawley|100 listed buildings of all grades, in the Borough of Crawley.]]<ref name="CBC">{{cite web|url=http://www.crawley.gov.uk/pub_livx/groups/webcontent/documents/adviceguidance/int116583.pdf |format=PDF |title=Listed Buildings in Crawley |date=6 July 2011 |publisher=Crawley Borough Council |accessdate=4 February 2013 |archiveurl=http://www.webcitation.org/6EBbjoKcj?url=http%3A%2F%2Fwww.crawley.gov.uk%2Fpub_livx%2Fgroups%2Fwebcontent%2Fdocuments%2Fadviceguidance%2Fint116583.pdf |archivedate=4 February 2013 |deadurl=no |df= }}</ref>

==See also==
*[[Listed buildings in Crawley]]

==References==
===Notes===
{{reflist|2}}

===Bibliography===
{{refbegin}}
*{{cite book|last=Blow|first=Christopher J.|title=Transport Terminals and Modal Interchanges: Planning and Design|publisher=Elsevier|year=2005|isbn=0-7506-5693-X|url=https://books.google.com/books?id=ZcAoO6-99tAC&pg=PA3&lpg=PA3&dq=%22Morris+Jackaman%22&source=web&ots=Vd9BHHbbQX&sig=6ag0VY-6r6ioCke1Nn1BWXXbZ_0&hl=en&sa=X&oi=book_result&resnum=4&ct=result#PPA3,M1|ref=harv}}
*{{cite book|last=Gwynne|first=Peter|title=A History of Crawley|publisher=Phillimore & Co|location=Chichester|year=1990|edition=1st|isbn=0-85033-718-6|ref=harv}}
*{{cite book|last1=King|first1=John|last2=Tait|first2=Geoff|title=Golden Gatwick: 50 Years of Aviation|publisher=British Airports Authority and the Royal Aeronautical Society|year=1980|ref=harv}}
*{{cite book|last=Macdonald|first=Susan|title=Concrete: Building Pathology|publisher=Blackwell Publishing|location=Oxford|year=2002|isbn=0-632-05251-1|url=https://books.google.com/books?id=nNlEyaQGlKkC&pg=PA44&lpg=PA44&dq=Hoar,+Marlow+and+Lovett&source=web&ots=xjuDIjq-xg&sig=czaEagJKnedxIsd_9yuFlHRF4Mc&hl=en&sa=X&oi=book_result&resnum=2&ct=result#PPA47,M1|ref=harv}}
{{refend}}

==External links==
{{Commons category|Beehive (Gatwick Airport)}}
{{Portal|West Sussex|Aviation|Architecture}}
* [http://www.gettyimages.co.uk/detail/news-photo/the-construction-site-of-the-main-terminal-building-at-news-photo/3376830 Photo of The Beehive under construction showing the extent of the use of brick]
* [http://www.gettyimages.co.uk/detail/news-photo/gatwick-airport-news-photo/3361907 Photo of The Beehive with the original aircraft hard surfacing surrounding it]

{{Crawley}}

[[Category:Buildings and structures in Crawley]]
[[Category:Transport in West Sussex]]
[[Category:Transport infrastructure completed in 1936]]
[[Category:Art Deco architecture in England]]
[[Category:Grade II* listed buildings in West Sussex]]
[[Category:Airport terminals]]
[[Category:Gatwick Airport]]
[[Category:Airline headquarters]]