{{Redirect|FEMS|the fire department of the District of Columbia|District of Columbia Fire and Emergency Medical Services Department}}

'''Federation of European Microbiological Societies''' ('''FEMS''') is an international European scientific organization, formed by the union of a number of national organizations; there are now 51 members from 36 European countries, regular and provisional.<ref>"The Federation of European Microbiological Societies: An Historical Review" by A. Dawes, ''FEMS Microbiology Letters'' 100 (1992) 15-24,</ref> Members can apply for fellowships, grants and/or support when organising a meeting. FEMS facilitates exchange of scientific knowledge to all microbiologists in Europe and worldwide by publishing five microbiology journals and organising a biennial congress for microbiologists around the world. It also initiates campaigns such as the European Academy of Microbiology (EAM) and the European Microbiology Forum (EMF).<ref>http://www.fems-microbiology.org/about-fems.html</ref>

Since 1977, it is the sponsor of ''FEMS Microbiology Letters'', a single journal, that was later on followed by 4 more journals:

* ''[[FEMS Microbiology Letters]]''
* ''[[FEMS Microbiology Reviews]]''
* ''[[Pathogens and Disease]]'' a journal preceded by ''[[FEMS Immunology and Medical Microbiology]]''
* ''[[FEMS Microbiology Ecology]]''
* ''[[FEMS Yeast Research]]''

Originally published for the Society by [[Elsevier]], then by [[Wiley-Blackwell]], they are now published by [[Oxford University Press]].

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20081206010717/http://www.fems-microbiology.org:80/website/nl/page16.asp Federation website]
*[http://europeanacademyofmicrobiology.org/ European Academy of Microbiology website]
*[http://www.oxfordjournals.org/page/6395/1 Pathogens and Disease journal website ]
*[http://www.oxfordjournals.org/page/6395/2 FEMS Microbiology Ecology journal website]
*[http://www.oxfordjournals.org/page/6395/3 FEMS Microbiology Letters journal website]
*[http://www.oxfordjournals.org/page/6395/4 FEMS Microbiology Reviews journal website]
*[http://www.oxfordjournals.org/page/6395/5 FEMS Yeast Research journal website]

{{Authority control}}
[[Category:Microbiology]]
[[Category:Oxford University Press academic journals]]
[[Category:Scientific supraorganizations]]
[[Category:Federations|European Microbiological Societies]]