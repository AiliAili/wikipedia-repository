{{Use dmy dates|date=March 2017}}
{{Use British English|date=March 2017}}
{{Infobox military person
|honorific_prefix  =
|name              =Peter Vigne Ayerst
|honorific_suffix  =
|native_name       =
|native_name_lang  =
|image         =Royal Air Force- France, 1939-1940. C173.jpg
|image_size    =
|alt           =
|caption       =Ayerst (bottom left) in France
|birth_date    ={{birth date|df=yes|1920|11|04}}
|death_date    ={{Death date and age|df=yes|2014|05|15|1920|11|04}}
|birth_place   =[[Westcliff-on-Sea]], [[Essex, United Kingdom]]
|death_place   =
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =
|birth_name    =
|allegiance    ={{flag|United Kingdom|23px}}
|branch        ={{air force|United Kingdom|23px}}
|serviceyears  =
|rank          =
|servicenumber = <!--Do not use data from primary sources such as service records.-->
|unit          =[[No. 73 Squadron RAF]]
|commands      =
|battles       =[[World War II]]
|battles_label =
|awards        =
|memorials     =
|spouse        = <!-- Add spouse if reliably sourced --> 
|relations     =
|laterwork     =
|signature     =
|website       = <!-- {{URL|example.com}} -->
|module        =
}}
[[Wing commander (rank)|Wing Commander]] '''Peter Vigne Ayerst''', [[Distinguished Flying Cross (United Kingdom)|DFC]] (4 November 1920 – 15 May 2014)<ref name="pr2009">{{cite web|url=http://www.sys-con.com/node/1171686|title=UK Commemorates Battle of Britain Commander|date=4 November 2009|publisher=sys-con.com|accessdate=8 September 2012}}</ref> was a British [[Royal Air force]] officer and [[World War II]]  [[flying ace]].<ref name="bbcnews2010">{{cite news|url=http://news.bbc.co.uk/1/hi/england/london/8653349.stm|title=Pilot is reunited with his WWII Spitfire in London|date=30 April 2010|publisher=news.bbc.co.uk|accessdate=8 September 2012}}</ref> He was the last surviving [[RAF Advanced Air Striking Force|73 Squadron]] pilot and test pilot from [[Castle Bromwich Aerodrome]].<ref name="pr2009" />

== Early life ==
Ayerst was born on 4 November 1920 in [[Westcliff-on-Sea]], Essex, England. He was educated at [[Westcliff High School for Boys]], a state [[grammar school]] in his home town.<ref name="obit - Times">{{cite news|title=Wing Commander Peter Ayerst|url=http://www.thetimes.co.uk/tto/opinion/obituaries/article4134716.ece|accessdate=30 June 2014|work=The Times|date=30 June 2014}}</ref>

== Military career ==
Ayerst was [[commissioned officer|commissioned]] into the [[Royal Air Force]] on 14 December 1938 as an [[acting pilot officer]] on probation.<ref name="LG 27 December 1938">{{London Gazette |issue=34583 |date=27 December 1938 |startpage=8249 |endpage= |supp= |accessdate=1 July 2014}}</ref> In August 1939, he was posted to [[No. 73 Squadron RAF]] to fly [[Hawker Hurricane|Hurricanes]].<ref name="locallondon2008">{{cite news|url=http://www.thisislocallondon.co.uk/news/2277547.two_generations_join_in_the_air/|title=Two generations join in the air|date=16 May 2008|publisher=''[[Evening Standard]]''|accessdate=8 September 2012}}</ref> He was regraded to [[pilot officer]] on probation on 3 September 1939 and his commission was confirmed on 6 October 1939.<ref name="LG 10 October 1939">{{London Gazette |issue=34705 |date=10 October 1939 |startpage=6796 |endpage= |supp= |accessdate=1 July 2014}}</ref>

He was sent to France with the squadron and scored his first victory in April 1940. After a spell instructing, when he shared in the destruction of a [[Heinkel He 111|He111]] with two other instructors, he had postings with both 145 and 243 Squadrons.

In July 1942 he went to North Africa with 33 Squadron,<ref name="locallondon2008" /><ref name="googlebooks">{{cite book|url=https://books.google.com/books?id=KYfYGwAACAAJ&dq=Peter+Ayerst&source=bl&ots=AO3RB2A0dj&sig=uOz0B5asmiwXDRRYihdxNCsewRg&hl=en&sa=X&ei=5KVLUPH7FKWO2QW-5ICAAw&ved=0CDUQ6AEwAQ|title=Spirit of the Blue: Peter Ayerst: A Fighter Pilot's Story|date=1 October 2005|publisher=books.google.com|accessdate=8 September 2012}}</ref> before being promoted to flight commander with 238 Squadron, both postings with further combat success. After a period in South Africa, he returned to the UK, joining 124 Squadron flying [[Supermarine Spitfire variants part two#Mk VII .28type 351.29|Spitfire MkVIIs]] in defence of the invasion ports, where he scored his final victory; then flew Spitfire MkIXs on bomber escorts to Germany. He later became a Spitfire test pilot at Castle Bromwich with the instruction of [[Alex Henshaw]].<ref name="pr2009" /> After the war, he became one of the most highly regarded wartime instructors in the RAF.<ref>{{cite news|last=Berg|first=Sanchia|url=http://news.bbc.co.uk/today/hi/today/newsid_8933000/8933372.stm|title=Battle of Britain survival 'a question of luck'|date=21 August 2010|publisher=news.bbc.co.uk|accessdate=8 September 2012}}</ref> His final victory tally stood at 5 destroyed, 1 probable, 3 damaged and 2 further destroyed on the ground.{{citation needed|date=July 2014}} In September 1944, he was awarded the Distinguished Flying Cross.<ref name="locallondon2008" />

== Later life ==
Ayerst was involved in a biography about his military experience tilted ''Spirit of the Blue: A Fighter Pilot's Story''. It was published 2004.<ref name=googlebooks /> He died on 15 May 2014 at the age of 93.<ref>[http://announcements.telegraph.co.uk/deaths/177359/ayerst AYERST]</ref>

== References ==
{{Reflist}}

== External links ==
* [http://news.bbc.co.uk/today/hi/today/newsid_8933000/8933372.stm 2010 interview with BBC News]
*{{IMDb name|5760014|Peter Ayerst}}

{{DEFAULTSORT:Ayerst, Peter V.}}
[[Category:1920 births]]
[[Category:2014 deaths]]
[[Category:British World War II pilots]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:People educated at Westcliff High School for Boys]]