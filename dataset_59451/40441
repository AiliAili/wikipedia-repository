{{Use dmy dates|date=May 2014}}

{{Spanish name|Lafuente|Garcia-Rojo}}

[[File:Abelardo Lafuente.jpg|thumb|Abelardo Lafuente the Spanish architect of Shanghai's Golden Era]]

'''Abelardo Lafuente Garcia-Rojo''' (Madrid 1871 – Shanghai 1931) was a Spanish architect<ref name=podcast>{{cite news|last=La Hora de Asia|title=Abelardo Lafuente: arquitectura española en Shanghai|url=http://www.rtve.es/alacarta/audios/la-hora-de-asia/hora-asia-abelardo-lafuente-arquitectura-espanola-shanghai-21-05-11/1107013/|accessdate=5 July 2012|newspaper=Radio Nacional España|date=21 May 2011|language=spanish}}</ref> and entrepreneur who had his own studio in colonial Shanghai.<ref name="Cultural Magazine">{{cite news|last=Aldama|first=Zigor|title=Un español en el Shanghái precomunista|url=http://elpais.com/diario/2011/06/25/babelia/1308960770_850215.html|accessdate=5 July 2012|newspaper=Babelia 1.022|date=25 June 2011|agency=El Pais|pages=24|language=Spanish}}</ref> Between 1913 and 1931 he was the only Spaniard to have an architect studio registered in the city; Lafuente collaborated with several contemporary architects. Lafuente blended the mozarab and moorish styles from Spain into Chinese architecture. In 2009, the Spanish architect Alvaro Leonardo Perez established the ''Lafuente Research Project''. The findings of this project were first presented at the Shanghai World Expo in 2010 in collaboration with the [[Instituto Cervantes]]. The Lafuente Research Project is ongoing.

== History ==
Lafuente's professional career began in [[Manila]]. His studio was named ''A. Lafuente Architect & Contractor''. In 1913 Lafuente moved to Shanghai where he opened a new studio named ''A. Lafuente Garcia-Rojo, Architect & Contractor''. Later, Lafuente joined American architect G. O. Wootten to form the ''Lafuente & Wootten'' studio. The two architects worked together for several years, their most notable work being the ballroom at the [[Astor House Hotel (Shanghai)|Astor House Hotel]], built in 1917. After this period he maintained the studio on his own and called it ''A. Lafuente Gª Rojo Architect''. Afterwards he partnered with Russian architect A. J. Yaron, changing the studio's name once again to ''Lafuente & Yaron''.

For the better part of the following 10 years Lafuente worked for the biggest hotel company in Asia at the time: the [[Hong Kong and Shanghai Hotels|Hong Kong and Shanghai Hotels Ltd.]], rehabilitating and refurbishing their buildings. His most important work during this project was the Majestic Hotel<ref>{{cite web|title=Hotel Majestic, the most important company's hotel in Shanghai|url=http://www.hshgroup.com/en/~/media/Files/HSHGroup/Investor_Relations/Financial_Results/2010/Annual_Report/AR_2010_C08.ashx|publisher=HSH|accessdate=11 July 2012}}</ref> (1924) where he designed its new ballroom (the most luxurious in Asia at that time{{citation needed|date=January 2013}}). Some of his other projects included churches, mosques, private villas, apartment buildings, movie theaters, private clubs, and hospitals, among others.<ref>{{cite news|last=EFE|first=Agency|title=Un arquitecto español trabajando en la belle époque china|url=http://www.elmundo.es/elmundo/2011/12/20/cultura/1324375509.html|accessdate=11 July 2012|date=20 December 2011|language=Spanish}}</ref><ref>{{cite news|work=The north China Herald|title=A Jewish Club for Shanghai|pages=29–30|date=6 July 1918}}</ref>

His reputation in the Spanish colony is described in the book by Vicente Blasco Ibanez ''La vuelta al mundo de un novelista''. Spanish entrepreneur Antonio Ramos<ref name=Magazine>{{cite journal|last=Revista de Occidente|author2=Juan Ignacio Toro Escudero|title=Sombras orientales: Antonio Ramos y el primer cine chino|date=June 2010|issue=349/350|url=http://www.ortegaygasset.edu/fog/ver/408/revista-de-occidente/junio-2010}}</ref> was his most important influence during his time in China and his most outstanding client. Ramos brought motion pictures to China.{{citation needed|date=January 2013}} Lafuente was commissioned to build some of Ramos' most important cinemas.

Lafuente also worked in North America. In Los Angeles and in [[Tijuana]] he built houses. During 1927–1930, Lafuente maintained operations in Shanghai while he developed his business in California and Mexico. At the end of this period, after being significantly affected by the [[Crash of 1929]], he decided to return to Shanghai and undertake more projects there. He died in Shanghai at the Shanghai General Hospital in 1931, aged 60.

==References==
{{reflist}}

{{DEFAULTSORT:Garcia, Abelardo}}
[[Category:1871 births]]
[[Category:Spanish businesspeople]]
[[Category:Spanish architects]]
[[Category:1931 deaths]]