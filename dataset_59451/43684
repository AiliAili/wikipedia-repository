<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=SD-1 Minisport
 | image=Spacek SD-1 Minisport.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Amateur-built aircraft]]
 | national origin=[[Czech Republic]]
 | manufacturer=[[Spacek sro]]<br />[[SkyCraft Airplanes]]
 | designer= 	Igor Špaček
 | first flight=
 | introduced=
 | retired=
 | status=Plans and kits available (2016)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built= about 41 flying by November 2015<ref name=JAWA16/>
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]210 (plans only, 2016)<br>US$54,000 production model<ref>{{cite web|title=SkyCraft|url=http://www.skycraftairplanes.com/sd-1-minisport/|accessdate=16 August 2014}}</ref> 
 | developed from= 
 | variants with their own articles=
}}
|}
[[File:SD-1 Minisport.jpg|right|thumb|SkyCraft Airplanes SD-1 Minisport]]

The '''Spacek SD-1 Minisport''' is a [[Czech Republic|Czech]] [[amateur-built aircraft]], designed by Igor Špaček and produced by [[Spacek sro]] of [[Hodonin]]. The aircraft is also produced in the US by [[SkyCraft Airplanes]] of [[Orem, Utah]] as a [[light-sport aircraft]]. The aircraft is supplied in the form of plans, as a kit for amateur construction, or as a ready-to-fly aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 120. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref>{{cite web|author=|url=http://generalaviationnews.com/2013/05/27/skycraft-begins-production-on-sd-1-minisport/ |title=SkyCraft begins production on SD-1 Minisport — General Aviation News |publisher=Generalaviationnews.com |date=2013-05-27 |accessdate=2014-05-01}}</ref>

==Design and development==
The aircraft features a cantilever [[low-wing]], a single-seat enclosed cockpit, fixed [[conventional landing gear]] or optionally [[tricycle landing gear]], a [[T-tail]] and a single engine in [[tractor configuration]]. Due to its very light weight it can qualify for the German 120&nbsp;kg category.<ref name="WDLA11" /><ref name="Design">{{cite web|url = http://sdplanes.com/sd1a.htm|title = The SD-1 Minisport Design Philosophy|accessdate = 19 December 2012|last = Spacek sro|year = 2012}}</ref> It complies with the United Kingdom SSDR rules for single seat deregulated microlight aeroplanes.<ref name="United Kingdom Civil Aviation Authority SSDR">{{cite web |url = http://www.caa.co.uk/docs/33/InformationNotice2014101.pdf |title = United Kingdom Civil Aviation Authority INFORMATION NOTICE Number: IN–2014/091 |accessdate = 8 December 2014}}</ref>

The aircraft is made from wood, with judicious use of composites, including for the [[Spar (aviation)|wing spar]]. Its {{convert|6|m|ft|1|abbr=on}} span wing employs an A315 [[airfoil]], has an area of {{convert|6|m2|sqft|abbr=on}} and utilizes [[flaperon]]s. Engines of {{convert|24|to|50|hp|kW|0|abbr=on}} can be used. Standard engines tested are the {{convert|28|hp|kW|0|abbr=on}} [[Hirth F33]], the {{convert|50|hp|kW|0|abbr=on}} [[Hirth F23]] [[two-stroke]]s, the {{convert|24|hp|kW|0|abbr=on}} [[Briggs and Stratton Vanguard]], or the [[Verner JCV-360]] powerplants. The [[Rotax 447]], [[Hirth 2702]], [[Zanzottera MZ 201]], [[Simonini Victor 1]], [[2si 460]] and [[Half VW]] can also be used.<ref name="WDLA11" /><ref name="Specs">{{cite web|url = http://sdplanes.com/sd1specsa.htm|title = The SD-1 Minisport Technical Datas|accessdate = 18 October 2012|last = Spacek sro|year = 2011}}{{sic}}</ref>

By November 2015 113 had been sold worldwide and about 41 were flying.<ref name=JAWA16>{{cite book |title= Jane's All the World's Aircraft  : development & production : 2016-17|last=Gunston |first=Bill |coauthors= |edition= |year=2016|publisher=IHS Global|location=|isbn= 978-0-7106-3177-0 |pages=208}}</ref>

At the end of May 2014 SkyCraft Airplanes announced that light-sport flight testing on its version had been completed. Their model has a revised cockpit, including [[Dynon SkyView]] instrumentation, a hydraulic brake system and the {{convert|50|hp|kW|0|abbr=on}} Hirth F-23 two-stroke fuel-injected engine.<ref name="Durden30May14">{{cite news|url = http://www.avweb.com/avwebflash/news/SD-1-Minisport-Flight-Testing-Completed222094-1.html|title = SD-1 Minisport Flight Testing Completed|accessdate = 2 June 2014|last = Durden|first = Rick|date = 30 May 2014| work = AVweb}}</ref><ref name="Grady08JUl14">{{cite news|url = http://www.avweb.com/avwebflash/news/SkyCraft-Ready-To-Fly-Expanding-In-Utah222311-1.html|title = SkyCraft Ready To Fly, Expanding In Utah|accessdate = 11 July 2014|last = Grady|first = Mary|date = 8 July 2014| work = AVweb}}</ref> The company's intention was that 12 aircraft would be built for the first production run.<ref>{{cite journal|magazine=AOPA Pilot|date=September 2014|title=Pilot Briefing}}</ref> However, as of 5 April 2016 the SD-1 was still not on the [[Federal Aviation Administration]]'s list of accepted light-sport aircraft.<ref name="FAA-LSA">{{cite web|url = http://www.faa.gov/aircraft/gen_av/light_sport/media/SLSA_Directory.xls|title = FAA Make/Model Directory for SLSA|accessdate = 12 August 2016|author = [[Federal Aviation Administration]]|date = 5 April 2016}}</ref>
<!-- ==Operational history== -->

==Variants==
;SD-1 TD
:Conventional landing gear (taildragger) version<ref name="Specs" />
;SD-1 TG
:Tricycle gear version<ref name="Specs" />
;SD-1 TD XL
:Conventional landing gear (taildragger) version for taller pilots<ref name="Specs" />
;SD-1 TG XL
:Tricycle gear version for taller pilots<ref name="Specs" />
;SD-2 SportMaster
: 2-seat tricycle gear version <ref>{{cite web|url=http://www.sdplanes.com/new/description/|title=SD-2 DESCRIPTION|work=sdplanes.com|accessdate=29 January 2016}}</ref>
<!-- ==Aircraft on display== -->

==Specifications (SD-1) ==
{{Aircraft specs
|ref=Bayerl and Spacek sro<ref name="WDLA11" /><ref name="Specs" /><ref>{{cite web|url=http://www.sdplanes.com/new/sd-1-versions/|title=SD-1 VERSIONS|work=sdplanes.com|accessdate=29 January 2016}}</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=4.35
|length ft=
|length in=
|length note=
|span m=6
|span ft=
|span in=
|span note=
|height m=1.23
|height ft=
|height in=
|height note=
|wing area sqm=6
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=A315
|empty weight kg=110
|empty weight lb=
|empty weight note=
|gross weight kg=240
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|34|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hirth F33]]
|eng1 type=single cylinder, air-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=28<!-- prop engines -->

|prop blade number=2
|prop name=wooden
|prop dia m=1.2
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=190
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=155
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=63
|stall speed mph=
|stall speed kts=
|stall speed note=flaperons deployed
|never exceed speed kmh=210
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+4/-2
|roll rate=
|glide ratio=13:1
|climb rate ms=3
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=40
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
;Similar aircraft
*[[Mini Coupe]]
*[[Sonex Aircraft Onex]]
*[[Corby Starlet]]
*[[Pazmany PL-4]]
*[[Mooney M-18 Mite]]

==References==
{{reflist|30em}}

==External links==
*{{Official website|http://sdplanes.com/}}
*[http://sdplanesusa.com/ Official USA website]

[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]