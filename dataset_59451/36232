{{good article}}
{{Infobox NRHP
  | name = Ambler's Texaco Gas Station
  | nrhp_type = 
  | image =Ambler's Texaco Gas Station4.JPG
  | caption = 
  | location = [[Dwight, Illinois|Dwight]], [[Livingston County, Illinois|Livingston County]], [[Illinois]]
  | nearest_city = 
  | lat_degrees = 
  | lat_minutes = 
  | lat_seconds = 
  | lat_direction = N
  | long_degrees = 
  | long_minutes = 
  | long_seconds = 
  | long_direction = W  
  | coord_display = inline,title
  | locmapin = Illinois#USA
  | built = 1933
  | architect = Jack Shore (builder)
  | added = November 29, 2001
  | area = less than one acre
  | mpsub = {{NRHP url|id=64500208|title=Route 66 through Illinois MPS}}
  | refnum = 01001311<ref name="nris">{{NRISref|version=2010a}}</ref>
  | governing_body = Village of Dwight
}}

'''Ambler's Texaco Gas Station''', also known as '''Becker's Marathon Gas Station''', is a historic [[filling station]] located at the intersection of [[U.S. Route 66|Old U.S. Route 66]] and [[Illinois Route 17]] in the village of [[Dwight, Illinois]], [[United States]]. The station has been identified as the longest operating gas station along Route 66; it dispensed fuel for 66 continuous years until 1999. The station is a good example of a domestic style gas station and derives its most common names from ownership stints by two different men. North of the station is an extant outbuilding that once operated as a commercial [[Ice house (building)#In the U.S.|icehouse]]. Ambler's was the subject of major restoration work from 2005&ndash;2007, and reopened as a Route 66 visitor's center in May 2007. It was added to the U.S. [[National Register of Historic Places]] in 2001.

==History==
Located at the intersection of [[U.S. Route 66]] and [[Illinois Route 17]] in [[Dwight, Illinois]], United States, Ambler's Texaco Station was built in 1933 by Jack Schore on property originally owned by Otto Strufe.<ref name=olsen/><ref name=nrhp/> The [[gas station]] is no longer in operation as a filling station, but when it closed, it held the record as the longest operating gas station along the historic Route 66. The station last dispensed [[gasoline]] in 1999.<ref name=olsen>Olsen, Russell A. ''Route 66 Lost & Found: Ruins and Relics Revisited'', ([https://books.google.com/books?id=UXpDqOFi5GsC&pg=PA10&lpg=PA10&dq=ambler's+texaco+station&source=web&ots=bwV2oxKBYP&sig=pAD4PG4uU2RgIECtSBDxeGFgqZU#PPA10,M1 Google Books]), Voyageur Press: 2006, (ISBN 0-7603-2623-1), p. 10. Retrieved 29 September 2007.</ref><ref name=suntimes>Hoekstra, Dave. "How slow can you go?", ([http://www.lexisnexis.com/us/lnacademic/results/docview/docview.do?risb=21_T2409447550&format=GNBFI&sort=null&startDocNo=1&resultsUrlKey=29_T2409450853&cisb=22_T2409450852&treeMax=true&treeWidth=0&csi=11064&docNo=1 LexisNexis]), ''Chicago Sun-Times'', 16 September 2007. Retrieved 3 November 2007.</ref> In 1936, the station was [[lease]]d to Vernon Von Qualen and became known as Vernon's Texaco Station. Over the next two years, Von Qualen purchased the station from Schore, and sold it in 1938 to Basil "Tubby" Ambler. Ambler owned the station from 1938&ndash;1966.<ref name=nrhp/> Ambler's ownership was the longest of any owner during the most historically significant period; as a result, the station was known as Ambler's longer than by any other name.<ref name=nrhp>Boyd, Larry D. "[http://gis.hpa.state.il.us/PDFs/218435.pdf Ambler's Texaco Gas Station]," ([[PDF]]), National Register of Historic Places Nomination Form, 18 May 2001, HAARGIS Database, ''Illinois Historic Preservation Agency''. Retrieved 29 September 2007.</ref> 
[[Image:Ambler's Texaco Gas Station14.JPG|thumb|left|Ambler's Texaco Gas Station was the longest operating [[gas station]] along [[U.S. Route 66]]. Gasoline stopped flowing in 1999.]]
The owner at the time of the building's nomination to the [[National Register of Historic Places]], Phil Becker, grew up living on Mazon Street (Illinois Route 17). He had been hanging out at the station since he was nine and began working at the station in 1964.<ref name=pontiac/> One year after Becker's employment began, Ambler sold the station to Earl Kochler.<ref name=nrhp/> Kochler sold the station to Royce McBeath, who ran it between 1965&ndash;1970. The station changed hands once again when Becker bought the station from McBeath on March 4, 1970.<ref name=nrhp/><ref name=pontiac/> About a year after Becker took over operation of the station, [[Texaco]] suddenly stopped supplying the location with [[gasoline]] and Becker contracted with [[Marathon Oil]].<ref name=olsen/> The brand change was accompanied by a name change to Becker's Marathon Gas Station, a name the station would be known by for over 26&nbsp;years.<ref name=olsen/> Becker operated the station, along with his wife Debbie, from the time of purchase until 1996.<ref name=pontiac>Shelton, Sheila. "[http://www.route66university.com/study/inthenews/286.php Texaco restoration completed]," ''Pontiac Daily Leader'' ([[Pontiac, Illinois]]), 25 April 2007, via Route 66 University. Retrieved 29 September 2007.</ref> After closing the station, the couple leased the building out as an auto repair shop and eventually donated the building to the village of Dwight.<ref name=pontiac/>

In 2005, a project to restore the old gas station began.<ref name=suntimes/> The village of Dwight applied for and received a [[United States dollar|US$]]10,400 federal cost-share grant from the U.S. [[National Park Service]] and its Route 66 Corridor Preservation Program.<ref name=fund/><ref name=pantagraph/><ref name=66corr>"[http://www.nps.gov/history/rt66/grnts/index.htm Cost-Share Grants],", Route 66 Corridor Preservation Program, ''U.S. National Park Service''. Retrieved 4 November 2007.</ref> The grant required the village to present matching funds in the amount of the grant. The restoration's ultimate goal was to reopen the station as a rest stop and interpretive center along the historic roadway.<ref name=fund>"[http://www.nps.gov/history/rt66/grnts/Funded%20Projects%2002.pdf Funded Projects &mdash; 2002]," ([[PDF]]), Route 66 Corridor Preservation Program, ''National Park Service''. Retrieved 29 September 2007.</ref>

==Restoration==
[[Image:Ambler's Texaco Gas Station6.JPG|thumb|right|The fully restored Ambler's Gas Station is meant to evoke a 1940s feel.]]
The combined grant and village-funded restoration included door, window, and roof repairs, along with interior and exterior repainting. The repainting matched the station's 1940s color scheme.<ref name=fund/> On April 24, 2007, the National Park Service presented a plaque commemorating the restoration project at the station.<ref name=morris>Anonymous. "[http://www.morrisdailyherald.com/articles/2007/04/23/neighbors/987dwight.txt Dwight restores Texaco station]," ''Morris Daily Herald'' ([[Morris, Illinois]]), 23 April 2007. Retrieved 29 September 2007.</ref> In early May 2007, with the restoration completed, Ambler's Texaco Gas Station reopened as a visitor's center.<ref name=suntimes/>

Following the restoration, the station evokes the 1940s; the interior is complete with a [[potbelly stove]], tiny [[Coca-Cola]] bottle and antique Dwight Lumber/Route 66 advertising [[yardstick]]s.<ref name=suntimes/> Also inside are an old cash register, old [[7 Up]] bottles and a plaid stamp saver book.<ref name=pantagraph/> The restored station is owned by the village of Dwight.<ref name=pantagraph>Holliday, Bob. "[http://www.pantagraph.com/articles/2007/05/07/news/doc463fa2972bbf5707857937.txt Dwight gas station welcome center on Old 66], ''The Pantagraph'' ([[Bloomington, Illinois]]), 7 May 2007. Retrieved 29 September 2007.</ref>

==Architecture==
The Ambler's Texaco Station is built in a common gas station style known as "house and canopy" style or "[[Domestic style (filling station)|domestic style]]."<ref name=nrhp/> The style was developed by [[Standard Oil]] of Ohio in 1916, and consisted of a small house-like building with an attached [[canopy (building)|canopy]]. The canopy extended out over the [[gas pump|pumps]] to protect customers from the weather.<ref name=mps/> The style was meant to evoke feelings of home and comfort in travelers and, in turn, to make people more at ease buying goods from the station.<ref name=mps>Seratt, Dorothy R.L. and Ryburn-Lamont, Terri. "{{NRHP url|id=64500208|title=Historic and Architectural Resources of Route 66 Through Illinois}}," ([[PDF]]), Multiple Property Documentation Form, August 1997, National Register Information System, National Register of Historic Places, ''National Park Service''. Retrieved 29 September 2007.</ref>

The one-story building has no basement and originally consisted of only the house and canopy portion, and three Texaco [[gas pump]]s. The house, or main office portion of the station includes the interior office and the men's and women's [[restroom]]s. The men's restroom is accessed from the interior office and the women's restroom is accessed via an outside door on the south side of the building. The original "LADIES" sign still protrudes from the wall above the entrance. The entire house section of the station is 23 feet (7.0 m) by 14&nbsp;feet (4.3&nbsp;m).<ref name=nrhp/> The exterior of the building is wood and sided with [[Clapboard (architecture)|clapboard]], save the garage bay area which is [[concrete block]]. The bay was built during World War II, between 1941&ndash;45, and material shortages forced the use of the differing building material.<ref name=nrhp/>

The building has a wooden, side [[gable]]d roof covered with [[asphalt shingle]]s, which extends over the station's canopy. The canopy supporting piers originally "flared out" and have been altered since the building's original construction. The three original Texaco pumps were replaced with 1960s era Marathon pumps, which are on display in the service bay. The Marathon change-over also resulted in a sign being attached to the roof of the service bay which read "Marathon," this sign is no longer extant.<ref name=nrhp/>

==Icehouse==
<!-- Icehouse (building) links to this section, please fix the link if you intend to change the section name. -->
[[Image:Ambler's Texaco Gas Station2.JPG|thumb|right|The icehouse, to the north of the station, is a [[contributing property]] to the [[National Register]] listing.]]
Located on the property of Ambler's Texaco Gas Station, just north of the station, is a 24-foot (7.3 m) by 16 foot (4.9&nbsp;m) wood clad, wooden frame building that once housed a commercial [[icehouse (building)|icehouse]]. Though its exact dates of operation are unknown, it is believed the icehouse was established by a member of Jack Schore's family during the 1930s. While the icehouse was in business, there was a small [[pond]] located about 200&nbsp;feet (61&nbsp;m) east of the property. Operators cut ice from the pond and stored it in the building until sale. After the icehouse ceased operation, a date that is also unknown, the building was used as a [[shed|storage shed]] for the [[gas station]]. The building features a double wall with [[sawdust]] stuffed in between the walls to provide [[Building insulation|insulation]]. The wooden roof has [[asphalt shingle]]s. Ambler's Texaco Station listing on the National Register lists the icehouse building as a [[contributing property]].<ref name=nrhp/>

==Significance==
The building is a good example of the house and canopy style, and for its architectural and commercial significance it was nominated to the National Register of Historic Places in October 2001 and listed on November 29, 2001.<ref name=nris/> Commercially, the station is most significant for the services and products it provided travelers along U.S. Route 66, including fuel, oil and automobile repairs. The gas station met all criteria listed in the National Register of Historic Places [[Multiple Property Submission|Multiple Property form]] as requirements for adding gas stations to the listing.<ref name=nrhp/> In order for gas stations along [[U.S. Route 66]] in Illinois to qualify for listing on the National Register, they must show a clear connection to and raise feelings associated with early traffic and tourism along Route 66.<ref name=mps/> A gas station listed for its architecture, such as Ambler's, must be a "good example" of an architectural style or type through many details from design to location, all of which must convey a connection with its historic appearance along Route 66.<ref name=mps/>

==See also==
{{Commons category|Ambler's Texaco Gas Station}}
*[[Standard Oil Gasoline Station (Odell, Illinois)|Standard Oil Gasoline Station]]
*[[U.S. Route 66 in Illinois]]

==Notes==
{{Reflist|35em}}

{{National Register of Historic Places}}

[[Category:Commercial buildings completed in 1934]]
[[Category:National Register of Historic Places in Livingston County, Illinois]]
[[Category:Dwight, Illinois]]
[[Category:Buildings and structures on U.S. Route 66]]
[[Category:U.S. Route 66 in Illinois]]
[[Category:Texaco]]
[[Category:Retail buildings in Illinois]]
[[Category:Gas stations on the National Register of Historic Places in Illinois]]
[[Category:Ice trade]]