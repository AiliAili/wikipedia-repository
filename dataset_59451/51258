{{Infobox journal
| title = European History Quarterly
| cover = [[File:European History Quarterly front cover.jpg]]
| editor = Professor Julian Swann
| discipline = [[History]]
| former_names = European Studies Review (until 1984; {{ISSN|0014-3111}}) 
| abbreviation = Eur. Hist. Q.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1971-present
| openaccess = 
| license = 
| impact = 0.262
| impact-year = 2014
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200846
| link1 = http://ehq.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ehq.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 123479187
| LCCN = 84643695
| CODEN = EHIQEH
| ISSN = 0265-6914
| eISSN = 1461-7110
}}
'''''European History Quarterly''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that publishes articles in the field of [[history]]. The journal was established in 1971 as the ''European Studies Review'' and obtained its current title in 1984. It covers a range of subjects from the later Middle Ages to post-1945.

'''Articles published in 2013 include:'''

Jennifer Foray,'A New Order, a New Empire: The Global Designs of the Dutch Nazi Party'.  

Maria Thomas, 'The faith and the fury: The construction of anticlerical collective identities in early twentieth-century Spain'.

Claudia Stein 'Images and Meaning-Making in a World of Resemblance: The Bavarian-Saxon Kidney Stone Affair of 1580'. 

Sarah E. Shurts, 'Resentment and the right: a twentieth-century cycle of reaction, revaluation, and retreat by the French extreme right'. 

Peter Polak-Springer, 'Jammin’ with Karlik’: the German-Polish radio war and the ‘Gleiwitz provocation,’1925-1939'. 

Bradley W. Hart, 'Science, politics, and prejudice: The dynamics and significance of British anthropology’s failure to confront Nazi racial ideology'. 

Mathias Persson, 'The utility of the ‘other’ German representations of Sweden in the second half of the eighteenth century'.

Joan Tumblety,‘Rethinking the fascist aesthetic: mass gymnastics, political spectacle and the stadium in 1930s France’. 

'''European History Quarterly Prize.'''

The journal awards an annual prize for the best article published each year. 
In 2012, the prize was awarded to Dr Paul R. Keenan for his article 'Card playing and gambling in eighteenth-century Russia', EHQ Vol. 42, number 3, July 2012.

In 2013, the prize was awarded to Dr Claudia Stein for her article 'Images and Meaning-Making in a World of Resemblance: The Bavarian-Saxon Kidney Stone Affair of 1580', EHQ Vol. 43, number 2, April 2013.

== Abstracting and indexing ==
''European History Quarterly'' is abstracted and indexed in [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.262, ranking it 136th out of 161 journals in the category "Political Science" and 53rd out of 87 journals in the category "History".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Political Science and History |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

==External links==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200846}}


[[Category:European history journals]]
[[Category:Publications established in 1971]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:SAGE Publications academic journals]]