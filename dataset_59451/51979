{{Infobox journal
| title = Journal of Neurogenetics
| cover = [[File:JNGcover.jpg]]
| editor = Chun-Fang Wu
| discipline = [[Neurogenetics]]
| abbreviation = J. Neurogenet.
| publisher = [[Taylor & Francis]]
| country =
| frequency = Quarterly
| history = 1983-present
| impact = 1.854
| impact-year = 2015
| website = http://journalofneurogenetics.org/index.php
| link1 = http://www.tandfonline.com/toc/ineg20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/ineg20
| link2-name = Online Archive
| JSTOR =
| OCLC = 10004179
| LCCN = 91640770
| CODEN = JLNEDK
| ISSN = 0167-7063
| eISSN = 1563-5260
}}
The '''''Journal of Neurogenetics''''' is a quarterly [[Peer review|peer-reviewed]] [[scientific journal]] that publishes research on the genetic basis of normal and abnormal function of the nervous system, within the field of [[neurogenetics]]. It is published by [[Taylor & Francis]], a division of [[Informa]]. The [[editor-in-chief]] is [[University of Iowa]] biologist Chun-Fang Wu.<ref>{{cite web |url=http://www.tandfonline.com/action/journalInformation?show=editorialBoard&journalCode=ineg20 |title= Editorial Board Members |accessdate=2016-09-07 |format= |work=tandfonline.com}}</ref>

== Abstracting and indexing ==
According to the ''[[Journal Citation Reports]]'', the journal's 2015 [[impact factor]] is 1.854.<ref name=WoS>{{cite web |url=http://www.tandfonline.com/toc/ineg20/current |title=Taylor & Francis Online |year=2015 |accessdate=2016-09-07}}</ref> In addition, the journal is indexed and abstracted in [[Aquatic Sciences and Fisheries Abstracts]], [[Elsevier BIOBASE]], [[Biological Abstracts]], [[BIOSIS Previews]], [[Chemical Abstracts Service]], [[EMBASE]], [[MEDLINE|Medline]]/[[PubMed]], [[Neuroscience Citation Index]], [[PsychINFO]], [[Science Citation Index Expanded]], [[Scopus]], and [[The Zoological Record]] .

== References ==
{{reflist}}

== External links ==
* {{Official|http://www.informahealthcare.com/neg}}

[[Category:Publications established in 1983]]
[[Category:Quarterly journals]]
[[Category:Behavioural genetics journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]