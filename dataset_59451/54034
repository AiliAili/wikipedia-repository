{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Albert Park
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 1,475
| pop_year            = {{CensusAU|2011}}
| pop_footnotes       = <ref name=ABS2011>{{Census 2011 AUS |id =SSC40004 |name=Albert Park (State Suburb) |accessdate=14 February 2015 | quick=on}}</ref>
| pop2                = 1,638
| pop2_year           = {{CensusAU|2006}}
| pop2_footnotes      = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41021 |name=Albert Park (State Suburb) |accessdate=28 May 2011 | quick=on}}</ref>
| est                 = 1877<ref name=Manning>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/a/a2.htm#albertp |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=29 May 2011}}</ref>
| postcode            = 5014<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/albert+park |title=Albert Park, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=28 May 2011}}</ref>
| area                = 
| dist1               = 8.9
| dir1                = NW
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = [[City of Charles Sturt]]<ref name=CharlesSturt>{{cite web |url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |title=City of Charles Sturt Wards and Council Members |author= |date= |work= |publisher=City of Charles Sturt |accessdate=1 June 2011}}</ref>
| stategov            = [[Electoral district of Cheltenham|Cheltenham]] <small>''(2011)''</small><ref name=ECSA>{{cite web |url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=28 May 2011}}</ref>
| fedgov              = [[Division of Port Adelaide|Port Adelaide]] <small>''(2011)''</small><ref name=AEC>{{cite web |url=http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Port+Adelaide&filterby=Electorate&divid=189 |title=Find my electorate: Port Adelaide |author= |date= |work= |publisher=Australian Electoral Commission |accessdate=28 May 2011}}</ref>
| near-n              = [[Queenstown, South Australia|Queenstown]]
| near-ne             = [[Cheltenham, South Australia|Cheltenham]]
| near-e              = [[Woodville, South Australia|Woodville]]
| near-se             = [[Woodville West, South Australia|Woodville West]]
| near-s              = [[Woodville West, South Australia|Woodville West]]
| near-sw             = [[Seaton, South Australia|Seaton]]
| near-w              = [[Hendon, South Australia|Hendon]]
| near-nw             = [[Hendon, South Australia|Hendon]]
}}

'''Albert Park''' is a [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Charles Sturt]].

==History==
Named for [[Albert, Prince Consort|Prince Albert]], Albert Park was laid out in 1877 by a W.R. Cave. The suburb was advertised as:<ref name=Manning/>

{{Quotation|Where the soil is suitable for flower and market gardens, being rich alluvial soil, and lucerne now growing there most luxuriantly and water can be obtained at six feet... Carters... will find it excellently situated as a stopping place for their teams and also for loading at night, being favourably placed in respect both to Adelaide and Port Adelaide.|||The Manning Index of South Australian History}}

In 1920 a parcel of {{convert|60|acre|ha|abbr=on}}  of land in Albert Park was bought by the aviator [[Harry Butler (aviator)|Harry Butler]], who set it up the Hendon Aerodrome. Part of this site was subdivided in 1921 for residential development, and together with the aerodrome this land became the new suburb of [[Hendon, South Australia|Hendon]]. The aerodrome was compulsorily acquired by the Commonwealth in 1922 and operated until 1927, when aviation operations were transferred to [[Parafield Airport|Parafield]].<ref>Marsden, Susan (1977): ''A history of Woodville.'' Corporation of the City of Woodville. Pp. 169-176. ISBN 0 9599828 4 1</ref>

''The Aerodrome'' Post Office opened on 19 August 1925. It was renamed ''Aero Park'' in 1945 and ''Albert Park'' in 1967, before closing in 1987.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Geography==
The suburb lies on the western side of the [[Port Road, Adelaide|Port Road]]-[[West Lakes Boulevard, Adelaide|West Lakes Boulevard]] intersection.<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The {{CensusAU|2006}} by the [[Australian Bureau of Statistics]] counted 1,638 persons in Albert Park on census night. Of these, 48% were male and 52% were female.<ref name=ABS2006/>

The majority of residents (72.8%) are of Australian birth, with other common census responses being [[Italy]] (4.0%) and [[England]] (2.8%).<ref name=ABS2006/>

The age distribution of Albert Park residents is skewed slightly higher than the greater Australian population. 71.5% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 28.5% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Politics==

===Local government===
Albert Park is part of West Woodville Ward in the [[City of Charles Sturt]] [[Local government in Australia|local government area]], being represented in that council by Tolley Wasylenko and Angela Keneally.<ref name=CharlesSturt/>

===State and federal===
Albert Park lies in the state [[Electoral districts of South Australia|electoral district]] of [[Electoral district of Cheltenham|Cheltenham]]<ref name=ECSA/> and the federal [[Divisions of the Australian House of Representatives|electoral division]] of [[Division of Port Adelaide|Port Adelaide]].<ref name=AEC/> The suburb is represented in the [[South Australian House of Assembly]] by [[Jay Weatherill]]<ref name=ECSA/> and [[Australian House of Representatives|federally]] by [[Mark Butler]].<ref name=AEC/>

==Community==

===Schools===
Our Lady Queen of Peace School is located on Botting Street <ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=28 May 2011| archiveurl= https://web.archive.org/web/20110426174836/http://australianschoolsdirectory.com.au/| archivedate= 26 April 2011 <!--DASHBot-->| deadurl= no}}</ref>

==Transportation==

===Roads===
Albert Park is serviced by [[Port Road, Adelaide|Port Road]], linking the suburb to [[Port Adelaide, Adelaide|Port Adelaide]] and [[Adelaide city centre]], and [[West Lakes Boulevard, Adelaide|West Lakes Boulevard]], which connects Albert Park to the shopping facilities at [[West Lakes, Adelaide|West Lakes]].<ref name=UBD/>

===Public transport===
Albert Park is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web |url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date= |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=28 May 2011| archiveurl= https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php| archivedate= 26 April 2011 <!--DASHBot-->| deadurl= no}}</ref>

====Trains====
The [[Grange railway line]] passes beside the suburb. The closest station is [[Albert Park railway station, Adelaide|Albert Park]].<ref name=Metro/>

Between 1940 and 1980, a further branch railway ran from Albert Park to a station within the industrial area of [[Hendon, South Australia|Hendon]].<ref>{{cite journal |last1=Milne |first1=Rod |last2= |first2= |year= |title=The Hendon Branch Line |journal=Australian Railway Historical Society Bulletin |volume= |issue=September 2002 |pages=323–327 |publisher= |doi= |url= |accessdate= }}</ref>

====Buses====
The suburb is serviced by bus routes run by the [[Adelaide Metro]].<ref name=Metro/>

==See also==
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.charlessturt.sa.gov.au |title=City of Charles Sturt |author= |date= |work=Official website |publisher=City of Charles Sturt |accessdate=28 May  2011| archiveurl= https://web.archive.org/web/20081013201303/http://www.charlessturt.sa.gov.au/| archivedate=13 October 2008<!--DASHBot-->| deadurl= no}}

{{Coord|-34.878|138.521|format=dms|type:city_region:AU-SA|display=title}}
{{City of Charles Sturt suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1877]]