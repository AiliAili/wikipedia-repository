The '''2010 Leinster Senior Football Championship''' was that year's installment of the annual [[Leinster Senior Football Championship]] held under the auspices of the [[Leinster GAA]]. It was won by [[Meath GAA|Meath]] who defeated [[Louth GAA|Louth]] in an eventful final on 11 July.<ref>{{cite news|url=http://www.rte.ie/sport/gaa/football/2012/0722/330148-dublin-meath/|title=Dublin 2-13 Meath 1-13|work=[[RTÉ Sport]]|date=22 July 2012|accessdate=22 July 2012}}</ref> A contentious goal was given. Irate Louth fans pursued the referee around the pitch at the final whistle, bottles were hurled from the stand and the mayhem was compared to soccer player [[Thierry Henry]]'s handball that [[2009 Republic of Ireland vs France football matches|cheated the Irish soccer team]] of their place at the [[2010 FIFA World Cup]] the previous November.<ref>{{cite news|url=http://www.rte.ie/sport/gaa/championship/2010/0711/crokerpark_meath_louth.html|title=Referee is attacked in Croke Park|work=[[RTÉ Sport]]|publisher=[[Raidió Teilifís Éireann]]|date=11 July 2010|accessdate=11 July 2010}}</ref><ref>{{cite news|first1=Patsy|last1=McGarry|first2=Conor|last2=Lally|url=http://www.irishtimes.com/newspaper/ireland/2010/0712/1224274518490.html|title=Referee shoved in chest after GAA final|date=12 July 2010|newspaper=[[The Irish Times]]|publisher=Irish Times Trust|accessdate=12 July 2010}}</ref><ref>{{cite news|first=Damian|last=Cullen|url=http://www.irishtimes.com/newspaper/sport/2010/0712/1224274518850.html|title=September Road|date=12 July 2010|newspaper=[[The Irish Times]]|publisher=Irish Times Trust|accessdate=12 July 2010|quote=After Joe “Thierry Henry” Sheridan's controversial handling of the ball at the end of yesterday's Leinster final, we wracked our brains for some of the other controversial and bizarre endings to GAA matches.}}</ref><ref>{{cite news|first=Johnny|last=Watterson|url=http://www.irishtimes.com/newspaper/sport/2010/0717/1224274900960.html|title=Hold the back page|date=17 July 2010|newspaper=[[The Irish Times]]|publisher=Irish Times Trust|accessdate=17 July 2010|quote=The unravelling of the Leinster football final sparked off radical suggestions the match should be replayed and while not as globally embarrassing as the FAI whimpering over the Thierry Henry hand ball, it seemed equally absurd. While Meath diplomacy forbids them to ridicule the “Replayistas” in the manner Fifa boss Sepp Blatter did with the FAI, be sure Meath players bridled at the suggestion.}}</ref><ref>{{cite news|first1=Allison|last1=Bray|first2=Colm|last2=Keys|first3=Denise|last3=Clarke|url=http://www.independent.ie/sport/gaelic-football/mayhem-and-madness-as-fans-attack-referee-2254900.html|title=Mayhem and madness as fans attack referee|date=12 July 2010|newspaper=[[Irish Independent]]|publisher=[[Independent News & Media]]|accessdate=12 July 2010|quote=Meath chairman Barney Allen refused to be drawn on a potential offer of a replay afterwards, drawing comparison with the Irish soccer team's plight last November after the Thierry Henry handball. "Ireland didn't get a replay when France got a lucky goal," said Mr Allen.}}</ref> Coincidentally, 11 July was also the date of the [[2010 FIFA World Cup Final]], which was played in [[South Africa]] later in the evening.

The winning Meath team received the [[Delaney Cup]], and automatically advanced to the quarter-final stage of the [[2010 All-Ireland Senior Football Championship]]. [[Kildare GAA|Kildare]] beat them there.<ref>{{cite news|url=http://www.rte.ie/sport/gaa/football/2010/0801/268794-meath_kildare/|title=Kildare storm past Meath to reach semis|work=[[RTÉ Sport]]|publisher=[[Raidió Teilifís Éireann]]|date=1 August 2010|accessdate=1 August 2010}}</ref>

==Bracket==
{{16TeamBracket-Compact-NoSeeds-Byes
| RD1=Preliminary round
| RD2=Quarter-finals
| RD3=Semi-finals
| RD4=Final
|group1=
|group2=
| RD1-seed01=
| RD1-team01='''[[Louth GAA|Louth]]'''
| RD1-score01='''1-11'''
| RD1-seed02=
| RD1-team02=[[Longford GAA|Longford]]
| RD1-score02=1-7
| RD1-seed05=
| RD1-team05='''[[Wicklow GAA|Wicklow]]'''
| RD1-score05='''3-13'''
| RD1-seed06=
| RD1-team06=[[Carlow GAA|Carlow]]
| RD1-score06=0-12
| RD1-seed09=
| RD1-team09='''[[Meath GAA|Meath]]'''
| RD1-score09='''1-20'''
| RD1-seed10=
| RD1-team10=[[Offaly GAA|Offaly]]
| RD1-score10=2-7
| RD2-seed01=
| RD2-team01='''[[Louth GAA|Louth]]'''
| RD2-score01='''1-22'''
| RD2-seed02=
| RD2-team02=[[Kildare GAA|Kildare]]
| RD2-score02=1-16
| RD2-seed03=
| RD2-team03=[[Wicklow GAA|Wicklow]]
| RD2-score03=1-11
| RD2-seed04=
| RD2-team04='''[[Westmeath GAA|Westmeath]]'''
| RD2-score04='''0-15'''
| RD2-seed05=
| RD2-team05='''[[Meath GAA|Meath]]'''
| RD2-score05='''2-14'''
| RD2-seed06=
| RD2-team06=[[Laois GAA|Laois]]
| RD2-score06=0-10
| RD2-seed07=
| RD2-team07=[[Wexford GAA|Wexford]]
| RD2-score07=0-15
| RD2-seed08=
| RD2-team08='''[[Dublin GAA|Dublin]]'''
| RD2-score08='''2-16'''
| RD3-seed01=
| RD3-team01='''[[Louth GAA|Louth]]'''
| RD3-score01='''1-15'''
| RD3-seed02=
| RD3-team02=[[Westmeath GAA|Westmeath]]
| RD3-score02=2-10
| RD3-seed03=
| RD3-team03='''[[Meath GAA|Meath]]'''
| RD3-score03='''5-9'''
| RD3-seed04=
| RD3-team04=[[Dublin GAA|Dublin]]
| RD3-score04=0-13
| RD4-seed01=
| RD4-team01=[[Louth GAA|Louth]]
| RD4-score01=1-10
| RD4-seed02=
| RD4-team02='''[[Meath GAA|Meath]]'''
| RD4-score02='''1-12'''
}}

==Preliminary round==
{{footballbox
| date = 16 May 2010
| round = Preliminary Round
| team1 = [[Wicklow GAA|Wicklow]]
| score =  3-13 – 0-12
| team2 = [[Carlow GAA|Carlow]]
| goals1 = L Glynn 1-4, T Hannon 0-6, P Earls, JP Dalton 1-0 each, S Furlong 0-2, P McWalter 0-1. <span style="color:red;">N Mernagh SO</span>
| goals2 = S Rea 0-4, A Curran, B Murphy 0-3 each, JJ Smith, D St Ledger 0-1 each. <span style="color:red;">B Murphy, T Walsh SO</span>
| stadium = [[O'Moore Park]], [[Portlaoise]]
| referee = G. Kinneavy ([[Galway]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0516/wicklow_carlow.html Report]
}}

{{footballbox
| date = 23 May 2010
| round = Preliminary Round
| team1 = [[Meath GAA|Meath]]
| score =  1-20 – 2-7
| team2 = [[Offaly GAA|Offaly]]
| goals1 = C Ward 0-8 J Sheridan 1-3, S O'Rourke 0-4, S Bray 0-2, G O'Brien, N Crawford, B Farrell 0-1 each.
| goals2 = K Casey 1-2, B Connor 1-0, N Darby 0-2, J Reynolds, N McNamee, A Sullivan 0-1 each. <span style="color:red;">J Coughlan SO</span>
| stadium = [[O'Moore Park]], [[Portlaoise]]
| referee = D. Fahy ([[Longford GAA|Longford]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0523/offaly_v_meath_leinster_football.html Report]
}}

{{footballbox
| date = 23 May 2010
| round = Preliminary Round
| team1 = [[Louth GAA|Louth]]
| score =  1-11 – 1-7
| team2 = [[Longford GAA|Longford]]
| goals1 = B White 0-4, S Lennon 1-0, C Judge, P Keenan, JP Rooney 0-2 each, D Byrne 0-1.
| goals2 = B McElvaney 1-0, F McGee, S McCormack 0-2 each, P Foy, P Dowd, K Mulligan 0-1 each.
| stadium = [[O'Moore Park]], [[Portlaoise]]
| referee = P. Fox ([[Westmeath GAA|Westmeath]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0523/louth_v_longford_leinster_football.html Report]
}}

==Quarter-finals==
{{footballbox
| date = 5 June 2010
| round = Quarter Final
| team1 = [[Louth GAA|Louth]]
| score =  1-22 - 1-16
| team2 = [[Kildare GAA|Kildare]]
| goals1 =  B White 0-6, C Judge, S Lennon 0-4 each, A McDonnell, JP Rooney 0-3 each, R Finnegan 1-0, M Brennan, A Reid 0-1 each.
| goals2 = J Doyle 0-9, P O'Neill 1-1, J Kavanagh 0-2, D Flynn, D Whyte, R Sweeney, A Smith 0-1 each.
| stadium = [[Páirc Tailteann]], [[Navan]]
| referee = M. Duffy ([[Sligo GAA|Sligo]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0605/louth_kildare_report.html Report]
}}

{{footballbox
| date = 6 June 2010
| round = Quarter Final
| team1 = [[Wicklow GAA|Wicklow]]
| score = 1-11 - 0-15
| team2 = [[Westmeath GAA|Westmeath]]
| goals1 = T Hannon 0-5, P Earls 1-1, S Furling 0-2, C Hyland, D Hayden, N Mernagh 0-1 each. <span style="color:red;">L Glynn SO</span>
| goals2 = M Flanagan, C Lynam, P Greville 0-3 each, D Glennon 0-2, P Bannon, M Ennis, D Harte, D Dolan 0-1 each. <span style="color:red;">J Gaffey, D Duffy SO</span>
| stadium = [[O'Connor Park]], [[Tullamore]]
| referee = M. Collins ([[Cork GAA|Cork]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0606/wicklow_westmeath.html Report]
}}

{{footballbox
| date = 13 June 2010
| round = Quarter Final
| team1 = [[Meath GAA|Meath]]
| score = 2-13 - 1-16<br />AET
| team2 = [[Laois GAA|Laois]]
| goals1 = G Reilly 1-4, J Sheridan 0-3, C McGuinness 1-0, C Ward, S O'Rourke 0-2 each, S Bray, C Gillespie 0-1 each.
| goals2 = D Kingston 0-6,  MJ Tierney 0-3, P McMahon 1-0, P Clancy, J O'Loughlin 0-2 each, C Healy, D Strong, R Munnelly 0-1 each.
| stadium = [[Croke Park]], [[Dublin]]
| referee = M. Duffy ([[Sligo GAA|Sligo]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0613/meath_laois.html Report]
| attendance = 49,757
}}

{{footballbox
| date = 13 June 2010
| round = Quarter Final
| team1 = [[Wexford GAA|Wexford]]
| score = 0-15 - 2-16<br />AET
| team2 = [[Dublin GAA|Dublin]]
| goals1 =  M Forde 0-7, S Roche, R Barry 0-2 each, E Bradley, A Flynn, C Lyng, PJ Banville 0-1 each.
| goals2 =  B Brogan 2-4, T Quinn 0-4, C Keaney, A Brogan 0-2  M McAuley, P Flynn, B Cullen, K Nolan 0-1 each.
| stadium = [[Croke Park]], [[Dublin]]
| referee = M. Haggins ([[Fermanagh GAA|Fermanagh]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0613/dublin_wexford.html Report]
| attendance = 49,757
}}

{{footballbox
|date= 19 June 2010
|round= Quarter Final Replay
|team1= [[Meath GAA|Meath]]
|score= 2-14 - 0-10
|team2= [[Laois GAA|Laois]]
|goals1= J Sheridan 2-2, G Reilly 0-4, C Ward, S O'Rourke 0-3 each, C King, B Meade 0-1 each.
|goals2= MJ Tierney 0-4, D Kingston 0-2, P O'Leary, P McMahon, C Healy, D Strong 0-1 each.
|stadium= [[O'Connor Park]], [[Tullamore]]
|referee= M. Condon ([[Waterford GAA|Waterford]])
|report= [http://www.rte.ie/sport/gaa/championship/2010/0619/laois_meath.html Report]
|attendance = 12,659
}}

==Semi-finals==
{{footballbox
| date = 27 June 2010
| round = Semi Final
| team1 = [[Louth GAA|Louth]]
| score = 1-15 - 2-10
| team2 = [[Westmeath GAA|Westmeath]]
| goals1 = C Judge 1-2, B White 0-4, P Keenan 0-3, S Lennon 0-2, M Brennan, A Reid, P Smith, D Maguire 0-1 each
| goals2 = P Greville 0-5, P Bannon 1-1, M Flanagan 1-0, D Glennon 0-2, D Dolan, G Egan 0-1 each
| stadium = [[Croke Park]], [[Dublin]]
| referee = S. Doyle ([[Wexford GAA|Wexford]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0627/louth_westmeath.html Report]
| attendance = 60,035
}}

{{footballbox
| date = 27 June 2010
| round = Semi Final
| team1 = [[Dublin GAA|Dublin]]
| score = 0-13 - 5-9
| team2 = [[Meath GAA|Meath]]
| goals1 = T Quinn 0-7, B Brogan 0-3, B Cullen, C Keaney, K McManamon 0-1 each
| goals2 = S Bray 2-1, C Ward 1-4, J Sheridan, B Farrell 1-0 each, S O'Rourke 0-3, G Reilly 0-1
| stadium = [[Croke Park]], [[Dublin]]
| referee = P. Hughes ([[Armagh GAA|Armagh]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0627/dublin_meath.html Report]
| attendance = 60,035
}}

==Final==
{{main|2010 Leinster Senior Football Championship Final}}
{{footballbox
| date = 11 July 2010
| round = Final
| team1 = [[Louth GAA|Louth]]
| score = 1-10 - 1-12
| team2 = [[Meath GAA|Meath]]
| goals1 = B White 0-4, JP Rooney 1-1, C Judge 0-2, P Keenan, A Reed, A McDonnell 0-1 each
| goals2 = G Reilly, C Ward 0-4 each, J Sheridan 1-0, S Bray 0-2, A Moyles 0-1
| stadium = [[Croke Park]], [[Dublin]]
| attendance = 48,875
| referee = M. Sludden ([[Tyrone GAA|Tyrone]])
| report = [http://www.rte.ie/sport/gaa/championship/2010/0711/louth_meath.html Report]
}}

==References==
{{reflist}}

==External links==
* [http://www.leinstergaa.ie/ Leinster GAA website]

{{Leinster Senior Football Championship}}

[[Category:2010 in Gaelic football|2L]]
[[Category:Leinster Senior Football Championship]]