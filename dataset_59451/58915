{{Infobox journal
| title = Microbiology and Molecular Biology Reviews
| cover = [[File:Microbiology and Molecular Biology Reviews cover.gif]]
| editor = [[Diana Downs]] 
| discipline = [[Microbiology]]
| abbreviation = Microbiol. Mol. Biol. Rev.
| formernames = Bacteriological Reviews, Microbiological Reviews
| publisher = [[American Society for Microbiology]]
| country = United States
| frequency = Quarterly
| history = 1937-present
| openaccess = Delayed, after 12 months
| impact = 14.167
| impact-year = 2015
| website = http://mmbr.asm.org/ 
| link1 = http://mmbr.asm.org/current.dtl
| link1-name = Online access
| link2 = http://mmbr.asm.org/archive/
| link2-name = Online archive
| JSTOR = 
| OCLC = 36145205
| LCCN = 97660630
| CODEN = MMBRF7
| ISSN = 1092-2172
| eISSN = 1098-5557
}}
'''''Microbiology and Molecular Biology Reviews''''' (published as MMBR) is a [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[American Society for Microbiology]].

== History ==
The journal was established in 1937 as ''Bacteriological Reviews'' ({{ISSN|0005-3678}}) and changed its name in 1978 to ''Microbiological Reviews'' ({{ISSN|0146-0749}}). It obtained its current title in 1997.<ref>{{cite journal |last=Schaechter |first=Moselio |title=A Paean to Microbiology and Molecular Biology Reviews |journal=Microbiology and Molecular Biology Reviews |volume=63 |issue=2 |pages=265 |date=1999 |issn=1092-2172 |pmc=98965 |pmid=10357850}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://mmbr.asm.org/}}

[[Category:Delayed open access journals]]
[[Category:Microbiology journals]]
[[Category:Publications established in 1937]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]


{{biology-journal-stub}}