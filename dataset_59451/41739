{{Infobox military structure
|name        =Princess Amelia's Battery
|native_name =
|partof      =[[Fortifications of Gibraltar]]
|location    =[[Upper Rock Nature Reserve]], [[Gibraltar]]
|image       =[[File:Princess Amelia's Battery, 2013.JPG|300px]]
|caption     =Two remaining buildings at Princess Amelia's Battery
|map_type    =Gibraltar
|map_size    =300
|map_alt     =
|map_caption =Location of Princess Amelia's Battery in [[Gibraltar]]
|type        =
|coordinates = {{coord|36.145334|-5.347271|type:landmark_region:GI|display=inline}}
|code        =
|built       ={{Start date|1732}}
|builder     =
|materials   =
|height      =
|used        =
|demolished  =
|condition   =Poor
|ownership   =[[Government of Gibraltar]]
|open_to_public =
|controlledby =
|garrison    =
|current_commander =
|commanders  =
|occupants   =
|battles     =
|events      =
|image2      =[[File:Princess Anne's Battery diagram.png|300px]]
|caption2    =Diagram of Princess Amelia's and Princess Anne's Batteries with north to the left. E - Gun No. 4 of [[Princess Anne's Battery]]
}}
'''Princess Amelia's Battery''' is an [[artillery battery]] in the [[British Overseas Territory]] of [[Gibraltar]]. It is located on Willis's Plateau at the northern end of the [[Upper Rock Nature Reserve]], adjacent to Gun No. 4 of [[Princess Anne's Battery]]. It was named after [[Princess Amelia of Great Britain]], the second daughter of [[George II of Great Britain|George II]]. It was formerly referred to as the 2nd Willis's Battery. The plateau and its batteries had previously been named after an artillery officer by the name of Willis who was outstanding during the [[capture of Gibraltar]] in 1704.  Princess Amelia's Battery saw action during the [[Great Siege of Gibraltar]], during which it sustained substantial damage. Little remains of the original site, aside from two derelict buildings. The battery is listed with the [[Gibraltar Heritage Trust]].

==Early history==
[[File:Three princesses by Maingaud.jpg|thumb|left|The daughters of [[George II of Great Britain|King George II]] ([[Anne, Princess Royal and Princess of Orange|Anne]], [[Princess Amelia of Great Britain|Amelia]], and [[Princess Caroline of Great Britain|Caroline]]), after whom three batteries in [[Gibraltar]] were named]]
[[File:Princess Amelia's Battery 1780.png|thumb|left|Princess Amelia's Battery, c. 1780 by [[Koehler gun depression carriage|gun depression carriage]] designer [[George Koehler]].]]
Princess Amelia's Battery is in Gibraltar, the British Overseas Territory at the southern end of the [[Iberian Peninsula]].<ref>{{cite web|title=List of Crown Dependencies & Overseas Territories|url=http://www.fco.gov.uk/en/publications-and-documents/treaties/uk-overseas-territories/list-crown-dependencies-overseas|work=fco.gov.uk|publisher=[[Foreign and Commonwealth Office]]|accessdate=16 October 2012}}</ref><ref>{{cite news|title=Neandertals' Last Stand was in Gibraltar, Study Suggests|url=http://news.nationalgeographic.com/news/2006/09/060913-neanderthals.html|accessdate=16 October 2012|newspaper=National Geographic News|agency=[[National Geographic Society]]|date=10 October 2006|author=Roach, John}}</ref> The artillery battery is located on Willis's Plateau at the northern end of the Upper Rock Nature Reserve, above [[Princess Caroline's Battery]] and adjacent to the fourth gun of Princess Anne's Battery. Both Princess Amelia's Battery and the adjacent Gun No. 4 are on higher ground than that of Guns No. 1 through 3 of Princess Anne's Battery.<ref>{{cite web|title=Map of Princess Amelia's Battery|url=https://maps.google.com/?q=36.145334,-5.347271&num=1&t=h&z=20|work=maps.google.com|publisher=[[Google Maps]]|accessdate=16 October 2012}}</ref><ref name=discover>{{cite web|title=Princess Anne's Battery No.4 Gun (E)|url=http://www.discovergibraltar.com/pages/mainlogo/mainfrm.htm|work=discovergibraltar.com|publisher=DiscoverGibraltar.com (Click Upper Rock Nature Reserve, Princess Anne's Battery, E 360 view at No.4 Gun)|accessdate=16 October 2012}}</ref>

The site was initially known as the 2nd Willis's Battery.<ref name=discover/> The Willis's Batteries were constructed at the area of the former Reduto ({{lang-en|[[Redoubt]]}}) de San Joachim, at an elevation of 440 feet above the isthmus.<ref>{{cite book|url=https://books.google.com/books?id=BC5QBR0oB04C&dq=Princess+Amelia%27s+Battery&q=Princess+Amelia%27s+Battery#v=snippet&q=Princess%20Amelia's%20Battery&f=false|title=The Fortifications of Gibraltar 1068-1945|date=31 October 2006|publisher=Osprey Publishing|isbn= 9781846030161|author=Fa, Darren|author2=Finlayson, Clive |author3=Hook, Adam |accessdate=16 October 2012|page=28}}</ref> During the capture of Gibraltar in 1704, an artillery officer by the name of Willis was outstanding in his manning of the guns. To honor the officer, the Batteries of San Joachim and the Bastion of San Jose were renamed Willis's.<ref>{{cite web|title=Entrance to Tunnels (A)|url=http://www.discovergibraltar.com/pages/mainlogo/mainfrm.htm|work=discovergibraltar.com|publisher=DiscoverGibraltar.com (Click Tunnels, 18th Century Tunnels (Middle Galleries), Entrance to Tunnels (A))|accessdate=16 October 2012}}</ref> The site was renamed again after Princess Amelia (1711 &ndash; 1786), the second daughter of King George II.<ref name=discover/><ref>{{cite web|title=King George II|url=http://www.nndb.com/people/021/000086760/|work=nndb.com|publisher=NNDB - Soylent Communications|accessdate=16 October 2012}}</ref><ref>{{cite web|title=Princess Amelia Sophia Eleanora (1711-1786), Second daughter of George II|url=http://www.npg.org.uk/collections/search/person/mp00095/princess-amelia-sophia-eleanora|work=npg.org.uk|publisher=National Portrait Gallery|accessdate=16 October 2012}}</ref> The proximity of the battery to Princess Anne's Battery is such that the latter is sometimes mistakenly referred to as Princess Amelia's Battery.<ref name=discovergib>{{cite web|title=Princess Anne's Battery No.1 Gun (A)|url=http://www.discovergibraltar.com/pages/mainlogo/mainfrm.htm|work=discovergibraltar.com|publisher=DiscoverGibraltar.com (Click Upper Rock Nature Reserve, then Princess Anne's Battery)|accessdate=16 October 2012}}</ref> Princess Amelia's Battery was one of several on Willis's Plateau that also included Princess Anne's Battery and [[Princess Royal's Battery]].<ref name=discover/>

Princess Amelia's Battery was first armed in 1732.<ref name=discover/> Three decades later, during the [[Anglo-Spanish War (1761–1763)|Spanish War of 1762]], there were six cannons at the battery, four 6-pounders and two 9-pounders.<ref name=discover/> By 1771, Princess Amelia's Battery had five [[embrasures]] directed in front and three additional cannons.<ref>{{cite book|title=The history of the Herculean Straits: now called the Straits of Gibraltar: including those ports of Spain and Barbary that lie contiguous thereto. Illustrated with several copper plates, Volume 2|year=1771|publisher=Printed by C. Rivington for the authour|url=https://books.google.com/books?id=3b9DAAAAYAAJ&pg=PA302&lpg=PA302&dq=Princess+Amelia's+Battery&source=bl&ots=6_jd8xpNoE&sig=AVFcZ6XzM__ieNI7BoBEW40aoRM&hl=en&sa=X&ei=8M59UOK6L-qLyAGx_IG4BQ&ved=0CE8Q6AEwBjgK#v=onepage&q=Princess%20Amelia's%20Battery&f=false|author=James, Thomas|accessdate=16 October 2012|page=302}}</ref> There were half a dozen guns behind embrasures by 1773.<ref name=discover/> Several years later, during the Great Siege of Gibraltar, many of the emplacements on Willis's Plateau, including those of Princess Amelia's Battery, sustained severe damage.<ref name=discover/> Of the [[fortifications of Gibraltar]], the batteries on Willis's Plateau received the brunt of the attack from the [[Spain|Spanish]] during the siege. Gilbard and [[John Drinkwater Bethune|Drinkwater Bethune]], among others, related the story of a shot which found its way through an embrasure in Princess Amelia's Battery, hitting four of the soldiers, and amputating between three and seven lower extremities in the process, depending on the source.<ref>{{cite book|title=A popular history of Gibraltar, its institutions, and its neighbourhood on both sides of the straits, and a guide book to their principal places and objects of interest|year=1888|publisher=Garrison Library|url=https://books.google.com/books?id=D6oKZxBZId4C&q=Princess+Amelia%27s#v=onepage&q=Princess%20Amelia's&f=false|author=Gilbard, Lieutenant Colonel George James|accessdate=16 October 2012|page=70}}</ref><ref>{{cite book|title=Encyclopædia Britannica; or A dictionary of arts, sciences, and miscellaneous literature, Volume 9|year=1823|publisher=Printed for Archibald Constable and Company|url=https://books.google.com/books?id=HconAAAAMAAJ&pg=PA711&lpg=PA711&dq=Princess+Amelia's+Battery&source=bl&ots=QjA_pdL5H8&sig=eLWRDOV7L-nyq0fsx8lkTEDU944&hl=en&sa=X&ei=_rh9UMr6DYbJyAGVnIDICQ&ved=0CDoQ6AEwBjgU#v=onepage&q=Princess%20Amelia's%20Battery&f=false|edition=6|accessdate=16 October 2012|page=711}}</ref><ref>{{cite book|title=A history of the late siege of Gibraltar|year=1786|url=https://books.google.com/books?id=yT4OAAAAQAAJ&pg=PA294&lpg=PA294&dq=Princess+Amelia's+Battery&source=bl&ots=SYad8DUjBA&sig=l7LwpkxvRuuOV2aSOB2ocwG4fi0&hl=en&sa=X&ei=B799UPDzHavYyAHs74DwBA&ved=0CDAQ6AEwAA#v=onepage&q=Princess%20Amelia's%20Battery&f=false|author=[[John Drinkwater Bethune|Drinkwater Bethune, John]]|edition=2|accessdate=16 October 2012|pages=226–227}}</ref>

==Recent history==
Two derelict buildings remain at the site of Princess Amelia's Battery, which is listed with the Gibraltar Heritage Trust.<ref name=discover/><ref>{{cite web|title=Gibraltar Heritage Trust Act 1989|url=http://www.gibraltarlaws.gov.gi/articles/1989-12o.pdf|work=gibraltarlaws.gov.gi|publisher=[[Government of Gibraltar]]|accessdate=16 October 2012}}</ref>

== References ==
{{Reflist}}

== External links ==
* [https://maps.google.com/?q=36.145334,-5.347271&num=1&t=h&z=20 Google map of Princess Amelia's Battery]
* [http://www.subbrit.org.uk/sb-sites/sites/p/princess_annes_battery/index16.shtml Photograph of two buildings at Princess Amelia's Battery]
* [http://www.subbrit.org.uk/sb-sites/sites/p/princess_annes_battery/index.shtml Diagram of position of Princess Amelia's Battery relative to other batteries]
{{Commons category}}
{{Fortifications of Gibraltar}}
{{Use dmy dates|date=March 2017}}

[[Category:Articles created via the Article Wizard]]
[[Category:Batteries in Gibraltar]]
[[Category:Coastal artillery]]