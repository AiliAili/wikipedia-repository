<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Bonney Gull
 | image=Bonney Gull before it crashed & killed its maker, Curtis Field, N.Y.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Experimental aircraft]]
 | national origin=[[United States]]
 | manufacturer=
 | designer=[[Leonard Warden Bonney]]
 | first flight=4 May 1928
 | introduced=1928
 | retired=
 | status=Crashed on first flight
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost=$83000 in 1928 <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Bonney Gull''' was an [[experimental aircraft]] that used variable incidence wings with bird-like shapes.<ref>{{cite book|title=The challenging skies: the colorful story of aviation's most exciting years, 1919-1939|author=Cecil R. Roseberry}}</ref>

==Development==
[[Leonard Warden Bonney]] was an early aviator, who flew with the [[Wright Exhibition Team]] as early as 1910. An experienced aviator with service in the [[First World War]], Bonney set out to develop a plane with more efficient wings and controls than contemporary aircraft. Noting the [[gull]]'s two to one lift to weight ratio, he set about molding gull wings for their shape. Construction took place over the course of five years. The ideas were tested in MIT and the Daniel Guggenheim School of Aeronautics wind tunnels.<ref>{{cite journal|magazine=Popular Science Monthly|date=December 1926|title=New Airplane built like a gull|page=44}}</ref>

==Design==
A 1928 issue of [[Time magazine]] described the unusual aircraft:

<blockquote>
It was fat in body with graceful curving wings. Bonney followed the bird principle, abandoned the [[aileron]], or balancing contrivance which airplane designers have always considered an essential feature of stability in the air. His plane had new features: an expanding and contracting tail, like a blackbird's, for varying loads; variable [[camber (aerodynamics)|camber]] in the wings, so that they could flatten out like a gull's when flying level; a varying [[angle of incidence (aerodynamics)|angle of incidence]] to its wings, so that they could turn sideways into the wind on landing...<ref name="time"
>{{cite web 
| url= http://www.time.com/time/magazine/article/0,9171,787255,00.html
| title= AERONAUTICS: Aerodynamics
| publisher= ''Time''
| date= May 14, 1928
| accessdate= 2009-10-10
}}</ref>
</blockquote>

The Gull was assembled at the Kirkham facility in [[Garden City, New York]] and [[Mitchel Field]]. It used [[conventional landing gear]], a [[mid-wing]] arrangement, corrugated [[aluminum]] skins, and a [[radial engine]]. The cockpit featured a large streamlined greenhouse bubble with two seats. The large tailwheel was steerable and fully [[Aircraft fairing|faired]]. The tail used small vertical stabilizers, with large elevators that could be swept back in flight.<ref name="Stroff44">{{Cite book|title=Long Island Aircraft Manufacturers|author=Joshua Stoff|page=44}}</ref> The aircraft profile was not unusual for the era with the exception of the highly tapered and swept back wings with a large dihedral and large tapered tail surfaces.

Some features seemed far-fetched, but were actually ahead of their time.<ref>{{cite journal|magazine=Popular Science|date=October 2003|page=136}}</ref> The wings mounted flush with the aircraft, but had a mechanism which allowed them to rotate in angle of incidence, as well as fold rearward from ten degrees of sweep to forty five degrees for braking and storage.<ref>{{cite book|title=World encyclopaedia of aircraft manufacturers: from the pioneers to the present day|author=Gunston, Bill}}</ref> These features were incorporated on future carrier aircraft such as the [[Fairey Firefly]] for storage rather than braking and flight control. Variable incidence mechanisms are used commonly to trim tail surfaces, however some aircraft such as the [[Vought F-8 Crusader]] have deployed the concept in practical use. The variable sweep mechanism can be seen on some high-speed aircraft such as the [[General Dynamics F-111 Aardvark]] to expand their flight envelope. Bonney used differential sweep for roll control.<ref name="Stroff44" />

==Operational history==
Bonney was unable to find a willing test pilot and chose to fly the aircraft himself.<ref>{{cite book|title=Getting off the ground: the pioneers of aviation speak for themselves|author=George Vecsey, George C. Dade}}</ref> He performed a test hop, damaging the landing gear once. On 4 May 1928 Bonney took up another aircraft on a flight, then announced he would test fly the Gull that day. Bonney was killed during the [[maiden flight]] when the aircraft nosedived into the ground from about 50 feet of altitude, seconds after taking off from [[Curtiss Field]] on [[Long Island]].<ref>{{cite book|title=Test pilot|author=James Collins}}</ref> [[Pathé News]] was onsite to film the first flight. The newsreel shows the aircraft experiencing a roll to the left which was corrected, and a single oscillation in pitch before nosing straight down into the ground tossing out Bonney. Bonney was taken to a [[Mineola Hospital]] where he died.<ref>{{cite news|newspaper=The New York Time|date=5 May 1928|page=1}}</ref><ref>{{cite book|title=Long Island aircraft crashes 1909-1959|author=Joshua Stoff|page=51}}</ref><ref>{{cite news|newspaper=New York Times|date=May 5, 1928|title=Curtiss Field, Long Island, May 4, 1928. Leonard W. Bonney, pioneer aviator who learned to fly under Orville Wright in 1910, was fatally injured this afternoon in the first flight he had ever made with his Gull}}</ref>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Bonny Gull) ==
{{Aircraft specs
|ref=Long Island Aircraft Manufacturers<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Charles B. Kirkham|Kirkham]]
|eng1 type=9 cylinder Radial
|eng1 kw=<!-- prop engines -->
|eng1 hp=180<!-- prop engines -->
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=Y
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
*[[Kirkham Air Yacht]] - A gull winged aircraft built by Kirkham<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}

==External links==
*[http://www.earlyaviators.com/ebonney.htm Early Aviators]
*[http://www.spoonercentral.com/Bonney/LWB.html Spooner]
*[http://www.findarticles.com/p/articles/mi_qa3897/is_200106/ai_n8972663 Flight Journal: Bonney Gull]
*{{British pathe|id=16135|v=-m-AUvXSW3U|d=Newsreel footage of the flight of the ''Bonney Gull'' (silent)|year=1928}}
*[https://www.youtube.com/watch?v=97Udr_1FkiI.htm Youtube]

[[Category:Homebuilt aircraft]]