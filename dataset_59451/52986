{{Use dmy dates|date=February 2013}}
{{Infobox journal
| title = The ISME Journal
| cover = 
| editor = Mark Bailey, [[George Kowalchuk]]
| discipline = [[Microbial ecology]]
| formernames = 
| abbreviation = ISME J.
| publisher = [[Nature Publishing Group]]
| country =
| frequency = Monthly
| history = 2007&ndash;present
| openaccess =
| license = 
| impact = 9.302
| impact-year = 2014
| website = http://www.nature.com/ismej/index.html
| link1 = http://www.nature.com/ismej/archive/index.html
| link1-name = Online archive
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 182943088
| LCCN = 2007243685
| CODEN = 
| ISSN = 1751-7362
| eISSN = 1751-7370
}}
'''''The ISME Journal''': Multidisciplinary Journal of Microbial Ecology'' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering all areas of [[microbial ecology]] spanning the breadth of microbial life, including [[bacteria]], [[archaea]], microbial [[eukaryotes]], and [[virus]]es. It is an official publication of the [[International Society for Microbial Ecology]] (ISME) and publishes original research articles, [[Literature review|reviews]], and commentaries. The founding [[editors-in-chief]] are Mark Bailey and George Kowalchuk.  The journal is published on behalf of ISME by the [[Nature Publishing Group]]. According to the ''[[Journal Citation Reports]]'', the journal had a 2013 [[impact factor]] of 9.267, ranking it 4th out of 141 journals in the category "Ecology".<ref name=WoS1>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Ecology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> and 9th out of 119 in the category "Microbiology".<ref name=WoS2>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Microbiology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.nature.com/ismej/index.html}}

{{DEFAULTSORT:Isme Journal, The}}
[[Category:Ecology journals]]
[[Category:Microbiology journals]]
[[Category:Publications established in 2007]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]