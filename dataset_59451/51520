{{Infobox journal
| cover         = 
| editor        = [[Leila De Floriani]]
| discipline    = [[Visualization (computer graphics)|Visualization]], [[Computer graphics]], and [[Virtual reality]]
| peer-reviewed = Yes
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = [[IEEE Computer Society]] 
| country       = United States
| frequency     = Monthly
| history       = 1995–
| openaccess   = Hybrid
| license       = 
| impact        = 2.34
| impact-year   = 2016
| website       = https://www.computer.org/web/tvcg
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 30748801
| LCCN          = 
| CODEN         = 
| ISSN          = 1077-2626
| eISSN         = 
| boxwidth      = 
}}
'''''IEEE Transactions on Visualization and Computer Graphics (TVCG)''''' is an academic journal published by the [[IEEE Computer Society]]. The journal publishes papers on subjects related to [[computer graphics]] and [[visualization]] techniques, systems, software, hardware, and user interface issues.<ref>{{cite book|last1=Chen|first1=Chaomei|last2=Hou|first2=Haiyan|last3=Hu|first3=Zhigang|last4=Liu|first4=Shengo|editor1-last=Dill|editor1-first=Jon|editor2-last=Earnshaw|editor2-first=Rae|editor3-last=Kasik|editor3-first=David|editor4-last=Vince|editor4-first=Jon|editor5-last=Wong|editor5-first=Pak Chung|title=Expanding the Frontiers of Visual Analytics and Visualization|date=17 April 2012|publisher=Springer|isbn=9781447128045|page=22|chapter=An Illuminated Path: The Impact of the Work of Jim Thomas}}</ref><ref>{{cite web|title=IEEE Transactions on Visualization and Computer Graphics|url=https://www.computer.org/web/tvcg|publisher=IEEE Computer Society|accessdate=7 April 2017}}</ref> TVCG is considered the top journal in the field of visualization.<ref>{{cite web|last1=Kosara|first1=Robert|title=A Guide to the Quality of Different Visualization Venues|url=https://eagereyes.org/blog/2013/a-guide-to-the-quality-of-different-visualization-venues|website=eagereyes|accessdate=7 April 2017|date=11 November 2013}}</ref><ref>{{cite web|last1=Elmqvist|first1=Niklas|title=Top Scientific Conferences and Journals in InfoVis {{!}} Niklas Elmqvist, Ph.D.|url=https://sites.umiacs.umd.edu/elm/2016/01/21/infovis-venues/|website=UMIACS|publisher=University of Maryland, College Park|accessdate=7 April 2017|date=21 January 2016}}</ref> The journal allows ether traditional manuscript submission, or [[open access]] manuscript submission to allow unrestricted public access to the article via [[IEEE Xplore]] (if selected).

Since 2011, TVCG has allowed authors to present recently accepted papers at partner conferences.<ref>{{cite web|title=Conference Partners|url=https://www.computer.org/web/tvcg/conference-partners|website=Transactions on Visualization and Computer Graphics|publisher=IEEE Computer Society|accessdate=7 April 2017}}</ref> These conferences include:
*[[IEEE Visualization|IEEE Visualization (VIS)]], including SciVis, InfoVis, and VAST
*IEEE Virtual Reality Conference (IEEE VR)
*[[International Symposium on Mixed and Augmented Reality|IEEE International Symposium on Mixed and Augmented Reality (ISMAR)]]
*ACM Symposium on Interactive 3D Graphics and Games (I3DG)
*IEEE Pacific Visualization Conference (IEEE PacVis)
*[[ACM SIGGRAPH]]/Eurographics Symposium on Computer Animation (SCA)
*[[Symposium on Geometry Processing|Eurographics Symposium on Geometry Processing (SGP)]]
*Pacific Graphics Conference (PG)
*Eurovis - The EG and [[Technical Committee on Visualization and Graphics|VGTC]] Conference on Visualization
*Graphics Interfaces (GI)

==References==
{{Reflist}}

[[Category:IEEE academic journals]]
[[Category:Computer science journals]]
[[Category:Computer graphics]]
[[Category:Visualization (graphic)]]
[[Category:Virtual reality]]