{{Use dmy dates|date=April 2012}}
{{Infobox officeholder
|honorific-prefix= [[Admiral of the Fleet]] [[the Right Honourable]]
|name= The Lord Keyes
|honorific-suffix= {{post-nominals|country=GBR|size=100%|sep=,|GCB|KCVO|CMG|DSO}}
|image= Sir Roger Keyes.jpg
|image_size= 
|alt= 
|caption= Vice Admiral Sir Roger Keyes, 1918
|office = Member of the [[House of Lords]]<br>as [[Baron Keyes]]
|term_start = 22 January 1943
|term_end = 26 December 1945
|predecessor = ''Peerage created''
|successor = Roger George Bowlby Keyes
|office2 = Member of Parliament<br>for [[Portsmouth North (UK Parliament constituency)|Portsmouth North]]
|term_start2 = 19 February 1934
|term_end2 = 22 January 1943
|predecessor2 = [[Bertram Falle, 1st Baron Portsea|Sir Bertram Falle]]
|successor2 = [[William James (Royal Navy admiral)|Sir William James]]
|birth_date= {{Birth date|df=yes|1872|10|4}}
|death_date= {{Death date and age|df=yes|1945|12|26|1872|10|4}}
|birth_place= [[Punjab (British India)|Punjab]], [[British India]]
|death_place= [[Tingewick]], [[United Kingdom]]
|placeofburial= St James's Cemetery, [[Dover]]
|nickname= 
|party = [[Conservative Party (UK)|Conservative]]
|allegiance= United Kingdom
|branch= [[Royal Navy]]
|serviceyears= 1885–1935<br/>1940–1941
|rank= [[Admiral of the Fleet (Royal Navy)|Admiral of the Fleet]]
|servicenumber= 
|unit= 
|commands= {{HMS|Opossum|1895|6}} (1898–99)<br/>{{HMS|Hart|1895|6}} (1899–00)<br/>{{HMS|Fame|1896|6}} (1900–01)<br/>{{HMS|Bat|1896|6}} (1901)<br/>{{HMS|Falcon|1899|6}} (1902)<br/>{{HMS|Sprightly|1900|6}} (1902)<br/>{{HMS|Venus|1895|6}} (1908–10)<br/>Commodore-in-Charge, Submarine Service (1912–14)<br/>{{HMS|Centurion|1911|6}} (1916–17)<br/>[[Dover Patrol]] (1917–18)<br/>[[Battlecruiser Squadron (United Kingdom)|Battlecruiser Squadron]], [[Atlantic Fleet (United Kingdom)|Atlantic Fleet]] (1919–21)<br/>Commander-in-Chief, [[Mediterranean Fleet]] (1925–28)<br/>[[Commander-in-Chief, Portsmouth]] (1929–31)<br/>Director of [[Combined Operations Headquarters|Combined Operations]] (1940–41)
|battles= [[Boxer Rising]]<br/>[[First World War]]<br/>[[Second World War]]
|mawards= [[Knight Grand Cross in the Order of the Bath]]<br/>[[Knight Commander of the Royal Victorian Order]]<br/>[[Companion of the Order of St Michael and St George]]<br/>[[Distinguished Service Order]]
|relations= [[Charles Patton Keyes|Sir Charles Patton Keyes]] (father)<br>[[Geoffrey Keyes (VC)|Geoffrey Keyes]] (son)
|laterwork= [[Member of Parliament]] for [[Portsmouth North (UK Parliament constituency)|Portsmouth North]] (1934–43)
}}
[[Admiral of the Fleet (Royal Navy)|Admiral of the Fleet]] '''Roger John Brownlow Keyes, 1st Baron Keyes''', {{post-nominals|country=GBR|size=100%|sep=,|GCB|KCVO|CMG|DSO}} (4 October 1872 – 26&nbsp;December 1945) was a [[Royal Navy]] officer. As a junior officer he served in a [[corvette]] operating from [[Zanzibar]] on slavery suppression missions. Early in the [[Boxer Rebellion]], he led a mission to capture a flotilla of four Chinese destroyers moored to a wharf on the [[Hai River|Peiho River]]. He was one of the first men to climb over the [[Beijing|Peking]] walls, to break through to the besieged diplomatic legations and to free the legations.

During the First World War Keyes was heavily involved in the organisation of the [[Naval operations in the Dardanelles Campaign|Dardanelles Campaign]]. Keyes took charge in an operation when six trawlers and a cruiser attempted to clear the Kephez minefield. The operation was a failure, as the Turkish mobile artillery pieces bombarded Keyes' minesweeping squadron. He went on to be Director of Plans at the Admiralty and then took command of the [[Dover Patrol]]: he altered tactics and the Dover Patrol sank five U-Boats in the first month after implementation of Keyes' plan compared with just two in the previous two years. He also planned and led the famous [[Zeebrugge Raid|raids]] on the German submarine pens in the Belgian ports of [[Zeebrugge]] and [[Ostend]].

Between the wars Keyes commanded the [[Battlecruiser Squadron (United Kingdom)|Battlecruiser Squadron]], the [[Atlantic Fleet (United Kingdom)|Atlantic Fleet]] and then the [[Mediterranean Fleet]] before becoming [[Commander-in-Chief, Portsmouth]]. During the Second World War he initially became liaison officer to [[Leopold III of Belgium|Leopold III]], King of the Belgians. He went on to be the first Director of [[Combined Operations Headquarters|Combined Operations]] and implemented plans for the training of [[British Commandos|commandos]] and raids on hostile coasts.

==Early years==
Born the second son of General [[Charles Patton Keyes|Sir Charles Patton Keyes]] of the [[British Indian Army|Indian Army]] and Katherine Jessie Keyes (née Norman),<ref name=odnb>{{cite web|url=http://www.oxforddnb.com/view/article/34309?docPos=2|title=Sir Roger Keyes|publisher=Oxford Dictionary of National Biography|accessdate=3 October 2014}}</ref> Keyes told his parents from an early age: "I am going to be an Admiral".<ref>{{cite web|last=|first=|authorlink=|title = Admiral Sir Roger Keyes|work = Dover: Lock and Key of the Kingdom|publisher = www.dover-kent.co.uk|date = 2000–2006|url = http://www.dover-kent.co.uk/people/keyes.htm|doi =|accessdate =3 October 2014}}</ref> After being brought up in India and then the UK, where he attended [[Preparatory school (UK)|preparatory school]] at [[Margate]], he joined the Royal Navy as a [[cadet]] in the training ship {{HMS|Britannia|1860|6}} on 15 July 1885.<ref name=heath145>Heathcote, p. 145</ref> He was appointed to the [[cruiser]] {{HMS|Raleigh|1873|6}}, flagship of the [[Cape of Good Hope Station|Cape of Good Hope and West Africa Station]], in August 1887.<ref name=heath145/> Promoted to [[midshipman]] on 15 November 1887, he transferred to the [[corvette]] {{HMS|Turquoise}}, operating from [[Zanzibar]] on slavery suppression missions.<ref name=heath145/> Promoted to [[sub-lieutenant]] on 14 November 1891<ref>{{London Gazette |issue=26366|date=24 January 1893|startpage=412|supp=|accessdate=3 October 2014}}</ref> and to [[lieutenant]] on 28 August 1893,<ref>{{London Gazette |issue=26444|date=26 September 1893|startpage=5433|supp=|accessdate=3 October 2014}}</ref> he joined the [[sloop]] {{HMS|Beagle|1889|6}} on the [[Pacific Station]] later that year.<ref name=heath145/> After returning home in 1897 he became commanding officer of the [[destroyer]] {{HMS|Opossum|1895|6}} at [[Plymouth]] in January 1898.<ref name=heath145/>

==China==
[[File:Keyes HMS Fame 1900 AWM A05029.jpg|thumb|left|Lieutenant Keyes (sitting) with other officers aboard the destroyer [[HMS Fame (1896)|HMS ''Fame'']] in 1900]]
Keyes was then posted out to China to command another destroyer, {{HMS|Hart|1895|6}}, in September 1898 transferring to a newer ship, {{HMS|Fame|1896|6}} in January 1899. In April 1899 he went to the rescue of a small British force which was attacked and surrounded by irregular Chinese forces while attempting to demarcate the border of the Hong Kong [[New Territories]]. He went ashore, leading half the landing party, and, while HMS ''Fame'' fired on the besiegers, he led the charge which routed the Chinese and freed the troops.<ref>Keyes 1939, p. 165–173</ref>

In June 1900, early in the [[Boxer Rebellion]], Keyes led a mission to capture a flotilla of four Chinese destroyers moored to a wharf on the [[Hai River|Peiho River]]. Together with another junior officer, he took boarding parties onto the Chinese destroyers, captured the destroyers and secured the wharf.<ref name=odnb/> Shortly thereafter he led a mission to capture the heavily fortified fort at Hsi-cheng: he loaded HMS ''Fame'' with a landing party of 32 men, armed with rifles, pistols, cutlasses and explosives. His men quickly destroyed the Chinese gun mountings, blew up the powder magazine and returned to the ship.<ref>Keyes 1939, p. 243–258</ref>

Keyes was one of the first men to climb over the [[Beijing|Peking]] walls, to break through to the besieged diplomatic legations and to free the legations.  For this he was promoted to [[commander]] on 9 November 1900.<ref>{{London Gazette |issue=27245|date=9 November 1900|startpage=6855|supp=|accessdate=3 October 2014}}</ref> Keyes later recalled about the sack of Beijing:"Every Chinaman...was treated as a Boxer by the Russian and French troops, and the slaughter of men, women, and children in retaliation was revolting".<ref>Preston, p. 284</ref>

==Diplomatic service==
Keyes was in May 1901 appointed to the command of the destroyer {{HMS|Bat|1896|6}} serving in the [[HMNB Devonport|Devonport]] instructional flotilla. In January 1902 he was appointed in command of the destroyer {{HMS|Falcon|1899|6}}, which took ''Bat''´s crew and her place in the flotilla,<ref>{{Cite newspaper The Times |articlename=Naval & Military intelligence |day_of_week=Thursday |date=2 January 1902 |page_number=8 |issue=36654| }}</ref><ref>{{Cite newspaper The Times |articlename=Naval & Military intelligence |day_of_week=Thursday |date=16 January 1902 |page_number=7 |issue=36666| }}</ref> and four months later he again brought his crew and was appointed in command of the destroyer [[HMS Sprightly (1900)|HMS ''Sprightly'']], which served in the flotilla from May 1902.<ref>{{Cite newspaper The Times |articlename=Naval & Military intelligence|day_of_week=Wednesday |date=21 May 1902 |page_number=10 |issue=36773| }}</ref> He was posted to the intelligence section at the [[Admiralty]] in 1904 and then became naval [[attaché]] at the British Embassy in Rome in January 1905.<ref name=heath145/> Promoted to [[Captain (Royal Navy)|captain]] on 30 June 1905,<ref>{{London Gazette |issue=27812|date=30 June 1905|startpage=4557|supp=|accessdate=3 October 2014}}</ref> he was appointed a [[Royal Victorian Order|Member of the Royal Victorian Order]] on 24 April 1906.<ref name=mvo>{{London Gazette |issue=27911|date=8 May 1906|startpage=3164|supp=|accessdate=3 October 2014}}</ref> He took up command of the [[cruiser]] {{HMS|Venus|1895|6}} in the [[Atlantic Fleet (United Kingdom)|Atlantic Fleet]] in 1908 before going on to be Inspecting Captain of Submarines in 1910 and, having been appointed a [[Order of the Bath|Companion of the Order of the Bath]] on 19 June 1911,<ref name=cb>{{London Gazette |issue=28505|date=16 June 1911|startpage=4588|supp=yes|accessdate=3 October 2014}}</ref> he became commodore of the Submarine Service in 1912.<ref name=heath145/> As head of the Submarine Service, he introduced an element of competition into the construction of [[submarine]]s, which had previously been built by [[Vickers Limited|Vickers]] and tended to go to sea in a destroyer because of the primitive visibility from early submarines.<ref name=odnb/> He became a naval [[aide-de-camp]] to [[George V|the King]] on 15 September 1914.<ref>{{London Gazette |issue=28906|date=18 September 1914|startpage=7396|supp=|accessdate=3 October 2014}}</ref>

==First World War==
[[File:Sketch of 'Vice Admiral Sir Roger Keyes (1872–1945), KCB, CMG, CVO, DSO'.jpg|thumb|right|Sketch of Keyes by [[Glyn Warren Philpot]], 1918. [[Imperial War Museum]]]]
When the First World War broke out, Keyes took command of the Eighth Submarine Flotilla at [[Harwich Dockyard|Harwich]].<ref name=heath145/> He proposed, planned and took part in the first [[Battle of Heligoland Bight (1914)|Battle of Heligoland Bight]] in August 1914 flying his broad pendant in the destroyer {{HMS|Lurcher|1912|6}}.<ref name=heath145/> He went alongside the sinking German cruiser {{SMS|Mainz||6}} and picked up 220 survivors for which he was [[mentioned in dispatches]].<ref name=heath145/>

Keyes became Chief of Staff to Vice-Admiral [[Sackville Carden]], commander of the Royal Navy squadron off the Dardanelles, in February 1915 and was heavily involved in the organisation of the [[Naval operations in the Dardanelles Campaign|Dardanelles Campaign]].<ref name=heath145/> After slow progress, the bombardment of the Turkish defences was called off due to low ammunition stocks and fears of a newly laid Turkish minefield. Writing to his wife, Keyes expressed frustration at the lack of imagination of his new superior, Vice-Admiral [[John de Robeck]], arguing that "We must have a clear channel through the minefield for the ships to close to decisive range to hammer the forts and then land men to destroy the guns."<ref>Carlyon, p. 82</ref> Keyes took charge in an operation in March 1915 when six trawlers and the cruiser {{HMS|Amethyst|1903|6}} attempted to clear the Kephez minefield. The operation was a failure, as the Turkish mobile artillery pieces bombarded Keyes' minesweeping squadron. Heavy damage was inflicted on four of the six trawlers, while HMS ''Amethyst'' was badly hit and had her steering gear damaged. After another abortive attempt to clear the mines a few days later, the naval attempt to force the straits was abandoned and instead troops were landed to assault the guns.<ref>Carlyon, p. 83–84</ref> For his service during the Dardanelles Campaign, Keyes was appointed a [[Order of St Michael and St George|Companion of the Order of St Michael and St George]] on 1 January 1916<ref name=cmg>{{London Gazette |issue=29423|date=31 December 1915|startpage=83|supp=yes|accessdate=3 October 2014}}</ref> and awarded the [[Distinguished Service Order]] on 3 June 1916.<ref name=dso>{{London Gazette |issue=29608|date=2 June 1916|startpage=5563|supp=yes|accessdate=3 October 2014}}</ref>

Keyes took command of the [[battleship]] {{HMS|Centurion|1911|6}} in the [[Grand Fleet]] in June 1916 and, having been promoted to [[rear-admiral]] on 10 April 1917,<ref>{{London Gazette |issue=30017|date=13 April 1917|startpage=3496|supp=|accessdate=3 October 2014}}</ref> became second in command of the [[4th Battle Squadron (United Kingdom)|4th Battle Squadron]] with his flag in the battleship {{HMS|Colossus|1910|6}} in June 1917.<ref name=heath146>Heathcote, p. 146</ref> He went on to be Director of Plans at the Admiralty in October 1917 and then became [[Commander-in-Chief, Dover]] and commander of the [[Dover Patrol]] in January 1918.<ref name=heath146/> Prior to Keyes, the Dover Patrol had been commanded by Admiral [[Reginald Bacon]] and had succeeded in sinking two German U-Boats in the English Channel in the previous two years, but out of 88,000 crossings by ships only five had been torpedoed and one sunk by gunfire.<ref>Marder, p. 347</ref> After Keyes took control, he altered tactics, and the Dover Patrol sank five U-Boats in the first month after implementation of Keyes' plan.<ref>Halpern, p. 407</ref>

In April 1918 Keyes planned and led the famous [[Zeebrugge Raid|raids]] on the German submarine pens in the [[Belgium|Belgian]] ports of [[Zeebrugge]] and [[Ostend]].<ref name=heath146/> He was advanced to [[Royal Victorian Order|Commander of the Royal Victorian Order]] on 30 March 1918<ref name=cvo>{{London Gazette |issue=30613|date=5 April 1918|startpage=4132|supp=|accessdate=3 October 2014}}</ref> and to [[Order of the Bath|Knight Commander of the Order of the Bath]] on 24 April 1918.<ref name=kcb>{{London Gazette |issue=30655|date=26 April 1918|startpage=5064|supp=|accessdate=3 October 2014}}</ref> He was then advanced to [[Royal Victorian Order|Knight Commander of the Royal Victorian Order]] on 10 December 1918<ref name=kcvo>{{London Gazette |city=e|issue=13371|date=20 December 1918|startpage=4612|supp=|accessdate=3 October 2014}}</ref> and made a [[baronet]] on 29 December 1919.<ref name=baronet>{{London Gazette |issue=31708|date=30 December 1919 |startpage=15988|supp=|accessdate=3 October 2014}}</ref>

==Inter-war years==
[[File:HMS Royal Oak (08).jpg|thumb|left|The battleship, [[HMS Royal Oak (08)|HMS ''Royal Oak'']], scene of an incident which Keyes was thought by the Admiralty to have handled badly]]
Keyes was given command of the new [[Battlecruiser Squadron (United Kingdom)|Battlecruiser Squadron]] hoisting his flag at [[Scapa Flow]] in the [[battlecruiser]] {{HMS|Lion|1910|6}} in March 1919.<ref name=heath146/> He moved his flag to the new battlecruiser {{HMS|Hood|51|6}} in early 1920.<ref>{{cite web|url=http://www.hmshood.com/crew/biography/keyes_bio.htm|title=Biography of Admiral of the Fleet Sir Roger John Brownlow Keyes|publisher=HMS Hood Association|accessdate=3 October 2014}}</ref> Promoted to [[vice-admiral]] on 16 May 1921,<ref>{{London Gazette |issue=32329 |date=20 May 1921|startpage=4004|supp=|accessdate=3 October 2014}}</ref> he became [[List of senior officers of the Royal Navy#Deputy Chiefs of the Naval Staff|Deputy Chief of the Naval Staff]] in November 1921 and then Commander-in-Chief of the [[Mediterranean Fleet]] in June 1925 with promotion to full [[admiral]] on 1 March 1926.<ref>{{London Gazette |issue=33139|date=5 March 1926|startpage=1650|supp=|accessdate=3 October 2014}}</ref>

In January 1928 at dance on the quarterdeck of the battleship [[HMS Royal Oak (08)|HMS ''Royal Oak'']] Rear Admiral Bernard Collard, Second-in-command of the [[1st Battle Squadron (United Kingdom)|1st Battle Squadron]] openly lambasted Royal Marine Bandmaster, Percy Barnacle, and allegedly said "I won't have a bugger like that in my ship" in the presence of ship's officers and guests.<ref>Glenton, p. 28–34</ref> Captain [[Kenneth Dewar]] and Commander Henry Daniel accused Collard of "vindictive fault-finding" and openly humiliating and insulting them before their crew, referring to an incident involving Collard's disembarkation from the ship in March 1928 where the admiral had openly said that he was "fed up with the ship";<ref name="scotsman_28-04-03">{{cite journal |date=3 April 1928|title=Commander's Evidence |journal=The Scotsman }}</ref> Collard countercharged the two with failing to follow orders and treating him "worse than a midshipman".<ref>Glenton, p. 177–183</ref> Letters of complaint from Dewar and Daniel were passed on to Keyes. The press picked up on the story worldwide, describing the affair—with some hyperbole—as a "mutiny".<ref name="time280326">{{cite web|title = Royal Oak| work = Time |date = 26 March 1928 | url = http://www.time.com/time/magazine/article/0,9171,787012,00.html|accessdate=3 October 2014}}</ref> Keyes was thought by the Admiralty to have handled the matter badly and this may have adversely affected his chances of becoming [[First Sea Lord]].<ref name=heath147>Heathcote, p. 147</ref> He became [[Commander-in-Chief, Portsmouth]] in May 1929, was promoted to [[Admiral of the Fleet (Royal Navy)|Admiral of the Fleet]] on 8 May 1930<ref>{{London Gazette |issue=33604|date=9 May 1930|startpage=2867|supp=|accessdate=3 October 2014}}</ref> and was advanced to [[Order of the Bath|Knight Grand Cross of the Order of the Bath]] on 3 June 1930.<ref name=gcb>{{London Gazette |city=e|issue=14658|date=6 June 1930|startpage=645|supp=|accessdate=3 October 2014}}</ref> He then bought a house at [[Tingewick]] in [[Buckinghamshire]] and retired in May 1935.<ref>{{London Gazette |issue=34159 |date=10 May 1935|startpage=3048|supp=|accessdate=3 October 2014}}</ref>

Keyes was elected [[Conservative Party (UK)|Conservative]] [[Member of Parliament]] for [[Portsmouth North (UK Parliament constituency)|Portsmouth North]] in January 1934.<ref name=heath147/> In Parliament he fought disarmament and sought to have the [[Fleet Air Arm]] put back under the control of the navy.<ref name=heath147/> He was opposed to the [[Munich Agreement]] that [[Neville Chamberlain]] had reached with [[Adolf Hitler]] in 1938 and, along with [[Winston Churchill]] was one of the few who withheld support from the Government on this issue.<ref name=heath147/>

==Second World War==
[[File:Leopold III van België (1934).jpg|thumb|right|[[Leopold III of Belgium|King Leopold III of Belgium]] to whom Keyes was liaison officer]]
When the Second World War broke out, Keyes was very anxious to obtain active service, but at the same time criticised the Chiefs of Staff.<ref name=heath147/> He reached the conclusion that the regaining of [[Trondheim]] was the key to victory in Norway. He advocated the forcing of Trondheim Fjord by battleships and the landing of a military force to recapture the city. He sought an interview with Winston Churchill, then [[First Lord of the Admiralty]], submitted an outline plan to seize the city and offered to lead the expedition. If the Admiralty did not wish to hazard newer ships, he would take in old battleships. The chiefs of staff reached similar conclusions, with the addition of subsidiary landings north at [[Namsos Campaign|Namsos]] and south at [[Åndalsnes landings|Åndalsnes]]. However they failed to send [[capital ship]]s into Trondheimsfjord. German destroyers dominated the fjord, no airfields were seized to provide air cover and troops earmarked for the centre prong were never landed. When the troops were evacuated in early May 1940 there was shock in Britain. Parliament gathered for the [[Norway Debate]] on 7 and 8 May 1940. Making a dramatic entrance in the full uniform of an Admiral of the Fleet, including medals, Keyes defended the navy and strongly criticised the government.<ref>{{cite book |author=Harold Nicolson |author-link=Harold Nicolson |editor=Nigel Nicolson |editor-link=Nigel Nicolson |year=1967 |title=The Diaries and Letters of Harold Nicolson. Volume II: The War Years, 1939–1945 |location=New York |publisher=Atheneum |pages=76–77}}</ref> Chamberlain's government fell two days later and Winston Churchill became [[Prime Minister]].<ref name=heath147/>

When Germany invaded the [[Low Countries]] in May 1940, Churchill appointed Keyes liaison officer to [[Leopold III of Belgium|Leopold III]], King of the Belgians. But when Belgium surrendered suddenly to the Germans later that month both Leopold and Keyes were attacked in the British press.<ref name=heath147/>

Keyes became the first Director of [[Combined Operations Headquarters|Combined Operations]] in June 1940 and implemented plans for the training of [[British Commandos|commandos]] and raids on hostile coasts.<ref name=heath147/> He came up with bold schemes which were considered impractical by the Chiefs of Staff and he was removed from office in October 1941.<ref name=heath147/> He was elevated to the peerage as '''Baron Keyes''', of Zeebrugge and of Dover in the County of Kent on 22 January 1943.<ref>{{London Gazette |issue=35874 |date=22 January 1943 |startpage=445|supp=|accessdate=3 October 2014}}</ref>

Keyes suffered a detached [[retina]] in early 1944. He then undertook a goodwill tour of Canada, Australia and New Zealand at the request of the British Government in July 1944. During his visit to the [[amphibious warfare ship]] {{USS|Appalachian|AGC-1|6}} he suffered smoke inhalation following an attack by Japanese aircraft and never fully recovered. He died at his home in Tingewick on 26 December 1945 and was buried at the Zeebrugge corner of St James's Cemetery in [[Dover]].<ref name=heath147/>

==Family==
In 1906 Keyes married Eva Mary Bowlby: they had two sons and three daughters including [[Geoffrey Keyes (VC)|Geoffrey Keyes]], who was killed in action in 1941 and was posthumously awarded the [[Victoria Cross]].<ref>{{cite web|url=http://www.victoriacross.org.uk/vvashcr4.htm|title=Geoffrey Keyes|publisher=Lord Ashcroft VC Collection|accessdate=30 October 2014}}</ref>

==Honours and awards==
*[[Knight Grand Cross of the Order of the Bath]] – 3 June 1930<ref name=gcb/> (KCB – 24 April 1918,<ref name=kcb/> CB – 19 June 1911<ref name=cb/>)
*[[Knight Commander of the Royal Victorian Order]] – 10 December 1918<ref name=kcvo/> (CVO – 30 March 1918,<ref name=cvo/> MVO – 24 April 1906<ref name=mvo/>)
*[[Companion of the Order of St Michael and St George]] – 1 January 1916<ref name=cmg/>
*[[Companion of the Distinguished Service Order]] – 3 June 1916<ref name=dso/>
*[[Mention in Despatches]] – 14 March 1916<ref>{{London Gazette |issue=29507|date=14 March 1916|startpage=2869|supp=yes|accessdate=3 October 2014}}</ref>
*[[Legion d'Honneur|Commandeur of the Legion of Honour]] (France) – 5 April 1916<ref>{{London Gazette |issue=29538|date=7 April 1916|startpage=3691|supp=|accessdate=3 October 2014}}</ref>
*[[Navy Distinguished Service Medal]] (United States) – 16 September 1919<ref>{{London Gazette |issue=31553|date=12 September 1919|startpage=11583|supp=yes|accessdate=3 October 2014}}</ref>
*[[Order of Leopold (Belgium)|Grand Cross, Order of Leopold]] (Belgium) – 2 August 1921<ref>{{London Gazette |issue=32413|date=5 August 1921|startpage=6174|supp=|accessdate=3 October 2014}}</ref> (Grand Officer – 23 July 1918<ref>{{London Gazette |issue=30807|date=19 July 1918|startpage=8599|supp=yes|accessdate=3 October 2014}}</ref>)
*[[Croix de Guerre|Croix de Guerre 1914–1918]] (France) – 23 July 1918<ref>{{London Gazette |issue=30807 |date=19 July 1918|startpage=8599|supp=yes|accessdate=3 October 2014}}</ref>
*[[Order of the Iron Crown (Austria)|Order of the Iron Crown]], Second Class ([[Austria-Hungary]]) – 24 February 1908<ref>{{London Gazette |issue=28113 |date=25 February 1908|startpage=1315|supp=|accessdate=3 October 2014}}</ref>
*[[Order of the Medjidieh|Order of the Medjidieh, Second Class]] (Turkey) – 4 June 1908<ref>{{London Gazette |issue=28143|date=5 June 1908|startpage=4167|supp=|accessdate=3 October 2014}}</ref>
*[[Order of St. Maurice and St. Lazarus|Commander of the Order of St. Maurice and St. Lazarus]] (Italy) – 22 June 1908<ref>{{London Gazette |issue=28150|date=23 June 1908|startpage=4554|supp=|accessdate=3 October 2014}}</ref>
*[[Order of the Redeemer|Order of the Redeemer, Third Class]] (Greece) – 24 June 1909<ref>{{London Gazette |issue=28265|date=29 June 1909|startpage=4962|supp=|accessdate=3 October 2014}}</ref>

==References==
{{reflist|30em}}

==Sources==
* {{cite book|last=Carlyon|first=Les|title=Gallipoli|publisher=Bantam|year=2003|isbn=978-0553815061}}
* {{cite book|last=Glenton|first=Robert|title=The Royal Oak Affair: The Saga of Admiral Collard and Bandmaster Barnacle|publisher=Pen & Sword Books|year=1991|isbn=978-0850522662}}
* {{cite book|last=Halpern|first=Paul|title=A Naval History of World War I|publisher=Routleadge|year=1995|isbn=978-1857284980}}
* {{cite book|last=Heathcote |first=Tony |title=The British Admirals of the Fleet 1734 – 1995 |publisher=Pen & Sword Ltd |year=2002 |isbn=0-85052-835-6}}
* {{cite book|last=Keyes|first=Roger|title=Adventures Ashore and Afloat|publisher=London: George Harrap & Co.|year=1939}}
* {{cite book|last=Marder|first=Arthur Jacob|title=From the Dreadnought to Scapa Flow Volume III|publisher= London: Oxford University Press|year= 1969|isbn=978-1848322004}}
* {{cite book|last=Preston|first=Diana|title=The Boxer Rebellion: The Dramatic Story of China's War on Foreigners that Shook the World in the Summer of 1900|publisher= Berkley Books|year= 2000|asin=B00BUW73OS}}

==Further reading==
*{{cite book|last=Aspinall-Oglander|first=Cecil|title=Roger Keyes|publisher=London: The Hogarth Press|year=1951}}
*{{cite book|last=Halpern|first=Paul G. (ed.)|title=The Keyes Papers: Selections from the Private and Official Correspondence of Admiral of the Fleet Baron Keyes of Zeebrugge|publisher=Allen & Unwin|location=London}}
*#1914–1918 (1979), ISBN 0-04-942164-6
*#1919–1938 (1981), ISBN 0-04-942165-4
*#1939–1945 (1981), ISBN 0-04-942172-7
*{{cite book|last=Keyes|first=Roger|title=Naval Memoirs, 2 vols|publisher=London: Thornton Butterworth|year=1934}}
*{{cite book|last=Keyes|first=Roger|title=The Fight For Gallipoli|publisher=London: Eyre & Spottiswoode|year=1941}}
*{{cite book|last=Keyes|first=Roger|title=Amphibious Warfare and Combined Operations|publisher=Lees Knowles Lectures. Cambridge: University Press|year=1943}}
*{{cite book|last=St John-McAlister|first=Michael|title=The Keyes Papers at the British Library|publisher=Electronic British Library Journal}}

==External links==
{{Commons category|Roger Keyes, 1st Baron Keyes}}
* {{Hansard-contribs | admiral-sir-roger-keyes | Roger Keyes }}
* '''[http://www.europeana-collections-1914-1918.eu/ Europeana Collections 1914–1918]''' makes 425,000 First World War items from European libraries available online, including The Keyes Papers
*{{DP-xlink|http://dreadnoughtproject.org/tfs/index.php/Roger_John_Brownlow_Keyes,_First_Baron_Keyes}}

{{s-start}}
{{s-mil}}
{{s-bef | before=[[Reginald Bacon|Sir Reginald Bacon]]}}
{{s-ttl | title=[[Commander-in-Chief, Dover]] | years=1918&ndash;1919}}
{{s-aft | after=Post disbanded}}
|-
{{Succession box| title=[[Battlecruiser Squadron (United Kingdom)|Commander, Battlecruiser Squadron]] | before=New Post | after=[[Walter Cowan|Sir Walter Cowan]]| years=1919–1921}}
|-
{{succession box|title=[[Deputy Chief of the Naval Staff]]|before=[[Osmond Brock|Sir Osmond Brock]]|after=[[Frederick Field (Royal Navy officer)|Sir Frederick Field]]|years=1921–1925}}
|-
{{succession box | title=[[Mediterranean Fleet|Commander-in-Chief, Mediterranean Fleet]] | before=[[Osmond Brock|Sir Osmond Brock]] | after=[[Frederick Field (Royal Navy officer)|Sir Frederick Field]]| years=1925–1928}}
|-
{{succession box | title=[[Commander-in-Chief, Portsmouth]] | before=[[Osmond Brock|Sir Osmond Brock]] | after=[[Arthur Waistell|Sir Arthur Waistell]]| years=1929–1931}}
|-
{{s-par|uk}}
{{succession box  | title  = [[Portsmouth North (UK Parliament constituency)|Member of Parliament for Portsmouth North]]  | years  = 1934–1943  | before = [[Bertram Godfray]]  | after  = [[William Milbourne James|Sir William James]]}}
{{s-reg|uk-bt}}
{{s-new|creation}}
{{s-ttl|title=[[Keyes Baronets|Baronet]]'''<br>(of Dover)| years='''1919 – 1945}}
{{s-aft|after=[[Roger Keyes, 2nd Baron Keyes|Roger George Bowlby Keyes]]}}
{{s-reg|uk}}
{{s-new|creation}}
{{s-ttl|title=[[Baron Keyes]]| years=1943–1945}}
{{s-aft|after=[[Roger Keyes, 2nd Baron Keyes|Roger George Bowlby Keyes]]}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Keyes, Roger John Brownlow}}
[[Category:1872 births]]
[[Category:1945 deaths]]
[[Category:Knights Grand Cross of the Order of the Bath]]
[[Category:Knights Commander of the Royal Victorian Order]]
[[Category:Companions of the Order of St Michael and St George]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Grand Officers of the Order of Leopold II]]
[[Category:Commandeurs of the Légion d'honneur]]
[[Category:Foreign recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Order of the Medjidie, 2nd class]]
[[Category:Commanders of the Order of Saints Maurice and Lazarus]]
[[Category:Recipients of the Croix de guerre 1914–1918 (France)]]
[[Category:Baronets in the Baronetage of the United Kingdom|Keyes, Roger John Brownlow Keyes, 1st Baron]]
[[Category:Barons in the Peerage of the United Kingdom|Keyes, Roger John Brownlow Keyes, 1st Baron]]
[[Category:Royal Navy admirals of the fleet]]
[[Category:Royal Navy admirals of World War I]]
[[Category:Royal Navy admirals of World War II]]
[[Category:Lords of the Admiralty]]
[[Category:Members of the Parliament of the United Kingdom for English constituencies]]
[[Category:Conservative Party (UK) MPs]]
[[Category:UK MPs 1931–35]]
[[Category:UK MPs 1935–45]]
[[Category:Royal Navy personnel of the Boxer Rebellion|Keyes, Roger John Brownlow Keyes, 1st Baron]]