{|{{Infobox Aircraft Begin
 | name= Go 244
 | image= File:Go244-1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Military transport monoplane
 | national origin=Germany
 | manufacturer=[[Gothaer Waggonfabrik|Gotha]]
 | designer=
 | first flight=1940s
 | introduced=
 | retired=
 | status=
 | produced=174<ref>*{{cite book |last1=Chant |first1= Chris  |title=Aircraft of World War II  |year=1999  |publisher=Brown Books |location=London  |page=147  |isbn=189788446X}}</ref>
 | primary user=
 | number built=
 | developed from=
 | variants with their own articles=
 | developed into=
}}
|}

The '''Gotha Go 244''' was a transport [[aircraft]] used by the ''[[Luftwaffe]]'' during [[World War II]].

==Development==
The Go 244 was the powered version of the [[Gotha Go 242]] [[military glider]] transport. Studies for powered versions of the Go 242 began early in the design of the glider, with one early proposal being for modification to allow a single Argus As 10C engine to be temporarily attached to the nose of the glider to allow recovery back to base after use. This idea was rejected, but the alternative of a permanently powered twin-engined version was taken forward.<ref name="AI p291">''Air International'' December 1989, p. 291.</ref>

Three Go 242s were modified as prototypes of the powered Go 244, fitted with varying surplus radial engines.  The first prototype, the Go 244 V1 was powered by two 660&nbsp;hp (492&nbsp;kW) [[BMW 132]], while the second prototype had 700&nbsp;hp (522&nbsp;kW) [[Gnome-Rhône 14M]]s — and the third 750&nbsp;hp (560&nbsp;kW) [[Shvetsov M-25]] A engines, with this model of [[Aviadvigatel|Shvetsov]] [[OKB]] engine design being essentially a Soviet-built [[Wright R-1820|Wright ''Cyclone'']] American-based nine-cylinder radial.  Although only the third prototype offered adequate engine out performance, the Luftwaffe had large stocks of captured Gnome engines, so this was chosen as the basis for the production conversion — usually fitted in [[counter-rotating propellers|counter-rotating]] pairs in production — although a few more aircraft were fitted with the BMW and Shvetsov engines.<ref name="AI p291-2">''Air International'' December 1989, pp. 291–2.</ref><ref name="Smith&Kay p219">Smith and Kay 1972, p.219.</ref>

The B series was the main production model, being based on the Go 242B with a wheeled tricycle undercarriage and with fuel and oil carried in the tailbooms.<ref name="AI p292">''Air International'' December 1989, p. 292.</ref> 133 were converted from Go 242 Bs,<ref name="Ford">{{cite book |last1=Ford |first1=Roger |title=Germany's Secret Weapons of World War II |date=2013 |publisher=Amber Books |location=London |isbn=9781909160569}}</ref> while a further 41 were built from new before production reverted to the glider Go 242.<ref name="AI p309">''Air International'' December 1989, p. 309.</ref> Plans were also created for single-engined variants with a nose-mounted [[Argus Motoren|Argus A 10C]] or [[Junkers Jumo 211]].<ref name=eow>{{cite book | last = Bishop| first = Chris| authorlink = |author2=Roger Ford| title = The Encyclopedia of Weapons of World War II| publisher = Sterling| year = 2002| location = | isbn = 1-58663-762-2| page = 408}}</ref>

==Operational history==
The first examples of the Go 244 were delivered to operational units in [[Greece]], based in [[Crete]] in March 1942. Some were also assigned to transport ''Geschwader'' in [[North Africa Campaign|North Africa]] and the [[Eastern Front (World War II)|Eastern Front]] but on the former front they proved vulnerable to [[anti-aircraft]] fire and were withdrawn, being replaced by [[Junkers Ju 52]] or [[Messerschmitt Me 323]] aircraft.<ref name=eow/>

==Variants==
* '''Go 244 A-1''' - prototype, using the BMW 132 radial engine
* '''Go 244 B-1''' - production version, with fixed landing gear
* '''Go 244 B-2''' - B-1 with improved landing gear including a larger semi-retractable nose wheel
* '''Go 242 B-3''' - paratroop-carrying version of B-1 with double rear doors
* '''Go 244 B-4''' - paratroop-carrying version of B-2 with doors of B-3 and landing gear of B-2
* '''Go 244 B-5''' - training version with dual controls

==Specifications (Go 244 B-1)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=<!-- options: plane/copter --> plane
|jet or prop?=<!-- options: jet/prop/both/neither --> prop
|ref=Gotha's Twin-Boom Troopers<ref name="AI p287-8">''Air International'' December 1989, pp. 288.</ref>
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->
|crew=1 or 2 pilots
|capacity=up to 23 troops or freight
|length main= 15.80 m
|length alt= 51 ft 10 in
|span main= 24.50 m
|span alt= 80 ft 4½ in
|height main= 4.70 m
|height alt= 15 ft 5 in 
|area main= 64.4 m²
|area alt= 693 ft²
|airfoil=
|empty weight main= 5,100 kg
|empty weight alt= 11,243 lb
|loaded weight main=6,800 kg
|loaded weight alt=14,991 lb
|useful load main=
|useful load alt=
|max takeoff weight main= 7,162 kg
|max takeoff weight alt= 15,789 lb 
|more general=
|engine (prop)=[[Gnome-Rhône 14M]] 06/07
|type of prop=14-cylinder [[radial engine]]
|number of props=2
|power main=742 hp 
|power alt= 553 kW
|power original=
|power more=at 2,000 m (6,560 ft)
|max speed main= 290 km/h
|max speed alt= 157 knots, 180 mph
|max speed more=at 4,000 m (13,100 ft)
|cruise speed main= 270 km/h 
|cruise speed alt= 146 knots, 168 mph 
|cruise speed more= at 3,900 m (12,800 ft)
|never exceed speed main= 
|never exceed speed alt= 
|stall speed main= 
|stall speed alt= 
|range main= 410 km 
|range alt= 222 [[nautical mile|nmi]], 255 mi
|ceiling main= 8,350 m
|ceiling alt= 27,395 ft
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 
|power/mass alt= 
|more performance=*'''Climb to 1,000 m (3,300 ft):''' 3 min
|guns=3 × 7.92 mm (.312 in) [[MG 15 machine gun|MG 15]] or [[MG 81 machine gun|MG 81Z]] machine guns
|avionics=
}}

==See also==
{{aircontent|
|related=
*[[Gotha Go 242|Go 242]]
|similar aircraft=
*[[Fairchild C-82 Packet]]
|lists=
*[[List of World War II military aircraft of Germany]] 
*[[List of military aircraft of Germany]] 
|see also=
}}

==References==
{{Commons category}}
;Notes
{{reflist}}
;Bibliography
*{{cite magazine |last1= |first1= |last2= |first2= |date=December 1989 |title=Gotha's Twin-Boom  Troopers|magazine=[[Air International]] |publisher= |volume= 37 |issue=  6|pages=286–292, 309 |url= |doi= }}
*{{cite book |last1=Smith |first1= J R |last2=Kay |first2=Antony L  |title=German Aircraft of the Second World War  |year=1972  |publisher=Putnam |location=London  |isbn=0370000242}}

{{Gotha aircraft}}
{{RLM aircraft designations}}

[[Category:German military transport aircraft 1940–1949]]
[[Category:Gotha aircraft|Go 244]]