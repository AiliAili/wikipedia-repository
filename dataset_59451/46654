'''Benjamin B. Bourdon''' (1860–1943) was a French psychologist born in Normandy on August 5th, 1860. He is often referred to as a pioneer of [[experimental psychology]] in France.<ref name="Nicolas"> Nicolas, S., 1998 "Benjamin Bourdon (1860-1943): Fondateur du laboratoire de psychologie et de linguistique expérimentale a l`Université de Rennes (1896) Année Psychologique 98 271-293</ref> Bourdon founded the first university established Experimental Psychology and Linguistics laboratory at the [[University of Rennes]] in 1896 and integrated the first experimental psychology course in a provincial university in 1891.<ref name="Ono">Ono H, Lillakas L, Kapoor A, Wong I, 2013, ''Replicating and extending Bourdon`s (1902) experiment on motion parallax'' Perception, 2013, volume 42, pages 45-59</ref> His life is known by means of his (1932) autobiography in [[Carl Murchison]]`s compilation of autobiographies (1932)<ref name="Bourdon">Bourdon B, 1932 ''Benjamin Bourdon'', in A history of Psychology in Autobiography volume II, Ed. C Murchison (New York: Russell " Russell) pp 1-16</ref> and biographies by Nicolas, S. (1998),<ref name="Nicolas" /> Beuchet (1961),<ref name="Beuchet">Beuchet J, 1961 '' Un pionnier des sciences humaines, Benjamin Bourdon (1860-1943)'' Annales de Bretagnes 68 299-345</ref> and Piéron (1961).<ref name="Pieron">''Pieron H, 1961 ''Benjamin Bourdon comme je lài connu'' Psychologie Francaise 6 163-172''</ref>  The accounts of Bourdon`s life describe him as   one of the few French advocates of the new scientific psychology.<ref name="Ono" />

== Family and childhood ==
Bourdon was born in [[Normandy]], France on August 5, 1860. His village was composed of five or six educated men such as the priest, doctors and notaries. Bourdon spent his childhood and part of his adolescence in the class of farmers, sailors and quarry workers.  Bourdon’s father was a farmer and his mother was from humble origins, daughter of a farmer and described by Bourdon (1932) as being unassuming, superior in mental ability but lacking the ambition and favourable conditions to reach a higher social level.<ref name="Bourdon" />

At the age of twelve, he entered  the boarding school Lycée de Coutances, graduating with the ''bachelier es letter es sciences'' at nineteen. After hesitating between a teaching career and a career in law,  he decided to study law in Paris but lost interest in the subject after a year,  turning  to the preparation for a teaching career in philosophy at the [[Sorbonne]]. Among t his teachers were [[Elme-Marie Caro]] (1826-1887), Ludovic Carrau (1842-1889), and [[Paul Janet]] (1823-1899), the most eminent French representatives of philosophy at the time.<ref name="Nicolas"/>  Nevertheless, Bourdon gave little credit to the teaching of philosophy for his intellectual development, but ascribed his biggest influences came from reading Berkeley, Hume, the Mills, Bain, Spencer, James and [[Theodule Ribot]] (1839-1916), the latter was also Bourdon`s teacher.<ref name="Bourdon" />

In 1886, after having obtained the highest level of teaching diploma available in France, Bourdon received a scholarship to go to Germany for a year where he got the opportunity to learn from [[Wundt]] in Leipzig in his laboratory of experimental psychology. This experience gave Bourdon the ambition to bring France up to date with the progress and development of the study of psychology.<ref name="Bourdon" />

== Experimental psychology laboratory ==

In 1887, Bourdon returned to France and in the following year, he became professor at Rennes University. In 1891 he took charge of his first philosophy class where he integrated the first provincial university experimental psychology course where he tackled questions on perception, attention and consciousness. At the time in France, "experimental" was related to experiments on [[hypnotism]] because the universities were focused on scientific psychology studies based on pathologies and psychics.<ref name="Nicolas" />  Bourdon felt as though the centralization in France interfered with educational progress. All universities needed to prepare the students for an exam held in Paris, where the educational programs were created and imposed throughout the country. Bourdon felt as though philosophy students were insufficiently prepared in matters of science.<ref name="Bourdon" />

In 1892, Bourdon received his doctorate degree with a French thesis entitled L’expression des émotions et des Tendances dans le Language and a Latin thesis related to qualities of perception in Descartes.  In January 1932, Bourdon founded the first university laboratory of experimental psychology and linguistics in the University of Rennes. Bourdon explains in his autobiography that during the war, the laboratory served as a hospital where he re-educated speech to the wounded men who suffered from [[aphasia]]. Bourdon`s laboratory was vandalised and looted during the two World Wars (Beuchet, 1961). Nevertheless, his laboratory was restored in 1946 by his successor Albert Burlond and it still stands today.<ref name="Ono" />

== Research ==

Bourdon`s work was more focused on the areas of ideas, memory, and perception. He was very fond of manual training and built many experimental apparatuses himself.  His findings were published in 74 papers. ''La Perception Visuelle de l’Espace'' published in 1902, is said to be his most important work in which he discusses spatial perception and conducts new experiments such as studies on motion parallax.<ref name="Ono"/> His contribution to psychometrics was the test of attention and concentration that bears his names and is used today with modification (i.e. Bourdon–Vos test and [[Bourdon-Wiersma test]]). See Kamphuis (1962), Vos (1992), Wiersma (1932).<ref name="Ono" />

== References ==

{{Reflist}}

==External links==
* {{Internet Archive author |sname=Benjamin B. Bourdon}}

{{Authority control}}

{{DEFAULTSORT:Bourdon, Benjamin B.}}
[[Category:1860 births]]
[[Category:1943 deaths]]
[[Category:French psychologists]]
[[Category:Experimental psychologists]]
[[Category:University of Rennes faculty]]