{{Redirect|New York State Route 74|previous alignments of NY 74}}
{{Infobox road
|country=USA
|name=Route&nbsp;74
|marker_image={{Infobox road/shieldmain/USA|state=NY|type=NY|route=74}} {{Infobox road/shieldmain/USA|state=VT|type=VT|route=74}}
|map_notes=Map of Essex County in northeastern New York with NY&nbsp;74 highlighted in solid red and of Addison County in western Vermont with VT&nbsp;74 highlighted in dotted red
|map_alt=NY 74 and VT 74 both follow east–west alignments from Interstate 87 in Schroon, New York, to VT 30 in Cornwall, Vermont. The combined route passes through Ticonderoga, where NY 74 intersects NY 22, crosses Lake Champlain, and intersects VT 22A in Shoreham.
|maint=[[New York State Department of Transportation|NYSDOT]] and [[Vermont Agency of Transportation|VTrans]]
|length_mi=33.713
|length_notes=20.45 mi in New York<ref name="2014tdr" /><br>13.263 mi in Vermont<ref name="VT Route Log" />
|history=NY&nbsp;74 designated NY&nbsp;73 in 1930;<ref name=renum /> renumbered to NY&nbsp;74 {{circa|1973}}<ref name="1972map" /><ref name="1973map" /><br />VT&nbsp;74 assigned mid-1950s<ref name="1954map" /><ref name="1956map" />
|direction_a=West
|terminus_a={{jct|state=NY|I|87}} in [[Schroon, New York|Schroon, NY]]
|junction={{jct|state=NY|NY|9N|NY|22}} in [[Ticonderoga, New York|Ticonderoga, NY]]<br>{{jct|state=VT|VT|22A}} in [[Shoreham, Vermont|Shoreham, VT]]
|direction_b=East
|terminus_b={{jct|state=VT|VT|30}} in [[Cornwall, Vermont|Cornwall, VT]]
|counties=[[Essex County, New York|Essex (NY)]], [[Addison County, Vermont|Addison (VT)]]
|browse={{infobox road/browselinks/USA|state=NY}}{{infobox road/browselinks/USA|state=VT}}{{ny browse|previous_route=73|previous_type=NY|next_type=NY|next_route=75|route=NY}}{{vt browse|previous_route=73A|previous_type=VT|next_type=VT|next_route=78|route=VT}}{{vt browse|previous_type=VT|previous_route=F-8|route=VT F-9, VT F-9A|next_type=VT|next_route=F-10}}
}}
'''New York State Route&nbsp;74''' ('''NY&nbsp;74''') and '''Vermont Route&nbsp;74''' ('''VT&nbsp;74''') are adjoining state highways in the northeastern United States, connected by one of the last remaining [[Cable ferry|cable ferries]] in North America.  Together they extend for {{convert|34|mi|0}} through [[Essex County, New York]], and [[Addison County, Vermont]]. NY&nbsp;74 begins at exit&nbsp;28 off [[Interstate&nbsp;87]] (I-87) in [[Schroon, New York|the hamlet of Severance]] in the [[Adirondack Mountains]] region of the northern part of [[New York State]]. It extends {{convert|20.44|mi|2}} to the western shore of [[Lake Champlain]] in [[Ticonderoga, New York|Ticonderoga]]. There, the seasonal Fort Ticonderoga–Larrabees Point Ferry carries cars across the state border into [[Vermont]], where VT&nbsp;74 starts at the lake's eastern shore and terminates {{convert|13.26|mi|2}} later at a junction with [[Vermont Route 30|VT&nbsp;30]] in the town of [[Cornwall, Vermont|Cornwall]].

NY&nbsp;74 is a descendant of the historic Ticonderoga and Schroon Turnpike, which was a privately owned highway chartered in 1832, and segments of NY&nbsp;74 follow the alignment of the original 19th-century turnpike. The connecting ferry route predates both NY&nbsp;74 and VT&nbsp;74 and began operation in 1759 on an informal basis. The ferry operation formalized at the close of the 18th century and upgraded to a cable system in 1946.

Due to extensive changes in designations in both states during the 20th century, the entire length of the present highway consists of renamed segments from other highways. The New York portion of the cross-state Route&nbsp;74 west of Ticonderoga was designated as part of [[New York State Route 73|NY&nbsp;73]] in the [[1930 state highway renumbering (New York)|1930 renumbering of state highways in New York]], while the Vermont section carried several different designations from the 1920s to the late 1930s, when it became solely part of '''Vermont Route&nbsp;F-9'''. NY&nbsp;73 was extended east to Lake Champlain in the 1950s—replacing '''New York State Route&nbsp;347'''—and VT&nbsp;F-9 was split into [[Vermont Route 73|VT&nbsp;73]] and VT&nbsp;74 shortly afterward. The Schroon–Ticonderoga highway was redesignated as NY&nbsp;74 {{circa|lk=no|1973}} after NY&nbsp;73 was cut back to its current eastern terminus in [[Elizabethtown, New York|Elizabethtown]].

==Route description==

===NY 74===
NY&nbsp;74 originates at exit&nbsp;28 of the [[Adirondack Northway]] (I-87) in the town of [[Schroon Lake, New York|Schroon]]. The starting interchange is located near local landmark Severance Hill, which reaches an altitude of {{convert|1600|ft|m}}. NY&nbsp;74 intersects with the north–south [[U.S. Route 9 in New York|U.S. Route&nbsp;9]] (US&nbsp;9) shortly after the northbound ramps of the Adirondack Northway. It then meets Stowell Road just before crossing the [[Schroon River]]. The highway meets a few local road intersections just south of Goosebury Hill before encountering Paradox Lake.  NY&nbsp;74 runs mostly parallel to the lake and intersects with a local campground entrance road as it continues eastward from Schroon.<ref name="google">{{Google maps |url=https://maps.google.com/maps?hl=en&rlz=1T4ADBF_enUS232US232&q=from:+NYS+Route+74/RT-74+%4043.871460,+-73.755100+to:+RT-74+%4043.854022,+-73.385637&um=1&ie=UTF-8&sa=N&tab=wl |title= Overview Map of NY&nbsp;74 |accessdate=January 30, 2008}}</ref>

[[File:END! NY 74 West.jpg|left|thumb|The western terminus of NY&nbsp;74 at I-87. The end sign, however, is located at US&nbsp;9.|alt=Ground-level view of a road and an associated road sign. More roadsigns and an overhead bridge are visible in the distance.]]
Cotters Pond is a small landmark located beyond a few mountains and hills on the southern side of NY&nbsp;74 near the end of Paradox Lake. Cotters Pond, by state law, is a water body that cannot be used for baitfishing. This was designated by the [[New York State Department of Environmental Conservation]].<ref name="cotters">{{cite web|url=http://www.dec.ny.gov/outdoor/31443.html|title=Special Fishing Regulations for Essex County|year=2009|publisher=[[New York State Department of Environmental Conservation]]|accessdate=January 2, 2009}}</ref> Shortly afterward, NY&nbsp;74 passes Bumbo Pond and enters the hamlet of Paradox. After leaving Paradox, NY&nbsp;74 turns to the southeast along the base of Skiff Mountain. The highway then approaches a creek that flows into Eagle Lake. NY&nbsp;74 crosses over [[Ticonderoga, New York|Eagle Lake]] and runs along the base of Eagle Cliff and the shores of the lake. After leaving Eagle Lake, the highway intersects with [[County Route 2 (Essex County, New York)|County Route&nbsp;2]] (CR&nbsp;2), the first numbered highway that NY&nbsp;74 encounters after US&nbsp;9.<ref name="google"/>

This stretch of NY&nbsp;74 skirts the base of Keeney Mountain, which peaks at {{convert|1400|ft|m}}. NY&nbsp;74 then intersects with [[County Route 56 (Essex County, New York)|CR&nbsp;56]], which parallels the main route to the south. CR&nbsp;56 merges back with NY&nbsp;74, which leaves the mountainous region for the hamlet of Ticonderoga. NY&nbsp;74 intersects with [[New York State Route 9N|NY&nbsp;9N]] and NY&nbsp;22 in the hamlet; the latter of the two highways becomes concurrent with NY&nbsp;74. NY&nbsp;22 and NY&nbsp;74 continue to the east, heading around the outskirts of Ticonderoga. NY&nbsp;74 intersects with [[County Route 49 (Essex County, New York)|CR&nbsp;49]] before NY&nbsp;74 turns to the southeast. NY&nbsp;22 and NY&nbsp;74 head southward toward the center of Ticonderoga. At the intersection with Montcalm Street, NY&nbsp;74 turns eastward off NY&nbsp;22, which heads southward for [[Washington County, New York|Washington County]].<ref name="google"/>
[[File:NY 74 looking east toward Vermont.jpg|thumb|View towards Vermont as NY&nbsp;74 descends to Ticonderoga|alt=A road curving to the left with a car on it and a sign warning of a steep grade. It is descending toward a lower area with some buildings and another stretch of road. In the distance is a line of mountains.]]
NY&nbsp;74 crosses local roads as it continues eastward toward Lake Champlain. The highway passes the entrance to [[Fort Ticonderoga]] and the [[Ticonderoga (Amtrak station)|Ticonderoga Amtrak station]]. The New York portion of NY&nbsp;74 terminates at a ferry landing by Lake Champlain at the state border.<ref name="google"/>

===Fort Ticonderoga–Larrabees Point Ferry===
The Fort Ticonderoga–Larrabees Point Ferry is the oldest and southernmost ferry on Lake Champlain.<ref name="Ferry">{{cite web|url=http://www.middlebury.net/tiferry/|title=Ticonderoga Ferry|first=James A. |last=Peden|year=2008|accessdate=August 27, 2008|publisher=Middlebury Networks}}</ref>  Its cable system consists of two {{convert|1.1|in|cm|adj=on}} steel cables in parallel alignment.<ref name="ferrysite" />  The current ferry barge, in operation since 1959, is powered by a sixteen-ton tugboat built in 1979 that can hold up to 18 cars.<ref name="Ferry" />  The seasonal ferry is half a mile long and operates from May through October.<ref name="ferrysite" /><ref name="googleferry">{{google maps |url=https://maps.google.com/maps?sourceid=navclient&ie=UTF-8&rlz=1T4ADBF_enUS232US232&q=from%3A%20Fort%20Ticonderoga-Larrabees%20Point%20Fry%20%4043.854016%2C%20-73.385363%20to%3A%20Fort%20Ticonderoga-Larrabees%20Pt%20Fry%20%4043.855254%2C%20-73.376423&um=1&sa=N&tab=wl |title=Fort Ticonderoga–Larrabees Point Fry @43.854016, -73.385363 |accessdate=August 27, 2008 |link= no }}</ref> The seven-minute passage operates during daylight hours.<ref name="Ferry"/>

===VT 74===
After crossing the state line via the Fort Ticonderoga–Larrabees Point Ferry, VT&nbsp;74 begins its track into Vermont. The highway heads a short distance to the north, passing a thinly populated area in Shoreham, as intermittent forest yields to fields and farmlands. After {{convert|0.49|mi|2}} from the border, VT&nbsp;74 intersects with [[Vermont Route 73|VT&nbsp;73]] before encountering Barnum Hill Road.  Near the Barnum Hill intersection, VT&nbsp;74 passes developed areas and bends more toward the north. VT&nbsp;74 then turns to the northeast at Smith Street and enters a patch of forest. Afterward VT&nbsp;74 climbs a hill and intersects with Harrington Hill Road where it turns northward once again.<ref name="googlevt">{{Google maps |url=https://maps.google.com/maps?sourceid=navclient&ie=UTF-8&rlz=1T4ADBF_enUS232US232&q=from%3A%20Fort%20Ticonderoga-Larrabees%20Pt%20Fry%20%4043.854510%2C%20-73.381900%20to%3A%20VT-74%20%4043.887550%2C%20-73.322276%20to%3AVT-74%20%4043.960808%2C%20-73.209842&um=1&sa=N&tab=wl |title=overview map of VT 74 |accessdate=August 26, 2008 |link= no}}</ref>

[[File:West 74 at 22A concurrency southern end.jpg|left|thumb|Signs at the southern end of VT&nbsp;74's concurrency with VT&nbsp;22A|alt=Ground-level view of a road with two green and white roadsigns. A church is visible on the left.]]
Fields and forests surround this thinly populated stretch of highway.  At the intersection with Blue Harbor Road, VT&nbsp;74 turns to the east and heads toward downtown Shoreham. Within central Shoreham, VT&nbsp;74 is known as Main Street and has a short concurrency with VT&nbsp;22A.  As VT&nbsp;74 leaves the densely populated portion of Shoreham, the concurrency ends and VT&nbsp;74 takes an eastward turn toward Cornwall.<ref name="googlevt"/>

As VT&nbsp;74 returns to the rural countryside it bends toward the northeast for most of the distance to Cornwall. Through this stretch the highway winds through forests and occasional farmland. VT&nbsp;74 straightens at an intersection with Bates Road. A connector road called North Palmer Road merges with VT&nbsp;74 shortly afterward. VT&nbsp;74 winds again for a stretch and straightens a second time near the intersection with Elmendorf Road as it continues toward Cornwall, intersecting with several township highways and approaching increasingly residential areas. At Clark Road in Cornwall, VT&nbsp;74 turns to the northeast once again, passing through more forests before terminating at [[Vermont Route 30|VT&nbsp;30]] in Cornwall.<ref name="googlevt"/>

==Early history==
{{wide image|Ticonderoga and Schroon Turnpike.jpg|700px|align-cap=center|The Ticonderoga and Schroon Turnpike and surrounding region, detail from an 1839 map of New York State|alt=The Ticonderoga and Schroon Turnpike began north of Schroon and headed east through Paradox to the western edge of Lake Champlain at Ticonderoga. The turnpike connected to other long-distance roads at each end.}}

===Schroon and its early highways===
Settlers of European descent began to populate the region near modern Schroon around 1797.  The area's first municipality was the town of [[Crown Point, New York|Crown Point]], which originally included considerable portions of thinly populated land that later developed into separate townships.  The first of these divisions occurred on March&nbsp;20, 1804 with the establishment of the town of Schroon.  Minerva split from Crown Point in March 1817, before another municipal reorganization in 1840 implemented further reductions to the land area of Crown Point.  During this period two thoroughfares served the area that correspond to parts of modern NY&nbsp;74 and US&nbsp;9.  One of these old state roads traversed the route covered by the current alignment of NY&nbsp;74 from Schroon to Ticonderoga.<ref name="book">{{cite book|title=The History of Essex County|first=H.P. |last=Smith|publisher=D. Mason and Company|year=1885}}</ref>

===Ticonderoga and Schroon Turnpike===
The entire length of NY&nbsp;74 from Ticonderoga to Schroon covers the same route as the 19th century Ticonderoga and Schroon Turnpike. The turnpike, chartered in April 1832, was built to a stretch of highway from the two towns.<ref name="turnpike incoporation">{{cite book|url=https://books.google.com/books?id=c-VKAAAAMAAJ&pg=PA312&dq=Ticonderoga+and+Schroon+Turnpike&ei=W7e0SPHvGJWKyQTozNG0Dw|title=Journal of the Senate of the State of New-York, at their Fifty-Fifth session|author=New York State Senate|publisher=E. Croswell|year=1832|accessdate=August 27, 2008 |page=312}}</ref> Two toll gates were erected specifically for use on the highway, and the charter allowed for additional toll gates at a spacing of approximately one for every ten miles of completed highway. The turnpike management raised funds by selling 600 shares valued at [[USD|$]]25 (equivalent to ${{formatnum:{{inflation|US|25|1832|r=0}}}} in {{CURRENTYEAR}}) each. $20,000 (equivalent to ${{formatnum:{{inflation|US|20000|1832|r=0}}}} in {{CURRENTYEAR}}) was also set aside for properties along the highway.<ref>{{cite book|url=https://books.google.com/books?id=o1k4AAAAIAAJ&pg=PA520&dq=ticonderoga+and+schroon+turnpike&lr=&ei=CXyxSKjJN43IywThv9CFBw#PPA520,M1v|title=Laws of the State of New York Passed at the Sessions of the Legislature|author=New York State Legislature |pages=520–521|year=1832|publisher=Legislative Bill Drafting Commission|accessdate=August 24, 2008}}</ref>

==Designation history==

===Designations from 1913 to the 1930s===
[[File:NY 74 over truss bridge.jpg|thumb|A truss bridge on NY&nbsp;74 in [[Essex County, New York|Essex County]]|alt=A relatively small steel bridge seen from the side.]]
In 1913, the [[New York State Legislature]] designated most of modern NY&nbsp;74 as Route&nbsp;22-b, an unsigned [[legislative route]]. It ran for {{convert|17.06|mi|2}} from Route&nbsp;22 (now [[U.S. Route 9 in New York|US&nbsp;9]]) in [[Schroon, New York|Schroon]] to the western edge of the then-[[Ticonderoga (hamlet), New York|village of Ticonderoga]].<ref name="1918book">{{cite book |url=https://books.google.com/books?id=ffqwAAAAIAAJ&pg=RA1-PA72 |author=New York State Legislature |title=Laws of the State of New York passed at the One Hundred and Forty-First Session of the Legislature |chapter=Tables of Laws and Codes Amended or Repealed |year=1918 |publisher=J. B. Lyon Company |location=[[Albany, New York]] |page=72 |accessdate=July 31, 2009}}</ref><ref name="1920book">{{cite book |url=https://books.google.com/books?id=Sj4CAAAAYAAJ&pg=PA533 |author=New York State Department of Highways |title=Report of the State Commissioner of Highways |year=1920 |publisher=J. B. Lyon Company |location=Albany, New York |page=533 |accessdate=July 31, 2009}}</ref> On March&nbsp;1, 1921, Route&nbsp;22-b became part of Route&nbsp;48, a new route created as part of a partial renumbering of New York's legislative route system.<ref name="1921book">{{cite book |url=https://books.google.com/books?id=6pE4AAAAIAAJ&pg=PA42 |author=New York State Legislature |title=Laws of the State of New York passed at the One Hundred and Forty-Fourth Session of the Legislature |chapter=Tables of Laws and Codes Amended or Repealed |year=1921 |publisher=J. B. Lyon Company |location=Albany, New York |pages=42, 60, 71–72 |accessdate=April 30, 2010}}</ref> In 1924, when state highways were first publicly signed with route numbers, the highway from Schroon to Ticonderoga remained unnumbered.<ref name=1924nyt>{{cite news |title=New York's Main Highways Designated by Numbers |newspaper=The New York Times |date=December 21, 1924 |page=XX9}}</ref> During the [[1930 state highway renumbering (New York)|1930 renumbering of state highways in New York]], the segment of modern NY&nbsp;74 between [[U.S. Route 9 in New York|US&nbsp;9]] in [[Schroon, New York|Schroon]] and NY&nbsp;22 in [[Ticonderoga (hamlet), New York|Ticonderoga]] was designated as part of [[New York State Route 73|NY&nbsp;73]],<ref name=renum>{{cite news|first=Leon A. |last=Dickinson |title=New Signs for State Highways |newspaper=[[The New York Times]] |date=January 12, 1930 |page=136}}</ref> a route extending from [[New York State Route 28N|NY&nbsp;28N]] in [[Tahawus, New York|Tahawus]] to Ticonderoga. NY&nbsp;73 followed the [[Blue Ridge Road]] from Tahawus to [[North Hudson, New York|North Hudson]] and had an [[overlap (road)|overlap]] with US&nbsp;9 from North Hudson to Schroon.<ref>{{cite map |title=Road Map of New York |year=1930 |publisher=[[Standard Oil Company of New York]] |cartography=General Drafting}}</ref>

The portion of NY&nbsp;73 between Tahawus and [[North Hudson, New York|North Hudson]] was removed from the state highway system {{circa|lk=no|1936}}. As a result, NY&nbsp;73 was truncated to a new western terminus at US&nbsp;9 in Schroon, eliminating the concurrency with US&nbsp;9.<ref name=1935map>{{cite map |title=Road Map & Historical Guide: New York |publisher=[[Sun Oil Company]] |year=1935 |cartography=Rand McNally and Company}}</ref><ref name="1936map">{{cite map|title=New York State Map|publisher=Esso|cartography=General Drafting Inc.|year=1936}}</ref> The Blue Ridge Road is now designated as [[County Route 84 (Essex County, New York)|CR&nbsp;84]] from NY&nbsp;28N in Tawahus to the [[Adirondack Northway]] in North Hudson,<ref name="googleCR84">{{Google maps |url=https://maps.google.com/maps?sourceid=navclient&ie=UTF-8&rlz=1T4ADBF_enUS232US232&q=from%3A%20Blue%20Ridge%20Rd%2FCR-84%20%4043.948310%2C%20-74.068730%20to%3A%20Blue%20Ridge%20Rd%2FCR-84%20%4043.952648%2C%20-73.728458&um=1&sa=N&tab=wl |title= Overview Map of Essex CR&nbsp;84 |accessdate=August 26, 2008}}</ref> and as NY&nbsp;910K, a short unsigned [[reference route (New York)|reference route]], from the Northway to US&nbsp;9.<ref>{{cite book |url=https://www.dot.ny.gov/divisions/operating/oom/transportation-systems/repository/2012%20tour-bk.pdf |title=Official Description of Highway Touring Routes, Bicycling Touring Routes, Scenic Byways, & Commemorative/Memorial Designations in New York State |author=New York State Department of Transportation |date=January 2012 |format=PDF |accessdate=February 1, 2012}}</ref>

On the Vermont side, the road connecting Larrabees Point to the main north–south highways in the area was designated as VT&nbsp;F-9 by 1926. VT&nbsp;F-9 began at the ferry landing at Larrabees Point and continued northeast to Shoreham Center, where it briefly overlapped with then-VT&nbsp;30A (modern [[Vermont Route 22A|VT&nbsp;22A]]). Past VT&nbsp;30A, VT&nbsp;F-9 continued east along what are now town highways through [[Whiting, Vermont|Whiting]] to [[Leicester, Vermont|Leicester]], where it ended at a junction with [[U.S. Route 7 (Vermont)|US&nbsp;7]]. At the time, modern VT&nbsp;74 between Shoreham and Cornwall centers was known as VT&nbsp;F-9A while what is now [[Vermont Route 73|VT&nbsp;73]] from Larrabees Point to Brown Lane north of the town center of [[Orwell, Vermont|Orwell]] was part of [[Vermont Route F-10A|VT&nbsp;F-10A]].<ref>{{cite book |title=Report of a Survey of Transportation on the State Highways of Vermont |author1= [[Bureau of Public Roads]] |author2=[[Vermont State Highway Department]] |year=1927 |pages=68, 83–84 |url=http://www.aot.state.vt.us/planning/documents/mapping/publications/1927Doc_full.pdf |format=PDF |accessdate=April 30, 2010}}</ref>

===Changes from 1933 onward===
By 1933, the highway linking NY&nbsp;22 in Ticonderoga to the ferry for Larrabees Point became part of [[New York State Route 8|NY&nbsp;8]].<ref name="1933map">{{cite map |title=Texaco Road Map: New England |publisher=[[Texas Oil Company]] |year=1932 |cartography=Rand McNally and Company}}</ref> NY&nbsp;8 was realigned {{circa|lk=no|1934}} to follow [[New York State Route 22|NY&nbsp;22]] north from Ticonderoga to [[Crown Point, New York|Crown Point]], where it left NY&nbsp;22 to follow [[New York State Route 347 (1930–1934)|NY&nbsp;347]] (modern [[New York State Route 185|NY&nbsp;185]]) to the [[Champlain Bridge (United States)|Champlain Bridge]]. The NY&nbsp;347 designation was reassigned to NY&nbsp;8's former routing between NY&nbsp;22 and the ferry landing east of Ticonderoga.<ref name="1933map" /><ref>{{cite map |title=Road Map of New York |publisher=Texas Oil Company |year=1934 |cartography=Rand McNally and Company}}</ref>

[[File:Vermont 74 after the ferry landing.jpg|thumb|left|VT&nbsp;74 departing from the Larabees Point ferry landing|alt=Ground-level view of a road with a series of signs visible on the right side.]]
VT&nbsp;F-9, meanwhile, was extended southward to Orwell over VT&nbsp;F-10A by 1938. Like VT&nbsp;F-10A, it initially bypassed Orwell to the north on Brown Lane;<ref name="1938gb">{{cite book |last=Thibodeau |first=William A. |title=The ALA Green Book |edition=1938–39 |year=1938 |publisher=Automobile Legal Association}}</ref> however, it was realigned in the late 1930s to follow modern VT&nbsp;73 into Orwell. Around the same time, VT&nbsp;F-9 was realigned east of Shoreham to follow VT&nbsp;F-9A northeast to Cornwall. The VT&nbsp;F-9A designation was eliminated while the former routing of VT&nbsp;F-9 between Shoreham and Leicester became unnumbered.<ref name="1938gb" /><ref>{{cite map |title=New York Info-Map |publisher=[[Gulf Oil Company]] |year=1940 |cartography=Rand McNally and Company}}</ref> In the early 1950s, New York extended NY&nbsp;73 east to the ferry landing by way of NY&nbsp;347 and Wicker and Montcalm Streets.<ref name="1954map">{{cite map |title=New York with Special Maps of Putnam–Rockland–Westchester Counties and Finger Lakes Region |publisher=[[Esso]] |cartography=[[General Drafting]] |edition=1955–56 |year=1954}}</ref><ref>{{cite map |title=New York |publisher=[[Sunoco]] |cartography=Rand McNally and Company |year=1952}}</ref> Vermont renumbered the Larrabees Point–Orwell section of VT&nbsp;F-9 to VT&nbsp;73 in the mid-1950s in order to match the New York route number. At the same time, the Larrabees Point–Cornwall section of VT&nbsp;F-9 was renumbered by Vermont to VT&nbsp;74.<ref name="1954map" /><ref name="1956map">{{cite map |title=New York with Special Maps of Putnam–Rockland–Westchester Counties and Finger Lakes Region |publisher=Esso |cartography=General Drafting |edition=1957 |year=1956}}</ref>

NY&nbsp;73 was truncated to its current eastern terminus in [[Underwood, New York|Underwood]] {{circa|lk=no|1973}}, eliminating a lengthy [[overlap (road)|overlap]] with US&nbsp;9, while its former routing from Schroon to Lake Champlain was renumbered to NY&nbsp;74, matching the other Vermont route number that ends in Larrabees Point. The [[New York State Route 74 (1930–1973)|already existing NY&nbsp;74]] in Chautauqua County (in the [[Jamestown, New York|Jamestown]] area) was renumbered to [[New York State Route 474|NY&nbsp;474]] as a result.<ref name="1972map">{{cite map |title=New York and New Jersey Tourgide<!--sic--> Map |publisher=[[Gulf Oil Company]] |cartography=[[Rand McNally and Company]] |year=1972 |edition=1972}}</ref><ref name="1973map">{{cite map |title=New York |publisher=[[Shell Oil Company]] |cartography=[[H.M. Gousha Company]] |year=1973 |edition=1973}}</ref> The opening of the [[Adirondack Northway]] in 1967 also resulted in a slight shift of the western terminus of NY&nbsp;74 from US&nbsp;9 to I-87.<ref name="adrdknrthwy">{{cite news |url=http://www.adirondacklife.com/index.php?Itemid=173&id=96&option=com_content&task=view |title=Presenting the winner of Adirondack Life's profile–writing contest 2007 |first=Ann Breen |last=Metcalfe |newspaper=Adirondack Life |year=2008 |accessdate=October 5, 2008}}</ref> By 1981, NY&nbsp;74 was rerouted to follow its current alignment around the northeastern edge of Ticonderoga, on a concurrency with NY&nbsp;22.<ref>{{cite map |title=[[I Love New York]] Tourism Map |publisher=[[State of New York]] |cartography=Rand McNally and Company |year=1981}}</ref> The highway has remained the same since that date.<ref name="ticonderoga quad">{{cite map |url=http://gis.ny.gov/gisdata/quads/drg24/dotpreview/index.cfm?code=j51 |title=Ticonderoga Digital Raster Quadrangle |publisher=New York State Department of Transportation |year=1992 |scale=1:24,000 |accessdate=January 20, 2010}}</ref>

The New York State Department of Transportation has announced a project to repave NY&nbsp;74 from Chilson to Paradox.  The project is in preliminary development, with construction is expected to begin in early 2013 and reach completion about a year later.  Funding will come from state sources and run an estimated cost of $8.6 million.<ref name="future">{{cite web|url=https://www.dot.ny.gov/portal/pls/portal/MEXIS_APP.WEPIDYNPAGEMULTI.show?p_arg_names=p_pin&p_arg_values=111000|title=Route 74 from Chilson to Paradox|year=2008|publisher=New York State Department of Transportation|accessdate=December 20, 2008}}</ref>

==Ferry history==
According to its website, the ferry operated informally from 1759 and in an organized way from 1799.<ref name="Ferry" /> The earliest ferries are believed to have been [[rowboat]]s or [[canoe]]s; "a double-ended sailing scow was in service by 1800.  This vessel was about {{convert|30|ft|m}} long, with a mainsail that would swing completely around the mast to provide a simple means of reversing course."<ref name="ferrysite">{{cite web|url=http://www.ticonderogany.com/members/fortticonderogaferry.html|title=Fort Ticonderoga Ferry|author=Ticonderoga Area Chamber of Commerce|year=2008|accessdate=August 27, 2008|publisher=Ticonderoga Area Chamber of Commerce}}</ref>

Ferry size continued to increase with traffic until the system upgraded to a cable guidance system in 1946.<ref name="ferrysite" />  John S. Larrabee of Vermont established the first regular ferry at the location in the late 18th century.<ref name="firstferry">{{cite book|url=http://www.middlebury.edu/academics/lis/lib/guides_and_tutorials/subject_guides/collection_guide-vermont/internet_resources/history_addison_county/chap32_hac.htm|title=History of Addison County, Vermont|first=H.P. |last=Smith|publisher=D. Mason & Co., Publishers |year=1886|accessdate=August 27, 2008 |at= Chapter 32|archiveurl=https://web.archive.org/web/20080324023825/http://www.middlebury.edu/academics/lis/lib/guides_and_tutorials/subject_guides/collection_guide-vermont/internet_resources/history_addison_county/chap32_hac.htm |archivedate=March 24, 2008|deadurl=yes}}</ref>  The [[Vermont State Legislature]] approved a franchise for a ferry from Larrabees Point to Ticonderoga in 1907, to the Shoreham and Ticonderoga Ferry Company.<ref name="1907ferry">{{cite book|url=https://books.google.com/books?id=ZaxMAAAAMAAJ&pg=PA207&dq=Larabees+Point+Ferry&lr=&ei=rW20SNCQA4nKjgGbqIQa#PPA208,M1|title=Journal of the House of the State of Vermont|author=Mr. Alexander, from the committee on elections|publisher=Vermont General Assembly|year=1907|accessdate=August 26, 2008|page=207}}</ref> The New York State Legislature granted the ferry a franchise in 1918.<ref name="1918ferry">{{cite book|url=https://books.google.com/books?id=KIM4AAAAIAAJ&pg=RA1-PA288&dq=Larrabees+Point+Ferry&lr=&ei=5Z-0SK-iF4m6zATmpvX1Dg|title=Laws of the State of New York|author=New York State Legislative Bill Drafting Commission|publisher=State of New York|year=1918|accessdate=August 27, 2008 |page=288}}</ref>

==Major intersections==
{{jcttop|state_col=state|length_ref=<ref name="2014tdr">{{cite web |url=https://www.dot.ny.gov/divisions/engineering/technical-services/hds-respository/NYSDOT_Traffic_Data_Report_2014.pdf |title=2014 Traffic Data Report for New York State |date=July 22, 2015 |format=PDF |page=225 |publisher=[[New York State Department of Transportation]] |accessdate=September 16, 2016}}</ref><ref name="VT Route Log">{{cite web|url=http://vtransplanning.vermont.gov/sites/aot_policy/files/Final%20Web.pdf|title=2012 (Route Log) AADTs for State Highways|author=Traffic Research Unit|date=May 2013|website=|publisher=Policy, Planning and Intermodal Development Division, [[Vermont Agency of Transportation]]|accessdate=March 17, 2015}}</ref>|location_ref=<ref name="VT Route Log" />}}
{{NYint
|sspan=4
|county=Essex
|cspan=4
|mile=0.00
|road={{jct|state=NY|I|87}}
|location=Schroon
|lspan=2
|notes=Exit&nbsp;28 on I-87
}}
{{NYint
|mile=0.17
|road={{jct|state=NY|US|9|city1=Schroon Lake|city2=North Hudson|city3=Elizabethtown}}
}}
{{NYint
|mile=17.41
|road={{jct|state=NY|NY|9N|NY|22|dir2=north|name1=Wicker Street|location1=[[Ticonderoga (hamlet), New York|Ticonderoga hamlet]]|city2=Crown Point}}
|location=Ticonderoga
|lspan=2
|type=concur
|notes=Western terminus of NY&nbsp;22&nbsp;/ NY&nbsp;74 [[overlap (road)|overlap]]
}}
{{NYint
|mile=19.03
|road={{jct|state=NY|NY|22|dir1=south|name1=Mountain Road|city1=Whitehall}}
|type=concur
|notes=Eastern terminus of NY&nbsp;22&nbsp;/ NY&nbsp;74 overlap
}}
{{jctplace
|river=[[Lake Champlain]]
|river_wide=yes
|mile=20.45
|mile2=0.000
|line=yes
|place={{jct|extra=ferry}} Fort Ticonderoga–Larrabees Point Ferry; connects to VT&nbsp;74 eastbound and NY&nbsp;74 westbound
}}
{{VTint
|sspan=4
|county=Addison
|cspan=4
|mile=0.493
|location=Shoreham
|lspan=3
|road={{jct|state=VT|Town|73|city1=Orwell|city2=Brandon}}
|notes=Western terminus of VT&nbsp;73
}}
{{VTint
|mile=5.241<!--3.107 on VT 22A-->
|road={{jct|state=VT|VT|22A|dir1=south|city1=Orwell|city2=Fair Haven}}
|type=concur
|notes=Southern terminus of VT&nbsp;22A&nbsp;/ VT&nbsp;74 overlap
}}
<!-- VT 22A Shoreham MP 3.107-3.536 -->
{{VTint
|mile=5.670
|road={{jct|state=VT|VT|22A|dir1=north|city1=Bridport|city2=Vergennes}}
|type=concur
|notes=Northern terminus of VT&nbsp;22A&nbsp;/ VT&nbsp;74 overlap
}}
<!-- Cornwall TL 10.364 -->
{{VTint
|mile=13.263
|road={{jct|state=VT|VT|30|city1=Middlebury|city2=Whiting}}
|location=Cornwall
}}
{{NYintbtm|col=8|keys=concur}}

==See also==
*{{Portal-inline|New York Roads}}
*{{portal-inline|U.S. Roads}}
*{{portal-inline|Vermont}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category multi|New York State Route 74|Vermont Route 74}}
{{Attached KML}}
{{NYSR external links|type=N|nyroutes=yes|termini=yes|route=74|alps=yes}}
* [http://www.state-ends.com/vermont/0740/ Photographs of VT 74 termini (state-ends.com)]

{{Featured article}}

{{DEFAULTSORT:State Route 74 (New York-Vermont)}}
[[Category:State highways in New York|074]]
[[Category:State highways in Vermont|074]]
[[Category:Transportation in Essex County, New York|New York State Route 074]]
[[Category:Transportation in Addison County, Vermont|Vermont Route 074]]