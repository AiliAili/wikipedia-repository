{{multiple issues|
{{notability|date=September 2011}}
{{primary sources|date=September 2011}}
}}

'''MobiHoc''', the '''International Symposium on Mobile Ad Hoc Networking and Computing''', is a series of annual meetings sponsored by [[Association for Computing Machinery|ACM]] [[SIGMOBILE]], focusing on the latest research in the rapidly growing area of mobile [[ad hoc network]]ing and computing.
MobiHoc is a highly selective, single-track forum to address the challenges emerging from wireless ad hoc networking and computing, with the focus being on issues at and above the MAC layer. MobiHoc has been held every year since 2000.

==Ranking==
Although there is no official ranking of [[academic conference]]s on [[wireless networking]] research, MobiHoc is widely regarded by researchers as one of the three (along with [[International Conference on Mobile Computing and Networking|MobiCom]] and [[MobiSys]]) most prestigious conferences focusing on [[wireless networking]] research. The [[acceptance rate]] for 2008 was 14.6% (44 out of 300 papers accepted for publication).

==Previous MobiHoc Conferences==
MobiHoc has been held at the following locations:

* MobiHoc 2015, [[Hangzhou]], China, 22–25 June 2015
* MobiHoc 2014, [[Philadelphia]], [[Pennsylvania]], United States, 11–14 August 2014
* MobiHoc 2013, [[Bangalore]], India, 29 July - 1 August 2013
* MobiHoc 2012, [[Hilton Head Island, South Carolina|Hilton Head Island]], [[South Carolina]], United States, 11–14 June 2012
* MobiHoc 2011, [[Paris]], France, 16–20 May 2011
* MobiHoc 2010, [[Chicago]], [[Illinois]], United States, September 2010
* MobiHoc 2009, [[New Orleans]], [[Louisiana]], United States, 18–21 May 2009
* MobiHoc 2008, [[Hong Kong]], China, 26–30 May 2008
* MobiHoc 2007, [[Montreal]], [[Quebec]], Canada, 9–14 September 2007
* MobiHoc 2006, [[Florence]], Italy, 22–25 May 2006
* MobiHoc 2005, [[Urbana, Illinois|Urbana]]-[[Champaign, Illinois|Champaign]], [[Illinois]], United States, 25–28 May 2005
* MobiHoc 2004, [[Tokyo]], Japan, 24–26 May 2004
* MobiHoc 2003, [[Annapolis]], [[Maryland]], United States, 1–3 June 2003
* MobiHoc 2002, [[Lausanne]], Switzerland, 9–11 June 2002
* MobiHoc 2001, [[Long Beach, California|Long Beach]], [[California]], United States, 4–5 October 2001
* MobiHoc 2000, [[Boston]], [[Massachusetts]], United States, 11 August 2000, as a workshop affiliated with the [[MobiCom (conference)|MobiCom]] 2000 conference also sponsored by [[SIGMOBILE]]. Subsequently, MobiHoc became a separate [[symposium]]

==External links==
* [http://www.sigmobile.org/ SIGMOBILE]
* [http://www.sigmobile.org/mobihoc/ MobiHoc]

[[Category:Computer networking conferences]]


{{compu-conference-stub}}