{{Infobox journal
| cover = [[File:Journal_of_Geosciences_front_cover,_2007.jpg]]
| editor = V. Janoušek
| discipline = [[Earth sciences]]
| abbreviation = J. Geosci.
| formernames = Časopis pro mineralogii a geologii ''(1956-1992)'', Journal of the Czech Geological Society ''(1993-2006)''
| publisher = [[Czech Geological Society]]
| country = [[Czech Republic]]
| frequency = Quarterly
| history = 1956-present
| openaccess = Yes
| license =
| impact = 1.279
| impact-year = 2011
| website = http://www.jgeosci.org/
| link1 = http://www.jgeosci.org/contents
| link1-name = Online tables of contents
| link2 =
| link2-name =
| JSTOR =
| OCLC =
| LCCN =
| CODEN =
| ISSN = 1802-6222
| eISSN = 1803-1943
}}
'''''Journal of Geosciences''''' (1956-1992 ''Časopis pro mineralogii a geologii'', 1993-2006: ''Journal of the Czech Geological Society'') is a [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[Czech Geological Society]] and its predecessors since 1956. The journal covers research in the fields of [[igneous petrology|igneous]] and [[metamorphic petrology]], [[geochemistry]], and [[mineralogy]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
* [[Science Citation Index Expanded]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[Scopus]]
* [[GeoRef]]
In addition, it is part of the [[Geoscience e-Journals]] collection. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 1.279.<ref name=WoS>{{cite book |year=2012 |chapter=Journal of Geosciences |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-23 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.jgeosci.org/}}

[[Category:English-language journals]]
[[Category:Geology journals]]
[[Category:Open access journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1956]]
[[Category:Academic journals published by learned and professional societies]]