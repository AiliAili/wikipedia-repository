{{good article}}
{{Infobox scientist
| name              =
| image             = File:Robert F. Bacher.GIF
| image_size        = 240
| alt               = Head and shoulders of a man in suit and tie with glasses
| caption           =
| birth_date        = {{Birth date|1905|08|31}}
| birth_place       = [[Loudonville, Ohio]]
| death_date        = {{Death date and age|2004|11|18|1905|08|31}}
| death_place       = [[Montecito, California]]
| residence         =
| citizenship       =
| nationality       = [[United States]]
| fields            = [[Physics]]
| workplaces        = [[Cornell University]], [[Los Alamos Laboratory]]
| alma_mater        = [[University of Michigan]]
| doctoral_advisor  =
| academic_advisors = [[Samuel Goudsmit]]<ref>{{MathGenealogy|id=88677}}</ref>
| doctoral_students = [[Boyce McDaniel]]
| notable_students  =
| known_for         = [[Manhattan Project]]
| author_abbrev_bot =
| author_abbrev_zoo =
| influences        =
| influenced        =
| awards            =
| signature         = Robert F. Bacher signature.jpg
| signature_alt     = Robert F. Bacher's signature
| footnotes         =
| spouse            =
}}
'''Robert Fox Bacher''' (August 31, 1905 – November 18, 2004) was an [[United States|American]] [[nuclear physics|nuclear]] [[physicist]] and one of the leaders of the [[Manhattan Project]]. Born in [[Loudonville, Ohio]], Bacher obtained his [[undergraduate degree]] and [[doctorate]] from the [[University of Michigan]], writing his 1930 doctoral thesis under the supervision of [[Samuel Goudsmit]] on the [[Zeeman effect]] of the [[hyperfine structure]] of atomic levels. After graduate work at the [[California Institute of Technology]] (Caltech) and the [[Massachusetts Institute of Technology]] (MIT), he accepted a job at [[Columbia University]]. In 1935 he accepted an offer from [[Hans Bethe]] to work with him at [[Cornell University]] in [[Ithaca, New York]], It was there that Bacher collaborated with Bethe on his book ''Nuclear Physics. A: Stationary States of Nuclei'' (1936), the first of three books that would become known as the "Bethe Bible".

In December 1940, Bacher joined the [[Radiation Laboratory]] at MIT, although he did not immediately cease his research at Cornell into the [[neutron cross section]] of [[cadmium]]. The Radiation Laboratory was organized into two sections, one for incoming [[radar]] signals, and one for outgoing radar signals. Bacher was appointed to handle the incoming signals section. Here he gained valuable experience in administration, coordinating not just the efforts of his scientists, but also those of [[General Electric]] and [[RCA]]. In 1942, Bacher was approached by [[Robert Oppenheimer]] to join the  Manhattan Project at its new laboratory in [[Los Alamos National Laboratory|Los Alamos, New Mexico]]. It was at Bacher's insistence that Los Alamos became a civilian rather than a military laboratory. At Los Alamos, Bacher headed the project's P (Physics) Division, and later its G (Gadget) Division. Bacher worked closely with Oppenheimer, and the two men discussed the project's progress on a daily basis.

After the war, Bacher became director of the Laboratory of Nuclear Studies at Cornell. He also served on the [[U.S. Atomic Energy Commission]], the civilian agency that replaced the wartime Manhattan Project, and in 1947 he became one of its inaugural commissioners. He left in 1949 to become head Division of Physics, Mathematics, and Astronomy at Caltech. He was appointed a member of the [[President's Science Advisory Committee]] (PSAC) in 1958. In 1962, he became Caltech's vice president and [[provost (education)|provost]]. He stepped down from the post of provost in 1970, and became a [[professor emeritus]] in 1976. He died in 2004 at the age of 99.

== Early life and career ==
Bacher was born in [[Loudonville, Ohio]], on August 31, 1905, the only child of Harry and Byrl Fox Bacher. In 1908, the family moved to [[Ann Arbor, Michigan]],{{sfn|Carr|2008|pp=2–3}} where Harry worked as a banker and Byrl as a voice teacher on the [[University of Michigan]] faculty.{{sfn|Whaling|2009|p=3}} Bacher attended the W. S. Perry School, and later [[Ann Arbor High School]]. While there he met Professor [[Harrison M. Randall]], the head of the physics department at the University of Michigan, who encouraged him to study physics.<ref name="Oral History, part 1">{{cite web |date=June 30, 1966 |url=http://www.aip.org/history/ohilist/27978.html |title=Oral History interview transcript with Robert Bacher |publisher=American Institute of Physics, Niels Bohr Library and Archives |accessdate=22 March 2013}}</ref>

Bacher entered the University of Michigan, where he joined the [[Kappa Sigma]]  [[fraternities and sororities|fraternity]] and lived in the frat house. He became the house manager in his [[Sophomore year|sophomore]] year, but moved back home in his [[Junior (education)|junior]] year to concentrate on physics. At Randall's suggestion he applied to the [[Harvard Graduate School of Arts and Sciences]], which he entered in 1926 to study for his doctorate. His fees were paid by his family, but his father had a heart attack and could no longer afford them, so in 1927 Bacher moved back to Ann Arbor, where he lived at home,<ref name="Oral History, part 1" />{{sfn|Whaling|2009|pp=3–5}} and attended the University of Michigan. He received a Charles A. Coffin Foundation Fellowship from [[General Electric]].{{sfn|Carr|2008|pp=4–6}}

To build up the theoretical physics department at the University of Michigan, Randall recruited four distinguished young physicists to work at Ann Arbor in 1927: [[Otto Laporte]], [[George Uhlenbeck]], [[Samuel Goudsmit]], and [[David M. Dennison]]. The University of Michigan was no longer a backwater in theoretical physics. Bacher immediately signed up for Goudsmit's course on atomic structure. With Goudsmit he investigated the [[Zeeman effect]] of [[hyperfine structure]] of atomic levels, which became the subject of his 1930 [[PhD]] thesis.{{sfn|Whaling|2009|pp=5–8}}

On May 30, 1930, Bacher married Jean Dow. His mother gave them a [[Ford Model A (1927–1931)|Ford Model A]] and the use of the family's lakeside holiday house, where they entertained guests including [[Paul Ehrenfest]] and [[Enrico Fermi]]. In 1931, with a [[United States National Research Council|National Research Council]] Fellowship, he spent a year at the [[California Institute of Technology]] (Caltech) because [[Ira Bowen]] taught there. At Caltech Bacher attended lectures by [[Robert Oppenheimer]], but spent most of his time at the [[Mt. Wilson Observatory]], which had a better library. Bacher decided to create a work listing the energy, [[coupling constant]], [[Parity (physics)|parity]] and electron configurations of all the known atoms and [[ion]]s, working with Goudsmit back in Ann Arbor. The result was a book, ''Atomic Energy States as Derived from the Analysis of Optical Spectra'' (1932), which they dedicated to Randall.{{sfn|Whaling|2009|pp=5–8}}{{sfn|Carr|2008|p=5}}

In the second year of his National Research Council Fellowship, Bacher moved across the country to the [[Massachusetts Institute of Technology]] to work with [[John C. Slater]], who had taught Bacher at Harvard. While there, Slater asked him to conduct a seminar on [[John Chadwick]]'s recent discovery of the [[neutron]]. Reading Chadwick's paper, he realized that anomalies in then-current theory would be resolved if the [[Spin (physics)|spin]] of the neutron was ½. This became the subject of a letter he submitted to the ''[[Physical Review]]'' with [[visiting scholar]] [[Edward Condon]].{{sfn|Whaling|2009|pp=8–9}}  A year later, Bacher became the first to suggest the [[neutron magnetic moment|neutron had a magnetic moment]] equal to about minus one [[nuclear magneton]], based on the unusually small magnetic moment of [[nitrogen]] determined from its hyperfine structure.{{sfn|Pais|1986|p=412}}

Afterwards Bacher returned to the University of Michigan on an Alfred H. Lloyd Fellowship. Jobs were hard to find at this time due to the [[Great Depression]], and in 1934 he accepted a job at [[Columbia University]], where he worked with [[Isidor Isaac Rabi]], [[Jerrold Zacharias]], [[Jerome Kellogg]] and [[Sid Millman]]. However it was difficult living in New York on his salary, and Jean gave birth to their first child, Martha, in December 1935. A second child, Andrew, was born in 1938. Bacher therefore accepted an offer from [[Hans Bethe]] to work with him at [[Cornell University]] in [[Ithaca, New York]].  Ithaca was a university town similar to Ann Arbor, where Bacher and Jean had been raised. At Cornell Bacher worked with Bethe on  his book ''Nuclear Physics. A: Stationary States of Nuclei'' (1936), the first of three books that would become known as the "Bethe Bible".{{sfn|Carr|2008|pp=6–8}}{{sfn|Whaling|2009|pp=11–14}}

== World War II ==

=== Radiation Laboratory ===
{{Gallery |title=Las Alamos ID badges |width=140 | height=200 | lines=1 |align=right
|File:Robert Bacher ID badge.png | alt1=Mug shot of a man with glasses in suit and tie. The number O-15 appears in front of him.|Robert Bacher
|File:Jean Dow Bacher Los Alamos ID.png | alt2=Mug shot of woman with glasses in suit The number V-56 appears in front of him. |Jean Dow Bacher
}}
In December 1940 Bacher joined the [[Radiation Laboratory]] at MIT, but did not immediately cease his research. He reached an arrangement with its director, [[Lee Alvin DuBridge]], to return to Cornell for four days every three weeks until it was completed. He was researching the [[neutron cross section]] of [[cadmium]], a topic of interest to Enrico Fermi, who was attempting to build a [[nuclear reactor]], but whose figures did not agree with Bacher's. Bacher carefully checked his results, and Fermi, convinced of their correctness, urged Bacher to publish them. Bacher submitted a paper to the ''Physical Review'' with instructions to withhold publication until after the war, and the paper was not published until 1946.{{sfn|Carr|2008|pp=9–10}}{{sfn|Whaling|2009|pp=11–14}}

DuBridge organized the Radiation Laboratory into two sections, one for incoming [[radar]] signals, and one for outgoing radar signals. Bacher was appointed to handle the incoming signals section. Here he gained valuable experience in administration, coordinating not just the efforts of his scientists, but also those of General Electric and [[RCA]].{{sfn|Carr|2008|pp=9–10}} He later recalled: {{quote|We quickly concluded that the ultimate discrimination between signals reflected from a target, as opposed to noise from the transmitter, should be done finally on the cathode-ray tube. We had to develop the tubes, and then contract with GE and RCA to work together on producing them. I supervised the contracts myself, visiting GE one week and RCA the next, and the following week we would hold a joint meeting at the Rad Lab in Cambridge. I was getting into contract management."{{sfn|Whaling|2009|pp=11–14}} }}

=== Manhattan Project ===
In 1942, Bacher and Rabi, the Radiation Laboratory's assistant director, were approached by Oppenheimer to join the [[Manhattan Project]] at its new [[Los Alamos National Laboratory|laboratory in Los Alamos, New Mexico]]. They convinced Oppenheimer that the plan for a military laboratory would not work, since a scientific effort would need to be a civilian affair. The plan was modified, and the new laboratory would be a civilian one, run by the [[University of California]] under contract from the [[United States Department of War|War Department]]. Bacher felt that this was his first contribution to the success of the project. He met with Oppenheimer at Los Alamos in April 1943, but was not convinced that he was needed.{{sfn|Whaling|2009|pp=15–17}} Oppenheimer wrote to him that:{{quote|You know that I have been extremely eager to have your help in this work. I think perhaps you have not fully realized how much I appreciate your administrative experience and obvious administrative wisdom, nor how aware I am of our need for just this in the present project. Perhaps too you do not evaluate highly enough the fact that you have worked so much in neutron physics, and that you are so well informed about the last year’s developments at MIT. These three qualifications make you, in my opinion, very nearly unique. In addition, I want to express in writing my own confidence in your stability and judgement, qualities on which this stormy enterprise puts a very high premium.{{sfn|Whaling|2009|p=17}} }}

[[File:Leslie Groves, Richard Tolman and Robert Bacher.jpg|thumb|left|Bacher is awarded the [[Medal for Merit]] by Major General [[Leslie R. Groves, Jr.]] |alt= Two men in suits, with medals pinned on the left breast. One shakes hands with a fat man in an Army uniform.]]
Bacher then accepted the job at Los Alamos, moving there in May 1943 to become the head of the Experimental Physics Division (P Division).  For his seven group leaders he had John H. Williams, [[Robert R. Wilson]], [[John H. Manley]], [[Darol Froman|Darol K. Froman]], [[Emilio G. Segrè]], [[Bruno B. Rossi]] and [[Donald W. Kerst]]. Bacher worked closely with Oppenheimer, and the two men discussed the project's progress on a daily basis.{{sfn|Carr|2008|pp=16–20}}

In July 1944, Oppenheimer reorganized the laboratory to focus on the problem of building an [[implosion-type nuclear weapon]], which was necessary because the [[gun-type nuclear weapon|gun-type design]] would not work with [[plutonium]]. P Division was broken up, and Bacher became the head of the G (for Gadget) Division. This much larger division contained eleven groups headed by leaders who included [[William Higinbotham]], [[Seth Neddermeyer]], [[Edwin McMillan]], [[Luis Walter Alvarez|Luis Alvarez]] and [[Otto Frisch]].{{sfn|Carr|2008|pp=22–25}}{{sfn|Whaling|2009|pp=19–21}}

While [[George Kistiakowsky]]'s Explosives Division (X Division) developed the [[explosive lens]]es, G Division designed the rest of the weapon. There were numerous difficulties to overcome, not the least of which was devising a means of detonating the lenses with the required speed. [[Robert F. Christy]]'s solid [[Pit (nuclear weapon)|core]] design was chosen as the most likely design to succeed.{{sfn|Carr|2008|pp=22–25}}{{sfn|Whaling|2009|pp=19–21}}

To coordinate the efforts of the laboratory, Oppenheimer created the "Cowpuncher Committee", so-called because they were to "ride herd" on the implosion effort and coordinate all the efforts of the laboratory. It included Bacher, along with [[Samuel Allison]], George Kistiakowsky, [[Deak Parsons]], [[Charles Lauritsen]] and [[Hartley Rowe]].{{sfn|Hewlett|Anderson|1962|p=318}}

Three days before the day the bomb was to be [[Trinity test|test detonated]] in the [[New Mexico]] desert, Bacher was part of the pit assembly team, which assembled the nuclear capsule (a cylindrical section of the uranium tamper, containing the plutonium core and initiator) in an [[McDonald Ranch House|old farmhouse]] near the [[Alamogordo]] testing site. When the capsule was driven to the shot tower and inserted into the spherical tamper, inside the explosive assembly, it stuck.  Bacher recognized that expansion of the capsule due to the heat given off by the plutonium core was causing the jam, and that leaving the two parts in contact would equalize temperatures and allow the capsule to be inserted fully.{{sfn|Coster-Mullen|2012|pp = 49-50}} After watching the test, his reaction was merely "Well, it works."{{sfn|Carr|2008|p=30}}

Bacher was packaging the [[Demon core|third core]] for shipment to [[Tinian]] on August 12 at the Ice House at Los Alamos when he received word that the Japanese had initiated [[surrender of Japan|surrender negotiations]].{{sfn|Nichols|1987|pp=215–216}} He had already set G Division the task of designing and building new types of cores and assemblies, and he co-authored a report with Robert Wilson urging the development of [[Edward Teller]]'s [[History of the Teller–Ulam design|Super]] bomb. He served on a committee chaired by [[Richard Tolman]] that looked into declassifying documents produced by the Manhattan Project,{{sfn|Carr|2008|pp=33–34}} and one chaired by [[Manson Benedict]] that investigated the technical feasibility of international control of nuclear energy. For his services to the Manhattan Project Bacher was awarded the [[Medal for Merit]] on January 12, 1946.{{sfn|Whaling|2009|pp=19–21}}

== Post-World War II ==

=== Atomic Energy Commission ===
After the war, Bacher returned to Ithaca to head Cornell's Laboratory for Nuclear Studies. He agreed with Bethe that what Cornell needed to become a major player in high energy nuclear physics was a new [[synchrotron]], but first he needed to find somewhere to put it. However, in 1946 Bacher was appointed to the Scientific and Technical Subcommittee of the new [[United Nations Atomic Energy Commission]], along with fellow United States representatives Tolman and Oppenheimer. Bacher therefore had to divide his time between Ithaca and New York City.{{sfn|Whaling|2009|pp=21–23}}

[[File:Bacher, Lilienthal, Pike, Waymack and Strauss.jpg|thumb|right||The five Atomic Energy Commissioners at Los Alamos. Left to right: Bacher, [[David E. Lilienthal]], [[Sumner Pike]], [[William W. Waymack]] and [[Lewis L. Strauss]] |alt=Five men in suits with hats and coats.]]
In October 1946 [[David Lilienthal]] asked Bacher to become one of the inaugural commissioners of the [[United States Atomic Energy Commission]], the civilian agency that was being formed to replace the wartime Manhattan Project. As a [[Republican Party (United States)|Republican]], Bacher was easily confirmed by the [[United States Senate|Senate]] members of the [[United States Congress Joint Committee on Atomic Energy]] by an 8–0 vote. As he was the only one of the five commissioners who was a scientist—an important factor in his decision to accept the post—he played a leading role in the selection of the Atomic Energy Commission's influential General Advisory Committee{{sfn|Whaling|2009|p=24}} to which nine scientists were appointed: [[James Bryant Conant|James Conant]], Lee DuBridge, Enrico Fermi, Robert Oppenheimer, Isidor Isaac Rabi, Hartley Rowe, [[Glenn Seaborg]], [[Cyril Smith]] and Hood Worthington.{{sfn|Hewlett|Duncan|1969|p=665}}

Bacher and fellow commissioner [[Sumner Pike]] began with an inspection of Los Alamos and the [[Hanford Site]], and conducted an inventory of the [[fissionable material]] at Los Alamos with [[Norris Bradbury]], who had succeeded Oppenheimer as its director. He found that only nine atomic bombs had been built in 1946; only four would be in 1947, primarily due to production problems with the reactors at Hanford. These problems were on their way to resolution when Bacher observed the [[Operation Sandstone]] nuclear tests at [[Enewetak Atoll]] in 1948 as the Atomic Energy Commission's representative. Bacher's original two-year term would have expired on January 1, 1949, but President [[Harry Truman]] convinced him to stay on. Bacher resigned in May 1949, and this time the President was unable to dissuade him.{{sfn|Whaling|2009|pp=25–28}}{{sfn|Carr|2008|pp=42–43}}

Bacher wished to return to academia, but Robert Wilson was now the head of Cornell's Laboratory for Nuclear Studies, and Bacher felt that it would be awkward working for someone who was one of his group leaders at Los Alamos. He therefore accepted an offer from Lee DuBridge of the chair of the Division of Physics, Mathematics, and Astronomy at Caltech. However the work at the Atomic Energy Commission was not so easily left behind. Senator [[Bourke Hickenlooper]] charged the Commission with mismanagement, specifically cost overruns at Hanford, awarding a scholarship to a [[communist]], and the loss of {{convert|289|g}} of uranium from the [[Argonne National Laboratory]]. Bacher felt obligated to return to Washington to testify on Lilienthal's behalf before the Joint Committee on Atomic Energy.{{sfn|Hewlett|Duncan|1969|pp=355–361}}{{sfn|Whaling|2009|pp=28–31}}

Another crisis broke in September 1949 when the Air Force picked up signs of the [[Soviet Union]]'s [[RDS-1]] nuclear test. Bacher joined Oppenheimer, Parsons, [[General (United States)|General]] [[Hoyt Vandenberg]], the Atomic Energy Commissioners and a British delegation under [[William Penney]] to discuss what to do. The recent devaluation of the British pound had already triggered a financial crisis, and there was concern about how the markets would react to the news. Oppenheimer and Bacher saw the evidence of a nuclear test as conclusive, and Bacher in particular came down strongly on the side of making a public announcement, as the number of people who already knew made a leak almost inevitable. Truman made the announcement a few days later.{{sfn|Hewlett|Duncan|1969|pp=362–369}}

=== Caltech ===
The division chair that Bacher now occupied at Caltech had been vacant since [[Robert A. Millikan]] had retired in 1945. Although nominally a professorship, it was primarily an administrative post. In 1949 there were 17 professors in the department, of whom nine were physicists, two were astrophysicists, and the remaining six were mathematicians. There were two world class research laboratories funded by the [[Office of Naval Research]], the Cosmic Ray Laboratory that had been founded by Millikan, which was now directed by [[Carl David Anderson|Carl Anderson]], and the  W. K. Kellogg Radiation Laboratory which was directed by Charles Lauritsen. But there were no facilities for high energy physics; these would have to be created from scratch.{{sfn|Whaling|2009|pp=28–31}}

In moving into high energy physics Bacher had the full support not just of DuBridge, but of Anderson and Lauritsen as well. Lauritsen had already hired [[Robert Langmuir]] to begin designing a new 600 MeV synchrotron, and Bacher found a large building to house it that had originally been used for grinding and polishing the Palomar Observatory's {{convert|200|in|adj=on}} mirror, but which had been empty since 1948. He arranged for Atomic Energy Commission and Office of Naval Research grants worth $1 million to build it and the other required facilities, and $300,000 a year to run it.{{sfn|Whaling|2009|pp=32–33}}

Facilities were not enough; Bacher needed physicists. Lauritsen had made a start on this too, by hiring Robert Christy. Bacher hired experimental physicists [[Alvin V. Tollestrup]], [[Robert M. Walker (physicist)|Robert M. Walker]]  and [[Matthew Sands]]. The physicist that Bacher decided he wanted most, though, was [[Richard Feynman]].{{sfn|Whaling|2009|pp=33–34}} To get him, Bacher offered a large salary, and agreed to pay for Feynman's 1950–1951 sabbatical in Brazil. Feynman would go on to win the [[Nobel Prize in Physics]] in 1965.{{sfn|Carr|2008|pp=44–45}} In 1955 Bacher hired [[Murray Gell-Mann]], who would win the Nobel Prize in 1969.<ref>{{cite web |url=http://www.nobelprize.org/nobel_prizes/physics/laureates/1969/gell-mann-bio.html |title=The Nobel Prize in Physics 1969 – Murray Gell-Mann |publisher=The Nobel Foundation |accessdate=March 27, 2013}}</ref>

The relatively new field of [[radio astronomy]] sparked Bacher's interest, and he hired [[John Gatenby Bolton|John Bolton]] and [[Gordon Stanley]] from the Australian [[Commonwealth Scientific and Industrial Research Organisation]] in 1955. A grant from the Office of Naval Research allowed Bolton to build the [[Owens Valley Radio Observatory]], which became an important center for the study of [[quasar]]s.{{sfn|Whaling|2009|pp=32–33}}

<!-- Bacher was elected a Fellow of the [[American Academy of Arts and Sciences]] in 1953.<ref name=AAAS>{{cite web|title=Book of Members, 1780–2010: Chapter B|url=http://www.amacad.org/publications/BookofMembers/ChapterB.pdf|publisher=American Academy of Arts and Sciences|accessdate=April 28, 2011}}</ref> -->
Caltech did not spell an end to Bacher's service in Washington. He served two terms as a member of the [[President's Science Advisory Committee]] (PSAC) under President [[Dwight D. Eisenhower]], from November 18, 1953, to June 30, 1955, and from December 9, 1957, to December 31, 1959. During his first term he testified on behalf of his old friend at the [[Oppenheimer security hearing]] in 1954. During his second, he worked with [[James L. Fisk]] and [[Ernest Lawrence]] to examine how a [[Partial Nuclear Test Ban Treaty]] could be monitored.{{sfn|Carr|2008|pp=52–56}}{{sfn|Whaling|2009|p=40}}

Bacher remained chair of the division of physics, mathematics and astronomy at Caltech until 1962, when he was appointed as vice president and [[provost (education)|provost]]. He stepped down from the post of provost in 1970 at the age of 65, and became a [[professor emeritus]] in 1976.<ref name="Obituary" /> He still did some research at the [[Jet Propulsion Laboratory]], and visited Caltech from time to time. In 1983 he was master of ceremonies at Los Alamos for events commemorating the 40th anniversary of the founding of the laboratory in 1943.{{sfn|Carr|2008|pp=62–65}}

Bacher died on November 18, 2004, at a [[retirement home]] in [[Montecito, California]]. He was survived by his daughter, Martha Bacher Eaton, and son, Andrew Dow Bacher, a nuclear physicist working at [[Indiana University]],<ref name="Obituary">{{cite news |last=Pearce |first=Jeremy |date=November 22, 2004 |url=https://www.nytimes.com/2004/11/22/obituaries/22bacher.html?_r=0 |accessdate=March 25, 2013 |newspaper=[[New York Times]] |title=Robert Bacher, Manhattan Project Physicist, Dies at 99 }}</ref> his wife Jean having died on May 28, 1994.{{sfn|Carr|2008|pp=62–65}} His papers are in the California Institute of Technology Archives.<ref>{{cite web |url=http://www.oac.cdlib.org/findaid/ark:/13030/kt3489q4fh/ |accessdate=March 25, 2013 |title=Finding Aid for the Robert F. Bacher Papers 1924–1994 |publisher=[[Online Archive of California]] }}</ref>

== Notes ==
{{Reflist|30em}}

== References ==
*{{cite book |last=Carr |first=Alan B. |year=2008 |title=The Forgotten Physicist: Robert F. Bacher, 1905–2004 |publisher=Los Alamos Historical Society |location=Los Alamos, New Mexico |isbn=978-0-941232-36-4 |ref=harv }}
* {{cite book |last=Coster-Mullen |first=John |title=Atom Bombs: The Top Secret Inside Story of Little Boy and Fat Man |location=Waukesha, Wisconsin |publisher=J. Coster-Mullen |year=2012 |oclc=298514167 |ref=harv }}
* {{cite book |last=Hewlett |first=Richard G. |authorlink=Richard G. Hewlett |year=1962 |last2=Anderson |first2=Oscar E. |title=The New World, 1939–1946 |location=University Park |publisher=Pennsylvania State University Press |url=http://www.governmentattic.org/5docs/TheNewWorld1939-1946.pdf |accessdate=March 26, 2013 |isbn=0-520-07186-7 |oclc=637004643 |ref=harv }}
* {{cite book |last=Hewlett |first=Richard G. |year=1969 |last2=Duncan |first2=Francis |title=Atomic Shield, 1947–1952 |series=A History of the United States Atomic Energy Commission |publisher=Pennsylvania State University Press |location=University Park |isbn=0-520-07187-5 |oclc=3717478 |ref=harv }}
* {{cite book |last=Nichols |first=Kenneth David |authorlink=Kenneth Nichols |year=1987 |title=The Road to Trinity: A Personal Account of How America's Nuclear Policies Were Made |location=New York |publisher=William Morrow and Company |isbn=0-688-06910-X |oclc=15223648 |ref=harv }}
* {{cite book |last=Pais |first=Abraham |authorlink=Abraham Pais | year=1986 |title=Inward Bound |location=Oxford |publisher=Oxford University Press |isbn= 0198519974 |ref=harv}}
*{{cite book |last=Whaling |first=Ward |year=2009 |url=http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/bacher-robert-f.pdf |title=Robert F. Bacher 1905–2004 |publisher=National Academy of Sciences |series=Biographical Memoir |location=Washington, D.C. |accessdate=March 22, 2013 |ref=harv}}

== External links ==
*[http://manhattanprojectvoices.org/oral-histories/robert-bachers-interview-part-1 1983 Audio Interview with Robert Bacher by Martin Sherwin] Voices of the Manhattan Project
*[http://alsos.wlu.edu/qsearch.aspx?browse=people/Bacher,+Robert Annotated bibliography for Robert Bacher from the Alsos Digital Library for Nuclear Issues]
*[http://www.aip.org/history/ohilist/27978.html Oral History interview transcript with Robert Bacher June 30, 1966, American Institute of Physics, Niels Bohr Library and Archives]
*[http://www.aip.org/history/ohilist/27979.html Oral History interview transcript with Robert Bacher February 13, 1986, American Institute of Physics, Niels Bohr Library and Archives]

{{Presidents of the American Physical Society}}
{{Manhattan Project}}
{{Subject bar
| portal1=World War II
| portal2=Physics
| portal3=History of science
| portal4=Biography
| portal5=Cold War
| commons=y
}}
{{Authority control}}

{{DEFAULTSORT:Bacher, Robert}}
[[Category:1905 births]]
[[Category:2004 deaths]]
[[Category:People from Loudonville, Ohio]]
[[Category:American nuclear physicists]]
[[Category:Manhattan Project people]]
[[Category:Cornell University faculty]]
[[Category:California Institute of Technology faculty]]
[[Category:Fellows of the American Academy of Arts and Sciences]]
[[Category:University of Michigan alumni]]