'''Lisa V. Goodrich''' is a Professor in the [[Department of Neurobiology, Harvard Medical School|Department of Neurobiology]] at the [[Harvard Medical School]].
{{Infobox scientist
| honorific_prefix =
| name        = Lisa Goodrich
| honorific_suffix =
| native_name = 
| native_name_lang = 
| image       =         <!--(filename only, i.e. without "File:" prefix)-->
| image_size  = 
| alt         = 
| caption     = 
| birth_date  =         <!--{{birth date |YYYY|MM|DD}}-->
| birth_place = [[Washington D.C.]]
| death_date  =         <!--{{death date and age |YYYY|MM|DD |YYYY|MM|DD}} (death date then birth date)-->
| death_place = 
| death_cause = 
| resting_place = 
| resting_place_coordinates =  <!--{{coord|LAT|LONG|type:landmark|display=inline,title}}-->
| other_names = 
| residence   = 
| citizenship = 
| nationality = [[United States|American]]
| fields      = [[Neurobiology]]
| workplaces  = [[Harvard University]]
| patrons     = 
| education   = 
| alma_mater  = [[Harvard University]]<br>[[Stanford University]]
| thesis_title =        <!--(or  | thesis1_title =  and  | thesis2_title = )-->
| thesis_url  =         <!--(or  | thesis1_url  =   and  | thesis2_url  =  )-->
| thesis_year =         <!--(or  | thesis1_year =   and  | thesis2_year =  )-->
| doctoral_advisor =    [[Matthew P. Scott|Matthew Scott]]
| academic_advisors = 
| doctoral_students = 
| notable_students = 
| known_for   = 
| influences  = 
| influenced  = 
| awards      = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| spouse      =         <!--(or | spouses = )-->
| partner     =         <!--(or | partners = )-->
| children    = 
| signature   =         <!--(filename only)-->
| signature_alt = 
| website     =         {{URL|http://goodrich.med.harvard.edu/}}
| footnotes   = 
}}
== Biography ==
She grew up in [[Concord, Massachusetts]] and she received her degree in Biology from [[Harvard University]] in 1991. After graduating from Harvard, she completed her Ph.D program in Neuroscience at [[Stanford University]] in 1998. Following the completion of this Ph.D program, she did a postdoctoral fellowship under  Marc Tessier-Lavigne at the [[University of California]].  While concluding her postdoctoral fellowship, she cultivated a particular interest in the auditory system and utilized her familiarity with molecular genetics and cell behavior, and applied it to the sensory system. Lisa currently resides in [[Newton, Massachusetts]]. She enjoys reading and cooking.<ref>{{cite web|last1=Goodrich|first1=Lisa|title=Lisa Goodrich, PhD|url=http://dms.hms.harvard.edu/neuroscience/fac/goodrich.php|publisher=[[Harvard University]]|accessdate=20 December 2015}}</ref>

== Research ==
In her laboratory, her group studies the cellular and molecular mechanisms that impact the development of [[Biological neural network|neural circuits]]. The stages of development being studied are determination, differentiation, how [[Axon|axonal connections]] are formed, and generation of behavior. Her research program focuses on detecting genes necessary for hearing and balance. Her research hopes to connect molecular pathways that are essential in [[cochlea]] development, and to influence these pathways to repair the cochlea after suffering damage.<ref>{{cite web|title=Lab Members|url=http://goodrich.med.harvard.edu/lab-members.html|publisher=Goodrich Lab|accessdate=20 December 2015}}</ref> It focuses   on how the steps of circuit assembly coincide to construct neural networks that are committed to particular functions. In order to do so, research from mouse [[molecular genetics]] and [[genomics]] is studied to discover how circuit assembly is regulated. The two main assemblies she studies are the auditory circuit assembly and the rental circuit assembly.<ref>{{cite web|title=Research|url=http://goodrich.med.harvard.edu/research.html|publisher=Goodrich Lab|accessdate=20 December 2015}}</ref> Her lab utilizes various genetic techniques in mice and [[Assay|biochemical assays]] and [[Embryology|embryological]] studies in chicks. She also uses [[forward genetics|forward]] and [[reverse genetics|reverse]] genetic approaches in mice to gain an understanding of how genetic [[mutation]]s can lead to fluctuations in hearing and balances..<ref>{{cite web|title=Restoring Hearing|url=http://www.actiononhearingloss.org.uk/your-hearing/biomedical-research/projects-and-research/researchers-and-phd-students/researchers/restoring-hearing/lisa-goodrich.aspx|publisher=[[Action on Hearing Loss]]|accessdate=20 December 2015}}</ref> 

She also studies [[morphogenesis]], and how this developmental event is essential in how the nervous system functions.<ref name="resources">{{cite web|title=Resources|url=http://goodrich.med.harvard.edu/resources.html|publisher=Goodrich Lab|accessdate=20 December 2015}}</ref> Goodrich currently has 42 publications on research relative to the [[sensory system]] and [[neurobiology]].  Among them are her articles on "Morphological and physiological development of auditory synapses," "Gata3 is a critical regulator of cochlear wiring," and "Deaf and dizzy mice: A genetic dissection of inner ear development."<ref name="people">{{cite web|title=Department of Neurobiology|url=http://neuro.hms.harvard.edu/people/faculty|publisher=[[Harvard Medical School]]|accessdate=20 December 2015}}</ref>  She also studies [[morphogenesis]], and how this developmental event is essential in how the nervous system functions.<ref name="resources">{{cite web|title=Resources|url=http://goodrich.med.harvard.edu/resources.html|publisher=Goodrich Lab|accessdate=20 December 2015}}</ref> 

== Publications ==
She currently has 42 publications.  Among them are her articles on "Morphological and physiological development of auditory synapses," "Gata3 is a critical regulator of cochlear wiring," and "Deaf and dizzy mice: A genetic dissection of inner ear development."<ref name="people">{{cite web|title=Department of Neurobiology|url=http://neuro.hms.harvard.edu/people/faculty|publisher=[[Harvard Medical School]]|accessdate=20 December 2015}}</ref> 

Her laboratory  has also created a number of resources that are made available to other researchers who have an interest in the inner ear and its development.<ref>{{cite web|title=Lisa Goodrich|url=http://neuro.hms.harvard.edu/people/faculty/lisa-goodrich|publisher=[[Harvard Medical School]]|accessdate=20 December 2015}}</ref> 

== Honors ==
In 1999 she was the recipient of a regional award from [[American Association for the Advancement of Science|AAAS Science journal]] for her essay on “Patching together development and disease." This essay was based on her work in the department of Developmental Biology at Stanford University.<ref>{{cite web|title=Winning Entries|url=http://www.sciencemag.org/site/feature/data/pharmacia/prize/1999.xhtml|website=sciencemag.org|publisher=[[American Association for the Advancement of Science]]|accessdate=20 December 2015}}</ref>

==References==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Goodrich, Lisa}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Harvard University alumni]]
[[Category:American neuroscientists]]
[[Category:Harvard Medical School faculty]]
[[Category:Stanford University alumni]]