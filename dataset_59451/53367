{{refimprove|date=April 2015}}
{{Business administration}}

'''Asset management''', broadly defined,  refers to any system that monitors and maintains things of value to an entity or group or ethnic groups. It may apply to both tangible [[asset]]s such as buildings and to intangible [[asset]]s such as human capital, or humans in general [[intellectual property]], and [[goodwill (accounting)|goodwill]] and [[financial assets]]. Asset management is a systematic process of deploying, operating, maintaining, upgrading, and disposing of assets cost-effectively or ineffectively.

The term is most commonly used in the financial world to describe people and companies that manage investments on behalf of others. These include, for example, [[Investment management|investment managers]] that manage the assets of a [[pension fund]].

Alternative views of asset management in the engineering environment are: the practice of managing assets to achieve the greatest return (particularly useful for productive assets such as plant and equipment), and the process of monitoring and maintaining facilities systems, with the objective of providing the best possible service to users in all dimensions(appropriate for [[public infrastructure]] assets).

==By industry==

===Financial asset management===
The most common usage of the term "asset manager" refers to [[investment management]], the sector of the [[financial services]] industry that manages [[investment fund]]s and [[Security segregation|segregated client accounts]]. An asset management is a part of a financial company which comprises experts who manage money and handle the investments of clients. From studying the client's assets to planning and looking after the investments, all things are looked after by the asset managers and recommendations are provided based on the financial health of each client.

===Infrastructure asset management===
{{main article|Infrastructure asset management}}
Infrastructure asset management is the combination of management, financial, economic, engineering, and other practices applied to physical assets with the objective of providing the required level of service in the most cost-effective manner. It includes the management of the entire lifecycle—including design, construction, commissioning, operating, maintaining, repairing, modifying, replacing and decommissioning/disposal—of physical and infrastructure assets.<ref>[http://www.lgam.info/asset-management Local Government & Municipal Knowledge Base]</ref> Operating and sustainment of assets in a constrained budget environment require a prioritization scheme. As a way of illustration, the recent development of [[renewable energy]] has seen the rise of effective asset managers involved in the management of solar systems (solar park, rooftops and windmills). These teams are actually more and more teaming-up with financial asset managers in order to offer turn key solutions to investors. Infrastructure  asset management became very important in most of the developed countries in the 21st century, since their infrastructure network became almost complete in the 20th century and they have to manage to operate and maintain them cost effectively.<ref>Vanier, D. (2001) Why Industry Needs Asset Management Tools. ASCE Journal of Computing in Civil Engineering. Vol 15(1)</ref>
[[Software asset management]] is one kind of infrastructure asset management.

===Enterprise asset management===
[[Enterprise asset management]] is the business of processing and enabling information systems that support management of an organization's assets, both physical assets, called "tangible", and non-physical, "intangible" assets.

* Physical asset management: the practice of managing the entire lifecycle (design, construction, commissioning, operating, maintaining, repairing, modifying, replacing and decommissioning/disposal) of physical and infrastructure assets such as structures, production and service plant, power, water and waste treatment facilities, distribution networks, transport systems, buildings and other physical assets. It is related to [[asset health management]].
* [[Infrastructure asset management]] expands on this theme in relation primarily to public sector, [[utilities]], property and transport systems. Additionally, Asset Management can refer to shaping the future interfaces amongst the human, built, and natural environments through collaborative and evidence-based decision processes
* [[Fixed assets management]]: an accounting process that seeks to track fixed assets for the purposes of [[financial accounting]]
* [[IT asset management]]: the set of business practices that join financial, contractual and inventory functions to support life cycle management and strategic decision making for the IT environment. This is also one of the processes defined within [[IT service management]]
* [[Digital asset management]]: a form of electronic media content management that includes digital assets

===Public asset management===
{{POV-section|date=April 2014}}
Public asset management, also called corporate asset management, expands the definition of enterprise asset management (EAM) by incorporating the management of all things of value to a municipal [[jurisdiction]] and its citizens' expectations. An example in which public asset management is used is land-use development and planning.

An EAM requires an asset registry (inventory of assets and their attributes) combined with a [[computerized maintenance management system]] (CMMS). All public assets are interconnected and share proximity, possible through the use of [[Geographic information system|geographic information systems (GIS)]].

A GIS-centric public asset management standardizes data and allows interoperability, providing users the capability to reuse, coordinate, and share information in an efficient and effective manner by making the GIS geo-database the asset registry. A GIS platform combined with information of both the "hard" and "soft" assets helps to remove the traditional silos of structured municipal functions. While the [[hard asset]]s are the typical physical assets or infrastructure assets, the soft assets of a municipality includes permits, license, code enforcement, right-of-ways and other land-focused work activities.

The GIS platform is only an assisting tool; asset managers need to make informed decisions about their assets in order to fulfill their organizational goals. While geospatially displayed information can assist in decision-making, the deep understanding of markets, engineering systems, and human interaction enabled by analysis and synthesis of information not found in GIS systems is also present.

==See also==
* [[List of asset management firms]]
* [[P2P Asset Management]]
* [[Robo-advisor]]

==References==
{{reflist}}

==Further reading==
* Baird, G. "Defining Public Asset Management for Municipal Water Utilities". ''Journal American Water Works Association'' May 2011, 103:5:30, www.awwa.org

{{Library resources box 
|by=no 
|onlinebooks=no 
|others=no 
|about=yes 
|label=Asset management }}

{{Finance}}
{{Financial risk}}

[[Category:Management accounting]]
[[Category:Asset management]]
[[Category:Valuation (finance)]]