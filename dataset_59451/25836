{{Multiple issues|
{{Essay-like|date=December 2013}}
{{Original research|date=November 2015}}
}}

"'''Crank'''" is a [[pejorative]] term used for a person who holds an unshakable belief that most of his or her contemporaries consider to be false.<ref>[http://www.m-w.com/cgi-bin/dictionary?book=Dictionary&va=crank Crank] at Merriam-Webster Online Dictionary</ref> A crank belief is so wildly at variance with those commonly held as to be considered ludicrous. Cranks characteristically dismiss all evidence or arguments which contradict their own unconventional beliefs, making any rational debate a futile task and rendering them impervious to facts, evidence, and rational inference.

Common synonyms for "crank" include '''crackpot''' and '''kook'''. A crank differs from a [[Fanaticism|fanatic]] in that the subject of the fanatic's [[Fixation (psychology)|obsession]] is either not necessarily widely regarded as wrong or not necessarily a "fringe" belief. Similarly, the word ''[[quackery|quack]]'' is reserved for someone who promotes a medical remedy or practice that is widely considered to be ineffective; this term, however, does not imply any deep belief in the idea or product they are attempting to sell. ''Crank'' may also refer to an ill-tempered individual or one who is in a bad mood, but that usage is not the subject of this article.

Although experts in the field find a crank's beliefs ridiculous, cranks are sometimes very successful in convincing non-experts of their views. A famous example is the [[Indiana Pi Bill]] where a state legislature nearly wrote into law a crank result in geometry.

==Etymology==

[[Old English]] ''cranc-'' is preserved in modern English [[crankshaft]], and obsolete ''crancstæf'' "a weaver's instrument".
It is from a [[Proto-Germanic]] stem ''*krank-'' meaning "bend". German ''krank'' has a modern meaning of "sick, ill",<ref>Compare to the German noun, ''Krankenhaus'', i.e., "hospital."</ref> evolved from a former meaning "weak, small". English ''crank'' in its modern sense is first recorded 1833, and ''cranky'' in a sense of "irritable" dates from 1821. The term was popularised in 1872 for being applied to [[Horace Greeley]] who was ridiculed during his campaign for the U.S. presidency. In 1882, the term was used to describe [[Charles Guiteau]] who shot U.S. president [[James Garfield]].

In 1906, ''[[Nature (journal)|Nature]]'' offered essentially the same definition which is used here:
{{quotation|A crank is defined as a man who cannot be turned.|''Nature'', 8 Nov 1906, 25/2}}

The term ''crank'' (or ''krank'') was once the favored term for spectators at sporting events, a term later supplanted by ''fans''. By implication, the "kranks in the bleaching boards" think they know more about the sport than do its participants. There is more discussion of this term in ''The Dickson Baseball Dictionary'', by Paul Dickson.

The word ''crackpot'' apparently also first appeared in 1883:
{{quotation|My aunty knew lots, and called them crack-pots.|''Broadside Ballad'', 1883}}

As noted in ''Webster's New Collegiate Dictionary'', the terms ''crackpot'', ''crackbrain'', and ''cracked'' are synonymous, and suggest a metaphorically "broken" head. The terms ''crazy'' and ''crazed'' also originally meant "broken" and derive from the same root word as ''cracked''. The dictionary gives no indication that ''pate'' and ''pot'' have the same root, despite their apparent similarity, and implied colloquial use of ''pot'' to mean "head" in the word ''crackpot''. However, the term ''craze'' is also used to refer to minute cracks in pottery glaze, again suggesting the metaphorical connection of cracked pots with questionable mental health.

The term ''kook'' appears to be much more recent. The [[adjective|adjectival]]-form, ''kooky'', was apparently coined as part of American teen-ager (or [[beatnik]]) [[slang]], which [http://www.etymonline.com/index.php?term=kooky derives from] the pejorative meaning of the noun '''cuckoo'''. In late 1958, [[Edd Byrnes]] first played a hair-combing parking lot attendant called "Kookie" on [[77 Sunset Strip]]. The [[noun]]-form ''kook'', may have first appeared in 1960 in Britain's ''[[Daily Mail]]'' newspaper:
{{quotation|A kook, Daddy-O, is a screwball who is 'gone' farther than most|''Daily Mail'', 22 Aug 1960, 4/5}}

==Common characteristics==

The second book of the mathematician and popular author [[Martin Gardner]] was a study of crank beliefs, ''[[Fads and Fallacies in the Name of Science]]''. More recently, the mathematician [[Underwood Dudley]] has written a series of books on mathematical cranks, including ''The Trisectors'', ''Mathematical Cranks'', and ''Numerology: Or, What Pythagoras Wrought''. And in a 1992 [[UseNet]] post, the mathematician [[John Baez]] humorously proposed a checklist, the [[Crackpot index]], intended to diagnose cranky beliefs regarding contemporary physics.<ref>[[John Baez]], [news:6s22qv$in9$1@pravda.ucr.edu New improved crackpot index] an update to the 1992 list, 26 August 1998, [[sci.physics]] ([https://groups.google.com/group/sci.physics/msg/5312a801e0785e66 archived message] on [[Google Groups]]).</ref>

According to these authors, virtually universal characteristics of cranks include:
#Cranks overestimate their own knowledge and ability, and underestimate that of acknowledged experts.
#Cranks insist that their alleged discoveries are urgently important.
#Cranks rarely, if ever, acknowledge any error, no matter how trivial.
#Cranks love to talk about their own beliefs, often in inappropriate social situations, but they tend to be bad listeners, being uninterested in anyone else's experience or opinions.

Some cranks lack academic achievement, in which case they typically assert that academic training in the subject of their crank belief is not only unnecessary for discovering the truth, but actively harmful because they believe it poisons the minds by teaching falsehoods. Others greatly exaggerate their personal achievements, and may insist that some achievement (real or alleged) in some entirely unrelated area of human endeavor implies that their cranky opinion should be taken seriously.

Some cranks claim vast knowledge of any relevant literature, while others claim that familiarity with previous work is entirely unnecessary; regardless, cranks inevitably reveal that whether or not they believe themselves to be knowledgeable concerning relevant matters of fact, mainstream opinion, or previous work, they are not, in fact, well-informed concerning the topic of their belief.

In addition, many cranks:
#seriously misunderstand the mainstream opinion to which they believe that they are objecting,
#stress that they have been working out their ideas for many decades, and claim that this fact alone entails that their belief cannot be dismissed as resting upon some simple error,
#compare themselves with luminaries in their chosen field (often [[Galileo Galilei]], [[Nicolaus Copernicus]], [[Leonhard Euler]], [[Isaac Newton]], [[Albert Einstein]] or [[Georg Cantor]]),{{citation needed|date=March 2017}} implying that the mere unpopularity of some belief is in itself evidence of plausibility,
#claim that their ideas are being suppressed, typically by secret intelligence organizations, mainstream science, powerful business interests, or other groups which, they allege, are terrified by the possibility of their revolutionary insights becoming widely known,
#appear to regard themselves as persons of unique historical importance.

Cranks who contradict some mainstream opinion in some highly technical field, (e.g. [[mathematics]], [[cryptography]], [[physics]]) frequently:
#exhibit a marked lack of technical ability,
#misunderstand or fail to use standard notation and terminology,
#ignore fine distinctions which are essential to correctly understand mainstream belief.
That is, cranks tend to ignore any previous insights which have been proven by experience to facilitate discussion and analysis of the topic of their cranky claims; indeed, they often assert that these innovations ''obscure'' rather than ''clarify'' the situation.<ref>{{cite journal | author=Hodges, Wilfrid | title=An Editor Recalls Some Hopeless Papers | journal=The Bulletin of Symbolic Logic | year=1998 | volume=4 | issue=1 | pages=1–16 | url=http://www.math.ucla.edu/~asl/bsl/0401/0401-001.ps | doi=10.2307/421003 | jstor=421003}} A paper describing several attempts at disproving [[Cantor's diagonal argument]], looking at the flaws in their arguments and reasoning.</ref>

In addition, cranky scientific theories do not in fact qualify as [[scientific theory|theories]] as this term is commonly understood within science. For example, crank theories in physics typically fail to result in testable predictions, which makes them [[unfalsifiable]] and hence unscientific. Or the crank may present their ideas in such a confused, [[not even wrong]] manner that it is impossible to determine what they are actually claiming.

Perhaps surprisingly, many cranks may appear quite normal when they are not passionately expounding their cranky belief, and they may even be successful in careers unrelated to their cranky beliefs.

==Internet cranks==
{{See also|Usenet personality}}

The rise of the Internet has given another outlet to people well outside the mainstream who may get labeled cranks due to internet [[newsgroup|posting]]s or [[website]]s promoting particular beliefs. There are a number of websites devoted to listing people as cranks. Community-edited websites like Wikipedia have been described as vulnerable to cranks.<ref>"[http://www.accessmylibrary.com/coms2/summary_0286-32770712_ITM Fact or fiction? Who contributes to Wikipedia? Despite ... ]", ''[[Global Agenda]]'', March 12, 2007, Retrieved 23 April 2010</ref><ref>{{cite news| url=http://www.accessmylibrary.com/coms2/summary_0286-26188052_ITM | work=Booklist | title=Wikipedia.(Brief Article) | date=September 15, 2002}}</ref>

Science fiction author and critic [[Bruce Sterling]] noted in his essay in CATSCAN 13:
<blockquote>Online communication can wonderfully liberate the tender soul of some well-meaning personage who, for whatever reason, is physically uncharismatic. Unfortunately, online communication also fertilizes the eccentricities of hopeless cranks, who at last find themselves in firm possession of a wondrous [[soap box|soapbox]] that the [[Trilateral Commission]] and the [[Men In Black]] had previously denied them.<ref>[http://w2.eff.org/Misc/Publications/Bruce_Sterling/Catscan_columns/catscan.13 CATSCAN 13: "Electronic Text"] (Bruce Sterling, ''SF Eye'') Retrieved 8 August 2012</ref></blockquote>

There are also newsgroups which are nominally devoted to discussing (''alt.usenet.kooks'') or poking fun at (''[[alt.slack]], [[alt.religion.kibology]]'') supposed cranks.

==Crank magnetism==

The term '''''crank magnetism''''' was coined by [[physiologist]] and blogger [[Mark Hoofnagle]] on the ''Denialism Blog'' in 2007 to describe the propensity of cranks to hold multiple irrational, unsupported or ludicrous beliefs that are often unrelated to one another, referring to [[William Dembski]] endorsing both a [[Holocaust denier]] and a [[conspiracy theory]] put forward by [[Peter Duesberg]].<ref>{{cite web|last=Hoofnagle|first=Mark|title=Crank Magnetism|url=http://scienceblogs.com/denialism/2007/06/28/crank-magnetism-1/|accessdate=25 November 2015}}</ref> Crank magnetism may be considered to operate wherever a single person propounds a number of unrelated [[denialist]] conjectures, poorly supported [[conspiracy theories]], or [[pseudoscientific]] claims. Thus, some of the common crank characteristics — such as the lack of technical ability, ignorance of scientific terminology, and claims that alternative ideas are being suppressed by the mainstream — may be operating on and manifested in multiple [[orthogonal]] assertions.

[[Mark Hoofnagle|Hoofnagle]]'s fellow blogger Orac has discussed crank magnetism in relation to the writings of British columnist [[Melanie Phillips]], who denies anthropogenic [[global warming]] and who has promoted [[intelligent design]] and the [[MMR vaccine controversy|discredited view that the MMR vaccine causes autism in children]].<ref>{{cite web|last=Hoofnagle|first=Mark|title=Melanie Phillips: Crank magnetism in action on evolution and vaccines|url=http://scienceblogs.com/insolence/2009/05/06/melanie-phillips-crank-magnetism-in-acti/|accessdate=25 November 2015}}</ref> Blogger Luke Scientiæ has commented on the relationship between the number of unrelated claims that magnetic cranks make and the extent of their open hostility to science.<ref>{{cite web|last=Luke Scientiæ|title=A Few Comments on Crank Magnetism|url=http://www.lukesci.com/2011/07/27/a-few-comments-on-crank-magnetism/|accessdate=15 August 2011}}</ref> He has also coined the phrase "magnetic hoax" in relation to hoax claims that attract multiple crank interpretations.<ref>{{cite web|last=Luke Scientiae|title=The Magnetic Hoax: The Giant Hoax as an Example|url=http://www.lukesci.com/2011/08/15/the-magnetic-hoax-the-giant-hoax-as-an-example/|accessdate=15 August 2011}}</ref>

===Studies===
One study, ''NASA faked the moon landing—Therefore (Climate) Science is a Hoax: An Anatomy of the Motivated Rejection of Science'', gave evidence that [[climate change denial]] correlated with [[moon landing conspiracy theories|moon landing]] and [[9/11 conspiracy theories]], staunch beliefs in ''laissez-faire'' [[free-market capitalism]], denial of the [[Health effects of tobacco|link between tobacco smoking and lung cancer]], [[HIV/AIDS denialism]] and [[Assassination of Martin Luther King, Jr.#Conspiracy theories|MLK death conspiracy theories]]:<ref name="MOTIVATED REJECTION OF SCIENCE">Stephan Lewandowsky, Klaus Oberauer, Gilles Gignac. [http://websites.psychology.uwa.edu.au/labs/cogscience/documents/LskyetalPsychScienceinPressClimateConspiracy.pdf "NASA faked the moon landing—Therefore (Climate) Science is a Hoax: An Anatomy of the Motivated Rejection of Science."] ''Psychological Science'' (in press)</ref>

<blockquote>Although nearly all domain experts agree that human CO2 emissions are altering the world's climate, segments of the public remain unconvinced by the scientific evidence. Internet blogs have become a vocal platform for climate denial, and bloggers have taken a prominent and influential role in questioning climate science. We report a survey (N > 1100) of climate blog users to identify the variables underlying acceptance and rejection of climate science. Paralleling previous work, we find that endorsement of a ''laissez-faire'' conception of free-market economics predicts rejection of climate science (r ' .80 between latent constructs). Endorsement of the free market also predicted the rejection of other established scientific findings, such as the facts that HIV causes AIDS and that smoking causes lung cancer. We additionally show that endorsement of a cluster of conspiracy theories (e.g., that the CIA killed Martin-Luther King or that NASA faked the moon landing) predicts rejection of climate science as well as the rejection of other scientific findings, above and beyond endorsement of ''laissez-faire'' free markets. This provides empirical confirmation of previous suggestions that conspiracist ideation contributes to the rejection of science. Acceptance of science, by contrast, was strongly associated with the perception of a consensus among scientists.<ref name="MOTIVATED REJECTION OF SCIENCE" /></blockquote>

Another study titled ''Dead and Alive: Beliefs in Contradictory Conspiracy Theories'' managed to show that, not only will cranks be attracted to and believe in numerous conspiracy theories all at once, but will continue to do so even if the theories in question are completely and utterly incompatible with one another.<ref name="Michael J. Wood">Michael J. Wood, Karen M. Douglas, Robbie M. Sutton. [http://www.academia.edu/1207098/Dead_and_alive_Beliefs_in_contradictory_conspiracy_theories "Dead and Alive: Beliefs in Contradictory Conspiracy Theories"] ''Social Psychological and Personality Science'' (in press)</ref> For instance, the study showed that: "... the more participants believed that [[Princess Diana]] faked her own death, the more they believed that she was murdered [and that] ... the more participants believed that [[Osama Bin Laden]] was already dead when U.S. special forces raided his compound in Pakistan, the more they believed he is still alive," and that "Hierarchical regression models showed that mutually incompatible conspiracy theories are positively associated because both are associated with the view that the authorities are engaged in a cover-up".<ref name="Michael J. Wood" />

Studies such as ''Belief in Conspiracy Theories'' state that conspiracy theories relating to the [[John F. Kennedy assassination conspiracy theories|assassination of JFK]], the [[moon landing conspiracy theories|moon landing]] and [[9/11 conspiracy theories|the September 11th attacks]] are united by a common thread: distrust of the government-endorsed story. This leads the believer to attach other conspiracies as well. Someone with a distrust of the government will likely reject any and all stories or reports directly issued by state agencies or other authorities that are seen as part of the establishment. Thus, any conspiracy will seem more plausible to the conspiracy theorist because this fits with their worldview.<ref>Ted Goertzel. [http://www.jstor.org/stable/3791630 Belief in Conspiracy Theories.] ''International Society of Political Psychology'', vol. 15, no. 4, 1994. DOI 10.2307/3791630</ref>

===Cultic milieu===
In academic [[sociology]], a similar notion to crank magnetism exists, namely Colin Campbell's concept of the ''cultic [[Social environment|milieu]]'', which he used:
<blockquote>...to refer to a society's deviant belief systems and practices and their associated collectivities, institutions, individuals, and media of communication. He described it as including "the worlds of the occult and the magical, of spiritualism and psychic phenomena, of mysticism and new thought, of alien intelligences and lost civilizations, of faith healing and nature cure" (Campbell 1972:122), and it can be seen, more generally, to be the point at which deviant science meets deviant religion. What unifies these diverse elements, apart from a consciousness of their deviant status and an ensuing sense of common cause, is an overlapping communication structure of magazines, pamphlets, lectures, and informal meetings, together with the common ideology of ''seekership''.<ref>"[http://hirr.hartsem.edu/ency/cult.htm Cult]" William H. Swatos, Jr. Editor. ''[[Encyclopedia of Religion and Society]]'', [[Hartford Institute for Religion Research]].</ref></blockquote>

[[Michael Barkun]]'s book ''[[A Culture of Conspiracy]]'' traces the history of certain UFO and "New World Order" conspiracy theories. He finds that these theories and communities were originally distinct, but that certain bookshops and magazines would sell/advertise books of both genres. He traces, over the years, the gradual synthesis as ideas from one milieu start to invade another until both communities routinely refer to both, [[Area 51]] and [[black helicopters]] as part of the same conspiracy canon, for example. He describes the process as [[Improvisational Millennialism]], where people select from existing conspiracy theories to invent their own synthesis. But largely posits that the synthesis as driven by the theories sharing the same transmission channels.<ref name="Culture of Conspiracy">Barkun, Michael. [http://www.ucpress.edu/book.php?isbn=9780520276826 ''A Culture of Conspiracy: Apocalyptic Visions in Contemporary America''] ([[United States]]) 2003 ([[University of California Press]]; 1st edition). ISBN 0-520-23805-2. OCLC [https://www.worldcat.org/oclc/51305869 51305869].</ref>

==See also==
{{div col|colwidth=30em}}
* [[Crackpot index]]
* [[Creativity and mental illness]]
* [[Eccentricity (behavior)]]
* [[Illusory superiority]]
* [[Donna Kossy]]
* [[Dunning–Kruger effect]]
* [[List of topics characterized as pseudoscience]]
* [[Paranoia]]
* [[Pseudophysics]]
* [[Pseudoscholarship]]
* [[Pseudoscience]]
* [[Sovereign citizen movement]]
* [[Tallinna narrid ja narrikesed]]
* [[Tax protester]]
{{div col end}}

;Spoofs
* [[James Parry|Kibo]]
* [[Psychoceramics]]

==References==
{{reflist|30em}}

==Further reading==
*{{cite book | author=Dudley, Underwood | title=A Budget of Trisections | location=New York | publisher= [[Springer-Verlag]] | year=1987 | isbn=0-387-96568-8 }}
*{{cite book | author=Dudley, Underwood | title=Mathematical Cranks | location=Washington, D.C. | publisher= [[Mathematical Association of America]] | year=1992 | isbn=0-88385-507-0}}
*{{cite book | author=Dudley, Underwood | title=The Trisectors | location=Washington, D.C. | publisher= [[Mathematical Association of America]] | year = 1996 | isbn=0-88385-514-3 }}
*{{cite book | author=Dudley, Underwood | title=Numerology: Or, What Pythagoras Wrought | location=Washington, D.C. | publisher= [[Mathematical Association of America]] | year = 1997 | isbn=0-88385-524-0 }}
*{{cite book |author=Dudley, Underwood |title=On Jargon: How to Call a Crank a Crank (and Win If You Get Sued) |year=2008 |publisher=The UMAP Journal, 29.1 |url=http://ns.comap.com/wwwdev.comap.com/pdf/999/On-Jargon-How-Call-Crank-Crank.pdf}}
*{{cite book | author=Eves, Howard | title= Mathematical Circles Squared; A Third Collection of Mathematical Stories and Anecdotes | location=Boston | publisher= Prindle, Weber & Schmidt | year=1972 | isbn=0-87150-154-6}}
*{{cite book | author=[[Martin Gardner|Gardner, Martin]] | title= Fads and Fallacies in the Name of Science | location=New York | publisher= [[Dover Publications|Dover]] | year=1957 | isbn=0-486-20394-8 |LCCN=57003844}}
*Williams, William F. (Editor) (2000). ''[[Encyclopedia of Pseudoscience]]: From Alien Abductions to Zone Therapy'' Facts on File ISBN 0-8160-3351-X
* [[Kossy, Donna]]. ''[[Kooks: A Guide to the Outer Limits of Human Belief]]'', Los Angeles: [[Feral House]], 2001 (2nd ed. exp. from 1994). (ISBN 978-0-922915-67-5)
*{{cite journal | last=Kruger|first= Justin|author2=David Dunning | title=Unskilled and Unaware of It: How Difficulties in Recognizing One's Own Incompetence Lead to Inflated Self-Assessments | journal=J. Pers. And Soc. Psych. | year=1989 | volume=71 | pages=1121–1134 | url=http://www.apa.org/journals/features/psp7761121.pdf|format=PDF}}

==External links==
* [http://www.crank.net Crank Dot Net] Cranks and their theories listed and categorised.

{{Clear}}
{{pseudoscience}}

{{DEFAULTSORT:Crank (Person)}}
[[Category:Pseudo-scholarship]]
[[Category:Pejorative terms for people]]
[[Category:Narcissism]]
[[Category:Cognitive inertia]]
[[Category:Belief]]
[[Category:Personality traits]]
[[Category:Internet culture]]
