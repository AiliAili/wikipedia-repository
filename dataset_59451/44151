{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox airport 
| name         = Bristol (Whitchurch) Airport
| nativename   = Whitchurch Airport
| image        = Hengrove Park.jpg
| caption  = Part of the former runway can be seen in what is now Hengrove Park
| image2       = 
| type         = Closed
| owner        = [[Bristol City Council|Bristol Corporation]]
| operator     = Bristol Airport Committee
| city-served  = [[Bristol]]<br>[[West of England]]<br>[[Gloucestershire]]<br>[[Somerset]]
| location     = [[Whitchurch, Bristol]]
| built        = {{Start date|1930}}
| used         = 1930 - {{End date|1957}}
| elevation-f  = 200
| elevation-m  = 61
| coordinates  = {{coord|51|24|46|N|002|35|11|W|type:airport_region:GB|display=inline,title}}
| pushpin_map            = United Kingdom Bristol
| pushpin_label          = Whitchurch
| pushpin_map_caption    = Location in Bristol
| website      = 
| metric-rwy   =  Y
| r1-number    = <s>10/20</s>
| r1-length-f  = 3,048
| r1-length-m  = 929
| r1-surface   = Originally [[grass]], [[asphalt]] from 1941
| stat-year    = 1936
| stat1-header = Movements
| stat1-data   = 4,810
| stat2-header = Passengers
| stat2-data   = 6,003
| stat3-header = Freight
| stat3-data   = 2,520 lb
| footnotes =<ref>Wakefield, 60, 143</ref>
}}
'''Bristol (Whitchurch) Airport''', also known as '''Whitchurch Airport''', was a municipal airport in [[Bristol]], England, three miles (5&nbsp;km) south of the city centre, from 1930 to 1957. It was the main airport for Bristol and area. During [[World War II]], it was one of the few civil airports that remained operational, enabling air connections to Lisbon and Shannon and from there to the United States.

The airport closed in 1957, with services transferred to the former [[Bristol Airport#RAF Lulsgate Bottom|RAF Lulsgate Bottom]]. The former airfield is now occupied by a [[sports centre]], trading estates and retail parks. The [[South Bristol Community Hospital]] opened on the site in 2012.

==Early history==
In 1929 the Corporation of the City of Bristol bought {{convert|298|acre|km2}} of farmland to the south of the city, near [[Whitchurch, Bristol|Whitchurch]], for a new municipal airport.<ref name=bcc-hengrovepark>
{{cite web
|url=http://www.bristol.gov.uk/ccm/content/Environment-Planning/Parks-and-open-spaces/parks/hengrove-park.en?page=2#internalSection2
|title=Bristol City Council: Parks and open spaces: Hengrove Park
|publisher=www.bristol.gov.uk
|accessdate=19 December 2010
|last=
|first=
}} 
</ref> On 31 May 1930, the airport was officially opened by [[Prince George, Duke of Kent|HRH Prince George, Duke of Kent]]. In its first year of operation, the airport handled 915 passengers, and by 1939 it handled 4,000 passengers.<ref name=bcc-hengrovepark/> The Wessex Aeroplane Club relocated from [[Bristol Filton Airport|Filton Airfield]],<ref name="berryman">{{cite book |title=Somerset airfields in the Second World War |last=Berryman |first=David |authorlink= |coauthors= |year=2006 |publisher=Countryside Books |location=Newbury |isbn=1-85306-864-0 |pages=159–179 }}</ref> and together with Bristol Corporation, managed the facilities.<ref>Wakefield, 1-3</ref> The first buildings were a hangar, a club house for the flying club, and an aircraft showroom.<ref>Wakefield, 11</ref>

Early services offered by "air ferry" were to [[Cardiff]], [[Torquay]] and [[Teignmouth]]. By 1932, two air taxi firms were based at the airport.<ref>Wakefield, 33</ref> By 1934, Bristol Air Taxis had become Western Airways, and it was soon joined by [[Railway Air Services]], a subsidiary of [[Imperial Airways]], offering connections to [[Plymouth]], [[Birmingham]], [[London]], [[Southampton]] and [[Liverpool]].<ref>Wakefield, 49-51</ref> On 18 October 1938, the Straight Corporation, headed by [[Whitney Straight]] purchased control of Norman Edgar (Western Airways), Ltd. and renamed it Western Airways, Ltd.<ref>{{cite web | url=http://www.rafweb.org/Biographies/Straight.htm | title=Air Commodore W W Straight (90680) | publisher=Air of Authority - A History of RAF Organisation | accessdate=23 January 2011}}</ref>

In July 1935, a new terminal building was opened, and regular international services started with flights on the Cardiff-Whitchurch-[[Le Touquet]]-[[Paris Le Bourget]] route.<ref>Wakefield, 55</ref> In 1937, Irish Sea Airways (precursor of [[Aer Lingus]]), and Great Western and Southern Airlines commenced operations from Whitchurch.<ref>Wakefield, 62-65</ref>

In July 1938, the [[Her Majesty's Government|British Government]] formed a [[Civil Air Guard]] to train pilots for what was widely seen as the forthcoming war.<ref>
{{cite web
|url=https://select.nytimes.com/gst/abstract.html?res=F60911FF3E55157A93C6AB178CD85F4C8385F9
|title=Britain to Train 'Civil Air Guard'; 9,000 Persons Will Be Taught to Fly at Low Cost Through Subsidies to Clubs|publisher=The New York Times
|date=24 July 1938
|accessdate=2009-05-28
|last=Special Cable
|first=
}}
</ref> The Bristol and Wessex Aeroplane Club was one of the training organisations enlisted in this effort,<ref name="berryman"/> and in addition No. 33 Elementary and Reserve Flying Training School was established at Whitchurch to prepare [[Royal Air Force Volunteer Reserve (Training Branch)|RAFVR]] pilots.<ref>Wakefield, 74</ref> In August 1938, [[Frank Barnwell]], the chief designer of the [[Bristol Aeroplane Company]], died when an ultra light monoplane he had designed and built himself, the [[Barnwell B.S.W.]], struck a bump when taking off and stalled, crashing onto a nearby road.<ref>[http://www.flightglobal.com/pdfarchive/view/1938/1938%20-%202278.html Flight 11 August 1938 (Obituary Frank Barnwell)]</ref>

==World War II==
[[File:IWM-CH14314 Albatross 205210633.jpg|thumb|A BOAC [[de Havilland Albatross]] at Whitchurch, circa 1941]]
In late August 1939, the airport was requisitioned by the [[Air Ministry]], and was declared a Restricted Area. Starting on 1 September 1939, 59 aircraft belonging to Imperial Airways and [[British Airways Ltd]] were evacuated from [[Croydon Airport]] and [[Heston Aerodrome]] to Whitchurch. The two airlines, that were in the process of merging to form [[British Overseas Airways Corporation]] (BOAC), became the nucleus of [[National Air Communications]] (NAC), formed to undertake wartime air transport work.<ref>Wakefield, 82-83</ref> Airport security was increased, with barbed wire fencing and Air Ministry police posts. During the next two years, an east-west tarmac runway and taxiways were constructed.<ref>Wakefield, 85-90</ref> In late 1939, civilian flights resumed.<ref>Wakefield, 89</ref> From  September 1940, six aircraft of the Dutch airline [[KLM]], which had escaped to Britain after the German invasion of the Netherlands, were also based at Whitchurch. These aircraft with their Dutch crews operated flights to [[Lisbon]] in neutral Portugal, under [[air charter|charter]] to BOAC.<ref>Wakefield, 91-93</ref>

The [[Air Transport Auxiliary]] established No. 2 Ferry Pilots Pool at Whitchurch during 1940. No. 2 FPP was mainly concerned with ferrying [[Bristol Blenheim|Blenheims]], [[Bristol Beaufighter|Beaufighters]] and [[Bristol Beaufort|Beauforts]] built by the [[Bristol Aeroplane Company]] at [[Filton]]; [[Hawker Hurricane|Hurricanes]] built by the [[Gloster Aircraft Company]] at [[Brockworth, Gloucestershire]] and [[Westland Whirlwind (fighter)|Whirlwinds]] and [[Supermarine Spitfire|Spitfires]] produced by [[Westland Aircraft]] at [[Yeovil]] in [[Somerset]].<ref>Wakefield, 93-94</ref> The unit was disbanded in 1945.

During 1942–1943, civil services were developed to [[Shannon Airport]] and an extension of the Lisbon route to [[Gibraltar]], with Lisbon and Shannon providing connections to the United States. Famous passengers who used these services included [[Bob Hope]], [[Bing Crosby]], [[Wilhelmina of the Netherlands|Queen Wilhelmina of the Netherlands]] and [[Eleanor Roosevelt]].<ref>Wakefield, 120-124</ref> On 1 June 1943, [[BOAC Flight 777]] was shot down en route to Whitchurch from Lisbon, with the loss of four Dutch crew and 13 passengers, including the actor [[Leslie Howard (actor)|Leslie Howard]].<ref>Wakefield, 107–108</ref>

In November 1944, BOAC moved out to [[Bournemouth Airport|Hurn Airport, Bournemouth]], because the runways there were capable of accommodating larger aircraft, and the success of the [[Invasion of Normandy]] had lessened the danger from the [[Luftwaffe]].<ref>Wakefield, 131</ref>

==Post war==
After the war, the airport came under the control of the [[Ministry of Civil Aviation (United Kingdom)|Ministry of Civil Aviation]]. A number of flying clubs used the airport, but the airport did not attract many scheduled services, although from 1953 [[Morton Air Services]] operated flights to the [[Channel Islands]], the [[Isle of Wight]] and the [[Isle of Man]], whilst [[Cambrian Airways]] operated both domestic and international flights to France.<ref>Wakefield, 147-153</ref>

The airport had become too small for airline operations, with surrounding housing estates limiting runway extension, so a new site at the former RAF Lulsgate Bottom was opened in May 1957 as [[Bristol Airport]].<ref>Wakefield, 153-154</ref> In 1957, Flying ceased at Whitchurch, and in 1959 the airfield was re-opened as [[Whitchurch Circuit]], a car racing circuit holding [[Formula Two]] and [[Formula Three]] races. Over the years, the area has been developed as housing and trading estates known as Hengrove Park, although part of the main runway still exists.<ref>
{{cite book
|url=https://books.google.com/books?id=3DY9AAAAIAAJ&pg=PA110&dq=whitchurch+airport&ei=fJapSbcPi-4y98CQygg#PPA110,M1
|title=Industrial history from the air. 
|publisher=Google Book Search
|accessdate=2009-05-29
|last=Hudson
|first=Kenneth
}}
</ref>

In 1993 a [[Cessna]] aircraft made an emergency landing there short of fuel.<ref>{{cite web|url=http://freespace.virgin.net/gary.morris69/whitchurch.html |title=Archived copy |accessdate=2010-10-30 |deadurl=yes |archiveurl=https://web.archive.org/web/20121015223502/http://freespace.virgin.net/gary.morris69/whitchurch.html |archivedate=15 October 2012 |df=dmy }}</ref>

In 2009, it was announced that part of the former airfield was to be developed as [[South Bristol Community Hospital]], a Skills Academy and Leisure Centre.<ref>
{{cite web
|url=http://www.hengrovepark.com/
|title=Hengrove Park Phase 1
|publisher=www.hengrovepark.com
|accessdate=2009-05-29
|last=
|first=
}}
</ref>

==Notes==
{{reflist|30em}}

==Bibliography==
*Cluett, Douglas; Bogle, Joanna; Learmonth, Bob. 1984. Croydon Airport and The Battle for Britain. London Borough of Sutton. ISBN 0-907335-11-X.
*Cluett, Douglas; Nash, Joanna; Learmonth, Bob. 1980. Croydon Airport 1928 - 1939, The Great Days. London Borough of Sutton ISBN 0-9503224-8-2
*Doyle, Neville. 2002. The Triple Alliance: The Predecessors of the first British Airways. [[Air-Britain]]. ISBN 0-85130-286-6
*Moss, Peter W. 1962. Impressments Log (Vol I-IV). Air-Britain.
*{{Cite book | last=Wakefield | first=Kenneth | coauthors= | title="Somewhere in the west country": the history of Bristol (Whitchurch) Airport, 1930-1957 | year=1997 | publisher=Crécy | location=Wilmslow | isbn=0-947554-65-3 | pages=}}

==External links==
{{Commons category|Bristol (Whitchurch) Airport}}
*[http://www.flightglobal.com/pdfarchive/view/1933/1933%20-%201166.html Flight (at Flightglobal) 7 December 1933 Page 228 Airport description and map]
*[https://web.archive.org/web/20060926192823/www.chew76.fsnet.co.uk/whitchurch/whitchurch.html Archived "Bristol Aviation" page for Whitchurch]

{{Defunct airports in the United Kingdom}}

[[Category:Defunct airports in England]]
[[Category:Airports established in 1930]]
[[Category:1930 establishments in England]]
[[Category:Transport in Bristol]]
[[Category:Airports in South West England]]