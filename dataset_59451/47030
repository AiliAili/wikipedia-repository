[[File:JorgensenStudioYOSE10243.jpg|thumb|right|Jorgensen inside his [[Chris Jorgensen Studio|Studio]] in [[Yosemite National Park]]]]

'''Chris Jorgensen''' (October 7, 1860- June 24, 1935) was a Norwegian-born American landscape painter. Jorgensen is best known for his paintings of [[Yosemite Valley]] and the [[California Missions]].

==Early life and influences==

Chris Jorgensen was born in [[Oslo]] [[Norway]] in 1860, to parents Ole and Sophie Jorgensen. Ole Jorgensen died of tuberculosis in 1864, leaving his wife to care for Chris and his four older siblings.<ref name="ChrisJorgensen">Chris Jorgensen, Yosemite Artist. San Francisco: The Yosemite Fund, 2004.</ref>{{rp|3}} In 1870 Sophie immigrated to the United States to reunite with her brother in [[San Francisco]], California.

Jorgensen met his future mentor [[Virgil Williams]] shortly before the [[San Francisco School of Design]] opened in February 1874. Williams, the director of this academy, spotted the young fourteen-year-old Jorgensen sketching near his residence in the city and invited him to be the first free student.<ref name="LittellKatherine">Littell, Katherine. Chris Jorgensen: Californian Pioneer Artist. Sonora: Fine Arts Research Publishing Co, 1988.</ref>{{rp|5}} Williams' influence over Jorgensen’s budding artistic style was great. As Williams had been exposed to French and Italian methods of painting in his travels abroad, he emphasized the importance of regular exhibition to the young Jorgensen. Jorgensen’s lifelong tendency towards classical compositions arose from Williams’ mentorship.

Another influential mentor of Jorgensen’s was [[Thomas Hill (painter)|Thomas Hill]], a highly successful California artist during the late 19th century. From Hill, Jorgensen developed a more impressionistic technique and learned to intensify the colors in his works by coating them in a heavy glaze.<ref name="LittellKatherine" />{{rp|5}}

At the end of his course at the School of Design in 1881, Williams appointed Jorgensen an instructor and the Assistant Director of the School.<ref name="ChrisJorgensen" />{{rp|3}} It was in this period that Jorgensen had the good fortune to fall in love with and marry one of his art students in 1888, Angela [[Ghirardelli]], heiress to the Ghirardelli fortune.

==Yosemite period==

[[File:JorgensenIndianHeadYOSE24912.jpg|thumb|left|Indian Head, Watercolor, 1900]]
Influenced by the travels of Thomas Hill, Virgil Williams and [[Albert Bierstadt]], Jorgensen decided to visit the famed Yosemite Valley in 1898.<ref name="ChrisJorgensen"/>{{rp|4}} Two years later, the artist submitted an application to the Yosemite Commission for permission to construct a [[Chris Jorgensen Studio|studio]] in the valley itself on July 12, 1900.<ref name="LittellKatherine"/>{{rp|38}} The commission granted their permission for an initial four-year lease which was repeatedly extended. Not only did the commission wish to promote Yosemite’s beauty by having a popular artist in residence, but they also had the intention of eventually taking over use of the structure when Jorgensen’s lease expired.<ref>{{cite web|title=An Artist and His Chocolate|url=http://www.nps.gov/yose/blogs/An-Artist-and-His-Chocolate.htm|publisher=Yosemite Ranger Notes|accessdate=1 February 2015|date=September 2, 2014}}</ref> Jorgensen constructed his home for a total of $5,000<ref name="LittellKatherine_a">Littell, Katherine. Chris Jorgensen: Californian Pioneer Artist. Masters Thesis, Stanislaus, California State University, 1992.</ref> and lived there during the summers and some winters from 1900 to 1917. At the time of the artist’s occupation, the house’s living room was described by the San Francisco Chronicle (February 17, 1901) as one of the “most unique and artistic studios on the coast” containing all manner of objects from skins and baskets to beer steins and water buffalo horns.<ref name="LittellKatherine_a" /> 
 
Jorgensen and his wife Angela donated many Native American baskets to the [[Yosemite Museum]] between 1922 and 1929 and many of Jorgensen’s works came to the Museum as a bequest from Angelina Jorgensen after her death.<ref name="LittellKatherine"/>{{rp|6}}

==Later life and death==

In 1903 the Jorgensens bought a house in Carmel which they used as a winter studio. This house still exists today as the Hotel La Playa in [[Carmel-by-the-Sea, California|Carmel]]. His Yosemite studio became the first museum in Yosemite Valley in 1922 although it was later dismantled.<ref name="ChrisJorgensen"/>{{rp|7}} 
  
In 1917 when the Jorgensens left Yosemite, they decided to live permanently in [[Piedmont, California]], in order to remain closer to the Ghirardelli family.<ref>http://www.thefreelibrary.com/The+Ghirardelli+story.-a0104669393</ref> Chris Jorgensen died in 1935 from a heart attack. His wife lived only several months after him.

==Works==

Jorgensen was an unusual artist in the sense that he often chose to paint in watercolor, a medium that was still viewed as inferior to oil painting at this time.  However, Jorgensen was more successful in the medium of watercolor than in oil. His style of careful delineation in the foreground and distinct foggy washes in the horizon appears charming in a transparent medium but more primitive in heavier oils. Watercolor also lent itself well to his habit of painting [[en plein air]]. During his career as an artist, Jorgensen was successfully received and made a tidy profit selling his paintings both in Yosemite and San Francisco.

'''Early Works'''
* ''Tug Favorite, pencil and chalk on paper, 1880
* ''House and Tree, watercolor on paper,1883''

'''Selected Oil Paintings'''
* ''Vernal and Nevada Falls, 1903
* ''Lena Brown and Child, 1905
* ''Yosemite, 1906
* ''Trail in High Sierra, 1909
* ''Mariposa Grove, Two Sequoias, 1910''

'''Selected Watercolor Paintings'''
* ''Wawona
* ''Minarets in Yosemite
* ''Mariposa Grove
* ''Happy Isles
* ''Mariposa Grove. Base of Sequoia gigantea
* ''Yosemite Falls and Overhanging Rock
* ''Half Dome from the North

==References==
{{reflist}}
{{Commons category|Chris Jorgensen}}

==External links==
* [http://www.nps.gov/yose/planyourvisit/historic.htm Yosemite Museums, Historic Buildings, and Cemeteries]
* [http://missiontour.org/jorgensen/index.htm Mission Watercolors at the Sonoma Mission]

{{coord|37.7486|-119.5880|type:landmark_region:US-CA|display=title}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Jorgensen, Chris}}
[[Category:1860 births]]
[[Category:1935 deaths]]
[[Category:20th-century American painters]]
[[Category:American landscape painters]]
[[Category:Painters from California]]
[[Category:19th-century American painters]]
[[Category:American male painters]]
[[Category:People from Piedmont, California]]