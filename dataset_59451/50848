{{Infobox journal
| title         = Cancer Cytopathology
| cover         = CancerCyto.png
| editor        = Celeste N. Powers
| discipline    = [[Pathology]], [[Oncology]]
| publisher     = [[Wiley-Blackwell]] for the [[American Cancer Society]]
| country       = [[United States]]
| frequency     = Monthly
| history       = 1997–present
| openaccess    = "OnlineOpen" optional
| license       = 
| impact        = 3.183
| impact-year   = 2015
| website       = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1934-6638
| link1         = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1934-6638/currentissue
| link1-name    = Online access
| link2         = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1934-6638/issues
| link2-name    = Online archive
| RSS           = 
| atom          = 
| JSTOR         = 
| eISSN         = 1934-6638
| boxwidth      = 
}}
'''''Cancer Cytopathology''''' is a monthly [[peer-reviewed]] [[scientific journal]] which covers practice of [[cytopathology]] and its related [[oncology]]-based disciplines. It is one of three official journals of the [[American Cancer Society]] and is published by [[Wiley-Blackwell]] on behalf of the society. The current [[editor-in-chief]] is Celeste N. Powers. ''Cancer Cytopathology'' was published as a supplement of [[Cancer (journal)|''Cancer'']] from 1997 until 2008 when it was split into a separate journal.<ref name=LC>{{cite web |url=http://lccn.loc.gov/50001289 |title=Cancer |work=Library of Congress Catalog |publisher=[[Library of Congress]] |format= |accessdate=2015-01-07}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Academic OneFile]]<ref name=UWEB/>
*[[Academic Search]]<ref name=UWEB/>
*[[Biological Abstracts]]
*[[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-01-07}}</ref>
*[[CAB Abstracts]]<ref name= CABAB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title= Serials cited |work= [[CAB Abstracts]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2015-01-07}}</ref>
*[[Chemical Abstracts]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-01-07 }}{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
*[[CINAHL]]<ref name=CINAHL>{{cite web |url=http://www.ebscohost.com/titleLists/ccf-coverage.htm |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2015-01-07}}</ref>
*[[Current Contents]]/Clinical Medicine<ref name=ISI/>
*Current Contents/Life Sciences<ref name=ISI/>
*[[Current Index to Statistics]]
*[[Elsevier BIOBASE]]<ref name=UWEB/>
*[[Embase]]<ref name=UWEB/>
*[[Global Health]]<ref name= CABGH>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/global-health/ |title= Serials cited |work= [[Global Health]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2015-01-07}}</ref>
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/374236 |title= ''Cancer'' |work= [[United States National Library of Medicine|NLM]] Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2015-01-07}}</ref>
*[[International Bibliography of Periodical Literature]]<ref name=UWEB>{{cite web |title= ''Cancer'' |url= http://www.ulrichsweb.serialssolutions.com/title/1420616000864/43347 |work= [[Ulrichsweb]] |publisher= [[ProQuest]] |accessdate=2015-01-07 |subscription= yes}}</ref>
*[[PASCAL (database)|PASCAL]]
*[[PsychINFO]]<ref>{{cite web |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2015-01-07}}</ref>
*[[Science Citation Index]]<ref name=ISI/>
*[[Scopus]]<ref name=UWEB/>
*Sociedad Iberoamericana de Informacion Cientifica (SIIC) databases
*[[Tropical Diseases Bulletin]]<ref name= CABTDB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/tropical-diseases-bulletin/ |title= Serials cited |work= [[Tropical Diseases Bulletin]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2015-01-07}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.183, ranking it 17th out of 78 journals in the category "Pathology" and 95th out of 213 journals in "Oncology".<ref name="WoS">{{cite book|title=2015 Journal Citation Reports|last=|first=|publisher=[[Thomson Reuters]]|year=2016|isbn=|edition=Science|series=[[Web of Science]]|location=|pages=|chapter=Journals Ranked by Impact: Oncology|via=}}</ref>

==References==
{{Reflist|30em}}


{{American Cancer Society}}

[[Category:Oncology journals]]
[[Category:Publications established in 1948]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Academic journals associated with learned and professional societies]]
[[Category:American Cancer Society]]


{{medical-journal-stub}}