{{Infobox video game
| image = Knytt Underground Main Menu Title.png
| caption = The logo screen from the main menu, presented as a playable area of the game
| developer = {{Unbulleted list|[[Green Hill Games]]|[[Nifflas]]}}
| publisher = [[Ripstone]]
| series = ''[[Knytt]]''
| released = {{Video game release|WW|18 December 2012}}
| genre = [[Adventure game|Adventure]], [[Platform game|platform]]
| platforms = [[Linux]], [[Microsoft Windows]], [[OS X]], [[PlayStation 3]], [[PlayStation Vita]], [[Wii U]]
}}

'''''Knytt Underground''''' is a 2D adventure-exploration game with elements of arcade [[Platform Game|platformers]] and combining the mechanics of ''[[Knytt]]'' and ''[[Within a Deep Forest]].'' It was developed by [[Nicklas Nygren]] and released in late 2012, with the [[PlayStation]] platforms seeing the first launch. ''Underground'' offers two playable characters, non-linear gameplay, multiple endings and a large number of side-quests and secrets.

==Gameplay==
''Knytt Underground''<nowiki/>'s gameplay fuses the mechanics of Nifflas' earlier games. From ''[[Knytt]]'' it borrows the freedom to climb vertical walls as well as being able to safely fall from any height (barring hazards like lava and acid, but unlike ''Knytt'' water is not hazardous). From ''[[Within a Deep Forest|Deep Forest]]'' it incorporates the ability to transform into a [[bouncing ball]]. However, there is only one ball "material" and different abilities must be gained by flowers that bestow temporary power-ups which are only usable in Sprite form, a new future introduced in ''Underground.''

There are no hostile characters or antagonists in the story except for robots that follow a preset behavior and are confined to the room they spawn in. The player may interact with various [[NPCs]] and receive quests from them. Some characters may become traveling companions and influence certain events. The player also has an inventory system for items which are used for trading, completing tasks or gaining access to certain areas.

The game is set in a post-apocalyptic future where the surface has been rendered uninhabitable. Humans no longer exist and the surviving creatures all live underground, although remnants of human technology are commonplace. There are different factions within the subterranean world such as the mystical Myriadists and a group called the Internet dedicated to salvaging human artifacts.

The main world is divided into 47 rooms across by 29 rooms down, for a total of at least 1,363 rooms. However, there are many secret areas that exist off the in-game map, leading to over 1,800 total locations in all.<ref name=steampowered.com /> Each region of the world has a distinct visual theme and some areas are analogous to the levels in ''[[Within a Deep Forest]]''. Shortcuts for quickly traveling across large distances can be found via an alternative dimension called "the Disorder".

==Plot==
The player controls Mi Sprocket, a "sprite" who sets out to make a wish at the Fairy Springs, but due to her [[Muteness|inability to speak]], she is misunderstood by everyone and unwillingly recruited into a mission to save the world. Two of the fairies decide to accompany her; Cilia the Moon Fairy and Dora the Sun Fairy. They provide the majority of the dialogue within the game and each has her individual temperament, often in contrast to each other. Mi has to choose which fairy to send to speak on her behalf when initiating conversations with NPCs, and they can influence which quests the player is able to take and may even affect the outcome of those quests.

Mi's primary goal involves ringing the Six Bells of Fate, a task which falls upon a member of the Sprocket family every six hundred years and is required by prophecy to prevent the destruction of the world. There are numerous side quests as well and most may be completed in multiple ways, awarding the player with different achievements and endings. ''Underground'' often breaks the [[fourth wall]], with the developer himself making appearances as an NPC and various characters making remarks on common video-game [[cliché]]s, including the apparent pointlessness of the main quest.

==Development==
In July 2012, Nifflas announced that he was working on ''Knytt Underground'', and that it is his biggest game yet. It was released on the PlayStation Network for PlayStation 3 and PS Vita in late December 2012, and the console versions include extra content.<ref name="blog.us.playstation.com">[http://blog.us.playstation.com/2012/12/18/knytt-happens-knytt-underground-rolls-to-ps3-ps-vita-today/ "Knytt Happens: Knytt Underground Rolls to PS3, PS Vita Today – PlayStation.Blog"]. Blog.us.playstation.com. Retrieved 2013-08-17.</ref><ref>[http://indiegames.com/2012/07/nifflas_to_create_knytt_underg.html "The Weblog Nifflas to Create Knytt Underground for PlayStation, PC"]. IndieGames.com. 2012-07-26. Retrieved 2013-08-17.</ref>  The game was released on Steam on October 25, 2013.<ref name="steampowered.com">[http://store.steampowered.com/app/248190/ "Knytt Underground on Steam"]. Steampowered.com. Retrieved 2013-10-27</ref> A Wii U version of the game was confirmed in August 2013, and was being ported by Nygren's colleague Mathias Kærlev.<ref>[http://www.nintendolife.com/news/2013/08/ripstone_confirms_details_for_knytt_underground_and_pure_chess_on_the_wii_u_eshop "Ripstone Confirms Details for Knytt Underground and Pure Chess on the Wii U eShop"]. Nintendo Life. Retrieved 2013-08-28.</ref> The game was released on December 19, 2013 in both North America and Europe.<ref>{{cite web|url=http://www.nintendolife.com/news/2013/12/knytt_underground_arrives_on_the_wii_u_eshop_on_19th_december |title=Knytt Underground Arrives on the Wii U eShop on 19th December - Wii U eShop News @ Nintendo Life |publisher=Nintendolife.com |date= |accessdate=2015-05-29}}</ref> ''Underground'' was Nifflas' biggest project at the time of its release, encompassing the biggest world and the most characters, sub-quests and secrets of any of his games thus far.

==Soundtrack==
Besides Nifflas' own songs, ''Knytt Underground'' includes music by D Fast, David Kanaga, Fredrik Häthén, LPChip, Jonas Mattebo, Stephen Ascher, Void Pointer, and Yann Van Der Cruyssen. The soundtrack is available as a free download.<ref>{{cite web|url=http://steamcommunity.com/app/248190/discussions/0/684839198957358334/ |title=Free download - official soundtrack! :: Knytt Underground General Discussions |publisher=Steamcommunity.com |date=2013-10-29 |accessdate=2015-05-29}}</ref>

==References==
{{Reflist}}

==External links==
* [http://nifflas.ni2.se/?page=Knytt+Underground Official website]
* [http://www.knyttunderground.com Official website for PlayStation versions]

[[Category:2012 video games]]
[[Category:Indie video games]]
[[Category:Linux games]]
[[Category:Metroidvania games]]
[[Category:MacOS games]]
[[Category:Platform games]]
[[Category:PlayStation 3 games]]
[[Category:PlayStation Vita games]]
[[Category:Post-apocalyptic video games]]
[[Category:Video games developed in Sweden]]
[[Category:Video games featuring female protagonists]]
[[Category:Windows games]]
[[Category:Video games with silhouette graphics]]
[[Category:PhyreEngine games]]
[[Category:Wii U eShop games]]