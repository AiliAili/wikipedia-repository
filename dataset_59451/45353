{{Infobox military person
| name          =Friedrich Friedrichs
| image         =Friedrich_Friedrichs.jpg
| caption       =
| birth_date          = 21 February 1895
| death_date          = {{Death date and age|df=yes|1918|7|15|1895|2|21}}
| placeofburial_label = 
| placeofburial = 
| birth_place  =Spork, [[Westphalia]], [[Germany]]
| death_place  =[[Arcq]], [[France]]
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =Fritz
| allegiance    =German Empire
| branch        =Infantry, then flying service
| serviceyears  =ca 1915–1918
| rank          =Leutnant
| unit          =FA(A) 264, [[Jasta 10]]
| commands      =
| battles       =
| awards        =[[Royal House Order of Hohenzollern]], [[Iron Cross]]
| relations     =
| laterwork     =
}}
Leutnant '''Friedrich Friedrichs''' was a [[World War I]] [[fighter ace]] credited with 21 confirmed victories.

Undaunted by an early invaliding by infantry combat during early World War I, Friedrichs switched to aviation. After serving reconnaissance duty, he moved to being a [[fighter pilot]] with [[Jagdstaffel 10]]. He shot down 11 enemy [[observation balloons]], making him a top [[balloon buster]]; he also shot down 10 enemy airplanes. He was killed in action on 15 July 1918 when his [[Fokker D.VII]] burst into flames in midair.

==Early life and military service==
Friedrich "Fritz" Friedrichs was born in Spork, Westphalia, in western Germany, on 21 February 1895.<ref name=lines110/> His father was a customs official. Friedrichs attended Hermann-Tast Gymnasium. He received his diploma in 1914.{{citation needed|date=February 2013}} He was interested in a medical career.<ref name="Pfalz Scout Aces p.26">{{cite book |title= Pfalz Scout Aces of World War I |page= 26 }}</ref>

He volunteered for service in the [[German Army (German Empire)|German Imperial Army]]'s Infantry Regiment No. 85 on 14 August 1914. On 9 October, he went to the front. He served with them until seconded to officer training in Munich. Upon graduation on 23 September 1915, he was commissioned a lieutenant in the reserves and posted to Infantry Regiment No. 32.<ref name="Pfalz Scout Aces p.26"/> During its campaign in Serbia, he was hit by shellfire and wounded so seriously that he was declared unfit for further duty because of permanent damage to his left leg.<ref name=lines110>Franks et al 1993, p. 110.</ref>

==Aerial service==
See also [[Aerial victory standards of World War I]].

Friedrichs then transferred to the ''[[Luftstreitkräfte]]''.<ref name=lines110/> He underwent training from 1 October 1916 through 20 February 1917. He undertook aviation training at [[Cologne]] and [[Paderborn]] before learning artillery observation at [[Jüterbog]]. On 9 June,{{citation needed|date=February 2013}} having trained as both observer and pilot, he was posted to FA(A) 264, an artillery observation squadron. While serving with them, he won both classes of the [[Iron Cross]].<ref name=lines110/>

{|{{Infobox Aircraft Begin
  |name = Pfalz D.III
  |image = Pfalzdiiia.jpg
  |caption = Friedrichs flew this type without success when first assigned to fly fighters.
}}
|}

On 4 January 1918, he began a week's ''Jastashule''. Upon completion, he moved up to Royal Prussian [[Jasta 10]], a part of [[Jagdgeschwader 1 (World War I)|Jagdgeschwader 1]],<ref name=lines110/> to fly a [[Pfalz D.III]].{{citation needed|date=February 2013}}

His first claim, on 18 March 1918, went unconfirmed. Three days later, he became a [[balloon buster]], blasting one of the floating observation posts at Ruyalcourt, France, in the vicinity of the [[Somme (department)|Somme]]. He scored once more in March, downing a [[Royal Aircraft Factory S.E.5|SE.5a]] on the 27th.<ref name=lines110/>

Friedrichs had no successes in April. He shot down a [[D.H.9]] on 3 May, and a [[Sopwith Camel]] on the 15th. Three days later, he began a string of six consecutive victories over observation balloons that ended on 8 June 1918. This brought his count to ten. He ran off nine more triumphs that month, including four more balloons. He had one confirmed win, over a [[Nieuport 28]] on 2 July, and an unconfirmed claim on 8 July.<ref name=lines110/>

==Killed in action==
On 15 July, an unexpected hazard caught up with Friedrichs. His aircraft caught fire in midair. It seems most probable that the incendiary bullets loaded on his airplane spontaneously ignited and set his [[Fokker D.VII]] on fire. Friedrichs parachuted out of the conflagration, but the parachute's harness and lines entangled in the plane's tail and Friedrichs fell to his death.<ref>{{cite book |title= Pfalz Scout Aces of World War I |page= 21 }}</ref>

Friedrich Friedrichs had already been awarded the Knight's Cross with Swords of the [[Hohenzollern House Order]].<ref name=lines110/> He had also been recommended for the [[Pour le Merite]]. Reportedly, it was awarded on 20 July, five days after his death.<ref>The Aerodrome website's relevant page on Pour le Merite awards http://www.theaerodrome.com/medals/germany/prussia_opm.php?pageNum_recipients=2&totalRows_recipients=63#recipients Retrieved 2 February 2013. Note: Year that award was made says 1916, which posits an award in advance of receipt of both the Iron Cross and the House Order of Hohenzollern. Such award in contrary to regulations.</ref> However, the rolls of the order do not list him as a recipient.<ref>The Pour le Merite website http://www.pourlemerite.org/  Retrieved 2 February 2013. Note: Also contrary to German army regulations was the posthumous award of the Pour le Merite.</ref>

==Notes==
{{reflist}}

==References==

* [[Norman Franks|Franks, Norman]]; Bailey, Frank W.; Guest, Russell. ''Above the Lines: The Aces and Fighter Units of the German Air Service, Naval Air Service and Flanders Marine Corps, 1914–1918''. Grub Street, 1993. ISBN 0-948817-73-9, ISBN 978-0-948817-73-1.
* {{cite book|last=Van Wyngarden|first=Greg|title=Pfalz Scout Aces of World War 1|year=2006|publisher=Osprey Publishing|location=|isbn=978-1-84176-998-1}}

{{wwi-air}}

{{DEFAULTSORT:Friedrichs, Friedrich}}
[[Category:1895 births]]
[[Category:1918 deaths]]
[[Category:People from the Province of Westphalia]]
[[Category:German World War I flying aces]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Prussian Army personnel]]
[[Category:Luftstreitkräfte personnel]]
[[Category:Victims of aviation accidents or incidents in France]]
[[Category:German military personnel killed in World War I]]