{{Infobox journal
| title = Harvard Educational Review
| cover =
| editor =
| discipline = [[Education]]
| abbreviation = Harvard Educ. Rev.
| publisher = Harvard Education Publishing Group
| country = United States
| frequency = Quarterly
| history = 1930-present
| openaccess =
| license =
| website = http://www.hepg.org/main/her/Index.html
| LCCN = 34007870
| OCLC = 1587741
| ISSN = 0017-8055
| eISSN = 1943-5045
}}
The '''''Harvard Educational Review''''' is a [[Peer review|peer-reviewed]] [[academic journal]] of opinion and research dealing with [[education]], associated with the [[Harvard Graduate School of Education]], and published by the Harvard Education Publishing Group. The journal was established in 1930.

Since 1945, editorial decisions have been carried out by an autonomous graduate student [[editorial board]]. This student board works together to bring to publication manuscripts on a wide range of topics and from a number of disciplines.

==Alumni==
Notable alumni of the ''Harvard Educational Review'' include:
*[[Lisa Delpit]], educationalist and [[MacArthur Fellow]]
*[[Sara Lawrence-Lightfoot]], educational sociologist and [[MacArthur Fellow]]
*[[Orval Hobart Mowrer]], psychologist and former president of the [[American Psychological Association]]
*[[Lauren Resnick]], educational psychologist
*[[Theodore Sizer]], educationalist and founder of the [[Coalition of Essential Schools]]
*[[Julian Stanley]], psychologist and founder of the [[Center for Talented Youth]]

==External links==
*{{Official website|http://www.hepg.org/main/her/Index.html}}

[[Category:Harvard University academic journals]]
[[Category:Education journals]]
[[Category:Publications established in 1930]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Academic journals edited by students]]