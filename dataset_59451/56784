{{Infobox journal
| title = Epidemiologic Reviews
| cover =
| discipline = [[Epidemiology]]
| abbreviation = Epidemiol. Rev.
| editor =[[Michel A. Ibrahim]]
| publisher = [[Oxford University Press]] on behalf of the [[Johns Hopkins Bloomberg School of Public Health]]
| country =
| frequency = Annually
| history = 1979-present
| impact = 9.269
| impact-year = 2012
| website = http://epirev.oxfordjournals.org
| link1 = http://epirev.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://epirev.oxfordjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 0193-936X
| eISSN = 1478-6729
| OCLC = 476308479
| LCCN = 79007564
| CODEN = EPIRD7
}}
'''''Epidemiologic Reviews''''' is an annual [[peer-reviewed]] [[scientific journal]] covering [[epidemiology]] and published by [[Oxford University Press]] on behalf of the [[Johns Hopkins Bloomberg School of Public Health]]. The journal was established in 1979 by [[Neal Nathanson]] and [[Philip Sartwell|Philip E. Sartwell]]. The longest running [[editor-in-chief]] was Haroutune Armenian. The current editor-in-chief is Michel A. Ibrahim (Johns Hopkins Bloomberg School of Public Health).

== History ==
The journal was established by Neal Nathanson with Philip Sartwell and the help of the editorial staff at the ''[[American Journal of Epidemiology]]''.<ref name="Introduction1979">{{cite journal |author=Philip E. Sartwell |title=Introduction |journal=Epidemiologic Reviews |volume=1 |pages=1–16 |date=October 1979 |pmid=398262 |first2=DW}}</ref>

During the initial period, primarily headed by Nathanson, the journal established its credibility by soliciting pieces from well-respected epidemiologists and researchers that had also or were in the process of contributing to the ''American Journal of Epidemiology''.<ref name="Introduction1979" /> The topics broke down equally under "three general topics: infectious diseases, other conditions, and general topics related to both," according to Sartwell in his introduction of the journal in its first volume.<ref name="Introduction1979" />

In 1985, [[Moyses Szklo]] took over as editor-in-chief.<ref name="EditorialChanging2004">{{cite journal |title=Editorial: Changing of the Guard |journal=Epidemiologic Reviews |date=October 2004 |last1=Ibrahim |first1=Michel A. |volume=26 |pages=I |doi=10.1093/epirev/mxh011}}</ref> After Szklo, the journal was led by Haroutune K. Armenian, the longest serving editor-in-chief to date.<ref name="EditorialChanging2004" /> Armenian regularly obtained advice from Leon Gordis and Jon Samet.<ref name="EditorialChanging2004" /> In 1999, the journal separated from being a single 200+ page annual issue into two 100-page bi-annual issues, with the first one focusing on themes popular to public health and the second one focused on methodology. The following year, the journal switched publishers from the [[Johns Hopkins University Press]] to the Oxford University Press.

Though Armenian began working with Michel A. Ibrahim as co-editor-in-chief starting in 2003, Ibrahim's leadership did not fully commence until 2005 after Armenian had officially retired from his position.<ref name="EditorialChanging2004" />

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[BIOSIS Previews]]
* [[CAB Abstracts]]
* [[CINAHL]]
* [[Current Contents]]/Life Sciences
* [[EMBASE]]
* [[Global Health]]
* [[Index medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index]]
* [[Tropical Diseases Bulletin]]
* [[World Agricultural Economics and Rural Sociology Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 9.269, ranking it first out of 161 journals in the category "Public, Environmental & Occupational Health".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Public, Environmental & Occupational Health |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== Editors-in-chief ==
The following persons have been editor-in-chief of the journal:
{{columns-list|colwidth=30em|
*[[Philip E. Sartwell]] (1979–1982)
*[[Neal Nathanson]] (1979–1984)
*[[Moyses Szklo]] (1985–1988)
*[[Haroutune Armenian]] (1989–2004)
*[[Michel A. Ibrahim]] (2003–present)
}}

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://epirev.oxfordjournals.org}}


[[Category:Epidemiology journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Annual journals]]
[[Category:Publications established in 1979]]
[[Category:English-language journals]]
[[Category:Johns Hopkins University]]