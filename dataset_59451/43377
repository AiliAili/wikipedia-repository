<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{|{{Infobox Aircraft Begin
 | name=Pipit
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Naval [[Fighter aircraft|fighter]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Parnall|George Parnalll and Company]]
 | designer=Harold Bolas
 | first flight=Summer 1928
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Parnall Pipit''' was a single-engined, single-seat naval [[Fighter aircraft|fighter]] designed to an [[Air Ministry]] specification in 1927.  Two prototypes were built but both were destroyed by tail flutter.

==Design and development==

The Pipit<ref name="Wix">{{harvnb|Wixey|1990|pages=167–175}}</ref> was Parnall's submission to  [[List of Air Ministry Specifications#1920-1929|Air Ministry specification 21/26]], which called for a single-seat shipborne fighter.  The same specification attracted a version of the [[Vickers 141]], modified from the original landplane fighter<ref>{{harvnb|Andrews|Morgan|1988|pp=233–4}}</ref> and the private venture [[Hawker Hoopoe]]<ref>{{harvnb|Mason|1971|pp=162–5}}</ref>  The specification required operation off deck or water.<ref name="Wix"/>

The Pipit was a single-bay biplane with staggered, equal-span wings, unswept and of constant chord.  The upper wing only carried dihedral; the ailerons were on the lower wing, extending over most of the span.  There was a large rounded cutout in the upper wing over the cockpit for visibility, since the pilot's head was immediately below the trailing edge.  Because of the stagger, he sat ahead of the lower trailing edge and so there were a pair of cutouts in the lower wings, a little way out from the roots, to assist downward vision. Apart from the stainless-steel N-shaped interplane struts, the wings were duralumin structures, fabric covered.  The lower wing was fixed near the bottom of the fuselage and the upper one supported above it by four outward-leaning centre-section struts.<ref name="Wix"/>

The Pipit's fuselage was constructed from stainless steel tubes braced with duralumin struts.  The 495&nbsp;hp (370&nbsp;kW) [[Rolls-Royce Kestrel|Rolls-Royce F.XI]] watercooled V-12 was housed in a refined, streamlined aluminium cowling.  Behind the cockpit the fuselage was fabric covered, as was the empennage, which had a stainless steel structure. The fin was tall and rounded, carrying a rudder which was neither aerodynamically nor dynamically balanced.  The tailplane was attached at mid-fuselage and was rather rectangular, carrying split elevators which were horn balanced.<ref name="Wix"/>

A retractable radiator, deployed via a handwheel in the cockpit, was mounted under the nose.  When retracted this radiator warmed the cockpit and it was claimed that there would be no need for electrically heated clothing in the open cockpit, even at 20,000&nbsp;ft (6,100 m) in winter.  There were air vents for cooling the cockpit in summer and also a skin-type ancillary radiator built into the upper wing centre section surface. The land undercarriage was of the single-axle type, with forward-leaning [[oleo strut|oleo]] legs to the lower fuselage ahead of the leading edge and rearward struts.  The wheels incorporated servo-assisted brakes operated from the rudder bar.  The land- and seaplane undercarriages were designed to be rapidly interchangeable and shared the same attachment points, though the oleo legs were moved aft for the floats and there were additional bracing struts forward from their feet to the fuselage.  The floats, linked by a pair of horizontal struts, were duralumin with a single step and water rudder.<ref name="Wix"/>

The pilot's view was good, particularly over the nose, important for deck landings.  The Pipit was armed with a pair of Vickers machine guns, mounted at seat level and firing via troughs in the fuselage sides.<ref name="Wix"/>

The Pipit prototype, ''N232'', first flew sometime in the summer of 1928.  In October it flew to the [[Aeroplane and Armament Experimental Establishment|A&AAE]] at [[RAF Martlesham Heath]] for official trials.  During that month, the Pipit was put into a terminal-velocity dive during which tail [[Aeroelastic flutter|flutter]] set in and became so violent that the tailplane spar fractured.  The aircraft was a complete writeoff, but the pilot survived, despite a broken neck, to fly again.  The loss was a severe blow to Parnall's, who had staked a lot on the hope of getting a production order for the promising Pipit.  A second prototype was built, as the first but with distinguishing modifications. It was allocated the serial ''N233'' but, confusingly, was painted with the same serial as the first prototype.  The tailplane was now braced on each underside with a pair of struts, the fin and rudder had a less smoothly-rounded shape largely because of the addition of a large horn balance and there were now ailerons on all wings, linked between upper and lower planes by an external rod.  This aircraft began testing at the Parnall factory field at [[Yate]] early in 1929, but on 24 February<ref>Mason gives this date. Wixey gives two different ones, both in February: 14th (p.230) and 17th (p.173)</ref> the Pipit encountered flutter strong enough to break the fin and rudder from the fuselage.  The aircraft was lost, but the pilot escaped by parachute.<ref name="Wix"/><ref>Mason, Francis K., "The British Fighter since 1912", Naval Institute Press, Annapolis, Maryland, 1992, Library of Congress card number 92-63026, ISBN 1-55750-082-7, p. 207.</ref>

After the failure of the Pipit, Parnall never received a production order for a military aircraft and never submitted a front-line prototype again, though they did compete for the trainer specification [[List of Air Ministry Specifications#1930-1939|Air Ministry specification T.1/37]] with the [[Parnall 382|Parnall Heck III]]. The Pipit crashes did contribute to a better understand of flutter and how to prevent it, with wind-tunnel models and a detailed study of the two cases published in 1930.  A subsequent and more general report came out of a research program which reported in 1931 with an emphasis on structural stiffness and above all careful mass (dynamic) balancing.<ref name="Wix"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref={{harvnb|Wixey|1990|p=175}}<!-- reference -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=
|length ft=26
|length in=0
|length note=
|span m=
|span ft=35
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=10
|height in=5.5
|height note=
|wing area sqm=
|wing area sqft=361
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=3050
|empty weight note=
|gross weight kg=
|gross weight lb=3980
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rolls-Royce Kestrel|Rolls-Royce F.XI]]
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=495<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=173
|max speed kts=
|max speed note=at 3,000 ft (914 m)
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=10,000 ft (3,048 m) 7.5 min
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|guns= 2× 0.303 (7.7 mm) fixed, forward firing [[Vickers machine guns]]
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{commons category|Parnall}}

===Citation===
{{reflist}}

===Cited sources===
*{{cite book |title= Vickers Aircraft since 1908 |last= Andrews |first= CF |last2=Morgan|first2= E.B. |edition= 2nd |year= 1988|publisher= Putnam|location= London|isbn= 0-85177-815-1|ref=harv}}
*{{cite book |title= Hawker Aircraft since 1920|last= Mason |first= Francis K |coauthors= |edition= 2nd|year= 1971|publisher= Putnam|location= London|isbn= 0-370-00066-8|ref=harv}}
*{{cite book |title= Parnall Aircraft since 1914|last=Wixey|first=Kenneth |coauthors= |edition= |year= 1990|publisher= Naval Institute Press|location=Annapolis|isbn= 1-55750-930-1|ref=harv}}

{{refbegin}}
<!-- insert the reference sources here -->
{{refend}}

<!-- ==External links== -->
{{Parnall aircraft}}

[[Category:British fighter aircraft 1920–1929]]
[[Category:Parnall aircraft|Pipit]]
[[Category:Carrier-based aircraft]]