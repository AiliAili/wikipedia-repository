{{Good article}}
{{Infobox military conflict
|conflict=Action of 24 March 1811
|partof=the [[Napoleonic Wars]]
|image=<div style="position: relative">[[File:Basse-Normandie region location map.svg|250px]]{{Image label|x=0.340|y=0.025|scale=250|text=[[Image:Red pog.svg|8px|Action]]}}</div>
|caption=Map of [[Lower Normandy]]<br />Location of the destruction of ''Amazone''
|date=24–25 March 1811
|place=near [[Phare de Gatteville]], [[Normandy]]
|result=British victory
|combatant1={{flagcountry|United Kingdom}}
|combatant2={{flagicon|France}} [[First French Empire|French Empire]]
|commander1=[[James Macnamara]]
|commander2=[[Bernard-Louis Rosseau]]
|strength1=[[Ship of the line]] [[HMS Berwick (1809)|HMS ''Berwick'']], [[frigates]] [[HMS Amelia (1796)|HMS ''Amelia'']], [[HMS Niobe (1800)|HMS ''Niobe'']], [[brig|brig-sloops]] [[HMS Goshawk (1806)|HMS ''Goshawk'']], [[HMS Hawk (1806)|HMS ''Hawk'']]
|strength2=[[Frigate]] [[French frigate Amazone (1807)|''Amazone'']]
|casualties1=Two killed, one wounded
|casualties2=''Amazone'' destroyed
|campaignbox=
}}{{Coord|49|41|38.12|N|1|16|39.31|W|type:event|display=title}}

The '''Action of 24 March 1811''' was a minor naval engagement of the [[Napoleonic Wars]], fought as part of the [[Royal Navy]] [[blockade]] of the French [[English Channel]] ports. By 1811, Royal Navy control of the French coast was so entrenched that French ships were unable to travel safely even in French territorial waters. In late 1810, French frigates [[French frigate Elisa (1808)|''Elisa'']] and [[French frigate Amazone (1807)|''Amazone'']] sailed from [[Le Havre]] to join with a larger squadron at [[Cherbourg]], but were intercepted by a British frigate squadron and forced to shelter at [[Saint-Vaast-la-Hougue]]. There they came under sustained attack and ''Elisa'' was destroyed, ''Amazone'' successfully slipping back to Le Havre under cover of darkness. To prevent ''Amazone'' from escaping once more, the British blockade squadron was reinforced.

On the evening of 23 March 1811, ''Amazone'' left Le Havre once more, sailing west towards Cherbourg through the night. Escaping the ships watching Le Havre, ''Amazone'' was sighted at dawn on 24 March weathering [[Cape Barfleur]] by [[ship of the line]] [[HMS Berwick (1809)|HMS ''Berwick'']], which pursued the French frigate into a bay {{convert|1|nmi|km}} west of the [[Phare de Gatteville]] lighthouse. There ''Berwick'', reinforced by a squadron of smaller ships, attacked ''Amazone'' but was unable to approach through the rocks and shoals of the coast. Plans were made overnight to attack the frigate with [[ship's boat]]s, but on the following day the French Captain [[Bernard-Louis Rosseau]] set his ship on fire to prevent its capture.

==Action==
By the autumn of 1810 the [[Napoleonic Wars]] had lasted for seven years and the [[French Navy]], over the course of the conflict, had been successively driven from the Atlantic until every French port was watched by a [[Royal Navy]] close [[blockade]], ready to attack any French ship which emerged from harbour. In 1809, the main French fleet at [[Brest, France|Brest]] had attempted to break out into open water, only to be driven back and defeated at the [[Battle of Basque Roads]]. Much of the French effort at sea subsequently fell on commerce raiders, including [[privateers]] and [[frigate]] squadrons, often operating from smaller harbours such as those on the Northern coast of France in the [[English Channel]].<ref name="RG111">Gardiner, p. 111</ref> The two principal raiding ports were [[Cherbourg]] and [[Le Havre]] in [[Normandy]], each of which maintained squadrons. In 1810, Cherbourg's anchorage held two [[ships of the line]] and a large, newly built frigate [[French frigate Iphigénie (1810)|''Iphigénie'']], while the frigates [[French frigate Elisa (1808)|''Elisa'']] and [[French frigate Amazone (1807)|''Amazone'']] were stationed at Le Havre.<ref name="WJ239">James, p. 239</ref>

Both ports were watched closely by a blockade squadron detached from the [[Channel Fleet]], including ships of the line off Cherbourg and two frigates, [[HMS Diana (1794)|HMS ''Diana'']] and [[HMS Niobe (1800)|HMS ''Niobe'']], off Le Havre. On 12 November 1810 ''Elisa'' and ''Amazone'' attempted to break out of Le Havre and join with the squadron at Cherbourg, slipping past the blockade in the darkness. Spotted in the early hours of 13 November, the frigates managed to anchor at the well-defended harbour of  [[Saint-Vaast-la-Hougue]], where on 15 November they [[Action of 15 November 1810|were attacked]] by the combined blockade forces from Cherbourg and Le Havre. Although the attack was beaten back, ''Elisa'' was too badly damaged to continue the mission and on 27 November ''Amazone'' successfully returned to Le Havre without encountering the British forces. ''Elisa'' was subsequently driven onshore and destroyed by the British squadron.<ref name="WLC474">Clowes, p. 474</ref>

===''Amazone''<nowiki>'</nowiki>s journey===
[[File:John Christian Schetky, HMS Amelia Chasing the French Frigate Aréthuse 1813 (1852).jpg|thumb|upright=1.1|right|''HMS Amelia'', [[John Christian Schetky]], 1852, [[Norwich Castle]]]]
''Amazone'', commanded by Captain [[Bernard-Louis Rosseau]], attempted to sail to Cherbourg again on 23 March 1811. On this occasion Rosseau successfully evaded the blockade of Le Havre and by dawn on 24 March was weathering the point of [[Cape Barfleur]].<ref name="WJ332"/> As the frigate passed the [[Phare de Gatteville]] lighthouse, only a few miles from her destination, she was spotted by a British ship of the line recently sailed from the British fleet anchorage at [[St Helens, Isle of Wight|St Helens]], the 74-gun [[HMS Berwick (1809)|HMS ''Berwick'']] under Captain [[James Macnamara]], then sailing approximately {{convert|12|nmi|km}} offshore. Macnamara took ''Berwick'' in pursuit, seeking to cut off Rosseau's advance, and the French captain managed to evade ''Berwick'' by taking shelter in a small bay {{convert|1|nmi|km}} west of the lighthouse.<ref name="LG1">{{London Gazette|issue=16469|startpage=573|endpage=|date=26 March 1811}}</ref>

Navigation through the rocks of the bay had been difficult, and as she entered the bay ''Amazone''<nowiki>'</nowiki>s [[rudder]] had been torn away, rendering the ship unmanoeuverable. With ''Amazone'' trapped, Macnamara called up the rest of the Cherbourg squadron, the frigate [[HMS Amelia (1796)|HMS ''Amelia'']] under Captain [[Frederick Paul Irby]] and the [[brig|brig-sloops]] [[HMS Goshawk (1806)|HMS ''Goshawk'']] under Commander James Lilburn and [[HMS Hawk (1806)|HMS ''Hawk'']] under Commander Henry Bourchier. This force was required to remain a substantial distance offshore as the rocky coastline posed a considerable danger to the British ships. Macnamara's plan was to wait for high tide and then bring the squadron closer inshore to bombard the French frigate into surrender.<ref name="WJ332">James, p. 332</ref> As they waited, the squadron was joined by ''Niobe'' from the Le Havre under Captain [[John Wentworth Loring]]. At 16:00 the tides were optimal for the attack and ''Niobe'', ''Amelia'' and ''Berwick'' sailed into the bay. However, Rosseau had situated ''Amazone'' in a strong position, protected from close attack by rocks and shoals and in consequence the British ships could only fire as they [[Jibe (sailing)|wore around]], resulting in scattered and inaccurate fire. As they advanced, the British ships came under fire from ''Amazone'', which killed one sailor on ''Berwick'' and killed one and wounded another on ''Amelia'', while the British fire had no effect on the French ship. At 18:00, Macnamara withdrew his ships from the bay, all three vessels having suffered considerable damage to their rigging and sails from ''Amazone''<nowiki>'</nowiki>s shot.<ref name="LG1"/>

===Aftermath===
Macnamara considered plans to use [[ship's boats]] to attack ''Amazone'' directly, but resolved to attempt to enter  the bay again on the morning of 25 March. As dawn broke however it became apparent that Rosseau had abandoned his damaged ship with his crew, setting the frigate on fire as he departed. By the end of the day ''Amazone'' had been burnt to the waterline and destroyed.<ref name="LG1"/> Macnamara's squadron returned to their blockade duties off Cherbourg. The remaining frigate in the region, ''Iphigénie'', did eventually succeed in breaking out of Cherbourg, but was intercepted and captured in the Atlantic during a raiding mission in January 1814.<ref name="WLC544">Clowes, p. 544</ref>

== Notes ==
{{Reflist|2}}

== References ==
* {{cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997 |origyear=1900
 | chapter =
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume V
 | publisher = Chatham Publishing
 | location =
 | isbn = 1-86176-014-0
}}
* {{cite book
 | last =  Gardiner (ed.)
 | first = Robert
 | authorlink =
 | year = 2001 |origyear=1998
 | chapter =
 | title = The Victory of Seapower
 | publisher = Caxton Editions
 | location =
 | isbn = 1-84067-359-1
}}
* {{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002
 | origyear= 1827
 | chapter =
 | title = The Naval History of Great Britain, Volume 5, 1808–1811
 | publisher = Conway Maritime Press
 | location = London
 | isbn = 0-85177-909-3
}}

[[Category:Naval battles involving France]]
[[Category:Naval battles involving the United Kingdom]]
[[Category:Naval battles of the Napoleonic Wars]]
[[Category:Conflicts in 1811]]
[[Category:March 1811 events]]