{{Infobox airport
|name         = Riga International Airport
|nativename   = <small>''{{lang|lv|Starptautiskā lidosta "Rīga"}}''</small>
|image        = RIX Logo.svg
|image-width  = 200
|image2       = Riga Airport 2016.jpg
|image2-width = 250
|IATA         = RIX
|ICAO         = EVRA
|type         = Public
|owner        = [[Government of Latvia]]
|operator     = [[TAV Airports]]
|city-served  = [[Riga]], [[Latvia]]
|location     = [[Mārupe municipality]]
|hub          = 
* [[airBaltic]]
* [[Norwegian Air Shuttle]]
* [[Primera Air]]
* [[SmartLynx Airlines]]
* [[Wizz Air]]
|elevation-f  = 36
|elevation-m  = 11
|coordinates  = {{coord|56|55|25|N|023|58|16|E|region:LV|display=inline,title}}
|website      = [http://www.riga-airport.com riga-airport.com]
| pushpin_map            = Latvia
| pushpin_label_position =
| pushpin_label          = RIX
| pushpin_map_alt        =
| pushpin_mapsize        =
| pushpin_image          =
| pushpin_map_caption    = Location of the airport by Riga
|metric-rwy   = y
|r1-number    = 18/36
|r1-length-m  = 3,200
|r1-length-f  = 10,500
|r1-surface   = [[Concrete]]/[[Asphalt]]
|stat-year    = 2016
|stat1-header = Number of Passengers
|stat1-data   = 5,400,243
| stat2-header = Aircraft movements
| stat2-data   = 68,061
|footnotes    = Source (excluding statistics): [[Aeronautical Information Publication|AIP]] at [[European Organisation for the Safety of Air Navigation|EUROCONTROL]]<ref name="AIP">[http://www.ead.eurocontrol.int/eadbasic/eais-EE94E17C273BC637BA060D5232D51B79/ZXRGPYDH5FN6M/EN/2009-07-02-AIRAC/html/index.html?show=eAIP/EV-AD-2.EVRA-en-GB.html EAD Basic]</ref>
}}

'''Riga International Airport''' ({{lang-lv|Starptautiskā lidosta "Rīga"}}; {{airport codes|RIX|EVRA|p=n}}) is the [[international airport]] of [[Riga]], the capital of [[Latvia]], and the largest airport in the [[Baltic state]]s with direct flights to over 90 destinations in 30 countries. It serves as a hub for [[airBaltic]], [[SmartLynx Airlines]], [[RAF-Avia]], Vip Avia and [[Inversija]] and as one of the base airports for [[Wizz Air]]. Latvian national carrier airBaltic is the biggest in the airport, followed by [[Ryanair]].

The airport is located in the [[Mārupe municipality]]<ref name="AirBalticHQ">"[http://www.airbaltic.com/public/offices_and_representatives.html airBaltic in Riga]." [[AirBaltic]]. Retrieved on 16 January 2010. "Air Baltic Corporation AS Registration number: 40003245752 ADMINISTRATION RIGA INTERNATIONAL AIRPORT Marupes county, LV-1053, Latvia"</ref> {{convert|5.4|NM|abbr=on|lk=in}} west<ref name="AIP"/> of Riga and is a state-owned [[Joint stock company|joint-stock company]], with the owner of all shares being the [[government of Latvia]]. The holder of the state capital share is Latvia's [[Ministry of Transport (Latvia)|Ministry of Transport]].

==History==
The airport was built in 1973 as an alternative to [[Spilve Airport]], which had become outdated.

Renovation and modernization of the airport was completed in 2001, coinciding with the 800th anniversary of the founding of the city. In 2006 and 2016, the new north terminal extensions were opened. The airport has three terminals: A & B for [[Schengen Agreement|Schengen]] and C for both [[Schengen Agreement|Schengen]] and non-Schengen departures. Arrivals 1, in terminal A, handles the Schengen arrivals, while Arrivals 2, in terminal C, handles the non-Schengen arrivals.<ref>[http://www.riga-airport.com/media/image/plansNEWen.jpg Riga Airport Plan]</ref> A maintenance, repair and overhaul facility was opened in the autumn of 2006, to be run as a joint venture between two local companies: Concors and SR-Technik. The airport has [[Instrument landing system#ILS categories|ILS CAT II]].<ref>{{cite web|url=http://www.riga-airport.com/en/main/b2b/aviation/airlines/operational-facilities|title=Operational Facilities|publisher=|accessdate=3 June 2015}}</ref>

The airport is owned by the Republic of Latvia via the Ministry of Transport of the Republic of Latvia.<ref>{{cite web|title=Airport at a glance|url=http://www.riga-airport.com/en/main/about-company|website=Riga Airport Website}}</ref>

==Airlines and destinations==
The following airlines operate regular scheduled and charter flights to and from Riga:<ref>[http://www.riga-airport.com/en/main/flights/flight_schedules riga-airport.com - Flight schedules] retrieved 16 October 2016</ref>

{{Airport-dest-list
<!--  -->
|[[Aegean Airlines]]| '''Seasonal:''' [[Athens International Airport|Athens]]
<!-- -->
| [[Aeroflot]] | [[Sheremetyevo International Airport|Moscow–Sheremetyevo]]
<!-- -->
| [[airBaltic]] | [[Abu Dhabi International Airport|Abu Dhabi]] (resumes 29 October 2017), [[Amsterdam Airport Schiphol|Amsterdam]], [[Barcelona-El Prat Airport|Barcelona]], [[Berlin Tegel Airport|Berlin–Tegel]], [[Billund Airport|Billund]], [[Brussels Airport|Brussels]], [[Budapest Ferenc Liszt International Airport|Budapest]], [[Copenhagen Airport|Copenhagen]], [[Düsseldorf Airport|Düsseldorf]], [[Frankfurt Airport|Frankfurt]], [[Geneva Airport|Geneva]] (resumes 4 May 2017), [[Göteborg Landvetter Airport|Gothenburg]] (resumes 1 June 2017), [[Hamburg Airport|Hamburg]], [[Helsinki Airport|Helsinki]], [[Kazan International Airport|Kazan]] (begins {{date|2017-4-26}}),<ref>{{cite web|title=airBaltic to Launch Riga – Kazan {{!}} airBaltic|url=https://www.airbaltic.com/en/airbaltic-to-launch-riga-kazan|website=www.airbaltic.com|publisher=airBaltic|accessdate=7 February 2017|language=en}}</ref> [[Boryspil International Airport|Kiev–Boryspil]], [[Larnaca International Airport|Larnaca]], [[Liepāja International Airport|Liepāja]] (resumes {{date|2017-5-16}}),<ref>{{cite news|title="airBaltic'' maijā uzsāks lidojumus Rīga-Liepāja; biļetes - sākot no 15 eiro|url=http://nra.lv/latvija/regionos/203557-airbaltic-maija-uzsaks-lidojumus-riga-liepaja-biletes-sakot-no-15-eiro.htm|accessdate=13 March 2017|work=nra.lv|date=13 March 2017}}</ref> [[Gatwick Airport|London–Gatwick]], [[Malpensa Airport|Milan–Malpensa]], [[Minsk National Airport|Minsk]], [[Sheremetyevo International Airport|Moscow–Sheremetyevo]], [[Munich Airport|Munich]], [[Oslo Airport, Gardermoen|Oslo–Gardermoen]], [[Palanga International Airport|Palanga]], [[Charles de Gaulle Airport|Paris–Charles de Gaulle]], [[Prague Václav Havel Airport|Prague]], [[Leonardo da Vinci–Fiumicino Airport|Rome–Fiumicino]], [[Pulkovo Airport|Saint Petersburg]], [[Stavanger Airport|Stavanger]] (resumes 2 May 2017), [[Stockholm Arlanda Airport|Stockholm–Arlanda]], [[Tallinn Airport|Tallinn]], [[Tampere–Pirkkala Airport|Tampere]], [[Tbilisi Airport|Tbilisi]], [[Ben Gurion International Airport|Tel Aviv–Ben Gurion]], [[Turku Airport|Turku]], [[Vienna International Airport|Vienna]], [[Vilnius Airport|Vilnius]], [[Warsaw Chopin Airport|Warsaw–Chopin]], [[Zürich Airport|Zürich]] <br>'''Seasonal:''' [[Aberdeen Airport|Aberdeen]] (resumes 2 May 2017),<ref>http://www.routesonline.com/news/38/airlineroute/269187/air-baltic-plans-new-routes-in-s17/</ref> [[Athens Elefthérios Venizélos International Airport|Athens]], [[Heydar Aliyev International Airport|Baku]], [[Burgas Airport|Burgas]], [[Catania–Fontanarossa Airport|Catania]] (begins 4 May 2017), [[Dubrovnik Airport|Dubrovnik]], [[Nice Côte d'Azur Airport|Nice]], [[Odessa International Airport|Odessa]], [[Olbia Costa Smeralda Airport|Olbia]], [[Palma de Mallorca Airport|Palma de Mallorca]], [[Poprad-Tatry Airport|Poprad–Tatry]], [[Keflavík International Airport|Reykjavík–Keflavík]], [[Adolfo Suárez Madrid–Barajas Airport|Madrid]] (begins 26 May 2017), [[Rhodes Airport|Rhodes]], [[Rijeka Airport|Rijeka]], [[Salzburg Airport|Salzburg]], [[Thessaloniki International Airport|Thessaloniki]], [[Venice Marco Polo Airport|Venice]], [[Verona Villafranca Airport|Verona]] <br>'''Charter:''' [[Burgas Airport|Burgas]], [[Dalaman Airport|Dalaman]], [[Heraklion International Airport|Heraklion]]
<!-- -->
| [[Belavia]] | [[Minsk National Airport|Minsk]]
<!-- -->
|[[Ellinair]] | '''Seasonal:''' [[Makedonia Airport|Thessaloniki]] <br> '''Seasonal charter:''' [[Corfu International Airport|Corfu]]
<!-- -->
| [[Finnair]]<br>{{nowrap|operated by [[Nordic Regional Airlines]]}}| [[Helsinki Airport|Helsinki]]
<!-- -->
| [[LOT Polish Airlines]] | [[Warsaw Chopin Airport|Warsaw–Chopin]]
<!-- -->
| [[Lufthansa]] | [[Frankfurt Airport|Frankfurt]]
<!-- -->
| {{nowrap|[[Norwegian Air Shuttle]]}} | [[Copenhagen Airport|Copenhagen]], [[Oslo Airport, Gardermoen|Oslo–Gardermoen]], [[Stockholm Arlanda Airport|Stockholm–Arlanda]], [[Trondheim Airport, Værnes|Trondheim]]
<!-- -->
| [[RusLine]] | [[Moscow-Domodedovo]] (begins 29 May 2017)<ref>[http://www.routesonline.com/news/38/airlineroute/271502/rusline-s17-european-route-additions/ RusLine begin operations from Moscow to Riga from May 2017]</ref>
<!-- -->
| [[Ryanair]] | [[Orio al Serio Airport|Bergamo]], [[Berlin Schönefeld Airport|Berlin-Schönefeld]], [[Bremen Airport|Bremen]], [[Brussels South Charleroi Airport|Charleroi]], [[Cologne Bonn Airport|Cologne/Bonn]], [[Dublin Airport|Dublin]], [[East Midlands Airport|East Midlands]], [[Glasgow Airport|Glasgow]], [[Frankfurt Hahn Airport|Hahn]], [[Leeds Bradford Airport|Leeds/Bradford]], [[London Stansted Airport|London–Stansted]], [[Malta Airport|Malta]] (begins October 2017), [[Manchester Airport|Manchester]]
<!-- -->
| [[Scandinavian Airlines]] | [[Copenhagen Airport|Copenhagen]], [[Stockholm Arlanda Airport|Stockholm–Arlanda]]
<!-- -->
| {{nowrap|[[SmartLynx Airlines]]}}| '''Charter:''' [[Antalya Airport|Antalya]], [[Orio al Serio Airport|Bergamo]], [[Burgas Airport|Burgas]], [[Gran Canaria Airport|Gran Canaria]], [[Heraklion International Airport|Heraklion]], [[Hurghada International Airport|Hurghada]], [[Monastir Habib Bourguiba International Airport|Monastir]], [[Rijeka Airport|Rijeka]], [[Ras Al Khaimah International Airport|Ras Al Khaimah]], [[Sharm el-Sheikh International Airport|Sharm el-Sheikh]], [[Tenerife South Airport|Tenerife–South]], [[Varna Airport|Varna]] 
<!-- -->
| [[Turkish Airlines]] | [[Istanbul–Atatürk]]
<!-- -->
| {{nowrap|[[Ukraine International Airlines]]}} | [[Kiev–Boryspil]]
<!-- -->
| [[UTair Aviation]] | [[Vnukovo International Airport|Moscow–Vnukovo]]
<!-- -->
| [[Uzbekistan Airways]] | [[New York–JFK]], [[Tashkent International Airport|Tashkent]]
<!-- --> 
| [[Wizz Air]] |[[Barcelona–El Prat Airport|Barcelona]], [[Bari Airport|Bari]] (begins 25 June 2017), [[Bergen Airport, Flesland|Bergen]], [[Robin Hood Airport Doncaster Sheffield|Doncaster/Sheffield]], [[Dortmund Airport|Dortmund]], [[Eindhoven Airport|Eindhoven]], [[Liverpool Airport|Liverpool]], [[London–Luton]], [[Keflavík International Airport|Reykjavík–Keflavík]] (begins 25 June 2017), [[Sandefjord Airport, Torp|Sandefjord]], [[Ben Gurion International Airport|Tel Aviv–Ben Gurion]] 
}}

==Statistics==
===Route statistics===
<center>
{| class="wikitable" style="font-size: 100%" width=align=
|+ '''Busiest routes from Riga (2016)<ref>[http://www.riga-airport.com/uploads/files/Partneriem/aviacija/statistika/2016/12_RIX_Statistics%202016_DEC.pdf]</ref>'''
|-
! Rank
! City
! Passengers
! Airlines
|-
| 1
| {{flagicon|UK}} [[London]]
| 540,124
| airBaltic, Ryanair, Wizz Air
|-
| 2
| {{flagicon|Russia}} [[Moscow]]
| 442,902
| Aeroflot, airBaltic, UTair Aviation
|-
| 3
| {{flagicon|Norway}} [[Oslo]]
| 280,865
| airBaltic, Norwegian Air Shuttle, Wizz Air
|-
| 4
| {{flagicon|Germany}} [[Frankfurt]]
| 280,865
| airBaltic, Lufthansa, Ryanair
|-
| 5
| {{flagicon|Finland}} [[Helsinki]]
| 259,260
| airBaltic, Finnair
|-
| 6
| {{flagicon|Germany}} [[Berlin]]
| 232,253
| airBaltic, Ryanair
|-
| 7
| {{flagicon|Sweden}} [[Stockholm]]
| 232,253
| airBaltic, Norwegian Air Shuttle, Scandinavian
|-
| 8
| {{flagicon|Estonia}} [[Tallinn]]
| 205,247
| airBaltic
|-
| 9
| {{flagicon|Lithuania}} [[Vilnius]]
| 167,438
| airBaltic
|-
| 10
| {{flagicon|Denmark}} [[Copenhagen]]
| 151,235
| airBaltic, Norwegian Air Shuttle
|}
</center>

===Annual passenger numbers at Riga Airport (millions)===
<center>
{| style="border:solid 0px #aaa;" cellpadding="10" cellspacing="0"
|+ ''' '''
|-
| <timeline>
ImageSize = width:auto height:250 barincrement:29
PlotArea = left:30 bottom:15 top:10 right:15
AlignBars = late
Period = from:0 till:6
TimeAxis = orientation:vertical

Colors =
 id:gray    value:gray(0.5)
  id:line1                 value:gray(0.9)
  id:line2                 value:gray(0.7)

ScaleMajor = start:0 increment:1 gridcolor:line2

PlotData=
 color:blue width:20
 bar:1993 from:start till:0.2
 bar:1994 from:start till:0.398
 bar:1995 from:start till:0.504
 bar:1996 from:start till:0.506
 bar:1997 from:start till:0.535
 bar:1998 from:start till:0.555
 bar:1999 from:start till:0.562
 bar:2000 from:start till:0.574
 bar:2001 from:start till:0.623
 bar:2002 from:start till:0.633
 bar:2003 from:start till:0.712
 bar:2004 from:start till:1.060
 bar:2005 from:start till:1.878
 bar:2006 from:start till:2.495
 bar:2007 from:start till:3.161
 bar:2008 from:start till:3.691
 bar:2009 from:start till:4.066
 bar:2010 from:start till:4.664
 bar:2011 from:start till:5.107
 bar:2012 from:start till:4.768
 bar:2013 from:start till:4.793
 bar:2014 from:start till:4.814
 bar:2015 from:start till:5.162
 bar:2016 from:start till:5.401
 color:darkblue
 bar:2017 from:start till:1.133
</timeline>
|-
| Updated: 1 April 2017
|}
</center>

==Ground transportation==
[[File:Check-In at Riga Airport.jpg|thumb|Check-in]]
[[File:Bus stop at Riga Airport.jpg|thumb|Bus stop at Riga Airport]]

===Bus===
Riga Airport is accessible by bus line 22, operated by [[Rīgas Satiksme]], which runs between Riga city centre and the airport. Moreover, there are international bus connections from the airport to cities in [[Estonia]], [[Lithuania]], [[Poland]], [[Germany]], [[Russia]] and [[Belarus]].

===Car===
Riga Airport can be reached by car via the highway P133 which connects the airport with [[European route E22]]. The airport has 3 car parking areas, with ~1500 parking spaces, offering both short- and long-term parking.

==Other facilities==
* [[AirBaltic]] has its head office on the property of Riga Airport.<ref name="AirBalticHQ"/>
* The [[Latvian Civil Aviation Agency]] also has its head office at Riga Airport.<ref>"[http://www.caa.lv/en/contacts Contacts]." [[Latvian Civil Aviation Agency]]. Retrieved on 19 January 2012. "Civil aviation agency Address: Airport "Riga", LV-1053, Latvia"</ref>

==Incidents and accidents==
* On 17 September 2016 an [[airBaltic]] [[Bombardier Aerospace|Bombardier]] [[Bombardier Dash 8#Series 400|Dash 8 Q400 NextGen]] aircraft made an emergency landing on the runway of Riga International Airport without its nose landing gear deployed. The plane was carrying 63 passengers and 4 crewmembers and was forced to return to Riga International Airport following issues with its front chassis. The runway was closed between 10:26 and 15:55 as a safety precaution following an emergency landing. Seven inbound flights and four outbound flights were cancelled, 17 flights were diverted to [[Tallinn Airport]] and [[Kaunas Airport]] and others were delayed. The aircraft involved was YL-BAI and the flight BT 641 was scheduled to fly from Riga to [[Zürich Airport]]. No injuries were reported.<ref>{{cite news |title=airBaltic flight makes emergency landing at Riga airport |url=http://www.baltic-course.com/eng/good_for_business/?doc=124129 |work=The Baltic Course |date=17 September 2016 |accessdate=21 September 2016}}</ref><ref>{{cite news |date=17 September 2016 |title=17 flights diverted today due to closed runway at Riga Airport |url=http://www.baltictimes.com/17_flights_diverted_today_due_to_closed_runway_at_riga_airport/ |newspaper=The Baltic Times |access-date=21 September 2016}}</ref><ref>{{cite news |title=Incidents ar 'airBaltic' lidmašīnu ietekmējis 1341 kompānijas pasažieri |url=http://www.delfi.lv/news/national/politics/incidents-ar-airbaltic-lidmasinu-ietekmejis-1341-kompanijas-pasazieri.d?id=47920679 |publisher=www.delfi.lv |language=Latvian |date=17 September 2016 |accessdate=21 September 2016}}</ref>
* On 17 February 2017 a [[VIM Airlines]] charter flight to [[Ufa]], [[Russia]] slid off the runway during take-off. The plane was carrying 40 passengers and 7 crew members. No injuries were reported. Aircraft's engine was damaged as it hit airport equipment. The runway was checked and closed for three hours after incident. Flights were diverted to [[Tallinn Airport]] and [[Kaunas Airport]] and others were delayed. The Transport Accident and Incident Investigation Bureau of Latvia started an investigation.

==See also==
* [[List of the busiest airports in Europe]]
* [[List of largest airports in the Baltic states]]
* [[List of the busiest airports in the former USSR]]
* [[List of airports in Latvia]]
* [[Transportation in Latvia]]
* [[Rīgas Satiksme]] (Riga Public Transport)

==References==
{{reflist}}

==External links==
{{Commonscat-inline|Riga International Airport}}
*{{official website|http://www.riga-airport.com}}
*[http://bestriga.com/en/page/expanded/type/articles/gpart/5/object/94 RIX Marks the Spot for Expansion]
*{{NWS-current|EVRA}}
*{{ASN|RIX}}

{{Portalbar|Latvia|Aviation}}
{{Airports in Latvia}}
{{Transport hubs of Latvia}}
{{Use dmy dates|date=May 2014}}
{{Airports built in the Soviet Union}}

[[Category:Airports built in the Soviet Union]]
[[Category:Airports in Riga]]
[[Category:Airports in Latvia]]
[[Category:Airports established in 1973]]
[[Category:1973 establishments in the Soviet Union]]
[[Category:1973 establishments in Latvia]]