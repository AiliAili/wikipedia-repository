{{Hatnote|"TCMOS" redirects here.  See also [[CMOS (disambiguation)]].}}
{{Italic title}}
[[File:CMOS 1906 Edition.png|thumb|250px|right|Title page of the first edition of the ''Chicago Manual of Style'' (1906)]]
{{styles}}
[[File:The Chicago Manual of Style 16th edition.gif|thumb|right|''The Chicago Manual of Style'', sixteenth edition]]

'''''The Chicago Manual of Style''''' (abbreviated in writing as '''''CMS''''' or '''''CMOS''''' [the version used on its website], or, by some writers as '''''Chicago''''') is a [[style guide]] for [[American English]] published since 1906 by the [[University of Chicago Press]]. Its sixteen editions have prescribed writing and [[citation]] styles widely used in publishing. It is "one of the most widely used and respected style guides in the United States."<ref name="spencer2011">David Spencer, "[http://typedesk.com/2011/02/15/chicago-manual-of-style-16th-edition-2 Chicago Manual of Style, 16th Edition]", Type Desk, February 15, 2011. Accessed March 16, 2011.</ref> ''CMOS'' deals with aspects of editorial practice, from American English grammar and use for document preparation.

==Availability and usage==
''The Chicago Manual of Style'' is published in hardcover and online. The online edition includes the searchable text of both the fifteenth and sixteenth—its most recent—editions with features such as tools for editors, a citation guide summary, and searchable access to a Q&A, where University of Chicago Press editors answer readers' style questions. ''The Chicago Manual of Style'' also discusses the parts of a book and the editing process. An annual subscription is required for access to the online content of the ''Manual''. (Access to the Q&A, however, is free, as well as to various editing tools.)

''The Chicago Manual of Style'' is used in some social science publications and most historical journals. It remains the basis for the ''Style Guide of the [[American Anthropological Association]]'' and the ''Style Sheet'' for the [[Organization of American Historians]]. Many small publishers throughout the world adopt it as their style.

''The Chicago Manual of Style'' includes chapters relevant to publishers of books and journals. It is used widely by academic and some trade publishers, as well as editors and authors who are required by those publishers to follow it. Kate L. Turabian's ''[[A Manual for Writers of Research Papers, Theses, and Dissertations]]'' is based on the ''Manual''.

''Chicago'' style offers writers a choice of several different formats. It allows the mixing of formats, provided that the result is clear and consistent. For instance, the fifteenth edition of ''The Chicago Manual of Style'' permits the use of both in-text [[Citation#Citation systems|citation systems]] and/or [[Note (typography)|footnotes or endnotes]], including use of "content notes"; it gives information about in-text citation by page number (such as [[The MLA Style Manual|MLA style]]) or by year of publication (like [[APA style]]); it even provides for variations in styles of footnotes and endnotes, depending on whether the paper includes a full bibliography at the end.
<ref>{{cite web |title=Why Are There Diﬀerent Citation Styles? |url= http://ctl.yale.edu/writing/using-sources/why-are-there-different-citation-styles |publisher=Yale College Writing Center |accessdate=2014-06-23}}</ref>

==Citation styles==
Two types of citation styles are provided.  In both cases, two parts are needed.  First, notation in the text, which indicates that the information immediately preceding was from another source, and second, the full citation, which is placed at another location.

===Author-date style=== 
Using author-date style,{{efn|As used with periodical/journal articles.}} the sourced text is indicated parenthetically with the last name(s) of the author(s) and the year of publication with no intervening punctuation.<ref name=cmos4>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=15.21|isbn=978-0226104201|edition=16th|chapter=Author-Date References: Text Citations--Basic Form}}</ref>

 Research has found that students do not always cite their work properly (Smith 2016).

When page numbers are used, they are placed along with the author's last name and date of publication ''after'' an interposed comma.<ref name=cmos4 />  
     
 Research has found that students do not always cite their work properly (Smith 2016, 24).

If the author's name is used in the text, only the date of publication need be cited parenthetically (with or without the page number).<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=15.5|isbn=978-0226104201|edition=16th|chapter=The Author-Date System: An Overview}}</ref>

 Research done by Smith found that students do not always cite their work properly (2016).

In-text citations are usually placed just inside a mark of punctuation. An exception to this rule is for [[block quotation]]s, where the citation is placed outside the punctuation.<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=15.25|isbn=978-0226104201|edition=16th|chapter=Author-Date References: Text Citations in Relation to Direct Quotations}}</ref>

The full citation for the source is then included in a references section at the end of the material.<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=15.6|isbn=978-0226104201|edition=16th|chapter=Author-Date References: Basic Structure of a Reference List Entry}}</ref> As publication dates are prominent in this style, the reference entry places the publication date ''following'' the author(s) name.<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=15.14|isbn=978-0226104201|edition=16th|chapter=Author-Date References: Placement of Dates in Reference List Entries}}</ref>

  Heilman, James M., and Andrew G. West. 2015. "Wikipedia and Medicine: Quantifying Readership, Editors, and the Significance of Natural Language." ''Journal of Medical Internet Research'' [[Volume (bibliography)|17]] [[Issue (periodical)|(3)]]: e62. [[Digital object identifier|doi]]:10.2196/jmir.4069.

===Notes and bibliography style===
Using notes and bibliography style,{{efn| As used with periodical/journal articles.}} the sourced text is indicated by a [[superscript]]ed note number which corresponds to a full citation either at the bottom of the page (as a footnote) or at the end of a main body of text (as an endnote).<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.15|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Basic Structure of a Note}}</ref> In both instances the citation is also placed in a bibliography entry at the end of the material, listed in alphabetical order of the author's last name.<ref name=cmos2 /> One of the main differences in structure between a note and a bibliography entry is the placement of commas in the former and periods in the latter.<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.173|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Punctuation in Periodical Citations}}</ref>

The following is an example of a journal article citation provided as a note and its bibliography entry, respectively. The third, colored example of the bibliography entry provides a key to reference each part of the citation. (Coloring is for demonstration purposes and is not used in actual formatting.)

  1. James M. Heilman and Andrew G. West, "Wikipedia and Medicine: Quantifying Readership, Editors, and the Significance of Natural Language," ''Journal of Medical Internet Research'' [[Volume (bibliography)|17]], [[Issue (periodical)|no. 3]] (2015): e62, [[Digital object identifier|doi]]:10.2196/jmir.4069.

  Heilman, James M., and Andrew G. West. "Wikipedia and Medicine: Quantifying Readership, Editors, and the Significance of Natural Language." ''Journal of Medical Internet Research'' [[Volume (bibliography)|17]], [[Issue (periodical)|no. 3]] (2015): e62. [[Digital object identifier|doi]]:10.2196/jmir.4069.

<span style="border:3px orange; border-style:none dotted solid;">Heilman, James M., and Andrew G. West</span>
<span style="border:3px blue; border-style:none dotted solid;">"Wikipedia and Medicine: Quantifying Readership, Editors, and the Significance of Natural Language."</span>
<span style="border:3px lightgreen; border-style:none dotted solid;">''Journal of Medical Internet Research''</span>
<span style="border:3px red; border-style:none dotted solid;">[[Volume (bibliography)|17]]</span>
<span style="border:3px yellow; border-style:none dotted solid;">[[Issue (periodical)|no. 3]]</span>
<span style="border:3px lightgreen; border-style:none dotted solid;">(2015)</span>
<span style="border:3px blue; border-style:none dotted solid;">e62</span>
<span style="border:3px plum; border-style:none dotted solid;">doi:10.2196/jmir.4069</span>

*<span style="border:3px orange; border-style:none dotted solid;">Author(s)</span> first listed author's name inverted in the bibliography entry<ref name=cmos2>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.16|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Basic Structure of a Bibliography Entry}}</ref>
*<span style="border:3px blue; border-style:none dotted solid;">Article title</span> inside [[quotation mark]]s<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.176|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Journal Article--Title}}</ref>
*<span style="border:3px lightgreen; border-style:none dotted solid;">Journal title</span> in [[italic type]]<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.179|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Title of Journal}}</ref>
*<span style="border:3px red; border-style:none dotted solid;">[[Volume (bibliography)|Volume]]</span><ref name=vol>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.180|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Journal Volume, Issue, and Date}}</ref>
*<span style="border:3px yellow; border-style:none dotted solid;">[[Periodical literature|Issue]]</span><ref name=vol />
*<span style="border:3px lightgreen; border-style:none dotted solid;">Year</span> along with month, if specified<ref name=vol />
*<span style="border:3px blue; border-style:none dotted solid;">Page numbers</span>{{efn| The Heilman and West example article was published electronically without page numbers.}} specific page number in a note; page range in a bibliography entry<ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.17|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Page Numbers and Other Locators}}</ref><ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.183|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Journal Page References}}</ref>
*<span style="border:3px plum; border-style:none dotted solid;">[[Digital object identifier]]</span><ref>{{cite book|title=The Chicago Manual of Style|publisher=University of Chicago Press|location=14.6|isbn=978-0226104201|edition=16th|chapter=Notes and Bibliography: Digital Object Identifiers (DOIs)}}</ref>

==History==
What now is known as ''The Chicago Manual of Style'' was first published in 1906 under the title ''Manual of Style: Being a compilation of the typographical rules in force at the University of Chicago Press, to which are appended specimens of type in use''. From its first 203-page edition,<ref>''[http://www.chicagomanualofstyle.org/facsimile/CMSfacsimile_all.pdf Manual of Style: Being a Compilation of the Typographical Rules in Force at the University of Chicago Press]''. [[Chicago]]: [[University of Chicago Press|U of Chicago P]], 1906, 203.</ref> the ''CMOS'' evolved into a comprehensive reference style guide of 1,026 pages in its sixteenth edition.<ref name="spencer2011" /> It was one of the first editorial style guides published in the United States, and it is largely responsible for research methodology standardization, notably [[Citation#Citation styles|citation style]].

The most significant revision to the manual was made for the twelfth edition, published in 1969. Its first printing of 20,000 copies sold out before it was printed.<ref name="abouthistory">"[http://www.chicagomanualofstyle.org/about16_history.html The History of the Chicago Manual of Style]", University of Chicago Press, 2010. Retrieved March 17, 2011.</ref> In 1982, with the publication of the thirteenth edition, it was officially retitled ''The Chicago Manual of Style'' adopting the informal name already in widespread use.<ref name="abouthistory" />

More recently, the publishers have released a new edition about every ten years. The fifteenth edition was revised to reflect the emergence of computer technology and the internet in publishing, offering guidance for citing electronic works. Other changes included a chapter by [[Bryan A. Garner]] on American English grammar and use,<ref>Geoffrey K. Pullum, "[http://itre.cis.upenn.edu/~myl/languagelog/archives/001869.html The Chicago Manual of Style&nbsp;– and Grammar]", Language Log, February 2, 2005. Accessed February 12, 2012.</ref> and a revised treatment of mathematical copy.<ref name=CMOSOnline>{{webarchive |url=https://web.archive.org/web/20090217154429/http://chicagomanualofstyle.org/about15.html |date=February 17, 2009 |title=What’s New in the Fifteenth Edition of The Chicago Manual of Style }}</ref>

In August 2010, the sixteenth edition was published simultaneously in the hardcover and online editions for the first time in the ''Manual''{{-'}}s history. In a departure from the trademark red-orange cover, the sixteenth edition featured a robin's-egg blue dust jacket (a nod to older editions with blue jackets, such as the eleventh and twelfth). The sixteenth edition features "music, foreign languages, and computer topics (such as [[Unicode]] characters and [[URL]]s)".<ref name="spencer2011" /> It also offers expanded recommendations for producing electronic publications, including web-based content and [[e-book]]s. An updated appendix on production and digital technology demystifies the process of electronic [[workflow]] and offers a primer on the use of [[XML]] markup. It also included a revised glossary, including a host of terms associated with [[electronic publishing|electronic]] and print publishing. The ''Chicago'' system of documentation is streamlined to achieve greater consistency between the author-date and notes-bibliography systems of citation, making both systems easier to use. In addition, updated and expanded examples address the many questions that arise when documenting online and digital sources, from the use of [[digital object identifier|DOI]]s to citing [[social networking site]]s. Figures and tables are updated throughout the book, including a return to the ''Manual''{{-'}}s popular hyphenation table and new, selective listings of Unicode numbers for special characters.

In 2013, an adapted Spanish version was published by the [[University of Deusto]] in [[Bilbao]], Spain.<ref>[http://www.deusto-publicaciones.es/index.php/main/libro/994/es Manual de Estilo Chicago-Deusto. Edición adaptada al español]</ref>

In April 2016, the publisher is releasing ''The Chicago Guide to Grammar, Usage, and Punctuation'', [[Bryan A. Garner]]'s expansion of his ''Chicago Manual of Style'' chapter on the topic, and coinciding with the release of the new edition of ''Garner's Modern American Usage''.

===History of editions===
* [https://archive.org/details/manualofstylebei00univrich First Edition, 1906]
* [https://archive.org/details/manualofstylecomp00univrich Second Edition, 1910]
* [https://archive.org/details/manualofstylecom00univrich Third Edition, 1911]
* [https://archive.org/details/amanualstyleaco00presgoog Fourth Edition, 1914]
* [https://archive.org/details/manualofstylecom00univ Fifth Edition, 1917]
* Sixth Edition, 1919
* [https://archive.org/details/manualofstylecon00univiala Seventh Edition, 1920]
* Eighth Edition, 1925
* Ninth Edition, 1927
* Tenth Edition, 1937
* Eleventh Edition, 1949
* Twelfth Edition, 1969
* Thirteenth Edition, 1982
* Fourteenth Edition, 1993
* Fifteenth Edition, 2003
* Sixteenth Edition, 2010

==Recent printed editions==
* {{cite book |author=University of Chicago |title=The Chicago Manual of Style |edition=fifteenth |year= 2003 |publisher=Univ. of Chicago Press|location= Chicago |isbn=0-226-10403-6}}
* {{cite book |author=University of Chicago |title=The Chicago Manual of Style |edition=sixteenth |year= 2010 |publisher=Univ. of Chicago Press|location= Chicago |isbn=978-0-226-10420-1}}

==See also==
* [[Linguistic prescription]]
* [[Wikipedia:Manual of Style|Wikipedia's Manual of Style]]
* [[Oxford Standard for Citation of Legal Authorities]] or "OSCOLA"
* [[Turabian|Kate L. Turabian's ''A Manual for Writers of Research Papers, Theses, and Dissertations'']]

==Notes==
{{notelist}}

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.chicagomanualofstyle.org/}}
** [http://www.chicagomanualofstyle.org/about16_history.html History]
** [http://www.chicagomanualofstyle.org/tools_citationguide.html Quick Guide]
** [http://www.chicagomanualofstyle.org/about16_facsimile.html Facsimile of the 1st Edition: Manual of Style]
* [http://robocite.com/#chicago-author-date Automatically generate Chicago citations based on DOI, ISBN and URL].
* [http://library.williams.edu/citing/styles/chicago1.php ''CMS'' Bibliography Samples] at [[Williams College]]
* [http://www.lib.sfu.ca/help/writing/chicago-turabian ''CMS''] at [[Simon Fraser University]]
* ''[http://www.aaanet.org/publications/guidelines.cfm AAA Style Guide]'' at [[American Anthropological Association]]—Uses ''The Chicago Manual of Style'', fifteenth edition
* ''[http://magazine.oah.org/contributors/style_guide.html OAH Style Sheet]'' at [[Organization of American Historians]]—Uses ''The Chicago Manual of Style'', fifteenth edition

{{DEFAULTSORT:Chicago Manual of Style}}
[[Category:1906 books|Chicago Manual of Style, The]]
[[Category:Academic style guides]]
[[Category:Style guides for American English|Chicago Manual of Style, The]]
[[Category:University of Chicago Press books|Chicago Manual of Style, The]]