{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Devon Park
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 752
| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41356 |name=Devon Park (State Suburb) |accessdate=4 June 2011| quick=on}}</ref><br />775 <small>''(2001 Census)''</small><ref name=ABS2001>{{Census 2001 AUS |id =SSC41341 |name=Devon Park (State Suburb) |accessdate=4 June 2011| quick=on}}</ref>
| est                 = 1920<ref name=Manning>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/d/d3.htm#devonP |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=4 June 2011}}</ref>
| postcode            = 5008<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/devon+park |title=Devon Park, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=4 June 2011}}</ref>
| area                = 
| dist1               = 4.6
| dir1                = NW
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = [[City of Port Adelaide]]<ref name=PortAdelaideEnf>{{cite web |url=http://www.portenf.sa.gov.au/site/page.cfm?u=160 |title=Parks Ward Councillors |author= |date= |work= |publisher=City of Port Adelaide Enfield |accessdate=4 June 2011}}</ref>
| lga2                = [[City of Charles Sturt]]<ref name=CharlesSturt>{{cite web|url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |title=City of Charles Sturt Wards and Council Members |author= |date= |work= |publisher=City of Charles Sturt |accessdate=4 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110805135335/http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |archivedate=5 August 2011 |df=dmy }}</ref>
| stategov            = [[Electoral district of Croydon (South Australia)|Croydon]] <small>''(2011)''</small><ref name=ECSA>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=4 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov              = [[Division of Adelaide|Adelaide]] <small>''(2011)''</small><ref name=AEC>{{cite web |url=http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Adelaide&filterby=Electorate&divid=179 |title=Find my electorate: Adelaide |author= |date= |work= |publisher=Australian Electoral Commission |accessdate=4 June 2011}}</ref>
| near-n              = [[Dudley Park, South Australia|Dudley Park]]
| near-ne             = [[Prospect, South Australia|Prospect]]
| near-e              = [[Prospect, South Australia|Prospect]]
| near-se             = [[Prospect, South Australia|Prospect]]
| near-s              = [[Renown Park, South Australia|Renown Park]]
| near-sw             = [[Renown Park, South Australia|Renown Park]]
| near-w              = [[Renown Park, South Australia|Renown Park]]
| near-nw             = [[Croydon Park, South Australia|Croydon Park]]
}}

'''Devon Park''' is an inner northern [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the cities of [[City of Port Adelaide Enfield|Port Adelaide Enfield]] and [[City of Charles Sturt|Charles Sturt]].

==Geography==
Devon Park lies beside the [[Gawler Central railway line]] near, but not meeting, the intersection of [[Torrens Road, Adelaide|Torrens Road]] and [[Churchill Road, Adelaide|Churchill Road]].<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 752 persons in Devon Park on census night. Of these, 48.9% were male and 51.1% were female.<ref name=ABS2006/>

The majority of residents (66.4%) are of Australian birth, with other common census responses being [[England]] (5.3%), [[Italy]] (2.9%), [[Greece]] (2.3%), [[India]] (2.1%) and [[Vietnam]] (2.0%).<ref name=ABS2006/>

The age distribution of Devon Park residents is skewed higher than the greater Australian population. 75.6% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 24.4% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Politics==

===Local government===
Devon Park is split between two [[Local government in Australia|local government areas]]. The majority of the suburb is in the [[City of Port Adelaide Enfield]] but a small portion in the south lies within the [[City of Charles Sturt]].<ref name=UBD/>

Within Port Adelaide Enfield, Devon Park is part of Parks Ward and is represented in that council by Claire Boan and Guy Wilcock, with previous Parks Ward Councillor Tung Ngo having been elected to the South Australian Parliament at the 2014 State Election.<ref name=PortAdelaideEnf/>

The Charles Sturt portion of the suburb forms part of that council's Hindmarsh Ward, being represented in that council by Paul Alexandrides and Craig Auricht.<ref name=CharlesSturt/>

===State and federal===
Devon Park lies in the state [[Electoral districts of South Australia|electoral district]] of [[Electoral district of Croydon (South Australia)|Croydon]]<ref name=ECSA/> and the federal [[Divisions of the Australian House of Representatives|electoral division]] of [[Division of Adelaide|Adelaide]].<ref name=AEC/> The suburb is represented in the [[South Australian House of Assembly]] by [[Michael Atkinson]]<ref name=ECSA/> and [[Australian House of Representatives|federally]] by [[Kate Ellis]].<ref name=AEC/>

==Facilities and attractions==

===Parks===
There is a park located between Harrison Road and Ashby Crescent.<ref name=UBD/>

==Transportation==

===Roads===
Devon Park is indirectly serviced by [[South Road, Adelaide|South Road]] and [[Torrens Road, Adelaide|Torrens Road]].<ref name=UBD/>

===Public transport===
Devon Park is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date= |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=4 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

====Trains====
The [[Gawler Central railway line|Gawler]] train service passes beside the suburb. The closest station is [[Dudley Park railway station|Dudley Park]].<ref name=Metro/>

====Buses====
The suburb is serviced by bus routes run by the [[Adelaide Metro]].<ref name=Metro/>

==See also==
{{Commons category}}
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.portenf.sa.gov.au |title=City of Port Adelaide Enfield|author= |date= |work=Official website |publisher=City of Port Adelaide Enfield |accessdate=4 June 2011}}
*{{cite web |url=http://www.charlessturt.sa.gov.au |title=City of Charles Sturt |author= |date= |work=Official website |publisher=City of Charles Sturt |accessdate=4 June 2011}}

{{Coord|-34.888|138.58|format=dms|type:city_region:AU-SA|display=title}}
{{City of Port Adelaide Enfield suburbs}}
{{City of Charles Sturt suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1920]]