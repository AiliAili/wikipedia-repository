{{Use mdy dates|date=May 2014}}
{{Infobox airport
| name         = Southwest Florida International Airport
| image        = RSW Logo.svg
| image-width  = 175
| image2       = Southwest_Florida_International_Airport_RSW.jpg
| image2-width = 250
| IATA         = RSW
| ICAO         = KRSW
| FAA          = RSW
| type         = Public
| owner        = [[Lee County, Florida|Lee County]]
| operator     = Lee County Port Authority
| city-served  = [[Fort Myers, Florida|Fort Myers]], Florida
| location     = South Fort Myers, Florida
| focus_city   = [[Sun Country Airlines]]
| elevation-f  = 30
| elevation-m  = 9
| coordinates  = {{coord|26|32|10|N|081|45|19|W|region:US-FL_type:airport}}
| website      = [http://www.flylcpa.com/ flylcpa.com]
| pushpin_map            = USA Florida#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Florida / United States
| pushpin_label          = '''RSW'''
| pushpin_label_position = right
| r1-number    = 6/24
| r1-length-f  = 12,000
| r1-length-m  = 3,658
| r1-surface   = [[Asphalt]]
| stat-year    = 2013
| stat1-header = Aircraft operations
| stat1-data   = 100,000
| stat2-header = Passengers
| stat2-data   = 8,371,801 ('15)
| stat3-header = Total Cargo (Lbs)
| stat3-data   = 32,156,880
| footnotes    = Source: [http://flylcpa.com/uploads/pagesfiles/1926.pdf]
}}

'''Southwest Florida International Airport''' {{Airport codes|RSW|KRSW|RSW}} is a county-owned airport in the South Fort Myers region of [[unincorporated area|unincorporated]] [[Lee County, Florida|Lee County]], Florida. The airport's market is [[Southwest Florida]]: [[Bonita Springs, Florida|Bonita Springs]], [[Cape Coral, Florida|Cape Coral]],  [[Captiva, Florida|Captiva Island]], [[Estero, Florida|Estero]], [[Fort Myers, Florida|Fort Myers]], [[Marco Island, Florida|Marco Island]], [[Naples, Florida|Naples]] and [[Sanibel, Florida|Sanibel Island]].  In 2015 passengers numbered 8,371,801. The airport is the second busiest single-runway airport in the United States after [[San Diego International Airport]].<ref>{{cite news|last1=Davis|first1=Rob|title=Airport Questions Answered|url=http://voiceofsandiego.org/uncategorized/airport-questions-answered/|accessdate=17 March 2015|work=Voice of San Diego|date=20 April 2006}}</ref>  It is a [[U.S. Customs and Border Protection]] port of entry.

The airport sits on 13,555 acres of land just southeast of Fort Myers, making it the third-largest airport in the United States in terms of land size (after [[Denver International Airport|Denver]] and [[Dallas/Fort Worth International Airport|Dallas/Fort Worth]]).  Though, 6,000 acres of the airport's land includes swamp land that has been set aside for [[environmental mitigation]].<ref>{{cite web|title=Southwest Florida Transportation: Are We There Yet?|url=http://www.gulfshorelife.com/January-2014/Southwest-Florida-Transportation-Are-We-There-Yet/|website=Gulfshore Life|accessdate=12 October 2015}}</ref> The airport code '''RSW''' means "'''R'''egional '''S'''outh-'''W'''est" (for '''Southwest Florida Regional Airport'''). In 1993 the Lee County Port Authority renamed it Southwest Florida International Airport.

==History==
RSW was conceived in 1973 when it was clear that the existing airport in Fort Myers, [[Page Field]], was too small to handle increasing numbers of commercial flights.  Expanding Page Field was determined to be impractical since its airfield was constrained by U.S. 41 to the west and expanding the airfield to the east would require bridging the [[Ten Mile Canal]] and relocating a railroad track.<ref>{{cite web|title=Southwest Florida Regional Airport Environmental Impact Statement|url=https://books.google.com/books?id=l_k0AQAAMAAJ&printsec=frontcover#v=onepage&q&f=false|accessdate=15 August 2016}}</ref>

A number of sites were considered for a new regional airport, including southern [[Charlotte County, Florida|Charlotte County]], [[Estero, Florida|Estero]], and northeast Cape Coral near [[Burnt Store Marina, Florida|Burnt Store Marina]].  The government of Lee County ultimately selected a site near the end of [[County Road 876 (Lee County, Florida)|Daniels Road]] which was a dirt road at the time.  An advantage to this location was its proximity to [[Interstate 75 in Florida|Interstate 75]], which was under construction and would have an interchange with Daniels Road, providing easy access (Interstate 75 was opened to traffic through Fort Myers in 1979).<ref>{{cite web|title=Interstate 75|url=http://www.aaroads.com/guide.php?page=i0075fl|website=AA Roads|accessdate=28 July 2016}}</ref>

Groundbreaking was in 1980, and '''Southwest Florida Regional Airport''' opened on schedule, May 14, 1983, with a single 8400-ft runway. [[Delta Air Lines]] operated the first flight.  The original terminal was located on the north side of the runway at the end of Chamberlin Parkway.

The airport was renamed '''Southwest Florida International Airport''' in 1993, though it had hosted international flights since 1984 and U.S. Customs since 1987, mainly for flights to Germany. The name change coincided with the completion of a 55,000 square foot Federal Inspection facility annexed to the original terminal's Concourse A.<ref>{{cite web|title=A History of Aviation in Lee County|url=http://www.flylcpa.com/uploads/pagesfiles/511.pdf|website=Southwest Florida International Airport|accessdate=12 October 2015}}</ref> The runway was also lengthened to 12,000&nbsp;ft (3,658 m) at the same time to better accommodate international service (making it the fourth-longest runway in Florida).<ref>{{cite web|title=Southwest Florida International Airport|url=http://www.freightmovesflorida.com/docs/default-source/default-document-library/634665599509832500-southwest_florida_international_airport_12_22_11_2_.pdf?sfvrsn=0|website=Freight Moves Florida|accessdate=12 October 2015}}</ref> The airport has hosted [[Boeing 747]]s (including [[Air Force One]]), but as of 2009 the largest aircraft scheduled to RSW are the [[Airbus A330-200]]s on [[Air Berlin]] non-stop flights to [[Düsseldorf]], the [[Airbus A330|Airbus A330-300]] seasonally to [[Minneapolis–Saint Paul International Airport|Minneapolis/St. Paul]] and the [[Boeing 767-300]]s operated by [[Delta Air Lines]] non-stop from [[Minneapolis–Saint Paul International Airport|Minneapolis/St. Paul]], [[Hartsfield–Jackson Atlanta International Airport|Atlanta]], and [[Detroit Metropolitan Wayne County Airport|Detroit]] .

In 1988 the airport exceeded its annual capacity of 3 million passengers; by 2004, the airport was serving nearly 7 million passengers annually.  The original terminal had 17 gates on two concourses.  While three of the gates were added in a minor expansion of the B concourse in the late 1990s, the original terminal's design was not conducive to a major expansion.

With the terminal operating at more than double its intended capacity, construction of a new Midfield Terminal Complex began in February 2002. The $438 million terminal opened on September 9, 2005. The terminal, designed by Spillis Candela/[[AECOM|DMJM Aviation]],<ref>{{cite web|url=http://southeast.construction.com/features/archive/0411_Feature2.asp |title=Ready for Takeoff? |publisher=Southeast Construction |date=September 11, 2001 |accessdate=July 20, 2012}}</ref> has three concourses and 28 gates and can eventually expand to five concourses with 65 gates.  Demolition of the former terminal north of the airfield was completed in spring 2006.

In early 2015, Terminal Access Road, the airport's main entrance road, was extended past Treeline Avenue to connect directly to Interstate 75, allowing airport-related traffic to avoid local streets. The airport can now be accessed directly from the freeway at Exit 128.<ref>{{cite web|url=http://i75onthego.com/homeAirportDC.html |title=I-75onthego  -  I-75 Direct Connect |work=i75onthego.com |accessdate=June 14, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20150523183856/http://www.i75onthego.com/homeAirportDC.html |archivedate=May 23, 2015 |df=mdy }}</ref>  Terminal Access Road was then expanded to six lanes in late 2016.<ref>[http://flylcpa.com/uploads/pagesfiles/1440.pdf]</ref>

==Current and future projects==
A new $16 million Airport Rescue and Fire Fighting facility (Lee County Station 92) opened in July 2013. A {{convert|9100|ft|m|abbr=on}} parallel runway is in planning. The project includes a relocated air traffic control tower, apron expansion, crossfield taxiway system, mitigation activities and FPL electrical line relocation. The apron expansion and crossfield taxiway system were completed in late 2013. The entire project is estimated to cost $454 million.

Plans are in place for Skyplex – a commercial and industrial park in the location of the old terminal. Other airport-related businesses, such as a hotel, are in the planning stages. A retail gasoline outlet near the airport's entrance opened in June 2014.<ref>[http://flylcpa.com/uploads/pagesfiles/1329.pdf]</ref><ref>[http://www.news-press.com/apps/pbcs.dll/article?AID=/20060405/BUSINESS/604050395/1075 ]{{dead link|date=June 2012}}</ref>

==Facilities==
[[File:Southwest Florida International Airport Overhead Shot.jpg|thumb|An overhead view of Southwest Florida International Airport]]
[[File:Southwest Florida International Airport Atrium.jpg|right|thumb|East Atrium]]
[[File:Southwest Florida International Airport Main Terminal.jpg|thumb|Main Terminal]]
The airport covers 13,555 acres (54.9&nbsp;km<sup>2</sup>), 10&nbsp;mi (16&nbsp;km) southeast of [[Fort Myers, Florida|Fort Myers]].

;Runways
* Runway 6/24: 12,000 x 150&nbsp;ft (3,658 x 46 m) Asphalt [http://www.gcr1.com/5010WEB/airport.cfm?Site=RSW Airport IQ 5010]

;Activity<ref>{{cite web|url=http://flylcpa.com/monthlystats/ |title=Southwest Florida International Airport |publisher=Flylcpa.com |date= |accessdate=June 15, 2012}}</ref>
In 2011 the airport had 83,385 aircraft operations, average 228 per day.

;Terminal
* {{convert|798000|sqft|m2|abbr=on}}
* Design capacity is 10 million passengers per year, with 28 gates on 3 concourses (current B,C and D). The terminal buildings can be expanded incrementally to 65 gates on 5 concourses (A-E).

;Parking
* 11,250 spaces for hourly/daily parking
* 30-space "cell-phone lot" for customers picking up arriving passengers

;Awards
*J.D. Power & Associates Airport Satisfaction Study – Ranked 2nd among North American airports with under 10 million annual passengers
*[[Florida Department of Transportation]] 2008 Commercial Airport of the Year
*Airports Council International-North America Excellence in Marketing and Communications 2008: 1st Place Special Events for Aviation Day
*Airports Council International-North America 2008: 1st Place for Concession Convenience and 2nd Place for Food Concessions
*Airports Council International-North America 2009: 2nd Place Newsletter – Internal or E-mail and 2nd Place Special Events – Berlin Airlift
*[[Federal Aviation Administration]] 2009 Disadvantaged Business Enterprise Advocate and Partner Award
*Florida Airports Council 2008 Environmental Excellence Award for Mitigation Park
*Airport Revenue News 2008 Best Concessions Award for top Concessions Program Design

==Terminals==
The airport has one terminal with three concourses: '''Concourse B''' (Gates B1-B9), '''Concourse C''' (Gates C1-C9), and '''Concourse D''' (Gates D1-D10). Customs and Immigration services for international flights are located on the lower level of Concourse B. "Concourses A and E" designations have been reserved for the planned future expansion of the terminal.

==Airlines and destinations==
===Passenger===
<!--DO NOT ADD TERMINAL/CONCOURSE INFORMATION TO THIS TABLE AS IT VIOLATES [[WP:NOTTRAVEL]], SOURCES ADDED PER DISCUSSION AT [[Wikipedia_talk:WikiProject_Airports#Proposal_to_Change_Template_Discussion]], PLEASE EXPRESS CONCERNS YOU MAY HAVE AT THIS DISCUSSION, NOT BY CHANGING THIS TABLE!!!-->
{{Airport-dest-list | 3rdcoltitle=Refs
<!-- -->
| [[Air Berlin]] | [[Düsseldorf Airport|Düsseldorf]] | <ref name="AirBerlinRoutes">{{cite web|title=Route Network - airberlin.com|url=https://www.airberlin.com/en/site/landingpages/map.php|website=www.airberlin.com|accessdate=19 February 2017|language=en}}</ref>
<!-- -->
| [[Air Canada Rouge]] | [[Toronto Pearson International Airport|Toronto–Pearson]]<br>'''Seasonal''': [[Montréal–Pierre Elliott Trudeau International Airport|Montréal–Trudeau]] | <ref name="AirCanadaRoutes">{{cite web|title=Flight Schedules|url=https://beta.aircanada.com/us/en/aco/home/book/routes-and-partners/flight-schedules.html?acid=beta%7Credirect%7Caircanada.com%7CNoBar|accessdate=19 February 2017}}</ref>
<!-- -->
| [[American Airlines]] | [[Charlotte Douglas International Airport|Charlotte]], [[O'Hare International Airport|Chicago–O'Hare]], [[Dallas/Fort Worth International Airport|Dallas/Fort Worth]], [[Philadelphia International Airport|Philadelphia]]<br>'''Seasonal''': [[Ronald Reagan Washington National Airport|Washington–National]] | <ref name="AmericanRoutes">{{cite web|title=Flight schedules and notifications|url=https://www.aa.com/travelInformation/flights/schedule|accessdate=19 February 2017}}</ref>
<!-- -->
| [[American Eagle (airline brand)|American Eagle]] | [[Ronald Reagan Washington National Airport|Washington–National]] | <ref name="AmericanRoutes" />
<!-- -->
| [[Delta Air Lines]] | [[Hartsfield–Jackson Atlanta International Airport|Atlanta]], [[Boston Logan International Airport|Boston]], [[Cincinnati/Northern Kentucky International Airport|Cincinnati]], [[Detroit Metropolitan Airport|Detroit]],
 [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]], [[LaGuardia Airport|New York–LaGuardia]]<br>'''Seasonal''': [[John F. Kennedy International Airport|New York–JFK]] | <ref name="DeltaRoutes">{{cite web|title=FLIGHT SCHEDULES|url=https://www.delta.com/flightinfo/viewFlightSchedulesSetup.action|accessdate=19 February 2017}}</ref>
<!-- -->
| [[Delta Connection]] | '''Seasonal: ''' [[Boston Logan International Airport|Boston]], [[Cincinnati/Northern Kentucky International Airport|Cincinnati]], [[Port Columbus International Airport|Columbus (OH)]], [[Indianapolis International Airport|Indianapolis]], [[John F. Kennedy International Airport|New York–JFK]], [[LaGuardia Airport|New York–LaGuardia]] | <ref name="DeltaRoutes" />
<!-- -->
| [[Frontier Airlines]] | [[Cincinnati/Northern Kentucky International Airport|Cincinnati]], [[Cleveland Hopkins International Airport|Cleveland]], [[Denver International Airport|Denver]], [[Detroit Metropolitan Airport|Detroit]]<br>'''Seasonal:''' [[Port Columbus International Airport|Columbus (OH)]], [[Indianapolis International Airport|Indianapolis]], [[General Mitchell International Airport|Milwaukee]], [[Lambert–St. Louis International Airport|St. Louis]] | <ref name="FrontierRoutes">{{cite web|title=Route Map-Frontier Airlines|url=https://www.flyfrontier.com/plan-and-book/route-map/|website=Frontier Airlines|accessdate=19 February 2017}}</ref>
<!-- -->
| [[JetBlue Airways]] | [[Logan International Airport|Boston]], [[Newark Liberty International Airport|Newark]], [[John F. Kennedy International Airport|New York–JFK]], [[Ronald Reagan Washington National Airport|Washington–National]], [[Westchester County Airport|White Plains]]<br>'''Seasonal''': [[Bradley International Airport|Hartford]]| <ref name="JetblueRoutes">{{cite web|title=JetBlue - Where We Jet: Flight Destinations|url=http://www.jetblue.com/WhereWeJet/|website=www.jetblue.com|accessdate=19 February 2017|language=en}}</ref>
<!-- -->
| [[Silver Airways]] | [[Key West International Airport|Key West]], [[Lynden Pindling International Airport|Nassau]], [[Orlando International Airport|Orlando]] | <ref name="SilverRoutes">{{cite web|title=Route Map Silver Airways|url=https://www.silverairways.com/more-information/travel-information/route-map|website=www.silverairways.com|accessdate=19 February 2017}}</ref>
<!-- -->
| [[Southwest Airlines]]| [[Hartsfield–Jackson Atlanta International Airport|Atlanta]], [[Baltimore/Washington International Thurgood Marshall Airport|Baltimore]], [[Chicago Midway International Airport|Chicago–Midway]], [[Port Columbus International Airport|Columbus (OH)]], [[Bradley International Airport|Hartford]],  [[Indianapolis International Airport|Indianapolis]], [[Pittsburgh International Airport|Pittsburgh]], [[Lambert–St. Louis International Airport|St. Louis]]<br>'''Seasonal''': [[Albany International Airport|Albany]], [[Buffalo Niagara International Airport|Buffalo]], [[Denver International Airport|Denver]], [[Bishop International Airport|Flint]], [[Gerald R. Ford International Airport|Grand Rapids]], [[Kansas City International Airport|Kansas City]], [[General Mitchell International Airport|Milwaukee]], [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]], [[Nashville International Airport|Nashville]], [[T. F. Green Airport|Providence]], [[Ronald Reagan Washington National Airport|Washington–National]] | <ref name="SouthwestRoutes">{{cite web|title=Check Flight Schedules|url=https://www.southwest.com/air/flight-schedules/index.html|accessdate=19 February 2017}}</ref> 
<!-- -->
| [[Spirit Airlines]] | [[Atlantic City International Airport|Atlantic City]], [[O'Hare International Airport|Chicago–O'Hare]], [[Detroit Metropolitan Airport|Detroit]]<br>'''Seasonal''': [[Akron–Canton Airport|Akron/Canton]],<ref name="spiritairlines">{{cite news | url=http://fox8.com/2016/07/26/akron-canton-airport-to-make-major-air-service-announcement/ | title=Spirit Airlines expanding to Akron–Canton Airport | work=[[WJW (TV)]] | date=July 26, 2016 | accessdate=July 26, 2016 | author=Loreno, Darcie}}</ref> [[Baltimore/Washington International Thurgood Marshall Airport|Baltimore]], [[Logan International Airport|Boston]], [[Cleveland Hopkins International Airport|Cleveland]], [[Dallas/Fort Worth International Airport|Dallas/Fort Worth]], [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]], [[Arnold Palmer Regional Airport|Pittsburgh–Latrobe]] | <ref name="SpiritRoutes">{{cite web|title=Spirit Airlines - Route Map|url=https://www.spirit.com/RouteMaps.aspx|website=www.spirit.com|accessdate=19 February 2017|language=en}}</ref>
<!-- -->
| {{nowrap|[[Sun Country Airlines]]}} | [[Cancún International Airport|Cancún]], [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul]], [[Luis Muñoz Marin International Airport|San Juan]] | <ref name="SunCountryRoutes">{{cite web|title=Sun Country Airlines-Route Map|url=http://sy.fltmaps.com/en|website=sy.fltmaps.com|accessdate=19 February 2017|language=en}}</ref>
<!-- -->
| [[United Airlines]] | [[George Bush Intercontinental Airport|Houston–Intercontinental]], [[Newark Liberty International Airport|Newark]]<br>'''Seasonal''': [[O'Hare International Airport|Chicago–O'Hare]], [[Cleveland Hopkins International Airport|Cleveland]], [[Denver International Airport|Denver]] | <ref name="UnitedRoutes">{{cite web|title=Timetable|url=https://www.united.com/web/en-US/apps/travel/timetable/default.aspx|accessdate=19 February 2017}}</ref>
<!-- -->
| [[United Express]] | '''Seasonal''': [[O'Hare International Airport|Chicago–O'Hare]], [[George Bush Intercontinental Airport|Houston–Intercontinental]], [[Newark Liberty International Airport|Newark]] | <ref name="UnitedRoutes" />
<!-- -->
| [[WestJet]] | [[Toronto Pearson International Airport|Toronto–Pearson]]<br>'''Seasonal''': [[Ottawa Macdonald–Cartier International Airport|Ottawa]] | <ref name="WestJetRoutes">{{cite web|title=Route map - WestJet.com|url=https://www.westjet.com/en-ca/travel-info/flight-info/route-map|website=www.westjet.com|accessdate=19 February 2017|language=en}}</ref>
<!-- -->
}}

===Cargo===
{{Airport-dest-list
<!-- -->
|[[FedEx Express]]|[[Memphis International Airport|Memphis]]
<!-- -->
|[[UPS Airlines]]|[[Fort Lauderdale–Hollywood International Airport|Fort Lauderdale]], [[Louisville International Airport|Louisville]]
<!-- -->
}}

==Statistics==

===Top destinations===
{| class="wikitable sortable" style="font-size: 95%" width= align=
|+ '''Busiest domestic routes from RSW <br>(Nov 2015 – Oct 2016)'''<ref>{{cite web|url=http://www.transtats.bts.gov/airports.asp?pn=1&Airport=RSW&Airport_Name=Ft.%20Myers,%20FL:%20Southwest%20Florida%20International%20Airport%20&carrier=FACTS |title=RITA &#124; BTS &#124; Transtats |publisher=Transtats.bts.gov |date= |accessdate=June 2016}}</ref>
! Rank
! City (Airport)
! Passengers
! Carriers
|-
| 1
| [[Hartsfield–Jackson Atlanta International Airport|Atlanta, Georgia]]
| 586,000
| Delta, Southwest, Spirit
|-
| 2
| [[O'Hare International Airport|Chicago–O'Hare, Illinois]]
| 311,000
| American, Spirit, United
|-
| 3
| [[Detroit Metropolitan Airport|Detroit, Michigan]]
| 282,000
| Delta, Spirit
|-
| 4
| [[Charlotte/Douglas International Airport|Charlotte, North Carolina]] 
| 236,000
| American
|-
| 5
| [[Logan International Airport|Boston, Massachusetts]]
| 231,000
| JetBlue, Southwest, Spirit
|-
| 6
| [[Newark Liberty International Airport|Newark, New Jersey]]
| 230,000
| JetBlue, United
|-
| 7
| [[Minneapolis−Saint Paul International Airport|Minneapolis/St. Paul, Minnesota]]
| 216,000
| Delta, Southwest, Spirit, Sun Country
|-
| 8
| [[Baltimore–Washington International Airport|Baltimore, Maryland]]
| 174,000
| Southwest
|-
| 9
| [[Midway International Airport|Chicago–Midway, Illinois]] 
| 169,000
| Southwest 
|-
| 10
| [[LaGuardia Airport|New York–LaGuardia, New York]]
| 149,000
| Delta
|}

{| class="wikitable sortable" style="font-size: 95%"
|+ '''Busiest international routes from RSW (2014)''' <ref>{{cite web|url=http://www.dot.gov/office-policy/aviation-policy/us-international-passenger-data-year-datecalendar-year-2013|title=U.S.-International Passenger Data for Year To Date/Calendar Year 2013|work=Department of Transportation|accessdate=June 14, 2015}}</ref>
|-
! Rank
! Airport
! Passengers
! Carriers
|-
| 1
| [[Toronto Pearson International Airport|Toronto, Canada]]
| 133,534
| Air Canada, WestJet
|-
| 2
| [[Düsseldorf Airport|Düsseldorf, Germany]]
| 39,891
| Air Berlin
|-
| 3
| [[Montréal–Pierre Elliott Trudeau International Airport|Montréal, Canada]]
| 5,713
| Air Canada
|-
| 4
| [[Ottawa Macdonald–Cartier International Airport|Ottawa, Canada]]
| 4,165
| WestJet
|-
|}
{| class="wikitable sortable" style="font-size: 100%"
|+ '''Largest airlines at RSW<br>(Nov 2015 – Oct 2016)<ref>{{cite web|url=http://www.transtats.bts.gov/airports.asp?pn=1&Airport=RSW&Airport_Name=Fort%20Myers,%20FL:%20Southwest%20Florida%20International&carrier=FACTS|title=RITA - BTS - Transtats|work=bts.gov|accessdate=June 2016}}</ref>'''
|-
! Rank
! Airline
! Share
|-
| 1
| [[Southwest Airlines]]
| 21.95%
|-
| 2
| [[Delta Air Lines]]
| 21.71%
|-
| 3
| [[American Airlines]]
| 15.51%
|-
| 4
| [[JetBlue]]
| 13.60%
|-
| 5
| [[United Airlines]]
| 8.47%
|-
| 6
| Others
| 18.76%
|}

===Annual traffic===
{| class="wikitable" style="font-size: 95%"
|+ '''Annual passenger traffic (enplaned + deplaned), 1983 - NOV 2016<ref>{{cite web|url=http://www.flylcpa.com/monthlystats/|title=Southwest Florida International Airport|work=flylcpa.com|accessdate=June 14, 2015}}</ref>
! Year
! Passengers
! Year
! Passengers
! Year
! Passengers
! Year
! Passengers
|-
| 1983||544,636||1993||3,717,758||2003||5,891,668||2013||7,637,801
|-
| 1984||1,311,937||1994||4,005,067||2004||6,736,630||2014||7,970,493
|-
| 1985||1,701,969||1995||4,098,264||2005||7,518,169||2015||8,371,801
|-
| 1986||2,129,548||1996||4,317,347||2006||7,643,217||2016||8,604,673
|-
| 1987||2,687,053||1997||4,477,865||2007||8,049,676||||
|-
| 1988||3,115,124||1998||4,667,207||2008||7,603,845||||
|-
| 1989||3,231,092||1999||4,897,253||2009||7,415,958||||
|-
| 1990||3,734,067||2000||5,207,212||2010||7,514,316||||
|-
| 1991||3,436,520||2001||5,277,708||2011||7,537,745||||
|-
| 1992||3,472,661||2002||5,185,648||2012||7,350,625||||
|}

==Accidents and incidents==
* September 8, 2005 – A [[Condor Flugdienst|Condor]] [[Boeing 767-300ER]] bound for Frankfurt, Germany returned safely to the airport after experiencing engine trouble. Ironically, this was the final flight to depart from the airport's original terminal.<ref>{{cite news|title=The First 24 Hours|agency=The News-Press|date=9 September 2005}}</ref>
* November 28, 2007 – A single-engine fixed wing aircraft crashed about 9:20&nbsp;a.m. one mile (1.6&nbsp;km) west of Runway 6. The crash killed the pilot. This is the first reported crash on airport property.<ref>[http://www.news-press.com/apps/pbcs.dll/article?AID=/20071128/NEWS01/71128016/1075 ]{{dead link|date=June 2012}}</ref>
* April 13, 2009 – A Beech King Air 200 (N559DW) was carrying four passengers when the pilot went unconscious and later died.  Doug White, a passenger, was guided into the airport by air traffic controller Brian Norton, assisted by controller Dan Favio.  It was later reported that White was a single engine private pilot with about 130 hours of experience in single engine aircraft.  All passengers aboard survived and the plane was not damaged.<ref>{{cite news| url=http://www.cnn.com/2009/US/04/13/florida.plane.emergency/index.html#cnnSTCVideo | work=CNN | title=Passenger lands turboprop plane after pilot dies | date=April 13, 2009}}</ref>

==Ground transport==
[[LeeTran]] bus No. 50 serves the airport.

==See also==
* [[Southwest Florida]]
* [[Florida Suncoast]]

==References==
{{Reflist}}

==External links==
{{Commons category inline|Southwest Florida International Airport}}
* [http://www.flylcpa.com/ Southwest Florida International Airport] (official site)
** [http://www.flylcpa.com/aboutlcpa/ Lee County Port Authority]
** [http://www.flylcpa.com/fmy/ Page Field General Aviation Airport] (formerly the primary airport in the area; now a general aviation airport)
* {{cite web|url= http://www.cfaspp.com/PopUps/DownloadDocument.aspx?doctype=facilityinfo&uaid=FL000085 |title=Southwest Florida International Airport }} brochure from [http://www.cfaspp.com/ CFASPP]
* {{FAA-diagram|06757}}
* {{FAA-procedures|RSW}}
* {{US-airport|RSW}}
**[http://www.PrivateSky.net/ PrivateSky® Aviation] (Official Site) Serves as FBO and Gulfstream MRO Service Center

{{Florida airports}}

[[Category:Airports in Florida]]
[[Category:Airports established in 1983]]
[[Category:Transportation in Lee County, Florida]]
[[Category:Transportation in Fort Myers, Florida]]
[[Category:Buildings and structures in Lee County, Florida]]