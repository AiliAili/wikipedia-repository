{{about|the heavy metal band||Body count (disambiguation)}}
{{Infobox musical artist <!-- See Wikipedia:WikiProject Musicians -->
| name             = Body Count
| background       = group_or_band
| image            = Bodycount.jpg <!-- Put only the image name (e.g. Example.png) without the File: prefix. -->
| image_size       = 270px
| landscape        = yes
| caption          = Body Count performs at a concert in [[Prague]], 2006
| alias            =
| origin           = [[Los Angeles]], [[California]], {{nowrap|United States}}
| genre            = {{flatlist|
*[[Thrash metal]]<ref>{{cite web|url=https://books.google.com/books?id=t9eocwUfoSoC&pg=PA90&lpg=PA90&dq=body+count+born+dead+rollingstone&source=bl&ots=BjKomk6N_8&sig=pU1TF154CfsH2Qc1GDfVJYkmUuU&hl=en&sa=X&ved=0CDoQ6AEwBGoVChMIurSpq9v8xgIVwR4eCh3sLQf1#v=onepage&q=body%20count%20born%20dead%20rollingstone&f=false |title=The New Rolling Stone Album Guide - Nathan Brackett, Christian David Hoard - Google Books |publisher=''[[Rolling Stone]]'' |date= |accessdate=July 28, 2015}}</ref>
*[[hardcore punk]]<ref name="ew1">{{cite web | url=http://www.ew.com/article/1992/05/08/body-count | title=Body Count | work=[[Entertainment Weekly]] | date=May 8, 1992 | accessdate=July 29, 2015 | author=Dimartino, Dave}}</ref>
*{{nowrap|[[rap metal]]<ref>{{cite web|title=Body Count’s Ice-T Talks Shit but Does the Shooting|url=http://decibelmagazine.com/blog/featured/body-counts-ice-t-talks-shit-but-does-the-shooting|website=Decibel Magazine|accessdate=May 2, 2015|date=June 4, 2014|quote=Back in the day, Body Count was categorized as “rap metal” or “nu metal.” Manslaughter sounds like just a straight-up metal album, without the hip-hop tropes that -- because of your background (and ethnicity) -- the band was initially categorized by.}}</ref><ref>{{cite book|last1=Metcalf|first1=Metcalf|editor1-last=Turner|editor1-first=Will|title=Rapper, Writer, Pop-Cultural Player: Ice-T and the Politics of Black Cultural Production|publisher=Ashgate Publishing Company|pages=109|quote=Moreover, the band's second album, 'Born Dead,' released on Virgin Records in September 1994, peaked at a lowly 74. Upon its release, the Los Angeles Times remarked that 'it's time to pull the plug on this genre [of rap-metal]. The novelty has worn off.'}}</ref><ref>{{cite web|last1=Workman|first1=Adam|title=Album review: Body Count – Manslaughter|url=http://www.thenational.ae/arts-lifestyle/music/album-review-body-count-x2013-manslaughter|website=TheNational|accessdate=May 2, 2015|date=June 23, 2014|quote=The uncompromising Los Angeles rap-metal crew Body Count clearly haven’t gone soft in the eight years since their last album.}}</ref>}}
}}
| years_active     = {{flatlist|
*1990–2006
*2009–present
}}
| label            = {{flatlist|
*[[Century Media Records|Century Media]]
*[[Sumerian Records|Sumerian]]
*Escapi
*[[Virgin Records|Virgin]]
*[[Sire Records|Sire]]
}}
| associated_acts  = {{flatlist|
*[[Ice T]]
*Ice Pick
*[[Megadeth]]
*[[Madball]]
*[[The Trolls]]
*[[Evildead]]
*[[Agent Steel]]
}}
| website          = {{URL|http://www.icet.com/}}
| current_members  = [[Ice-T]]<br />[[Ernie C]]<br />[[Laughing Colors|Ill Will]]<br />[[Steel Prophet|Vincent Price]]<br />[[Juan Garcia (guitarist)|Juan of the Dead]]<br />Sean E Sean<br />Little Ice
| past_members     = [[Mooseman]]<br />[[Victor Ray Wilson|Beatmaster V]]<br />[[D-Roc the Executioner]]<br />Griz<br />O.T.<br />Bendrix
}}

'''Body Count''' is an American [[heavy metal music|heavy metal]] band formed in [[Los Angeles]], [[California]], in 1990. The group is fronted by [[Ice-T]], who co-founded the group with lead guitarist [[Ernie C]] out of their interest in heavy metal music. Ice-T took on the role of vocalist and writing the lyrics for most of the group's songs. Lead guitarist Ernie C has been responsible for writing the group's music. Their controversial [[Body Count (album)|self-titled debut album]] was released on [[Sire Records]] in 1992.

The song "[[Cop Killer (song)|Cop Killer]]" was the subject of much controversy. Although Sire Records' parent company, [[Warner Bros. Records]], defended the single, Ice-T chose to remove the track from the album because he felt that the controversy had eclipsed the music itself. The group left Sire the following year. Since then, they have released four further albums on different labels, none of which have been received as commercially or critically well as their debut album.

Three out of the band's original six members are deceased: [[D-Roc the Executioner|D-Roc]] died from [[lymphoma]], [[Victor Ray Wilson|Beatmaster V]] from [[leukemia]] and [[Mooseman]] in a [[drive-by shooting]].

==History==

===Pre-formation (1990–1991)===
[[Ice-T]]'s interest in [[heavy metal music|heavy metal]] stemmed from sharing a room with his cousin Earl, who was a fan of [[rock music]] and only listened to the local rock stations. Ice-T particularly enjoyed heavy metal, citing [[Edgar Winter]], [[Led Zeppelin]] and [[Black Sabbath]] as his favorite bands.<ref name=IceCentury-127/> Ice-T attended [[Crenshaw High School]], where a few classmates shared his interest in the genre, including musicians [[Ernie C]], [[Mooseman]], [[Victor Ray Wilson|Beatmaster V]] and [[D-Roc the Executioner]]. Ice-T began a solo career as a rapper, and later decided to form Body Count with these friends.<ref name=IceCentury-127/><ref name="AllMusic">{{cite web |last=Erlewine |first=Stephen Thomas |title=Body Count Biography |url={{Allmusic|class=artist|id=p26209|pure_url=yes}} |accessdate=2007-10-08 |date= |year= |work= |publisher=[[AllMusic]] |pages= |doi= |archiveurl= |archivedate= |quote= }}</ref>

Ice-T co-wrote the band's music and lyrics with lead guitarist Ernie C, and took on the duties of lead vocalist, even though he felt that he did not have a great singing voice.<ref name="IceOpinion">{{cite book |author1=Ice T |last2=Sigmund |first2=Heidi |editor= |others= |title=The Ice Opinion: Who Gives a Fuck? |url= |accessdate= |edition= |series= |date= |year=1994 |publisher=Pan Books |location= |isbn=0-330-33629-0 |oclc= |doi= |id= |pages=99–101; 166–180 |chapter= |chapterurl= |quote= }}</ref> The original line-up consisted of Mooseman on [[bass guitar|bass]], Beatmaster V on [[drum kit|drums]] and D-Roc on [[rhythm guitar]].

===Touring and debut album (1991–1992) ===
Ice-T introduced the band at [[Lollapalooza]] in 1991, devoting half of his set to his hip hop songs, and half to Body Count songs, increasing his appeal with both alternative music fans and middle-class teenagers.<ref>{{cite web |url={{Allmusic|class=artist|id=p89063|pure_url=yes}} |title=Ice-T > Biography |accessdate=2007-10-09 |last=Erlewine |first=Stephen Thomas |work=[[AllMusic]] |publisher=[[Macrovision Corporation]]}}</ref> Some considered the Body Count performances to be the highlight of the tour.<ref>{{cite book |last=Apter |first=Jeff |authorlink= |editor= |others= |title=Fornication: The Red Hot Chili Peppers Story |url= |accessdate= |edition= |series= |date= |year=2004 |publisher=Omnibus Press |location= |isbn=1-84449-381-4 |oclc= |doi= |id= |page=250 |chapter= |chapterurl= |quote= }}</ref> The group made its first album appearance on Ice-T's 1991 solo album ''[[O.G. Original Gangster]]''. The song, "Body Count", was preceded by a spoken introduction in which Ice-T responds to allegations that he had "sold out" by incorporating rock elements into his rap albums by pointing out that rock music originated with [[African-American]] artists such as [[Chuck Berry]], [[Bo Diddley]] and [[Little Richard]], in addition to stating that "as far as I'm concerned, music is music. I don't look at it as rock, [[Rhythm and blues|R&B]], or all that kind of stuff. I just look at it as music. [...] I do what I like and I happen to like [[rock 'n' roll]], and I feel sorry for anybody who only listens to one form of music."<ref>Ice-T (1991). "Body Count". ''O.G. Original Gangster''. [[Sire Records|Sire]]/[[Warner Bros. Records]]. ISBN 7-5992-6492-2</ref>

Body Count's [[Body Count (album)|self-titled debut album]] was released on [[Sire Records|Sire]]/[[Warner Bros. Records]] on March 31, 1992. On the strength of the album, Body Count toured internationally, developing a strong following.<ref name=IceCentury-127/> When the group performed in [[Milan]], [[Italy]], some of the [[punk subculture|punks]] in the crowd began spitting at Ernie C.<ref name=IceCentury-127/> Ice-T attempted to calm the situation by telling the crowd not to spit, but the spitting continued.<ref name=IceCentury-127/> As the band prepared to play "[[Cop Killer (song)|Cop Killer]]", Ice-T identified an audience member who spit in his direction; Ice-T responded by rushing into the crowd and punching the spitter.<ref name=IceCentury-127/> As the band began to play, some of the audience began fighting with Ice-T. Body Count escaped the crowd mid-song, and the promoter immediately shut the concert down.<ref name=IceCentury-127/>

Outside the venue, angry audience members trashed the band's tour bus. The band hailed a taxicab, but its driver abandoned the cab when the mob surrounded the taxi, leading Body Count to steal the taxi in order to escape, abandoning it and their tour coats a mile away from the venue.<ref name=IceCentury-127/> They hailed another cab, and the driver attempted to take them back to the venue until the band screamed at the driver to take them to the hotel.<ref name=IceCentury-127/> The incident was the subject of much controversy and coverage on Italian television. The band appeared on a Milan radio station, where the disc jockey told his audience, "Some clowns tried to ruin his concert. We should be angry at them. Ice-T is a guest in our country, we invited him to do all these sold-out shows, and we love him!"<ref name=IceCentury-127/> Several Italian fans apologized for the behavior of the Milan audience.<ref name=IceCentury-127/>

===Controversy over the song Cop Killer (1992)===
{{main|Body Count (album)|Cop Killer (song)}}

The song "Cop Killer", intended to criticize corrupt [[police officers]], encountered controversy, as it was seen as an attack against the entire police force.<ref name="IceOpinion"/><ref name="Time">{{cite news |url=http://www.time.com/time/magazine/article/0,9171,976203,00.html |title=Ice T Melts |accessdate=2007-10-09 |last= |first= |authorlink= |date=August 10, 1992 |year= |work= |publisher=[[Time (magazine)|Time]] |pages= |archiveurl= |archivedate= |quote= }}</ref> According to Ice-T, "I thought I was safe. I thought within the world of rock'n'roll, you could be free to write what you want. Hell, I was listening to [[Talking Heads]] singin' '[[Psycho Killer]].' Fuck it, I'll make 'Cop Killer'! But, that was the cross of metal with something that was real. Now we’re not just killing your family, we’re killing somebody so real that everybody just went, 'oh shit.'"<ref name="Escapi">{{cite web |url=http://www.escapimusic.com/eu/index.php?option=com_content&task=view&id=108&Itemid=31 |title=Body Count |accessdate=2007-10-09 |last= |first= |authorlink= |date= |year= |work= |publisher=Escapi Music Group |pages= |archiveurl=https://web.archive.org/web/20070928084335/http://www.escapimusic.com/eu/index.php?option=com_content&task=view&id=108&Itemid=31 |archivedate=2007-09-28}}</ref>
{| border="0" style="font-size:80%; float:right; color:black; width:130px;"
|-
|
{{Listen|filename=Body Count Cop Killer.ogg|title="Cop Killer" (sample)|description=|format=[[Ogg]]}}
|}
The Dallas Police Association and the Combined Law Enforcement Association of Texas launched a campaign to force [[Warner Bros. Records]] to withdraw the album.<ref name="YouthMedia">{{cite book |last=Osgerby |first=Bill |authorlink= |editor= |others= |title=Youth Media |url= |accessdate= |edition= |series= |date= |year=2004 |publisher=Routledge |location= |isbn=0-415-23808-0 |oclc= |doi= |id= |pages=68–69 |chapter= |chapterurl= |quote= }}</ref> Within a week, they were joined by police organizations across the [[United States]].<ref name="YouthMedia"/> Some critics argued that the song could cause crime and violence.<ref name="YouthMedia"/> Many defended the song on the basis of the group's right to [[freedom of speech]]. In ''The Ice Opinion: Who Gives a Fuck'', Ice-T wrote that "The people who did have a platform were way off backing me on the First Amendment. That's not where all the anger should have been directed. The anger should have been generated back at the police. [...] Because people jumped on the wrong issue they were able to drive this thing totally through Warner Brothers."<ref name="IceOpinion"/>

Over the next month, controversy against the band grew. Vice President [[Dan Quayle]] branded "Cop Killer" as being "obscene," and President [[George H.W. Bush]] publicly denounced any record company that would release such a product.<ref name="YouthMedia"/> At a [[Time-Warner]] shareholders' meeting, actor [[Charlton Heston]] stood and read lyrics from the song "KKK Bitch" to an astonished audience and demanded that the company take action.<ref name="YouthMedia"/> The criticism escalated to the point where death threats were sent to Time-Warner executives, and [[shareholder]]s threatened to pull out of the company. Finally, Ice-T decided to remove "Cop Killer" from the album of his own volition.<ref name="IceOpinion"/><ref name="Time"/><ref name="Roc">{{cite web |url=http://www.theroc.org/roc-mag/textarch/roc-11/roc11-09.htm |title=Ice-T speaks out on censorship, Cop Killer, his leaving Warner Bros., and more|accessdate=2007-10-09 |last=Heck |first=Mike |authorlink= |date= |year= |work= |publisher=The Roc |pages= |archiveurl= |archivedate= |quote= }}</ref> In an interview, Ice-T stated that "I didn't want my band to get pigeon-holed as that's the only reason that record sold. It just got outta hand and I was just tired of hearing it. I said, 'fuck it,' I mean they're saying we did it for money, and we didn't. I'd gave the record away, ya know, let's move on, let's get back to real issues, not a record but the cops that are out there killing people."<ref name="Roc"/>

"Cop Killer" was replaced by a new version of "Freedom of Speech," a song from Ice-T's 1989 solo album ''[[The Iceberg/Freedom of Speech...Just Watch What You Say]]''. The song was re-edited and remixed to give it a more rock-oriented sound. Ice-T left Warner Bros. Records the following year because of disputes over the Ice-T solo album ''[[Home Invasion (album)|Home Invasion]]'',<ref name="IceOpinion"/> taking Body Count with him. Despite the controversy, the album received some praise, including A- reviews from ''[[Entertainment Weekly]]'' and ''[[The Village Voice]]'', who later ranked the album among their list of ''The 40 Best Albums of 1992''.<ref name="VillageVoice">{{cite news |first= |last= |authorlink= |title=The 40 Best Albums Of 1992 |url= |work= |publisher=[[The Village Voice]] |id= |pages= |page= |date=March 2, 1993 |quote= |archiveurl= |archivedate= }}</ref> ''[[Variety (magazine)|Variety]]'' reported that the album had sold 480,000 copies by January 29, 1993.<ref>{{cite news |last1=Augusto |first1=Troy J. |last2=Turman |first2=Katherine |title=WB board put Ice-T out in cold |url=http://www.variety.com/article/VR103473.html?categoryid=16&cs=1 |work= |publisher=[[Variety (magazine)|Variety]] |id= |pages= |page= |date=January 29, 1993 |accessdate=2007-10-09 |quote= |archiveurl= |archivedate= }}</ref>

===Continued albums: ''Born Dead'', ''Violent Demise'' and ''Murder 4 Hire'' (1993–2008)===
[[File:Icet.jpg|right|thumb|Ice-T performing with Body Count in 2006.]]
In 1993, Body Count recorded a cover of "[[Hey Joe]]" for the [[Jimi Hendrix]] [[tribute album]] ''[[Stone Free: A Tribute to Jimi Hendrix]]''.<ref>{{cite web |url={{Allmusic|class=album|id=r188440|pure_url=yes}} |title=''Stone Free: A Tribute to Jimi Hendrix'' overview |accessdate=2007-10-09 |last=Erlewine |first=Stephen Thomas |authorlink= |date= |year= |work= |publisher=[[AllMusic]] |pages= |doi= |archiveurl= |archivedate= |quote= }}</ref> The band released their second album, ''[[Born Dead]]'' in 1994 on [[Virgin Records]]. Prior to the recording of Body Count's third album ''[[Violent Demise: The Last Days]]'' (1997), bassist Mooseman left the group and was replaced by Griz. Drummer Beatmaster V died of [[leukemia]] soon after the album was completed,<ref name="RSdeaths">{{cite web|url=http://www.rollingstone.com/artists/icet/articles/story/6436921/body_count_guitarist_dead |title=Body Count Guitarist Dead |accessdate=2007-10-09 |last=Devenish |first=Colin |authorlink= |date=August 19, 2004 |year= |work= |publisher=Rolling Stone |pages= |quote= |deadurl=yes |archiveurl=https://web.archive.org/web/20071013111109/http://www.rollingstone.com:80/artists/icet/articles/story/6436921/body_count_guitarist_dead |archivedate=2007-10-13 |df= }}</ref> and a new drummer named O.T. filled in the position. Bassist Griz left the band later on, and in the meanwhile, former bassist Mooseman was shot in a drive-by shooting in February 2001 after recording an album and preparing for another tour with [[Iggy Pop]] in his band the Trolls.<ref name="RSdeaths"/> In late 2004, rhythm guitarist D-Roc died due to complications from lymphoma, leaving only Ice-T and Ernie C from the original line-up.<ref name="RSdeaths"/>

Ice-T has stated that "For me, honestly, after something like that, you can either come to a dead stop or you can go on. [...] It was so emotional. We were in the middle of making a new record together and he goes and dies? It was like, 'damn!' Soon enough, though, everybody was like, 'c'mon c'mon you gotta do it.' It was make-or-break. The key essence of Body Count is it's a band made up of friends. It's not about going out and hiring the best drummer or the best guitarist. If we don't know you, you can’t be in the band."<ref name="Escapi"/>

In July 2006, Body Count released their fourth album, ''[[Murder 4 Hire]]'' on the indie record label Escapi Music.<ref name="Escapi"/> Its album cover, featuring [[Uncle Sam]] holding a cardboard sign reading "Will Kill for Money," compares the [[United States military]] to [[contract killers]].<ref name="Decibel">{{cite web |url=http://www.decibelmagazine.com/features_detail.aspx?id=4908 |archiveurl=https://web.archive.org/web/20061020204850/http://www.decibelmagazine.com/features_detail.aspx?id=4908 |archivedate=2006-10-20 |title=Interview with Ice-T |accessdate=2007-10-09 |last=Bennett |first=J |publisher=[[Decibel Magazine]]}}</ref> The then-line-up included drummer O.T., bassist Vincent Price and rhythm guitarist Bendrix. The band then took an extended hiatus for a couple of years; in regards to the future of Body Count, Ernie C stated, "We will carry on the band. I don't know if it will be Body Count, but in some form, Ice and I will always play together."<ref name="RSdeaths"/>

===''Gears of War 3'', ''Manslaughter'' and ''Bloodlust'' (2009–present)===
On September 6, 2009, Body Count made an appearance at the [[Warped Tour|Vans Warped Tour]] 15th-anniversary party at Club Nokia in downtown Los Angeles. The group played a 20-minute set, covered [[Slayer]], and closed with their controversial classic "Cop Killer".<ref>{{cite web |url=http://www.roadrunnerrecords.com/blabbermouth.Net/news.aspx?mode=Article&newsitemID=126679 |date=2009-09-09 |title=Body Count Is Back! |accessdate=2009-09-11 |publisher=[[Blabbermouth.net]]}}</ref> Also on the bill were [[NOFX]], [[Katy Perry]], [[Pennywise (band)|Pennywise]], [[Bad Religion]] and [[Rise Against]]. Mike Sullivan of [[ExploreMusic]] recently caught up with Ernie C at the 2010 edition of the Vans Warped Tour. While briefly chatting, Ernie C divulged that the band is recording its fifth studio album.<ref name="Explore Music">{{cite web |url=http://www.exploremusic.com/explore-music-radio/2010-07-06/Heres-The-Deal-MySpace-Google-Iron-Maiden-Body-Count |title=Interview with Ernie C |accessdate=2010-07-07 |publisher=[[ExploreMusic]]}}</ref> Body Count wrote an exclusive song, "The Gears of War", for the video game ''[[Gears of War 3]]'', and performed it at a party promoting the game.<ref>{{cite web|url=http://kotaku.com/5809789/what-do-ice+t-gears-of-war-3-and-heavy-metal-have-in-common |title=This Was Ice-T's Gears of War 3 Concert |publisher=Kotaku.com |date=2011-06-08 |accessdate=2012-03-01}}</ref>

On December 9, 2012, Ice-T announced on [[Twitter]] that Body Count would begin production on a fifth studio album in January 2013.<ref>{{cite web|url=http://twitter.com/FINALLEVEL/status/277873968701132800 |title=Twitter / FINALLEVEL: BodyCount Fans: BodyCount is |publisher=Twitter.com |date= |accessdate=2013-02-18}}</ref> The following day, Ice-T revealed that Body Count has signed with [[Sumerian Records]].<ref name=Sumerian>{{cite web|url=http://twitter.com/FINALLEVEL/status/278109056772763648 |title=Twitter / FINALLEVEL: FYI: The BodyCount deal is |publisher=Twitter.com |date= |accessdate=2013-02-18}}</ref> Ice-T suggested that the album was going to be titled ''Rise!''<ref name=Sumerian/> or ''Manslaughter''.<ref>{{cite web|url=http://twitter.com/FINALLEVEL/status/279255464938721280 |title=Twitter / FINALLEVEL: BodyCount starts recording |publisher=Twitter.com |date= |accessdate=2013-02-18}}</ref><ref>{{cite web|author= |url=http://twitter.com/FINALLEVEL/status/281541286345986048 |title=Twitter / FINALLEVEL: BodyCount at 'Warpped Tour' |publisher=Twitter.com |date= |accessdate=2013-02-18}}</ref>

On May 10, 2013, Ice-T announced that work on the fifth studio album had begun and that it would be titled ''[[Manslaughter (album)|Manslaughter]]''. The album was released on June 10, 2014.<ref>{{cite web|url=http://www.blabbermouth.net/news/ice-t-says-new-body-count-album-manslaughter-is-brutal/ |title=Ice T Says New Body Count Album 'Manslaughter' Is 'Brutal' |publisher=Blabbermouth.net |date=2014-05-05 |accessdate=2014-05-08}}</ref> On May 13, 2014, Ice-T played the song "Talk Shit, Get Shot" as a teaser for the new album.

The sixth Body Count album, titled ''[[Bloodlust (Body Count album)|Bloodlust]]'', is expected to be released in March 2017 via [[Century Media Records]].<ref>{{cite web|url=http://www.metalsucks.net/2016/06/29/body-count-release-new-album-2017-cover-raining-blood-like-fucking-boss/ |title=Archived copy |accessdate=2016-07-12 |deadurl=no |archiveurl=https://web.archive.org/web/20160817111935/http://www.metalsucks.net/2016/06/29/body-count-release-new-album-2017-cover-raining-blood-like-fucking-boss/ |archivedate=2016-08-17 |df= }}</ref> On December 28 2016, Ice T posted a preview of the first single "No Lives Matter" to [[Twitter]]. Guest musicians confirmed to appear on the album are [[Max Cavalera]], [[Randy Blythe]] and [[Dave Mustaine]]. Upon the release of ''Bloodlust'', it was confirmed that Ice T's son Tracy Marrow Jr, aka Little Ice, is now a part of the band, performing backing vocals.

==Style==

===Lyrics===
Ice-T's lyrics focus on reality-based themes, including [[gang]] life, because he felt it would be scarier than the fantasy-based horror themes of most heavy metal bands.<ref name=IceCentury-127/> The band's third album, ''[[Violent Demise: The Last Days]]'', featured album cover art depicting the hand signs of these gangs.<ref name=IceCentury-127/> According to Ice-T, "We named the group Body Count because every Sunday night in L.A., I'd watch the news, and the newscasters would tally up the youths killed in gang homicides that week and then just segue to sports. 'Is that all I am,' I thought, 'a body count?'"<ref name="IceOpinion"/>

When the band's debut album was released, Ice-T defined it as being "a rock album with a rap mentality."<ref name="Dellamora" /> Like Ice-T's hip hop albums, the group's material focused on various social and political issues, with songs focusing on topics ranging from police brutality to drug abuse. Ernie C has stated that "We were just a band that played the songs that we knew how to write. Everybody writes about whatever they learned growing up, and we were no exception. Like the [[Beach Boys]] sing about the beach, we sing about the way we grew up."<ref name="MorningCall">{{cite news |last=Yoxheimer |first=Aaron |authorlink= |title=Despite a high body count of its own, band is a survivor |url=http://www.popmatters.com/pm/article/despite-a-high-body-count-of-its-own-band-is-a-survivor |work= |publisher=[[The Morning Call]] |id= |pages= |page= |date=April 6, 2007 |accessdate=2007-10-09 |quote= |archiveurl= |archivedate= }}</ref>

===Music===
Body Count's musical style derives from the dark, ominous tone of [[heavy metal music|heavy metal]] bands such as [[Black Sabbath]], as well as faster [[thrash metal]] bands such as [[Slayer]].<ref name=IceCentury-127>{{cite book |last1=Marrow |first1=Tracy |last2=Century |first2=Douglas |title=Ice: A Memoir of Gangster Life and Redemption—from South Central to Hollywood |year=2011 |publisher=Random House |isbn=978-0-345-52328-0 |pages=127–140 |chapter=Freedom of Speech }}</ref> The band's music is described as [[speed metal]]<ref name="Rose">{{cite book |last=Rose |first=Tricia |title=Black Noise: Rap Music and Black Culture in Contemporary America |year=1994 |publisher=Wesleyan University Press |isbn=0-8195-6275-0 |page=130}}</ref><ref name="Austin">{{cite book |last1=Austin |first1=Joe |last2=Willard |first2=Michael Nevin |title=Generations of Youth: Youth Cultures and History in Twentieth-century America |year=1998 |publisher=NYU Press |isbn=0-8147-0646-0 |pages=401–402 }}</ref> and thrash metal.<ref name="Dellamora">{{cite book |last=Dellamora |first=Richard |title=Postmodern Apocalypse: Theory and Cultural Practice at the End |year=1995 |publisher=University of Pennsylvania Press |isbn=0-8122-1558-3 }}</ref><ref name="Christie">{{cite book |last=Christie |first=Ian |title=Sound of the Beast: The Complete Headbanging History of Heavy Metal |year=2003 |publisher=HarperCollins |isbn=0-380-81127-8 |page=300 }}</ref> According to Ernie C, "We wanted to be a big [[punk rock|punk]] band [...] Our first record is almost a punk record."<ref name="MorningCall"/> The presence of a rapper in a heavy metal band has been credited for paving the way for the rise of [[rap metal]] and [[nu metal]],<ref name="CNN">{{cite news |url=http://www.cnn.com/SHOWBIZ/Music/9910/27/ice.t/ |title=No thaw for rapper Ice T |accessdate=2007-10-09 |last=Freydkin |first=Donna |date=October 27, 1999 |publisher=CNN |archiveurl=https://web.archive.org/web/20050212155357/http://www.cnn.com/SHOWBIZ/Music/9910/27/ice.t/ |archivedate=2005-02-12}}</ref><ref>{{cite book |last=Taylor |first=Steve |title=A to X of Alternative Music |year=2006 |publisher=Continuum International Publishing Group |isbn=978-0-8264-8217-4 |page=127 |chapter=Ice-T }}</ref> even though Ice-T does not [[rapping|rap]] in most Body Count songs, and considers it to solely be a [[rock music|rock]] band.<ref name=IceCentury-127/> According to Ernie C, "A lot of rappers want to be in a rock band, but it has to be done sincerely. You can’t just get anybody on guitar and expect it to work. [...] Ice and I, on the other hand, really loved the music we were doing, and it showed."<ref name="MorningCall"/>

==Members==
{{col-begin}}
{{col-2}}

;Current
*[[Ice-T]] – [[singing|vocals]] <small>(1990—2006, 2009-present)</small>
*[[Ernie C]] – [[lead guitar]], backing vocals <small>(1990—2006, 2009-present)</small>
*Sean E Sean – [[Sampler (musical instrument)|sampler]], backing vocals <small>(1990—2006, 2009-present)</small>
*[[Steel Prophet|Vincent Price]] – [[bass guitar|bass]], backing vocals <small>(2001—present)</small>
*Ill Will – [[drum kit|drums]] <small>(2009—present)</small>
*[[Juan Garcia (guitarist)|Juan of the Dead]] – [[rhythm guitar]], backing vocals <small>(2013—present)</small>
*Little Ice – backing vocals <small>(2017–present)</small>
{{col-2}}
;Former
*[[Beatmaster V]] – drums <small>(1990—death in 1996)</small>
*[[Mooseman]] – bass, backing vocals <small>(1990—1996, died in 2001)</small>
*[[D-Roc the Executioner]] – rhythm guitar, backing vocals <small>(1990—death in 2004)</small>
*Griz – bass, backing vocals <small>(1996—2001)</small>
*O.T. – drums <small>(1996—2006)</small>
*Bendrix – rhythm guitar, backing vocals <small>(2004—2006)</small>
{{col-end}}

===Timeline===
{{#tag:timeline|
ImageSize = width:800 height:auto barincrement:20
PlotArea = left:120 bottom:80 top:0 right:15
Alignbars = justify
DateFormat = mm/dd/yyyy
Period = from:01/01/1990 till:01/01/2018
TimeAxis = orientation:horizontal format:yyyy
Legend = orientation:vertical position:bottom columns:4
ScaleMajor = increment:2 start:1990
ScaleMinor = increment:1 start:1991

Colors =
 id:vocals  value:red     legend:Vocals
 id:lguitar value:teal    legend:Lead_guitar,&nbsp;Backing_vocals
 id:rguitar value:green   legend:Rhythm_guitar,&nbsp;Backing_vocals
 id:bass    value:blue    legend:Bass,&nbsp;Backing_vocals
 id:drums   value:orange  legend:Drums
 id:sample  value:purple    legend:Sampler,&nbsp;Backing_vocals
 id:Lines1  value:black   legend:Studio_releases

LineData =
 at:03/10/1992 color:Lines1 layer:back
 at:09/06/1994 color:Lines1 layer:back
 at:03/11/1997 color:Lines1 layer:back
 at:08/01/2006 color:Lines1 layer:back
 at:06/10/2014 color:Lines1 layer:back
 at:03/15/2017 color:Lines1 layer:back

BarData =
 bar:IceT text:"Ice-T"
 bar:ErnieC text:"Ernie C"

 bar:DRoc text:"D-Roc The Executioner"
 bar:Bendrix text:"Bendrix"
 bar:Juan text:"Juan of the Dead"

 bar:Mooseman text:"Mooseman"
 bar:Griz text:"Griz"
 bar:VincentPrice text:"Vincent Price"
 
 bar:BeatmasterV text:"Beatmaster V"
 bar:OT text:"O.T."
 bar:IllWill text:"Ill Will"

 bar:Sean text:"Sean E Sean"

PlotData=
 width:11 textcolor:black align:left anchor:from shift:(10,-4)
 bar:IceT from:01/01/1990 till:08/31/2006 color:vocals
 bar:IceT from:09/06/2009 till:end color:vocals

 bar:ErnieC from:01/01/1990 till:08/31/2006 color:lguitar
 bar:ErnieC from:09/06/2009 till:end color:lguitar

 bar:DRoc from:01/01/1990 till:08/17/2004 color:rguitar
 bar:Bendrix from:08/17/2004 till:08/31/2006 color:rguitar
 bar:Juan from:11/09/2013 till:end color:rguitar

 bar:Mooseman from:01/01/1990 till:03/01/1996 color:bass
 bar:Griz from:03/01/1996 till:02/01/2001 color:bass
 bar:VincentPrice from:02/01/2001 till:08/31/2006 color:bass
 bar:VincentPrice from:09/06/2009 till:end color:bass

 bar:BeatmasterV from:01/01/1990 till:04/30/1996 color:drums
 bar:OT from:05/30/1996 till:08/31/2006 color:drums
 bar:IllWill from:09/06/2009 till:end color:drums

 bar:Sean from:01/01/1990 till:08/31/2006 color:sample
 bar:Sean from:09/06/2009 till:end color:sample
}}

==Discography==

===Studio albums===
*''[[Body Count (album)|Body Count]]'' (1992)
*''[[Born Dead]]'' (1994)
*''[[Violent Demise: The Last Days]]'' (1997)
*''[[Murder 4 Hire]]'' (2006)
*''[[Manslaughter (album)|Manslaughter]]'' (2014)
*''[[Bloodlust (Body Count album)|Bloodlust]]'' (2017)

===Singles===
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
!rowspan="2"|Year
!rowspan="2"|Single
!colspan="5"|Peak chart positions
!rowspan="2"|Album
|- style="font-size:smaller"
!width="35"|<small>[[ARIA Charts|AUS]]</small><br><ref>Australian ([[ARIA Charts|ARIA Chart]]) peaks:
*Top 50 peaks: {{cite web|url=http://australian-charts.com/showinterpret.asp?interpret=Body+Count|title=australian-charts.com > Body Count in Australian Charts|publisher=Hung Medien|accessdate=24 August 2016}}
*Top 100 peaks to December 2010: {{cite book|last=Ryan|first=Gavin|title=Australia's Music Charts 1988-2010|year=2011|publisher=Moonlight Publishing|location=Mt. Martha, VIC, Australia}}
*"Hey Joe": {{cite web|url=http://i.imgur.com/qCLjUoj.jpg|title=The ARIA Australian Top 100 Singles Chart – Week Ending 06 Mar 1994|publisher=Imgur.com (original document published by [[Australian Recording Industry Association|ARIA]])|accessdate=24 August 2016}}
*"Born Dead" (single): {{cite web|url=http://i.imgur.com/Z9W21Rc.jpg|title=The ARIA Australian Top 100 Singles Chart – Week Ending 13 Nov 1994|publisher=Imgur.com (original document published by ARIA)|accessdate=24 August 2016}}</ref>
!width="35"|<small>[[Media Control Charts|GER]]</small><br><ref>{{cite web|url=http://germancharts.de/search.asp?cat=s&artist=Body+Count&artist_search=starts&title=&title_search=starts |title=Body Count - German Chart |publisher=germancharts.de |accessdate=15 January 2016}}</ref>
!width="35"|<small>[[Official New Zealand Music Chart|NZ]]</small><br><ref>{{cite web|url=http://charts.org.nz/search.asp?cat=s&artist=Body+Count&artist_search=starts&title=&title_search=starts |title=Body Count - New Zealand Chart |publisher=charts.org.nz |accessdate=15 January 2016}}</ref>
!width="35"|<small>[[Swiss Hitparade|SWI]]</small><br><ref>{{cite web|url=http://hitparade.ch/search.asp?cat=s&from=&to=&artist=Body+Count&artist_search=starts&title=&title_search=starts |title=Body Count - Swiss Chart |publisher=hitparade.ch |accessdate=15 January 2016}}</ref>
!width="35"| <small>[[UK Singles Chart|UK]]</small><br><ref>{{cite web|url=http://www.officialcharts.com/artist/29452/body-count/ |title=Body Count - UK Chart |publisher=The Official Charts Company |accessdate=15 January 2016}}</ref>
|-
| rowspan="2"|1992
! scope="row"| "[[There Goes the Neighborhood (Body Count song)|There Goes the Neighborhood]]"
| 86 || — || 35 || — || —
|align="left" rowspan="2"|''Body Count''
|-
! scope="row"| "The Winner Loses"
| — || — || — || — || —
|-
| rowspan="1"|1993
! scope="row"| "Hey Joe"
| 67 || — || 31 || — || —
|align="left" rowspan="1"|''[[Stone Free: A Tribute to Jimi Hendrix]]''
|-
| rowspan="2"|1994
! scope="row"| "Born Dead"
| 52 || 32 || — || 44 || 28
|align="left" rowspan="2"|''Born Dead''
|-
! scope="row"| "Necessary Evil"
| — || — || — || 41 || 45
|-
|align="center" colspan="10" style="font-size:8pt"| "—" denotes releases that did not chart or were not released.
|}

==Videos==
;DVD
*''Murder 4 Hire'' (2004)
*''Live in L.A.'' (2005)
*''Smoke Out Festival Presents: Body Count'' (2005)
;Music Videos
*''The Winner Loses'' (1992)
*''There Goes The Neighborhood'' (1992)
*''Body Count's In The House'' (1992)
*''Hey Joe'' (1993)
*''Born Dead'' (1994)
*''Necessary Evil'' (1994)
*''Medley: Masters Of Revenge/Killin' Floor/Drive By/Street Lobotomy'' (1994)
*''I Used To Love Her'' (1997)
*''Relationships'' (2006)
*''Talk Shit, Get Shot'' (2014)
*''Institutionalized 2014'' (2015)
*''No Lives Matter'' (2017)
*''Black Hoodie'' (2017)

==References==
{{Reflist|2}}

==External links==
{{Commonscat|Body Count (Band)|Body Count}}
*[http://bodycountband.com/ Official Body Count website]

{{Body Count}}
{{Good article}}

{{Authority control}}
[[Category:Body Count| ]]
[[Category:African-American musical groups]]
[[Category:African-American heavy metal musical groups]]
[[Category:Heavy metal musical groups from California]]
[[Category:Ice-T]]
[[Category:Sire Records artists]]
[[Category:Virgin Records artists]]
[[Category:Sumerian Records artists]]
[[Category:Musical groups from Los Angeles]]
[[Category:Musical groups established in 1990]]
[[Category:Musical quintets]]
[[Category:American thrash metal musical groups]]
[[Category:Speed metal musical groups]]
[[Category:Hardcore punk groups from California]]