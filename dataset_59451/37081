{{good article}}
{{redirect|Baahubali}}
{{Use Indian English|date=December 2015}}
{{Use dmy dates|date=December 2015}}
{{Infobox deity
| type             = Jain
| deity_of         = [[God in Jainism|Jain deity]]
| image            = The statue of Gommateshvara Bahubali dating 978-993 AD..jpg
| caption          = The 57ft high [[Gommateshwara statue]] at [[Shravanabelagola]], Karnataka, was built in 981 A.D
| alt              = Bahubali
| other_names      = Gommateshwara (ಗೊಮ್ಮಟೇಶ್ವರ)
| Kannada_script   = ಬಾಹುಬಲಿ
| Kannada_transliteration = Bāhubali
| father           = [[Adinath Tirthankar]]
| mother           = Sunanda
| children         = Somakirti (also known as Mahabala)
| height           = 525 bows (157.5 metres)
| birth_place  = [[Ayodhya]]
| moksha_place = [[Mount Kailash]]
}}
{{Jainism}}
'''Bahubali''' ({{Lang-en|One With Strong Arms}}), a much revered figure among [[Jainism|Jains]], was the son of [[Rishabhanatha|Adinath]], the first ''[[tirthankara]]'' of Jainism, and the younger brother of [[Bharata Chakravartin]]. He is said to have meditated motionless for one year in  a standing posture (''[[kayotsarga]]'') and that during this time, climbing plants grew around his legs. After his year of meditation, Bahubali is said to have attained omniscience (''[[Kevala Jnana]]''). According to Jain texts, Bahubali's soul was liberated from the cycle of births and deaths (''[[Moksha (Jainism)|moksha]]'' ) at [[Mount Kailash]]. He is revered as a liberated soul (''[[Siddha]]'') by the Jains.

Bahubali is also called '''Gommateshwara''' because of the [[Gommateshwara statue]] dedicated to him. The statue was built by the [[Western Ganga Dynasty|Ganga dynasty]] minister and commander [[Chavundaraya]]; it is a {{convert|57|ft|m|adj=on}} monolith (statue carved from a single piece of rock) situated above a hill in [[Shravanabelagola]] in the [[Hassan district]], [[Karnataka]] state, [[India]]. It was built in around 981 A.D. and is one of the largest free-standing statues in the world.

== Legends ==
The ''[[Ādi purāṇa]]'', a 9th-century [[Sanskrit]] poem, deals with the ten lives of the first ''[[tirthankara]]'', [[Rishabhanatha]] and his two sons [[Bharata Chakravartin|Bharata]] and Bahubali. It was composed by [[Jinasena]], a ''[[Digambara monk]]''.{{sfn|Granoff|1993|p=208}}

===Family life===
According to Jain texts, Bahubali was born to Rishabhanatha and Sunanda during the [[Ikshvaku dynasty]] in [[Ayodhya]].{{sfn|Sangave|1981|p=66}}{{sfn|Zimmer|1953|p=212}}{{sfn|Champat Rai Jain|1929|p=xv}}{{sfn|Dundas|2002|p=120}} He is said to have excelled in studying medicine, archery, [[floriculture]], and the knowledge of precious gems. Bahubali had a son named Somakirti (also known as Mahabala).{{sfn|Champat Rai Jain|1929|p=106}} When Rishabhanatha decided to become a monk, he distributed his kingdom among his 100 sons. Bharata was gifted the kingdom of Vinita ([[Ayodhya]]) and Bahubali got the kingdom of Asmaka from [[South India]], having Podanapur as its capital.{{sfn|Sangave|1981|p=67}} After winning six divisions of earth in all directions (''digvijaya''), Bharata proceeded to his capital Ayodhyapuri with a huge army and divine ''chakra-ratna''—spinning, disk-like super weapon with serrated edges.{{sfn|Sangave|1981|p=67}} But the ''chakra-ratna'' stopped on its own at the entrance of Ayodhyapuri, signalling to the emperor that his 99 brothers have yet not submitted to his authority.{{sfn|Vijay K. Jain|2013|p=x}} Bharata's 98 brothers became [[Jain monk]]s' and submitted their kingdoms to him. Bahubali was endowed with the final and superior body of extraordinary sturdiness and strength (''vajra-ṛṣabhanārācasaṃhanana'') like Bharata.{{sfn|Vijay K. Jain|2013|p=xi}} He hurled open defiance at the ''[[chakravartin]]'' and challenged him to a fight.{{sfn|Champat Rai Jain|1929|p=143}}

The ministers on both sides gave the following argument to prevent war; "The brothers themselves, cannot be killed by any means; they are in their last incarnations in transmigration, and possess bodies which no weapon may mortally wound in warfare! Let them fight out the issue by themselves in other ways."{{sfn|Champat Rai Jain|1929|p=144}} It was then decided that to settle the dispute, three kinds of contests between Bharata and Bahubali would be held. These were eye-fight (staring at each other), water-fight (''jala-yuddha''), and wrestling (''[[malla-yuddha]]''). Bahubali won all the three contests over his elder brother, Bharata.{{sfn|Sangave|1981|p=67}}{{sfn|Champat Rai Jain|1929|p=105}}

=== Renunciation ===
[[File:Badami, Höhle 4, Bahubali (1999).jpg|thumb|upright 0.75|Sculpture depicting Bahubali's meditation in ''[[Kayotsarga]]'' posture with vines enveloped around his body (Photo: Badami caves)]]

After the fight, Bahubali was filled with disgust at the world and developed a desire for renunciation. Bahubali abandoned his clothes and kingdom to become a ''Digambara'' monk and began meditating with great resolve to attain omniscience (''[[Kevala Jnana]]'').{{sfn|Champat Rai Jain|1929|p=145}}

He is said to have meditated motionless in a standing posture (''[[kayotsarga]]'') for a year, during which time climbing plants grew around his legs.{{sfn|Champat Rai Jain|1929|p=145–146}} However, he was adamant and continued his practice unmindful of the vines, ants, and dust that enveloped his body. According to Jain text ''Ādi purāṇa'', on the last day of Bahubali's one year long fast, Bharata came in all humility to Bahubali and worshiped him with veneration and respect. A painful regret that he had been the cause of his elder brother's humiliation had been disturbing Bahubali's meditation; this was dispersed when Bharata worshipped him.<ref>{{cite book|title=Ādipurāṇa|author=Āchārya Jinasena|publisher=[[Bharatiya Jnanpith]]|isbn=978-81-263-1844-5|p=217}}</ref> Bahubali was then able to destroy the four kinds of inimical ''[[Karma in Jainism|karmas]]'', including the knowledge obscuring karma, and he attained omniscience (''kevala jnana''). He was now revered as an omniscient being (''[[Kevali]]'').{{sfn|Champat Rai Jain|1929|p=145–146}} Bahubali finally attained liberation (''[[Moksha (Jainism)|moksha]]'') and became a pure, liberated soul (''siddha'').{{sfn|Champat Rai Jain|1929|p=146}} He is said to be the first ''Digambara'' monk to have attained ''moksha'' in the present half-cycle of time (''avasarpini'').{{sfn|Sangave|1981|p=66}}

== Statues==
There are five monolithic statues of Bahubali  measuring more than 6&nbsp;m (20 feet) in height in [[Karnataka]]:

* 17.4&nbsp;m (57 feet) at [[Shravanabelagola]] in [[Hassan District]] in 981 AD{{sfn|Sangave|1981|p=66}}{{sfn|Sangave|1981|p=25}}<ref name=toi/>
* 12.8&nbsp;m (42 feet) at [[Karkala]] in [[Udupi District]] in 1430 AD<ref name=toi/>
* 11.9&nbsp;m (39 feet) at [[Dharmasthala]] in [[Dakshina Kannada]] District in 1973 AD<ref name=toi/>
* 10.7&nbsp;m (35 feet) at [[Venur]] in [[Dakshina Kannada]] District in 1604 AD<ref name=toi/>
* 6&nbsp;m (20 feet) at [[Gommatagiri]] in [[Mysore District]] in 12th Century AD<ref name=hindu/>

These statues will be briefly discussed in order now.

=== Shravanabelagola ===
{{main article|Gommateshwara statue}}
The monolithic statue of Bahubali at Shravanabelagola, located {{Convert|158|km|miles|abbr=on}} from [[Bangalore]], was carved from a single block of granite. The statue was built by the [[Western Ganga Dynasty|Ganga dynasty]] minister and commander [[Chavundaraya]]; it is {{convert|57 |ft|m|adj=on}} tall and is situated above a hill in Shravanabelagola, in the Hassan district of Karnataka. It was built in and around 981 A.D. and is one of the largest free-standing statues in the world.{{sfn|Sangave|1981|p=66}}{{sfn|Zimmer|1953|p=212}}{{sfn|Rice|1889|p = 53}} The statue is visible from {{Convert|25|km|miles}} away.{{sfn|Saran|2014|p=118}} Shravanabelagola has remained a centre of pilgrimage (''[[Tirtha (Jainism)|tirtha]]'') for the Jains.<ref>{{citation |title=March of Mysore |url=https://books.google.com/books?id=97YeAQAAIAAJ |date=1966 |publisher=[[University of California]] |volume=3 |p=56 }}</ref>

=== Karkala ===
{{Main|Karkala}}
[[File:Gomateshwara Statue, Karkala.jpg|thumb|upright 0.75|left||Bahubali monolith of [[Karkala]]]]
Karkala is known for its {{convert|42|feet|m|abbr=on|adj=on}} monolithic statue of Gomateshwara Bahubali, which is believed to have been built around 1432 and is the second-tallest statue in the State.{{sfn|Sangave|1981|p=90}}<ref name=toi>{{citation |last=Pinto |first=Stanley |authorlink=Stanley Pinto |title=12-year wait ends, all eyes on 42-ft-tall Karkala Bahubali |url=http://timesofindia.indiatimes.com/city/mangaluru/12-year-wait-ends-all-eyes-on-42-ft-tall-Karkala-Bahubali/articleshow/45964077.cms |work=[[The Times of India]] |date=21 January 2015 |agency=[[Times News Network]] |location=[[Mangaluru]] }}</ref> The statue is built on an elevated platform on top of a rocky hill. It was consecrated on 13 February 1432 by Veera Pandya Bhairarasa Wodeyar, scion of the Bhairarasa Dynasty, feudatory of the Vijayanagar Ruler.<ref name=toi/><ref>{{citation |title=Bahubali abhisheka from today |url=http://thehindu.com/news/national/karnataka/bahubali-abhisheka-from-today/article6806743.ece |work=[[The Hindu]] |date=21 January 2015 }}</ref>

===Dharmastala===
[[File:Gomateshwara of Dharmasthala.jpg|thumb|Bahubali monolith of [[Dharmasthala]] (1973 CE)]]
{{Main|Dharmastala}}
 
A {{convert|39|feet|m|adj=on|abbr=out}} high statue with a {{convert|13|ft|m|adj=on|abbr=out}} pedestal that weighs about {{Convert|175|tonnes|kg|abbr=on}} is installed at Dharmasthala in Karnataka.<ref>http://www.herenow4u.net/index.php?id=86311</ref><ref name=toi/>

=== Venur ===
[[File:Venur_-_Bahubali.JPG|thumb|upright 0.75|Bahubali monolith of [[Venur]]]]
{{Main|Venur}}
Venur is a small town in Dakshina Kannada district, Karnataka state, situated on the bank of the [[Gurupura River]]. Thimmanna Ajila built a {{Convert|38|feet|m|abbr=out|adj=on}} colossus of Gommateshwara there in 1604 AD.{{sfn|Sangave|1981|p=90}}<ref name=toi/>{{sfn|Titze|1998|p=48}} The staue at Venur is the shortest of the three Gommateshwaras within {{Convert|250|km|miles|abbr=on}} around it. It tands in an enclosure on the same pattern as that of the statue at Shravanabelagola. The Kings of Ajila Dynasty ruled here from 1154 to 1786.<ref>{{citation |last=Pinto |first=Stanley |authorlink=Stanley Pinto |title=10-day Mahamastakabhisheka at Karkala from today |url=http://timesofindia.com/city/mangaluru/10-day-Mahamastakabhisheka-at-Karkala-from-today/articleshow/45962346.cms |work=[[The Times of India]] |date=21 January 2015 |location=[[Mangaluru]] |agency=[[Times News Network|TNN]] }}</ref>

=== Gommatagiri ===
{{Main|Gommatagiri}}
[[File:Statue of Bahubali at Gommatagiri, Mysore.JPG|thumb|left|upright 0.75|Bahubali monolith of [[Gommatagiri]], Mysore]]
Gommatagiri is an acclaimed Jain centre. The 12th-century granite statue of Bahubali, also known as Gomateshwara, is erected atop a {{Convert|50|m|feet|abbr=out|adj=on}} tall hillock called 'Shravana Gudda'.<ref name=hindu>{{citation |title=Gommatagiri statue crying for attention |url=http://www.thehindu.com/todays-paper/tp-national/tp-karnataka/article3243137.ece |work=[[The Hindu]] |date=22 January 2006 }}</ref> The Jain centre attracts many pilgrims during the annual ''[[Mahamastakabhisheka]]'' in September.{{sfn|Sangave|1981|p=90}}<ref name=hindu/> The statue at Gommatagiri is very similar to the {{Convert|58|feet|m|abbr=out|adj=on}} Gommateshwara statue in Shravanabelagola, except that it is smaller. Historians attribute the statue to an early Vijayanagar period.<ref name=hindu/>

=== Kumbhoj ===
[[File:BahubaliKumbhoj.JPG|thumb|{{convert|28|ft|m|adj=on|abbr=out}}-high monolith of Bahubali at [[Kumbhoj]]]]
{{Main|Kumbhoj}}

[[Kumbhoj]] is the name of an ancient town located in [[Kolhapur district]], [[Maharashtra]]. The town is about eight kilometers from Hatkanangale, about twenty seven kilometers from Kolhapur. The famous Jain pilgrimage centre where a {{convert|28|ft|m|adj=on|abbr=out}}-high statue of Bahubali is installed is {{convert|2|km}} from the Kumbhoj city.{{sfn|Sangave|1981|p=91}}

=== Arthipura ===
There is a {{convert|10|ft|m|adj=on|abbr=out}}-high statue of Bahubali at Arthipura, [[Mandya district]].<ref>{{citation |title=Bahubali of Artipura |url=http://www.frontline.in/arts-and-culture/heritage/bahubali-of-artipura/article8455428.ece |work=[[Frontline (magazine)|Frontline]] |date=29 April 2016 }}</ref>

In 2016, the [[Archaeological Survey of India]] (ASI) excavated another {{convert|13|feet|m|adj=on|abbr=on}}-high statue of Bahubali made in the 3rd&nbsp;–&#32;9th centuries in Arthipura.<ref>{{citation |last=Girish |first=M. B. |authorlink=M. B. Girish |title=Another Jain centre under excavation in Mandya district |url=http://www.deccanchronicle.com/151204/nation-current-affairs/article/another-jain-centre-under-excavation-mandya-district |work=[[Deccan Chronicle]] |date=23 February 2016 |origyear=4 December 2015 }}</ref> ASI has also excavated an 8th-century statue of Bahubali in Arthipura, [[Maddur, Mandya]], Karnataka, that is {{convert|3|feet|m}} wide and {{convert|3.5|feet|m|abbr=on}} tall.<ref>{{citation |title=Eighth Century Jain Temple Discovered in Maddur |url=http://www.newindianexpress.com/states/karnataka/Eighth-Century-Jain-Temple-Discovered-in-Maddur/2015/01/07/article2607640.ece |work=[[The New Indian Express]] |agency=Express News Service |date=7 January 2015 }}</ref>

==In literature==
[[File:Poetic old Kannada inscription of Bopanna dated 1180 CE at Shravanabelagola.jpg|upright 0.75|thumb|Poem by Boppanna]]

The life-story of Bahubali has been discussed in many works.
=== Sanskrit ===
* The [[Ādi purāṇa]] composed by ''Āchārya'' Jinasena.
*  ''Bahubali charitra'' written in the 9th century A.D.{{sfn|Sangave|1981|p=51}}

=== Kannada ===
* A 10th-century [[Kannada]] text based on the Sanskrit text was written by the poet [[Adikavi Pampa]].<ref>{{citation |url=http://www.kamat.com/kalranga/kar/literature/history2.htm |title=History of Kannada literature |work=kamat.com }}</ref><ref>{{citation |title=Students' Britannica India, Volumes 1–5 |url=https://books.google.com/books?id=AE_LIg9G5CgC&pg=PA78 |publisher=[[Popular Prakashan]] |isbn=0-85229-760-2 |p=78 }}</ref>
* A poem dated 1180 was composed by a Jain poet named Boppanna (also known as Sujanottamsa), in praise of Bahubali.{{sfn|Sangave|1981|p=84}}

== Images ==

Pictured below are some of the images depiciting Bahubali that are located at various places in India.
<gallery>
YSR_State_arch_museum_-_bahubaludu_at_patan_cheru.jpg|Bahubali statue at [[Telangana State Archaeology Museum|YSR state Archaeology Museum]], Hyderabad, 12th century 
Bahubali monolith of Halebidu.JPG|Bahubali monolith of [[Halebidu]]
Mumbai 03-2016 102 Teenmurti Temple.jpg|{{convert|28|ft|m|adj=on|abbr=out}}-high statue of Bahubali at [[Sanjay_Gandhi_National_Park#Jain_temple|Teenmurti Temple]], [[Mumbai]]
Gommaṭeśvara.JPG|Gomateshwara at [[Kalugumalai Jain Beds]], 8th century 
Cholapandiyapuram andimalai Bahubali.jpg|Bahubali at [[Cholapandiyapuram#Andimalai|Andimalai Caves]], 10th century
</gallery>

{{clear}}

==See also==
{{Commons category}}

*[[God in Jainism]]
*[[Jain cosmology]]
*[[Jainism in Karnataka]]
*[[Statue of Ahimsa]]
*[[Bawangaja]]

{{clear}}

== Notes ==
{{Reflist|colwidth=30em}}

== References ==

* {{citation |last=Dundas |first=Paul |authorlink=Paul Dundas |title=The Jains |url=https://books.google.co.in/books?id=X8iAAgAAQBAJ |edition=Second |date=2002 |orig-year=1992 |publisher=[[Routledge]] |isbn=0-415-26605-X |location=[[London]] and [[New York City]] }}
* {{citation |last=Granoff |first=Phyllis |authorlink=Phyllis Granoff |title=The Clever Adulteress and Other Stories: A Treasury of Jaina Literature |url=https://books.google.co.in/books?id=Po9tUNX0SYAC |date=1993 |publisher=[[Motilal Banarsidass]] |orig-year=1990 |isbn=81-208-1150-X }}
* {{citation |last=Jain |first=Champat Rai |authorlink=Champat Rai Jain |title=Risabha Deva – The Founder of Jainism |url=https://archive.org/details/JainismFounder |publisher=The Indian Press Limited |location=[[Allahabad]] |date=1929 |quote={{PD-notice}} |ref={{sfnref|Champat Rai Jain|1929}} }}
* {{citation |last=Jain |first=Vijay K. |authorlink=Vijay K. Jain |title=Ācārya Nemichandra's Dravyasaṃgraha |url=https://books.google.co.in/books?id=g9CJ3jZpcqYC |date=2013 |quote={{PD-notice}} |publisher=Vikalp Printers |isbn=978-81-903639-5-2 |ref={{sfnref|Vijay K. Jain|2013}} }}
* {{citation |last=Rice |first=Benjamin Lewis |authorlink=B. Lewis Rice |title=Inscriptions at Sravana Belgola: a chief seat of the Jains, (Archaeological Survey of Mysore) |publisher=[[Mysore Govt. Central Press]] |location=[[Bangalore]] |date=1889 |url=https://archive.org/stream/inscriptionsatsr00rice#page/n5/mode/2up }}
* {{citation |last=Sangave |first=Vilas Adinath |authorlink=Vilas Adinath Sangave |title=The Sacred Shravanabelagola (A Socio-Religious Study) |url=https://books.google.co.in/books?id=nZ3S6CW5KKQC |date=1981 |publisher=[[Bharatiya Jnanpith]] |edition=1st }}
* {{citation |last=Saran |first=Renu |authorlink=Renu Saran |url=https://books.google.co.in/books?id=xHNOBAAAQBAJ |title=Monuments of India |isbn=978-93-5165-298-4 |date=19 August 2014 }}
* {{citation |last=Titze |first=Kurt |authorlink=Kurt Titze |title=Jainism: A Pictorial Guide to the Religion of Non-Violence |url=https://books.google.co.in/books?id=loQkEIf8z5wC |publisher=[[Motilal Banarsidass]] |edition=2 |date=1998 |isbn=81-208-1534-3 }}
* {{citation |last=Zimmer |first=Heinrich |authorlink=Heinrich Zimmer |title=Philosophies Of India |date=1953 |orig-year=April 1952 |editor-first=Joseph |editor-last=Campbell |editorlink=Joseph Campbell |publisher=[[Routledge]] & Kegan Paul Ltd |location=[[London]], E.C. 4 |url=https://archive.org/details/Philosophy.of.India.by.Heinrich.Zimmer |isbn=978-81-208-0739-6 }}

==External links==
* [http://shribahubali.org Shri Bahubali]

{{Jain Gods}}
{{Jainism topics}}
{{Authority control}}

[[Category:God in Jainism]]
[[Category:Ikshvaku dynasty]]
[[Category:Asceticism]]
[[Category:Religious behaviour and experience]]
[[Category:GA-Class Jainism articles]]