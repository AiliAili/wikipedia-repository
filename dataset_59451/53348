[[File:WilliamFitchAllen.jpg|right|thumb|William F. Allen (1877)]]
'''William Fitch Allen''' (July 28, 1808 – June 3, 1878) was an American lawyer and politician.

==Life==
He was born on July 28, 1808, in [[Windham County, Connecticut]], the eldest son of Abner Harvey Allen and Cynthia Palmer, a sister of the mother of [[Sanford E. Church]]. He graduated from [[Union College]] in 1826. Then he studied law in the office of [[John C. Wright (comptroller)|John C. Wright]] at [[Esperance, New York]], and  in the office of C. M. and E. S. Lee at [[Rochester, New York]]. He was admitted to the bar in 1829, and commenced practice in partnership with [[George Fisher (New York)|George Fisher]] at Oswego until 1833. In 1834, he formed a partnership with [[Abraham P. Grant]] which continued until his election to the [[New York Supreme Court]]. He was Supervisor of the Town of Oswego in 1836 and 1837.<ref>[https://archive.org/stream/historyofoswegoc01john#page/202/mode/1up ''History of Oswego County, New York''] by Crisfield Johnson (L. H. Everts & Co., Philadelphia PA, 1877; pg. 202)</ref>

Allen was a [[United States Democratic Party|Democratic]] member of the [[New York State Assembly]] in [[66th New York State Legislature|1843]] and [[67th New York State Legislature|1844]].<ref>[https://books.google.com/books?id=E3sFAAAAQAAJ&pg=PA255 Google Book] ''The New York Civil List'' compiled by Franklin Benjamin Hough (pages 255, 351; Weed, Parsons and Co., 1858)</ref> From 1845 to 1847, he was [[United States Attorney for the Northern District of New York]]. He was a Justice of the New York Supreme Court from 1847 to 1863, and sat [[ex officio]] on the Court of Appeals in 1854 and 1862. In 1863, he removed to [[New York City]] and resumed the practice of law there.

He was [[New York State Comptroller]] from 1868 to 1870, elected at the [[New York state election, 1867]]; and re-elected at the [[New York state election, 1869]]. He resigned this office in June 1870.

At the [[New York special judicial election, 1870]], he was elected to the [[New York Court of Appeals]], and in July 1870 became one of the first judges of the new court upon its re-organization after the amendment of the State Constitution in 1869. He remained on the bench until his death on June 3, 1878, in [[Oswego, New York]].<ref>[http://query.nytimes.com/mem/archive-free/pdf?res=9E0CEFDB1E3FE63BBC4C53DFB0668383669FDE ''OBITUARY; JUDGE WILLIAM F. ALLEN''] in the ''[[New York Times]]'' on June 4, 1878</ref>

==Sources==
{{reflist}}

==External links==
*[http://www.courts.state.ny.us/history/elecbook/thereshallbe/pg95.htm] Court of Appeals history
*[http://www.nycourts.gov/history/elecbook/chadbourne_coa/pg5.htm] Bio at NYCoA history

{{s-start}}
{{s-off}}
{{succession box | title = [[New York State Comptroller]] | before = [[Thomas Hillhouse (adjutant general)|Thomas Hillhouse]] | after = [[Asher P. Nichols]] | years = 1868&ndash;1870}}
{{s-end}}

{{NYSComptroller}}

{{Authority control}}

{{DEFAULTSORT:Allen, William F.}}
[[Category:1808 births]]
[[Category:1878 deaths]]
[[Category:New York State Comptrollers]]
[[Category:Members of the New York State Assembly]]
[[Category:People from Oswego, New York]]
[[Category:New York Supreme Court Justices]]
[[Category:New York Court of Appeals judges]]
[[Category:People from Windham County, Connecticut]]
[[Category:Union College (New York) alumni]]
[[Category:United States Attorneys for the Northern District of New York]]
[[Category:Town supervisors in New York]]