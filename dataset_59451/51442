{{Infobox journal
| title = Health
| cover = [[File:Health journal front cover.gif]]
| editor = Michael Traynor
| discipline = [[Healthcare]]
| former_names =
| abbreviation = Health
| publisher = [[SAGE Publications]]
| country =
| frequency = Bimonthly
| history = 1997–present
| openaccess =
| license =
| impact = 1.324 
| impact-year = 2013
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200904
| link1 = http://hea.sagepub.com/content/current
| link1-name = Online access
| link2 = http://hea.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 300289269
| LCCN = 97652924
| CODEN =
| ISSN = 1363-4593
| eISSN = 1461-7196
}}
'''''Health''': An Interdisciplinary Journal for the Social Study of Health, Illness and Medicine'' is a bimonthly [[Peer review|peer-reviewed]] [[healthcare journal]] that covers research in the fields of [[health]] and the [[social sciences]]. The journal was established in 1997 with Alan Radley [[Loughborough University]]) as founding editor and is published by [[SAGE Publications]].

== Abstracting and indexing ==
''Health'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.324, ranking it 70 out of 136 journals in the category "Public, Environmental & Occupational Health (SSCI)" and 18 out of 37 journals in the category "Social Sciences, Biomedical".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Social Sciences, Biomedical |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-08-24 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

==External links==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200904}}

[[Category:Healthcare journals]]
[[Category:Bimonthly journals]]
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]