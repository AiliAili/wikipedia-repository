{{Infobox journal
| cover = 
| title = Doklady Physics
| editor = [[Vladimir E. Fortov]]
| discipline = [[Physics]] and [[astronomy]]
| former_names = Soviet Physics-Doklady, Physics-Doklady
| abbreviation = Dokl. Phys.
| publisher = [[MAIK Nauka]]/Interperiodica and [[Springer Science+Business Media]]
| country = Russia
| frequency = Monthly
| history = 1956-present
| openaccess =
| license =
| impact = 0.473
| impact-year = 2013
| website = http://www.springer.com/journal/11446/about
| link1 = http://link.springer.com/journal/volumesAndIssues/11446
| link1-name = Online archive
| link2 = http://www.maik.rssi.ru/cgi-perl/journal.pl?lang=eng&name=danphys
| link2-name = Journal page at MAIK Nauka/Interperiodica website
| JSTOR = 
| OCLC = 312931650
| LCCN = 98646622
| CODEN = DOPHFU 
| ISSN = 1028-3358
| eISSN = 1562-6903
}}
'''''Doklady Physics: A Journal of the Russian Academy of Sciences''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by [[MAIK Nauka]]/Interperiodica and [[Springer Science+Business Media]]. This journal covers [[Russian language|Russian]] to [[English language|English]] translations of [[physics]], [[engineering|technical physics]], [[astronomy]], and [[mechanics]] articles from ''Doklady Akademii Nauk'' (English: ''Proceedings of the Russian Academy of Sciences''). The [[editor-in-chief]] is [[Vladimir E. Fortov]]. The journal was established in 1956 as ''Soviet Physics-Doklady''<ref name=LC1>{{cite web |url=http://lccn.loc.gov/58032694 |title=Soviet Physics-Doklady |work=Library of Congress Catalog |publisher=[[Library of Congress]] |accessdate=2015-03-25}}</ref> and renamed ''Physics-Doklady'' in 1993,<ref name=LC2>{{cite web |url=http://lccn.loc.gov/93646142 |title=Physics-Doklady |work=Library of Congress Catalog |publisher=[[Library of Congress]] |accessdate=2015-03-25}}</ref> before obtaining its current title in 1998.<ref name=LC3>{{cite web |url=http://lccn.loc.gov/98646622 |title=Doklady Physics |work=Library of Congress Catalog |publisher=[[Library of Congress]] |accessdate=2015-03-25}}</ref>

==Abstracting and indexing==
This journal is abstracted and indexed in:
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Science Citation Index]]
* [[Chemical Abstracts Service]]
* [[Compendex]]
* [[Scopus]]
* [[Inspec]]
* [[Current Mathematical Publications]]
* [[Zentralblatt Math]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.473.<ref name=WoS>{{cite book |year=2014 |chapter=Doklady Physics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/journal/11446/about}}

[[Category:Nauka academic journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Physics journals]]
[[Category:Publications established in 1998]]
[[Category:English-language journals]]