{{multiple issues|
{{COI|date=February 2013}}
{{advert|date=February 2013}}
}}
{{Infobox company
|name               = RSM International
|logo               = RSM International Logo.jpg
|type               = Member firms have different legal structures<br />United States and United Kingdom: [[Limited liability partnership|Limited Liability Partnership]]
|slogan             = "The Power of Being Understood"
|location           = [[London]], [[United Kingdom]]
|foundation         = 1964
|area_served        = Worldwide
|key_people         = [[Jean Stephens]] {{small|(CEO)}}
|industry           = [[Professional services]]
|services = [[Assurance services|Assurance]]<br />[[Tax|Tax Advisory]]<br />[[Management consulting|Consulting]]<br />[[Financial adviser|Financial Advisory]]<br />Other
|divisions          = Audit, Tax, Consulting
|revenue            = {{Increase}} [[United States dollar|US$]] 4.87&nbsp;billion {{small|(2015)}}<ref>{{cite web|url=http://www.rsm.global/news/rsm-achieves-6-fee-income-growth-and-moves-6th-worldwide-ranking |accessdate=11 August 2016}}</ref>
|num_employees      = 41,000<ref>https://www.accountancyage.com/2017/02/08/brand-strength-leads-to-fee-income-growth-for-rsm/</ref>
|homepage           = [http://www.rsm.global/ rsm.global]
}}

'''RSM International''', branded '''RSM''', is a [[Multinational corporation|multi-national]] network of [[accounting]] firms, forming the sixth largest accountancy [[Accounting networks and associations|professional services network]] in the world. The member firms of RSM are independent accounting and advisory firms each of which practices in its own right, and is unified as part of the network. The network is not a separate legal entity of any description in any jurisdiction, and does not provide services.

The largest member firms are [[RSM US]], formerly known as McGladrey, the fifth largest accounting firm in the [[United States]]; [[RSM UK]], formerly Baker Tilly LLP, the seventh largest accounting firm in the [[United Kingdom]]; and RSM China, formerly Ruihua Certified Public Accountants, the third largest accounting firm in [[China]]. Together, the North American, European, and Asia Pacific regions accounted for $4.87 billion of revenues in 2016.

On October 26, 2015, all member firms of the network were rebranded as RSM.

==History==
RSM International has been in continuous existence since 1964, when it began as a small network that was originally called DRM. In 1993, the organisation restructured, and changed its name to RSM International.

Historically, RSM was derived from the initials of three of the original founding member firms of the organization: 
* [[Robson Rhodes]] (United Kingdom)
* Salustro Reydel (France)
* [[McGladrey]] (United States)
Founding member Robson Rhodes was acquired by [[Grant Thornton]], and absorbed within their network,<ref>[http://www.telegraph.co.uk/money/main.jhtml?xml=/money/2007/04/29/cnaudits29.xml "Auditors join forces to take on Big Four"], ''Daily Telegraph'', 28 April 2007</ref> while Salustro Reydel merged with [[KPMG]].<ref>[http://www.vrlknowledgebank.com/newsletter_article_home.php?id=8&issueid=691&articleid=11750 International Accounting Bulletin, "RSM Salustro Reydel, KPMG to merge"]</ref> [[McGladrey]], or known as the RSM US member firm, is still part of the network today.

In January 2006, Ms. Jean Stephens became the first female chief executive officer, of a Top 10 international accounting network.<ref>[http://www.rsmi.com/en/press/first-female-chief-executive-officer-for-a-top-ten-international-accounting-netw.aspx RSM International press release] Retrieved 12 July 2012</ref>

===Logo===
The current RSM logo – ‘The Progress Bar’ – was introduced in October 2015. The network’s global brand positioning ‘The Power of Being Understood’ was also introduced at this time.<ref>[http://www.rsm.global/global-news/rsm-launches-new-global-brand RSM launches new global brand focused on 'understanding'] Retrieved 26 October 2015</ref>

==Services==
RSM member firms provide the core service lines of [[audit]] and [[assurance services|assurance]], [[accounting]], risk advisory, tax, transaction support, [[corporate restructuring]] & insolvency and [[IFRS]] services. RSM member firms also provide other business and consulting services. Member firms provide services according to the specific regulations and laws in their country.

==Member firms==
RSM is represented in 120
 countries (as of December 2015).<ref>[http://www.rsm.global/about-us RSM About Us] Retrieved 02 December 2015</ref>

The member and correspondent firms of RSM are organised into geographic regions for the purposes of business development, administration, quality control and for the coordination of cross-border or multinational team assignments. The regions are constituted as follows: [[Asia Pacific]], [[Latin America]], [[North America]], [[Europe]], [[Africa]] and [[MENA]]. Member firms are well established practices of high local standing, most of which are ranked within the top ten in their own country.

===France===
In March 2005, subsequent to Salustro Reydel merging with KPMG, RSM France was created as the new French member firm, with offices in [[Paris]].

===United Kingdom===
Following the resignation of Robson Rhodes, Bentley Jennison later joined the RSM International network as its British member, becoming [[RSM Bentley Jennison]].

RSM Bentley Jennison later merged with Tenon Group to become [[RSM Tenon]], a company listed on the [[London Stock Exchange]]. The financial problems of the combined RSM Tenon Group led it falling into administration with its assets acquired by [[Baker Tilly]]. In April 2014, Baker Tilly UK announced it would be joining the RSM International network later in the year, and would adopt the RSM brand.<ref>http://www.dailyrecord.co.uk/business/professionals/baker-tilly-acquired-assets-former-3415608</ref>

==Professional representation==
In August 2010, Robert Dohrer,<ref>[http://www.ifac.org/bio/robert-dohrer Biography of Robert Dohrer]</ref> RSM Global Leader for Quality and Risk, was appointed chairman of the [[International Federation of Accountants]] [[Forum of Firms]], for a three year term. RSM is also represented on other professional international committees, including [http://www.egian.eu/ EGIAN] and the [[International Federation of Accountants]] [http://www.ifac.org/about-ifac/transnational-auditors-committee Transnational Auditors Committee].

===RSM and the European Business Awards===
RSM was the lead sponsor, and corporate champion of the programme [[European Business Awards]] during 2013/14.<ref>http://www.rsmi.com/en/press/rsm-announces-lead-sponsorship-of-the-european-business-awards.aspx</ref><ref>[http://www.businessawardseurope.com/news/item/rsm-announces-lead-sponsorship-of-the-european-business-awards RSM announces lead sponsorship of the European Business Awards] Retrieved 1 March 2013</ref>

==See also==
*[[Professional services networks]]
*[[Accounting networks and associations]]

==References==
<references/>

==External links==
* [http://www.rsm.global/ RSM International]  (official web site)

[[Category:RSM|*]]
[[Category:Auditing]]
[[Category:International management consulting firms]]
[[Category:Management consulting firms of the United Kingdom]]
[[Category:Management consulting firms of the United States]]
[[Category:Accounting firms]]
[[Category:Companies established in 1964]]