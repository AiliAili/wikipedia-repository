{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox musical artist <!-- See Wikipedia:WikiProject Musicians -->
| name                = David Lyttle
| image               =
| background       = non_vocal_instrumentalist
| birth_name       = 
| birth_date         = {{Birth date and age|df=yes|1984|6|21}}
| birth_place        =  [[Waringstown]], Northern Ireland
| genre               = [[Jazz]], [[soul music|soul]], [[hip hop music|hip hop]], [[Folk music|folk]]
| occupation        = Musician, songwriter, producer, label owner
| instrument        = Drums, bass, keyboards
| years_active     =
| label                = [[Lyte Records|Lyte]]
| associated_acts =[[Joe Lovano]], [[Talib Kweli]], [[Soweto Kinch]], [[Duke Special]], [[Andreas Varady]], [[Jason Rebello]], [[Jean Toussaint]], [[Louis Stewart (guitarist)|Louis Stewart]]
| website            = {{URL|www.davidlyttle.com}}
}}

'''David Lyttle''' (born 21 June 1984) is a musician, songwriter, producer, composer, and record label owner from Northern Ireland. He has released three solo albums and received nominations in the [[MOBO Awards]] and [[Urban Music Awards]].

==Background==
Born in [[Waringstown]], he began his professional career under the direction of his parents as a child performer with the Lyttle Family. In his teens he was active as a drummer, DJ and also studied classical cello. Since the age of eighteen, Lyttle has been active mostly as a drummer, songwriter and producer. He earned a PhD in musicology from the [[University of Ulster]] in 2009.<ref name="bare_url">{{cite web|title=Jazz Stars Set for Coleraine performance|url=http://news.ulster.ac.uk/releases/2010/4900.html|accessdate=15 May 2012}}</ref>

==Career==

===2007–2011: Career beginnings===
Lyttle began performing professionally at age four, playing percussion with the Lyttle Family in Ireland and, when he was a teenager, the United States. In 2007, following studies at the [[University of Ulster]], Skidmore Jazz Institute, New York, and the [[Banff Centre]], Canada, he reached a national audience as an Irish jazz performer with [[Louis Stewart (guitarist)|Louis Stewart]]. He released his debut solo album ''True Story'' later in 2007 and began featuring prominent international jazz artists in his touring groups, including [[Greg Osby]],<ref name="journalofmusic">{{cite web|title='Live Reviews: David Lyttle Group feat. Greg Osby', The Journal of Music|url=http://journalofmusic.com/criticism/live-reviews-david-lyttle-group-featuring-greg-osby|accessdate=21 May 2012}}</ref> [[Jean Toussaint]], [[Terell Stafford]] and [[Soweto Kinch]],<ref name="timeout">{{cite web|title='London Jazz Festival', Time Out |url=http://www.timeout.com/london/music/event/118172/london-jazz-festival-david-lyttle-trio-derek-nievergelt/index.php?option=com_content&task=view&id=8493&Itemid=89 |accessdate=21 May 2012 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> whose band he occasionally appears in.

From 2008 to 2010 Lyttle composed a series of literary-inspired suites. ''A Christmas Carol'', inspired by the [[Charles Dickens]] novel, premiered in 2008 and toured Ireland in 2009.<ref name="culturenorthernireland">{{cite web|last=Rainey|first=Stephen|title="Music Review: A Christmas Carol", Culture Northern Ireland|url=http://www.culturenorthernireland.org/article/2422/music-review-a-christmas-carol|accessdate=15 May 2012}}</ref> ''Dark Tales'', which was inspired by the works of [[Charles Dickens]], [[Edgar Allan Poe]] and [[Robert Louis Stevenson]], premiered at Belfast's [[Ulster Hall]] also in 2009, and subsequently toured Ireland and Great Britain in 2010. In 2011 he was commissioned by Peace Three and Jews Schmooze to write ''The Chronicles Suite'', which was inspired primarily by [[CS Lewis]]' [[The Chronicles of Narnia]].

===2012–present: ''Interlude'' and ''Faces''===

Lyttle's second studio album ''[[Interlude (David Lyttle album)|Interlude]]'' was released on 2 January 2012 and was influenced by hip hop, soul and jazz. Its guests included Mercury-nominated rapper [[Soweto Kinch]], his sister Rhea Lyttle and mother Anne Lyttle, bassist [[Pino Palladino]] and pianist [[Jason Rebello]]. First heard on [[BBC Introducing]] on [[BBC Radio 1]],<ref name="bbc">{{cite web|title=BBC Introducing, 12/9/12|url=http://www.bbc.co.uk/programmes/b014f3dc
|accessdate=15 May 2012}}</ref> the album received positive reviews in Ireland and Britain. [[MOBO]] described it as "an exceptional album"<ref name="MOBO">{{cite web|title=David Lyttle Interlude|url=http://www.mobo.com/news-blogs/david-lyttle-interlude
|accessdate=2 April 2014}}</ref> and [[Hot Press]] said it was "a rare sort of treat to come out of Ireland".<ref name="Hot Press">{{cite web|title=David Lyttle: Interlude|url=http://www.hotpress.com/archive/8880646.html
|accessdate=2 April 2014}}</ref>

On 26 June 2013 Lyttle released the single "Celebrate" with [[Belfast]] arts organisation Beat Carnival and Irish recording artist [[Duke Special]].<ref name="Arts Council of Northern Ireland">{{cite web|title=The Beat Carnival celebrates homegrown talent with new music track and video|url=http://www.artscouncil-ni.org/news/the-beat-carnival-celebrates-homegrown-talent-with-new-music-track-and-vide|accessdate=2 April 2014}}</ref>

Lyttle's third studio album ''[[Faces (David Lyttle album)|Faces]]'' was released on 16 March 2015 and featured collaborations with [[Talib Kweli]], [[Duke Special]], [[Joe Lovano]] and several of his guests from ''Interlude''.<ref name="David Lyttle, Marlbank">{{cite web|title=David Lyttle, Faces, Lyte Records|url=http://www.marlbank.net/reviews/2396-david-lyttle-faces-lyte-records|accessdate=23 Jan 2015}}</ref><ref name="David Lyttle website">{{cite web|title=David Lyttle, About|url=http://www.davidlyttle.com/about.htm|accessdate=23 Jan 2015}}</ref> It has been well received by critics with Dave DiMartino of [[Rolling Stone]] calling it "one of the best, robust listening experiences you’re likely to have all year"<ref name="Rolling Stone">{{cite web |last=DiMartino |first=Dave |url=http://www.rollingstone.com/music/news/renovated-apartments-following-foellakzoid-more-20150402
 |title=David Lyttle Faces |date=2 April 2015 |accessdate=3 April 2015}}</ref> and Colm O'Hare of [[Hot Press]] describing it as "one of the most inventive Irish releases of the year."<ref name="Hot Press magazine">
{{cite web |last=O'Hare |first=Colm |url=http://www.hotpress.com/music/reviews/albums/David-Lyttle---Faces/13599888.html |title=David Lyttle - Faces|date=2 March 2015 |accessdate=3 April 2015}}</ref>

Lyttle was nominated in the 2015 [[MOBO Awards]] for Best Jazz Act.<ref>{{cite news| url=http://voting.mobo.com/artists/david-lyttle | work=MOBO Awards | title=David Lyttle | date=5 October 2015}}</ref> He is the first Irish musician to have been nominated for a MOBO.<ref>{{cite news| url=http://www.culturenorthernireland.org/features/music/david-lyttle-makes-history-mobo-nod | work=Culture Northern Ireland | title=David Lyttle Makes History | date=9 October 2015}}</ref> He was also nominated for an [[Urban Music Award]] in October 2015.<ref name="Urban Music Awards">
{{cite web |url=http://urbanmusicawards.net/2015/10/nominations-are-announced-for-the-13th-annual-urban-music-awards-2015/| work=Urban Music Awards |title=Nominations Are Announced|accessdate=2 Dec 2015}}</ref>

On 20 November 2015 Lyttle released ''Say & Do'', a collaborative album with Northern Irish singer/songwriter [[VerseChorusVerse]]. It reached No. 1 in the Amazon UK blues chart.<ref name="Lyte Records">
{{cite web |url=http://www.lyterecords.com/versechorusverse-collaborates-with-david-lyttle-on-roots-album.htm |title=VerseChorusVerse Collaborates|accessdate=2 Dec 2015}}</ref>

===Artist residencies===

Lyttle has carried out a number of artist residencies, mostly recently in 2015 in [[Derry]] at the [[Nerve Centre (organisation)]] as part of the city's legacy program following its designation as [[UK City of Culture]]<ref name="Londonderry Sentinel">{{cite web |url=http://www.londonderrysentinel.co.uk/news/jazz-drummer-lyttle-is-new-fixture-1-6693338|title=Jazz drummer Lyttle is new Derry fixture|accessdate=4 Jan 2017}}</ref> and in 2016 in [[Belfast]] at the [[Metropolitan Arts Centre]], where he mentored up-and-coming jazz musicians from Northern Ireland.<ref name="BBC">{{cite web |url=http://www.bbc.co.uk/programmes/p03kjtfp|title=Arts Show|accessdate=4 Jan 2017}}</ref> In December 2016 he announced a U.S. residency scheduled for April 2017 and funded by an [[Arts Council of Northern Ireland]] Major Individual Award worth £15,000.<ref name="Arts Council">{{cite web |url=http://www.artscouncil-ni.org/news/arts-council-honours-four-major-artists|title=Arts Council honours four major artists|accessdate=4 Jan 2017}}</ref> Lyttle is also one of three [[British Council]] and [[PRS for Music]]'s Musicians In Residence in China for 2017.<ref name="The Guardian">{{cite web |url=https://www.theguardian.com/music/2016/nov/28/emmy-the-great-music-exchange-china-british-council|title=Emmy the Great joins music exchange programme in China |accessdate=4 Jan 2017}}</ref>

===Lyte Records===

As owner and founder of [[Lyte Records]], Lyttle has released albums by notable jazz, blues and roots artists, including [[Ari Hoenig]], [[Jason Rebello]], [[Nigel Mooney]] and [[Jean Toussaint]]. He has also produced debut albums for young talents such as Israeli classical pianist Ariel Lanyi<ref name="community-relations">{{cite web|title= Jews Schmooze 2011 'Sounds Familiar' Programme |url=http://www.community-relations.org.uk/about-us/news/item/679/jews-schmooze-2011-sounds-familiar-programme|accessdate=15 May 2012}}</ref> and Slovakian jazz guitarist [[Andreas Varady]],<ref name="allaboutjazz">{{cite web|last=Lindsay|first=Bruce|title="Andreas Varady / David Lyttle: Questions (2010)", All About Jazz|url=http://www.allaboutjazz.com/php/article.php?id=38089|accessdate=15 May 2012}}</ref> who is managed by American producer [[Quincy Jones]]<ref name="quincyjones">{{cite web|title=Quincy Jones Artists|url=http://www.quincyjones.com/artists/|accessdate=29 January 2013}}</ref> and was discovered by Lyttle in 2010.<ref name="guitarplayer">{{cite web|title="Following the Footsteps of Giants", Guitar Player|url=http://www.guitarplayer.com/article/4045|accessdate=21 May 2012}}</ref>

==Solo Discography==

* ''True Story'' (2007)
* ''[[Interlude (David Lyttle album)|Interlude]]'' (2012)
* ''[[Faces (David Lyttle album)|Faces]]'' (2015)

==References==
{{Reflist|2}}

==External links==
* {{Official website|http://www.davidlyttle.com}}

{{DEFAULTSORT:Lyttle, David}}
[[Category:1984 births]]
[[Category:Living people]]
[[Category:People from Waringstown]]
[[Category:Jazz musicians from Northern Ireland]]
[[Category:Musicians from Northern Ireland]]