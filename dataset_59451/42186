<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=  Akaflieg Berlin B13
 | image=Akafliegberlinb13.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type= High Performance Two-Seat Glider (with sustainer)<ref name="B13">http://akaflieg.extern.tu-berlin.de/joomla/index2.php?option=com_content&do_pdf=1&id=25</ref>
 | national origin=[[Germany]]
 | manufacturer= [[Akaflieg Berlin]]
 | designer=   
 | first flight= 1991
 | introduced=
 | retired=
 | status=
 | primary user=[[Akaflieg Berlin]]
 | number built=  1
 | developed from=
 | variants with their own articles=  
}}
|}
The '''Akaflieg Berlin B13''' is a two-seat motor-glider designed and built in [[Germany]].<ref name="B13"/>

== Development ==
Students at Akaflieg Berlin studied a high-performance motor-glider with the engine in the nose and an automatically folding propeller. After approval was given, a prototype was built in 1991, as the ' Akaflieg Berlin B13', using [[GFRP]] (glass-fibre re-inforced plastic) to form a fuselage with smooth lines, housing the engine in the nose and a two-seat side-by-side cockpit covered by a large plexiglas canopy.<ref name="B13"/>

Intended to use a sustainer motor, for sustaining flight only, power was to be supplied by a modified 24.5KW (33&nbsp;hp) engine in the extreme nose driving a 5-blade folding retractable propeller, specially developed by [[Prof. Oehler]]. However, problems with the integration of the engine with the fuselage have prevented the fitting of the engine, resulting in the 'B13' being restricted to un-powered flight only.<ref name="B13"/>
<!-- ==Operational history== -->
<!-- == Variants == -->
<!-- ==Operators (choose)== -->

==Specifications (B13 power off)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1988-89,<ref name=JAWA88-89>{{cite book |title=Jane's All the World's Aircraft 1988-89 |year=1988 |publisher=Jane's Information Group |location=London |isbn=0-7106-0867-5 |editor=John W.R. Taylor}}</ref> sailplanedirectory.com<ref name=sdb13>{{cite web|title=B-13 Akaflieg Berlin|url=http://www.sailplanedirectory.com/PlaneDetails.cfm?next=42|website=sailplanedirectory.com|accessdate=19 January 2015}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|crew=2
|length m=8.3
|span m=23.2
|height m=1.9
|wing area sqm=18.9
|aspect ratio=28.5
|airfoil=HQ 41/ 14,35
|empty weight kg=590
|max takeoff weight kg=850
|fuel capacity={{convert|25|l|USgal impgal|abbr=on}}

|more general=
*'''Water Ballast:''' {{convert|160|l|USgal impgal|abbr=on}}
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 377]]
|eng1 type=2-cyl. horizontally-opposed air-cooled piston engine
|eng1 kw=24

|prop blade number=5
|prop name=Oehler folding propeller

<!--
        Performance
-->
|stall speed kmh=70
|never exceed speed kmh=280
|never exceed speed note=in smooth air
*:{{convert|200|kph|kn mph|abbr=on}} in rough air
*:{{convert|75|kph|kn mph|abbr=on}} on aero-tow
*:{{convert|120|kph|kn mph|abbr=on}} on winch launch
|g limits=+5.3 - 2.65
|glide ratio=49 at {{convert|105|kph|kn mph|abbr=on}}
|sink rate ms=0.55
|sink rate note= at {{convert|75|kph|kn mph|abbr=on}}
|wing loading kg/m2=42.2
|wing loading note=at max t/o weight
|power/mass=0.0376 kW/kg (0.0229 hp/lb)

|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft  -->
* [[Stemme S-10]]
* [[Caproni Vizzola Calif]]

|lists=<!-- related lists -->
}}

==References==
{{reflist}}

{{refbegin}}
{{refend}}

==External links==
* [http://www.sailplanedirectory.com/PlaneDetails.cfm?next=42]  Akaflieg Berlin B13
* [http://akaflieg.extern.tu-berlin.de/joomla/index2.php?option=com_content&do_pdf=1&id=26] Akaflieg Berlin
* [http://www.sailplanedirectory.com/berlin.htm] Sailplane Directory
* [http://www.akaflieg-berlin.de] Akaflieg Berlin

[[Category:German sailplanes 1990–1999]]