{{Infobox monarch
| name               =Amon  
| title              =King of [[Kingdom of Judah|Judah]] 
| image              =Amon rex.png
|image_size       = 160px
| caption            =Amon from [[Guillaume Rouillé]]'s ''[[Promptuarii Iconum Insigniorum]]'', 1554
| reign              =643/642 – 641/640 BC<ref name="Thiele"/><ref name="McFall">{{Cite journal|author=Leslie McFall |year=1991 |title=A Translation Guide to the Chronological Data in Kings and Chronicles |journal=[[Bibliotheca Sacra]] |volume=148 |pages=3–45 |publisher=[[Dallas Theological Seminary]] |url=http://www.btinternet.com/~lmf12/TransGuide.pdf |archiveurl=https://web.archive.org/web/20110721042159/http://www.btinternet.com/~lmf12/TransGuide.pdf |archivedate=July 21, 2011 |deadurl=unfit }}</ref>
| full name          = 
| native_lang1       =Hebrew
| native_lang1_name1 =אָמוֹן 
| birth_date         =c. 664 BC
| birth_place        =[[Kingdom of Judah|Judah]] 
| death_date         =c. 641 BC
| death_place        =[[Jerusalem]] 
| burial_date        = 641 BC
| burial_place       = Garden of Uzza<ref name="Jewish Encyclopedia">{{cite encyclopedia |author=Charles J. Mendelsohn |authorlink= |author2=Kaufmann Kohler |author3=Morris Jastrow|editor= |encyclopedia=[[Jewish Encyclopedia]] |title= Amon, King of Judah|url=http://jewishencyclopedia.com/articles/1420-amon-king-of-judah|edition=1st |date= |year=1906|publisher=Funk & Wagnalls |volume=I|pages=526–527}}</ref>
| predecessor        =[[Manasseh of Judah|Manasseh]]
| successor          =[[Josiah]] 
| queen              =Jedidah<ref>{{cite book | url=https://books.google.com/books?id=3ZqqR2_lZpoC&pg=PA532&dq=jedidiah+mother+of+Amon#v=onepage&q=jedidiah%20mother%20of%20Amon&f=false | title=The Hebrew Monarchy: A Commentary, with a Harmony of the Parallel Texts and Extracts from the Prophetical Books | publisher=Eyre and Spottiswoode | author=Andrew Wood | chapter=The Kingdom of Judah | chapterurl=https://books.google.com/books?id=3ZqqR2_lZpoC&pg=PA532&lpg=PA532&dq=jedidiah+mother+of+Amon&source=bl&ots=kUnbHJTx8Q&sig=eqDX8y5E-jv0pGiB2kW7MGsbyQo&hl=en&sa=X&ei=BKb4Tu3BEeHw0gG8sei5BA&ved=0CEYQ6AEwBQ#v=onepage&q=jedidiah%20mother%20of%20Amon&f=false | year=1896 | isbn=1-149-80041-0}}</ref>
| issue              = [[Josiah]]
| royal house        =[[Davidic line|House of David]] 
| father             =[[Manasseh of Judah|Manasseh]] 
| mother             =[[Meshullemeth]]<ref name="Flavius Josephus">[[Flavius Josephus]] (c. 93 CE). ''[http://www.ccel.org/j/josephus/works/ant-10.htm Antiquities of the Jews]''. Book X, Chapter 3, Section 2. Translated from the [[Latin]] by [[William Whiston]] from The Christian Classics Ethereal Library.</ref>
| religion           =[[Idolatry]]
}}
'''Amon of Judah''' ({{lang-he|אָמוֹן}} ''’Āmōn''; {{lang-el|Αμων}}; {{lang-la|Amon}}) was a 7th-century BC [[Kings of Judah|King of Judah]] who, according to the [[Bible|biblical account]], succeeded his father [[Manasseh of Judah]]. Amon is most remembered for his [[Idolatry|idolatrous]] practices while [[Kings of Judah|king]], which led to a revolt against him and eventually his assassination in c. 641 BC.

==Life==
{{Kings of Judah|align=left|margins=0.5em 1em 0.5em 0.5em}}
Amon was the son of [[Manasseh of Judah|King Manasseh of Judah]] and [[Meshullemeth]], a daughter of Haruz of Jotbah.<ref name="Flavius Josephus" /> Although the date is unknown, the [[Hebrew Bible]] records that he married Jedidah, the daughter of Adaiah of [[Bozkath]]. Amon began his reign of Judah at the age of 22, and reigned for two years.<ref name="2 Kings">{{bibleverse|2|Kings|21:18-26|RSV}}</ref> Biblical scholar and archeologist [[William F. Albright]] has dated his reign to 642 – 640, while professor [[E. R. Thiele]] offers the dates 643/642&nbsp;– 641/640.<ref name="Thiele">{{cite book |author=Edwin R. Thiele|edition=3rd|title=[[The Mysterious Numbers of the Hebrew Kings]]|year=1983|publisher=Kregel Publications|location=Grand Rapids, Michigan|isbn=0-8254-3825-X |chapter=9}}</ref> Thiele's dates are tied to the reign of Amon's son [[Josiah]], whose death at the hands of Pharaoh [[Necho II]] occurred in the summer of 609. Josiah's death, which is independently confirmed in [[History of Egypt|Egyptian history]],<ref>{{cite book | url=http://www.scribd.com/doc/10493581/Chronicles-of-Chaldaean-Kings-626556-BC-in-the-British-Museum | title=Chronicles of Chaldean Kings | publisher=Trustees of the British Museum | author=D.J. Wiseman | year=1956 | pages=94–95}}</ref> places the end of Amon's reign, 31 years earlier, in 641 or 640 and the beginning of his rule in 643 or 642.<ref name="Thiele" />

The [[Hebrew Bible]] records that Amon continued his father Manasseh's practice of [[idolatry]] and set up pagan images as his father had done.<ref name="Jewish Encyclopedia" /> [[Books of Kings|II Kings]] states that Amon "did that which was evil in the sight of YAWEH, as did Manasseh his father. And he walked in all the way that his father walked in, and served the idols that his father served, and worshipped them."<ref name="2 Kings" /> Similarly, [[Books of Chronicles|II Chronicles]] records that "…he did that which was evil in the sight of the Lord, as did Manasseh his father; and Amon sacrificed unto all the graven images which Manasseh his father had made, and served them."<ref>{{bibleverse|2|Chronicles|33:22|RSV}}</ref> The [[Talmud|Talmudic tradition]] recounts that "Amon burnt the Torah, and allowed spider webs to cover the <!---[[Altar (Bible)#In the Temple|-->altar<!--]]--> [through complete disuse]&nbsp;... Amon sinned very much."<ref>''[[Sanhedrin (tractate)|Tractate Sanhedrin]]'', Folio 103a. 1902 Translation by Rabbi Isisdore Epstein.</ref> Like other textual sources, [[Flavius Josephus]] too criticizes the reign of Amon, describing his reign similarly to the [[Hebrew bible|Bible]].<ref>{{cite journal |author=Christopher Begg|year=1996|title= Jotham and Amon: Two Minor Kings of Judah According to Josephus|journal=[[Bulletin for Biblical Research]] |volume=6|issue=1|page=13|publisher=[[Institute for Biblical Research]] |doi= |pmid= |pmc= |url=http://www.ibr-bbr.org/files/bbr/BBR_1996_01_Begg_JothamInJosephus.pdf}}</ref>

After reigning two years, Amon was assassinated by his servants, who conspired against him, and was succeeded by his son Josiah, who at the time was eight years old.<ref>{{bibleverse|2|Kings|22:1|RSV}}</ref> After Amon's assassination his murderers became unpopular with the people, and were ultimately killed.<ref>{{cite book | url=https://books.google.com/books?id=4vk0AAAAMAAJ&printsec=frontcover#v=onepage&q&f=false | title=Great leaders of Hebrew history from Manasseh to John the Baptist | publisher=The Macmillan Company | author=Henry Fowler | year=1920 | page=11}}</ref> Some scholars, such as Abraham Malamat, assert that Amon was assassinated because people disliked the heavy influence that [[Neo-Assyrian Empire|Assyria]], an age-old enemy of Judah responsible for the [[Assyrian captivity of Israel|destruction]] of the [[Kingdom of Israel (Samaria)|Kingdom of Israel]], had upon him.<ref>{{cite journal | title=History of Biblical Israel: Major Problems and Minor Issues | publisher=[[American Schools of Oriental Research]] | author=Nili S. Fox | journal=[[Bulletin of the American Schools of Oriental Research]] | year=2002 | volume=II | issue=327 | pages=90–92 | issn=0003-097X | doi=10.2307/1357868| jstor=1357868 | last2=Malamat }}</ref>

==Era==
Amon's reign was in the midst of a transitional time for the [[Levant]] and the entire [[Mesopotamia]]n region. To the east of [[Kingdom of Judah|Judah]], the [[Neo-Assyrian Empire|Assyrian Empire]] was beginning to disintegrate while the [[Neo-Babylonian Empire|Babylonian Empire]] had not yet risen to replace it. To the west, [[Twenty-sixth dynasty of Egypt|Egypt]] was still recovering under [[Psamtik I]] from its Assyrian occupation,<ref>{{cite book | title=The Third Intermediate Period in Egypt, 1100-650 B.C. | publisher=Aris & Phillips Ltd | author=Kenneth Kitchen | year=1986 | edition=2nd | isbn=978-0-85668-298-8}}</ref> transforming from a [[vassal state]] to an autonomous ally.<ref>{{cite web |url=http://www.metmuseum.org/toah/hd/lapd/hd_lapd.htm|title=Egypt in the Late Period (ca. 712–332 B.C.)|author=James Allen and Marsha Hill|year=2004|publisher=The Metropolitan Museum of Art|accessdate=21 December 2011}}</ref> In this power vacuum, many smaller states such as Judah were able to govern themselves without foreign intervention from larger empires.<ref>{{cite journal |author=Bernd Schipper |year=2010|title=Egypt and the Kingdom of Judah under Josiah and Jehoiakim|journal=Journal of the Institute of Archaeology of Tel Aviv University |volume=37 |issue=2|pages=200–226|url=http://www.ingentaconnect.com/content/maney/tav/2010/00000037/00000002/art00007|publisher=Maney Publishing |doi=10.1179/033443510x12760074470865}}
</ref>

==See also==
{{Portal|Judaism}}
*[[Manasseh of Judah]]
*[[Josiah]]
*[[Kings of Judah]]
*[[Kingdom of Judah]]

==References==
{{Reflist|30em}}

{{JewishEncyclopedia|article=Amon, King of Judah|url=http://jewishencyclopedia.com/articles/1420-amon-king-of-judah|noicon=1}}

{{s-start}}
{{s-hou|[[Davidic line|House of David]]}}
{{s-bef|before=[[Manasseh of Judah|Manasseh]]}}
{{s-ttl|title=[[Kings of Judah|King of Judah]]|years=643–641 BC}}
{{s-aft|after=[[Josiah]]}}
{{end}}

{{IsraeliteKings|collapsed=yes}}
{{good article}}

{{Authority control}}

[[Category:Kings of ancient Judah]]
[[Category:7th-century BC biblical rulers]]
[[Category:660s BC births]]
[[Category:640s BC deaths]]
[[Category:People murdered in Israel]]
[[Category:Biblical murder victims]]
[[Category:7th-century BCE Jews]]