{{good article}}
{{Use mdy dates|date=September 2011}}
{{Infobox ice hockey player
| name = George Armstrong
| position = [[Winger (ice hockey)|Right Wing]]
| shoots = Right
| height_ft = 6
| height_in = 1
| weight_lb = 184
| played_for = [[Toronto Maple Leafs]]
| birth_date = {{birth date and age|1930|7|6}} 
| birth_place = [[Skead, Ontario|Skead]], [[Ontario]], Canada
| career_start = 1949
| career_end = 1971
| image = Chex Photo George Armstrong.JPG
| alt = Armstrong poses for a studio shot in his Maple Leafs uniform.  His sweater features a "C" patch denoting he was captain of his team.
| halloffame = 1975
}}
'''George Edward "Chief" Armstrong''' (born July 6, 1930) is a [[Canadians|Canadian]] former professional [[ice hockey]] [[centre (ice hockey)|centre]] who played 21 seasons in the [[National Hockey League]] (NHL) for the [[Toronto Maple Leafs]]. He played [[List of NHL players with 1000 games played|1,188 NHL games]] between 1950 and 1971, all with Toronto and a franchise record, and was the team's [[Captain (ice hockey)|captain]] for 13 seasons. Armstrong was a member of four [[Stanley Cup]] championship teams and played in seven [[NHL All-Star Game]]s. He scored the final goal of the NHL's "[[Original Six]]" era as Toronto won the [[1967 Stanley Cup Final|1967 Stanley Cup]].

Armstrong played both [[junior hockey|junior]] and [[senior ice hockey|senior]] hockey in the [[Toronto Marlboros]] organization and was a member of the [[1950 Allan Cup]] winning team as senior champions of Canada. He returned to the Marlboros following his playing career and coached the junior team to two [[Memorial Cup]] championships. He served as a [[scout (sport)|scout]] for the [[Quebec Nordiques]], as an assistant general manager of the Maple Leafs and for part of the [[1988–89 NHL season]] as [[List of Toronto Maple Leafs head coaches|Toronto's head coach]].  Armstrong was inducted into the [[Hockey Hall of Fame]] in 1975 and the Maple Leafs [[List of NHL retired numbers|honoured]] his uniform number 10 in 1998, and would later officially retire the number, along with 10 others, during a pre-game ceremony on October 15, 2016.

==Early life==
Armstrong was born in 1930 in [[Skead, Ontario]], to a [[Scottish Canadian]] father and part [[Ojibway]] mother.<ref>{{harvnb|Rogers|Smith|1994|p=379}}</ref> Armstrong’s mother being half Ojibwa has Armstrong coming from a line of woodland people of northeastern North America. George Armstrong’s aboriginal heritage makes up of approximately 160,000 people, with a strong political and social activism.<ref>{{Cite news|url=http://www.thecanadianencyclopedia.ca/en/article/ojibwa/|title=Ojibwa|last=Bishop|first=Charles A.|work=The Canadian Encyclopedia|access-date=2017-03-29|language=en}}</ref>He grew up in [[Falconbridge, Ontario]] where his father was a nickel miner.<ref name="LOH1on1">{{citation |last=Shea |first=Kevin |url=http://www.hhof.com/htmlSpotlight/spot_oneononep197501.shtml |title=One on one with George Armstrong |publisher=Hockey Hall of Fame |date=2007-03-30 |accessdate=2014-02-02}}</ref> Sport was an important part of Armstrong's family as his father played [[association football|soccer]] and his mother was a [[canoeing|canoeist]].  The younger Armstrong developed a passion for hockey but was a poor skater, which his father believed was a consequence of a case of [[spinal meningitis]] George suffered at the age of six.<ref name="NotAshamed">{{citation |url=https://news.google.com/newspapers?nid=1946&dat=19670513&id=IEMjAAAAIBAJ&sjid=HbkFAAAAIBAJ&pg=2167,3099268 |title=Armstrong no longer ashamed |work=Montreal Gazette |date=1967-05-13 |accessdate=2014-02-02}}</ref>

While attending Sudbury High School, Armstrong played on the hockey team with [[Mirl "Red" McCarthy|Red McCarthy]] and [[Tim Horton]]. Inspired by a newspaper advertisement offering tryouts with the [[Copper Cliff Redmen]] of the [[Northern Ontario Junior Hockey Association]] (NOJHA), Armstrong convinced Horton and McCarthy to join him in trying out. They made the team and Armstrong began his [[junior hockey]] career at age 16 in the 1946–47 season.<ref name="ghpg8">{{harvnb|Griggs|Horton|1997|p=8}}</ref> He recorded six goals and five assists in nine games and caught the attention of [[scout (sports)|scouts]] for the [[National Hockey League]] (NHL)'s [[Toronto Maple Leafs]] who added him to their protected list.<ref>{{harvnb|Mortillaro|2011|p=19}}</ref> He also played with the [[Prince Albert Blackhawks]] for part of that season.<ref name="OGHHOF">{{harvnb|Duplacey|Zweig|2010|p=23}}</ref> Armstrong quit school in grade 11 to focus on his hockey career.<ref name="ghpg8" />

==Playing career==

===Junior and senior===
The Maple Leafs placed Armstrong on the [[Stratford Kroehlers]] in the [[Ontario Hockey Association]] (OHA) junior division for the 1947–48 season. He led the league in both assists (43) and points (73),<ref name="OGHHOF" /> and was named recipient of the [[Red Tilson Trophy]] as the OHA's most valuable player.<ref name="RedTilsonTrophy">{{citation |editor-last=Bell |editor-first=Aaron |title=2013–14 OHL Media Guide |publisher=Ontario Hockey League |year=2013 |page=131}}</ref> Promoted to the [[Toronto Marlboros]] for the 1948–49 season, Armstrong recorded 62 points in 39 games with the junior squad and played in three regular season and ten post-season matches for the [[senior ice hockey|senior]] team.<ref name="NHLStats">{{citation |url=http://www.nhl.com/ice/player.htm?id=8444971 |title=George Armstrong player card |publisher=National Hockey League |accessdate=2014-02-02}}</ref> Armstrong remained with the senior Marlboros in 1949–50 where he served as [[captain (ice hockey)|captain]].<ref name="meh83">{{harvnb|Meharg|2005|p=83}}</ref> He led the OHA senior division with 64 goals, at the time an OHA record,<ref name="LOH1on1" /> and recorded 115 points in 39 games. He was again named the winner of the Red Tilson Trophy.<ref name="RedTilsonTrophy" /><ref name="NHLStats" />

The Maple Leafs briefly recalled Armstrong during the [[1949–50 NHL season|1949–50 season]] and he made his NHL debut on December 3, 1949. He appeared in two games before returning to the Marlboros.<ref name="LOH1on1" /> In the [[1950 Allan Cup|1950]] [[Allan Cup]] playdowns, he recorded 19 goals and 19 assists in 14 games as the Marlboros won the national senior championship.<ref name="LOH1on1" />  It was also during the season that he earned his nickname.  While visiting the [[Stoney Indian Reserves Nos. 142, 143, and 144|Stoney Reserve]] in [[Alberta]] with the Marlboros, the locals presented Armstrong with a ceremonial headdress and called him "Big Chief Shoot the Puck" owing to his own Native heritage. The nickname was often shortened to "Chief".<ref name="LOHBio">{{citation |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p197501&type=Player&page=bio&list= |title=George Armstrong Biography |publisher=Hockey Hall of Fame |accessdate=2014-02-02}}</ref>

===Toronto Maple Leafs===
{{Quote box| quote ="This kid's got everything. He has size, speed, and he can shoot 'em into the net better that any hockey player I've known in a long time. I'll be surprised if he doesn't become a superstar."|align=right |width=30%|source=—Toronto's assistant general manager [[King Clancy]]'s assessment of Armstrong in 1952.<ref name="LOH1on1" />}}
Upon turning professional in [[1950–51 AHL season|1950–51]], Armstrong was assigned to Toronto's [[American Hockey League]] (AHL) affiliate, the [[Pittsburgh Hornets]].<ref name="LOHBio" /> In 71 games for Pittsburgh, he recorded 15 goals and 48 points.<ref name="NHLStats" /> Despite being hampered by hand and wrist injuries suffered in [[fighting in ice hockey|fights]], Armstrong was the AHL's leading goal scorer and stood second in points by mid-season in [[1951–52 AHL season|1951–52]].<ref>{{citation |last=Jordan |first=Jimmy |url=https://news.google.com/newspapers?nid=1129&dat=19520123&id=XpJRAAAAIBAJ&sjid=q2oDAAAAIBAJ&pg=2702,2011426 |title=Armstrong has chance to take scoring lead |work=Pittsburgh Post-Gazette |date=1952-01-23 |accessdate=2014-02-03 |page=18}}</ref> He was recalled to Toronto during the season and scored his first NHL goal, against goaltender [[Gerry McNeil]] of the [[Montreal Canadiens]]. It was the first goal ever scored by a player with Native heritage.<ref>{{harvnb|Mortillaro|2011|p=22}}</ref> He finished the season with three goals and three assists in 20 games with Toronto.<ref name="NHLStats" />

Though he missed the start of the [[1952–53 NHL season|1952–53 season]] due to a separated shoulder, Armstrong earned a permanent spot on the Maple Leafs' roster.<ref name="LOH1on1" /> He quietly established himself as an important contributor for Toronto by recording 25 points that season, then scoring 32 points the following season and 28 in [[1954–55 NHL season|1954–55]].<ref>{{harvnb|Downey|Gogishvili|Park|2013|pp=221–223}}</ref> A 48-point season in [[1955–56 NHL season|1955–56]] was second on the team to [[Tod Sloan (ice hockey)|Tod Sloan]]'s 66.  Armstrong then led the Maple Leafs in scoring with 44 points in [[1956–57 NHL season|1956–57]] despite missing 14 of his team's games.<ref>{{harvnb|Downey|Gogishvili|Park|2013|pp=224–225}}</ref> He was named to play in the [[NHL All-Star Game]] in both seasons. They were the first two of seven he would ultimately play.<ref name="OGHHOF" />

[[File:George Armstrong action photo.jpg|thumb|left|George Armstrong with the Maple Leafs, circa 1970|alt=Armstrong poses for a photographer while wearing his full Maple Leafs uniform.]]
The Maple Leafs named Armstrong the team's [[captain (ice hockey)|captain]] in [[1957–58 NHL season|1957–58]] as he succeeded [[Ted Kennedy (ice hockey)|Ted Kennedy]] and [[Jimmy Thomson (ice hockey b. 1927)|Jim Thomson]] who served as co-captains the season before.<ref>{{harvnb|Downey|Gogishvili|Park|2013|pp=225–226}}</ref> He finished fourth in team scoring with 42 points, then played his third All-Star Game during the [[1958–59 NHL season|1958–59 season]].<ref name="OGHHOF" /> He recorded four assists in the [[1959 Stanley Cup playoffs|playoffs]] as the Maple Leafs reached the [[1959 Stanley Cup Final]], but lost to the [[Montreal Canadiens]].<ref name="NHLStats" /><ref>{{citation |last=Bastable |first=Jim |url=https://news.google.com/newspapers?id=P9cxAAAAIBAJ&sjid=JuQFAAAAIBAJ&pg=4916%2C1109132 |title=4th straight Stanley Cup despite Leafs' great bid |work=Ottawa Citizen |date=1959-04-20 |accessdate=2014-02-03 |page=11}}</ref> With 51 points in [[1959–60 NHL season|1959–60]], Armstrong finished one behind [[Bob Pulford]] for the team lead.<ref>{{harvnb|Downey|Gogishvili|Park|2013|p=228}}</ref> Toronto again reached the [[1960 Stanley Cup Final|Stanley Cup Final]] where they were again eliminated by Montreal.<ref>{{citation |last=Carroll |first=Dink |url=https://news.google.com/newspapers?id=I7EtAAAAIBAJ&sjid=uZwFAAAAIBAJ&pg=5009%2C3501324 |title=Habs win 4–0, fifth straight Cup |work=Montreal Gazette |date=1960-04-15 |accessdate=2014-02-03 |page=16}}</ref>

The Maple Leafs finally reached the NHL's peak two seasons later.<ref name="LOH1on1" /> Armstrong set a career high with 53 points in the [[1961–62 NHL season|1961–62]] regular season, then added 12 points in 12 playoff games for Toronto.<ref name="NHLStats" /> He started the play that resulted in the [[Stanley Cup]] clinching goal, rushing the puck up ice before passing to [[Tim Horton]], who then passed to goal-scorer [[Dick Duff]] that capped off a 2–1 victory in the sixth and deciding game of the series against the [[Chicago Blackhawks|Chicago Black Hawks]].<ref>{{citation |url=https://news.google.com/newspapers?id=uDAyAAAAIBAJ&sjid=huUFAAAAIBAJ&pg=5141%2C1471664 |title=Thousands out in early hours to hail champs |work=Ottawa Citizen |date=1962-04-23 |accessdate=2014-02-04 |page=1}}</ref> As Maple Leafs captain, Armstrong was presented the trophy by league president [[Clarence Campbell]].<ref name="LOH1on1" /> It was the first of three consecutive championships for Toronto as the Maple Leafs of 1962–1964 became the fourth [[dynasty (sports)|dynasty]] in NHL history.<ref>{{citation |url=http://www.nhl.com/ice/page.htm?id=31167 |title=Dynasties: Eight benchmark teams |publisher=National Hockey League |accessdate=2014-02-04}}</ref> Individually, Armstrong scored 21, 19 and 20 goals over the three seasons and by virtue of the NHL's All-Star Game format of the time that had the defending champion play the all-stars of the remaining teams, appeared in his [[16th National Hockey League All-Star Game|fourth]], [[17th National Hockey League All-Star Game|fifth]] and [[18th National Hockey League All-Star Game|sixth]] All-Star Games.<ref name="OGHHOF" /> Early in the [[1963–64 NHL season|1963–64 season]], on December 1, 1963, Armstrong scored his 200th career NHL goal.<ref>{{citation |url=https://news.google.com/newspapers?nid=1129&dat=19631202&id=ZsZaAAAAIBAJ&sjid=OmwDAAAAIBAJ&pg=7093,186179 |title=Maple Leafs roll over Wings, 4–1 |work=Pittsburgh Post-Gazette |date=1963-12-02 |accessdate=2014-02-04 |page=37}}</ref>

A 37-point season followed in [[1964–65 NHL season|1964–65]], then 51 points the [[1965–66 NHL season|1965–66 season]].<ref name="NHLStats" />  By [[1966–67 NHL season|1966–67]], Armstrong led an aging Maple Leafs team that entered the playoffs as an [[underdog (term)|underdog]] against a dominant Chicago team. The Maple leafs nonetheless eliminated the Black Hawks in six games to set up the [[1967 Stanley Cup Final]] against Montreal. The Canadiens were so confident of victory that a display area for the Stanley Cup had been set up at the [[Québec Pavilion|Quebec pavilion]] at [[Expo 67]] prior to the series' start.<ref name="Pincus116">{{harvnb|Pincus|2006|pp=116–117}}</ref> The Maple Leafs dashed Montreal's hopes by winning the championship in six games.  Armstrong scored the final goal of the series in a 3–1 victory in the deciding contest.<ref>{{citation |last=Taylor |first=Sterling |url=https://news.google.com/newspapers?id=nckyAAAAIBAJ&sjid=YO0FAAAAIBAJ&pg=698%2C637564 |title=11th Cup for Leafs |work=Ottawa Citizen |date=1967-05-03 |accessdate=2014-02-04 |page=23}}</ref> It was also the last goal scored in the NHL's "[[Original Six]]" era as the league was set to [[1967 NHL expansion|double in size]] to 12 teams for the [[1967–68 NHL season|1967–68 season]].<ref name="OGHHOF" />

Armstrong announced his intention to retire as a player following the championship but changed his mind and returned for another season.<ref name="FirstRetirements">{{citation |url=https://news.google.com/newspapers?nid=1499&dat=19681009&id=tN8jAAAAIBAJ&sjid=EygEAAAAIBAJ&pg=3193,5181895 |title=Hockey vets leave camp |work=Milwaukee Sentinel |date=1968-10-09 |accessdate=2014-02-04 |page=21}}</ref> The Maple Leafs placed him on their protected list for the [[1967 NHL Expansion Draft|1967 Expansion Draft]], and he remained with Toronto.<ref>{{citation |url=https://news.google.com/newspapers?nid=1873&dat=19670607&id=C28eAAAAIBAJ&sjid=n8kEAAAAIBAJ&pg=690,1462740 |title=Few veterans drawn in big hockey draft |work=Daytona Beach Morning Journal |date=1967-06-07 |accessdate=2014-02-04 |page=13}}</ref> He played in his [[21st National Hockey League All-Star Game|seventh]] All-Star Game in 1968 and finished the season with 34 points.<ref name="OGHHOF" /> Retiring following the season before changing his mind became an annual event for Armstrong as he announced his intention to leave the game in five straight years.<ref name="FinalRetirement">{{citation |url=https://news.google.com/newspapers?nid=1946&dat=19711021&id=7YY1AAAAIBAJ&sjid=5qEFAAAAIBAJ&pg=4664,2879277 |title=Armstrong moves upstairs |work=Montreal Gazette |date=1971-10-21 |accessdate=2014-02-04 |page=20}}</ref> He remained a consistent scorer for Toronto, recording 27, 28 and 25 points in his following three seasons.<ref name="NHLStats" /> He finally ended his playing career after the [[1970–71 NHL season|1970–71 season]] to take an office position in the Maple Leafs.<ref name="FinalRetirement" />, and also he finished his career with two hundred and ninety-six goals, four goals away from obtaining three hundred.<ref>{{Cite web|url=https://www.hhof.com/htmlSpotlight/spot_treasurep197501.shtml|title=Legends of Hockey - Spotlight - George Armstrong - Treasure Chest|website=www.hhof.com|access-date=2017-03-29}}</ref>

==Coaching and scouting career==
Armstrong was announced as the head coach of his former junior team, the Toronto Marlboros, in July 1972.<ref>{{citation|url=https://news.google.com/newspapers?nid=1946&dat=19720704&id=CB4rAAAAIBAJ&sjid=raEFAAAAIBAJ&pg=4620,271643|title=Armstrong to coach Marlies|newspaper=Montreal Gazette|date=1972-07-04|page=14|accessdate=2014-02-01}}</ref> Though he had preferred his previous role as a [[scout (sports)|scout]] to coaching, Armstrong led the Marlboros to [[Memorial Cup]] victories on two occasions: in [[1973 Memorial Cup|1973]] and [[1975 Memorial Cup|1975]]. In 1977, Armstrong's name circulated as a possible successor to Maple Leafs coach [[Red Kelly]] when the latter was fired by the team.<ref>{{citation |url=https://news.google.com/newspapers?nid=1946&dat=19770618&id=VZguAAAAIBAJ&sjid=n6EFAAAAIBAJ&pg=1226,514169|title=Maple Leafs chop coach Kelly, possible successors named|newspaper=Montreal Gazette|date=1977-06-18|page=15|accessdate=2014-02-01}}</ref> When approached by the organization with the prospect of assuming head coaching duties however, Armstrong rejected the possibility. His decision resulted in animosity from within the organization and subsequently led to his resignation as coach of the Marlboros that season to accept a scouting post with the [[Quebec Nordiques]].<ref name="meh86">{{harvnb|Meharg|2005|p=86}}</ref>

Some 10 years later, Armstrong returned to the Maple Leafs organization in the dual capacities of assistant [[general manager]] and scout. During the [[1988–89 NHL season|1988–89 season]], and after management had fired head coach [[John Brophy (ice hockey)|John Brophy]], team owner [[Harold Ballard]] was adamant that Armstrong be named Brophy's replacement. Armstrong agreed to take the position, but increasingly delegated majority of his duties to assistant coach [[Garry Lariviere]].  The Maple Leafs finished with 17 wins in 47 Armstrong's games coached. He was replaced by [[Doug Carpenter]] the following season and returned to his scouting capacities with the team. Armstrong remains a scout with the Maple Leafs to this day.<ref name="meh86" /><ref>{{cite web|url=http://mapleleafs.nhl.com/club/page.htm?id=42249 |title=Toronto Maple Leafs - Alumni - Toronto Maple Leafs - Team |publisher=Toronto Maple Leafs Hockey Club |accessdate=2014-02-02}}</ref> In 1972-73, he was selected as Coach of the Year by his peers in the OHL (Toronto Malboros)<ref>{{Cite web|url=http://www.hockeydb.com/ihdb/awards/awarddetail.php?award_id=99|title=Coach of the Year award winners of the OHA at hockeydb.com|website=www.hockeydb.com|language=en|access-date=2017-03-29}}</ref>. 

==Playing style==
The Toronto Maple Leafs described Armstrong as being a "consistent, durable and hardworking" player throughout his 21-season career that spanned parts of four decades.<ref name="HonouredNumber" /> A consummate leader, Armstrong was lauded by owner [[Conn Smythe]] as "the best captain, as a captain, the Leafs have ever had".<ref name="LOHBio" /> His 713 career points were the second most all-time in Toronto franchise history at the time of his retirement,<ref name="LOH1on1" /> and {{asof|2014}} remains fifth-best.<ref>{{harvnb|Downey|Gogishvili|Park|2013|p=299}}</ref> His [[List of NHL players with 1000 games played|1,187 NHL games]] are the most by any player in Toronto history, and he remains the franchise leader with 417 career assists and 713 points by a right wing.<ref name="OGHHOF" /> The Maple Leafs named him the co-recipient, with [[Bob Pulford]], of the [[J. P. Bickell Memorial Award]] in 1959. The award is presented to members of the organization who perform with a high standard of excellence.<ref name="HonouredNumber" />  In 1998, the franchise [[List of NHL retired numbers|honoured]] his uniform number 10.<ref>{{citation |url=https://news.google.com/newspapers?nid=875&dat=19980301&id=xkZQAAAAIBAJ&sjid=VVYDAAAAIBAJ&pg=6544,108271 |title=Flyers spoil debut of Rangers' coach |work=Lake Havasu News-Herald |date=1998-03-01 |accessdate=2014-02-04 |page=19A}}</ref> In 2013, he ranked was number 14 on [[Sportsnet]]'s list of the greatest Maple Leafs.<ref name="sportsnet">{{cite web|url=http://www.sportsnet.ca/hockey/24-7/greatest-maple-leafs-no-14-george-armstrong/ |title=Greatest Maple Leafs: No. 14 George Armstrong |publisher=Sportsnet.ca |accessdate=2014-02-02}}</ref> Armstrong was inducted into the [[Hockey Hall of Fame]] in 1975 and the [[Ontario Sports Hall of Fame]] in 2010.<ref name="LOH1on1" /><ref>{{cite web|title=George Armstrong|url=http://oshof.ca/index.php/honoured-members/item/18-george-armstrong|website=http://oshof.ca/|accessdate=25 September 2014}}</ref>

==Personal life==
Armstrong resides in Toronto with his wife Betty. The couple have four children: Fred, Betty-Ann, Brian and Lorne. He is the uncle of [[Dale McCourt]], a former first overall draft pick by the [[Detroit Red Wings]] in the [[1977 NHL Amateur Draft]].<ref name="cupjournal">{{cite web|url=http://www.hhof.com/htmlstcjournal/exSCJ05_18.shtml |title=Hockey Hall of Fame - Stanley Cup Journals: 18 |publisher=Hockey Hall of Fame |accessdate=2014-02-02}}</ref> When given a day with the [[Stanley Cup]] in 2005, Armstrong elected to have a family gathering with it at his son's home in [[Vaughan, Ontario]].<ref name="cupjournal" /> His granddaughter Kalley was a team captain with the [[Harvard Crimson women's ice hockey]] program.<ref>{{cite web|url=https://www.thestar.com/sports/hockey/2015/03/19/harvard-captain-kalley-armstrong-leads-crimson-into-womens-frozen-four.html|title= Harvard captain Kalley Armstrong leads Crimson into women’s Frozen Four|publisher=Toronto Star |date=2016-03-19|accessdate=2016-11-21}}</ref> She would join Kelly Paton's coaching staff with the [[Western Mustangs women's ice hockey]] program in the autumn of 2016.

Armstrong was recognized by the NHL for his charitable efforts in 1969 when he was named the inaugural recipient of the [[Charlie Conacher Humanitarian Award]].<ref name="ConacherAward" /> Proud of his Native heritage, Armstrong often supported programs organized by both [[Aboriginal Affairs and Northern Development Canada|Indian and Northern Affairs]] and non-governmental agencies that aimed to promote positive role models for Native children.<ref>{{harvnb|Marks|2008|pp=54–55}}</ref>

Armstrong had a brief film career, appearing as himself in the 1971 film ''[[Face-Off (1971 film)|Face-Off]]'', a.k.a. "Winter Comes Early".<ref>{{cite web|url=http://www.imdb.com/name/nm0035708/ |title=George Armstrong- IMDB |accessdate=2014-05-12}}</ref>

==Career statistics==

===Playing career===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:50em;"
|- style="background:#e0e0e0;"
! colspan="3" style="background:#fff;"| &nbsp;
! rowspan="99" style="background:#fff;"| &nbsp;
! colspan="5"                    | [[regular season|Regular&nbsp;season]]
! rowspan="99" style="background:#fff;"| &nbsp;
! colspan="5"                    | [[Playoffs]]
|- style="background:#e0e0e0;"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|-
| 1946–47 ||[[Copper Cliff Redmen]]||[[Northern Ontario Junior Hockey Association|NOJHA]]||9||6||5||11||4||5||0||1||1||10
|- bgcolor="#f0f0f0"
| [[1947–48 OHA season|1947–48]]||[[Stratford Kroehlers]]||[[Ontario Hockey Association|OHA Jr.]]||36||33||40||73||33||—||—||—||—||—
|-
| 1948–49||[[Toronto Marlboros]]||OHA Jr.||39||29||33||62||89||10||7||10||17||2
|- bgcolor="#f0f0f0"
| 1948–49||Toronto Marlboros||OHA Sr.||3||0||0||0||2||10||2||5||7||6
|-
| 1949–50||Toronto Marlboros||OHA Sr.||45||64||51||115||74||3||0||0||0||0||
|- bgcolor="#f0f0f0"
| [[1949–50 NHL season|1949–50]]||[[Toronto Maple Leafs]]||[[NHL]]||2||0||0||0||0||—||—||—||—||—
|-
| [[1950 Allan Cup|1949–50]]||Toronto Marlboros||[[Allan Cup]]||—||—||—||—||—||17||19||19||38||18
|- bgcolor="#f0f0f0"
| [[1950–51 AHL season|1950–51]]||[[Pittsburgh Hornets]]||[[American Hockey League|AHL]]||71||15||33||48||49||13||4||9||13||6
|-
| [[1951–52 AHL season|1951–52]]||Pittsburgh Hornets||AHL||50||30||29||59||62||—||—||—||—||—
|- bgcolor="#f0f0f0"
| [[1951–52 NHL season|1951–52]]||Toronto Maple Leafs||NHL||20||3||3||6||30||4||0||0||0||2
|-
| [[1952–53 NHL season|1952–53]]||Toronto Maple Leafs||NHL||52||14||11||25||54||—||—||—||—||—
|- bgcolor="#f0f0f0"
| [[1953–54 NHL season|1953–54]]||Toronto Maple Leafs||NHL||63||17||15||32||60||5||1||0||1||2
|-
| [[1954–55 NHL season|1954–55]]||Toronto Maple Leafs||NHL||66||10||18||28||80||4||1||0||1||4
|- bgcolor="#f0f0f0"
| [[1955–56 NHL season|1955–56]]||Toronto Maple Leafs||NHL||67||16||32||48||97||5||4||2||6||0
|-
| [[1956–57 NHL season|1956–57]]||Toronto Maple Leafs||NHL||54||18||26||44||37||—||—||—||—||—
|- bgcolor="#f0f0f0"
| [[1957–58 NHL season|1957–58]]||Toronto Maple Leafs||NHL||59||17||25||42||93||—||—||—||—||—
|-
| [[1958–59 NHL season|1958–59]]||Toronto Maple Leafs||NHL||59||20||16||36||37||12||0||4||4||10
|- bgcolor="#f0f0f0"
| [[1959–60 NHL season|1959–60]]||Toronto Maple Leafs||NHL||70||23||28||51||60||10||1||4||5||4
|-
| [[1960–61 NHL season|1960–61]]||Toronto Maple Leafs||NHL||47||14||19||33||21||5||1||1||2||0
|- bgcolor="#f0f0f0"
| [[1961–62 NHL season|1961–62]]||Toronto Maple Leafs*||NHL||70||21||32||53||27||12||7||5||12||2
|-
| [[1962–63 NHL season|1962–63]]||Toronto Maple Leafs*||NHL||70||19||24||43||27||10||3||6||9||4
|- bgcolor="#f0f0f0"
| [[1963–64 NHL season|1963–64]]||Toronto Maple Leafs*||NHL||67||20||17||37||14||14||5||8||13||10
|-
| [[1964–65 NHL season|1964–65]]||Toronto Maple Leafs||NHL||59||15||22||37||14||6||1||0||1||4
|- bgcolor="#f0f0f0"
| [[1965–66 NHL season|1965–66]]||Toronto Maple Leafs||NHL||70||16||35||51||12||4||0||1||1||4
|-
| [[1966–67 NHL season|1966–67]]||Toronto Maple Leafs*||NHL||70||9||24||33||26||9||2||1||3||6
|- bgcolor="#f0f0f0"
| [[1967–68 NHL season|1967–68]]||Toronto Maple Leafs||NHL||62||13||21||34||4||—||—||—||—||—
|-
| [[1968–69 NHL season|1968–69]]||Toronto Maple Leafs||NHL||53||11||16||27||10||4||0||0||0||0
|- bgcolor="#f0f0f0"
| [[1969–70 NHL season|1969–70]]||Toronto Maple Leafs||NHL||49||13||15||28||12||—||—||—||—||—
|-
| [[1970–71 NHL season|1970–71]]||Toronto Maple Leafs||NHL||59||7||18||25||6||6||0||2||2||0
|- bgcolor="#e0e0e0"
! colspan="3" | NHL totals
! 1188
! 296
! 417
! 713
! 721
! 110
! 26
! 34
! 60
! 52
|}
<nowiki>*</nowiki> [[Stanley Cup]] champion

===Coaching career===
{| class="wikitable" style="font-size: 95%; text-align:center; width:55em;"
|- 
! rowspan="2"|Season !! rowspan="2"|Team !! rowspan="2"|League !! colspan="6"|[[Regular season]] !! colspan="1"|[[Playoffs|Post season]]
|-
! G !! W !! L !! T !! Pct !! Division rank !! Result
|-
| [[1988–89 NHL season|1988–89]] || [[Toronto Maple Leafs]] || [[National Hockey League|NHL]] ||47||17||26||4||.404||5th in [[Norris Division|Norris]]||Did not qualify
|}

==Awards and honours==
{| class="wikitable"
|+ style="text-align:center; background:#e0e0e0" |Career
|-
! scope="col" style="width:20em" | Award
! scope="col" | Year
! scope="col" | Ref.
|-
| [[Red Tilson Trophy]]<br /><small>[[Ontario Hockey Association|OHA]] most valuable player</small>
| 1947–48<br />1949–50
| <ref name="RedTilsonTrophy" />
|-
| [[Allan Cup]] champion
| [[1950 Allan Cup|1949–50]]
| <ref name="LOH1on1" />
|-
| Played in the [[NHL All-Star Game]]
| [[10th National Hockey League All-Star Game|1956]], [[11th National Hockey League All-Star Game|1957]]<br />[[13th National Hockey League All-Star Game|1959]], [[16th National Hockey League All-Star Game|1962]]<br />[[17th National Hockey League All-Star Game|1963]], [[18th National Hockey League All-Star Game|1964]]<br />[[21st National Hockey League All-Star Game|1968]]
| <ref name="OGHHOF" />
|-
| [[J. P. Bickell Memorial Award]]<br /><small>TOR – Outstanding performance</small>
| 1959
| <ref name="HonouredNumber">{{harvnb|Downey|Gogishvili|Park|2013|p=379}}</ref>
|-
| [[Stanley Cup]] champion
| [[1962 Stanley Cup Final|1961–62]], [[1963 Stanley Cup Final|1962–63]]<br />[[1964 Stanley Cup Final|1963–64]], [[1967 Stanley Cup Final|1966–67]]
| <ref name="OGHHOF" />
|-
| [[Charlie Conacher Humanitarian Award]]
| [[1968–69 NHL season|1968–69]]
| <ref name="ConacherAward">{{citation |url=https://news.google.com/newspapers?nid=1338&dat=19690530&id=ao9YAAAAIBAJ&sjid=GvgDAAAAIBAJ&pg=3808,4456435 |title=Leaf captain gets award |work=Spokane Daily Chronicle |date=1969-05-30 |accessdate=2014-02-03 |page=13}}</ref>
|-
| [[Memorial Cup]] champion
| 1973, 1975 (as coach)
| <ref name="meh86" />
|}

==See also==
*[[list of NHL players who spent their entire career with one franchise]]

==References==
*''Career statistics'': {{cite web |url=http://www.nhl.com/ice/player.htm?id=8444971 |title=George Armstrong player card |publisher=National Hockey League |accessdate=2014-02-02}}
{{reflist|2}}

==Bibliography==
*{{citation |editor-last=Downey |editor-first=Craig |editor-last2=Gogishvili |editor-first2=Aaron |editor-last3=Park |editor-first3=Pat |title=2013–14 Toronto Maple Leafs Media Guide |publisher=Toronto Maple Leafs Hockey Club |year=2013}}
*{{citation |last=Duplacey |first=James |last2=Zweig |first2=Eric |title=Official Guide to the Players of the Hockey Hall of Fame |publisher=Firefly Books |year=2010 |isbn=1-55407-662-5}}
*{{citation |last=Griggs |first=Tim |last2=Horton |first2=Lori |title=In Loving Memory: A Tribute to Tim Horton |publisher=ECW Press |year=1997 |url=https://books.google.com/?id=ZLZbMvs5qjUC&pg=PA1&dq=george+armstrong+copper+cliff#v=onepage&q=george%20armstrong%20copper%20cliff&f=false |isbn=978-1-55022-319-4}}
*{{citation |last=Marks |first=Don |title=They Call Me Chief: Warriors on Ice |publisher=J. Gordon Shillingford Publishing Inc. |year=2008 |isbn=1-897289-34-0}}
*{{citation |last=Meharg |first=Bruce |last2= |first2= |title=Legends of the Leafs: Toronto's 200 Greatest Hockey Heroes |publisher=AuthorHouse |year=2005 |url=https://books.google.com/?id=UikBx7a1nzoC&pg=PA83&dq=george+armstrong+copper+cliff#v=onepage&q=george%20armstrong%20copper%20cliff&f=false |isbn=978-1-4670-6931-1}}
*{{citation |last=Mortillaro |first=Nicole |last2= |first2= |title=Hockey Trailblazers |publisher=Scholastic Canada |year=2011 |url=https://books.google.com/?id=5bako-2zzKQC&pg=PA19&dq=george+armstrong+copper+cliff#v=onepage&q=george%20armstrong%20copper%20cliff&f=false |isbn=978-1-4431-0469-2}}
*{{Citation|last=Pincus|first=Arthur|year=2006|title=The Official Illustrated NHL History|publisher=Readers Digest|isbn=0-88850-800-X}}
*{{Citation|last=Rogers |first=Edward |last2=Smith |first2=Donald |url=https://books.google.com/?id=dvhyVWgWhyEC&pg=PA379&dq=George+Armstrong+ojibway+hockey#v=onepage&q=George%20Armstrong%20ojibway%20hockey&f=false |title=Aboriginal Ontario: Historical Perspectives on the First Nations |publisher=Dundurn Press Ltd. |year=1994 |isbn=1-55002-209-1}}

==External links==
{{Commons category|George Armstrong (ice hockey)}}
*{{hockeydb|106|George Armstrong}}
*{{Legendsmember|Player|P197501|George Armstrong}}
*[http://www.hockey-reference.com/players/a/armstge01.html George Armstrong profile at Hockey-Reference.com]
*{{IMDb name|id=0035708}}

{{S-start}}
{{Succession box| before = [[Ted Kennedy (ice hockey)|Ted Kennedy]] | title = [[List of Toronto Maple Leafs captains|Toronto Maple Leafs captain]] | years = [[1958-59 NHL season|1958]]–[[1968–69 NHL season|69]] | after = [[Dave Keon]] }}
{{Succession box| before = [[John Brophy (ice hockey)|John Brophy]] | title = [[List of Toronto Maple Leafs head coaches|Head coach of the Toronto Maple Leafs]] | years = [[1988–89 NHL season|1988–89]] | after = [[Doug Carpenter]] }}
{{S-end}}

{{DEFAULTSORT:Armstrong, George}}
[[Category:1930 births]]
[[Category:Living people]]
[[Category:Canadian ice hockey centres]]
[[Category:Canadian ice hockey coaches]]
[[Category:Canadian people of First Nations descent]]
[[Category:First Nations sportspeople]]
[[Category:Ice hockey people from Ontario]]
[[Category:Hockey Hall of Fame inductees]]
[[Category:Pittsburgh Hornets players]]
[[Category:Quebec Nordiques]]
[[Category:Sportspeople from Greater Sudbury]]
[[Category:Stanley Cup champions]]
[[Category:Stratford Kroehlers players]]
[[Category:Toronto Maple Leafs coaches]]
[[Category:Toronto Maple Leafs executives]]
[[Category:Toronto Maple Leafs players]]
[[Category:Toronto Maple Leafs scouts]]
[[Category:Toronto Marlboros coaches]]