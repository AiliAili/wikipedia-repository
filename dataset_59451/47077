{{Infobox people
|name = Elizabeth Clark
|image = EC 1915.jpg
|caption = Elizabeth Clark c1915
|birth_date = 14 May 1875
|birth_place = Hartlebury Worcestshire
|occupation = Story teller, lecturer, author
|death_date = 21 April 1972
|death_place = Winchester
|resting_place = Kilmeston Hampshire
|years_active = 1915-1955
}}
'''Annette Elizabeth Clark''' (14 May 1875 – 21 April 1972) who was known to her family as Nettie, was a story teller of children's stories, a lecturer in the craft of story telling and the author of ten collections of children's stories published during her lifetime.  Her stories were broadcast on the BBC children's programmes.

== Early life ==
Elizabeth Clark was born in 1875 the eldest of six children to Reverend William Maitland Clark and Annette Laura Clark in Hartlebury where her father was headmaster of Hartlebury Grammar School.<ref>{{cite news | title = Death of Miss Elizabeth Clark: Lecturer and Story-teller | newspaper = Hampshire Chronicle | date = 1972-04-28}}</ref><ref name="TG">{{cite news|last1=Thorpe|first1=Vanessa|title=Nursery tales enchant a new generation as publishers rediscover old-fashioned values|url=https://www.theguardian.com/books/2016/oct/09/children-books-enid-blyton-elizabeth-clark|accessdate=15 December 2016|publisher=The Guardian|date=9 October 2016}}</ref>  Four years later the family moved to another school in Hampstead and then, in about 1883, to Kingsgate House in Winchester to establish a boys' preparatory school.  They lived at Kingsgate House until 1904 when it was sold to Winchester College.   While at Kingsgate House Elizabeth Clark along with her sisters were some of the earliest pupils at the (then) Winchester High School for Girls (now St Swithun's).

Her father subsequently became vicar of Kilmeston some 8 miles east of Winchester.  As the eldest daughter in a Victorian/Edwardian vicarage she was expected to assist her parents in the life of the parish.  She gradually became aware of her latent gift as a story teller as she began to capture the interest of the village children by telling them fairy stories.  She became determined to make story telling her life's work and left home to live in London.

In 1915 she was invited to give regular Story-hours at the then newly-formed Play-Centres.  The following year she gave a course of lectures at the West Ham and District Education Centre.   The photograph above is taken from a promotional pamphlet she put out which quoted favourable press reviews, including an article that appeared in the Evening Standard: <blockquote> ''Miss Elizabeth Clark is an idealist. Her stories are related with a sympathetic understanding of what a child wants to know ... She can carry the children with her not only into the well-known world of Grimm and Anderson, but along routes of old saga for which East and West are alike responsible.''<ref>{{citation | title = The Magic of Story-Telling: Miss Elizabeth Clark's Fairy Tales of the Steppes  | newspaper = Evening Standard and St James's Gazette | page = 13 | date = 1916-04-30}}</ref>  </blockquote>

== After the First World War ==

In 1919 the [[London County Council]] appointed her to give a course of lectures to London teachers which she continued to give twice a year for more than ten years.  Meanwhile she was lecturing to an increasing number of students in the University Training Centres in [[Scotland]]. 
[[File:nettie_1.png|thumb|upright|A miniature painting of Elizabeth Clark by her sister Frances Irene Donaldson]]
In 1931 she was invited by the Executive of the National [[Girl Scouts of America]] to lecture at their conferences in the East and Mid-West.  After a tour of two months, she returned to England following which she gave a talk to a Luncheon Club in Hull on story-telling in America.  The following appeared the Hull Daily Mail
<blockquote>
''As I write I cannot help still thinking of the delightful way of story-telling which is possessed by Miss Elizabeth Clark, who spoke at the Women's Luncheon Club, Hull, on the art of story-telling in America.  I remembered her as soon as she began, and the fact that she had been to the Luncheon Club before, and had enchanted me especially with the tale of a hungry  family finding a cabbage in their garden which grew and grew until they had enough to make a good meal and soup for the whole family even giving the "Little Moose a cup o' soup", to use the wording of  a child to whom she had previously told it.''

''She really is a refreshing person, and makes one feel young again as well as becoming extremely envious of her art.   I think, too, she makes our next feeble attempt at story-telling easier,  because she makes you realise that to understand a story people must really “see” in their mind the objects spoken of.  She always gives a vivid picture, and you have no doubt as to what the people in the story are like, what the surroundings look like.''<ref name =HullDM/>
</blockquote>

Her 1933 publication ''Twenty Tales for Telling'' was dedicated to the Girl Scouts of America and includes a story ''Jack-in-the-Pulpit'' based on her experiences in [[New England]].<ref name=Clark1933>{{cite book |last= Clark|first= Elizabeth|date= 1933|title= Twenty Tales for Telling |publisher= University of London Press  |pages= 11–21,}}</ref> Alexander Haddow wrote in a review  of this collection about Elizabeth Clark's power of telling stories:

<blockquote>''Those who have heard her tell a story know what a perfect artist she is, how she lives her part, what perfect command of her voice and what a gift of expression she has.''</blockquote>

He had heard her speak and wondered how a girl of eight would react to the printed text.  Having read them all, the girl to whom he gave the book pronounced them all lovely leaving the reviewer with the strong impression that Miss Clark's charm had come through in print.

<blockquote>''There's a sweetness in her English and a simplicity both in vocabulary and construction that make it particularly suitable for children.''<ref>{{cite journal |last= Haddow |first=Alexander |title=Twenty Tales for Telling   |journal= The Scottish Educational Journal, Monthly Book Supplement |date=28 April 1933}}</ref></blockquote>
[[File:EC 1930.jpg|thumb|upright|Book cover for ''Tell me a Tale'' showing Elizabeth Clark telling stories]]

She lived in the [[Notting Hill]] area of London during the inter war years, returning to [[Winchester]] before the second world war where she lived with her sister Dorothy who taught Mathematics at St. Swithun's.  She continued to lecture regularly and widely in England and Scotland.<ref name = HullDM>{{cite news | title = The Art of Story-Telling| newspaper = Hull Daily Mail | page = 5 | date = 1932-03-11}}</ref><ref>{{cite news | title = Story-Building and Story-Telling| newspaper = Derbyshire Times and Chesterfield Herald | page = 18 | date = 1933-04-01}}</ref><ref>{{cite news | title = Scottish Girl Guides : "Handrails" for Guides | newspaper = The Scotsman | page = 14 | date = 1935-10-28}}</ref><ref>{{cite news | title = Art of Story-Telling | newspaper = Morpeth Herald | page = 12 | date = 1937-10-01}}</ref><ref>{{cite news | title = Miss Clark in Motherwell | newspaper = Motherwell Times | page = 5 | date = 1939-02-24}}</ref><ref>{{cite news | title = Tables Turned on Wearside teachers | newspaper = Sunderland Daily Echo and Shipping Gazette | page = 6 | date = 1951-04-20}}</ref>

Elizabeth Clark's stories were broadcast on the [[BBC]] children's programme Children's Corner between 1924 and 1926 sometimes told by others sometimes told by her.  Similarly her stories could be heard during the war years between 1940-1944 and later from 1961-1965 on [[Children's Hour]].  Some were subsequently told on the BBC's TV [[Jackanory]] programme in 1979.<ref name="TG"/><ref>{{cite web |url=http://genome.ch.bbc.co.uk/ |title = BBC Genome Beta 1923-2009     |access-date=27 November 2016}}</ref>
                                                                                     
In 1951 she wrote to the Director of Studies, Mr Hardie, for the Training of Teachers at the Training College in Aberdeen explaining she would find it difficult to continue her regular visits there.  However she continued to tell stories nearer home.  She died in Winchester in April 1972 and is buried at [[Kilmeston]].{{citation needed|date=December 2016}}

In 1995 Winchester City Council included in a citywide literary festival an exhibition entitled ''Hampshire Daughters''. This featured three women writers with Winchester connections:  [[Jane Austen]], [[Charlotte Yonge]] and Elizabeth Clark who were born approximately 50 years apart.<ref>Programme for A Celebration of Literature in Winchester 15th-24th September 1995. Published by The Special Events Unit,  Winchester Tourist Information Centre</ref>''

== Technique and sources of material ==

For the material for her stories she drew upon folk lore world wide, from history, legend and the [[Bible]]. ''Stories to Tell and How to Tell Them'',<ref name = Clark1927/> ''More Stories and how to Tell Them''  <ref name = Clark1928/> and ''The Tale that had no Ending'' <ref name = Clark1929/> each have an introduction on the art of story telling and following each story some comments about, for example, its origin and tips on how to tell or read it.

For the tales from legend she explains where she found the tale and how and why she adapted it. In ''The Old Woman and the Pixies and the Tulips'' she 'builds' the little white house in detail, its garden and all its flowers until the scene stands clear and the action can begin.<ref name = Clark1927 /> {{ rp | 126–137}} In her notes she urges setting the scene slowly and without hurry for the listeners to be able place themselves in context.  “...unless listeners have a perfect idea of the “geography” of the story, they will not be able to grasp the situation as they should".  There is often a moral as in the [[Norse mythology|Norse]] folk-tale ''Jonathan John and his Wife'', but earlier versions she felt had been brief and concentrated too much on the quarrel between man and wife, and a bias against Jonathan which she sought to rebalance.<ref name=Clark1927>{{cite book |last= Clark |first= Elizabeth |date= 1927|title= Stories to Tell and How to Tell Them  |publisher= University of London Press   }}</ref>{{rp|72–81}}  Others like the ''Old Woman who Lived in a Vinegar Bottle'' <ref name=Clark1928>{{cite book |last= Clark|first= Elizabeth|date= 1928|title= More Stories and How to Tell Them |publisher= University of London Press  }}</ref> {{rp|47–60}}and ''Good Luck and Mrs Featherfuss'' <ref name=Clark1929>{{cite book |last= Clark |first= Elizabeth |date= 1929|title= The Tale that had no Ending  |publisher= University of London Press  }}</ref>{{rp |125–136}} each have a lesson too, but after finishing it the reader, young or old, is left to decide for themselves what that lesson is.

In her telling of Bible stories, as in ''A Little Book of Bible Stories'', included stories such as that of Moses and Miriam and ''The Little Ark of Rushes''.<ref name=Clark1938b>{{cite book |last= Clark |first= Elizabeth |date= 1938|title= A Little Book of Bible Stories |publisher= University of London Press }}</ref>{{rp |77–84 }} Other legendary stories were set in the [[Middle East]],<ref name = Clark1936>{{cite book |last=Clark |first=Elizabeth |date=1936 |title=Tales for Jack and Jane |publisher = University of London Press }}</ref>{{rp |114–123}} [[Japan]],<ref name = Clark1929 />{{rp |42–52}}<ref name = Clark1938a />{{rp|68–96}} [[India]]<ref name = Clark1928/>{{ rp|77–85}} and [[Russia]].<ref name = Clark1933 />{{ rp |168–176}}

She often drew on her own experiences as in for example the Elizabeth Ann Stories about her own childhood in which things happen to, or are made to happen by, Elizabeth Ann and her sister Ruth Mabel (her younger sister in real life).<ref name=Clark1933/>{{rp |7}} {{rp|94–101}}{{ rp|102–103}}<ref name=Clark1938a>{{cite book |last= Clark |first= Elizabeth |date= 1938|title= Tell me a Tale |publisher= University of London Press }}</ref>{{ rp |92–103}} Many of her stories were initially published in the periodical ''Child Education''.<ref>''Child Education''. Published by Evans Brothers Limited, 44-48 Clarence Road, St. Albans, Herts.</ref> For example, between January 1944 and December 1946 she published seven short stories about a little girl called Polly and her great-grandmother. These were later included in ''Sunshine Tales for Rainy Days''.<ref name=Clark1948>{{cite book |last= Clark |first= Elizabeth |date= 1948|title= Sunshine Tales for Rainy Days |publisher= University of London Press }}</ref> One of her original stories (as opposed to retold legends) ''Father Christmas and the Donkey'',<ref name=Clark1993>{{cite book |last= Clark |first= Elizabeth |date= 1993|title= Father Christmas and the Donkey |publisher= Penguin Books }}</ref> which was first published in 1942 as ''The Donkey that Helped Father Christmas'',<ref name=Clark1942>{{cite book |last= Clark |first= Elizabeth |date= 1942|title= Twilight and Fireside |publisher= University of London Press }}</ref>{{ rp |98–108}} was written following the [[London blitz]] in [[World War II]] and for children evacuated as a result.  The boys in the original story were evacuees from the blitz.

Clark's love of animals shines through her collections.  Sometimes it's the interactions amongst animals alone as in ''Father Sparrow's Tug of War''.<ref name = Clark1927/>{{rp|82–92}}  Sometimes humans' relationships and interdependence with animals is the key to the story as in (albeit very briefly) ''Robin Redbreast's Thanksgiving''<ref name = Clark1927/>{{rp |138–150}} not forgetting of course ''Father Christmas and the Donkey''.

== Publications ==
In total, Clark wrote over a hundred stories by the end of her life.<ref>{{cite web|title=ELIZABETH CLARK|url=http://pikkupublishing.com/elizabeth-clark/|website=Pikku Publishing|accessdate=15 December 2016}}</ref> The majority of these appear in her published collections:
{| class="wikitable"
|-
| ''Stories to tell and How to Tell Them'' 
| University of London Press. 1927
|-
| ''Practical Infant Teacher'' 
| Isaac Pitman & Sons. 1928 
|-
| ''More Stories and How to Tell Them'' 
| University of London Press. 1928
|-
| ''The Tale that had no Ending and Other Stories'' 
| University of London Press. 1929
|-
| ''Twenty Tales for Telling'' 
| University of London Press. 1933
|-
| ''Standard Bearers'' 
| University of London Press. 1934
|-
| ''Tales for Jack and Jane'' 
| University of London Press. 1936	
|-
| ''The Elizabeth Clark Story Books'' (4 booklets) 
| University of London Press. 1936
|-
| ''A Little Book of Bible Stories'' 
| University of London Press. 1938
|-
| ''Tell Me a Tale'' 
| University of London Press. 1938
|-
| ''Twilight and Fireside'' 
| University of London Press. 1942
|-
| ''Sunshine Tales for Rainy Days'' 
| University of London Press. 1948 
|-
| ''Elizabeth Clark Plays'' <br> Adapted by Gladys Cooper from stories by Elizabeth Clark
| University of London Press. 1953
|-
|}

Some stories have been reprinted since her death:

{| class="wikitable"
|- 
| ''Stories to Tell''
| Brockhampton Press. 1974
|-
| Audiotape:  Stories from ''Stories to Tell'' <br>  Read by Anna Ford 
| Hodder & Stoughton. Times Newspapers Ltd. <br> Ivan Berg Associates.  1978	
|-
| ''Country Tales to Tell'' 
| Hodder & Stoughton. 1978  
|-
| ''Tales for All Seasons'' 
| Hodder & Stoughton. 1986  
|-
| ''Country Tales '' 
| Hodder Children’s Books. 1996
|-
| ''Father Christmas and the Donkey''  <br> Illustrated by Jan Ormerod 
| Penguin Books. 1993 <br>Translated into German, Italian, Swedish and Japanese
|- 
| ''Elizabeth Clark Story Books'' Vols I and II 
| Pikku Publishing. 2015
|-
| ''Elizabeth Clark Story Books'' Vols III and IV 
| Pikku Publishing. 2016
|-
| ''Das Eselchen und der Weihnachtsmann'' 
| Coppenrath. 2016
|-
| ''Das Eselchen und der Weihnachtsmann'' <br> text only in collection ''Tierisch tolle Weihnachten'' 
| Coppenrath. 2016
|}

== References ==
{{reflist|30em}}
{{authority control}}

{{DEFAULTSORT:Clark, Elizabeth}}
[[Category:English women writers]]
[[Category:English children's writers]]
[[Category:British storytellers]]
[[Category:1875 births]]
[[Category:1972 deaths]]
[[Category:19th-century English writers]]
[[Category:20th-century English writers]]
[[Category:People from Worcestershire (before 1974)]]