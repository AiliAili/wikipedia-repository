'''Schlieren imaging''' is a method to visualize density variations in transparent media.<ref name="korpel-1987">
A. Korpel, D. Mehrl and H.H. Lin, ''Schlieren Imaging of Sound Fields'', IEEE 1987 Ultrasonics Symposium, pages: 515–518, (1987).
</ref>
[[Image:Schlieren HIFU 1MHz.JPG|thumb|300px|Schlieren imaging of a focusing ultrasonic transducer]]
The term "schlieren imaging" is commonly used as a synonym for [[schlieren photography]], though this article particularly treats visualization of the pressure field produced by [[ultrasonic sensor#Transducers|ultrasonic transducers]], generally in water or tissue-mimicking media. The method provides a two-dimensional (2D) projection image of the acoustic beam in real-time ("live video"). 
The unique properties of the method enable the investigation of specific features of the acoustic field (e.g. focal point in [[HIFU]] transducers), detection of acoustic beam-profile irregularities (e.g. due to defects in transducer) and on-line identification of time-dependent phenomena <ref name="brown-2009">
S.A. Brown, et al., ''Characterization of Nonthermal Focused Ultrasound for Noninvasive Selective Fat Cell Disruption (Lysis): Technical and Preclinical Assessment'', Plast. Reconstr. Surg., '''124(1)''', pages: 92–101, (2009).
</ref>
(e.g. in [[phased array ultrasonics|phased array transducers]]). Some researchers say that schlieren imaging is equivalent to an [[radiography|X-ray radiograph]] of the acoustic field.

== Setup ==
[[Image:Schlieren imaging setup.GIF|thumb|300px|Schlieren imaging system setup: linear lens-based configuration]]
The optical setup of a schlieren imaging system may comprise the following main sections:
Parallel beam, focusing element, stop (sharp edge) and a camera.
The parallel beam may be achieved by a point-like light source (a laser focused into a pinhole is sometimes used) placed in the focal point of a collimating optical element (lens or mirror). 
The focusing element may be a lens or a mirror.
The optical stop may be realized by a razor placed horizontally or vertically in the focal point of the focusing element, carefully positioned to block the light spot image on its edge.
The camera is positioned behind the stop and may be equipped with a suitable lens.

== Physics ==

=== Ray optics description ===
A parallel beam is described as a group of straight and parallel 'rays'.
The rays cross through the transparent medium while potentially interacting with the contained acoustic field, and finally reach the focusing element.
Note that the principle of a focusing element is directing (i.e. focusing) rays that are parallel - into a single point on the focal plane of the element. 
Thus, the population of rays crossing the focal plane of the focusing element can be divided into two groups: those that interacted with the acoustic field and those that didn't. The latter group is undisturbed by the acoustic field, so it remains parallel and forms a point in a well-defined position in the focal plane. The optical stop is positioned exactly at that point, so as to prevent all corresponding rays from further propagating through the system and to the camera.
Thus we get rid of the portion of light that crossed the acoustic field without interaction.
However, there are also rays that did interact with the acoustic field in the following manner:
If a ray travels through a region of nonuniform density whose spatial gradient has a component orthogonal to the ray, that ray is deflected from its original orientation, as if it were passing through a [[prism (optics)|prism]]. This ray is no longer parallel, so it doesn't intersect the focal point of the focusing element and is not blocked by the knife. In some circumstances the deflected ray escapes the knife-blade and reaches the camera to create a point-like image on the camera-sensor, with a position and intensity related to the inhomogeneity experienced by the ray. An image is formed in this way, exclusively by rays that interacted with the acoustic field, providing a mapping of the acoustic field.

=== Physical optics description ===
The [[acousto-optics|acousto-optic]] effect couples the optical [[refractive index]] of the medium with its density and pressure. Thus, spatial and temporal variations in pressure (e.g., due to ultrasound radiation) induces corresponding variations in refractive index. Optical [[wavelength]] and [[wavenumber]] in medium depend on refractive index. The [[Phase (waves)|phase]] acquired by [[electromagnetic radiation|electromagnetic wave]] traveling through the medium is related to the line-integral of the wavenumber along the propagation line.
For a plane-wave electromagnetic radiation traveling parallel to the Z-axis, the XY planes are iso-phase manifolds (regions of constant phase; the phase does not depend on coordinates (x,y)). However, when the wave emerges from the acoustic field, XY planes are not iso-phase manifoldes anymore; the information about the accumulated pressure along each (x,y) line resides in the phase of the emerging radiation, forming a phase image (phasor) in the XY plane. The phase information is given by the Raman-Nath parameter:<ref name="raman-1935">
C.V. Raman and N.S.N. Nath, ''The Diffraction of Light by High Frequency Sound Waves: Part I'',
Proc. Indian Academy Sci., '''2''', pages: 406–412, (1935).
</ref><br />
: <math> v(x,y)= \frac{2\pi\kappa}{\lambda} \int{p(x,y,z)} \, dz</math>
with <math>\kappa</math> - the piezooptic coefficient, <math>\lambda</math> the optical wavelength and 
<math>p(x, y, z)</math> the three-dimensional pressure field.<ref name="cook">
B.D. Cook, E. Cavanagh and H.D. Dardy, ''A Numerical Procedure for Calculating the Integrated Acoustooptic Effect'', IEEE Trans. on Sonics and Ultrasound, '''SU-27(4)''', pages:202-207, (1980).
</ref>
The schlieren technique converts the phase information into an intensity image, detectable by a camera or a screen.

== Application ==
The accepted gold-standard for quantitative acoustic measurement is the [[hydrophone]]. However, scanning the acoustic field with a hydrophone suffers from several limitations, giving rise to supplementary evaluation methods such as the schlieren imaging. The importance of the schlieren imaging technique is prominent in HIFU research and development.
<ref name="charlebois">
T.F. Charlebois and R.C. Pelton,
''Quantitative 2D and 3D Schlieren Imaging for Acoustic Power and Intensity Measurements'',
Medical Electronics, pages: 66–73, (1995).
</ref>
Advantages of schlieren imaging include:
* Free field: the investigated acoustic field is not distorted by the measuring probe.
* High intensity measurements: the method is compatible with high acoustic intensities.
* Real time: Schlieren imaging system provides on-line, live video of the acoustic field.

== References ==
{{Reflist}}

== External links ==
* [https://www.youtube.com/watch?v=02VCZsd9M58 A presentation of schlieren imaging on YouTube]
* [http://www.ultrasonics.org/Proceedings_2007_UIA/Ben-Ezra_2007_UIA.pdf Acoustic  Field Characteriztation with Schlieren System - a short presentation]

[[Category:Acoustics]]
[[Category:Ultrasound]]
[[Category:Optics]]
[[Category:Imaging]]