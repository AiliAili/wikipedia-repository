{{Infobox journal
| title = Journal of Experimental Psychology: General
| cover = [[File:JEP, General.gif]]
| editor = [[Isabel Gauthier]]
| discipline = [[Experimental Psychology]]
| abbreviation = J. Exp. Psychol.-Gen. 
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Bimonthly
| history = 1916-present
| openaccess = 
| license =
| impact = 4.070
| impact-year = 2015
| website = http://www.apa.org/pubs/journals/xge/index.aspx
| link1 = http://psycnet.apa.org/index.cfm?fa=browsePA.volumes&jcode=xge
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 2441503
| LCCN = 
| CODEN =
| ISSN = 0096-3445
| eISSN = 1939-2222
}}
The '''''Journal of Experimental Psychology: General''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]]. It was established in 1975 as an independent section of the ''[[Journal of Experimental Psychology]]'' and covers research in [[experimental psychology]].<ref name="apa.org">{{cite web |date=June 25, 2013 | url=http://www.apa.org/pubs/journals/xge/index.aspx |title=Journal of Experimental Psychology: General|publisher=[[American Psychological Association]] |accessdate=2013-06-25}}</ref> The journal may include articles on the following topics:<ref name="apa.org"/> 
* social processes 
* developmental processes 
* psychopathology 
* neuroscience 
* computational modeling

The current [[editor-in-chief]] is [[Isabel Gauthier]] ([[Vanderbilt University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by [[MEDLINE]]/[[PubMed]] and the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.070, ranking it 6th out of 85 journals in the category "Psychology, Experimental".<ref name="WoS">{{cite book|title=2015 [[Journal Citation Reports]]|last=|first=|publisher=[[Thomson Reuters]]|year=2016|isbn=|edition=Social Sciences|series=Web of Science|location=|pages=|chapter=Journals Ranked by Impact: Psychology, Experimental|quote=|postscript=.|via=|accessdate=2015-08-26}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official website| http://www.apa.org/pubs/journals/xge/index.aspx}}

[[Category:American Psychological Association academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1975]]
[[Category:Quarterly journals]]
[[Category:Experimental psychology journals]]


{{psych-journal-stub}}