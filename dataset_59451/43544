<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=A.10
 | image=Saunders A.10.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Single-seat fighteer
 | national origin=United Kingdom
 | manufacturer=[[Saunders-Roe|S.E. Saunders Ltd]].
 | designer=Harry Knowler
 | first flight=27 January 1929
 | introduced=
 | retired=1933
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Saunders A.10''' was a private venture four-gun fighter.  It was a single-seat, single-engined biplane with poor handling, later serving as a gun-testing aircraft.

==Development==
After their first aircraft, the [[Saunders T.1|T.1]], Saunders did not build another landplane until 1928 when the '''Saunders A.10''' single-seat fighter appeared. The original design was a private venture, its distinguishing feature its four-gun armament at a time when two machine guns were standard. Because of this the A.10 was sometimes referred to as the "Multi-gun"; because by the time the A.10 was ready to fly S.E.Saunders Ltd had been bought out by A.V. Roe and John Lloyd it is sometimes known as the [[Saunders-Roe|Saunders/Saro]] A.10<ref name="A10">{{Harvnb|London|1988|p=85.}}</ref> or the Saro A.10.
 
[[List of Air Ministry Specifications#1920-1929|Air Ministry specification F.20/27]] was issued during the design process and Saunders decided to submit the A.10 even though that specification only called for two guns and suggested the use of the radial [[Bristol Mercury]] rather than the inline [[Rolls-Royce Kestrel|Rolls-Royce F.XIS]] that designer Harry Knowler had chosen.<ref name="A10"/>  The [[Air Ministry]] provided an unsupercharged F.XI engine and the A.10 first flew on 27 January 1929.  It was a compact single bay [[sesquiplane]] with a duraluminum airframe, fabric covered throughout apart from the forward fuselage.  The fuselage was largely built from tubular members, bolted together.  The pilot sat with the upper wing at eye level to optimise the view both above and below, helped by cut-outs in the lower plane roots and a thinner 
aerofoil in the upper centre section.  The breeches of all four guns were accessible to him, with two in the forward decking and two in the fuselage sides.  The water-cooled engine's chin radiator sat behind a roller blind 
shutter, close to the twin-bladed propeller.  The simple single axle undercarriage was mounted on a pair of inverted V-struts joining the fuselage ahead and aft of the lower wing.<ref name="Lon">{{Harvnb|London| 1988|pp=85–87, 89.}}</ref>

The broader chord, greater span upper wing carried the ailerons, initially unbalanced but quickly modified to Frise hinged type to lighten the feel.  Stagger was exaggerated by the smaller chord of the lower wing, which was mounted on the bottom of the fuselage.  The rudder and elevators, the latter mounted on a cantilever tailplane were also unbalanced.<ref name="Lon"/>

After the first flight and the aileron modifications, flight testing recommenced in March.  In July it was decided to submit the aircraft under [[List of Air Ministry Specifications#1920-1929|Air Ministry specification F.10/27]] as well as F.20/27, the earlier specification calling for a six gun fighter.  In armament terms, the A.10 fitted neither.  In August 1929 it went to the [[Aeroplane and Armament Experimental Establishment]] (A&AEE) for tests under F.20/27, armed with just two guns. The reports were very critical: the A.10 suffered from longitudinal instability and was almost impossible to hold at constant speed in dives or in the climb.  Ground handling was also said to be difficult.<ref>{{Harvnb|London| 1988|pp=87–89.}}</ref>

These trials ended in January 1930 and the A.10 went back to Saro's for modification.  Changes included a 3 in (76&nbsp;mm) shift aft of the main undercarriage to improve ground handling, a 1&nbsp;ft 9 in (533&nbsp;mm) fuselage extension and revised empennage.  The tailplane now had a straight, rather than swept leading edge.  When it went back to the A&AEE in September most of the old faults remained and no more were ordered.<ref>{{Harvnb|London| 1988|p=90.}}</ref>

Curiously, in view of the A&AEE's comments on the problems the A.10's longitudinal instability would pose it as a gun platform, it was used by the Establishment to explore the effects of multi-gun armament, trials also involving the [[Gloster Gauntlet|Gloster SS.19]].  These lasted initially until December 1930, then, after a period of unserviceability from 1932-3.<ref>{{Harvnb|London 1988|p=91.}}</ref>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (F.20/27 configuration, short fuselage)==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value e.g. "at low level", "unladen". -->
{{aerospecs
|ref={{harvnb|London|1988|p=92.}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=1
|capacity=
|length m=7.44
|length ft=24
|length in=5
|span m=9.75
|span ft=32
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.97
|height ft=9
|height in=9
|wing area sqm=25.36
|wing area sqft=273
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=2,674
|gross weight kg=
|gross weight lb=3,467
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Rolls-Royce Kestrel|Rolls-Royce F.XI]] 12-cylinder water-cooled inline
|eng1 kw=358<!-- prop engines -->
|eng1 hp=480<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=322
|max speed mph=200
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=8,840
|ceiling ft=(service) 29,000
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=2×0.303 in (7.7 mm) [[Vickers machine gun]]s
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Saunders-Roe}}
;Notes
{{Reflist}}

;Bibliography
{{Refbegin}}
*{{cite book |title= Saunders and Saro Aircraft since 1917|last= London|first=Peter |authorlink= |coauthors= |year= |publisher=Putnam Publishing, 1988  |location=London |isbn= 0-85177-814-3 |page= |pages= |url=|ref=harv }} 
{{refend}}
<!-- ==External links== -->
{{Saro aircraft}}

[[Category:British fighter aircraft 1920–1929]]
[[Category:Saro aircraft|A.10]]
[[Category:Sesquiplanes]]