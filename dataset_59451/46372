{{Infobox ancient site
| name           = Baking Pot
| alternate_name = 
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| map            = 
| map_type       = 
| map_alt        = 
| map_caption    = 
| map_size       = 
| relief         = 
| coordinates             = {{coord|17|12|11|N|89|1|10|W|display=inline}}
| map_dot_label  = 
| location       = [[San Ignacio (town)|San Ignacio, Belize]],&nbsp;[[Cayo District, Belize|Cayo District]],&nbsp;{{flag|Belize}}
| region         = [[Cayo District, Belize|Cayo District]]
| built          = 
| abandoned      = 
| epochs         = Preclassic to Postclassic occupation
| cultures       = Maya
| event          = 
| excavations    = 
| archaeologists = Oliver Ricketson, Bullard and Bullard, Belize Valley Archaeological Reconnaissance Project (current)

| architectural_styles = 
| architectural_details = 

| notes          = 


| designation1            = 
| designation1_offname    = 
| designation1_type       = 
| designation1_criteria   = 
| designation1_date       = 
| delisted1_date          = 
| designation1_partof     = 
| designation1_number     = 
| designation1_free1name  = 
| designation1_free1value = 
| designation1_free2name  = 
| designation1_free2value = 
| designation1_free3name  = 
| designation1_free3value = 
| designation2            = 
| designation2_offname    = 
| designation2_type       = 
| designation2_criteria   = 
| designation2_date       = 
| delisted2_date          = 
| designation2_partof     = 
| designation2_number     = 
| designation2_free1name  = 
| designation2_free1value = 
| designation2_free2name  = 
| designation2_free2value = 
| designation2_free3name  = 
| designation2_free3value = 
| precolumbian = yes <!-- non-functional tracking parameter, do not remove/change -->
}}
{{Maya civilization}}
'''Baking Pot''' is a [[Maya civilization|Maya]] [[archaeological site]] located in the [[Belize River|Belize River Valley]] on the southern bank of the river, northeast of modern day town of [[San Ignacio (town)|San Ignacio]] in the [[Cayo District]] of [[Belize]]; it is {{convert|6|km|mi}} downstream from the [[Barton Ramie]] and [[Lower Dover]] archaeological sites. Baking Pot is associated with an extensive amount of research into Maya settlements, community-based archaeology, and of agricultural production; the site possesses lithic workshops, and possible evidence of cash-cropping [[Cocoa bean|cacao]]<ref name="Willey 65">Willey, G. R., Bullard, W. R., Glass, J. B., Gifford, J. C., & Elliot, O. (1965). Prehistoric Maya settlements in the Belize Valley. Cambridge, Mass: Peabody Museum.</ref><ref>Audet, Carolyn, and Jaime J. Awe. (2004)What's Cooking at Baking Pot: A Report of the 2001-2003 Field Seasons. In Jaime Awe, John Morris and Sherilyne Jones, Eds., Research Reports in Belezian Archaeology, Vol 1, pp. 49-60.</ref> as well as a long occupation from the [[Preclassic Maya|Preclassic]] through to the [[Mesoamerican chronology|Postclassic]] period.

The site at Baking Pot is unique in that it had a large population during the Terminal Classic while other sites in the Belize River Valley were declining, and occupation continued into the Postclassic whereas major Classic Period sites in the southern lowlands were by then abandoned.<ref name="Aimers, James J. 2003">Aimers, James J. (2003). Abandonment and Nonabandonment at Baking Pot, Belize. In Takeshi Inomata and Ronald W. Wedd, Eds., The Archaeology of Settlement Abandonment in Middle America. Salt Lake City: University of Utah Press.</ref>  After the Classic period site cores in much of the Belize Valley were abandoned, but at Baking Pot “survey and excavation of house mounds and plazuela groups immediately outside the site core suggested that Postclassic occupation there is more substantial and prolonged than in the site core”.<ref name="Aimers, James J. 2003"/> The abundance of [[Tayasal]]-associated Augustine Red ceramics at Baking Pot, along with the association of these ceramics with a different organizational and settlement pattern suggest that there was an intrusion of people from central [[Petén Basin|Petén]] during this time.<ref name="Aimers, James J. 2003"/><ref>Chase, Arlen F. (1986). Time Depth or Vacuum: The 11.3.0.0.0 Correlation and the Lowland Maya Postclassic, in J.A. Sabloff and E.W. Andrews V, Eds., Late Lowland Maya Civilization: Classic to Postclassic, pp. 99-140, University of New Mexico Press, Albuquerque.</ref> Researchers like Aimers favor a gradual abandonment of the site at a much later time period than other sites in the region.

== History ==
In the late [[Preclassic Maya|Preclassic]], Baking Pot had a small population with little public [[Maya architecture|architecture]]. In the Early Classic, the site experienced a construction boom. Two architectural groups were built, with Group A to the north and Group B (containing the largest structure at Baking Pot) to the south. This north-south orientation is similar to nearby [[Xunantunich]].<ref name="Awe 2008">Awe, Jaime J. (2008) Architectural Manifestations of Power and Prestige: Examples from Classic Period Monumental Architecture at Cahal Pech, Xunantunich and Caracol, Belize. In John Morris, Sherilyne Jones, Jaime Awe, and Christophe Helmke, Eds., Research Reports in Belizean Archaeology  Volume 5, pp. 159-174. Institute of Archaeology, National Institute of Culture and History, Belmopan, Belize.</ref> Earlier excavators like Ricketson, [[Gordon Willey]], and Bullard and Bullard describe these groups as Group 1 and Group 2. These major complexes make up the center of Baking Pot and are connected by a causeway (or ''[[sacbe]]''). In the Late Classic, the population increased to approximately 3000 people. Toward the end of the Classic period the local elite left and the palace complexes in the city center were abandoned. In the early Postclassic, people were still living on a portion of the site but rarely used the ceremonial center and there was very little new construction. Much of the people living at Baking Pot were farmers; being close to the Belize River the site has fertile soil in an [[alluvial valley]] and is primarily associated with agricultural production.

== Excavations ==
In the 1920s, A.H. Anderson first conducted archaeological research at Baking Pot after some materials from the site were used in the construction of the western highway.  Later excavations by Oliver Ricketson,<ref name ="Ricketson 31">Ricketson, O. G. (1931). Excavations at Baking Pot, British Honduras. In Contributions to American Archaeology, Vol 1, No. 1, pp. 1-28. Washington, D.C: Carnegie Institute of Washington</ref> Gordon Willey in the 1950s and William Bullard and Mary Bullard completed major excavations in the 1960s,.<ref name ="Bullard 65">Bullard, W. R., & Bullard, M. R. (1965). Late classic finds at Baking Pot, British Honduras. Art and Archaeology Occasional Papers, No. 8. Toronto: University of Toronto Press.</ref> Wiley is best known for his excavations and settlement research at Barton Ramie and for his focus on Maya households during a time when most people were only focused on elite.<ref name="Willey 65"/> Limited work was done at the site during the 1920s, 1950s, and 1960s until the [[Belize Valley Archeological Reconnaissance Project]] started working there in the 1990s, with its work continuing into the 21st century<ref name="ReferenceA">Awe, Jaime J., Julie Hoggarth, and Christophe Helmke. Prehistoric Settlement Patterns in the Upper Belize River Valley and Their Implications for Models of Low-Density Urbanism.</ref> with BVAR researchers working under the direction of Dr. Jaime Awe, including Jim Conlon, Jim Aimers, Josalyn Ferguson, Jennifer Piehl, Carolyn Audet, Christophe Helmke, and Julie Hoggarth.

A complex water management system exists at the site, including a series of aguadas and seasonal streams, along with the presence of drains in the palace complex, work to feed water from the foothills to the south down through the site and into the aduadas before dumping into the Belize River.  Baking Pot is named after large pots were found by archaeologists that were once used to boil [[chicle]]. At the Bedran group nearby, a house group that was excavated, burial grave goods were found including painted ceramic vessels with a [[primary standard sequence]] around the top dated to the Early Classic.<ref name="AweHelmke 05">Awe, Jaime J. and Christophe G.B. Helmke. (2005). Alive and Kicking in the 3rd to 6th Centuries A.D.: Defining the Early Classic in the Belize River Valley. In Jaime Awe, John Morris, Sherilyne Jones, and Christophe Helmke, Eds., Research Reports in Belizean Archaeology, Vol 2:39-52. Institute of Archaeology, National Institute of Culture and History, Belmopan, Belize.</ref> These vessels were cacao drinking vessels and were thought to contain a placename: 
''Four Water Place'', although this is now reinterpreted as a royal title.  There are no carved monuments at Baking Pot, although several uncarved [[Maya stelae|stelae]] and uncarved altars have been found. 

A causeway extends south and to the west of Group B and ends at a causeway terminus structure (Mound 190). Here hundreds of broken vessels were found in front of the stairway, possibly from a termination ritual. Mound 190 had deposits with finger bones, an altar, and intact mini ceramic vessels below it with tiny specks of jade. This mound also contains evidence of ritual activity and is believed to be used for ritual/ceremonial purposes. The discovery of finger bones is similar to the finger bowl caches associated with Caracol (and also found at [[Cahal Pech]])  and may provide evidence of Caracol control or influence at Baking Pot at the time.<ref name="HelmkeAwe 08">Helmke, Christophe and Jaime J. Awe. (2008). Organización Territorial de los Antiguos Mayas de Belice Central: Confluencia de Datos Arqueológicos y Epigráficos. Mayeb 20:65-91. In press Ancient Maya Territorial Organization of Central Belize: Confluence of Archaeological and Epigraphic Data. Acta Mesoamericana.</ref> Vessel 2 at Baking Pot describes its owner in a similar structure that is found at Caracol as well.<ref name="HelmkeAwe 08" /> [[Naranjo]] pottery has also been found here at Baking Pot, and evidence for a push to control the Belize River Valley after the fall of Tikal was described on a monument at Xunantunich.

== References ==
{{Reflist}}

{{Refbegin}}

== Further reading ==
* Audet, Carolyn. (2004). Excavations of structure 190, Baking Pot, Belize. In Carolyn Audet and Jaime Awe, Eds., The Belize Valley Archaeological Reconnaissance Project: Report on the 2003 Field Season, pp.&nbsp;35–55. Institute of Archaeology, National Institute of Culture and History, Belmopan, Belize.
* Awe, Jaime J. "Early Classic/Late Classic Maya" Lecture sponsored by The Belize Valley Archaeological Reconnaissance Project. Galen University, 17 July 2011.
* Bullard, W. R., & Bullard, M. R. (1965). Late classic finds at Baking Pot, British Honduras. Art and Archaeology Occasional Papers, No. 8. Toronto: University of Toronto Press.
* Chase, Diane Z. (2004). Diverse Voices: Toward and Understanding of Belize Valley Archaeology. In James F. Garber, Ed., The Ancient Maya of the Belize Valley: Half a Century of Archaeological Research, pp.&nbsp;335–348. Gainesville: University Press of Florida.
* Conlon, James F. and Allan F. Moore. (2003). Identifying Urban and Rural Settlement Components: An Examination of Classic Period Plazuela Group Function at the ancient Maya Site of Baking Pot, Belize, in Gyles Iannone and Samuel V. Connell, Eds., Perspectives on ancient Maya rural complexity, pp.&nbsp;59–70.  Los Angeles: Cotsen Institute of Archaeology, University of California, Los Angeles.
* Hoggarth, Julie A.  (2009) Settlement and Community Organization in the Classic to Postclassic Transition: Research Methodology and Aims of the 2007 to 2010 Settlement Research at Baking Pot, Belize. In Julie A. Hoggarth and Jaime J. Awe, Eds., The Belize Valley Archaeological Reconnaissance Project: A Report of the 2008 Field Season, pp.&nbsp;1–25. Institute of Archaeology, National Institute of Culture and History, Belmopan, Belize.
* Piehl, J.C. (1997). The Burial Complexes of Baking Pot: Preliminary Report on the 1996 Field Season. In Belize Valley Archaeological Reconnaissance Project: Progress Report of the 1996 Field Season, edited by Jaime J. Awe and J.M. Conlon. pp.&nbsp;59–70. Department of Anthropology. Trent University. Peterborough, Ontario.
* Piehl, Jennifer C. (2004). Performing Identity at an Ancient Maya City: The Archaeology of Houses, Health and Social Differentiation at the Site of Baling Pot, Belize. Ph.D. Dissertation. Department of Anthropology, Tulane University, New Orleans.
* Weeks, John M., Hill, Jane A., & Carnegie Institution of Washington. (2006). The Carnegie Maya: The Carnegie Institution of Washington Maya research program, 1913-1957. Boulder: University Press of Colorado.
{{Refend}}

== External links ==
* [http://www.nichbelize.org/ia-general/welcome-to-the-institute-of-archaeology.html/ Belize Institute of Archaeology, National Institute of Culture and History]
* [http://www.bvar.org/ Belize Valley Archaeological Reconnaissance Project]
* [http://www.famsi.org/reports/02090/section01.htm/ Foundation for the Advancement of Mesoamerican Studies, Inc.]

{{Maya sites}}

[[Category:Maya sites in Belize]]
[[Category:Cayo District]]
[[Category:1920s archaeological discoveries]]