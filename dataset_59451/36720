{{Infobox Peer
|name          = The Earl of Arundel
|image         = Arundel2.JPG
|image_size    = 240px
|caption       = John FitzAlan's funeral effigy in [[Arundel Castle]].
|birth_date    = {{Birth date|1408|02|14|df=yes}}
|birth_place   = [[Lytchett Matravers]], [[Dorset]]
|death_date    = {{Death date and age|1435|06|12|1408|02|14|df=yes}}
|death_place   = [[Beauvais]], [[France]]
|death_cause   = Wounded in battle
|resting_place = Arundel Castle, [[Sussex]]
|resting_place_coordinates = {{coord|50|51|22|N|0|33|13|W|type:landmark|display=inline}}
|title         = 14th Earl of Arundel
|tenure        = 1433&ndash;35
|other_titles  = [[Baron Maltravers]]<br>Duke of [[Touraine]]
|residence     = Arundel Castle
|nationality   = [[England|English]]
|locality      = 
|other_names   = 
|years_active  = c. 1430&ndash;35
|wars_and_battles = [[Hundred Years' War]]<br>• [[Siege of Compiègne]]<br>• [[Battle of Gerbevoy]]
|offices       = 
|networth      = 
|known_for     = Military service in the Hundred Years' War
|predecessor   = [[John FitzAlan, 13th Earl of Arundel]]
|heir          = 
|successor     = [[Humphrey FitzAlan, 15th Earl of Arundel]]
|spouse        = Constance Fanhope, Maud Lovell
|issue         = Humphrey FitzAlan, 8th Earl of Arundel
|parents       = John FitzAlan, 13th Earl of Arundel, Eleanor Berkeley
}}
[[File:Quartered arms of Sir John FitzAlan, 14th Earl of Arundel, KG.png|thumb|243px|Arms of Sir John FitzAlan, 14th Earl of Arundel, KG - 1 and 4, gules a lion rampant or (FitzAlan); 2 and 3, sable, a fret or (Maltravers)]]

'''John FitzAlan, 14th Earl of Arundel, 4th Baron Maltravers''' <small>[[Order of the Garter|KG]]</small> (14 February 1408 &ndash; 12 June 1435) was an [[Kingdom of England|English]] nobleman and military commander during the later phases of the [[Hundred Years' War]]. His father, [[John FitzAlan, 13th Earl of Arundel|John FitzAlan, 3rd Baron Maltravers]], fought a long battle to lay claim to the Arundel earldom, a battle that was not finally resolved until after the father's death, when John FitzAlan the son was finally confirmed in the title in 1433.

Already before this, in 1430, FitzAlan had departed for France, where he held a series of important command positions. He served under [[John of Lancaster, 1st Duke of Bedford|John, Duke of Bedford]], the uncle of the eight-year-old King [[Henry VI of England|Henry VI]]. FitzAlan was involved in recovering fortresses in the [[Île-de-France]] region, and in suppressing local rebellions. His military career ended, however, at the [[Battle of Gerbevoy]] in 1435. Refusing to retreat in the face of superior forces, Arundel was shot in the foot and captured. His leg was later amputated, and he died shortly afterwards from the injury. His final resting place was a matter of dispute until the mid-nineteenth century, when his tomb at [[Arundel Castle]] was revealed to contain a skeleton missing one leg.

Arundel was considered a great soldier by his contemporaries. He had been a successful commander in France, in a period of decline for the English, and his death was a great loss to his country. He was succeeded by his son [[Humphrey FitzAlan, 15th Earl of Arundel|Humphrey]], who did not live to adulthood. The title of Earl of Arundel then went to John's younger brother [[William FitzAlan, 9th Earl of Arundel|William]].

==Family background==
{{Further2|[[John FitzAlan, 14th Earl of Arundel#FitzAlan family tree|FitzAlan family tree]]}}
John FitzAlan was born at [[Lytchett Matravers]] in [[Dorset]] on 14 February 1408.<ref name=Cokayne>Cokayne (1910&ndash;59), pp. 247&ndash;8.</ref> He was the son of [[John FitzAlan, 13th Earl of Arundel|John FitzAlan, 3rd Baron Maltravers]] (1385&ndash;1421) and Eleanor (d. 1455), daughter of Sir John Berkeley of [[Beverstone]], [[Gloucestershire]].<ref name=DNB>Curry (2004)</ref> John FitzAlan the elder, through his great-great-grandfather [[Richard FitzAlan, 4th Earl of Arundel]], made a claim on the [[Earl of Arundel|earldom of Arundel]] after the death of [[Thomas FitzAlan, 5th Earl of Arundel]], in 1415. The claim was disputed, however, by Thomas's three sisters and their families, foremost among these [[Elizabeth Fitzalan, Duchess of Norfolk|Elizabeth FitzAlan]], who had married [[Thomas de Mowbray, 1st Duke of Norfolk]].<ref name=DNB/> It is debatable whether Maltravers ever held the title of Earl of Arundel; he was summoned to parliament under this title once, in 1416, but never again.<ref name=HBC>Fryde (1961), p. 415.</ref> When he died in 1421, the dispute continued under his son, and it was not until in 1433 that the younger John FitzAlan finally had his title confirmed in parliament.<ref name=HBC/> Four years earlier, in July 1429, he had received his late father's estates and title.<ref name=Collins128/>

As a child, John FitzAlan was contracted to marry Constance, who was the daughter of [[John Cornwall, 1st Baron Fanhope|John Cornwall, Baron Fanhope]], and through her mother [[Elizabeth of Lancaster, Duchess of Exeter|Elizabeth]] granddaughter of [[John of Gaunt, 1st Duke of Lancaster|John of Gaunt]]. The two may or may not have married, but Constance was dead by 1429, when John married Maud, daughter of Robert Lovell.<ref name=Cokayne/> FitzAlan was knighted in 1426 along with the four-year-old King [[Henry VI of England|Henry VI]], where he was referred to as "Dominus de Maultravers" ("[[Baron Maltravers|Lord Maltravers]]"). In the summer of 1429 he was summoned to parliament, this time styled "Johanni Arundell' Chivaler", meaning he was now [[Baron Arundel|Lord Arundel]].<ref name=Cokayne/> In 1430, however, in an [[indenture]] for service with the king in France, he was styled Earl of Arundel, a title he also used himself.<ref name=DNB/> When he was finally officially recognised in his title of Earl of Arundel in 1433, this was based on the recognition that the title went with the possession of [[Arundel Castle]].<ref>Cokayne (1910&ndash;59), p. 231n.</ref> In reality though, the grant was just as much a reward for the military services he had by that point rendered in France.<ref name=Collins128/>

==Service in France==
[[File:Arundel Castle on a Sunny October Day.jpg|thumb|left|[[Arundel Castle]] was the main residence of the FitzAlan family, but John FitzAlan spent most of the years from 1430 until his death in France.]]
John FitzAlan the father had been a prominent soldier in the [[Hundred Years' War]] under King [[Henry V of England|Henry V]], and the son followed in his father's footsteps.<ref name=Collins128>Collins (2000), p. 128.</ref> On 23 April 1430, the younger FitzAlan departed for France in the company of the Earl of Huntingdon.  There he soon made a name for himself as a soldier, under the command of the king's uncle, [[John of Lancaster, 1st Duke of Bedford|John, Duke of Bedford]].<ref name=DNB/> In June he took part in the [[Siege of Compiègne]], where [[Joan of Arc]] had recently been captured.<ref name=Collins128/> Later, he raised the siege of [[Anglure]] with the help of the [[Duchy of Burgundy|Burgundians]].<ref name=DNB/> On 17 December 1431, he was present when Henry VI was crowned King of France in Paris, where he distinguished himself at the accompanying [[Tournament (medieval)|tournament]].<ref name=Cokayne/> FitzAlan's military success led to several important appointments of command; in November 1431, he was made lieutenant of the [[Rouen]] garrison, and shortly after also captain of [[Vernon, Eure|Vernon]].  In January 1432 he was appointed captain of [[Verneuil, Marne|Verneuil]].  On the night of 3 February he was taken by surprise while in bed at the Great Tower of [[Rouen Castle]], when a band of French soldiers from nearby [[Ricarville]] managed to take the castle.  Arundel was hoisted down the walls in a basket; and made his escape.  The assailants could not hold the castle, because Marshall Boussac refused to garrison the town; Guillaume de Ricarville was forced to surrender twelve days later.<ref name=DNB/> In April 1432, FitzAlan was rewarded for his actions so far by initiation into the [[Order of the Garter]].<ref name=Collins128/>
In a separate action from Rouen Arundel was sent to rescue Saint Lo. from an attack by the duke of Alençon's army, after the town's captain Raoul Tesson had been appointed to replace Suffolk, who was captured at the [[Battle of Jargeau]].  The French retreated to the fastness of Mont St Michel, from where they continued to raid Anglo-Normandy towns, like Granville in 1433.

From early 1432 onwards, FitzAlan held several regional commands in northern France.  One of his tasks was recovering fortresses in the [[Île-de-France]] region, at which he was mostly successful.  At [[Lagny-sur-Marne]] he blew up the bridge to prevent the citizens from reaching the castle, but still failed to take the fortification.<ref name=DNB/> In December he was appointed to a regional command in [[Upper Normandy]], but had to defend the town of [[Sées]] from a siege.  On 10 March 1433, he issued a pardon to the inhabitants when the town was retaken from the Armangnacs.

In July Arundel was instead made [[lieutenant-general]] of [[Lower Normandy]].  The earl continued his work of recovering lost fortresses that belonged to Ambrose de Loré, [[Bonsmoulins]] was taken easily, but de Loré's family had occupied [[Saint-Cenery]].  After three months of culverin bombardment the walls were breached and most of the garrison killed.  The remainder were allowed to march out unharmed.

In the County of Alençon, a young, tall and courageous earl led the campaign that probably took place in 1433,<ref>J H Ramsay, pp.462-3; Barker, p.201</ref> taking back [[Saint-Célerin]], [[Sillé-le-Guillaume]], where there was a short skirmish.  The Armagnacs arrived, and demanded the return of the hostages in the castle; Arundel feigned to agree and departed.  As soon as the Armangnacs rode off Arundel returned and took the castle by assault.<ref>J Chartier, Vallet de Viriville (ed.), Chronique de Charles VII, Roi de France, Pain 1858, pp.165-8; Barker, p.200</ref> and by 1434 [[Beaumont-le-Vicomte]].  In December 1433, Bedford again appointed him commander in Upper Normandy, as well as captain of [[Pont-de-l'Arche]].<ref name=DNB/>

By now the Earl of Arundel might have returned briefly to England in May 1434, when he was in the process of gathering troops for an English expedition to France.  But John, Duke of Bedford was forced to raise loans for the soldiers' pay in the Maine campaign.<ref>Beaurepaire, Les Etats (1859)</ref>  That Spring he was joined in Paris by Talbot with nearly 1000 reinforcements from England.  Later in May he was replaced as lieutenant of Upper Normandy by [[John Talbot, 1st Earl of Shrewsbury|John Talbot, Earl of Shrewsbury]], and instead received a command between the [[Seine]] and [[Loire]] rivers. This effectively meant that the two shared the command of Normandy, with Talbot east of the Seine and Arundel to the west.<ref>Pollard (1983), p. 19.</ref>  But they combined operations capturing Beaumont-sur-Oise, and then Creil, which was finally taken on 20 June 1434.<ref>Barker, p.206</ref>  In the summer Arundel captured the Mantes-Chartres regional fortresses; it appeared for a time at least the Armagnacs no longer posed a threat to Paris.

On 8 September, Arundel was also made Duke of [[Touraine]] &ndash; an area held by the French.  The grant was made as a reward for his good service, but also in the hope that he would campaign in the area.<ref>Pollard (1983), p. 52n.</ref> In October he was made captain of [[Saint-Lô]], where he had to deal with a rebellion in the [[Bessin]] area.  The [[Counts and Dukes of Alençon|Duke of Alençon]] was trying to exploit the revolt to take control of [[Avranches]], but Arundel managed to prevent the French advance and ended the rebellion.<ref name=DNB/>

But in early 1435 the area of western Normandy erupted in popular revolt.  Arundel wads summoned to call muster from Rouen in order to protect Caen.  Arundel was joined by another lieutenant-general Lord Scales from his base at Domfront, commanded to relieve Avranches.  Alençon intended to build a fortress at Savigny, but when the English found out, the bailli of Cotentin was required to demolish it.  With 800 men, Arundel was sent to recover Rue was he learnt that La Hire was fortifying Gerberoy, only 37 miles east of Rouen.  Talbot had previously cleared out the Picardy, but when Arundel arrived he discovered to his surprise La Hire and Poton de Xantrailles had already occupied the fortress.  He was forced to give battle or besiege.<ref>Barker, pp.216-17</ref>

==Death and aftermath==
[[File:Arundel4.JPG|thumb|John FitzAlan's free-standing [[cadaver tomb]] was opened in 1857, to reveal a skeleton missing one leg.]]
On the night of 31 May/1 June 1435, Arundel was at [[Mantes-la-Jolie]] in the Île-de-France, when he was ordered to relocate north to Gournay-sur-Epte (now [[Gournay-en-Bray]]). When he was informed that the French had taken over the nearby fortress at [[Gerberoy]], he moved quickly to attack it. The English met with a large French force at Gerberoy.  Many withdrew to Gournay in panic, but Arundel remained to fight.<ref name=DNB/> In the ensuing [[Battle of Gerbevoy|battle]], Arundel lost many of his men and was himself hit in the foot by a shot from a [[culverin]] &ndash; a primitive [[musket]].<ref name=Aberth237>Aberth (2000), p. 237.</ref> Heavily wounded, he was taken to [[Beauvais]] as a captive of the French. According to the French historian [[Thomas Basin]], Arundel was humiliated by his defeat and refused to receive medical treatment for the damage to his foot.  The leg was eventually amputated, but Arundel's life could not be saved; he died of his injuries on 12 June 1435,<ref>Douet-D'Arcy, vol.5, pp.79-86; Barker, p.217-8</ref> depriving the English of one of their youngest, most able and dedicated military leaders.<ref>Barker, p.218</ref> Arundel was replaced in his command by [[Thomas de Scales|Lord Scales]].<ref>Pollard (1983), p. 36.</ref>

There was long uncertainty about what had happened to the earl's body.  The French chronicler [[Jehan de Waurin]] claimed that Arundel had simply been buried in Beauvais.  In the mid-nineteenth century, however, the chaplain of the [[Duke of Norfolk]] came upon the will of Arundel's squire, [[Fulk Eyton]], when he died later in 1454.  Eyton maintained therein that he had secured the earl's body and brought it back to England, for which he had been rewarded with a payment of 1400 Marks.  The body was disinterred brought back to England, then entombed in the [[Fitzalan Chapel]] of [[Arundel Castle]], as Arundel had expressly wished for in his own will.  On 16 November 1857, the tomb in the Arundel chapel carrying the earl's effigy was opened.  In it was found a skeleton measuring over six feet, with a missing leg.<ref name=Aberth237/><ref>Tierney, Discovery of the Remains, pp.237-9</ref>

Arundel's military career coincided with a period of general decline for the English in France.<ref>Curry (2003), p. 3.</ref> He had been an unusually successful campaigner.  His death was lamented in England and celebrated in France.<ref name=DNB/> He was referred to as the "English Achilles"; the historian [[Polydore Vergil]] called him "a man of singular valour, constancy, and gravity."<ref name=Collins128/> With his wife, Maud, he had a son, [[Humphrey FitzAlan, 15th Earl of Arundel|Humphrey]], who was born on 30 January 1429. Humphrey succeeded to his father's title, but died on 24 April 1438, while still a minor. John FitzAlan's younger brother, [[William FitzAlan, 9th Earl of Arundel|William]], was next in line of succession. William was born in 1417 and was created Earl of Arundel in 1438 when he came of age.<ref name=HBC/>

==FitzAlan family tree==
The following simplified family tree shows the background for the dispute over the Arundel title and inheritance. Solid lines denote children and dashed lines marriages.<ref name=HBC/><ref>Cokayne (1910&ndash;59), pp. 242&ndash;8, 253.</ref>

{{familytree/start}}
{{familytree ||||||||||||Ri10|||Ri10=[[Richard FitzAlan, 10th Earl of Arundel]]<br />(c. 1313–1376)}}
{{familytree ||||||||,|-|-|-|-|^|-|-|-|.||}}
{{familytree |||||||Ri11||||||||J1A|||Ri11=[[Richard FitzAlan, 11th Earl of Arundel]]<br />(1346–1397)||J1A=[[John FitzAlan, 1st Baron Arundel]]<br />(c. 1348–1379)}}
{{familytree |||,|-|-|v|-|^|-|v|-|-|-|.|||!|}}
{{familytree |El| |Jo| |Ma| |T12||J2A|El=[[Elizabeth Fitzalan, Duchess of Norfolk|Elizabeth FitzAlan]]<br />(1366–1425)|Jo=[[Joan de Beauchamp, Baroness Bergavenny]]<br />(c.1375–1435)|Ma=Margaret FitzAlan<br />(b. after 1375)|T12=[[Thomas FitzAlan, 12th Earl of Arundel]]<br />(1381-1415)|J2A=[[John FitzAlan, 2nd Baron Arundel]]<br />(1364-1390)}}
{{familytree |||:|||:||||:|||||||!|}}
{{familytree |Tm| |WB| |RL| |||||J3M||Tm=[[Thomas de Mowbray, 1st Duke of Norfolk]]<br />(1366-1399)|WB=[[William de Beauchamp, 1st Baron Bergavenny]]<br />(c. 1343-1411)|RL=Rowland Lenthall|J3M=[[John FitzAlan, 13th Earl of Arundel|John FitzAlan, 3rd Baron Arundel, 13th Earl of Arundel]]<br />(1385-1421)}}
{{familytree |||||||||||||||||!|}}
{{familytree |||||||||||||||||J14||J14=John FitzAlan, 14th Earl of Arundel<br />(1408-1435)}}
{{familytree/end}}

==References==
{{reflist|3}}

==Sources==
*{{cite book|last=Aberth|first=John|title=From the Brink of the Apocalypse: Confronting Famine, War, Plague, and Death in the Later Middle Ages|edition=|publisher=Routledge|location=New York, London|year=2000|url=https://books.google.com/books?id=4xyp-SscNBkC&pg=PA237|isbn=0-415-92715-3}}
*{{cite book|last=Cokayne|first=George|authorlink=George Cokayne|title=[[The Complete Peerage|The Complete Peerage of England, Scotland, Ireland, Great Britain and the United Kingdom]]|edition=New|publisher=The St. Catherine Press|location=London|year=1910–59|volume=i|pages=}}
*{{cite book|last=Collins|first=Hugh E. L.|title=The Order of the Garter, 1348-1461: Chivalry and Politics in Late Medieval England|edition=|publisher=Clarendon|location=Oxford|year=2000|url=https://books.google.com/books?id=sKapp53K4_MC&pg=PA128|isbn=0-19-820817-0}}
*{{cite encyclopedia|last=Curry|first=Anne|authorlink=Anne Curry|title=Fitzalan, John (VI), seventh earl of Arundel (1408–1435)|encyclopedia=Oxford Dictionary of National Biography|publisher=Oxford University Press|location=Oxford|year=2004|doi=10.1093/ref:odnb/9532}}<!--|accessdate=2009-05-11-->
*{{cite book|last=Curry|first=Anne|title=The Hundred Years War|edition=|publisher=Macmillan|location=Basingstoke|year=2003|url=https://books.google.com/books?id=Hssfq3WZM6wC&pg=PA3|isbn=0-333-53176-0}}
*{{cite book|last=Fryde|first=E. B.|title=Handbook of British Chronology|edition=Second|publisher=Royal Historical Society|location=London|year=1961|page=415|isbn=}}
*{{cite book|last=Pollard|first=A. J.|authorlink=A. J. Pollard|title=John Talbot and the War in France, 1427-1453|edition=|publisher=Royal Historical Society|location=London|year=1983|url=|isbn=0-901050-88-1}}

=== Secondary ===
* J Barker, Conquest: The English Kingdom of France 1417-1450, Little, Brown: London, 2009
* C de Beaurepaire, Les Etats d e Normandie sous la Domination Anglaise 1422-1435, Évreux 1859
* L Douet-D'Arcq, (ed.), La Chronique d'Enguerran de Montsrelet, Paris, 1859–62, vols 3-6.
* A J R Pollard, John Talbot and the War in France, 1427-1453, London and New Jersey, 1983
* Thomas Rymer, ed., Foedera, Conventiones, Litterae, London, 1726–35, 10 vols.
* Very Rev Canon Tierney, Discoery of the Remains of John, 7th Earl of Arundel, Sussex Archaeological Collections, vol.12, (1860), pp.&nbsp;232–9
* J H Wylie, The History of England under Henry the Fifth (1896)

{{s-start}}
{{s-reg|en}}
{{succession box
 | before = [[John FitzAlan, 13th Earl of Arundel|John FitzAlan]]
 | title  = [[Earl of Arundel]]
 | after  = [[Humphrey FitzAlan, 8th Earl of Arundel|Humphrey FitzAlan]]
 | years  = 1433&ndash;1435 }}
{{s-end}}

{{good article}}

{{DEFAULTSORT:Arundel, John Fitzalan, 14th Earl Of}}
[[Category:1408 births]]
[[Category:1435 deaths]]
[[Category:Knights of the Garter]]
[[Category:Earls of Arundel|*14]]
[[Category:Barons Maltravers|*04]]
[[Category:Dukes of Touraine]]
[[Category:Knights of the Bath]]
[[Category:People of the Hundred Years' War]]
[[Category:FitzAlan family]]