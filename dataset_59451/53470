{{Underlinked|date=May 2015}}
'''Database auditing''' involves observing a [[database]] so as to be aware of the actions of database [[user (computing)|user]]s.  Database administrators and consultants often set up auditing for security purposes, for example, to ensure that those without the permission to access information do not access it.<ref>
{{cite book
| last       = Mullins
| first      = Craig
| title      = Database administration: the complete guide to practices and procedures
| url        = https://books.google.com/books?id=KVHUbiSMYjAC
| accessdate = January 19, 2011 
| year       = 2002
| publisher  = Addison-Wesley
| isbn       = 978-0-201-74129-2
| page       = 402
| quote      = Audit trails help promote data integrity by enabling the detection of ''security'' breaches [...].
}}
</ref>

==References==
{{Reflist}}

==Further reading==
*Gallegos, F.  C. Gonzales, D. Manson, and S. Senft.  Information Technology
*Control and Audit.  Second Edition.  Boca Raton, FL:  CRC Press LLC, 2000.
*Ron Ben-Natan, IBM Gold Consultant and Guardium CTO. Implementing Database Security and Auditing. Digital Press, 2005.
*<cite id=Mookhey>KK Mookhey (2005). IT Audit.  Vol. 8.  Auditing MS SQL Server Security.</cite>
*<cite id=Mazer>IT Audit.  Vol. 8  Murray Mazer.  Database Auditing-Essential Business Practice for Today’s Risk Management May 19, 2005.</cite>
*[http://e.bis.business.utah.edu/ebis_class/2003s/Oracle/database.804/a58397/ch22.html Oracle.  1999.  Version 8.0.  Auditing Database Use.  May 9, 2005.]{{full citation needed|date=January 2011}}

{{Database}}

{{DEFAULTSORT:Database Audit}}
[[Category:Database security|Audit]]
[[Category:Types of auditing]]
[[Category:Computer access control]]


{{compu-stub}}