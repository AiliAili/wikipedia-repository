{{multiple issues|
{{notability|date=July 2011}}
{{refimprove|date=July 2011}}
}}

{{Italic title}}
The '''''Hardy–Ramanujan Journal''''' is a [[mathematics journal]] covering [[prime number]]s, [[Diophantine equation]]s, and [[transcendental number]]s. It is named for [[G. H. Hardy]] and [[Srinivasa Ramanujan]]. Together with the ''Ramanujan Journal'' and the ''Journal of the Ramanujan Mathematical Society'', it is one of three journals named after Ramanujan.<ref>[http://www.imsc.res.in/~rao/ramanujan/newnow/RMSJournal.htm Life and Work], [[Institute of Mathematical Sciences, Chennai|Institute of Mathematical Sciences]], accessed 2011-07-24.</ref><ref>{{citation|title=Srinivasa Ramanujan: a mathematical genius|first=K. Srinivasa|last=Rao|publisher=East West Books|year=1998|page=173|isbn=978-81-86852-14-9}}.</ref><ref>{{citation|title=Theta constants, Riemann surfaces, and the modular group: an introduction with applications to uniformization theorems, partition identities, and combinatorial number theory|volume=37|series=[[Graduate Studies in Mathematics]]|first1=Hershel M.|last1=Farkas|first2=Irwin|last2=Kra|publisher=[[American Mathematical Society]]|year=2001|isbn=978-0-8218-1392-8|page=516|url=https://books.google.com/books?id=sdEzLARwUbQC&pg=PA516}}.</ref>

It was established in 1978 by [[R. Balasubramanian]] and [[Kanakanahalli Ramachandra|K. Ramachandra]] and is published once a year on Ramanujan's birthday December 22. It is indexed in [[MathSciNet]].<ref name=":0">[http://www.ams.org/mathscinet/search/journaldoc.html?cn=HardyRamanujan_J Journal Information for "Hardy-Ramanujan Journal"], [[MathSciNet]], accessed 2011-07-24.</ref>

Both Balasubramanian and Ramachandra are respected mathematicians and accomplished a great deal in the field of mathematics. They both also focused their mathematical careers on number theory. Most importantly, they were both inspired by Srinivasa Ramanujan, which led them to the creation of the Hardy–Ramanujan Journal. Before Ramachandra’s death, the two would publish a new journal almost every year on Ramanujan’s birthday, December 22. However, after that time a decision was made to continue the journal by a new team of editors. These editors consisted of people who shared the same passion as Ramachandra and Balasubramanian and contributed to the journal in the past. The goal of the journal remains the same.<ref>{{Cite web|title = Hardy-Ramanujan Journal - Home|url = http://hrj.episciences.org/|website = hrj.episciences.org|accessdate = 2015-12-11}}</ref>

The last few volumes of the journal have provided new information on a wide array of topics, with an additional focus on one of the founders of the Hardy-Ramanujan journal, Ramachandra, who died in 2011.  Most notably, an autobiography of Ramachandra is included in Volume 36.  There are also obituaries from students and colleagues in Volumes 34 and 35, which are entitled "K. Ramachandra: Reminiscences of his Students".<ref name=":0" />

The very last volume, the 37th issued, returned the focus of the journal to the discussion of developing math topics.  One of the featured articles, "New developments on the twin prime problem and generalizations" discusses some tremendous progress made in the specified problem.  It was submitted by Murty, M. Ram, who frequently writes for the Hardy–Ramanujan Journal.<ref name=":0" />

==References==
{{reflist}}

== External links ==
* [http://www.imsc.res.in/~balu/Hrj/book/ Page scans of the Hardy–Ramanujan Journal]

{{DEFAULTSORT:Hardy-Ramanujan Journal}}
[[Category:Mathematics journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1978]]
[[Category:Annual journals]]
[[Category:Srinivasa Ramanujan]]