{{Refimprove|date=June 2007}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
| name     = Kilburn
| city     = Adelaide
| state    = sa
| image    = Kilburn.jpg
| caption  = Grand Junction Road, Kilburn
| lga      = City of Port Adelaide Enfield
| postcode = 5084
| est      = 
| pop      = 
| area     = 
| stategov = [[Electoral district of Enfield|Enfield]]<ref name="PLB"/>
| fedgov   = [[Division of Adelaide|Adelaide]]
| dist1    = 
| location1= 
| near-nw  = [[Wingfield, South Australia|Wingfield]]
| near-n   = [[Dry Creek, South Australia|Dry Creek]], [[Gepps Cross, South Australia|Gepps Cross]]
| near-ne  = [[Gepps Cross, South Australia|Gepps Cross]]
| near-e   = [[Enfield, South Australia|Enfield]]
| near-se  = [[Sefton Park, South Australia|Sefton Park]]
| near-s   = [[Prospect, South Australia|Prospect]]
| near-sw  = [[Dudley Park, South Australia|Dudley Park]]
| near-w   = [[Regency Park, South Australia|Regency Park]]
}}
'''Kilburn''' is a suburb in the inner north of [[Adelaide]], [[South Australia]]. The suburb borders [[Blair Athol, South Australia|Blair Athol]], [[Gepps Cross, South Australia|Gepps Cross]], [[Wingfield, South Australia|Wingfield]], [[Regency Park, South Australia|Regency Park]] and [[Prospect, South Australia|Prospect]].<ref>{{cite book|title=2003 Adelaide Street Directory, 41st Edition |publisher=UBD (A Division of Universal Press Pty Ltd) |year=2003 |isbn=0-7319-1441-4}}</ref> Kilburn shares the same postcode as Blair Athol which is 5084 and was previously known as 'Little Chicago' before its name was changed during the 20th century.<ref name="PLB">{{Cite web |url=http://maps.sa.gov.au/plb |title=Property Location Browser: Search for 'Kilburn, SUB' (ID SA0036544) |publisher=Government of South Australia |accessdate=5 April 2016}}</ref>

==Education==
Kilburn has one primary school. ''St Brigids Primary School'' is a [[private school]] located on Le Hunte St. It caters for year levels Reception to Year 7. Also, ''St. Gabriel's Primary School'', [[Enfield, South Australia|Enfield]] is also located near Kilburn. It is situated on Whittington Tce, is co-educational and also caters for Reception to Year 7.

==Transport==
Kilburn is only a 10- to 15-minute drive from the [[Central Business District|CBD]] (Central Business District). The suburb is well-serviced by public transport. The G10 and G11 [[bus]]lines pass through Kilburn and [[Blair Athol, South Australia|Blair Athol]] along [[Prospect Road, Adelaide|Prospect Road]]. [[Kilburn railway station, Adelaide|Kilburn train station]], located on Railway Terrace, has services that go to and from the [[Adelaide railway station|city]]. Buses 235, 237, 238 and 239 traverse Kilburn along [[Churchill Road]] and terminate at Kilburn, [[Valley View, South Australia|Valley View]], [[Mawson Lakes, South Australia|Mawson Lakes]] [[University of South Australia|UniSA]] and [[Arndale Central Shopping Centre|Arndale Shopping Centre]] respectively.

==Heritage listings==

Kilburn has a number of heritage-listed sites, including:

* Churchill Road: [[Islington Railway Workshops Chief Mechanical Engineer's Office]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=2955 | title=Islington Railway Workshops Chief Mechanical Engineer's Office | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* Churchill Road: [[Islington Railway Workshops Fabrication Shop]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=2954 | title=Islington Railway Workshops Fabrication Shop | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* Churchill Road: [[Islington Railway Workshops Foundry]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=2952 | title=Islington Railway Workshops Foundry | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* Churchill Road: [[Islington Railway Workshops Apprentice School]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=2951 | title=Islington Railway Workshops Apprentice School | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* Churchill Road: [[Islington Railway Workshops Electrical Shop]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=2950 | title=Islington Railway Workshops Electrical Shop | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* Churchill Road: [[Islington Railway Workshops Fabrication Shop Annex]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=2953 | title=Islington Railway Workshops Fabrication Shop Annex | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* 498 Churchill Road: [[Tubemakers Administration Building No. 2]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=26324 | title=Former Tubemakers Administration Building No 2 | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* 500 Churchill Road: [[Tubemakers Administration Building No. 1]]  <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=24963 | title=Former Tubemakers Administration Building No 1 | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>

==See also==
* [[Churchill Road]]
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*[http://www.portenf.sa.gov.au/ City of Port Adelaide Enfield]

{{Coord|-34.861|138.597|format=dms|type:city_region:AU-SA|display=title}}
{{City of Port Adelaide Enfield suburbs}}

[[Category:Suburbs of Adelaide]]