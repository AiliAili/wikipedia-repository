{{Infobox military person
|name=Werner Husemann
|birth_date={{birth date|1919|11|10|df=y}}
|death_date={{death date and age|2014|2|2|1919|11|10|df=yes}}
|image=File:Werner Husemann.jpg
|birth_place=[[Bad Salzuflen|Schötmar]]/[[Lippe]]
|death_place=[[Bad Salzuflen]]
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1941–1945
|rank=''[[Major (Germany)|Major]]''
|unit=[[NJG 1]], [[NJG 3]]
|commands=7./NJG 1<br/>I./NJG 3
|battles=[[World War II]]
*[[Defense of the Reich]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Werner Husemann''' (10 November 1919 – 2 February 2014) was a German [[Luftwaffe]] [[night fighter]] [[fighter ace|ace]] and recipient of the [[Knight's Cross of the Iron Cross]] during [[World War II]]. The Knight's Cross of the Iron Cross was awarded to recognise extreme battlefield bravery or successful military leadership. Husemann claimed 34 aerial victories—that is, 34 aerial combat encounters resulting in the destruction of the enemy aircraft—during World War II.<ref group="Note">For a list of Luftwaffe night fighter aces see ''[[List of German World War II night fighter aces]]''.</ref>

==Career==
Husemann was born on 10 November 1919 in [[Bad Salzuflen|Schötmar]]/[[Lippe]]. He enlisted in the Luftwaffe in 1941 and served with a weather reconnaissance squadron. In late 1942 he transferred to the ''[[Stab (Luftwaffe designation)|Stab]]'' (staff) of [[Nachtjagdgeschwader 1]] (the 1st Night Fighter Wing). He claimed his first aerial victory on the night of 17 to 18 August 1942. His victories had increased to 17 by the end of 1943, among them three British [[Avro Lancaster]] bombers shot down on the night of 25/26 June 1943. He was appointed ''[[Staffelkapitän]]'' (squadron commander) of the 7th squadron of NJG 1 on 1 October 1943. Husemann was awarded the [[German Cross]] in Gold on 24 October 1943, and the ''[[Ehrenpokal der Luftwaffe]]'' on 1 November 1943. The following January, he became commander of the I. Gruppe of [[Nachtjagdgeschwader 3]] (NJG 3—3rd Night Fighter Wing). Husemann was awarded the [[Knight's Cross of the Iron Cross]] ({{lang|de|''Ritterkreuz des Eisernen Kreuzes''}})) on 30 September 1944 after he had been credited with 30 aerial victories. By the war's end he had scored 34 victories in over 250 night combat missions. His last 13 victories were claimed with ''[[Oberfeldwebel]]'' [[Hans-Georg Schierholz]] as his wireless/radio operator.{{sfn|Obermaier|1989|p=138}}

==Aerial victory claims==
Husemann was credited with 30 nocturnal aerial victories claimed in over 250 combat missions.{{sfn|Obermaier|1989|p=138}}

<center>
{| class="wikitable plainrowheaders" style="text-align:right;"
|-
! colspan="6" | Chronicle of aerial victories
|-
!scope="col"| Victory
!scope="col" style="width:130px"| Date
!scope="col"| Time
!scope="col"| Type
!scope="col"| Location
!scope="col"| Serial No./Squadron No.
|-
! colspan="6" | – ''Nachtjagdgeschwader'' 1 –
|-
| 1
| 17 December 1941
| 20:53
| [[Short Stirling|Stirling]]{{sfn|Foreman|Matthews|Parry|2004|p=62}}
| {{Convert|6|km|mi|abbr=on}} northeast of [[Zwolle]]
|
|-
| 2
| 27 April 1943
| 03:37
| Stirling{{sfn|Foreman|Matthews|Parry|2004|p=77}}
| {{Convert|17|km|mi|abbr=on}} west of [[Hilversum]]
|
|-
| 3
| 12 June 1943
| 02:55
| [[Vickers Wellington|Wellington]]{{sfn|Foreman|Matthews|Parry|2004|p=84}}
|
|
|-
| 4
| 13 June 1943
| 02:20
| [[Avro Lancaster|Lancaster]]{{sfn|Foreman|Matthews|Parry|2004|p=85}}
| [[Tubbergen]]
|
|-
| 10
| 24 August 1943
| 00:50
| [[Handley Page Halifax|Halifax]]{{sfn|Foreman|Matthews|Parry|2004|p=105}}
| [[Biesenthal]]
|
|}
</center>

==Awards==
* [[Iron Cross]] (1939) 2nd and 1st Class
* [[Honour Goblet of the Luftwaffe]] (''Ehrenpokal der Luftwaffe'') on 1 November 1943 as ''[[Oberleutnant]]'' and pilot{{sfn|Patzwall|2008|p=107}}
* [[German Cross]] in Gold on 24 October 1943 as ''Oberleutnant'' in the 7./Nachtjagdgeschwader 1{{sfn|Patzwall|Scherzer|2001|p=203}}
* [[Knight's Cross of the Iron Cross]] on 30 September 1944 as ''[[Major (Germany)|Major]]'' and ''[[Gruppenkommandeur]]'' of the I./Nachtjagdgeschwader 3{{sfn|Fellgiebel|2000|p=238}}{{sfn|Scherzer|2007|p=412}}

==Notes==
{{reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
  |ref=harv
}}
* {{Cite book
  |last1=Foreman
  |first1=John
  |last2=Matthews
  |first2=Johannes
  |last3=Parry
  |first3=Simon
  |year=2004
  |title=Luftwaffe Night Fighter Claims 1939–1945
  |trans_title=
  |language=English
  |location=Walton on Thames
  |publisher=Red Kite
  |isbn=978-0-9538061-4-0
  |ref=harv
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
  |ref=harv
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
  |ref=harv
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
{{Refend}}

==External links==
*[http://www.ww2awards.com/person/29841 World War 2 Awards.com]

{{s-start}}
{{s-mil}}
{{succession box|
before=Hauptmann [[Paul Szameitat]]|
after=disbanded|
title=Gruppenkommandeur of I. [[Nachtjagdgeschwader 3]]|
years=4 January 1944 – 8 May 1945
}}
{{s-end}}

{{Knight's Cross recipients of NJG 3}}
{{Top German World War II night fighter aces}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Husemann, Werner}}
[[Category:1919 births]]
[[Category:2014 deaths]]
[[Category:People from Bad Salzuflen]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:People from Lippe]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]