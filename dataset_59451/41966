{{refimprove|date=December 2010}}
[[Image:BC eye.jpg|thumb|right|300px|The Border Collie uses a direct stare at sheep, known as "the eye", to intimidate while herding at a trial.]]
A '''sheepdog trial''' (also '''herding event,''' '''stock dog trial''' or simply '''dog trial''') is a competitive [[List of dog sports|dog sport]] in which [[herding dog]]s move [[sheep]] around a field, fences, gates, or enclosures as directed by their handlers. Such events are particularly associated with [[hill farming]] areas, where sheep range widely on largely [[fence|unfenced]] land. These trials take place in the United Kingdom, Ireland, South Africa, Chile, Canada, the USA, Australia, New Zealand and other farming nations.

Some venues allow only dogs of known herding [[dog breed|breed]]s to compete; others allow any dog that has been trained to work stock.

== History ==
[[Image:FannsieBasson&Don-byVickieClose.jpg|thumb|right|South Africa's Faansie Basson competes at the 2009 Bank of the West Soldier Hollow Classic in [[Midway, Utah]], with his dog, Don.]]

[[Image:Border Collie sheepdog trial.jpg|thumb|right|A Border Collie at the sheepdog trials at Rural Hill Farm in Huntersville, North Carolina]]

The first dog trials were held in [[Wanaka]], New Zealand, in 1867<ref>[http://www.dogfind.co.nz/index_files/Page1595.htm Oamaru Times Report on the first Recorded Trial in the World Tuesday, April 30, 1867]," Dogfind.co.nz. Retrieved 7 October 2012.</ref> with reports of trials at Wanaka, Waitangi and Te Aka in 1868, at Wanaka in 1869 and Haldon Station in the [[Mackenzie Basin|Mackenzie Country]] in 1870. Australia also has a long history of dog trialing, with a kelpie named Brutus reported in the local paper in Young, NSW, as winning a sheepdog trial in 1871.<ref>[http://nla.gov.au/nla.news-article101094244 PASTORAL SHOW AT YOUNG 1872, August 28 The Goulburn Herald and Chronicle NSW : 1864 - 1881, p 2 Retrieved October 2, 2014]</ref>

Janet Larson, in "The Versatile Border Collie," recounts the first sheepdog trials held in the United Kingdom: "The first sheepdog trial was held in [[Bala, Gwynedd|Bala]], [[Wales]], on October 9, 1873.  It was organized by [[Richard John Lloyd Price]], squire of Rhiwlas Estate and friend of [[Sewallis Shirley (MP)|Sewallis Shirley]], MP, founder of [[the Kennel Club]] that same year.  Ten dogs competed and over 300 spectators attended. The winner was Mr. James Thompson with Tweed, a compact, black and tan Scottish bred dog with a foxy face.

The first Scottish sheepdog trial was held at the Carnworth Agricultural Society Show in Lanarkshire around 1874.  It is reported that the winner was James Gardner of Pentland with a black and white bitch named Sly, who worked with 'eye.'  The prize was one pound, which was considerable money in those days."

Trials quickly spread in England and Scotland. From the beginning, shepherds realized that show collies, also becoming popular at the time, quickly lost the keen working instincts honed in working collies. In 1876, a trial was organized in Alexandra Park by the sheepman as a challenge to the show fanciers to demonstrate that show collies could still work.  There were two judges for work and appearance.  The result was a disaster for the show fanciers.  Show collies barked, yelped and lost control of many sheep.  The winner was a common red coated working collie named Maddie, owned by John Thomas, a Welsh shepherd.

The success of those early trials led to events in the United States in the 1880s. Today that tradition continues under the aegis of organizations such as the International Sheepdog Society in Great Britain and the United States Border Collie Handlers Association in the United States.

Today the sport continues to be popular throughout the world.  In the United Kingdom, England, Scotland, Wales and Ireland all host national championships followed by an International Championship featuring the best dogs and handlers from each of the four.  Their sanctioning body, the International Sheepdog Society also hosts a World Championship every three years with dogs participating from throughout the world.

Among the most prestigious trials held annually today in North America are the USBCHA National Championship which is held at various locations throughout North America, The Meeker Classic in Meeker Colorado, and the Bank of the West Soldier Hollow Classic in [[Midway, Utah]].  The Soldier Hollow event, held on Labor Day weekend (a major American holiday) features competitors from around the world and boasts the world's biggest annual crowd with 26,400 attending in 2009.  Through 2009 competitors representing 16 countries and 6 continents have competed at Soldier Hollow (a 2002 Winter Olympic Venue).

==Event types==
[[Image:Yard Trials 7.JPG|thumb|An [[Australian Kelpie]] running over the backs of sheep during a yard dog trial, Walcha, NSW]]

Events vary with different courses being predominant in different parts of the world.  In the United Kingdom, Europe, North America and in Southern Africa (The Republic of South Africa, Botswana, and Zambia), the British Course (shown at right) is most common.

In Australia, there are several events, but the key element is the control of three to six sheep by one or two highly trained dogs under the control of a single handler. Both time and obedience play a part, as competitors are penalized if a sheep strays from the prescribed course.  Another popular event involves having the dog split six sheep into two groups of three and conducting each group in turn to small pens through a defined course by heading dogs. The group not being led is guarded by one of the two dogs, an eye-dog (from its ability to keep the sheep still by head movement alone). This is more difficult than it sounds because the two groups of sheep invariably try to stay together.

Yard Dog Trials are also gaining in popularity. In these competitions dogs are required to move sheep through several yards, including a drafting race and sometimes into and out of a truck, with minimum assistance.

==Trial field==
[[Image:Trialfield.jpeg|thumb|250px|right|A popular version of a trial field]]

The exact layout of the trial field can vary significantly. Most experienced handlers agree that there are certain elements that are important to ensure that the challenge to the dog and handler is a fair and complete test. These elements include:
* The dog must leave the handler and fetch sheep that are some distance away
* The dog must take control of the sheep and bring them to the handler
* It is against the dog's instinct to drive the sheep away from the handler so an ''away drive'' is a good test and should be included
* The dog and handler should be able to combine to move the sheep into a confined space, typically a pen but in some trials they are asked to load them onto a vehicle.
Other popular test elements that are often added include:
* The dog must separate the group into two groups in a controlled way in accordance with the instructions from the judge. This may involve some sheep being marked and the dog and handler working together to separate them from the rest or some variation of that. This is known as ''shedding'' and is almost always required to be done in a ring marked out on the ground.
* ''Singling'' is another test in which the dog and handler combine to separate one sheep from the group.
* Most trials include a ''cross drive'' where the dog is required to move the sheep in a controlled way in a straight line from one side of the field to the other in front of the handler but some distance away from them.

In addition there are various elements that may be added to increase the level of difficulty of a trial. One such example is the ''double lift'' where the dog is required to fetch one group of sheep, bring them to the handler, look back and find another group, somewhere else on the trial field some distance away. They must then leave the first group and do a second outrun to fetch the others and bring them to join the first group.

In most competitions the dog will be required to do the fetching and driving tests on their own. During these test elements the handler must remain at a stake positioned during the layout of the trial course. During the shedding, singling and penning the handler usually leaves the stake and works with the dog to achieve the task.

===Scoring===
The most popular scoring system works as follows:
* A judge watches each run and assigns a score based on their judgment.
* Each test element is assigned a maximum score. For example there may be 10 points for the cast (outrun) and so on.
* Each competitor is assigned the full amount for each element before they start.
* As they negotiate each test element a judge deducts points for each fault. For example during a drive the judge may deduct points when the sheep move off line. During each element they can only lose as many points as are assigned to that element.
* They must negotiate each element in sequence before proceeding to the next.
* A set amount of time for the whole course, usually around 15 minutes, is decided on before the start of the trial.
* There is no advantage in completing the course in a short amount of time but if the competitor runs out of time then they will lose all the points for the element they were in the process of completing and all those that they have yet to attempt.
* The competitor's score is the sum of their score for all completed elements.

For most elements the judge focuses on the behaviour of the sheep not the dog or handler. Dogs are judged on the efficiency of their work and on qualities of good stockmanship.  A dog that needlessly harasses or hurries the sheep will penalized and a dog that bites a sheep may be disqualified.

This points type of system has been in use since at least 1979<ref>[http://www.fairhillscottishgames.org/about/sheepdogs.html Colonial Highland Gathering Program, June 9, 1979]</ref> and may have been formalized at about that same time.

==Popular media==
''[[Owd Bob]]'' was a popular [[children's book]] centered on the rivalry between two sheepdogs at sheepdog trials, and their owners.

In 1984, sheepdog trainer, essayist, poet and novelist [[Donald McCaig]] penned a novel entitled ''Nop's Trials'' which detailed the shadier side of the business and is an excellent primer for the sport's intricacies and culture.

Sheepdogs are interesting enough to watch that they have been featured on television and in film. In New Zealand, ''[[A Dog's Show]]'' was a popular television show until the late 1980s, screening just before the weekend news. In the United Kingdom between 1975 and 1999, the [[BBC]] ran ''[[One Man and His Dog]]'', which had a large urban audience. The movie ''[[Babe (film)|Babe]]'', about a [[pig]] who wants to herd sheep, was based on [[Dick King-Smith]]'s book ''[[The Sheep Pig]]'', about sheepdog trials in northern England.

A sheepdog trial is mentioned many times in [[Terry Pratchett]]'s ''[[Tiffany Aching]]'' series, although it has never been written about in detail.

{{wide image|The crowd at the finals of the 2009 Bank of the West Soldier Hollow Classic.jpg|1044px|The Bank of the West Soldier Hollow Classic held annually in Heber, Utah drew 25,100 spectators in 2010|alt=The Bank of the West Soldier Hollow Classic held annually in Midway, Utah drew 24,600 spectators in 2009}}

==See also==
* [[Herding dog]] – the main article about sheepdogs
* [[Livestock guardian dog]]
* [[List of dog sports]]
* [[Championship (dog)]]
* [[Shepherd's whistle]]
* [[International Sheep Dog Society]]
* [[South African Sheepdog Association]]

==References==
{{Reflist}}

==Further reading==
*{{cite book|first1 = Jeanne Joy | last1 = Hartnagle-Taylor | first2 = Ty | last2 = Taylor |year = 2010|title=Stockdog Savvy|publisher=Alpine Publications|isbn=978-1-57779-106-5|pages=|chapter=}}

==External links==
{{Commons category|Sheepdog trials}}
*{{dmoz|/Recreation/Pets/Dogs/Activities/Herding/|Herding}}

{{Dog sports}}

[[Category:Dog sports]]
[[Category:Sheep]]
[[Category:Herding dogs]]