{{Use dmy dates|date=November 2013}}
{{Infobox military conflict
|conflict=Battle of the Shangani
|partof=the [[First Matabele War]]
|image=[[File:Battle of the Shangani.jpg|300px]]
|caption=The battle, as depicted by [[Richard Caton Woodville, Jr.]] (1856–1927)
|date=25 October 1893
|place=[[Shangani River]], Zimbabwe
|result=British victory
|combatant1=[[British South Africa Police]]
|combatant2=[[Northern Ndebele people#Ndebele Kingdom|Ndebele Kingdom]]
|commander1=[[Patrick William Forbes]]
|commander2=Manonda{{KIA}}; Mjaan
|strength1=700
|strength2=c.5,000
|casualties1=4 (not including African auxiliaries)
|casualties2= c.1,500
}}
The '''Battle of the Shangani''' took place on 25 October 1893, during the [[First Matabele War]] in what is now Zimbabwe. A British column was attacked at night by a large force of Matabele, but repulsed them with heavy loss of life to the Matabele. It is noted for being the first battle in which the [[Maxim gun]] played an important role.

==Context==
The leaders of British Southern Africa [[Cecil Rhodes]] and [[Leander Starr Jameson]] had responded to a raid by the Matabele with force. A British column commanded by Major [[Patrick William Forbes]] was sent into Matabeleland. It advanced towards Bulawayo, the territory's capital. The force was made up of around 700 men of the paramilitary [[British South Africa Police]], along with an unknown number of native auxiliaries. In addition to rifles, the column was equipped with five [[Maxim gun]]s, three other rapid-fire guns, two cannon, and 200 rifles.<ref name = "xx">Robert I. Rotberg & Miles F. Shore, ''The Founder:Cecil Rhodes and the Pursuit of Power'', Oxford University Press, New York, 1988, p.442.</ref>

==Battle==
The Matabele ([[Northern Ndebele people|Ndebele]]) king [[Lobengula]] planned a surprise attack at night. The British set up camp at the  Shangani river, forming into a circular defensive ''laager'' on the model pioneered by the Boers. Lobengula's generals Manonda and Mjaan launched the attack with 5-6,000 warriors. However, the British sentries soon alerted the soldiers. According to trooper Jack Carruthers the attack came at 2:15a.m., "a peaceful night, clear sky but on the dark side. The bugles gave the alarm, the camp was all excitement in a moment, all noise with the opening of ammunition boxes and shouting of officers, the men were getting into their places... the scouts had hardly time to save themselves. The outer sentries also had narrow escapes getting back into laager."<ref name = "carr">[http://archiver.rootsweb.ancestry.com/th/read/RHODESIAN-PIONEERS/2009-06/1244721982 Note books of Jack Carruthers - Victoria Scout]</ref>

Lobengula's troops were a disciplined force by pre-colonial African standards, and were equipped with both [[Assegai|assegais]] and [[Martini-Henry|Martini Henry rifles]], but the British pioneers' Maxim guns, which had never before been used in battle, far exceeded expectations, according to an eyewitness "mow[ing] them down literally like grass".<ref name=ferguson188/> By the time the Matabele withdrew, they had suffered around 1,500 fatalities; the BSAP, on the other hand, had lost only four men.<ref name=ferguson188>Ferguson, Niall (April 2004). ''Empire: the rise and demise of the British world order and the lessons for global power''. New York: Basic Books. p.188.</ref> The devastating effectiveness of the Maxims was such that they cut down wave after wave of advancing Matabele. Hubert Hervey, one of the British troopers, commented that the Matabele were not able to make good use of their own weapons: "the Matabele firing was very inaccurate and poor, and did hardly any damage."<ref name = "xx"/>

The defeated Matabele left the battlefield, while their leader Manonda committed suicide by hanging himself.<ref>Tony Jaques (ed), ''Dictionary of Battles and Sieges: P-Z'', Greenwood Publishing Group, 2007, p.931.</ref> According to Carruthers, he was not alone, "The Matebele retreated at daylight; several had hung themselves to trees with their girdles rather than return beaten. One in desperation, it seemed, had fallen on his own assegai."<ref name = "carr"/>

==Aftermath==
The battle proved the effectiveness of the Maxim machine gun, which was to become central to later colonial battles. Cecil Rhodes, writing to Sir [[Gordon Sprigg]], said that "the shooting must have been excellent. . . . It proves the [white] men were not only brave, but cool, and did not lose their heads, though surrounded with the hordes."<ref name = "xx"/> A week later, on 1 November, 2,000 Matabele riflemen and 4,000 warriors attacked Forbes at [[Bembezi]], about {{convert|30|mi}} north-east of Bulawayo, but again they were no match for the crushing firepower of the major's Maxims: about 2,500 more Matabele were killed.<ref>Knight, Ian (July 1989) ''Queen Victoria's Enemies: Southern Africa''. Oxford: Osprey Publishing, pp.35-6</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Shangani, Battle of the}}
[[Category:African resistance to colonialism]]
[[Category:Conflicts in 1893]]
[[Category:1893 in Matabeleland]]