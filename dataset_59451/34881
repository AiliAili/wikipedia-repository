{{Use mdy dates|date=November 2013}}
{{Good article}}
{{Infobox NFL season
| team = Canton Bulldogs
| logo = [[File:1920 canton bulldogs team.png|250px]]
| caption = Canton Bulldogs team.
| year = 1920
| record = 7–4–2 Overall<br> 6–2 [[NFL|APFA]]
| league_place = 8th
| coach = [[Jim Thorpe]]
| stadium = [[League Field]]<ref>{{cite web | url=http://www.hickoksports.com/history/cantonbulldogs.shtml | title=Canton Bulldogs | publisher=HickokSports.com | date=February 18, 2009 | accessdate=March 31, 2012}}</ref>
| playoffs =
}}

The '''[[1920 APFA season|1920]] [[Canton Bulldogs]] season''' was the franchise's sixteenth and its first in the [[American Professional Football Association]] (APFA), which became the [[National Football League]] two years later. [[Jim Thorpe]], the APFA's president, was Canton's coach and a back who played on the team. The Bulldogs entered the season coming off a 9–0–1 performance as [[Ohio League]] champions in [[1919 Canton Bulldogs season|1919]]. The team opened the season with a 48–0 victory over the [[Pitcairn Quakers]], and finished with a 7–4–2 record, taking eighth place in the 14-team APFA. A then-record crowd of 17,000 fans watched Canton's week 12 game against [[Union Club of Phoenixville|Union AA of Phoenixville]].

The 1920 season was Thorpe's last with the Bulldogs. Thorpe, who was of mixed [[Native Americans in the United States|American Indian]] ancestry, left after the season to organize and play for an all-Native American team in [[LaRue, Ohio]]. [[Cap Edwards]] replaced Thorpe as the team's coach, and [[Wilbur Henry]], [[Cub Buck]], [[Harrie Dadmun]], [[Joe Guyon]], and [[Pete Calac]] were named to the [[All-Pro]] list. Three 1920 Bulldogs players—Thorpe, Guyon and [[Pete Henry]]—were later elected to the [[Pro Football Hall of Fame]].

{{TOC Limit|2}}

== Offseason ==
Representatives of four Ohio League teams—the Bulldogs, the [[Cleveland Tigers (NFL)|Cleveland Tigers]], the [[Dayton Triangles]], and the [[Akron Pros]]—called a meeting on August 20, 1920, to discuss the formation of a new league. At the meeting, they tentatively agreed on a [[salary cap]] and pledged not to sign college players or players already under contract with other teams. They also agreed on a name for the circuit: the American Professional Football Conference.<ref>PFRA Research (1980), pp. 3–4</ref><ref>Siwoff, Zimmber & Marini (2010), pp. 352–353</ref> They then invited other professional teams to a second meeting on September 17. At that meeting, held at Bulldogs owner [[Ralph Hay]]'s [[Hupmobile]] showroom in Canton, representatives of the [[Rock Island Independents]], the [[Muncie Flyers]], the [[Decatur Staleys]], the [[Racine Cardinals]], the [[Massillon Tigers]], the [[Chicago Cardinals (NFL, 1920–59)|Chicago Cardinals]], and the [[Hammond Pros]] agreed to join the league. Representatives of the [[Buffalo All-Americans]] and [[Rochester Jeffersons]] could not attend the meeting, but sent letters to Hay asking to be included in the league.<ref name="nflbday4">PFRA Research (1980), p. 4</ref> Team representatives changed the league's name slightly to the American Professional Football Association and elected officers, installing Jim Thorpe as president.<ref name="nflbday4" /><ref>{{Cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=9503E5DC173AE532A2575AC1A96F9C946195D6CF | title=Thorpe Made President | newspaper=The New York Times| date=September 19, 1920 | format=PDF}}</ref><ref>{{Cite news | url=https://news.google.com/newspapers?id=f8MWAAAAIBAJ&sjid=OiEEAAAAIBAJ&pg=3568,5105560&dq=gridders&hl=en | title=Organize Pro Gridders; Choose Thorpe, Prexy | newspaper=[[The Milwaukee Journal]] | date=September 19, 1920 | page=24}}</ref> Under the new league structure, teams created their schedules dynamically as the season progressed,<ref>Peterson (1997), p. 74</ref><ref>Davis (2005), p. 59</ref> and representatives of each team voted to determine the winner of the APFA trophy.<ref>{{Cite news | last=Price | first=Mark | date=April 25, 2011 | url=http://www.ohio.com/news/searching-for-lost-trophy-1.204246 | title=Searching for Lost Trophy | newspaper=[[Akron Beacon-Journal]] | accessdate=June 23, 2012}}</ref>

== Schedule ==
[[File:Jim Thorpe football.png|thumb|right|Jim Thorpe, player-coach of the Canton Bulldogs]]

{| class="wikitable" style="text-align:center"
|-
! Week !! Date !! Opponent !! Result !! Venue !! Attendance !! Record
|-
! scope="row" | 1
| colspan="6" | ''No game scheduled''
|- style="background:#dfd;"
! scope="row" | 2
| October 3, 1920 || vs. [[Pitcairn Quakers]]{{Dagger}} || 48–0 W || [[Lakeside Park (Canton, Ohio)|Lakeside Park]] || {{NA}} || 1–0
|- style="background:#dfd;"
! scope="row" | 3
| October 10, 1920 || vs. [[Toledo Maroons]]{{Dagger}} || 42–0 W || Lakeside Park || {{NA}} || 2–0
|- style="background:#dfd;"
! scope="row" | 4
| October 17, 1920 || vs. [[Cleveland Tigers (NFL)|Cleveland Tigers]] || 7–0 W || Lakeside Park || 7,000 || 3–0
|- style="background:#fea;"
! scope="row" | 5
| October 24, 1920 || at [[Dayton Triangles]] || 20–20 T || [[Triangle Park (Dayton)|Triangle Park]] || 5,000 || 3–0–1
|- style="background:#fdd;"
! scope="row" | 6
| October 31, 1920 || vs. [[Akron Pros]] || 10–0 L || Lakeside Park || 6,000–10,000 || 3–1–1
|- style="background:#dfd;"
! scope="row" | 7
| November 7, 1920 || at Cleveland Tigers || 18–0 W || [[League Park|Dunn Field]] || 8,000 || 4–1–1
|- style="background:#dfd;"
! scope="row" | 8
| November 14, 1920 || vs. [[Chicago Tigers]] || 21–0 W || Lakeside Park || 8,000 || 5–1–1
|- style="background:#dfd;"
! scope="row" | 9
| November 21, 1920 || at [[Buffalo (1920s NFL teams)|Buffalo All-Americans]] || 3–0 W || [[Buffalo Baseball Park]] || 9,000 || 6–1–1
|- style="background:#fdd;"
! scope="row" | 10
| November 25, 1920 || at Akron Pros || 7–0 L || [[League Field]] || 6,500 || 6–2–1
|- style="background:#fdd;"
! scope="row" rowspan="2" | 11
| December 4, 1920 || at Buffalo All-Americans || 7–3 L || [[Polo Grounds]] || 10,000–12,000 || 6–3–1
|- style="background:#fea;"
| December 5, 1920 || at [[Washington Glee Club]]{{Dagger}} || 0–0 T || [[New Haven, Connecticut]] || 3,000 || 6–3–2
|- style="background:#fdd;"
! scope="row" | 12 
| December 11, 1920 || at [[Union Club of Phoenixville|Union AA of Phoenixville]]{{Dagger}} || 13–7 L || [[Phillies Park]] || 17,000 || 6–4–2
|- style="background:#dfd;"
! scope="row" | 13
| December 18, 1920 || at [[Richmond AC]]{{Dagger}} || 39–0 W || [[Boulevard Field]] || {{NA}} || 7–4–2
|- style="text-align: center;"
| colspan="7" | A dagger ({{Dagger}}) indicates teams not affiliated with the APFA.<ref name="nflhist">NFL History (2003), pp. 1–7</ref><ref name="dogs">{{cite web | url=http://www.profootballarchives.com/1920apfacan.html | title=1920 Canton Bulldogs | work=The Pro Football Archives | publisher=Maher Sports Media | accessdate=April 2, 2012}}</ref>
|}

== Game summaries ==

=== Week 2: vs. Pitcairn Quakers ===
{{Linescore Amfootball
|Road=Quakers
|R1=0|R2=0|R3=0|R4=0
|Home='''Bulldogs'''
|H1=34|H2=0|H3=0|H4=14
}}
''October 3, 1920, at Lakeside Park''

The Bulldogs opened the 1920 season against the Pitcairn Quakers. The team got out to a quick lead and was never in danger, scoring 34 points in the first quarter as back Joe Guyon rushed for three touchdowns, tackle [[Pete Henry]] caught a 15-yard touchdown pass from back [[Tex Grigg]], and back [[Johnny Hendren]] returned an interception for a touchdown.<ref name="2news">{{Cite news | url=https://news.google.com/newspapers?id=dGxKAAAAIBAJ&sjid=QYYMAAAAIBAJ&pg=2868%2C2793544 | title=Canton Trims Pitcairn Team | newspaper=[[Youngstown Vindicator]]| date=October 4, 1920 | page=19}}</ref><ref name="hist1">NFL History (2003), p. 1</ref> The Bulldogs were only forced to punt once in the game, and did not attempt to score in the second and third quarters because of the large lead.<ref name="2news" /> In the fourth quarter, however, end [[Bunny Corcoran]] caught a 35-yard touchdown pass from Guyon, and back [[Ike Martin]] ran for a one-yard touchdown.<ref name="hist1" /> The final score was 48–0, and Guyon was the offensive star.<ref name="2news" />

=== Week 3: vs. Toledo Maroons ===
{{Linescore Amfootball
|Road=Maroon
|R1=0|R2=0|R3=0|R4=0
|Home='''Bulldogs'''
|H1=7|H2=14|H3=14|H4=7
}}
''October 10, 1920, at Lakeside Park''

The Bulldogs were scheduled to play their second game against the [[Rochester Jeffersons]], but faced the Toledo Maroons after that match was cancelled.<ref name="dogs" /> For the second game in a row the Bulldogs scored over 40 points in a shutout as Martin and Guyon dominated on offense.<ref name="3news">{{Cite news | url=https://news.google.com/newspapers?nid=pqgf-8x9CmQC&dat=19201011&printsec=frontpage&hl=en | title=Canton Stops Toledo Eleven | newspaper=Youngstown Vindicator | date=October 11, 1920 | page=11}}</ref> Martin scored the first touchdown of the game with a run in the first quarter.<ref name="hist2">NFL History (2003), p. 2</ref> In the second quarter, Hendren scored a rushing touchdown, and end [[Tom Whelan]] caught a touchdown pass from Grigg.<ref name="hist2" /> In the third quarter, Martin caught a touchdown pass from Grigg, and Grigg rushed for another.<ref name="hist2" /> The Bulldogs' final score was a rushing touchdown from back [[Pete Calac]] in the fourth quarter.<ref name="hist2" /> The final score was 48–0.<ref name="3news" /> The Maroons never got close to scoring and did not make a single a first down.<ref name="3news" />

=== Week 4: vs. Cleveland Tigers ===
{{Linescore Amfootball
|Road=Tigers
|R1=0|R2=0|R3=0|R4=0
|Home='''Bulldogs'''
|H1=7|H2=0|H3=0|H4=0
}}
''October 17, 1920, at Lakeside Park''

The Bulldogs next faced the Cleveland Tigers, their first APFA opponent, and won 7–0 before a crowd of 7,000 people.<ref name="hist2" /> Despite the Bulldogs' 15 first downs, the only score of the game came on Martin's 7-yard touchdown run in the first quarter.<ref name="hist2" /><ref name="4news">{{Cite news | url=https://news.google.com/newspapers?id=-u9XAAAAIBAJ&sjid=BEUNAAAAIBAJ&pg=4221%2C2532398 | title=Canton Beats Cleveland | date=October 18, 1920 | page=14 | newspaper=[[The Toledo News-Bee]]}}</ref> Thorpe made his season debut in the game, coming in as a substitute in the fourth quarter.<ref name="4news" />

=== Week 5: at Dayton Triangles ===
{{Linescore Amfootball
|Road=Bulldogs
|R1=7|R2=7|R3=3|R4=3
|Home=Triangles
|H1=0|H2=14|H3=6|H4=0
}}
''October 24, 1920, at Triangle Park''

Bulldogs battled the Dayton Triangles in week five. The Bulldogs opened the scoring in the first quarter on a two-yard rushing touchdown by [[Pete Calac]].<ref name="hist3" /> But the Triangles came back in the second quarter,<ref name="5news">{{Cite news | url=https://news.google.com/newspapers?nid=pqgf-8x9CmQC&dat=19201025&printsec=frontpage&hl=en | title=Thorpe Saves Canton Eleven with Two Goals | newspaper=Youngstown Vindicator| date=October 25, 1920 | page=10}}</ref> scoring twice: back Frank Bacon had a four-yard rushing touchdown, and end [[Dave Reese]] had a 50-yard receiving touchdown.<ref name="hist3" /> Guyon scored a 22-yard rushing touchdown during the corner, but the extra point sailed wide.<ref name="hist3" /> In the third quarter, the Triangles responded with a 3-yard rushing touchdown by back [[Lou Partlow]], but Dayton missed the extra point to make the score 20–14.<ref name="hist3" /> Thorpe then came into the game,<ref name="5news" /> and kicked a 45-yard field goal to bring his team within three points.<ref name="hist3" /><ref name="5news" /> In the final minutes, Thorpe kicked another 35-yard field goal to tie it.<ref name="hist3" /> The Triangles were the first team to score on the Bulldogs since the opening game of the previous year.<ref name="5news" />

=== Week 6: vs. Akron Pros ===
{{Linescore Amfootball
|Road='''Pros'''
|R1=3|R2=7|R3=0|R4=0
|Home=Bulldogs
|H1=0|H2=0|H3=0|H4=0
}}
''October 31, 1920, at Lakeside Park''

The Bulldogs' next opponent was the Akron Pros, who were undefeated at the time and were gaining attention around the league.<ref name="carroll1982-2">Carroll (1982), p. 2</ref> The game was the first of a two-game series between the Bulldogs and Pros, considered to be two of the best teams in the country.<ref name="6news">{{Cite news | url=https://news.google.com/newspapers?id=MC1KAAAAIBAJ&sjid=_YUMAAAAIBAJ&pg=3302%2C1526680 | title=Akron Takes Canton in Great Game | newspaper=Youngstown Vindicator| date=November 1, 1920 | page=20}}</ref> In the first quarter, after an exchange of punts, Pros tackle [[Charlie Copley]] kicked a 38-yard field goal.<ref name="hist3">NFL History (2003), p. 3</ref> On a Bulldog possession at midfield, a Gilroy pass was tipped by the Pros' Copley and [[Bob Nash (American football)|Bob Nash]].<ref name="6news" /> Pros tackle [[Pike Johnson]] caught the ball before it landed and ran 55 yards for a touchdown.<ref name="hist3" /> In the third quarter, Jim Thorpe came into the game but could not get the Bulldogs back into the game.<ref name="6news" />

=== Week 7: at Cleveland Tigers ===
{{Linescore Amfootball
|Road='''Bulldogs'''
|R1=0|R2=14|R3=2|R4=2
|Home=Tigers
|H1=0|H2=0|H3=0|H4=0
}}
''November 7, 1920, at Dunn Field''

Coming off their first loss, the Bulldogs faced the Cleveland Tigers in week seven. Neither team scored in the first quarter, but the Bulldogs ran for two touchdowns in the second. Calac and Grigg had 6- and 15-yard rushing touchdowns.<ref name="hist4">NFL History (2003), p. 4</ref> The Bulldogs' defense forced two safeties—one in the third and one in the fourth quarter—to win the game 18–0.<ref name="hist4" />
{{Clear}}

=== Week 8: vs. Chicago Tigers ===
{{Linescore Amfootball
|Road=Tigers
|R1=0|R2=0|R3=0|R4=0
|Home='''Bulldogs'''
|H1=0|H2=14|H3=7|H4=0
}}
''November 14, 1920, at Lakeside Park''

The Bulldogs' next matchup was against the Chicago Tigers. The first scoring came in the second quarter, when Higgins recovered a fumble and ran it back for a touchdown.<ref name="hist4-5">NFL History (2003), pp. 4–5</ref> In the same quarter, Henry caught an interception and ran it back 50 yards for a touchdown.<ref name="hist4-5" /> Calac then ran for a one-yard touchdown in the third quarter to seal the 21–0 win.<ref name="hist4-5" />
{{Clear}}

=== Week 9: at Buffalo All-Americans ===
{{Linescore Amfootball
|Road='''Bulldogs'''
|R1=0|R2=0|R3=0|R4=3
|Home=All-Americans
|H1=0|H2=0|H3=0|H4=0
}}
''November 21, 1920, at Buffalo Baseball Park''

In week nine, the Bulldogs played the Buffalo All-Americans, who were undefeated at the time. Thorpe started the game but came out at halftime because he believed it would end in a tie.<ref name="9news">{{Cite news | url=https://news.google.com/newspapers?nid=pqgf-8x9CmQC&dat=19201122&printsec=frontpage&hl=en | title=Canton Downs Buffalo Team | newspaper=Youngstown Vindicator| page=11 | date=November 22, 1920}}</ref> Both teams were slowed by a muddy field, and the football became soggy after three quarters.<ref name="9news" /> The lone score of the game came with under four minutes to play: a field goal from the Bulldogs' Feeney.<ref name="9news" /> The game was the only loss of the season for the All-Americans.

===Week 10: at Akron Pros===
{{Linescore Amfootball
|Road=Bulldogs
|R1=0|R2=0|R3=0|R4=0
|Home='''Pros'''
|H1=7|H2=0|H3=0|H4=0
}}
''November 25, 1920, at League Park''

In week ten, the Bulldogs played the Pros for the second time in the season.<ref name="carroll1982-2" /> In the first quarter, a fumbled punt by the Bulldogs gave the Pros the ball at their 32-yard line.<ref name="carroll1982-2" /><ref name="Week 10 news" /> On the ensuing drive, the Pros passed for the game's lone score, a touchdown from King to Nash.<ref name="Week 10 news" /> The Bulldogs lost 7–0 in the first professional game [[NFL on Thanksgiving Day|played on Thanksgiving Day]], which launched a yearly tradition.<ref name="Week 10 news">{{Cite news | title=Akron Defeats Canton Eleven: Wins Professional Championship for the World from Thorpe's Bulldogs | date=November 26, 1920 | url=https://news.google.com/newspapers?nid=pqgf-8x9CmQC&dat=19201126&printsec=frontpage&hl=en | newspaper=Youngstown Vindicator| page=13}}</ref><ref>{{cite web | url=http://www.nfl.com/thanksgiving/story?id=09000d5d8045a5e3&template=without-video&confirm=true | title=Thanksgiving Serves up Classic Games | publisher=[[National Football League]] | accessdate=December 14, 2011}}</ref>

=== Week 11: at Buffalo All-Americans ===
{{Linescore Amfootball
|Road=Bulldogs
|R1=0|R2=0|R3=3|R4=0
|Home='''All-Americans'''
|H1=0|H2=0|H3=0|H4=7
}}
''December 4, 1920, at Polo Grounds''

The following week, the Bulldogs played their second game against the All-Americans, losing 7–3.<ref name="11news1">{{Cite news | url=https://news.google.com/newspapers?id=aO9WAAAAIBAJ&sjid=fkINAAAAIBAJ&pg=1212,1102615&dq=canton+bulldogs&hl=en | title=Tigers Tame Jim Thorpe's Bulldogs | newspaper=[[The Sunday Chronicle]] | date=December 5, 1920 | page=12}}</ref> The Bulldogs did not get a first down or complete a pass during the game,<ref name="11news2">{{Cite news | url=https://query.nytimes.com/mem/archive-free/pdf?res=F10614FF345411738DDDAC0894DA415B808EF1D3 | title=All-Americans Win Pro Gridiron Game | newspaper=[[The New York Times]] | date=December 5, 1920}}</ref> but Thorpe kicked a field goal in the third quarter after a fumble recovery for the team's only score.<ref name="11news1" /> In the fourth quarter, All-Americans tackle Youngstrom blocked a Thorpe punt and returned it for a touchdown.<ref name="11news2" /> ''The Sunday Chronicle'' named Thorpe, Henry and Lowe as the Bulldogs' stars, while Anderson, Youngstrom, and Miller were the standouts for the All-Americans.<ref name="11news1" />

=== Week 11: at Washington Glee Club ===
{{Linescore Amfootball
|Road=Bulldogs
|R1=0|R2=0|R3=0|R4=0
|Home=Glee Club
|H1=0|H2=0|H3=0|H4=0
}}
''December 5, 1920, in Weiss Park''

The following day, the Bulldogs played the non-APFA Washington Glee Club.<ref name="glee">{{cite web | url=http://www.profootballarchives.com/1920newhwa.html | title=1920 New Haven Washington Glees | work=The Pro Football Archives | publisher=Maher Sports Media | accessdate=April 2, 2012}}</ref> Coming into the game, the Glee Club allowed just seven points all season.<ref name="glee" /> The teams tied 0–0 before a crowd of 3,000 people.<ref name="hist6">NFL History (2003), p. 6</ref>
{{Clear}}

=== Week 12: at Union AA of Phoenixville ===
{{Linescore Amfootball
|Road=Bulldogs
|R1=0|R2=7|R3=0|R4=0
|Home='''AA of Phoenixville'''
|H1=0|H2=7|H3=6|H4=0
}}
''December 11, 1920, at Phillies Park''

In their third game in seven days, the Bulldogs played Union AA of Phoenixville, who came into the game undefeated.<ref>{{cite web | url=http://www.profootballarchives.com/1920uni.html | title=1920 Union AA | work=The Pro Football Archives | publisher=Maher Sports Media | accessdate=April 2, 2012}}</ref><ref>{{cite web | author=PFRA Research | url=http://www.profootballresearchers.org/Articles/Forward_Into_Invisibility.pdf | title=Forward into Invisibility: 1920 | publisher=[[Professional Football Researchers Association]] | page=5 | accessdate=April 2, 2012}}</ref> Before the largest recorded crowd of the season, the Bulldogs lost to Union AA 13–7.<ref name="nflhist6-7">NFL History (2003), pp. 6–7</ref> Neither team scored in the first quarter, but each scored a touchdown in the second.<ref name="nflhist6-7" /> The Bulldogs' Calac had a six-yard rushing touchdown, and Union AA's, Hayes caught a six-yard pass from Scott for a touchdown.<ref name="nflhist6-7" /> In the third quarter, Union AA's Hayes blocked a punt and ran it back for a touchdown, sealing the win.<ref name="nflhist6-7" /> Despite not being part of the APFA, AA of Phoenixville after the season called themselves the "US Professional Champions".<ref name="oncemore">{{cite web | author=PFRA Research | url=http://www.profootballresearchers.org/articles/once_more_with_feeling.pdf | title=Once More, With Feeling: 1921 | publisher=[[Professional Football Researchers Association]] | accessdate=April 2, 2012}}</ref>

=== Week 13: at Richmond AC ===
{{Linescore Amfootball
|Road='''Bulldogs'''
|R1=13|R2=0|R3=13|R4=13
|Home=Richmond AC
|H1=0|H2=0|H3=0|H4=0
}}
''December 18, 1920, at Boulevard Field''

The Bulldogs beat Richmond AC 39–0 in the final game of the season.<ref name="hist7">NFL History (2003), p. 7</ref> Richmond AC was not part of the APFA, and this was the team's only game in 1920.<ref>{{cite web | url=http://www.profootballarchives.com/1920ric.html | title=1920 Richmond AC | work=The Pro Football Archives | publisher=Maher Sports Media | accessdate=April 2, 2012}}</ref> The Bulldogs scored 13 points in the first, third, and fourth quarter to win in the shutout.<ref name="hist7" /> Guyon scored two rushing touchdowns, while Jim Thorpe threw touchdown passes to Corcoran and Lowe. The other two touchdowns came on runs by Whelen and Grigg.<ref name="hist7" /> Guyon made two field goals, and Thorpe added a third.<ref name="hist7" />

== Standings ==
{{1920 APFA standings}}

== Post-season ==
Hurt by losses to the Akron Pros and Buffalo All-Americans, the Bulldogs did not contend for the APFA trophy in 1920. Following the season, Thorpe left to start a new club composed of Native Americans in LaRue, Ohio<ref name="oncemore" /> and Cap Edwards took over as head coach. Sportswriter Bruce Copeland compiled the All-Pro list for the 1920 season, naming the Bulldogs' Wilbur Henry to the first team. Cub Buck, Harry Dadmun, and Joe Guyon were on the second team, and Pete Calac was on the third team.<ref>Hogrogian (1984), pp. 1–2</ref> Three men who played for the 1920 Canton Bulldogs were later inducted into the Pro Football Hall of Fame: Thorpe and Pete Henry in 1963 and Guyon in 1966.<ref>{{Cite web | url=http://www.profootballhof.com/hof/member.aspx?PlayerId=84 | title=Joe Guyon | publisher=[[Pro Football Hall of Fame]] | accessdate=June 23, 2012}}</ref><ref>{{Cite web | url=http://www.profootballhof.com/hof/member.aspx?PlayerId=94 | title=Wilbur (Pete) Henry | publisher=[[Pro Football Hall of Fame]] | accessdate=June 23, 2012}}</ref><ref>{{Cite web | url=http://www.profootballhof.com/hof/member.aspx?PlayerId=213 | title=Jim Thorpe | publisher=[[Pro Football Hall of Fame]] | accessdate=June 23, 2012}}</ref>

== Roster ==
{| class="toccolours" style="text-align: left;"
|-
! colspan="9" style="background:Maroon; color:white; text-align:center;"|'''Canton Bulldogs 1920 roster'''<ref>{{cite web | last=Ruehle | first=Bernie | date=July 18, 2007 | url=http://www.rci.rutgers.edu/~maxymuk/home/ongoing/canton.html | title=Canton Bulldogs (1920–23, 1925–26) | publisher=[[Rutgers University]] | accessdate=March 29, 2012}}</ref>
|-
|
| style="font-size:95%; vertical-align:top;"| 
* Cub Buck ([[Tackle (American football)|T]])
* Pete Calac ([[Back (American football)|B]]/[[End (American football)|E]])
* Bunny Corcoran (E/B)
* Harrie Dadmun ([[Guard (American football)|G]]/E)
* Cap Edwards (G/T)
* Al Feeney ([[Center (American football)|C]])
* Johnny Gilroy (B)
* Tom Gormley (G/T)
* Larry Green (E)
* Tex Grigg (B)
| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| 
* [[Joe Guyon]] (B/E)
* [[Doc Haggerty]] (G)
* Art Haley (B)
* John Hendren (B)
* [[Pete Henry]] (T)
* [[Bob Higgins (American football)|Bob Higgins]] (E)
* [[John Kellison]] (G/T)
* Bull Lowe (E/T)
* Buck MacDonald (G)
* Al Maginnes (G/C)
| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| 
* Ike Martin (B)
* Ralph Meadow (E)
* [[Joe Murphy (American football)|Joe Murphy]] (G)
* [[Dan O'Connor (American football)|Dan O'Connor]] (G/T)
* [[Lou Smyth]] (B)
* [[Dutch Speck]] (G/C)
* Jim Thorpe (B/E)
* Tom Whelan (E/C)
|}

== Notes ==
{{Reflist|2}}

== References ==
* {{Cite journal | last1=Braunwart | first1=Bob | last2=Carroll | first2=Bob | year=1981 | url=http://www.profootballresearchers.org/Coffin_Corner/03-07-068.pdf | title=The Ohio League | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=3 | issue=7}}
* {{Cite journal | last=Carroll | first=Bob | year=1982 | url=http://www.profootballresearchers.org/Coffin_Corner/04-12-119.pdf | title=Akron Pros 1920 | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=4 | issue=12}}
* {{Cite journal | last=Hogrogian | first=John | year=1984 | url=http://www.profootballresearchers.org/Coffin_Corner/06-01-173.pdf | title=1920 All-Pros | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=6 | issue=1}}
* {{Cite journal | author=PFRA Research | year=1980 | url=http://www.profootballresearchers.org/Coffin_Corner/02-08-038.pdf | title=Happy Birthday NFL? | journal=[[The Coffin Corner]] | publisher=[[Professional Football Researchers Association]] | volume=2 | issue=8 | archiveurl=http://www.webcitation.org/5zNOTWbno | archivedate=June 11, 2011}}
* {{Cite book | last1=Siwoff | first1=Seymour | last2=Zimmber | first2=Jon | last3=Marini | first3=Matt | year=2010 | title=The Official NFL Record and Fact Book 2010 | publisher=[[National Football League]] | isbn=9781603208338}}

{{1920 APFA season by team}}
{{Canton Bulldogs}}
{{Canton Bulldogs seasons}}

{{DEFAULTSORT:1920 Canton Bulldogs Season}}
[[Category:Canton Bulldogs seasons]]
[[Category:1920 American Professional Football Association season by team|Canton Bulldogs]]