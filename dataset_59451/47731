'''Engineering Research Centers (ERC)''' are university-led institutions developed through the [[National Science Foundation|National Science Foundation (NSF)]] Directorate of Engineering.<ref name="NSF Website ERC">{{cite web|title=Engineering Research Centers|url=http://www.nsf.gov/funding/pgm_summ.jsp?pims_id=5502&org=EEC|publisher=National Science Foundation|accessdate=4 August 2012}}</ref>  While ERCs are initially funded by the NSF, they are expected to be self-sustaining within 10 years of being founded.  The Engineering Research Centers program was originally developed in 1984 with the mission of removing disparity between academic and industrial engineering applications.  In this way, engineering students would, theoretically, be better prepared to enter the engineering workforce.  As a result, the [[United States]] would gain a competitive advantage over other countries.  There have been three generations of Engineering Research Centers.  Each of these generations has been specifically designed to meet the dynamic engineering demands of the United States.  Due to the limited amount of funding available for ERCs, the program is competitive; out of 143 proposals submitted in 2008, only 5 were awarded centers.<ref name="NC ERC">{{cite news|title=On-Going Funded Research Projects: N.C. A&T Awarded NSF Engineering Research Center |url=http://www.ncat.edu/academics/coe/cben/research/index.html |accessdate=7 August 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120814032613/http://www.ncat.edu:80/academics/coe/cben/research/index.html |archivedate=14 August 2012 |df= }}</ref><ref name=Chiang>{{cite web|last=Chiang|first=Eric|title=Industry-University Cooperative Research Centers|url=http://www.ericchiang.org/files/Chiang_JTT2001.pdf|accessdate=16 August 2012}}</ref>  Commercialization of academic research is one of the primary goals of NSF ERCs.<ref name=LEDs>{{cite web|title=SiC Systems and the Smart Lighting ERC awarded National Science Foundation grant for green LEDs|url=http://ledsmagazine.com/press/25883|publisher=LEDs Magazine|accessdate=16 August 2012}}</ref><ref name=UConnecticut>{{cite web|title=Data Management Plans for Grant Funded Projects (NSF, NIH) |url=http://www.lib.uconn.edu/scholarlycommunication/data.html |publisher=University of Connecticut |accessdate=16 August 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120512203327/http://lib.uconn.edu/scholarlycommunication/data.html |archivedate=12 May 2012 |df= }}</ref><ref name="Data Management">{{cite web|title=Data Management for NSF Engineering Directorate Proposals and Awards|url=http://nsf.gov/eng/general/ENG_DMP_Policy.pdf|publisher=National Science Foundation|accessdate=16 August 2012}}</ref>

==Generation one==

The first generation of NSF ERCs began between 1985 and 1990 and encouraged academic institutions to focus education on manufacturing and commercial design.<ref name="ERC PDF">{{cite web|title=Engineering Research Centers (ERC)  Partnerships in Transformational Research, Education and Technology - A Focused Call for Nanosystems ERCs (NERCs)|url=http://www.nsf.gov/pubs/2011/nsf11537/nsf11537.pdf|publisher=National Science Foundation|accessdate=4 August 2012}}</ref>  This first generation comprised 18 centers.

==Generation two==

The National Science Foundation began funding the second generation of centers beginning in 1994 and continued until 2006.  This second generation included 22 centers and was focused on manufacturing efficiency.<ref name="ERC PDF" />  Unlike the first generation of ERCs, the second generation encouraged multi-university partnerships and also focused on developing pre-college, engineering-bound students.<ref name="ERC PDF" />  In addition, the second generation of ERCs was designed to help academic research reach commercialization.<ref name="ERC PDF" />  However, unlike later generations, the second generation focused on domestic programs and largely ignored the potential in global partnerships.

==Generation three==

Beginning in 2008, the NSF began accepting proposals for the third generation (Gen-3) of Engineering Research Centers.  Gen-3 ERCs were largely created due to decreased student interest in sciences and engineering and an increasingly global economy.<ref name="ERC PDF" />  To meet these challenges, the Gen-3 ERCs were commissioned to increase interest in innovation and unify different engineering pipelines (i.e. domestic and international institutions, academic and commercial institutions).<ref name=CSUOhio>{{cite web|title=Upcoming Grant Opportunities |url=http://www.csuohio.edu/research/wcsse/pdf/upcominggrants.pdf |publisher=Cleveland State University |accessdate=16 August 2012 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> Gen-3 programs particularly focus on [[nanotechnology]].

===Partner structure===
The mission of Gen-3 ERCs differs from the mission of the second generation ERCs in that Gen-3 centers embrace a global perspective. In creating the Gen-3 ERCs, the NSF recognized that streamlining existing processes is not enough to remain competitive in a global market.<ref name="ERC PDF" /><ref name=Duke>{{cite web|title=National Science Foundation - Engineering Research Centers (ERC) -- Partnerships in Transformational Research, Education and Technology - A Focused Call for Nanosystems ERCs (NERCs) |url=https://researchfunding.duke.edu/detail.asp?OppID=6434 |publisher=Duke University |accessdate=16 August 2012 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>  Instead, Gen-3 programs focus on also developing and globally commercializing novel engineering solutions.  Like second generation ERCs, Gen-3 programs use a multi-university model and are required to include between 1 and 4 domestic partners.  At least one of these partner universities must serve a large population of underrepresented groups.<ref name="ERC PDF" />  Gen-3 programs are also required to include between 1 and 3 non-domestic partners.<ref name="ERC PDF" />  Faculty from non-partner universities may become affiliated in order to fill "expertise gaps".

Above and beyond university partner requirements, Gen-3 ERCs are required to partner with domestic pre-college institutions, particularly local middle schools and high schools.  ERCs must also have "industry/practitioner" members that pay fees to use ERC resources.<ref name="ERC PDF" />  These industry/practitioner members may include businesses and hospitals.

ERCs are required to support [[Research Experiences for Undergraduates]], [[Research Experiences for Teachers]], and pre-college (Young Scholars) summer research programs.<ref name="ERC PDF" /><ref name="Rutgers" />  In addition, ERCs partner with local K-12 institutions to increase the abilities of science and engineering educators.<ref name="UH Hilo">{{cite news|title=Local K-12 teachers partner with CoP to teach engineering principles|url=http://www.uhh.hawaii.edu/news/press/release/1199|accessdate=16 August 2012|newspaper=UH Hilo Press|date=July 9, 2012}}</ref>  Because of the global focus of Gen-3 ERCs, centers often host foreign exchange students.<ref name=Rutgers>{{cite web|title=NSF Grants Awarded To Rutgers Faculty |url=http://nsf.rutgers.edu/2008/04/research_8796.html |archive-url=https://archive.is/20130409234049/http://nsf.rutgers.edu/2008/04/research_8796.html |dead-url=yes |archive-date=9 April 2013 |publisher=Rutgers |accessdate=16 August 2012 }}</ref>

===Funding structure===

Funding for an ERC may not exceed $3,250,000 for the first year, but this limit increases by $250,000 per year until it reaches a maximum of $4,000,000.<ref name="ERC PDF" /><ref name="Light Directory">{{cite web|title=NSF Awards $18.5 Million For Smart Lighting Center|url=http://lightdirectory.com/news-NSF-Awards-%2418.5-Million-For-Smart-Lighting-Center-.html|publisher=Light Directory|accessdate=16 August 2012}}</ref>  The NSF committed enough funds ($9,750,000) to support three new centers beginning in the summer of 2012.<ref name="ERC PDF" />  Corporate partners are permitted to supplement this funding, and their contributions can be quite significant.<ref name=H&P>{{cite web|title=Engineering Research Center receives support|url=http://hydraulicspneumatics.com/other-technologies/engineering-research-center-receives-support|publisher=Hydraulics and Pneumatics|accessdate=16 August 2012}}</ref>  For example, in 2006, over 50 organizations invested in the Engineering Research Center for Compact and Efficient Fluid Power (CEFP) and added approximately $3 million to the CEFP's budget.<ref name="Green Car Congress">{{cite web|title=NSF Funds Engineering Research Center for Fluid Power|url=http://www.greencarcongress.com/2006/08/nsf_funds_engin.html|publisher=Green Car Congress|accessdate=7 August 2012}}</ref>  In some circumstances, ERCs obtain additional funding through other governmental agencies.<ref name=9News>{{cite news|title=Colorado School of Mines tapped to fix urban water problems |url=http://www.9news.com/news/article/209222/188/Colorado-School-of-Mines-tapped-to-fix-urban-water-problems |archive-url=https://archive.is/20130410121000/http://www.9news.com/news/article/209222/188/Colorado-School-of-Mines-tapped-to-fix-urban-water-problems |dead-url=yes |archive-date=April 10, 2013 |accessdate=16 August 2012 |newspaper=9News |date=July 21, 2011 }}</ref>

==Current centers<ref name="ERCA Current">{{cite web|title=Engineering Research Center|url=http://www.erc-assoc.org/centers|publisher=Engineering Research Centers Association|accessdate=5 August 2012}}</ref> ==

Currently, 17 ERCs are supported by the National Science Foundation.  These centers are listed below along with the years during which they were founded.  Programs founded before 2008 are second generation programs; programs founded in or after 2008 are Gen-3 programs.

===Manufacturing===
{| class="wikitable"
|-
! Center !! Lead Institution !! Year Founded
|-
| ERC for [[Center_for_Compact_and_Efficient_Fluid_Power|Compact and Efficient Fluid Power]] (CCEFP)[http://www.ccefp.org/]|| [[University of Minnesota]] || 2006
|-
| ERC for Structured Organic Particulate Systems, (C-SOPS)|| [[Rutgers University]] || 2006
|-
| Synthetic Biology ERC (SynBERC)  || [[University of California at Berkeley]]|| 2006
|-
| Center for Biorenewable Chemicals ([http://www.cbirc.iastate.edu/ CBiRC])|| [[Iowa State University]]|| 2008
|}

===Biotechnology and health care===
{| class="wikitable"
|-
! Center !! Lead Institution !! Year Founded
|-
| Biomimetic MicroElectronic Systems (BMES) ERC|| [[University of Southern California]] || 2003
|-
| Quality of Life ERC (QoLT)|| [[Carnegie Mellon University]] || 2006
|-
| ERC for Revolutionizing Metallic Biomaterials (RMB)|| [[North Carolina A&T University]]|| 2008
|-
| Engineering Research [[Center for Sensorimotor Neural Engineering]] (CSNE)|| [[University of Washington]]|| 2011
|}

===Energy, sustainability, and infrastructure===
{| class="wikitable"
|-
! Center !! Lead Institution !! Year Founded
|-
| [http://www.freedm.ncsu.edu Future Renewable Electric Energy Delivery and Management (FREEDM) Systems Center]|| [[North Carolina State University]] || 2008
|-
| [[Smart Lighting ERC]]|| [[Rensselaer Polytechnic Institute]]|| 2008
|-
| ERC for Quantum Energy and Sustainable Solar Technologies (QESST)|| [[Arizona State University]]|| 2011
|-
| ERC for Re-Inventing America’s Urban Water Infrastructure (ReNUWIt)|| [[Stanford University]]|| 2011
|-
| ERC for Ultra-wide Area Resilient Electric Energy Transmission Networks (CURENT)|| [[University of Tennessee–Knoxville]]|| 2011
|}

===Microelectronics, sensing, and information technology===
{| class="wikitable"
|-
! Center !! Lead Institution !! Year Founded
|-
| ERC for Power Optimization for Electro-Thermal Systems (POETS) <ref>{[http://poets-erc.org/ Power Optimization for Electro-Thermal Systems]}</ref>|| [[University of Illinois at Urbana-Champaign]] / [[University of Arkansas]] / [[Stanford University]] / [[Howard University]] || 2015

|-

| [[Engineering Research Center for Collaborative Adaptive Sensing of the Atmosphere|ERC for Collaborative Adaptive Sensing of the Atmosphere (CASA)]]|| [[University of Massachusetts Amherst]]|| 2003
|-
| ERC for Extreme Ultraviolet Science and Technology (EUV ERC)|| [[Colorado State University]] / [[University of Colorado at Boulder]] / [[University of California at Berkeley]] / [[Lawrence Berkeley National Laboratory]] <ref>{{cite web|url=http://euverc.colostate.edu/|title=ERC for Extreme Ultraviolet Science and Technology|accessdate=22 Mar 2013}}</ref>|| 2003
|-
| ERC on Mid-Infrared Technologies for Health and the Environment (MIRTHE)|| [[Princeton University]]|| 2006
|-
| Center for Integrated Access Networks (CIAN)|| [[University of Arizona]]|| 2008
|}

==Past (graduated) centers==

The following centers no longer receive funding from the National Science Foundation. Centers founded in or after 1994 are second generation ERCs.  Centers founded before 1994 are first generation.

===Manufacturing===
{| class="wikitable"
! Center !! Lead Institution !! Year Founded !! Year of Graduation
|-
| Systems Research Center|| [[University of Maryland]]|| 1985/1994|| 1997
|-
| Engineering Design Research Center|| [[Carnegie Mellon University]]|| 1986|| 1997
|-
| ERC for Net Shape Manufacturing|| [[Ohio State University]]|| 1986|| 1997
|-
| Center for Interfacial Engineering|| [[University of Minnesota]]|| 1988|| 1999
|-
|-
| Particle Engineering Research Center|| [[University of Florida]]|| 1995|| 2006
|-
| ERC for Environmentally Benign Semiconductor Manufacturing|| [[University of Arizona]]|| 1996|| 2006
|-
| ERC for Reconfigurable Manufacturing Systems|| [[University of Michigan]]|| 1996|| 2007
|-
| Center for Advanced Engineering of Fibers and Films|| [[Clemson University]]|| 1998|| 2008
|-
| Gordon ERC for Subsurface Sensing and Imaging Systems|| [[Northeastern University]]|| 2000|| 2010
|-
| [[Engineering Research Center for Wireless Integrated Microsystems|ERC for Wireless Integrated MicroSystems]]|| [[University of Michigan]]|| 2000|| 2010
|}

===Biotechnology and health care===
{| class="wikitable"
|-
! Center!! Lead Institution !! Year Founded !! Year of Graduation
|-
| Emerging Cardiovascular Technologies|| [[Duke University]]|| 1987|| 1998
|-
| Biotechnology Process Engineering Center|| [[Massachusetts Institute of Technology]]|| 1985/1995|| 2005
|-
| Center for Biofilm Engineering|| [[Montana State University]]|| 1990|| 2001
|-
| Engineered Biomaterials ERC|| [[University of Washington]]|| 1996|| 2007
|-
| ERC for Computer-Integrated Surgical Systems and Technology|| [[Johns Hopkins University]]|| 1998|| 2008
|-
| ERC for the Engineering of Living Tissues|| [[Georgia Institute of Technology]]|| 1998|| 2008
|-
| VaNTH ERC for Bioengineering Educational Technologies|| [[Vanderbilt University]]|| 1999|| 2007
|}

===Energy, sustainability, and infrastructure===
{| class="wikitable"
|-
! Center !! Lead Institution !! Year Founded !! Year of Graduation
|-
| Advanced Combustion Engineering Research Center|| [[Brigham Young University]]/[[University of Utah]]|| 1986|| 1997
|-
| ERC for Advanced Technology for Large Structural Systems|| [[Lehigh University]]|| 1986|| 1997
|-
| Mid-America Earthquake Center|| [[University of Illinois at Urbana-Champaign]]|| 1997|| 2007
|-
| Multidisciplinary Center for Earthquake Engineering Research|| [[University at Buffalo, The State University of New York|The University at Buffalo]]|| 1997|| 2007
|-
| Pacific Earthquake Engineering Research Center|| [[University of California at Berkeley]]|| 1997|| 2007
|-
| Offshore Technology Research Center|| [[Texas A&M]]/[[University of Texas]]|| 1988|| 1999
|}

===Microelectronics, sensing, and information technology===
{| class="wikitable"
|-
! ERC !! Lead Institution !! Year Founded !! Year of Graduation
|-
| ERC for Compound Semiconductor Microelectronics|| [[University of Illinois]]|| 1986|| 1997
|-
| Data Storage Systems Center|| [[Carnegie Mellon University]]|| 1990|| 2001
|-
| ERC for Computational Field Simulation|| [[Mississippi State University]]|| 1990|| 2001
|-
| ERC for Neuromorphic Systems Engineering|| [[California Institute of Technology]]|| 1995|| 2006
|-
| Microelectronics Packaging Research Center|| [[Georgia Institute of Technology]]|| 1994<ref>http://www.nsf.gov/pubs/2000/nsf00137/nsf00137q.htm</ref>|| 2006
|-
| Integrated Media Systems Center|| [[University of Southern California]]|| 1996|| 2007
|-
| ERC for Power Electronics Systems|| [[Virginia Polytechnic Institute and State University]]|| 1998|| 2008
|}

==References==

 {{Reflist}}

==External links==
* [http://www.erc-assoc.org/ ERC Association]

==See also==

[[Category:National Science Foundation]]