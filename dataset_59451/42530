<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Caudron Type L
 | image=Caudron Type L.png
 | caption=Type L at the 1913 Paris Salon
}}{{Infobox Aircraft Type
 | type=Naval observation aircraft
 | national origin=[[France]]
 | manufacturer=[[Caudron]]
 | designer=
 | first flight=c.1913
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=4
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Caudron Type L''' was a two-seat [[France|French]] [[pusher configuration]] [[amphibious aircraft|amphibious]] [[biplane]], flown around 1913 and intended for naval use.

==Design==

With unequal span, [[biplane#Bays|two bay]], [[stagger (aeronautics)|unstaggered]] wings and an open frame [[fuselage]] the Type L had much in common with the smaller [[Caudron Type Caudron-Fabre|Caudron-Fabre]] pusher amphibian, as well as with the [[tractor configuration]] [[Caudron Type J|Type J]]  amphibians and [[Caudron Type H|Type H]] [[floatplane]]s, some of which were also amphibians. Most of the earlier Caudron biplane types had similar wings and fuselages. The Type L had three sets of parallel and vertical [[interplane strut]]s on each side, the innermost close to the central [[nacelle]] and bracing the centre section.  The overhanging upper wing were braced  by a pair of parallel, outward leaning struts which joined the lower wing at the base of the outer vertical struts.<ref name=Hauet/>  The inner struts, to which the engine was attached, were [[sorbus|ash]] and the remainder hollowed out [[spruce]]; all joined the rather close pairs of [[spar (aeronautics)|spar]]s in the upper and lower wings.<ref name=Flight/> Diagonal [[flying wire]]s completed the bracing. The interplane gap was {{convert|1.65|m|ftin|abbr=on|0}}.<ref name=Hauet/> Lateral ([[Flight dynamics (fixed-wing aircraft)|roll]]) control was by [[wing warping]].<ref name=Flight/>

The Type L did not have a long, enclosed fuselage but instead the [[empennage]] was supported on an open girder.  The Type L, unusually for Caudron, had two parallel sided, cross braced side frames.<ref name=Hauet/> The longitudinal members were steel and the cross-members ash.<ref name=Flight/>  The side frames began from the rear spars of both upper and lower wings and converged rearwards to meet at the tail, with upper and lower cross-members towards the rear.<ref name=Hauet/>  A short nacelle on the lower wing contained an open, wide [[cockpit]] with [[tandem#Side-by-side seating|side-by-side seating]], the pilot on the right.  Controls were conventional.  The nacelle ended behind the forward wing spar, ahead of the {{convert|100|hp|kW|abbr=on|0}} [[Gnome Delta]] nine cylinder [[rotary engine]] mounted on the rear interplane struts and driving a two blade [[propeller (aeronautics)|propeller]] via 2:1 reduction gearing and a long shaft to clear the [[trailing edge]]. At the extreme tail, the final vertical frame member served as the axis for the [[rudder]], with a small, roughly triangular [[fin]] ahead of it. The [[elevator (aeronautics)|elevator]] hinge was also mounted high on this member,<ref name=Flight/> with a straight edged [[tailplane]] ahead of it.<ref name=Hauet/>

The Type L had short floats, flat bottomed with no [[chine]] but in plan and profile curving inwards to the nose.  Unlike the Fabre floats of the [[Caudron Type Caudron-Fabre|Caudron-Fabre]], these had a step about halfway back. A landing wheel was located on the float centreline, working on an unsprung axle in a slot through to the upper side.  The floats were mounted on outer N-form struts to the bases of the inner interplane struts and by W-form struts to the lower longitudinal girder member,<ref name=Hauet/> pivoted at the front with rubber [[shock absorber]]s at the rear.<ref name=Flight/> Another rectangular cross-section but unstepped float was positioned under the tail.

The first prototype, intended for delivery to the [[Royal Navy|British Navy]], appeared at the 1913 Paris Salon in December<ref name=Flight/> but it is not known if it had been flown by then. It was destroyed by fire after an engine backfire during tests.  The second was trialled by the [[French Navy]], who found it unsatisfactory, it was then delivered to the British as a replacement for the lost first prototype. The last two were rebuilt as [[Caudron Type J|Type J marine]]s and used by the French Navy.<ref name=Hauet/>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref=Hauet (2001) p.53<ref name=Hauet/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One pilot
|capacity=One passenger
|length m=8.05
|length note=
|upper span m=14.7
|upper span note=
|lower span m=9.3
|lower span note=
|height m=3.1
|height note=
|wing area sqm=40
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=510
|empty weight note=
|gross weight kg=730
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Gnome Delta]]
|eng1 type=9-cylinder [[rotary engine|rotary]]
|eng1 hp=100
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=3.0
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=90
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=

}}

==References==
{{reflist|refs=

<ref name=Hauet>{{cite book |title=Les Avions Caudrons |last=Hauet|first=André|year=2001|volume=1|publisher=Lela Presse|location=Outreau|isbn=2 914017-08-1|pages=53–5}}</ref>

<ref name=Flight>{{cite magazine |date=20 December 1913 |title= The Paris Aero Salon 1913 - Caudron.|magazine= [[Flight International|Flight]]|volume=V |issue=51 |pages=1379–80  |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201353.html}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

{{Caudron aircraft}}

[[Category:Amphibious aircraft]]
[[Category:French military aircraft 1910–1919]]
[[Category:Caudron aircraft|TL]]