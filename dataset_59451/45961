{{infobox curling
| Name = 2015 GSOC Tour Challenge
| Logo = 
| Size = 
| Host city = [[Paradise, Newfoundland and Labrador|Paradise]], [[Newfoundland and Labrador]]
| Arena = [[Paradise Double Ice Complex]]
| Dates = September 8–13
| Men's winner = {{flagicon|AB}} '''Team Koe'''
| Curling club = [[The Glencoe Club|Glencoe CC]], [[Calgary]]
| Skip = [[Kevin Koe]]
| Third = [[Marc Kennedy]]
| Second = [[Brent Laing]]
| Lead = [[Ben Hebert]]
| Finalist = {{flagicon|NL}} [[Brad Gushue]]
| Women's winner = {{flagicon|SUI}} '''Team Tirinzoni'''
| Curling club2 = 
| Skip2 = [[Silvana Tirinzoni]]
| Third2 = [[Manuela Siegrist]]
| Second2 = [[Esther Neuenschwander]]
| Lead2 = [[Marlene Albrecht]]
| Finalist2 = {{flagicon|ON}} [[Rachel Homan]]
}}
The '''2015 [[GSOC Tour Challenge]]''' was held from September 8 to 13 at the [[Paradise Double Ice Complex]] in [[Paradise, Newfoundland and Labrador|Paradise]], [[Newfoundland and Labrador]]. It was the first [[Grand Slam (curling)|Grand Slam]] event of the [[2015–16 curling season]] for both the men's and women's World Curling Tour.

{{TOC limit|4}}

==Men==

===Tier I===

====Teams====
The teams are listed as follows:<ref name="schedule">{{cite web|url=http://www.thegrandslamofcurling.com/curling/tour-challenge/gsoc-tour-challenge-schedule/|title=Tour Challenge Schedule|date=31 July 2015|publisher=[[Grand Slam of Curling]]|accessdate=2 August 2015}}</ref><ref name="mteams">{{cite web|url=http://www.worldcurl.com/events.php?eventid=3826&view=Teams|title=2015 GSOC Tour Challenge – Men's teams|publisher=[[World Curling Tour]]|accessdate=2 August 2015}}</ref>
{| class=wikitable
|-
! Skip !! Third !! Second !! Lead !! Locale
|-
| [[Brendan Bottcher]] || [[Tom Appelman]] || [[Brad Thiessen (curler)|Brad Thiessen]] || [[Karrick Martin]] || {{flagicon|AB}} [[Edmonton]], [[Alberta]]
|-
| [[Reid Carruthers]] || [[Braeden Moskowy]] || [[Derek Samagalski]] || [[Colin Hodgson]] || {{flagicon|MB}} [[West St. Paul, Manitoba|West St. Paul]], [[Manitoba]]
|-
| [[Adam Casey (curler)|Adam Casey]] || [[David Mathers (curler)|David Mathers]] || [[Anson Carmody]] || [[Robbie Doherty]] || {{flagicon|PE}} [[Charlottetown]], [[Prince Edward Island]]
|-
| [[Benoît Schwarz]] (fourth) || [[Claudio Pätz]] || [[Peter de Cruz]] (skip) || [[Valentin Tanner]] || {{flagicon|SUI}} [[Geneva]], [[Switzerland]]
|-
| [[Niklas Edin]] || [[Oskar Eriksson]] || [[Kristian Lindström]] || [[Christoffer Sundgren]] || {{flagicon|SWE}} [[Karlstad]], [[Sweden]]
|-
| [[John Epping]] || [[Mathew Camm]] || [[Patrick Janssen]] || [[Tim March]] || {{flagicon|ON}} [[Toronto]], [[Ontario]]
|-
| [[Brad Gushue]] || [[Mark Nichols (curler)|Mark Nichols]] || [[Brett Gallant]] || [[Geoff Walker (curler)|Geoff Walker]] || {{flagicon|NL}} [[Newfoundland and Labrador]]
|-
| [[Glenn Howard]] || [[Richard Hart (curler)|Richard Hart]] || [[Adam Spencer (curler)|Adam Spencer]] || [[Scott Howard (curler)|Scott Howard]] || {{flagicon|ON}} [[Penetanguishene]], [[Ontario]]
|-
| [[Brad Jacobs (curler)|Brad Jacobs]] || [[Ryan Fry]] || [[E. J. Harnden]] || [[Ryan Harnden]] || {{flagicon|ON}} [[Sault Ste. Marie, Ontario|Sault Ste. Marie]], [[Ontario]]
|-
| [[Kevin Koe]] || [[Brent Laing]] || [[Marc Kennedy]] || [[Ben Hebert]] || {{flagicon|AB}} [[Calgary]], [[Alberta]]
|-
| [[Steve Laycock]] || [[Kirk Muyres]] || [[Colton Flasch]] || [[Dallan Muyres]] || {{flagicon|SK}} [[Saskatoon]], [[Saskatchewan]]
|-
| [[Mike McEwen (curler)|Mike McEwen]] || [[B. J. Neufeld]] || [[Matt Wozniak]] || [[Denni Neufeld]] || {{flagicon|MB}} [[Winnipeg]], [[Manitoba]]
|-
| [[Sven Michel]] || [[Marc Pfister]] || [[Enrico Pfister]] || [[Simon Gempeler]] || {{flagicon|SUI}} [[Adelboden]], [[Switzerland]]
|-
| [[David Murdoch]] || [[Greg Drummond]] || [[Scott Andrews (curler)|Scott Andrews]] || [[Michael Goodfellow (curler)|Michael Goodfellow]] || {{flagicon|SCO}} [[Stirling]], [[Scotland]]
|-
| [[John Shuster]] || [[Tyler George]] || [[Matt Hamilton (curler)|Matt Hamilton]] || [[John Landsteiner]] || {{flagicon|USA}} [[Duluth]], [[Minnesota]]
|}

====Round Robin Standings====
''Final Round Robin Standings''
{| class="wikitable" style="text-align: center;"
|-
!colspan=2|Key
|-
| style="background:#ffffcc; width:20px;"|
|align=left|Teams to Playoffs
|}
{| table
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool A !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|MB}} [[Reid Carruthers]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|NL}} [[Brad Gushue]] || 3 || 1
|-
| {{flagicon|PE}} [[Adam Casey (curler)|Adam Casey]] || 1 || 3
|-
| {{flagicon|SWE}} [[Niklas Edin]] || 1 || 3
|-
| {{flagicon|SCO}} [[David Murdoch]] || 1 || 3
|-
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool B !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|AB}} [[Kevin Koe]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|USA}} [[John Shuster]] || 2 || 2
|-
| {{flagicon|SUI}} [[Peter de Cruz]] || 1 || 3
|-
| {{flagicon|ON}} [[John Epping]] || 1 || 3
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool C !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|AB}} [[Brendan Bottcher]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Glenn Howard]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]] || 3 || 1
|-
| {{flagicon|SK}} [[Steve Laycock]] || 1 || 3
|-
| {{flagicon|SUI}} [[Sven Michel]] || 0 || 4
|}
|}

====Round Robin Results====
All draw times are listed in [[Newfoundland Time Zone]].<ref name="schedule"/>

=====Draw 1=====
''Tuesday, September 8, 7:00 pm''
{{Curlingbox8
| sheet =A 
| team1 ={{flagicon|NL}} [[Brad Gushue]]
|0|1|0|0|2|0|0|0| |3
| team2 ={{flagicon|MB}} [[Reid Carruthers]] {{X}}
|1|0|0|1|0|1|1|1| |5
}}
{{Curlingbox8
| sheet =C
| team1 ={{flagicon|ON}} [[John Epping]]
|0|1|0|1|0|2|0|0|1|5
| team2 ={{flagicon|AB}} [[Kevin Koe]] {{X}}
|0|0|1|0|2|0|0|1|0|4
}}

=====Draw 2=====
''Wednesday, September 9, 9:00 am''
{{Curlingbox8
| sheet =A 
| team1 = {{flagicon|AB}} [[Brendan Bottcher]] {{X}}
|2|0|1|2|0|0|1|X| |6
| team2 ={{flagicon|SK}} [[Steve Laycock]] 
|0|1|0|0|1|2|0|X| |4
}}
{{Curlingbox8
| sheet =B
| team1 = {{flagicon|ON}} [[Glenn Howard]]
|0|2|1|0|3|0|2|X| |8
| team2 ={{flagicon|SUI}} [[Sven Michel]] {{X}}
|1|0|0|4|0|1|0|X| |6
}}

=====Draw 3=====
''Wednesday, September 9, 12:30 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|AB}} [[Kevin Koe]]
|2|1|1|0|2|0|1|X| |7
| team2 = {{flagicon|USA}} [[John Shuster]]
|0|0|0|2|0|1|0|X| |3
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]]
|2|0|0|0|0|0|0|1| |3
| team2 = {{flagicon|SUI}} [[Peter de Cruz]]
|0|0|0|1|0|1|0|0| |2
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|SWE}} [[Niklas Edin]] {{X}}
|0|1|0|0|1|0|2|0| |4
| team2 = {{flagicon|SCO}} [[David Murdoch]]
|1|0|0|2|0|2|0|1| |6
}}

=====Draw 4=====
''Wednesday, September 9, 4:00 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|MB}} [[Reid Carruthers]]
|0|1|0|0|2|2|0|X| |5
| team2 = {{flagicon|PE}} [[Adam Casey (curler)|Adam Casey]] {{X}}
|1|0|0|1|0|0|1|X| |3
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]] {{X}}
|0|1|0|1|1|0|0|1| |4
| team2 =  {{flagicon|AB}} [[Brendan Bottcher]]
|0|0|2|0|0|1|0|0| |3
}}

=====Draw 5=====
''Wednesday, September 9, 7:30 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|NL}} [[Brad Gushue]] {{X}}
|2|0|2|0|3|0|X|X| |7
| team2 = {{flagicon|SCO}} [[David Murdoch]]
|0|1|0|1|0|1|X|X| |3
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|ON}} [[John Epping]]
|1|0|0|2|0|0|2|0| |5
| team2 = {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]]
|0|3|0|0|0|1|0|2| |6
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|SK}} [[Steve Laycock]] 
|0|2|0|3|1|0|0|1| |7
| team2 = {{flagicon|SUI}} [[Sven Michel]] {{X}}
|1|0|2|0|0|2|1|0| |6
}}

=====Draw 6=====
''Thursday, September 10, 9:00 am''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]]
|2|0|2|0|0|1|0|1| |6
| team2 = {{flagicon|USA}} [[John Shuster]]
|0|2|0|0|1|0|2|0| |5
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|SWE}} [[Niklas Edin]]
|1|0|1|0|X|X|X|X| |2
| team2 = {{flagicon|MB}} [[Reid Carruthers]]
|0|5|0|2|X|X|X|X| |7
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|AB}} [[Kevin Koe]]
|1|0|3|0|2|0|0|1| |7
| team2 = {{flagicon|SUI}} [[Peter de Cruz]]
|0|1|0|2|0|2|0|0| |5
}}

=====Draw 7=====
''Thursday, September 10, 12:30 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|PE}} [[Adam Casey (curler)|Adam Casey]] {{X}}
|4|0|2|0|0|1|0|X| |7
| team2 = {{flagicon|SCO}} [[David Murdoch]]
|0|0|0|2|1|0|1|X| |4
}}
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|AB}} [[Brendan Bottcher]] {{X}}
|0|0|0|2|0|3|2|X| |7
| team2 = {{flagicon|SUI}} [[Sven Michel]]
|0|0|0|0|1|0|0|X| |1
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]] {{X}}
|0|0|1|0|2|0|2|0| |5
| team2 = {{flagicon|ON}} [[Glenn Howard]]
|0|0|0|2|0|1|0|3| |6
}}

=====Draw 8=====
''Thursday, September 10, 4:00 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|ON}} [[John Epping]] {{X}}
|2|0|0|0|0|0|X|X| |2
| team2 = {{flagicon|SUI}} [[Peter de Cruz]]
|0|2|0|3|0|3|X|X| |8
}}

=====Draw 9=====
''Thursday, September 10, 7:30 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]] {{X}}
|0|2|3|1|X|X|X|X| |6
| team2 = {{flagicon|SUI}} [[Sven Michel]]
|0|0|0|0|X|X|X|X| |0
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|NL}} [[Brad Gushue]]
|2|1|0|5|X|X|X|X| |8
| team2 = {{flagicon|PE}} [[Adam Casey (curler)|Adam Casey]] {{X}}
|0|0|1|0|X|X|X|X| |1
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|SK}} [[Steve Laycock]]  {{X}}
|0|0|1|0|1|0|X|X| |2
| team2 = {{flagicon|ON}} [[Glenn Howard]]
|1|0|0|2|0|4|X|X| |7
}}

=====Draw 10=====
''Friday, September 11, 9:00 am''
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|SWE}} [[Niklas Edin]]
|0|1|0|3|0|3|0|1| |8
| team2 = {{flagicon|PE}} [[Adam Casey (curler)|Adam Casey]]
|1|0|2|0|2|0|1|0| |6
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|ON}} [[John Epping]]
|0|0|0|0|1|0|x|x| |1
| team2 = {{flagicon|USA}} [[John Shuster]]
|0|1|1|1|0|2|x|x| |5
}}

=====Draw 11=====
''Friday, September 11, 12:30 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]] {{X}}
|2|0|3|0|2|X|X|X| |7
| team2 = {{flagicon|SK}} [[Steve Laycock]] 
|0|1|0|1|0|X|X|X| |2
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|ON}} [[Glenn Howard]]
|0|0|1|1|0|1|0|X| |3
| team2 = {{flagicon|AB}} [[Brendan Bottcher]] {{X}}
|0|2|0|0|1|0|2|X| |5
}}

=====Draw 12=====
''Friday, September 11, 16:00 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|NL}} [[Brad Gushue]] {{X}}
|3|0|3|0|2|1|X|X| |9
| team2 = {{flagicon|SWE}} [[Niklas Edin]]
|0|2|0|3|0|0|X|X| |5
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|AB}} [[Kevin Koe]] {{X}}
|2|0|2|1|0|0|0|0| |5
| team2 = {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]]
|0|1|0|0|2|1|0|0| |4
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|USA}} [[John Shuster]] {{X}}
|2|0|0|2|0|1|1|X| |6
| team2 = {{flagicon|SUI}} [[Peter de Cruz]]
|0|1|1|0|1|0|0|X| |3
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|MB}} [[Reid Carruthers]] {{X}}
|2|0|1|1|0|2|0|0| |6
| team2 = {{flagicon|SCO}} [[David Murdoch]]
|0|1|0|0|2|0|1|1| |5
}}

====Playoffs====
{{8TeamBracket-NoSeeds
| RD1=Quarterfinals
| RD2=Semifinals
| RD3=Final
| score-width=
| team-width=150px

| RD1-team1= {{flagicon|MB}} [[Reid Carruthers]]
| RD1-score1= 4
| RD1-team2= {{flagicon|USA}} '''[[John Shuster]]'''
| RD1-score2= '''6'''
| RD1-team3= {{flagicon|AB}} [[Brendan Bottcher]]
| RD1-score3= 6
| RD1-team4= {{flagicon|AB}} '''[[Kevin Koe]]'''
| RD1-score4= '''7'''
| RD1-team5= {{flagicon|NL}} '''[[Brad Gushue]]'''
| RD1-score5= '''4'''
| RD1-team6= {{flagicon|ON}} [[Glenn Howard]]
| RD1-score6= 2
| RD1-team7= {{flagicon|MB}} '''[[Mike McEwen (curler)|Mike McEwen]]'''
| RD1-score7= '''6'''
| RD1-team8= {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]]
| RD1-score8= 1

| RD2-team1= {{flagicon|USA}} [[John Shuster]]
| RD2-score1= 4
| RD2-team2= {{flagicon|AB}} '''[[Kevin Koe]]'''
| RD2-score2= '''9'''
| RD2-team3= {{flagicon|NL}} '''[[Brad Gushue]]'''
| RD2-score3= '''5'''
| RD2-team4= {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]]
| RD2-score4= 3

| RD3-team1= {{flagicon|AB}} '''[[Kevin Koe]]'''
| RD3-score1= '''4'''
| RD3-team2= {{flagicon|NL}} [[Brad Gushue]]
| RD3-score2= 3
}}

=====Quarterfinals=====
''Saturday, September 12, 11:00 am''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Reid Carruthers]] {{X}}
|2|0|0|0|1|0|1|0| |4
| team2 = {{flagicon|USA}} [[John Shuster]]
|0|0|0|2|0|3|0|1| |6
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|AB}} [[Brendan Bottcher]]
|0|1|1|2|0|2|0|0| |6
| team2 = {{flagicon|AB}} [[Kevin Koe]] {{X}}
|1|0|0|0|3|0|2|1| |7
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|NL}} [[Brad Gushue]] {{X}}
|0|0|1|0|0|2|0|1| |4
| team2 = {{flagicon|ON}} [[Glenn Howard]]
|0|0|0|1|0|0|1|0| |2
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]] {{X}}
|1|0|0|3|2|X|X|X| |6
| team2 = {{flagicon|ON}} [[Brad Jacobs (curler)|Brad Jacobs]]
|0|1|0|0|0|X|X|X| |1
}}

=====Semifinals=====
''Saturday, September 12, 7:00 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|USA}} [[John Shuster]]
|0|1|0|0|1|0|X|X| |2
| team2 = {{flagicon|AB}} [[Kevin Koe]] {{X}}
|1|0|3|1|0|4|X|X| |9
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|NL}} [[Brad Gushue]] {{X}}
|2|0|0|1|0|0|2|X| |5
| team2 = {{flagicon|MB}} [[Mike McEwen (curler)|Mike McEwen]]
|0|2|0|0|0|1|0|X| |3
}}

=====Final=====
''Sunday, September 13, 6:30 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|AB}} [[Kevin Koe]] {{X}}
|0|0|1|0|1|0|1|0|1|4
| team2 = {{flagicon|NL}} [[Brad Gushue]]
|0|1|0|0|0|1|0|1|0|3
}}

===Tier II===

====Round Robin Standings====
''Final Round Robin Standings''
{| class="wikitable" style="text-align: center;"
|-
!colspan=2|Key
|-
| style="background:#ffffcc; width:20px;"|
|align=left|Teams to Playoffs
|-
| style="background:#ccffcc; width:20px;"|
|align=left|Teams to Tiebreakers
|}
{| table
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool A !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Mark Kean]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|SCO}} [[Kyle Smith (curler)|Kyle Smith]] || 3 || 1
|- bgcolor=#ccffcc
| {{flagicon|NB}} [[James Grattan (curler)|James Grattan]] || 2 || 2
|-
| {{flagicon|ON}} [[Travis Fanset]] || 1 || 3
|-
| {{flagicon|NL}} [[Rick Rowsell]] || 0 || 4
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool B !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Greg Balsdon]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|NS}} [[Shawn Adams]] || 3 || 1
|- bgcolor=#ccffcc
| {{flagicon|SK}} [[Josh Heidt]] || 2 || 2
|-
| {{flagicon|NB}} [[Rene Comeau]] || 1 || 3
|-
| {{flagicon|NL}} [[Greg Smith (curler)|Greg Smith]] || 0 || 4
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool C !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|BC}} [[Jim Cotter (curler)|Jim Cotter]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|SCO}} [[Tom Brewster]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|NS}} [[Chad Stevens]] || 2|| 2
|-
| {{flagicon|NL}} [[Colin Thomas]] || 1 || 3
|-
| {{flagicon|NL}} [[Andrew Symonds (curler)|Andrew Symonds]] || 0 || 4
|}
|}

====Tiebreaker====
''Friday, September 11, 8:30 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|NB}} [[James Grattan (curler)|James Grattan]]
|0|0|3|0|3|1|X|X| |7
| team2 = {{flagicon|SK}} [[Josh Heidt]] {{X}}
|0|1|0|1|0|0|X|X| |2
}}

====Playoffs====
{{8TeamBracket-NoSeeds
| RD1=Quarterfinals
| RD2=Semifinals
| RD3=Final
| score-width=
| team-width=
| RD1-team1= {{flagicon|BC}} '''[[Jim Cotter (curler)|Jim Cotter]]'''
| RD1-score1= '''5'''
| RD1-team2= {{flagicon|NB}} [[James Grattan (curler)|James Grattan]] 
| RD1-score2= 4
| RD1-team3= {{flagicon|NS}} [[Shawn Adams]]
| RD1-score3= 2
| RD1-team4= {{flagicon|SCO}} '''[[Kyle Smith (curler)|Kyle Smith]] '''
| RD1-score4= '''9'''
| RD1-team5= {{flagicon|ON}} '''[[Mark Kean]]'''
| RD1-score5= '''9'''
| RD1-team6= {{flagicon|NS}} [[Chad Stevens]]
| RD1-score6= 7
| RD1-team7= {{flagicon|ON}} [[Greg Balsdon]]
| RD1-score7= 4
| RD1-team8= {{flagicon|SCO}} '''[[Tom Brewster]] '''
| RD1-score8= '''7'''

| RD2-team1= {{flagicon|BC}} '''[[Jim Cotter (curler)|Jim Cotter]]'''
| RD2-score1= '''9'''
| RD2-team2= {{flagicon|SCO}} [[Kyle Smith (curler)|Kyle Smith]] 
| RD2-score2= 2
| RD2-team3= {{flagicon|ON}} '''[[Mark Kean]]'''
| RD2-score3= '''6'''
| RD2-team4= {{flagicon|SCO}} [[Tom Brewster]] 
| RD2-score4= 4

| RD3-team1= {{flagicon|BC}} '''[[Jim Cotter (curler)|Jim Cotter]]'''
| RD3-score1= '''5'''
| RD3-team2= {{flagicon|ON}} [[Mark Kean]] 
| RD3-score2= 3
}}

=====Quarterfinals=====
''Saturday, September 12, 11:30 am''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|BC}} [[Jim Cotter (curler)|Jim Cotter]] {{X}}
|0|1|0|1|0|1|0|2| |5
| team2 = {{flagicon|NB}} [[James Grattan (curler)|James Grattan]] 
|0|0|1|0|1|0|2|0| |4
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|NS}} [[Shawn Adams]] {{X}}
|0|0|1|1|0|0|X|X| |2
| team2 = {{flagicon|SCO}} [[Kyle Smith (curler)|Kyle Smith]] 
|2|0|0|0|5|2|X|X| |9
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Mark Kean]] {{X}}
|3|0|1|0|2|0|0|3| |9
| team2 = {{flagicon|NS}} [[Chad Stevens]]
|0|1|0|2|0|3|1|0| |7
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Greg Balsdon]] {{X}}
|1|0|2|0|0|1|0|X| |4
| team2 = {{flagicon|SCO}} [[Tom Brewster]]
|0|2|0|1|3|0|1|X| |7
}}

=====Semifinals=====
''Saturday, September 12, 7:00 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|BC}} [[Jim Cotter (curler)|Jim Cotter]] {{X}}
|2|0|3|0|4|X|X|X| |9
| team2 = {{flagicon|SCO}} [[Kyle Smith (curler)|Kyle Smith]] 
|0|1|0|1|0|X|X|X| |2
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Mark Kean]] {{X}}
|2|0|1|0|1|1|0|1| |6
| team2 = {{flagicon|SCO}} [[Tom Brewster]]
|0|2|0|0|0|0|2|0| |4
}}

=====Final=====
''Sunday, September 13, 6:00 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|BC}} [[Jim Cotter (curler)|Jim Cotter]] {{X}}
|1|1|1|0|0|1|0|1| |5
| team2 = {{flagicon|ON}} [[Mark Kean]]
|0|0|0|1|1|0|1|0| |3
}}

==Women==

===Tier I===

====Teams====
The teams are listed as follows:<ref name="schedule"/><ref name="wteams">{{cite web|url=http://www.worldcurl.com/events.php?eventid=3825&view=Teams|title=2015 GSOC Tour Challenge – Women's teams|publisher=[[World Curling Tour]]|accessdate=2 August 2015}}</ref>
{| class=wikitable
|-
! Skip !! Third !! Second !! Lead !! Locale
|-
| [[Chelsea Carey]] || [[Amy Nixon]] || [[Jocelyn Peterman]] || [[Laine Peters]] || {{flagicon|AB}} [[Edmonton]], [[Alberta]]
|-
| [[Binia Feltscher]] || [[Irene Schori]] || [[Franziska Kaufmann]] || [[Cristine Urech]] || {{flagicon|SUI}} [[Flims]], [[Switzerland]]
|-
| [[Tracy Fleury]] || [[Crystal Webster]] || [[Jenn Horgan]] || [[Amanda Gates]] || {{flagicon|ON}} [[Greater Sudbury|Sudbury]], [[Ontario]] 
|-
| [[Julie Hastings]] || [[Christy Trombley]] || [[Stacey Smith (curler)|Stacey Smith]] || [[Katrina Collins]] || {{flagicon|Ontario}} [[Thornhill, Ontario|Thornhill]], [[Ontario]]
|-
| [[Rachel Homan]] || [[Emma Miskew]] || [[Joanne Courtney]] || [[Lisa Weagle]] || {{flagicon|ON}} [[Ottawa]], [[Ontario]]
|-
| [[Jennifer Jones (curler)|Jennifer Jones]] || [[Kaitlyn Lawes]] || [[Jill Officer]] || [[Jennifer Clark-Rouire]] || {{flagicon|MB}} [[Winnipeg]], [[Manitoba]]
|-
| [[Kim Eun-jung (curler)|Kim Eun-jung]] || [[Kim Kyeong-ae (curler)|Kim Kyeong-ae]] || [[Kim Seon-yeong (curler)|Kim Seon-yeong]] || [[Kim Yeong-mi]] || {{flagicon|KOR}} [[Uiseong]], [[South Korea]] 
|-
| [[Kristy McDonald]] || [[Kate Cameron (curler)|Kate Cameron]] || [[Leslie Wilson-Westscott]] || [[Raunora Westscott]] || {{flagicon|MB}} [[Winnipeg]], [[Manitoba]]
|-
| [[Sherry Middaugh]] || [[Jo-Ann Rizzo]] || [[Lee Merklinger]] || [[Leigh Armstrong]] || {{flagicon|ON}} [[Coldwater, Ontario|Coldwater]], [[Ontario]]
|-
| [[Eve Muirhead]] || [[Anna Sloan]] || [[Vicki Adams]] || [[Sarah Reid (curler)|Sarah Reid]] || {{flagicon|SCO}} [[Stirling]], [[Scotland]]
|-
| [[Alina Pätz]] || [[Nadine Lehmann]] || [[Marisa Winkelhausen]] || [[Nicole Schwägli]] || {{flagicon|SUI}} [[Zurich]], [[Switzerland]]
|-
| [[Kelsey Rocque]] || [[Laura Crocker]] || [[Taylor McDonald]] || [[Jen Gates]] || {{flagicon|AB}} [[Edmonton]], [[Alberta]]
|-
| [[Anna Sidorova]] || [[Margarita Fomina]] || [[Aleksandra Saitova]] || [[Alina Kovaleva]] || {{flagicon|RUS}} [[Moscow]], [[Russia]]
|-
| [[Valerie Sweeting]] || [[Lori Olson-Johns]] || [[Dana Ferguson]] || [[Rachelle Brown]] || {{flagicon|AB}} [[Edmonton]], [[Alberta]]
|-
| [[Silvana Tirinzoni]] || [[Manuela Siegrist]] || [[Esther Neuenschwander]] || [[Marlene Albrecht]] || {{flagicon|SUI}} [[Aarau]], [[Switzerland]]
|}

====Round Robin Standings====
''Final Round Robin Standings''
{| class="wikitable" style="text-align: center;"
|-
!colspan=2|Key
|-
| style="background:#ffffcc; width:20px;"|
|align=left|Teams to Playoffs
|-
| style="background:#ccffcc; width:20px;"|
|align=left|Teams to Tiebreakers
|}
{| table
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool A !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Sherry Middaugh]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|MB}} [[Kristy McDonald]] || 3 || 1
|- bgcolor=#ccffcc
| {{flagicon|MB}} [[Jennifer Jones (curler)|Jennifer Jones]] || 2 || 2
|- bgcolor=#ccffcc
| {{flagicon|AB}} [[Chelsea Carey]] || 2 || 2
|-
| {{flagicon|RUS}} [[Anna Sidorova]] || 0 || 4
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool B !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Rachel Homan]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|AB}} [[Kelsey Rocque]] || 3 || 1
|-
| {{flagicon|ON}} [[Julie Hastings]] || 1 || 3
|-
| {{flagicon|SUI}} [[Binia Feltscher]] || 1 || 3
|-
| {{flagicon|AB}} [[Valerie Sweeting]] || 1 || 3
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=150| Pool C !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]] || 3 || 1
|- bgcolor=#ffffcc
| {{flagicon|SUI}} [[Silvana Tirinzoni]] || 2 || 2
|- bgcolor=#ccffcc
| {{flagicon|SCO}} [[Eve Muirhead]] || 2 || 2
|- bgcolor=#ccffcc
| {{flagicon|ON}} [[Tracy Fleury]] || 2 || 2
|-
| {{flagicon|SUI}} [[Alina Pätz]] || 1 || 3
|}
|}

====Round Robin Results====
The draw is listed as follows:<ref name="schedule"/>

=====Draw 1=====
''Tuesday, September 8, 7:00 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|SCO}} [[Eve Muirhead]]
|1|0|1|1|0|2|0|0| |5
| team2 = {{flagicon|SUI}} [[Silvana Tirinzoni]] {{X}}
|0|0|0|0|1|0|2|1| |4
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|MB}} [[Jennifer Jones (curler)|Jennifer Jones]]
|0|0|1|0|1|1|0|1|0|4
| team2 = {{flagicon|ON}} [[Sherry Middaugh]] {{X}}
|0|2|0|2|0|0|0|0|1|5
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|AB}} [[Valerie Sweeting]] {{X}}
|0|1|0|0|3|0|X|X| |4
| team2 = {{flagicon|AB}} [[Kelsey Rocque]]
|1|0|4|1|0|3|X|X| |9
}}

=====Draw 2=====
''Wednesday, September 9, 9:00 am''
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|ON}} [[Tracy Fleury]]
|0|4|0|2|0|0|1|X| |7
| team2 = {{flagicon|SUI}} [[Alina Pätz]] {{X}}
|4|0|2|0|2|3|0|X| |11
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|ON}} [[Rachel Homan]]
|0|3|0|2|0|0|0|1|1|7
| team2 = {{flagicon|ON}} [[Julie Hastings]] {{X}}
|2|0|2|0|1|0|1|0|0|6
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|AB}} [[Chelsea Carey]] {{X}}
|0|2|0|0|3|0|0|1| |6
| team2 = {{flagicon|RUS}} [[Anna Sidorova]]
|0|0|1|1|0|0|1|0| |3
}}

=====Draw 3=====
''Wednesday, September 9, 12:30 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|AB}} [[Valerie Sweeting]]
|0|3|0|0|2|0|0|2| |7
| team2 = {{flagicon|SUI}} [[Binia Feltscher]]
|2|0|3|1|0|3|1|0| |10
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|MB}} [[Jennifer Jones (curler)|Jennifer Jones]]
|0|0|4|0|0|1|0|0|0|5
| team2 = {{flagicon|MB}} [[Kristy McDonald]] {{X}}
|0|1|0|1|2|0|0|1|1|6
}}

=====Draw 4=====
''Wednesday, September 9, 4:00 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|SCO}} [[Eve Muirhead]] {{X}}
|2|0|1|0|0|2|0|1|0|6
| team2 = {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]]
|0|1|0|2|1|0|2|0|2|8
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|AB}} [[Kelsey Rocque]] {{X}}
|0|3|3|2|0|1|X|X| |9
| team2 = {{flagicon|ON}} [[Julie Hastings]]
|1|0|0|0|1|0|X|X| |2
}}

=====Draw 5=====
''Wednesday, September 9, 7:30 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|SUI}} [[Alina Pätz]]
|0|0|1|0|0|0|X|X| |1
| team2 = {{flagicon|SUI}} [[Silvana Tirinzoni]] {{X}}
|1|1|0|3|1|1|X|X| |7
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|ON}} [[Sherry Middaugh]]
|0|0|0|1|0|2|1|X| |4
| team2 = {{flagicon|RUS}} [[Anna Sidorova]] {{X}}
|0|1|0|0|0|0|0|X| |1
}}

=====Draw 6=====
''Thursday, September 10, 9:00 am''
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|AB}} [[Chelsea Carey]]
|1|0|0|2|1|0|1|X| |5
| team2 = {{flagicon|MB}} [[Kristy McDonald]]
|0|0|1|0|0|1|0|X| |2
}}

=====Draw 7=====
''Thursday, September 10, 12:30 pm''
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|ON}} [[Tracy Fleury]]
|1|0|1|0|3|0|2|0|1|8
| team2 = {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]] {{X}}
|0|1|0|2|0|1|0|3|0|7
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|ON}} [[Rachel Homan]] {{X}}
|0|1|2|0|3|0|2|X| |8
| team2 = {{flagicon|SUI}} [[Binia Feltscher]]
|0|0|0|2|0|1|0|X| |3
}}

=====Draw 8=====
''Thursday, September 10, 4:00 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|MB}} [[Jennifer Jones (curler)|Jennifer Jones]]
|0|2|2|0|1|1|1|X| |7
| team2 = {{flagicon|RUS}} [[Anna Sidorova]] {{X}}
|2|0|0|1|0|0|0|X| |3
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|SCO}} [[Eve Muirhead]] {{X}}
|1|0|1|0|5|X|X|X| |7
| team2 = {{flagicon|SUI}} [[Alina Pätz]]
|0|0|0|1|0|X|X|X| |1
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|AB}} [[Valerie Sweeting]]
|1|3|0|3|0|1|X|X| |8
| team2 = {{flagicon|ON}} [[Julie Hastings]] {{X}}
|0|0|1|0|2|0|X|X| |3
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|ON}} [[Sherry Middaugh]] {{X}}
|0|1|0|2|0|1|1|0| |5
| team2 = {{flagicon|AB}} [[Chelsea Carey]]
|0|0|2|0|1|0|0|1| |4
}}

=====Draw 9=====
''Thursday, September 10, 7:30 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|AB}} [[Kelsey Rocque]]
|0|0|0|0|X|X|X|X| |0
| team2 = {{flagicon|ON}} [[Rachel Homan]] {{X}}
|1|2|1|3|X|X|X|X| |7
}}
{{Curlingbox8
| sheet = E
| team1 = {{flagicon|SUI}} [[Silvana Tirinzoni]]
|0|3|0|3|0|0|0|1|2|9
| team2 = {{flagicon|ON}} [[Tracy Fleury]] {{X}}
|2|0|2|0|1|1|1|0|0|7
}}

=====Draw 10=====
''Friday, September 11, 9:00 am''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|AB}} [[Kelsey Rocque]]
|0|0|2|0|3|1|0|X| |6
| team2 = {{flagicon|SUI}} [[Binia Feltscher]] {{X}}
|1|1|0|1|0|0|1|X| |4
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|SUI}} [[Silvana Tirinzoni]]
|0|2|0|2|0|0|1|X| |5
| team2 = {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]] {{X}}
|3|0|1|0|2|3|0|X| |9
}}

=====Draw 11=====
''Friday, September 11, 12:30 pm''
{{Curlingbox8
| sheet = A
| team1 = {{flagicon|ON}} [[Sherry Middaugh]]
|0|1|0|1|0|2|1|0| |5
| team2 = {{flagicon|MB}} [[Kristy McDonald]] {{X}}
|2|0|2|0|2|0|0|1| |7
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|MB}} [[Jennifer Jones (curler)|Jennifer Jones]]
|2|0|1|0|0|2|0|2| |7
| team2 = {{flagicon|AB}} [[Chelsea Carey]] {{X}}
|0|1|0|1|1|0|3|0| |6
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|ON}} [[Tracy Fleury]] {{X}}
|1|1|0|3|0|2|0|2| |9
| team2 = {{flagicon|SCO}} [[Eve Muirhead]]
|0|0|2|0|2|0|1|0| |5
}}

=====Draw 12=====
''Friday, September 11, 16:00 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|ON}} [[Julie Hastings]]
|0|2|2|0|0|1|2|0| |7
| team2 = {{flagicon|SUI}} [[Binia Feltscher]] {{X}}
|1|0|0|1|1|0|0|1| |4
}}

=====Draw 13=====
''Friday, September 11, 19:30 pm''
{{Curlingbox8
| sheet = B
| team1 = {{flagicon|RUS}} [[Anna Sidorova]] {{X}}
|0|1|0|0|0|1|0|1| |3
| team2 = {{flagicon|MB}} [[Kristy McDonald]]
|1|0|0|0|2|0|1|0| |4
}}
{{Curlingbox8
| sheet = C
| team1 = {{flagicon|AB}} [[Valerie Sweeting]]
|0|0|0|0|1|0|1|X| |2
| team2 = {{flagicon|ON}} [[Rachel Homan]] {{X}}
|0|2|1|0|0|3|0|X| |6
}}
{{Curlingbox8
| sheet = D
| team1 = {{flagicon|SUI}} [[Alina Pätz]]
|0|0|2|0|0|2|1|0| |5
| team2 = {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]] {{X}}
|0|2|0|2|3|0|0|1| |8
}}

====Tiebreakers====
''Saturday, September 12, 8:00 am''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Jennifer Jones (curler)|Jennifer Jones]]
|1|0|0|2|0|1|0|0| |4
| team2 = {{flagicon|SCO}} [[Eve Muirhead]] {{X}}
|0|2|0|0|1|0|1|1| |5
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|AB}} [[Chelsea Carey]] {{X}}
|1|0|1|0|2|0|0|0| |4
| team2 = {{flagicon|ON}} [[Tracy Fleury]]
|0|1|0|2|0|1|0|1| |5
}}

====Playoffs====
{{8TeamBracket-NoSeeds
| RD1=Quarterfinals
| RD2=Semifinals
| RD3=Final
| score-width=
| team-width=150px

| RD1-team1= {{flagicon|ON}} '''[[Rachel Homan]]'''
| RD1-score1= '''7'''
| RD1-team2= {{flagicon|SCO}} [[Eve Muirhead]]
| RD1-score2= 3
| RD1-team3= {{flagicon|KOR}} '''[[Kim Eun-jung (curler)|Kim Eun-jung]]'''
| RD1-score3= '''7'''
| RD1-team4= {{flagicon|MB}} [[Kristy McDonald]]
| RD1-score4= 2
| RD1-team5= {{flagicon|ON}} [[Sherry Middaugh]]
| RD1-score5= 4
| RD1-team6= {{flagicon|ON}} '''[[Tracy Fleury]]'''
| RD1-score6= '''6'''
| RD1-team7= {{flagicon|AB}} [[Kelsey Rocque]]
| RD1-score7= 1
| RD1-team8= {{flagicon|SUI}} '''[[Silvana Tirinzoni]]'''
| RD1-score8= '''6'''

| RD2-team1= {{flagicon|ON}} '''[[Rachel Homan]]'''
| RD2-score1= '''6'''
| RD2-team2= {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]]
| RD2-score2= 3
| RD2-team3= {{flagicon|ON}} [[Tracy Fleury]]
| RD2-score3= 7
| RD2-team4= {{flagicon|SUI}} '''[[Silvana Tirinzoni]]'''
| RD2-score4= '''9'''

| RD3-team1= {{flagicon|ON}} [[Rachel Homan]]
| RD3-score1= 5
| RD3-team2= {{flagicon|SUI}} '''[[Silvana Tirinzoni]]'''
| RD3-score2= '''6'''
}}

=====Quarterfinals=====
''Saturday, September 12, 2:30 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Rachel Homan]] {{X}}
|0|3|0|1|0|2|0|1| |7
| team2 = {{flagicon|SCO}} [[Eve Muirhead]]
|0|0|1|0|1|0|1|0| |3
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]]
|0|2|0|3|0|1|1|X| |7
| team2 = {{flagicon|MB}} [[Kristy McDonald]] {{X}}
|1|0|1|0|0|0|0|X| |2
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Sherry Middaugh]] {{X}}
|0|0|0|2|1|0|1|0| |4
| team2 = {{flagicon|ON}} [[Tracy Fleury]]
|0|2|1|0|0|2|0|1| |6
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|AB}} [[Kelsey Rocque]] {{X}}
|0|0|0|0|1|0|0|X| |1
| team2 = {{flagicon|SUI}} [[Silvana Tirinzoni]]
|0|2|1|1|0|1|1|X| |6
}}

=====Semifinals=====
''Saturday, September 12, 5:30 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Rachel Homan]] {{X}}
|1|0|2|1|0|2|0|X| |6
| team2 = {{flagicon|KOR}} [[Kim Eun-jung (curler)|Kim Eun-jung]]
|0|1|0|0|1|0|1|X| |3
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Tracy Fleury]]
|0|3|0|0|1|0|3|0| |7
| team2 = {{flagicon|SUI}} [[Silvana Tirinzoni]] {{X}}
|3|0|2|1|0|2|0|1| |9
}}

=====Final=====
''Sunday, September 13, 11:30 am''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|ON}} [[Rachel Homan]] {{X}}
|0|3|0|1|0|1|0|0| |5
| team2 = {{flagicon|SUI}} [[Silvana Tirinzoni]]
|0|0|1|0|1|0|2|2| |6
}}

===Tier II===

====Round Robin Standings====
''Final Round Robin Standings''
{| class="wikitable" style="text-align: center;"
|-
!colspan=2|Key
|-
| style="background:#ffffcc; width:20px;"|
|align=left|Teams to Playoffs
|-
| style="background:#ccffcc; width:20px;"|
|align=left|Teams to Tiebreakers
|}
{| table
|valign=top width=10%|
{| class=wikitable
|- 
!width=200| Pool A !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|PE}} [[Suzanne Birt]] || 2 || 2
|- bgcolor=#ffffcc
| {{flagicon|NB}} [[Sylvie Robichaud]] || 2 || 2
|- bgcolor=#ccffcc
| {{flagicon|MB}} [[Cathy Overton-Clapham]] || 2 || 2
|- bgcolor=#ccffcc
| {{flagicon|USA}} [[Nina Roth]] || 2 || 2
|- bgcolor=#ccffcc
| {{flagicon|MB}} [[Michelle Montford]] || 2 || 2
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=200| Pool B !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|MB}} [[Kerri Einarson]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|ON}} [[Jill Thurston]] || 3 || 1
|-
| {{flagicon|SWE}} [[Anna Hasselborg]] || 1 || 3
|-
| {{flagicon|NS}} [[Mary-Anne Arsenault]] || 1 || 3
|-
| {{flagicon|NL}} [[Heather Strong]] || 1 || 3
|}
|valign=top width=10%|
{| class=wikitable
|-
!width=200| Pool C !!width=15| W !!width=15| L
|- bgcolor=#ffffcc
| {{flagicon|MB}} [[Barb Spencer]] || 4 || 0
|- bgcolor=#ffffcc
| {{flagicon|USA}} [[Jamie Sinclair]] || 3 || 1
|- bgcolor=#ccffcc
| {{flagicon|SK}} [[Amber Holland]] || 2 || 2
|-
| {{flagicon|NL}} [[Stacie Curtis]] || 1 || 3
|-
| {{flagicon|SWE}} [[Cissi Östlund]] || 0 || 4
|}
|}

====Tiebreakers====
''Saturday, September 12, 8:30 am''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Cathy Overton-Clapham]]
|1|0|0|1|1|0|0|X| |3
| team2 = {{flagicon|MB}} [[Michelle Montford]]
|0|3|3|0|0|1|1|X| |8
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|SK}} [[Amber Holland]]
|1|0|1|0|3|3|0|X| |8
| team2 = {{flagicon|USA}} [[Nina Roth]]
|0|2|0|1|0|0|1|X| |4
}}

====Playoffs====
{{8TeamBracket-NoSeeds
| RD1=Quarterfinals
| RD2=Semifinals
| RD3=Final
| score-width=
| team-width=
| RD1-team1= {{flagicon|MB}} '''[[Kerri Einarson]]
| RD1-score1= '''8
| RD1-team2= {{flagicon|MB}} [[Michelle Montford]]
| RD1-score2= 1 
| RD1-team3= {{flagicon|MB}} '''[[Jill Thurston]]
| RD1-score3= '''8
| RD1-team4= {{flagicon|PE}} [[Suzanne Birt]]
| RD1-score4= 5
| RD1-team5= {{flagicon|MB}} [[Barb Spencer]]
| RD1-score5= 3
| RD1-team6= {{flagicon|SK}} '''[[Amber Holland]]
| RD1-score6= '''9
| RD1-team7={{flagicon|USA}} '''[[Jamie Sinclair]]
| RD1-score7= '''5
| RD1-team8={{flagicon|NB}} [[Sylvie Robichaud]]
| RD1-score8= 4
| RD2-team1={{flagicon|MB}} '''[[Kerri Einarson]]
| RD2-score1= '''7
| RD2-team2={{flagicon|MB}} [[Jill Thurston]]
| RD2-score2= 6
| RD2-team3={{flagicon|SK}} '''[[Amber Holland]]
| RD2-score3= '''7
| RD2-team4={{flagicon|USA}} [[Jamie Sinclair]]
| RD2-score4= 4
| RD3-team1={{flagicon|MB}} '''[[Kerri Einarson]]
| RD3-score1= '''8
| RD3-team2={{flagicon|SK}} [[Amber Holland]]
| RD3-score2= 2 
}}

=====Quarterfinals=====
''Saturday, September 12, 3:00 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Kerri Einarson]] {{X}}
|2|0|3|1|2|X|X|X| |8
| team2 = {{flagicon|MB}} [[Michelle Montford]]
|0|1|0|0|0|X|X|X| |1
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Jill Thurston]] {{X}}
|3|0|1|0|1|0|3|X| |8
| team2 = {{flagicon|PE}} [[Suzanne Birt]]
|0|1|0|2|0|2|0|X| |5
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Barb Spencer]] {{X}}
|0|0|0|1|1|0|1|0| |3
| team2 = {{flagicon|SK}} [[Amber Holland]]
|0|0|1|0|0|3|0|5| |9
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|USA}} [[Jamie Sinclair]] {{X}}
|1|1|0|0|2|0|0|1| |5
| team2 = {{flagicon|NB}} [[Sylvie Robichaud]]
|0|0|0|3|0|0|1|0| |4
}}

=====Semifinals=====
''Saturday, September 12, 7:00 pm''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Kerri Einarson]] {{X}}
|2|0|2|0|0|0|0|2|1 |7
| team2 = {{flagicon|MB}} [[Jill Thurston]] 
|0|1|0|2|2|0|1|0|0 |6
}}
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|SK}} [[Amber Holland]] {{X}}
|1|0|1|2|1|0|0|2| |7
| team2 = {{flagicon|USA}} [[Jamie Sinclair]]
|0|2|0|0|0|1|1|0| |4
}}

=====Final=====
''Sunday, September 13, 11:30 am''
{{Curlingbox8
| sheet = 
| team1 = {{flagicon|MB}} [[Kerri Einarson]] {{X}}
|2|1|0|1|0|3|1|X| |8
| team2 = {{flagicon|SK}} [[Amber Holland]]
|0|0|1|0|1|0|0|X| |2
}}

==References==
{{reflist}}

==External links==
*{{official website|http://www.thegrandslamofcurling.com/curling/tour-challenge}}
{{WCTevent|3826|M}}
{{WCTevent|3825|W}}

{{2015–16 curling season}}

[[Category:2015 in Canadian curling]]
[[Category:Curling in Newfoundland and Labrador]]