{{coord|41.2506211|-77.8369352|type:river_globe:earth_region:US-PA|display=title}}
'''Dennison Fork''' is a stream located northwest of [[Lock Haven, Pennsylvania|Lock Haven]], about 6.9 miles from [[Renovo, Pennsylvania|Renovo]] in [[Noyes Township, Clinton County, Pennsylvania]], in the [[United States]].<ref>http://www.hookandbullet.com/fishing-dennison-fork-renovo-pa/</ref> The stream is located south east of Cooks Run.<ref>http://www.hookandbullet.com/fishing-dennison-fork-renovo-pa/</ref> The Dennison Fork branches off from the Fish Dam Run stream, which branches off from the south side of the [[West Branch Susquehanna River]].

The Dennison Fork is located along the [[Chuck Keiper Trail]] within the [[Sproul State Forest]].<ref>http://www.dcnr.state.pa.us/forestry/recreation/hiking/stateforesttrails/chuckkeipertrail/index.htm</ref>

On May 31, 1985, a tornado ripped through North Central Pennsylvania. As a result, over 8,000 acres of forest land in the Sproul State Forest was flattened. The Dennison Fork Trail, located on the [[Chuck Keiper Trail]] was damaged severely. As a result, the trail in this area has been relocated.<ref>http://www.dcnr.state.pa.us/forestry/recreation/hiking/stateforesttrails/chuckkeipertrail/index.htm</ref> The only access to the Chuck Keiper Trail is via [[Pennsylvania Route 144]], through the vastly underpopulated and lonely plateau between [[Renovo]] and [[Snow Shoe, Pennsylvania]].

The Dennison Fork was most likely named after the Dennison family that settled and resided in [[Clinton County, Pennsylvania]] as early as the 1840s. The Dennison family moved between Leidy, Keating and Noyes Townships in Clinton County, but maintained possession of a parcel of land on the banks of the [[West Branch Susquehanna River]] in Noyes Township, known as the Dennison tract.<ref>http://www.clintoncogensociety.org/cemeteries/139.htm</ref>

==See also==
*[[List of rivers of Pennsylvania]]
*[[Chuck Keiper Trail]]

==References==
{{reflist}}

*http://www.hookandbullet.com/fishing-dennison-fork-renovo-pa/
*http://us.geoview.info/dennison_fork,5186745
*http://cartographic.info/usa/map.php?id=1173157
*https://www.topoquest.com/place-detail.php?id=1173157
*http://www.dcnr.state.pa.us/forestry/recreation/hiking/stateforesttrails/chuckkeipertrail/index.htm



[[Category:Rivers of Clinton County, Pennsylvania]]