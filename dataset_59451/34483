{{good article}}
{{infobox military unit
|unit_name= 1st Split Partisan Detachment
|image= 
|caption=
|dates=11–14 August 1941 
|country= 
|allegiance= [[Yugoslav Partisans]]
|branch= 
|type=
|role= 
|size= Three [[platoon]]s (planned)
|command_structure=
|garrison=
|garrison_label=
|nickname=
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|equipment=
|equipment_label=
|battles=[[World War II in Yugoslavia]]
|anniversaries=
|decorations=
|battle_honours=
|battle_honours_label=
|disbanded=
|flying_hours=
|website=
<!-- Commanders -->
|commander1= 
|commander1_label= [[Mirko Kovačević]]
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|commander4=
|commander4_label=
|notable_commanders=Đordano Borovčić-Kurir
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=
|identification_symbol_2_label=
|identification_symbol_3=
|identification_symbol_3_label=
|identification_symbol_4=
|identification_symbol_4_label=
}}

The '''1st Split Partisan Detachment''' ({{lang-sh-Latn|Prvi splitski partizanski odred}}) or the '''1st Split Detachment''' ({{lang-sh-Latn|Prvi splitski odred|links=no}}) was a short-lived unit of the [[Yugoslav Partisans]] during [[World War II]]. It was composed of volunteers from the city of [[Split, Croatia|Split]] and was created in August 1941, just four months after the [[Axis powers|Axis]] [[World War II in Yugoslavia|occupation of Yugoslavia]], and the [[annexation]] of Split and most of [[Dalmatia]] by the [[Kingdom of Italy]]. The unit, composed mostly of young men with little or no fighting experience, planned to relocate to the [[Dinara]] mountains to join other Partisan units in fighting the Axis powers.

After initial organizational problems, the weakened detachment reached an area near the village of [[Košute]] where they were engaged by [[Ustaše Militia]] backed by Italian reinforcements. After a day of fighting and the death of one of their commanders, members of the detachment began to retreat. In the end, the Partisans suffered four [[killed in action]] and 25 taken prisoner, while 13 managed to escape. All but three of the prisoners were later executed by firing squad. A Split Detachment was reformed, but most of its fighters were part of the 3rd Dalmatian Assault Brigade.

==Background==
[[File:Bundesarchiv B 145 Bild-F016229-0008, Italienisches Kriegsschiff im Hafen von Split.jpg|thumb|right|250px|An Italian ship in the [[Port of Split]] following the city's occupation.]]
The [[Axis powers]] began their [[Invasion of Yugoslavia|invasion]] of the [[Kingdom of Yugoslavia]] on 6 April 1941. Just four days later, a [[puppet state]] known as the [[Independent State of Croatia]] ({{lang-sh-Latn|Nezavisna Država Hrvatska}} – NDH) was declared, encompassing most of modern-day [[Croatia]], [[Bosnia and Herzegovina]] and parts of modern-day [[Serbia]]. In accordance with the [[Treaties of Rome (1941)|Treaties of Rome]] signed by the [[Ustaše]] leadership of the NDH, a large part of the Croatian coastline and islands, including the cities of [[Split, Croatia|Split]] and [[Rijeka]], were incorporated into the [[Kingdom of Italy]]. The low morale of Yugoslav troops in Split and the uncontested advance of the Italian Army through Dalmatia resulted in a number of desertions. Paramilitary formations of the [[Croatian Peasant Party]] ({{lang-sh-Latn|Hrvatska Seljačka Stranka|links=no}}, HSS) disarmed soldiers. By 11 April, there were no organized military formations remaining in Split, and a large number of police and [[gendarmerie]] switched their allegiance to the NDH. With Split under the control of the new Ustaše government, the authorities apprehended 300 citizens they deemed to be political enemies. On 13 April, members of the [[League of Communist Youth of Yugoslavia]] ({{lang-sh-Latn|Savez komunističke omladine Jugoslavije|links=no}}, SKOJ) broke into a number of Ustaše weapons depots, stealing dozens of rifles and machine guns, as well as ammunition and hand grenades.{{sfn|Leontić|1960|p=42}}

On the evening of 15 April, the first troops of the Italian Army entered Split, signifying the start of their occupation. On 21 April, civil control of Split passed from the NDH to Italian authorities, followed by the raising of the Italian flag over the city. On 18 May, Italy and the NDH signed the Treaties of Rome, confirming Italian rule of Dalmatia, including Split.{{sfn|Leontić|1960|p=43}}

==History==

===Formation===
On 7 August 1941, Pavle Pap-Šilja and Mirko Kovačević-Lala arrived in Split with instructions from the Central Committee of the [[Communist Party of Croatia]] ({{lang-sh-Latn|Centralni komitet komunističke partije Hrvatske|links=no}}) to discuss the forming of [[Yugoslav Partisan|Partisan]] detachments to fight the Axis occupiers. In a meeting with the members of the Regional Committee, they agreed that the new detachments should operate in the [[Dinara]] mountains where their fighters could rely on support from the population of the [[Sinj]] and [[Livanjsko field]] regions as well as other Partisan formations planned for [[Bosanska Krajina]] and [[Lika]]. This plan meant that the detachments would have to be formed and then relocated to the Dinara mountains. Relocation meant crossing the rough terrain of [[Zagora (Croatia)|Zagora]], whose population did not support the Partisans. Despite these difficulties, it was decided that in the next two days the Regional Committee would form seven detachments, including one from Split.{{sfn|Leontić|1960|p=51}}{{sfn|Gizdić|1957|p=242}}

According to the initial plan, on the night of 11 August, the Split Detachment was to move just outside the town and collect weapons that had been captured on 13 April. They would then link up with the Solin Detachment and continue towards [[Kamešnica, Koprivnica-Križevci County|Kamešnica]]. From there, they would travel to [[Otok, Split-Dalmatia County|Otok]] to meet with the Sinj Detachment. On 8 August, a meeting of Communist Party members and sympathisers was held in a field house between Split and [[Stobreč]] to ask for volunteers to join the Split Detachment. The detachment was formed on 11 August, and consisted of 66 members organized in three [[platoon]]s. The detachment departed Split on the same day, under the command of Đordano Borovčić-Kurir with  [[commissar]] Alfred Santini, and accompanied by Kovačević-Lala who was in overall command of all Partisan detachments in Dalmatia.{{sfn|Gizdić|1957|pp=247–250}}{{sfn|Kovač|Vojnović|1976|pp=367–372}}{{sfn|Kvesić|1960|pp=135–145}}

===Ambush and aftermath===
The plan went awry when the 2nd Platoon got lost and missed a rendezvous with the 1st and 3rd Platoon, which were waiting for them near [[Mravince]]. The 2nd Platoon was therefore disbanded while the rest of the detachment continued via [[Mosor]] to [[Dicmo]] where they were to meet up with their guides. They arrived on the night of 12 August, and upon realizing their guides were not there, decided to continue alone towards Kamešnica, but got lost and ran out of supplies during the night. At dawn on 13 August the detachment camped near the village of Krušvar. In the evening they once again continued towards Kamešnica, and at dawn on 14 August, they camped near the village of Košute, near [[Trilj]]. Borovčić-Kurir tasked two fighters to go into the village and ask the locals for water and directions. After talking to some locals, the fighters were fired upon by a member of the Ustaše Militia of the village. They returned fire then continued back to their camp and notified the others about the incident.{{sfn|Kvesić|1960|pp=135–145}}{{sfn|Kovač|Vojnović|1976|pp=367–372}}

Just as they finished their report, the detachment was fired on by Ustaše Militia. The Ustaše called for help from Sinj, and by 18:00, Italian reinforcements had arrived and began encircling the detachment, engaging them with [[mortar (weapon)|mortars]] and light [[artillery]]. Outnumbered and facing an enemy with greater firepower, the Partisans planned to hold their positions until evening then retreat using the cover of darkness. About 20:00, Kovačević-Lala, one of the most experienced members of the detachment, was killed after being hit by a mortar round, which had a significant effect on the morale of the remaining fighters. Borovčić-Kurir issued the order to retreat, but it turned into chaos as individuals fled the area.{{sfn|Kvesić|1960|pp=135–145}}{{sfn|Kovač|Vojnović|1976|pp=367–372}}

Four Partisans died in the fighting, and three more were summarily shot after capture. Twenty-five were captured and taken prisoner, while 13 managed to escape. The 25 that were captured were moved to Sinj, where one of the prisoners was beaten to death. The rest were placed on trial before a special court that was sent from [[Mostar]]. Three prisoners were acquitted while the remaining 21 were taken to Ruduša near Sinj, and executed by [[firing squad]].{{sfn|Kovač|Vojnović|1976|pp=367–372}}{{sfn|Kvesić|1960|pp=135–145}} According to the Yugoslav historian Mate Šalov, a Split Detachment was still active at the time of the [[Italian Armistice]] in September 1943, but only consisted of the detachment staff and a few fighters, with the Dinara Battalion forming part of the 3rd Dalmatian Assault Brigade.{{sfn|Šalov|1980|p=17}}

==Commemoration==
After the war, a [[State school|public school]] was named after Borovčić-Kurir. In 1962, the 1st Split Partisan Detachment was commemorated with a {{convert|15|m|ftin|abbr=off|lk=in}} tall monument designed by Vuko Bombardelli, which was erected in Ruduša where the captured members of the detachment were executed. The monument was destroyed by an explosion in August 1992, during the [[Croatian War of Independence]].{{sfn|Zaboravljena strijeljana splitska dica}} It was renovated in 2009 with financial aid from the owners of the [[RNK Split]] football club, as several members of the detachment had played for the club before the war.{{sfn|Ruduša: Braća Žužul obnovila partizanski spomenik}} In 1981, a monument dedicated to the detachment was placed in Vinkovačka street in Split. In July 2013, this monument was vandalized by unknown perpetrators who spray painted symbols of the Ustaše, [[swastika]]s, and the initials of the [[Croatian Civic Party]] (''Hrvatska građanska stranka''; HGS) and the [[Croatian Pure Party of Rights]] (''Hrvatska čista stranka prava''; HČSP). This was the second time the monument had been damaged.{{sfn|Opet oštećen spomenik splitskim partizanima}} A movie about the detachment, entitled ''[[Prvi splitski odred]]'' and directed by Vojdrag Berčić, was released in 1972.{{sfn|Prvi splitski odred}}

In August 2014, the Mayor of Split [[Ivo Baldasar]] announced he would propose that the Split city council name a street in the Mejaši neighbourhood after the detachment.{{sfn|Nakon otkrivanja spomenika HOS-u pod stijegom Rafaela Bobana, Baldasar vraća partizane u Split}} The initiative was met with opposition from the Mejaši district leaders, who stated that people living in Mejaši had a "different political orientation" and that such action would devalue the Croatian War of Independence and all of its values.{{sfn|Mejaši neće ulicu Prvog splitskog partizanskog odreda: 'To je drsko, mi smo potpuno drugačijeg svjetonazora!'}} The initiative was also met with opposition from the [[Croatian Democratic Union]] (''Hrvatska demokratska zajednica''; HDZ) members of the council who accused Baldasar of "forging history", because "the detachment was never Partisan, and its members didn't die under Partisan insignia." A HDZ council member explained that even during the existence of the [[Socialist Federal Republic of Yugoslavia]], the street that was named after the detachment did not have the word ''Partisan'' in it.{{sfn|Nakon otkrivanja spomenika HOS-u pod stijegom Rafaela Bobana, Baldasar vraća partizane u Split}} Members of the Organization of Anti-fascists and Anti-fascist Fighters of Split were also dissatisfied because the proposal was intended for a minor street at the edge of the city. Faced with this opposition, Baldasar withdrew his proposal during a council meeting on 25 August, explaining that he could not "allow arguments surrounding street names".{{sfn|Baldasar: Ne mogu dozvoliti svađu oko imena ulice}}

==Notes==
;Citations
{{reflist|2}}

==References==
;Books
{{refbegin|2}}
*{{cite book|ref=harv|last=Leontić|first=Boro|year=1960|title=Split 1941|url=https://books.google.com/books/about/Split_1941.html?id=zL0BAAAAMAAJ&redir_esc=y|location= [[Belgrade]]|publisher=Prosveta|oclc=23573206|accessdate=4 September 2014}}
*{{cite book|ref=harv|last=Kvesić|first=Sibe|year=1960|title=Dalmacija u Narodnooslobodilačkoj borbi|trans_title=Dalmatia in the People's Liberation War|url=http://katalog.kgz.hr/pagesResults/bibliografskiZapis.aspx?&currentPage=1&searchById=1&sort=0&fid0=4&fv0=Lykos&spid0=1&spv0=&selectedId=86003937|location=[[Zagreb]]|publisher=Lykos|oclc=22095267|accessdate=4 September 2014}}
*{{cite book|ref=harv|last1=Kovač|first1=Tatjana|last2=Vojnović|first2=Mijo|year=1976|title=U spomen revoluciji|trans_title=In Remembrance of the Revolution|url=https://books.google.com/books/about/U_spomen_revoluciji.html?id=f1OFAAAAIAAJ&redir_esc=y|publisher=Institut za historiju radničkog pokreta Dalmacije|location=[[Split, Croatia|Split]]|oclc=442814475|accessdate=4 September 2014}}
*{{cite book|ref=harv|last1=Gizdić|first1=Drago|year=1957|url=https://books.google.com/books/about/Dalmacija_1941.html?id=MaUaAAAAIAAJ&redir_esc=y|title=Dalmacija 1941: prilozi historiji Narodnooslobodilačke borbe|trans_title=Dalmatia 1941: Contribution to the History of the People's Liberation War|publisher=27. srpanj|location=Zagreb|oclc=41008546|accessdate=4 September 2014}}
*{{cite book|ref=harv|last1=Šalov|first1=Mate|year=1980|url=http://www.worldcat.org/title/cetvrta-dalmatinska-ltsplitskagt-brigada/oclc/641824519?ht=edition&referer=di|title=Četvrta Dalmatinska (Splitska) brigada|trans_title=4th Dalmatian (Split) Brigade|publisher=Institut za historiju radničkog pokreta Dalmacije|location=Split|oclc=641824519|accessdate=21 November 2014}}
{{refend}}

;Other sources
{{refbegin|2}}
*{{cite news|ref={{harvid|Ruduša: Braća Žužul obnovila partizanski spomenik}}|newspaper=EZadar|url=http://www.ezadar.hr/clanak/rudusa-braca-zuzul-obnovila-partizanski-spomenik|title=Ruduša: Braća Žužul obnovila partizanski spomenik|trans_title=Ruduša: The Žužul brothers renovated a Partisan monument|date=22 August 2009|accessdate=7 September 2014}}
*{{cite news|ref={{harvid|Opet oštećen spomenik splitskim partizanima}}|newspaper=[[Slobodna Dalmacija]]|url=http://www.slobodnadalmacija.hr/Split/tabid/72/articleType/ArticleView/articleId/215868/Default.aspx|title=Opet oštećen spomenik splitskim partizanima|trans_title=Monument to the Split partisans vandalized once again|date=16 July 2013|accessdate=20 November 2014}}
*{{cite news|ref={{harvid|Mejaši neće ulicu Prvog splitskog partizanskog odreda: 'To je drsko, mi smo potpuno drugačijeg svjetonazora!'}}|newspaper=Slobodna Dalmacija|url=http://www.slobodnadalmacija.hr/Split/tabid/72/articleType/ArticleView/articleId/254897/Default.aspx|title=Mejaši neće ulicu Prvog splitskog partizanskog odreda: 'To je drsko, mi smo potpuno drugačijeg svjetonazora!'|trans_title=Mejaši don't want a street of the 1st Split Partisan Detachment: 'That's rude, we have a completely different political orientation'|date=24 August 2014|accessdate=20 November 2014}}
*{{cite news|ref={{harvid|Nakon otkrivanja spomenika HOS-u pod stijegom Rafaela Bobana, Baldasar vraća partizane u Split}}|newspaper=[[Index.hr]]|url=http://www.index.hr/vijesti/clanak/nakon-otkrivanja-spomenika-hosu-pod-stijegom-rafaela-bobana-baldasar-vraca-partizane-u-split/766572.aspx|title=Nakon otkrivanja spomenika HOS-u pod stijegom Rafaela Bobana, Baldasar vraća partizane u Split|trans_title=After revealing the HOS monument under the flag of Rafael Boban, Baldasar returns the Partisans to Split|date=20 August 2014|accessdate=20 November 2014}}
*{{cite news|ref={{harvid|Baldasar: Ne mogu dozvoliti svađu oko imena ulice}}|newspaper=Index.hr|url=http://www.index.hr/vijesti/clanak/povukao-prijedlog-baldasar-dozivio-katastrofu-s-ulicom-prvog-splitskog-partizanskog-odreda/767282.aspx|title=Baldasar: NE mogu dozvoliti svađu oko imena ulice|trans_title=Baldasar: I can't allow a fight about a street name|date=25 August 2014|accessdate=20 November 2014}}
*{{cite news|ref={{harvid|Zaboravljena strijeljana splitska dica}}|newspaper=Slobodna Dalmacija|url=http://www.slobodnadalmacija.hr/Split/tabid/72/articleType/ArticleView/articleId/23097/Default.aspx|title=Zaboravljena strijeljana splitska dica|trans_title=The forgotten executed children of Split|date=21 September 2008|accessdate=21 November 2014}}
*{{cite web |ref={{harvid|Prvi splitski odred}}|url= http://www.imdb.com/title/tt0187428/|title=Prvi splitski odred|trans_title=First Split Detachment|website= imdb.com|publisher=[[Internet Movie Database]]|accessdate=21 November 2014}}
{{refend}}

[[Category:Detachments of the Yugoslav Partisans]]
[[Category:20th century in Split]]
[[Category:Military units and formations established in 1941]]