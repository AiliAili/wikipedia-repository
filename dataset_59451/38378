{{Infobox military conflict
|conflict=Bolesław I's expedition to Kiev
|partof=Kievan succession crisis, 1015–1019
|image=
[[File:Kievan Rus en.jpg|300px]]
|caption=Kievan Rus in the 11th century, with adjoining regions
|date= 1018
|place=[[Kievan Rus']]
|casus=Military aid of Polish ruler [[Bolesław I of Poland|Bolesław I the Brave]] to his son-in-law, [[Sviatopolk I of Kiev|Sviatopolk I the Accurse]]
|territory=
|result=Temporary victory for Sviatopolk and Bolesław
|combatant1=Kievan Rus' loyal to Sviatopolk allied with the [[History of Poland (966-1385)|Duchy of Poland]] and [[Kingdom of Hungary]]
|combatant2=Kievan Rus' loyal to Yaroslav the Wise
|commander1=[[Bolesław I of Poland|Bolesław I the Brave]]<br>[[Sviatopolk I of Kiev|Sviatopolk I the Accursed]]
|commander2=[[Yaroslav I the Wise]]
|strength1=uncertain: Germans (~300 knights), Hungarians (~500) and Pechenegs (~1,000 fighters), 2,000–5,000 Polish soldiers (est.)<ref name="Wyprawa8"/>{{Ref label|a|a|none}}
|strength2=unknown but estimated as similar to opposing size<ref name="Wyprawa8"/>
|casualties1=unknown
|casualties2=unknown
|notes=
}}

The '''intervention in the Kievan succession crisis''' of 1015–1019 by the Polish ruler [[Bolesław I of Poland|Bolesław Chrobry]] was an episode in the struggle between [[Sviatopolk I of Kiev|Sviatopolk I Vladimirovich]] ("the Accursed") and his brother [[Yaroslav I the Wise|Yaroslav]] ("the Wise") for the rulership of Kiev and [[Kievan Rus]]'. It occurred when Sviatopolk's father-in-law Bolesław, ruler of Poland, intervened on Sviatopolk's behalf.

The intervention was initially successful as Bolesław defeated Yaroslav's armies, and temporarily secured the throne for Sviatopolk. But when Bolesław withdrew himself and his army from Kiev, Sviatopolk was unable to retain his position, being defeated by Yaroslav in the following year. Chronicles of the expedition include legendary accounts as well as factual history and have been subject to varied interpretations.

== Background ==
The ruler of [[History of Poland (966–1385)|Poland]], [[Bolesław I of Poland|Bolesław I]], and the [[Grand Duke of Kiev|ruler of Kiev]], [[Vladimir I of Kiev|Vladimir I]], had previously fought over the [[Cherven Towns]] (in what was later called [[Red Ruthenia]]) in a conflict that ended favorably for Vladimir.<ref name="pat30"/> Furthermore, Bolesław, who already had two wives, wanted to marry Predslava, one of [[Family life and children of Vladimir I|Vladimir's daughters]], in order to cement ties between the two families. Despite Bolesław's best efforts, the offer was refused and instead he had to accept a less prestigious connection to the house of Vladimir through the marriage of Bolesław's daughter to Vladimir's son, Sviatopolk.<ref name="pat30"/><ref name="Wyprawa7"/> Between 1005 and 1013, Vladimir arranged Sviatopolk's marriage to Bolesław's daughter, whose name has not survived in sources.<ref name="Wyprawa6"/><ref name="pvl4"/>

It is possible that Vladimir decided that neither Sviatopolk nor Yaroslav would succeed to the Kievan throne after his death, as both Sviatopolk and Yaroslav revolted against their father.<ref name="Wyprawa6"/><ref name="FrankShep-1845"/> Vladimir perhaps intended that Sviatopolk would only receive the remote town of [[Principality of Turov and Pinsk|Turov]] (''Turaŭ'') after his death, and perhaps choosing his younger sons, [[Boris and Gleb]], as successors despite Sviatopolk being older.<ref name="Wyprawa6"/> Although Sviatopolk is known to have been older than Boris and Gleb, the exact birth order of Vladimir's sons is not known and Sviatopolk is alleged in some sources to have been a bastard.<ref>Franklin & Shepard, ''Emergence of Rus'', 190-1.</ref> Perhaps unhappy by his rule being restricted to only a small [[appanage]], Sviatopolk plotted to overthrow his father.<ref name="Wyprawa6"/> Those theories, however, are based on very little evidence, and in the words of two historians, the origins of their "quarrels with their father are obscure".<ref name="FrankShep-185"/> According to [[Thietmar of Merseburg]], Bolesław encouraged Sviatopolk's revolt through his daughter and the latter's wife, though he does not specify the goal of the revolt.<ref name="Thietmar"/> Sviatopolk's conspiracy was, in the event, thwarted by Vladimir, who called Sviatopolk and his entourage to Kiev and jailed them in 1013.<ref name="Wyprawa6"/>

The planned overthrow, if it existed, may have been supported by Bishop [[Reinbern]] of [[Kołobrzeg]], who had traveled with Bolesław's daughter.<ref name=Thietmar/> According to the same chronicler, Reinbern actively took part in converting pagans in and around the Rus lands, but was imprisoned with Sviatopolk and the latter's wife.<ref name=Thietmar/> Reinbern, who might have acted in the interest of [[Catholic Church|Catholic]] [[Holy See|Rome]], died shortly after being imprisoned.<ref name="Thietmar"/><ref name="pat30"/> It is of note that Bolesław invaded Kiev's lands in 1013. This was possibly Bolesław's first attempt to re-take the Cherven Towns,<ref>Franklin & Shepard, ''Emergence of Rus'', 199.</ref> though it has also been argued that his goal might have been to free Sviatopolk.<ref name="Wyprawa6"/>

=== Death of Vladimir ===
[[File:Boleslaus I.jpg|upright|thumb|Bolesław the Brave]]
Just before Vladimir died, he had sent his son Boris on campaign against the nomads in the south.<ref>Cross (ed.), ''Russian Primary Chronicle'', p, 126; Franklin & Shepard, ''Emergence of Rus'', 184—5.</ref> According to the ''Primary Chronicle'', Sviatopolk seized Kiev while those of Vladimir's retainers who were with Boris on campaign encouraged Boris to take power, an offer Boris refused apparently stating "Be it not for me to raise my hand against my elder brother".<ref>Cross (ed.), ''Russian Primary Chronicle'', p, 126.</ref> In the confusion resulting from the death of Vladimir Sviatopolk was able to seize power in Kiev, as Yaroslav was in the north, Mstislav in the south, [[Sviatoslav of Smolensk|Sviatoslav]] in the Derevlian land, Gleb in [[Murom]] and Boris on the aforementioned expedition against the Pechenegs.<ref name=FrankShep-1845 /><ref>Martin, ''Medieval Russia'', 44—5.</ref> As Franklin and Shepard put it, Sviatopolk's "previous arrest turned to his advantage, for it ensured that he was already ... closest to the center of power".<ref name=FrankShep-185/> According to the ''Primary Chronicle'', Sviatopolk successfully arranged the murder of three of his brothers, Boris of Rostov, Gleb of Murom and Sviatoslav of the Derevlian lands.<ref name=FrankShep-185/><ref>Cross (ed.), ''Ruthenians Primary Chronicle'', 126—30; these events, while not uncommon for their time, contributed to earning Sviatopolk the nickname of "The Accursed".</ref>

When news of the [[fratricide]]s reached Vladimir's fourth son, [[Yaroslav the Wise]] in [[Novgorod]], he came to Kiev from the north with Novgorodians and [[Varangians]]. Sviatopolk's reign in Kiev was threatened. After a 3-month stand-off near [[Lyubech]], Sviatopolk was defeated and "fled to the Poles".<ref>Franklin & Shepard, ''Emergence of Rus'', 186</ref><ref name="pvl5"/> Bolesław, who had recently agreed a peace with the [[Holy Roman Empire|German Kingdom]] (the [[Treaty of Bautzen]]), agreed to support his son-in-law through military intervention.<ref name="Wyprawa6"/>

==Sources==
There are three main sources that provide historians with evidence for these events. The best and most reliable account is from a chronicle by Bishop [[Thietmar of Merseburg]], who obtained detailed information from Saxon knights fighting for Bolesław.<ref>Franklin & Shepard, ''Emergence of Rus'', 184.</ref>

The ''Primary Chronicle'' attributed to [[Nestor the Chronicler]] is another sources giving a detailed account of events, its reliability being variable, depending event-by-event on the sources from which it was compiled. Nestor's writing reflects the typical Rus' admiration of [[Saint Vladimir]], while Bishop Thietmar's account, despite a generally positive attitude towards the Rus', paints both Bolesław and Vladimir exclusively in a negative light.<ref name="pat30"/>

A third source is the ''[[Cronicae et gesta ducum sive principum Polonorum|Chronicle of Polish Dukes]]'', a semi-legendary ode to the early Polish dukes written in the 1110s by the Benedictine monk [[Gallus Anonymus|Gallus']].<ref>Barford, ''Early Slavs'', 9.</ref> This account portrays Bolesław in a very positive light.

==Expedition to Kiev==
[[File:Bilibin yaroslav.jpg|upright|thumb|right|Yaroslav the Wise]]
According to Thietmar, the army of Bolesław crossed the border in 1018 and reached Kiev later that same year. Little is known about the armies. Thietmar relates:<blockquote>"Among those rendering assistance to the aforesaid duke, were three hundred of our [German] warriors, five hundred Hungarians, and one thousand Pechenegs".<ref name="Thietmar834"/></blockquote>
Polish historian [[Rafał Jaworski]] states that the estimates of the size of Bolesław's army range between 2,000–5,000 Polish warriors, in addition to Thietmar's reported 1,000&nbsp;[[Pechenegs]], 300&nbsp;German [[knight]]s, and 500&nbsp;Hungarian mercenaries.<ref name="Wyprawa8"/> Less is known about Yaroslav's army, but it is assumed that he managed to collect a force of similar size. It is also believed that he was aware of Bolesław's intentions and had time to make defensive preparations.<ref name="Wyprawa8"/>

The narrative of Bolesław's invasion is almost entirely dependent upon the account of Thietmar:<blockquote>"We may not keep silent regarding the sad and harmful events that occurred in Rus. For, on our [German] advice, Boleslav attacked it with a large army and caused much destruction. On July 22, the duke [Boleslav] came up to a certain river, where he ordered his army to set up camp and separate the necessary bridges. Also camped near the river, along with his army, was [Yaroslav] the king of the Ruthenians."<ref>VIII. 31; Warner (ed.), ''Chronicon'', 382—3.</ref></blockquote> Probably after concentrating his forces during June, in July Bolesław led his troops to the border - the banks of the [[Southern Bug]] River, near one of the settlements of the [[Volhynia]] region.<ref name="Wyprawa8"/> In the meantime, Bolesław's Pecheneg allies approached Kiev, forcing Yaroslav to detach a part of his forces to ensure the safety of his capital.<ref name="Wyprawa8"/> According to Jaworski, Yaroslav, in turn, wanted to prevent Bolesław from uniting with the Pechenegs, defeat Bolesław's main force and then take care of the less organized Pechenegs.<ref name="Wyprawa8"/>

The two armies met on opposite banks of the River Bug.<ref name="Wyprawa8"/> Yaroslav's forces may have taken position with archers covering the crossing points. Bolesław seems to have taken his time, allowing his army to rest, and started work on makeshift bridges.<ref name="Wyprawa8"/> The [[Battle of the River Bug]] finally occurred around July 23.<ref name="Wyprawa9"/>

Thietmar's near-contemporary account offered the following: <blockquote>The Poles provoked the enemy into fighting and, with unexpected success, drove them from the river bank which they [the Rus] were supposed to defend. Elated by this news, Boleslav hastily notified his companions and quickly cross the river although not without effort. In contrast, the hostile army, drawn up in battle formation, vainly attempted to defend its homeland. It collapsed at the first attack, however, and failed to mount any effective resistance. Among those who fled, many were killed, but only a few of the victors were lost. On our side, the dead included Erich, an illustrious knight whom our emperor had long held in chains. From that day on, with every success, Boleslav drove the scattered enemies before him; and the whole populace received and honoured him with many gifts.<ref name="Thietmar831"/></blockquote>

According to the later ''Chronicle of Polish Dukes'' by Gallus, the battle occurred by accident: When Bolesław decided to throw a feast to boost his army's morale, Yaroslav's archers and scouts decided to create trouble for the Polish servants who were gutting the animals and preparing them near the river. However, they only annoyed them enough that the servants themselves crossed the relatively shallow river and chased away Yaroslav's surprised troops, who had been guarding the river.<ref name="Wyprawa9"/> Bolesław learned of the skirmish sooner than Yaroslav, and managed to move most of his army across the river, defeating the surprised Yaroslav.<ref name="Wyprawa9"/>

The Russian ''Primary Chronicle'' gives a different version of events, in which both armies were combat ready and separated by the river before Bolesław, enraged by insults from across the river, charged with his army, surprising Yaroslav and scattering his forces.<ref name=pvl5/><ref name="Wyprawa9"/> All accounts agree that the Polish prince was victorious in the battle.<ref name="Wyprawa9"/> Yaroslav retreated north to Novgorod, rather than to Kiev - likely suspecting that he lacked enough strength to defend Kiev, which was besieged by the Pechenegs and had a significant pro-Sviatopolk faction within its walls.<ref name="Wyprawa9"/> Nestor notes that after reaching Novgorod, Yaroslav attempted to flee "overseas" in hopes of coming back with a Varangian force, but according to the ''Primary Chronicle'', the citizens of Novgorod pressured him to lead the fight back to Bolesław and Sviatopolk.<ref name=pvl5/><ref name="grekov"/>

== Fall and occupation of Kiev ==
[[File:Matejko-chrobry at Kiev (Kijow).jpg|upright|thumb|right|Bolesław Chrobry and Svetopelk at Kiev, in a legendary (if ahistorical) moment of hitting the [[Golden Gate (Kiev)|Golden Gate]] with the [[Szczerbiec]] sword. Painting by [[Jan Matejko]].]]

Bolesław's victory opened the road to Kiev, already under harassment from his Pecheneg allies.<ref name="Wyprawa8"/> <blockquote>At Boleslav's instigation, the very strong city of Kiev was disturbed by the constant attacks of hostile Pechenegs and severely weakened by fire. It was defended by the inhabitants, but quickly surrendered to the foreign warriors, after its king [Yaroslav] fled and abandoned it.<ref name=Thietmar831/></blockquote>

The city, which suffered from fires caused by the Pecheneg siege, surrendered upon seeing the main Polish army on August 14.<ref name="Wyprawa10"/> The entering forces, led by Bolesław, were ceremonially welcomed by the local [[archbishop]] and [[Family life and children of Vladimir I|Vladimir's family]]:<ref name="Wyprawa10"/> <blockquote>On 14 August, the city received Boleslav and Sventipolk [i.e. Sviatopolk], its long-absent lord. Thereafter, through his favour, and from fear of us, the whole region was brought into submission. When they arrived, the archbishop of that city received them, at the [[Saint Sophia Cathedral in Kiev|church of St Sophia]], with relics of the saints and other kinds of ceremonial apparatus.<ref name=Thietmar831/></blockquote>

A later popular Polish legend related to the history of the [[Polish Crown Jewels|Polish coronation weapon]], the [[Szczerbiec]] sword, is the tale of the [[Golden Gate (Kiev)|Golden Gate of Kiev]], upon which the Szczerbiec was supposedly notched when Bolesław's entered the city.<ref name="Wyprawa10"/> This legend has no historical basis, however, and the gate was only built approximately 20&nbsp;years later, while the sword itself was not forged until 200&nbsp;years later. It is of course possible, however unlikely, that Bolesław notched another gate with another sword, thus giving rise to the legend.<ref name="Wyprawa11"/>

Bolesław sent his German and Hungarian mercenaries home after Sviatopolk was re-established on the Kievan throne, "the populace" having "flocked to him" and having "appeared loyal".<ref name=Thietmar834/> It is not known how long Bolesław remained in and around Kiev. The 10 months given by the unreliable account of Gallus is fanciful.<ref name="Franklin187n14"/> Bolesław in fact departed within a few months and, as Thietmar died on December 1, 1018, Bolesław must have been back in Poland a good time before December.<ref name=Franklin187n14/>

The ''Primary Chronicle'' alleges that as the result of Polish plundering, Sviatopolk ordered "that any [[Name of Poland|Lyakhs]] [i.e. Poles] found in the city should be killed".<ref name=grekov/><ref name="PVL6526"/> The resulting unrest, according to the same source, forced Bolesław to leave Kiev, whereupon Sviatopolk was left to fend for himself.<ref name="pat30"/><ref name=grekov/><ref name=PVL6526/><ref name="Ryzhov"/> This negative turn of events is omitted in the only contemporary source, Thietmar of Merseberg's ''Chronikon''.<ref>See VIII. chs 31—33; Warner (ed.), ''Chronicon'', 383—5.</ref> By contrast, his summary of the expedition, written in a part of the ''Chronikon'' not devoted to the expedition, recounts that:<blockquote>Duke Boleslav invaded the Ruthenian king's realm with his army. After placing his long-exiled brother-in-law, the Russian's brother, on the throne, he returned in high spirits.<ref>VII. 66; Warner (ed.), ''Chronicon'', 354.</ref></blockquote>

According to Thietmar, Bolesław asked Yaroslav to return his daughter, whom Yaroslav had taken prisoner.<ref name="Wyprawa10"/> As Yaroslav refused, Bolesław took members of Yaroslav's family to Poland as prisoners when he returned to his country in September. His captives included Vladimir's widow and Yaroslav's sister, Predslava, whose hand Bolesław had sought earlier. Having been rebuffed, Bolesław now took her as a [[concubine]].<ref name="Wyprawa10"/> The Polish duke also took some commoners as well as much of the treasury of Kiev.<ref name="Ryzhov"/> Among the notable commoners was the venerated Saint [[Moses the Hungarian]].<ref name="pat30"/>

In the past some historians (such as Zhylenko and Kostomarov) have conjectured that Bolesław decided to rule Kievan lands himself, though Bolesław had no power base there and no Rurikid blood.<ref name="pat30"/><ref name="Wyprawa10"/><ref name=kost/> Bolesław's main motivation, according to the interpretations of modern historians, was to regain the Cherven Towns for his patrimony, while at the same time aiding his kinsman, to whom he had an obligation.<ref>Franklin & Shepard, ''Emergence of Rus'', 199, 253.</ref><ref name="Martin-45"/> The expedition also furnished an occasion to enrich his followers from Kiev's famous wealth. Bolesław, soon after his arrival, sent a significant force to quarter in Kiev and nearby towns, forcing Kievans to sustain them, and collected significant tributes that he divided among his allies.<ref name="Wyprawa10"/> It was related by Thietmar that before departing, Bolesław <blockquote>was shown an unspeakable amount of treasure, most of which he distributed among his friends and supporters.<ref name="Thietmar834"/></blockquote>

On many later occasions in the Kievan period the rulers of Poland, as well as Hungarians or Pechenegs, were paid to intervene in Rus succession disputes; in the case of Bolesław II, the Polish monarch took the money without making any expedition.<ref>Franklin & Shepard, ''Emergence of Rus'', 257—8, 329—30.</ref>

== Aftermath ==
Sviatopolk lost the throne soon afterwards and lost his life the following year.<ref name="Wyprawa11"/> As Bolesław was involved in a conflict with [[Holy Roman Emperor]] [[Henry II, Holy Roman Emperor|Henry II]], he did not intervene on behalf of his son-in-law when he was deposed and instead signed a pact with Yaroslav, who had successfully regained the throne. Although he lost control of Kiev, Bolesław succeeded in keeping the Cherven Towns captured by [[Vladimir the Great]] in 981; he was crowned [[King of Poland]] in 1025.<ref name="Wyprawa11"/> Yaroslav outlived Bolesław and contributed greatly to the strengthening of [[Kievan Rus']].<ref name="Wyprawa12"/>

==Notes==
'''a''' {{Note label|a|a|none}} The estimate is based on the work of Polish historian [[Rafał Jaworski]].

==Inline references==
<!--See http://en.wikipedia.org/wiki/Wikipedia:Footnotes for an explanation of how to generate footnotes using the <ref(erences/)> tags-->
{{reflist|2|refs=

<ref name="FrankShep-1845">Franklin & Shepard, ''Emergence of Rus'', 184—5.</ref>

<ref name="FrankShep-185">Franklin & Shepard, ''Emergence of Rus'', 185.</ref>

<ref name="Franklin187n14">Franklin & Shepard, ''Emergence of Rus'', 187, n. 14.</ref>

<ref name="Martin-45">Martin, ''Medieval Russia'', 45.</ref>

<ref name="PVL6526">PVL, s.a. 6526, Cross (ed.), ''Russian Primary Chronicle'', 132.</ref>

<ref name="Ryzhov">Ryzhov, 1999</ref>

<ref name="Thietmar">''Thietmar Merseburgensis Episcopi Chronicon'', VII. 72; Warner (ed.), ''Chronicon'', 358.</ref>

<ref name="Thietmar831">VIII. 31; Warner (ed.), ''Chronicon'', 383.</ref>

<ref name="Thietmar834">VIII. 32; Warner (ed.), ''Chronicon'', 384.</ref>

<ref name="Wyprawa10">''Wyprawa Kijowska Chrobrego'', 10</ref>

<ref name="Wyprawa11">''Wyprawa Kijowska Chrobrego'', 11</ref>

<ref name="Wyprawa12">''Wyprawa Kijowska Chrobrego'', 12</ref>

<ref name="Wyprawa6">''Wyprawa Kijowska Chrobrego'', 6</ref>

<ref name="Wyprawa7">''Wyprawa Kijowska Chrobrego'', 7</ref>

<ref name="Wyprawa8">''Wyprawa Kijowska Chrobrego'', p.8</ref>

<ref name="Wyprawa9">''Wyprawa Kijowska Chrobrego'', 9.</ref>

<ref name="grekov">Grekov, [http://www.bibliotekar.ru/rusFroyanov/24.htm section 12]</ref>

<ref name="kost">Kostomarov, Yaroslav</ref>

<ref name="pat30">Zhylenko, W. 30</ref>

<ref name="pvl4">The [[Primary Chronicle]], [http://www.infoliolib.info/rlit/pvl/pvl4.html Section].</ref>

<ref name="pvl5">The ''[[Primary Chronicle]]'', [http://www.infoliolib.info/rlit/pvl/pvl5.html Section]</ref>
}}

== References ==
* ''Wyprawa Kijowska Chrobrego'' Chwała Oręża Polskiego Nr 2. [[Rzeczpospolita (newspaper)|Rzeczpospolita]] and [[Mówią Wieki]]. Primary author [[Rafał Jaworski]]. 5 August 2006 {{pl icon}}
* {{citation |last= Barford |first= P. M. |authorlink=P. M. Barford|title= The Early Slavs: Culture and Society in Early Medieval Eastern Europe | date= 2001|publisher= Cornell University Press |location= Ithaca |isbn= 0-8014-3977-9}}
* {{citation | editor-last = Cross | editor-first = Samuel Hazzard | editor2-last = Sherbowitz-Wetzor| editor2-first= Olgerd | title = The Russian Primary Chronicle: Laurentian Text | place = Cambridge, MA | publisher = Medieval Academy of America | year = 1953 | series = The Medieval Academy of America Publication No. 60 | isbn =}}
* {{citation |last= Franklin |first= Simon |authorlink=Simon Franklin| last2= Shepard | first2= Jonathan | author2-link = Jonathan Shepard|title=The Emergence of Rus, 750-1200 |year= 1996 |series=Longman History of Russia |publisher= Longman |location= London & New York |isbn= 0-582-49091-X}}
* [[Boris Grekov|Grekov, Boris]] (1882–1953), ''"Kievskaya Rus'"'', AST, 2004, ISBN 5-17-025449-0 {{Ru icon}}
* {{citation |last= Martin |first= Janet |authorlink= Janet Martin|title=Medieval Russia, 970-1584| year= 1995 | series= Cambridge Medieval Textbooks |publisher= Cambridge University Press |location= Cambridge |isbn= 0-521-36832-4}}
* "[http://litopys.org.ua/paterikon/paterikon.htm Патерик Києво-Печерський]", organized, adapted into Ukrainian, and footnoted by Iryna Zhylenko, Kiev, 2001. [http://litopys.org.ua/paterikon/pat30.htm Sec. 30 and editor's notes] {{uk icon}}
* [[Nikolay Kostomarov|Kostomarov, Nikolay]] (1817–85), "Russkaya istoriya v zhizneopisaniyakh ee glavneyshikh deyateley" ("Russian History in Biographies of its main figures"), Moskva (Moscow), "Mysl'", 1993, ISBN 5-244-00742-4, {{LCCN|94152432}} [http://www.magister.msk.ru/library/history/kostomar/kostom02.htm#3 Section 2: Yaroslav] {{Ru icon}}
* Ryzhov, Konstantin, (1999). ''All monarchs of the world: Russia: 600 short biographies.'' Moscow: Veche, Veche. ISBN 5-7838-0268-9. (''Рыжов Константин (1999).'' Все монархи мира: Россия: 600 кратких жизнеописаний (in Russian). Москва: Вече.), {{LCCN|98160491}} {{Ru icon}}
* {{citation | editor-last = Warner | editor-first = David A. | title = Ottonian Germany: The Chronicon of Thietmar of Merseburg | place = Manchester & New York | publisher = Manchester University Press | year = 2001 | series = Manchester Medieval Sources Series | isbn = 0-7190-4926-1}}

{{good article}}

{{DEFAULTSORT:Boleslaw I's intervention in the Kievan succession crisis}}
[[Category:11th century in Kievan Rus']]
[[Category:Military history of Ukraine]]
[[Category:Medieval Ukraine]]
[[Category:Military operations involving Poland]]
[[Category:1018 in Europe]]
[[Category:Conflicts in 1018]]
[[Category:Succession]]
[[Category:1018 in Poland]]
[[Category:11th century in Ukraine]]