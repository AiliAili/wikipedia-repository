{{Use mdy dates|date=November 2014}}
{{Infobox musical artist
| name                = Philip C. Hey
| image               = Philip C. Hey.png
| caption             =
| image_size          =
| background          = non_vocal_instrumentalist
| birth_date          = {{Birth date and age|1953|5|21}}
| birth_place         = [[New York City|New York, NY]], US
| instrument          = Drums, percussion
| genre               = [[Jazz]], [[bebop]], [[hard bop]]
| occupation          = Musician, composer, educator, bandleader
| associated_acts     = [[Phil Hey Quartet]]
| current_members     =
| past_members        =
| notable_instruments =
| website             = [http://www.philhey.com www.philhey.com]
}}
'''Phil Hey''' (born May 21, 1953) is an American [[Jazz drumming|jazz drummer]] born in [[New York City|New York, NY]].  He has worked with several leading [[jazz]] musicians including [[Dewey Redman]], [[Jay McShann]], [[Mose Allison]], [[Benny Carter]], [[Charlie Rouse]], [[Harold Land]], [[Charlie Byrd]], [[David "Fathead" Newman]], [[Geoff Keezer]], [[Mark Murphy (singer)|Mark Murphy]], [[Benny Golson]], [[Stacey Kent]], and [[Kenny Barron]].<ref name="police">{{cite web|url=http://www.jazzpolice.com/content/view/11323/68|title=Jazz Police Biography of the Phil Hey Quartet|publisher=}}</ref>

== Biography ==

Born in New York City, he grew up in [[Philadelphia]] and the [[St Paul, Minnesota|St. Paul]] suburb of [[Roseville, Minnesota]].<ref name="police" /> The [[Twin Cities]]-region continues to be his base, although he regularly performs throughout North America as a [[sideman]].<ref>{{cite web|url=http://www.staceykent.com/176.html|title=Performing with Stacey Kent at Birdland in New York, NY|publisher=}}</ref>

He started his music study with mentor and legendary jazz drummer [[Ed Blackwell]] at the [[Creative Music Studio]] in New York in 1975.  His relationship with Blackwell continued until Blackwell's death in 1992.<ref name = "UMN">{{cite web|url=https://music.umn.edu/people/faculty-staff/profile?UID=heyxx002|title=University of Minnesota Faculty Biography/Profile|publisher=}}</ref> He has also studied with Floyd Thompson<ref>{{cite web|url=http://home.comcast.net/~thompsondrumstudio/|title=FloydThompson Drum Studio with List of Former Students|publisher=}}</ref> and Marv Dahlgren, the former principal percussionist of the Minnesota Orchestra.<ref name="police" /><ref>[http://www.mcnallysmith.edu/faculty/marv-dahlgren "Marv Dahlgren McNally Smith Faculty Biography/Profile"]</ref> In addition to several jazz musicians he counts the [[Beatles]] and [[1960s in music|1960s rock]] groups as early music influences.  He also credits his parents and his childhood band instructor for their support and encouragement in pursuing a music career.

He actively performs with several groups and leads his own quartet.  The [[Phil Hey Quartet]] features Phil Hey on drums, Tom Lewis on bass, Dave Hagedorn on vibraphone, and Phil Aaron on piano.  The group released ''Subduction: Live At [[Artists' Quarter|The Artist's Quarter]]'' in 2005 which was subsequently named ''Best Jazz CD of the Year'' by the Twin Cities alternative weekly newspaper ''[[City Pages]]''.<ref>[http://wp.stolaf.edu/jazz/files/2013/11/biographies.pdf "St. Olaf Jazz Workshop Biography/Profile"]</ref>   ''City Pages'' also named him ''2006 Jazz Musician of the Year''.<ref>{{cite web|url=http://www.citypages.com/bestof/2006/award/best-jazz-artist-2173/|title=City Pages Best Jazz Artist Minneapolis 2006 – Phil Hey|date=March 31, 2007|publisher=}}</ref> His first album, ''Let Them All Come'' with Pat Moriarty, was released in 1977 on the small private label ''Min Records''.  The cover art by Homer Lambrecht is featured in ''Freedom, Rhythm, and Sound'', a compilation of a unique jazz album artwork by [[Gilles Peterson]] and Stuart Baker (''[[Soul Jazz Records]]'').<ref>{{cite book |last1= Peterson|first1= Gilles|last2= Baker|first2= Stuart|date= October 31, 2009|title= Freedom, Rhythm and Sound|trans_title= |url= |deadurl= |format= hardcover|location= |publisher= Soul Jazz Records Publishing|isbn= 0955481724|archiveurl= |archivedate= |accessdate= |via= |subscription= |quote= }}</ref>  In total he has appeared on over 125 recordings and remains a first-call musician supporting regional recording artists as well as touring jazz artists.  His jazz recordings include the critically acclaimed [[Von Freeman]]'s ''Live At [[Dakota Jazz Club|The Dakota]]'',<ref>{{cite web|url=http://www.premonitionrecords.com/common/press/vfreeman/downbeat.html|title=DownBeat Magazine Album Review, August 2001}}</ref> Pete Whitman's X-Tet ''Where's When?'', Tom Hubbard's ''Tribute to Mingus'', and Ed Berger's ''I'm Glad There is You'' – all of which received 4 out of 5 star ratings by ''[[Down Beat]]'' magazine reviewers.

In addition to his work as a jazz musician he has played regional performances with blues and rock acts, including [[Nick St. Nicholas]], [[George "Mojo" Buford]], and [[Fred McDowell|Mississippi Fred McDowell]].  He has appeared on the soundtrack of ''[[the 6th Day]]'' (2000) starring [[Arnold Schwarzenegger]] and several independent film soundtracks including ''Been Rich All My Life'' (2006).  In addition, he has played many touring theater productions, including ''The D.B. Cooper Project'', ''[[Joseph and the Amazing Technicolor Dreamcoat]]'', and [[Irving Berlin]]'s ''I Love a Piano'' and has performed with comics [[Bob Hope]], [[Red Skelton]], and [[Don Rickles]].

== Educator ==
Phil Hey is a faculty member at the [[University of Minnesota]] School of Music<ref name="UMN" /> where he teaches jazz percussion and directs the jazz ensemble.  He also serves as music faculty at [[St. Olaf College]]<ref>{{cite web|url=http://wp.stolaf.edu/music/percussion|title=St.Olaf Faculty Biography/Profile|publisher=}}</ref> and the [[MacPhail Center for Music]], and he instructs jazz students at several other regional colleges, high schools, and jazz clinics.<ref>{{cite web|url=http://mnyouthjazz.com/staff.php|title=Minnesota Youth Jazz Bands|publisher=}}</ref> He is also a former music instructor at [[Macalester College]], serving from 1997 to 2008.

== Equipment ==
Ellis Drum Shop released the Phil Hey Signature Kit, a limited edition 6 piece shell drum kit with maple shells in 2012.<ref>{{cite web|url=http://ellisdrumshop.com/recently-around-the-shop/|title=Phil Hey Signature Kit at Ellis Drum Shop|publisher=}}</ref>

== Discography ==
=== As leader ===
* 1977 ''Let Them All Come'', with Pat Moriarty
* 2005 ''Subduction Live at the Artist's Quarter''
* 2009 ''Conflict!'', with Kelly Rossum

=== As sideman ===
'''With [[Chris Bates (musician)|Chris Bates]]'''
* 2014 ''Good Vibes Trio''

'''With Ed Berger'''
* 1999 ''I'm Glad There Is You''

'''With Terry Lee Burns'''
* 1997 ''Freehand''

'''With Laura Caviani'''
* 1999 ''Angels We Haven't Heard''

'''With the Cedar Avenue Big Band'''
* 2002 ''Land of 10,000 Licks''

'''With Debbie Duncan'''
* 1993 ''Live at the Dakota''
* 1995 ''It Must Be Christmas''
* 2007 ''I Thought About You''

'''With Dan Estrem and John Holmquist'''
* 1988 ''Bossa''
* 1990 ''Meditation''

'''With [[Connie Evingson]]'''
* 1998 ''I Have Dreamed''
* 1999 ''Some Cats Know''
* 2003 ''Let It Be Jazz Connie Evingson Sings the Beatles''
* 2008 ''Little Did I Dream''
* 2012 ''Sweet Happy Life''

'''With [[Von Freeman]]'''
* 2001 ''Live at the Dakota''<ref name="1 September 2001">{{cite web|last1=Bennett|first1=Bill|title=Von Freeman: Live at the Dakota - JazzTimes|url=https://jazztimes.com/reviews/albums/von-freeman-live-at-the-dakota/|website=JazzTimes|accessdate=8 February 2017}}</ref>
* 2007 ''The Best of Von Freeman on Premonition''

'''With Dave Hagedorn'''
* 2003 ''Vibes Solidliquid''

'''With Glen Helgeson'''
* 1995 ''Spirit of the Wood''

'''With Tom Hubbard'''
* 1989 ''Tribute to Mingus''

'''With the [[JazzMn Orchestra|JazzMN Orchestra]]'''
* 2000 ''JazzMN Big Band''<ref name="JMN">{{cite web|title=JazzMN {{!}} JazzMN CD|url=http://www.jazzmn.org/music/jazzmncd.php|website=www.jazzmn.org|accessdate=8 February 2017}}</ref>

'''With [[Gordon Johnson (musician)|Gordon Johnson]]'''
* 2005 ''Trios Version 3.0''
* 2008 ''GJ4''
* 2010 ''Trios No. 5''

'''With Dave Karr and Mulligan Stew'''
* 2004 ''Cookin' at the Hot Summer Jazz Festival''

'''With Mary Louise Knutson'''
* 2001 ''Call Me When You Get There''
* 2011 ''In the Bubble''

'''With Chris Lomheim'''
* 2000 ''The Bridge''

'''With the Minnesota Klezmer Band'''
* 1998 ''Bulka's Song''

'''With David Mitchell'''
* 2000 ''Young Cats''

'''With Lucia Newell'''
* 2004 ''Steeped in Strayhorn''

'''With the [[O'Neill Brothers]]'''
* 2004 ''On Broadway with the O'Neill Brothers''

'''With the Out to Lunch Quintet'''
* 2006 ''Live at the Artist's Quarter''

'''With [[Preston Reed]]'''
* 1991 ''[[Halfway Home (album)|Halfway Home]]''

'''With [[Rio Nido]]'''
* 1986 ''[[Voicings]]''

'''With [[Claudia Schmidt]]'''
* 1991 ''Essential Tension''
* 2012 ''Bend in the River Collected Songs''

'''With Ted Unseth and the Americana Classic Jazz Orchestra'''
* 2007 ''20th Anniversary Concert with Benny Waters''

'''With Benny Weinbeck'''
* 1998 ''Sweet Love''
* 2011 ''Live at D'Amico Kitchen''

'''With Pete Whitman'''
* 1998 ''Departure Point''
* 2001 ''The Sound of Water''
* 2003 ''Where's When?''

'''With Steve Yeager'''
* 2003 ''New Groove Blues''

== Concert video ==

'''With Benny Weinbeck Trio'''
* 2011 – ''Benny Weinbeck Trio: Live at D'Amico Kitchen'' (DVD)

== References ==

{{reflist}}

== External links ==
* [http://www.philhey.com Phil Hey Official Website, ''www.philhey.com'']
* [http://www.philaaronmusic.com Phil Aaron Official Website,  ''Phil Hey Quartet Pianist'', ''www.philaaronmusic.com'']

{{DEFAULTSORT:Hey, Phil}}
[[Category:1953 births]]
[[Category:Living people]]
[[Category:American jazz drummers]]
[[Category:American session musicians]]
[[Category:Musicians from Minneapolis]]
[[Category:Musicians from Minnesota]]
[[Category:Bebop drummers]]
[[Category:Hard bop drummers]]
[[Category:American jazz bandleaders]]
[[Category:American jazz educators]]
[[Category:Jazz educators]]
[[Category:American people of German descent]]
[[Category:American people of Scotch-Irish descent]]