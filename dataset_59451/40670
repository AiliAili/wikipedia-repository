A '''Mars analog habitat''' is one of several historical, existing or proposed research stations designed to simulate the physical and psychological environment of a Martian exploration mission.  These habitats are used to study the equipment and techniques that will be used to analyze the surface of [[Mars]] during a future manned mission, and the simulated isolation of the volunteer inhabitants allows scientists to study the [[medical]] and [[psychosocial]] effects of long-term [[space missions]]. They are often constructed in support of extensive Mars analogs (see [[List of Mars analogs]]). However, sometimes existing natural places are also valued as Mars analogs, such as a high Earth altitude where the pressure is equal to the pressure at the surface of Mars or a certain cave. Manned Mars habitats are featured in most human Mars missions; an alternative may be terraforming or telepresence.

The ISS has also been described as a predecessor to Mars expedition.<ref>[https://books.google.com/books?id=abIKvmDXh_kC&pg=PA298&dq=Mars+habitat&hl=en&sa=X&ved=0CEUQ6AEwCGoVChMI69OBgqH_yAIVQm0mCh250Ap2#v=onepage&q=Mars%20habitat&f=false Expedition Mars By Martin J.L. Turner]</ref> In relation to a Mars habitat, it was noted that they are both essentially closed systems.<ref>[https://books.google.com/books?id=abIKvmDXh_kC&pg=PA298&dq=Mars+habitat&hl=en&sa=X&ved=0CEUQ6AEwCGoVChMI69OBgqH_yAIVQm0mCh250Ap2#v=onepage&q=Mars%20habitat&f=false Expedition Mars By Martin J.L. Turner]</ref>

== Scientific motivation ==
Mars analog habitats are established to prepare astronauts, engineers, and researchers for the future challenges of sending a manned mission to Mars. These analogs are inhabited by small teams of volunteers and may operate under “Mars-like” conditions for a few days to over a year. Habitats are often located in areas that closely resemble the environment of Mars, such as [[polar desert]]s. While living in the habitats, crew members are mostly isolated from the outside world, survive on a diet of freeze-dried food, and may conduct field experiments while wearing protective space suits. Meanwhile, researchers analyze the crew members’ medical and psychological conditions and study the social and teamwork dynamics of the crew.

== Historical missions, experiments, and results ==

=== Flashline Mars Arctic Research Station ===
[[File:FMARS 2009 hab.jpg|thumb|The FMARS hab in 2009]]
The [[Flashline Mars Arctic Research Station]] (FMARS) was established in 2000 in the territory of [[Nunavut, Canada]].<ref>{{cite web|url=http://arctic.marssociety.org/home |title=Flashline Mars Arctic Research Station |publisher=Arctic.marssociety.org |accessdate=2013-12-22}}</ref>  This station is the first Mars analog created by the [[Mars Society]], a space advocacy group established in August 1998.  Since the beginning of field operations in April 2001, twelve separate crews (consisting of 6-7 personnel, each) have inhabited the station.  While most missions lasted less than a single month, FMARS Crew 11 remained in the Martian simulation for 100 days.

FMARS is guided by three goals set by the Mars Society: to serve as a testbed for human Mars exploration, to serve as a useful research facility at a Mars analog site, and to generate public support for sending humans to Mars.<ref>{{cite web|url=http://www.marssociety.org/home/about/purpose |title=Purpose |publisher=The Mars Society |accessdate=2013-12-22}}</ref>  Research conducted by crewmembers while living in the habitat include an analysis of subsurface water detection techniques.  Crewmembers set up seismic sensors along the [[Haughton crater]] while wearing prototype space suits, then tested how the sensors reacted to mini-earthquakes (produced by a sledgehammer) to produce a three-dimensional subsurface map.<ref>
{{Cite journal
| issn = 0094-5765
| volume = 64
| pages = 457–466
| last = Dehant
| first = Vladimir
|author2=Philippe Lognonne |author3=Michel Diament |author4=Véronique Dehant
 | title = Subsurface water detection on Mars by astronauts using a seismic refraction method: Tests during a manned Mars mission simulation
| journal = Acta Astronautica
| date = 2009-02-01
| bibcode = 2009AcAau..64..457P
| doi = 10.1016/j.actaastro.2008.07.005
| issue = 4
}}</ref>

Additional experiments examined how well the FMARS crewmembers could work with an “Earth-based” remote science team.  In one example, the remote science team used aerial images to select ten regions of scientific interest for the FMARS crew to explore and analyze.<ref>{{cite conference |url=http://quest.arc.nasa.gov/projects/spacewardbound/docs/Sklar_Rupert_SEMS.pdf |title= A Field Methodology Approach between an Earth-Based Remote Science Team and a Planetary-Based Field Crew |author1=Stacy T. Sklar  |author2=Shannon M. Rupert  |lastauthoramp=yes |year=2006 |conference= AAS 06-260 |editor= Jonathan Clarke }}</ref>  This experiment helped researchers determine more efficient methods of scouting and investigating locations of interest under the communications limitations of a manned mission to Mars.  A study performed on the FMARS crew of July 2009 examined the psychological difficulties faced by crewmembers (especially the effects of isolation from family, conflicts among crewmembers, and diet) as well as the positive effects of problem-solving and exploration.<ref>{{cite conference |url=http://pdf.aiaa.org/preview/2010/CDReadyMSO10_2129/PV2010_2258.pdf |title=Flashline Mars Arctic Research Station (FMARS) 2009 Crew Perspectives |author1=Kristine Ferrone |author2=Stacy L. Cusack |author3=Christy Garvin |author4=Walter V. Kramer |author5=Joseph E. Palaia |author6=Brian Shiro |date= 25–30 April 2010 |conference= Proceedings of the AIAA [[SpaceOps]] 2010 Conference | pages= 2010–2258}}</ref>

=== Mars Desert Research Station ===
[[File:MDRS Campus.jpg|thumb|480px|The Mars Society's Mars Desert Research Station]]
The [[Mars Desert Research Station]] (MDRS) is the second Mars analog habitat established by the Mars Society.  Located on the [[San Rafael Swell]] in [[Utah]], the MDRS has been inhabited by 130 individual crews (of roughly 6 members each) between the first field season in December 2001 and the twelfth field season ending in May 2013.<ref>{{cite web|url=https://sites.google.com/a/marssociety.org/mdrs2012/home/previous-field-seasons |title=Previous Field Seasons - Mars Desert Research Station |publisher=Sites.google.com |accessdate=2013-12-22}}</ref>  Each crew typically remains in the habitat for one or two weeks.  MDRS is less focused on the psychosocial aspects of space exploration than FMARS.  Instead, crewmembers of MDRS focus on conducting field research in simulated Martian conditions.

[[File:BYU Mars Rover 2009-2.jpg|thumb|The ''[[BYU Mars Rover]]'' undergoing field tests at the Mars Desert Research Station.]]

Several important research results have come out of the MDRS experiments.  Crewmembers were able to detect methane in dry desert soils, developing techniques which could detect [[methane]] – an important [[biosignature]] – in the soil of Mars.<ref>{{Cite journal
| doi = 10.1016/j.icarus.2005.06.008
| issn = 0019-1035
| volume = 178
| pages = 277–280
| last = Moran
| first = Mark
|author2=Joseph D. Miller |author3=Tim Kral |author4=Dave Scott
 | title = Desert methane: Implications for life detection on Mars
| journal = Icarus
| date = 2005-11-01
| bibcode = 2005Icar..178..277M
}}</ref>  Soil and vapor samples from the MDRS habitat were also found to contain significant amounts of [[methanogens]], which were not previously suspected to thrive in a desert environment.<ref>{{cite web|url=http://www.newscientist.com/article/dn8428-extreme-bugs-back-idea-of-life-on-mars.html |title=Extreme bugs back idea of life on Mars - space - 07 December 2005 |publisher=New Scientist |date=2005-12-07 |accessdate=2013-12-22}}</ref>  This research supported the possibility of microbial [[Life on mars|life on Mars]] and demonstrated that members of a manned mission could detect such signs of life during an EVA.

=== Mars-500 ===
The [[Mars-500]] mission was a series of experiments conducted between 2007 and 2011 and sponsored by [[Russia]], the [[European Space Agency]], and [[China]].  The primary focus of these experiments was to study the effects of long-term isolation on a small crew, so that the psychological difficulties of a voyage to Mars and an extended stay on its surface may be better understood.<ref>{{cite web|url=http://mars500.imbp.ru/en/520_about.html |title="Mars-500" project |publisher=Mars500.imbp.ru |accessdate=2013-12-22}}</ref>  Three separate missions were performed: a 14-day isolation in November 2007, a 105-day isolation completed in July 2009, and a 520-day isolation from April 2010 to October 2011.  Unlike other Mars Analog missions, Mars-500 did not take place in a Mars-like environment, but in a Moscow research institute.

An important focus of the Mars-500 research has been the early diagnosis of “adverse personal dynamics” which would affect cooperation among the crew, as well as the development of methods to overcome such issues.<ref>{{cite web|url=http://mars500.imbp.ru/en/105_results.html |title="Mars-500" project |publisher=Mars500.imbp.ru |accessdate=2013-12-22}}</ref>  Researchers decided that any sort of psychological support on future missions would need to be tailored to each individual crew member, not just to the group as a whole or to subgroups belonging to different space agencies.  Mars-500 also found that most psychological issues were exacerbated by isolation and a lack of stimulus, emphasizing the need to prevent sensory deprivation and boredom.

While female Russian biologist Marina Tugusheva participated in the 2007 experiment, women were reportedly excluded from the 2009 and 2011 missions to prevent issues caused by sexual tension.<ref>{{cite web|author=Claire Bates |url=http://www.dailymail.co.uk/sciencetech/article-1165837/Women-excluded-Mars-mission-crew-prevent-sexual-tension-ruining-105-day-voyage.html |title=Women excluded from 'Mars mission' crew to prevent sexual tension ruining 105-day voyage &#124; Mail Online |publisher=Dailymail.co.uk |date=2009-03-31 |accessdate=2013-12-22}}</ref>  However, given the multinational nature of the crew, special efforts were made to promote efficient interaction among a multicultural crew.<ref>{{cite web|url=http://mars500.imbp.ru/en/520_one_year.html |title="Mars-500" project |publisher=Mars500.imbp.ru |accessdate=2013-12-22}}</ref>

=== HI-SEAS ===
The Hawaii Space Exploration Analog and Simulation ([[HI-SEAS]]) program is a series of missions organized by [[Cornell University]] and the [[University of Hawaii at Manoa|University of Hawai'i at Mānoa]].<ref>{{cite web|url=http://hi-seas.org/ |title=Hi-Seas |publisher=Hi-Seas |accessdate=2013-12-22}}</ref>  The habitat is located 8,000 feet above sea level on [[Mauna Loa]] on the [[Big Island of Hawaii]].  The first HI-SEAS mission ran from April to August 2013, and three more missions are planned to take place between 2014 and 2016, including a twelve-month mission between June 2015 and May 2016.

Like other missions, HI-SEAS evaluates the social and psychological status of the crewmembers while they live in isolation and conduct field work in Mars-like environments and conditions.  However, HI-SEAS is especially focused on the diet and nutrition of its crewmembers.  In particular, HI-SEAS is studying how “food intake in a confined and isolated setting similar to what an astronaut would experience during a mission” affects the moods of the crew members and their interactions with each other.<ref>{{cite web|author=Yajaira Sierra-Sastre |url=http://hi-seas.org/?p=1644 |title=HI-SEAS – HI-SEAS Research Overview |publisher=Hi-seas.org |date=2013-06-05 |accessdate=2013-12-22}}</ref>  Crew members prepare their own meals using combinations of shelf-stable ingredients and pre-packaged meals and frequently complete surveys to determine their mood, health, and satisfaction with the food.

HI-SEAS also undertakes research proposals submitted by outside researchers.  As part of this effort, HI-SEAS is testing software which monitors psychological welfare by analyzing text-based communication, so that future astronauts’ limited access to psychiatric help is less of an issue.  In addition, HI-SEAS is testing the durability of antimicrobial clothing used daily by the crew members.

== Possible future missions ==
The Flashline Mars Arctic Research Station, Mars Desert Research Station, and HI-SEAS missions are all ongoing.  FMARS will launch a one-year mission (Mars Arctic 365) in August 2014.<ref>{{cite web|url=http://ma365.marssociety.org/ |title=Mars Arctic 365 &#124; a Mars Society Mission on Devon Island &#124; A Long-Duration Simulated Expedition to Mars to be Done in the Polar Desert of the Canadian High Arctic |publisher=Ma365.marssociety.org |accessdate=2013-12-22}}</ref>  Just as in past missions, Mars Arctic 365 will consist of six volunteers with skills in fields such as geology, biochemistry, microbiology, and engineering, all of whom will conduct field research in Mars-like conditions while in contact with remote support teams.  MDRS has scheduled additional short-term missions through May 2014.<ref>{{cite web|url=http://mdrs.marssociety.org/ |title=Mars Desert Research Station |publisher=Mdrs.marssociety.org |date=2013-12-07 |accessdate=2013-12-22}}</ref>
The Mars Society has planned to establish two more habitats in Mars-like desert locations: the [[European Mars Analog Research Station]] in [[Iceland]], and the [[Australia Mars Analog Research Station]] in the [[Australian outback]].<ref>{{cite web|url=http://chapters.marssociety.org/canada/expedition-mars.org/ExpeditionTwo/html/stations.php |title=Expedition-Mars.org: Expedition Two to the Australian Mars Analog |publisher=Chapters.marssociety.org |accessdate=2013-12-22}}</ref>  While these stations were to be built in 2003, neither has moved past the planning stages, and the future of the missions is unclear.

==See also==
* [[Mars habitat]]
* [[Australia Mars Analog Research Station]]
* [[Climate of Mars]]
* [[Effect of spaceflight on the human body]]
* [[European Mars Analog Research Station]]
* [[Exploration of Mars]]
* [[Life on Mars]]
* [[Present day Mars habitability analogue environments on Earth]]
* [[List of manned Mars mission plans in the 20th century]]
* [[Manned mission to Mars]]
* [[Mars Analogue Research Station Program]]
* [[Mars Desert Research Station]]
* [[Mars Society]]
* [[Mars to Stay]]
* [[Biosphere 2]]

==References==
{{reflist}}

{{Manned mission to Mars}}
{{Mars}}

[[Category:Mars Society]]
[[Category:Human spaceflight analogs]]
[[Category:Mars]]