[[File:ABCLS Logo.jpg|right|thumb |The ABCLS logo]]
The '''Association of British Columbia Land Surveyors (ABCLS)''' is a self-governing, non-profit, [[non-governmental organization]] which sets educational requirements, examines for admission, and regulates professional [[surveying|land surveyors]] within [[British Columbia]], [[Canada]]. The ABCLS is responsible for developing [[by-laws|bylaws]] and guidelines for the conduct of its members, establishing and administering entry requirements for the profession, and liaising with governmental bodies and other [[professional associations|associations]] to improve the quality of the profession's service to the public.<ref name="Civic Info BC. Association of BC Land Surveyors">[http://www.civicinfo.bc.ca/ubcm/ViewSupplier.asp?supplierid=483&find=a Civic Info BC. "Association of BC Land Surveyors." Accessed June 21, 2016]</ref>
 
The ABCLS' mission statement is:

The Association of British Columbia Land Surveyors protects the public interest and the integrity of the survey systems in British Columbia by regulating and governing the practice of land surveying in the province.<ref name="ABCLS Mission">[http://www.abcls.ca/?page_id=11554 ABCLS Website. "ABCLS Mission."  Accessed May 25, 2016]</ref>

The ABCLS is governed by the ''Land Surveyors Act'', a Provincial [[statute]] which sets out the framework within which the Association and its members operate.<ref name="Land Surveyors Act">[http://www.bclaws.ca/civix/document/id/complete/statreg/96248_01 Canada. British Columbia. Land Surveyors Act. &#91;Victoria BC&#93;, 1996]</ref>
The ABCLS is headquartered in [[Sidney, British Columbia]].<ref name="ABCLS Webpage">[http://www.abcls.ca ABCLS Webpage. "Contact Us." Accessed May 25, 2016]</ref><ref>http://www.abcls.ca/?page_id=42</ref>
   
==History==

The profession of land surveying was not regulated in British Columbia until the late 1800s.<ref name="Whittaker, John A. 1990">Whittaker, John A. ''Early Land Surveyors of British Columbia.'' Victoria: The Corporation of Land Surveyors of British Columbia. 1990. ISBN 1-55056-090-5</ref> Before 1891, the provincial government recognized a cadre of professional Surveyors - many of whom had been [[Royal Engineers]]<ref name="Whittaker, John A. 1990"/> 
In 1890, the Association of Provincial Land Surveyors was launched during a two-day long meeting held in Victoria and attended by 22 surveyors.<ref name="Gordon">Gordon, K. ''Made to Measure''. BC: Sono Nis Press. 2006. ISBN 1550560905</ref>  In April 1891, the Provincial Land Surveyors' Act was proclaimed by the [[Executive Council of British Columbia|British Columbia Government]] in response to a petition made to the government by the Association, in which they requested, "(...) the introduction of an act to protect authorized surveyors in the performance of their duties, to prevent unqualified persons acting as surveyors and to provide for duly qualified persons from time to time being admitted to practice." <ref>Thomson, D.W. ''Men and Meridians'' Vol. 2. Ottawa: Department of Energy, Mines and Resources. 1967.</ref> Framed by W.S. Gore, then the Provincial Surveyor General, the  Act established a Board of Examiners (made up of the Surveyor General and five other surveyors) and set down policies for [[Articled clerk|articling pupils]].  Thereafter, only surveyors who passed the examinations and subsequently were approved by the Board were allowed to practice in the province of British Columbia. Authorized surveyors were then called "Provincial Land Surveyors of the Province of British Columbia"; they received a numbered commission and seal, and were entitled to append the initials "PLS" after their names. The PLS group of surveyors eventually numbered eighty-five members.<ref name="Gordon"/>  The Act also incorporated a governing body - the Association of Provincial Land Surveyors - for their profession. However, the Association was relatively weak, and in 1905 it was replaced by the Corporation of Land Surveyors of the Province of British Columbia.  The Corporation was established under new [[legislation]], the Provincial Land Surveyors' Act (1905), which strengthened the management of the profession.<ref name="Gordon"/>   A new Board of Examiners was formed and, as before, only surveyors who were recognized by that Board were entitled to practice in the province. Those so recognized were designated by the initials "BCLS" being appended to their name. Existing PLSs and LSs were also permitted to continue practicing under a [[grandfather clause]]. Although the Provincial Surveyors' Act has since been amended, the Association of British Columbia Land Surveyors now numbers over nine hundred and forty BCLS Commissions and continues to be the governing body of the profession. In order to practice legal land surveying in British Columbia, one must be a commissioned BCLS in good standing.<ref name="Land Surveyors Act"/>

==Becoming a BC Land Surveyor==
<!-- Deleted image removed:  [[File:ABCLS draw the line.jpeg|right|thumb]]  -->
There are three streams by which individuals can be registered as a Land Surveyor in Training and work towards obtaining a commission as a BC land surveyor:

#Obtaining a Certificate of Completion from the Canadian Board of Examiners for Professional Surveyors (CBEPS).
#Obtaining a degree in geomatics engineering from the British Columbia Institute of Technology (BCIT).
#Being a foreign trained professional with a minimum of two years experience as a cadastral land surveyor in their home country.<ref name="ABCLS Becoming a Land Surveyor">[http://www.abcls.ca/?page_id=4382 ABCLS Webpage. "Becoming a Land Surveyor." Accessed June 21, 2016]</ref>
 
===Certificate of Completion===
The Canadian Board of Examiners for Professional Surveyors (CBEPS) has accredited the following geomatics programs which can lead to one registering as a Land Surveyor in Training:

#[[University of Calgary]]
#[[University of New Brunswick]]

Individuals who have not graduated from the above programs but have other relevant academic and professional experience or may have been educated in foreign countries may apply to the CBEPS for a Certificate of Completion (C of C) and/or exemptions from required examinations.  
Additionally, graduates from programs other than geomatics engineering may apply to the CBEPS to have their credentials reviewed for exemptions from required examinations.
Once the CBEPS is satisfied that candidates have satisfied the mandatory academic requirements and have passed (or have been exempted from) necessary examinations, they issue the candidate a C of C. Candidates working through their CBEPS requirements or attending an educational institution in geomatics engineering may register as a survey student with the ABCLS.<ref name="Candidate Application">[https://www.cbeps-cceag.ca/candidate-application CBEPS. "Candidate Application." Accessed June 21, 2016]</ref>

===Degree in Geomatics Engineering from BCIT===
A graduate from the geomatics degree program at BCIT with the required grade in the required courses may apply for entry as a Land Surveyor in Training.<ref name="Geomatics">[http://www.bcit.ca/study/programs/geomatics BCIT. "Geomatics Engineering. " Accessed May 30, 2016]</ref>

===Foreign Trained Professionals===
Foreign trained professionals may apply for direct entry as a Land Surveyor in Training when they have two years of experience working in their home country.  Applications must include academic qualifications, an internationally accredited standards agency credential evaluation and transcript analysis certificate and proof of two years experience in cadastral land surveying in their home country.<ref name="Candidate Application"/>

===Land Surveyors in Training===
Once a candidate completes one of the three streams above, they are able to register with the ABCLS as a land surveyor in training (LST) and begin an articling period which typically lasts 18–32 months. This takes place under the supervision of a master who is a registered BC land surveyor. 
During the articling period, the candidate will write a series of professional examinations and complete three field projects. The examinations are intended to demonstrate that the candidate has a strong knowledge of every aspect of professional land surveying. These include statutes, regulations, practice of land surveying, the General Survey Instruction Rules, and the Surveyor General Circular Letters as they relate to each component of their education. The three field projects are intended to demonstrate the candidates’ practical skills including field measurements, evidence evaluation, boundary resolution, interaction with clients, and project management. Professional examinations and field projects are intended to test the candidates’ required theoretical knowledge and practical skills required to succeed as a land surveyor.<ref name="ABCLS Exam Information">[http://www.abcls.ca/?page_id=268 ABCLS Webpage. "Exam Information."  Accessed May 30, 2016]</ref>

Upon completion of the articling period and professional examinations, the candidate must sit a Professional Assessment Interview (PAI), which is [[Adjudication|adjudicated]] by members of the Board of Management. This interview covers a broad range of topics related to land surveying and is designed to evaluate the candidates’ qualifications, knowledge, manner, and professionalism. Candidates who successfully complete the PAI are able to obtain a commission as a BC land surveyor.<ref name="Professional Assessment Interview">[http://www.abcls.ca/?page_id=456 ABCLS Webpage. "Professional Assessment Interview." Accessed May 30, 2016]</ref>

Surveyors from other Canadian jurisdictions who are in good standing with their home association do not need to register as survey students, but do have to fulfill requirements which depend on each candidate's individual license history with their home association.<ref name="Career Entry">[http://www.abcls.ca/?page_id=35 ABCLS Webpage. "Career Entry." Accessed May 30, 2016]</ref>

==Membership==
Membership in the Association is restricted to those who hold a BCLS commission and registered students in the pursuit of such accreditation.  ABCLS Membership levels are:<ref name="ABCLS General Info">[http://www.abcls.ca/?page_id=309 ABCLS Webpage. "General Info." Accessed May 30, 2016]</ref>

*'''Practising''' – members authorized to conduct surveys within the Province of British Columbia
*'''Nonpractising''' – members currently not authorized to conduct surveys
*'''Retired''' – members who have retired from active practice
*'''Life Members'''– members who have been recognized by their peers for their contribution and service to the profession.  These members have all the privileges of a practising land surveyor but are no longer required to pay dues
*'''Land Surveyor Associates''' – members who are land surveyors in another jurisdiction in Canada and who have applied to become a practising BCLS
*'''Land Surveyors in Training''' – members who have completed their formal university education or equivalent and who are completing the professional requirements to be a practising land surveyor
*'''Survey Student''' – a person who is working towards completing their formal university education or equivalent and who wishes to be enrolled for educational purposes

==Board of Management==

The [[Board of directors|Board of Management]] (the Board) is authorized under the Land Surveyors Act, which identifies the Board's responsibilities and functions.<ref name="Land Surveyors Act"/>

The Board and Executive Officers are elected according to the ABCLS [[by-laws]]. A Vice President is elected annually for a two-year term. During the second year of their term they serve as President. In keeping with unofficial tradition since 1929, the outgoing President presents the incoming President with a pair of [[spats (footwear)|Spats]].<ref name="Gordon"/>   In addition to the President and Vice President, the Board consists of four elected members and four appointees; the Past President, a Lieutenant Governor in Council (an  appointed member of the public), the Surveyor General of BC, and a member appointed to serve as Secretary.  Elected members serve two-year terms and may run for additional terms or run for the position of Vice President.<ref name="Board of Management">[http://www.abcls.ca/?page_id=9700 ABCLS Webpage. "Board of Management." Accessed May 30, 2016]</ref>

==Types of Surveying==

Types of surveying include:
*[[Surveying|Land surveying]]: measurement and positioning on land <ref>Kahmen, Heribert; Faig, Wolfgang. ''Surveying''. Berlin: de Gruyter. 1998. ISBN 3-11-008303-5. Retrieved 2016-05-31</ref> 
*[[Hydrography]]: measurement and positioning on water<ref name="Hydrography">[http://oceanservice.noaa.gov/facts/hydrography.html National Ocean Service. "Hydrography."  Accessed June 1, 2016]</ref>
*[[Photogrammetry]]: measurement and positioning with the use of aerial photographs<ref name="Photogrammetry">[http://www.aols.org/branches-geomatics/photogrammetric-surveying Association of Ontario Land Surveyors. "Photogrammetry."  Accessed June 1, 2016]</ref>
*[[Map]]ping: drafting of maps or charts
*[[Remote Sensing]]: collection of data on an object or an area on the Earth from a distance (satellites, aircraft, etc.)<ref name="Remote Sensing">[http://oceanservice.noaa.gov/facts/remotesensing.html National Ocean Service. "Remote Sensing."  Accessed June 1, 2016]</ref>
*[[Land registration]]: management of property rights information (titles, [[deed]]s)<ref name="Functions of a Surveyor">[http://www.fig.net/about/general/definition/index.asp International Federation of Surveyors. "Functions of a Surveyor." Accessed June 1, 2016]</ref>
*[[Geographic Information Systems]] (GIS): computerized geographic information management<ref name="GIS">[http://nationalgeographic.org/encyclopedia/geographic-information-system-gis/ National Geographic Society. "GIS." Accessed June 1, 2016]</ref>

==Services Provided by BC Land Surveyors==
BC Land Surveyors provide expertise in all areas of land, water and space measurement. Services can be grouped as follows:<ref name="BCLS Services">[http://www.abcls.ca/?page_id=316 ABCLS webpage. "BCLS Services."  Accessed June 1, 2016]</ref>
[[File:Land Survey (14000483127).jpg|right|thumb|Land Survey (14000483127)]]

===Cadastral===

''Cadastral'' comes from the French word which refers to the register of lands. The "cadastral fabric" of B.C. refers to property boundaries, survey monuments, legal documents, maps, and regulations which are required to make the system work. [[Cadastre|Cadastral]] legal surveys involve the measurement, placement, title descriptions and/or re-establishment of legal boundaries on land (at, above, or below the surface), water, and air space. This scope of work includes [[lease]]s, mineral claims, and the determination of any approved physical right of tenure, occupation, and title. Many BCLSs are also consulted in matters such as land use applications, approvals, and re-[[zoning]]s.<ref name="Cadastral Surveying">[http://www.aols.org/branches-geomatics/cadastral-surveying Association of Ontario Land Surveyors. "Cadastral Surveying." Accessed June 1, 2016]</ref>

===Topographical Survey===
A [[topography|topographical]] survey plan shows ground features, [[elevation]]s, and [[Boundary (topology)|contours]]. These plans may also show cultural details such as buildings, fences, roads, and [[groundcover|ground cover]]. Topographical plans are used in the design of [[Subdivision (land)|subdivisions]], roads, and for residential planning purposes.<ref name="Topographical Survey">[http://www.adobeinc.com/faq/what-topographic-survey-and-when-it-needed Adobe Associates Inc. "Topographical Survey." Accessed June 1, 2016]</ref>

===Control Surveys===

Control surveys can be carried out to inform the following: determination of scales for photogrammetric mapping, establishment of precise [[Geographic coordinate system|coordinates]] based on a universal mapping system, establishment of sites for hydrographic charts, mine construction, or the routing of highways and transmission lines. [[Global Positioning System]] (GPS) technology increases the effectiveness of control surveying.

===Building Location Certificates===
Surveys for the purpose of providing a certificate of building location on a particular parcel of land may be undertaken by a BCLS. These surveys are normally carried out for the protection of an owner, and are generally accepted as proof that there are no encroachment or [[zoning]] placement problems.<ref name="Location Certificates">[http://www.bcsurveyors.ca/building-location-certificate/ Axis Land Surveying. "Location Certificates." Accessed June 1, 2016]</ref>

===Site Planning, Subdivision Design, and Consulting===

A BCLS is qualified to carry out [[subdivision (land)|subdivision]] design and all matters of parcel division in accordance with local, provincial and/or regional zoning and development by-laws.<ref name="Land Surveyors Act"/>

===Survey and Property Law===
BCLSs can provide [[expert witness]] [[testimony]] in matters of [[lawsuit|litigation]], where survey or boundary determinations are in question. The BCLS and a land surveying firm authorized to practice under permit must assume full responsibility for the correctness, validity, and accuracy of their own surveys including [[raw data]] and [[fieldnotes|field notes]] collected.<ref name="Land Surveyors">[https://www.saskcareers.ca/occupation/2301/Land%20Surveyor/about-the-job Sask Careers. "Land Surveyors."  Accessed June 1, 2016]</ref>

===Advanced Geomatics===

Many BC land surveyors also have skills, expertise and experience in advanced [[geomatics]] and survey applications.  This can include precise surveys for machine alignment and deformation analysis, mapping and GIS (Geographic Information Systems) expertise, remote sensing, real-time GPS (Global Positioning System) surveys, mapping, and hydrographic surveys.<ref name="Geomatics"/>

==Practice Advisory Department==

The Practice Advisory Department (PAD) is a division of the ABCLS which exists to promote the professional competency of land surveyors, which in turn protects the public interest. The protection of the public interest is of the utmost importance to the ABCLS, as framed in the Association's Mission Statement (see above). The PAD strives to monitor the competence level of the ABCLS membership while considering the expectations of the public, government, and ABCLS members. The PAD aims to ensure that all land surveys completed in British Columbia meet a high quality of standards, and that the overall integrity of the province's cadastre is maintained. In order to achieve these goals, the PAD initiated the Practice Review Program, which consists of two primary activities: an Annual Plan Review and a Practice Review.

The Annual Plan Review sees the yearly review of at least one completed survey plan by all practising BCLSs. This approach provides the valuable functions of:
*monitoring the performance of all practising BCLSs who produce statute plans
*monitoring and understanding trends in performance issues common to the profession
*producing performance statistics useful for evaluating member performance
*providing a venue for dialogue with members to clarify issues

The Practice Review component of the PAD is a means of addressing any shortcomings in practitioner's work, focusing on members who require assistance, education, or mentoring. Newly commissioned BCLSs are also subject to Practice Reviews with the aim of establishing an individual baseline and ensuring that their practice has the essential elements of a sustainable professional practice.<ref name="ABCLS Bylaws">[http://www.abcls.ca/wp-content/uploads/pdfs/2016_Bylaws_Approved_16-03-04.pdf ABCLS Webpage. "Bylaws." June 2, 2016]</ref>

== McVittie House ==

Thomas Thane McVittie was born in [[Toronto]], [[Ontario]] on May 5, 1858. He moved to British Columbia in 1879 and opened a land surveying office in Galbraith's Ferry in 1881. The town was renamed [[Fort Steele]] in 1888.<ref name="Fort Steele">[http://fortsteele.ca/our-story/history/ Fort Steele Heritage Town. "History." Accessed June 23, 2016]</ref> McVittie became a well-known local British Columbia Land Surveyor in the East Kootenay region from 1881 to 1918. He was responsible not only for the surveying of the Fort Steele [[townsite]], but for numerous other [[East Kootenay|Regional District of East Kootenay]] townsites and mineral claims as well. In addition to his land surveying duties, McVittie was a [[churchwarden|church warden]], [[Board of education|school trustee]], townsite agent, [[Justice of the Peace]], secretary of the Fort Steele Mining Association, and president of the liberal-conservative club.  McVittie renovated the one room cabin that he had built to make a home for his bride who would be joining him from New York. Renovations were completed by early November 1899, and McVittie and Anna Galbraith were married in [[Calgary]], [[Alberta]] in late December of that year. Anna Galbraith was the niece of Fort Steele's most prominent citizen and McVittie's good friend [[Robert Leslie Thomas Galbraith|Robert Galbraith]].<ref name="McVittie House">[http://www.cranbrooktownsman.com/ourtown/315872261.html "McVittie House, Land Surveying Office open at Fort Steele." ''The Townsman'', July 16, 2015]</ref>

=== Restoration project ===
In 1993, ABCLS entered into a partnership with Fort Steele Heritage Town to move the historic home and land surveying office of Thomas Thane McVittie to the Heritage Town site across [[British Columbia Highway 93]]-[[British Columbia Highway 95|95]]. The project was made possible through the joint financial cooperation of the Association of BC Land Surveyors, Fort Steele Heritage Town, Friends of Fort Steele, and the [[Teck Resources|Teck Corporation]]. Hundreds of hours of restoration efforts by Association members and interested volunteers resulted in several improvements and updates being carried out on the structures. A grand opening ceremony was held on July 11, 2015.<ref name="McVittie House"/>

==See also==
*[[Association of Canada Lands Surveyors]]
*[[Dominion Land Survey]]
*[[Land Title and Survey Authority]]

==References==
{{Reflist|30em}}

{{DEFAULTSORT:Association fnBritish Columbia Land Surveyors}}
[[Category:Professional associations based in Canada]]
[[Category:Surveying organizations]]
[[Category:Surveyors]]
[[Category:Canadian surveyors]]
[[Category:Land management]]