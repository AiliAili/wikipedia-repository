{{Infobox college sports rivalry
| name = Clemson–Georgia Tech football rivalry
| team1_image = Clemson University Tiger Paw logo.svg
| team1_image_size = 100
| team2_image = Georgia Tech Outline Interlocking logo.svg
| team2_image_size = 100
| team1 = [[Clemson Tigers football|Clemson Tigers]]
| team2 = [[Georgia Tech Yellow Jackets football|Georgia Tech]]
| total_meetings = 81
| series_record = Georgia Tech leads, 50–29–2
| first_meeting_date = November 24, 1898
| first_meeting_result = Clemson 23, Georgia Tech 0
| last_meeting_date = September 22, 2016
| last_meeting_result = Clemson 26, Georgia Tech 7
| next_meeting_date = 2017 in Clemson
| largest_win_margin = Clemson, 73–0 ([[1903 Clemson Tigers football team|1903]])
| longest_win_streak = Georgia Tech, 15 ([[1908 Georgia Tech Yellow Jackets football team|1908]]–1934)
| current_win_streak = Clemson, 2 ([[2015 Clemson Tigers football team|2015]]–present)
}}

The '''Clemson–Georgia Tech football rivalry''' is an American [[college football]] [[college rivalry|rivalry]] between the [[Clemson Tigers football]] team of [[Clemson University]] and [[Georgia Tech Yellow Jackets football]] team of [[Georgia Institute of Technology|Georgia Tech]].  Both schools are members of the [[Atlantic Coast Conference]].  Since conference expansion in 2005, Clemson represents the Atlantic Division while Georgia Tech plays in the Coastal Division, and are currently designated as cross-divisional rivals.

The ACC series between the schools, beginning in 1983 as a standard home-home series, has been very close and competitive, with Clemson currently leading the series 18-16.  Since 1899, all games prior to 1974 were played in [[Atlanta, Georgia]].

Both schools also have intense in-state rivalries against larger schools which both happen to be in the SEC: Georgia Tech's [[Clean, Old-Fashioned Hate]] rivalry with the [[Georgia Bulldogs football|Georgia Bulldogs]], and Clemson's [[Clemson–South Carolina rivalry#Football|Palmetto Bowl]] game against the [[South Carolina Gamecocks football|South Carolina Gamecocks]].

==Series history==
[[File:Georgia Tech Clemson 2007 Halftime.jpg|thumb|right|Halftime at the 2007 Clemson–Georgia Tech game at [[Bobby Dodd Stadium]]]]

Through 2016, the teams have played 81 times, with Tech leading the series 50–29–2, with 60 games played in Atlanta, and only 19 games played in Clemson's [[Memorial Stadium (Clemson)|Memorial Stadium]].<ref>{{cite web|url=http://football.stassen.com/cgi-bin/records/opp-opp.pl?start=1869&end=2013&team1=Clemson&team2=Georgia+Tech|title=Clemson vs Georgia Tech, 1869–2013|publisher=Stassen College Football Information|accessdate=2013-06-11}}</ref> The teams first met in 1898, when Clemson's third-year program defeated Georgia Tech 23–0 to finish with a 3–1 record. The following year, the Tigers beat Tech again, 41–5.<ref>{{cite news|first=Christopher|last=Kisco|url=http://www.highbeam.com/doc/1P1-23995879.html|title=Ga. Tech–Clemson rivalry steeped in tradition|work=[[The Tiger (Clemson University newspaper)|The Tiger]]|publisher=University Wire|date=1999-11-12}}</ref> In 1904, Georgia Tech lured away Clemson's head coach, [[John Heisman]] (namesake of the [[Heisman Trophy]]), with the prospect of $450 pay raise (${{formatnum:{{Inflation|US|450|1904|r=0}}}} [[inflation adjustment|adjusted for inflation]]), which was a 25% salary increase.<ref>{{cite news|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=CS&s_site=thestate&p_multi=CS&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EE338A6F47D84B3&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Carolina–Clemson: The Great Debate|work=[[The State (newspaper)|The State]]|pages=C12|date=2001-08-26}}</ref>

In 1977, Georgia Tech, a year before it joined the ACC, decided to end its series with Clemson. George Bennett, a Clemson athletics booster, was determined to preserve the game, as the trip to Atlanta provided a unique experience for the Tigers players and fanbase who had not been to a bowl game since [[1959 Bluebonnet Bowl|1959]]. In what was supposed to be the final game in Atlanta, upon Bennett's suggestion, thousands of Clemson supporters paid their expenses with [[United States two-dollar bill|two-dollar bill]]s stamped with the shape of a tiger paw. This demonstrated the large amount of money that the Clemson fanbase regularly pumped into the local economy because of the game.<ref name=bill>{{cite news|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=CS&s_site=thestate&p_multi=CS&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0FDA03A2CBEA0333&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Tech Fits the Bill for Tigers|work=[[The State (newspaper)|The State]]|pages=C1|date=2003-09-16}}</ref>

The series resumed in 1983 when Georgia Tech began playing football in the ACC. This has become one of the most competitive rivalries in the ACC with a record of 17-16, with Clemson currently leading the series by one game (excluding the vacated [[2009 ACC Championship Game]] won by Tech). These games have often been decided at the last minute and by small margins.<ref>{{cite news|url=http://new.accessnorthga.com/detail.php?n=161249|title=Another crazy chapter written in Clemson–Georgia Tech rivalry|publisher=AccessNorthGa.com|work=[[Associated Press]]|date=2004-09-12}}</ref> Nine of the games between 1996 and 2006 were decided by five points or less.<ref>{{cite news|url=http://www.usatoday.com/sports/college/football/games/2006-10-21-gatech-clemson_x.htm|title=Clemson jolts Georgia Tech 31–7, lands sixth win in a row|work=[[USA Today]]|date=2006-10-22|first=Pete|last=Iacobelli}}</ref>

When the [[Atlantic Coast Conference]]- ACC- [[2005 NCAA football realignment|reorganized in 2005]] to form [[Atlantic Coast Conference#Divisions|divisions]] for the sport of football, Clemson and Georgia Tech were designated as cross-division rivals.<ref>[http://www.theacc.com/sports/m-footbl/spec-rel/082105aad.html Atlantic Coast Conference Football Divisional Tiebreaker], Atlantic Coast Conference, August 21, 2005.</ref> This means that their football teams meet every season, unlike games between each team's other non-divisional conference opponents, which are played less often on a rotational basis.

In the 2009 season, both teams won their respective division in the ACC. For the first time in the series' history, the two teams met for a second time in a season on December 5, 2009 in the [[2009 ACC Championship Game|ACC Championship Game]]. The game marked the first ever December meeting between the two teams, as well as the first post-season meeting. It was also the first time the series has been played outside of [[Atlanta, GA|Atlanta]] or [[Clemson, SC|Clemson]] since 1899. Georgia Tech won 39–34; however, the NCAA later vacated the last 3 games of Georgia Tech's 2009 season along with the ACC Championship.  The NCAA determined that starting WR Demaryius Thomas should have been ruled ineligible ahead of the previous game for accepting $312 worth of clothing from a potential agent.  While the offense was minor, and the individual never proven to be an agent and the clothing returned, the NCAA ruled that Georgia Tech's athletic department had prepared the players prior to submitting statements, and was generally uncooperative with the NCAA investigation.  Therefore, all games following the alleged offense have been vacated, including the 2009 ACC championship game with Clemson.<ref>{{cite news|url=http://espn.go.com/college-sports/story/_/id/6769894/ncaa-places-georgia-tech-yellow-jackets-four-years-probation|title=NCAA places Georgia Tech on probation|work=[[ESPN]]|date=2011-07-18}}</ref>

In the 2011 season, both teams started 6–0.  Clemson had beaten previous national champion Auburn earlier in the season, and the match-up was highly anticipated until Georgia Tech stumbled losing two straight games, at Virginia and at Miami (FL).  Clemson's [[Andre Ellington]] was injured, and after 4 Clemson turnovers, Georgia Tech went on to upset the #5 Tigers 31-17.  Both teams went on to lose three games toward the end of the season, with the exception of Clemson's dominating performance over Coastal Division champion #5 Virginia Tech to clinch the [[2011 ACC Championship Game|2011 ACC Championship]].

==Game results==

''Clemson victories are shaded <span style="color:#F66733;">██</span> orange.  Georgia Tech victories are shaded <span style="color:#C59353;">██</span> gold.  Ties are  white.  Vacated is shown in <span style="color:#C0C0C0;">█</span> grey.''

{{Col-begin}}
{{Col-1-of-2}}
{| class="wikitable" style="font-size: 90%; text-align:center"
!Date
!Winner
!Score
!Location
|-style="background:#F66733; color:white" 
| November 24, 1898 || Clemson || 23–0 || Augusta, GA
|-style="background:#F66733; color:white" 
| November 30, 1899 || Clemson || 41–5 || Greenville, SC
|-style="background:#F66733; color:white" 
| October 18, 1902 || Clemson || 44–5 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 17, 1903 || Clemson || 73–0 || Atlanta, GA
|-
| November 5, 1904 || Tie || 11–11 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 30, 1905 || Georgia Tech || 17–10 || Atlanta, GA
|-style="background:#F66733; color:white" 
| November 29, 1906 || Clemson || 10–0 || Atlanta, GA
|-style="background:#F66733; color:white" 
| November 28, 1907 || Clemson || 6–5 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 26, 1908 || Georgia Tech || 30–6 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 25, 1909 || Georgia Tech || 29–3 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 24, 1910 || Georgia Tech || 34–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 30, 1911 || Georgia Tech || 32–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 28, 1912 || Georgia Tech || 20–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 27, 1913 || Georgia Tech || 34–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 26, 1914 || Georgia Tech || 26–6 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 5, 1918 || Georgia Tech || 28–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 9, 1919 || Georgia Tech || 28–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 6, 1920 || Georgia Tech || 7–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 5, 1921 || Georgia Tech || 48–7 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 4, 1922 || Georgia Tech || 21–7 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 1, 1932 || Georgia Tech || 32–14 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 30, 1933 || Georgia Tech || 39–2 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 29, 1934 || Georgia Tech || 12–7 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 31, 1936 || Clemson || 14–13 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 6, 1937 || Georgia Tech || 7–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 20, 1943 || Georgia Tech || 41–6 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 30, 1944 || Georgia Tech || 51–0 || Atlanta, GA
|-style="background:#F66733; color:white" 
| November 24, 1945 || Clemson || 21–7 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 7, 1953 || Georgia Tech || 20–7 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 8, 1958 || Georgia Tech || 13–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 3, 1959 || Georgia Tech || 16–6 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 22, 1962 || Georgia Tech || 26–9 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 28, 1963 || Georgia Tech || 27–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 3, 1964 || Georgia Tech || 14–7 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 2, 1965 || Georgia Tech || 38–6 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 1, 1966 || Georgia Tech || 13–12 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 7, 1967 || Georgia Tech || 10–0 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 5, 1968 || Georgia Tech || 24–21 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 4, 1969 || Clemson || 21–10 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 3, 1970 || Georgia Tech || 28–7 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 2, 1971 || Georgia Tech || 24–14 || Atlanta, GA
|}
{{Col-2-of-2}}
{| class="wikitable" style="font-size: 90%; text-align:center"
!Date
!Winner
!Score
!Location
|-style="background:#C59353; color:white" 
| October 7, 1972 || Georgia Tech || 31–9 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 29, 1973 || Georgia Tech || 29–21 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 28, 1974 || Clemson || 21–17 || Clemson, SC
|-style="background:#C59353; color:white" 
| September 27, 1975 || Georgia Tech || 33–28 || Atlanta, GA
|-
| September 25, 1976 || Tie || 24–24 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 24, 1977 || Clemson || 31–14 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 24, 1983 || Clemson || 41–14 || Clemson, SC
|-style="background:#C59353; color:white" 
| September 29, 1984 || Georgia Tech || 28–21 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 28, 1985 || Georgia Tech || 14–3 || Clemson, SC
|-style="background:#F66733; color:white" 
| September 27, 1986 || Clemson || 27–3 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 26, 1987 || Clemson || 33–12 || Clemson, SC
|-style="background:#F66733; color:white" 
| September 24, 1988 || Clemson || 30–13 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 14, 1989 || Georgia Tech || 30–14 || Clemson, SC
|-style="background:#C59353; color:white" 
| October 13, 1990 || Georgia Tech || 21–19 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 28, 1991 || Clemson || 9–7 || Clemson, SC
|-style="background:#C59353; color:white" 
| September 26, 1992 || Georgia Tech || 20–16 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 25, 1993 || Clemson || 16–13 || Clemson, SC
|-style="background:#F66733; color:white" 
| November 12, 1994 || Clemson || 20–10 || Clemson, SC
|-style="background:#F66733; color:white" 
| October 28, 1995 || Clemson || 24–3 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 19, 1996 || Clemson || 28–25 || Clemson, SC
|-style="background:#C59353; color:white" 
| September 27, 1997 || Georgia Tech || 23–20 || Atlanta, GA
|-style="background:#C59353; color:white" 
| November 12, 1998 || Georgia Tech || 24–21 || Clemson, SC
|-style="background:#C59353; color:white" 
| November 13, 1999 || Georgia Tech || 45–42 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 28, 2000 || Georgia Tech || 31–28 || Clemson, SC
|-style="background:#F66733; color:white" 
| September 29, 2001 || Clemson || 47–44 || Atlanta, GA
|-style="background:#F66733; color:white" 
| September 14, 2002 || Clemson || 24–19 || Clemson, SC
|-style="background:#F66733; color:white" 
| September 20, 2003 || Clemson || 39–3 || Atlanta, GA
|-style="background:#C59353; color:white" 
| September 11, 2004 || Georgia Tech || 28–24 || Clemson, SC
|-style="background:#C59353; color:white" 
| October 29, 2005 || Georgia Tech || 10–9 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 21, 2006 || Clemson || 31–7 || Clemson, SC
|-style="background:#C59353; color:white" 
| September 29, 2007 || Georgia Tech || 13–3 || Atlanta, GA
|-style="background:#C59353; color:white" 
| October 18, 2008 || Georgia Tech || 21–17 || Clemson, SC
|-style="background:#C59353; color:white" 
| September 10, 2009 || Georgia Tech || 30–27 || Atlanta, GA
|-style="background:#C0C0C0; color:white" 
| December 5, 2009 || Georgia Tech || 39–34 || Tampa, FL
|-style="background:#F66733; color:white" 
| October 23, 2010 || Clemson || 27–13 || Clemson, SC
|-style="background:#C59353; color:white" 
| October 29, 2011 || Georgia Tech || 31–17 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 6, 2012 || Clemson || 47–31 || Clemson, SC
|-style="background:#F66733; color:white" 
| November 14, 2013 || Clemson || 55–31 || Clemson, SC
|-style="background:#C59353; color:white" 
| November 15, 2014 || Georgia Tech || 28–6 || Atlanta, GA
|-style="background:#F66733; color:white" 
| October 10, 2015 || Clemson || 43–24 || Clemson, SC
|-style="background:#F66733; color:white"
| September 22, 2016 || Clemson || 26–7 || Atlanta, GA
|}
{{Col-end}}

==References==
{{reflist|2}}

==External links==
*Roy Martin, [http://www.thetigernet.com/view/story.do?id=5157 Clemson – Georgia Tech Preview], TigerNet.com, October 28, 2005.
*[http://nl.newsbank.com/nl-search/we/Archives?p_product=CS&s_site=thestate&p_multi=CS&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0F60BBA6709964C6&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM Ga. Tech – Clemson becomes heated rivalry], ''The State'', p.&nbsp;C1, September 13, 2002.
*Scott Michaux, [http://chronicle.augusta.com/stories/102206/mic_101574.shtml Tigers win the big matchup, but little decided], ''[[The Augusta Chronicle]]'', October 22, 2006.

{{Clemson Tigers football navbox}}
{{Georgia Tech Yellow Jackets football navbox}}
{{Atlantic Coast Conference rivalry navbox}}

{{DEFAULTSORT:Clemson-Georgia Tech football rivalry}}
[[Category:College football rivalries in the United States]]
[[Category:Clemson Tigers football]]
[[Category:Georgia Tech Yellow Jackets football]]