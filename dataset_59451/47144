'''Collateralized loan obligations''' ('''CLOs''') are a form of [[securitization]] where payments from multiple middle sized and large [[business loan]]s are pooled together and passed on to different classes of owners in various [[tranche]]s. A CLO is a type of [[collateralized debt obligation]].

==Leveraging==

Each class of owner may receive larger yields in exchange for being the first in line to risk losing money if the businesses fail to repay the loans that a CLO has purchased. The actual loans used are  multimillion-dollar loans to either privately or publicly owned enterprises.   Known as [[syndicated loan]]s and originated by a lead bank with the intention of the majority of the loans being immediately "syndicated", or sold, to the collateralized loan obligation owners. The lead bank retains a minority amount of the loan while usually maintaining "agent" responsibilities representing the interests of the syndicate of CLOs as well as servicing the loan payments to the syndicate (though the lead bank can designate another bank to assume the agent bank role upon syndication closing). The loans are usually termed "high risk", "high yield", or "leveraged", that is, loans to companies which owe an above average amount of money for their size and kind of business, usually because a new business owner has borrowed funds against the business to purchase it (known as a "[[leveraged buyout]]"), because the business has borrowed funds to buy another business, or because the enterprise borrowed funds to pay a dividend to equity owners.

==Rationale==

The reason behind the creation of CLOs was to increase the supply of willing business lenders, so as to lower the price (interest costs) of loans to businesses and to allow banks more often to immediately sell loans to external investor/lenders so as to facilitate the lending of money to business clients and earn fees with little to no risk to themselves. CLOs accomplish this through a 'tranche' structure. Instead of a regular lending situation where a lender can earn a fixed interest rate but be at risk for a loss if the business does not repay the loan, CLOs combine multiple loans but don't transmit the loan payments equally to the CLO owners. Instead, the owners are divided into different classes, called "tranches", with each class entitled to more of the interest payments than the next, but with them being ahead in line in absorbing any losses amongst the loan group due to the failure of the businesses to repay. Normally a leveraged loan would have a fixed interest rate, but potentially only a certain lender would feel that the risk of loss is worth the interest that is charged. By pooling multiple loans and dividing them into tranches, in effect multiple loans are created, with relatively safe ones being paid lower interest rates (designed to appeal to conservative investors), and higher risk ones appealing to higher risk investors (by offering a higher interest rate). The whole point is to lower the cost of money to businesses by increasing the supply of lenders (attracting both conservative and risk taking lenders).

CLOs were created because the same "tranching" structure was invented and proven to work for home mortgages in the early 1980s. Very early on, pools of residential home mortgages were turned into different tranches of bonds to appeal to various forms of investors. Corporations with good credit ratings were already able to borrow cheaply with bonds, but those that couldn't had to borrow from banks at higher costs. The CLO created a means by which companies with weaker credit ratings could borrow from institutions other than banks, lowering the overall cost of money to them.

==Demand==

As a result of the [[subprime mortgage crisis]], the demand for lending money either in the form of mortgage bonds or CLOs almost ground to a halt, with negligible issuance in 2008 and 2009.<ref>{{cite web|title=CLOs/Institutional Investors|url=http://www.leveragedloan.com/primer/#!institutional-investors|work=Leveraged Loan Primer {{!}} LCD}}</ref> 

The market for U.S. collateralized loan obligations was truly reborn in 2012, however, hitting $55.2 billion, with new-issue CLO volume quadrupling from the previous year, according to data from Royal Bank of Scotland analysts. Big names such as Barclays, RBS and Nomura launched their first deals since before the credit crisis; and smaller names such as Onex, Valcour, Kramer Van Kirk, and Och Ziff ventured for the first ever time into the CLO market, reflecting the rebounding of market confidence in CLOs as an investment vehicle.<ref>http://www.creditflux.com/Newsletter/2012-08-02/Citi-leads-charge-as-CLO-volumes-surge-past-last-yearrsquos-tally/</ref> CLO issuance has soared since then, culminating in full-year 2013 CLO issuance in the U.S. of $81.9 billion, the most since the pre-Lehman era of 2006-07, as a combination of rising interest rates and below-trend default rates drew significant amounts of capital to the leveraged loan asset class. <ref>{{cite news|title=2013 CLO Issuance Hits $81.9B; Most Since 2007|url=http://www.forbes.com/sites/spleverage/2014/01/02/2013-clo-issuance-hits-81-9b-most-since-2007/ | work=Forbes | first=Steve|last=Miller|date=2014-01-02}}</ref>

The US CLO market picked up even more steam in 2014, with $124.1 billion in issuance, easily surpassing the prior record of $97 billion in 2006. <ref>{{cite web|last1=Husband|first1=Sarah|title=U.S. CLO market prints record $123.6B of new issuance in 2014|url=http://www.leveragedloan.com/us-clo-issuance-hits-record-123-6b-in-2014/}}</ref>

==Risk retention==
In 2015, however, CLO issuance eased as investors eyed new risk-retention rules scheduled to go into effect at the end of 2016.<ref>{{cite web|title=Leveraged Loans: 2016 CLO Outlook – Credit Concerns and Risk Retention in Focus|url=http://www.leveragedloan.com/leveraged-loans-2016-clo-outlook-credit-concerns-and-risk-retention-in-focus/}}</ref> These rules, among other items, require managers to retain 5% of the entire size of the CLO.<ref>{{cite web|title=Credit Risk Retention|url=http://www.occ.gov/news-issuances/news-releases/2014/nr-occ-2014-139b.pdf|publisher=Office of the Comptroller of the Currency}}</ref>
Consequently, CLO issuance slipped to a still-healthy $97.34 billion in 2015, though it slowed considerably at the tail end of the year, and all but shut down in the early months of 2016, as the leveraged loan market battled investor resistance to risk in general, amid plunging oil prices.<ref>{{cite web|title=Loan Market Primer: CLOs|url=http://www.leveragedloan.com/primer/#!clos}}</ref>

==See also==
*[[Collateralized debt obligation]], securitization vehicle for various debt instruments
*[[Collateralized fund obligation]], securitization vehicle for private equity and hedge fund assets
*[[Collateralized mortgage obligation]], securitization vehicle for residential mortgages

==References==
{{Reflist|2}}
*[http://www.securitization.net/knowledge/transactions/coll_loan_obl.asp Collateralized Loan Obligations: A Powerful New Portfolio Management Tool for Banks]

{{structured finance}}

[[Category:Securities (finance)]]
[[Category:Mortgage]]
[[Category:Loans]]