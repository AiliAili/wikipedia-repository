[[File:Gamiani.jpg|thumb|One of the illustrations of ''Gamiani, or Two Nights of Excess'' published in Brussels in 1833. Illustration  by [[Achille Devéria]]]] 
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Gamiani, or Two Nights of Excess
| title_orig    = 
| translator    = 
| image         = <!--prefer 1st edition-->
| caption = 
| author        = [[Alfred de Musset]]
| illustrator   = [[Édouard-Henri Avril]]
| cover_artist  = 
| country       = France
| language      = French
| series        = 
| genre         = Erotic
| publisher     = 
| release_date  = 1833
| english_release_date =
| media_type    =
| pages         = 60 pages
| isbn =  978-1-59654-221-1
| preceded_by   = 
| followed_by   = 
}}
'''''Gamiani, or Two Nights of Excess''''' ({{lang-fr|Gamiani, ou deux nuits d'excès}}) is a French [[erotic novel]] first published in 1833. Its authorship is anonymous, but it is believed to have been written by [[Alfred de Musset]] and the [[lesbian]] eponymous heroine a portrait of his lover, [[George Sand]].<ref>{{cite book
  | last = Kendall-Davies
  | first = Barbara
  | title = The Life and Work of Pauline Viardot Garcia
  | publisher = Cambridge Scholars Press
  | year = 2003
  | pages = 45–46
  | url = https://books.google.com/books?id=zppg-I0Bw1UC&pg=PA46&d
  | isbn =1-904303-27-7 }}
</ref> It became a [[bestseller]] among nineteenth century erotic literature.<ref>{{cite book
  | last = Livia
  | first = Anna
  |author2=Kira Hall
  | title = Queerly Phrased: Language, Gender, and Sexuality
  | publisher = [[Oxford University Press]] US
  | year = 1997
  | url = https://books.google.com/books?id=wfmnli6hW_QC&d
  | isbn =0-19-510470-6
  | page = 157 }}</ref>

The novel was illustrated with unsigned lithographs whose authorship remains unknown. They have been attributed to [[Achille Devéria]] and [[Octave Tassaert]], among others.

==Outline==
Modeled after George Sand, this work gives us a young man named Alcide observing the Countess Gamiani and a young girl named Fanny, engaged in their lesbian bed. Having watched them and provoked by their abandonment, he reveals himself, joins them, and they spend the night alternately sharing their intimate histories and their bodies. The stories they tell include the rape of one in a monastery and the nearly fatal debauchment of another in a convent, as well as encounters with a number of animals, including an ape and a donkey.


==References==
{{Reflist}}

==External links==
*{{Wikisource-inline|Gamiani, or Two Passionate Nights|single=true}}

[[Category:1833 novels]]
[[Category:Novels by Alfred de Musset]]
[[Category:French erotic novels]]
[[Category:French LGBT novels]]
[[Category:Works published anonymously]]


{{1830s-novel-stub}}
{{erotic-novel-stub}}
{{LGBT-novel-stub}}