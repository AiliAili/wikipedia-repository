{{Infobox journal
| title = Future Microbiology
| cover = [[File: Future_Microbiology_cover.jpg|100px|alt=Cover]]
| former_name = <!-- or |former_names= -->
| abbreviation = Future Microbiol.
| discipline = [[Microbiology]]
| editor = Richard A. Calderone , B. Brett Finlay
| publisher = [[Future Medicine Ltd]]
| country = 
| history = 2006–present
| frequency = Monthly
| openaccess = 
| license = 
| impact = 3.637
| impact-year = 2015
| ISSN = 1746-0913
| eISSN = 1746-0921
| CODEN = FMUIAR
| JSTOR = 
| LCCN = 
| OCLC = 316181912
| website = http://www.futuremedicine.com/loi/fmb
| link1 =
| link1-name =
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
| boxwidth = 
}}
'''''Future Microbiology''''' is a monthly [[peer-reviewed]] [[medical journal]] that was established in 2006 and is published by Future Medicine. The [[editors-in-chief]] are Richard A. Calderone ([[Georgetown University]]) and B. Brett Finlay ([[University of British Columbia]]). The journal covers all aspects of the [[microbiological]] sciences, including [[virology]], [[bacteriology]], [[parasitology]], and [[mycology]].

== Abstracting and indexing==
The journal is abstracted and indexed in [[BIOSIS Previews]], [[Biotechnology Citation Index]], [[CAB Abstracts]], [[Chemical Abstracts]], [[Elsevier Biobase]], [[EMBASE]]/[[Excerpta Medica]], [[Index Medicus]]/[[MEDLINE]]/[[PubMed]], [[Science Citation Index Expanded]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.637, ranking it 35th out of 123 journals in the category "Microbiology".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Microbiology |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

==External links==
* {{Official website|http://www.futuremedicine.com/loi/fmb}}

[[Category:English-language journals]]
[[Category:Microbiology journals]]
[[Category:Publications established in 2006]]
[[Category:Monthly journals]]
[[Category:Future Science Group academic journals]]