{{Other people|Frank Andrews}}
{{Infobox military person
|honorific_prefix=[[Lieutenant general (United States)|Lieutenant General]]
|name=Frank Maxwell Andrews
|birth_date={{birth date|1884|02|03}}
|death_date={{death date and age|1943|05|03|1884|02|03}} 
|birth_place=[[Nashville, Tennessee|Nashville]], [[Tennessee]], [[United States]]
|death_place=Mount Fagradalsfjall, [[Kingdom of Iceland|Iceland]]
|image=FMApic2.jpg
| image_size=220
|caption=
|nickname="Andy"
|allegiance={{flag|United States|1912}}
|branch={{army|USA}}
|serviceyears=1906–1943
|rank=[[File:US-O9 insignia.svg|35px]] [[Lieutenant general (United States)|Lieutenant General]]
|commands= [[1st Operations Group|1st Pursuit Group]]<br>[[United States Army Air Corps#General Headquarters Air Force|General Headquarters Air Force]]<br>[[Sixth Air Force|Panama Air Force]]<br>[[Caribbean Defense Command]]<br>[[U.S. Army Forces in the Middle East]]<br>[[European Theater of Operations, United States Army|European Theater of Operations]]
|battles= [[World War I]]<br>[[World War II]]
|awards=[[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]]<br>[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]
}}
[[Lieutenant general (United States)|Lieutenant General]] '''Frank Maxwell Andrews''' (February 3, 1884&nbsp;&ndash; May 3, 1943) was a senior [[Officer (armed forces)|officer]] of the [[United States Army]] and one of the founders of the [[United States Army Air Forces]], which was later to become the [[United States Air Force]]. In leadership positions within the [[United States Army Air Corps|Army Air Corps]], he succeeded in advancing progress toward a separate and independent Air Force where predecessors and allies such as [[Billy Mitchell]] had failed. Andrews was the first head of a centralized American air force and the first air officer to serve on the Army's general staff. In early 1943, he took the place of [[General (United_States)|General]] [[Dwight D. Eisenhower]] as commander of all U.S. troops in the [[European Theater of Operations, United States Army|European Theater of Operations]].

General Andrews was killed in an airplane accident during an inspection tour in [[Iceland]] in 1943. He was the first of four lieutenant generals in the U.S. Army to die during the war, the others being [[Lesley J. McNair]], [[Simon Bolivar Buckner, Jr.]] and [[Millard Harmon]]. [[Joint Base Andrews]] in [[Maryland]] (formerly [[Andrews Air Force Base]]) is named after him, as well as Andrews Barracks (a [[kaserne]] in [[Berlin]], [[Germany]]), Frank Andrews Boulevard at [[Alexandria International Airport (Louisiana)|Alexandria International Airport]] (the former [[England AFB]]), in Louisiana, General Andrews Airport (demolished) in [[Santo Domingo]], [[Dominican Republic]], Andrews Engineering Building [[Eglin Air Force Base]], [[Andrews Avenue]] in [[Pasay]], [[Philippines]] and Andrews Theater at Keflavík Naval Base, Iceland.

==Early life and World War I==
Born in [[Nashville, Tennessee]], Andrews was the grandson of a cavalry soldier who fought alongside [[Nathan Bedford Forrest]] and the great-great-nephew of two Tennessee governors, [[John C. Brown]] and [[Neill S. Brown]].<ref>''Nashville Banner'', 5 May 1943</ref> He graduated from the city's [[Montgomery Bell Academy]] in 1901 and graduated from the [[United States Military Academy]] at West Point in 1906.<ref>Marquis Who's Who, Inc. ''Who Was Who in American History, the Military''. Chicago: Marquis Who's Who, 1975. P. 12  ISBN 0837932017 {{OCLC|657162692}}</ref>

Andrews graduated 42nd in his class and was commissioned a [[second lieutenant]] in the [[8th Cavalry Regiment (United States)|8th Cavalry]] on June 12, 1906, assigned to the [[Philippines]] from October 1906 to May 1907, and then to [[Fort Huachuca]], Arizona. In 1912, he was promoted to an available billet as a [[first lieutenant]] in the [[2nd Stryker Cavalry Regiment (United States)|2nd Cavalry]], at [[Fort Bliss]], Texas, and in 1916 received a promotion to [[captain (army)|captain]] in the regiment while at [[Plattsburgh Air Force Base|Plattsburgh Barracks]], New York.

The [[United States Army]] he joined was smaller than that of [[Bulgaria]], but it gave the young second lieutenant ample opportunities to play [[polo]], see the world (serving as [[aide-de-camp]] to Gen. [[Montgomery M. Macomb]] in [[Hawaii]] between 1911 and 1913), and observe the [[High politics|high]] and [[low politics]] of leadership. After marrying Jeannette "Johnny" Allen, the high-spirited daughter of Maj. Gen. [[Henry Tureman Allen]], in 1914, Andrews gained entrée into elite inner circles of Washington society and within the military.

A story related in the press many times during Andrews' lifetime claimed that Gen. Allen forestalled aeronautical aspirations of his future son-in-law by declaring that no daughter of his would marry a flyer. Andrews' service records, however, show that his commanding officer in the Second Cavalry vetoed his application for temporary aeronautical duty with the [[Army Signal Corps]] in February 1914, a decision that held firm despite a plea from the Chief Signal Officer's for reconsideration by higher-ups.

After the United States entered [[World War I]], Andrews was promoted to temporary [[major (rank)|major]] on August 5, 1917, and assigned over the objections of his cavalry commander to the [[Aviation Section, U.S. Signal Corps]] as part of its wartime expansion. After staff duty in [[Washington, D.C.]] in the Office of the Chief Signal Officer between September 26, 1917, and April 25, 1918, Andrews went to [[Rockwell Field]], California, for flying training. There, he earned a [[U.S. Air Force Aeronautical Ratings|rating of Junior Military Aviator]] at the age of 34. As with nearly all mid-career officers detailed to the Aviation Section, Andrews did not serve in France but as an administrator in the huge training establishment created to provide pilots. He commanded various training airfields in Texas and Florida, and served in the war plans division of the Army General Staff in Washington, D.C. Following the war, he replaced Brig. Gen. Billy Mitchell as Air Officer of the Army of Occupation in Germany, which his father-in-law, Gen. Allen, commanded. While in Germany, Andrews received his permanent establishment promotion to major, Cavalry, when the National Defense Act of 1920 took effect on July 1, and then transferred in grade to the [[United States Army Air Service|Air Service]], which the Act had made a [[combat arm]] of the Army, on August 6.

==Air Service and Air Corps duty==
After returning to the United States in 1923, Andrews again assumed command of [[Kelly Field]], Texas, and he became the first commandant of the advanced flying school established there. In 1927, he attended the [[Air Corps Tactical School]] at [[Langley Field]], Virginia, and the following year he went to the Army [[Command and General Staff School]] at [[Fort Leavenworth]], Kansas. Promoted to lieutenant colonel, Andrews served as the chief of the Army Air Corps' Training and Operations Division in 1930-1931 before being replaced by the new Chief of the Air Corps, Maj. Gen. [[Benjamin D. Foulois]]. He then commanded the [[1st Operations Group|1st Pursuit Group]] at [[Selfridge Field]], Michigan. After graduation from the [[United States Army War College|Army War College]] in 1933, Andrews returned to the General Staff in 1934.

In March 1935, Andrews was appointed by [[Chief of Staff of the United States Army|Army Chief of Staff]] [[Douglas MacArthur]] to command the newly formed [[United States Army Air Corps#GHQ Air Force|General Headquarters (GHQ) Air Force]], which consolidated all the Army Air Corps' tactical units under a single commander. The Army promoted Andrews to brigadier general (temporary) and to major general (temporary) less than a year later.

A vocal proponent of the four-engine heavy bomber in general and the [[B-17 Flying Fortress]] in particular, General Andrews advocated the purchase of the B-17 in large numbers as the Army's standard bomber. MacArthur, however, was replaced as Chief of Staff by Gen. [[Malin Craig]] in October 1935. Craig, who opposed any mission for the Air Corps except that of supporting ground forces, and the Army General Staff, actively opposing a movement for a separate air force, disagreed with Andrews that the B-17 had proven its superiority as a bomber over all other types. Instead it cut back on planned purchases of B-17s to procure smaller but cheaper (and inferior) twin-engine light and medium bombers such as the [[Douglas B-18]]. However, the war in Europe would soon prove the advocates of long range airpower correct.

==Later career, and World War II==
Andrews was passed over for appointment as Chief of the Air Corps following the death of Maj. Gen. [[Oscar Westover]] in September 1938, partly because of his aggressive support for strategic bombing.<ref>Arnold was selected over Andrews, who was senior, because he was the incumbent Assistant Chief of Air Corps, was well-qualified, and because Army Chief of Staff Craig threatened to resign if Arnold was not appointed.</ref> He became a trusted air adviser to [[George Marshall|George C. Marshall]], newly appointed as deputy chief of staff of the Army in 1938, but Andrews pushed too hard for the taste of more senior authorities.

In January 1939, after president Franklin D. Roosevelt had publicly called for a large expansion of the Air Corps, Andrews described the United States as a "sixth-rate airpower" at a speech to the National Aeronautic Association, antagonizing isolationist Secretary of War [[Harry Woodring]], who was then assuring the public of U.S. air strength. At the end of Andrews’ four-year term as Commanding General of GHQAF on March 1, he was not reappointed, reverted to his permanent rank of colonel, and was reassigned as air officer for the Eighth [[Corps Area]] in San Antonio, the same exile to which Billy Mitchell had been sent. Possibly expected to retire, he instead was recalled to Washington just four months later by Marshall after President Roosevelt named Marshall to serve as Chief of Staff following Craig's retirement. His first senior staff selection, Marshall's choice of Andrews and its permanent promotion to brigadier general prompted furious opposition from Woodring and others, over which Marshall prevailed after threatening to resign his new post. As Assistant Chief of Staff for Operations (G-3), he was in charge of readying the entire Army in the run-up to America’s inevitable involvement in the war.<ref>Copp, DeWitt S. (2003), ''Frank M. Andrews: Marshall's Airman'', Air Force History and Museums Program, Washington, D.C., pp. 16-17.</ref>

[[File:Lt. Gen. Andrews inspects the radio set at the C.P. of the Provisional Maneuver Force..jpg|thumb|left|250px|Lt. Gen. Andrews (in the middle) inspects a radio set at the Command Post of the Provisional Maneuver Force in Puerto Rico, November 1941. Next to him are generals: [[James Lawton Collins]] and [[Harry C. Ingles]].]]In 1940, Andrews assumed control of the Air Corps' [[Sixth Air Force|Panama Canal Air Force]], and in 1941, he became commander of the Caribbean Defense Command, which had the critically important duty during [[World War II]] of defending the southern approaches to the United States, including the vital [[Panama Canal]]. In February 1942, General Andrews was in Aruba and witnessed the German submarine [[Attack on Aruba|attack]] on the island. That same year he went to North Africa, where he spent three months in command of all United States forces in the Middle East from a base in Cairo.

At the [[Casablanca Conference]] in January 1943, Lieut. Gen. Andrews was appointed commander of all United States forces in the European Theater of Operations, replacing [[Dwight D. Eisenhower]]. In his memoirs, Gen. [[Henry H. Arnold]], commander of the Army Air Forces in World War II, expressed the belief that Andrews would have been given the command of the Allied invasion of Europe &mdash; the position that eventually went to Gen. Eisenhower.<ref>in Global Mission, 1949 memoir</ref> Gen. Marshall would say, late in life, that Andrews was the only general he had a chance to groom for a possible Supreme Allied Command later in the war.<ref>Pogue, Forrest C. George C. Marshall Interviews and Reminiscences for Forrest C. Pogue. Lexington, Virginia: George C. Marshall Research Foundation, 1991; pp. 565, 582.</ref>

However, on May 3, 1943, during an inspection tour, Lieut. Gen. Andrews was killed in crash of the ''[[Hot Stuff (aircraft)|Hot Stuff]]'', a [[B-24|B-24D-1-CO Liberator]], of the [[8th Air Force]] out of [[RAF Bovingdon]], England, on Mt. Fagradalsfjall on the [[Reykjanes]] peninsula after an aborted attempt to land at the [[Royal Air Force]] [[Royal Air Force Station|station]] [[RAF Kaldadarnes|Kaldadarnes]] (Iceland). Andrews and thirteen others died in the crash; only the tail gunner, S/Sgt. George A. Eisel of Columbus, Ohio, survived. Others killed in the crash included [[Adna Wright Leonard]], presiding Methodist bishop of North America, who was on a pastoral tour; Chaplains Col. Frank L. Miller (Washington, D.C.) and Maj. Robert H. Humphrey (Lynchburg, Va.), accompanying Bishop Leonard; Brig. Gen. Charles M. Barth (hometown Walter, Minn.), Andrews' chief of staff; Col. Morrow Krum (Lake Forest, Ill.), press officer for the ETO; Lt. Col. Fred A. Chapman (Grove Hill, Ala.) and Maj. Theodore C. Totman (Jamestown, N.Y.), senior aides to Andrews; pilot Capt. Robert H. Shannon (Washington, Iowa), of the 330th BS, 93rd BG; Capt. Joseph T. Johnson (Los Angeles); navigator Capt. James E. Gott (Berea, Ky.); Master Sgt. [[George C. Weir]] (McRae, Ark.); Tech. Sgt. Kenneth A. Jeffers (Oriskany Falls, N.Y.); and Staff Sgt. Paul H. McQueen (Endwell, N.Y.).<ref>"Toll in Iceland Accident Now 14; Storm Warning Went Unheeded," ''Washington Post'', May 6, 1943.</ref> 
[[File:FMA crash photo1.JPG|thumb|300px|right|U.S. Army personnel remove bodies from the wreckage of Andrews' B-24 after it struck a mountainside in Iceland, May 1943.]]

The B-24D Liberator that crashed, named "Hot Stuff", is believed to be the real first heavy bomber in the 8th Air Force to complete 25 missions. It flew her 25th mission on 7 February 1943, three-and-a-half months before "[[Memphis_Belle_(aircraft)|Memphis Belle]]", but as it was destroyed in the crash, the War Department chose to send the B-17 home and celebrate it as the first one.

Andrews was the highest-ranking Allied officer to die in the line of duty to that time in the war.<ref>Copp, DeWitt S., "Forged In Fire", The Air Force Historical Foundation, Doubleday & Company,  Garden City, New York, 1982, Library of Congress card number 81-43265, ISBN 0-385-15911-0, pages 393-395.</ref> At the time of his death, he was Commanding General, United States Forces, European Theatre of Operations. [[Camp Springs Army Air Field]], [[Maryland]], was renamed [[Joint Base Andrews Naval Air Facility|Andrews Field]] (later [[Joint Base Andrews Naval Air Facility]]), for him on 7 February 1945.<ref name="Mueller">Mueller, Robert, "Air Force Bases Volume 1: Active Air Force Bases Within the United States of America on 17 September 1982", United States Air Force Historical Research Center, Office of Air Force History, Washington, D.C., 1989, ISBN 0-912799-53-6, page 5.</ref><ref>{{webarchive |url=https://web.archive.org/web/20060709024035/www.wpafb.af.mil/museum/afp/b24crash.htm |date=July 9, 2006 }}</ref>
 
Andrews is buried at [[Arlington National Cemetery]].
[[File:FMA grave-1.jpg|thumb|300px|right|Andrews' grave at [[Arlington National Cemetery]].]]

==Legacy==
[[Joint Base Andrews]], located a few miles southeast of [[Washington, D.C.]] and the home base of [[Air Force One]], is named in honor of Andrews.

A Royal Air Force airfield called RAF Station Great Saling, in England was renamed after him, [[RAF Andrews Field|Andrews Field]], in [[Essex]] [[England]].  It was the first airfield constructed in 1943 by army engineers in the [[United Kingdom]] during World War II.  It was notable as having been the only renamed US airfield in the United Kingdom during World War II.  It was used by the USAAF 96th Bombardment Group (Heavy) and the 322nd Bombardment Group (Medium) during the war, and also by several RAF squadrons before being closed in 1946.  Today, a small part of the former wartime airfield is still in use as a small private flying facility.

Andrews Avenue, a road leading to the [[Philippines]]' [[Ninoy Aquino International Airport]] Terminal 3 was named after him.

==See also==
{{Portal|United States Army|World War II}}

==References==
{{Reflist}}
*Biography from Wright-Patterson Air Force Base museum. Public domain. {{webarchive |url=https://web.archive.org/web/20051127212436/http://www.wpafb.af.mil/museum/afp/afp1299.htm |date=November 27, 2005 }}
*Andrews' pre-World War I personnel file: File #1139074, Record Group 94, National Archives, Washington, D.C.
*Larry I. Bland, ed., ''George C. Marshall Interviews and Reminiscences for Forrest C. Pogue'' (Lexington, VA: George C. Marshall Research Foundation, 1991), pp.&nbsp;510, 582.
*On the "Hot Stuff" plane: http://www.warhistoryonline.com/war-articles/memphis-belle-v-b-24-hot-stuff-history-came-celebrate-wrong-wwii-warbird.html

==External links==
*[http://permanent.access.gpo.gov/airforcehistory/www.airforcehistory.hq.af.mil/Publications/fulltext/FrankMAndrews.pdf DeWitt S. Copp, ''Frank M. Andrews: Marshall's Airman'' (2003)]
*{{Find a Grave|9116688|accessdate=2008-02-10}}

{{S-start}}
{{S-mil}}
{{Succession box|
 title=Commanding General of U.S. Army Europe|
 before= [[Dwight D. Eisenhower]] |
 years= 4 February 1943 - 3 May 1943 |
 after= [[Jacob L. Devers]]
}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Andrews, Frank Maxwell}}
[[Category:1884 births]]
[[Category:1943 deaths]]
[[Category:Aerial warfare pioneers]]
[[Category:American military personnel of World War II]]
[[Category:American military personnel killed in World War II]]
[[Category:American aviators]]
[[Category:Aviators from Tennessee]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:Aviators killed in aviation accidents or incidents]]
[[Category:People from Nashville, Tennessee]]
[[Category:United States Military Academy alumni]]
[[Category:Victims of aviation accidents or incidents in Iceland]]
[[Category:United States Army Command and General Staff College alumni]]
[[Category:Air Corps Tactical School alumni]]
[[Category:United States Army Air Forces generals]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]