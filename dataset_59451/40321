{{Use mdy dates|date=January 2015}}
{{Infobox building
| name                = Deming Armory
| native_name         = 
| native_name_lang    = 
| image               = Deming Luna Mimbres Museum.jpg
| image_alt           = The red-brick front facade of the Deming Armory
| image_caption       = Deming Armory in 2009
| former_names        = State Armory
| alternate_names     = Deming Luna Mimbres Museum
| status              = Used as a museum
| building_type       = Government [[Armory (military)|armory]]
| architectural_style = Castellated style<ref name="Still Serving">{{cite book|last=Super|first=David|title=Still Serving: Reusing America's Historic National Guard Armories|year=2000|publisher=National Trust for Historic Preservation, The National Guard Bureau|pages=25–26|url=http://www.denix.osd.mil/cr/upload/STILL_SERVING_HIGH-RES_0.PDF}}</ref> 
| address             = 301 S. Silver Ave.
| location_town       = Deming, New Mexico
| location_country    = United States
| current_tenants     = Deming Luna Mimbres Museum
| start_date          = 1915
| completion_date     = 1916
| renovation_date     = 
| demolition_date     = 
| cost                = 
| ren_cost            = 
| client              = United States Department of the Army 
| owner               = Luna County Historical Society, Inc. (current)
| other_dimensions    = {{convert|50|ft}} across x {{convert|132|ft}} deep
| structural_system   = 
| architect           = Henry C. Trost
| architecture_firm   = [[Trost & Trost]]
| main_contractor     = W. W. Barracks
| awards              = 
| designations        = 
| embedded            = 
 {{Infobox NRHP
 | embed = yes
 | name = Deming Armory
 | coordinates = {{coord|32|15|59|N|107|45|21|W|display=inline,title}}
| locmapin = New Mexico
 | map_alt = Located in New Mexico, in the southwest portion of the state
 | map_caption = Location in New Mexico
 | architect = [[Henry Trost|Henry C. Trost]]
 | refnum = 83001624<ref name="nris">{{NRISref|version=2010a}}</ref> 
 | added = April 21, 1983
 | designated_other1 = N.M. State Register of Cultural Properties
 | designated_other1_date = January 20, 1978
 | designated_other1_number = [http://www.nmhistoricpreservation.org/assets/files/registers/2012%20Report_%20Section%203_%20Arranged%20by%20Number.pdf 584]
 | designated_other1_num_position = bottom
 }}
}}

The '''Deming Armory''' (formerly known as the '''State Armory''', now also known as the '''Deming Luna Mimbres Museum''') is a historic [[Armory (military)|armory]] in the [[United States]], located at 301 South Silver Avenue in [[Deming, New Mexico|Deming]], [[Luna County, New Mexico]].  The building was built for the [[United States Department of the Army]] in 1915–16, and is currently being used as the premises for the [[local museum]] in Deming.<ref name=Museum>{{cite web|title=Museum History|url=http://www.lunacountyhistoricalsociety.com/museum-history|work=Deming Luna Mimbres Museum|publisher=Luna County Historical Society|accessdate=August 3, 2013}}</ref>   The armory was added to the [[New Mexico State Register of Cultural Properties]] in 1978 and was listed on the [[National Register of Historic Places]] in 1983.

==History==
The building, designed by architect [[Trost & Trost|Henry C. Trost]], was originally constructed as a [[National Guard of the United States|National Guard]] armory. The building was the first armory to be built after New Mexico became a state in 1912.<ref name=trostorg>{{cite web|title=Deming Armory|url=http://www.henrytrost.org/buildings/deming-armory/|publisher=Henry C. Trost Historical Organization|accessdate=August 3, 2013}}</ref>  It was constructed to train soldiers to guard the border with Mexico during the ongoing [[Mexican Revolution]].<ref name=trostorg />

The armory served the [[New Mexico National Guard]] for 60 years.  The building housed a chapter of the [[United Service Organizations|USO]] during [[World War II]], in support of the men training at [[Deming Municipal Airport|Deming Army Airfield]]. During the years that the building served as a military facility, it was also used as a community center for hosting dances, basketball games and other local events.<ref name="Still Serving"/>

Two events in 1976 contributed towards the launch of a campaign to acquire the Deming Armory as new premises for the town's museum.  First, the National Guard vacated the building and moved to a new facility on the south side of Deming.  Second, a local businessman, Hubert Ruebush, offered to donate his mother's old electric [[washing machine]], the first such washer in Deming, to the museum.  When Ruebush learned that the museum could not accommodate the washer in its cramped facilities, he donated $6000 to acquire the armory on the condition that his donation be matched through local [[fundraising]].  Ruebush's condition was quickly satisfied, and a delegation from Deming subsequently negotiated a selling price of $11,500 for the building with the State Armory Board in [[Santa Fe, New Mexico|Santa Fe]].<ref name="Still Serving"/>

==Architecture==
The Deming Armory is an imposing red-brick building, two stories in height and resting on a raised basement.  It comprises three [[Bay (architecture)|bay]]s along the front elevation, with a depth of nine bays, and [[pilaster]]s divide the side elevations.  The facade is characterized by decorative [[brickwork]], concrete detailing, and a large central arch leading to double-leaf doors.<ref>{{cite web|title=Downtown Deming Historic District |url=http://www.nmhistoricpreservation.org/assets/files/register-nominations/Downtown%20Deming%20Historic%20District_NRHP%20nomination.pdf |work=National Register of Historic Places Registration |publisher=National Park Service |accessdate=August 3, 2013 |page=16 |year=2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20150104072022/http://www.nmhistoricpreservation.org/assets/files/register-nominations/Downtown%20Deming%20Historic%20District_NRHP%20nomination.pdf |archivedate=January 4, 2015 |df=mdy }}</ref>  The walls of the building are {{convert|13|in}} thick.<ref name="Museum"/>

==Deming Luna Mimbres Museum==
The Deming Luna Mimbres Museum, also known as the Deming Luna County Museum, is operated by the Luna County Historical Society.  The museum's exhibits focus on the everyday lives and history of area residents.  Collections include [[Mimbres pottery]], dolls and toys, glass, china, crystal, teapots, silver, copper and Depression glass, and saddles and ranching equipment.  Other exhibits include period rooms, furniture, paintings, tools, clothes, Native American artifacts, gems and geodes, weapons, war memorabilia, vehicles and farm equipment.<ref>{{cite web|title=Museum Highlights|url=http://www.lunacountyhistoricalsociety.com/museum-highlights|publisher=Deming Luna Mimbres Museum|accessdate=April 3, 2014}}</ref>

==See also==
{{Portal box|NRHP|New Mexico}}
*[[National Register of Historic Places listings in Luna County, New Mexico]]

==References==
{{reflist}}

==External links==
*[http://www.lunacountyhistoricalsociety.com/ Deming Luna Mimbres Museum] - Luna County Historical Society

{{National Register of Historic Places}}

[[Category:Museums in Luna County, New Mexico]]
[[Category:History museums in New Mexico]]
[[Category:Armories on the National Register of Historic Places in New Mexico]]
[[Category:Government buildings completed in 1916]]
[[Category:New Mexico State Register of Cultural Properties]]
[[Category:Trost & Trost buildings]]
[[Category:National Register of Historic Places in Luna County, New Mexico]]