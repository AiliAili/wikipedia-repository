{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Clara in Blunderland
| title_orig   = 
| translator   = 
| image         = Clara-in-blunderland-cover-1902.png<!--prefer 1st edition - It is the first edition-->
| caption = First edition cover of ''Clara in Blunderland''
| author       = [[Edward Harold Begbie|Caroline Lewis]]
| cover_artist = 
| country       = United Kingdom
| language      = English
| genre        = [[Fantasy novel]], [[Parody]]
| publisher    = [[William Heinemann]]
| release_date = 1902
| media_type   = Print (hardback)
| pages        = xvi, 150
| preceded_by  = 
| followed_by  = [[Lost in Blunderland]]
}}

'''''Clara in Blunderland'''''  is a novel by Caroline Lewis (pseudonym for [[Edward Harold Begbie]], J. Stafford Ransome, and M. H. Temple), written in 1902 and published by William Heinemann of London. It is a political [[parody]] of [[Lewis Carroll]]'s two books, ''[[Alice's Adventures in Wonderland]]'' and ''[[Through the Looking-Glass]]''. The book was followed a year later by a sequel, ''[[Lost in Blunderland]]''.

The book is critical of the British Government's engagement in the [[Second Boer War]]. A number of prominent politicians are represented by characters from the "Alice" books: Clara (the equivalent of Alice) represents [[Arthur Balfour]], the [[Leader of the House of Commons]];<ref>Sigler, Carolyn, ed. 1997. ''Alternative Alices: Visions and Revisions of Lewis Carroll's "Alice" Books.'' Lexington, KY, University Press of Kentucky. Pp. 340-347</ref> the [[Red Queen (Through the Looking Glass)|Red Queen]] is [[Joseph Chamberlain]], the [[Duchess (Alice's Adventures in Wonderland)|Duchess]] is [[Robert Cecil, 3rd Marquess of Salisbury|Robert Cecil]], [[Humpty Dumpty#In Through the Looking-Glass|Crumpty-Bumpty]] is [[Henry Campbell-Bannerman]], the [[The Walrus and the Carpenter|Walrus]] is [[William Vernon Harcourt (politician)|William Vernon Harcourt]], the [[Cheshire Cat|Dalmeny Cat]] is [[Archibald Philip Primrose|Lord Rosebery]],<ref>Dickinson, Evelyn. 1902. "Literary Note and Books of the Month", in ''United Australia'', Vol. II, No. 12, June 20, 1902</ref> and the [[Caterpillar (Alice's Adventures in Wonderland)|Caterpillar]] is [[Winston Churchill]].

The book features 40 drawings after the originals by [[John Tenniel]] which were drawn by journalist J. Stafford Ransome, credited as "S.R.".

[[File:Clara-in-blunderland-cover-2010.png|150px|left|thumb|2010 edition cover of ''Clara in Blunderland'']]

==Index==
* Clara felt very listless i 1
* Time Time t 7
* Chestnut Land 14
* You thought you could shoot that wildebeeste on your 22
* She came across a little house 29
* The Goosebaby 35
* Get on to Joe 58

==Bibliography==
*Lewis, Caroline (2010) [http://www.evertype.com/books/clara.html ''Clara in Blunderland'']. Evertype. ISBN 978-1-904808-49-7

==Notes==
{{reflist}}

{{alice}}

[[Category:1902 novels]]
[[Category:Books based on Alice in Wonderland]]
[[Category:British fantasy novels]]
[[Category:Works published under a pseudonym]]
[[Category:Novels by Edward Harold Begbie]]


{{1900s-fantasy-novel-stub}}