{{Use dmy dates|date=August 2014}}
{{Infobox military conflict
| conflict    = Battle of Beirut
| partof      = the [[Italo-Turkish War]]
| image       = [[File:BattleOfBeirut1912.jpg|300px]]
| caption     = ''A sketch of warship positions during the Battle of Beirut.''
| date        = 24 February 1912
| place       = off [[Beirut]], [[Ottoman Empire]]
| result      = Italian victory
| combatant1  = {{flagicon|Kingdom of Italy}} [[Kingdom of Italy (1861-1946)|Italy]]
| combatant2  = {{flag|Ottoman Empire}}
| commander1  = {{flagicon|Kingdom of Italy}} [[Paolo Thaon di Revel]]
| commander2  = unknown
| strength1   = 2 [[armoured cruiser]]s
| strength2   = 1 [[corvette]]<br>1 [[torpedo boat]]
| casualties1 = none 
| casualties2 = 1 corvette sunk<br> 1 torpedo boat sunk<br>6 [[Lighter (barge)|lighter]]s sunk
| notes = '''Civilian Casualties:''' 66 killed
}}
{{Campaignbox Italo-Turkish War}}

The '''Battle of Beirut''' was a [[naval battle]] off the coast of [[Beirut]] during the [[Italo-Turkish War]]. Italian fears that the [[Ottoman Empire|Ottoman]] naval forces at Beirut could be used to threaten the approach to the [[Suez canal]] led the Italian military to order the destruction of the Ottoman naval presence in the area. On 24 February 1912 two Italian [[armoured cruiser]]s attacked and sank an Ottoman casemate [[corvette]] and six [[Lighter (barge)|lighter]]s, retired, then returned and sank an Ottoman [[torpedo boat]]. 

As a result of the battle all [[Ottoman Navy|Ottoman naval forces]] in the region were annihilated, thus ensuring the approaches to the Suez Canal were open to the Italians. Besides the naval losses, the city of Beirut itself suffered significant damage from the Italian warships.

==Background==
During the Italo-Turkish War, the [[Italian military]] feared that Ottoman naval forces in the Mediterranean would stage raid on the Italian supply and [[troopship]]s headed for [[Italian East Africa]]. In order to prevent such a raid, Rear Admiral [[Paolo Thaon di Revel]] was ordered to clear the harbor of Beirut of what Ottoman naval vessels he might find there. Revel's force consisted of two armoured cruisers: [[Italian cruiser Giuseppe Garibaldi (1899)|''Giuseppe Garibaldi'']] and [[Italian cruiser Francesco Ferruccio|''Francesco Ferruccio'']].<ref name="Earle, 1092">Earle 1912, p. 1092.</ref> Both cruisers were of the [[Giuseppe Garibaldi class cruiser|''Giuseppe Garibaldi'' class]] and armed with two 10&nbsp;inch guns in turrets, ten 6&nbsp;inch guns, six 4.7&nbsp;inch guns, ten 6-pounders, ten 1-pounders, 2 Maxim machine guns, and five torpedo tubes.<ref>Brassey 1898, p. 36.</ref> 

In contrast the Ottoman forces consisted of the casemate corvette {{ship|Ottoman ironclad|Avnillah||2}} and the torpedo boat ''[[Ottoman torpedo boat Angora|Angora]]''.<ref>Beehler 1913, p. 54.</ref> ''Angora'' was a relatively new vessel completed in 1906 and armed with two 37&nbsp;mm cannons as well as two 14&nbsp;inch torpedo tubes with a pair of torpedoes per tube.<ref>Gardiner 1985, p. 392.</ref> In contrast ''Avnillah'' was an antiquated ironclad corvette built in 1869. After a reconstruction was completed in 1907 she was armed with four [[3"/50 caliber gun|3-inch gun]]s and eight six pounders. In addition to her cannon she was also armed with a single 14&nbsp;inch [[torpedo tube]].<ref>Gardiner 1985, p. 389.</ref> Thus the Ottoman force was entirely outgunned by the Italians, giving them a severe disadvantage in the looming battle.

==Battle==
[[File:Beirut battle.jpg|thumb|left|An Italian cruiser shelling Ottoman vessels in Beirut harbor|alt=An Italian cruiser bombards two Ottoman ships with smoke billowing over the city.]]

The two Italian cruisers approached the harbor and fired a blank shot at the Ottoman vessels lying there.<ref name="Earle, 1092"/> Upon sighting the Italian ships, the Ottoman commander on ''Avnillah'' sent out a launch under a flag of truce to communicate with the enemy. While negotiating, the Ottoman commander ordered ''Angora'' to position itself near the harbor's [[Mole (architecture)|mole]]. At 07:30, Admiral Revel ordered the Ottoman launch to return with an ultimatum addressed to the [[Wāli]] of Beirut informing him to [[Surrender (military)|surrender]] his two warships by 09:00.<ref>{{cite book
  | last = 
  | first = 
  | authorlink = 

  | title = Article 5 – No Title
  | publisher = New York Times
  | date = 26 February 1912
  | location = 
  | page = 1
  | url = https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9B07E3D6173CE633A25755C2A9649C946396D6CF
  | doi = 
  | id = 
  | isbn =
| format=PDF}}</ref> The message was received by the Wali at 08:30. The Wali was in the process of issuing an order of surrender but this was not received by the Italians by the deadline. Accordingly, at 09:00, the Italians began their attack on the Ottoman ships in the harbor.<ref>Hidden 1912, p. 456.</ref>

At a distance of {{convert|6000|m|yd|abbr=off}}, the Italians opened fire upon the Ottoman corvette. The Ottomans returned fire ineffectively until 09:35 when the Italian gunfire set ''Avnillah'' afire. Receiving heavy damage and outgunned, the corvette [[Striking the colors|struck her colours]] and the crew abandoned ship. At this point ''Garibaldi'' sailed in close and engaged ''Angora'' at {{convert|600|m|yd|abbr=off}} with gunfire but failed to damage it.<ref name="Earle, 1092"/> ''Garibaldi'' then attempted to finish off ''Avnillah'' by firing a torpedo at her. However, the torpedo deviated from its trajectory and hit several lighters moored nearby, sinking six of them.<ref>Beehler 1913, p. 97.</ref> Undeterred, the Italian cruiser fired a second torpedo that struck the Ottoman corvette amidships. By 11:00 the corvette was sunk in shallow water and the pair of cruisers withdrew to the north.<ref>Beehler 1913, p. 55.</ref> The action was not over however; at 13:45, the Italian cruisers returned and once more engaged the Ottoman forces. The only warship left in the harbor was the torpedo boat ''Angora'' so ''Ferruccio'' moved in close and engaged it with gunfire for three minutes before it joined ''Avni-Illah'' at the bottom of Beirut's harbor. Once the fighting had ended the two Italian cruisers sailed off in a westward direction.<ref name="Earle, 1094">Earle 1912, p. 1094.</ref>

== Aftermath ==
The Ottoman naval presence at Beirut was completely annihilated, removing the only Turkish naval threat to Italian transports in the area and giving the Italians complete naval dominance of the southern [[Mediterranean Sea]] for the rest of the war. Casualties on the Ottoman side were heavy. Both Ottoman warships were sunk, with ''Avnillah'' alone taking 58 killed and 108 wounded. In contrast the Italian ships not only took no casualties, but no direct hits from the Ottoman warships as well.<ref>Beehler 1913, p. 106.</ref> The damage was not restricted to the Ottoman naval vessels present at Beirut, as the city took heavy damage as well. Stray shots from the cruisers decimated the city. Fires broke out as a direct result of the stray gunfire, destroying several banks and part of the city's [[customs house]] as well as other buildings. Combined from the fires and shelling, 66 civilians were killed in the city along with hundreds of others wounded.<ref name="Earle, 1094"/>

As retribution for the Italian actions at Beirut, four days after the battle the central Ottoman government ordered the Wilyets of Beirut, [[Aleppo]], and [[Damascus]] to expel all Italian citizens from their jurisdictions, resulting in the [[deportation]] of over 60,000 Italians from the region. Despite the retaliatory expulsion of Italian citizens from the area, the battle gave the Italian forces complete naval superiority in the approaches to the Suez Canal and Italian forces in Eritrea could now be reinforced without hesitation, eliminating much of the Ottoman threat to the region. Thus the battle was both a strategic and tactical Italian victory.<ref name="Earle, 1094"/>

== Notes ==
{{Reflist|20em}}

==References==
{{Commons category|Battle of Beirut (1912)}}
{{refbegin}}
*{{cite book
  | last = Beehler| first = William| authorlink =| title = The history of the Italian-Turkish War, September 29, 1911, to October 18, 1912| publisher = The Advertiser Republican | year = 1913| location =Annapolis| url = https://books.google.com/books?id=OWcoAAAAYAAJ&pg=PA58&dq=awn+illah#v=onepage&q=awn%20illah&f=false| doi = | id = | isbn = | oclc = 63576798 }}
*{{cite book
  | last = Brassey | first = T. A. | authorlink =| title = Naval Annual 1898| publisher = J. Griffin and Co.| year = 1898 | location = Portsmouth| url = https://books.google.com/books?id=KhcuAAAAYAAJ&pg=PA35&dq=Giuseppe+Garibaldi+cruiser#v=onepage&q=Giuseppe%20Garibaldi%20cruiser&f=false | id = | isbn = }}
*{{cite book
  | last = Earle| first = Ralph| authorlink = Ralph Earle (American naval officer)| title = Naval Institute proceedings, Volume 38| publisher = United States Naval Institute| year = 1912| location = | url = https://books.google.com/books?id=ZZzJoYNBHEEC&pg=PA1092&dq=beirut+angora#v=onepage&q=beirut%20angora&f=false| doi = | id = | isbn = }}
*{{cite book
  | last = Ellis| first = Raff | authorlink =| title = Kisses From a Distance| publisher = Cune Press| year = 2007 | location = Seattle| pages = | url = https://books.google.com/books?id=XfvuZeRUEckC&pg=PA129&dq=angora+torpedo+boat+beirut#v=onepage&q=angora%20torpedo%20boat%20beirut&f=false | doi = | id = | isbn =978-1-885942-45-6 }}
*{{cite book
  | last = Gardiner | first = Robert | title = Conway's all the world's fighting ships, 1906–1921| publisher = Conway Maritime Press| year = 1985 | location = London| pages = | url = https://books.google.com/books?id=V2r_TBjR2TYC&pg=PP4&dq=conway+all+the+worlds+fighting+ships#v=onepage&q=angora&f=false | doi = | id = | isbn = 978-0-87021-907-8}}
*{{cite book
  | last = Hidden| first = Andrew W. | authorlink =| title = The Ottoman Dynasty| publisher = Nicholas Hidden| year = 1912 | location = New York| pages = | url = https://books.google.com/books?id=ZVtFAAAAYAAJ&pg=PA452&dq=angora+torpedo+boat+beirut&lr=#v=onepage&q=angora%20torpedo%20boat%20beirut&f=false| doi = | id = | isbn = }}
{{refend}}

{{Ottoman battles in the 20th century}}

{{good article}}

{{coord|33.8869|N|35.5131|E|source:wikidata|display=title}}

{{DEFAULTSORT:Beirut, Battle of}}
[[Category:Conflicts in 1912]]
[[Category:1912 in the Ottoman Empire]]
[[Category:Naval battles and operations of the Italo-Turkish War|Beirut]]
[[Category:Maritime incidents in 1912]]
[[Category:Maritime incidents in Lebanon]]
[[Category:History of Beirut]]
[[Category:Battles of the Italo-Turkish War]]
[[Category:February 1912 events]]
[[Category:20th century in Beirut]]