The '''Astronomical Society of India''' ('''ASI''') is an [[India]]n society of professional [[astronomer]]s and other professionals from related disciplines. It was founded in 1972, with [[Vainu Bappu]] being the founder President of the Society, and as of 2010 has a membership of approximately 1000. Its registered office is at the Astronomy Department, [[Osmania University]], [[Hyderabad, India]]. Its primary objective is the promotion of Astronomy and related branches of science. It organises meetings, supports and tries to popularise Astronomy and related subjects and publishes the [[Bulletin of the Astronomical Society of India]].<ref>[http://www.astron-soc.in/index.html Astronomical Society of India] {{webarchive |url=https://web.archive.org/web/20110304111620/http://www.astron-soc.in/index.html |date=March 4, 2011 }}</ref>

Prof. Rajaram Nityananda of the National Centre for [[Radio astronomy|Radio Astrophysics]] at the Tata Institute of Fundamental Research, [[Neighborhoods of Pune|Ganeshkhind]], 
[[Pune]] is the Society's President.<ref>[http://www.astron-soc.in/executive.html The Executive Council of the Astronomical Society of India] {{webarchive |url=https://web.archive.org/web/20110304111443/http://www.astron-soc.in/executive.html |date=March 4, 2011 }}</ref>

The Society makes a series of awards, the most prestigious of which is the ''Prof. M. K. Vainu Bappu Gold Medal'' awarded once every two years to "honour exceptional contributions to Astronomy and Astrophysics by young scientists anywhere in the world."<ref name="awards">[http://www.astron-soc.in/awards.html Awards and Endowments instituted by the Astronomical Society of India] {{webarchive |url=https://web.archive.org/web/20110304110925/http://www.astron-soc.in/awards.html |date=March 4, 2011 }}</ref> Previous award winners include:
* 1986 [[Yasuo Fukui]]
* 1988 [[George Efstathiou]]<ref name="Publications2003">{{cite book|author=Europa Publications|title=The International Who's Who 2004|url=https://books.google.com/books?id=neKm1X6YPY0C&pg=PA480|year=2003|publisher=Psychology Press|isbn=978-1-85743-217-6|pages=480–}}</ref> and [[Shrinivas_Kulkarni]]
* 1990 [[D.J. Saikia]] and [[Dipankar Bhattacharya (astrophysicist)|Dipankar Bhattacharya]]
* 1992 Pawan Kumar
* 1994 [[Matthew Colless]] 
* 1996 [[Sarbani Basu]]
* 1998 [[Peter Martinez]]<ref>{{cite press release|url=http://old.saao.ac.za/no_cache/public-info/news/news/article/96/ |title=International Medal to South African astronomer |publisher=South African Astronomical Observatory |accessdate=June 10, 2015 |date=November 29, 2000 |deadurl=yes |archiveurl=https://web.archive.org/web/20150610171631/http://old.saao.ac.za/no_cache/public-info/news/news/article/96/ |archivedate=June 10, 2015 |df= }}</ref>
* 2000 [[Biswajit Paul (astronomer)|Biswajit Paul]] and [[Alycia J. Weinberger]]
* 2002 [[Brian Schmidt|Brian P. Schmidt]]<ref>{{cite journal|journal=Bulletin of the Astronomical Society of India|volume=32|number=1|page=83|url=http://adsabs.harvard.edu/full/2004BASI...32...83.|title=Society Matters|last=Ambastha|first=Ashok|date=March 2004|bibcode = 2004BASI...32...83. }}</ref>
* 2004 [[R. Srianand]] and [[Ray Jayawardhana]]<ref>{{cite news|newspaper=The Sunday Times|url=http://www.sundaytimes.lk/070218/Plus/023_pls.html|date=February 18, 2007|location=Sri Lanka|title=Lanka astronomer wins two top awards|issn=1391-0531|accessdate=June 5, 2015}}</ref>
* 2006 [[Banibrata Mukhopadhyay]]
* 2008 [[Niayesh Afshordi]] and [[Nissim Kanekar]]<ref>{{cite press release|url=http://www.perimeterinstitute.ca/news/niayesh-afshordi-wins-vainu-bappu-gold-medal|title=Niayesh Afshordi wins Vainu Bappu Gold Medal|publisher=[[Perimeter Institute for Theoretical Physics]]|date=April 21, 2011|last=Lambert|first=Lisa}}</ref>
* 2010 [[Marta Burgay]] and [[Parampreet Singh]]<ref>{{cite press release|title=LSU and Campus Federal Credit Union Name Rainmakers|date=March 4, 2015|url=http://www.lsu.edu/ur/ocur/lsunews/MediaCenter/News/2015/03/item75601.html|publisher=[[Louisiana State University]]|last=Satake|first=Alison}}</ref><ref>{{cite web|url=http://www.astron-soc.in/awards.php#vainu_bappu|title=Professor M. K. Vainu Bappu Gold Medal|publisher=Astronomical Society of India|accessdate=June 10, 2015}}</ref>

The Society also runs two prestigious lectures: the Modali Endowment Lecture and the R. C. Gupta Endowment Lecture.<ref name="awards"/>

== Publications ==
* [[Bulletin of the Astronomical Society of India]]

== References ==
{{Reflist}}

== External links ==
* {{official website|https://web.archive.org/web/20110304111620/http://www.astron-soc.in:80/index.html}}

{{Authority control}}

[[Category:Astronomy organizations]]
[[Category:Scientific organizations established in 1972]]
[[Category:Scientific organisations based in India]]
[[Category:Organisations based in Hyderabad, India]]
[[Category:1972 establishments in India]]
[[Category:Astronomy in India]]