{{Infobox organization
| name          = Chesterton House
| native_name   = 
| image         = CHES Logo CMYK long.jpg
| image_size    = 
| alt           = 
| caption       = 
| abbreviation  = 
| motto         = 
| predecessor   = 
| merged        = 
| successor     = 
| formation     = {{start date|2000}}
| founder       = 
| extinction    = 
| merger        = 
| type          = Non-profit 501(c)(3) organization
| tax_id        =  
| registration_id = 
| status        = 
| purpose       = 
| headquarters  = 
| location      = Ithaca, NY
| coords        = 
| region        = 
| services      = 
| products      = 
| methods       = 
| fields        = 
| membership    = 
| language      = 
| owner         = 
| sec_gen       = 
| leader_title  = Executive Director
| leader_name   = Karl E. Johnson
| board_of_directors = 
| key_people    = 
| main_organ    = 
| parent_organization = 
| subsidiaries  = 
| secessions    = 
| affiliations  = 
| budget        = 
| revenue       = 
| disbursements = 
| expenses      = 
| endowment     = 
| staff         = 
| volunteers    = 
| slogan        = 
| mission       = Chesterton House exists to facilitate the discovery of the intellectual richness of the historic Christian faith, thereby empowering more faithful Christian living.
| website       =  {{url|chestertonhouse.org}}
| remarks       = 
| formerly      = 
| footnotes     = 
}}

'''Chesterton House''' is a [[Christian Study Centers|Christian Study Center]] and [[501(c) organization|501(c)(3) organization]] affiliated with [[Cornell University]] in [[Ithaca, New York]] that works with the students, staff, faculty, and administration of Cornell to bridge the academy and the Christian church. The work of the organization has been mentioned in major media outlets such as the New York Times and Wall Street Journal.

==History==
A group of pastors and professors in Ithaca, NY came together to form Chesterton House in 2000 in order to "facilitate discovery of the intellectual riches of the historic Christian faith, thereby empowering more faithful Christian living."<ref>{{Cite news|url = http://sites.silaspartners.com/partner/Article_Display_Page/0,,PTID323422_CHID664014_CIID2077054,00.html|title = Reclaiming the Mind|last = McGarvey|first = Stephen|date = 2005|work = byFaith Magazine|access-date = 22 September 2015|via = }}</ref> The Study Center is named after [[G. K. Chesterton|G.K. Chesterton]], the [[British people|British]] writer and humorist who influenced [[Mahatma Gandhi|Gandhi]] and [[C. S. Lewis|C.S. Lewis]].

According to their 2014 Annual Report, Chesterton House describes its residential community as "a cross between a [[Fraternity house|fraternity]] and a [[monastery]]."<ref>{{Cite web|url = http://chestertonhouse.org/sites/default/files/2014Chesterton_Annual_Report_WebView_0.pdf|title = Chesterton House Annual Report 2014|date = |accessdate = 18 September 2015|website = Chesterton House|publisher = Chesterton House|last = |first = }}</ref> They have an annual budget of over $500,000, 60% of which is donated. The remainder comes from program service revenue and special fundraisers, such as when the center issued a [[jazz]] CD featuring [[William Edgar (apologist)|William Edgar]] and [[John Patitucci]].<ref>{{Cite news|url = http://cornellalumnimagazine.com/index.php?option=com_content&task=view&id=410&Itemid=56&ed=11|title = Heavenly Music|last = |first = |date = 19 May 2009|work = Cornell Alumni Magazine|access-date = 22 September 2015|via = }}</ref>
 
The Chesterton House building, located on The Knoll, is an English [[Tudor architecture|Tudor-style]] mansion that includes an industrial kitchen, sun room, living room, bedrooms, and a library with periodical subscriptions and thousands of volumes.<ref>{{Cite news|url = http://www.news.cornell.edu/stories/2014/12/firm-foundation-chesterton-house|title = A firm foundation for Chesterton House|last = Wallace|first = Diane Lebo|date = 17 December 2014|work = Cornell Chronicle|access-date = 22 September 2015}}</ref>

[[File:Chesterton House building The Knoll.jpg|thumb|Chesterton House building on The Knoll.]]

The [[Executive director|Executive Director]] Karl E. Johnson, also a founding member and current board chair of the Consortium of Christian Study Centers (CCSC),<ref>{{cite web |title=A Brief History of the Consortium |url=http://studycentersonline.org/about/history/ |publisher=Consortium of Christian Study Centers  |accessdate=18 September 2015}}</ref> said in the [[Cornell Chronicle]],"[W]e want to communicate that a proper study and understanding of religion helps facilitate human flourishing in all areas of life, whether in the arts, public policy, or the modern research university."<ref>{{Cite news|url = http://www.news.cornell.edu/stories/2011/10/beimfohr-lecture-feature-speakers-faith-society|title = Cornell University|last = Johnson|first = Karl E.|date = 3 October 2011|work = Cornell Chronicle|access-date = 18 September 2015|via = }}</ref> Dick Keyes of [[L'Abri|L’Abri]] speaks of Chesterton House as a place where ideas are "argued, debated, persuaded, reasoned."<ref>{{Cite web|url = http://chestertonhouse.org/about/endorsements|title = Endorsements|date = |accessdate = 18 September 2015|website = Chesterton House|publisher = |last = Keyes|first = Dick}}</ref>

Chesterton House is an affiliate member of Cornell United Religious Work, which organizes all religious groups on Cornell’s campus, including Cornell Hillel and the [[Cornell Catholic Community]]. Chesterton House’s advisory board includes [[Ken Blanchard]], Andy Crouch formally of [[Christianity Today]], [[D. A. Carson|D.A. Carson]], [[Elaine Howard Ecklund]], [[Ard Louis]], and [[Eleonore Stump]]. There is also a Governing Board and a Faculty Advisory Board.

In 2013 twenty-one university [[Campus Ministry|campus ministry]] organizations were awarded $2.9 million from the [[Lilly Endowment]] in order to further their vocation-related programs.<ref>{{cite web |title = Lilly Endowment Investing in Campus Ministries|url = http://www.insideindianabusiness.com/newsitem.asp?id=62541|publisher = Inside Indiana Business|date = November 26, 2013|accessdate = 18 September 2015}}</ref> Chesterton House was one of the four Christian Study Centers to receive the grant, including Hill House Ministries at the [[University of Texas at Austin]], the Center for Christian Study at the [[University of Virginia]], and the Christian Study Center of [[Gainesville, Florida|Gainesville]] at the [[University of Florida]].<ref>{{cite press release |title = Lilly Endowment gives $2.9M to Strengthen 21 Campus Ministry Organizations|url = http://www.lillyendowment.org/pdf/LillyEndowment11-25-2013PressRelease.pdf|publisher = Lilly Endowment|date = November 25, 2013|accessdate = 18 September 2015}}</ref> The Oread Center in [[Kansas]], also a member of the Consortium of Christian Study Centers, received part of a $4 million grant in 2014.<ref>{{cite press release |title=Lilly Endowment Helps to Strengthen Campus Ministries Worldwide |url= http://www.lillyendowment.org/pdf/2014CAMPUSMINISTRY.pdf |publisher=Lilly Endowment |date=November 17, 2014 |accessdate=October 24, 2015}}</ref>

==Programs==
[[File:ACrouch4.jpg|thumb|Author Andy Crouch at Chesterton House.]]

All programs are categorized into three groups: events, residential, and courses.

Since 2000, as part of their "partnership model of ministry,"<ref>{{Cite web|url = http://chestertonhouse.org/about/our-story|title = Our Story|date = 2015|accessdate = 29 September 2015|website = Chesterton House|publisher = |last = |first = }}</ref> Chesterton House co-sponsors events with [[academic department]]s, many local churches, and over a dozen campus ministries, including Chinese Bible Study, [[Cru (Christian organization)|Cru]], [[Fellowship of Christian Athletes|FCA]], [[The Navigators (organization)|Navigators]], [[InterVarsity Christian Fellowship|InterVarsity]], as well as Graduate Christian Fellowship and the graduate student fellowships at the [[Cornell Law School]] and the [[Samuel Curtis Johnson Graduate School of Management|Johnson Graduate School of Management]]. Chesterton House regularly hosts public lectures on topics such as art, culture, economics, faith and science, food security, human trafficking, race, technology, and theology.<ref>{{Cite news|url = http://www.news.cornell.edu/stories/2014/11/christian-atheist-scientists-tackle-human-nature|title = Christian, atheist scientists tackle human nature|last = Linehan|first = Rose|date = 13 November 2014|work = Cornell Chronicle|access-date = 22 September 2015|via = }}</ref><ref>{{Cite news|url = http://chestertonhouse.org/sites/default/files/Answering-the-Call.pdf|title = Answering the Call|last = Elvy|first = Catherine|date = 2014|work = Christian Union: The Magazine|access-date = 22 September 2015|via = }}</ref> The organization’s Alan T. and Linda M. Beimfohr Lecture, which addresses issues pertaining to faith in a pluralistic society, has been delivered by Felicia Wu Song, [[Richard Stearns (World Vision)|Richard Stearns]], [[Joel Salatin]], and [https://cbhd.org/content/jimmy-lin-md-phd-mhs Dr. C. Jimmy Lin].<ref>{{Cite news|url = http://www.news.cornell.edu/stories/2011/10/beimfohr-lecture-feature-speakers-faith-society|title = New Beimfohr Lecture gift will bring speakers on faith and society to campus|last = Aloi|first = Daniel|date = 3 October 2011|work = Cornell Chronicle|access-date = 22 September 2015}}</ref> Other past speakers include [[Makoto Fujimura]], Lisa Sharon Harper, [[Philip Jenkins]], Robin Jensen, [[Richard Mouw]], [[Mark Noll]], [[Alvin Plantinga]], [[Cornelius Plantinga]], [[John Polkinghorne|Sir John Polkinghorne]], [[Eleonore Stump]], [[Nicholas Wolterstorff]], [[Jeremy Begbie|Dr. Jeremy Begbie]][https://cbhd.org/content/jimmy-lin-md-phd-mhs .].<ref name="byFaith2011">{{cite web |title = Christian Study Centers Help Students See the Richness of the Faith|url = http://byfaithonline.com/christian-study-centers-help-students-see-the-richness-of-the-faith/|work = byFaith|date = December 8, 2011|publisher = [[Presbyterian Church in America]]|accessdate = 18 September 2015}}
</ref>

In 2010 Chesterton House established "residential living learning centers" in which dozens of men and women participate each year.<ref>{{Cite news|url = http://cornellsun.com/section/opinion/content/2012/03/05/true-brotherhood-knoll?fb_ref=Default%2C%40Total|title = The True Brotherhood of The Knoll|last = Bellin|first = Judah|date = 5 March 2012|work = Cornell Daily Sun|access-date = 25 September 2015|via = }}</ref> This includes a Sunday night weekly meal, cooking, cleaning, regular prayer and [[Bible|Scripture]] reading, daily devotions, an annual retreat, and semester service projects.

In 2014, in conjunction with [[Gordon College (Massachusetts)|Gordon College]], Chesterton House began offering courses for credit in biblical studies. Past courses include studies of the [[Gospel of Mark|book of Mark]], [[hermeneutics]], the [[book of Proverbs]], and vocation.<ref name="Whyte2014">{{cite news |last=Whyte |first=Liz Essley |title=Campus Crusades |url=http://www.philanthropyroundtable.org/topic/excellence_in_philanthropy/campus_crusades  |journal=Philanthropy |date=Fall 2014 |pages=28–35 |publisher=[[Philanthropy Roundtable]]}}</ref>

Other regular events at Chesterton House and other Christian Study Centers include retreats, conferences, discussion groups, and film viewings.<ref name=Osburn2014>{{cite web |last=Osburn |first=Robert |title=Good News for the Naked Public University |url=http://www.firstthings.com/web-exclusives/2014/10/good-news-for-the-naked-public-university |publisher=[[First Things]] |date=October 21, 2014 |accessdate=18 September 2015}}</ref><ref>{{Cite web|url = http://www.crosswalk.com/faith/spiritual-life/christian-collegians-feed-minds-at-chesterton-house-1361744.html|title = Christian Collegians Feed Minds at Chesterton House|date = 6 November 2005|accessdate = 22 September 2015|website = Crosswalk|publisher = |last = Wayne|first = Robert}}</ref> Chesterton House staff advise several student organizations, including Cornell Claritas, a journal, and student chapters of [[Christian Legal Society]], [[International Justice Mission]], and [[Veritas Forum]].

==See also==
{{Portal|Christianity}}
* [[Christian Study Centers]]
* [[Cru (Christian organization)|Cru]]
* [[Fellowship of Christian Athletes|Fellowship of Christian Athletes (FCA)]]
* [[International Justice Mission]]
* [[InterVarsity Christian Fellowship]]
* [[The Navigators (organization)|The Navigators]]

==References==
{{Reflist|30em}}

==Further reading==
* {{cite web |last=Noll |first=Mark |title=The Evangelical Mind Today |url=http://www.firstthings.com/article/2004/10/the-evangelical-mind-today |work=First Things |date=October 2004 |accessdate=17 September 2015}}
* Sommerville, C. John (2006). ''The Decline of the Secular University''. Oxford: Oxford University Press. ISBN 0195306953.
* "[https://www.wsj.com/articles/SB10001424052748704597704574487532250568304 Winning Not Just Hearts but Minds]." ''WSJ''. N.p., n.d. Web. 22 Jan. 2016.                          
* Worthen, Molly. "[https://www.nytimes.com/2016/01/17/opinion/sunday/hallelujah-college.html Hallelujah College]." ''The New York Times''. The New York Times, 16 Jan. 2016. Web. 22 Jan. 2016.

==External links==
* [http://chestertonhouse.org/ Chesterton House]
* [https://studycentersonline.org/ Consortium of Christian Study Centers (CCSC)]
* [http://www.newcollegeberkeley.org/ New College Berkeley]

{{DEFAULTSORT:Chesterton House}}
[[Category:Student religious organizations in the United States]]
[[Category:Christian organizations based in the United States]]
[[Category:Christian organizations established in the 20th century]]
[[Category:Evangelicalism]]
[[Category:G. K. Chesterton]]