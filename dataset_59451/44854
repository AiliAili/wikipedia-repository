{{use mdy dates|date=July 2012}}
{{Infobox company
|  name   = CliftonLarsonAllen LLP
|  type   = [[Limited Liability Partnership]] (LLP)
|  foundation     = 2012; merger of Clifton Gunderson (1960) and LarsonAllen (1953)
|  location       = [[Minneapolis, Minnesota]], [[United States]]
|  key_people     = Denny Schleper, [[CEO]]
|  industry       = [[Professional services]]
|  services       = [[Audit]]<br>[[Accounting]]<br>[[Tax]]<br>[[Management consulting|Consulting]]<br>[[Financial adviser|Advisory]]
|  revenue        = {{gain}} [[United States dollar|US$]]598 million (2015)
|  num_employees  = 3,799 (2015)
|  homepage       = {{url|http://www.CLAconnect.com}}
}}

'''CliftonLarsonAllen LLP''' (known as '''CliftonLarsonAllen''' or '''CLA''') is a professional services network and the tenth largest<ref name="top-100-2015">[http://digital.accountingtoday.com/accountingtoday/top_100_firms_2015#pg14 Accounting Today Top 100 Firms 2015], ''Accounting Today'', March 9, 2015. Retrieved March 24, 2015.</ref>  accountancy firm in the United States. It was established in 2012 when two top 20 U.S. firms merged: Clifton Gunderson LLP (based in [[Milwaukee]], Wisconsin) and LarsonAllen LLP (based in [[Minneapolis]], Minnesota).<ref>[http://www.journalofaccountancy.com/Web/20114744.htm Clifton Gunderson, LarsonAllen to Merge, Create Top 10 Firm], ''Journal of Accountancy'', November 2, 2011. Retrieved July 11, 2012.</ref> CliftonLarsonAllen's headquarters are in Minneapolis, Minnesota.<ref>http://www.claconnect.com/AboutUs/</ref> It is a member of the Nexia International [[accounting network]].<ref name=nexia-secures>{{cite news| url=http://www.vrl-financial-news.com/accounting/intl-accounting-bulletin/issues/iab-2012/iab-502/nexia-secures-cliftonlarsonall.aspx |title=Nexia secures CliftonLarsonAllen |newspaper=International Accounting Bulletin | date=February 23, 2012 |accessdate = July 11, 2012 }}</ref>

== History ==

===Clifton Gunderson===
Clifton Gunderson was founded in 1960 in [[Peoria, Illinois]], as Clifton, Gunderson, Coker and DeBruyn. The firm's first acquisition occurred in Illinois in 1965. In 1973, its name was shortened to Clifton, Gunderson; it became a limited liability partnership (LLP) in 2001. From 1976 to 1978, Clifton Gunderson added firms in Colorado, Wisconsin and Missouri. From 1982 to 1987, it expanded to Maryland, Arizona, Indiana and Iowa. Expansion continued into Ohio, Texas, and Virginia in the 1990s, and by the end of the decade, Clifton Gunderson had grown to become a well-known regional player in accounting.{{cn|date=October 2016}}

In 2009, Kris McMasters became the firm's fourth chief executive, and the accounting profession’s first female CEO among the top 50 firms.<ref>[http://www.accountingtoday.com/prc_issues/2008_6/27885-1.html Top Firm Names First Woman CEO], ''Accounting Today'', April 5, 2008.</ref><ref name=km-retires/> The year 2010 brought five acquisitions, and by 2011, Clifton Gunderson had expanded into California and Pennsylvania, becoming strongly established in the middle market. 
It was the 14th largest accounting firm in the US in 2010,<ref name=top100-2011/> and the largest member firm of [[HLB International]] until it left that network in 2012 after the merger with LarsonAllen.<ref name=nexia-secures/>

===LarsonAllen===
LarsonAllen was founded by young entrepreneurs Rholan Larson and John Allen who started a small public accounting practice in the suburbs of Minneapolis in 1953. Bob Weishair joined their firm in 1957 to form Larson, Allen, Weishair & Co., LLP.{{cn|date=October 2016}}

By 1987, the firm had grown to 165 employees and moved its headquarters to downtown Minneapolis. Gordy Viere was elected CEO in 1989. Under his leadership, the firm began expanding throughout Minnesota, adding offices in St. Cloud, Brainerd, and St. Paul by 1990. In the following decade, locations were opened through a series of acquisitions in Austin, Minnesota (1993); St. Louis, Missouri (1995); Charlotte, North Carolina (1998); and Philadelphia, Pennsylvania (1999).{{cn|date=October 2016}}

In the 2000s, LarsonAllen expanded into Arizona, Florida, Idaho, Massachusetts, Texas, Washington DC, and Wisconsin. The firm changed its legal name to LarsonAllen LLP (LarsonAllen) in 2007. In 2010, LarsonAllen moved into the Pacific Northwest, adding locations throughout Washington and Idaho,<ref>[http://www.bizjournals.com/seattle/stories/2010/08/09/story5.html Minneapolis firm acquires LeMaster Daniels], '' Seattle Business Journal'', August 9, 2010.</ref>
becoming the 18th largest accounting firm in the United States.<ref name=top100-2011>{{cite web|title=Accounting Today's Top 100 Firms 2011|url=http://digital.accountingtoday.com/accountingtoday/top100firms2011#pg15}}</ref>

In 2009, LarsonAllen was named a Best Place to Work in Charlotte,<ref name="Charlotte">[http://charlotte.bizjournals.com/charlotte/stories/2009/11/02/daily35.html Charlotte Business Journal]</ref> Minneapolis, Philadelphia,<ref name="Phila">[http://www.larsonallen.com/WorkArea//DownloadAsset.aspx?id=448 Philadelphia Best Places to Work]</ref> Phoenix<ref name="Phoenix">[http://phoenix.bizjournals.com/phoenix/stories/2009/12/07/daily57.html Phoenix Business Journal]</ref> and St. Louis <ref name="StL">[http://www.larsonallen.com/WorkArea//DownloadAsset.aspx?id=463 St. Louis Best Places to Work]</ref> by local business journals, and the firm’s learning and development team was honored by the [[American Society for Training & Development]] (ASTD).<ref name="AboutLA">[http://www.larsonallen.com/aboutus.aspx?id=329 LarsonAllen awards page]</ref>

===CliftonLarsonAllen===
Merger discussions between Clifton Gunderson and LarsonAllen commenced in the spring of 2011 and progressed rapidly, so that in the fall of that year they announced plans to merge the two firms. The merger occurred January 2, 2012. The decision to affiliate to Nexia rather than HLB International was taken after the merger.<ref name=nexia-secures/><ref name=to-merge/>

CliftonLarsonAllen emphasizes industry-specific services, as well as a focus on privately held companies and their owners, nonprofits, and governmental entities.<ref name="merger-mania">{{cite news|title=Entering an era of mergers: Clifton Gunderson example of national trend|url=http://www.bizjournals.com/milwaukee/print-edition/2011/12/23/entering-an-era-of-mergers-clifton.html?page=all|accessdate=25 July 2012|newspaper=Milwaukee Business Journal|date=December 23, 2011}}</ref> 
''[[Accounting Today]]'' reported speculation that the merged firm may expand into public company audits.<ref name=to-merge>{{cite news|url=http://www.accountingtoday.com/news/Clifton-Gunderson-LarsonAllen-Merge-60653-1.html |title=Clifton Gunderson and LarsonAllen to Merge |author=Michael Cohn |date=November 1, 2011 |newspaper=Accounting Today |accessdate=July 11, 2012 }}</ref>

Kris McMasters and Gordy Viere became the first co-CEOs of CliftonLarsonAllen LLP,<ref name=km-retires/> with Viere also serving as the CEO of CliftonLarsonAllen Holdings.<ref>{{cite web|url=http://www.cliftoncpa.com/Resources/Whitepapers/Details/615 |title=Clifton Gunderson, LarsonAllen Complete Merger |publisher=Clifton Gunderson |date=January 3, 2012 |accessdate=July 11, 2012 |archiveurl=https://web.archive.org/web/20120202111540/http://cliftoncpa.com/Resources/Whitepapers/Details/615 |archivedate=February 2, 2012 |deadurl=yes |df=mdy }}</ref> McMasters retired April 1, 2013, leaving Viere as sole CEO.<ref name=km-retires>[http://www.accountingweb.com/article/krista-mcmasters-cliftonlarsonallen-co-ceo-announces-retirement/221389 Krista McMasters, CliftonLarsonAllen Co-CEO, Announces Retirement], AccountingWeb, March 19, 2013. Retrieved 2013-04-04.</ref> Denny Schleper became the CEO of CliftonLarsonAllen in January 2015 after Viere retired. <ref name=schleper-ceo>[http://www.accountingtoday.com/news/people-news/cla-viere-passes-the-leadership-torch-to-schleper-73177-1.html CLA’s Viere Passes the Leadership Torch to Schleper] ''Accounting Today'', January 2, 2015.</ref> 

In September 2013, CliftonLarsenAllen agreed to pay a $35,100,000 settlement on the audit of the City of Dixon.  CG had performed the audit for many years but failed to recognize a $54,000,000 fraud.<ref>{{Citation | last = Jenco| first = Melissa| author-link = | last2 = | first2 = | author2-link = | title = Dixon to get $40 million in settlement of embezzlement case| newspaper = Chicago Tribune| pages = | year = | date = September 25, 2013| url = http://articles.chicagotribune.com/2013-09-25/news/chi-dixon-to-get-40-million-in-settlement-of-embezzlement-case-20130925_1_rita-crundwell-janis-card-associates-samuel-card| archiveurl = | archivedate = | accessdate = 24 August 2016 }}</ref> In addition, the firm was fined by the Illinois Department of Financial and Professional Regulation.<ref>[http://www.idfpr.com/Forms/DISCPLN/2015_02enf.pdf Illinois Department of Financial and Professional Regulation News], page 2. February 2015.</ref>

== Offices and personnel==
CliftonLarsonAllen employs more than 4,500 people, including more than 600 principals and 1,550 CPAs, in nearly 100 cities and towns across the United States.{{cn|date=October 2016}}

== Awards and honors==
In August 2012, CEOs Gordy Viere and Kris McMasters were named among the top 100 most influential people in accounting by ''[[Accounting Today]]''.<ref name="top-100-2012">{{cite news|url=http://digital.accountingtoday.com/accountingtoday/2012top100people#pg1
|title=The Top 100 Most Influential People in Accounting |date=August 1, 2012 |newspaper=Accounting Today |accessdate=September 14, 2012 }}</ref>

==References==
<references/>

[[Category:Accounting firms of the United States]]
[[Category:Companies established in 1953]]
[[Category:1953 establishments in Minnesota]]