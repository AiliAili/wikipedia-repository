{{Infobox company
 | name = Philosophy Documentation Center
 | logo = [[File:pdclogo2.jpg]]
 | type = [[Publishing|Publisher]], service provider
 | genre = [[Applied ethics]], [[classics]], [[philosophy]], [[religious studies]]
 | foundation = [[Bowling Green State University]], 1966
 | founder =
 | location_city = [[Charlottesville, Virginia]]
 | location_country = United States
 | location =
 | locations =
 | key_people = George Leaman (director), Pamela Swope (associate director), Susanne Mueller-Grote (electronic publishing)
 | industry = Publishing, [[internet service provider|Internet service]]s
 | products = [[Academic journal]]s, [[proceedings|conference proceedings]], [[anthologies]], [[reference work]], [[academic database]]s, [[digital media]]
 | services = Membership management, content digitization & hosting
 | revenue =
 | operating_income =
 | net_income =
 | owner =
 | num_employees =
 | parent =
 | divisions =
 | subsid =
 | slogan =
 | homepage = {{URL|http://www.pdcnet.org/}}
 | footnotes =
 | intl =
}}
The '''Philosophy Documentation Center''' is a [[non-profit organization|non-profit]] [[publishing|publisher]] and resource center that provides access to scholarly materials in [[applied ethics]], [[classics]], [[philosophy]], [[religious studies]], and related disciplines. It publishes [[academic journal]]s, [[proceedings|conference proceedings]], [[anthologies]], and online research [[database]]s, often in cooperation with [[scholarly association|scholarly]] and [[professional association]]s. It also provides membership management and electronic publishing services, and hosts electronic journals, series, and other publications from several countries.

== History ==
The Philosophy Documentation Center was established in 1966 at [[Bowling Green State University]] in Ohio to manage the publication of specialized reference works in philosophy. It was founded by two members of the university philosophy department, Ramona Cormier and Richard Lineback, who recognized a need to improve access to the growing body of philosophical literature in English and other languages. Its first publication was ''The Philosopher’s Index'', which provided bibliographic listings, indexed by subject and author, of recently published journal articles in philosophy. In 1970 PDC assumed responsibility for the publication of the ''[[Directory of American Philosophers]]'' and the ''[[International Directory of Philosophy and Philosophers]]''.

In 1974, PDC began publishing a series of research bibliographies to provide systematic overviews of the primary and secondary work of major philosophers. That same year PDC published its first journal, ''[[Philosophy Research Archives]]'', in cooperation with the [[American Philosophical Association]] and the [[Canadian Philosophical Association]].<ref>''Philosophy Research Archives'', Microfiche 1, 1975</ref> This bilingual project was established in microfiche format in an effort to overcome the space limitations of traditional print journals.<ref>"Meeting of the Board of Officers of the American Philosophical Association" in ''Proceedings and Addresses of the American Philosophical Association'', Vol. 48 (1974 - 1975), p.80.</ref> This concept was developed by representatives of the [[American Council of Learned Societies]], the Humanities Research Council of Canada, PDC, and the American and Canadian philosophical associations.<ref>''Philosophy Research Archives'', "Final Statement of Purpose" (August 22, 1974), quoted in the ''Journal of Philosophical Research'', Special Supplement, 2016, http://dx.doi.org/10.5840/jpr201641Supplement66</ref> It was one of the earliest experiments with non-traditional formats in journal publishing and is freely available online.<ref>{{cite web |url=http://www.pdcnet.org/pra |title=''Philosophy Research Archives'' website |accessdate=8 December 2016}}</ref>

In 1977, PDC published a collection of papers from the First National Workshop-Conference on Teaching Philosophy, its first publishing work in support of a new professional organization.<ref>''Teaching Philosophy Today''. edited by [[Terrell Ward Bynum]] and Sidney Reisberg, in cooperation with the National Information and Resource Center for the Teaching of Philosophy, 1977. [[WorldCat]] Record, OCLC Accession No.3286509. This and subsequent Workshop-Conferences on Teaching Philosophy led to the establishment of the [[American Association of Philosophy Teachers]]. See the editor's introduction to the 2nd edition (2012), pp.v-vi.</ref> PDC also provided publishing services to independent philosophy publications for the first time.<ref>PDC invoices for 1977, retrieved 21 September 2012. This work began with typesetting for ''[[Idealistic Studies]]'' and ''The Journal of Critical Analysis''.</ref> PDC's production support for serial publications in print and electronic format has expanded significantly since that time. PDC currently produces over a hundred print and electronic publications in philosophy and neighboring disciplines, and provides online access to complete sets of journals and series in several languages.<ref name=home>{{Cite web|url=http://www.pdcnet.org |title=Philosophy Documentation Center web site|accessdate=30 July 2016}}</ref>

In 1995 the editor and owner of ''The Philosopher’s Index'' retired from Bowling Green State University and ended his long association with PDC.<ref>"Hail & Farewell" in ''Phil Facts'', No.20, Summer 1995, p.1</ref> Since 1995 he has continued to publish ''Philosopher's Index'' separate from PDC with his own organization ([[Philosopher's Information Center]]).<ref>''The Philosopher's Index'', volume 29, no.3, Fall 1995, p.ii</ref> PDC continued to develop other publications and services, with a focus on the publishing and membership management needs of professional associations and scholarly societies. The scope of this work increased over time, with notable projects such as the ''Proceedings of the Twentieth [[World Congress of Philosophy]]'' and the major publications of the [[American Philosophical Association]].<ref>In 1999-2000 PDC produced the ''Proceedings and Addresses of the American Philosophical Association'', the ''APA Graduate Guide to Philosophy Programs'', and the ''APA Newsletters''. See "Publication Agreement between the American Philosophical Association and the Philosophy Documentation Center", November 11, 1999.</ref> Fields covered by PDC now include [[applied ethics]], [[classics]], [[philosophy]], [[religious studies]], and [[semiotics]]. It manages memberships for twenty organizations,<ref>{{Cite web|url=http://www.pdcnet.org/pdc/bvdb.nsf/membershipindex?openform |title=Membership Services, Philosophy Documentation Center web site|accessdate=4 September 2012}}</ref> and this work includes secure hosting, digital work flow management, authenticated access to electronic resources, and online conference registration.<ref name=home />

In 2001 PDC relocated its operations and most of its staff to [[Charlottesville, Virginia]] as a consequence of increasing technical demands of this expanding range of services.<ref>Atkins, Ace. "Deep Thoughts" in ''C-Ville Weekly'', July 24–30, 2001, p.5</ref>

== Electronic resources ==
In 1979, PDC launched [[dial-up access]] to ''The Philosopher's Index'' database in cooperation with [[DIALOG]]; ''Philosopher's Index'' was made available on CD-ROM in 1990. In 1992, PDC published ''The Logic Works'', an instructional software program for introductory logic courses, in cooperation with [[Rob Brady]]. A number of versions of this DOS-based program were published by PDC for 10 years. PDC also published ''EthicsWorks'', a software package for introductory ethics courses, in cooperation with [[Robert Pielke]]. In 1996, PDC partnered with InteLex Corporation to develop [[POIESIS: Philosophy Online Serials]]. This project made the content of dozens of print journals accessible online in conjunction with print subscriptions. It also allowed for the creation of complete back issue collections of several journals on CD-ROM, including ''[[Business Ethics Quarterly]]'', ''[[Philosophy and Theology|Philosophy & Theology]]'', ''[[Review of Metaphysics|The Review of Metaphysics]]'', and ''[[Teaching Philosophy]]''.

In 2009, PDC launched its eCollection of journals and series from several countries, with onlince access for institutions, single individuals, and membership organizations.<ref>{{cite web |url=http://www.pdcnet.org/ecollection |title=Philosophy Documentation Center - About eCollection |accessdate=2 September 2012}}</ref> PDC partnered with PORTICO to ensure long term preservation of this collection and also participates in the CLOCKSS digital archive.<ref>{{cite web |url=http://www.portico.org/digital-preservation/news-events/news/general-news/the-philosophy-documentation-center-to-preserve-e-journals-in-portico |title=Portico announces agreement with Philosophy Documentation Center |accessdate=15 March 2012}}</ref><ref>{{cite web |url=http://edina.ac.uk/cgi-bin/news.cgi?filename=2013-07-15-six_new_clockss_publishers.txt |title=CLOCKSS Archive announces participation of Philosophy Documentation Center |accessdate=15 July 2013}}</ref>

In 2010, PDC launched the [[International Directory of Philosophy]], an online database consolidating the content of the ''[[Directory of American Philosophers]]'' and the ''[[International Directory of Philosophy and Philosophers]]''. This database contains information on university philosophy departments and programs, professional societies, research centers, journals, and publishers in approximately 130 countries.

In 2011, PDC launched the [[Philosophy Research Index]], a new indexing database not associated with ''The Philosopher's Index'', the ''International Philosophical Bibliography'', or other resource. The goal of the project was to build complete bibliographic coverage of philosophical literature in several languages and by July 2014 it contained over 1.3 million listings. At that time PDC partnered with the PhilPapers Foundation and Philosophy Research Index was incorporated into the [[PhilPapers]] database.<ref>{{Cite web|url=http://philpapers.org/bbs/thread.pl?tId=942 |title= 
PhilPapers to incorporate Philosophy Research Index, PhilPapers web site|accessdate=2 August 2014}}</ref>

PDC provides single document access options for non-subscribers for all journals and series on its site, including a free preview of the first page of each document. The implementation allows each publication to choose its own access terms.<ref name=home /> Pre-publication access to forthcoming articles is provided for select titles.<ref>{{Cite web|url=http://www.pdcnet.org/wp/services/onlinefirst/ |title=Online First Pre-publication, Philosophy Documentation Center web site|accessdate=4 August 2014}}</ref>

PDC is a member of [[CrossRef]] and integrates [[digital object identifier]]s into all journals it publishes.<ref>{{cite web |url=http://www.crossref.org/crweblog/2011/08/new_crossref_members_97.html |title=CrossRef Blog |accessdate=15 September 2012}}</ref> PDC also participates in CrossRef's Cited-By Linking and CrossMark version control services.<ref>{{cite web |url=http://www.crossref.org/citedby/index.html |title=CrossRef Cited-by Linking |accessdate=8 March 2013}}</ref><ref>{{cite web |url=http://www.crossref.org/crossmark/AboutParticipatingPubs.htm |title=CrossMark Participants |accessdate=9 April 2013}}</ref>

== See also ==
* [[:Category:Philosophy Documentation Center academic journals|Academic journals published by PDC]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.pdcnet.org/}}

[[Category:Non-profit academic publishers]]
[[Category:Book publishing companies based in Virginia]]
[[Category:Educational publishing companies]]
[[Category:Philosophical literature]]
[[Category:Philosophy organizations]]
[[Category:Philosophy Documentation Center academic journals|*]]
[[Category:Publishing companies established in 1966]]
[[Category:Bibliographic database providers]]
[[Category:1966 establishments in Ohio]]