{{Infobox law firm
| name = Jacksonville Area Legal Aid
| slogan= "A wealth of justice for those who have neither."
| key_people= Jim Kowalski<ref name ="award">{{cite web | publisher=Daily Record| title=Jacksonville Area Legal Aid director honored for public service | url=https://www.jaxdailyrecord.com/showstory.php?Story_id=546078 | date=September 7, 2015 | author=Max Marbut | access-date=October 19, 2015}}</ref>
| logo = 
| headquarters = 126 W. Adams St.<br>[[Jacksonville, Florida]]
| num_offices = 
| num_attorneys = More than 30
| practice_areas = [[Civil law (area)| Civil Defense]] for [[indigent]] clients.
| date_founded =Organized in the 1930s and legally named in 1973
| company_type =  [[501(c)|501(c)(3)]]
| homepage = {{URL|1=http://www.jaxlegalaid.org}}
| revenue=
}}

The '''Jacksonville Area Legal Aid''', '''JALA''', developed during the [[Great Depression in the United States|Great Depression]] and the [[recession of 1937]] out of a group of attorneys who organized to provide [[pro bono]] legal services to those who could not afford the services.<ref name="home">{{cite web | publisher=JALA | title=Welcome to the Jacksonville Area Legal Aid (JALA) | url=http://www.jaxlegalaid.org/home.html | access-date=June 22, 2015}}</ref> The Jacksonville Area Legal Aid was officially named in 1973, and received [[501(c)|501(c)(3)]] tax status in 1976.<ref name="home"/>  JALA is a mid-size law firm with over 50 lawyers and support staff who offer free legal services to low income clients in [[Civil law (area)|civil legal matters]] which include public benefits, employment/unemployment law, family law, landlord-tenant disputes, fair housing, guardianship, refugee and asylee immigration, foreclosure defense, and consumer law.<ref>{{cite web | publisher=The Jacksonville Bar Association | title=Legal Aid | url=https://www.jaxbar.org/public-resources/legal-aid/ | access-date=June 22, 2015 }}</ref>  JALA works with [[Florida Coastal School of Law]]<ref>{{cite web | publisher=Florida Pro Bono | title=Jacksonville Area Legal Aid and Florida Coastal School of Law Collaborate to . . . GET THE WORD OUT | url=http://www.floridaprobono.org/news/article.259185-Jacksonville_Area_Legal_Aid_and_Florida_Coastal_School_of_Law_Collaborate_t | date=June 14, 2009 | access-date=June 23, 2015}}</ref> and has supported accredited externships with the [[Florida State University]].<ref>{{cite web | publisher=[[Florida State University]] | title=Jacksonville Area Legal Aid | url=http://archive.law.fsu.edu/academic_programs/jd_program/clinics/legalservices_programs/JacksonvilleAreaLegalAidJacksonville.html | access-date=June 23, 2015}}</ref>

==Funding and budget==
JALA receives funding from the Florida Bar Foundation, a foundation that receives funding by collecting [[Interest on Lawyer Trust Accounts| interest on lawyer trust accounts]].<ref name="ftu">{{cite web | publisher=[[The Florida Times-Union]] | title=Funding for legal aid reaches crisis point in Florida | url=http://jacksonville.com/news/florida/2014-06-25/story/funding-legal-aid-reaches-crisis-point-florida | author=Andrew Pantazi | date=June 25, 2014 | access-date=June 27, 2015}}</ref>  Saint Johns County and Jacksonville also donate to JALA,<ref name="ftu"/> although the Jacksonville City Council did not fund JALA "in 2013 or 2014, aside from dedicated grants for specific programs like veterans and homeless representation."<ref name="folio">{{cite web | publisher=''[[Folio Weekly]]'' | title=Jacksonville Area Legal Aid Battles Budget Cuts | url=http://folioweekly.com/JACKSONVILLE-AREA-LEGAL-AID-BATTLES-BUDGET-CUTS,11637 | access-date=June 22, 2015}}</ref><ref>{{cite web | publisher=[[WJXT]] | title=Jacksonville Area Legal Aid facing cuts | url=http://www.news4jax.com/news/jacksonville-area-legal-aide-facing-cuts/20882254 | author=Jim Piggott | date=July 8, 2013 | access-date=June 27, 2015}}</ref> In 2008 and 2009, JALA received $1.2 million from the bar foundation; however, due to a drop in interest rates JALA's budget was impacted in 2014 and only expected to receive $252,798 during the 2015 and 2016 fiscal year.<ref name="ftu"/>  The result of the 2014 budget reduction required JALA to layoff employees, cut its services to low-income clients, and close the office on some days.<ref name="folio"/><ref>{{cite web | publisher=[[Financial News & Daily Record]] | title=Jacksonville Area Legal Aid may soon get funding from the city | url=https://www.jaxdailyrecord.com/showstory.php?Story_id=543955 | author=David Chapman | date=September 25, 2014 | access-date=June 26, 2015}}</ref><ref>{{cite web | publisher=First Coast News | title=City to cut legal aid funding for thousands | url=http://www.firstcoastnews.com/story/news/local/2014/08/25/city-floats-legal-aid-cuts/14600413/ | author=Andrew Capasso | date=August 25, 2014}}</ref>
==Pro bono assistance==
[[File:Jacksonville-Area-Legal-Aid.JPG|thumb|The Jacksonville Area Legal Aid]]
JALA offers free legal seminars and assistance to the public.<ref>{{cite web | publisher=City of Jacksonville | title=Free Legal Clinics | url=http://www.coj.net/departments/fourth-judicial-circuit-court/family/free-legal-clinics.aspx | access-date=July 8, 2015 }}</ref>
===Civil rights===
JALA is the [[Fair Housing Act]] enforcement agency for Northeast Florida.<ref>{{cite web| publisher=[[WJCT]] | title=St. Augustine Tour Highlights Civil Rights Struggles Past And Present | url=http://news.wjct.org/post/st-augustine-tour-highlights-civil-rights-struggles-past-and-present | date=May 1, 2015 | access-date=July 8, 2015 }}</ref>

===Family law===
Monthly family law seminars are given to assist families in dissolution of marriage, paternity/timeshare custody, and child support modification<ref> {{cite web | publisher= [[Financial News & Daily Record]] | title=Pro bono spotlight: Family law attorneys assist hundreds annually | url=http://www.jaxdailyrecord.com/showstory.php?Story_id=545539 | author=Kathy Para | date=June 1, 2015 | access-date=June 27, 2015}}</ref>

===Foreclosure and bankruptcy===
JALA offers assistance to clients affected by foreclosure, and assist the City of Jacksonville in developing strategies related to foreclosed homes affected by the [[subprime mortgage crisis]]<ref>{{cite web | publisher=[[WJCT]] | title=Jacksonville City Council Passes Bill To Help Fight ‘Zombie’ Houses | url=http://news.wjct.org/post/jacksonville-city-council-passes-bill-help-fight-zombie-houses | date=June 10, 2015 | author=Lindsey Kilbride | access-date=June 27, 2015 }}</ref><ref>{{cite web | publisher=[[The Florida Bar]] | title=Lynn Drysdale of Jacksonville Area Legal Aid Is 2014 Consumer Protection Lawyer of the Year | url=http://www.floridabar.org/TFB/TFBPublic.nsf/WNewsReleases/922499287ADEADDF85257D1900558F38?OpenDocument | date=July 18, 2014 | access-date=June 27, 2015 }}</ref>  Additionally, JALA gives bankruptcy assistance by holding free legal seminars and offering pro bono defense to individuals in bankruptcy.<ref>{{cite web | publisher=[[Financial News & Daily Record]] | title=Pro bono spotlight: Helping those who need to file bankruptcy | url=https://www.jaxdailyrecord.com/showstory.php?Story_id=545622 | author=Kathy Para | date=June 15, 2015 | access-date=June 27, 2015 }}</ref>

===Immigration===
JALA operates the Refugee Immigration Project, a legal project designed to give immigration assistance to asylum seekers, refugees, and victims of human trafficking<ref> {{cite web| publisher=FloridaLawHelp.org | title=Jacksonville Area Legal Aid, Inc. | url=http://floridalawhelp.org/organization/jacksonville-area-legal-aid-inc-3/family-law/divorce-contested?ref=ZH4Nd | access-date=July 8, 2015}}</ref>

===Mental health===
JALA operates the Mental Health Advocacy Project, a program developed to help the mentally ill with the greatest financial needs gain access to treatment.<ref>{{cite web| publisher= [[National Alliance on Mental Illness]] | title=Jacksonville Area Legal Aid Mental Health Advocacy Project | url=http://jacksonville.nami.org/Information/JaxLegalAid.pdf | access-date=July 8, 2015}}</ref>

===Northeast Florida Medical Legal Partnership===
The Northeast Florida Medical Legal Partnership (NFMLP) is pro bono project of JALA that combines the knowledge of medical and legal professional to improve the health of low income and vulnerable patients.<ref>{{cite web | publisher=[[Financial News & Daily Record]] | title=Leadership, pro bono efforts sustain partnership | url=http://www.jaxdailyrecord.com/showstory.php?Story_id=545575 | author=Kathy Para | date=June 8, 2015 | access-date=June 27, 2015 }}</ref>

==Three Rivers Legal Services==
The Three Rivers Legal Services (Three Rivers) is a pro bono legal aid law firm headquartered in [[Gainesville, Florida]] and established in 1977.<ref>{{cite web | publisher=Three Rivers Legal Services | title=Who We Are | url=http://www.trls.org | access-date=June 27, 2015}}</ref>
JALA collaborates with Three Rivers to offer legal services in the Northeast Florida region.<ref>{{cite web | publisher=Jax Daily Record | title=Pro bono spotlight: Let 2015 be the year you volunteer time | url=http://www.jaxdailyrecord.com/showstory.php?Story_id=544626 | author=Kathy Para | date=January 5, 2015 | access-date=June 27, 2015}}</ref>  JALA and Three Rivers work together in clinics to restore civil rights, expunge criminal records,<ref>{{cite web | publisher=Jacksonville Area Legal Aid | title=Restoration of Civil Rights & Sealing and Expungement of Criminal Records | url=http://www.jaxlegalaid.org/free-clinics.html | access-date=June 27, 2015}}</ref> and assist in bankruptcy cases.<ref>{{cite web | publisher=Jax Daily Record | title=Pro bono spotlight: Helping those who need to file bankruptcy | url= https://www.jaxdailyrecord.com/showstory.php?Story_id=545622 | author=Kathy Para | date=June 15, 2015 | access-date=June 27, 2015}}</ref>

==References==
{{reflist|2}}

*
*
*
*

==External links==
*[http://www.jaxlegalaid.org/ Official Website]
*[http://www.probono.net/oppsguide/organization.105949-Jacksonville_Area_Legal_Aid Pro Bono Information]

[[Category:Legal aid]]
[[Category:Jacksonville, Florida]]