{{Good article}}
{{DISPLAYTITLE:''Abraham Lincoln'' (Flannery)}}{{Infobox Monument
|monument_name = Abraham Lincoln
|image         = [[File:Lincoln at the DC Court of Appeals.JPG|275px]]
|location      = 400 block of Indiana Ave NW [[Washington, D.C.]], U.S.
|designer      = [[Lot Flannery]] (sculptor)<br>Frank G. Pierson (architect)
|material      = Marble (sculpture)<br>Granite (base)
|height        = {{convert|7.3|ft|sp=uk}}
|width         = {{convert|2.9|ft|sp=uk}}
|length        = {{convert|2.5|ft|sp=uk}}
|open          = April 15, 1868
|dedicated_to  = [[Abraham Lincoln]]
|coordinates   = {{Coord|38.8949769|-77.0175131|type:landmark_region:US_dim:6|display=inline,title}}
}}
'''''Abraham Lincoln''''' is a marble sculpture of U.S. President [[Abraham Lincoln]] by Irish artist [[Lot Flannery]], located in front of the old [[District of Columbia City Hall]] in [[Washington, D.C.]], United States.  It was installed several blocks from [[Ford's Theatre]], where Lincoln was assassinated.  Dedicated in 1868 on the third anniversary of [[Assassination of Abraham Lincoln|Lincoln's death]], dignitaries at the unveiling ceremony included President [[Andrew Johnson]] and Generals [[Ulysses S. Grant]], [[William Tecumseh Sherman]] and [[Winfield Scott Hancock]].  The statue has been removed and rededicated twice.  The first rededication was in 1923 following an outpouring of support from citizens and a veterans group that the statue be restored.  The second rededication took place in 2009 after a three-year remodeling of the old City Hall.  The statue is the nation's oldest extant memorial to the president.  It previously stood on a column, but now rests on top of an octagonal base.

==History==

===Background===
{{multiple image
| align     = left
| direction = vertical
| header    = 
| width     = 200

| image1    = Lincoln Monument, Metropolitan Church, from Robert N. Dennis collection of stereoscopic views.jpg
| alt1      = 
| caption1  =

| image2    = Lincoln Monument, Metropolitan Church, from Robert N. Dennis collection of stereoscopic views 2.jpg
| alt2      = 
| caption2  = Stereoscopic views showing the statue c. 1865–1890
}}
Most of the residents of [[Washington, D.C.]] were shocked and horrified by the [[Assassination of Abraham Lincoln|assassination]] of [[Republican Party (United States)|Republican]] President [[Abraham Lincoln]] on April 14, 1865.  Because of its geographical location, the city was sometimes suspected of being sympathetic to the [[Confederate States of America|Confederacy]] during the [[American Civil War|Civil War]].  Since Lincoln was killed in Washington, some of the residents worried Republican congressional leaders would seek revenge on the city.  Thirteen days after Lincoln died, in an attempt to show loyalty, city and business leaders decided to erect a memorial honoring the slain president.<ref name=testament>{{cite book | author=Jacob, Kathryn Allamong | title=Testament to Union: Civil War Monuments in Washington, D.C. | url=https://books.google.com/books?id=M6HIB4fNjGoC&lpg=PA184&ots=kueTz0ptQ-&dq=%22Lot%20Flannery%22%20Abraham%20Lincoln&pg=PA68#v=onepage&q=Flannery&f=false | location=Charlottesville | publisher=JHU Press | pages=68–71 | date=1998 | isbn=9780801858611 | accessdate=October 30, 2014 }}</ref>  It was the first Lincoln monument commissioned after his death, but not the first one built.<ref name=testament/><ref name=smithsonian>{{cite web | url=http://siris-artinventories.si.edu/ipac20/ipac.jsp?session=G1I98048Q3397.39158&menu=search&aspect=Keyword&npp=50&ipp=20&spp=20&profile=ariall&ri=&term=&index=.GW&aspect=Keyword&term=&index=.AW&term=&index=.TW&term=&index=.SW&term=&index=.FW&term=&index=.OW&term=76005971&index=.NW&x=9&y=10#focus | title=Abraham Lincoln, (sculpture). | publisher=Smithsonian American Art Museum | accessdate=October 30, 2014 }}</ref>  In 1866, a plaster statue (later replaced by a metal one) of Lincoln was erected in [[San Francisco]].  It was destroyed during the firestorm that followed the [[1906 San Francisco earthquake|1906 earthquake]].<ref name=testament/>

The total cost of the memorial was $25,000.  Washingtonians were responsible for most of the donations with the remaining funds raised by the Lincoln Monument Association.<ref name=testament/><ref name=smithsonian/>  The largest donation came from [[John T. Ford]], the manager of Ford's Theatre at the time of Lincoln's assassination.  He held a benefit performance at his theater in [[Baltimore]], raising $1,800.<ref name=testament/><ref name=court>{{cite web | url=http://www.dccourts.gov/internet/documents/2009-04-15_LincolnStatue_Release.pdf | title=Courts to Rededicate City's Oldest Lincoln Statue | date=April 15, 2009 | publisher=District of Columbia Court of Appeals | accessdate=October 30, 2014 }}</ref>  Although several designs were submitted, the monument's planning committee unanimously chose the model by Lot Flannery (1836&ndash;1922), a local Irish-American artist, calling it the "most spirited" and "an excellent likeness."<ref name=testament/>  Flannery had known Lincoln and was at Ford's Theatre the night of the assassination.  His statue is the only statue of Lincoln created by someone who knew him.<ref name=hackett>{{cite news | date=December 19, 1922 |title=Lot Flannery, 86, Sculptor, is Dead |url=http://chroniclingamerica.loc.gov/lccn/sn83045462/1922-12-19/ed-1/seq-2/ |newspaper=The Evening Star |accessdate=October 30, 2014 }}</ref>  In addition to the Lincoln sculpture, Flannery's notable works include the Arsenal Monument at [[Congressional Cemetery]] and a sculpture of President [[Chester Arthur]] on display at the [[Smithsonian American Art Museum]].<ref name=congressional>{{cite web | url=http://www.cem.va.gov/cems/lots/congressional.asp | title=Congressional Cemetery Government Lots | publisher=United States Department of Veterans Affairs | accessdate=October 30, 2014 }}</ref><ref name=arthur>{{cite web | url=http://siris-artinventories.si.edu/ipac20/ipac.jsp?session=14146I97151C1.895&profile=ariall&source=~!siartinventories&view=subscriptionsummary&uri=full=3100001~!112562~!1&ri=6&aspect=Browse&menu=search&ipp=20&spp=20&staffonly=&term=Flannery,+Lot,+active+1850-1859,+sculptor.&index=AUTHOR&uindex=&aspect=Browse&menu=search&ri=6 | title=Chester Arthur, (sculpture). | publisher=Smithsonian American Art Museum | accessdate=October 30, 2014 }}</ref>  Frank G. Pierson was chosen to be the monument's architect.<ref name=smithsonian/>

===Dedication===
On the evening of April 14, 1868, the Lincoln statue was moved from Flannery's studio to City Hall.  The covered statue was guarded by police so no one could see it before the dedication ceremony the following day.  On April 15, all of the city's offices were closed at noon and all flags were flown at half-staff.<ref name=testament/>  An estimated 20,000 people, around 20% of Washington's population, attended the dedication.<ref name=court/>  Dignitaries at the dedication included President [[Andrew Johnson]], General [[Ulysses S. Grant]], General [[William Tecumseh Sherman]], and General [[Winfield Scott Hancock]].  Supreme Court Justices and members of Congress were not in attendance because Johnson's [[Impeachment of Andrew Johnson|impeachment trial]] was taking place.  A [[Freemasonry|Masonic]] ceremony, along with music and prayers, took place at the dedication before the main speech by Major General Benjamin Brown French.  Following the speech, Washington mayor [[Richard Wallach]] introduced Johnson, who uncovered the statue.  The crowd cheered, followed by more music and finally a benediction.<ref name=testament/>

===Removal and rededications===
[[File:Lincoln statue - City Hall.jpg|thumb|upright|right|The memorial being dismantled in 1919]]

In 1919, the memorial was dismantled and placed in storage during the renovations of City Hall.  Some of the city's residents and officials didn't want the memorial reinstalled when renovations were complete since the much larger and grander [[Lincoln Memorial]] was already under construction.<ref name=testament/><ref name=smithsonian/>  Others thought the tall pedestal was unsafe.<ref name=bio>{{cite web | url=http://americanart.si.edu/collections/search/artist/?id=1578 | title=Lot Flannery | publisher=Smithsonian American Art Museum | accessdate=October 30, 2014 }}</ref>  When the public heard of the plans to leave the monument in storage, many were upset and groups such as the [[Grand Army of the Republic]] demanded the statue be reinstalled.<ref name=testament/>  President [[Warren G. Harding]] even lobbied Congress on behalf of angry citizens.<ref name=bio/> Government officials conceded, but by that point, the statue was missing.  It was later found in crates behind the [[Bureau of Engraving and Printing]] and cleaned.  On June 21, 1922, an [[Act of Congress]] authorized the rededication, which took place April 15, 1923, 55 years after the initial dedication.  When the statue was replaced, it was set on a new shorter base instead of the original column.  An unexpected consequence of this was vandals having easy access to the statue.  Lincoln's fingers were broken off several times and his right hand had to be replaced.<ref name=testament/><ref name=smithsonian/>

In 2006, the memorial was moved when renovations once again took place on the old City Hall, now home to the [[District of Columbia Court of Appeals]].  The statue was restored and cleaned before being returned when renovations were completed in 2009.  On April 15, 2009, 144 years after the original dedication, the memorial was rededicated a second time.<ref name=court/>  The statue is the country's oldest extant memorial to Lincoln.<ref name=testament/>  It is one of six statues in public places in Washington, D.C. honoring the slain president.<ref name=pohl>{{cite book | title=Abraham Lincoln and the End of Slavery in the District of Columbia | publisher=Eastern Branch Press |author1=Pohl, Robert S. |author2=Wennersten, John R. | year=2009 | location=Washington, D.C. | pages=131 | isbn=9780578016887 }}</ref>
{{clear}}

==Design and location==
[[File:Lincoln statue by Highsmith.tif|thumb|upright|The statue in 2010]]

The marble statue is located on Indiana Avenue NW, in front of the old [[District of Columbia City Hall]] in the [[Judiciary Square]] neighborhood.<ref name=pohl/>   It measures {{convert|7.3|ft|sp=uk}} high {{convert|2.9|ft|sp=uk}} wide while the granite base measures {{convert|6.4|ft|sp=uk}} high and {{convert|7|ft|sp=uk}} wide.  The statue portrays Abraham Lincoln standing, wearing a long coat with a bow tie and waistcoat.  His left hand rests on a [[fasces]] while his right arm is by his side.  Lincoln's partially open right hand points to the ground as he looks to his left.  The right hand was replaced at some point and the new one is considered too large to scale.  A sword or scroll previously hung by his right side, but is now missing.  The two-tiered base consists of a rectangle on top of a lower square-shaped foundation.<ref name=smithsonian/>

The statue originally stood on an 18-foot (5.5 m) high marble column atop a 6-foot (1.8 m) high octagonal base.<ref name=smithsonian/>  A reporter asked [[Lot Flannery]] why the statue was set on such a high pedestal.  He responded: "I lived through the days and nights of gloom following the assassination. As to every one else, it was a personal lamentation. And when it fell to me to carve and erect this statue I resolved and did place it so high that no assassin's hand could ever again strike him down."<ref name=hackett/>

Inscriptions on the monument include the following:<ref name=smithsonian/>
* Lot Flannery, Sculptor (rear of the sculpture)
* LINCOLN (front of the base)
* ABRAHAM LINCOLN / 1809–1865 / THIS STATVE WAS ERECTED / BY THE CITIZENS OF THE / DISTRICT OF COLVMBIA / APRIL 15 1868 / RE-ERECTED APRIL 15 1923 / VNDER ACT OF CONGRESS / OF JVNE 21 1922 (rear of the base)
* Frank G. Pierson, Architect (rear of the base, lowest section)

==See also==
{{Portal|Visual arts|Washington, D.C.}}
* [[1868 in art]]
* [[List of public art in Washington, D.C., Ward 6]]
* [[Outdoor sculpture in Washington, D.C.]]
{{Clear}}

==References==
{{Reflist|30em}}

==External links==
* {{Commons category-inline|Abraham Lincoln statue (District of Columbia City Hall)}}

{{Abraham Lincoln}}
{{Lincoln memorials}}

[[Category:1868 establishments in Washington, D.C.]]
[[Category:1868 sculptures]]
[[Category:Granite sculptures in Washington, D.C.]]
[[Category:Marble sculptures in Washington, D.C.]]
[[Category:Monuments and memorials to Abraham Lincoln]]
[[Category:Outdoor sculptures in Washington, D.C.]]
[[Category:Sculptures of men in Washington, D.C.]]
[[Category:Statues in Washington, D.C.]]
[[Category:Statues of Abraham Lincoln]]