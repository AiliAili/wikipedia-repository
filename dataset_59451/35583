{{good article}}
{{for|the women's race|2016 Tour of Flanders for Women}}{{Infobox cycling race report
| name               = 2016 Tour of Flanders
| image              = 2016 Tour of Flanders logo.svg
| image_caption      = 
| image_size         = 250
| series             = [[2016 UCI World Tour]]
| race_no            = 8
| season_no          = 28
| date               = 3 April 2016
| stages             = 1
| distance           = 255
| unit               = km
| time               = 6h 10' 37"
| speed              = 41.283
| first              = [[Peter Sagan]]
| first_team         = {{ct|TNK|2016}}
| first_nat          = SVK
| second              = [[Fabian Cancellara]]
| second_team         = {{ct|TFR|2016}}
| second_nat          = SUI
| third             = [[Sep Vanmarcke]]
| third_team        = {{ct|TLJ|2016}}
| third_nat         = BEL
| previous           = [[2015 Tour of Flanders|2015]]
| next               = [[2017 Tour of Flanders|2017]]
}}
The '''2016 Tour of Flanders''' was a one-day [[classic cycle races|classic cycling race]] that took place in [[Belgium]] on Sunday 3 April 2016. It was the 100th edition of the [[Tour of Flanders]]; it was the eighth event of the [[UCI World Tour]] and the third of the [[cobbled classics|cobbled one-day classics]].<ref>{{cite web|last1=Wynn|first1=Nigel|title=UCI WorldTour calendar 2016|url=http://www.cyclingweekly.co.uk/news/latest-news/uci-worldtour-calendar-2016-193076|website=[[Cycling Weekly]]|publisher=[[Time Inc. UK]]|accessdate=5 December 2015}}</ref> It was the second [[Classic cycle races#The "Monuments"|Monument race]] of the 2016 cycling season.

The race started in [[Bruges]] and finished in [[Oudenaarde]]. The total distance was {{convert|255|km|mi}}, covering 18 categorized climbs and seven flat cobblestoned sectors. The principal favourites for the overall victory were [[Fabian Cancellara]] ({{ct|TFR|2016}}), [[Tom Boonen]] ({{ct|EQS|2016}}) and [[Peter Sagan]] ({{ct|TNK|2016}}).

After several riders had abandoned the race due to crashes and injuries, the decisive break was formed with {{convert|30|km}} to go by Sagan, [[Michał Kwiatkowski]] ({{ct|SKY|2016}}) and [[Sep Vanmarcke]] ({{ct|TLJ|2016}}). On the last climb of the [[Oude Kwaremont]], Kwiatkowski was dropped; on the Paterberg, which followed shortly afterwards and was final climb of the race, Sagan dropped Vanmarcke and set off alone towards the finish. Vanmarcke and Cancellara collaborated in an effort to chase him down, but Sagan was able to ride to the finish alone. Cancellara finished second with Vanmarcke third, both more than 20 seconds behind Sagan.

==Route==
[[File:Bruges01.jpg|thumb|left|[[Markt (Bruges)|Market Square]] in [[Bruges]], scene of the start of the 100th Tour of Flanders.]]
[[File:Ronde van Vlaanderen 2016 – profile.svg|thumb|Route profile]]
[[File:Ronde van Vlaanderen 2016 lap1.png|thumb|First lap of the circuit (red) and transition to the second lap (green).]]
[[File:Ronde van Vlaanderen 2016 lap2.png|thumb|Second lap of the circuit (red) and final (green)]]
The route of the 100th edition was presented on 29 November 2015 at [[Brussels Airport]].<ref name="voorstelling" >{{cite web|title=Parcours 100e Ronde van Vlaanderen voorgesteld: "Het begint herkenbaar te worden"|trans-title=The route of the 100th Tour of Flanders is announced: "It's starting to be accepted"|url=http://www.nieuwsblad.be/sportwereld/cnt/dmf20151129_01995231|website=Sportwereld|publisher=Het Mediahuis|accessdate=4 December 2015|language=Dutch}}</ref> It contained small changes from previous editions: the Tiegemberg in [[West Flanders]] was omitted, while in the [[Flemish Ardennes]] the flat cobbled sector of the Holleweg was cut from the race and replaced with the Jagerij cobbled road.<ref name="Wegwijzer" >{{cite web|title=100° Ronde van Vlaanderen. Roadmap|url=http://rvv.pro.p.assets.flandersclassics.be/files/3177/original/rvv2016-elite-men-roadmap-26nov15.pdf?1448970526|website=rondevanvlaanderen.be|publisher=[[Flanders Classics]]|accessdate=4 December 2015}}</ref> The change was needed to keep the iconic Molenberg climb in the route due to roadworks.<ref name="voorstelling" /> The recent tradition of announcing a ''{{lang|nl|Dorp van de Ronde}}'' ("Village of the Tour") was also abandoned, although the route passed through [[Kanegem]] and [[Aarsele]] in tribute to [[Briek Schotte]] and [[Roger Decock]] (the oldest living winner of the Tour of Flanders) respectively.<ref name="Route announced" >{{cite web|title=Route announced for 100th edition of the Tour of Flanders|url=http://www.cyclingnews.com/news/route-announced-for-100th-edition-of-the-tour-of-flanders/|website=[[Cycling News]]|publisher=Immediate Media Company|accessdate=4 December 2015}}</ref>

The race started in the [[Markt (Bruges)|Market Square]] in [[Bruges]] with a neutralised zone, leaving the town to the south. The racing started outside the city and took the riders southwest through [[Torhout]] to [[Roeselare]], then east through [[Ardooie]] and [[Tielt]], then south-east to [[Oudenaarde]], passing across the first cobbled sector of the day, the flat Huisepontweg. After Oudenaarde, the route continued into a series of circuits through the hilly area to the south and east. After {{convert|103|km}} came the first climb, the [[Oude Kwaremont]], a {{convert|2200|m|yd|-2|adj=on}}, partially cobbled climb that would feature twice more later in the race. The route turned north and, following the climb of the non-cobbled Kortekeer, the riders entered a {{convert|20|km|adj=on}} section with several cobbled roads. These included the cobbled climbs of the Eikenberg and the Wolvenberg, then the flat Ruiterstraat, Kerkgate and Jagerij, the climb of the Molenberg, then finally the {{convert|2300|m|yd|-2}} of the flat Paddestraat. The route then turned back to the south-west, across the flat Haaghoek cobbles and then the non-cobbled climbs of the Leberg, Berendries, Valkenberg, Kaperij and Kanarieberg. This brought the route back to take on the second ascent of the Oude Kwaremont, followed immediately by the steep cobbled climb of the [[Paterberg]], {{convert|360|m|yd|-1}} long with an average gradient of 12.9% and sections at over 20%. At the summit of the Paterberg, there were {{convert|51|km}} left to the finish.{{sfn|Roadbook|2016|p=15}}{{sfn|Roadbook|2016|pp=26–31}}

There were six more climbs in the next {{convert|40|km|adj=on}} loop. The first of these was the [[Koppenberg]], which includes the steepest roads of the race, with gradients of 22%. This was followed by the flat cobbles of the Mariaborrestraat and the climbs of the Steenbeekdries and the [[Taaienberg]]. The roads took the riders south into [[Ronse]] for the climb of the [[Kruisberg]], then north-west to the foot of the Oude Kwaremont. The Oude Kwaremont–Paterberg combination was used for a second time; at the top of the Paterberg there were {{convert|11.7|km}} to the finish. These took place over mainly flat roads, with a long finishing straight on the outskirts of Oudenaarde.{{sfn|Roadbook|2016|p=15}}{{sfn|Roadbook|2016|pp=31–32}}<ref name="Wegwijzer" />

===Climbs and cobbled roads===
In total, the race included eighteen categorized climbs.<ref name="Wegwijzer" />

{| class="wikitable plainrowheaders sortable" style="text-align: center;"
|+ Categorized climbs in the 2016 Tour of Flanders<ref name="Wegwijzer" />
! scope="col" | Number
! scope="col" | Name
! scope="col" | Distance from finish ({{abbr|km|kilometres}})
! scope="col" | Road surface
! scope="col" | Length ({{abbr|m|metres}})
! scope="col" | Average gradient (%)
! scope="col" | Maximum gradient (%)
|-
| 1
! scope="row" style="text-align: left;" | [[Oude Kwaremont]]
| 152
| cobbles
| 2200
| 4.2%
| 11%
|-
| 2
! scope="row" style="text-align: left;" | [[Kortekeer]]
| 141
| asphalt
| 1000
| 6.4%
| 17.1%
|-
| 3
! scope="row" style="text-align: left;" | [[Eikenberg]]
| 134
| cobbles
| 1300
| 6.2%
| 11%
|-
| 4
! scope="row" style="text-align: left;" | [[Wolvenberg]]
| 130
| asphalt
| 666
| 6.8%
| 17.3%
|-
| 5
! scope="row" style="text-align: left;" | [[Molenberg (Zwalm)|Molenberg]]
| 118
| cobbles
| 463
| 7%
| 14.2%
|-
| 6
! scope="row" style="text-align: left;" | [[Leberg]]
| 97
| asphalt
| 700
| 6.1%
| 14%
|-
| 7
! scope="row" style="text-align: left;" | [[Berendries]]
| 93
| asphalt
| 940
| 7.1%
| 12.4%
|-
| 8
! scope="row" style="text-align: left;" | [[Valkenberg (Brakel)|Valkenberg]]
| 88
| asphalt
| 875
| 6%
| 15%
|-
| 9
! scope="row" style="text-align: left;" | [[Kaperij]]
| 77
| asphalt
| 1250
| 5%
| 8%
|-
| 10
! scope="row" style="text-align: left;" | [[Kanarieberg]]
| 70
| asphalt
| 1000
| 7.7%
| 14%
|-
| 11
! scope="row" style="text-align: left;" | [[Oude Kwaremont]]
| 54
| cobbles
| 2200
| 4.2%
| 11%
|-
| 12
! scope="row" style="text-align: left;" | [[Paterberg]]
| 51
| cobbles
| 400
| 12.5%
| 20%
|-
| 13
! scope="row" style="text-align: left;" | [[Koppenberg]]
| 44
| cobbles
| 600
| 11.6%
| 22%
|-
| 14
! scope="row" style="text-align: left;" | [[Steenbeekdries]]
| 39
| cobbles
| 820
| 7.6%
| 12.8%
|-
| 15
! scope="row" style="text-align: left;" | [[Taaienberg]]
| 36
| cobbles
| 800
| 7.1%
| 18%
|-
| 16
! scope="row" style="text-align: left;" | [[Kruisberg]]–[[Hotond]]
| 26
| cobbles–asphalt
| 2500
| 5%
| 9%
|-
| 17
! scope="row" style="text-align: left;" | [[Oude Kwaremont]]
| 16
| cobbles
| 2200
| 4.2%
| 12%
|-
| 18
! scope="row" style="text-align: left;" | [[Paterberg]]
| 13
| cobbles
| 400
| 12.5%
| 20%
|}

Additionally, there were seven sectors of flat cobbled roads:<ref name="Wegwijzer" />
[[File:Sint-Kornelis-Horebeke - Haaghoek 2.jpg|thumb|right|The Haaghoek in [[Horebeke|Sint-Kornelis-Horebeke]] is the penultimate flat cobbled sector of the race, at 101 km from the finish.]]
{| class="wikitable plainrowheaders sortable" style="text-align:center;"
|+ Flat cobbled roads in the 2016 Tour of Flanders
! scope="col" | Number
! scope="col" | Cobbled Sector
! scope="col" | Distance from finish ({{abbr|km|kilometres}})
! scope="col" | Length ({{abbr|m|metres}})
|-
| 1
! scope="row" style="text-align: left;" | [[Huisepontweg]]
| 173
| 1600
|-
| 2
! scope="row" style="text-align: left;" | [[Ruitersstraat]]
| 130
| 800
|-
| 3
! scope="row" style="text-align: left;" | [[Mater-Kerkgate]]
| 127
| 2650
|-
| 4
! scope="row" style="text-align: left;" | [[Jagerij]]
| 124
| 800
|-
| 5
! scope="row" style="text-align: left;" | [[Paddestraat]]
| 113
| 2300
|-
| 6
! scope="row" style="text-align: left;" | [[Haaghoek]]
| 101
| 2000
|-
| 7
! scope="row" style="text-align: left;" | [[Mariaborrestraat]]
| 40
| 2000
|}

==Participating teams==

The 18 [[List of 2016 UCI WorldTeams and riders|UCI WorldTeams]] were automatically invited and were obliged to participate in the race.<ref>{{cite web|url=http://www.uci.ch/mm/Document/News/Rulesandregulation/16/82/39/2-ROA-20150619-E_English.pdf|title=UCI Cycling Regulations: Part 2: Road Races page 110 article 2.15.127|work=[[Union Cycliste Internationale]]|accessdate=20 February 2016|archive-url=https://web.archive.org/web/20150702030045/http://www.uci.ch/mm/Document/News/Rulesandregulation/16/82/39/2-ROA-20150619-E_English.pdf|archive-date=2 July 2015}}</ref> An additional seven [[List of 2016 UCI Professional Continental and Continental teams|UCI Professional Continental teams]] were given [[wildcard (sports)|wildcard]] entries: two Belgian teams ({{ct|WGG|2016}} and {{ct|TSV|2016}}), a Dutch team ({{ct|ROO|2016}}), a German team ({{ct|BOH|2016}}), a French team ({{ct|DEN|2016}}), an Italian team ({{ct|STH|2016a}}) and a Polish team ({{ct|CCC|2016}}).<ref>{{cite web|title=Direct Energie, Southeast picked as Tour of Flanders wildcards. Six teams in addition to 18 WorldTour squads announced|url=http://www.cyclingnews.com/news/direct-energie-southeast-picked-as-tour-of-flanders-wildcards/|website=[[Cycling News]]|publisher=[[Immediate Media Company]]|accessdate=20 February 2016|archivedate=22 January 2016}}</ref> With eight on each team, the [[peloton]] at the start of the race included 200 riders. Of these, 118 reached the finish line.<ref name="pcs-result">{{cite web|title=Ronde van Vlaanderen / Tour des Flandres|url=http://www.procyclingstats.com/race/Ronde_van_Vlaanderen_2016|website=ProCyclingStats|accessdate=9 April 2016}}</ref>

{{cyclingteamlist|title=UCI WorldTeams|ALM|AST|BMC|team1={{ct|CPT|2016a}}|DDD|EQS|FDJ|IAM|LAM|LTS|MOV|team2={{ct|OBE|2016a}}|TGA|KAT|TLJ|SKY|TNK|TFR|year=2016}}
{{cyclingteamlist|title=UCI Professional Continental teams|BOH|CCC|DEN|ROO|team1={{ct|STH|2016a}}|TSV|WGG|year=2016}}

==Pre-race favourites==

The principal favourites for the race were [[Tom Boonen]] ({{ct|EQS|2016}}) and [[Fabian Cancellara]] ({{ct|EQS|2016}}), both of whom had won the race on three previous occasions.<ref name="cn-preview">{{cite news|last1=Woodpower|first1=Zeb|title=Tour of Flanders 2016: Preview|url=http://www.cyclingnews.com/tourofflanders/preview/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=31 March 2016}}</ref> Cancellara was in strong form, having won the [[2016 Strade Bianche|Strade Bianche]] and having come fourth in [[2016 E3 Harelbeke|E3 Harelbeke]] and [[2016 Gent–Wevelgem|Gent–Wevelgem]] the previous week. Boonen, however, was not in strong form: he had not won a major classic since 2012 and had not performed strongly in the previous week's races. Nevertheless, ''[[Cycling Weekly]]'' wrote "you can never write the three-time Flanders winner off in a race like this."<ref name="cw-preview">{{cite news|last1=Clarke|first1=Stuart|title=Nine riders to watch at the Tour of Flanders|url=http://www.cyclingweekly.co.uk/news/racing/nine-riders-watch-tour-flanders-218816|accessdate=9 April 2016|work=[[Cycling Weekly]]|publisher=[[Time Inc. UK]]|date=3 April 2016|language=en-US}}</ref> The other former winners present at the start were [[Alexander Kristoff]] ({{ct|KAT|2016}}), the defending champion, and Cancellara's teammate [[Stijn Devolder]]. Kristoff's strong sprint gave him an advantage if he was in a group that finished together; he had, however, been ill in the previous week. Devolder, meanwhile, was expected to work for Cancellara.<ref name="cw-preview"/>

The other major favourite for the race was [[Peter Sagan]] ({{ct|TNK|2016}}), the [[2015 UCI Road World Championships – Men's road race|reigning world road race champion]]. Sagan had come second in E3 Harelbeke and first in Gent–Wevelgem. He had the advantage of being able to follow attacks and also to wait to use his strong sprint if he came to the finish with other riders.<ref name=cw-preview/><ref name="vn-preview">{{cite news|last1=Fretz|first1=Caley|title=Preview: Flanders is bike racing at its finest|url=http://velonews.competitor.com/2016/03/news/preview-flanders-bike-racing-finest_400515|accessdate=9 April 2016|work=[[VeloNews]]|publisher=[[Competitor Group, Inc.]]|date=31 March 2016|language=en-US}}</ref>

Other riders with a chance of victory included [[Michał Kwiatkowski]] ({{ct|SKY|2016}}), who had won E3 Harelbeke ahead of Sagan and who had a strong team with [[Geraint Thomas]], [[Ian Stannard]] and [[Luke Rowe]]; [[Greg Van Avermaet]] ({{ct|BMC|2016}}); Boonen's teammates [[Niki Terpstra]] and [[Zdeněk Štybar]]; [[Sep Vanmarcke]] ({{ct|TLJ|2016}}); and [[Tiesj Benoot]] ({{ct|LTS|2016}}).<ref name=cw-preview/><ref name=vn-preview/>

==Race summary==

[[File:Antwerpen - Tour de France, étape 3, 6 juillet 2015, départ (195).JPG|thumb|upright|[[Imanol Erviti]], who rode for {{convert|180|km}} in the breakaway and finished seventh ''(pictured at the [[2015 Tour de France]])'']]
Before the race began, there was a minute's silence in memory of [[Antoine Demoitié]], a {{ct|WGG|2016|nolink=y}} rider who had been killed in a crash with a motorbike during Gent–Wevelgem. After {{convert|25|km}}, the riders arrived in [[Hooglede]], the birthplace of [[Daan Myngheer]], a rider for {{ct|RLM|2016}} who had died following a heart attack in the [[2016 Critérium International|Critérium International]]; the peloton rode slowly through the town in memory of him.<ref name="cn-report">{{cite news|title=Peter Sagan storms to Tour of Flanders win|url=http://www.cyclingnews.com/tourofflanders/results/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref>

The day's [[breakaway (cycling)|breakaway]] took a long time to form: over an hour into the day's racing, with more than {{convert|70|km}} covered, a six-man move broke free. The riders were [[Hugo Houle]] ({{ct|ALM|2016}}), [[Federico Zurlo]] ({{Ct|LAM|2016}}), [[Imanol Erviti]] ([[Movistar Team|Movistar]]), [[Lukas Pöstlberger]] (Bora–Argon 18), [[Gijs Van Hoecke]] (Topsport Vlaanderen–Baloise) and [[Wesley Kreder]] (Roompot–Oranje Peloton).<ref name=cn-report/><ref name="cw-report">{{cite news|last1=Wynn|first1=Nigel|title=Peter Sagan wins Tour of Flanders|url=http://www.cyclingweekly.co.uk/news/racing/peter-sagan-wins-tour-of-flanders-219413|accessdate=9 April 2016|work=[[Cycling Weekly]]|publisher=[[Time Inc. UK]]|date=3 April 2016|language=en-US}}</ref> They earned a lead of over four minutes, but broke apart as the day's climbs began.<ref name=cw-report/>

During the middle part of the race several riders were forced to withdraw after crashes. [[Arnaud Démare]] ({{ct|FDJ|2016}}), the winner of [[2016 Milan–San Remo|Milan–San Remo]], crashed after {{convert|100|km}}, while [[Marcus Burghardt]] (BMC) and Tiesj Benoot crashed on the Wolvenberg shortly afterwards.<ref name=cn-report/> Less than {{convert|30|km}} later, several BMC riders crashed together and four were forced to withdraw. These included Greg Van Avermaet, who broke his collarbone.<ref name="vn-crashes">{{cite news|last1=Hood|first1=Andrew|title=Van Avermaet, Demare, Benoot crash out of De Ronde|url=http://velonews.competitor.com/2016/04/news/van-avermaet-demare-benoot-crash-out_400814|accessdate=9 April 2016|work=[[VeloNews]]|publisher=[[Competitor Group, Inc.]]|date=3 April 2016|language=en-US}}</ref>

[[File:Gent - Omloop Het Nieuwsblad, 27 februari 2016 (E11).JPG|thumb|upright|[[Peter Sagan]], wearing the rainbow-striped jersey of the reigning world champion ''(pictured at the [[2016 Omloop Het Nieuwsblad]])'']]
On the Molenberg, with more than {{convert|110|km}} remaining, a hard effort by [[Tony Martin (cyclist)|Tony Martin]] (Etixx–Quick-Step) caused a split in the main peloton, with 25 riders in the front group, but the groups came back together shortly afterwards. [[André Greipel]] (Lotto–Soudal) and [[Nils Politt]] (Katusha) then attacked on the Leberg and were allowed to go; they were followed by [[Dmitriy Gruzdev]] ({{ct|AST|2016}}) and [[Dimitri Claeys]] ({{ct|WGG|2016|nolink=Y}}). These four riders joined up with Houle, Erviti and Van Hoecke from the original break; Houle was dropped soon afterwards to form a six-man leading group, which had a two-minute lead at the foot of the second ascent of the Oude Kwaremont.<ref name=cn-report/><ref name=cw-report/> On the climb, [[Stijn Vandenbergh]] (Etixx–Quick-Step) and [[Dylan van Baarle]] ({{ct|CPT|2016a}}) attacked from the peloton. Another group, including Sep Vanmarcke and Ian Stannard, attacked before the Koppenberg. Stannard then went solo over the top of the climb.<ref name=cn-report/> Meanwhile, Vandenbergh and Van Baarle caught up with the group of leaders.<ref name=cw-report/>

There were several more attacks before the climb of the Taaienberg, where a small group of favourites formed and quickly caught Stannard. Shortly afterwards, with {{convert|30|km}} remaining, Michał Kwiatkowski and Peter Sagan attacked together and were joined by Sep Vanmarcke.<ref name=cn-report/> They crossed the Kruisberg together and caught the remainder of the breakaway with {{convert|23.5|km}} remaining, 40 seconds ahead of the peloton.<ref name=cw-report/> On the final climb of the Oude Kwaremont, Kwiatkowski was unable to follow Sagan and Vanmarcke, while Cancellara rode clear of the peloton. Sagan was first to the summit with Vanmarcke; Cancellara was twelve seconds behind. Cancellara was then caught by Niki Terpstra, Erviti and Claeys.<ref name=cn-report/>

On the final climb, the Paterberg, Sagan rode away from Vanmarcke, who was caught by Cancellara at the top of the climb. They rode in pursuit of Sagan, who had a 15-second lead at the summit. The chasing pair were unable to bring Sagan back and he rode to the finish to win his first Monument. Cancellara finished second, 25 seconds back, with Vanmarcke allowing him to take second place. Kristoff won the sprint for fourth place, ahead of [[Luke Rowe]] (Sky), 49 seconds behind Sagan.<ref name=cn-report/>

==Result==
{{cyclingresult start|title=Top 10 (of 100 finishers)<ref name=pcs-result/>}}
{{cyclingresult|1|[[Peter Sagan]]|SVK|{{ct|TNK|2016}}|6hr 10' 37"}}
{{cyclingresult|2|[[Fabian Cancellara]]|SUI|{{ct|TFR|2016}}|+ 25"}}
{{cyclingresult|3|[[Sep Vanmarcke]]|BEL|{{ct|TLJ|2016}}|+ 28"}}
{{cyclingresult|4|[[Alexander Kristoff]]|NOR|{{ct|KAT|2016}}|+ 49"}}
{{cyclingresult|5|[[Luke Rowe]]|GBR|{{ct|SKY|2016}}|+ 49"}}
{{cyclingresult|6|[[Dylan van Baarle]]|NED|{{ct|CPT|2016a}}|+ 49"}}
{{cyclingresult|7|[[Imanol Erviti]]|ESP|{{ct|MOV|2016}}|+ 49"}}
{{cyclingresult|8|[[Zdeněk Štybar]]|CZE|{{ct|EQS|2016}}|+ 49"}}
{{cyclingresult|9|[[Dimitri Claeys]]|BEL|{{ct|WGG|2016}}|+ 49"}}
{{cyclingresult|10|[[Niki Terpstra]]|NED|{{ct|EQS|2016}}| + 49"}}
{{cyclingresult end}}

==Post-race analysis==

===Rider reactions===

Sagan celebrated his victory by doing a [[wheelie]] after the finishing line. He said afterwards that it was the hardest Tour of Flanders he had ever raced, having been "full gas" throughout and suggested that Cancellara had made a mistake by not following the attack he had made with Kwiatkowski and Vanmarcke.<ref>{{cite news|last1=Ryan|first1=Barry|title=Sagan: Tour of Flanders was full gas from start to finish|url=http://www.cyclingnews.com/news/sagan-tour-of-flanders-was-full-gas-from-start-to-finish/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref> Cancellara was emotional after finishing his final Tour of Flanders. He said that he had "missed this one second", referring to the attack by Sagan. He said that he and Vanmarcke had done all they could in the chase, but that Sagan was too strong.<ref>{{cite news|last1=O'Shea|first1=Sadhbh|title=Cancellara misses out on swansong victory at Tour of Flanders|url=http://www.cyclingnews.com/news/cancellara-misses-out-on-swansong-victory-at-tour-of-flanders/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref> Vanmarcke, meanwhile, said that he had struggled due to crashes in the middle part of the race and then had suffered cramp when Sagan attacked on the Paterberg. He described the move by Sagan and Kwiatkowski as the "decisive moment in the race".<ref>{{cite news|last1=Decaulwé|first1=Brecht|title=Vanmarcke back on the podium at Tour of Flanders|url=http://www.cyclingnews.com/news/vanmarcke-back-on-the-podium-at-tour-of-flanders/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref>

Boonen, who finished fifteenth, described Sagan as "really strong" and as the "deserved winner"; he said "The way [Sagan] rode up the Paterberg was a good showcase on how to ride a bike". Boonen, meanwhile, turned his attention to [[2016 Paris–Roubaix|Paris–Roubaix]] the following week.<ref>{{cite news|last1=Decaluwé|first1=Brecht|title=Confident Boonen no factor in Tour of Flanders|url=http://www.cyclingnews.com/news/confident-boonen-no-factor-in-tour-of-flanders/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref> Van Avermaet described himself as "really disappointed" following his crash, which ruled him out of Paris–Roubaix.<ref>{{cite news|last1=Ryan|first1=Barry|title=Van Avermaet ruled out of Paris-Roubaix after Tour of Flanders crash|url=http://www.cyclingnews.com/news/van-avermaet-ruled-out-of-paris-roubaix-after-tour-of-flanders-crash/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref> Luke Rowe's fifth place was Team Sky's best ever performance in the Tour of Flanders; he praised Kwiatkowski's move, but said "there’s not much you can do when someone has better legs"; he described Sagan, Cancellara and Vanmarcke as "the three strongest guys in the race".<ref>{{cite news|last1=Ryan|first1=Barry|title=Rowe accepts the law of the strongest at Tour of Flanders|url=http://www.cyclingnews.com/news/rowe-accepts-the-law-of-the-strongest-at-tour-of-flanders/|accessdate=9 April 2016|work=[[Cyclingnews.com]]|publisher=[[Immediate Media Company]]|date=3 April 2016}}</ref> Imanol Erviti, who had been in the breakaway for {{convert|180|km}}, finished seventh. ''[[Cycling Weekly]]'' described this as "phenomenal"; it was Movistar's best ever result.<ref>{{cite news|last1=Clarke|first1=Stuart|title=Imanol Erviti makes history in phenomenal Tour of Flanders ride|url=http://www.cyclingweekly.co.uk/news/racing/imanol-erviti-makes-history-phenomenal-tour-flanders-ride-219509|accessdate=9 April 2016|work=[[Cycling Weekly]]|publisher=[[Time Inc. UK]]|date=4 April 2016|language=en-US}}</ref>

===World Tour standings===

Sagan remained at the top of the [[2016 UCI World Tour]] standings following his victory; his total of 329 points was more than 100 ahead of the next rider, his teammate [[Alberto Contador]]. Cancellara moved up from thirteenth to fourth and Vanmarcke from sixteenth to sixth.<ref name=wt-standings>{{cite web|title=UCI WorldTour Ranking - 2016|url=http://www.uci.html.infostradasports.com/cache/2/TheASP.asp@PageID%3D102007&SportID%3D102&CompetitionID%3D24055&SeasonID%3D490&EventID%3D12146&GenderID%3D1&ClassID%3D1&EventPhaseID%3D1444755&Phase1ID%3D1444789&TaalCode%3D2&StyleID%3D0&Cache%3D2.html?579662|website=uci.ch|publisher=[[Union Cycliste Internationale]]|accessdate=9 April 2016|archiveurl=https://web.archive.org/web/20160409221857/http://www.uci.html.infostradasports.com/cache/2/TheASP.asp@PageID%3D102007&SportID%3D102&CompetitionID%3D24055&SeasonID%3D490&EventID%3D12146&GenderID%3D1&ClassID%3D1&EventPhaseID%3D1444755&Phase1ID%3D1444789&TaalCode%3D2&StyleID%3D0&Cache%3D2.html?579662|archivedate=9 April 2016|date=3 April 2016}}</ref> Sagan's total put Slovakia in third place in the nations' rankings, ahead of Great Britain and Spain, even though he was the only Slovakian to have scored World Tour points.<ref>{{cite news|last1=Wynn|first1=Nigel|title=Peter Sagan now has more WorldTour points than British riders' combined total|url=http://www.cyclingweekly.co.uk/news/racing/peter-sagan-now-worldtour-points-great-britain-219560|accessdate=9 April 2016|work=[[Cycling Weekly]]|publisher=[[Time Inc. UK]]|date=4 April 2016|language=en-US}}</ref>

{{cyclingresult start|title=UCI World Tour standings on 3 April 2016<ref name=wt-standings/>|points=yes}}
{{cyclingresult|1|[[Peter Sagan]]|SVK|{{ct|TNK|2016}}|329}}
{{cyclingresult|2|[[Richie Porte]]|AUS|{{ct|BMC|2016}}|222}}
{{cyclingresult|3|[[Alberto Contador]]|ESP|{{ct|TNK|2016}}|171}}
{{cyclingresult|4|[[Fabian Cancellara]]|SUI|{{ct|TFR|2016}}|166}}
{{cyclingresult|5|[[Greg Van Avermaet]]|BEL|{{ct|BMC|2016}}|162}}
{{cyclingresult|6|[[Sep Vanmarcke]]|BEL|{{ct|TLJ|2016}}|141}}
{{cyclingresult|7|[[Arnaud Démare]]|FRA|{{ct|FDJ|2016}}|137}}
{{cyclingresult|8|[[Sergio Henao]]|COL|{{ct|SKY|2016}}|115}}
{{cyclingresult|9|[[Simon Gerrans]]|AUS|{{ct|OBE|2016a}}|113}}
{{cyclingresult|10|[[Alexander Kristoff]]|NOR|{{ct|KAT|2016}}|106}}
{{cyclingresult end}}

==References==

===Sources===
* {{cite book|title=Ronde van Vlaanderen (road book)|trans-title=Tour of Flanders|url=http://rvv.pro.p.assets.flandersclassics.be/files/3333/original/FC16_RVV_TECHNISCGEGIDS_MEN_def.pdf?1458659642|publisher=Flanders Classics|year=2016|location=Vilvoorde|ref={{harvid|Roadbook|2016}}|accessdate=2 April 2016}}

===Footnotes===
{{Reflist|30em}}

==See also==
*[[2016 in men's road cycling]]

==External links==
{{commons category|2016 Tour of Flanders|2016 Tour of Flanders}}
* {{Official website|http://www.rondevanvlaanderen.be/en}}

{{2016 UCI World Tour}}
{{Tour of Flanders}}

{{DEFAULTSORT:2016 Tour of Flanders}}
[[Category:Tour of Flanders]]
[[Category:2016 UCI World Tour|Tour of Flanders]]
[[Category:2016 in Belgian sport|Tour of Flanders]]