{{Infobox journal
| title = Annales de Gergonne
| cover = 
| caption = 
| former_name = Annales de Mathématiques pures et appliquées
| abbreviation = 
| discipline = Mathematics
| language = French
| editor = [[Joseph Gergonne]]
| publisher = 
| country = France
| history = 1810–1822
| frequency = Monthly
| openaccess = 
| license = 
| impact = 
| impact-year = 
| CODEN = 
| JSTOR = 
| LCCN = 
| OCLC = 
| website = http://www.numdam.org/numdam-bin/feuilleter?j=AMPA&sl=0
| link1 = 
| link1-name = 
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
}}
The '''''Annales de Gergonne''''' was a [[mathematical journal]] published in [[Nimes, France]] from 1810 to 1831 by [[Joseph-Diaz Gergonne]]. '''''Annales de Mathématiques pures et appliquées''''' was largely devoted to [[geometry]], with additional articles on history, philosophy, and [[mathematics education]] showing [[interdisciplinarity]].<ref> Christian Gerini (2008) "Les Annales de Mathématique de Gergonne : un journal du 19ème siècle numérisé et mediatisé au bénéfice d’une interdisciplinarité entre mathématique, histoire, didactique et philosophie", ''Actes del’European Summer University on the History and Epistemology in Mathematics Education'', ([https://archivesic.ccsd.cnrs.fr/sic_00397233/en/ CNRS citation])</ref><ref>{{citation |title=Les journaux de mathématiques dans la première moitié du xixe siècle en Europe |first=Norbert |last=Verdier |journal=Philosophia Scientiae |volume=13 |issue=2 |year=2009 |pages=97–126 |url=http://philosophiascientiae.revues.org/297 |language=French}}</ref>

"In the ''Annales'', Gergonne established in form and content a set of exceptionally high standards for mathematical journalism. New symbols and new terms to enrich mathematical literature are found here for the first time. The journal, which met with instant approval, became a model for many another editor. Cauchy, Poncelet, Brianchon, Steiner, Plucker, Crelle, Poisson, Ampere, Chasles, and Liouville sent articles for publication."<ref> Laura Guggenbuhl (1959) "Gergonne, founder of the Annales de Mathématiques", [[Mathematics Teacher]] 52(8):621–9</ref>

[[Operational calculus]] was developed in the journal in 1814 by [[Francois-Joseph Servois]].<ref>[[Francois-Joseph Servois]] (1814) [http://www.numdam.org/item?id=AMPA_1814-1815__5__93_0 Analise Transcendante. Essai sur unNouveu Mode d'Exposition des Principes der Calcul Differential], ''Annales de Gergonne'' 5: 93–140</ref>

==References==
{{Reflist|30em}}

==External links==
* [http://www.numdam.org/numdam-bin/feuilleter?j=AMPA&sl=0 Archive Tome 1 to Tome 22] from NUMDAM (Numerisation de documents anciens mathematiques) at [[CNRS]]

[[Category:Mathematics journals]]
[[Category:French-language journals]]
[[Category:Defunct journals]]
[[Category:Publications established in 1810]]
[[Category:Monthly journals]]
[[Category:Publications disestablished in 1822]]