The '''''Dutch Journal of Music Theory''''', or ''Tijdschrift voor Muziektheorie'' (ISSN 1385-3066 [print], 1876-2824 [online]) was a peer-reviewed, [[academic journal]] specializing in [[music theory]] and [[musical analysis|analysis]]. It appeared from 1996 until 2013, published by Amsterdam University Press, triannually in issues of about 80 pages.<ref>http://www.djmt.nl/cgi/t/text/text-idx?c=djmt;sid=53b038676add61db7c6b4d82e6ca56c8;tpl=abonnement-en.tpl</ref> It includes articles and reviews in Dutch, English, and German.

Its website describes the ''Dutch Journal of Music Theory'' as serving "the professional music theorist, the musician or musicologist whose work involves music theory, and the reader who is otherwise interested in the structure and function of music. [It] contains main articles, research papers and essays, and includes discussion, book reviews, reports and announcements. The journal covers a wide range of music: from medieval to contemporary music, form classical to non-western music, from pop to jazz music. In addition to submissions which focus specifically on music theory, the journal also welcomes contributions addressing music pedagogical, historical, philosophical, or critical issues."<ref>http://www.djmt.nl/cgi/t/text/text-idx?c=djmt;sid=53b038676add61db7c6b4d82e6ca56c8;tpl=about-en.tpl</ref>

From 2014 onwards the ''Dutch Journal of Music Theory'' is published by Leuven University Press and appears under the new name [http://upers.kuleuven.be/en/series/music-theory-and-analysis-mta ''Music Theory and Analysis (MTA), International Journal of the Dutch-Flemish Society for Music Theory'']. It is edited by Pieter Bergé, Steven Vande Moortele and Nathan J. Martin.

== Notes ==
{{reflist|2}}

==External links==
* [http://www.djmt.nl/ ''Dutch Journal of Music Theory'' website]
* [http://www.upl.be/mta ''Music Theory and Analysis'' website]

[[Category:Music theory journals]]
[[Category:Contemporary classical music journals]]
[[Category:Triannual journals]]
[[Category:Publications established in 1996]]
[[Category:Multilingual journals]]