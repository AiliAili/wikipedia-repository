{{Infobox road
|state=NJ
|type=NJ
|route=324
|alternate_name=Old Ferry Road
|map=New Jersey Route&nbsp;324 map.svg
|map_notes=Map of Route&nbsp;324 is highlighted in red
|length_mi=1.51
|length_ref=<ref name="sld">{{cite web|publisher=[[New Jersey Department of Transportation]]|url=http://www.state.nj.us/transportation/refdata/sldiag/00000324__-.pdf|format=PDF|title=Route&nbsp;324 straight line diagram|accessdate=October 17, 2009|year=2009}}</ref>

|direction_a=West
|terminus_a=Shore of the [[Delaware River]] in [[Logan Township, New Jersey|Logan Township]]
|direction_b=East
|terminus_b=[[Cul-de-sac]] in Logan Township
|established=1974
|counties=[[Gloucester County, New Jersey|Gloucester]]
|previous_type=US
|previous_route=322
|next_type=NJ
|next_route=347
}}
'''Route&nbsp;324''' is an isolated [[state highway]] in [[Logan Township, New Jersey|Logan Township]] in the U.S. state of [[New Jersey]]. The two-lane concrete route runs along the {{convert|1.51|mi|km|adj=mid|-long}} alignment of Old Ferry Road from the shore of the [[Delaware River]] to a cul-de-sac near the interchange between [[U.S. Route&nbsp;322 in New Jersey|U.S. Route&nbsp;322]] and [[U.S. Route&nbsp;130]] in Logan Township. The route does not intersect with any state routes or county routes along its entire alignment.

Route&nbsp;324 was a former alignment of U.S. Route&nbsp;322 that served the Bridgeport-Chester ferry between [[Bridgeport, New Jersey]], and [[Chester, Pennsylvania]]. The ferry first ran on July 1, 1930, with Route&nbsp;322 being designated along the ferry and its access road in 1936 from [[Pennsylvania]]. The highway and ferry also shared the co-designation of '''State Highway Route&nbsp;S-44''', a spur off [[New Jersey Route&nbsp;44|State Highway Route&nbsp;44]] in Bridgeport. Route&nbsp;S-44 was decommissioned in the [[1953 New Jersey state highway renumbering|state highway renumbering]] while Route&nbsp;322 remained intact until the opening of the [[Commodore Barry Bridge]] in February 1974. The ferry made its last run at 8&nbsp;p.m. on February 1, and closed down for good. Route&nbsp;322 was realigned onto the Commodore Barry Bridge while the former ferry alignment became Route&nbsp;324.

== Route&nbsp;description ==
[[File:NJ 324 at Springers Lane.jpg|left|thumb|Route&nbsp;324 at the junction with Springer Lane, with an old [[U.S. Route&nbsp;322 in New Jersey|US&nbsp;322]] shield on the dirt aside the highway]]
Route&nbsp;324 begins at a dead-end along the shoreline of the [[Delaware River]] and the wreckage of the old ferry dock in [[Logan Township, New Jersey|Logan Township]]. Route&nbsp;324 heads eastward along Old Ferry Road, a two-lane concrete roadway surrounded by trees and fields. The two-lane concrete highway remains such for a distance, paralleling [[U.S. Route 322 in New Jersey|U.S. Route&nbsp;322]] to the south, passing a business. The route heads to the east, crossing south of a pond and intersecting with Springer Lane and a dirt road in Logan Township.<ref name="bing">{{bing maps |title= Overview map of New Jersey Route&nbsp;324 |url= http://www.maps.bing.com/maps/default.aspx?v=2&FORM=LMLTCP&cp=qn2s7b8qk2qz&style=b&lvl=1&tilt=-90&dir=0&alt=-1000&phx=0&phy=0&phscl=1&scene=39921883&rtp=pos.39.8047998547554_-75.3509491682053_near%20Old%20Ferry%20Rd%2C%20Bridgeport%2C%20New%20Jersey%2008014%2C%20United%20States__~pos.39.8182994127274_-75.3727501630783_near%20Old%20Ferry%20Rd%2C%20Bridgeport%2C%20New%20Jersey%2008014%2C%20United%20States__&rtop=0~0~0&encType=1 |accessdate= October 17, 2009}}</ref>

At Springer Lane, which is a former alignment of [[New Jersey Route 44|Route&nbsp;44]], the highway comes into the open, crossing under power lines and intersecting with former alignments of roadway, overgrown with grass. A short distance from Springers Lane, the highway continues to the only other intersection along the route, which is for Island Road, a connector to [[U.S. Route&nbsp;130]]. The highway, however, continues through the desolate portions of Logan Township along a power line. A short distance later, the route passes to the south of the only development along the highway, a boat marina and two homes. Route&nbsp;324 continues as a two-lane concrete road eastward until reaching a cul-de-sac just short of the U.S. Route&nbsp;130/U.S. Route&nbsp;322 interchange in Logan Township, where the designation ends.<ref name="bing" />

==History==
[[File:NJ S44 (cutout).svg|right|thumb|75px|Route 322 between the ferry and US 130/SHR 44 was State Highway Route S-44 until 1953]]
The ferry route that Route&nbsp;324 serviced first made its crossings of the Delaware River on July 1, 1930, from [[Chester, Pennsylvania]], to [[Bridgeport, New Jersey]]. The ferry service was run with two boats, ''Chester'', a large boat with a capacity of 60&nbsp;motor vehicles, and ''Bridgeport'', a smaller boat with a capacity of 48. The ferry system was seen as an advantage across the Delaware, providing drivers a backup from the few bridges that existed along the river.<ref name="ferry">{{cite web|url=http://www.oldchesterpa.com/chester_bridgeport_ferry.htm|title=Chester–Bridgeport Ferry|year=2009|publisher=[[Chester, Pennsylvania]]|accessdate=October 17, 2009}}</ref> In 1936, the Bridgeport–Chester Ferry was granted the alignment for the extension of U.S. Route 322 from Pennsylvania and into New Jersey.<ref name="sld 2004">{{cite web|url=http://www.state.nj.us/transportation/refdata/sldiag/00000322__-.pdf |format=PDF|title=US 322 Straight Line Diagram|publisher=New Jersey Department of Transportation|year=2004|accessdate=June 14, 2007 |archiveurl = https://web.archive.org/web/20040724135549/http://www.state.nj.us/transportation/refdata/sldiag/00000322__-.pdf |archivedate = July 24, 2004}}</ref><ref name="mwm">{{cite map|publisher=Mid-West Map Co.|title= Map of Pennsylvania and New Jersey |year=1941|cartography=[[H.M. Gousha]]|url=http://www.mapsofpa.com/roadcart/1941_1467m.jpg|accessdate=March 29, 2009}}</ref> Along with the U.S. Route&nbsp;322 designation, the [[New Jersey State Highway Department]] designated the alignment of Route&nbsp;322 from the ferry to U.S. Route&nbsp;130, then designated as State Highway Route&nbsp;44, as State Highway Route&nbsp;S-44.<ref name=nj1939>State of New Jersey, Laws of 1939, compiled.</ref>
[[File:Chester-Bridgeport Ferry site.jpg|left|thumb|The former site of the Chester–Bridgeport ferry in Bridgeport, as seen from the terminus of Route&nbsp;324]]
{{see also|Commodore Barry Bridge}}
Route&nbsp;322 survived the [[1953 New Jersey state highway renumbering|state highway renumbering]] because of the fact that it was a U.S. Route. However, the co-designation on the ferry stretch, State Highway Route&nbsp;S-44 was decommissioned in favor of just one designation.<ref name="nj1953">[[Wikisource:1953 New Jersey state highway renumbering]]</ref><ref name=nyt1953>{{cite news|accessdate=July 20, 2009|title=New Road Signs Ready in New Jersey|work=[[The New York Times]]|date=December 16, 1952|url=http://img123.imageshack.us/img123/6933/19521216newroadsignsreaiu6.jpg}}</ref> U.S. Route&nbsp;322 remained on the ferry route for several years, and plans arose during the 1960s to construct a new bridge between the [[Delaware Memorial Bridge|Delaware Memorial]] and [[Walt Whitman Bridge|Walt Whitman]] bridges.<ref name="bridge70">{{cite news|title=Bridge Promises Brighter Life for Poor of Chester|last=Reilly|first=Robert A. |date=May 24, 1970|publisher=The Philadelphia Inquirer}}</ref> Construction began that year, and on February 1, 1974, the newly named Commodore Barry Bridge opened for traffic. At 8&nbsp;p.m. that evening, the Bridgeport–Chester Ferry ran its final {{convert|1|mi|km|adj=on}} boat ride across the Delaware, and the service was closed down for good.<ref name="opens">{{cite news|title=Bridgeport-Chester Bridge Opens, Replacing Ferry|date=February 2, 1974|publisher=''The [[New York Times]]''}}</ref> The alignment of U.S. Route&nbsp;322 was moved onto the Commodore Barry Bridge,<ref name="chevron">{{cite map|publisher=[[Chevron Oil Company]]|title= Map of New Jersey |year=1969|cartography=H.M. Gousha}}</ref> while the former alignment was redesignated Route&nbsp;324. The route has remained virtually intact since then.<ref name="sld" />

== Major intersections ==
{{Jcttop|length_ref=<ref name="sld"/>|county=Gloucester|location=Logan Township|state=NJ}}
{{NJint
|mile=0.00
|road=[[Delaware River]]
|notes=Western terminus }}
{{NJint
|mile=1.51
|road=Cul-de-sac
|notes=Eastern terminus}}
{{Jctbtm}}

==See also==
*{{Portal-inline|U.S. Roads}}
*{{Portal-inline|New Jersey}}

==References==
{{Reflist|2}}

== External links ==
{{Attached KML|display=inline,title}}
*[https://www.youtube.com/watch?v=Xivx3I5dejo Eastbound] and [https://www.youtube.com/watch?v=sgaArVLGP1w westbound] video tours of Route 324
*[http://www.state.nj.us/transportation/refdata/traffic_orders/speed/rt324.shtm Speed Limits for State Roads: Route 324]
{{good article}}

{{DEFAULTSORT:324}}
[[Category:State highways in New Jersey]]
[[Category:Transportation in Gloucester County, New Jersey]]
[[Category:U.S. Route 322|New Jersey Route 324]]