{{Italic title}}
The '''''Connaissance des temps''''' (English: Knowledge of Time) is an official yearly publication of astronomical [[ephemerides]] in France. Until just after the [[French Revolution]], the title appeared as '''''Connoissance des temps''''', and for several years afterwards as '''''Connaissance des tems'''''.

== History ==
''Connaissance des temps'' is the oldest such publication in the world, published without interruption since 1679 (originally named  ''La Connoissance des Temps ou calendrier et éphémérides du lever & coucher du Soleil, de la Lune & des autres planètes''), when the astronomer Jean Picard  (1620–1682) obtained from the King the right to create the journal. The first eight editors were:

*1679–1684: [[Jean Picard]] (1620–1682)
*1685–1701: [[Jean Le Fèvre (astronomer)|Jean Le Fèvre]] (1650–1706)
*1702–1729: Jacques Lieutaud (1660–1733)
*1730–1734: [[Louis Godin]] (1704–1760)
*1735–1759: [[Giovanni Domenico Maraldi]] (1709–1788)
*1760–1775: [[Joseph Jérôme Lefrançois de Lalande]] (1732–1807)
*1776–1787: [[Edme-Sébastien Jeaurat]] (1725–1803)
*1788–1794: [[Pierre Méchain]] (1744–1804)

Other notable astronomers who edited the ''Connaissance des temps'' were:

*[[Alexis Bouvard]] (1767–1843)<ref>{{cite book |title=The Biographical Encyclopedia of Astronomers |last=Hockey |first=Thomas |year=2009 |publisher=[[Springer Publishing]] |isbn=978-0-387-31022-0 |accessdate=July 15, 2015 |url=http://www.springerreference.com/docs/html/chapterdbid/58193.html}}</ref>
*[[Bureau des longitudes]]
**[[Rodolphe Radau]] (1835–1911)
**[[Marie Henri Andoyer]] (1862–1929)

Among the other prestigious national astronomical ephemerides, ''[[The Nautical Almanac]]'' was only established in 1767 and the ''[[Berliner Astronomisches Jahrbuch]]'' in 1776.

== Contents ==
The volumes of the ''Connaissance des temps'' had two parts:
* a section of ephemerides, containing various tables
* articles giving a deeper coverage of various topics, often written by famous astronomers

== References ==
{{Reflist}}
*[[Jérôme Lalande|Lalande]], {{Google books |title="Bibliographie astronomique: avec l'histoire de l'astronomie depuis 1781 jusqu'à 1802", 1803 |page=312 |id=7fDPAAAAMAAJ }}
* {{Google books |title=Cosmos, 2e série, tome II, 1865 |page=375 |id=pRRbAAAAQAAJ }}
* [http://livres.edpsciences.org/ouvrage.php?ISBN=2-86883-657-7]

== External links ==
* [http://gallica.bnf.fr/ark:/12148/cb327469896/date Connaissance des Temps for the years 1679 to 1803]
* [http://gallica.bnf.fr/ark:/12148/cb34354613f/date Connaissance des Temps for the years 1804 to 1961]
* [http://gallica.bnf.fr/ark:/12148/cb343785544/date Annuaire pour l'an ..., a popular version for the general public, for the years 1797 to 1969]
{{Authority control}}
{{DEFAULTSORT:Connaissance Des Temps}}
[[Category:Astronomical almanacs]]
[[Category:Publications established in 1679]]
[[Category:1679 establishments in France]]