{{good article}}
{{Infobox military person
|honorific_prefix = ''vojvoda''
|name          = Jovan Babunski
|native_name   = Јован Стојковић Бабунски
|birth_date    = {{birth date|df=yes|1878|12|25}}
|death_date    = {{death date and age|df=yes|1920|2|17|1878|12|25}}
|birth_place   = Martolci, [[Ottoman Empire]]
|death_place   = [[Veles (city)|Veles]], [[Kingdom of Serbs, Croats and Slovenes]]
|placeofburial =
|placeofburial_label= 
|image         = Vojvoda Babunski.jpg
|nickname      = Babunski
|allegiance    = {{flagicon image|Chetniks Flag.svg}} [[Chetniks]] (1905–1920)<br/>{{flag|Kingdom of Serbia}} (1908–1920)<br/>{{flag|Kingdom of Serbs, Croats and Slovenes}} (1918–1920)
|serviceyears  = 1905–1920
|rank          = ''[[vojvoda]]''
|branch        = 
|commands      =
|unit          =
|battles       = {{tree list}}
*[[Macedonian Struggle]]
*[[First Balkan War]]
*[[Second Balkan War]]:
**{{Tree list/final branch}}[[Battle of Bregalnica]]
{{tree list/end}}
{{tree list}}
*[[World War I]]:
**[[Serbian Campaign (World War I)|Serbian Front]]
**{{Tree list/final branch}}[[Salonika Front]]
{{tree list/end}}
|awards        = {{flagicon image|Order of the Karađorđe's Star with Swords rib.png}} [[Order of the Star of Karađorđe]]
}}

'''Jovan Stojković''' ({{lang-sr-cyr|Јован Стојковић}}; 25 December 1878&nbsp;– 17 February 1920), known as '''Jovan Babunski''' (Јован Бабунски), was a [[Serbs|Serbian]] [[Chetnik#World War I and the Kingdom of Yugoslavia|Chetnik]] commander ({{lang-sr|''vojvoda'', вoјвода}}) during the [[Balkan Wars]] and [[World War I]]. Following the murder of his brother and nephew by the Pro-Bulgarian [[Internal Macedonian Revolutionary Organization]] (IMRO), he joined a Chetnik band and took command of Chetnik units on the [[Vardar River]], where he and his men often engaged Bulgarian and Ottoman forces.

With the outbreak of the [[First Balkan War]] he joined the [[Serbian Army]] and was wounded while fighting in the village of Strevica. During the [[Second Balkan War]], he joined a Serbian volunteer detachment and fought at the [[Battle of Bregalnica]]. During [[World War I]], Babunski and his Chetnik detachment fought [[Austria-Hungary|Austro-Hungarian]] forces in the summer of 1914 and later fought on the [[Salonika Front]], where Babunski was ordained by French General [[Louis Franchet d'Espèrey]] after he and his men captured two German [[midget submarine]]s and their crews. After the war, Babunski and his 250-strong force helped Serb authorities suppress Bulgarian resistance in the Macedonian towns of [[Bitola]] and [[Tikveš]], committing several atrocities in the process. Considered one of the most famous Chetnik commanders of his time, Babunski died in Veles in February 1920.

==Early life==
Jovan Stojković was born in the village of Martolci in [[Macedonia (region)|central Macedonia]], at the foot of [[Babuna (mountain)|Mount Babuna]], near [[Veles (city)|Veles]], on 25 December 1878.{{sfn|Stanojević|2000|p=110}} In his youth, he was nicknamed "Babunski", by which he was referred to for the rest of his life.{{sfn|Palairet|2016|p=146}} He began attending [[primary school]] at the age of 10. It was here that his teacher [[Bulgarisation|Bulgarianized]] his name into "Ivan Stojkov". Displeased, Babunski's father took his son to the Serbian [[consulate]] in [[Skopje]] and requested that his son be transferred to a primary school in [[Belgrade]].{{sfn|Great Britain and the Eastern Question|1920|p=427}} Babunski's primary and secondary education took place in Belgrade, [[Valjevo]] and [[Niš]]. In his twenties, he worked as a [[Serbian language|Serbian]]-language teacher in [[Tetovo]] and Veles.{{sfn|Bechev|2009|p=21}}

==Guerrilla activities==
[[File:Srpske Vojvode u Staroj Srbiji i Makedoniji.jpg|thumb|left|Babunski ''(first row, third from right)'' with a group of Chetniks, during Macedonian Civil War 1903–08]]
[[File:Chetnik Voivodes.jpg|thumb|left|Chetnik leaders during [[legalisation]] in time of [[Young Turk Revolution]] in 1908. Babunski is seen standing, first from right.]]
In 1905, Babunski's brother and nephew were killed by the [[Internal Macedonian Revolutionary Organization]] ({{lang-bg|Вътрешна Македонска Революционна Организация}}; VMRO).{{sfn|Bechev|2009|p=21}} Seeking revenge, he joined the Chetnik band of [[Gligor Sokolović]] and [[Temeljko Barjaktarević]]. That year, he became a Chetnik ''[[vojvoda]]''.{{sfn|Great Britain and the Eastern Question|1920|p=427}} Afterwards, he defended the right bank of the [[Vardar River]] against Bulgarian insurgents and protected persecuted Serb villages against Bulgarian and Ottoman attacks.{{sfn|Bataković|2005|p=194}} This prompted the VMRO to place a [[Bounty (reward)|bounty]] of 20,000 [[Bulgarian lev|leva]] on his head.{{sfn|Great Britain and the Eastern Question|1920|p=427}} Through these actions, Babunski became one of the five leading Serbian guerilla chiefs in [[Macedonia (region)|Macedonia]].{{sfn|Banac|1984|p=316}} Babunski's participation in the struggle against the Ottomans and Bulgarians came at a great personal cost; his wife was tortured in order to disclose his whereabouts and one of his children was killed.{{sfn|Great Britain and the Eastern Question|1920|p=427}} With the [[Young Turk Revolution]] in 1908, the Ottomans declared a ceasefire between their forces and those of the Chetniks. Babunski left the Chetniks' ranks and returned to civilian life. He was later arrested by the Ottoman authorities, but quickly escaped from prison.{{sfn|Pejčić|2007|p=63}} That year, he returned to the [[Kingdom of Serbia|Serbia]].{{sfn|Bataković|2005|p=194}}
[[File:Јован Бабунски на Солунском фронту.jpg|thumb|right|150px|Babunski in a Serbian Army uniform with [[Order of Karađorđe's Star|Order of Karageorge's Star]], [[Legion of Honour]], [[Croix de guerre 1914-1918 (France)|Croix de guerre]] and [[Medal for Bravery (1912)|Medal for Bravery]] during World War I]]
Babunski fought with the [[Royal Serbian Army]] during the [[First Balkan War]] and was wounded in a skirmish with Ottoman Turkish forces in the village of Strevica while serving under commander [[Vojin Popović]].{{sfn|Pejčić|2007|p=63}} During the [[Second Balkan War]], he fought with a Serbian volunteer detachment at the [[Battle of Bregalnica]].{{sfn|Pejčić|2007|p=70}} 

Following the outbreak of [[World War I]], Babunski formed the [[Sava]] Chetnik detachment, which was placed under the command of Major [[Vojislav Tankosić]].{{sfn|Pejčić|2007|p=74}} The unit suffered its first casualties when [[Austria-Hungary|Austro-Hungarian]] [[river monitor]]s shelled Belgrade on the night of 28 July 1914, killing a 16-year-old Chetnik volunteer named Dušan Đonović, the first victim of the war. Shortly afterwards, Babunski's Chetniks destroyed a railway bridge on the Sava to prevent the Austro-Hungarians from crossing.{{sfn|Lyon|2015|p=97}} Babunski and his men returned to Macedonia in 1915 and fought Bulgarian guerillas.{{sfn|Pejčić|2007|pp=78–79}} That autumn Babunski and his Chetniks were assigned to the town of [[Kačanik]], where they joined other Serbian forces in fighting a Bulgarian division that they managed to hold to a standstill for nearly a month despite suffering heavy losses.{{sfn|Pejčić|2007|pp=82–85}} With the [[Serbian army's retreat through Albania]] that winter, Babunski and his men withdrew to the Greek island of [[Corfu]].{{sfn|Pejčić|2007|p=94}} They then joined Serb forces at the [[Salonika Front]].{{sfn|Bechev|2009|p=21}} Here, Babunski was assigned to the Serbian [[First Army (Serbia)|First Army]] and was involved in guarding [[Lake Prespa]] from the Bulgarians.{{sfn|Pejčić|2007|p=123}} Later, he and his Chetnik detachments participated in capturing enemy soldiers and gathering intelligence from the front.{{sfn|Pejčić|2007|p=378}} In 1917, French General [[Louis Franchet d'Espèrey]] awarded Babunski a medal following the capture of two German [[midget submarine]]s and their crews by him and his men.{{sfn|Pejčić|2007|p=380}} Babunski was also a recipient of the [[Order of the Star of Karađorđe]].{{sfn|Vlahović|1989|p=274}}

==Later life and legacy==
[[File:Babunski's Chetniks by the grave.jpg|thumb|right|Babunski's Chetniks by his grave.]]
After the war, Babunski's Chetniks were dispatched to Kosovo and Macedonia,{{sfn|Newman|2012|p=154}} reinforcing the 50,000 soldiers that had been deployed to quell the armed uprisings there.{{sfn|Ramet|2006|p=47}} Babunski's force of 250 men helped the authorities suppress resistance in the towns of [[Bitola]] and [[Tikveš]],{{sfn|Bechev|2009|pp=21–22}} targeting locals sympathetic to the Bulgarian ''[[Komitadji|komitas]]'', and committing several atrocities in the process.{{sfn|Banac|1984|p=320}} Forces under his command also committed several atrocities in Albania.{{sfn|Newman|2012|p=146}} Chetnik bands, including those of Babunski, are also said to have enslaved locals and turned them into forced labourers for the armed forces of the [[Kingdom of Serbs, Croats and Slovenes]].{{sfn|Ramet|2006|p=47}} By the summer of 1919, the authorities had decided that paramilitary formations such as Babunski's were not "furthering the state's aims in the region".{{sfn|Newman|2012|p=154}} Babunski died in Veles on 17 February 1920, after [[1918 flu pandemic|contracting influenza]].{{sfn|Great Britain and the Eastern Question|1920|p=427}}

The historian [[Dušan T. Bataković]] characterizes Babunski as "exceptionally courageous and determined".{{sfn|Bataković|2005|p=194}} John Paul Newman, a historian specializing in Yugoslavia's interwar paramilitary formations, believes Babunski would have become one of the most powerful figures in the [[interwar period|interwar]] Chetnik Association had it not been for his premature demise. Babunski was celebrated as a national hero following his death and featured heavily in veterans' commemorations during the interwar period.{{sfn|Newman|2015|pp=104–105}} A monument dedicated to him was constructed in Veles in 1924, but was destroyed by Bulgarian occupational authorities during [[World War II]], when Macedonia was annexed by Bulgaria following Yugoslavia's dismemberment by the [[Axis powers|Axis]].{{sfn|Bataković|2005|p=194}} Babunski's battles against Bulgarian guerillas in Ottoman Macedonia inspired the [[patriotic song]] ''Sprem'te se sprem'te, četnici'' or ''Srpska mi truba zatrubi''.{{sfn|Brock|1952|p=166}} The first version of the song ''[https://www.YouTube.com/watch?v=4SfPKN5KZg4 Serb Trump Plays For Me]'' was recorded and released as a singl in [[Richmond, Indiana]], [[US]] by vocals Vaso Bukvich with the "Yugoslavia" [[Tamburica]] Orchestra on July 23, 1924. <ref>[http://tamburitza78s.blogspot.rs/2009/09/yugoslavia-tamburasko-drustvo-part-2.html Tamburitza and Folk Music From America and Europa]</ref>His ''nom de guerre'', Babunski, was adopted as a surname by his descendants.<ref name=RdV>{{cite book|title=Recueil de Vardar|url=https://books.google.com/books?id=k2lpAAAAMAAJ|year=2006|publisher=Akademija|page=99}}</ref>

==Notes==
{{reflist|20em}}

{{commons category|Jovan Babunski}}

==References==
{{refbegin|40em}}
* {{cite book
  | last = Banac
  | first = Ivo
  | authorlink = Ivo Banac
  | year = 1984
  | title = The National Question in Yugoslavia: Origins, History, Politics
  | publisher = Cornell University Press,
  | location = Ithaca, New York
  | isbn = 978-0-8014-9493-2
  | url = https://books.google.ca/books?id=KfqbujXqQBkC
  | ref = harv
  }}
* {{cite book
  | last = Bataković
  | first = Dušan T.
  | authorlink = Dušan T. Bataković
  | year = 2005
  | title = Histoire du peuple serbe
  | trans_title = History of the Serb People
  | language = French
  | publisher = L'age d'Homme
  | location = Lausanne, Switzerland
  | isbn = 978-2-8251-1958-7
  | url = https://books.google.ca/books?id=a0jA_LdH6nsC
  | ref = harv
  }}
* {{cite book
  | last = Bechev
  | first = Dimitar
  | year = 2009
  | title = Historical Dictionary of the Republic of Macedonia
  | publisher = Rowman & Littlefield
  | location = Lanham, Maryland
  | isbn = 978-0-8108-6295-1
  | url = https://books.google.ca/books?id=ilGfCIF4Ao4C
  | ref = harv
  }}
* {{cite book
  | last = Brock
  | first = Ray
  | year = 1952
  | title = Blood, Oil and Sand
  | publisher = The Bodley Head
  | location = London, England
  | ref = harv
  }}
* {{cite journal
  | author = Great Britain and the Eastern Question
  | year = 25 March 1920
  | title = Serbian Sketches
  | journal = Great Britain and the Eastern Question
  | publisher = Journal of International Relations
  | volume = 17
  | number = 464
  | ref = harv
  }}
* {{cite book
  | last = Lyon
  | first = James
  | year = 2015
  | title = Serbia and the Balkan Front, 1914: The Outbreak of the Great War
  | publisher = Bloomsbury Publishing
  | location = New York, New York
  | isbn = 978-1-47258-006-1
  | url = https://books.google.ca/books?id=laYkCgAAQBAJ
  | ref = harv
  }}
* {{cite book
  | last = Newman
  | first = John Paul
  | editor1-last = Gerwarth
  | editor1-first = Robert
  | editor2-last = Horne
  | editor2-first = John
  | year = 2012
  | title = War in Peace: Paramilitary Violence in Europe After the Great War
  | chapter = Paramilitary Violence in the Balkans
  | publisher = Oxford University Press
  | location = Oxford, England
  | isbn = 978-0-19-968605-6
  | url = https://books.google.ca/books?id=Ap94gZsbu6QC
  | ref = harv
  }}
* {{cite book
  | last = Newman
  | first = John Paul
  | year = 2015
  | title = Yugoslavia in the Shadow of War: Veterans and the Limits of State Building, 1903–1945
  | publisher = Cambridge University Press
  | location = Cambridge, England
  | isbn = 978-1-10707-076-9
  | url = https://books.google.ca/books?id=Q8nSCQAAQBAJ
  | ref = harv
  }}
* {{cite book
  | last = Palairet
  | first = Michael
  | year = 2016
  | title = From the Fifteenth Century to the Present
  | series = Macedonia: A Voyage Through History
  | volume = 2
  | publisher = Cambridge University Press
  | location = Cambridge, England
  | isbn = 978-1-44388-849-3
  | url = https://books.google.ca/books?id=nyb5DAAAQBAJ
  | ref = harv
  }}
* {{cite book
  | last = Pejčić
  | first =  Predrag
  | year = 2007
  | title = Četnički pokret u Kraljevini Srbiji
  | trans_title = The Chetnik Movement in the Kingdom of Yugoslavia
  | language = Serbian
  | publisher = Pogledi
  | location = Kragujevac, Serbia
  | isbn = 978-86-82235-55-2
  | ref = harv
  }}
* {{cite book
  | last = Ramet
  | first = Sabrina P.
  | authorlink = Sabrina P. Ramet
  | year = 2006
  | title = The Three Yugoslavias: State-Building and Legitimation, 1918–2005
  | publisher = Indiana University Press
  | location = Bloomington, Indiana
  | isbn = 978-0-253-34656-8
  | url = https://books.google.com/books?id=FTw3lEqi2-oC
  | ref = harv
  }}
* {{cite book
  | last = Stanojević
  | first = Stanoje
  | year = 2000
  | title = Narodna enciklopedija: srpsko-hrvatsko-slovenačka
  | trans_title = Serb-Croat-Slovenian National Encyclopaedia
  | publisher = Budućnost
  | location = Belgrade, Yugoslavia
  | ref = harv
  }}
* {{cite book
  | last = Vlahović
  | first = Tomislav
  | year = 1989
  | title = Vitezovi Karađorđeve zvezde
  | trans_title = The Knights of the Star of Karađorđe
  | publisher = Beostar
  | location = Belgrade, Yugoslavia
  | ref = harv
  }}
{{refend}}

{{Chetniks in Macedonia|state=collapsed}}

{{DEFAULTSORT:Babunski, Jovan}}
[[Category:People from Čaška Municipality]]
[[Category:Serbian military personnel of the Balkan Wars]]
[[Category:Chetniks of the Macedonian Struggle]]
[[Category:Serbian military personnel of World War I]]
[[Category:1878 births]]
[[Category:1920 deaths]]
[[Category:20th-century Serbian people]]
[[Category:Royal Serbian Army soldiers]]
[[Category:Serbs of the Republic of Macedonia]]