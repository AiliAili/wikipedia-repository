{{about||the bluegrass musician|Vern Williams}}
{{Infobox person
|name=Vern Williams
|birth_date        = {{birth date and age|1949|6|9}}
|birth_place=[[Washington, D.C.]]
|image=Vern S. Williams.JPG
|occupation=Math teacher
|caption = Vern Williams at Longfellow Middle School in 2014}}
'''Vern Williams''' is a mathematics teacher at [[BASIS Schools|BASIS Independent McLean]] in [[McLean, Virginia]]. In 2007, Williams was one of 17 experts appointed by the [[George W. Bush Administration|George W. Bush administration]] to the National Mathematics Advisory Panel, and was the only member of the panel that was a practicing K-12 teacher. He decries the use of "fuzzy math" and believes that math textbooks have been "dumbed down" for students who dislike math.<ref name="washpost">Jay Mathews, Washington Post. "Is America's best high school soft on math?" <http://www.washingtonpost.com/wp-dyn/content/article/2011/02/27/AR2011022704827.html></ref>
<ref>{{Cite web|url=http://mclean.basisindependent.com/faculty-and-staff/teacher-bio.php?teacherID=11|title=Teaching Faculty – Vern Williams|last=|first=|date=|website=mclean.basisindependent.com|publisher=BASIS Independent McLean|access-date=2016-07-20}}</ref>
==History==
Williams was born on June 9, 1949, in [[Washington, D.C.]] He attended [[Paul Public Charter School|Paul Junior High School]], [[Coolidge Senior High School (Washington, DC)|Coolidge High School]], and graduated from the [[University of Maryland]] in 1972 with a BS in math education.<ref name="nmap bio">National Mathematics Advisory Panel. "Biography of Mr. Vern Williams" <{{cite web|url=http://www2.ed.gov/about/bdscomm/list/mathpanel/bios/williams.html |title=Archived copy |accessdate=October 25, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20141129052110/http://www2.ed.gov/about/bdscomm/list/mathpanel/bios/williams.html |archivedate=November 29, 2014 }}></ref><ref name="pacific standard">Noah Davis, Pacific Standard. "What Makes You So Smart, Middle School Math Teacher?"<http://www.psmag.com/navigation/books-and-culture/makes-smart-middle-school-math-teacher-92896></ref> Williams first taught at [[Hayfield Secondary School]] in Alexandria, Virginia. He then taught for 7 years at [[Glasgow Middle School]]. Williams later transferred to [[Henry Wadsworth Longfellow Middle School|Longfellow Middle School]], and then to BASIS Independent McLean in 2016, where he teaches currently.<ref name="pacific standard" />
==Views on math==
Williams is a strong believer in the eradication of "[[Math Wars|fuzzy math]]" and is a self-proclaimed teacher of "real math."<ref name="pacific standard" /> He is strongly against math fads, which he believe "have taken the math out of mathematics and replaced it with calculators, watered down content and picture books."<ref name="math reasoning">Vern Williams, Math Reasoning. <http://www.mathreasoning.com></ref> Williams also believes that teachers need free rein to create curriculums in order to teach successfully, and that "politicians, researchers, special interest groups, school system bureaucracies, [and] unions" shouldn't attempt to control what a teacher is allowed to teach.<ref name="nytimes">Vern Williams, New York Times. "Let Us Teach!" <http://www.nytimes.com/roomfordebate/2011/03/27/how-to-raise-the-status-of-teachers/let-us-teach></ref>
However, Williams receives some criticism for his more traditional views, and has been called an "anti-progressive" and a "radical" by some opponents.<ref name="criticism">Michael Paul Goldenberg, Rational Math Education (blog). "See How They Run, Like Pigs From A Gun...: Vern Williams and the NMP Report" <http://rationalmathed.blogspot.com/2009/05/see-how-they-run-like-pigs-from-gun.html></ref>
==Achievements/Recognition==
* Williams was one of 17 experts in the country appointed to the National Mathematics Advisory Panel by the Bush administration.<ref name="nmap bio" />
* He has been called by the [[Washington Post]] "one of the best math teachers in the country."<ref name="washpost" />
* Williams has received 2 national awards from the [[Mathematics Association of America]] for distinguished teaching.<ref name="nmap bio" />
* Williams coached the Longfellow [[MathCounts]] team for 14 years. Under his lead, the team earned awards at regional, state, and national levels of competition.<ref name="math reasoning" />
==References==
{{reflist}}


{{DEFAULTSORT:Williams, Vern}}
[[Category:1949 births]]
[[Category:Living people]]