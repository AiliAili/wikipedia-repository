{{expand language|topic=|langcode=es|otherarticle=|date=July 2015}}
{{expand language|topic=|langcode=pt|otherarticle=|date=July 2015}}
{{Multiple issues|
{{advert|date=August 2010}}
{{external links|date=July 2015}}
{{no footnotes|date=July 2015}}
}}
The '''Latin American Bibliography''' refers to the set of [[databases]] and information services on [[academic journals]] from [[Latin America]] and the [[Caribbean]] created by the [[National Autonomous University of Mexico]] (UNAM) in the decade of the seventies.{{Clarify|date=August 2009}}

Nowadays, the Latin-American Bibliography is composed by the following databases: CLASE (''Latin-American Citations in [[Social Sciences]] and [[Humanities]]''); PERIODICA (''Index of Latin-American Journals in [[Science]]''); [[Latindex]] (''Regional Co-operative Information System for Scholarly Journals from [[Latin America]], the Caribbean, Spain and Portugal'').

These databases were created by a group of information professionals, who identified the need to register, preserve and give access to the Latin-American knowledge published in the main academic [[Academic journal|journals]] of the region. Within UNAM, the fostering institution of these information products was the Science and Humanities Information Center (CICH) created in 1971.

For the size of its collection of Latin-American journals, for the quantity of compiled records and for the duration and consistency of the project, the Latin-American Bibliography produced in the UNAM constitutes one of the most valuable resources for scholars and experts specializing in Latin-American affairs.{{Citation needed|date=August 2009}}

==Products==

Three databases are available through the web site of UNAM’s General Directorate for Libraries [http://dgb.unam.mx General Directorate for Libraries]:

'''CLASE''' (''Latin-American Citations in Social Sciences and Humanities''). Bibliographical database, with more than 280,000 records, of which nearly 14,000 provide abstracts and links to the full text of the documents. It includes more than 1,400 journals specializing in Social Sciences, Humanities and Arts, from more than 20 countries of Latin America and the Caribbean. Documents not available in full text can be retrieved through the Document Supply Service of the Latin-American Serials Collection (Hemeroteca Latinoamericana) of the DGB. Direct link: [http://dgb.unam.mx/clase.html CLASE website]

'''PERIODICA''' (Index of Latin-American Journals in Science). Bibliographical database with more than 315,000 records, of which near 60,000 provide abstracts and links to the full text of the documents. The database indexes more than 1,500 journals specializing in Science and Technology, from more than 20 countries of Latin America and the Caribbean. Documents not available in full text can be retrieved through the Document Supply Service of the Latin-American Serials Collection (Hemeroteca Latinoamericana) of the DGB. Direct link: [http://dgb.unam.mx/periodica.html PERIODICA website]

'''[[Latindex]]''' (Regional Co-operative Information System for Scholarly Journals from Latin America, the Caribbean, Spain and Portugal). This initiative provides relevant information and data of the scholarly journals edited in the [[Iberoamerica]]n region. Three databases are produced through the collaborative work of the member institutions: '''Directory''',: with more than 17,000 records; '''Catalogue''', with more than 3,500 selected journals that fulfill international quality criteria and an '''Index of Electronic Journals''', offering nearly 3,000 links to available resources in full text. Direct link: [http://www.latindex.org Latindex website]

Currently, the Department of Latin-American Bibliography contributes to the production of two other Latin-American information products:

'''ASFA''' (''Aquatic Sciences and Fisheries Abstracts''). Bibliographical international database on Aquatic Sciences and [[Fisheries]], covering subject areas such as [[technology]] and [[Public administration|administration]] of the marine environments and its resources (salt and sweet waters), including its socioeconomic and juridical aspects. It offers abstracts of articles published in approximately 7,000 periodic publications, besides thesis, monographs and other not conventional literature. The contribution relative to the Mexican journals is produced in the Department of Latin-American Bibliography from 1981. Link: [http://www.fao.org/fishery/asfa ASFA website]

'''[[SciELO]] Mexico''' (''Scientific Electronic Library Online''). Open access electronic journals collection that includes a selection of the most recognized academic publications of the country in all areas of knowledge, previously selected accordingly to the most accepted criteria related to content and editorial standards. Currently it offers the full text of more than 2,500 articles from 28 academic Mexican journals. Direct link: [http://www.scielo.org.mx/scielo.php Scielo México website]

Over the time, other databases were produced by the Department of Latin-American Bibliography during its more than 30 years of existence, namely:

'''BLAT''' (''Latin-American Bibliography I and II''), with information compiled from international sources, mainly documents from Latin-American origin (produced by Latin American authors and institutions) or those in which their object of study was related to the region. The database ceased in 1997. Another one was '''MEXINV''', as a subset of CLASE, offered bibliographical records of documents relative only to [[Mexico]]. This database ceased in the decade of the nineties.

===Institution===

Currently, the databases described above are produced by the Department of Latin-American Bibliography, part of the Assistant Office for Information Services of the General Directorate for Libraries (DGB) of the National Autonomous University of Mexico (UNAM). The original databases (BLAT, CLASE, PERIODICA, MEXINV and Latindex) were created by the Science and Humanities Information Center (CICH). Since the incorporation of the CICH to UNAM’s General Directorate for Libraries in 1997, this institution acts as Responsible Editor.

==References==

*Alonso Gamboa, José Octavio. Servicios, productos, docencia e investigación en información: la experiencia del Centro de Información Científica y Humanística de la Universidad Nacional Autónoma de México. Ciencias de la Información, vol. 24, no. 4, diciembre, 1993. p.&nbsp;201-208. URL: [http://www.bibliociencias.cu/gsdl/cgi-bin/library?e=d-000-00---0revistas--00-0-0--0prompt-10---4------0-1l--1-es-50---20-about---00031-001-1-0utfZz-8-00&cl=CL2.772&d=HASH01caacf727585263378aa110&x=1]
*Alonso Gamboa, José Octavio. Accesso a revistas latinoamericanas en Internet. Una opción a través de las bases de datos Clase y Periódica. Ciencia da Informação, vol. 27, no. 1, Janeiro-abril, 1998, p.&nbsp;90-95. URL: http://www.scielo.br/pdf/ci/v27n1/12.pdf
*Alonso Gamboa, José Octavio y Felipe Rafael Reyna Espinosa. Compilación de datos bibliométricos regionales usando las bases de datos CLASE y PERIÓDICA. Revista Interamericana de Bibliotecología, 2005. Vol. 28, no. 1, enero-junio: 63-78. URL: http://bibliotecologia.udea.edu.co/revinbi/Numeros/2801/doc3_28.html
*Russell, Jane M.; Madera-Jaramillo, María J.; Hernández- García, Yoscelina y Ainsworth, Shirley. Mexican collaboration networks in the international and regional arenas. En: Kretschmer, H. & Havemann, F. (Eds.): Proceedings of WIS 2008, Berlin. Fourth International Conference on Webometrics, Informetrics and Scientometrics & Ninth COLLNET Meeting, Humboldt-Universität zu Berlin, Institute for Library and Information Science (IBI). URL: http://www.collnet.de/Berlin-2008/RussellWIS2008mcn.pdf

[[Category:Bibliographic databases and indexes]]