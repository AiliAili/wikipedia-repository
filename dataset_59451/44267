{{Infobox airport 
| name = RAF Bramcote
| nativename = [[File:Ensign of the Royal Air Force.svg|90px]]
| nativename-a = 
| nativename-r = 
| image = 
| image-width = 
| caption = 
| IATA = 
| ICAO = 
| type = Military   
| owner = 
| operator = [[Royal Air Force]] (RAF)
| city-served = 
| location = [[Bramcote, Warwickshire|Bramcote]]
| built = 1939
| used = 1940-1946 RAF use <br> 1946-1959 [[Royal Navy|RN]] use
| elevation-f = 354
| elevation-m = 113
| coordinates = {{coord|52|29|23|N|001|23|57|W|region:GB_type:airport|display=inline,title}}
| pushpin_map = Warwickshire
| pushpin_label = RAF Bramcote
| pushpin_map_caption = Location in Warwickshire
| website = 
| r1-number = 00/00
| r1-length-f = 0 
| r1-length-m = 0
| r1-surface = [[Concrete]]
| r2-number = 00/00
| r2-length-f = 0 
| r2-length-m = 0
| r2-surface = Concrete  
| r3-number = 00/00
| r3-length-f = 0 
| r3-length-m = 0
| r3-surface = Concrete 
| stat-year = 
| stat1-header = 
| stat1-data = 
| stat2-header = 
| stat2-data = 
}}
'''RAF Bramcote''' is a [[Royal Air Force]] station located {{Convert|4|mi}} south-east of [[Nuneaton]], [[Warwickshire]], England during the [[Second World War]]. It later became '''HMS Gamecock''' and then [[Gamecock Barracks]].
<ref name="CT">{{cite web|url=http://www.controltowers.co.uk/B/Bramcote.htm |title=RAF Bramcote - RN HMS Gamecock - airfield |publisher=Control Towers|accessdate=2 May 2012}}</ref>

==Royal Air Force operations==
The airfield was built by [[John Laing Group|John Laing & Son]] in the late 1930s.<ref>Ritchie, p. 91</ref> The first unit to use the airfield was [[No. 215 Squadron RAF]] who joined on 10 September 1939 with the [[Vickers Wellington]] and the [[Avro Anson]] before leaving on 8 April 1940.<ref name="Halley1988p218">{{Harvnb|Halley|1988|p=218.}}</ref>

The next unit to use the station was [[No. 18 (Polish) Operational Training Unit]] (OTU) flying the [[Vickers Wellington]] which arrived from [[RAF Hucknall]] during June 1940. The unit used [[RAF Bitteswell]] and [[RAF Nuneaton]] as satellites between February 1942 and February 1943. However soon after this the OTU moved to [[RAF Finningley]] during March 1943.<ref name="CT"/>

During the [[Battle of Britain]] [[No. 300 Polish Bomber Squadron]] was formed at the airfield on 1 July 1940 with the [[Fairey Battle]] I before moving to [[RAF Swinderby]] on 22 August 1940 accompanied by [[No. 301 Polish Bomber Squadron]] which formed 21 days later and left for Swinderby 6 days later on the 28th.<ref name="Jefford1988p84">{{Harvnb|Jefford|1988|p=84.}}</ref>

These squadrons were replaced by [[No. 304 Polish Bomber Squadron]] and [[No. 305 Polish Bomber Squadron]] which formed at the airfield during August 1940 flying Battle I's and switched to [[Vickers Wellington]] IC's during November 1940 before moving to [[RAF Syerston]] on 2 December 1940.<ref name="Halley1988p358">{{Harvnb|Halley|1988|p=358.}}</ref><ref name="Halley1988p359">{{Harvnb|Halley|1988|p=359.}}</ref>

[[No. 151 Squadron RAF]] moved in on 28 November 1940 with the [[Hawker Hurricane]] with a detachment going to [[RAF Wittering]]. On 22 December 1940 the unit moved to Wittering to equip with the [[Boulton Paul Defiant]] I.<ref name="Jefford1988p62">{{Harvnb|Jefford|1988|p=62.}}</ref>

Sometime in 1941 [[No.1513 BAT Flight]] arrived using [[Airspeed Oxford]]s but after five years the unit moved out.<ref name="CT"/>

During April 1943 [[No. 105 (Transport) Operational Training Unit]] formed at the airfield flying [[Vickers Wellington]]s these were supplmented with [[C-47 Skytrain|Douglas Dakota]]s in March 1945. Between November 1944 and July 1945 Bitteswell was used as a satellite providing some relief for the busy station before the unit was renamed 1381 (T) Conversion Unit in August 1945 and moved out to [[RAF Desborough]].<ref name="CT"/>

The gap was somewhat filled by 1510 BAT Flight using the Oxford who arrived during July 1946. However, after four months the flight moved out. With the airfield being transferred to the [[Royal Navy]] being renamed HMS Gamecock.<ref name="CT"/>

==Royal Navy operations==
[[File:Supermarine Seafire 47 VP487.474 1833 Sqn WVTN 16.05.53 edited-2.jpg|thumb|right|Supermarine Seafire F.47s of 1833 Squadron RNVR based at Bramcote in 1953.]]
'''RNAS Bramcote''' was given the ships name '''HMS Gamecock''' following RN normal practice and it was used by flying units of the [[Royal Naval Volunteer Reserve]] between August 1947 and October 1957. The first unit to be based was '''1833 Squadron''' equipped with [[Supermarine Seafire]] fighters. Initially the Seafire F15 and F.17 were used, but from June 1952, the unit became the only RNVR squadron to be equipped with the Seafire FR.47, fitted with contra-rotating propellers. These were replaced by the [[Hawker Sea Fury]] FB.11 in February 1954.  The jet-powered [[Supermarine Attacker]] was received in October 1955, and because these required better runway facilities, the squadron then moved to nearby [[RAF Honiley]].<ref name="Sturtivant1994p00">{{Harvnb|Sturtivant|1994|p=00.}}</ref>

The '''Midland Air Division''' was formed on 1 July 1953 to control Bramcote-based squadrons. '''1844 Squadron''' formed at Bramcote on 15 February 1954, being equipped with [[Fairey Firefly]] AS.6 anti-submarine aircraft. [[Grumman Avenger]] AS.5 aircraft replaced the Fireflies in March 1956. Both squadrons ceased to exist on 10 March 1957 when all of the United Kingdom's reserve flying units were disbanded as an economy measure.<ref name="Sturtivant1994p00"/>

In 1959 the airfield was transferred to the British Army as [[Gamecock Barracks]].<ref>{{cite web|url=http://homepage.ntlworld.com/g.carline1/juniorleaders.html |title=The Junior Leaders Regiment RA|publisher=G Carline|accessdate=6 May 2012}}</ref>

==Units and aircraft==
* [[No. 151 Squadron RAF]] (1940) [[Hawker Hurricane|Hawker Hurricane I]] then [[Boulton Paul Defiant]].<ref name="Jefford1988p62"/>
* [[No. 215 Squadron RAF]] (1939) [[Vickers Wellington|Vickers Wellington I]].<ref name="Halley1988p218"/>
* [[No. 300 Squadron RAF|No. 300 (Polish) Squadron RAF]] (1940) [[Fairey Battle]].<ref name="Jefford1988p84"/>
* [[No. 301 Squadron RAF|No. 301 (Polish) Squadron RAF]] (1940) [[Fairey Battle]].<ref name="Jefford1988p84"/>
* [[No. 304 Squadron RAF|No. 304 (Polish) Squadron RAF]] (1940) [[Fairey Battle]] then [[Vickers Wellington|Vickers Wellington IC]].<ref name="Halley1988p358"/>
* [[No. 305 Squadron RAF|No. 305 (Polish) Squadron RAF]] (1940) [[Fairey Battle]] then [[Vickers Wellington|Vickers Wellington IC]].<ref name="Halley1988p359"/>
* [[No. 18 Operational Training Unit RAF]] (1940–1943) [[Vickers Wellington]].<ref name="CT"/>
* [[No. 105 (Transport) Operational Training Unit RAF|No. 105 Operational Training Unit RAF]] (1943–1945) [[Vickers Wellington]] then [[Douglas C-47 Skytrain|Douglas Dakota]].<ref name="CT"/>
* [[No. 1513 (Beam Approach Training) Flight RAF]].<ref name="CT"/>
* [[1833 Naval Air Squadron]]
* [[1844 Naval Air Squadron]]
* No. 1 Air Traffic School<ref name="ABCT">{{cite web|url=http://www.abct.org.uk/airfields/airfield-finder/bramcote/ |title=Bramcote |publisher=[[Airfields of Britain Conservation Trust]]|accessdate=3 June 2016}}</ref>
* No. 1 School of Air Movement.<ref name="ABCT"/>
* No. 6 Anti-Aircraft Co-operation Unit.<ref name="ABCT"/>
* No. 17 Air Crew Holding Unit.<ref name="ABCT"/>
* [[No. 42 Gliding School RAF]].<ref name="ABCT"/>
* [[No. 1381 (Transport) Conversion Unit RAF]].<ref name="ABCT"/>
* [[No. 1510 BABS Flight RAF]].<ref name="ABCT"/>
* [[No. 1513 (Radio Aids Training) Flight RAF]].<ref name="ABCT"/>
* [[No. 2735 Squadron RAF Regiment]].<ref name="ABCT"/>
* Oxford Test Flight.<ref name="ABCT"/>
* Squadron & Flight Commanders School.<ref name="ABCT"/>
* Transport Command Air Crew Examining Unit.<ref name="ABCT"/>
* Transport Command Examining Unit.<ref name="ABCT"/>
* Transport Command Initial Conversion Unit.<ref name="ABCT"/>

==See also==
* [[List of former Royal Air Force stations]]

==References==

===Citations===
{{reflist|2}}

===Bibliography===
*{{wikicite|ref={{harvid|Jefford|1988}}|reference=Jefford, C.G, [[Order of the British Empire|MBE]],BA,RAF (Retd). ''RAF Squadrons, a Comprehensive Record of the Movement and Equipment of all RAF Squadrons and their Antecedents since 1912''. Shrewsbury, Shropshire, UK: Airlife Publishing, 1988. ISBN 1-84037-141-2.}}
*{{wikicite|ref={{harvid|Sturtivant|1994}}|reference=Sturtivant, R. ''The Squadrons of the Fleet Air Arm''. Air-Britain (Historians) Ltd, 2001. ISBN 0-85130-223-8.}}
*{{wikicite|ref={{harvid|Halley|1988}}|reference=Halley, J.J. ''The Squadrons of the Royal Air Force & Commonwealth, 1981-1988''. Tonbridge, Kent, UK: Air-Britain (Historians) Ltd., 1988. ISBN 0-85130-164-9.}}
* {{cite book|first=Berry|last=Ritchie|title=The Good Builder: The John Laing Story|publisher=James & James|year=1997|isbn=}}

==External links==
*[http://www.controltowers.co.uk/B/Bramcote.htm Control Towers - RAF Bramcote]
{{Royal Air Force}}

{{DEFAULTSORT:Bramcote}}
[[Category:Military history of Poland during World War II]]
[[Category:Royal Air Force stations in Warwickshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]