{{Orphan|date=May 2014}}
{{Infobox automobile
| name = DHMakerBus
| image = 
| caption = 
| type = Mobile makerspace and technology education classroom
| manufacturer = Beth Compton, Kim Martin, and Ryan Hunt
| aka = The MakerBus
| production = 
| model_years = 2013
| assembly = 
| designer = 
| class = 
| body_style = 
| layout = 
| platform = 
| related = 
| engine = 
| transmission = 
| propulsion = 
| wheelbase = <!--{{convert|NNNN|mm|in|1|abbr=on}}-->
| length = <!--{{convert|NNNN|mm|in|1|abbr=on}}-->
| width = <!--{{convert|NNNN|mm|in|1|abbr=on}}-->
| height = <!--{{convert|NNNN|mm|in|1|abbr=on}}-->
| weight = <!--{{convert|NNNN|-|NNNN|kg|lb|0|abbr=on}}-->
| predecessor = 
| successor = 
| sp = uk
}}

'''The MakerBus''', also known as the '''DHMakerBus''', is a mobile [[Makerspaces|makerspace]] and technology education classroom in [[London, Ontario]]. The MakerBus claims to be the first of its kind in [[Canada]].<ref>{{cite web|last=Hunt|first=Ryan|title=The MakerBus|url=http://www.cbc.ca/ontariomorning/episodes/2014/03/17/the-makerbus/|work=Ontario Morning|publisher=CBC|accessdate=2 May 2014}}</ref>

== History ==

The MakerBus was founded in 2013 by Beth Compton, Kim Martin, and Ryan Hunt.<ref>{{cite web|last=Gilbert|first=Craig|title=Want a new way to teach tech in London? Get on the bus|url=http://www.ourwindsor.ca/news-story/2535742-want-a-new-way-to-teach-tech-in-london-get-on-the-bus/|work=London Community News|publisher=London Community News|accessdate=2 May 2014}}</ref> The bus was created for two purposes. First, it was intended to be used to take attendees of the Digital Humanities 2013 Conference from Southern Ontario and Michigan to the conference in Lincoln, Nebraska.<ref>{{cite web|last=Gilbert|first=Craig|title=London students make big splash with ‘The Bus’|url=http://www.londoncommunitynews.com/news-story/4021292-london-students-make-big-splash-with-the-bus-/|work=London Community News|publisher=London Community News|accessdate=2 May 2014}}</ref> Second, the MakerBus was created to help provide greater access to technology, making,{{clarify|date=May 2014}} and education to the London/Middlesex region.<ref>{{cite web|last=Mullins|first=Angela|title=Campaign for roving digital classroom gaining traction|url=http://metronews.ca/news/london/692650/campaign-for-roving-digital-classroom-gaining-traction/|work=Metro News|publisher=Metro News|accessdate=2 May 2014}}</ref>
To raise money for the project, then called the "DHMakerBus," a [[Indiegogo]] crowd-funding campaign was launched on April 8, 2013 with a funding goal of $10,000.<ref>{{cite web|last=Hunt|first=Ryan|title=DHMakerBus|url=https://www.indiegogo.com/projects/dhmakerbus|publisher=Indiegogo|accessdate=5 May 2014}}</ref> At the end of the two-month funding window, the DHMakerBus received $2,856 in community funding. With this funding the MakerBus team purchased a used school bus.<ref>{{cite web|last=Hunt|first=Ryan|title=MakerBus Recap – What we’ve done, where we are, and where we’re going|url=http://dhmakerbus.com/2014/01/30/makerbus-recap-what-weve-done-where-we-are-and-where-were-going/|accessdate=5 May 2014}}</ref>

== Activities ==

The MakerBus team seeks to promote access to technology and education in the London/Middlesex region. In the first year of its operation the MakerBus has worked with community partners to create pop-up educational events that promote non-traditional uses of technology.<ref>{{cite web|last=Gilbert|first=Craig|title=Stretching out slime, playing Tetris with a carrot – and learning?|url=http://www.londoncommunitynews.com/news-story/4129463-stretching-out-slime-playing-tetris-with-a-carrot-and-learning-/|accessdate=5 May 2014}}</ref>
The MakerBus has worked with a number of educational partners in London, like Literacy Link South Central, the London Public Libraries, and the Western Fair.<ref>{{cite web|title=Masonville|url=http://www.londonpubliclibrary.ca/category/location/all-locations/masonville?page=1}}</ref>

== Projects ==

To date the largest project undertaken by the MakerBus is the MAKE London Wearable Technology and Made Clothing Fashion Show. This event was held in partnership with [[Museum London]] and saw homemade clothing designers from around Southern Ontario come together to create a fashion show that celebrated the fashion and technology.<ref>{{cite web|title=Celebrating Maker Culture at Museum London|url=http://www.forestcityfashionista.com/2014/03/celebrating-maker-culture-at-museum.html}}</ref><ref>{{cite web|last=Belanger|first=Joe|title=Wearable Tech Clothing and Fashion Show, as Museum London’s Third Thursday program.|url=http://www.lfpress.com/2014/03/17/wearable-tech-clothing-and-fashion-show-as-museum-londons-third-thursday-program}}</ref>
Other projects include an ongoing partnership with Literacy Link South Central (a part of the Ontario Literacy Network) to create classes that focus on improving adult digital literacy skills. Past examples of these classes include using [[Quad copter|quadcopters]] as a teaching tool for math skills and using the [[Microprocessor|MakeyMakey]] to build confidence in adult learners.<ref>{{cite web|title=What is the Maker Movement?|url=http://london.ctvnews.ca/video?clipId=314169}}</ref>
The MakerBus team has held community events in London to raise awareness of the [[maker movement]] like the MAKE London Day of Making that brought together local makers and members of the general public.<ref>{{cite web|last=Meyer|first=Sean|title=A Day of Making in London|url=http://www.londoncommunitynews.com/news-story/4234295-a-day-of-making-in-london/}}</ref>

== Partners ==

The MakerBus works with a variety of partners in London, ON. Partners include:

Literacy Link South Central
London Public Libraries
Thames Valley District School Board
Bread and Roses Books
Ontario Library Association

== References ==
{{reflist}}

[[Category:Hackerspaces]]
[[Category:Educational facilities]]