{{Infobox writer <!-- For more information see [[:Template:Infobox Writer/doc]]. -->
| name          = Khachatur Abovian
| image         = Abovianportrait.jpg
| image_size    =
| alt           =
| caption       = Portrait of Khachatur Abovian, by [[Ludwig von Maydell]] (1831)
| birth_date    = {{OldStyleDate|October 15|1809|October 3}}
| birth_place   = [[Kanaker]], [[Erivan Khanate]], [[Qajar dynasty|Persian Empire]]<br />(modern-day [[Yerevan]], Armenia)
| death_date    = {{Death date and age|1848|04|14|1809|10|15}} (disappeared)
| death_place   =
| resting_place =
| occupation    = novelist, playwright, teacher, poet
| language      =
| nationality   = 
| ethnicity     = [[Armenians|Armenian]]
| religion      = [[Armenian Apostolic Church]]
| citizenship   =
| education     =
| alma_mater    =
| period        = [[Romanticism]]
| genre         =
| subject       =
| movement      =
| notableworks  =
| spouse        = Emilia Looze (German-Swedish)
| partner       =
| children      = 2 children
| relatives     =
| awards        =
| signature     =
| signature_alt =
| module        =
| website       = <!-- www.example.com -->
| portaldisp    =
}}

'''Khachatur Abovian''' (or '''Abovyan''';<ref>{{cite web|title=The history of the foundation of Khachatur Abovyan's house-museum|url=http://www.abovyanmuseum.am/en/home-museum.html|website=Khachatur Abovyan's house-museum|accessdate=25 August 2015}}</ref> {{lang-hy|Խաչատուր Աբովյան}}; {{OldStyleDate|October 15|1809|October 3}}{{spaced ndash}}{{OldStyleDate|April 14|1848|April 2}} (disappeared)) was an [[Armenians|Armenian]] writer and national public figure of the early 19th century who mysteriously vanished in 1848 and eventually presumed dead. He was an educator, poet and an advocate of modernization.<ref name="Pan 143">Panossian, p. 143.</ref> Reputed as the father of [[Armenian literature|modern Armenian literature]], he is best remembered for his novel ''[[Wounds of Armenia]]''.<ref name="Ruth">{{Cite web|last=Bedevian|first=Ruth|title=Writer and Patriot: Khachatur Abovyan (sic)|date=December 8, 2004|url=http://www.groong.org/orig/rb-20041208.html|accessdate=July 13, 2008}}</ref> Written in 1841 and published posthumously in 1858, it was the first novel published in the modern [[Armenian language]] using [[Eastern Armenian]] based on the [[Yerevan dialect]] instead of [[Classical Armenian]].<ref name="Pan 143"/>

Abovian was far ahead of his time and virtually none of his works was published during his lifetime. Only after the establishment of the [[Armenian SSR]] was Abovian accorded the recognition and stature he merited.<ref>Hacikyan et al., p. 214.</ref> Abovian is regarded as one of the foremost figures not just in Armenian literature but [[History of Armenia|Armenian history]] at large.<ref name="Hewsen">[[Robert H. Hewsen|Hewsen, Robert H.]] "The Meliks of Eastern Armenia: IV: The Siwnid Origins of Xac'atur Abovean." ''[[Revue des études Arméniennes]]''. NS: '''XIV''', 1980, pp. 459–468.</ref> Abovian's influence on [[Western Armenian]] literature was not as strong as it was on Eastern Armenian, particularly in its formative years.<ref>Bardakjian, p. 135.</ref>

==Early life and career==
[[File:Bashinjagyan5.jpg|thumb|left|Painting in 1884 by [[Gevorg Bashinjaghian]] of the house where Abovian was born ]]
Abovian was born in 1809 in the village of [[Qanaqer-Zeytun|Kanaker]], then part of the [[Qajar dynasty|Qajar Persian Empire]],<ref>Nalbandian, p. 61.</ref> and now a district of [[Yerevan]], [[Armenia]].<ref name="Hac 211">Hacikyan et al., p. 211.</ref> Abovian's family were descendants of the Beglaryan ''[[melik]]'' family in [[Gülüstan, Goranboy|Gulistan]], one of five Armenian families who ruled around the current day region of [[Nagorno-Karabakh]]. The Abovian family held the position of ''[[tanuter]]'' (a hereditary lordship) in Kanaker; Abovian's uncle was the last ''tanuter'' of Kanaker. His aunt was the wife of Sahak Aghamalian, the last melik of Yerevan at the time of the Russian annexation in 1828.<ref>Haxthausen, pp. 153-155.</ref>  His social origins and descent imbued him at an early age with a sense of responsibility to his people.<ref name="Hewsen"/> He was born six years after his parents, Avetik and Takuhi, married. He had a brother, Garabed, who died at the age of three.<ref name="Ruth"/>

At age 10, Abovian was taken by his father to [[Echmiadzin]] to study for the priesthood. He dropped out after five years and moved to [[Tiflis]] in 1822 to study [[Armenian studies]] and languages at the [[Nersisyan School]] under the guidance of Harutiun Alamdarian. Abovian graduated in 1826 and began preparing to move to [[Venice]] to further his education. However, the outbreak of the [[Russo-Persian War (1826-1828)|Russo-Persian War]] (1826–28) curtailed his plans. For the following three years he taught briefly at [[Sanahin]] and then worked for [[Catholicos of Armenia|Catholicos]] [[Yeprem of Armenia]] as his clerk and translator.<ref name="Hac 211"/> While working for the Catholicos, the twenty-year-old Abovian met many notable foreigners, including the diplomat and playwright [[Alexandr Griboyedov]], who was stuck in [[Echmiadzin]] en route to [[Tabriz]] in September 1828.<ref>Abov, p. 28.</ref> Griboyedov's weekly ''Tifliskiye Vedemosti'' became the first paper to publish an article on Abovian.<ref name="Khach 29">Khachaturian, p. 29.</ref>

==Conquest of Ararat==
The turning point in Abovian's life was the arrival of [[Friedrich Parrot]] in Armenia in September 1829, a professor of [[Naturalism (philosophy)|natural philosophy]] from the [[University of Tartu|University of Dorpat]] in [[Governorate of Livonia|Livonia]] (in present-day [[Tartu|Tartu, Estonia]]). Parrot traveled to Armenia to climb [[Mount Ararat]] to conduct [[geology|geological studies]] and required a local guide and a translator for the expedition. The Catholicos assigned Abovian to these tasks.<ref name="Hac 211"/> With Abovian's assistance, Parrot became the first explorer in modern times to reach the summit of Mount Ararat. The project received full approval from the emperor [[Nicholas I of Russia|Nicholas I]], who provided the expedition with a military escort.<ref>Parrot, p. x.</ref>

[[File:Friedrich Parrot.jpg|thumb|Abovian's mentor Friedrich Parrot]]
Abovian and Parrot crossed the [[Arax River]] into the district of [[Surmalinsky Uyezd|Surmali]] and headed to the Armenian village of [[Yenidoğan, Aralık|Akhuri]] situated on the northern slope of Ararat {{convert|4000|ft|m}} above sea level. Following the advice of Harutiun Alamdarian of Tiflis, they set up base camp at the [[Saint Hakob of Akori monastery|Monastery of St. Hakob]] some {{convert|2400|ft|m}} higher, at an elevation of {{convert|6375|ft|m}}.<ref>Parrot, p. 103.</ref> Abovian was one of the last travelers to visit Akhuri and the monastery before a disastrous earthquake completely buried both in May 1840.<ref name="Ketchian"/> Their first attempt to climb the mountain, using the northeast slope, failed as a result of lack of warm clothing.

Six days later, on the advice of Stepan Khojiants, the village chief of Akhuri, the ascent was attempted from the northwest side. After reaching an elevation of {{convert|16028|ft|m}}, they turned back because they did not reach the summit before sundown. They reached the summit on their third attempt at 3:15 p.m. on October 9, 1829.<ref>Parrot, p. 139.</ref> Abovian dug a hole in the ice and erected a wooden cross facing north.<ref>Parrot, pp. 141-142.</ref> Abovian picked up a chunk of ice from the summit and carried it down with him in a bottle, considering the water holy. On November 8, Parrot and Abovian climbed up Lesser Ararat. Years later, in 1845, the German mineralogist [[Otto Wilhelm Hermann von Abich]] climbed Ararat with Abovian. Abovian's third and last ascent of Ararat was with the Englishman Henry Danby Seymour in 1846.<ref name="Ketchian"/>

==The Dorpat years==
[[File:Album von Dorpat, TKM 0031H 05, crop.jpg|thumb|[[University of Tartu|University of Dorpat]] in the mid-19th century]]
Impressed with Abovian's thirst for knowledge, Parrot arranged for a Russian state scholarship for Abovian to study at the University of Dorpat in 1830.<ref>Bardakjian, p. 255.</ref> He entered the university directly without additional preparation and studied in the Philosophy faculty of the Philological-Historical department from September 3, 1830 until January 18, 1836.<ref>Khachaturian, p. 52.</ref> The years in Dorpat were very fruitful for Abovian who studied social and natural sciences, European literature and philosophy, and mastered German, Russian, French and [[Latin]].<ref name="Hac 211"/> At this time Abovian fell under the influence of [[German Romanticism]].<ref>Panossian, p. 144.</ref> In addition, Abovian established numerous contacts with European intellectuals of the time. At the university he became friends with the sons of [[Nikolay Karamzin]] who studied with him.<ref>Abov, p. 48.</ref> In 1834 Abovian visited his cousin Maria (daughter of melik Sahak Aghamalian) in [[St. Petersburg]], then married to the Georgian [[Alexander, son of Erekle II of Georgia|Prince Alexander]]. Prior to graduation, Abovian learned his mother Takuhi had died.<ref name="Hewsen"/>

==Return to Armenia==
In 1836 he returned home anxious to embark on a mission of enlightenment.<ref name="Hac 212">Hacikyan et al., p. 212.</ref> Abovian’s efforts were thwarted as he faced a growing and hostile reaction from the [[Armenian Apostolic Church|Armenian clergy]] as well as [[Tsarist]] officials, largely stemming from his opposition to [[dogmatism]] and formalism in the school system. Abovian was appointed as the supervisor of the [[Tiflis]] [[uyezd]] school and married a German woman named Emilia Looze (d. 1870) in 1839.<ref name="Hewsen"/><ref name="Hac 212"/> In 1840 he was approached by English traveler [[Anne Lister]], who was visiting Tiflis. She hoped that Abovian would guide her on another expedition to Mount Ararat which ultimately did not occur.<ref>{{cite journal|last=Lang|first=David|title=Notes and Communications|journal=Bulletin of the School of Oriental and African Studies|volume=53|issue=1|page=117|doi=10.1017/S0041977X00021303|publisher=Cambridge University Press|location=University of London|year=1990|jstor=618973}}</ref> He was dismissed from the school in 1843 and was transferred to the uyezd {{Clarify|date=September 2013}} school in Yerevan where he encountered apathy and antagonism from his colleagues and the clergy.<ref name="Hac 212"/>

In the summer of the same year, Abovian was visited by two German travellers. A [[Bavaria]]n professor, [[Moritz Wagner]], from the [[University of Munich]], arrived in May and toured the [[Lake Sevan]] region with Abovian and thereafter corresponded with him on a regular basis.<ref name="Guest">Guest, p. 188.</ref> In July Abovian also accompanied Wagner on the first recorded ascent of [[Mount Aragats]] in Armenia.<ref name="Ketchian">{{Cite journal|last=Ketchian |first=Philip K. |title=Climbing Ararat: Then and Now |journal=[[The Armenian Weekly]] |volume=71 |issue=52 |date=December 24, 2005 |url=http://www.hairenik.com/armenianweekly/fea12240501.htm |deadurl=yes |archiveurl=https://web.archive.org/web/20090908015245/http://www.hairenik.com/armenianweekly/fea12240501.htm |archivedate=September 8, 2009 }}</ref>

In August, Abovian escorted the German Baron [[August von Haxthausen]] around the province.<ref>Haxthausen, pp. xvii-xx.</ref> They visited the Abovian family home in Kanaker and attended a service at the [[Blue Mosque, Yerevan|Blue Mosque]].<ref>Haxthausen, pp. 147-172 and pp. 187-191.</ref>  They also visited a [[Yazidi]] encampment where they met the chief Timur Aga and exchanged pleasantries with a rider from [[Ivan Paskevich|Count Paskevich]]'s guard. He became a trusted friend of the Yazidi community in Armenia, and when the chief returned with lavish gifts from a banquet in Tiflis organized by the viceroy of the Caucasus [[Mikhail Semyonovich Vorontsov]] in 1844, he organized a tribal feast and Abovian was invited to attend.<ref name="Guest"/> In 1845 he applied for a position at the Catholicate of [[Echmiadzin]] but was not accepted. The following year, he became a contributor to Vorontsov's weekly newspaper, ''Kavkaz'', for which Abovian wrote three articles.<ref name="Khach 29"/>

==Disappearance==
On April 14, 1848, Abovian left his home for an early morning walk, and was never seen again; his disappearance remains unresolved.<ref name="Hac 212"/> His wife Emilia did not report him missing for a month.<ref name="Ruth"/> Their children, Vartan (1840–1896) and Zarmandukht (later known as Adelaide; 1843–1909), were ages eight and five, respectively, at the time of the disappearance.<ref name="Ruth"/><ref name="Hewsen"/>

Numerous theories have been proposed attempting to explain his disappearance: that he committed suicide, was murdered by his Persian or Turkish enemies, or arrested and exiled to [[Siberia]] by the [[Special Corps of Gendarmes]], among others.<ref name="Hewsen"/> Given his love for his children and their young age, it is generally disregarded that Abovian committed suicide.<ref name="Hewsen"/> Writer [[Axel Bakunts]] put forward the theory that Abovian was in Western Europe engulfed in the [[Revolutions of 1848]].<ref>{{hy icon}} O. Melkonyan, ''«Ուշագրավ վկայություն Խաչատուր Աբովյանի առեղծվածային անհայտացման մասին»'' (Remarkable testimony regarding the mysterious disappearance of Khachatur Abovyan), ''«Կրթություն»'' (Education), '''7''' (116), 2003</ref>

==Writings==
Abovian wrote novels, stories, descriptions, plays, scientific and artistic compositions, verses and fables. He was the first Armenian writer to compose literature for children.<ref name="GSE">{{cite encyclopedia|title=Абовян Хачатур (Abovian Khachatur)|encyclopedia="Большая Советская Энциклопедия" ([[Great Soviet Encyclopedia]]), 3rd edition|url=http://dic.academic.ru/dic.nsf/bse/60887/%D0%90%D0%B1%D0%BE%D0%B2%D1%8F%D0%BD|accessdate=2008-07-14|language=ru}}</ref>

===''Wounds of Armenia''===
The historical novel ''[[Wounds of Armenia]]'' (written in 1841, first published in 1858) was the first Armenian secular novel dedicated to the fate of the Armenian people and its struggle for liberation in the period of [[Russo-Persian War (1826–28)|Russo-Persian war of 1826–1828]]. The novel dealt with the suffering of Armenians under Persian occupation.<ref name="GSE"/> The basic concept of the novel was the assertion of feelings of national merit, patriotism and hatred of oppressors. These themes had a profound influence over wide layers of Armenian society. The hero, Agassi, personifies the freedom-loving national spirit and its will to fight against the foreign conquerors. "Give away your life, but never give away your native lands", is his motto.<ref name="GSE"/> The story begins with an abduction of an Armenian girl by a band of thugs sent by the Persian [[sardar]] that triggers an uprising led by Agassi.<ref>Hacikyan et al., p. 213.</ref>

Abovian saw in strengthening of the friendship of Russian and Armenian peoples a guarantee of the national, political and cultural revival of his native lands.<ref name="B137">Bardakjian, p. 137.</ref> However; when Abovian wrote the novel he was already disillusioned with Tsarist policies in Armenia, particularly with the implementation of ''Polozhenie'' (Statute) in 1836 which greatly reduced the political power of the Armenian Catholicos and the abolishment of the [[Armenian Oblast]] in 1840.<ref name="B137"/> In the novel, elements of [[romanticism]] and [[Literary realism|realism]] are interlaced while the narration is supplanted by [[Lyric poetry|lyrical]] retreats.<ref name="GSE"/>

===Other works===
Abovian's poetry was filled with satire best expressed in ''The wine jug'', in which he criticized Russian bureaucracy. ''Leisure entertainment'' was adapted by Abovian from notes he took in public gatherings. The work is a collection of fables in verse that chastise vice, injustice and moral degeneration.<ref>Bardakjian, p. 136.</ref> He wrote scientific and artistic non-fiction works such as the ''Discovery of America'' and ''Book of Stories''.<ref name="GSE"/> Abovian translated to the Armenian language the works of [[Homer]], [[Goethe]], [[Friedrich Schiller]], Nikolay Karamzin, [[Ivan Krylov|I. A. Krylov]] and others. He continued promoting secular and comprehensive (mental, moral, working, physical) training, school accessibility, free education for the indigent and equal education of boys and girls.<ref name="GSE"/> Pedagogical compositions of Abovian include the book for reading ''Introduction to education'' (1838), a textbook of Russian grammar and an Armenian-language novel ''History of Tigran, or a moral manual for the Armenian children'' (printed in 1941). He was the first Armenian to study scientific ethnography: the way of life and customs of the peasants of the native settlements around Kanaker, inhabitants of Yerevan, and gathered and studied Armenian and [[Kurd]]ish folklore.<ref name="GSE"/>

==Legacy==
[[File:Stamp of USSR 1867.jpg|thumb|left|Khachatur Abovian on a 1956 [[Soviet Union|Soviet]] stamp]]
[[File:Khachatur Abovian in Kanaker Yerevan.jpg|thumb|Abovian's statue at his native village of Kanaker, nowadays part of the capital [[Yerevan]]]]
Abovian's life is well remembered in Armenia. During the years in which Armenia was under Soviet rule, his pro-Russia stance was emphasized.<ref name="SAE">{{hy icon}} Hakobyan, P. ''«Աբովյան»'' (Abovyan). [[Soviet Armenian Encyclopedia]]. vol. i. Yerevan, Armenian SSR: [[Armenian Academy of Sciences]], 1974, pp. 32–35.</ref> Schools, streets, boulevards and parks were named after him.<ref name="SAE"/> The village of Elar, located {{convert|10|km|mi}} northeast of Yerevan, was named after him in 1961. Two years later, as the village's population grew larger, [[Abovyan]] was accorded with city status. His home in Kanaker was turned into a house-museum in 1939, and many of his original writings are preserved there.<ref>{{hy icon}} Anon. ''«Աբովյանի Տուն-թանգարան»'' (Abovyan's House-Museum). ''Soviet Armenian Encyclopedia''. vol. i. Yerevan, Armenian SSR: Armenian Academy of Sciences, 1974, p. 38.<!-- any ISBN number? --></ref>

Two prominent statues of Abovian stand in Yerevan. The concept of the first statue dates back to 1908 when a number of Armenian intellectuals in [[Russian Armenia]] decided to commemorate the 60th anniversary of Abovian's disappearance and raise funds for a statue. These included [[Alexander Shirvanzade]],
[[Hovhannes Tumanyan]] and [[Gevorg Bashinjagyan]].<ref>Khanjyan, p. 35.</ref> By 1910 they had collected enough funds to order the statue. It was designed by M. Grigoryan and sculpted by Andreas Ter-Manukyan in Paris between 1910–13. The statue is {{convert|4.5|m|ft}} high and made of bronze on a granite pedestal. As a result of a misunderstanding the statue was only delivered to Yerevan in 1925 and first erected on Abovian street by the cinema ''Moscow'' in 1933 and then moved to the children's park on the banks of the [[Hrazdan river]]. In 1964, it found its permanent home by the Abovian house-museum in Kanaker.<ref>Khanjyan, pp 36–37</ref> The second statue of Abovian in Yerevan was erected in Abovian square in 1950. The {{convert|9|m|ft|adj=on}} high bronze statue was designed by Gevork Tamanian (son of [[Alexander Tamanian]]) and sculpted by Suren Stepanyan.<ref>Khanjyan, p. 39.</ref>

The work Abovian accomplished in the field of education was remembered. Yerevan's State [[Pedagogic]] Institute was named after him. On February 28, 1964, a medal was named in his honour (Աբովյանի Անվան Մեդալ) and which was awarded to school teachers who showed exceptional abilities in teaching and education. Between 1948–84, five documentary films were produced in the Armenian SSR about the life and work of Abovian.<ref>{{cite web|title=Documentaries|publisher=Armenian Association of Film-Critics and Cinema- Journalists|url=http://www.arm-cinema.am/en/documentaries|accessdate=2008-07-18}}</ref>

===Portrait===
Abovian's portrait is one of the most exceptional exhibits of the [[Charents Museum of Literature and Arts|Museum of Literature and Arts after Charents]]. It is an oil painting with a size of {{convert|20.5|cm|in}} by {{convert|27.5|cm|in}}. In 1938 Abovian's grandsons brought it to the museum. When Abovian's son Vardan returned to the Caucasus, he found the painting in a badly deteriorated condition. But by Vardan's request Armenian painter [[Gevorg Bashinjagyan]] restored the portrait. He cut worn-out edges, glued it to a hard paper and then filled the cracks with corresponding colors. The painter of the portrait was Ludwig von Maydell, from [[University of Tartu|Dorpat University]]. He painted it in the fall of 1830, when Abovian was only 20 or 21 years old. This portrait is the only painting of Abovian made during his lifetime.<ref>[http://gatmuseum.am/literature.html Charents Museum of Literature and Arts]</ref><ref>[http://gatmuseum.am/images/abovyan.jpg Original portrait of Abovian]</ref>

==Selected bibliography==

===Prose===
{{col-begin}}
{{col-2}}
'''Novels'''
* ''[[Wounds of Armenia]], or lamentation of the patriot'' (Tiflis 1858)
* ''History of Tigran, or a moral manual for Armenian children'' (1941)
'''Non-fiction'''
* ''Introduction to education'' (Tiflis 1838)
* ''Collection of algebra exercises'' (1868)
* ''New theoretical and practical Russian grammar for Armenians'' (1839)

{{col-2}}

'''Other'''
* ''Unpublished works'' (Tiflis 1904)
* ''Unpublished letters'' (Vienna 1929)
'''Stories'''
* ''The Turkish girl'' (Yerevan 1941)
{{col-end}}

===Poetry===
{{col-begin}}
{{col-2}}
* ''The wine jug'' (Tiflis 1912)
* ''Folk songs'' (Yerevan 1939)
* ''Poems'' (Yerevan 1941)
* ''Poetry for children'' (Yerevan 1941)
{{col-2}}
'''Fables'''
* ''Leisure entertainment'' (Tiflis 1864, includes the play ''Feodora'')
* ''Fables'' (Yerevan 1941)
{{col-end}}

==Notes==
{{Reflist|30em}}

==References==
* {{cite book |first=G.A. |last =Abov |title =Khachatur Abovian: Life and work |publisher =[[Armenian Academy of Sciences]] |location=Yerevan | year =1948|language=ru}}
* {{cite book |first=Kevork B. | last =Bardakjian |title =A Reference Guide to Modern Armenian Literature, 1500–1920 |publisher = Wayne State University Press| year =2000| isbn=0-8143-2747-8}}
* {{cite book |first=John S. | last =Guest |title = The Yezidis: A Study in Survival | publisher = Routledge| year =1987| isbn=0-7103-0115-4}}
* {{cite book |first=Agop J. | last =Hacikyan |author2 =Gabriel Basmajian|author3=Edward S. Franchuk |title = The Heritage of Armenian Literature, Vol. 3: From The Eighteenth Century To Modern Times | publisher = Wayne State University Press| date =October 30, 2005| isbn=0-8143-3221-8 | page = 1069}}
* {{cite book
  |last =Haxthausen
  |first =Baron August von
  |translator=John Edward Taylor
  |others=Introduction by Pietro A. Shakarian. Foreword by [[Dominic Lieven]]
  |title =Transcaucasia and the Tribes of the Caucasus
  |publisher=[[Gomidas Institute]]
  |place=London
  |date=2016
  |orig-year=1854-55
  |isbn=978-1909382312}}
* {{cite book
  |last =Khachaturian
  |first =Lisa
  |title =Cultivating Nationhood in Imperial Russia: The Periodical Press and the Formation of a Modern Armenian Identity
  |publisher=Transaction Publishers
  |date=2009
  |isbn=1-4128-0848-0}}
* {{cite book
  |last =Khanjyan
  |first=Artush
  |title =The Monuments of Yerevan
  |publisher = VMV-Print
  |year =2004
  |isbn=99941-920-1-9}}
* {{cite book
  |last =Nalbandian
  |first=Louise Ziazan
  |title =The Armenian Revolutionary Movement of the Nineteenth Century: the Origins and Development of Armenian Political Parties
  |publisher = Department of History, [[Stanford University]]
  |year =1958}}
* {{cite book
  |last =Panossian
  |first =Razmik
  |title =The Armenians: From Kings And Priests to Merchants And Commissars
  |publisher=Columbia University Press
  |location=New York
  |date=2006
  |isbn=0-231-13926-8}}
* {{cite book
  |last =Parrot
  |first =Friedrich
  |translator=[[William Desborough Cooley]]
  |others=Introduction by Pietro A. Shakarian
  |title =Journey to Ararat
  |publisher=Gomidas Institute
  |place=London
  |date=2016
  |orig-year=1846
  |isbn=978-1909382244}}

==Further reading==
* {{hy icon}} Hakobyan P., S. Dulyan et al. "Abovyan, Khachatur", "Abovyan (city)", "Medal after Abovyan", "Abovyan House-Museum" in the [[Soviet Armenian Encyclopedia]]. vol. i. Yerevan, Armenian SSR: [[Armenian Academy of Sciences]], 1974, pp.&nbsp;32–35, 38.

==External links==
{{wikisourcelang|hy|Խաչատուր Աբովյան}}
* {{commonscat-inline|Khachatur Abovian}}
* {{ru icon}} [http://www.armenianhouse.org/abovyan/wounds-of-armenia/wounds.html ''Раны Армении'' (Wounds of Armenia)] 1977 translation to Russian. Hosted by ArmenianHouse.org
* [http://abovyanmuseum.am/en/ Khachatur Abovian House-Museum]
{{Good article}}

{{Authority control}}
{{Armenian nationalism}}
{{Romanticism}}

{{DEFAULTSORT:Abovian, Khachatur}}
[[Category:1809 births]]
[[Category:1848 deaths]]
[[Category:Place of death unknown]]
[[Category:Year of death unknown]]
[[Category:Imperial Russian Armenians]]
[[Category:Persian Armenians]]
[[Category:Armenian activists]]
[[Category:19th-century Armenian writers]]
[[Category:Writers from Yerevan]]
[[Category:Armenian male writers]]
[[Category:Nersisian School alumni]]