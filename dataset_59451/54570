{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = The Ebb-Tide
| image          = The-Ebb-Tide-Meteyard.jpg
| caption  = Cover and title page design by T. B. Meteyard for the first US printing (1894)
| author         = [[Robert Louis Stevenson]] <br/> [[Lloyd Osbourne]]
| illustrator    =
| cover_artist   =
| country        = Scotland
| language       = English
| genre          = [[Adventure novel]]
| publisher      = [[Heinemann (book publisher)|Heinemann]] (UK)<br>Smith & Kimball (US)
| release_date   = 1894
| media_type     = Print ([[Hardcover|Hardback]])
| pages          =
}}

'''''The Ebb-Tide. A Trio and a Quartette''''' ([[1894 in literature|1894]]) is a short [[novel]] written by [[Robert Louis Stevenson]] and his stepson [[Lloyd Osbourne]]. It was published the year Stevenson died.

==Plot==
Three beggars operate in the port of [[Papeete]] on [[Tahiti]]. They are Herrick, a failed English businessman; Davis, an American [[sea captain]] disgraced by the loss of his last ship; and Huish, a dishonest [[Cockney]] of various employments.

One day an off-course [[schooner]] carrying a cargo of [[Champagne (wine)|champagne]] from [[San Francisco]] to [[Sydney]] arrives in port, its officers having been killed by [[smallpox]]. With no-one else willing to risk infection, the U.S. consul employs Davis to take over the ship for the remainder of its voyage. Davis brings the other two men, along with a plan to steal the ship and navigate it to [[Peru]], where they will sell the cargo and vessel and disappear with the money.

Once at sea, Davis and Huish start drinking the cargo and spend almost all of their time intoxicated. Herrick, whose conscience is severely troubled by the plan but feels he has no other way to escape poverty, is left alone to manage the ship and three native crew members, despite having no seafaring experience.

Several days later the would-be thieves discover they have been victims of a fraud: most of the cargo is not champagne but merely bottles of water. Evidently the shipper and the previous captain had intended to sink the ship deliberately and claim the full value of the "champagne" on [[insurance]].

Now sober, Davis discovers that his rushed preparations and drunkenness leave the ship with insufficient food to reach Peru.  The only port they can reach without starving is Papeete, where they would surely be imprisoned for their actions.

They sight an unknown island, where they discover an upper-class Englishman named Attwater. Attwater, a devout Christian, has been harvesting [[pearl]]s here for many years with the help of several dozen native workers, all except four of whom have recently also died of smallpox.

The three men hatch a new plan to kill Attwater and take his pearls, but Herrick's guilt-stricken demeanour and Huish's drunken ramblings soon betray them. Attwater and his servants force them back onto the ship at gunpoint. Unable to live with himself, Herrick jumps overboard and tries to drown himself. Failing even in this, he swims to the shore and throws himself on Attwater's mercy.

The next day, Huish proposes a final plan which shocks even the unscrupulous Davis: they will go to meet Attwater under a [[flag of truce]], and Huish will disable him by [[throwing acid]] in his face. Attwater is suspicious, realises what is going on, and forces Huish to fatally spread the vitriol on himself. Attwater threatens to kill Davis as well, but forgives him and tells him, [[Jesus and the woman taken in adultery|"Go, and sin no more."]]

Two weeks later, the surviving men prepare to leave the island as Attwater's own ship approaches. Davis is now repentant and fervently religious to an almost crazed degree, and he urges the [[atheist]] Herrick to join him in his faith.

==Analysis==

The lengthy voyage of the stolen ship has been described as "a microcosm of imperialist society, directed by greedy but incompetent whites, the labour supplied by long-suffering natives who fulfil their duties without orders and are true to the missionary faith which the Europeans make no pretence of respecting".<ref name="sst">[[Roslyn Jolly]], "Introduction" in ''Robert Louis Stevenson: South Sea Tales'' (1996)</ref>

The strange and memorable character of Attwater, ruthlessly violent while talking always of Jesus' forgiveness, who alternately repels and fascinates the other characters, reflects Stevenson's own conflicted feelings about Christianity.<ref name="sst"/>

==Adaptations==
The novel was adapted into the films ''[[Ebb Tide (1922 film)|Ebb Tide]]'' (1922), ''[[Ebb Tide (1937 film)|Ebb Tide]]'' (1937), and ''[[Adventure Island (film)|Adventure Island]]'' (1947).

==References==
<references/>

==External links==
*[https://archive.org/search.php?query=title%3Aebb-tide%20creator%3Astevenson%20-contributor%3Agutenberg%20AND%20mediatype%3Atexts ''The Ebb-Tide''], at [[Internet Archive]] (scanned books color illustrated original editions)
* {{Gutenberg|no=1604|name=The Ebb-Tide}} (plain text and HTML)
* {{librivox book | title=The Ebb-Tide | author=Robert Louis STEVENSON}}

{{Robert Louis Stevenson}}

{{DEFAULTSORT:Ebb-Tide, The}}
[[Category:1894 novels]]
[[Category:Novels by Robert Louis Stevenson]]
[[Category:Scottish novels]]
[[Category:British adventure novels]]
[[Category:Novels set in Oceania]]
[[Category:Heinemann (publisher) books]]
[[Category:Novels set in Tahiti]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]
[[Category:Literary collaborations]]