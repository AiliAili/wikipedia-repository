{{Infobox Academic Conference
 | history = 1993–present
 | discipline = [[Information systems]]
 | abbreviation = ECIS
 | publisher = [[Association for Information Systems]]
 | country= International
 | frequency = annual
}}
The '''European Conference on Information Systems''' ('''ECIS''') is an annual conference for [[Information Systems]] and [[Information Technology]] academics and professionals and was affiliated with the [[Association for Information Systems]]. The organization of the conference comes under the purview of the ECIS Standing Committee. After being an affiliated AIS conference for many years, the European Conference on Information Systems now has officially been adopted as the World Region 2 (Europe, Africa and Middle East) conference for AIS since 2017.<ref>[http://aisnet.org/news/334030/ECIS-Named-Official-Conference-of-AIS-Region-2.htm ECIS Named Official Conference of AIS Region 2]</ref>

ECIS is considered the premier information systems event in the European region and provides a platform for panel discussions and the presentation of [[peer-reviewed]] information systems research papers. The conference more recently had acceptance rates in the low 30% range.{{Citation needed|date=November 2014}} The electronic version of the conference proceedings (1993-2008-excluding 1995 and 1998) and full citations (1993-2008) are available publicly.

The first ECIS conference took place in 1993 in [[Henley-on-Thames, United Kingdom]].

==ECIS Venues==

{| class="wikitable"
! Year
! Venue
! Dates
|-
| 1993 
| Henley-on-Thames, United Kingdom 
| 29-30 March 
|-
| 1994 
| Nijenrode, The Netherlands 
| 30-31 May 
|-
| 1995 
| Athens, Greece 
| 1-3 June 
|-
| 1996 
| Lisbon, Portugal 
| 2-4 July 
|-
| 1997 
| Cork, Ireland<ref>http://www.ucc.ie/esrc/ecis97.html</ref>
| 19-21 June 
|-
| 1998 
| Aix-en-Provence, France 
| 4-6 June 
|-
| 1999 
| Copenhagen, Denmark<ref>http://www.ecis99.cbs.dk/ </ref>
| 23-25 June 
|-
| 2000 
| Vienna, Austria<ref>http://ecis2000.wu-wien.ac.at/start.htm </ref>
| 3-5 July 
|-
| 2001 
| Bled, Slovenia<ref>http://ecis2001.fov.uni-mb.si/</ref>
| 27-29 June 
|-
| 2002 
| Gdansk, Poland<ref>http://ecis2002.univ.gda.pl/ecis2002.html</ref>
| 6-8 June 
|-
| 2003 
| Naples Italy<ref>{{cite web|url=http://www.ecis2003.it/ECIS2.html |title=Archived copy |accessdate=2007-01-09 |deadurl=yes |archiveurl=https://web.archive.org/web/20061217145438/http://www.ecis2003.it:80/ECIS2.html |archivedate=2006-12-17 |df= }}</ref>
| 19-21 June 
|-
| 2004 
| Turku, Finland<ref>http://www.ecis2004.fi/</ref>
| 14-16 June 
|-
| 2005 
| Regensburg, Germany<ref>http://www.ecis2005.de/ </ref>
| 26-28 May 
|-
| 2006 
| Gothenburg, Sweden<ref>http://www.ecis2006.se/ </ref>
| 12-14 June 
|-
| 2007 
| St Gallen, Switzerland<ref>http://www.ecis2007.ch/ </ref>
| 7-9 June
|-
| 2008 
| Galway, Ireland<ref>http://www.ecis2008.ie/ </ref>
| 9-11 June 
|-
| 2009 
| Verona, Italy<ref>http://www.ecis2009.it/ </ref>
| 8-10 June 
|-
| 2010 
| Pretoria, South Africa<ref>http://web.up.ac.za/default.asp?ipkCategoryID=8136 </ref>
| 7-9 June 
|-
| 2011 
| Helsinki, Finland<ref>http://project.hkkk.fi/ecis2011/</ref>
| 9-11 June 
|-
| 2012 
| Barcelona, Spain<ref>http://www.ecis2012.eu/</ref>
| 10-13 June 
|-
| 2013 
| Utrecht, Netherlands<ref>http://www.ecis2013.nl/</ref>
| 6-8 June 
|-
| 2014
| Tel Aviv, Israel<ref>http://www.ecis2014.eu/</ref>
| 9-11 June 
|-
| 2015
| Münster, Germany <ref>http://www.ecis2015.eu/</ref>
| 27-29 May
|-
| 2016
| Istanbul, Turkey <ref>http://www.ecis2016.eu/</ref>
| 12-15 June
|-
| 2017
| Guimarães, Portugal <ref>http://ecis2017.dsi.uminho.pt/</ref>
| 5-10 June
|-
| 2018
| Portsmouth, United Kingdom <ref>http://www.ecis2018.eu/</ref>
| 23-28 June
|}

{{clear}}

==See also==
*[[Information Systems]]
*[[Information systems]]
*[[Management Information Systems]]

== References ==
{{reflist|2}}

==External links==
*[http://is2.lse.ac.uk/asp/aspecis/ ECIS Electronic copies of papers (2000-2005) and full citations (1993-2005)]
*[http://www.aisnet.org/ Association for Information Systems]
*[http://personal.lse.ac.uk/whitley/ecis/ ECIS Standing Committee]
*[http://is2.lse.ac.uk/asp/aspecis/AcceptanceRates.htm ECIS conferences approximate acceptance rates]
*[http://is2.lse.ac.uk/asp/aspecis/ ECIS Proceedings]




[[Category:Information systems conferences]]
[[Category:Academic conferences]]