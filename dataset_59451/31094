{{EngvarB|date=September 2013}}
{{Italic title}}
{{Use dmy dates|date=September 2013}}
[[File:William Hogarth 006.jpg|thumb|[[William Hogarth]]]]
'''''The Four Stages of Cruelty''''' is a series of four printed [[engraving]]s published by English artist [[William Hogarth]] in 1751. Each print depicts a different stage in the life of the fictional Tom Nero.

Beginning with the [[cruelty to animals|torture of a dog]] as a child in the ''First stage of cruelty'', Nero progresses to beating his horse as a man in the ''Second stage of cruelty'', and then to robbery, [[seduction]], and murder in ''Cruelty in perfection''. Finally, in ''The reward of cruelty'', he receives what Hogarth warns is the inevitable fate of those who start down the path Nero has followed: his body is taken from the [[gallows]] after his execution as a murderer and is mutilated by [[surgery|surgeon]]s in the [[anatomical theatre]].

The prints were intended as a form of [[morality|moral]] instruction; Hogarth was dismayed by the routine acts of cruelty he witnessed on the streets of London. Issued on cheap paper, the prints were destined for the [[working class|lower classes]]. The series shows a roughness of execution and a brutality that is untempered by the funny touches common in Hogarth's other works, but which he felt was necessary to impress his message on the intended audience. Nevertheless, the pictures still carry the wealth of detail and subtle references that are characteristic of Hogarth.

==History==
In common with other prints by Hogarth, such as [[Beer Street and Gin Lane|''Beer Street'' and ''Gin Lane'']], ''The Four Stages of Cruelty'' was issued as a warning against immoral behaviour, showing the easy path from childish thug to convicted criminal. His aim was to correct "that barbarous treatment of animals, the very sight of which renders the streets of our metropolis so distressing to every feeling mind".<ref name="H1">{{Cite book|title=Anecdotes of William Hogarth, Written by Himself: With Essays on His Life and Genius, and Criticisms on his Work|author=William Hogarth|chapter=Remarks on various prints|pages=64–65, 233–238 and 336|year=1833|publisher=J.B. Nichols and Son
|url= https://books.google.com/?id=nwgIAAAAMAAJ&printsec=titlepage }}</ref> Hogarth loved animals, picturing himself with his [[pug]] in a self-portrait, and marking the graves of his dogs and birds at his home in [[Chiswick]].<ref>{{Cite book|author=Uglow, Jenny|title=Hogarth: a life and a world|publisher=Faber and Faber|year=1997|isbn=0-571-16996-1|page=501}}</ref>

Hogarth deliberately portrayed the subjects of the engravings with little subtlety since he meant the prints to be understood by "men of the lowest rank"<ref name="H1" /> when seen on the walls of workshops or [[tavern]]s.<ref name="hs">{{Cite web
|url=http://www.haleysteele.com/hogarth/plates/four_stages.html
|title=Art of William Hogarth|publisher=Haley and Steele|year=2003|accessdate=15 January 2007 |archiveurl=https://web.archive.org/web/20050406052419/www.haleysteele.com/hogarth/plates/four_stages.html |archivedate=6 April 2005}}</ref> The images themselves, as with ''Beer Street'' and ''Gin Lane'', were roughly drawn, lacking the finer lines of some of his other works. Fine engraving and delicate artwork would have rendered the prints too expensive for the intended audience, and Hogarth also believed a bold stroke could portray the passions of the subjects just as well as fine lines, noting that "neither great correctness of drawing or fine engraving were at all necessary".<ref>Quoted in Uglow, p.506.</ref>

To ensure that the prints were priced within reach of the intended audience, Hogarth originally commissioned the block-cutter J. Bell to produce the four designs as [[woodcut]]s. This proved more expensive than expected, so only the last two of the four images were cut and were not issued commercially at the time.<ref name="hs" /> Instead, Hogarth proceeded to create the engravings himself and announced the publication of the prints, along with that of ''Beer Street'' and ''Gin Lane'', in the ''[[London Evening Post]]'' over three days from 14–16 February 1751.<ref name="Paul1">{{Cite book|title=Hogarth: Art and Politics, 1750–64 Vol 3|author=Ronald Paulson|publisher=Lutterworth Press|year=1993|isbn=0-7188-2875-5|page=596}}</ref> The prints themselves were published on 21 February 1751<ref name="gordon">{{Cite web|url=http://www.litencyc.com/php/sworks.php?rec=true&UID=807|title=The Four Stages of Cruelty|author=I. R. F. Gordon|publisher=The Literary Encyclopedia|date=5 November 2003|accessdate=15 January 2007}}</ref> and each was accompanied by a moralising commentary, written by the Rev. [[James Townley]], a friend of Hogarth's.<ref name="gordon" /> As with earlier engravings, such as ''[[Industry and Idleness]]'', individual prints were sold on "ordinary" paper for ''1s.'' (one [[shilling]], equating to about £&nbsp;{{formatprice|{{inflation|UK|0.05|1751|r=1}}}} in {{CURRENTYEAR}} terms), cheap enough to be purchased by the lower classes as a means of moral instruction. "Fine" versions were also available on "superior" paper for ''1s. 6d.'' (one shilling and [[Sixpence (British coin)|sixpence]], about £&nbsp;{{formatprice|{{inflation|UK|0.075|1751|r=1}}}} in {{CURRENTYEAR}} terms) for collectors.<ref name="H1" />

Variations on plates III and IV exist from Bell's original woodcuts, bearing the earlier date of 1 January 1750,<ref name="H1" /> and were reprinted in 1790 by [[John Boydell]], but examples from either of the woodcut printings are uncommon.<ref name="hs" />{{Ref_label|A|a|none}}

==Prints==

===''First stage of cruelty''===
[[Image:William Hogarth - The First Stage of Cruelty- Children Torturing Animals - Google Art Project.jpg|thumb|''First stage of cruelty'' (Plate I)]]
In the first print Hogarth introduces Tom Nero, whose surname may have been inspired by the [[Nero|Roman Emperor of the same name]] or a [[contraction (grammar)|contraction]] of "No hero".<ref name="GU">{{Cite web|url=https://www.theguardian.com/comment/story/0,,1356440,00.html|title=A Georgian invention|author=Jonathan Jones|date=22 November 2004|work=Guardian |location=UK |accessdate=28 January 2007}}</ref><ref name="BMJ">{{Cite journal|journal=British Medical Journal|volume=309|issue=6970|title=Dr Doubledose: a taste of one's own medicine|author=Roy Porter|date=24 December 1994|pages=1714–1718|url=http://www.bmj.com/cgi/content/full/309/6970/1714|accessdate=28 January 2007|pmid=7819999|doi=10.1136/bmj.309.6970.1714|pmc=2542682}}</ref> Conspicuous in the centre of the plate, he is shown being assisted by other boys to insert an arrow into a dog's [[rectum]], a torture apparently inspired by a devil punishing a sinner in [[Jacques Callot]]'s ''Temptation of St. Anthony''.<ref name="Paul1" /> An initialled badge on the shoulder of his light-hued and ragged coat shows him to be a pupil of the [[charity school]] of the parish of [[St Giles, London|St Giles]]. Hogarth used this notorious [[slum]] area as the background for many of his works including ''Gin Lane'' and ''Noon'', part of the ''[[Four Times of the Day]]'' series. A more tender-hearted boy, perhaps the dog's owner,<ref name="SS">{{Cite book|title=Engravings by Hogarth: 101 Prints|author=Sean Shesgreen|year=1974|publisher=Dover Publications, Inc.|location=New York|isbn=0-486-22479-1}}</ref> pleads with Nero to stop tormenting the frightened animal, even offering food in an attempt to appease him. This boy supposedly represents a young [[George III of the United Kingdom|George III]].<ref name="H2">{{Cite book|title=Anecdotes of William Hogarth, Written by Himself: With Essays on His Life and Genius, and Criticisms on his Work|author=John Ireland|chapter=Four stages of cruelty|pages=233–40|year=1833|publisher=J.B. Nichols and Son|url= https://books.google.com/?id=nwgIAAAAMAAJ&printsec=titlepage&dq=hogarth }}</ref> His appearance is deliberately more pleasing than the scowling ugly ruffians that populate the rest of the picture, made clear in the text at the bottom of the scene:
{| style="font-style:italic; font-size:90%; text-align:center"
|
While various Scenes of sportive Woe,<br />
The Infant Race employ,<br />
And {{not a typo|tortur'd}} Victims bleeding shew,<br />
The Tyrant in the Boy.
|
Behold! a Youth of gentler Heart,<br />
To spare the Creature's pain,<br />
O take, he cries—take all my Tart,<br />
But Tears and Tart are vain.<br />
|
Learn from this fair Example—You<br />
Whom savage Sports delight,<br />
How Cruelty disgusts the view,<br />
While Pity charms the sight.
|}
[[Image:GeorgeIIIincruelty.PNG|left|thumb|The young "[[George III of the United Kingdom|George III]]"]]
The other boys carry out equally barbaric acts: the two boys at the top of the steps are burning the eyes out of a bird with a hot needle heated by the [[link-boy]]'s torch; the boys in the foreground are [[cock throwing|throwing at a cock]] (perhaps an allusion to a nationalistic enmity towards the [[Early Modern France|French]], and a suggestion that the action takes place on [[Shrove Tuesday]], the traditional day for cock-shying);<ref name="H2" /> another boy ties a bone to a dog's tail—tempting, but out of reach; a pair of fighting cats are hung by their tails and taunted by a jeering group of boys; in the bottom left-hand corner a dog is set on a cat; and in the rear of the picture another cat tied to two [[wikt:bladder|bladders]] is thrown from a high window. In a foreshadowing of his ultimate fate, Tom Nero's name is written under the chalk drawing of a man hanging from the gallows; the meaning is made clear by the schoolboy artist pointing towards Tom. The absence of parish officers who should be controlling the boys is an intentional rebuke on Hogarth's part; he agreed with [[Henry Fielding]] that one of the causes for the rising crime rate was the lack of care from the overseers of the poor, who were too often interested in the posts only for the social status and monetary rewards they could bring.<ref name="Paul1" />

Below the text the authorship is established: ''Designed by W. Hogarth, Published according to Act of Parliament. 1 Feb..&nbsp;1751'' The [[Act of Parliament]] referred to is the [[Engraving Copyright Act 1734]]. Many of Hogarth's earlier works had been reproduced in great numbers without his authority or any payment of [[royalties]], and he was keen to protect his artistic property, so had encouraged his friends in [[Parliament of the United Kingdom|Parliament]] to pass a law to protect the rights of engravers. Hogarth had been so instrumental in pushing the Bill through Parliament that on passing it became known as the "Hogarth Act".<ref name="gordon2">{{Cite web|url=http://www.litencyc.com/php/sworks.php?rec=true&UID=7031|title=A Rake's Progress|author=I. R. F. Gordon|publisher=The Literary Encyclopedia|date=19 July 2003|accessdate=15 January 2007}}</ref>

===''Second stage of cruelty''===
[[Image:cruelty2.JPG|thumb|left|''Second stage of cruelty'' (Plate II)]]
[[Image:Rembrandt Balaam's Ass.jpg|thumb|left|[[Rembrandt]], ''[[Balaam and the Ass]]'']]
[[Image:GeorgeTaylorHogarth.jpg|left|thumb|''George Taylor Triumphing over Death'']]
In the second plate, the scene is [[Thavies Inn]] Gate (sometimes ironically written as Thieves Inn Gate), one of the [[Inns of Chancery]] which housed associations of lawyers in London.<ref>{{Cite web|url=http://www.lincolnsinn.org.uk/chancery.asp|title=The Inn of Chancery|accessdate=27 February 2007| archiveurl = https://web.archive.org/web/20061002042130/http://www.lincolnsinn.org.uk/chancery.asp| archivedate = 2 October 2006}}</ref> Tom Nero has grown up and become a [[hackney carriage|hackney]] [[coachman]], and the recreational cruelty of the schoolboy has turned into the professional cruelty of a man at work. Tom's horse, worn out from years of mistreatment and overloading, has collapsed, breaking its leg and upsetting the carriage. Disregarding the animal's pain, Tom has beaten it so furiously that he has put its eye out. In a satirical aside, Hogarth shows four corpulent [[barrister]]s struggling to climb out of the carriage in a ludicrous state. They are probably [[caricature]]s of eminent jurists, but Hogarth did not reveal the subjects' names, and they have not been identified. Elsewhere in the scene, other acts of cruelty against animals take place: a [[drover (Britain)|drover]] beats a [[domestic sheep|lamb]] to death, an [[donkey|ass]] is driven on by force despite being overloaded, and an [[bull-baiting|enraged bull]] tosses one of its tormentors. Some of these acts are recounted in the moral accompanying the print:
{| style="font-style:italic; font-size:90%; text-align:center"
|
The generous Steed in hoary Age,<br />
Subdu'd by Labour lies;  <br />
And mourns a cruel Master's rage,<br />
While Nature Strength denies.<br />
|
The tender Lamb o'er drove and faint,<br />
Amidst expiring Throws;<br />
Bleats forth it's innocent complaint<br />
And dies beneath the Blows.
|
Inhuman Wretch!  say whence proceeds<br />
This coward Cruelty?<br />
What Int'rest springs from barb'rous deeds?<br />
What Joy from Misery?
|}

The cruelty has also advanced to include abuse of people. A [[Wagon|dray]] crushes a playing boy while the [[drayman]] sleeps, oblivious to the boy's injury and the beer spilling from his barrels. Posters in the background advertise a [[cockfight]] and a [[boxing]] match as further evidence of the brutal entertainments favoured by the subjects of the image. The boxing match is to take place at Broughton's Amphitheatre, a notoriously tough venue established by the "father of pugilism", [[Jack Broughton]]: a contemporary bill records that the contestants would fight with their left leg strapped to the floor, with the one with the fewest bleeding wounds being adjudged the victor.<ref>Uglow, p.503.</ref> One of the advertised participants in the boxing match is [[James Field (criminal)|James Field]], who was hanged two weeks before the prints were issued and features again in the final image of the series; the other participant is [[George Taylor (boxer)|George "the Barber" Taylor]], who had been champion of England but was defeated by Broughton and retired in 1750. On Taylor's death in 1757, Hogarth produced a number of sketches of him wrestling Death, probably for his tomb.<ref name="Paul4">{{Cite book|title=Hogarth's Graphic Works|author=Ronald Paulson|year=1965|publisher=Yale University Press|location=New Haven & London|isbn=0-9514808-0-4}}</ref><ref name="Paul5">{{Cite book|title=Hogarth's "Harlot": Sacred Parody in Enlightenment England|author=Ronald Paulson|year=2003|publisher=Johns Hopkins University Press|page=424|isbn=0-8018-7391-6}}</ref>{{Ref_label|B|b|none}}

According to Werner Busch, the composition alludes to Rembrandt's painting, ''Balaam's Ass'' (1626).<ref>See Werner Busch, ''Nachahmung als burgerliches Kunstprinzip: Ikonographische Zitate bei Hogarth und in seiner Nachfolge'' (Hildesheim 1977).</ref>

In an echo of the first plate, there is but one person who shows concern for the welfare of the tormented horse. To the left of Nero, and almost unseen, a man notes down Nero's hackney coach number to report him.<ref name="H2" />

===''Cruelty in perfection''===
[[Image:cruelty3.JPG|thumb|''Cruelty in perfection'' (Plate III)]]
[[Image:Cruelty3wc.PNG|thumb|In Bell's woodcut, a log in the foreground bears his and Hogarth's names and the date.]]
By the time of the third plate, Tom Nero has progressed from the mistreatment of animals to theft and murder. Having encouraged his pregnant lover, Ann Gill, to rob and leave her mistress, he murders the girl when she meets him. The murder is shown to be particularly brutal: her neck, wrist, and [[index finger]] are almost severed. Her trinket box{{Ref_label|C|c|none}} and the goods she had stolen lie on the ground beside her, and the index finger of her partially severed hand points to the words "God's Revenge against Murder" written on a book that, along with the ''[[Book of Common Prayer]]'', has fallen from the box.<ref>Uglow, p.504.</ref> A woman searching Nero's pockets uncovers pistols, a number of [[pocket watch]]es—evidence of his having turned to highway robbery (as Tom Idle did in ''[[Industry and Idleness]]''),<ref>It has been suggested that he has done so out of desperation following his dismissal as a coach driver because of the report against him from the Second Stage, as well as other atrocities assuredly committed by him. {{Cite book|title=William Hogarth, Marriage A La Mode And other Engravings|author=Samuel Ireland|publisher=[[Lear Publishers]]|quote=As a hackney-coachman, his barbarity did not pass unnoticed; his treatment of his horses became publicly known, and was attended with a discharge from his place: being therefore at a loss for a maintenance, his wicked turn of mind soon led him upon the road, which is shewn by this pistols and watches found upon him.}}</ref>  and a letter from Ann Gill which reads:

{{quote|Dear Tommy<br />My mistress has been the best of women to me, and my conscience flies in my face as often as I think of wronging her; yet I am resolved to venture body and soul to do as you would have me, so do not fail to meet me as you said you would, for I will bring along with me all the things I can lay my hands on. So no more at present; but I remain yours till death.<br /> Ann Gill.}}

The spelling is perfect and while this is perhaps unrealistic, Hogarth deliberately avoids any chance of the scene becoming comical.<ref name="H2" /> A discarded envelope is addressed "To Tho<sup>s</sup> Nero at Pinne...". [[Ronald Paulson]] sees a parallel between the lamb beaten to death in the ''Second Stage'' and the defenceless girl murdered here.<ref name="Paul1" /> Below the print, the text claims that Nero, if not repentant, is at least stunned by his actions:

{| style="font-style:italic; font-size:90%; text-align:center"
|
To lawless Love when once betray'd.<br />
Soon Crime to Crime succeeds:<br />
At length beguil'd to Theft, the Maid <br />
By her Beguiler bleeds.
|
Yet learn, seducing Man! nor Night,<br />
With all its sable Cloud, <br />
can screen the guilty Deed from sight;<br />
Foul Murder cries aloud.
|
The gaping Wounds and bloodstain'd steel,<br />
Now shock his trembling Soul:<br />
But Oh! what Pangs his Breast must feel,<br /> 
When Death his Knell shall toll.
|}

Various features in the print are meant to intensify the feelings of dread: the murder takes place in a graveyard, said to be [[St Pancras, London|St Pancras]] but suggested by [[John Ireland (biographer)|John Ireland]] to resemble [[Marylebone]];<ref name="H2" /> an [[owl]] and a [[bat]] fly around the scene; the moon shines down on the crime; the clock strikes one for the end of the [[witching hour]]. The composition of the image may allude to [[Anthony van Dyck]]'s ''The Arrest of Christ''.<ref name="BK">{{Cite book|title=Hogarth's Enthusiasm Delineated: Borrowing from the Old Masters as a Weapon in the War between an English Artist and self-styled Connoisseurs|author=Bernd Krysmanski|location=New York|publisher=Georg Olms|year=1996|isbn=3-487-10233-1}}</ref> A lone [[Parable of the Good Samaritan|Good Samaritan]] appears again: among the snarling faces of Tom's accusers, a single face looks to the heavens in pity.

In the alternative image for this stage, produced as a woodcut by Bell, Tom is shown with his hands free. There are also differences in the wording of the letter<ref name="H1" /> and some items, like the lantern and books, are larger and simpler while others, such as the man to the left of Tom and the [[topiary]] bush, have been removed.<ref name="Paulson">{{Cite book|title=Hogarth|author=Ronald Paulson|publisher=James Clarke & Co.|year=1992|isbn= 0-7188-2875-5|page=35}}</ref> The owl has become a winged [[hourglass]] on the clock tower.

===''The reward of cruelty''===
[[Image:cruelty4.JPG|thumb|right|''The reward of cruelty'' (Plate IV)]]
Having been tried and found guilty of murder, Nero has now been [[hanging|hanged]] and his body taken for the ignominious process of [[public dissection]]. The year after the prints were issued, the [[Murder Act 1752]] would ensure that the bodies of murderers could be delivered to the surgeons so they could be "dissected and anatomised". It was hoped this further punishment on the body and denial of burial would act as a deterrent.<ref name="OB">{{Cite web|url= http://www.oldbaileyonline.org/history/crime/punishment.html|title=Criminal Punishment at the Old Bailey|year=2003|publisher=The Old Bailey Proceedings Online|accessdate=12 January 2007| archiveurl = https://web.archive.org/web/20061212164652/http://www.oldbaileyonline.org/history/crime/punishment.html| archivedate = 12 December 2006}}</ref> At the time Hogarth made the engravings, this right was not enshrined in law, but the surgeons still removed bodies when they could.<ref name="Paul1" />

A [[tattoo]] on his arm identifies Tom Nero, and the rope still around his neck shows his method of execution. The dissectors, their hearts hardened after years of working with cadavers, are shown to have as much feeling for the body as Nero had for his victims; his eye is put out just as his horse's was, and a dog feeds on his heart, taking a poetic revenge for the torture inflicted on one of its kind in the first plate.<ref name="Paul1" /> Nero's face appears contorted in agony and although this depiction is not realistic, Hogarth meant it to heighten the fear for the audience. Just as his murdered mistress's finger pointed to Nero's destiny in ''Cruelty in Perfection'', in this print Nero's finger points to the boiled bones being prepared for display, indicating his ultimate fate.

While the surgeons working on the body are observed by the [[square academic cap|mortar-boarded]] academics in the front row, the physicians, who can be identified by their wigs and canes, largely ignore the dissection and consult among themselves.<ref name="FH">{{Cite book|title=From Hogarth to Rowlandson: Medicine in Art in Eighteenth-century Britain|author=Fiona Haslam|publisher=Liverpool University Press|year=1996 | location=Liverpool|pages=264–5|isbn=0-85323-630-5}}</ref> The president has been identified as [[John Freke (surgeon)|John Freke]], president of the [[Royal College of Surgeons of England|Royal College of Surgeons]] at the time.<ref name="H2" />{{Ref_label|D|d|none}} Freke had been involved in the high-profile attempt to secure the body of condemned rioter [[Bosavern Penlez]] for dissection in 1749.<ref name="Paul1" />
Aside from the over-enthusiastic dissection of the body and the boiling of the bones ''[[in situ]]'', the image portrays the procedure as it would have been carried out.<ref name="finlay">{{Cite journal|journal=Bulletin of the Medical Library Association|volume=32|issue=3|date=1 July 1944|pages=356–68|title=William Hogarth and the Doctors|author=Finlay Foster|pmid=16016656|pmc=194385}}</ref>

Two skeletons to the rear left and right of the print are labelled as [[James Field (criminal)|James Field]], a well-known boxer who also featured on a poster in the second plate, and [[James MacLaine|Macleane]], an infamous [[highwayman]]. Both men were hanged shortly before the print was published (Macleane in 1750 and Field in 1751). The skeletons seemingly point to one another. Field's name above the skeleton on the left may have been a last minute substitution for "GENTL HARRY" referring to [[Henry Simms]], also known as Young Gentleman Harry. Simms was a robber who was executed in 1747.<ref name="Paul4" /> The motif of the lone "good man" is carried through to this final plate, where one of the academics points at the skeleton of James Field, indicating the inevitable outcome for those who start down the path of cruelty.<ref name="Paul1" />
[[Image:Ketham p64.jpg|thumb|right|This woodcut image from 1495 has many of the basic elements of Hogarth's picture.]]

The composition of the scene is a [[pastiche]] of the frontispiece of [[Vesalius|Andreas Vesalius]]'s ''[[De humani corporis fabrica]]'', and it possibly also borrows from ''Quack Physicians' Hall'' (c. 1730) by the Dutch artist [[Egbert van Heemskerck]], who had lived in England and whose work Hogarth admired.<ref name="FH" /> An earlier source of inspiration may have been a woodcut in the 1495 ''[[Fasciculus medicinae|Fasciculo di medicina]]'' by [[Johannes de Ketham]] which, although simpler, has many of the same elements, including the seated president flanked by two windows.<ref name="BK" />

Below the print are these final words:

{| style="font-style:italic; font-size:90%; text-align:center"
|
Behold the Villain's dire disgrace!<br />
Not Death itself can end.<br />
He finds no peaceful Burial-Place,<br />
His breathless Corse, no friend.
|
Torn from the Root, that wicked Tongue,<br />
Which daily swore and curst! <br />
Those Eyeballs from their Sockets wrung,<br />
That glow'd with lawless Lust!
|
His Heart expos'd to prying Eyes,<br />
To Pity has no claim;<br />
But, dreadful! from his Bones shall rise,<br />
His Monument of Shame.
|}

==Reception==
Hogarth was pleased with the results. ''[[European Magazine]]'' reported that he commented to a bookseller from [[Cornhill, London|Cornhill]] (a Mr. Sewell):<ref name="Paul1" />
{{quote|there is no part of my works of which I am so proud, and in which I now feel so happy, as in the series of ''The Four Stages of Cruelty'' because I believe the publication of theme has checked the diabolical spirit of barbarity to the brute creation which, I am sorry to say, was once so prevalent in this country.|sign=''European Magazine''|source=June 1801}}
In his unfinished ''Apology for Painters'' he commented further:
{{quote|I had rather, if cruelty has been prevented by the four prints, be the maker of them than the [Raphael] cartoons, unless I lived in a Roman Catholic country.<ref>{{Cite book|title=Hogarth|first=Lawrence|last=Gowing|publisher=The Tate Gallery|year=1972|id=SBN 8435-6035-5}}</ref>}}
In his 1817 book ''Shakespeare and His Times'', [[Nathan Drake (essayist and physician)|Nathan Drake]] credits the representation of "throwing at cocks" in the first plate for changing public opinion about the practice, which was common at the time, and prompting magistrates to take a harder line on offenders.
Others found the series less to their liking. [[Charles Lamb (writer)|Charles Lamb]] dismissed the series as mere caricature, not worthy to be included alongside Hogarth's other work, but rather something produced as the result of a "wayward humour" outside of his normal habits.<ref name="CL">{{Cite journal|journal=The Reflector|author=Charles Lamb|title=On the genius and character of Hogarth: with some remarks on a passage in the writings of the late Mr. Barry|volume=2|issue=3|year=1811|pages=61–77}}</ref> Art historian [[Allan Cunningham (author)|Allan Cunningham]] also had strong feelings about the series:<ref name="Cunningham">{{Cite book|title=The Lives of the Most Eminent British Painters and Sculptors|chapter=William Hogarth|author=Allan Cunningham|year=1831|publisher=J and J Harper|page=57}}</ref>
{{quote|I wish it had never been painted. There is indeed great skill in the grouping, and profound knowledge of character; but the whole effect is gross, brutal and revolting. A savage boy grows into a savage man, and concludes a career of cruelty and outrage by an atrocious murder, for which he is hanged and dissected.}}
The [[Anatomy Act 1832]] ended the dissection of murderers, and most of the animal tortures depicted were outlawed by the [[Cruelty to Animals Act 1835]], so by the 1850s ''The Four Stages of Cruelty'' had come to be viewed as a somewhat historical series, though still one with the power to shock,<ref name="cassell">{{Cite book|title=John Casell's [[Art Treasures Exhibition]]|publisher=W. Kent and Co.|year=1858}}</ref> a power it retains for a modern audience.<ref name="finlay" />

==See also==
{{Commons category|William Hogarth}}
{{clear}}

==Notes==
{{refbegin}}

'''a.''' {{Note_label|A|a|none}}A pair of impressions from Bell's original printing were acquired for £1600 by the [[University of Glasgow]]'s [[Hunterian Museum and Art Gallery]] in 2005.<ref name="NMS">{{Cite web|url=http://www.nms.ac.uk/FileAccess.aspx?id=42|title=National Fund for Acquisitions Grants Paid 2005–2006|publisher=National Museums of Scotland|year=2006|accessdate=25 January 2007}}</ref>

'''b.''' {{Note_label|B|b|none}}There is some confusion over the date of George "The Barber" Taylor's career and death. In his earlier work Paulson puts him as a pupil of Broughton, killed in a fight with him in 1750, and the Tate Gallery dates Hogarth's sketches to c.&nbsp;1750.<ref name="Tate">{{Cite web|url=http://www.tate.org.uk/servlet/ViewWork?cgroupid=999999961&workid=23004&searchid=9264|title=George Taylor Triumphing over Death|publisher=Tate Collection|year=2004|accessdate=25 January 2007}}</ref> In ''Hogarth's "Harlot"'', he states that Taylor retired in 1750 but came out of retirement in 1757 for a final fight in which he was badly beaten, dying from his injuries several months later. Most records date Taylor's championship to the middle 1730s.

'''c.''' {{Note_label|C|c|none}}The initials on the box are normally read as A. G. for Ann Gill, but the G resembles a D, suggesting the box too may have been stolen.

'''d.''' {{Note_label|D|d|none}}John Ireland identifies the president as "Mr Frieake, the master of Nourse, to whom [[Percival Pott|Mr Potts]] was a pupil". Since Ireland identifies him as the master of Nourse, he undoubtedly means John Freke, an acquaintance of Hogarth's and surgeon at [[St Bartholomew's Hospital]] from 1729–1755 and a Governor 1736–1756. The dissection could be taking place at St Bartholomew's Hospital, where all three surgeons were based, but it also has features of the Cutlerian Theatre of the [[Royal College of Physicians]] near [[Newgate]] (particularly the throne, which bears their arms, and its curved wall resembling a [[Cockfight|cockpit]]) and the niches of the [[Worshipful Company of Barbers|Barber-Surgeons' Hall]] (which was not used for dissection after the surgeons split away to form the [[Company of Surgeons]] in 1745).

{{refend}}

==References==
{{Reflist|30em}}

{{William Hogarth}}

{{featured article}}

{{DEFAULTSORT:Four Stages Of Cruelty}}
[[Category:Prints by William Hogarth]]
[[Category:1751 works]]
[[Category:Animal rights media]]
[[Category:Cruelty to animals]]