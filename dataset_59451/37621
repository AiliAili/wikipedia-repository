{{About|Wilco album|the 2007 Tord Gustavsen Trio album|Being There (Tord Gustavsen album)|}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
 | Name = Being There
 | Type = studio
 | Artist = [[Wilco]]
 | Cover = Wilco-BeingThere.jpg
 | Released = October 29, 1996
 | Recorded = September 1995 – July 1996
 | Studio = {{hlist|[[Chicago Recording Company]] (Chicago, Illinois)|Warzone Recorders (Chicago, Illinois)|The Studio (Springfield, Missouri)|Moonshine Studios (Atlanta, Georgia)}}
 | Genre = {{hlist|[[Alternative country]]|[[alternative rock]]}}
 | Length = 76:47
 | Label = [[Reprise Records|Reprise]]
 | Producer = Wilco
 | Last album  = ''[[A.M. (Wilco album)|A.M.]]''<br />(1995)
 | This album  = '''''Being There'''''<br />(1996)
 | Next album  = ''[[Mermaid Avenue]]''<br />(1998)
 | Misc = {{Singles
  | Name           = Being There
  | Type           = album
  | single 1       = Outtasite (Outta Mind)
  | single 1 date  = 1997<ref>{{cite web|url={{Allmusic|class=album|id=r241294|pure_url=yes}}|title=Being There:Charts & Awards: Billboard Singles|work=Allmusic|accessdate=August 20, 2009}}</ref>
}}
}}

'''''Being There''''' is the second [[studio album]] by the American [[alternative rock]] band [[Wilco]], released on October 29, 1996 by [[Reprise Records]]. Despite its release as a [[double album]], ''Being There'' was sold at a single album price as a result of a deal between lead singer [[Jeff Tweedy]] and the band's label Reprise Records. The album was an improvement for the band in both sales and critical reception, in contrast to their debut album ''[[A.M. (Wilco album)|A.M.]]'' (1995). Taking its name from the [[Being There|1979 film of the same name]]{{Citation needed|date=February 2017}}, the self-produced album featured more surrealistic and introspective writing than on ''A.M.''. This was due in part to several significant changes in Tweedy's life, including the birth of his first child. Musically, it juxtaposed the [[alternative country]] styles songs reminiscent of [[Uncle Tupelo]] with psychedelic, surreal songs. It was the only Wilco album with steel guitarist Bob Egan, and their last with [[multi-instrumentalist]] [[Max Johnston]].

== Background ==
[[Jeff Tweedy]] formed [[Wilco]] in 1994 after creative differences between [[Jay Farrar]] and Tweedy caused the breakup of [[Uncle Tupelo]]. The band entered the recording studio almost immediately afterwards to record and release ''[[A.M. (Wilco album)|A.M.]]'' in 1995, which saw disappointing sales. Jay Farrar's new band [[Son Volt]] released ''[[Trace (album)|Trace]]'' in late 1995 to critical praise and good sales numbers. ''Trace'' also provided a [[college rock]] hit song in "Drown", which entered the top ten of the ''[[Billboard (magazine)|Billboard]]'' [[Mainstream Rock Tracks]] chart, further increasing competition between the two bands.<ref>For an overview of the formation of Wilco, and the competition between Farrar and Tweedy, see {{Harvnb|Kot|2004|Ref=CITEREFKot2004|pp=89–97}}</ref>

Tweedy felt that Wilco was incomplete without a second guitarist due to the departure of [[Brian Henneman]] after the ''A.M.'' recording sessions. Wilco's road manager  Bob Andrews helped Tweedy get in contact with [[Jay Bennett]], a multi-instrumentalist who had been looking for a new band to join since his [[power pop]] band Titanic Love Affair had been dismissed from its record label. Bennett joined Wilco after Tweedy sent him a few Uncle Tupelo songs and a copy of ''A.M.''. Tweedy was intrigued by the fact that Bennett could play keyboards, an instrument no other Wilco member played.<ref>Kot 2004. p. 94</ref>

== Production ==
The first conceptions of material for the album came during a particularly stressful time in Tweedy's life. Tweedy had recently quit smoking [[marijuana]], attendance at Wilco concerts was dwindling, and Tweedy was trying to manage his marriage, a mortgage, and the birth of his first child.<ref>Kot 2004. pp. 96–7</ref><ref name="Kot1">Kot 2004. p. 109</ref> He responded to these pressures:<ref name="Kot1"/>

{{cquote|I was a later bloomer. I was in my thirties before I even came to terms with the idea that I was making a living as a recording artist. I was in a punk band for a long time and thought it was my life. I was a bass player in a band making fifty dollars a night, paying eighty dollars a month in rent, making indie records and not getting paid for them, and having this naive sense of well-being that I would always do this and never have much more responsibility than that. I went from that to being a dad and a major-label recording artist who had the pressure of supporting a family and also making something I felt good about artistically.}}

For ''Being There'', Tweedy wanted to blend the experiences he had making music with the ones he had listening to music. One of the first songs that Tweedy wrote was "Misunderstood", a song about a tortured musical artist from the point of view of a fan. The song contains several overt references to the breakup of Uncle Tupelo, including the addition of insults that Farrar used against Tweedy—specifically one calling him a "[[mother's boy|mama's boy]]". Part of the song's lyrics ("Take the guitar player for a ride/See, he ain't never been satisfied/He thinks he owes some kind of debt/Be years before he gets over it") are originally from [[Peter Laughner]], an underground alternative rock artist from [[Cleveland]] that died at 24 in 1977, taken from his song "Amphetamine".<ref name="rs">{{cite journal|last=Kot|first=Greg|authorlink=Greg Kot|url=http://www.rollingstone.com/music/albumreviews/being-there-19961024|title=Being There|work=[[Rolling Stone]]|date=October 24, 1996|accessdate=July 29, 2007|archiveurl=https://web.archive.org/web/20070619094452/http://www.rollingstone.com/artists/wilco/albums/album/240156/review/5946124/being_there|archivedate=June 19, 2007|deadurl=no}}</ref><ref name="laughner-pf">{{cite web|last1=Wilcox|first1=Tyler|title=Invisible Hits: Peter Laughner, Guitar Anti-Hero|url=http://pitchfork.com/thepitch/412-invisible-hits-peter-laughner-guitar-anti-hero/|website=[[Pitchfork (website)|Pitchfork]]|accessdate=24 November 2016}}</ref> The song concludes with the artist lashing out against the listener with satirical self-pity, a rebellion against the way that fans saw Uncle Tupelo as only an archetype of [[Gram Parsons]] inspired country rock. To induce a feeling of chaos on the track, the members of Wilco recorded a track where the members switched to novel instruments and placed sections of it into the song.<ref>Kot 2004. pp. 110–1</ref> The theme of a "tortured artist" is found in other songs as well; the end of "Sunken Treasure" features Tweedy calling for the renewal of his youth as a punk rocker.<ref name="Kot2">Kot 2004. pp. 112–3</ref>

A dichotomy of musical styling was featured in the album's songs. "Hotel Arizona", "Sunken Treasure", and "Misunderstood" featured personal language and more surrealism compared to [[alternative country]] songs such as "The Lonely 1" and "Far, Far Away". To enhance this dichotomy between simple and surreal, each song was practiced, recorded, and mixed in only one day.<ref name="Kot2"/> The album's name was taken from the 1979 [[comedy movie|comedy]] ''[[Being There]]'', because the band believed that [[Peter Sellers]]' character Chance had an analogous mentality to the mindset of the album.<ref>Kot 2004. p. 117</ref> Wilco sought to incorporate influences from other bands, but not to an overbearing degree; however, they were unable to accomplish this with songs like the [[Rolling Stones]]-influenced "Monday".<ref name="Kot2"/><ref>{{cite news|last=Blackstock|first=Peter|url=http://www.nodepression.net/issues/nd05/wilco.html|title=Being There, Doing That|publisher=''[[No Depression (periodical)|No Depression]]''|date=September 1996|archiveurl = https://web.archive.org/web/20070928002630/http://www.nodepression.net/issues/nd05/wilco.html |archivedate = September 28, 2007|deadurl=yes}} Last accessed July 28, 2007.</ref> Unlike radio-friendly ''A.M.'', the band had no preference about whether ''Being There'' could yield radio hits.<ref>Kot 2004. p. 114</ref>

When the recording sessions were done, Wilco had originally recorded thirty songs, but were able to cut it down to nineteen songs covering a span of seventy-seven minutes. Tweedy decided that he wanted to release all of the material as a double CD, but was concerned that consumers would be reluctant to purchase it. The purchasing price of a double album was at least $30, but single albums cost (at most) $17.98. Tweedy approached record executive Joe McEwen, who had originally signed Uncle Tupelo to a Warner Brothers label, about selling ''Being There'' at a single album price. McEwen was reluctant at first, but was able to convince Reprise Records president Howie Klein to adhere to Tweedy's request.<ref name="Kot3">Kot 2004. p. 116</ref><ref>Kot 2004. p. 201</ref> To compensate for the financial loss that the label would take, Tweedy agreed to cut most of his royalties for the album. By 2003, it was estimated that he lost nearly $600,000 because of this, but Tweedy remained satisfied by the deal.<ref name="Kot3"/>

''Being There'' was the only Wilco album with [[Freakwater]]'s [[Bob Egan]]. Egan was invited to come to the studio after Freakwater opened for Wilco for a few shows. He only played on two tracks ("Far, Far Away" and "Dreamer in My Dreams"), but accompanied the band on the support tour. It was the last album for [[multi-instrumentalist]] [[Max Johnston]], who left because he was undergoing marital problems and believed that Bennett was taking over his place in the band.<ref>Kot 2004. p. 115</ref> Jeff Tweedy performed as the lead singer and secondary guitarist. Jay Bennett was the lead guitarist, and also played a variety of other instruments. [[John Stirratt]] played bass guitar and [[Ken Coomer]] played drums. All members of the band played a different instrument on "Misunderstood".<ref>''Being There'' liner notes, October 29, 1996. [[Reprise Records]].</ref>

== Reception ==
{{Album ratings
| rev1 = [[AllMusic]]
| rev1score = {{Rating|4.5|5}}<ref name="allmusic">{{cite web|last=Ankeny|first=Jason|url=http://www.allmusic.com/album/being-there-mw0000613155|title=Being There – Wilco|publisher=[[AllMusic]]|accessdate=July 29, 2007}}</ref>
| rev2 = ''[[Chicago Tribune]]''
| rev2Score = {{Rating|3|4}}<ref name="chicago">{{cite news|last=Rothschild|first=David|url=http://articles.chicagotribune.com/1996-11-08/entertainment/9611080113_1_tweedy-wilco-beach-boys|title=Wilco: Being There (Reprise)|work=[[Chicago Tribune]]|date=November 8, 1996|accessdate=May 31, 2013}}</ref>
| rev3 = ''[[Entertainment Weekly]]''
| rev3score = A<ref name="ew">{{cite journal|last=Schinder|first=Scott|url=http://www.ew.com/article/1996/10/25/being-there|title=Being There|work=[[Entertainment Weekly]]|date=October 25, 1996|accessdate=May 31, 2013}}</ref>
| rev4 = ''[[Los Angeles Times]]''
| rev4Score = {{Rating|3.5|4}}<ref name="latimes">{{cite news|last=Scribner|first=Sara|url=http://articles.latimes.com/1996-10-27/entertainment/ca-58192_1_record-collection|title=Wilco, 'Being There,' Reprise|work=[[Los Angeles Times]]|date=October 27, 1996|accessdate=December 7, 2015}}</ref>
| rev5 = ''[[NME]]''
| rev5Score = 9/10<ref name="nme">{{cite news|last=Bailie|first=Stuart|url=http://www.nme.com/reviews/reviews/19980101000345reviews.html|title=Wilco – Being There|work=[[NME]]|date=February 1, 1997|accessdate=December 7, 2015|archiveurl=https://web.archive.org/web/20000817111322/http://www.nme.com/reviews/reviews/19980101000345reviews.html|archivedate=August 17, 2000}}</ref>
| rev6 = [[Pitchfork Media]]
| rev6score = 6.8/10<ref name="pf">{{cite web|last=Schreiber|first=Ryan|url=http://www.pitchforkmedia.com/article/record_review/23180-being-there?artist_title=23180-being-there|title=Wilco: Being There|publisher=[[Pitchfork Media]]|date=November 1, 1996|accessdate=July 29, 2007|archiveurl=https://web.archive.org/web/20010828114626/http://pitchforkmedia.com/record-reviews/w/wilco/being-there.shtml|archivedate=August 28, 2001}}</ref>
| rev7 = ''[[Rolling Stone]]''
| rev7score = {{Rating|4|5}}<ref name="rs" />
| rev8 = ''[[The Rolling Stone Album Guide]]''
| rev8Score = {{Rating|4|5}}<ref>{{cite book|chapter=Wilco|last=Kot|first=Greg|authorlink=Greg Kot|title=[[The Rolling Stone Album Guide|The New Rolling Stone Album Guide]]|editor1-last=Brackett|editor1-first=Nathan|editor2-last=Hoard|editor2-first=Christian|publisher=[[Simon & Schuster]]|year=2004|isbn=0-7432-0169-8|pages=873–74}}</ref>
| rev9 = ''[[Spin (magazine)|Spin]]''
| rev9score = 7/10<ref name="spin">{{cite journal|last=Vowell|first=Sarah|url=https://books.google.com/books?id=dgSxMMIfuU8C&pg=PA122|title=Wilco: Being There|work=[[Spin (magazine)|Spin]]|volume=12|issue=8|date=November 1996|accessdate=December 7, 2015|pages=122–23}}</ref>
| rev10 = ''[[The Village Voice]]''
| rev10Score = B+<ref name="rc">{{cite news|last=Christgau|first=Robert|authorlink=Robert Christgau|url=http://www.robertchristgau.com/xg/cg/cgv297-97.php|title=Consumer Guide|work=[[The Village Voice]]|date=March 11, 1997|accessdate=December 7, 2015}}</ref>
}}<!-- Automatically generated by DASHBot-->
''Being There'' was received positively by critics. [[AllMusic]] editor Jason Ankeny gave the album a four-and-a-half stars and referred to it as "the group's great leap forward." He praised the band's ability to juxtapose [[psychedelia]] and [[power pop]] with tracks that "wouldn't sound at all out of place on ''[[Exile on Main Street]]''".<ref name="allmusic"/> [[Greg Kot]], writing for ''[[Rolling Stone]]'', gave the album four stars and lauded how it "venture[d] out into an anxiety-ridden world sure thing&nbsp;... the solace they continue to find in rock and roll."<ref name="rs"/> [[Robert Christgau]] was more modest, writing "there's no point in denying Jeff Tweedy's achievement as long as you recognize its insularity."<ref name="rc"/> Ryan Schreiber of [[Pitchfork Media]] called the album "massively improved as both a band and as songwriters" but also noted that "the two-disc set seems a little more than a marketing scheme."<ref name="pf"/> In 2003, Pitchfork Media named it the 88th best album of the 1990s.<ref>[http://pitchfork.com/features/staff-lists/5923-top-100-albums-of-the-1990s/2/ Pitchfork Feature: Top 100 Albums of the 1990s<!-- Bot generated title -->]</ref> Critics at ''[[The Village Voice]]'' named ''Being There'' the 14th best of the year on its [[Pazz & Jop]] critics poll for 1996.<ref>{{cite news|url=http://www.robertchristgau.com/xg/pnj/pjres96.php|title=The 1996 Pazz & Jop Critics Poll|work=[[The Village Voice]]|date=February 25, 2007|accessdate=July 11, 2007}}</ref> In 2004, [[Stylus Magazine]] placed 178th on their "Top 101–200 Favorite Albums of All-Time" list.<ref>[http://www.stylusmagazine.com/articles/weekly_article/top-101-200-favourite-albums-ever-the-stylus-magazine-list.htm Top 101–200 Favourite Albums Ever : The Stylus Magazine List - Article - Stylus Magazine<!-- Bot generated title -->]</ref>  The album was also included in the book ''[[1001 Albums You Must Hear Before You Die]]''.<ref>{{cite book|author1=Robert Dimery|author2=Michael Lydon|title=1001 Albums You Must Hear Before You Die: Revised and Updated Edition|accessdate= |date=23 March 2010|publisher=Universe|isbn=978-0-7893-2074-2}}</ref>

The album was a marked improvement over ''A.M.'' on the ''[[Billboard (magazine)|Billboard]]'' charts. It peaked at number 73 on the [[Billboard 200|''Billboard'' 200]], whereas ''A.M.'' failed to hit the chart at all.<ref>{{cite news|title=The ''Billboard'' 200|publisher=''Billboard''|date=November 16, 1996}}</ref> "Outtasite (Outta Mind)" was released as a single, and received moderate airplay on some [[college rock]] radio stations.<ref>{{cite news|title=Hot Mainstream Rock Tracks|publisher=''[[Billboard (magazine)|Billboard]]''|date=April 19, 1997}}</ref><ref>{{cite news|title=Hot Modern Rock Tracks|publisher=''[[Billboard (magazine)|Billboard]]''|date=March 15, 1997}}</ref> As of 2003, the album had sold over 300,000 copies.

[[Valve Corporation]] used "Someone Else's Song" as a basis for the opening theme for The Engineer in their [[first-person shooter]] game ''[[Team Fortress 2]]''.

== Track listing ==
All songs written by [[Jeff Tweedy]].

{{Track listing
| headline        = Side one
| title1          = Misunderstood
| note1          = The verse starting 'Take the guitar player for a ride' is written by [[Peter Laughner]].
| length1         = 6:28
| title2          = Far, Far Away
| length2         = 3:20
| title3          = Monday
| length3         = 3:33
| title4          = Outtasite (Outta Mind)
| length4         = 2:34
| title5          = Forget the Flowers
| length5         = 2:47
}}
{{Track listing
| headline        = Side two
| title1          = Red-Eyed and Blue
| length1         = 2:45
| title2          = I Got You (At the End of the Century)
| length2         = 3:57
| title3          = What's the World Got in Store
| length3         = 3:09
| title4          = Hotel Arizona
| length4         = 3:37
| title5          = Say You Miss Me
| length5         = 4:07
}}

{{Track listing
| headline        = Side three
| title1          = Sunken Treasure
| length1         = 6:51
| title2          = Someday Soon
| length2         = 2:33
| title3          = Outta Mind (Outta Sight)
| length3         = 3:20
| title4          = Someone Else's Song
| length4         = 3:21
| title5          = Kingpin
| length5         = 5:17
}}
{{Track listing
| headline        = Side four
| title1          = (Was I) In Your Dreams
| length1         = 3:30
| title2          = Why Would You Wanna Live
| length2         = 4:16
| title3          = The Lonely 1
| length3         = 4:48
| title4          = Dreamer in My Dreams
| length4         = 6:43
}}

== Personnel ==
Wilco:
* [[Jeff Tweedy]] – lead and backing vocals, guitars, bass, radio
* [[John Stirratt]] – bass, piano, violin, backing vocals
* [[Jay Bennett]] – guitars, piano, organ, harmonica, lap steel, drums, accordion, backing vocals
* Ken Coomer – drums, percussion, guitars, backing vocals
* Max Johnston – dobro, fiddle, mandolin, banjo, backing vocals
Additional musicians:
* Bob Egan – pedal steel (D1: T2), national steel guitar (D2: T9)
* Greg Leisz – pedal steel (D2: T8)
* Larry Williams – tenor sax (D1: T3)
* Gary Grant, [[Jerry Hey]] – trumpet (D1: T3)
* Jesse Green – violin (D2: T8)
* Dan Higgins – baritone sax, tenor sax (D1: T3)
* Jim Rondinelli, [[Chris Shepard]] – [[audio engineering|engineering]], [[audio mastering|mastering]]
* Ron Lowe, Mike Scotella, Lou Whitney, [[Chris Shepard]]<ref>{{cite web | url=http://www.chicagorecording.com/chrisshepard/index.html | title=Chris Shepard at Chicago Recording Company | accessdate=March 27, 2011}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> – engineers
* Bob Ludwig, Skip Saylor, Jim Scott – mastering
* Dahn Davis – [[graphic design]]
* Brad Miller – [[photography]]

== Notes and references ==
{{reflist|2}}

== References ==
* {{Cite book|last=Kot|first=Greg|author-link=Greg Kot|year=2004|title=[[Wilco: Learning How to Die]]|edition=1st|place=[[New York City]], [[New York (state)|NY]]|publisher=Broadway Books|isbn=0-7679-1558-5}}

{{Wilco}}
{{good article}}

[[Category:1996 albums]]
[[Category:Reprise Records albums]]
[[Category:Wilco albums]]
[[Category:English-language albums]]
[[Category:Albums produced by Jeff Tweedy]]