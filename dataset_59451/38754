{{good article}}
{{other ships|HMS Bristol}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Bristol (1910).JPG|300px]]
|Ship caption=''Bristol'' underway
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag={{shipboxflag|UK|naval}}
|Ship name=''Bristol''
|Ship namesake=[[Bristol]]
|Ship ordered=
|Ship awarded=
|Ship builder=[[John Brown & Company]], [[Clydebank]]
|Ship laid down=23 March 1909
|Ship launched=23 February 1910
|Ship christened=
|Ship commissioned=December 1910
|Ship recommissioned=
|Ship decommissioned=30 May 1919
|Ship in service=
|Ship out of service=
|Ship refit=
|Ship struck=
|Ship fate=Sold for [[ship breaking|scrap]], 9 May 1921
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=(as built)
|Ship class={{Sclass2-|Town|cruiser (1910)|0}} [[light cruiser]]
|Ship displacement={{convert|4800|LT|t|0}}
|Ship length=*{{convert|430|ft|m|abbr=on|1}} [[Length between perpendiculars|p/p]]
*{{convert|453|ft|m|abbr=on|1}} [[Length overall|o/a]]
|Ship beam={{convert|47|ft|m|abbr=on|1}} 
|Ship draught={{convert|15|ft|3|in|m|abbr=on}} (mean)
|Ship power=*{{convert|22000|shp|kW|lk=in|abbr=on}}
*12 × [[Yarrow boiler]]s
|Ship propulsion= 2 × shafts; 2 × Brown-Curtis [[steam turbine]]s
|Ship speed={{convert|25|kn|lk=in|abbr=on}}
|Ship range={{convert|5830|nmi|lk=in}} at {{convert|10|kn}}
|Ship complement=410
|Ship armament=*2 × single [[BL 6 inch Mk XI naval gun|BL {{convert|6|in|mm|abbr=on|0}} Mk XI gun]]s
*10 × single [[BL 4 inch naval gun Mk VII|BL {{convert|4|in|mm|abbr=on|0}} Mk VII guns]]
*4 × single [[Ordnance QF 3 pounder Vickers|QF 3-pounder ({{convert|47|mm|in|abbr=on}}) guns]]
*2 × [[British 18-inch torpedo|{{convert|18|in|mm|abbr=on|0}}]] [[torpedo tube]]s
|Ship armour=*[[Deck (ship)|Deck]]: {{convert|.75|-|2|in|mm|abbr=on}}
*[[Conning tower|Conning Tower]]: {{convert|6|in|mm|abbr=on}}
|Ship notes=
}}
|}

'''HMS ''Bristol''''' was a {{sclass2-|Town|cruiser (1910)|0}} [[light cruiser]] built for the [[Royal Navy]] in the first decade of the 20th century. She was the [[lead ship]] of the five in her [[ship class|sub-class]] and was completed in late 1910. The ship spent part of her early career in [[Reserve fleet|reserve]] before she was transferred to the [[4th Cruiser Squadron (United Kingdom)|4th Cruiser Squadron]] (4th CS) of the [[North America and West Indies Station]] in mid-1914. ''Bristol'' was briefly deployed to Mexico during the [[Mexican Revolution]] to protect British interests there.

The ship was tasked to protect Allied shipping off the coasts of North and South America from German [[commerce raider]]s after World War I began in August 1914. She briefly encountered a German light cruiser in the [[West Indies]] a few days after the war began, but the battle was inconclusive. A few months later, ''Bristol'' played a minor role in the [[Battle of the Falkland Islands]] in December by sinking some of the [[collier (ship)|collier]]s belonging to the [[German East Asia Squadron]]. After a lengthy refit in mid-1915, the ship was transferred to the [[Adriatic Force]] where she participated in the [[Battle of the Strait of Otranto (1917)|Battle of the Strait of Otranto]] in 1917. ''Bristol'' returned to her former task of patrolling off the east coast of South America, after a brief time escorting convoys off [[West Africa]] in early 1918, and continued to do so after the end of the war. She was placed in reserve in mid-1919, listed for sale in 1920 and was sold for [[ship breaking|scrap]] in 1921.

==Design and description==
The ''Bristol'' sub-class{{#tag:ref|Sometimes known as the ''Glasgow'' class.<ref name="warv1n1 p56">Lyon, Part 1, p. 56</ref>|group=Note}} was intended for a variety of roles including both trade protection and duties with the fleet.<ref name="conways06 p51">Gardiner & Gray 1985, p. 51</ref> They were {{convert|453|ft|m|1}} [[overall length|long overall]], with a [[Beam (nautical)|beam]] of {{convert|47|ft|m|1}} and a [[Draft (ship)|draught]] of {{convert|15|ft|6|in|m|1}}. [[Displacement (ship)|Displacement]] was {{convert|4800|LT|t|lk=on}} normal and {{convert|5300|LT|t}} at full load. Twelve [[Yarrow boiler]]s fed ''Bristol''{{'}}s Brown-Curtis [[steam turbine]]s, driving two [[propeller shaft]]s, that were rated at {{convert|22000|shp|lk=in}} for a design speed of {{convert|25|kn}}.<ref name="conways06 p51"/> The ship reached {{convert|27|kn}} during her [[sea trials]] from {{convert|28711|shp|abbr=on}}. The ship's experimental two-shaft layout was very successful, giving greater efficiency, especially at lower speeds, than the four-shaft arrangement of her [[sister ship]]s.<ref name="warv1i2 p59">Lyon, Part 2, pp. 59–60</ref> The boilers used both [[fuel oil]] and coal, with {{convert|1353|LT|t|0}} of coal and {{convert|256|LT|t|0}} tons of oil carried, which gave a range of {{convert|5830|nmi|lk=in}} at {{convert|10|kn}}.<ref name=f3>Friedman, p. 383</ref>

The main armament of the ''Bristol'' class was two [[BL 6 inch Mk XI naval gun|BL 6-inch (152 mm) Mk XI guns]] that were mounted on the centreline fore and aft of the [[superstructure]] and ten [[BL 4 inch naval gun Mk VII|BL 4-inch Mk VII]] guns in waist mountings. All these guns were fitted with [[gun shield]]s.<ref name="conways06 p51"/> Four [[Ordnance QF 3-pounder Vickers|Vickers 3-pounder (47&nbsp;mm)]] [[saluting gun]]s were fitted, while two submerged [[British 18-inch torpedo|18-inch (450 mm)]] [[torpedo tubes]] were fitted.<ref>Lyon, Part 2, pp. 55–57</ref> This armament was considered rather too light for ships of this size,<ref name="warv1n1 p53">Lyon, Part 1, p. 53</ref> while the waist guns were subject to immersion in a high sea, making them difficult to work.<ref name="Brown p63"/>

The ''Bristol''s were considered [[protected cruiser]]s, with an armoured [[deck (ship)|deck]] providing protection for the ships' vitals. The armoured deck was {{convert|2|in|mm}} thick over the [[Magazine (artillery)|magazines]] and machinery, {{convert|1|in|mm}} over the steering gear and {{convert|3/4|in|mm|0}} elsewhere. The [[conning tower]] was protected by {{convert|6|in|mm|0}} of armour, with the gun shields having {{convert|3|in|mm}} armour, as did the ammunition hoists.<ref>Lyon, Part 2, p. 59</ref> As the protective deck was at [[waterline]], the ships were given a large [[metacentric height]] so that they would remain stable in the event of flooding above the armoured deck. This, however, resulted in the ships [[Ship_motions#Rotation_motions|rolling]] badly making them poor gun platforms.<ref name="Brown p63">Brown, p. 63</ref> One problem with the armour of the ''Bristols'' which was shared with the other Town-class ships was the sizeable gap between the bottom of the gun shields and the deck, which allowed shell splinters to pass through the gap, giving large numbers of leg injuries in the ships' gun crews.<ref name="warv1n2 p57">Lyon, Part 2, p. 57</ref>

==Construction and career==
''Bristol'', the fifth ship of her name to serve in the Royal Navy,<ref>Colledge, p. 49</ref> was named after the [[Bristol|eponymous city]]. She was [[laid down]] on 23 March 1909 at [[John Brown & Company]]'s [[Clydebank]] shipyard, [[ship naming and launching|launched]] on 23 February 1910 and completed on 17 December 1910.<ref name=f11>Friedman, p. 411</ref> As of 18 February 1913, the ship was assigned to the [[5th Battle Squadron (United Kingdom)|5th Battle Squadron]] of the reserve [[Second Fleet (United Kingdom)|Second Fleet]] at [[HMNB Devonport|Devonport]].<ref>{{cite web|title=The Monthly Navy List|url=https://archive.org/details/navylistmar1913grea|website=National Library of Scotland|date=March 1913|publisher=[[His Majesty's Stationery Office]]|location=London|accessdate=8 August 2016|page=269b}}</ref> On 1 July, ''Bristol'' was transferred to the [[2nd Light Cruiser Squadron (United Kingdom)|2nd Light Cruiser Squadron]] of the Second Fleet. Almost six months later, some of her crewmen helped to put out a fire in [[HMNB Portsmouth|Portsmouth Dockyard]] on 20 December; two men were killed fighting the fire.<ref name=tr>Transcript</ref>

On 18 May 1914, the ship was reassigned to the 4th CS<ref>{{cite web|title=The Navy List|url=https://archive.org/details/navylistjun1914grea|website=National Library of Scotland|date=June 1914|publisher=[[His Majesty's Stationery Office]]|location=London|accessdate=8 August 2016|page=269}}</ref> of the North America and West Indies Station and sailed that day to join the squadron. She arrived at [[Jamaica]] on the 31st and was ordered to depart for Mexico after recoaling to protect British interests during the on-going Mexican Revolution. ''Bristol'' spent most of June in [[Tampico]], but sailed for [[Veracruz]] at the end of the month where [[Rear-Admiral (Royal Navy)|Rear-Admiral]] [[Christopher Craddock]], the squadron commander, inspected the ship and her crew on 1 July. The ship departed for Puerto México (present day [[Coatzacoalcos]]) on the 15th and served as temporary refuge for members of the family of ex-president [[Victoriano Huerta]] for a few days as they were fleeing the country at the time of the [[United States occupation of Veracruz]]. ''Bristol'' sailed for Jamaica on the 31st as tensions with Germany rose.<ref name=tr/>

===World War I===
The 4th Cruiser Squadron was tasked to protect Allied merchant shipping from commerce raiders in the Caribbean and along the East Coast of North America and Craddock dispersed his ships shortly before the war began on 4 August 1914 in a futile search for the two German warships known to be in the area. In the early evening of 6 August, ''Bristol'' spotted the German light cruiser {{SMS|Karlsruhe|1912|2}}, but failed to inflict any significant damage before engine problems allowed the German ship to disengage behind her own smoke. Later in the month, ''Bristol'' began patrolling off the northern coast of Brazil in an unsuccessful attempt to find the German ships. The ship was detached to continue to patrol the Brazilian coast and did not join Craddock's ships as they searched for the East Asia Squadron off the [[Chile|Chilean]] coast in October.<ref>Corbett, I, pp. 44–49, 261–62, 309, 324</ref> 

In mid-October, ''Bristol'', together with the [[armoured cruiser]] {{HMS|Cornwall|1902|2}} and two [[armed merchant cruiser]]s was assigned to a new squadron commanded by Rear Admiral [[Archibald Stoddart]] that was tasked to patrol the South American coast north of [[Montevideo]], [[Uruguay]].<ref>Corbett, I, p. 327</ref> After Craddock's squadron was destroyed in the [[Battle of Coronel]] on 1 November, the ship rendezvoused at the [[Abrolhos Archipelago]] on 26 November with the reinforcements commanded by [[Vice-Admiral]] [[Doveton Sturdee]]. The ships then proceeded to the [[Falkland Islands]] where they arrived on 7 December.<ref>Massie, pp. 244, 249–50</ref>

====Battle of the Falklands====
{{main|Battle of the Falklands}}
Upon arrival at [[Port Stanley]] on 7 December, Sturdee gave permission for ''Bristol'' to put out her fires to clean her boilers and repair both engines. He planned to recoal the entire squadron the following day from the two available colliers and to begin the search for the East Asia Squadron the day after. Vice-Admiral [[Maximilian von Spee]], commander of the German squadron, had other plans and intended to destroy the radio station at Port Stanley on the morning of 8 December. The appearance of two German ships at 07:30 caught Sturdee's ships by surprise although they were driven off by {{convert|12|in|adj=on|0}} shells fired by the [[predreadnought battleship]] {{HMS|Canopus|1897|2}} when they came within range around 09:20. This gave time for ''Bristol'' to reassemble her engines and raise steam. As the ship was leaving harbour around 10:45, she received reports of German ships about {{convert|30|mi}} south and Sturdee ordered her to intercept and destroy them, together with the armed merchant cruiser {{SS|Macedonia}}. The British ships were able to capture two of the three German colliers and sank them after taking off their crews.<ref>Massie, pp. 251, 257–59, 263–64</ref>

The light cruiser {{SMS|Dresden}} was the only German ship able to disengage from the battle and Sturdee ordered ''Bristol'' to [[Puntas Arenas]], Chile, after the ship was reported coaling there on 13 December. She arrived there the next day, but ''Dresden'' had left the night before<ref>Massie, pp. 279–80</ref> ''Bristol'' spent the next several months hunting for the German cruiser along the Argentinian and Chilean coasts and in the innumerable bays and inlets of [[Tierra del Fuego]]. During this time, she struck a [[shoal]] and seriously damaged her [[rudder]] on 22 February 1915. In late April, the ship began patrolling off the Brazilian coast. ''Bristol'' began a major refit at [[Gibraltar]] on 27 May that lasted until 5 August. Upon its completion, she was transferred to the British Adriatic Force to help contain the [[Austro-Hungarian Navy|Austro-Hungarian Fleet]] and defend the [[Otranto Barrage]]. She then spent most of the next several months patrolling the lower reaches of the [[Adriatic Sea]] and the [[Strait of Otranto]]. On 3 October, her crew was inspected by Rear-Admiral [[Cecil Thursby]]. On 26 November, ''Bristol'' sailed for Gibraltar en route to a refit at [[William Beardmore and Company]]'s shipyard in [[Dalmuir]] that began on 17 December.<ref name=tr/>

====Battle of the Strait of Otranto====
{{main|Battle of the Strait of Otranto (1917)}}

On the early morning of 15 May 1917, the Austro-Hungarians made their most serious attack of the war on the [[naval drifter]]s controlling the barrage. ''Bristol'' was the "ready ship" at [[Brindisi]] and was at a half-hour's notice for sea; she departed at 04:50, escorted by two Italian [[destroyer]]s, in an attempt to intercept the three Austrian light cruisers that had conducted the attack. [[Contrammiraglio]] (Rear-Admiral) [[Alfredo Acton]], commander of the ships at Brindisi, ordered that her speed be limited to {{convert|20|kn}} to prevent her from getting too far ahead of the other ships now raising steam. One of these was ''Bristol''{{'}}s [[sister ship|half-sister]] {{HMS|Dartmouth|1911|2}}, which departed at 05:36, also escorted by a pair of Italian destroyers. The ''Dartmouth'' group overtook ''Bristol'' and her consorts by 07:12 and were joined by the Italian [[scout cruiser]] {{ship|Italian cruiser|Aquila||2}} around 07:40. Acton deployed his cruisers in [[line abreast]] with ''Aquila'' leading them and the destroyers guarding the flanks. ''Bristol''{{'}}s bottom was foul, however, and limited the group to a speed of {{convert|24|kn}}.<ref>Halpern, pp. 68–73</ref>

Five minutes later, the Allied ships spotted clouds of smoke on the horizon and Acton ordered the Italian ships to attack shortly afterwards while the two British cruisers turned to cut off the two Austrian destroyers. ''Aquila'' opened fire at 08:15 at long range, but inflicted no damage before she was immobilised by a hit at 08:32 that detonated inside her central [[Fire room|boiler room]] and severed her main steam pipe. The Austrian ships managed to disengage before the cruisers could close the distance.<ref>Halpern, pp. 77–79</ref>

The main Austro-Hungarian force of three light cruisers trailed the leading destroyers by a considerable distance and [[Commander]] [[Miklós Horthy]] spotted the Allied force around 09:05. Acton spotted them about five minutes later and manoeuvred his ships to cover the disabled ''Aquila'' rather than [[Crossing the T|crossing the Austrians' T]]. The British ships opened fire about 09:30, although Horthy's ships quickly laid a [[smoke screen]] and turned away through it. Both sides settled on parallel courses to the north-northwest and ''Bristol'' gradually began to fall behind and could eventually only use her bow gun at very long range before ceasing fire at 10:15. The British fire was moderately effective, but the Austrian ships concentrated their fire on ''Dartmouth'' which was hit three times, although not significantly damaged. Acton reduced his speed around 10:45 to allow ''Bristol'' to catch up. At 10:58 he ordered speed to be increased and turned two minutes later in an unsuccessful attempt to cut off the trailing Austrian cruiser. At 11:04, the British cruisers ceased fire and turned away on Acton's order, presumably to avoid encountering Austro-Hungarian reinforcements which Acton knew were en route. During the battle the British cruisers were repeatedly attacked by Austro-Hungarian aircraft, but they inflicted no significant damage or casualties.<ref>Halpern, pp. 81–83, 86–92</ref>

====Subsequent operations====
On 1 January 1918, ''Bristol'' was briefly based at the port of [[Kalloni]], [[Lesbos]], before sailing to Gibraltar for a long refit. She arrived on 17 January and departed on 2 April, bound for [[Sierra Leone]], [[West Africa]], to begin convoy escort duties<ref name=tr/> there despite being assigned to the East Coast of South America Station.<ref>{{cite web|title=Supplement to the Monthly Navy List Showing the Organisation of the Fleet, Flag Officer's Commands, &c.|url=https://ia700709.us.archive.org/21/items/navylistmay1918grea/navylistmay1918grea.pdf|website=National Library of Scotland|publisher=Admiralty|date=May 1918|accessdate=8 August 2016|page=21}}</ref> On 1 May, ''Bristol'' departed [[Dakar]], [[French West Africa]], bound for the Brazilian coast, arriving at [[Rio de Janeiro]] on the 15th. At the end of the month, she spent a few days salvaging ammunition from the wreck of the [[cargo liner]] {{SS|Highland Scot}}. The ship patrolled the coasts of Brazil and Uruguay for the rest of the war and visited Port Stanley at the end of the year where she remained until 11 January 1919. ''Bristol'' sailed for the [[Cape Verde Islands]] in late April and then to Gibraltar where she arrived on 13 May. En route, three sailors [[1918 flu pandemic|died from influenza]]. A few days later, she departed for the UK and arrived at Portsmouth on the 21st. ''Bristol'' was [[Ship decommissioning|paid off]] on 30 May and reduced to reserve the same day.<ref name=tr/> The ship was listed for disposal in May 1920<ref name=f11/> and was sold for scrap on 9 May 1921 to [[Thos W Ward]] of [[Hayle]].<ref>Lyon, Part 3, p. 51</ref>

==Notes==
{{reflist|group=Note}}  

==Footnotes==
{{reflist|30em}}

== Bibliography ==
*{{cite book|last=Brown|first=David K.|title=The Grand Fleet: Warship Design and Development 1906–1922|year=2010|publisher=Seaforth Publishing|location=Barnsley, UK|isbn=978-1-84832-085-7}}
*{{Colledge}}
* {{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations to the Battle of the Falklands|edition=2nd, reprint of the 1938|series=History of the Great War: Based on Official Documents|volume=I|publisher=Imperial War Museum and Battery Press|location=London and Nashville, Tennessee|isbn=0-89839-256-X}}
*{{cite book|last=Friedman|first=Norman|title=British Cruisers: Two World Wars and After|year=2010|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|isbn=978-1-59114-078-8}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5|last-author-amp=yes}}
*{{cite book|last=Halpern|first=Paul G.|title=The Battle of the Otranto Straits: Controlling the Gateway to the Adriatic in WWI|year=2004|location=Bloomington, Indiana|publisher=Indiana University Press, 2004|isbn=0-253-34379-8}}
*{{cite journal|last=Lyon|first=David|title=The First Town Class 1908–31: Part 1|journal=[[Warship (journal)|Warship]]|issue=No. 1|volume=Vol. 1|year=1977|pages=48–58|publisher=Conway Maritime Press|location=London|isbn=0-85177-132-7}}
*{{cite journal|last=Lyon|first=David|title=The First Town Class 1908–31: Part 2|journal=Warship|issue=No. 2|volume=Vol. 1|year=1977|pages=54–61|publisher=Conway Maritime Press|location=London|isbn=0-85177-132-7}}
*{{cite journal|last=Lyon|first=David|title=The First Town Class 1908–31: Part 3|journal=Warship|issue=No. 3|volume=Vol. 1|year=1977|pages=46–51|publisher=Conway Maritime Press|location=London|isbn=0-85177-132-7}}
* {{cite book|title=[[Castles of Steel: Britain, Germany, and the Winning of the Great War at Sea]] |last=Massie|first=Robert K.|authorlink=Robert K. Massie|publisher=Jonathan Cape|year=2004|location= London|isbn=0-224-04092-8}}
*{{cite web|url=http://naval-history.net/OWShips-WW1-06-HMS_Bristol.htm|title=HMS BRISTOL – July 1913 to May 1919, UK home, West Indies, South America, Battle of the Falklands, Mediterranean, South American Station, Convoy escort, UK home|work=Royal Navy Log Books of the World War 1 Era|publisher=Naval-History.net|accessdate=8 August 2016}}

==External links==
{{Commons category|HMS Bristol (ship, 1910)}}
*[http://www.worldwar1.co.uk/light-cruiser/hms-Bristol.html Ships of the Bristol group]

{{Town class cruiser 1910}}

{{DEFAULTSORT:Bristol (1910)}}
[[Category:Town-class cruisers (1910) of the Royal Navy]]
[[Category:Clyde-built ships]]
[[Category:1910 ships]]
[[Category:World War I cruisers of the United Kingdom]]