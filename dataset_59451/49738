{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox character
| colour      =
| colour text =
| name        = Miss Havisham
| series      = [[Great Expectations]]
| image       = Havisham.jpg
| caption     = Miss Havisham, by [[Harry Furniss]]
| first       =
| last        =
| cause       =
| creator     = [[Charles Dickens]]
| portrayer   = [[Gillian Anderson]]<br>[[Anne Bancroft]]<br>[[Helena Bonham Carter]]<br>[[Joan Hickson]]<br>[[Martita Hunt]]<br>[[Margaret Leighton]]<br>[[Tuppence Middleton]]<br>[[Charlotte Rampling]]<br>[[Florence Reed]]<br>[[Jean Simmons]]<br>[[Tabu (actress)|Tabu]]
| episode     =
| nickname    =
| alias       =
| occupation = [[Inheritance|Heiress]]<br>[[Hermit]]
| gender      = Female
| significantother = [[Compeyson]]
| family      = [[Arthur Havisham]] (half brother)
| relatives   = [[Herbert Pocket|Pocket family]] (cousins)<br>Cousin Raymond<br>Georgiana<br>Bentley Drummle (son-in-law)
| children    = [[Estella (Great Expectations)|Estella]] (adoptive daughter)
| nationality = British
}}

'''Miss Havisham''' is a significant character in the [[Charles Dickens]] novel ''[[Great Expectations]]'' (1861). She is a wealthy [[spinster]] who lives in her ruined [[mansion]], Satis House, with her adopted daughter, [[Estella (Great Expectations)|Estella]]. Dickens describes her as looking like "the witch of the place".

Although she has often been portrayed in film versions as very elderly, Dickens's own notes indicate that she is only in her mid-fifties. However, it is also indicated that her long life away from the sunlight has in itself aged her, and she is said to look like a cross between a [[wax sculpture|waxwork]] and a [[skeleton]], with moving eyes.

==Character history==
Miss Havisham's father was a wealthy [[brewer]] and her mother had died shortly after she was born. He married again in secret and conceived a son, Arthur, with the family cook, but Miss Havisham's relationship with her brother was far from harmonious.

She inherited most of her father's fortune and fell in love with a man named [[Compeyson]], who had conspired with the jealous Arthur to swindle her of her riches. Her cousin, [[Herbert Pocket|Matthew Pocket]], warned her to be careful, but she was too much in love to listen. On the wedding day, while she was dressing, Miss Havisham received a letter from Compeyson and realised he had [[Breach of promise|defrauded]] her and she had been left at the altar.
[[File:Breakhisheart.jpg|thumb|Miss Havisham with Estella and Pip ([[H. M. Brock]])]]
Humiliated and heartbroken, Miss Havisham suffered a [[mental breakdown]] and remained alone in her decaying mansion [[Satis House]] – never removing her [[wedding dress]], wearing only one shoe, leaving the [[wedding breakfast]] and [[wedding cake|cake]] uneaten on the table, and allowing only a few people to see her. She even had the clocks in her mansion stopped at twenty minutes to nine: the exact time when she had received Compeyson's letter.

Time passed and Miss Havisham had her lawyer, [[Great Expectations#The lawyer and his circle|Mr. Jaggers]], adopt a daughter for her.
{{quote|I had been shut up in these rooms a long time (I don't know how long; you know what time the clocks keep here), when I told him that I wanted a little girl to rear and love, and save from my fate. I had first seen him when I sent for him to lay this place waste for me; having read of him in the newspapers, before I and the world parted. He told me that he would look about him for such an orphan child. One night he brought her here asleep, and I called her [[Estella (Great Expectations)|Estella]].}}

===From protection to revenge===
While Miss Havisham's original goal was to prevent Estella from suffering as she had at the hands of a man, it changed as Estella grew older:

{{quote|Believe this: when she first came, I meant to save her from misery like my own. At first I meant no more. But as she grew, and promised to be very beautiful, I gradually did worse, and with my praises, and with my jewels, and with my teachings, and with this figure of myself always before her a warning to back and point my lessons, I stole her heart away and put ice in its place.}} While Estella was still a child, Miss Havisham began casting about for boys who could be a testing ground for Estella's education in breaking the hearts of men as vicarious revenge for Miss Havisham's pain. [[Great Expectations|Pip]], the narrator, is the eventual victim; and Miss Havisham readily dresses Estella in jewels to enhance her beauty and to exemplify all the more the vast social gulf between her and Pip. When, as a young adult, Estella leaves for France to receive education, Miss Havisham eagerly asks him, "Do you feel you have lost her?"

===Repentance and death===
[[File:HavishamFraser.png||thumb|Miss Havisham is begging Pip for his forgiveness ([[F. A. Fraser]])]]
Miss Havisham [[repentance|repents]] late in the novel when Estella leaves to marry Pip's rival, [[Great Expectations#Pip's antagonists|Bentley Drummle]]; and she realises that she has caused Pip’s heart to be broken in the same manner as her own; rather than achieving any kind of personal revenge, she has only caused more pain. Miss Havisham begs Pip for forgiveness.

{{quote|Until you spoke to [Estella] the other day, and until I saw in you a looking-glass that showed me what I once felt myself, I did not know what I had done. What have I done! What have I done!}}

After Pip leaves, Miss Havisham's dress catches on fire from her fireplace. Pip rushes back in and saves her. However, she has suffered severe burns to the front of her torso (she is laid on her back), up to the throat. The last words she speaks in the novel are (in a [[delirium]]) to Pip, referencing both Estella and a note she, Miss Havisham, has given him with her signature: "Take the pencil and write under my name, 'I forgive her!'"

A surgeon dresses her burns, and says that they are "far from hopeless". However, despite rallying for a time, she dies a few weeks later, leaving Estella as her chief beneficiary, and a considerable sum to Herbert Pocket's father, as a result of Pip's reference.

==Claimed prototypes==
{{refimprove|date=January 2014}}

Eliza Emily Donnithorne (1827–1886) of [[Camperdown, New South Wales|Camperdown]], Sydney, was jilted by her groom on her wedding day and spent the rest of her life in a darkened house, her rotting wedding cake left as it was on the table, and with her front door kept permanently ajar in case her groom ever returned. She was widely considered at the time to be Dickens' model for Miss Havisham, although this cannot be proven. Although Charles Dickens had a deep-seated interest in Australia, saw it as a place of opportunity and encouraged two of his sons to emigrate there, the writer never visited it himself, but it features in detail in many of his works, notably ''Great Expectations'' itself. He obtained his information on colonial life in New South Wales from two Sydney researchers. He also had numerous friends and acquaintances who settled in Australia who sent him letters detailing curious aspects of life in the colonies, knowing he could use it as source material for future novels. They could easily have conveyed the Donnithorne story to him. Australia features prominently in ''Great Expectations'', and New South Wales is where Pip’s benefactor Abel Magwitch made his fortune.

In the 1965 Penguin edition, Angus Calder notes at Chapter 8 that "James Payn, a minor novelist, claimed to have given Dickens the idea for Miss Havisham – from a living original of his acquaintance. He declared that Dickens's account was 'not one whit exaggerated'." Although it is documented Dickens encountered a wealthy recluse called Elizabeth Parker on whom it is widely believed he based the character, whilst staying in [[Newport, Shropshire]], at the aptly named Havisham Court.

Charles Dickens may also have known of Eliza Jumel through his connections in America. In old age, many years after Dickens' first tour of the United States, Eliza Jumel suffered from dementia and demonstrated some eccentric behaviors that can be paralleled with Miss Havisham's. Eliza Jumel's adopted grandniece, also named Eliza, might – in this theory – be a model for Estella. Madame Jumel had inherited her wealth from her first husband, a wealthy French merchant in NYC (not a brewer). Her second husband, [[Aaron Burr]], was rumored to have married Eliza Jumel for her money. There is little credible evidence, however, to indicate that Eliza was emotionally distraught by her separation from Burr after a year of marriage. A persistent story that Eliza Jumel once hosted a dinner for Joseph Bonaparte at her New York mansion, that the moldy remains of this dinner were discovered by Dickens at Jumel's mansion decades later, and that Eliza thus became the inspiration for Miss Havisham, is easily discredited. Joseph Bonaparte left America for the last time in 1832. Charles Dickens arrived for his first visit to America in 1842. In the intervening years, Eliza Jumel had not only been married to Aaron Burr, but also rented out her mansion to several tenants while she was traveling. Neither Burr nor her tenants would have tolerated or preserved a perfectly serviceable dining room full of moldy food for a decade. Further, the two weeks of Charles Dickens tour in New York in 1842 overlapped with Eliza Jumel's absence from the city, and it is unlikely that the two ever met. Eliza Jumel died in 1865, and Charles Dickens did not return to the city until 1867.

A passage in E.T.A. Hoffmann's ''The Serapion Brethren'' of 1819–1821 presents a striking parallel and literary prototype. In the section of the book called "A Fragment of the Lives of Three Friends," one Alexander tells his friends about his deceased aunt who waited in vain for her bridegroom on their intended wedding day, and who would annually, on the anniversary of the non-wedding, dress in her wedding-gown, lay out chocolate, wine, and cake on a table, and then spend the day from early morning until 10 at night walking up and down, "sighing and softly lamenting".

==Alternative versions==
''[[Miss Havisham's Fire]]'' (1979, revised 2001) is an [[opera]] composed by [[Dominick Argento]] with a libretto by John Olon-Scrymgeour, based on Dickens' character. The entire story is told in flashback during an inquiry into Miss Havisham’s death. The opera gives her first name as "Aurelia".

Miss Havisham is a major character in the comic detective/mystery series of novels featuring [[Thursday Next]] by [[Jasper Fforde]]. The stories are set in a fantasy/alternate universe milieu, in which characters borrowed from [[Classic book|classic literature]] play a prominent role.

A young version of Miss Havisham is portrayed by [[Tuppence Middleton]] in the 2015 BBC TV series ''[[Dickensian (TV series)|Dickensian]]''. The series gives her the first name Amelia and references the period of her life in the months running up to her wedding. Satis House is relocated to London within the same community as other characters from novels by Dickens.

==In film and television==
In film adaptations of ''Great Expectations'', Miss Havisham has been played by a number of distinguished actresses, including:
*[[Florence Reed]] (1934)
*[[Martita Hunt]] (1946)
*[[Margaret Leighton]] (1974)
*[[Joan Hickson]] (1981)
*[[Jean Simmons]] (who had previously played Estella in 1946 opposite Hunt) (1989)
*[[Anne Bancroft]] (1998) (a version which modernised the story to the twentieth century and changed the names of several characters)
*[[Charlotte Rampling]] (1999)
*[[Gillian Anderson]] (2011, 3-part TV movie adaptation)<ref>{{cite web|url=http://www.bbc.co.uk/programmes/b018wmhr/characters/miss-havisham |title=BBC One – Great Expectations – Miss Havisham |publisher=Bbc.co.uk |date=1 January 1970 |accessdate=14 August 2012}}</ref><ref>{{cite web|last=Osborn |first=Michael |url=http://www.bbc.co.uk/news/entertainment-arts-16047263 |title=BBC News – Great Expectations: Miss Havisham given 'youthful' air |publisher=Bbc.co.uk |date=24 December 2011 |accessdate=14 August 2012}}</ref><ref>{{cite web|author=Gillian Anderson |url=http://www.bbc.co.uk/blogs/tv/2011/12/great-expectations-gillian-anderson.shtml |title=TV blog: Great Expectations: Falling in love with Miss Havisham |publisher=BBC |date= |accessdate=14 August 2012}}</ref>
*[[Helena Bonham Carter]] (2012) (in this version the character is given the first name "Eleanor" but no one addresses her by it)
*[[Tuppence Middleton]] (2015) (in this version the character is given the first name "Amelia" and referenced as such)
*[[Tabu (actress)|Tabu]] (2016) (in ''[[Fitoor]]'', a Hindi version as Begum Hazrat [Miss Havisham] )

==Characters inspired by Miss Havisham==
Both ''[[Sunset Boulevard (film)|Sunset Boulevard]]'' and ''[[What Ever Happened to Baby Jane? (1962 film)|What Ever Happened to Baby Jane?]]'' were inspired by [[David Lean]]'s adaptation of ''[[Great Expectations (1946 film)|Great Expectations]]'', as were, by extension, the characters of [[Norma Desmond]] and [[Baby Jane Hudson]], and their homes.<ref>{{cite web|url=http://www.filmsite.org/suns.html|title=''Sunset Boulevard''|publisher=Filmsite.org|accessdate=14 August 2012}}</ref> In ''Sunset Boulevard'', [[Joe Gillis]] compares Norma Desmond to Miss Havisham during his narration.

A character in the animated series ''[[Chowder (TV series)|Chowder]]'', Endive, has had a similar experience to Miss Havisham in that her fiancé did not show up on their wedding day. Endive also has an apprentice, Panini, whom she teaches to avoid men.

Miss Habersham, a character in [[William Faulkner]]'s novel ''[[Intruder in the Dust]]'', is likely derived from Miss Havisham.{{citation needed|date=April 2016}}

==In science==
The condition of the "Miss Havisham effect" has been coined by scientists to describe a person who suffers a painful longing for lost love, which can become a physically addictive pleasure by activation of reward and pleasure centres in the brain, which have been identified to regulate addictive behaviour – regions commonly known to be responsible for craving and drug, alcohol and gambling addiction.<ref>{{cite journal|url=http://www.sciencedirect.com/science/article/pii/S1053811908006101|title=Craving love? Enduring grief activates brain's reward center | doi=10.1016/j.neuroimage.2008.04.256|volume=42|pages=969–972|pmid=18559294|pmc=2553561|journal=NeuroImage}}</ref><ref>{{cite web|last=Perry|first=Keith|url=http://www.telegraph.co.uk/news/newstopics/howaboutthat/2211085/Pining-for-lost-love-can-be-physically-addictive.html|title=Pining for lost love can be physically addictive|work=Telegraph|date=2008-06-28|accessdate=2014-03-25}}</ref><ref>{{cite web|last=Selway |first=Jennifer |url=http://www.express.co.uk/comment/columnists/jennifer-selway/361836/Charles-Dickens-greatest-heroine |title=Charles Dickens' greatest heroine|work=Daily Express|date=2012-12-01|accessdate=2014-03-25}}</ref>

==References==
{{Reflist|2}}

==External links==
{{Commons}}
*{{IMDb character|0012114|Miss Havisham}}

{{Great Expectations}}

{{DEFAULTSORT:Havisham, Miss}}
[[Category:Fictional characters introduced in 1861]]
[[Category:Great Expectations characters]]
[[Category:Newport, Shropshire]]
[[Category:Fictional hermits]]
[[Category:Fictional English people]]