{{Infobox journal
| title           = Management International Review
| cover           = 
| discipline      = Economy, international management
| editor          = Joachim Wolf, Michael-Jörg Oesterle
| frequency       = 6/year
| publisher       = [[Springer-Verlag]]
| country         = Germany
| language        = English
| website         = http://www.springer.com/business+%26+management/journal/11575
| ISSN            = 0938-8249
}}

'''''Management International Review''''' (''MIR'') is a business and management journal dealing with aspects of international management. Founded in 1960, ''MIR'' published its 50th [[volume (bibliography)|volume]] in 2010. As of 2012, ''MIR'''s [[impact factor]]  was 1.043 and its Google [[H-index|H-Index]] (as of July 2013) was 21. It is published by [[Springer Science+Business Media|Springer-Verlag]].

Since 1960 there have been between four and eight issues per volume, and between six and eight articles per issue. ''MIR'' normally publishes six issues a year, often including select ''focused issues'' (called ''special issues'' before 2005)<ref name="MIR-Oline">[http://www.bwl.uni-kiel.de/mir/ Official homepage of MIR]</ref> focusing on a single aspect of international management.

[[Klaus Macharzina]]<ref>[https://www.uni-hohenheim.de/uploads/media/lebenslauf_macharzina.pdf Vitae of Klaus Macharzinas published by ''Hohenheim University] (german).</ref> served as editor in chief for twenty-five years (1980-2005) and has since been honorary editor.<ref name="MIR-Oline"/> In 2005, his former PhD candidates Joachim Wolf and Michael-Jörg Oesterle succeeded him as editors in chief. From 2005 to 2008, ''MIR'''s editorial office was at [[Christian-Albrechts-Universität zu Kiel|Christian Albrechts University of Kiel]]. Since 2008, it has been at [[Johannes Gutenberg-Universität Mainz|Johannes Gutenberg University of Mainz]]. It rotates every three years.

== References ==
{{Reflist}}

[[Category:English-language magazines]]
[[Category:German business magazines]]
[[Category:Magazines established in 1960]]