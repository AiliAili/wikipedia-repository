[[File:Applesauce cake.jpg|thumb|248px|A slice of applesauce cake with a cinnamon and sugar topping]]

'''Applesauce cake''' is a dessert cake prepared using [[apple sauce]], flour and sugar as primary ingredients. Various spices are typically used, and it tends to be a moist cake. Applesauce cake prepared with chunky-style apple sauce may be less moist. Several additional ingredients may also be used in its preparation, and it is sometimes prepared and served as a [[coffee cake]]. The cake dates back to early [[Colonial history of the United States|colonial times]] in the United States. National Applesauce Cake Day occurs annually on June 6 in the U.S.

== History ==
The preparation of applesauce cake dates back to early [[Colonial history of the United States|colonial times]] in the [[New England Colonies]] of the northeastern United States.<ref name="Ojakangas p. 239" /> From 1900 to the 1950s,<ref name="Knapp Wyeth Bok 1950" /> recipes for applesauce cake frequently appeared in American cookbooks.<ref name="ALK" /> In the United States, [[List of food days#United States|National Applesauce Cake Day]] occurs annually on June 6.<ref name="Eatocracy" />

== Ingredients and preparation ==
[[File:Applesauce cupcakes.jpg|thumb|Applesauce [[cupcake]]s]]

Applesauce cake is a dessert cake prepared using [[apple sauce]], flour and sugar as main ingredients. Store-bought or homemade applesauce may be used in its preparation.<ref name="tribunedigital-baltimoresun 2013" /> Additional ingredients include eggs, butter, margarine or oil, raisins and plumped raisins, dates, chopped apple, chopped nuts (such as walnuts and pecans), cocoa powder, and spices such as cinnamon, cloves, nutmeg and allspice.<ref name="Ojakangas p. 239" /><ref name="San Jose Mercury News 2015" /><ref name="ABC News 2014" /><ref name="Magazine 2008 p. 26" /><ref name="Brass Brass Ryan 2011 p. 30" /><ref name="Rogers Egerton 2004 p. 202" /><ref name="Brownetone" /> Some versions include dried or fresh, finely grated [[ginger]].<ref name="Post-Gazette 2015" /><ref name="King5 2015" /> After baking, applesauce cake is sometimes topped with an [[Icing (food)|icing]],<ref name="ABC News 2014" /> frosting or [[Glaze (cooking technique)|glaze]], such as a caramel glaze.<ref name="Post-Gazette 2015" /><ref name="WRIC 2007" /> It also may be served topped with a dusting of [[Powdered sugar|confectioner's sugar]]<ref name="Lee 2013" /><ref>{{cite web |url=http://www.food.com/recipe/paula-deens-applesauce-cake-356237 |title=Paula Deen's Applesauce Cake |first1=Paula |last1=Deen |authorlink1=Paula Deen |first2=Amy |last2=Zoe |accessdate=September 30, 2015}}</ref> or [[whipped cream]].<ref name="Ross 2013" /> [[Gluten-free]] applesauce cake can be prepared using rice flour.<ref name="Cain 2015 p. 634" />

Applesauce cake tends to be moist due to the liquid content present in the apple sauce.<ref name="Magazine 2008 p. 26" /> However, using a chunky-style apple sauce can result in a cake with less moisture, compared to using standard apple sauce.<ref name="The Courier-Journal 2015" /> Letting it sit for one or two days before serving can increase its flavor,{{efn|"Like most cakes of this type, it tastes even better when allowed to sit a day or two, and it will keep well and stay moist for up to a week when wrapped properly."<ref name="tribunedigital-baltimoresun 2013" />}}{{efn|"... the cake is better after the first day, and keeps well."<ref name="Beard 2009" />}} as this allows time for the ingredients to intermingle within the cake.

It can be prepared using various types of [[cake pan]]s, such as a ring-shaped [[bundt cake]] using a bundt pan,<ref name="HeraldCourier" /><ref name="Traverso 2011" /> in loaf form using a loaf pan, or as a [[sheet cake]] with a sheet cake pan.<ref name="Beard 2009" /> Applesauce cake is sometimes prepared in the form of [[cupcake]]s.<ref name="King5 2015" /><ref name="WRIC 2007" />

== Variations ==
Applesauce cake may be prepared and served as a type of [[coffee cake]],<ref name="Platt 2012" /> which may include a sweet crumb topping.<ref name="San Jose Mercury News 2013" /> Simple versions may be prepared using prepared coffee cake mix, apple sauce, and other various ingredients.<ref name="Brownetone" /> Fruits such as blueberries, cranberries and raisins may also be used in applesauce coffee cake.<ref name="Ettinger 2006" /><ref name="Ridgaway 2011" /><ref name="Kendall 2005" />

<gallery class="center" caption="" widths="215px" heights="145px">
File:Applesauce walnut coffee cake.jpg|A close-up view of applesauce walnut [[coffee cake]]
File:Applesauce cupcakes with icing.jpg|Applesauce cupcakes with icing
</gallery>

== See also ==
{{portal|Food}}
{{div col|colwidth=30em}}
* [[Apple cake]]
* [[Apple pie]]
* [[List of apple dishes]]
* [[List of cakes]]
* [[List of desserts]]
{{div col end}}

== Notes ==
{{notelist}}

== References ==
{{reflist|30em|refs=
<ref name="San Jose Mercury News 2015">{{cite news | title=Recipe: Applesauce Cake |first1=Pat |last1=Gerard| newspaper=[[San Jose Mercury News]] | date=February 25, 2015 | url=http://www.mercurynews.com/eat-drink-play/ci_27566313/recipe-applesauce-cake | accessdate=September 28, 2015}}</ref>
<ref name="ABC News 2014">{{cite web |first1=Faith |last1=Ford | title=Applesauce Cake with Caramel Icing |publisher=[[ABC News]] |work=[[Good Morning America]] | date=August 10, 2014 | url=http://abcnews.go.com/GMA/recipe?id=7056627 | accessdate=September 28, 2015}}</ref>
<ref name="Magazine 2008 p. 26">{{cite book |journal=[[Country Living]]| title=Great Cakes: Home-baked Creations from the Country Living Kitchen | publisher=Hearst Books | series=Country Living Series | year=2008 | isbn=978-1-58816-686-9 | url=https://books.google.com/books?id=aS0hbMtD9DkC&pg=PA26 | page=26}}</ref>
<ref name="Cain 2015 p. 634">{{cite book | last=Cain | first=N. | title=Against the Grain: Extraordinary Gluten-Free Recipes Made from Real, All-Natural Ingredients | publisher=Potter/TenSpeed/Harmony | year=2015 | isbn=978-0-385-34556-9 | url=https://books.google.com/books?id=3UVABAAAQBAJ&pg=PT634 | page=634}}</ref>
<ref name="Brass Brass Ryan 2011 p. 30">{{cite book | last=Brass | first=M. | last2=Brass | first2=S. | last3=Ryan | first3=A. | title=Heirloom Baking with the Brass Sisters: More Than 100 Years of Recipes Discovered and Collected by the Queens of Comfort Food | publisher=Hachette Books | year=2011 | isbn=978-1-57912-881-4 | url=https://books.google.com/books?id=yPnH8XkiYckC&pg=PA30 | page=30}}</ref>
<ref name="Ojakangas p. 239">{{cite book | last=Ojakangas | first=Beatrice A. | title=Great Old-Fashioned American Desserts | publisher=[[University of Minnesota Press]] | isbn=978-1-4529-0711-6 | url=https://books.google.com/books?id=N71EjBL2hmMC&pg=PA239 | page=239}}</ref>
<ref name="Rogers Egerton 2004 p. 202">{{cite book | last=Rogers | first=A.T. | last2=Egerton | first2=J. | title=Hungry for Home: Stories of Food from Across the Carolinas with More Than 200 Favorite Recipes | publisher=John F. Blair | year=2004 | isbn=978-0-89587-301-9 | url=https://books.google.com/books?id=k2dvkct5rm0C&pg=PA202| pages=202–203}}</ref>
<ref name="Eatocracy">{{cite web |first1=Emily |last1=Smith |publisher=[[CNN]]| title=National applesauce cake day | website=National applesauce cake day – Eatocracy | date=June 6, 2013 | url=http://eatocracy.cnn.com/2013/06/06/national-applesauce-cake-day/ | ref={{sfnref | National applesauce cake day – Eatocracy | 2013}} | accessdate=September 28, 2015}}</ref>
<ref name="San Jose Mercury News 2013">{{cite web |author2=[[Martha Stewart]]'s Cakes' |last1=Clarkson |first1=Potter | title=Recipe: Applesauce Coffee Cake | newspaper=[[San Jose Mercury News]] | date=September 24, 2013 | url=http://www.mercurynews.com/recipes/ci_24141121/recipe-applesauce-coffee-cake | accessdate=September 28, 2015}}</ref>
<ref name="Brownetone">{{cite news | url=https://news.google.com/newspapers?nid=1338&dat=19691010&id=omtYAAAAIBAJ&sjid=J_gDAAAAIBAJ&pg=3327,3001199&hl=en | title=Cooking Is Fun | date=October 10, 1969 | agency=''[[Spokane Daily Chronicle]]'' | accessdate=28 September 2015 | author=Brownetone, Cecily}}</ref>
<ref name="Post-Gazette 2015">{{cite web | first1=Gretchen |last1=McKay | title=Plate: Pittsburgh food, recipes, restaurants, drinks | date=September 26, 2015 | url=http://www.post-gazette.com/life/let-s-eat/2015/09/26/Let-s-eat-Gingered-Applesauce-Cake-Glazed-with-Caramel/stories/201509260024 |newspaper=[[Pittsburgh Post-Gazette]] | accessdate=September 28, 2015}}</ref>
<ref name="Lee 2013">{{cite web | last=Lee | first=Sandra | title=Applesauce Cake: Sweet Dreams: Food Network | website=[[Food Network]] | date=November 2, 2013 | url=http://www.foodnetwork.com/recipes/applesauce-cake-recipe.html | accessdate=September 28, 2015}}</ref>
<ref name="Ross 2013">{{cite web | last=Ross | first=Alicia | title=Betty's Applesauce Cake is a treasured recipe to one reader |newspaper=[[The Dallas Morning News]] | date=April 9, 2013 | url=http://www.dallasnews.com/newsletters/recipe-of-the-day/20130409-betty-s-applesauce-cake-is-a-treasured-recipe-to-one-reader.ece | accessdate=September 30, 2015}}</ref>
<ref name="tribunedigital-baltimoresun 2013">{{cite news | title=Applesauce spice cake |first1=Julie |last1=Rothman |newspaper=[[The Baltimore Sun]] | date=November 8, 2013 | url=http://articles.baltimoresun.com/2013-11-08/news/bs-fo-recipe-finder-spice-cake-1113-20131108_1_spice-cake-applesauce-cake-recipe-finder | accessdate=September 30, 2015}}</ref>
<ref name="HeraldCourier">{{cite web | title=Applesauce Cake with Caramel Icing | date=September 24, 2015 | url=http://www.heraldcourier.com/community/flavor/applesauce-cake-with-caramel-icing/article_0cb8140e-62ed-11e5-8d34-d3d48f28fbe0.html |newspaper=[[Bristol Herald Courier]] HeraldCourier.com| accessdate=September 30, 2015}}</ref>
<ref name="The Courier-Journal 2015">{{cite news |first1=M. Pamela |last1=Magers| title=The Best  –  Chunky applesauce |newspaper=[[The Courier-Journal]] | date=May 1, 2015 | url=http://www.courier-journal.com/story/life/shopping/2015/05/01/best-chunky-applesauce/26695699/ | accessdate=September 30, 2015}}</ref>
<ref name="Platt 2012">{{cite web | last=Platt | first=Heather | title=Peter Reinhart's Upcoming Gluten-Free, Sugar-Free Baking Book | website=[[LA Weekly]] | date=August 2, 2012 | url=http://www.laweekly.com/restaurants/peter-reinharts-upcoming-gluten-free-sugar-free-baking-book-2378570 | accessdate=March 8, 2016}}</ref>
<ref name="Ettinger 2006">{{cite book | last=Ettinger | first=J. | title=Bob's Red Mill Baking Book | publisher=Running Press | year=2006 | isbn=978-0-7867-5231-7 | url=https://books.google.com/books?id=X5K3xIV8NUUC&pg=PA113 | page=113}}</ref>
<ref name="King5 2015">{{cite web | title=Spicy Gingerbread Applesauce Cupcakes | publisher=[[KING-TV]] | author=Kerbs, Nicki | date=December 22, 2015 | url=http://www.king5.com/story/news/health/2015/12/22/spicy-gingerbread-applesauce-cupcakes/77783582/ | accessdate=March 8, 2016}}</ref>
<ref name="WRIC 2007">{{cite web | title=Applesauce Cupcakes with Caramel Frosting | publisher=[[WRIC-TV]] | date=February 14, 2007 | url=http://wric.com/2007/02/13/applesauce-cupcakes-with-caramel-frosting/ | accessdate=March 8, 2016}}</ref>
<ref name="Traverso 2011">{{cite book | last=Traverso | first=A. | title=The Apple Lover's Cookbook | publisher=W. W. Norton | year=2011 | isbn=978-0-393-06599-2 | url=https://books.google.com/books?id=NZjiAAAAQBAJ&pg=PA255 | page=255}}</ref>
<ref name="Beard 2009">{{cite book | last=Beard | first=J. | title=James Beard's American Cookery | publisher=Little, Brown | year=2009 | isbn=978-0-316-06981-6 | url=https://books.google.com/books?id=MHbSW8MylhkC&pg=PT1648&dq=applesauce+cake,+loaf+pan" | accessdate=March 8, 2016 | page=pt163–164}}</ref>
<ref name="Ridgaway 2011">{{cite book | last=Ridgaway | first=D. | title=The Gourmet's Guide to Cooking with Liquors and Spirits: Extraordinary Recipes Made with Vodka, Rum, Whiskey, and More! | publisher=Quarry Books | year=2011 | isbn=978-1-61058-101-1 | url=https://books.google.com/books?id=aRBWjELuQkcC&pg=PA54 | page=54}}</ref>
<ref name="ALK">{{cite web | url=http://auntlilskitchen.com/breads/world-war-ii-applesauce-cake | title=Recipes from 1900–1950 | publisher=Aunt Lil's Kitchen | accessdate=8 March 2016}}</ref>
<ref name="Knapp Wyeth Bok 1950">{{cite book | last=Knapp | first=L. | last2=Wyeth | first2=N.C. | last3=Bok | first3=E.W. | title=Ladies' Home Journal | publisher=LHJ Publishing, Incorporated | issue=v. 67 | year=1950 | url=https://books.google.com/books?id=ldgcAQAAMAAJ | page=124}}</ref>
<ref name="Kendall 2005">{{cite book | last=Kendall | first=P. | title=High Altitude Baking: 200 Delicious Recipes & Tips for Perfect High Altitude Cookies, Cakes, Breads & More | publisher=3D PressINC | year=2005 | isbn=978-1-889593-15-9 | url=https://books.google.com/books?id=XVCvWxhTLuEC&pg=PA68 | page=68}}</ref>
}}

== Further reading ==
{{Commons}}
* {{cite book | last=Fisher | first=Carol |first2=John C. |last2=Fisher |location=Columbia, Missouri | title=Pot Roast, Politics, and Ants in the Pantry: Missouri's Cookbook Heritage | publisher=[[University of Missouri Press]] | year=2008 | isbn=978-0-8262-6634-7 | url=https://books.google.com/books?id=CC2Qm_yCDVUC&pg=PA81 | page=81}}
* {{cite web  |first1=Melba |last1=Lovelace  | title=Eggless Applesauce Cake Can Become a Fruit Cake, Too | website=NewsOK.com | date=March 1, 1990 | url=http://newsok.com/eggless-applesauce-cake-can-become-a-fruit-cake-too/article/2308808 | accessdate=September 30, 2015}}

{{Cakes}}
{{Good article}}

[[Category:Apple products]]
[[Category:Cakes]]