'''GraphiCon''' is the largest International conference on [[computer graphics]] and [[computer vision]] in the countries of the former [[Soviet Union]].

The conference is hosted by [[Moscow State University]] in association with [[Keldysh Institute of Applied Mathematics]], [[Russian Center of Computing for Physics and Technology]], and the Russian Computer Graphics Society. The Conference is held in close cooperation with [[Eurographics|Eurographics Association]].

== Conference topics ==

The main topics of the conference include (this list is not exhaustive):

* [[Computer Graphics|Graphics]] and [[multimedia]]:
** [[Geometric modeling|Geometry modeling]] and [[geometry processing|processing]]
** [[Photorealistic]] [[rendering (computer graphics)|rendering]] techniques
** [[Scientific visualization]]
** [[Image-based modeling and rendering|Image-based techniques]]
** Computer graphics for [[mobile device]]s
** [[Graphics hardware|Computer graphics hardware]]
** Graphics in [[computer game]]s
** [[Computer animation|Animation]] and [[computer simulation|simulation]]
** [[Virtual reality|Virtual]] and [[augmented reality]]
* [[Image processing|Image]] and [[video processing]]:
** [[Medical image processing]]
** [[Feature extraction#Low-level|Early vision]] and [[Computer graphics#Image types|image representation]]
** [[Video tracking|Tracking]] and [[video surveillance|surveillance]]
** [[Segmentation (image processing)|Segmentation]] and [[data clustering|grouping]]
** [[Image enhancement]], [[photo restoration|restoration]] and [[super-resolution]]
* [[Computer vision]]:
** [[3D reconstruction]] and acquisition
** [[Pattern recognition|Object localization and recognition]]
** [[Multi-Sensor Data Fusion|Multi-sensor fusion]] and 3D registration
** [[Structure from motion]] and [[Computer stereo vision|stereo]]
** Scene modeling
** [[Statistical methods]] and [[Machine learning|learning]]
* Applications

== Format ==

The following sections are organized for the event:
* [[Summer school|Young scientists school]] courses and [[master class]]es
* [[Academic paper|Full paper]] presentations
* [[Work in progress#General|Work in progress]] presentations
* [[State of the art|STAR]] reports
* Invited talks
* Industrial presentations
* [[Multimedia]] shows
* [[Round table (discussion)|Round table discussions]]

== Specific GraphiCon conferences ==

{| class="wikitable"
|-
!Year
!Location
!Links
!Notes
|-
|1998
|[[Moscow]]
|[http://www.graphicon.ru/1998/ website], [http://www.graphicon.ru/1998/proceedings/ papers]
|
|-
|1999
|[[Moscow]]
|[http://www.graphicon.ru/1999/ website], [http://www.graphicon.ru/1999/proceedings/ papers]
|
|-
|2000
|[[Moscow]]
|[http://www.graphicon.ru/2000/ website], [http://www.graphicon.ru/2000/proceedings/ papers]
|
|-
|2001
|[[Nizhny Novgorod]]
|[http://www.graphicon.ru/2001/ website], [http://www.graphicon.ru/2001/proceedings/ papers]
|
|-
|2002
|[[Nizhny Novgorod]]
|[http://www.graphicon.ru/2002/ website], [http://www.graphicon.ru/2002/proceedings/ papers]
|
|-
|2003
|[[Moscow]]
|[http://www.graphicon.ru/2003/ website], [http://www.graphicon.ru/2003/proceedings/ papers]
|
|-
|2004
|[[Moscow]]
|[http://www.graphicon.ru/2004/ website], [http://www.graphicon.ru/2004/proceedings/ papers]
|
|-
|2005
|[[Novosibirsk]]
|[http://www.graphicon.ru/2005/ website], [http://www.graphicon.ru/2005/proceedings/ papers] 
|
|-
|2006
|[[Novosibirsk]]
|[http://www.graphicon.ru/2006/ website], [http://www.graphicon.ru/2006/proceedings/ papers]
|
|-
|2007
|[[Moscow]]
|[http://www.graphicon.ru/2007/ website], [http://www.graphicon.ru/2007/proceedings/ papers]
|
|-
|2008
|[[Moscow]]
|[http://www.graphicon.ru/2008/ website], [http://www.graphicon.ru/2008/proceedings/ papers]
|
|-
|2009
|[[Moscow]]
| [http://gc2009.graphicon.ru/ website], [http://www.graphicon.ru/proceedings/2009 papers]
|
|-
|2010
|[[Saint-Petersburg]]
| [http://gc2010.graphicon.ru/ website], [http://www.graphicon.ru/proceedings/2010 papers]
|
|-
|2011
|[[Moscow]]
| [http://gc2011.graphicon.ru/ website], [http://www.graphicon.ru/proceedings/2011 papers]
|
|-
|2012
|[[Moscow]]
| [http://gc2012.graphicon.ru/ website], [http://www.graphicon.ru/proceedings/2012 papers]
|
|-
|2013
|[[Vladivostok]]
| [http://2013.graphicon.ru/ website]
|
|}

== See also ==

* [[Eurographics]] — the biggest conference on computer graphics in Europe
* [[SIGGRAPH]] — the world biggest conference on computer graphics
* [[List of computer science conferences#Computer_graphics]]

== External links ==
* http://www.graphicon.ru/

[[Category:Computer vision research infrastructure]]
[[Category:Computer graphics conferences]]


{{compu-graphics-stub}}