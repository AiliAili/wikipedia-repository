{{Infobox journal
| title = The Review of Financial Studies
| cover = 
| editor = Andrew Karolyi
| discipline = [[Finance]]
| former_names = 
| abbreviation = Rev. Financ. Stud.
| publisher = [[Oxford University Press]] on behalf of the [[Society for Financial Studies]]
| country =
| frequency = Monthly
| history = 1988-present
| openaccess = 
| license = 
| impact = 3.119
| impact-year = 2015
| website = http://rfs.oxfordjournals.org/
| link1 = http://www.oxfordjournals.org/our_journals/revfin/content/current
| link1-name = Online access
| link2 = http://rfs.oxfordjournals.org/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 40777420
| LCCN = sf93094251
| CODEN = 
| ISSN = 0893-9454
| eISSN = 1465-7368
}}
'''''The Review of Financial Studies''''' is a [[peer-reviewed]] [[academic journal]] covering the field of [[finance]]. It is published by [[Oxford University Press]] on behalf of the [[Society for Financial Studies]]. It was established following discussions at the 1986 [[Western Finance Association]] meetings, and the first issue was published in 1988.<ref>{{cite web |url=http://www.sfsrfs.org/history.php |title=History of the RFS}}</ref> The current [[editor-in-chief]] is Andrew Karolyi. It is considered to be one of the premier finance journals.<ref>{{Cite journal |last=Oltheten |first=Elisabeth |last2=Theoharakis |first2=Vasilis |last3=Travlos |first3=Nickolaos G. |date=2005-03-01 |title=Faculty Perceptions and Readership Patterns of Finance Journals: A Global View  |journal=Journal of Financial and Quantitative Analysis |volume=40 |issue=1 |pages=223–239 |doi=10.1017/S0022109000001800}}</ref><ref>{{Cite journal |last=Borokhovich |first=Kenneth A. |last2=Bricker |first2=Robert J. |last3=Simkins |first3=Betty J. |date=1994-06-01 |title=Journal Communication and Influence in Financial Research |journal=The Journal of Finance |volume=49 |issue=2 |pages=713–725 |doi=10.1111/j.1540-6261.1994.tb05159.x}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.119.<ref name=WoS>{{cite book |year=2016 |chapter=The Review of Financial Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== Editors ==
The following persons are or have been editors-in-chief:
* Andrew Karolyi (Cornell University, 2014–present)
* [[David Hirshleifer]] (UC Irvine, 2011–2014)
* Matthew Spiegel (Yale University, 2005–2011)
* Maureen O'Hara (Cornell University, 2000–2005)
* [[Ravi Jagannathan]] (Northwestern University, 1997–2000)
* [[Franklin Allen]] (University of Pennsylvania, 1994–1997)
* Chester Spatt (Carnegie Mellon University, 1991–1994)
* [[Michael Brennan (finance)|Michael Brennan]] (UCLA; 1988–1990)

==Awards==
The Michael J. Brennan Best Paper Award, named after the first editor of the journal, is an annual prize given to authors of the best paper published in the journal in the past year. The first prize is [[U.S. Dollar|US$]]20,000 and the second prize is US$7,000.<ref name=BGIMBA>{{cite web |url=http://www.oxfordjournals.org/our_journals/revfin/awards_michaelbrennan.html |accessdate=2014-08-10 |publisher=Oxford University Press |title=Awards}}</ref> The journal also awards a Rising Scholar Award, a Referee of the Year Award, and multiple Distinguished Referee Awards. The winners are announced at the Society for Financial Studies Cavalcade conference in May as well as in the journal and on its website. The winners are selected by the editorial board, including the executive editor, the co-editors, and the associate editors.

==References==
{{reflist}}

==External links==
* {{Official website|http://www.sfsrfs.org/}}

{{DEFAULTSORT:Review Of Financial Studies, The}}
[[Category:Finance journals]]
[[Category:Publications established in 1988]]
[[Category:English-language journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Monthly journals]]
[[Category:Academic journals associated with learned and professional societies]]