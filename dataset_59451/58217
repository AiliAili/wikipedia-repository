{{primary sources|date=January 2015}}
{{third-party|date=January 2015}}
{{Italic title}}
The '''''Journal of Interactive Advertising''''' is a biannual [[peer-reviewed]] [[academic journal]] covering [[interactive advertising]], [[marketing]], and communication. It is published by [[Routledge]] on behalf of the [[American Academy of Advertising]].<ref>{{cite web |url=http://www.aaasite.org/page-1557243 |title=Journal of Interactive Advertising |publisher=American Academy of Advertising}}</ref> It was established in Fall 2000 by John D. Leckenby ([[University of Texas at Austin]]) and Hairong Li ([[Michigan State University]]). The American Academy of Advertising acquired the journal on January 1, 2008.

== History ==
The journal was established in Fall 2000 by John D. Leckenby (University of Texas at Austin) and Hairong Li (Michigan State University), who served as founding [[editors-in-chief]]. It was an official publication of the Department of Advertising, Public Relations, and Retailing at Michigan State University and the Department of Advertising at The University of Texas at Austin until December 31, 2007. The American Academy of Advertising became its publisher on January 1, 2008, when John D. Leckenby became past editor and Hairong Li became sole editor. In Fall 2013, [[Routlege]] took over publishing.

== Editors-in-chief ==
The following persons have been [[editor-in-chief]]:<ref>{{cite web |url=http://jiad.org/estaff.html |title=Editorial Staff - JIAD |work=Journal of Interactive Advertising}}</ref>
* John D. Leckenby (2000-2007)
* Hairong Li (2000-)
* Steve Edwards 
* Terry Daugherty (current)

== Abstracting and indexing ==
The journal is abstracted and indexed in [[EBSCOhost]].

== References ==
{{reflist}}

== External links ==
* {{Official website|http://jiad.org/index.html}}
* [http://www.aaasite.org/ American Academy of Advertising]

[[Category:Business and management journals]]
[[Category:Publications established in 2000]]
[[Category:Biannual journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]
[[Category:Communication journals]]