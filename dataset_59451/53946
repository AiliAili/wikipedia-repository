'''Pagans in Recovery''' ('''PIR''') is the phrase which is frequently used to describe the collective efforts of [[Neopagan]]s to achieve abstinence or the remission of compulsive/addictive behaviors through [[twelve-step program]]s such as [[Alcoholics Anonymous]], [[Narcotics Anonymous]],  [[Overeaters Anonymous]], [[Al-Anon/Alateen]], etc. These efforts generally focus on modifying or adapting the twelve steps to accommodate the Pagan world-view as well as creating Pagan-friendly twelve step meetings either as part of a pre-existing twelve-step program, or as independent entities.

==History and development==
The term 'Pagans in Recovery' appears to have first been used in a Neopagan newsletter from [[Ohio]] prior to 1989 which was titled "Pagans in Recovery".<ref>http://www.sacred-texts.com/bos/bos158.htm</ref> Isaac Bonewits also used the term in an essay he wrote in 1996.<ref>http://www.neopagan.net/PIR.html</ref>

==Why separate from regular [[12-step|twelve-step]] meetings?==
Many [[paganism|Pagans]] are uncomfortable with traditional twelve-step meetings because of the use of Christian prayers, the difficulty in finding supportive sponsors, the assumption that a person's Higher Power is male, etc.<ref>http://www.witchvox.com/va/dt_va.html?a=usxx&c=words&id=4583</ref> Some Pagans find the 12 steps themselves too reminiscent of Christian theology to be applicable to their belief systems.<ref>http://www.angelfire.com/wi/theosis/aaspirit.html</ref> Pagans have been "ousted from A.A. meetings or shunned" when members of that A.A. group discovered that they were Pagans.  However this type of conduct would not be approved by A.A. itself as any person who "has a desire to stop drinking" may declare themselves a member of A.A.<ref>http://web.uni-marburg.de/religionswissenschaft/journal/diskus/foltz.html</ref>

The focus in alternative groups tends to be on tolerance, balance, building better boundaries, healing old wounds, making amends, taking our power back and right action. As a result, these groups tend to be more in tune with [[Pagan]], [[New Age]], [[Indigenous peoples of the Americas|Native American]], [[humanistic]], [[feminist]], and [[Buddhist]] teachings, as well as with the more progressive versions of the mainstream faiths.
  
Another issue among Pagans in recovery is the one-sided image of addicts, alcoholics, codependents, and survivors of [[dysfunctional families]] portrayed in official 12 Step literature and in the many books published on recovery and dysfunctional family systems since the 1980s.  For example, Kasl and others in the field of addiction have long noted that the classic "Characteristics of Adult Children of Alcoholics"<ref>http://www.drjan.com/13char.html</ref> and the list known as "The Problem" in ACA<ref>http://www.adultchildren.org/lit/Problem.s</ref> (which are read at every ACA meeting) focus strongly on "character defects" and do not adequately support the creation or celebration of character strengths, strengths which are often the result of surviving these very systems.

Many twelve-step programs, such as [[Narcotics Anonymous]], have special interest groups, typically meetings specifically geared towards young people, men, women, gays and lesbians, etc.<ref>http://www.na.org/naway/en/naway4-e-4.htm</ref> Alcoholics Anonymous has also started meetings specifically for [[Indigenous peoples of the Americas|Native American]]s which accommodate the Native American view of spirituality.<ref>http://www.naigso-aa.org/</ref> Pagans who are recovering alcoholics have started A.A. meetings specifically for Pagans<ref>http://news.earthhousemn.org/index.php?name=News&file=article&sid=1</ref>

==What is different about recovery programs for Pagans?==
Many Pagans seem to prefer a mutually supportive, spiritually based twelve step approach to recovery<ref>http://health.ivillage.com/mentalhealth/mhaddiction/0,,760v,00.html</ref> over non-spiritually based programs such as [[Secular Organizations for Sobriety]], where one is expected to keep his or her spiritual beliefs separate from recovery,<ref>http://www.cfiwest.org/sos/find.htm</ref> or [[Rational Recovery]], which is not spiritually based and does not encourage members to seek support from others in recovery.<ref>http://www.bookrags.com/other/drugs/rational-recovery-rr-edaa-03.html</ref> Generally speaking, Pagan twelve step meetings follow the same format as other twelve step meetings except that they use Pagan friendly readings (which have not been approved by the General Service Office of Alcoholics Anonymous or other twelve step organizations), and substitute Pagan friendly prayers for the [[Lord's Prayer]] (which is not AA Conference approved, but widely used) and the [[Serenity Prayer]].<ref>http://www.jelder.com/Pagan/11SSC_protocol.PDF</ref> For example, the ''Recovery Spiral: A Pagan Path to Healing'' by Cynthia Jane Collins  is sometimes used instead of or along with the [[The Big Book (Alcoholics Anonymous)|Big Book]] of Alcoholics Anonymous, and the Native American Great Spirit prayer may be substituted for the Lord's Prayer.<ref>http://www.rewritables.net/cybriety/recovery_prayers.htm</ref>

Some Pagan twelve step groups have reworked or reworded the twelve steps so as to make them more applicable to Pagans, especially in allowing for a [[Polytheistic]] and non-gendered view of divinity.<ref>http://www.recovery.org/pagan.htm</ref> The members of Pagan twelve step groups are still expected to work the twelve steps as a means of spiritual growth, obtain a sponsor, make amends for harm they have caused, and to help others.<ref>http://www.sacredcenters.com/articles/recovery.html</ref><ref>http://www.widdershins.org/vol1iss2/11.htm</ref>

Some twelve step meetings for Pagans are eclectic, meaning that anyone from a twelve step recovery program, regardless of the nature of their addiction, may participate in the meeting. This is in sharp contrast to Alcoholics Anonymous' concept of "Singleness of Purpose" which holds that alcoholics should only work with other alcoholics.<ref>http://www.aaprimarypurpose.org/SinglePurpose.htm</ref>

==See also==
* [[Alcoholics Anonymous#United States Court rulings]]

==References==
{{reflist}}

==External links==
*[http://www.pagansinrecovery.com/ Pagans in Recovery Bulletin Board Meeting]
*[http://paganinstitute.org/PI/recovery.html Nine Step Pagans]
*[http://spiralsteps.org/ The Spiral Steps]
*[http://www.therecoverygroup.org/odat/pagan/ Pagan ODAT group for overeaters]
*[http://www.candledark.net/silver/13-steps.html a Wiccan 13 step path]
*[http://newbeginningspaganrecovery.weebly.com/ New Beginnings Pagan Recovery Support Group - Austin, TX]
{{paganism}}

[[Category:Neopagan beliefs and practices|Recovery]]
[[Category:1989 introductions]]
[[Category:Twelve-step programs]]
[[Category:Addiction and substance abuse organizations]]