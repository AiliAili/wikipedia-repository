<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Michelin Cup Biplane 
|image=File:Cody Aircraft Mark II RAE-O114.jpg 
|caption=
}}{{Infobox Aircraft Type
 |type=[[Experimental aircraft]]
 |manufacturer=S. F. Cody
 |designer=S. F. Cody
 |first flight=April 1910
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=1
 |variants with their own articles=
}}
|}
The '''Cody Michelin Cup Biplane''' was an [[experimental aircraft]] designed and built in Britain during 1910 by [[Samuel Franklin Cody]], a prominent showman and aviation pioneer. Cody had worked with the [[British Army]] on experiments with man-lifting kites and in October 1908 had successfully built and flown the [[British Army Aeroplane No 1]], making the first officially verified powered flight in the United Kingdom. Cody broke the existing endurance record twice in the aircraft, the second flight, made on 31 December 1910, winning him the [[Michelin Cup]] for the longest-lasting flight made over a closed circuit in the United Kingdom before the end of the year.<ref>Lewis 1962 p. 192</ref>

==Background==
In 1910 there were a number of prizes on offer, offering both prestige and in some cases large sums of money. Among them were the £4,000 [[Baron de Forest]] prize for the longest all-British flight to a destination in mainland Europe, the Michelin Cup and £500 endurance prize for the longest flight observed over a closed circuit and £10,000 offered by the ''[[Daily Mail]]'' newspaper for a flight between London and [[Manchester]].<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200362.html "Daily Mail" Flight Prizes] [[Flight International|''Flight'']] 19 June 1909</ref>

==Design==
[[File:Cody Aircraft Mark II RAE-O42.jpg|thumbnail]]
After his success with the Army Aeroplane Cody started work on a new design for 1910.  This was a three-bay biplane of similar design.  A large elevator, divided into two sections at the centre, was carried in front of the wing on three sets of  booms, one at each end and the third at the centre.<ref name="Lewis191">Lewis 1962 p.191</ref> A rectangular rudder was carried on two booms extending aft, the lower attached to the apex of inverted V struts below the engine bearers, which also supported the long skid projecting back from below the lower wings's [[leading edge]]. Lateral control was by [[aileron]]s mounted on the outermost [[interplane strut]]s of each wing.  The [[Elevator (aircraft)|elevators]] were operated by a pair of bamboo push-rods, leading to control horns mounted at the centre of each half of the elevator. It had a [[tricycle undercarriage]], augmented by a long skid projecting back behind the wing [[trailing edge]] and small wheels  mounted on the wingtips.  The aircraft was initially powered by a {{convert|60|hp|kW|0|abbr=on}} Green water-cooled engine mounted on the lower wing using a chain to drive a single two-bladed pusher propeller mounted on a shaft halfway between the wings.<ref name="Lewis191" />

==Operational history==
The aircraft was flying by June 1910. On 7 June Cody was awarded his Aero Club certificate, the seventh issued,<ref>[http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200449.html Official Notices to Members: Aviator's Certificates], [[Flight Magazine]] 11 June 1910</ref> having made his qualifying flights at Laffan's Plain, Farnborough.  After a crash, which kept Cody in bed for a few days in June, the Green powerplant was replaced by a {{convert|60|hp|kW|0|abbr=on}} [[E.N.V. Motor Syndicate#90° V-8 engines|E.N.V. Type F]] engine. A fixed horizontal surface was added to the rudder and the ailerons were moved inwards and back, so that they were between the rear struts and the outer bay of each wing.<ref name="Lewis191" />

On 21 July, Cody stayed in the air for 2 hours 24 minutes, covering a distance of {{convert|94.5|mi|km|0|abbr=on}} miles over a closed circuit at Laffan's Plain, setting a new British endurance record. This record was beaten by [[Thomas Sopwith]] in November, but on the last day of the year Cody managed to stay aloft for 4 hours 47 minutes at Laffans Plain,<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200007.html Mr Sopwith's Final Try]</ref> so winning the Michelin Cup and a prize of £500.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200006.html The British Michelin Cup] [[Flight magazine]] 7 January 1911</ref>

Cody continued flying the aircraft in 1911 while working on a new aircraft to compete in the [[Daily Mail Circuit of Britain Air Race]], in January making a remarkable flight carrying three passengers, one of whom had to stand on the wing.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200048.html Laffan'sPlain], [[Flight International|Flight magazine]], 21 January 1911</ref> In March 1911 the aircraft was displayed at the 1911 Aero Show at Olympia.

==Specifications==
{{Aircraft specs
|ref=Lewis<!-- for giving the reference for the data -->
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=1
|length m=
|length ft=38
|length in=6
|length note=
|span m=
|span ft=46
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=540
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=2,200
|empty weight note=
|gross weight kg=
|gross weight lb=2,950
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Green D.4]]
|eng1 type=4-cyl water-cooled in-line piston engine
|eng1 kw=45<!-- prop engines -->
|eng1 hp=60<!-- prop engines -->
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=65
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=

*[[British Army Aeroplane No 1]]
*[[Cody Circuit of Britain Biplane]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
*Lewis, P ''British Aircraft 1809–1914''.London, Putnam and Co, 1962

<!-- ==External links== -->
{{Cody aircraft}}

[[Category:Cody aircraft|Michelin]]
[[Category:British experimental aircraft 1900–1909]]
[[Category:Single-engined pusher aircraft]]
[[Category:Biplanes]]
[[Category:Canard aircraft]]