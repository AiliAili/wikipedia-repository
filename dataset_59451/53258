{{Infobox NCAA team season
|Mode = football
|Year = 2008
|Team = North Carolina Tar Heels
|Image = University of North Carolina Tarheels Interlocking NC logo.svg
|ImageSize = 100
|Conference = Atlantic Coast Conference
|Division = Coastal
|ShortConference = ACC
|BCSRank =
|CoachRank =
|APRank =
|Record=0–5
|ConfRecord=0–4
|WinsVacated=8<ref name="vacate">North Carolina has vacated all wins from the 2008 and 2009 football seasons: {{cite news|url=http://grfx.cstv.com/photos/schools/unc/sports/m-footbl/auto_pdf/2011-12/misc_non_event/NOAresponse091911.pdf|title=North Carolina Response to Notice of NCAA Allegations|date=September 19, 2011|accessdate=September 19, 2011}}</ref>
|ConfWinsVacated=4
|HeadCoach = [[Butch Davis]]
|OffCoach = [[John Shoop]]
|DefCoach = [[Everett Withers]]
|OScheme = [[Pro-style offense|Pro-style]]
|DScheme =  [[4–3 defense|4–3]]
|StadiumArena = [[Kenan Memorial Stadium]]<br>(Capacity: 60,000)
|Champion = 
|BowlTourneyResult = L 30–31
|BowlTourney = [[2008 Meineke Car Care Bowl|Meineke Car Care Bowl]] vs. [[2008 West Virginia Mountaineers football team|West Virginia]]
}}
{{2008 ACC football standings}}
The '''2008 North Carolina Tar Heels football team''' represented the [[University of North Carolina at Chapel Hill|University of North Carolina]] in the [[2008 NCAA Division I FBS football season]]. The Tar Heels played their home games at [[Kenan Memorial Stadium]] in [[Chapel Hill, North Carolina]], competed in the [[Atlantic Coast Conference]], and were led by second-year coach [[Butch Davis]]. The Tar Heels began their season on August 30 against [[McNeese State University|McNeese State]] at  [[Kenan Memorial Stadium]].  The team went 4–4 in conference play and 8–5 overall, but in 2011, North Carolina vacated all its wins from the 2008 and [[2009 North Carolina Tar Heels football team|2009 seasons]].<ref name="vacate" />

==Recruiting==
The Tar Heels received 16 [[Letter of intent|letters of intent]] on [[National Signing Day]], February 6, 2008. One student athlete had already enrolled before National Signing Day and one signed several days later, making this class smaller and less-heralded than the previous year's class.
{{College Athlete Recruit Start|40=yes|collapse=yes}}
{{College Athlete Recruit Entry
| recruit    = A. J. Blue
| position   = QB
| hometown   = Dallas, North Carolina
| highschool = North Gaston HS
| feet   = 6
| inches = 1
| weight = 208
| 40     = 4.6
| commitdate   = January 18, 2008
| scout stars  = 2
| rivals stars = 2
| espn grade   = 75
}}
{{College Athlete Recruit Entry
| recruit    = Zach Brown
| position   = LB
| hometown   = Columbia, Maryland
| highschool = Hargrave Military Academy
| feet   = 6
| inches = 1
| weight = 213
| 40     = 4.42
| commitdate   = January 20, 2008
| scout stars  = 4
| rivals stars = 4
| espn grade   = 79
}}
{{College Athlete Recruit Entry
| recruit    = Jonathan Cooper
| position   = OG
| hometown   = Wilmington, North Carolina
| highschool = John T. Hoggard HS
| feet   = 6
| inches = 3
| weight = 296
| 40     = 5.1
| commitdate   = January 25, 2008
| scout stars  = 3
| rivals stars = 3
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = Quinton Coples
| position   = DE
| hometown   = Kinston, North Carolina
| highschool = Hargrave Military Academy
| feet   = 6
| inches = 6
| weight = 237
| 40     = 4.65
| commitdate   = February 6, 2008
| scout stars  = 4
| rivals stars = 4
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = Herman Davidson
| position   = S
| hometown   = Long Beach, California
| highschool = Polytechnic HS
| feet   = 6
| inches = 3
| weight = 215
| 40     = 4.5
| commitdate   = February 6, 2008
| scout stars  = 4
| rivals stars = 3
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Dion Guy
| position   = LB
| hometown   = Washington, D.C.
| highschool = Woodson HS
| feet   = 6
| inches = 3
| weight = 219
| 40     = 4.61
| commitdate   = February 4, 2008
| scout stars  = 2
| rivals stars = 2
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Braden Hanson
| position   = QB
| hometown   = Charlotte, North Carolina
| highschool = Charlotte Latin HS
| feet   = 6
| inches = 5
| weight = 197
| 40     = NA
| commitdate   = July 10, 2007
| scout stars  = 3
| rivals stars = 3
| espn grade   = 73
}}
{{College Athlete Recruit Entry
| recruit    = Todd Harrelson
| position   = WR
| hometown   = Chesapeake, Virginia
| highschool = Oscar F. Smith HS
| feet   = 6
| inches = 1
| weight = 188
| 40     = 4.53
| commitdate   = June 28, 2007
| scout stars  = 3
| rivals stars = 3
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = Kenneth Harris
| position   = LB
| hometown   = Decatur, Georgia
| highschool = Columbia HS
| feet   = 6
| inches = 4
| weight = 206
| 40     = 4.60
| commitdate   = February 3, 2008
| scout stars  = 2
| rivals stars = 2
| espn grade   = 40
}}
{{College Athlete Recruit Entry
| recruit    = Dwight Jones
| position   = WR
| hometown   = Burlington, North Carolina
| highschool = Cummings HS
| feet   = 6
| inches = 4
| weight = 210
| 40     = 4.61
| commitdate   = January 26, 2007
| scout stars  = 4
| rivals stars = 5
| espn grade   = 81
}}
{{College Athlete Recruit Entry
| recruit    = Michael McAdoo
| position   = DE
| hometown   = Antioch, Tennessee
| highschool = Antioch HS
| feet   = 6
| inches = 7
| weight = 228
| 40     = 4.75
| commitdate   = January 31, 2008
| scout stars  = 3
| rivals stars = 3
| espn grade   = 77
}}
{{College Athlete Recruit Entry
| recruit    = Ebele Okakpu
| position   = LB
| hometown   = Roswell, Georgia
| highschool = Roswell HS
| feet   = 6
| inches = 2
| weight = 208
| 40     = 4.50
| commitdate   = August 4, 2007
| scout stars  = 4
| rivals stars = 3
| espn grade   = 75
}}
{{College Athlete Recruit Entry
| recruit    = Robert Quinn
| position   = DE
| hometown   = North Charleston, South Carolina
| highschool = Fort Dorchester HS
| feet   = 6
| inches = 5
| weight = 250
| 40     = 4.72
| commitdate   = February 6, 2008
| scout stars  = 4
| rivals stars = 4
| espn grade   = 80
}}
{{College Athlete Recruit Entry
| recruit    = Kevin Reddick
| position   = LB
| hometown   = New Bern, North Carolina
| highschool = New Bern HS
| feet   = 6
| inches = 2
| weight = 220
| 40     = 4.57
| commitdate   = January 13, 2008
| scout stars  = 4
| rivals stars = 3
| espn grade   = 78
}}
{{College Athlete Recruit Entry
| recruit    = Joseph Townsend
| position   = DT
| hometown   = San Jose, California
| highschool = Foothill College
| feet   = 6
| inches = 4
| weight = 290
| 40     = 4.87
| commitdate   = February 9, 2008
| scout stars  = 4
| rivals stars = 3
| espn grade   = NA
}}
{{College Athlete Recruit Entry
| recruit    = Randy White
| position   = TE
| hometown   = Bristol, Virginia
| highschool = Virginia HS
| feet   = 6
| inches = 5
| weight = 234
| 40     = 4.79
| commitdate   = June 15, 2007
| scout stars  = 2
| rivals stars = 2
| espn grade   = 73
}}
{{College Athlete Recruit Entry
| recruit    = Melvin Williams
| position   = S
| hometown   = Lebanon, Tennessee
| highschool = Coffeyville CC
| feet   = 6
| inches = 0
| weight = 203
| 40     = 4.55
| commitdate   = October 23, 2007
| scout stars  = 4
| rivals stars = 4
| espn grade   = NA
}}
{{College Athlete Recruit Entry
| recruit    = Christian Wilson
| position   = TE
| hometown   = McKees Rocks, Pennsylvania
| highschool = Montour HS
| feet   = 6
| inches = 3
| weight = 235
| 40     = 4.60
| commitdate   = February 6, 2008
| scout stars  = 4
| rivals stars = 4
| espn grade   = 81
}}
{{College Athlete Recruit Entry
| recruit    = Jamal Womble
| position   = RB
| hometown   = Sierra Vista, Arizona
| highschool = Buena HS
| feet   = 5
| inches = 10
| weight = 221
| 40     = 4.41
| commitdate   = December 5, 2007
| scout stars  = 4
| rivals stars = 4
| espn grade   = 78
}}
{{College Athlete Recruit End
| 40               = yes
| year             = 2007
| rivals ref title = North Carolina Commit List for 2007
| scout ref title  = Scout.com Football Recruiting: North Carolina
| espn ref title   = RecruitTracker 2007: North Carolina
| rivals school    =
| scout s          =
| espn schoolid    =
| scout overall    = 15
| rivals overall   = 16
| accessdate       = March 4, 2008
| collapse         = yes
}}

==Coaching staff==
After signing a contract extension at the end of the 2007 season, [[Butch Davis]] enters his second season as head coach.  [[Chuck Pagano]] resigned as defensive coordinator and defensive backs coach to become an assistant coach with the [[Baltimore Ravens]] in the [[National Football League|NFL]]. He was replaced by [[University of Minnesota|Minnesota]] assistant  Everett Withers.<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/112107aac.html Butch Davis Agrees To Contract Extension]." ''tarheelblue.com.'' Retrieved on January 28, 2008.</ref><ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/021108aaa.htmll Pagano Headed Back To NFL]." ''tarheelblue.com.'' Retrieved on February 12, 2008.</ref>

{| class="wikitable"
! '''Name'''
! '''Position''' <ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/fbccoaches.html North Carolina Coaching Staff]." ''tarheelblue.com.'' Retrieved on January 28, 2008.</ref>
! '''Seasons in Position'''
|- align="center"
| [[Butch Davis]]
| Head Coach
| 2nd
|- align="center"
| [[John Blake (American football)|John Blake]]
| Associate Head Coach / Recruiting Coordinator / Defensive Line
| 2nd
|- align="center"
| Ken Browning
| Running Backs
| 15th
|- align="center"
| Jeff Connors
| Strength and Conditioning Coordinator
| 8th
|- align="center"
| Steve Hagen
| Tight Ends
| 2nd
|- align="center"
| John Lovett
| Special Teams Coordinator / Defensive Assistant
| 2nd
|- align="center"
| Sam Pittman
| Offensive Line
| 2nd
|- align="center"
| [[John Shoop]]
| Offensive Coordinator / Quarterbacks
| 2nd
|- align="center"
| Tommy Thigpen
| Linebackers
| 4th
|- align="center"
| Charlie Williams
| Wide Receivers
| 2nd
|- align="center"
| Everett Withers
| Defensive Coordinator / Defensive Backs
| 1st
|-
|}

==Roster==
{| class="toccolours" style="border-collapse:collapse; font-size:90%;"
|-
|
|-
|valign="top"|

;Wide Receiver
*1 [[Brooks Foster]] – ''Senior''
*2 Cooter Arnold – ''Senior''
*3 Kenton Thornton – ''Junior''
*15 Anthony Parker-Boyd – ''Sophomore''
*19 Josh Washburn – ''Junior''
*23 Quentin Plair – ''Sophomore''
*34 Brett Long – ''Senior''
*82 Todd Harrelson – ''Freshman''
*83 [[Dwight Jones (American football)|Dwight Jones]] – ''Freshman''
*85 Rashad Mason – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*87 [[Brandon Tate]] – ''Senior''
*88 [[Hakeem Nicks]] – ''Junior''

;Offensive Lineman
*60 Zack Handerson – ''Sophomore''
*64 [[Jonathan Cooper]] – ''Freshman''
*65 Cam Holland – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*66 Mike Ingersoll – ''Sophomore''
*67 Morgan Randall – ''Sophomore''
*68 Mike Dykes – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*69 Lowell Dyer – ''Junior''
*70 Alan Pelc – ''Sophomore''
*71 Carl Gaskins – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*72 Kyle Jolly – ''Junior''
*73 Aaron Stahl – ''Junior''
*74 Sam Ellis – ''Sophomore''
*75 Garrett Reynolds – ''Senior''
*76 Bryon Bishop – ''Senior''
*77 Kevin Bryant – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*79 Calvin Darity – ''Senior''

;Half-back
*17 [[Zack Pianalto]] – ''Sophomore''
*33 Christian Wilson – ''Freshman''

;Tight End
*80 Ed Barham – ''Sophomore''
*81 B.J. Phillips – ''Sophomore''
*86 Randy White – ''Freshman''
*89 [[Richard Quinn (American football)|Richard Quinn]] – ''Senior''
|width="25"|&nbsp;
|valign="top"|

;Quarterback
*7 Mike Paulus – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*11 Cameron Sexton – ''Junior''
*13 [[T.J. Yates]] – ''Sophomore''
*14 Braden Hanson – ''Freshman''

;Running Back
*5 Jamal Womble – ''Freshman''
*8 [[Greg Little (American football)|Greg Little]] – ''Sophomore''
*20 [[Shaun Draughn]] – ''Sophomore''
*30 Carter Brown – ''Sophomore''
*32 Ryan Houston – ''Sophomore''
*45 Devon Ramsay – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''

;Fullback
*4 Bobby Rome – ''Junior''
*6 Anthony Elzy – ''Sophomore''
*43 Curtis Byrd – ''Sophomore''

;Defensive tackle
*9 [[Marvin Austin]] – ''Sophomore''
*91 Tydreke Powell – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*93 [[Cam Thomas]] – ''Junior''
*94 Brian White – ''Junior''
*96 Tavares Brown – ''Junior''
*97 Aleric Mullins – ''Junior''

;Defensive End
*40 Darius Powell – ''Sophomore''
*42 [[Robert Quinn (American football)|Robert Quinn]] – ''Freshman''
*84 Vince Jacobs – ''Sophomore''
*90 [[Quinton Coples]] – ''Freshman''
*92 [[E. J. Wilson]] – ''Junior''
*94 [[Michael McAdoo]] – ''Freshman''
*95 Greg Elleby – ''Sophomore''
*98 Darrius Massenburg – ''Sophomore''

;Cornerback
*12 Charles Brown – ''Sophomore''
*16 Kendric Burney – ''Sophomore''
*23 Jordan Hemby – ''Junior''
*24 Tavorris Jolly – ''Sophomore''
*26 Richie Rich – ''Junior''
*29 Brian Gupton – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*34 [[Johnny White (American football)|Johnny White]] – ''Sophomore''
*37 LaCount Fantroy – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
|width="25"|&nbsp;
|valign="top"|

;Linebacker
*35 Herman Davidson – ''Freshman''
*36 Kennedy Tinsley – ''Junior''
*41 Mark Paschal – ''Senior''
*44 [[Chase Rice]] – ''Senior''
*45 Alex Crisp – ''Sophomore''
*47 [[Zach Brown]] – ''Freshman''
*49 [[Ryan Taylor (American football)|Ryan Taylor]] – ''Junior''
*52 [[Quan Sturdivant]] – ''Sophomore''
*53 Kenneth Harris – ''Freshman''
*54 [[Bruce Carter (American football)|Bruce Carter]] – ''Sophomore''
*55 Linwan Euwell – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*57 Dion Guy – ''Freshman''
*57 Hayden Hunter – ''Sophomore''
*58 Ebele Okakpu – ''Freshman''

;Safety
*7 Josh Stewart – ''Sophomore''
*10 Melvin Williams – ''Junior''
*21 [[Da'Norris Searcy]] – ''Sophomore''
*25 Matt Merletti – ''Sophomore''
*27 [[Deunta Williams]] – ''Sophomore''
*28 Jonathan Smith – ''Sophomore''
*31 Trimane Goddard – ''Senior''
*32 Tyler Caldwell – ''Junior''
*43 Jabir Jones – ''Senior''

;Long Snapper
*51 Trevor Stuart – ''Sophomore''
*58 Mark House – ''Sophomore''

;Punter / Place Kicker
*11 Casey Barth – ''Freshman''
*16 Trase Jones – ''Sophomore''
*17 Grant Schallock – ''Sophomore''
*18 Jay Wooten – [[File:Redshirt.svg|10px|Redshirt]] ''Freshman''
*19 Terrence Brown – ''Senior''
*29 Reid Phillips – ''Sophomore''
|}

<!-- Removed until alignment is fixed within transclusion
===Depth chart===
{{2008 North Carolina Tar Heels Depth Chart}}

-->

==Schedule==
{{CFB Schedule Start|time=|rank=yes|ranklink=|rankyear=2008|tv=|attend=yes}}
{{CFB Schedule Entry
| date         = August 30
| time         = 6:05 PM
| w/l          = v
| nonconf      = yes
| homecoming   =
| away         =
| neutral      =
| rank         =
| opponent     = {{cfb link|year=2008|team=McNeese State Cowboys|title=McNeese State}}
| opprank      =
| site_stadium = [[Kenan Memorial Stadium]]
| site_cityst  = [[Chapel Hill, North Carolina]]
| gamename     =
| tv           = [[ESPN3|ESPN360]]
| score        = 35–27
| overtime     =
| attend       = 58,000
}}
{{CFB Schedule Entry
| date         = September 11
| time         = 7:45 PM
| w/l          = v
| nonconf      = yes
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[2008 Rutgers Scarlet Knights football team|Rutgers]]
| opprank      =
| site_stadium = [[Rutgers Stadium]]
| site_cityst  = [[Piscataway, New Jersey]]
| gamename     =
| tv           = [[ESPN]]
| score        = 44–12
| overtime     =
| attend       = 42,502
}}
{{CFB Schedule Entry
| date         = September 20
| time         = 3:30 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         =
| opponent     = [[2008 Virginia Tech Hokies football team|Virginia Tech]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, North Carolina
| gamename     =
| tv           = [[ESPN on ABC|ABC]]/ESPN
| score        = 17–20
| overtime     =
| attend       = 59,800
}}
{{CFB Schedule Entry
| date         = September 27
| time         = 12:00 PM
| w/l          = v
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[2008 Miami Hurricanes football team|Miami (FL)]]
| opprank      =
| site_stadium = [[Dolphin Stadium]]
| site_cityst  = [[Miami Gardens, Florida]]
| gamename     =
| tv           = [[ESPN2]]
| score        = 28–24
| overtime     =
| attend       = 35,830
}}
{{CFB Schedule Entry
| date         = October 4
| time         = 7:00 PM
| w/l          = v
| nonconf      = yes
| homecoming   =
| away         =
| neutral      = 
| rank         =
| opponent     = [[2008 Connecticut Huskies football team|Connecticut]]
| opprank      = 23
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, North Carolina
| gamename     =
| tv           = ESPN2
| score        = 38–12
| overtime     =
| attend       = 59,500
}}
{{CFB Schedule Entry
| date         = October 11
| time         = 3:30 PM
| w/l          = v
| nonconf      = yes
| homecoming   =
| away         =
| neutral      =
| rank         = 22
| opponent     = [[2008 Notre Dame Fighting Irish football team|Notre Dame]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, North Carolina
| gamename     =
| tv           = ABC/ESPN
| score        = 29–24
| overtime     =
| attend       = 60,500
}}
{{CFB Schedule Entry
| date         = October 18
| time         = 3:30 PM
| w/l          = l
| nonconf      = 
| homecoming   =
| away         = yes
| neutral      =
| rank         = 18
| opponent     = [[2008 Virginia Cavaliers Football Team|Virginia]]
| opprank      =
| site_stadium = [[Scott Stadium]]
| site_cityst  = [[Charlottesville, Virginia]]
| gamename     = [[South's Oldest Rivalry (North Carolina-Virginia)|South's Oldest Rivalry]]
| tv           = ABC/ESPN2
| score        = 13–16
| overtime     = OT
| attend       = 52,342
}}
{{CFB Schedule Entry
| date         = October 25
| time         = 12:00 PM
| w/l          = v
| nonconf      = 
| homecoming   =
| away         =
| neutral      =
| rank         =
| opponent     = [[2008 Boston College Eagles football team|Boston College]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, North Carolina
| gamename     =
| tv           = [[Raycom Sports|Raycom]]
| score        = 45–24
| overtime     =
| attend       = 48,000
}}
{{CFB Schedule Entry
| date         = November 8
| time         = 12:00 PM
| w/l          = v
| nonconf      =
| homecoming   = yes
| away         =
| neutral      =
| rank         = 19
| opponent     = [[2008 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| opprank      = 20
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, North Carolina
| gamename     =
| tv           = Raycom
| score        = 28–7
| overtime     =
| attend       = 59,000
}}
{{CFB Schedule Entry
| date         = November 15
| time         = 3:30 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         = 17
| opponent     = [[2008 Maryland Terrapins football team|Maryland]]
| opprank      =
| site_stadium = [[Byrd Stadium]]
| site_cityst  = [[College Park, Maryland]]
| gamename     =
| tv           = ABC/ESPN
| score        = 15–17
| overtime     =
| attend       = 46,113
}}
{{CFB Schedule Entry
| date         = November 22
| time         = 12:00 PM
| w/l          = l
| nonconf      =
| homecoming   =
| away         =
| neutral      =
| rank         =
| opponent     = [[2008 NC State Wolfpack football team|North Carolina State]]
| opprank      =
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, North Carolina
| gamename     =
| tv           = Raycom
| score        = 10–41
| overtime     =
| attend       = 60,000
}}
{{CFB Schedule Entry
| date         = November 29
| time         = 3:30 PM
| w/l          = v
| nonconf      =
| homecoming   =
| away         = yes
| neutral      =
| rank         =
| opponent     = [[2008 Duke Blue Devils football team|Duke]]
| opprank      =
| site_stadium = [[Wallace Wade Stadium]]
| site_cityst  = [[Durham, North Carolina]]
| gamename     = [[Victory Bell (North Carolina–Duke)|Victory Bell Game]]
| tv           = [[ESPNU]]
| score        = 28–20
| overtime     =
| attend       = 30,322
}}
{{CFB Schedule Entry
| date         = December 27
| time         = 1:00 PM
| w/l          = l
| nonconf      = yes
| homecoming   =
| away         =
| neutral      = yes
| rank         =
| opponent     = [[2008 West Virginia Mountaineers football team|West Virginia]]
| opprank      =
| site_stadium = [[Bank of America Stadium]]
| site_cityst  = [[Charlotte, North Carolina]]
| gamename     = [[2008 Meineke Car Care Bowl|Meineke Car Care Bowl]]
| tv           = ESPN
| score        = 30–31
| overtime     =
| attend       = 73,712
}}
{{CFB Schedule End|rank=|poll=[[AP Poll]]|timezone=[[Eastern Time Zone|Eastern Time]]}}

'''Did not play:''' [[Clemson University|Clemson]], [[Florida State University|Florida State]], and [[Wake Forest University|Wake Forest]].<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/020808aan.html ACC Releases North Carolina's 2008 Football Schedule]." ''tarheelblue.com.'' Retrieved on February 12, 2008.</ref> The spring football game is on March 28, 2009.<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/020809aab.html Spring Football Showcase Set For March 28]." ''tarheelblue.com.'' Retrieved on February 9, 2009.</ref>

==Game summaries==

===McNeese State===
{{Linescore Amfootball|
|Road=McNeese State
|R1=0
|R2=14
|R3=6
|R4=7
|Home='''North Carolina'''
|H1=7
|H2=7
|H3=7
|H4=14
}}
[[Brandon Tate]] put on a dazzling one-man show, scoring on an 82-yard punt return and putting North Carolina ahead for good with a 57-yard TD catch to help the Tar Heels hold off McNeese State 35-27. Tate finished with a school-record 397 all-purpose yards for the Tar Heels, who showed little else in an unimpressive start to their second season under Butch Davis. In a game suspended nearly two hours due to weather, they blew a 14-0 first-half lead and had to rally from a third-quarter deficit against Football Championship Subdivision McNeese State, who outplayed the Heels much of the way. [[T.J. Yates]] had 221 passing yards and 2 TDs.<ref>[http://espn.go.com/ncf/recap?gameId=282430153]</ref>
{{Clear}}

===Rutgers===
{{see also|2008 Rutgers Scarlet Knights football team}}
{{Linescore Amfootball|
|Road='''North Carolina'''
|R1=0
|R2=17
|R3=21
|R4=6
|Home=Rutgers
|H1=3
|H2=3
|H3=0
|H4=6
}}
[[T.J. Yates]] threw three touchdown passes and the Tar Heels won for the first time outside North Carolina since 2002, beating error-prone Rutgers 44-12 in a nationally televised game that pitted [[Butch Davis]] against his former pupil, [[Greg Schiano]]. [[Hakeem Nicks]] and [[Brandon Tate]] combined for 10 catches, 204 receiving yards, and three touchdowns as the Heels offense rolled over Rutgers. Carolina's defense generated 4 Rutgers turnovers (3 INTs and a fumble) and scored off of a 66-yard interception return by [[Bruce Carter (American football)|Bruce Carter]].<ref>[http://espn.go.com/ncf/recap?gameId=282550164]</ref>
{{Clear}}

==References==
{{Reflist}}

{{North Carolina Tar Heels football navbox}}

{{DEFAULTSORT:2008 North Carolina Tar Heels Football Team}}
[[Category:North Carolina Tar Heels football seasons]]
[[Category:2008 Atlantic Coast Conference football season|North Carolina Tar Heels]]
[[Category:2008 in North Carolina|Tar Heels]]