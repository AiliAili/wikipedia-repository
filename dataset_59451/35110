{{good article}}
{{Infobox hurricane season
| Basin=NIO
| Year=1989
| Track=1989 North Indian Ocean cyclone season summary.jpg
| First storm formed=May 23, 1989
| Last storm dissipated=November 10, 1989
| Strongest storm name=[[Typhoon Gay (1989)|Gay]]
| Strongest storm pressure=930
| Strongest storm winds=125
| Average wind speed=3
| Total disturbances=10
| Total depressions=
| Total storms=3
| Total super=1
| Fatalities=1,785 total
| Damages=25.27
| five seasons=[[1985-1989 North Indian Ocean cyclone seasons#1987_North_Indian_Ocean_cyclone_season|1987]], [[1985-1989 North Indian Ocean cyclone seasons#1988_North_Indian_Ocean_cyclone_season|1988]], '''1989''', [[1990 North Indian Ocean cyclone season|1990]], [[1991 North Indian Ocean cyclone season|1991]]
|Atlantic season=1989 Atlantic hurricane season
|East Pacific season=1989 Pacific hurricane season
|West Pacific season=1989 Pacific typhoon season
}}
The '''1989 North Indian Ocean cyclone season''' was a below-average season in annual cycle of [[tropical cyclone]] formation. The season has no official bounds but cyclones tend to form between April and December. These dates conventionally delimit the period of each year when most tropical cyclones form in the northern [[Indian Ocean]]. There are two main seas in the North Indian Ocean—the [[Bay of Bengal]] to the east of the [[Indian subcontinent]] and the [[Arabian Sea]] to the west of India. The official [[Regional Specialized Meteorological Centre]] in this basin is the [[India Meteorological Department|India Meteorological Department (IMD)]], while the [[Joint Typhoon Warning Center|Joint Typhoon Warning Center (JTWC)]] releases unofficial advisories. An average of five tropical cyclones form in the North Indian Ocean every season with peaks in May and November.<ref>{{cite web|publisher=Indian Meteorological Department|year=2012|accessdate=June 8, 2012|title=Frequently Asked Questions: What is the annual frequency of Cyclones over the Indian Seas? What is its intra-annual variation?|url=http://www.imd.gov.in/section/nhac/dynamic/faq/FAQP.htm#q18}}</ref> Cyclones occurring between the meridians [[45th meridian east|45°E]] and [[100th meridian east|100°E]] are included in the season by the IMD.<ref>{{cite web|publisher=India Meteorological Department|date=May 25, 2009|accessdate=July 16, 2012|title=Bulletins Issued by Regional Specialized Meteorological Centre (RSMC) - Tropical Cyclones, New Delhi|url=http://www.imd.gov.in/section/nhac/dynamic/bulletins.pdf|format=[[PDF]]}}</ref>

Throughout the season, the IMD monitored ten depressions, three of which became cyclonic storms.<ref>{{cite web|url=http://www.imd.gov.in/section/nhac/dynamic/ANNUAL_FREQ_CYCLONIC_DISTURBANCES.pdf|title=Annual frequency of cyclonic disturbances (Maximum sustained windspeeds of 17 knots or more), Cyclones (34 knots or more) and Severe Cyclones (48 knots or more) over the Bay of Bengal (BOB), Arabian Sea (AS) and land surface of India|publisher=India Meteorological Department|date=August 31, 2010|accessdate=March 25, 2012}}</ref> The strongest storm of the year was Super Cyclonic Storm Gay. Crossing the [[Malay Peninsula]] into the Bay of Bengal on November&nbsp;4, Gay became one of the most powerful systems on record in the basin, attaining an estimated pressure of 930&nbsp;mbar (hPa; 27.46&nbsp;inHg). Collectively, the storms were responsible for at least 1,785&nbsp;fatalities, 1,445 of which were due the disastrous flooding triggered by the July Cyclonic Storm, and more than $25&nbsp;million in damage.
 
__TOC__
{{clear}}
<!-- Not enough information to warrant a season summary section -->

==Systems==
<center>
<timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:50 left:20
Legend    = columns:4 left:30 top:58 columnwidth:270
AlignBars = early
DateFormat = dd/mm/yyyy
Period     = from:01/05/1989 till:01/12/1989
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/05/1989
Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)    legend:Depression/Deep_Depression
  id:TS     value:rgb(0,0.98,0.96)    legend:Cyclonic_Storm
  id:ST     value:rgb(0.80,1,1)       legend:Severe_Cyclonic_Storm
  id:VS     value:rgb(1,1,0.8)        legend:Very_Severe_Cyclonic_Storm
  id:ES     value:rgb(1,0.76,0.25)    legend:Extremely_Severe_Cyclonic_Storm
  id:SU     value:rgb(1,0.38,0.38)    legend:Super_Cyclonic_Storm
Backgroundcolors = canvas:canvas
BarData =
  barset:Hurricane
  bar:Month
PlotData=
  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:23/05/1989 till:27/05/1989 color:ST text:BOB 01
  from:07/06/1989 till:13/06/1989 color:TD text:ARB 01
  from:12/06/1989 till:14/06/1989 color:TD text:BOB 02
  from:20/06/1989 till:21/06/1989 color:TD text:BOB 03
  from:22/07/1989 till:25/07/1989 color:TS text:BOB 04
   barset:break
  from:16/08/1989 till:17/08/1989 color:TD text:BOB 05
  from:17/10/1989 till:18/10/1989 color:TD text:BOB 06
  from:04/11/1989 till:10/11/1989 color:SU text:[[Typhoon Gay (1989)|Gay]]
  from:11/11/1989 till:11/11/1989 color:TD text:BOB 08
  from:17/11/1989 till:20/11/1989 color:TD text:BOB 09
  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas

  from:01/05/1989 till:31/05/1989 text:May
  from:01/06/1989 till:01/07/1989 text:June
  from:01/07/1989 till:01/08/1989 text:July
  from:01/08/1989 till:01/09/1989 text:August
  from:01/09/1989 till:01/10/1989 text:September
  from:01/10/1989 till:01/11/1989 text:October
  from:01/11/1989 till:01/12/1989 text:November

</timeline>
</center>

===Severe Cyclonic Storm BOB 01/01B===
{{Infobox Hurricane Small
|Basin=NIO
|Type1=svrcyclstorm
|Formed=May 23
|Dissipated=May 27
|Image=01B May 27 1989 0209Z.png
|Track=Cyclone 01B 1989 track.png
|1-min winds=55
|Pressure=984
}}
In mid-May, a [[monsoon trough]] situated over the Bay of Bengal began showing signs of [[Tropical cyclogenesis|cyclonic development]]. By May&nbsp;20, synoptic data indicated the presence of a weak circulation; however, the system remained disorganized. Following a dramatic increase in [[Atmospheric convection|convection]] and organization, the JTWC issued a [[Tropical Cyclone Formation Alert]] on May&nbsp;23 and subsequently began monitoring the system as a tropical depression hours later. Initially, the depression tracked slowly towards the north-northwest before abruptly turning westward and slowing due to weak mid-level steering currents. During this time, the storm gradually intensified and was limited by northwesterly [[wind shear]]. By May&nbsp;26, the storm turned northward and accelerated. Later that day, 01B attained its peak intensity with winds of 100&nbsp;km/h (65&nbsp;mph) shortly before making [[Landfall (meteorology)|landfall]] in eastern India. The system quickly weakened once inland and was last noted on May 27 as a dissipating low.<ref name="01BATCR">{{cite web|author=Lt. Cdr. Nicholas D. Gural|work=Joint Typhoon Warning Center|publisher=United States Navy|year=1990|accessdate=March 25, 2012|title=1989 Annual Tropical Cyclone Report: Tropical Cyclone 01B|pages=188–189|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1989atcr.pdf|format=[[PDF]]}}</ref>

Striking India on May&nbsp;26, the storm brought wind gusts up to 130&nbsp;km/h (80&nbsp;mph) and torrential rains, amounting to {{convert|210|mm|in|abbr=on}}, which caused widespread damage.<ref name="AP1"/><ref name="Xinhua1">{{cite news|publisher=Xinhua General News|date=May 27, 1989|title=Cyclone causes widespread damage in India's Orissa coast|location=[[New Delhi, India]]}} (Accessed through LexisNexis)</ref> The hardest hit area was [[Midnapore]] where more than 10,000 homes were destroyed. At least 17 people were killed in the district alone and more than 50,000 were left homeless.<ref name="AP1"/> Communications across [[Orissa, India|Orissa]] were severely disrupted as broadcast stations, government buildings, and hundreds of telephone poles were destroyed.<ref name="Xinhua1"/> Further inland, heavy rains from the storm triggered several landslides that killed at least two people in [[Darjeeling|Darjeeling Hills]].<ref>{{cite news|publisher=Xinhua General News|date=May 29, 1989|title=Death toll in cyclone rises to 26 in India|location=New Delhi, India}} (Accessed through LexisNexis)</ref> Throughout eastern India, 61 people were killed and more than 500,000 were left homeless by the storm.<ref name="AP2"/><ref name="IndiaDeaths">{{cite web|publisher=India Meteorological Department|year=1999|accessdate=March 25, 2012|title=Historical records of Severe Cyclones which formed in the Bay of Bengal and made landfall at the eastern coast of India during the period from 1970-1999|url=http://www.imd.gov.in/section/nhac/static/cyclone-history-bb.htm}}</ref>

In nearby [[Bangladesh]], strong winds produced by the storm destroyed 500&nbsp;homes across 11&nbsp;villages.<ref name="AP1">{{cite news|publisher=Associated Press|date=May 27, 1989|title=32 Killed, Thousands Homeless in Cyclone|location=[[Calcutta, India]]}} (Accessed through LexisNexis)</ref> Areas [[Daulatpur-Saturia Tornado|devastated by a tornado]] a month prior were severely affected by the cyclone. In [[Tangail, Bangladesh|Tangail]], a powerful [[tornado]] spawned by the storm destroyed 2,000 homes and killed 10 people.<ref name="AP2"/> At least 60&nbsp;people perished and 2,000 others were injured across the country.<ref name="AP2">{{cite news|publisher=Associated Press|date=May 28, 1989|title=Cyclone Kills 60, Leaves 500,000 Homeless|location=[[Dhaka, Bangladesh]]}} (Accessed through LexisNexis)</ref><ref>{{cite news|work=Associated Press|publisher=Courier-Mail|date=May 29, 1989|title=Fifty Killed in Cyclone: Toll to Rise}} (Accessed through LexisNexis)</ref> Offshore, 150 fishermen went missing during the storm and were feared dead.<ref name="AP3">{{cite news|publisher=Associated Press|date=May 29, 1989|title=Cyclone Kills Scores in Bangladesh|location=Dhaka, Bangladesh}} (Accessed through LexisNexis)</ref> In the wake of the storm, widespread search and rescue missions took place in cities flattened by the cyclone.<ref>{{cite news|work=Associated Press|publisher=St. Louis Post-Dispatch|date=May 29, 1989|title=Workers Search Rubble in Dhaka After Cyclone|page=8A|location=Dhaka, Bangladesh}} (Accessed through LexisNexis)</ref>
{{Clear}}

===Depression ARB 01/02A===
{{Infobox Hurricane Small
|Basin=NIO
|Type1=niodepression
|Formed=June 7
|Dissipated=June 13
|Image=02A Jun 12 1989 0914Z.png
|Track=Cyclone 02A 1989 track.png
|1-min winds=35
|Pressure=996
}}
On June&nbsp;7, small area of low pressure developed off the west coast of India. Over the following two days, convection associated with the low gradually organized and by June&nbsp;9, satellite intensity estimates from the JTWC reached 55&nbsp;km/h (30&nbsp;mph). A TCFA was subsequently issued for the system before it made landfall in [[Gujarat]] early on June&nbsp;10. Although overland, the low maintained significant convection as it turned westward and through its re-emergence into the Arabian Sea on June&nbsp;11. Once back over water, convection rapidly spread westward in response to an anticyclone over the [[Arabian Peninsula]] and [[Afghanistan]]. Early on June&nbsp;12, the cyclone was estimated to have attained tropical storm status based on a ship report near the center of 65&nbsp;km/h (40&nbsp;mph) sustained winds and a surface pressure of 998&nbsp;mbar (hPa; 29.47&nbsp;inHg). Later that day, strong wind shear stemming from the anticyclone displaced convection from the tropical storm by more than 110&nbsp;km (70&nbsp;mi), prompting the final advisory from the JTWC. The remnants of the system were last noted on June&nbsp;13 dissipating over the Arabian Sea.<ref name="02AATCR">{{cite web|author=Lt. Richard H. Bouchard|work=Joint Typhoon Warning Center|publisher=United States Navy|year=1990|accessdate=June 8, 2012|title=1989 Annual Tropical Cyclone Report: Tropical Cyclone 02B|pages=190–191|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1989atcr.pdf|format=[[PDF]]}}</ref>

{{Clear}}

===Cyclonic Storm BOB 04===
{{Infobox Hurricane Small
|Basin=NIO
|Type1=cyclstorm
|Formed=July 22
|Dissipated=July 25
|Image=
|Track=
|1-min winds=
|Pressure=
}}
On July&nbsp;22, the IMD began monitoring a depression over the Bay of Bengal. Tracking west-northwest, the system intensified into a cyclonic storm later that day before making landfall in [[Andhra Pradesh]], just north of [[Vishakhapatnam]]. Once onshore the storm accelerated towards the northwest and weakened. By July&nbsp;24, the remnants of the cyclone were located over the state of [[Maharashtra]]. The system was last noted the following day over [[Gujarat]] and moving into [[Pakistan]].<ref name="IMDAtlas">{{cite web|work=Cyclone Warning & Research Centre, Regional Meteorological Centre|publisher=India Meteorological Department|year=2012|accessdate=June 7, 2012|title=Cyclone Web Atlas|url=http://www.rmcchennaieatlas.tn.nic.in/Login.aspx|location=Chennai, India}}</ref>

Across Andhra Pradesh, [[Orissa, India|Orissa]], and [[Uttar Pradesh]] heavy rains produced by the storm triggered flash flooding and mudslides that killed at least 414 people.<ref>{{cite news|work=Agence France-Presse|publisher=The Independent|date=July 27, 1989|title=Ships look for 2,000 missing in floods|location=Bombay, India}} (Accessed through LexisNexis)</ref> According to [[Chief Minister (India)|Chief Minister]] [[N. T. Rama Rao|Nandamuri Taraka Rama Rao]], approximately 70,000 homes were destroyed in Andhra Pradesh. While over Maharashtra, the storm produced torrential rainfall, reaching {{convert|280|mm|in|abbr=on}} in 24&nbsp;hours in [[Bombay]], which caused deadly flash flooding and mudslides.<ref name="AP724"/> Most of the railway tracks in metropolitan Bombay were left underwater, paralyzing the city and forcing businesses to close for several days. The city's stock exchange remained open, though only sparse trading was observed.<ref name="TG727">{{cite news|author=Ajoy Bose|publisher=The Guardian|date=July 27, 1989|title=Hundreds feared dead in Indian monsoon flooding|location=New Delhi, India}} (Accessed through LexisNexis)</ref> Flood waters isolated 46 villages in the region, prompting the deployment of the Indian Army for rescue missions.<ref name="AP724">{{cite news|work=Associated Press|date=July 24, 1989|title=Downpours in Western India Kill at Least 100|location=Bombay, India}} (Accessed through LexisNexis)</ref> At least 500 people were killed throughout Maharashtra, more than 200 of which took place in the [[Raigad district]].<ref name="725AP"/><ref>{{cite news|work=The Times of India|publisher=Xinhua General News|date=July 26, 1989|title=750 killed as flood situation worsens|location=New Delhi, India}} (Accessed through LexisNexis)</ref> An unknown number of people were killed after a bridge collapsed with two train carriages on it.<ref name="TG727"/> Additionally, 75 others were reported missing in the district according to local police.<ref name="725AP">{{cite news|work=Associated Press|date=July 25, 1989|title=Death Toll From Storm Tops 450|location=Bombay, India}} (Accessed through LexisNexis)</ref> Offshore, 500 fishermen went missing in connection to the storm and are believed to have died.<ref>{{cite news|work=Hobart Mercury|date=July 28, 1989|title=Storm toll 800}} (Accessed through LexisNexis)</ref>

Flooding rains extended into Pakistan by July&nbsp;26.<ref>{{cite news|work=The Muslim|publisher=Xinhua General News|date=July 26, 1989|title=7 killed in heavy Karachi rains|location=Islamabad, Pakistan}} (Accessed through LexisNexis)</ref> Flash floods in the slums outside [[Karachi]] killed at least 16 people and washed away 500 huts. An estimated 20,000 people were left homeless in the city. Communication and transportation throughout Karachi was reportedly paralyzed as well due to widespread power outages. Further north in [[Hyderabad, India|Hyderabad]], six others were killed by the storm.<ref>{{cite news|work=Xinhua General News|date=July 27, 1989|title=Rains' death toll rises to 16 in Karachi, Pakistan|location=Islamabad, Pakistan}} (Accessed through LexisNexis)</ref> Throughout the country, at least 31 people were killed.<ref>{{cite news|work=Associated Press|date=July 27, 1989|title=Thousands Dead Throughout Asia In Floods, Mudslides|location=Beijing, China}} (Accessed through LexisNexis)</ref>
{{clear}}

===Super Cyclonic Storm Gay===
{{Infobox Hurricane Small
|Basin=NIO
|Formed=November 4 (entered basin)
|Dissipated=November 10
|Image=Typhoon Gay 08 nov 1989 0826Z.jpg
|Track=Gay 1989 track.png
|3-min winds=125
|1-min winds=140
|Pressure=898
}}
{{Main|Typhoon Gay (1989)}}
On November&nbsp;2, a tropical depression, later named ''Gay'', developed in the [[Gulf of Thailand]] and favorable atmospheric conditions allowed the system to undergo [[rapid deepening|rapid intensification]]. By November&nbsp;3, Gay had intensified to a Category&nbsp;3-equivalent typhoon before striking [[Thailand]].<ref name="GayATCR"/> Crossing the [[Kra Isthmus]] in approximately six hours, the system emerged into the Bay of Bengal as a Category&nbsp;1-equivalent cyclone and assumed a west-northwesterly track towards India. For the next four days, the storm gradually reorganized before reaching a small area favorable for more significant intensification late on November&nbsp;6. Hours before making landfall in India, Gay attained its peak intensity as a Category&nbsp;5-equivalent cyclone with winds estimated at 260&nbsp;km/h (160&nbsp;mph).<ref name="GayATCR">{{cite web|author=Lt. Dianne K. Crittenden|work=Joint Typhoon Warning Center|publisher=United States Navy|year=1990|accessdate=June 8, 2012|title=1989 Annual Tropical Cyclone Report: Typhoon Gay (32W)|pages=166–172|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1989atcr.pdf|format=[[PDF]]}}</ref> Additionally, the IMD estimated that the storm had three-minute sustained winds of 240&nbsp;km/h (145&nbsp;mph), classifying Gay as a modern-day Super Cyclonic Storm.<ref name="IMDWind">{{cite journal|author=G.S. Mandal and Akhilesh Gupta|publisher=Indian Meteorological Society|journal=Advances in Tropical Meteorology|year=1996|location=New Delhi, India|title=The Wind Structure, Size and Damage Potential of Some Recent Cyclone of Hurricane Intensity in the North Indian Ocean|page=421|issue=50}}</ref><ref>{{cite web|publisher=Global Facility for Disaster Reduction and Recovery |date=September 2011 |accessdate=June 8, 2012 |title=Cyclones, storm surges, floods, landslides |page=9 |url=https://www.gfdrr.org/gfdrr/sites/gfdrr.org/files/1_Introduction.pdf |format=[[PDF]] |deadurl=yes |archiveurl=https://web.archive.org/web/20120426055441/https://www.gfdrr.org/gfdrr/sites/gfdrr.org/files/1_Introduction.pdf |archivedate=April 26, 2012 |df= }}</ref> The powerful storm soon made landfall near [[Kavali]], India, in Andhra Pradesh before rapidly weakening onshore. The system eventually dissipated over Maharashtra on November&nbsp;10.<ref name="GayATCR"/>

In Thailand, the storm caused extensive damage both onshore and off, killing 833&nbsp;people and inflicting approximately {{ntsp|11000000000||[[Thai Baht|฿]]}} ({{ntsp|496500000||US$}}) in damage.<ref>{{cite report|author=Vipa Rungdilokroajn |publisher=Universidad Nacional Autónoma de Nicaragua |year=February 1990 |accessdate=December 16, 2011 |title=Natural Disasters in Thailand |location=Bangkok, Thailand |url=http://desastres.unanleon.edu.ni/pdf2/2006/abril/parte2/pdf/eng/doc5692/doc5692-contenido.pdf |format=[[PDF]] |deadurl=yes |archiveurl=https://web.archive.org/web/20120426052009/http://desastres.unanleon.edu.ni/pdf2/2006/abril/parte2/pdf/eng/doc5692/doc5692-contenido.pdf |archivedate=April 26, 2012 |df= }}</ref><ref>{{cite web|publisher=Asian Disaster Reduction Center|year=1998|accessdate=December 15, 2011|title=Thailand Country Report|url=http://www.adrc.asia/countryreport/THA/THAeng98/index.html}}</ref> Striking India as a powerful cyclone, Gay damaged or destroyed about 20,000&nbsp;homes in [[Andhra Pradesh]], leaving 100,000&nbsp;people homeless.<ref>{{cite web|work=Reuters|publisher=Los Angeles Times|location=Los Angeles, California|date=November 11, 1989|accessdate=June 8, 2012|title=Typhoon Death Toll Rises; 500 Missing Off Thailand Coast|url=http://articles.latimes.com/1989-11-11/news/mn-1002_1_death-toll}}</ref> In that country, 69 deaths and {{ntsp|410000000||{{Indian Rupee|link=Indian Rupee}}}} ({{ntsp|25270000||US$}}) in damage were attributed to Gay.<ref>{{cite journal|author=S. Raghavan and S. Rajesh|journal=Bulletin of the American Meteorological Society|date=May 2003|accessdate=June 8, 2012|title=Trends in Tropical Cyclone Impact: A Study in Andhra Pradesh, India|url=http://journals.ametsoc.org/doi/abs/10.1175/BAMS-84-5-635|doi=10.1175/BAMS-84-5-635|volume=85|issue=5|pages=635–644|format=[[PDF]]|bibcode = 2003BAMS...84..635R }}</ref><ref>{{cite web|publisher=India Meteorological Department|year=1999|accessdate=June 8, 2012|title=Historical records of Severe Cyclones which formed in the Bay of Bengal and made landfall at the eastern coast of India during the period from 1970–1999|url=http://www.imd.gov.in/section/nhac/static/cyclone-history-bb.htm}}</ref>
{{Clear}}

===Other systems===
In addition to the storms listed above, the IMD monitored six other depressions throughout the year.
*June&nbsp;12&nbsp;– 14
:Formed in the northern Bay of Bengal and made landfall in Orissa, India.<ref name="IMDAtlas"/>
*June&nbsp;20&nbsp;– 21
:Formed in the northern Bay of Bengal and made landfall in [[West Bengal]], India.<ref name="IMDAtlas"/>
*August&nbsp;16&nbsp;– 17
:Formed in the Bay of Bengal and made landfall in Andhra Pradesh, India.<ref name="IMDAtlas"/>
*October&nbsp;17&nbsp;– 18
:Formed in the Bay of Bengal and made landfall near the India/Bangladesh border.<ref name="IMDAtlas"/> In Bangladesh, heavy rains and high winds, estimated at {{convert|60|to|70|km/h|mph|abbr=on}}, caused significant damage. At least 100 people were injured and 1,000 homes were damaged or destroyed, mainly in the [[Chandpur District]]. Following the storm, the Bangladesh Red Crescent Society dispatched four medical teams and relief materials to the affected regions.<ref>{{cite news|work=Xinhua General News|date=October 19, 1989|title=Storm batters Southeast Bangladesh|location=Dhaka, Bangladesh}} (Accessed through LexisNexis)</ref>
*November&nbsp;11
:Brief depression formed over the Bay of Bengal before dissipating just north of [[Sri Lanka]] the same day.<ref name="IMDAtlas"/>
*November&nbsp;17&nbsp;– 20
:A slow moving depression formed over the Bay of Bengal, northeast of Sri Lanka, and meandered in the same general area for three days before dissipating.<ref name="IMDAtlas"/>

==See also==
{{Portal|Tropical cyclones}}
*[[List of North Indian Ocean cyclone seasons]]
*[[1989 Atlantic hurricane season]]
*[[1989 Pacific hurricane season]]
*[[1989 Pacific typhoon season]]
*Australian cyclone seasons: [[1988–89 Australian region cyclone season|1988–89]], [[1989–90 Australian region cyclone season|1989–90]]
*South Pacific cyclone seasons: [[1988–89 South Pacific cyclone season|1988–89]], [[1989–90 South Pacific cyclone season|1989–90]]
*South-West Indian Ocean cyclone seasons: [[1988–89 South-West Indian Ocean cyclone season|1988–89]], [[1989–90 South-West Indian Ocean cyclone season|1989–90]]

==References==
{{Reflist|2}}

==External links==
*[http://www.imd.gov.in/section/nhac/dynamic/cyclone.htm India Meteorological Department]
*[http://www.usno.navy.mil/JTWC/ Joint Typhoon Warning Center]

{{TC Decades|Year=1980|basin=North Indian Ocean|type=cyclone}}

{{DEFAULTSORT:1989 North Indian Ocean Cyclone Season}}
[[Category:1989 North Indian Ocean cyclone season| ]]
[[Category:Articles which contain graphical timelines]]