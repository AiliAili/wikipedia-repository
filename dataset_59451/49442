{{multiple issues|
{{ref improve|date=February 2015}}
{{third-party|date=February 2015}}
}}
{{Infobox video game
|title=Magnetic Joe
|image=
|caption= Game Title Screen
|developer=Most Wanted Entertainment
|publisher=HD Publishing
|designer=Ferenc Thier
|programmer=Ferenc Thier
|artist=Ferenc Thier, Csaba Kemeri
|released=2006
|genre=[[Puzzle video game]]
|modes=[[Single-player]]
|platforms=[[Mobile phone|Mobile]], [[iPhone]], [[Sidewards]], [[#Ports_and_remakes|various]]
}}

'''''Magnetic Joe''''' is a [[Puzzle video game|puzzle]]-[[Platform game|platformed]] [[video game]] developed for [[mobile phone]]s by Most Wanted Entertainment/Mobile and published by HD publishing<ref>{{cite web|url=http://www.mobygames.com/browse/games/hd-publishing-bv|title=MobyGames: Game Browser|work=MobyGames}}</ref> in 2006. The objective of the game is to guide a magnetic metal ball known as Joe to a designated exit in each level using various magnetic forces.

==Gameplay==

The game features a one-button control scheme where the player presses a single button, or touches the screen, to activate Joe's magnetism. 
<ref>{{cite web|url=http://www.mobilegamefaqs.com/reviewfull.php?orig=rev&id=640|title=Magnetic Joe mobile game review by Mobile Game Faqs|work=mobilegamefaqs.com}}</ref>
When Joe passes near a magnetic cell his magnetism activates. The player can see a lightning effect between Joe and the magnet, and Joe moves towards the magnet. As he moves closer to the magnet, his movement and rotation changes. The player can control the ball's movement by timing when the magnetism is activated. Different magnets push or pull the ball in different directions and are marked accordingly. 

Joe can move around by rolling and bouncing. However, frequent hazards on the map such as spiked floors and walls add a challenge to this form of movement.

==Versions==

===Magnetic Joe (mobile / J2ME)===
The original game was released in 2006 for mobile phones. It featured 50 levels to complete 3 different "worlds".

It was possible to play randomly generated 'secret' levels by entering a code. Levels were generated based on the code entered.

The original game was well received by the gaming press, praising its simple and great game mechanics. It won a 'best casual game' award in 2006.<ref>{{cite web|url=http://www.mobilegamefaqs.com/mgf_awards.php?year=2006&award=casual|title=Mobile Game Faqs - Awards 2006|work=mobilegamefaqs.com}}</ref> "Magnetic Joe is incredibly simple and incredibly addictive, the game can be frustrating and extremely rewarding. Absolutely brilliant."

===Magnetic Joe 2 (mobile / J2ME)===

The second Magnetic Joe game had the same basic gameplay, but introduced new interactive game objects and characters. Interactive objects include teleports, a cannon, a lift, and breakable walls. New game characters are Josephine (Joe's Girlfriend), Invisible Joe, Bad Joe, and Robot Joe. The game also introduced new gameplay modes. In "collect mode", the player has to first find three "Little Joe"-s before the exits are activated and it's possible to win. In "enemy mode", there are enemies to avoid in the levels. Magnetic Joe 2 has a skateboarding minigame.<ref name="Magnetic Joe 2">{{citeweb|url=http://www.macworld.com/product/72320/magnetic-joe-2.html|title=Magnetic Joe 2|accessdate=2014-02-24}}</ref>

===Magnetic Joe  (Nintendo DSi)===

A Nintendo DSi version of the game introduced a story mode and local wi-fi multiplayer.<ref>[http://www.nintendolife.com/reviews/2011/03/magnetic_joe_dsiware Nintendo Life Review]</ref> In story mode, the player must complete levels through several "worlds" that feature enemies, obstacles, and bosses unique to that world. 
It featured three different t modes: Classic, Time, and Collect. Classic mode features the same rules as the original mobile game. In Time Mode, levels have to be completed within a predefined time limit. Meanwhile, in Collect Mode, the player needs to collect special items before moving to the exit. Each of the game modes also feature Hard variations in which the player is limited in the number of times Joe can touch an obstacle before losing.
<br>
In the wi-fi multiplayer player challenge, two players play on the same level at once.

===Magnetic Joe 1 & 2 (iPhone)===

Magnetic Joe 1 & 2 for the iPhone is a port from the original Magnetic Joe for the iPhone, adding an online leaderboard feature. Each level is timed, and users can submit their times to an online leaderboard to beat the predefined 'developer time' for each level. By improving their times, players are able to unlock new characters to play in the game.
 
Note: Magnetic Joe was removed from App Store due to the demise of the publisher.<ref name="Magnetic Joe Review"> {{citeweb|url=http://toucharcade.com/2009/02/14/highly-rated-magnetic-joe-game-arrives-on-the-iphone-for-free/|title=Magnetic Joe Review|publisher=Touch Arcade|accessdate=2014-02-24}}</ref>

==External links==
* [http://www.mobygames.com/game-group/magnetic-joe-series Mobygames Magnetic Joe series page]

==References==
{{reflist|30em}}

[[Category:Puzzle video games]]
[[Category:2006 video games]]
[[Category:Platform games]]
[[Category:Mobile games]]