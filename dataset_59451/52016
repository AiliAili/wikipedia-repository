{{Infobox journal
| title = Journal of Physical Chemistry Letters
| cover = [[Image:letters.gif]]
| editor = [[George C. Schatz]]
| discipline = [[Physical chemistry]]
| abbreviation = J. Phys. Chem. Lett.
| publisher = [[ACS Publications]]
| country = United States
| frequency = Biweekly
| history = 2010-present
| impact = 8.539
| impact-year= 2015
| website = http://pubs.acs.org/journals/jpclcd
| link1 = http://pubs.acs.org/toc/jpclcd/current
| link1-name = Online access
| link2 = http://pubs.acs.org/loi/jpclcd
| link2-name = Online archive
| ISSN = 1948-7185
| OCLC = 819373282
| CODEN = JPCLCD
}}
The '''''Journal of Physical Chemistry Letters''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[American Chemical Society]], designed to complement the ''[[Journal of Physical Chemistry]]''. The [[editor-in-chief]] is George C. Schatz ([[Northwestern University]]). The ''Journal of Physical Chemistry Letters'' covers research on all aspects of [[physical chemistry]]. According to the ''[[Journal Citation Reports]]'', the journal had an [[impact factor]] of 7.458 for 2014<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Physical Chemistry Letters |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] }}</ref> and its latest impact factor is 8.539 for 2015.<ref name=2015IF>{{cite web |url=http://connect.acspubs.org/jcr |title=Impact Factor and Beyond |accessdate=2016-06-20}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://pubs.acs.org/journals/jpclcd}}

{{DEFAULTSORT:Journal Of Physical Chemistry Letters}}
[[Category:American Chemical Society academic journals]]
[[Category:Biweekly journals]]
[[Category:Chemistry journals]]
[[Category:Physical chemistry journals]]
[[Category:Publications established in 2010]]
[[Category:English-language journals]]